--
-- File:: pipeline/export/ui/map_txd_ui.ms
-- Description:: Map TXD Exporter Rollout Definition
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 10 June 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/maptxd.ms"
filein "pipeline/util/string.ms"
filein "rockstar/helpers/parentiser.ms"
filein "rockstar/util/mapobj.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

--
-- name: RsParentTxdRoll
-- desc: Parent TXD rollout definition.
--
rollout RsParentTxdRoll "Parent Txd"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Map_Export#Parent_Txd" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	group "Map:"
	(
		dropdownlist lstMaps ""
		button btnRefresh 			"Refresh" align:#right
	)
	button btnParentise 			"Parentise Map..." 	width:120
	button btnTxd 						"Export Extra Txd's" 	width:120
	button btnGlobals 				"Global Txd's..."		width:120
	button btnTxdGlobal 			"Export Global Txd's"	width:120
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	
	fn RefreshMapList = (

		local maps = RsMapGetMapContainers()
		local mapnames = #()
		for map in maps do
		(
			if ( not map.is_exportable() ) then
				continue	
		
			append mapnames map.name
		)
		
		lstMaps.items = mapnames	
	)
	
	-------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------
	
	--
	-- event: 
	-- desc: Refresh the map list for TXD parentiser.
	--
	on btnRefresh pressed do (
		RefreshMapList()
	)
	
	--
	-- event:
	-- desc:
	--
	on btnParentise pressed do (
		
		local maps = RsMapGetMapContainers()
		local themap = undefined
		for map in maps do
		(
			if ( map.name == lstMaps.selected ) then
			(
				themap = map
				break;
			)
		)
		
		if ( undefined != themap ) then
		(
			RsMapSetupGlobals themap
			if ( "" != RsMapName ) then (

				RsCreateParentiserDialog RsMapName
			)
		)
		else
		(
			messageBox "Invalid map selection.  Refresh the list for the currently loaded containers."
		)
	)
	
	--
	-- event:
	-- desc:
	--
	on btnTxd pressed do (
	
		if RsPreExport() == false then return false
	
		local txd = RsMapParentTXD()
		txd.DoSceneTxdExport()
		RsPostExport()
		
		if (RsQueryBoxTimeOut "do you want to build the map?") then (
		
			RsMapBuildImage()
		)
	)
	
	--
	-- event: 
	-- desc:
	--
	on btnTxdGlobal pressed do (
	
		local txd = RsMapParentTXD()
		txd.DoGlobalTxdExport()
	)
	
	--
	-- event:
	-- desc:
	--
	on btnGlobals pressed do (
	
		RsCreateParentiserDialog "global"	
	)
	
	--
	-- event: RsParentTxdRoll open
	-- desc:
	--
	on RsParentTxdRoll open do (
	
		RefreshMapList()
	)
	
	--
	-- event: RsParentTxdRoll closed
	-- desc:
	--
	on RsParentTxdRoll close do (
	
		RsSettingWrite "rsparenttxd" "rollup" (not RsParentTxdRoll.open)
	)
)

--
-- name: RsMapTxdRoll
-- desc: Map TXD rollout definition.
--
rollout RsMapTxdRoll "Txd Map Export"
(	
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Map_Export#Txd_Map_Export" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	dropdownlist ddlTxdList "Txd's:"
	
	button btnExportAll "Export All" width:85 offset:[-45,0]
	button btnRefresh "Refresh" width:85 offset:[45,-26]
	
	button btnExport "Export" width:85 offset:[-45,0]
	button btnExportSel "Export Selected" width:85 offset:[45,-26]
	
	button btnColourCode "Colour by Txd" width:85 offset:[-45,0]
	button btnSelect "Select by Txd" width:85 offset:[45,-26]
	
	button btnExportTex "Export Textures"
	button btnExportNMTex "Export NormalMap Textures"
	button btnExportTwiddle "Export Twiddled Normal Map"
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	
	--
	-- name: RefreshTxds
	-- desc: Refresh Txd list (called from file open callback and during export)
	--
	fn RefreshTxds = (
		
		local txd = RsMapTXD()
		ddlTxdList.items = txd.GetAllSceneTXDs()
	)

	-------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------
	
	--
	-- event: btnExport pressed
	-- desc: Export single highlighted TXD
	--
	on btnExport pressed do (
	
		if RsPreExport() == false then return false
		
		local txd = RsMapTXD()
		txd.ExportSingleTXD ddlTxdList.selected

		RsPostExport()
		
		if (querybox "do you wish to build the map?") == true then (
				
			RsMapBuildImage()
		)
	)

	--
	-- event: btnExportAll pressed
	-- desc: Export all TXDs
	--
	on btnExportAll pressed do (
		
		if RsPreExport() == false then return false
		
		local txd = RsMapTXD()
		txd.ExportAll()		
		
		RsPostExport()
		
		if (querybox "do you wish to build the map?") == true then (
				
			RsMapBuildImage()
		)
	)
	
	--
	-- event: btnExportSel
	-- desc: Export txd's of selected objects
	--
	on btnExportSel pressed do (
		
		if RsPreExport() == false then return false
		
		local txd = RsMapTXD()
		txd.ExportSelected()
		
		RsPostExport()
		
		if (querybox "do you wish to build the map?") == true then (
				
			RsMapBuildImage()
		)
		
	)

	--
	-- event: btnExportTwiddle pressed
	-- desc:
	--
	on btnExportTwiddle pressed do (
	
		inputFilename = getOpenFileName caption:"source file..."
		
		if inputFilename != undefined then (
		
			outputFilename = getSaveFileName caption "target file..." types:"dds files (*.dds)|*.dds" filename:inputFilename

			if outputFilename != undefined then (
				if querybox "Do you want to keep the native texture resolution?" == true then (
					rexSetTextureShift 0
					rexExportBumpTexture inputFilename outputFilename RsOtherTexMaxSize true
					rexSetTextureShift 1
				) else (
					rexExportBumpTexture inputFilename outputFilename RsOtherTexMaxSize true
				)
			)
		)
	)

	--
	-- event: btnExportTex pressed
	-- desc: Export all textures for scene objects.
	--
	on btnExportTex pressed do (
	
		txdlist = #()
		txdobjlist = #()
		RsGetTxdList rootnode.children txdlist txdobjlist
		local txd = RsMapTXD()
		txd.DeleteTextures txdlist txdobjlist
		txd.ExportTextures txdlist txdobjlist
	)

	--
	-- event: btnExportNMTex pressed
	-- desc: Export all normal maps for scene objects.
	--
	on btnExportNMTex pressed do (
	
		txdlist = #()
		txdobjlist = #()
		local txd = RsMapTXD()
		RsGetTxdList rootnode.children txdlist txdobjlist
		txd.DeleteTextures txdlist txdobjlist normalonly:true
		txd.ExportTextures txdlist txdobjlist normalonly:true
	)

	--
	-- event: 
	-- desc: colour code all the objects by which txd's they use
	--
	on btnColourCode pressed do (
	
		local idxTxd = getattrindex "Gta Object" "TXD"
		
		if idxTxd == undefined do return false

		txdlist = #()
		txdcolour = #()
		
		for obj in rootNode.children do (

			if (getattrclass obj) == "Gta Object" do (
			
				valTxd = getattr obj idxTxd
				
				if (findItem txdList valTxd) == 0 do (
				
					append txdList valTxd
				)
			)
		)

		for txdlists=1 to txdlist.count do (
		
			txdcolour[txdlists] = [(random 0 255),(random 0 255),(random 0 255)]
		)
		
		for obj in rootNode.children do (

			if (getattrclass obj) == "Gta Object" do (
			
				valTxd = getattr obj idxTxd 
				objTxd = findItem txdList valTxd
				if objTxd > 0 then obj.wirecolor = txdcolour[objTxd]
			)
		)		
	)
	
	--
	-- event: btnSelect pressed
	-- desc: select all objects that use the current txd
	--
	on btnSelect pressed do (
			
		txdName = RsLowercase(ddlTxdList.selected)
		objList = #()
	
		mapObjectList = #()	
		RsGetMapObjects rootnode.children mapObjectList
	
		local txd = RsMapTXD()
		for obj in mapObjectList do (
	
			txd.GetObjectWithTxd objList obj txdName
		)		
		
		select objList
	)

	--
	-- event: btnRefresh pressed
	-- desc: Refresh our UI list of TXDs in scene.
	--
	on btnRefresh pressed do (
	
		local txd = RsMapTXD()
		ddlTxdList.items = txd.GetAllSceneTXDs()
	)

	--
	-- event: Rollout open
	-- desc: initialisation of rollout, install event handler to
	--       refresh the TXD list on file open.
	--
	on RsMapTxdRoll open do (
		
		callbacks.addScript #filePostOpen "RsMapTxdRoll.RefreshTxds()" id:#RsMapPostOpen

		--local txd = RsMapTXD()
		--ddlTxdList.items = txd.GetAllSceneTXDs()	
	)
	
	--
	-- event: Rollout close
	-- desc: shutdown of rollout, remove event handler, save rolled state
	--
	on RsMapTxdRoll close do (
	
		callbacks.removeScripts #filePostOpen id:#RsMapPostOpen
	
		RsSettingWrite "rsmaptxd" "rollup" (not RsMapTxdRoll.open)
	)
)

-- pipeline/export/ui/map_txd_ui.ms
