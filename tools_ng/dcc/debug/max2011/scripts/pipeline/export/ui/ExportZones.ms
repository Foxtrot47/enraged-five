--
-- File:: pipeline/export/ui/ExportZones.ms
-- Description:: Export Zones
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- 
-- Based off of the original by Greg Smith.
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/zones.ms"
filein "pipeline/import/ui/ImportZones.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

rollout RsZoneExporterRoll "Zone Exporter"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Export/Import_Zones#Export_Zones" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnExportPopZones	"Export POPZONES" 	width:120
	button btnExportMapZones	"Export MAPZONES" 	width:120
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------

	--
	-- event: btnExportPopZones pressed
	-- desc: 
	--
	on btnExportPopZones pressed do (
		RsMapExportPopZoneFile ( getSaveFilename caption:"Zone IPL files" types:"Zone IPL Files (*.ipl)|*.ipl" )
	)
	
	--
	-- event: btnExportMapZones pressed
	-- desc: 
	--
	on btnExportMapZones pressed do (
		RsMapExportMapZoneFile ( getSaveFilename caption:"Zone IPL files" types:"Zone IPL Files (*.ipl)|*.ipl" )
	)
)


-----------------------------------------------------------------------------
-- Entry-Point
-----------------------------------------------------------------------------

try CloseRolloutFloater RsZoneExporterUtil catch()
RsZoneExporterUtil = newRolloutFloater "Export/Import Zones" 200 215 50 126
addRollout RsZoneExporterRoll RsZoneExporterUtil
addRollout RsZoneImporterRoll RsZoneExporterUtil

-- ExportZones.ms
