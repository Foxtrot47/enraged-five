--
-- File:: MapSceneXmlExporter.ms
-- Description:: SceneXml Exporter Rollout Definition
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 11 February 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/scenexml.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

rollout RsSceneXmlRoll "SceneXml"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp					"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Map_Export#SceneXml" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button 	btnSaveSceneXml		"Save SceneXml" 			width:120
	button 	btnSaveSceneXmlAs "Save SceneXml As..." width:120
	button	btnTestMap				"Test Map"						width:120
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None	
	
	-------------------------------------------------------------------------
	-- UI Events
	-------------------------------------------------------------------------

	-------------------------------------------------------------------------
	-- "Save SceneXml" pressed
	-------------------------------------------------------------------------
	on btnSaveSceneXml pressed do (
	
		local maps = RsMapGetMapContainers()
		for map in maps do
		(
			if ( not map.is_exportable() ) then
				continue		
		
			format "Exporting %\n" map.name
			RsMapSetupGlobals map
			RsSceneXmlExport map
		)
	)

	-------------------------------------------------------------------------
	-- "Save SceneXml As" pressed
	-------------------------------------------------------------------------
	on btnSaveSceneXmlAs pressed do (
	
		filename = getSaveFileName caption:"Save SceneXml as..." \
					types:"XML File (*.xml)|*.xml"
		if ( undefined != filename ) then
			RsSceneXmlExportAs filename
	)
	
	-------------------------------------------------------------------------
	-- "Test Map" pressed
	-------------------------------------------------------------------------
	on btnTestMap pressed do (
	
		try
		(
			RsSceneXmlValidate()
		)
		catch
		(
			format "RsSceneXmlValidate exception: %\n" ( getCurrentException() )
		)
	)
		
	-------------------------------------------------------------------------
	-- Rollout close handler
	-------------------------------------------------------------------------	
	on RsSceneXmlRoll close do
	(	
		RsSettingWrite "rsscenexml" "rollup" (not RsSceneXmlRoll.open)
	)
)

-- MapSceneXmlExporter.ms
