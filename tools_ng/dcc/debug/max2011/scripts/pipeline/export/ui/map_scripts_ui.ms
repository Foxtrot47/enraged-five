--
-- File:: pipeline/export/ui/map_scripts_ui.ms
-- Description:: Local Asset Checks rollout
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 24 August 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"
filein "pipeline/util/file.ms"
filein "pipeline/util/string.ms"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------

--
--
--
--
rollout RsScriptsRoll "Local Asset Checks"
(
	button btnCheckObjects "Check Objects" width:100
	button btnCheckPeds "Check Peds" width:100
	button btnCheckRepeats "Check Repeats" width:100
	button btnCheckUnused "Check Unused" width:100
	button btnGetUsage "Get Usage" width:100
	button btnSelectUsage "Select From Usage" width:100
	edittext edtFindObject "Object to Find:"
	button btnFindObject "Find Object" width:100
	
	on btnFindObject pressed do (
	
		objectName = RsLowercase (edtFindObject.text)
	
		utilityFolder = RsConfigGetProjRootDir() + RsConfigGetProjectName() + "_bin/" + RsConfigGetProjectName() + "_general/utility/"
		commandLine = RsConfigGetBinDir() + RsConfigGetRagebuilder() + " " + utilityFolder + "findobject.rbs -find " + objectName
		DOSCommand commandLine		
	)
	
	on btnGetUsage pressed do (
	
		mapName = RsLowercase ((filterstring maxfilename ".")[1])
	
		rubyFileName = RsConfig.toolsroot + "/tools/util/data_get_props_usage.rb"
		args = " --proj='" + RsConfigGetProjectName() + "'" + " --map='" + mapName + "' --output='x:/get_usage.txt'"
		commandLine = "ruby " + rubyFileName + args
		print commandLine
		DOSCommand commandLine
		edit "x:/get_usage.txt"
	)
	
	on btnSelectUsage pressed do (
	
		filenameOutput = getopenfilename()
		
		fileOutput = openfile filenameOutput mode:"rb"
		foundList = #()
		
		while eof fileOutput == false do (
		
			fileLine = readline fileOutput
			
			findIndex = findstring fileLine "-"
			
			if findIndex != undefined then (
			
				objTokens = filterstring fileLine "-"
				objName = objTokens[1]
				obj = getnodebyname objName exact:true ignorecase:true
				
				if obj != undefined then (
				
					append foundList obj
				)
			)
		)
		
		close fileOutput
		
		select foundList		
	)
	
	on btnCheckObjects pressed do (
		utilityFolder = RsConfigGetProjRootDir() + "bin/general/utility/"
		commandLine = RsConfigGetBinDir() + RsConfigGetRagebuilder() + " " + utilityFolder + "checkObjects.rbs" 
		DOSCommand commandLine		
		
		filenameOutput = "c:/missingobjects.txt"
		fileOutput = createfile filenameOutput --mode:"rb"
		foundList = #()
		
		while eof fileOutput == false do (
		
			fileLine = readline fileOutput
			
			findIndex = findstring fileLine "-image:/"
			
			if findIndex != undefined then (
			
				objName = substring fileLine 1 (findIndex - 1)
				obj = getnodebyname objName exact:true ignorecase:true
				
				if obj != undefined then (
				
					append foundList obj
				)
			)
		)
		
		close fileOutput
		
		select foundList
	)
	
	on btnCheckPeds pressed do (
		utilityFolder = RsConfigGetProjRootDir() + "bin/general/utility/"
		commandLine = RsConfigGetBinDir() + RsConfigGetRagebuilder() + " " + utilityFolder + "checkPeds.rbs" 
		DOSCommand commandLine
	)
	
	on btnCheckRepeats pressed do (
		utilityFolder = RsConfigGetProjRootDir() + "bin/general/utility/"
		commandLine = RsConfigGetBinDir() + RsConfigGetRagebuilder() + " " + utilityFolder + "checkRepeats.rbs" 
		DOSCommand commandLine
	)
	
	on btnCheckUnused pressed do (
	
		utilityFolder = RsConfigGetProjRootDir() + "bin/general/utility/"
		commandLine = RsConfigGetBinDir() + RsConfigGetRagebuilder() + " " + utilityFolder + "checkUnused.rbs" 
		DOSCommand commandLine
	)
	
	on RsScriptsRoll close do (
	
		RsSettingWrite "rsmapchecks" "rollup" (not RsScriptsRoll.open)
	)
)

-- pipeline/export/ui/map_scripts_ui.ms
