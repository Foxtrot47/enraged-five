--
-- File:: weapon_exporter_ui.ms
-- Description:: Weapons exporter UI rollout.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- 
-- Based off of the original by Greg Smith.
--

-- Weapon-specific includes
filein "pipeline/export/models/weapon.ms"
filein "pipeline/export/models/ruby_script_generator.ms"
filein "pipeline/export/models/validation.ms"

-- Additional includes
filein "pipeline/export/ui/weapon_exporter_ui_anim.ms"
filein "pipeline/export/ui/ExporterErrorDialog.ms"
filein "pipeline/export/ui/shared_texture_list_ui.ms"
filein "pipeline/util/file.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

rollout RsWeaponRoll "Weapon Export"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Model_Export#Weapon_Export" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnExportSel		"Export Selected" 	width:100
	button btnPreviewSel	"Preview Selected" 	width:100
	button btnExportAll 	"Export All" 				width:100
	button btnExtractRPF	"Extract RPF"				width:100
	button btnBuildRPF 		"Build RPF" 				width:100
	checkbox chkExportMeshMap	"Export Mesh Map"	checked:false visible:false
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------

	--
	-- event: btnExportSel pressed
	-- desc: Invokes weapon exporter on selected weapons
	--
	on btnExportSel pressed do (
	
		RsWeaponExportSel false chkExportMeshMap.checked
	)
	
	--
	-- event: btnPreviewSel pressed
	-- desc: Invokes preview weapon exporter on selected weapons
	--
	on btnPreviewSel pressed do (
		RsClearPatchDir()
		RsWeaponExportSel true false
	)
	
	--
	-- event: btnExportAll pressed
	-- desc: Invokes weapon exporter for all top-level meshes in file.
	--
	on btnExportAll pressed do (
	
		local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
		for obj in $objects do (

			if undefined != obj.parent then
				continue

			if ( GetAttr obj idxDontExport ) then
				continue

			select obj
		
			format "Exporting weapon: %\n" obj.name
			if RsIsFileReadOnly (RsConfigGetIndependentDir() + "models/cdimages/weapons.rpf") == false do (
						
				messagebox "weapons.rpf is read-only"
				return false
			)
			if RsWeaponExportIsValidForExport() == false then return false
			if RsWeaponExportModel() == false then return false
			if RsWeaponExportResourceModel false == false then return false
			if RsWeaponAnimExportResource() == false then return false
			if RsWeaponExportResourceTxd false == false then return false

			if chkExportMeshMap.checked == true then (
				if RsWeaponExportMeshMap() == false then return false
			)
		)
		
		if RsWeaponExportResourceBounds false == false then return false
		if RsWeaponExportBuildRPF() == false then return false
	)

	--
	-- event: btnExtractRPF pressed
	-- desc: Extracts the current weapons image into the user's stream.
	--
	on btnExtractRPF pressed do (
		RsWeaponExtractRPF()
	)
	
	
	--
	-- event: btnBuildRPF pressed
	-- desc: Build the weapons image from the configured stream contents.
	--
	on btnBuildRPF pressed do (
		if RsWeaponExportBuildRPF() == false then return false
	)
	
	--
	-- event: RsWeaponRoll close
	-- desc: shutdown of rollout.
	--
	on RsWeaponRoll close do (
	
		RsSettingWrite "rsweapon" "rollup" (not RsWeaponRoll.open)
		if ( undefined != shared_tex_list ) then
			RsSettingWrite "rsweapontex" "rollup" (not shared_tex_list.open)
	)
)

-- weapon_exporter_ui.ms
