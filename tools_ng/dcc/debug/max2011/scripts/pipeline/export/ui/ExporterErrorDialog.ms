--
-- ExporterErrorDialog.ms
-- Generic exporter error dialog
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 25 February 2008
--


--
-- name: RsCreateExporterErrorDialog
-- desc:
--
fn RsCreateExporterErrorDialog rollname caption messages = (

	rci = rolloutCreator rollname caption
	rci.begin()

	-------------------------------------------------------------------------
	-- Locals
	-------------------------------------------------------------------------
	
	rci.addLocal "erroritems" init:messages
	
	-------------------------------------------------------------------------
	-- Widgets
	-------------------------------------------------------------------------	
	
	rci.addControl #listbox #lstMessages "Errors / Warnings:" paramStr:"items:erroritems"

	-------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------

	-- None
	
	-------------------------------------------------------------------------
	-- Create rollout and return
	-------------------------------------------------------------------------
	
	-- This returns the dynamic rollout definition to the caller.
	-- It can be passed to the addRollout function.
	rci.end()	
)


-- End of ExporterErrorDialog.ms
