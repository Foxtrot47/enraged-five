--
-- File:: Vehicle_Exporter_UI_Anim.ms
-- Description:: Vehicle exporter animation UI rollout.
--
-- Author:: Adam Munson <adam.munson@rockstarnorth.com>
-- Date:: 08 July 2010
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/util/dialog.ms"

rollout RsVehicleAnimRoll "Vehicle Animation"
(

	button btnSaveAnimation "Save Animation" width:100
	button btnLoadAnimation	"Load Animation" width:100
	button btnExportAnimation "Export Animation" width:100
	
	on btnSaveAnimation pressed do (
	
		if (selection.count == 1 and classof selection[1] == Dummy) then (
				
			rollout saveAnimName "Animation name" (

				edittext edtAnimName "" fieldWidth:150
				button btnSaveAnim "Save" width:100

				on btnSaveAnim pressed do (

					if (edtAnimName.text != "") then (

						animDir = maxFilePath + "anim"
						RsMakeSurePathExists animDir
						fileName = (filterstring maxFileName ".")[1]
						saveFilename = animDir + "\\" + fileName + "_" + edtAnimName.text + ".xaf"
						RsVehicleAnimSave saveFilename selection[1]
					) 
					else (
					
						messagebox "No name given - animation not saved"
					)
					destroyDialog saveAnimName
				)
			)
			createDialog saveAnimName 300 50
		)
		else (

			messagebox "Select the vehicle dummy"					
		) 
	)
	
	on btnLoadAnimation pressed do (
	
		if (selection.count == 1 and classof selection[1] == Dummy) then (

			global animsList = #()				
			RsGetVehicleAnimationList maxfilename animsList
			global animNames = #()				
			
			if (animsList.count != 0) then (
			
				--Filter the filename to find the animation name which is
				--between the last underscore and .xaf, as added by this script
				--when the animation is saved.
				for anim in animsList do (
			
					tokens = filterstring anim "_."
					append animNames tokens[tokens.count-1]
				)				
						
				rollout loadAnimName "Select Animation to load:" (

					listbox lstAnims "Animations" items:animNames
					button btnLoadAnim "Load Animation" width:100

					on btnLoadAnim pressed do (

						destroyDialog loadAnimName
						RsVehicleAnimLoad animsList[lstAnims.selection] selection[1]
					)
				)
				createDialog loadAnimName 300 200			
			)
			else (
			
				messagebox ("No animations found for " + selection[1].name)
			)
		)
		else (

			messagebox "Select the vehicle dummy"					
		) 
	)
	
	on btnExportAnimation pressed do (
	
		if (selection.count == 1 and classof selection[1] == Dummy) then (
		
			global animsList = #()				
			RsGetVehicleAnimationList maxfilename animsList
			global animNames = #()				

			if (animsList.count != 0) then (

				for anim in animsList do (

					tokens = filterstring anim "_."
					append animNames tokens[tokens.count-1]
				)				
				
				-- Rather than use a global to monitor last loaded anim,
				-- just force it to load the animation again
				rollout exportAnimName "Select Animation to export:" (

					listbox lstAnims "Animations" items:animNames
					button btnExportAnim "Export Animation" width:100

					on btnExportAnim pressed do (

						animFilename = animsList[lstAnims.selection]
						destroyDialog exportAnimName
						RsVehicleAnimLoad animFilename selection[1]	
						RsVehicleAnimExport animFilename selection[1]
						
						if ( RsQueryBoxTimeOut "Resource clip dictionary?" ) do	(
						
							RsVehicleAnimExportResource selection[1]
						)
					)
				)
				createDialog exportAnimName 300 200			
			)
			else (

				messagebox ("No animations found for " + selection[1].name)
			)
		)
		else (
		
			messagebox "Select the vehicle dummy"					
		) 		
	)
	
	on RsVehicleAnimRoll close do (
		
		RsSettingWrite "rsvehicleanim" "rollup" (not RsVehicleAnimRoll.open)
	)
)