--
-- File:: pipeline/export/ui/map_namespace_ui.ms
-- Description:: Map Namespace Rollout Definition
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 17 July 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/globals.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

rollout RsMapNamespaceRoll "Object Namespace"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink 	lnkHelp			"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Map_Export#Object_Namespace" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button 		btnSetupObjects	"Setup Objects" 	width:120
	button 		btnFindDuplicates	"Find Duplicate object names"	width:150
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------

	--
	-- event: "Setup Objects" button pressed
	-- desc: 
	--
	
	fn findDuplicateObjects = 
	(
		collectedObjects = #()
		duplicates = #()
		for obj in objects do 
		(
			local index = 0
			for oIndex = 1 to collectedObjects.count do
			(
				if collectedObjects[oIndex].name==obj.name then 
				(
					index=oIndex
					break
				)
			)	
			if(index!=0) then 
			(
				print (obj.name+", "+collectedObjects[index].name)
				append duplicates obj
				append duplicates collectedObjects[index]
			)
			append collectedObjects obj
		)
		select duplicates
	)
	
	on btnSetupObjects pressed do (
		
		local maps = RsMapGetMapContainers()
		for map in maps do
		(
			if ( not map.is_exportable() ) then
				continue		
		
			RsMapSetupGlobals map
			RsMapNamespacePass map
		)
	)
	
	on btnFindDuplicates pressed do findDuplicateObjects()
	--
	-- event: rollout close event
	-- desc:
	--
	on RsMapNamespaceRoll close do (
		
		RsSettingWrite "rsmapnamespace" "rollup" (not RsMapNamespaceRoll.open)
	)
)

-- pipeline/export/ui/map_namespace_ui.ms
