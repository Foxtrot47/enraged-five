--
-- File:: pipeline/export/ui/map_collada_ui.ms
-- Description:: Map Collada Exporter Rollout Definition
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 1 May 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/collada.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

rollout RsMapColladaRoll "Collada Export"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Map_Export#Collada_Export" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button 	btnSaveSelected	"Save Selected" 	width:120
	button	btnSaveMap		"Save Map"			width:120
	button	btnSaveMapZipped "Save Map (zipped)" width:120
	button 	btnDeleteFiles	"Delete Files"		width:120
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None (see pipeline/export/maps/collada.ms)
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------

	--
	-- event: "Save Selected" button pressed
	-- desc: Save selected geometry out to COLLADA files
	--
	on btnSaveSelected pressed do (
	
		if ( 0 == selection.count ) then
		(
			MessageBox "No objects selected to export as Collada files."	
		)
		else
		(
			RsMapCollada.ExportSelected()
		)
	)

	--
	-- event: "Save Map" button pressed
	-- desc: Save all map to COLLADA files
	--
	on btnSaveMap pressed do (
	
		RsMapCollada.Export()
	)
	
	--
	-- event: "Save Map (zipped)" button pressed
	-- desc: Save all map to COLLADA files 
	--
	on btnSaveMapZipped pressed do (
	
		RsMapCollada.Export zipped:true
	)

	--
	-- event: "Delete Files" button pressed
	-- desc: Delete all files from the COLLADA export stream.
	--
	on btnDeleteFiles pressed do (
	
		RsMapSetupGlobals()
		RsMapCollada.CleanFiles()
	)

	--
	-- event: Rollout close handled
	-- desc: 
	--
	on RsMapColladaRoll close do (
		RsSettingWrite "rscollada" "rollup" (not RsMapColladaRoll.open)
	)
)

-- pipeline/export/ui/map_collada_ui.ms
