--
-- File:: pipeline/export/weapons/anim.ms
-- Description:: Animated weapon exporter functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 17 April 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/hierarchy.ms"
filein "rockstar/util/rexreport.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsWeaponIsValidBone
-- desc: Determine whether a particular node is a valid weapon bone.
--
fn RsWeaponAnimIsValidBone rootbone = 
(
	local retval = true

	if (classof rootbone != Editable_mesh) and 
		(classof rootbone != Editable_poly) and 
		(classof rootbone != Point) and 
		(classof rootbone != BoneGeometry) then 
	(
		retval = false
	)	
	retval
)

--
-- name: RsWeaponGetAnimatedBones
-- desc: Return all animated bones for a weapon object.
--
fn RsWeaponGetAnimatedBones obj sel = 
(
	if RsWeaponAnimIsValidBone obj then (
	
		format "Appending: %\n" obj.name
		append sel obj
	)
	
	for childobj in obj.children do (
	
		RsWeaponGetAnimatedBones childobj sel
	)
)

--
-- name: RsWeaponAnimLoad
-- desc: Load weapon animation from XAF file.
--
fn RsWeaponAnimLoad animFilename = (

	local weaponRootBone = ( getNodeByName "gun_ROOT" exact:true ignoreCase:true )
	local weaponBones = ( RsHierarchy_AllRootChildren weaponRootBone )
	
	LoadSaveAnimation.setUpAnimsForLoad weaponBones
	return ( LoadSaveAnimation.loadAnimation animFilename weaponBones )
)

--
-- name: RsWeaponAnimSaveAs
-- desc: Save weapon animation to XAF file.
--
fn RsWeaponAnimSaveAs animFilename = (

	local weaponRootBone = ( getNodeByName "gun_ROOT" exact:true ignoreCase:true )
	local weaponBones = ( RsHierarchy_AllRootChildren weaponRootBone )
	local attrNames = #()
	local attrValues = #()
	
	LoadSaveAnimation.setUpAnimsForSave weaponBones animatedTracks:true includeContraints:true keyable:true
	return ( LoadSaveAnimation.saveAnimation animFilename weaponBones attrNames attrValues animatedTracks:true includeConstraints:true keyableTracks:true saveSegment:true segInterval:animationRange segKeyPerFrame:true )
)

--
-- name: RsWeaponAnimIsValidForExport
-- desc: Determine whether the current weapon model is valid for an animated weapon.
--
fn RsWeaponAnimIsValidForExport = 
(

	local gunRootBone = ( getNodeByName "gun_ROOT" exact:true ignoreCase:true )
	if ( undefined == gunRootBone ) then
	(
		MessageBox "Animated weapon model has no \"gun_ROOT\" bone defined.  Export aborted."
		return ( false )
	)

	return ( true )
)

--
-- name: RsWeaponAnimExportResource
-- desc: Resource the weapon anim data using RageBuilder.
--
fn RsWeaponAnimExportResource = 
(
	local weaponName = ( RsRemoveExtension maxfilename )
	local weaponFile = "weapons/" + weaponName

	local rbFileName = RsConfigGetScriptDir() + "weapons/" + weaponName + "_anim_resourcing.rb"
	RsMakeSurePathExists rbFileName
	
	local rbFile = openfile rbFileName mode:"w+"
	RBGenWeaponResourcingScriptPre rbFile weaponName false
	RBGenWeaponResourcingScriptAnim rbFile
	RBGenResourcingScriptPost rbFile
	close rbFile

	-- Run the Ruby resourcing script for the animation data.
	local cmdline = "ruby " + rbFileName
	format "Command line: %\n" cmdline
	if ( doscommand cmdline != 0 ) then 
	(
		messagebox "Failed to build anim resource"
		return false
	)			

	return true
)

--////////////////////////////////////////////////////////////
fn RsFindParentMostBone rootbone = (

	while classof rootbone.parent == Biped_Object or classof rootbone.parent == BoneGeometry do (
	
		rootbone = rootbone.parent
	)
	
	rootbone
)

--////////////////////////////////////////////////////////////
fn RsGetAnimFlags selbone = (

	checkobj = RsFindParentMostBone selbone
--	checkobj = RsFindFirstValidBoneForTracks checkobj
	
	flags = 0
	
	if checkobj != undefined then (
	
		numnotetrks = numnotetracks checkobj	
		
		if numnotetrks > 0 then (
		
			flags = flags + 2

			foundEffectKeys = false
			
			for i = 1 to numnotetrks do (					
			
				notetrk = getnotetrack checkobj i
				
				if notetrk.name == "Event_Track" then (
				
					for notetrkkey in notetrk.keys do (
										
						checkVal = notetrkkey.value as integer
					
						if checkVal != undefined then (
					
							if 	(bit.and checkVal (bit.shift 1 20) > 0) or \
								(bit.and checkVal (bit.shift 1 21) > 0) or \
								(bit.and checkVal (bit.shift 1 22) > 0) or \
								(bit.and checkVal (bit.shift 1 23) > 0) or \
								(bit.and checkVal (bit.shift 1 24) > 0) or \
								(bit.and checkVal (bit.shift 1 25) > 0) or \
								(bit.and checkVal (bit.shift 1 26) > 0) then (
	
								foundEffectKeys = true
							)
						)
					)
				)
			)
			
			if foundEffectKeys then (
			
				flags = flags + 512
			)
		)
	)
	
	extraMoverNode = getnodebyname "mover" exact:true
	
	if extraMoverNode != undefined then flags = flags + 4
	
	flags
)

fn RsFindParentMostBoneForExportRec rootbone = (

	while rootbone.parent != rootnode and rootbone.parent != undefined do (
	
		rootbone = rootbone.parent
	)
	
	rootbone
)

--////////////////////////////////////////////////////////////
fn RsGetDepth obj = (

	depth = 0
	
	if obj != undefined then (

		depth = 1

		while (obj.parent != rootnode) and (obj.parent != undefined) do (

			obj = obj.parent
			depth = depth + 1
		)
	)
	
	depth
)

--////////////////////////////////////////////////////////////
fn RsFindParentMostBoneForExport selset = (

	highest = undefined
	
	for obj in selset do (
	
		parentmost = RsFindParentMostBoneForExportRec obj
		
		if highest == undefined or RsGetDepth highest > RsGetDepth obj then (
		
			highest = parentmost
		)
	)
	
	highest
)

--
-- name: RsWeaponAnimExportData
-- desc: Export the animation data (.anim/.clip) using RexMax.
--
fn RsWeaponAnimExportData animFilename =
(
	local weaponName = ( RsRemoveExtension maxfilename )
	local weaponFile = ( RsConfigGetStreamDir() + "weapons/" + weaponName )
	local weaponDir = ( weaponFile + "/" )
	local animName = ( RsRemovePathAndExtension animFilename )
	local animPath = ( RsRemoveFilename animFilename )
	local clipFile = ( RsRemoveExtension animFilename )
	local boneGunRoot = ( getNodeByName "gun_ROOT" exact:true ignoreCase:true )
	
	RsMakeSurePathExists weaponDir
	RsMakeSurePathExists animPath

	rexReset()
	local graphRoot = rexGetRoot()

	-------------------------------------------------------------------------
	-- Rex Export Animation
	-------------------------------------------------------------------------
	local animation = rexAddChild graphRoot animName "Animation"
	rexSetPropertyValue animation "OutputPath" animPath
    -- sconde: Need to export projectData channels as they contain the note tracks
	rexSetPropertyValue animation "ChannelsToExport" "(translateX;translateY;translateZ;)(rotateX;rotateY;rotateZ;)(projectData;projectData0;projectData1;projectData2;projectData3;projectData4;projectData5;projectData6;projectData7;)"
	rexSetPropertyValue animation "AnimMoverTrack" "false"
	rexSetPropertyValue animation "AnimAuthoredOrient" "true"
	rexSetPropertyValue animation "AnimCompressionTolerance" "0.0005"
	rexSetPropertyValue animation "AnimExtraMoverTrack" "mover"
	rexSetPropertyValue animation "AnimLocalSpace" "true"
	rexSetPropertyValue animation "AnimExportSelected" "true"
	
	clearSelection()
	local selset = #()
	RsWeaponGetAnimatedBones boneGunRoot selset
	select selset

    -- sconde: Find the parent most bone
	selobj = undefined
	if (classof selection[1] == Biped_Object or classof selection[1] == BoneGeometry) then selobj = RsFindParentMostBone selection[1]
	else selobj = RsFindParentMostBoneForExport selection

	-- sconde: For some reason the track's name is not saved in the .xaf file
	-- sconde: The EventTrackManager.ms file assumes track 1 is the Event_Track so do the same here (should probably be in a common file or something)
	track = getNoteTrack selobj 1
	if (track != undefined) then
	(
		track.name = "Event_Track"
	)

    -- sconde: Determine what the animation project flags should be and tell rex about them
	projectFlags = RsGetAnimFlags selobj
	rexSetPropertyValue animation "AnimProjectFlags" (projectFlags as string)

	rexSetNodes animation

	RsRexExportWithReport()
	clearSelection()

	-------------------------------------------------------------------------
	-- Export Clip
	-------------------------------------------------------------------------
	format "Saving clip %\n" clipFile
		
	clipEditInst = clipEditor clipFile
	clipsave clipFile boneGunRoot clipEditInst

	true
)

--
-- name: RsWeaponAnimExportCurrent
-- desc: Validate and export currently loaded animation to a file.
--
fn RsWeaponAnimExportCurrent animFilename =
(	
	if ( false == RsWeaponAnimIsValidForExport() ) then
		return ( false )
	if ( false == ( RsWeaponAnimExportData animFilename ) ) then
		return ( false )
)

-- pipeline/export/weapons/anim.ms
