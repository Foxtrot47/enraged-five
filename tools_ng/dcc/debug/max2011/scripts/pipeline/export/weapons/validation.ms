--
-- validation.ms
-- Weapon Export Validation Functions
--
-- David Muir <david.muir@rockstarnorth.com>
-- 5 August 2008
--

filein "pipeline/export/weapons/globals.ms"

--
-- Name: IsValidBonesRecCheck
-- Desc:
--
fn RsWeaponExportIsValidBonesRecCheck obj = (

	if classof obj == Editable_Mesh or classof obj == Editable_Poly or classof obj == Dummy then (

		objTag = getuserprop obj "tag"

		if objTag != undefined then (

			objTag = RsLowercase(objTag)
			idxFound = finditem RsWeaponTagList objTag

			if idxFound == 0 then (

				append RsWeaponTagList objTag
				append RsWeaponObjectList obj
			) else (

				messagebox ("tag " + objTag + " is repeated in " + obj.name + " and " + RsWeaponObjectList[idxFound].name) title:"error"
				return false
			)
		)
	)

	for childobj in obj.children do (

		if RsWeaponExportIsValidBonesRecCheck childobj == false then return false
	)

	true
)

--
-- Name: IsValidForExport
-- Desc: Check that we have a valid weapon selection for export.
--
fn RsWeaponExportIsValidForExport = (

	RsWeaponObjectList = #()
	RsWeaponTagList = #()

	if selection.count != 1 then (

		messagebox "please select just one object"
		return false			
	)

	if ( "Gta Object" != ( GetAttrClass selection[1] ) ) then (

		messagebox ("object has to be an editable mesh (" + selection[1].name + ")")
		return false
	)

	if selection[1].parent != undefined then (

		messagebox "has to be a root object"
		return false
	)

	if RsWeaponExportIsValidBonesRecCheck selection[1] == false then (

		return false
	)

	if (filterstring selection[1].name " ").count > 1 then (

		messagebox (selection[1].name + " has spaces in name, not exporting")
		return false
	)

	return true	
)

-- validation.ms
