--
-- globals.ms
-- Weapon Export Global Data
--
-- David Muir <david.muir@rockstarnorth.com>
-- 5 August 2008
--

filein "pipeline/util/shared_texture_list.ms"

RsWeaponObjectList = #()
RsWeaponTagList = #()

RsTextureList = #()
RsTextureLoad = "targa file (*.tga)|*.tga|bitmap file (*.bmp)|*.bmp"

if undefined == RsWeaponSharedTextureList then
	RsWeaponSharedTextureList = SharedTextureList()

RsWeaponsTextureListFilename = RsConfigGetContentDir() + "/shared_textures/weapons.xml"

-- global.ms
