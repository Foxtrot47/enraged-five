--
-- File:: pipeline/export/water.ms
-- Description:: Water export/import functions.
--
-- Author:: Greg Smith <greg@rockstarnorth.com>
-- Date:: 30/3/2004
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 31 March 2010
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
-- None

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
RsWaterErrors = #()
RsWaterErrorObjs = #()

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------

--
-- rollout: RsWaterErrorsRoll
-- desc:
--
rollout RsWaterErrorsRoll "Water Export Errors" 
(
	---------------------------------------------------------------------------
	-- UI
	---------------------------------------------------------------------------
	listbox lstErrors  	"Errors:" height:14 items:RsWaterErrors

	---------------------------------------------------------------------------
	-- Events
	---------------------------------------------------------------------------
	
	--
	-- event: lstErrors item selected
	-- desc: Select related object that generated the error.
	--
	on lstErrors selected item do (

		if ( undefined != RsWaterErrorObjs[item] ) then (
		
			if isdeleted RsWaterErrorObjs[item] == false then (
				select RsWaterErrorObjs[item]
				max zoomext sel
			)
		)
	)
)

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: ExportWaterTestVert
-- desc:
--
fn ExportWaterTestVert vertTest = (

	for i = 1 to 2 do (

		modval = abs (mod vertTest[i] 2.0)
		
		if modval > 0.0001 then (
			modval = abs(modval - 2.0)
		)

		if modval >= 0.0005 and modval <= 1.9995 then (
		
			print modval
		
			return false
		)
	)

	return true
)


--
-- name: ExportWaterTest
-- desc:
--
fn ExportWaterTest = (

	RsWaterErrors = #()
	RsWaterErrorObjs = #()

	for obj in rootnode.children do (
		
		if classof obj == Gta_Waterhelper then (
		
			failed = false
		
			width = obj.width / 2.0
			depth = obj.depth / 2.0
			
			vert1 = [-width,depth,obj.pointheight1]
			vert2 = [width,depth,obj.pointheight2]
			vert3 = [-width,-depth,obj.pointheight3]
			vert4 = [width,-depth,obj.pointheight4]
			
			vert1 = vert1 * obj.objecttransform
			vert2 = vert2 * obj.objecttransform
			vert3 = vert3 * obj.objecttransform
			vert4 = vert4 * obj.objecttransform
			
			if obj.type == true then (
-- 				if width != depth then (
-- 					append RsWaterErrors (obj.name + " not a 45 degree triangle")
-- 					append RsWaterErrorObjs obj
-- 					failed = true
-- 				)
				append RsWaterErrors (obj.name + " is a triangle section which is not supported at the moment.")
				append RsWaterErrorObjs obj
				failed = true
			)
			
			if(obj.rotation != (quat 0 0 0 1)) then
			(
				append RsWaterErrors (obj.name + " is rotated which is not supported at the moment.")
				append RsWaterErrorObjs obj
				failed = true
			)
			if ExportWaterTestVert vert1 == false and failed == false then (
				append RsWaterErrors ("vert1 of " + obj.name + " is not on 2m boundary in xy")
				append RsWaterErrorObjs obj
				failed = true
			)
			if ExportWaterTestVert vert2 == false and failed == false then (
				append RsWaterErrors ("vert2 of " + obj.name + " is not on 2m boundary in xy")
				append RsWaterErrorObjs obj
				failed = true
			)
			if ExportWaterTestVert vert3 == false and failed == false then (
				append RsWaterErrors ("vert3 of " + obj.name + " is not on 2m boundary in xy")
				append RsWaterErrorObjs obj
				failed = true
			)
			if obj.type == false then (
				if ExportWaterTestVert vert4 == false and failed == false then (
					append RsWaterErrors ("vert4 of " + obj.name + " is not on 2m boundary in xy")
					append RsWaterErrorObjs obj
					failed = true
				)
			)
		)
	)
	
	if RsWaterErrorObjs.count == 0 then (
		return true
	)
	
	try CloseRolloutFloater RsMapWaterErrorsUtil catch()
	
	RsMapWaterErrorsUtil = newRolloutFloater "Water Errors" 450 260 1 136
	addRollout RsWaterErrorsRoll RsMapWaterErrorsUtil
	RsWaterErrorsRoll.lstErrors.items = RsWaterErrors	
	
	return false
)


--
-- name: ExportWaterObjFile
-- desc:
--
fn ExportWaterObjFile filename = (

	if ExportWaterTest() == false then (
		return 0
	)

	if filename == undefined do return false

	file = createfile filename

	if file == undefined do (
		messagebox "Cannot open file" title:"Water Object File Export"
		return false
	)	

	for obj in rootnode.children do (
		
		if classof obj == Gta_Waterhelper then (
		
			width = obj.width / 2.0
			depth = obj.depth / 2.0
			
			vert1 = [-width,depth,obj.pointheight1]
			vert2 = [width,depth,obj.pointheight2]
			vert3 = [-width,-depth,obj.pointheight3]
			vert4 = [width,-depth,obj.pointheight4]
			
			vert1 = vert1 * obj.objecttransform
			vert2 = vert2 * obj.objecttransform
			vert3 = vert3 * obj.objecttransform
			vert4 = vert4 * obj.objecttransform
			
			isVisible = getattr obj (getattrindex "Gta Water" "Visible")
			isLimitedDepth = getattr obj (getattrindex "Gta Water" "Limited Depth")
			isStandard = getattr obj (getattrindex "Gta Water" "Standard")
			isCalming = getattr obj (getattrindex "Gta Water" "Calming")
			valCalmness = getattr obj (getattrindex "Gta Water" "Calmness")
			vert1flowX = getattr obj (getattrindex "Gta Water" "Point1 Flow X")
			vert1flowY = getattr obj (getattrindex "Gta Water" "Point1 Flow Y")
			vert1strengthX = getattr obj (getattrindex "Gta Water" "Point1 Big Wave Strength")
			vert1strengthY = getattr obj (getattrindex "Gta Water" "Point1 Small Wave Strength")
			vert2flowX = getattr obj (getattrindex "Gta Water" "Point2 Flow X")
			vert2flowY = getattr obj (getattrindex "Gta Water" "Point2 Flow Y")
			vert2strengthX = getattr obj (getattrindex "Gta Water" "Point2 Big Wave Strength")
			vert2strengthY = getattr obj (getattrindex "Gta Water" "Point2 Small Wave Strength")
			vert3flowX = getattr obj (getattrindex "Gta Water" "Point3 Flow X")
			vert3flowY = getattr obj (getattrindex "Gta Water" "Point3 Flow Y")
			vert3strengthX = getattr obj (getattrindex "Gta Water" "Point3 Big Wave Strength")
			vert3strengthY = getattr obj (getattrindex "Gta Water" "Point3 Small Wave Strength")
			vert4flowX = getattr obj (getattrindex "Gta Water" "Point4 Flow X")
			vert4flowY = getattr obj (getattrindex "Gta Water" "Point4 Flow Y")
			vert4strengthX = getattr obj (getattrindex "Gta Water" "Point4 Big Wave Strength")
			vert4strengthY = getattr obj (getattrindex "Gta Water" "Point4 Small Wave Strength")
			
			outputFlags = 0
			
			if isVisible == true then (
				outputFlags = outputFlags + 1
			)
			
			if isLimitedDepth == true then (
				outputFlags = outputFlags + 2
			)
			
			if isStandard == true then (
				outputFlags = outputFlags + 4
			)

			if isCalming == true then (
				outputFlags = outputFlags + 8
			)
			
			if obj.type == true then (
			
				format "% % % % % % %   " vert1[1] vert1[2] vert1[3] vert1flowX vert1flowY vert1strengthX vert1strengthY to:file
				format "% % % % % % %   " vert2[1] vert2[2] vert2[3] vert2flowX vert2flowY vert2strengthX vert2strengthY to:file
				format "% % % % % % % %\n" vert3[1] vert3[2] vert3[3] vert3flowX vert3flowY vert3strengthX vert3strengthY outputFlags to:file
			
			) else (		
				format "% % % % % % %   " vert1[1] vert1[2] vert1[3] vert1flowX vert1flowY vert1strengthX vert1strengthY to:file
				format "% % % % % % %   " vert2[1] vert2[2] vert2[3] vert2flowX vert2flowY vert2strengthX vert2strengthY to:file
				format "% % % % % % %   " vert3[1] vert3[2] vert3[3] vert3flowX vert3flowY vert3strengthX vert3strengthY to:file
				format "% % % % % % % % %\n" vert4[1] vert4[2] vert4[3] vert4flowX vert4flowY vert4strengthX vert4strengthY outputFlags valCalmness to:file
			)
		)
	)
	
	close file
)


--
-- name: ImportWaterObjFile
-- desc:
--
fn ImportWaterObjFile filename = (
	file = openfile filename mode:"r"
	
	if file == undefined then (
		return 0
	)
	
	linecount = 0
	
	while eof file == false do (
	
		linecount = linecount + 1
	
		fileLine = readline file
		
		if fileLine != "processed" then (
		
			fileLineTokens = filterstring fileLine " "
		
			vertA = [(fileLineTokens[1] as integer),(fileLineTokens[2] as integer),(fileLineTokens[3] as float)] 
			vertB = [(fileLineTokens[8] as integer),(fileLineTokens[9] as integer),(fileLineTokens[10] as float)] 
			vertC = [(fileLineTokens[15] as integer),(fileLineTokens[16] as integer),(fileLineTokens[17] as float)]
		
			newObj = Gta_Waterhelper()
			newObj.type = true
			
			if vertA[1] == vertC[1] and vertA[2] == vertB[2] then (
				waterPos = (vertB + vertC) / 2
				waterDim = vertC - vertB
				
				if (vertB[1] < vertC[1] or vertB[2] < vertC[2]) and fileLineTokens.count != 28 then (
					newObj.rotation = (quat 0 0 -1 0)
				)
			) else (
				if vertB[1] == vertC[1] and vertA[2] == vertB[2] then (
					waterPos = (vertA + vertC) / 2
					waterDim = vertC - vertA

					if (vertA[1] < vertC[1] or vertA[2] < vertC[2]) and fileLineTokens.count != 28 then (
						newObj.rotation = (quat 0 0 -0.707107 0.707107)
					) else (
						newObj.rotation = (quat 0 0 0.707107 0.707107)
					)
				)
			)
			
			waterDim = [abs(waterDim[1]),abs(waterDim[2]),abs(waterDim[3])]
					
			if waterDim[1] == 0 or waterDim[2] == 0 then (
				waterPos = (vertA + vertC) / 2
				waterDim = vertC - vertA
				waterDim = [abs(waterDim[1]),abs(waterDim[2]),abs(waterDim[3])]
			)
			
			newObj.width = waterDim[1]
			newObj.depth = waterDim[2]
			newObj.pos = waterPos
			newObj.pointheight1 = vertA[3] - waterPos[3]
			newObj.pointheight2 = vertB[3] - waterPos[3]
			newObj.pointheight3 = vertC[3] - waterPos[3]
			newObj.pointheight4 = 0.0
	
			vert1flowX = fileLineTokens[4] as float
			vert1flowY = fileLineTokens[5] as float
			vert1strengthX = fileLineTokens[6] as float
			vert1strengthY = fileLineTokens[7] as float
			vert2flowX = fileLineTokens[11] as float
			vert2flowY = fileLineTokens[12] as float
			vert2strengthX = fileLineTokens[13] as float
			vert2strengthY = fileLineTokens[14] as float
			vert3flowX = fileLineTokens[18] as float
			vert3flowY = fileLineTokens[19] as float
			vert3strengthX = fileLineTokens[20] as float
			vert3strengthY = fileLineTokens[21] as float
	
			if fileLineTokens.count > 27 then (
			
				vertD = [(fileLineTokens[22] as integer),(fileLineTokens[23] as integer),(fileLineTokens[24] as float)]
			
				vert4flowX = fileLineTokens[25] as float
				vert4flowY = fileLineTokens[26] as float
				vert4strengthX = fileLineTokens[27] as float
				vert4strengthY = fileLineTokens[28] as float
				newObj.type = false
				newObj.pointheight4 = vertD[3] - waterPos[3]
				
				if (newObj.pointheight1 == newObj.pointheight2) and (newObj.pointheight1 == newObj.pointheight3) and (newObj.pointheight4 != newObj.pointheight1) then (
				
					newObj.pointheight4 = newObj.pointheight1
				)
				
				if newObj.pointheight4 != newObj.pointheight1 then (
				
					print "entry"
					print linecount
				
					print newObj.pointheight1
					print newObj.pointheight2
					print newObj.pointheight3
					print newObj.pointheight4	
				)
				
				if fileLineTokens.count > 28 then (
				
					flagVals = fileLineTokens[29] as integer
					
					if (bit.and flagVals 1) != 0 then (
						setattr newObj (getattrindex "Gta Water" "Visible") true
					) else (
						setattr newObj (getattrindex "Gta Water" "Visible") false
					)
					
					if (bit.and flagVals 2) != 0 then (
						setattr newObj (getattrindex "Gta Water" "Limited Depth") true
					) else (
						setattr newObj (getattrindex "Gta Water" "Limited Depth") false
					)
				
					if (bit.and flagVals 4) != 0 then (
						setattr newObj (getattrindex "Gta Water" "Standard") true
					) else (
						setattr newObj (getattrindex "Gta Water" "Standard") false
					)
					
					if (bit.and flagVals 8) != 0 then (
										
						setattr newObj (getattrindex "Gta Water" "Calming") true
					) else (
						setattr newObj (getattrindex "Gta Water" "Calming") false
					)					
				
					if fileLineTokens.count > 29 then (

						flagVals = fileLineTokens[30] as float

						setattr newObj (getattrindex "Gta Water" "Calmness") flagVals
					)		
				) else (
				
					setattr newObj (getattrindex "Gta Water" "Limited Depth" ) false
				
					if vert1strengthY == -1 and vert2strengthY == -1 and vert3strengthY == -1 then (
						setattr newObj (getattrindex "Gta Water" "Visible") false
						vert4strengthY = -1
					) else (
						setattr newObj (getattrindex "Gta Water" "Visible") true
					)				
				)
				
			) else (
				vert4flowX = vert3flowX
				vert4flowY = vert3flowY
				vert4strengthX = vert3strengthX
				vert4strengthY = vert3strengthY		
				newObj.type = true
				newObj.pointheight4 = newObj.pointheight3
			
				if fileLineTokens.count == 22 then (
				
					if (bit.and flagVals 1) != 0 then (
						setattr newObj (getattrindex "Gta Water" "Visible") true
					) else (
						setattr newObj (getattrindex "Gta Water" "Visible") false
					)
					
					if (bit.and flagVals 2) != 0 then (
						setattr newObj (getattrindex "Gta Water" "Limited Depth") true
					) else (
						setattr newObj (getattrindex "Gta Water" "Limited Depth") false
					)
				
				) else (
					
					setattr newObj (getattrindex "Gta Water" "Limited Depth") false
				
					if vert1strengthY == -1 and vert2strengthY == -1 and vert3strengthY == -1 then (
						setattr newObj (getattrindex "Gta Water" "Visible") false
						vert4strengthY = -1
					) else (
						setattr newObj (getattrindex "Gta Water" "Visible") true
					)				
				)
			)			

			setattr newObj (getattrindex "Gta Water" "Point1 Flow X") vert1flowX
			setattr newObj (getattrindex "Gta Water" "Point1 Flow Y") vert1flowY
			setattr newObj (getattrindex "Gta Water" "Point1 Big Wave Strength") vert1strengthX
			setattr newObj (getattrindex "Gta Water" "Point1 Small Wave Strength") vert1strengthY
			setattr newObj (getattrindex "Gta Water" "Point2 Flow X") vert2flowX
			setattr newObj (getattrindex "Gta Water" "Point2 Flow Y") vert2flowY
			setattr newObj (getattrindex "Gta Water" "Point2 Big Wave Strength") vert2strengthX
			setattr newObj (getattrindex "Gta Water" "Point2 Small Wave Strength") vert2strengthY
			setattr newObj (getattrindex "Gta Water" "Point3 Flow X") vert3flowX
			setattr newObj (getattrindex "Gta Water" "Point3 Flow Y") vert3flowY
			setattr newObj (getattrindex "Gta Water" "Point3 Big Wave Strength") vert3strengthX
			setattr newObj (getattrindex "Gta Water" "Point3 Small Wave Strength") vert3strengthY
			setattr newObj (getattrindex "Gta Water" "Point4 Flow X") vert4flowX
			setattr newObj (getattrindex "Gta Water" "Point4 Flow Y") vert4flowY
			setattr newObj (getattrindex "Gta Water" "Point4 Big Wave Strength") vert4strengthX
			setattr newObj (getattrindex "Gta Water" "Point4 Small Wave Strength") vert4strengthY
		)
	)
	
	close file
) 

-- pipeline/export/water.ms
