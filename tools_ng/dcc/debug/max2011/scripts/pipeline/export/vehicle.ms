-- 
-- File:: pipeline/export/vehicle.ms
-- Description:: Main Vehicle Export Functions
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 6 October 2008
--
-- Refactored the original from 16/5/2005 by Greg Smith.
--
-- Modded:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- date::   26/5/2010
-- Use spec files to define degrees of freedom for the skel file

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/vehicles/globals.ms"
filein "pipeline/export/vehicles/ruby_script_generator.ms"
filein "pipeline/export/vehicles/textures.ms"
filein "pipeline/export/vehicles/utils.ms"
filein "pipeline/export/vehicles/validation.ms"
filein "pipeline/util/batch_export_list.ms"

filein "rockstar/util/attribute.ms"
filein "pipeline/util/file.ms"
filein "rockstar/util/material.ms"
filein "rockstar/util/rexreport.ms"
filein "pipeline/util/string.ms"
filein "rockstar/util/anim.ms"

-----------------------------------------------------------------------------
-- Export Functions
-----------------------------------------------------------------------------

--
-- name: RsVehicleCreateSkinnedNonCustom
-- desc: Create skinned vehicle with no custom skinned component (old method).
--       This has been kept for speed, rather than always using the Skin Wrap
--       modifier.
--
fn RsVehicleCreateSkinnedNonCustom srcobj = (
	format "Creating vehicle skin for %\n" srcobj.name

	local exportset = #()
	local srcobjlod1 = getnodebyname (srcobj.name + "_l1")
	local srcobjlod2 = getnodebyname (srcobj.name + "_l2") 
	local srcobjhi = undefined
			
	if (substring srcobj.name (srcobj.name.count - 2) 3) == "_lo" then (

		srcobjhi = getnodebyname ((substring srcobj.name 1 (srcobj.name.count - 3)) + "_hi")
	)

	local rotorNode1 = getnodebyname ("static_rotor1") exact:true ignorecase:true
	local rotorNode2 = getnodebyname ("static_rotor2") exact:true ignorecase:true

	local wheelNode = getnodebyname ("wheelmesh_lf") exact:true ignorecase:true
	local wheelNodeL1 = getnodebyname ("wheelmesh_lf_l1") exact:true ignorecase:true

	local wheelNodeR = getnodebyname ("wheelmesh_lr") exact:true ignorecase:true
	local wheelNodeRL1 = getnodebyname ("wheelmesh_lr_l1") exact:true ignorecase:true

	local wheelNodeBF = getnodebyname ("wheelmeshbk_f") exact:true ignorecase:true
	local wheelNodeBFL1 = getnodebyname ("wheelmeshbk_f_l1") exact:true ignorecase:true
	local wheelNodeBR = getnodebyname ("wheelmeshbk_r") exact:true ignorecase:true
	local wheelNodeBRL1 = getnodebyname ("wheelmeshbk_r_l1") exact:true ignorecase:true

	local savetranslod1 = undefined
	local savetranslod2 = undefined
	local savetranshi = undefined
	local namelod1 = undefined
	local namelod2 = undefined
	local namehi = undefined

	if srcobjlod1 != undefined then (

		savetranslod1 = srcobjlod1.transform
		srcobjlod1.transform = srcobj.transform
		namelod1 = srcobjlod1.name + "_skin"
	)

	if srcobjlod2 != undefined then (

		savetranslod2 = srcobjlod2.transform
		srcobjlod2.transform = srcobj.transform
		namelod2 = srcobjlod2.name + "_skin"
	)

	if srcobjhi != undefined then (

		savetranshi = srcobjhi.transform
		srcobjhi.transform = srcobj.transform
		namehi = srcobjhi.name + "_skin"
	)	

	local tempName = srcobj.name + "_skin"
	local tempSkelName = srcobj.name + "_skel"

	local tempObj = getnodebyname tempName exact:true
	if tempObj != undefined then RsVehicleUtil.DeleteRec tempObj

	tempObj = getnodebyname tempSkelName exact:true
	if tempObj != undefined then RsVehicleUtil.DeleteRec tempObj

	newobj = Editable_Mesh()
	newobj.name = tempName
	newobj.transform = srcobj.transform

	skelobj = Dummy()
	skelobj.name = tempSkelName
	skelobj.boxsize = [.2,.2,.2]
	skelobj.transform = srcobj.transform

	local boneList = #()
	local vertexBone = #()
	local copymat = RsVehicleUtil.AttachMeshesRec newobj skelobj srcobj undefined boneList vertexBone

	update newobj

	newobj.material = copy copymat

	select newobj

	setCommandPanelTaskMode #modify
	newSkin = Skin()
	newSkin.mirrorEnabled = off

	addmodifier newobj newSkin

	for obj in boneList do (

		skinops.addbone newSkin obj 1
	)

	if newobj.numverts == vertexBone.count then (

		for i = 1 to vertexBone.count do (

			skinops.replacevertexweights newSkin i vertexBone[i] 1.0
		)
	) else (

		messagebox "Vehicle Exporter: error in skinning"
	)	

	RsVehicleUtil.AddExisting srcobjlod1 savetranslod1 namelod1 boneList
	RsVehicleUtil.AddExisting srcobjlod2 savetranslod2 namelod2 boneList
	RsVehicleUtil.AddExisting srcobjhi savetranshi namehi boneList		

	RsVehicleUtil.AttachWheelsToSkeleton skelobj wheelNode wheelNodeL1 wheelNodeR wheelNodeRL1
	RsVehicleUtil.AttachWheelsToSkeleton skelobj wheelNodeBF wheelNodeBFL1 wheelNodeBR wheelNodeBRL1
	RsVehicleUtil.AttachRotorToSkeleton skelobj rotorNode1
	RsVehicleUtil.AttachRotor2ToSkeleton skelobj rotorNode2
)

--
-- name: RsVehicleCreateSkinnedWithCustom
-- desc: Create skinned vehicle with custom skinned component(s) (new method).
--       This is slower than the method above because we create a collapsed
--       duplicate and then use the Skin Wrap modifier to copy the skinning
--       information across (to include the custom skinned objects).
--
fn RsVehicleCreateSkinnedWithCustom srcobj customobjs = (
	format "Creating vehicle skin for % with custom Skinned objects:\n" srcobj.name
	for o in customobjs do ( format "\t%\n" o.name )

	local exportset = #()
	local srcobjlod1 = getnodebyname (srcobj.name + "_l1")
	local srcobjlod2 = getnodebyname (srcobj.name + "_l2") 
	local srcobjhi = undefined
			
	if (substring srcobj.name (srcobj.name.count - 2) 3) == "_lo" then (

		srcobjhi = getnodebyname ((substring srcobj.name 1 (srcobj.name.count - 3)) + "_hi")
	)

	local rotorNode1 = getnodebyname ("static_rotor1") exact:true ignorecase:true
	local rotorNode2 = getnodebyname ("static_rotor2") exact:true ignorecase:true

	local wheelNode = getnodebyname ("wheelmesh_lf") exact:true ignorecase:true
	local wheelNodeL1 = getnodebyname ("wheelmesh_lf_l1") exact:true ignorecase:true

	local wheelNodeR = getnodebyname ("wheelmesh_lr") exact:true ignorecase:true
	local wheelNodeRL1 = getnodebyname ("wheelmesh_lr_l1") exact:true ignorecase:true

	local wheelNodeBF = getnodebyname ("wheelmeshbk_f") exact:true ignorecase:true
	local wheelNodeBFL1 = getnodebyname ("wheelmeshbk_f_l1") exact:true ignorecase:true
	local wheelNodeBR = getnodebyname ("wheelmeshbk_r") exact:true ignorecase:true
	local wheelNodeBRL1 = getnodebyname ("wheelmeshbk_r_l1") exact:true ignorecase:true

	local savetranslod1 = undefined
	local savetranslod2 = undefined
	local savetranshi = undefined
	local namelod1 = undefined
	local namelod2 = undefined
	local namehi = undefined

	if srcobjlod1 != undefined then (

		savetranslod1 = srcobjlod1.transform
		srcobjlod1.transform = srcobj.transform
		namelod1 = srcobjlod1.name + "_skin"
	)

	if srcobjlod2 != undefined then (

		savetranslod2 = srcobjlod2.transform
		srcobjlod2.transform = srcobj.transform
		namelod2 = srcobjlod2.name + "_skin"
	)

	if srcobjhi != undefined then (

		savetranshi = srcobjhi.transform
		srcobjhi.transform = srcobj.transform
		namehi = srcobjhi.name + "_skin"
	)	

	local tempName = srcobj.name + "_skin"
	local tempSkelName = srcobj.name + "_skel"

	local tempObj = getnodebyname tempName exact:true
	if tempObj != undefined then RsVehicleUtil.DeleteRec tempObj

	tempObj = getnodebyname tempSkelName exact:true
	if tempObj != undefined then RsVehicleUtil.DeleteRec tempObj

	newobj = Editable_Mesh()
	newobj.name = tempName
	newobj.transform = srcobj.transform

	skelobj = Dummy()
	skelobj.name = tempSkelName
	skelobj.boxsize = [.2,.2,.2]
	skelobj.transform = srcobj.transform

	-- Prior to collapsing all the meshes together, detach the custom skinned 
	-- objects as these cannot be collapsed just yet.
	tempCustomRoot = Dummy()
	tempCustomRoot.name = "VehicleExporterCustomSkinnedGroup"
	for o in customobjs do
		o.parent = tempCustomRoot

	local boneList = #()
	local vertexBone = #()
	local copymat = RsVehicleUtil.AttachMeshesRec newobj skelobj srcobj undefined boneList vertexBone

	update newobj

	newobj.material = copy copymat

	select newobj

	setCommandPanelTaskMode #modify
	newSkin = Skin()
	newSkin.mirrorEnabled = off

	addmodifier newobj newSkin

	for obj in boneList do (

		skinops.addbone newSkin obj 1
	)

	if newobj.numverts == vertexBone.count then (

		for i = 1 to vertexBone.count do (

			skinops.replacevertexweights newSkin i vertexBone[i] 1.0
		)
	) else (

		messagebox "Vehicle Exporter: error in skinning"
	)	

	-- After the skinning we re-parent the custom Skinned geometry back onto
	-- the newly skinned vehicle skin.  Delete the temp dummy too.
	for o in customobjs do
		o.parent = newobj
	delete tempCustomRoot

	RsVehicleUtil.ProcessCustomSkinned newobj customobjs boneList
	RsVehicleUtil.CreateSkinnedWithCustomPostProcess newobj srcobj customobjs boneList

	RsVehicleUtil.AddExisting srcobjlod1 savetranslod1 namelod1 boneList
	RsVehicleUtil.AddExisting srcobjlod2 savetranslod2 namelod2 boneList
	RsVehicleUtil.AddExisting srcobjhi savetranshi namehi boneList		

	RsVehicleUtil.AttachWheelsToSkeleton skelobj wheelNode wheelNodeL1 wheelNodeR wheelNodeRL1
	RsVehicleUtil.AttachWheelsToSkeleton skelobj wheelNodeBF wheelNodeBFL1 wheelNodeBR wheelNodeBRL1
	RsVehicleUtil.AttachRotorToSkeleton skelobj rotorNode1
	RsVehicleUtil.AttachRotor2ToSkeleton skelobj rotorNode2
)

--
-- name: RsVehicleCreateSkinned
-- desc: Create skinned vehicle.  This is now a wrapper around the 
--       SkinnedNonCustom and SkinnedWithCustom functions above.
--
fn RsVehicleCreateSkinned srcobj = (
	
	print RsVehicleUtil
	
	local custom_skinned = ( RsVehicleUtil.GetCustomSkinnedObjects srcobj )
	if ( custom_skinned.count > 0 ) then
	(
		-- New method that handles custom skinned geometry.
		RsVehicleCreateSkinnedWithCustom srcobj custom_skinned
	)
	else
	(
		-- Old (but faster) method.
		RsVehicleCreateSkinnedNonCustom srcobj
	)
)


--
-- name: RsVehicleReplaceWheel
-- desc:
--
fn RsVehicleReplaceWheel targetPath wheelNode wheelID = (

	wheelFilename = targetPath + RsVehicleName + (wheelID as string) + ".bnd"
	RsMakeSurePathExists wheelFilename

	wheelFile = openfile wheelFilename mode:"wt"

	if wheelFile == undefined then return 0		

	marginVal = (wheelNode.grid.width / 2.0f)
	heightVal = (wheelNode.grid.length / 2.0f) - marginVal

	format "version: 1.10\n" to:wheelFile
	format "type: geometry_curved\n" to:wheelFile
	format "\n" to:wheelFile
	format "curved_edges: 2\n" to:wheelFile
	format "curved_faces: 0\n" to:wheelFile
	format "\n" to:wheelFile
	format "verts: 2\n" to:wheelFile
	format "\n" to:wheelFile
	format "margin: %\n" marginVal to:wheelFile
	format "materials: 2\n" to:wheelFile
	format "polys: 0\n" to:wheelFile
	format "\n" to:wheelFile
	format "\n" to:wheelFile
	format "\n" to:wheelFile
	format "v 0.0 0.0 -%\n" heightVal to:wheelFile
	format "v 0.0 0.0  %\n" heightVal to:wheelFile
	format "\n" to:wheelFile
	format "type: BASE\n" to:wheelFile
	format "mtl rubber {\n" to:wheelFile
	format "	elasticity: 0.100000 \n" to:wheelFile
	format "	friction: 0.500000 \n" to:wheelFile
	format "}\n" to:wheelFile
	format "\n" to:wheelFile
	format "type: BASE\n" to:wheelFile
	format "mtl rubber {\n" to:wheelFile
	format "	elasticity: 0.100000 \n" to:wheelFile
	format "	friction: 0.500000 \n" to:wheelFile
	format "}\n" to:wheelFile
	format "\n" to:wheelFile
	format "curved_edge	0 1 % 1.0 0.0 0.0  1.0 0.0 0.0\n" heightVal to:wheelFile
	format "curved_edge 1 0 % 1.0 0.0 0.0 -1.0 0.0 0.0	\n" heightVal to:wheelFile	

	close wheelFile
)





-- 
-- name: RsVehicleExportModelSkinned
-- desc: export the vehicles model
--
fn RsVehicleExportModelSkinned = (

	cursel = #()
	cursel = cursel + selection

	meshNode = cursel[1]	
	meshLodNode1 = getnodebyname (meshNode.name + "_l1_skin") exact:true ignorecase:true
	meshLodNode2 = getnodebyname (meshNode.name + "_l2_skin") exact:true ignorecase:true

	rotorNode1 = getnodebyname ("static_rotor1") exact:true ignorecase:true
	rotorNode2 = getnodebyname ("static_rotor2") exact:true ignorecase:true
	moveRotorNode1 = getnodebyname ("moving_rotor1") exact:true ignorecase:true
	moveRotorNode2 = getnodebyname ("moving_rotor2") exact:true ignorecase:true

	wheelNode = getnodebyname ("wheelmesh_lf") exact:true ignorecase:true
	wheelNodeL1 = getnodebyname ("wheelmesh_lf_l1") exact:true ignorecase:true
	wheelNodeR = getnodebyname ("wheelmesh_lr") exact:true ignorecase:true
	wheelNodeRL1 = getnodebyname ("wheelmesh_lr_l1") exact:true ignorecase:true		

	wheelNodeBF = getnodebyname ("wheelmeshbk_f") exact:true ignorecase:true
	wheelNodeBFL1 = getnodebyname ("wheelmeshbk_f_l1") exact:true ignorecase:true
	wheelNodeBR = getnodebyname ("wheelmeshbk_r") exact:true ignorecase:true
	wheelNodeBRL1 = getnodebyname ("wheelmeshbk_r_l1") exact:true ignorecase:true				

	trackNodeL = getnodebyname ( "track_l" ) exact:true ignorecase:true
	trackNodeR = getnodebyname ( "track_r" ) exact:true ignorecase:true

	tireNode = getnodebyname ("tire") exact:true ignorecase:true
	
	wheelBoneL = getnodebyname ("wheel_lf") exact:true ignorecase:true
	wheelBoneLR = getnodebyname ("wheel_lr") exact:true ignorecase:true
	wheelBoneR = getnodebyname ("wheel_rf") exact:true ignorecase:true
	wheelBoneRR = getnodebyname ("wheel_rr") exact:true ignorecase:true
	chassisBone = getnodebyname ("chassis") exact:true ignorecase:true


	RsTruckVal = getuserprop meshNode "istruck"

	if RsTruckVal == undefined then RsTruckVal = false

	if wheelNode != undefined then setuserprop wheelNode "fragonly" "true"
	if wheelNodeL1 != undefined then setuserprop wheelNodeL1 "fragonly" "true"
	if wheelNodeR != undefined then setuserprop wheelNodeR "fragonly" "true"
	if wheelNodeRL1 != undefined then setuserprop wheelNodeRL1 "fragonly" "true"
	if tireNode != undefined then setuserprop tireNode "fragonly" "true"
	if rotorNode1 != undefined then setuserprop rotorNode1 "fragonly" "true"
	if rotorNode2 != undefined then setuserprop rotorNode2 "fragonly" "true"
	if moveRotorNode1 != undefined then setuserprop moveRotorNode1 "fragonly" "true"
	if moveRotorNode2 != undefined then setuserprop moveRotorNode2 "fragonly" "true"
	if wheelNodeBF != undefined then setuserprop wheelNodeBF "fragonly" "true"
	if wheelNodeBFL1 != undefined then setuserprop wheelNodeBFL1 "fragonly" "true"
	if wheelNodeBR != undefined then setuserprop wheelNodeBR "fragonly" "true"
	if wheelNodeBRL1 != undefined then setuserprop wheelNodeBRL1 "fragonly" "true"
	
	if wheelBoneL != undefined then setuserprop wheelBoneL "exportTrans" "true"
	if wheelBoneLR != undefined then setuserprop wheelBoneLR "exportTrans" "true"
	if wheelBoneR != undefined then setuserprop wheelBoneR "exportTrans" "true"
	if wheelBoneRR != undefined then setuserprop wheelBoneRR "exportTrans" "true"
	if chassisBone != undefined then setuserprop chassisBone "exportTrans" "true"

	if meshLodNode1 != undefined then (

		setuserprop meshLodNode1 "lod" "1"
	)

	if wheelNodeL1 != undefined then (

		setuserprop wheelNodeL1 "lod" "1"
	)

	if wheelNodeRL1 != undefined then (

		setuserprop wheelNodeRL1 "lod" "1"
	)

	if wheelNodeBFL1 != undefined then (

		setuserprop wheelNodeBFL1 "lod" "1"
	)

	if wheelNodeBRL1 != undefined then (

		setuserprop wheelNodeBRL1 "lod" "1"
	)

	if meshLodNode2 != undefined then (

		setuserprop meshLodNode2 "lod" "2"
	)

	select meshNode

	RsVehicleName = cursel[1].name

	boneroot = rexGetSkinRootBone cursel[1]

	vehicleFile = RsVehicleSettings.GetStreamDir() + "/" + cursel[1].name
	vehicleDir = vehicleFile + "/"

	RsDeleteDirectory vehicleFile
	RsDeleteFiles (vehicleFile + ".*")
	
	-- Start using a spec file so we can export translation on arbitrary bones more easily
	
	select boneroot
	specfilename = sysInfo.tempdir + "spec.xml"
	RsCreateSpecFile boneroot specfilename formap:true

	select meshnode
	rexReset()
	graphRoot = rexGetRoot()

	entity = rexAddChild graphRoot "entity" "Entity"
	rexSetPropertyValue entity "OutputPath" vehicleDir
	rexSetPropertyValue entity "SkipBoundSectionOfTypeFile" "true"
	rexSetPropertyValue entity "EntityWriteBoundSituations" "true"
	rexSetPropertyValue entity "EntityFragmentWriteMeshes" "true"
	rexSetPropertyValue entity "FragmentForceLoadCommonDrawable" "1"

	wheelListObjs = #()
	wheelListIds = #()

	RsVehicleUtil.SetupFragBounds boneroot entity vehicleDir wheelobjs:wheelListObjs wheelids:wheelListIds

	select meshNode

	skel = rexAddChild entity "skeleton" "Skeleton"
	rexSetPropertyValue skel "OutputPath" vehicleDir
	rexSetPropertyValue skel "SkeletonRootAtOrigin" "true"
	rexSetPropertyValue skel "SkeletonUseBoneIDs" "false"
	rexSetPropertyValue skel "ExportCtrlFile" specfilename
	--rexSetPropertyValue skel "ChannelsToExport" "(transX*;transY*;transZ*;)(rotX;rotY;rotZ;)"
	rexSetPropertyValue skel "SkeletonWriteLimitAndLockInfo" "true"
	rexSetNodes skel

	toSelect = #()
	append toSelect meshNode

	if meshLodNode1 != undefined then append toSelect meshLodNode1
	if meshLodNode2 != undefined then append toSelect meshLodNode2
	if wheelNode != undefined then append toSelect wheelNode
	if rotorNode1 != undefined then append toSelect rotorNode1
	if rotorNode2 != undefined then append toSelect rotorNode2

	if wheelNodeL1 != undefined then append toSelect wheelNodeL1 		
	if wheelNodeR != undefined then append toSelect wheelNodeR
	if wheelNodeRL1 != undefined then append toSelect wheelNodeRL1 
	if tireNode != undefined then append toSelect tireNode

	if wheelNodeBF != undefined then append toSelect wheelNodeBF
	if wheelNodeBFL1 != undefined then append toSelect wheelNodeBFL1
	if wheelNodeBR != undefined then append toSelect wheelNodeBR
	if wheelNodeBRL1 != undefined then append toSelect wheelNodeBRL1
	
	if trackNodeL != undefined then append toSelect trackNodeL
	if trackNodeR != undefined then append toSelect trackNodeR

	select toSelect

	mesh = rexAddChild entity "mesh" "Mesh"
	rexSetPropertyValue mesh "OutputPath" vehicleDir
	rexSetPropertyValue mesh "ExportTextures" "false"
	rexSetPropertyValue mesh "TextureOutputPath" vehicleDir
	rexSetPropertyValue mesh "MeshForSkeleton" "true"		
	if ( RsVehicleAsAscii ) then
		rexSetPropertyValue mesh "MeshAsAscii" "true"
	else
		rexSetPropertyValue mesh "MeshAsAscii" "false"
	rexSetNodes mesh

	select meshNode

	fragment = rexAddChild entity "fragment" "Fragment"
	rexSetPropertyValue fragment "OutputPath" vehicleDir
	rexSetNodes fragment

	boundobjs = #()
	RsCollCount = 0

	-- delete old bounds
	if RsDeleteFiles (vehicleDir + "/*.bnd") == false then (

		messagebox "couldnt delete bounds files for vehicle export"
		return false
	)

	RsRexExportWithReport()	

	deletefile specfilename

	for i = 1 to wheelListObjs.count do (

		RsVehicleReplaceWheel vehicleDir wheelListObjs[i] wheelListIds[i]
	)		

	select cursel

	texmaplist = #()
	maxsizelist = #()
	isbumplist = #()
	newbumplist = #()
	RsGetTexMapsFromObjNoStrip meshNode texmaplist maxsizelist isbumplist 
	if wheelNode != undefined then RsGetTexMapsFromObjNoStrip wheelNode texmaplist maxsizelist isbumplist 
	if wheelNodeL1 != undefined then RsGetTexMapsFromObjNoStrip wheelNodeL1 texmaplist maxsizelist isbumplist 
	if wheelNodeR != undefined then RsGetTexMapsFromObjNoStrip wheelNodeR texmaplist maxsizelist isbumplist 
	if wheelNodeRL1 != undefined then RsGetTexMapsFromObjNoStrip wheelNodeRL1 texmaplist maxsizelist isbumplist 
	if tireNode != undefined then RsGetTexMapsFromObjNoStrip tireNode texmaplist maxsizelist isbumplist 
	if rotorNode1 != undefined then RsGetTexMapsFromObjNoStrip rotorNode1 texmaplist maxsizelist isbumplist 
	if rotorNode2 != undefined then RsGetTexMapsFromObjNoStrip rotorNode2 texmaplist maxsizelist isbumplist 
	if wheelNodeBF != undefined then RsGetTexMapsFromObjNoStrip wheelNodeBF texmaplist maxsizelist isbumplist 
	if wheelNodeBFL1 != undefined then RsGetTexMapsFromObjNoStrip wheelNodeBFL1 texmaplist maxsizelist isbumplist 
	if wheelNodeBR != undefined then RsGetTexMapsFromObjNoStrip wheelNodeBR texmaplist maxsizelist isbumplist 
	if wheelNodeBRL1 != undefined then RsGetTexMapsFromObjNoStrip wheelNodeBRL1 texmaplist maxsizelist isbumplist 

	if RsTruckVal then (

		RsVehicleTruckTextureList.reload()
		texmaplist = RsVehicleTexture.RemoveSharedFromTexMapList texmaplist isbumplist newbumplist RsVehicleTruckTextureList
	) else (

		RsVehicleTextureList.reload()
		texmaplist = RsVehicleTexture.RemoveSharedFromTexMapList texmaplist isbumplist newbumplist RsVehicleTextureList
	)

	if (RsVehicleTexture.Export texmaplist newbumplist vehicleDir RsVehicleName) == false then return false
	RsVehicleTextureVariationList.reload()
	RsVehicleTextureVariationList.export vehicleDir
	RsVehicleUtil.CreateCustomRagebuilderScripts()

	true
)



--
-- name: RsVehicleResourceModelSkinned
-- desc: resourcify the model data
--
fn RsVehicleResourceModelSkinned isPatch = (

	texmaplist = #()
	maxsizelist = #()
	isbumplist = #()
	RsGetTexMapsFromObj selection[1] texmaplist maxsizelist isbumplist

	rotorNode1 = getnodebyname ("static_rotor1") exact:true ignorecase:true	
	rotorNode2 = getnodebyname ("static_rotor2") exact:true ignorecase:true
	moveRotorNode1 = getnodebyname ("moving_rotor1") exact:true ignorecase:true	
	moveRotorNode2 = getnodebyname ("moving_rotor2") exact:true ignorecase:true
	wheelNode = getnodebyname ("wheelmesh_lf") exact:true ignorecase:true
	wheelNodeL1 = getnodebyname ("wheelmesh_lf_l1") exact:true ignorecase:true
	wheelNodeR = getnodebyname ("wheelmesh_lr") exact:true ignorecase:true
	wheelNodeRL1 = getnodebyname ("wheelmesh_lr_l1") exact:true ignorecase:true		
	wheelNodeBF = getnodebyname ("wheelmeshbk_f") exact:true ignorecase:true
	wheelNodeBFL1 = getnodebyname ("wheelmeshbk_f_l1") exact:true ignorecase:true
	wheelNodeBR = getnodebyname ("wheelmeshbk_r") exact:true ignorecase:true
	wheelNodeBRL1 = getnodebyname ("wheelmeshbk_r_l1") exact:true ignorecase:true				
	tireNode = getnodebyname ("tire") exact:true ignorecase:true

	if wheelNode != undefined then RsGetTexMapsFromObj wheelNode texmaplist maxsizelist isbumplist 
	if wheelNodeL1 != undefined then RsGetTexMapsFromObj wheelNodeL1 texmaplist maxsizelist isbumplist 
	if wheelNodeR != undefined then RsGetTexMapsFromObj wheelNodeR texmaplist maxsizelist isbumplist 
	if wheelNodeRL1 != undefined then RsGetTexMapsFromObj wheelNodeRL1 texmaplist maxsizelist isbumplist 
	if tireNode != undefined then RsGetTexMapsFromObj tireNode texmaplist maxsizelist isbumplist 
	if rotorNode1 != undefined then RsGetTexMapsFromObj rotorNode1 texmaplist maxsizelist isbumplist 
	if rotorNode2 != undefined then RsGetTexMapsFromObj rotorNode2 texmaplist maxsizelist isbumplist 
	if moveRotorNode1 != undefined then RsGetTexMapsFromObj moveRotorNode1  texmaplist maxsizelist isbumplist 
	if moveRotorNode2 != undefined then RsGetTexMapsFromObj moveRotorNode2  texmaplist maxsizelist isbumplist 
	if wheelNodeBF != undefined then RsGetTexMapsFromObj wheelNodeBF texmaplist maxsizelist isbumplist 
	if wheelNodeBFL1 != undefined then RsGetTexMapsFromObj wheelNodeBFL1 texmaplist maxsizelist isbumplist 
	if wheelNodeBR != undefined then RsGetTexMapsFromObj wheelNodeBR texmaplist maxsizelist isbumplist 
	if wheelNodeBRL1 != undefined then RsGetTexMapsFromObj wheelNodeBRL1 texmaplist maxsizelist isbumplist 

	local vehicle = selection[1]
	local vehicleName = vehicle.name
	local rbFileName = RsVehicleSettings.GetScriptDir() + vehicleName + ".rb"
	RsMakeSurePathExists rbFileName

	rbFile = openfile rbFileName mode:"w+"
	RBGenVehicleResourcingScriptPre rbFile vehicle false
	RBGenVehicleResourcingScriptModel rbFile
	RBGenResourcingScriptPost rbFile
	if isPatch == true then (
		RBGenResourcingScriptPatch rbFile
	)


	close rbFile

	-- Run Ruby resource script for the model.
	if ( 0 != ( RsVehicleScriptGenerator.RunScript rbFileName ) ) then 
	(
		local ss = stringStream ""
		format "Failed to build skinned model fragment using script:\n%" rbFileName to:ss
		messageBox (ss as string) title:"Vehicle Exporter Error"
		return false
	)			

	return true
)


--
-- name: RsVehicleSkinnedBatchExport
-- desc: Batch export vehicles listed in $(content)/batch/vehicles_skinned.xml
--
fn RsVehicleSkinnedBatchExport = (

	batch_list = BatchExportList()
	batch_list.Load( RsConfigGetContentDir() + "/batch/vehicles_skinned.xml" )
	
	-- Loop through every batch export file.
	batch_files = batch_list.GetMaxFilenames()
	for maxfile in batch_files do (
	
		format "Batch exporting: %\n" maxfile
		
		--Suppress Max warnings before loading
		SetQuietMode true
		loadMaxFile maxfile

		if loadMaxFile maxfile == false then (

			batchWarning = maxfile + " failed to load"
			append RsVehicleBatchWarnings batchWarning
		)
		else (
			skinMeshname = (substring maxfilename 1 (maxfilename.count - 12)) + "_skin"
			format "\tSkin meshname: %\n" skinMeshname
			
			meshFound = false
			clearSelection()
			for obj in rootnode.children do (

				if obj.name == skinMeshname then (
					clearSelection()
					select obj
					meshFound = true
				)
			)

			if meshFound == true then RsVehicleTagBones()
			sel = selection[1]

			if sel ==  undefined or meshFound == false then (
				messagebox "Could not find a skinned root object"
				batchWarning = skinMeshname + " not found. File not exported."
				append RsVehicleBatchWarnings batchWarning
			)
			else (
				saveName = sel.name
				sel.name = substring sel.name 1 (sel.name.count - 5)

				retval = false

				if RsVehicleExportModelSkinned() == true then (
					if RsVehicleResourceModelSkinned() == true then (	
						retval = true
					)
				)
				sel.name = saveName
				retval
			)
		)
	)

	RsVehicleCheckBatchExport RsVehicleBatchWarnings
	RsVehicleBatchWarnings = #()
	SetQuietMode false
	return true
)

