--
-- validation.ms
-- Weapon Export Validation Functions
--
-- David Muir <david.muir@rockstarnorth.com>
-- 5 August 2008
--

filein "pipeline/export/weapons/globals.ms"
filein "rockstar/util/material.ms"

--
-- Name: IsValidBonesRecCheck
-- Desc:
--
fn RsWeaponExportIsValidBonesRecCheck obj = (

	if classof obj == Editable_Mesh or classof obj == Editable_Poly or classof obj == Dummy or classof obj == PolyMeshObject then (

		objTag = getuserprop obj "tag"

		if objTag != undefined then (

			objTag = RsLowercase(objTag)
			idxFound = finditem RsWeaponTagList objTag

			if idxFound == 0 then (

				append RsWeaponTagList objTag
				append RsWeaponObjectList obj
			) else (

				messagebox ("tag " + objTag + " is repeated in " + obj.name + " and " + RsWeaponObjectList[idxFound].name) title:"error"
				return false
			)
		)
	)

	for childobj in obj.children do (

		if RsWeaponExportIsValidBonesRecCheck childobj == false then return false
	)

	true
)

--
-- Name: IsValidForExport
-- Desc: Check that we have a valid weapon selection for export.
--
fn RsWeaponExportIsValidForExport = (

	RsWeaponObjectList = #()
	RsWeaponTagList = #()

	if selection.count != 1 then (

		messagebox "please select just one object"
		return false			
	)

	if selection[1].parent != undefined then (

		messagebox "has to be a root object"
		return false
	)

	if RsWeaponExportIsValidBonesRecCheck selection[1] == false then (

		return false
	)

	if (filterstring selection[1].name " ").count > 1 then (

		messagebox (selection[1].name + " has spaces in name, not exporting")
		return false
	)
	
	if RsWeaponAreTexturesValid selection[1] == false then (

		return false
	)

	return true	
)

--
-- Name: AreTexturesValid
-- Desc: Checks that textures are valid (dimensions are a power of 2)
--
fn RsWeaponAreTexturesValid obj = (
	global RsModTexErrors = #()
	
	texlist = #()
	sizelist = #()
	bumplist = #()
	
	RsGetTexMapsFromObjNoStrip obj texlist sizelist bumplist
	
	for tex in texlist do (
		-- Only check textures which exist on this computer
		if RsFileExist tex then (
			valid = true
			b = openBitmap tex
			
			-- Check the dimensions
			if ( ( bit.and b.width (b.width - 1) ) != 0 ) then valid = false
			if ( ( bit.and b.height (b.height - 1) ) != 0 ) then valid = false
			
			if not valid then (
				message = tex + " ( " + (b.width as string) + "x" + (b.height as string) + " )"
				append RsModTexErrors message
			)
		)
	)
	
	-- If we found errors, display them
	if (RsModTexErrors.count > 0) then (
		rollout RsTexWarningsRoll "Found textures with invalid dimensions!"
		(
			listbox lstBadTex items:RsModTexErrors
			button btnOK "OK" width:100 

			on btnOK pressed do (
				DestroyDialog RsTexWarningsRoll
			)	
		)
	
		CreateDialog RsTexWarningsRoll width:600 modal:true
		return false
	)
	else (
		return true
	)
)

-- validation.ms
