-- Rockstar Weapon Export
-- Rockstar North
-- 16/5/2005
-- by Greg Smith
-- Exports a weapon into the game


-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/models/ruby_script_generator.ms"
filein "pipeline/export/models/validation.ms"
filein "pipeline/util/drawablelod.ms"
filein "rockstar/util/material.ms"
filein "rockstar/util/attribute.ms"
filein "rockstar/util/rexreport.ms"
filein "rockstar/util/collutil.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
isFragment = false

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsWeaponExportExtraTextures 
-- desc:
--
fn RsWeaponExportExtraTextures weapDir = (

	for texfile in RsTextureList do (

		ddsfile = weapDir + (RsRemovePathAndExtension texfile) + ".dds"

		rexExportTexture texfile ddsfile
	)		
)


--
-- name: RsWeaponExportMeshMap
-- desc: Export a file that maps between mesh name and mesh index
-- note: Should only be called after a weapon export
--
fn RsWeaponExportMeshMap =
(
	local weaponNode = selection[1]
	local entityFile = RsConfigGetStreamDir() + "weapons/" + weaponNode.name + "/entity.type"
	local cmdline = "x:/tools/util/GenerateLODInfo.py " + entityFile
	format "Command line: %\n" cmdline
	if doscommand cmdline != 0 then (
		messagebox "Failed to export mesh map."
		return false
	)
)


--
-- name: RsWeaponExportModel
-- desc: Export the selected weapon model using rexMax.
--
fn RsWeaponExportModel = 
(
	local weaponNode = selection[1]
	local weaponFile = RsConfigGetStreamDir() + "weapons/" + weaponNode.name
	local weaponDir = weaponFile + "/"

	-- Ensure we don't delete the ICD.
	RsDeleteFiles (weaponFile + ".ift")
	RsDeleteFiles (weaponFile + ".itd")
	RsDeleteFiles (weaponFile + ".idr")
	RsDeleteFiles (weaponDir + "*.*")

	rexReset()
	local graphRoot = rexGetRoot()

	local origobj = #() + selection
	local selectobjs = #()

	for childobj in weaponNode.children do (

		if classof childobj == Col_Mesh or classof childobj == Col_Box or classof childobj == Col_Sphere or classof childobj == Col_Capsule then (

			append selectobjs childobj
		)
	)

	-------------------------------------------------------------------------
	-- Export Geometry
	-------------------------------------------------------------------------

	entity = rexAddChild graphRoot "entity" "Entity"
	rexSetPropertyValue entity "OutputPath" weaponDir
	rexSetPropertyValue entity "EntityFragmentWriteMeshes" "true"

	-- RAGE Drawable LOD Support
	local selobjs = RsLodDrawable_SetUserProps origobj[1]
	selectMore selobjs

	mesh = rexAddChild entity "mesh" "Mesh"
	rexSetPropertyValue mesh "OutputPath" weaponDir
	rexSetPropertyValue mesh "ExportTextures" "true"
	rexSetPropertyValue mesh "TextureOutputPath" weaponDir
	rexSetPropertyValue mesh "MeshSkinOffset" "true"
	rexSetNodes mesh

	skel = rexAddChild entity "skeleton" "Skeleton"
	rexSetPropertyValue skel "OutputPath" weaponDir
	rexSetPropertyValue skel "SkeletonUseBoneIDs" "true"
	rexSetPropertyValue skel "SkeletonRootAtOrigin" "true"
	rexSetPropertyValue skel "ChannelsToExport" "(transX;transY;transZ;)(rotX;rotY;rotZ;)"
	rexSetPropertyValue skel "SkeletonWriteLimitAndLockInfo" "true"
	rexSetNodes skel
	
	-- Added so that if object is skinned then it's a fragment too and doesn't
	-- require the 'Is Fragment' flag set.
	if ( undefined != weaponNode.modifiers[#Skin] ) then
	(
		RsSetupFragBounds weaponNode entity weaponDir		
		fragment = rexAddChild entity "fragment" "Fragment"
		rexSetPropertyValue fragment "OutputPath" weaponDir
		rexSetNodes fragment
		isFragment = true
	)

	-------------------------------------------------------------------------
	-- Export Bounds
	-------------------------------------------------------------------------

	if ( selectobjs.count > 0 ) then 
	(
		bound = rexAddChild entity weaponNode.name "Bound"
		rexSetPropertyValue bound "OutputPath" weaponDir
		rexSetPropertyValue bound "ForceExportAsComposite" "false"
		rexSetPropertyValue bound "ExportAsComposite" "true"
		rexSetPropertyValue bound "BoundExportWorldSpace" "false"
		rexSetPropertyValue bound "BoundZeroPrimitiveCentroids" "true"

		surfaceProperty = ""

		for selobj in selectobjs do (

			surfaceType = RsGetCollSurfaceTypeString selobj

			surfaceProperty = surfaceProperty + selobj.name + "="
			surfaceProperty = surfaceProperty + surfaceType + ":"
		)

		rexSetPropertyValue bound "SurfaceType" surfaceProperty

		select selectobjs 

		rexSetNodes bound
	)

	select origobj

	RsRexExportWithReport()	

	RsWeaponExportExtraTextures weaponDir

	return true
)


--
-- name: RsWeaponExportResourceModel
-- desc: Resource the model data.
--
fn RsWeaponExportResourceModel isPatch = (

	local weaponNode = selection[1]
	local weaponName = weaponNode.name
	local weaponFile = "weapons/" + weaponName

	local rbFileName = RsConfigGetScriptDir() + "weapons/" + weaponName + "_model_resourcing.rb"
	RsMakeSurePathExists rbFileName

	local idxFragment = getattrindex "Gta Object" "Is Fragment"
	
	-- If this isn't skinned, then check to see if 'Is Fragment' is flagged
	if (isFragment == false) do (
	
		isFragment = ( getattr weaponNode idxFragment )
	)
	
	if ( 0 != ( RsWeaponScriptGenerator.BuildDrawable rbFileName isPatch:isPatch isFragment:isFragment ) ) then
	(
		messageBox "Failed to build weapon drawable/fragment."
		return false
	)
	
	true
)


--
-- name: RsWeaponExportResourceBounds
-- desc: Resource the bounds data.
--
fn RsWeaponExportResourceBounds isPatch = (

	local weaponNode = selection[1]
	local weaponName = weaponNode.name
	local weaponFile = "weapons/" + weaponName
	
	local rbFileName = RsConfigGetScriptDir() + "weapons/" + weaponName + "_bound_resourcing.rb"
	RsMakeSurePathExists rbFileName
	
	if ( 0 != ( RsWeaponScriptGenerator.BuildBoundsDictionary rbFileName isPatch:isPatch ) ) then
	(
		messageBox "Failed to build weapon bounds dictionary."
		return false
	)
	
	true
)


--
-- name: RsWeaponExportResourceTxd
-- desc: Resource the texture dictionary data.
--
fn RsWeaponExportResourceTxd isPatch = (

	local texmaplist = #()
	local maxsizelist = #()
	local isbumplist = #()
	RsGetTexMapsFromObj selection[1] texmaplist maxsizelist isbumplist

	local weaponNode = selection[1]
	local weaponName = weaponNode.name
	local weaponFile = "weapons/" + weaponName

	local rbFileName = RsConfigGetScriptDir() + "weapons/" + weaponName + "_itd_resourcing.rb"
	RsMakeSurePathExists rbFileName
	
	if ( 0 != ( RsWeaponScriptGenerator.BuildTextureDictionary rbFileName isPatch:isPatch ) ) then
	(
		messageBox "Failed to build weapon texture dictionary."
		return false
	)
	
	true
)


--
-- name: RsWeaponExtractRPF
-- desc: Extract the weapon RPF into the stream.
--
fn RsWeaponExtractRPF =
(
	local rbFileName = RsConfigGetScriptDir() + "weapons/weapon_rpf_extract.rb"
	RsMakeSurePathExists rbFileName
	
	if ( 0 != ( RsWeaponScriptGenerator.ExtractRPF rbFileName ) ) then
	(
		messageBox "Failed to extract the weapons RPF."
		return false
	)
	
	true
)

--
-- name: RsWeaponExportBuildRPF
-- desc: Builds the weapon RPF.
--
fn RsWeaponExportBuildRPF = (

	local rbFileName = RsConfigGetScriptDir() + "weapons/weapon_build_rpf.rb"
	RsMakeSurePathExists rbFileName

	if ( 0 != ( RsWeaponScriptGenerator.BuildRPF rbFileName ) ) then
	(
		messageBox "Failed to build weapons RPF."
		return false
	)
	
	true
)

--
-- func: RsWeaponExportSel 
-- desc: 
--
fn RsWeaponExportSel isPatch exportMeshMap = (

	local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
	local sellist = #()

	for obj in selection do (
		
		if ( "Gta Object" == GetAttrClass obj ) then
		(
			if ( GetAttr obj idxDontExport ) then (
				continue
			)
			else (
				append sellist obj
			)
		)
	)
	if ( 0 == sellist.count ) then (
		print "No exportable objects in selection"
		return false
	)

	for obj in sellist do (
	
		-- reset global
		isFragment = false
		if obj.parent == undefined then (

			select obj

			format "Exporting weapon: %\n" obj.name
			if RsIsFileReadOnly (RsConfigGetIndependentDir() + "models/cdimages/weapons.rpf") == true do (
			
				messagebox "weapons.rpf is read-only"
				return false
			)
			if RsWeaponExportIsValidForExport() == false then return false
			if RsWeaponExportModel() == false then return false
			if RsWeaponExportResourceModel isPatch == false then return false
			if RsWeaponExportResourceTxd isPatch == false then return false
			if RsWeaponExportResourceBounds isPatch == false then return false
			if exportMeshMap then
			(
				if RsWeaponExportMeshMap() == false then return false
			)
		)
		else
		(
			format "Object % has a parent.  Invalid for weapon export.\n" obj.name
		)
	)

	if RsWeaponExportResourceBounds isPatch == false then return false
	if isPatch == false then (
		if RsWeaponExportBuildRPF() == false then return false
	)

	select sellist
)


-- weapon.ms
