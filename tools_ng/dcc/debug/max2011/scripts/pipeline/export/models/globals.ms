--
-- File:: globals.ms
-- Description:: Simple model mesh and material exporter global data.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 5 December 2008
--

-- Uses
filein "pipeline/util/settingsave.ms"

RsModelOutputDirectoryDefault = ( RsConfigGetStreamDir() + "models/" )
RsModelOutputDirectory = (RsSettingsReadString "rsmodel" "outputdirectory" RsModelOutputDirectoryDefault)

--
-- name: RsModelSetOutputDirectory
-- desc: Sets and saves the output directory path set by the user.
--
fn RsModelSetOutputDirectory output_dir = (

	RsModelOutputDirectory = output_dir
	RsSettingWrite "rsmodel" "outputdirectory" RsModelOutputDirectory
)

-- globals.ms
