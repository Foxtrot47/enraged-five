--
-- File:: pipeline/export/models/ruby_script_generator.ms
-- Description::
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 7 December 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/models/globals.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------


--
-- struct: RsModelScriptGeneratorStruct
-- desc: Model Ruby script generator container for functions.
--
struct RsModelScriptGeneratorStruct
(

	--
	-- name: RunScript
	-- desc: Run a Ruby script with optional arguments.
	--
	fn RunScript rubyFileName args:"" = (
	
		local commandLine = stringStream ""
		format "ruby \"%\" %" rubyFilename args to:commandLine

		DOSCommand commandLine
	),
	
	--
	-- name: GenerateResourceCommonHeader
	-- desc:
	--
	-- note: Should not be called externally.
	--
	fn GenerateResourceCommonHeader rubyFile = (
		
		local projname = RsConfigGetProjectName()
		format "require 'pipeline/config/projects'\n" to:rubyFile
		format "Pipeline::Config::instance().logtostdout = true\n" to:rubyFile
		format "require 'pipeline/config/project'\n" to:rubyFile
		format "require 'pipeline/content/content_core'\n" to:rubyFile
		format "require 'pipeline/gui/exception_dialog'\n" to:rubyFile
		format "require 'pipeline/os/file'\n" to:rubyFile
		format "require 'pipeline/projectutil/data_convert'\n" to:rubyFile
		format "require 'pipeline/resourcing/convert'\n" to:rubyFile
		format "require 'pipeline/resourcing/util'\n" to:rubyFile
		format "require 'pipeline/util/rage'\n" to:rubyFile
		format "require 'pipeline/util/string'\n" to:rubyFile
		format "include Pipeline\n\n" to:rubyFile

		format "begin\n\n" to:rubyFile
		format "\tproject = Pipeline::Config.instance.projects[\"%\"]\n" projname to:rubyFile
		format "\tproject.load_config( )\n" to:rubyFile
		format "\tbranch = project.branches[project.default_branch]\n" to:rubyFile
		format "\tr = RageUtils::new( project, branch.name )\n\n" to:rubyFile
	),


	--
	-- name: GenerateResourceCommonFooter
	-- desc:
	--
	-- note: Should not be called externally.
	--
	fn GenerateResourceCommonFooter rubyFile = (

		format "rescue Exception => ex\n\n" to:rubyFile
		format "GUI::ExceptionDialog::show_dialog( ex )\n" to:rubyFile
		format "end\n\n" to:rubyFile 	
	),
	
	--
	-- name: BuildDrawable
	-- desc: Build a drawable IDR file from a directory.
	--
	fn BuildDrawable rubyFileName drawableFile drawableDir = (

		RsMakeSurePathExists rubyFileName
		local rubyFile = createFile rubyFileName
		
		GenerateResourceCommonHeader rubyFile
		
		drawableFile = RsMakeSafeSlashes( drawableFile )
		format "\tidr_filename = '%'\n" drawableFile to:rubyFile
		format "\tif ( 0 != 'idr'.casecmp( OS::Path::get_extension( idr_filename ) ) ) then\n" to:rubyFile
		format "\t\tidr_filename = OS::Path::replace_ext( idr_filename, 'idr' )\n" to:rubyFile
		format "\tend\n" to:rubyFile
		format "\tfiles = OS::FindEx::find_files( OS::Path::combine( '%', '*.*' ) )\n" drawableDir to:rubyFile
		format "\tr.pack.start()\n" to:rubyFile
		format "\tfiles.each do |filename|\n" to:rubyFile
		format "\t\tr.pack.add( filename, OS::Path::get_filename( filename ) )\n" to:rubyFile
		format "\tend\n" to:rubyFile
		format "\tr.pack.save( idr_filename )\n" to:rubyFile 
		format "\tr.pack.close( )\n" to:rubyFile
		
		GenerateResourceCommonFooter rubyFile
		
		close rubyFile
		RunScript rubyFileName
	),	
	
	
	--
	-- name: BuildFragment
	-- desc: Build a fragment IFT file from a directory.
	--
	fn BuildFragment rubyFileName fragmentFile fragmentDir = (

		RsMakeSurePathExists rubyFileName
		local rubyFile = createFile rubyFileName
		
		GenerateResourceCommonHeader rubyFile
		
		fragmentFile = RsMakeSafeSlashes( fragmentFile )
		format "\tift_filename = '%'\n" fragmentFile to:rubyFile
		format "\tif ( 0 != 'idr'.casecmp( OS::Path::get_extension( ift_filename ) ) ) then\n" to:rubyFile
		format "\t\tift_filename = OS::Path::replace_ext( ift_filename, 'ift' )\n" to:rubyFile
		format "\tend\n" to:rubyFile
		format "\tfiles = OS::FindEx::find_files( OS::Path::combine( '%', '*.*' ) )\n" fragmentDir to:rubyFile
		format "\tr.pack.start()\n" to:rubyFile
		format "\tfiles.each do |filename|\n" to:rubyFile
		format "\t\tr.pack.add( filename, OS::Path::get_filename( filename ) )\n" to:rubyFile
		format "\tend\n" to:rubyFile
		format "\tr.pack.save( ift_filename )\n" to:rubyFile 
		format "\tr.pack.close( )\n" to:rubyFile
		
		GenerateResourceCommonFooter rubyFile
		
		close rubyFile
		RunScript rubyFileName	
	),
	
	--
	-- name: BuildDrawableDictionary
	-- desc: Build a drawable dictionary IDD file from a set of directories.
	--
	fn BuildDrawableDictionary rubyFileName drawableDictFile drawableDirs = (

		RsMakeSurePathExists rubyFileName
		local rubyFile = createFile rubyFileName
		
		GenerateResourceCommonHeader rubyFile
		
		drawableDictFile = RsMakeSafeSlashes( drawableDictFile )
		format "\tidd_filename = '%'\n" drawableDictFile to:rubyFile
		format "\tif ( 0 != 'idd'.casecmp( OS::Path::get_extension( idd_filename ) ) ) then\n" to:rubyFile
		format "\t\tidd_filename = OS::Path::replace_ext( idd_filename, 'idd' )\n" to:rubyFile
		format "\tend\n" to:rubyFile
		format "\tr.pack.start()\n" to:rubyFile
		for drawableDir in drawableDirs do
		(
			format "\tfiles = OS::FindEx::find_files( OS::Path::combine( '%', '*.*' ) )\n" drawableDir to:rubyFile
			format "\tfiles.each do |filename|\n" to:rubyFile
			format "\t\tr.pack.add( filename, OS::Path::combine( OS::Path::get_trailing_directory( '%' ), OS::Path::get_filename( filename ) ) )\n" drawableDir to:rubyFile
			format "\tend\n" to:rubyFile
		)
		format "\tr.pack.save( idd_filename )\n" to:rubyFile 
		format "\tr.pack.close( )\n" to:rubyFile
		
		GenerateResourceCommonFooter rubyFile
		
		close rubyFile
		RunScript rubyFileName
	)	
)


--
-- struct: RsWeaponScriptGeneratorStruct
-- desc: Weapon Ruby script generator container for functions.
--
struct RsWeaponScriptGeneratorStruct
(
	
	--
	-- name: RunScript
	-- desc: Run a Ruby script with optional arguments.
	--
	fn RunScript rubyFileName args:"" = (
	
		local commandLine = stringStream ""
		format "ruby \"%\" %" rubyFilename args to:commandLine

		DOSCommand commandLine
	),

	--
	-- name: GenerateResourceCommonHeader
	-- desc:
	--
	-- note: Should not be called externally.
	--
	fn GenerateResourceCommonHeader rubyFile weapon_name load_content = (
		
		local projname = RsConfigGetProjectName()
		format "require 'pipeline/config/projects'\n" to:rubyFile
		format "require 'pipeline/config/project'\n" to:rubyFile
		format "require 'pipeline/content/content_core'\n" to:rubyFile
		format "require 'pipeline/gui/exception_dialog'\n" to:rubyFile
		format "require 'pipeline/os/file'\n" to:rubyFile
		format "require 'pipeline/projectutil/data_convert'\n" to:rubyFile
		format "require 'pipeline/resourcing/convert'\n" to:rubyFile
		format "require 'pipeline/resourcing/util'\n" to:rubyFile
		format "require 'pipeline/util/string'\n" to:rubyFile
		format "include Pipeline\n\n" to:rubyFile

		format "project = Pipeline::Config.instance.projects[\"%\"]\n" projname to:rubyFile
		format "project.load_config( )\n" to:rubyFile
		if ( load_content ) then
			format "project.load_content( nil, Project::ContentType::OUTPUT )\n" to:rubyFile
		format "branch = project.branches[project.default_branch]\n\n" to:rubyFile

		if ( undefined != weapon_name ) then
			format "weapon_name = '%'\n\n" weapon_name to:rubyFile

		format "stream_dir = Pipeline::OS::Path::combine( project.netstream, 'weapons' )\n" to:rubyFile
		if ( undefined != weapon_name ) then
			format "weapon_stream_dir = Pipeline::OS::Path::combine( stream_dir, weapon_name )\n" to:rubyFile
		format "script_dir = Pipeline::OS::Path::combine( project.netstream, 'mapscript', 'weapons' )\n" to:rubyFile

		format "component_group = Pipeline::Content::Group.new( 'weapon_content_group' )\n\n" to:rubyFile
	),


	--
	-- name: GenerateResourceCommonFooter
	-- desc:
	--
	-- note: Should not be called externally.
	--
	fn GenerateResourceCommonFooter rubyFile = (

		format "if component_group.children.size > 0\n" to:rubyFile
		format "\tconvert = Pipeline::ConvertSystem.instance()\n" to:rubyFile
		format "\tconvert.setup( project, nil, true, true, false)\n" to:rubyFile

		format "\tcomponent_group.children.each do | content_group |\n" to:rubyFile
		format "\t\tif content_group.methods.include?( 'build' )\n" to:rubyFile
		format "\t\t\tcontent_group.build()\n" to:rubyFile 
		format "\t\tend\n" to:rubyFile
		format "\tend\n" to:rubyFile

		format "\tconvert.build(component_group)\n" to:rubyFile 

		format "end\n\n" to:rubyFile 	
	),
	
	
	--
	-- name: GenerateResourcePatchFooter
	-- desc:
	--
	-- note: Should not be called externally.
	--
	fn GenerateResourcePatchFooter rubyFile = (
		patch_dir = RsConfigGetPatchDir()
		RsMakeSurePathExists patch_dir

		format "target_content_group = Pipeline::Content::Group.new(\"patch_group\")\n" to:rubyFile
		format "Pipeline::Resourcing::create_target_content_from_indepenent( component_group, target_content_group, project, \"%\" )\n" patch_dir to:rubyFile
		format "convert.build(target_content_group)" to:rubyFile
	),

	--
	-- name: BuildBoundsDictionary
	-- desc:
	--
	fn BuildBoundsDictionary rubyFileName isPatch:false = (
	
		RsMakeSurePathExists rubyFileName
		local weaponName = selection[1].name
		local projectName = RsConfigGetProjectName()
		local rubyFile = createFile rubyFileName
		
		GenerateResourceCommonHeader rubyFile weaponName false
		
		format "ibd = Content::RPF.new( 'weapons', stream_dir, 'ibd', branch.ind_target )\n" to:rubyFile
		format "bound_files = OS::FindEx::find_files_recurse( OS::Path::combine( stream_dir, '*.bnd' ) )\n" to:rubyFile
		format "bound_files.each do |file|\n" to:rubyFile
		format "\tputs \"Adding: #{file}\"\n" to:rubyFile
		format "\tibd.inputs << Content::File::from_filename( file )\n" to:rubyFile
		format "end\n\n" to:rubyFile

		format "component_group.children << ibd\n\n" to:rubyFile
		GenerateResourceCommonFooter rubyFile
		
		if ( isPatch ) then
			GenerateResourcePatchFooter rubyFile

		close rubyFile
		RunScript rubyFileName
	),
	
	--
	-- name: BuildDrawable
	-- desc:
	--
	fn BuildDrawable rubyFileName isPatch:false isFragment:false = (

		RsMakeSurePathExists rubyFileName
		local weaponName = selection[1].name
		local projectName = RsConfigGetProjectName()
		local rubyFile = createFile rubyFileName
		
		GenerateResourceCommonHeader rubyFile weaponName false
		
		if ( isFragment ) then
			format "ift = Content::RPF.new( weapon_name, stream_dir, 'ift', branch.ind_target )\n" to:rubyFile
		else
			format "idr = Content::RPF.new( weapon_name, stream_dir, 'idr', branch.ind_target )\n" to:rubyFile

		format "all_files = OS::FindEx::find_files( OS::Path::combine( weapon_stream_dir, '*.*' ) )\n" to:rubyFile
		format "all_files.each do |file|\n" to:rubyFile
		format "\tnext if ( file.ends_with( '.dds' ) )\n" to:rubyFile
		format "\tnext if ( file.ends_with( '.bnd' ) )\n\n" to:rubyFile

		format "\tputs \"Adding: #{file}\"\n" to:rubyFile
		if ( isFragment ) then
			format "\tift.inputs << Content::File::from_filename( file )\n" to:rubyFile
		else
			format "\tidr.inputs << Content::File::from_filename( file )\n" to:rubyFile
		format "end\n\n" to:rubyFile

		if ( isFragment ) then
			format "component_group.children << ift\n" to:rubyFile
		else
			format "component_group.children << idr\n" to:rubyFile
		GenerateResourceCommonFooter rubyFile
		
		if ( isPatch ) then
			GenerateResourcePatchFooter rubyFile			
	
		close rubyFile
		RunScript rubyFileName
	),

	--
	-- name: BuildTextureDictionary
	-- desc:
	--
	fn BuildTextureDictionary rubyFileName isPatch:false = (
	
		RsMakeSurePathExists rubyFileName
		local weaponName = selection[1].name
		local projectName = RsConfigGetProjectName()
		local rubyFile = createFile rubyFileName	
		
		GenerateResourceCommonHeader rubyFile weaponName false
		
		format "itd = Content::RPF.new( weapon_name, stream_dir, 'itd', branch.ind_target )\n" to:rubyFile
		format "texture_files = OS::FindEx::find_files_recurse( OS::Path::combine( weapon_stream_dir, '*.dds' ) )\n" to:rubyFile
		format "texture_files.each do |texture|\n" to:rubyFile
		format "\titd.inputs << Content::File::from_filename( texture )\n" to:rubyFile
		format "end\n\n" to:rubyFile

		format "component_group.children << itd\n\n" to:rubyFile
		GenerateResourceCommonFooter rubyFile
		
		if ( isPatch ) then
			GenerateResourcePatchFooter rubyFile		
		
		close rubyFile
		RunScript rubyFileName
	),

	--
	-- name: BuildImage
	-- desc: Create and run the vehicle RPF resourcing Ruby script body.
	--
	fn BuildRPF rubyFileName rebuild:false = (

		RsMakeSurePathExists rubyFileName
		local projectName = RsConfigGetProjectName()
		local rubyFile = createFile rubyFileName

		format "require 'pipeline/config/projects'\n" to:rubyFile
		format "require 'pipeline/config/project'\n" to:rubyFile
		format "require 'pipeline/content/content_core'\n" to:rubyFile
		format "require 'pipeline/gui/exception_dialog'\n" to:rubyFile
		format "require 'pipeline/os/file'\n" to:rubyFile
		format "require 'pipeline/projectutil/data_convert'\n" to:rubyFile
		format "require 'pipeline/util/rage'\n" to:rubyFile
		format "include Pipeline\n\n" to:rubyFile

		format "begin\n" to:rubyFile
		format "\tproject = Pipeline::Config.instance.projects[\"%\"]\n" projectName to:rubyFile
		format "\tproject.load_config( )\n" to:rubyFile
		format "\tproject.load_content( nil, Project::ContentType::OUTPUT )\n" to:rubyFile
		format "\tbranch = project.branches[project.default_branch]\n\n" to:rubyFile
		format "\tstream_dir = Pipeline::OS::Path::combine( project.netstream, 'weapons' )\n" to:rubyFile
		
		format "\timgs = project.content.find_by_script( '\"weaponsrpf\" == content.xml_type' )\n" to:rubyFile
		format "\timage_files = OS::FindEx::find_files( OS::Path::combine( stream_dir, '*.i*' ) )\n" to:rubyFile
		format "\tr = RageUtils.new( project, branch.name )\n" to:rubyFile
		format "\tr.pack.start_uncompressed()\n\n" to:rubyFile
		format "\timage_files.each do |filename|\n" to:rubyFile
		format "\t\tputs \"Adding file: #{filename}\"\n" to:rubyFile
		format "\t\tr.pack.add( filename, OS::Path::get_filename( filename ) )\n" to:rubyFile
		format "\tend\n\n" to:rubyFile
		format "\tr.pack.save( imgs[0].filename )\n" to:rubyFile
		format "\tr.pack.close( )\n" to:rubyFile

		format "\tProjectUtil::data_convert_file( imgs[0].filename )\n\n" to:rubyFile

		format "rescue Exception => ex\n" to:rubyFile
		format "\tGUI::ExceptionDialog.show_dialog( ex )\n" to:rubyFile
		format "end\n\n" to:rubyFile
		
		close rubyFile
		RunScript rubyFileName
	),


	--
	-- name: ExtractRPF
	-- desc: Create and run the vehicle RPF extraction script.
	--
	fn ExtractRPF rubyFileName = (

		RsMakeSurePathExists rubyFileName
		local projectName = RsConfigGetProjectName()
		local rubyFile = createFile rubyFileName

		format "require 'pipeline/config/projects'\n" to:rubyFile
		format "require 'pipeline/config/project'\n" to:rubyFile
		format "require 'pipeline/content/content_core'\n" to:rubyFile
		format "require 'pipeline/gui/exception_dialog'\n" to:rubyFile
		format "require 'pipeline/os/file'\n" to:rubyFile
		format "require 'pipeline/projectutil/data_extract'\n" to:rubyFile
		format "require 'pipeline/util/rage'\n" to:rubyFile
		format "include Pipeline\n\n" to:rubyFile

		format "begin\n" to:rubyFile
		format "\tproject = Pipeline::Config.instance.projects[\"%\"]\n" projectName to:rubyFile
		format "\tproject.load_config( )\n" to:rubyFile
		format "\tproject.load_content( nil, Project::ContentType::OUTPUT )\n" to:rubyFile
		format "\tbranch = project.branches[project.default_branch]\n\n" to:rubyFile

		format "\timgs = project.content.find_by_script( '\"weaponsrpf\" == content.xml_type' )\n" to:rubyFile
		format "\tstream_dir = Pipeline::OS::Path::combine( project.netstream, 'weapons' )\n" to:rubyFile
		format "\tr = RageUtils.new( project, branch.name )\n" to:rubyFile
		format "\tputs \"Extract: #{imgs[0].filename}\"\n" to:rubyFile
		format "\trpffiles = ProjectUtil::data_extract_rpf( r, imgs[0].filename, stream_dir, true ) do |rpfname|\n" to:rubyFile
		format "\t\tputs \"Extracting: #{rpfname}\"\n" to:rubyFile
		format "\tend\n" to:rubyFile

		format "\trpffiles.each do |rpfname|\n" to:rubyFile
		format "\t\tif ( 'ibd' == OS::Path::get_extension( rpfname ) ) then\n" to:rubyFile
		format "\t\t\tProjectUtil::data_extract_rpf( r, rpfname, stream_dir, true ) do |filename|\n" to:rubyFile
		format "\t\t\t\tputs \"\tExtracting: #{filename}\"\n" to:rubyFile
		format "\t\t\t\tdest = OS::Path::combine( Pipeline::OS::Path::remove_extension( filename ), OS::Path::get_filename( filename ) )\n" to:rubyFile
		format "\t\t\t\tOS::FileUtilsEx::move_file( filename, dest, true )\n" to:rubyFile
		format "\t\t\tend\n" to:rubyFile
		format "\t\telse\n" to:rubyFile
		format "\t\t\toutput_dir = OS::Path::remove_extension( rpfname )\n" to:rubyFile
		format "\t\t\tProjectUtil::data_extract_rpf( r, rpfname, output_dir, true ) do |filename|\n" to:rubyFile
		format "\t\t\t\tputs \"\tExtracting: #{filename}\"\n" to:rubyFile
		format "\t\t\tend\n" to:rubyFile
		format "\t\tend\n" to:rubyFile
		format "\tend\n" to:rubyFile

		format "rescue Exception => ex\n" to:rubyFile
		format "\tGUI::ExceptionDialog.show_dialog( ex )\n" to:rubyFile
		format "end\n" to:rubyFile
		
		close rubyFile
		RunScript rubyFileName
	)

)

-- Create instances of our script generator struct.
global RsWeaponScriptGenerator = RsWeaponScriptGeneratorStruct()
global RsModelScriptGenerator = RsModelScriptGeneratorStruct()

-- pipeline/export/models/ruby_script_generator.ms
