--
-- File:: pipeline/ui/SetObjectAttr.ms.ms
-- Description:: Set Object Attributes
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com
-- Date:: 19 March 2010
--
-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/UtilityFunc.ms"	

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout AttrRoll "Set Object Attributes"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Set_Object_Attributes" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	button btnIPL "Don't Add to IPL" width:100 height:25
	button btnDontExport "Don't Export" width:100 height:25
	button btnApplyRad "Don't Apply Radiosity" width:100 height:25
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnIPL pressed do (
		AddIgnoreSelected()
	)
	
	on btnDontExport pressed do (
		SetDontExportSelected()
	)
	
	on btnApplyRad pressed do (
		SetDontApplyRadiosity()
	)
)

try CloseRolloutFloater AttrRollout catch()
AttrRollout = newRolloutFloater "Set Object Attributes" 200 700 50 126
addRollout AttrRoll AttrRollout rolledup:false