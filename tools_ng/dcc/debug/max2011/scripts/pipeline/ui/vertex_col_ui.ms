--
-- File:: pipeline/ui/vertex_col_ui.ms
-- Description:: Vertex colour and illum channel darken/lighten utility.
--
-- Author:: Greg Smith (original)
-- Date:: 22 September 2004
--
-- Author:: David Muir <david.muir@rockstarnorth.com> (vertex-selection and some cleanup)
-- Date:: 30 July 2009
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com> (combining with new Radiosity and Lighting tool)
-- Date:: 10 February 2010  
--

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

fn GtaChangeColAbs obj value doVertex doIllum relative = (

	if ( Editable_Mesh != ( classof obj ) ) then 
		return false
	
	local VERT_CPV = 0
	local VERT_ILLUM = -1
	
	format "vert: %; illum: % percentage: %\n" doVertex doIllum value
	value /= 100
	
	if ( true == doVertex ) then 
	(
		local numCPVVerts = getNumCPVVerts(obj.mesh)
		local selVerts = ( getVertSelection obj )
	
		if ( BitArray == ( classof selVerts ) and ( false == selVerts.isEmpty ) ) then
		(
			-- Process selection mesh verts
			format "Process selected mesh verts...\n"
			for v in selVerts do
			(
				if not (meshop.getMapSupport obj VERT_CPV) then
				(
					print "no colour channel on this model."
					return false
				)
				vertColour = meshop.getMapVert obj VERT_CPV v
				vertColour  *= 255
				if ( undefined != vertColour ) then 
				(
					-- do our shit
					if relative then
					(
						vertColour.x += (vertColour.x * value)
						vertColour.y += (vertColour.y * value)
						vertColour.z += (vertColour.z * value)
					)
					else
					(
						vertColour.x += (255 * value)
						vertColour.y += (255 * value)
						vertColour.z += (255 * value)
					)
					-- Clamp
					if vertColour.x < 0.0 then (
						vertColour.x = 0.0
					)
					if vertColour.x > 255.0 then (
						vertColour.x = 255.0
					)

					if vertColour.y < 0.0 then (
						vertColour.y = 0.0
					)
					if vertColour.y > 255.0 then (
						vertColour.y = 255.0
					)

					if vertColour.z < 0.0 then (
						vertColour.z = 0.0
					)
					if vertColour.z > 255.0 then (
						vertColour.z = 255.0
					)

					-- set the colour
					meshop.setVertColor obj VERT_CPV v vertColour
				)		
			)
		)
		else
		(
			-- Process all mesh verts
			format "Process all mesh verts...\n"
			for i = 1 to numCPVVerts do 
			(
				-- get the colour

				vertColour = getVertColor obj.mesh i

				if vertColour != undefined then (

					-- do our shit
					if relative then
					(
						vertColour.x += vertColour.x * value
						vertColour.y += vertColour.y * value
						vertColour.z += vertColour.z * value
					)
					else
					(
						vertColour.x += 255 * value
						vertColour.y += 255 * value
						vertColour.z += 255 * value
					)

					if vertColour.r < 0.0 then (
						vertColour.r = 0.0
					)
					if vertColour.r > 255.0 then (
						vertColour.r = 255.0
					)

					if vertColour.g < 0.0 then (
						vertColour.g = 0.0
					)
					if vertColour.g > 255.0 then (
						vertColour.g = 255.0
					)

					if vertColour.b < 0.0 then (
						vertColour.b = 0.0
					)
					if vertColour.b > 255.0 then (
						vertColour.b = 255.0
					)

					-- set the colour
					setVertColor obj.mesh i vertColour
				)
			)
		)
	)
	
	if ( true == doIllum ) then 
	(
	
		local numIllumVerts = getNumIVCVerts(obj.mesh)
		local selVerts = ( getVertSelection obj )
		
		if ( BitArray == ( classof selVerts ) and ( false == selVerts.isEmpty ) ) then
		(
			-- Process selection mesh verts
			format "Process selected mesh verts...\n"
			for v in selVerts do
			(
				if not (meshop.getMapSupport obj VERT_ILLUM) then
				(
					format "no illum channel on this model."
					return false
				)

				vertColour = meshop.getMapVert obj VERT_ILLUM v
				vertColour  *= 255
				
				if ( undefined != vertColour ) then 
				(
					-- do our shit
					if relative then
					(
						vertColour.x += vertColour.x * value
						vertColour.y += vertColour.y * value
						vertColour.z += vertColour.z * value
					)
					else
					(
						vertColour.x += 255 * value
						vertColour.y += 255 * value
						vertColour.z += 255 * value
					)

					-- Clamp
					if vertColour.x < 0.0 then (
						vertColour.x = 0.0
					)
					if vertColour.x > 255.0 then (
						vertColour.x = 255.0
					)

					if vertColour.y < 0.0 then (
						vertColour.y = 0.0
					)
					if vertColour.y > 255.0 then (
						vertColour.y = 255.0
					)

					if vertColour.z < 0.0 then (
						vertColour.z = 0.0
					)
					if vertColour.z > 255.0 then (
						vertColour.z = 255.0
					)

					-- set the colour
					meshop.setVertColor obj VERT_ILLUM v vertColour
				)		
			)	
		)
		else
		(	
			-- Process all mesh verts
			format "Process all mesh verts...\n"
			for i = 1 to numIllumVerts do (

				-- get the colour		
				vertColour = getIllumVertColor obj.mesh i

				-- do our shit			
				if vertColour != false then (

					if relative then
					(
						vertColour.x += vertColour.x * value
						vertColour.y += vertColour.y * value
						vertColour.z += vertColour.z * value
					)
					else
					(
						vertColour.x += 255 * value
						vertColour.y += 255 * value
						vertColour.z += 255 * value
					)

					if vertColour[1] < 0.0 then (
						vertColour[1] = 0.0
					)
					if vertColour[1] > 255.0 then (
						vertColour[1] = 255.0
					)

					if vertColour[2] < 0.0 then (
						vertColour[2] = 0.0
					)
					if vertColour[2] > 255.0 then (
						vertColour[2] = 255.0
					)

					if vertColour[3] < 0.0 then (
						vertColour[3] = 0.0
					)
					if vertColour[3] > 255.0 then (
						vertColour[3] = 255.0
					)

					-- set the colour				
					setIllumVertColor obj.mesh i vertColour
				)
			)
		)
	)
	
	true
)
	
	
fn getSelectedCV obj vertChecked = (
	doVC = false
	
	if getNumCPVVerts(obj.mesh) > 0 and vertChecked then (
		doVC = true
	)

	selVCVerts = #()
	currVCFace = undefined
	for i = 1 to obj.mesh.selectedverts.count do (
		for j = 1 to obj.mesh.faces.count do (
	
			currFace = getface obj.mesh j
			if doVC then currVCFace = getVCFace obj.mesh j
	
			for k = 1 to 3 do (
			
				if currFace[k] == obj.mesh.selectedverts[i].index then (
				
					if doVC then (
						
						append selVCVerts currVCFace[k]
					)
				)
			)
		)
	)
		
	return selVCVerts 
)


fn getSelectedIV obj illumChecked = (
	doIC = false
	
	if getNumIVCVerts(obj.mesh) > 0 and illumChecked then (
		doIC = true
	)
	
	selICVerts = #()
	currIVFace = undefined
	
	for i = 1 to obj.mesh.selectedverts.count do (
		for j = 1 to obj.mesh.faces.count do (
			
			currFace = getface obj.mesh j
			if doIC then currIVFace = getIVFace obj.mesh j
			
			for k = 1 to 3 do (
				
				if currFace[k] == obj.mesh.selectedverts[i].index then (
				
					if doIC then (
						
						append selICVerts currIVFace[k]
					)
				)
			)
		)
	)
		
	return selICVerts 
)


--------------------------------------------------------------
-- Handlers for external events
--------------------------------------------------------------
fn CopyButton vertChecked illumChecked = (
	if selection.count < 1 then (
			return 0
	)
		
	if classof selection[1] != Editable_Mesh then (
		return 0
	)
		
	obj = selection[1]
	colours = #()
	
	selVCVerts = getSelectedCV obj vertChecked
	selICVerts = getSelectedIV obj illumChecked
		
	if selVCVerts.count > 0 then (
		
		avgVertColTemp = [0,0,0]
		
		for i = 1 to selVCVerts.count do (
			
			currVertCol = getVertColor obj.mesh selVCVerts[i]
		
			avgVertColTemp[1] = avgVertColTemp[1] + currVertCol.r
			avgVertColTemp[2] = avgVertColTemp[2] + currVertCol.g
			avgVertColTemp[3] = avgVertColTemp[3] + currVertCol.b
		)
			
		avgVertColTemp = avgVertColTemp / selVCVerts.count	
			
		for i = 1 to 3 do (
			if avgVertColTemp[i] > 255.0 then (
				avgVertColTemp[i] = 255.0
			)
			if avgVertColTemp[i] < 0 then (
				avgVertColTemp[i] = 0
			)
		)
			
		append colours (color avgVertColTemp[1] avgVertColTemp[2] avgVertColTemp[3])
	) else (
		append colours (color 255 255 255)
	)
		
	if selICVerts.count > 0 then (
		
		avgIllumColTemp = [0,0,0]
		
		for i = 1 to selICVerts.count do (
			
			currVertCol = getIllumVertColor obj.mesh selICVerts[i]
				
			avgVertColTemp[1] = avgVertColTemp[1] + currVertCol[1]
			avgVertColTemp[2] = avgVertColTemp[2] + currVertCol[2]
			avgVertColTemp[3] = avgVertColTemp[3] + currVertCol[3]
		)
			
		avgIllumColTemp = avgVertColTemp / selICVerts.count
				
		for i = 1 to 3 do (
			if avgIllumColTemp[i] > 255.0 then (
				avgIllumColTemp[i] = 255.0
			)
			if avgIllumColTemp[i] < 0 then (
				avgIllumColTemp[i] = 0
			)
		)
					
		append colours (color avgIllumColTemp[1] avgIllumColTemp[2] avgIllumColTemp[3])
	) else (
		append colours (color 255 255 255)
	)
	
	return colours
)

fn PasteButton colourVert colourIllum vertChecked illumChecked = (
	if selection.count < 1 then (
			return 0
	)
		
	if classof selection[1] != Editable_Mesh then (
		return 0
	)
		
	obj = selection[1]
	
	selVCVerts = getSelectedCV obj vertChecked
	selICVerts = getSelectedIV obj illumChecked
		
	if selVCVerts.count > 0 then (		
		for i = 1 to selVCVerts.count do (
			setVertColor obj.mesh selVCVerts[i] colourVert
		)
	)
		
	if selICVerts.count > 0 then (
		for i = 1 to selICVerts.count do (
			setIllumVertColor obj.mesh selICVerts[i] [colourIllum.r,colourIllum.g,colourIllum.b]
		)
	)
)

-- pipeline/ui/vertex_col_ui.ms
