--
-- File:: pipeline/ui/ERotFinder.ms
-- Description::  Find non z-rotation xrefs
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- For Dave C
-- v1.0
--
-- v1.1 Added Z-rotation of object finder
--
-----------------------------------------------------------------------------

	rollout ERotFinder_roll "Rot Find"
	(
		--variables
		
		--ui
		
		button ERFindem "XY-rot xrefs"
		
		button EZFindem "Z-rot Objects"
						
		on ERFindem pressed do
		(
				deselect $*
										
				for loopa = 1 to rootnode.children.count do (
					--name = rootnode.children[loopa].name
					
					if (classof( rootnode.children[loopa]) == XRefObject) do(
					
						if rootnode.children[loopa].rotation.y != 0 then
						(
							selectMore rootnode.children[loopa]
						) else
						(
							if rootnode.children[loopa].rotation.x != 0 then
							(
								selectMore rootnode.children[loopa]
							)
						)
					
					)					
				)
				
				gc()
				
		)
		
		on EZFindem pressed do
		(
				deselect $*
										
				for loopa = 1 to rootnode.children.count do (
					--name = rootnode.children[loopa].name
					
					if (classof( rootnode.children[loopa]) == Editable_Mesh) do(
					
						if rootnode.children[loopa].rotation.z != 0 then
						(
							selectMore rootnode.children[loopa]
						) 					
					)
					if (classof( rootnode.children[loopa]) == Editable_Poly) do(
					
						if rootnode.children[loopa].rotation.z != 0 then
						(
							selectMore rootnode.children[loopa]
						) 					
					)					
				)
				
				gc()
				
		)

		


	
	)

	if ERotFinder_float != undefined then closerolloutfloater ERotFinder_float
	ERotFinder_float = newrolloutfloater "ERotFinder" 110 90 100 100
	addrollout ERotFinder_roll ERotFinder_float
