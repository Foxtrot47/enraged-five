--
-- File:: pipeline/ui/lod_visibility_controller.ms
-- Description:: Rockstar LOD Visibility Controller
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 16/1/2006
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Global Variables
-----------------------------------------------------------------------------
-- Used to stop resetting visibility if the LOD Visibility Controller roll isn't enabled
-- GTA5 #5372
Global visControllerEnabled = false

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
-- Global Event callback
fn Callback_VisControllerDisable = (

	if (visControllerEnabled) do (
	
		MessageBox "LOD Visibility Controller will now be disabled."
		LODVisController_Disable()
	)

)

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout RsLODVisControllerRoll "LOD Visibility Controller"
(
	----------------------------------------------------------------------------
	-- Rollout UI
	----------------------------------------------------------------------------
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/LOD_Toolkit#LOD_Visibility_Controller" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	group "LOD Preview"
	(
		button btnViewport "Viewport Mode" width:150
		button btnTXD	   "TXD Mode" width:150
		button btnPlayer   "Player Mode" width:150
		button btnDisable  "Disable" width:150
	)
	
	group "Status:"
	(
		label lblStatusMsg "Disabled"
	)
		
	----------------------------------------------------------------------------
	-- Event Handlers
	----------------------------------------------------------------------------
	
	-- "Normal Mode" Button Press
	on btnViewport pressed do
	(
		-- Invoke LODController Plugin
		if ( false == LODVisController_NormalEnable() ) then
		(
			MessageBox "LOD Visibility Controller plugin is disabled. Check $(MAX_ROOT)\\LODVisCtrl_Disable.txt"
			return false
		)
		
		-- Update status message
		lblStatusMsg.text = "Normal Mode"
		
		-- Redraw the viewports
		completeRedraw()
		
		visControllerEnabled = true
	)
	
	-- "TXD Mode" Button Press
	on btnTXD pressed do
	(
		-- Verify we have a selection before enabling this mode.
		selObjs = getCurrentSelection()
		if ( 0 == selObjs.count ) then
		(
			MessageBox "Please select the nodes whose TXD names you want visible."
			return false	
		)
		
		-- Invoke LODController Plugin
		if ( false == LODVisController_TXDEnable() ) then
		(
			MessageBox "LOD Visibility Controller plugin is disabled. Check $(MAX_ROOT)\\LODVisCtrl_Disable.txt"
			return false
		)
		
		-- Update status message
		lblStatusMsg.text = "TXD Mode"
					
		-- Redraw the viewports
		completeRedraw()
		
		visControllerEnabled = true
	)
	
	-- "Player Mode" Button Press
	on btnPlayer pressed do
	(
		-- Verify we have a selection before enabling this mode.
		selObjs = getCurrentSelection()
		if ( 1 != selObjs.count ) then
		(
			MessageBox "Please select a single object you want to act as the player."
			return false	
		)		
		
		-- Invoke LODController Plugin
		if ( not LODVisController_PlayerEnable() ) then
		(
			MessageBox "LOD Visibility Controller plugin is disabled. Check $(MAX_ROOT)\\LODVisCtrl_Disable.txt"
			return false
		)
		
		-- Update status message
		lblStatusMsg.text = "Player Mode"
					
		-- Redraw the viewports
		completeRedraw()
		
		visControllerEnabled = true
	)
	
	-- "Disable" Button Press
	on btnDisable pressed do
	(
		-- Invoke LODController Plugin
		LODVisController_Disable()
		
		-- Update status message
		lblStatusMsg.text = "Disabled"
				
		-- Redraw the viewports
		completeRedraw()
		
		visControllerEnabled = false
	)
	
	-- Rollout open event
	on RsLODVisControllerRoll open do
	(
		callbacks.addScript #filePreSave "Callback_VisControllerDisable()" id:#visCtrlPreSave
		callbacks.addScript #filePreOpen "Callback_VisControllerDisable()" id:#visCtrlPreOpen
	)
	
	-- Rollout close event
	on RsLODVisControllerRoll close do
	(
		callbacks.removeScripts id:#visCtrlPreSave
		callbacks.removeScripts id:#visCtrlPreOpen
		
		-- Invoke LODController Plugin
		if (visControllerEnabled) do (
			LODVisController_Disable()
			redrawViews()
		)
	)
)