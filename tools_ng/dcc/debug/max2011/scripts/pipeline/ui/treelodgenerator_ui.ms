--
-- File:: pipeline/util/treelodgenerator.ms
-- Description:: Map Tree LOD generator functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 24 June 2009
--
-- HISTORY
-- Based off of the original "Tree Check and Lod" utility by Greg Smith and
-- Luke Openshaw.
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/treelodgenerator.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- rollout: RsTreeLodGeneratorRoll
-- desc: Tree LOD generator UI rollout.
--
rollout RsTreeLodGeneratorRoll "Tree LOD Generator"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/LOD_Toolkit#Tree_LOD_Generator" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	button btnGenerate 		"Generate for Selection" 	width:133 height:26
	button btnFixNormals	"Fix Normals" 				width:133 height:26 across:2
	button btnAdjustOrient	"Adjust Orientation" 		width:133 height:26
	group "Options:"
	(
		edittext edtLodFile		"LOD File:"
		button   btnSetLodFile	"Set..." align:#right
		edittext edtPrefix		"LOD Object Prefix:"
		checkbox chkHideAfter	"Hide after Generation" triState:1 checked:true
		checkbox chkCentrePivot "Centre Pivot" triState:1 checked:true
	)

	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None (see pipeline/util/treelodgenerator.ms)

	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------
	
	--
	-- event:
	-- desc:
	--
	on btnGenerate pressed do
	(
		if ( 0 == selection.count ) then
		(
			MessageBox "No objects selected to generate Tree LODs for."	
		)
		else
		(
			-- Create new undo context for easy undoing of Tree LOD generation.
			with undo label:"Tree LOD Generator" on
			(
				local filename = ( edtLodFile.text )
				local prefix = ( edtPrefix.text )
				local hideObjs = chkHideAfter.checked
				local centrePiv = chkCentrePivot.checked
				RsTreeLodGenerator.Generate selection filename prefix hideObjs centrePiv
			)
		)
	)
	
	--
	-- event: btnFixNormals button pressed
	-- desc:
	--
	on btnFixNormals pressed do
	(		
		if ( 0 == selection.count ) then
		(
			MessageBox "No objects selected to Fix Normals for."	
		)
		else
		(
			with undo label:"Fix Normals" on
			(
				local filename = ( edtLodFile.text )
				RsTreeLodGenerator.FixNormals selection filename
			)
		)		
	)
	
	--
	-- event: btnAdjustOrient button pressed 
	-- desc: 
	--
	on btnAdjustOrient pressed do
	(
		if ( 0 == selection.count ) then
		(
			MessageBox "No objects selected to Adjust Orientation for."	
		)
		else
		(
			with undo label:"Adjust Orientation" on
			(
				local filename = ( edtLodFile.text )
				RsTreeLodGenerator.AdjustOrientation selection filename
			)
		)	
	)
	
	--
	-- event:
	-- desc:
	--
	on btnSetLodFile pressed do
	(
		local filename = getOpenFileName caption:"Set Tree LOD filename:" \
							types:"3dsmax Files (*.max)|*.max"
		if ( undefined != filename ) then
		(
			edtLodFile.text = filename
			RsTreeLodGenerator.SaveFilename filename
		)
	)
	
	--
	-- event: rollout opened event
	-- desc: Load settings
	--
	on RsTreeLodGeneratorRoll open do
	(
		edtLodFile.text = RsTreeLodGenerator.LoadFilename()
		edtPrefix.text = RsTreeLodGenerator.LoadPrefix()
		chkHideAfter.checked = RsTreeLodGenerator.LoadHideAfter()
		chkCentrePivot.checked = RsTreeLodGenerator.LoadCentrePivot()
	)
	
	--
	-- event: rollout closed event
	-- desc: Save settings
	--
	on RsTreeLodGeneratorRoll close do
	(
		-- No need to save filename as thats saved when using the Set button.	
		RsTreeLodGenerator.SavePrefix edtPrefix.text
		RsTreeLodGenerator.SaveHideAfter chkHideAfter.checked
		RsTreeLodGenerator.SaveCentrePivot chkCentrePivot.checked
	)
)
-- pipeline/util/treelodgenerator.ms
