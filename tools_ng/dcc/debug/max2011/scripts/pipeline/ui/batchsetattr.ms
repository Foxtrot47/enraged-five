--
-- File:: pipeline/ui/batchsetattr.ms
-- Description::  Batch set attributes on objects
--
-----------------------------------------------------------------------------

(
	txdsInTheScene = #()

	rollout setattrroll "Parameters" width:162 height:314
	(
		fn batchsetattr class attr value = 
		(

			if attr == "" do
			(
				messagebox "Please enter an attribute name"
				return false
			)
	
			if selection.count == 0 do
			(
				messagebox "Please select some objects"
				return false
			)
--
-- 			two different possibilities if the class field hasn't been filled then check 
-- 			all attribute classes. Otherwise only change the attribute in the specified class
--
			if class != "" then
			(
				index = getattrindex class attr
				if index == undefined do
				(
					messagebox ("Attribute \""+attr+"\" is not a member of class \""+class+"\"")
					return false
				)
				
				type = getattrtype class index
				if type == undefined do
					return false
			
				for obj in selection do
				(
					objclass = getattrclass obj
					if objclass != class do
						continue

					if type == "string" then
						setattr obj index value
					else if type == "bool" then
						setattr obj index (value == "true")
					else
						setattr obj index (value as float)
				)
		
			)
			else
			(
				for obj in selection do
				(
					objclass = getattrclass obj
					if objclass == undefined do
						continue

					index = getattrindex objclass attr
					if index == undefined do
						continue
				
					type = getattrtype objclass index
					if type == undefined do
						continue
				
					if type == "string" then
						setattr obj index value
					else if type == "bool" then
 						setattr obj index (value == "true")
					else
						setattr obj index (value as float)
				)
			)
		)


-- UI

		button go "Set Selected" pos:[32,172] width:127 height:20
		combobox attrvalue "" pos:[52,85] width:99 height:5 items:#("") selection:1
		dropdownList attrclass "" pos:[52,25] width:102 height:21 items:#("Gta Collision", "Gta Time", "Gta Group", "Gta Light", "Gta Lookat", "Gta Object", "Gta Particle", "Gta Pickup", "Gta Queue", "Gta Xrefs", "Gta Zone", "GtaCullzone") selection:6		
		dropdownList attrname "" pos:[52,55] width:102 height:21 items:#("LOD distance", "TXD", "Surface Type", "Piece Type", "Do not fade", "Draw last", "Additive", "Is Subway", "Ignore Lighting", "No Zbuffer write", "Area Code", "Level Design Object", "No Shadows", "Generic", "Code Glass", "Artist Glass") selection:1
		label written "Written by Adam Fowler" pos:[36,205] width:112 height:13
		label dma "Modified by Scot Fraser, Corey Lake Rockstar Vancouver 2003" pos:[35,240] width:105 height:60
		label lbl5attrname "Attribute:" pos:[7,57] width:43 height:17
		label lbl6 "DMA Design 1999" pos:[36,220] width:87 height:13
		label lbl7 "Value:" pos:[18,93] width:32 height:14
		label lb18 "Class:" pos:[18,30] width:32 height:14
-- Events
		on go pressed do
		(
			if(attrclass.selected == "Gta Collision") then
			(
				batchsetattr attrclass.selected attrname.selected attrvalue.selection
			)
			else
			(
				batchsetattr attrclass.selected attrname.selected attrvalue.text
			)
		)
		on attrclass selected sel do
		(
			if sel == 1 do
			(		
				attrname.selection = 3
				attrvalue.items = #("TARMAC", "GRASS", "GRAVEL", "MUD", "PAVEMENT", "CAR", "GLASS", "TRANSPARENT CLOTH", "GARAGE DOOR")
				attrvalue.selection = 3
			)
		)
		on attrname selected sel do
		(
			if sel == 2 do
			(		
				attrclass.selection = 6
				GetTxdList txdsInTheScene
				attrvalue.items = txdsInTheScene
			)
		)
	)	
	try CloseRolloutFloater setattrutil catch ()
	setattrutil = newRolloutFloater "Set Attributes" 200 340 40 96
	addRollout setattrroll setattrutil
)