--
-- File:: VertexSnap.ms
-- Description:: Given a selection of meshes, snaps vertices on one object to any vertices on another object in the selection
--					  which are within the given tolerance.
--
-- Author:: Greg Smith (original)
-- Date:: 9 September 2004
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com> (Cleanup, moved main function over from GTA3)
-- Date:: 3 August 2009 
--
	
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	fn RnFilterSnapPick o = (

	cursel = #()
	cursel = cursel + selection

	retval = false

	if finditem cursel o == 0 then (
				retval = true
	)
	
	retval
	)
	
	fn SnapVertsByTol snapset tolerance = (
		
		-- For every object in the selection except the last one 
		-- (we assume this has been checked by the end)
		for i = 1 to (snapset.count - 1)do (
			otherObjects = deepCopy snapset
			deleteItem otherObjects i
			
			-- Get the number of verts, supporting multiple object types
			if (classof snapset[i] == Editable_Mesh) then (
				aNum = getNumVerts snapset[i]
			) else if (classof snapset[i] == Editable_Poly) then (
				aNum = snapset[i].getNumVertices()
			) else (
				messagebox "This class of object is not supported."
				return false
			)
			
			-- Find a match for each vert
			for aVIndex = 1 to aNum do (			
				-- Get the current vert, supporting multiple object types
				if (classof snapset[i] == Editable_Mesh) then (
					aVert = getVert snapset[i] aVIndex
				) else if (classof snapset[i] == Editable_Poly) then (
					aVert = snapset[i].getVertex( aVIndex )
				) else (
					messagebox "This class of object is not supported."
					return false
				)
					
				-- Check against the verts in other objects
				for bObj in otherObjects do (
					-- Get the number of verts, supporting multiple object types
					if (classof bObj == Editable_Mesh) then (
						bNum = getNumVerts bObj
					) else if (classof bObj == Editable_Poly) then (
						bNum = bObj.getNumVertices()
					) else (
						messagebox "This class of object is not supported."
						return false
					)
					
					for bVIndex = 1 to bNum do (
						-- Get the current vert, supporting multiple object types
						if (classof bObj == Editable_Mesh) then (
							bVert = getVert bObj bVIndex
						) else if (classof bObj == Editable_Poly) then (
							bVert = bObj.getVertex( bVIndex )
						) else (
							messagebox "This class of object is not supported."
							return false
						)
						
						 -- Do we have a match?
						if (((distance aVert bVert) < tolerance) and (bVert != aVert)) then (
							-- Move this vert to the match and skip to the next vert
							offset = [0, 0, 0]
							offset.x = bVert.x - aVert.x
							offset.y = bVert.y - aVert.y
							offset.z = bVert.z - aVert.z
							move snapset[i].verts[aVIndex] offset
							format "Found Match: % in % moved to %\n" aVert snapset[i] bVert
						)
					)
					
				) --for bObj in otherObjects
			) --for v = 1 to getNumVerts snapset[i]
		) --for obj in snapset
	)
