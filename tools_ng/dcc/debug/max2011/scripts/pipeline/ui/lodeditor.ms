--
-- File:: pipeline/ui/lodeditor.ms
-- Description:: LOD Editor
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
-- Date:: 18/9/2003
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/UtilityFunc.ms"
filein "pipeline\\util\\file.ms"
filein "rockstar\\export\\settings.ms"
filein "pipeline/ui/drawablelodeditor.ms"

-----------------------------------------------------------------------------
-- Global
-----------------------------------------------------------------------------
currentPickSelection = #()
currentLODChildNames = #()

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
fn changePickLODSelection = (
	GtaPickLodParentUtil.rollouts[1].updateSelection()
)

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- SCENE LOD EDITOR
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout GtaPickLodParentRoll "Scene LOD Editor"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/LOD_Editor#Scene_LOD_Editor" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	edittext currentItem "Current:" enabled:false
	edittext parentItem "Parent:" enabled:false
	button buttonSelect "Select Parent" across:2
	button buttonSelectChild "Select Children"
	listbox childrenItems "Children:"
	button buttonAdd "Pick Parent" across:2
	button buttonAddChildren "Pick Children"
	button buttonRemove "Remove Parent" across:2
	button buttonRemoveChildren "Remove Children"
	button buttonLoadFromExported "Load From Export"
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	fn updateSelection = (
		currentPickSelection = #()
	
		for obj in selection do (
			append currentPickSelection obj
		)	
		
		if currentPickSelection.count > 1 then (
			currentItem.text = "Multiple"
			parentItem.text = ""
			
			for selobj in currentPickSelection do (
					
				parnode = getlodattrparent selobj
				
				if parnode == undefined or isdeleted parnode == true then (
					parnodename = "undefined"
				) else (
					parnodename = parnode.name
				)		
				
				if parentItem.text != "Multiple" then (
					if parentItem.text == "" then (
						parentItem.text = parnodename
					) else (
						if parentItem.text != parnodename then (
							parentItem.text = "Multiple"
						)
					)
				)
			)
		) else (
			if currentPickSelection.count == 1 and currentPickSelection[1] != undefined then (
			
				currentItem.text = currentPickSelection[1].name
				parnode = getlodattrparent selection[1]

				if parnode != undefined then (
					if isdeleted parnode == true then (
						parnode = undefined 
					)
				)

				if parnode == undefined or isdeleted parnode == true then (
					parentItem.text = "None"
				) else (
					parentItem.text = parnode.name
				)

				lodChildren = getLODChildren currentPickSelection[1]

				currentLODChildNames = #()

				for lodChild in lodChildren do (
				
					if isdeleted lodChild == false then (
				
						append currentLODChildNames lodChild.name
					)
				)
				
			) else (
				currentItem.text = "None"
				parentItem.text = "None"
				currentPickSelection = #()
				currentLODChildNames = #()
			)
			
			childrenItems.items = currentLODChildNames
		)
	)
	
	fn pickCallback obj = (
	
		for pickObj in currentPickSelection do (
			if obj == pickObj then (
				return false
			)
		)
	
		return true
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on childrenItems doubleClicked numClicked do (
	
		newselect = getnodebyname childrenItems.items[numClicked]
		
		if newselect != undefined then (
			select newselect
		)
	)
	
	on buttonSelect pressed do (
		newselect = getnodebyname parentItem.text
		
		if newselect != undefined then (
			select newselect
		)
	)
	
	on buttonSelectChild pressed do (
	
		selObjs = #()
		
		for obj in currentPickSelection do (
		
			lodChildren = getLODChildren obj
			
			for childobj in lodChildren do (
			
				if isdeleted childobj == false then (
			
					append selObjs childobj
				)
			)
		)
		
		select selObjs
	)
	
	on buttonAdd pressed do (
			
		callbacks.removescripts id:#pickLODselect
		
		if currentPickSelection.count < 1 then (
		
			MessageBox "please select at least one object"
			callbacks.addscript #selectionSetChanged "GtaPickLodParentUtil.rollouts[1].updateSelection()" id:#pickLODselect
			return 0	
		)
				
		parent = pickObject filter:pickCallback
						
		if parent == undefined then (
					
			callbacks.addscript #selectionSetChanged "GtaPickLodParentUtil.rollouts[1].updateSelection()" id:#pickLODselect
			return 0
		)
			
		for selobj in currentPickSelection do (
		
			AddLodAttr selobj
			SetLodAttrParent selobj parent
		)	
		
		flashnodes #(parent)
		
		select currentPickSelection
		
		updateSelection()
		
		callbacks.addscript #selectionSetChanged "GtaPickLodParentUtil.rollouts[1].updateSelection()" id:#pickLODselect
	)	
	
	on buttonAddChildren pressed do (
	
		callbacks.removescripts id:#pickLODselect
		
		if currentPickSelection.count != 1 then (
		
			MessageBox "please select only one object to add children to"
			callbacks.addscript #selectionSetChanged "GtaPickLodParentUtil.rollouts[1].updateSelection()" id:#pickLODselect
		)
				
		parent = currentPickSelection[1]	
		selectlist = pickObject filter:pickCallback count:#multiple
									
		for selobj in selectlist do (
		
			AddLodAttr selobj
			SetLodAttrParent selobj parent
		)	
		
		flashnodes selectlist
		
		select parent
		
		updateSelection()
		
		callbacks.addscript #selectionSetChanged "GtaPickLodParentUtil.rollouts[1].updateSelection()" id:#pickLODselect
		
	)
	
	on buttonRemoveChildren pressed do (
	
		if selection.count < 1 then (
			MessageBox "please select at least one object"
		)
					
		for selobj in selection do (
			
			lodChildren = getLODChildren selobj
			
			for childobj in lodChildren do (			

				DelLodAttr childobj
			)
		)	
		
		updateSelection()		
	)
	
	on buttonRemove pressed do (
	
		if selection.count < 1 then (
			MessageBox "please select at least one object"
		)
					
		for selobj in selection do (
			DelLodAttr selobj
		)	
		
		updateSelection()
	)	
	
	fn getInstances filenameIPL instanceData = (
	
		fileMainIPL = openfile filenameIPL mode:"r"
		
		if fileMainIPL != undefined then (
		
			mode = #none
		
			while eof fileMainIPL == false do (

				lineMainIPL = readline fileMainIPL
				
				if lineMainIPL == "end" then (
				
					mode = #none
				) else if mode == #none then (
				
					if lineMainIPL == "inst" then mode = #inst
				) else if mode == #inst then (
				
					tokensMainIPL = filterstring lineMainIPL ", "
					
					append instanceData tokensMainIPL
				)
			)

			close fileMainIPL
		)		
	)
	
	fn doesVectorMatch check checkAgainst tolerance = (
	
		if check.x + tolerance < checkAgainst.x then return false
		if check.y + tolerance < checkAgainst.y then return false
		if check.z + tolerance < checkAgainst.z then return false			
		if check.x - tolerance > checkAgainst.x then return false
		if check.y - tolerance > checkAgainst.y then return false
		if check.z - tolerance > checkAgainst.z then return false
		
		return true
	)		

	fn findObjectByNameAndPos parentobj objname objpos = (
	
		for obj in parentobj.children do (
	
			nameMatch = false
	
			if classof obj == XRefObject then (
			
				if obj.objectname == objname then nameMatch = true
			) else if classof obj == InternalRef then (
			
				srcobj = ixref_gettarget obj
			
				if srcobj != undefined then (
			
					if srcobj.name == objname then nameMatch = true
				)
			) else (
			
				if obj.name == objname then nameMatch = true
			)
	
			if nameMatch then (
	
				if doesVectorMatch objpos obj.pos 0.001 then (
				
					return obj
				)
			)
		
			foundobj = findObjectByNameAndPos obj objname objpos
	
			if foundobj != undefined then return foundobj
		)
		
		return undefined
	)
	
	fn checkLodSetup instListCheck instListMain = (
	
		for inst in instListCheck do (
		
			instIndex = inst[10] as integer
		
			if instIndex != -1 then (
			
				lodinst = instListMain[instIndex + 1]
			
				obj = findObjectByNameAndPos rootnode inst[1] [inst[3] as float,inst[4] as float,inst[5] as float]
				lodobj = findObjectByNameAndPos rootnode lodinst[1] [lodinst[3] as float,lodinst[4] as float,lodinst[5] as float]
			
				if obj != undefined and lodobj != undefined then (
				
					dellodattr obj
					addlodattr obj
					setlodattrparent obj lodobj
				) else (
				
					if obj == undefined then (
				
						print ("not found: " + inst[1] + " at " + ([inst[3] as float,inst[4] as float,inst[5] as float] as string))
					) else (
						
						print ("not found: " + lodinst[1] + " at " + ([lodinst[3] as float,lodinst[4] as float,lodinst[5] as float] as string))
					)
				)
			)
		)		
	)
	
	on buttonLoadFromExported pressed do (
	
		RsMapSetupGlobals()

		filenameMainIPL = RsMakeBackSlashes(RsMapDir + RsMapName + ".ipl")
		filenamesStreamIPL = getfiles (RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/" + "*.ipl")
		
		instancesMain = #()
		
		getInstances filenameMainIPL instancesMain
		
		checkLodSetup instancesMain instancesMain
				
		for filenameStreamIPL in filenamesStreamIPL do (
			
			print filenameStreamIPL
			
			local instancesStream = #()
		
			getInstances filenameStreamIPL instancesStream
			
			checkLodSetup instancesStream instancesMain		
		)
	)
	
	on GtaPickLodParentRoll open do (
		updateSelection()
		callbacks.addscript #selectionSetChanged "GtaPickLodParentUtil.rollouts[1].updateSelection()" id:#pickLODselect
	)
	
	on GtaPickLodParentRoll close do (
		callbacks.removescripts id:#pickLODselect
	)
)

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- LOD VIEW
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout LodViewRoll "LOD View"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/LOD_Editor#LOD_View" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnSceneView "Scene LOD View" across:2
	button btnDrawableView "Drawable LOD View"
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnSceneView pressed do (
		ShowLODView()
	)
	
	on btnDrawableView pressed do (
		LodDrawable_Show()
	)
)

try CloseRolloutFloater GtaPickLodParentUtil catch()
GtaPickLodParentUtil = newRolloutFloater "LOD Editor" 250 767 50 96
addRollout GtaPickLodParentRoll GtaPickLodParentUtil
addRollout RsDrawableLodEditorRoll GtaPickLodParentUtil
addRollout LodViewRoll GtaPickLodParentUtil