--
-- File:: pipeline/ui/containers.ms
-- Description:: Container helper rollout utility.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 15 January 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
-- None

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
-- None

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsContainersRoll "Containers"
(
	---------------------------------------------------------------------------
	-- UI Widgets
	---------------------------------------------------------------------------
	group "Container Construction"
	(
		button btnCreateFromSel "Create from Selection" width:150
		button btnCreateFromFile "Create Inherited from File" width:150
	)

	group "Scene Containers"
	(
		button btnOpenSel	"Open Selection" width:150
		button btnCloseSel	"Close Selection" width:150
		button btnLoadSel		"Load Selection" width:150
		button btnUnloadSel	"Unload Selection" width:150
		button btnSaveSel		"Save Selection" width:150
		button btnReloadSel	"Reload Selection" width:150
	)
	
	---------------------------------------------------------------------------
	-- Event Handlers
	---------------------------------------------------------------------------

	--
	-- event: btnCreateFromSel pressed
	-- desc: Create a container for the currently selected objects.
	--
	on btnCreateFromSel pressed do
	(
		cont = ( Containers.CreateContainer selection )
		if ( undefined != cont ) then
			select cont
	)
	
	--
	-- event: btnCreateFromFile pressed
	-- desc: Create an inherited container from a MAXC file.
	--
	on btnCreateFromFile pressed do
	(
		cont = ( Containers.CreateInheritedContainer "" )
		if ( undefined != cont ) then
			select cont
	)
	
	--
	-- event: btnOpenSel pressed
	-- desc: Open selected containers.
	--
	on btnOpenSel pressed do
	(
		for o in selection do
		(
			if ( not Container == classof o ) then
				continue
			if ( o.unloaded ) then
				o.LoadContainer()
			if ( not o.IsOpen() ) then
				o.setOpen true
		)	
	)
	
	--
	-- event: btnCloseSel pressed
	-- desc: Close selected containers.
	--
	on btnCloseSel pressed do
	(
		for o in selection do
		(
			if ( not Container == classof o ) then
				continue
			if ( o.IsOpen() ) then
				o.setOpen false
		)	
	)	
	
	--
	-- event: btnCreateFromSel pressed
	-- desc: 
	--
	on btnLoadSel pressed do
	(
		for o in selection do
		(
			if ( not Container == classof o ) then
				continue
			if ( o.unloaded ) then
				o.LoadContainer()
		)
	)
	
	--
	-- event: btnCreateFromSel pressed
	-- desc: 
	--
	on btnUnloadSel pressed do
	(
		for o in selection do
		(
			if ( not Container == classof o ) then
				continue
			if ( not o.unloaded ) then
				o.UnloadContainer()
		)
	)
	
	--
	-- event: btnCreateFromSel pressed
	-- desc: Create a container for the currently selected objects.
	--
	on btnSaveSel pressed do
	(
		for o in selection do
		(
			if ( not Container == classof o ) then
				continue
			o.SaveContainer false
		)
	)
	
	--
	-- event: btnCreateFromSel pressed
	-- desc: Create a container for the currently selected objects.
	--
	on btnReloadSel pressed do
	(
		for o in selection do
		(
			if ( not Container == classof o ) then
				continue
			o.ReloadContainer()
		)
	)
)

try CloseRolloutFloater RsContainersUtil catch()
RsContainersUtil = newRolloutFloater "Containers" 200 300
addRollout RsContainersRoll RsContainersUtil

-- pipeline/ui/containers.ms
