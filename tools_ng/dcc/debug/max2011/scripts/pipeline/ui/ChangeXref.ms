--
-- File:: pipeline/ui/ChangeXref.ms
-- Description:: Allows the user to change the object name and max file that a selection of xref's
-- 						Point to with pattern matching on the names
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com>
-- Date:: 30/7/2003
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Function
-----------------------------------------------------------------------------
fn MHGetNewStringFromPattern stringIn patternIn patternOut = (
	
	--this isnt a perfect conversion but should cope with most
	--cases especially as it should only get here if it gets passed matchpattern
	--i hope there isnt a function already to do this...
	
	--get the bits of the string used as a filter
	patternInBits = filterstring patternIn "?*" 

	--now get the bits that will transfer through to the
	--output string (ie the bits not in the filter)
	passedThroughBits = #()	
	currentString = stringIn
	startstring = 1
	endstring = 1
	wildfirst = false
	
	--this gets us the bits of string that will fall through
	--with the wildcards
	for i = 1 to patternInBits.count do (
	
		pattBit = patternInBits[i]
		pattIndex = findstring currentString pattBit
		
		if i == 1 then (
			if pattIndex > 1 then (

				wildfirst = true

				passover = substring currentString 1 (pattIndex - 1)			
				append passedThroughBits passover
				
				currentString = substring currentString (pattIndex + pattBit.count) (currentString.count - pattIndex - pattBit.count + 1)
				startstring = 1
				--print currentString
			) else (
				startstring = pattIndex + pattBit.count
				--print startstring
			)
		) else (

			endstring = pattIndex - 1

			passover = substring currentString startstring (endstring - startstring + 1)
			append passedThroughBits passover
			
			currentString = substring currentString (endstring + 1) (currentString.count - endstring)
			startstring = pattIndex + pattBit.count - endstring
		)
	)
	
	if startstring <= currentString.count then (
		passover = substring currentString startstring (currentString.count - startstring + 1)		
		append passedThroughBits passover
	)

	--this constructs us the new string
	patternOutBits = filterstring patternOut "?*" 
	
	outString = ""
	
	for i = 1 to patternOutBits.count do (
	
		if wildfirst == true then (
			--pattBit = passedThroughBits[i]
			--outString = outString --+ pattBit

			pattBit = patternOutBits[i]	
			outString = outString + pattBit
		) else (
			pattBit = patternOutBits[i]
			outString = outString + pattBit
	
			--pattBit = passedThroughBits[i]
			--outString = outString --+ pattBit
		)
	)

--	print outString
	
	return outString
)

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout ChangeXrefs "Change Xrefs"
(
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Xref_Toolkit#Change_Xrefs" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	edittext oldobjecttext "Find Xref ObjectName:"
	edittext newobjecttext "Replace Xref ObjectName:"
	edittext filenametext "New Xref Filename:"
	button change "Change"
	
	on change pressed do (
	
		matchs = 0
		
		if selection.count == 0 then (
			messagebox "no objects selected"
			return 0
		)
		
		for obj in selection do (
				
			if classof(selection[1]) == XRefObject then (
				
				if matchpattern obj.objectname pattern:oldobjecttext.text then (
	
					-- if a new xref filename has been supplied then set it
					
					if filenametext.text != "" then (
						obj.filename = filenametext.text
					)
	
					if newobjecttext.text != "" then (
						obj.objectname = MHGetNewStringFromPattern obj.objectname oldobjecttext.text newobjecttext.text
					)
					
					matchs = matchs + 1
				)
			)
		)
		
		if matchs == 0 then (
			messagebox "no matches found"
		) else (
			messagebox ("found " + (matchs as string) + " matches")
		)
	)
)