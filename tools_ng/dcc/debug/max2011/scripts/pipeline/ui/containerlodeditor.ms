--
-- File:: pipeline/ui/containerlodeditor.ms
-- Description::
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 26 May 2010
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/objects.ms"
filein "pipeline/export/maps/globals.ms"
filein "pipeline/util/file.ms"

-----------------------------------------------------------------------------
-- .Net Initialisation
-----------------------------------------------------------------------------
dotNet.loadAssembly "System.Windows.Forms.dll"
dotNet.loadassembly "MaxCustomControls.dll"

dotNetColorClass = dotNetClass "System.Drawing.Color"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout ContainerLODEditorRoll "Container LOD Editor"
(
	-------------------------------------------------------------------------
	-- Locals
	-------------------------------------------------------------------------
	local attrLODForContainers = ( GetAttrIndex "Gta Object" "LOD For Containers" )
	local modified = false

	-------------------------------------------------------------------------
	-- UI Widgets
	-------------------------------------------------------------------------
	edittext				txtLevel			"Level:" enabled:false
	edittext 				txtObject 		"Object:" enabled:false
	edittext				txtCurrent		"Current:" enabled:false
	dotNetControl 	tvContainers 	"System.Windows.Forms.TreeView" Height:300 CheckBoxes:true
	button					btnRefresh		"Refresh" width:100 across:2
	button					btnApply			"Apply" width:100
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	
	--
	-- name: Init
	-- desc: Initialisation function.
	--
	fn Init = (
		
		local maps = RsMapGetMapContainers()
		if ( 0 == maps.count ) then
			messageBox "Invalid 3dsmax file loaded or no containers found."
		else
			RsMapSetupGlobals maps[1]
			
		txtLevel.text = RsMapLevel
		txtObject.text = "None"
	)
	
	--
	-- name: AddNodes
	-- desc:
	--
	fn AddNodes cnode tnode = (
	
		if ( ( ContentGroup == classof cnode ) or 
				 ( ContentLevel == classof cnode ) ) then
		(
			local nodeName = cnode.name
			if ( "" == nodeName ) then
				nodeName = RsRemovePath( cnode.path )
		
			tn = tnode.Nodes.Add( nodeName )
			for n in cnode.children do
			(
				if ( undefined != n ) then
				(	
					AddNodes n tn
				)
			)
		)
		else if ( ContentMapRpf == classof cnode ) then
		(
			if ( ( 1 == cnode.inputs.count ) and 
			     ( cnode.inputs[1].instances ) ) then
			(
				n = tnode.Nodes.Add( cnode.name )
				n.ForeColor = dotNetColorClass.Green
			)
		)
	)
	
	--
	-- name: RefreshTreeView
	-- desc: Refresh our tree view control for our currently selected object.
	--
	fn RefreshTreeView = (
	
		-- Populate tree view with nodes for our entire level map nodes (non-generic).
		local levelRoot = RsProjectContentFind RsMapLevel type:"level"
		tvContainers.CheckBoxes = true
		tvContainers.BeginUpdate()
		tvContainers.Nodes.Clear()
		for cnode in levelRoot.children do
		(
			if ( undefined != cnode ) then
			(
				AddNodes cnode tvContainers
			)
		)
		tvContainers.EndUpdate()
	)


	--
	-- name: GetCheckedContainers
	-- desc: Return array of checked nodes from the specified root.
	--
	fn GetCheckedContainers root = (
	
		local conts = #()
		local index = 0
		
		for index = 0 to (root.Nodes.Count - 1) do
		(	
			local cnode = ( root.Nodes.Item[index] )
			if ( cnode.Checked ) then
				append conts ( tolower cnode.Text )
			
			conts += ( GetCheckedContainers cnode )
		)
		conts
	)

	--
	-- name: SetCheckedContainers
	-- desc: Set checked nodes from the specified root.
	--
	fn SetCheckedContainers root conts = (
	
		--format "Root: %; Conts: %\n" root.Text conts
	
		for index = 0 to (root.Nodes.Count - 1) do
		(
			local cnode = (root.Nodes.Item[index])
			if ( 0 != findItem conts ( tolower cnode.Text ) ) then
				cnode.Checked = true
			else
				cnode.Checked = false

			SetCheckedContainers cnode conts
		)
	)
	
	--
	-- name: UpdateForSelectionChange
	-- desc: Update our UI for a selection change.
	--
	fn UpdateForSelectionChange = (
	
		if ( 0 == $selection.count ) then
		(		
			txtObject.text = "None"
			txtCurrent.text = "None"
			btnApply.enabled = false
		)
		else
		(
			if ( ( 1 == $selection.count ) and ( "Gta Object" == GetAttrClass $selection[1] ) )then
			(
				local attrValue = ( GetAttr $selection[1] attrLODForContainers )
				local attrConts = ( filterString attrValue "," )
				print attrConts
				
				txtObject.text = $selection[1].name
				txtCurrent.text = attrValue
				btnApply.enabled = true
				
				-- Set tree check state				
				SetCheckedContainers tvContainers attrConts
			)
			else
			(
				txtObject.text = "None or invalid selection"
				txtCurrent.text = "None"
				btnApply.enabled = false
			)
		)
	)

	--
	-- name: ApplyChanges
	-- desc:
	--
	fn ApplyChanges = (
		-- Loop through the TreeView nodes constructing an array of container
		-- names checked.  Then sort and set our attribute value.
				
		local conts = ( GetCheckedContainers tvContainers )
		sort conts
		format "AttrValue: %\n" conts
		
		local attrValue = ""
		for index = 1 to conts.count do
		(
			if ( 1 == index ) then
				attrValue = conts[index]
			else
				attrValue = attrValue + "," + conts[index]
		)
		SetAttr $selection[1] attrLODForContainers attrValue
		txtCurrent.text = ( GetAttr $selection[1] attrLODForContainers )
		
		modified = false
	)

	-------------------------------------------------------------------------
	-- .Net Control Event Handler Functions
	-------------------------------------------------------------------------
	
	--
	-- event: AfterCheck event for .Net TreeView control
	-- desc: Set our modified state.
	--
	on tvContainers AfterCheck arg do
	(
		modified = true
	)
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------
	
	--
	-- event: Refresh pressed
	-- desc: Refresh our Tree View control.
	--
	on btnRefresh pressed do
	(
		Init()
		RefreshTreeView()
		UpdateForSelectionChange()
	)
	
	--
	-- event: Apply pressed
	-- desc: Refresh current selected object's "LOD For Containers" attribute.
	--
	on btnApply pressed do
	(
		ApplyChanges()
	)
	
	--
	-- event: Rollout open
	-- desc: Refresh our tree view for our current selection and register
	--       a callback for changes in selection.
	--
	on ContainerLODEditorRoll open do
	(
		Init()
		RefreshTreeView()
		UpdateForSelectionChange()
		callbacks.addScript #selectionSetChanged "ContainerLODEditorUtil.rollouts[1].UpdateForSelectionChange()" id:#containerLODEditorSelChange
	)
	
	--
	-- event: Rollout oktoclose
	-- desc: Callback for when user requests rollout to close, if we our modified
	--       state is set then we prompt the user asking if changes should be
	--       applied prior to closing.
	--
	on ContainerLODEditorRoll oktoclose do
	(
		if ( modified ) then
		(
			case ( yesNoCancelBox "Do you want to apply your changes?" ) of
			(
				#yes:
					applyChanges()
				#no:
					true
				#cancel:
					false
			)
		)
		else
		(
			-- Close no worries.
			true
		)
	)
	
	--
	-- event: Rollout close
	-- desc: Unregister our selection change callback.
	--
	on ContainerLODEditorRoll close do
	(
		callbacks.removeScripts #selectionSetChanged id:#containerLODEditorSelChange
	)
)

-----------------------------------------------------------------------------
-- Entry
-----------------------------------------------------------------------------
try CloseRolloutFloater ContainerLODEditorUtil catch()
ContainerLODEditorUtil = newRolloutFloater "Container LOD Editor" 300 435 50 96
addRollout ContainerLODEditorRoll ContainerLODEditorUtil

-- pipeline/ui/containerlodeditor.ms
