--
-- File:: drawablelodeditor.ms
-- Description:: RAGE Drawable LOD Editor
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 21 April 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/drawablelod.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
-- None

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsDrawableLodEditorRoll "Drawable LOD Editor"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/LOD_Editor#Drawable_LOD_Editor" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	edittext 	currentItem 	"Current:" enabled:false
	
	group		"Higher Detail Object:" (
		edittext 	txtHDItem 		"" enabled:false
		button 		btnSelectHD		"Select" across:3
		button 		btnPickHD		"Pick"
		button		btnRemoveHD 	"Remove"
	)
	
	group		"Lower Detail Object:" (
		edittext	txtLDItem		"" enabled:false
		button 		btnSelectLD 	"Select" across:3
		button		btnPickLD 		"Pick"
		button		btnRemoveLD		"Remove"	
	)
	
	group 	"Tools:" (
		button		btnExpandInY	"Expand Y-Axis" width:100
		button		btnExpandInX	"Expand X-Axis" width:100
		button		btnCollapse		"Collapse" width:100
	)
	
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	--
	-- name: updateSelection
	-- desc: Function triggered when the selection changes and this rollout is open.
	--
	fn updateSelection = (
		local currentPickSelection = #()
	
		for obj in selection do (
			append currentPickSelection obj
		)	
		
		if ( currentPickSelection.count == 1 and currentPickSelection[1] != undefined ) then 
		(
			currentItem.text = currentPickSelection[1].name

			local hdmodel = RsLodDrawable_GetHigherDetailModel currentPickSelection[1]
			if ( undefined != hdmodel ) then
				txtHDItem.text = ( hdmodel.name )
			else
				txtHDItem.text = "None"

			local ldmodel = RsLodDrawable_GetLowerDetailModel currentPickSelection[1]
			if ( undefined != ldmodel ) then
				txtLDItem.text = ( ldmodel.name )
			else
				txtLDItem.text = "None"
			
			local expandEnabled = ( undefined != hdmodel or undefined != ldmodel )
			btnExpandInY.enabled = expandEnabled
			btnExpandInX.enabled = expandEnabled
			btnCollapse.enabled = expandEnabled
		) 
		else 
		(
			currentItem.text = "Invalid"
			txtHDItem.text = "None"
			txtLDItem.text = "None"
			btnExpandInY.enabled = false
			btnExpandInX.enabled = false
			btnCollapse.enabled = false
		)
	)
	
	--
	-- name: pickCallback
	-- desc: Function triggered when picking for a new node.
	--
	fn pickCallback obj = (
	
		if ( obj == selection[1] ) then
			return ( false )
		if ( "Gta Object" != ( GetAttrClass obj ) ) then
			return ( false )
			
		return ( true )
	)
	
	--
	-- name: expandHierarchy
	-- desc: Expands a drawable lod hierarchy in the x or y axis.
	--
	fn expandHierarchy obj axis:#xaxis = (
	
		local lowestDetailObj = obj
		while ( RsLodDrawable_HasLowerDetailModel lowestDetailObj ) do
			lowestDetailObj = ( RsLodDrawable_GetLowerDetailModel lowestDetailObj )
		
		format "Lowest detail model: %\n" lowestDetailObj.name
		
		local nextDetailObj = ( RsLodDrawable_GetHigherDetailModel lowestDetailObj )
		local size = ( lowestDetailObj.max - lowestDetailObj.min )
		local newPos = nextDetailObj.pos
		while ( undefined != nextDetailObj ) do
		(			
			if ( #xaxis == axis ) then
			(
				newPos.x = newPos.x + size.x + 0.5
			)
			else if ( #yaxis == axis ) then
			(
				newPos.y = newPos.y + size.y + 0.5
			)
			nextDetailObj.pos = newPos
			
			nextDetailObj = ( RsLodDrawable_GetHigherDetailModel nextDetailObj )
		)
	)
	
	--
	-- name: collapseHierarchy
	-- desc: Collapses a drawable lod hierarchy aligning the pivot points.
	--
	fn collapseHierarchy obj = (
	
		local lowestDetailObj = obj
		while ( RsLodDrawable_HasLowerDetailModel lowestDetailObj ) do
			lowestDetailObj = ( RsLodDrawable_GetLowerDetailModel lowestDetailObj )
			
		format "Lowest detail model: %\n" lowestDetailObj.name
		local nextDetailObj = ( RsLodDrawable_GetHigherDetailModel lowestDetailObj )
		while ( undefined != nextDetailObj ) do
		(
			nextDetailObj.pos = lowestDetailObj.pos
			nextDetailObj = ( RsLodDrawable_GetHigherDetailModel nextDetailObj )
		)
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	--
	-- event: btnSelectHD button pressed
	-- desc: Select the currently set Higher Detail node.
	--
	on btnSelectHD pressed do
	(
		local toSel = ( RsLodDrawable_GetHigherDetailModel selection[1] )
		if ( undefined != toSel ) then
			select toSel
	)
	
	--
	-- event: btnPickHD button pressed.
	-- desc: Pick a new Higher Detail node.
	--
	on btnPickHD pressed do
	(
		local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
		local selobj = selection[1]
		callbacks.removescripts id:#DrawableLodEditorSelect
		
		local selHD = pickObject count:1 filter:pickCallback
		if ( undefined != selHD ) then
		(		
			RsLodDrawable_SetLowerDetailModel selHD selobj

			select selObj
			updateSelection()
		)
		callbacks.addscript #selectionSetChanged "GtaPickLodParentUtil.rollouts[2].updateSelection()" id:#DrawableLodEditorSelect
	)
	
	--
	-- event: btnRemoveHD button pressed
	-- desc: Remove the Higher Detail node.
	--
	on btnRemoveHD pressed do
	(
		local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
		local selobj = selection[1]
		callbacks.removescripts id:#DrawableLodEditorSelect
		
		local selHD = ( RsLodDrawable_GetHigherDetailModel selobj )
		if ( undefined != selHD ) then
		(
			LodDrawable_Delete selHD		
			select selobj
			updateSelection()
		)
		callbacks.addscript #selectionSetChanged "GtaPickLodParentUtil.rollouts[2].updateSelection()" id:#DrawableLodEditorSelect
	)
	
	--
	-- event: btnSelectLD button pressed
	-- desc: Select the currently set Lower Detail node.
	--
	on btnSelectLD pressed do
	(
		local toSel = ( RsLodDrawable_GetLowerDetailModel selection[1] )
		if ( undefined != toSel ) then
			select toSel
	)
	
	--
	-- event: btnPickLD button pressed
	-- desc: Pick a new Lower Detail node.
	--
	on btnPickLD pressed do
	(
		local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
		local selobj = selection[1]
		callbacks.removescripts id:#DrawableLodEditorSelect
		
		local selLD = pickObject count:1 filter:pickCallback
		if ( undefined != selLD ) then
		(		
			RsLodDrawable_SetLowerDetailModel selobj selLD

			select selObj
			updateSelection()
		)
		callbacks.addscript #selectionSetChanged "GtaPickLodParentUtil.rollouts[2].updateSelection()" id:#DrawableLodEditorSelect
	)
	
	--
	-- event: btnRemoveLD button pressed
	-- desc: Remove the Lower Detail node.
	--
	on btnRemoveLD pressed do
	(
		local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
		local selobj = selection[1]
		callbacks.removescripts id:#DrawableLodEditorSelect
		
		local selLD = ( RsLodDrawable_GetLowerDetailModel selobj )
		if ( undefined != selLD ) then
		(
			LodDrawable_Delete selobj
		
			select selobj
			updateSelection()
		)
		callbacks.addscript #selectionSetChanged "GtaPickLodParentUtil.rollouts[2].updateSelection()" id:#DrawableLodEditorSelect
	)
	
	--
	-- event: btnExpandInY button pressed
	-- desc: Expand drawable hierarchy along y-axis.
	--
	on btnExpandInY	pressed do
	(
		local selobj = selection[1]
		expandHierarchy selobj axis:#yaxis
	)
	
	--
	-- event: btnExpandInX button pressed
	-- desc: Expand drawable hierarchy along x-axis.
	--
	on btnExpandInX	pressed do
	(
		local selobj = selection[1]
		expandHierarchy selobj axis:#xaxis
	)
	
	--
	-- name: btnCollapse button pressed
	-- desc: Collapse drawable hierarchy.
	--
	on btnCollapse pressed do
	(
		local selobj = selection[1]
		collapseHierarchy selobj
	)
	
	--
	-- event: Rollout open event.
	-- desc: Add the selection change handler.
	--
	on RsDrawableLodEditorRoll open do (
		updateSelection()
		callbacks.addscript #selectionSetChanged "GtaPickLodParentUtil.rollouts[2].updateSelection()" id:#DrawableLodEditorSelect
	)
	
	--
	-- event: Rollout close event.
	-- desc: Remove the selection change handler.
	--
	on RsDrawableLodEditorRoll close do (
		callbacks.removescripts id:#DrawableLodEditorSelect
	)
)

-- drawablelodeditor.ms
