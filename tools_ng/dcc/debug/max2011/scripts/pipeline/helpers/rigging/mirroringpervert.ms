--Author: Mike Wilson
--Date: 26/03/2010

rollout mirroringRollout "Mirroring Per-Vert" width:196 height:250
(
	struct bonee( boneID, boneName, weight )
	struct vert( bones, selected )
	
	local vertArray = #()
	local boneList = #()
	local skinMod = undefined
	
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Vertex_Mirroring_Tool" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	editText edtTolerance "" pos:[32,85] width:124 height:23 enabled:false
	button btnPreview "Preview" pos:[26,140] width:71 height:30
	button btnGO "GO" pos:[95,140] width:71 height:30
	button btnCopy "Copy" pos:[26,170] width:71 height:30
	button btnPaste "Paste" pos:[95,170] width:71 height:30
	slider sldTolerance "Tolerance" pos:[28,20] width:150 height:44 range:[0,100,35.365]
	progressBar pbProgress "ProgressBar" pos:[18,230] width:164 height:14
	
	local selectedVertCount = 0
	local count = 1
	local previousRunWasPreview = false

	local selectedVertListFromPreview = #()
	
	local copiedVerts = #()
	
	-- validate if the current node is valid and has a skin modifier applied and is selected
	fn Validate = 
	(
		if($ != undefined) then
		(
			skinMod = $.modifiers["skin"]
			if(skinMod != undefined) then
			(
				try
				(
					-- skinops can only be used if the modifier is selected
					skinOps.GetBoneName skinMod 1 0
				)
				catch
				(
					messageBox "Skin modifier must be selected." title:"Warning"
					return false
				)

				return true
			)
		)
		
		messageBox "No skin modifier applied or is not selected." title:"Warning"
		return false
	)
	
	fn Init = 
	(
		vertArray.count = 0
		boneList.count = 0
		pbProgress.value = 0
		selectedVertCount = 0
		count = 1
		edtTolerance.text = ((sldTolerance.value * 0.0001) as String)
	)
	
	on mirroringRollout open do
	(
		Init()
	)
	
	-- build a bone list which we use to lookup any mirrored bones
	fn BuildBoneList = 
	(
		for i = 1 to skinOps.GetNumberBones skinMod do
		(
			b = bonee()
			b.boneID = i
			b.boneName = skinOps.GetBoneName skinMod i 0
			append boneList b
		)	
	)
	
	-- build a vertex list with each vertex having a list of bones weighted to it
	fn BuildVertList = 
	(
		for i = 1 to (skinOps.GetNumberVertices skinMod) do
		(
			v = vert()
			v.selected = (skinOps.IsVertexSelected skinMod i)
			v.bones = #()
			
			if(v.selected == 1) then ( selectedVertCount += 1 )
			
			for j = 1 to (skinOps.GetVertexWeightCount skinMod i) do
			(
				b = bonee()
				b.boneID = (skinOps.GetVertexWeightBoneID skinMod i j)
				b.boneName = (skinOps.GetBoneName skinMod b.boneID 0)
				b.weight = (skinOps.GetVertexWeight skinMod i j)
				
				append v.bones b
			)
			
			append vertArray v
		)
		
		if(vertArray.count != $.Vertices.count) then
		(
			messageBox "Mesh vert count does not match skin modifier vert count." title:"Warning"
			return false
		)
	)
	
	-- check if two values are nearly equal based on a tolerance as vertices wont be equal
	fn AreNearlyEqual x y = 
	(
		return Abs(x - y) <= (sldTolerance.value * 0.0001)
	)
	
	-- reset the bone weights on a vertex, if the vertex has being weighted to a bone previously the normalisation will knacker up our
	-- weightings we apply later. to fix this we weight the bone fully to the root bone (index 1) and then unnormalize it and remove the weight
	-- so the vertex is completely unweighted.
	fn ResetVertexWeights vertexID = 
	(
		if(boneList.count > 1) then
		(
			print "done"
			--skinOps.unNormalizeVertex skinMod vertexID true
			--skinOps.SetVertexWeights skinMod vertexID 1 1.0
			--skinOps.unNormalizeVertex skinMod vertexID false
			--skinOps.SetVertexWeights skinMod vertexID 1 0.0
			
			skinOps.unNormalizeVertex skinMod vertexID false
			skinOps.SetVertexWeights skinMod vertexID 1 1.0
			skinOps.unNormalizeVertex skinMod vertexID true
			--skinOps.unNormalizeVertex skinMod vertexID false
			skinOps.SetVertexWeights skinMod vertexID 1 0.0
		)
	)
	
	fn ProcessVerts = 
	(
		if(selectedVertCount == 0) then
		(
			messageBox "No vertices selected." title:"Warning"
			return false
		)
		
		for i = 1 to vertArray.count do
		(
			if(vertArray[i].selected == 1) then
			(
				-- get the vert position and invert it so its the mirror, x is 0 based
				vertPosition = $.Vertices[i].pos
				vertPosition.x = (vertPosition.x * -1)
				
				-- look through all the verts in the model for a match
				for j = 1 to $.Vertices.count do
				(
					if((AreNearlyEqual vertPosition.x $.Vertices[j].pos.x == true) and
					(AreNearlyEqual vertPosition.y $.Vertices[j].pos.y == true) and
					(AreNearlyEqual vertPosition.z $.Vertices[j].pos.z== true) ) then
					(
						if(i == j) then
						(
							continue
						)				
						
						ResetVertexWeights j
						
						for k = 1 to vertArray[i].bones.count do
						(
							-- if the bone is a right sided bone based on naming _R_
							index = findString vertArray[i].bones[k].boneName "_R_"	
							if(index != undefined) then
							(
								-- the bone attached to the vert is specified as being a right side bone so find the left side
								boneToFind = replace vertArray[i].bones[k].boneName index 3 "_L_"
								
								for l = 1 to boneList.count do
								(
									if(boneList[l].boneName == boneToFind) then
									(
										skinOps.SetVertexWeights skinMod j boneList[l].boneID vertArray[i].bones[k].weight
									)
								)
								continue
							)
							
							-- if the bone is a left sided bone based on naming _L_
							index = findString vertArray[i].bones[k].boneName "_L_"	
							if(index != undefined) then
							(
								-- the bone attached to the vert is specified as being a left side bone so find the right side
								boneToFind = replace vertArray[i].bones[k].boneName index 3 "_R_"
								
								for l = 1 to boneList.count do
								(
									if(boneList[l].boneName == boneToFind) then
									(
										skinOps.SetVertexWeights skinMod j boneList[l].boneID vertArray[i].bones[k].weight
									)
								)
								continue
							)		
		
							skinOps.SetVertexWeights skinMod j vertArray[i].bones[k].boneID vertArray[i].bones[k].weight		
						)
						
						break
					)
				)
				
				pbProgress.value = 100.*count/selectedVertCount
				count += 1
			)
		)
	)

	fn PreviewProcess = 
	(
		if(selectedVertCount == 0) then
		(
			messageBox "No vertices selected." title:"Warning"
			return false
		)
		
		previousRunWasPreview = true

		selectedVertArray = #()
		
		for i = 1 to vertArray.count do
		(
			if(vertArray[i].selected == 1) then
			(
				append selectedVertListFromPreview i
				
				-- get the vert position and invert it so its the mirror, x is 0 based
				vertPosition = $.Vertices[i].pos
				vertPosition.x = (vertPosition.x * -1)
				
				-- look through all the verts in the model for a match
				for j = 1 to $.Vertices.count do
				(
					if((AreNearlyEqual vertPosition.x $.Vertices[j].pos.x == true) and
					(AreNearlyEqual vertPosition.y $.Vertices[j].pos.y == true) and
					(AreNearlyEqual vertPosition.z $.Vertices[j].pos.z== true) ) then
					(
						if(i == j) then
						(
							continue
						)

						append selectedVertArray j				
						
					)
				)

				pbProgress.value = 100.*count/selectedVertCount
				count += 1
			)
		)

		skinOps.SelectVertices skinMod selectedVertArray
	)
	
	fn CopyVert =
	(
		copiedVerts.count = 0
		
		for i = 1 to vertArray.count do
		(
			if(vertArray[i].selected == 1) then
			(
				--print (i as string)
				append copiedVerts i
				break;
			)
		)
	)
	
	fn PasteVert = 
	(
		for i = 1 to (skinOps.GetNumberVertices skinMod) do
		(
			if(skinOps.IsVertexSelected skinMod i == 1) then
			(
				ResetVertexWeights i
				
				for j = 1 to vertArray.count do
				(
					if(vertArray[j].selected == 1) then
					(
						for m = 1 to copiedVerts.count do
						(
							if(j == copiedVerts[m]) then
							(
								for k = 1 to vertArray[j].bones.count do
								(
									-- if the bone is a right sided bone based on naming _R_
									index = findString vertArray[j].bones[k].boneName "_R_"	
									if(index != undefined) then
									(
										-- the bone attached to the vert is specified as being a right side bone so find the left side
										boneToFind = replace vertArray[j].bones[k].boneName index 3 "_L_"
											
										for l = 1 to boneList.count do
										(
											if(boneList[l].boneName == boneToFind) then
											(
												print (vertArray[j].bones[k].weight as string)
												skinOps.SetVertexWeights skinMod i boneList[l].boneID vertArray[j].bones[k].weight
											)
										)
										continue
									)
										
									-- if the bone is a left sided bone based on naming _L_
									index = findString vertArray[j].bones[k].boneName "_L_"	
									if(index != undefined) then
									(
										-- the bone attached to the vert is specified as being a left side bone so find the right side
										boneToFind = replace vertArray[j].bones[k].boneName index 3 "_R_"
											
										for l = 1 to boneList.count do
										(
											if(boneList[l].boneName == boneToFind) then
											(
												print (vertArray[j].bones[k].weight as string)
												skinOps.SetVertexWeights skinMod i boneList[l].boneID vertArray[j].bones[k].weight
											)
										)
										continue
									)		
					
									print (vertArray[j].bones[k].weight as string)
									skinOps.SetVertexWeights skinMod i vertArray[j].bones[k].boneID vertArray[j].bones[k].weight
								)
							)
						) 
						
						break		
					)	
				)
				break
			)
		)
	)
	
	fn Process =
	(
		if(Validate() == false) then ( return false )
		
		if(previousRunWasPreview == true) then
		(
			skinOps.SelectVertices skinMod selectedVertListFromPreview
			selectedVertListFromPreview.count = 0
		)
		
		Init()
		BuildVertList()
		BuildBoneList()
		ProcessVerts()
		
		previousRunWasPreview=false
	)
	
	on sldTolerance changed x do
	(
		edtTolerance.text = ((x * 0.0001) as String)
	)
	
	on btnGO pressed do 
	(
		Process()
	)

	on btnPreview pressed do 
	(
		if(Validate() == false) then ( return false )
		
		Init()
		BuildVertList()
		BuildBoneList()
		PreviewProcess()		
	)
	
	on btnCopy pressed do 
	(
		if(Validate() == false) then ( return false )
		
		Init()
		BuildVertList()
		BuildBoneList()
			
		CopyVert()		
	)
	
	on btnPaste pressed do 
	(
		PasteVert()		
	)
)

createDialog mirroringRollout

