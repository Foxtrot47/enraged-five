fn bakeAnimToPoints model =
(
	if undefined==model or Array==(classof model) then
	(
		messagebox "Select only one model of the animated hirarchy."
		return false
	)
	
	if undefined!=RsStatedAnimRollout and RsStatedAnimRollout.open then 
		RsStatedAnimRollout.enableCallbacks false
	
	local groupIdx = getattrindex "Gta Object" "groupName"
	local stateIdx = getattrindex "Gta Object" "animState"
	local txdIdx = getattrindex "Gta Object" "TXD"
	local dynIdx = getattrindex "Gta Object" "Is Dynamic"
	local animIdx = getattrindex "Gta Object" "Has Anim"
	local rootModel = model
	local originalMaterial = rootModel.material
	while undefined!=rootModel.parent do rootModel = rootModel.parent

	progressStart "Checking materials"
	local childindex = 1
	local childCount = rootModel.children.count
	for thechild in rootModel.children do
	(
		local percent = (((childindex as float)/ childCount)*100)
		if originalMaterial!=thechild.material then
		(
			if (queryBox ("Material on "+theChild.name+" is not the same as on the root node. Abort?")) then
			(
				progressend()
				return false
			)
		)
		childindex = childindex+1
		progressupdate percent
	)
	progressend()

	if undefined!=(getNodeByName "rig_root") then delete (getNodeByName "rig_root")
	local rootRig = Box name:"rig_root" pos:rootModel.pos width:1 length:1 height:1
	converttomesh rootRig
	setAttr rootRig txdIdx (getAttr rootModel txdIdx)
	setAttr rootRig dynIdx true
	setAttr rootRig animIdx true
	if undefined!=(getNodeByName "root_bone") then delete (getNodeByName "root_bone")
	local rootBone = Box name:"root_bone" pos:rootModel.pos width:0.5 length:0.5 height:0.5
	converttomesh rootBone
	setAttr rootBone txdIdx (getAttr rootModel txdIdx)
	setAttr rootBone dynIdx true
	setAttr rootBone animIdx true
	append rootRig.children rootBone
	local compoundMesh = Editable_mesh name:rootModel.name pos:rootModel.pos
	compoundMesh.material = undefined
	local vertindeces = #()

	progressStart "baking"
	print ("children:"+rootModel.children.count as string)

	local childindex = 1
	local childCount = rootModel.children.count
	for thechild in rootModel.children do
	(
		local percent = (((childindex as float)/ childCount)*100)
		childindex = childindex+1
		--local thechild = rootModel.children[childindex]
		thechild.material = undefined

		-- baking animation onto point helpers
		local thename = ("bone_"+thechild.name)--(substring child.name 1 5)+(childindex as string))
		local formerBones = for o in objects where o.name==thename collect o
		delete formerBones
		local newBone = (box width:1 length:1 height:1 name:thename)
		newBone.pivot = newBone.center
		converttomesh newBone
		setAttr newBone txdIdx (getAttr rootModel txdIdx)
		setAttr rootBone dynIdx true
		append rootBone.children newBone
		if thechild.rotation.isanimated or thechild.pos.isanimated then
		(
			thechild.rotation.controller = TCB_Rotation()
			newBone.rotation.controller = TCB_Rotation()
			local childRotationKeys = thechild.rotation.controller.keys
			local boneRotationKeys = newBone.rotation.controller.keys
			for theKey in childRotationKeys do
			(
				appendKey boneRotationKeys theKey
			)

			newBone.pos.x_position.controller = (classof thechild.pos.x_position.controller)()
			newBone.pos.y_position.controller = (classof thechild.pos.y_position.controller)()
			newBone.pos.z_position.controller = (classof thechild.pos.z_position.controller)()
			for theKey in thechild.pos.x_position.controller.keys do
				appendKey newBone.pos.x_position.controller.keys theKey
			for theKey in thechild.pos.y_position.controller.keys do
				appendKey newBone.pos.y_position.controller.keys theKey
			for theKey in thechild.pos.z_position.controller.keys do
				appendKey newBone.pos.z_position.controller.keys theKey
		)

		-- actach to mesh and save off
		--local childCopy = copy thechild
		thechild.material = undefined
		--try(
			attach compoundMesh thechild
		--)catch(print (getCurrentException()))
		local currVertCount = getnumverts compoundMesh
		append vertindeces currVertCount

		progressUpdate percent
	)
	progressEnd()
	setAttr compoundMesh groupIdx (getAttr rootModel groupIdx)
	setAttr compoundMesh txdIdx (getAttr rootModel txdIdx)
	setAttr compoundMesh stateIdx "Animation"
	setAttr compoundMesh dynIdx true
	delete rootModel
	
	local theSkin = Skin()
	theSkin.mirrorEnabled = off
	compoundMesh.material = originalMaterial
	print compoundMesh.material
	select compoundMesh
	setCommandPanelTaskMode #modify
	addModifier compoundMesh (theSkin)
	update compoundMesh
	
	--progressStart "skinning" SKINOPS DOESNT LIKE THIS
	local lastIndex = 1
	for boneIndex=1 to vertindeces.count do
	(
		--local percent = 1 --(((boneIndex as float)/ vertindeces.count)*100)
		-- current array entry is highest vertindex
		--print rootBone.children[boneIndex]
		skinOps.addbone theSkin rootBone.children[boneIndex] 0
		
		if compoundMesh.numverts != vertindeces[vertindeces.count] then 
		(
			messagebox ("not equal vert numbers in meshes: "+ compoundMesh.numverts as string +","+ vertindeces[vertindeces.count] as string)
			return false
		)
		
 		for i=lastIndex to vertindeces[boneIndex] do
		(
 			skinOps.replacevertexweights theSkin i boneIndex 1.0
		)
--		print (lastIndex as string+", "+vertindeces[boneIndex] as string)
		lastIndex = vertindeces[boneIndex]+1
	)
	skinOps.addbone theSkin rootBone 0
	
	gRsStatedAnimTool.Deinit()
	if undefined!=RsStatedAnimRollout and RsStatedAnimRollout.open then
	(
		RsStatedAnimRollout.enableCallbacks true
	)
)