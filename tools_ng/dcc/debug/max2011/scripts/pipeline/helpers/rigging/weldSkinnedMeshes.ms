global gSkinWeldProgresssDialog = undefined
global allDueProcesses = 0
global processed = 0

fn increaseProgress = 
(
	processed = processed + 1
	if undefined==gSkinWeldProgresssDialog then return false
	try(
		gSkinWeldProgresssDialog.Controls.Item[2].value = ((processed as float)/allDueProcesses)*100
	)catch()
	gSkinWeldProgresssDialog.Focus()
	gSkinWeldProgresssDialog.Refresh()
--		print (processed as string+", "+allDueProcesses as string)
)

fn stopButtonPressed =
(
	print "3. Button Pressed - No Arguments"
)

fn weldSkinnedObjects objs = 
(
	if objs.count<=2 then
	(
		messagebox "select at least two skinned objects"
		return false
	)
	
	--try
	(
		gSkinWeldProgresssDialog = dotNetObject "MaxCustomControls.ProgressDialog"
		gSkinWeldProgresssDialog.Show()
		gSkinWeldProgresssDialog.text = "Create Skinning Progress"
		gSkinWeldProgresssDialog.Controls.Item[0].text = "stop"
		gSkinWeldProgresssDialog.Controls.Item[1].text = "preparing..."
		dotNet.addEventHandler gSkinWeldProgresssDialog.Controls.Item[0] "click" stopButtonPressed
		gSkinWeldProgresssDialog.Controls.Item[2].value = 0

		local groupIdx = getattrindex "Gta Object" "groupName"
		local stateIdx = getattrindex "Gta Object" "animState"
		local txdIdx = getattrindex "Gta Object" "TXD"
		local dynIdx = getattrindex "Gta Object" "Is Dynamic"
		local animIdx = getattrindex "Gta Object" "Has Anim"
		
		local formerSkinObj = #()
		for obj in objs do
			append formerSkinObj obj.modifiers[#Skin]

		-- setting up infofrom first mesh
		
		local firstObjectsBoneRoot = undefined
		local compObj = copy objs[1]
		compObj.material = undefined
		compObj.name = objs[1].name
		if not (matchPattern compObj.name pattern:"*_merge*") then compObj.name = compObj.name+"_merge"
		select compObj
		converttomesh compObj
		local formerVertCount = getnumverts compObj
		local theSkin = Skin()
		addModifier compObj theSkin
		select compObj
		setCommandPanelTaskMode #modify

		allDueProcesses = 0
		if undefined!=formerSkinObj[1] then
		(
			select objs[1]
			allDueProcesses = 
				(skinOps.GetNumberBones formerSkinObj[1]) + 
				((skinOps.GetNumberVertices formerSkinObj[1])*2)
			print ("number of allDueProcesses:"+allDueProcesses as string)
		)
		else allDueProcesses = objs[1].numverts

		-- setting up info from following meshes

		for objIndex=2 to objs.count do
		(
			local compObjTemp = copy objs[objIndex]
			compObjTemp.material = undefined
			select compObjTemp
			converttomesh compObjTemp
			attach compObj compObjTemp

			if undefined!=formerSkinObj[objIndex] then
			(
				select objs[objIndex]
				allDueProcesses = allDueProcesses +
					(skinOps.GetNumberBones formerSkinObj[objIndex]) + 
					((skinOps.GetNumberVertices formerSkinObj[objIndex])*2)
				print ("number of allDueProcesses:"+allDueProcesses as string)
			)
			else allDueProcesses = allDueProcesses +  objs[objIndex].numverts
		)
		compObj.material = objs[1].material

		processed = 0

		----------------------------------------------------------------------------------------------------------
		-- First object
		-- check for errors
-- 		if compObj.numverts != (objs[1].numverts + objs[2].numverts) then 
-- 		(
-- 			messagebox ("not equal vert numbers in meshes: "+ compObj.numverts as string +","+ objs[1].numverts as string)
-- 			gSkinWeldProgresssDialog.Close()
-- 			return false
-- 		)
		-- add bones
		gSkinWeldProgresssDialog.Controls.Item[1].text = "Getting first objects skin weights..."
		if undefined!=formerSkinObj[1] then
		(
			select compObj
			print ("next process count:"+(skinOps.GetNumberBones formerSkinObj[1]) as string)
			local lastBoneParent = undefined
			for i=1 to skinOps.GetNumberBones formerSkinObj[1] do
			(
				local theBone = getNodeByName (skinOps.GetBoneName formerSkinObj[1] i 0)
				if "Gta Object"!=(getattrclass theBone) then messagebox ("The bone "+theBone.name+"is not of an attributable class (e,g, Editable mesh)")
				setAttr theBone txdIdx (getAttr objs[1] txdIdx)
				setAttr theBone dynIdx true
				if undefined!=theBone and undefined!=theBone.parent and lastBoneParent==theBone.parent then
					firstObjectsBoneRoot = theBone.parent
				lastBoneParent = theBone.parent
				skinops.addBone theSkin theBone 1
				increaseProgress()
			)
			-- get bnone weights
			select objs[1]
			modpanel.setCurrentObject formerSkinObj[1]
			completeRedraw()
			local weights = #()
			print ("next process count:"+(skinOps.GetNumberVertices formerSkinObj[1]) as string)
			for k=1 to (skinOps.GetNumberVertices formerSkinObj[1]) do
			(
				local boneWeights  = #(0,0,0)
				for i=1 to (skinOps.GetVertexWeightCount formerSkinObj[1] k) do
				(
					local systemBoneIndex = skinOps.GetVertexWeightBoneID formerSkinObj[1] k i
					boneWeights[systemBoneIndex] = (skinOps.GetVertexWeight formerSkinObj[1] k i)
				)
				append weights boneWeights
				increaseProgress()
			)
		)
		else -- dummy weights for unskinned object
		(
			for k=1 to objs[1].numverts do
			(
				local boneWeights  = #(0,0,0)
				append weights boneWeights
			)
		)
		--------------------------------------------------------------------------------------
		-- Other Objects 
		--------------------------------------------------------------------------------------
		for objIndex=2 to objs.count do
		(
			gSkinWeldProgresssDialog.Controls.Item[1].text = ("Getting skin weights for object "+(objIndex as string)+" ...")
			if undefined!=formerSkinObj[objIndex] then
			(
				-- object 2
				local boneoffset = skinOps.GetNumberBones theSkin
				-- add bones
				print ("next process count:"+(skinOps.GetNumberBones formerSkinObj[objIndex]) as string)
				for i=1 to skinOps.GetNumberBones formerSkinObj[objIndex] do
				(
					local theBone = getNodeByName (skinOps.GetBoneName formerSkinObj[objIndex] i 0)
					if "Gta Object"!=(getattrclass theBone) then messagebox ("The bone "+theBone.name+"is not of an attributable class (e,g, Editable mesh)")
					setAttr theBone txdIdx (getAttr objs[1] txdIdx)
					setAttr theBone dynIdx true
					if undefined!=theBone and undefined!=firstObjectsBoneRoot then
						append firstObjectsBoneRoot.children theBone
					skinops.addBone theSkin theBone 1
					increaseProgress()
				)
				-- bone offset from first one
				select objs[objIndex]
				modpanel.setCurrentObject formerSkinObj[objIndex]
				completeRedraw()
				print ("next process count:"+(skinOps.GetNumberVertices formerSkinObj[objIndex]) as string)
				for k=1 to (skinOps.GetNumberVertices formerSkinObj[objIndex]) do
				(
					local boneWeights  = #(0,0,0)
					for i=1 to (skinOps.GetVertexWeightCount formerSkinObj[objIndex] k) do
					(
						local systemBoneIndex = skinOps.GetVertexWeightBoneID formerSkinObj[objIndex] k i
						boneWeights[(boneoffset + systemBoneIndex)] = (skinOps.GetVertexWeight formerSkinObj[objIndex] k i)
					)
					append weights boneWeights
					increaseProgress()
				)
			)
			else -- dummy weights for unskinned object
			(
				for k=1 to objs[objIndex].numverts do
				(
					local boneWeights  = #(0,0,0)
					append weights boneWeights
				)
			)
		)
		-- set weights
		select compObj
		modpanel.setCurrentObject theSkin
		completeRedraw()
		print ("next process count:"+(skinOps.GetNumberVertices theSkin) as string)
		gSkinWeldProgresssDialog.Controls.Item[1].text = ("Copying weights into new object...")
		for k=1 to (skinOps.GetNumberVertices theSkin) do
		(
			--print ("weights:"+weights[k] as string)
			for i=1 to (skinOps.GetNumberBones theSkin) do
			(
				if weights[k][i]==undefined then weights[k][i] = 0.0
				skinops.SetVertexWeights theSkin k i weights[k][i]
			)
			increaseProgress()
		)
		select compObj
		setAttr compObj groupIdx (getAttr objs[1] groupIdx)
		setAttr compObj txdIdx (getAttr objs[1] txdIdx)
		setAttr compObj stateIdx (getAttr objs[1] stateIdx)
		setAttr compObj dynIdx (getAttr objs[1] dynIdx)
	)
-- 	catch
-- 	(
-- 		print (getCurrentException())
-- 		gSkinWeldProgresssDialog.Close()
-- 		return false
-- 	)
	gSkinWeldProgresssDialog.Close()
	return true
)

fn weldSelection = 
(
	currentSelection = (selection as array)
	if weldSkinnedObjects currentSelection then
		delete currentSelection
)