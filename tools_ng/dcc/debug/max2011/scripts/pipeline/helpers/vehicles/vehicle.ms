--
-- File:: pipeline/helpers/vehicles/vehicle.ms
-- Description:: Vehicle Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 19 March 2010
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/helpers/vehicles/stray_vert_cleaner.ms"
filein "pipeline/util/pivot_reset.ms"

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout ResetRoll "Reset Pivot Transform and Scale"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Vehicle_Toolkit#Reset_Pivot_Transform_and_Scale" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnGo "Go" width:150 height:30
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnGo pressed do (
		for o in $selection do 
			pivot_reset_transform o
	)
)

try CloseRolloutFloater VehicleToolkit catch()
VehicleToolkit = newRolloutFloater "Vehicle Toolkit" 200 215 50 96
addRollout ResetRoll VehicleToolkit
addRollout strayverts VehicleToolkit