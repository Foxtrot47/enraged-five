--
-- File:: pipeline/helpers/vehicles/stray_vert_cleaner.ms
-- Description:: Script to pick up stray verts after vehicle skin projection
--
-- Author:: Rick Stirling <rick.stirling@rockstarnorth.com>
-- Date:: February 2010
--
-----------------------------------------------------------------------------


rollout strayverts "Stray Vert Cleaner"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Stray_Vert_Cleaner" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnAddMeshSelect "Add Mesh Selector" width:150 height:30
	button btnFixSelect "Fix Selected Faces" width:150 height:30
	
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Add mesh select to make it easier to grab model sections
	--------------------------------------------------------------
	on btnAddMeshSelect pressed do 	(
		if selection.count !=  1 then messagebox "You must select 1 skinned mesh" 
		else ( 
			modPanel.addModToSelection (Mesh_Select ()) ui:on
				subobjectLevel = 5
		)	
	)

	--------------------------------------------------------------
	-- Fix the bad verts
	--------------------------------------------------------------
	on btnFixSelect pressed do (
		-- needs better error checking I assume?
		if selection.count !=  1 then messagebox "You must select 1 skinned mesh" 
		
	-- Assume the mesh is valid		
		else ( 
			skobj = $
			
			--Get the selected verts
			-- We can assume edit mesh? 
			selectedverts =#()
			selectedFaces = (getFaceSelection $ as array)
			
			-- Get all the verts in this face selection
			for theFace= 1 to selectedfaces.count do (
					
				-- Find the verts used in each face
				faceverts = (meshop.getVertsUsingFace skobj selectedFaces[theFace]) as array
				for fv = 1 to faceverts.count do (
					v = faceverts[fv]
					-- If this vert is new, add it to the list of all verts.
					appendifunique selectedverts v
				)
			)
		
			-- At this point I have a vert list
			-- Build a weight array for those selected verts
			BoneNArray =#()
			BoneArray = #()
			WeightArray = #()
			PossibleGoodBones = #()
			
			modPanel.setCurrentObject skobj.modifiers[#Skin]
			objskin = skobj.modifiers[#skin]
			howmanybones = skinops.getnumberbones objskin
			
			-- For each vert, find the weights
			for theVert = 1 to selectedverts.count do (
				-- Get the number of bones, the boneid and name and the weight
				BNumber = skinOps.getVertexWeightCount objSkin selectedverts[theVert]
				boneID = skinOps.getVertexWeightBoneId objSkin selectedverts[theVert] BNumber
				boneWeight = skinOps.getVertexWeight objSkin selectedverts[theVert] BNumber
				
				append BoneNArray BNumber 
				append BoneArray boneID
				append WeightArray boneWeight
			)
			

			
			-- Make some assumptions
			-- The most used bone is the correct one
			-- Find the most used bone
			-- There must be a better way
			
			-- Find the unique bones
			for test =  1 to BoneArray.count do (
				p = BoneArray[test]
				appendifunique PossibleGoodBones p
			)
			
			mostFrequent = 0
			mostFrequentBone = 0
			
			for i = 1 to PossibleGoodBones.count do (
					pgb = PossibleGoodBones[i]
					thecount = 0
					for b = 1 to BoneArray.count do (
						if BoneArray[b] == pgb then thecount=thecount+1
					)
					if thecount > 	mostFrequent then 	(
						mostFrequent = thecount
						mostFrequentBone = pgb
					)
			)
			
			-- We now have the most common boneid
			-- Set all selected verts 100 to that bone
			for theVert = 1 to selectedverts.count do (
				skinOps.SetVertexWeights objSkin  selectedverts[theVert] mostFrequentBone 1.0
			)
			
			-- Go back to the face selection code
			modPanel.setCurrentObject $.modifiers[#Mesh_Select]
		)
	)
	
)



