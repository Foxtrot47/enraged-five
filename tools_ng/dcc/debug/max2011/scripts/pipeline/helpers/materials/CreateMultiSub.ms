--
-- File:: pipeline/helpers/materials/CreateMultiSub.ms
-- Description::  Functions for working with multisubs
--
-- 29/10/2003
-- by Greg Smith <greg.smith@rockstarnorth.com>
--
-----------------------------------------------------------------------------

filein "pipeline/export/maps/objects.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
newSubMatList = #()
newSubMatIndexList = #()

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
fn GTAaddNewSubMap submap = (

	-- look to see if the submat is already in here
	-- if so return the index else add it and return
	-- the new index
	
	index = 1
	
	for newSubMat in newSubMatList do (

		if classof submap == RexBoundMtl and classof newSubMat == RexBoundMtl then (
		
				if 	RexIsCollisionEqual submap newSubMat == true then (
				
					return index
				)
		
		) else (

			if rexAreMatsEqual submap newSubMat then (
			
				return index
			)
		)
	
		index = index + 1
	)
	
	if classof submap == RexBoundMtl then (
	
		submap.name = (RexGetCollisionName submap)
	)
	
	print ("adding new map: " + (newSubMatList.count as string))
	
	-- if we go here then add it to the end
	append newSubMatList submap
	
	return newSubMatList.count
)

fn GTAcreateNewMultiSubSel createset prefix:"" = (
	
	newSubMatList = #()
	newSubMatIndexList = #()

	for obj in createset do (
	
		numFaces = getNumFaces obj	
		newSubMatIndex = #()
	
		-- copy of the face material id's
		-- to a temporary buffer for the object
		if classof obj == Editable_Mesh then (
	
			for i = 1 to numFaces do (		
			
				append newSubMatIndex (getFaceMatID obj i)
			)	
		)
		else if classof obj == Editable_Poly then (

			for i = 1 to numFaces do (		
			
				append newSubMatIndex (polyOp.getFaceMatID obj i)
			)			
		)
	
		if classof(obj.material) == Multimaterial then (
	
			-- go through each material, looking to see if
			-- it is used in the object. if so add it to the
			-- new multisub and set and material indices with the 
			-- the new index
						
			for oldindex = 1 to obj.material.materialList.count do (
			
				oldSubMat = obj.material.materialList[oldindex]
				childmatid = obj.material.materialIDList[oldindex]
			
				-- is it used in the model?
				foundMatch = false				
				
				for i = 1 to numFaces do (
				
					if classof obj == Editable_Mesh then (
					
						if getFaceMatID obj i == childmatid then (
					
							foundMatch = true
							exit
						)
					) else if classof obj == Editable_Poly then (
					
						if polyOp.getFaceMatID obj i == childmatid then (
					
							foundMatch = true
							exit
						)
					)
				)
				
				--if it is then add it to the new multisub
				if foundMatch == true then (
				
					newindex = GTAaddNewSubMap oldSubMat
								
					for i = 1 to numFaces do (
					
						if classof obj == Editable_Mesh then (
						
							checkindex = getFaceMatID obj i
						)
						else if classof obj == Editable_Poly then (
						
							checkindex = polyOp.getFaceMatID obj i
						)
				
						if checkindex == childmatid then (
						
							newSubMatIndex[i] = newindex
						)
					)
				)
			)			
		) else (	
		
			newindex = GTAaddNewSubMap obj.material
			
			--change every material index to the new material index
			
			for i = 1 to numFaces do (		
			
				newSubMatIndex[i] = newindex
			)
		)
		
		-- add the temporary before for the face material id's
		-- to the end of the object array
		append newSubMatIndexList newSubMatIndex
	)
	
	-- create the new material
	newmat = Multimaterial()
	newmat.name = prefix + " - " + newmat.name
	newmat.numsubs = newSubMatList.count
	newmat.materialList = newSubMatList
	j = 1
		
	-- copy the face material id's back and set the material
	-- to our new multisub
	for obj in createset do (
	
		numFaces = getNumFaces obj	
	
		for i = 1 to numFaces do (
		
			if classof obj == Editable_Mesh then (
			
				setFaceMatID obj i newSubMatIndexList[j][i]
			) else if classof obj == Editable_Poly then (
			
				polyOp.setFaceMatID obj i newSubMatIndexList[j][i]
			)
		)
	
		obj.material = newmat
		j = j + 1
	)
)

fn GTAcreateNewMultiSub = (

	subobjectLevel = 0

	newSubMatObject = #()
	collObjects = #()
	
	container_obj = -1
	
	for obj in selection do (
	
		if classof obj == Col_Mesh then (
		
			append collObjects obj
			col2mesh obj
		)
	
		if (classof(obj) == Editable_mesh or classof(obj) == Editable_poly) and obj.modifiers.count == 0 then (
	
			new_container_obj = RsMapObjectGetTopLevelContainerFor obj
			
			if container_obj == -1 then (
				
				container_obj = new_container_obj
			) else if container_obj != new_container_obj then (
				
				messagebox "trying to create a multisub across containers, failed"
				return 0
			)
			
			append newSubMatObject obj
		)
	)
	
	prefix = ""
	
	if container_obj != undefined then (
	
		prefix = container_obj.name
	)
	
	GTAcreateNewMultiSubSel newSubMatObject prefix:prefix
	
	for obj in collObjects do (
	
		mesh2col obj
	)
)

fn GTAcreateNewMultiSubTxd prefix:"" = (

	subobjectLevel = 0

	newTxdList = #()
	
	-- create a list of the unique txd names in the selection
	
	for obj in selection do (
	
		if classof(obj) == Editable_mesh and obj.modifiers.count == 0 then (
		
			txdIndex = -1
		
			if getattrclass obj == "Gta Object" then (
			
				txdIndex = GetAttrIndex "Gta Object" "TXD"
			)

			if getattrclass obj == "GtaAnimHierarchy" then (
			
				txdIndex = GetAttrIndex "GtaAnimHierarchy" "TXD"
			)
			
			if txdIndex != -1 then (
			
				strTxdName = GetAttr obj txdIndex
				
				found = false
				
				for txditem in newTxdList do (
				
					if newTxdList == txditem then (
					
						found = true
					)
				)
				
				if found == false then (		
		
					append newTxdList strTxdName
				)
			)
		)
	)
	
	-- run through each unique txd name, creating a new multisub that
	-- has the textures of each object with the txd name
	
	for txditem in newTxdList do (
	
		newSubMatObject = #()
		
		for obj in selection do (
		
			if classof(obj) == Editable_mesh and obj.modifiers.count == 0 then (
			
				txdIndex = -1
			
				if getattrclass obj == "Gta Object" then (
				
					txdIndex = GetAttrIndex "Gta Object" "TXD"
				)
	
				if getattrclass obj == "GtaAnimHierarchy" then (
				
					txdIndex = GetAttrIndex "GtaAnimHierarchy" "TXD"
				)
			
				if txdIndex != -1 then (
				
					strTxdName = GetAttr obj txdIndex

					if strTxdName == txditem then (
					
						append newSubMatObject obj
					)
				)
			)
		)
		
		if prefix == "" then (
		
			GTAcreateNewMultiSubSel newSubMatObject prefix:txditem
		) else (
			
			GTAcreateNewMultiSubSel newSubMatObject prefix:prefix
		)
	)
)

fn GTAcreateNewMultiSubObject = (

	subobjectLevel = 0

	selList = #()
	
	for obj in selection do (
	
		append selList obj
	)

	for obj in selList do (
	
		select obj
		GTAcreateNewMultiSubTxd prefix:obj.name
	)
)