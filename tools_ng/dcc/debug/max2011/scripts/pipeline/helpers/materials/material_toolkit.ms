--
-- File:: pipeline/helpers/material/material_toolkit.ms
-- Description:: Material Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 18 March 2010
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/util/material.ms"
filein "pipeline/helpers/materials/viewtxd.ms"
filein "rockstar/helpers/specfixup.ms"
filein "pipeline/util/UtilityFunc.ms"
filein "pipeline/helpers/materials/CreateMultiSub.ms"

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- SCRIPT HELPERS
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout MatScriptRoll "Script Helpers" width:280 height:300
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Material_Toolkit#Script_Helpers" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	button btnNewSub "New MultiSub" pos:[9,51] width:129 height:26
	button btnSubTXD "New MultiSubs for TXDs" pos:[141,51] width:129 height:26
	button btnSubObject "New MultiSubs for Objects" pos:[66,82] width:147 height:26
	button btnTexInfo "Tex Map Info" pos:[9,20] width:129 height:26
	button btnResetDiff "Reset Diffuse" pos:[141,20] width:129 height:26
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnNewSub pressed do (
		GTAcreateNewMultiSub()
	)
	
	on btnSubTXD pressed do (
		GTAcreateNewMultiSubTxd()
	)
	
	on btnSubObject pressed do (
		GTAcreateNewMultiSubObject()
	)
	
	on btnTexInfo pressed do (
		filein "rockstar/helpers/texmaps.ms"
	)
	
	on btnResetDiff pressed do (
		ResetDiffuseToWhite()
	)
)

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- UV MAPPING
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout UVMapRoll "UV Mapping" width:286 height:293
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	HyperLink lnkHelp "Help?" pos:[250,3] width:28 height:15 address:"https://devstar.rockstargames.com/wiki/index.php/Material_Toolkit#UV_Mapping" color:(color 0 0 255) hovercolor:(color 0 0 255) visitedcolor:(color 0 0 255)
	GroupBox grpBox "Box Mapper" pos:[8,16] width:264 height:89
	button btnMapsize2 "2x2x2" pos:[15,57] width:55 height:16
	button btnMapsize3 "3x3x3" pos:[15,81] width:55 height:16
	button btnMapsize4 "4x4x4" pos:[79,57] width:55 height:16
	button btnMapsize5 "5x5x5" pos:[79,81] width:55 height:16
	button btnMapsizefit "To Fit" pos:[143,57] width:64 height:16
	button btnMapsizecust "Apply" pos:[231,81] width:32 height:16
	spinner spnMapsiz "Custom " pos:[169,81] width:60 height:16 range:[0,1000,10]
	GroupBox grpTexTools "Texture Tools" pos:[9,108] width:160 height:87
	spinner spnXRep "X Repeat: " pos:[37,126] width:109 height:16
	spinner spnYRep "Y Repeat: " pos:[37,145] width:109 height:16 range:[0,100,0]
	button btnTexture "Texture" pos:[24,168] width:62 height:21
	button btnFacemap "Facemap" pos:[94,168] width:62 height:21
	GroupBox grpMatTools "Material Tools" pos:[173,108] width:99 height:87
	label lblMatID "Mat# to face sel:" pos:[180,133] width:85 height:17
	spinner spnMatID "" pos:[184,150] width:76 height:16 range:[0,100,1] type:#integer
	spinner spnBoxMatChannel "Tex channel: " pos:[153,34] width:104 height:16 range:[1,99,1] type:#integer
	radiobuttons radUnits "" pos:[15,33] width:119 height:16 labels:#("meters", "tiles") columns:2
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Box Mapper
	--------------------------------------------------------------
	fn bungonboxmap bmapsize = (
		mapmod = uvwmap()

		--set up box map
		mapmod.maptype = 4
		mapmod.mapChannel = spnBoxMatChannel.value
		if radUnits.state==1 then
		(
--			print "maters"
			mapmod.length = bmapsize
			mapmod.width = bmapsize
			mapmod.height = bmapsize
		)
		else
		(
--			print "tiles"
			mapmod.utile = bmapsize
			mapmod.vtile = bmapsize
			mapmod.wtile = bmapsize
		)
		--add modifier to selection
		modPanel.addModtoSelection( mapmod )
	)

	fn bungonboxmapfit = (
		mapmod = uvwmap()

		--set up box map
		mapmod.maptype = 4
			
		--add modifier to selection
		modPanel.addModtoSelection( mapmod )
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////	

	on btnMapsize2 pressed do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 2
				collapseStack $
			)
		)
	on btnMapsize2 rightClick do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 2
				modPanel.addModtoSelection( unwrap_uvw() )
				$.unwrap_uvw.edit()
				$.unwrap_uvw.fitSelected()
			)
		)
	on btnMapsize3 pressed do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 3
				collapseStack $
			)
		)
	on btnMapsize3 rightClick do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 3
				modPanel.addModtoSelection( unwrap_uvw() )
				$.unwrap_uvw.edit()
				$.unwrap_uvw.fitSelected()
			)
		)
	on btnMapsize4 pressed do
	(	
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 4
				collapseStack $
			)
		)
	on btnMapsize4 rightClick do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 4
				modPanel.addModtoSelection( unwrap_uvw() )
				$.unwrap_uvw.edit()
				$.unwrap_uvw.fitSelected()
			)
		)
	on btnMapsize5 pressed do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 5
				collapseStack $
			)
		)
	on btnMapsize5 rightClick do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 5
				modPanel.addModtoSelection( unwrap_uvw() )
				$.unwrap_uvw.edit()
				$.unwrap_uvw.fitSelected()
			)
		)
	on btnMapsizefit pressed do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmapfit()
				collapseStack $
			)
		)
	on btnMapsizefit rightClick do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmapfit()
				modPanel.addModtoSelection( unwrap_uvw() )
				$.unwrap_uvw.edit()
				$.unwrap_uvw.fitSelected()
			)
		)
	on btnMapsizecust pressed do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap spnMapsiz.value
				collapseStack $
			)
		)
	on btnMapsizecust rightClick do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap cmapsiz.value
				modPanel.addModtoSelection( unwrap_uvw() )
				$.unwrap_uvw.edit()
				$.unwrap_uvw.fitSelected()
			)
		)
	on spnMapsiz changed val do
	(
		if selection.count != 1 then (
			messagebox "Please select one object."
		)
		else (
			bungonboxmap val
			collapseStack $
		)
	)
	on btnTexture pressed do
	(
		if selection.count != 1 then (
			messagebox "Please select one object."
		)
		else (
			modPanel.addModToSelection (Uvwmap ())
			$.modifiers[#UVW_Mapping].maptype = 4
			$.modifiers[#UVW_Mapping].utile = spnXRep.value
			$.modifiers[#UVW_Mapping].vtile =spnYRep.value
			macros.run "Modifier Stack" "Convert_to_Mesh"
			subobjectLevel = 4
		)
	)
	on btnFacemap pressed do
	(
		if selection.count != 1 then (
			messagebox "Please select one object."
		)
		else (
			modPanel.addModToSelection (Uvwmap ())
			$.modifiers[#UVW_Mapping].maptype = 5
			$.modifiers[#UVW_Mapping].utile = spnXRep.value
			$.modifiers[#UVW_Mapping].vtile =spnYRep.value
			macros.run "Modifier Stack" "Convert_to_Mesh"
			subobjectLevel = 4
		)
	)
	on spnMatID changed newmatid do
	(
		if selection.count != 1 then (
			messagebox "Please select one object."
		)
		else (
			modPanel.addModToSelection (Materialmodifier ())
			$.modifiers[#Material].materialID = newmatid
			macros.run "Modifier Stack" "Convert_to_Mesh"
			subobjectLevel = 4
		)
	)
)

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- OPTIMISATION
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout OpRoll "Optimisation"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Material_Toolkit#Optimisation" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	groupBox grpMulti "MultiSubs in Scene" pos:[6,20] width:131 height:107
	spinner spnMultiSubLimit "" pos:[26,51] width:91 height:16
	button btnCheckMultiSubCount "Check Count" pos:[29,78] width:85 height:29
	
	groupBox grpViewTXD "View TXD" pos:[142,20] width:131 height:107
	button btnViewITD "View ITD" pos:[163,40] width:85 height:24
	button btnViewIDR "View IDR" pos:[164,68] width:85 height:24
	checkbox chkUseStream "Use the stream" checked:true pos:[160,97] width:109 height:24
	
	groupBox grpComposite "Composite Specular Setup" pos:[6,128] width:267 height:61
	button btnMerge "Merge" pos:[21,148] width:73 height:29
	button btnList "List" pos:[104,148] width:73 height:29
	button btnRestore "Restore" pos:[186,148] width:73 height:29
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	-----------------------------------------------------------------------------
	-- MultiSubs in Scene
	-----------------------------------------------------------------------------
	on btnCheckMultiSubCount pressed do (
		global mswarningList = #()

		for obj in rootnode.children do (
			mapList = #()
			polyCount = #()

			if classof obj == Editable_mesh or classof obj == Editable_Poly then (
				RsMatGetMapIdsUsedOnObjectWithCount obj mapList polycount:polyCount
				multiSubLimit = spnMultiSubLimit.value as integer
				
				if mapList.count > multiSubLimit then (
					warningstring = obj.name + " is using " + (mapList.count as string) + " unique submaterial IDs from it's material " + obj.material.name
					append mswarningList warningstring
				)
			)
		)

		rollout RsMapCritErrors "Number of Multisubs"
		(
			listbox lstMissing items:mswarningList
			button btnOK "OK" width:100 pos:[150,150]

			on btnOK pressed do (
				DestroyDialog RsMapCritErrors
			)	
		)
		CreateDialog RsMapCritErrors width:600 modal:true
	)
	
	-----------------------------------------------------------------------------
	-- View TXD
	-----------------------------------------------------------------------------
	on btnViewITD pressed do (
		ViewITD chkUseStream.checked
	)
	
	on btnViewIDR pressed do (
		ViewIDR chkUseStream.checked
	)
	
	-----------------------------------------------------------------------------
	-- Composite Specular Setup
	-----------------------------------------------------------------------------
	on btnMerge pressed do (
		DoFixup()
	)	
	
	on btnList pressed do (
		DoList()
	)

	on btnRestore pressed do (
		DoRemove()
	)
	
)

try CloseRolloutFloater MaterialToolkit catch()
MaterialToolkit = newRolloutFloater "Material Toolkit" 300 605 50 96
addRollout MatScriptRoll MaterialToolkit
addRollout UVMapRoll MaterialToolkit
addRollout OpRoll MaterialToolkit