-- Rockstar Proxy Texture Manager
-- Rockstar North
-- 04/05/2009
-- by Luke Openshaw

-- manage the usage of proxy textures for max performance improvements

filein "rockstar/export/settings.ms"
filein "pipeline/util/file.ms"
filein "rockstar/util/material.ms"
filein "rockstar/util/shader_config_loader.ms"

rollout RsProxyTextureMgrRoll "Proxy Texture Manager"
(
	button btnSwapSceneProxy "Swap Entire Scene To Proxy"
	button btnSwapSceneHiRes "Swap Entire Scene To Hi Res"
	
	button btnSwapSelectedProxy "Swap Selected Objects to Proxy"
	button btnSwapSelectedHiRes "Swap Selected Objects to HiRes"
	
	local MissingFileList = #()

	--------------------------------------------------------------
	-- Build the path for the switch and ensure the destination file
	-- exists.  If not add to a list of missing textures for missing
	-- texture report
	--------------------------------------------------------------	
	fn BuildEquivPathForSwitch proxy texmap &newpath = (
		
		newpath = ""
		if proxy == true then newpath = RsConfigGetProxyTextureSourceDir()
		else newpath = RsConfigGetTextureSourceDir()
		
		texsourcepathtokens = filterstring newpath "/"
		
		texmaptokens = filterstring texmap "\\"
		tokencountdiff = texmaptokens.count - texsourcepathtokens.count

		if tokencountdiff > 0 then (
			for i = (texsourcepathtokens.count + 1) to texmaptokens.count do (
				if i == (texsourcepathtokens.count + 1) then newpath = newpath + texmaptokens[i]
				else newpath = newpath + "/" + texmaptokens[i]
			)

			if RsFileExist newpath then (
				return true
			)
			else (
				append MissingFileList newpath
				--print ("Missing: " + newpath)
				return false
			)

			
		)
	)
	
	
	--------------------------------------------------------------
	-- Switch textures for a given object.  Deal with DX materials 
	--------------------------------------------------------------
	fn SwitchTextures proxy obj mat = (
		texmaplist = #()
		bitmaplist = #()
		errorlist = #()
		
		
		RsGetTexMapsFromMaterialWithMap obj mat texmaplist bitmaplist
		for texmap in bitmaplist do (
			newpath = ""
			if texmap != undefined then (
				if BuildEquivPathForSwitch proxy texmap.filename &newpath == true then (
					-- Only update textures that actually need to be changed
					if RsLowercase( RsMakeSafeSlashes ( texmap.filename ) ) != RsLowercase( RsMakeSafeSlashes ( newpath ) ) then (
						print ("Switching: " + texmap.filename + " to: " + newpath)
						texmap.filename = newpath
						gc()
					)
				)
			)
		)
		
		-- DX shaders are a special case.  Unfortunately we cant just loop the tex maps but have to access them by name... which you can't query
		if classof mat == DirectX_9_Shader then (
			global gmat = mat -- Variables used in execute method need to be global so they can execute in the global scope.  See maxscript documentation
			global gmatvar = undefined
			
			dxshadername = RsRemovePathAndExtension mat.effectfile
			shader = RsLoadShaderFromXml "terrain" dxshadername
			
			if shader != undefined then (
				for shadervar in shader.shadervars do (
					gmatvar = undefined 
					try (
						gmatvar = execute ("gmat." + shadervar.name)
					) catch (
						append errorlist ((getCurrentException()) + " " + (shadervar.name as string) )
					)
					if gmatvar != undefined then (
						-- Check if the variable has changed rather than wholesale copy and gc()
						-- since it seems to leak
						if shadervar.type == "difftex" or shadervar.type == "bumptex" then (
							newpath = ""
							--print (obj.name + ": " + mat.name + ": " + (gmatvar as string) + ": " + shadervar.name)
							if BuildEquivPathForSwitch proxy gmatvar.filename &newpath == true then (
								if RsLowercase( RsMakeSafeSlashes ( gmatvar.filename ) ) != RsLowercase( RsMakeSafeSlashes ( newpath ) ) then (
									print ("Switching for DX: " + gmatvar.filename + " to: " + newpath)
									gmatvar.filename = newpath
									execute ("gmat." + shadervar.name + " = gmatvar")
									gc()
								)

							)
						)
					)
				)
			)
		)
		
		for err in errorlist do (
			print err
		)
	)
	
	--------------------------------------------------------------
	-- report misplaced textures
	--------------------------------------------------------------
	fn ReportMissingTextures = (

		rollout RsMapMissingTextures "The following textures are missing from your local drive and have not been switched"
		(
			listbox lstMisplaced items:MissingFileList
			button btnOK "OK" width:100 pos:[150,150]

			on btnOK pressed do (

				DestroyDialog RsMapMissingTextures
			)	
		)

		CreateDialog RsMapMissingTextures width:800 modal:true
	)
	
	--------------------------------------------------------------
	-- kick off the switch for the objects in the object list provided
	--------------------------------------------------------------
	fn SwitchTexturesForObjects objlist proxy = (
		MissingFileList = #()
		for obj in objlist do (
			
			if classof obj == Editable_Poly or classof obj == Editable_mesh or classof obj == PolyMeshObject then (
				SwitchTextures proxy obj obj.mat
			)
		)
		if MissingFileList.count > 0 then ReportMissingTextures()
		else messagebox "Texture switch complete."
	)
	
	on btnSwapSceneProxy pressed do (
		if querybox "Are you sure you want to switch all textures in the scene?" == true then SwitchTexturesForObjects rootnode.children true
	)
	
	on btnSwapSceneHiRes pressed do (
		if querybox "Are you sure you want to switch all textures in the scene?" == true then SwitchTexturesForObjects rootnode.children false
	)
	
	on btnSwapSelectedProxy pressed do (
		SwitchTexturesForObjects (selection as array) true
	)
	
	on btnSwapSelectedHiRes pressed do (
		SwitchTexturesForObjects (selection as array) false
	)
	
)

RsProxyUtil = newRolloutFloater "Tex Proxy Manager" 200 260 50 126
addRollout RsProxyTextureMgrRoll RsProxyUtil 