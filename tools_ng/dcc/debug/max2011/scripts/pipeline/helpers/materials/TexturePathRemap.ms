--
-- File:: pipeline/helpers/materials/texturepathremap.ms
-- Description:: Utility plugin to remap texture paths. Replaces
--							string in original texture to new string
--
-- Author:: Adam Fowler <adam.fowler@rockstarnorth.com>
--

-----------------------------------------------------------------------------
-- History
-----------------------------------------------------------------------------
--
-- 11/2/10
-- Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Adding support for Rage shaders
--

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
maxTextureList = #()
remapTextureList = #()

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout texRemapRoll "Texture File List"
(
	
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Texture_Path_Remap" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	listbox textureList items:remapTextureList width:550 height:12
	button refreshBtn "Refresh" align:#left offset:[554,-166] width:50 height:24
	button findBtn "Find" align:#left offset:[554,0] width:50 height:24
	button replaceBtn "Replace"align:#left offset:[554,0] width:50 height:24
	button closeBtn "Close" align:#left offset:[554,50] width:50 height:24
	edittext findEdit "Find:" across:2 width:175 height:20
	edittext replaceEdit "Replace:" width:195 height:20
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	fn AppendMap map = (
		print map
		if ( map != undefined ) then 
		(
			if ( Bitmaptexture == classof map ) and 
			   ( undefined != map.filename ) and 
			   ( "" != map.filename ) then 
			(
				append remapTextureList map.filename
				append maxTextureList map
			)
		)
	)
	
	fn AppendMaterialMaps mat = (
		if (classof mat) == Standardmaterial then (
			for map in mat.maps do (
				AppendMap map
			)
		)
		else if (classof mat) == Blend then (
			AppendMaterialMaps mat.map1
			AppendMaterialMaps mat.map2
		)
		else if (classof mat) == compositematerial or (classof mat) == Multimaterial then (
			for submat in mat.materiallist do 
				AppendMaterialMaps submat
		)
		else if (classof mat) == DoubleSided then (
			AppendMaterialMaps mat.material1
			AppendMaterialMaps mat.material2
		)
		else if (classof mat) == MatteShadow then (
			AppendMap mat.map
		)
		else if (classof mat) == RaytraceMaterial then (
			AppendMap mat.ambientMap
			AppendMap mat.diffuseMap
			AppendMap mat.specularMap
			AppendMap mat.shininessMap
			AppendMap mat.shinestrengthMap
			AppendMap mat.selfillumMap
			AppendMap mat.opacityMap
			AppendMap mat.filterMap
			AppendMap mat.bumpMap
			AppendMap mat.reflectionMap
			AppendMap mat.refractionMap

			AppendMap mat.displacementMap
		)
		else if (classof mat) == Shellac then (
			AppendMaterialMaps mat.shellacmtl1
			AppendMaterialMaps mat.shellacmtl2
		)
		else if (classof mat) == TopBottom then (
			AppendMaterialMaps mat.topMaterial
			AppendMaterialMaps mat.bottomMaterial
		)
		else if (classof mat) == Rage_Shader then (
			numvars = getNumSubTexmaps mat
			
			for i = 1 to numvars do (
				AppendMap (getSubTexmap mat i)
			)
		)
	)
	fn ConstructTextureList = (
		maxTextureList = #()
		remapTextureList = #()
		for mat in scenematerials do AppendMaterialMaps mat
	)
	-- Remove any textures that don't have the required string
	fn FilterTextureList text = (
		newTextureList = #()
		newFilenameList = #()
		
		-- create new list of textures
		for i=1 to remapTextureList.count do (
			if (findString remapTextureList[i] text ) != undefined then (
				append newTextureList maxTextureList[i]
			)
		)
		
		-- construct filename list from new texture list
		for tex in newTextureList do append newFilenameList tex.filename
		
		maxTextureList = newTextureList
		remapTextureList = newFilenameList
	)
	
	-- Replace string in current list of files
	fn ReplaceTextureList findText replaceText = (
		local posn
		local newFilename
		for i=1 to remapTextureList.count do (
			posn = findString remapTextureList[i] findText
			if posn != undefined then (
				newFilename = replace remapTextureList[i] posn findText.count replaceText
				maxTextureList[i].filename = newFilename
				remapTextureList[i] = newFilename
			)
		)
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on closeBtn pressed do closerolloutfloater texRemapUtil
	on refreshBtn pressed do ( 
		ConstructTextureList()
		textureList.items = remapTextureList
	)
	on findBtn pressed do (
		if findEdit.text == "" then (
			MessageBox "Please enter some text in the 'Find:' text field" title:"TexturePathRemap"
			return false
		)	
		FilterTextureList findEdit.text
		textureList.items = remapTextureList
	)
	on replaceBtn pressed do (
		if findEdit.text == "" then (
			MessageBox "Please enter some text in the 'Find:' text field" title:"TexturePathRemap"
			return false
		)	
		ReplaceTextureList findEdit.text replaceEdit.text
		textureList.items = remapTextureList
	)
)

try CloseRolloutFloater texRemapUtil catch ()
texRemapUtil = newRolloutFloater "Texture Path Remap" 650 264 40 96
texRemapRoll.ConstructTextureList()
addRollout texRemapRoll texRemapUtil

