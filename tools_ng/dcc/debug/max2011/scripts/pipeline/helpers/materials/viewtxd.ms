--
-- File:: pipeline/helpers/materials/viewtxd.ms
-- Description:: Functions for viewing TXD files from Max
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 8 December 2009
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- 18/3/2010
-- by Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Modified for use in the Material Toolkit
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
include "pipeline/util/string.ms"
include "rockstar/export/settings.ms"
include "pipeline/export/maps/globals.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

	--------------------------------------------------------------
	-- Object validation (as a quick sanity check)
	--------------------------------------------------------------
	fn ValidateObject = (
		if selection.count != 1 then (

			messagebox "Please select one object"
			return false			
		)
		
		if ( classof selection[1] != Editable_mesh ) and ( classof selection[1] != Editable_Poly ) then (
			messagebox "Please select an editable mesh or editable poly"
			return false
		)
		
		return true
	)

	--------------------------------------------------------------
	-- Extracts the RPF and returns the matching txdname from the extracted directory
	--------------------------------------------------------------
	fn ExtractRPF rubyFileName outputDir rpfname = (
		RsMakeSurePathExists rubyFileName
		local projectName = RsConfigGetProjectName()
		local rubyFile = createFile rubyFileName
			
		format "require 'pipeline/config/projects'\n" to:rubyFile
		format "require 'pipeline/config/project'\n" to:rubyFile
		format "require 'pipeline/content/content_core'\n" to:rubyFile
		format "require 'pipeline/gui/exception_dialog'\n" to:rubyFile
		format "require 'pipeline/os/file'\n" to:rubyFile
		format "require 'pipeline/projectutil/data_extract'\n" to:rubyFile
		format "require 'pipeline/util/rage'\n" to:rubyFile
		format "include Pipeline\n\n" to:rubyFile

		format "begin\n" to:rubyFile
		format "\trpf_filename = '%'\n" rpfname to:rubyFile 
		format "\tproject = Pipeline::Config.instance.projects[\"%\"]\n" projectName to:rubyFile
		format "\tproject.load_config( )\n" to:rubyFile
		format "\tbranch = project.branches[project.default_branch]\n\n" to:rubyFile
		format "\ttemp_dir = '%'\n" outputDir to:rubyFile
		format "\tr = RageUtils.new( project, branch.name )\n" to:rubyFile
		format "\tProjectUtil::data_extract_rpf( r, rpf_filename, temp_dir, true ) do |filename|\n" to:rubyFile
		format "\t\tputs \"Extracting: #{filename}\"\n" to:rubyFile
		format "\tend\n" to:rubyFile
		format "rescue Exception => ex\n" to:rubyFile
		format "\tGUI::ExceptionDialog.show_dialog( ex )\n" to:rubyFile
		format "end\n" to:rubyFile
			
		close rubyFile
		commandLine = "ruby " + rubyFileName
		print commandLine
		DOSCommand commandLine
	)

	--------------------------------------------------------------
	-- Opens the most recent version of a TXD file
	--------------------------------------------------------------
	fn OpenTXD txdname useStream = (
		-- Compare with the RPF unless the user has chosen to always use the stream
		if not useStream then (
			rpfname = RsConfigGetIndMapDir() + RsMapLevel + "/" + RsMapName + ".rpf"
			tempdir = "x:/TEMP_viewtxd/"
			
			-- Check whether this is the newest version of the file
			modrpf = RsFileModDate rpfname
			modstream = RsFileModDate txdname
			local dateTimeClass = dotNetClass "System.DateTime"
			local comp = ( dateTimeClass.Compare modrpf modstream )
			-- If the rpf is newer than the stream, give the user the option to use that file
			if comp > 0 then (
				if ( querybox "The RPF for this map is newer than the data in your stream.  Would you like to extract it?" ) then (
					-- Write and run a ruby script to extract the rpf
					rubyFileName = tempdir + "extract_txd.rb"
					outputDir = tempdir + RsMapName + "/"
					ExtractRPF rubyFileName outputDir rpfname
					txdname = outputDir + ( filenameFromPath txdname )
				)
			)
		)
		
		-- Get RPF Viewer path
		rpfviewer = ( RsConfigGetBinDir() + "rpfviewer/RPFViewer.exe" )
		if not (doesFileExist rpfviewer) then (
			error = "RPF Viewer not found at location " + rpfviewer
			messagebox error
		)
		
		-- Open the file using the RPF Viewer
		commandLine = rpfviewer + " " + txdname
		print commandLine
		HiddenDOSCommand commandLine
	)

	--------------------------------------------------------------
	-- Opens an ITD file in the RPF Viewer
	--------------------------------------------------------------	
	fn ViewITD useStream = (
		if ValidateObject() == false then return false
		
		local maps = RsMapGetMapContainers()
		if ( 1 != maps.count ) then
		(
			messageBox "Internal error with RsMapGetMapContainers.  Contact tools."
			format "Maps: %\n" maps
			return false
		)
		RsMapSetupGlobals maps[1]
		
		idxTXD = getattr $ (getattrindex "Gta Object" "TXD")
		txdname = ""
		
		-- Get TXD name
		txdname = ( RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/" + idxTXD + ".itd" )
		format "Opening TXD: %\n" txdname
		if not (doesFileExist txdname) then (
			messagebox "No .itd found in the stream for this object"
		)
		
		OpenTXD txdname useStream
	)

	--------------------------------------------------------------
	-- Opens an IDR file in the RPF Viewer
	--------------------------------------------------------------
	fn ViewIDR useStream = (
		if ValidateObject() == false then return false

		local maps = RsMapGetMapContainers()
		if ( 1 != maps.count ) then
		(
			messageBox "Internal error with RsMapGetMapContainers.  Contact tools."
			format "Maps: %\n" maps
			return false
		)
		RsMapSetupGlobals maps[1]
		txdname = ""
		
		-- Get TXD name
		txdname = ( RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/" + (RsLowercase $.name) + ".idr" )
		format "Opening TXD: %\n" txdname
		if not (doesFileExist txdname) then (
			messagebox "No .idr found in the stream for this object"
		)
		
		OpenTXD txdname useStream
	)