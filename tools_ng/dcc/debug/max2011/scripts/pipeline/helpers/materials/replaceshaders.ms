--
-- File:: pipeline/helpers/materials/replaceshaders.ms
-- Description:: Replace Shaders
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 17 March 2010
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/ui/shaderreplace.ms"
filein "rockstar/helpers/stdmatreplace.ms"
filein "rockstar/helpers/rageshadertostdmat.ms"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
try CloseRolloutFloater ShaderReplaceUtil catch()
ShaderReplaceUtil = newRolloutFloater "Replace Shaders" 320 470 50 126
addRollout StdMatReplacerRoll ShaderReplaceUtil 
addRollout RageShaderStdMatReplacerRoll ShaderReplaceUtil
addRollout GTAShaderReplacerRoll ShaderReplaceUtil