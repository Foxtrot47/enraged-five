--
-- File:: pipeline/helpers/props/proprenderer.ms
-- Description:: Utility to render props to a network drive.
--
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 16 January 2006
-- Date:: 18 January 2010 (readying for R* London's PropViewer)
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/objects.ms"
filein "pipeline/export/maps/globals.ms"
filein "pipeline/util/file.ms"
filein "rockstar/export/settings.ms"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

rollout RsRenderPropsRoll "Prop Renderer"
(
	---------------------------------------------------------------------------
	-- UI Widgets
	---------------------------------------------------------------------------
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Props_and_MILOs#Prop_Renderer" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	button btnRenderSelected "Render Selected" enabled:true width:100
	button btnRenderAll "Render All" enabled:true width:100
	group "Options"
	(
		label lblOutputPath "Output Path:"
		edittext edtRenderOutPath text:( RsConfigGetNetworkStreamDir() + "propViewerRenders" )
	)
	
	---------------------------------------------------------------------------
	-- Functions
	---------------------------------------------------------------------------
	--
	-- fn:	GeneraeAndSaveImages
	-- desc:Gets the viewport bmp and saves as jpeg for large and small images
	--
	fn GenerateAndSaveImages filename filenameSmall edgeFaces:false = (
	
		max hide inv
		max tool zoomextents

		-- Large img
		gw.setPos 0 0 640 480
		viewport.setShowEdgeFaces edgeFaces
		redrawviews()
		img = gw.getViewportDib()
		img.filename = filename
		save img
		
		-- Small img
		smallImg = bitmap 320 240
		copy img smallImg
		smallImg.filename = filenameSmall
		save smallImg
		
		close img
		close smallImg
		
		max unhide all
	)

	--
	-- fn:	GetObjDimensions
	-- desc:Returns the object's dimensions  
	--
	fn GetObjDimensions obj bb = (
	
		widthX 	= bb[2].x - bb[1].x
		lengthY = bb[2].y - bb[1].y		
		heightZ = bb[2].z - bb[1].z

		-- Incase these come out negative, make them positive
		if (widthX < 0 ) do (

			widthX *= -1
		)
		if (lengthY < 0 ) do (

			lengthY *= -1
		)
		if (heightZ < 0 ) do (
		
			heightZ *= -1
		)
		
		objDim = Point3 widthX lengthY heightZ		
	)

	--
	-- fn:	PositionScaleJaxx
	-- desc:Places the ScaleJaxx depending on the size of the object being rendered  
	--
	fn PositionScaleJaxx jaxx objDim objCentre = (
		
		jaxx.pos = objCentre
		
		-- Jaxx is pink if object is over 10 units
		if (objDim.z >= 10) then (
		
			jaxx.wirecolor = (color 255 64 200)
		)
		-- Yellow if between 2 and 10 units
		else if (objDim.z >= 2) then (
		
			jaxx.wirecolor = (color 200 200 32)
		)
		-- Green if smaller than 2 units
		else (
		
			jaxx.wirecolor = (color 0 128 0)	
		)
		
		-- If obj is longer (x axis) than longer (y axis), put jaxx in front/behind
		if (objDim.x >= objDim.y) then (
					
			if (objDim.z > jaxx.height) then (
			
				jaxx.pos.y -= (objDim.y / 2 + jaxx.length / 2)
			)
			else (
				jaxx.pos.y += (objDim.y / 2 + jaxx.length / 2)
			)
		)
		-- Otherwise, put jaxx to left of object
		else (
		
			jaxx.pos.x -= (objDim.x/2 + jaxx.width/2)
		)
		
		-- The origin isn't consistent for all objects - some have their z origin in their centre
		-- so make sure the jaxx is 'standing' at same level as the prop is, rather than just 
		-- relying on the origin to always start on the 'ground' plane
		jaxx.pos.z = objCentre.z - (objDim.z / 2)
	)
	
	--
	-- fn:	SetupCamera
	-- desc:Returns a camera and sets the viewport to use it for the render
	--
	fn SetupCamera objDim objCentre camDistanceFactor = (
	
		-- Get the largest dimension of the object and use that for positioning
		-- the camera.  This is used rather than individual dimensions due to 
		-- some very flat objects like widescreen TVs.
		camDistance = objDim.x
		if (objDim.y > camDistance) do (
			
			camDistance = objDim.y
		)
		if (objDim.z > camDistance) do (
					
			camDistance = objDim.z
		)	
	
		camTarget = targetObject pos:objCentre
		cam = TargetCamera target:camTarget
		camX = objCentre.x + (camDistance * camDistanceFactor)
		camY = objCentre.y - (camDistance * camDistanceFactor)
		camZ = objCentre.z + (camDistance * camDistanceFactor)
		cam.pos = Point3 camX camY camZ
		viewport.setcamera cam
		cam
	)
	
	--
	-- name: RenderSelected
	-- desc: Render selected objects out to network drive, two images.
	-- 	One rendered textured, another with edged faces and a ScaleJaxx.
	--
	fn RenderSelected = (
	
		sel = selection as array
		jpeg.setQuality 100
		viewport.setRenderLevel #smoothhighlights
		viewport.setType #view_persp_user
		viewport.setGridVisibility #all false
		windowSizeX = gw.getWinSizeX()
		windowSizeY = gw.getWinSizeY()
		camDistanceFactor = 2
		
		-- If the viewcube or stats is visible, turn it off for renders.  Turned back on at end
		-- if it was visible before this script was called.
		viewCubeVis = ViewCubeOps.Visibility
		if (viewCubeVis) do (
			
			ViewCubeOps.Visibility = false
		)
		
		progressStart "Rendering objects"
		currentObject = 1
		
		try (
			for obj in sel do 
			(			
				if getProgressCancel() then throw("progress_cancel")

				if ( "Gta Object" != GetAttrClass obj ) do
					continue

				if ( true == (GetAttr obj (GetAttrIndex "Gta Object" "Dont Export"))) do
					continue

				bb = nodeLocalBoundingBox obj
				objCentre = (bb[1]+ bb[2]) / 2
				objDim = GetObjDimensions obj bb			
				cam = SetupCamera objDim objCentre camDistanceFactor

				maxFileN = RsRemoveExtension( maxFilename )
				filenameConcat = edtRenderOutPath.text + "/" + RsMapLevel + "/" + maxFileN + "/" + obj.name			
				--filenameConcat = "C:/PropRenders/" + RsMapLevel + "/" + maxFileN + "/" + obj.name

				-- Default image		
				defaultFilename = filenameConcat + ".jpg"
				defaultFilenameSmall = filenameConcat + "_Small.jpg"
				RsMakeSurePathExists defaultFilename
				select obj			
				GenerateAndSaveImages defaultFilename defaultFilenameSmall edgeFaces:false
				format "Render: %\n" defaultFilename
				format "Render: %\n" defaultFilenameSmall

				-- ScaleJaxx image
				scaleFilename = filenameConcat + "_Jaxx.jpg"
				scaleFilenameSmall = filenameConcat + "_Jaxx_Small.jpg"
				jaxx = P_Standing()
				PositionScaleJaxx jaxx objDim objCentre
				selectmore jaxx
				GenerateAndSaveImages scaleFilename scaleFilenameSmall edgeFaces:true
				format "Render: %\n" scaleFilename
				format "Render: %\n" scaleFilenameSmall
				delete jaxx
				delete cam

				progressupdate ((100.0 * currentObject) / sel.count)
				currentObject += 1
			)
		)
		catch (progress_cancel)
		progressEnd()
		
		-- Set viewport back
		viewport.setShowEdgeFaces false
		viewport.setGridVisibility #all true
		ViewCubeOps.Visibility = viewCubeVis
		gw.setPos 0 0 windowSizeX windowSizeY
		redrawViews()		
	)

	---------------------------------------------------------------------------
	-- Events
	---------------------------------------------------------------------------
	
	on btnRenderSelected pressed do (
	
		-- Loop through all RsMapContainers for the scene.
		local maps = RsMapGetMapContainers()
		for map in maps do
		(
			if ( not map.is_exportable() ) then
				continue	
				
			RsMapSetupGlobals map
			RenderSelected()			
		)
		true
	)
	
	on btnRenderAll pressed do (
		
		-- Loop through all RsMapContainers for the scene.
		local maps = RsMapGetMapContainers()
		for map in maps do
		(
			if ( not map.is_exportable() ) then
				continue	
		
			RsMapSetupGlobals map
			max select all
			RenderSelected()
		)
		true
	)
)