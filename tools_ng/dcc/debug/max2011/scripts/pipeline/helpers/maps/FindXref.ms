--
-- File:: pipeline/helpers/maps/FindXref.ms
-- Description:: Find Xrefs
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com>
-- Date:: 22/9/2004
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/Func.ms"

-----------------------------------------------------------------------------
-- Global
-----------------------------------------------------------------------------
GtaXrefFoundName = #()
GtaXrefFoundItem = #()

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout GtaFindXrefRoll "Find XRefs" 
(
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Xref_Toolkit#Find_XRefs" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	edittext textFindName "Find Xref ObjectName:"
	button butFind "Find"
	listbox lstFound "Found" height:10
	
	on butFind pressed do (

		GtaXrefFoundName = #()
		GtaXrefFoundItem = #()
	
		checkName = GTAlowercase textFindName.text
	
		for obj in rootnode.children do (
		
			if classof obj == XrefObject do (
		
				checkNameB = GTAlowercase obj.objectname
	
				if obj.objectname == textFindName.text then (
				
					append GtaXrefFoundName obj.name
					append GtaXrefFoundItem obj
				)
			)
		)
		
		lstFound.items = GtaXrefFoundName
	)
	
	on lstFound selected item do (
		if (GtaXrefFoundItem[item] != undefined) then (
			select GtaXrefFoundItem[item]
			max zoomext sel
		)
	)
)