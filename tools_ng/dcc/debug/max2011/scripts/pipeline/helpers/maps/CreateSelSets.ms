--
-- File:: pipeline/helpers/maps/CreateSelSets.ms
-- Description::  Will create selection sets for a number of different types of objects
--
-- 23/1/2004
-- by Greg Smith <greg.smith@rockstarnorth.com>
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/UtilityFunc.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
GtaAllShadowMesh = #()
GtaAllCollisionMesh = #()
GtaAllCollisionBox = #()
GtaAllCollisionSphere = #()
GtaAllEntryExit = #()
GtaAllNormalObject = #()
GtaAllDontExport = #()
GtaAllNoRadiosity = #()
GtaAllCollGroup = #()
GtaAllIplGroup = #()
GtaAllBreakable = #()
GtaAllSpriteLights = #()
GtaAllLODParentParent = #()
GtaAllLODParent = #()
GtaAllLODChildren = #()
GtaAllNonLOD = #()
GtaAllLODNotLinked = #()
GtaAllDrawLast = #()
GtaAllTimeOfDay = #()
GtaAllEnvironmentMapped = #()
GtaAllCoverPoints = #()
GtaAllAttractors = #()
GtaAllRotatedXY = #()
GtaAllLowPriority = #()
GtaAllDynamic = #()
GtaAllOcclusion = #()
GtaAllMiloTris = #()
GtaAllPortals = #()
GtaAllMiloRooms = #()
GtaAllIRefParents = #()
GtaAllNoCollision = #()
GtaAllShadowCastingLights = #()

GtaAllAnimatedObject = #()
GtaAllUvAnimatedObject = #()
GtaAllFragmentProxy = #()
GtaAllAnimationProxy = #()

GtaCollGroupNames = #()
GtaIpllGroupNames = #()

GtaAddUniqueNames GtaCollGroupNames "Collision Group"
GtaAddUniqueNames GtaIpllGroupNames "IPL Group"

for val in GtaCollGroupNames do (
	append GtaAllCollGroup #()
)

for val in GtaIpllGroupNames do (
	append GtaAllIplGroup #()
)

fn GtaAddNodeToSelSets obj level = (

	if isdeleted obj == true then (
		return 0
	)

	if getattrclass obj == "Gta Light" then (
	
		if getattr obj (getattrindex "Gta Light" "Cast Static Object Shadow") or getattr obj (getattrindex "Gta Light" "Cast Dynamic Object Shadow") then (
	
			append GtaAllShadowCastingLights obj 
		)
	)

	if getattrclass obj == "Gta LightPhoto" then (
	
		if getattr obj (getattrindex "Gta LightPhoto" "Cast Static Object Shadow") or getattr obj (getattrindex "Gta LightPhoto" "Cast Dynamic Object Shadow") then (
	
			append GtaAllShadowCastingLights obj 
		)
	)

	if classof obj == Box then (
		append GtaAllOcclusion obj
	)

	if classof obj == Gta_Cover then (
		append GtaAllCoverPoints obj
	)

	if classof obj == Gta_Attractor then (
		append GtaAllAttractors obj
	)
	
	if classof obj == GtaMloPortal then (
		append GtaAllPortals obj
	)
	
	if classof obj == GtaMloRoom then (
		append GtaAllMiloRooms obj
	)

	if level == 1 then (
	
		if classof obj == InternalRef then (
		
			checkobj = ixref_gettarget obj
		
			if checkobj != undefined then (
		
				if finditem GtaAllIRefParents checkobj == 0 then (
				
					append GtaAllIRefParents checkobj
				)
			)
		)
	
		idxDontExport = -1
		idxNoRadiosity = -1
		idxCollGroup = -1
		idxIplGroup = -1
		idxDrawLast = -1
		idxLowPriority = -1
		idxLODDistance = -1
	
		if GtaDoesObjectHaveEnvMap obj == true then (
			append GtaAllEnvironmentMapped obj 
		)	
	
		if getattrclass obj == "Gta MILOTri" then (
		
			append GtaAllMiloTris obj
		)
	
		if getattrclass	obj == "Gta Object" then (
		
			eulerAngles = quattoeuler obj.rotation
		
			if abs(eulerAngles.x) > 0.001 then (
				append GtaAllRotatedXY obj
			)
		
			if abs(eulerAngles.y) > 0.001 then (
				append GtaAllRotatedXY obj
			)
		
			hasChildren = false
		
			for childobj in obj.children do (
			
				if getattrclass childobj == "Gta Object" then (
				
					hasChildren = true
				)
			)
			
			if level == 1 then (
			
				hasCollision = false
			
				for childobj in obj.children do (
				
					if getattrclass childobj == "Gta Collision" then (
					
						hasCollision = true
					)	
				)
				
				if hasCollision == false then (
				
					append GtaAllNoCollision obj	
				)
			)
			
			if hasChildren == true then (
				
			) else (
				
			)
		
			append GtaAllNormalObject obj
		
			idxTimeOfDay = GetAttrIndex "Gta Object" "Is time object"
			idxDontExport = GetAttrIndex "Gta Object" "Dont Export"
			idxNoRadiosity = GetAttrIndex "Gta Object" "Dont Apply Radiosity"
			idxCollGroup = GetAttrIndex "Gta Object" "Collision Group"
			idxIplGroup = GetAttrIndex "Gta Object" "IPL Group"
			idxIsBreakable = GetAttrIndex "Gta Object" "Breakable"
			idxDrawLast = GetAttrIndex "Gta Object" "Draw last"
			idxLowPriority = GetAttrIndex "Gta Object" "Low Priority"
			idxIsDynamic = GetAttrIndex "Gta Object" "Is Dynamic"
			
			idxHasAnim = getattrindex "Gta Object" "Has Anim"
			idxHasUvAnim = GetAttrIndex "Gta Object" "Has UvAnim"
			idxIsFragProxy = getattrindex "Gta Object" "Is FragProxy"
			idxIsAnimProxy = getattrindex "Gta Object" "Is AnimProxy"
						
			valTimeOfDay = GetAttr obj idxTimeOfDay
			valLODDistance = RsGetObjLodDistance obj

			if valTimeOfDay == true then (
			
				append GtaAllTimeOfDay obj
			)
			
			if getattr obj idxIsFragProxy then (
			
				append GtaAllFragmentProxy obj
			)

			if getattr obj idxIsAnimProxy then (
			
				append GtaAllAnimationProxy obj
			)
			
			if getattr obj idxHasAnim then (
			
				append GtaAllAnimatedObject obj
			)
	
			if getattr obj idxHasUvAnim then (
			
				append GtaAllUvAnimatedObject  obj
			)
	
			valBreakable = GetAttr obj idxIsBreakable
			
			if valBreakable == true then (
			
				append GtaAllBreakable obj
			)

			parent = GetLodAttrParent obj
			children = getLODChildren obj
			
			if children.count == 0 then (
			
				if valLODDistance > 300.0 then (
				
					if classof obj != XRefObject then (
				
						append GtaAllLODNotLinked obj
					)
				) else (
					if parent == undefined then (
					
						append GtaAllNonLOD obj
					) else (
					
						append GtaAllLODChildren obj
					)
				)
			) else (
			
				superlod = false
	
				if parent == undefined then (
			
					for childobj in children do (

						ccobjs = getLODChildren childobj

						if ccobjs.count > 0 then (

							superlod = true	
						)
					)
				)
			
				if superlod then (
				
					append GtaAllLODParentParent obj
				) else (
				
					append GtaAllLODParent obj
				)
			)
		)
		
		if idxDontExport != -1 then (
		
			valDontExport = GetAttr obj idxDontExport
			valNoRadiosity = GetAttr obj idxNoRadiosity
			valCollGroup = GetAttr obj idxCollGroup
			valIplGroup = GetAttr obj idxIplGroup
			valDrawLast = GetAttr obj idxDrawLast
			valLowPriority = GetAttr obj idxLowPriority
			valIsDynamic = GetAttr obj idxIsDynamic
			
			if valLowPriority == true then (
			
				append GtaAllLowPriority obj
			)
			
			if valIsDynamic == true then (
			
				append GtaAllDynamic obj
			)
			
			if valCollGroup == "" then (
			
				valCollGroup = "undefined"
			)
			
			if valIplGroup == "" then (
			
				valIplGroup = "undefined"
			)
			
			valCollGroup = GTAlowercase valCollGroup
			valIplGroup = GTAlowercase valIplGroup
			
			for i = 1 to GtaCollGroupNames.count do (
			
				collgroupName = GtaCollGroupNames[i]
				
				if collgroupName == valCollGroup then (
				
					append GtaAllCollGroup[i] obj
				)
			)
			
			for i = 1 to GtaIpllGroupNames.count do (
			
				iplgroupName = GtaIpllGroupNames[i]
				
				if iplgroupName == valIplGroup then (
				
					append GtaAllIplGroup[i] obj
				)
			)
			
			if valDrawLast == true then (
				append GtaAllDrawLast obj
			)
			
			if valDontExport == true then (
				append GtaAllDontExport obj
			)
			
			if valNoRadiosity == true then (
				append GtaAllNoRadiosity obj
			)
		)
		
		if classof obj == Gta_EntryExit2 then (
			append GtaAllEntryExit obj
		)
		
		if classof obj == entryexit_plugin_def then (
			append GtaAllEntryExit obj
		)
	)

	if level == 2 then (
	
		if classof obj == Shad_Mesh then (	
			append GtaAllShadowMesh obj
		)
	
		if classof obj == Col_Sphere then (
			append GtaAllCollisionSphere obj
		)
		
		if classof obj == Col_Box then (
			append GtaAllCollisionBox obj
		)
		
		if classof obj == Col_Mesh then (
			append GtaAllCollisionMesh obj
		)
		
		if superclassof obj == light then (
			append GtaAllSpriteLights obj
		)
	)

	for childobj in obj.children do (
	
		GtaAddNodeToSelSets childobj (level + 1)
	)
)

fn GtaCreateSelectionSets = (
	
	GtaAllShadowMesh = #()
	GtaAllCollisionMesh = #()
	GtaAllCollisionBox = #()
	GtaAllCollisionSphere = #()
	GtaAllEntryExit = #()
	GtaAllAnimatedObject = #()
	GtaAllNormalObject = #()
	GtaAllDontExport = #()
	GtaAllNoRadiosity = #()
	GtaAllCollGroup = #()
	GtaAllIplGroup = #()
	GtaAllBreakable = #()
	GtaAllSpriteLights = #()
	GtaAllLODParentParent = #()
	GtaAllLODParent = #()
	GtaAllLODChildren = #()
	GtaAllNonLOD = #()
	GtaCollGroupNames = #()
	GtaIpllGroupNames = #()
	GtaAllDrawLast = #()
	GtaAllTimeOfDay = #()
	GtaAllLODNotLinked = #()
	GtaAllEnvironmentMapped = #()
	GtaAllCoverPoints = #()
	GtaAllAttractors = #()
	GtaAllRotatedXY = #()
	GtaAllLowPriority = #()
	GtaAllDynamic = #()
	GtaAllOcclusion = #()
	GtaAllMiloTris = #()
	GtaAllPortals = #()
	GtaAllMiloRooms = #()
	GtaAllFragmentProxy = #()
	GtaAllAnimationProxy = #()
	GtaAllIRefParents = #()
	GtaAllNoCollision = #()
	GtaAllShadowCastingLights = #()
	
	-- create the arrays for each selection set

	for obj in rootnode.children do (
	
		GtaAddNodeToSelSets obj 1
	)
	
	-- set up the selection sets

	if GtaAllIRefParents.count == 0 then (
		if selectionsets["GtaAllIRefParents"] != undefined then (
			deleteitem selectionsets "GtaAllIRefParents"
		)	
	) else (
		selectionsets["GtaAllIRefParents"] = GtaAllIRefParents
	)

	if GtaAllDynamic.count == 0 then (
		if selectionsets["GtaAllDynamic"] != undefined then (
			deleteitem selectionsets "GtaAllDynamic"
		)	
	) else (
		selectionsets["GtaAllDynamic"] = GtaAllDynamic
	)


	if GtaAllLowPriority.count == 0 then (
		if selectionsets["GtaAllLowPriority"] != undefined then (
			deleteitem selectionsets "GtaAllLowPriority"
		)	
	) else (
		selectionsets["GtaAllLowPriority"] = GtaAllLowPriority
	)

	if GtaAllRotatedXY.count == 0 then (
		if selectionsets["GtaAllRotatedXY"] != undefined then (
			deleteitem selectionsets "GtaAllRotatedXY"
		)	
	) else (
		selectionsets["GtaAllRotatedXY"] = GtaAllRotatedXY
	)

	if GtaAllCoverPoints.count == 0 then (
		if selectionsets["GtaAllCoverPoints"] != undefined then (
			deleteitem selectionsets "GtaAllCoverPoints"
		)	
	) else (
		selectionsets["GtaAllCoverPoints"] = GtaAllCoverPoints
	)

	if GtaAllAttractors.count == 0 then (
		if selectionsets["GtaAllAttractors"] != undefined then (
			deleteitem selectionsets "GtaAllAttractors"
		)	
	) else (
		selectionsets["GtaAllAttractors"] = GtaAllAttractors
	)

	if GtaAllLODNotLinked.count == 0 then (
		if selectionsets["GtaAllLODNotLinked"] != undefined then (
			deleteitem selectionsets "GtaAllLODNotLinked"
		)	
	) else (
		selectionsets["GtaAllLODNotLinked"] = GtaAllLODNotLinked
	)

	if GtaAllEnvironmentMapped.count == 0 then (
		if selectionsets["GtaAllEnvironmentMapped"] != undefined then (
			deleteitem selectionsets "GtaAllEnvironmentMapped"
		)	
	) else (
		selectionsets["GtaAllEnvironmentMapped"] = GtaAllEnvironmentMapped
	)

	if GtaAllTimeOfDay.count == 0 then (
		if selectionsets["GtaAllTimeOfDay"] != undefined then (
			deleteitem selectionsets "GtaAllTimeOfDay"
		)	
	) else (
		selectionsets["GtaAllTimeOfDay"] = GtaAllTimeOfDay
	)

	if GtaAllDrawLast.count == 0 then (
		if selectionsets["GtaAllDrawLast"] != undefined then (
			deleteitem selectionsets "GtaAllDrawLast"
		)	
	) else (
		selectionsets["GtaAllDrawLast"] = GtaAllDrawLast
	)
	
	if GtaAllCollisionMesh.count == 0 then (
		if selectionsets["GtaAllCollMesh"] != undefined then (
			deleteitem selectionsets "GtaAllCollMesh"
		)	
	) else (
		selectionsets["GtaAllCollMesh"] = GtaAllCollisionMesh
	)
	
	if GtaAllCollisionBox.count == 0 then (
		if selectionsets["GtaAllCollBox"] != undefined then (
			deleteitem selectionsets "GtaAllCollBox"
		)
	) else (
		selectionsets["GtaAllCollBox"] = GtaAllCollisionBox
	)
	
	if GtaAllFragmentProxy.count == 0 then (
		if selectionsets["GtaAllFragmentProxy"] != undefined then (
			deleteitem selectionsets "GtaAllFragmentProxy"
		)
	) else (
		selectionsets["GtaAllFragmentProxy"] = GtaAllFragmentProxy
	)

	if GtaAllAnimationProxy.count == 0 then (
		if selectionsets["GtaAllAnimationProxy"] != undefined then (
			deleteitem selectionsets "GtaAllAnimationProxy"
		)
	) else (
		selectionsets["GtaAllAnimationProxy"] = GtaAllAnimationProxy
	)
	
	if GtaAllCollisionSphere.count == 0 then (
		if selectionsets["GtaAllCollSphere"] != undefined then (
			deleteitem selectionsets "GtaAllCollSphere"
		)
	) else (
		selectionsets["GtaAllCollSphere"] = GtaAllCollisionSphere
	)
	
	if GtaAllEntryExit.count == 0 then (
		if selectionsets["GtaAllEntryExit"] != undefined then (
			deleteitem selectionsets "GtaAllEntryExit"
		)
	) else (
		selectionsets["GtaAllEntryExit"] = GtaAllEntryExit
	)
	
	if GtaAllAnimatedObject.count == 0 then (
		if selectionsets["GtaAllAnimatedObject"] != undefined then (
			deleteitem selectionsets "GtaAllAnimatedObject"
		)
	) else (
		selectionsets["GtaAllAnimatedObject"] = GtaAllAnimatedObject
	)
	
	if GtaAllUvAnimatedObject.count == 0 then (
		if selectionsets["GtaAllUvAnimatedObject"] != undefined then (
			deleteitem selectionsets "GtaAllUvAnimatedObject"
		)
	) else (
		selectionsets["GtaAllUvAnimatedObject"] = GtaAllUvAnimatedObject
	)
	
	if GtaAllNormalObject.count == 0 then (
		if selectionsets["GtaAllNormalObject"] != undefined then (
			deleteitem selectionsets "GtaAllNormalObject"
		)
	) else (
		selectionsets["GtaAllNormalObject"] = GtaAllNormalObject
	)
	
	if GtaAllDontExport.count == 0 then (
		if selectionsets["GtaAllDontExport"] != undefined then (
			deleteitem selectionsets "GtaAllDontExport"
		)
	) else (
		selectionsets["GtaAllDontExport"] = GtaAllDontExport
	)

	if GtaAllNoRadiosity.count == 0 then (
		if selectionsets["GtaAllNoRadiosity"] != undefined then (
			deleteitem selectionsets "GtaAllNoRadiosity"
		)
	) else (
		selectionsets["GtaAllNoRadiosity"] = GtaAllNoRadiosity
	)

	if GtaAllBreakable.count == 0 then (
		if selectionsets["GtaAllBreakable"] != undefined then (
			deleteitem selectionsets "GtaAllBreakable"
		)
	) else (
		selectionsets["GtaAllBreakable"] = GtaAllBreakable
	)

	if GtaAllSpriteLights.count == 0 then (
		if selectionsets["GtaAllSpriteLights"] != undefined then (
			deleteitem selectionsets "GtaAllSpriteLights"
		)
	) else (
		selectionsets["GtaAllSpriteLights"] = GtaAllSpriteLights
	)

	if GtaAllLODParentParent.count == 0 then (
		if selectionsets["GtaAllLODParentParent"] != undefined then (
			deleteitem selectionsets "GtaAllLODParentParent"
		)
	) else (
		selectionsets["GtaAllLODParentParent"] = GtaAllLODParentParent
	)	
	
	if GtaAllLODParent.count == 0 then (
		if selectionsets["GtaAllLODParent"] != undefined then (
			deleteitem selectionsets "GtaAllLODParent"
		)
	) else (
		selectionsets["GtaAllLODParent"] = GtaAllLODParent
	)	
	
	if GtaAllLODChildren.count == 0 then (
		if selectionsets["GtaAllLODChildren"] != undefined then (
			deleteitem selectionsets "GtaAllLODChildren"
		)
	) else (
		selectionsets["GtaAllLODChildren"] = GtaAllLODChildren
	)	
	
	if GtaAllNonLOD.count == 0 then (
		if selectionsets["GtaAllNonLOD"] != undefined then (
			deleteitem selectionsets "GtaAllNonLOD"
		)
	) else (
		selectionsets["GtaAllNonLOD"] = GtaAllNonLOD
	)		

	if GtaAllOcclusion.count == 0 then (
		if selectionsets["GtaAllOcclusion"] != undefined then (
			deleteitem selectionsets "GtaAllOcclusion"
		)
	) else (
		selectionsets["GtaAllOcclusion"] = GtaAllOcclusion
	)
	
	if GtaAllMiloTris.count == 0 then (
		if selectionsets["GtaAllMiloTris"] != undefined then (
			deleteitem selectionsets "GtaAllMiloTris"
		)	
	) else (
		selectionsets["GtaAllMiloTris"] = GtaAllMiloTris
	)
	
	if GtaAllShadowMesh.count == 0 then (
		if selectionsets["GtaAllShadowMesh"] != undefined then (
			deleteitem selectionsets "GtaAllShadowMesh"
		)
	) else (
		selectionsets["GtaAllShadowMesh"] = GtaAllShadowMesh
	)
	
	if GtaAllPortals.count == 0 then (
		if selectionsets["GtaAllPortals"] != undefined then (
			deleteitem selectionsets "GtaAllPortals"
		)
	) else (
		selectionsets["GtaAllPortals"] = GtaAllPortals
	)

	if GtaAllMiloRooms.count == 0 then (
		if selectionsets["GtaAllMiloRooms"] != undefined then (
			deleteitem selectionsets "GtaAllMiloRooms"
		)
	) else (
		selectionsets["GtaAllMiloRooms"] = GtaAllMiloRooms
	)

	if GtaAllNoCollision.count == 0 then (
		if selectionsets["GtaAllNoCollision"] != undefined then (
			deleteitem selectionsets "GtaAllNoCollision"
		)
	) else (
		selectionsets["GtaAllNoCollision"] = GtaAllNoCollision
	)
	
	if GtaAllShadowCastingLights.count == 0 then (
		if selectionsets["GtaAllShadowCastingLights"] != undefined then (
			deleteitem selectionsets "GtaAllShadowCastingLights"
		)
	) else (
		selectionsets["GtaAllShadowCastingLights"] = GtaAllShadowCastingLights
	)	
	
	numSelSets = getnumnamedselsets()
	delSelItems = #()
	
	for i = 1 to numSelSets do (
	
		selsetname = getnamedselsetname i
		
		if selsetname != undefined then (
			
			subname = substring selsetname 1 15
			
			if subname == "GtaAllCollGroup" then (
				append delSelItems selsetname 
			) else (	
				subname = substring selsetname 1 14
				
				if subname == "GtaAllIplGroup" then (
					append delSelItems selsetname 
				)
			)
		)
	)
	
	for deleteSelItem in delSelItems do (
		deleteitem selectionsets deleteSelItem 
	)
	
	for i = 1 to GtaCollGroupNames.count do (
		selsetname = "GtaAllCollGroup" + GtaCollGroupNames[i]
		selectionsets[selsetname] = GtaAllCollGroup[i]
	)

	for i = 1 to GtaIpllGroupNames.count do (
		selsetname = "GtaAllIplGroup" + GtaIpllGroupNames[i]
		selectionsets[selsetname] = GtaAllIplGroup[i]
	)
)

GtaCreateSelectionSets()