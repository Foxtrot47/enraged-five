-- BakeTextureToCPV 
-- GunnarD
-- 10/06/2010
-- Baking textures to vertex channel

filein "rockstar/util/material.ms"
filein "rockstar/util/dialog.ms"
filein "rockstar/helpers/terrain_helperFunctions.ms"

global vertQueryMethod

fn RsGetColourAtVert model vertnum = 
(
	local facelist = meshop.getFacesUsingVert model #{vertnum}
	-- lookup  all texmaps
	local textureNamesArraysMap = #()
	local textureArraysMap = #()
	for f in facelist do
	(
		local myMatId = getfacematid model f
		if textureArraysMap[myMatId]!=undefined then continue

		LocalTexMapNames = #()
		LocalTexMapsBmps = #()
		collMaterial = "default"
		
		RsSetMapsFlags false true false false
		RsGetTexMapsFromObjWithMaps model LocalTexMapNames LocalTexMapsBmps id:myMatId --matID
		RsSetMapsFlags true true true true
-- 		print (model.name+", "+LocalTexMapNames.count as string)
-- 		for tex in LocalTexMapsBmps do print ("		"+tex.filename)

		local theMaterial = model.material
		if multimaterial==(classof theMaterial) then theMaterial = theMaterial.materialList[myMatId]
		local evaluated = false
		local shadername = theMaterial.name
		if Rage_Shader==(classof theMaterial) then
		(
			shadername = rstgetshadername theMaterial
			if matchPattern shadername pattern:"*terrain*" then
			(
				local theTexMap = GetDominantTextureFromTerrainFace model theMaterial myMatId f
				textureNamesArraysMap[myMatId] = theTexMap.name
				textureArraysMap[myMatId] = theTexMap.bitmap
--				print ("IS terrain shader!!!:"+shadername)
				evaluated = true
			)
		)
		if not evaluated then
		(
--			print ("not terrain shader?!?:"+shadername)
			textureNamesArraysMap[myMatId] = LocalTexMapNames[1]
			textureArraysMap[myMatId] = LocalTexMapsBmps[1].bitmap
		)
	)
	
--	print (textureNamesArraysMap as string)
	
	local vertColor = (color 1 1 1 1)
	for f in facelist do
	(
		local myMatId = getfacematid model f
		if textureArraysMap[myMatId] == undefined then
		(
			print ("matid "+myMatId+" hasn't got maps")
			continue
		)
		local vertIndeces = getFace model f
		local myIndex = if (vertIndeces.x==vertnum) then 1 else if (vertIndeces.y==vertnum) then 2 else if (vertIndeces.z==vertnum) then 3 else 0
		if myIndex==0 then 
		(
			print ("failed at :"+vertnum)
			return false
		)
		
		local myUvFace = (getTvFace model f)
		local myUv = getTVert model myUvFace[myIndex]
		local moduloedUvs = [(mod myUv[1] 1.0), (mod myUv[2] 1.0)]
		local firstBitmaptex = textureArraysMap[myMatId]
		if moduloedUvs[1] < 0 then moduloedUvs[1] = 1.0 + moduloedUvs[1]
		if moduloedUvs[2] < 0 then moduloedUvs[2] = 1.0 + moduloedUvs[2]
		moduloedUvs = moduloedUvs * (point2 firstBitmaptex.width firstBitmaptex.height)
		try(
			vertColor += (getPixels firstBitmaptex moduloedUvs 1)[1]
		)catch(print (getcurrentException()))
	)
--	print vertColor
--	print facelist.numberSet 
	return vertColor /= facelist.numberSet 
)

fn GetVertCloseToPos mesh pos = 
(
	local lastDistance = 999
	local lastIndex = 0
	for vIndex=1 to mesh.verts.count do
	(
		local currDistance = length ((getvert mesh vIndex)-pos)
		if vIndex==1 or currDistance<lastDistance then
		(
			lastDistance = currDistance
			lastIndex = vIndex
		)
	)
	return lastIndex
)

fn RsBakeTexturesToCPVForObject fromModel toModel:undefined mapChannel:0 =
(
	vertQueryMethod = #normalvertQuery
	if getnumverts CollSurfaceObj != getnumverts CollSurfaceObj.parent then
	(
		if (RsQueryBoxTimeOut "Resolution mismatch. Adjacency approximation (slow!)?") then
			vertQueryMethod = #adjacencyVertQuery
		else 
			return false
	)

	progressStart "Baking textures to CPV"
	
	if Editable_poly==(classof fromModel) then
	(
		if (RsQueryBoxTimeOut "The source object will get converted to an Editable mesh. Continue?") then
			converttomesh fromModel
		else
			return false
	)
	if Editable_poly==(classof toModel) then
	(
		if (RsQueryBoxTimeOut "The target object will get converted to an Editable mesh. Continue?") then
			converttomesh toModel
		else
			return false
	)

	if undefined==toModel then toModel=fromModel
	toModel.vertexColorType = #map_channel
	toModel.vertexColorMapChannel = mapChannel
	with redraw off
	undo off
	(
		for vertNum = 1 to toModel.verts.count while not getProgressCancel() do
		(
			local colour
			if #normalvertQuery==vertQueryMethod then
				colour = RsGetColourAtVert fromModel vertNum --((getVertSelection $) as array)[1]
			else
			(
				local closestVertIndex = GetVertCloseToPos fromModel (getVert toModel vertNum)
				colour = RsGetColourAtVert fromModel closestVertIndex
			)
			meshop.setVertColor toModel mapChannel #{vertNum} colour
			local progress = ((vertNum as Float)/(getnumverts frommodel))*100
			progressUpdate progress
		)
	)
	progressend()
)