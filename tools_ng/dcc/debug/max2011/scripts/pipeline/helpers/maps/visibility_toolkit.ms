--
-- File:: rockstar/helpers/vis.ms
-- Description:: Visibility Helper
--
-- Date:: 5/9/2006
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- 15/3/2010
-- by Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Added in Block Visibility
--
-- 26/6/2007
-- by David Muir <david.muir@rockstarnorth.com>
-- Map Block Visibility Helper
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/objects.ms"
filein "rockstar/util/lod.ms"
filein "rockstar/util/material.ms"
filein "pipeline/util/IdeUtils.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
fn RsGetAll rootobj alllist = (

	for childobj in rootobj.children do (
	
		append alllist childobj
		
		RsGetAll childobj alllist
	)
)

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- VISIBILITY
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout RsVisRoll "Visibility"
(
	--////////////////////////////////////////////////////////////
	-- data
	--////////////////////////////////////////////////////////////

	local ideFileList = #()
	local ideObjectList = #()

	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////

	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Visibility_Set" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnXrefs "Xrefs" width:100 align:#left
	checkbox chkHideXref "Hide" offset:[110,-22]
	checkbox chkFreeXref "Freeze" offset:[160,-20]
	button btnSpeedtree "Speedtree" width:100 align:#left
	checkbox chkHideSpeedtree "Hide" offset:[110,-22]
	checkbox chkFreeSpeedtree "Freeze" offset:[160,-20]
	button btnLods "Lods" width:100 align:#left
	checkbox chkHideLods "Hide" offset:[110,-22]
	checkbox chkFreeLods "Freeze" offset:[160,-20]
	button btnSuperlods "Superlods" width:100 align:#left
	checkbox chkHideSuperlods "Hide" offset:[110,-22]
	checkbox chkFreeSuperlods "Freeze" offset:[160,-20]
	button btnGridHelpers "Grid helpers" width:100 align:#left
	checkbox chkHideGridHelpers "Hide" offset:[110,-22]
	checkbox chkFreeGridHelpers "Freeze" offset:[160,-20]
	button btnPortals "Portals" width:100 align:#left
	checkbox chkHidePortals "Hide" offset:[110,-22]
	checkbox chkFreePortals "Freeze" offset:[160,-20]
	button btnCollision "Collision" width:100 align:#left
	checkbox chkHideCollision "Hide" offset:[110,-22]
	checkbox chkFreeCollision "Freeze" offset:[160,-20]
	button btnLeedsCollision "Leeds Collision" width:100 align:#left
	checkbox chkLeedsHideCollision "Hide" offset:[110,-22]
	checkbox chkLeedsFreeCollision "Freeze" offset:[160,-20]
	button btnOptCollision "Optimised Collision" width:100 align:#left
	checkbox chkHideOptCollision "Hide" offset:[110,-22]
	checkbox chkFreeOptCollision "Freeze" offset:[160,-20]
	button btnDontExport "Dont Export" width:100 align:#left
	checkbox chkHideDontExport "Hide" offset:[110,-22]
	checkbox chkFreeDontExport "Freeze" offset:[160,-20]
	button btnLeeds "Leeds" width:100 align:#left
	checkbox chkLeedsHide "Hide" offset:[110,-22]
	checkbox chkLeedsFree "Freeze" offset:[160,-20]
	button btnMilos "Milos" width:100 align:#left
	checkbox chkHideMilos "Hide" offset:[110,-22]
	checkbox chkFreeMilos "Freeze" offset:[160,-20]
	button btnLights "Lights" width:100 align:#left
	checkbox chkHideLights "Hide" offset:[110,-22]
	checkbox chkFreeLights "Freeze" offset:[160,-20]
	button btnUnattached "Unattached lights" width:100 align:#left
	checkbox chkHideUnattached "Hide" offset:[110,-22]
	checkbox chkFreeUnattached "Freeze" offset:[160,-20]
	button btnParticles "Particles" width:100 align:#left
	checkbox chkHideParticles "Hide" offset:[110,-22]
	checkbox chkFreeParticles "Freeze" offset:[160,-20]
	button btnInternalRef "Internal ref" width:100 align:#left
	checkbox chkHideInternalRef "Hide" offset:[110,-22]
	checkbox chkFreeInternalRef "Freeze" offset:[160,-20]
	button btnCarGen "cargen" width:100 align:#left
	checkbox chkHideCarGen "Hide" offset:[110,-22]
	checkbox chkFreeCarGen "Freeze" offset:[160,-20]
	button btnErrors "Errors" width:100 align:#left
	checkbox chkHideErrors "Hide" offset:[110,-22]
	checkbox chkFreeErrors "Freeze" offset:[160,-20]
	button btnBlocks "Blocks" width:100 align:#left
	checkbox chkHideBlocks "Hide" offset:[110,-22]
	checkbox chkFreeBlocks "Freeze" offset:[160,-20]
	button btnAlphad "Alpha'd" width:100 align:#left
	checkbox chkHideAlphad "Hide" offset:[110,-22]
	checkbox chkFreeAlphad "Freeze" offset:[160,-20]
	button btnDynamic "Dynamic" width:100 align:#left
	checkbox chkHideDynamic "Hide" offset:[110,-22]
	checkbox chkFreeDynamic "Freeze" offset:[160,-20]
	
	button btnReloadIDEs "Reload IDEs" width:100
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	on btnXrefs pressed do (

		objOut = #()
	
		for obj in rootnode.children do (
					
			if classof obj == XRefObject then append objOut obj
		)	
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	on chkHideXref changed val do (
	
		for obj in rootnode.children do (
					
			if classof obj == XRefObject then obj.isHidden = val
		)		
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	on chkFreeXref changed val do (
	
		for obj in rootnode.children do (
					
			if classof obj == XRefObject then obj.isFrozen = val			
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnSpeedtree pressed do (
	
		objOut = #()
	
		for obj in rootnode.children do (
					
			if classof obj == SpeedTree_4 then append objOut obj
			
			if classof obj == XRefObject then (
			
				refobj = obj.getsrcitem()
				
				if classof refobj == SpeedTree_4 then append objOut obj
			)
		)	
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHideSpeedtree changed val do (
	
		for obj in rootnode.children do (
					
			if classof obj == SpeedTree_4 then obj.isHidden = val
			
			if classof obj == XRefObject then (
						
				refobj = obj.getsrcitem()
				
				if classof refobj == SpeedTree_4 then obj.isHidden = val
			)
		)		
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeSpeedtree changed val do (
	
		for obj in rootnode.children do (
					
			if classof obj == SpeedTree_4 then obj.isFrozen = val			
			
			if classof obj == XRefObject then (
			
				refobj = obj.getsrcitem()
				
				if classof refobj == SpeedTree_4 then obj.isFrozen = val			
			)
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnLods pressed do (
	
		objOut = #()
	
		for obj in rootnode.children do (

			if RsIsLOD obj then append objOut obj
		)
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHideLods changed val do (
		
		for obj in rootnode.children do (

			if RsIsLOD obj then obj.isHidden = val
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeLods changed val do (
		
		for obj in rootnode.children do (
		
			if RsIsLOD obj then obj.isFrozen = val
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnSuperlods pressed do (
	
		objOut = #()
	
		for obj in rootnode.children do (

			if RsIsSuperLOD obj then append objOut obj
		)
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHideSuperlods changed val do (
	
		for obj in rootnode.children do (

			if RsIsSuperLOD obj then obj.isHidden = val
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeSuperlods changed val do (
	
		for obj in rootnode.children do (

			if RsIsSuperLOD obj then obj.isFrozen = val
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnGridHelpers pressed do (
	
		objOut = #()
	
		for obj in rootnode.children do (
					
			if classof obj == grid then append objOut obj
		)	
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHideGridHelpers changed val do (
	
		for obj in rootnode.children do (
					
			if classof obj == grid then obj.isHidden = val
		)		
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeGridHelpers changed val do (
	
		for obj in rootnode.children do (
					
			if classof obj == grid then obj.isFrozen = val
		)		
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnPortals pressed do (
	
		objOut = #()
		alllist = #()
		RsGetAll rootnode alllist
		
		for obj in alllist do (
		
			if classof obj == GtaMloPortal then append objOut obj
		)
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHidePortals changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
		
		for obj in alllist do (
		
			if classof obj == GtaMloPortal then obj.isHidden = val
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreePortals changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
		
		for obj in alllist do (
		
			if classof obj == GtaMloPortal then obj.isFrozen = val
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnCollision pressed do (
	
		objOut = #()
		alllist = #()
		RsGetAll rootnode alllist
		
		for obj in alllist do (
			
			if getattrclass obj == "Gta Collision" then append objOut obj
		)
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHideCollision changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
		
		for obj in alllist do (
			
			if getattrclass obj == "Gta Collision" then obj.isHidden = val
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeCollision changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
		
		for obj in alllist do (
			
			if getattrclass obj == "Gta Collision" then obj.isFrozen = val
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnLeedsCollision pressed do (
	
		idxLeeds = getattrindex "Gta Collision" "Edited by Leeds"
	
		objOut = #()
		alllist = #()
		RsGetAll rootnode alllist
		
		for obj in alllist do (
			
			if (getattrclass obj == "Gta Collision" and getattr obj idxLeeds == true) then append objOut obj
		)
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkLeedsHideCollision changed val do (
	
		idxLeeds = getattrindex "Gta Collision" "Edited by Leeds"
	
		alllist = #()
		RsGetAll rootnode alllist
		
		for obj in alllist do (
			
			if (getattrclass obj == "Gta Collision" and getattr obj idxLeeds == true) then obj.isHidden = val
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkLeedsFreeCollision changed val do (
	
		idxLeeds = getattrindex "Gta Collision" "Edited by Leeds"
	
		alllist = #()
		RsGetAll rootnode alllist
		
		for obj in alllist do (
			
			if (getattrclass obj == "Gta Collision" and getattr obj idxLeeds == true) then obj.isFrozen = val
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------			
	on btnOptCollision pressed do (
	
		objOut = #()
		alllist = #()
		RsGetAll rootnode alllist
		idxHandEdited = getattrindex "Gta Object" "Hand edited collision"
		
		for obj in alllist do (
			
			if getattrclass obj == "Gta Collision" then (
			
				if obj.parent != undefined and getattrclass obj.parent == "Gta Object" then (
			
					if getattr obj.parent idxHandEdited then (
					
						append objOut obj.parent
					)
				)
			)
		)
		
		select objOut				
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------			
	on chkHideOptCollision changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
		idxHandEdited = getattrindex "Gta Object" "Hand edited collision"

		for obj in alllist do (
			
			if getattrclass obj == "Gta Collision" then (
			
				if obj.parent != undefined and getattrclass obj.parent == "Gta Object" then (
			
					if getattr obj.parent idxHandEdited then (
				
						obj.parent.isHidden = val
					)
				)
			)
		)		
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------			
	on chkFreeOptCollision changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
		idxHandEdited = getattrindex "Gta Object" "Hand edited collision"
		
		for obj in alllist do (
			
			if getattrclass obj == "Gta Collision" then (
			
				if obj.parent != undefined and getattrclass obj.parent == "Gta Object" then (
			
					if getattr obj.parent idxHandEdited then (
				
						obj.parent.isFrozen = val
					)
				)
			)
		)		
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnDontExport pressed do (
	
		objOut = #()
		alllist = #()
		RsGetAll rootnode alllist
		
		idxDontExport = getattrindex "Gta Object" "Dont Export"
		
		for obj in alllist do (
						
			if getattrclass obj == "Gta Object" then (
			
				valDontExport = getattr obj idxDontExport
				
				if valDontExport == true then append objOut obj
			
			)
		)
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHideDontExport changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
		
		idxDontExport = getattrindex "Gta Object" "Dont Export"
		
		for obj in alllist do (
						
			if getattrclass obj == "Gta Object" then (
			
				valDontExport = getattr obj idxDontExport
				
				if valDontExport == true then obj.isHidden = val
			
			)
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeDontExport changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
		
		idxDontExport = getattrindex "Gta Object" "Dont Export"
		
		for obj in alllist do (
						
			if getattrclass obj == "Gta Object" then (
			
				valDontExport = getattr obj idxDontExport
				
				if valDontExport == true then obj.isFrozen = val
			
			)
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnLeeds pressed do (
	
		objOut = #()
		alllist = #()
		RsGetAll rootnode alllist
		
		idxLeeds = getattrindex "Gta Object" "Edited by Leeds"
		
		for obj in alllist do (
						
			if getattrclass obj == "Gta Object" then (
							
				if getattr obj idxLeeds == true then append objOut obj
			)
		)
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkLeedsHide changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
		
		idxLeeds = getattrindex "Gta Object" "Edited by Leeds"
		
		for obj in alllist do (
						
			if getattrclass obj == "Gta Object" then (
							
				if getattr obj idxLeeds == true then obj.isHidden = val
			)
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkLeedsFree changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
		
		idxLeeds = getattrindex "Gta Object" "Edited by Leeds"
		
		for obj in alllist do (
						
			if getattrclass obj == "Gta Object" then (
					
				if getattr obj idxLeeds == true then obj.isFrozen = val		
			)
		)
	)	

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnMilos pressed do (
	
		objOut = #()
	
		for obj in rootnode.children do (
					
			if classof obj == Gta_MILO then append objOut obj
		)	
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHideMilos changed val do (
	
		for obj in rootnode.children do (
					
			if classof obj == Gta_MILO then obj.isHidden = val
		)	
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeMilos changed val do (
	
		for obj in rootnode.children do (
					
			if classof obj == Gta_MILO then obj.isFrozen = val
		)		
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnLights pressed do (
	
		objOut = #()
		alllist = #()
		RsGetAll rootnode alllist
	
		for obj in alllist do (
					
			if superclassof obj == light then append objOut obj
		)
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHideLights changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
	
		for obj in alllist do (
					
			if superclassof obj == light then obj.isHidden = val
		)	
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeLights changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist	
	
		for obj in alllist do (
					
			if superclassof obj == light then obj.isFrozen = val
		)	
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnUnattached pressed do (
	
		objOut = #()
	
		for obj in rootnode.children do (
					
			if superclassof obj == light then append objOut obj
		)	
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHideUnattached changed val do (
	
		for obj in rootnode.children do (
					
			if superclassof obj == light then obj.isHidden = val
		)	
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeUnattached changed val do (
	
		for obj in rootnode.children do (
					
			if superclassof obj == light then obj.isFrozen = val
		)	
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnParticles pressed do (
	
		objOut = #()
		alllist = #()
		RsGetAll rootnode alllist
	
		for obj in alllist do (
					
			if getattrclass obj == "RAGE Particle" then append objOut obj
		)	
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHideParticles changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
	
		for obj in alllist do (
					
			if getattrclass obj == "RAGE Particle" then obj.isHidden = val
		)	
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeParticles changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
	
		for obj in alllist do (
					
			if getattrclass obj == "RAGE Particle" then obj.isFrozen = val
		)	
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnInternalRef pressed do (
	
		objOut = #()
	
		for obj in rootnode.children do (
					
			if classof obj == InternalRef then append objOut obj
		)	
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHideInternalRef changed val do (
			
		for obj in rootnode.children do (
					
			if classof obj == InternalRef then obj.isHidden = val
		)	
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeInternalRef changed val do (
	
		for obj in rootnode.children do (
					
			if classof obj == InternalRef then obj.isFrozen = val
		)	
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnCarGen pressed do (
	
		objOut = #()
		alllist = #()
		RsGetAll rootnode alllist
	
		for obj in alllist do (
					
			if classof obj == GtaCarGen then append objOut obj
		)	
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHideCarGen changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
	
		for obj in alllist do (
					
			if classof obj == GtaCarGen then obj.isHidden = val
		)	
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeCarGen changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
	
		for obj in alllist do (
					
			if classof obj == GtaCarGen then obj.isFrozen = val
		)	
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnErrors pressed do (
	
		objOut = #()
	
		if RsMapErrorList != undefined then (
		
			for obj in RsMapErrorList do (

				append objOut obj
			)				
		)
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHideErrors changed val do (
	
		if RsMapErrorList != undefined then (
		
			for obj in RsMapErrorList do (

				obj.isHidden = val
			)				
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeErrors changed val do (
	
		if RsMapErrorList != undefined then (
		
			for obj in RsMapErrorList do (

				obj.isFrozen = val
			)				
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	on btnBlocks pressed do (

		objOut = #()
	
		for obj in rootnode.children do (
					
			if classof obj == GtaBlock then append objOut obj
		)	
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	on chkHideBlocks changed val do (
	
		for obj in rootnode.children do (
					
			if classof obj == GtaBlock then obj.isHidden = val
		)		
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	on chkFreeBlocks changed val do (
	
		for obj in rootnode.children do (
					
			if classof obj == GtaBlock then obj.isFrozen = val			
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnAlphad pressed do (
	
		objOut = #()
		alllist = #()
		RsGetAll rootnode alllist
		
		for obj in alllist do (
						
			if getattrclass obj == "Gta Object" then (
							
				if RsCheckIsAlphad obj then append objOut obj
			)
		)
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkHideAlphad changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
		
		idxDontExport = getattrindex "Gta Object" "Dont Export"
		
		for obj in alllist do (
						
			if getattrclass obj == "Gta Object" then (
							
				if RsCheckIsAlphad obj then obj.isHidden = val
			)
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeAlphad changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
		
		idxDontExport = getattrindex "Gta Object" "Dont Export"
		
		for obj in alllist do (
						
			if getattrclass obj == "Gta Object" then (
							
				if RsCheckIsAlphad obj then obj.isFrozen = val
			)
		)
	)


	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnDynamic pressed do (
	
		objOut = #()
		alllist = #()
		RsGetAll rootnode alllist
		
		idxDynamic = GetAttrIndex "Gta Object" "Is Dynamic"
		
		for obj in alllist do (

			if ( "Gta Object" == GetAttrClass obj ) then (
				
				if ( XRefObject == classof obj ) then
				(
					-- Need to read flags from IDE files
					local ideObject = GtaFindIDEObject ideObjectList obj.objectName
					if ( ( undefined != ideObject ) and ( ideObject.hasFlag ideObject.FLAG_ISDYNAMIC ) ) then
						append objOut obj	
				)
				else
				(
					-- Read attributes normally
					if ( GetAttr obj idxDynamic ) then 
						append objOut obj
				)				
			)
		)
		
		select objOut
	)
	
	--------------------------------------------------------------
	-- Hide/unhide Dynamic Objects
	--------------------------------------------------------------		
	on chkHideDynamic changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
		
		idxDynamic = GetAttrIndex "Gta Object" "Is Dynamic"
		
		for obj in alllist do (
						
			if ( "Gta Object" == GetAttrClass obj ) then (
				
				if ( XRefObject == classof obj ) then
				(
					-- Need to read flags from IDE files
					local ideObject = GtaFindIDEObject ideObjectList obj.objectName
					if ( ( undefined != ideObject ) and ( ideObject.hasFlag ideObject.FLAG_ISDYNAMIC ) ) then
						obj.isHidden = val		
				)
				else
				(
					-- Read attributes normally
					if ( GetAttr obj idxDynamic ) then 
						obj.isHidden = val
				)				
			)
		)
	)
	
	--------------------------------------------------------------
	-- Freeze/unfreeze Dynamic Objects
	--------------------------------------------------------------		
	on chkFreeDynamic changed val do (
	
		alllist = #()
		RsGetAll rootnode alllist
		
		idxDynamic = GetAttrIndex "Gta Object" "Is Dynamic"
		
		for obj in alllist do (
						
			if ( "Gta Object" == GetAttrClass obj ) then (
				
				if ( XRefObject == classof obj ) then
				(				
					-- Need to read flags from IDE files
					local ideObject = GtaFindIDEObject ideObjectList obj.objectName
					if ( ( undefined != ideObject ) and ( ideObject.hasFlag ideObject.FLAG_ISDYNAMIC ) ) then
						obj.isFrozen = val
				)
				else
				(
					-- Read attributes normally
					if ( GetAttr obj idxDynamic ) then 
						obj.isFrozen = val				
				)				
			)
		)
	)
	
	--////////////////////////////////////////////////////////////
	-- utility button events
	--////////////////////////////////////////////////////////////
	on btnReloadIDEs pressed do
	(
		-- Reload IDE Files
		GtaLoadIDEFiles GTA_MapsRoot &ideFileList &ideObjectList
	)
	
	--////////////////////////////////////////////////////////////
	-- rollout events
	--////////////////////////////////////////////////////////////
	on RsVisRoll open do
	(
		-- Preload IDE Files
		GtaLoadIDEFiles GTA_MapsRoot &ideFileList &ideObjectList
	)
	
)


--
-- rollout:
-- desc:
--
rollout RsPropGroupRoll "PropGroup"
(
	---------------------------------------------------------------------------
	-- Widgets
	---------------------------------------------------------------------------
	dropdownlist 	lstPropGroups "Prop Groups:" items:#()
	button				btnRefresh 		"Refresh" width:100
	button 				btnSelect 		"Select" width:100 across:2
	button				btnSelOrphan	"Select Orphans" width:100
	checkbox 			chkHide				"Hide" across:2
	checkbox			chkFreeze			"Freeze"
	button				btnColorise		"Auto-Colorise" width:100 across:2
	button				btnImport			"Import" width:100
	button				btnSave				"Save" width:100 across:2 
	button				btnDelete			"Delete" width:100
	
	---------------------------------------------------------------------------
	-- Functions
	---------------------------------------------------------------------------	
	
	--
	-- name: GetPropGroupObjects
	-- desc: Return array of prop group objects.
	--
	fn GetPropGroupObjects groupName = (
	
		RsMapObjectGetGroupObjects groupName
	)
	
	--
	-- name: RefreshPropGroupList
	-- desc: Refresh the prop group drop down list.
	--
	fn RefreshPropGroupList = (
		
		lstPropGroups.items = RsMapObjectGetPropGroups()
	)
	
	--
	-- name: SelectPropGroupObjects
	-- desc: Select all of the objects in the specified prop group
	--
	fn SelectPropGroupObjects groupName = (
	
		local attrObjectPropGroupIdx = ( GetAttrIndex "Gta Object" "Prop Group" )
		local attrMiloPropGroupIdx = ( GetAttrIndex "Gta MILOTri" "Prop Group" )
		
		clearSelection()
		objset = GetPropGroupObjects groupName
		select objset
	)
	
	---------------------------------------------------------------------------
	-- Events
	---------------------------------------------------------------------------
	
	--
	-- event: btnRefresh pressed
	-- desc: Refresh prop group list.
	--
	on btnRefresh pressed do
	(
		RefreshPropGroupList()
	)
	
	--
	-- event: btnSelect pressed
	-- desc: Select prop group.
	--
	on btnSelect pressed do
	(
		SelectPropGroupObjects lstPropGroups.selected
	)
	
	--
	-- event: btnSelOrphan pressed
	-- desc: Select XRefs not in a prop group.
	--
	on btnSelOrphan pressed do
	(
		local attrObjectPropGroupIdx = ( GetAttrIndex "Gta Object" "Prop Group" )
		local attrMiloPropGroupIdx = ( GetAttrIndex "Gta MILOTri" "Prop Group" )
		local defaultGroupName = "undefined"
		
		clearSelection()
		for o in $objects do
		(
			if ( XRefObject != classof o ) then
				continue
			
			if ( "Gta Object" == GetAttrClass o ) then
			(
				local groupName = ( GetAttr o attrObjectPropGroupIdx )
				if ( defaultGroupName == groupName ) then
					selectMore o
			)
			else if ( "Gta MILOTri" == GetAttrClass o ) then
			(
				local groupName = ( GetAttr o attrMiloPropGroupIdx )
				if ( defaultGroupName == groupName ) then
					selectMore o
			)
		)
	)
	
	--
	-- event: btnHide pressed
	-- desc: hide all objects in prop group.
	--
	on chkHide changed state do
	(
		objset = GetPropGroupObjects lstPropGroups.selected
		if ( state ) then
		(
			for o in objset do
				hide o
		)
		else
		(
			for o in objset do
				o.isHidden = false
		)
	)

	--
	-- event: chkFreeze state changed
	-- desc: toggle freeze all objects in prop group.
	--
	on chkFreeze changed state do
	(
		objset = GetPropGroupObjects lstPropGroups.selected
		if ( state ) then
		(
			for o in objset do
				freeze o
		)
		else
		(
			for o in objset do
				unfreeze o
		)
	)
	
	--
	-- event: btnColorise pressed
	-- desc: Auto-random colorise prop group objects.
	--
	on btnColorise pressed do
	(
		local groupNames = RsMapObjectGetPropGroups()
		local colours = #()
		for groupName in groupNames do
		(
			format "Group: %\n" groupName
		
			local r = random 0 255
			local g = random 0 255
			local b = random 0 255
			local col = (color r g b)
			
			local objset = ( GetPropGroupObjects groupName )
			for o in objset do
			(
				o.wireColor = col
			)
		)
	)
	
	--
	-- event: btnImport pressed
	-- desc: Import a new prop group.
	--
	on btnImport pressed do
	(
		local groupName = lstPropGroups.selected
		local filename = getMAXOpenFileName()
		
		if ( undefined != filename ) then
		(
			mergeMaxFile filename #prompt
		)	
	)
	
	--
	-- event: btnSave pressed
	-- desc: save props in selected group to a user-selected .max file.
	--
	on btnSave pressed do
	(
		local groupName = lstPropGroups.selected
		local filename = getMAXSaveFileName()
		
		if ( undefined != filename ) then
		(
			objset = GetPropGroupObjects lstPropGroups.selected
			saveNodes objset filename
		)
	)
	
	--
	-- event: btnDelete pressed
	-- desc: Delete all objects in the selected prop group.
	--
	on btnDelete pressed do
	(
		local ss = stringStream ""
		local groupName = ( lstPropGroups.selected )
		format "Are you sure you want to delete all props in the % prop group?" groupName to:ss
		
		if ( querybox (ss as string) ) then
		(
			objset = ( GetPropGroupObjects groupName )
			for o in objset do
				delete o
		)
	)
	
	--
	-- event: PropGroupRoll opened
	-- desc: Initially populate the pro pgroup list.
	--
	on RsPropGroupRoll open do
	(
		RefreshPropGroupList()
	)	
)


--------------------------------------------------------------
-- beginning of execution
--------------------------------------------------------------
try CloseRolloutFloater RsVisUtil catch()
RsVisUtil = newRolloutFloater "Visibility" 250 790 50 126
addRollout RsVisRoll RsVisUtil rolledup:false
addRollout RsPropGroupRoll RsVisUtil rolledup:false
