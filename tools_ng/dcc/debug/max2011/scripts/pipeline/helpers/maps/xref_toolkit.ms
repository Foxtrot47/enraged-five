-- 
-- File:: pipeline/helpers/maps/xref_toolkit.ms
-- Description:: XRef Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 16 March 2010
--
-- Author:: Adam Munson <adam.munson@rockstarnorth.com>
-- Updated:: 7 May 2010
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/ui/ChangeXref.ms"
filein "pipeline/helpers/maps/FindXref.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
XRefList = #()
XRefDeleteList = #()

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
-- AM - These functions were in separate files previously, but I moved them here
-- as the extra files only contained these small functions (and were only referenced
-- from this file).  
--

fn RsSetNamesRepoint = (

	RsNameList = #()
	RsNameIndex = #()
	
	for childobj in selection do (
	
		if classof childobj == XRefObject then (
	
			childobj.objectName = childobj.name
			updateXRef childobj
	
			srcName = childobj.objectName
			idxFound = finditem RsNameList srcName
			
			newName = srcName

			if idxFound == 0 then (
			
				append RsNameList srcName
				append RsNameIndex 0
				
				newName = newName + "_0"
			) else (
			
				RsNameIndex[idxFound] = RsNameIndex[idxFound] + 1
				newName = newName + "_" + (RsNameIndex[idxFound] as string)
			)
			print ("renaming " + childobj.name + " to " + newName)
		
			childobj.name = newName 
		)
	)
)

fn RsSetNames rootobj = (

	for childobj in rootnode.children do (
	
		if classof childobj == XRefObject then (
	
			srcName = childobj.objectName
			
			newName = srcName
			print ("renaming " + childobj.name + " to " + newName)
		
			childobj.name = newName 
		)
	)
)

fn RsUpdateXrefs = (

	local numRec = objXRefMgr.recordCount
	local totalCount = 0
	local currItem = 0

	for i=1 to numRec do
	(
		local rec = objXRefMgr.GetRecord(i)
		local items=#()
		local numItem = rec.GetItems #XRefAllType &items 	
	
		for j=1 to numItem do
		(
			if (items[j].unresolved) then totalCount = totalCount + 1
		)
	)

	if querybox ("going to fix " + (totalCount as string) + " xrefs. continue?") == false then return 0

	progressStart "fixing xrefs"
	
	local counter = 0
	local tempName = "toto"
	
	for i=1 to numRec do
	(		
		local rec = objXRefMgr.GetRecord(i)
		local items=#()
		local numItem = rec.GetItems #XRefAllType &items 
		
		for j=1 to numItem do
		(
			if (items[j].unresolved) then 
			(
				progressUpdate (100.0*currItem/totalCount)
				
				currItem = currItem + 1

				if getProgressCancel() == true then 
				(
					progressEnd()
					return false
				)
			
				if counter == 50 then (

					counter = 0
					gc()
				) else (

					counter = counter + 1
				)			
			
				local originalName = items[j].srcItemName
				objXRefMgr.SetXRefItemSrcName &items[j] tempname
				objXRefMgr.SetXRefItemSrcName &items[j] originalName 
			)
		)
	)
	
	progressEnd()
)

--
--fn:	RsRecreateXref
--desc:	Replaces XRef using current XRef attributes and transform, 
--	but updating the pivot if it's different in the prop source file.
--
fn RsRecreateXref obj = (	
		
	isGtaObject = false
	
	select obj
	copyattrs()
	
	if getattrclass obj == "Gta Object" then (
	
		isGtaObject = true
	)

	pos = obj.position
	rot = obj.rotation
	objectParent = obj.parent
	colour = obj.wirecolor
	offsetpos = obj.objectoffsetpos
	offsetrot = obj.objectoffsetrot
	filename = obj.filename
	objectname = obj.objectname
	name = obj.name

	newobj = xrefs.addnewxrefobject filename objectname dupMtlNameAction:#useXRefed

	if (newobj != undefined) do (
	
		append XRefDeleteList obj		
		newobj.name = name		
		newobj.position = (offsetpos - newobj.objectoffsetpos)
		newobj.rotation = rot + (offsetrot - newobj.objectoffsetrot)
		newobj.position += pos
		newobj.wirecolor = colour
		newobj.parent = objectParent

		if isGtaObject == true and getattrclass newobj == "Gta Object" then (

			select newobj
			pasteattrs()	
		)
	)	
	unhide newobj
)

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout XRefScriptRoll "Script Helpers"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Xref_Toolkit#Script_Helpers" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	button btnRepoint "Repoint Refs" pos:[23,20] width:162 height:26
	button btnRename "Rename Refs" pos:[196,20] width:162 height:26
	button btnFix "Fix Xrefs" pos:[23,51] width:162 height:26
	button btnFind "Find Unresolved Xrefs" pos:[196,51] width:162 height:26
	button btnReset "Reset Pivots for Xrefs" pos:[110,85] width:162 height:26
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnRepoint pressed do (
	
		if querybox "Repoint xrefs to match their names?" == true then (

			RsSetNamesRepoint()
		)
	)
	
	on btnRename pressed do (
	
		if querybox "Rename xrefs to match their references?" == true then (

			RsSetNames rootnode
		)
	)
	
	on btnFix pressed do (
		
		RsUpdateXRefs()
	)
	
	on btnFind pressed do (
		
		clearSelection()
		for obj in $objects do(
		
			if ((classof obj == XRefObject) and (obj.unresolved == true)) then (
			
				selectMore obj
			)
		)
	)
	
	on btnReset pressed do (
		
		XRefList = #()
		XRefDeleteList = #()
		
		progressStart "Updating XRefs"
		progressUpdate(0.0)
		
		for selobj in $selection do (
		
			if classof selobj == XRefObject then (
			
				append XRefList selobj
			)
		)
		
		currentItem = 0		
		for obj in XRefList do (
			
			if (getProgressCancel() == true) do (
									
				break
			)
			RsRecreateXref obj
			currentItem += 1
			progressUpdate(100.0 * currentItem / XRefList.count)
		)
		
		for obj in XRefDeleteList do (
		
			delete obj	
		)
		progressend()
	)
)

try CloseRolloutFloater XrefToolkit catch()
XrefToolkit = newRolloutFloater "Xref Toolkit" 400 550 50 96
addRollout XRefScriptRoll XrefToolkit
addRollout ChangeXrefs XrefToolkit
addRollout GtaFindXrefRoll XrefToolkit
