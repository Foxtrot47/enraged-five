-- 
-- File:: pipeline/helpers/maps/modelling.ms
-- Description:: Modelling Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 16 March 2010
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/SelFacesByAngle.ms"
filein "pipeline/UI/VertexSnap.ms"
filein "rockstar/helpers/gridhelper.ms"
filein "rockstar/helpers/extrude.ms"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- DIVIDE
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout DivideRoll "Divide Model"
(
	local MainFloater
	local sub_name
	
	local offset_length	= 400
	local bDelete		= false
	local bLOD			= false	
	local bAligned		= false			
	local columns		= 0
	local rows			= 0
	
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Modelling_Toolkit#Divide_Model" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	button btnSlice90 "Slice 90" pos:[17,20] width:118 height:27
	button btnSlice180 "Slice 180" pos:[146,20] width:116 height:27
	
	groupBox grpChop "Chop" pos:[17,48] width:245 height:138
	spinner spnLength "Size: " pos:[81,68] width:136 height:16 range:[0,1e+006,400] type:#integer scale:100
	editText edtName "Name: " pos:[63,90] width:153 height:18
	checkbox chkAligned "World Aligned" pos:[32,115] width:84 height:21
	checkbox chkLod "LOD Object" pos:[32,135] width:84 height:21
	checkbox chkDelete "Delete Parent" pos:[32,156] width:84 height:21
	button btnGo "Begin" pos:[141,130] width:103 height:25
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	fn worldAlign coord = (
	difference = abs(mod coord offset_length)
	adjustment = abs(offset_length - difference)
	coord  -=  adjustment
	)
		
	fn subdivide = (
		if (selection.count == 0) then (
			messagebox "Error - No object selected" title: "Choppy Chop"
		)
		else (
			obj = $
			start_x = obj.min.x
			start_y = obj.min.y
			start_z = obj.min.z
		
			if(bAligned == true)do (
				if (mod start_x offset_length) != 0 do
					start_x = worldAlign start_x
				if (mod start_y offset_length) != 0 do
					start_y = worldAlign start_y
			)							
				
			size_x = abs(start_x - obj.max.x)
			size_y = abs(start_y - obj.max.y)
			size_z = abs(obj.min.z - obj.max.z)
				
			columns = (size_x / offset_length) as integer		
			rows = (size_y / offset_length) as integer

			if (mod size_x offset_length) != 0 do
				columns += 1
			if (mod size_y offset_length) != 0 do
				rows += 1

			pos_x = start_x
			pos_y = start_y
			pos_z = start_z
				
			for j in 0 to columns do (
				for i in 0 to rows do (
					Box lengthsegs:1 widthsegs:1 heightsegs:1 length:offset_length width:offset_length height:size_z mapCoords:off pos:[pos_x,pos_y,pos_z] isSelected:on
						pos_y += offset_length
						
					boolObj.createBooleanObject $
					boolObj.setBoolOp $ 2
					boolObj.SetOperandB $ obj 3 4

					addmodifier $ (Edit_Mesh ())
					maxOps.CollapseNode $ on
				
					if $.numverts == 0 then
						delete $
					else (
						if bLOD == true then (
							strCount = sub_name.count
							strCount -= 3
							newString = "LOD" + (substring sub_name 4 strCount)
							$.name = newString
							$.pivot = $.center
						)
						else (
							$.name = sub_name
							$.pivot = $.center
							x=plane()
							x.position = $.pivot-[0,0,10]
							x.length= 1
							x.width=1
							x.lengthsegs=1
							x.widthsegs=1
							$.parent = x
							converttomesh $
						)
						
						$.name = uniqueName $.name
							
						if bLOD == true do (
							clonename = uniqueName sub_name
							copy $ isSelected:on name: clonename
							$.pivot = $.center
						)
					)
					gc
				)
				pos_y = start_y
				pos_x += offset_length
			)
			if bDelete == true do
				delete obj
		)
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Slice
	--------------------------------------------------------------
	on btnSlice90 pressed do (
		if selection.count != 1 then (
			messagebox "Please select one object"
		)
		else (
			modPanel.addModToSelection (SliceModifier ()) ui:on
			$.modifiers[#Slice].slice_plane.rotation =(quat 1 0 1 0)
		)
	)
	
	on btnSlice180 pressed do (
		if selection.count != 1 then (
			messagebox "Please select one object"
		)
		else (
			modPanel.addModToSelection (SliceModifier ()) ui:on
			$.modifiers[#Slice].slice_plane.rotation =(quat 1 0 0 1)
		)
	)
	
	--------------------------------------------------------------
	-- Chop
	--------------------------------------------------------------
	on spnLength changed val do (
		offset_length = spnLength.value
	)
	
	on edtName entered text do (
		sub_name = edtName.text
	)
	
	on btnGo pressed do ( 	
		subdivide()
	)
	
	on chkAligned changed state do (
		if chkAligned.checked == true do
			bAligned = true
	)
	
	on chkLod changed state do (
		if chkLod.checked == true do
			bLOD = true
	)
	
	on chkDelete changed state do (
		if chkDelete.checked == true do
			bDelete = true
	)
	
	--------------------------------------------------------------
	-- Chop
	--------------------------------------------------------------
	on DivideRoll open do (
		if selection.count == 1 then (
			edtName.text = $.name
		)
	)
)

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- HELPERS
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout ModelHelpersRoll "Modelling Helpers"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Modelling_Toolkit#Modelling_Helpers" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	groupBox grpVertex "Vertex Snap" pos:[7,20] width:130 height:97
	spinner spnSnapVal "Tolerance: " pos:[37,38] width:97 height:16
	button btnDoSnap "Snap To" pos:[21,62] width:102 height:22
	button btnDoSnapSel "Snap Selected" pos:[21,88] width:102 height:22
	
	groupBox grpFaces "Select Faces by Angle" pos:[143,20] width:130 height:95
	spinner spnAngle "Angle: " pos:[157,38] width:111 height:16 range:[0,100,0]
	button btnGreater "Select Greater" pos:[159,62] width:102 height:22
	button btnLess "Select Less" pos:[159,88] width:102 height:22
	
	button btnSetBestPivot "Set Best Pivot for BBox" pos:[41,124] width:188 height:25
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Vertex Snap
	--------------------------------------------------------------
	on btnDoSnap pressed do (
		
		snapset = #()
		cursel = #()
		cursel = cursel + selection
		
		messagebox "pick the object to snap to"
		
		snapto = (pickobject count:1 filter:RnFilterSnapPick)
			
		if snapto != undefined and (finditem cursel snapto == 0) then (
		
			append snapset snapto
			snapset = snapset + selection
		
			SnapVertsByTol (snapset as array) spnSnapVal.value
		)
	)
	
	on btnDoSnapSel pressed do (
	
		snapset = #()
		snapset = snapset + selection
	
		SnapVertsByTol (snapset as array) spnSnapVal.value		
	)
	
	--------------------------------------------------------------
	-- Select Faces By Angle
	--------------------------------------------------------------
	on btnGreater pressed do (
		selectGreater spnAngle.value
	)
	
	on btnLess pressed do (
		selectLess spnAngle.value
	)
	
	--------------------------------------------------------------
	-- Set Best Pivot for BBox
	--------------------------------------------------------------
	on btnSetBestPivot pressed do (
		filein "pipeline/util/SetPivotForBBox.ms"
	)
)

try CloseRolloutFloater ModelToolkit catch()
ModelToolkit = newRolloutFloater "Modelling Toolkit" 300 700 50 96
addRollout DivideRoll ModelToolkit
addRollout ModelHelpersRoll ModelToolkit
addRollout RsGridRoll ModelToolkit
addRollout RsExtrudeRoll ModelToolkit