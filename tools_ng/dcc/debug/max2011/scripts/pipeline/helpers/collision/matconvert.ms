--
-- File:: matconvert.ms
-- Desc:: material converter
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: Pre-GTA4, updated 5 December 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/string.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
RsSourceMaterial = #()
RsTargetMaterial = #()

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsLoadConvertFile
-- desc: Load <common>/data/materials/mtl_convert.txt
--
fn RsLoadConvertFile filename = (

	RsSourceMaterial = #()
	RsTargetMaterial = #()

	datfile = openfile filename
	
	if datfile == undefined then return false
	
	while eof datfile == false do (
	
		datline = readline datfile
		
		if datline.count > 0 and datline[1] != "#" then (
		
			lineTokens = filterstring datline " \t"
			
			if ( 2 == lineTokens.count ) then
			(
				lineTokens[1] = RsUppercase lineTokens[1]
				lineTokens[2] = RsUppercase lineTokens[2]
				
				format "Map % ==> %\n" lineTokens[1] lineTokens[2]
				
				append RsSourceMaterial lineTokens[1]
				append RsTargetMaterial lineTokens[2]
			)
		)
	)
	
	close datfile
	
	( RsSourceMaterial.count == RsTargetMaterial.count )
)

--
-- name: RsConvertCollisionOnMaterial
-- desc:
--
fn RsConvertCollisionOnMaterial mat = (

	if classof mat == RexBoundMtl then (
	
		collName = RexGetCollisionName mat 
		
		idFound = finditem RsSourceMaterial collName
		
		if idFound != 0 then (
		
			RexSetCollisionName mat RsTargetMaterial[idFound]		
		)
	)
)

--
-- name: RsConvertCollisionOnObjectCollision
-- desc:
--
fn RsConvertCollisionOnObjectCollision childobj = (

	if ( Col_Mesh == classof childobj ) then (
	
		local idxSurfaceType = getattrindex "Gta Collision" "Coll Type"
		local valSurfaceType = RsUppercase(getattr childobj idxSurfaceType)

		local idFound = finditem RsSourceMaterial valSurfaceType 
		
		if idFound != 0 then (

			savetrans = childobj.transform
			col2mesh childobj

			if classof childobj.material == Multimaterial then (

				for childmat in childobj.material.materiallist do (

					if classof childmat == RexBoundMtl then (

						RsConvertCollisionOnMaterial childmat		
					)
				)
			)
			else if classof childobj.material == RexBoundMtl then (

				RsConvertCollisionOnMaterial childobj.material
			)
			else (

				boundmat = RexBoundMtl()

				RexSetCollisionName boundmat RsTargetMaterial[idFound]
				childobj.material = Multimaterial()
				childobj.material.materiallist[1] = boundmat

				numFaces = getnumfaces childobj

				for i = 1 to numFaces do (

					SetfaceMatID childobj i 1
				)
			) 

			mesh2col childobj
			childobj.transform = savetrans
		)	
	) 
	
	if ( getattrclass childobj == "Gta Collision" ) then (
	
		local idxSurfaceType = getattrindex "Gta Collision" "Coll Type"
		local valSurfaceType = RsUppercase(getattr childobj idxSurfaceType)
		
		local idFound = finditem RsSourceMaterial valSurfaceType 
		format "replace %\n" valSurfaceType
		
		if idFound != 0 then (
		
			format "% from % to %\n" childobj.name valSurfaceType RsTargetMaterial[idFound]
			setattr childobj idxSurfaceType RsTargetMaterial[idFound]
		)
	)
)

--
-- name: RsConvertCollisionOnObject
-- desc:
--
fn RsConvertCollisionOnObject obj = (

	if getattrclass obj == "Gta Object" then (
	
		for childobj in obj.children do (
					
			RsConvertCollisionOnObjectCollision childobj
		)
		
	) else (
	
		RsConvertCollisionOnObjectCollision obj
	)
)
