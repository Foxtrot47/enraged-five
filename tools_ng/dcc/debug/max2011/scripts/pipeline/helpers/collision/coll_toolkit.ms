--
-- File:: pipeline/helpers/collision/coll_toolkit.ms
-- Description:: Collision Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 16 March 2010
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- 19/08/2009
-- by Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Rockstar Ground Painter
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
--filein "rockstar/helpers/secondsurface_painter.ms"
filein "pipeline/helpers/collision/coll_painter.ms"
filein "pipeline/helpers/collision/matconvert.ms"
filein "pipeline/util/content.ms"
filein "pipeline/util/xml.ms"
filein "pipeline/export/maps/globals.ms"
filein "pipeline/helpers/maps/BakeTextureToCPV.ms"

-----------------------------------------------------------------------------
-- Global
-----------------------------------------------------------------------------
struct RsCollCategory (name, entries)

RsCollCategories = #()
global RsCollCats = #()
RsCollItems = #()
RsIdxCollType = getattrindex "Gta Collision" "Coll Type"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- COLLISION HELPERS
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout CollHelpersRoll "Collision Helpers" width:355 height:321
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Collision_Toolkit#Collision_Helpers" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	--groupBox grpCat "Categories" pos:[11,20] width:195 height:119
	group "Categories:"
	(
		dropDownList cboCategories "Category:" --pos:[34,40] width:151 height:40
		dropDownList cboCollision "Type:" --pos:[33,85] width:151 height:40
	)
	button btnConvert "Convert" width:100
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Categories
	--------------------------------------------------------------	
	fn LoadEntriesFromFile filename = (
		intFile = openfile filename mode:"rb"
		
		while eof intFile == false do (
			intLine = RsRemoveSpacesFromStartOfString(readline intFile)
	
			if intLine.count > 0 and intLine[1] != "#" then (
				intTokens = filterstring intLine " \t"		
				found = false
				
				for collCat in RsCollCategories do (			
					if collCat.name == intTokens[2] then (	
						append collCat.entries intTokens[1]
						found = true
					)
				)
				
				if found == false then (	
					collCat = RsCollCategory intTokens[2] #(intTokens[1])
					append RsCollCategories collCat
				)
			)
		)
		
		close intFile		
	)
	
	fn LoadEntries = (
		intFilename = ( RsConfigGetCommonDir() ) + "data/materials/materials.dat"
		LoadEntriesFromFile intFilename
		RsCollCats = #()
		
		for collCat in RsCollCategories do (		
			if collCat.name != undefined then (
				append RsCollCats collCat.name
			)
		)

		cboCategories.items = RsCollCats
	)

	fn UpdateTypes arg = (
		RsCollItems = #()				
		
		if arg != 0 then (	
			idxCat = 0
			
			for i = 1 to RsCollCategories.count do (
				if RsCollCategories[i].name == cboCategories.items[arg] then (
					idxCat = i
				)
			)
		
			RsCollItems = RsCollItems + RsCollCategories[idxCat].entries
		)
		
		sort RsCollItems
		
		cboCollision.items = RsCollItems		
		cboCollision.selection = 1
	)

	fn UpdateSelection = (
	
		if selection.count > 0 then (
			if getattrclass selection[1] == "Gta Collision" then (
				valCollType = RsUppercase(getattr selection[1] RsIdxCollType)
				newCollCategory = ""
				
				for collCat in RsCollCategories do (
				
					for collEntry in collCat.entries do (
						if collEntry == valCollType then (
							newCollCategory = collCat.name
						)
					)
				)

				idxFound = finditem cboCategories.items newCollCategory
				cboCategories.selection = idxFound

				UpdateTypes idxFound

				idxFound = finditem cboCollision.items valCollType
				cboCollision.selection = idxFound
			)
		)
	)


	fn SetType = (
		if selection.count > 0 then (
		
			if getattrclass selection[1] == "Gta Collision" then (
				if cboCollision.selection != 0 then (
					setattr selection[1] RsIdxCollType cboCollision.items[cboCollision.selection]
				)
			)
		)
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Categories
	--------------------------------------------------------------	
	on cboCategories selected arg do (
		UpdateTypes arg
		SetType()
	)
	
	on cboCollision selected arg do (
		SetType()
	)
	
	--------------------------------------------------------------
	-- Convert
	--------------------------------------------------------------	
	on btnConvert pressed do (
	
		local index = ( GetAttrIndex "Gta Collision" "Coll Type" )
		if ( 0 == $selection.count ) then
		(
			messageBox "No objects selected."
		)
		
		for obj in selection do (

			local oldtype = ( GetAttr obj index )
			RsConvertCollisionOnObject obj
			local newtype = ( GetAttr obj index )
			
			if ( "Gta Collision" == GetAttrClass obj ) then
				format "% collision type from % to %\n" obj.name oldtype newtype
		)
	)
	
	--------------------------------------------------------------
	-- Rollout open/close
	--------------------------------------------------------------
	on CollHelpersRoll open do (
-- 		local filename = ( RsConfigGetCommonDir() + "data/materials/mtl_convert.txt" )
-- 		if not ( RsLoadConvertFile filename ) then
-- 		(
-- 			ss = stringStream ""
-- 			format "Error: loading %." filename to:ss
-- 			messagebox ( ss as string )
-- 		)
-- 		
-- 		LoadEntries()
-- 		callbacks.addscript #selectionSetChanged "CollisionToolkit.rollouts[1].UpdateSelection()" id:#rscollobjselect
-- 		UpdateSelection()
	)
	
	on CollHelpersRoll close do (
		callbacks.removescripts id:#rscollobjselect
		RsSettingWrite "rscollsetup" "rollup" (not CollHelpersRoll.open)
	)
)

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- PAINTING
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout PaintingRoll "Painting"
(	
	-----------------------------------------------------------------------------
	-- Global
	-----------------------------------------------------------------------------
	global CollSurfaceObj 
	global OrigObject
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Collision_Toolkit#Painting" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	group "1: Start - convert to mesh:"
	(
--		dropdownlist lstPaletteLevel
		button btnInitialise "Initialise" width:120
	)
	
	group "visibility"
	(
		radiobuttons objectVis "objects' visibillty" labels:#("only collision mesh","only original mesh","both") default:1
		checkbox hideWorld "hide world" checked:true
	)
	
	group "2: Alter - paint"
	(
		label lblSurface "Second Surface:" align:#left
		button btnBeginPaintScnd "Start Painting" width:120
		label spacer "" height:20

		label lblGround "Ground Painting:" align:#left
-- 		dropDownList cboColours "Colour:" width:150
-- 		button btnApplyColourToPolys "Apply" width:70
		button btnAverage "Average map colours" width:120
		button btnBeginPaintGrnd "Start Painting" width:120
	)

	group "3: End - convert back to collision:"
	(
		button btnCollapseToCol "Collapse'n'convert" width:120
	)
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Ground Painting
	--------------------------------------------------------------
	local GROUND_PAINT_CHANNEL = 11
	local ColourPalette = #()
	
	struct ColourEntry (name, value)
	
	fn StringToColour colasstring = (
		tokens = filterstring colasstring ","
		return (color (tokens[1] as integer) (tokens[2] as integer) (tokens[3] as integer))
	)
	
	fn FindColour colourlist colourname = (
		
		for colour in colourlist do (
			if colour.name == colourname then (
				colasstring = colour.value
				return StringToColour colasstring
			)
		)
		undefined 
	)
	
	fn VerifySelection = (
		
		sel = selection as array
		if sel.count != 1 then (
			messagebox "Please select one mesh only"
			return false
		) 
		local tempobj = sel[1]
		if undefined==tempobj.parent or "Gta Object"!=(getattrclass tempobj.parent) then(
			messagebox "you're supposed to have selected a converted collision here."
			return false
		)
		if Editable_mesh!=(classof tempobj) then(
			messagebox "Convert to editable mesh first by pressing initialize."
			return false
		)
		CollSurfaceObj = tempobj
			
		return true
	)

	--
	-- name: setLevelPalette
	-- desc: Setup the palette for the specified level name.
	--
	fn setLevelPalette levelName = (
		
		xmlDocPath = RsConfigGetProjBinConfigDir() + "/maps/" + levelname + "/ground_palette.xml"
		format "Setup level palette: %\n" xmlDocPath
		
		if ( ( getFiles xmlDocPath ).count != 0 ) then (
			
			xmlDoc = XmlDocument()	
			xmlDoc.init()
			xmlDoc.load xmlDocPath

			tempcolourarray = #()

			xmlRoot = xmlDoc.document.DocumentElement
			if xmlRoot != undefined then (
				attrelems = xmlRoot.childnodes
				for i = 0 to ( attrelems.Count - 1 ) do (	
					attrelem = attrelems.itemof(i)
					if attrelem != undefined then (
						nameattr = attrelem.Attributes.ItemOf( "name" )
						valueattr = attrelem.Attributes.ItemOf( "value" )

						colentry = ColourEntry nameattr.value valueattr.value
						append ColourPalette colentry
						append tempcolourarray nameattr.value
					)
				)
			)

--			cboColours.items = tempcolourarray
		)
		else
		(
			messagebox ("Could not find " + xmlDocPath + ".  Check there is a colour template file for this level.")
			cboColours.items = #()
		)
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	--
	-- event: lstPaletteLevel item selected
	-- desc:
	--
	on lstPaletteLevel selected index do (
		
		local levelName = ( lstPaletteLevel.items[index] )
		setLevelPalette levelName
	)
	
	--------------------------------------------------------------
	-- Second Surface Painting
	--------------------------------------------------------------
	on btnBeginPaintScnd pressed do (
		if VerifySelection() == true then ConvertAndApplyModifier #SecondSurface
	)
	
	on btnAverage pressed do
	(
		if VerifySelection() == true then
		(
			RsBakeTexturesToCPVForObject CollSurfaceObj.parent toModel:CollSurfaceObj mapChannel:11
		)
	)
	
	on btnBeginPaintGrnd pressed do (
		if VerifySelection() == true then ConvertAndApplyModifier #GroundPaint
	)
	
	--------------------------------------------------------------
	-- Ground Painting
	--------------------------------------------------------------
	on btnInitialise pressed do (
		if selection.count != 1 then (
			messagebox "Select an object"
			return false
		)

		obj = selection[1]
		
		if (classof obj)!=Col_Mesh then
		(
			messagebox "Ground painting works on collision meshes, select the right object."
			return false
		)
		
		col2Mesh obj
		
		CollSurfaceObj = obj
		OrigObject = obj.parent
		print CollSurfaceObj
		print OrigObject
		selectionsets["collisionToolSelection"] = CollSurfaceObj
		hide objects
		unhide CollSurfaceObj

		-- First nuke the colour to default on every face
		--numfaces = getnumfaces obj
		--for i =1 to numfaces do (
		--	meshop.setFaceColor obj.mesh GROUND_PAINT_CHANNEL i (StringToColour ColourPalette[1].value)
		--)
		
		obj.showvertexcolors = true
		obj.vertexColorType = #color
--		obj.vertexColorMapChannel = GROUND_PAINT_CHANNEL
	)
	
	on btnApplyColourToPolys pressed do (
		if selection.count != 1 then (
			messagebox "Select an object"
			return false
		)
		
		obj = selection[1]

		-- Get the face selection and set to the selected colour
		faceidxs = getfaceselection obj
		meshop.setFaceColor obj.mesh GROUND_PAINT_CHANNEL faceidxs (findcolour ColourPalette cboColours.selected)
		
		update obj
	)
	
	on btnCollapseToCol pressed do (
		if selection.count != 1 then (
			messagebox "Select an object"
			return false
		)
		
		obj = selection[1]
		
		if classof obj == Editable_Mesh or classof obj == Editable_Poly then (
			CollapseStack obj
			mesh2col obj
		)
		unhide objects
		selectionsets["collisionToolSelection"] = #()
	)
	
	on PaintingRoll open do (
		
-- 		local levels = ( RsContentGetLevels() )
-- 		local levelName = levels[2]
-- 		lstPaletteLevel.items = levels
-- 		setLevelPalette levelName
	)
	
	on objectVis changed val do
	(
		if undefined==CollSurfaceObj or undefined==OrigObject then
		(
			print CollSurfaceObj
			print OrigObject
			messagebox "You must first initialise with converting a collision mesh."
			return false
		)
		case val of
		(
			1:(unhide CollSurfaceObj; hide OrigObject; selectionsets["collisionToolSelection"] = CollSurfaceObj)
			2:(hide CollSurfaceObj; unhide OrigObject; selectionsets["collisionToolSelection"] = OrigObject)
			3:(unhide CollSurfaceObj; unhide OrigObject; selectionsets["collisionToolSelection"] = #(OrigObject, CollSurfaceObj))
		)
		select selectionsets["collisionToolSelection"]
	)
	
	on hideWorld changed val do 
	(
		if val then 
		(
			hide objects
			unhide selectionsets["collisionToolSelection"]
		)
		else  unhide objects
	)
)

try CloseRolloutFloater CollisionToolkit catch()
CollisionToolkit = newRolloutFloater "Collision Toolkit" 200 500 50 96
addRollout CollHelpersRoll CollisionToolkit rolledup:(RsSettingsReadBoolean "rscollsetup" "rollup" false)
addRollout PaintingRoll CollisionToolkit rolledup:false

-- pipeline/helpers/collision/coll_toolkit.ms
