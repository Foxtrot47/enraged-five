--
-- File:: cloth.ms
-- Description:: Cloth support functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 24 April 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/util/collutil.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
-- None (yet)
-- Channel indices may go here.
--

-----------------------------------------------------------------------------
-- Struct
-----------------------------------------------------------------------------

struct RsClothStruct
(
	--
	-- name: IsCloth
	-- desc: Return whether an object is flagged as being cloth.
	--
	fn IsCloth obj = (
		if ( "Gta Object" != GetAttrClass obj ) then
			return ( false )
		
		local idxIsCloth = ( GetAttrIndex "Gta Object" "Is Cloth" )
		return (GetAttr obj idxIsCloth)
	),
	
	--
	-- name: SetupEnvClothForExport
	-- desc: Setup environment cloth for export.
	--
	fn SetupEnvClothForExport obj rexentity rexmesh = (
	
		-- Add the EnvCloth child
		local envCloth = rexAddChild rexentity "EnvCloth" "EnvCloth"
		local envClothPiece = rexAddChild envCloth "ClothPiece" "ClothPiece"
		
		-- Setup mesh (default setting for now)
		rexSetPropertyValue rexmesh "MeshBlindVertexDataChannels" "(pin|10|9|0.0;pinradius|11|21|0.0;pinramp|12|22|0.0;edgecomp|13|23|0.25;bslength|14|24|0.5;bsstr|15|25|0.7)"		
		
		local savesel = selection
		select obj
		rexSetNodes envClothPiece
		select savesel
		
		true
	),
	
	--
	-- name: SetupCharClothForExport
	-- desc: Setup character cloth for export.
	--
	fn SetupCharClothForExport obj skelroot outputdir rexentity rexmesh = (
	
		local savesel = selection
	
		-- Add the Fragment child
		select skelroot
		local fragment = rexAddChild rexentity "fragment" "Fragment"
		rexSetPropertyValue fragment "OutputPath" outputdir
		rexSetNodes fragment
	
		-- Add the EnvCloth child
		local envCloth = rexAddChild rexentity "CharCloth" "CharCloth"
		local envClothPiece = rexAddChild envCloth "ClothPiece" "ClothPiece"
		
		-- Setup mesh (default setting for now)
		rexSetPropertyValue rexmesh "MeshBlindVertexDataChannels" "(pin|10|9|0.0;pinradius|11|21|0.0;pinramp|12|22|0.0;edgecomp|13|23|0.25;bslength|14|24|0.5;bsstr|15|25|0.7)"		
		
		clearSelection()
		-- Attach bounds (optional)
		for child in obj.children do
		(
			-- Ignore non-collision primitives
			if ( "Gta Collision" != ( GetAttrClass child ) ) then
				continue
			
			selectMore child
		)
		
		selectMore obj
		rexSetNodes envClothPiece
		select savesel
		
		true	
	)
)

-----------------------------------------------------------------------------
-- Struct Global Instances
-----------------------------------------------------------------------------
global RsCloth = RsClothStruct()


-- cloth.ms
