--
-- File:: pipeline/util/info_toolkit.ms
-- Description:: Info Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 18 March 2010
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/posprint.ms"

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout InfoScriptRoll "Script Helpers"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Info_Toolkit#Script_Helpers" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	button btnDegen "Check for Degenerates" across:2
	button btnTest "Test Mesh Topology"
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnDegen pressed do (
		filein "pipeline/util/CheckDegenShare.ms"	
	)
	
	on btnTest pressed do (
		filein "pipeline/util/TestMeshTopology.ms"
	)
)

try CloseRolloutFloater InfoToolkit catch()
InfoToolkit = newRolloutFloater "Info Toolkit" 400 240 50 126
addRollout InfoScriptRoll InfoToolkit
addRollout RsPosPrintRoll InfoToolkit