--
-- File:: pipeline/util/lodsphere.ms
-- Description::
--
-- Author:: Unknown (original)
-- Author:: David Muir <david.muir@rockstarnorth.com>
--

if ( 0 == selection.count ) then
(
	MessageBox "No objects selected to create LOD Range Spheres."
)
else
(
	for o in selection do
	(
		if ( "Gta Object" != GetAttrClass o ) then
			continue
	
		local idxLodDistance = ( GetAttrIndex "Gta Object" "LOD distance" )
	
		-- Create LOD Range Sphere
		local xpos = o.position.x
		local ypos = o.position.y
		local zpos = o.position.z
		local nextbug = sphere()
		local lodd = ( GetAttr o idxLodDistance )
		nextbug.radius=lodd
		nextbug.segs=7
		nextbug.position=[xpos,ypos,zpos]
		nextbug.wirecolor=red
		nextbug.name="LODRANGE_"+o.name
	)
)

-- pipeline/util/lodsphere.ms
		