--
-- File:: pipeline/util/p4.ms
-- Description:: Perforce bindings for MaxScript.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 21 June 2010
--

-----------------------------------------------------------------------------
-- .Net Uses
-----------------------------------------------------------------------------
for i = 1 to pluginPaths.count() do
(
	local plugpath = ( pluginPaths.get i ) + "\\p4api.dll"
	if ( RsFileExists( plugpath ) ) then
	(
		( dotnet.loadAssembly plugpath )
	)
)
dotNet.loadAssembly "System.Windows.Forms"

--
-- name: RsPerforcePasswordDialog
-- desc:
--
rollout RsPerforcePasswordDialog "Perforce Password"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	label lblPortLbl "Port:" width:100 across:2
	label lblPort "" align:#left
	label lblUserLbl "User:" width:100 across:2
	label lblUser "" align:#left
	label lblPassword "Password:" width:100 across:2 
	dotNetControl txtPassword "System.Windows.Forms.TextBox" width:250 align:#left
	button btnLogin "Login" width:100 align:#right
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------
	
	--
	-- event: btnLogin pressed
	-- desc: Login button pressed.  Attempt low-level Perforce login.
	--
	on btnLogin pressed do
	(
		DestroyDialog RsPerforcePasswordDialog
		::P4.p4.login( txtPassword.text )
	)
	
	--
	-- event: RsPerforcePasswordDialog open.
	-- desc: Some .Net UI initialisation.
	--
	on RsPerforcePasswordDialog open do
	(
		lblUser.text = ::P4.p4.User
		lblPort.text = ::P4.p4.Port
		
		txtPassword.UseSystemPasswordChar = true
		txtPassword.Height = 35
	)
		
) -- RsPerforcePasswordDialog rollout

--
-- struct: Perforce
-- desc: Perforce functions for MaxScript.
-- 
-- notes:
--   Just like any Perforce API please batch your requests as much as possible;
--   by passing MaxScript Array objects as file arguments.
--
-- examples:
--   p4 = RsPerforce()
--   (RsPerforce false cwd:undefined P4:dotNetObject:P4API.P4Connection result:undefined)
--   p4.connect "x:\\payne"
--   Successfully connect to the Perforce server (rsgvanp4s1:1666 as david.muir).
--   true
--   p4.local2depot "x:\\payne\\build\\dev\\levelstatus.txt"
--   "//projects/payne/build/dev/levelstatus.txt"
--
struct RsPerforce
(
	connected = false,
	cwd = undefined,
	p4 = dotNetObject "P4API.P4Connection",	-- .Net P4API.P4Connection object.
	result = undefined,									-- Last P4.Net result (RecordSet) or undefined.
	
	--
	-- name: connect
	-- desc: Connect to the Perforce server that has cwd (directory) mapped.
	--
	-- This required your Perforce environment to be set up correctly.
	--
	fn connect dir = (
		local pwd = ( sysInfo.currentdir )
		try
		(
			cwd = dir
			sysInfo.currentdir = cwd
			
			try
			(
				p4.connect()
				p4.run "login" #("-s")
				format "Successfully connect to the Perforce server (% as %).\n" p4.Port p4.User
			)
			catch 
			(
				format "Exception connecting to Perforce: %\n" (getCurrentException())
				CreateDialog RsPerforcePasswordDialog width:300 modal:true escapeEnable:true
			)
		)
		catch (
			format "Exception connecting to Perforce: %\n" (getCurrentException())
		)
		sysInfo.currentdir = pwd
		( p4.IsValidConnection true true )
	),
	
	--
	-- name: disconnect
	-- desc: Disconnect from the Perforce server.
	--
	fn disconnect = (
		p4.Disconnect()
	),

	--
	-- name: run
	-- desc: Generic run method that maps straight onto the low-level P4API.P4 object.
	--
	fn run cmd args = (
		
		local pwd = ( sysInfo.currentdir )
		result = undefined
		--try
		(
			format "P4 % %\n" cmd args
			sysInfo.currentdir = cwd
			result = ( p4.Run cmd args )
		)
		/*catch (
			format "P4 run error: %\n" (getCurrentException())
		)*/
		sysInfo.currentdir = pwd
	
		-- Process the RecordSet into something to return to MaxScript.
		( not result.HasErrors() )
	),
	
	--
	-- name: connected
	-- desc: Return true iff successfully connected; false otherwise.
	--
	fn connected = (
		p4.IsValidConnection true true
	),
	
	--
	-- name: local2depot
	-- desc: Return depot path for a specified local file.
	--
	fn local2depot filename = (
		if ( Array == classof filename ) then
		(
			run "where" filename
			files = #()
			for i = 1 to (result.Records.Count) do
			(
				append files result.Records[i].Item["depotFile"]
			)
			files
		)
		else
		(
			run "where" #( filename )
			result.Item[0].Fields.Item["depotFile"]
		)
	),
	
	--
	-- name: sync
	-- desc: Sync to latest revision of the specified file.
	--
	fn sync filename = (
		if ( Array == classof filename ) then
			run "sync" filename
		else
			run "sync" #( filename )
	),
	
	--
	-- name: edit
	-- desc: Checkout current revision of the specified file.
	--
	fn edit filename = (
		if ( Array == classof filename ) then
			run "edit" filename
		else
			run "edit" #( filename )
	),
	
	--
	-- name: add
	-- desc: Mark the file for add (default changelist).
	--
	fn add filename = (
		if ( Array == classof filename ) then
			run "add" filename
		else
			run "add" #( filename )
	)
)

-- pipeline/util/p4.ms
