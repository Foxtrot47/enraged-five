--
-- File: zones.ms
-- Desc: Gta Zone import/export functions.
--
-- Author: David Muir <david.muir@rockstarnorth.com>
-- Date: 20 January 2009
--
-- The file has common functionality between the Import and Export Zones.
-- Based off of the original <scripts>/GTA/ImportZones.ms and ExportZones.ms.
--

-- Global Gta Zone attribute class.
RsGtaZoneAttributeClass = "Gta Zone"

--
-- name: RsMapImportPopZoneFile
-- desc: Export Gta Zone helpers to a Population Zone IPL file.
--
fn RsMapImportPopZoneFile filename = (
	local file
	local line
	local str
	local elements
	local minV, maxV
	local zone
	local zonename2index
	
	local bReadingZones = false

	if ( undefined == filename ) then
		return false
	
	zonename2index = getattrindex RsGtaZoneAttributeClass "Zone name"
	if ( undefined == zonename2index ) then 
	(
		messagebox "Zone attributes are not setup" title:"Zone file export"
		return false
	)
	
	file = openfile filename mode:"r"
	
	while (not eof file) do (
		line = readline file
		if line == "zone" do ( 
			bReadingZones = true
			continue
		)	
		if line == "end" do bReadingZones = false

		if bReadingZones == true do (
			elements = filterString line ", "
			zone = Box2Geometry()
			zone.name = elements[1]
			minV = (Point3 (elements[2] as float) (elements[3] as float) (elements[4] as float))
			maxV = (Point3 (elements[5] as float) (elements[6] as float) (elements[7] as float))
			
			zone.pos.x = (maxV.x + minV.x)/2
			zone.pos.y = (maxV.y + minV.y)/2
			zone.pos.z = minV.z
			zone.width = maxV.x - minV.x
			zone.length = maxV.y - minV.y
			zone.height = maxV.z - minV.z
			
			if elements[8] != undefined then (
				setattr zone zonename2index (elements[8] as string)
			) else (
				setattr zone zonename2index (elements[1] as string)
			)
		)
	)
	
	close file
	
	return true
)

--
-- name: RsMapExportPopZoneFile
-- desc: Export Gta Zone helpers to a Population Zone IPL file.
--
fn RsMapExportPopZoneFile filename = (
	local file
	local zonename
	local zonename2
	local zonename2index
	
	if ( undefined == filename ) then
		return false
	
	local zonename2index = getattrindex RsGtaZoneAttributeClass "Zone name" 
	if ( undefined == zonename2index ) then 
	(
		messagebox "Zone attributes are not setup" title:"Zone file export"
		return false
	)
	
	file = createfile filename
	if ( undefined == file ) then 
	(
		local ss = stringStream ""
		format "Cannot create file: %" filename to:ss
		messagebox ( ss as string ) title:"Zone file export"
		return false
	)	
	format "zone\n" to:file
	
	for obj in rootNode.children do (
	
		if ( RsGtaZoneAttributeClass == GetAttrClass obj ) then 
		(
			local zonename = substring obj.name 1 8

			local minvalue = [obj.pos.x - obj.width/2, obj.pos.y - obj.length/2, obj.pos.z]
			local maxvalue = [obj.pos.x + obj.width/2, obj.pos.y + obj.length/2, obj.pos.z + obj.height]

			local zonename2 = getattr obj zonename2index
			format "%, " zonename to:file
			format "%, %, %, " minvalue.x minvalue.y minvalue.z to:file
			format "%, %, %, " maxvalue.x maxvalue.y maxvalue.z to:file
			format "%\n" zonename2 to:file
		)
	)
	format "end\n" to:file
	
	close file
)

--
-- name: RsMapImportMapZoneFile
-- desc: Import Gta Zone helpers from a named file.
--
fn RsMapImportMapZoneFile filename = (
	local file
	local line
	local str
	local elements
	local minV, maxV
	local zone
	local zoneindex
	local levelindex
	local zonename2index
	
	local bReadingZones = false

	if ( undefined == filename ) then
		return false
	
	zonename2index = getattrindex RsGtaZoneAttributeClass "Zone name"
	if ( undefined == zonename2index ) then 
	(
		messagebox "Zone attributes are not setup" title:"Zone file export"
		return false
	)
	
	file = openfile filename mode:"r"
	
	while (not eof file) do (
		line = readline file
		if line == "mzon" do ( 
			bReadingZones = true
			continue
		)	
		if line == "end" do bReadingZones = false

		if bReadingZones == true do (
			elements = filterString line ", "
			zone = Box2Geometry()
			zone.name = elements[7]
			minV = (Point3 (elements[1] as float) (elements[2] as float) (elements[3] as float))
			maxV = (Point3 (elements[4] as float) (elements[5] as float) (elements[6] as float))
			
			zone.pos.x = (maxV.x + minV.x)/2
			zone.pos.y = (maxV.y + minV.y)/2
			zone.pos.z = minV.z
			zone.width = maxV.x - minV.x
			zone.length = maxV.y - minV.y
			zone.height = maxV.z - minV.z
			
			if elements[7] != undefined then (
				setattr zone zonename2index (elements[7] as string)
			) else (
				setattr zone zonename2index "unknown"
			)
		)
	)
	
	close file
	
	return true
)

--
-- name: RsMapExportMapZoneFile
-- desc: Export Gta Zone helpers to a Map Zone IPL file.
--
fn RsMapExportMapZoneFile filename = (
	local file
	local zonename
	local zonename2
	local zoneindex
	local zonetype
	local levelindex
	local zonelevel
	local zonename2index
	
	if ( undefined == filename ) then
		return false
	
	local zonename2index = getattrindex RsGtaZoneAttributeClass "Zone name" 
	if ( undefined == zonename2index ) then 
	(
		messagebox "Zone attributes are not setup" title:"Zone file export"
		return false
	)
	
	file = createfile filename
	if ( undefined == file ) then 
	(
		locla ss = stringStream ""
		format "Cannot create file: %" filename to:ss
		messagebox ( ss as string ) title:"Zone file export"
		return false
	)	
	format "mzon\n" to:file
	
	for obj in rootNode.children do (
	
		if ( RsGtaZoneAttributeClass == GetAttrClass obj ) then 
		(
			local minvalue = [obj.pos.x - obj.width/2, obj.pos.y - obj.length/2, obj.pos.z]
			local maxvalue = [obj.pos.x + obj.width/2, obj.pos.y + obj.length/2, obj.pos.z + obj.height]

			local zonename = getattr obj zonename2index
			format "%, %, %, " minvalue.x minvalue.y minvalue.z to:file
			format "%, %, %, " maxvalue.x maxvalue.y maxvalue.z to:file
			format "%\n" zonename to:file
		)
	)
	format "end\n" to:file
	
	close file
)

-- zones.ms
