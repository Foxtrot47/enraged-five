--
-- File:: pipeline/util/TestMeshTopology.ms
-- Description:: test that the triangles of two seperate meshes have their faces referencing the
-- 						same vertex index
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com>
-- Date:: 28/4/2004
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
topologyerrors = #()
topologyindices = #()
topologyObjB = undefined

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
fn swapVerts objChange oldVertID newVertID = (

	print ("swapping vert " + (oldVertID as string) + " and " + (newVertID as string))

	oldVert = objChange.verts[oldVertID].pos
	newVert = objChange.verts[newVertID].pos

	numFaces = getnumfaces objChange
	
	for i = 1 to numFaces do (
	
		curFace = getface objChange i
	
		for j = 1 to 3 do (
		
			if curFace[j] == oldVertID then (
				curFace[j] = newVertID
			) else (
				if curFace[j] == newVertID then (
					curFace[j] = oldVertID
				)
			)
		)
		
		setface objChange i curFace
	)
	
	objChange.verts[oldVertID].pos = newVert 
	objChange.verts[newVertID].pos = oldVert 
)

fn swapTextureVerts objChange oldVertID newVertID = (

	print ("swapping texture vert " + (oldVertID as string) + " and " + (newVertID as string))

	oldVert = gettvert objChange oldVertID
	newVert = gettvert objChange newVertID

	numFaces = getnumfaces objChange
	
	for i = 1 to numFaces do (
	
		curFace = getface objChange i
	
		for j = 1 to 3 do (
		
			if curFace[j] == oldVertID then (
				curFace[j] = newVertID
			) else (
				if curFace[j] == newVertID then (
					curFace[j] = oldVertID
				)
			)
		)
		
		setface objChange i curFace
	)
	
	settvert objChange oldVertID newVert 
	settvert objChange newVertID oldVert 
)

--
-- name:		setSmoothingGroupsInObjects 
-- description:	set the smoothing groups to be the same
fn setSmoothingGroupsInObjects objA objB = (
	numFacesA = getnumfaces objA
	numFacesB = getnumfaces objB

	if numFacesA != numFacesB then (
		append topologyerrors "Objects have a different number of faces"
		append topologyindices -1
		return 0
	)
	for i = 1 to numFacesA do (
	
		grpA = getFaceSmoothGroup objA i
		grpB = getFaceSmoothGroup objB i
		
		if grpA != grpB do (
			--setFaceSmoothGroup objB i grpA
			append topologyerrors ("smoothing group of face " + (i as string) + " is different")
			append topologyindices i
		)
	)	
)

--
-- name:		swapVertsInObjects 
-- description:	swap round the vertices in an object
fn swapVertsInObjects objA objB = (
	numFacesA = getnumfaces objA
	numFacesB = getnumfaces objB

	if numFacesA != numFacesB then (
		append topologyerrors "Objects have a different number of faces"
		append topologyindices -1
		return 0
	)

	numVertsA = getnumverts objA
	numVertsB = getnumverts objB

	if numVertsA != numVertsB then (
		append topologyerrors "Objects have a different number of verticies"
		append topologyindices -1
		return 0
	)
	
	print (objA.name + " has " + (numFacesA as string) + " faces and " + (numVertsA as string) + " vertices")
	print (objB.name + " has " + (numFacesB as string) + " faces and " + (numVertsB as string) + " vertices")
	-- on the first go we will try and fix the object
	
	for i = 1 to numFacesA do (
	
		faceA = getface objA i
		faceB = getface objB i
		
		-- check to see if the face has just had its verts moved round
		-- and fix if this is the case
		
		fixed = false
		
		if faceA[1] != faceB[1] and faceA[2] != faceB[2] and faceA[3] != faceB[3] then (
			append topologyerrors ("face " + (i as string) + " should use verts " + faceA[1] + ", " + faceA[2] + " and " + faceA[3])
			append topologyindices i
		)
	)
)

--
-- name:		swapTextureVertsInObjects 
-- description:	swap round the texture coordinates in an object
fn swapTextureVertsInObjects objA objB = (
	numFacesA = getnumfaces objA
	numFacesB = getnumfaces objB

	numVertsA = getnumtverts objA
	numVertsB = getnumtverts objB

	print (objA.name + " has " + (numFacesA as string) + " faces and " + (numVertsA as string) + " texture vertices")
	print (objB.name + " has " + (numFacesB as string) + " faces and " + (numVertsB as string) + " texture vertices")

	if numFacesA != numFacesB then (
		append topologyerrors "Objects have a different number of faces"
		append topologyindices -1
		return 0
	)

	if numVertsA != numVertsB then (
		append topologyerrors "Objects have a different number of texture vertices"
		append topologyindices -1
		return 0
	)
	
	-- on the first go we will try and fix the object
	
	for i = 1 to numFacesA do (
	
		faceA = gettvface objA i
		faceB = gettvface objB i
		
		-- check to see if the face has just had its verts moved round
		-- and fix if this is the case
		
		fixed = false
		
		if faceA[1] != faceB[1] and faceA[2] != faceB[2] and faceA[3] != faceB[3] then (
			append topologyerrors ("texturing on face " + (i as string) + " is wrong")
			append topologyindices i
		)
	)
	
)

fn GtaItObjEditableMesh obj = (
	return (classof obj == Editable_Mesh)
)

fn GtaTestMeshTopology = (
	
	messagebox "Select Main Object"
	objA = pickobject filter:GtaItObjEditableMesh
	
	if objA == undefined then (
		return 0
	)

	messagebox "Select Object To Test and Fix"
	objB = pickobject filter:GtaItObjEditableMesh

	if objB == undefined then (
		return 0
	)
	
	if objA == objB then (
		messagebox "Pick two different objects"
		return 0
	)
	
	if classof objA != Editable_mesh or classof objB != Editable_mesh then (
		messagebox "Both objects have to be editable meshes"
		return 0
	)
	
	print ("Comparing " + objA.name + " with " + objB.name)
	
	setSmoothingGroupsInObjects objA objB
	swapVertsInObjects objA objB
	swapTextureVertsInObjects objA objB

	topologyObjB = objB
)

GtaTestMeshTopology()

--
-- Errors rollout
--
rollout GtaTopologyErrorsRoll "Gta Topology Errors" 
(
-- ui --
	listbox errors  "Errors:" height:14 items:topologyerrors

-- methods --
	on errors selected index do (
		if topologyindices[index] == -1 do return true
		
		-- select object
		if $ != topologyObjB do	(select topologyObjB)
		if (classof(modPanel.getCurrentObject()) != Editable_mesh) do (modPanel.setCurrentObject topologyObjB)
		-- set faces to be subobject level
		subObjectLevel = 3
		setfaceselection topologyObjB #(topologyindices[index])
	)
)

try CloseRolloutFloater GtaTopologyErrorsUtil catch()

if topologyerrors.count > 0 then (

	GtaTopologyErrorsUtil = newRolloutFloater "GTA3" 350 300 1 136
	addRollout GtaTopologyErrorsRoll GtaTopologyErrorsUtil
	GtaTopologyErrorsRoll.errors.items = topologyerrors 	
) else (
	messagebox "The objects are the same" title:"GTA"
)