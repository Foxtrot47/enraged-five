--
-- File:: pipeline/util/SetPivotForBBox.ms
-- Description::
-- A quick way of finding a best fit pivot axis for a better aligned bounding box.
-- I have to make some assumptions about the polygon information in order
-- to do this quickly in script. I proper way of doing it would you use vertex 
-- averaging to find a best fit/optimum axis (see Oriented Bounding Box code)

-- Assumptions are that:
-- 1. one of the axes is always up (0 0 1)
-- 2. the second longest side of each polygon is used, assuming that most triangles
-- are right angle triangles
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com>
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- 29/3/2010
-- by Marissa Warner-Wu
-- Adding in editable poly support.
--
-- 11/8/2003
-- by Greg Smith
-----------------------------------------------------------------------------

---------------------------------------------------------------------------
-- Function to reset the pivot
---------------------------------------------------------------------------
fn setPivot setObj pivotMat = (

	originalTrans = setObj.objecttransform

	setObj.rotation = pivotMat.rotationpart
	setObj.position = pivotMat.translationpart
		
	setObj.objectoffsetpos = point3 0 0 0 
	setObj.objectoffsetrot = quat 0 0 0 1
	setObj.objectoffsetscale = point3 1 1 1
	
	newTrans = inverse setObj.objecttransform
	newObjTrans = originalTrans * newTrans;
	
	setObj.objectoffsetpos = newObjTrans.translationpart
	setObj.objectoffsetrot = newObjTrans.rotationpart
	setObj.objectoffsetscale = newObjTrans.scalepart
)


---------------------------------------------------------------------------
-- Go through all the objects in the selection on running the script
---------------------------------------------------------------------------
for currObj in selection do (
	global isPoly = false
	global currMods = #()
	
	if (classof currobj != Editable_mesh) and  (classof currobj != Editable_poly) and (classof currobj != PolyMeshObject ) then (
		continue
	)

	hasBoxes = false

	for childObj in currObj.children do (
		if classof childObj == Col_Box then (
			setinheritanceflags childobj #none keeppos:true
			hasBoxes = true
		)
		
		if classof childObj == Col_Sphere then (
			setinheritanceflags childobj #none keeppos:true
		)
		
		if classof childObj == Col_Mesh then (
			setinheritanceflags childobj #none keeppos:true
		)
	)
	
	if hasBoxes == true then (
		continue
	)
	
	-- If this is an editable poly, convert so that its faces will match editable mesh
	if (classof currobj == Editable_poly) or (classof currobj == PolyMeshObject) then (
		isPoly = true
		
		for mod in currObj.modifiers do (
			append currMods mod
		)
		
		convertToMesh currObj
	)

	numVerts = getNumVerts currObj
	numFaces = getNumFaces currObj
	upSide = point3 0 0 1
	longestSide = point3 0 0 0
	lastSide = point3 0 0 0

	-- go through each face looking for the longest axis
	for i = 1 to numFaces do (
		currFace = getFace currObj i

		vertA = getVert currObj currFace[1]
		vertB = getVert currObj currFace[2]
		vertC = getVert currObj currFace[3]

		vertA[3] = 0
		vertB[3] = 0
		vertC[3] = 0

		faceA = vertB - vertA
		faceB = vertC - vertA
		faceC = vertB - vertC

		faceLen = #(0,0,0)

		faceLenA = faceLen[1] = length(vertB - vertA)
		faceLenB = faceLen[2] = length(vertC - vertA)
		faceLenC = faceLen[3] = length(vertB - vertC)

		sort FaceLen

		if faceLenA == FaceLen[2] then (
			midFace = faceA	
		) else (
			if faceLenB == FaceLen[2] then (
				midFace = faceB

			) else (
				midFace = faceC
			)
		)

		if length midFace > length longestSide then (
			longestSide = midFace
		)
	)

	-- go through each point to find the centre point of the box

	transVal = point3 0 0 0

	for i = 1 to numVerts do (
		transVal = transVal + (getVert currObj i)
	)

	transVal = transVal / numVerts

	-- create the matrix of new pivot axes and offset

	longestSide = -normalize longestSide
	lastSide = cross longestSide upSide
	matRot = matrix3 longestSide lastSide upSide transVal 
	scale matRot (point3 -1 1 1)
	setPivot currObj matRot
	
	if isPoly then (
		convertToPoly currObj
		
		for mod in currMods do (
			print (mod as string)
			addModifier currObj mod
		)
	)
)