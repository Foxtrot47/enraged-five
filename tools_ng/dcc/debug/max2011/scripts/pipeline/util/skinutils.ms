--
-- File:: pipeline/util/skinutils.ms
-- Description:: Skin modifier utilities
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 7 July 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/file.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- utility: RsSkinUtils
-- desc: Skin modifier utilities
--

utility RsSkinUtils "RsSkinUtils"
(

	--
	-- name: HasSkin
	-- desc: Return true if object has Skin modifier, false otherwise.
	--
	fn HasSkin obj = (
		( obj.modifiers[#Skin] != undefined )
	)

	--
	-- name: BakeAndSaveWeights
	-- desc: Bake vertex weights and save to filename
	--
	fn BakeAndSaveWeights obj filename = (
		
		local result = false
		
		if ( HasSkin obj ) then
		(		
			setCommandPanelTaskMode #modify
			modPanel.setCurrentObject obj.modifiers[#Skin]
			subobjectLevel = 1
			obj.modifiers[#Skin].Filter_Vertices = true

			local nSkinVerts = skinOps.getNumberVertices obj.modifiers[#Skin]
			skinOps.SelectVertices obj.modifiers[#Skin] #{1..nSkinVerts}

			skinOps.bakeSelectedVerts obj.modifiers[#Skin]
			RsMakeSurePathExists filename
			skinOps.saveEnvelope obj.modifiers[#Skin] filename
			subobjectlevel = 0

			result = true
		)
		result
	)
	
	--
	-- name: SkinAndLoadWeights
	-- desc: Create Skin modifier and load vertex weights from filename
	--
	fn SkinAndLoadWeights obj filename bone_list match_by_name:false bone_limit:4 = (

		setCommandPanelTaskMode #modify
		skinMod = Skin()
		skinMod.bone_Limit = bone_limit
		skinMod.showNoEnvelopes = true
		addModifier obj skinMod
		subobjectLevel = 1
		modPanel.setCurrentObject obj.modifiers[#Skin]

		-- Prior to loading the weights, if we are matching by name then we
		-- install a handler to press the Match By Name button on the load
		-- dialog.
		if ( match_by_name ) then
		(
			DialogMonitorOPS.RegisterNotification RsSkinUtils.__LoadEnvMatchByNameCallback ID:#SkinAndLoadWeightsCallback
			DialogMonitorOPS.Enabled = true
		)

		for b in bone_list do
			try ( skinOps.addBone obj.modifiers[#Skin] b 1 ) catch ()
		completeRedraw()
		skinOps.loadEnvelope obj.modifiers[#Skin] filename
		
		-- Uninstall the dialog callback.
		if ( match_by_name ) then
		(
			DialogMonitorOPS.Enabled = false
			DialogMonitorOPS.UnregisterNotification ID:#SkinAndLoadWeightsCallback
		)
		subobjectLevel = 0
		true
	)
	
	-------------------------------------------------------------------------
	-- Private (seriously, DO NOT USE)
	-------------------------------------------------------------------------
	
	--
	-- name: __LoadEnvMatchByNameCallback
	-- desc: dialog voodoo to press the "Match By Name" button on load env dialog.
	--
	fn __LoadEnvMatchByNameCallback = (
		WindowHandle = DialogMonitorOPS.GetWindowHandle()
		theDialogName = UIAccessor.GetWindowText WindowHandle
		
		if theDialogName != undefined and matchpattern theDialogName pattern:"*Load Envelopes*" do
			UIAccessor.PressButtonByName WindowHandle "Match by Name"           
		
		if theDialogName != undefined and matchpattern theDialogName pattern:"*Load Envelopes*" do
			UIAccessor.PressButtonByName WindowHandle "OK"
		true
	)
)

-- pipeline/util/skinutils.ms
