--
-- File:: drawablelod.ms
-- Description:: Drawable LOD Hierarchy Utility Functions
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 24 March 2009
-- 

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/drawablelod_private.ms"
filein "pipeline/util/userprops.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsLodDrawable_SetHigherDetailModel
-- desc: Set the higher detail mesh (model_hd) for model.
--
fn RsLodDrawable_SetHigherDetailModel model model_hd = (
	
	if ( not LodDrawable_HasContainer model_hd ) then
		DrawableLod_Add model_hd
	return ( DrawableLod_SetParent model_hd model )
)


--
-- name: RsLodDrawable_GetHigherDetailModel
-- desc: Get the higher detail mesh for model.
--
fn RsLodDrawable_GetHigherDetailModel model = (
	
	local children = ( __RsLodDrawable_GetChildren model )
	if ( children.count > 0 ) then
		return ( children[1] )
	else
		return ( undefined )
)


--
-- name: RsLodDrawable_HasHigherDetailModel
-- desc: Return whether model has a higher detail mesh assigned.
--
fn RsLodDrawable_HasHigherDetailModel model = (

	return ( undefined != RsLodDrawable_GetHigherDetailModel model )
)


--
-- name: RsLodDrawable_SetLowerDetailModel
-- desc: Set the lower detail mesh (model_ld) for model.
--
fn RsLodDrawable_SetLowerDetailModel model model_ld = (
	
	if ( not LodDrawable_HasContainer model ) then
		LodDrawable_Add model
	return ( LodDrawable_SetParent model model_ld )
)


--
-- name: RsLodDrawable_GetLowerDetailModel
-- desc: Get the lower detail mesh for model.
--
fn RsLodDrawable_GetLowerDetailModel model = (
	
	return ( LodDrawable_GetParent model )
)


--
-- name: RsLodDrawable_HasLowerDetailModel
-- desc: Return whether the model has a lower detail mesh assigned.
--
fn RsLodDrawable_HasLowerDetailModel model = (
	return ( undefined != LodDrawable_GetParent model )
)


--
-- name: RsLodDrawable_SetUserProps
-- desc: Setup max user properties for the complete Drawable LOD hierarchy 
--       starting with the lodroot node specified. 
-- return: Array of objects that have had lod user property set (typically 
--         used for selection to then select nodes for Rex export).
--
-- This is recursive, when called from exporter enduser code DO NOT specify
-- the depth parameter unless you know what you are doing.
--
fn RsLodDrawable_SetUserProps lodroot depth:0 = (
	
	-- We do not do anything if the root object specified is not within
	-- a drawable LOD hierarchy.
	if ( not ( RsLodDrawable_HasHigherDetailModel lodroot ) and
	     not ( RsLodDrawable_HasLowerDetailModel lodroot ) ) then
	(
		-- Force LOD to 0, in case it was previously in a drawable
		-- LOD hierarchy and no longer is.
		if ( 0 != ( getuserprop lodroot "lod" ) ) then
			setuserprop lodroot "lod" 0

		return ( #() )
	)
	
	local objs = #()
	format "RsLodDrawable_SetUserProps % %\n" lodroot depth
	
	local lodDistAttrIndex = ( GetAttrIndex "Gta Object" "LOD distance" )
	local lod_dist = ( GetAttr lodroot lodDistAttrIndex )
	
	-- Set "lodthreshold" user property to LOD distance if we aren't
	-- the lower detail model (fade distance will be used for that).
	if ( RsLodDrawable_HasLowerDetailModel lodroot ) then
	(
		format "Set userprop % \"lodthreshold\" %\n" lodroot lod_dist 
		setuserprop lodroot "lodthreshold" lod_dist
	)
	else
	(
		-- Ensure we don't have a "lodthreshold" property set.
		RsUserProp_Clear lodroot "lodthreshold"
	)
	
	-- Set "lod" user property to our current depth (unless 0).
	format "Set userprop % \"lod\" %\n" lodroot depth
	setuserprop lodroot "lod" depth
	if ( depth > 0 ) then
	(		
		append objs lodroot
	)
	
	local parobj = ( LodDrawable_GetParent lodroot )
	if ( undefined != parobj ) then
	(
		subobjs = RsLodDrawable_SetUserProps parobj depth:(depth+1)
		join objs subobjs
	)
	
	objs
)


-- drawablelod.ms
