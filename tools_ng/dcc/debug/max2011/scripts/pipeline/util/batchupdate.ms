--
-- File:: pipeline/util/batchupdate.ms
-- Description:: LOD Toolkit
--
-- Author:: Adam Munson <Adam.munson@rockstarnorth.com>
-- Date:: 14 June 2010
--
-----------------------------------------------------------------------------
--////////////////////////////////////////////////////////////
-- uses
--////////////////////////////////////////////////////////////

filein "pipeline/util/xml.ms"

--////////////////////////////////////////////////////////////
-- functions
--////////////////////////////////////////////////////////////

fn RsBatchUpdate xmlFile script = (

	maxFileList = #()

	batchXmlDoc = XmlDocument()
	batchXmlDoc.init()
	batchXmlDoc.load xmlFile
	
	rootElem = batchXmlDoc.document.DocumentElement
	childNodeList = rootElem.ChildNodes
	
	if (rootElem.name == "Batch") then (
	
		for i = 0 to ( childNodeList.Count - 1 ) do (
	
			maxFile = childNodeList.ItemOf(i)
			if (maxFile.name == "file") do (
				
				append maxFileList maxFile.innertext
			)
		)
		
		progressStart "Batch update"
		totalFiles = maxFileList.count
		currentFile = 1
		
		for maxFile in maxFileList do (
			
			
			loadmaxfile maxFile
			filein script
			
			progressUpdate (100.0*currentFile/totalFiles)
			currentFile += 1
			if getProgressCancel() then throw("progress_cancel")
		)
		progressEnd()
	)
	else (
	
		messagebox "The XML doc doesn't have <Batch> as it's root element"
	)

)

rollout BatchUpdateRoll "Batch Update"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Batch_Update#Batch_Update" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	edittext edtXMLFile "XML File:" readOnly:true
	button btnLoadXML "Select XML" width:80 height:20 align:#right
	edittext edtScriptFile "Script file:" readOnly:true
	button btnLoadScript "Select Script" width:80 height:20 align:#right
	button btnStartBatchUpdate "Start update" width:100 height:20
	
	local script
	local xmlFile
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnLoadXML pressed do (
	
		xmlFile = getOpenFileName caption:"Select an XML file" types:"XML (*.xml)|*.xml"
		if (undefined != xmlFile) then (
		
			edtXMLFile.text = xmlFile
		)
		else (
		
			edtXMLFile.text = ""
		)
	)
	
	on btnLoadScript pressed do (
		
		script = getOpenFileName caption:"Select a max script file" types:"MAXScript (*.ms)|*.ms" 
		if (undefined != script) then (
		
			edtScriptFile.text = script
		)
		else (
				
			edtScriptFile.text = ""
		)
	)
	
	on btnStartBatchUpdate pressed do (
	
		if (undefined != xmlFile and undefined != script) then (
		
			RsBatchUpdate xmlFile script
		)
		else (
		
			messagebox "Select an Xml File and a Script to use"
		)
	)

)

try CloseRolloutFloater BatchUpdate catch()
BatchUpdate = newRolloutFloater "Batch Update" 400 200
addRollout BatchUpdateRoll BatchUpdate