--
-- File:: pipeline/util/placement.ms
-- Description:: Placement Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 15 March 2010
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- by David Muir <david.muir@rockstarnorth.com>
-- Authored some scripts used here
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/helpers/maps/EPlanter.ms"
filein "pipeline/util/MeshUtil.ms"
filein "rockstar/helpers/pathnodecheck.ms"
filein "rockstar/util/datimporter.ms"
filein "pipeline/helpers/climbing/handhold_importer.ms"

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- PLACEMENT
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout PlaceHelpersRoll "Placement Helpers"
(
	local fSectorSize = 50.0
	local fWidthInSectors = 120.0
	local fDepthInSectors = 120.0
	
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Placement_Toolkit#Placement_Helpers" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	groupBox grpAlignZ "Align in Z" pos:[18,20] width:112 height:95
	button btnAlignZ "Pick Align To" pos:[29,59] width:91 height:28
	
	groupBox grpInstancer "Instancer" pos:[146,20] width:112 height:95
	pickbutton btnChooseInst "Choose Instance" pos:[156,43] width:91 height:28
	button btnChangeModels "Change Models" pos:[157,80] width:91 height:28
	
	groupBox grpWorld "World Sector Calculator" pos:[17,120] width:242 height:98
	label lblWorld "World Centre:" pos:[39,141] width:77 height:16
	spinner spnWorldX "X: " pos:[85,156] width:55 height:16
	spinner spnWorldY "Y: " pos:[163,156] width:55 height:16 range:[0,100,0]
	label lblSector "Sector:" pos:[40,177] width:77 height:16
	spinner spnSecX "X: " pos:[85,192] width:55 height:16 range:[0,100,0]
	spinner spnSecY "Y: " pos:[163,192] width:55 height:16 range:[0,100,0]
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Align to Z
	--------------------------------------------------------------
	fn pickCallback obj = (
		if obj == saveSel then (
			return false
		)
		if classof obj != Editable_mesh then (
			return false
		)
	
		return true
	)
	
	--------------------------------------------------------------
	-- World-Sector Calculator
	--------------------------------------------------------------
	fn Refresh worldUpdate = (
	
		if ( worldUpdate ) then
		(
			-- World coordinates updated, reflect change in sector coords
			spnSecX.value = floor( ( ( spnWorldX.value as float ) / fSectorSize ) + ( fWidthInSectors / 2.0 ) )
			spnSecY.value = floor( ( ( spnWorldY.value as float ) / fSectorSize ) + ( fDepthInSectors / 2.0 ) ) 
		)
		else
		(
			-- Sector coordinates updated, reflect change in world coords
			spnWorldX.value = ( ( ( spnSecX.value as float ) - fWidthInSectors / 2.0 ) * fSectorSize ) + fSectorSize / 2.0
			spnWorldY.value = ( ( ( spnSecY.value as float ) - fDepthInSectors / 2.0 ) * fSectorSize ) + fSectorSize / 2.0
		)
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Align to Z
	--------------------------------------------------------------
	on btnAlignZ pressed do (
		if selection.count != 1 then (
			messagebox "Please select one object"
			return false
		)
		saveSel = selection[1]
		
		if classof saveSel != Editable_mesh then (
			return false
		)

		pickedObj = pickObject filter:pickCallback
		GtaAlignToGround saveSel pickedObj
	)
	
	--------------------------------------------------------------
	-- Instancer
	--------------------------------------------------------------
	on btnChooseInst picked obj do   
	(       
		global masterobj = obj
		btnChooseInst.text = obj.name
	)        
		
	on btnChangeModels pressed do    
	(   
		if masterobj == undefined then (
			messagebox "Please select an object to instance."
		)
		else (
			models = selection
			mastermat=masterobj.material    
			for model= 1 to models.count do         
			(       	
				models[model].objectoffsetpos = [0,0,0]
				models[model].objectoffsetrot = (quat 0 0 0 1)
				instancereplace models[model] masterobj         
				models[model].material=mastermat        
			)   
		)
	)

	--------------------------------------------------------------
	-- World-Sector Calculator
	--------------------------------------------------------------
	on spnWorldX changed val do
	(
		Refresh true
	)
	
	on spnWorldY changed val do
	(
		Refresh true
	)	
		
	on spnSecX changed val do
	(
		Refresh false
	)	
	
	on spnSecY changed val do
	(
		Refresh false
	)
	
	on GtaWorldSectorRoll open do
	(
		Refresh true
	)
)

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- CHECKS
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout ChecksRoll "Checks"
(	
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Placement_Toolkit#Checks" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	groupBox grpPathCheck "Path Node Checker" pos:[18,20] width:245 height:82
	button btnFixPos "Fix Positions" pos:[30,48] width:91 height:28
	button btnCheck "Check Link Deviation" pos:[142,61] width:110 height:28
	spinner spnDeviation "Deviation " pos:[155,39] width:97 height:16
	
	groupBox grpBadObj "Bad Object Position Finder" pos:[18,107] width:245 height:60
	button btnFind "Find" pos:[42,127] width:91 height:28
	button btnSelect "Select" pos:[143,127] width:91 height:28
	
	progressBar barLoad "ProgressBar" pos:[22,173] width:238 height:15 color:red
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Path Node Checker
	--------------------------------------------------------------
	fn RecGetFixNodes rootobj setlist = (
		for i = 1 to rootobj.children.count do (
			obj = rootobj.children[i]
			
			if obj.ishidden == false then (		
				if classof obj == Col_Mesh then (
					append RsCollisionList obj
				) else if classof obj == VehicleNode then (
					append setlist obj
				)
			)
			
			RecGetFixNodes obj setlist
		)			
	)

	fn RecGetCheckNodes rootobj setlist = (
		for i = 1 to rootobj.children.count do (
			obj = rootobj.children[i]
		
			if obj.ishidden == false then (
				if classof obj == Col_Mesh then (
					append RsCollisionList obj
				) else if classof obj == VehicleLink then (
					append setlist obj
				)
			)
			
			RecGetCheckNodes obj setlist
		)			
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Path Node Checker
	--------------------------------------------------------------
	on btnFixPos pressed do (
		local RsPathNodeList = #()	
		RecGetFixNodes rootnode RsPathNodeList 
		
		for i = 1 to RsPathNodeList.count do (		
			obj = RsPathNodeList[i]
			barLoad.value = 100.0 * ((i as float)/ RsPathNodeList.count)								
			checkNodePathAgainstCollision obj
		)
	)
	
	on btnCheck pressed do (
	
		local RsPathList = #()	
		RecGetCheckNodes rootnode RsPathList 
		RsErrorList = #()

		for i = 1 to RsPathList.count do (	
			obj = RsPathList[i]
			barLoad.value = 100.0 * ((i as float)/ RsPathList.count)
			
			if checkLinkAgainstCollision obj spnDeviation.value then (
				append RsErrorList obj.name 
			)
		)		
		
		rollout RsLinkErrors "Errors"
		(
			listbox lstErrors "Links:" items:RsErrorList height:10
			button btnOK "OK"

			on lstErrors selected item do (
				foundobj = getnodebyname RsErrorList[item] exact:true

				if (foundobj != undefined) then (			
					if isdeleted foundobj == false then (			
						select foundobj
						max zoomext sel
					)
				)
			)

			on btnOK pressed do (
				DestroyDialog RsLinkErrors 
			)	
		)
	
		CreateDialog RsLinkErrors width:300 modal:false
	)
	
	--------------------------------------------------------------
	-- Bad Object Position Finder
	--------------------------------------------------------------
	on btnFind pressed do
	(
		format "Finding bad objects...\n"
		barLoad.value = 0
		i = 0
		cnt = 0
		for obj in $objects do
		(
			i += 1
			barLoad.value = 100.* i / $objects.count
			if ( bit.isFinite( obj.pos.x ) and bit.isFinite( obj.pos.y ) and 
				 bit.isFinite( obj.pos.z ) ) then
				continue
			
			cnt += 1
			format "Invalid object position: %s %s\n" obj.name obj.pos
		)
		format "% objects found.\n" cnt
	)
	
	on btnSelect pressed do
	(
		clearSelection()
		barLoad.value = 0
		i = 0		
		cnt = 0
		for obj in $objects do
		(
			i += 1
			barLoad.value = 100.* i / $objects.count
			if ( bit.isFinite( obj.pos.x ) and bit.isFinite( obj.pos.y ) and 
				 bit.isFinite( obj.pos.z ) ) then
				continue
			
			cnt += 1
			selectMore obj
		)
		format "% objects selected.\n" cnt
	)
)

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- FILE IMPORTER
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout FileImportRoll "File Importer"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Placement_Toolkit#File_Importer" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	groupBox grpDAT "DAT Importer" pos:[18,20] width:112 height:95
	button btnImport "Import" pos:[30,40] width:91 height:28
	button btnExport "Export" pos:[30,72] width:91 height:28
	
	groupBox grpHandhold "Handhold Importer" pos:[146,20] width:112 height:95
	button btnLoadHandHoldXml "Load File" pos:[156,54] width:91 height:28
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- DAT Importer
	--------------------------------------------------------------
	on btnImport pressed do (
		file = getopenfilename caption:"dat file to import" filename:"x:\\gta\\build\\common\\data\\paths\\" types:"DAT (*.dat)|*.dat|"
		if file != undefined then BuildLine file
	)
	
	on btnExport pressed do (
		file = getsavefilename caption:"dat file to export" filename:"x:\\gta\\build\\common\\data\\paths\\" types:"DAT (*.dat)|*.dat|"
		if file != undefined then ExportSpline file
	)
	
	--------------------------------------------------------------
	-- Handhold Importer
	--------------------------------------------------------------
	on btnLoadHandHoldXml pressed do (
		LoadHandholdFile (getOpenFilename caption:"Handhold File" types:"xml file (*.xml)|*.xml")
	)
)

try CloseRolloutFloater PlacementToolkit catch()
PlacementToolkit = newRolloutFloater "Placement Toolkit" 300 750 50 96
addRollout Eplanter_roll PlacementToolkit
addRollout PlaceHelpersRoll PlacementToolkit
addRollout ChecksRoll PlacementToolkit
addRollout FileImportRoll PlacementToolkit