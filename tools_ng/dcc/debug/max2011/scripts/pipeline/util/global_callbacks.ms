--
-- File:: pipeline/util/global_callbacks.ms
-- Description:: Global callback functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 1 July 2009
--

--
-- name: RsGlobalCallbackProjectCheck
-- desc: This function is called on max file load, to prevent artists loading
--       a file from one project in a different project's Max configuration.
--
fn RsGlobalCallbackProjectCheck = (

	try
	(
		local params = ( callbacks.notificationParam() )
		if ( 1 == params[1] ) then
		(	
			local project = RsConfigInfos[RsConfig.currconfig]
			local filename = toLower ( RsMakeSafeSlashes params[2] )

			print "RsGlobalCallbackProjectCheck"
			format "\tProject:  %\n" project
			format "\tRoot:     %\n" project.projrootdir
			format "\tFilename: %\n" filename 

			if ( 1 != ( findString filename project.projrootdir ) ) then
			(
				ss = stringStream ""
				format "WARNING: Attempting to load a 3dsmax file not within % project's root directory.\n\n" project.name to:ss
				format "  DO NOT save this file if it has to be used for another project.  Attributes may be lost.\n\n" to:ss
				format "  Project root: %.\n" RsConfigInfos[RsConfig.currconfig].projrootdir to:ss
				format "  Filename:     %." filename to:ss
				messageBox (ss as String) beep:true
			)
		)
	)
	catch
	(
		format "Exception: %\n" ( getCurrentException() )
		--MXSDebugger.openDialog break:true message:"Exception during project check callback."
	)
)

-- pipeline/util/global_callbacks.ms
