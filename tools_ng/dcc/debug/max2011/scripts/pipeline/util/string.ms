-- Rockstar string utilities
-- Rockstar North
-- 1/3/2005
-- by Greg Smith

-- various functions for transforming strings

----------------------------------------------------------------------------------------
-- removes all the spaces from a string
----------------------------------------------------------------------------------------
fn RsRemoveSpacesFromStartOfString filename = (

	foundSomething = false
	newfilename = ""
	
	for i = 1 to filename.count do (
	
		if foundSomething or (filename[i] != " " and filename[i] != "\t") then (
		
			foundSomething = true
		)
		
		if foundSomething then (
		
			append newfilename filename[i]
		)
	)	
	
	newfilename
)

----------------------------------------------------------------------------------------
-- removes all the spaces from a string
----------------------------------------------------------------------------------------
fn RsRemoveSpacesFromString filename = (

	newfilename = ""
	
	for i = 1 to filename.count do (
	
		if filename[i] != " " then (
		
			append newfilename filename[i]
		)
	)	
	
	newfilename
)

--------------------------------------------------------------
-- removes all tabs from a string
--------------------------------------------------------------
fn RsRemoveTabsFromString filename = (
	
	newfilename = ""
		
	for i = 1 to filename.count do (

		if filename[i] != "	" then (

			append newfilename filename[i]
		)
	)	
		
	newfilename


)

--------------------------------------------------------------
-- returns true if the string contains spaces
--------------------------------------------------------------
fn RsContainsSpaces instring = (

	local retval = true
	blah = findstring instring " "

	if blah == undefined then (
	
		retval = false
	)
	
	retval
)

--------------------------------------------------------------
-- returns a lowercase version of the string
--------------------------------------------------------------
fn RsUppercase instring = (

	local upper, lower, outstring
	upper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lower="abcdefghijklmnopqrstuvwxyz" 
	
	outstring=copy instring 
		
	for i=1 to outstring.count do (
	
		j = findString lower outstring[i] 
		
		if (j != undefined) do (
		
			outstring[i] = upper[j] 
		)
	) 
	
	outstring
)

--------------------------------------------------------------
-- returns a lowercase version of the string
--------------------------------------------------------------
fn RsLowercase instring = (

	local upper, lower, outstring
	upper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lower="abcdefghijklmnopqrstuvwxyz" 
	
	outstring=copy instring 
		
	for i=1 to outstring.count do (
	
		j = findString upper outstring[i] 
		
		if (j != undefined) do (
		
			outstring[i] = lower[j] 
		)
	) 
	
	outstring
)

--------------------------------------------------------------
-- returns a version of the string with spaces converted to underscores
--------------------------------------------------------------
fn RsRemoveSpaces instring = (

	local upper, lower, outstring
	
	outstring=copy instring 
		
	for i=1 to outstring.count do (	
	
		if outstring[i] == " " then (
		
			outstring[i] = "_"
		)
	) 
	
	outstring
)

--------------------------------------------------------------
-- returns a list of string tokens (copes with spaces in speech marks)
--------------------------------------------------------------
fn RsFilter parse delimit = (

	inString = false
	tokenList = #()
	token = ""
	
	for i = 1 to parse.count do (
	
		char = parse[i]
		
		if char == delimit and inString == false then (
		
			append tokenList token
			token = ""
		) else (
		
			if char == "\"" then (
			
				if inString then (
				
					inString = false
				) else (
				
					inString = true
				)
			) 
			
			token = token + char
		)
	)
	
	if token != "" then (
	
		append tokenList token
	)
	
	tokenList
)

----------------------------------------------------------------------------------------
-- removes all the given character from a string
----------------------------------------------------------------------------------------
fn RsRemoveCharacterFromString filename char = (

	newfilename = ""
	
	for i = 1 to filename.count do (
	
		if filename[i] != char then (
		
			append newfilename filename[i]
		)
	)	
	
	newfilename
)

fn RsReplaceChar instring char1 char2= (

	local upper, lower, outstring
	
	outstring=copy instring 
		
	for i=1 to outstring.count do (	
	
		if outstring[i] == char1 then (
			
			outstring[i] = char2
		)
	) 
	
	outstring
)