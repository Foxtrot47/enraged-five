--
-- File:: pipeline/util/lod_toolkit.ms
-- Description:: LOD Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 19 March 2010
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/helpers/TerrainLodder.ms"
filein "pipeline/ui/lodfiddler.ms"
filein "pipeline/util/UtilityFunc.ms"
filein "pipeline/ui/treelodgenerator_ui.ms"
filein "pipeline/ui/lod_visibility_controller.ms"

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- SCRIPT HELPERS
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout LodScriptsRoll "Script Helpers"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/LOD_Toolkit#Script_Helpers" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	button btnGuess "Guess LOD Distance" width:133 height:26 across:2
	button btnTest "Test LOD Name" width:133 height:26
	button btnParent "Parent by Name & Pos" width:133 height:26 across:2
	button btnSphere "Show LOD Spheres" width:133 height:26
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnGuess pressed do (
		GuessLodDist selection
	)
	
	on btnTest pressed do (
		GtaTestForLodName()
	)
	
	on btnParent pressed do (
		filein "pipeline/util/setlodbynameandpos.ms"
	)
	
	on btnSphere pressed do (
		filein "pipeline/util/lodsphere.ms"
	)
)

try CloseRolloutFloater LodToolkit catch()
LodToolkit = newRolloutFloater "LOD Toolkit" 300 750 50 96
addRollout LodScriptsRoll LodToolkit
addRollout TerrainLodder_roll LodToolkit
addRollout RsTreeLodGeneratorRoll LodToolkit
addRollout objlodset LodToolkit
addRollout RsLODVisControllerRoll LodToolkit