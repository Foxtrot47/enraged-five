--
-- File:: pipeline/util/assert.ms
-- Description::
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 1 February 2010
--

--
-- name: RsAssert
-- desc: Invoke MaxScript assert function using common defaults.
--
-- If we use this across our MaxScript and find that its slow its easy enough 
-- to kill all references on the release branch rather than using "assert"
-- directly.
--
fn RsAssert expr message:"" options:#(#all) showstack:true = (

	assert expr message:message options:options showstack:showstack
)

-- pipeline/util/assert.ms
