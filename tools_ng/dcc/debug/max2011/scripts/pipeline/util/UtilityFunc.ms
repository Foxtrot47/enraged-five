--
-- File:: pipeline/util/UtilityFunc.ms
-- Description:: Small gta max utils for interface
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/GenSettings.ms"
filein "pipeline/util/Func.ms"
filein "rockstar/util/lod.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
fn GtaItObjEditableMesh obj = (
	return (classof obj == Editable_Mesh)
)

fn GtaCopyVertColours = (

	messagebox "Select Object To Copy From"
	objFrom = pickObject filter:GtaItObjEditableMesh
	
	messagebox "Select Object To Copy To"
	objTo = pickObject filter:GtaItObjEditableMesh
	
	if classof objFrom != Editable_Mesh then (
		return 0
	)

	if classof objTo != Editable_Mesh then (
		return 0
	)
	
	copyvertcoloursexact objFrom objTo
)

fn UpdateSelectionSets = (
	-- make a selection set so that the ide export (gta3.dle) doesnt export specified objects
	-- make a selection set so that the ipl export (gta3.dle) doesnt export specified objects

	SelDontExport = #()
	SelDontAddToIPL = #()

	for obj in $objects do (
		idxDontExport = -1
		idxDontAddToIPL = -1
		idxDontAddToIDE = -1

		if GetAttrClass obj == "Gta Object" then (
			idxDontExport = GetAttrIndex "Gta Object" "Dont Export"
			idxDontAddToIPL = GetAttrIndex "Gta Object" "Dont Add To IPL"
			idxDontAddToIDE = GetAttrIndex "Gta Object" "Dont Add To IDE"
		)

		if GetAttrClass obj == "GtaAnimHierarchy" then (
			idxDontExport = GetAttrIndex "GtaAnimHierarchy" "Dont Export"
			idxDontAddToIPL = GetAttrIndex "GtaAnimHierarchy" "Dont Add To IPL"
		)

		if idxDontExport != -1 then (
			if GetAttr obj idxDontExport == true then (
				append SelDontExport obj
			)			
		)

		if idxDontAddToIDE != -1 then (
			if GetAttr obj idxDontAddToIDE == true then (
				append SelDontExport obj
			)			
		)

		if idxDontAddToIPL != -1 then (
			if GetAttr obj idxDontAddToIPL == true then (
				append SelDontAddToIPL obj
			)			
		)
	)

	selectionsets["GtaDontExport"] = SelDontExport
	selectionsets["GtaIgnore"] = SelDontAddToIPL
)

fn roundVector vector = (

	vecRound = copy vector

	for i = 1 to 3 do (
	
		roundVal = vecRound[i]
		
		roundVal = roundVal * 128
		roundVal = floor roundVal
		roundVal = roundVal / 128
		
		vecRound[i] = roundVal
	)
	
	return vecRound
)

fn GTAChangePivotQuantised obj = (
	if classof obj == XRefObject then (
		newPivot = roundVector obj.pos
		
		if newPivot != obj.pos then (
			obj.pos = newPivot
			return 1
		) 
		
		return 0
	)
	
	if classof obj == Editable_mesh then (
		
		newPivot = roundVector obj.pivot
		
		if newPivot != obj.pivot then (
		
			obj.pivot = newPivot
		) 
		
		return 0
	)

	return 0
)

fn checkSelectedLessThanFiveTwelve = (

	for obj in selection do
	(
		obj.wirecolor = white
		
		if obj.max.x - obj.min.x > 512 do
			obj.wirecolor = red
		if obj.max.y - obj.min.y > 512 do
			obj.wirecolor = red
		if obj.max.z - obj.min.z > 512 do
			obj.wirecolor = red
	)
)

fn selectLODParent = (

	obj = selection[1]
	clearSelection()
	
	if (obj == undefined) do (
	
		return 0
	)
	parent = getlodattrparent obj
	
	if parent == undefined then (
		clearselection()
	) else (
		select parent	
	)
)

fn getMaxLODLevels lodparent = (
	
	childnodes = getLODChildren lodparent
	
	maxinchild = 0
	
	for childnode in childnodes do (
	
		inchild = getMaxLODLevels childnode
	
		if inchild > maxinchild then (
			maxinchild = inchild
		)
	)
	
	maxinchild = maxinchild + 1
	
	return maxinchild 
)

fn getLODChildren lodparent = (
	childObjs = #()
	
	obj = lodparent

	refobjs = refs.dependents obj

	for refObj in refobjs do (
	
		if isdeleted refObj == false then (
	
			if classof(refObj) == LodAttributes then (			
				
				refobjs2 = refs.dependents refObj
			
				if isProperty refobjs2[1] "name" then (
			
					append childObjs refobjs2[1]
				)
			)
		)
	)
	
	return childObjs
)

fn selectLODChildren = (

	if (selection[1] == undefined) do (
	
		return 0
	)
	lodChildObjs = getLODChildren selection[1]
	
	select lodChildObjs
)

fn selectAllLODChildrenRecursive obj currlevel = (
	
	colval = 255 - (30 * currlevel)

	obj.wirecolor = color colval colval colval

	refobjs = refs.dependents obj

	newlevel = currlevel + 1

	for refObj in refobjs do (
		if classof(refObj) == LodAttributes then (			
			
			refobjs2 = refs.dependents refObj
		--	refobjs3 = refs.dependents refobjs2[1]
			
			selectmore refobjs2[1]
			
			selectAllLODChildrenRecursive refobjs2[1] newlevel
		)
	)
)

fn selectAllLODChildren = (

	selall = #()

	level = 0

	for obj in selection do (
		append selall obj
	)

	clearSelection()

	for obj in selall do (
		selectAllLODChildrenRecursive obj level
	)
)

fn selectAllXrefRec objSearchList objAddList = (

	if objSearchList == undefined then (
		return 0
	)

	for obj in objSearchList do (
	
		if classof obj == XrefObject then (
			append objAddList obj
		)
	
		selectAllXrefRec obj.children objAddList
	)
)

fn selectAllXref = (
	selectSet = #()
	
	selectAllXrefRec $objects selectSet
	
	select selectSet
)

fn selectAllNormal = (

	selectSet = #()
	
	for obj in $objects do (
		if getattrclass	obj == "Gta Object" then (
			append selectSet obj
		)
	)
	
	select selectSet
)

--
-- fn: findHorizonObjectIndex
-- desc: Searches the Attributes array to see if 'Horizon Object' is specified
--
fn findHorizonObjectIndex = (

	-- Find the attribute list and split it into an array
	data = filterString (getControlData "Gta Object" "Attribute") ","
	
	for attr = 1 to data.count do (
	
		if (data[attr] == "Horizon Object") do (
		
			-- This needs decremented because the array starts at 0 in the plugin
			-- but begins at 1 in max
			return (attr - 1)
		)
	)
	
	false
)

fn selectAllHorizon = (

	selectSet = #()
	
	horizonIndex = findHorizonObjectIndex()
	
	if ( horizonIndex == false ) do (
	
		messagebox "No Horizon Object attribute found"
		return false
	)	 
	
	for obj in $objects do (
	
		if ( getattrclass obj == "Gta Object" ) do (
			
			-- Find out the index of Attribute and then find the 
			-- attribute this object has and see if it is Horizon Object
			attributeIndex = getattrindex "Gta Object" "Attribute"
			objAttribute = getattr obj attributeIndex
			
			if (objAttribute == horizonIndex) do (
				
				append SelectSet obj
			)
		)
	)
	
	select selectSet
)

fn setAllMaterialstoSummedArea = (
	print "calling new function"
)


global myattrib

fn ColourLightWireByLightColour = (

	maxLightMult = 0.0

	for lights = 1 to selection.count do
	(
		multVal = 1.0
	
		if isproperty selection[lights] "multiplier" then (
		
			multVal = selection[lights].multiplier
		)
	
		if(multVal > maxLightMult) then (
		
			maxLightMult = multVal
		)
	)

	for lights = 1 to selection.count do
	(
		try (
		
			multVal = 1.0

			if isproperty selection[lights] "multiplier" then (

				multVal = selection[lights].multiplier
			)		
		
			setval = (0.5 * selection[lights].rgb) + (0.5 * selection[lights].rgb * (multVal / maxLightMult))
			setval.a = 255.0
				
			selection[lights].wirecolor = setval
			selection[lights].wirecolor = setval
		)
	    	catch()
       )
)

fn AddEnvMapRecursive mat = (

	if (hasProperty mat "materialList") == true then (
	
		for childmat in mat.materialList do (
			AddEnvMapRecursive childmat
		)
	) else (
	
		found = false
		numAttrib = custAttributes.count mat
		
		for i = 1 to numAttrib do (
			attDef = custAttributes.get mat i
				
			if attDef.name == "mydata" then (	
				found = true
			)
		)
	
		if found == false then (
			custAttributes.add mat myattrib
		
			mat.mydata.TargetApp = "RenderWare"
			mat.mydata.DataType = "Material"
		)
	)
)

fn AddEnvMapAttributes = (

	for obj in selection do (

		if obj.material != undefined then (

			AddEnvMapRecursive obj.material
		)
	)
)

fn RemoveEnvMapRecursive mat = (

	if (hasProperty mat "materialList") == true then (

		numAttrib = custAttributes.count mat
		
		for i = 1 to numAttrib do (
			attDef = custAttributes.get mat i
				
			if attDef.name == "mydata" then (	
				custAttributes.delete mat i
				exit
			)
		)
		
		for childmat in mat.materialList do (
			RemoveEnvMapRecursive childmat
		)
		
	) else (
	
		numAttrib = custAttributes.count mat
		
		for i = 1 to numAttrib do (
			attDef = custAttributes.get mat i
				
			if attDef.name == "mydata" then (	
				custAttributes.delete mat i
				exit
			)
		)
	)
)

fn RemoveEnvMapAttributes = (

	for obj in selection do (

		if obj.material != undefined then (

			RemoveEnvMapRecursive obj.material
		)
	)
)

-- applies the optimise modifier to 
fn OptimisePlanarPolys = (
	if selection.count != 1 then (
		messagebox "Please select one object"
	)
	else (
		newopt = optimize()
		
		newopt.facethreshold1 = 0.01
		newopt.edgethreshold1 = 0.0
		newopt.bias1 = 0.0

		addModifier selection[1] newopt
	)
)

fn compareStringCaseInsensitiveFn v1 v2 = (
	lowercaseA = GTAlowercase v1
	lowercaseB = GTAlowercase v2
	
	if lowercaseA == lowercaseB then (
		return 0
	)
	
	if lowercaseA > lowercaseB then (
		return 1
	)
	
	return -1
)

fn GetObjectsUsingTXDsRecursive obj txdNameCompare objList = (
	if (getattrclass obj) == "Gta Object" do (
	
		local index = getattrindex "Gta Object" "TXD"

		valDontExport = getattr obj (getattrindex "Gta Object" "Dont Export")
		
		if valDontExport == false then (
			If (classof obj) !=XRefObject do (
				txdName = GTAlowercase(getattr obj index)
				if txdNameCompare == txdName then (
					append objList obj
				)
			)
		)
	)

	if (getattrclass obj) == "GtaAnimHierarchy" do (
	
		local index = getattrindex "GtaAnimHierarchy" "TXD"
		
		valDontExport = getattr obj (getattrindex "GtaAnimHierarchy" "Dont Export")
		
		if valDontExport == false then (
			If (classof obj) !=XRefObject do(
				txdName = GTAlowercase(getattr obj index)
				if txdNameCompare == txdName then (
					append objList obj
				)
			)
		)
	)

	if classof obj == Gta_MultiBldg then (
		for childobj in obj.children do (
			GetObjectsUsingTXDsRecursive childobj txdNameCompare objList
		)
	)
)

fn GetObjectsUsingTXD txdName objList = (
	for obj in $objects do (
		GetObjectsUsingTXDsRecursive obj txdName objList
	)
)

fn GetTXDRecursive obj txdList = (
	
	-- ignore objects without Gta Object attribute class
	if (getattrclass obj) == "Gta Object" do (
	
		local index = getattrindex "Gta Object" "TXD"

		valDontExport = getattr obj (getattrindex "Gta Object" "Dont Export")
		
		if valDontExport == false then (
			If (classof obj) !=XRefObject do (
				txdName = GTAlowercase(getattr obj index)
				if (findItem txdList txdName) == 0 do (
					append txdList txdName
				)
			)
		)
	)

	if (getattrclass obj) == "GtaAnimHierarchy" do (
	
		local index = getattrindex "GtaAnimHierarchy" "TXD"
		
		valDontExport = getattr obj (getattrindex "GtaAnimHierarchy" "Dont Export")
		
		if valDontExport == false then (
			If (classof obj) !=XRefObject do(
				txdName = GTAlowercase(getattr obj index)
				if (findItem txdList txdName) == 0 do (
					append txdList txdName
				)
			)
		)
	)

	if classof obj == Gta_MultiBldg then (
		for childobj in obj.children do (
			GetTXDRecursive childobj txdList
		)
	)
)

-- get TXDs from selected objects
fn GetTxdListSelected txdList = (

	for obj in selection do (
		GetTXDRecursive obj txdList
	)
	
	if txdList.count == 0 do (
		MessageBox ("TXD list is empty (selected) " + (selection.count as string))
		return false
	)
	
	qsort txdList compareStringCaseInsensitiveFn
	
	return true
)

-- get TXDs from scene
fn GetTxdList txdList = (

	for obj in $objects do (
		GetTXDRecursive obj txdList
	)
	
	if txdList.count == 0 do (
		MessageBox "TXD list is empty"
		return false
	)
	
	qsort txdList compareStringCaseInsensitiveFn
	
	return true
)

-- get TXDs from scene but append a string onto the end of each item
fn GetTxdListAppend txdList txtappend = (
	local index = getattrindex "Gta Object" "TXD"
	local txdName
	if index == undefined do (
		MessageBox "Gta Object class doesn't have a member called TXD"
		return false
	)
	if (getattrtype "Gta Object" index != "string") do (
		MessageBox "TXD member is not type string"
		return false
	)
	
	for obj in $objects do (
		-- ignore objects without Gta Object attribute class
		if (getattrclass obj) == "Gta Object" do (
				if (classof obj) !=XRefObject do(
				txdName = getattr obj index
				if (findItem txdList (txdName + txtappend)) == 0 do (
					append txdList (txdName + txtappend)
				)
			)
		)
	)
	
	if txdList.count == 0 do (
		MessageBox "TXD list is empty"
		return false
	)
	
	return true
)

fn GtaAddUniqueNames selset fieldname = (

	idxObjectField = GetAttrIndex "Gta Object" fieldname
	idxAnimHierField = GetAttrIndex "GtaAnimHierarchy" fieldname
	
	idxDontExportObj = GetAttrIndex "Gta Object" "Dont Export"
	idxDontExportAnim = GetAttrIndex "GtaAnimHierarchy" "Dont Export"

	for obj in $objects do (
	
		idxField = -1
		idxDontExport = -1

		if classof obj != XrefObject then (

			if GetAttrClass obj == "Gta Object" then (
				idxField = idxObjectField
				idxDontExport = idxDontExportObj
			)

			if GetAttrClass obj == "GtaAnimHierarchy" then (
				idxField = idxAnimHierField
				idxDontExport = idxDontExportAnim
			)

			if idxDontExport != -1 and idxField != -1 then (

				if GetAttr obj idxDontExport == false then (
	
					if idxField != -1 then (
	
						alreadyAdded = false
						fieldString =  GTAlowercase(GetAttr obj idxField)
	
						for selItem in selset do (				
							if fieldString == selItem then (
								alreadyAdded = true		
								exit
							)
						)
	
						if alreadyAdded == false then (
							append selset fieldString
						)
					)
				)
			)
		)
	)
	
	qsort selset compareStringCaseInsensitiveFn
)

fn SetDontExportSelected = (
	--selectionsets["GtaDontExport"] = selection
	
	idxDontExportGtaObj = GetAttrIndex "Gta Object" "Dont Export"
	idxDontExportGtaAnim = GetAttrIndex "GtaAnimHierarchy" "Dont Export"
	
	for obj in $objects do (
		
		if GetAttrClass obj =="Gta Object" then (
			idxDontExport = idxDontExportGtaObj
		)
		
		if GetAttrClass obj =="GtaAnimHierarchy" then (
			idxDontExport = idxDontExportGtaAnim
		)
		
		if idxDontExport != undefined then (

			found = false
		
			for checkobj in selection do (
			
				if checkobj == obj then (
					found = true
					exit
				)
			)
			
			if found then (
				SetAttr obj idxDontExport true
			) else (
				SetAttr obj idxDontExport false
			)
		)
	)
)

fn AddIgnoreSelected = (
	--selectionsets["GtaIgnore"] = selection
	
	idxDontAddToIPLGtaObj = GetAttrIndex "Gta Object" "Dont Add To IPL"
	idxDontAddToIPLGtaAnim = GetAttrIndex "GtaAnimHierarchy" "Dont Add To IPL"
	
	for obj in $objects do (
				
		if GetAttrClass obj =="Gta Object" then (
			idxDontAddToIPL = idxDontAddToIPLGtaObj
		)
		
		if GetAttrClass obj =="GtaAnimHierarchy" then (
			idxDontAddToIPL = idxDontAddToIPLGtaAnim
		)
		
		if idxDontAddToIPL != undefined then (

			found = false
		
			for checkobj in selection do (
			
				if checkobj == obj then (
					found = true
					exit
				)
			)
			
			if found then (
				SetAttr obj idxDontAddToIPL true
			) else (
				SetAttr obj idxDontAddToIPL false
			)
		)
	)
)

fn SetDontApplyRadiosity = (

	idxDontApplyRadiosityGtaObj = GetAttrIndex "Gta Object" "Dont Apply Radiosity"
	idxDontApplyRadiosityGtaAnim = GetAttrIndex "GtaAnimHierarchy" "Dont Apply Radiosity"

	for obj in $objects do (
		
		if GetAttrClass obj =="Gta Object" then (
			idxDontApplyRadiosity = idxDontApplyRadiosityGtaObj
		)
		
		if GetAttrClass obj =="GtaAnimHierarchy" then (
			idxDontApplyRadiosity = idxDontApplyRadiosityGtaAnim
		)
		
		if idxDontApplyRadiosity != undefined then (

			found = false
		
			for checkobj in selection do (
			
				if checkobj == obj then (
					found = true
					exit
				)
			)
			
			if found then (
				SetAttr obj idxDontApplyRadiosity true
			) else (
				SetAttr obj idxDontApplyRadiosity false
			)
		)
	)
)

-- creates a collision mesh for the currently selected object
fn CreateCollisionMesh = (

	if selection.count != 1 then (
	
		return 0
	)
	
	if (selection[1].scale.x != 1.0) or (selection[1].scale.y != 1.0) or (selection[1].scale.z != 1.0) then (
	
		messagebox "error: object is scaled"
		return 0
	)	
	
	-- volumecreatemod selection[1]
	volcreate = VolumeCreate()
	
	addModifier selection[1] volcreate
	volumecreatemod volcreate
	deleteModifier selection[1] volcreate
	subobjectLevel = 4
)

-- creates a collision mesh for the currently selected object
fn CreateShadowMesh = (

	if selection.count != 1 then (
	
		return 0
	)
	
	if (selection[1].scale.x != 1.0) or (selection[1].scale.y != 1.0) or (selection[1].scale.z != 1.0) then (
	
		messagebox "error: object is scaled"
		return 0
	)
	
	volcreate = VolumeCreate()
	
	addModifier selection[1] volcreate
	volumecreateshad volcreate
	deleteModifier selection[1] volcreate
	subobjectLevel = 4
)

fn CreateCollisionMeshRemoved = (

	if selection.count != 1 then (
		return 0
	)
	
	--make a list of all this objects children
	parentObj = selection[1]
	if (getAttrClass parentObj)!="Gta Object" then
	(
		messagebox "has to be mesh class!"
		return false
	)
	
	origChildObjs = #()
	
	for childObj in parentObj.children do (
		append origChildObjs childObj
	)

	--create the collision mesh
	CreateCollisionMesh()

	--find the new object
	newObj = undefined 	
	
	for childObj in parentObj.children do (
	
		found = false
	
		for compareObj in origChildObjs do (
			if compareObj == childObj then (
				found = true
			)
		)
		
		if found == false then (
			newObj = childObj
		)
	)
	
	if newObj != undefined then (
		newObj.parent = parentObj.parent	
	)
	
	select parentObj
	subobjectLevel = 4
	
	-- delete the selected faces in the object we
	-- we are creating from

	meshop.deletefaces parentObj (getfaceselection parentObj)

	return unwrapuvw
)

fn CreateCollisionCapsule = (

	if selection.count != 1 then (
	
		return 0
	)
	
	if (selection[1].scale.x != 1.0) or (selection[1].scale.y != 1.0) or (selection[1].scale.z != 1.0) then (
	
		messagebox "error: object is scaled"
		return 0
	)
	
	-- volumecreatemod selection[1]
	volcreate = VolumeCreate()
	
	addModifier selection[1] volcreate
	volumecreatecapsule volcreate
	deleteModifier selection[1] volcreate
	subobjectLevel = 4
)

fn CreateCollisionCylinder = (

	if selection.count != 1 then (
	
		return 0
	)
	
	if (selection[1].scale.x != 1.0) or (selection[1].scale.y != 1.0) or (selection[1].scale.z != 1.0) then (
	
		messagebox "error: object is scaled"
		return 0
	)
	
	-- volumecreatemod selection[1]
	volcreate = VolumeCreate()
	
	addModifier selection[1] volcreate
	volumecreatecylinder volcreate
	deleteModifier selection[1] volcreate
	subobjectLevel = 4
)

fn CreateCollisionBox = (

	if selection.count != 1 then (
	
		return 0
	)
	
	if (selection[1].scale.x != 1.0) or (selection[1].scale.y != 1.0) or (selection[1].scale.z != 1.0) then (
	
		messagebox "error: object is scaled"
		return 0
	)
	
	-- volumecreatemod selection[1]
	volcreate = VolumeCreate()
	
	addModifier selection[1] volcreate
	volumecreatecube volcreate
	deleteModifier selection[1] volcreate
	subobjectLevel = 4
)

fn CreateCollisionSphere = (

	if selection.count != 1 then (
	
		return 0
	)
	
	if (selection[1].scale.x != 1.0) or (selection[1].scale.y != 1.0) or (selection[1].scale.z != 1.0) then (
	
		messagebox "error: object is scaled"
		return 0
	)
	
	-- volumecreatemod selection[1]
	volcreate = VolumeCreate()
	
	addModifier selection[1] volcreate
	volumecreatesphere volcreate
	deleteModifier selection[1] volcreate
	subobjectLevel = 4
)

-- reset the gta id for the selected objects. they will reacquire them on next export
fn removedUniqueIDFromSelected = (
	for object = 1 to selection.count do (
		RemoveUniqueID selection[object]
	)
)

fn ShowSuperLodObjects objset = (
	for object = 1 to objset.count do 				
	(
		if getattrclass objset[object] == "Gta Collision" then (
				unhide objset[object]
		) else (
	
			if objset[object] == XRefObject then (
				currentname = GTAlowercase(objset[object].objectname)
			) else (
				currentname = GTAlowercase(objset[object].name)
			)

			found = (findstring currentname "superlod") 

			if found == undefined and (classof objset[object] != Sphere) then  (
				hide objset[object] 
			) else (
				unhide objset[object]
			)
		)
		
		if objset[object].children != undefined then (
			ShowSuperLodObjects objset[object].children
		)
	)
)

fn ShowLodObjects objset = (
	for object = 1 to objset.count do 				
	( 	
		if getattrclass objset[object] == "Gta Collision" then (
				unhide objset[object]
		) else (
	
			if objset[object] == XRefObject then (
				currentname = GTAlowercase(objset[object].objectname)
			) else (
				currentname = GTAlowercase(objset[object].name)
			)

			found = (findstring currentname "superlod") 

			if found != undefined then  (
				hide objset[object] 
			) else (
				found = (findstring currentname "lod") 

				if found == undefined and (classof objset[object] != Sphere) then (

					if classof objset[object] != XRefObject then (

						valLodDistance = RsGetObjLodDistance objset[object]

						if valLodDistance > 300 then (
							unhide objset[object]
						) else (
							hide objset[object] 
						)
					) else (
						hide objset[object] 
					)

				) else (
					unhide objset[object]
				)
			)
		)
		
		if objset[object].children != undefined then (
			ShowLodObjects objset[object].children
		)
	)
)

fn ShowHiDetailObjects objset = (
	for object = 1 to objset.count do 				
	( 		
		if getattrclass objset[object] == "Gta Collision" then (
			unhide objset[object]
		) else (
	
			if objset[object] == XRefObject then (
				currentname = GTAlowercase(objset[object].objectname)
			) else (
				currentname = GTAlowercase(objset[object].name)
			)

			found = (findstring currentname "lod") 

			if classof objset[object] == Sphere then (
				unhide objset[object]
			) else (

				if found != undefined then  (
					hide objset[object] 
				) else (

					if classof objset[object] != XRefObject then (

						valLodDistance = RsGetObjLodDistance idxLodDistance

						if valLodDistance < 300 then (
							unhide objset[object]
						)
					) else (
						unhide objset[object]
					)
				)
			)
		)
		
		if objset[object].children != undefined then (
			ShowHiDetailObjects objset[object].children
		)
	)
)

--hides or displays objects containing objname
fn HideNamedObjects objname hideme = (
	for object = 1 to $objects.count do 				
	( 				
		currentname = $objects[object].name 				
		found = (findstring currentname objname) 
		if hideme == true then(
			if found != undefined  then hide $objects[object] else unhide $objects[object]
		) else(
			if found != undefined  then unhide $objects[object] else hide $objects[object]	
		)
	)
)

--setwirecolour of all objects in the scene based on its TXD value
fn SetWireColourByTXD = (
	local classname = "Gta Object"
	local attrindex = getattrindex classname "TXD"
	if attrindex == undefined do return false
	
	txdlist=#()
	txdcolour=#()
	for obj in $objects do (
		-- ignore objects without Gta Object attribute class
		if (getattrclass obj) == "Gta Object" do (
			txdName = getattr obj attrindex -- this is the txd name
			if (findItem txdList txdName) == 0 do (
				append txdList txdName
			)
		)
	)
	---setsup colors

	for txdlists=1 to txdlist.count do
	(
		txdcolour[txdlists] = [(random 0 255),(random 0 255),(random 0 255)]
	)
	for obj in $objects do 
	(
		try (
			txdName = getattr obj attrindex 
			objtxd=findItem txdList txdName
			if objtxd > 0 then obj.wirecolor = txdcolour[objtxd]
		)
		catch()
	)
)
	
	
-- GuessLodDist of object based on the size of the object
fn GuessLodDist objList = (
	local lodDistBaseLists = #(#(100), #(100,350), #(150,210,250), #(130, 190, 230, 250))
	--local lodDistBaseLists = #(#(250), #(175,250), #(150,210,250), #(130, 190, 230, 250))
	local distAttrIndex = getattrindex "Gta Object" "LOD distance"
	
	if objlist.count == 0 do (
		
		messagebox "No objects selected"
		return false
	)
	
	if objlist == undefined do (
	
		messagebox "Object list is undefined"
		return false
	)
	
	for obj in objList do (
		local name = (GtaGetName obj.name)
		local lodList
		local dist
		local list
		local bbSize
		local maxDim
		local ratio = 0.4
		
		-- If LOD 0
		if (GtaGetLod obj.name) == 0 do (
			lodList = #()
			
			-- work out ratio to multiply all the object lod distances by. This is
			-- based on the volume of the object
			bbSize = obj.max - obj.min
			maxDim = bbSize.x
			if bbSize.y > maxDim do maxDim = bbSize.y
			if bbSize.z > maxDim do maxDim = bbSize.z
			--maxdimwas 20
			if maxDim > 50 then (
				ratio = 1.0
			) else (
				if maxDim > 1 do (ratio = 0.4 + (0.6 * (maxDim - 1) / 15)) --was 19
			)	
			
			-- calculate maxDim in x or y
			maxDim = bbSize.x

			if bbSize.y > maxDim do maxDim = bbSize.y

			-- Get list of object with object name
			GtaFindObjectsNamed name lodList
			
			list = lodList.count
			
			-- for the number of lods in the list
			for lodNum = 1 to list do (
				-- find lod with number 'lodnum'
				for lodIndex = 1 to list do (
					if (GtaGetLod lodList[lodIndex].name) == (lodNum-1) do (
						-- set lod distance
						dist = (ratio * lodDistBaseLists[list][lodNum]) + (maxDim/2)
						setattr lodList[lodIndex] distAttrIndex (dist as integer)
 					)
				)
			)
		)
	)
)

-- Gta Test for objects have LODs
fn GtaTestForLodName = (
	for loopa = 1 to $objects.count do (
		name = $objects[loopa].name
		$objects[loopa].wirecolor = white
		newname = "LOD"+(substring name 4 -1)
		if (findstring name "LOD" ==undefined) do(
			for testb = 1 to $objects.count do (
				if ($objects[testb].name == newname) do $objects[testb].wirecolor = red
				print newname
			)
		)
	)
)

fn ResetDiffuseToWhiteForMat setmat = (
	if classof setmat == standard then 
	(
		setmat.diffuse=[255,255,255]
	)
	if classof setmat == rwmaterial then 
	(
		setmat.defmtl_color=[255,255,255]
	) 
	if classof setmat == multimaterial then
	(
		for subs = 1 to setmat.materiallist.count do
		(
			ResetDiffuseToWhiteForMat setmat.materialList[subs]
		)
	)
)

-- set diffuse on all the scene materials to be white
fn ResetDiffuseToWhite = (
	for mat = 1 to scenematerials.count do 
	(
		ResetDiffuseToWhiteForMat scenematerials[mat]
	)
)

-- functions for switching the viewed map for light maps

lightmapChangeType = undefined

fn lightmapLightMapRW mat = (		
	if mat.defmtl_matfxeffect != 4 then (
		return 0
	)
	
	if lightmapChangeType == 1 then (
		showTextureMap mat true
	)
	
	if lightmapChangeType == 2 then (
		showTextureMap mat mat.defmtl_texmap_texture true
	)
	
	if lightmapChangeType == 3 then (
		showTextureMap mat mat.defmtl_texmap_pass2 true
	)
	
	return 0
)

fn lightmapLightMapMulti mat = (
	for i = 1 to mat.materialList.count do (
	
		submat = mat.materialList[i]
	
		if classof(submat) == RwMaterial then (
			lightmapLightMapRW submat
		)

		if classof(submat) == Multimaterial then (
			lightmapLightMapMulti submat
		)
	)
	
	return mat
)

fn lightmapLightMapDoMat mat = (
	if classof(mat) == RwMaterial then (
		lightmapLightMapRW mat
	)
	
	if classof(mat) == Multimaterial then (
		lightmapLightMapMulti mat
	)
	
	return mat
)

fn lightmapLightMapDoChildren parentnode = (

	for childnode in parentnode do (
		lightmapLightMapDoMat childnode.material
		
		if childnode.children != undefined then (
			lightmapLightMapDoChildren childnode.children
		)
	)
)

fn lightMapShowAll = (
	lightmapChangeType = 1
	lightmapLightMapDoChildren selection
)

fn lightMapShowBase = (
	lightmapChangeType = 2
	lightmapLightMapDoChildren selection
)

fn lightMapShowLightMap = (
	lightmapChangeType = 3
	lightmapLightMapDoChildren selection
)