--
-- File:: shared_texture_list.ms
-- Description:: Shared Texture List Text File Management
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 6 February 2008
--
-- This file defines a reusable list structure for maintaining an text file
-- list of shared texture files.
--
-- Example Usage
--
--   texlist = SharedTextureList()
--   texlist.Load <filename>
--   textures = texlist.GetTextureFilename()
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
fileIn "pipeline/util/file.ms"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

--
-- struct: SharedTextureList 
-- desc:
--
struct SharedTextureList
(

	filename = undefined,	-- Shared texture list filename
	textures = #(),			-- Shared texture filename array
	
	--
	-- name: load
	-- desc: Load list from a file (if exists)
	--
	fn load filePath = (
	
		filename = filePath
		textures = #()
		local fileExists = ( ( getFiles filename ).count != 0 )

		if ( ( undefined != filename ) and fileExists ) then
		(
			try
			(
				format "Loading shared texture list: %\n" filename
				local fsTextures = ( openFile filename mode:"r" )
				while ( not eof( fsTextures ) ) do
				(
					local lineData = ( readLine fsTextures )
					lineData = ( RsMakeSafeSlashes lineData )
					append textures lineData
				)
				close fsTextures
				
				true
			)
			catch
			(
				false
			)			
		)
		else
		(
			false
		)
	),
	
	--
	-- name: reload
	-- desc: Reload list from XML file
	--
	fn reload = (
	
		( load filename )
	),
	
	--
	-- name: save
	-- desc: Save list to current text file.
	--
	fn save = (	
	
		try
		(
			local fsTextures = ( openFile filename mode:"w" )
			for t in textures do
			(
				format "Write: %\n" t
				format "%\n" t to:fsTextures 
			)
			close fsTextures

			true
		)
		catch
		(
			false
		)	
	),
	
	--
	-- name: saveas
	-- desc: Save texture list as new text file.
	--
	fn saveas newfilename = (
	
		filename = newfilename
		save()
	),
	
	--
	-- name: count
	-- desc: Return number of textures in list
	--
	fn count = (
	
		textures.count
	),
	
	--
	-- name: contains
	-- desc: Returns true iff texture list contains filename, false otherwise.
	--
	fn contains texfile = (
	
		texfile = RsMakeSafeSlashes( texfile )
		( 0 != ( findItem textures texfile ) )
	),
	
	--
	-- name: add
	-- desc: Add texture to list
	--
	fn add texfile = (
	
		texfile = RsMakeSafeSlashes( texfile )
		if ( contains texfile ) then
		(
			false
		)
		else
		(
			append textures texfile
			true
		)
	),
	
	--
	-- name: remove
	-- desc: Remove texture from list
	--
	fn remove texfile = (
	
		texfile = RsMakeSafeSlashes( texfile )
		local index = ( findItem textures texfile )
		if ( -1 == index ) then
			false
		else
		(
			deleteItem textures index
			true
		)
	),
	
	--
	-- name: export
	-- desc: Export all of the textures to a specific directory
	--
	fn export directory = (
		
		format "Exporting textures:\n"
		for texmap in textures do
		(
			local infilename = ( RsRemovePath texmap )
			local infile = texmap
			local outfile = RsMakeSafeSlashes (directory+"/"+infilename)
			
			format "\nExport texture  : % to %\n" infile outfile
			rexExportTexture infile outfile
		)
	)
	
)

-- End of shared_texture_list.ms
