--
-- batch_export_list.ms
-- Interface to a batch export list XML file.
--
-- David Muir <david.muir@rockstarnorth.com>
-- 7 October 2008
--

include "pipeline/util/file.ms"
include "pipeline/util/xml.ms"

struct BatchExportList
(

	filename = "",
	xml,

	--
	-- name: Load
	-- desc: Load batch export list from XML filename
	--
	fn Load xmlfilename = (
	
		filename = xmlfilename
		xml = XmlDocument()
		xml.init()
		xml.load( filename )
		
		xmlRoot = xml.document.DocumentElement
		( "batch" == xmlRoot.name )
	),
	
	--
	-- name: Reload
	-- desc: Reload list from XML file
	--
	fn Reload = (
	
		load filename
	),
	
	--
	-- name: GetRootPath
	-- desc: Return the root path string for all max files in the batch.
	--
	fn GetRootPath = (
		
		rootpath = ""
		xmlRoot = xml.document.DocumentElement
		if ( "batch" == xmlRoot.name ) then
		(
			rootpath = xmlRoot.Attributes.ItemOf( "root" )
		)
		
		rootpath
	),
	
	--
	-- name: GetMaxFilenames
	-- desc: Return array of absolute Max filenames for batch export.
	--
	fn GetMaxFilenames = (
		
		maxfiles = #()
		xmlRoot = xml.document.DocumentElement
		if ( "batch" == xmlRoot.name ) then
		(
			local rootpath = xmlRoot.Attributes.ItemOf( "root" )
			rootpath = rootpath.Value
		
			local childNodeList = xmlRoot.ChildNodes
			for nChild = 0 to ( childNodeList.Count - 1 ) do
			(
				
				local elem = childNodeList.ItemOf( nChild )
				if ( "file" == elem.name ) then
				(
				
					local attr = elem.Attributes.ItemOf( "filename" )
					append maxfiles ( rootpath + "/" + attr.Value )
				)
			)
		)
		
		maxfiles
	),
	
	--
	-- name: Count
	-- desc: Return number of maxfiles in the batch list
	--
	fn Count = (
	
		local maxfiles = GetMaxFilenames()
		maxfiles.count
	)
)

-- batch_export_list.ms
