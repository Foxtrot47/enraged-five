filein "rockstar/util/attribute.ms"

RsFragBoundList = #()
RsTotalSurfProperty = ""

--------------------------------------------------------------
-- setup the ragdoll version of the ped model's bounds elements
--------------------------------------------------------------		
fn RsSetupFragBoundsInner objnode rexroot pedDir = (

	numBones = 0

	if objnode == undefined then (
	
		return 0
	)

	if classof objnode == Col_Capsule or classof objnode == Col_Mesh or classof objnode == Col_Box or classof objnode == Col_Sphere then (

		surfaceType = RsGetCollSurfaceTypeString objnode
		RsTotalSurfProperty = RsTotalSurfProperty + objnode.name + "=" + surfaceType + ":"
		
		append RsFragBoundList objnode
	) else (
	
		nameCheck = RsLowercase objnode.name
	
		subnameCheck = ""
		
		if nameCheck.count >= 3 then (
	
			subnameCheck = (substring nameCheck (nameCheck.count - 2) 3)
		)
	
		if (findstring namecheck "footsteps" == undefined) and (subnameCheck != "nub") then (
		
			numBones += 1
		)
		
--		print numBones
	)

	for childobj in objnode.children do (
	
		numBones += RsSetupFragBoundsInner childobj rexroot pedDir
	)
	
	numBones
)


--------------------------------------------------------------
-- setup the ragdoll version of the ped model's bounds elements
--------------------------------------------------------------		
fn RsSetupFragBounds objnode rexroot pedDir = (

	RsFragBoundList = #()

	ret = RsSetupFragBoundsInner objnode rexroot pedDir
	clearSelection()
	select RsFragBoundList
	
	if selection.count > 0 then (
	
		bound = rexAddChild rexroot selection[1].name "Bound"
		rexSetPropertyValue bound "OutputPath" pedDir
		rexSetPropertyValue bound "ForceExportAsComposite" "false"
		rexSetPropertyValue bound "ExportAsComposite" "false"
		rexSetPropertyValue bound "BoundZeroPrimitiveCentroids" "true"
		rexSetPropertyValue bound "BoundExportWorldSpace" "false"
		rexSetPropertyValue bound "SurfaceType" RsTotalSurfProperty
		rexSetNodes bound	
	)
	
	ret
)