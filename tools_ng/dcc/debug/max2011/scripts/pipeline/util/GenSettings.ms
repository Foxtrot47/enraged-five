--
-- File:: pipeline/util/GenSettings.ms
-- Description:: General settings functions
--
-----------------------------------------------------------------------------

include "pipeline/util/ReadIni.ms"

-- utility functions

global GtaPossibleDevKits = #()

fn FillPossibleIPs = (
	GtaPossibleDevKits = #()

	possDevKitsFilename = ((getdir #maxroot) + "/plugcfg") + "\\devkits.txt"

	possDevKitsFile = openfile possDevKitsFilename mode:"r"

	if possDevKitsFile != undefined then (

		while eof(possDevKitsFile) == false do (
			possDevKitsLine = readLine possDevKitsFile
			possDevKitsTokens = filterstring possDevKitsLine "."

			if possDevKitsTokens.count == 4 then (
				append GtaPossibleDevKits possDevKitsLine
			)
		)

		close possDevKitsFile
	)
)

FillPossibleIPs()

fn AddSlashAtEndOfFolderName &foldername = (
	if foldername[foldername.count] != "\\" do (
		append foldername "\\"
	)
)

fn GetProjectName = (

	--now takes the project name as the name in the title of each setting

	if gta_configname == "<custom>" then (
		folders = (filterstring gta_project_rootdir "\\" )
		return folders[folders.count]
	)

	return gta_configname
) 


fn GetRWFilemakerDll = (

	returnString = "unknown"

	if (gta_target as string) == "1" do returnString = "rwfmps2_36003.dll"
	if (gta_target as string) == "2" do returnString = "rwfmpc_36003.dll" 
	if (gta_target as string) == "3" do returnString = "rwfmxbox_36030.dll" 
	
	return returnString
)

fn GetPlatformString = (
	
	returnString = "unknown"
	
	if (gta_target as string) == "1" do returnString = "ps2"
	if (gta_target as string) == "2" do returnString = "pc"
	if (gta_target as string) == "3" do returnString = "xbox"
		
	return returnString
)

fn GetListIndexFromActiveIndex activeindex = (
	listindex = 0
	for i = 1 to activeindex do (
		if gta_cnfactive[i] == 1 do (
			listindex = listindex + 1
		)
	)
	
	return listindex
)

fn GetActiveIndexFromListIndex listindex = (
	activeIndex = 0
	for i = 1 to listindex do (
		activeIndex = activeIndex + 1
		while gta_cnfactive[activeIndex] == 0 do (
			activeIndex = activeIndex + 1
		)
	)
	
	return activeIndex
)

-- the variable list

-- configuration information
-- this is just initial information, the user is able to add, delete and modify the configurations
-- config 1 is always active as the custom configuration

gta_numconfigs = ReadGtaIntegerOveride gta_numconfigs "settings" "NumConfigs" 12
gta_cnfactive = #(1,1,1,1,1,1,1,1,1)
gta_platform_labels = #("PS2", "PC", "XBOX")
gta_cnfcurrent = ReadGtaInteger gta_cnfcurrent "settings" "SetConfig" 1
gta_configs = #("<custom>", "SA_PS2", "SA_PC", "MIAMI_PS2", "MIAMI_PC","TOKYO_PS2","SA_XBOX","ZED_PC","GTA_PC")
gta_cnfplatform = #(1,1,2,1,2,1,3,2,2)
gta_cnftristrip = #(1,1,1,1,1,1,1,1,1)
gta_cnfpreinstance = #(false,false,false,false,false,false,false,false,false)
gta_cnfbindir = #("x:\\sa\\gta_bin\\","x:\\sa\\gta_bin\\","x:\\sa\\gta_bin\\","x:\\sa\\gta_bin\\","x:\\sa\\gta_bin\\","x:\\sa\\gta_bin\\","x:\\sa\\gta_bin\\","x:\\tools_release\\gta_bin\\","x:\\tools_release\\gta_bin\\")
gta_cnfprojbin = #("x:\\sa\\gta_bin\\sa_ps2\\","x:\\sa\\gta_bin\\sa_ps2\\","x:\\sa\\gta_bin\\sa_pc\\","x:\\sa\\gta_bin\\miami_ps2\\","x:\\sa\\gta_bin\\miami_pc\\","x:\\sa\\gta_bin\\Tokyo_ps2\\","x:\\sa\\gta_bin\\sa_xbox\\","x:\\tools_release\\gta_bin\\zed\\","x:\\tools_release\\gta_bin\\gta\\")
gta_cnfprojroot = #("x:\\sa\\sa_ps2\\","x:\\sa\\sa_ps2\\","x:\\sa\\sa_pc\\","x:\\sa\\miami_ps2\\","x:\\sa\\miami_pc\\","x:\\Tokyo\\TokyoPs2\\","x:\\sa\\sa_xbox\\","x:\\zed\\zed_pc\\","x:\\gta\\gta_pc\\")
gta_cnfanimdir = #("x:\\sa\\sa art\\anim\\","x:\\sa\\sa art\\anim\\","x:\\sa\\sa art\\anim\\","x:\\sa\\miami art\\anim\\","x:\\sa\\miami art\\anim\\","x:\\Tokyo\\Tokyo_art\\anim\\","x:\\sa\\sa art\\anim\\","x:\\zed\\zed_art\\anim\\","x:\\gta\\gta_art\\anim\\")
gta_cnftexturedir = #("n:\\texturesps2\\","n:\\texturesps2\\","n:\\texturespc\\","n:\\texturesps2\\","n:\\texturespc\\","n:\\Tokyo_texturesps2\\","n:\\texturesxbox\\","n:\\textureszed\\","n:\\texturesgta\\")
gta_cnftextureloddir = #("n:\\textureslod\\","n:\\textureslod\\","n:\\textureslod\\","n:\\textureslod\\","n:\\textureslod\\","n:\\Tokyo_textureslod\\","n:\\textureslod\\","n:\\textureszedlod\\","n:\\texturesgtalod\\")
gta_cnfstreamdir = #("n:\\streamps2\\","n:\\streamps2\\","n:\\streampc\\","n:\\streamps2\\","n:\\streampc\\","n:\\Tokyo_streamps2\\","n:\\streamxbox\\","n:\\streamzed\\","n:\\streamgta\\")

-- these are the important gta settings used in various scripts

global gta_configname = ReadConfigGtaString "Name" gta_configs[gta_cnfcurrent]
global gta_bindir = ReadConfigGtaString "Bin" gta_cnfbindir[gta_cnfcurrent]
global gta_project_bin = ReadConfigGtaString "ProjectBin" gta_cnfprojbin[gta_cnfcurrent]
global gta_project_rootdir = ReadConfigGtaString "RootDir" gta_cnfprojroot[gta_cnfcurrent]
global gta_animdir = ReadConfigGtaString "AnimDir" gta_cnfanimdir[gta_cnfcurrent]
global gta_texturedir = ReadConfigGtaString "TextureDir" gta_cnftexturedir[gta_cnfcurrent]
global gta_lodtexturedir = ReadConfigGtaString "LodTextureDir" gta_cnftextureloddir[gta_cnfcurrent]
global gta_streamdir  = ReadConfigGtaString "StreamDir" gta_cnfstreamdir[gta_cnfcurrent]
global gta_target = ReadConfigGtaString "Target" gta_cnfplatform[gta_cnfcurrent]
global gta_rwfilemaker = "rwfilemaker"
global gta_export_settings_rolledup = ReadGtaBoolean gta_export_settings_rolledup "settings" "settingsrollup" false
global gta_tristrip = ReadConfigGtaString "TriStrip" gta_cnftristrip[gta_cnfcurrent]
global gta_exportlog = ReadGtaBoolean gta_exportlog "settings" "ExportLog" false
global gta_preinstance = ReadConfigGtaBoolean "Preinstance" gta_cnfpreinstance[gta_cnfcurrent]
global gta_preinstanceipidx = ReadGtaInteger gta_preinstanceipidx "settings" "PreIP" 1
global gta_comutil = "\"c:\\program files\\metrowerks\\codewarrior\\bin\\cwcomutil\" -quit -tg ps2t10kt -d "

-- the rollout

rollout GtaSettingsRoll "GTA Settings"
(
	-- UI
	
	checkbox showexportlog "Show Export Log" checked:gta_exportlog
	dropdownlist iplist "Preinstance IP" 
	-- Store Configurations on a project basis
	dropdownlist configlist "Configurations"
	
	button buttonAdd "Add" offset:[-30,0]
	button buttonRemove "Remove" offset:[20,-26]
	
	edittext confignametext "Config Name:" 
	radiobuttons platformradio "Target Platform" labels:gta_platform_labels default:1
	checkbox tristripcheck "Tri Stripping"
	checkbox preinstancecheck "Preinstance" checked:gta_preinstance
	
	group "Directories" (
		edittext bintext "Bin Folder:"
		edittext projectbintext "Project Bin:"
		edittext projectroottext "Project Root:"
		edittext texturetext "Texture Dir:"
		edittext lodtexturetext "Lod Dir:"
		edittext projectanimtext "Anim Folder:"
		edittext streamtext "Stream Folder:"
	)
	
	-- FUNCTIONS
	
	fn fillListBox = (
		local indexCount = 1
		listItems = #()
	
		if listItems.count > 0 then (
			deleteItem listItems listItems.count
		)
	
		-- fills the list box with only active configurations
		for i = 1 to gta_numconfigs do (
			if gta_cnfactive[i] == 1 then (
				indexCount = indexCount + 1				
				append listItems (ReadGtaStringOverride gta_configs[i] ("Config" + (i as string)) "Name" gta_configs[i])
			)
		)
		
		configlist.items = listItems
	)
	
	fn fillIpListBox = (
		iplist.items = GtaPossibleDevKits
	)
	
	fn updatePlatformRadio value = (
		gta_target = value as string
	)
	fn updateSettings value = (
	
		gta_cnfcurrent = value as integer
		WriteGtaVariable gta_cnfcurrent "settings" "SetConfig"
	
		-- do any ui changes
		
		if gta_cnfcurrent == 1 then (
			buttonRemove.enabled = false
			confignametext.enabled = false
		) else (
			buttonRemove.enabled = true
			confignametext.enabled = true
		)
		
		-- update the variables
			
		fillListBox()
		
		-- change our target variables
		
		updatePlatformRadio (ReadConfigGtaInteger "Target" gta_cnfplatform[gta_cnfcurrent])
		gta_bindir = ReadConfigGtaString "Bin" gta_cnfbindir[gta_cnfcurrent]
		gta_project_bin = ReadConfigGtaString "ProjectBin" gta_cnfprojbin[gta_cnfcurrent]
		gta_project_rootdir = ReadConfigGtaString "RootDir" gta_cnfprojroot[gta_cnfcurrent]
		gta_animdir = ReadConfigGtaString "AnimDir" gta_cnfanimdir[gta_cnfcurrent]
		gta_texturedir = ReadConfigGtaString "TextureDir" gta_cnftexturedir[gta_cnfcurrent]
		gta_lodtexturedir = ReadConfigGtaString "LodTextureDir" gta_cnftextureloddir[gta_cnfcurrent]
		gta_streamdir  = ReadConfigGtaString "StreamDir" gta_cnfstreamdir[gta_cnfcurrent]
		gta_configname = ReadConfigGtaString "Name" gta_configs[gta_cnfcurrent]
		
		--lets just always make this 0 for now
		
		gta_tristrip = ReadConfigGtaString "TriStrip" gta_cnftristrip[gta_cnfcurrent]
		
		--update the gui
		
		platformradio.state = (gta_target as integer)
		confignametext.text = gta_configname
		bintext.text = gta_bindir
		projectbintext.text = gta_project_bin
		projectroottext.text = gta_project_rootdir
		texturetext.text = gta_texturedir
		lodtexturetext.text = gta_lodtexturedir
		projectanimtext.text = gta_animdir
		streamtext.text = gta_streamdir
				
		if (gta_tristrip as string) == "1" then (
			tristripcheck.checked = on
		) else (
			tristripcheck.checked = off
		)
	)
	
	-- EVENTS
	
	on buttonAdd pressed do (
		--try and find a non active configuration
		for i = 1 to gta_numconfigs do (
			if gta_cnfactive[i] == 0 then (
				gta_cnfactive[i] = 1
				gta_configs[i] = "New Config"
				updateSettings i
				WriteConfigGtaVariable gta_cnfactive[i] "active"
				configlist.selection = (GetListIndexFromActiveIndex i)
				
				platformradio.state = 1
				bintext.text = "\\"
				projectbintext.text = "\\"
				projectroottext.text = "\\"
				texturetext.text = "\\"
				lodtexturetext.text = "\\"
				projectanimtext.text = "\\"
				
				updatePlatformRadio (platformradio.state)

				tristripcheck.checked = true

				gta_bindir = bintext.text
				gta_project_bin = projectbintext.text
				gta_project_rootdir = projectroottext.text
				gta_animdir = texturetext.text
				gta_texturedir = lodtexturetext.text
				gta_lodtexturedir = projectanimtext.text
				gta_streamdir = streamtext.text

				WriteConfigGtaVariable gta_target "Target" 
--				WriteConfigGtaVariable gta_rwfilemaker "RwFilemaker" 
				WriteConfigGtaVariable gta_configs[gta_cnfcurrent] "Name"
				WriteConfigGtaVariable gta_bindir "Bin"
				WriteConfigGtaVariable gta_project_bin "ProjectBin"
				WriteConfigGtaVariable gta_project_rootdir "RootDir"
				WriteConfigGtaVariable gta_animdir "AnimDir"
				WriteConfigGtaVariable gta_texturedir "TextureDir"
				WriteConfigGtaVariable gta_lodtexturedir "LodTextureDir"
				WriteConfigGtaVariable gta_tristrip "TriStrip"
				WriteConfigGtaVariable gta_streamdir "StreamDir"
				WriteConfigGtaVariable gta_preinstance "Preinstance"

				gta_cnftextureloddir[gta_cnfcurrent] = gta_lodtexturedir
				WriteConfigGtaVariable gta_lodtexturedir "LodTextureDir"
				
				return 1
			)
		)
		
		-- if we get here then we need to add to the total number of configurations allowed
		gta_numconfigs = gta_numconfigs + 1
		WriteGtaVariable gta_numconfigs "settings" "NumConfigs"
		
		append gta_cnftristrip "1"
		append gta_cnfplatform 1
		append gta_cnfactive 1
		append gta_configs ""
		append gta_cnfbindir ""
		append gta_cnfprojbin ""
		append gta_cnfprojroot ""
		append gta_cnfanimdir ""
		append gta_cnftexturedir ""
		append gta_cnftextureloddir ""
		append gta_cnfstreamdir ""
	)
	on buttonRemove pressed do (
	
		if gta_cnfcurrent == 1 then (
			print "cant delete configuration 1"
			return 0
		)
	
		gta_cnfactive[gta_cnfcurrent] = 0
		WriteConfigGtaVariable gta_cnfactive[gta_cnfcurrent] "active"

		configlist.selection = 1
		updateSettings 1
	)
	on GtaSettingsRoll open do (
		gta_numconfigs = ReadGtaIntegerOveride gta_numconfigs "settings" "NumConfigs" 10
		
		for i = 5 to gta_numconfigs do (
			append gta_cnftristrip "1"
			append gta_cnfplatform 1
			append gta_cnfactive 0
			append gta_configs ""
			append gta_cnfbindir ""
			append gta_cnfprojbin ""
			append gta_cnfprojroot ""
			append gta_cnfanimdir ""
			append gta_cnftexturedir ""
			append gta_cnftextureloddir ""
			append gta_cnfstreamdir ""
		)

		for i = 1 to gta_numconfigs do (
			-- add items to the array up to the number of configs we are meant to have
			-- load in the active information for each configuration
			gta_cnfcurrent = i
			gta_cnfactive[gta_cnfcurrent] = ReadConfigGtaInteger "active" gta_cnfactive[gta_cnfcurrent]
		)
		
		gta_cnfcurrent = ReadGtaIntegerOveride gta_cnfcurrent "settings" "SetConfig" 1
		configlist.selection = gta_cnfcurrent
		iplist.selection = gta_preinstanceipidx
		updateSettings gta_cnfcurrent
		
		fillIpListBox()
	)
	on GtaSettingsRoll close do (
		gta_export_settings_rolledup = not GtaSettingsRoll.open
		WriteGtaVariable gta_export_settings_rolledup "settings" "settingsrollup"
	)
	on configlist selected value do (
		updateSettings (GetActiveIndexFromListIndex value)
	)
	on iplist selected value do (
		gta_preinstanceipidx = value
		WriteGtaVariable gta_preinstanceipidx "settings" "PreIP"
	)
	on showexportlog changed state do (
		gta_exportlog = showexportlog.checked
		WriteGtaVariable gta_exportlog "settings" "ExportLog"
	)

	on preinstancecheck changed state do (
		gta_preinstance = preinstancecheck.checked
		WriteConfigGtaVariable gta_preinstance "Preinstance"
	)

	on tristripcheck changed state do (
	
		if state == on then (
			gta_tristrip = "1"
		) else (
			gta_tristrip = "0"
		)
		
		gta_cnftristrip[gta_cnfcurrent] = gta_tristrip
		WriteConfigGtaVariable gta_tristrip "TriStrip"	
	)
	on platformradio changed value do (
		updatePlatformRadio value
		gta_cnfplatform[gta_cnfcurrent] = value
		WriteConfigGtaVariable gta_target "Target" 
--		WriteConfigGtaVariable gta_rwfilemaker "RwFilemaker" 
	)
	on confignametext changed text do(
		gta_configname = text
		gta_configs[gta_cnfcurrent] = text
		WriteConfigGtaVariable gta_configname "Name"
		fillListBox()
		configlist.selection = (GetListIndexFromActiveIndex gta_cnfcurrent)
	)
	on streamtext changed text do (
		gta_streamdir = text
		AddSlashAtEndOfFolderName gta_streamdir
		gta_cnfstreamdir[gta_cnfcurrent] = gta_streamdir
		WriteConfigGtaVariable gta_streamdir "StreamDir"
	)
	on bintext changed text do (
		gta_bindir = text
		AddSlashAtEndOfFolderName gta_bindir
		gta_cnfbindir[gta_cnfcurrent] = gta_bindir
		WriteConfigGtaVariable gta_bindir "Bin"
	)
	on projectbintext changed text do (
		gta_project_bin = text
		AddSlashAtEndOfFolderName gta_project_bin
		gta_cnfprojbin[gta_cnfcurrent] = gta_project_bin
		WriteConfigGtaVariable gta_project_bin "ProjectBin"
	)
	on projectroottext changed text do (
		gta_project_rootdir = text
		AddSlashAtEndOfFolderName gta_project_rootdir
		gta_cnfprojroot[gta_cnfcurrent] = gta_project_rootdir
		WriteConfigGtaVariable gta_project_rootdir "RootDir"
	)
	on projectanimtext changed text do (
		gta_animdir = text
		AddSlashAtEndOfFolderName gta_animdir
		gta_cnfanimdir[gta_cnfcurrent] = gta_animdir
		WriteConfigGtaVariable gta_animdir "AnimDir"
	)
	on texturetext changed text do (
		gta_texturedir = text
		AddSlashAtEndOfFolderName gta_texturedir
		gta_cnftexturedir[gta_cnfcurrent] = gta_texturedir
		WriteConfigGtaVariable gta_texturedir "TextureDir"
	)
	on lodtexturetext changed text do (
		gta_lodtexturedir = text
		AddSlashAtEndOfFolderName gta_lodtexturedir
		gta_cnftextureloddir[gta_cnfcurrent] = gta_lodtexturedir
		WriteConfigGtaVariable gta_lodtexturedir "LodTextureDir"
	)
)
