------------------------------------------------------------------------------
--
-- xml2.ms
-- MaxScript XML Reader/Writer
--
-- David Muir <david.muir@rockstarnorth.com>
-- Edit: Luke Openshaw
--
-- Uses .Net 2.0 framework (installed with Max)
--
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Globals
------------------------------------------------------------------------------

dotnet.LoadAssembly "System.Xml.dll"

------------------------------------------------------------------------------
-- Structure Definitions
------------------------------------------------------------------------------

struct XmlDocument
(

	------------------------------------------------------------------------------------
	-- PUBLIC MEMBER DATA
	------------------------------------------------------------------------------------

	--
	-- .Net System.Xml.XmlDocument Object
	--
	-- See http://msdn2.microsoft.com/en-us/library/system.xml.xmldocument.aspx for
	-- documentation on methods and properties for node manipulation.
	--

	document,

	------------------------------------------------------------------------------------
	-- CONSTANTS
	------------------------------------------------------------------------------------
	
	XmlNodeType = dotNetClass "System.Xml.XmlNodeType",
	
	------------------------------------------------------------------------------------
	-- PUBLIC METHODS
	------------------------------------------------------------------------------------
	
	-- call this before you start any I/O operations 
	function init force:false = 
	(	
		
		document = dotNetObject "System.Xml.XmlDocument" 
		
		local cpi = document.createProcessingInstruction ("xml") ("version=\"1.0\" encoding = \"Windows-1252\"")
		document.AppendChild cpi
	),
	
	-- loads an xml document into memory
	function load xmlDocName =
	(
		document.load xmlDocName
		true
	),
	
	-- save the in-memory xml document to file 
	function save xmlDocName =
	(
		document.save xmlDocName
		true
	),
	
	-- create an xmlnode
	function createnode type nodename namespace = 
	(

		xmlnode = document.createnode type nodename namespace
		xmlnode
	),
	
	function createTextNode thetext =
	(
		xmlnode = document.createTextNode thetext
		xmlnode
	),
	
	-- create an xmlelement
	function createelement elemname = 
	(
		
		xmlelem = document.createelement elemname
		xmlelem
	),
	
	-- create an xmlattribute
	function createattribute attrname attributeValue:undefined =
	(
		xmlattr = document.createattribute attrname
		if undefined!=attributeValue then
			xmlattr.value = attributeValue as string
		xmlattr
	),
	
	-- remove a node
	function removechild root node =
	(
		root.removechild node
	)
)

fn RsCreateXmlElement name elemattributes xmldoc = (
	projelem = xmldoc.createelement(name)
	for attribute in elemattributes do (
		attr = xmldoc.createattribute attribute.name
		attr.value = attribute.value as string
		projelem.Attributes.append(attr)
	)
	projelem
)

fn RsGetXmlElement elem elemsearch = (
	childelems = elem.childnodes
	for i = 0 to ( childelems.Count - 1 ) do (
		childelem = childelems.itemof(i)
		if childelem.name == elemsearch then return childelem
	)
	undefined
)

-- End of xml2.ms
