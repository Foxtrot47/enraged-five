--
-- File:: pipeline/util/scripttimer.ms
-- Description:: Timing Utility Functions
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 6 May 2009
--
-- Example Usage
--
-- t = ScriptTimer(); 
-- t.Start(); exportFile "C:\\export_test.xml"; t.End(); print( t.interval() );
--
-- t.Start(); << Code to Time >>; t.End()
--

dotNet.loadAssembly "System"

struct ScriptTimer
(
	dotNetDateTime = dotNetClass "System.DateTime",
	startTime,
	endTime,
	
	function start = (
	
		startTime = dotNetDateTime.Now
	),
	
	function end = (
	
		endTime = dotNetDateTime.Now
	),
	
	function interval = (
	
		-- DateTime ticks is in 100-nanosecond units.  We convert difference 
		-- into milliseconds which should be enough for us.
		
		diff = endTime.Subtract( startTime )		
		( ( diff.Ticks * 100 ) / 1000 / 1000 )
	)
)

-- pipeline/util/scripttimer.ms
