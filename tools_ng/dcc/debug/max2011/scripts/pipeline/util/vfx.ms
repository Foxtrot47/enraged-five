--
-- File:: pipeline/util/vfx.ms
-- Description:: VFX Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com
-- Date:: 19 March 2010
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/helpers/explosion_cat.ms"
filein "rockstar/helpers/particle_cat.ms"
filein "rockstar/helpers/VFXParticleConvert.ms"

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------

try CloseRolloutFloater VFXToolkit catch()
VFXToolkit = newRolloutFloater "VFX Toolkit" 200 700 50 126
addRollout RsExpSetupRoll VFXToolkit rolledup:false
addRollout RsPartSetupRoll VFXToolkit rolledup:false
addRollout RsVFXConversionWizRoll VFXToolkit rolledup:false
addRollout RsPartDBRoll VFXToolkit rolledup:false