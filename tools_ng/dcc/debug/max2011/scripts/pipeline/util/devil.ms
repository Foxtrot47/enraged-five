------------------------------------------------------------------------------
-- Globals
------------------------------------------------------------------------------
devlddlpath = pluginpaths.get (pluginpaths.count()) + "DevIL.NET.dll"
dotnet.LoadAssembly devlddlpath 

global devil = dotNetClass "DevIL.DevIL"
------------------------------------------------------------------------------
-- Structure Definitions
------------------------------------------------------------------------------


struct DevilManager
(
	------------------------------------------------------------------------------------
	-- CONSTANTS
	------------------------------------------------------------------------------------
	dfile,
	
		
	
	-- call this before you start any I/O operations 
	function init force:false = 
	(	
		
	),
	
	function loadbitmap filename = (
		bmpd = devil.LoadBitMap filename
		bmpd
	),
	
	function savebitmap outfilename bmpd = (
		devil.SaveBitMap outfilename bmpd
	),
	
	function newbitmap outfilename width height = (
		devil.NewBitMap outfilename width height
	),
	
	function blit src dest destx desty destz srcx srcy scrz width height depth = (
		devil.Blit src dest destx desty destz srcx srcy scrz width height depth
	)
)


