--
-- File:: pipeline/util/ReadIni.ms
--
-----------------------------------------------------------------------------

fn ReadGtaVariable folder name = (
	return getINISetting "gta.ini" folder name
)

fn ReadConfigGtaString name default= (
	strindex = gta_cnfcurrent as string
	section = "Config" + strindex

	var = ReadGtaVariable section name
	if var == "" do var = default
	return var	
)

fn ReadConfigGtaInteger name default= (
	strindex = gta_cnfcurrent as string
	section = "Config" + strindex
	
	svar = ReadGtaVariable section name
	if svar != "" then 
		var = (svar as integer)
	else
		var = default
		
	return var
)

fn ReadConfigGtaBoolean name default= (
	strindex = gta_cnfcurrent as string
	section = "Config" + strindex
	
	svar = ReadGtaVariable section name
	print svar
	if svar != "" then (
		if svar == "true" then 
			var = true
		else
			var = false	
	) else (
	
		if default == undefined then (
			var = false
		) else (
			var = default
		)
	)
		
	return var
)

fn ReadGtaStringOverride var folder name default = (
	var = ReadGtaVariable folder name
	if var == "" do var = default
	return var	
)

fn ReadGtaString var folder name default = (
	if var == undefined do var = ReadGtaVariable folder name
	if var == "" do var = default
	return var	
)

fn ReadGtaIntegerOveride var folder name default = (
	local svar
	
	svar = ReadGtaVariable folder name

	print svar

	if svar != "" then 
		var = (svar as integer)
	else
		var = default
	
	return var	
)

fn ReadGtaInteger var folder name default = (
	local svar
	
	if var == undefined do (
		svar = ReadGtaVariable folder name
		
		print svar
		
		if svar != "" then 
			var = (svar as integer)
		else
			var = default
	)
	
	return var	
)

fn ReadGtaBoolean var folder name default = (
	local svar
	
	if var == undefined do (
		svar = ReadGtaVariable folder name
		print svar
		if svar != "" then (
			if svar == "true" then 
				var = true
			else
				var = false	
		) else
			var = default
	)	
	return var	
)

-- function to write GTA settings variable to INI file
fn WriteGtaVariable var folder name = (
	setINISetting "gta.ini" folder name (var as string)
)

fn WriteConfigGtaVariable var name = (
	strindex = gta_cnfcurrent as string
	section = "Config" + strindex

	WriteGtaVariable var section name
)
