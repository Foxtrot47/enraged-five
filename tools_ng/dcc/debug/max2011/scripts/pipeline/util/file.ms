-- Rockstar File Utility
-- Rockstar North
-- 1/3/2005
-- by Greg Smith
-- by Luke Openshaw

-- Set of utility functions for dealing with files

include "pipeline/util/string.ms"

----------------------------------------------------------------------------------------
-- from a full path to a file return just the filename with no extension
----------------------------------------------------------------------------------------
fn RsRemovePathAndExtension path = (
	getFilenameFile path
)

----------------------------------------------------------------------------------------
-- 
----------------------------------------------------------------------------------------
fn RsRemoveExtension path = (

	local temp = filterstring path "."
	
	temp[1]
)


----------------------------------------------------------------------------------------
-- 
----------------------------------------------------------------------------------------
fn RsRemovePath path = (

	local temp = filterstring path "\\"
	filename = temp[temp.count]
	temp = filterstring filename "/"
	temp[temp.count]
)


----------------------------------------------------------------------------------------
-- find the last occurance of a character in a string
----------------------------------------------------------------------------------------
fn RsFindLast searched char = (

	retidx = -1

	for i = 1 to searched.count do (
	
		if retidx == -1 then (
	
			index = searched.count - (i - 1)

			if char == searched[index] then (
			
				retidx = index
			)
		)
	)
	
	retidx
)


----------------------------------------------------------------------------------------
-- 
----------------------------------------------------------------------------------------
fn RsRemoveFile path = (

	lastIndexA = RsFindLast path "\\"
	lastIndexB = RsFindLast path "/"
	
	if lastIndexB > lastIndexA then lastIndexA = lastIndexB
	
	retval = path
	
	if lastIndexA > 0 then (
	
		retval = substring path 1 lastIndexA
	)
	
	retval
)


--------------------------------------------------------------
-- makes sure that a passed in string is a valid path
--------------------------------------------------------------
fn RsMakeSafeSlashes pathin = (

	pathout = ""

	if classof pathin == String then (
	
		for i = 1 to pathin.count do (

			c = pathin[i]

			if c == "\\" then (

				pathout = pathout + "/"
			) else (

				pathout = pathout + c
			)
		)
	)
	
	pathout
)

--------------------------------------------------------------
-- makes sure that a passed in string is a valid path
--------------------------------------------------------------
fn RsMakeBackSlashes pathin = (

	pathout = ""

	if classof pathin == String then (
	
		for i = 1 to pathin.count do (

			c = pathin[i]

			if c == "/" then (

				pathout = pathout + "\\"
			) else (

				pathout = pathout + c
			)
		)
	)
	
	pathout
)

----------------------------------------------------------------------------------------
-- takes the filename of the path, leaving just the directory that the file is in
----------------------------------------------------------------------------------------
fn RsRemoveFilename fullpath = (

	fullpath = RsMakeSafeSlashes fullpath
	lastindex = RsFindLast fullpath "/"
	retval = ""
	
	if lastindex == -1 then (
	
		retval = fullpath
	) else (
	
		retval = (substring fullpath 1 lastindex)
	)
	
	retval
)

--------------------------------------------------------------
-- makes sure that a passed in string is a valid path
--------------------------------------------------------------
fn RsMakeSafePath pathin = (

	pathout = RsMakeSafeSlashes pathin
	
	if pathout[pathout.count] != "/" then (
	
		pathout = pathout + "/"
	)
	
	pathout
)

-------------------------------------------------------------------------------------------------
-- check that all the paths exist for the passed in file path
-------------------------------------------------------------------------------------------------
fn RsMakeSurePathExists filePath = 
(
	local dirPath = ""
	if ( "" == ( getFilenameType filePath ) ) then 
		dirPath = filePath 
	else 
		dirPath = (getFilenamePath filePath)
		
	makedir DirPath
)

-------------------------------------------------------------------------------------------------
-- delete a directory and any files in contains
-------------------------------------------------------------------------------------------------
fn RsDeleteDirectory dir = (

--	diruse = RsMakeBackSlashes dir
--	cmdline = ("rd /s /q " + diruse)
--	doscommand cmdline

	diruse = RsMakeSafeSlashes dir
	rexDeleteDirectory diruse
)

-------------------------------------------------------------------------------------------------
-- delete all the files of a certain type in a directory
-------------------------------------------------------------------------------------------------
fn RsDeleteFiles wildcard = (

	wildcard = RsMakeBackSlashes wildcard

	retval = true
	files = getfiles wildcard
	
	for file in files do (
	
		if retval == true then (
	
			if deletefile file == false then (

				retval = false
			)
		)
	)
	
	retval
)

-------------------------------------------------------------------------------------------------
-- copy all files by wildcard to a target folder
-------------------------------------------------------------------------------------------------
fn RsCopyFiles wildcard targetdir = (

	retval = true
	files = getfiles wildcard
	
	for file in files do (
	
		if retval == true then (
	
			targetfile = RsRemovePath file
	
			if copyfile file (targetdir + "/" + targetfile) == false then (

				retval = false
			)
		)
	)
	
	retval
)

-------------------------------------------------------------------------------------------------
-- returns true if a file exists and is readonly else false
-------------------------------------------------------------------------------------------------
fn RsIsFileReadOnly file = (

	if ( doesFileExist file ) then 
		( getfileattribute file #readOnly ) 
	else 
		false
)


-------------------------------------------------------------------------------------------------
-- name:		RsFindFilesRecursive
-- description:	Recursively find files matching wildcard (e.g. "*.ide") and return array of them
-------------------------------------------------------------------------------------------------
fn RsFindFilesRecursive rootpath wildcard = (

	foundFiles = #()

	dirs = GetDirectories ( rootpath + "/*" )
	for d in dirs do
		join dirs ( GetDirectories ( d + "/*" ) )
		
	for f in dirs do
		join foundFiles ( getFiles ( f + wildcard ) )
	
	-- Post process to have consistent file paths
	for i = 1 to foundFiles.count do
	(
		foundFiles[i] = RsMakeSafeSlashes foundFiles[i]
	)
	
	-- Return
	foundFiles
)

-------------------------------------------------------------------------------------------------
-- name:		RsFileExist
-- description:	Check if a file exists
-------------------------------------------------------------------------------------------------
fn RsFileExist filepath = 
(
	doesFileExist filepath
)

--
-- name: RsDirectoryWriteable
-- desc: Check that a directory exists and is writeable
--
fn RsDirectoryWriteable dir = (
	
	-- Because RsConfigGetNetworkDir() returns a path ending with a '/', this should check
	-- if the '/' is present and if so remove it.
	if (matchPattern dir pattern:"*/" == true) then (
	
		dir = substring dir 1 (dir.count - 1)
	)
	
	isDirectoryWriteable dir
)

--
-- name: RsFileModDate
-- desc: Return file modifier date/time as .Net DateTime object 
--       (for comparison)
--
fn RsFileModDate filename = (

	local fileClass = dotNetClass "System.IO.File"
	local md = fileClass.GetLastWriteTime( filename )
	md
)

--
-- name: RsFileCreateDate
-- desc: Return file creation date/time as .Net DateTime object 
--       (for comparison)
--
fn RsFileCreateDate filename = (

	local fileClass = dotNetClass "System.IO.File"
	local md = fileClass.GetCreationTime( filename )
	md
)

-- End of MAXScript
