--
-- File:: pipeline/util/Func.ms
-- Description:: List of functions used throughout GTA scripts
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- 29/10/09
-- Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Removed old and unused functions
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/GenSettings.ms"

-- MWW 29/10/09: Tools cleanup
-- filein "gta\\xml.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global GTAMapStreamDir = undefined
global GTAMapBankDir = undefined
global GTAMapBankName = undefined
global GtaMapGroupSuffix = ""

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
fn GTAlowercase instring = (
	local upper, lower, outstring
	upper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lower="abcdefghijklmnopqrstuvwxyz" 
	
	outstring=copy instring 
		
	for i=1 to outstring.count do (
		j = findString upper outstring[i] 
		
		if (j != undefined) do (
			outstring[i] = lower[j] 
		)
	) 
	outstring
)

fn GtaDoesMaterialHaveEnvMap mat = (

	if mat == undefined then (
		return false
	)
	
	if classof(mat) == Standardmaterial then (
		if mat.reflectionMap != undefined then (
			return true
		)
	)
	
	if classof(mat) == RwMaterial then (
		if mat.defmtl_texmap_envmap != undefined then (
			return true
		)
	)
	
	return false
)

fn GtaDoesObjectHaveEnvMap obj = (

	if obj == undefined then (
		return false
	)

	if classof obj == Editable_Mesh then (

		if obj.material == undefined then (
			return false
		)

		if (hasProperty obj.material "materialList") == true then (	
			for i = 1 to obj.material.materialList.count do (

				childmat = obj.material.materialList[i]
				childmatid = obj.material.materialIDList[i]

				if GtaDoesMaterialHaveEnvMap childmat == true then (
							
					-- look to see if this sub material is used by the geometry

					numFaces = getnumfaces obj

					for j = 1 to numFaces do (

						if classof obj == Editable_Mesh then (

							matID = getfacematid obj j
						) else if classof obj == Editable_Poly then (

							matID = polyop.getfacematid obj j
						)

						if matID == childmatid then (
							return true
						)
					)
				)		
			)
		) else (
			return GtaDoesMaterialHaveEnvMap obj.material
		)
	)
	
	for childobj in obj.children do (
	
		if GtaDoesObjectHaveEnvMap childobj == true then (
			return true
		)
	)
	
	return false
)

fn GtaColExportSettings isOptimised outputLog doShadow = (

	colSettingFilename = ((getdir #maxroot) + "/plugcfg") + "\\ColAnimExport.cfg"

	deletefile colSettingFilename

	if isOptimised == true or outputLog == true then (
		
		colSettingsFile = openfile colSettingFilename mode:"w+"
		
		if isOptimised == true then (
			format "OPTIMISECOL\n" to:colSettingsFile 
		)

		if outputLog == true then (
			format "OUTPUTLOG\n" to:colSettingsFile 
		)
		
		if doShadow == true then (
			format "DOSHADOW\n" to:colSettingsFile 
		)
		
		close colSettingsFile	
	)
)

fn GetTxdInDirectoryList txdlist txdnameonlylist = (

	gettxdexported = GTAMapStreamDir + "\\*.txd"

	temptxdlist = getfiles gettxdexported

	for exportedtxd in temptxdlist do (
		temp = filterstring exportedtxd "\\"
		exportedtxdname = temp[temp.count]
		exportedtxdname = (filterstring exportedtxdname ".")[1]

		append txdlist exportedtxd
		append txdnameonlylist (GTAlowercase exportedtxdname)
	)
)

fn GetDffInDirectoryList dfflist dffnameonlylist = (

	getdffexported = GTAMapStreamDir + "\\*.dff"

	tempdfflist = getfiles getdffexported	

	for exportdff in tempdfflist do (
		temp = filterstring exportdff "\\"
		exporteddffname = temp[temp.count]
		exporteddffname = GTAlowercase((filterstring exporteddffname ".")[1])

		append dfflist exportdff
		append dffnameonlylist exporteddffname
	)
)

-- MWW 29/10/09: Tools cleanup
/*
fn GetIsInterior = (

	if maxfilename == "" then (
		return false
	)

	GTAMapBankName = ((filterstring maxfilename ".")[1])
	testidename = GTAlowercase(GTAMapBankName + ".ide")

	gtarootelement = ParseXmlFile (((getdir #maxroot) + "/plugcfg") + "\\gta3.xml")
	gtarootelement = getxmlelement gtarootelement "gta"
	gtaelement = getxmlelement gtarootelement (GetProjectName())
	gtaelement = getxmlelement gtaelement "ide"

	for elems in gtaelement.entries do (
	
		testfilename = GTAlowercase(elems.entries[1])
		
		indexsub = findstring testfilename testidename
		
		if indexsub == undefined then (		
			continue
		)
		
		indexsub = findstring testfilename "\\interior\\"

		if indexsub != undefined then (		
			return true
		)
	)

	return false
)

	
fn GTASetMapFolders = (

	GTAMapStreamDir = undefined
	GTAMapBankDir = undefined
	GTAMapBankName = undefined
	GtaMapGroupSuffix = ""

	if maxfilename == "" then (
		return 0
	)

	-- Bank name

	GTAMapBankName = ((filterstring maxfilename ".")[1])

	-- Stream directory	
	
	GTAMapStreamDir = gta_streamdir + GTAMapBankName + "\\"

	-- Bank directory

	testidename = GTAlowercase(GTAMapBankName + ".ide")

	gtarootelement = ParseXmlFile (((getdir #maxroot) + "/plugcfg") + "\\gta3.xml")
	gtarootelement = getxmlelement gtarootelement "gta"
	gtaelement = getxmlelement gtarootelement (GetProjectName())
	gtaelement = getxmlelement gtaelement "ide"

	GTAMapBankDir = undefined 

	for elems in gtaelement.entries do (
	
		testfilename = GTAlowercase(elems.entries[1])
		
		indexsub = findstring testfilename testidename
		
		if indexsub != undefined then (
		
			testfilename = substring testfilename 1 (indexsub - 1)
		
			if findstring testfilename ":" == undefined then (
				GTAMapBankDir = gta_project_rootdir + testfilename
			) else (
				GTAMapBankDir = testfilename
			)
		
			found = true
		)
	)

	-- Group Suffix
	
	testmaxname = GTAlowercase(maxfilepath + maxfilename)
	
	gtaelement = getxmlelement gtarootelement (GetProjectName() + "_mapfiles")	
	gtaelement = getxmlelement gtaelement "objects"

	for elems in gtaelement.entries do (
	
		testfilename = GTAlowercase(elems.entries[1])
		
		if testmaxname == testfilename then (
				
			for paramentry in elems.params do (
		
				if findstring paramentry "suffix=" != undefined then (
					GtaMapGroupSuffix = substring paramentry 9 (paramentry.count - 9)
				)
			)
		)
	)
	
	gtaelement = getxmlelement gtarootelement (GetProjectName() + "_generic")
	gtaelement = getxmlelement gtaelement "objects"
	
	for elems in gtaelement.entries do (
	
		testfilename = GTAlowercase(elems.entries[1])
		
		if testmaxname == testfilename then (
				
			for paramentry in elems.params do (
		
				if findstring paramentry "suffix=" != undefined then (
					GtaMapGroupSuffix = GTAlowercase(substring paramentry 9 (paramentry.count - 9))
				)
			)
		)
	)
)
*/

fn GTASaveTXDStats = (
	
	GTASetMapFolders()
	
	if GTAMapBankDir == undefined then (
		
		return 0
	)
	
	statFilename = getweek() as string
	
	if statFilename.count < 2 then (
		statFilename = "0" + (getweek() as string)
	)
	
	statFilename = GTAMapBankName + "_" + (getyear() as string) + statFilename + ".txt"
	
	makedir("n:\\mapstats\\")
	makedir("n:\\mapstats\\" + GTAMapBankName + "\\")
	outputtexstats("n:\\mapstats\\" + GTAMapBankName + "\\" + statFilename) gta_streamdir
)

fn GTASaveSceneStats = (
	--export out our scene statistics
	
	GTASetMapFolders()
	
	if GTAMapBankDir == undefined then (
	
		--this isnt a map file
	
		return 0
	)
	
	statFilename = getweek() as string
	
	if statFilename.count < 2 then (
		statFilename = "0" + (getweek() as string)
	)
	
	statFilename = GTAMapBankName + "_" + (getyear() as string) + statFilename + ".csv"
	
	statFile = openfile ("n:\\mapstats\\" + GTAMapBankName + "\\" + statFilename) mode:"r"
	
	if statFile != undefined then (
		close statFile
		return 0
	)
	
	makedir("n:\\mapstats\\")
	makedir("n:\\mapstats\\" + GTAMapBankName + "\\")
	outputscenestats("n:\\mapstats\\" + GTAMapBankName + "\\" + statFilename) gta_streamdir
)

--
-- Work out where the GTA level of detail extension starts in a model name
-- ie it is the last "_L" in the string. It should also have a number after it
--
fn GtaGetExtStartOld name = (
	local end = findstring name "_L"
	local next
	local size = name.count
	local numbers = "0123456789"
	
	-- if can't find "_L" extension return undefined
	if end == undefined do return end
	
	do (
		next = findstring (substring name (end+1) size) "_L"
		if next != undefined do end = end + next
	) while next != undefined

	--make sure every character after _l is 0
	--else dont treat this as a lod name
	
	size = size - end - 1
	if size == 0 do return undefined
	
	for i = 1 to size do (
		intbit = (substring name (end + 2 + i - 1) 1)

		j = findstring numbers intbit[1]
		
		if j == undefined do (
			return undefined
		)
	)

	return end
)

fn GtaGetExtStart name = (
	local end = findstring name "_L"
	local next
	local size = name.count
	local numbers = "0"
	
	-- if can't find "_L" extension return undefined
	if end == undefined do return end
	
	do (
		next = findstring (substring name (end+1) size) "_L"
		if next != undefined do end = end + next
	) while next != undefined

	--make sure every character after _l is 0
	--else dont treat this as a lod name
	
	size = size - end - 1
	if size == 0 do return undefined
	
	for i = 1 to size do (
		intbit = (substring name (end + 2 + i - 1) 1)

		j = findstring numbers intbit[1]
		
		if j == undefined do (
			return undefined
		)
	)

	return end
)

--
-- Get the GTA base name from a model name. eg pylon_L01 has base name pylon
--
fn GtaGetName name = (
	
	retname = name

	local ext = GtaGetExtStart name
	
	if ext != undefined then
		retname = (substring name 1 (ext-1))
		
	-- now take _dam off the extension if it exists
	
	if retname.count > 4 then (
		if (substring retname (retname.count - 3) -1) == "_dam" then (
			retname = substring retname 1 (retname.count - 4)
		)
	)
	
	return GTAlowercase(retname)
)

--
-- Get the GTA level of detail number from a model name
--
fn GtaGetLod name = (
	local ext = GtaGetExtStart name
	if ext == undefined then
		return 0
	else
		return (substring name (ext+2) name.count) as integer
)

--
-- Find all the objects with a specified basename ie all the objects of one type
--
-- ignore the _dam at the end so that these objects will get grouped together

fn GtaFindObjectsNamed name list = (
	for obj in $objects do
	(
		checkname = GtaGetName obj.name
		
		if (checkname == name and (classof obj) != XRefObject) do (
			append list obj
		)
	)		
)

fn GtaFindObjectsContainingNameFromEverything name list = (
	for obj in rootNode.children do
	(
		checkname = GtaGetName obj.name
	
		if (findstring checkname name != undefined) do (
			append list obj
		)
	)		
)

fn GtaFindObjectsNamedFromEverything name list = (
	for obj in rootNode.children do
	(
		checkname = GtaGetName obj.name
	
		if (checkname == name) do (
			append list obj
		)
	)		
)

--
-- Find mesh bone influences
--
fn FindSkinnedMesh bone = (
	-- find Skin modifier in dependents list
	for obj in (refs.dependents bone) do (
		if (classof obj) == Skin do (
			-- find editable mesh in skin modifiers dependents list
			for obj2 in (refs.dependents obj) do (
				if (classof obj2) == Editable_mesh do (
					return obj2;
				)
			)
		)
	)
	return undefined
)

fn GtaMakeGenCDImage inputfolder outputimage = (

	-- any cdimage building should finally call this function
	-- with a directory to build the image from and a output cdimage name
	-- that way it is easy to switch them between being preinstanced and not

	if inputfolder == undefined or outputimage == undefined then (
		return false
	)
	
	--make sure the image is deleted
	deletefile outputimage
	
	inputfolder = GTAlowercase inputfolder
	outputimage = GTAlowercase outputimage
	
	if gta_preinstance == true and (findstring outputimage "player" == undefined) then (
	
		-- this will be our temporary work directory
		preinstdir = gta_bindir + "preinst\\"

		--make sure it exists
		makedir preinstdir

		--make sure it doesnt have any files in it that will end up in a cdimage
		fileCount = (getfiles (preinstdir + "*.dff")).count
		fileCount += (getfiles (preinstdir + "*.ifp")).count
		fileCount += (getfiles (preinstdir + "*.txd")).count
		fileCount += (getfiles (preinstdir + "*.col")).count
		fileCount += (getfiles (preinstdir + "*.ipl")).count

		if fileCount > 0 then (
			runcmd = "del " + preinstdir + "*.dff " + preinstdir + "*.ifp " + preinstdir + "*.txd " + preinstdir + "*.col " + preinstdir + "*.ipl /q"
			rt = doscommand runcmd
			if rt != 0 then (
				messagebox "failed to delete files from preinstance directory" title:"makecdimage"
				return false
			)
		)
		
		--copy the files we want to work with the the work directory
		runcmd = "copy " + inputfolder + "*.* " + preinstdir + "*.*"
		rt = doscommand runcmd
		if rt != 0 then (
			messagebox "failed to copy files to preinstance directory" title:"makecdimage"
			return false
		)
		
		-- create a rwfilemaker script for preinstancing
		runcmd = gta_bindir + "rwsmaker " + gta_bindir + "preinst\\template.txt " + preinstdir + "preinst.rws " + preinstdir + "*.dff"
		rt = doscommand runcmd
		if rt != 0 then (
			messagebox "failed to create rwfilemaker script" title:"makecdimage"
			return false
		)
		
		for devkit in GtaPossibleDevKits do (
		
			-- run the preinstance, keep trying with each possible ip until there is success	
			--runcmd = "\"c:\\program files\\metrowerks\\codewarrior\\bin\\cwcomutil\" -quit -tg ps2t10kt -d " + GtaPossibleDevKits[gta_preinstanceipidx] + " " + gta_bindir + "rwfilemaker.elf " + preinstdir + "preinst.rws"
			--runcmd = "x:\\sa\\gta_bin\\ps2conn -q -i " + devkit + " " + gta_bindir + "rwfilemaker.elf " + preinstdir + "preinst.rws"
			runcmd = "x:\\sa\\gta_bin\\winps2conn -q -i " + devkit + " " + gta_bindir + "rwfilemaker.elf " + preinstdir + "preinst.rws"
			rt = doscommand runcmd
	
			if rt != -1 then (
				exit
			)
		)
		
		if rt != 0 then (
			messagebox "couldnt preinstance the data" title:"makecdimage"
--			return false
		)
		
		-- build the image
		runcmd = gta_bindir + "cdimagemaker " + outputimage + " "
		runcmd += preinstdir + "*.dff "
		runcmd += preinstdir + "*.ifp "
		runcmd += preinstdir + "*.txd "
		runcmd += preinstdir + "*.col "
		runcmd += preinstdir + "*.ipl "
		rt = doscommand runcmd
		if rt != 0 then (
			messagebox "failed to make the cd image" title:"makecdimage"
			return false
		)
		
	) else (

		-- just build the image from the stream files

		local command = gta_bindir + "cdimagemaker " + outputimage + " "
		command += inputfolder + "*.dff "
		command += inputfolder + "*.ifp "
		command += inputfolder + "*.txd "
		command += inputfolder + "*.col "
		command += inputfolder + "*.ipl "
		rt = doscommand command

		if rt != 0 then (
			messagebox "failed to make the cd image" title:"makecdimage"
			return false
		)
	)
	
	return true
)

-- make cdimage for one bank
fn GtaMakeMapCdImage = (
	return GtaMakeGenCDImage GTAMapStreamDir (GTAMapBankDir + GTAMapBankName)
)

-- run makefinalcd script
fn MakeFinalCd prompt:true = (
	if (prompt == false) or (querybox "Do you want to build the game" title:"what'll I do?" == true) do (
	
		rt = Doscommand (gta_project_bin + "makefinalcd.bat " + gta_project_bin)
		if rt != 0 do (
			messagebox "Failed to build the game" title:"GTA"
			return false
		)		
	)
)

-- MWW 29/10/09: Tools cleanup
/*
fn MaxFileIsInGenericSectionRecursive  xmlel = (

	if classof xmlfile != xmlelement then (
		return false
	)

	if xmlel.name == "sa_ps2_generic" then (
		for maxxmlel in xmlel.entries[1].entries do (
			if GTAlowercase(maxxmlel.name) == GTAlowercase(maxFilePath + maxFilename) then (
				return true
			)
		)
	)

	for xmlelchild in xmlel.entries do (
		retval = MaxFileIsInGenericSectionRecursive xmlelchild
		
		if retval == true then (
			return true
		)
	)
	
	return false
)

fn MaxFileIsInGenericSection = (
	xmlfile = ParseXmlFile "C:\\3dsmax5\\plugcfg\\gta3.xml"
	
	return (MaxFileIsInGenericSectionRecursive xmlfile)
)

-- Get array of IDEs from plugcfg/GTA3.xml file
fn GetIDEArray = (
	folders = (filterstring  gta_project_rootdir "\\" )
	baseidegroupname = folders[folders.count]
	local filename = ((getdir #maxroot) + "/plugcfg") + "\\gta3.xml"
	local rootelement = ParseXmlFile filename
	local gtaelement = GetXmlElement rootelement "gta"
	local projelement = GetXmlElement gtaelement (GetProjectName())
	local ideelement = GetXmlElement projelement "ide"
	
	idearray = #()
	
	for element in ideelement.entries do (
		append idearray (GetXmlEntry element)
	)
	return idearray
)
*/

--the same as GetTxdListAppend but reads from an ide file
fn GetTxdListFromIdeAppend ideFile txdList txdappend = (
	try readFile = openFile ideFile mode:"rt"
	catch return false
	
	while (eof readFile == false) do
	(
		line = readLine readFile
		
		-- up until the first end will be the obj list
		if line == "end" then (
			return true
		)
		
		-- all lines up until the end should be correct
		-- but just to be safe
		if line.count > 7 then (
			tokens = filterString line " ,"
			
			if tokens[3].count > 1 then (
				--print tokens[3]
				
				if (findItem txdList ((tokens[3]) + txdappend)) == 0 then (
					append txdList ((tokens[3]) + txdappend)
				)
			)
		)
	)
	
	closeFile readFile
	
	return true
)

fn RwFMRunScript rwsfilename = (
--	docmd = "\"" + gta_bindir + gta_rwfilemaker + "\" " + rwsfilename + " " + (GetPlatformString())
--	docmd = "\"" + gta_bindir + gta_rwfilemaker + "\" " + rwsfilename + " rwfmpc_37000.dll")
	docmd = "\"" + gta_bindir + gta_rwfilemaker + "\" " + rwsfilename + " rwfmpc_37000.dll"
	doscommand docmd
)