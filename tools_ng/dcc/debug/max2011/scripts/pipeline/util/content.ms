--
-- File:: pipeline/util/content.ms
-- Description:: Helper functions for querying the content tree.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 24 March 2010
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
-- None

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsContentGetLevels
-- desc: Return an array of level names for the current project.
--
fn RsContentGetLevels = (

	RsProjectContentReload()
	local levels = RsProjectContentFindAll type:"level"
	local levelNames = #()
	
	for level in levels do
	(
		append levelNames level.name
	)
		
	levelNames
)

--
-- name: RsContentGetLevel
-- desc: Return ContentLevel node from a ContentMapRpf node.
--
fn RsContentGetLevel mapRpfNode = (

	RsAssert (ContentMapRpf == classof mapRpfNode) message:"Invalid ContentMapRpf node specified."
	local parent = mapRpfNode.parent
	while ( ( ContentLevel != classof parent ) and ( undefined != parent ) ) do
	(
		parent = parent.parent
	)
	parent
)

--
-- name: RsContentGetLevelMaps
-- desc: Return array of ContentMapRpf nodes 
--
fn RsContentGetLevelMaps levelname genericOnly:false = (
	
	RsProjectContentReload()
	local levelNode = RsProjectContentFind levelname type:"level"
	local mapRpfNodes = RsProjectContentFindAll type:"maprpf"
	local maps = #()
	if ( undefined != levelNode and mapRpfNodes.count > 0 ) then
	(
		for i = 1 to mapRpfNodes.count do
		(
			local mapRpfNode = mapRpfNodes[i]
			local mapRpfLevel = ( RsContentGetLevel mapRpfNode )
			if ( undefined != mapRpfLevel and mapRpfLevel.name == levelNode.name ) then
			(
				if ( genericOnly ) then
				(
					if ( ( mapRpfNode.inputs.count > 0 ) and ( not mapRpfNode.inputs[1].instances ) ) then
						append maps mapRpfNode
				)
				else
				(
					append maps mapRpfNode
				)
			)
		)
	)
	maps
)


-- pipeline/util/content.ms
