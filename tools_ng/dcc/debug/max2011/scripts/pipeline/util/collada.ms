--
-- File:: pipeline/util/collada.ms
-- Description:: Base collada functions (not-exporter specific).
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 6 May 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
-- None

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
-- None

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- utility: RsColladaStruct
-- desc: Basic COLLADA export utility functions and constants.
--
-- GLOBAL INSTANCE: RsCollada
--
struct RsColladaStruct
(
	ext = ".DAE",
	ext_zip = ".ZIP",
	
	--
	-- name: ExportObjectsSeparately
	-- desc: Export an array of objects into a directory as separate
	--         Collada (DAE) files.
	--
	-- notes: We support Array elements within the objs Array to support the
	--        export of dynamic collision with the dynamic object's mesh.  They
	--        will be exported to a single Collada file.
	--
	fn ExportObjectsSeparately objs directory zipped:false = 
	(
		RsMakeSurePathExists directory
		local selSet = selection
		clearSelection()
		
		local numObjs = ( objs.count )
		local idxObj = 1
		
		progressStart "Collada Export"
		for obj in objs do
		(
			select obj
			
			local objFilename = ""
			local objName = ""

			if ( Array == ( classof obj ) ) then
			(
				objName = obj[1].name
				objFilename = ( directory + objName )
				if ( zipped ) then
					objFilename = objFilename + ext_zip
				else
					objFilename = objFilename + ext
					
				format "Exporting object: % (% objects) to Collada file: %\n" objName obj.count objFilename
			)
			else
			(
				objName = obj.name
				objFilename = ( directory + obj.name )
				if ( zipped ) then
					objFilename = objFilename + ext_zip
				else
					objFilename = objFilename + ext
					
				format "Exporting object: % to Collada file: %\n" objName objFilename
			)
			exportFile objFilename #noPrompt selectedOnly:true using:ColladaExporter
			progressUpdate ( ( idxObj / numObjs ) * 100.0 )
			if getProgressCancel() then throw("progress_cancel")
			
			idxObj = ( idxObj + 1 )
			flushLog()
		)
		progressEnd()
		
		select selSet
		true
	),
	
	--
	-- name: ExportObjectsTogether
	-- desc: Export an array of objects into a single Collada (DAE) file.
	--
	fn ExportObjectsTogether objs filename =
	(
		RsMakeSurePathExists filename
		local selSet = selection
		clearSelection()

		format "RsCollada.ExportObjectsTogether:\n"
		for obj in objs do
		(
			selectMore obj
			format "\t%\n" obj.name
		)
		exportFile filename #noPrompt selectedOnly:true using:ColladaExporter

		select selSet
		true
	)
	
) -- RsColladaStruct struct

-----------------------------------------------------------------------------
-- Struct Global Instances
-----------------------------------------------------------------------------
global RsCollada = RsColladaStruct()

-- pipeline/util/collada.ms
