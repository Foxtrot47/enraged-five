--
-- File:: pipeline/util/IdeUtils.ms
-- Description:: Allows loading and editing of ide/ipl files in script
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com>
-- Date:: 20/4/2004
--
-----------------------------------------------------------------------------
-- ChangeLog
--
-- 26/06/2007 -- DHM -- GTA4 compatibility update
-- 		Updated IDE loading to "GtaLoadIDEObjects" function
-- 		Now correctly parses *some* of the IDE files.
--		Added GtaLoadIDEFiles function and GTA_MapsRoot global path string
--		IPL data loading not touched
--		
-------------------------------------------------------------------------------------------			 

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/Func.ms"
filein "pipeline\\util\\string.ms"
filein "pipeline\\util\\file.ms"

-------------------------------------------------------------------------------------------
-- Structure Definitions
-------------------------------------------------------------------------------------------

struct ideobjectclass (

	---------------------------------------------------------------------------------------
	-- Constants
	---------------------------------------------------------------------------------------
	FLAG_LIGHTREFLECTION 		= 0x00000001,
	FLAG_DONTFADE				= 0x00000002,
	FLAG_DRAWLAST				= 0x00000004,
	FLAG_INSTANCED				= 0x00000008,
	--FLAG_RESERVED1			= 0x00000010,
	FLAG_ISFIXED				= 0x00000020,
	FLAG_NOZBUFFERWRITE			= 0x00000040,
	FLAG_NOSHADOWS				= 0x00000080,
	FLAG_ISGENERIC				= 0x00000100,
	FLAG_HASANIM				= 0x00000200,
	FLAG_HASUVANIM				= 0x00000400,
	FLAG_SHADOWONLY				= 0x00000800,
	FLAG_ISDAMAGEABLE			= 0x00001000,
	FLAG_DONTCASTSHADOWS		= 0x00002000,
	FLAG_CASTTEXSHADOWS			= 0x00004000,
	FLAG_NOFLYERCOLLIDE			= 0x00008000,
	--FLAG_RESERVED2			= 0x00010000,
	FLAG_ISDYNAMIC				= 0x00020000,
	--FLAG_RESERVED3			= 0x00040000,
	--FLAG_RESEVRED4			= 0x00080000,
	--FLAG_RESERVED5			= 0x00100000,
	FLAG_NOBACKFACECULLING		= 0x00200000,
	FLAG_DOESNOTPROVIDECOVER	= 0x00400000,
	--FLAG_RESERVED6			= 0x00800000,
	FLAG_ISLADDER				= 0x01000000,
	FLAG_ISFIREESCAPE			= 0x02000000,
	FLAG_HASDOORPHYSICS			= 0x04000000,
	FLAG_ISFIXEDFORNAV			= 0x08000000,
	FLAG_NOPEDCOLLISION			= 0x10000000,
	FLAG_AMBIENTMULTPLIER		= 0x20000000,

	---------------------------------------------------------------------------------------
	-- Properties
	---------------------------------------------------------------------------------------
	
	name,		-- Object Name (string)
	txd, 		-- TXD Name (string)
	loddist, 	-- LOD Distance (float)
	flags,		-- Flags (bitfield)
	
	---------------------------------------------------------------------------------------
	-- Methods
	---------------------------------------------------------------------------------------

	-- Flag check status
	function hasFlag flag = (
	
		return ( ( bit.and flags flag ) == flag )
	),

	-- Compare two ideObjectClass objects
	function cmp o1 o2 = (
		if ( classof o1 != ideObjectClass ) then
			throw "o1 must be of type ideObjectClass"
		if ( classof o2 != ideObjectClass ) then
			throw "o2 must be of type ideObjectClass"
		
		if ( o1.name == o2.name ) then
			return 0
		else if ( o1.name < o2.name ) then
			return -1
		else
			return 1
	)
)

struct iplobjectinst (id, name, areacode, px, py, pz, rx, ry, rz, rw, lodparent)

-------------------------------------------------------------------------------------------
-- Global Data
-------------------------------------------------------------------------------------------

global GTA_MapsRoot = "x:/gta/build/independent/data/maps/"


-------------------------------------------------------------------------------------------
-- name:		GtaLoadIDEObjects
-- description:	Load an IDE file adding object definitions to the loadList,
--				** Note not all objects nor data is parsed **
--				
-- Reference:	FileLoader.cpp : CFileLoader::LoadObject(const char* pLine)
--
-------------------------------------------------------------------------------------------

fn GtaLoadIDEObjects filename loadList = (
	
	ideFile = openfile filename mode:"rt"
	
	ideLine = readLine ideFile
	
	-- find the beginning of the objects
	
	while(eof ideFile == false) do (
		
		if ideLine == "objs" then (
			exit
		)
		
		ideLine = readLine ideFile	
	)
	
	ideLine = readLine ideFile	
	
	-- read the objects in

 	while(eof ideFile == false) do (
		
		if ideLine == "end" then (
			exit
		)
		
		ideTokens = filterstring ideLine ", "
		
		objName = RsLowercase(ideTokens[1])
		objTXDName = RsLowercase(ideTokens[2])
		objLODDist = ideTokens[3] as float
		objFlags = ideTokens[4] as integer
		
		newObjClass = ideobjectclass name:objName txd:objTXDName loddist:objLODDist flags:objFlags
		
		append loadList newObjClass
		
		ideLine = readLine ideFile	
	)
	
	-- time objects
	
	while(eof ideFile == false) do (
		
		if ideLine == "tobj" then (
			exit
		)
		
		ideLine = readLine ideFile	
	)
	
	ideLine = readLine ideFile	
	
	-- read the objects in

 	while(eof ideFile == false) do (
		
		if ideLine == "end" then (
			exit
		)
		
		ideTokens = filterstring ideLine ", "
		
		objName = RsLowercase(ideTokens[1])
		objTXDName = RsLowercase(ideTokens[2])
		objLODDist = ideTokens[3] as float
		objFlags = ideTokens[4] as integer
		
		newObjClass = ideobjectclass name:objName txd:objTXDName loddist:objLODDist flags:objFlags
		
		append loadList newObjClass
		
		ideLine = readLine ideFile	
	)
	
	-- anim objects
	
	while(eof ideFile == false) do (
		
		if ideLine == "anim" then (
			exit
		)
		
		ideLine = readLine ideFile	
	)
	
	ideLine = readLine ideFile	
	
	-- read the objects in

 	while(eof ideFile == false) do (
		
		if ideLine == "end" then (
			exit
		)
		
		ideTokens = filterstring ideLine ", "
		
		objName = RsLowercase(ideTokens[1])
		objTXDName = RsLowercase(ideTokens[2])
		-- Anim file name
		objLODDist = ideTokens[4] as float
		objFlags = ideTokens[5] as integer
		
		newObjClass = ideobjectclass name:objName txd:objTXDName loddist:objLODDist flags:objFlags
		
		append loadList newObjClass
		
		ideLine = readLine ideFile	
	)
	
	close ideFile
)

-------------------------------------------------------------------------------------------
-- name:		GtaLoadIDEFiles
-- description:	Load an IDE file adding object definitions to the ideObjectList
--				Both object class name and TXD name are added
--
--				IMPORTANT: ideObjectList returned is SORTED.  This is required as it speeds
--						   up GtaFindIDEObject considerably.  Peace out.
-------------------------------------------------------------------------------------------
fn GtaLoadIDEFiles rootpath &ideFileList &ideObjectList = (

	ideObjectList = #()
	ideFileList = #()
	ideFileList = RsFindFilesRecursive rootpath "*.ide"

	for f in ideFileList do
	(
		GtaLoadIDEObjects f ideObjectList
	)
	
	sort ideFileList
	qsort ideObjectList ideObjectClass.cmp
)

-------------------------------------------------------------------------------------------
-- name:		GtaFindIDEObject
-- description:	Find and return object definition based on name, given ideObjectList as
--				filled in by GtaLoadIDEFiles / GtaLoadIDEObjects
--
--				IMPORTANT: ideObjectList must be SORTED.  GtaLoadIDEFiles does this :P
--						   so use it.
-------------------------------------------------------------------------------------------
fn GtaFindIDEObject ideObjectList objName = (

	local low = 1
	local high = ideObjectList.count
	
	while ( low <= high ) do
	(
		mid = (low+high) / 2
		
		if ( ideObjectList[mid].name > RsLowercase(objName) ) then
			high = mid - 1
		else if ( ideObjectList[mid].name < RsLowercase(objName) ) then
			low = mid + 1
		else
			return ( ideObjectList[mid] )
	)
	return undefined
)

-------------------------------------------------------------------------------------------
-- name:		GtaFindIDEObjectL
-- description:	Find and return object definition based on name, given ideObjectList as
--				filled in by GtaLoadIDEFiles / GtaLoadIDEObjects
--
--				IMPORTANT: ideObjectList does not need to be sorted.  This is a linear
--						   search and is much slower than GtaFindIDEObject above.
-------------------------------------------------------------------------------------------
fn GtaFindIDEObjectL ideObjectList objName = (

	local objnamel = ( RsLowercase objName )

	for o in ideObjectList do
	(
		if ( objnamel == o.name ) then
			return ( o )
	)
	
	-- Failed to find object definition, return undefined
	undefined
)


-------------------------------------------------------------------------------------------


fn GtaLoadIPLObjectsNameOnly filename loadList = (
	
	iplFile = openfile filename mode:"rt"
	
	iplLine = readLine iplFile
	
	-- find the beginning of the objects
	
	while(eof iplFile == false) do (
		
		if iplLine == "inst" then (
		
			exit
		)
		
		iplLine = readLine iplFile	
	)
	
	-- read the objects in

 	while(eof iplFile == false) do (
		
		if iplLine == "end" then (
			exit
		)
		
		iplTokens = filterstring iplLine ", "
		
		append loadList iplTokens[1]
		
		iplLine = readLine iplFile	
	)
	
	close iplFile
	
--	print loadList.count
--	for iplentry in loadList do (
--		print iplentry
--	)
)

fn GtaIsIDEListUsedInIPL ideEntries iplfilename = (

	iplEntries = #()

	GtaLoadIPLObjectsNameOnly iplfilename iplEntries 
	
	for ideEntry in ideEntries do (
	
		if finditem iplEntries ideEntry != 0 then (		
			return true
		)
	)
	
	return false
)

fn GtaFindIDEUsed idefilename = (
	
	getiplexported = gta_streamdir + "\\*.ipl"

	iplfilenamelist = getfiles getiplexported

	ideEntries = #()

	GtaLoadIDEObjectsNameOnly idefilename ideEntries

	for iplfilename in iplfilenamelist do (
		if GtaIsIDEListUsedInIPL ideEntries iplfilename == true then (
			print (iplfilename + " uses " + idefilename)
		)
	)
)

fn GtaFindObjectInIDENotUsed idefilename = (

	getiplexported = gta_streamdir + "\\*.ipl"

	iplfilenamelist = getfiles getiplexported
	
	ideEntries = #()
	
	GtaLoadIDEObjectsJustObjectClasses idefilename ideEntries
	
	originalcount = ideEntries.count

	for iplfilename in iplfilenamelist do (
	
		iplEntries = #()
	
		GtaLoadIPLObjectsNameOnly iplfilename iplEntries 
	
		i = 1

		while(i <= ideEntries.count) do (
		
			ideEntry = (ideEntries[i].id as string)
		
			if finditem iplEntries ideEntry != 0 then (						
			
				deleteitem ideEntries i
			) else (
				i = i + 1
			)
		)
	)
	
	if ideEntries.count > 0 then (
	
		print ((ideEntries.count as string) + " out of " + (originalcount as string) + " ide entries are not used")
	
		for ideEntry in ideEntries do (
			print (ideEntry.name + " (" + (ideEntry.id as string) + ")")
		)
	) else (
		print "all ide entries are used"
	)
)

fn GtaSyncIPLFiletoIDEArray iplfilename idearray = (

	tempfilename = "c:\\temp.ipl"

	iplNewFile = openfile tempfilename mode:"w+"
	
	if iplNewFile == undefined then (
		messagebox "couldnt open temporary file"
		return false
	)
	
	iplFile = openfile iplfilename mode:"rt"
	
	iplLine = readLine iplFile
	
	-- find the beginning of the object instances
	while(eof iplFile == false) do (
		
		if iplLine == "inst" then (
			exit
		)
		
		iplLine = readLine iplFile	
	)
	
	format "inst\n" to:iplNewFile
	
	-- go through each object instance
	iplLine = readLine iplFile
	
 	while(eof iplFile == false) do (
		
		if iplLine == "end" then (
			exit
		)
		
		ideTokens = filterstring iplLine ", "
		
		objId = ideTokens[1] as integer
		objName = GTAlowercase(ideTokens[2])
		objAreacode = ideTokens[3] as integer
		objPx = ideTokens[4]
		objPy = ideTokens[5]
		objPz = ideTokens[6]
		objRx = ideTokens[7]
		objRy = ideTokens[8]
		objRz = ideTokens[9]
		objRw = ideTokens[10]
		objLodparent = ideTokens[11] as integer
				
		for ideobj in idearray do (
			if ideobj.name == objName then (
				objId = ideobj.id			
				exit
			)
		)
		
		objName = GTAlowercase(ideTokens[2])
		
		format "%, %, %, " objId objName objAreacode to:iplNewFile
		format "%, %, %, " objPx objPy objPz to:iplNewFile
		format "%, %, %, %, " objRx objRy objRz objRw to:iplNewFile
		format "%\n" objLodparent to:iplNewFile
		
		iplLine = readLine iplFile	
	)
	
	format "end\n" to:iplNewFile
	
	--write out the rest of the file
 	while(eof iplFile == false) do (
		
		try	(
			iplLine = readLine iplFile
		)
		catch (
			exit
		)
		
		if iplLine == undefined then (
			exit
		)
		
		if iplLine.count == undefined then (
			exit
		)
				
		format "%\n" iplLine to:iplNewFile
	)
	
	close iplFile
	close iplNewFile 
	
	deletefile iplfilename
	retval = copyfile tempfilename iplfilename 
	
	if retval == false then (
		print ("couldnt copy over " + iplfilename)
	)
	
	deletefile tempfilename
	
	return retval
)

fn GtaSyncIPLFiletoIDE iplfilename idefilename = (
	ideObjClasses = #()
	GtaLoadIDEObjectsJustObjectClasses idefilename ideObjClasses
	GtaSyncIPLFiletoIDEArray iplfilename ideObjClasses 
)

fn GtaSyncIPLDirectoryToIDE idefilename iplpath = (
	
	iplpath = iplpath + "*.ipl"
	iplfilenames = getfiles iplpath
	
	ideObjClasses = #()
	GtaLoadIDEObjectsJustObjectClasses idefilename ideObjClasses

	-- sync each ipl in a directory with this ide file

	for iplfilename in iplfilenames do (
		GtaSyncIPLFiletoIDEArray iplfilename ideObjClasses 
	)
)

fn GtaSyncMakeCdImage streamdir imagename = (

	return GtaMakeGenCDImage streamdir imagename

	local command = gta_bindir + "cdimagemaker " + imagename + " "
	command += streamdir + "\\*.dff "
	command += streamdir + "\\*.ifp "
	command += streamdir + "\\*.txd "
	command += streamdir + "\\*.col "
	command += streamdir + "\\*.ipl "
	
	print command
	
	rt = doscommand command
	
	if rt != 0 then (
		return false
	)
	
	return true
)

fn GtaSyncIPLsToIDEName idefilename = (
	
	idefilename = GTAlowercase(idefilename)
	
	ipllist = #()
	idefilelist = #()
	idedirlist = #()
	idenamelist = #()

	gtarootelement = ParseXmlFile (((getdir #maxroot) + "/plugcfg") + "\\gta3.xml")
	gtaelement = getxmlelement gtarootelement "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2_generic"
	gtaelement = getxmlelement gtaelement "objects"	
	
	for elems in gtaelement.entries do (
		testfilename = GTAlowercase(elems.entries[1])
		testfilename = filterstring testfilename "\\"
		testfilename = testfilename[testfilename.count]
		testfilename = filterstring testfilename "."
		testfilename = testfilename[1]
		
		append ipllist testfilename
	)
	
	gtarootelement = ParseXmlFile (((getdir #maxroot) + "/plugcfg") + "\\gta3.xml")
	gtaelement = getxmlelement gtarootelement "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2_mapfiles"
	gtaelement = getxmlelement gtaelement "objects"	
	
	for elems in gtaelement.entries do (
		testfilename = GTAlowercase(elems.entries[1])
		testfilename = filterstring testfilename "\\"
		testfilename = testfilename[testfilename.count]
		testfilename = filterstring testfilename "."
		testfilename = testfilename[1]
		
		append ipllist testfilename
	)
	
	gtarootelement = ParseXmlFile (((getdir #maxroot) + "/plugcfg") + "\\gta3.xml")
	gtaelement = getxmlelement gtarootelement "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2"
	gtaelement = getxmlelement gtaelement "ide"	
	
	for elems in gtaelement.entries do (
		testfilename = GTAlowercase(elems.entries[1])
		idepathelem = filterstring testfilename "\\"
		idepath = ""
		
		for i = 1 to (idepathelem.count - 1) do (
			idepath = idepath + idepathelem[i] + "\\"
		)
		
		append idedirlist idepath
		append idefilelist testfilename
		
		testfilename = GTAlowercase(elems.entries[1])
		testfilename = filterstring testfilename "\\"
		testfilename = testfilename[testfilename.count]
		testfilename = filterstring testfilename "."
		testfilename = testfilename[1]

		append idenamelist testfilename
	)

	
	for idefile in idefilelist do (	
	
		if idefilename == idefile then (
	
			print idefile		
			for iplitem in ipllist do (
				streamdir = gta_streamdir + iplitem + "\\"		
				GtaSyncIPLDirectoryToIDE idefile streamdir
			)			
			for idedir in idedirlist do (
				GtaSyncIPLDirectoryToIDE idefile idedir
			)
		)
	)

	for i = 1 to idedirlist.count do (
		GtaSyncMakeCdImage (gta_streamdir + idenamelist[i] + "\\") (idedirlist[i] + idenamelist[i])
	)

	MakeFinalCd prompt:false
)

fn GtaSyncIPLsToIDEs = (
	
	ipllist = #()
	idefilelist = #()
	idedirlist = #()
	idenamelist = #()

	gtarootelement = ParseXmlFile (((getdir #maxroot) + "/plugcfg") + "\\gta3.xml")
	gtaelement = getxmlelement gtarootelement "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2_generic"
	gtaelement = getxmlelement gtaelement "objects"	
	
	for elems in gtaelement.entries do (
		testfilename = GTAlowercase(elems.entries[1])
		testfilename = filterstring testfilename "\\"
		testfilename = testfilename[testfilename.count]
		testfilename = filterstring testfilename "."
		testfilename = testfilename[1]
		
		append ipllist testfilename
	)
	
	gtarootelement = ParseXmlFile (((getdir #maxroot) + "/plugcfg") + "\\gta3.xml")
	gtaelement = getxmlelement gtarootelement "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2_mapfiles"
	gtaelement = getxmlelement gtaelement "objects"	
	
	for elems in gtaelement.entries do (
		testfilename = GTAlowercase(elems.entries[1])
		testfilename = filterstring testfilename "\\"
		testfilename = testfilename[testfilename.count]
		testfilename = filterstring testfilename "."
		testfilename = testfilename[1]
		
		append ipllist testfilename
	)
	
	gtarootelement = ParseXmlFile (((getdir #maxroot) + "/plugcfg") + "\\gta3.xml")
	gtaelement = getxmlelement gtarootelement "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2"
	gtaelement = getxmlelement gtaelement "ide"	
	
	for elems in gtaelement.entries do (
		testfilename = GTAlowercase(elems.entries[1])
		idepathelem = filterstring testfilename "\\"
		idepath = ""
		
		for i = 1 to (idepathelem.count - 1) do (
			idepath = idepath + idepathelem[i] + "\\"
		)
		
		append idedirlist idepath
		append idefilelist testfilename
		
		testfilename = GTAlowercase(elems.entries[1])
		testfilename = filterstring testfilename "\\"
		testfilename = testfilename[testfilename.count]
		testfilename = filterstring testfilename "."
		testfilename = testfilename[1]

		append idenamelist testfilename
	)
			
	for iplitem in ipllist do (
		streamdir = gta_streamdir + iplitem + "\\"	
		syncipltoide streamdir idefilelist
	)			
	for idedir in idedirlist do (
		syncipltoide idedir idefilelist 
	)

--	for i = 1 to idedirlist.count do (
--		GtaSyncMakeCdImage (gta_streamdir + idenamelist[i] + "\\") (idedirlist[i] + idenamelist[i])
--	)

--	MakeFinalCd()
)

struct MapSection ( name, beforeStart, beforeEnd, afterStart, afterEnd)

fn GtaSyncIPLsRebuildImages idelist = (

	ipllist = #()
	idefilelist = #()
	idedirlist = #()
	idenamelist = #()
	ideallfilelist = #()
	idealldirlist = #()
	ideallnamelist = #()
	
	gtarootelement = ParseXmlFile (((getdir #maxroot) + "/plugcfg") + "\\gta3.xml")
	gtaelement = getxmlelement gtarootelement "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2"
	gtaelement = getxmlelement gtaelement "ide"	
	
	for elems in gtaelement.entries do (
		idefilename = GTAlowercase(elems.entries[1])
		idepathelem = filterstring idefilename "\\"
		idepath = ""
		
		for i = 1 to (idepathelem.count - 1) do (
			idepath = idepath + idepathelem[i] + "\\"
		)
		
		testfilename = idefilename
		
		testfilename = GTAlowercase(elems.entries[1])
		testfilename = filterstring testfilename "\\"
		testfilename = testfilename[testfilename.count]
		testfilename = filterstring testfilename "."
		testfilename = testfilename[1]
	
		found = false
	
		for ideentry in idelist do (
			if ideentry == testfilename then (
				found = true
			)
		)

		append idealldirlist idepath
		append ideallfilelist idefilename
		append ideallnamelist testfilename
		
		if found == true then (
			append idedirlist idepath
			append idefilelist idefilename
			append idenamelist testfilename
		)
	)

	for i = 1 to idealldirlist.count do (
		GtaSyncMakeCdImage (gta_streamdir + ideallnamelist[i] + "\\") (idealldirlist[i] + ideallnamelist[i])
	)

	MakeFinalCd()
)

fn GtaSyncIPLsToIDEsListOnly idelist = (
	
	ipllist = #()
	idefilelist = #()
	idedirlist = #()
	idenamelist = #()
	ideallfilelist = #()
	idealldirlist = #()
	ideallnamelist = #()

	idealluniquedirlist = #()

	gtarootelement = ParseXmlFile (((getdir #maxroot) + "/plugcfg") + "\\gta3.xml")
	gtaelement = getxmlelement gtarootelement "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2_generic"
	gtaelement = getxmlelement gtaelement "objects"	
	
	for elems in gtaelement.entries do (
		testfilename = GTAlowercase(elems.entries[1])
		testfilename = filterstring testfilename "\\"
		testfilename = testfilename[testfilename.count]
		testfilename = filterstring testfilename "."
		testfilename = testfilename[1]
		
		append ipllist testfilename
	)
	
	gtarootelement = ParseXmlFile (((getdir #maxroot) + "/plugcfg") + "\\gta3.xml")
	gtaelement = getxmlelement gtarootelement "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2_mapfiles"
	gtaelement = getxmlelement gtaelement "objects"	
	
	for elems in gtaelement.entries do (
		testfilename = GTAlowercase(elems.entries[1])
		testfilename = filterstring testfilename "\\"
		testfilename = testfilename[testfilename.count]
		testfilename = filterstring testfilename "."
		testfilename = testfilename[1]
		
		append ipllist testfilename
	)
	
	gtarootelement = ParseXmlFile (((getdir #maxroot) + "/plugcfg") + "\\gta3.xml")
	gtaelement = getxmlelement gtarootelement "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2"
	gtaelement = getxmlelement gtaelement "ide"	
	
	for elems in gtaelement.entries do (
		idefilename = GTAlowercase(elems.entries[1])
		idepathelem = filterstring idefilename "\\"
		idepath = ""
		
		for i = 1 to (idepathelem.count - 1) do (
			idepath = idepath + idepathelem[i] + "\\"
		)
		
		testfilename = idefilename
		
		testfilename = GTAlowercase(elems.entries[1])
		testfilename = filterstring testfilename "\\"
		testfilename = testfilename[testfilename.count]
		testfilename = filterstring testfilename "."
		testfilename = testfilename[1]
	
		found = false
	
		for ideentry in idelist do (
			if ideentry == testfilename then (
				found = true
			)
		)

		append idealldirlist idepath
		append ideallfilelist idefilename
		append ideallnamelist testfilename
		
		idelowerpath = GTAlowercase(idepath)
		if finditem idealluniquedirlist idelowerpath == 0 then (
			append idealluniquedirlist idelowerpath
		)
		
		if found == true then (
			append idedirlist idepath
			append idefilelist idefilename
			append idenamelist testfilename
		)
	)

	print "here"

	for idefile in idefilelist do (
		print idefile
	)

	for iplitem in ipllist do (
		streamdir = gta_streamdir + iplitem + "\\"	
		syncipltoide streamdir idefilelist
	)			
	for idedir in idealluniquedirlist do (
		syncipltoide idedir idefilelist
	)

	for i = 1 to idealldirlist.count do (
		GtaSyncMakeCdImage (gta_streamdir + ideallnamelist[i] + "\\") (idealldirlist[i] + ideallnamelist[i])
	)
	
	MakeFinalCd()
)

MainIdxCounter = 0
IdChangeList = #()
IdChangeToList = #()

fn GtaSyncIdeFileChunk ideNewFile ideFile section mapSect = (

	ideLine = readLine ideFile

	while(eof ideFile == false) do (
		
		if ideLine == section then (
			exit
		)
		
		format "%\n" ideLine to:ideNewFile
		ideLine = readLine ideFile	
	)
	
	format "%\n" section to:ideNewFile
	
	-- go through each object instance
	ideLine = readLine ideFile
	
 	while(eof ideFile == false) do (
		
		if ideLine == "end" then (
			exit
		)
		
		ideTokens = filterstring ideLine ", "
		
		objId = ideTokens[1] as integer
		objName = ideTokens[2]
			
		append IdChangeList objId
		objId = mapSect.afterStart + MainIdxCounter
		append IdChangeToList objId
		
		MainIdxCounter = MainIdxCounter + 1

		if mapSect.afterEnd != -1 and objId > mapSect.afterEnd then (
			print (objName + " is over end of new section " + (objId as string))
			return false
		)

		if section == "anim" then (
			format "% ,%" objId objName to:ideNewFile
	
			for j = 3 to ideTokens.count do (
				format " ,%" ideTokens[j] to:ideNewFile
			)
		) else (
			format "%, %" objId objName to:ideNewFile
	
			for j = 3 to ideTokens.count do (
				format ", %" ideTokens[j] to:ideNewFile
			)
		)	

		format "\n" to:ideNewFile
				
		ideLine = readLine ideFile	
	)
	
	format "end\n" to:ideNewFile
	
	return true
)

fn GtaSyncIdeFile2dfxChunk ideNewFile ideFile section mapSect = (

	ideLine = readLine ideFile

	while(eof ideFile == false) do (
		
		if ideLine == section then (
			exit
		)
		
		format "%\n" ideLine to:ideNewFile
		ideLine = readLine ideFile	
	)
	
	format "%\n" section to:ideNewFile
	
	-- go through each object instance
	ideLine = readLine ideFile
	
 	while(eof ideFile == false) do (
		
		if ideLine == "end" then (
			exit
		)
		
		ideTokens = filterstring ideLine ", "
		
		objId = ideTokens[1] as integer
			
		idxFound = finditem IdChangeList objId	
		if idxFound == 0 then (
			objId = objId - mapSect.beforeStart + mapSect.afterStart
		) else (
			objId = IdChangeToList[idxFound]
		)

		if mapSect.afterEnd != -1 and objId > mapSect.afterEnd then (
			print (objName + " is over end of new section " + (objId as string))
			return false
		)
		
		format "%" objId to:ideNewFile

		for j = 2 to ideTokens.count do (		
			format ", %" ideTokens[j] to:ideNewFile
		)

		format "\n" to:ideNewFile
				
		ideLine = readLine ideFile	
	)
	
	format "end\n" to:ideNewFile
	
	return true
)

fn GtaSyncIDEFileForSection ideFileName mapSect = (
	tempfilename = "c:\\temp.ide"

	IdChangeList = #()
	IdChangeToList = #()

	ideNewFile = openfile tempfilename mode:"w+"
	
	if ideNewFile == undefined then (
		messagebox "couldnt open temporary file"
		return false
	)
	
	ideFile = openfile ideFileName mode:"rt"
	
	-- find the beginning of the object classes
	MainIdxCounter = 0
	
	if GtaSyncIdeFileChunk ideNewFile ideFile "objs" mapSect == false then (
		close ideFile
		close ideNewFile
		return false
	)
	
	if GtaSyncIdeFileChunk ideNewFile ideFile "tobj" mapSect == false then (
		close ideFile
		close ideNewFile
		return false
	)
	
	if GtaSyncIdeFile2dfxChunk ideNewFile ideFile "2dfx" mapSect == false then (
		close ideFile
		close ideNewFile
		return false
	)	
	
	if GtaSyncIdeFileChunk ideNewFile ideFile "anim" mapSect == false then (
		close ideFile
		close ideNewFile
		return false
	)
	
	--write out the rest of the file
 	while(eof ideFile == false) do (	
		ideLine = readLine ideFile		
		format "%\n" ideLine to:ideNewFile
	)
	
	close ideFile
	close ideNewFile 
	
	deletefile idefilename
	retval = copyfile tempfilename idefilename 
	
	if retval == false then (
		print ("couldnt copy over " + idefilename)
	)
	
	deletefile tempfilename
	
	return retval
)

fn GtaSyncExtractSection afterXML mapSect = (

	mapSectCompare = mapSect.name + "."

	XMLElem = ParseXmlFile afterXML

	gtaelement = getxmlelement XMLElem "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2"
	gtaelement = getxmlelement gtaelement "ide"	
	
	for elems in gtaelement.entries do (
		
		if findstring elems.entries[1] mapSectCompare != undefined then (			
			imgname = (filterstring elems.entries[1] ".")[1] + ".img"

			streamdir = gta_streamdir + mapSect.name + "\\"
			makedir streamdir
			cmdline = "del /Q " + streamdir + "*.*"
			
			print cmdline
			doscommand cmdline
			
			sysinfo.currentdir = streamdir
			cmdline = gta_bindir + "cdimageextract " + imgname + " *"
			
			print cmdline
			
			if doscommand cmdline != 0 then (
				return false
			)
		)
	)
	
	return true
)

fn GtaSyncIDEForSection afterXML mapSect = (

	mapSectCompare = mapSect.name + "."

	XMLElem = ParseXmlFile afterXML

	gtaelement = getxmlelement XMLElem "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2"
	gtaelement = getxmlelement gtaelement "ide"	
	
	for elems in gtaelement.entries do (
		
		if findstring elems.entries[1] mapSectCompare != undefined then (			
			if GtaSyncIDEFileForSection elems.entries[1] mapSect == false then (
				return false
			)
		)
	)
	
	return true
)

fn qsMapSectionBefores secA secB = (
	if secA.beforeStart < secB.beforeStart then (
		return -1
	)
	
	if secA.beforeStart > secB.beforeStart then (
		return 1
	)
	
	return 0
)

fn qsMapSectionAfters secA secB = (
	if secA.afterStart < secB.afterStart then (
		return -1
	)
	
	if secA.afterStart > secB.afterStart then (
		return 1
	)
	
	return 0
)

fn GtaSyncAllIpl beforeXML afterXML = (

	beforeXMLElem = ParseXmlFile beforeXML
	afterXMLElem = ParseXmlFile afterXML

	mapSections = #()
	
	-- the before xml file
	
	gtaelement = getxmlelement beforeXMLElem "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2_generic"
	gtaelement = getxmlelement gtaelement "objects"	
	
	for elems in gtaelement.entries do (
	
		mapSect = MapSection()
	
		testfilename = GTAlowercase(elems.entries[1])
		testfilename = filterstring testfilename "\\"
		testfilename = testfilename[testfilename.count]
		testfilename = filterstring testfilename "."
		testfilename = testfilename[1]
	
		mapSect.name = testfilename
		
		idnumber = (filterstring elems.params[1] "\"")[2] as integer
		
		mapSect.beforeStart = idnumber
		mapSect.beforeEnd = -1
		mapSect.afterStart = idnumber
		mapSect.afterEnd = -1
		
		append mapSections mapSect
	)
	
	gtaelement = getxmlelement beforeXMLElem "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2_mapfiles"
	gtaelement = getxmlelement gtaelement "objects"	
	
	for elems in gtaelement.entries do (
	
		mapSect = MapSection()
	
		testfilename = GTAlowercase(elems.entries[1])
		testfilename = filterstring testfilename "\\"
		testfilename = testfilename[testfilename.count]
		testfilename = filterstring testfilename "."
		testfilename = testfilename[1]
	
		mapSect.name = testfilename
		
		idnumber = (filterstring elems.params[1] "\"")[2] as integer
		
		mapSect.beforeStart = idnumber
		mapSect.beforeEnd = -1
		mapSect.afterStart = idnumber
		mapSect.afterEnd = -1
	
		append mapSections mapSect
	)
	
	qsort mapSections qsMapSectionBefores
	
	for i = 2 to mapSections.count do (
	
		mapSect = mapSections[i]
		
		mapSectBefore = mapSections[i - 1]
		mapSectBefore.beforeEnd = mapSect.beforeStart - 1
		mapSectBefore.afterEnd = mapSect.afterStart - 1		
	)
	
	-- the after xml file
	
	gtaelement = getxmlelement afterXMLElem "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2_generic"
	gtaelement = getxmlelement gtaelement "objects"	
	
	for elems in gtaelement.entries do (
	
		testfilename = GTAlowercase(elems.entries[1])
		testfilename = filterstring testfilename "\\"
		testfilename = testfilename[testfilename.count]
		testfilename = filterstring testfilename "."
		testfilename = testfilename[1]
	
		j = -1
	
		for i = 1 to mapSections.count do (
			if mapSections[i].name == testfilename then (
				j = i
			)
		)
		
		if j != -1 then (
	
			idnumber = (filterstring elems.params[1] "\"")[2] as integer
			
			mapSections[j].afterStart = idnumber
			mapSections[j].afterEnd = -1
		)
	)
	
	gtaelement = getxmlelement afterXMLElem "gta"
	gtaelement = getxmlelement gtaelement "sa_ps2_mapfiles"
	gtaelement = getxmlelement gtaelement "objects"	
	
	for elems in gtaelement.entries do (
		
		testfilename = GTAlowercase(elems.entries[1])
		testfilename = filterstring testfilename "\\"
		testfilename = testfilename[testfilename.count]
		testfilename = filterstring testfilename "."
		testfilename = testfilename[1]
	
		j = -1
	
		for i = 1 to mapSections.count do (
			if mapSections[i].name == testfilename then (
				j = i
			)
		)
		
		idnumber = (filterstring elems.params[1] "\"")[2] as integer
		
		if j != -1 then (
			mapSections[j].afterStart = idnumber
			mapSections[j].afterEnd = -1
		)
	)
	
	qsort mapSections qsMapSectionAfters
	
	for i = 2 to mapSections.count do (
	
		mapSect = mapSections[i]
		
		mapSectBefore = mapSections[i - 1]
		mapSectBefore.afterEnd = mapSect.afterStart - 1		
	)

	for i = 1 to mapSections.count do (
		mapSect = mapSections[i]
		GtaSyncExtractSection afterXML mapSect
	)

	found = undefined

	do
	(
		found = false
	
		for i = 1 to mapSections.count do (	
			mapSect = mapSections[i]

			if (mapSect.beforeStart == mapSect.afterStart) and (mapSect.beforeEnd == mapSect.afterEnd) then (
				deleteItem mapSections i
				found = true
				exit
			)
		)
	)
	while(found)
	
	ideNameList = #()
	
	for i = 1 to mapSections.count do (
	
		mapSect = mapSections[i]
		append ideNameList mapSect.name		
		
		print ("sync ide: " + mapSect.name)
		print ("before start:" + (mapSect.beforeStart as string) + " end: " + (mapSect.beforeEnd as string))
		print ("after start:" + (mapSect.afterStart as string) + " end: " + (mapSect.afterEnd as string))	
		
		if GtaSyncIDEForSection afterXML mapSect == false then (
			print "failed to sync ide"
			return false
		)
	)
	 	
	GtaSyncIPLsToIDEsListOnly ideNameList
--	GtaSyncIPLsToIDEs()
)
