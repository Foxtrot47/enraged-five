--
-- boneidlist.ms
-- Bone ID List XML File Management
--
-- David Muir <david.muir@rockstarnorth.com>
-- 25 February 2008
--
-- Example Usage
--
--   l = BoneListID()
--   l.load "X:\gta\bin\source\vehicles\vehicles_boneid.xml"
--   b = l.GetBoneIDs()
--

include "pipeline/util/xml.ms"

struct BoneID
(
	id,
	name
)

struct BoneIDList
(

	filename = "",
	xml,
		
	--
	-- name: Load
	-- desc: Load list from XML file
	--
	fn Load xmlfile = (
	
		filename = xmlfile
		xml = XmlDocument()
		xml.init()
		xml.load( filename )
		
		xmlRoot = xml.document.DocumentElement
		( "bones" == xmlRoot.name )
	),
	
	--
	-- name: Reload
	-- desc: Reload list from XML file
	--
	fn Reload = (
	
		load filename
	),
	
	--
	-- name: GetBoneIDs
	-- desc: Return array of bone ID / name pairs
	--
	fn GetBoneIDs = (
		
		local boneids = #()
		xmlRoot = xml.document.DocumentElement
		if ( "bones" == xmlRoot.name ) then
		(
		
			local childNodeList = xmlRoot.ChildNodes
			for nChild = 0 to ( childNodeList.Count - 1 ) do
			(
				
				local elem = childNodeList.ItemOf( nChild )
				if ( "bone" == elem.name ) then
				(
				
					local attrID = elem.Attributes.ItemOf( "id" )
					local attrName = elem.Attributes.ItemOf( "name" )
					local id = attrID.Value as integer
					
					append boneids ( BoneID id attrName.Value )
				)
			)
		)
		
		boneids
	),
	
	--
	-- name: Count
	-- desc: Return number of textures in list
	--
	fn Count = (
	
		local boneids = GetBoneIDs()
		boneids.count
	)	
)

-- End of boneidlist.ms
