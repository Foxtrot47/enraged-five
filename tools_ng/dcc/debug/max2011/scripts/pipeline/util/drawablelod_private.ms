--
-- File:: drawablelod_private.ms
-- Description:: Drawable LOD Hierarchy Private Functions
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 24 March 2009
-- 

-----------------------------------------------------------------------------
-- PRIVATE FUNCTIONS -- DO NOT USE
-----------------------------------------------------------------------------


--
-- name: __RsLodDrawable_GetChildren (private)
-- desc: Return Array of Drawable LOD children.
--
fn __RsLodDrawable_GetChildren lodparent = (
	local childObjs = #()
	local obj = lodparent

	refobjs = refs.dependents obj

	for refObj in refobjs do (
	
		if isdeleted refObj == false then (
	
			if classof(refObj) == LodDrawableAttributes then (			
				
				refobjs2 = refs.dependents refObj
			
				if isProperty refobjs2[1] "name" then (
			
					append childObjs refobjs2[1]
				)
			)
		)
	)
	
	return childObjs
)

-- drawablelod_private.ms
