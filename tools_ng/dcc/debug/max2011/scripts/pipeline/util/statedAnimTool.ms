try gRsStatedAnimTool.Deinit() catch()
global gRsStatedAnimTool = sRsStatedAnimTool()

filein "pipeline/helpers/rigging/weldSkinnedMeshes.ms"
filein "pipeline/helpers/rigging/collapseChildMeshesAndSkin.ms"

global gCameraPos = undefined

-- fn comparePickedNodes v1 v2 =
-- (
-- 	local o1 = v1[1]
-- 	local o2 = v2[1]
-- 	print o1
-- 	print o2
-- 	if o1.isHidden or not (IsValidNode o1) then 
-- 	(
-- 		print "1 is hidden"
-- 		return 1
-- 	)
-- 	if o2.isHidden or not (IsValidNode o2) then 
-- 	(
-- 		print "2 is hidden"
-- 		return -1
-- 	)
-- 	print "none is hidden"
-- 	local dist1 = o1.pos - gCameraPos
-- 	local dist2 = o1.pos - gCameraPos
-- 	local d = length (dist1 - dist2)
-- 	case of
-- 	(
-- 		(d < 0.): -1
-- 		(d > 0.): 1
-- 		default: 0
-- 	)
-- )
-- tool StatePicker
-- (
-- 	on mousePoint click do coordsys grid
-- 	(
-- 		local mouseray = mapScreenToWorldRay mouse.pos
-- 		local nodearray = intersectRayScene mouseray
-- 		if nodearray.count>=1 then
-- 		(
-- 			local coordSysTM = Inverse(getViewTM())
-- 			-- get the view position from this matrix
-- 			gCameraPos = coordSysTM.row4
-- 			qsort nodearray comparePickedNodes
-- 			local obj = nodearray[1][1]
-- 			print  obj
-- 			if "Gta Object"!=(getattrclass obj) then
-- 			(
-- 				messagebox "not a valid \"Gta Object\""
-- 				return false
-- 			)
-- 			print ("click:"+click as string)
-- 			local rootOfObject = gRsStatedAnimTool.addObject obj click

-- 			print gRsStatedAnimTool.currAnim.objectArray.count
-- 			if gRsStatedAnimTool.currAnim.objectArray.count == 3 then 
-- 			(
-- 				print "stop"
-- 				#stop
-- 				return true
-- 			)
-- 		)
-- 	)
-- )

rollout RsAnimFinder "Scene Animation Finder" width:160 height:180
(
	progressbar findprogress ""
	listBox animGroupList "AnimGroups in scene" width:144 height:8
	button loadButton "Load" width:72 height:16 across:2
	button cancelButton "Cancel" width:64 height:16
	
	on RsAnimFinder open do
	(
		animGroupList.items = gRsStatedAnimTool.findAnimGroups theProgressbar:findprogress
	)
	on loadButton pressed do
	(
		if undefined!=animGroupList.selected then
			gRsStatedAnimTool.loadAnim animGroupList.selected
		RsStatedAnimRollout.updateList()
		DestroyDialog RsAnimFinder
	)
	on animGroupList doubleClicked arg do
	(
		if undefined!=animGroupList.selected then
			gRsStatedAnimTool.loadAnim animGroupList.selected
		RsStatedAnimRollout.updateList()
		DestroyDialog RsAnimFinder
	)
	on cancelButton pressed do
	(
		DestroyDialog RsAnimFinder
	)
)

try (CloseRolloutFloater RsStatedAnimFloater ) catch(print "First open")

rollout RsStatedAnimRollout "Stated animation setup" width:272 height:232
(
	hyperlink help "help" address:"https://devstar.rockstargames.com/wiki/index.php/Simulated_Map_Animations#Art_setup" align:#right color:(color 0 0 255)
	listbox objectList "States" pos:[8,8] width:136 height:4
	button startButton "play" pos:[56,104] width:32 height:16
	button stopButton "stop" pos:[96,104] width:32 height:16
	pickbutton addButton "Add state object" pos:[152,46] width:120 height:16
	button deleteButton "Delete state object" pos:[152,64] width:120 height:16
	checkbox hideCheck "Hide inactive states" pos:[136,104] width:120 height:16 checked:true
	button setupButton "start" pos:[16,104] width:32 height:16
	edittext gorupNameBox "groupName" pos:[16,152] width:176 height:16 text:"DEFAULT"
	GroupBox grp1 "Save to objects attributes" pos:[8,136] width:256 height:80
	button saveAttrButton "Save" pos:[16,192] width:48 height:16
	button deleteAttrButton "Delete" pos:[72,192] width:48 height:16
	checkbox loopCheckBox "looped animation" pos:[16,172] width:112 height:16
	button proxyButton "Build proxy" pos:[128,192] width:64 height:16
	GroupBox grp3 "Playback" pos:[8,88] width:256 height:40
	button findAnimButton "Find animation in scene" pos:[152,28] width:120 height:16
	
	fn updateList = 
	(
		objectList.items  = gRsStatedAnimTool.getObjectNames()
		gorupNameBox.text = gRsStatedAnimTool.getCurrentAnimGroupName()
		loopCheckBox.checked = gRsStatedAnimTool.currAnim.looped
	)
	
	fn selChanged ev nd = 
	(
		if not gRsStatedAnimTool.selectionChanged() then 
			gRsStatedAnimTool.Deinit()

		updateList()
	)
	
	fn enableCallbacks flag = 
	(
		if flag then
			global gAnimProxySelChangeCb = NodeEventCallback selectionChanged:selChanged
		else
			gAnimProxySelChangeCb = undefined
	)
	
	on objectList selected val do 
	(
		gRsStatedAnimTool.selectObject val
	)
	on RsStatedAnimRollout open do
	(
		global gAnimProxySelChangeCb = NodeEventCallback selectionChanged:selChanged
		callbacks.removeScripts id:#RsStatedAnimPreLoad
		callbacks.addScript #filePreOpenProcess "gRsStatedAnimTool.Deinit(); RsStatedAnimRollout.updateList()" id:#RsStatedAnimPreLoad
		print "initialised"
	)
	on RsStatedAnimRollout close do
	(
		gRsStatedAnimTool.Deinit()
		gAnimProxySelChangeCb = undefined
		callbacks.removeScripts id:#RsStatedAnimPreLoad
		gc()
	)
	on findAnimButton pressed do
	(
		try DestroyDialog RsAnimFinder catch()
		CreateDialog RsAnimFinder
	)
	on startButton pressed do
	(
		gRsStatedAnimTool.startPlayback()
	)
	on stopButton pressed do
	(
		gRsStatedAnimTool.stopPlayback()
		gRsStatedAnimTool.unhideAll()
	)
	on addButton picked obj do
	(
		if "Gta Object"!=(getattrclass obj) then
		(
			messagebox "not a valid \"Gta Object\""
			return false
		)
	
		local rootOfObject = gRsStatedAnimTool.addObject obj objectList.selection
		if false==rootOfObject then return false
	
		updateList()
		local groupIdx = getattrindex "Gta Object" "groupName"
		local objGroupName = getAttr rootOfObject groupIdx
		if objGroupName != "" then gorupNameBox.text=objGroupName
	)
-- 	on addButton pressed do
-- 	(
-- 		startTool StatePicker
-- 	)
-- 	on deleteButton pressed do
-- 	(
-- 		if objectList.selected!=undefined then
-- 		(
-- 			if (gRsStatedAnimTool.deleteObject objectList.selected) then
-- 			(
-- 				updateList()
-- 			)
-- 		)
-- 	)
	on setupButton pressed do
	(
		gRsStatedAnimTool.hideInactiveObjects 1
	)
	on saveAttrButton pressed do
	(
		gRsStatedAnimTool.saveAttributes gorupNameBox.text
	)
	on deleteAttrButton pressed do
	(
		gRsStatedAnimTool.deleteAttributes()
	)
	on loopCheckBox changed val do
	(
		gRsStatedAnimTool.currAnim.looped = val
	)
	on proxyButton pressed do
	(
		gRsStatedAnimTool.buildProxy gorupNameBox.text
	)
)

rollout RsStatedAnimTools "Stated animation tools" width:272 height:232
(
	hyperlink help "help" address:"https://devstar.rockstargames.com/wiki/index.php/Simulated_Map_Animations#Skinning_tools" align:#right color:(color 0 0 255)
	button buttCreateSkin "Create skinned version" width:265 tooltip:"All objects of the animation are supposed to be in one hirarchy setup as if to export for an animated object. Select one of the objects of the animation and click this button. The material of the selection is the multimaterial that's put on the end result."
	button buttWeldSkins "Weld two selected skinned meshes" width:265 tooltip:"This function assumes two or more objects to be selected of which at least one is skinned. The first object is the reference for the result's name and material. Also, if the first object's bone hirarchy is set up for export, all the bones of the objects to be attached will get welded into the hirarchy."
	
	on buttCreateSkin pressed do
	(
		bakeAnimToPoints $
	)
	on buttWeldSkins pressed do
	(
		weldSelection()
	)
)

global RsStatedAnimFloater = newRolloutFloater "Stated anim tools" 300 360
addRollout RsStatedAnimRollout RsStatedAnimFloater
addRollout RsStatedAnimTools RsStatedAnimFloater
--CreateDialog RsStatedAnimRollout