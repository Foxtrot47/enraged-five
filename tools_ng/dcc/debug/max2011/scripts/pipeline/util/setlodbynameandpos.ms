--
-- File:: pipeline/util/setlodbynameandpos.ms
-- Description:: Sets the lod by the name of the object and its position
--
-----------------------------------------------------------------------------

selset = #()

for obj in selection do
(
	append selset obj
	true=0
	if classof obj==editable_mesh do (true=1)
	if classof obj==editable_poly do (true=1)
	if classof obj==XRefObject do (true=1)
	if classof lods==InternalRef do (true=1)

	if true==1 do
	( if findstring obj.name "lod" == undefined do
		(
		for lods in rootnode.children do
			(
			lodtrue=0
			if classof lods==editable_mesh do lodtrue=1
			if classof obj==editable_poly do lodtrue=1
			if classof lods==XRefObject do lodtrue=1
			if classof lods==InternalRef do lodtrue=1
			if lodtrue==1 do
				(
				if findstring lods.name "lod" != undefined do
					(
					pos=0
					xx=lods.position.x - obj.position.x
					yy=lods.position.y - obj.position.y
					zz=lods.position.z - obj.position.z
					if xx <1 and xx>-1 do (pos = pos +1)
					if yy <1 and yy>-1 do (pos = pos +1)
					if zz <1 and zz>-1 do (pos = pos +1)
					if pos==3 do
						(
						--we made it it's the lod!
						addlodattr obj
						setlodattrparent obj lods
						)
					)
				)
			)
		)
	)
)
clearselection()
select selset