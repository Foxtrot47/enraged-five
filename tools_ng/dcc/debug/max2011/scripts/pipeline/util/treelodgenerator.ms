--
-- File:: pipeline/util/treelodgenerator.ms
-- Description:: Map Tree LOD generator functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 24 June 2009
--
-- HISTORY
-- Based off of the original "Tree Check and Lod" utility by Greg Smith and
-- Luke Openshaw.
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"
filein "pipeline/util/settingsave.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- utility: RsTreeLodGenerator
-- desc: Tree LOD generator functions.
--
utility RsTreeLodGenerator "RsTreeLodGenerator"
(
	--
	-- name: Generate
	-- desc:
	--
	-- param: Array of objects to generate Tree LOD for.
	-- param: srcFilename
	-- param: prefix
	--
	fn Generate objects srcFilename prefix hideAfter centrePiv = (

		setCommandPanelTaskMode #create 

		RsTreeLodGenerator.__RegisterDialogMonitorOPS()
		local objlist = ( RsTreeLodGenerator.__FindValidObjects objects srcFilename )
		if ( 0 == objlist.count ) then
		(
			MessageBox "No valid objects to create Tree LODs for in object list."
			return ( false )
		)

		format "Valid Objects:\n"
		for o in objlist do
		(
			format "\t%\n" o.name
		)

		RsTreeLodGenerator.__DoAdjust objlist
		firstobj = undefined

		progressstart "Creating Tree LODs: Importing LOD Objects"
		firstobjnormals = undefined
		firstobjnormalcount = 0
		firstobjrot = undefined
		normallist = #()
		newobjlist = #()

		for i = 1 to objlist.count do (

			obj = objlist[i]
			objpos = obj.pos
			objrot = obj.rotation

			progressupdate ((100.0 * i) / objlist.count)

			local lodobjname = (prefix + obj.objectname)
			newobj = xrefs.addnewxrefobject srcFilename lodobjname manipulators:#merge

			if ( newobj == undefined ) then 
			(
				format "Unable to create XRef for %.  Is % defined in %?  The SLOD names are case sensitive\n" obj.objectname lodobjname srcFilename
				continue
			)

			if ( XRef_Material == classof newobj.material ) then 
			(
				format "Copying XRef_Material: %\n" newobj.material
				newobj.material = copy (newobj.material.getsourcematerial true)
			)

			newobj.objectoffsetpos = [0,0,0]
			newobj.objectoffsetrot = (quat 0 0 0 1)
			newobj.rotation = objrot
			newobj.pos = objpos

			converttomesh newobj

			if ( undefined == firstobj ) then 
			(
				firstobj = newobj
				print(newobj.name)
				print(newobj.pos)

				addmodifier firstobj (Edit_Normals())
				select firstobj

				setCommandPanelTaskMode #modify					
				modPanel.setCurrentObject firstobj.modifiers[1]

				firstobjnormals = firstobj.modifiers[1]
				firstobjnormalcount = firstobjnormals.GetNumNormals()
				firstobjrot = firstobj.rotation
			) 
			else 
			(
				-- Switch modifiers as needed.  Limitation in max
				addmodifier newobj (Edit_Normals())
				select newobj

				setCommandPanelTaskMode #modify					
				modPanel.setCurrentObject newobj.modifiers[1]

				newobjnormals = newobj.modifiers[1]

				--format "attach:\n"
				--format "\tfirstobj: %\n" firstobj
				--format "\tnewobj: %\n" newobj
				--meshop.attach firstobj newobj deleteSourceNode:false

				for i=1 to newobjnormals.GetNumNormals() do (
					-- Need to apply original object rotations to the normals since they
					-- will be moved into the base object's coord space
					normalasmatrix = MatrixFromNormal (newobjnormals.GetNormal i)
					rotate normalasmatrix firstobjrot
					rotate normalasmatrix (inverse newobj.rotation)
					append normallist (normalasmatrix.row3)

				)
				append newobjlist newobj
				unhide newobj
				collapsestack newobj
			)
		)

		progressend()
		progressstart "Creating Tree LODs: Merging LOD Objects"

		if ( undefined == firstobj) do (
		
			messagebox "Failed - were Xref objects created successfully?  (Check the listener) SLOD object names are case sensitive"
			progressend()
			return false
		)
		
		select firstobj
		setCommandPanelTaskMode #modify					
		modPanel.setCurrentObject firstobj.modifiers[1]
		-- Lay down the normals after the new mesh has been created.
		-- Fortunately this works
		j = 1
		for i = (firstobjnormalcount + 1) to firstobj.modifiers[1].GetNumNormals() do (
			--print normallist[j]
			firstobj.modifiers[1].SetNormal i normallist[j]
			j = j + 1
		) 

		firstobj.modifiers[1].Unify toAverage:true
		unhide firstobj
		collapsestack firstobj

		firstobj.name = ( RsTreeLodGenerator.__DetermineSLodName "TreeSLOD_" )

		savesel = #() + selection

		select firstobj
		selectmore newobjlist
		
		-- Attach all meshes together to form SLOD object.
		for o in newobjlist do
		(
			format "Attaching: %\n" o.name
			meshop.attach firstobj o deleteSourceNode:false
		)

		if ( centrePiv ) then
			CenterPivot firstobj
			
		-- Delete all temporary objects
		for obj in newobjlist do delete obj

		-- Fix up scene hierarchy.
		-- Hide objects after generation if requested.
		select savesel
		for obj in objlist do (
			AddLodAttr obj
			setlodattrparent obj firstobj
			
			if ( hideAfter ) then
				hide obj
		)
		if ( hideAfter ) then
			hide firstobj
		RsTreeLodGenerator.__RestoreDefaultAttrs firstobj
		
		progressEnd()

		RsTreeLodGenerator.__UnregisterDialogMonitorOPS()
	)

	--
	-- name: FixNormals
	-- desc:
	--
	fn FixNormals objects srcFilename = (

		setCommandPanelTaskMode #modify
		for obj in objects do (

			nomod = false

			if obj.modifiers.count > 1 then (
				
				print "obj can only have 1 modifier"
				return 0
			) 
		
			if obj.modifiers.count == 0 then (
			
				nomod = true
			)
		
			if classof obj == Editable_Poly then (
			
				convertto obj Editable_Mesh
			)
		
			if obj.modifiers.count == 0 then (
			
				addmodifier obj (Edit_Normals())
			)
			
			if (classof obj.modifiers[1]) != Edit_normals then (
			
				print "obj has to have Edit_normals modifier"
				return 0
			)
			
			refMod = obj.modifiers[1]
			
			if refmod.getnumfaces() != getnumfaces(obj) then (
		
				print "number of normal faces does match mesh faces"
				return 0	
			)
			
			numfaces = refmod.getnumfaces()
		
			for i = 1 to numfaces do (
			
				if refmod.getfacedegree i != 3 then (
				
					print "normal face isnt a tri"
					return 0				
				)
			)
		
			flipfaces = #()
						
			for i = 1 to numfaces do (
			
				meshFace = getface obj i
				
				normal_idx_a = refmod.getnormalid i 1
				normal_idx_b = refmod.getnormalid i 2
				normal_idx_c = refmod.getnormalid i 3
				normal_a = refMod.Getnormal normal_idx_a
				normal_b = refMod.Getnormal normal_idx_b
				normal_c = refMod.Getnormal normal_idx_c
					
				normal_check = (normal_a + normal_b + normal_c) / 3.0
				
				vert_a = (getvert obj meshFace[1]) * obj.transform
				vert_b = (getvert obj meshFace[2]) * obj.transform
				vert_c = (getvert obj meshFace[3]) * obj.transform
				
				edge_a = normalize(vert_b - vert_a)
				edge_b = normalize(vert_c - vert_a)
				
				normal_face = cross edge_a edge_b
		
				check_val = dot normal_check normal_face
				
				if check_val < 0.0 then (
				
					append flipfaces i
					print "flip"
					print i	
				)
			)
			
			collapsestack obj
			
			for flip in flipfaces do (
			
				meshFace = getface obj flip
				meshTVFace = gettvface obj flip
				meshNewFace = [meshFace[2],meshFace[1],meshFace[3]]
				meshNewTVFace = [meshTVFace[2],meshTVFace[1],meshTVFace[3]]
			
				settvface obj flip meshNewTVFace
				setface obj flip meshNewFace		
			)
			
			update obj
			
			if nomod == false then (
			
				addmodifier obj (Edit_Normals())
			)
		)
	)

	--
	-- name: AdjustOrientation
	-- desc:
	--
	fn AdjustOrientation objects srcFilename = (
	
		local objlist = ( RsTreeLodGenerator.__FindValidObjects objects srcFilename )
		if ( 0 == objlist.count ) then
		(
			MessageBox "No valid objects to adjust orientation for in object list."
			return ( false )
		)		
		
		RsTreeLodGenerator.__DoAdjust objlist
		true
	)

	-------------------------------------------------------------------------
	-- Save Setting Functions
	-------------------------------------------------------------------------

	--
	-- name: SaveFilename
	-- desc: Save the filename as the Tree LOD maxfile.
	--
	fn SaveFilename filename = (
		RsSettingWrite "rstreelodgenerator" "filename" filename
	)

	--
	-- name: SavePrefix
	-- desc: Save the LOD object prefix string.
	--
	fn SavePrefix prefix = (
		RsSettingWrite "rstreelodgenerator" "prefix" prefix
	)

	--
	-- name: SaveHideAfter
	-- desc:
	--
	fn SaveHideAfter hideAfter = (
		RsSettingWrite "rstreelodgenerator" "hideafter" hideAfter
	)
	
	--
	-- name: SaveCentrePivot
	-- desc:
	--
	fn SaveCentrePivot centrepivot = (
		RsSettingWrite "rstreelodgenerator" "centrepivot" centrepivot
	)

	-------------------------------------------------------------------------
	-- Load Setting Functions
	-------------------------------------------------------------------------	
	
	--
	-- name: LoadFilename
	-- desc: Return the saved filename for the Tree LOD maxfile.
	--
	fn LoadFilename = (
		(RsSettingsReadString "rstreelodgenerator" "filename" "")
	)
	
	--
	-- name: LoadPrefix
	-- desc:
	--
	fn LoadPrefix = (
		(RsSettingsReadString "rstreelodgenerator" "prefix" "SLOD_")
	)
	
	--
	-- name: LoadHideAfter
	-- desc:
	--
	fn LoadHideAfter = (
		(RsSettingsReadBoolean "rstreelodgenerator" "hideafter" true)
	)
	
	--
	-- name: LoadCentrePivot
	-- desc:
	--
	fn LoadCentrePivot = (
		(RsSettingsReadBoolean "rstreelodgenerator" "centrepivot" true)
	)
	
	-------------------------------------------------------------------------
	-- Private Functions (DO NOT USE)
	-------------------------------------------------------------------------
	
	--
	-- name: __RestoreDefaultAttrs
	-- desc: Restore default attributes for SLOD object
	--
	fn __RestoreDefaultAttrs obj = (
		local idxTxdNameAttr = ( GetAttrIndex "Gta Object" "TXD" )
		local defTxdNameAttr = ( GetAttrDefault "Gta Object" idxTxdNameAttr )
		local idxLodDistanceAttr = ( GetAttrIndex "Gta Object" "LOD distance" )
		local defLodDistanceAttr = ( GetAttrDefault "Gta Object" idxLodDistanceAttr )
		local idxDontExportAttr = ( GetAttrIndex "Gta Object" "Dont Export" )
		local idxDontAddToIplAttr = ( GetAttrIndex "Gta Object" "Dont Add To IPL" ) 
		
		-- Set default TXD
		SetAttr obj idxTxdNameAttr defTxdNameAttr
		-- Set default LOD distance
		SetAttr obj idxLodDistanceAttr defLodDistanceAttr
		-- Set default export options
		SetAttr obj idxDontExportAttr false
		SetAttr obj idxDontAddToIplAttr false
	)
	
	--
	-- name: Determine the SLOD object's name.
	-- desc: Return the SLOD object's name.
	--
	fn __DetermineSLodName lodprefix = (
		local testindex = 0
		local testname = ""
		while true do (

			testname = lodprefix + (testindex as string)
			testindex = testindex + 1

			foundobj = getnodebyname testname exact:true

			if foundobj == undefined then exit
		)
		testname
	)
	
	--
	-- name: __FindValidObjects
	-- desc: Find valid objects from object list (return Array)
	--
	fn __FindValidObjects objects srcFilename = (
		
		validObjs = #()
		for obj in objects do
		(
			if ( XRefObject != classof obj ) then
				continue
			
			-- If the XRef has been reffed in from the same file as the
			-- LOD target file, then its a valid object.
			if ( 0 == ( stricmp obj.filename srcFilename ) ) then
			(
				append validObjs obj
			)
		)
		validObjs
	)
	
	--
	-- name: __DoAdjust
	-- desc:
	--
	fn __DoAdjust objects = (
		for obj in objects do (
				
			pos = obj.pos
			obj.pos = [0,0,0]
			newrot = RsTreeLodGenerator.__QuantizeQuaternionOnZAxis obj.rotation
			obj.rotation = newrot
			obj.pos = pos
		)			
	)
	
	--
	-- name: __QuantizeQuaternionOnZAxis
	-- desc:
	--
	fn __QuantizeQuaternionOnZAxis r = (
	
		d = 8.0
		dir = r.axis
		ang = r.angle
	
		angle = ang / 360.0
		angle = angle * d
		angle = floor( angle + 0.01 )
		angle = angle / d;
		angle = angle * 360.0;
	
		r = eulerToQuat (eulerangles 0 0 angle) order:1
		
		return r;
	)
	
	fn __CallbackDialogMonitorOPS = (
		WindowHandle = DialogMonitorOPS.GetWindowHandle()

		if ( UIAccessor.GetWindowText WindowHandle ) == "Duplicate Material Name" then 
		(
			UIAccessor.PressButtonByName WindowHandle "Auto-Rename Merged Material"
		)
		true
	)

	fn __RegisterDialogMonitorOPS = (
		DialogMonitorOPS.RegisterNotification __CallbackDialogMonitorOPS id:#treeLodGenerator	
		DialogMonitorOPS.Enabled = true
		DialogMonitorOPS.ShowNotification()
	)
	
	fn __UnregisterDialogMonitorOPS = (
		DialogMonitorOPS.Enabled = false
		DialogMonitorOPS.unRegisterNotification id:#treeLodGenerator	
	)
)

-- pipeline/util/treelodgenerator.ms
