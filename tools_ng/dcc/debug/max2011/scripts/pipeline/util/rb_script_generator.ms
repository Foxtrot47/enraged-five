-- Ruby script generator 
-- Rockstar North
-- 15/7/2008
-- by Luke Openshaw

include "rockstar/export/settings.ms"
include "pipeline/util/file.ms"


RS_CONTENT_ALL = 		0xFFFF
RS_CONTENT_DEFAULT = 		0x0007
RS_CONTENT_MAPS = 		0x0001
RS_CONTENT_PEDS = 		0x0002
RS_CONTENT_VEHICLES = 		0x0004
RS_CONTENT_ANIM = 		0x0010
RS_CONTENT_OUTPUT = 		0x0020
RS_CONTENT_PLATFORM = 		0x0040
RS_CONTENT_CLIP = 		0x0200

--
-- Description: Generic function for creating a resourcing script for content system
--
-- In: rubyfilename - outputfilename
-- In: projname - project name
-- In: contentname 
-- In: contenttype
--
fn RBGenResourcingScript rubyFileName projname rb_script contenttype =  (
	rubyFile = createfile rubyFileName--mode:"w+"

	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/projectutil/data_convert'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n\n" to:rubyFile

	projname = RsConfigGetProjectName()
	format "project = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile

	format "project.load_content(nil, %)\n" contenttype to:rubyfile
	format "content_list = project.content.find_by_script( \"%\" )\n\n" rb_script to:rubyFile

	format "convert = Pipeline::ConvertSystem.instance()\n" to:rubyfile
	format "convert.setup( project, nil, false, true, false)\n\n" to:rubyfile
	format "content_list.each do | content |\n" to:rubyfile 
	format "\tconvert.build(content)\n" to:rubyfile
	format "end\n" to:rubyfile

	format "content_list.each do |content|\n" to:rubyFile
	format "\tProjectUtil::data_convert_file( content.filename )\n" to:rubyFile
	format "end\n" to:rubyFile

	close rubyFile
)

--
-- Description: Generic function for creating a resourcing script for content system.  This should be used for all image generation
--
-- In: rubyfile - file object
-- In: projname - project name
-- In: imagename - output image name 
-- In: subdir - additional directory structure to append to the stream
--
fn RBGenDynamicResourcingScript rubyfile projname imagename subdir rebuild:false =  (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n" to:rubyFile

	format "project = Pipeline::Config.instance.projects[\"%\"]\n" projname to:rubyFile
	format "project.load_content(nil, Pipeline::Project::ContentType::OUTPUT | Pipeline::Project::ContentType::PLATFORM)\n" to:rubyFile
	format "image_name = \"%\"\n" imagename to:rubyFile

	format "ind_output_node = nil\n" to:rubyFile
	format "plat_output_list = []\n" to:rubyFile

	format "img_output_list = project.content.find_by_script(\"content.name == '\" + image_name + \"' and content.xml_type == 'image'\" )\n" to:rubyFile
	format "img_output_list.each do | img_output_item |\n" to:rubyFile
	format "\tif img_output_item.target == project.ind_target\n" to:rubyFile
	format "\t\tind_output_node = img_output_item\n" to:rubyFile
	format "\telse\n" to:rubyFile
	format "\t\tplat_output_list << img_output_item\n" to:rubyFile
	format "\tend\n" to:rubyFile
	format "end\n\n" to:rubyFile


	format "stream_dir = project.netstream\n" to:rubyFile
	format "stream_dir = Pipeline::OS::Path.combine(stream_dir, \"%\")\n" subdir to:rubyFile

	format "pack_file_list = Pipeline::OS::FindEx.find_files(stream_dir + \"/*.i*\", false)\n" to:rubyFile
	format "pack_file_list.each do | pack_file |\n" to:rubyFile
	format "\tind_output_node.add_input(Pipeline::Content::RPF.new( Pipeline::OS::Path.get_basename( pack_file ), Pipeline::OS::Path.get_directory( pack_file ), Pipeline::OS::Path.get_extension( pack_file ), project.ind_target ))\n" to:rubyFile
	format "end\n\n" to:rubyFile

	format "convert = Pipeline::ConvertSystem.instance()\n" to:rubyFile
	format "convert.setup( project, nil, false, %, false)\n" rebuild to:rubyFile
	format "convert.build(ind_output_node)\n" to:rubyFile
	format "plat_output_list.each do | map_output_item |\n" to:rubyFile
	format "\tconvert.build(map_output_item)\n\n" to:rubyFile

	format "end\n" to:rubyFile
)


--
-- name: RBGenDynamicRPFResourcingScript
-- desc: Generic function for creating a resourcing script for content system.  This should be used for all RPF generation
--
-- In: rubyfile - file object
-- In: projname - project name
-- In: imagename - output RPF name 
-- In: subdir - additional directory structure to append to the stream
--
fn RBGenDynamicRPFResourcingScript rubyfile projname imagename subdir rebuild:false =  (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n" to:rubyFile

	format "project = Pipeline::Config.instance.projects[\"%\"]\n" projname to:rubyFile
	format "project.load_content(nil, Pipeline::Project::ContentType::OUTPUT | Pipeline::Project::ContentType::PLATFORM)\n" to:rubyFile
	format "image_name = \"%\"\n" imagename to:rubyFile

	format "ind_output_node = nil\n" to:rubyFile
	format "plat_output_list = []\n" to:rubyFile

	format "img_output_list = project.content.find_by_script(\"content.name == '\" + image_name + \"' and content.xml_type == 'rpf'\" )\n" to:rubyFile
	format "img_output_list.each do | img_output_item |\n" to:rubyFile
	format "\tif img_output_item.target == project.ind_target\n" to:rubyFile
	format "\t\tind_output_node = img_output_item\n" to:rubyFile
	format "\telse\n" to:rubyFile
	format "\t\tplat_output_list << img_output_item\n" to:rubyFile
	format "\tend\n" to:rubyFile
	format "end\n\n" to:rubyFile


	format "stream_dir = project.netstream\n" to:rubyFile
	format "stream_dir = Pipeline::OS::Path.combine(stream_dir, \"%\")\n" subdir to:rubyFile

	format "pack_file_list = Pipeline::OS::FindEx.find_files(stream_dir + \"/*.i*\", false)\n" to:rubyFile
	format "pack_file_list.each do | pack_file |\n" to:rubyFile
	format "\tind_output_node.add_input(Pipeline::Content::RPF.new( Pipeline::OS::Path.get_basename( pack_file ), Pipeline::OS::Path.get_directory( pack_file ), Pipeline::OS::Path.get_extension( pack_file ), project.ind_target ))\n" to:rubyFile
	format "end\n\n" to:rubyFile

	format "convert = Pipeline::ConvertSystem.instance()\n" to:rubyFile
	format "convert.setup( project, nil, false, %, false)\n" rebuild to:rubyFile
	format "convert.build(ind_output_node)\n" to:rubyFile
	format "plat_output_list.each do | map_output_item |\n" to:rubyFile
	format "\tconvert.build(map_output_item)\n\n" to:rubyFile

	format "end\n" to:rubyFile
)

-- These should be temporary.  map content needs to be added as image inputs and only loaded
-- in resourcing scenarios
fn RBGenMapResourcingScriptPre rubyFile = (
	
	format "require 'pipeline/log/log'\n" to:rubyFile
	format "Pipeline::Config::instance()::logtostdout = true\n" to:rubyFile
	format "\n" to:rubyFile
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n\n" to:rubyFile
	format "require 'pipeline/content/content_map'\n\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n\n" to:rubyFile
	
	projname = RsConfigGetProjectName()
	format "project = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile
	
	format "project.load_content(nil, Pipeline::Project::ContentType::MAPS)\n" to:rubyfile
	format "map_name = \"%\"\n" RsMapName to:rubyfile
	
	-- Dont really need to do this, could just call RsIsGeneric but it keeps it contained
	format "map_content_node = project.content.find_first( map_name, \"map\" )\n\n" to:rubyFile
	format "generic = !map_content_node.exportinstances\n" to:rubyFile
	format "stream_dir = (false == generic) ? project.netstream : project.netgenstream\n" to:rubyfile
	format "stream_dir = Pipeline::OS::Path.combine(stream_dir, \"mapscript\", map_name)\n" to:rubyFile
	format "# Create a container for the map conponent content\n" to:rubyFile
	format "component_group = Pipeline::Content::Group.new(\"map_content_group\")\n\n" to:rubyFile
	format "print(\"Resourcing...\")\n" to:rubyFile 
  
)

-- These should be temporary.  map content needs to be added as image inputs and only loaded
-- in resourcing scenarios
fn RBGenGtxdResourcingScriptPre rubyFile = (
	
	format "require 'pipeline/log/log'\n" to:rubyFile
	format "Pipeline::Config::instance()::logtostdout = true\n" to:rubyFile
	format "\n" to:rubyFile
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n\n" to:rubyFile
	format "require 'pipeline/content/content_map'\n\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n\n" to:rubyFile
	
	projname = RsConfigGetProjectName()
	format "project = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile
	
	format "project.load_content(nil, Pipeline::Project::ContentType::MAPS)\n" to:rubyfile
	format "map_name = \"gtxd\"\n"  to:rubyfile
	
	-- Dont really need to do this, could just call RsIsGeneric but it keeps it contained
	format "stream_dir = project.netstream\n" to:rubyfile
	format "stream_dir = Pipeline::OS::Path.combine(stream_dir, \"mapscript\", map_name)\n" to:rubyFile
	format "# Create a container for the map conponent content\n" to:rubyFile
	format "component_group = Pipeline::Content::Group.new(\"map_content_group\")\n\n" to:rubyFile
	format "print(\"Resourcing...\")\n" to:rubyFile 
  
)

fn RBGenResourcingScriptPost rubyFile = (
	format "if component_group.children.size > 0\n" to:rubyFile
	format "\tconvert = Pipeline::ConvertSystem.instance()\n" to:rubyFile
	format "\tconvert.setup( project, nil, true, true, false)\n" to:rubyFile
	
	format "\tcomponent_group.children.each do | content_group |\n" to:rubyFile
	format "\t\tif content_group.methods.include?( 'build' )\n" to:rubyFile
	format "\t\t\tcontent_group.build()\n" to:rubyFile 
	format "\t\tend\n" to:rubyFile
	format "\tend\n" to:rubyFile
	
	format "\tconvert.build(component_group)\n" to:rubyFile 
	
	format "end\n\n" to:rubyFile 
)

fn RBGenResourcingScriptPatch rubyFile = (
	patch_dir = RsConfigGetPatchDir()
	RsMakeSurePathExists patch_dir
	
	format "target_content_group = Pipeline::Content::Group.new(\"patch_group\")\n" to:rubyFile
	format "Pipeline::Resourcing::create_target_content_from_indepenent( component_group, target_content_group, project, \"%\" )\n" patch_dir to:rubyFile
	format "convert.build(target_content_group)" to:rubyFile
	
)



fn RBGenPedResourcingScriptPre rubyFile = (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n\n" to:rubyFile
	format "require 'pipeline/content/content_ped'\n\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n\n" to:rubyFile
	
	projname = RsConfigGetProjectName()
	format "project = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile
	
	format "project.load_content(nil, Pipeline::Project::ContentType::PEDS)\n" to:rubyfile
	format "component_group = Pipeline::Content::Group.new(\"ped_content_group\")\n\n" to:rubyFile
)

-----------------------------------------------------------------------------
-- Misc Scripts
-----------------------------------------------------------------------------

-- Clip content generator
fn RBGenClipResourcingScriptPre rubyFile = (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n\n" to:rubyFile
	format "require 'pipeline/content/content_clip'\n\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n\n" to:rubyFile

	projname = RsConfigGetProjectName()
	format "project = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile

	format "project.load_content(nil, Pipeline::Project::ContentType::PEDS)\n" to:rubyfile
	format "component_group = Pipeline::Content::Group.new(\"ped_content_group\")\n\n" to:rubyFile
)

-- Anim content generator
fn RBGenAnimResourcingScriptPre rubyFile = (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n\n" to:rubyFile
	format "require 'pipeline/content/content_anim'\n\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n\n" to:rubyFile

	projname = RsConfigGetProjectName()
	format "project = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile

	format "project.load_content(nil, Pipeline::Project::ContentType::PEDS)\n" to:rubyfile
	format "component_group = Pipeline::Content::Group.new(\"ped_content_group\")\n\n" to:rubyFile
)

-- Cut prop content generator
fn RBGenCutPropResourcingScriptPre rubyfile = (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n\n" to:rubyFile
	format "require 'pipeline/content/content_cutscene'\n\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n\n" to:rubyFile
	
	projname = RsConfigGetProjectName()
	format "project = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile
	
	format "project.load_content(nil, Pipeline::Project::ContentType::PEDS)\n" to:rubyfile
	format "component_group = Pipeline::Content::Group.new(\"cutprops_content_group\")\n\n" to:rubyFile
)

-- Cut prop content generator
fn RBGenExpressionResourcingScriptPre rubyfile = (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n\n" to:rubyFile
	format "require 'pipeline/content/content_expression'\n\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n\n" to:rubyFile
	
	projname = RsConfigGetProjectName()
	format "project = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile
	
	format "project.load_content(nil, Pipeline::Project::ContentType::PEDS)\n" to:rubyfile
	format "component_group = Pipeline::Content::Group.new(\"expression_content_group\")\n\n" to:rubyFile
)


-----------------------------------------------------------------------------
-- Model Exporter Resourcing Scripts
-----------------------------------------------------------------------------

--
-- name: RBGenModelResourcingScriptPre
-- desc: 
--
fn RBGenModelResourcingScript rubyFile outdir objname isFrag = (

	projname = RsConfigGetProjectName()
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/config/project'\n" to:rubyFile
	format "require 'pipeline/content/content_core'\n" to:rubyFile
	format "require 'pipeline/gui/exception_dialog'\n" to:rubyFile
	format "require 'pipeline/os/file'\n" to:rubyFile
	format "require 'pipeline/projectutil/data_convert'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n" to:rubyFile
	format "require 'pipeline/util/string'\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n" to:rubyFile
	format "include Pipeline\n\n" to:rubyFile

	format "begin\n" to:rubyFile
	format "\tproject = Pipeline::Config.instance.projects[\"%\"]\n" projname to:rubyFile
	format "\tproject.load_config( )\n" to:rubyFile
	format "\tbranch = project.branches[project.default_branch]\n\n" to:rubyFile
	
	format "\tmodelname = '%'\n" objname to:rubyFile
	format "\tstream_dir = '%'\n" outdir to:rubyFile
	
	if ( isFrag ) then
	(
		format "\tift = Content::RPF.new( modelname, stream_dir, 'ift', branch.ind_target )\n" to:rubyFile
	)
	else
	(
		format "\tidr = Content::RPF.new( modelname, stream_dir, 'idr', branch.ind_target )\n" to:rubyFile
	)
	
	format "\tall_files = OS::FindEx::find_files( OS::Path::combine( '%/%', '*.*' ) )\n" outdir objname to:rubyFile
	format "\tall_files.each do |file|\n" to:rubyFile
	format "\t\tputs \"Adding: #{file}\"\n" to:rubyFile
	
	if ( isFrag ) then
	(
		format "\t\tift.inputs << Content::File::from_filename( file )\n" to:rubyFile
	)
	else
	(
		format "\t\tidr.inputs << Content::File::from_filename( file )\n" to:rubyFile
	)
	
	format "\tend\n" to:rubyFile
	
	format "component_group = Pipeline::Content::Group.new(\"map_content_group\")\n\n" to:rubyFile
	if ( isFrag ) then
		format "\tcomponent_group.children << ift\n" to:rubyFile
	else
		format "\tcomponent_group.children << idr\n" to:rubyFile

	RBGenResourcingScriptPost rubyfile

	format "rescue Exception => ex\n" to:rubyFile
	format "\tPipeline::GUI::ExceptionDialog.show_dialog( ex )\n" to:rubyFile
	format "\texit( 1 )\n" to:rubyFile
	format "end\n" to:rubyFile
)

