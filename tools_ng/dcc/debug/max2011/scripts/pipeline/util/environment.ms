--
-- environment.ms
-- Project Environment Table Class
--
-- MaxScript equivalent for the Ruby Environment Class to allow us to parse
-- project configuration files and variables within 3dsmax.
--
-- David Muir <david.muir@rockstarnorth.com>
-- 8 February 2008
--

--
-- name: Environment
-- desc: Environment table class for project configuration variables.
--
struct Environment
(

	-------------------------------------------------------------------------
	-- Private Data
	-------------------------------------------------------------------------
	
	__variables = #(),
	__values = #(),
	
	-------------------------------------------------------------------------
	-- Public Methods
	-------------------------------------------------------------------------

	--
	-- name: lookup
	-- desc: Return the value of the specified environment variable.
	--	
	fn lookup varname = (
	
		local index = ( findItem __variables varname )
		result = undefined
		
		if ( 0 != index ) then
			result = __values[index]
			
		result
	),
	
	--
	-- name: subst
	-- desc: Return a new string with any environment variables having their
	--       actual value substituted in place.
	--	
	fn subst str = (
	
		if ( undefined == str ) then
			return ( "" )
	
		result = str
		for i = 1 to __variables.count do
		(
		
			local varss = stringStream ""
			format "$(%)" __variables[i] to:varss
			
			local var = ( varss as string )
			local index = ( findString result var )
			
			if ( undefined != index ) then
				result = ( replace result index var.count __values[i] ) 
		)
		
		result
	),
	
	--
	-- name: add
	-- desc: Add a new or replace an existing, environment variable 
	--       definition in our symbol table.
	--	
	fn add varname val = (
	
		append __variables varname
		append __values val
		
		true
	),
	
	--
	-- name: clear
	-- desc: Clear all environment variables.
	--
	fn clear = (

		__variables = #()
		__values = #()
		
		true
	),

	--
	-- name: count
	-- desc: Return number of variables we have currently defined.
	--	
	fn count = (
	
		__variables.count
	),

	--
	-- name: print
	-- desc: Print listing of symbol table to MaxScript Listener.
	--	
	fn print = (
	
		format "Environment Listing:\n"
		format "--------------------\n"
		for i = 1 to __variables.count do
		(
			format "% => %\n" __variables[i] __values[i]
		)
	)	
)

-- End of environment.ms
