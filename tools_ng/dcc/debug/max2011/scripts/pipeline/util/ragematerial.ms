-- Rockstar Material Functions
-- Rockstar North
-- 8/5/2009
-- by Luke Openshaw

-- utilities for DX rage shader materials

include "rockstar/util/material.ms"

-----------------------------------------------------------------
-- Build a list of materials that are applied to an array of objects
-----------------------------------------------------------------
fn RsGetMaterialsAppliedToObjects objs = (
	matlist = #()
	for obj in objs do (
		if classof obj.material == Rage_Shader then append matlist obj.mat
		else if classof obj.material == MultiMaterial then (
			for submat in obj.material.materiallist do (
				if classof submat == Rage_Shader then append matlist submat
			)
		)
	)
	return matlist
)

-----------------------------------------------------------------
-- Convert 
-----------------------------------------------------------------
fn RsConvertStdMaterialsOnSelected objs = (
	matlist = #()
	for obj in objs do (
		if classof obj.material == Standardmaterial then (
			newMat = RstCreateRstMtlFromStdMtl obj.mat
			if newmat != undefined then obj.mat = newMat
		)
		else if classof obj.material == MultiMaterial then (
			idx = 1
			mapList = #()
			RsMatGetMapIdsUsedOnObjectWithCount obj mapList
			for submat in obj.material.materiallist do (
				if classof submat == Standardmaterial then (
					if (finditem mapList idx) != 0 then (
						newMat = RstCreateRstMtlFromStdMtl submat
						if newMat != undefined then obj.material.materiallist[idx] = newMat
					)
					else print ("id "+(idx as string)+" is not used on object")
				)
				else print ("number "+(idx as string)+" is not a standardmaterial")
				
				idx = idx + 1
			)
		)
	)
)

-----------------------------------------------------------------
-- Build a list of materials that are applied to selected faces
-----------------------------------------------------------------
fn RsGetMaterialsAppliedToFaces = (
	matlist = #()
	if selection.count != 1 then (
		messagebox "Multiple or no objects selected"
		return false
	)

	obj = selection[1]
	mat = obj.mat


	-- If the object only has one material applied to it then that is the only
	-- material we need to operate on.  Othersiwe work out which materials
	-- in the MultiMaterial need to have the textures scaled
	if classof mat == Rage_Shader then append matlist mat
	else if classof mat == MultiMaterial then (
		matidlist = #() -- List of participant material ids
		polysel = polyop.getFaceSelection obj

		for polyidx in polysel do (
			matidx = polyop.getFaceMatID obj polyidx
			if finditem matidlist matidx == 0 then append matidlist matidx
		)

		for matidx in matidlist do (
			append matlist mat.materiallist[matidx]
		)
	)
	return matlist
)

-----------------------------------------------------------------
-- Toggle lighting on array of objects
-----------------------------------------------------------------
fn RsToggleDxLightingOnObjs objs val = (	
	progressStart("Toggling lighting on objects")
	matNum = 1
	matlist = RsGetMaterialsAppliedToObjects objs
	
	for mat in matlist do (
		
		RstSetLightingMode mat val		
		progressUpdate(100.0 * matNum / matlist.count)
		matNum = matNum + 1
	)		
	messagebox( "Finished setting lighting flag on " + (matlist.count as string) + " materials.")	
	progressEnd()
)

-----------------------------------------------------------------
-- Toggle lighting on array of faces
-----------------------------------------------------------------
fn RsToggleDxLightingOnFaces val = (	
	progressStart("Toggling lighting on faces")
	matNum =1 
	matlist = RsGetMaterialsAppliedToFaces()
	
	for mat in matlist do (
		
		RstSetLightingMode mat val		
		progressUpdate(100.0 * matNum / matlist.count)
		matNum = matNum + 1
	)	
	messagebox( "Finished setting lighting flag on " + (matlist.count as string) + " materials.")
	progressEnd()	
)

-----------------------------------------------------------------
-- Scale textures on selected object materials
-----------------------------------------------------------------
fn RsScaleTexuresOnSelectedObjs multiplier objs = (
	progressStart("Scaling textures on objects")
	matNum = 1 	
	matlist = RsGetMaterialsAppliedToObjects objs
	
	for mat in matlist do (
		
		RstSetTextureScaleValue mat multiplier		
		progressUpdate(100.0 * matNum / matlist.count)
		matNum = matNum + 1
	)	
	messagebox( "Finished scaling textures on " + (matlist.count as string) + " materials.")
	progressEnd()
)

-----------------------------------------------------------------
-- Scale textures on selected faces materials
-----------------------------------------------------------------
fn RsScaleTexuresOnMaterialsForSelectedFaces multiplier = (
	progressStart("Scaling textures on faces")
	matNum = 1 	
	matlist = RsGetMaterialsAppliedToFaces()
	
	for mat in matlist do (
	
		RstSetTextureScaleValue mat multiplier		
		progressUpdate(100.0 * matNum / matlist.count)
		matNum = matNum + 1
	)	
	messagebox( "Finished scaling textures on " + (matlist.count as string)+ " materials.")
	progressEnd()
)


-- Mark HB's more solid method of traversing the scene materials
fn updatemesh list hw =

(
	
	for mat in list do (
		
		if hw then (
			enablehardwarematerial mat on
			RstSetHwRenderMode mat true
		)
		else (
			enablehardwarematerial mat off
			RstSetHwRenderMode mat false
		)
	)
	--if hw then enablehardwarematerial list on

	--else enablehardwarematerial list off

)

 

fn RsToggleHardwareRendering hw = (

            

	shaderList = for i in getClassInstances rage_shader collect (i)

	updatemesh shaderList hw
	forceCompleteRedraw()

)


