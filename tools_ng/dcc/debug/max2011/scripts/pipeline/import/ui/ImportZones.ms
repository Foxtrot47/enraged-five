--
-- File:: pipeline/import/ui/ImportZones.ms
-- Description:: Import Zones
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- 
-- Based off of the original by Greg Smith.
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/zones.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

rollout RsZoneImporterRoll "Zone Importer"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Export/Import_Zones#Import_Zones" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnExportPopZones	"Import POPZONES" 	width:120
	button btnExportMapZones	"Import MAPZONES" 	width:120
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------

	--
	-- event: btnImportPopZones pressed
	-- desc: 
	--
	on btnExportPopZones pressed do (
		RsMapImportPopZoneFile ( getOpenFilename caption:"Zone IPL files" types:"Zone IPL Files (*.ipl)|*.ipl" )
	)
	
	--
	-- event: btnIMportMapZones pressed
	-- desc: 
	--
	on btnExportMapZones pressed do (
		RsMapImportMapZoneFile ( getOpenFilename caption:"Zone IPL files" types:"Zone IPL Files (*.ipl)|*.ipl" )
	)
)

-- ImportZones.ms
