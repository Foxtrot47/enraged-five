nodefilename = getOpenFileName caption:"node file to open" filename:"x:/chase.xml" types:"debug node file (*.xml)|*.xml"

nodefile = XmlDocument()    
nodefile.init()
nodefile.load nodefilename

_rootnode = nodefile.document.DocumentElement

nodelist = #()

s = line()

last_value = -1
idx = undefined

childNodeList = _rootnode.ChildNodes
for nChild = 0 to ( childNodeList.Count - 1 ) do
(
	local _groupnode = childNodeList.ItemOf( nChild )
	if ( "nodes" == _groupnode.name ) then
	(
		local childNodeBList = _groupnode.ChildNodes
		for nChild = 0 to ( childNodeBList.Count - 1 ) do
		(

			local elem = childNodeBList.ItemOf( nChild )
			if ( "node" == elem.name ) then
			(
				local item_title = elem.Attributes.ItemOf("title")
				local item_id = elem.Attributes.ItemOf("id")
				local item_heading = elem.Attributes.ItemOf("heading")
				local item_x = elem.Attributes.ItemOf("x")
				local item_y = elem.Attributes.ItemOf("y")
				local item_z = elem.Attributes.ItemOf("z") 
				
				local value_text = elem.innertext as string
				local value_x = item_x.value as float
				local value_y = item_y.value as float
				local value_z = item_z.value as float
				local value_id = item_id.value as integer
				
				if value_text != "" then (
				
					t = RsTextNode()
					t.pos =  [value_x,value_y,value_z]
					t.description = value_text
				)
				
				nodelist.count = value_id + 1
				nodelist[value_id + 1] = [value_x,value_y,value_z]
			)
		)
	)
	else if ( "links" == _groupnode.name ) then
	(
		local childNodeBList = _groupnode.ChildNodes
		for nChild = 0 to ( childNodeBList.Count - 1 ) do
		(

			local elem = childNodeBList.ItemOf( nChild )
			if ( "link" == elem.name ) then
			(
				local item_start = elem.Attributes.ItemOf("startnode")
				local item_end = elem.Attributes.ItemOf("endnode")
				local value_start = item_start.value as integer
				local value_end = item_end.value as integer
				
				point_start = nodelist[value_start + 1]
				point_end = nodelist[value_end + 1]
				
				if (point_start != undefined) and (point_end != undefined) then (
				
					if last_value != point_start then
					
						idx = addNewSpline s		
						addKnot s idx #smooth #curve point_start								
					end
					
					addKnot s idx #smooth #curve point_end
					last_value = point_end
				)
			)
		)
	)
)

updateShape s