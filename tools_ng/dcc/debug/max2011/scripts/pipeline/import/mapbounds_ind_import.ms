--
-- File:: mapbounds_ind_import.ms
-- Description:: Map bounds independent file (.bnd) file importer.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 28 October 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
RsMapBoundsVersion = "1.10"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsPlaceHelpersAtSSVerts
-- desc: place heleprs where the second surface verts are
--
fn RsPlaceHelpersAtSSVerts verts fs = (
	
	for i=1 to fs.count do (
		
		if fs[i] != 0 then (
			
			hpos = verts[i]
			hpos.z = hpos.z + fs[i]
			h = Dummy()
			h.boxsize = boxsize = [.5,.5,.5]
			h.pos = hpos
		)
		
	)
)

--
-- name: RsMapBoundsImportSingleBound
-- desc:
--
fn RsMapBoundsImportSingleBound fstream filename = (
	
	verts = #()
	tris = #()
	fs = #()
	
	-- Parse file line-by-line
	data = readline fstream
	data = filterstring data " "
	
	while ( not eof fstream ) and ( data[1] != "bound:" ) do (	
		
		if ( data[1] == "v" ) then
			append verts [ data[2] as float, data[3] as float, data[4] as float ]
		
		else if ( data[1] == "tri" ) then
		(
			v1 = ( data[2] as integer ) + 1
			v2 = ( data[3] as integer ) + 1
			v3 = ( data[4] as integer ) + 1
			
			append tris [ v1, v2, v3 ]
		)
		else if ( data[1] == "quad" ) then
		(
			v1 = ( data[2] as integer ) + 1
			v2 = ( data[3] as integer ) + 1
			v3 = ( data[4] as integer ) + 1
			v4 = ( data[5] as integer ) + 1
		
			append tris [ v1, v2, v3 ]
			append tris [ v1, v3, v4 ]
		)
		else if ( data[1] == "f" ) then
		(
			
			append fs ( data[2] as float )
		)
		
		-- Read next line
		data = readline fstream
		data = filterstring data " "
	)
	format "Bound: % verts, % tris\n" verts.count tris.count
	
	coll = mesh vertices:verts faces:tris
	
	if querybox "Do you want to import second surface data?" == true then RsPlaceHelpersAtSSVerts verts fs
	
	coll.name = filename
	return ( coll )
)

--
-- name: RsMapBoundsImport
-- desc:
--
fn RsMapBoundsImport filename = (

	if ( undefined == filename ) then
		return ( false )
		
	file = openFile filename
	if ( undefined == file ) then
		return ( false )
	
	colls = #()
	
	-- Parse file line-by-line
	while not eof file do (
	
		data = readline file
		data = filterstring data " "
		
		coll = RsMapBoundsImportSingleBound file filename
		append colls coll
	)
	
	select colls
	
	close file
	return ( true )
)

-- mapbounds_ind_import.ms
