
project = RsConfigGetActiveProject()
RsConfigLoadProject project
if ( project.count > 0 ) then
(
	format "Username     : %\n" ( RsConfigGetUsername() )
	format "Usertype     : %\n" ( RsConfigGetUsertype() )
	format "Email        : %\n" ( RsConfigGetEmailAddress() )
	format "\n\n"
	format "Active Proj  : %\n" ( RsConfigGetActiveProject() )
	format "Projects     : %\n" ( RsConfigGetProjects() )
	format "\n\n"
	format "Win32 Enabled: %\n" ( RsConfigGetTargetEnabled "win32" )
	format "360 Enabled  : %\n" ( RsConfigGetTargetEnabled "xbox360" )	
	format "PS3 Enabled  : %\n" ( RsConfigGetTargetEnabled "ps3" )
	format "\n\n"	
	format "UIName       : %\n" ( RsConfigGetProjectUIName() )
	format "Root         : %\n" ( RsConfigGetProjectRootDir() )
	format "Local        : %\n" ( RsConfigGetProjectLocalDir() )
	format "Network      : %\n" ( RsConfigGetProjectNetworkDir() )
	format "Bin          : %\n" ( RsConfigGetProjectBinDir() )
	format "Build        : %\n" ( RsConfigGetProjectBuildDir() )
	format "Independent  : %\n" ( RsConfigGetProjectIndependentDir() )
	format "Common       : %\n" ( RsConfigGetProjectCommonDir() )
	format "Shaders      : %\n" ( RsConfigGetProjectShadersDir() )
)
else
(
	MessageBox "Error - active project is invalid."
)


-- Try Jimmy...
project = "jimmy"
RsConfigLoadProject project
format "Win32 Enabled: %\n" ( RsConfigGetTargetEnabled "win32" )
format "360 Enabled  : %\n" ( RsConfigGetTargetEnabled "xbox360" )	
format "PS3 Enabled  : %\n" ( RsConfigGetTargetEnabled "ps3" )
format "\n\n"	
format "UIName       : %\n" ( RsConfigGetProjectUIName project )
format "Root         : %\n" ( RsConfigGetProjectRootDir project )
format "Local        : %\n" ( RsConfigGetProjectLocalDir project )
format "Network      : %\n" ( RsConfigGetProjectNetworkDir project )
format "Bin          : %\n" ( RsConfigGetProjectBinDir project )
format "Build        : %\n" ( RsConfigGetProjectBuildDir project )
format "Independent  : %\n" ( RsConfigGetProjectIndependentDir project )
format "Common       : %\n" ( RsConfigGetProjectCommonDir project )
format "Shaders      : %\n" ( RsConfigGetProjectShadersDir project )



rollout RsFakeRollout "Fake Exporter"
(

	-------------------------------------------------------------------------
	-- Widgets
	-------------------------------------------------------------------------
	
	button 	btnCreateSkinned 		"Create Skinned" 		width:150
	button	btnAddLimits			"Add Limits" 			width:150
	button 	btnExportSkinned 		"Export Skinned" 		width:150
	button 	btnExportSkinnedBatch	"Batch Export Skinned" 	width:150
	button 	btnBuildImage 			"Build Image" 			width:150
	button 	btnReBuildImage 		"Rebuild Image" 		width:150
	button  btnEditTextures			"Edit Shared Textures"	width:150
	
	-------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------

	on btnCreateSkinned pressed do
	(
		VehicleCreateSkinned()
	)

	on btnAddLimits pressed do
	(
		VehicleAddLimits()
	)

	on btnExportSkinned pressed do
	(
		errors = VehicleExportSkinned()
		
		if ( undefined != errors and errors.count > 0 ) then
		(
			roll = RsCreateExporterErrorDialog "RsVehicleErrorRollout" "Vehicle Exporter Errors / Warnings" errors
			CreateDialog roll with:600 model:true
		)
	)
	
	on btnExportSkinnedBatch pressed do
	(
		errors = VehicleExportSkinnedBatch()
		
		if ( undefined != errors and errors.count > 0 ) then
		(
			roll = RsCreateExporterErrorDialog "RsVehicleErrorRollout" "Vehicle Batch Exporter Errors / Warnings" errors
			CreateDialog roll width:600 modal:true	
		)
	)

	on btnBuildImage pressed do
	(
		VehicleBuildImage()
	)
	
	on btnReBuildImage pressed do
	(
		VehicleBuildImage rebuild:true
	)

	on btnEditTextures pressed do
	(
		try CloseRolloutFloater floaterRollout catch()
		floaterRollout = newRolloutFloater "Vehicle Shared Textures" 250 500
		
		addRollout ( RsCreateSharedTextureListRollout "RsVehicleSharedTexRoll" "Shared Textures" RsConfig.activeProject.vehicleconf.shared_textures.filename ) floaterRollout
		addRollout ( RsCreateSharedTextureListRollout "RsVehicleSharedTruckTexRoll" "Shared Truck Textures" RsConfig.activeProject.vehicleconf.shared_truck_textures.filename ) floaterRollout
		addRollout ( RsCreateSharedTextureListRollout "RsVehicleVariationTexRoll" "Variation Textures" RsConfig.activeProject.vehicleconf.variation_textures.filename ) floaterRollout
		addRollout RsVehicleSharedTextureRoll floaterRollout
	)
)


-----------------------------------------------------------------------------
-- Entry-Point
-----------------------------------------------------------------------------

try CloseRolloutFloater RsFakeUtil catch()
RsFakeUtil = newRolloutFloater "RS Fake Exporter" 250 500 50 126
addRollout RsExportConfigRollout RsFakeUtil
addRollout RsFakeRollout RsFakeUtil


