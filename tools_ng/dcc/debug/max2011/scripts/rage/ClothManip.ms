-- Cloth manipulator
-- Written by Kevin Rose

-- This "manipulator" doesn't actually manipulate anything, it is just a way
-- to view the cloth values on each vertex

plugin simpleManipulator clothManip
    name:"ClothManip"
    invisible:true
(
    -- This manipulator manipulates any node with a "length" property
    on canManipulateNode node do
	(
		if( RAGEClothEditorFloater.open == false ) then
		(
			print("Open the cloth editor to view values")
			return false
		)
		
		return ((classof node.baseObject == Editable_Mesh) or (classof node.baseObject == Editable_Poly))
	)

    -- Create the manipulator gizmo.
    -- This is called initially and whenever the manipulator target changes
    on updateGizmos do
    (
        -- Clear the current gizmo cache
        this.clearGizmos()
		if( RAGEClothEditorFloater.open == false ) then
		(
			print("Open the cloth editor to view values")
			return false
		)

		iSelectedChannel = -1
		aobRollouts = RAGEClothEditorFloater.rollouts
		for r = 1 to aobRollouts.count do
		(
			obRollout = aobRollouts[r]
			-- print( "obRollout.title = "+ obRollout.title )
			if( obRollout.title == "Cloth Channel View" ) then
			(
				-- Found the roll out, so get the channel to view
				for i = 1 to obRollout.controls.count do
				(
					-- print obRollout.controls[i].caption
					if( obRollout.controls[i].caption == "Cloth Tuning Channel" ) then
					(
						-- Got the drop down, so find which channel is selected
						channelDrop = obRollout.controls[i]
						iSelectedChannel = channelDrop.selection
					)
				)
			)
		)
		
		-- print("iSelectedChannel = "+ (iSelectedChannel as string))
		if(iSelectedChannel == -1) then
		(
			return false
		)
		iDataChannel = RAGEClothChannelTable[iSelectedChannel].vDataChn
		
		-- Get number of verts
		iNumOfVerts = getNumVerts target
		-- print("iNumOfVerts = "+ iNumOfVerts as string)
        -- local flags = (gizmoUseScreenSpace + gizmoActiveViewportOnly)
        local flags = 0

		if classof target == Editable_mesh then 
		(
			for i = 1 to iNumOfVerts do
			(
				in coordsys local vertPos = meshop.getVert target i
				-- vertPos = target.getVertex (target.getFaceVertex i 1)
				-- print vertPos
				if ( meshop.getVDataChannelSupport target iDataChannel ) then
				(
					-- Get value
					fPinValue = meshop.getVDataValue target iDataChannel i

					-- Calc color
					fR = 0.0
					fB = 0.0
					if(fPinValue < 0.5) then
					(
						fR = fPinValue * 2.0
						fB = 1.0
					)
					else
					(
						fR = 1.0
						fB = 1.0 - ((fPinValue - 0.5) * 2.0)
					)
					
					-- Add text
					this.addGizmoText (fPinValue as string) vertPos flags [fR,0,fB] [fR,0,fB]

					-- Add point
			        this.addGizmoMarker #smallCircle vertPos flags [fR,0,fB] [fR,0,fB]
				)
				else
				(
					this.addGizmoText "n/a" vertPos flags [1,1,1] [1,1,1]
			        this.addGizmoMarker #smallCircle vertPos flags [1,1,1] [1,1,1]
				)
			)
		)
		else if classof target == Editable_poly then 
		(
			for i = 1 to iNumOfVerts do
			(
				in coordsys local vertPos = polyop.getVert target i
				-- vertPos = target.getVertex (target.getFaceVertex i 1)
				-- print vertPos
				if ( polyop.getVDataChannelSupport target iDataChannel ) then
				(
					-- Get value
					fPinValue = polyop.getVDataValue target iDataChannel i

					-- Calc color
					fR = 0.0
					fB = 0.0
					if(fPinValue < 0.5) then
					(
						fR = fPinValue * 2.0
						fB = 1.0
					)
					else
					(
						fR = 1.0
						fB = 1.0 - ((fPinValue - 0.5) * 2.0)
					)
					
					-- Add text
					this.addGizmoText (fPinValue as string) vertPos flags [fR,0,fB] [fR,0,fB]

					-- Add point
			        this.addGizmoMarker #smallCircle vertPos flags [fR,0,fB] [fR,0,fB]
				)
				else
				(
					this.addGizmoText "n/a" vertPos flags [1,1,1] [1,1,1]
			        this.addGizmoMarker #smallCircle vertPos flags [1,1,1] [1,1,1]
				)
			)
		)
		
        this.addGizmoText RAGEClothChannelTable[iSelectedChannel].userName [0,0,0] flags [1,1,1] [1,1,1]

        -- return the ToolTip string
        return node.name 
    )
)
