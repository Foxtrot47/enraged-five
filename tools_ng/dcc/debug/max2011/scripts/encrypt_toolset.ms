-- Recursive Maxscript Encrypt
-- Rockstar North
-- 30/04/2010
-- by Luke Openshaw

-- encryption functions

global gScriptFileList = #()

------------------------------------------------------------------------------
-- Recure through all subdirs filling maxscript array
------------------------------------------------------------------------------
fn BuildScriptListRecursive scriptDir = (
	scripts = getFiles (scriptDir + "/*.ms")
	for s in scripts do (
		append gScriptFileList s 
	)

	dirs = getDirectories (scriptDir + "/*")
	for dir in dirs do (
		BuildScriptListRecursive dir
	)
)

------------------------------------------------------------------------------
-- Build array of scripts
------------------------------------------------------------------------------
fn BuildScriptList scriptsRoot = (
	--scriptsRoot = getdir #scripts
	dirs = getDirectories (scriptsRoot + "/*")

	for d in dirs do (
		BuildScriptListRecursive d
	)
)

------------------------------------------------------------------------------
-- Root script for deleteing encrypted copies
------------------------------------------------------------------------------
fn FlushUnencryptedScripts = (
	for s in gScriptFileList do (
		s = RsMakeSafeSlashes s
		print s
		deletefile s
	)
)


------------------------------------------------------------------------------
-- Root dir for encrypting all maxscripts in toolchain
------------------------------------------------------------------------------
fn EncryptScripts = (
	for s in gScriptFileList do (
		
		encryptScript s
	)
)

------------------------------------------------------------------------------
-- Copy files from current to outsource toolset. UNUSED!
------------------------------------------------------------------------------
fn CopyEncryptedFiles = (
	scriptsRoot = getdir #scripts
	currentString = "current"
	currentIdx = findstring scriptsRoot currentString 
	
	for s in gScriptFileList do (
		fullFilePath = s + "e"
		outsourceFilePath = replace fullFilePath currentIdx currentString.count "outsource"
		RsMakeSurePathExists outsourceFilePath
		print ("Copying: " + outsourceFilePath)
		copyfile fullFilePath outsourceFilePath
	)
)


scriptsRoot = getdir #scripts
currentString = "current"
currentIdx = findstring scriptsRoot currentString 
outsourceScriptPath = replace scriptsRoot currentIdx currentString.count "outsource"

BuildScriptList outsourceScriptPath
EncryptScripts()
CopyEncryptedFiles()
FlushUnencryptedScripts()