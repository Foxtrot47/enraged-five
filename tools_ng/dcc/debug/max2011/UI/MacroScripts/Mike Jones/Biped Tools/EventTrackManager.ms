
--//////////////////////////////////////////////////////////////////////////////////
-- Event Track Manager. Mike Jones.
--
-- Allows user to alter event tracks assigned to a biped.
-- Select biped part and run script.
--
--//////////////////////////////////////////////////////////////////////////////////
		
-- list of notetracks
NoteTrackNames = #(
						"Event_Track",
						"AmbientFlags_Track",
						"Audio_Track",
						"GestureSpeakerTrack",
						"GestureListenerTrack",
						"FacialSpeakerTrack",
						"FacialListenerTrack",
						"Viseme_Track"
				)

-- list of all SW Events - NB this gets filled in via a file load
FullEventList = #()

-- SW Events File path
EventsFilePath = ((getDir #maxroot) + "UI\MacroScripts\\Mike Jones\\Biped Tools\\CodeEvents.txt")
		
-- list of all Ambient Flags - NB this gets filled in via a file load
FullFlagList = #()
 
--Ambient Flags File path
AmbientFlagsFilePath = ((getDir #maxroot) + "UI\MacroScripts\\Mike Jones\\Biped Tools\\AmbientFlags.txt")
			
-- Audio Events - NB this gets filled in via a file load
AudioEvents = #()

--Audio Event File path
AudioEventFilePath = ((getDir #maxroot) + "UI\MacroScripts\\Mike Jones\\Biped Tools\\AudioEvents.txt")

-- Facial Events
FacialEvents = #()

-- Facial Events Directory
FacialEventDir = "X:\\gta\\gta_art\\anim\\export\\FACIALS@"

--//////////////////////////////////////////////////////////////////////////////////

function loadEvents eventArray filepath =
(
	--open Events file
	filehandle = openFile filepath 
	count = 1
		
	--read in each Event and assign to array
	while ((eof filehandle) != true) do
	(
		eventArray[count] = readline filehandle
		count += 1
	)
	
	--close file
	close filehandle
)

fn getFacialAnims FacialEvents FacialEventDir =
(
	files = #()
	tmp_array = #()
	
	--get directories
	dir_array = GetDirectories (FacialEventDir + "/*")
	
	for d in dir_array do
	(
		join dir_array (GetDirectories (d+"/*"))
	)
	
	--get all files in dirs 
	for f in dir_array do
	(
		join files (getFiles (f + "\\*.anim"))
	)
	
	--remove any duplicates
	for k = 1 to files.count do
	(
		files[k] = getFilenameFile files[k] 
		
		duplicate = false
		
		for m = 1 to tmp_array.count do
		(
			if tmp_array[m] == files[k] then duplicate = true
		)
		
		if duplicate == false then tmp_array[tmp_array.count+1] = files[k]
	)
	
	--set up facial events UI
	for i = 1 to tmp_array.count do FacialEvents[i] = tmp_array[i]

)

function getEventTracks object tracknum = 
-- function returns a string[] which contains all currently assigned biped note events
-- tracknum refers to which notetrack to get i.e. 1 is Event tracks, 3 is Audio events etc.
(
	noteTrackList = #()
	
	track = getNoteTrack object tracknum
	
	sortNoteKeys track
	keyarray = track.keys
	
	count =1
									
	for key in keyarray do
	(
			tmp = "\" "
			
			if (tracknum == 1) then 
			--only do this for code events not audio events
			(
				--convert bitfield total into respective string in FullEventList
				event_selection = key.value
				tmp_str = ""
				
				for i = 1 to 32 do
				(
					if (bit.get (event_selection as integer) i) == true then
					(
						tmp_str += FullEventList[i]
						tmp_str += " "
					)
				)
				
				if (event_selection as integer == 0) then tmp_str = "0"
				
				tmp += tmp_str
			)
			
			else tmp += key.value
			
			tmp = trimright tmp
			tmp += " \" on frame "
			
			-- get time of key
			time = (getNoteKeyTime track count as string)
			-- trim off annoying 'f' char which is returned from time
			time = trimRight time "f"
			tmp += time
			
			noteTrackList[count] = tmp
			-- next event
			count += 1
	)

	-- return string[]
	return noteTrackList
)

fn isBitSet tmp_key_value selectedNote =
(
	if (bit.get tmp_key_value selectedNote) == true then return true else return false
)

--//////////////////////////////////////////////////////////////////////////////////
-- Ambient Flag Fns - set up UI correctly on a BIP load
--//////////////////////////////////////////////////////////////////////////////////


fn getFlagRadioSelection bipname = 
(
	track = getNoteTrack bipname 2
	keys = track.keys
	if keys.count == 0 then return 2
	if keys[1].value == "0" then return 2 else return 1
)

fn getFlagBoxEnabled bipname = 
(
	track = getNoteTrack bipname 2
	keys = track.keys
	if keys.count == 0 then return false
	if keys[1].value == "0" then return false else return true
)

fn getFlagBoxSelection bipname = 
(	
	--array to store user selection
	bitArray = #{}

	track = getNoteTrack bipname 2
	keys = track.keys
	if keys.count == 0 then return bitArray
	if keys[1].value == "0" then return bitArray

	flagselection = keys[1].value

	for i = 1 to 32 do
	(
		if (bit.get (flagselection as integer) i) == true then
		(
			append bitArray i
		)
	)
	
	return bitArray
)

fn doesStringContainLetters string =
(
	letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	for i = 1 to string.count do
	(
		result = findstring letters string[i] 
		if result != undefined then
		(
			return true 
		)
	)
	return false
)

function createNoteTracks object =
(
	--////////////////////////////////////////////////////////
	--copy any existing notekeys, for backwards compatability
	--////////////////////////////////////////////////////////
	existingkeys = #()
	
	num_tracks = numNoteTracks object

	for i = 1 to num_tracks do
	(
		track = getNoteTrack object i 
		--copy any keys
		existingkeys[i] = track.keys
	)
				
	--////////////////////////////////////////////////////////
	--delete any existing note tracks
	--////////////////////////////////////////////////////////
	
	num_tracks = numNoteTracks object

	for i = 1 to num_tracks do
	(
		track = getNoteTrack object 1 
		--delete notetracks
		deleteNoteTrack object track
	)
	--////////////////////////////////////////////////////////
	--create note tracks
	--////////////////////////////////////////////////////////

	for i = 1 to NoteTrackNames.count do
	(
		track = notetrack NoteTrackNames[i]
		addNoteTrack object track
	)
	
	--////////////////////////////////////////////////////////
	--paste any existing keys back on to new notetracks
	--////////////////////////////////////////////////////////
	
	for i = 1 to existingkeys.count do
	(
		track = getNoteTrack object i
		
		-- get key array from old notetrack
		oldkeys = existingkeys[i]
		newKeyTrack = track.keys
		
		local newkey
		
		for j = 1 to oldkeys.count do
		-- for each key in array paste back into new not track
		(
			newkey = addnewnotekey newKeyTrack oldkeys[j].time
			newkey.value = oldkeys[j].value
		)
		
		--//////////////////////////////////////////////////////////////////////////////////
		--check events for older versions 
		--convert any old style string based events to bit selection
		--//////////////////////////////////////////////////////////////////////////////////
		if ((i == 1) and (oldkeys.count > 0)) then
		(
			eventKeys = track.keys
						
			for j = 1 to eventKeys.count do
			(
				if ((doesStringContainLetters eventKeys[j].value) == true) then 
				(
					str_array = filterString eventKeys[j].value " "
					bit_total = 0
					tmp_array = #()
					
					--first remove any duplicates sitting on the same frame
					for k = 1 to str_array.count do
					(
						duplicate = false
						
						for m = 1 to tmp_array.count do
						(
							if tmp_array[m] == str_array[k] then duplicate = true
						)
						
						if duplicate == false then tmp_array[tmp_array.count+1] = str_array[k]
					)
					
					--convert each string element to its bit field value in respect to events list
					for str in tmp_array do
					(
						--find a match for each event and convert to bitfield
						for count = 1 to FullEventList.count do
						(	
							if str == FullEventList[count] then bit_total += (bit.shift 1 (count-1))
						)
					)
					
					--store new bitfield version of events
					eventKeys[j].value = bit_total as string
				)
			)
		)
		
		--//////////////////////////////////////////////////////////////////////////////////
		--check animflags for older versions 
		--weeds out old letter based versions and seperated numerical flags
		--//////////////////////////////////////////////////////////////////////////////////
		if ((i == 2) and (oldkeys.count > 0)) then
		(
			--catch old letter based animflags and delete them
			if ((doesStringContainLetters newkey.value) == true) then deleteNoteKeys track.keys #allKeys
			
			--catch old style numerical ambient flags and total them up
			flagselection = newkey.value
			
			if (flagselection.count > 1) then
			(
				total = 0
				strArray = filterString flagselection " "

				for str in strArray do
				(
					total += (str as integer)
				)	
				
				newkey.value = total as string	
			)
		
		)
	)
)

--//////////////////////////////////////////////////////////////////////////////////
-- main program body.
-- first check user has selected a biped object
--//////////////////////////////////////////////////////////////////////////////////

-- check selection
if (selection.count == 0)
then messagebox "Select object to view event tracks"

-- if at least one object is selected...
else
(					
	-- Get selection
	object = selection[1]

	-- get COM name if biped is selected
	if ((classof selection[1])== biped_object) 
		then bipname = biped.getNode object 14 
			else bipname = object
	
	--//////////////////////////////////////////////////////////////////////////////////
	-- UI definition.
	--
	-- Create two list boxes. One has currently assigned events (kept up to date with getEventTracks fn), 
	-- the other lists all possible event tracks (FullEventList string array defined at top of script)
	--
	-- Create two buttons. One deletes currently selected event, one adds a new event. After either adding or deleting
	-- an event the currrently assigned events list will be re-sorted. Added events use the current time slider
	-- value to determine time.
	--//////////////////////////////////////////////////////////////////////////////////

	
		rollout rolloutEventTrack "Events"
		(
				
			listbox currentEventTrack "Assigned Event Tracks:" align:#left width:300 height:6 items:(getEventTracks (bipname) 1)
			
			button deleteButton "Delete Event" align:#left width:300

			listbox availableEventTrack "Events to Assign:"  align:#left width:300 height:6 items:FullEventList 
			
			button assignButton "Assign Event" align:#left width:300
			
			label spacer ""
			
			fn titleUpdate = 
			--fn updates rollout title to show whether events/flags are selected 
			--useful for spotting rogue events when rolled up!
			(
				if (currentEventTrack.items[1] == "0") or \
					((currentEventTrack.items[1] == (("\" 0 \" on frame 0") as string)) and currentEventTrack.items.count == 1) or \
					currentEventTrack.items.count == 0 then rolloutEventTrack.title = "Events" 
				else rolloutEventTrack.title = ("Events [ ! ]")
			)
			
			--//////////////////////////////////////////////////////////////////////////////////
			on deleteButton pressed do 
			(
				selectedNote = currentEventTrack.selection
				
				if (selectedNote>0) then
				-- if there is a selected event
				(
					track = getNoteTrack bipname 1

					keyarray = track.keys
						
					-- get selected event
	 				selectedNote = currentEventTrack.selection
					
					-- delete it
					deleteNoteKey keyArray selectedNote
					
					-- refresh current events
					currentEventTrack.items = getEventTracks (bipname) 1
				)
				
				-- update rollout title
				titleUpdate()
			)
			
			--//////////////////////////////////////////////////////////////////////////////////
			-- assign event button pressed
			--//////////////////////////////////////////////////////////////////////////////////
			on assignButton pressed do 
			(		
				track = getNoteTrack bipname 1
		
 				selectedNote = availableEventTrack.selection
				
				-- does notekey already exist on frame?
				keycheck = getNoteKeyIndex track slidertime
				
				if (keycheck == undefined) then
				--if not then create a key
				(
					notekey = addNewNoteKey track slidertime	
				)
				--else use existing one
				else notekey = track.keys[keycheck]
				
				tmp_key_value = notekey.value as integer
				
				--convert selection to bitfield
				new_event_as_bitfield = (bit.shift 1 (selectedNote-1))
				
				--check whether bit is already set? Don't want to set duplicate
				if ((isBitSet tmp_key_value selectedNote) == false) then
				(
					notekey.value = (tmp_key_value + new_event_as_bitfield) as string
				)
				
				-- refresh current events list
				currentEventTrack.items = getEventTracks (bipname) 1
				
				-- update rollout title
				titleUpdate()
			)
			
			on rolloutEventTrack open do
			(
				-- update rollout title
				titleUpdate()
			)
		)
		
		
		rollout rolloutAmbientFlags "Ambient Flags" 
		(
			radiobuttons flagRadio "Set Ambient Flags?" labels:#("Yes", "No") default:(getFlagRadioSelection bipname) align:#left
			multiListBox flagBox "Select multiple items with <Ctrl>" items:FullFlagList enabled:(getFlagBoxEnabled bipname) selection:(getFlagBoxSelection bipname)
			
			fn titleUpdate = 
			--fn updates rollout title to show whether events/flags are selected 
			--useful for spotting rogue events when rolled up!
			(
				bitArray = flagBox.selection as array
				if (bitArray.count == 0) then rolloutAmbientFlags.title = "Ambient Flags" 
				else rolloutAmbientFlags.title = "Ambient Flags [ ! ]"
			)

			on flagRadio changed state do
			(
				if state == 1 then flagBox.enabled = true
				if state == 2 then 
				(
					--grey out list box
					flagBox.enabled = false
					
					--turn off selection
					flagBox.selection = 0
					
					--delete any existing flags
					track = getNoteTrack bipname 2
					deleteNoteKeys track.keys #allKeys
				)
				
				-- update rollout title
				titleUpdate()
			)
			
			on flagBox selected state do
			(
				--get currently selected items
				bitArray = flagBox.selection
				
				track = getNoteTrack bipname 2
						
				--delete any existing flags
				deleteNoteKeys track.keys #allKeys
													
				--add in appropriate flag
				notekey = addNewNoteKey track 0
				
				local bitTotal = 0
				
				for number in bitArray do
				--go through each selected item, convert to bit position, and add to bitTotal
				(
					bitTotal +=  (bit.shift 1 (number-1))
				)
				
				noteKey.value = bitTotal as string
				
				-- update rollout title
				titleUpdate()
			)
			
			on rolloutAmbientFlags open do
			(
				-- update rollout title
				titleUpdate()
			)

			--required because of poor Max data cleanup problem					
			on rolloutAmbientFlags close do
			(
				flagBox.selection = 0
			)
		)
		
		rollout rolloutAudioEvents "Audio Events" 
		(
			listbox currentEventTrack "Assigned Audio Events:" align:#left width:300 height:6 items:(getEventTracks (bipname) 3)
			
			button deleteAudioButton "Delete Event" align:#left width:300

			listbox availableEventTrack "Audio Events to Assign:"  align:#left width:300 height:6 items:AudioEvents
			
			button assignAudioButton "Assign Event" align:#left width:300
			
			label spacer ""
			
			fn titleUpdate = 
			--fn updates rollout title to show whether events/flags are selected 
			--useful for spotting rogue events when rolled up!
			(
				if (currentEventTrack.items[1] == "0") or \
					((currentEventTrack.items[1] == (("\" 0 \" on frame 0") as string)) and currentEventTrack.items.count == 1) or \
					currentEventTrack.items.count == 0 then rolloutAudioEvents.title = "Audio Events" 
				else rolloutAudioEvents.title = ("Audio Events [ ! ]")
			)
			
			--//////////////////////////////////////////////////////////////////////////////////
			-- delete event button pressed
			--//////////////////////////////////////////////////////////////////////////////////
			on deleteAudioButton pressed do 
			(
				selectedNote = currentEventTrack.selection
				
				if (selectedNote>0) then
				-- if there is a selected event
				(
					track = getNoteTrack bipname 3

					keyarray = track.keys
						
					-- get selected event
	 				selectedNote = currentEventTrack.selection
					
					-- delete it
					deleteNoteKey keyArray selectedNote
					
					-- refresh current events
					currentEventTrack.items = getEventTracks (bipname) 3
				)
				
				-- update rollout title
				titleUpdate()
			)
			
			--//////////////////////////////////////////////////////////////////////////////////
			-- assign event button pressed
			--//////////////////////////////////////////////////////////////////////////////////
			on assignAudioButton pressed do 
			(		
				track = getNoteTrack bipname 3
		
 				selectedNote = availableEventTrack.selection
				
				-- does notekey already exist on frame?
				keycheck = getNoteKeyIndex track slidertime
				
				if (keycheck == undefined) then
				-- no existing note key on time frame
				-- so create a new one with selected event
				(
					notekey = addNewNoteKey track slidertime
					notekey.value = AudioEvents[selectedNote]
				)
				
				else
				-- if there is an existing note key then append to it
				(
					notekey = track.keys[keycheck]
					-- leave a single white space between events
					notekey.value += " "
					notekey.value += AudioEvents[selectedNote]
				)
				
				-- refresh current events list
				currentEventTrack.items = getEventTracks (bipname) 3
				
				-- update rollout title
				titleUpdate()
			)

			on rolloutAudioEvents open do
			(
				-- update rollout title
				titleUpdate()
			)

		)
		
		
		rollout rolloutFacialEvents "Facial Events" 
		(
			label dividerA  "---------------------------------------------------------------------------------------------"
			label reminder  "NOTE: Get latest on FACIALS@ folder to get up to date list."
			label dividerB  "---------------------------------------------------------------------------------------------"
		
			listbox currentEventTrack "Assigned Facial Events:" align:#left width:300 height:6 items:(getEventTracks (bipname) 6)
			
			button deleteFacialButton "Delete Event" align:#left width:300

			listbox availableEventTrack "Facial Events to Assign:"  align:#left width:300 height:6 items:FacialEvents
			
			button assignFacialButton "Assign Event" align:#left width:300
			
			label spacerB ""
			
			fn titleUpdate = 
			--fn updates rollout title to show whether events/flags are selected 
			--useful for spotting rogue events when rolled up!
			(
				if (currentEventTrack.items[1] == "0") or \
					((currentEventTrack.items[1] == (("\" 0 \" on frame 0") as string)) and currentEventTrack.items.count == 1) or \
					currentEventTrack.items.count == 0 then rolloutFacialEvents.title = "Facial Events" 
				else rolloutFacialEvents.title = ("Facial Events [ ! ]")
			)
			
			--//////////////////////////////////////////////////////////////////////////////////
			-- delete event button pressed
			--//////////////////////////////////////////////////////////////////////////////////
			on deleteFacialButton pressed do 
			(
				selectedNote = currentEventTrack.selection
				
				if (selectedNote>0) then
				-- if there is a selected event
				(
					track = getNoteTrack bipname 6

					keyarray = track.keys
						
					-- get selected event
	 				selectedNote = currentEventTrack.selection
					
					-- delete it
					deleteNoteKey keyArray selectedNote
					
					-- refresh current events
					currentEventTrack.items = getEventTracks (bipname) 6
				)
				
				-- update rollout title
				titleUpdate()
			)
			
			--//////////////////////////////////////////////////////////////////////////////////
			-- assign event button pressed
			--//////////////////////////////////////////////////////////////////////////////////
			on assignFacialButton pressed do 
			(		
				track = getNoteTrack bipname 6
		
 				selectedNote = availableEventTrack.selection
				
				-- does notekey already exist on frame?
				keycheck = getNoteKeyIndex track slidertime
				
				if (keycheck == undefined) then
				-- no existing note key on time frame
				-- so create a new one with selected event
				(
					notekey = addNewNoteKey track slidertime
					notekey.value = FacialEvents[selectedNote]
				)
				
				else
				-- if there is an existing note key then append to it
				(
					notekey = track.keys[keycheck]
					-- leave a single white space between events
					notekey.value += " "
					notekey.value += FacialEvents[selectedNote]
				)
				
				-- refresh current events list
				currentEventTrack.items = getEventTracks (bipname) 6
				
				-- update rollout title
				titleUpdate()
			)

			on rolloutFacialEvents open do
			(
				-- update rollout title
				titleUpdate()
			)

		)
		
		rollout rolloutDebug "Debug" 
		(
			button printTracksButton "Print names of NoteTracks" align:#left width:300
			
			on printTracksButton pressed do 
			(	
				num_tracks = numNoteTracks bipname

				print "Note Tracks assigned >>>>>>"
				
				for i = 1 to num_tracks do
				(
					track = getNoteTrack object i 
					print track
				)
			)	
		)
					
		-- get SW Events File
		loadEvents FullEventList EventsFilePath
		
		-- get Audio File Events
		loadEvents AudioEvents AudioEventFilePath
		--sort audio list alphabetically
		sort AudioEvents
		
		-- get Ambient File Events
		loadEvents FullFlagList AmbientFlagsFilePath
		
		-- get Facial Anims from Directory
		getFacialAnims FacialEvents FacialEventDir
		
		-- create note tracks
		createNoteTracks bipname
		
		-- create rollout UI
		floater = newRollOutFloater "Event Track Manager" 340 380
		addrollout rolloutEventTrack floater rolledup:false
		addrollout rolloutAmbientFlags floater rolledup:true
		addrollout rolloutFacialEvents floater rolledup:true
		addrollout rolloutAudioEvents floater rolledup:true
		--addrollout rolloutDebug floater rolledup:true
)
