macroScript SlidingToFreeform
	category:"Biped Tools"
	toolTip:"Changes selected keys to Freeform"
	icon:#("BlurScripts", 2)
(
	--changes freeform keys to sliding IK
					--select keys in time bar
					
					fn setIKBlend value =
					(
						for object in selection do
						(
							if (classof object == biped_object) then
							(
								for i = 1 to object.controller.keys.count do
								(
									key = biped.getKey object.controller i
									if key.selected and key.type == #body then 
									(
										sliderTime = key.time
								
										biped.setFreeKey object
									)
								)
							)
						)
					)
					
					setIKBlend 0 
)
