macroScript OffsetBiped
	category:"Biped Tools"
	toolTip:"Offset Biped COM over a time range"
	icon:#("bip_curve", 3)
(
	(
		global Offset_Handle
				global Offset_Pos = [407,77]
				
				fn OffsetBiped posX posX_CB posY posY_CB posZ posZ_CB 
								rotX rotX_CB rotY rotY_CB rotZ rotZ_CB 
									start end =
				(	
					undo on(
		
						--offsets a section of Biped animation 
						object = selection[1]
						objTransform = #()
						
						progressStart "Updating Biped's position..."
		
						--get COM name
						bipname = biped.getNode object 14
							
						--go through range and record transform
						for i = start to end do
						(
							--update Slider
							sliderTime = i
							
							append objTransform bipname.transform
						)	
							
						--go through range and subtract offset
						for i = 1 to ((end-start)+1) do
						(
							--update Slider
							sliderTime = start-1+i
							
							--create temp matrix
							tempTransform = objTransform[i]
							tempTranslation = tempTransform.translation
							tempRotation = (tempTransform.rotation as eulerangles)
							
							-- add transforms

								if (posX_CB == true) then tempTranslation.x += posX
								if (posY_CB == true) then tempTranslation.y += posY
								if (posZ_CB == true) then tempTranslation.z += posZ
								
								if (rotX_CB == true) then tempRotation.x += rotX
								if (rotY_CB == true) then tempRotation.y += rotY
								if (rotZ_CB == true) then tempRotation.z += rotZ
				
							--print tempTransform.translation
							tempTransform.rotation = ((eulerangles tempRotation.x tempRotation.y tempRotation.z) as quat)
							tempTransform.translation = tempTranslation
							
							-- apply matrix back to biped
							bipname.transform = tempTransform
		
							if not (progressUpdate (100.0 * i/(end-start))) then exit
						)
							
						print "Biped Offset complete"
						progressEnd()
						
					) --end of undo
				)
				
				rollout Offset_Cont "Offset Biped"
				(
					local astart = animationrange.start,
						  aend = animationrange.end			
						  
					group "Position"
					(
						checkbox posX_CB "X" checked:true across:3 align:#center fieldwidth:50
						checkbox posY_CB "Y" checked:true across:3 align:#center fieldwidth:50
						checkbox posZ_CB "Z" checked:true across:3 align:#center fieldwidth:50
						spinner posX "X" range:[-1000000,1000000,0] across:3 type:#float fieldwidth:50 align:#center enabled:true
						spinner posY "Y" range:[-1000000,1000000,0] type:#float fieldwidth:50 align:#center enabled:true
						spinner posZ "Z" range:[-1000000,1000000,0] type:#float fieldwidth:50 align:#center enabled:true
					)

					group "Rotation"
					(
						checkbox rotX_CB "X" checked:true across:3 align:#center fieldwidth:50
						checkbox rotY_CB "Y" checked:true across:3 align:#center fieldwidth:50
						checkbox rotZ_CB "Z" checked:true across:3 align:#center fieldwidth:50
						spinner rotX "X" range:[-1000000,1000000,0] across:3 type:#float fieldwidth:50 align:#center enabled:true
						spinner rotY "Y" range:[-1000000,1000000,0] type:#float fieldwidth:50 align:#center enabled:true
						spinner rotZ "Z" range:[-1000000,1000000,0] type:#float fieldwidth:50 align:#center enabled:true
					)
					
					group "Frame Range"
					(
						spinner sframe "Start Time" range:[0,1000000,astart] type:#integer fieldwidth:50 align:#center enabled:true
						spinner eframe "End Time" range:[0,1000000,aend] type:#integer fieldwidth:50 align:#center enabled:true
					)
					
					group "Offset"
					(
						button AnimKey "Go" align:#center width:175 across:1
					)
					
					--actions
					on posX_CB changed theState do posX.enabled = thestate
					on posY_CB changed theState do posY.enabled = thestate
					on posZ_CB changed theState do posZ.enabled = thestate
					on rotX_CB changed theState do rotX.enabled = thestate
					on rotY_CB changed theState do rotY.enabled = thestate
					on rotZ_CB changed theState do rotZ.enabled = thestate

					on AnimKey pressed do
					(			
						if (selection.count == 0) or ((classof selection[1])!= biped_object) then messagebox "Select Biped to offset"
						else OffsetBiped posX.value posX.enabled posY.value posY.enabled posZ.value posZ.enabled rotX.value rotX.enabled rotY.value rotY.enabled rotZ.value rotZ.enabled sframe.value eframe.value
					)
				)

				try
				(
					closerolloutfloater Offset_Handle
					Offset_Pos = Offset_Handle.pos
			
				)catch()
				
				Offset_Handle = newrolloutfloater "Offset Biped" 280 290
				Offset_Handle.pos = Offset_Pos
				addrollout Offset_Cont Offset_Handle rolledup:false
	)
)
