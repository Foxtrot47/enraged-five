macroScript SnapShotParent
	category:"Biped Tools"
	toolTip:"Create Snapshot and parents to object"
	icon:#("Systems", 1)
(
	--Script creates a snapshot of the object 
			--and parents object to it, useful for adding control object to bip root
			
			sel = selection [1]
			if (sel == undefined) then
			(
				 messagebox "Nothing selected?"
			)
			else
			(
				dummy = snapshot sel
				select dummy
				scale dummy [1.5,1.5,1.5]
				dummy.name = "ParentObject"
				sel.parent = $ParentObject

			)
)
