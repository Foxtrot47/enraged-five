macroScript ConstrainBiped
	category:"Biped Tools"
	toolTip:"Creates a constraint for selected Biped part"
	icon:#("FileLinkActionItems", 7)
(
	--//////////////////////////////////////////////////////////////////////////////////
	-- Constrain Biped by Mike Jones (07.07.06)
	--
	-- DESCRIPTION: Allows user to set up a constraint on a biped object. Object is constrained to a helper node.
	--
	-- USAGE: Select a single Biped object and run script.
	-- Orientation and Position checkboxes determine which transforms are constrained.
	-- Keep Animation checkbox determines whether existing position is retained.
	-- 
	--//////////////////////////////////////////////////////////////////////////////////

	fn constrainBiped orientBox posBox animBox sframe eframe step object =
	(
		undo on
		(	
			--create Constraint helper
			constraintNode = ExposeTm pos:[0,0,0.0,0.0] size:0.2 prefix:"Constraint_Node"
		
			objectPos = #()
			objectRot = #()
			
			if animBox == true then
			(
				--/////////////////////////////////////////////////////////
				--copy animation from Biped part
				--/////////////////////////////////////////////////////////
				
				with animate off
				(
					for i = sframe to eframe do
					(
						slidertime = i
						
						objectPos[1+i] = object.transform.pos
						objectRot[1+i] = inverse object.transform.rotation
					)
				)
	
				--/////////////////////////////////////////////////////////
				--paste animation on to constraint
				--/////////////////////////////////////////////////////////	
				with animate on
				(
					for i = sframe to eframe do
					(
						slidertime = i
						
						if orientBox == true then constraintNode.rotation = objectRot[1+i]
						if posBox == true then constraintNode.pos = objectPos[1+i]
					)
				)
			)
			
			else
			(
				--simply align constraint to object if keep animation is not selected
				constraintNode.rotation = inverse object.transform.rotation
				constraintNode.pos = object.transform.pos
			)
			
			--/////////////////////////////////////////////////////////
			--create constraint script and add to scale controller
			--/////////////////////////////////////////////////////////
			
			constraintScript = scale_script()
			constraintNode.scale.controller = constraintScript
			
			tmpScript = ""
			
			if posBox == true then
			(
				tmpScript += "biped.settransform "
				tmpScript += "$'" + object.name + "'"
				tmpScript += " #pos "
				tmpScript += "$'" + ConstraintNode.name + "'"
				tmpScript += ".pos false\n"
			)
			
			if orientBox == true then
			(
				tmpScript += "biped.settransform "
				tmpScript += "$'" + object.name + "'"
				tmpScript += " #rotation "
				tmpScript += "(inverse $'" + ConstraintNode.name + "'"
				tmpScript += ".rotation) false\n"
			)

			--finish script controller
			tmpScript += "[1,1,1]\n"
			
			--add script to node
			constraintScript.script = tmpScript
			
			--add Biped Node to script
			bipname = biped.getNode object 14
			constraintScript.addconstant "Constraint" 0
			constraintScript.addNode "Constraint" bipname
			
		) -- end of Undo
	
		return constraintNode
	)

	rollout Constraint_Cont "Constrain Biped"
	(
		local astart = animationrange.start, aend = animationrange.end			
						
		group "Contraint Options"
  		(
			checkbox orientBox "Orientation" checked:true across:3 align:#left
			checkbox posBox "Position" checked:true align:#left
			checkbox animBox "Keep Animation" checked:true align:#center
		)
		
		group "Frame Range"
		(
			spinner sframe "Start Time" range:[0,1000000,astart]  offset:[-70,0] type:#integer fieldwidth:50 align:#right enabled:true
			spinner eframe "End Time" range:[0,1000000,aend] offset:[-70,0] type:#integer fieldwidth:50 align:#right enabled:true
			spinner step "Step" range:[0,1000000,1] offset:[-70,0] type:#integer fieldwidth:50 align:#right enabled:true
		)
					
		group "Constrain"
		(
			button GoButton "Go" align:#center width:175 across:1
		)

		on animBox changed false do 
		(
			sframe.enabled = false
			eframe.enabled = false
			step.enabled = false
		)
		
		on GoButton pressed do
		(			
			if (selection.count == 0) or ((classof selection[1])!= biped_object) then messagebox "Select Biped to Constrain"
			else 
			(
				--variable to store last constraintNode 
				constraintNode
								
				for object in selection do
				(
					constraintNode = ConstrainBiped orientBox.checked posBox.checked animBox.checked sframe.value eframe.value step.value object
				)
				
				--leave user with constraint selected
				select constraintNode
			)
		)
	)

	try
	(
		closerolloutfloater Constraint_Handle
		Constraint_Pos = Constraint_Handle.pos
			
	)catch()
				
	Constraint_Handle = newrolloutfloater "Constrain Biped" 280 225
	
	addrollout Constraint_Cont Constraint_Handle rolledup:false
)