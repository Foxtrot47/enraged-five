macroScript TCBhard
	category:"Biped Tools"
	toolTip:"Sets selected TCB keys to 50 - hard"
	icon:#("TrackViewKeyTangents", 6)
(
		--set all selected Biped objects tension values to supplied tension value
			--only does it on selected keys
			
			fn setTCBTension tension =
			(
				for object in selection do
				(
					if (classof object == biped_object) then
					(
						for i = 1 to object.controller.keys.count do
						(
							key = biped.getKey object.controller i
							if key.selected then key.tension = tension
						)
					)
				)
			)
			
			setTCBTension 50
)
