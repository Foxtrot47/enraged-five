macroScript FitTimeBar
	category:"Biped Tools"
	toolTip:"Update TimeBar so it fits all keys in scene"
	icon:#("TrackViewStatus", 5)
(
	--Update TimeBar so it fits keys
	minimum = 1000
	maximum = 0

	--loop through each object
	for object in objects do
	(
		--loop through any keys
		keys = object.controller.keys
		
		--check time of each key and compare against min and max
		for key in keys do
		(
			if key.time < minimum then minimum = key.time
			if key.time > maximum then maximum = key.time
		)
	)
	
	if (minimum-maximum == 0) then maximum+=1

	--update timebar with min and max
	animationrange = interval minimum maximum

)