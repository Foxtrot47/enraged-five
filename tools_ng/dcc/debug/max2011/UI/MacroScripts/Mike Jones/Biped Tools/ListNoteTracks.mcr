macroScript ListNoteTracks
	category:"Biped Tools"
	toolTip:"List event tracks for the selected Biped"
	icon:#("Maintoolbar", 89)
(
	(
		--check selection
		if (selection.count == 0) or ((classof selection[1])!= biped_object) 
		then messagebox "Select Biped to check for event tracks"
		
		--if Biped part is selected...
		else
		(
			--Get selected Biped part
			object = selection[1]
			
			--get COM name
			bipname = biped.getNode object 14

			--does biped already have a notetrack?
			notecheck = hasNoteTracks bipname
			print notecheck
			if (notecheck==false) then messagebox "No event tracks found"
			else
			(

				--used to store our pointer to the event track
				trackhandle
			
				--get existing notetrack
				trackhandle = getNoteTrack bipname 1
				sortNoteKeys trackhandle
				
				keyarray = trackhandle.keys
				
				noteTrackList = #()
				count =1
				
				for key in keyarray do
				(
					tmp = "< "
					tmp += key.value
					tmp = trimright tmp
					tmp += " > on frame "
					tmp += (getNoteKeyTime trackhandle count as string)
					noteTrackList[count] = tmp
					count += 1
				)
				
				rollout rolloutEventTrack "Biped Event Tracks" 
				(
					listbox listEventTrack "Events:" height:10 items:noteTrackList 
				)
				
				createDialog rolloutEventTrack 200 175 --create a Dialog from the rollout
			)
		)
	)
)
