macroScript RandomizeBiped
	category:"Biped Tools"
	toolTip:"Randomize Biped part over a time range"
	icon:#("TrackViewStatus", 1)
(
	(
		global Offset_Handle
				global Offset_Pos = [407,77]
				
				fn OffsetBiped posX posY posZ rotX rotY rotZ start end step =
				(	
					undo on
					(							
						--Get selected Biped part
						object = selection[1]
				
						--------------------------------
						--copy animation from Biped part
						--------------------------------
						objectTM = #()
						
						for i = start to end do
						(
							slidertime = i
							objectTM[1+i] = object.transform							
						)
						
						-----------------------------------------
						--paste animation back with random offset
						-----------------------------------------
					
						for i = start to end by step do
						(
							slidertime = i
							
							existingPos = objectTM[1+i].position
							existingRot = (objectTM[1+i].rotation as eulerangles)
							
							--add random offsets
							existingPos = [random (existingPos.x-posX) (existingPos.x + posX), random (existingPos.y-posY) (existingPos.y+posY), random (existingPos.z-posZ) (existingPos.z+posZ)]
		
							existingRot.x = random (existingRot.x-rotX) (existingRot.x + rotX)
							existingRot.y = random (existingRot.y-rotY) (existingRot.y+rotY)
							existingRot.z = random (existingRot.z-rotZ) (existingRot.z+rotZ)

							if classof object == Biped_Object then
							(
								biped.setTransform object #pos existingPos true
								biped.setTransform object #rotation (existingRot as quat) true
							)
							else
							(
								object.position = existingPos
								object.rotation = existingRot
								
								max set key on selected
							)
						)
																
					) --end of undo
				)
				
				rollout Offset_Cont "Randomize Biped"
				(
					local astart = animationrange.start,
						  aend = animationrange.end			
						  
					group "Position Offset"
					(
						spinner posX "X" range:[-1000000,1000000,0] across:3 scale:0.001 type:#float fieldwidth:50 align:#center enabled:true
						spinner posY "Y" range:[-1000000,1000000,0] scale:0.001 type:#float fieldwidth:50 align:#center enabled:true
						spinner posZ "Z" range:[-1000000,1000000,0] scale:0.001 type:#float fieldwidth:50 align:#center enabled:true
					)

					group "Rotation Offset"
					(
						spinner rotX "X" range:[-1000000,1000000,0] across:3 scale:0.001 type:#float fieldwidth:50 align:#center enabled:true
						spinner rotY "Y" range:[-1000000,1000000,0] scale:0.001 type:#float fieldwidth:50 align:#center enabled:true
						spinner rotZ "Z" range:[-1000000,1000000,0] scale:0.001 type:#float fieldwidth:50 align:#center enabled:true
					)
					
					group "Frame Range"
					(
						
						spinner sframe "Start Time" range:[0,1000000,astart]  offset:[-70,0] type:#integer fieldwidth:50 align:#right enabled:true
						spinner eframe "End Time" range:[0,1000000,aend] offset:[-70,0] type:#integer fieldwidth:50 align:#right enabled:true
						spinner step "Step" range:[0,1000000,1] offset:[-70,0] type:#integer fieldwidth:50 align:#right enabled:true
					)
					
					group "Randomize"
					(
						button AnimKey "Go" align:#center width:175 across:1
					)

					on AnimKey pressed do
					(			
						if (selection.count == 0) then messagebox "Select object to Randomize"
						else OffsetBiped posX.value posY.value posZ.value rotX.value rotY.value rotZ.value sframe.value eframe.value step.value
					)
				)

				try
				(
					closerolloutfloater Offset_Handle
					Offset_Pos = Offset_Handle.pos
			
				)catch()
				
				Offset_Handle = newrolloutfloater "Randomize Biped" 280 270
				Offset_Handle.pos = Offset_Pos
				addrollout Offset_Cont Offset_Handle rolledup:false
	)
)
