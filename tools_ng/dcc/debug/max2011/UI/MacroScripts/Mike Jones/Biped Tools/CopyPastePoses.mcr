macroScript CopyPastePoses
	category:"Biped Tools"
	toolTip:"Copies sequence of animation as Biped Poses - can reverse animation"
	icon:#("JJTools", 40)
(
	global CPP_floater
			global CPP_pos = [407,77]
			
			fn CopyPastePoses start end destination reverse COM =
			(
				--copies selected Biped as a sequence of poses for animation range
				--start and end define copied range
				--destination defines target frame
				--reverse defines whether animation should be reversed or not
				--COM defines whether the COM is copied/pasted
				
				print reverse
				
				poseIndex= #()
				obj = selection[1]
				
				for i = start to end do
				(
					slidertime = i
					pose = biped.copyposture obj.controller #pose COM COM COM 
					append poseIndex pose
				)
				
				slidertime = destination
				
				if reverse then 
				(
					i = (end - start)
					
					while i > 0 do
					(
						biped.pasteposture obj.controller #pose false poseIndex[i]
						slidertime += 1
						i -= 1
						--biped.deletecopy obj.controller #pose poseIndex[i]
					)
				)
				
				else 
				(
					i = 1
					
					while i <= (end - start) do
					(
						print i
						print poseIndex[i]
						biped.pasteposture obj.controller #pose false poseIndex[i]
						slidertime += 1			
						i += 1
						--biped.deletecopy obj.controller #pose poseIndex[i]
					)	
				)
			)
			
			rollout CPP_cont "CopyPastePoses"
			(
				local astart = animationrange.start,
					  aend = animationrange.end
					  
				group "Frame Range"
				(
					spinner sframe "Start Time" range:[0,1000000,astart] type:#integer fieldwidth:50 align:#right enabled:true
					spinner eframe "End Time" range:[0,1000000,aend] type:#integer fieldwidth:50 align:#right enabled:true
					spinner dest "Destination" range:[0,1000000,(aend+1)] type:#integer fieldwidth:50 align:#right enabled:true

				)

				group "Reverse?"
				(
					radiobuttons reverse labels:#("Yes", "No") align:#center default:2
				)
				
				group "Copy and Paste"
				(
					button AnimKey "OK" align:#center across:1
				)
							
				on sframe changed val do
				(
					animationrange.start = val
				)
				
				on dest changed val do
				(
					if val <= eframe.value then dest.value = eframe.value+1 
					else animationrange.end = val+(eframe.value-sframe.value)
				)
				
				on AnimKey pressed do
				(
					local reverseState = false
					
					if reverse.state == 1 then reverseState = true else reverseState = false		
			
					if (selection.count == 0) or ((classof selection[1])!= biped_object) then messagebox "Select Biped to copy"
					else CopyPastePoses sframe.value eframe.value dest.value reverseState true
				)
			)

			try
			(
				closerolloutfloater CPP_floater
				CPP_pos = CPP_floater.pos
		
			)catch()
			
			CPP_floater = newrolloutfloater "CopyPastePoses" 170 220
			CPP_floater.pos = CPP_pos
			addrollout CPP_cont CPP_floater rolledup:false
)
