macroScript AddDummyAndMoverNodes
	category:"Biped Tools"
	toolTip:"Adds a Dummy and Mover node to the selected Biped"
	icon:#("Helpers", 1)
(
	--/////////////////////////////////////////////
	--add dummy node and mover node to selected bip
	--Mike Jones
	--/////////////////////////////////////////////
	
	
	--check selection
	if (selection.count == 0) or ((classof selection[1])!= biped_object) 
	then messagebox "Select Biped to add Dummy and Mover nodes to"
	else 
	(
		query = queryBox "Add Dummy and Mover Node?" title:"Add Dummy&Mover Nodes"
		if query == true then 
		(	
		
			--Get selected Biped part
			object = selection[1]
			
			--get COM name
			bipname = biped.getNode object 14
			
			--Delete any existing Dummy or Mover Nodes
			moverNode = getnodebyname "mover" exact:true
			if moverNode != undefined then delete moverNode
			dummyNode = getnodebyname "Dummy01" exact:true
			if dummyNode != undefined then delete dummyNode
			axisNode = getnodebyname "axis_helper" exact:true
			if axisNode != undefined then delete axisNode
			layer = layermanager.getlayerfromname "Mover Node"
			if layer != undefined then layermanager.deleteLayerByName "Mover Node"
			
			--move slider to start
			slidertime = animationrange.start
			
			--get root bone z height
			bipname.transform.controller.figureMode = true
			z_height = bipname.transform[4].z
			bipname.transform.controller.figureMode = false
			
			--create Dummy node
			newDummyNode = Dummy()
			newDummyNode.name = "Dummy01"
			newDummyNode.boxsize = [.2,.2,.2]
			newDummyNode.pos = [0.0,0.0,z_height]
			
			--parent biped to it
			bipname.parent = newDummyNode
			
			--create Mover Node
			newMoverNode = Dummy()
			newMoverNode.name = "mover"
			newMoverNode.boxsize = [.3,.3,.3]
			newMoverNode.pos = [0.0,0.0,z_height]
			
			--create axis helper node
			axis_helper = Point pos:[0,0,1] isSelected:off cross:off axistripod:on size:0.25 name:"axis_helper"
			setTransformLockFlags axis_helper #all
			
			--parent nodes correctly
			newMoverNode.parent = newDummyNode
			axis_helper.parent = newMoverNode
			
			--create a new layer for Dummy and Mover nodes 
			layer = layermanager.newlayerfromname "Mover Node"
			layer.addnode axis_helper
			layer.addnode newMoverNode 
			layer.addnode newDummyNode
		)
	)
)