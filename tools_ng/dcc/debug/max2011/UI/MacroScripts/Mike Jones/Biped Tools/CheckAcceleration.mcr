macroScript CheckAcceleration
	category:"Biped Tools"
	toolTip:"Checks selected object's acceleration does not exceed 1 unit per frame"
	icon:#("Helpers", 5)
(
	if (selection.count == 0) then messagebox "Select object to check acceleration"
	else
	(
		--check acceleration on selected object does not exceed 1 unit per frame
		
		object = selection[1]
		
		--move slide to start
		slidertime = animationrange.start
				
		--store initial position to allow last check
		lastPos = object.transform.pos
		lastVel = [0.0,0.0,0.0]
		lastAcc = [0.0,0.0,0.0]
				
		-- store bad frames in tmp array, count holds number of bad frames (+1)
		count = 1
		tmp = #()
		
		for i = animationrange.start to animationrange.end+1 do
		(
			-- don't update slider if we're at the very end frame
			if (i < animationrange.end+1) then
			( 
				slidertime = i
			)	
				currPos = object.transform.pos 
				currVel = ((currPos - lastPos)*30)
				currAcc = (currVel - lastVel)
					
				if ((length(currAcc)) > 1) then 
				(
					tmp[count]=("WARNING: Too much acceleration on frame " + (slidertime as string))
					count += 1	
				)
				
				lastPos = currPos
				lastVel = currVel
				lastAcc = currAcc
		)
		
		--if count is still 1 then no bad frames must have been found
		if (count == 1) then tmp[1] = "Object did not exceed limits!"

		-- Create listbox showing results
		Rollout AccelRollOut "Check Acceleration"
		(
			listbox currentEventTrack "" width:275 height:10 items:tmp
		)
	
		createDialog AccelRollOut 300 155 --create a Dialog from the rollout
	)
)