macroScript ZeroBipedXY
	category:"Biped Tools"
	toolTip:"Zero Biped X and Y starting position"
	icon:#("bip_curve", 11)
(
		--move selected Biped's COM to zero in X and Y axis, ready for export
	query = queryBox "Zero Biped X&Y tracks?" title:"ZeroBipedXY"
	if query == true then 
	(		
		progressStart "Updating Biped's position..."
		
		for object in selection do
		(
			if (classof object == biped_object) then
			(
				--get COM name
				bipname = biped.getNode object 14
				
				--get starting X&Y offset values of the first keyframe
				startKey = biped.getKey bipname.transform.controller.horizontal.controller 1
				numOfKeys = bipname.transform.controller.horizontal.controller.keys.count
				endkey = biped.getKey bipname.transform.controller.horizontal.controller numOfKeys
				
				offsetX = startKey.x
				offsetY = startKey.y
				
				--go through each key and subtract offset
				for i = 1 to bipname.transform.controller.horizontal.controller.keys.count do
				(
					key = biped.getKey bipname.transform.controller.horizontal.controller i
					key.x -= offsetX
					key.y -= offsetY
					
					if not (progressUpdate (100.0 * i/endkey.time)) then exit
				)
				
			)
		
		print "Biped's X&Y COM tracks zero'd"
		
		)
		
		progressEnd()
	)
)
