macroScript AddNoteTrack
	category:"Biped Tools"
	toolTip:"Adds an event track to the selected Biped"
	icon:#("Maintoolbar", 87)
(
	--check selection
	if (selection.count == 0) or ((classof selection[1])!= biped_object) 
	then messagebox "Select Biped to add notetrack"
	
	--if Biped part is selected...
	else
	(
		--Get selected Biped part
		object = selection[1]
		
		--get COM name
		bipname = biped.getNode object 14

		--does biped already have a notetrack?
		notecheck = hasNoteTracks bipname
		
		--used to store our pointer to the event track
		trackhandle
		
		if (notecheck == false) then
		--create a notetrack
		(
			newtrack = notetrack "Event_Track"
			addNoteTrack bipname newtrack
			trackhandle = getNoteTrack bipname 1;
		)
		else
		--get existing notetrack
		(
			--get first(hopefully only) note track
			trackhandle = getNoteTrack bipname 1;
		)
		
		rollout addEventTrack "Add Event Track" 
		(
			--define an edittext with width 100 and the label on top:
			edittext eventstring "Event string to add" fieldWidth:200 labelOnTop:true
		
			--If the user enters a string...
			on eventstring entered txt do
			(
				--And the name is not an empty string,
				if txt != "" do 
				(
					notekey = addNewNoteKey trackhandle slidertime
					notekey.value = txt
					eventstring.text = ""
				)
				
			destroydialog addEventTrack
			
			print ("<" + txt + ">" + " note Track added to frame " + (slidertime as string) +".")
			)
		)
		
		createDialog addEventTrack 300 60 --create a Dialog from the rollout
	)
)