macroScript updateMoverNode
	category:"Biped Tools"
	toolTip:"Select mover node and run this - it will update the position"
	icon:#("Helpers", 2)
(
	--check selection
	if (selection.count == 0) then messagebox "Select Mover Node"
	else 
	(
		dummyNode = selection[1].parent
		
		local biproot
		
		for child in dummyNode.children do
		(
			if classof child == biped_object then biproot = child
		)

		--get position	
		bipPos = biped.getTransform biproot #pos

		--move mover node
		$mover.pos.x = bipPos.x
		$mover.pos.y = bipPos.y
	)
)
