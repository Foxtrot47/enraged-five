macroScript PosConstrainBiped
	category:"Biped Tools"
	toolTip:"Creates an animated position constraint for selected Biped part"
	icon:#("FileLinkActionItems", 7)
(
	/***Create Animated Position Constraint for selected Biped part****/
	
	--check selection
	if (selection.count == 0) or ((classof selection[1])!= biped_object) then messagebox "Select Biped part to constrain"
	
	--if Biped part is selected...
	else
	(
		--Get selected Biped part
		object = selection[1]

		--create Constraint helper
		constraintNode = ExposeTm pos:[0,0,0.0,0.0] size:0.2 prefix:"POS_Constraint_Node"
		
		--------------------------------
		--copy animation from Biped part
		--------------------------------
		objectTM = #()
		
		for i = animationrange.start to animationrange.end do
		(
			slidertime = i
			objectTM[1+i] = object.transform.pos
		)
		
		--align Constraint helper to Biped part
		constraintNode.pos = object.transform.pos
		
		--add orientation script node (use scale script)
		posScript = scale_script()
		constraintNode.scale.controller = posScript
		
		--create controller script
		tmpScript = "biped.settransform "
		tmpScript += "$'" + object.name + "'"
		tmpScript += " #pos "
		tmpScript += "$'" + ConstraintNode.name + "'"
		tmpScript += ".pos false\n" 
		tmpScript += "[1,1,1]\n"
		
		--add script to node
		posScript.script = tmpScript
		
		--add Biped Node to script
		bipname = biped.getNode object 14
		posScript.addconstant "Position" 0
		posScript.addNode "Position" bipname
		
		----------------------------------
		--paste animation on to constraint
		----------------------------------		
		with animate on
		(
			for i = animationrange.start to animationrange.end do
			(
				slidertime = i
				constraintNode.pos = objectTM[1+i]
			)
		)
	)
)