macroScript PelvisFix
	category:"Biped Tools"
	toolTip:"Pelvis Flip fix for Max 7->8"
	icon:#("MergeAnim", 1)
(
	(
		for object in selection do
		(
			if (classof object == biped_object) then
			(
				pelvis = biped.getNode object 12

				for i = 1 to pelvis.controller.keys.count do
				(
					sliderTime = pelvis.controller.keys[i].time
					
					rot = biped.getTransform pelvis #rotation
					biped.setTransform pelvis #rotation rot true
				)
			)
		)
	)

)
