macroScript OrientConstrainBiped
	category:"Biped Tools"
	toolTip:"Creates an animated orientation constraint for selected Biped part"
	icon:#("FileLinkActionItems", 9)
(
	/***Create Animated Orientation Constraint for selected Biped part****/
	
	--check selection
	if (selection.count == 0) or ((classof selection[1])!= biped_object) then messagebox "Select Biped part to constrain"
	
	--if Biped part is selected...
	else
	(
		--Get selected Biped part
		object = selection[1]

		--create Constraint helper
		constraintNode = ExposeTm pos:[0,0,0.0,0.0] size:0.2 prefix:"Constraint_Node"
		
		--------------------------------
		--copy animation from Biped part
		--------------------------------
		objectTM = #()
		
		for i = animationrange.start to animationrange.end do
		(
			slidertime = i
			objectTM[1+i] = object.transform.rotation
		)
		
		--align Constraint helper to Biped part
		constraintNode.rotation = object.transform.rotation
		
		--add orientation script node (use scale script)
		orientationScript = scale_script()
		constraintNode.scale.controller = orientationScript
		
		--create controller script
		tmpScript = "biped.settransform "
		tmpScript += "$'" + object.name + "'"
		tmpScript += " #rotation "
		tmpScript += "$'" + ConstraintNode.name + "'"
		tmpScript += ".rotation false\n" 
		tmpScript += "[1,1,1]\n"
		
		--add script to node
		orientationScript.script = tmpScript
		
		--add Biped Node to script
		bipname = biped.getNode object 14
		orientationScript.addconstant "Orientate" 0
		orientationScript.addNode "Orientate" bipname
		
		----------------------------------
		--paste animation on to constraint
		----------------------------------		
		with animate on
		(
			for i = animationrange.start to animationrange.end do
			(
				slidertime = i
				constraintNode.rotation = objectTM[1+i]
			)
		)
	)
)