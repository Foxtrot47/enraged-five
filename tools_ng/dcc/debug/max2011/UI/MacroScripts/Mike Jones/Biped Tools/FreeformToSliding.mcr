macroScript FreeformToSliding
	category:"Biped Tools"
	toolTip:"Changes selected keys to Sliding Keys"
	icon:#("JJTools", 31)
(
		--changes multiple freeform keys to sliding IK
		--select keys in time bar
				
				fn setIKBlend value =
				(
					for object in selection do
					(
						if (classof object == biped_object) then
						(
							for i = 1 to object.controller.keys.count do
							(
								key = biped.getKey object.controller i
								if key.selected and key.type == #body then 
								(
									sliderTime = key.time
									biped.setSlidingKey object
									
								)
							)
						)
					)
				)
				
				setIKBlend 1 
)
