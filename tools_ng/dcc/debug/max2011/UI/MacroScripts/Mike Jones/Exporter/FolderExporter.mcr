macroScript FolderExport
	category:"Exporter"
	toolTip:"Export selected anim folders. Must open R* exporter first!"
	icon:#("LayerToolbar", 1)
(
	global RsAnimRoll

	fn returnAnimFolders folder =
	-- fn returns a string array of sub folder names in folder which contain export files
	-- string array is alphabetically sorted
	(
	
		fn uppercase instring = -- beginning of function definition
		( 
			local upper, lower, outstring -- declare variables as local
			
			upper="ABCDEFGHIJKLMNOPQRSTUVWXYZ" -- set variables to literals
			lower="abcdefghijklmnopqrstuvwxyz"
			
			-- create an unique copy of the string referenced by instring, and store
			-- reference to unique copy in outstring
			outstring = copy instring
			
			-- increment from 1 to number of character in string
			for i = 1 to outstring.count do
			
			-- see if the single character at index i in outstring is present in string lower
			-- If so, j equals position in string lower. If not, j equals undefined
			( 
				j = findString lower outstring[i]
				-- if character was found in lower, replace with corresponding character in upper
			
				if (j != undefined) do outstring[i]=upper[j]
			)
			outstring -- value of outstring will be returned as function result
		)
	
		folder = "X:\GTA\gta_art\anim\export"
		
		--collect all folders in export
		dirs = GetDirectories (folder+"/*")
		for dir in dirs do
		(
			join dirs (GetDirectories (dir+"*"))
		)
		
		--remove empty folders from list
		copy_dirs = #()
		for i = 1 to dirs.count do
		(
			files = getFiles ((dirs[i] as string) + "*.*")
			if files.count != 0 then append copy_dirs dirs[i]
		)
		dirs = copy_dirs
		
		--convert folders to uppercase only
		for i = 1 to dirs.count do
		(
			dirs[i] = uppercase dirs[i]
		)
		
		--sort folders alphabetically
		sort dirs
		
		return dirs
	)


	rollout exportFolder "Folder Exporter" 
	(
		--////////////////////////////////////////////////////////////////////////////////////////
		-- create listbox with anim folders
		--////////////////////////////////////////////////////////////////////////////////////////
		
		local exportDir = "X:\GTA\gta_art\anim\export"
		
		multiListBox folderlistbox "Anim Folders" items:(returnAnimFolders exportDir) height:33
		button buttonExportFolders "Export Folders" align:#left width:475

		on buttonExportFolders pressed do
		(			
			local anim_folders = returnAnimFolders exportDir
			--for i = 1 to folderlistbox.selection.count do
			list_selection = folderlistbox.selection as array
			for item_number in list_selection do
			(
				--build each folder
				rsAnimRoll.BuildDirRec anim_folders[item_number]
			)
			
			query = queryBox "Build Image?" title:"Animation Export"
			if query == true then 
			(	
				--build new img
				rsAnimRoll.BuildAnims()
			)
			
			--////////////////////////////////////////////////////////////////////////////////////////
			-- Close rollout 
			--////////////////////////////////////////////////////////////////////////////////////////
			DestroyDialog exportFolder 
		)
	)
		
	createDialog exportFolder width:500 height:500

)