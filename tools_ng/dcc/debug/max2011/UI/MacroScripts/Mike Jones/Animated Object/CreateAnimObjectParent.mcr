macroScript CreateAnimObjectParent
	category:"Animated Object"
	toolTip:"Create AnimObject Parent"
	icon:#("AtmosApp", 1)
(
	(
		--Create AnimObject Dummy parent
		obj = selection [1]
				
		if (obj == undefined) then
		(
			 messagebox "Nothing selected?"
		)
			
		else
		(	
			--create Dummy
			objParent = Dummy()
			objParent.boxsize = [0.05,0.05,0.05]
			objParent.name = (obj.name+"_Parent")
			
			--align Dummy with object
			objParent.transform = obj.transform
			
			--parent object to Dummy
			obj.parent = objParent
		)
	)
)
