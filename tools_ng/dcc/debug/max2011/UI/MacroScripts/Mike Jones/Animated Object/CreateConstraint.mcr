macroScript CreateConstraint
	category:"Animated Object"
	toolTip:"Create Constraint"
	icon:#("Classic", 7)
(
	(
			--Create a point and orient constraint for the animated object
			
			obj = selection [1]
			
			if (obj == undefined) then
			(
				 messagebox "Nothing selected?"
			)
		
			else
			(
				--create Constraint object
				ConstraintParent = Point Size:0.1 Name:(obj.name+"_constraint")
				
				
				--Copy existing animation from anim object
				start = animationrange.start
				end = animationrange.end
				objectTM = #()

				index = 1
				
				for i = start to end do
				(
						slidertime = i
						objectTM[index] = obj.transform
						index+=1
				)

				--Add Constraints
				slidertime = start
				ConstraintParent.transform = obj.transform

				--add Pos Constraint
				--check to see if Position list is available first
				if (showproperties obj.position.controller == false) then
				(
					obj.position.controller = Position_list()
				) 
	
				obj.position.available.controller = Position_Constraint()
				posConstraintInterface = obj.position.controller.Position_Constraint.controller.constraints
				posConstraintInterface.appendTarget ConstraintParent 100.0
				
				--add Rot Constraint
				--check to see if Rotation list is available first
				if (showproperties obj.rotation.controller == false) then
				(
					obj.rotation.controller = Rotation_list()
				) 
				obj.rotation.available.controller = Orientation_Constraint()
				rotConstraintInterface = obj.rotation.controller.Orientation_Constraint.controller.constraints
				rotConstraintInterface.appendTarget ConstraintParent 100.0
				
				--Paste animation on to constraint object
				index = 1
				
				for i = start to end do
				(
						slidertime = i
						ConstraintParent.transform = objectTM[index]		
						index+=1
				)	
			)
	)
)
