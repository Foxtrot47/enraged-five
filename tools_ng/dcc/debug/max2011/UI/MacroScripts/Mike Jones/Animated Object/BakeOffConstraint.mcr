macroScript BakeOffConstraint
	category:"Animated Object"
	toolTip:"Bake Off Constraint"
	icon:#("bip_curve", 7)
(
	(
		(
			--Bake Constraints off an animated object 
			
			start = animationrange.start
			end = animationrange.end
			
			for object in selection do
			(
				objectTM = #()

				--copy transforms
				index = 1
				
				for i = start to end do
				(
						slidertime = i
						objectTM[index] = object.transform
						index+=1
				)
				
				--delete constraints
				ConstraintName = object.position.controller.Position_Constraint.constraints
				Target = ConstraintName.getNode 1
				
				controllerCount = object.position.controller.getCount()
				for i = 1 to controllerCount do
				(
					if ((object.position.controller.getname i)=="Position Constraint") 
						then 
							object.position.controller.delete i	
				)
				
				controllerCount = object.rotation.controller.getCount()
				for i = 1 to controllerCount do
				(
					if ((object.rotation.controller.getname i)=="Orientation Constraint") 
						then 
							object.rotation.controller.delete i
				)
				
				delete Target
				
				--paste transforms
				index = 1
				
				for i = start to end do
				(
						slidertime = i
						object.transform = objectTM[index]		
						index+=1
				)
			)
		)
	)
)
