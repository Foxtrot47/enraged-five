macroScript AttachAnimObject
	category:"Animated Object"
	toolTip:"Attach AnimObject to Biped"
	icon:#("Reactor", 18)
(
	(
			--Attach anim object to biped part 
			
			obj = selection
			
			if (selection.count != 2) then
			(
				 messagebox "Select two objects. Animated Object first, Biped part second."
			)
			
			else
			(	
				--align oject to Biped part and parent
				obj[1].parent.transform = obj[2].transform
				obj[1].parent.parent = obj[2]
			)
	)
)
