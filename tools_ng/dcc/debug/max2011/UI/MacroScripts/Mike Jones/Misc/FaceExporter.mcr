macroScript FaceExporter
	category:"Misc"
	toolTip:"Viseme Exporter. Select Biped first"
	icon:#("Material_Modifiers", 3)
(
	--///////////////////////////////////////////////////////////
	-- Face Exporter
	-- Exports Viseme anim files using R* exporter
	--
	-- USAGE:
	--
	-- 1. Load up MAX scene containing face shapes
	-- 2. Select biped part
	-- 3. Open R* anim exporter rollout
	-- 3. Run this script
	-- 4. Select which folders viseme anims need exporting to then press go.
	-- 5. Script should output anim files.
	--
	-- NOTE: Script will automatically detect 24 and 14 bone rigs and export appropriately
	-- Script will need the R* exporter rollout opeened as certain R* functions are required.
	--///////////////////////////////////////////////////////////
	
	global RsAnimRoll
	
	rollout faceExporter "Face Exporter" 
	(
		--/////////////////////////////////////////////////////////
		-- UI elements
		--/////////////////////////////////////////////////////////
		
		button viseme_button "Viseme Folder" width:280
		
		edittext viseme_text enabled:false

		group ""
		(
			button goButton "Go" enabled:false width:280
		)
		
		--/////////////////////////////////////////////////////////
		-- UI functions
		--/////////////////////////////////////////////////////////
		
		local facial_path, viseme_path
		
		on viseme_button pressed do
		(
			--get viseme folders
			viseme_path = getSavePath caption:"Select folder to export Facial anim files" initialDir:"X:\GTA\gta_art\anim\export"
	
			viseme_text.text = viseme_path 
			
			goButton.enabled = true
		)
		
		on goButton pressed do
		(
			--/////////////////////////////////////////////////////////
			-- Bone sets
			--/////////////////////////////////////////////////////////
			
			hires_bones = #(							
								$C_jawaJnt,
								$L_jawaJnt,
								$L_jawbJnt,
								$R_jawaJnt,
								$R_jawbJnt,
								$C_lowlidJnt,
								$L_browaJnt,
								$L_browbJnt,
								$L_cheekJnt,
								$L_corneraJnt,
								$L_cornerbJnt,
								$L_Eyejnt,
								$L_LidJnt,
								$L_LipUpaJnt,
								$R_browaJnt,
								$R_browbJnt,
								$R_cheekJnt,
								$R_corneraJnt,
								$R_cornerbJnt,
								$R_Eyejnt,
								$R_LidJnt,
								$R_LipUpaJnt,
								$C_tongue_b_Jnt,
								$C_tongue_a_Jnt
							)

			lores_bones = #(
								$FB_C_Cheeks,
								$FB_C_Brow,
								$PointFB_C_Jaw,
								$FB_C_Jaw,
								$PointFB_L_LipLower,
								$FB_L_LipLower,
								$PointFB_R_LipLower,
								$FB_R_LipLower,
								$FB_L_Brow,
								$FB_L_Crn_Mouth,
								$FB_L_Eyeball,
								$FB_L_Eyelid,
								$PointFB_L_LipUpper,
								$FB_L_LipUpper,
								$FB_R_Brow,
								$FB_R_Crn_Mouth,
								$FB_R_Eyeball,
								$FB_R_Eyelid,
								$PointFB_R_LipUpper,
								$FB_R_LipUpper
							)

			--/////////////////////////////////////////////////////////
			-- Facial/Viseme pose definitions
			-- Structures store which poses are exported with which bones
			--/////////////////////////////////////////////////////////
			
			--facial struct to store poses and respective selection sets
			struct facial_struct (posename, selectionset) 
	
			--definition for hi-res bone viseme poses
			hires_viseme_poses = #(
							facial_struct posename:"Rest" selectionset:#($C_jawaJnt,$L_jawaJnt,$L_jawbJnt,$R_jawaJnt,$R_jawbJnt,$L_corneraJnt,$L_cornerbJnt,$R_corneraJnt,$R_cornerbJnt,$L_LipUpaJnt,$R_LipUpaJnt,$C_tongue_b_Jnt,$C_tongue_a_Jnt),
							facial_struct posename:"A" selectionset:#($C_jawaJnt,$L_jawaJnt,$L_jawbJnt,$R_jawaJnt,$R_jawbJnt,$L_corneraJnt,$L_cornerbJnt,$R_corneraJnt,$R_cornerbJnt,$L_LipUpaJnt,$R_LipUpaJnt,$C_tongue_b_Jnt,$C_tongue_a_Jnt),
							facial_struct posename:"E" selectionset:#($C_jawaJnt,$L_jawaJnt,$L_jawbJnt,$R_jawaJnt,$R_jawbJnt,$L_corneraJnt,$L_cornerbJnt,$R_corneraJnt,$R_cornerbJnt,$L_LipUpaJnt,$R_LipUpaJnt,$C_tongue_b_Jnt,$C_tongue_a_Jnt),
							facial_struct posename:"FV" selectionset:#($C_jawaJnt,$L_jawaJnt,$L_jawbJnt,$R_jawaJnt,$R_jawbJnt,$L_corneraJnt,$L_cornerbJnt,$R_corneraJnt,$R_cornerbJnt,$L_LipUpaJnt,$R_LipUpaJnt,$C_tongue_b_Jnt,$C_tongue_a_Jnt),
							facial_struct posename:"LTH" selectionset:#($C_jawaJnt,$L_jawaJnt,$L_jawbJnt,$R_jawaJnt,$R_jawbJnt,$L_corneraJnt,$L_cornerbJnt,$R_corneraJnt,$R_cornerbJnt,$L_LipUpaJnt,$R_LipUpaJnt,$C_tongue_b_Jnt,$C_tongue_a_Jnt),
							facial_struct posename:"MBP" selectionset:#($C_jawaJnt,$L_jawaJnt,$L_jawbJnt,$R_jawaJnt,$R_jawbJnt,$L_corneraJnt,$L_cornerbJnt,$R_corneraJnt,$R_cornerbJnt,$L_LipUpaJnt,$R_LipUpaJnt,$C_tongue_b_Jnt,$C_tongue_a_Jnt),
							facial_struct posename:"OUWQ" selectionset:#($C_jawaJnt,$L_jawaJnt,$L_jawbJnt,$R_jawaJnt,$R_jawbJnt,$L_corneraJnt,$L_cornerbJnt,$R_corneraJnt,$R_cornerbJnt,$L_LipUpaJnt,$R_LipUpaJnt,$C_tongue_b_Jnt,$C_tongue_a_Jnt),
							facial_struct posename:"S" selectionset:#($C_jawaJnt,$L_jawaJnt,$L_jawbJnt,$R_jawaJnt,$R_jawbJnt,$L_corneraJnt,$L_cornerbJnt,$R_corneraJnt,$R_cornerbJnt,$L_LipUpaJnt,$R_LipUpaJnt,$C_tongue_b_Jnt,$C_tongue_a_Jnt)
							)
					
			--definition for lo-res bone viseme poses
			lores_viseme_poses = #(
							facial_struct posename:"Rest" selectionset:#($FB_L_Crn_Mouth, $FB_R_Crn_Mouth, $PointFB_C_Jaw, $FB_C_Jaw, $PointFB_L_LipLower, $FB_L_LipLower, $PointFB_R_LipLower, $FB_R_LipLower, $PointFB_L_LipUpper, $FB_L_LipUpper, $PointFB_R_LipUpper, $FB_R_LipUpper),
							facial_struct posename:"A" selectionset:#($FB_L_Crn_Mouth, $FB_R_Crn_Mouth, $PointFB_C_Jaw, $FB_C_Jaw, $PointFB_L_LipLower, $FB_L_LipLower, $PointFB_R_LipLower, $FB_R_LipLower, $PointFB_L_LipUpper, $FB_L_LipUpper, $PointFB_R_LipUpper, $FB_R_LipUpper),
							facial_struct posename:"E" selectionset:#($FB_L_Crn_Mouth, $FB_R_Crn_Mouth, $PointFB_C_Jaw, $FB_C_Jaw, $PointFB_L_LipLower, $FB_L_LipLower, $PointFB_R_LipLower, $FB_R_LipLower, $PointFB_L_LipUpper, $FB_L_LipUpper, $PointFB_R_LipUpper, $FB_R_LipUpper),
							facial_struct posename:"FV" selectionset:#($FB_L_Crn_Mouth, $FB_R_Crn_Mouth, $PointFB_C_Jaw, $FB_C_Jaw, $PointFB_L_LipLower, $FB_L_LipLower, $PointFB_R_LipLower, $FB_R_LipLower, $PointFB_L_LipUpper, $FB_L_LipUpper, $PointFB_R_LipUpper, $FB_R_LipUpper),
							facial_struct posename:"LTH" selectionset:#($FB_L_Crn_Mouth, $FB_R_Crn_Mouth, $PointFB_C_Jaw, $FB_C_Jaw, $PointFB_L_LipLower, $FB_L_LipLower, $PointFB_R_LipLower, $FB_R_LipLower, $PointFB_L_LipUpper, $FB_L_LipUpper, $PointFB_R_LipUpper, $FB_R_LipUpper),
							facial_struct posename:"MBP" selectionset:#($FB_L_Crn_Mouth, $FB_R_Crn_Mouth, $PointFB_C_Jaw, $FB_C_Jaw, $PointFB_L_LipLower, $FB_L_LipLower, $PointFB_R_LipLower, $FB_R_LipLower, $PointFB_L_LipUpper, $FB_L_LipUpper, $PointFB_R_LipUpper, $FB_R_LipUpper),
							facial_struct posename:"OUWQ" selectionset:#($FB_L_Crn_Mouth, $FB_R_Crn_Mouth, $PointFB_C_Jaw, $FB_C_Jaw, $PointFB_L_LipLower, $FB_L_LipLower, $PointFB_R_LipLower, $FB_R_LipLower, $PointFB_L_LipUpper, $FB_L_LipUpper, $PointFB_R_LipUpper, $FB_R_LipUpper),
							facial_struct posename:"S" selectionset:#($FB_L_Crn_Mouth, $FB_R_Crn_Mouth, $PointFB_C_Jaw, $FB_C_Jaw, $PointFB_L_LipLower, $FB_L_LipLower, $PointFB_R_LipLower, $FB_R_LipLower, $PointFB_L_LipUpper, $FB_L_LipUpper, $PointFB_R_LipUpper, $FB_R_LipUpper)
							)
			
			--/////////////////////////////////////////////////////////
			-- First determine whether character has a hi or low res facial rig
			--/////////////////////////////////////////////////////////

				local rig_type = 0, check_count = 0
				
				for bone in hires_bones do
				(
					if bone != undefined then check_count += 1
				)
					
				for bone in lores_bones do
				(
					if bone != undefined then check_count += 1
				)
				
				if check_count == lores_bones.count then rig_type = 1
				if check_count == hires_bones.count then rig_type = 2
				
				
			--/////////////////////////////////////////////////////////
			-- Main 
			--/////////////////////////////////////////////////////////
			
			if rig_type > 0 then
			-- perform relevant export if scene is valid
			(
				--set up slider ready for export
				animationrange = interval 0 7
				
				--/////////////////////////////////////////////////////////
				-- Export Visemes 
				--/////////////////////////////////////////////////////////
					
					if rig_type == 1 then
					-- export lo-res
					(
						print "Exporting lo-res Viseme Shapes..."
						for i = 1 to lores_viseme_poses.count do
						(
							select (lores_viseme_poses[i].selectionset) 
							RsAnimRoll.RestrictAnimToTwoFrames (lores_viseme_poses[i].selectionset) (i - 1) (viseme_path + "\\" + lores_viseme_poses[i].posename)
						)
					)
					
					else
					-- export hi-res
					(
						print "Exporting hi-res Viseme Shapes..."
						for i = 1 to hires_viseme_poses.count do
						(
							select (hires_viseme_poses[i].selectionset) 
							RsAnimRoll.RestrictAnimToTwoFrames (hires_viseme_poses[i].selectionset) (i - 1) (viseme_path + "\\" + hires_viseme_poses[i].posename)
						)

					)
					
				-- finished export
				print "Export Complete!"
				destroyDialog faceExporter
			)
			
			-- Error catch incorrect skeletons
			if rig_type == 0 then messagebox "Error: Incorrect number of bones found!"
		)
	)
	
	-- check selection, there should be a biped selected
	if (selection.count == 0) or ((classof selection[1])!= biped_object) 
	then
	(
		messagebox "Select Biped to export face shapes"
	)
	else
	(
		--create Face Export dialog, go...
		createDialog faceExporter width:300
	)
)