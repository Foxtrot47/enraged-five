macroScript bipRenderer
	category:"Misc"
	toolTip:"bipRenderer. Renders avis from multiple biped files."
	icon:#("Render", 7)
(
			fn updateTimeBar = 
			(
				--Update TimeBar so it fits keys
				minimum = 1000
				maximum = 0
			
				--loop through each object
				for object in objects do
				(
					--loop through any keys
					keys = object.controller.keys
					
					--check time of each key and compare against min and max
					for key in keys do
					(
						if key.time < minimum then minimum = key.time
						if key.time > maximum then maximum = key.time
					)
				)
				
				if (minimum-maximum == 0) then maximum+=1
			
				--update timebar with min and max
				animationrange = interval minimum maximum
			)
		
			rollout bipRenderer "Biped Renderer" 
			(
				--/////////////////////////////////////////////////////////
				-- UI elements
				--/////////////////////////////////////////////////////////
				
				button sourceButton "Bip Source Folder" width:280
				
				edittext sourceText enabled:false
				
				button targetButton "AVI Destination Folder" width:280
				
				edittext targetText enabled:false

				group ""
				(
					button goButton "Go" enabled:false width:280
				)
				
				--/////////////////////////////////////////////////////////
				-- UI functions
				--/////////////////////////////////////////////////////////
				
				local goButtonState = 0
				local bipPath, aviPath
				
				on sourceButton pressed do
				(
					--get bip folders 
					bipPath = getSavePath caption:"Select Bip parent folder to render" initialDir:"X:\GTA\gta_art\anim\exported_bips"
			
					sourceText.text = bipPath
					goButtonState += 1
					
					--if both source and destination folders are selected then enable go button
					if goButtonState > 1 then goButton.enabled = true
				)
				
				on targetButton pressed do
				(
					--get bip folders
					aviPath = getSavePath caption:"Select folder to save AVIs" initialDir:"X:\GTA\gta_art\anim\exported_aviz"
			
					targetText.text = aviPath 
					goButtonState += 1
					
					--if both source and destination folders are selected then enable go button
					if goButtonState > 1 then goButton.enabled = true
				)
				
				on goButton pressed do
				(
					--/////////////////////////////////////////////////////////
					-- get Bip root
					--/////////////////////////////////////////////////////////
					local bipObject
					
					for object in objects do
					(
						if ((classof object)==biped_object) then bipObject = biped.getNode object 14
					)
					
					--/////////////////////////////////////////////////////////
					-- get bip files
					--/////////////////////////////////////////////////////////
					local bipList = #()
					local dirList = #()
					
					join dirList (getDirectories (sourceText.text + "*"))
					
					for dirs in dirList do 
					--get sub folders
					(
						join dirList (getDirectories (dirs + "*"))
					)
					
					for dirs in dirList do 
					--get bip files
					(
						join bipList (getFiles (dirs + "*.bip"))
					)
					
					--/////////////////////////////////////////////////////////
					-- main loop
					-- 1. load in bip file
					-- 2. render each frame out to avi
					--/////////////////////////////////////////////////////////
					
					local files_done = 0
					
					progressStart "Generating avis..."
					
					for bipFile in bipList do
					(
						-- load bip file
						biped.loadBipFile bipObject.transform.controller bipFile
						
						-- select nothing
						clearSelection()
						
						--get filename
						filename = getFilenameFile bipFile
										
						-- render AVI
						preview_name = ((aviPath as string) + "//" + filename + ".avi")
				
						view_size = getViewSize()
						
						anim_bmp = bitmap (view_size.x/2) (view_size.y/2) filename:preview_name
						
						--update timebar to fit animation extents
						updateTimeBar() 
						
						for t = animationrange.start to animationrange.end do
						(
							--update time
							sliderTime = t
							
							-- zoom camera
							-- max tool zoomextents
							
							--grab image	
							dib = gw.getViewportDib()
						
							copy dib anim_bmp
						
							save anim_bmp	
						)
						close anim_bmp
						gc()
						
						--update file count
						files_done += 1
						
						--update progress bar
						if not (progressUpdate (100.0 * (files_done/(bipList.count)))) then exit
					)
					
					progressEnd()
				)
			)
			
			createDialog bipRenderer width:300
)
