macroScript bvhToBip
	category:"Misc"
	toolTip:"bvhToBip. Converts a folder of bvh anims to bip format."
	icon:#("Radiosity", 4)
(
			fn updateTimeBar = 
			(
				--Update TimeBar so it fits keys
				minimum = 1000
				maximum = 0
			
				--loop through each object
				for object in objects do
				(
					--loop through any keys
					keys = object.controller.keys
					
					--check time of each key and compare against min and max
					for key in keys do
					(
						if key.time < minimum then minimum = key.time
						if key.time > maximum then maximum = key.time
					)
				)
				
				if (minimum-maximum == 0) then maximum+=1
			
				--update timebar with min and max
				animationrange = interval minimum maximum
			)
		
			rollout bipRenderer "BVH to BIP exporter" 
			(
				--/////////////////////////////////////////////////////////
				-- UI elements
				--/////////////////////////////////////////////////////////
				
				button sourceButton "BVH Source Folder" width:280
				
				edittext sourceText enabled:false
				
				button targetButton "BIP Destination Folder" width:280
				
				edittext targetText enabled:false

				group ""
				(
					button goButton "Go" enabled:false width:280
				)
				
				--/////////////////////////////////////////////////////////
				-- UI functions
				--/////////////////////////////////////////////////////////
				
				local goButtonState = 0
				local bvhPath, bipPath
				
				on sourceButton pressed do
				(
					--get bvh folder 
					bvhPath = getSavePath caption:"Select folder with BVH files" initialDir:"X:\GTA\gta_art\anim\exported_bips"
			
					sourceText.text = bvhPath
					goButtonState += 1
					
					--if both source and destination folders are selected then enable go button
					if goButtonState > 1 then goButton.enabled = true
				)
				
				on targetButton pressed do
				(
					--get bip folders
					bipPath = getSavePath caption:"Select folder to save BIPs" initialDir:"X:\GTA\gta_art\anim\exported_bips"
			
					targetText.text = bipPath 
					goButtonState += 1
					
					--if both source and destination folders are selected then enable go button
					if goButtonState > 1 then goButton.enabled = true
				)
				
				on goButton pressed do
				(
					--/////////////////////////////////////////////////////////
					-- get Bip root
					--/////////////////////////////////////////////////////////
					local bipObject
					
					bipObject = biped.getNode selection[1] 14
					
					--/////////////////////////////////////////////////////////
					-- get bvh files
					--/////////////////////////////////////////////////////////
					local bvhList = #()
					local dirList = #()
					
					join dirList (getDirectories (sourceText.text + "*"))
						
					for dirs in dirList do 
					--get bvh files
					(
						join bvhList(getFiles (dirs + "*.bvh"))
					)
						
					--/////////////////////////////////////////////////////////
					-- main loop
					-- 1. load in bvh file
					-- 2. save back out as bip file
					--/////////////////////////////////////////////////////////
					
					local files_done = 0
										
					for bvhFile in bvhList do
					(
						-- load bvh file
						biped.loadMocapFile bipObject.transform.controller bvhFile
										
						--update timebar to fit animation extents
						updateTimeBar() 
								
						--get filename
						filename = getFilenameFile bvhFile
										
						--save back out as bip file
						biped.saveBipFile bipObject.transform.controller (bipPath + "/" + filename + ".bip")
						
						--update file count
						files_done += 1
						
						--update progress
						print (files_done as string + "/" + bvhList.count as string + " bvh files converted...")
					)
					
					progressEnd()
				)
			)
			
			if (selection.count == 0) or ((classof selection[1])!= biped_object) 
				then messagebox "Select Biped to export Bip files with"
			else createDialog bipRenderer width:300
)
