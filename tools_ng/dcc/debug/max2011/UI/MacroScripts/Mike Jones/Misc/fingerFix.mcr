macroScript fingerFix
	category:"Misc"
	toolTip:"Copies animation from third finger to fourth."
	icon:#("FileLinkActionItems", 3)
(
	query = queryBox "Fix little finger?" title:"Finger fix"
	if query == true then 
	(	
		with animate on (
	
			for i = animationrange.start to animationrange.end do
			(
				slidertime = i
				
				biped.settransform $'Char R Finger4' #rotation $'Char R Finger3'.transform.rotation true
				biped.settransform $'Char R Finger41' #rotation $'Char R Finger31'.transform.rotation true
				biped.settransform $'Char R Finger42' #rotation $'Char R Finger32'.transform.rotation true
				biped.settransform $'Char L Finger4' #rotation $'Char L Finger3'.transform.rotation true
			 	biped.settransform $'Char L Finger41' #rotation $'Char L Finger31'.transform.rotation true
				biped.settransform $'Char L Finger42' #rotation $'Char L Finger32'.transform.rotation true
			)
		)
	)
)
