macroScript MorphMaker
	category:"Misc"
	toolTip:"Morph Maker. Select one mesh object and set up timeslider"
	icon:#("PowerBoolean", 1)
(
	--/////////////////////////////////////////////////////////////////////
	-- Morph Maker
	--
	-- Select one mesh object and set up timeslider range before running
	-- Mike Jones
	--/////////////////////////////////////////////////////////////////////
	
	if (selection.count == 0) then messagebox "Select object to produce morphs"
	else
	(
		object = selection[1]
		count = 0
		
		for i = animationrange.start to animationrange.end do
		(
		
			--update timeslider
			slidertime = i
			
			--create snapshot
			morph = snapshot object
			
			--name it via frame time
			morph.name = ("Morph_" + (count as string))
			
			--position it relative to the selection and each other
			morph.pos.x = object.pos.x - 0.5 - (0.25 * count)
			morph.pos.y = object.pos.y 
			morph.pos.z = object.pos.z + 0.5
			
			--increment count to offset morph pos
			count += 1
		)
	)
)