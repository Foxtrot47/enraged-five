macroScript WeaponCreator
	category:"Misc"
	toolTip:"Weapon Creator. Select Biped to attach weapon to."
	icon:#("TrackViewStatus", 10)
(
	--list of available weapons in CJs MAX scene
	weaponsList = #(
					"W_AK47",
					"W_BAT",
					"W_CUE",
					"W_GLOCK",
					"W_GRENADE",
					"W_MOLOTOV",
					"W_MP5",
					"W_NSTICK",
					"W_PSG1",
					"W_SHOTGUN",
					"W_UZI",
					"W_PUMPSHOT"
					)
	
	--store animbutton state and temp turn it off
	autokeystate = animbuttonstate
	animbuttonstate = false
				
	if (selection.count == 0) or ((classof selection[1])!= biped_object) then messagebox "Select Biped to attach weapon"
	else
	(		
		rollout weaponSelector "Weapon Creator" 
		(
			--////////////////////////////////////////////////////////////////////////////////////////
			-- create listbox with a list of available weapons
			--////////////////////////////////////////////////////////////////////////////////////////
			
			listbox weaponListBox "Weapons:" items:weaponsList height:15
			
			label weaponLabelA "Double Click on weapon - " align:#left
			label weaponLabelB "attaches to Biped R Hand." align:#left
			label weaponLabelC ""
			
			--////////////////////////////////////////////////////////////////////////////////////////
			-- When user double clicks on the weapon name - intiate load
			-- (Main)
			--///////////////////////////////////////////////////////////////////////////////////////
			
			on weaponListBox doubleClicked itm do 
			(
				--store selected biped_part
				biped_part = selection[1]
				
				--get Right hand to attach weapon to
				biped_rhand = biped.getnode biped_part #rarm link:4
			
				--////////////////////////////////////////////////////////////////////////////////////////
				-- load all weapon nodes in from scene
				--////////////////////////////////////////////////////////////////////////////////////////
				
				mergeMAXFile "X:\GTA\gta_art\models\Props\LEV_DES\WeaponsCJ.max" #select #mergeDups #renameMtlDups quiet:false 
			
				nodes = #()
				
				for object in selection do
				(
					if (object.name as name) == (weaponListBox.selected as name) then
					(			
						nodes[1] = object
				
						for child in object.children do
						(
							nodes[nodes.count + 1] = child
						)
						exit
					)
				)
				
				--////////////////////////////////////////////////////////////////////////////////////////
				-- remove unwanted weapons from scene
				--////////////////////////////////////////////////////////////////////////////////////////	
			
				--remove nodes from selection
				deselect nodes
				
				--delete selection
				delete selection
				
				--////////////////////////////////////////////////////////////////////////////////////////
				-- put weapon into layer
				--////////////////////////////////////////////////////////////////////////////////////////	
				
				weaponlayer = layermanager.newlayerfromname nodes[1].name
				weaponlayer.isfrozen = true 
				
				for node in nodes do 
				(
					--move to weapon layer
					weaponlayer.addnode node
					
					--hide all
					node.ishidden = true
				)
				
				--unhide weapon
				nodes[1].ishidden = false
				
				--////////////////////////////////////////////////////////////////////////////////////////
				-- reparent weapon to gungrip
				--////////////////////////////////////////////////////////////////////////////////////////
				
				--unparent gun_grip
				$gun_grip.parent = undefined
				
				--parent gun to gun_grip
				nodes[1].parent = $gun_grip
				
				--align gun to biped_part
				$gun_grip.transform = biped_rhand.transform
				
				--parent gun to biped_part
				$gun_grip.parent = biped_rhand
				
				--return animbuttonstate 
				animbuttonstate = autokeystate 
				
				--////////////////////////////////////////////////////////////////////////////////////////
				-- Close rollout 
				--////////////////////////////////////////////////////////////////////////////////////////
				DestroyDialog weaponSelector 
			)
		)
		
		createDialog weaponSelector 
	)	
)
