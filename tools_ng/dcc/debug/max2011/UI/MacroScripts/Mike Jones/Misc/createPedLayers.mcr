macroScript createPedLayers
	category:"Misc"
	toolTip:"Creates layers for biped parts, mesh and misc items."
(
	rollout layerPedPrefix "Layer Prefix" 
	(
		--define an edittext with width 100 and the label on top:
		edittext prefix_txt "Layer prefix:" fieldWidth:200 labelOnTop:true
		
		 
		--If the user entered a new name...
		on prefix_txt entered txt do
		(
			----------------------------------------------------------------------------
			--create layers for ped 
			bipedlayer = layermanager.newlayerfromname (txt + "Biped")
			meshlayer = layermanager.newlayerfromname (txt + "Mesh")
			defaultlayer = layermanager.newlayerfromname (txt + "Default")
			defaultlayer.ishidden = true
	
			----------------------------------------------------------------------------
	
			for object in objects do
			(
				--if biped object
				if ((classof object) == biped_object) then 
				(
					bipedlayer.addnode object
					object.boxmode = false
					object.showlinks = false
					object.xray = false
					object.showVertexColors = false
					
					bonenum = biped.getIdLink object 
					if ((bonenum.x > 17) or (bonenum.x == 16)) then defaultlayer.addnode object
				)
				
				--if mesh object
				if ((classof object) == PolyMeshObject) then meshlayer.addnode object
				
				--if neither
				if (((classof object) != biped_object) and \
					((classof object) != PolyMeshObject)) then defaultlayer.addnode object
			)
			----------------------------------------------------------------------------
			
			DestroyDialog layerPedPrefix 

		)
	)
	
	createDialog layerPedPrefix 300 50  
)
