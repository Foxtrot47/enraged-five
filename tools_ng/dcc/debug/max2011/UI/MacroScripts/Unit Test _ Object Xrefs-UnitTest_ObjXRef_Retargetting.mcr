macroScript UnitTest_ObjXRef_Retargetting
	ButtonText:"UnitTest_ObjXRef_Retargetting"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Test - Object Xrefs" 
	Tooltip:"Unit Test - Obj Xrefs - Retargetting" 
(	
--------------------------------------------------------------------------------
-- Globals to this macro
--------------------------------------------------------------------------------
-- The temp files must be in the scene folder so that stripping the path of
-- xrefs does not render them unresolved
global gTempFile1 = (getdir #Scene) + "\\UT.ObjXref.Retargetting.01.max"	
global gTempFile2 = (getdir #Scene) + "\\UT.ObjXref.Retargetting.02.max"		
	
----------------------------------------------------
-- Utility methods
----------------------------------------------------
-- Tests retargetting an xref record
-- Retargets an xref record from one valid file to another valid file
-- \post There is only one xref record whose source file name is the new file
function doTestRetargetXrefRecord oldSrcFile newSrcFile useObjXrefMgrMethod =
(
	ts.assert (1 == objXrefMgr.recordCount) "Check xrefRec count before retargetting"
	local xRec = objXrefMgr.getRecord 1
	ts.assert (oldSrcFile == xRec.srcFileName) ("Check xrefRec.srcFileName before retargetting: " + xRec.srcFileName)
	
	if ((getFilenamePath newSrcFile).count > 0) then (
		ts.assert(1 == (getFiles newSrcFile).count) ("Check new src file exists: " + newSrcFile)
	)
		
	-- Retarget xref record to source file 2
	if (useObjXrefMgrMethod) then 
	(
		local res = objXrefMgr.SetRecordSrcFile  xRec newSrcFile
		ts.assert (true == res) "Check - record retargetted using objXRefMgr method"
	)
	else 
	(
		xRec.srcFileName = newSrcFile
	)
	
	ts.assert (1 == objXrefMgr.recordCount) "Check xrefRec count after retargetting"
	xRec = objXrefMgr.getRecord 1
	ts.assert (newSrcFile == xRec.srcFileName) ("Xref record retargetted to: " + xRec.srcFileName)
)

-- Tests retargetting of al xref items within a record
-- Retargets all xref item of a record from one valid file to another valid file
-- \post There is only one xref record whose source file name is the new file
function doTestRetargetXRefItems oldSrcFile newSrcFile useObjXrefMgrMethod = 
(
	ts.assert (1 == objXrefMgr.recordCount) "Check xrefRec count before retargetting"
	local xRec = objXrefMgr.getRecord 1
	ts.assert (oldSrcFile == xRec.srcFileName) ("Check xrefRec.srcFileName before retargetting: " + xRec.srcFileName)
	local xItemCount = xRec.ItemCount #(#XRefAllType)
	ts.assert (xItemCount > 0) "Check xrefItem count before retargetting"
	
	if ((getFilenamePath newSrcFile).count > 0) then (
		ts.assert(1 == (getFiles newSrcFile).count) ("Check new src file exists: " + newSrcFile)
	)
		
	-- Retarget xref items one by one
	local numRetargetableXrefs = 0
	local xItems = #()
	xRec.GetItems #(#XRefAllType) &xItems 
	-- Turn quiet mode on, to prevent "partial system found" message from popping up
	with quiet true 
	(
		for x in xItems do 
		(
			ts.assert (oldSrcFile == x.srcFileName) ("Check xrefItem.srcFileName before retargetting: " + x.srcFileName)
			if (useObjXrefMgrMethod) then (
				local res = objXrefMgr.SetXRefItemSrcFile x newSrcFile
			)
			else (
				x.srcFileName = newSrcFile
			)
	
			if (x.retargetable) then (
				numRetargetableXrefs = numRetargetableXrefs + 1
				-- Xref Item retargetting is expected to be successful
				ts.assert (newSrcFile == x.srcFileName) ("Check xrefItem retargetted successfully: " + x.srcFileName)
			)
			else (
				-- Xref Item retargetting should fail
				ts.assert (oldSrcFile == x.srcFileName) ("Check xrefItem not retargetted: " + x.srcFileName)
			)
		)
	)	
	
	if (numRetargetableXrefs == xItemCount) then 
	(
		-- if all xref items were retargetable, they all should've been moved to a new xref record
		ts.assert (1 == objXrefMgr.recordCount) "Check xrefRec count after retargetting"
		xRec = objXrefMgr.getRecord 1
		ts.assert (newSrcFile == xRec.srcFileName) ("Xref record retargetted to: " + xRec.srcFileName)
	)
	else if (numRetargetableXrefs > 0 and numRetargetableXrefs < xItemCount) then 
	(
		-- The non-retargetable xref items should still live in their original xref record
		ts.assert (2 == objXrefMgr.recordCount) "Check xrefRec count after retargetting"
		local xRec1 = objXrefMgr.getRecord 1
		local xRec2 = objXrefMgr.getRecord 2
		ts.assert ((oldSrcFile == xRec1.srcFileName and newSrcFile == xRec2.srcFileName) or \
							(oldSrcFile == xRec2.srcFileName and newSrcFile == xRec1.srcFileName)) \
							("Xref record not retargetted: " + xRec.srcFileName)		
	)
	else 
	(
		-- None of the xref items were retargetable
		ts.assert (1 == objXrefMgr.recordCount) "Check xrefRec count after retargetting"		
		xRec = objXrefMgr.getRecord 1
		ts.assert (oldSrcFile == xRec.srcFileName) ("Xref record not retargetted: " + xRec.srcFileName)
	)
)

--------------------------------------------
-- Unit Test methods
--------------------------------------------	
-- Unit test fixture setup
function setupUT =
(
	resetMaxFile #noprompt
	
	-- create a source file and save it twice
	sphere material:(StandardMaterial diffuse:red ambient:red) pos:[0, 0, 100]
	ringArray.Create()
		
	-- Save out source files: save the scene twice into 2 identical source files
	local res = oxUtil.saveTmpFile gTempFile1
	ts.assert (true == res) ("Save Temp File 1: " + gTempFile1)
	local res = oxUtil.saveTmpFile gTempFile2
	ts.assert (true == res) ("Save Temp File 2: " + gTempFile2)
	resetMaxFile #noprompt
	
	-- Xref all obj, ctrl and mtl from the source file 1
	objXrefMgr.mergeTransforms = false
	objXrefMgr.mergeMaterials = false
	local xRec = objXrefMgr.AddXRefItemsFromFile gTempFile1
	ts.assert (undefined != xRec) "Xref created"
)

-- Unit test fixture teardown
function teardownUT =
(
	-- no scene tear down required
)

-- Tests retargetting of an xref record
function testRetargetXrefRecord =
(
	local retargetUsingObjXrefMgr = false	
	doTestRetargetXRefRecord gTempFile1 gTempFile2 retargetUsingObjXrefMgr
)

-- Tests stripping the path of an xref record
function testStripPathXrefRecord =
(
	local retargetUsingObjXrefMgr = false
	doTestRetargetXRefRecord gTempFile1 (filenameFromPath gTempFile1) retargetUsingObjXrefMgr
)

-- Tests stripping the path of an xref record using objXrefMgr method
function testStripPathXrefRecordUsingObjXRefMgr =
(
	local retargetUsingObjXrefMgr = true
	doTestRetargetXRefRecord gTempFile1 (filenameFromPath gTempFile1) retargetUsingObjXrefMgr
)

function testStripAndRestoreXRefRecordPath = 
(
	local retargetUsingObjXrefMgr = true
	doTestRetargetXRefRecord gTempFile1 (filenameFromPath gTempFile1) retargetUsingObjXrefMgr
	doTestRetargetXRefRecord (filenameFromPath gTempFile1) gTempFile1 retargetUsingObjXrefMgr
)

-- Tests retargetting of xref items
function testRetargetXrefItems =
(
	local retargetUsingObjXrefMgr = false
	doTestRetargetXRefItems gTempFile1 gTempFile2 retargetUsingObjXrefMgr
)

-- Tests stripping the path of xref items
function testStripPathXrefItems =
(
	local retargetUsingObjXrefMgr = false
	doTestRetargetXRefItems gTempFile1 (filenameFromPath gTempFile1) retargetUsingObjXrefMgr
)
	
-- Tests stripping the path of xref items using objXrefMgr method
function testStripPathXrefItemsUsingObjXrefMgr =
(
	local retargetUsingObjXrefMgr = true
	doTestRetargetXRefItems gTempFile1 (filenameFromPath gTempFile1) retargetUsingObjXrefMgr
)

--------------------------------------------
-- Macro methods
--------------------------------------------
On isEnabled return true
On isVisible return true

global createTestSuite = undefined
global ts = undefined
On Execute Do	
(
	-- Create instance of xref utility "object"
  if undefined == createObjXRefUtil do (
  	fileIn ((getdir #scripts) + "/startup/MxsUnitTest/ObjXRef/OBjXRef.Util.ms")
  )
	oxUtil = createObjXRefUtil()

	if createTestSuite == undefined do (
		fileIn ((getdir #scripts) + "/startup/MxsUnitTest/MxsUnitTestFramework.ms")
	)

  -- create a test suite that outputs cmmands into a log file and messages into a new maxscript window (default behaviour)
  -- local logFileName = ((getdir #maxroot) + "DumpObjXRefMgr.log")
  -- global xrefDumpWnd = newScript()
  ts = createTestSuite "UnitTest_ObjXRef_Retargetting" setupUT teardownUT -- cmdLog:undefined msgLog:xrefDumpWnd
	
	-- Test retargetting xref records
	ts.addTest testRetargetXrefRecord
	ts.addTest testStripPathXrefRecord
	ts.addTest testStripPathXrefRecordUsingObjXRefMgr
	ts.addTest testStripAndRestoreXRefRecordPath
	-- TODO: nested xref records, they should not be retargetable
	
	-- Test retargetting xref items
	ts.addTest testRetargetXrefItems
	ts.addTest testStripPathXrefItems
	ts.addTest testStripPathXrefItemsUsingObjXrefMgr
	
	-- TODO: add tests for proxy xref records\items
	-- TODO: add tests for nested xref records\items
	ts.run()
)

) -- END MACRO
