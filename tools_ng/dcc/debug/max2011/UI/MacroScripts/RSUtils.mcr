-- Rockstar Utils macros
-- Rockstar North
-- 2/2/2005
-- by Greg Smith

-- ///////////////////////////////////////////////////////////////////////////
-- utils
-- ///////////////////////////////////////////////////////////////////////////

macroscript CustomLauncher
	category:"RS Utils"
	ButtonText:"Custom Game Launcher"
(
	-- Store where the launcher bat file is     
	LauncherBat = ( systemTools.getEnvVariable "RS_PROJROOT" ) + "/tools/script/util/game_loader.bat"
	
	-- Go!
	ShellLaunch LauncherBat ""
)

macroscript MapBrowser
	category:"RS Utils"
	ButtonText:"Map Browser"
(
	local mapBrowserFilename = ( systemTools.getEnvVariable "RS_TOOLSROOT" ) + "/bin/MapBrowser/MapBrowser.exe"
	ShellLaunch mapBrowserFilename ""
)

macroscript ShowSelector
	category:"RS Utils"
	ButtonText:"Object Selector"
(	
	filein "pipeline\\util\\selector.ms"
)

macroscript BatchUpdate
	category:"RS Utils"
	ButtonText:"Batch Update"
(	
	filein "pipeline\\util\\batchupdate.ms"
)

macroscript StopProgress
	category:"RS Utils"
	ButtonText:"Stop Progress"
(
	progressend()
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Organisational Tools
-- ///////////////////////////////////////////////////////////////////////////

macroscript Bugstar
	category:"RS Organisational"
	tooltip:"Bugstar"
	ButtonText:"Bugstar"
(	
	filein "rockstar\\helpers\\bugstar.ms"
)

macroscript VisibilitySet
	category:"RS Organisational"
	tooltip:"Set the hide and freeze of objects by type"
	ButtonText:"Visibility Set"
(	
	filein "pipeline/helpers/maps/visibility_toolkit.ms"
)

macroscript CreateSelectionSets
	category:"RS Organisational"
	tooltip:"Create useful selection sets"
	ButtonText:"Create Selection Sets"
(	
	filein "pipeline/helpers/maps/CreateSelSets.ms"
)

macroscript ProjMan
	category:"RS Organisational"
	ButtonText:"Project Manager"
(	
	filein "pipeline/util/projman.ms"
)

macroscript Containers
	category:"RS Organisational"
	ButtonText:"Containers"
(
	filein "pipeline/ui/containers.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Placement tools
-- ///////////////////////////////////////////////////////////////////////////

macroscript PlacementToolkit
	category:"RS Placement"
	ButtonText:"Placement Toolkit"
(	
	filein "pipeline\\util\\placement.ms"
)

macroScript ObjPosChecker
	category:"RS Placement"
	ButtonText:"Physical Object Position Checker"
(
	filein "pipeline/ui/ObjectPosChecker.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Lighting
-- ///////////////////////////////////////////////////////////////////////////

macroscript RadiosityLighting
	category:"RS Lighting"
	tooltip:"Radiosity and lighting toolset"
	ButtonText:"Radiosity and Lighting"
(	
	filein "pipeline\\util\\radiosity_lighting.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Characters
-- ///////////////////////////////////////////////////////////////////////////

macroscript RsAudioTagger
	category: "RS Characters"
	tooltip: "Ped Audio Tag"
	ButtonText: "Ped Audio Tag"
(
	filein "rockstar/helpers/pedaudiotag.ms"
)

macroscript RsPedSyncTool
	category: "RS Characters"
	tooltip: "Ped Sync Tool"
	ButtonText: "Sync Tool"
(
	filein "rockstar/helpers/pedsync.ms"
)

macroscript RsPedVarManager
	category: "RS Characters"
	tooltip: "Ped Var Manager"
	ButtonText: "Ped Var Manager"
(
	filein "rockstar/helpers/pedvarcutmgr.ms"
)

macroscript RsMirroringPerVertTool
	category: "RS Characters"
	tooltip: "Vertex Mirroring Tool"
	ButtonText: "Vertex Mirroring Tool"
(
	filein "pipeline\\helpers\\rigging\\mirroringpervert.ms"
)
-- ///////////////////////////////////////////////////////////////////////////
-- Rs Interiors and Props
-- ///////////////////////////////////////////////////////////////////////////

macroscript PropsMilos
	category: "RS Interiors"
	ButtonText: "Props and MILOs"
(
	filein "rockstar\\helpers\\milo.ms"
)

macroscript MiloOperations
	category: "RS Interiors"
	ButtonText: "MILO Operations"
(
	filein "pipeline\\helpers\\interiors\\milo_ops.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Collision
-- ///////////////////////////////////////////////////////////////////////////

macroScript CollToolkit
	category:"RS Collision"
	tooltip:"Collision Toolkit"
	ButtonText:"Collision Toolkit"
(
	filein "pipeline/helpers/collision/coll_toolkit.ms"
)

macroScript AudioMatCheck
	category:"RS Collision"
	tooltip:"Audio Material Check"
	ButtonText:"Audio Material Check"
(
	filein "rockstar/helpers/collaudiocheck.ms"
)

macroScript CollRenamer
	category:"RS Collision"
	tooltip:"Rename All Collision"
	ButtonText:"Rename All Collision"
(
	filein "rockstar/helpers/collrenamer.ms"
)

macroScript ProceduralTypes
	category:"RS Collision"
	tooltip:"Procedural Types"
	ButtonText:"Procedural Types"
(
	filein "rockstar/helpers/proc_types.ms"
)

macroScript RsCollisionSet
	category:"RS Collision"
	tooltip:"Collision Set"
	ButtonText:"Collision Set"
(
	filein "rockstar/helpers/collset.ms"
)

macroscript OptimisePlanar
	category:"RS Collision"
	ButtonText:"Optimise Planar Polys"
(
	include "pipeline/util/UtilityFunc.ms"	
	
	OptimisePlanarPolys()
)

macroscript SetBoundMaterials
	category:"RS Collision"
	tooltip:"Set bound materials"
	ButtonText:"Bound Material Toggle"
(
	filein "rockstar\\helpers\\boundmaterialflagtoggle.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Xrefs
-- ///////////////////////////////////////////////////////////////////////////

macroscript XrefToolkit
	category: "RS Xrefs"
	ButtonText: "Xref Toolkit"
(
	filein "pipeline/helpers/maps/xref_toolkit.ms"
)

macroscript PropViewer
	category:"RS Xrefs"
	ButtonText:"Prop Viewer"
(
	local propViewerFilename = ( systemTools.getEnvVariable "RS_TOOLSROOT" ) + "/bin/PropViewer/PropViewer.exe"
	ShellLaunch propViewerFilename ""
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Modelling
-- ///////////////////////////////////////////////////////////////////////////

macroScript ModelToolkit
	category:"RS Modelling"
	tooltip:"Modelling Toolkit"
	ButtonText:"Modelling Toolkit"
(
	filein "pipeline/helpers/maps/modelling.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Material
-- ///////////////////////////////////////////////////////////////////////////

macroScript MaterialToolkit
	category:"RS Material"
	tooltip:"Material Toolkit"
	ButtonText:"Material Toolkit"
(
	filein "pipeline/helpers/materials/material_toolkit.ms"
)

macroScript ChangeShaders
	category:"RS Material"
	tooltip:"Change shader attributes"
	ButtonText:"Change Shaders"
(
	filein "pipeline/helpers/materials/changeshaders.ms"
)

macroScript ReplaceShaders
	category:"RS Material"
	tooltip:"Replace one shader with another"
	ButtonText:"Replace Shaders"
(
	filein "pipeline/helpers/materials/replaceshaders.ms"
)

macroScript BadTex
	category:"RS Material"
	tooltip:"Bad Texture Finder"
	ButtonText:"Bad Texture Finder"
(
	filein "rockstar/helpers/badtex.ms"
)

macroScript RsTerrainSetup
	category:"RS Material"
	tooltip:"Setup for multipass"
	ButtonText:"Terrain Setup"
(
	filein "rockstar/helpers/terrain.ms"
)

macroScript RsTerrainCbPaint
	category:"RS Material"
	tooltip:"Setup for terrain painting"
	ButtonText:"Terrain Paint"
(
	filein "rockstar/helpers/terrain_painter.ms"
)

macroscript TexturePathRemap
	category:"RS Material"
	tooltip:"Texture Path Remap"
	ButtonText:"Texture Path Remap"
(
	filein "pipeline/helpers/materials/texturepathremap.ms"
)

macroscript VertexColourTools
	category:"RS Material"
	ButtonText:"Vertex Colour Tools"
(	
	filein "pipeline/ui/vertexcolourtools.ms"
)

macroscript EnvironmentMap
	category:"RS Material"
	tooltip:"Create cubic envmap based on surroundings"
	ButtonText:"Environment Map Generator"
(
	filein "rockstar\\helpers\\environmentmap.ms"
)

macroscript ShowMapsInViewport
	category:"RS Material"
	tooltip:"Show/hide diffuse texture maps for Rage Shader materials"
	ButtonText:"Show Maps in Viewport"
(
	filein "rockstar\\helpers\\ShowMapsInViewport.ms"
)

macroscript GrassPainter
	category:"RS Material"
	tooltip:"Tools for editing grass and tree materials"
	ButtonText:"Grass Painter"
(
	filein "pipeline\\helpers\\materials\\GrassPainter.ms"
)

macroscript VertAnglePainter
	category:"RS Material"
	ButtonText:"Vert Angle Painter"
(
	filein "rockstar\\helpers\\VertAnglePaint.ms"
)

macroscript ProxyTextureSwap
	category:"RS Material"
	ButtonText:"Proxy texture Swap"
(
	filein "pipeline\\helpers\\materials\\proxytexturemgr.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Info
-- ///////////////////////////////////////////////////////////////////////////

macroscript InfoToolkit
	category:"RS Info"
	ButtonText:"Info Toolkit"
(	
	filein "pipeline/util/info_toolkit.ms"	
)

macroscript DoShowSceneStats
	category:"RS Info"
	ButtonText:"Scene Stats"
(	
	filein "rockstar\\helpers\\scenestats.ms"
)

macroscript DoShowSceneImage
	category:"RS Info"
	ButtonText:"Scene Image"
(	
	filein "rockstar\\helpers\\sceneimage.ms"
)

macroscript DoShowTextureStats
	category:"RS Info"
	ButtonText:"Texture Stats"
(
	filein "rockstar\\helpers\\TextureStats.ms"
)

macroscript DoShowTXDStats
	category:"RS Info"
	ButtonText:"Texture TXD Stats"
(
	filein "rockstar\\helpers\\TextureStatsTXD.ms"
)

macroscript ImportDebugNodes
	category:"RS Info"
	ButtonText:"Import Debug Nodes"
(
	filein "pipeline\\import\\ui\\ImportDebugNodes.ms"
)	

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Lod
-- ///////////////////////////////////////////////////////////////////////////

macroscript LodEditor
	category:"RS Lod"
	ButtonText:"LOD Editor"
(	
	filein "pipeline\\ui\\lodeditor.ms"
)

macroscript LodToolkit
	category:"RS Lod"
	ButtonText:"LOD Toolkit"
(	
	filein "pipeline\\util\\lod_toolkit.ms"
)

macroscript LodModUtil
	category:"RS Lod"
	ButtonText:"Lod Modifier"
(	
	filein "rockstar\\helpers\\lodmod.ms"
)

macroscript LodContainerTool
	category:"RS Lod"
	ButtonText:"Container LOD Editor"
(
	filein "pipeline/ui/containerlodeditor.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Internal Refs
-- ///////////////////////////////////////////////////////////////////////////

macroscript IrefToolkit
	category:"RS InternalRefs"
	ButtonText:"Iref Toolkit"
(	
	filein "pipeline/helpers/maps/iref_toolkit.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs VFX
-- ///////////////////////////////////////////////////////////////////////////

macroscript VFXToolkit
	category:"RS VFX"
	ButtonText:"VFX Toolkit"
(	
	filein "pipeline/util/vfx.ms"
)

macroscript StatedAnimationSetup
	category:"RS VFX"
	ButtonText:"Stated Animation Setup"
(	
	filein "pipeline/util/statedAnimTool.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Vehicle
-- ///////////////////////////////////////////////////////////////////////////

macroScript VehicleToolkit
	category:"RS Vehicle"
	ButtonText:"Vehicle Toolkit"
(
	filein "pipeline/helpers/vehicles/vehicle.ms"
)

macroscript RsImportVehicleRec
	category:"RS Vehicle"
	ButtonText:"Import Vehicle Recording"
(	
	include "pipeline/import/importvehiclerecording.ms"
	recordingFilename = getOpenFileName caption:"Select a vehicle recording .ivr file" types:"Vehicle recording (*.ivr)|*.ivr" 
	vehicleFilename = getOpenFileName caption:"Select a vehicle max file" types:"Vehicle (*.max)|*.max" 
	
	if ( recordingFilename != undefined and vehicleFilename != undefined ) then (

		clearSelection()
		mergeMAXFile vehicleFilename #prompt #select
		RsImportVehicleRecording recordingFilename
	) 
	else (
	
		messagebox "No recording or vehicle file selected"
	)
	
	
)

-- ///////////////////////////////////////////////////////////////////////////
-- Cloth tools
-- ///////////////////////////////////////////////////////////////////////////

macroscript RsClothEditor
	category:"RS Cloth"
	ButtonText:"Rage Cloth Editor"
(
	filein "rage\\RageClothEditor.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Debugging tools
-- ///////////////////////////////////////////////////////////////////////////

macroscript RsImportIndBoundsFromFile
	category: "RS Debug"
	ButtonText: "Import independent bounds"
(
	include "pipeline/import/mapbounds_ind_import.ms"
	
	filename = getOpenFileName caption:"Open independent bounds file" types:"Independent bounds file (*.bnd)|*.bnd" initialDir:RsConfigGetStreamDir()
		
	format "Importing bounds file: %\n" filename
	RsMapBoundsImport filename
)

macroscript RsImportIndBoundsFromDirectory
	category: "RS Debug"
	ButtonText: "Import independent bounds (directory)"
(
	include "pipeline/import/mapbounds_ind_import.ms"

	filename = getOpenFileName caption:"Open independent bounds files from" types:"Independent bounds file (*.bnd)|*.bnd" initialDir:RsConfigGetStreamDir()
	filepath = getFilenamePath filename
	
	files = getFiles ( filepath + "\\*.bnd" )
	for f in files do (
		format "Importing bounds file: %\n" f
		RsMapBoundsImport f
	)
)

macroscript SetCameraFromGame
	category: "RS Debug"
	ButtonText:"Set Camera From Game"
(
	filein "rockstar\\helpers\\camload.ms"	
)

macroscript ScriptDump
	category: "RS Debug"
	ButtonText:"Script Dump"
(
	filein "rockstar\\helpers\\scriptdump.ms"	
)

macroscript RsFragmentTuner
	category: "RS Debug"
	ButtonText:"Fragment tuner"
(
	filein "pipeline\\util\\fragtune.ms"
)

macroscript ERotFinder
	category: "RS Debug"
	ButtonText:"Rotation Finder"
(
	filein "pipeline\\ui\\ERotFinder.ms"
)

macroscript SetObjectAttr
	category: "RS Debug"
	ButtonText:"Set Object Attributes"
(
	include "pipeline/ui/SetObjectAttr.ms"	
)

-- ///////////////////////////////////////////////////////////////////////////
-- export
-- ///////////////////////////////////////////////////////////////////////////

macroScript RsMapExport
	category:"RS Export"
	tooltip:"Map Export"
	ButtonText:"Map Export"
(
	filein "rockstar/export/map.ms"
)

macroScript RsPedExport
	category:"RS Export"
	tooltip:"Ped Export"
	ButtonText:"Ped Export"
(
	filein "pipeline/export/ui/ped_ui.ms"
)

macroScript RsVehicleExport
	category:"RS Export"
	tooltip:"Vehicle Export"
	ButtonText:"Vehicle Export"
(
	filein "pipeline/export/ui/vehicle_exporter_ui.ms"
)

macroScript RsModelExport
	category:"RS Export"
	tooltip:"Model/Weapon Export"
	ButtonText:"Model/Weapon Export"
(
	filein "pipeline/export/ui/model_exporter_ui.ms"
)

macroScript RsSingleIPLExport
	category:"RS Export"
	tooltip:"Single IPL Export"
	ButtonText:"Single IPL Export"
(
	filein "pipeline/export/ui/MapSingleIPLExporter.ms"
)

macroscript RsPlayerCoords
	category:"RS Export"
	ButtonText:"Export Player Coords"
(	
	filein "pipeline/export/playercoords.ms";
)

macroscript RsWaterExport
	category:"RS Export"
	ButtonText:"Export Water"
(	
	filein "pipeline/export/ui/water_ui.ms";
)

macroscript RsExportImportZones
	category:"RS Export"
	ButtonText:"Export/Import Zones"
(	
	filein "pipeline/export/ui/ExportZones.ms";
)

macroscript RsExportAmbientScripts
	category:"RS Export"
	ButtonText:"Export Ambient Scripts"
(	
	filein "rockstar/export/ambientscript.ms";
)

macroscript RsExportCutLoc
	category: "RS Export"
	ButtonText: "Export Cutscene Locations"
(
	filein "rockstar/export/custscenelocation.ms"
)

macroscript RsExportViseme
	category: "RS Export"
	ButtonText: "Export Visemes"
(
	filein "rockstar/export/visemeexport.ms"
)

macroscript RsExportVectorMap
	category:"RS Export"
	ButtonText:"Export VectorMap"
(
	include "pipeline/util/xml.ms"
	include "pipeline/export/maps/vector_map.ms"
	
	local filename = getSaveFileName caption:"Save Vector Map As..." types:"Vector Map XML Files (*.xml)|*.xml"
	if ( undefined != filename ) then
		ExportVectorMapData filename
)

-- ///////////////////////////////////////////////////////////////////////////
-- Quad menu
-- ///////////////////////////////////////////////////////////////////////////

macroscript ShowSuperLodDetail
	category:"RS Utils"
	ButtonText:"Show Super Lod Detail"
(
	include "rockstar/util/lod.ms"
	for obj in rootnode.children do (

		-- Recurse through containers
		if ( Container == classof obj ) then
		(
			for o in obj.children do (				
				o.isHidden = not ( RsIsSuperLOD o )
			)
		)
		else if ( "Gta Object" == GetAttrClass obj ) then
		(
			obj.isHidden = not ( RsIsSuperLOD obj )
		)
	)
)

macroscript ShowLodDetail
	category:"RS Utils"
	ButtonText:"Show Lod Detail"
(
	include "rockstar/util/lod.ms"
	for obj in rootnode.children do (

		-- Recurse through containers
		if ( Container == classof obj ) then
		(
			for o in obj.children do (				
				o.isHidden = not ( RsIsLOD o )
			)
		)
		else if ( "Gta Object" == GetAttrClass obj ) then
		(
			obj.isHidden = not ( RsIsLOD obj )
		)
	)
)

macroscript ShowHiDetail
	category:"RS Utils"
	ButtonText:"Show High Detail"
(
	include "rockstar/util/lod.ms"
	for obj in rootnode.children do (

		-- Recurse through containers
		if ( Container == classof obj ) then
		(
			for o in obj.children do (				
				o.isHidden = ( RsIsAnyLOD o )
			)
		)
		else if ( "Gta Object" == GetAttrClass obj ) then
		(
			obj.isHidden = ( RsIsAnyLOD obj )
		)
	)
)

macroscript ShowMapsInViewportQuad
	category:"RS Utils"
	tooltip:"Show/hide diffuse texture maps for Rage Shader materials"
	ButtonText:"Show Maps in Viewport"
(
	filein "rockstar\\helpers\\ShowMapsInViewport.ms"
)

macroScript TextureTools
	category:"RS Utils"
	tooltip:"Texture Tools"
	ButtonText:"Texture Tools"
(
	filein "gta/utils/texturetools.ms"
)

macroscript BatchSetAttr
	category:"RS Utils"
	tooltip:"Batch Set Attributes"
	ButtonText:"Batch Set Attributes"
(
	filein "pipeline/ui/batchsetattr.ms"
)

macroscript CreateCollMeshFromExist
	category:"RS Utils"
	ButtonText:"Create a Collision Mesh From Existing"
(
	include "pipeline/util/UtilityFunc.ms"	
	CreateCollisionMeshRemoved()
)

macroscript CreateCollMesh
	category:"RS Utils"
	ButtonText:"Create a Collision Mesh"
(
	include "pipeline/util/UtilityFunc.ms"	
	CreateCollisionMesh()
)

macroscript CreateCollCapsule
	category:"RS Utils"
	ButtonText:"Create a Collision Capsule"
(
	include "pipeline/util/UtilityFunc.ms"	
	CreateCollisionCapsule()
)

macroscript CreateCollCylinder
	category:"RS Utils"
	ButtonText:"Create a Collision Cylinder"
(
	include "pipeline/util/UtilityFunc.ms"	
	CreateCollisionCylinder()
)

macroscript CreateShadMesh
	category:"RS Utils"
	ButtonText:"Create a Shadow Mesh"
(
	include "pipeline/util/UtilityFunc.ms"	
	CreateShadowMesh()
)

macroscript CreateCollBox
	category:"RS Utils"
	ButtonText:"Create a Collision Box"
(
	include "pipeline/util/UtilityFunc.ms"	
	CreateCollisionBox()
)

macroscript CreateCollSphere
	category:"RS Utils"
	ButtonText:"Create a Collision Sphere"
(
	include "pipeline/util/UtilityFunc.ms"	
	CreateCollisionSphere()
)

macroscript SelectLODChildren
	category:"RS Utils"
	ButtonText:"Select LOD Children"
(	
	include "pipeline/util/UtilityFunc.ms"
	selectLODChildren()
)

macroscript SelectLODParent
	category:"RS Utils"
	ButtonText:"Select LOD Parent"
(	
	include "pipeline/util/UtilityFunc.ms"
	selectLODParent()
)

macroscript SelectLODAllSet
	category:"RS Utils"
	ButtonText:"Select All LOD Set"
(	
	include "pipeline/util/UtilityFunc.ms"
	selectAllLODChildren()
)

macroscript SelectXrefAllSet
	category:"RS Utils"
	ButtonText:"Select All Xref Set"
(	
	include "pipeline/util/UtilityFunc.ms"
	selectAllXref()
)

macroscript SelectNormalAllSet
	category:"RS Utils"
	ButtonText:"Select All Normal Set"
(	
	include "pipeline/util/UtilityFunc.ms"
	selectAllNormal()
)

macroscript SelectHorizonAllSet
	category:"RS Utils"
	ButtonText:"Select All Horizon Objects"
(	
	include "pipeline/util/UtilityFunc.ms"
	selectAllHorizon()
)

macroscript SwitchSceneDXMats
	category:"RS Utils"
	ButtonText:"Switch Scene to DX Rage Materials"
(	
	
	include "pipeline\\util\\ragematerial.ms"
	RsToggleHardwareRendering true
)

macroscript SwitchSceneRageMats
	category:"RS Utils"
	ButtonText:"Switch Scene to Rage Materials"
(	
	
	include "pipeline\\util\\ragematerial.ms"
	RsToggleHardwareRendering false
)

macroscript SwitchStdMatsToRage
	category:"RS Utils"
	ButtonText:"Convert Std Mat to Rage Mat"
(	
	
	include "pipeline\\util\\ragematerial.ms"
	RsConvertStdMaterialsOnSelected (selection as array)
)
-- Lighting
macroscript LightSceneOn
	category:"RS Utils"
	ButtonText:"On"
(
	include "pipeline\\util\\ragematerial.ms"
	if querybox "Are you sure you want to switch on lighting for the whole scene?" title:"Scene Wide Lighting" then (
		RsToggleDxLightingOnObjs rootnode.children true
	)
)

macroscript LightSceneOff
	category:"RS Utils"
	ButtonText:"Off"
(
	include "pipeline\\util\\ragematerial.ms"
	if querybox "Are you sure you want to switch on lighting for the whole scene?" title:"Scene Wide Lighting" then (
		RsToggleDxLightingOnObjs rootnode.children false
	)
)

macroscript LightObjOn
	category:"RS Utils"
	ButtonText:"On"
(
	include "pipeline\\util\\ragematerial.ms"
	
	objs = selection as array
	RsToggleDxLightingOnObjs objs true
)

macroscript LightObjOff
	category:"RS Utils"
	ButtonText:"Off"
(
	include "pipeline\\util\\ragematerial.ms"
	
	objs = selection as array
	RsToggleDxLightingOnObjs objs false
)

macroscript LightFaceOn
	category:"RS Utils"
	ButtonText:"On"
(
	include "pipeline\\util\\ragematerial.ms"
	
	objs = selection as array
	RsToggleDxLightingOnFaces true
)

macroscript LightFaceOff
	category:"RS Utils"
	ButtonText:"Off"
(
	include "pipeline\\util\\ragematerial.ms"
	
	objs = selection as array
	RsToggleDxLightingOnFaces false
)

-- Texture scaling
macroscript To100pc
	category:"RS Utils"
	ButtonText:"1.0"
(
	include "pipeline\\util\\ragematerial.ms"
	objs = selection as array
	RsScaleTexuresOnSelectedObjs 1.0 objs
)

macroscript To75pc
	category:"RS Utils"
	ButtonText:"0.75"
(
	include "pipeline\\util\\ragematerial.ms"
	objs = selection as array
	RsScaleTexuresOnSelectedObjs 0.75 objs
)

macroscript To50pc
	category:"RS Utils"
	ButtonText:"0.5"
(
	include "pipeline\\util\\ragematerial.ms"
	objs = selection as array
	RsScaleTexuresOnSelectedObjs 0.5 objs
)

macroscript To25pc
	category:"RS Utils"
	ButtonText:"0.25"
(
	include "pipeline\\util\\ragematerial.ms"
	objs = selection as array
	RsScaleTexuresOnSelectedObjs 0.25 objs
)

macroscript To12pc
	category:"RS Utils"
	ButtonText:"0.125"
(
	include "pipeline\\util\\ragematerial.ms"
	objs = selection as array
	RsScaleTexuresOnSelectedObjs 0.125 objs
)

macroscript To100pcScene
	category:"RS Utils"
	ButtonText:"1.0"
(
	if querybox "Are you sure you want to scale the whole scene's textures?  You are best off doing this in wireframe mode." title:"You crazy fuck!!!" then (
		include "pipeline\\util\\ragematerial.ms"
		RsScaleTexuresOnSelectedObjs 1.0 rootnode.children
	)
)

macroscript To75pcScene
	category:"RS Utils"
	ButtonText:"0.75"
(
	if querybox "Are you sure you want to scale the whole scene's textures?  You are best off doing this in wireframe mode." title:"You crazy fuck!!!" then (
		include "pipeline\\util\\ragematerial.ms"
		RsScaleTexuresOnSelectedObjs 0.75 rootnode.children
	)
)

macroscript To50pcScene
	category:"RS Utils"
	ButtonText:"0.5"
(
	if querybox "Are you sure you want to scale the whole scene's textures?  You are best off doing this in wireframe mode." title:"You crazy fuck!!!" then (
		include "pipeline\\util\\ragematerial.ms"
		RsScaleTexuresOnSelectedObjs 0.5 rootnode.children
	)
)

macroscript To25pcScene
	category:"RS Utils"
	ButtonText:"0.25"
(
	if querybox "Are you sure you want to scale the whole scene's textures?  You are best off doing this in wireframe mode." title:"You crazy fuck!!!" then (
		include "pipeline\\util\\ragematerial.ms"
		RsScaleTexuresOnSelectedObjs 0.25 rootnode.children
	)
)

macroscript To12pcScene
	category:"RS Utils"
	ButtonText:"0.125"
(
	if querybox "Are you sure you want to scale the whole scene's textures?  You are best off doing this in wireframe mode." title:"You crazy fuck!!!" then (
		include "pipeline\\util\\ragematerial.ms"
		RsScaleTexuresOnSelectedObjs 0.125 rootnode.children
	)
)

macroscript To100pcFace
	category:"RS Utils"
	ButtonText:"1.0"
(
	include "pipeline\\util\\ragematerial.ms"
	RsScaleTexuresOnMaterialsForSelectedFaces 1.0
)

macroscript To75pcFace
	category:"RS Utils"
	ButtonText:"0.75"
(
	include "pipeline\\util\\ragematerial.ms"
	RsScaleTexuresOnMaterialsForSelectedFaces 0.75
)

macroscript To50pcFace
	category:"RS Utils"
	ButtonText:"0.5"
(
	include "pipeline\\util\\ragematerial.ms"
	RsScaleTexuresOnMaterialsForSelectedFaces 0.5
)

macroscript To25pcFace
	category:"RS Utils"
	ButtonText:"0.25"
(
	include "pipeline\\util\\ragematerial.ms"
	RsScaleTexuresOnMaterialsForSelectedFaces 0.25
)
	
macroscript To12pcFace
	category:"RS Utils"
	ButtonText:"0.125"
(
	include "pipeline\\util\\ragematerial.ms"
	RsScaleTexuresOnMaterialsForSelectedFaces 0.125
)

-- ///////////////////////////////////////////////////////////////////////////
-- called from script or not directly called...
-- ///////////////////////////////////////////////////////////////////////////

macroscript slice90
	category:"RS Modelling"
	ButtonText:"slice 90"
(	
	modPanel.addModToSelection (SliceModifier ()) ui:on
	$.modifiers[#Slice].slice_plane.rotation =(quat 1 0 1 0)
)

macroscript slice180
	category:"RS Modelling"
	ButtonText:"slice 180"
(	
	modPanel.addModToSelection (SliceModifier ()) ui:on
	$.modifiers[#Slice].slice_plane.rotation =(quat 1 0 0 1)
)

macroscript SetLODByNameAndPos
	category:"RS Lod"
	ButtonText:"Set LOD Parent By Name and Pos"
(	
	include "pipeline\\util\\setlodbynameandpos.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Deprecated
-- ///////////////////////////////////////////////////////////////////////////

macroscript RsExportCarRec
	category:"RS Export"
	ButtonText:"Export Car Recordings"
(	
	filein "rockstar/export/caranim.ms";
)

macroscript SetCameraFromGame
	category:"RS Utils"
	ButtonText:"Set Camera From Game"
(
	filein "rockstar\\helpers\\camload.ms"	
)

macroscript ScriptDump
	category:"RS Utils"
	ButtonText:"Script Dump"
(
	filein "rockstar\\helpers\\scriptdump.ms"	
)

macroscript RsFragmentTuner
	category:"RS Utils"
	ButtonText:"Fragment tuner"
(
	filein "pipeline\\util\\fragtune.ms"
)

macroscript IgnoreMe
	category:"RS Utils"
	ButtonText:"Dont Add to IPL"
(
	include "pipeline/util/UtilityFunc.ms"	
	
	AddIgnoreSelected()
)

macroscript DontExport
	category:"RS Utils"
	ButtonText:"Dont Export Selected"
(	
	include "pipeline/util/UtilityFunc.ms"	
	
	SetDontExportSelected()
)

macroscript DontApplyRadiosity
	category:"RS Utils"
	ButtonText:"Dont Apply Radiosity"
(	
	include "pipeline/util/UtilityFunc.ms"	
	
	SetDontApplyRadiosity()
)

macroscript ParticleCat
	category:"RS Utils"
	ButtonText:"Particle Set"
(	
	filein "rockstar\\helpers\\particle_cat.ms"
)

macroscript ExplosionCat
	category:"RS Utils"
	ButtonText:"Explosion Set"
(	
	filein "rockstar\\helpers\\explosion_cat.ms"
)

macroScript BlockVisibilitySet
	category:"RS Organisational"
	tooltip:"Set the hide and freeze of objects by block"
	ButtonText:"Block Visibility Set"
(
	filein "rockstar\\helpers\\visblock.ms"
)

macroscript ShowNotes
	category:"RS Organisational"
	ButtonText:"Display Notes"
(	
	filein "gta\\utils\\GtaDisplayNotes.ms"
)

macroscript CheckPathNodes
	category:"RS Placement"
	ButtonText:"Check Path Nodes"
(	
	filein "rockstar\\helpers\\pathnodecheck.ms"
)

macroscript AlignObjectsInZ
	category:"RS Placement"
	ButtonText:"Align Objects in Z"
(	
	filein "gta\\utils\\GtaAlignObject.ms"
)

macroscript CreateInstancer
	category:"RS Placement"
	tooltip:"Select an object and wrap selected to this"
	ButtonText:"Instancer"
(	
	include "pipeline/util/UtilityFunc.ms"
	createInstancer()
)

macroscript DatImporter
	category:"RS Placement"
	ButtonText:"DAT Importer"
(
	filein "rockstar\\util\\datimporter.ms"
)

macroScript sectorCalc
	category:"RS Placement"
	ButtonText:"World - Sector Calculator"
(
	filein "gta\\utils\\GtaWorldSector.ms"
)

macroScript BadObjPosChecker
	category:"RS Placement"
	ButtonText:"Bad Object Position Checker"
(
	filein "rockstar\\util\\badobjpos_finder.ms"
)

macroScript HandholdImporter
	category:"RS Placement"
	ButtonText:"Handhold Importer"
(
	filein "pipeline\\helpers\\climbing\\handhold_importer.ms"
)

macroscript RenderProps
	category: "RS Interiors"
	tooltip:"Render images of all props in a file"
	ButtonText: "Prop Renderer"
(
	filein "pipeline/helpers/props/proprenderer.ms"
)

macroscript MiloFiddler
	category: "RS Interiors"
	ButtonText: "MILO Fiddler"
(
	filein "rockstar\\helpers\\milo.ms"
)

macroScript BoundRoomHelper
	category:"RS Collision"
	tooltip:"RexBound/Room Util"
	ButtonText:"RexBound/Room Util"
(
	filein "rockstar/helpers/boundroom.ms"
)

macroScript RsCollisionConvert
	category:"RS Collision"
	tooltip:"Collision Convert"
	ButtonText:"Collision Convert"
(
	filein "pipeline/helpers/collision/matconvert.ms"
)

macroScript RsCollisionCategories
	category:"RS Collision"
	tooltip:"Collision Categories"
	ButtonText:"Collision Categories"
(
	filein "rockstar/helpers/collcat.ms"
)

macroScript RsSecondSurfacePainter
	category:"RS Collision"
	tooltip:"Second Surface Painter"
	ButtonText:"Second Surface Painter"
(
	filein "rockstar/helpers/secondsurface_painter.ms"
)

macroScript RsGroundPainter
	category:"RS Collision"
	tooltip:"Ground Painter"
	ButtonText:"Ground Painter"
(
	filein "pipeline/helpers/collision/ground_painter.ms"
)

macroscript RepointXrefs
	category: "RS Xrefs"
	ButtonText: "Repoint Refs"
(
	filein "rockstar\\util\\repointxrefs.ms"
)

macroscript RenameXrefs
	category: "RS Xrefs"
	ButtonText: "Rename Refs"
(
	filein "rockstar\\util\\renamexrefs.ms"
)

macroscript InternalRefs
	category: "RS Xrefs"
	ButtonText: "Internal Refs"
(
	filein "rockstar\\helpers\\internalref.ms"
)

macroscript ResetPivots
	category:"RS Xrefs"
	ButtonText:"Reset Pivots for Xrefs"
(	
	filein "gta\\utils\\GtaPivotReset.ms"
)

macroscript ChangeXRef
	category:"RS Xrefs"
	ButtonText:"Change Xrefs"
(	
	filein "gta\\utils\\GtaChangeXref.ms"
)

macroscript DoFindXref
	category:"RS Xrefs"
	ButtonText:"Find XRef"
(
	filein "gta\\utils\\GtaFindXref.ms"
)

macroscript FixXREFs
	category:"RS Xrefs"
	ButtonText:"Fix Xrefs"
(
	filein "rockstar\\util\\UpdateXREF.ms"
)

macroScript RsGridHelper
	category:"RS Modelling"
	tooltip:"Grid Helper"
	ButtonText:"Grid Helper"
(
	filein "rockstar/helpers/gridhelper.ms"
)

macroscript SelectFaceByAngle
	category:"RS Modelling"
	ButtonText:"Select Faces by Angle"
(	
	filein "gta\\utils\\GtaSelFacesByAngle.ms"
)	

macroscript DoSnapVertPositions
	category:"RS Modelling"
	tooltip:"Smooths gaps between meshes by snapping the verts together"
	ButtonText:"Snap Vertex Positions"
(	
	filein "pipeline\\ui\\vertexsnap.ms"
)

macroscript ExtrudeTexture
	category: "RS Modelling"
	ButtonText: "Extrude with Auto Texture"
(
	filein "rockstar\\helpers\\extrude.ms"
)

macroscript ChoppyChop
	category:"RS Modelling"
	tooltip:"Slices landscape up"
	ButtonText:"Choppy chop"
(
	filein "gta\\utils\\Choppy_chop.ms"
)

macroscript SetPivotForBBox
	category:"RS Modelling"
	ButtonText:"Set Best Pivot For BBox"
(	
	filein "gta\\utils\\SetPivotForBBox.ms"
)

macroScript CompositeSpecSetup
	category:"RS Material"
	tooltip:"Composite Specular Setup"
	ButtonText:"Composite Specular Setup"
(
	filein "rockstar/helpers/specfixup.ms"
)

macroScript ShaderValueChange
	category:"RS Material"
	tooltip:"Replace Rs Shader Value"
	ButtonText:"Shader Attribute Changer"
(
	filein "rockstar/helpers/shaderchange.ms"
)

macroScript ShaderReplacer
	category:"RS Material"
	tooltip:"Replace Rs Shader with another"
	ButtonText:"Replace Rs Shader"
(
	filein "pipeline/ui/shaderreplace.ms"
)

macroScript StdMatReplacer
	category:"RS Material"
	tooltip:"Replace Standard Material with Rage Shader"
	ButtonText:"Replace StdMat with Rage Shader"
(
	filein "rockstar/helpers/stdmatreplace.ms"
)

macroScript RageShdrReplacer
	category:"RS Material"
	tooltip:"Replace Rage Shader with Standard Material"
	ButtonText:"Replace Rage Shader with StdMat"
(
	filein "rockstar/helpers/rageshadertostdmat.ms"
)

macroScript TextureTools
	category:"RS Material"
	tooltip:"Texture Tools"
	ButtonText:"Texture Tools"
(
	filein "gta/utils/texturetools.ms"
)

macroScript MaterialMapSet
	category:"RS Material"
	tooltip:"Material Map Set"
	ButtonText:"Material Map Set"
(
	filein "rockstar/util/matmapset.ms"
)

macroScript RsTexMapInfo
	category:"RS Material"
	tooltip:"Tex Map Info"
	ButtonText:"Tex Map Info"
(
	filein "rockstar/helpers/texmaps.ms"
)

macroscript ResetDiffuse
	category:"RS Material"
	tooltip:"Sets diffuse colour to grey"
	ButtonText:"Reset Diffuse"
(
	include "pipeline/util/UtilityFunc.ms"
	ResetDiffuseToWhite()
)

macroscript CreateMultiSub
	category:"RS Material"
	tooltip:"Tidy your multisub"
	ButtonText:"Create New MultiSub"
(	
	include "pipeline/helpers/materials/CreateMultiSub.ms"
	GTAcreateNewMultiSub()
)

macroscript MultiSubCountForScene
	category:"RS Material"
	tooltip:"Multi Sub count"
	ButtonText:"Multi Sub count for scene"
(	
	filein "rockstar\\helpers\\materialutils.ms"
	
)

macroscript CreateMultiSubTxd
	category:"RS Material"
	ButtonText:"Create New MultiSubs for Txds"
(	
	include "pipeline/helpers/materials/CreateMultiSub.ms"
	GTAcreateNewMultiSubTxd()
)

macroscript CreateMultiSubTxdObject
	category:"RS Material"
	ButtonText:"Create New MultiSubs for Objects"
(	
	include "pipeline/helpers/materials/CreateMultiSub.ms"
	GTAcreateNewMultiSubObject()
)

macroscript SetBoundMaterials
	category:"RS Material"
	tooltip:"Set bound materials"
	ButtonText:"Bound Material Toggle"
(
	filein "rockstar\\helpers\\boundmaterialflagtoggle.ms"
)

macroscript GrassPainter
	category:"RS Material"
	tooltip:"Tools for editing grass and tree materials"
	ButtonText:"Grass Painter"
(
	filein "pipeline\\helpers\\materials\\GrassPainter.ms"
)

macroscript ViewTXD
	category:"RS Material"
	ButtonText:"View TXD"
(
	filein "pipeline\\helpers\\materials\\viewtxd.ms"
)

macroscript CheckDegenerates
	category:"RS Info"
	ButtonText:"Check For Degenerates"
(	
	filein "pipeline/util/CheckDegenShare.ms"	
)

macroscript TestMeshTopology
	category:"RS Info"
	ButtonText:"Test Mesh Topology"
(	
	filein "pipeline/util/TestMeshTopology.ms"
)

macroscript DoShowShaderStats
	category:"RS Info"
	ButtonText:"Shader Stats"
(
	ShellLaunch "http://greg-alienbrain/shader_stats.php" ""
)

macroscript PosPrint
	category:"RS Info"
	ButtonText:"Position/Rotation Print"
(
	filein "pipeline\\util\\posprint.ms"
)

macroscript SceneLodEditor
	category:"RS Lod"
	ButtonText:"Scene LOD Editor"
(	
	filein "pipeline\\ui\\lodeditor.ms"
)

macroscript DrawableLodEditor
	category:"RS Lod"
	ButtonText:"Drawable LOD Editor"
(
	filein "pipeline\\ui\\drawablelodeditor.ms"
)

macroscript ShowSceneLODView
	category:"RS Lod"
	ButtonText:"Scene LOD View"
(	
	ShowLODView()
)

macroscript ShowDrawableLODView
	category:"RS Lod"
	ButtonText:"Drawable LOD View"
(
	LodDrawable_Show()
)

macroscript ShowLodSphere
	category:"RS Lod"
	ButtonText:"Show LOD Spheres"
(
	filein "pipeline\\util\\lodsphere.ms"
)

macroscript LodFiddler
	category:"RS Lod"
	ButtonText:"Lod Fiddler"
(	
	filein "gta\\utils\\lodfiddler.ms"
)

macroscript GuessLod
	category:"RS Lod"
	tooltip:"Guess LoD distances of selected objects"
	ButtonText:"Guess LoD dist"
(
	include "pipeline/util/UtilityFunc.ms"
	GuessLodDist selection
)

macroscript TestLodName
	category:"RS Lod"
	ButtonText:"Test LoD name"
(
	include "pipeline/util/UtilityFunc.ms"
	GtaTestForLodName()
)

macroscript AssignVisTrack
	category:"RS Lod"
	ButtonText:"Assign LOD Vis Track"
(	
	AssignVisControl()
)

macroscript AssignVisTrackSelected
	category:"RS Lod"
	ButtonText:"Assign LOD Vis Track Selected"
(	
	AssignVisControlSelected()
)

macroscript ClearVisTrack
	category:"RS Lod"
	ButtonText:"Clear Vis Track"
(	
	ClearVisControl()
)

macroscript LandLodder
	category:"RS Lod"
	ButtonText:"Captain Stu's Land Lodder Loving"
(
	filein "rockstar\\helpers\\TerrainLodder.ms"
)

macroscript TreeLodGenerator
	category:"RS Lod"
	ButtonText:"Tree LOD Generator"
(
	filein "pipeline\\ui\\treelodgenerator_ui.ms"
)

macroscript LODVisController
	category:"RS Lod"
	ButtonText:"LOD Visibility Controller"
(	
	filein "pipeline\\ui\\lod_visibility_controller.ms"
)

macroscript VertAnglePainter
	category:"RS Material"
	ButtonText:"Vert Angle Painter"
(
	filein "rockstar\\helpers\\VertAnglePaint.ms"
)

macroscript ProxyTextureSwap
	category:"RS Material"
	ButtonText:"Proxy texture Swap"
(
	filein "pipeline\\helpers\\materials\\proxytexturemgr.ms"
)

macroscript MultiAssignRefs
	category:"RS InternalRefs"
	ButtonText:"Multi Assign Refs"
(	
	filein "rockstar\\helpers\\ixrmultiassign.ms"
)

macroscript FixIxRefMaterials
	category:"RS InternalRefs"
	ButtonText:"Fix IRef Materials (force)"
(
	for obj in $objects do
	(
		if ( InternalRef != classof obj ) then
			continue
		ixref_setmaterial obj
	)
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs VFX
-- ///////////////////////////////////////////////////////////////////////////

macroscript ParticleConvert
	category:"RS VFX"
	ButtonText:"Particle Conversion Wizard"
(	
	filein "rockstar\\helpers\\VFXParticleConvert.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Vehicle
-- ///////////////////////////////////////////////////////////////////////////

macroScript ResetPivot
	category:"RS Vehicle"
	ButtonText:"Reset Pivot Transform and Scale"
(
	include "pipeline/util/pivot_reset.ms"
	for o in $selection do 
		pivot_reset_transform o
)

macroscript StrayVertCleaner
	category:"RS Vehicle"
	ButtonText:"Stray Vert Cleaner"
(	
	filein "pipeline\\helpers\\vehicles\\stray_vert_cleaner.ms"
)

macroScript RsAnimExport
	category:"RS Export"
	tooltip:"Anim Export"
	ButtonText:"Anim Export"
(
	filein "rockstar/export/anim.ms"
)

macroscript RsExportZones
	category:"RS Export"
	ButtonText:"Export Zones"
(	
	filein "pipeline/export/ui/ExportZones.ms";
)

macroscript RsImportZones
	category:"RS Export"
	ButtonText:"Import Zones"
(	
	filein "pipeline/import/ui/ImportZones.ms";
)

macroscript RsClothEditor
	category:"RS Cloth"
	ButtonText:"Rage Cloth Editor"
(
	filein "rage\\RageClothEditor.ms"
)


-- ///////////////////////////////////////////////////////////////////////////
-- Debugging tools
-- ///////////////////////////////////////////////////////////////////////////

macroscript RsImportIndBoundsFromFile
	category: "RS Debug"
	ButtonText: "Import independent bounds"
(
	include "pipeline/import/mapbounds_ind_import.ms"
	
	filename = getOpenFileName caption:"Open independent bounds file" types:"Independent bounds file (*.bnd)|*.bnd" initialDir:RsConfigGetStreamDir()
		
	format "Importing bounds file: %\n" filename
	RsMapBoundsImport filename
)

macroscript RsImportIndBoundsFromDirectory
	category: "RS Debug"
	ButtonText: "Import independent bounds (directory)"
(
	include "pipeline/import/mapbounds_ind_import.ms"

	filename = getOpenFileName caption:"Open independent bounds files from" types:"Independent bounds file (*.bnd)|*.bnd" initialDir:RsConfigGetStreamDir()
	filepath = getFilenamePath filename
	
	files = getFiles ( filepath + "\\*.bnd" )
	for f in files do (
		format "Importing bounds file: %\n" f
		RsMapBoundsImport f
	)
)

