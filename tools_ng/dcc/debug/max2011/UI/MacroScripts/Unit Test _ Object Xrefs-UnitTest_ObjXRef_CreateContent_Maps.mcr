macroScript UnitTest_ObjXRef_CreateContent_Maps
	ButtonText:"UnitTest_ObjXRef_CreateContent_Maps"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Test - Object Xrefs" 
	Tooltip:"Unit Test - Obj Xref - CreateContentMaps" 
(
	-- Macro Variables
	local strWorkingFolder 
	strWorkingFolder = GetDir #scene

-------------------------------------------------------------------------------
function write_max_files_with_all_maps = 
(
	--variables
	local obj, m, file, fname, progMax, progVal
	local arMaps = #()
	
	-- log file
	local strTestName = "write_max_files_with_all_maps"
	local fstrLog = createFile ( strWorkingFolder + "\\" + strTestName + ".log")
	
	-- gathers maps in an array
	arMaps = for c in texturemap.classes where c.creatable collect c
		
	--calculates the percentage for the progress bar
	resetMaxFile #noprompt
	progVal = 0
	progMax = arMaps.count
	progressStart strTestName 
	
	for m in arMaps do
	(
		--progress bar handling
		progVal = progVal + 1
		progressUpdate (100.0 * progVal / progMax)
		if getProgressCancel() then exit
	
		--*****************************************************************
		--MAKE GEOMETRY, APPLY SINGLE MATERIAL WITH MAP, SAVE FILE
		--*****************************************************************
		resetMaxFile #noPrompt
		file = (strWorkingFolder + "\\MAP_" + (m as string)+ ".max")
		fname = filenameFromPath file
		print ("Creating " + fname + "... ") to:fstrLog
		
		obj = box()
		obj.material = StandardMaterial()
		obj.material.diffusemap = (execute((m as string) + "()"))
		actionMan.executeAction 0 "40807"  -- Views: Activate All Maps
		
		--saves the file and log it
		doSaveMaxFile file fstrLog
		flush fstrLog
		gc()
									
		--*****************************************************************
		--MAKE GEOMETRY, APPLY MSOM MATERIAL WITH MTL AS SUB MTL WITH MAP, SAVE FILE
		--*****************************************************************
		--make a box with multi material
		resetMaxFile #noPrompt
		file = (strWorkingFolder + "\\MAP_IN_MSOM_" + (m as string)+ ".max")
		fname = filenameFromPath file
		print ("Creating " + fname + "... ") to:fstrLog

		obj = box()
		obj.material = multimaterial()
		obj.material.numsubs = 1
		
		-- set the multimaterial sub material to each material type
		obj.material.materialList[1] = StandardMaterial()
		obj.material.materialList[1].diffusemap = (execute((m as string) + "()"))
		actionMan.executeAction 0 "40807"  -- Views: Activate All Maps

		--saves the file and log it
		doSaveMaxFile file fstrLog			
		flush fstrLog	
		gc()
		
	)--end loop through creatable modifiers
	
	--finishes stream output
	close fstrLog 

	--progress bar handling
	progressEnd()
	resetMaxFile #noprompt
	gc()
)--write_max_files_with_all_maps function

-- MACRO HANDLERS
On Execute Do 
(
	write_max_files_with_all_maps()
)

)--end macro
