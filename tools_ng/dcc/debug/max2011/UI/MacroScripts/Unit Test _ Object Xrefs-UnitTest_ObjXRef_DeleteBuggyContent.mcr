macroScript UnitTest_ObjXRef_DeleteBuggyContent
	ButtonText:"UnitTest_ObjXRef_DeleteBuggyContent"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Test - Object Xrefs" 
	Tooltip:"Unit Test - Obj Xref - DeleteBuggyContent" 
(
	-- Macro Variables
	local strWorkingFolder 
	strWorkingFolder = GetDir #scene

/* ---------------------------------------------------------------------------
	deletes known buggy files causing crashes with xrefs, preventing the 
	unit tests to function obviously, theses files are looked at by name 
	and it is assumed that they where created with the current unit test suite.
----------------------------------------------------------------------------*/
function delete_buggy_content = 
(
	local files = #()
	local f
	--deletes the "garment_maker" modifier files
	--files = getFiles (strWorkingFolder + "\\*Garment*")
	files =  (getFiles (strWorkingFolder + "\\*Skin_Morph*"))
	join files (getFiles (strWorkingFolder + "\\*Vol__Select*"))
	for f in files do 
	(
		deleteFile f
		messagebox ( "Deleting " + f)
	)
	
)--delete_buggy_content function

-- MACRO HANDLERS
On Execute Do 
(
	delete_buggy_content()
)

)--end macro
