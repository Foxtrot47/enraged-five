macroScript UnitTest_ObjXRef_CreateContent_Controllers
	ButtonText:"UnitTest_ObjXRef_CreateContent_Controllers"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Test - Object Xrefs" 
	Tooltip:"Unit Test - Obj Xref - CreateContentControllers" 
(
	-- Macro Variables
	local strWorkingFolder 
	strWorkingFolder = GetDir #scene

-----------------------------------------------------------------------
function write_max_files_with_all_Matrix3Controllers = 
(
	--variables
	local obj, m,file, fname, progMax, progVal
	local arControllers = #()
	
	-- log file
	local strTestName = "write_max_files_with_all_matrix3controllers"
	local fstrLog = createFile ( strWorkingFolder + "\\" + strTestName + ".log")

	--*****************************************************************
	--MAKE GEOMETRY, APPLY <any> MATRIX3 CONTROLLER, SAVE FILE
	--*****************************************************************
	-- gathers Controllers in an array
	arControllers = for c in matrix3controller.classes where c.creatable collect c	

	--calculates the percentage for the progress bar
	resetMaxFile #noprompt
	progVal = 0
	progMax = arControllers.count
	progressStart strTestName 
	
	--loop through all Controllers and make a max file on disk
	--disableSceneRedraw()

	for m in arControllers do
	(
		resetMaxFile #noprompt

		--progress bar handling
		progVal = progVal + 1
		progressUpdate (100.0 * progVal / progMax)
		if getProgressCancel() then exit
	
		print "---------------------------------" to:fstrLog
		print ("Creating " + (m as string) + "... ") to:fstrLog
		
		try 
		(		
			obj = box()
			obj.controller = (execute((m as string) + "()"))
		)
		catch 
		(
			print ("Error: " + getCurrentException()) to:fstrLog
			throw()
		)

		--saves the file and log it
		file = (strWorkingFolder + "\\MATRIX3CONTROLLER_" + (m as string)+ ".max")
		doSaveMaxFile file fstrLog

		--log file
		flush fstrLog	
		gc()
				
	)--end loop through creatable controllers
	
	--finishes stream output
	close fstrLog 

	--redraws enable
	enableSceneRedraw()

	--progress bar handling
	progressEnd()
	gc()
	
)--end function
	
----------------------------------------------------------------------
function write_max_files_with_all_FloatControllers = 
(
	--variables
	local obj, m,file, fname, progMax, progVal
	local arControllers = #()

	local strTestName = "write_max_files_with_all_floatcontrollers"
	local fstrLog = createFile ( strWorkingFolder + "\\" + strTestName + ".log")

	-- gathers Controllers in an array
	arControllers = for c in floatcontroller.classes where c.creatable collect c	

	--calculates the percentage for the progress bar
	resetMaxFile #noprompt
	progVal = 0
	progMax = arControllers.count
	progressStart strTestName 
	
	--loop through all Controllers and make a max file on disk
	--disableSceneRedraw()

	--*****************************************************************
	--MAKE GEOMETRY, APPLY POINT3 PRS MATRIX3 CONTROLLER, <any> FLOAT CONTROLLER SAVE FILE
	--*****************************************************************
	for m in arControllers do
	(
		resetMaxFile #noprompt
	
		--progress bar handling
		progVal = progVal + 1
		progressUpdate (100.0 * progVal / progMax)
		if getProgressCancel() then exit
	
		print "---------------------------------" to:fstrLog
		print ("Creating " + (m as string) + "... ") to:fstrLog

		--make the object and controller
		try
		(
			obj = box()
			obj.controller = prs()
			obj.controller.position.controller.xposition.controller = (execute((m as string) + "()"))
		)
		catch 
		(
			print ("Error: " + getCurrentException()) to:fstrLog
			throw()
		)

		--saves the file and log it
		file = (strWorkingFolder + "\\FLOATCONTROLLER_" + (m as string)+ ".max")
		doSaveMaxFile file fstrLog					

		--log file
		flush fstrLog	
		gc()
	)--end loop through creatable controllers
	
	--finishes stream output
	close fstrLog 

	--redraws enable
	enableSceneRedraw()

	--progress bar handling
	progressEnd()
	resetMaxFile #noprompt
	gc()
)--end function
	
----------------------------------------------------------------------
function write_max_files_with_all_MorphControllers = 
(
	--variables
	local obj, m,file, fname, progMax, progVal
	local arControllers = #()

	local strTestName = "write_max_files_with_all_morphcontrollers"
	local fstrLog = createFile ( strWorkingFolder + "\\" + strTestName + ".log")

	-- gathers Controllers in an array
	arControllers = for c in morphcontroller.classes where c.creatable collect c	

	--calculates the percentage for the progress bar
	resetMaxFile #noprompt
	progVal = 0
	progMax = arControllers.count
	progressStart strTestName 
	
	--loop through all Controllers and make a max file on disk
	--disableSceneRedraw()

	--*****************************************************************
	--MAKE GEOMETRY, CONVERT TO MORPH OBJECT, APPLY <any> MORPH CONTROLLER SAVE FILE
	--*****************************************************************
	for m in arControllers do
	(
		resetMaxFile #noPrompt
			
		--progress bar handling
		progVal = progVal + 1
		progressUpdate (100.0 * progVal / progMax)
		if getProgressCancel() then exit
	
		print "---------------------------------" to:fstrLog
		print ("Creating " + (m as string) + "... ") to:fstrLog
		
		--make the object and controller
		try
		(
			obj = box()
			createmorphobject obj
			obj.morph.controller = (execute((m as string) + "()"))
		)
		catch 
		(
			print ("Error: " + getCurrentException()) to:fstrLog
			throw()
		)

		--saves the file and log it
		file = (strWorkingFolder + "\\MORPHCONTROLLER_" + (m as string)+ ".max")
		doSaveMaxFile file fstrLog					

		--log file
		flush fstrLog	
		gc()
	)--end loop through creatable controllers
	
	--finishes stream output
	close fstrLog 

	--redraws enable
	enableSceneRedraw()

	--progress bar handling
	progressEnd()
	resetMaxFile #noprompt
	gc()
)--end function

------------------------------------------------------------------------
function write_max_files_with_all_Point3Controllers = 
(
	--variables
	local obj, m,file, fname, progMax, progVal
	local arControllers = #()
	local strTestName = "write_max_files_with_all_point3controllers"
	local fstrLog = createFile ( strWorkingFolder + "\\" + strTestName + ".log")

	-- gathers Controllers in an array
	arControllers = for c in point3controller.classes where c.creatable collect c	

	--calculates the percentage for the progress bar
	resetMaxFile #noprompt
	progVal = 0
	progMax = arControllers.count
	progressStart strTestName 
	
	--loop through all Controllers and make a max file on disk
	--disableSceneRedraw()

	--*****************************************************************
	--MAKE OMNILIGHT, APPLY <any> POINT3 CONTROLLER TO COLOR PARAMETER, SAVE FILE
	--*****************************************************************

	for m in arControllers do
	(
		resetMaxFile #noPrompt
		
		--progress bar handling
		progVal = progVal + 1
		progressUpdate (100.0 * progVal / progMax)
		if getProgressCancel() then exit
	
		print "---------------------------------" to:fstrLog
		print ("Creating " + (m as string) + "... ") to:fstrLog
		
		--make the object and controller
		try
		(
			obj = omnilight()
			obj.color.controller = (execute((m as string) + "()"))
		)
		catch 
		(
			print ("Error: " + getCurrentException()) to:fstrLog
			throw()
		)

		--saves the file and log it
		file = (strWorkingFolder + "\\POINT3CONTROLLER_" + (m as string)+ ".max")
		doSaveMaxFile file fstrLog					
		
		--log file
		flush fstrLog	
		gc()				
	)--end loop through creatable controllers
	
	--finishes stream output
	close fstrLog 

	--redraws enable
	enableSceneRedraw()

	--progress bar handling
	progressEnd()
	resetMaxFile #noprompt
	gc()
)--end function

	
----------------------------------------------------------------------
function write_max_files_with_all_PositionControllers = 
(
	--variables
	local obj, m,file, fname, progMax, progVal
	local arControllers = #()
	local strTestName = "write_max_files_with_all_positioncontrollers"
	local fstrLog = createFile ( strWorkingFolder + "\\" + strTestName + ".log")

	-- gathers Controllers in an array
	arControllers = for c in positioncontroller.classes where c.creatable collect c	

	--calculates the percentage for the progress bar
	resetMaxFile #noprompt
	progVal = 0
	progMax = arControllers.count
	progressStart strTestName 
	
	--loop through all Controllers and make a max file on disk
	--disableSceneRedraw()

	--*****************************************************************
	--MAKE GEOMETRY, APPLY <any> POSITION CONTROLLER, SAVE FILE
	--*****************************************************************
	for m in arControllers do
	(
		resetMaxFile #noPrompt
		
		--progress bar handling
		progVal = progVal + 1
		progressUpdate (100.0 * progVal / progMax)
		if getProgressCancel() then exit
	
		print "---------------------------------" to:fstrLog
		print ("Creating " + (m as string) + "... ") to:fstrLog
		
		--make the object and controller
		try
		(
			obj = box()
			obj.position.controller = (execute((m as string) + "()"))
		)
		catch 
		(
			print ("Error: " + getCurrentException()) to:fstrLog
			throw()
		)

		--saves the file and log it
		file = (strWorkingFolder + "\\POSITIONCONTROLLER_" + (m as string)+ ".max")
		doSaveMaxFile file fstrLog					

		--log file
		flush fstrLog	
		gc()				
	)--end loop through creatable controllers
	
	--finishes stream output
	close fstrLog 

	--redraws enable
	enableSceneRedraw()

	--progress bar handling
	progressEnd()
	resetMaxFile #noprompt
	gc()
	
	
)--end function

	
-----------------------------------------------------------------------
function write_max_files_with_all_RotationControllers = 
(
	--variables
	local obj, m,file, fname, progMax, progVal
	local arControllers = #()
	local strTestName = "write_max_files_with_all_rotationcontrollers"
	local fstrLog = createFile ( strWorkingFolder + "\\" + strTestName + ".log")

	-- gathers Controllers in an array
	arControllers = for c in Rotationcontroller.classes where c.creatable collect c	

	--calculates the percentage for the progress bar
	resetMaxFile #noprompt
	progVal = 0
	progMax = arControllers.count
	progressStart strTestName 
	
	--loop through all Controllers and make a max file on disk
	--disableSceneRedraw()

	--*****************************************************************
	--MAKE GEOMETRY, APPLY <any> Rotation CONTROLLER, SAVE FILE
	--*****************************************************************
	for m in arControllers do
	(
		resetMaxFile #noPrompt
		
		--progress bar handling
		progVal = progVal + 1
		progressUpdate (100.0 * progVal / progMax)
		if getProgressCancel() then exit
	
		print "---------------------------------" to:fstrLog
		print ("Creating " + (m as string) + "... ") to:fstrLog
		
		--make the object and controller
		try 
		(
			obj = box()
			obj.Rotation.controller = (execute((m as string) + "()"))
		)
		catch 
		(
			print ("Error: " + getCurrentException()) to:fstrLog
			throw()
		)

		--saves the file and log it
		file = (strWorkingFolder + "\\ROTATIONCONTROLLER_" + (m as string)+ ".max")
		doSaveMaxFile file fstrLog					
	
		--log file
		flush fstrLog	
		gc()				
	)--end loop through creatable controllers
	
	--finishes stream output
	close fstrLog 

	--redraws enable
	enableSceneRedraw()

	--progress bar handling
	progressEnd()
	resetMaxFile #noprompt
	gc()
	
	
)--end function

-------------------------------------------------------------------------------
function write_max_files_with_all_ScaleControllers = 
(
	--variables
	local obj, m,file, fname, progMax, progVal
	local arControllers = #()
	local strTestName = "write_max_files_with_all_scalecontrollers"
	local fstrLog = createFile ( strWorkingFolder + "\\" + strTestName + ".log")

	-- gathers Controllers in an array
	arControllers = for c in Scalecontroller.classes where c.creatable collect c	

	--calculates the percentage for the progress bar
	resetMaxFile #noprompt
	progVal = 0
	progMax = arControllers.count
	progressStart strTestName 
	
	--loop through all Controllers and make a max file on disk
	--disableSceneRedraw()

	--*****************************************************************
	--MAKE GEOMETRY, APPLY <any> Scale CONTROLLER, SAVE FILE
	--*****************************************************************
	for m in arControllers do
	(
		resetMaxFile #noPrompt
		
		--progress bar handling
		progVal = progVal + 1
		progressUpdate (100.0 * progVal / progMax)
		if getProgressCancel() then exit
	
		print "---------------------------------" to:fstrLog
		print ("Creating " + (m as string) + "... ") to:fstrLog
		
		--make the object and controller
		try 
		(
			obj = box()
			obj.Scale.controller = (execute((m as string) + "()"))
		)
		catch 
		(
			print ("Error: " + getCurrentException()) to:fstrLog
			throw()
		)
		
		--saves the file and log it
		file = (strWorkingFolder + "\\SCALECONTROLLER_" + (m as string)+ ".max")
		doSaveMaxFile file fstrLog					

		--log file
		flush fstrLog	
		gc()				
	)--end loop through creatable controllers
	
	--finishes stream output
	close fstrLog 

	--redraws enable
	enableSceneRedraw()

	--progress bar handling
	progressEnd()
	resetMaxFile #noprompt
	gc()
)--end function


-- MACRO HANDLERS
On Execute Do 
(
		write_max_files_with_all_Matrix3Controllers()
		write_max_files_with_all_FloatControllers()
		write_max_files_with_all_MorphControllers()
		write_max_files_with_all_Point3Controllers()
		write_max_files_with_all_PositionControllers()
		write_max_files_with_all_RotationControllers()
		write_max_files_with_all_ScaleControllers()
		--write_max_files_with_all_QuatControllers() -- no quat controller classes in product yet
		--write_max_files_with_all_MasterBlockControllers() -- cannot create MasterBlockControllers via MXS
)

)--end macro
