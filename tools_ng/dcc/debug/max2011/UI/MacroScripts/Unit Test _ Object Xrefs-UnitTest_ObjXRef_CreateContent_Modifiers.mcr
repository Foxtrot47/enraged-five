macroScript UnitTest_ObjXRef_CreateContent_Modifiers
	ButtonText:"UnitTest_ObjXRef_CreateContent_Modifiers"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Test - Object Xrefs" 
	Tooltip:"Unit Test - Obj Xref - CreateContentModifiers" 
(
	-- Macro Variables
	local strWorkingFolder 
	strWorkingFolder = GetDir #scene

/*----------------------------------------------------------------------------
	get a working foder, object, modifier type and a string describing the filetype
	makes a file name out of this
	applies the modifier on the object
	save as a max file in the working folder
	
	NOTE:  at the end of this macro, there is a function that deletes content 
	that is recognized as being buggy and unresolvable. 
---------------------------------------------------------------------------*/
function saveMaxFileWithObjectAndModifier strWorkingFolder obj mod ftype toStream = 
(
	fname = (strWorkingFolder + "\\" + (mod as string)+ "_" +  ftype + ".max")
	if not (doesFileExist fname) do
	(
		if (validModifier obj mod) do
		(
			print ("Creating " + fname  + "... ") to:toStream
			try 
			(
				addModifier obj (execute((mod as string) + "()"))
			)
			catch 
			(
				print ("Error: " + getCurrentException()) to:toStream
				throw()
			)
			doSaveMaxFile fname toStream
		)
	)
)--end saveMaxFileWithObjectAndModifier strWorkingFolder obj mod ftype function

-----------------------------------------------------------------------------
function write_max_files_with_all_modifiers = 
(
	local strTestName = "write_max_files_with_all_modifiers"
	local fstrLog = createFile ( strWorkingFolder + "\\" + strTestName + ".log")

	--variables
	local obj, m, fname, progMax, progVal, fType
	local creatableModifiers = #()
	local creatableSpaceWarpsModifiers = #()
	-- These cause errors when testing
	local modsToSkip = #(
		HairMod -- assert when rendering with mental ray
	)
	
	format "Skiping modifiers that cause errors: : %\n" (modsToSkip as string)
	format "Skiping modifiers that cause errors: : %\n" (modsToSkip as string) to:fstrLog

	-- strip out non creatable modifiers and those marked as to be skipped
	creatableModifiers = for c in modifier.classes where (c.creatable and 0 == (findItem modsToSkip c)) collect c

	-- strip out non creatable spacewarps
	creatableSpaceWarpsModifiers = for c in SpaceWarpmodifier.classes where c.creatable collect c
		
	join creatableModifiers creatableSpaceWarpsModifiers 
	
	--calculates the percentage for the progress bar
	resetMaxFile #noprompt
	progVal = 0
	progMax = creatableModifiers.count - modsToSkip.count
	progressStart "Creating files with all modifiers"
	
	--loop through creatablemodifiers and make a max file on disk
	--disableSceneRedraw()
	for m in creatableModifiers do
	(
		--progress bar handling
		progVal = progVal + 1
		progressUpdate (100.0 * progVal / progMax)
		if getProgressCancel() then exit
	
		-- GEOMETRY
		obj = box()
		ftype = "GEOMETRY"
		saveMaxFileWithObjectAndModifier strWorkingFolder obj m ftype fstrLog 
		resetMaxFile #noprompt

		--SHAPE
		obj = rectangle()
		ftype = "SHAPE"
		saveMaxFileWithObjectAndModifier strWorkingFolder obj m ftype fstrLog 
		resetMaxFile #noprompt
		
		--CAMERA
		obj = Targetcamera fov:45 nearclip:1 farclip:1000 nearrange:0 farrange:1000 mpassEnabled:off mpassRenderPerPass:off pos:[131.816,68.0035,0] isSelected:on target:(Targetobject transform:(matrix3 [1,0,0] [0,1,0] [0,0,1] [-31.7196,3.59732,0]))
		ftype = "TARGETCAMERA"
		saveMaxFileWithObjectAndModifier strWorkingFolder obj m ftype fstrLog 
		resetMaxFile #noprompt
		
		--TARGETLIGHT
		obj = targetSpot rgb:(color 255 255 255) shadowColor:(color 0 0 0) multiplier:1 contrast:0 softenDiffuseEdge:0 nearAttenStart:0 nearAttenEnd:40 farAttenStart:80 farAttenEnd:200 decayRadius:40 atmosOpacity:100 atmosColorAmt:100 shadowMultiplier:1 hotspot:43 falloff:45 aspect:1 pos:[386.989,138.097,0] isSelected:on target:(Targetobject transform:(matrix3 [1,0,0] [0,1,0] [0,0,1] [-9.33096,-37.3272,0]))
		ftype = "TARGETLIGHT"
		saveMaxFileWithObjectAndModifier strWorkingFolder obj m ftype fstrLog 
		resetMaxFile #noprompt
		
		--HELPER
		obj = dummy()
		ftype = "HELPER"
		saveMaxFileWithObjectAndModifier strWorkingFolder obj m ftype fstrLog 
		resetMaxFile #noprompt
		
		--SPACEWARPS
		obj = gravity()
		ftype = "SPACEWARP"
		saveMaxFileWithObjectAndModifier strWorkingFolder obj m ftype fstrLog 
		resetMaxFile #noprompt

		--BONES
		obj = bone()
		ftype = "BONE"
		saveMaxFileWithObjectAndModifier strWorkingFolder obj m ftype fstrLog 
		resetMaxFile #noprompt
		
	)--end loop through creatable modifiers
	--enableSceneRedraw()

	--progress bar handling
	progressEnd()
	resetMaxFile #noprompt
	flush fstrLog
	gc()

	--finishes stream output
	close fstrLog 
)--end write_max_files_with_all_modifiers function


-- MACRO HANDLERS
On Execute Do 
(
	write_max_files_with_all_modifiers()
)

)--end macro
