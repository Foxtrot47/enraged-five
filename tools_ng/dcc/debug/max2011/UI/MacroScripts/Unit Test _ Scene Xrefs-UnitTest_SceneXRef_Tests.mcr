macroScript UnitTest_SceneXRef_Tests
	ButtonText:"UnitTest_SceneXRef_Tests"
	Category:"Unit Test - Scene Xrefs" 
	internalCategory:"Unit Test - Scene Xrefs" 
	Tooltip:"Unit Test - Scene Xref - Test" 
(	
--------------------------------------------------------------------------------
-- Globals to this macro
--------------------------------------------------------------------------------
global gTempFile1 = (getdir #Temp) + "\\UT.SceneXref.Test.01.max"
global gTempFile2 = (getdir #Temp) + "\\UT.SceneXref.Test.02.max"
--------------------------------------------------------------------------------
-- Utility methods
--------------------------------------------------------------------------------
function saveFile aFileName = 
(
	deleteFile aFileName 
	ts.assert ((getFiles aFileName).count == 0) "Temp file deleted"

	local res = saveMaxFile aFileName quiet:true
	ts.assert (true == res) "Temp file saved"
)

--------------------------------------------------------------------------------
-- Unit Test methods
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Unit test fixture setup
function setupUT =
(
	resetMaxFile #noprompt
)

--------------------------------------------------------------------------------
-- Unit test fixture teardown
function teardownUT =
(
	-- no scene tear down required
)

--------------------------------------------------------------------------------
-- Tests creating scene xrefs
function testCreateSceneXref =
(
	-- create a source file 
	b=box()
	saveFile gTempFile1

	-- create scene xref 
	resetmaxFile #noPrompt
	local xRec = xrefs.addNewXRefFile gTempFile1
	
	-- tests result 
	ts.assert(xRec != undefined) "Xref Record - created"
	ts.assert(xRec.tree != undefined) "Xref Tree - created"
	ts.assert(xRec.tree.children != undefined) "Xref Tree - has nodes"
	ts.assert(1 == xRec.tree.children.count) "Xref Tree - verify number of xrefed objects"
)

--------------------------------------------------------------------------------
-- Tests scene xref overlays
function testOverlay = 
(
	-- create two source file 
	box pos:[20, 0, 0]
	saveFile gTempFile1

	resetMaxFile #noprompt	
	sphere pos:[0, 20, 0]
	local xRec = xrefs.addNewXRefFile gTempFile1
	ts.assert(xRec != undefined) "File 1 scene xrefed into File 2 as overlay"
	xRec.overlay = true
	saveFile gTempFile2
	
	-- load File 1 and xref File 2
	local res = loadMaxFile gTempFile1 quiet:true
	ts.assert(true == res) "File 1 loaded"
	local xRec = xrefs.addNewXRefFile gTempFile2
	ts.assert(xRec != undefined) "File 2 scene xrefed into File 1"
	local nextedXRec = xrefs.getXRefFile 1 root:xRec
	ts.assert(nextedXRec != undefined and true == nextedXRec.overlay) "File 1 overlay scene xrefed into File 2 which is scene xrefed into File 1"
	ts.assert(0 == nextedXRec.tree.children.count) "No objects loaded from overlay scene xref"
	saveFile gTempFile1

	-- load File 2 and xref File 1
	local res = loadMaxFile gTempFile2 quiet:true
	ts.assert(true == res) "File 2 loaded"
	local xRec = xrefs.getXRefFile 1
	ts.assert(xRec != undefined) "File 1 loaded as scene xref in File 2"
	ts.assert(0 == (xrefs.getXRefFileCount root:xRec)) "No nested scene xrefs found in File 1"

)
	
--------------------------------------------------------------------------------	
-- Tests creating circular scene xrefs
-- http://clarifyweb:9140/dtsthin/Front?act=defect&defID=682202
function testCreateCircularSceneXref =
(
	-- create two source file 
	box pos:[20, 0, 0]
	saveFile gTempFile1

	resetMaxFile #noprompt	
	sphere pos:[0, 20, 0]
	local xRec = xrefs.addNewXRefFile gTempFile1
	ts.assert(xRec != undefined) "File 1 scene xrefed into File 2"
	saveFile gTempFile2
	
	-- load File 1 and xref File 2
	local res = loadMaxFile gTempFile1 quiet:true
	ts.assert(true == res) "File 1 loaded"
	with quiet on
	(
		-- tests result 
		local xRec = xrefs.addNewXRefFile gTempFile2
		ts.assert(xRec != undefined) "File 2 scene xrefed into File 1"
		ts.assert(0 == (xrefs.getXRefFileCount root:xRec)) "No nested scene xrefs found in File 2"
	)
	saveFile gTempFile1

	-- load File 2 and xref File 1
	local res = loadMaxFile gTempFile2 quiet:true
	ts.assert(true == res) "File 2 loaded"
	local xRec = xrefs.getXRefFile 1
	ts.assert(xRec != undefined) "File 1 loaded as scene xref in File 2"
	ts.assert(0 == (xrefs.getXRefFileCount root:xRec)) "No nested scene xrefs found in File 1"

	-- load File 2 and set name of xrefed File 1 to File 2
	with quiet on xRec.fileName = gTempFile2
	ts.assert(gtempFile1 == xRec.filename) "Xrefed File 1 was not set to File 2"

)

--------------------------------------------
-- Macro methods
--------------------------------------------
On isEnabled return true
On isVisible return true

global createTestSuite = undefined
global ts = undefined
On Execute Do	
(
	-- Load Mxs Unit Test framework
	if createTestSuite == undefined do (
		fileIn ((getdir #scripts) + "/Startup/MxsUnitTest/MxsUnitTestFramework.ms")
	)
	-- if above fails, add exe\MxsUnitTest as 3rd party plugin path and manually
	-- load and evaluate your unit test macro. The line below will ensure the mxs 
	-- unit test framework is loaded in this case
	if createTestSuite == undefined do (
		fileIn ("MxsUnitTestFramework.ms")
	)

	-- create a test suite that outputs cmmands into a log file and messages into a new maxscript window (default behaviour)
	local testSuiteName = "UnitTest.SceneXref.Tests"
	local logFileName = ((getdir #temp) + "\\" + testSuiteName + ".log")
	ts = createTestSuite testSuiteName setupUT teardownUT cmdLog:(createFile logFileName)

	ts.addTest testCreateSceneXref
	ts.addTest testCreateCircularSceneXref
	ts.addTest testOverlay

	ts.run()
)

) -- END MACRO
