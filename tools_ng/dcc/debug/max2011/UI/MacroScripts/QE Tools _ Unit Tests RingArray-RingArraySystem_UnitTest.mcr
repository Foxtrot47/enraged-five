macroScript RingArraySystem_UnitTest
	ButtonText:"UT.RingArraySystem"		
	Category:"QE Tools - Unit Tests RingArray" 
	internalCategory:"QE Tools - Unit Tests RingArray" 
	Tooltip:"UT.RingArray" 
	--Icon:#("<icon_name>",1)
(	
	-- Unit test fixture setup
	fn setupUT =
	(
		resetmaxFile #noPrompt
		global gUndoLevels = theHold.getMaxUndoLevels()
		global gQuietMode = SetQuietMode true
		global gRingArrayFile = (getdir #Scene) + "\\Temp.RingArraySystem.max"
		global gXrefRingArrayFile = (getdir #Scene) + "\\Temp.RingArraySystem.Xref.max"
		
		deleteFile gRingArrayFile
		deleteFile gXRefRingArrayFile
	)

	-- Unit test fixture teardown
	fn teardownUT =
	(
		theHold.SetMaxUndoLevels gUndoLevels
		SetQuietMode gQuietMode
	)
	
	-- Unit tests --
    -----------------------------------------------------------------------
	fn testCreate =
	(
	  local objCountPreCreate = $objects.count
	  undo "RA_Create" on (
	    ra = RingArray.create()
	    ts.assert (0 < ra.numNodes) "Create"
	  )
	  local objCountPostCreate = $objects.count
	  
	  max undo
	  ts.assert ($objects.count == objCountPreCreate) "Create undone"
	  max redo
      ts.assert ($objects.count == objCountPostCreate) "Create redone"
	  max undo
	  ts.assert ($objects.count == objCountPreCreate) "Create undone"	  
	)

    -----------------------------------------------------------------------
	fn testPersistence =
	(
	  local objCountPreCreate = objects.count
      local ra = RingArray.create()
	  local objCountPostCreate = objects.count
	  
	  max hold
	  resetmaxFile #noPrompt
	  ts.assert ($objects.count == objCountPreCreate) "Scene reset after hold"	  

	  max fetch
	  ts.assert ($objects.count == objCountPostCreate) "Scene fetched"
	)

    -----------------------------------------------------------------------
	fn testAddNodes =
	(
	  undo off (
        local ra = RingArray.create()
	  )
	  
	  local objCountPreAdd = objects.count
	  undo "RA_AddNodes" on (
	    ra.numNodes = 10
	    ts.assert (10 == ra.numNodes) "numNode == 10"
	  )
	  local objCountPostAdd = objects.count
	  
	  max undo
	  ts.assert ($objects.count == objCountPreAdd) "numNode increase undone"
	  max redo
      ts.assert ($objects.count == objCountPostAdd) "numNode increase redone"
	  max undo
	  ts.assert ($objects.count == objCountPreAdd) "numNode increase undone"  
	)

    -----------------------------------------------------------------------
	fn testRemoveNodes =
	(
	  undo off (
        local ra = RingArray.create numNodes:8
	  )
	  
	  local objCountPreRemove = objects.count
	  undo "RA_RemoveNodes" on (
	    ra.numNodes = 1
        ts.assert (1 == ra.numNodes) "numNode == 1"
	  )
	  local objCountPostRemove = objects.count
	  
	  max undo
	  ts.assert ($objects.count == objCountPreRemove) "numNode decrease undone"
	  max redo
      ts.assert ($objects.count == objCountPostRemove) "numNode decrease redone"
      max undo
	  ts.assert ($objects.count == objCountPreRemove) "numNode decrease undone"  
	)

    -----------------------------------------------------------------------
	fn testDeleteSys =
	(
	  -- note that deleting ring array system nodes does not
	  -- notify the ring array master of the deletion of the nodes
	  -- and thus it still points to the deleted nodes
	  
	  -- Indirect references will fix this and ensure that the 
	  -- ring array master does not try to dereference deleted nodes
	  
	  undo off (
        local ra = RingArray.create()
	  )

	  local objCountPreSysDelete = objects.count
	  undo "RA_DeleteSys" on (
	    delete (ra.GetNode 1)
		ts.assert (1 == $objects.count) "System was deleted"
	  )
	  local objCountPostSysDelete = objects.count
	  
	  max undo
	  ts.assert ($objects.count == objCountPreSysDelete) "system deletion undone"
	  max redo
      ts.assert ($objects.count == objCountPostSysDelete) "system deletion redone"
      max undo
	  ts.assert ($objects.count == objCountPreSysDelete) "System deletion undone"  
	)

    -------------------------------------------------------------------------
	fn testCloneSys =
	(
	  undo off (
        local ra = RingArray.create()
	  )
	  
	  local objCountPreSysClone = objects.count
	  undo "RA_CloneSys" on (
        local sysNodes = #()
		ra.GetNodes &sysNodes #Ctx_Clone

		local oneNode = ra.GetNode 1
        local actualNodes = #()
		local newNodes = #()
		res = maxOps.cloneNodes &oneNode cloneType:#copy actualNodeList:&actualNodes newNodes:&newNodes
	    ts.assert (true == res)  "Clone"
        ts.assert (newNodes.count == sysNodes.count) "Cloned node count"
	  )
	  local objCountPostSysClone = objects.count
	  
	  max undo
	  ts.assert ($objects.count == objCountPreSysClone) "system clone undone"
	  max redo
      ts.assert ($objects.count == objCountPostSysClone) "system clone redone"
      max undo
	  ts.assert ($objects.count == objCountPreSysClone) "System clone undone"  
	  max redo
      ts.assert ($objects.count == objCountPostSysClone) "system clone redone"

	  max hold
	  resetmaxFile #noPrompt
	  ts.assert (0 == $objects.count) "Hold and Reset Scene"

	  max fetch
	  ts.assert ($objects.count == objCountPostSysClone) "Scene fetched"
	)

    -------------------------------------------------------------------------
	fn saveRingArray fileName = 
	(
      deleteFile fileName 
      ts.assert ((getFiles fileName).count == 0) "Temp file does not exist"
	  
      local ra = RingArray.create()
	  
	  local res = saveMaxFile fileName  quiet:true
	  ts.assert (true == res) "Save Temp File"
	  
	  local objCount = objects.count
	  
	  resetmaxFile #noPrompt

	  return objCount
	)
	
    -------------------------------------------------------------------------
	fn testMergeSys =
	(
	  local objCountPreSysMerge = saveRingArray(gRingArrayFile)
	  
	  local res = mergeMaxFile gRingArrayFile quiet:true
	  ts.assert (true == res) "Merge Temp File"

	  local objCountPostSysMerge = objects.count
	  ts.assert (objCountPostSysMerge == objCountPreSysMerge) "System merged"
	  
	  -- Test that the merged objects still form a system
	  local mc = getMasterController $Box01
	  local ra = getInterface mc "IRingArrayMaster"
	  ts.assert (ra.numNodes == (objCountPreSysMerge-1)) "System merged"
	  
	  testCloneSys()
	)

    -------------------------------------------------------------------------
	fn testXrefSys =
	(
	  local objCountPreSysXref = saveRingArray(gRingArrayFile)

	  local xoRec = objXrefMgr.AddXrefItemsFromFile gRingArrayFile -- xref objects\materials\ctrls
	  ts.assert (objXrefMgr.recordCount == 1) "Xref objects  - check record count"
	  ts.assert ((xoRec.itemCount #XRefObjectType) == objCountPreSysXref) "Xref objects - check xref item count in record"

	  local objCountPostSysXref = objects.count
	  ts.assert (objCountPostSysXref == objCountPreSysXref) "System xrefed (object Count Test)"

	  local mc1 = getMasterController $Box01
	  local mc2 = getMasterController $Box01
	  local ra = getInterface mc1 "IRingArrayMaster"
	  ts.assert (ra.numNodes == (objCountPreSysXref - 1)) "System Xrefed (numNodes test)"
	  
	  local msg = "System Xrefed (master controller identity test)"
	  if (mc1 != mc2) then ts.logFailure(msg)
	  else ts.logSuccess(msg)
	)
	
	-------------------------------------------------------------------------

	fn testNestedXrefSys = 
	(
	  -- create and save ring array, this resets the scene
	  local objCountPreSysXref = saveRingArray(gRingArrayFile)

	  -- xref saved ring array and save the file
	  local xoRec = objXrefMgr.AddXrefItemsFromFile gRingArrayFile -- xref objects\materials\ctrls
	  ts.assert (objXrefMgr.recordCount == 1) "Xref objects - check record count"
	  ts.assert ((xoRec.itemCount #XRefObjectType) == objCountPreSysXref) "Xref objects - check xref item count in record"
  	  local res = saveMaxFile gXRefRingArrayFile quiet:true
	  ts.assert (true == res) "Save Xrefed objects"
	  resetmaxFile #noPrompt

	  -- xref the scene containing the xrefed RingArray
  	  local xoRec = objXrefMgr.AddXrefItemsFromFile gXRefRingArrayFile -- xref objects\materials\ctrls
	  ts.assert (objXrefMgr.recordCount == 2) "Xref Xrefed Objects - check record count"
	  ts.assert ((xoRec.itemCount #XRefObjectType) == objCountPreSysXref) "Xref Xrefed Objects - check xref item count in record"

	  local mc1 = getMasterController $Box01
	  local mc2 = getMasterController $Box01
	  local ra = getInterface mc1 "IRingArrayMaster"
	  ts.assert (ra.numNodes == (objCountPreSysXref - 1)) "System Xrefed (numNodes test)"
	  
	  local msg = "System Xrefed (master controller identity test)"
	  if (mc1 != mc2) then ts.logFailure(msg)
	  else ts.logSuccess(msg)
	)

	-- Macro methods
	On isEnabled return true
	On isVisible return true

  -- this is created by the fileIn command
  global createTestSuite = undefined 
  global ts = undefined
  
	On Execute Do	
	(
		-- Load Mxs Unit Test framework
	  if createTestSuite == undefined do fileIn ((getdir #scripts) + "/Startup/MxsUnitTest/MxsUnitTestFramework.ms")
	  -- if above fails, add exe\MxsUnitTest as 3rd party plugin path and manually
	  -- load and evaluate your unit test macro. The line below will ensure the mxs 
	  -- unit test framework is loaded in this case
	  if createTestSuite == undefined do fileIn ("MxsUnitTestFramework.ms")

	  -- create a test suite that outputs cmmands into a log file and messages into a new maxscript window (default behaviour)
	  local logFileName = ((getdir #maxroot) + "UT.RingArray.log")
	  ts = createTestSuite "RingArrayUnitTest" setupUT teardownUT cmdLog:(createFile logFileName)
	  ts.addTest testCreate
	  ts.addTest testPersistence
	  ts.addTest testAddNodes
	  ts.addTest testRemoveNodes
	  ts.addTest testDeleteSys	  
	  ts.addTest testCloneSys
	  ts.addTest testMergeSys
	  ts.addTest testXrefSys 
	  ts.addTest testNestedXrefSys

	  -- Tests to be added:
	  -- Test loading\merging\xrefing legacy RingArrays
  			-- ts.addTest testLegacy

	  -- Test integrity of xrefed systems
			-- ts.addTest testReplaceSlaveCtrl

	  -- Test that xrefed systems work as they do in the src file
			-- ts.addTest testCloneXrefSys 
			-- ts.addTest testDeleteXRefSys 
			-- ts.addTest testReplaceSlaveCtrl
	  
	  -- Test xref commands on xrefed systems
			-- ts.addTest testMergeXrefSys -- Apply the Merge Xref command to the xrefed system
	  
	  ts.run()
	)
)
