macroScript UnitTest_ObjXRef_CreateContent_Materials
	ButtonText:"UnitTest_ObjXRef_CreateContent_Materials"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Test - Object Xrefs" 
	Tooltip:"Unit Test - Obj Xref - CreateContentMaterials" 
(
	-- Macro Variables
	local strWorkingFolder 
	strWorkingFolder = GetDir #scene

-------------------------------------------------------------------------------
function write_max_files_with_all_materials = 
(
	--variables
	local obj, m,file, fname, progMax, progVal
	local arMaterials = #()
	
	-- log file
	local strTestName = "write_max_files_with_all_materials"
	local fstrLog = createFile ( strWorkingFolder + "\\" + strTestName + ".log")

	-- gathers materials in an array
	arMaterials = for c in material.classes where c.creatable collect c
		
	--calculates the percentage for the progress bar
	resetMaxFile #noprompt
	progVal = 0
	progMax = arMaterials.count
	progressStart strTestName 
	
	for m in arMaterials do
	(
		--progress bar handling
		progVal = progVal + 1
		progressUpdate (100.0 * progVal / progMax)
		if getProgressCancel() then exit
	
		--*****************************************************************
		--MAKE GEOMETRY, APPLY SINGLE MATERIAL, SAVE FILE
		--*****************************************************************
		resetMaxFile #noPrompt
		file = (strWorkingFolder + "\\MATERIAL_" + (m as string)+ ".max")
		fname = filenameFromPath file
		print ("Creating " + fname + "... ") to:fstrLog

		obj = box()
		obj.material = (execute((m as string) + "()"))
		actionMan.executeAction 0 "40807"  -- Views: Activate All Maps

		--saves the file and log it
		doSaveMaxFile file fstrLog 
		flush fstrLog	
		gc()
									
		--*****************************************************************
		--MAKE GEOMETRY, APPLY MSOM MATERIAL WITH MTL AS SUB MTL, SAVE FILE
		--*****************************************************************
		--make a box with multi material
		resetMaxFile #noPrompt
		file = (strWorkingFolder + "\\MSOM_" + (m as string)+ ".max")
		fname = filenameFromPath file
		print ("Creating " + fname + "... ") to:fstrLog

		obj = box()
		obj.material = multimaterial()
		obj.material.numsubs = 1
		
		-- set the multimaterial sub material to each material type
		obj.material.materialList[1] = (execute((m as string) + "()"))
		actionMan.executeAction 0 "40807"  -- Views: Activate All Maps
		
		--saves the file and log it
		doSaveMaxFile file fstrLog 
		flush fstrLog	
		gc()
	)--end loop through creatable mos
	
	--finishes stream output
	close fstrLog 

	--progress bar handling
	progressEnd()
	resetMaxFile #noprompt
	gc()
	
)--write_max_files_with_all_materials function

-- MACRO HANDLERS
On Execute Do 
(
	write_max_files_with_all_materials()
)

)--end macro
