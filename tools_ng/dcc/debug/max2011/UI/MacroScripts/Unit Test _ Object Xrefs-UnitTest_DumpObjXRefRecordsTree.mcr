macroScript UnitTest_DumpObjXRefRecordsTree
	ButtonText:"UnitTest_DumpObjXRefRecordsTree"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Test - Object Xrefs" 
	Tooltip:"Unit Test - Obj Xref - DumpObjXRefRecordsTree" 
(	
---------------------------------------------------
-- Global utility functions and objects
---------------------------------------------------
-- Object XRef helper methods defined elsewhere
global OX_DumpObjXRefRecordsTree = undefined

-- Unit Test related globals
global createTestSuite = undefined
global ts = undefined

--------------------------------------------
-- Unit Test methods
--------------------------------------------	
-- Unit test fixture setup
fn setupUT =
(
	-- no scene setup required
)

-- Unit test fixture teardown
fn teardownUT =
(
	-- no scene tear down required
)

-- This does the real dumping of xref records
fn testDumpObjXRefRecordsTree =
(
	OX_DumpObjXRefRecordsTree()
)

--------------------------------------------
-- Macro methods
--------------------------------------------
On isEnabled return true
On isVisible return true

On Execute Do	
(
if undefined == OX_DumpObjXRefRecordsTree do fileIn ("OBjXRef.Util.ms")
if createTestSuite == undefined do fileIn ((getdir #scripts) + "/startup/MxsUnitTest/MxsUnitTestFramework.ms")
if createTestSuite == undefined do fileIn ("MxsUnitTestFramework.ms")

  -- create a test suite that outputs cmmands into a log file and messages into a new maxscript window (default behaviour)
  -- local logFileName = ((getdir #maxroot) + "DumpObjXRefRecordsTree.log")
  -- global xrefDumpWnd = newScript()
  ts = createTestSuite "DumpObjXRefRecordsTree" setupUT teardownUT -- cmdLog:undefined msgLog:xrefDumpWnd
	ts.addTest testDumpObjXRefRecordsTree
	ts.run()
)

) -- END MACRO
