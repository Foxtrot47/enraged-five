macroScript SchematicView_UnitTest_CA
	ButtonText:"SchematicViewUnitTest_CA" 
	Category:"Qe Tools - Unit Tests" 
	internalCategory:"Qe Tools - Unit Tests" 
	Tooltip:"Schematic VIew Unit Test - CA" 
	-- Needs Icon
	--Icon:#("Max_edit_modifiers",1)
(	
	-- Macro methods 	
	On isEnabled return true 	
	On isVisible return true  	
		
	On Execute Do	 	
	(
		max reset file 
		-- Create CA
		myCA = attributes weaponData 
		( 
			parameters main rollout:params 
			( 
				hitPoints type:#float ui:hits default:10 
				cost type:#float ui:cost default:100 
				sound type:#string 
			) 

			rollout params "Weapon Parameters" 
			( 
				spinner hits "Hit Points" type:#float 
				spinner cost "Cost" type:#float 
				dropdownlist sound_dd "Sound" items:#("boom", "sparkle", "zap", "fizzle") 
				on sound_dd selected i do sound = sound_dd.items[i] 
			) 
		) 

		-- Create Node Level CA
		myPos = 0
		s0 = sphere name:"ca-node" position:[myPos,0,0] radius:5
		custattributes.add s0 myCA baseobject:false #unique

		-- Create Object Level CA
		myPos += 15
		s1 = sphere name:"ca-object" position:[myPos,0,0] radius:5
		custattributes.add s1 myCA #unique

		-- Create Object and Modifier Level CA
		myPos += 15
		s2 = sphere name:"ca-objectAndBendMod" position:[myPos,0,0] radius:5
		custattributes.add s2 myCA #unique
		b1 = Bend()
		addModifier s2 b1
		custattributes.add b1 myCA #unique

		-- Create Material Level CA
		myPos += 15
		s3 = sphere name:"ca-Material" position:[myPos,0,0] radius:5
		m1 = Standard()
		s3.material = m1
		custattributes.add m1 myCA #unique

		-- Create Controller Level CA
		myPos += 15
		s4 = sphere name:"ca-PosController" position:[myPos,0,0] radius:5
		c1 = s4.position.controller
		custattributes.add c1 myCA #unique

		-- Create WSM Level CA
		myPos += 15
		s5 = sphere name:"ca-WSMBind" position:[myPos,0,0] radius:5
		w1 = SpaceRipple()
		bindSpaceWarp s5 w1
		b1 = s5.modifiers[1]
		custattributes.add b1 myCA #unique
	)
)
