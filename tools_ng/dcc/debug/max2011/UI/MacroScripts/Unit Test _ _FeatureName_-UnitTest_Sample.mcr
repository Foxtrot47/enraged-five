macroScript UnitTest_Sample
	-- Text displayed in menus or on buttons
	ButtonText:"UnitTest_Sample"						
	-- Category to be used for all mxs unit tests
	Category:"Unit Test - <FeatureName>"		
	internalCategory:"Unit Test - <FeatureName>" 
	-- Text displayed in Customize UI dialog
	Tooltip:"Unit Test - <FeatureName> - <Test Suite Name>"		
	--Icon:#("<icon_name>",1)
(	
------------------------------------------------------------------------------
-- Macro global definitions
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Helper functions
------------------------------------------------------------------------------

------------------------------------------------------------------------------	
-- Unit test fixture setup
function setupUT =
(
	--print "Debug: setupUT"
	-- add here fixture setup code
	resetmaxFile #noPrompt
	b = box()
)

------------------------------------------------------------------------------
-- Unit test fixture teardown
function  teardownUT =
(
	--print "Debug: teardownUT"
	-- add here fixture teardown code
)

------------------------------------------------------------------------------
-- Unit Test functions
------------------------------------------------------------------------------
function testShouldFail =
(
	-- commands executed via the "exec" function of the unit test suite
	-- are logged in the log file
	ts.exec "res = 8/0"
)

------------------------------------------------------------------------------
function testShouldPass =
(
	-- Use the assert mechanims to checks assumptions\conditions
	-- If the assertion fails, the test will fail
	
	ts.assert (1 == objects.count) "Verify number of objects in scene"
)

-- Macro methods
On isEnabled return true
On isVisible return true

-- this is created by the fileIn command
global createTestSuite = undefined 
global ts = undefined

On Execute Do	
(
	-- Load Mxs Unit Test framework
  if createTestSuite == undefined do fileIn ((getdir #scripts) + "/Startup/MxsUnitTest/MxsUnitTestFramework.ms")
  -- if above fails, add exe\MxsUnitTest as 3rd party plugin path and manually
  -- load and evaluate your unit test macro. The line below will ensure the mxs 
  -- unit test framework is loaded in this case
  if createTestSuite == undefined do fileIn ("MxsUnitTestFramework.ms")

  -- create a test suite that outputs cmmands into a log file and messages into a new maxscript window (default behaviour)
  local testName = "UT.Sample"
  local logFileName = ((getdir #temp) + "\\" + testName + ".log")
  ts = createTestSuite testName setupUT teardownUT cmdLog:(createFile logFileName)
  
  -- Add tests to the suite 
	ts.addTest testShouldFail
	ts.addTest testShouldPass
	
	-- Execute suite of tests
	ts.run()
)

) -- End of macro
