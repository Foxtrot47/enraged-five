macroScript UnitTest_ObjXRef_DumpXRefManager
	ButtonText:"UnitTest_ObjXRef_DumpXRefManager"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Test - Object Xrefs" 
	Tooltip:"Unit Test - Obj Xrefs - DumpXRefManager" 
(	
--------------------------------------------
-- Unit Test methods
--------------------------------------------	
-- Unit test fixture setup
function setupUT =
(
	-- no scene setup required
)

-- Unit test fixture teardown
function teardownUT =
(
	-- no scene tear down required
)

-- This does the real dumping of xref records
function testDumpObjXRefMgr =
(
	oxUtil.DumpObjXRefMgr true 
)

--------------------------------------------
-- Macro methods
--------------------------------------------
On isEnabled return true
On isVisible return true

global createTestSuite = undefined
global ts = undefined
global oxUtil = undefined

On Execute Do	
(
	-- Create instance of xref utility "object"
  if undefined == createObjXRefUtil do fileIn ((getdir #scripts) + "/startup/MxsUnitTest/ObjXRef/OBjXRef.Util.ms")
	oxUtil = createObjXRefUtil()
  	
	-- Create instance of mxs unit test "object"
	if createTestSuite == undefined do fileIn ((getdir #scripts) + "/startup/MxsUnitTest/MxsUnitTestFramework.ms")
	if createTestSuite == undefined do fileIn ("MxsUnitTestFramework.ms")

  -- create a test suite that outputs cmmands into a log file and messages into a new maxscript window (default behaviour)
  -- local logFileName = ((getdir #maxroot) + "DumpObjXRefMgr.log")
  -- global xrefDumpWnd = newScript()
  ts = createTestSuite "DumpXrefRecords" setupUT teardownUT -- cmdLog:undefined msgLog:xrefDumpWnd
	ts.addTest testDumpObjXRefMgr
	ts.run()
)

) -- END MACRO
