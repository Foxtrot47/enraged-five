macroScript LoadAndRenderAll
	ButtonText:"UT.FileIO.LoadAndRender"
	Category:"QE Tools - File IO" 
	internalCategory:"QE Tools - File IO" 
	Tooltip:"UT.FileIO.LoadAndRender" 
(

-- general locals
local files, sourceDir
local processTheScene, collectFileNames, existFile, existDir, makeDirPath, getTheFile, stripBasePath
local debug=false
local filesToProcess="*.max"
local loadTypeOptions = #(#normal, #xrefScene, #xrefObject, #merge)

local lastMemUsage = (sysinfo.getMaxMemoryInfo())[8] / 1048576.

-- script process specific locals

-- this function is called to load the file. Must return TRUE or FALSE
fn getTheFile theFile loadType quietMode:false = with quiet quietMode
(
	case loadType of
	(
		#normal: 		loadMaxFile theFile
		#xrefScene: 	classof (xrefs.addNewXRefFile theFile) == XRefScene
		#xrefObject: 	classof (objXRefMgr.AddXRefItemsFromFile theFile promptObjNames:false) == MixinInterface
		#merge: 		mergeMaxFile theFile
		default: 		false
	)
)

-- this function is called after the file is loaded. 
-- If the scene is processed ok, return true. Otherwise return false.
-- The name of the file and the filestream to use for logging are parameters
fn processTheScene theFile logfile doRender:true quietMode:false holdFetch:false cloneTest:false = with quiet quietMode
(
	try 
	(
		redrawViews()
		local testResult = true
		if (doRender) do
		(
			actionMan.executeAction 0 "40807"  -- Views: Activate All Maps	
			renderWidth = 640/4
			renderHeight = 480/4
			rendOutputFilename = (getFilenameFile theFile) +".jpg"
			rendTimeType = 1
			if viewport.activeViewport == 0 then viewport.activeViewport = viewport.numViews
			render outputfile:(getdir #image+"\\"+(getFilenameFile theFile) +".jpg")
		)
		if (holdFetch) do
		(
			local n_old = objects.count
			holdMaxFile()
			fetchMaxFile()
			local n_new = objects.count
			format "\thold/fetch test: \tobject count - pre/post: % / %\n" n_old n_new to:logfile
			format "\thold/fetch test: \tobject count - pre/post: % / %\n" n_old n_new
			if (n_old != n_new) do testResult = false

		)
		if (cloneTest) do
		(
			local n_old = objects.count
			res = maxops.CloneNodes objects
			if not res then
			(
				format "\tCloning objects failed\n" to:logfile
				format "\tCloning objects failed\n"
				testResult = false
			)
			else
			(
				local n_new = objects.count
				format "\tclone test: \t\tobject count - pre/post: % / %\n" n_old n_new to:logfile
				format "\tclone test: \t\tobject count - pre/post: % / %\n" n_old n_new
				if ((n_old*2) != n_new) do testResult = false
			)
		)
		testResult
	)
	catch
		false
)

-- this function is called to collect all the files in a base directory with a given extension.
-- if subdirs:true is specified, files in subdirectories are also collected
fn collectFileNames baseDir ext subdirs:false fileList: =
(
	--format "Processing: %\n" baseDir
	if fileList == unsupplied do (fileList=#(); if baseDir[baseDir.count] != "\\" do baseDir += "\\")
-- get the files with the specified extension in this directory
	for fname in (getFiles (baseDir+ext)) do append fileList fname
-- recursively process each subdirectory
	if subdirs do 
		for aDir in (getDirectories (baseDir+"*.*")) do 
			collectFileNames aDir ext subdirs:true fileList:fileList
	fileList
)

-- a function to strip the base directory from the file name in the array of file names
fn stripBasePath baseDir files=
(
	local baseDirLen=baseDir.count+1
	for i=1 to files.count do files[i]=substring files[i] baseDirLen -1
)

Rollout LoadAndRenderAll "Load and Render" width:200 
(
	-- define the user interface
	Button aboutProcessAll "About..."
	Label srclbl "Source Directory:" offset:[0,5]
	Button SourcePath "--none--" width:190
	label notelbla "note: a log file is created in this directory"
	checkbox doSubDirs "Process subdirectories" checked:true offset:[-8,0]
	label num2process_t "# Files to process:" across:2 align:#left offset:[0,5]
	label numFilesToProcess "0" offset:[0,5]
	radiobuttons loadType "File load type:" labels:#("Normal Load","XRef Scene","XRef Objects", "Merge") align:#left offset:[0,5]
	checkbox doRender "Render Files" checked:true offset:[-8,0]
	checkbox quietMode "Quiet Mode" checked:true offset:[-8,0]
	checkbox holdFetch "Hold && Fetch" checked:true offset:[-8,0]
	checkbox cloneTest "Clone All" checked:true offset:[-8,0]
	spinner sRepeatCount "File repeat count: " type:#integer range:[0,100,0] fieldWidth:40 align:#left offset:[-8,0]
	Button Process "Start File Processing" enabled:false offset:[0,5]
	label nprocessedlbl "# files processed: " across:2
	label l_nprocessed "0"
	progressbar pbar
	label l_memstat1 "Memory (Private Bytes) (MB):" align:#left
	label l_memstat2a "Used:" align:#left across:2
	label l_memstat2b "" align:#left
	label l_memstat3a "Delta:" align:#left across:2
	label l_memstat3b "" align:#left
	label l_memstat4a "Largest Memory Block Avail (MB):" align:#left
	label l_memstat4b "" align:#left
	label l_abortText "" align:#left
	
	-- following is executed when the utility is started
	on LoadAndRenderAll open do 
	(	
		files=#()
		meminfo = sysinfo.getMaxMemoryInfo()
		meminfo[8] /= 1048576.
		l_memstat2b.text = meminfo[8] as string
		lastMemUsage = meminfo[8]
		l_memstat4b.text = ">>>> CALCULATING <<<<"
		Module_Snooper_Interface.GetLargestAvailableHeapBlockSizes  &largestBlocks numBlocks:1
		largestBlocks[1]  /= 1048576.
		l_memstat4b.text = largestBlocks[1] as string
	)
	
	-- define the user interface handlers
	
	on aboutProcessAll pressed do
			messagebox "Load And Render All\nby Larry Minton" 
	
	on SourcePath pressed do 
	(
		if (sourceDir == undefined) do
			sourceDir=(Getdir #scene)
		local tmp=getSavePath caption:"Specify Source Directory" initialDir:sourceDir
		if tmp != undefined do 
		(	
			sourceDir=tmp
			if sourceDir[sourceDir.count] != "\\" do sourceDir += "\\"
			SourcePath.text = SourcePath.tooltip = sourceDir
			if debug do (format "Source directory: %\n" sourceDir)
			Process.enabled=true
			files=collectFileNames sourceDir filesToProcess subdirs:doSubDirs.checked
			stripBasePath sourceDir files
			numFilesToProcess.text=files.count as string
		)
	)--End on SourcePath
	
	on doSubDirs changed state do
	(
		if sourceDir != undefined do 
		(
			files=collectFileNames sourceDir filesToProcess subdirs:doSubDirs.checked
			stripBasePath sourceDir files
			numFilesToProcess.text=files.count as string
		)
	)
	
	on Process pressed do
	(
		local theFile, status_ok
		local num_failed=0
		local num_ok=0
		local num_processed=0
		deletefile (sourceDir+"message_file.log")
		logfile=createfile (sourceDir+"message_file.log")
		if logfile == undefined do throw "Error creating log file in " sourceDir
		local fcount = files.count as string
		resetMaxFile #noPrompt
		meminfo = sysinfo.getMaxMemoryInfo()
		meminfo[8] /= 1048576.
		lastMemUsage = meminfo[8]
		str = "Memory Stats: Used:" + meminfo[8] as string + " / Leaked: " + (meminfo[8]-lastMemUsage) as string
		format "\t%\n" str to:logfile
		l_memstat2b.text = meminfo[8] as string
		l_memstat3b.text = (meminfo[8]-lastMemUsage) as string
		
		Module_Snooper_Interface.GetLargestAvailableHeapBlockSizes  &largestBlocks numBlocks:5
		for i = 1 to largestBlocks.count do largestBlocks[i] /= 1048576.
		format "\tLargest avail memory blocks in MB: %\n" largestBlocks to:logfile
		l_memstat4b.text = largestBlocks[1] as string

		l_abortText.text = "Press and hold Esc key to cancel"
		for fin in files while not keyboard.escPressed do 
		(
			for repeatval = 0 to sRepeatCount.value do
			(
				-- rebuild the file name
				theFile=sourceDir + fin
				format "Processing file: %\n" theFile to:logfile
				flush logfile
				format "Processing file: %\n" theFile
				try (status_ok=getTheFile theFile loadTypeOptions[loadType.state] quietMode:quietMode.checked)
				catch (status_ok=false)
				if status_ok then 
					status_ok=processTheScene theFile logfile doRender:doRender.checked quietMode:quietMode.checked \
															  holdFetch:holdFetch.checked cloneTest:cloneTest.checked 
				else
					messagebox ("file load failure on file "+theFile)
				try 
				(
					resetMaxFile #noPrompt
				)
				catch 
				(
					messagebox ("file reset failure on file "+theFile)
					throw()
				)
				try
				(
					gc()
				)
				catch
				(
					messagebox ("gc failure on file "+theFile)
				)
				meminfo = sysinfo.getMaxMemoryInfo()
				meminfo[8] /= 1048576.
				str = "Memory Stats: Used:" + meminfo[8] as string + " / Leaked: " + (meminfo[8]-lastMemUsage) as string
				format "\t%\n" str to:logfile
				l_memstat2b.text = meminfo[8] as string
				l_memstat3b.text = (meminfo[8]-lastMemUsage) as string
				lastMemUsage = meminfo[8]
		
				Module_Snooper_Interface.GetLargestAvailableHeapBlockSizes  &largestBlocks numBlocks:5
				for i = 1 to largestBlocks.count do largestBlocks[i] /= 1048576.
				format "\tLargest avail memory blocks in MB: %\n" largestBlocks to:logfile
				l_memstat4b.text = largestBlocks[1] as string

				num_processed += 1
				l_nprocessed.text = num_processed as string
				pbar.value=(100*num_processed/files.count)
				if status_ok then
					num_ok += 1
				else
				(
					format "Error processing file: %\n" theFile to:logfile
					num_failed += 1
				)
			)
		)
		pbar.value = 0
		l_nprocessed.text = "0"
		l_abortText.text = ""
		resetMaxFile #noPrompt
		gc()
		close logfile
		messagebox ("Number of failures processing files: "+(num_failed as string)+ 
				    "\nNumber of successes processing files: "+(num_ok as string)+
					"\nSee file "+(logfile as string)+" for message log") title:"Processing Complete"
	)--End on Process
)--End LoadAndRenderAll

On Execute Do 
(
	createdialog LoadAndRenderAll
)

) -- end macro
