macroScript UnitTest_ObjXRef_VerifyDefects
	ButtonText:"UnitTest_ObjXRef_VerifyDefects"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Tests - Object Xrefs" 
	Tooltip:"Unit Test - Obj Xref - VerifyDefects" 
(	
--------------------------------------------------------------------------------
-- Globals to this macro
--------------------------------------------------------------------------------
global gTempFile01 = (getdir #Temp) + "\\UT.ObjXref.VerifyDefects.01.max"
global gTempFile02 = (getdir #Temp) + "\\UT.ObjXref.VerifyDefects.02.max"
global gTempFile03 = (getdir #Temp) + "\\UT.ObjXref.VerifyDefects.03.max"

--------------------------------------------------------------------------------
-- Utility methods
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
function xrefTwiceAndCombine aFileName aXrefOptRec1 aXrefOptRec2 = 
(
	objXrefMgr.dupObjNameAction = #xref
	local recs = #()
	local rec1 = objXrefMgr.AddXRefItemsFromFile aFileName promptObjNames:false xrefOptions:aXrefOptRec1
	ts.assert (rec1 != undefined) "Create first xref record"
	recTotalObjCount = rec1.ItemCount #(#XRefAllType)
	local rec2 = objXrefMgr.AddXRefItemsFromFile aFileName promptObjNames:false xrefOptions:aXrefOptRec2
	ts.assert (rec2 != undefined) "Create second xref record"
	local recTotalObjCount = recTotalObjCount + rec2.ItemCount #(#XRefAllType)
	append recs rec1
	append recs rec2
	
	-- Combine the two records
	local rec = objXrefMgr.CombineRecords recs
	
	-- Return count of xrefs in un-combined records
	recTotalObjCount
)

--------------------------------------------------------------------------------
-- Unit Test methods
--------------------------------------------------------------------------------

-- Unit test fixture setup------------------------------------------------------
function setupUT =
(
	resetMaxFile #noprompt
)

-- Unit test fixture teardown---------------------------------------------------
function teardownUT =
(
	-- no scene tear down required
)

--------------------------------------------------------------------------------
-- Tests combining xref records that point to the same
-- source file and use the same xref options
function testCombineIdenticalXrefRecords =
(
	-- create a source file 
	b=box()
	local res = oxUtil.saveTmpFile gTempFile01
	ts.assert (true == res) "Save Temp File"

	-- create xref records and combine them
	resetmaxFile #noPrompt
	local xrefOptions = #(#xrefModifiers)
	local recTotalObjCount =  xrefTwiceAndCombine gTempFile01 xrefOptions xrefOptions

	-- tests result of combining xref records
	ts.assert(1 == objXRefMgr.recordCount) "Xref Record Combine - successful"
	local rec = objXRefMgr.getRecord 1
	ts.assert (recTotalObjCount == rec.ItemCount  #(#XRefAllType)) "Validate xref count in combined record"
)

--------------------------------------------------------------------------------
-- Tests combining xref records that point to the same
-- source file and use different xref options
function testCombineNonIdenticalXrefRecords =
(
	-- create a source file 
	b=box()
	local res = oxUtil.saveTmpFile gTempFile01
	ts.assert (true == res) "Save Temp File"

	-- create xref records and combine them
	resetmaxFile #noPrompt
	local xrefOptionsRec1 = #(#xrefModifiers)
	local xrefOptionsRec2 = #(#xrefModifiers, #mergeControllers)
	local recTotalObjCount =  xrefTwiceAndCombine gTempFile01 xrefOptionsRec1 xrefOptionsRec2

	-- tests result of combining xref records
	ts.assert(objXRefMgr.recordCount > 1) "Xref Record Combine - failed"
)

--------------------------------------------------------------------------------
-- Tests combining xref records that point to the same
-- source file and use the same xref options except for
-- the ones that are not taken into account when 
-- combining them
function testCombineQuasyIdenticalXrefRecords =
(
	-- create a source file 
	b=box()
	local res = oxUtil.saveTmpFile gTempFile01
	ts.assert (true == res) "Save Temp File"

	-- create xref records and combine them
	resetmaxFile #noPrompt
	local xrefOptionsRec1 = #(#xrefModifiers)
	local xrefOptionsRec2 = #(#xrefModifiers, #selectNodes)
	local recTotalObjCount =  xrefTwiceAndCombine gTempFile01 xrefOptionsRec1 xrefOptionsRec2

	-- tests result of combining xref records
	ts.assert(1 == objXRefMgr.recordCount) "Xref Record Combine - successful"
	local rec = objXRefMgr.getRecord 1
	ts.assert (recTotalObjCount == rec.ItemCount  #(#XRefAllType)) "Validate xref count in combined record"
)
--------------------------------------------------------------------------------
-- http://clarifyweb:9140/dtsthin/Front?act=defect&defID=810478
function testCreateXRefProxy =
(
	-- create two source files 
	box pos:[50, 0, 0]
	local res = oxUtil.saveTmpFile gTempFile01
	ts.assert (true == res) "Save Temp File 01"
	resetMaxFile #noprompt
	sphere pos:[0, 50, 0]
	local res = oxUtil.saveTmpFile gTempFile02
	ts.assert (true == res) "Save Temp File 02"
	
	-- Xref object from File 1	
	resetMaxFile #noprompt
	local xRec = objXRefMgr.AddXRefItemsFromFile gTempFile01
	ts.assert(undefined != xRec) "File 01 obj xrefed"
	
	-- set up proxy for first xref object
	local xi = xRec.getItem 1 #XRefObjectType
	ts.assert(undefined != xi) "Get first xref objects from xref record"
	objXrefMgr.SetProxyItemSrcFile xi gTempFile02
	objXrefMgr.SetProxyItemSrcName xi "Sphere01"
	xi.useProxy = true
	local baseobj = xi.getSrcItem()
	ts.assert(Sphere == classof(baseobj)) "Xref src is proxy"
	xi.useProxy = false
	baseobj = xi.getSrcItem()
	ts.assert(Box == classof(baseobj)) "Xref src is not proxy"
)
	
--------------------------------------------------------------------------------
-- Test combining scene and object xrefs in the same scene
-- http://clarifyweb:9140/dtsthin/Front?act=defect&defID=787909
function testSceneObjXrefCombination = 
(
	-- create two source files 
	box pos:[50, 0, 0] material:(standardMaterial diffuseColor:red)
	local res = oxUtil.saveTmpFile gTempFile01
	ts.assert (true == res) "Save Temp File 01"
	resetMaxFile #noprompt
	sphere pos:[0, 50, 0] material:(standardMaterial diffuseColor:blue)
	local res = oxUtil.saveTmpFile gTempFile02
	ts.assert (true == res) "Save Temp File 02"
	resetMaxFile #noprompt
	
	-- Scene xref File 01 and object xref File 02, the save and reload
	local sx = xrefs.addNewXRefFile gTempFile01
	ts.assert(undefined != sx and 1 == sx.tree.children.count) "File 01 scene xrefed"
	local ox = objXrefMgr.AddXRefItemsFromFile gTempFile02
	local xItemCount = ox.ItemCount #(#XRefAllType)
	ts.assert(undefined != ox) "File 02 scene xrefed"
	local res = oxUtil.saveTmpFile gTempFile03
	ts.assert (true == res) "Save master scene"
	res= loadMaxFile gTempFile03 quiet:true
	-- test that master file reloaded properly
	sx = xrefs.getXrefFile 1
	ts.assert(undefined != sx and 1 == sx.tree.children.count) "Scene xref reloaded"
	ox = objXrefMgr.getRecord 1
	ts.assert(undefined != ox and xItemCount == ox.ItemCount #(#XRefAllType)) "Object xref reloaded"
	local xItems = #()
	ox.getItems #(#XRefAllType) &xItems
	for xi in xItems do (
		ts.assert(false == xi.unresolved) ("Item " + (xi.srcItemName) + "[class = " + ((classof xi) as string) + "] resolved")
	)
)	
--------------------------------------------------------------------------------
-- 	
--------------------------------------------------------------------------------
-- Macro methods
--------------------------------------------------------------------------------
On isEnabled return true
On isVisible return true

global createTestSuite = undefined
global ts = undefined
global oxUtil = undefined

On Execute Do	
(
	-- Create instance of xref utility "object"
  if undefined == createObjXRefUtil do (
  	fileIn ((getdir #scripts) + "/startup/MxsUnitTest/ObjXRef/OBjXRef.Util.ms")
  )
	oxUtil = createObjXRefUtil()

	-- Load Mxs Unit Test framework
	if createTestSuite == undefined do (
		fileIn ((getdir #scripts) + "/Startup/MxsUnitTest/MxsUnitTestFramework.ms")
	)
	-- if above fails, add exe\MxsUnitTest as 3rd party plugin path and manually
	-- load and evaluate your unit test macro. The line below will ensure the mxs 
	-- unit test framework is loaded in this case
	if createTestSuite == undefined do (
		fileIn ("MxsUnitTestFramework.ms")
	)

	-- create a test suite that outputs cmmands into a log file and messages into a new maxscript window (default behaviour)
	local testName = "UT.ObjXref.VerifyDefects"
	local logFileName = ((getdir #temp) + testName + ".log")
	ts = createTestSuite testName setupUT teardownUT cmdLog:(createFile logFileName)

	ts.addTest testCombineIdenticalXrefRecords
	ts.addTest testCombineNonIdenticalXrefRecords
	ts.addTest testCombineQuasyIdenticalXrefRecords
	ts.addTest testCreateXRefProxy
	ts.addTest testSceneObjXrefCombination
	ts.run()
)

) -- END MACRO
