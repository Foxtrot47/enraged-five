macroScript grpAssembliesInGroups
	ButtonText:"grpAssembliesInGroups"
	Category:"Qe Tools - Unit Tests Groups" 
	internalCategory:"Qe Tools - Unit Tests Groups" 
	Tooltip:"grpAssembliesInGroups" 
	-- Needs Icon
	--Icon:#("Max_edit_modifiers",1)
(	
	-- Unit test fixture setup
	fn setupUT =
	(
		resetmaxFile #noPrompt
		-- create an assembly and a "free" object
		global _boxes = #()
		b = box()
		append _boxes b
		for i=1 to 2 do 
		(
			tmpBox = copy b
			tmpBox.pos = [i * 30, 0, 0]
			append _boxes tmpBox
		)
		global _a = assemblymgr.assemble #(_boxes[1], _boxes[2]) name:"a" classDesc:Dummy

	)

	-- Unit test fixture teardown
	fn teardownUT =
	(
		--resetmaxFile #noPrompt
	)
	
	-----------------------------------------------------
	--
	fn test0 =
	(
		g = group #(_boxes[3], _a) name:"g"
		Select g
		print "test0 - CanDisassemble on closed group with inner assemblies (NO)"
		res = assemblymgr.canDisassemble $
		if (res == true)
			then throw "FAILED\n"
			else print "PASSED\n"
		print "test0 - CanExplode on closed group with inner assemblies (NO)"
		res = assemblymgr.canExplode $
		if (res == true)
			then throw "FAILED\n"
			else print "PASSED\n"
		print "test0 - CanExplode on closed assembly in closed group (NO)"
		res = assemblymgr.canExplode _a
		if (res == true)
			then throw "FAILED\n"
			else print "PASSED\n"
		print "test0 - CanOpen on closed group with inner assemblies (NO)"
		res = assemblymgr.canOpen $
		if (res == true)
			then throw "FAILED\n"
			else print "PASSED\n"
		print "test0 - CanClose on closed group with inner assemblies (NO)"
		res = assemblymgr.canClose $
		if (res == true)
			then throw "FAILED\n"
			else print "PASSED\n"
		print "test0 - CanDetach on closed group with inner assemblies (NO)"
		res = assemblymgr.canDetach _a
		if (res == true)
			then throw "FAILED\n"
			else print "PASSED\n"
		print "test0 - CanAssemble closed group member (NO)"
		res = assemblymgr.canAssemble _boxes[3]
		if (res == true)
			then throw "FAILED\n"
			else print "PASSED\n"


		setgroupopen g true
		print "test0 - CanDisassemble on closed assembly in open group (YES)"
		res = assemblymgr.canDisassemble _a
		if (res == false)
			then throw "FAILED\n"
			else print "PASSED\n"
		print "test0 - CanExplode on closed assembly in open group (YES)"
		res = assemblymgr.canExplode _a
		if (res == false)
			then throw "FAILED\n"
			else print "PASSED\n"
		print "test0 - CanOpen on closed assembly in open group (YES)"
		res = assemblymgr.canOpen _a
		if (res == false)
			then throw "FAILED\n"
			else print "PASSED\n"
		res = assemblymgr.Open _a
		print "test0 - CanDetach on open assembly member within inner assembly in open group (YEs)"
		res = assemblymgr.canDetach _boxes[1]
		if (res == false)
			then throw "FAILED\n"
			else print "PASSED\n"
		print "test0 - CanClose on open assembly in open group (YES)"
		res = assemblymgr.canClose _a
		if (res == false)
			then throw "FAILED\n"
			else print "PASSED\n"

	
	)


	-----------------------------------------------------
	--
	fn test1 =
	(
		print "test1 - Group assemblies"
		g = group #(_boxes[3], _a) name:"g"
		if (_boxes[1].assemblymember == false or
			_a.assemblyhead == false or
			(isgroupmember _a) == false)
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-----------------------------------------------------
	--
	fn test2 =
	(
		print "test2 - Open Group"
		g = group #(_boxes[3], _a) name:"g"
		setgroupopen g true 
		if (_boxes[1].assemblymember == false or
			_boxes[1].assemblymemberopen == true or
			_a.assemblyhead == false or
			_a.assemblyheadopen == true or
			(isgroupmember _a) == false or
			(isopengroupmember _a) == false)
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-----------------------------------------------------
	--
	fn test3 =
	(
		print "test3 - Close group"
		g = group #(_boxes[3], _a) name:"g"
		setgroupopen g true 

		setgroupopen g false
		if (_boxes[1].assemblymember == false or
			_boxes[1].assemblymemberopen == true or
			_a.assemblyhead == false or
			_a.assemblyheadopen == true or
			(isgroupmember _a) == false or
			(isopengroupmember _a) == true)
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-----------------------------------------------------
	--
	fn test4 =
	(
		print "test4 - Ungroup Group"
		g = group #(_boxes[3], _a) name:"g"
		
		ungroup g
		if (_boxes[1].assemblymember == false or
			_boxes[1].assemblymemberopen == true or
			_a.assemblyhead == false or
			_a.assemblyheadopen == true or
			(isgroupmember _a) == true or
			(isopengroupmember _a) == true)
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-----------------------------------------------------
	--
	fn test5 =
	(
		print "test5 - Explode Group"
		g = group #(_boxes[3], _a) name:"g"
		
		ExplodeGroup g
		if (_boxes[1].assemblymember == false or
			_boxes[1].assemblymemberopen == true or
			_a.assemblyhead == false or
			_a.assemblyheadopen == true or
			(isgroupmember _a) == true or
			(isopengroupmember _a) == true)
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-----------------------------------------------------
	--
	fn test6 =
	(
		print "test6 - Detach Assembly"
		g = group #(_boxes[3], _a) name:"g"
		setgroupopen g true 

		max group detach _a 
		if (_boxes[1].assemblymember == false or
			_boxes[1].assemblymemberopen == true or
			_a.assemblyhead == false or
			_a.assemblyheadopen == true or
			(isgroupmember _a) == true or
			(isopengroupmember _a) == true)
			then throw "FAILED\n"
			else print "PASSED\n"
	)
	
	-----------------------------------------------------
	--
	fn test7 =
	(
		print "test7 - Assemble Open Group Member (YES)"
		g = group #(_boxes[3], _a) name:"g"
		setgroupopen g true
		_a2 = assemblyMgr.Assemble _boxes[3] name:"a2" classDesc:Dummy
		if (_boxes[3].assemblymember == false or
			_boxes[3].assemblymemberopen == true or
			_a2.assemblyhead == false or
			_a2.assemblyheadopen == true or
			(isgroupmember _a2) == false or
			(isopengroupmember _a2) == false)
			then throw "FAILED\n"
			else print "PASSED\n"
	)
	-----------------------------------------------------
	--
	fn test8 =
	(
		print "test8 - Explode Assembly Open Group Member (YES)"
		g = group #(_boxes[3], _a) name:"g"
		setgroupopen g true
		res = assemblyMgr.Explode _a 
		if (_boxes[1].assemblymember == true or
			_boxes[1].assemblymemberopen == true or
			(isgroupmember _boxes[1]) == false or
			(isopengroupmember _boxes[1]) == false)
			then throw "FAILED\n"
			else print "PASSED\n"
	)
	-----------------------------------------------------
	--
	fn test9 =
	(
		print "test9 - Disassemble Assembly Open Group Member (YES)"
		g = group #(_boxes[3], _a) name:"g"
		setgroupopen g true
		res = assemblyMgr.Disassemble _a 
		if (_boxes[1].assemblymember == true or
			_boxes[1].assemblymemberopen == true or
			(isgroupmember _boxes[1]) == false or
			(isopengroupmember _boxes[1]) == false)
			then throw "FAILED\n"
			else print "PASSED\n"
	)
	
	-- Macro methods
	On isEnabled return true
	On isVisible return true

	On Execute Do	
	(
	    global createTestSuite
	  if createTestSuite == undefined do fileIn ((getdir #scripts) + "/../../MxsUnitTest/MxsUnitTestFramework.ms")
		ts = createTestSuite "grpAssembliesInGroups" setupUT teardownUT
		ts.addTest test0
		ts.addTest test1
		ts.addTest test2
		ts.addTest test3
		ts.addTest test4
		ts.addTest test5
		ts.addTest test6
		ts.addTest test7
		ts.addTest test8
		ts.addTest test9


		-- add more tests
		ts.run()
	)
)
