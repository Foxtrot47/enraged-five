-- Rockstar Utils macros
-- Rockstar North
-- 2/2/2005
-- by Greg Smith

-- ///////////////////////////////////////////////////////////////////////////
-- utils
-- ///////////////////////////////////////////////////////////////////////////
macroscript RsWorkBench
	category:"RS Utils"
	ButtonText:"Workbench"
(
	local mapBrowserFilename = RsConfigGetToolsBinDir() + "/workbench/Workbench.exe"
	ShellLaunch mapBrowserFilename ""
)

macroscript RsWarpPlayer
	category:"RS Utils"
	ButtonText:"Warp Player To Selection"
(
	fileIn "pipeline/util/warp.ms"
	
	::warpPlayerToSelection()
)

macroscript ShowSelector
	category:"RS Utils"
	ButtonText:"Object Selector"
(	
	filein (::RsConfigGetWildWestDir() + "script/3dsMax/General_tools/selector.ms")
)

macroscript BatchUpdate
	category:"RS Utils"
	ButtonText:"Batch Update"
(	
	filein "pipeline/util/batchupdate.ms"
)

macroscript StopProgress
	category:"RS Utils"
	ButtonText:"Stop Progress"
(
	progressend()
)

macroscript RedrawAllShaders
	category:"RS Utils"
	ButtonText:"Stop Progress"
(
	::RstUpdateRageShaders()
)

macroscript GlobalVarSearch
	category:"RS Utils"
	ButtonText:"Global var search"
(
	filein "pipeline/util/RsGlobalVarSearch.ms"
)

macroScript UVTools
	category:"RS Utils" 
	ButtonText:"UVTools V3.0"
(
	filein "rockstar/util/uvtools/Scripts/GTs-UVToolsExec.ms"
)

macroScript FragmentTools
	category:"RS Utils" 
	ButtonText:"Fragment Tools"
(
	try (DestroyDialog ::RsFragToolsRoll) catch()
	filein "pipeline/ui/FragmentTools.ms"
	CreateDialog ::RsFragToolsRoll
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Organisational Tools
-- ///////////////////////////////////////////////////////////////////////////

macroscript GuidTools
	category:"RS Organisational"
	ButtonText:"GUID Tools"
(	
	filein "pipeline/ui/GuidTools_ui.ms"
)


macroscript Bugstar
	category:"RS Organisational"
	tooltip:"Bugstar"
	ButtonText:"Bugstar"
(	
	filein "pipeline/helpers/debug/bugstar_ui.ms"
)

macroscript VisibilitySet
	category:"RS Organisational"
	tooltip:"Set the hide and freeze of objects by type"
	ButtonText:"Visibility Set"
(	
	filein "pipeline/helpers/maps/visibility_toolkit.ms"
)

macroscript CreateSelectionSets
	category:"RS Organisational"
	tooltip:"Create useful selection sets"
	ButtonText:"Create Selection Sets"
(
	undo "Create selection sets" on 
	(
		filein "pipeline/helpers/maps/CreateSelSets.ms"
	)
)

macroscript ProjMan
	category:"RS Organisational"
	ButtonText:"Project Manager"
(	
	filein "pipeline/ui/projman.ms"
)

macroscript Containers
	category:"RS Organisational"
	ButtonText:"Container Tools"
(
	filein (::RsConfigGetWildWestDir() + "script/3dsMax/Maps/Containers/Containers.ms")
)

-- Allows multiple containers to be inherited at once:
macroscript RsInheritContainers
	category:"RS Organisational"
	ButtonText:"Inherit Containers"
	tooltip:"Inherit Containers"
	Icon:#("Containers",6)
(
	::RsContFuncs.inheritBrowsedFiles()
)

-- Replaced selected containers with closed versions, allowing merged-in containers to be refreshed:
macroscript RsReinheritContainers
	category:"RS Organisational"
	ButtonText:"ReInherit Containers"
	tooltip:"ReInherit Selected Containers"
	Icon:#("Containers",11)
(
	local newConts = ::RsContFuncs.reInherit selection
	selectMore newConts
)

macroscript RsShowFileExportInExplorer
	category:"RS Organisational"
	ButtonText:"Show export-file: Current Maxfile"
(
	filein "pipeline/util/showInExplorer.ms"
	::RSshowFileExportInExplorer()
)

macroscript RSshowObjExportInExplorer
	category:"RS Organisational"
	ButtonText:"Show export-file: Selected object"
(
	filein "pipeline/util/showInExplorer.ms"
	::RSshowObjExportInExplorer selection
)

macroscript RSshowTXDExportInExplorer
	category:"RS Organisational"
	ButtonText:"Show export-file: Selected object's TXD"
(
	filein "pipeline/util/showInExplorer.ms"
	::RSshowTXDExportInExplorer selection
)

macroscript OpenDecisionMgr
	category:"RS Organisational"
	ButtonText:"Open Decision Manager"
(
	try (DestroyDialog RsDecisionControlRoll) catch()
	filein "pipeline/ui/SaveDecisionRoll.ms"
	CreateDialog RsDecisionControlRoll 
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Placement tools
-- ///////////////////////////////////////////////////////////////////////////

macroscript LiveMapEditing
	category:"RS Placement"
	ButtonText:"Live Map Editing (Max -> Game)"
(
	filein "pipeline/ui/live_map_editing.ms"
)

macroscript LiveMapEditingGameToMax
	category:"RS Placement"
	ButtonText:"Live Map Editing (Game -> Max)"
(
	filein "pipeline/ui/live_map_editing_game_to_max.ms"
)

macroscript Path_Deform
	category:"RS Placement"
	ButtonText:"Path Deform"
(
	filein "rockstar/util/path_deform.ms"
)

macroscript PlacementToolkit
	category:"RS Placement"
	ButtonText:"Placement Toolkit"
(	
	filein "pipeline/util/placement.ms"
)

macroScript ObjPosChecker
	category:"RS Placement"
	ButtonText:"Physical Object Position Checker"
(
	filein "pipeline/ui/ObjectPosChecker.ms"
)

macroscript Scatterer 
	category:"RS Placement"
	ButtonText:"Scatterer"
(
	filein "pipeline/util/scatterer.ms"
)

macroScript VegetationDropper
	category:"RS Placement"
	ButtonText:"Vegetation Dropper"
(
	filein "pipeline/helpers/terrain/terrain_utilities_ui.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Lighting
-- ///////////////////////////////////////////////////////////////////////////
macroscript RsCutsceneLightingTools
	category:"RS Lighting"
	tooltip:"Cutscene Lighting Tool"
	ButtonText:"Cutscene Lighting Tool"
(	
	filein "pipeline/ui/cutscenelight_ui.ms"
)

macroscript RadiosityLighting
	category:"RS Lighting"
	tooltip:"Radiosity and lighting toolset"
	ButtonText:"Radiosity and Lighting"
(	
	filein "pipeline/util/radiosity_lighting.ms"
)

macroscript LightLister
	category:"RS Lighting"
	tooltip:"Light Lister"
	ButtonText:"Light Lister"
(	
	filein "pipeline/util/light_lister.ms"
)

macroscript LightEditor
	category:"RS Lighting"
	tooltip:"Light Editor"
	ButtonText:"Light Editor"
(
	filein "pipeline/util/lighteditor.ms"
)

macroscript SceneBaker
	category:"RS Lighting"
	tooltip:"Scene Baker"
	ButtonText:"Scene Baker"
(
	filein "rockstar/util/scenebaker/rsn_scenebaker.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Characters
-- ///////////////////////////////////////////////////////////////////////////

macroscript RsMirroringPerVertTool
	category: "RS Characters"
	tooltip: "Vertex Mirroring Tool"
	ButtonText: "Vertex Mirroring Tool"
(
	filein "pipeline/helpers/rigging/mirroringpervert.ms"
)

macroscript RsFindDupBoneTags
	category: "RS Characters"
	tooltip: "Find duplicate bone tags and hash values on a skeleton"
	ButtonText: "Find Duplicate Bone Tags"
(
	filein "pipeline/util/finddup_bonetags.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Interiors and Props
-- ///////////////////////////////////////////////////////////////////////////

macroscript PropsMilos
	category: "RS Interiors"
	ButtonText: "Props and MILOs"
(
	filein "rockstar/helpers/milo.ms"
)

macroscript MiloOperations
	category: "RS Interiors"
	ButtonText: "MILO Operations"
(
	filein "pipeline/helpers/interiors/milo_ops.ms"
)

macroscript QuickDestructibleObjectSetup
	category: "RS Interiors"
	ButtonText: "Quick Destructible Object Setup"
(
	filein "pipeline/helpers/interiors/QuickDestructibleObjectSetup.ms"
)

macroscript DefaultEntitySetSelector
	category: "RS Interiors"
	ButtonText: "Default Entity Set Selector"
(
	filein "pipeline/ui/default_entity_set_selector.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Collision
-- ///////////////////////////////////////////////////////////////////////////

macroScript CollToolkit
	category:"RS Collision"
	tooltip:"Collision Toolkit"
	ButtonText:"Collision Toolkit"
(
	filein (::RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Collision/coll_toolkit.ms")
)

macroScript RsCollPaint
	category:"RS Collision"
	tooltip:"Old Procedural Painter"
	ButtonText:"Old Procedural Painter"
(
	MessageBox "This is the old procedural painter tool. We are now moving to a different system of editing procedurals. Please use the new procedural painter for any future edits where possible."
	
	fileIn (::RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Collision/ProcPainter_ui.ms")
)

macroScript AudioMatCheck
	category:"RS Collision"
	tooltip:"Audio Material Check"
	ButtonText:"Audio Material Check"
(
	filein "rockstar/helpers/collaudiocheck.ms"
)

macroScript RsCollisionSet
	category:"RS Collision"
	tooltip:"Collision Set"
	ButtonText:"Collision Set"
(
	filein (::RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Collision/collset.ms")
)

macroscript OptimisePlanar
	category:"RS Collision"
	ButtonText:"Optimise Planar Polys"
(
	::OptimisePlanarPolys()
)

macroscript SetBoundMaterials
	category:"RS Collision"
	tooltip:"Set bound materials"
	ButtonText:"Bound Material Toggle"
(
	filein "rockstar/helpers/boundmaterialflagtoggle.ms"
)

macroScript CollisionBoxCheck
	category:"RS Collision"
	tooltip:"Collision Box Check"
	ButtonText:"Collision Box Check"
(
	filein "rockstar/helpers/collboxcheck.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Xrefs
-- ///////////////////////////////////////////////////////////////////////////

macroscript XrefToolkit
	category: "RS Refs"
	ButtonText: "Ref Toolkit"
(
	filein "pipeline/helpers/maps/xref_toolkit.ms"
)

macroscript RSrefBrowser
	category: "RS Refs"
	ButtonText: "RSref Browser"
	Icon:#("rstar_main",1)
(
	::RSrefFuncs.createBrowser()
)

macroscript RSrefSettings
	category: "RS Refs"
	ButtonText: "RSref System Settings"
	Icon:#("rstar_main",2)
(
	::RSrefSettingFuncs.showRSrefSettings()
)

macroscript RSrefConverter
	category: "RS Refs"
	ButtonText: "Xref-to-RSref Converter"
	Icon:#("rstar_main",2)
(
	filein "pipeline/util/RSrefs_converter.ms"
)

macroscript PropViewer
	category:"RS Refs"
	ButtonText:"Prop Viewer"
(
	local propViewerFilename = ( systemTools.getEnvVariable "RS_TOOLSROOT" ) + "/bin/PropViewer/PropViewer.exe"
	ShellLaunch propViewerFilename ""
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Modelling
-- ///////////////////////////////////////////////////////////////////////////

macroScript ModelToolkit
	category:"RS Modelling"
	tooltip:"Modelling Toolkit"
	ButtonText:"Modelling Toolkit"
(
	filein (::RsConfigGetWildWestDir() + "script/3dsMax/Modelling/modellingToolkit.ms")
)

macroScript RsVertColToolkit
	category:"RS Modelling"
	tooltip:"Vertex Colour Toolkit"
	ButtonText:"Vertex Colour Toolkit"
(
	filein (::RsConfigGetWildwestDir() + "Script/3dsMax/Maps/Materials/vertexColour_ui.ms")
)

macroScript RSDecalCutter
	category:"RS Modelling"
	tooltip:"Decal Cutter"
	ButtonText:"Decal Cutter"
	Icon:#("Standard_Modifiers",22)
(
	filein "pipeline/helpers/models/decalCutter.ms"
)

macroScript RSDecalCutter
	category:"RS Modelling"
	tooltip:"Decal Cutter: Draw outline to create a new decal from selection"
	ButtonText:"Decal Cutter"
	Icon:#("Standard_Modifiers",22)
(
	filein "pipeline/helpers/models/decalCutter.ms"
)

macroScript RsModelSlicer
	category:"RS Modelling"
	tooltip:"Model Slicer: Draw outline to slice up selected meshes"
	ButtonText:"Model Slicer"
	Icon:#("PowerBoolean",9)
(
	filein "pipeline/helpers/models/modelSlicer.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Material
-- ///////////////////////////////////////////////////////////////////////////

macroScript MaterialToolkit
	category:"RS Material"
	tooltip:"Material Toolkit"
	ButtonText:"Material Toolkit"
(
	filein "pipeline/helpers/materials/material_toolkit.ms"
)

macroScript MaterialComparator
	category:"RS Material"
	tooltip:"Material Comparator"
	ButtonText:"Material Comparator"
(
	::gRsMtlComp.Show()
)

macroScript ChangeShaders
	category:"RS Material"
	tooltip:"Change shader attributes"
	ButtonText:"Change Shaders"
(
	filein "pipeline/helpers/materials/changeshaders.ms"
)

macroScript ReplaceShaders
	category:"RS Material"
	tooltip:"Replace one shader with another"
	ButtonText:"Replace Shaders"
(
	filein "pipeline/helpers/materials/replaceshaders.ms"
)

macroScript BadTex
	category:"RS Material"
	tooltip:"Bad Texture Finder"
	ButtonText:"Bad Texture Finder"
(
	filein "rockstar/helpers/badtex.ms"
)

macroScript RsTerrainSetup
	category:"RS Material"
	tooltip:"Setup for multipass"
	ButtonText:"Terrain Setup"
(
	filein "rockstar/helpers/terrain.ms"
)

macroScript RsTerrainCbPaint
	category:"RS Material"
	tooltip:"Setup for terrain painting"
	ButtonText:"Terrain Paint"
(
	filein (RsConfigGetWildwestDir() + "Script/3dsMax/Maps/Materials/terrain_painter.ms")
)

macroscript TexturePathRemap
	category:"RS Material"
	tooltip:"Texture Path Remap"
	ButtonText:"Texture Path Remap"
(
	filein "pipeline/helpers/materials/texturepathremap.ms"
)

macroscript VertexColourTools
	category:"RS Material"
	ButtonText:"Vertex Colour Tools"
(
	filein (::RsConfigGetWildwestDir() + "Script/3dsMax/Maps/Materials/vertexColour_toolset.ms")
)

macroscript EnvironmentMap
	category:"RS Material"
	tooltip:"Create cubic envmap based on surroundings"
	ButtonText:"Environment Map Generator"
(
	filein "rockstar/helpers/environmentmap.ms"
)

macroscript ShowMapsInViewport
	category:"RS Material"
	tooltip:"Show/hide diffuse texture maps for Rage Shader materials"
	ButtonText:"Show Maps in Viewport"
(
	filein "rockstar/helpers/ShowMapsInViewport.ms"
)

macroscript GrassPainter
	category:"RS Material"
	tooltip:"Tools for editing grass and tree materials"
	ButtonText:"Grass Painter"
(
	filein "pipeline/helpers/materials/GrassPainter.ms"
)

macroscript VertAnglePainter
	category:"RS Material"
	ButtonText:"Vert Angle Painter"
(
	filein "rockstar/helpers/VertAnglePaint.ms"
)

macroscript ProxyTextureSwap
	category:"RS Material"
	ButtonText:"Proxy texture Swap"
(
	filein "pipeline/helpers/materials/proxytexturemgr.ms"
)

macroscript ShaderEditor
	category:"RS Material"
	ButtonText:"Shader Editor"
(
	filein "pipeline/util/shadereditor.ms"
)

macroscript NitrousTools
	category:"RS Material"
	ButtonText:"Nitrous Tools"
(
	filein "pipeline/helpers/materials/nitro_switcher.ms"
)

macroscript SceneTextureValidator
	category:"RS Material"
	ButtonText:"Scene Texture Validator"
(
	filein "pipeline/helpers/materials/scene_texture_validator_ui.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Info
-- ///////////////////////////////////////////////////////////////////////////

macroscript InfoToolkit
	category:"RS Info"
	ButtonText:"Info Toolkit"
(	
	filein "pipeline/util/info_toolkit.ms"	
)

macroscript DoShowSceneStats
	category:"RS Info"
	ButtonText:"Scene Stats"
(	
	filein "rockstar/helpers/scenestats.ms"
)

macroscript DoShowTextureStats
	category:"RS Info"
	ButtonText:"Texture Stats"
(
	filein "rockstar/helpers/TextureStats.ms"
)

macroscript DoShowTXDStats
	category:"RS Info"
	ButtonText:"Texture TXD Stats"
(
	filein "rockstar/helpers/TextureStatsTXD.ms"
)


-- ///////////////////////////////////////////////////////////////////////////
-- Rs Lod
-- ///////////////////////////////////////////////////////////////////////////

macroscript LodEditor
	category:"RS Lod"
	ButtonText:"LOD Editor"
(	
	filein "pipeline/ui/lodeditor.ms"
)

macroscript LodToolkit
	category:"RS Lod"
	ButtonText:"LOD Toolkit"
(	
	filein "pipeline/util/lod_toolkit.ms"
)

macroscript LodModUtil
	category:"RS Lod"
	ButtonText:"Lod Modifier"
(	
	filein "rockstar/helpers/lodmod.ms"
)

macroscript SLOD2RefImporter
	category:"RS Lod"
	ButtonText:"SLOD2 Ref Importer"
(
	filein "pipeline/ui/slod2refimporter.ms"
)

macroscript RuntimeLODOverrideSubmission
	category:"RS Lod"
	ButtonText:"LOD Override Submission"
(
	filein "pipeline/ui/runtime_lod_override_submission.ms"
)

macroscript RuntimeLODOverrideImporter
	category:"RS Lod"
	ButtonText:"LOD Override Importer"
(
	filein "pipeline/ui/runtime_lod_override_importer.ms"
)

macroscript SimplygonToolkit
	category:"RS Lod"
	ButtonText:"Simplygon Toolkit"
(
	filein ( RsConfigGetWildwestDir() + "script/3dsmax/simplygon/simplygon_toolkit.ms" )
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Internal Refs
-- ///////////////////////////////////////////////////////////////////////////

macroscript IrefToolkit
	category:"RS InternalRefs"
	ButtonText:"Iref Toolkit"
(	
	filein "pipeline/helpers/maps/iref_toolkit.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs VFX
-- ///////////////////////////////////////////////////////////////////////////

macroscript VFXToolkit
	category:"RS VFX"
	ButtonText:"VFX Toolkit"
(	
	filein "pipeline/util/vfx.ms"
)

macroscript StatedAnimationSetup
	category:"RS VFX"
	ButtonText:"Stated Animation Setup"
(	
	filein "pipeline/util/statedAnimTool.ms"
)

macroscript WaterAlphaPaint
	category:"RS VFX"
	ButtonText:"Water Alpha Paint"
(	
	filein "pipeline/ui/WaterAlphaPainter_ui.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Vehicle
-- ///////////////////////////////////////////////////////////////////////////

macroScript VehicleToolkit
	category:"RS Vehicle"
	ButtonText:"Vehicle Toolkit"
(
	filein "pipeline/helpers/vehicles/vehicle.ms"
)

macroscript RsImportVehicleRec
	category:"RS Vehicle"
	ButtonText:"Import Vehicle Recording"
(	
	fileIn "pipeline/import/importvehiclerecording.ms"
	local recordingFilename = getOpenFileName caption:"Select a vehicle recording .ivr file" types:"Vehicle recording (*.ivr)|*.ivr" 
	local vehicleFilename = getOpenFileName caption:"Select a vehicle max file" types:"Vehicle (*.max)|*.max" 
	
	if ( recordingFilename != undefined and vehicleFilename != undefined ) then (

		clearSelection()
		mergeMAXFile vehicleFilename #prompt #select
		::RsImportVehicleRecording recordingFilename
	) 
	else (
	
		messagebox "No recording or vehicle file selected"
	)
	
	
)

-- ///////////////////////////////////////////////////////////////////////////
-- Cloth tools
-- ///////////////////////////////////////////////////////////////////////////

macroscript RsClothEditor
	category:"RS Cloth"
	ButtonText:"Rage Cloth Editor"
(
	filein "rage/RageClothEditor.ms"
)

macroscript RsClothLodTool
	category:"RS Cloth"
	ButtonText:"Cloth LOD Editor"
(
	filein "pipeline/ui/RSClothLodEditor.ms"
	::CreateClothLodEditorDialog()
)

macroscript RsClothCollTool
	category:"RS Cloth"
	ButtonText:"Cloth Collision Editor"
(
	filein "pipeline/ui/RSClothCollEditor.ms"
	::CreateClothCollEditorDialog()
)
macroscript RsClothMiscTools
	category:"RS Cloth"
	ButtonText:"Cloth Misc Tools"
(
	filein "pipeline/ui/clothMiscTools.ms"
)
-- ///////////////////////////////////////////////////////////////////////////
-- Misc 
-- ///////////////////////////////////////////////////////////////////////////

macroscript TestObjects
	category: "RS Debug"
	ButtonText:"Test Objects"
(
	if (RsCheckMap == undefined) do 
	(
		fileIn "pipeline/export/maps/maptest.ms"
	)
	
	::RsCheckMap()
	
	if (not ::gRsULog.HasErrors()) do 
	(
		messageBox "No problems found" title:"Test Objects"
	)
)

macroscript SetCameraFromGame
	category: "RS Debug"
	ButtonText:"Set Camera From Game"
(
	filein "rockstar/helpers/camload.ms"	
)

macroscript ScriptDump
	category: "RS Debug"
	ButtonText:"Script Dump"
(
	filein "rockstar/helpers/scriptdump.ms"	
)

macroscript RsFragmentTuner
	category: "RS Debug"
	ButtonText:"Fragment tuner"
(
	filein "pipeline/util/fragtune.ms"
)

macroscript ERotFinder
	category: "RS Debug"
	ButtonText:"Rotation Finder"
(
	filein "pipeline/ui/ERotFinder.ms"
)

macroscript SetObjectAttr
	category: "RS Debug"
	ButtonText:"Set Object Attributes"
(
	fileIn "pipeline/ui/SetObjectAttr.ms"	
)

macroscript RsNoteNodes
	category:"RS Debug"
	ButtonText:"Note Nodes"
(
	filein "pipeline/ui/notenodes_ui.ms"
)

macroscript RSremoveDuplicateMenus
	category:"RS Debug"
	ButtonText:"Remove duplicated unused menu-items"
(
	::RsRemoveUnusedDupeMenus()
)

macroscript RSshowMemSize
	category:"RS Debug"
	ButtonText:"3DSMax MemSize"
(
	fileIn "pipeline/helpers/debug/RS_memsize.ms"
)

macroscript RSResetAnimGroupName
	category:"RS Debug"
	ButtonText:"Reset Anim GroupName"
(
	local stateIdx = getattrindex "Gta Object" "animState"
	local groupNameIdx = getattrindex "Gta Object" "groupName"

	-- AJM: Changed the default animState value to be None in the att file, but old saved files
	-- may still have StartState saved
	local gtaObjects = \
		for o in $objects where 
			(getattrclass o == "Gta Object") and 
			(getattr o groupNameIdx != "DEFAULT") and 
			(not ::gRsStatedAnimTool.IsInValidAnimGroup (getattr o groupNameIdx)) 
				collect o 

	if ( gtaObjects.count > 0 ) then
	(
		msg = stringStream ""
		format "These objects have an animation groupName set but no state, do you want to reset them to the default value?\n\n" to:msg

		for o in gtaObjects do ( format "% " o.name to:msg )
		if (QueryBox msg title:"Objects") do
		(
			for o in gtaObjects do
			(
				setattr o groupNameIdx (getattrdefault "Gta Object" groupNameIdx)
			)
		)
	)
	else
	(
		MessageBox "No objects were found that have an incorrect anim groupName set"
	)
)

-- ///////////////////////////////////////////////////////////////////////////
-- export
-- ///////////////////////////////////////////////////////////////////////////

macroScript RsMapExport
	category:"RS Export"
	tooltip:"Map Export"
	ButtonText:"Map Export"
(
	filein "rockstar/export/map.ms"
)

macroScript RsMapExportReports
	category:"RS Export"
	tooltip:"Show non-modular Map Export Reports"
	ButtonText:"Map Export Reports"
(
	filein "pipeline/export/ui/ExportReport.ms"

	local maps = ::RsMapGetMapContainers()
	::RsMapShowExportReports maps modal:false countdown:false
)

macroScript ShowUniversalLogDialog
	category:"RS Export"
	tooltip:"Show non-modular Universal Log Dialog"
	ButtonText:"Universal Log"
(
	::ShowRsUlogDialog()
)

macroScript ClearUniversalLogDir
	category:"RS Export"
	tooltip:"Clear the universal log directory"
	ButtonText:"Clear Log Directory"
(
	::gRsULog.ClearLogDirectory()
)

macroScript RsPedExport
	category:"RS Export"
	tooltip:"Ped Export"
	ButtonText:"Ped Export"
(
	filein "pipeline/export/ui/ped_ui.ms"
)

macroScript RsVehicleExport
	category:"RS Export"
	tooltip:"Vehicle Export"
	ButtonText:"Vehicle Export"
(
	filein "pipeline/export/ui/vehicle_exporter_ui.ms"
)

macroScript RsExpressionsExport
	category:"RS Export"
	tooltip:"Expressions Export"
	ButtonText:"Expressions Export"
(
	filein "pipeline/export/ui/expressions_exporter_ui.ms"
)

macroScript RsModelExport
	category:"RS Export"
	tooltip:"Model/Weapon Export"
	ButtonText:"Model/Weapon Export"
(
	filein "pipeline/export/ui/model_exporter_ui.ms"
)

macroScript RsSingleIPLExport
	category:"RS Export"
	tooltip:"Single IPL Export"
	ButtonText:"Single IPL Export"
(
	filein "pipeline/export/ui/MapSingleIPLExporter.ms"
)

macroscript RsPlayerCoords
	category:"RS Export"
	ButtonText:"Export Player Coords"
(	
	filein "pipeline/export/playercoords.ms";
)

macroscript RsWaterExport
	category:"RS Export"
	ButtonText:"Export Water"
(	
	filein "pipeline/export/ui/water_ui.ms";
)

macroscript RsExportImportZones
	category:"RS Export"
	ButtonText:"Export/Import Zones"
(	
	filein "pipeline/export/ui/ExportZones.ms";
)

macroscript RsExportAmbientScripts
	category:"RS Export"
	ButtonText:"Export Ambient Scripts"
(	
	filein "rockstar/export/ambientscript.ms";
)

macroscript RsExportCutLoc
	category: "RS Export"
	ButtonText: "Export Cutscene Locations"
(
	filein "rockstar/export/custscenelocation.ms"
)

macroscript RsExportViseme
	category: "RS Export"
	ButtonText: "Export Visemes"
(
	filein "rockstar/export/visemeexport.ms"
)

macroscript RsExportVectorMap
	category:"RS Export"
	ButtonText:"Export VectorMap"
(
	--filein "pipeline/util/xml.ms" -- loaded on startup by startup.ms
	filein "pipeline/export/maps/vector_map.ms"
	
	local filename = getSaveFileName caption:"Save Vector Map As..." types:"Vector Map XML Files (*.xml)|*.xml"
	if ( undefined != filename ) then
		::ExportVectorMapData filename
)

-- ///////////////////////////////////////////////////////////////////////////
-- Quad menu
-- ///////////////////////////////////////////////////////////////////////////

macroscript ShowContainerLodDetail
	category:"RS Utils"
	ButtonText:"Show ContainerLod Detail"
(
	::RsShowLodLevel #containerLod
)

macroscript ShowSuperLodDetail
	category:"RS Utils"
	ButtonText:"Show SuperLod1 Detail"
(
	::RsShowLodLevel #slod1
)

macroscript ShowSuperLod2Detail
	category:"RS Utils"
	ButtonText:"Show SuperLod2 Detail"
(
	::RsShowLodLevel #slod2
)

macroscript ShowSuperLod3Detail
	category:"RS Utils"
	ButtonText:"Show SuperLod3 Detail"
(
	::RsShowLodLevel #slod3
)

macroscript ShowLodDetail
	category:"RS Utils"
	ButtonText:"Show Lod Detail"
(
	::RsShowLodLevel #lod
)

macroscript ShowHiDetail
	category:"RS Utils"
	ButtonText:"Show High Detail"
(
	::RsShowLodLevel #high
)

macroscript RsLodShowTool 
	category:"RS Utils"
	ButtonText:"Show LOD Level tool"
(
	::RsShowLodSlider.create()
)

macroscript ShowMapsInViewportQuad
	category:"RS Utils"
	tooltip:"Show/hide diffuse texture maps for Rage Shader materials"
	ButtonText:"Show Maps in Viewport"
(
	filein "rockstar/helpers/ShowMapsInViewport.ms"
)

macroScript TextureTools
	category:"RS Utils"
	tooltip:"Texture Tools"
	ButtonText:"Texture Tools"
(
	filein "gta/utils/texturetools.ms"
)

macroscript BatchSetAttr
	category:"RS Utils"
	tooltip:"Batch Set Attributes"
	ButtonText:"Batch Set Attributes"
(
	filein "pipeline/ui/batchsetattr.ms"
)

macroscript CreateCollMeshFromExist
	category:"RS Utils"
	ButtonText:"Create Collision Mesh From Existing"
(
	::CreateCollisionMeshRemoved()
)

macroscript CreateCollMesh
	category:"RS Utils"
	ButtonText:"Create Collision Mesh"
(
	::CreateCollisionMesh()
)

macroscript CreateCollCapsule
	category:"RS Utils"
	ButtonText:"Create Collision Capsule"
(
	::RsCreateCollisionGeometry collClass:#capsule
)

macroscript CreateCollCylinder
	category:"RS Utils"
	ButtonText:"Create Collision Cylinder"
(
	::RsCreateCollisionGeometry collClass:#cylinder
)

macroscript CreateShadMesh
	category:"RS Utils"
	ButtonText:"Create Shadow Mesh"
(
	::CreateShadowMesh()
)

macroscript CreateCollBox
	category:"RS Utils"
	ButtonText:"Create Collision Box"
(
	::RsCreateCollisionGeometry collClass:#box
)

macroscript CreateCollBoxZRot
	category:"RS Utils"
	ButtonText:"Create Collision Box - Z-Rotated"
(
	::RsCreateCollisionGeometry collClass:#box rotZ:true
)

macroscript CreateCollBoxYZRot
	category:"RS Utils"
	ButtonText:"Create Collision Box - YZ-Rotated"
(
	::RsCreateCollisionGeometry collClass:#box rotYZ:true
)

macroscript CreateCollBoxXYZRot
	category:"RS Utils"
	ButtonText:"Create Collision Box - XYZ-Rotated"
(
	::RsCreateCollisionGeometry collClass:#box rotXYZ:true
)

-- Macros for creating XYZ-aligned collision-box with mover/weapon flags set:
macroscript CreateCollBoxXYZRot_Mover
	category:"RS Utils"
	ButtonText:"Create Collision Box - XYZ-Rotated, Mover"
(
	if (::RsCollEditRoll == undefined) or (not ::RsCollEditRoll.open) do 
	(
		filein (::RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Collision/coll_toolkit.ms")
	)
	RsCollEditRoll.lstCollTypes.selection = ::gRsCollTypes.moverListNum
	::RsCreateCollisionGeometry collClass:#box rotXYZ:true
)
macroscript CreateCollBoxXYZRot_Weapon
	category:"RS Utils"
	ButtonText:"Create Collision Box - XYZ-Rotated, Weapons"
(
	if (::RsCollEditRoll == undefined) or (not ::RsCollEditRoll.open) do 
	(
		filein (::RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Collision/coll_toolkit.ms")
	)
	RsCollEditRoll.lstCollTypes.selection = ::gRsCollTypes.weaponsListNum
	::RsCreateCollisionGeometry collClass:#box rotXYZ:true
)
macroscript CreateCollBoxXYZRot_MoverWeapon
	category:"RS Utils"
	ButtonText:"Create Collision Box - XYZ-Rotated, Mover + Weapons"
(
	if (::RsCollEditRoll == undefined) or (not ::RsCollEditRoll.open) do 
	(
		filein (::RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Collision/coll_toolkit.ms")
	)
	RsCollEditRoll.lstCollTypes.selection = ::gRsCollTypes.moverWeaponsListNum
	::RsCreateCollisionGeometry collClass:#box rotXYZ:true
)

macroscript CreateCollSphere
	category:"RS Utils"
	ButtonText:"Create Collision Sphere"
(
	::RsCreateCollisionGeometry collClass:#sphere
)

macroscript SelectLODChildren
	category:"RS Utils"
	ButtonText:"Select LOD Children"
(	
	::selectLODChildren()
)

macroscript SelectLODParent
	category:"RS Utils"
	ButtonText:"Select LOD Parent"
(	
	
	::selectLODParent()
)

macroscript SelectLODAllSet
	category:"RS Utils"
	ButtonText:"Select All LOD Set"
(	
	::selectAllLODChildren()
)

macroscript SelectXrefAllSet
	category:"RS Utils"
	ButtonText:"Select All Xref Set"
(	
	::selectAllXref()
)

macroscript SelectNormalAllSet
	category:"RS Utils"
	ButtonText:"Select All Normal Set"
(	
	::selectAllNormal()
)

macroscript SelectHorizonAllSet
	category:"RS Utils"
	ButtonText:"Select All Horizon Objects"
(	
	::selectAllHorizon()
)

macroscript SwitchSceneDXMats
	category:"RS Utils"
	ButtonText:"Switch Scene to DX Rage Materials"
(	
	::RsSetHardwareRendering true
)

macroscript SwitchSceneRageMats
	category:"RS Utils"
	ButtonText:"Switch Scene to Rage Materials"
(	
	::RsSetHardwareRendering false
)

macroscript SwitchStdMatsToRage
	category:"RS Utils"
	ButtonText:"Convert Std Mat to Rage Mat"
(	
	::RsConvertStdMaterialsOnSelected (selection as array)
)
-- Lighting
macroscript LightSceneOn
	category:"RS Utils"
	ButtonText:"On"
(
	::RsToggleDxLightingOnObjs (objects as array) true
)

macroscript LightSceneOff
	category:"RS Utils"
	ButtonText:"Off"
(
	::RsToggleDxLightingOnObjs (objects as array) false
)

macroscript LightObjOn
	category:"RS Utils"
	ButtonText:"On"
(
	::RsToggleDxLightingOnObjs (objects as array) true
)

macroscript LightObjOff
	category:"RS Utils"
	ButtonText:"Off"
(
	::RsToggleDxLightingOnObjs (objects as array) false
)

macroscript LightFaceOn
	category:"RS Utils"
	ButtonText:"On"
(
	::RsToggleDxLightingOnFaces true
)

macroscript LightFaceOff
	category:"RS Utils"
	ButtonText:"Off"
(
	::RsToggleDxLightingOnFaces false
)

-- Texture scaling
macroscript To100pc
	category:"RS Utils"
	ButtonText:"1.0"
(
	::RsScaleTexturesOnSelectedObjs 1.0 (selection as array)
)

macroscript To75pc
	category:"RS Utils"
	ButtonText:"0.75"
(
	::RsScaleTexturesOnSelectedObjs 0.75 (selection as array)
)

macroscript To50pc
	category:"RS Utils"
	ButtonText:"0.5"
(
	::RsScaleTexturesOnSelectedObjs 0.5 (selection as array)
)

macroscript To25pc
	category:"RS Utils"
	ButtonText:"0.25"
(
	::RsScaleTexturesOnSelectedObjs 0.25 (selection as array)
)

macroscript To12pc
	category:"RS Utils"
	ButtonText:"0.125"
(
	::RsScaleTexturesOnSelectedObjs 0.125 (selection as array)
)

macroscript To100pcScene
	category:"RS Utils"
	ButtonText:"1.0"
(
	::RsScaleTexturesOnSelectedObjs 1.0 (objects as array)
)

macroscript To75pcScene
	category:"RS Utils"
	ButtonText:"0.75"
(
	::RsScaleTexturesOnSelectedObjs 0.75 (objects as array)
)

macroscript To50pcScene
	category:"RS Utils"
	ButtonText:"0.5"
(
	::RsScaleTexturesOnSelectedObjs 0.5 (objects as array)
)

macroscript To25pcScene
	category:"RS Utils"
	ButtonText:"0.25"
(
	::RsScaleTexturesOnSelectedObjs 0.25 (objects as array)
)

macroscript To12pcScene
	category:"RS Utils"
	ButtonText:"0.125"
(
	::RsScaleTexturesOnSelectedObjs 0.125 (objects as array)
)

macroscript To100pcFace
	category:"RS Utils"
	ButtonText:"1.0"
(
	::RsScaleTexturesOnMaterialsForSelectedFaces 1.0
)

macroscript To75pcFace
	category:"RS Utils"
	ButtonText:"0.75"
(
	::RsScaleTexturesOnMaterialsForSelectedFaces 0.75
)

macroscript To50pcFace
	category:"RS Utils"
	ButtonText:"0.5"
(
	::RsScaleTexturesOnMaterialsForSelectedFaces 0.5
)

macroscript To25pcFace
	category:"RS Utils"
	ButtonText:"0.25"
(
	::RsScaleTexturesOnMaterialsForSelectedFaces 0.25
)
	
macroscript To12pcFace
	category:"RS Utils"
	ButtonText:"0.125"
(
	::RsScaleTexturesOnMaterialsForSelectedFaces 0.125
)

-- ///////////////////////////////////////////////////////////////////////////
-- called from script or not directly called...
-- ///////////////////////////////////////////////////////////////////////////

macroscript slice90
	category:"RS Modelling"
	ButtonText:"slice 90"
(	
	modPanel.addModToSelection (SliceModifier ()) ui:on
	$.modifiers[#Slice].slice_plane.rotation =(quat 1 0 1 0)
)

macroscript slice180
	category:"RS Modelling"
	ButtonText:"slice 180"
(	
	modPanel.addModToSelection (SliceModifier ()) ui:on
	$.modifiers[#Slice].slice_plane.rotation =(quat 1 0 0 1)
)

macroscript SetLODByNameAndPos
	category:"RS Lod"
	ButtonText:"Set LOD Parent By Name and Pos"
(	
	fileIn "pipeline/util/setlodbynameandpos.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Deprecated
-- ///////////////////////////////////////////////////////////////////////////

macroscript RsExportCarRec
	category:"RS Export"
	ButtonText:"Export Car Recordings"
(	
	filein "rockstar/export/caranim.ms";
)

macroscript SetCameraFromGame
	category:"RS Utils"
	ButtonText:"Set Camera From Game"
(
	filein "rockstar/helpers/camload.ms"	
)

macroscript ScriptDump
	category:"RS Utils"
	ButtonText:"Script Dump"
(
	filein "rockstar/helpers/scriptdump.ms"	
)

macroscript RsFragmentTuner
	category:"RS Utils"
	ButtonText:"Fragment tuner"
(
	filein "pipeline/util/fragtune.ms"
)

macroscript IgnoreMe
	category:"RS Utils"
	ButtonText:"Dont Add to IPL"
(
	::AddIgnoreSelected()
)

macroscript DontExport
	category:"RS Utils"
	ButtonText:"Dont Export Selected"
(	
	::SetDontExportSelected()
)

macroscript DontApplyRadiosity
	category:"RS Utils"
	ButtonText:"Dont Apply Radiosity"
(	
	::SetDontApplyRadiosity()
)

macroscript ParticleCat
	category:"RS Utils"
	ButtonText:"Particle Set"
(	
	filein "rockstar/helpers/particle_cat.ms"
)

macroscript ExplosionCat
	category:"RS Utils"
	ButtonText:"Explosion Set"
(	
	filein "rockstar/helpers/explosion_cat.ms"
)

macroScript BlockVisibilitySet
	category:"RS Organisational"
	tooltip:"Set the hide and freeze of objects by block"
	ButtonText:"Block Visibility Set"
(
	filein "rockstar/helpers/visblock.ms"
)

macroscript ShowNotes
	category:"RS Organisational"
	ButtonText:"Display Notes"
(	
	filein "gta/utils/GtaDisplayNotes.ms"
)

macroscript CheckPathNodes
	category:"RS Placement"
	ButtonText:"Check Path Nodes"
(	
	filein "rockstar/helpers/pathnodecheck.ms"
)

macroscript AlignObjectsInZ
	category:"RS Placement"
	ButtonText:"Align Objects in Z"
(	
	filein "gta/utils/GtaAlignObject.ms"
)

macroscript CreateInstancer
	category:"RS Placement"
	tooltip:"Select an object and wrap selected to this"
	ButtonText:"Instancer"
(	
	::createInstancer()
)

macroscript DatImporter
	category:"RS Placement"
	ButtonText:"DAT Importer"
(
	filein "rockstar/util/datimporter.ms"
)

macroScript sectorCalc
	category:"RS Placement"
	ButtonText:"World - Sector Calculator"
(
	filein "gta/utils/GtaWorldSector.ms"
)

macroScript BadObjPosChecker
	category:"RS Placement"
	ButtonText:"Bad Object Position Checker"
(
	filein "rockstar/util/badobjpos_finder.ms"
)

macroScript HandholdImporter
	category:"RS Placement"
	ButtonText:"Handhold Importer"
(
	filein "pipeline/helpers/climbing/handhold_importer.ms"
)

macroscript RenderProps
	category: "RS Interiors"
	tooltip:"Render images of all props in a file"
	ButtonText: "Prop Renderer"
(
	filein (::RsConfigGetWildwestDir() + "script/3dsmax/props/proprenderer.ms")
)

macroscript MiloFiddler
	category: "RS Interiors"
	ButtonText: "MILO Fiddler"
(
	filein "rockstar/helpers/milo.ms"
)

macroScript BoundRoomHelper
	category:"RS Collision"
	tooltip:"RexBound/Room Util"
	ButtonText:"RexBound/Room Util"
(
	filein "rockstar/helpers/boundroom.ms"
)

macroScript RsCollisionCategories
	category:"RS Collision"
	tooltip:"Collision Categories"
	ButtonText:"Collision Categories"
(
	filein "rockstar/helpers/collcat.ms"
)

macroScript RsSecondSurfacePainter
	category:"RS Collision"
	tooltip:"Second Surface Painter"
	ButtonText:"Second Surface Painter"
(
	filein "rockstar/helpers/secondsurface_painter.ms"
)

macroscript RepointXrefs
	category: "RS Xrefs"
	ButtonText: "Repoint Refs"
(
	filein "rockstar/util/repointxrefs.ms"
)

macroscript RenameXrefs
	category: "RS Xrefs"
	ButtonText: "Rename Refs"
(
	filein "rockstar/util/renamexrefs.ms"
)

macroscript InternalRefs
	category: "RS Xrefs"
	ButtonText: "Internal Refs"
(
	filein "rockstar/helpers/internalref.ms"
)

macroscript ResetPivots
	category:"RS Xrefs"
	ButtonText:"Reset Pivots for Xrefs"
(	
	filein "gta/utils/GtaPivotReset.ms"
)

macroscript ChangeXRef
	category:"RS Xrefs"
	ButtonText:"Change Xrefs"
(	
	filein "gta/utils/GtaChangeXref.ms"
)

macroscript DoFindXref
	category:"RS Xrefs"
	ButtonText:"Find XRef"
(
	filein "gta/utils/GtaFindXref.ms"
)

macroscript FixXREFs
	category:"RS Xrefs"
	ButtonText:"Fix Xrefs"
(
	filein "rockstar/util/UpdateXREF.ms"
)

macroscript ChoppyChop
	category:"RS Modelling"
	tooltip:"Slices landscape up"
	ButtonText:"Choppy chop"
(
	filein "gta/utils/Choppy_chop.ms"
)

macroscript SetPivotForBBox
	category:"RS Modelling"
	ButtonText:"Set Best Pivot For BBox"
(	
	filein "gta/utils/SetPivotForBBox.ms"
)

macroScript CompositeSpecSetup
	category:"RS Material"
	tooltip:"Composite Specular Setup"
	ButtonText:"Composite Specular Setup"
(
	filein "rockstar/helpers/specfixup.ms"
)

macroScript ShaderValueChange
	category:"RS Material"
	tooltip:"Replace Rs Shader Value"
	ButtonText:"Shader Attribute Changer"
(
	filein "rockstar/helpers/shaderchange.ms"
)

macroScript ShaderReplacer
	category:"RS Material"
	tooltip:"Replace Rs Shader with another"
	ButtonText:"Replace Rs Shader"
(
	filein "pipeline/ui/shaderreplace.ms"
)

macroScript StdMatReplacer
	category:"RS Material"
	tooltip:"Replace Standard Material with Rage Shader"
	ButtonText:"Replace StdMat with Rage Shader"
(
	filein "rockstar/helpers/stdmatreplace.ms"
)

macroScript RageShdrReplacer
	category:"RS Material"
	tooltip:"Replace Rage Shader with Standard Material"
	ButtonText:"Replace Rage Shader with StdMat"
(
	filein "rockstar/helpers/rageshadertostdmat.ms"
)

macroScript TextureTools
	category:"RS Material"
	tooltip:"Texture Tools"
	ButtonText:"Texture Tools"
(
	filein "gta/utils/texturetools.ms"
)

macroScript MaterialMapSet
	category:"RS Material"
	tooltip:"Material Map Set"
	ButtonText:"Material Map Set"
(
	filein "rockstar/util/matmapset.ms"
)

macroScript RsTexMapInfo
	category:"RS Material"
	tooltip:"Tex Map Info"
	ButtonText:"Tex Map Info"
(
	filein "rockstar/helpers/texmaps.ms"
)

macroscript ResetDiffuse
	category:"RS Material"
	tooltip:"Sets diffuse colour to grey"
	ButtonText:"Reset Diffuse"
(
	::ResetDiffuseToWhite()
)

macroscript CreateMultiSub
	category:"RS Material"
	tooltip:"Tidy your multisub"
	ButtonText:"Create New MultiSub"
(	
	fileIn "pipeline/helpers/materials/CreateMultiSub.ms"
	::RScreateNewMultiSub()
)

macroscript MultiSubCountForScene
	category:"RS Material"
	tooltip:"Multi Sub count"
	ButtonText:"Multi Sub count for scene"
(	
	filein "rockstar/helpers/materialutils.ms"
	
)

macroscript CreateMultiSubTxd
	category:"RS Material"
	ButtonText:"Create New MultiSubs for Txds"
(	
	fileIn "pipeline/helpers/materials/CreateMultiSub.ms"
	::RScreateNewMultiSubTxd()
)

macroscript CreateMultiSubTxdObject
	category:"RS Material"
	ButtonText:"Create New MultiSubs for Objects"
(	
	fileIn "pipeline/helpers/materials/CreateMultiSub.ms"
	::RScreateNewMultiSubObject()
)

macroscript SetBoundMaterials
	category:"RS Material"
	tooltip:"Set bound materials"
	ButtonText:"Bound Material Toggle"
(
	filein "rockstar/helpers/boundmaterialflagtoggle.ms"
)

macroscript GrassPainter
	category:"RS Material"
	tooltip:"Tools for editing grass and tree materials"
	ButtonText:"Grass Painter"
(
	filein "pipeline/helpers/materials/GrassPainter.ms"
)

macroscript ViewTXD
	category:"RS Material"
	ButtonText:"View TXD"
(
	filein "pipeline/helpers/materials/viewtxd.ms"
)

macroscript CheckDegenerates
	category:"RS Info"
	ButtonText:"Check For Degenerates"
(	
	filein "pipeline/util/CheckDegenShare.ms"	
)

macroscript TestMeshTopology
	category:"RS Info"
	ButtonText:"Test Mesh Topology"
(	
	filein "pipeline/util/TestMeshTopology.ms"
)

macroscript DoShowShaderStats
	category:"RS Info"
	ButtonText:"Shader Stats"
(
	ShellLaunch "http://greg-alienbrain/shader_stats.php" ""
)

macroscript PosPrint
	category:"RS Info"
	ButtonText:"Position/Rotation Print"
(
	filein "pipeline/util/posprint.ms"
)

macroscript SceneLodEditor
	category:"RS Lod"
	ButtonText:"Scene LOD Editor"
(	
	filein "pipeline/ui/lodeditor.ms"
)

macroscript DrawableLodEditor
	category:"RS Lod"
	ButtonText:"Drawable LOD Editor"
(
	filein "pipeline/ui/drawablelodeditor.ms"
)

macroscript ShowSceneLODView
	category:"RS Lod"
	ButtonText:"Scene LOD View"
(	
	ShowLODView()
)

macroscript ShowDrawableLODView
	category:"RS Lod"
	ButtonText:"Drawable LOD View"
(
	LodDrawable_Show()
)

macroscript ShowLodSphere
	category:"RS Lod"
	ButtonText:"Show LOD Spheres"
(
	filein "pipeline/util/lodscripthelpers.ms"
	RsLodCreateRangeObjs objClass:sphere
)

macroscript LodFiddler
	category:"RS Lod"
	ButtonText:"Lod Fiddler"
(	
	filein "gta/utils/lodfiddler.ms"
)

macroscript GuessLod
	category:"RS Lod"
	tooltip:"Guess LoD distances of selected objects"
	ButtonText:"Guess LoD dist"
(
	::GuessLodDist selection
)

macroscript TestLodName
	category:"RS Lod"
	ButtonText:"Test LoD name"
(
	::GtaTestForLodName()
)

macroscript AssignVisTrack
	category:"RS Lod"
	ButtonText:"Assign LOD Vis Track"
(	
	AssignVisControl()
)

macroscript AssignVisTrackSelected
	category:"RS Lod"
	ButtonText:"Assign LOD Vis Track Selected"
(	
	AssignVisControlSelected()
)

macroscript ClearVisTrack
	category:"RS Lod"
	ButtonText:"Clear Vis Track"
(	
	ClearVisControl()
)

macroscript LandLodder
	category:"RS Lod"
	ButtonText:"Captain Stu's Land Lodder Loving"
(
	filein "rockstar/helpers/TerrainLodder.ms"
)

macroscript LODVisController
	category:"RS Lod"
	ButtonText:"LOD Visibility Controller"
(	
	filein "pipeline/ui/lod_visibility_controller.ms"
)

macroscript VertAnglePainter
	category:"RS Material"
	ButtonText:"Vert Angle Painter"
(
	filein "rockstar/helpers/VertAnglePaint.ms"
)

macroscript ProxyTextureSwap
	category:"RS Material"
	ButtonText:"Proxy texture Swap"
(
	filein "pipeline/helpers/materials/proxytexturemgr.ms"
)

macroscript MultiAssignRefs
	category:"RS InternalRefs"
	ButtonText:"Multi Assign Refs"
(	
	filein "rockstar/helpers/ixrmultiassign.ms"
)

macroscript FixIxRefMaterials
	category:"RS InternalRefs"
	ButtonText:"Fix IRef Materials (force)"
(
	for obj in $objects do
	(
		if ( InternalRef != classof obj ) then
			continue
		ixref_setmaterial obj
	)
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs VFX
-- ///////////////////////////////////////////////////////////////////////////

macroscript ParticleConvert
	category:"RS VFX"
	ButtonText:"Particle Conversion Wizard"
(	
	filein "rockstar/helpers/VFXParticleConvert.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- Rs Vehicle
-- ///////////////////////////////////////////////////////////////////////////

macroScript ResetPivot
	category:"RS Vehicle"
	ButtonText:"Reset Pivot Transform and Scale"
(
	fileIn "pipeline/util/pivot_reset.ms"
	for o in $selection do 
	(
		::pivot_reset_transform o
	)
)

macroscript StrayVertCleaner
	category:"RS Vehicle"
	ButtonText:"Stray Vert Cleaner"
(	
	filein "pipeline/helpers/vehicles/stray_vert_cleaner.ms"
)

macroScript RsAnimExport
	category:"RS Export"
	tooltip:"Anim Export"
	ButtonText:"Anim Export"
(
	filein "rockstar/export/anim.ms"
)

macroscript RsExportZones
	category:"RS Export"
	ButtonText:"Export Zones"
(	
	filein "pipeline/export/ui/ExportZones.ms";
)

macroscript RsImportZones
	category:"RS Export"
	ButtonText:"Import Zones"
(	
	filein "pipeline/import/ui/ImportZones.ms";
)


-- ///////////////////////////////////////////////////////////////////////////
-- Debugging tools
-- ///////////////////////////////////////////////////////////////////////////

macroscript RsImportIndBoundsFromFile
	category: "RS Debug"
	ButtonText: "Import independent bounds"
(
	filein "pipeline/import/mapbounds_ind_import.ms"
	
	::RsDebugBNDImport()
)

macroscript RsTextSearch
	category: "RS Debug"
	ButtonText:"TextSearch"
	tooltip:"Text Search"
	Icon:#("ViewportNavigationControls",1)
(
	filein "pipeline/util/RS_TextSearch.ms"
)

macroscript RsDebugOverlay
	category: "RS Debug"
	ButtonText:"DebugOverlay"
	tooltip:"Mesh Debug Overlay"
	Icon:#("Max_Edit_Modifiers",8)
(
	filein "pipeline/util/RS_DebugOverlay.ms"
)

macroscript RsAttributeEditor
	category: "RS Debug"
	ButtonText:"Attribute Editor"
	tooltip:"Non modal dialog for attribute editing"
(
	filein "pipeline/ui/RsAttributeEditor.ms"
)

macroscript RsSceneLinkEditor
	category: "RS Debug"
	ButtonText:"RsScenelink Editor"
	tooltip:"Dialog for generically editing scene links made with the RsSceneLink plugin"
(
	filein "pipeline/ui/RsSceneLinkEditor.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- RS Source Control
-- ///////////////////////////////////////////////////////////////////////////

macroscript RsPerforceTools
	category:"RS Source Control"
	tooltip:"Perforce Tools"
	ButtonText:"Perforce Tools"
(	
	filein "pipeline/ui/p4_ui.ms"
)

macroscript RsPerforceGetLatestContainers
	category:"RS Source Control"
	ButtonText:"Get latest on selected containers"
(
	fileIn "pipeline/util/p4_containers.ms"
	::gRsP4Container.getLatestFor $
)
					
macroscript RsPerforceCheckoutContainers
	category:"RS Source Control"
	ButtonText:"Checkout selected containers"
(
	fileIn "pipeline/util/p4_containers.ms"
	::gRsP4Container.checkOutFor $
)
					
macroscript RsPerforceCheckinContainers
	category:"RS Source Control"
	ButtonText:"Checkin selected containers"
(
	fileIn "pipeline/util/p4_containers.ms"
	::gRsP4Container.checkInFor $
)

macroscript RsP4syncSceneTexturesMacro
	category:"RS Source Control"
	ButtonText:"Sync scene textures"
(
	::RsP4syncSceneTextures()
)
macroscript RsSearchForTexRef
	category:"RS Debug"
	ButtonText:"Find Texture/material in scene"
(
	fileIn "pipeline/helpers/materials/searchTexRefInScene.ms"
	::SearchTexInScene()
)

-- ///////////////////////////////////////////////////////////////////////////
-- RS Animation
-- ///////////////////////////////////////////////////////////////////////////

macroscript RsAnimationToolsImporter
	category:"RS Animation"
	tooltip:"Animation Importer"
	ButtonText:"Animation Importer"
(	
	filein "pipeline/ui/animation_ui.ms"
)

macroscript RsAnimationToolsExporter
	category:"RS Animation"
	tooltip:"Prop Animation Exporter"
	ButtonText:"Prop Animation Exporter"
(	
	filein "pipeline/export/ui/prop_exporter_ui.ms"
)

macroscript RsBoneTagEditor
	category:"RS Animation"
	tooltip:"Bone Tag Overrides"
	ButtonText:"Bone Tag Overrides"
(
	try(DestroyDialog ::RsBoneTagRoll) catch()
	CreateDialog ::RsBoneTagRoll 400 200
)


-- ///////////////////////////////////////////////////////////////////////////
-- RS Terrain
-- ///////////////////////////////////////////////////////////////////////////

macroscript RsTerrainProcessing
	category:"RS Terrain"
	tooltip:"Terrain Processing"
	ButtonText:"Terrain Processing Tools"
(	
	filein "pipeline/helpers/terrain/terrainprocessing_ui.ms"
)

macroscript RsTextureTileGenerator
	category:"RS Terrain"
	tooltip:"Texture Tile Generator"
	ButtonText:"Texture Tile Generator"
(	
	filein "pipeline/helpers/terrain/RsTextureTileGenerator_ui.ms"
)

-- ///////////////////////////////////////////////////////////////////////////
-- RS Audio
-- ///////////////////////////////////////////////////////////////////////////

macroscript RsAudioTools
	category:"RS Audio"
	tooltip:"Audio Tool"
	ButtonText:"Audio Emitter Set"
(	
	filein "pipeline/ui/audioemitter_ui.ms"
)

-- MAX FIX -- Redefine Max macroscript to fix bugs --
-- ObjectPaintPaint: Inverts negatively-scaled objects after Object Paint is used:
Macroscript ObjectPaintPaint
category: "PolyTools"
internalCategory: "PolyTools"
tooltip: "ObjectPaint Paint"
autoUndoEnabled:false
(
	on isEnabled return
	(
		not (OBJPaintGetSetting 51)
	)
	on isChecked do
	(
		OBJPaintGetSetting 50
	)
	on execute do 
	(
		local objsBefore = objects as array
		
		local doStartTool = not (OBJPaintGetSetting 50)
		PolyBoost.ToolToggle 5 1 doStartTool
		
		for obj in objects where ((findItem objsBefore obj) == 0) and (obj.scale.x < 0) do 
		(
			obj.scale = [1,1,1]
			in coordsys local rotate obj (eulerangles 0 180 0)
		)
	)
)
