macroScript CreateMover
	category:"Biped Tools"
	toolTip:"Creates a mover"
	icon:#("Helpers", 1)
(
	
	fn CreateDummyAndMover =
	(
			--Get selected Biped part
			object = selection[1]
			
			--get COM name
			bipname = biped.getNode object 14
			
			--Delete any existing Dummy or Mover Nodes
			moverNode = getnodebyname "mover" exact:true
			if moverNode != undefined then delete moverNode
			dummyNode = getnodebyname "Dummy01" exact:true
			if dummyNode != undefined then delete dummyNode
			axisNode = getnodebyname "axis_helper" exact:true
			if axisNode != undefined then delete axisNode
			layer = layermanager.getlayerfromname "Mover Node"
			if layer != undefined then layermanager.deleteLayerByName "Mover Node"
			
			--move slider to start
			slidertime = animationrange.start
			
			--get root bone z height
			bipname.transform.controller.figureMode = true
			z_height = bipname.transform[4].z
			bipname.transform.controller.figureMode = false
			
			--create Dummy node
			newDummyNode = Dummy()
			newDummyNode.name = "Dummy01"
			newDummyNode.boxsize = [.2,.2,.2]
			newDummyNode.pos = [0.0,0.0,z_height]
			
			--parent biped to it
			bipname.parent = newDummyNode
			
			--create Mover Node
			newMoverNode = Dummy()
			newMoverNode.name = "mover"
			newMoverNode.boxsize = [.3,.3,.3]
			newMoverNode.pos = [0.0,0.0,z_height]
			
			--create axis helper node
			axis_helper = Point pos:[0,0,1] isSelected:off cross:off axistripod:on size:0.25 name:"axis_helper"
			setTransformLockFlags axis_helper #all
			
			--parent nodes correctly
			newMoverNode.parent = newDummyNode
			axis_helper.parent = newMoverNode
			
			--create a new layer for Dummy and Mover nodes 
			layer = layermanager.newlayerfromname "Mover Node"
			layer.addnode axis_helper
			layer.addnode newMoverNode 
			layer.addnode newDummyNode
	)
	
	fn UpdateMover xp yp zp yaw pitch roll = 
	(
		--check selection
		if (($mover != undefined) and ($mover.name == "mover")) then
		(	
			select $mover
		
			dummyNode = selection[1].parent
			
			biproot=$char.controller
			
			for child in dummyNode.children do
			(
				if classof child == biped_object then biproot = child
			)

			biproot.transform.controller.figuremode = true
			figureModeBipRot = biproot.transform as eulerAngles
			figureModeBipPos = biproot.transform[4]
			biproot.transform.controller.figuremode = false

			--delete all the keys on the mover track
			deleteKeys $.controller #allKeys
			
			dividorLimit = (ceil (((animationrange.end.frame - animationrange.start.frame) as float) / 140)) as integer
						
			with animate on
			(
				dividorCount = 0
				for i = animationrange.start to animationrange.end do
				(
					
					if dividorCount == 0 then (
						
						slidertime = i

						--get position and rotation
						bipPos = biped.getTransform biproot #pos
						bipRot = (biped.getTransform biproot #rotation) as eulerAngles

						--translate mover node
						if (xp ==true) then
						(		
							$mover.pos.x = bipPos.x
						)
						else
						(
							$mover.pos.x =0.0
						)

						if (yp == true) then
						(		
							$mover.pos.y = bipPos.y
						)
						else
						(
							$mover.pos.y =0.0
						)

						if (zp == true) then
						(		
							$mover.pos.z = bipPos.z
						)
						else
						(
							$mover.pos.z = figureModeBipPos.z
						)

						-- rotate mover node
						if (pitch == true) then
						(		
							$mover.rotation.x_rotation = 0.0
						)
						else
						(
							$mover.rotation.x_rotation=0.0
						)

						if (roll == true) then
						(		
							$mover.rotation.y_rotation = 0.0
						)
						else 
						(
							$mover.rotation.y_rotation = 0.0
						)

						if (yaw == true) then
						(		
							$mover.rotation.z_rotation = bipRot.z - figureModeBipRot.z
						)
						else
						(
							$mover.rotation.z_rotation = 0.0
						)
					)
					dividorCount = dividorCount + 1
					if dividorCount == dividorLimit then dividorCount = 0
				)
			)
		)
		else
		(
			messagebox "Invalid mover"
		)
	)
		
	if ((CreateMover != undefined) and (CreateMover.isdisplayed)) then
	(
		(destroyDialog CreateMover)
	)

	rollout CreateMover "Create Mover" width:150 height:200
	(
		groupBox grpDynamic "" pos:[19,31] width:161 height:283
		label lbl1 "Copy the following from the root track :" pos:[26,43] width:144 height:32

		groupBox grpPos "Position" pos:[27,83] width:144 height:72	
		checkbox chkX "X" pos:[39,98] width:116 height:15 enabled:true checked:true toolTip:"default is 0.0"
		checkbox chkY "Y" pos:[39,116] width:116 height:15 enabled:true checked:true toolTip:"default is 0.0"
		checkbox chkZ "Z" pos:[39,134] width:116 height:15 enabled:true checked:false toolTip:"default is figuremode Z"

		groupBox grpRot "Rotation" pos:[28,162] width:144 height:72
		checkbox chkYaw "Yaw" pos:[39,178] width:116 height:15 enabled:true checked:true toolTip:"default is yaw - figuremode yaw"
		checkbox chkPitch "Pitch" pos:[39,197] width:116 height:15 enabled:false checked:false toolTip:"default is 0.0"
		checkbox chkRoll "Roll" pos:[39,215] width:116 height:15 enabled:false checked:false toolTip:"default is 0.0"

		button btnCreateDynamicMover "Create mover" pos:[28,246] width:144 height:48 toolTip:"Create the mover"

		on btnCreateDynamicMover pressed do
		(	
			--check selection
			if (selection.count == 0) or ((classof selection[1])!= biped_object) then 
			(
				messagebox "Select Biped to add Dummy and Mover nodes to"
			)
			else 
			(					
				-- disable the redraw
				disableSceneRedraw()
				CreateDummyAndMover()			
				UpdateMover chkX.checked chkY.checked chkZ.checked chkYaw.checked chkPitch.checked chkRoll.checked
				enableSceneRedraw()		
			)
		)
	)

		
	createDialog CreateMover 215 430
)