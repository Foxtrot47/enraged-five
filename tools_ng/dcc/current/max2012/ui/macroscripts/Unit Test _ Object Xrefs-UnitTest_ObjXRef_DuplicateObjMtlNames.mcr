macroScript UnitTest_ObjXRef_DuplicateObjMtlNames
	ButtonText:"UnitTest_ObjXRef_DuplicateObjMtlNames"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Test - Object Xrefs" 
	Tooltip:"Unit Test - Obj Xref - DuplicateObjAndMtlNames" 
(	
-- Object XRef helper methods defined elsewhere
global createTestSuite = undefined
global ts = undefined

--------------------------------------------------------------------------------
-- Local Utility methods
--------------------------------------------------------------------------------
-- The path and name of the scene that will get xrefed
global gSourceFileName = ((getDir #temp) + "\\ObjXref.DuplicateNames.Test")
global gDupMtlName = "AAA"
global gDupObjName = "Object01"

-- This creates and saves the source scene
function createSourceScene =
(
  resetMaxFile #noprompt
  -- Create material
  meditMaterials[1].diffuseMap = Checker()
  meditMaterials[1].name = gDupMtlName
  
  -- Create objects 
  l_obj = Box length:20 width:20 height:40 mapcoords:on realWorldMapSize:on pos:[20, 20, 0]
  l_obj.name = gDupObjName
  l_obj.material = meditMaterials[1]
  l_obj = Box length:10 width:10 height:40 mapcoords:on realWorldMapSize:on pos:[-20, 20, 0]
  l_obj.name = gDupObjName
  l_obj.material = meditMaterials[1]

	-- Set dupObj and dupMtl name actions
  objXRefMgr.dupObjNameAction=#xref
  objXRefMgr.dupMtlNameAction=#useXRefed

  -- Save file
  local res = oxUtil.saveTmpFile (gSourceFileName + ".max")
  ts.assert (true == res) "Save Temp File"
)

function createSimpleXref = 
(
  xRec = objXRefMgr.AddXRefItemsFromFile (gSourceFileName + ".max") 
  
  return xRec
)

fn createNestedXref = 
(
  createSimpleXref()

  local l_MasterFileName = (gSourceFileName + ".XRef" + ".max")
  saveMAXFile l_MasterFileName
  
  resetMaxFile #noprompt
  xRec = objXRefMgr.AddXRefItemsFromFile l_MasterFileName 
  
  return xRec
)

--------------------------------------------
-- Unit Test methods
--------------------------------------------	
-- Unit test fixture setup
fn setupUT =
(
  resetMaxFile #noprompt

  -- Create Sphere 
  l_obj = Sphere radius:40 mapcoords:on realWorldMapSize:on pos:[-20, -20, 0]
  l_obj.name = gDupObjName
  -- red material
  meditMaterials[1].Diffuse = color 225 81 37
  meditMaterials[1].ambient = color 225 81 37

  -- Create Sphere 
  l_obj = Sphere radius:40 mapcoords:on realWorldMapSize:on pos:[20, -20, 0]
  l_obj.name = gDupObjName
  meditMaterials[1].name = gDupMtlName
  l_obj = Sphere radius:20 mapcoords:on realWorldMapSize:on pos:[-20, -20, 0]
  l_obj.name = gDupObjName
  meditMaterials[1].name = gDupMtlName
  
  --l_obj.material = meditMaterials[1]
)

-- Unit test fixture teardown
fn teardownUT =
(
	-- resetMaxFile #noprompt
)

-------------------------------------------
fn testDupObjNameSimpleXRef_Xref =
(
  local prevObjCount = objects.count
  
  print "test - Simple xrefing src file - #xref"
  objXRefMgr.dupObjNameAction = #xref
  local xRec = objXRefMgr.AddXRefItemsFromFile (gSourceFileName + ".max") 
  ts.assert (undefined != xRec) "Check xref created"
  for o in objects do
  (
  	ts.assert (o.name == gDupObjName) ("Check obj name = " + gDupObjName)
  )
  
)
--------------------------------------------	
function testDupObjNameSimpleXRef_skip =
(
  local prevObjCount = objects.count
  
  print "test - Simple xrefing src file - #skip"
  objXRefMgr.dupObjNameAction = #skip
  local xRec = objXRefMgr.AddXRefItemsFromFile (gSourceFileName + ".max") 
  ts.assert (undefined == xRec) "Check objects were not xrefed"
)

--------------------------------------------	
fn testDupObjNameSimpleXRef_deleteOld =
(
  local l_objects = (objects as array)
  
  print "test - Simple xrefing src file - #deleteOld"
  objXRefMgr.dupObjNameAction = #deleteOld
  local xRec = objXRefMgr.AddXRefItemsFromFile (gSourceFileName + ".max") 
	ts.assert (undefined != xRec) "Check xref created"
	
	local l_NotFoundDeletedNode = true
  for o in l_objects while l_NotFoundDeletedNode do
  (
		if (isValidNode(o) == false) do l_NotFoundDeletedNode = false
  )
  ts.assert (l_NotFoundDeletedNode) "Duplicated xrefed node was deleted"
)

--------------------------------------------	
fn testDupObjNameSimpleXRef_autoRename =
(
  print "test - Simple xrefing src file - #autoRename"
  objXRefMgr.dupObjNameAction = #autoRename
  local xRec = objXRefMgr.AddXRefItemsFromFile (gSourceFileName + ".max") 
	ts.assert (undefined != xRec) "Check xref created"

  local l_nameCount = 0
  for o in objects do
  (
		if (o.name == gDupObjName) do l_nameCount = l_nameCount + 1
  )
  ts.assert (1 == l_nameCount) "Duplicated xrefed node was renamed"
)


----------------------------------------------------------
fn testDupMtlNameSimpleXRef_useXRefed =
(
  print "test - Simple xrefing src file - #useXRefed"
  for o in objects do o.material = meditMaterials[1]
  objXRefMgr.dupMtlNameAction = #useXRefed

  local xRec = objXRefMgr.AddXRefItemsFromFile (gSourceFileName + ".max") 
	ts.assert (undefined != xRec) "Check xref created"

  local l_objectsUsingXRefMtl = 0
  for o in objects do
  (
    xm = getinterface o.material #IXrefMaterial
    if (undefined != xm) do l_objectsUsingXRefMtl = l_objectsUsingXRefMtl + 1
  )
  ts.assert (l_objectsUsingXRefMtl == objects.count) "xref material used on all objects"
)

----------------------------------------------------------
fn testDupMtlNameSimpleXRef_useScene =
(
  print "test - Simple xrefing src file - #useScene"
  for o in objects do o.material = meditMaterials[1]

  objXRefMgr.dupMtlNameAction = #useScene
  local xRec = objXRefMgr.AddXRefItemsFromFile (gSourceFileName + ".max") 
	ts.assert (undefined != xRec) "Check xref created"

  local l_objectsUsingXRefMtl = objects.count
  for o in objects do
  (
    xm = getinterface o.material #IXrefMaterial
    if (undefined == xm) do l_objectsUsingXRefMtl = l_objectsUsingXRefMtl - 1
  )
  ts.assert (0 == l_objectsUsingXRefMtl) "xref material not used on any object"
)

--------------------------------------------	
fn testDupMtlNameSimpleXRef_autoRename =
(
  print "test - Simple xrefing src file - #autoRename"
  local l_objectsUsingNonXRefMtlBefore = 0
  for o in objects do 
  (
    o.material = meditMaterials[1]
		l_objectsUsingNonXRefMtlBefore = l_objectsUsingNonXRefMtlBefore + 1
  )
  objXRefMgr.dupMtlNameAction = #autoRename

  local xRec = objXRefMgr.AddXRefItemsFromFile (gSourceFileName + ".max") 
	ts.assert (undefined != xRec) "Check xref created"

  l_objectsUsingNonXRefMtlAfter = 0
  for o in objects do
  (
    local xm = getinterface o.material #IXrefMaterial
    if (undefined == xm) do l_objectsUsingNonXRefMtlAfter = l_objectsUsingNonXRefMtlAfter + 1
  )
  ts.assert (l_objectsUsingNonXRefMtlBefore == l_objectsUsingNonXRefMtlAfter) "same number of non-xref materials used"
)

----------------------------------------------------------
fn testDupObjNameNestedXRef =
(
  print "test - Create nested xref"
  xRecPar = createNestedXRef()
)

----------------------------------------------------------
fn testDupMtlNameNestedXRef =
(
  print "test - Create nested xref"
  xRecPar = createNestedXRef()
)

--------------------------------------------
-- Macro methods
--------------------------------------------
On isEnabled return true
On isVisible return true

On Execute Do	
(
	-- Create instance of xref utility "object"
  if undefined == createObjXRefUtil do (
  	fileIn ((getdir #scripts) + "/startup/MxsUnitTest/ObjXRef/OBjXRef.Util.ms")
  )
	oxUtil = createObjXRefUtil()
	
  if undefined == createTestSuite do (
  	fileIn ((getdir #scripts) + "/startup/MxsUnitTest/MxsUnitTestFramework.ms")
  )
  if undefined == createTestSuite do (
  	fileIn ("MxsUnitTestFramework.ms")
  )

  -- create a test suite that outputs cmmands into a log file and messages into a new maxscript window (default behaviour)
  -- local logFileName = ((getdir #maxroot) + "DumpObjXRefMgr.log")
  -- global xrefDumpWnd = newScript()
	ts = createTestSuite "ObjXRef.TraverseHierarcy" setupUT teardownUT -- cmdLog:undefined msgLog:xrefDumpWnd
	ts.addTest testDupObjNameSimpleXRef_Xref
	ts.addTest testDupObjNameSimpleXRef_skip
	ts.addTest testDupObjNameSimpleXRef_deleteOld
	ts.addTest testDupObjNameSimpleXRef_autoRename
	ts.addTest testDupMtlNameSimpleXRef_useXRefed
	ts.addTest testDupMtlNameSimpleXRef_useScene
	ts.addTest testDupMtlNameSimpleXRef_autoRename
	
  createSourceScene()
  ts.run()
)

) -- END MACRO
