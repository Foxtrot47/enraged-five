macroScript UnitTest_ObjXRef_RemapSourceItem
	ButtonText:"UnitTest_ObjXRef_RemapSourceItem"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Test - Object Xrefs" 
	Tooltip:"Unit Test - Obj Xrefs - RemapSourceItem" 
(	
--------------------------------------------------------------------------------
-- Globals to this macro
--------------------------------------------------------------------------------
gSrcFile = (getdir #Scene) + "\\UT.ObjXref.RemapSourceItem.Source.max"	
gMasterFile_IncludeAll_ON = (getdir #Scene) + "\\UT.ObjXref.RemapSourceItem.Master.IncludeAll_ON.max"	
gMasterFile_IncludeAll_OFF = (getdir #Scene) + "\\UT.ObjXref.RemapSourceItem.Master.IncludeAll_OFF.max"	

-- Note: although the original intention was to be able to extend the range of 
-- object types to be tested by adding elements to these arrays, this concept 
-- does not scale well for controllers and setting up dependecies betweeb objects
-- and then testing that these dependencies were maintained over various xref scenarious.

-- The source object types and names are used to create them and test the validity of xrefs
global gSrcObjNames = #("Obj01", "Obj02")
global gSrcObjTypes = #(sphere, box)
global gSrcNewObjNames = #("Apple01", "Apple02")
global gSrcNewObjTypes = #(teapot, teapot)

-- The source material types and names are used to create them and test the validity of xrefs
-- Note that not all object types can be assigned a material (ex manipulators, helpers, etc)
global gSrcMtlNames = #("Mtl01", "Mtl02")
global gSrcMtlTypes = #(StandardMaterial, compositematerial)
global gSrcNewMtlNames = #("Montreal01", "Montreal02")
global gSrcNewMtlTypes = #(TopBottom, TopBottom)
	
-- The source controller types and names are used to create them and test the validity of xrefs
-- Note that only a few controller types are directly creatable and assignable to tm's of objects\nodes
global gSrcCtrlTypes = #(prs, link_constraint)
global gSrcNewCtrlTypes = #(prs, link_constraint)
	
-- The state of the xref items to be used when testing the results of tests
global gXrefItemResolvedState = #(true, true) 
global gXrefItemUnResolvedState = #(false, false) 
	
----------------------------------------------------
-- Utility methods
----------------------------------------------------
-- Creates a series of objects with materials and controllers
function doCreateSrcObjects aSrcObjTypes aSrcObjNames aSrcMtlTypes aSrcMtlNames aSrcCtrlTypes = 
(
	for i=1 to gSrcObjTypes.count do 
	(
		local obj = aSrcObjTypes[i] name:aSrcObjNames[i] pos:[0, 0, i*10]
		if (aSrcMtlTypes.count) >= i do (
			obj.material = (aSrcMtlTypes[i] name:aSrcMtlNames[i])
		)
		if (aSrcCtrlTypes.count >= i) do (
			obj.transform.controller = (aSrcCtrlTypes[i])()
		)
		-- Set up a dependence between two source objects
		if ((classof obj.transform.controller) == link_constraint and i >= 2) do 
		(
			obj.transform.controller.addTarget objects[i-1] 0
		)
	)
)

-- Tests expected resolve state and type of xref items passed as parameter
function doCheckXrefItems aXrefItems aSrcTypes aXrefItemResolvedState = 
(
	for i = 1 to aXrefItems.count do 
	(
		local xItem = GetInterface aXrefItems[i] "IXrefItem"
		ts.assert (aXrefItemResolvedState[i] != xItem.unresolved) ("Check " + (xItem.srcItemName) + " resolve state")
		if (true == aXrefItemResolvedState[i]) do
		(
			local srcItem = xItem.GetSrcItem()
			ts.assert ((classof srcItem) == aSrcTypes[i]) ("Check " + (xItem.srcItemName) + "'s type; got " + ((classof srcItem) as string) + ", expected " + (aSrcTypes[i] as string))
			
			-- check dependency between objects built via link constraint
			if (link_constraint == (classof srcItem)) do 
			(
				ts.assert (1 == srcItem.getNumTargets()) "Link Constraint Target Count"
				ts.assert ((srcItem.getNode 1) != undefined) "Link Constraint Target Resolved"
			)
		)
	)
)

-- Tests validity of all xref records in the objXrefMgr
function doCheckXrefRecords aSrcObjTypes aSrcMtlTypes aSrcCtrlTypes aXrefItemResolvedState = 
(
	ts.assert (objXrefMgr.recordCount > 0) "Non-zero xref record count"
	
	-- test all xref records
	for r = 1 to objXrefMgr.recordCount do
	(
		local xRec = objXrefMgr.GetRecord(r)
		ts.assert (undefined != xRec) ((r as string) + "'th Xref Record exists")
		
		-- test xref objects: resolve state and type
		local xItems = #()
		xRec.GetItems #XRefObjectType &xItems
		ts.assert (aSrcObjTypes.count == xItems.count) "xref objects count == source object count"
		doCheckXrefItems xItems aSrcObjTypes aXrefItemResolvedState	

		-- test xref materials: resolve state and type
		xItems = #()
		xRec.GetItems #XRefMaterialType &xItems
		ts.assert (aSrcMtlTypes.count == xItems.count) "xref mtl count == source mtl count"
		doCheckXrefItems xItems aSrcMtlTypes aXrefItemResolvedState			
		
		-- test xref controllers: resolve state and type
		xItems = #()
		xRec.GetItems #XRefControllerType &xItems
		ts.assert (aSrcCtrlTypes.count == xItems.count) "xref ctrl count == source ctrl count"
		doCheckXrefItems xItems aSrcCtrlTypes aXrefItemResolvedState		
	) -- end for each xref record
)

function doTestRemapXrefs aSrcFile aMasterFile =
(
	local res = loadMaxFile aMasterFile useFileUnits:true quiet:true
	ts.assert (true == res) "Master file loaded"
		
	-- test objects
	doCheckXrefRecords gSrcObjTypes gSrcMtlTypes gSrcCtrlTypes gXrefItemResolvedState
)

-- Tests remapping xrefs: rename one of the source objects
function doTestRemapXrefsRenameSourceNode aSrcFile aMasterFile =
(
	-- Load src file to be modified
	local res = loadMaxFile aSrcFile useFileUnits:true quiet:true
	ts.assert (true == res) "Source file loaded"
	ts.assert (objects.count != 0) "Source file non-empty"
	
	-- Modify src file (rename first source object) and save it
	for i=1 to gSrcNewObjNames.count do (
		objects[i].name = gSrcNewObjNames[i]
		if (objects[i].material != undefined and gSrcNewMtlNames.count >= i) do (
			objects[i].material.name = gSrcNewMtlNames[i]
		)
	)
	
	res = saveMaxFile aSrcFile  quiet:true
	ts.assert (true == res) ("Save modified src file: " + aSrcFile)

	-- Re-open master file and test results
	res = loadMaxFile aMasterFile useFileUnits:true quiet:true
	ts.assert (true == res) "Master file loaded"
	doCheckXrefRecords gSrcObjTypes gSrcMtlTypes gSrcCtrlTypes gXrefItemResolvedState
)

-- Tests remapping xrefs: delete and recreate source objects
function doTestRemapXrefsRecreateSourceNode aSrcFile aMasterFile =
(
	-- Load src file to be modified
	local res = loadMaxFile aSrcFile useFileUnits:true quiet:true
	ts.assert (true == res) "Source file loaded"
	ts.assert (objects.count != 0) "Source file non-empty"

	-- Modify src file (delete and re-create the source objects) and save it
	-- Make src objects of different types than they were originally. 
	-- Simply creating a new source scene from scratch would not work 
	-- because it may result in the objects having the same node handle 
	-- as in the original source file.
	delete objects
	doCreateSrcObjects gSrcNewObjTypes gSrcObjNames gSrcNewMtlTypes gSrcMtlNames gSrcNewCtrlTypes
	res = saveMaxFile aSrcFile  quiet:true
	ts.assert (true == res) ("Save modified src file: " + aSrcFile)

	-- Re-open master file and test
	res = loadMaxFile aMasterFile useFileUnits:true quiet:true
	ts.assert (true == res) "Master file loaded"
	doCheckXrefRecords gSrcNewObjTypes gSrcNewMtlTypes gSrcNewCtrlTypes gXrefItemResolvedState
)

-- Tests remapping xrefs: delete one of the source obj 
-- the corresponding xref should become unresolved
function doTestRemapXrefsDeleteSourceNode aSrcFile aMasterFile =
(
	-- Load src file to be modified
	local res = loadMaxFile aSrcFile useFileUnits:true quiet:true
	ts.assert (true == res) "Source file loaded"
	ts.assert (objects.count != 0) "Source file non-empty"

	-- Modify src file (delete one object) and save it
	delete objects
	res = saveMaxFile aSrcFile  quiet:true
	ts.assert (true == res) ("Save modified src file: " + aSrcFile)

	-- Re-open master file and test
	res = loadMaxFile aMasterFile useFileUnits:true quiet:true
	ts.assert (true == res) "Master file loaded"
	doCheckXrefRecords gSrcObjTypes gSrcMtlTypes gSrcCtrlTypes gXrefItemUnResolvedState
)

function doCreateMasterFile_IncludeAll_ON = 
(
	-- Xref all obj, ctrl and mtl from the source file 1
	objXrefMgr.mergeTransforms = false
	objXrefMgr.mergeMaterials = false
	objXrefMgr.mergeManipulators = false

	-- create master file with IncludeAll = ON, 
	local xRec = objXrefMgr.AddXRefItemsFromFile gSrcFile 
	ts.assert (undefined != xRec) "Xref created"
	ts.assert (true == xRec.includeAll) "IncludeAll = ON"
		
	-- Save out master file
	local res = saveMaxFile gMasterFile_IncludeAll_ON quiet:true
	ts.assert (true == res) ("Save Master File: " + gMasterFile_IncludeAll_ON)
)

function doCreateMasterFile_IncludeAll_OFF = 
(
	-- Xref all obj, ctrl and mtl from the source file 1
	objXrefMgr.mergeTransforms = false
	objXrefMgr.mergeMaterials = false
	objXrefMgr.mergeManipulators = false

	local xRec = objXrefMgr.AddXRefItemsFromFile gSrcFile objNames:gSrcObjNames
	ts.assert (undefined != xRec) "Xref created"
	ts.assert (false == xRec.includeAll) "IncludeAll = OFF"
	-- Save out master file
	local res = saveMaxFile gMasterFile_IncludeAll_OFF quiet:true
	ts.assert (true == res) ("Save Master File: " + gMasterFile_IncludeAll_OFF)
)

--------------------------------------------
-- Unit Test methods
--------------------------------------------	

-- Unit test fixture setup
function setupUT =
(
	resetMaxFile #noprompt
	
	-- Create a source file. We will be modifying this file for testing 
	-- the various scenarious for remapping xrefs to their source items
	doCreateSrcObjects gSrcObjTypes gSrcObjNames gSrcMtlTypes gSrcMtlNames gSrcCtrlTypes
	-- Save out source file
	local res = saveMaxFile gSrcFile quiet:true
	ts.assert (true == res) ("SetupUT: Save Src File: " + gSrcFile)
	resetMaxFile #noprompt

	-- create master file with IncludeAll = ON, 
	doCreateMasterFile_IncludeAll_ON()
	resetMaxFile #noprompt
	
	-- create master file with IncludeAll = OFF
	doCreateMasterFile_IncludeAll_OFF()
	resetMaxFile #noprompt
)

-- Unit test fixture teardown
function teardownUT =
(
	-- no scene tear down required
)

function testRemapXrefs_IncludeAll_ON =
(
	doTestRemapXRefs gSrcFile gMasterFile_IncludeAll_ON
)

-- Tests remapping xrefs: rename one of the source objects
function testRemapXrefsRenameSourceNode_IncludeAll_ON =
(
	doTestRemapXrefsRenameSourceNode gSrcFile gMasterFile_IncludeAll_ON
)

-- Tests remapping xrefs: delete and recreate source objects
function testRemapXrefsRecreateSourceNode_IncludeAll_ON =
(
	doTestRemapXrefsRecreateSourceNode gSrcFile gMasterFile_IncludeAll_ON
)

-- Tests remapping xrefs: delete one of the source obj 
-- the corresponding xref should become unresolved
function testRemapXrefsDeleteSourceNode_IncludeAll_ON =
(
	doTestRemapXrefsDeleteSourceNode gSrcFile gMasterFile_IncludeAll_ON
)

function testRemapXrefs_IncludeAll_OFF =
(
	doTestRemapXRefs gSrcFile gMasterFile_IncludeAll_OFF
)

-- Tests remapping xrefs: rename one of the source objects
function testRemapXrefsRenameSourceNode_IncludeAll_OFF =
(
	doTestRemapXrefsRenameSourceNode gSrcFile gMasterFile_IncludeAll_OFF
)

-- Tests remapping xrefs: delete and recreate source objects
function testRemapXrefsRecreateSourceNode_IncludeAll_OFF =
(
	doTestRemapXrefsRecreateSourceNode gSrcFile gMasterFile_IncludeAll_OFF
)

-- Tests remapping xrefs: delete one of the source obj 
-- the corresponding xref should become unresolved
function testRemapXrefsDeleteSourceNode_IncludeAll_OFF =
(
	doTestRemapXrefsDeleteSourceNode gSrcFile gMasterFile_IncludeAll_OFF
)
--------------------------------------------
-- Macro methods
--------------------------------------------
On isEnabled return true
On isVisible return true

global createTestSuite = undefined
global ts = undefined
On Execute Do	
(
	-- Create instance of xref utility "object"
  if undefined == createObjXRefUtil do (
  	fileIn ((getdir #scripts) + "/startup/MxsUnitTest/ObjXRef/OBjXRef.Util.ms")
  )
	oxUtil = createObjXRefUtil()

	if createTestSuite == undefined do (
		fileIn ((getdir #scripts) + "/startup/MxsUnitTest/MxsUnitTestFramework.ms")
	)

  -- create a test suite that outputs cmmands into a log file and messages into a new maxscript window (default behaviour)
  -- local logFileName = ((getdir #maxroot) + "DumpObjXRefMgr.log")
  -- global xrefDumpWnd = newScript()
  ts = createTestSuite "UnitTest_ObjXRef_RemappingSourceItems" setupUT teardownUT -- cmdLog:undefined msgLog:xrefDumpWnd

	ts.addTest testRemapXrefs_IncludeAll_ON
	ts.addTest testRemapXrefsRecreateSourceNode_IncludeAll_ON
	ts.addTest testRemapXrefsRenameSourceNode_IncludeAll_ON
	ts.addTest testRemapXrefsDeleteSourceNode_IncludeAll_ON
	
	ts.addTest testRemapXrefs_IncludeAll_OFF
	ts.addTest testRemapXrefsRecreateSourceNode_IncludeAll_OFF
	ts.addTest testRemapXrefsRenameSourceNode_IncludeAll_OFF
	ts.addTest testRemapXrefsDeleteSourceNode_IncludeAll_OFF

	-- TODO: test xref proxy
	-- TODO: test nested xref

	ts.run()
)

) -- END MACRO
