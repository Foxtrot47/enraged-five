macroScript UnitTest_ObjXRef_FileIO
	ButtonText:"UnitTest_ObjXRef_FileIO"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Test - Object Xrefs" 
	Tooltip:"Unit Test - Obj Xref - FileIO" 
(--macro begins


--****************************************************
-- VARIABLES LOCAL TO THE MACRO

--ui options held in variables
local fltXref_test_01 --floating dialog


--test types
local boolDoMergeScene = true
local boolDoXrefs = true 
local boolDoNestedXrefs = true 
local boolPlaybackTimeline = true

--test options
local boolMergeXrefedScene = true 
local boolRenderScene = true
local boolDeleteAllUndoRedoUndo = false

--xref options
local boolMergeMaterials = true

-- Directory with max files to test
local strWorkingFolder = GetDir #scene


--/////////////////////////////////////////////////////////////////////
/*
xref_test_01 function

1- loop through all max files in a given directory
2- opens them and gather data from them (count object, make a viewport grab at time 20)
3- xref them in a new scene and gather the same data
4- xref them again to make a nested xref and gather the same data
5- the model is rendered
6- the data is compared and logged
7- show maps in viewport is triggered to spot any issues with that


*/
function xref_test_01 = 

(--function begins

	--variables
	local strTestName = "xref_test_01_results"
	
	local file,fname, progVal,progmAX, srcNodeCount = 0, NodeCount, cam, bbox,rndrStatus
	local flist = #()
	local strFnameLog = (strWorkingFolder + "\\" + strTestName + ".log")
	local fstrLog = createFile strFnameLog 
	local fIntermediate = ( strWorkingFolder + "\\" + strTestName + "\\_temp.max")



	--test results paths	
	local strSourceFilesImagesDir = (strWorkingFolder  + "\\" + strTestName + "\\_imgSourceFilesImages")

	local strMergedFilesImagesDir = (strWorkingFolder  + "\\" + strTestName + "\\_imgMergedFilesImages")
	local strMasterFilesImagesDir = (strWorkingFolder  + "\\" + strTestName + "\\_imgMasterFilesImages")
	local strMasterNestedFilesImagesDir = (strWorkingFolder  + "\\" + strTestName + "\\_imgXrefedNestedFilesImages")
	local strMasterFilesMergedImagesDir = (strWorkingFolder  + "\\" + strTestName + "\\_imgXrefedFilesMergedImages")


	local strMergedComparisonResultImagesDir = (strWorkingFolder  + "\\" + strTestName + "\\_imgMergedComparisonResultsImages")
	local strXrefedComparisonResultImagesDir = (strWorkingFolder  + "\\" + strTestName + "\\_imgXrefedComparisonResultsImages")
	local strNestedComparisonResultImagesDir = (strWorkingFolder  + "\\" + strTestName + "\\_imgNestedComparisonResultsImages")
	local strXrefedMergedComparisonResultImagesDir = (strWorkingFolder  + "\\" + strTestName + "\\_imgXrefedMergedComparisonResultsImages")	
	
	
	
	--makes directories for the tests results
	makedir (strWorkingFolder + "\\" + strTestName)
	makedir strSourceFilesImagesDir 

	makedir strMergedFilesImagesDir 	
	makedir strMasterFilesImagesDir 
	makedir strMasterNestedFilesImagesDir 
	makedir strMasterFilesMergedImagesDir 
	
	makedir strMergedComparisonResultImagesDir 
	makedir strXrefedComparisonResultImagesDir 
	makedir strNestedComparisonResultImagesDir 
	makedir strXrefedMergedComparisonResultImagesDir 
	
	--flushes the images in the test results directories, if any
	deletefiles strSourceFilesImagesDir "*.*"
	
	deletefiles strMergedFilesImagesDir "*.*"	
	deletefiles strMasterFilesImagesDir "*.*"
	deletefiles strMasterNestedFilesImagesDir "*.*"
	deletefiles strMasterFilesMergedImagesDir "*.*"
	
	deletefiles strMergedComparisonResultImagesDir "*.*"
	deletefiles strXrefedComparisonResultImagesDir "*.*"
	deletefiles strNestedComparisonResultImagesDir "*.*"	
	deletefiles strXrefedMergedComparisonResultImagesDir "*.*"

	--sets the xrefs parameters based on the options of the rollout
	local arXrefParams = #()
	if (boolMergeMaterials == true) do append arXrefParams #mergeMaterials

	
	--/////////////////////////////////////////////////////////////////////////////////////////
	--LOOP TRHOUGH ALL FILES ON DISK
	--/////////////////////////////////////////////////////////////////////////////////////////
	
	--loop through the max files in the working directory
	flist = getfiles (strWorkingFolder + "\\*.max")
	
	--calculates the percentage for the progress bar
	progVal = 0
	progMax = flist.count
	progressStart ("Xrefing files: " + strTestName)
	enableEscape = true
	
	--dump options used to perform the test in the log file
	format "Test Types:\n\n" "" to:fstrLog
	format "    Merge Source Scene Test   = %\n" 	boolDoMergeScene to:fstrLog
	format "    Xrefs Test                = %\n" 	boolDoXrefs to:fstrLog
	format "    Nested Xrefs Test         = %\n" boolDoNestedXrefs to:fstrLog

	format "\nTest Options:\n\n" "" to:fstrLog
	format "    Playback Timeline         = %\n" boolPlaybackTimeline to:fstrLog
	format "    Render Scene              = %\n" boolRenderScene to:fstrLog	
	format "    Delete All Undo Redo Undo = %\n" boolDeleteAllUndoRedoUndo to:fstrLog	
	format "    Merge Xrefed Scene Test   = %\n" boolMergeXrefedScene to:fstrLog

	format "\nXref Options:\n\n" "" to:fstrLog
	format "Merge Materials           = %\n" boolMergeMaterials to:fstrLog

	if (boolDoNestedXrefs) do format "\nNOTE: Skipping nested xref tests of target lights and cameras\n"  to:fstrLog
	
	for file in flist do
	(
		
		--format "%\n"s a visual delimiter in the log file
		format "%\n" ("--------------------------------------\t\t\t") to:fstrLog

		--progress bar handling
		progVal = progVal + 1
		progressUpdate (100.0 * progVal / progMax)
		if getProgressCancel() then exit
	
		--///////////////////////////////////////////////////////////////////////////////////////////////
		--CLEARS OUT TEMPORARY FILES
		--///////////////////////////////////////////////////////////////////////////////////////////////
		
		deletefile fIntermediate 


		--///////////////////////////////////////////////////////////////////////////////////////////////
		--OPEN SOURCE FILE AND GATHER DATA
		--///////////////////////////////////////////////////////////////////////////////////////////////
		--opens the file, count the # of nodes in the scene
		fname = getFilenameFile file
		
		

		------------------------------------------------------------------------------------------------------------------------
		--open max file
		try (
				loadmaxfile file
				actionMan.executeAction 0 "40807"  -- Views: Activate All Maps	
				format "%\n" (fname + "\tok\topen\t") to:fstrLog
			)
		catch(format "%\n" (fname + "\terror\topen\t") to:fstrLog)
		

		------------------------------------------------------------------------------------------------------------------------
		--node count
		try (

				srcNodeCount = objects.count
				format "%\n" (fname + "\tok\tsource file objects count\t"+ (srcNodeCount as string) + "") to:fstrLog
			)
		catch(format "%\n" (fname + "\terror\tsource file objects count\t") to:fstrLog)
		
		
		------------------------------------------------------------------------------------------------------------------------
		--bounding box gathering
		try (

				bbox = objects.max
				format "%\n" (fname + "\tok\tsource file bounding box\t"+ (bbox as string) + "") to:fstrLog
			)
		catch(format "%\n" (fname + "\terror\tsource file bounding box\t") to:fstrLog)		

		------------------------------------------------------------------------------------------------------------------------
		--source file viewport grab
		try (			
				--make viewport grab at frame 20 and store in a directory as a bitmap
				sliderTime = 20
				cam = CreateBoundingCam()
				grabVport (strSourceFilesImagesDir + "\\" + fname) cam
				delete cam
				format "%\n" (fname + "\tok\tsource file viewport grab\t") to:fstrLog
				
			)
		catch(format "%\n" (fname + "\terror\tsource file viewport grab\t") to:fstrLog)

		------------------------------------------------------------------------------------------------------------------------
		--source file render
		if (boolRenderScene == true) do
		(
				format "Render Source File\n" to:fstrLog	
				cam = CreateBoundingCam()
				try (		
						-- scaline rendering test
						rndrStatus = renderFrame (strMasterFilesImagesDir + "\\" + fname) cam (Default_Scanline_Renderer())
		
						if (rndrStatus.count == 0) then (
							format "%\n" (fname + "\tok\tscanline render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
						else (
							format "%\n" (fname + "\terror\tscanline render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
				)
				catch (
					format "%\n" (fname + "\terror\tscanline render\t") to:fstrLog
				)	
						
				try (		
						-- mr rendering test
						rndrStatus = renderFrame (strMasterFilesImagesDir + "\\" + fname) cam (mental_ray_renderer())
						if (rndrStatus.count == 0) then (
							format "%\n" (fname + "\tok\tmr render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
						else (
							format "%\n" (fname + "\terror\tmr render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
				)
				catch (
					format "%\n" (fname + "\terror\tmr render\t") to:fstrLog
				)	
				delete cam
		)	

		--skip a line in the log for ease of reading
		format "%\n" ("") to:fstrLog		
		
		--///////////////////////////////////////////////////////////////////////////////////////////////
		--MERGE SOURCE FILES AND GATHER DATA
		-- the reason for merging the source data in a new file is that Xref and Merge shares the same code.
		-- therefore, it is important to exercise both merge and xrefs at the same time
		-- and spot differences between the 2
		--///////////////////////////////////////////////////////////////////////////////////////////////	

		if (boolDoMergeScene == true) do
		(
			--reset
			resetMaxFile #noprompt
			
			------------------------------------------------------------------------------------------------------------------------
			--merges the file
			try (
					mergeMaxFile file
					actionMan.executeAction 0 "40807"  -- Views: Activate All Maps	
					format "%\n" (fname + "\tok\tmerge\t") to:fstrLog
				)
			catch(format "%\n" (fname + "\terror\tmerge\t") to:fstrLog)
	
	
			------------------------------------------------------------------------------------------------------------------------	
			--hold
			try (
					holdmaxfile()
					format "%\n" (fname + "\tok\thold\t") to:fstrLog
				)
			catch(format "%\n" (fname + "\terror\thold\t") to:fstrLog)
	
			------------------------------------------------------------------------------------------------------------------------		
			--fetch
			try (
					fetchmaxfile quiet:true
					format "%\n" (fname + "\tok\tfetch\t") to:fstrLog
				)
			catch (format "%\n" (fname + "\terror\tfetch\t") to:fstrLog)
				
	
			------------------------------------------------------------------------------------------------------------------------		
			--delete all undo redo undo
			if (boolDeleteAllUndoRedoUndo == true) do
			(
				try (
						with undo "Delete All" on delete objects
						format "%\n" (fname + "\tok\tdelete all\t") to:fstrLog

						max undo
						format "%\n" (fname + "\tok\tundo delete all\t") to:fstrLog
						format "objects.count = %; srcNodeCount = %\n" $objects.count srcNodecount

						max redo
						format "%\n" (fname + "\tok\tredo delete all\t") to:fstrLog
						ts.assert ($objects.count == 0) "Delete All Redone"

						max undo
						format "%\n" (fname + "\tok\tundo delete all\t") to:fstrLog
				)
				catch (format "%\n" (fname + "\terror\tdelete all undo redo undo\t") to:fstrLog)
			)		
		
	
			------------------------------------------------------------------------------------------------------------------------
			--compares the # of nodes in the merged file vs the source file
			try (
					if (srcNodeCount == objects.count) then
					(
						 format "%\n" (fname + "\tok\tmerged file objects count\t" + (objects.count as string) + "") to:fstrLog
					)
					else
					(
						 format "%\n" (fname + "\terror\tmerged file objects count\t" + (objects.count as string) + "") to:fstrLog
					)
				)
			catch(format "%\n" (fname + "\terror\tsrcNodeCount\t") to:fstrLog)
	
			------------------------------------------------------------------------------------------------------------------------	
			--compares the bounding box in the merged file vs the source file
			try (
					
					if (bbox == objects.max) then
					(
						 format "%\n" (fname + "\tok\tmerged file bounding box\t" + (objects.max as string) + "") to:fstrLog
					)
					else
					(
						 format "%\n" (fname + "\terror\tmerged file bounding box\t" + (objects.max as string) + "") to:fstrLog
					)
				)
			catch(format "%\n" (fname + "\terror\tbounding box\t") to:fstrLog)
	
			------------------------------------------------------------------------------------------------------------------------
			--grab the viewport of merged entities
			try (
			
					--make viewport grab at frame 20 and store in a directory as a bitmap
					sliderTime = 20
					cam = CreateBoundingCam()
					grabVport (strMergedFilesImagesDir + "\\" + fname) cam
					delete cam
					format "%\n" (fname + "\tok\tmergedfile viewport grab\t") to:fstrLog
		
				)
			catch (format "%\n" (fname + "\terror\tmergedfile viewport grab\t") to:fstrLog)
		
	
			------------------------------------------------------------------------------------------------------------------------
			--merged file render
			if (boolRenderScene == true) do
			(	
	
				format "Render Merged File\n" to:fstrLog	
				cam = CreateBoundingCam()
				try (		
						-- scaline rendering test
						rndrStatus = renderFrame (strMasterFilesImagesDir + "\\" + fname) cam (Default_Scanline_Renderer())
		
						if (rndrStatus.count == 0) then (
							format "%\n" (fname + "\tok\tscanline render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
						else (
							format "%\n" (fname + "\terror\tscanline render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
				)
				catch (
					format "%\n" (fname + "\terror\tscanline render\t") to:fstrLog
				)	
						
				try (		
						-- mr rendering test
						rndrStatus = renderFrame (strMasterFilesImagesDir + "\\" + fname) cam (mental_ray_renderer())
						if (rndrStatus.count == 0) then (
							format "%\n" (fname + "\tok\tmr render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
						else (
							format "%\n" (fname + "\terror\tmr render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
				)
				catch (
					format "%\n" (fname + "\terror\tmr render\t") to:fstrLog
				)	
				delete cam
	
			)--end 	if (boolRenderScene == true)
	

			--skip a line in the log for ease of reading
			format "%\n" ("") to:fstrLog		
		)	--end if (boolDoMergeScene == true) do


		--///////////////////////////////////////////////////////////////////////////////////////////////
		--XREF ONCE AND GATHER DATA
		--///////////////////////////////////////////////////////////////////////////////////////////////	

		if (boolDoXrefs == true) do
		(
			--reset
			resetMaxFile #noprompt
			
			------------------------------------------------------------------------------------------------------------------------
			--xref once
			try (
					objXRefMgr.AddXRefItemsFromFile file promptObjNames:false xrefOptions:arXrefParams 		
					actionMan.executeAction 0 "40807"  -- Views: Activate All Maps	
					format "%\n" (fname + "\tok\txref objects\t") to:fstrLog
				)
			catch(format "%\n" (fname + "\terror\txref objects\t") to:fstrLog)
	
	
			------------------------------------------------------------------------------------------------------------------------	
			--hold
			try (
					holdmaxfile()
					format "%\n" (fname + "\tok\thold\t") to:fstrLog
				)
			catch(format "%\n" (fname + "\terror\thold\t") to:fstrLog)
	
			------------------------------------------------------------------------------------------------------------------------		
			--fetch
			try (
					fetchmaxfile quiet:true
					format "%\n" (fname + "\tok\tfetch\t") to:fstrLog
				)
			catch (format "%\n" (fname + "\tok\tfetch\t") to:fstrLog)
		
			------------------------------------------------------------------------------------------------------------------------		
			--merges the xrefed file in the scene via the xref manager
			if (boolMergeXrefedScene == true) do
			(
				------------------------------------------------------------------------------------------------------------------------	
				--merges the xref objects in the current scene
			
				try (
						objXRefMgr.MergeXRefItemsIntoScene objects		
						actionMan.executeAction 0 "40807"  -- Views: Activate All Maps	
						format "%\n" (fname + "\tok\tmerging xrefed objects\t") to:fstrLog
					)
				catch(format "%\n" (fname + "\terror\tmerging xrefed objects\t") to:fstrLog)
	

				------------------------------------------------------------------------------------------------------------------------	
				--hold
				try (
						holdmaxfile()
						format "%\n" (fname + "\tok\thold\t") to:fstrLog
					)
				catch(format "%\n" (fname + "\terror\thold\t") to:fstrLog)
		
				------------------------------------------------------------------------------------------------------------------------		
				--fetch
				try (
						fetchmaxfile quiet:true
						format "%\n" (fname + "\tok\tfetch\t") to:fstrLog
					)
				catch (format "%\n" (fname + "\terror\tfetch\t") to:fstrLog)
			
			
			) --end if (boolMergeXrefedScene == true) do

		
			------------------------------------------------------------------------------------------------------------------------		
			--delete all undo redo undo
			if (boolDeleteAllUndoRedoUndo == true) do
			(
			
				try (
						with undo "Delete All" on delete $objects
						format "%\n" (fname + "\tok\tdelete all\t") to:fstrLog
						max undo
						format "%\n" (fname + "\tok\tundo delete all\t") to:fstrLog
						max redo
						format "%\n" (fname + "\tok\tredo delete all\t") to:fstrLog
						max undo
						format "%\n" (fname + "\tok\tundo delete all\t") to:fstrLog

					)
				catch (format "%\n" (fname + "\terror\tdelete all undo redo undo\t") to:fstrLog)
			)					
	
			------------------------------------------------------------------------------------------------------------------------
			--compares the # of nodes in the master file vs the source file
			try (
					
					if (srcNodeCount == objects.count) then
					(
						 format "%\n" (fname + "\tok\tmaster file objects count\t" + (objects.count as string) + "") to:fstrLog
					)
					else
					(
						 format "%\n" (fname + "\terror\tmaster file objects count\t" + (objects.count as string) + "") to:fstrLog
					)
				)
			catch(format "%\n" (fname + "\terror\tsrcNodeCount\t") to:fstrLog)
	
			------------------------------------------------------------------------------------------------------------------------	
			--compares the bounding box in the master file vs the source file
			try (
					
					if (bbox == objects.max) then
					(
						 format "%\n" (fname + "\tok\tmaster file bounding box\t" + (objects.max as string) + "") to:fstrLog
					)
					else
					(
						 format "%\n" (fname + "\terror\tmaster file bounding box\t" + (objects.max as string) + "") to:fstrLog
					)
				)
			catch(format "%\n" (fname + "\terror\tbounding box\t") to:fstrLog)
	
			------------------------------------------------------------------------------------------------------------------------
			--grab the viewport of xrefed entities
			try (
			
					--make viewport grab at frame 20 and store in a directory as a bitmap
					sliderTime = 20
					cam = CreateBoundingCam()
					grabVport (strMasterFilesImagesDir + "\\" + fname) cam
					delete cam
					format "%\n" (fname + "\tok\tmasterfile viewport grab\t") to:fstrLog
		
				)
			catch (format "%\n" (fname + "\terror\tmasterfile viewport grab\t") to:fstrLog)
		
	
			------------------------------------------------------------------------------------------------------------------------
			--master file render
			if (boolRenderScene == true) do
			(	
				format "Render Master File\n" to:fstrLog
				cam = CreateBoundingCam()
				try (		
						-- scaline rendering test
						rndrStatus = renderFrame (strMasterFilesImagesDir + "\\" + fname) cam (Default_Scanline_Renderer())
		
						if (rndrStatus.count == 0) then (
							format "%\n" (fname + "\tok\tscanline render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
						else (
							format "%\n" (fname + "\terror\tscanline render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
				)
				catch (
					format "%\n" (fname + "\terror\tscanline render\t") to:fstrLog
				)	
						
				try (		
						-- mr rendering test
						rndrStatus = renderFrame (strMasterFilesImagesDir + "\\" + fname) cam (mental_ray_renderer())
						if (rndrStatus.count == 0) then (
							format "%\n" (fname + "\tok\tmr render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
						else (
							format "%\n" (fname + "\terror\tmr render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
				)
				catch (
					format "%\n" (fname + "\terror\tmr render\t") to:fstrLog
				)	
				delete cam
			)--end 	if (boolRenderScene == true)
	
			--skip a line in the log for ease of reading
			format "%\n" ("") to:fstrLog	
	
		)--if (boolDoXrefs == true) do	
		
		
		--///////////////////////////////////////////////////////////////////////////////////////////////
		--XREF A SECOND TIME AND GATHER DATA
		--///////////////////////////////////////////////////////////////////////////////////////////////	
		if (boolDoNestedXrefs == true) do
		(
			local findRes = findString fname "target"
			if (findRes != undefined) do continue
				
			--reset
			resetMaxFile #noprompt
	
			------------------------------------------------------------------------------------------------------------------------
			--xref twice
			try (
			
					--xref once
					objXRefMgr.AddXRefItemsFromFile file promptObjNames:false xrefOptions:arXrefParams 		
					actionMan.executeAction 0 "40807"  -- Views: Activate All Maps
					--xref a second time
					savemaxfile fIntermediate --saves the intermediate file
					resetMaxFile #noprompt
					objXRefMgr.AddXRefItemsFromFile fIntermediate promptObjNames:false xrefOptions:arXrefParams 		
					actionMan.executeAction 0 "40807"  -- Views: Activate All Maps
					format "%\n" (fname + "\tok\tnested xref objects") to:fstrLog
	
					)
			catch(format "%\n" (fname + "\terror\tnested xref objects\t") to:fstrLog)
	
			------------------------------------------------------------------------------------------------------------------------	
			--hold
			try (
					holdmaxfile()
					format "%\n" (fname + "\tok\tnested hold\t") to:fstrLog
				)
			catch(format "%\n" (fname + "\terror\tnested hold\t") to:fstrLog)
	
			------------------------------------------------------------------------------------------------------------------------	
			--fetch
			try (
					fetchmaxfile quiet:true
					format "%\n" (fname + "\tok\tnested fetch\t") to:fstrLog
				)
			catch (format "%\n" (fname + "\terror\tnested fetch\t") to:fstrLog)
	
	
			------------------------------------------------------------------------------------------------------------------------		
			--delete all undo redo undo
			if (boolDeleteAllUndoRedoUndo == true) do
			(
			
				try (
						with undo "Delete All" on delete $objects
						format "%\n" (fname + "\tok\tdelete all\t") to:fstrLog
						max undo
						format "%\n" (fname + "\tok\tundo delete all\t") to:fstrLog
						
						max redo
						format "%\n" (fname + "\tok\tredo delete all\t") to:fstrLog

						max undo
						format "%\n" (fname + "\tok\tundo delete all\t") to:fstrLog
					)
				catch (format "%\n" (fname + "\terror\tdelete all undo redo undo\t") to:fstrLog)
			)			
	
	
			------------------------------------------------------------------------------------------------------------------------
			--compares the # of nodes in the master file vs the source file
			try (
					
					if (srcNodeCount == $objects.count) then
					(
						 format "%\n" (fname + "\tok\tnested objects count\t" + (objects.count as string) + "") to:fstrLog
					)
					else
					(
						 format "%\n" (fname + "\terror\tnested objects count\t" + (objects.count as string) + "") to:fstrLog
					)
				)
			catch(format "%\n" (fname + "\terror\tnested srcNodeCount\t") to:fstrLog)
	
	
			------------------------------------------------------------------------------------------------------------------------
			--compares the bounding box in the master file vs the source file
			try (
					
					if (bbox == objects.max) then
					(
						 format "%\n" (fname + "\tok\tnested bounding box\t" + (objects.max as string) + "") to:fstrLog
					)
					else
					(
						 format "%\n" (fname + "\terror\tnested bounding box\t" + (objects.max as string) + "") to:fstrLog
					)
				)
			catch(format "%\n" (fname + "\terror\tnested bounding box\t") to:fstrLog)
	
	
			
			------------------------------------------------------------------------------------------------------------------------
			--grab the viewport of xrefed entities and render
			try (
			
					--make viewport grab at frame 50 and store in a directory as a bitmap
					sliderTime = 20
					
					--captures the viewport
	
					cam = CreateBoundingCam()
					grabVport (strMasterNestedFilesImagesDir+ "\\" + fname) cam
					delete cam
					format "%\n" (fname + "\tok\tnested viewport grab\t") to:fstrLog
		
				)
			catch (format "%\n" (fname + "\terror\tnested viewport grab\t") to:fstrLog)
			
		
			------------------------------------------------------------------------------------------------------------------------
			--master nested file render
			if (boolRenderScene == true) do
			(			
				format "Render Nested Xrefs\n" to:fstrLog	
				cam = CreateBoundingCam()
				try (		
						-- scaline rendering test
						rndrStatus = renderFrame (strMasterFilesImagesDir + "\\" + fname) cam (Default_Scanline_Renderer())
		
						if (rndrStatus.count == 0) then (
							format "%\n" (fname + "\tok\tscanline render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
						else (
							format "%\n" (fname + "\terror\tscanline render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
				)
				catch (
					format "%\n" (fname + "\terror\tscanline render\t") to:fstrLog
				)	
						
				try (		
						-- mr rendering test
						rndrStatus = renderFrame (strMasterFilesImagesDir + "\\" + fname) cam (mental_ray_renderer())
						if (rndrStatus.count == 0) then (
							format "%\n" (fname + "\tok\tmr render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
						else (
							format "%\n" (fname + "\terror\tmr render\t" + (rndrStatus as string)+ "") to:fstrLog
						)
				)
				catch (
					format "%\n" (fname + "\terror\tmr render\t") to:fstrLog
				)	
				delete cam

			--skip a line in the log for ease of reading
			format "%\n" ("") to:fstrLog	
			)
	
		)--end if (boolDoNestedXrefs == true) do

	

		--///////////////////////////////////////////////////////////////////////////////////////////////
		--PLAYBACK TIMELINE TO EXERCISE EVALUATION CODE AND FIND POTENTIAL CRASHES  NO DATA GATHERED
		--///////////////////////////////////////////////////////////////////////////////////////////////

		if (boolPlaybackTimeline == true) do	
		(

			------------------------------------------------------------------------------------------------------------------------		
			--playback timeline
			try (
					for t = animationrange.start to animationrange.end by 10f do
					(
						--zoom extents the viewport
						max tool zoomextents all
						sliderTime = t
					)
					
					format "%\n" (fname + "\tok\tplayback timeline") to:fstrLog
				)
			catch (format "%\n" (fname + "\terror\tplayback timeline") to:fstrLog)

			--skip a line in the log for ease of reading
			format "%\n" ("") to:fstrLog
		) --end if (boolPlaybackTimeline == true) do			



		--purges data
		flush fstrLog
		gc()
	)--end loop through files

	--progress bar handling
	progressEnd()

	--finishes stream output
	close fstrLog 
	
	
	
	
	--///////////////////////////////////////////////////////////////////////////////////////////////
	--RUNS IMAGE COMPARISON TOOL TO COMPARE SOURCE FILES AND XREFS FILES
	--///////////////////////////////////////////////////////////////////////////////////////////////
	
	--compares the source file and "MERGED" data
	if (boolDoMergeScene == true) do compareImages strSourceFilesImagesDir strMergedFilesImagesDir strMergedComparisonResultImagesDir strWorkingFolder (strTestName + "_merged")

	--compares the source file and "XREF ONCE" data
	if (boolDoXrefs == true) do compareImages strSourceFilesImagesDir strMasterFilesImagesDir strXrefedComparisonResultImagesDir strWorkingFolder (strTestName + "_xrefs")

	--compares the source file and "XREF TWICE" data
	if (boolDoNestedXrefs == true) do compareImages strSourceFilesImagesDir strMasterNestedFilesImagesDir strNestedComparisonResultImagesDir strWorkingFolder (strTestName + "_xrefs_nested")
	
	--launches the log
	shelllaunch strFnameLog  ""


)--end function

--end xref_test_01 function
--//////////////////////////////////////////////////////////////////////////////////////////////////




--//////////////////////////////////////////////////////////////////////////////
-- Dialog
--//////////////////////////////////////////////////////////////////////////////

rollout rltXref_test_01 "Xref Unit Tests: xref_test_01" width:300 height:400
(

	groupbox grpTestTypes "Test Types" pos:[5,10] width:275 height:105
	checkbox chkDoMergeScene "Merge Source Scene Test" checked:on pos:[10,30]
	checkbox chkDoXref "Xrefs Test" checked:on pos:[10,50]
	checkbox chkDoNestedXref "Nested Xrefs Test" checked:on pos:[10,70]
	checkbox chkPlaybackTimeline "Playback Timeline Over Xrefed Data Test" checked:on pos:[10,90]

	groupbox grpTestOptions "Test Options" pos:[5,120] width:275 height:85
	checkbox chkRenderScene "Render Scene (for all tests)" checked:on pos:[10,140]
	checkbox chkDeleteAllUndoRedoUndo "Delete All, Undo, Redo, Undo (for all tests)" checked:off pos:[10,160]
	checkbox chkMergeXrefedScene "Merge all Xrefed Objects (for all Xref tests)" checked:on pos:[10,180]

	groupbox grpXrefOptions "Xref Options" pos:[5,210] width:275 height:50
	checkbox chkMergeMaterials "Merge Materials" checked:off pos:[10,230]
	
	Label srclbl "Source Directory:" pos:[10, 265]
	Button SourcePath "--none--" width:265 pos:[10, 280]
	
	button btnProceed "Proceed" pos:[10,310] width:265 enabled:false
	

	--test types
	on chkDoMergeScene changed state do boolDoMergeScene = chkDoMergeScene.checked
	on chkDoXref changed state do boolDoXrefs = chkDoXref.checked
	on chkDoNestedXref changed state do boolDoNestedXrefs = chkDoNestedXref.checked

	--test options
	on chkPlaybackTimeline changed state do boolPlaybackTimeline = chkPlaybackTimeline.checked
	on chkDeleteAllUndoRedoUndo changed state do boolDeleteAllUndoRedoUndo = chkDeleteAllUndoRedoUndo.checked
	on chkMergeXrefedScene changed state do boolMergeXrefedScene = chkMergeXrefedScene.checked	


	on chkRenderScene changed state do boolRenderScene = chkRenderScene.checked

	--xref options
	on chkMergeMaterials changed state do boolMergeMaterials = chkMergeMaterials.checked
	
	on SourcePath pressed do 
	(
		local tmp=getSavePath caption:"Specify Source Directory" initialDir:strWorkingFolder
		if tmp != undefined do 
		(	
			strWorkingFolder = tmp
			if strWorkingFolder[strWorkingFolder.count] != "\\" do strWorkingFolder += "\\"
			SourcePath.text = SourcePath.tooltip = strWorkingFolder
			btnProceed.enabled = true
		)
	)--End on SourcePath


	on btnProceed pressed do xref_test_01()
)

  -- The test suite object is used for its assert facility
  global createTestSuite = undefined 
  global ts = undefined

-- Dummy method - just to create a test suite
function doNothing = 
(
)

On Execute Do 
(
	-- Load Mxs Unit Test framework
	  if createTestSuite == undefined do fileIn ((getdir #scripts) + "\\Startup\\MxsUnitTest\\MxsUnitTestFramework.ms")
	  if createTestSuite == undefined do fileIn ("MxsUnitTestFramework.ms")
	  ts = createTestSuite "UT.FileIOTests_Test_01" doNothing doNothing cmdLog:fstrLog
	
	-- dialog creation
	if fltXref_test_01 != undefined do closerolloutfloater fltXref_test_01
	
	fltXref_test_01 = newRolloutFloater "Xref Unit Tests" 300 350 200 200	
	addRollout rltXref_test_01 fltXref_test_01
	
)


)-- end macro
