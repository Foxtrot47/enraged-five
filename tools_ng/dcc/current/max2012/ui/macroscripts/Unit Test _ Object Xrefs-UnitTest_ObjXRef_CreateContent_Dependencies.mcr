macroScript UnitTest_ObjXRef_CreateContent_Dependencies
	ButtonText:"UnitTest_ObjXRef_CreateContent_Dependencies"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Test - Object Xrefs" 
	Tooltip:"Unit Test - Obj Xref - CreateContentDependencies" 
(

	-- Macro Variables
	local strWorkingFolder 
	strWorkingFolder = GetDir #scene

---------------------------------------------------------------------
function write_max_files_with_dependencies = 
(
	--variables
	local obj, objA, objB, m, file, fname, progMax, progVal
	
	-- log file
	local strTestName = "write_max_files_with_dependencies"
	local fstrLog = createFile ( strWorkingFolder + "\\" + strTestName + ".log")

	--calculates the percentage for the progress bar
	progVal = 0
	progMax = 100.0
	progressStart strTestName 
	
	--loop through all maps and make a max file on disk
	--disableSceneRedraw()

	--******************************************************************
	-- CAMERA MAP (map making a reference to a node)
	--make an object, camera and a camera map map, assign the camera to
	--the camera map map, animate the camera and box, save the file
	--*****************************************************************
	local fileName = "\\DEPENDENCIES_CAMERA_MAP_TARGETCAM.max"
	print ("Creating " + fileName + "... ") to:fstrLog
	resetMaxFile #noprompt	
	obj = box()
	obj.material = StandardMaterial()
	obj.material.diffusemap = camera_map_per_pixel()
	obj.material.diffusemap.camera = Targetcamera fov:45 nearclip:1 farclip:1000 nearrange:0 farrange:1000 mpassEnabled:off mpassRenderPerPass:off pos:[131.816,68.0035,0] isSelected:on target:(Targetobject transform:(matrix3 [1,0,0] [0,1,0] [0,0,1] [-31.7196,3.59732,0]))
	actionMan.executeAction 0 "40807"  -- Views: Activate All Maps
	animate on
	(	
		--animate the box
		at time 0 (obj.pos = [-100, 0, 0]; obj.scale = [1,1,1])
		at time 100 (obj.pos = [100, 100, 100]; obj.scale = [-1,2,10] )

		--animate the camera
		at time 0 (obj.material.diffusemap.camera.pos = [-100, 0, 0])
		at time 100 (obj.material.diffusemap.camera.pos = [100, 100, 100])
	)

	file = (strWorkingFolder + filename)
	doSaveMaxFile file fstrLog
	flush fstrLog
	gc()
	
	--*****************************************************************
	-- Shell Modifier (modifier making a reference to a node)
	--make 2 shapes, apply Bevel Profile modifier to first shape,
	--use the 2nd shape for the profile, animate the shapes,  save the file
	--*****************************************************************
	fileName = "\\DEPENDENCIES_SHELL_MODIFIER_AND_SPLINE.max"
	print ("Creating " + fileName + "... ") to:fstrLog
	resetMaxFile #noprompt
	obj = rectangle()
	addModifier obj (shell())
	obj.modifiers[1].bevelshape = circle()
	animate on
	(	
		--animate the rectangle
		at time 0 (obj.width = 25; obj.scale = [1,1,1])
		at time 100 (obj.width = 35; obj.scale = [1,2,0.25])

		--animate the circle
		at time 0 (obj.modifiers[1].bevelshape.radius = 20)
		at time 100 (obj.modifiers[1].bevelshape.radius = 100)
	)

	file = (strWorkingFolder + fileName)
	doSaveMaxFile file fstrLog
	flush fstrLog
	gc()
	
	--*****************************************************************
	-- PROJECTOR MAP (light making a reference to a map)
	-- make a light, assigns a map as a projector map, animate the map, save the file
	-- apply the same map on the diffuse channel of an object and show in viewport
	--*****************************************************************
	fileName = "\\DEPENDENCIES_PROJECTOR_MAP.max"
	print ("Creating " + fileName + "... ") to:fstrLog
	resetMaxFile #noprompt
	obj = omnilight()
	obj.projectormap = checker()
	objB = box()
	objB.material = StandardMaterial()
	objB.material.diffusemap = obj.projectormap
	showtexturemap objB.material true
	actionMan.executeAction 0 "40807"  -- Views: Activate All Maps
	
	animate on
	(	
		--animate the light
		at time 0 (obj.pos = [-20,-20,0]; obj.rgb = (color 255 255 255))
		at time 100 (obj.pos = [-10,-110,10]; obj.rgb = (color 0 255 0))
		
		--animate the checker
		at time 0 (obj.projectormap.color1 = (color 255 255 0))
		at time 100 (obj.projectormap.color1 = (color 0 255 0))
	)

	file = (strWorkingFolder + fileName)
	doSaveMaxFile file fstrLog
	flush fstrLog
	gc()

	--*****************************************************************
	-- PATH Constraint (controller referencing a node)
	-- make a teapot and circle, assigns the teapot to travel around the
	-- circle using a path constraint
	--*****************************************************************
	fileName =  "\\DEPENDENCIES_PATH_CONSTRAINT.max"
	print ("Creating " + fileName + "... ") to:fstrLog
	resetMaxFile #noprompt
	obj = teapot()
	obj.position.controller = Path()
	obj.position.controller.path = circle radius:80
	animate on
	(	
		--animate the path
		at time 0 (obj.position.controller.path.radius = 20)
		at time 100 (obj.position.controller.path.radius = 200)
	)

	file = (strWorkingFolder + fileName)
	doSaveMaxFile file fstrLog
	flush fstrLog
	gc()

	--*****************************************************************
	-- LOOK AT TARGET
	-- make 2 boxes, one looking at another one
	--*****************************************************************
	fileName =  "\\DEPENDENCIES_ARBITRARY_TARGET_OBJECT.max"
	print ("Creating " + fileName + "... ") to:fstrLog
	resetMaxFile #noprompt
	obj = box pos:[10,10,10]
	obj.target = box pos:[-10,-10,-10]
	obj.target.target = box pos:[20,-20,-30]
	animate on
	(	
		--animate the boxes
		at time 0 (obj.position = [0,0,0];obj.target.position = [10,10,10]; obj.target.target.position = [-10,-10,-10])
		at time 100 (obj.position = [0,0,10];obj.target.position = [0,0,110]; obj.target.target.position = [0,0,-110])
	)

	file = (strWorkingFolder + fileName)
	doSaveMaxFile file fstrLog
	flush fstrLog
	gc()

	--*****************************************************************
	-- PARAMETER WIRING
	-- make 2 boxes, wire their length one each other
	--*****************************************************************
	fileName = "\\DEPENDENCIES_PARAM_WIRING.max"
	print ("Creating " + fileName + "... ") to:fstrLog
	resetMaxFile #noprompt
	objA = box pos:[10,10,10]
	objB = box pos:[-10,-10,-10]
	paramWire.connect objA.baseobject[#length] objB.baseobject[#length] "Length"
	animate on
	(	
		--animate the boxes
		at time 0 (objA.length = 10)
		at time 100 (objA.length = 100)
	)

	file = (strWorkingFolder + fileName)
	doSaveMaxFile file fstrLog
	flush fstrLog
	gc()

	--*****************************************************************
	-- PARENT CHILD HIERARCHY
	-- make 3 boxes, parent them one each other, animate their pos and rot
	--*****************************************************************
	fileName =  "\\DEPENDENCIES_PARENT_CHILD_CHAIN.max"
	print ("Creating " + fileName + "... ") to:fstrLog
	resetMaxFile #noprompt
	obj = box pos:[0,0,0] length:5 width:5 height:5
	obj.parent = box pos:[0,0,10] length:5 width:5 height:5
	obj.parent.parent = box pos:[0,0,20] length:5 width:5 height:5
	animate on
	(	
		--animate the boxes
		at time 0 (obj.parent.parent.pos = [0,0,20])
		at time 0 (obj.parent.pos = [0,10,10])

		at time 100 (obj.parent.pos = [0,10,10]; obj.parent.rotation = (quat 0 1 0 0))
		at time 100 (obj.parent.parent.pos = [0,50,50]; obj.parent.parent.rotation = (quat 0 0 1 0))
	)

	file = (strWorkingFolder + fileName)
	doSaveMaxFile file fstrLog
	flush fstrLog
	gc()

	--*****************************************************************
	-- BOOLEAN COMPOUND OBJECTS
	-- make 2 boxes, boolean them togheter using the "instance" option
	-- animate them and see the boolean being updated
	-- the instanced operand has a an animated modifier on it as well
	--*****************************************************************
	fileName = "\\DEPENDENCIES_BOOLEAN.max"
	print ("Creating " + fileName + "... ") to:fstrLog
	resetMaxFile #noprompt
	objA = box pos:[0,0,0] length:50 width:50 height:50
	objB = box pos:[-25,-25,25] length:50 width:50 height:50
	
	--makes boolean
	boolObj.createBooleanObject objA
	boolObj.SetOperandB objA objB 1 1
	boolObj.SetBoolOp objA 3
	boolObj.setShowHiddenOps objA true
	
	--moves the objeB out of the way
	objB.pos = [100,100,25]
	objB.heightsegs = 20
	addModifier objB (Taper())

	animate on
	(	
		--animate the operand B
		at time 0 (objA.Operand_B_Transform.pos = [-25,-25,25])
		at time 100 (objA.Operand_B_Transform.pos = [25,25,0];objA.Operand_B_Transform.rotation =(quat 0 0 1 0))
		at time 100 (objB.modifiers[1].amount = -0.95; objB.modifiers[1].curve =  0.8;)
	)

	file = (strWorkingFolder + fileName)
	doSaveMaxFile file fstrLog
	flush fstrLog
	gc()

	--*****************************************************************
	-- MESHER COMPOUND OBJECT
	-- make 1 teapot, and a mesher object, set the teapot as the mesher object node
	-- apply a modifier on the teapot
	-- animate
	--*****************************************************************
	fileName = "\\DEPENDENCIES_MESHER.max"
	print ("Creating " + fileName + "... ") to:fstrLog
	resetMaxFile #noprompt
	obj = Mesher()
	obj.useCustomBounds = on
	obj.boundsMin = [28.4515,-23.3317,0]
	obj.boundsMax = [61.8114,0.855244,27.2675]

	objA = teapot pos:[25,25,25] segs:10
	addModifier objA (Taper())
	obj.pick = objA

	animate on
	(	
		--animate the operand B
		at time 0 (obj.pos = [-25,-25,25])
		at time 100 (obj.pos = [25,25,0];obj.rotation =(quat 0 0 1 0))
		at time 100 (objA.modifiers[1].amount = -0.95; objA.modifiers[1].curve =  0.8;)
	)

	file = (strWorkingFolder + fileName)
	doSaveMaxFile file fstrLog
	flush fstrLog
	gc()

	--*****************************************************************
	-- TERRAIN COMPOUND OBJECT
	-- make circles and assigns them to a terrain object as instances
	-- modify the circles and animate
	--*****************************************************************
	fileName =  "\\DEPENDENCIES_TERRAIN.max"
	print ("Creating " + fileName + "... ") to:fstrLog
	resetMaxFile #noprompt

	circle radius:100 pos:[0,0,0]
	circle radius:75 pos:[0,0,20]
	circle radius:30 pos:[0,0,30]
	circle radius:10 pos:[0,0,50]
	
	obj = Terrain()
	terrainOps.addOperand obj (instance $Circle01)
	terrainOps.addOperand obj (instance $Circle02)
	terrainOps.addOperand obj (instance $Circle03)
	terrainOps.addOperand obj (instance $Circle04)	
	
	animate on
	(	
		--animate the circles radius
		at time 0 ($Circle01.radius = 100; $Circle02.radius = 75; $Circle03.radius = 25;)
		at time 100 ($Circle01.radius = 150; $Circle02.radius = 135; $Circle03.radius = 75;)
		
		--animates the circles positions
		at time 100 (obj.Op_3_Transform.pos = [0,0,100];obj.Op_2_Transform.pos = [0,0,60];)
	)

	file = (strWorkingFolder + fileName)
	doSaveMaxFile file fstrLog
	flush fstrLog
	gc()

	--*****************************************************************
	-- DISPLACEMENT MAP ON MODIFIER 
	-- make a plane with a lot of segments, assigns a checker map as diffuse and displacement modifier
	-- animate the map, save the file
	--*****************************************************************
	fileName =  "\\DEPENDENCIES_DISPLACEMENT_MAP_MODIFIER.max"
	print ("Creating " + fileName + "... ") to:fstrLog
	resetMaxFile #noprompt
	obj = Plane length:100 width:100 pos:[0,0,0] lengthsegs:100 widthsegs:100
	obj.material = StandardMaterial()
	obj.material.diffusemap = noise()
	addModifier obj (Displace())
	obj.modifiers[1].map = obj.material.diffusemap
	obj.modifiers[1].strength = 50
	obj.modifiers[1].length = 100
	obj.modifiers[1].width = 100	
	actionMan.executeAction 0 "40807"  -- Views: Activate All Maps
	
	animate on
	(	
		--animate the checker
		at time 0 (obj.material.diffusemap.color1 = (color 255 0 255);obj.material.diffusemap.color2 = (color 10 10 0))
		at time 20 (obj.material.diffusemap.color1 = (color 12 25 45);obj.material.diffusemap.color2 = (color 210 210 100))	
		at time 100 (obj.material.diffusemap.color1 = (color 127 255 45);obj.material.diffusemap.color2 = (color 10 10 100))
	)
	
	file = (strWorkingFolder + fileName)
	doSaveMaxFile file fstrLog
	flush fstrLog
	gc()

	--*****************************************************************
	-- Atmospherics and effects
	--*****************************************************************
	fileName =  "\\DEPENDENCIES_ATMOSHERICS_EFFECTS.max"
	print ("Creating " + fileName + "... ") to:fstrLog
	resetMaxFile #noprompt
	obj = BoxGizmo length:100 width:100 height:100
	
	atmos = fire_effect density:20 outer_color:red
	addAtmospheric atmos
	appendGizmo atmos obj

	atmos = volume_fog()
	addAtmospheric atmos
	appendGizmo atmos obj

	animate on
	(	
		at time 0 (obj.length = 100;obj.width = 100;obj.height = 100)	
		at time 100 (obj.length = 200;obj.width = 20;obj.height = 50)	
	)

	file = (strWorkingFolder + fileName)
	doSaveMaxFile file fstrLog
	flush fstrLog
	gc()

	--*****************************************************************
	-- SPLINE IK AND BONES
	--*****************************************************************
	fileName =  "\\DEPENDENCIES_SPLINE_IK.max"
	print ("Creating " + fileName + "... ") to:fstrLog
	resetMaxFile #noprompt

	--makes bones
	obj  = BoneSys.createBone [0,0,0] [100,100,0] [0,0,1]
	objA = BoneSys.createBone [100,100,0] [0,150,0] [0,0,1]
	objB = BoneSys.createBone [0,150,0] [-50,-50,0] [0,0,1]
	
	objB.parent = objA
	objA.parent = obj

	--adds the spline
	splineObj = line pos:[0,0,0] 
	addnewspline splineObj 
	addknot splineObj 1 #smooth #curve [0,0,0]
	addknot splineObj 1 #smooth #curve [100,100,0]
	addknot splineObj 1 #smooth #curve [0,150,0]
	addknot splineObj 1 #smooth #curve [-50,-50,0]
	updateshape splineObj 
	
	--make the spline renderable
	splineObj.render_renderable = true
	splineObj.render_thickness = 10

	--applys the ik controller
	ctrl = iksys.IKChain  obj objB "SplineIKSolver"
	ctrl.transform.controller.pickShape = splineObj 
	--makes the spline IK
	mod = Spline_IK_Control()
	AddModifier splineObj (mod)
	mod.createHelper(0)

	animate on
	(	
		at time 0 ($Point03.pos = [0,150,0])	
		at time 100 ($Point03.pos = [-250,550,1500])	
	)

	file = (strWorkingFolder + fileName)
	doSaveMaxFile file fstrLog
	flush fstrLog
	gc()


	--finishes stream output
	close fstrLog 

	--redraws enabled	
	--enableSceneRedraw()

	--progress bar handling
	progressEnd()
	--resetMaxFile #noprompt
	gc()
	
)--write_max_files_with_dependencies

-- MACRO HANDLERS
On Execute Do 
(
	write_max_files_with_dependencies()
)

)--end macro
