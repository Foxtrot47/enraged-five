macroScript UnitTest_ObjXRef_Exporters
	ButtonText:"UnitTest_ObjXRef_Exporters"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Test - Object Xrefs" 
	Tooltip:"Unit Test - Obj XRef - Exporters" 
(--macro begins
	
--*****************************************
--exportFileAsAllTypes fname fstrLog

--loops through all exporter plugins
--export the current scene to a given filename
--log the success or failure
--*****************************************

function exportFileAsAllTypes file fstrLog = 
(

	for exp in exporterPlugin.classes do
	(
	if getProgressCancel() then exit
	try (
			exportFile (file + "_" + (exp as string)) #noprompt using:exp
			fname = getFilenameFile file
			print (fname + " xref objects export | " + (exp as string)+ " | ok") to:fstrLog
		)
	catch(print (fname + " xref objects export| error") to:fstrLog)
	)--end for loop
)--end function



--//////////////////////////////////////////////////////////////
-- Unit Test Functions

--/////////////////////////////////////////////////////////////////////
/*
xref_test_exporters

1- loop through all max files in a given directory
2- opens and export them to various formats
3- log the success or failure

*/

function xref_test_exporters = 

(--function begins

	--work path
	local strWorkingFolder 
	strWorkingFolder = GetDir #scene

	--variables
	local strTestName = "xref_exporters_results"
	
	local file,fname, progVal,progmAX, exp
	local flist = #()
	local fstrLog = createFile ( strWorkingFolder + "\\" + strTestName + ".log")
	local fIntermediate = ( strWorkingFolder + "\\" + strTestName + "\\_temp.max")

	--makes directories for the tests results
	makedir (strWorkingFolder + "\\" + strTestName)
	
	--deletes previously exported files if any
	deletefiles (strWorkingFolder + "\\" + strTestName) "*.*"

	--collects files in the directory
	flist = getfiles (strWorkingFolder + "\\*.max")
	
	--calculates the percentage for the progress bar
	progVal = 0
	progMax = flist.count
	progressStart ("Xrefing files: " + strTestName)
	enableEscape = true
	
	for file in flist do
	(
		--progress bar handling
		progVal = progVal + 1
		progressUpdate (100.0 * progVal / progMax)
		if getProgressCancel() then exit
	

		fname = getFilenameFile file
	
		--///////////////////////////////////////////////////////////////////////////////////////////////
		--XREF ONCE AND EXPORT
		--///////////////////////////////////////////////////////////////////////////////////////////////	
	
		--reset
		resetMaxFile #noprompt
		
		
		--xref
		try (
				objXRefMgr.AddXRefItemsFromFile file promptObjNames:false xrefOptions:#(#xrefModifiers)		
				actionMan.executeAction 0 "40807"  -- Views: Activate All Maps	
				print (fname + " xref objects | ok") to:fstrLog
			)
		catch(print (fname + " xref objects | error") to:fstrLog)

		--export file
		exportFileAsAllTypes (strWorkingFolder + "\\" + strTestName + "\\" + fname) fstrLog


		--///////////////////////////////////////////////////////////////////////////////////////////////
		--XREF A SECOND TIME AND EXPORT AGAIN
		--///////////////////////////////////////////////////////////////////////////////////////////////	
		--reset
		resetMaxFile #noprompt

		--xref
		try (
		
				--xref once
				objXRefMgr.AddXRefItemsFromFile file promptObjNames:false xrefOptions:#(#xrefModifiers)		
				actionMan.executeAction 0 "40807"  -- Views: Activate All Maps
				--xref a second time
				savemaxfile fIntermediate --saves the intermediate file
				resetMaxFile #noprompt
				objXRefMgr.AddXRefItemsFromFile fIntermediate promptObjNames:false xrefOptions:#(#xrefModifiers)		
				actionMan.executeAction 0 "40807"  -- Views: Activate All Maps
				print (fname + " nested xref objects | ok") to:fstrLog

				)
		catch(print (fname + " nested xref objects | error") to:fstrLog)


	
		flush fstrLog
		gc()

	)--end loop through files

	--progress bar handling
	progressEnd()

	--finishes stream output
	close fstrLog 


)--end function

--//////////////////////////////////////////////////////////////////////////////////////////////////


On Execute Do 
(

	xref_test_exporters()
)

)-- end macro
