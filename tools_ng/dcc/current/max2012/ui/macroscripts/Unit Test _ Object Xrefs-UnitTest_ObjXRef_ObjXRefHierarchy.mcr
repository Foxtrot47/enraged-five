macroScript UnitTest_ObjXRef_ObjXRefHierarchy
	ButtonText:"UnitTest_ObjXRef_ObjXRefHierarchy"
	Category:"Unit Test - Object Xrefs" 
	internalCategory:"Unit Test - Object Xrefs" 
	Tooltip:"Unit Test - Obj Xref - TraverseHierarchy" 
(	
-- Object XRef helper methods defined elsewhere
global createObjXRefUtil  = undefined
global createTestSuite = undefined
global ts = undefined
global oxUtil = undefined

----------------------------------------------------
-- Local Utility methods
----------------------------------------------------
-- The path and name of the scene that will get xrefed
global gSourceFileName = ((getDir #temp) + "\\ObjXref.Hierarchy.Test")

-- This creates and saves the source scene
function createSourceScene =
(
  resetMaxFile #noprompt

  -- Box with checker material
  l_obj = Box length:20 width:20 height:40 mapcoords:on realWorldMapSize:on pos:[20, 20, 0]
  meditMaterials[1].diffuseMap = Checker()
  l_obj.material = meditMaterials[1]

  -- Sphere gizmo with Fire Effect
  l_obj = SphereGizmo radius:16 pos:[-25,-25,0]
  l_effect = Fire_Effect ()
  addAtmospheric l_effect
  appendGizmo l_effect l_obj
  -- Save file
  local res = oxUtil.saveTmpFile (gSourceFileName + ".max")
  ts.assert (true == res) "Source file created"
)

function doTestSimpleXrefHierarchy xRec = 
(
  ts.assert (undefined != xRec) "Check xref record exists"

  local xrParent = #()
  xRec.GetParentRecords &xrParent
  ts.assert (xrParent.count <= 0) "Check # of parent records"

  local xrRoot = #()
  xRec.GetRootRecords &xrRoot
  ts.assert (xrRoot.count == 1 and xrRoot[1] == xRec) "Check # of root records"

  local xrChildren = #()
  xRec.GetChildRecords &xrChildren
  ts.assert (xrChildren.Count <= 0) "Check # of child records"
)

function doTestNestedXrefHierarchy xRecPar = 
(
  ts.assert (undefined != xRecPar) "Parent record exists"

  local xrChildren = #()
  xRecPar.GetChildRecords &xrChildren
  ts.assert (xrChildren.count == 1) "Check # of child records"

  local xrParent = #()
  xrChildren[1].GetParentRecords &xrParent
  ts.assert (xrParent.count == 1 and xrParent[1] == xRecPar) "Check # of parent records"

  local xrRoot = #()
  xrChildren[1].GetRootRecords &xrRoot
  ts.assert (xrRoot.count == 1 and xrRoot[1] == xRecPar) "Check # of root records"
)

--------------------------------------------
-- Unit Test methods
--------------------------------------------	
-- Unit test fixture setup
function setupUT =
(
  resetMaxFile #noprompt
)

-- Unit test fixture teardown
function teardownUT =
(
	-- resetMaxFile #noprompt
)

----------------------------------------------------------
function testSimpleXRefRecHierarchy =
(
  local xRec = objXRefMgr.AddXRefItemsFromFile (gSourceFileName + ".max")
  ts.assert (undefined != xRec) "Check simple xref created"
  doTestSimpleXrefHierarchy xRec

  print "test - Clone scene"
  max select all
  maxOps.CloneNodes $ offset:[-30, 30, 30] expandHierarchy:true cloneType:#copy 
  doTestSimpleXrefHierarchy xRec


  print "test - Delete all\undo"
  max select all
  -- accept if already holding
  wasHolding = theHold.holding()
  if wasHolding do theHold.Accept "pre Delete"
  undo "UT.XRef: Delete All Nodes" on (
    max delete
  )
  -- start holding again if were previously holding
  if wasHolding do theHold.Begin()
  ts.assert (0 == objXRefMgr.RecordCount) "No records after deleting all objects from scene"
  max undo
  doTestSimpleXrefHierarchy (objXRefMgr.getRecord 1)

  print "test - Hold\Fetch"
  holdMaxFile()
  FetchMaxFile quiet:true
  doTestSimpleXrefHierarchy (objXRefMgr.getRecord 1)
  
  oxUtil.DumpObjXRefRecordsTree()
  oxUtil.DumpParentRecords()
  oxUtil.DumpRootRecords()
)

----------------------------------------------------------
function testNestedXRefRecHierarchy =
(
  print "test - Create nested xref"
  local xRec = oxUtil.createNestedXref (gSourceFileName + ".max")
  ts.assert (undefined != xRec) "Check nested xref created"
  	
  doTestNestedXrefHierarchy xRec

  print "test - Clone scene"
  max select all
  maxOps.CloneNodes $ offset:[-30, 30, 30] expandHierarchy:true cloneType:#copy 
  doTestNestedXrefHierarchy xRec

  print "test - Delete all\undo"
  max select all
  -- accept if already holding
  wasHolding = theHold.holding()
  if wasHolding do theHold.Accept "pre Delete"
  undo "UT.XRef: Delete All Nodes" on (
    max delete
  )
  -- start holding again if were previously holding
  if wasHolding do theHold.Begin()
  ts.assert (0 == objXRefMgr.RecordCount) "No records after deleting all objects from scene"
  max undo
  doTestNestedXrefHierarchy (objXRefMgr.getRecord 1)

  print "test - Hold\Fetch"
  holdMaxFile()
  FetchMaxFile quiet:true
  doTestNestedXrefHierarchy (objXRefMgr.getRecord 1)
  
  oxUtil.DumpObjXRefRecordsTree()
  oxUtil.DumpParentRecords()
  oxUtil.DumpRootRecords()
)

function simpleTest = 
(
	resetMaxFile #noprompt
	objXRefMgr.AddXRefItemsFromFile ("ObjXref.Hierarchy.Test.max") 
	max select all
	max delete
	max undo
)
--------------------------------------------
-- Macro methods
--------------------------------------------
On isEnabled return true
On isVisible return true

On Execute Do	
(
	-- Create instance of xref utility "object"
  if undefined == createObjXRefUtil do (
  	fileIn ((getdir #scripts) + "/startup/MxsUnitTest/ObjXRef/OBjXRef.Util.ms")
  )
	oxUtil = createObjXRefUtil()
  
  if undefined == createTestSuite do fileIn ((getdir #scripts) + "/startup/MxsUnitTest/MxsUnitTestFramework.ms")
  if undefined == createTestSuite do fileIn ("MxsUnitTestFramework.ms")

  -- create a test suite that outputs cmmands into a log file and messages into a new maxscript window (default behaviour)
  -- local logFileName = ((getdir #maxroot) + "DumpObjXRefMgr.log")
  -- global xrefDumpWnd = newScript()
  ts = createTestSuite "ObjXRef.TraverseHierarcy" setupUT teardownUT -- cmdLog:undefined msgLog:xrefDumpWnd
  ts.addTest testSimpleXRefRecHierarchy
  ts.addtest testNestedXRefRecHierarchy
  --ts.addTest simpleTest

  createSourceScene()
  ts.run()
)

) -- END MACRO
