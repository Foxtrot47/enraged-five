macroScript amGroupsInAssemblies
	ButtonText:"amGroupsInAssemblies"
	Category:"Qe Tools - Unit Tests Assemblies" 
	internalCategory:"Qe Tools - Unit Tests Assemblies" 
	Tooltip:"amGroupsInAssemblies" 
	-- Needs Icon
	--Icon:#("Max_edit_modifiers",1)
(	
	-- Unit test fixture setup
	fn setupUT =
	(
		resetmaxFile #noPrompt
		-- create a group and a "free" object
		global _boxes = #()
		b = box()
		append _boxes b
		for i=1 to 2 do 
		(
			tmpBox = copy b
			tmpBox.pos = [i * 30, 0, 0]
			append _boxes tmpBox
		)
		global _g = group #(_boxes[1], _boxes[2])

	)

	-- Unit test fixture teardown
	fn teardownUT =
	(
		--resetmaxFile #noPrompt
	)
	
	-----------------------------------------------------
	--
	fn test1 =
	(
		print "test1 - Assemble Groups"
		a = assemblyMgr.assemble #(_boxes[3], _g) name:"a" classDesc:Dummy
		ts.assert "_boxes[1].assemblymember == false" exprDesc:"Assembling a group doesn't make its members assembly members"
		ts.assert "_g.assemblyhead == false" exprDesc:"Assembling a group doesn't make its head assembly head"
		ts.assert "_g.assemblymember == true" exprDesc:"Assembling a group makes its head assembly member"
	)

	-----------------------------------------------------
	--
	fn test2 =
	(
		print "test2 - Attach Groups"
		a = assemblymgr.assemble _boxes[3] name:"a" classDesc:Dummy
		assemblyMgr.attach _g assembly:a
		if (_boxes[1].assemblymember == true or
			_g.assemblyhead == true or
			_g.assemblymember == false) 
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-----------------------------------------------------
	--
	fn test3 =
	(
		print "test3 - Open Assembly"
		a = assemblyMgr.assemble #(_boxes[3], _g) name:"a" classDesc:Dummy
		assemblyMgr.open a
		if (_boxes[1].assemblymember == true or
			_g.assemblyhead == true or
			_g.assemblyheadopen == true or
			_g.assemblymember == false or
			_g.assemblymemberopen == false or
			(isgrouphead _g) == false)
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-----------------------------------------------------
	--
	fn test4 =
	(
		print "test4 - Close Assembly"
		a = assemblyMgr.assemble #(_boxes[3], _g) name:"a" classDesc:Dummy
		assemblyMgr.open a
		
		if ((assemblymgr.canclose _g) == false)
			do throw "FAILED\n"
		
		assemblyMgr.Close a
		if (_boxes[1].assemblymember == true or
			_g.assemblyhead == true or
			_g.assemblyheadopen == true or
			_g.assemblymember == false or
			_g.assemblymemberopen == true or
			(isgrouphead _g) == false)
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-----------------------------------------------------
	--
	fn test5 =
	(
		print "test5 - Disassemble Assembly"
		a = assemblyMgr.assemble #(_boxes[3], _g) name:"a" classDesc:Dummy
		
		assemblyMgr.Disassemble a
		if ((isgroupmember _boxes[1]) == false or
			_g.assemblyhead == true or
			_g.assemblyheadopen == true or
			_g.assemblymember == true or
			_g.assemblymemberopen == true or
			(isgrouphead _g) == false)
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-----------------------------------------------------
	--
	fn test6 =
	(
		print "test6 - Explode Assembly"
		a = assemblyMgr.assemble #(_boxes[3], _g) name:"a" classDesc:Dummy
		
		assemblyMgr.Explode a
		if ((isgroupmember _boxes[1]) == false or
			_g.assemblyhead == true or
			_g.assemblyheadopen == true or
			_g.assemblymember == true or
			_g.assemblymemberopen == true or
			(isgrouphead _g) == false)
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-----------------------------------------------------
	--
	fn test7 =
	(
		print "test7 - Detach Groups"
		a = assemblymgr.assemble #(_boxes[3], _g) name:"a" classDesc:Dummy
		assemblyMgr.open a
		assemblyMgr.detach _g 
		if (_g.assemblyhead == true or
			_g.assemblymember == true or
			(isgrouphead _g) == false) 
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-----------------------------------------------------
	--
	fn test8 =
	(
		print "test8 - Detach open assembly member (NO)"
		a = assemblymgr.assemble #(_boxes[3], _g) name:"a" classDesc:Dummy
		assemblyMgr.open a
		max group detach _boxes[3]
		if (_boxes[3].assemblymember == false)
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-----------------------------------------------------
	--
	fn test9 =
	(
		print "test9 - Ungroup assembly with inner Group (NO)"
		a = assemblymgr.assemble #(_boxes[3], _g) name:"a" classDesc:Dummy
		ungroup a
		if (a.assemblyhead == false or
			_boxes[3].assemblymember = false or
			(isgroupmember _boxes[1]) = false or
			_g.assemblymember == false)
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-----------------------------------------------------
	--
	fn test10 =
	(
		print "test10 - Open assembly with inner Group (NO)"
		a = assemblymgr.assemble #(_boxes[3], _g) name:"a" classDesc:Dummy
		setgroupopen a
		if (a.assemblyheadopen == false or
			_boxes[3].assemblymemberopen = false or
			(isgroupmemberopen _boxes[1]) = true or
			_g.assemblymemberopen == true)
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-----------------------------------------------------
	--
	fn test11 =
	(
		print "test11 - Explode assembly with inner Group (NO)"
		a = assemblymgr.assemble #(_boxes[3], _g) name:"a" classDesc:Dummy
		explodegroup a
		if (a.assemblyhead == false or
			_boxes[3].assemblymember = false or
			(isgroupmember _boxes[1]) = true or
			_g.assemblymember == true)
			then throw "FAILED\n"
			else print "PASSED\n"
	)

	-- Macro methods
	On isEnabled return true
	On isVisible return true

	On Execute Do	
	(
	    global createTestSuite

	  if createTestSuite == undefined do fileIn ((getdir #scripts) + "/Startup/MxsUnitTest/MxsUnitTestFramework.ms")
		ts = createTestSuite "amGroupsInAssemblies" setupUT teardownUT
		ts.addTest test1
		ts.addTest test2
		ts.addTest test3
		ts.addTest test4
		ts.addTest test5
		ts.addTest test6
		ts.addTest test7
		-- add more tests
		ts.run()
	)
)
