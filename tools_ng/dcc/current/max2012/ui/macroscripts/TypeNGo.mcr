macroscript TypeNGo category:"RS Wildwest"
(
	-- Macro only loads script if it hasn't been loaded already
	--	(the script automatically shows the TypeNgo dialog)
	if (TypeNgo_UI == undefined) then 
	(
		filein (::RsConfigGetWildWestDir()+"/script/3dsmax/Evaluation/TypeNgo/TypeNgo.ms")
	)
	else 
	(
		TypeNgo_UI.Show()
	)
)