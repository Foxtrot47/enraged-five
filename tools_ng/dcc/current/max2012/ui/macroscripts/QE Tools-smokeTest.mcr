macroScript smokeTest
	category:"QE Tools"
	tooltip:"smokeTest"
	buttontext:"smokeTest"
(
	global g_smokeTestData
	if g_smokeTestData == undefined then 
		messagebox "smokeTest.ms has not been executed"
	else 
		if not g_smokeTestData.up do
		(	-- test setIniSetting if running gMax
			local setINISettingError = false
			local fname = (getdir #maxdata)+"smokeTestLog.err"
			deleteFile fname
			if (productAppID == #gMax) do
			(	try (setINIsetting fname "gMax SmokeTest" "successful write" "true")
				catch setINISettingError = true
				if setINISettingError do throw "Error using setINIsetting"
			)
			g_smokeTestData.floater = NewRolloutFloater ("Smoke Test - v" + g_smokeTestData.version) g_smokeTestData.width g_smokeTestData.height
			AddRollout g_smokeTestData.theRollout g_smokeTestData.floater
		)
)
