macroScript BlockInstCreation_UnitTest
	ButtonText:"BlockInstCreation_UnitTest"
	Category:"Qe Tools - Unit Tests Acad Blocks" 
	internalCategory:"Qe Tools - Unit Tests Acad Blocks" 
	Tooltip:"BlockInstCreation Unit Test" 
	-- Needs Icon
	--Icon:#("Max_edit_modifiers",1)
(	
	-- Unit test fixture setup
	fn setupUT =
	(
  	  -- Create a hierarchy of nodes.
	  -- Boxes will represent block composites and spheres will be leaves
	  -- We'll create this structure:
	  -- block_1
	  --   block_2
	  --   leaf_1
	  --   leaf_2
	  -- block_2
	  --   leaf_1
	  --   leaf_3
	  -- resetmaxFile #noPrompt
	  persistent global gBlock_1 = #() -- holds node hierarchy representing block_1
	  append gBlock_1 (box())
	  append gBlock_1 (sphere radius: 10 pos:[10, 0, -10])
	  append gBlock_1 (sphere radius: 5 pos:[10, 0, -30])
	)

	-- Unit test fixture teardown
	fn teardownUT =
	(
		--print "Debug: teardownUT"
		-- add here fixture teardown code
	)
	
	-- Unit tests
	fn testCreateBlock =
	(
	  print "testCreateBlock - calling blockMgr to make blocks out of nodes"
	  if (undefined == blockMgr.MakeCompositeBlockRef(gBlock_1[1]))
	    then print "CompositeBlockRef Creation FAILED\n"
	  if (undefined == blockMgr.MakeLeafBlockRef(gBlock_1[2]))
	    then print "LeafBlockRef Creation FAILED\n"
 	  if (undefined == blockMgr.MakeLeafBlockRef(gBlock_1[3]))
	    then print "LeafBlockRef Creation FAILED\n"
	  if (false == (isProperty gBlock_1[1] "IBlockRefComponent"))
	    then print "CompositeBlockRef Creation FAILED\n"
      if (false == gBlock_1[1].IBlockRefComponent.AddComponent(gBlock_1[2]))
		then print "AddComponent FAILED\n"
      if (false == gBlock_1[1].IBlockRefComponent.AddComponent(gBlock_1[3]))
		then print "AddComponent FAILED\n"
	)
	fn testTHAT =
	(
		--print "Debug: testTHAT"
		-- add here test code that uses test fixture
	)


	-- Macro methods
	On isEnabled return true
	On isVisible return true

	On Execute Do	
	(
    global createTestSuite
	  if createTestSuite == undefined do fileIn "$scripts/../../msxunittest/testsuite.ms"
	  ts = createTestSuite "BlockInstCreationUnitTest" setupUT teardownUT
		ts.addTest testCreateBlock
		-- ts.addTest testTHAT
		-- add more tests
		ts.run()
	)
)
