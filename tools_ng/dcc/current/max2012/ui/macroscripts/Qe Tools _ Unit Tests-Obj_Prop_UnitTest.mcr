macroScript Obj_Prop_UnitTest
	ButtonText:"Modifier UI simplication Unit Test"
	Category:"Qe Tools - Unit Tests" 
	internalCategory:"Qe Tools - Unit Tests" 
	Tooltip:"Modifier UI simplication Unit Test" 
(	

	--##################################################
	-- Expected values
	--##################################################

	-- Smooth modidifer	
	fn getSmoothProperties =
	(
		properties = #("autoSmooth", true, true, \ -- Auto smooth
					   "preventIndirect", false, true, \ -- Prevent indirect
					   "threshold", (30.0 as string), true, \ -- Threshold
					   "smoothingBits", 0, true) -- Smoothing bits
					   
		return properties
	)
	
	-- Unit test fixture setup
	fn setupUT =
	(
		resetMaxFile #noPrompt
	)

	-- Unit test fixture teardown
	fn teardownUT =
	(
	)
	
	--##################################################
	-- Helper functions
	--##################################################
	
	fn fetchSmoothProperties mod properties =
	(
		append properties mod.autoSmooth
		append properties mod.preventIndirect
		append properties (mod.threshold as string)
		append properties mod.smoothingBits
	)

	fn validateProperties experimental expected =
	(
		if experimental.count != (expected.count / 3) then
		(
			format "Incorrect number of properties\n"
			throw "Error"
		)
		else
		(
			for i in 1 to experimental.count do
			(
				if experimental[i] != expected[(i * 3) - 1] and expected[i * 3] do
				(
					format "Incorrect Value for \"%\"\n" expected[i]
					format "%(experimental) != %(expected)\n" experimental[i] expected[(i * 3) - 1]
					throw "Error"
				)
			)
		)
	)
	
	fn changeValue valueName value properties =
	(
		for i in 1 to properties.count by 3 do
		(
			if properties[i] == valueName do
			(
				properties[i + 1] = value
				return true
			)
		)
		
		throw "Error"
	)
	
	--##################################################
	-- Unit tests
	--##################################################
	
	fn createSmooth =
	(
		box1 = box name:#box1
		smooth1 = smoothModifier()
		addModifier box1 smooth1
		
		index = modPanel.getModifierIndex box1 box1.smooth
		smooth1 = box1.modifiers[index]
		
		expected = getSmoothProperties()		
		experimental = #()
		fetchSmoothProperties smooth1 experimental
		validateProperties experimental expected
	)

	fn loadSmooth =
	(
		box1 = box name:#box1
		smooth1 = smoothModifier()
		addModifier box1 smooth1

		box2 = box name:#box2 pos:[20, 20, 20]
		smooth2 = smoothModifier()
		smooth2.autoSmooth = false
		smooth2.preventIndirect = true
		smooth2.threshold = 15.0
		smooth2.smoothingBits = 2
		addModifier box2 smooth2
	
		saveMaxFile "toload.max"
		
		resetMaxFile #noPrompt
		
		loadMaxFile "toload.max"
		
		box1 = getNodeByName "box1" exact:true
		index = modPanel.getModifierIndex box1 box1.smooth
		smooth1 = box1.modifiers[index]
		
		expected = getSmoothProperties()		
		experimental = #()
		fetchSmoothProperties smooth1 experimental
		validateProperties experimental expected
		
		box2 = getNodeByName "box2" exact:true
		index = modPanel.getModifierIndex box2 box2.smooth
		smooth2 = box2.modifiers[index]
		
		expected = getSmoothProperties()
		changeValue "autoSmooth" true expected
		changeValue "preventIndirect" true expected
		changeValue "threshold" (15.0 as string) expected
		changeValue "smoothingBits" 2 expected

		experimental = #()
		fetchSmoothProperties smooth2 experimental
		validateProperties experimental expected
	)
	
	fn mergeSmooth =
	(
		box1 = box name:#box1
		smooth1 = smoothModifier()
		addModifier box1 smooth1

		box2 = box name:#box2 pos:[20, 20, 20]
		smooth2 = smoothModifier()
		smooth2.autoSmooth = false
		smooth2.preventIndirect = true
		smooth2.threshold = 15.0
		smooth2.smoothingBits = 2
		addModifier box2 smooth2
	
		saveMaxFile "tomerge.max"
		
		resetMaxFile #noPrompt
		
		loadMaxFile "tomerge.max"
		
		box1 = getNodeByName "box1" exact:true
		index = modPanel.getModifierIndex box1 box1.smooth
		smooth1 = box1.modifiers[index]
		
		expected = getSmoothProperties()		
		experimental = #()
		fetchSmoothProperties smooth1 experimental
		validateProperties experimental expected
		
		box2 = getNodeByName "box2" exact:true
		index = modPanel.getModifierIndex box2 box2.smooth
		smooth2 = box2.modifiers[index]
		
		expected = getSmoothProperties()
		changeValue "autoSmooth" true expected
		changeValue "preventIndirect" true expected
		changeValue "threshold" (15.0 as string) expected
		changeValue "smoothingBits" 2 expected

		experimental = #()
		fetchSmoothProperties smooth2 experimental
		validateProperties experimental expected
	)
	
	-- Macro methods
	On isEnabled return true
	On isVisible return true

	On Execute Do	
	(
		global createTestSuite
	  if createTestSuite == undefined do fileIn ((getdir #scripts) + "/../../MxsUnitTest/MxsUnitTestFramework.ms")
		ts = createTestSuite "Modifier_UI_Simplification_UnitTest" setupUT teardownUT
		ts.addTest createSmooth
		ts.addTest loadSmooth
		ts.addTest mergeSmooth
		ts.run()
	)
)
