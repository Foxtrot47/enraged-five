-- 
-- File:: rockstar/export/settings.ms
-- Description:: Configuration data functions.
--
-- Author:: Greg Smith <greg@rockstarnorth.com>
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 1 March 2005
--
-- DHM 25 November 2009
-- With the tools framework we should really stop duplicating the project 
-- data into the RsConfigInfo struct and use the new Project struct directly
-- altering the functions in here to return data based on that.
--

-- NealC 12/08/2010
-- I've moved all the extra functions out of this file into settings_func.ms, 
-- which is loaded before this script on startup.
-- Lots of scripts load settings.ms, and the only bit that ever needs to be redefined is this rollout.

-- AJM 25/08/2010
-- Made the Bin and Project Root edittext objects readonly, and removed their events which 
-- previously allowed people to update their local.xml file for these two paths.
-- Also updated to get the other output paths to properly save when changed.
--

-- local "constants", used as labels for the checkboxes in settings.ms, and to check the targets within the local.xml
-- those matches the platform's Enum in RSG.Platform.Platform, in case and name
TargetWin32 	= "Win32"
TargetPS3 		= "PS3"
TargetXbox360 	= "Xbox360"
TargetWin64 	= "Win64"
TargetPS4 		= "PS4"
TargetXboxOne 	= "XboxOne"

rollout RsSettingsRoll "Settings"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	group "Branch Selection"
	(
		dropdownlist lstBranchSelect
	)
	
	group "Project Platforms"
	(
		checkbox chkWin32 	TargetWin32  	across:3
		checkbox chkPS3 	TargetPS3  
		checkbox chkXbox360 TargetXbox360 
		
		checkbox chkWin64 	TargetWin64 	across:3
		checkbox chkPS4 	TargetPS4
		checkbox chkXboxOne TargetXboxOne
	)
	group "Project Directories"
	(
		edittext edtProjectRootFolder "Root:" readOnly:true
		edittext edtBinFolder "Bin:" readOnly:true
		edittext edtStreamFolder "Stream:"	readOnly:true
	)
	group "Export Process"
	(
		checkbox chkForceTexture "Force Texture Export"
		checkbox chkDontStopForErrors "Don't stop for errors" visible:(RsUserIsSuperUser())
	)
	--////////////////////////////////////////////////////////////
	-- functions
	--////////////////////////////////////////////////////////////
	
	fn UpdateProjectPaths = 
	(
		edtProjectRootFolder.text = ( RsConfigGetProjRootDir() )
		edtBinFolder.text = ( RsConfigGetBinDir() )
		edtStreamFolder.text = ( RsConfigGetCacheDir() )
		
		CheckTargetEnabled chkWin32 	TargetWin32
		CheckTargetEnabled chkPS3 		TargetPS3
		CheckTargetEnabled chkXbox360 	TargetXbox360
		
		CheckTargetEnabled chkWin64 	TargetWin64
		CheckTargetEnabled chkPS4 		TargetPS4
		CheckTargetEnabled chkXboxOne 	TargetXboxOne
		
		chkForceTexture.checked = false
	)
	
	fn RefreshBranchDropdown =
	(
		-- Update possible Branches values
		local branchesEnumerator = gRsProject.Branches.GetEnumerator()
		local lstBranchesNames = #()

		-- no collect for all collection types, so iterate
		while branchesEnumerator.MoveNext() do
		(
			append lstBranchesNames branchesEnumerator.Current.Key
		)
		
		lstBranchSelect.Items = lstBranchesNames
		lstBranchSelect.Selection = findItem lstBranchSelect.items (gRsBranch.name)
	)
	
	-- refreshes the paths for the current project, repopulates the dropdown list for branches
	fn Refresh =
	(
		UpdateProjectPaths()
		RefreshBranchDropdown()
	)
		
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- initialisation of rollout
	--------------------------------------------------------------
	on RsSettingsRoll open do
	(		
		Refresh()
	)

	on RsSettingsRoll rolledUp down do 
	(
		RsSettingWrite "rsSettings" "rollup" (not down)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkForceTexture changed newVal do
	(
		rexSetForceTextureExport newVal
	)
	
	--------------------------------------------------------------
	-- changed value of pc config checkbox
	--------------------------------------------------------------	
	on chkWin32 changed newVal do
	(
		UpdateLocalTargets TargetWin32 newVal
	)

	--------------------------------------------------------------
	-- changed value of ps3 config checkbox
	--------------------------------------------------------------	
	on chkPS3 changed newVal do
	(
		UpdateLocalTargets TargetPS3 newVal
	)

	--------------------------------------------------------------
	-- changed value of xenon config checkbox
	--------------------------------------------------------------	
	on chkXbox360 changed newVal do
	(
		UpdateLocalTargets TargetXbox360 newVal
	)
	
	on chkWin64 changed newVal do
	(
		UpdateLocalTargets TargetWin64 newVal
	)
	
	on chkPS4 changed newVal do
	(
		UpdateLocalTargets TargetPS4 newVal
	)
	
	on chkXboxOne changed newVal do
	(
		UpdateLocalTargets TargetXboxOne newVal
	)
	
	on chkDontStopForErrors changed val do 
	(
		if (RsUserIsSuperUser()) then
			RsDontStopForErrors = val
	)
	
	--------------------------------------------------------------
	-- changed value of branch selection
	--------------------------------------------------------------
	on lstBranchSelect selected idx do
	(
		-- reload project and branch
		local maps = for map in RsMapGetMapContainers doingExport:true where map.is_exportable() collect map		
		local branchName  = lstBranchSelect.items[lstBranchSelect.Selection]
		
		if ( branchName != gRsBranch.name) do
		(
			RsContentTree.SetupProjectAndBranchGlobals dlcMaxFile:(maxfilepath + maxfilename) requestedBranchName:branchName
			RsContentTree.SyncContentTree()
			RsContentTree.LoadContentTree forceReload:true	
            RsSetRexProceduralList() -- used to reload the procedural list when between branches/core, as they have different procedural.meta files
			Refresh()
		)
	)	
)
-- rockstar/export/settings.ms
