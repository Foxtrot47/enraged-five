--
-- File:: rockstar\export\ProjectConfig.ms
-- Desc:: Asset Pipeline 3 content-tree methods.
--
-- Author:: Kevin Weinberg
-- Date:: 8 April 2013
--

filein "rockstar/export/settings.ms"
filein "pipeline/util/p4.ms"

struct RsProjectMapConfigStruct
(
	root = undefined,
	
	fn Load =
	(
		local metadataFile =  ((RsConfigGetAssetsDir core:true) + "maps/MapSectionMetadata.xml")
		local depotVersion = gRsPerforce.local2depot metadataFile
		if not (gRsPerforce.sync depotVersion) then
		(
			local errorMsg = stringStream ""
			format "Metadata file: % could not be synched from Perforce!" metadataFile to:errorMsg
			gRsULog.LogWarning errorMsg
			false
		)
		
		if RsFileExists metadataFile then
		(
			doc = dotNetObject "System.Xml.XmlDocument"
			doc.load metadataFile
			root = doc.documentElement
			true
		)
		else
		(
			gRsULog.LogError ("Metafile doesn't exist: "+metadataFile as string)
			false
		)
	),
	
	fn GetTextureDirectorySetting mapName = 
	(
		local nodes = #()
		if (undefined!=root) then
			root.SelectNodes ("//CMapSectionMetadata/CMapSection[@name='" + mapName + "']/TextureDirectory[@path]" )
		
		if nodes.count == 0 then undefined
		else 
		(
			local pathSetting = (nodes.item[0].GetAttribute "path") 
			(substituteString pathSetting "\\"  "/")
		)
	),
	
	fn GetTexturePrefixSetting mapName = 
	(
		local nodes = #()
		if (undefined!=root) then
			root.SelectNodes ("//CMapSectionMetadata/CMapSection[@name='" + mapName + "']/TexturePrefix[@prefix]" )
		if nodes.count == 0 then undefined
		else 
		(
			local pathSetting = (nodes.item[0].GetAttribute "prefix") 
			(substituteString pathSetting "\\"  "/")
		)
	),
	
	fn GetTextureTXDPrefixSetting mapName = 
	(
		local nodes = #()
		if (undefined!=root) then
			root.SelectNodes ("//CMapSectionMetadata/CMapSection[@name='" + mapName + "']/TXDPrefix[@prefix]" )
		if nodes.count == 0 then undefined
		else 
		(
			local pathSetting = (nodes.item[0].GetAttribute "prefix") 
			(substituteString pathSetting "\\"  "/")
		)
	)
)

if ::RsProjectMapConfig == undefined then 
(
	global RsProjectMapConfig = RsProjectMapConfigStruct()
	if ( RsProjectMapConfig.Load() == false ) then
	(
		RsProjectMapConfig = undefined
		gRsUlog.LogError ("RsProjectMapConfig failed to load.")
	)
)
