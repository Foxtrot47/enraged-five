--
-- File:: rockstar/export/cutscenelocation.ms
-- Description:: Rockstar Cutscene Loaction Exporter
--
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com
-- Date:: 18/9/2006
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
--filein "pipeline/util/file.ms" -- loaded on startup by startup.ms
filein "rockstar/export/settings.ms"

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout RsCutSceneLocationRoll "Cutscene Location Exporter"
(

	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Export_Cutscene_Locations" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnExportAll "Export All"
	button btnExportSelected "Export Selected"
	
	
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	fn ReShadeMaterials obj matIdCount = (
		faceCount = obj.faces.count
		
		obj.material = multimaterial numsubs:matIdCount
		mm = obj.material
		matCount = mm.materialidlist.count
		
		
		
		increm = 100.0 / matCount
		
		for j = 1 to matCount do (
			submat = standardmaterial()
			greyness = (increm * j) + 50
			submat.diffuse = [greyness, greyness, greyness] as color
			
			mm.materiallist[j] = submat
			mm.materiallist[j].name = j as string
			j = j + 1
		)
		
	)

	fn SaveCutSceneLocation selSet maxfilename = (
		
		outpath = getSaveFilename caption:"Water File" types:"max file (*.max)|*.max"
		RsMakeSurePathExists outpath
		
		saveNodes selSet outpath
		return true
		
		
	)

	fn FindMilo lstObjectList = (
		
		
		for obj in lstObjectList do (
			
			if classof obj == Gta_MILO then (
				return obj
			)
			
			
		)
		return undefined
		
	)


	fn CollapseAllToMesh lstObjectList lstMeshObjs lstBogusObjects = (
		
		materialIdCount = 0
		for obj in lstObjectList do (
			
			if classof obj != GtaMloRoom then (
				objCopy = copy obj
				if convertToMesh objCopy == undefined then (
					--append lstBogusObjects objCopy
					delete objCopy
				)
				else (
					--materialIdCount += objCopy.material.materialidlist.count
					if classof objCopy != Gta_MILOTri then (
						objCopy.material = undefined
					
						append lstMeshObjs objCopy
					)
					else delete objCopy
				)
			)
		)
		
		return materialIdCount
	)
	
	fn BuildSingleMesh lstMeshObjs miloName = (
		
		rootobj = lstMeshObjs[1]
		rootobj.name = miloName
		
		
		
		select rootObj

		for i=2 to lstMeshObjs.count do (
			attach rootobj lstMeshObjs[i]
			i=i+1
		)
		return rootobj
	)
	

	fn ExportCutsceneLocation lstObjectList CsLocate = (
		lstMeshObjs = #()
		lstBogusObjects = #()
		lstSelSet = #()
		
		miloObj = FindMilo lstObjectList
		
		
		if miloObj != undefined then (
			benchmark = point()
			benchmark.name = miloObj.name + "_benchmark"
			benchmark.pos = miloObj.pos
			matIDCount = CollapseAllToMesh lstObjectList lstMeshObjs lstBogusObjects
			
			singleMesh = BuildSingleMesh lstMeshObjs CsLocate.name
			append lstSelSet singleMesh
			append lstSelSet benchmark
			
			print lstSelSet[1]
			
			--lstSelSet[1].position = [0,0,0]
			select lstSelSet[1]
			max unlink
			
			lstSelSet[1].parent = benchmark
			benchmark.position = [0,0,0]
			miloObjCpy = copy miloObj
			append lstSelSet miloObjCpy
			freeze miloObjCpy
			hide miloObjCpy
			if SaveCutSceneLocation lstSelSet CsLocate.name == false then messagebox "Failed to save CS location"
			delete miloObjCpy
			delete singleMesh
			delete benchMark
		) else (
			
			CollapseAllToMesh lstObjectList lstMeshObjs lstBogusObjects
			
			
			
			singleMesh = BuildSingleMesh lstMeshObjs CsLocate.name
			
			append lstSelSet singleMesh
			
			oldPos = lstSelSet[1].position
			
			
			worldPosDummy = Dummy position:oldPos
			select worldPosDummy
			freeze worldPosDummy
			hide worldPosDummy
			append lstSelSet worldPosDummy
			lstSelSet[1].position = [0,0,0]
			select lstSelSet[1]
			max unlink
			
			
			if SaveCutSceneLocation lstSelSet CsLocate.name == false then messagebox "Failed to save CS location"
			
			delete singleMesh
			delete worldPosDummy
			
		)
		messagebox "Export complete"
	
	)
	
	fn GetGridBounds CsLocate csMin csMax = (
		csMin.x = CsLocate.position.x - (CsLocate.grid.width / 2)
		csMin.y = CsLocate.position.y - (CsLocate.grid.length / 2)
		csMax.x = CsLocate.position.x + (CsLocate.grid.width / 2)
		csMax.y = CsLocate.position.y + (CsLocate.grid.length / 2)
	)
	
	fn BuildSelectionSets lstCsLocateObjs = (
		
		csMin = Point2 0 0
		csMax = Point2 0 0
		
		
		
		for CsLocate in lstCsLocateObjs do (
			
			GetGridBounds CsLocate csMin csMax
			
			
			
			mainselset = #()
			select $*
			
			for obj in selection do 
			(
				if (isEditPolyMesh obj) or (isKindOf obj Gta_MILO) or (isInternalRef obj) then 
				(
					if obj.transform.position.x < csMax.x and obj.transform.position.y < csMax.y and obj.transform.position.x > csMin.x and obj.transform.position.y > csMin.y then (
						--if (substring (obj.name.count - 5) 5) != "_milo" then (
						append mainselset obj
						--)
					)
				) else if isRefObj obj then (
					
					myVar = obj.GetSrcItem() as string
					
					
					
					if myVar != "Col Mesh" then (
						
						if obj.position.x < csMax.x and obj.position.y < csMax.y and obj.position.x > csMin.x and obj.position.y > csMin.y then (
							
							append mainselset obj
						
						)
					)
				)
			)
			--print mainselset
			ExportCutsceneLocation mainselset CsLocate
		)
		
	)
	
	fn GetCSLocationObjs lstCsLocateObjs = (
		select $*
		
		for obj in selection do (
			if classof obj == GtaCSLocate then (
				append lstCsLocateObjs obj
			)
		)
			
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	on btnExportAll pressed do (
		lstCsLocateObjs = #()
	
		GetCSLocationObjs lstCsLocateObjs
		BuildSelectionSets lstCsLocateObjs
	)
	
	on btnExportSelected pressed do (
		if selection.count != 1 then messagebox "Please select one object only."
		
		if classof selection[1] != GtaCSLocate then messagebox "Please select a Gta CS Locate object"
		else (
			BuildSelectionSets selection[1]
		)
	)

)

try CloseRolloutFloater RsCutsceneLocation catch()
RsCutsceneLocation = newRolloutFloater "Export Cutscene Locations" 180 110 100 206
addRollout RsCutSceneLocationRoll RsCutsceneLocation 