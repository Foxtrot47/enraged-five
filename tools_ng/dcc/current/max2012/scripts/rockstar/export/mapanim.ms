-- Rockstar Map Export
-- Rockstar North
-- 1/3/2005
-- by Greg Smith
-- Export a map sections animations into the game

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/globals.ms"
filein "pipeline/export/maps/mapgeom.ms"
filein "pipeline/util/statedAnim.ms"
filein "pipeline/util/scene.ms"

-----------------------------------------------------------------------------
-- Script Globals
-----------------------------------------------------------------------------

struct RsExportAnim (model, shaderindex, texmapindex, texmap)

global idxHasAnim = getattrindex "Gta Object" "Has Anim"
global idxHasUvAnim = getattrindex "Gta Object" "Has UvAnim"
global idxAnimLoops = getattrindex "Gta Object" "looped"
global idxStatedAnimGroupName = getattrindex "Gta Object" "groupName"
global gAllowDrawableAnalysis = true

global gRsAnimExportForcedRange = undefined

fn RsGetAnimatedBones obj sel = (

	--if classof obj == Editable_Mesh or classof obj == Editable_Poly then (
	--if classof obj == Editable_Mesh or classof obj == Point or classof obj == Biped_Object or classof obj == BoneGeometry then (
	if RsIsValidBoneForMap obj then (
	
		append sel obj
	)
	
	for childobj in obj.children do (
	
		RsGetAnimatedBones childobj sel
	)
)

rollout RsMapAnimation "Animation Export"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////	

	button btnExport "Export" width:100

	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	fn MapAnimSaveClipFile filename model theSkin = (
		
		--eBoneMask eBoneMask_Parse(const char *str, const eBoneMask defaultVal);
		-- enum eAnimPlayerFlag
		-- {
		APF_UNUSED_1 = 0
		APF_UNUSED_2 = 1
		APF_UNUSED_3 = 2
		APF_UNUSED_4 = 3
		APF_ISPLAYING = 4
		APF_ISLOOPED = 5
		APF_LOOP_1 = 6
		APF_LOOP_2 = 7
		APF_ISSYNCRONISED = 8
		APF_UNUSED_5 = 9
		APF_UNUSED_6 =10
		APF_UNUSED_7 = 11
		APF_UNUSED_8 = 12
		APF_IGNOREROTATION = 13
		APF_ISBLENDAUTOREMOVE = 14
		APF_ISFINISHAUTOREMOVE = 15
		APF_IGNORE_BLEND_VELOCITY_EXTRACTION = 16
		APF_ADDITIVE = 17
		APF_FACIAL = 18
		APF_GESTURE = 19
		APF_CUTSCENE = 20
		APF_UPPERBODYONLY = 21
		APF_SKIP_NEXT_UPDATE_BLEND = 22
		APF_INCAR = 23
		APF_FOOTFALL = 24
		APF_COPY = 25
		APF_BLOCK_IK = 26
		APF_BLOCK_LEG_IK = 27
		APF_BLOCK_HEAD_IK = 28
		APF_FOLLOW_PED_ROOT_BONE = 29
		APF_ADD_TO_POINTCLOUD = 30
		APF_USE_DEFAULT_RCB = 31
		-- };

		if RsIsFileReadOnly filename then (

			gRsUlog.LogError (filename + " is read only") context:model
			return false
		)

		clipEditor filename
		clipLoad filename -- wtf, why isn't the Editor function doing this...
		local flags = 0
		if ("Gta Object"==(getattrclass theSkin) and getattr theSkin idxAnimLoops) then
		(
--			print "Object has looped animation."
			flags = bit.or flags (bit.shift 1 APF_ISLOOPED)
		)
		
		clipSetProperty "AnimPlayerFlags" (clipGetIntId()) "AnimPlayerFlags" flags

		local isRayFire = gRsStatedAnimTool.IsStatedAnimGroupMember theSkin
		if isRayFire then
		(
			gRsStatedAnimTool.loadAnim theSkin
			local compressionFilename = gRsStatedAnimTool.getCompression()
			while matchPattern compressionFilename pattern:"*.txt" do
				compressionFilename = substring compressionFilename 1 (compressionFilename.count-4)
			append compressionFilename ".txt"
			gRsUlog.LogMessage ("Using compression template "+compressionFilename+" for animated object "+theSkin.name+".") context:theSkin
			clipSetProperty "Compressionfile_DO_NOT_RESOURCE" (clipGetStringId()) "Compressionfile_DO_NOT_RESOURCE" compressionFilename
			clipSetProperty "LinearCompression_DO_NOT_RESOURCE" (clipGetIntId()) "LinearCompression_DO_NOT_RESOURCE" (UseLinearCompression=1)
		)
		else
			clipSetProperty "Compressionfile_DO_NOT_RESOURCE" (clipGetStringId()) "Compressionfile_DO_NOT_RESOURCE" "Default_Compress.txt"
		
		idxRandomiseStartPhase = getattrindex "Gta Object" "RandomiseStartPhase"
		idxRandomiseRate = getattrindex "Gta Object" "RandomiseRate"
		
		if  getattr theSkin idxRandomiseStartPhase then (
			idxMinPhase = getattrindex "Gta Object" "MinPhase"
			idxMaxPhase = getattrindex "Gta Object" "MaxPhase"
			
			minPhase = getattr theSkin idxMinPhase
			maxPhase = getattr theSkin idxMaxPhase
			
			if maxPhase >= minPhase then (
				clipSetProperty "RandomiseStartPhase" (clipGetFloatId()) "MinPhase" minPhase (clipGetFloatId()) "MaxPhase" maxPhase 
			)
		)
		
		if  getattr theSkin idxRandomiseStartPhase then (
			idxMinRate = getattrindex "Gta Object" "MinRate"
			idxMaxRate = getattrindex "Gta Object" "MaxRate"

			minRate = getattr theSkin idxMinRate
			maxRate = getattr theSkin idxMaxRate

			if maxRate >= minRate then (
				clipSetProperty "RandomiseRate" (clipGetFloatId()) "MinRate" minRate (clipGetFloatId()) "MaxRate" maxRate 
			)
		)
		
		clipsave filename
		
		if isRayFire then
		(
			local toolsAnimFolder = (RsConfigGetToolsBinDir core:true) + "/anim/"
			local exprExe = toolsAnimFolder + "makeexpressions.exe"
			local clipEditExe = toolsAnimFolder + "clipedit.exe"
			local rayFreExprFile = (RsConfigGetCacheDir()) + "maps/"+RsMapName as string+"/expressions/rayFire_"+model.name+".expr"
			RsMakeSurePathExists rayFreExprFile
			local res = DosCommand (exprExe+" -exprvisibilityscaleclip \""+filename+"\" -out \""+rayFreExprFile+"\"")
			if res!=0 then
			(
				gRsUlog.LogError ("Error while writing out rayfire expression file from clip \""+filename+"\"")
				return false
			)
			if doesFileExist rayFreExprFile then -- valid case if not exists. Just no visibility track present, I guess.
			(
				local res = DosCommand (clipEditExe+" -clip \""+filename+"\" -out \""+filename+"\" -converttype clipanimationexpression -clipexpression \""+rayFreExprFile+"\"")
				if res!=0 then
				(
					gRsUlog.LogError ("Error while updating the clip file \""+filename+"\" with the formerly written out rayfire expression file \""+rayFreExprFile+"\"")
					return false
				)
			)
		)
		
		true
	)
	
	--------------------------------------------------------------------------
	-- Creates Asset Pack XML
	--------------------------------------------------------------------------	
	fn CreateAnimAssetPack animList = (
		
		-- We do not create Asset Pack XML for empty content.
		if ( 0 == animList.Count ) then
			return true
		
		local xmlFileName = RsConfigGetScriptDir() + RsMapName + "/assetpack_animation.xml"
		RsMakeSurePathExists xmlFileName

		local dirTargetFolder = (RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/")
		local animationAssetPackFile = AP3AssetFile()
		animationAssetPackFile.Initialize filename:xmlFileName
		
		local clipFilename = dirTargetFolder + "/" + RsMapName + ".icd.zip"
		local clipZipElement = ( animationAssetPackFile.AddZipArchive clipFilename )
		
		-- Loop through our animation dictionaries.
		local animationDirectory = RsConfigGetAssetsDir() + "/maps/anims/" + RsMapName
		for exportItem in animList do (
			
			animationAssetPackFile.AddFile clipZipElement exportItem
		)
		
		result = true
		-- Write out AP3 Asset Pack Schedule file.
		animationAssetPackFile.WriteXmlFile xmlFileName
		RsMapAddContent RsMapName xmlFileName

		if ( not animationAssetPackFile.ValidateXml() ) then
		(
			gRsULog.LogError "Failed to create TXD Asset Pack XML.  See errors above."
			result = false
		)
		result
	)
	
	fn AppendAnimatedTexturesFromShader exportList model mtl shaderindex modelTexMaps = 
	(
		local texmaplist = #()
		RsGetMainTexMapsFromMaterial model mtl texmaplist firstpass:false

		for i = 1 to texmaplist.count do 
		(
			texmap = texmaplist[i]

			if classof texmap == Bitmaptexture and rexIsTexMapAnimated texmap then 
			(
				local modelTexIndex = findItem modelTexMaps texmap
				gRsUlog.LogMessage (mtl as string+" has animimated texture at index "+i as string)
				append exportList (RsExportAnim model shaderindex (modelTexIndex-1) texmaplist[i])
			)
		)
	)
	
	fn GetDrawableMaterialsRec obj mtl mtls =
	(
		if Multimaterial==(classof mtl) then
		(
			local drawableMtlIDs = (RsGetMatIdsUsedByObj obj) as array
			for mtlID in drawableMtlIDs do
			(
				local mtlIndexIndex = findItem mtl.materialIDList mtlID
				if 0!=mtlIndexIndex then
					GetDrawableMaterialsRec obj mtl.materiallist[mtlIndexIndex] mtls
			)
		)
		else if Rage_Shader==(classof mtl) then
		(
			appendIfUnique mtls mtl
		)
	)
	fn GetDrawableModelMaterialsRec root mtls = 
	(
		if "Gta Object"==(getAttrClass root) then
		(
			GetDrawableMaterialsRec root root.material mtls
		)
		
		local allDrawableModels = RsLodDrawable_GetAllModels root
		for drwblChild in allDrawableModels where drwblChild!=root do
		(
			GetDrawableModelMaterialsRec drwblChild mtls
		)
		for drwblChild in root.children do
		(
			GetDrawableModelMaterialsRec drwblChild mtls
		)
	)
	fn GetDrawableModelMaterials obj = 
	(
		local mtls = #()
		obj = GetExportRootNode obj
		GetDrawableModelMaterialsRec obj mtls
		--return
		mtls
	)

	-- AJM: Added in 'deleteCache' parameter as when doing export selected it was
	-- deleting other anim/clip files for other stuff not currently being exported
	fn ExportAnims models deleteCache:true changelistNum:undefined = 
	(
		RsMapExportCancelled = false
		
		currAnimRange = animationRange
		rootfolder = (RsConfigGetAssetsDir() + "maps/anims/" + RsMapName + "/")
		gRsPerforce.sync (rootfolder+"...")
		RsMakeSurePathExists(rootfolder+"test.txt")
		currsel = #()
		currsel = currsel + selection
	
		exportList = #()
		modelsin = #()
		lodmodelsin = #()
		modelsin = modelsin + models
		
		if deleteCache do
		(
			RsDeleteFiles (rootfolder + "*.anim")
			RsDeleteFiles (rootfolder + "*.clip")
		)

		-- find the animated objects
		
		if (modelsin = RsMapGeometry.RsFilterExportModels modelsin) == false then return false
		
		if gAllowDrawableAnalysis then
		(
			local allDrawbleParts = #()
			for m in modelsin do (join allDrawbleParts (RsLodDrawable_GetAllModels m))
			modelsin = makeUniqueArray allDrawbleParts
		)

		progressStart "setting up animation"
	
		local animSetup = "Animation Setup"
		gRsUlog.ProfileStart animSetup
		
		

		for i = 1 to modelsin.count while (RsMapProgress i modelsin.count) do 
		(			
			model = modelsin[i]


			local idxNewDLC = getattrindex "Gta Object" "New DLC"
			local idxTXDDLC = getattrindex "Gta Object" "TXD DLC"

			local isDLCObj = (getAttr model idxNewDLC) or (getAttr model idxTXDDLC)

			-- model animation

			if getattrclass model == "Gta Object" and getattr model idxHasAnim then (
	
				if RsIsObjectAnimated model then (

					append exportList (RsExportAnim model -1 -1 undefined)
				)
			)

			if getattrclass model == "Gta Object" and getattr model idxHasUvAnim then 
			(
				-- Bail if we detect a dlc project but not a dlc tagged object
				if( RsIsDLCProj() ) then 
				(
					if not isDLCObj then 
					(
						messagebox ("Found obj with uv anim which doesn't have a Dlc tag and we are exporting dlc: " + (model.name))
						continue
					)
				)
				
				local texmaplist = #()

				if not gAllowDrawableAnalysis then
				(
					local mm = model.material
					RsGetMainTexMapsFromMaterial model mm texmaplist
					-- uv animation
					if Multimaterial==(classof mm) then
					(
						local usedMtlIDs = (RsGetMatIdsUsedByObj model) as array
						local shaderindex = 0
						for mi in usedMtlIDs do
						(
							local mtlArrayIndex = findItem (mm.materialIDList) mi
							local subMtl = mm.materialList[mtlArrayIndex]
							if Rage_Shader==(classof subMtl) then
								AppendAnimatedTexturesFromShader exportList model subMtl shaderindex texmaplist
							shaderindex += 1
						)
					)
					else
						AppendAnimatedTexturesFromShader exportList model mm 0 texmaplist
				)
				else
				(
					local drawableMtls = GetDrawableModelMaterials model
					local animMeshes = #()
					if not RsIsObjectUVAnimatedRec model animMeshes:animMeshes then
						continue
					
					for animMesh in animMeshes do
					(
						local mm = animMesh.material
						RsGetMainTexMapsFromMaterial animMesh mm texmaplist firstpass:false
						-- uv animation
						if Multimaterial==(classof mm) then
						(
							local objectMtlIDs = (RsGetMatIdsUsedByObj animMesh) as array
							for mi in objectMtlIDs do
							(
								local mtlArrayIndex = findItem (mm.materialIDList) mi
								if 0==mtlArrayIndex then
								(
									gRsUlog.LogError ("Material index "+mi as string+" cound not be found in multi material attached to current object. Should never happen - talk to tools.") context:animMesh
									continue
								)
								
								local subMtl = mm.materialList[mtlArrayIndex]
								if Rage_Shader==(classof subMtl) then
								(
									local shaderindex = findItem drawableMtls subMtl
									print ("material "+(RstGetShaderName subMtl)+"has drawable shader index "+shaderindex as string)
									if shaderindex>0 then
									(
										AppendAnimatedTexturesFromShader exportList animMesh subMtl (shaderindex-1) texmaplist
									)
								)
							)
						)
						else
							AppendAnimatedTexturesFromShader exportList animMesh mm 0 texmaplist
					)
				)
			)
		)
		gRsUlog.ProfileEnd context:animSetup
		progressEnd()

		-- export the anim files
		
		local animExport = "Export Animation"
		gRsUlog.ProfileStart animExport
		
		

		if not RsMapExportCancelled do 
		(
			progressStart "exporting animation"
			local animFilesNames = #()
			for i = 1 to exportList.count while (RsMapProgress i exportList.count) do 
			(
				exportItem = exportList[i]

				model = exportItem.model
				local theSkin = model
				
				rexReset()
				local graphRoot = rexGetRoot()
				local rexSkel = rexAddChild graphRoot "skeleton" "Skeleton"
				rexSetPropertyValue rexSkel "SkeletonBoneTypes" (GetToleratedSkelBoneTypeClassIdString())
				
				if exportItem.shaderindex == -1 then (
		
					model = exportItem.model
					outname = model.name
					
					local newRange = undefined
					local exportChannels = "(translateX;translateY;translateZ;)(rotateX;rotateY;rotateZ;)"
					
					if gRsStatedAnimTool.IsStatedAnimGroupMember model then
					(
						append exportChannels "(scaleX;scaleY;scaleZ;)"
						gRsStatedAnimTool.loadAnim model
						local negRange = gRsStatedAnimTool.getAnimationRange()
						newRange = interval negRange.start negRange.end
						
						if RsHasVisTrackRec model then
							append exportChannels "(visibility;)"
					)
					append exportChannels "(projectData;projectData0;projectData1;projectData2;projectData3;projectData4;projectData5;projectData6;projectData7;)"

					rootbone = rexGetSkinRootBone model
					if rootbone != undefined then 
					(
						model = rootbone
					)
					-- if not already set by the rayfire tool
					if newRange==undefined then
						newRange = RsGetAnimRange model

					select model
		
					if ((newRange.end-newRange.start) as Integer)>0 then
					(
						animationRange = newRange
					)
					else
					(
						gRsUlog.LogError ("Object "+model.name+" is tagged with \"Has Anim\", but doesn't have animation on it.") context:model
					)
					
					local animAndClipPathnames = #((rootfolder + outname + ".anim"), (rootfolder + outname + ".clip"))
					
					gRsPerforce.add_or_edit animAndClipPathnames switches:#("-t", "binary+m")
					join animFilesNames animAndClipPathnames

					if(changelistNum != -1 and changelistNum != undefined) do
					(
						gRsPerforce.addToChangelist changelistNum animAndClipPathnames
					)
		
					animation = rexAddChild graphRoot outname "Animation" 
					rexSetPropertyValue animation "OutputPath" rootfolder
					rexSetPropertyValue animation "ChannelsToExport" exportChannels
					rexSetPropertyValue animation "AnimMoverTrack" "false"
					rexSetPropertyValue animation "AnimAuthoredOrient" "true"
					rexSetPropertyValue animation "AnimCompressionTolerance" "0.0005"
					rexSetPropertyValue animation "AnimExtraMoverTrack" "mover"
					rexSetPropertyValue animation "AnimLocalSpace" "true"
					rexSetPropertyValue animation "AnimExportSelected" "true"
					rexSetNodes animation
		
					selset = #()
					RsGetAnimatedBones model selset
					select selset
					
					RexExport false (gRsUlog.Filename())
					if not gRsUlog.Validate() then 
					(
						progressEnd()
						return false
					)
					
					clearSelection()
					
					MapAnimSaveClipFile ( rootfolder + outname ) model theSkin
				) else (
				
					model = exportItem.model
			
					select model
					
					animationRange = RsGetTexMapUVAnimRange exportItem.texmap
					
					-- NOTE
					-- Rex needs the texturemap index to look at the right tex map index, whereas runtime needs the shader index from the object. 
					-- That is actually accomplished by disassembling the name string of the animation, which needs no further comment.
					
					local shaderIndex = exportItem.shaderindex
					local overRideIndex = getUserProp model "overrideUVShaderIndex"
					if undefined!=overRideIndex then
						shaderIndex = overRideIndex
					if gAllowDrawableAnalysis then
					(
						local drawableHead = GetExportRootNode model
						overRideIndex = getUserProp drawableHead "overrideUVShaderIndex"
						if undefined!=overRideIndex then
							shaderIndex = overRideIndex
						uvName = (drawableHead.name + "_UV_" + (shaderIndex as string))
					)
					else
					(
						uvName = (model.name + "_UV_" + (shaderIndex as string))
					)
						
					gRsUlog.LogMessage ("model "+model.name+" exports uv anim "+uvName) context:model quiet:false
					local animAndClipPathnames = #((rootfolder + uvName + ".anim"), (rootfolder + uvName + ".clip"))
					gRsPerforce.add_or_edit animAndClipPathnames switches:#("-t", "binary+m")
					join animFilesNames animAndClipPathnames
		
					if(changelistNum != -1 and changelistNum != undefined) do
					(
						gRsPerforce.addToChangelist changelistNum animAndClipPathnames
					)
		
					animation = rexAddChild graphRoot uvName "Animation" 
					rexSetPropertyValue animation "OutputPath" rootfolder
					rexSetPropertyValue animation "ChannelsToExport" "(shaderSlideU;shaderSlideV;)"
					rexSetPropertyValue animation "AnimMoverTrack" "false"
					rexSetPropertyValue animation "AnimAuthoredOrient" "false"
					rexSetPropertyValue animation "AnimCompressionTolerance" "0.0005"
					rexSetPropertyValue animation "AnimExtraMoverTrack" "mover"
					rexSetPropertyValue animation "AnimLocalSpace" "true"
					rexSetPropertyValue animation "AnimUvAnimIndex" (exportItem.texmapindex as string)
					rexSetNodes animation
		
					clearSelection()
					
					RexExport false (gRsUlog.Filename())
		
					if not gRsUlog.Validate() then 
					(
						progressEnd()
						return false
					)
					MapAnimSaveClipFile ( rootfolder + uvName ) model theSkin
				)
			)		
			
			progressEnd()
			gRsUlog.ProfileEnd context:animExport

			local CreateAnimAssetPackMsg = "Create Anim Asset Pack"
			gRsUlog.ProfileStart CreateAnimAssetPackMsg
			CreateAnimAssetPack animFilesNames
			gRsUlog.ProfileEnd context:CreateAnimAssetPackMsg
		)
		
		selection = currsel
		animationRange = currAnimRange
			
		return (not RsMapExportCancelled)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	fn DoExportAnims = (
		
		if RsMapIscontainer then (
			containers = RsMapObjectGetTopLevelOpenContainers()
			for c in containers do (
				if ExportAnims c.children c.changelist == false then 
					return false
			)
		)
		else
		(
			maps = RsMapGetMapContainers()
			if (maps.count > 0) do
			(
				-- Should only be 1 'container' if a prop or interior file open
				if ExportAnims rootnode.children maps[1].changelist == false then 
					return false
			)
		)
		
		return true
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	on btnExport pressed do (

		-- Loop through all RsMapContainers for the scene.
		local maps = RsMapGetMapContainers()
		for map in maps do
		(
			if ( not map.is_exportable() ) then
				continue	

			if ( false == ( RsPreExport map ) ) then 
				return false
			if not DoExportAnims() then
				return false
			RsPostExport map
		)
		if (querybox "Do you wish to build the map(s)?") == true then (
			RsMapBuildImages maps
		)
	)

	on RsMapAnimation rolledUp down do 
	(
		RsSettingWrite "rsMapAnimation" "rollup" (not down)
	)
)
