filein "rockstar/util/anim.ms"
filein "pipeline/util/parameters.ms"

idxHasAnim = getattrindex "Gta Object" "Has Anim"
idxHasUvAnim = getattrindex "Gta Object" "Has UvAnim"

gRsMapValidTexturePathsAlreadyLoaded = false
gRsMapOptionalValidTexturePaths = #()

fn RsSetTransFlagsRec obj root: = 
(
	local recRoot = root
	if unsupplied==recRoot then
		recRoot = obj

	if HasControllerKeys obj.pos.controller then 
	(
		gRsUlog.LogDebug ("Sub object "+obj.name+" has an animated translation controller. Setting user prop on root "+recRoot.name) context:obj quiet:false
		setuserprop recRoot "exportTrans" true
		setuserprop obj "exportTrans" true
	)
	if hasProperty obj "rotation" and HasControllerKeys obj.rotation.controller then 
	(
		gRsUlog.LogDebug ("Sub object "+obj.name+" has an animated rotation controller. Setting user prop on root "+recRoot.name) context:obj quiet:false
		setuserprop recRoot "exportRotation" true
		setuserprop obj "exportRotation" true
	)
	if HasControllerKeys obj.scale.controller then 
	(
		gRsUlog.LogDebug ("Sub object "+obj.name+" has an animated scale controller. Setting user prop on root "+recRoot.name) context:obj quiet:false
		setuserprop recRoot "exportScale" true
		setuserprop obj "exportScale" true
	)
	if obj.visibility.isAnimated and HasControllerKeys obj.visibility.controller then 
	(
		gRsUlog.LogDebug ("Sub object "+obj.name+" has an animated visibility controller. Setting user prop on root "+recRoot.name) context:obj quiet:false
		setuserprop recRoot "exportVisibility" true
		setuserprop obj "exportVisibility" true
	)

	for childobj in obj.children do 
	(
		RsSetTransFlagsRec childobj root:recRoot
	)
)

-- recurse through lists and return on first found keys...
fn HasControllerKeys c =
(
	if c.keys.count>1 then 
		return true
	if isproperty c "count" then
		for subc = 1 to c.count do
			if HasControllerKeys c[subc] then
				return true
	return false
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsObjectAnimatedRec obj = (

	retval = false
	
	animRange = sNegInterval 0 -1
	RsGetControllerKeyRange obj &animRange
	
	if animRange.end>animRange.start then 
	(
		retval = true
	)
	
	for childobj in obj.children do (

		if RsIsObjectAnimatedRec childobj then (

			retval = true
		)
	)
	
	retval
	
)

fn RsIsValidBoneForMap rootbone = (

	return (0!=findItem gToleratedSkelBoneTypes (classof rootBone))
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsObjectAnimated obj needsAttribSet:true = 
(
	retval = false

	if (getattrclass obj=="Gta Object" and getattr obj idxHasAnim) or not needsAttribSet then 
	(
		rootbone = rexGetSkinRootBone obj
	
		if rootbone != undefined then 
		(
			obj = rootbone
			if not RsIsValidBoneForMap rootbone then 
			(
				gRsUlog.LogWarning (obj.name + " has skinned animation but invalid root bone, disabling") context:obj quiet:false
				if getattrclass obj == "Gta Object" then 
					setattr obj idxHasAnim false
				retval = false
			)
			else
			(
				retval = true
			)
		)
		RsSetTransFlagsRec obj
		return RsIsObjectAnimatedRec obj
	)
	
	retval
	
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsGetAnimRangeRec model animRange = (
	
	if RsIsValidBoneForMap model then (
	
		RsGetControllerKeyRange model &animRange
	)
	
	for childobj in model.children do (
	
		RsGetAnimRangeRec childobj animRange
	)
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsGetAnimRange model = (

	rootbone = rexGetSkinRootBone model
	
	if undefined==rootbone then rootbone = model

	retAnimRange = (sNegInterval 0 -1)
	
	RsGetAnimRangeRec rootbone retAnimRange
	
	if retAnimRange.start > retAnimRange.end then (
	
		tempVal = retAnimRange.start
		retAnimRange.start = retAnimRange.end
		retAnimRange.end = tempVal
	)
	
	retAnimRange = interval retAnimRange.start retAnimRange.end
	
	retAnimRange
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsGetTexMapUVAnimRange texmap = (
	
	animationRange = (interval 0f 3000f)
	currentAnimRange = animationRange
	
	beginUV = undefined
	endUV = undefined
	
	ctrlList = #()
	
	append ctrlList texmap.coords.u_offset.controller
	append ctrlList texmap.coords.v_offset.controller
	append ctrlList texmap.coords.W_Angle.controller
	
	for currentCtl in ctrlList do (
		
		if (currentCtl != undefined) and (currentCtl.keys.count > 1) then (
			
			if beginUV == undefined or beginUV > currentCtl.keys[1].time then (
			
				beginUV = currentCtl.keys[1].time
			)
			
			if endUV == undefined or endUV < currentCtl.keys[currentCtl.keys.count].time then (
			
				endUV = currentCtl.keys[currentCtl.keys.count].time
			)
		)	
	)
	if beginUV<0 or endUV<0 then
		gRsUlog.LogError ("texmap \""+texmap.name+"\"'s UVs have keys before 0, which is invalid and might lead to unexpected results.") context:texmap.name
	
	if beginUV != undefined and beginUV > currentAnimRange.start then (
	
		currentAnimRange.start = beginUV
	)
	
	if endUV != undefined and endUV < currentAnimRange.end then (
	
		currentAnimRange.end = endUV 
	)
	
	if currentAnimRange.start > currentAnimRange.end then (

		tempVal = currentAnimRange.start
		currentAnimRange.start = currentAnimRange.end
		currentAnimRange.end = tempVal
	)
	
	currentAnimRange
)

fn RsValidateUVShader model mtl = 
(
	if classof mtl != Rage_Shader then
	(
		gRsUlog.LogError ("Material "+mtl.name+" on model "+model.name+" has an animated texture, but is of type "+(classof mtl) as string) context:model
	)
	local preset = RstGetShaderName mtl
	if 0==(findItem RsValidUVShaderTypes preset) then
	(
		gRsUlog.LogError ("Material "+mtl.name+" on model "+model.name+" has an animated texture, but has incompatible shader preset "+preset as string+". https://devstar.rockstargames.com/wiki/index.php/Animated_UVs") context:model
	)
)

fn RsIsMtlUVAnimated model mtl =
(
	local foundAnim = false
	local texmaplist = #()
	RsGetMainTexMapsFromMaterial model mtl texmaplist firstpass:false
	for texmap in texmaplist while not foundAnim do 
	(
		if foundAnim == false and classof texmap == Bitmaptexture and rexIsTexMapAnimated texmap then 
		(
			foundAnim = true
		)
	)
	if foundAnim then
		RsValidateUVShader model mtl
	foundAnim
)

fn RsIsMeshUvAnimated model = 
(
	foundAnim = false

	if getattrclass model == "Gta Object" then (
	
		local mm = model.material
		if Multimaterial==(classof mm) then
		(
			local objectMtlIDs = (RsGetMatIdsUsedByObj model) as array
			for mi in objectMtlIDs do
			(
				local mtlArrayIndex = findItem (mm.materialIDList) mi
				if 0==mtlArrayIndex then
				(
					gRsUlog.LogError ("Material index "+mi as string+" cound not be found in multi material attached to current object. Should never happen - talk to tools.") context:model
					continue
				)
				local subMtl = mm.materialList[mtlArrayIndex]
				if RsIsMtlUVAnimated model subMtl then
					foundAnim = true
			)
		)
		else
		(
			if RsIsMtlUVAnimated model mm then
				foundAnim = true
		)
	)
	foundAnim
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsObjectUVAnimatedRec model animMeshes:#() = (

	local foundAnim = false

	if RsIsMeshUvAnimated model then
	(
		append animMeshes model
		foundAnim = true
	)
	
	for childobj in model.children do (
	
		local foundRet = RsIsObjectUVAnimatedRec childobj animMeshes:animMeshes
		
		if foundRet == true then 
			foundAnim = true
	)
	
	foundAnim
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsObjectUVAnimated model animMeshes:#() = (

	foundAnim = false

	if getattrclass model == "Gta Object" then (

		if getattr model idxHasUvAnim then (
	
			foundAnim = RsIsObjectUVAnimatedRec model animMeshes:animMeshes
			
			if foundAnim == false then 
			(
				gRsUlog.LogError ("Model "+model.name+" is tagged as having a UV animation, but the validation failed. Make sure all keys are t >= 0.") context:model
				setattr model idxHasUvAnim false
			)
		)
	)
	
	foundAnim 
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsObjectAnimatedAtAll model = (

	-- model animation
	foundAnim = false

	if RsIsObjectAnimated model then (

		foundAnim = true
	)

	-- uv animation
	if RsIsObjectUVAnimated model then (
	
		foundAnim = true
	)
	
	foundAnim
)

--
-- RsCheckTexturePath:
-- 	Check for correct texture path for this project on this file path.
--	If empty array is passed to 'sourcePaths', the generated sourcepath strings can be reused.
fn RsCheckTexturePath texmapPath sourcePaths:#() = 
(
	-- Return false if path is undefined, etc:
	if (not isKindOf texmapPath String) do return False

	if (sourcePaths.count == 0) do 
	(
		-- Core-project texture-source path:
		append sourcePaths (RsConfigGetTextureSourceDir Core:True)
		
		-- Check DLC texture-source path too, if a DLC project is active:
		if (RsIsDLCProj()) do 
		(
			append sourcePaths (RsConfigGetTextureSourceDir Core:False)
		)
	)
	
	-- Load the RSG.Pipeline.Project.xml file and see if any extra paths to allow.
	if (not gRsMapValidTexturePathsAlreadyLoaded) do
	(	
		local projectParams = RsPipelineParameters()
		local projectParamFile =  RsConfigGetToolsDir() + "etc/processors/RSG.Pipeline.Project.xml"
		local paramName = ("Extra Map Source Texture Paths [" + gRsBranch.name + "]")
		gRsMapOptionalValidTexturePaths = #()

		if true == projectParams.Load projectParamFile do 
		(
			if true == projectParams.params.containskey[paramName] do 
			(
				local mapSourcePathList = projectParams.params.item[paramName]
				for i = 0 to (mapSourcePathList.count - 1) do
				(
					if((mapSourcePathList.Item i) != undefined) do
					(
						local extraSourcePath = (RsMakeSafeSlashes (toLower (mapSourcePathList.Item i)))
						
						-- Might already have this path in the list
						appendIfUnique gRsMapOptionalValidTexturePaths extraSourcePath 
						if (findItem sourcePaths extraSourcePath == 0) do gRsULog.LogMessage ("Adding an extra valid map source texture path (" + extraSourcePath + ") which was specified in " + projectParamFile)
					)
				)					
				gRsMapValidTexturePathsAlreadyLoaded = true
			)	
		)			
	)		
	for optionalPath in gRsMapOptionalValidTexturePaths do (appendIfUnique sourcePaths optionalPath)
	
	local isValidPath = False
	
	for sourcePath in sourcePaths while (not isValidPath) do 
	(
		-- Find relative path from 'sourcePath' to 'texmapPath':
		local relPath = (pathConfig.convertPathToRelativeTo texmapPath sourcePath)
		
		-- Relative path begins with '.\' if 'texmapPath' is under 'sourcePath':
		isValidPath = (matchPattern relPath pattern:".\\*")
	)
	
	return isValidPath
)

--
-- RsDisallowModifiers
--		Fires errors and returns true if there are any modifiers specified not allowed on the object provided
--		If mods is unsupplied then no modifiers are permitted at all. 
fn RsDisallowModifiers obj mods: = (

	local returnVal = false

		if mods == unsupplied then
		(
			if(obj.modifiers.count == 0) then (false)
			else(
				returnVal = true
				for aMod in obj.modifiers do(
					gRsUlog.LogError ("Disallowed modifier: " + ((classof aMod) as string)) context:obj
				)
			)
		)else(
			if(classof mods == Array) then 
			(
				for sMod in mods do(
					for oMod in obj.modifiers do (
						if (sMod) == (classof oMod) then (
							gRsUlog.LogError ("Disallowed modifier: " + ((sMod) as string)) context:obj
							returnVal = true
						)
					)
				)
			)else(
				for oMod in obj.modifiers do (
					if (mods) == (classof oMod) then
					(
						gRsUlog.LogError ("Disallowed modifier: " + ((mods) as string)) context:obj
						returnVal = true
					)
				)
			)

		)
	
	returnVal
)