--
-- File:: rockstar\export\ProjectConfig.ms
-- Desc:: Asset Pipeline 3 content-tree methods.
--
-- Author:: Kevin Weinberg
-- Date:: 8 April 2013
--

struct RsProjectConfigStruct
(
	parametersClass = (dotNetClass "RSG.Base.Xml.Parameters"),
	params = (dotNetObject "System.Collections.Generic.Dictionary`2[System.String,System.Object]"),
	configFile = undefined,
	
	fn Load =
	(
		-- We unfortunately have to use the 'RS_TOOLSROOT' environment variable, 
		--	because there are instances where MaxScript may be run prior to the DLLs being loaded.
		configFile = (systemTools.getEnvVariable "RS_TOOLSROOT") + "/etc/processors/RSG.Pipeline.Project.xml"
		
		if (doesFileExist configFile) do 
		(
			parametersClass.Load configFile params False False
		)
	),
	
	-- Returns (text) parameter loaded from RSG.Pipeline.Project.xml, or 'Default' argument if unavailable.
	--	Values will be converted to non-text classes if 'Class' is specified.
	fn GetParameter parameterName Class: Default:undefined = 
	(
		local retVal = Default
		
		if ( params.ContainsKey[parameterName] == True ) do 
		(
			retVal = params.Item[parameterName]
			
			if (Class != unsupplied) do 
			(
				retVal = (retVal as Class)
			)
		)
		
		return retVal
	),
	
	-- Project-parameter load-functions for assorted classes
	--	(shouldn't be needed, as xml already tells the plugin which class to use...)
	fn GetBoolParameter parameterName Default:undefined = 
	(
		GetParameter parameterName Default:Default Class:BooleanClass
	),
	fn GetFloatParameter parameterName Default:undefined = 
	(
		GetParameter parameterName Default:Default Class:Float
	),
	
	-- Load config when struct is instanced:
	on Create do 
	(
		Load()
	)
)

global RsProjectConfig = RsProjectConfigStruct()
