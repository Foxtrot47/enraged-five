--
-- File:: rockstar/export/settings_funcs.ms
-- Description:: Setting functions for 3dsmax pipeline.
--

-----------------------------------------------------------------------------
-- Includes
-----------------------------------------------------------------------------
filein "pipeline/util/settingsave.ms"
filein "pipeline/export/maps/globals.ms"

----------------------------------------------------------------------------
-- Entry-Point
-----------------------------------------------------------------------------

rexSetForceTextureExport false
RsClampTextures = false
RsToolsInstalled = true

-- This is only used to get the path for the configswitch require.  It should only be used for this
fn RsGetRootDirForConfigSwitch = (
	RsMakeSafePath( RsConfigGetToolsRootDir() + "\\dcc" )
)

if not gRsIsOutSource then
(
	leadingPath = RsGetRootDirForConfigSwitch()
	if leadingPath != undefined then (
		filein (leadingPath + "/common/configswitch_clean.ms")
	)
)
else
(
	leadingPath = getdir #scripts
	if leadingPath != undefined then (
		filein (leadingPath + "/rockstar/configswitch_clean.ms")
	)	
)

--
-- name: RsConfigGetPluginDir
-- desc: Return project's 3dsmax plugin directory.
--
fn RsConfigGetPluginDir = (
	local pluginDir = undefined
	local pluginUserSettingsIni = ( pathConfig.getDir #maxData ) + "Plugin.UserSettings.ini"
	
	if ( doesFileExist pluginUserSettingsIni ) then (
		pluginDir = RsMakeSafeSlashes( getINISetting pluginUserSettingsIni "Directories" "RsPluginPath" )
	)
	
	pluginDir
)

--
-- name: RsConfigGetMetadataDir
-- desc: Return project's 'metadata' directory.
--
fn RsConfigGetMetadataDir core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreBranch.Metadata + "/" )
	else
		RsMakeSafeSlashes( gRsBranch.Metadata + "/" )
)

--
-- name: RsConfigGetMetadataDefinitionDir
-- desc: Return project's 'metadata' definition (PSC) directory.
--
fn RsConfigGetMetadataDefinitionDir core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreBranch.Definitions + "/" )
	else
		RsMakeSafeSlashes( gRsBranch.Definitions + "/" )
)

--
-- name: RsConfigGetAssetsDir 
-- desc: Return project's assets directory.
--
fn RsConfigGetAssetsDir core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreBranch.Assets + "/" )
	else
		RsMakeSafeSlashes( gRsBranch.Assets + "/" )
)

--
-- name: RsConfigGetExportDir
-- desc: Return project's root export directory.
--
fn RsConfigGetExportDir core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreBranch.Export +"/" )
	else
		RsMakeSafeSlashes( gRsBranch.Export +"/" )
)

--
-- name: RsConfigGetIndependentDir
-- desc: Return project's independent directory.
--
-- DHM: OBSOLETE: use RsConfigGetExportDir instead.
--
fn RsConfigGetIndependentDir core:false = (
	RsValidateProject()

	if core then
		gRsCoreBranch.Export
	else
		gRsBranch.Export
)

--
-- name: RsConfigGetProjectName 
-- desc: Return project name key (e.g. "jimmy" )
--
fn RsConfigGetProjectName core:false = (
	RsValidateProject()

	if core then
		gRsCoreProject.Name
	else
		gRsProject.Name
)

--
-- name: RsConfigGetProjRootDir
-- desc: Return absolute path to project root directory.
--
fn RsConfigGetProjRootDir core:false = (
	RsValidateProject()

	if core then
		gRsCoreProject.Root + "/"
	else
		gRsProject.Root + "/"
)

--
-- name: GetConfigGetProjSubDir
-- desc: Return a project's subdirectory definition, but falling back on default path if project path doesn't exist
--
fn GetConfigGetProjSubDir subPath core:false = (
	RsValidateProject()
	local ret = undefined
	if not core and doesFileExist ( gRsProject.Root + "/" + subPath ) then
		ret = RsMakeSafeSlashes( gRsProject.Root + "/" + subPath )
	else
		ret = RsMakeSafeSlashes( gRsCoreProject.Root + "/" + subPath )

	format "GetConfigGetProjSubDir() returning %\n" ret
	return ret
)
--
-- name: RsConfigGetProjConfigDir
-- desc: Return project configuration data directory.
--
fn RsConfigGetProjConfigDir core:false = (
	RsMakeSafeSlashes( RsConfigGetToolsConfigDir core:core + "/" )
)

--
-- name: RsConfigGetProjBinConfigDir
-- desc: Return absolute path to export configuration data ($(toolsconfig)\config).
--
fn RsConfigGetProjBinConfigDir core:false = (
	RsMakeSafeSlashes( RsConfigGetProjConfigDir core:core + "/config/" )
)

--
-- name: RsConfigGetTargetEnabled
-- desc: Return whether target platform is enabled.
--
-- Platforms are string keys: "win32", "ps3", "xbox360".
--
fn RsConfigGetTargetEnabled platform = (
	( RsProjectGetTargetEnabled platform )
)

--
-- name: RsConfigSetTargetEnabled
-- desc: Set whether target platform is enabled.
--
-- Platforms are string keys: "win32", "ps3", "xbox360".
--
fn RsConfigSetTargetEnabled platform enabled = 
(
	RsProjectSetTargetEnabled platform enabled
	RsProjectContentSave()
)

--
-- name: RsconfigGetBinDir
-- desc: Return tools 'bin' directory.
--
fn RsConfigGetBinDir core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreConfig.ToolsBin + "/" )
	else
		RsMakeSafeSlashes( gRsConfig.ToolsBin + "/" )
)

--
-- name: RsConfigGetLibDir
-- desc: Return tools 'lib' directory.
--
fn RsConfigGetLibDir core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreConfig.ToolsLib + "/" )
	else
		RsMakeSafeSlashes( gRsConfig.ToolsLib + "/" )
)

--
-- name: RsConfigGetContentDir
-- desc: Return absolute path to content data.
--
fn RsConfigGetContentDir core:false = (
	RsMakeSafeSlashes( RsConfigGetProjConfigDir core:core + "/content/" )
)

--
-- name: RsConfigGetTCPRoot
-- desc: Return root TCP dir
--
fn RsConfigGetTCPRoot core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreBranch.Metadata + "/textures/templates/" )
	else
		RsMakeSafeSlashes( gRsBranch.Metadata + "/textures/templates/" )
)

--
-- name: RsConfigGetTCPMapsRoot
-- desc: Return root TCP dir
--
fn RsConfigGetTCPMapsRoot core:false = (
	RsMakeSafeSlashes( RsConfigGetTCPRoot core:core + "/" + ::RsConfigGetTCMapsRootFolderName core:core + "/" )
)

--
-- name: RsConfigGetTCPGlobalsRoot
-- desc: Return root TCP dir
--
fn RsConfigGetTCPGlobalsRoot core:false = (
	RsMakeSafeSlashes( RsConfigGetTCPRoot core:core + "/globals/" )
)

--
-- name: RsConfigGetTCPMapsOtherRoot
-- desc: Return root TCP dir
--
fn RsConfigGetTCPMapsOtherRoot core:false = (
	RsMakeSafeSlashes( RsConfigGetTCPRoot core:core + "/maps_other/" )
)

--
-- name: RsConfigGetTCPPedsRoot
-- desc: Return root TCP dir
--
fn RsConfigGetTCPPedsRoot core:false = (
	RsMakeSafeSlashes( RsConfigGetTCPRoot core:core + "/peds/" )
)

--
-- name: RsConfigGetTCPVehiclesRoot
-- desc: Return root TCP dir
--
fn RsConfigGetTCPVehiclesRoot core:false mp:false = (
	local subFolder = "/vehicles/"
	if mp then
		subFolder = "/vehicles_mp/"
	RsMakeSafeSlashes( RsConfigGetTCPRoot core:core + subFolder )
)

--
-- name: RsConfigGetTCPWeaponsRoot
-- desc: Return root TCP dir
--
fn RsConfigGetTCPWeaponsRoot core:false = (
	RsMakeSafeSlashes( RsConfigGetTCPRoot core:core + "/weapons/" )
)

--
-- name: RsConfigGetContentDir
-- desc: Return root TCP dir
--
fn RsConfigGetTCPFxRoot core:false = (
	RsMakeSafeSlashes( RsConfigGetTCPRoot core:core + "/fx/" )
)

--
-- name: RsConfigGetTCSMapsRootFolderName
-- desc: Return root TCS directory name
--
-- NOTE:  url:bugstar:254946 - This setting is different across projects, but the setting will eventually become 
-- the same once texture linking is enabled across all pipelines.  In order to maintain an easier-to-integrate
-- environment, these project-specific settings are added here.  
--
fn RsConfigGetTCMapsRootFolderName core:false = 
(
	RsMakeSafeSlashes( RsProjectGetMapTexDir core:core )
)

--
-- name: RsConfigGetTCSMapsRoot
-- desc: Return root TCS dir
--
fn RsConfigGetTCSMapsRoot core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreBranch.Metadata + "/textures/" + RsConfigGetTCMapsRootFolderName core:core + "/" )
	else
		RsMakeSafeSlashes( gRsBranch.Metadata + "/textures/" + RsConfigGetTCMapsRootFolderName core:core + "/" )
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetMapsTexDir core:false = (

	RsMakeSafeSlashes( RsConfigGetAssetsDir core:core + "/maps/textures/" )
)

--
-- name: RsConfigGetCacheDir
-- desc: Return project cache directory.
--
fn RsConfigGetCacheDir core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreProject.Cache + "/raw/" )
	else
		RsMakeSafeSlashes( gRsProject.Cache + "/raw/" )
)

--
-- name: RsConfigGetProcessedDir
-- desc: Return project processed directory.
--
fn RsConfigGetProcessedDir core:false = (
	RsMakeSafeSlashes( RsConfigGetAssetsDir core:core + "/processed/" )
)

--
-- **OBSOLETE**: use RSConfigGetCacheDir instead
--
-- name: RsConfigGetStreamDir
-- desc: Return project cache directory.
--
fn RsConfigGetStreamDir core:false = (
	RsConfigGetCacheDir core:core
)

--
-- name: RsConfigGetPatchDir 
-- desc: Return path for preview/patch directory.
--
fn RsConfigGetPatchDir core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreBranch.Preview + "/" )
	else
		RsMakeSafeSlashes( gRsBranch.Preview + "/" )
)

--
-- name: RsClearPatchDir 
-- desc: Delete all files in preview/patch directory.
--
fn RsClearPatchDir core:false = (

	projDir = RsConfigGetPatchDir core:core
	RsDeleteDirectory projDir
)

--
-- **OBSOLETE**: use RSConfigGetCacheDir instead
--
-- name: RsConfigGetNetworkStreamDir
-- desc: Return project cache directory.
--
fn RsConfigGetNetworkStreamDir core:false = (

	RsConfigGetCacheDir core:core
)


--
-- **OBSOLETE**: use RSConfigGetCacheDir instead
--
-- name: RsConfigGetMapStreamDir
-- desc: Return project cache directory.
--
fn RsConfigGetMapStreamDir core:false = (

	RsConfigGetCacheDir core:core
)

--
-- name: RsConfigGetIndDir
-- desc: Return absolute path to project's independent directory.
--
fn RsConfigGetIndDir core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreBranch.Export )
	else
		RsMakeSafeSlashes( gRsBranch.Export )
)

--
-- name: RsConfigGetExportLevelsDir
-- desc: Return path to project's export/levels directory.
--
fn RsConfigGetExportLevelsDir core:false = (
	RsMakeSafeSlashes( RsConfigGetExportDir core:core + "levels/" )
)

--
-- name: RsConfigGetScriptDir
-- desc: Return cache scripts directory.
--
fn RsConfigGetScriptDir core:false = (

	RsMakeSafeSlashes( RsConfigGetCacheDir core:core + "/script/" )
)

--
-- name: RsConfigGetCommonDir
-- desc: Return project common directory.
--
fn RsConfigGetCommonDir core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreBranch.Common + "/" )
	else
		RsMakeSafeSlashes( gRsBranch.Common + "/" )
)

--
-- name: RsConfigGetShadersDir
-- desc: Return project shaders directory.
--
fn RsConfigGetShadersDir core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreBranch.Shaders + "/" )
	else
		RsMakeSafeSlashes( gRsBranch.Shaders + "/" )
)

--
-- name: RsConfigGetArtDir 
-- desc: Return project art directory.
--
fn RsConfigGetArtDir core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreBranch.Art + "/" )
	else
		RsMakeSafeSlashes( gRsBranch.Art + "/" )
)

--
-- name: RsConfigGetPedArtDir 
-- desc: Return ped specific art directory.
--
fn RsConfigGetPedArtDir core:false = (
	RsValidateProject()

	RsMakeSafeSlashes( (RsConfigGetProjRootDir core:core) + "/art/peds" )

)

--
-- name: RsConfigGetTextureSourceDir 
-- desc: Return source art textures directory.
--
fn RsConfigGetTextureSourceDir core:false = (
	RsValidateProject()

	local texPath = RsMakeSafeSlashes( gRsBranch.Art + "/textures/" )
	if core then
		texPath = RsMakeSafeSlashes( gRsCoreBranch.Art + "/textures/" )

	texpath
)

--
-- name: RsConfigGetMapsTintTexDir
-- desc: Return tint palettes texture directory.
--
fn RsConfigGetMapsTintTexDir core:false = (

	RsMakeSafeSlashes( RsConfigGetTextureSourceDir core:core + "tint_palettes/" )
)

--
-- name: RsConfigGetMapsTintTexDir
-- desc: Return tint palettes texture directory.
--
fn RsConfigGetMapsUserDefinedTintTexDir core:false = (

	RsMakeSafeSlashes( RsConfigGetMapsTintTexDir core:core + "user_defined_palettes/" )
)

-- name: RsConfigGetModelSourceDir 
-- desc: Return source models directory.
--
fn RsConfigGetModelSourceDir core:false = 
(
	RsValidateProject()

	if core then
		RsMakeSafeSlashes (gRsCoreBranch.Art + "/models/")
	else
		RsMakeSafeSlashes (gRsBranch.Art + "/models/")
)

-- name: RsConfigGetVehicleSourceDir 
-- desc: Return vehicles source directory.
--
fn RsConfigGetVehiclesSourceDir core:false = 
(
	RsValidateProject()

	if core then
		RsMakeSafeSlashes (gRsCoreBranch.Art + "/vehicles/")
	else
		RsMakeSafeSlashes (gRsBranch.Art + "/vehicles/")
)

--
-- name: RsConfigGetProxyTextureSourceDir 
-- desc: Return source art textures proxy directory.
--
-- *** OBSOLETE ***
--
fn RsConfigGetProxyTextureSourceDir core:false = (
	RsValidateProject()

	if core then
		RsMakeSafeSlashes( gRsCoreBranch.Art + "/textures_PROXY/" )
	else
		RsMakeSafeSlashes( gRsBranch.Art + "/textures_PROXY/" )
)

--
-- name: RsConfigGetToolsDir
-- desc: Get the root tools folder
--
fn RsConfigGetToolsDir core:false = (
	RsMakeSafeSlashes( RsConfigGetToolsRootDir core:core + "/" )
)

--
-- name:	RsConfigGetLogDir
-- desc:	Returns log directory
--
fn RsConfigGetLogDir core:false = (
	RsMakeSafeSlashes( RsConfigGetToolsDir core:core + "/logs/" )
)

--
-- name: RsConfigGetWildWestDir
-- desc: Return root directory of wildwest.
--
fn RsConfigGetWildWestDir core:false = (
	if (not gRsIsOutSource) then
	(
		RsMakeSafeSlashes( RsConfigGetToolsDir core:core + "/wildwest/" )
	)
	else
	(
		((getdir #scripts) + "wildwest/")
	)
)

--
-- name: RsConfigGetEtcDir
-- desc: Get the tools etc folder
--
fn RsConfigGetEtcDir core:false = (
	RsMakeSafeSlashes( RsConfigGetToolsDir core:core + "/etc/" )
)

--
-- name: RsConfigGetNetworkDir 
-- desc: Returns project's network directory
--
-- *** OBSOLETE ***
--
fn RsConfigGetNetworkDir core:false = 
(
	RsProjectGetNetworkGenericStreamDir core:core
)

--
-- name:RsConfigGetLocalAppDataDir
-- desc: Returns the windows local appdata location
--
fn RsConfigGetLocalAppDataDir = 
(
	RsMakeSafeSlashes (pathConfig.appendPath (systemTools.getEnvVariable "LOCALAPPDATA") "Rockstar_Games/")
)

--
-- Returns build folder: (e.g. x:/gta5/dev/build/dev/, or /dev_ng/)
fn RsConfigGetBuildDir = 
(
	RsMakeSafeSlashes (gRsBranch.build + "/")
)

-- Get dotNetClass to have access to enum static methods and types
RsgPlatform = dotNetClass "RSG.Platform.Platform"
SystemEnum = dotNetClass "System.Enum"
--
-- name: CheckTargetEnabled
-- desc: iterate through the branch's targets to get if it's enabled or not in local.xml
--
fn CheckTargetEnabled control platform =
(
	local tgtEnumerator = gRsBranch.Targets.GetEnumerator()
	local res = undefined
	
	-- Iterate through all Targets in selected branch.
	while tgtEnumerator.MoveNext() and res == undefined do
	(
		-- get RSG.Plaform.Platform's enum as string to compare against the platform passed as parameter (see settings.ms' call)
		-- basically it's a call to Enum.GetName( typeof(RSG.Platform.Platform), <current platform> ) in maxscript.
		local keyAsString = SystemEnum.GetName RsgPlatform tgtEnumerator.Current.Key
		
		if keyAsString == platform then
		(
			res = tgtEnumerator.Current.Value.Enabled
		)
	)
	
	-- set the behaviour of the controls depending on the result
	-- undefined : the target has not been found, so block the control
	-- true/false : the target has been found and we have the value, check or not
	if ( res == undefined ) then
	(
		control.enabled = false -- disable
		control.checked = false -- untick
	)
	else
	(
		control.enabled = true 	-- re-enable
		control.checked = res 
	)
)

fn RsProjectGetBugstarId = 
(
	if(gRsBugstarConfig != undefined) then
		gRsBugstarConfig.ProjectID
)



fn RsConfigGetAudioDir core:false = (
	RsMakeSafeSlashes ((RsConfigGetProjRootDir core:true) + "/audio/" + gRsBranch.Name + "/")
)


fn RsConfigGetExtraAudioOcclusionDir = (
	RsMakeSafeSlashes ((RsConfigGetProjRootDir core:false) + "/extraAudioOccluders.xml")
)



----------------------------------------------------------
-- name: UpdateLocalTargets
-- desc: Iterates throught the targets of the active branch, enables or disables it, and save to local.xml
-- params:
-- 		platform: string, should be passed using the "constants" called TargetXXX located in settings.ms
--		enabled: boolean, enables or disables the target if found
----------------------------------------------------------
fn UpdateLocalTargets platform enabled =
(
	local writableConfig = RsContentTree.configFactory.CreateWritableConfig()
	
	-- Update possible Branches values
	local writableCurrentBranch = writableConfig.Project.Branches.Item[gRsBranch.Name]
	local currentBranch = gRsBranch

	----------------------------------------------------------------------------------------------
	local writableTargetEnumerator = writableCurrentBranch.Targets.GetEnumerator()
	
	-- We cant access to branch.Targets.Item[<key>] because the key is an enum, and we have a string
	-- iterate through all Targets in selected branch.
	local found = undefined
	while writableTargetEnumerator.MoveNext() and found == undefined do
	(
		-- get RSG.Plaform.Platform's enum as string to compare against the platform passed as parameter (see settings.ms' call)
		-- basically it's a call to Enum.GetName( typeof(RSG.Platform.Platform), <current platform> ) in maxscript.
		local target = SystemEnum.GetName RsgPlatform writableTargetEnumerator.Current.Key
		
		if target == platform then
		(
			found = true;
			writableTargetEnumerator.Current.Value.Enabled = enabled
		)
	)
	
	-- save to local.xml the target change
	writableConfig.SaveLocal()
	
	----------------------------------------------------------------------------------------------
	-- we did the job for our local config file, so let's do it for the global one
	local currentTargetEnumerator = currentBranch.Targets.GetEnumerator()
	found = undefined
	
	while currentTargetEnumerator.MoveNext() and found == undefined do
	(
		local target = SystemEnum.GetName RsgPlatform currentTargetEnumerator.Current.Key
		
		if target == platform then
		(
			found = true;
			currentTargetEnumerator.Current.Value.Enabled = enabled
		)
	)
	
)
