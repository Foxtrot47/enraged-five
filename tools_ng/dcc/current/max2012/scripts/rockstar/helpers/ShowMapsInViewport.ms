--
-- File:: rockstar/helpers/ShowMapsInViewport.ms
-- Description:: Displays maps in the viewport
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 4 May 2007
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsShowMapsInViewportRoll "Material Map Control"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Show_Maps_in_Viewport" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	checkbox chkVisible "Visible in Viewports" checked:true
	checkbox chkScene "Only Scene Materials" checked:true
	checkbox chkStandard "Include Standard Materials" checked:false
	checkbox chkAlphaMaps "Toggle alpha maps too" checked:false
	button btnUpdate "Update" width:100
	progressbar barProgress

	label lblInfo1 "This only shows/hides diffuse texture on Rage Shader\nand Standard (when enabled) materials.\nIt will attempt alpha maps where possible if \n\"Toggle alpha maps too\" is checked." align:#left height:55

	-- Sets the show texture map state for a set of multi-materials
	fn setVisibility materials state = (
	
		numMaterial = 1 
		barProgress.value = 0
		for mat in materials do
		(
			barProgress.value = 100.0 * ( (numMaterial as float) / materials.count )	
			numMaterial += 1	
			
			-- Debug code
			--format "Material index: %, Class of material: %\n" numMaterial (classof mat)
			materiallist = #()			
			if ( Multimaterial == ( classof mat ) ) then (
				materiallist = mat.materiallist
			)
			else (
				materiallist = #(mat)
			)
			
			-- Debug code
			--format "Num submaterials: %\n" ( getNumSubMtls mat )
			
			
			for nSubMat = 1 to materiallist.count do
			(
				
				-- Debug code
				--format "\tSub-material index: %\n" nSubMat
				
				submat = materiallist[nSubMat]
				if ( undefined == submat ) then
					continue
				
				if ( Rage_Shader == ( classof submat ) ) then
				(
					if ( 0 == ( getNumSubTexmaps submat ) ) then
						continue
					
					-------------------------------------------------------------
					-- Set diffuse map
					
					texmap = getSubTexmap submat 1
					if ( undefined == texmap ) then
					(
					  	-- Debug code
						--format "Warning: Rage Shader material %, sub-material % does not have texture map 1 defined.\n" mat.name submat.name
						continue
					)
					
					showTextureMap submat texmap state
					
					-- See if there is an alpha map (if selected)
					if ( chkAlphaMaps.checked ) then
					(
					
						-------------------------------------------------------------
						-- Set alpha map (if alpha shader)

						if ( RstGetIsAlphaShader submat ) then
						(
							submat.showInViewport = true
						)
					)
					
				)
				else if ( ( Standard == ( classof submat ) ) and ( chkStandard.checked ) ) then
				(
					if ( 0 == ( getNumSubTexmaps submat ) ) then
						continue

					-------------------------------------------------------------
					-- Set diffuse map
					
					-- 2 is the diffuse map channel for Standard class materials
					texmap = getSubTexmap submat 2
					if ( undefined == texmap ) then
					(
						-- Debug code
						--format "Warning: Standard material %, sub-material % does not have diffuse texture map defined.\n" mat.name submat.name
						continue
					)
					
					showTextureMap submat texmap state

					-- See if there is an alpha map (if selected)
					if ( chkAlphaMaps.checked ) then
					(
					
						-------------------------------------------------------------
						-- Set alpha map	

						-- 7 is the alpha map channel for (most) Standard class materials
						texmap = getSubTexmap submat 7	
						if ( undefined == texmap ) then
						(
							-- Debug code
							--format "Warning: Standard material %, sub-material % does not have alpha texture map defined.\n" mat.name submat.name
							continue
						)

						showTextureMap submat state
					)
					
				)
				
			)
			
		)
		
		-- Reset progress bar
		barProgress.value = 0
	
	)

	on btnUpdate pressed do
	(
	  -- Using Scene Materials or all Material Editor materials?
	  if ( chkScene.checked ) then
	  (
	  
	  	if ( chkVisible.checked ) then
	  		setVisibility sceneMaterials on
	  	else
	  		setVisibility sceneMaterials off
	  
	  )
	  else
	  (
	  
	  	if ( chkVisible.checked ) then
	  		setVisibility meditMaterials on
	  	else
	  		setVisibility meditMaterials off
	  
	  )
	)
)

try CloseRolloutFloater RsShowMapsInViewport catch()
RsShowMapsInViewport = newRolloutFloater "Material Map Control" 300 240
addRollout RsShowMapsInViewportRoll RsShowMapsInViewport

-- End of script
