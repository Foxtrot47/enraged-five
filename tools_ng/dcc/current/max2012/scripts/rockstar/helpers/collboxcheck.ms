--
-- File:: rockstar/helpers/collboxcheck.ms
-- Description:: Utility to check the validity of all collision boxes
--
-- Author:: Jonny Rivers <jonathan.rivers@rockstarnorth.com
-- Date:: 3/4/2012
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- data
-----------------------------------------------------------------------------

thinBoxes_ = undefined-- the boxes found on the last check

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsCollisionBoxCheckRollout "Collision Box Check"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Collision_Box_Check" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	listbox lstThinBoxNames "Thin boxes (less than 2cm in any dimension):" height:20
	button btnRefresh "Refresh" width:100 across:2
	button btnClose "Close" width:100

	--////////////////////////////////////////////////////////////
	-- implementation
	--////////////////////////////////////////////////////////////
	fn RsCheckCollisionBoxes =
	(
		thinBoxes_ = #()

		for obj in $objects do
		(
			if ( classof obj == Col_Box ) do
			(
				if( obj.width < 0.01 or obj.length < 0.01 or obj.height < 0.01 ) then
				(
					append thinBoxes_ obj
				)
			)
		)

		dodgyBoxNames = #()
		for dodgyBox in thinBoxes_ do
		(
			append dodgyBoxNames dodgyBox.name
		)
		
		dodgyBoxNames
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on RsCollisionBoxCheckRollout open do
	(
		lstThinBoxNames.items = RsCheckCollisionBoxes()
	)

	on btnRefresh pressed do 
	(
		RsCheckCollisionBoxes()
		
		lstThinBoxNames.items = RsCheckCollisionBoxes()
	)
	
	on lstThinBoxNames selected item do (

		if (thinBoxes_[item] != undefined) then 
		(
			if isdeleted thinBoxes_[item] == false then 
			(
				select thinBoxes_[item]
				max zoomext sel
			)
		)
	)

	on btnClose pressed do 
	(
		DestroyDialog RsCollisionBoxCheckRollout 
	)	
)

CreateDialog RsCollisionBoxCheckRollout width:400 modal:false