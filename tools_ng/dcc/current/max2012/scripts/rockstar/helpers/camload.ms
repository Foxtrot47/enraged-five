global CameraLoadFile = RsConfigGetProjRootDir() + "build/dev/temp_debug.txt"

rollout RsCamLoadRoll "Set Camera From Game"
(
	--************************************
		--Interface
	--************************************
	edittext FilePathBox "Debug File: " text:CameraLoadFile bold:true readOnly:true enabled:false pos:[5, 10] fieldWidth:400
	button btnLoadFile "... " enabled:true pos:[470, 10]
	button btnClose "Close" enabled:true across:2
	button btnSet "Set" enabled:true
	
	on btnLoadFile pressed do
	(
		NewCameraLoadFile = getOpenFileName caption:"Debug File" filename:CameraLoadFile types:"Text(*.txt)"
		if NewCameraLoadFile != undefined Then
		(
			CameraLoadFile = NewCameraLoadFile
			FilePathBox.text = CameraLoadFile
		)
	)
	
	on btnClose pressed do
	(
		try CloseRolloutFloater CameraFromGameUtil catch ()
	)
	
	on btnSet pressed do
	(
		if selection.count != 1 then
		(
			messagebox "Please only have one camera object selected at a time."
			return false
		)
		if superclassof selection[1] != camera then
		(
			messagebox "You need to have a camera object selected."
			return false
		)
			
		camfile = openfile CameraLoadFile mode:"r"
		
		if camfile != undefined then
		(
			postoken = "Camera Position"
			posval = undefined

			while eof camfile == false do
			(
				camfileline = readline camfile
				
				if substring camfileline 1 postoken.count == postoken and posval == undefined then
				(
 					camfiletokens = filterstring camfileline ",) << >> " 
					posval = [camfiletokens[4] as number,camfiletokens[5] as number,camfiletokens[6] as number]
				)
	
-- 				if substring camfileline 1 rottoken.count == rottoken and rotval == undefined then
-- 				(
-- 					camfiletokens = filterstring camfileline ",)"
-- 					rotval = [camfiletokens[2] as number,camfiletokens[3] as number,camfiletokens[4] as number]
-- 				)
			)

			close camfile

			if superclassof $ == camera then
			(
				selection[1].pos = posval
			)

-- 			setmat = (eulerangles -90 0 0) as matrix3

-- 			inmatx = inverse ( (eulerangles 44.995380 0.0 0.0) as matrix3 )
-- 			inmaty = inverse ( (eulerangles 0.0 0.0 0.0) as matrix3 )
-- 			inmatz = inverse ( (eulerangles 0.0 0.0 45.696262) as matrix3 )

-- 			if superclassof $ == camera then
-- 			(
-- 				$.rotation = (inmatz * inmaty * inmatx * setmat) as quat
-- 			)
		)
		else
		(
			messagebox ("Unable to open the file " + CameraLoadFile " for reading")
		)
	)
)

try CloseRolloutFloater CameraFromGameUtil catch ()
global CameraFromGameUtil = newRolloutFloater "Set Camera From Camera" 550 100 40 96
addRollout RsCamLoadRoll CameraFromGameUtil




