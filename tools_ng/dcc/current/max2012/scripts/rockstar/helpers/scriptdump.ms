fn Dump2dMarkers filename = (

	writefile = openfile filename mode:"w"
	
	if writefile == undefined then return false
	
	idxWorkType = getattrindex "Gta Script" "Work Type"
	idxHome = getattrindex "Gta Script" "Home"
	idxLeisureType = getattrindex "Gta Script" "Leisure Type"
	idxFoodType = getattrindex "Gta Script" "Food Type"
	
	for obj in rootnode.children do (
	
		if getattrclass obj == "Gta Script" then (
		
			valWorkType = getattr obj idxWorkType
			valHome = getattr obj idxHome
			valLeisureType = getattr obj idxLeisureType
			valFoodType = getattr obj idxFoodType
		
			outline = "Name: " + obj.name
			outline = outline + ", Work Type: "
					
			case(valWorkType) of (	
			0: (outline = outline + "None")
			1: (outline = outline + "Bank Worker")
			2: (outline = outline + "Bar Worker")
			3: (outline = outline + "Bike Courier")
			4: (outline = outline + "Bouncer")
			5: (outline = outline + "Bus Driver")
			6: (outline = outline + "Chef")
			7: (outline = outline + "Cleaner")
			8: (outline = outline + "Shopper")
			9: (outline = outline + "Construction Worker")
			10: (outline = outline + "Criminal")
			11: (outline = outline + "Doctor")
			12: (outline = outline + "Factory Worker")
			13: (outline = outline + "Fireman")
			14: (outline = outline + "Gang Member")
			15: (outline = outline + "Garbage Collector")
			16: (outline = outline + "Airport Worker")
			17: (outline = outline + "Generic Worker")
			18: (outline = outline + "Hair Dresser")
			19: (outline = outline + "Homeless")
			20: (outline = outline + "Hotel Worker")
			21: (outline = outline + "House Wife")
			22: (outline = outline + "Lawyer")
			23: (outline = outline + "Lay About")
			24: (outline = outline + "Life Guard")
			25: (outline = outline + "Mechanic")
			26: (outline = outline + "Nurse")
			27: (outline = outline + "Paramedic")
			28: (outline = outline + "Pensioner")
			29: (outline = outline + "Pilot")
			30: (outline = outline + "Police")
			31: (outline = outline + "Priest")
			32: (outline = outline + "Business Worker")
			33: (outline = outline + "Prostitute")
			34: (outline = outline + "Receptionist")
			35: (outline = outline + "Security Guard")
			36: (outline = outline + "Shop Sales Assistant")
			37: (outline = outline + "Socialite")
			38: (outline = outline + "Stripper")
			39: (outline = outline + "Student")
			40: (outline = outline + "Taxi Driver")
			41: (outline = outline + "Telephone Engineer")
			42: (outline = outline + "Tourist")
			43: (outline = outline + "Truck Driver UPS")
			44: (outline = outline + "Unemployed Lay About")
			45: (outline = outline + "Waiter")
			46: (outline = outline + "Postman")
			47: (outline = outline + "Security Truck Driver")
			48: (outline = outline + "Train Driver")
			49: (outline = outline + "FBI CSI")
			default: (outline = outline + "unknown")
			)
			
			outline = outline + ", Home: "
			
			case(valHome) of (	
			0: (outline = outline + "None")
			1: (outline = outline + "Home")
			default: (outline = outline + "unknown")
			)
			
			outline = outline + ", Leisure Type: "
		
			case(valLeisureType) of (	
			0: (outline = outline + "None")
			1: (outline = outline + "Basketball")
			2: (outline = outline + "Table Tennis")
			3: (outline = outline + "Pool")
			4: (outline = outline + "Pub")
			5: (outline = outline + "Clubbing")
			6: (outline = outline + "Shopping")
			7: (outline = outline + "Video Games")
			8: (outline = outline + "Watch TV")
			9: (outline = outline + "Walk In Park")
			default: (outline = outline + "unknown")
			)
		
			outline = outline + ", Food Type: "
		
			case(valFoodType) of (			
			0: (outline = outline + "None")
			1: (outline = outline + "Eat At Home")
			2: (outline = outline + "Crap")
			3: (outline = outline + "Average")
			4: (outline = outline + "Good")
			default: (outline = outline + "unknown")
			)
			
			outline = outline + ", Pos: " + (obj.pos as string)
		
			quatRot = obj.transform as quat
			rotVals = quattoeuler quatRot order:1
			
			outline = outline + ", Rot: " + (rotVals.z as string)
			
			outline = outline + ", Children: " + (obj.children.count as string)
			
			for childobj in obj.children do (
			
				outline = outline + ", Pos: " + (childobj.pos as string)

				quatRot = childobj.transform as quat
				rotVals = quattoeuler quatRot order:1

				outline = outline + ", Rot: " + (rotVals.z as string)				
			)
		
			format "%\n" outline to:writefile
		)
	)
	
	close writefile
	
	return true
)

fn DumpScenarios filename = (

	writefile = openfile filename mode:"w"
	
	if writefile == undefined then return false
	
	idxPedType = getattrindex "Gta SpawnPoint" "ped type"
	idxStart = getattrindex "Gta SpawnPoint" "time start override"
	idxEnd = getattrindex "Gta SpawnPoint" "time end override"


	for obj in rootnode.children do (
	
		if getattrclass obj == "Gta SpawnPoint" then (
		
			valPedType = getattr obj idxPedType
			valStart = getattr obj idxStart
			valEnd = getattr obj idxEnd
			
			outline = "Name: " + (obj.name) + ", Pos: " + (obj.pos as string)
			quatRot = obj.transform as quat
			rotVals = quattoeuler quatRot order:1
			
			outline = outline + ", Rot: " + (rotVals.z as string)
			
			outline = outline + ", Type: " + valPedType
						
			outline = outline + ", Time Start Override: " + (valStart as string)
			outline = outline + ", Time End Override: " + (valEnd as string)
			
			format "%\n" outline to:writefile
		)
	)
	
	close writefile
	
	return true
)

rollout GtaScriptRepotsRoll "Script Reports"
(
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Script_Dump" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button button2dMarkers "Save 2d Markers"
	button buttonScenarios "Save Scenarios"
	
	on button2dMarkers pressed do (

		savefilename = getsavefilename()

		if savefilename != undefined then (

			Dump2dMarkers savefilename
		)
	)
	
	on buttonScenarios pressed do (
		
		savefilename = getsavefilename()

		if savefilename != undefined then (

			DumpScenarios savefilename
		)
	)
)

try CloseRolloutFloater GtaScriptRepotsUtil catch()
GtaScriptRepotsUtil = newRolloutFloater "GTA" 300 150 1 136
addRollout GtaScriptRepotsRoll GtaScriptRepotsUtil