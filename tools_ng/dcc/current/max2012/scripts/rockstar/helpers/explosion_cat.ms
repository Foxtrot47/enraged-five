--
-- File:: rockstar/helpers/explosion_cat.ms
-- Description:: Particle setup utility
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
--
-----------------------------------------------------------------------------

callbacks.removescripts id:#RsExplosionObjectSelected

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
RsCollCats = #()
RsCollItems = #()
-- Moved initialisation of these into the rollout as for some reason the RsIdxTriggerName
-- was getting set to 2 somewhere even though it should be 4, currently
global RsIdxTriggerName
global RsIdxPartName

global ExpXMLDoc

struct RsExpCollParticle (name)

RsExpCollParts = #()
RsExpCollCurrPartNames = #()

idxGtaExpAttachToAll = getattrindex "Gta Explosion" "Attach to All"
idxGtaExpIgnoreDmgModel = getattrindex "Gta Explosion" "Ignore Damage Model"
idxGtaExpPlayOnParent = getattrindex "Gta Explosion" "Play On Parent"
idxGtaExpOnlyOnDmgModel = getattrindex "Gta Explosion" "Only on damage model"
idxGtaExpAllowRubberBulletFX = getattrindex "Gta Explosion" "Allow Rubber Bullet Shot FX"
idxGtaExpAllowElectricBulletFX = getattrindex "Gta Explosion" "Allow Electric Bullet Shot FX"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsExpSetupRoll "Explosion Setup"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/VFX_Toolkit#Explosion_Setup" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	checkbox chkAttachToAll	"Attach to all"
	checkbox chkIgnoreDmgModel "Ignore damage model"
	checkbox chkPlayOnParent "Play on parent"
	checkbox chkOnlyOnDmgModel "Only on damage model"
	checkbox chkAllowRubberBulletShotFX "Allow Rubber Bullet Shot FX"
	checkbox chkAllowElectricBulletShotFX "Allow Electric Bullet Shot FX"
	dropdownlist cboParticle "Name:"
	dropdownlist cboTrigger "Trigger:" items:#("Shot Point","Break","Destroy","Shot Generic")
	button btnClone "Clone" width:100
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn LoadEntries = (
	
		local filename = ""
		
		-- Hardcoded shit - Dave told me to do it!
		if (gRsCoreBranch.name == "dev_ng") or (gRsCoreBranch.name == "dev_gen9") then (
			filename = (RsConfigGetExportDir core:true) + "data/metadata/explosion.pso.meta"
		)else if (gRsCoreBranch.name == "dev") then (
			filename = (RsConfigGetCommonDir core:true) + "data/explosion.meta"
		)else(
			gRsUlog.LogError ("gRsCoreBranch.name was not 'dev', 'dev_ng' or 'dev_gen9'.")
		)
		
		gRsPerforce.sync filename silent:true
		
		if ( doesFileExist filename == false ) do
		(
			gRsULog.LogError (filename + " does not exist!")
			return false
		)
		ExpXMLDoc = XMLDocument()
		ExpXMLDoc.init()
		ExpXMLDoc.load filename
		xmlRoot = ExpXMLDoc.document.DocumentElement
		format "xmlRoot: %\n" (xmlRoot as string)
		
		
		if xmlRoot != undefined then 
		(
			expTagNode = RsGetXmlElement xmlRoot "aExplosionTagData"
			items = expTagNode.childnodes
			for i = 0 to ( items.Count - 1 ) do 
			(
				item = items.itemof(i)
				tagNode = RsGetXmlElement item "name"
				
				if tagNode != undefined do 
				(
					newItem = RsExpCollParticle tagNode.innerText
					append RsExpCollParts newItem
				)
			)
		)
		ExpXMLDoc.Release()
		ExpXMLDoc = undefined
		/*
		*/
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn SetType = (
		
		for item in selection do (
		
			if getattrclass item == "Gta Explosion" then (
				setattr item RsIdxTriggerName (cboTrigger.selection - 1)
				-- Set the particle name string attribute
				setattr item RsIdxPartName (RsExpCollCurrPartNames[cboParticle.selection])
			)
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn UpdateSelection = (
	
		if selection.count > 0 then (
				
			if getattrclass selection[1] == "Gta Explosion" then (
				
				particleName = getattr selection[1] RsIdxPartName
				notFound = true
				for partNameIdx = 1 to RsExpCollCurrPartNames.count while notFound do
				(
					if ( matchPattern RsExpCollCurrPartNames[partNameIdx] pattern:particleName ) do
					(
						cboParticle.selection = partNameIdx
						notFound = false
					)
				)
				
				val = (getattr selection[1] RsIdxTriggerName) + 1
				cboTrigger.selection = val
	
				chkAttachToAll.checked = getattr selection[1] idxGtaExpAttachToAll 
				chkIgnoreDmgModel.checked =  getattr selection[1] idxGtaExpIgnoreDmgModel
				chkPlayOnParent.checked = getattr selection[1] idxGtaExpPlayOnParent
				chkOnlyOnDmgModel.checked = getattr selection[1] idxGtaExpOnlyOnDmgModel	
				chkAllowRubberBulletShotFX.checked = getattr selection[1] idxGtaExpAllowRubberBulletFX
				chkAllowElectricBulletShotFX.checked = getattr selection[1] idxGtaExpAllowElectricBulletFX
			)
		)		
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn UpdateTypes = (
	
		RsExpCollCurrPartNames = #()
		
		for item in RsExpCollParts do (
				
			append RsExpCollCurrPartNames item.name
		)
	
		cboParticle.items = RsExpCollCurrPartNames
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	--------------------------------------------------------------
	-- Clone
	--------------------------------------------------------------	
	on btnClone pressed do (

		if selection.count > 0 then (

			if ( "Gta Explosion" == GetAttrClass selection[1] ) then
			(
				nodeToCopy = selection[1]
				CopyAttrs()

				nodeCopy = copy nodeToCopy

				if ( undefined == nodeCopy ) then
				(
					MessageBox "Error cloning Gta Explosion node.  Contact tools."
					return false
				)

				select nodeCopy
				PasteAttrs()
				-- Added this purely so that the UI updates properly rather than looking like
				-- it hasn't cloned correctly.
				clearSelection()
				selectMore nodeCopy
			)
		)
	)

	--------------------------------------------------------------
	-- Checkbox attribute changes
	--------------------------------------------------------------	
	on chkAttachToAll changed arg do (
	
		for obj in selection do ( if getattrclass obj == "Gta Explosion" do ( setattr obj idxGtaExpAttachToAll arg ) )
	)
	
	on chkIgnoreDmgModel changed arg do (
	
		for obj in selection do ( if getattrclass obj == "Gta Explosion" do ( setattr obj idxGtaExpIgnoreDmgModel arg ) )
	)

	on chkPlayOnParent changed arg do (
	
		for obj in selection do ( if getattrclass obj == "Gta Explosion" do ( setattr obj idxGtaExpPlayOnParent arg ) )
	)

	on chkOnlyOnDmgModel changed arg do (
	
		for obj in selection do ( if getattrclass obj == "Gta Explosion" do ( setattr obj idxGtaExpOnlyOnDmgModel arg ) )
	)
	
	on chkAllowRubberBulletShotFX changed arg do (

		for obj in selection do ( if getattrclass obj == "Gta Explosion" do ( setattr obj idxGtaExpAllowRubberBulletFX arg ) )
	)
	
	on chkAllowElectricBulletShotFX changed arg do (

		for obj in selection do ( if getattrclass obj == "Gta Explosion" do ( setattr obj idxGtaExpAllowElectricBulletFX arg ) )
	)
	
	on cboParticle selected arg do (
	
		SetType()
	)
	
	on cboTrigger selected arg do (

		SetType()
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	on RsExpSetupRoll open do (
		
		RsIdxTriggerName = getattrindex "Gta Explosion" "Trigger"
		RsIdxPartName = getattrindex "Gta Explosion" "Explosion Name"
		LoadEntries()
		UpdateTypes()
		callbacks.addscript #selectionSetChanged "::RsExpSetupRoll.UpdateSelection()" id:#RsExplosionObjectSelected
		UpdateSelection()
	)
	
	--------------------------------------------------------------
	-- shutdown of rollout
	--------------------------------------------------------------
	on RsExpSetupRoll close do 
	(
		callbacks.removescripts id:#RsExplosionObjectSelected
	)
	
	on RsExpSetupRoll rolledUp down do 
	(
		RsSettingWrite "RsExpSetupRoll" "rollup" (not down)
	)
)