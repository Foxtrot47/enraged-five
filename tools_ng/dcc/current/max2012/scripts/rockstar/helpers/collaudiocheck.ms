--
-- File:: rockstar/helpers/collaudiocheck.ms
-- Description:: Utility to list all the collision without audio materials applied from the selected objects
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
-- Date:: 20/7/2007
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
idxAudioMat = getattrindex "Gta Collision" "Audio Material"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
fn RsGetCollisionObjectsWithAudioMat inputlist outputlist = (

	for obj in inputlist do (
	
		if classof obj == Col_Mesh then (

			valAudioMat = getattr obj idxAudioMat	
			
			if valAudioMat == "" then (
			
				append outputlist obj
			)
		)
		
		RsGetCollisionObjectsWithAudioMat obj.children outputlist
	)
)

tosetlist = #()
ItemList = #()

fn RsGetList = (

	tosetlist = #()
	ItemList = #()
	
	RsGetCollisionObjectsWithAudioMat selection tosetlist
	
	for obj in tosetlist do (
		
		append ItemList obj.name
	)	
)

RsGetList()

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsMissingAudioMat "Audio Material Check"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Audio_Material_Check" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	listbox lstErrors "Missing:" items:ItemList height:18
	button btnRefresh "Refresh" width:100 across:2
	button btnClose "Close" width:100

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on lstErrors selected item do (

		if (tosetlist[item] != undefined) then (
			
			if isdeleted tosetlist[item] == false then (
			
				select tosetlist[item]
				max zoomext sel
			)
		)
	)

	on btnRefresh pressed do (
	
		RsGetList()
		
		lstErrors.items = ItemList
	)

	on btnClose pressed do (

		DestroyDialog RsMissingAudioMat 
	)	
)

CreateDialog RsMissingAudioMat width:400 modal:false