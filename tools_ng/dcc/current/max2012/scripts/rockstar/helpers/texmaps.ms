-- List Texmaps
-- Neal D Corbett
-- Rockstar Leeds
-- 15/10/2010

-- print out all the texmaps used by the selected objects

(
	local outputWindow = newScript()
	
	local texmaplist = #()
	local maxsizelist = #()
	local isbumplist = #()

	for obj in selection do 
	(
		RsGetTexMapsFromObj obj texmaplist maxsizelist isbumplist
	)

	sort texmaplist

	format "Texmaps used by selected objects:\n\n" to:outputWindow

	for texmap in texmaplist do 
	(
		format "%\n" texmap to:outputWindow
	)
)