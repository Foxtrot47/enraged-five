--
-- File:: rockstar/helpers/VertAnglePaint.ms
-- Description:: Paints verts by angle
--							Removed blending for now as Max is a dick with multiplying colour values
--
-- Author:: Stuart Macdonald <stuart.macdonald@rockstarnorth.com>
-- Date:: 04-01-09
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------	
	rollout VertAnglePaint_roll "Vert Angle Paint"
	(
		--////////////////////////////////////////////////////////////
		-- globals
		--////////////////////////////////////////////////////////////
		global selset
		
		
		--////////////////////////////////////////////////////////////
		-- interface
		--////////////////////////////////////////////////////////////
		hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Vert_Angle_Painter" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

		label Lcolour "Colour" pos:[80,28] width:40 height:16
		label lAngle "Angle min/max" pos:[144,28] width:80 height:16
		--label lBlend "Blend " pos:[240,28] width:48 height:16
		
		checkbutton bLayer1 "Layer 1" pos:[8,52] width:56 height:24 enabled:true
		colorPicker cLayer1 "" pos:[72,52] width:48 height:24 color:(color 0 0 0)
		label Llayer "Layer" pos:[16,28] width:32 height:16
		spinner sMin1 "" pos:[136,54] width:40 height:16 range:[0,100,0] type:#integer scale:1
		spinner sMax1 "" pos:[184,54] width:40 height:16 range:[0,100,90] type:#integer scale:1
		--spinner sBlend1 "" pos:[240,54] width:48 height:16 range:[0,100,0] type:#integer scale:1
		
		checkbutton bLayer2 "Layer 2" pos:[8,84] width:56 height:24 enabled:true
		colorPicker cLayer2 "" pos:[72,84] width:48 height:24 color:(color 0 0 255)
		spinner sMin2 "" pos:[136,86] width:40 height:16 range:[0,100,0] type:#integer scale:1
		spinner sMax2 "" pos:[184,86] width:40 height:16 range:[0,100,90] type:#integer scale:1
		--spinner sBlend2 "" pos:[240,86] width:48 height:16 range:[0,100,0] type:#integer scale:1
		
		checkbutton bLayer3 "Layer 3" pos:[8,116] width:56 height:24 enabled:true
		colorPicker cLayer3 "" pos:[72,116] width:48 height:24 color:(color 0 255 0)
		spinner sMin3 "" pos:[136,118] width:40 height:16 range:[0,100,0] type:#integer scale:1
		spinner sMax3 "" pos:[184,118] width:40 height:16 range:[0,100,90] type:#integer scale:1
		--spinner sBlend3 "" pos:[240,118] width:48 height:16 range:[0,100,0] type:#integer scale:1
		
		checkbutton bLayer4 "Layer 4" pos:[8,148] width:56 height:24 enabled:true
		colorPicker cLayer4 "" pos:[72,148] width:48 height:24 color:(color 0 255 255)
		spinner sMin4 "" pos:[136,150] width:40 height:16 range:[0,100,0] type:#integer scale:1
		spinner sMax4 "" pos:[184,150] width:40 height:16 range:[0,100,90] type:#integer scale:1
		--spinner sBlend4 "" pos:[240,150] width:48 height:16 range:[0,100,0] type:#integer scale:1
		
		checkbutton bLayer5 "Layer 5" pos:[8,180] width:56 height:24 enabled:true
		colorPicker cLayer5 "" pos:[72,180] width:48 height:24 color:(color 255 0 0)
		spinner sMin5 "" pos:[136,182] width:40 height:16 range:[0,100,0] type:#integer scale:1
		spinner sMax5 "" pos:[184,182] width:40 height:16 range:[0,100,90] type:#integer scale:1
		--spinner sBlend5 "" pos:[240,181] width:48 height:16 range:[0,100,0] type:#integer scale:1
		
		checkbutton bLayer6 "Layer 6" pos:[8,212] width:56 height:24 enabled:true
		colorPicker cLayer6 "" pos:[72,212] width:48 height:24 color:(color 255 0 255)
		spinner sMin6 "" pos:[136,214] width:40 height:16 range:[0,100,0] type:#integer scale:1
		spinner sMax6 "" pos:[184,214] width:40 height:16 range:[0,100,90] type:#integer scale:1
		--spinner sBlend6 "" pos:[240,214] width:48 height:16 range:[0,100,0] type:#integer scale:1
		
		checkbutton bLayer7 "Layer 7" pos:[8,244] width:56 height:24 enabled:true
		colorPicker cLayer7 "" pos:[72,244] width:48 height:24 color:(color 255 255 0)
		spinner sMin7 "" pos:[136,246] width:40 height:16 range:[0,100,0] type:#integer scale:1
		spinner sMax7 "" pos:[184,246] width:40 height:16 range:[0,100,90] type:#integer scale:1
		--spinner sBlend7 "" pos:[240,246] width:48 height:16 range:[0,100,0] type:#integer scale:1
		
		checkbutton bLayer8 "Layer 8" pos:[8,276] width:56 height:24 enabled:true
		colorPicker cLayer8 "" pos:[72,276] width:48 height:24 color:(color 255 255 255)
		spinner sMin8 "" pos:[136,278] width:40 height:16 range:[0,100,0] type:#integer scale:1
		spinner sMax8 "" pos:[184,278] width:40 height:16 range:[0,100,90] type:#integer scale:1
		--spinner sBlend8 "" pos:[240,278] width:48 height:16 range:[0,100,0] type:#integer scale:1
		
		button Paint_it "Paint it" pos:[90,308] width:64 height:32
		progressbar pbarPainting
		
		--////////////////////////////////////////////////////////////
		-- events
		--////////////////////////////////////////////////////////////
		on Paint_it pressed do
		(
			gc()
			selset = getCurrentSelection()
			
			for n = 1 to selset.count do
			(
				pbarPainting.value = 0
				if classOf selSet[n] == editable_Poly then
				(
					numVerts = selSet[n].GetNumVertices()
					--make copy to find vert normals as it's a mesh function
					vObjCheck = copy selset[n]
					convertToMesh vObjCheck

					for p = 1 to numVerts do
					(
 						vNorm1 = getNormal vObjCheck p 
						vnorm2 = normalize [vNorm1.x, vNorm1.y, 0]
 						vAngle = asin ( dot vNorm1 vNorm2 )
						
						pbarPainting.value = (100.0 / numVerts) * p
							
 						--Layer 1
						if bLayer1.checked then
						(
							if (sMin1.value <= vAngle ) and (sMax1.value >= vAngle ) then 
							(
								polyOp.setVertColor selSet[n] 9 p cLayer1.color
							)
						)
						
						--Layer 2
						if bLayer2.checked then
						(
							if (sMin2.value <= vAngle ) and (sMax2.value >= vAngle ) then 
							(
								polyOp.setVertColor selSet[n] 9 p cLayer2.color
							)
						)
						
						--Layer 3
						if bLayer3.checked then
						(
							if (sMin3.value <= vAngle ) and (sMax3.value >= vAngle ) then 
							(
								polyOp.setVertColor selSet[n] 9 p cLayer3.color
							)
						)
 						
						--Layer 4
						if bLayer4.checked then
						(
							if (sMin4.value <= vAngle ) and (sMax4.value >= vAngle ) then 
							(
								polyOp.setVertColor selSet[n] 9 p cLayer4.color
							)
						)
						
						--Layer 5
						if bLayer5.checked then
						(
							if (sMin5.value <= vAngle ) and (sMax5.value >= vAngle ) then 
							(
								polyOp.setVertColor selSet[n] 9 p cLayer5.color
							)
						)
						
						--Layer 6
						if bLayer6.checked then
						(
							if (sMin6.value <= vAngle ) and (sMax6.value >= vAngle ) then 
							(
								polyOp.setVertColor selSet[n] 9 p cLayer6.color
							)
						)
						
						--Layer 7
						if bLayer7.checked then
						(
							if (sMin7.value <= vAngle ) and (sMax7.value >= vAngle ) then 
							(
								polyOp.setVertColor selSet[n] 9 p cLayer7.color
							)
						)
						
						--Layer 8
						if bLayer8.checked then
						(
							if (sMin8.value <= vAngle ) and (sMax8.value >= vAngle ) then 
							(
								polyOp.setVertColor selSet[n] 9 p cLayer8.color
							)
						)
					)
					
					delete vObjCheck
				)
				else
				(
					if classOf selSet[n] == editable_mesh then 
					(
						numVerts = getNumVerts selSet[n]
						
						for p = 1 to numVerts do
						(
							vNorm1 = getNormal selSet[n] p 
							vnorm2 = normalize [vNorm1.x, vNorm1.y, 0]
							vAngle = asin ( dot vNorm1 vNorm2 )
						
							pbarPainting.value = (100.0 / numVerts) * p
							
							--Layer 1
							if bLayer1.checked then
							(
								if (sMin1.value <= vAngle ) and (sMax1.value >= vAngle ) then 
								(
									meshOp.setVertColor selSet[n] 9 p cLayer1.color
								)
							)
						
							--Layer 2
							if bLayer2.checked then
							(
								if (sMin2.value <= vAngle ) and (sMax2.value >= vAngle ) then 
								(
									meshOp.setVertColor selSet[n] 9 p cLayer2.color
								)
							)
						
							--Layer 3
							if bLayer3.checked then
							(
								if (sMin3.value <= vAngle ) and (sMax3.value >= vAngle ) then 
								(
									meshOp.setVertColor selSet[n] 9 p cLayer3.color
								)
							)
							
							--Layer 4
							if bLayer4.checked then
							(
								if (sMin4.value <= vAngle ) and (sMax4.value >= vAngle ) then 
								(
									meshOp.setVertColor selSet[n] 9 p cLayer4.color
								)
							)
						
							--Layer 5
							if bLayer5.checked then
							(
								if (sMin5.value <= vAngle ) and (sMax5.value >= vAngle ) then 
								(
									meshOp.setVertColor selSet[n] 9 p cLayer5.color
								)
							)
							
							--Layer 6
							if bLayer6.checked then
							(
								if (sMin6.value <= vAngle ) and (sMax6.value >= vAngle ) then 
								(
									meshOp.setVertColor selSet[n] 9 p cLayer6.color
								)
							)
						
							--Layer 7
							if bLayer7.checked then
							(
								if (sMin7.value <= vAngle ) and (sMax7.value >= vAngle ) then 
								(
									meshOp.setVertColor selSet[n] 9 p cLayer7.color
								)
							)
						
							--Layer 8
							if bLayer8.checked then
							(
								if (sMin8.value <= vAngle ) and (sMax8.value >= vAngle ) then 
								(
									meshOp.setVertColor selSet[n] 9 p cLayer8.color
								)
							)
						)					
					)
					else
					(
						messagebox "Object has to be a editable_poly or editable_mesh to paint it"
					)
				)
				
				-- now update vert color display, odd way to do it but update doesn't work
				selSet[n].showVertexColors = on
				selSet[n].vertexColorType = 5
				selSet[n].vertexColorMapChannel = 9
				forceCompleteRedraw doDisabled:true
				
				pbarPainting.value = 0
			)
		)
	)

	if VertAnglePaint_float != undefined then closerolloutfloater VertAnglePaint_float
	VertAnglePaint_float = newrolloutfloater "Vert Angle Paint" 260 400 100 100
	addrollout VertAnglePaint_roll VertAnglePaint_float

