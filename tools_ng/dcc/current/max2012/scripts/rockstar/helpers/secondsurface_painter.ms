--
-- File:: rockstar/helpers/secondsurface_painter.ms
-- Description:: Rockstar Second Surface Painter
--							Second surface painting utility
--
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 26/1/2009
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Global
-----------------------------------------------------------------------------
	global SecondSurfaceObj 
	
-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
	fn VerifySelection = (
		
		sel = selection as array
		if sel.count != 1 then (
			messagebox "Please select one mesh only"
			return false
		) 
		SecondSurfaceObj = sel[1]
		
		if classof SecondSurfaceObj != Col_Mesh then (
			messagebox "Selected object is not a collision mesh"
			return false
		)
		
		
		return true
	)
	
	fn ConvertAndApplyModifier = (
		max modify mode
		col2mesh SecondSurfaceObj
		
		p = VertexPaint()
		p.mapchannel = 10
		addmodifier SecondSurfaceObj p
		vPainttool = VertexPaintTool()

		vPainttool.paintColor = color 0.0 0.0 0.0 
		vPainttool.mapDisplayChannel = 10
		vPainttool.curPaintMode = 1
				
		aaa = vPainttool.keepToolboxOpen	
		vPainttool.keepToolboxOpen = aaa
	)