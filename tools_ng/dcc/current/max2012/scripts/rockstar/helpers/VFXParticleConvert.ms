--
-- File:: rockstar/helpers/VFXParticleConvert.ms
-- Description:: VFX Conversion
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 15 May 2007
--
-----------------------------------------------------------------------------

filein "startup/objects/util/RsRageParticleObject.ms"

idxFXName = getattrindex "RAGE Particle" "Name"

fn ConvertToRSNParticleHelper allSelection: =
(
	if unsupplied==allSelection then
		allSelection = selection as array
	newSelection = #()
	progressstart "Converting particle objects"
	numConverted = 0
	numToConvert = allSelection.count
	for o in allSelection do
	(
		if RAGE_Particle!=(classof o) then 
			continue
		
		if ""==getattr o idxFXName then
			gRsUlog.LogWarning ("Name of emitter "+o.name+" is invalid. It will be the default value.") context:o
		
		select o
		o.layer.current = true
		copyAttrs()
		local newHelper = RsRageParticleHelper()
		select newHelper
		pasteAttrs()
		if undefined != o.parent then
			append o.parent.children newHelper
		newHelper.transform = o.transform
		newHelper.name = o.name
		append newSelection newHelper
		delete o
		progressupdate ((numConverted / numToConvert)*100)
	)
	select newSelection
	progressend()
)
	
-- VFX Conversion Wizard Rollout Definition
rollout RsVFXConversionWizRoll "VFX Particle Conversion Wizard"
(

	-------------------------------------------------------------------------------
	-- Interface Controls
	-------------------------------------------------------------------------------
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/VFX_Toolkit#Particle_Conversion_Wizard" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button 			btnConvert "Convert" width:100

	-------------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------------

	on btnConvert pressed do
	(
		ConvertToRSNParticleHelper()
	) -- btnConvert pressed handler
	
	-- Save rolled-up state:
	on RsVFXConversionWizRoll rolledUp down do 
	(
		RsSettingWrite "RsVFXConversionWizRoll" "rollup" (not down)
	)

) -- End of rollout definition

-- End of script
