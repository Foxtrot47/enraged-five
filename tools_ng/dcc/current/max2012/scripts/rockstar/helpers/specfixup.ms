--
-- File:: rockstar/helpers/specfixup.ms
-- Description:: Fixup specular shaders to take advantage of the specular colour mask
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
-- Date:: 17/5/2007
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
--filein "rockstar/util/material.ms" -- loaded on startup by startup.ms

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
	fn PopulateMaterialList obj mat matlist = (
	
		if classof mat == Rage_Shader then (
		
			add = false
		
			varcount = RstGetVariableCount mat
			
			for i = 1 to varcount do (
			
				varname = RstGetVariableName mat i
				
				if varname == "Specular Texture" then (
								
					add = true
					exit
				)
			)
		
			if add then (
		
				for checkmat in matlist do (

					if rexAreMatsEqual checkmat mat then (

						add = false
						exit
					)
				)
			)
			
			if add then (
			
				append matlist mat
			)
		
		) else if classof mat == MultiMaterial then (
		
			matidlist = #()
		
			RsMatGetMapIdsUsedOnObject obj matidlist

			for i = 1 to matidlist.count do (

				matidx = finditem mat.materialIDList matidlist[i]

				if matidx != 0 then (

					PopulateMaterialList obj mat.materiallist[matidx] matlist
				)
			)			
		)
	)

	--------------------------------------------------------------
	fn DoFixup = (
	
		txdlist = #()
		txdobjlist = #()

		mapObjectList = #()	
		RsGetMapObjects rootnode.children mapObjectList
	
		RsGetTxdList mapObjectList txdlist txdobjlist
		
		for i = 1 to txdlist.count do (
		
			txd = txdlist[i]
			matlist = #()
								
			for obj in txdobjlist[i] do (
				
				PopulateMaterialList obj obj.material matlist
			)
					
			matidxlist = #()
			texmapidxlist = #()
			specvaridxlist = #()
			
			for k = 1 to matlist.count do (
			
				item = matlist[k]
			
				submap = undefined
				texmapidx = -1
				specvaridx = -1
				
				numMaps = getnumsubtexmaps item
				
				for j = 1 to numMaps do (
				
					varname = RsGetTexMapNameFromRsShader item j
					
					if varname == "Specular Texture" then (
					
						submap = getsubtexmap item j
						texmapidx = j
						
						if classof submap == CompositeTexturemap then (
						
							submap = undefined
							texmapidx = -1
						)
						
						exit
					)
				)
				
				if submap != undefined then (
				
					okToAdd = false
				
					count = RstGetVariableCount item
					
					for j = 1 to count do (
					
						varname = RstGetVariableName item j
						
						if varname == "specular map intensity mask color" then (
						
							var = RstGetVariable item j
							specvaridx = j
							
							if var == [1,0,0] then (
							
								okToAdd = true
							)
						)
					)
					
					if okToAdd == false then (
					
						specvaridx = -1
						submap = undefined
					)
				)
								
				if texmapidx != -1 and specvaridx != -1 then (
				
					append matidxlist k		
					append texmapidxlist texmapidx
					append specvaridxlist specvaridx 
				)
			)
			
			if texmapidxlist.count > 1 then (
							
				numComposites = texmapidxlist.count / 3 as integer
				
				if mod texmapidxlist.count 3 > 1 then (
				
					numComposites = numComposites + 1
				)
				
				currentOffset = 0
				
				for i = 1 to numComposites do (
					
					amountToDo = texmapidxlist.count - currentOffset
				
					if amountToDo > 3 then amountToDo = 3

					newmap = compositetexturemap()
					
					newmap.maplist.count = amountToDo
					newmap.mapEnabled.count = amountToDo
					
					for j = 1 to amountToDo do (
					
						oldmat = matlist[matidxlist[currentOffset + j]]
					
						newmap.mapEnabled[j] = true
						newmap.maplist[j] = getsubtexmap oldmat texmapidxlist[currentOffset + j]
						
						if j == 1 then (
						
							RstSetVariable oldmat specvaridxlist[currentOffset + j] [1,0,0]
						) else if j == 2 then (
						
							RstSetVariable oldmat specvaridxlist[currentOffset + j] [0,1,0]
						) else (
						
							RstSetVariable oldmat specvaridxlist[currentOffset + j] [0,0,1]
						)
						
						setsubtexmap oldmat texmapidxlist[currentOffset + j] newmap 
					)
				
					currentOffset = currentOffset + 3
				)
			)
		)
	)
	
	--------------------------------------------------------------
	fn DoList = (
	
		txdlist = #()
		txdobjlist = #()

		mapObjectList = #()	
		RsGetMapObjects rootnode.children mapObjectList
	
		RsGetTxdList mapObjectList txdlist txdobjlist
		
		totalMaps = 0
		totalSlot1Maps = 0
		
		for i = 1 to txdlist.count do (
		
			txd = txdlist[i]
			matlist = #()
										
			for obj in txdobjlist[i] do (
				
				PopulateMaterialList obj obj.material matlist
			)
			
			matidxlist = #()
			texmapidxlist = #()
			specvaridxlist = #()
			
			for k = 1 to matlist.count do (
			
				item = matlist[k]
			
				submap = undefined
				texmapidx = -1
				specvaridx = -1
				
				numMaps = getnumsubtexmaps item
				
				for j = 1 to numMaps do (
				
					varname = RsGetTexMapNameFromRsShader item j
					
					if varname == "Specular Texture" then (
					
						submap = getsubtexmap item j
						texmapidx = j
						
						if classof submap != CompositeTexturemap then (
						
							submap = undefined
							texmapidx = -1
						)
						
						exit
					)
				)
				
				if submap != undefined then (
				
					okToAdd = false
				
					count = RstGetVariableCount item
					
					for j = 1 to count do (
					
						varname = RstGetVariableName item j
						
						if varname == "specular map intensity mask color" then (
						
							var = RstGetVariable item j
							specvaridx = j
							okToAdd = true
						)
					)
					
					if okToAdd == false then (
					
						specvaridx = -1
						submap = undefined
					)
				)
				
				if texmapidx != -1 and specvaridx != -1 then (
				
					submap = getsubtexmap item texmapidx
					var = RstGetVariable item specvaridx
				
					print "========================================="
					print ("material " + item.name + " in txd " + txd)
					print "composite map list:"
					
					for j = 1 to submap.maplist.count do (
					
						print submap.maplist[j].filename
					)
					
					print "using:"
				
					totalMaps = totalMaps + 1
	
					if var == [1,0,0] then (
						
						totalSlot1Maps = totalSlot1Maps + 1
						print submap.maplist[1].filename
					) else if var == [0,1,0] then (
					
						print submap.maplist[2].filename
					) else (
					
						print submap.maplist[3].filename
					)
				
					append matidxlist k		
					append texmapidxlist texmapidx
					append specvaridxlist specvaridx 
				)
			)
		)	

		print ((totalMaps as string) + " stored in " + (totalSlot1Maps as string) + " map slots")
	)
	
	--------------------------------------------------------------
	fn DoRemove = (
	
		txdlist = #()
		txdobjlist = #()

		mapObjectList = #()	
		RsGetMapObjects rootnode.children mapObjectList
	
		RsGetTxdList mapObjectList txdlist txdobjlist
		
		for i = 1 to txdlist.count do (
		
			txd = txdlist[i]
			matlist = #()
								
			for obj in txdobjlist[i] do (
				
				PopulateMaterialList obj obj.material matlist
			)
					
			matidxlist = #()
			texmapidxlist = #()
			specvaridxlist = #()
			
			for k = 1 to matlist.count do (
			
				item = matlist[k]
			
				submap = undefined
				texmapidx = -1
				specvaridx = -1
				
				numMaps = getnumsubtexmaps item
				
				for j = 1 to numMaps do (
				
					varname = RsGetTexMapNameFromRsShader item j
					
					if varname == "Specular Texture" then (
					
						submap = getsubtexmap item j
						texmapidx = j
						
						if classof submap != CompositeTexturemap then (
						
							submap = undefined
							texmapidx = -1
						)
						
						exit
					)
				)
				
				if submap != undefined then (
				
					okToAdd = false
				
					count = RstGetVariableCount item
					
					for j = 1 to count do (
					
						varname = RstGetVariableName item j
						
						if varname == "specular map intensity mask color" then (
						
							var = RstGetVariable item j
							specvaridx = j				
							okToAdd = true
						)
					)
					
					if okToAdd == false then (
					
						specvaridx = -1
						submap = undefined
					)
				)
								
				if texmapidx != -1 and specvaridx != -1 then (
				
					append matidxlist k		
					append texmapidxlist texmapidx
					append specvaridxlist specvaridx 
				)
			)
			
			for i = 1 to texmapidxlist.count do (
				
				oldmat = matlist[matidxlist[i]]
						
				oldval = RstGetVariable oldmat specvaridxlist[i]
				
				if oldval == [1,0,0] then (
									
					newmap = (getsubtexmap oldmat texmapidxlist[i]).mapList[1]
				) else if oldval == [0,1,0] then (
				
					newmap = (getsubtexmap oldmat texmapidxlist[i]).mapList[2]
				) else (
				
					newmap = (getsubtexmap oldmat texmapidxlist[i]).mapList[3]
				)
				
				RstSetVariable oldmat specvaridxlist[i] [1,0,0]
				setsubtexmap oldmat texmapidxlist[i] newmap
			)
		)		
	)