--
-- File:: rockstar/helpers/shaderchange.ms
-- Description:: Shader Variable Parameter Changer
--
-----------------------------------------------------------------------------

rollout ShaderChangeRoll "Shader Attribute Changer"
(
	-----------------------------------------------------------------------------------------
	-- LOCALS
	-----------------------------------------------------------------------------------------
	local materials = #()
	-----------------------------------------------------------------------------------------
	-- Interface
	-----------------------------------------------------------------------------------------
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Change_Shaders#Shader_Attribute_Changer" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)	
		
	dropdownlist lstShader "Shader:" items:#( "All on selected Objects" )
	dropdownlist lstName "Attribute Name:" 
	spinner spnValue "Target Value to Change:"range:[0,5000,0.01]
	spinner spnTolerance "Tolerance:" range:[0,5000,0.01]
	spinner spnNewValue "New Value:" range:[0,5000,100]
	button btnSet "Change" across:3 width:95 align:#left offset:[-5,0]
	button btnSetBelow "If Below Target" width:95 align:#left
	button btnSetAbove "If Above Target" width:95 align:#left offset:[5,0]
	
	-----------------------------------------------------------------------------------------
	-- Methods
	-----------------------------------------------------------------------------------------
	
	-- POPULATE THE LIST OF ATTRIBUTE VARIBLES 
	fn populateVarList = 
	(
		if (lstShader.selected == "All on selected Objects") then 
		(
			local varArray = #()
			for mat in materials where (mat!=undefined) do for n=1 to (RstGetVariableCount mat) where (RstGetVariableType mat n == "float") do appendIfUnique varArray (RstGetVariableName mat n)	
			lstName.items = varArray 
		)
		else
		(
			local selShader = undefined
			-- GET SELCTED SHADER 
			for mat in materials while selShader == undefined do if (RstGetShaderName mat == lstShader.selected) do selShader = mat
			-- GET FLOAT VARS OFF SHADER 	
			if (selShader != undefined) do 
			(
				lstName.items = (for n=1 to (RstGetVariableCount selShader) where (RstGetVariableType selShader n == "float") collect RstGetVariableName selShader n)
			)
		)
	)
	
	-- Populate combo box from our material list
	fn populateShaderList = 
	(
		materials = #()
		
		for obj in $selection do
		(
			if (GetAttrClass obj == "Gta Object") then
			(
				materialList = RsGetMaterialsOnObjFaces obj
				join materials materialList 
			)
		)
		
		items = #("All on selected Objects")

		materials = makeUniqueArray materials
		for mat in materials do
		(
			if(ClassOf mat == Rage_Shader) then
			(
				shaderName = RstGetShaderName mat
				appendIfUnique items shaderName
			)
			
		)
		sort items
		lstShader.items = items
		
		-- GET VARS 
		populateVarList()
	)
	

	fn modifyShaderValue material valname checkvalue tolerance newvalue type = 
	(		
		if classof material == Multimaterial then 
		(		
			for submat in material.materiallist do  modifyShaderValue submat valname checkvalue tolerance newvalue type		
		) 
		else if classof material == Rage_Shader then 
		(		
			-- DHM -- 2007-05-22 -- Check the shader name has been selected or "All"
			local shaderName = RstGetShaderName material
			local selectedShaderName = lstShader.selected
			
			if ((1 != lstShader.selection) and (shaderName != selectedShaderName)) then return false -- Ignore submaterial		
			-- End
		
			varcount = RstGetVariableCount material
		
			for i = 1 to varcount do  (
			
				varName = RstGetVariableName material i
				varType = RstGetVariableType material i
				
				if toupper varName == toupper valname and varType == "float" then 
				(					
					currvalue = RstGetVariable material i										
					if (type == #tol) AND (currvalue >= (checkvalue - tolerance)) and (currvalue <= (checkvalue + tolerance)) then RstSetVariable material i newvalue	
					if (type == #above) AND (currvalue >= checkvalue) then RstSetVariable material i newvalue
					if (type == #below) AND (currvalue <= checkvalue) then RstSetVariable material i newvalue							
				)
			)
		)
	)

	-- CHANGE ON SELECTION
	fn setSelected type = 
	(
		for obj in selection do 
		(		
			if classof obj == Editable_mesh or classof obj == Editable_poly do 
			(			
				modifyShaderValue obj.material lstName.selected spnValue.value spnTolerance.value spnNewValue.value	type
			)
		)
	)
	
	-----------------------------------------------------------------------------------------
	-- Events
	-----------------------------------------------------------------------------------------
	on btnSet pressed do setSelected #tol	
	on btnSetAbove pressed do setSelected #above
	on btnSetBelow pressed do setSelected #below
		
	on lstShader selected arg do populateVarList()	

	on ShaderChangeRoll open do 
	(	
		populateShaderList()	
		-- Install handler for selection updates
		callbacks.addScript #selectionSetChanged "ShaderChangeRoll.populateShaderList()" id:#shaderChangeSelection
	)
	
	on ShaderChangeRoll close do 
	(	
		-- Remove handler for selection updates
		callbacks.removeScripts id:#shaderChangeSelection
	)
)
-- End of script
