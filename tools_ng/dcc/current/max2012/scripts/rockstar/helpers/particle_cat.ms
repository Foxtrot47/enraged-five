--
-- File:: rockstar/helpers/particle_cat.ms
-- Description:: Particle setup utility
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- DHM - 30/04/2007
-- GtaParticle plugin changes to support emitter domain visualisation
--  o Added VFX Database rollout (to rebuild VFX in-memory database)
--  o Added addition Set handler code to mark object dirty
--
-- LPXO: 20/7/2009
-- Switch to using RAGE Particle
--
-- AJM: 09/02/2012
-- Removed the Setting of the particle type as it's done using the paramblock
-- of the RAGE Particle now.  Also added in Check PTFX Asset Names to use ULog
-- to show any objects that has particles on it from multiple ptfx dictionaries
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"
filein "pipeline/util/FxUtils.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
RsCollCats = #()
RsCollItems = #()
RsIdxParticleName = getattrindex "RAGE Particle" "Name"

struct RsCollParticle (name, type, assetName)

RsCollParts = #()
RsCollCurrPartNames = #()

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout RsPartSetupRoll "Particle Setup"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/VFX_Toolkit#Particle_Setup" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnClone "Clone" width:140
	button btnCheckAssetNames "Check PTFX Asset Names" width:140
	button btnFixController "Reset script controllers" width:140
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn LoadEntries = (
	
		filename = RsGetEntityFxInfoPath()
		if not (RsFileExists filename) then (
			return false
		)else(
			format "Entity.fx found at: %\n" filename
		)

		intFile = openfile filename mode:"rb"

		newType = #none
		
		while eof intFile == false do (
		
			intLine = RsRemoveSpacesFromStartOfString(readline intFile)
			
			if intLine.count > 0 and intLine[1] != "#" then (

				intTokens = filterstring intLine " \t"
				
				--print intTokens[1]
								
				if intTokens[1] == "ENTITYFX_AMBIENT_PTFX_START" then (

					newType = #ambient
				) else if intTokens[1] == "ENTITYFX_COLLISION_PTFX_START" then (

					newType = #collision
				) else if intTokens[1] == "ENTITYFX_SHOT_PTFX_START" then (

					newType = #shot
				) else if intTokens[1] == "FRAGMENTFX_BREAK_PTFX_START" then (

					newType = #break
				) else if intTokens[1] == "FRAGMENTFX_DESTROY_PTFX_START" then (

					newType = #destruction
				) else if intTokens[1] == "ENTITYFX_ANIM_PTFX_START" then (

					newType = #anim
				) else if intTokens[1] == "ENTITYFX_RAYFIRE_PTFX_START" then (

					newType = #rayfire
				) 
				else if intTokens[1] == "ENTITYFX_SHOT_PTFX_END" or  intTokens[1] == "ENTITYFX_AMBIENT_PTFX_END" or  intTokens[1] == "ENTITYFX_COLLISION_PTFX_END" or intTokens[1] == "FRAGMENTFX_BREAK_PTFX_END" or intTokens[1] == "FRAGMENTFX_DESTROY_PTFX_END" or intTokens[1] == "ENTITYFX_ANIM_PTFX_END" or intTokens[1] == "ENTITYFX_RAYFIRE_PTFX_END" then (

					newType = #none
				) else  if intTokens.count > 1 then (

					newItem = RsCollParticle intTokens[1] newType intTokens[2]
					append RsCollParts newItem
				)
			)
		)
		
		close intFile
	)
	
	fn UpdateAssetName = (
	
		if ( RsCollparts.count > 0 ) do
		(
			for i = 1 to RsCollParts.count do
			(
				if ( RsCollParts[i].name == cboParticle.selected ) do
				(
					edtAssetName.text = RsCollparts[i].assetName						
				)
			)	
		)
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- Clone
	--------------------------------------------------------------	
	on btnClone pressed do (
	
		if selection.count > 0 then (
		
			if ( "RAGE Particle" == GetAttrClass selection[1] ) then
			(
				nodeToCopy = selection[1]
				CopyAttrs()
				
				nodeCopy = copy nodeToCopy
			
				if ( undefined == nodeCopy ) then
				(
					MessageBox "Error cloning RAGE Particle node.  Contact tools."
					return false
				)

				clearSelection()
				selectMore nodeCopy
				PasteAttrs()
			)
		)
	)
	
	fn collectParticleHelpers levelObj &objList = (
	
		for c in levelObj.children do 
		(
			if (getattrclass c == "RAGE Particle") do (append objList c)
			collectParticleHelpers c &objList
		)
	)
	
	on btnCheckAssetNames pressed do (

		gRsUlog.init "PTFX Asset Name Check" appendToFile:false
		hasErrors = false
		struct ParticleAndAsset ( name, assetName )
		
		
		-- Check the object children to see if they have particles and if so, if they 
		-- would have the same ptfx asset name after export
		for obj in rootnode.children do
		(		
			attachedParticles = #()
			collectParticleHelpers obj &attachedParticles
			
			-- 1st dimension is object name, 2nd is the particles on the object
			particles = #()
			
			if ( attachedParticles.count > 0 ) do
			(
				local objAssetName = undefined
				for attachedPart in attachedParticles do
				(
					-- Get the particle type for this particle
					partType = getattr attachedPart RsIdxParticleName
					partAssetName = undefined

					-- Find out what asset name this particle's type belongs to
					for partIdx = 1 to RsCollParts.count do
					(
						if ( RsCollParts[partIdx].name == partType ) do
						(
							partAssetName = RsCollParts[partIdx].assetName
						)
					)
					-- Create a struct so for the warnings it can contain the particle and show
					-- the asset name associated with it to quickly see which ones are different
					particleStruct = ParticleAndAsset attachedPart.name partAssetName
					appendIfUnique particles particleStruct

					if ( objAssetName != undefined ) then
					(
						-- If one doesn't match then stop checking and add object
						-- and all its particle children to the array to be displayed 	
						if ( objAssetName != partAssetName ) do
						(									
							for p in particles do 
							(
								gRsULog.LogWarning ("Particle: " + p.name + "\t PTFX Asset Name: " + p.assetName) context:p
							)
							gRsULog.LogError ("More than 1 PTFX Asset name would be used for: " + obj.name) context:obj
							hasErrors = true
						)
					)
					else
					(
						objAssetName = partAssetName
					)
				)			
			)
		)
		-- Show ULog this way rather than with Validate() so that it isn't modal
		if ( hasErrors ) then ( ShowRsUlogDialog() )		
		else ( Messagebox "No problems with PTFX asset names on objects!" ) 
	)
	
	--------------------------------------------------------------
	-- Rollup Startup
	--------------------------------------------------------------
	on RsPartSetupRoll open do 
	(
		LoadEntries()
	)
	
	-- Save rolled-up state:
	on RsPartSetupRoll rolledUp down do 
	(
		RsSettingWrite "RsPartSetupRoll" "rollup" (not down)
	)
	
	on btnFixController pressed do
	(
		for obj in objects where RsRageParticleHelper == classof obj do
			setPropertyController obj "active" (boolcntrl())
		messagebox "Finished resetting controllers"
	)
)

-- VFX Database Rollout
rollout RsPartDBRoll "VFX Database"
(

	-- Interface
	button btnRebuild "Rebuild VFX DB" width:100
	label lblInfo2 "Use this rebuild button if you get\nlatest from Alienbrain to refresh\nthe loaded VFX. This saves\nyou having to restart 3DS Max." align:#left height:60

	-- Event handlers
	on btnRebuild pressed do
	(
		RsRebuildParticlesWithReport()
	)
	
	-- Save rolled-up state:
	on RsPartDBRoll rolledUp down do 
	(
		RsSettingWrite "RsPartDBRoll" "rollup" (not down)
	)
)
