--
-- File:: rockstar/helpers/decal.ms
-- Description:: Decal setup utility
-- Date: 14/10/2011
--
-- Author:: Adam Munson <adam.munson@rockstarnorth.com
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
RsIdxDecalName = getattrindex "RsDecal" "Decal Name"
RsIdxDecalTriggerName = getattrindex "RsDecal" "Trigger"

struct RsDecal (name, type)

RsDecals = #()
RsDecalCurrNames = #()

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout RsDecalSetupRoll "Decal Setup"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp			"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/VFX_Toolkit#Decal_Setup" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	checkbox chkBreak			"Break" checked:true
	checkbox chkCollision		"Collision" checked:true
	checkbox chkDestroy 		"Destroy" checked:true
	checkbox chkShot 			"Shot" checked:true
	dropdownlist cboDecals	 	"Name:"
	button btnSet 				"Set" width:100
	button btnClone 			"Clone" width:100
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn LoadEntries = (
	
		filename = RsGetEntityFxInfoPath()
			if not (RsFileExists filename) then (
				return false
			)else(
				format "Entity.fx found at: %\n" filename
			)

		intFile = openfile filename mode:"rb"
		
		if(intFile == undefined) then (
			gRsUlog.LogError("Error opening file: "+ filename)
			return false			
		)

		newType = #none
		
		while eof intFile == false do (
		
			intLine = RsRemoveSpacesFromStartOfString(readline intFile)
			
			if intLine.count > 0 and intLine[1] != "#" then (

				intTokens = filterstring intLine " \t"
				
				--print intTokens[1]
								
				if intTokens[1] == "FRAGMENTFX_BREAK_DECAL_START" then (

					newType = #break
				) else if intTokens[1] == "ENTITYFX_COLLISION_DECAL_START" then (

					newType = #collision
				) else if intTokens[1] == "FRAGMENTFX_DESTROY_DECAL_START" then (

					newType = #destroy
				) else if intTokens[1] == "ENTITYFX_SHOT_DECAL_START" then (

					newType = #shot
				)
				else if intTokens[1] == "FRAGMENTFX_BREAK_DECAL_END" or  intTokens[1] == "ENTITYFX_COLLISION_DECAL_END" or  intTokens[1] == "FRAGMENTFX_DESTROY_DECAL_END" or intTokens[1] == "ENTITYFX_SHOT_DECAL_END" then (

					newType = #none
				) else  if intTokens.count > 1 then (

					newItem = RsDecal intTokens[1] newType
					append RsDecals newItem
				)
			)
		)		
		close intFile
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn SetType = (
		
		for item in selection do (
		
			if getattrclass item == "RsDecal" then (
			
				for i = 1 to RsDecals.count do (
				
					if RsDecals[i].name == cboDecals.items[cboDecals.selection] then (
					
						setattr item RsIdxDecalName RsDecals[i].name
						
						item.name = "decal_" + RsDecals[i].name
						
						setval = 0
						
						if RsDecals[i].type == #break then (
						
							setval = 0
						) else if RsDecals[i].type == #collision then (
						
							setval = 1
						) else if RsDecals[i].type == #destroy then (
						
							setval = 2
						) else if RsDecals[i].type == #shot then (
						
							setval = 3
						) 					
						
						setattr item RsIdxDecalTriggerName setval
					)
				)
			)
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn UpdateTypes = (
	
		--print "updating"
	
		RsDecalCurrNames = #()
		
		for item in RsDecals do (
				
			show = false

			if chkBreak.checked and item.type == #break then show = true
			if chkCollision.checked and item.type == #collision then show = true
			if chkDestroy.checked and item.type == #destroy then show = true
			if chkShot.checked and item.type == #shot then show = true

			if show then (

				append RsDecalCurrNames item.name
			)
		)
	
		sort RsDecalCurrNames
	
		cboDecals.items = RsDecalCurrNames
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkBreak changed arg do (
	
		UpdateTypes()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkCollision changed arg do (
	
		UpdateTypes()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkDestroy changed arg do (
	
		UpdateTypes()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkShot changed arg do (
	
		UpdateTypes()
	)	
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnSet pressed do (
	
		SetType()
	)
	
	--------------------------------------------------------------
	-- Clone
	--------------------------------------------------------------	
	on btnClone pressed do (
	
		if selection.count > 0 then (
		
			if ( "RsDecal" == GetAttrClass selection[1] ) then
			(
				nodeToCopy = selection[1]
				CopyAttrs()
				
				nodeCopy = copy nodeToCopy
			
				if ( undefined == nodeCopy ) then
				(
					MessageBox "Error cloning RsDecal node.  Contact tools."
					return false
				)

				clearSelection()
				selectMore nodeCopy
				PasteAttrs()
			)
		)
	)
	
	--------------------------------------------------------------
	-- Rollup Startup
	--------------------------------------------------------------
	on RsDecalSetupRoll open do (
	
		LoadEntries()
		UpdateTypes()
	)
	
	-- Save rolled-up state:
	on RsDecalSetupRoll rolledUp down do 
	(
		RsSettingWrite "RsDecalSetupRoll" "rollup" (not down)
	)
)