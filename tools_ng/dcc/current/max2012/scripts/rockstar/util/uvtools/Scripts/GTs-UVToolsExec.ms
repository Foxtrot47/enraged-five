--BUG LIST
--********
--Information panel reverts to a colour used by another opened dialog?!

--TO DO LIST
--**********
--GameTools title page - produce and implement
--GameTools help file - produce and implement

--RECENTLY ADDED/FIXED
--********************
--'Do you want to add a bitmap to the diffuse map slot?' message box when adding a new material
--'Wire' and '2-Sided' checkboxes implemented
--Compress Materials not working? Ensure it updates GameTools UI once completed
--'Is this Alpha imbedded' querybox for alpha textures
--'select all polys using current ID number" - implement
--open editablemesh/poly ID selection - implement
--Reload Texture button - implement
--Resource collecting of materials bitmaps - implement
--Reroot textures - implement
try (destroydialog GameToolsExecutiveRoll) ; catch()

fileIn "rockstar/export/settings.ms"

global LBut=0, RealignToggle=0, DialogOffset=[0,0], resizetoggle=0
global ButtonNumber=0
-- DHM -- 10/01/2007 -- 3DS Max 9 Upgrade (#image returns user image directory, not system)
-- MWW -- 12/01/2010 -- Now maps this to wildwest directory
global ImageDir= ( (pathConfig.GetDir #scripts) + "/rockstar/util/uvtools/images/" )
global GameToolsBackdropBuffer= ( openbitmap (ImageDir + "GameToolsBackdrop.bmp") )
global GameToolsBackdropClicked= ( openbitmap (ImageDir + "GameToolsBackdropClicked.bmp") )
global GameToolsBackdropMASK= ( openbitmap (ImageDir + "GameToolsBackdropMASK.bmp") )
global GameToolsBackdrop= ( openbitmap (ImageDir + "GameToolsBackdrop.bmp") )
global EmptySlot=bitmap 52 49

global DrawPressedButton
global GameToolsExecutiveRoll
global UVOldFaceSel=0
global ForceUpdate=1
global UpdateError=0

global MessageMousePos=[0,0]
global DialogScale=[95,256]
global FaceBuffer=#(), FaceIDs=#()
global OBJ, ALLBITS, matval
global texture=bitmap 45 45, otexture=bitmap 45 45
global newtext=0, addmatsup=0, copiedmaterial
global StoredUVCoords=#(), StoredObject
global UndoNo, StoredUVCount, StoredFaces=#(), StoredID=#()
global ThumbToggle=0, ThumbToggleUV=0
global oops=0
global ShowTexTog=0
global VXMin=0.0
global VXMax=1.0
global VYMin=0.0
global VYMax=1.0
global VertList=#()

global ButtonBoundingBox=#(

	--minimise and close buttons
	[2,2], [20,20],
	[2,22],[20,40],
	[2,42],[20,60],

	--UVTools buttons
	[24,7],[57,40],[59,7],[92,40],
	[24,43],[57,76],[59,43],[92,76],
	[24,78],[57,111],[59,78],[92,111],
	[24,113],[57,146],[59,113],[92,146],
	[24,148],[57,181],[59,148],[92,181],
	[24,183],[57,216],[59,183],[92,216],
	[24,218],[57,251],[59,218],[92,251],

	--Wire/2-sided tick boxes
	[131,88],[141,98],
	[181,88],[191,98],

	--Face Selection - Editablemesh selection+select faces by ID
	[173,168],[186,180],
	[188,168],[201,180],

	--Material Editor Buttons
	[111,198],[137,224],
	[139,198],[165,224],
	[167,198],[193,224],
	[195,198],[221,224],
	[223,198],[249,224],

	[111,226],[137,252],
	[139,226],[165,252],
	[167,226],[193,252],
	[195,226],[221,252],
	[223,226],[249,252],
	
	--bitmap
	[198,85],[249,137]
)

-- AM - Moved this here as globals weren't initialised before the functions were evaluated - required
-- UV Tools to be closed and then reopened until it worked properly.
filein ( "rockstar\\util\\uvtools\\Scripts\\GTs-UVToolsExecFunctions.ms" )

--***********************************************************************************************************************************************

fn DrawPressedButton ButtonNo =
(
	--added GRS
	ButtonNumber = ((ButtonNumber as string) as integer)
	--end of added GRS

	tmpval=(ButtonNo*2)-1
	for Loop=ButtonBoundingBox[tmpval].y to ButtonBoundingBox[(tmpval+1)].y do
	(
		tmppixels=getpixels GameToolsBackdropClicked [(ButtonBoundingBox[tmpval].x),Loop] (ButtonBoundingBox[(tmpval+1)].x-ButtonBoundingBox[tmpval].x)
		setpixels GameToolsBackdrop [(ButtonBoundingBox[tmpVal].x),Loop] tmppixels
	)
	GameToolsBackdrop
)--fn DrawPressedButton

--***********************************************************************************************************************************************
rollout GameToolsExecutiveRoll ""
(
	bitmap GT_UI "" pos:[-2,-4] bitmap:gametoolsbackdrop enabled:false	

	edittext MultiMatName "" width:143 pos:[104,14] bold:true
	progressbar ProgressB "" pos:[-100,0] orient:#vertical width:15 height:90 color:(color 255 255 0)
	edittext SubMatName "" width:143 pos:[104,62]
	bitmap SubMatBit "" pos:[196,83] bitmap:EmptySlot
	spinner SelfIllumVal "" type:#integer range:[0,100,0] pos:[150,100] width:43 height:20
	spinner OpacityVal "" type:#integer range:[0,100,0] pos:[150,120] width:43 height:20
	dropdownlist MapType "" pos:[108,140] width:85 height:100 items:#("Diffuse","Opacity","Bump","Reflection","_______________","Ambient","_______________","Specular","Glossiness","Soften") 
	spinner LevelVal "" type:#integer range:[0,999,0] pos:[131,165] width:39 height:20
	colorpicker MapColor "" pos:[192,140] width:56 color:(color 0 0 255)
	spinner IDVal "" type:#integer range:[0,9999,1] pos:[211,165] width:37 height:20
	label UVInformation "UVToolsV3.0 - Written by A.Oakley" pos:[3,257] align:#left
	checkbox WireChk "" pos:[129,83] width:13 height:14
	checkbox TwoSidedChk "" pos:[179,83] width:13 height:14
	timer UVWatch "" pos:[-100,-100] interval:100

	on WireChk changed state do
	(
		try
		(
			local LevNode
			case (MultiMatName.enabled) of
			(
				true: LevNode=$selection[1].material[IDVal.value]
				false: LevNode=$selection[1].material
			)
			LevNode.wire=WireChk.state
		)
		catch()				
	)

	on TwoSidedChk changed state do
	(
		try
		(
			local LevNode
			case (MultiMatName.enabled) of
			(
				true: LevNode=$selection[1].material[IDVal.value]
				false: LevNode=$selection[1].material
			)
			LevNode.TwoSided=TwoSidedChk.state
		)
		catch()				
	)

	on MapColor changed ColVal do
	(
	
		try
		(
			local LevNode
			case (MultiMatName.enabled) of
			(
				true: LevNode=$selection[1].material[IDVal.value]
				false: LevNode=$selection[1].material
			)		

			case (MapType.selected) of
			(
				"Diffuse": LevNode.diffuse=ColVal
				"Ambient": LevNode.Ambient=ColVal
				"Specular": LevNode.specular=ColVal
			)
		)
		catch()		
	
	)

	on LevelVal changed LevVal do
	(
		try
		(
			local LevNode
			case (MultiMatName.enabled) of
			(
				true: LevNode=$selection[1].material[IDVal.value]
				false: LevNode=$selection[1].material
			)		

			case (MapType.selected) of
			(
				"Opacity": (LevNode.opacity=LevVal ; if LevVal>100.0 do LevelVal.value=100.0 )
				"Specular": LevNode.specularlevel=LevVal
				"Glossiness": (LevNode.glossiness=LevVal ; if LevVal>100.0 do LevelVal.value=100.0 )
				"Bump": LevNode.BumpMapAmount=LevVal
				"Reflection": LevNode.ReflectionMapAmount=LevVal
				"Soften": (LevNode.soften=(LevVal/100.0) ; if LevVal>100.0 do LevelVal.value=100.0 )
			)
		)
		catch()	
	)

	on OpacityVal changed OpacVal do
	(		
		try
		(	
			case (MultiMatName.enabled) of
			(
				true: ($selection[1].material[IDVal.value].opacity=OpacVal)
				false: ($selection[1].material.opacity=OpacVal)
			)		
		)
		catch()
	)
	
	on SelfIllumVal changed IllumVal do
	(	
		try
		(
			case (MultiMatName.enabled) of
			(
				true: ($selection[1].material[IDVal.value].selfIllumAmount=IllumVal)
				false: ($selection[1].material.selfIllumAmount=IllumVal)
			)	
		)
		catch()
	)
	
	on MultiMatName changed MultiText do ( try($selection[1].material.name=MultiText);catch() )
	
	on SubMatName changed SubMatText do 
	(
		try
		(
			case (classof $selection[1].material) of
			(
				Standardmaterial: $selection[1].material.name=SubMatText
				Multimaterial: $selection[1].material[IDVal.value].name=SubMatText
			)
		)
		catch() 
	)

	on bitmap pressed do selectbitmap()
	
	on MapType selected tmp do ForceUpdate=1
	
	on UVWatch tick do
	(
		try
		(				
			if $selection[1].selectedfaces[1].index!=UVOldFaceSel or ForceUpdate==1 do
			(
				UpdateError=0
				IDVal.value=UpdateMaterialUI()
				UVOldFaceSel=$selection[1].selectedfaces[1].index
				ForceUpdate=0
			)
		)
		catch( if UpDateError==0 do (UVOldFaceSel=0 ; UpDateError=1 ; UpdateMaterialUI() ; ForceUpdate=0 ) )
	)

	on IDVal changed state do
	(
		local TotMats=1
		try
		(
			local MatN=$selection[1]
			try 
			(
				if (classof $selection[1].material)==multimaterial then 
				(
					TotMats=$selection[1].material.count
					IDVal.value=mod IDVal.value $selection[1].material.count
					if IDVal.value==0 do IDVal.value=$selection[1].material.count
				)
				else
				(
					IDVal.value=1
				)
			)
			catch(IDVal.value=1)
			
			UpdateMaterialUI()
			case (classof MatN) of
			(
				Editable_mesh: ( try(for Loop in MatN.selectedfaces do (setfacematID MatN Loop.index IDVal.value) );catch())
				Editable_Poly: (try(for Loop in MatN.selectedfaces do (polyop.setfacematID MatN Loop.index IDVal.value) );catch())	
			)
			update MatN

			ForceUpdate=1
			UVInformation.text="Materials: "+(IDVal.value as string)+" of "+(TotMats as string)
		)
		catch()
	)

	on GameToolsExecutiveRoll mousemove tmp do 
	(
		if LBut==1 and ButtonNumber==0 do 
		(
			if RealignToggle==1 then (RealignToggle=0 ; DialogOffset=tmp) else (setdialogPos GameToolsExecutiveRoll (mouse.screenpos-DialogOffset))	
		) 
	)--on GameToolsExecutiveRoll mousemove...


	on GameToolsExecutiveRoll lbuttondown tmp do 	
	(
		try(destroydialog MessageRollout);catch()
		disablesceneredraw()
		LBut=1 ; RealignToggle=1 
		local PickColour=getpixels GameToolsBackdropMASK tmp 1
		if (PickColour[1].r)!=0 do
		(
			ButtonNumber=PickColour[1].r
			UVInformation.pos=UVInformation.pos
			MultiMatName.pos=MultiMatName.pos
			SubMatName.pos=SubMatName.pos
			SubMatBit.pos=SubMatBit.pos
			SelfIllumVal.pos=SelfIllumVal.pos
			OpacityVal.pos=OpacityVal.pos
			MapType.pos=MapType.pos
			LevelVal.pos=LevelVal.pos
			MapColor.pos=MapColor.pos
			IDVal.pos=IDVal.pos	
			WireChk.pos=WireChk.pos
			TwoSidedChk.pos=TwoSidedChk.pos
			--removed GRS
			--GT_UI.bitmap=copy (DrawPressedButton PickColour[1].r)
			--end of removed GRS
		)
		enablesceneredraw()
	)

	on GameToolsExecutiveRoll lbuttonup tmp do 
	(
		local PickColour=getpixels GameToolsBackdropMASK tmp 1
		try(if PickColour[1].r!=ButtonNumber do ButtonNumber=0);catch(ButtonNumber=0)
		disablesceneredraw()
		
		--*0 = No button pressed
		--*1 = Close button pressed
		--*2 = Reduce UVTools UI to show only UV tool buttons
		--*3 = Minimise button pressed
		--*4 = Copy UV Coordinates
		--5 = Paste UV Coordinates
		--6 = Flip UV Coordinates Horizontally
		--7 = Flip UV Coordinates Vertically
		--8 = Rotate UV Coordinates in 90 degree increments
		--9 = Compress selected objects Vertex colour table
		--10 = Compress selected objects Texture Vertex table
		--11= Toggle display of alpha textures on selected object
		--12= Apply an unwrap modifier to the selected object
		--13= Undo UVTools operation
		--14= Information regarding UVTools Exec edtion
		--15= Align selected faces to viewport
		--16= View map selected faces
		--*29= Copy material to designated material editor slot
		--*30= Clear out material editor
		
		LBut=0
		try
		(

		-- added by GRS
		ButtonNumber = ((ButtonNumber as string) as integer)
		-- end of added by GRS
		case (ButtonNumber as integer) of
		(
			0 :	()
			1 :	( destroydialog GameToolsExecutiveRoll ; close GameToolsBackdrop ; gc light:true )
			2 :	( if GameToolsExecutiveRoll.width!=95 then ( GameToolsExecutiveRoll.height=256 ; GameToolsExecutiveRoll.width=95 ; DialogScale=[95,256]) else ( GameToolsExecutiveRoll.width=256 ; GameToolsExecutiveRoll.height=272 ; DialogScale=[256,272])  )
			3 :	( if GameToolsExecutiveRoll.width<DialogScale.x then ( GameToolsExecutiveRoll.height=DialogScale.y ;  GameToolsExecutiveRoll.width=DialogScale.x  ) else ( GameToolsExecutiveRoll.height=256 ; GameToolsExecutiveRoll.width=19 )  )
			4 :	UVCopy()
			5 : UVPaste()
			6 :	FlipX()
			7 :	FlipY()
			8 : RotateIt()
			9 : FaceMapIt()
			10: ( UVInformation.text="Compressing Vertex Color table - please wait..." ; VCOptimize() ; ProgressB.pos=[-100,0])
			11: ( UVInformation.text="Compressing Texture Vertex table - please wait..." ; UVOptimize() ; ProgressB.pos=[-100,0])
			12: ShowBitmaps()
			13: UnwrapUVCoords()
			14: (UndoTaskExec UndoNo)
			15: DisplayGameToolsInfo()
			16: ( try ( NormalAlignExec $ ; DisableSceneRedraw() ; NormalAlign $ ) ; catch() ; enableSceneRedraw() )
			17: ( DisableSceneRedraw() ; try ( PasteViewFacesExec (FlattenViewVerts()) ) ; catch() ; EnableSceneRedraw() ) 
			20: ( if (classof $selection[1])==Editable_mesh then (meshops.selectbyID $) else (polyops.selectbyID $) ; update $)
			21: SetSelectedID()
			22: ( try ( CopiedMaterial=selection[1].material ; UVInformation.text="Material Copied to memory") ; catch ( UVInformation.text="ERROR! Couldn't copy material to memory!"))
			23: ( try ( selection[1].material=CopiedMaterial ; UVInformation.text="Material pasted successfully" ) ; catch(UVInformation.text="ERROR! Couldn't paste material to your model")  )
			24: ( AddImage() ; ForceUpdate=1 )
			25: ( RemoveImage() ; ForceUpdate=1 )
			26: ( if $selection.count!=0 do (CollectTextures()) ; ForceUpdate=1 )
			27: (
					case (classof $selection[1].material) of
					(
						standardmaterial: (
							LevNode=selection[1].material
							case Maptype.selected of
							(
								"Opacity": LevNode.OpacityMap.bitmap=openbitmap LevNode.OpacityMap.bitmap.filename
								"Specular": LevNode.specularLevelMap.bitmap=openbitmap LevNode.specularLevelMap.bitmap.filename
								"Glossiness": LevNode.GlossinessMap.bitmap=openbitmap LevNode.GlossinessMap.bitmap.filename
								"Bump": LevNode.BumpMap.bitmap=openbitmap LevNode.BumpMap.bitmap.filename
								"Reflection": LevNode.ReflectionMap.bitmap=openbitmap LevNode.ReflectionMap.bitmap.filename
								"Diffuse": LevNode.DiffuseMap.bitmap=openbitmap LevNode.DiffuseMap.bitmap.filename
								)
						)
						multimaterial: (
							LevNode=selection[1].material[IDVal.value]
							case Maptype.selected of
							(
								"Opacity": LevNode.OpacityMap.bitmap=openbitmap LevNode.OpacityMap.bitmap.filename
								"Specular": LevNode.specularLevelMap.bitmap=openbitmap LevNode.specularLevelMap.bitmap.filename
								"Glossiness": LevNode.GlossinessMap.bitmap=openbitmap LevNode.GlossinessMap.bitmap.filename
								"Bump": LevNode.BumpMap.bitmap=openbitmap LevNode.BumpMap.bitmap.filename
								"Reflection": LevNode.ReflectionMap.bitmap=openbitmap LevNode.ReflectionMap.bitmap.filename
								"Diffuse": LevNode.DiffuseMap.bitmap=openbitmap LevNode.DiffuseMap.bitmap.filename
							)
						)--multimaterial:
					)--case (classof $selection...
					forceupdate=1
				)--27:
			28: (CompressMaterials() ; ProgressB.pos=[-100,0])
			29: ( max mtledit ; try(meditmaterials[medit.getactivemtlslot()]=$.material);catch() )
			30: ( for Loop=1 to meditmaterials.count do (meditmaterials[Loop]=standard()) )
			31: ( if $selection.count!=0 do (ReRootTextures()) ; ForceUpdate=1 )

			32: (
					try
					(
						if SubMatName.enabled==true do
						(
						local LevNode
						case (MultiMatName.enabled) of
						(
							true: LevNode=$selection[1].material[IDVal.value]
							false: LevNode=$selection[1].material
						)
						
						if keyboard.controlpressed==true then
						(
							try
							(
								if keyboard.altpressed==true then
								(
									AreYouSure=querybox "This will remove the map slot's bitmap\nAre You Sure?"
									
									if AreYouSure==true do
									(
										case (MapType.selected) of
										(
											"Opacity": LevNode.OpacityMap=undefined
											"Specular": LevNode.specularLevelMap=undefined
											"Glossiness": LevNode.GlossinessMap=undefined
											"Bump": LevNode.BumpMap=undefined
											"Reflection": LevNode.ReflectionMap=undefined
											"Diffuse": LevNode.DiffuseMap=undefined
										)
									)
								)
								else
								(
									case (MapType.selected) of
									(
										"Opacity": display LevNode.OpacityMap.bitmap
										"Specular": display LevNode.specularLevelMap.bitmap
										"Glossiness": display LevNode.GlossinessMap.bitmap
										"Bump": display LevNode.BumpMap.bitmap
										"Reflection": display LevNode.ReflectionMap.bitmap
										"Diffuse": display LevNode.DiffuseMap.bitmap
									)								
								)

							)
							catch()
						)
						else
						(
							local NewMap
							case (MapType.selected) of
							(
								"Opacity": (
									NewMap=selectbitmap()
									local AlphaImbedded=querybox "Is the alpha imbedded into the bitmap file?"
									if NewMap!=undefined do 
									(
										LevNode.OpacityMap = bitmaptexture filename:NewMap.filename
										case AlphaImbedded of
										(
											true: (
												LevNode.opacitymap.MonoOutput=1
												LevNode.opacitymap.RGBOutput=1				
											)--true...
											false: (
												LevNode.opacitymap.MonoOutput=0
												LevNode.opacitymap.RGBOutput=0			
											)--false...
										)--case...
									) --if NewMap...
								)--"Opacity":...
								"Specular": (NewMap=selectbitmap() ; if NewMap!=undefined do (  LevNode.specularLevelMap = bitmaptexture filename:NewMap.filename) )
								"Glossiness": (NewMap=selectbitmap() ; if NewMap!=undefined do (  LevNode.GlossinessMap = bitmaptexture filename:NewMap.filename) )
								"Bump": (NewMap=selectbitmap() ; if NewMap!=undefined do (  LevNode.BumpMap = bitmaptexture filename:NewMap.filename) )
								"Reflection": (NewMap=selectbitmap() ; if NewMap!=undefined do (  LevNode.ReflectionMap = bitmaptexture filename:NewMap.filename) )
								"Diffuse": (NewMap=selectbitmap() ; if NewMap!=undefined do (  LevNode.DiffuseMap = bitmaptexture filename:NewMap.filename) )
							)
							showtexturemap LevNode on						
						)--else
					)--if SubMatName.enabled==true do...
					)--try...
					catch()
					ForceUpdate=1
				)--32:
	
		)
		)
		catch(UVInformation.text="UVToolsV3.0 - Written by A.Oakley")
		
		enablesceneredraw()
		ButtonNumber=0
		if DrawPressedButton!=0 do 
		(
			SubMatBit.pos=SubMatBit.pos
			-- removed GRS
			--GameToolsBackdrop=copy GameToolsBackdropBuffer
			-- end of removed GRS
			UVInformation.pos=UVInformation.pos
			MapColor.pos=MapColor.pos
			MultiMatName.pos=MultiMatName.pos
			MultiMatName.pos=MultiMatName.pos
			SubMatName.pos=SubMatName.pos
			SelfIllumVal.pos=SelfIllumVal.pos
			OpacityVal.pos=OpacityVal.pos
			MapType.pos=MapType.pos
			LevelVal.pos=LevelVal.pos
			IDVal.pos=IDVal.pos
			WireChk.pos=WireChk.pos
			TwoSidedChk.pos=TwoSidedChk.pos
			GT_UI.bitmap=GameToolsBackdrop
		)
	)--on GameToolsExecutiveRoll lbuttonup do...

	on GameToolsExecutiveRoll Rbuttonup tmp do 
	(
		local PickColour=((getpixels GameToolsBackdropMASK tmp 1)[1].r) as integer
		--local ToolTipColor=(color 178 192 192)
		ToolTipColor=(color 102 108 111)
		case PickColour of
		(
			1: ( TextMessage "Close down UVTools"  ToolTipColor )
			2: ( TextMessage "Hide/unhide the mini material editor window" ToolTipColor )
			3: ( TextMessage "Minimise/maximise UVTool window" ToolTipColor )
			4: ( TextMessage "Copies UV Coordinates, from your selected faces, to memory" ToolTipColor )
			5: ( TextMessage "Pastes copied UV Coordinates to your selected faces" ToolTipColor )
			6: ( TextMessage "Flip UV Coordinates, of your selected faces, horizontally" ToolTipColor )
			7: ( TextMessage "Flip UV Coordinates, of your selected faces, vertically" ToolTipColor )
			8: ( TextMessage "Rotate UV Coordinates, of your selected faces, clockwise" ToolTipColor )
			9: ( TextMessage "Face map selected faces" ToolTipColor )
			10:( TextMessage "Compress the vertex colour table on your selected object" ToolTipColor )
			11:( TextMessage "Compress the texture vertex table on your selected object" ToolTipColor )
			12:( TextMessage "Texture display toggle on selected objects" ToolTipColor )
			13:( TextMessage "Unwrap UVcoordinates for editing" ToolTipColor )
			14:( TextMessage "Undo UVTools operation" ToolTipColor )
			15:( TextMessage "Copyright information and help for UVTools" ToolTipColor )
			16:( TextMessage "Align selected faces to the viewport" ToolTipColor )
			17:( TextMessage "Map selected faces using the viewport as projection" ToolTipColor )
			18:( TextMessage "Toggle wireframe in the presently displayed material" ToolTipColor )
			20:( TextMessage "Select by material ID - Extended options" ToolTipColor )
			21:( TextMessage "Select faces that contain current ID number" ToolTipColor )
			22:( TextMessage "Makes a copy of your selected model's texture to memory" ToolTipColor )
			23:( TextMessage "Pastes the copied material, in memory, to your selected object" ToolTipColor )				
			24:( TextMessage "Add a new material to your model" ToolTipColor )
			25:( TextMessage "Remove material from the selected faces on your model" ToolTipColor )
			26:( TextMessage "Collect selected model's textures and copies them to a directory of your choice" ToolTipColor )
			27:( TextMessage "Reload currently selected material slot" ToolTipColor )
			28:( TextMessage "Compresses your models materials by removing duplicate and unused entries" ToolTipColor )				
			29:( TextMessage "Copy your model's material to the selected slot in the material editor" ToolTipColor )
			30:( TextMessage "Remove all the materials from the material editor" ToolTipColor )
			31:( TextMessage "Reroot all the material textures to a new selected directory" ToolTipColor )
			32:( TextMessage "Click to change/add map - holding CTRL will view the map and ALT+CTRL will remove the map" ToolTipColor )

		)--case PickColour...			

	)--in GameToolsExecutiveRoll Rbuttondown...
)--rollout GameToolsExecutiveRoll...


--***********************************************************************************************************************************************
createdialog GameToolsExecutiveRoll pos:(mouse.screenpos+[-10,-65]) width:95 height:256 fgcolor:(color 255 255 255) bgcolor:(color 102 108 111) style:#() --bitmap:GameToolsBackdrop bmpstyle:#bmp_tile style:#()
