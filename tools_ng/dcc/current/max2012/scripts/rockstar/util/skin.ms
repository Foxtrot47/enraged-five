-- Rockstar Skin Utilities
-- Rockstar North
-- 20/9/2005
-- by Greg Smith

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsNodeGetSkinModifier node = (

	for modi in node.modifiers do (
	
		if classof(modi) == Skin then (
		
			return modi
		)
	)
	
	return undefined
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsSkinGetBoneNames skinmod = (

	numBones = skinops.getnumberbones skinmod
	listBoneName = #()
	
	for i = 1 to numBones do (
		
		boneName = skinops.getbonename skinmod i 0
		append listBoneName boneName
	)	
	
	return listBoneName
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsGetSkinnerConfigName obj = (

	return (maxfilepath + "skinner_" + obj.name + ".info")
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsGetBone skinMod index = (

	boneName = skinops.getbonename skinMod index 0	
	
	return getnodebyname boneName exact:true
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsMirrorBoneName boneName = (

	case(boneName) of (
	"Char": return "Char"
	"Char Pelvis": return "Char Pelvis"
	"Char L Thigh": return "Char R Thigh"
	"Char L Calf": return "Char R Calf"
	"Char L Foot": return "Char R Foot"
	"Char L Toe0": return "Char R Toe0"
	"Char L Toe0Nub": return "Char R Toe0Nub"
	"L Calf Roll": return "R Calf Roll"
	"Char R Thigh": return "Char L Thigh"
	"Char R Calf": return "Char L Calf"
	"Char R Foot": return "Char L Foot"
	"Char R Toe0": return "Char L Toe0"
	"Char R Toe0Nub": return "Char L Toe0Nub"
	"R Calf Roll": return "L Calf Roll"
	"Char Spine": return "Char Spine"
	"Char Spine1": return "Char Spine1"
	"Char Spine2": return "Char Spine2"
	"Char L Clavicle": return "Char R Clavicle"
	"Char L UpperArm": return "Char R UpperArm"
	"Char L Forearm": return "Char R Forearm"
	"Char L Hand": return "Char R Hand"
	"Char L Finger0": return "Char R Finger0"
	"Char L Finger01": return "Char R Finger01"
	"Char L Finger02": return "Char R Finger02"
	"Char L Finger0Nub": return "Char R Finger0Nub"
	"Char L Finger1": return "Char R Finger1"
	"Char L Finger11": return "Char R Finger11"
	"Char L Finger12": return "Char R Finger12"
	"Char L Finger1Nub": return "Char R Finger1Nub"
	"Char L Finger2": return "Char R Finger2"
	"Char L Finger21": return "Char R Finger21"
	"Char L Finger22": return "Char R Finger22"
	"Char L Finger2Nub": return "Char R Finger2Nub"
	"Char L Finger3": return "Char R Finger3"
	"Char L Finger31": return "Char R Finger31"
	"Char L Finger32": return "Char R Finger32"
	"Char L Finger3Nub": return "Char R Finger3Nub"
	"Char L ForeTwist": return "Char R ForeTwist"
	"Char L ForeTwist1": return "Char R ForeTwist1"
	"L UpperArmRoll": return "R UpperArmRoll"
	"Char Neck": return "Char Neck"
	"Char Head": return "Char Head"
	"Char HeadNub": return "Char HeadNub"
	"Char R Clavicle": return "Char L Clavicle"
	"Char R UpperArm": return "Char L UpperArm"
	"Char R Forearm": return "Char L Forearm"
	"Char R Hand": return "Char L Hand"
	"Char R Finger0": return "Char L Finger0"
	"Char R Finger01": return "Char L Finger01"
	"Char R Finger02": return "Char L Finger02"
	"Char R Finger0Nub": return "Char L Finger0Nub"
	"Char R Finger1": return "Char L Finger1"
	"Char R Finger11": return "Char L Finger11"
	"Char R Finger12": return "Char L Finger12"
	"Char R Finger1Nub": return "Char L Finger1Nub"
	"Char R Finger2": return "Char L Finger2"
	"Char R Finger21": return "Char L Finger21"
	"Char R Finger22": return "Char L Finger22"
	"Char R Finger2Nub": return "Char L Finger2Nub"
	"Char R Finger3": return "Char L Finger3"
	"Char R Finger31": return "Char L Finger31"
	"Char R Finger32": return "Char L Finger32"
	"Char R Finger3Nub": return "Char L Finger3Nub"
	"Char R ForeTwist": return "Char L ForeTwist"
	"Char R ForeTwist1": return "Char L ForeTwist1"
	"R UpperArmRoll": return "L UpperArmRoll"
	"Neck_Roll": return "Neck_Roll"
	"Spine_2_roll": return "Spine_2_roll"
	)	
	
	return undefined 
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsMirrorBoneIDList skinMod idList = (

	listBoneName = RsSkinGetBoneNames skinMod
	idMirrorList = #()
	
	for id in idList do (
	
		mirrorName = RsMirrorBoneName (skinops.getbonename skinMod id 0)
		
		if mirrorName == undefined then return undefined 
		
		newid = finditem listBoneName mirrorName
		
		if newid == 0 then return undefined 
		
		append idMirrorList newid
	)
	
	return idMirrorList
)