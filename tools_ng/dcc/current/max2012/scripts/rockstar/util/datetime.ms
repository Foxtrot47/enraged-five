-- Rockstar DateTime Utilities
-- Rockstar North
-- 23 May 2007
-- by David Muir <david.muir@rockstarnorth.com>
--

---------------------------------------------------------------------------------
-- Return date as string
--
-- [return] Date as string (DD/MM/YYYY on UK configured locale)
---------------------------------------------------------------------------------
fn RsGetDate = (

	dosCommand "date /t >MAXScript_RsGetDate.tmp"
	fp = openfile "MAXScript_RsGetDate.tmp" mode:"rt"
	if ( undefined == fp ) then
		return undefined
	
	dateStr = readline fp
	close fp
	deleteFile "MAXScript_RsGetDate.tmp"

	-- Trim space chars
	dateStr = trimleft dateStr
	dateStr = trimright dateStr

	dateStr
)

---------------------------------------------------------------------------------
-- Return reversed date as string (not suitable for filenames/paths)
--
-- [return] Date as string (YYYY/MM/DD on UK configured locale)
---------------------------------------------------------------------------------
fn RsGetDateReverse = (

	dateStr = RsGetDate()
	strs = filterString dateStr "-/"
	
	ss = stringStream ""
	format "%/%/%" strs[3] strs[2] strs[1] to:ss
	
	(ss as string)
)

---------------------------------------------------------------------------------
-- Return reversed date for use in file/directory names
--
-- [return] Date as string (YYYY-MM-DD on UK configured locale)
---------------------------------------------------------------------------------
fn RsGetDateReverseFilesystem = (

	dateStr = RsGetDateReverse()
	strs = filterString dateStr "/"
	
	ss = stringStream ""
	format "%-%-%" strs[1] strs[2] strs[3] to:ss

	(ss as string)
)

---------------------------------------------------------------------------------
-- Return reversed date for use in file/directory names
--
-- [return] Date as string (YYYYMMDD on UK configured locale)
---------------------------------------------------------------------------------
fn RsGetDateReverseFilesystem2 = (

	dateStr = RsGetDateReverse()
	strs = filterString dateStr "/"
	
	ss = stringStream ""
	format "%%%" strs[1] strs[2] strs[3] to:ss

	(ss as string)
)

---------------------------------------------------------------------------------
-- Return time as string
--
-- [return] Time as string (HH:MM on UK configured locale)
---------------------------------------------------------------------------------
fn RsGetTime = (

	dosCommand "time /t >MAXScript_RsGetTime.tmp"
	fp = openfile "MAXScript_RsGetTime.tmp" mode:"rt"
	if ( undefined == fp ) then
		return undefined
	
	timeStr = readline fp
	close fp
	deleteFile "MAXScript_RsGetTime.tmp"

	-- Trim space chars
	timeStr = trimleft timeStr
	timeStr = trimright timeStr

	timeStr
)

-- End of script
