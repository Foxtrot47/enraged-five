--SM_Path_Deform
--
-- Stuart Macdonald
--
-- v1.0 11/03/10
--
-- v1.1 10/11/10
-- Added spacing function for placement
-- Fixed placement fo first object in array
--
-- to do
-- 
	
try (destroyDialog RsPathDeformRoll) catch ()

rollout RsPathDeformRoll "Path Deform/Array" width:250
(	
	-------------------------------------------------------------------------------------
	-- LOCALS
	-------------------------------------------------------------------------------------
	local theObj, theObjOp, theSplineObj, objLength, splLength
	local setDeform = true
	local setArray = false
	local setTrim = false
	
	-------------------------------------------------------------------------------------
	-- FILTERS
	-------------------------------------------------------------------------------------	
	fn objFilter obj =  isProperty obj "mesh"
	fn filterSpline obj = (isKindOf obj Shape)

	-------------------------------------------------------------------------------------
	--UI
	-------------------------------------------------------------------------------------
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:RsPathDeformRoll.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel versionNum:1.10 versionName:"NormalPancake" wiki:"" filename:(getThisScriptFilename())		

	label lb1 "Object : " width:100 across:2 align:#right offset:[-10,3]
	pickbutton btnPickObj "Pick object" filter:objFilter autoDisplay:true width:180 align:#left offset:[-60,0]
		tooltip:"Pick object to deform/array\n(Right-click to clear)" message:"Pick object to deform/array" 

	label lb2 "Path : " width:100 across:2 align:#right offset:[0,3]	
	pickbutton btnPickSpline "Pick path" filter:filterSpline autoDisplay:true  width:180 align:#left offset:[-60,0]
		tooltip:"Pick path for deform/array/(Right-click to clear)" message:"Pick path for deform/array"
	
	
	group "Options:"
	(
		checkbox chkUseCopy "Use Copy of Object" enabled:true checked:false 
		checkbox chkDeform "Deform" enabled:true across:2			
		checkbox chkTrimObj  "Trim Object" enabled:true 
		checkbox chkArrayObj "Array" enabled:true across:2
		spinner spnObjOffset "Spacing:" align:#left width:100 range:[-99.99,99.99,0.0] type:#float
		checkbox chkMergeArray "Merge Array To One Object" enabled:false
		
	)
	
	progressBar prog_Deform value:0 width:(RsPathDeformRoll.width - 8) align:#center
	button btnGo "Path Deform Object" height:30 offset:[-1,0] width:(RsPathDeformRoll.width - 8)
	
	-------------------------------------------------------------------------------------
	--Functions
	-------------------------------------------------------------------------------------
	fn goDeformObject theObj objOffset =
	(
		local startPoint = theObj.pos
		local numVerts = theObjOp.getNumVerts theObj
		
		-- Get vert-positions (in world-space)
		local startVerts = for vertNum = 1 to numVerts collect (theObjOp.getVert theObj vertNum)
		local startXform = theObj.transform
		
		-- Give object zero rotation, collect verts again:
		local rotXform = matrix3 1
		scale rotXform theObj.scale
		translate rotXform theObj.pos
		theObj.transform = rotXform
		local rotVerts = for vertNum = 1 to numVerts collect (theObjOp.getVert theObj vertNum)
		
		-- Revert object-rotation:		
		theObj.transform = startXform
		
		-- These verts hang over the end of the spline and will be trimmed if that option is selected:
		local trimVertList = #{}
		trimVertList.count = numVerts
		
		local nvPos, spPos, spTangent
		local startSpPos, startSpTangent, startMatrix
		local endSpPos, endSpTangent, endMatrix

		for vertNum = 1 to numVerts do
		(
			local vertPos = rotVerts[vertNum]
			
			local vOffset = vertPos - startPoint
			
			--get proportion along obj length
			local vlengthPos = -((startPoint.y - vertPos.y) + objOffset)
			vlengthPos /= splLength
			
			local outVert = false
			
			case of 
			(
				(vlengthPos < 0):
				(
					if (startMatrix == undefined) do 
					(
						spPos = interpCurve3D theSplineObj 1 0.0
						spTangent = tangentCurve3D theSplineObj 1 0.0
						
						startMatrix = matrix3 (normalize (cross spTangent [0,0,1])) spTangent [0,0,1] spPos
					)
					
					nvPos =  (vOffset * startMatrix)
					trimVertList[vertNum] = true
				) 
				(vlengthPos > 1):
				( 
					if (endMatrix == undefined) do 
					(
						spPos = interpCurve3D theSplineObj 1 1.0
						spTangent = tangentCurve3D theSplineObj 1 1.0
						
						endMatrix = matrix3 (normalize (cross spTangent [0,0,1])) spTangent [0,0,1] spPos
					)
					
					nvPos =  (vOffset * endMatrix)
					trimVertList[vertNum] = true
				) 
				Default:
				( 
					spPos = interpCurve3D theSplineObj 1 vlengthPos
					spTangent = tangentCurve3D theSplineObj 1 vlengthPos
					
					local perpVector = cross spTangent [0,0,1]
					perpVector = normalize perpVector
					
					nvPos = spPos + (vOffset.x * perpVector)
					nvPos.z = spPos.z + vOffset.z
				)
			)
			
			-- Don't change vert if it's going to be in the same place:
			if (distance startVerts[vertNum] nvPos) > 0.001 do 
			(
				theObjOp.setVert theObj vertNum nvPos
			)
		)
		
		if chkTrimObj.checked and (trimVertList.numberSet != 0) do
		(
			theObjOp.deleteVerts theObj trimVertList
			theObjOp.deleteIsoVerts theObj
			
			if (theObjOp.getNumFaces == 0) do 
			(
				delete theObj
			)
		)
		
		return true
	)
		
	fn goArrayObject theObj objOffset =
	(
		local startPoint = theObj.pos
		local offsetRatio = abs (objOffset/splLength) as float
		local spPos, spTangent

		if offsetRatio <= 1 then 
		( 
			spPos = interpCurve3D theSplineObj 1 offsetRatio
			spTangent = tangentCurve3D theSplineObj 1 offsetRatio
		
		) else
		(
			spPos = interpCurve3D theSplineObj 1 1.0
			spTangent = tangentCurve3D theSplineObj 1 1.0
			spPos += (spTangent * abs (objOffset - splLength) )
		)
		
		local perpVector = cross spTangent [0,0,1]
		perpVector = normalize perpVector
		
		theObj.pos = spPos
		
		--create transform matrix for object
		local theMatrix = matrix3 perpVector spTangent [0,0,1] spPos
		
		local scaleWas = theObj.scale
		
		preRotate theMatrix theObj.rotation
		theObj.transform = theMatrix
		theObj.scale = scaleWas
	)
	
	fn getPolyObjLength =
	(
		polyBB = nodeLocalBoundingBox btnPickObj.object
		
		return (polyBB[1].y - polyBB[2].y)
	)
	
	fn setCtrlsEnabled = 
	(
		if not isValidNode btnPickObj.object do 
		(
			btnPickObj.object = undefined
		)
		if not isValidNode btnPickSpline.object do 
		(
			btnPickSpline.object = undefined
		)
		
		local hasObj = isValidNode btnPickObj.object
		local hasPath = isValidNode btnPickSpline.object
		local hasBoth = hasObj and hasPath
		local hasDeformable = (isKindOf btnPickObj.object Editable_Mesh) or (isKindOf btnPickObj.object Editable_Poly)
		
		#(chkDeform, chkArrayObj, chkTrimObj, spnObjOffset, btnGo).enabled = hasBoth
		
		if hasBoth do 
		(
			chkDeform.enabled = hasDeformable
			if chkDeform.enabled then 
			(
				chkDeform.state = setDeform
				chkArrayObj.state = setArray
			) 
			else 
			(
				chkDeform.state = false
				chkArrayObj.state = true
				chkArrayObj.enabled = false
			)
			
			-- Minimum spacing is the object-length: (pulled in a bit to avoid infinitely-long placing-loop)
			local spacingVal = spnObjOffset.value
			spnObjOffset.range.x = getPolyObjLength() + 0.01
			spnObjOffset.value = spacingVal
			
			chkTrimObj.enabled = (chkDeform.enabled and chkDeform.checked)
			if chkTrimObj.enabled then (chkTrimObj.state = setTrim) else (chkTrimObj.state = false)
			
			spnObjOffset.enabled = chkArrayObj.checked
		)		

		chkMergeArray.enabled =  chkArrayObj.state
	)
	-------------------------------------------------------------------------------------
	--Events
	-------------------------------------------------------------------------------------
	on chkUseCopy changed state do setCtrlsEnabled()
	on btnPickObj picked obj do (setCtrlsEnabled())
	on btnPickObj rightclick do
	(
		btnPickObj.object = undefined
		setCtrlsEnabled()
	)
	
	on btnPickSpline picked obj do (setCtrlsEnabled())
	on btnPickSpline rightclick do
	(
		btnPickSpline.object = undefined
		setCtrlsEnabled()
	)
	
	-- Store manually-set values, so that can be restored after blanking later:
	on chkDeform changed state do (setDeform = state; setCtrlsEnabled())
	on chkTrimObj changed state do (setTrim = state; setCtrlsEnabled())
	on chkArrayObj changed state do (setArray = state; setCtrlsEnabled())
	
	-- Do Deform/Array
	on btnGo pressed do undo "Path Deform/Array" on 
	(
		theObj = btnPickObj.object
		theSplineObj = btnPickSpline.object
		
		local cancel = false
		if not isValidNode theObj do 
		(
			btnPickObj.object = undefined
			cancel = true
		)
		
		if not isValidNode theSplineObj do 
		(
			btnPickSpline.object = undefined
			cancel = true
		)
		
		if cancel do 
		(
			setCtrlsEnabled()
			return false
		)

		if chkUseCopy.checked do 
		(
			theObj = copy theObj
		)
		
		if chkDeform.checked do 
		(
			collapseStack theObj
		)
		
		theObjOp = RsMeshPolyOp theObj
		
		--get spline length
		local segLengths = getSegLengths theSplineObj 1
		splLength = segLengths[segLengths.count]
		
		startLoc = interpCurve3d theSplineObj 1 0.0
		theObj.pos = startLoc
		
		objLength = getPolyObjLength()
		objLength -= spnObjOffset.value
		
		local objOffset = 0.0
		local numSteps = if chkArrayObj.checked then ((abs(splLength/objLength)) + 1) else 1
		
		local objs = #(theObj)		
		
		
		if chkArrayObj.checked do 
		(
			for n = 2 to numSteps do
			(
				append objs (copy theObj)			
			)
		)

		for objNum = 1 to objs.count do 
		(
			prog_Deform.value = 100.0 * objNum/objs.count			
			local obj = objs[objNum]
			
			if chkArrayObj.checked do 
			(
				goArrayObject obj objOffset
			)
			
			if chkDeform.checked do 
			(
				goDeformObject obj objOffset
			)
			
			objOffset += objLength
		)
		
		-- MERGE IN TO ON OBJECT
		if (chkMergeArray.state) AND (chkArrayObj.checked) do for i=2 to objs.count do (RsMeshPolyOp objs[1]).attach objs[1] objs[i]		
		
		completeRedraw()
	)
	
	on RsPathDeformRoll open do 
	(
		banner.setup() 
		setCtrlsEnabled()
	)
)

createDialog RsPathDeformRoll style:#(#style_border,#style_toolwindow,#style_sysmenu)
