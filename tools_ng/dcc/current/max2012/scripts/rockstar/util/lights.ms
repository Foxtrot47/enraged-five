-- Rage light functions
-- Rockstar North
-- 16/5/2005
-- by Greg Smith

-- various light utility functions

--------------------------------------------------------------
-- returns all the lights in the scene
--------------------------------------------------------------
fn RsGetAllLights = (

	alllights = #()
	
	for obj in rootnode.children do (

		if superclassof obj == light then (
		
			append alllights obj
		)
	)
	
	return alllights
)

--------------------------------------------------------------
-- qsort function that sort by the distance from QsortObject
--------------------------------------------------------------
global QsortObject = undefined
fn RsQsortLightsByDistance v1 v2 = (

	length1 = length (QsortObject.pos - v1.pos)
	length2 = length (QsortObject.pos - v2.pos)
	
	if length1 < length2 then (
	
		return -1
	)
	
	if length1 > length2 then (
	
		return 1
	)
	
	return 0
)

--------------------------------------------------------------
-- returns the specified number of lights that are nearest the specified obj
--------------------------------------------------------------
fn RsGetNearestLights number obj = (

	retLights = #()
	alllights = RsGetAllLights()
	
	QsortObject = obj
	
	qsort alllights RsQsortLightsByDistance

	for i = 1 to number do (
	
		if alllights.count < i then (
		
			exit
		)
		
		append retLights alllights[i]
	)
	
	return retLights
)


fn RsLightsMigrateFlashinessFlags = 
(
	local flashinessData = filterstring (getControlData "Gta LightPhoto" "Flashiness") ","
	local electricIndex = (findItem flashinessData "Electric") - 1
	local planeIndex = (findItem flashinessData "Plane") - 1
	local stroboIndex = (findItem flashinessData "Strobe") - 1
	for l in lights where l.parent!=undefined do
	(
		local idxLightFlashiness
		local idxLightDontExport
		local idxPlane
		local idxStrobo
		local idxElectric
		case (getattrclass l) of 
		(
			"Gta Light":
			(
				idxLightFlashiness = getattrindex "Gta Light" "Flashiness"
				idxLightDontExport = getattrindex "Gta Light" "Dont Export"
				idxPlane = getattrindex "Gta Light" "Plane"
				idxStrobo = getattrindex "Gta Light" "Strobe"
				idxElectric = getattrindex "Gta Light" "Is Electric"
			)
			"Gta LightPhoto":
			(
				idxLightFlashiness = getattrindex "Gta LightPhoto" "Flashiness"
				idxLightDontExport = getattrindex "Gta LightPhoto" "Dont Export"
				idxPlane = getattrindex "Gta LightPhoto" "Plane"
				idxStrobo = getattrindex "Gta LightPhoto" "Strobe"
				idxElectric = getattrindex "Gta LightPhoto" "Is Electric"
			)
			default:
			(
				-- Not a supported light type - targets somehow making it too in here
				continue
			)
		)
		if getattr l idxLightDontExport do
			continue
		
		if getattr l idxElectric do
		(
			gRsUlog.LogWarning ("Light \""+l.name+"\"'s flag \"Is Electric\" has been retired and the flashiness value has been set in its place. Please make sure this is set up as desired.") context:l quiet:false
			setAttr l idxLightFlashiness electricIndex
			setAttr l idxElectric false
		)
		if getattr l idxStrobo do
		(
			gRsUlog.LogWarning ("Light \""+l.name+"\"'s flag \"Strobe\" has been retired and the flashiness value has been set in its place. Please make sure this is set up as desired.") context:l quiet:false
			setAttr l idxLightFlashiness stroboIndex
			setAttr l idxStrobo false
		)
		if getattr l idxPlane do
		(
			gRsUlog.LogWarning ("Light \""+l.name+"\"'s flag \"Plane\" has been retired and the flashiness value has been set in its place. Please make sure this is set up as desired.") context:l quiet:false
			setAttr l idxLightFlashiness planeIndex
			setAttr l idxPlane false
		)
	)
)