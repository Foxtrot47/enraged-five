rollout vb_SavePresetDialog "Preset Name"
(
	--edittext <name> [ <caption> ] [text:<string>] [fieldWidth:<integer>] [height:<integer>] [bold:<boolean>] [labelOnTop:<boolean>] [readOnly:<boolean>]
	edittext txtPresetName text:"changeme" caption:"Enter a new preset name" labelOnTop:true width:170
	button btnSavePreset "Save" width:170
	
	--Save preset
	on btnSavePreset pressed do
	(
		--check for empty text
		if txtPresetName.text == "" or txtPresetName.text == "changeme" then
		(
			messageBox "Please enter a name for the preset" title:"Error!"
			return 0
		)
		
		currentSettings = RSL_SceneBaker.getCurrentSettingsAsPreset()
	
		local index = RSL_SceneBaker.findPreset currentSettings
		
		if index == 0 then
		(
			currentSettings.name = txtPresetName.text
			append RSL_SceneBaker.presetsArray currentSettings
			RSL_SceneBaker.saveAsLocalPreset currentSettings
			
			--refresh the dropdown presets
			RSL_SceneBaker.readPresets localPresets:true
			--populate presets dropdown
			::rsn_vertexBaker.ddm_presets.items = RSL_SceneBaker.presetNames
			::rsn_vertexBaker.ddm_presets.selection = RSL_SceneBaker.presetNames.count
		)
		else
		(
			messageBox ("The current settings are already saved as " + RSL_SceneBaker.presetsArray[index].name) title:"Error..."
		)
		
		--close this dialog
		DestroyDialog vb_SavePresetDialog
	)
)