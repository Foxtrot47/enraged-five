rollout vb_advancedOptions "Advanced Options"
(		
	group "Scene Type"
	(
		dropdownlist ddl_sceneTypes items:RSL_SceneBaker.sceneTypes
	)
	group "Lights"
	(
		checkbox ckb_skyLight "Sky Light" tooltip:"Makes use of a skylight if found otherwise creates one"
		--checkbox ckb_useSceneLights "Use Scene Lights"
	)
	group "Materials"
	(
		--checkbox ckb_prepare "Prepare Materials"
		checkbox ckb_emissive "Use Emissive" tooltip:"Use emissive shaders as ligths in the bake"
		--checkbox ckb_overlay "Ignore Overlays"
	)	
	group "Vertex Channel"
	(
		label lb_appyTo "Bake Channel" align:#left
		radiobuttons rbtn_channel labels:#("Colour", "Illumination", "Alpha")
		--checkbox ckb_blackColour "Blacken Colour" 
		--checkbox ckb_blackIllumination "Blacken Illumination" 
		checkbox ckb_collapseStack "Collapse Stack" tooltip:"Enable to collapse the stack after lighting"
	)
	
	group "Radiosity Settings"
	(		
		label lblQuality "Initial Quality(%):" height:15 across:2 align:#left
		spinner spnQuality ""  range:[0.0,100.0,90] width:50 height:16
		
		label lblRefineAll "Refine (All Objects):" height:15 across:2 align:#left
		spinner spnRefineAll ""  width:50 height:16 range:[0,100,0] type:#integer

		--label lblRefineSelect "Refine (Selected Objects):"  height:15  across:2 align:#left
		--spinner spnRefineSelected ""  width:50 height:16 range:[0,100,0] type:#integer

		label lblIndirectLight "Indirect Light Filtering:" height:15 across:2 align:#left
		spinner spnIndirectLight "" width:50 height:16 range:[0,100,3] type:#integer

		label lblDirectLight "Direct Light Filtering:"   height:15 across:2 align:#left
		spinner spnDirectLight "" width:50 height:16 range:[0,100,3] type:#integer
	)
	
	group "Exposure Settings"
	(
		label lblExposureType "Exposure"
		label lblBrightness "Brightness:" height:15 across:2 align:#left
		spinner spnBrightness ""  width:50 height:16 range:[0,200,50] type:#integer 
 
		label lblContrast "Contrast:"  height:15 across:2 align:#left
		spinner spnContrast "" width:50 height:16 range:[0,100,100] type:#integer 
		
		label lblMidTones "Mid Tones:"   height:15 across:2 align:#left
		spinner spnMidTones"" width:50 height:16 range:[0,20,0.6] type:#Float 

		label lblPhysicalScale "Physical Scale:"  height:15 across:2 align:#left
		spinner spnPhysicalScale "" width:50 height:16 range:[0,20000,1500] type:#integer 
		
		checkbox ckb_desaturate "Desaturate"
		
		checkbox ckb_indirectOnly "Indirect Only"
	)
	
	
	button btn_preset "Save As Preset..." width: 240
	--button btn_apply "Apply" width: 240
	
	--//////////////////////
	--Events dear boy
	--//////////////////////
	--on btn_apply pressed do DestroyDialog vb_advancedOptions
	fn ddl_sceneTypes_selected arg =
	(
		RSL_SceneBaker.sceneType = RSL_SceneBaker.sceneTypes[arg]
	)
	
	on ddl_sceneTypes selected arg do ddl_sceneTypes_selected arg
	
	--//////////////////////////Lights//////////////////////////////
	-----------------------------
	-- Use Skylight
	------------------------------
	on ckb_skyLight changed theState do
	(
		RSL_SceneBaker.useSkyLight = theState
	)
	
	/*
	------------------------------
	--Use lights
	------------------------------
	on ckb_useSceneLights changed theState do
	(
		RSL_SceneBaker.useSceneLights = theState
	)
	*/
	
	--////////////////////////////Materials////////////////////////
	------------------------------
	--use emissive
	------------------------------
	on ckb_emissive changed theState do
	(
		RSL_SceneBaker.useEmissive = theState
	)
	
	
	--/////////////////////////Vertex Channel////////////////////
	------------------------------
	--cpv channel
	------------------------------
	on rbtn_channel changed arg do
	(
		--print arg
		if arg == 1 then			--vertex Colour
		(
			RSL_SceneBaker.vertexColorChannel = true
			RSL_SceneBaker.vertexIllumChannel = false
		)
		else if arg == 2 then	--Illumination
		(
			RSL_SceneBaker.vertexColorChannel = false
			RSL_SceneBaker.vertexIllumChannel = true
		)
		else if arg == 3 then	--Alpha
		(
			RSL_SceneBaker.vertexColorChannel = false
			RSL_SceneBaker.vertexIllumChannel = false
			RSL_SceneBaker.vertexAlphaChannel = true
		)
	)
	
	------------------------------
	--collapse Stack
	------------------------------
	on ckb_collapseStack changed state do
	(
		RSL_SceneBaker.collapseStackSetting = state
	)
	
	------------------------------
	--blacken colour
	------------------------------
	on ckb_blackColour changed theState do
	(
		RSL_SceneBaker.blackenColour = theState
	)
	
	------------------------------
	--blacken Illumination
	------------------------------
	on ckb_blackIllumination changed theState do
	(
		RSL_SceneBaker.blackenIllum = theState
	)
	
	------------------------------
	--desaturate
	------------------------------
	on ckb_desaturate changed theState do 
	(
		RSL_SceneBaker.desaturate = theState
	)
	
	------------------------------
	--indirectOnly
	------------------------------
	on ckb_indirectOnly changed theState do
	(
		RSL_SceneBaker.indirectOnly = theState
	)
	
	--///////////////////////////Radiosity settings ////////////////////////////
	on spnQuality changed arg do RSL_SceneBaker.qualityDefault = arg
	on spnRefineAll changed arg do RSL_SceneBaker.refineDefault = arg
	on spnIndirectLight changed arg do RSL_SceneBaker.directFilterDefault = arg
	on spnDirectLight changed arg do RSL_SceneBaker.filterDefault = arg
		
	--///////////////////////////Exposure settings////////////////////////////
	on spnBrightness changed arg do RSL_SceneBaker.brightnessDefault = arg
	on spnContrast changed arg do RSL_SceneBaker.contrastDefault = arg
	on spnMidTones changed arg do RSL_SceneBaker.midTonesDefault = arg
	on spnPhysicalScale changed arg do RSL_SceneBaker.physicalScaleDefault = arg
	
	------------------------------
	--	 Save preset
	------------------------------
	on btn_preset pressed do
	( 
		--createDialog vb_SavePresetDialog
		try(DestroyDialog vb_SavePresetDialog) catch()
		--try(rsn_windowPos  = (GetDialogPos rsn_vertexBaker)) catch()
		createDialog vb_SavePresetDialog 200 60 style:#(#style_sysmenu, #style_toolwindow)
		SetDialogPos vb_SavePresetDialog [RSL_SceneBaker.rsn_windowPos[1]+240, RSL_SceneBaker.rsn_windowPos[2]+470]
	)
	
)