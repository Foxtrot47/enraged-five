
struct NightLight
(
	prop,
	pos = Point3 0 0 0,
	intensity,
	distribution,
	lightShape
)
--create a NightLight()
mainNightLight = NightLight()

struct TimeCycleStruct
(
	prop,
	pos = Point3 0 0 0,
	height,
    length,
    width,
    seed,
	percentage,
    range,
    startHour,
    endHour,
    IDString
)


mainTimeCycle = TimeCycleStruct()

struct NightLightUtils
(
	configPath = (RsConfigGetToolsDir() + "etc/config/generic/PropNightLights.xml"),
	nightLightNameRoot = "NightBake",
	propLights = #(),
	propTimeCycles = #(),
	createdLights = #(),
	createdTimeCycles = #(),
	exclusionName = "",
	
	fn loadLightConfig =
	(
		propLights = #()
		propTimeCycle = #()
		
		XML = dotNetObject "System.Xml.XmlDocument"
		XML.load configPath
		
		root = XML.SelectSingleNode "Lights"
		propNodes = root.childNodes
		--propNodesIt = propNodes.getEnumerator()
		--photoLights = XML.GetElementsByTagName "PhotoLight"
		--photoLightsIt = photoLights.getEnumerator()
		
		for i = 0 to (propNodes.count - 1) do
		(
			thisPropNode = propNodes.Item[i]
			photoLights = #()
			
			--get the lights
			photoLightNodes = thisPropNode.getElementsByTagName "PhotoLight"
			
			for p = 0 to (photoLightNodes.count - 1) do
			(
				thisPhotoLight = photoLightNodes.itemOf[p]
				
				--print thisPhotoLight.innerXml
				local nightLight = copy mainNightLight
				
				--prop
				propNode = thisPhotoLight.selectSingleNode "Prop"
				nightLight.prop = propNode.getAttribute "Name"
				
				--Position
				positionNode = thisPhotoLight.selectSingleNode "Position"
				nightLight.pos = [0, 0, 0]
				nightLight.pos.x = (positionNode.getAttribute "X") as Float
				nightLight.pos.y = (positionNode.getAttribute "Y") as Float
				nightLight.pos.z = (positionNode.getAttribute "Z") as Float
				
				--Intensity
				intensityNode = thisPhotoLight.selectSingleNode "Intensity"
				nightLight.intensity = (intensityNode.innerText) as Float
				
				--Distribution
				distributionNode = thisPhotoLight.selectSingleNode "Distribution"
				nightLight.distribution = (distributionNode.innerText) as Integer
				
				--Shape
				shapeNode = thisPhotoLight.selectSingleNode "Shape"
				nightLight.lightShape = ("#" + shapeNode.innerText)
				
				--add to the light type list
				append photoLights (DataPair propName:nightLight.prop lightStruct:nightLight)
				--print photoLights
			)
			append propLights photoLights
			
			-------------------------
			--TIMECYCLES
			------------------------
			timeCycles = #()
			
			--get the timecycles
			TCNodes = thisPropNode.getElementsByTagName "TimeCycle"
			
			for p = 0 to (TCNodes.count - 1) do
			(
				thisTCNode = TCNodes.itemOf[p]
				
				local timeCycleNode = TimeCycleStruct()
				
				--prop
				propNode = thisTCNode.selectSingleNode "Prop"
				timeCycleNode.prop = propNode.getAttribute "Name"
				
				--Position
				positionNode = thisTCNode.selectSingleNode "Position"
				timeCycleNode.pos = [0, 0, 0]
				timeCycleNode.pos.x = (positionNode.getAttribute "X") as Float
				timeCycleNode.pos.y = (positionNode.getAttribute "Y") as Float
				timeCycleNode.pos.z = (positionNode.getAttribute "Z") as Float
				
				--<height>
				timeCycleNode.height = (thisTCNode.selectSingleNode "height").InnerText as Float
				
				--<length>
				timeCycleNode.length = (thisTCNode.selectSingleNode "length").InnerText as Float
      
				--<width>
				timeCycleNode.width = (thisTCNode.selectSingleNode "width").InnerText as Float
				
				--<seed>
				timeCycleNode.seed = (thisTCNode.selectSingleNode "seed").InnerText as Integer
				
				--<percentage>
				timeCycleNode.percentage = (thisTCNode.selectSingleNode "Percentage").InnerText as Float
				
				--<range>
				timeCycleNode.range = (thisTCNode.selectSingleNode "Range").InnerText as Float
				
				--<startHour>
				timeCycleNode.startHour = (thisTCNode.selectSingleNode "StartHour").InnerText as Integer
				
				--<endHour>
				timeCycleNode.endHour = (thisTCNode.selectSingleNode "EndHour").InnerText as Integer
				
				--<IDString>
				timeCycleNode.IDString = (thisTCNode.selectSingleNode "IDString").InnerText
				
				append timeCycles (DataPair propName:timeCycleNode.prop TCStruct:timeCycleNode)
			)
			if timeCycles != undefined then	append propTimeCycles timeCycles
		)
		propTimeCycles = for item in propTimeCycles where item.count != 0 collect item
		--print propTimeCycles
		--for p in propLights do print p
		--print propLights[14][1]
		
	),
	
	--JL
	fn createJunctionLight_TimeCycleStruct = 
	(
		TimeCycleStruct pos:(Point3 0 0 -20) \
						height:30 length:30 width:30 \
						percentage:100 \
						range:30 \
						startHour:0 \
						endHour:23 \
						IDString:"StreetLightingJunction"
	),
	
	--SAL
	fn createStreetAreaLight_TimeCycleStruct =
	(
		TimeCycleStruct	pos:(Point3 0 0 -10) \
						height:15 length:20 width:20 \
						percentage:100 \
						range:20 \
						startHour:0 \
						endHour:23 \
						IDString:"StreetLighting"
	),
	
	fn generatePhotoLights =
	(
		toolMode.coordsys #world
		createdLights = #()
		
		RSrefObjects = for o in $objects where classOf o == RSrefObject collect o
		
		--lightprop name list cleaned
		propLightNames = for prop in nightLightUtils.propLights collect substituteString prop[1].propName "_frag_" ""
			
		RsRefLights = #()
		
		for prop in RSrefObjects do
		(
			for name in propLightNames where name == prop.objectName do append RsRefLights prop
		)
						
		for prop in RsRefLights do
		(
			--print prop.objectName
			--get the nightlight for the prop
			--local nightLight = (for propLight in propLights where findString propLight.propName prop.objectName != 0 collect propLight.lightStruct)[1]
			local nightLights = (for aPropLight in propLights where matchPattern prop.objectName pattern:aPropLight[1].propName == true collect aPropLight)[1]
			--format "propname: % lightname: % pos: % propPos: % resultPos: %\n" prop.objectName nightLight.prop nightLight.pos prop.pos (prop.pos + nightLight.pos)
			
			for nightLight in nightLights do
			(
				nightLight = nightLight.lightStruct
				
				lightPos = prop.pos + nightLight.pos
				propPrefix = undefined
				wireColour = undefined
				case (filterstring prop.objectName "_")[2] of
				(
					"Traffic":
					(
						wireColour = color 255 64 0
						propPrefix = "_TL_"
					)
					
					
					default:
					(
						wireColour = color 0 128 255
						propPrefix = "_SL_"
					)
				)
				nightLightName = (nightLightNameRoot+propPrefix+prop.objectName)
				theFreeLight = Free_Light name:nightLightName pos:lightPos intensity:nightLight.intensity distribution:nightLight.distribution type:nightLight.lightShape
				theFreeLight.wirecolor = wireColour
				theFreeLight.pivot = prop.pivot
				
				--rotate the light so that it is orientated as the prop
				propRot = prop.rotation as angleAxis
				theFreeLight.rotation.z_rotation = propRot.angle * propRot.axis.z
				
				append createdLights theFreeLight
			)
		)
		
	),
	
	fn createAreaLightTC theLight type =
	(
		local timeCycle = undefined
		local wireColour = undefined
		
		case type of
		(
			"JL":
			(
				timeCycle = createJunctionLight_TimeCycleStruct()
				wireColour = color 255 192 0
			)
			
			"SAL":
			(
				timeCycle = createStreetAreaLight_TimeCycleStruct()
				wireColour = color 0 255 0
			)
		)
		
		
		local TCName = ("TimeCycle_"+ theLight.name)
		
		local theTCNode = GtaTimeCycle name:TCName
		
		theTCNode.pos = theLight.pos + timeCycle.pos
		theTCNode.BoxGizmo.height = timeCycle.height
		theTCNode.BoxGizmo.length = timeCycle.length
		theTCNode.BoxGizmo.width = timeCycle.width
		
		--set gta attrs
		--percentage
		setAttr theTCNode 1 (timeCycle.percentage as Float)
		
		--Range
		setAttr theTCNode 2 (timeCycle.range as Float)
		
		--StartHour
		setAttr theTCNode 3 (timeCycle.startHour as Integer)
		
		--EndHour
		setAttr theTCNode 4 (timeCycle.endHour as Integer)
		
		--IDString
		setAttr theTCNode 5 timeCycle.IDString
		
		
		--format "created:%\n" theTCNode
		theTCNode.wirecolor = wireColour
		--theTCNode.pivot = theLight.pivot
	),
	
	fn generateAreaLightTCs =
	(
		local freeAreaLights  = for o in objects where classOf o == Free_Area collect o
			
		for item in freeAreaLights do
		(
			--get its name to determine type
			local nameBits = filterString item.name "_"
			if nameBits[1] != "NightBake" then
			(
				format "Not a Nightbake area light\n"
				continue
			)
			
			local type = undefined
			if findString nameBits[2] "JL" == 1 then
			(
				type = "JL"
			)
			else if findString nameBits[2] "SAL" == 1 then
			(
				type = "SAL"
			)
			
			case type of
			(
				"JL":
				(
					createAreaLightTC item "JL"
				)
				"SAL":
				(
					createAreaLightTC item "SAL"
				)
			)
		)
	),
	
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	-- Generate TC for pointlights
	-- Mode is either:
	--#props -  This generates the TC relative to the prop pivot and rotates it correctly
	--#ref - generates the TC from the lighting reference file
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn generatePointLightTCs mode:#ref =
	(
		--collect up all the lights we need to create TC for 
		local freeLights = for o in objects where classOf o ==  Free_Light collect
		(
			if (matchPattern o.name pattern:"NightBake*") == false then
			(
				dontCollect
			)
			else
			(
				o
			)
		)
			
		for item in freeLights do
		(
			-- ..except for the excluded ones
			if exclusionName != "" then
			(
				if matchPattern item.name pattern:("*"+exclusionName+"*") ignoreCase:true == true then continue
			)
			
			--get its name to determine type
			local nameBits = filterString item.name "_"
			if nameBits[1] != "NightBake" then
			(
				format "Not a Nightbake area light\n"
				continue
			)
			
			--find the timecycle struct for this light based on its name
			local timeCycle = undefined
			if propTimeCycles.count == 0 then
			(
				format "config is not loaded"
				messagebox "config is not loaded\n" title:"ERROR"
				return false
			)
			
			timeCycle = (for tc in propTimeCycles where findString item.name tc[1].propName != undefined collect tc[1].TCStruct)[1]
			if timeCycle == undefined then continue
				
			
			local TCName = ("TimeCycle_" + item.name)
			local theTCNode = GtaTimeCycle name:TCName
		
			case mode of
			(
				#prop:theTCNode.pos = item.pos + timeCycle.pos
				#ref:
				(
					undo off
					(
						local itemClone = copy item
						CenterPivot itemClone
						local centreredPos = [itemClone.pos.x, itemClone.pos.y, itemClone.pos.z - (timeCycle.height / 2)]
						theTCNode.pos = centreredPos
						
						delete itemClone
					)
				)
			)
			
			theTCNode.BoxGizmo.height = timeCycle.height
			theTCNode.BoxGizmo.length = timeCycle.length
			theTCNode.BoxGizmo.width = timeCycle.width
			
			--set gta attrs
			--percentage
			setAttr theTCNode 1 (timeCycle.percentage as Float)
			
			--Range
			setAttr theTCNode 2 (timeCycle.range as Float)
			
			--StartHour
			setAttr theTCNode 3 (timeCycle.startHour as Integer)
			
			--EndHour
			setAttr theTCNode 4 (timeCycle.endHour as Integer)
			
			--IDString
			setAttr theTCNode 5 timeCycle.IDString
			
			--theTCNode.pivot = item.pivot
		)
	),
	
	
	--//////////////////////////////////////////////////////////////////////////
	-- Generate TimeCycle modifiers from the loaded
	-- lighting reference scene
	--/////////////////////////////////////////////////////////////////////////
	fn generateTimeCycles = 
	(
		toolMode.coordsys #world
		createdTimeCycles = #()
		
		RSrefObjects = for o in $objects where classOf o == RSrefObject collect o
		
		--generate TC for Free_Light
		--local selectedMode = vb_NightBakeSetup.ddlMode.selected
		local selectedMode = "Ref"
		
		if selectedMode == "Prop" then
		(
			generatePointLightTCs()
		)
		else if selectedMode == "Ref" then 
		(
			generatePointLightTCs mode:#ref
		)
		
		
		--generate TC for Free_Area lights, like junction and Street Area Lights
		generateAreaLightTCs()
		
	),
	
	fn cleanPhotoLights =
	(
		for l in createdLights do delete l
	),
	
	fn getUserPickPoint =
	(
		local nodeHit = undefined
		local targetPoint = pickPoint snap:#3D
		if classOf targetPoint != Point3 then return nodeHit			--check for user exit
			
		nodeHit = intersectRayScene (ray (targetPoint+[0, 0, 2000]) [0, 0, -1])
	),
	
	fn createAreaLight type: =
	(
		local nodeHit = getUserPickPoint()
		if nodeHit != undefined and nodeHit.count != 0 then
		(
			print nodeHit
			local hitPoint = nodeHit[1][2].pos
			local lightPos = hitPoint+[0, 0, 15]
			
			case type of
			(
				#junctionLight:
				(
					Free_Area 	name:(nightLightNameRoot+"_JL") \
								pos:lightPos \
								Distribution:0 \
								intensity:7161.97 \
								light_length:30.0 \
								light_Width:30 \
								type:#Free_Rectangle \
								wirecolor:(color 255 255 0)
				)
				
				#streetAreaLight:
				(
					Free_Area	name:(nightLightNameRoot+"_SAL") \
								pos:lightPos \
								Distribution:0 \
								intensity:1500 \
								light_length:30.0 \
								light_Width:30 \
								type:#Free_Rectangle \
								wirecolor:(color 0 255 0)
				)
			)
			
			return true
		)
		else
		(
			return false
		)
		
		return false
	)
)

nightLightUtils = NightLightUtils()
try( destroyDialog nightLightSetup )catch()
rollout nightLightSetup "Night Light Setup"
(
	--VARS
	local configPath = (RsConfigGetToolsDir() + "etc/config/generic/PropNightLights.xml")
	
	--UI
	button btn_createSetup "Create config"
	--button btn_loadConfig "Load Config"
	--button btn_genLights "Create Lights"
	--button btn_CleanLights "Clean Lights"
	
	
	--FUNCS
	fn createNightLightsConfig = 
	(
		--create config
		XML = dotNetObject "System.Xml.XmlDocument"
		declaration = XML.CreateXmlDeclaration "1.0" "utf-8" "yes"
		XML.appendChild declaration
		lightsElem = xml.createElement "Lights"
		XML.appendChild lightsElem
		
		toolMode.coordsys #world
		--iterate scene lights
		for l in $lights where classOf l != RageLight do
		(
			case (classOf l) of
			(
				Free_Light:
				(
					theProp = undefined
					theNode = undefined
					
					if l.parent != undefined then
					(
						theNode = l.parent
						while theNode != undefined do
						(
							theProp = theNode
							theNode = theNode.parent
						)
					
						--print theProp
						
						--get the light position relative to the prop pivot
						lightPos = l.pos - theProp.pos
						--lightPos = l.pos
						--box pos:lightPos length:1 width:1 height:1
						
						format "prop: % lightPos: % \n" theProp lightPos
						
						--create the Node
						propFormattedName = substituteString theProp.name "_frag_" "" 
						propNodes 	= XML.getElementsByTagName propFormattedName
						
						propNode = undefined
						if propNodes.count != 0 then
						(
							propNode = propNodes.itemOf[0]
						)
						else
						(
							propNode	= XML.createElement (substituteString theProp.name "_frag_" "")
						)
						
						--//////////////////////////////////////////
						--Create PhotoLight definition
						--//////////////////////////////////////////
						photoLight 	= XML.createElement "PhotoLight"
						
							--Prop
							prop			= XML.createElement "Prop"
							propName		= XML.createAttribute "Name"
							propName.value	= substituteString theProp.name "_frag_" ""
							prop.setAttributeNode propName
							
							--position
							position  		= XML.createElement "Position"
							xAttr			= XML.createAttribute "X"
							xAttr.value		= lightPos.x as string
							position.setAttributeNode xAttr
							
							yAttr			= XML.createAttribute "Y"
							yAttr.value		= lightPos.y as string
							position.setAttributeNode yAttr
							
							zAttr			= XML.createAttribute "Z"
							zAttr.value		= lightPos.z as string
							position.setAttributeNode zAttr
							
							--Intensity
							intensity		= XML.createElement "Intensity"
							intensity.innerText = (getProperty l #intensity) as String
							
							--Light Distribution
							distribution	= XML.createElement "Distribution"
							distribution.innerText = (getProperty l #Distribution) as String
							
							--Light Shape
							lightShape		= XML.createElement "Shape"
							lightShape.innerText = getProperty l #type
						
						--add nodes
						photoLight.appendChild prop
						photoLight.appendChild position
						photoLight.appendChild intensity
						photoLight.appendChild distribution
						photoLight.appendChild lightShape
						propNode.appendChild photoLight
						
						
						--//////////////////////////////////////////
						--Create TimeCycle definition
						--//////////////////////////////////////////
						if l.children.count != 0 and classOf l.children[1] == GtaTimeCycle then
						(
							tc = l.children[1]
							tcPos = tc.pos - theProp.pos
							tcProperties = getPropNames tc.BoxGizmo
							
							gtaAttrNames = for i = 1 to (getNumAttr "Gta TimeCycle") collect getAttrName "Gta TimeCycle" i
							
							timeCycle = XML.createElement "TimeCycle"
							
							--Prop
							timeCycle.appendChild (copy prop)
							
							--position
							position  		= XML.createElement "Position"
							xAttr			= XML.createAttribute "X"
							xAttr.value		= tcPos.x as string
							position.setAttributeNode xAttr
							
							yAttr			= XML.createAttribute "Y"
							yAttr.value		= tcPos.y as string
							position.setAttributeNode yAttr
							
							zAttr			= XML.createAttribute "Z"
							zAttr.value		= tcPos.z as string
							position.setAttributeNode zAttr
							
							timeCycle.appendChild position
							
							--node properties
							for prop in tcProperties do
							(
								propElem = XML.createElement (prop as String)
								propElem.InnerText = (getProperty tc.BoxGizmo prop) as String
								timeCycle.appendChild propElem
							)
							
							--Gta TimeCycle properties
							for attr in gtaAttrNames do
							(
								idx = getAttrIndex "Gta TimeCycle" attr
								propElem = XML.createElement attr
								propElem.InnerText = (getAttr tc idx) as String
								timeCycle.appendChild propElem
							)
							
							--append to propnode
							propNode.appendChild timeCycle
							
						)
						
						--append to root
						lightsElem.appendChild propNode
					)
					else	--its a junction area light
					(
						
					)
					
				)
				
				default:format "not supported: % \n" l
			)--end case
		)
		
		--print XML.innerXML
		--save the xml
		gRsPerforce.add_or_edit #(configPath) silent:false
		XML.save configPath
	)
	
	-----------------------------------
	--	EVENTS dear boy
	-----------------------------------
	on btn_createSetup pressed do
	(
		createNightLightsConfig()
	)
	
	on btn_loadConfig pressed do
	(
		nightLightUtils.loadLightConfig()
	)
	
	on btn_genLights pressed do
	(
		nightLightUtils.generatePhotoLights()
		nightLightUtils.generateTimeCycles()
	)
	
	on btn_CleanLights pressed do
	(
		nightLightUtils.cleanPhotoLights()
	)
	
	on nightLightSetup open do
	(
		if ( not gRsIsOutSource ) do
		(
			if gRsPerforce.connected != true then gRsPerforce.connect()
		)
	)
	
)

--createNightLightsConfig()

--createDialog nightLightSetup