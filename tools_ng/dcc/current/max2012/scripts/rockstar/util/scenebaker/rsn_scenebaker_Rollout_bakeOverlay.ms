rollout vb_BakeOverlay "Bake Vault"
(
	local bOC = RSL_SceneBaker.bakeOverlayChannel
	local vcBackup = 31
	--group ""
	--(
		--button btn_previewBakeOverlay "Preview" width:170 height:30
		--button btn_cleanCPV "Clean CPV" width:160 height:30
		button btn_bakeOverlay "Store Bake" width:160 height:30 enabled:true
		button btn_revertBakeOverlay "Retrieve Bake" width:160 height:30 enabled:true
	--)
	
	/*
	fn copyCPV node src dst =
	(
		case (classOf node.baseobject) of
		(
			
			Editable_poly:
			(
				if polyop.getMapSupport node dst == false then polyop.setMapSupport node dst true
				
				srcVCount = polyop.getNumMapVerts node src
				polyop.setNumMapVerts node dst srcVCount
				
				getMapVert = polyop.getMapVert
				setMapVert = polyop.setMapVert
				
				--flush
				
				for v = 1 to srcVCount do
				(
					setMapVert node dst v [0, 0, 0]
				)
				
				
				--copy
				for v = 1 to srcVCount do
				(
					setMapVert node dst v (getMapVert node src v)
				)
			)
			
			Editable_Mesh:
			(
				if meshop.getMapSupport node dst == false then meshop.setMapSupport node dst true
				
				srcVCount = meshop.getNumMapVerts node src
				meshop.setNumMapVerts node dst srcVCount
				
				getMapVert = meshop.getMapVert
				setMapVert = meshop.setMapVert
				
				--flush
				for v = 1 to srcVCount do
				(
					setMapVert node dst v [0, 0, 0]
				)
				
				--copy
				for v = 1 to srcVCount do
				(
					setMapVert node dst v (getMapVert node src v)
				)
			)
		)--end case
		
	)
	*/
	fn copyCPV node src dst =
	(
		case (classOf node.baseobject) of
		(
			Editable_Poly:
			(
				polyop.setNumMaps node 33
				polyop.setMapSupport node dst true
			)
			Editable_Mesh:
			(
				meshop.setNumMaps node 33
				meshop.setMapSupport node dst true
			)
		)
		
		ChannelInfo.CopyChannel node 3 src
		ChannelInfo.PasteChannel node 3 dst
		CollapseStack node
	)
	
	fn cleanCPV theMesh =
	(
		--clean the vc
		ChannelInfo.CopyChannel theMesh 3 0
		ChannelInfo.PasteChannel theMesh 3 bOC
		maxOps.CollapseNode theMesh off 
		
		--flush
		theTriMesh = snapshotasmesh theMesh
		for mv = 1 to (meshop.getNumMapVerts theTriMesh bOC) do
		(
			case (classOf theMesh) of
			(
				Editable_Mesh:meshop.setMapVert theMesh bOC mv [1, 1, 1]
				Editable_Poly:polyop.setMapVert theMesh bOC mv [1, 1, 1]
			)
		)
		/*
		cui.commandPanelOpen = true
		setCommandPanelTaskMode #modify
		vp = VertexPaint()
		vp.mapChannel = bOC
		vp.layerMode = "Normal"
		vpTool = VertexPaintTool()
		vpTool.paintColor = color 255 255 255
		addModifier theMesh vp
		*/
		--UIAccessor.PressButton 4395316
		
		--maxOps.CollapseNode theMesh off 
	)
	
	fn selectionChanged =
	(
		--print "selection changed"
		if $selection.count == 0 then return false
			
		theMesh = $selection[1]
		
		case (classOf theMesh.baseobject) of
		(
			Editable_Poly:
			(	
				if (polyop.getMapSupport theMesh vcBackup) == true then
				(
					btn_bakeOverlay.enabled = true
					btn_revertBakeOverlay.enabled = true
				)
				else
				(
					btn_bakeOverlay.enabled = true
					btn_revertBakeOverlay.enabled = false
				)
			)
			
			Editable_Mesh:
			(
				if (meshop.getMapSupport theMesh vcBackup) == true then
				(
					btn_bakeOverlay.enabled = true
					btn_revertBakeOverlay.enabled = true
				)
				else
				(
					btn_bakeOverlay.enabled = true
					btn_revertBakeOverlay.enabled = false
				)
			)
		)--end case
	)
	
	on btn_cleanCPV pressed do
	(
		theMesh = $selection[1]
		cleanCPV theMesh
	)
	
	on btn_revertBakeOverlay pressed do
	(
		if $selection.count == 0 then return false
		if superClassOf $selection[1] != GeometryClass then return false
		theMesh = $selection[1]
		
		--copy channel 31 back over vc to restore
		copyCPV theMesh vcBackup 0
		btn_revertBakeOverlay.enabled = true
		btn_bakeOverlay.enabled = true
	)
	
	on btn_bakeOverlay pressed do
	(
		if $selection.count == 0 then return false
		if superClassOf $selection[1] != GeometryClass then return false
		theMesh = $selection[1]
		
		
		
		--copy vc to channel 31 so it can be reverted
		--cleanCPV theMesh 0
		copyCPV theMesh 0 vcBackup
		
		/*
		--check the number of mapverts between channel 0 and 30 to make sure they match
		--otherwise something needs to be done about it
		snapMesh = snapShotAsMesh theMesh
		vc_Count = meshop.getNumMapVerts snapMesh 0
		bOC_Count = meshop.getNumMapVerts snapMesh bOC
		
		if vc_Count != bOC_Count then
		(
			messageBox "We have a count mismatch between vertex coluor and overlay" title:"Mismatch Error"
		)
		
		
		
		
		fn multiplyBake theMesh =
		(
			--srcArray = for v = 1 to meshop.getNumMapVerts $ 0 collect meshop.getMapVert $ 0 v
			case (classOf theMesh.baseobject) of
			(
				Editable_Poly:
				(		
					for mapVert = 1 to (polyop.getNumMapVerts theMesh 0) do
					(
						--Blend the overlay with the bake
						--src
						overlayCol = polyop.getMapVert theMesh bOC mapVert
						if overlayCol != [1,1,1] then
						(
							srcCol = polyop.getMapVert theMesh 0 mapVert
							sumCol = srcCol * overlayCol
							polyop.setMapVert theMesh 0 mapVert sumCol
						)
					)
				)
				
				Editable_Mesh:
				(
					for mapVert = 1 to (meshop.getNumMapVerts theMesh 0) do
					(
						--Blend the overlay with the bake
						--src
						overlayCol = meshop.getMapVert theMesh bOC mapVert
						if overlayCol != [1,1,1] then
						(
							srcCol = meshop.getMapVert theMesh 0 mapVert
							sumCol = srcCol * overlayCol
							meshop.setMapVert theMesh 0 mapVert sumCol
						)
					)
				)
			)--end case
		)
		
		
		case (classOf theMesh.baseobject) of
		(
			Editable_Poly:
			(
				if (polyop.getMapSupport theMesh bOC) == true then
				(
					--cleanCPV theMesh 0
					--convertToMesh theMesh
					--meshop.deleteIsoMapVertsAll theMesh
					
					multiplyBake theMesh	
				)
				
				--convertToPoly theMesh
			)
			Editable_Mesh:
			(
				if (meshop.getMapSupport theMesh bOC) == true then
				(
					meshop.deleteIsoMapVertsAll theMesh
					
					multiplyBake theMesh
				)
			)
		)
		*/
		--check for data in channel 30
		
		
		--if there is copy vc into 31 as a temp store
		
		--now overlay 30 with vc
		--btn_bakeOverlay.enabled = false
		--btn_revertBakeOverlay.enabled = true
	)
	
	on vb_BakeOverlay rolledUp arg do
	(
		if arg then
		(
			rsn_vertexBaker.bakeOverlay.height = 115
			rsn_vertexBaker.height += 90
		)
		else
		(
			rsn_vertexBaker.bakeOverlay.height = 25
			rsn_vertexBaker.height -= 90
		)
	)
)