rollout vb_NightBakeSetup "NightBake Operations"
(
	
	button btn_createPropLights "Create Prop Lights" width:190
	button btn_cleanPropLights "Delete Prop Lights" width:190
	button btn_createJunctionLight "Create Junction Light" width:190
	button btn_createStreetAreaLight "Create Street Area Light" width:190
	
	group "TimeCycles:"
	(
		--dropdownlist ddlMode items:#("Prop", "Ref")
		checkbox chkExcludeOpt across:2 width:20
		edittext edtExcludeString "Exclude: " enabled:false offset:[-65,0] width:150
		button btn_createTimeCycles "Create TimeCycle Modifiers" width:170
	)

	--//////////////////////
	--Events dear boy
	--//////////////////////
	on btn_createPropLights pressed do
	(
		nightLightUtils.loadLightConfig()
		nightLightUtils.generatePhotoLights()
	)
	
	on btn_cleanPropLights pressed do
	(
		nightLightUtils.cleanPhotoLights()
	)
	
	on btn_createJunctionLight pressed do
	(
		keepDoing = true
		while keepDoing do
		(
			keepDoing = nightLightUtils.createAreaLight type:#junctionLight
		)
	)
	
	on btn_createStreetAreaLight pressed do
	(
		keepDoing = true
		while keepDoing do
		(
			keepDoing = nightLightUtils.createAreaLight type:#streetAreaLight
		)
	)
	
	--////////////////////////////////////////////
	-- Create time cycle events
	--////////////////////////////////////////////
	
	--////////////////////////////////////////////
	-- Mode changed
	--////////////////////////////////////////////
	
	
	--////////////////////////////////////////////
	-- Exclusion mode active
	--////////////////////////////////////////////
	on chkExcludeOpt changed state do
	(
		case state of
		(
			true:edtExcludeString.enabled = true
			false:
			(
				edtExcludeString.text = ""
				edtExcludeString.enabled = false
			)
		)
	)
	
	--////////////////////////////////////////////
	-- Exclusion string changed
	--////////////////////////////////////////////
	on edtExcludeString changed arg do
	(
		NightLightUtils.exclusionName = arg
	)
	
	--////////////////////////////////////////////
	-- Create the time cycle nodes
	--////////////////////////////////////////////
	on btn_createTimeCycles pressed do
	(
		nightLightUtils.loadLightConfig()
		nightLightUtils.generateTimeCycles()
	)
	
	on vb_NightBakeSetup rolledUp arg do
	(
		if arg then
		(
			rsn_vertexBaker.nightBakeControl.height = 145
			rsn_vertexBaker.height += 120
		)
		else
		(
			rsn_vertexBaker.nightBakeControl.height = 25
			rsn_vertexBaker.height -= 120
		)
	)
	
	on vb_NightBakeSetup open do
	(
		if gRsIsOutSource == true then btn_createTimeCycles.enabled = false
	)
)