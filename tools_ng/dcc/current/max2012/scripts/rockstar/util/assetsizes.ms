--
-- assetsizes.ms
-- Asset Size Utilities
-- David Muir <david.muir@rockstarnorth.com>
--

-------------------------------------------------------------------------------------------
-- name:		RsAssetDrawableMemSize
-- description: Fetch information regarding Drawable (IDR) asset sizes for Max objects
--				
--
-- [in]	 objs		 	List of Max objects to fetch information about
-- [in]	 inclRefs		Include XRefs (boolean)
-- [in[  progress		Progress bar to update
-------------------------------------------------------------------------------------------
fn RsAssetDrawableMemSize objs inclRefs:false progress: = 
(
	local idrNameList = #()
	local idrSizeList = #()

	local objSizes = for objNum = 1 to objs.count collect 
	(
		local obj = objs[objNum]
		local getModelSize = 0
		
		if (progress != unsupplied) do 
		(
			progress.value = 100.0 * objNum / objs.count
		)
		
		local checkExts = #(".idr.zip", ".ift.zip")

		if "Gta Object" == ( GetAttrClass obj ) do 
		(
			if ( isRefObj obj ) then 
			(
				if inclRefs do 
				(
					local objName = obj.ObjectName

					local findNum = findItem idrNameList objName
					if (findNum == 0) then
					(
						-- Store model-name in list
						append idrNameList objName
						
						local objPathName = ( RsRemovePathAndExtension( obj.filename ) )
						local baseFilename = RsConfigGetCacheDir() + "maps/" + objPathName + "/" + objName

						for ext in checkExts while (getModelSize == 0) do 
						(
							getModelSize = getFileSize (baseFilename + ext)
						)
						
						if (getModelSize == 0) do 
						(
							format "Model-file not found: % %\n" baseFilename (checkExts as string)
						)

						append idrSizeList getModelSize
					)
					else 
					(
						getModelSize = idrSizeList[findNum]
					)
				)
			)
			else 
			(
				local objName = obj.name
				
				local findNum = findItem idrNameList objName
				if (findNum == 0) then
				(
					-- Store IDR name in list
					append idrNameList idrName

					local objPathName = ( RsRemovePathAndExtension maxFilename )
					local baseFilename = RsConfigGetCacheDir() + "maps/" + objPathName + "/" + objName

					for ext in checkExts while (getModelSize == 0) do 
					(
						getModelSize = getFileSize (baseFilename + ext)
					)
					
					if (getModelSize == 0) do 
					(
						format "Model-file not found: % %\n" baseFilename (checkExts as string)
					)

					append idrSizeList getModelSize
				)
				else 
				(
					getModelSize = idrSizeList[findNum]
				)
			)
		) -- End of Gta Objects

		if (getModelSize == 0) then dontCollect else 
		(
			dataPair obj:obj size:getModelSize
		)
	) -- End of for each object
	
	return objSizes
) -- End of RsAssetDrawableMemSize function


-------------------------------------------------------------------------------------------
-- name:		RsAssetTXDMemSize
-- description: Fetch information regarding Texture Dictionary (TXD) asset sizes for Max objects
--				(includes XRefs)
--
-- [in]	 objs		 	List of Max objects to fetch information about
-- [in]	 inclRefs		Include Refs (boolean)
-- [in[  progress		Progress bar to update
-- [out] txdNameList	List of txd names
-- [out] txdSizeList	List of txd file sizes
-- [out] txdFileList	List of txd files
-------------------------------------------------------------------------------------------
fn RsAssetTXDMemSize objs inclRefs:false progress: = 
(
	local txdNameList = #("CHANGEME")
	local txdSizeList = #(0)
	local txdFileList = #(undefined)
	local txdAttrIndex = GetAttrIndex "Gta Object" "TXD"
	
	-- Load txd data for RsRefs:
	if inclRefs do 
	(
		local refDefs = for obj in objs where isRsRef obj collect obj.refDef
			
		RsRefFuncs.loadTxds (makeUniqueArray refDefs)
	)

	local objSizes = for objNum = 1 to objs.count collect 
	(
		local obj = objs[objNum]
		local txdFileSize = 0
		
		if (progress != unsupplied) do 
		(
			progress.value = 100.0 * objNum / objs.count
		)

		if "Gta Object" == ( GetAttrClass obj ) do 
		(
			if ( isRsRef obj ) then
			(
				if inclRefs and (obj.refDef != undefined) do 
				(
					local txdName = obj.refDef.txd
					local txdPathname = RsRemovePathAndExtension obj.filename

					local findNum = findItem txdNameList txdName
					
					if (findNum == 0) then 
					(						
						-- Store TXD name in list
						append txdNameList txdName

						-- Store TXD size in list
						local txdFilename = stringStream ""
						format "%maps/%/%.itd.zip" (RsConfigGetCacheDir()) txdPathname txdName to:txdFilename
						txdFilename = txdFilename as string
						
						txdFileSize = getfilesize txdFilename
							
						if (txdFileSize == 0) do 
						(
							format "TXD-file not found: %\n" txdFilename
						)

						append txdSizeList txdFileSize
						append txdFileList txdFilename
					)
					else 
					(
						txdFileSize = txdSizeList[findNum]
					)
				)
			)
			else 
			(
				-- Get TXD name attribute
				local txdName = ( GetAttr obj txdAttrIndex )
				
				local findNum = findItem txdNameList txdName

				if (findNum == 0) then
				(
					local txdPathname = RsRemovePathAndExtension maxFilename
					
					-- Store TXD name in list
					append txdNameList txdName

					-- Store TXD size in list
					local txdFilename = stringStream ""
					format "%maps/%/%.itd.zip" (RsConfigGetCacheDir()) txdPathname txdName to:txdFilename
					txdFilename = txdFilename as string

					txdFileSize = getfilesize txdFilename
						
					if (txdFileSize == 0) do 
					(
						format "TXD-file not found: %\n" txdFilename
					)
					
					append txdSizeList txdFileSize
					append txdFileList txdFilename
				)
				else 
				(
					txdFileSize = txdSizeList[findNum]
				)
			)
		)
		
		if (txdFileSize == 0) then dontCollect else 
		(
			dataPair obj:obj size:txdFileSize
		)
	) -- End of for each object
	
	return objSizes
)

-- End of script
--print (RsAssetDrawableMemSize selection inclRefs:true)
--print (RsAssetTXDMemSize selection inclRefs:true)