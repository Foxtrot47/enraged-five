-- Rockstar DAT Importer
-- Rockstar North
-- 14/8/2006
-- by Luke Openshaw

-- Creates splines from dat files.

filein "pipeline/helpers/arrays.ms"
	---------------------------------------------------------------
	-- Import data and build a spline
	---------------------------------------------------------------
	fn BuildLine file = (
		
		datFile = openFile file
		
		if datFile == undefined then (
			messagebox "Could not open file"
			return false
		)
		
		splineObj = Line()
		addNewSpline splineObj 
		leftstations = #()
		rightstations = #()
		leftstationnames = #()
		rightstationnames = #()
		tunnelpoints = #()
		forwardBranchPoints = #()
		backwardBranchPoints = #()
		forwardBranchTrackNames = #()
		backwardBranchTrackNames = #()
		i = 0
		isOpen = true
		while eof(datFile) == false do (
			currentLine = readLine datFile
			
			arrLine = filterString currentLine " "
			splinePoint = [0,0,0]
			inVector = [0,0,0]
			outVector = [0,0,0]
			if arrLine.count > 3 then (
				tokenIndex = 1
				if matchPattern (arrLine[tokenIndex] as string) pattern:"c" then (
					tokenIndex += 1
					splinePoint.x = arrLine[tokenIndex] as float
					tokenIndex += 1
					splinePoint.y = arrLine[tokenIndex] as float
					tokenIndex += 1
					splinePoint.z = arrLine[tokenIndex] as float
					tokenIndex += 1
					
					inVector.x = arrLine[tokenIndex] as float
					tokenIndex += 1
					inVector.y = arrLine[tokenIndex] as float
					tokenIndex += 1
					inVector.z = arrLine[tokenIndex] as float
					tokenIndex += 1
					
					outVector.x = arrLine[tokenIndex] as float
					tokenIndex += 1
					outVector.y = arrLine[tokenIndex] as float
					tokenIndex += 1
					outVector.z = arrLine[tokenIndex] as float
					tokenIndex += 1	

					print ("Add bezier knot at [" + splinePoint.x as string + ", " + splinePoint.y as string + ", " + splinePoint.z as string + "] with in vector [" + inVector.x as string + ", " + inVector.y as string + ", " + inVector.z as string + "] and out vector [" + outVector.x as string + ", " + outVector.y as string + ", " + outVector.z as string + "]\n")
					addKnot splineObj 1 #bezier #curve splinePoint inVector outVector

					-- 11th value is arc length, skip it
					tokenIndex += 1
					isCurvedNode = true
				)
				else (
					splinePoint.x = arrLine[tokenIndex] as float
					tokenIndex += 1
					splinePoint.y = arrLine[tokenIndex] as float
					tokenIndex += 1
					splinePoint.z = arrLine[tokenIndex] as float
					tokenIndex += 1	
					
					addKnot splineObj 1 #corner #curve splinePoint 

					-- 4th value is arc length, skip it
					tokenIndex += 1
				)
				local vertFlags = arrLine[tokenIndex] as number
				if 0<(bit.and vertFlags 1) then (
					append leftstations (i as string)
					if arrLine.count == tokenIndex+1 then append leftstationnames arrLine[tokenIndex+1]
				)
				else if 0<(bit.and vertFlags 2) then (
					append rightstations (i as string)
					if arrLine.count == tokenIndex+1 then append rightstationnames arrLine[tokenIndex+1]
				)
				else if 0<(bit.and vertFlags 4) then (
					append tunnelpoints (i as string)
				)
				else if 0<(bit.and vertFlags 8) then (
					append forwardBranchPoints (i as string)
					if arrLine.count == tokenIndex+1 then append forwardBranchTrackNames arrLine[tokenIndex+1]
				)
				else if 0<(bit.and vertFlags 16) then (
					append backwardBranchPoints (i as string)
					if arrLine.count == tokenIndex+1 then append backwardBranchTrackNames arrLine[tokenIndex+1]
				)
			)
			else if arrLine.count == 3 then (
				isOpen = matchPattern (arrLine[3] as string) pattern:"open"
			)
			i=i+1
		)
		
		if isOpen == false then (
			close splineObj 1
		)
		
		--stationStr = "stations="
		leftstationStr=""
		for station in leftstations do (
			leftstationStr = leftstationStr+station+","	
		)
		
		rightstationStr=""
		for station in rightstations do (
			rightstationStr = rightstationStr+station+","	
		)
		
		rightNameStr=""
		for stnname in rightstationnames do rightNameStr = rightNameStr+stnname+","
		
		leftNameStr=""
		for stnname in leftstationnames do leftNameStr = leftNameStr+stnname+","
			
		forwardBranchpointsStr=""
		for forwardBranchpoint in forwardBranchpoints do (
			forwardBranchpointsStr = forwardBranchpointsStr+forwardBranchpoint+","	
		)

		backwardBranchpointsStr=""
		for backwardBranchpoint in backwardBranchpoints do (
			backwardBranchpointsStr = backwardBranchpointsStr+backwardBranchpoint+","	
		)

		forwardBranchTrackNamesStr=""
		for forwardBranchTrackName in forwardBranchTrackNames do (
			forwardBranchTrackNamesStr = forwardBranchTrackNamesStr+forwardBranchTrackName+","	
		)

		backwardBranchTrackNamesStr=""
		for backwardBranchTrackName in backwardBranchTrackNames do (
			backwardBranchTrackNamesStr = backwardBranchTrackNamesStr+backwardBranchTrackName+","	
		)
			
		local tunnelpointsString = ""
		if tunnelpoints.count>0 then
			tunnelpointsString = RsArrayJoin tunnelpoints token:","
		
		setuserprop splineObj "leftstations" leftstationStr
		setuserprop splineObj "rightstations" rightstationStr
		setuserprop splineObj "leftstationnames" leftNameStr
		setuserprop splineObj "rightstationnames" rightNameStr
		setuserprop splineObj "forwardBranchpoints" forwardBranchpointsStr
		setuserprop splineObj "backwardBranchpoints" backwardBranchpointsStr
		setuserprop splineObj "forwardBranchTrackNames" forwardBranchTrackNamesStr
		setuserprop splineObj "backwardBranchTrackNames" backwardBranchTrackNamesStr
		setuserprop splineObj "inTunnel" tunnelpointsString
		print (leftstationStr)
		print (rightstationStr)
		print (leftNameStr)
		print (rightNameStr)
		print (forwardBranchpointsStr)
		print (backwardBranchpointsStr)
		print (forwardBranchTrackNamesStr)
		print (backwardBranchTrackNamesStr)

		updateShape splineObj
			
	)
	
	fn ExportSpline file = (
		splineObj = selection[1]
		
		if splineObj == undefined then (
			messagebox "Select a line object to export"
			return 0
		)
		
		leftstationstring = getuserprop splineObj "leftstations"
		tunnelstring = (getuserprop splineObj "inTunnel" as string)
		rightstationstring = getuserprop splineObj "rightstations"
		oldstylestationstring = getuserprop splineObj "stations"
		forwardBranchPointsString = getuserprop splineObj "forwardBranchpoints"
		backwardBranchPointsString = getuserprop splineObj "backwardBranchpoints"
		forwardBranchTrackNamesString = getuserprop splineObj "forwardBranchTrackNames"
		backwardBranchTrackNamesString = getuserprop splineObj "backwardBranchTrackNames"
		
		leftstationnamesstring = getuserprop splineObj "leftstationnames"
		rightstationnamesstring = getuserprop splineObj "rightstationnames"
		
		leftstations = #()
		rightstations = #()
		
		leftstationnames = #()
		rightstationnames = #()
		tunnelpoints = #()
		
		forwardBranchpoints = #()
		backwardBranchpoints = #()
		forwardBranchTrackNames = #()
		backwardBranchTrackNames = #()
		
		if leftstationstring != undefined then (
			stationtokens = filterstring leftstationstring ","
			
			for stationtok in stationtokens do (
				append leftstations (stationtok as number)
			)
		)
		
		-- Append the old style as left stations by default
		if oldstylestationstring != undefined then (
			stationtokens = filterstring oldstylestationstring ","

			for stationtok in stationtokens do (
				
				append leftstations (stationtok as number)
			)
		)
		
		if rightstationstring != undefined then (
			stationtokens = filterstring rightstationstring ","

			for stationtok in stationtokens do (
				append rightstations (stationtok as number)
			)
		)
		
		if leftstationnamesstring != undefined then (
			stationtokens = filterstring leftstationnamesstring ","

			for stationtok in stationtokens do (
	
				append leftstationnames stationtok 
			)
		)

		if rightstationnamesstring != undefined then (
			stationtokens = filterstring rightstationnamesstring ","

			for stationtok in stationtokens do (
				append rightstationnames stationtok
			)
		)
		
				
		if forwardBranchPointsString != undefined then (
			forwardBranchpointtokens = filterstring forwardBranchPointsString ","

			for forwardBranchpointtok in forwardBranchpointtokens do (
				append forwardBranchpoints (forwardBranchpointtok as number)
			)
		)
		
		if backwardBranchPointsString != undefined then (
			backwardBranchpointtokens = filterstring backwardBranchPointsString ","

			for backwardBranchpointtok in backwardBranchpointtokens do (
				append backwardBranchpoints (backwardBranchpointtok as number)
			)
		)

		if forwardBranchTrackNamesString != undefined then (
			forwardBranchTrackNamesTokens = filterstring forwardBranchTrackNamesString ","

			for forwardBranchTrackNametok in forwardBranchTrackNamesTokens do (
				append forwardBranchTrackNames forwardBranchTrackNametok
			)
		)

		if backwardBranchTrackNamesString != undefined then (
			backwardBranchTrackNametokens = filterstring backwardBranchTrackNamesString ","

			for backwardBranchTrackNametok in backwardBranchTrackNametokens do (
				append backwardBranchTrackNames backwardBranchTrackNametok
			)
		)
		
		if undefined!=tunnelstring then
		(
			tunnelpoints = for n in (filterString tunnelstring ", ") collect n as number
		)
		print ("tunnelpoints :"+tunnelpoints as string)
		
		--Removed for alan blaine
		if (rightstationnames.count != rightstations.count) or (leftstationnames.count != leftstations.count) then (
		--	messagebox "Not all stations have station names"
			print "Warning, there are stations without names."
			--return 0
		)
		
		if(forwardBranchTrackNames.count != forwardBranchpoints.count) or (backwardBranchTrackNames.count != backwardBranchpoints.count) then (
			print("Warning num forward track names "  + (forwardBranchTrackNames.count as string) + ", points " + (forwardBranchpoints.count as string) + ", backward names " + (backwardBranchTrackNames.count as string) + ", points " + (backwardBranchpoints.count as string))
			messagebox "Not all branch point have branch track name"
			return 0
		)
		
		if classof(splineObj) != line then (
			messagebox "Selected object is not line"
			return 0
		)
		
		datFile = createFile file
		
		if datFile == undefined then (
			messagebox "Could not create file"
			return false
		)
		
		knotCount = numKnots splineObj
		currLine = (knotCount as string) + " "
		
		curveCount = 0
		for i = 1 to knotCount do(
			if getKnotType $ 1 i == #bezier then (
				curveCount += 1
			)
		)
		
		append currLine ((curveCount as string) + " ")

		if isClosed $ 1 then (
			append currLine "close\n"
		)
		else (
			append currLine "open\n"
		)
		
		format currLine to: datFile
		
		local segmentLengths = GetSegLengths $ 1 byVertex: false
		local numSegments = segmentLengths.count
		local segmentLengthStartIndex = (segmentLengths.count - 1) / 2
		
		for i = 1 to numSegments do
		(
			print ("segment " + i as string + " has length " + segmentLengths[i] as string)
		)
		for i = 1 to knotCount do (
			local knotPoint = getKnotPoint $ 1 i
			local stationName = ""
			local branchTrackName = ""
			
			local leftIdx = findItem leftstations i
			local rightIdx = findItem rightstations i
			local isInTunnel = (findItem tunnelpoints i)!=0
			local forwardBranchPointIdx = findItem forwardBranchpoints i
			local backwardBranchPointIdx = findItem backwardBranchpoints i
			
			local stationIdx = 0
			local branchIdx = 0
			if leftIdx  !=0 then (
				stationIdx = 1
				if leftstationnames[leftIdx] != undefined then (
					stationName = leftstationnames[leftIdx]
				)
			)
			else if rightIdx !=0 then (
				stationIdx = 2
				if rightstationnames[rightIdx] != undefined then (
					stationName = rightstationnames[rightIdx]
				)
			)
			else if forwardBranchPointIdx != 0 then (
				branchIdx = 8
				branchTrackName = forwardBranchTrackNames[forwardBranchPointIdx]
				print("forward branch point idx " + forwardBranchPointIdx as string + ", name " + branchTrackName)
			)
			else if backwardBranchPointIdx != 0 then (
				branchIdx = 16
				branchTrackName = backwardBranchTrackNames[backwardBranchPointIdx]
				print("backward branch point idx " + backwardBranchPointIdx as string + ", name " + branchTrackName)
			)
			local vertFlags =  bit.or stationIdx branchIdx;
			
			if isInTunnel then
				vertFlags = bit.or vertFlags 4

			local segLength = 0
			local segIndex = segmentLengthStartIndex + i
			if segIndex < numSegments then segLength = segmentLengths[segIndex]
			
			if getKnotType $ 1 i == #bezier then (
				local inVec = getInVec $ 1 i
				local outVec = getOutVec $ 1 i
				currLine = ("c " + knotPoint.x as string + " " + knotPoint.y as string + " " + knotPoint.z as string + " " +
					inVec.x as string + " " + inVec.y as string + " " + inVec.z as string + " " +
					outVec.x as string + " " + outVec.y as string + " " + outVec.z as string + " " + segLength as string + " " + vertFlags as string)
			)
			else (
				currLine = (knotPoint.x as string + " " + knotPoint.y as string + " " + knotPoint.z as string + " " + segLength as string + " " + vertFlags as string)
			)
				
			if stationName != "" then (
				append currLine (" " + stationName)
			)
			else if branchTrackName != "" then (
				print("append track name " + branchTrackName)
				append currLine (" " + branchTrackName)
			)
			append currLine "\n"
			
			format currLine to:datFile
		)
		
		close datFile
		
		
	)

	
