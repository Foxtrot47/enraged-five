-- Rockstar Rex Report
-- Rockstar North
-- 17/4/2007
-- by Greg Smith

RsRexReport = #()
RsCountVal = 10
RsRetVal = true

fn RsRexExportWithReport = (

	RsRetVal = true

	RsRexReport = RexExport()

	if RsRexReport != true then (

		RsCountVal = 10
		RsRetVal = false
		
		rollout RsRexExportReport "rexExport Errors"
		(
			listbox lstMissing items:RsRexReport height:20
			checkbox chkFreeze "Freeze" pos:[50,280]
--			button btnOK "OK" width:100 pos:[150,280]
			button btnCancel "Close" width:100 pos:[250,280]
			timer tmrCount

--			on btnOK pressed do (
--
--				RsRetVal = true
--				DestroyDialog RsRexExportReport
--			)	
			
			on btnCancel pressed do 
			(
				DestroyDialog RsRexExportReport
			)		
						
			on tmrCount tick do (
/*
				if chkFreeze.checked == false then (

					if RsCountVal == 0 then (

						RsRetVal = false
						DestroyDialog RsRexExportReport 
					) else (

						RsCountVal = RsCountVal - 1
						btnCancel.text = "Close (" + (RsCountVal as string) + ")"
					)
				)*/
			)
			
			on RsRexExportWithReport open do 
			(
				btnCancel.text = "Close (" + (RsCountVal as string) + ")"
			)
			
			on lstMissing selected val do
			(
				local msg = lstMissing.items[val]
				local index = findString msg " - Context:"
				if index!=undefined then
				(
					local contextNodeName = substring msg (index+11) (msg.count - (index+10))
					local contextNode = getnodeByName contextNodeName
					if undefined!=contextNode then
					(
						select contextNode
					)
				)
			)
		)

		CreateDialog RsRexExportReport width:800 modal:true
	)
	
	RsRetVal
)