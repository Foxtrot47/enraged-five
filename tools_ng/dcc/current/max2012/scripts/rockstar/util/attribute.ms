-- Rockstar Map Export
-- Rockstar North
-- 11/5/2005
-- by Greg Smith

-- some useful utilities relating to the object attributes

----------------------------------------------------------------------------------------
-- turn the surface type enum into a string. this list has to match that in gta3.att
----------------------------------------------------------------------------------------
fn RsCollSurfaceTypeToString index = (

	-- rage treats underscores as a special case
	-- so we need to change underscores to dashes
	-- in the material names

	case index of (

		0: return "DEFAULT"
		1: return "TARMAC"
		2: return "TARMAC_FUCKED"
		3: return "TARMAC_REALLYFUCKED"
		4: return "PAVEMENT"
		5: return "PAVEMENT_FUCKED"
		6: return "GRAVEL"
		7: return "FUCKED_CONCRETE"
		8: return "PAINTED_GROUND"
		9: return "GRASS_SHORT_LUSH"
		10: return "GRASS_MEDIUM_LUSH"
		11: return "GRASS_LONG_LUSH"
		12: return "GRASS_SHORT_DRY"
		13: return "GRASS_MEDIUM_DRY"
		14: return "GRASS_LONG_DRY"
		15: return "GOLFGRASS_ROUGH"
		16: return "GOLFGRASS_SMOOTH"
		17: return "STEEP_SLIDYGRASS"
		18: return "STEEP_CLIFF"
		19: return "FLOWERBED"
		20: return "MEADOW"
		21: return "WASTEGROUND"
		22: return "WOODLANDGROUND"
		23: return "VEGETATION"
		24: return "MUD_WET"
		25: return "MUD_DRY"
		26: return "DIRT"
		27: return "DIRTTRACK"
		28: return "SAND_DEEP"
		29: return "SAND_MEDIUM"
		30: return "SAND_COMPACT"
		31: return "SAND_ARID"
		32: return "SAND_MORE"
		33: return "SAND_BEACH"
		34: return "CONCRETE_BEACH"
		35: return "ROCK_DRY"
		36: return "ROCK_WET"
		37: return "ROCK_CLIFF"
		38: return "WATER_RIVERBED"
		39: return "WATER_SHALLOW"
		40: return "CORNFIELD"
		41: return "HEDGE"
		42: return "WOOD_CRATES"
		43: return "WOOD_SOLID"
		44: return "WOOD_THIN"
		45: return "GLASS_WEAK"
		46: return "GLASS_MEDIUM"
		47: return "GLASS_STRONG"
		48: return "EMPTY1"
		49: return "EMPTY2"
		50: return "GARAGE_DOOR"
		51: return "THICK_METAL_PLATE"
		52: return "SCAFFOLD_POLE"
		53: return "LAMP_POST"
		54: return "METAL_GATE"
		55: return "METAL_CHAIN_FENCE"
		56: return "GIRDER"
		57: return "FIRE_HYDRANT"
		58: return "CONTAINER"
		59: return "NEWS_VENDOR"
		60: return "WHEELBASE"
		61: return "CARDBOARDBOX"
		62: return "PED"
		63: return "CAR"
		64: return "CAR_PANEL"
		65: return "CAR_MOVINGCOMPONENT"
		66: return "TRANSPARENT_CLOTH"
		67: return "RUBBER"
		68: return "PLASTIC"
		69: return "TRANSPARENT_STONE"
		70: return "WOOD_BENCH"
		71: return "CARPET"
		72: return "FLOORBOARD"
		73: return "STAIRSWOOD"
		74: return "P_SAND"
		75: return "P_SAND_DENSE"
		76: return "P_SAND_ARID"
		77: return "P_SAND_COMPACT"
		78: return "P_SAND_ROCKY"
		79: return "P_SANDBEACH"
		80: return "P_GRASS_SHORT"
		81: return "P_GRASS_MEADOW"
		82: return "P_GRASS_DRY"
		83: return "P_WOODLAND"
		84: return "P_WOODDENSE"
		85: return "P_ROADSIDE"
		86: return "P_ROADSIDEDES"
		87: return "P_FLOWERBED"
		88: return "P_WASTEGROUND"
		89: return "P_CONCRETE"
		90: return "P_OFFICEDESK"
		91: return "P_711SHELF1"
		92: return "P_711SHELF2"
		93: return "P_711SHELF3"
		94: return "P_RESTUARANTTABLE"
		95: return "P_BARTABLE"
		96: return "P_UNDERWATERLUSH"
		97: return "P_UNDERWATERBARREN"
		98: return "P_UNDERWATERCORAL"
		99: return "P_UNDERWATERDEEP"
		100: return "P_RIVERBED"
		101: return "P_RUBBLE"
		102: return "P_BEDROOMFLOOR"
		103: return "P_KIRCHENFLOOR"
		104: return "P_LIVINGRMFLOOR"
		105: return "P_CORRIDORFLOOR"
		106: return "P_711FLOOR"
		107: return "P_FASTFOODFLOOR"
		108: return "P_SKANKYFLOOR"
		109: return "P_MOUNTAIN"
		110: return "P_MARSH"
		111: return "P_BUSHY"
		112: return "P_BUSHYMIX"
		113: return "P_BUSHYDRY"
		114: return "P_BUSHYMID"
		115: return "P_GRASSWEEFLOWERS"
		116: return "P_GRASSDRYTALL"
		117: return "P_GRASSLUSHTALL"
		118: return "P_GRASSGRNMIX"
		119: return "P_GRASSBRNMIX"
		120: return "P_GRASSLOW"
		121: return "P_GRASSROCKY"
		122: return "P_GRASSSMALLTREES"
		123: return "P_DIRTROCKY"
		124: return "P_DIRTWEEDS"
		125: return "P_GRASSWEEDS"
		126: return "P_RIVEREDGE"
		127: return "P_POOLSIDE"
		128: return "P_FORESTSTUMPS"
		129: return "P_FORESTSTICKS"
		130: return "P_FORRESTLEAVES"
		131: return "P_DESERTROCKS"
		132: return "P_FORRESTDRY"
		133: return "P_SPARSEFLOWERS"
		134: return "P_BUILDINGSITE"
		135: return "P_DOCKLANDS"
		136: return "P_INDUSTRIAL"
		137: return "P_INDUSTJETTY"
		138: return "P_CONCRETELITTER"
		139: return "P_ALLEYRUBISH"
		140: return "P_JUNKYARDPILES"
		141: return "P_JUNKYARDGRND"
		142: return "P_DUMP"
		143: return "P_CACTUSDENSE"
		144: return "P_AIRPORTGRND"
		145: return "P_CORNFIELD"
		146: return "P_GRASSLIGHT"
		147: return "P_GRASSLIGHTER"
		148: return "P_GRASSLIGHTER2"
		149: return "P_GRASSMID1"
		150: return "P_GRASSMID2"
		151: return "P_GRASSDARK"
		152: return "P_GRASSDARK2"
		153: return "P_GRASSDIRTMIX"
		154: return "P_RIVERBEDSTONE"
		155: return "P_RIVERBEDSHALLOW"
		156: return "P_RIVERBEDWEEDS"
		157: return "P_SEAWEED"
		158: return "DOOR"
		159: return "PLASTICBARRIER"
		160: return "PARKGRASS"
		161: return "STAIRSSTONE"
		162: return "STAIRSMETAL"
		163: return "STAIRSCARPET"
		164: return "FLOORMETAL"
		165: return "FLOORCONCRETE"
		166: return "BIN_BAG"
		167: return "THIN_METAL_SHEET"
		168: return "METAL_BARREL"
		169: return "PLASTIC_CONE"
		170: return "PLASTIC_DUMPSTER"
		171: return "METAL_DUMPSTER"
		172: return "WOOD_PICKET_FENCE"
		173: return "WOOD_SLATTED_FENCE"
		174: return "WOOD_RANCH_FENCE"
		175: return "UNBREAKABLE_GLASS"
		176: return "HAY_BALE"
		177: return "GORE"
		178: return "RAILTRACK"
		179: return "FLESH"
		180: return "WALL_POST"
		181: return "BS_PAVEMENT"
		182: return "P_711SHELF4"
		183: return "P_711SHELF5"
		184: return "P_711SHELF6"
		185: return "P_711SHELF7"
		186: return "P_711SHELF8"
		187: return "P_711SHELF9"
		188: return "P_711SHELF10"
		189: return "P_711SHELF11"
		190: return "P_711SHELF12"
		191: return "P_711SHELF13"
		192: return "P_711SHELF14"
		193: return "P_711SHELF15"
		194: return "P_711SHELF16"
		195: return "P_711SHELF17"
		196: return "P_711SHELF18"
		197: return "P_711SHELF19"
		198: return "P_711SHELF20"
		199: return "P_711SHELF21"
		200: return "P_711SHELF22"
		201: return "P_711SHELF23"
		202: return "P_711SHELF24"
		203: return "P_711SHELF25"
		204: return "P_711SHELF26"
		205: return "P_711SHELF27"
		206: return "P_711SHELF28"
		207: return "P_711SHELF29"
		208: return "P_711SHELF30"
	)
	
	return "unknown"
)

fn RsGetCollSurfaceTypeString obj = (

	if getattrclass obj != "Gta Collision" then (
		
		return "unknown"
	)
	
	idxCollType = getattrindex "Gta Collision" "Coll Type"	
	
	idxStairs = getattrindex "Gta Collision" "Stairs"
	idxNonClimable = getattrindex "Gta Collision" "Non Climable"
	idxSeeThrough = getattrindex "Gta Collision" "See Through"
	idxShootThrough = getattrindex "Gta Collision" "Shoot Through"
	idxDoesntProvideCover = getattrindex "Gta Collision" "Does Not Provide Cover"
	idxPath = getattrindex "Gta Collision" "Path"
	idxCamCol = getattrindex "Gta Collision" "Non Camera Collidable"
	idxCamColAllowClip = getattrindex "Gta Collision" "Non Camera Collidable Allow Clipping"
	idxNoDecal = getattrindex "Gta Collision" "No Decal"
	idxNoNavmesh = getattrindex "Gta Collision" "No Navmesh"
	idxNoNetworkSpawn = getattrindex "Gta Collision" "No Network Spawn"
	
	idxRoomID = getattrindex "Gta Collision" "Room ID"
	
	flagVal = 0
	roomID = getattr obj idxRoomID
	
	if getattr obj idxStairs then flagVal = flagVal + 1
	if getattr obj idxNonClimable then flagVal = flagVal + 2
	if getattr obj idxSeeThrough then flagVal = flagVal + 4
	if getattr obj idxShootThrough then flagVal = flagVal + 8
	if getattr obj idxDoesntProvideCover then flagVal = flagVal + 16
	if getattr obj idxPath then flagVal = flagVal + 32
	if getattr obj idxCamCol then flagVal = flagVal + 64
	if getattr obj idxNoDecal then flagVal = flagVal + 256
	if idxNoNavmesh != undefined then (
		if getattr obj idxNoNavmesh then flagVal = flagVal + 512
	)
	if getattr obj idxNoNetworkSpawn then flagVal = flagVal + 16384
	if getattr obj idxCamColAllowClip then flagVal = flagVal + 32768
	
	valCollType = getattr obj idxCollType
	
	if valCollType == "" then valCollType = "DEFAULT"
	
	valCollType = valCollType + "|0|" + (roomID as string) + "|0|" + (flagVal as string)
	
--	RsCollSurfaceTypeToString valSurfaceType
	valCollType
)

fn RsGetCollRoomIDString obj = (

	if getattrclass obj != "Gta Collision" then (
		
		return "0"
	)
	
	roomID = getattr obj (getattrindex "Gta Collision" "Room ID")
	
	return (roomID as string)
)
