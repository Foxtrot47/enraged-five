--
-- IconMaker256Milo.ms: Creates an icon for the selected object
-- MUst be a Gta_MILO object
-- Must only be 1 selected object
-- Creates a 256 x 256 JPEG
-- Written by Shaun
-- Based on code Written by Aaron Garbut, Adam Fowler, Greg Smith
--

backgroundcolor = [128,128,128]

tempDir = sysInfo.tempdir
if "" == tempDir then tempDir = "C:\\"
miloClassName = "Gta_MILO"

sel = selection as array

if sel.count == 1 then
(
	miloObj = sel[1]
	text = (classof miloObj) as string
	if text == miloClassName then
	(

		clearSelection()
		selectMore miloObj.children
	
		savepos = miloObj.pos

		x = -savePos.x
		miloObj.pos = [0,0,0]
		
		max hide inv
						max tool zoomextents
		filenameJpg = tempDir + miloObj.name + ".jpg" 
		print filenameJpg 
		bitmapsave = render outputwidth:512 outputheight:512 vfb:false		bitmapsave.filename = filenameJpg
		save bitmapsave
		
		max unhide all
	
		miloObj.pos = savepos
	)
)
