--filein "X:/gta5/tools_ng/wildwest/script/3dsMax/Characters/Rigging/deltaMushSkinning/deltaMushWorker.ms"

filein "rockstar/util/deltaMush/deltaMushFunctions.ms"
filein "rockstar/util/deltaMush/deltaMushWorker.ms" 

-- filein "rockstar/util/deltaMush/deltaMushFunctions.ms" --hack to get around declaration scope.

filein (RsConfigGetToolsDir() + "dcc/current/max2012/scripts/rockstar/util/deltaMush/deltaMushFunctions.ms")

plugin simpleMod DeltaMushSetup
   name:"DeltaMushSetup"
   classID:#(0x556083ba, 0x694b8ca)
   version:1
   (	   
		parameters main rollout:params
		(
			RelaxAmount type:#float ui:spnRelaxAmount default:1.0
			RelaxIter type:#integer ui:spnRelaxIter default:3
		)
	   
		rollout params "DeltaMushSetup"
		(			
-- 			local relaxAmountController =bezier_float()
-- 				relaxAmountController.value = 1.0
-- 			local relaxIterController =bezier_float()
-- 				relaxIterController.value = 3
			
			spinner spnRelaxAmount "Relax Amount" tooltip: "Set amount of relaxing" width:100 align:#right type:#float range:[0,1,1] fieldWidth:30 --controller:relaxAmountController
			spinner spnRelaxIter "Relax Iterations" tooltip: "Set number of relax iterations" width:100 align:#right type:#integer range:[1,10,3] fieldWidth:30 --controller:relaxIterController 
			button btnApplyDeltaMush "Apply Delta Mush" width:140 height:30 tooltip:"Convert Delta Mush into a skin modifier"
			checkBox chkAutoSkin "Auto convert to Skin" tooltip:"Automatically convert the Delta mushing into a skin modifier."
			
-- 			on params open do 
-- 			(
-- 				spnRelaxAmount.value = 1.0
-- 				spnRelaxIter.value = 3
-- 			)
			
			on spnRelaxAmount changed val do 
			(								
				workerNode = getNodeByName ($.name+"_bindMesh_bindAnim")
				if workerNode != undefined do
				(
					if workerNode.modifiers[#DeltaMushWorker] != undefined do 
					(
						workerNode.modifiers[#DeltaMushWorker].RelaxAmount = val --spnRelaxAmount.value
					)
				)
			)

			on spnRelaxIter changed val do 
			(	
				workerNode = getNodeByName ($.name+"_bindMesh_bindAnim")
				if workerNode != undefined do
				(
					if workerNode.modifiers[#DeltaMushWorker] != undefined do 
					(
						workerNode.modifiers[#DeltaMushWorker].RelaxIter = val --spnRelaxAmount.value
					)
				)
			)
			
			on btnApplyDeltaMush pressed do 
			(
				local pickedObj = $
				local dummyNode = getNodeByName "Dummy01"
				local relaxAmount = spnRelaxAmount.value 
				local relaxIter = spnRelaxIter.value 
				local autoSkin = chkAutoSkin.state
				
				RSTA_ApplyDeltaMush pickedObj dummyNode relaxAmount relaxIter autoskin undefined
			)			
			
		)	   
   )