--modified from a snippet at form http://forums.cgsociety.org/showthread.php?f=6&t=1214485&highlight=delta+mush

/*

--REAL DELTA MUSH TECHNIQUE

1 a) original mesh : BINDMESH
1) relax copy of BINDMESH: RELAXBINDMESH
2) for each vert in RELAXBINDMESH calculate a local rest coordinate system R based on the normal n and tangent t at the vert
3) Calculate a vector offset V for each vert in the coordsys R. I think this means between the RELAXBINDMESH and BINDMESH
4) Cache these vector offset
5) When mesh deforms during animation, Delta Mush smooths a copy of the points as it did for RELAXBINDMESH. Call this ANIMRELAXMESH
6) Have anone relaxed animated mesh ANIMMESH
6) For each vert in ANIMRELAXMESH a local current coordinate system C is made similar to R
7) The volume can now be restored by moving each point in the ANIMMESH to V in the coordinate system C


*/

plugin simpleMod DeltaMushWorker
   name:"DeltaMushWorker"
   classID:#(0x6904bde3, 0x17c79b7b)
   version:1
   (
	   --we need a bind mesh, bindMesh_anim relaxMesh and relaxMesh anim. These all start of dupes of bindMesh_Anim.
	   --bindMesh is just a base editPoly no other modifiers
	   --relaxMesh is a copy of bindMesh but with a relax modifier on, usually with relax of 1.0 and 3 iterations, then an editpoly mod above
	   --relaxMesh_anim is a copy of bindMesh but with a skin modifier with 1 influence per vert, then an edit poly, then a relax as on relaxMesh, then an edit poly on top
	   --bindMesh anim is bindMesh with a copy of the skin modifier form relaxMesh_anim then an edit poly mod then THIS modifier at the top
	   
		fn RSTA_DM_GetVertNormPoly PolyA Vert = --Function to get vertex normal based on faces normals
		(
			PolyArr = (polyop.getFacesUsingVert PolyA Vert) as array
			Norm = [0.0,0.0,0.0]
			For kkkk = 1 to PolyArr.count do
			(
				Norm = Norm  + ((polyop.getFaceNormal PolyA PolyArr[kkkk]))-- * (PolyA.transform))
			)
			return (normalize (Norm / (PolyArr.count * 1.0)))
		)
		
		parameters main rollout:params
		(
			RelaxedMesh type:#node ui:Pck01 subAnim:false --relaxed mesh in "skin" pose
			NoneRelaxedMesh type:#node ui:Pck02 subAnim:false --non relaxed mesh in "skin" pose
			AnimatedRelaxed type:#node ui:Pck03 subAnim:false --animated relaxed mesh
			
			RelaxAmount type:#float ui:spnRelaxAmount default:1.0
			RelaxIter type:#integer ui:spnRelaxIter default:3			
			NoOfInfluences type:#integer ui:spnNoOfInfluences default:4
			
			--we apply this modifier to the none relaxed animated mesh
		)
		
		rollout params "DeltaMushWorker"
		(
			Pickbutton Pck01 "Relaxed" autoDisplay:true width:140 height:30 tooltip:"Relaxed mesh in bind pose"
			Pickbutton Pck02 "None Relaxed" autoDisplay:true width:140 height:30 tooltip:"None Relaxed Mesh in bind pose"
			Pickbutton Pck03 "Animated" autoDisplay:true width:140 height:30 tooltip:"Animated relaxed mesh"
			
			spinner spnRelaxAmount "Relax Amount" tooltip: "Set amount of relaxing" width:100 align:#right type:#float range:[0,1,1] fieldWidth:30 --controller:relaxAmountController
			spinner spnRelaxIter "Relax Iterations" tooltip: "Set number of relax iterations" width:100 align:#right type:#integer range:[1,10,3] fieldWidth:30 --controller:relaxIterController 
			
			spinner spnNoOfInfluences "Bone affect Limit" tooltip: "Set number of vertex influences for skin" width:100 align:#right type:#integer range:[1,20,4] fieldWidth:30
			button btnConvertToSkin "Convert to Skin" width:140 height:30 tooltip:"Convert Delta Mush into a skin modifier"
			checkBox chkCleanUp "Cleanup Mush assets" tooltip:"Cleanup Delta Mush objects on Convert to Skin" default:true
			
			on params open do 
			(
				chkCleanUp.state = true
				suf = "_deltaMush_bindAnim"
				mainObjName = substring $.name 1 (($.name.count ) - (suf.count))
				mainObj = getNodeByName mainObjName
				format ("mainObj:"+mainObj.name+"\n")
				format ("trying to set relaxAmount to "+(mainObj.modifiers[#DeltaMushSetup].RelaxAmount as string)+"\n")
				format ("trying to set RelaxIter to "+(mainObj.modifiers[#DeltaMushSetup].RelaxIter as string)+"\n")
				$.modifiers[#DeltaMushWorker].RelaxAmount = mainObj.modifiers[#DeltaMushSetup].RelaxAmount 
				$.modifiers[#DeltaMushWorker].RelaxIter = mainObj.modifiers[#DeltaMushSetup].RelaxIter 
			)
			
			on Pck03 picked obj do 
			(
				localNamePrefixLength = $.name.count
				relaxObjName = ((substring $.name 1 (localNamePrefixLength - 9))+"_relax")
				relaxObj = getNodeByName relaxObjName
				
				$.modifiers[#DeltaMushWorker].relaxAmount = relaxObj.modifiers[#Relax].Relax_Value
			)
						
			on spnRelaxAmount changed val do 
			(								
				localNamePrefixLength = $.name.count
				relaxObjName = ((substring $.name 1 (localNamePrefixLength - 9))+"_relax")
				
				relaxObj = getNodeByName relaxObjName
				
				relaxObj.modifiers[#Relax].Relax_Value = val --spnRelaxAmount.value
				completeRedraw()
			)

			on spnRelaxIter changed val do 
			(	
				localNamePrefixLength = $.name.count
				relaxObjName = ((substring $.name 1 (localNamePrefixLength - 9))+"_relax")
				
				relaxObj = getNodeByName relaxObjName
				
				relaxObj.modifiers[#Relax].iterations = val --spnRelaxIter.value
				completeRedraw()
			)
			
			on btnConvertToSkin pressed do 
			(
				--messagebox "ClicketyClick" beep:true
				
				local deltaMesh = $ --this is the mesh with the delta mush on as we want to track how it is deforming
				local noOfInfluences = spnNoOfInfluences.value
				local cleanUpDeltaData = chkCleanUp.state
				
				RSTA_DM_convertToSkin deltaMesh noOfInfluences cleanUpDeltaData 
			)			
			
			
		)
		
		on map i p do
		(
			if i != 0 and RelaxedMesh!=undefined  and NoneRelaxedMesh!=undefined  and AnimatedRelaxed!=undefined then
			(
				PointA = polyop.getVert RelaxedMesh i --relaxed mesh in bind
				PointB = polyop.getVert NoneRelaxedMesh i --none relaxed mesh in bind
				PointC = polyop.getVert AnimatedRelaxed i --animatd relaxed mesh
			  
				NormA = RSTA_DM_GetVertNormPoly RelaxedMesh i
				NormC = RSTA_DM_GetVertNormPoly AnimatedRelaxed i
				Sub = PointB - PointA
			  
				EdgesX = ((polyop.getEdgesUsingVert AnimatedRelaxed i) as array)
				NewSub = [0.0,0.0,0.0]
				For j = 1 to EdgesX.count do --for every edge create two matrixes. Get vertex pos in one Matrix. And transform it into second. Add them together.
				(
					EdgeVert = ((polyop.getVertsUsingEdge AnimatedRelaxed EdgesX[j])as array)
					NormA02 = normalize ((polyop.getVert RelaxedMesh EdgeVert[1]) - (polyop.getVert RelaxedMesh EdgeVert[2]))
					NormA03 = normalize (cross NormA02 NormA)
					NormA02 = normalize (cross NormA03 NormA)
					OldMatrix = inverse (matrix3 NormA  NormA02  NormA03  [0,0,0])
				  
					NormC02 = normalize ((polyop.getVert AnimatedRelaxed EdgeVert[1]) - (polyop.getVert AnimatedRelaxed EdgeVert[2]))
					NormC03 = normalize (cross NormC02 NormC)
					NormC02 = normalize (cross NormC03 NormC)
					NewMatrix = matrix3 NormC  NormC02 NormC03 [0,0,0]
					NewSub = NewSub + ((Normalize Sub) * OldMatrix * NewMatrix)
				)
				p = (Normalize NewSub) * (length Sub) + PointC
			)
			else(p)
		)
   )