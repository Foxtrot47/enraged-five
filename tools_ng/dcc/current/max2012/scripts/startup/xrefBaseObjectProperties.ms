utility xrefBaseProperties "Xref base object properties"
(
	local xrefPropSelectionChanged = undefined
	local DynRollout = undefined
	
	fn selectionChanged ev nd = 
	(
		if selection.count<1 or selection.count>1 then
		(
			try(closeRolloutFloater DynRollout)catch()
			DynRollout = undefined
			return false
		)
		
		local xrefobj = selection[1]
		
		if XrefObject!=(classof xrefobj) then
		(
			try(closeRolloutFloater DynRollout)catch()
			DynRollout = undefined
			return false
		)
		
		local obj = (xrefobj.getSourceObject false)
		if undefined==obj then return false
		
		local rci = rolloutCreator "dynXrefRollout" xrefobj.name
		rci.begin()
		rci.addControl #label "mylabel" "Note:This does NOT alter the original object." paramStr:"width:150 height:30 align:#left"
		
		local proplist = getPropNames obj
		local varnum = 0
		local height = 65
		for propKey in proplist do
		(
			local prop = getproperty obj propKey
			local type = classof prop
			local varname = "control"+(varnum as string)
			varnum = varnum+1
			case type of
			(
				BooleanClass:
				(
					rci.addControl #checkbox varname propKey paramStr:("checked:"+prop as string)
					local thecode = ("($"+xrefobj.name+".getsourceobject false)."+propKey+" = val")
					rci.addHandler varname #changed filter:on paramStr:"val" codeStr:thecode
				)
				String:
				(
					rci.addControl #editbox varname propKey
					local thecode = ("($"+xrefobj.name+".getsourceobject false)."+propKey+" = val")
					rci.addHandler varname #changed filter:on paramStr:"val" codeStr:thecode
				)
				Float:
				(
					rci.addControl #spinner varname propKey
					local thecode = ("($"+xrefobj.name+".getsourceobject false)."+propKey+" = val")
					rci.addHandler varname #changed filter:on paramStr:"val" codeStr:thecode
				)
				Integer:
				(
					rci.addControl #spinner varname propKey
					local thecode = ("($"+xrefobj.name+".getsourceobject false)."+propKey+" = val")
					rci.addHandler varname #changed filter:on paramStr:"val" codeStr:thecode
				)
-- 				String:
-- 				(
-- 					rci.addControl #button #myButton "My Button"
-- 					rci.addHandler #myButton #pressed filter:on codeStr:"MessageBox @Isn't this cool@ title:@Wow@"
-- 				)
				default: print ("Parameter \""+propKey+"\"'s type \""+(type as string)+"\" is not supported:")
			)
			height = height+15
		)
		if height>700 then height = 700
		try(closeRolloutFloater DynRollout)catch()
		DynRollout = newRolloutFloater "Xref base object properties" 200 height
		addRollout (rci.end()) DynRollout
	)
	
	on xrefBaseProperties open do
	(
		xrefPropSelectionChanged = NodeEventCallback selectionChanged:selectionChanged
	) -- end on MyUtil open
	on xrefBaseProperties close do
	(
		try(closeRolloutFloater DynRollout)catch()
		DynRollout = undefined
		xrefPropSelectionChanged = undefined
		gc light:true
	) -- end on MyUtil close
)	
