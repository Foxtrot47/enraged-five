-- LightInstance Helper Object
-- 18/11/2010
-- by Kevin Weinberg

filein "startup/objects/util/RageLight.ms"

(
	global RsRefLightsRerun -- Undefined if script hasn't been run before
	
	for slaveStruct in #(RsRefSlaveLights, RsRefSlaveBakeLights) do 
	(
		try (unregisterRedrawViewsCallback RsRefSlaveLights.postDelete) catch ()
		try (unregisterRedrawViewsCallback RsRefSlaveLights.fixUnparented) catch ()
	)

	callbacks.removeScripts id:#RsRefSlaveLights
	deleteAllChangeHandlers id:#RsRefSlaveLights
	
	-- This struct is initialised after light-instance objects are defined:
	global RsRefSlaveLights = undefined
	global RsRefSlaveBakeLights = undefined
	
	RSrefData.updateSlavesActive = false

	OK
)

plugin Light RageLightInstance
	name:"RAGE_Light_Instance"
	classID:#(0x386a41cb, 0x61cb00ca)
	category:"RAGE Lights"
	extends:RageLight
	autoPromoteDelegateProps:true
	invisible:true
	version:1
(
	local isSlave -- Lets functions know that this is a standard slave-object via isProperty
	
	local parentNode, parentTransform
	local originalXform, offsetXform
	local transformHandler, deleteHandler, paramHandler
	
	local slaveStruct
	local originalInst, actualOriginalAttribVals
	local originalName = ""
	
	parameters RsRefSlaveParams rollout:RsRefSlaveInstanceRollout
	(
		allChangedValueArray type:#stringtab tabSizeVariable:true

		originalPropValueArray type:#stringtab tabSizeVariable:true
		instancePropNameArray type:#stringtab tabSizeVariable:true
		
		originalAttributeValueArray type:#stringtab tabSizeVariable:true
		instanceAttributeNameArray type:#stringtab tabSizeVariable:true
		
		refHandle type:#string
	)
	
	fn updateColour = ()
	
	-- THIS ROLLOUT SHOULD BE KEPT IN-SYNC WITH OTHER INSTANCES OF RsRefSlaveInstanceRollout --
	rollout RsRefSlaveInstanceRollout "Instance Properties:"
	(
		label lblSrcName "Source Object:" align:#left across:2
		hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/RSref_Slave_Objects" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)	
		
		editText txtSrcName "" text:"unknown" readOnly:true offset:[-3,-3]
		
		multilistbox instanceAttributesListBox "Edited Attributes:" offset:[0,-2]
		button resetValButton "Reset selected values" width:120
		button resetValsButton "Reset all" width:120
		
		label lblGuid "GUID:" align:#left
		editText txtGuid "" text:"unknown" readOnly:true height:40 offset:[-3,-4]
		
		fn updateEditsRoll = 
		(
			-- Make sure RsRef database is active:
			if RsRefData.notLoaded do 
			(
				RsRefFuncs.databaseActive toolName:"Slave-object system" debugFuncSrc:"[updateEditsRoll]"
				
				if not RsRefData.notLoaded do 
				(
					select selection
				)
			)
			
			if not RsRefSlaveInstanceRollout.open do return false
				
			if (originalInst != undefined) do (txtSrcName.text = originalInst.originalName)
			
			txtGuid.text = RsWordWrap (refHandle as string) 130 delimiters:#("-", " ")
			
			local itemList = for item in allChangedValueArray collect item
			sort itemList
			
			local noChange = (itemList.count == instanceAttributesListBox.items.count)
			for n = 1 to itemList.count while noChange do 
			(
				noChange = (instanceAttributesListBox.items[n] == itemList[n])
			)
			
			-- Don't change display-list if it's not changed:
			if not noChange do (instanceAttributesListBox.items = itemList)
			updateColour()
		)
		
		on resetValButton pressed do 
		(
			for objIndex in instanceAttributesListBox.selection do
			(
				local attrName = instanceAttributesListBox.items[objIndex]
				RsRefSlaveLights.resetInstanceChange this attrName updateAttrs:false
			)
			RsRefSlaveLights.updateUnchangedAttribs this
			
			updateEditsRoll()
		)
		
		on resetValsButton pressed do 
		(
			instancePropNameArray = #()
			instanceAttributeNameArray = #()
			allChangedValueArray = #()
			
			RsRefSlaveLights.updateUnchangedAttribs this
			
			updateEditsRoll()
		)
			
		on RsRefSlaveInstanceRollout open do 
		(
			updateEditsRoll()
		)
		on RsRefSlaveInstanceRollout rolledUp down do (updateEditsRoll())
	)

	fn xformHandlerFunc = RsRefSlaveLights.xformHandlerFunc this
	fn createChangeHandler = RsRefSlaveLights.createChangeHandler this
	fn deleteHandlerFunc = RsRefSlaveLights.deleteHandlerFunc this
	fn updateUnchangedAttribs theNode: = RsRefSlaveLights.updateUnchangedAttribs this theNode:theNode
	fn handleAttributeChange attrName = RsRefSlaveLights.handleAttributeChange this attrName
	
	-- Called by createChangeHandler, if handlers are required:
	fn doSetupCallbacks theNode = 
	(
		-- Set up transform/deletion handlers:
		transformHandler = when transform theNode changes id:#RsRefSlaveLights do xformHandlerFunc()
		deleteHandler = when theNode deleted id:#RsRefSlaveLights do deleteHandlerFunc()
	)
	
	on postLoad do createChangeHandler()
	
	tool create 
	(
		on mousePoint click do
		(
			ClickPoint = worldPoint
			viewTM = getCPTM()
			nodeTM = (transMatrix ClickPoint) * (inverse viewTM)
			createChangeHandler()
			#stop
		)
	)
)

plugin Light RsPhotoLightInstance
	name:"RsPhotoLightInstance"
	classID:#(0x2344705a, 0x4b440a74)
	category:"RAGE Lights"
	extends:Free_Light
	autoPromoteDelegateProps:true
	invisible:true
	version:1
(
	local isSlave -- Lets functions know that this is a standard slave-object via isProperty
	
	local parentNode, parentTransform
	local originalXform, offsetXform
	local transformHandler, deleteHandler, paramHandler
	
	local slaveStruct
	local originalInst, actualOriginalAttribVals
	local originalName = ""
	
	parameters RsRefSlaveParams rollout:RsRefSlaveInstanceRollout
	(
		allChangedValueArray type:#stringtab tabSizeVariable:true

		originalPropValueArray type:#stringtab tabSizeVariable:true
		instancePropNameArray type:#stringtab tabSizeVariable:true
		
		originalAttributeValueArray type:#stringtab tabSizeVariable:true
		instanceAttributeNameArray type:#stringtab tabSizeVariable:true
		
		refHandle type:#string
	)
	
	fn updateColour = ()
	
	-- THIS ROLLOUT SHOULD BE KEPT IN-SYNC WITH OTHER INSTANCES OF RsRefSlaveInstanceRollout --
	rollout RsRefSlaveInstanceRollout "Instance Properties:"
	(
		label lblSrcName "Source Object:" align:#left across:2
		hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/RSref_Slave_Objects" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)	
		
		editText txtSrcName "" text:"unknown" readOnly:true offset:[-3,-3]
		
		multilistbox instanceAttributesListBox "Edited Attributes:" offset:[0,-2]
		button resetValButton "Reset selected values" width:120
		button resetValsButton "Reset all" width:120
		
		label lblGuid "GUID:" align:#left
		editText txtGuid "" text:"unknown" readOnly:true height:40 offset:[-3,-4]
		
		fn updateEditsRoll = 
		(
			-- Make sure RsRef database is active:
			if RsRefData.notLoaded do 
			(
				RsRefFuncs.databaseActive toolName:"Slave-object system" debugFuncSrc:"[updateEditsRoll]"
				
				if not RsRefData.notLoaded do 
				(
					select selection
				)
			)
			
			if not RsRefSlaveInstanceRollout.open do return false
			
			if (originalInst != undefined) do (txtSrcName.text = originalInst.originalName)
			
			txtGuid.text = RsWordWrap (refHandle as string) 130 delimiters:#("-", " ")

			local itemList = for item in allChangedValueArray collect item
			sort itemList
			
			local noChange = (itemList.count == instanceAttributesListBox.items.count)
			for n = 1 to itemList.count while noChange do 
			(
				noChange = (instanceAttributesListBox.items[n] == itemList[n])
			)
			
			-- Don't change display-list if it's not changed:
			if not noChange do (instanceAttributesListBox.items = itemList)
			updateColour()
		)
		
		on resetValButton pressed do 
		(
			for objIndex in instanceAttributesListBox.selection do
			(
				local attrName = instanceAttributesListBox.items[objIndex]
				RsRefSlaveBakeLights.resetInstanceChange this attrName updateAttrs:false
			)
			RsRefSlaveBakeLights.updateUnchangedAttribs this
			
			updateEditsRoll()
		)
		
		on resetValsButton pressed do 
		(
			RsRefSlavesAllowParamHandler = false
			instancePropNameArray = #()
			instanceAttributeNameArray = #()
			allChangedValueArray = #()
			RsRefSlavesAllowParamHandler = true
			
			RsRefSlaveBakeLights.updateUnchangedAttribs this
			
			updateEditsRoll()
		)
			
		on RsRefSlaveInstanceRollout open do 
		(
			updateEditsRoll()
		)
		on RsRefSlaveInstanceRollout rolledUp down do (updateEditsRoll())
	)

	fn xformHandlerFunc = RsRefSlaveBakeLights.xformHandlerFunc this
	fn createChangeHandler = RsRefSlaveBakeLights.createChangeHandler this
	fn deleteHandlerFunc = RsRefSlaveBakeLights.deleteHandlerFunc this
	fn paramHandlerFunc = RsRefSlaveBakeLights.paramHandlerFunc this
		
	fn updateUnchangedAttribs theNode: = RsRefSlaveBakeLights.updateUnchangedAttribs this theNode:theNode
	fn handleAttributeChange attrName = RsRefSlaveBakeLights.handleAttributeChange this attrName
	
	-- Called by createChangeHandler, if handlers are required:
	fn doSetupCallbacks theNode = 
	(
		-- Set up transform/deletion handlers:
		transformHandler = when transform theNode changes id:#RsRefSlaveLights do xformHandlerFunc()
		deleteHandler = when theNode deleted id:#RsRefSlaveLights do deleteHandlerFunc()
		paramHandler = when parameters theNode changes id:#RsRefSlaveLights do paramHandlerFunc()
		
		OK
	)
	
	on update do 
	(
		--RsRefSlavesAllowParamHandler = false
		
		
		--RsRefSlavesAllowParamHandler = true
	)
	
	on postLoad do createChangeHandler()
	
	tool create 
	(
		on mousePoint click do
		(
			ClickPoint = worldPoint
			viewTM = getCPTM()
			nodeTM = (transMatrix ClickPoint) * (inverse viewTM)
			createChangeHandler()
			#stop
		)
	)
)

------------------------------------------
-- Set up RsRefSlaveLights struct: --
------------------------------------------
(
	fn xmlRageLightFilter xmlNode = (xmlNode.class == "rage_light")
	
	-- There's a lot of possible photometric class-names...
	fn xmlPhotoLightFilter xmlNode = 
	(
		0 != findItem \
		#(
			"free_light",
			"free_linear",
			"free_area",
			"free_disc",
			"free_sphere",
			"free_cylinder",
			"target_light",
			"target_linear",
			"target_area",
			"target_disc",
			"target_sphere",
			"target_cylinder"
		) xmlNode.class
	)

	RsRefSlaveLights = RsRefSlaveTypeStruct \
		structName:"RsRefSlaveLights" \
		slaveClass:RageLightInstance \
		slaveAttrClass:"Gta LightPhoto" \
		xmlObjFilter:xmlRageLightFilter \
		objFlagName:#showLights \
		refFlagName:#hasLights \
		slaveArrayName:#lights
	RsRefSlaveLights.init()
	
	RsRefSlaveBakeLights = RsRefSlaveTypeStruct \
		structName:"RsRefSlaveBakeLights" \
		slaveClass:RsPhotoLightInstance \
		slaveAttrClass:"Gta LightPhoto" \
		xmlObjFilter:xmlPhotoLightFilter \
		objFlagName:#showbakelights \
		refFlagName:#hasLights \
		slaveArrayName:#bakeLights \ 
		excludeParams:#("includelist", "excludelist", "shadowgenerator", "flux", "webfile") \ 
		aliasParams:#(dataPair xmlName:"castshadows" objName:"cast_shadows", dataPair xmlName:"" objName:"distribution") \ 
		orderedParams:#("type","targetDistance","intensitytype","usekelvin") 
	RsRefSlaveBakeLights.init()
	
	-- CALLBACKS --
	callbacks.addScript #nodePreDelete "RsRefSlaveLights.preDelete (callbacks.notificationParam())" id:#RsRefSlaveLights
	callbacks.addScript #nodePreDelete "RsRefSlaveBakeLights.preDelete (callbacks.notificationParam())" id:#RsRefSlaveLights
)

-- Update slave-objects on script-rerun:
if (RsRefLightsRerun != undefined) do 
(
	RsRefData.refs.lights = undefined
	RsRefData.refs.bakeLights = undefined

	RsRefSlaveTypeStruct.updateAllSlaves()
)
RsRefLightsRerun = true

OK
