-- Rockstar mloPortal Object
-- Rockstar North
-- 14/10/2005
-- by Greg Smith

-- helper object for milo portal setup

global gCurrLightShaftEditObj = undefined
global gDirEditObj = undefined
		
fn DelDirEditObj = 
(
	try delete gDirEditObj catch()
	deleteAllChangeHandlers id:#idDirEditObj
	gCurrLightShaftEditObj = undefined
	gDirEditObj = undefined
)

plugin helper GtaLightShaft
	name:"Gta LightShaft"
	classID:#(0x15a33bcd, 0x18df6fab)
	category:"Gta"
	extends:RSGrid
( 
	local idxShaftColourR = getattrindex "Gta LightShaft" "R"
	local idxShaftColourG = getattrindex "Gta LightShaft" "G"
	local idxShaftColourB = getattrindex "Gta LightShaft" "B"
	local idxShaftLength = getattrindex "Gta LightShaft" "Length"
	local idxShaftSoftness = getattrindex "Gta LightShaft" "Softness"
	local idxShaftDirAmount = getattrindex "Gta LightShaft" "Direction Amount"
	local idxShaftDensityType = getattrindex "Gta LightShaft" "Density Type"
	local idxShaftVolumeType = getattrindex "Gta LightShaft" "Volume Type"
	local directionXidx = ( GetAttrIndex "Gta LightShaft" "Direction X" )
	local directionYidx = ( GetAttrIndex "Gta LightShaft" "Direction Y" )
	local directionZidx = ( GetAttrIndex "Gta LightShaft" "Direction Z" )

 	local idxShaftUseLightDir = getattrindex "Gta LightShaft" "Use Sun Light Direction"
 	local idxShaftUseLightCol = getattrindex "Gta LightShaft" "Use Sun Light Colour"
 	local idxShaftIntensityScale = getattrindex "Gta LightShaft" "Scale By Sun Intensity"
 	local idxShaftColourScale = getattrindex "Gta LightShaft" "Scale By Sun Colour"
 	local idxShaftFontAndBehind = getattrindex "Gta LightShaft" "Draw In Front And Behind"
	
	fn GetNode = 
	(
		local deps = refs.dependents this
		for r in deps where 
			(isValidNode r) and
			(classof r==GtaLightShaft) do
		(
			return r
		)
		return undefined
	)

	fn trygetattr idx attrName: = 
	(
		local theNode = GetNode()
		if unsupplied!=attrName then
		(
			if undefined==theNode then
				return execute ( getControlData "Gta LightShaft" attrName )
			return execute (getControlData "Gta LightShaft" attrName theNode )
		)
		
		if undefined==theNode then
			return getattrDefault "Gta LightShaft" idx
		return getattr theNode idx
	)
	
	fn trysetattr idx val = 
	(
		local theNode = GetNode()
		if undefined==theNode then
			return false
		setattr theNode idx val
	)	
	
	fn SetDirVec vec = 
	(
		trysetattr idxShaftLength (length vec)
		local normVec = normalize vec
		trysetattr directionXidx normVec.x
		trysetattr directionYidx normVec.y
		trysetattr directionZidx normVec.z
	)
	fn GetDirVec = 
	(
		local normVec = [
			trygetattr directionXidx,
			trygetattr directionYidx,
			trygetattr directionZidx]
		
		return normVec *= trygetattr idxShaftLength
	)
	
	fn DrawDirVecs = 
	(
		local theNode = GetNode()
		if undefined==theNode or theNode.isHidden then 
			return false
		local hl = theNode.Grid.length /2
		local hw = theNode.Grid.width /2
		local corners = #(
			[ -hw, -hl, 0],
			[  hw, -hl, 0],
			[  hw,  hl, 0],
			[ -hw,  hl, 0])
			
		local meshCache = RsGWPrimMgr.getPrim()
		meshCache.IRsGWPrim.Clear()
		for ci=1 to corners.count do
		(
			local c1 = corners[ci]
			local dv = GetDirVec()
			local c2 = c1+dv
			local ci2 = ci+1
			if ci2>corners.count then
				ci2 = 1
			local c3 = corners[ci2]+dv
			meshCache.IRsGWPrim.addLine #(c1,c2,c3)
		)
		meshCache.IRsGWPrim.setOrigin theNode.transform
		meshCache.IRsGWPrim.setColour (color \
				(trygetattr idxShaftColourR) \
				(trygetattr idxShaftColourG) \
				(trygetattr idxShaftColourB) \
			)
		meshCache.IRsGWPrim.draw false
	)
	
	fn RegisterDrawCallback onlyUnregister:false = 
	(
		unRegisterRedrawViewsCallback this.DrawDirVecs
		if not onlyUnregister then
			registerRedrawViewsCallback this.DrawDirVecs
	)
	
	rollout RsLightShaftRoll "Lightshaft params"
	(
		colorpicker pickCol "Colour"
		dropdownlist cmbDensityType "Density Type"
		dropdownlist cmbVolumeType "Volume Type"
		spinner spnLength "Length"
		spinner spnSoftness "Softness"
		spinner spnDirectionAmount "Direction Amount"
		
		checkbox chckUseLightCol "Use Sun Light Colour"
		checkbox chckIntensityScale "Scale by sun intensity"
		checkbox chckColourScale "Scale by sun colour"
		checkbox chckFontAndBehind "Draw In Front And Behind"
		
		checkbutton btnEditDir "Edit Direction" width:150 height:25
		
		on btnEditDir changed val do
		(
			local theNode = GetNode()
			if val and undefined!=theNode then 
			(
				local x = trygetattr directionXidx
				local y = trygetattr directionYidx
				local z = trygetattr directionZidx
				local l = trygetattr idxShaftLength
				local p = [x,y,z] * l
				p = p * (inverse theNode.rotation)
				gDirEditObj = point box:true pos:(theNode.pos + p) size:(theNode.Grid.length/2) wirecolor:pickCol.color
				gCurrLightShaftEditObj = theNode
				gDirEditObj.parent = theNode
				select gDirEditObj
				when transform gDirEditObj changes id:#idDirEditObj do
				(
					if undefined==gDirEditObj or undefined==gCurrLightShaftEditObj then
						return false
					local diff = in coordsys (inverse gCurrLightShaftEditObj.rotation as matrix3) (gDirEditObj.pos - gCurrLightShaftEditObj.pos)
					gCurrLightShaftEditObj.SetDirVec diff
				)
				when gDirEditObj deleted id:#idDirEditObj do
					DelDirEditObj()
			)
			else
			(
				DelDirEditObj()
			)
			redrawViews()
		)			
	
		on RsLightShaftRoll open do
		(
			cmbDensityType.items = filterString (getcontroldata "Gta LightShaft" "Density Type") ","
			cmbVolumeType.items = filterString (getcontroldata "Gta LightShaft" "Volume Type") ","
			
			pickCol.color = (color \
				(trygetattr idxShaftColourR) \
				(trygetattr idxShaftColourG) \
				(trygetattr idxShaftColourB) \
			)
			spnLength.range = trygetattr idxShaftLength attrName:"Length"
			spnSoftness.range = trygetattr idxShaftSoftness attrName:"Softness"
			spnDirectionAmount.range = trygetattr idxShaftDirAmount attrName:"Direction Amount"
			-- cope with old attribute having been unset
			if not (trygetattr idxShaftUseLightDir) then
			(
				spnDirectionAmount.value = 1.0
				trysetattr idxShaftUseLightDir true
			)
			
			cmbDensityType.selection = (trygetattr idxShaftDensityType) + 1
			cmbVolumeType.selection = (trygetattr idxShaftVolumeType) + 1
			
			chckUseLightCol.state = trygetattr idxShaftUseLightCol
			chckIntensityScale.state = trygetattr idxShaftIntensityScale
			chckColourScale.state = trygetattr idxShaftColourScale
			chckFontAndBehind.state = trygetattr idxShaftFontAndBehind
			
			local theNode = GetNode()
			if undefined!=theNode then
				theNode.wirecolor = pickCol.color
			
			if gCurrLightShaftEditObj!=theNode or undefined==gDirEditObj then
				DelDirEditObj()
			else if undefined!=theNode then
				btnEditDir.checked = true
			
			if undefined==theNode then
				for c in RsLightShaftRoll.controls do
					c.enabled = false
		)
		
		on pickCol changed val do
		(
			trysetattr idxShaftColourR val.r
			trysetattr idxShaftColourG val.g
			trysetattr idxShaftColourB val.b
		)
		on cmbDensityType selected val do
		(
			trysetattr idxShaftDensityType (val-1)
		)
		on cmbVolumeType selected  val do
		(
			trysetattr idxShaftVolumeType (val-1)
		)
		on spnLength changed val do 
		(
			trysetattr idxShaftLength val
		)
		on spnSoftness changed val do 
		(
			trysetattr idxShaftSoftness val
		)
		on spnDirectionAmount changed val do 
		(
			trysetattr idxShaftDirAmount val
		)
		on chckUseLightCol changed val do 
		(
			trysetattr idxShaftUseLightCol val
		)
		on chckIntensityScale changed val do 
		(
			trysetattr idxShaftIntensityScale val
		)
		on chckColourScale changed val do 
		(
			trysetattr idxShaftColourScale val
		)
		on chckFontAndBehind changed val do 
		(
			trysetattr idxShaftFontAndBehind val
		)
	)
	on postLoad do
	(
		RegisterDrawCallback()
	)
	on postCreate do 
	(
		RegisterDrawCallback()
	)
	on deleted do
	(
		RegisterDrawCallback onlyUnregister:true
		DelDirEditObj()
	)
	on clone original do 
	(
		RegisterDrawCallback()
	)
)