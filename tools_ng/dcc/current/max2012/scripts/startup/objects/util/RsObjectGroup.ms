-- Helper to define IMAP Group properties centralised in a scene

plugin Helper ObjectGroupHelper
extends:Dummy
name:"RS ObjectGroup"
classID:#(0x43557692, 0x52628d2b)
replaceUI:true
category:"RS Helpers"
invisible:true
(
	local meshObj = undefined
	
	fn GetNode = 
	(
		for r in (refs.dependents this) where isValidnode r do 
			return r
	)

	parameters main rollout:ObjectHelperRoll
	(
		objectGroupName type:#string default:"" ui:editImapName
		viewportSize type:#float default:10.0 ui:spnViewportSize
		
		on viewportSize set val do
		(
			meshObj = undefined
		)
	)
	
	rollout ObjectHelperRoll "Properties"
	(
		label lbl1 "Object group Name:"
		edittext editImapName "" width:140
		
		spinner spnViewportSize "Viewport Size"
	)

	on getDisplayMesh do
	(
		local theNode = GetNode()
		gw.text theNode.pos ("Group \""+objectGroupName+"\"") color:yellow
		if (meshObj == undefined) do
		(
			meshObj = createInstance box length:viewportSize width:viewportSize height:viewportSize mapCoords:false
		)
		meshObj.mesh
	)
)