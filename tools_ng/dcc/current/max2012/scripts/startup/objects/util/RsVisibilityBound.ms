--
-- File:: startup/objects/util/RsVisibilityBound.ms
-- Description:: Manual box scripted object to override IDE/ITYP bounds.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 8 August 2011
--

--
-- name: RsVisibilityBound
-- desc: Artists can create these box bounds to override the bound data output
--       in the IDE/ITYP file(s).
--
plugin helper RsVisibilityBound
	name:"RsVisibilityBound"
	classID:#( 0x37eb4b76, 0x69a3545c )
	category:"RS Helpers"
	replaceUI:true
	extends:dummy
(
	local showMesh, boxMinWas, boxMaxWas, meshObjs
	
	parameters params rollout:paramsRoll
	(
		autoUpdate	type:#boolean default:true animatable:false
		updateAnim	type:#boolean default:true animatable:true
		
		boxMin		type:#point3 default:[-1.0,-1.0,-1.0] animatable:false
		boxMax		type:#point3 default:[1.0,1.0,1.0] animatable:false
	)
	
	fn getAllParentMeshes obj outVal:#() = 
	(
		if (superClassOf obj == GeometryClass) do 
		(
			append outVal obj
			
			for childObj in obj.children do 
			(
				getAllParentMeshes childObj outVal:outVal
			)
		)
		
		outVal
	)
	
	fn setBBox animation:false checkCreateMode:true = 
	(
		local obj = (refs.dependentNodes This)[1]
		local objParent = obj.parent
		
		-- Get parent-object mesh-object, and its mesh-children:
		meshObjs = getAllParentMeshes objParent
		
		local startTime = animationRange.start
		local endTime = if animation then animationRange.end else animationRange.start
		
		-- Get position-matrices for mesh-verts in local space:
		local objVertPosMatrixLists = for obj in meshObjs collect
		(
			local objMesh = copy obj.mesh
			(for vertNum = 1 to objMesh.numVerts collect (transMatrix (getVert objMesh vertNum)))
		)
		
		if (meshObjs.count != 0) then 
		(
			local startXform
			at time startTime 
			(
				startXform = obj.parent.ObjectTransform
				obj.transform = startXform
				setInheritanceFlags obj #none
			)
			
			pushPrompt "Fitting Bounding Box"
			
			local minPositions = #()
			local maxPositions = #()
			
			for thisTime = startTime to endTime do 
			(
				at time thisTime 
				(
					for objNum = 1 to meshObjs.count do 
					(
						local meshObj = meshObjs[objNum]						
						local meshObjTrans = meshObj.ObjectTransform * (inverse startXform)

						local posList = for item in objVertPosMatrixLists[objNum] collect (item * meshObjTrans).translationPart
						
						local minPos, maxPos
						RsGetBBox posList &minPos &maxPos

						append minPositions minPos
						append maxPositions maxPos
					)
				)
			)
			
			popPrompt()
			
			local minPos, maxPos
			
			RsGetBBox minPositions &minPos &maxPos
			boxMin = minPos
			
			RsGetBBox maxPositions &minPos &maxPos
			boxMax = maxPos
		)
		else 
		(
			-- Set default parameters:
			local tempObj = createInstance RsVisibilityBound
			boxMin = tempObj.boxMin
			boxMax = tempObj.boxMax
			
			if (objParent == undefined) then 
			(
				messageBox "Object needs a mesh/poly parent for this to work" title:"Object has no parent"
			)
			else 
			(
				messageBox "Object needs a mesh/poly parent for this to work" title:"Invalid parent object"
			)
		)
		
		showMesh = undefined
		completeRedraw()
	)
	
	-- This function works out if the fit-button was clicked from an existing object, or during Create mode.
	-- If the object hasn't been created yet, this will create one in the scene before fitting its box.
	fn startSetBBox animation:false = 
	(
		if (isCreatingObject RsVisibilityBound) then 
		(
			local objParent
			
			if (selection.count == 1) do 
			(
				objParent = selection[1]
			)
			
			if (not isKindOf objParent RsVisibilityBound) and (isProperty objParent "mesh") then  
			(
				undo "Create Bounding Box" on 
				(
					local obj = RsVisibilityBound parent:objParent
					obj.setBBox animation:animation
				)
			)
			else 
			(
				messageBox "Please select a mesh/poly object first" title:"Invalid object selected"
			)
		)
		else 
		(
			undo "Fit Bounding Box" on 
			(
				setBBox animation:animation
			)
		)
	)
	
	rollout paramsRoll "Options"
	(
		local spnWidth = 60
		local spnOffsetMin = [-34,0]
		local spnOffsetMax = [-16,0]
		
		group "Edit bounding box:"
		(
			label lblMaxMin "Min:               Max:" align:#left offset:[16,-3]
			label lblX "X:" align:#left offset:[0,1] across:3
			spinner spnXMin "" width:spnWidth range:[-99999.0, 99999.0, -1.0] offset:spnOffsetMin
			spinner spnXMax "" width:spnWidth range:[-99999.0, 99999.0, 1.0] offset:spnOffsetMax
			label lblY "Y:" align:#left offset:[0,1] across:3
			spinner spnYMin "" width:spnWidth range:[-99999.0, 99999.0, -1.0] offset:spnOffsetMin
			spinner spnYMax "" width:spnWidth range:[-99999.0, 99999.0, 1.0] offset:spnOffsetMax
			label lblZ "Z:" align:#left offset:[0,1] across:3
			spinner spnZMin "" width:spnWidth range:[-99999.0, 99999.0, -1.0] offset:spnOffsetMin
			spinner spnZMax "" width:spnWidth range:[-99999.0, 99999.0, 1.0] offset:spnOffsetMax
		)

		label lblHelpBack "          " align:#right offset:[0,0]
		hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Overriding_Object_Visibility_Bounds" align:#right offset:[0,-40] color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
		
		local minSpinners = #(spnXMin, spnYMin, spnZMin)
		local maxSpinners = #(spnXMax, spnYMax, spnZMax)
		
		group "Fit box to parent:"
		(
			button btnAutoFit 		"1st Frame" width:96 align:#left across:2
			checkButton chkAutoUpdate "Auto" width:36 align:#right
			button btnAutoFitAnim "Animation" width:96 align:#left across:2
			checkButton chkAutoUpdateAnim "Auto" width:36 align:#right
		)
		
		fn setCtrls = 
		(
			local bounds = #(boxMin, boxMax)
			local boundsCtrls = #(minSpinners, maxSpinners)
			
			for boundNum = 1 to 2 do 
			(
				for ctrlNum = 1 to 3 do 
				(
					boundsCtrls[boundNum][ctrlNum].value = bounds[boundNum][ctrlNum]
				)
			)
			
			chkAutoUpdate.checked = autoUpdate and not updateAnim
			chkAutoUpdateAnim.checked = autoUpdate and updateAnim
			
			if (isCreatingObject RsVisibilityBound) do 
			(
				minSpinners.enabled = false
				maxSpinners.enabled = false
			)
		)
		
		fn setMinMaxVal doMin idx val = 
		(
			autoUpdate = false
			#(chkAutoUpdate, chkAutoUpdateAnim).checked = false
			
			local bound = if doMin then boxMin else boxMax
			local oppBound = if doMin then boxMax else boxMin
			bound[idx] = val

			if doMin then 
			(
				boxMin = bound
				if (val > oppBound[idx]) do 
				(
					oppBound[idx] = val
					boxMax = oppBound
				)
			)
			else 
			(
				boxMax = bound
				if (val < oppBound[idx]) do 
				(
					oppBound[idx] = val
					boxMin = oppBound
				)
			)
			
			setCtrls()
			redrawViews()
		)
		
		on spnXMin changed newVal do (setMinMaxVal true 1 newVal)
		on spnXMax changed newVal do (setMinMaxVal false 1 newVal)
		on spnYMin changed newVal do (setMinMaxVal true 2 newVal)
		on spnYMax changed newVal do (setMinMaxVal false 2 newVal)
		on spnZMin changed newVal do (setMinMaxVal true 3 newVal)
		on spnZMax changed newVal do (setMinMaxVal false 3 newVal)
		
		on chkAutoUpdate changed newVal do 
		(
			if newVal then 
			(
				chkAutoUpdateAnim.state = false
				autoUpdate = true
				updateAnim = false
				startSetBBox()
				setCtrls()
			)
			else 
			(
				autoUpdate = false
			)
		)
		on chkAutoUpdateAnim changed newVal do 
		(
			if newVal then 
			(
				chkAutoUpdate.state = false
				autoUpdate = true
				updateAnim = true
				startSetBBox animation:true
				setCtrls()
			)
			else 
			(
				autoUpdate = false
			)
		)
		
		on btnAutoFit pressed do
		(
			if updateAnim do (autoUpdate = false)
			startSetBBox()
			setCtrls()
		)
		
		on btnAutoFitAnim pressed do
		(
			if not updateAnim do (autoUpdate = false)
			startSetBBox animation:true
			setCtrls()
		)
		
		on paramsRoll open do 
		(
			lnkHelp.pos.y = lblHelpBack.pos.y = 7
			
			setCtrls()
		)
	)
	
	on update do 
	(
		-- Refresh mesh:
		showMesh = boxMinWas = boxMaxWas = meshObjs = undefined
		boxMin = boxMin
	)
	
	on load do 
	(
		local obj = (refs.dependentNodes This)[1]
		local objParent = obj.parent
		
		-- Get parent-object mesh-object, and its mesh-children:
		meshObjs = getAllParentMeshes objParent
	)
	
	on getDisplayMesh do 
	(
		local valueDiff = (boxMinWas != boxMin) or (boxMaxWas != boxMax)
		
		if autoUpdate and (meshObjs == undefined) do 
		(
			setBBox animation:updateAnim
		)
		
		if (showMesh == undefined) or valueDiff do 
		(
			boxMinWas = boxMin
			boxMaxWas = boxMax
			
			if valueDiff and paramsRoll.open do 
			(
				paramsRoll.setCtrls()
			)
			
			local boxSize = (boxMax - boxMin)
			
			showMesh = manip.makeBox (boxMin + (0.5 * boxSize)) boxSize.y boxSize.x boxSize.z 1 1 1
		)
		
		return showMesh
	)
	
	tool create prompt:"Click to create object" 
	(
		local preCreateSel
		
		on start do 
		(
			preCreateSel  = undefined
			
			-- Store link to the currently-selected object, before selection is cleared by the incoming created object:
			local currentSel = GetCurrentSelection()
			if (currentSel.count == 1) and (not isKindOf currentSel[1] RsVisibilityBound) and (isProperty currentSel[1] "mesh") do 
			(
				preCreateSel = currentSel[1]
			)
		)
		
		on mousePoint click do 
		(
			-- Auto-fit box if a suitable parent-object was selected before creation:
			if (preCreateSel != undefined) then 
			(
				local thisObj = (refs.dependentNodes This)[1]
				thisObj.parent = preCreateSel
				
				setBBox animation:true
				
				select preCreateSel
			)
			else 
			(
				nodeTM.translation = gridPoint
			)			
			
			#stop
		)
	) -- end create tool
)

-- Set up object-move callback, to ensure that RsVisibilityBound transforms always match their parents' at the start-frame:
if (RsVisBndEventCallback != undefined) do 
(
	format "Resetting RsVisibilityBound node callback\n"
	RsVisBndEventCallback = undefined
	gc light:true
)

-- Called by RsVisBndXformEvent, updates moved RsVisibilityBound objects if they or their parents are moved
fn RsVisBndUpdateObj obj setBbox:false = 
(
	if (isKindOf obj RsVisibilityBound) then 
	(
		if (obj.parent != undefined) do 
		(
			if setBbox and obj.autoUpdate then 
			(
				obj.setBBox animation:obj.updateAnim
			)
			else 
			(
				local objXform = obj.transform
				local parXform = at time animationRange.start obj.parent.ObjectTransform
				local xformSame = true
				
				for n = 1 to 4 where (distance objXform[n] parXform[n] > 0.0001) while xformSame do 
				(
					xformSame = false
					
					obj.transform = parXform
				)
			)
		)
	)
	else 
	(
		for childObj in obj.children do 
		(
			RsVisBndUpdateObj childObj setBbox:true
		)
	)
)

-- Triggered when a node is transformed:
fn RsVisBndXformEvent ev Nodes = 
(
	local changeObjs = for nodeHandle in nodes collect (getAnimByHandle nodeHandle)
	
	local visBndObjs = for obj in helpers where (isKindOf obj RsVisibilityBound) collect obj
	
	for obj in visBndObjs do 
	(
		local keepBBox = true
		
		if obj.autoUpdate do 
		(
			for item in obj.meshObjs while keepBBox do 
			(
				if (not isValidNode item) or ((findItem changeObjs item) != 0) or ((findItem changeObjs item.parent) != 0) do (keepBBox = false)
			)
		)

		if not keepBBox then 
		(
			obj.setBBox animation:obj.updateAnim
			completeRedraw()
		)
		else 
		(
			if (obj.parent != undefined) and (((findItem changeObjs obj) != 0) or ((findItem changeObjs obj.parent) != 0)) do 
			(
				local objXform = obj.transform
				local parXform = at time animationRange.start obj.parent.ObjectTransform
				local xformSame = true
				
				for n = 1 to 4 where (distance objXform[n] parXform[n] > 0.0001) while xformSame do 
				(
					xformSame = false
					
					obj.transform = parXform
				)
				completeRedraw()
			)
		)
	)
)

global RsVisBndEventCallback = NodeEventCallback controllerOtherEvent:RsVisBndXformEvent mouseUp:true

--Test-export command:
--SceneXmlExportFrom (objects as array) "x:/testy.xml" (gRsUlog.Filename())

-- startup/objects/util/RsVisibilityBound.ms
