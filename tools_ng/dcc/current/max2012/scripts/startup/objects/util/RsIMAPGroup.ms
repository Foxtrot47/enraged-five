-- Helper to define IMAP Group properties centralised in a scene

filein "RsObjectGroup.ms"

plugin Helper IMAPGroupHelper
extends:ObjectGroupHelper
name:"RS IMAP Group Helper"
classID:#(0x53295ee4, 0x28ab885b)
replaceUI:false
category:"RS Helpers"
(
	fn SetListBoxItems = 
	(
		if undefined==this.IMAPHelperRoll then
			return false
		
		this.IMAPHelperRoll.lstImapGroupTimeSet.items = for i=1 to 24 collect (i as string)
		local itemsset = #{}
		for i =1 to 32 do
			itemsset[i] = bit.get this.imapGroupTime i
		this.IMAPHelperRoll.lstImapGroupTimeSet.selection = itemsset

		this.IMAPHelperRoll.lstImapGroupWeatherSet.items = gWeatherTypes
		local itemsset = #{}
		for type in this.imapGroupWeatherTypes do
		(
			local foundWeatherIndex = findItem gWeatherTypes type
			if 0!=foundWeatherIndex then
				itemsset[foundWeatherIndex] = true
			this.IMAPHelperRoll.lstImapGroupWeatherSet.selection = itemsset
		)
	)

	parameters main rollout:IMAPHelperRoll
	(
		imapGroupTypes type:#stringTab tabSizeVariable:true
		imapGroupTime type:#integer
		imapGroupWeatherTypes type:#stringTab tabSizeVariable:true
	)
	
	rollout IMAPHelperRoll "Properties"
	(
		button btnFindImapGroup "Find" width:140
		button btnSelectGroup "Select objects" width:140
		multilistbox lstImapGroupTimeSet ""
		multilistbox lstImapGroupWeatherSet ""
		
		on IMAPHelperRoll open do
		(
			SetListBoxItems()
		)
		on lstImapGroupTimeSet selectionEnd do
		(
			imapGroupTime = 0
			for bitnum in lstImapGroupTimeSet.selection do
				imapGroupTime = bit.set imapGroupTime bitnum true
			
			if 0!=imapGroupTime then
				appendIfUnique imapGroupTypes "TIME_DEPENDENT"
			else if 0!=findItem imapGroupTypes "TIME_DEPENDENT" then
				deleteItem imapGroupTypes (findItem imapGroupTypes "TIME_DEPENDENT")
		)
		on lstImapGroupWeatherSet selectionEnd do
		(
			imapGroupWeatherTypes = #()
			for bitnum in lstImapGroupWeatherSet.selection do
				append imapGroupWeatherTypes lstImapGroupWeatherSet.items[bitnum]

			if 0!=imapGroupWeatherTypes.count then
				appendIfUnique imapGroupTypes "WEATHER_DEPENDENT"
			else if 0!=findItem imapGroupTypes "WEATHER_DEPENDENT" then
				deleteItem imapGroupTypes (findItem imapGroupTypes "WEATHER_DEPENDENT")
		)
		
		on btnFindImapGroup pressed do
		(
			rollout RsFindImapGroupRoll "Find Imap Groups in scene"
			(
				local obj = undefined
				
				listbox listFoundImapGroups ""
				
				on RsFindImapGroupRoll open do 
				(
					local idxImapGroup = getattrindex "Gta Object" "IPL Group"
					local foundGroups = #()
					for o in objects where "Gta Object"==getattrclass o do
					(
						local imapGroup = getattr o idxImapGroup
						if "undefined" != (toLower imapGroup) then
							appendIfUnique foundGroups imapGroup
					)
					listFoundImapGroups.items = foundGroups
				)
				
				on listFoundImapGroups doubleclicked val do
				(
					if $!=obj and IMAPGroupHelper == classof $ then
						$.delegate.objectGroupName = listFoundImapGroups.selected
					try( DestroyDialog RsFindImapGroupRoll ) catch()
				)
			)
			CreateDialog RsFindImapGroupRoll modal:true
		)
		
		on btnSelectGroup pressed do
		(
			local idxImapGroup = getattrindex "Gta Object" "IPL Group"
			
			clearSelection()
			for o in objects where "Gta Object"==getattrclass o and delegate.objectGroupName==(getattr o idxImapGroup) do
			(
				selectmore o
			)
		)
	)
)