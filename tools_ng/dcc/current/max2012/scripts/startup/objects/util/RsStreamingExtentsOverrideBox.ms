--
-- File:: startup/objects/util/RsStreamingExtentsOverrideBox.ms
-- Description:: Streaming Extents Override box helper.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 24 November 2011
--

plugin SimpleObject RsStreamingExtentsOverrideBox
	name:"RsStreamingExtentsOverrideBox"
	category:"RS Objects"
	replaceUI:false
	extends:Box2Geometry
	classID:#(0x49251b2b, 0x73650349)
(	
) -- plugin

-- startup/objects/util/RsStreamingExtentsOverrideBox.ms
