plugin simpleMod RsSplineFlatten
name:"Rs Spline Flatten"
classID:#(0x1a8b0b9b, 0x60ae1dfc)
version:1
(
	local theNode = undefined
	
	-- FUNCTIONS -------------------------------------------------------------
	fn pickFilter obj = (classof obj == Line)
	
	-- PARAMETERS --------------------------------------------------------
	parameters main rollout:params
	(
		amount type:#integer ui:amtSpin default:4
		spline type:#node 
	)

	-- USER INTERFACE ----------------------------------------------------
	rollout params "Spline Flatten Options"
	(
		group "Input Spline : "
		(
			pickbutton btn_pickSpine "Pick Spline"  filter:pickFilter width:140
		)
		
		group "Options : "
		(
			spinner amtSpin "Thickness: " type:#integer range:[0,1000,4] width:140 offset:[-10,0]	
			--checkbox cbx_autoUpdate "AutoUpate" width:140 
		)
		
		-- UI EVENTS -----------------------------------------------------
		on params open do 
		(
			if (spline != undefined) do btn_pickSpine.text = spline.name
			theNode = (refs.dependentnodes this)[1]
		)
		--------------------------------------------------------------------
		on btn_pickSpine picked obj do
		(
			if (obj != undefined) do 
			(
				spline = obj
				btn_pickSpine.text = spline.name
			)
		)		
		
	)

	-- PLUGIN EVENTS  --------------------------------------------------------------
	on load do theNode = (refs.dependentnodes this)[1]	
	on map i p do
	(
		if (spline != undefined AND theNode != undefined) then 
		(
			-- GET DISTANCE
			local wP = p + theNode.pos
			local cP = pathInterp spline 1 (nearestPathParam spline 1 wP)
			-- DISTANCE WITHOUT Z
			local dist = distance [wP.x,wP.y,0] [cP.x,cP.y,0]
			if (dist < amount) then p =  [p.x,p.y,cp.z] 	
			else p			
		)
		else p		
	)
	
)
