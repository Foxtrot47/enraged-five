plugin simpleMod RsSplineTwistPreview
name:"RsSplineTwistPreview"
classID:#(0x5d12f6b1, 0x124ba469)
version:1
(
	local theNode = undefined
	local doUpdate = false
	local waitUntil = timestamp()
	local widthUpdate = false	
	
	-- PARAMETERS --------------------------------------------------------
	parameters main rollout:params
	(
		sTwists type:#nodetab tabSizeVariable:true
		spline type:#node 	
		on spline set arg do this.getSTwists()		
	)
	----------------------------------------------------------------------------------------
	fn splineFilter obj = 
	(
		if (classof obj == Line) do return true
		if (classof obj == SplineShape) do return true
		false
	)
	----------------------------------------------------------------------------------------
	fn thisNodeValid = 
	(
		if (theNode == undefined) do theNode = (refs.dependentnodes this)[1]
		return (theNode != undefined) 
	)
	----------------------------------------------------------------------------------------
	fn ease_in_out t b c d =
	(
		t = t as float; b = b as float; c = c as float; d = d as float 	
		
		if ((t /= d / 2) < 1) then v =  (c - b) / 2 * t * t + b		
		else v = -(c - b) / 2 * ((t - 1) * ((t - 1) - 2) - 1) + b
		
		return v 
	)
	-----------------------------------------------------------
	fn sTwist_sortByPercent v1 v2 =
	(
		if (v1.percent < v2.percent) do return -1
		if (v1.percent > v2.percent) do return 1
		return 0
	)
	-----------------------------------------------------------
	fn getSTwists = 
	(
		if (spline != undefined) do 
		(		
			local temp_sTwists =  for c in spline.children where (classof c == RsSplineTwist) collect c			
			qsort temp_sTwists sTwist_sortByPercent			
			sTwists = temp_sTwists
		)
	)
	-----------------------------------------------------------
	fn get_twistAtPercent spline percent = 
	(	
		-- CHECK FOR DELETES
		for i in sTwists do if (i == undefined) do getSTwists()
		-- IF NOT ENOUGH SPLINE TWISTS 
		if (sTwists.count < 1) do return 0

		-- GET SPLINE TWISTS TO THE LEFT AND RIGHT OF PERCENT POINT
		local c = 1
		while (c < sTwists.count + 1) AND (percent > sTwists[c].percent) do c += 1			
		local idx_left = if (c == 1) then c else c-1
		local idx_right = if (c > sTwists.count) then idx_left else c				
		local left_sTwist = sTwists[idx_left]
		local right_sTwist = sTwists[idx_right]
		
		-- GET RELATIVE PERCENT BETWEEN SPLINE TWISTS					
		local relativePercent = (100.0/(right_sTwist.percent - left_sTwist.percent)) * (percent - left_sTwist.percent)
		relativePercent = (if relativePercent > 100 then 100 else relativePercent)

		-- GET TWIST AMOUNT WITH EASE IN/OUT 				
		return (ease_in_out relativePercent left_sTwist.angle right_sTwist.angle 100)					
	)
	----------------------------------------------------
	fn updateLengthSegs =  
	(
		if (thisNodeValid()) AND (spline != undefined) AND (classof theNode.baseobject == Plane) do 
		(
			local splineLegth = (getSegLengths spline 1)			
			splineLegth = splineLegth[splineLegth.count]
			theNode.lengthsegs = (splineLegth/10) as integer		
		)
	)
	----------------------------------------------------
	fn setWidth arg = 
	(
		if (theNode != undefined) AND (classof theNode.baseobject == Plane) do 
		(
			widthUpdate = true
			theNode.width = arg
		)
	)

	-- USER INTERFACE ----------------------------------------------------
	rollout params "Options"
	(		
		pickbutton btn_pickSpine "Pick Spline"  filter:splineFilter width:150 	
		--button btn_updateLengthSegs "Update Length" width:150
		spinner spn_width "Width :" align:#right fieldwidth:50 offset:[8,0]
		
		
		-- UI EVENTS -----------------------------------------------------
		on params open do 
		(
			if (spline != undefined) do btn_pickSpine.text = spline.name
			getSTwists()
			if (theNode != undefined) AND (classof theNode.baseobject == Plane) do 
			(
				spn_width.value = theNode.width
			)
		)
		-----------------------------------------------------------------------------
		on btn_pickSpine picked obj do
		(
			if (obj != undefined) do 
			(
				spline = obj
				btn_pickSpine.text = spline.name				
			)
		)	
		-----------------------------------------------------------------------------
		--on btn_updateLengthSegs pressed do updateLengthSegs()		
		on spn_width changed arg do  setWidth arg	
	)
	
	-- LOCALS --------------------------------------------------------------
	local lengthFraction
	local spPos
	local spTangent 
	local tm

	-- PLUGIN EVENTS  --------------------------------------------------------------
	on map i p do
	(
		if (i == 1) AND NOT (widthUpdate) do this.updateLengthSegs()
		widthUpdate = false			

		if (spline != undefined) AND (thisNodeValid()) do 
		(			
			-- LENGTH  ALONG SPLINE				
			lengthFraction =  (p.y - min.y)/(max.y - min.y) 			
			lengthFraction = if (lengthFraction >= 1) then 1 else lengthFraction
			lengthFraction = if (lengthFraction <= 0) then 0 else lengthFraction
			
			-- CURRENT SPLINE INFO 						
			spPos = interpCurve3D spline 1 lengthFraction		
			spTangent = tangentCurve3D spline 1 lengthFraction		
			
			-- TRANS MATRIX 			
			tm = transMatrix [p.x,0,p.z]	
				
			-- SPLINE DIRECTION				
			rotateX tm (atan2 spTangent.z 1) -- SLOPE	
			rotateY tm (get_twistAtPercent spline (lengthFraction * 100)) 	-- TWIST/ROLL
			rotateZ tm  -(atan2 spTangent.x spTangent.y) 	-- DIRECTION					
				
			p = spPos + tm.pos - theNode.pos					
			
			return p	
		)
		return p		
	)	
)
