--
-- File:: startup/objects/util/RsOcclusionBox.ms
-- Description:: Occlusion box helper.
--
-- 	Author: Neil Finlay (neil.finlay@rockstarnorth.com)
--	Date: 27/08/14
-- 	Added a really roundabout method of catching the object as it's
--	deselected and centreing it's pivot.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 3 October 2011
--

global gRsOcclusionBoxCallBackItem = undefined

plugin SimpleObject RsOcclusionBox
	name:"RsOcclusionBox"
	category:"RS Objects"
	replaceUI:false
	extends:Box2Geometry
	classID:#(0xf8e73c7, 0x2e512b81)
(
	
	fn GetNode = (
		refsArray = (refs.dependents this)
		local theNode = for i in refsArray where isvalidnode i collect i
		if (classof theNode)==Array then theNode = theNode[1]
		theNode
	)

	fn callbackFunc ev nodes = (
		for n in nodes where (classof (GetAnimByHandle n) == RsOcclusionBox) do(
			obj = GetAnimByHandle n
			CenterPivot obj
		)
	)
	
	on create do(
		if(gRsOcclusionBoxCallBackItem == undefined) do(
			gRsOcclusionBoxCallBackItem = NodeEventCallback mouseUp:true selectionChanged:callbackFunc
		)
	)		
	
	-- Take care of loaded ones.
	on load do(
		n = GetNode()
		if (n != undefined) then CenterPivot( n )
	)

	-- Occlusion Box helper utilities.
	rollout util "Utilities"
	(
		group "Boxes"
		(			
			button btnBoxSelectAll "Select All" width:130
			button btnBoxConvertAll "Convert All Boxes" width:130
			button btnBoxConvertSel "Convert Selected Boxes" width:130
			checkbox chkDeleteBoxAfterConvert "Delete after conversion" width:130
		)
		group "Meshes"
		(
			button btnMeshSelectAll "Select All" width:130
			button btnMeshSetIsOccl "Set as 'Is Occlusion'" width:130
			button btnMeshClearIsOccl "Clear 'Is Occlusion'" width:130
		)
		
		fn convertToOcclusion o =
		(			
			if ( Box == classof o ) then
			(
				occlBox = RsOcclusionBox()
				
				occlBox.Box2.Length	= o.Length
				occlBox.Box2.Width = o.Width
				occlBox.Box2.Height	= o.Height
				occlBox.Box2.LengthSegments = o.LengthSegments
				occlBox.Box2.WidthSegments = o.WidthSegments
				occlBox.Box2.HeightSegments = o.HeightSegments
				occlBox.Box2.GenerateUVs = false
				occlBox.Box2.Front = true
				occlBox.Box2.Top = true
				occlBox.Box2.Left = true
				occlBox.Box2.Back = true
				occlBox.Box2.Bottom = true
				occlBox.Box2.Right = true
				occlBox.transform = o.transform
				occlBox.name = o.name
				
				occlBox
			)
			else
			(
				format "Couldn't copy parameters from %:% to %:% as they aren't both the required type (box2Geometry and RsPopZone or \
						RsMapZone)\n" box2Obj (classof box2Obj) newZoneObj (classof newZoneObj)
			)	
		)
		
		on btnBoxSelectAll pressed do
		(
			clearSelection()
			for o in $objects do
			(
				if ( classof o == RsOcclusionBox ) then 
				(
					selectMore o
				)
			)			
		)
		
		on btnBoxConvertAll pressed do
		(
			local boxDeleteList = #()
			for o in $objects do
			(
				if ( classof o == Box ) do 
				(
					convertToOcclusion o
					append boxDeleteList o 
				)
			)
			
			if (chkDeleteBoxAfterConvert.checked)do for i in boxDeleteList do delete i
		)
		
		on btnBoxConvertSel pressed do
		(
			local boxDeleteList = #()
			for o in $selection do
			(
				if ( classof o == Box ) do 
				(
					convertToOcclusion o
					append boxDeleteList o 
				)
			)
			
			if (chkDeleteBoxAfterConvert.checked)do for i in boxDeleteList do delete i
		)
		
		on btnMeshSelectAll pressed do
		(
			clearSelection()
			for o in $objects do
			(
				if ( "Gta Object" == GetAttrClass o ) then selectMore o
			)
		)
		
		on btnMeshSetIsOccl pressed do
		(
			local idxIsOcclusion = ( GetAttrIndex "Gta Object" "Is Occlusion" )
			for o in $selection do
			(
				if ( "Gta Object" == GetAttrClass o ) then
				(
					SetAttr o idxIsOcclusion true
				)
			)
		)
		
		on btnMeshClearIsOccl pressed do
		(
			local idxIsOcclusion = ( GetAttrIndex "Gta Object" "Is Occlusion" )
			for o in $selection do
			(
				if ( "Gta Object" == GetAttrClass o ) then
				(
					SetAttr o idxIsOcclusion false
				)
			)
		)
	) -- rollout
) -- plugin

-- startup/objects/util/RsOcclusionBox.ms
