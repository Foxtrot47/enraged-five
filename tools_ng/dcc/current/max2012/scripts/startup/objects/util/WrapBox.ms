-- Box wrapping around object's extremes
-- Author: Gunnar Droege
-- Date: 12/8/2011

plugin simpleObject wrapbox
	name:"Wrap Box"
	category:"RS Useful Geometry"
	extends:Box
	--replaceUI:true
	--classID:#(0x19036f36, 0x31065ab5)
(
	local wrap
	local alignment = 1
	local widthm, height, depth
	
	parameters params rollout:ui
	(
		userTransform type:#matrix3 default:(matrix3 1)
		userYaw type:#float ui:yaw default:0.0
		userPitch type:#float ui:pitch default:0.0
		userRoll type:#float ui:roll default:0.0
		userPush type:#float ui:push default:0.0
		
		on userYaw set val do
		(
			userTransform = rotateYPRMatrix userYaw userPitch userRoll
			this.createMesh()
		)
		on userPitch set val do
		(
			userTransform = rotateYPRMatrix userYaw userPitch userRoll
			this.createMesh()
		)
		on userRoll set val do
		(
			userTransform = rotateYPRMatrix userYaw userPitch userRoll
			this.createMesh()
		)
		on userPush set val do
		(
			this.createMesh()
		)
	)
	
	rollout ui "Alignment"
	(
		radiobuttons alignButtons labels:#( "World","Object") default:1
		spinner yaw "yaw" range:[-180,180,0] type:#float
		spinner pitch "pitch" range:[-180,180,0] type:#float
		spinner roll "roll" range:[-180,180,0] type:#float
		spinner push "push" range:[-1,5,0] type:#float scale:0.01
		
		on alignButtons changed val do
		(
			alignment=val
			this.createMesh()
		)
	)
	
	on buildMesh do
	(
	)
	
	tool create
	( 
		--on start do #stop
	)
	
	on create do
	(
		this.createMesh()
		#stop
	)
	
	fn createMesh = 
	(
		undo off
		(
			if wrap!=undefined then delete wrap
			if $!="undefined" and (classof $ != Editable_Poly) and (classof $ != Editable_Mesh) then
			(
				print ("Object is not compatible:"+(classof $) as string)
				return null
			)
			else
			(
				userTransform = rotateYPRMatrix userYaw userPitch userRoll
				local firstoffset = $.pos
				local firstrotation = $.rotation
				local firstTrans = $.transform 
				local guessedTrans

				local facePoints
				if classof $ == Editable_Poly then
					facePoints = polyOp.getVertsUsingFace $ (polyOp.getFaceSelection $)
				else if classof $ == Editable_Mesh then
					facePoints = meshOp.getVertsUsingFace $ (getFaceSelection $)
				
				facePoints = (facePoints as array)
				
				if facePoints.count==0 then
					if classof $ == Editable_Poly then
						for k=1 to (polyOp.getNumVerts $) do 
							append facePoints k
					else if classof $ == Editable_Mesh then
						for k=1 to (getNumVerts $) do 
							append facePoints k
				
				
				--print facePoints
				--print "******"

				local minX, maxX
				local minY, maxY
				local minZ, maxZ
				local inited=false
				local position
				local dimensions
				
				--print userTransform

				--get selected faces
				for pointInst in facePoints do
				(
					local currpointInst

					if alignment==1 then
					(
						if classof $ == Editable_Poly then
							currpointInst = (polyOp.getVert $ pointInst )
						else
							currpointInst = (meshOp.getVert $ pointInst )
					)
					else if alignment==2 then
					(
						if classof $ == Editable_Poly then
							currpointInst = (polyOp.getVert $ pointInst ) * inverse firstTrans 
						else
							currpointInst = (meshOp.getVert $ pointInst ) * inverse firstTrans 
					)

					--user transform
					currpointInst *= inverse userTransform
					
					--dum = point pos:(currpointInst) size:0.1
					--init
					if not inited then
					(
						minX = currpointInst.x
						maxX = currpointInst.x
						minY = currpointInst.y
						maxY = currpointInst.y
						minZ = currpointInst.z
						maxZ = currpointInst.z
						inited = true
					)
					else (
						if currpointInst.x < minX then minX = currpointInst.x
						if currpointInst.x > maxX then maxX = currpointInst.x
						if currpointInst.y < minY then minY = currpointInst.y
						if currpointInst.y > maxY then maxY = currpointInst.y
						if currpointInst.z < minZ then minZ = currpointInst.z
						if currpointInst.z > maxZ then maxZ = currpointInst.z
					)
					
				)			

				if facePoints.count>0 then 
				(
					dimensions = [maxX-minX, maxY-minY, maxZ-minZ]
					--print dimensions
		
					wrap = box width:dimensions.x length:dimensions.y height:dimensions.z lengthsegs:delegate.lengthsegs widthsegs:delegate.widthsegs heightsegs:delegate.heightsegs mapcoords:delegate.mapcoords
					
					local trns = matrix3 1
					
					--translation
					local pos = [minX, minY, minZ] + [dimensions.x/2,dimensions.y/2,dimensions.z/2]
					trns = transMatrix pos
					if alignment==2 then 
						userTransform *= firstTrans
		
					--apply pretransforms
					trns = trns * userTransform

					wrap.transform = trns
					
					--scale
					wrap.objectOffsetPos = [0,0,-dimensions.z/2]
					wrap.scale = [1,1,1]+(dimensions/5*userPush)
					
					mesh = wrap
				)
				else messageBox "Select some faces!"
			)
		)
	)
	
	on create do
	(
		when parameters this.delegate changes lengthsegs do createMesh()
		when parameters this.delegate changes widthsegs do createMesh()
		when parameters this.delegate changes heightsegs do createMesh()
		when parameters this.delegate changes mapcoords do createMesh()
	)
)
