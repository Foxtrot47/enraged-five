fn getSphericalTCNameDefs =
(
	local iniPath = (RsConfigGetToolsDir() + "etc/config/generic/SphericalTCNameDefs.ini")
	if doesFileExist iniPath == false then gRsPerforce.sync #(iniPath)
	
	global gGtaTimeCycleSphere_NameDefs = for name in getINISetting iniPath "TCnames" collect name
	
	global gGtaTimeCycleSphere_NameDescriptions = for def in getINISetting iniPath "TCnames" collect
	(
		getIniSetting iniPath "TCnames" def
	)
)

plugin helper GtaTimeCycle
	name:"Gta TimeCycle"
	classID:#(0x38397ec9, 0x59f36f99)
	category:"Gta"
	autoPromoteDelegateProps:true
	replaceUI:true
	initialRollupState:0x02
	extends:BoxGizmo
(
	local rollupState = false
	
	local gtaAttr_Name = 		getAttrIndex "Gta TimeCycle" "IDString"
	local gtaAttr_Range = 		getAttrIndex "Gta TimeCycle" "Range"
	local gtaAttr_Percentage = 	getAttrIndex "Gta TimeCycle" "Percentage"
	local gtaAttr_StartHour = 	getAttrIndex "Gta TimeCycle" "StartHour"
	local gtaAttr_EndHour = 	getAttrIndex "Gta TimeCycle" "EndHour"
	local doUpdate = false
	
	local meshCacheRadius = 	undefined
	local meshCacheRange = 		undefined
	local radiusColour = 		color 255 192 0
	local rangeColour = 		color 0 127 255
	
	local TCNameDefs = #()
	
	local meshObj = undefined
	local theNode
	
	parameters pBlock rollout:params
	(
		tcName 		type:#string	ui:edtName			default:""
		range 		type:#float 	ui:spnRange			default:1.0
		percentage 	type:#float 	ui:spnPercentage	default:0.0
		startHour 	type:#integer 	ui:spnStartHour		default:0
		endHour 	type:#integer 	ui:spnEndHour		default:23
	)
	
	--//////////////////////////////////////////////////
	-- Update Attrs
	--/////////////////////////////////////////////////
	fn updateAttrs attr: =
	(
		--setAttr theNode gtaAttr_Name theNode.params.edtName.text
		case attr of
		(
			#tcname:setAttr theNode gtaAttr_Name this.tcName
			#range:setAttr theNode gtaAttr_Range this.range
			#percent:setAttr theNode gtaAttr_Percentage this.percentage
			#starthour:setAttr theNode gtaAttr_StartHour this.startHour
			#endhour:setAttr theNode gtaAttr_EndHour this.endHour
			
			default:
			(
				setAttr theNode gtaAttr_Name this.tcName
				setAttr theNode gtaAttr_Range this.range
				setAttr theNode gtaAttr_Percentage this.percentage
				setAttr theNode gtaAttr_StartHour this.startHour
				setAttr theNode gtaAttr_EndHour this.endHour
			)
		)
	)
	
	fn pushAttrUpdate =
	(
		if theNode == undefined then theNode = (refs.dependents this)[1]
		
		if theNode != undefined then
		(
			this.tcName 	=	getAttr theNode gtaAttr_Name
			this.range 		= 	getAttr theNode gtaAttr_Range
			this.percentage =	getAttr theNode gtaAttr_Percentage
			this.startHour	=	getAttr theNode gtaAttr_StartHour
			this.endHour	=	getAttr theNode gtaAttr_EndHour
		)
	)
	
	rollout params "TC Settings"
	(
		group "Properties:"
		(
			edittext edtName "Name: "
			dropdownlist ddlNameDefs
			edittext edtNameDefDesc "Description:"labelOnTop:true readOnly:true height:36
			spinner spnRange "Range" type:#float default:0.25 range:[1.0,1000,1.5]
			spinner spnPercentage "Percentage" type:#float
			spinner spnStartHour "StartHour" type:#integer range:[0,23,0]
			spinner spnEndHour "EndHour" type:#integer range:[0,23,23]
		)
		
		group "Dimensions:"
		(
			spinner spnLength "Length" type:#float range:[0, 10000, this.length]
			spinner spnWidth "Width" type:#float range:[0, 10000, this.width]
			spinner spnHeight "Height" type:#float range:[0, 10000, this.height]
		)
		
		--//////////////////////////////////////////////////
		-- FUNCTIONS
		--//////////////////////////////////////////////////
		
		
		
		--//////////////////////////////////////////////////
		--EVENTS
		--//////////////////////////////////////////////////
		
		on spnLength changed val do
		(
			this.length = val
			redrawViews()
		)
		
		on spnWidth changed val do
		(
			this.width = val
			redrawViews()
		)
		
		on spnHeight changed val do
		(
			this.height = val
			redrawViews()
		)
		
		--//////////////////////////////////////////////////
		-- Name
		--//////////////////////////////////////////////////
		on edtName changed arg do
		(
			updateAttrs attr:#tcname
		)
		
		--//////////////////////////////////////////////////
		-- TC item
		--//////////////////////////////////////////////////
		fn ddlChanged arg =
		(
			edtName.text = ddlNameDefs.selected
			
			edtNameDefDesc.text = gGtaTimeCycleSphere_NameDescriptions[ddlNameDefs.selection]
			ddlIndex = ddlNameDefs.selection
			
			updateAttrs attr:#tcname
		)
		on ddlNameDefs selected arg do ddlChanged arg
		
		
		--//////////////////////////////////////////////////
		-- Range
		--//////////////////////////////////////////////////
		on spnRange changed val do
		(
			if val > (this.length + 0.1) then
			(
				updateAttrs attr:#range
			)
			else
			(
				updateAttrs attr:#range
			)
		)
		
		--//////////////////////////////////////////////////
		-- Percentage
		--//////////////////////////////////////////////////
		on spnPercentage changed val do
		(
			updateAttrs()
			updateAttrs attr:#percentage
		)
		
		--//////////////////////////////////////////////////
		-- Starthour
		--//////////////////////////////////////////////////
		on spnStartHour changed val do
		(
			updateAttrs attr:#starthour
		)
		
		--//////////////////////////////////////////////////
		-- EndHour
		--//////////////////////////////////////////////////
		on spnStartEnd changed val do
		(
			updateAttrs attr:#endhour
		)
		
		--//////////////////////////////////////////////////
		-- Open
		--//////////////////////////////////////////////////
		on params open do
		(
			pushAttrUpdate()
			
			--format "loading? %\n" loading
			theNode = (refs.dependents this)[1]
			
			if (theNode != undefined) and (not loading) then
			(
				--Sync the name defs if we need to
				if gGtaTimeCycleSphere_NameDefs == undefined then getSphericalTCNameDefs()
				if ddlNameDefs.items.count == 0 then
				(
					ddlNameDefs.items = gGtaTimeCycleSphere_NameDefs
				)
				
				local defLookup = findItem gGtaTimeCycleSphere_NameDefs theNode.tcName
				if defLookup != 0 then
				(
					ddlNameDefs.selection = defLookup
					edtNameDefDesc.text = gGtaTimeCycleSphere_NameDescriptions[defLookup]
				)
				
				setTransformLockFlags theNode #{4..9}
				
				--theNode.tcName = getAttr theNode gtaAttr_Name
			)
		)
	)
	
	--//////////////////////////////////////////////////
	-- Draw mesh
	--//////////////////////////////////////////////////
	on getDisplayMesh do
	(
		if doUpdate then
		(
			updateAttrs()
			doUpdate = false
		)
		
		if theNode == undefined then theNode = (refs.dependents this)[1]
		local vpScale = 1.0
		
		gw.setTransform theNode.transform
		
		--Box
		meshObj = manip.makeBox [0, 0, (0.5 * this.height)] this.length this.width this.height 1 1 1
		
		--Range(outer)
		rangeBox = manip.makeBox [0, 0, (0.5 * this.height)] \
						(this.length + (2.0 * this.range)) \
						(this.width + (2.0 * this.range)) \
						(this.height + (3.0 * this.range)) \
						1 1 1
							
		rangeVerts = for v=1 to 8 collect (getvert rangeBox v)
		
							
		gw.setColor #line rangeColour
		gw.polyline #(rangeVerts[1], rangeVerts[2], rangeVerts[4], rangeVerts[3]) true	--bottom face
		gw.polyline #(rangeVerts[5], rangeVerts[6], rangeVerts[8], rangeVerts[7]) true	--top face
		--sides
		gw.polyline #(rangeVerts[1], rangeVerts[5]) false
		gw.polyline #(rangeVerts[2], rangeVerts[6]) false
		gw.polyline #(rangeVerts[4], rangeVerts[8]) false
		gw.polyline #(rangeVerts[3], rangeVerts[7]) false
		
							
		--viewport scale
		if viewport.getType() != undefined then
		(
			if viewport.IsPerspView() then
			(
				if theNode != undefined then
				(
					local eyePos = (Inverse(getViewTm()))[4]
					vpScale = (length (theNode.pos - eyePos))/30
					vpScale *= viewport.getFOV()/50
				)
			)
			else
				vpScale = viewport.getFOV()/3
		)
		
		gw.setTransform(Matrix3 1)
		
		local textPos = gw.hTransPoint theNode.pos
		local textPos2 = gw.hTransPoint (theNode.pos + [0, 0, (vpScale * 0.25)])
		
		gw.hText textPos ((this.percentage as String) + "%") color:green
		gw.hText textPos2 (this.tcName) color:yellow
		
		gw.updateScreen()

		meshObj

	)
	
	on clone original do
	(
		this.tcName = original.tcName
		this.range = original.range
		this.percentage = original.percentage
		this.startHour = original.startHour
		this.endHour = original.endHour
		
		doUpdate = true
	)
	
	on update do
	(
		pushAttrUpdate()
	)
)    
     
        