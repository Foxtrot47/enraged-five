filein "pipeline/util/scene.ms"
filein "pipeline/ui/RsTimeTools.ms"

global useNativeGwLib = true

-----------------------------------------------------------
-- types
-----------------------------------------------------------

struct sMeshCacheEntry
(
	points = #(),
	closed = false
)

struct RsRageLightCullPlaneHelper
(
	manipDummy = undefined,
	owningLight = undefined,
	dirty = false,
	meshCache = undefined,

	fn SetManipPos = 
	(
		if undefined==manipDummy then
			return true
		local planeDef = owningLight.lightCullPlane
		local dir = -[planeDef.x,planeDef.y,planeDef.z]
		local offset = dir * planeDef.w
		local worldPos = (offset * owningLight.transform)
		manipDummy.pos = worldPos
		dirty = true
	),
	
	fn show = 
	(
		manipDummy = point()
		SetManipPos()
		select manipDummy
		manipDummy.parent = owningLight
		RsSceneLink.SetParent LinkType_CustomHelperLink manipDummy owningLight
		dirty = true
		when transform manipDummy changes id:#idRageLightCullPlane manipObj do
		(
			local theLight = RsSceneLink.GetParent LinkType_CustomHelperLink manipObj
			if undefined==theLight then
				return false
			local diff = in coordsys (inverse theLight.rotation as matrix3) (theLight.pos - manipObj.pos)
			theLight.SetCullPlaneVec diff
			if undefined!=theLight.cullPLaneHandle then
				theLight.cullPLaneHandle.dirty = true
		)
		when manipDummy deleted id:#idRageLightCullPlane manipObj do
		(
			local theLight = RsSceneLink.GetParent LinkType_CustomHelperLink manipObj
			if undefined!=theLight and undefined!=theLight.cullPLaneHandle then
				theLight.cullPLaneHandle.manipDummy = undefined
			deleteAllChangeHandlers id:#idRageLightCullPlane
			gc light:true
		)
	),
	
	fn hideMe = 
	(
		if isValidNode manipDummy then
			delete manipDummy
		manipDummy = undefined
		deleteAllChangeHandlers id:#idRageLightCullPlane
		gc light:true
	),
	
	fn draw = 
	(
		if undefined==owningLight or manipDummy== undefined then
			return true
		
		if undefined==meshCache then
			meshCache = RsGWPrimMgr.getPrim()
		
		local primObj = meshCache.IRsGWPrim
		if dirty and manipDummy != undefined then
		(
			local planeDef = owningLight.lightCullPlane
			local dir = -[planeDef.x,planeDef.y,planeDef.z]
			local offset = dir * planeDef.w
			local worldPos = (offset * owningLight.transform)
			
			primObj.Clear()
			primObj.setresolution 30
			
--			local offset = manipDummy.pos - owningLight.pos
			local verts = #(offset, [0,0,0])
			primObj.Addline verts

			local trans = matrixFromNormal dir
			trans.translation = offset
			primObj.AddCircle (2*planeDef.w) localTrans:trans
		)
		
		primObj.setOrigin owningLight.transform
		primObj.setColour green
		primObj.draw false
	)
)
-----------------------------------------------------------
-- globals
-----------------------------------------------------------

global lightEditorInstance
global isConversionRunning = false
global horizontallySharedControlNames = for i=1 to 24 by 4 collect (i as string)
global gCullPlaneHandle = undefined

global tempAttrStore = dotnetobject "RSG.MaxUtils.MaxDictionary"

global gSharedLightTypes = #(
	#omni,
	#freeSpot,
	#targetSpot,
	#freeDirect,
	#targetDirect
	)
global gRageLightTypes = #(
	#omni,
	#freeSpot,
	#targetSpot,
	#freeDirect,
	#targetDirect,
	#capsule
	)
global gMaxToRageTypeMap = 
	#(
		#(#Free_Point, 1)
	)
global gRageToMaxTypeMap = 
	#(
		#(6, #omni)
	)
fn getRageFroMaxLight fromLightType = 
(
	for l in gMaxToRageTypeMap where l[1]==fromLightType do return l[2]
	return findItem gSharedLightTypes fromLightType
)
fn getMaxFromRageLight fromLightType = 
(
	for l in gRageToMaxTypeMap where l[1]==fromLightType do return l[2]
	return gSharedLightTypes[fromLightType]
)
	
global standardPropCopyMap =
	#(
		#("lightEnabled", "enabled"),
		#("lightColour", "rgb"),
--		#("lightType", "type"),
		#("lightIntensity", "multiplier"),
--		#("lightAttenStart", "nearAttenStart"),
		#("lightAttenEnd", "farAttenEnd"),
		#("lightShadowMap", "shadowProjectorMap")
	)
global photoPropCopyMap =
	#(
		#("lightColour", "rgb"),
--		#("lightType", "type"),
		#("lightIntensity", "multiplier"),
--		#("lightAttenStart", "nearAttenStart"),
		#("lightAttenEnd", "endFarAttenuation")
	)
	
-- Falloff and Hotspot are used, but do not exist in the #omni light type structure.
global nonOmniPropCopyMap =
	#(
		#("lightFalloff", "falloff"),
		#("lightHotspot", "hotspot")
	)

fn LightEditorWriteAttribute attribute val =
(
	if lightEditorInstance != undefined and lightEditorInstance.mainForm != undefined then
	(
		lightEditorInstance.WriteAttribute attribute val
	)
)

fn NotifyRageLightCreated originalNode newNode = 
(
	if lightEditorInstance != undefined and lightEditorInstance.mainForm != undefined then
	(
		lightEditorInstance.NotifyRageLightCreated originalNode newNode 
	)
)

fn NotifyRageLightCreationCompleted =
(
	if lightEditorInstance != undefined and lightEditorInstance.mainForm != undefined then
	(
		lightEditorInstance.NotifyRageLightCreationComplete() 
	)
)

fn LightEditorConnectedStatus = 
(
	if lightEditorInstance != undefined and lightEditorInstance.mainForm != undefined then
	(
		return lightEditorInstance.isConnected 
	)
	
	return false
)

-- Ragelight not yet defined. String comparison sonlves it.
fn ShowMeshOnLightObject obj = 
(
	if 	("RageLightInstance" == classof obj as string and obj.delegate.lightAttenShow) or
		("RageLight" == classof obj as string and obj.lightAttenShow) then
		return true
	return false
)

-- Is the light of the selected handle the handle's owner.
fn IsCullPlaneHandleActive obj = 
(
	local lightObj = case (classof obj as string) of
	(
		"RageLightInstance":obj.delegate
		"RageLight":obj
		default:undefined
	)
	if undefined==lightObj then
		return false
	
	if 	isValidNode lightObj.cullPLaneHandle.manipDummy and
		lightObj.cullPLaneHandle.manipDummy.isSelected then
	(
		return true
	)
	
	return false
)
-----------------------------------------------------------
-- class definition
-----------------------------------------------------------
	
plugin Light RageLight
extends:Omnilight
name:"RAGE_Light"
classID:#(0x4ff87de6, 0x1b693928)
category:"RAGE Lights"
replaceUI:true
(
	local meshObj = undefined
	local meshCache = undefined
	local bitmapX =130
	local bitmapY = 40
	local theCanvasBitmap = bitmap bitmapX bitmapY color:gray--(colorMan.getColor #background)
	local meshObjCache = undefined
	local needsCloneFrom = undefined
	local otherRollouts = #()
	local cullPLaneHandle = (RsRageLightCullPlaneHelper())

	parameters RageLightNodeParams
	(
		lightNodeRef type:#integer default:-1
	)

	fn GetNode all:false onlyActive:true isNot: = 
	(
		local refsArray = (refs.dependents this)
		local depLights = for item in refsArray where 
		(
			(isvalidnode item) and (isKindof item Light) and 
			(not onlyActive or item.isSelected or ShowMeshOnLightObject item or IsCullPlaneHandleActive item) and
			(item!=isNot)
		) collect item

		local theNode = case of 
		(
			all:
			(
				if (isKindOf depLights Array) then depLights else #(depLights)
			)
			default: 
			(
				if (isKindOf depLights Array) then depLights[1] else depLights
			)
		)
		return theNode
	)

	fn setLightMeshColour theNode: = 
	(
		if (theNode == unsupplied) do 
		(
			theNode = GetNode onlyActive:false
		)

		if undefined!=theNode then
		(
			if not this.delegate.enabled do
			(
				theNode.wirecolor=black
				lightMeshColour=black
				return true
			)
			
			local setColour = case gRageLightTypes[this.lightType] of
			(
				#omni: yellow
				#freeSpot: blue
				#targetSpot: green
				#freeDirect: red
				#targetDirect: Orange
			)
			
			if (setColour != undefined) and (theNode.wirecolor != setColour) do 
			(
				theNode.wirecolor = setColour
			)
		)
	)

	-- Notify RageLightInstance objects of changes to this delegate:
	fn notifyNode param val =
	(
		local theNode = GetNode()
		-- checking for validity fails after max new start for some reason... =(
		local className = (classof theNode) as string
		if className == "RageLightInstance" do 
		(
			theNode.handleAttributeChange param
		)
	)
	
	parameters RageLightParams rollout:RageLightMainRollout
	(
		lightEnabled type:#boolean default:true ui:enabledCheckbox
		lightColour type:#color default:(color 255 255 255) ui:colourSwatch
		lightType type:#integer default:1 ui:cmbType
		lightIntensity type:#float default:1.0 ui:spnIntensity
		lightAttenEnd type:#float default:50.0 ui:spnAttenEnd
		lightWidthAttenEnd type:#float default:50.0 ui:spnWidthFalloff
		lightHotspot type:#float default:20.0 ui:spnHotspot
		lightFalloff type:#float default:50.0 ui:spnFallsize
		lightMeshResolution type:#integer default:10 ui:spnRes
		lightMeshSize type:#float default:0.5 ui:spnSize
		lightMeshColour type:#color default:[0,70,120] ui:pickCol
		lightIsSquare type:#boolean default:false ui:chckSquare
		lightShadowMap type:#texturemap ui:buttShadowMap
		lightExpFalloff type:#float default:8.0 ui:spnExp
		lightAttenShow type:#boolean default:false ui:chkAttenShow
		lightMeshColourEnabled type:#boolean default:false ui:chkMeshColour
		lightUseCullPlane type:#boolean default:false ui:chckUseCullPlane
		lightCullPlane type:#point4 default:(point4 0 0 1 0)
		lightUseOuterColour type:#boolean default:false ui:chckUseOuterCol
		lightOuterColour type:#color default:(color 255 255 255) ui:pickOuterCol
		lightOuterColourInten type:#float default:1.0 ui:spnOuterColInten
		lightOuterColourExp type:#float default:1.0 ui:spnOuterColExp
		
		on lightEnabled set val do
		(
			this.delegate.enabled = val
			setLightMeshColour()
			notifyNode "lightEnabled" val
		)
		
		on lightUseCullPlane set val do
		(
			notifyNode "lightUseCullPlane" val
		)
		
		on lightType set val do
		(
			setLightMeshColour()
		)
		
		on lightColour set val do
		(
			this.delegate.rgb = val
			if not lightMeshColourEnabled do lightMeshColour = val
			notifyNode "lightColour" val
		)
		on lightIntensity set val do 
		(
			this.delegate.multiplier = val
			notifyNode "lightIntensity" val
		)
-- 		on lightAttenStart set val do 
-- 		(
-- 			this.delegate.nearAttenStart = val
-- 			meshObj = undefined
-- 			
-- 			notifyNode "lightAttenStart" val
-- 			--Unsupported by the RageLightInstance.
-- 		)
		on lightAttenEnd set val do 
		(
			this.delegate.farAttenEnd = val
			this.delegate.useFarAtten = true
			notifyNode "lightAttenEnd" val
			meshObj = undefined
		)
		
		on lightWidthAttenEnd set val do 
		(
			notifyNode "lightWidthAttenEnd" val
			meshObj = undefined
		)
		
		on lightHotspot set val do 
		(
			if this.delegate.type==#omni then
				return false
			this.delegate.hotspot = val
			notifyNode "lightHotspot" val
			if lightFalloff<val then
				lightFalloff = val
			meshObj = undefined
		)
		on lightFalloff set val do 
		(
			if this.delegate.type==#omni then
				return false
			this.delegate.falloff = val
			notifyNode "lightFalloff" val
			if lightHotspot>val then
				lightHotspot = val
			meshObj = undefined
		)
		on lightShadowMap set val do 
		(
			-- Update tooltip if rollout is open:
			this.RageLightMainRollout.setLightMapTooltip texMap:val
			
			if this.delegate.type==#omni then
				return false
			this.delegate.shadowProjectorMap = val
			if undefined!=val then
				this.delegate.useShadowProjectorMap = true
			else
				this.delegate.useShadowProjectorMap = false
			
--			meshObj = undefined
			
-- 			local theNode = GetNode()
-- 			if theNode != undefined then
-- 			(
-- 				local className = classof theNode 
-- 				if className == RageLightInstance then
-- 				(
-- 					theNode.notifyShadowMapChange()
-- 				)
-- 			)
		)
		on lightMeshResolution set val do 
		(
			notifyNode "lightMeshResolution" val
			meshObj = undefined
			completeRedraw()
		)
		on lightMeshSize set val do 
		(
			notifyNode "lightMeshSize" val
			meshObj = undefined
		)
		
		on lightMeshColour set val do
		(
			if lightMeshColourEnabled do
			(
				lightMeshColour = val
				notifyNode "lightMeshColour" val
				meshObj = undefined
			)
		)

		on lightType set val do 
		(
 			this.delegate.type = getMaxFromRageLight lightType
			setLightMeshColour()
			notifyNode "lightType" val
			meshObj = undefined
			if lightType > 3 then
			(
				lightShadowMap = undefined
			)

		)
		
		on lightIsSquare set val do
		(
			if val then
			(
				lightMeshResolution = 4
			)
			else
			(
				lightMeshResolution = 10
			)
			notifyNode "lightIsSquare" val
			meshObj = undefined
		)
		
		on lightShadowMap set Val do 
		(
			local NewVal = ""
			
			if (isKindOf Val BitmapTexture) do 
			(
				NewVal = Val.Filename
			)
			
			-- Pass texturemap-filename to notification-handler (or "" if none)
			notifyNode "lightShadowMap" NewVal
		)
		
		on lightExpFalloff set val do
		(
			notifyNode "lightExpFalloff" val
		)
		
	)

	fn SetCullPlaneVec vec = 
	(
		lightCullPlane.w = length vec
		local normVec = normalize vec
		lightCullPlane.x = normVec.x
		lightCullPlane.y = normVec.y
		lightCullPlane.z = normVec.z
		if undefined!=::RageLightMainRollout then
		(
			::RageLightMainRollout.spnCullx.x = lightCullPlane.x
			::RageLightMainRollout.spnCullx.y = lightCullPlane.y
			::RageLightMainRollout.spnCullx.z = lightCullPlane.z
			::RageLightMainRollout.spnCullx.w = lightCullPlane.w
		)
		
		notifyNode "lightCullPlane" lightCullPlane
	)

	----------------------------------------------------
	-- Copy light-properties from another light (which can be of a different light-class) 
	--	to this one, or to a newly-created one
	----------------------------------------------------
	fn CopyFrom fromLight withCreate:false deleteOriginal:true copyTrans:true = 
	(
		if not isKindOf fromLight light then
		(
			messagebox (fromLight as string+" is not a light to copy from.")
			return false
		)
		
		local fromMaxLight = if isKindOf fromLight rageLight then fromLight.delegate else fromLight
		
		if undefined == (getRageFroMaxLight fromMaxLight.type) then
		(
			print ("object "+fromMaxLight.name+" has not supported light type: "+fromMaxLight.type as string)
			return false
		)

		local theNode
		if withCreate then 
		(
			local theFalloff = if (not isKindOf fromMaxLight Omnilight) then fromMaxLight.falloff else 50.0
			theNode = RageLight lightColour:fromMaxLight.rgb lightFalloff:theFalloff
		)
		else 
		(
			theNode = GetNode()
		)
		
		theNode.lightType = (getRageFroMaxLight fromMaxLight.type)
		
		if (isValidNode theNode) do 
		(
			theNode.delegate.type = fromMaxLight.type
		)
		
		local propCopyMap = standardPropCopyMap
		-- photometric or normal
		if (isProperty fromMaxLight "endFarAttenuation") do
		(
			propCopyMap = photoPropCopyMap
		)
		
		for p in propCopyMap do
		(
			local theProp = getproperty fromMaxLight p[2]

			setProperty fromMaxLight p[2] theProp
			
			if (isValidNode theNode) do
			(
				setProperty theNode p[1] theProp
			)
		)		
		
		if (fromMaxLight.type != #omni) do 
		(
			for p in nonOmniPropCopyMap do
			(
				local theProp = getproperty fromMaxLight p[2]

				setProperty fromMaxLight p[2] theProp
				
				if (isValidNode theNode) do 
				(
					setProperty theNode p[1] theProp
				)
			)
		)

		--local oldNode = GetNode()
		--::NotifyRageLightCreated oldNode theNode
		if (isValidNode fromLight) do 
		(
			if (isValidNode theNode) do
			(
				copyAttrs fromLight
				pasteattrs theNode

				if copyTrans do
				(
					theNode.transform = fromLight.transform
					theNode.parent = fromLight.parent
					for c in fromLight.children do c.parent = theNode
					
					-- Transfer direction-target position, if applicable:
					if (isProperty fromMaxLight #target) and (isValidNode fromMaxLight.target) do 
					(
						theNode.target.transform = fromMaxLight.target.transform
					)
				)
			)
			
			if deleteOriginal do 
			(
				delete fromLight
			)
		)

		if ( LightEditorConnectedStatus() == false ) then
		(
			-- HACK:  If the Light Editor is active, do not select the newly created light.
			-- Because of the way these scripted plugins are created, change handlers to these
			-- objects will throw MaxScript exceptions.  Users have to step out of the RageLight
			-- "tool" mode and select the newly created light in order for this to work. - KRW
			--select theNode
		)
		--::NotifyRageLightCreationCompleted()		
	)
	
	fn yProj val = 
	(
		val = val *2 - 1
		val = acos (val)
		val =  (90-val)/90
		val = (val+1)/2
	)
	
	fn paintHelperMesh =
	( 
		local theNodes = GetNode all:true
		if undefined!=theNodes then
		(
			for theNode in theNodes do
			(
				if useNativeGwLib and Array==(classof meshCache) then
					meshCache = undefined
				if not useNativeGwLib and Array!=(classof meshCache) then
					meshCache = undefined
				
				if (undefined==meshCache or meshObj == undefined or (Trimesh==(classof meshObj) and isDeleted meshObj)) do 
				(
					if useNativeGwLib then
					(
						meshCache = RsGWPrimMgr.getPrim()
						meshCache.IRsGWPrim.Clear()
						meshCache.IRsGWPrim.setresolution lightMeshResolution
					)
					else
					(
						meshCache = #()
					)
					
					--gw.resetUpdateRect()
					--gw.enlargeUpdateRect #whole
					
					--omni
					if lightType==1 then
					(
						if useNativeGwLib then
						(
							meshCache.IRsGWPrim.addSphere lightAttenEnd
						)
						else
						(
							local verts1 = #()
							local verts2 = #()
							local verts3 = #()
							local step = 360.0/lightMeshResolution
							for k=0 to lightMeshResolution do
							(
								local ak = (cos (step*k)) * lightAttenEnd
								local gk = (sin (step*k)) * lightAttenEnd
								append verts1 (point3 ak gk 0)
								append verts2 (point3 ak 0 gk)
								append verts3 (point3 0 ak gk)
							)
							append meshCache (sMeshCacheEntry points:verts1 closed:false)
							append meshCache (sMeshCacheEntry points:verts2 closed:false)
							append meshCache (sMeshCacheEntry points:verts3 closed:false)
						)
					)
					--spots
					else if lightType<4 then
					(
						if useNativeGwLib then
						(
							meshCache.IRsGWPrim.AddCone lightAttenEnd lightFalloff true
							meshCache.IRsGWPrim.AddCone lightAttenEnd lightHotspot false
							
							if undefined != theNode.lightShadowMap then
							(
								local hl = -(lightFalloff * (lightAttenEnd/25) /4)
								meshCache.IRsGWPrim.AddLine #(
									[hl,hl,-lightAttenEnd],
									[hl,-hl,-lightAttenEnd],
									[-hl,-hl,-lightAttenEnd],
									[-hl,hl,-lightAttenEnd],
									[hl,hl,-lightAttenEnd])
								-- arrow
								local arrowLineLength = (hl/2)
								local arrowLineHalfLength = (arrowLineLength/2)
								meshCache.IRsGWPrim.AddLine #(
									[hl,										0,									-lightAttenEnd],
									[hl+arrowLineLength,			0,									-lightAttenEnd])
								meshCache.IRsGWPrim.AddLine #(
									[hl+arrowLineHalfLength,	-arrowLineHalfLength,	-lightAttenEnd],
									[hl+arrowLineLength,			0,									-lightAttenEnd])
								meshCache.IRsGWPrim.AddLine #(
									[hl+arrowLineHalfLength,	arrowLineHalfLength, 	-lightAttenEnd],
									[hl+arrowLineLength,			0,									-lightAttenEnd])
							)
						)
						else
						(
							local akSpot = (cos (lightHotspot/2)) * lightAttenEnd
							local gkSpot = (sin (lightHotspot/2)) * lightAttenEnd
							local akSpotOuter = (cos (lightFalloff/2)) * lightAttenEnd
							local gkSpotOuter = (sin (lightFalloff/2)) * lightAttenEnd
							local innerOuterFactor = (gkSpotOuter/gkSpot)
							local vertsBottom = #()
							local vertsBottomOuter = #()
							local vertsHemiSphere = #()
							local step = 360.0/lightMeshResolution
							local arcResolution = lightMeshResolution/3
							local arcStep = (lightFalloff/2)/arcResolution
							for k=0 to lightMeshResolution do
							(
								local currStep = (step*k) 
								if lightIsSquare then
								(
									currStep = currStep + 45
								)
								local ak = (cos currStep) * gkSpot
								local gk = (sin currStep) * gkSpot
								local currPoint = (point3 ak gk 0)
								append vertsBottom (point3 currPoint.x currPoint.y -akSpot)
								local outerPoint = currPoint * innerOuterFactor
								local outerDir = normalize outerPoint
			-- normalize before applying z component!
								outerPoint[3] = -akSpotOuter
								append vertsBottomOuter outerPoint
								local vertsArc = #()
								for k=0 to arcResolution do
								(
									local akArc = (cos (arcStep*k)) * lightAttenEnd
									local gkArc = (sin (arcStep*k)) * lightAttenEnd
									local currArcPoint = outerDir * gkArc
									currArcPoint[3] = -akArc
									append vertsArc currArcPoint
								)
								append vertsHemiSphere vertsArc
							)
							append meshCache (sMeshCacheEntry points:vertsBottom closed:true)
							local topVert = [0,0,0]
							for i=1 to vertsBottom.count do 
								append meshCache (sMeshCacheEntry points:#(topVert,vertsBottom[i]) closed:true)
							append meshCache (sMeshCacheEntry points:vertsBottomOuter closed:true)
							for i=1 to vertsBottomOuter.count do 
								append meshCache (sMeshCacheEntry points:#(topVert,vertsBottomOuter[i]) closed:true)
							for i=1 to vertsHemiSphere.count do
								append meshCache (sMeshCacheEntry points:vertsHemiSphere[i] closed:false)
						)
					)
					-- directionals
					else if lightType<6 then
					(
						if useNativeGwLib then
						(
							meshCache.IRsGWPrim.AddCylinder lightAttenEnd lightHotspot
						)
						else
						(
							local akSpot = (cos (lightHotspot/2)) * lightAttenEnd
							local gkSpot = (sin (lightHotspot/2)) * lightAttenEnd
							local gkSpotOuter = (sin (lightFalloff/2)) * lightAttenEnd
							local innerOuterFactor = (gkSpotOuter/gkSpot)
							local vertsBottom = #()
							local vertsTop = #()
							local vertsBottomOuter = #()
							local vertsTopOuter = #()
							local step = 360.0/lightMeshResolution
							for k=0 to lightMeshResolution do
							(
								local ak = (cos (step*k)) * gkSpot
								local gk = (sin (step*k)) * gkSpot
								local currPoint = (point2 ak gk)
								append vertsBottom (point3 currPoint.x currPoint.y 0)
								append vertsTop (point3 currPoint.x currPoint.y -akSpot)
								local outerPoint = currPoint * innerOuterFactor
								append vertsBottomOuter (point3 outerPoint.x outerPoint.y 0)
								append vertsTopOuter (point3 outerPoint.x outerPoint.y -akSpot)
							)
							append meshCache (sMeshCacheEntry points:vertsBottom closed:true)
							append meshCache (sMeshCacheEntry points:vertsTop closed:true)
							for i=1 to vertsBottom.count do gw.Polyline #(vertsTop[i],vertsBottom[i]) true
							append meshCache (sMeshCacheEntry points:vertsBottomOuter closed:true)
							append meshCache (sMeshCacheEntry points:vertsTopOuter closed:true)
							for i=1 to vertsBottomOuter.count do gw.Polyline #(vertsBottomOuter[i],vertsTopOuter[i]) true
						)
					)
					-- capsule
					else if lightType==6 then
					(
						if useNativeGwLib then
						(
							local trans = Matrix3(1)
							trans.translation = [0,0,lightWidthAttenEnd/2]
							
							meshCache.IRsGWPrim.AddCapsule lightWidthAttenEnd lightAttenEnd true localTrans:trans
						)
					)
					
					gw.updateScreen()
				)

				if useNativeGwLib then
				(
					meshCache.IRsGWPrim.setOrigin theNode.transform
					if not lightMeshColourEnabled then
						meshCache.IRsGWPrim.setColour lightColour
					else
						meshCache.IRsGWPrim.setColour lightMeshColour
					meshCache.IRsGWPrim.draw false
				)
				else
				(
					gw.setTransform theNode.transform
					if not lightMeshColourEnabled then
						gw.setColor #line lightColour
					else
						gw.setColor #line lightMeshColour

					if undefined!=meshCache then
						for m in meshCache do
							gw.polyline m.points m.closed
				)
			)
		)
	)
	
	on update do 
	(
		meshObj = undefined
	)
	
	on getDisplayMesh do
	(
		local theNode = GetNode()
		
		if undefined!=theNode then
		(
			if undefined!=theNode.target then
			(
				gw.setColor #line green
				gw.setTransform (matrix3 1)
				gw.polyline #(theNode.pos, theNode.target.pos) false
			)
			
			if (ShowMeshOnLightObject theNode or theNode.isSelected) then
				paintHelperMesh()
		)

		if undefined!=cullPLaneHandle then
			cullPLaneHandle.draw()
		
		if (meshObj == undefined or (Trimesh==(classof meshObj) and isDeleted meshObj)) do 
		(
			setLightMeshColour()
			
			local theMesh = undefined
			--screen scale factor to scale the node mesh to keep it the same size regardless of distance from the viewport camera
			--local screenScaleFactor = gw.nonScalingObjectSize() * (gw.getVPWorldWidth theNode.position) / 360.0
			--local screenScaleMesh = lightMeshSize * screenScaleFactor
			
			case of 
			(
				--omni
				(lightType == 1):
				(
					local theCone =  createinstance sphere segs:4 radius:lightMeshSize mapCoords:false
					theMesh = theCone.mesh
				)
				--freedirect
				(lightType < 4):
				(
					local buggerVal = if lightHotspot>lightFalloff then lightHotspot else lightFalloff
					local anKathete = (cos (buggerVal/2)) * lightMeshSize
					local gegenKathete = (sin (buggerVal/2)) * lightMeshSize
					local daCone = createinstance cone radius1:0.0 radius2:gegenKathete height:anKathete heightsegs:1 sides:lightMeshResolution mapCoords:false
					theMesh = daCone.mesh
					local m = rotateZMatrix 45					
					if lightIsSquare then
					(
						for v=1 to getnumverts theMesh do
						(
							local vert = (getVert theMesh v)
							setvert theMesh v (vert * m)
						)
					)
				)
				(lightType < 6):
				(
					local halfSize = lightMeshSize/2
					local quarterSize = halfSize/2
					local res = 1 --lightMeshResolution
					local thebox = createInstance box length:quarterSize width:quarterSize height:halfSize widthsegs:res heightsegs:res lengthsegs:res mapCoords:false
					local arrowHead = createInstance pyramid width:halfSize depth:halfSize height:halfSize widthsegs:res heightsegs:res depthsegs:res mapCoords:false
					theMesh = arrowHead.mesh
					for vi=1 to theMesh.verts.count do
						setvert theMesh vi ((getvert theMesh vi)+[0,0,halfSize])
					update theMesh
					meshop.attach theMesh thebox.mesh
					update theMesh
				)
				-- capsule
				Default:
				(
					local rel = lightWidthAttenEnd/lightAttenEnd
					local cylheight = lightMeshSize * rel
					local halfSize = cylheight/2+lightMeshSize
					local res = lightMeshResolution
					local theCaps = createInstance capsule radius:lightMeshSize height:cylheight sides:res heightsegs:1 mapCoords:false
					theMesh = theCaps.mesh
					local t = transmatrix [0,0,-halfSize]
					for vi=1 to theMesh.verts.count do
					(
						setvert theMesh vi ((getVert theMesh vi) * t)
					)
				)
			)
			
			-- rotate to point down
			local rotMat = rotateXMatrix 180
			for vi=1 to theMesh.verts.count do
			(
				local v = getvert theMesh vi
				v = v * rotMat
				setvert theMesh vi v
			)
			meshObj = copy theMesh
			meshObjCache = copy theMesh
		)
		local vpScale = 1.0
		theNode = GetNode onlyActive:false
		if viewport.IsPerspView() then
		(
			if undefined!=theNode then
			(
				local eyePos = (Inverse(getViewTm()))[4]
				vpScale = (length (theNode.pos - eyePos))/23
				vpScale *= viewport.getFOV()/50
			)
--			vpScale = viewport.getFocalDistance()/30
		)
		else
			vpScale = viewport.getFOV()/3
		local scaledMesh = copy meshObjCache
		scale scaledMesh [vpScale,vpScale,vpScale]

		return scaledMesh
	)

	-----------------------------------------------------------
	-- bitmap drawing for exponent graph
	-----------------------------------------------------------
	fn __powapprox a b = 
	(
		return a / ((1.0 - b) * a + b)
	)

	fn paintBrush pos =
	(
		pos = pos * [bitmapX, bitmapY]
		--print pos
		setPixels theCanvasBitmap pos #(((colorMan.getColor #text)*255))
	)

	fn drawCurve val =
	(
		theCanvasBitmap = bitmap bitmapX bitmapY color:((colorMan.getColor #background)*255)
		local lastPos = [1,1]
		local width = bitmapX-2
		for xi=2 to width do
		(
			x = ((xi as float)/width)
			local y = __powapprox x val
			local swappedX = 1.0 - x
			local swappedY = 1.0 - y
			paintBrush [swappedX,swappedY]
		)
		return theCanvasBitmap 
	)

	-----------------------------------------------------------
	-- UI
	-----------------------------------------------------------
	rollout RageLightMainRollout "Main Properties"
	(
		local spnCullWidth = 35
		hyperlink helpLink "help?" address:"https://devstar.rockstargames.com/wiki/index.php/Rage_Lights#Max_Interface" align:#right color:(color 0 0 255) visitedColor:(color 0 0 255)
		checkbox enabledCheckbox "Enabled" 
		dropdownlist cmbType "Type:" items:gRageLightTypes
		colorpicker colourSwatch "Light Colour:  " align:#right
		spinner spnIntensity "Intensity:" type:#float range:[0.0,256.0,1.0]
		group "Attenuation"
		(
			checkbox chkAttenShow "Show" checked:true across:2 align:#left
			spinner spnAttenEnd "Range:" type:#float range:[0,1000,lightAttenEnd] offset:[-15,1] width:95 align:#left
			spinner spnExp "Exp. Falloff" type:#float range:[0,256,lightHotspot] scale:0.01
			bitmap bmpExpCurve "" width:bitmapX height:bitmapY bitmap:theCanvasBitmap
		)
		group "Spot Light Params"
		(
			spinner spnHotspot "Hotspot:" type:#float range:[0,1000,lightHotspot]
			spinner spnFallsize "Radial Falloff:" type:#float range:[0,1000,lightFalloff]
			mapbutton buttShadowMap "<<Shadow map>>" width:120
			checkbox chckSquare "Square Projection" checked:false
		)
		group "Capsule Light Params"
		(
			spinner spnWidthFalloff "Width Falloff:" type:#float range:[0,1000,lightWidthAttenEnd]
		)
		group "Culling Plane"
		(
			checkbox chckUseCullPlane "Use Culling Plane"
			checkbutton chckManCullPlane "Edit" width:120 height:25 checked:(cullPLaneHandle.manipDummy!=undefined)
			spinner spnCullx "" width:spnCullWidth type:#float range:[-1,1,lightCullPlane.x] across:4
			spinner spnCully "" width:spnCullWidth type:#float range:[-1,1,lightCullPlane.y]
			spinner spnCullz "" width:spnCullWidth type:#float range:[-1,1,lightCullPlane.z]
			spinner spnCullw "" width:spnCullWidth type:#float range:[-100,100,lightCullPlane.w]
		)
		group "Volume Params"
		(
			checkbox chckUseOuterCol "Outer Colour:" across:2
			colorpicker pickOuterCol "" align:#right offset:[0,-4]
			spinner spnOuterColInten "Intensity" range:[0.0,1.0,1.0]
			spinner spnOuterColExp "Exponent" range:[0.0,5000.0,1.0]
		)
		group "Viewport Mesh"
		(
			spinner spnRes "Resolution" type:#integer range:[5,1000,lightMeshResolution] enabled:(not lightIsSquare)
			spinner spnSize "Mesh Size" type:#float range:[0.1,100,lightMeshSize]
			colorpicker pickCol "Mesh Colour" width:100 height:20 across:2
			checkbox chkMeshColour "" checked:false align:#right
		)
		
		-- Set LightShadow map-button's tooltip to its map's filename:
		fn setLightMapTooltip texMap:lightShadowMap = 
		(
			if RageLightMainRollout.open do 
			(
				buttShadowMap.tooltip = ""
				
				if (isKindOf texMap BitmapTexture and undefined!=texMap.filename) do 
				(
					buttShadowMap.tooltip = texMap.filename
				)
			)
		)
	
		fn EnableTypeParameters = 
		(			
			if this.lightType > 3 then
			(
				--Omni lights do not have a hotspot or a falloff variable.
				spnHotspot.enabled = false
				spnFallsize.enabled = false
				chckSquare.enabled = false
				buttShadowMap.enabled = false
			)
			else
			(
				spnHotspot.enabled = true
				spnFallsize.enabled = true
				chckSquare.enabled = true
				buttShadowMap.enabled = true
			)
-- 			if this.lightType != (findItem gRageLightTypes #capsule) then
-- 				spnWidthFalloff.enabled = false
-- 			else
--				spnWidthFalloff.enabled = true
			
			if classof (GetNode())==RageLightInstance then
			(
				cmbType.enabled = false
			)
		)
		
		fn removeRolls = 
		(
			for roll in otherRollouts do 
			(
				removeRollout roll
			)
		)
		
		fn addRollouts = 
		(
			removeRolls()
			
			if not (isCreatingObject() or ($ == undefined)) do 
			(
				otherRollouts = RsCreateAttrRoll $ propClassName:"Gta LightPhoto"
				
				addRollout RsTimeToolsRoll
				for roll in otherRollouts do 
				(
					addRollout roll
				)
				append otherRollouts RsTimeToolsRoll
			)
		)
		
		on spnCullx changed val do
		(
			lightCullPlane.x = val
			cullPLaneHandle.SetManipPos()
			notifyNode "lightCullPlane" lightCullPlane
			redrawViews()
		)
		on spnCully changed val do
		(
			lightCullPlane.y = val
			cullPLaneHandle.SetManipPos()
			notifyNode "lightCullPlane" lightCullPlane
			redrawViews()
		)
		on spnCullz changed val do
		(
			lightCullPlane.z = val
			cullPLaneHandle.SetManipPos()
			notifyNode "lightCullPlane" lightCullPlane
			redrawViews()
		)
		on spnCullw changed val do
		(
			lightCullPlane.w = val
			cullPLaneHandle.SetManipPos()
			notifyNode "lightCullPlane" lightCullPlane
			redrawViews()
		)
		
		on RageLightMainRollout open do
		(
			local thenode = GetNode()
			if undefined != theNode then
			(
				-- check buffer from creation 
				local className = getattrclass thenode
				for k=1 to getnumattr className where tempAttrStore.Containskey k do
					setAttr thenode k (tempAttrStore.item k)
				tempAttrStore.Clear()

				if undefined!=needsCloneFrom then
				(
					local cloneFromNode = needsCloneFrom.GetNode onlyActive:false isNot:thenode
					CopyFrom cloneFromNode deleteOriginal:false copyTrans:false
					needsCloneFrom = undefined
				)
				
				cullPLaneHandle.owningLight = thenode
			)			
			spnCullx.value = lightCullPlane.x
			spnCully.value = lightCullPlane.y
			spnCullz.value = lightCullPlane.z
			spnCullw.value = lightCullPlane.w
		
			addRollouts()
			
			EnableTypeParameters()
			bmpExpCurve.bitmap = drawCurve spnExp.value
			
			setLightMapTooltip()
		)
		
		on RageLightMainRollout close do
		(
			removeRolls()
--			chckManCullPlane.state = off
		)
		
		on chckManCullPlane changed state do
		(
			chckUseCullPlane.state = on
			if state then
				cullPLaneHandle.show()
			else
				cullPLaneHandle.hideMe()
		)
		
		on chckUseCullPlane changed isChecked do
		(
			if not isChecked do
			(
				chckManCullPlane.state = off
				cullPLaneHandle.hideMe()
			)
		)
		
		on pickOuterCol changed val do
		(
			chckUseOuterCol.state = on
		)
		
		on cmbType selected val do
		(
			lightType = val
			EnableTypeParameters()
		)
		
		on chckSquare changed val do
		(
			if val then
				spnRes.enabled = false
			else
				spnRes.enabled = true
		)
		on buttShadowMap rightclick do
		(
			popUpMenu ::RsTexClearMenu
		)
		
		on spnExp changed val do
		(
			bmpExpCurve.bitmap = drawCurve val
		)
		
		on colourSwatch changed arg do
		(
			col = arg
			if arg.v < 48 then col.v = 48
			
			colourSwatch.color = col
		)
		
		on pickCol changed arg do
		(
			col = arg
			if arg.v < 48 then col.v = 48
			
			pickCol.color = col
		)
	)

	fn refreshUI = 
	(
		if RageLightMainRollout.open do 
		(
			for r in RageLightMainRollout.otherRollouts do removeRollout r
			addRollouts()
			RageLightMainRollout.EnableTypeParameters()
			
			setLightMapTooltip()
		)
		return true
	)
	
	on create do
	(
		local c = selection as array
		
		local lightSelection = for s in c where iskindof s Light and (not isKindOf s RageLight) and (not isProperty s #isSlave) collect s
		
		if not isConversionRunning and lightSelection.count>0 then
		(
			if querybox "The scene selection is not empty. Do you want the lights in the selection to be converted to RageLights? " then
			(
				isConversionRunning = true
				clearSelection()
				try(
					for s in lightSelection do
					(
						CopyFrom s withCreate:true
					)
				)catch(print(getcurrentException()))
				isConversionRunning = false
			)
		)
	)
	
	on clone original do 
	(
		needsCloneFrom = original
	)
	
	on attachedToNode newNode do
	(
		local original = GetNode onlyActive:false isNot:newNode
		needsCloneFrom = original
	)
)

fn CheckRageLightTexMapSet = 
(
	if selection.count != 1 then
		return false
	if not (iskindof selection[1] RageLight) then
		return false
	local rl = selection[1]
	if rl.lightShadowMap == undefined then
		return false
	
	return true
)

rcmenu RsTexClearMenu 
(
	menuItem clearItem "Clear" enabled:(CheckRageLightTexMapSet())
	menuItem editItem "Put in active material editor slot" enabled:(CheckRageLightTexMapSet())

	on clearItem picked do 
	(
		if selection.count != 1 then
			return false
		if not (iskindof selection[1] RageLight) then
			return false

		local rl = selection[1]
		rl.lightShadowMap = undefined 
	)
	on editItem picked do
	(
		local rl = selection[1]
		meditmaterials[activeMeditSlot] = rl.lightShadowMap
	)
)




