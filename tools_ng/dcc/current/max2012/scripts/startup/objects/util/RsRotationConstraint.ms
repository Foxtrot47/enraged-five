--
-- File:: startup/RsRotationConstraint.ms
-- Description:: Single axis rotation constraint helper.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 11 November 2009
--
-- References::
--  http://forums.cgsociety.org/showthread.php?f=98&t=822103&highlight=scripted+helper
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/constraints.ms"
filein "rockstar/export/maputil.ms"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

--
-- name: RsRotationConstraint
-- desc: Rotational constraint helper object
--
plugin SimpleManipulator RsRotationConstraint
	name:"RotationConstraint"
	classID:#(0x2ea24371, 0x65810cc5)
	category:"RAGE Physics"
(
	-------------------------------------------------------------------------
	-- Parameter Blocks
	-------------------------------------------------------------------------
	
	--
	-- pblock: paramLimits
	-- desc: Limits parameter block.
	--
	parameters paramLimits rollout:rolloutConstraint
	(
		Axis type:#integer ui:lstAxis default:1
		LimitMin type:#angle ui:spnMin default:0
		LimitMax type:#angle ui:spnMax default:0
		orientation type:#float ui:spnOrient default:0.0

		-- Handler to ensure LimitMin does not exceed LimitMax
		on LimitMin set arg do
		(
			if ( arg > LimitMax ) then
				LimitMin = LimitMax			
		)
		
		-- Handler to ensure LimitMax does not exceed LimitMin
		on LimitMax set arg do
		(
			if ( arg < LimitMin ) then
				LimitMax = LimitMin
		)
	)
	
	--
	-- pblock: paramSize
	-- desc: Helper size parameter block.
	--
	parameters paramSize rollout:rolloutConstraint
	(
		Size type:#worldUnits ui:spnSize default:0.25
	)
	
	fn getParentAnimExtent = 
	(
		local theNode = (refs.dependents this)[1]
		if undefined==theNode or undefined==theNode.parent then
		(
			messagebox "Couldn't resolve node or node's parent"
		)
		local p = theNode.parent
		local range = RsGetAnimRange p
		animButtonState = false
		LimitMin = 0
		LimitMax = 0
		for k=range.start to range.end do
		(
			local currValue 
			at time k
			(
				local c = p.rotation.controller
				if Rotation_List==(classof c) then
					c = c[c.count]
				
				case Axis of
				(
					1:(currValue = c.x_rotation)
					2:(currValue = c.y_rotation)
					3:(currValue = c.z_rotation)
				)
			)
			if currValue>LimitMax then LimitMax = currValue
			if currValue<LimitMin then LimitMin = currValue
		)
	)
	
	-------------------------------------------------------------------------
	-- Rollouts
	-------------------------------------------------------------------------
	
	--
	-- rollout: rolloutConstraint
	-- desc: Rollout for all parameter blocks.
	--
	rollout rolloutConstraint "Parameters"
	(
		dropdownlist lstAxis "Axis:" items:#("X-Axis", "Y-Axis", "Z-Axis")
		spinner spnMin "LimitMin:    x" range:[-360.0,360.0,0.0]
		spinner spnMax "LimitMax:  +" range:[-360.0,360.0,0.0]
		spinner spnSize "Size:            "
		spinner spnOrient "Orientation:" range:[-360.0,360.0,0.0]
		button btngetAnimExtents "Get animated parent's extents"
		local orientButtonDownValOrient = 0.0
		
		on rolloutConstraint open do 
		(
			orientButtonDownValOrient = orientation
		)
		
		on btngetAnimExtents pressed do
		(
			getParentAnimExtent()
		)
		
		on spnOrient buttondown do
		(
			orientButtonDownValOrient = spnOrient.value
		)
		on spnOrient changed val do
		(
			LimitMin = LimitMin - (val - orientButtonDownValOrient)
			LimitMax = LimitMax - (val - orientButtonDownValOrient)
			orientButtonDownValOrient = val
			local theNode = (refs.dependents this)[1]
			if undefined!=theNode then
			(
				case Axis of
				(
					1:(theNode.rotation.controller.x_rotation = val)
					2:(theNode.rotation.controller.y_rotation = val)
					3:(theNode.rotation.controller.z_rotation = val)
				)
			)
		)
	)
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------

	--
	-- event: canManipulate
	-- desc:
	--
	on canManipulate target return (classOf target) == RsRotationConstraint
	
	--
	-- event: updateGizmos
	-- desc: Refresh gizmo representation.
	--
	on updateGizmos do
	(		
		this.clearGizmos()
		local flags = (gizmoActiveViewportOnly)
		local segsize = 9.0
		local segs = ( LimitMax - LimitMin ) / segsize
		
		local axisstart, axisend
		local axisarc = manip.makeGizmoShape()
		local axiscolour, markerColour
		
		case Axis of
		(
			1: -- X-Axis Constraint
			(
				axiscolour = [1,0,0]
				markerColour = [1,0.7,0.7]
				
				axisstart = [0,cos(LimitMin),sin(LimitMin)] * size
				axisend = [0,cos(LimitMax),sin(LimitMax)] * size
				axisarc.addPoint( [0,0,0] )
				axisarc.addPoint( axisstart )
				for seg = 0 to segs do
				(
					local angle_from = LimitMin + (seg*segsize)
					local xp = [0,cos(angle_from),sin(angle_from)] * size
					axisarc.addPoint( xp )
				)
				axisarc.addPoint( axisend )
				axisarc.addPoint( [0,0,0] )
			)
			2: -- Y-Axis Constraint
			(				
				axiscolour = [0,1,0]
				markerColour = [0.7,1,0.7]
				
				axisstart = [cos(LimitMin),0,sin(LimitMin)] * size
				axisend = [cos(LimitMax),0,sin(LimitMax)] * size
				axisarc.addPoint( [0,0,0] )
				axisarc.addPoint( axisstart )
				for seg = 0 to segs do
				(
					local angle_from = LimitMin + (seg*segsize)
					local xp = [cos(angle_from),0,sin(angle_from)] * size
					axisarc.addPoint( xp )
				)
				axisarc.addPoint( axisend )
				axisarc.addPoint( [0,0,0] )
			)			
			3: -- Z-Axis Constraint
			(
				axiscolour = [0,0,1]
				markerColour = [0.7,0.7,1]
				
				axisstart = [cos(LimitMin),sin(LimitMin),0] * size
				axisend = [cos(LimitMax),sin(LimitMax),0] * size
				axisarc.addPoint( [0,0,0] )
				axisarc.addPoint( axisstart )
				for seg = 0 to segs do
				(
					local angle_from = LimitMin + (seg*segsize)
					local xp = [cos(angle_from),sin(angle_from),0] * size
					axisarc.addPoint( xp )
				)
				axisarc.addPoint( axisend )
				axisarc.addPoint( [0,0,0] )
			)				
		)
		
		local orientSize = size
		--if orientation!=0.0 then
		(
			local orientLine = manip.makeGizmoShape()
			orientLine.addPoint( [0,0,0] )
			local xp = 
			case Axis of
			(
				1:(xp = [0,1,0] * orientSize)
				2:(xp = [1,0,0] * orientSize)
				3:(xp = [1,0,0] * orientSize)
			)
			orientLine.addPoint( xp )
			axiscolour = [1,0,0]
			this.addGizmoShape orientLine flags axiscolour axiscolour
		)
		
		axiscolour = [1,1,1]
		this.addGizmoShape axisarc flags axiscolour axiscolour
		this.addGizmoMarker #bigBox axisstart flags axiscolour axiscolour
		this.addGizmoMarker #bigBox axisend flags axiscolour axiscolour
		this.addGizmoMarker #plusSign axisend flags markerColour markerColour
		this.addGizmoMarker #xMarker axisstart flags markerColour markerColour
		
		"RotationConstraint"
	)
	
	on mouseDown m which do
    (
      -- Toggle the value of the "Hide" state
      if (which == 1) then 
		target.hide = not hide 
    )
	
	--
	-- event: create
	-- desc: Event handler called on manipulator creation.
	--
	tool create
	(
        on mousePoint click do
           case click of
           (
              1:
				(
					#stop 
				)
           )
	)
)

-- startup/RsRotationConstraint.ms
