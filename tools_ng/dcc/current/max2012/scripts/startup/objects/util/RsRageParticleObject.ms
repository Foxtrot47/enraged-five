-- ParticleHelper Object
-- written by Gunnar Droege

-- Remove existing callbacks/rollouts, if re-running script:
(
	try (removeRollout gDomainRollout) catch ()
	try (removeRollout RsPartDBRoll) catch ()
	
	deleteAllChangeHandlers id:#RsParticleObjects

	for obj in helpers where (isKindOf obj ::RsRageParticleHelper) and (obj.changeScaleHandler != undefined) do 
	(
		deleteChangeHandler obj.changeScaleHandler
	)
	
	if (::gRsParticleSelectHandler != undefined) do 
	(
		gRsParticleSelectHandler = undefined
		gc light:true
	)
)

filein "pipeline/util/scene.ms"
filein "rockstar/helpers/particle_cat.ms"

global CollParts = #()
global CollCurrPartNames = #()
global triggerStates = #("ambient","collision","shot","break","destruction","anim","rayfire","in water")
global gTriggerKeys = #("ENTITYFX_AMBIENT_PTFX_START", "ENTITYFX_COLLISION_PTFX_START", "ENTITYFX_SHOT_PTFX_START", "FRAGMENTFX_BREAK_PTFX_START", "FRAGMENTFX_DESTROY_PTFX_START", "ENTITYFX_ANIM_PTFX_START", "ENTITYFX_RAYFIRE_PTFX_START","ENTITYFX_INWATER_PTFX_START")
global triggerColours= #((color 128 255 64),green,blue,yellow,(color 0 255 255),(color 255 0 255),(color 200 128 0),(color 100 50 100))
global gLastSelectedType = undefined
	
global gDomainRollout = undefined

fn RsParticleSelectionCallback ev nd = 
(
	if nd.count<=0 do return false
	
-- 		local aNode = nd[1]
-- 		if aNode.selected and "RAGE Particle"==(getattrclass aNode) then
-- 		(
-- 			gLastSelectedType = aNode.myType
-- 		)
	
	local objs = for nodeHandle in nd collect (getAnimByHandle nodeHandle)
	local particleObjs = for obj in objs where (isKindOf obj ::RsRageParticleHelper) collect obj
	
	if (particleObjs.count != 0) do 
	(
		deleteAllChangeHandlers id:#RsParticleObjects
		
		for obj in particleObjs do 
		(
			obj.RefreshEffectDefinition theNode:obj delHandlers:false
		)
	)
)

global gRsParticleSelectHandler = NodeEventCallback selectionChanged:RsParticleSelectionCallback
	
struct RsCollParticle (name, type, assetName)
struct sParticleCreateTempSave (idx, val)

plugin Helper RsRageParticleHelper
extends:dummy
name:"RSN_Particle"
classID:#(0x3b8f7c4d, 0x7637f9d)
--classID:#(0x110a6ecf, 0x561063cf) -- plugin classid
category:"RAGE Effects"
( 
	local lastSize --, meshObj

	local changeScaleHandler = undefined

	local nameIdx = getattrindex "RAGE Particle" "Name"
	local isTriggeredIdx = getattrindex "RAGE Particle" "Is Triggered"
	local attachIdx = getattrindex "RAGE Particle" "Attach to All"
	local scaleIdx = getattrindex "RAGE Particle" "Scale"
	local probIdx = getattrindex "RAGE Particle" "Probability"
	local hasTintIdx = getattrindex "RAGE Particle" "Has Tint"
	local tintRIdx = getattrindex "RAGE Particle" "R"
	local tintGIdx = getattrindex "RAGE Particle" "G"
	local tintBIdx = getattrindex "RAGE Particle" "B"
	local tintAIdx = getattrindex "RAGE Particle" "A"
	local ignoreIdx = getattrindex "RAGE Particle" "Ignore Damage Model"
	local playIdx = getattrindex "RAGE Particle" "Play On Parent"
	local onlyDModelIdx = getattrindex "RAGE Particle" "Only on damage model"
	local sizeXIdx = getattrindex "RAGE Particle" "Emitter Size X"
	local sizeYIdx = getattrindex "RAGE Particle" "Emitter Size Y"
	local sizeZIdx = getattrindex "RAGE Particle" "Emitter Size Z"
	local rubberBulletFXidx = getattrindex "RAGE Particle" "Allow Rubber Bullet Shot FX"
	local electricBulletFXidx = getattrindex "RAGE Particle" "Allow Electric Bullet Shot FX"
	local creationSave = #()
	local effectDefinition = undefined
	local transformHandler = undefined
	local iJustGotCloned = undefined
	local triggerIdx = 0
	
	local thisNode
	fn GetNode = 
	(
		if (not isValidNode thisNode) do 
		(
			thisNode = undefined
			
			for obj in (refs.dependents this) where (isKindof obj RsRageParticleHelper) while (thisNode == undefined) do 
			(
				thisNode = obj
			)
		)
		
		return thisNode
	)
	
	fn GetTriggerByTriggerId triggerId = 
	(
		local keyIndex = finditem gTriggerKeys triggerId
		if keyIndex > 0 then
			return triggerStates[keyIndex]
		else
			return undefined
	)
	fn GetTriggerByFxType effectId = 
	(
		local retVal = undefined
		local found = False
		
		for particle in CollParts while (not found) do 
		(
			if (particle.name == effectId) do 
			(
				retVal = particle.type
				found = True
			)
		)

		return retVal
	)

	fn transformObject obj matrix = 
	(
		for vi=1 to getnumverts obj do
		(
			local v = getvert obj vi
			local newV = v * matrix
			setvert obj vi newV
		)
	)
	
	fn AppendCreationSave pidx pval = 
	(
		local idxFound = undefined
		for a in creationSave where a.idx==pidx do (idxFound=a; exit)
		if undefined==idxFound then
		(
			local tempSave1 = sParticleCreateTempSave()
			tempSave1.idx=pidx
			tempSave1.val=pval
--			print ("new value "+tempSave1 as string)
			append creationSave tempSave1
		)
		else
		(
			if Array==classof idxFound then
				idxFound = idxFound[1]
			idxFound.val = pval
--			print ("found value "+idxFound as string)
		)
	)
	
	parameters main rollout:ParticleParams
	(
		meshObj type:#maxObject
		myType type:#string default:""
		hideDomain type:#Boolean ui:checkHideDomain default:false
		showGizmo type:#Boolean ui:checkShowGizmo default:false
		active type:#Boolean ui:checkActive default:true
		minZoom type:#float default:0.0
		maxZoom type:#float default:100.0
 		myZoom type:#float ui:sliderZoom default:0.0
		myKeyFrame type:#integer ui:spnKeyframe default:1
		isWorldSpace type:#Boolean default:false
		hideLabel type:#Boolean ui:checkHideLabel default:false
		
		on hideDomain set val do 
		(
			meshObj = undefined
		)
		on showGizmo set val do 
		(
			meshObj = undefined
		)
		on myZoom set val do 
		(
			meshObj = undefined
		)
		on myType set val do
		(
			effectDefinition = undefined
			local theNode = GetNode()
			if undefined!=theNode and not matchPattern theNode.name pattern:("*"+val+"*") then
			(
				theNode.name = uniqueName ("RSNParticle_"+val+"_")
			)
			setattr theNode nameIdx val
		)
	)
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	fn RemoveDomainRollout = 
	(
		try ( removeRollout gDomainRollout ) catch()
		gDomainRollout = undefined
	)
	
	fn CreateDomainRollout = 
	(
		RemoveDomainRollout()
		
		if (effectDefinition == undefined) do (return False)
		
		local rc = rolloutCreator "RsDomainRoll" "Domain drawing"
		rc.begin()
		local controlIndex = 1
		rc.addControl #label "infoLabel" "Hover to see domain name"
		for r in effectDefinition.fxRules do
		(
			local controlName = r.emName+"_"+(controlIndex as string)
			rc.addControl #checkbox controlName r.emType paramStr:("checked:true tooltip:\""+r.emName+"\"")
			rc.addHandler controlName #changed paramStr:" val " codeStr:"$selection[1].meshObj = undefined; redrawViews()"
			controlIndex += 1
		)
		gDomainRollout = rc.end()
		AddRollout gDomainRollout
	)

	fn UpdateProperties theNode: = 
	(
 		if (effectDefinition == undefined) do return false
		
		local setWorldSpace = false
		for rule in effectDefinition.fxRules where rule.emIsWorldSpace while not setWorldSpace do
		(
			setWorldSpace = rule.emIsWorldSpace
		)
		
		-- Only set param if changed:
		if (isWorldSpace != setWorldSpace) do 
		(
			isWorldSpace = setWorldSpace
		)
		
		local paramRoll = paramRoll
		if (paramRoll != undefined) and paramRoll.open do 
		(
			paramRoll.spnKeyframe.range = [1,effectDefinition.fxGlobalMinScale.count,myKeyFrame]
			paramRoll.spnKeyframe.text = ("Key of #"+effectDefinition.fxGlobalMinScale.count as string)
			
			if isWorldSpace then
			(
				paramRoll.btnWorldSpace.text = "WORLD SPACE"
				paramRoll.btnWorldSpace.tooltip = "This effect has emitter domains defined in worldspace. Any rotations would cause the data to be invalid."
			)
			else
			(
				paramRoll.btnWorldSpace.text = "local space"
				paramRoll.btnWorldSpace.tooltip = ""
			)
		)
		
		if (theNode == unsupplied) do 
		(
			theNode = GetNode()
		)
		
		if isWorldSpace and undefined!=theNode then 
		(
--			theNode.transform = transMatrix (theNode.pos)
			transformHandler = when transform theNode changes id:#RsParticleObjects obj do 
			(
-- 				local theNode = obj.GetNode()
-- 				if undefined!=theNode then 
-- 					theNode.transform = transMatrix (theNode.pos)
				obj.meshObj = undefined
			)
		)

		return true
	)
	
	fn RefreshEffectDefinition theNode: delHandlers:true =
	(
		if (theNode == unsupplied) do 
		(
			theNode = GetNode()
		)
		
		if (theNode != undefined) do 
		(
			local getType = getattr theNode nameIdx
			
			-- Only set param if changed:
			if (myType != getType) do 
			(
				myType = getType
			)
		)
		
		if (effectDefinition == undefined) do 
		(
			try (effectDefinition = rageparticle_fxemitters myType) catch 
			(
				try (
--					print "rebuild particles"
					rageparticle_rebuild()
					effectDefinition = rageparticle_fxemitters myType
				)catch(
					print "rebuild catch"
					effectDefinition = undefined
				)
			)
		)
		
		if delHandlers do 
		(
			deleteAllChangeHandlers id:#RsParticleObjects
		)
		
		return (UpdateProperties theNode:theNode)
	)
	

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn LoadEntries = 
	(
		CollParts = #()
		filename = RsGetEntityFxInfoPath()

		intFile = openfile filename mode:"rb"
		
		if(intFile == undefined) then (
			gRsUlog.LogError("Error opening file: "+ filename)
			return false			
		)

		local newTrigger = #none
		
		while eof intFile == false do 
		(
			intLine = RsRemoveSpacesFromStartOfString(readline intFile)
			if intLine.count>0 and intLine[1] != "#" then 
			(
				local intTokens = filterstring intLine " \t"
				if intTokens.count==1 then
				(
					local triggerType = GetTriggerByTriggerId intTokens[1]
					if undefined!=triggerType then
						newTrigger = triggerType
					else
					(
--						print ("trigger type "+ intTokens[1] as string +" is not found in array")
						newTrigger = #none
					)
				) 
				else  if newTrigger!=#none then 
				(
					newItem = RsCollParticle intTokens[1] newTrigger intTokens[2]
					appendIfUnique CollParts newItem
				)
			)
		)
		
		close intFile
	)

	fn UpdateTypes = (
	
--		print "updating_______________________________________________________"
	
		CollCurrPartNames = #()
		
		local theNode = GetNode()
		
		local addIndex =  1
		if 	undefined==triggerIdx or 
			false==triggerIdx or
			triggerIdx<=0 then 
		(
			triggerIdx = 1
		)
		for item in CollParts do 
		(
			show = false
			if triggerStates[triggerIdx]==item.type then 
				show = true
			if show then (
				append CollCurrPartNames item.name
				addIndex = addIndex+1
			)
		)
	)
	
	-- Viewport-draw bits:
	fn redrawviews_p theNode:(GetNode()) = 
	(
		if not hideLabel and (isValidNode theNode) and (not theNode.isHiddenInVpt) do 
		(
			gw.settransform theNode.transform
			local r = getattr theNode tintRIdx
			local g = getattr theNode tintGIdx
			local b = getattr theNode tintBIdx
			local a = getattr theNode tintAIdx
			
			gw.setColor #line (color r g b)
			gw.Marker [0,0,0] #circle color:(color r g b a)
			gw.text ((point3 0 0 1)) myType color:(color r g b)

			if showGizmo then
			(
				local myScale = (getattr theNode scaleIdx)
				local colours = #(red, green, blue)
				for vi = 1 to 3 do
				(
					gw.setColor #line colours[vi]
					local p = point3 0 0 0
					p[vi] = myScale * 1.5
					gw.PolyLine #([0,0,0], p) false
				)
			)
		)
	)
	
	fn refresh handlers:false = 
	(
		thisNode = undefined
		
		-- update types
		LoadEntries()
		UpdateTypes()

		if handlers do 
		(
			if (gRsParticleSelectHandler != undefined) do 
			(
				gRsParticleSelectHandler = undefined
				gc light:true
			)

			setPropertyController this "active" (boolcntrl())
		)
	)
	
	fn isValidParent obj = 
	(
		not isKindOf obj RsRageParticleHelper
	)
	
	fn setNodeParent obj theNode:(GetNode()) = 
	(			
		if (theNode.parent != obj) do 
		(
			-- Add this node to same container:
			local objCont = RsGetObjContainer obj
			if (objCont != undefined) do 
			(
				objCont.addNodesToContent theNode
			)
			
			theNode.parent = obj
		)
		
		theNode.ParticleParams.setPickButtonText()
	)
	
	rollout ParticleParams "Parameters"
	(
		hyperlink hyperHelp "HELP?" color:(color 100 100 255) address:"https://devstar.rockstargames.com/wiki/index.php/RSN_Particle_helper" align:#right offset:[6,-4]
		label lbl1 "Attached to object:" align:#left offset:[-6,-14]
		pickbutton btnPickParent "<None>" filter:isValidParent width:154 offset:[-1,-2]
		group "Type:"
		(
			radiobuttons chkTrigger labels:triggerStates default:1 align:#left columns:2
			dotnetcontrol dropDownType "Combobox" height:20 width:160 offset:[-12,0]
			edittext edtAssetName "PTFX Asset Name:" labelOnTop:true readOnly:true
		)
		group "Start and stop (Animatable)"
		(
			checkbox checkActive "Active"
		)
		group "Viewport appearance"
		(
			checkbox checkHideDomain "Hide Domain"
			checkbox checkHideLabel "Hide Label"
			checkbox checkShowGizmo "Show axis tripod"
			slider sliderZoom "Preview game size variation:" range:(point3 0.0 1.0 myZoom)
			spinner spnKeyframe "Keyframe" type:#integer range:[1,1,1]
			button btnWorldSpace "local space" tooltip:"" border:false
		)
		group "Advanced"
		(
			checkbox checkUseColour "colour enabled" checked:false
			colorpicker colour "Particle tint:" enabled:false alpha:true
			spinner spinProb "Probability:" range:[0.0,100.0,100.0]
			spinner spinScale "Effect scale:" range:[0.0,10.0,1.0]
			checkbox checkAttachToAll "Attach to all"
			checkbox checkIgnore "Ignore damage model"
			checkbox checkPlay "Play on parent"
			checkbox checkOnlyDamage "Only on damage model"
			checkbox checkAllowRubberBulletShotFX "Allow Rubber Bullet Shot FX"
			checkbox checkAllowElectricBulletShotFX "Allow Electric Bullet Shot FX"
		)
		
		fn updateAssetName = (

			if ( CollParts.count > 0 ) do
			(
				for i = 1 to CollParts.count do
				(
					if ( CollParts[i].name == myType ) do
					(
						edtAssetName.text = Collparts[i].assetName						
					)
				)
			)
		)
		
		fn setPickButtonText = 
		(
			local theNode = GetNode()
			local nodeParent = theNode.parent
			
			btnPickParent.text = if (nodeParent != undefined) then 
			(
				nodeParent.name
			)
			else 
			(
				"<NONE>"
			)
		)
		
		on btnPickParent picked obj do
		(
			setNodeParent obj
		)
		on btnPickParent rightClick do
		(
			setNodeParent undefined
		)
		
		on ParticleParams open do
		(
			local theNode = GetNode()
			if (isValidNode theNode) then
			(
				for attr in creationSave do
				(
					print (attr as string)
					setattr theNode attr.idx attr.val
				)
				creationSave = #()
				
				if "RAGE Particle"==(getattrClass theNode) then
				(
					checkIgnore.checked = getAttr theNode ignoreIdx
					checkPlay.checked = getAttr theNode playIdx
					checkOnlyDamage.checked = getAttr theNode onlyDModelIdx
					checkAttachToAll.checked = getAttr theNode attachIdx
					checkAllowRubberBulletShotFX.checked = getAttr theNode rubberBulletFXidx
					checkAllowElectricBulletShotFX.checked = getAttr theNode electricBulletFXidx
					spinScale.value = getAttr theNode scaleIdx
					spinprob.value = getAttr theNode probIdx
					checkUseColour.checked = getAttr theNode hasTintIdx
					local r = getattr theNode tintRIdx
					local g = getattr theNode tintGIdx
					local b = getattr theNode tintBIdx
					local a = getattr theNode tintAIdx
					colour.color = (color r g b a)
					colour.enabled = checkUseColour.checked
					
					if ""==myType then
					(
						myType = getattr theNode nameIdx
					)
					local triggerType = GetTriggerByFxType myType
					triggerIdx = findItem triggerStates triggerType
					chkTrigger.state = triggerIdx
--					print ("effect trigger for "+myType as string+" is "+triggerType as string)
					if undefined!=triggerType then
					(
						UpdateTypes()
						dropDownType.items.addrange CollCurrPartNames
						local itemIndex = findItem CollCurrPartNames myType
						if 0!=itemIndex then dropDownType.SelectedIndex = (itemIndex-1)
					)
				)

				changeScaleHandler = when transform theNode changes do
				(
					local theNode = GetNode()
					if "RAGE Particle"==(getattrClass theNode) then
					(
						setAttr theNode sizeXIdx (floor(theNode.scale.x * 1000)/1000)
						setAttr theNode sizeYIdx (floor(theNode.scale.y * 1000)/1000)
						setAttr theNode sizeZIdx (floor(theNode.scale.z * 1000)/1000)
					)
					else
					(
						AppendCreationSave sizeXIdx (floor(theNode.scale.x * 1000)/1000)
						AppendCreationSave sizeYIdx (floor(theNode.scale.y * 1000)/1000)
						AppendCreationSave sizeZIdx (floor(theNode.scale.z * 1000)/1000)
					)
		--			theNode.transform.scale  = [1,1,1]
				)
				
				gLastSelectedType = myType
				
				setPickButtonText()
				
				if undefined!=effectDefinition then
					spnKeyframe.text = ("Key of #"+effectDefinition.fxGlobalMinScale.count as string+" :")

				CreateDomainRollout()
			)
			else
			(
				UpdateTypes()
				--setup  name
				dropDownType.items.addrange CollCurrPartNames
				if undefined!=CollCurrPartNames[1] then
					myType = CollCurrPartNames[1] --dropDownType.selected
				if undefined!=gLastSelectedType then
					myType = gLastSelectedType
				
				AppendCreationSave nameIdx myType
			)
			
			updateAssetName()
			try( dotnet.setLifetimeControl dropDownType #dotnet) catch()
			dropDownType.DropDownWidth = 200
			dropDownType.DropDownStyle = (dotnetclass "ComboBoxStyle").DropDownList
			addRollout RsPartDBRoll
			
			setFocus dropDownType
		)
		on ParticleParams close do
		(
			if (changeScaleHandler != undefined) do 
			(
				deleteChangeHandler changeScaleHandler
			)
			
			removeRollout RsPartDBRoll
			RemoveDomainRollout()
		)
		on chkTrigger changed val do
		(
			triggerIdx = val
			UpdateTypes()
			
			dropDownType.items.clear()
			
			if (CollCurrPartNames.count > 0) do 
			(
				dropDownType.items.addrange CollCurrPartNames
				dropDownType.SelectedIndex = 0
				
				myType = CollCurrPartNames[1]
			)
			
			meshObj = undefined
			updateAssetName()
			redrawViews()
		)
		on checkIgnore changed val do
		(
			local theNode = GetNode()
			if "RAGE Particle"==(getattrClass theNode) then
				setAttr theNode ignoreIdx val
			else
			(
				AppendCreationSave ignoreIdx val
			)
		)
		on checkAttachToAll changed val do
		(
			local theNode = GetNode()
			if "RAGE Particle"==(getattrClass theNode) then
				setAttr theNode attachIdx val
			else
			(
				AppendCreationSave attachIdx val
			)
		)
		on checkPlay changed val do 
		(
			local theNode = GetNode()
			if "RAGE Particle"==(getattrClass theNode) then
				setAttr theNode playIdx val
			else
			(
				AppendCreationSave playIdx val
			)
		)
		on checkOnlyDamage changed val do 
		(
			local theNode = GetNode()
			if "RAGE Particle"==(getattrClass theNode) then
				setAttr theNode onlyDModelIdx val
			else
			(
				AppendCreationSave onlyDModelIdx val
			)
		)	
		on checkAllowRubberBulletShotFX changed val do 
		(
			local theNode = GetNode()
			if "RAGE Particle"==(getattrClass theNode) then
				setAttr theNode rubberBulletFXidx val
			else
			(
				AppendCreationSave rubberBulletFXidx val
			)
		)	
		on checkAllowElectricBulletShotFX changed val do 
		(
			local theNode = GetNode()
			if "RAGE Particle"==(getattrClass theNode) then
				setAttr theNode electricBulletFXidx val
			else
			(
				AppendCreationSave electricBulletFXidx val
			)
		)
		on spinScale entered do
		(
			local theNode = GetNode()
			if "RAGE Particle"==(getattrClass theNode) then
			(
				setAttr theNode scaleIdx spinScale.value
			)
			else
			(
				AppendCreationSave scaleIdx spinScale.value
			)
			meshObj = undefined
			redrawViews()
		) 
		on spinprob entered do
		(
			local theNode = GetNode()
			if "RAGE Particle"==(getattrClass theNode) then
				setAttr theNode probIdx spinprob.value
			else
			(
				AppendCreationSave probIdx spinprob.value
			)
		)
		on checkUseColour changed val do 
		(
			if val then colour.enabled = true
			else colour.enabled = false
			local theNode = GetNode()
			if "RAGE Particle"==(getattrClass theNode) then
				setAttr theNode hasTintIdx val
			else
			(
				AppendCreationSave hasTintIdx val
			)
		)
		on colour changed val do
		(
			local theNode = GetNode()
			if "RAGE Particle"==(getattrClass theNode) then
			(
				setAttr theNode tintRIdx val.r
				setAttr theNode tintGIdx val.g
				setAttr theNode tintBIdx val.b
				setAttr theNode tintAIdx val.a
			)
			else
			(
				AppendCreationSave tintRIdx val.r
				AppendCreationSave tintGIdx val.g
				AppendCreationSave tintBIdx val.b
				AppendCreationSave tintAIdx val.a
			)
		)
		on dropDownType SelectionChangeCommitted Sender Args do
		(
			myType = (dropDownType.selectedItem as string)

			local theNode = GetNode()
			if "RAGE Particle"!=(getattrClass theNode) then
			(
				AppendCreationSave nameIdx myType
			)
			myZoom = 1.0
			meshObj = undefined

			updateAssetName()
			redrawViews()
			
			setFocus dropDownType
		)
		on spnKeyframe changed val do 
		(
			meshObj = undefined
		)
	)

	fn updateAttributes =
	(
		local theNode = GetNode()
		if undefined!=theNode and ParticleParams.open then
		(
			setattr theNode nameIdx myType

			if undefined!=effectDefinition then
			(
				setAttr theNode isTriggeredIdx (effectDefinition.fxLoopCount!=-1)
			)
			setAttr theNode scaleIdx ParticleParams.spinScale.value
			setAttr theNode probIdx ParticleParams.spinprob.value
			setattr theNode hasTintIdx ParticleParams.checkUseColour.state
			setAttr theNode tintRIdx ParticleParams.colour.color.r
			setAttr theNode tintGIdx ParticleParams.colour.color.g
			setAttr theNode tintBIdx ParticleParams.colour.color.b
			setAttr theNode tintAIdx ParticleParams.colour.color.a
			setattr theNode ignoreIdx ParticleParams.checkIgnore.state
			setattr theNode playIdx ParticleParams.checkPlay.state
			setattr theNode onlyDModelIdx ParticleParams.checkOnlyDamage.state
			setattr theNode rubberBulletFXidx ParticleParams.checkAllowRubberBulletShotFX.state
			setattr theNode electricBulletFXidx ParticleParams.checkAllowElectricBulletShotFX.state
			setAttr theNode sizeXIdx (floor(theNode.scale.x * 1000)/1000)
			setAttr theNode sizeYIdx (floor(theNode.scale.y * 1000)/1000)
			setAttr theNode sizeZIdx (floor(theNode.scale.z * 1000)/1000)
		)
		else
		(
			return false
		)
		return true
	)
	
	--fn getDisplayMesh = 
	on getDisplayMesh do 
	(
		local theNode = GetNode()
		local size = 1
		
		if undefined==theNode then
			return false

		if XRefObject==(classof theNode) then
		(
			messagebox ("Particle helper objects may not be xref'ed! "+theNode as string)
--			objXRefMgr.RemoveXRefItemsFromScene #(theNode)
			return mesh()
		)
		
		if undefined!=iJustGotCloned and RsRageParticleHelper==(classof iJustGotCloned) then
		(
			print "iJustGotCloned?!"
			local cloneFrom = (iJustGotCloned.GetNode())
			if undefined!=cloneFrom then
			(
				copyAttrs cloneFrom
				pasteAttrs theNode
			)
			iJustGotCloned = undefined
		)

		if ""==myType then
		(
			meshObj = undefined
			if undefined==CollCurrPartNames[1] then
				UpdateTypes()
			myType = CollCurrPartNames[1]
		)
		
		local needsReevaluation = (meshObj == undefined or isDeleted meshObj) 
		if triggerIdx == 0 or needsReevaluation then
		(
			local triggerType = GetTriggerByFxType myType
			triggerIdx = findItem triggerStates triggerType
		)		
		--try
		(
			with redraw off in coordsys world
			(
				if needsReevaluation do 
				(
					local myScale = (getattr theNode scaleIdx)
					local needsNewDomainRollout = effectDefinition==undefined
					RefreshEffectDefinition()
					if needsNewDomainRollout and 
						#modify==getCommandPanelTaskMode() and
						selection.count==1 and 
						selection[1]==theNode then
					(
						CreateDomainRollout()
					)

					meshObj = createinstance Editable_Mesh()
					local newPivot = undefined
					if not hideDomain then
					(
						
						if undefined==effectDefinition then
						(
							RsNotifyPopUp_create title:"Effect build errors!" text:"Choose another effect from the dropdown please."
							meshObj = createInstance box length:size width:size height:size mapCoords:false
							lastSize = size
							if triggerIdx>0 and triggerIdx<=triggerColours.count then
								theNode.wirecolor = triggerColours[triggerIdx]
							gRsUlog.LogError ("Particle object "+theNode.name+" has an unresolved type. Please reset. https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Particle_Effects:_unresolved_particle_type") context:theNode
							theNode.name = "REPLACE"
							
							redrawviews_p()
							return meshObj.mesh
						)
						
						if undefined!=theNode then
							setAttr theNode isTriggeredIdx (effectDefinition.fxLoopCount!=-1)
						
						local controlIndex = 1 -- info label is number one.
						if myKeyFrame>effectDefinition.fxGlobalMinScale.count then
							myKeyFrame = effectDefinition.fxGlobalMinScale.count
						local minEffectScale = 1
						local maxEffectScale = 1
						if myKeyFrame>0 then
						(
							minEffectScale = effectDefinition.fxGlobalMinScale[myKeyFrame] / 100
							maxEffectScale = effectDefinition.fxGlobalMaxScale[myKeyFrame] / 100
						)
						local userScalePart = (maxEffectScale-minEffectScale) * myZoom
						local userScale = (minEffectScale + userScalePart)
						for emitter in effectDefinition.fxRules do
						(
							controlIndex += 1
							if 	undefined!=gDomainRollout and
								(controlIndex>gDomainRollout.controls.count or not
								gDomainRollout.controls[controlIndex].checked) then
								continue

							local partobj = undefined
							local myZoomVec = emitter.emZoom
							minZoom = myZoomVec.x/100.0
							maxZoom = myZoomVec.y/100.0
							local userZoom = (maxZoom-minZoom) * myZoom
							local emZoom = (minZoom + userZoom) * 100.0
							local scaledSize = myScale * emZoom * emitter.emSize * userScale 
---							local scaledInnerSize = myScale * emZoom * emitter.emInnerSize * userScale
							local scaledPos = myScale * emZoom * emitter.emPos
--							print ("size:"+scaledSize as string)
							
							local transMat = (matrix3 1)
							local rotMatrix = (eulerangles emitter.emDir.x emitter.emDir.y emitter.emDir.z) as matrix3
							
							case emitter.emType of
							(
								"Box": (
									partobj = createInstance box width:scaledSize.x length:scaledSize.y height:scaledSize.z mapCoords:false
									transMat.translation = (point3 0 0 (-scaledSize.z/2))
								)
								"Sphere": (
									partobj = createInstance sphere radius:scaledSize.x mapCoords:false
								)
								"Cylinder": (
									partobj = createInstance cylinder height:scaledSize.y radius:1 mapCoords:false
									transMat.translation = (point3 0 0 (-scaledSize.y/2))
									rotateX transMat 90
									scale transMat (point3 scaledSize.x 1 scaledSize.z)
								)
							)
							if undefined!=partobj and not isDeleted partobj then
							(
								local partMesh = partobj.mesh
								transformObject partMesh transMat
								if undefined!=theNode and emitter.emIsWorldSpace then
								(
									rotate rotMatrix (theNode.rotation)
									scaledPos *= (theNode.rotation as matrix3)
								)
								transformObject partMesh rotMatrix
								
								local posMatrix = transMatrix scaledPos
								transformObject partMesh posMatrix
								meshop.attach meshObj partMesh
							)
						)
					)-- not hideDomain
					local circleobj = createinstance cylinder height:0 radius:myScale
					meshop.attach meshObj circleobj.mesh

					lastSize = size
				)
			)-- end redraw off
		)
-- 		catch ( print (getCurrentException()))
		local useColourOverride = ParticleParams.checkUseColour.state
		try(
			theNode.wirecolor = 
			(
				if active then 
				(
					if isWorldSpace then
						red
					else
						triggerColours[triggerIdx] 
				)
				else
					black
			)
		)
		catch(
			print ("Error:"+getCurrentException())
		)
		
		-- Viewport-draw:
		redrawviews_p()
		
		return meshObj.mesh
	)
	
	on update do 
	(
		meshObj = undefined
		refresh()
	)
	on postLoad do
	(
		refresh handlers:true
	)
	on postCreate do 
	(
		refresh handlers:true
	)
	on clone original do 
	(
		refresh handlers:true
		meshObj = undefined
		iJustGotCloned = original
	)
	
	tool create
	(
		local preCreateSel
		
		on start do 
		(	
			-- Store link to the currently-selected object, before selection is cleared by the incoming created object:
			preCreateSel = undefined
			local currentSel = GetCurrentSelection()
			if (currentSel.count == 1) do 
			(
				preCreateSel = currentSel[1]
			)
		)
		
		on mousePoint click do 
		(
			nodeTM.translation = gridPoint
			
			-- Apply values and jittering from Browser:
			--	Does nothing if tool is closed, or isn't in matching object-mode.
			local ThisNode = (refs.dependentNodes This)[1]
			::RSrefBrowserRoll.SetupNewObj ThisNode nodeXform:nodeTM
			
			-- Parent new node to object that was selected when create-tool started:
			if (isValidNode preCreateSel) do 
			(
				local theNode = GetNode printdeps:true
				theNode.setNodeParent preCreateSel theNode:theNode
			)
			
			return #stop
		)
	)
)
