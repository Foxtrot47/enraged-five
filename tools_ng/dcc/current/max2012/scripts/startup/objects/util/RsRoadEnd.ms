plugin helper RsRoadEnd
name:"RsRoadEnd"
classID:#(0x67b164c8, 0x6c30d894)
extends: dummy
category:"RS Helpers"
(
	------------------------------------------------------------------
	-- LOCALS
	------------------------------------------------------------------
	local meshObj
	local thisNode
	local otherNode
	------------------------------------------------------------------
	-- PARAMETERS
	------------------------------------------------------------------
	parameters pblock rollout:params
	(
		magnitude type:#float  ui:spn_outVel default:1
		spline type:#node 
		otherEndHandle type:#integer
		isStart type:#boolean
		preview type:#node
	)	
	------------------------------------------------------------------
	-- FUNTIONS
	------------------------------------------------------------------
	fn getNodesFromHandles = 
	(
		if (thisNode == undefined) do thisNode = (refs.dependentnodes this)[1]	
		otherNode = if (otherEndHandle != undefined) then (maxOps.getNodeByHandle otherEndHandle) else undefined
		
		return ((thisNode != undefined) AND (otherNode != undefined))
	)
	------------------------------------------------------------------
	fn getVector n =
	(
		return normalize ((preTranslate n.transform [0,1,0]).translationpart - n.pos)
	)
	------------------------------------------------------------------
	fn getBezierHandle n1 n2 m =
	(
		local d = (distance n1 n2)/2
		local v = getVector n1 
		return (n1.pos + (v * d *  m))
	)
	------------------------------------------------------------------
	fn makeSpine = 
	(
		if (spline == undefined) do 
		(			
			if (getNodesFromHandles()) do 
			(	
				isStart = true
				otherNode.isStart = false
				
				ss = SplineShape pos:thisNode.pos
				addNewSpline ss
				addKnot ss 1 #bezier #curve  thisNode.pos thisNode.pos (getBezierHandle thisNode otherNode thisNode.magnitude)
				addKnot ss 1 #bezier #curve  otherNode.pos (getBezierHandle otherNode thisNode otherNode.magnitude) otherNode.pos 
				updateShape ss	

				spline = ss
				otherNode.spline = ss
				completeRedraw()
				
				this.params.spn_inVel.enabled = true
				this.params.spn_inVel.value = otherNode.magnitude
			)
		)
	)
	------------------------------------------------------------------
	fn updateSpline = 
	(
		if (spline != undefined) do 
		(			
			if (getNodesFromHandles()) do 
			(
				if (isStart) then setKnotPoint  spline 1 1 thisNode.pos
				else setKnotPoint  spline 1 (numknots spline) thisNode.pos
				updateShape spline
				
				this.outVelChanged()
			)
		)
	)
	------------------------------------------------------------------
	fn otherEnd_picked obj =
	(
		otherEndHandle = obj.handle
		if (otherEndHandle != undefined) do this.params.btn_pickOtherEnd.text = (maxOps.getNodeByHandle otherEndHandle).name
		if (thisNode != undefined) do obj.otherEndHandle = thisNode.handle
		this.makeSpine()
	)	
	------------------------------------------------------------------	
	fn selectOtherEnd = 
	(
		if (otherEndHandle != undefined) do 
		(
			local otherNode = (maxOps.getNodeByHandle otherEndHandle)
			if (otherNode != undefined) do select otherNode
		)
	)
	------------------------------------------------------------------
	fn clearOtherEnd = 
	(
		if (getNodesFromHandles()) do 
		(
			otherEndHandle = -1
			this.params.btn_pickOtherEnd.text = "Pick Other End"
			if (spline != undefined) AND (queryBox "Do you want to delete Spline as well?" title:"Question?") do 
			(
				delete spline 
				spline = undefined
			)
			otherNode.otherEndHandle = -1
			this.params.spn_inVel.enabled = false
		)
	)
	------------------------------------------------------------------
	fn outVelChanged arg = 
	(		
		if (spline != undefined) AND (getNodesFromHandles()) do 
		(
			if (isStart) then setOutVec spline 1 1 (getBezierHandle thisNode otherNode arg) 		
			else setinVec spline 1 (numknots spline) (getBezierHandle thisNode otherNode arg) 		
			updateShape spline
		)
	)	
	------------------------------------------------------------------
	fn inVelChanged arg = 
	(
		if (spline != undefined) AND (getNodesFromHandles()) do 
		(
			otherNode.magnitude = arg 
			otherNode.outVelChanged arg
		)
	)
	------------------------------------------------------------------
	fn drawPreview arg =
	(	
		if (arg) then -- DRAW
		(
			if (preview == undefined) do 
			(
				if (spline != undefined) do 
				(
					preview = plane width:10 length:10 lengthsegs:1 widthsegs:1
					local r = (::rssplinetwistpreview())
					addModifier preview r
					r.spline = spline
					--select p
					r.updateLengthSegs()
					
					if (otherNode != undefined) do otherNode.preview = preview
					this.params.cbn_drawPreview.checked = true
				)
			)
		)
		else -- REMOVE
		(
			if (preview != undefined) do 
			(
				delete preview 
				preview = undefined
				if (otherNode != undefined) do otherNode.preview = undefined
				redrawViews()
			)
		)
	)
	------------------------------------------------------------------
	fn endFilder obj = return (classof obj == RsRoadEnd)
	------------------------------------------------------------------
	-- CONTROLS
	------------------------------------------------------------------	 
	rollout params "Parameters"
	(
		button btn_pickOtherEnd "Pick Other End"  width:99 across:3 align:#left offset:[-5,0]
		button btn_selectOtherEnd "S" width:22 align:#right offset:[27,0]
		button btn_clearOtherEnd "X" widht:22 align:#right offset:[5,0]
		button btn_makeSpline "Update Spline" width:145
		
		group "Bezier Handles"
		(
			Spinner spn_outVel "Out Velocity:" offset:[5,0]
			Spinner spn_inVel "In Velocity:" offset:[5,0] enabled:false
		)
		
		checkbutton cbn_drawPreview "Draw Preview" width:150  align:#left offset:[-5,0]
		
		-- CONTROL EVENTS 
		on params open do 
		(
			getNodesFromHandles()
			if (spline == undefined) do spn_inVel.enabled = false
			if (otherNode != undefined) do
			(
				btn_pickOtherEnd.text =  otherNode.name	
				spn_inVel.enabled = true
				spn_inVel.value = otherNode.magnitude
			)
			
			-- REMOVE ROUGE PREVIEWS
			if (spline == undefined) AND (preview != undefined) do 
			(
				delete preview 
				preview = undefined
				if (otherNode != undefined) do otherNode.preview = undefined
			)
			
			cbn_drawPreview.checked = if (preview == undefined) then false else true
		)
		------------------------------------------------------------------
		on btn_pickOtherEnd pressed do 
		(
			getNodesFromHandles()
			local p = pickObject count:1 filter:endFilder rubberBand:thisNode.pos
			if (p != undefined) do otherEnd_picked p 
		)	
		------------------------------------------------------------------
		on btn_clearOtherEnd pressed do clearOtherEnd()
		------------------------------------------------------------------
		on btn_selectOtherEnd pressed do selectOtherEnd()
		------------------------------------------------------------------
		on spn_outVel changed arg do outVelChanged arg
		------------------------------------------------------------------
		on spn_inVel changed arg do inVelChanged arg
		-------------------------------------------------------------------
		on btn_makeSpline pressed do makeSpine()
		-------------------------------------------------------------------
		on cbn_drawPreview changed arg do drawPreview arg
	)

	------------------------------------------------------------------
	-- EVENTS
	------------------------------------------------------------------
	on getDisplayMesh do
	(	  
		if (meshObj == undefined) do
		(		
			local vp = #([0,0,0],[-1.5,0,0],[-1.5,3,0],[-3,3,0],[0,8,0],[3,3,0],[1.5,3,0],[1.5,0,0])
			local fl = #([1,3,2],[1,8,7],[1,7,3],[3,7,5],[3,5,4],[7,6,5])
			
			meshobj = trimesh()
			setmesh meshObj vertices:vp faces:fl
			meshop.autoEdge meshobj #{7, 9..10, 13, 18} 100
		)
		meshObj.mesh
	)
	------------------------------------------------------------------
	on useWireColor do color 0 255 0		
	------------------------------------------------------------------
	-- TOOL
	------------------------------------------------------------------ 
	tool create
	(
		on mousePoint click do
		(
			nodeTM.translation = gridPoint
			#stop
		)
	)	
)
