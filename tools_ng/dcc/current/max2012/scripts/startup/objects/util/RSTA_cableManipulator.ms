/*Notes
	AutoGrid needs to be off before creating cable proxies otherwise the cable drawing will be out of position

*/


function genCablePoints startNode endNode count =
(
	outPoints = #()
	for i=1 to count do
	(
		append outPoints (startNode.pos + ((endNode.pos - startNode.pos) / count) * i)
	)
	outPoints
)

plugin simpleManipulator CableProxy
    name:"Cable Proxy"
	classID:#(0x1860182, 0x79724a54)
    category:"RS Helpers"
(
    -- Create the green and red colors for the gizmo
    local greenColor =  colorMan.getColor #manipulatorsActive
    local redColor =    colorMan.getColor #manipulatorsSelected
	local offsetLength = 0.0
	local editing = false
	local textPos = [0, 0, 0]
	local tooltip = ""

	parameters pblock rollout:params
	(
		startNode type:#node
		endNode type:#node
		points type:#point3Tab tabSizeVariable:true
		pntCount type:#integer default:10
		slack type:#float default:0.03
		nameTag type:#string default:"Cable"
		uid type:#string default:"{00000000-0000-0000-0000-000000000000}"
	)
	
	rollout params "Cable Proxy"
	(
		spinner spnSlack "Slack" default:0.03 range:[0.001, 3.0, 0.03] scale:0.001 type:#float
		spinner spnPntCount "Count" default:10 range:[3,30,10] type:#integer

		on spnPntCount changed val do
		(
			this.pntCount = val
			this.updateGizmos 1f &toolTip
		)
		
		on spnSlack changed val do
		(
			catenaryCurve.slack = val
			this.slack = val
			this.updateGizmos 1f &toolTip
		)
		
		on params open do
		(
			spnSlack.value = slack
		)
	)
	
	fn getNode =
	(
		local cableProxyNode = undefined
		local dependents = (refs.dependents this)
		local cableProxies = for item in dependents where ((isValidNode item) and (classof item == CableProxy)) collect item
		if cableProxies.count != 0 then
		(
			cableProxyNode = cableProxies[1]
		)
		
		cableProxyNode
	)
	
    -- This manipulator manipulates any node with a "radius" property
    on canManipulate target return false
	
	tool create
  	(
  		local startNode, endNode
		
		--AutoGrid needs to be off before creating cable proxies otherwise the cable drawing will be out of position
		on start do
		(
			maxOps.AutoGrid = off
		)
		
		on mousePoint click do
		(
			case click of
  		    (
				1:
				(
					startNode = Point pos:worldPoint wirecolor:green constantscreensize:true name:"CableStart"
				)
				3:
				(
					endNode = Point pos:worldPoint wirecolor:red constantscreensize:true name:"CableEnd"
				)
				4:
				(
					try
					(
						this.startNode = startNode
						this.endNode = endNode
						
						local theNode = (refs.dependentNodes this)[1]
						setTransformLockFlags theNode #all
						
							
						theNode.points = genCablePoints startNode endNode 10
							
						theNode.uid = genGuid()
							
						editing = true
						#stop
					)
					catch
					(
						#stop
					)
					
					#stop
						
				)
				5: #stop
  		   )
	   )
  		
  	)
	
	
    -- Create the manipulator gizmo.
    -- This is called initially and whenever the manipulator target changes
    on updateGizmos do
    (
		this.clearGizmos()
  		
		if editing and (startNode != undefined) and (endNode != undefined) do
		(
			local theNode = getNode()
			if (theNode != undefined) then
			(
				points = #()
				catenaryCurve.slack = this.slack
				catenaryCurve.simulateHangingWire startNode.pos endNode.pos pntCount
				points = catenaryCurve.points
				
				textPos = startNode.pos + (0.5*(endNode.pos - startNode.pos))
			
				theNode.addGizmoText this.nameTag textPos 0 greenColor redColor
				
				local giz = manip.makeGizmoShape()
				giz.startNewLine()
				giz.addPoint startNode.pos
				for pnt in points do
				(
					giz.addPoint pnt
				)
				
				theNode.addGizmoShape giz 0 greenColor redColor
			)
		)
		
		true
    )
	
	on load do
	(
		this.editing = true
	)
	
  
	on MouseDown m which do
	(
		offsetLength = 0.0
	)
	
	
)
