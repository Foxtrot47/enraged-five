plugin Helper RsScenarioPoint
name:"RsScenarioPoint" 
classID:#(0x59c4fdcf, 0x5bd84639)
category:"RS Helpers"
extends:Point 
( 
	parameters pblock rollout:params
	(
		spawnType type:#string animatable:false default:"Seat_Bench"
		pedType type:#string animatable:false default:"None"
		startTime type:#integer animatable:false default:0
		endTime type:#integer animatable:false default:24
	)

	rollout params "RsScenarioPoint Parameters"
	(
		edittext txtSpawnType "Spawn Type:" text:"Test" readonly:true
		edittext txtPedType "Ped Type:" text:"" readonly:true
		edittext txtStartTime "Start Time:" text:"0" readonly:true
		edittext txtEndTime "End Time:" text:"24" readonly:true
	
		on params open do 
		(
			txtSpawnType.text = spawnType
			txtPedType.text = pedType
			txtStartTime.text = (startTime as string)
			txtEndTime.text = (endTime as string)
		)
		
		fn UpdateUI = 
		(
			txtSpawnType.text = spawnType
			txtPedType.text = pedType
			txtStartTime.text = (startTime as string)
			txtEndTime.text = (endTime as string)
		)
	)
	
	fn UpdateUI =
	(
		params.UpdateUI()
	)

	tool create 
	( 
		on mousePoint click do 
		(
			nodeTM.translation = gridPoint
			#stop 
		)
	) 
)
