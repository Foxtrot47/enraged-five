--
-- File:: startup\objects\util\RsExplosionObject.ms
-- Description:: Explosion setup utility
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
-- Move To Parameter Rollout: Terry Litrenta <terry.litrenta@rockstartoronto.com>
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global RsIdxTriggerName
global RsIdxPartName

struct RsExpCollParticle (name)
struct sExplosionCreateTempSave (idx, val)

plugin helper GtaExplosion
	name:"Gta Explosion"
	classID:#(0x7f2d077a, 0x1de6659c)
	category:"Gta"
	extends:Gta_Pickup
	version:2
(	
	local idxGtaExpAttachToAll = getattrindex "Gta Explosion" "Attach to All"
	local idxGtaExpIgnoreDmgModel = getattrindex "Gta Explosion" "Ignore Damage Model"
	local idxGtaExpPlayOnParent = getattrindex "Gta Explosion" "Play On Parent"
	local idxGtaExpOnlyOnDmgModel = getattrindex "Gta Explosion" "Only on damage model"
	local idxGtaExpAllowRubberBulletFX = getattrindex "Gta Explosion" "Allow Rubber Bullet Shot FX"
	local idxGtaExpAllowElectricBulletFX = getattrindex "Gta Explosion" "Allow Electric Bullet Shot FX"
	
	local RsExpCollParts = #()
	local RsExpCollCurrPartNames = #()

	local creationSave = #()
	local thisNode

	fn AppendCreationSave pidx pval = 
	(
		local idxFound = undefined
		for a in creationSave where a.idx==pidx do (idxFound=a; exit)
		if undefined==idxFound then
		(
			local tempSave1 = sExplosionCreateTempSave()
			tempSave1.idx=pidx
			tempSave1.val=pval
--			print ("new value "+tempSave1 as string)
			append creationSave tempSave1
		)
		else
		(
			if Array==classof idxFound then
				idxFound = idxFound[1]
			idxFound.val = pval
--			print ("found value "+idxFound as string)
		)
	)

	fn GetNode = (
		if (not isValidNode thisNode) do (thisNode = undefined)
	
		for obj in (refs.dependents this) where (isKindof obj GtaExplosion) while (thisNode == undefined) do 
		(
			thisNode = obj
		)
	
		return thisNode
	)
	
	-----------------------------------------------------------------------------
	-- Rollout
	-----------------------------------------------------------------------------
	rollout ExplosionParams "Parameters"
	(
		
		--////////////////////////////////////////////////////////////
		-- Interface
		--////////////////////////////////////////////////////////////
		hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/VFX_Toolkit#Explosion_Setup" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

		checkbox checkAttachToAll "Attach to All"
		dropdownlist cboParticle "Name:"
		dropdownlist cboTrigger "Trigger:" items:#("Shot Point","Break","Destroy","Shot Generic")
		checkbox checkIgnoreDamageModel "Ignore Damage Model"
		checkbox checkOnlyOnDamageModel "Only on Damage Model"
		checkbox checkPlayOnParent "Play on Parent"
		checkbox checkAllowRubberBulletShotFX "Allow Rubber Bullet Shot FX"
		checkbox checkAllowElectricBulletShotFX "Allow Electric Bullet Shot FX"
		
		--////////////////////////////////////////////////////////////
		-- Methods
		--////////////////////////////////////////////////////////////
		fn LoadEntries = (
			
			local filename = ""
			
			-- Hardcoded shit - Dave told me to do it!
			if (gRsCoreBranch.name == "dev_ng") or (gRsCoreBranch.name == "dev_gen9") then (
				filename = (RsConfigGetExportDir core:true) + "data/metadata/explosion.pso.meta"
			)else if (gRsCoreBranch.name == "dev") then (
				filename = (RsConfigGetCommonDir core:true) + "data/explosion.meta"
			)else(
				gRsUlog.LogError ("gRsCoreBranch.name was not 'dev', 'dev_ng' or 'dev_gen9'.")
			)
			
			gRsPerforce.sync filename silent:true
			
			if ( doesFileExist filename == false ) do
			(
				gRsULog.LogError (filename + " does not exist!")
				return false
			)
			
			xmlDoc = XMLDocument()
			xmlDoc.init()
			xmlDoc.load filename
			xmlRoot = xmlDoc.document.DocumentElement
			
			if xmlRoot != undefined then 
			(
				expTagNode = RsGetXmlElement xmlRoot "aExplosionTagData"
				items = expTagNode.childnodes
				for i = 0 to ( items.Count - 1 ) do 
				(
					item = items.itemof(i)
					tagNode = RsGetXmlElement item "name"
					
					if tagNode != undefined do 
					(
						newItem = RsExpCollParticle tagNode.innerText
						append RsExpCollParts newItem
					)
				)
			)
		)
		
		fn SetType = (
			local theNode = GetNode()

			if (undefined != theNode) then
			(
				if getattrclass item == "Gta Explosion" then (
					
					setattr item RsIdxTriggerName (cboTrigger.selection - 1)
					-- Set the particle name string attribute
					setattr item RsIdxPartName (RsExpCollCurrPartNames[cboParticle.selection])
				)
			)
			else
			(
				AppendCreationSave RsIdxTriggerName (cboTrigger.selection - 1)
				AppendCreationSave RsIdxPartName (RsExpCollCurrPartNames[cboParticle.selection])
			)
		)
		
		fn UpdateTypes = (
			RsExpCollCurrPartNames = #()
			
			for item in RsExpCollParts do
			(
				append RsExpCollCurrPartNames item.name
			)
		
			cboParticle.items = RsExpCollCurrPartNames
		)
		
		fn UpdateSelection = (
			local theNode = GetNode()
			
			if (theNode != undefined) then (
				for attr in creationSave do
				(
					print (attr as string)
					setattr theNode attr.idx attr.val
				)
			)
			creationSave = #()
					
			if getattrclass theNode == "Gta Explosion" then (
				
				particleName = getattr theNode RsIdxPartName
				notFound = true
				for partNameIdx = 1 to RsExpCollCurrPartNames.count while notFound do
				(
					if ( matchPattern RsExpCollCurrPartNames[partNameIdx] pattern:particleName ) do
					(
						cboParticle.selection = partNameIdx
						notFound = false
					)
				)
				
				val = (getattr theNode RsIdxTriggerName) + 1
				cboTrigger.selection = val
	
				checkAttachToAll.checked = getattr theNode idxGtaExpAttachToAll 
				checkIgnoreDamageModel.checked =  getattr theNode idxGtaExpIgnoreDmgModel
				checkPlayOnParent.checked = getattr theNode idxGtaExpPlayOnParent
				checkOnlyOnDamageModel.checked = getattr theNode idxGtaExpOnlyOnDmgModel	
				checkAllowRubberBulletShotFX.checked = getattr theNode idxGtaExpAllowRubberBulletFX
				checkAllowElectricBulletShotFX.checked = getattr theNode idxGtaExpAllowElectricBulletFX
			)
		)

		--------------------------------------------------------------
		-- Checkbox attribute changes
		--------------------------------------------------------------
		on checkAttachToAll changed arg do (
			local theNode = GetNode()

			if (undefined != theNode) then
			(
				if getattrclass theNode == "Gta Explosion" do
				(
					setattr theNode idxGtaExpAttachToAll arg
				)
			)
			else
			(
				AppendCreationSave idxGtaExpAttachToAll arg
			)
		)
		
		on checkIgnoreDamageModel changed arg do (
			local theNode = GetNode()

			if (undefined != theNode) then
			(
				if getattrclass theNode == "Gta Explosion" do
				(
					setattr theNode idxGtaExpIgnoreDmgModel arg
				)
			)
			else
			(
				AppendCreationSave idxGtaExpIgnoreDmgModel arg
			)
		)

		on checkPlayOnParent changed arg do (
			local theNode = GetNode()

			if (undefined != theNode) then
			(
				if getattrclass theNode == "Gta Explosion" do
				(
					setattr theNode idxGtaExpPlayOnParent arg
				)
			)
			else
			(
				AppendCreationSave idxGtaExpPlayOnParent arg
			)
		)

		on checkOnlyOnDamageModel changed arg do (
			local theNode = GetNode()

			if (undefined != theNode) then
			(
				if getattrclass theNode == "Gta Explosion" do
				(
					setattr theNode idxGtaExpOnlyOnDmgModel arg
				)
			)
			else
			(
				AppendCreationSave idxGtaExpOnlyOnDmgModel arg
			)
		)
		
		on checkAllowRubberBulletShotFX changed arg do (
			local theNode = GetNode()

			if (undefined != theNode) then
			(
				if getattrclass theNode == "Gta Explosion" do
				(
					setattr theNode idxGtaExpAllowRubberBulletFX arg
				)
			)
			else
			(
				AppendCreationSave idxGtaExpAllowRubberBulletFX arg
			)
		)
		
		on checkAllowElectricBulletShotFX changed arg do (
			local theNode = GetNode()

			if (undefined != theNode) then
			(
				if getattrclass theNode == "Gta Explosion" do
				(
					setattr theNode idxGtaExpAllowElectricBulletFX arg
				)
			)
			else
			(
				AppendCreationSave idxGtaExpAllowElectricBulletFX arg
			)
		)
		
		on cboParticle selected arg do (
			SetType()
		)
		
		on cboTrigger selected arg do (
			SetType()
		)
		
		on ExplosionParams open do (
			RsIdxTriggerName = getattrindex "Gta Explosion" "Trigger"
			RsIdxPartName = getattrindex "Gta Explosion" "Explosion Name"
			
			LoadEntries()
			UpdateTypes()
			UpdateSelection()
		)
	)
)