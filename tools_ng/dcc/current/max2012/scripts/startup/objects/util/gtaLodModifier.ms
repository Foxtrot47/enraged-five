-- Rockstar gtaLodModifier Object
-- Rockstar North
-- 30/07/2007
-- by Greg Smith

-- helper object for milo portal setup

RsDisplayNodeList = #()

fn cbLodModAdd o = (

	if finditem RsDisplayNodeList o.name != 0 then return false
	
	true
)

plugin helper GtaLodModifier
	name:"Gta LodModifier"
	classID:#(0x8597790, 0xda46def)
	category:"Gta Helpers"
	extends:BoxGizmo
( 
	parameters objParams rollout:objRollout
	(
		LodNodeList type:#nodeTab tabSizeVariable:true
	)

	rollout objRollout "Lod Modifier"
	(
		listbox lstNodes "Lod Nodes"
		button btnAdd "Add"
		button btnRemove "Remove"
		button btnSelect "Select Nodes"
		button btnHide "Hide Nodes"
		button btnShow "Show Nodes"
		
		on btnSelect pressed do (
		
			clearselection()
			
			sellist = #()
			
			for item in LodNodeList do append sellist item
			
			select sellist
		)
		
		on btnHide pressed do (
		
			for item in LodNodeList do (
			
				item.ishidden = true
			)
		)
		
		on btnShow pressed do (
		
			for item in LodNodeList do (
			
				item.ishidden = false
			)			
		)
		
		on btnAdd pressed do (
		
			objlist = pickobject count:#multiple filter:cbLodModAdd
					
			if objlist != undefined then (
				
				for getobj in objlist do (
				
					append LodNodeList getobj
					append RsDisplayNodeList getobj.name
				)
			)
			
			lstNodes.items = RsDisplayNodeList
		)
		
		on btnRemove pressed do (
		
			if lstNodes.selection != 0 then (

				deleteitem LodNodeList lstNodes.selection
				deleteitem RsDisplayNodeList lstNodes.selection
			
			)
			
			lstNodes.items = RsDisplayNodeList
		)
		
		on objRollout open do (
		
			RsDisplayNodeList = #()
		
			for item in LodNodeList do (
			
				append RsDisplayNodeList item.name
			)
			
			lstNodes.items = RsDisplayNodeList
		)
	)
)