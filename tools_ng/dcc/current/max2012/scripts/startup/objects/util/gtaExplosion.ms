-- Rockstar Gta Explosion Object
-- Rockstar North
-- 8/2/2007
-- by Greg Smith

-- 2007/05/15 - DHM
--						Version 2: handle conversion of Explosion Tag attribute index (7->10)
-- 2012/07/31 - AJM: 
--						The explosion idx value is no longer used, plus explosion helpers
-- 						shouldn't be so old to be version 1 at all thesedays.

plugin helper GtaExplosion
	name:"Gta Explosion"
	classID:#(0x7f2d077a, 0x1de6659c)
	category:"Gta"
	extends:Gta_Pickup
	version:2
( )