--Name defs for the roollout are stored here (RsConfigGetToolsDir() + "etc/config/generic/SphericalTCNameDefs.ini")
global gGtaTimeCycleSphere_DisplayNames = true

fn getSphericalTCNameDefs =
(
	local iniPath = (RsConfigGetToolsDir() + "etc/config/generic/SphericalTCNameDefs.ini")
	if doesFileExist iniPath == false then gRsPerforce.sync #(iniPath)
	
	global gGtaTimeCycleSphere_NameDefs = for name in getINISetting iniPath "TCnames" collect name
	
	global gGtaTimeCycleSphere_NameDescriptions = for def in getINISetting iniPath "TCnames" collect
	(
		getIniSetting iniPath "TCnames" def
	)
	 -- Add a blank space to the drop down for if the TC is set to undefined
	gGtaTimeCycleSphere_NameDefs = join #("") gGtaTimeCycleSphere_NameDefs
	gGtaTimeCycleSphere_NameDescriptions =  join #("") gGtaTimeCycleSphere_NameDescriptions
)

plugin helper GtaTimeCycleSphere
	name:"Gta TC Sphere"
	classID:#(0x18133d65, 0x296f4bd8)
	category:"Gta"
	replaceUI:true
	initialRollupState:0x03
	extends:SphereGizmo
(
	local gtaAttr_Name = 		getAttrIndex "Gta TC Sphere" "Name"
	local gtaAttr_Range = 		getAttrIndex "Gta TC Sphere" "Range"
	local gtaAttr_Percentage = 	getAttrIndex "Gta TC Sphere" "Percentage"
	local gtaAttr_StartHour = 	getAttrIndex "Gta TC Sphere" "StartHour"
	local gtaAttr_EndHour = 	getAttrIndex "Gta TC Sphere" "EndHour"
	
	local meshCacheRadius = 	undefined
	local radiusColour = 		color 255 192 0
	local rangeColour = 		color 0 255 0
	
	local theNode
	
	--////////////////////////////////////////////////////////////////////////////////////////////////////
	-- PARAMETERS
	--////////////////////////////////////////////////////////////////////////////////////////////////////
	
	parameters pBlock rollout:TCSphereUI
	(
		tcName 		type:#string	ui:edtName			default:""
		usrRadius 	type:#float 	ui:spnRadius		default:1.0
		range 		type:#float 	ui:spnRange			default:1.0
		percentage 	type:#float 	ui:spnPercentage	default:100.0
		startHour 	type:#integer 	ui:spnStartHour		default:0
		endHour 	type:#integer 	ui:spnEndHour		default:23
	)
	
	fn pushAttrUpdate =
	(
		if theNode == undefined then theNode = (refs.dependents this)[1]
		
		if theNode != undefined then
		(
			this.tcName 	=	getAttr theNode gtaAttr_Name
			this.range		=	getAttr theNode gtaAttr_Range
			this.percentage =	getAttr theNode gtaAttr_Percentage
			this.startHour	=	getAttr theNode gtaAttr_StartHour
			this.endHour	=	getAttr theNode gtaAttr_EndHour
		)
	)
	
	--////////////////////////////////////////////////////////////////////////////////////////////////////
	-- USER INTER FACE
	--////////////////////////////////////////////////////////////////////////////////////////////////////
	
	rollout TCSphereUI "TC Sphere Settings"
	(
		group "TC Mod Name : "
		(
			edittext edtName ""  width:140 offset:[-4,0]
		)
		group "Quick Set : "
		(
			dropdownlist ddlNameDefs
		)
		group "Settings : "
		(
			spinner spnRadius "Radius : " type:#float val:this.radius range:[0.1, 999, 1]
			spinner spnRange "Range : " type:#float default:0.25 range:[0.1, 1000, 1.0]
			spinner spnPercentage "Percentage : " type:#float
			spinner spnStartHour "Start Hour : " type:#integer range:[0, 23, 0]
			spinner spnEndHour "End Hour : " type:#integer range:[0, 23, 23]
		)
		
		group "Viewport : "
		(
			checkbox cbx_display_name "Display Name and Values" checked:gGtaTimeCycleSphere_DisplayNames
		)
		
		
		--////////////////////////////////////////////////////////////////////////////////////////////////////
		-- FUNCTIONS
		--////////////////////////////////////////////////////////////////////////////////////////////////////
		
		--//////////////////////////////////////////////////
		-- Update Attrs
		--//////////////////////////////////////////////////
		fn updateAttrs attr: =
		(			
			case attr of
			(
				#tcname:setAttr theNode gtaAttr_Name this.tcName
				#range:setAttr theNode gtaAttr_Range this.range
				#percent:setAttr theNode gtaAttr_Percentage this.percentage
				#starthour:setAttr theNode gtaAttr_StartHour this.startHour
				#endhour:setAttr theNode gtaAttr_EndHour this.endHour
				
				default:
				(
					setAttr theNode gtaAttr_Name this.tcName
					setAttr theNode gtaAttr_Range this.range
					setAttr theNode gtaAttr_Percentage this.percentage
					setAttr theNode gtaAttr_StartHour this.startHour
					setAttr theNode gtaAttr_EndHour this.endHour
				)
			)
		)
		
		--////////////////////////////////////////////////////////////////////////////////////////////////////
		--EVENTS
		--////////////////////////////////////////////////////////////////////////////////////////////////////
		
		--//////////////////////////////////////////////////
		-- Name
		--//////////////////////////////////////////////////
		on edtName changed arg do
		(
			updateAttrs attr:#tcname
		)
		-- Push the dropdown info on enter press, back to 1st for blank name
		on edtName entered arg do
		(
			idx = (findItem ddlNameDefs.items arg)
			if idx == 0 do idx = 1
			ddlNameDefs.selection=idx
			ddlNameDefs.tooltip = gGtaTimeCycleSphere_NameDescriptions[ddlNameDefs.selection]
			updateAttrs attr:#tcname
		)
		
		--//////////////////////////////////////////////////
		-- TC item
		--//////////////////////////////////////////////////
		fn ddlChanged arg =
		(
			edtName.text = ddlNameDefs.selected			
			ddlNameDefs.tooltip = gGtaTimeCycleSphere_NameDescriptions[ddlNameDefs.selection]
			ddlIndex = ddlNameDefs.selection			
			updateAttrs attr:#tcname
		)
		
		on ddlNameDefs selected arg do ddlChanged arg
		
		--//////////////////////////////////////////////////
		-- Radius
		--//////////////////////////////////////////////////
		on spnRadius changed val do
		(
			this.delegate.radius = val
		)
		
		--//////////////////////////////////////////////////
		-- Range
		--//////////////////////////////////////////////////
		on spnRange changed val do
		(
			updateAttrs attr:#range
		)
		
		--//////////////////////////////////////////////////
		-- Percentage
		--//////////////////////////////////////////////////
		on spnPercentage changed val do
		(
			updateAttrs attr:#percent
		)
		
		--//////////////////////////////////////////////////
		-- Starthour
		--//////////////////////////////////////////////////
		on spnStartHour changed val do
		(
			updateAttrs attr:#starthour
		)
		
		--//////////////////////////////////////////////////
		-- EndHour
		--//////////////////////////////////////////////////
		on spnStartEnd changed val do
		(
			updateAttrs attr:#endhour
		)
		
		--//////////////////////////////////////////////////
		-- Display names
		--//////////////////////////////////////////////////
		on cbx_display_name changed arg do
		(
			gGtaTimeCycleSphere_DisplayNames = arg
			CompleteRedraw()
		)
		
		--//////////////////////////////////////////////////
		-- Open
		--//////////////////////////////////////////////////
		on TCSphereUI open do
		(
			pushAttrUpdate()
			
			spnRadius.value = this.delegate.radius
			
			--Sync the name defs if we need to
			if gGtaTimeCycleSphere_NameDefs == undefined then getSphericalTCNameDefs()
			ddlNameDefs.items = gGtaTimeCycleSphere_NameDefs
			
			theNode = (refs.dependents this)[1]
			if theNode != undefined then
			(
				-- Set the radius if the user draws a spher
				usrRadius = theNode.SphereGizmo.radius
				local defLookup = findItem gGtaTimeCycleSphere_NameDefs edtName.text
				if defLookup != 0 then
				(
					ddlNameDefs.selection = defLookup
					ddlNameDefs.tooltip = gGtaTimeCycleSphere_NameDescriptions[ddlNameDefs.selection]
				)
				
				updateAttrs()
			)
			
			cbx_display_name.checked = if (gGtaTimeCycleSphere_DisplayNames == undefined) then false else gGtaTimeCycleSphere_DisplayNames
		)
	)
	
	--//////////////////////////////////////////////////
	-- Draw mesh
	--//////////////////////////////////////////////////
	on getDisplayMesh do
	(
		local theNode = (refs.dependents this)[1]

		gw.setTransform(Matrix3 1)
		local textPos = gw.hTransPoint theNode.pos	
		local textPos2 
		
		-- NITROS : sends back textPos as undefined and so you can't add 12 to it.
		-- Could use this .. NitrousGraphicsManager.IsEnabled()
		if undefined != textPos do textPos2 = textPos + [0, 12, 0] 
			
		centreSelector = manip.makeSphere [0,0,0] 0.5 4
		
		meshCacheRadius = RsGWPrimMgr.getPrim()
		meshCacheRadius.IRsGWPrim.setresolution 20
		meshCacheRadius.IRsGWPrim.setOrigin theNode.transform
	
		meshCacheRadius.IRsGWPrim.addSphere this.delegate.radius
		meshCacheRadius.IRsGWPrim.setColour radiusColour
		meshCacheRadius.IRsGWPrim.draw true
		
		meshCacheRadius.IRsGWPrim.addSphere (this.delegate.radius + this.range)
		meshCacheRadius.IRsGWPrim.setColour rangeColour
		meshCacheRadius.IRsGWPrim.draw false
		
		--radiusSphere
		local vpScale = 1
		
		if viewport.getType() != undefined then
		(
			if viewport.IsPerspView() then
			(
				if theNode != undefined then
				(
					local eyePos = (Inverse(getViewTm()))[4]
					vpScale = (length (theNode.pos - eyePos))/30
					vpScale *= viewport.getFOV()/50
				)
			)
			else vpScale = viewport.getFOV()/3
			
			scale centreSelector [vpScale,vpScale,vpScale]
		)	
		
		
		if (gGtaTimeCycleSphere_DisplayNames != undefined) do
		(
			if (gGtaTimeCycleSphere_DisplayNames) do gw.hText textPos ((this.percentage as String) + "%") color:green
			if (gGtaTimeCycleSphere_DisplayNames) do gw.hText textPos2 (this.tcName) color:yellow	
		)		
		
		return centreSelector
	)
	
	on clone original do
	(
		this.tcName = original.tcName
	)
	
	on load do
	(
		this.usrRadius = this.delegate.radius
	)
	
	on update do
	(
		pushAttrUpdate()
	)
	
)
