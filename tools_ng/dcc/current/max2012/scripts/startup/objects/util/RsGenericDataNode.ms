-----------------------------------------------------------------------------
-- Generic data-storage node
-----------------------------------------------------------------------------
-- Minimal object-definition intended to store xml/node-list data to 
--	Maxfiles/containers
--
-- Neal D Corbett (R* Leeds) 11/2012
-----------------------------------------------------------------------------

plugin helper RsGenericDataNode
	name:"_RsGenericDataNode_"
	category:"RS Objects"
	classID:#(0x6fc003fc, 0x5b80487c)
	extends:dummy
	invisible:True
	version:3
(
	parameters main rollout:mainRoll
	(
		-- Used to tell different datanode-types apart:
		dataClass type:#string
		
		-- Stores links to nodes:
		nodeRefs type:#maxObjectTab tabSize:0 tabSizeVariable:true
		
		-- Store meshes, materials:
		meshObjs type:#maxObjectTab tabSize:0 tabSizeVariable:true
		materials type:#materialTab tabSize:0 tabSizeVariable:true
		
		dataString type:#string default:""
		dataVersion type:#integer default:1 animatable:false
		
		-- OBSOLETE, replaced by nodeRefs --
		--	(strongly-linked node-refs cause major slowdowns as they cause lots of event-notifications)
		nodeList type:#nodeTab tabSizeVariable:true
	)

	-- Store/retrieve objs array as weak-referenced nodeRefs:	
	fn getNodeList = 
	(
		for item in nodeRefs collect 
		(
			item.node
		)
	)
	fn setNodeList objs = 
	(
		-- Store nodeTransformMonitor instead of object-node, 
		--	otherwise this storage-node would be bombarded with event-notifications every time that obj is changed
		nodeRefs = for obj in objs collect 
		(
			nodeTransformMonitor node:obj forwardTransformChangeMsgs:False
		)
	)
	
	on update do 
	(
		-- Convert strongly-linked node-references from early versions of this object:
		if (version == 1) do 
		(
			setNodeList nodeList
			nodeList.count = 0
		)
	)
	
	rollout mainRoll "R* Data Node" 
	(
		label lblDataClass "Class: [not set]"
		button btnShowData "Show Data" width:140
		button btnSelObjs "Select object-list" width:140 offset:[-1, -2]
		
		on btnShowData pressed do 
		(
			local txtFilename = "$temp/temp.txt"
			deleteFile txtFilename
			local txtFile = createFile txtFilename
			
			format "DataClass:\n  %\n\n" DataClass to:txtFile
			format "DataVersion: %\n\n" DataVersion to:txtFile
			
			format "Counts: NodeRefs: %  MeshObjs:%  Materials:%\n\n" NodeRefs.Count MeshObjs.Count Materials.Count to:txtFile
			
			format "NodeRefs:\n" to:txtFile
			for n = 1 to nodeRefs.count do 
			(
				local obj = nodeRefs[n].node
				format "  %: %\n" n (if (isValidNode obj) then obj.name else (obj as string)) to:txtFile
			)
			format "\n" to:txtFile
			
			format "DataString:\n  %\n\n" dataString to:txtFile
			
			close txtFile
			
			edit txtFilename
		)
		
		on btnSelObjs pressed do 
		(
			undo "Select" on 
			(
				local selObjs = for item in nodeRefs where (isValidNode item.node) collect item.node
				if (selObjs.count == 0) then (clearSelection()) else (select selObjs)
			)
		)
		
		on mainRoll open do 
		(
			if (dataClass != undefined) do 
			(
				lblDataClass.text = ("Class: " + dataClass)
			)
		)
	)
)


-----------------------------------------------------------------------------
-- OBSOLETE!!!  auto-replaced with RsGenericDataNode
-----------------------------------------------------------------------------
plugin helper RsLodCombinerDataNode
	name:"_RsLodCombinerData_"
	category:"RS Objects"
	classID:#(0x6af374c5, 0x5ed17e0e)
	extends:dummy
	invisible:True
	version:1
(
	parameters main 
	(
		groupsXML default:"" type:#string
		objsList type:#nodeTab tabSize:0 tabSizeVariable:true
		nodeRefs type:#maxObjectTab tabSize:0 tabSizeVariable:true
	)

	-- Replace this old node-type with generic datanode.
	-- If object is in a closed container, just convert the objsList nodeTab-array
	on update do 
	(
		undo off 
		(
			-- Convert nodeList array to weak-referenced nodeRefs:
			if (objsList.count != 0) do 
			(
				nodeRefs = for obj in objsList collect 
				(
					nodeTransformMonitor node:obj forwardTransformChangeMsgs:False
				)
				objsList.count = 0
			)
			
			local thisNode = (refs.dependents This)[1]
			local doReplace = false
			
			if (isValidNode thisNode) do 
			(
				local parentNode = thisNode
				local closedCont = False
				
				while not closedCont and (parentNode != undefined) do 
				(
					parentNode = parentNode.parent
					closedCont = (isKindOf parentNode MAXRootNode)
				)
				
				if not closedCont do 
				(
					doReplace = True
				)
			)
			
			if (doReplace) do 
			(
				format "Upgrading ComboLodder data-node: % (% node listed)\n" thisNode.name nodeRefs.count
				
				local newNode = RsGenericDataNode pos:thisNode.pos dataClass:"ComboLodder" name:(uniqueName "_RsLodCombinerData_") isHidden:true isFrozen:true
				
				-- Copy data:
				if (groupsXML != undefined) do 
				(
					newNode.dataString = groupsXML
				)
				newNode.nodeRefs = (nodeRefs as array)
				
				instanceReplace thisNode newNode
				delete newNode
			)
		)
	)
)
