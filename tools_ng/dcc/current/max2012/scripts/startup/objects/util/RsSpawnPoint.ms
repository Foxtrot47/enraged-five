-- SpawnPoints

(
	global RsRefSpawnPointRerun -- Undefined if script hasn't been run before
	
	unregisterRedrawViewsCallback try (RsRefSlaveSpawns.postDelete) catch ()
	unregisterRedrawViewsCallback try (RsRefSlaveSpawns.fixUnparented) catch ()
	callbacks.removeScripts id:#RsRefSlaveSpawns
	deleteAllChangeHandlers id:#RsRefSlaveSpawns

	global RsSpawnTypesList = undefined
	global RsSpawnModelSetList = undefined
	global RsSpawnGroupList = undefined
	global idxSpawnPointModelSet = getattrindex "Gta SpawnPoint" "Model Set"
	
	-- These values are replaced with meshes when loaded:
	global RsSpawnPointModel_Seat = "$scripts/startup/objects/util/RsSpawnPoint_seat.txt"
	global RsSpawnPointModel_Stand = "$scripts/startup/objects/util/RsSpawnPoint_stand.txt"
	
	-- This struct is initialised after RsSpawnPointInstance is defined:
	global RsRefSlaveSpawns = undefined
	
	RSrefData.updateSlavesActive = false
	
	OK
)

-- AJM: Added for url:bugstar:179950 - Populate spawn points from data in scenarios.meta
fn RsSpawnPointLoadTypesFromMeta = 
(
	RsSpawnTypesList = #()
	local ItemTypeName = "CScenarioPlayAnimsInfo"
	
	local MetaDataManager = RsGetMetaDataManager()
	local spawnPointsFileName = ( (RsConfigGetCommonDir Core:True) + "data/ai/scenarios.meta" )

	if ( doesFileExist spawnPointsFileName == false ) then 
	(
		print ( "The scenario meta file doesn't exist!: " + spawnPointsFileName )
		return false
	)
	
	pushPrompt "Loading spawnpoint model set metadata"
	
	MetaDataManager.LoadWithMetaFile spawnPointsFileName
	local scenariosList = MetaDataManager.FindFirstStructureNamed "Scenarios"

	if ( scenariosList.length != 0 ) do
	(
		for i = 1 to scenariosList.length do 
		(		
			local scenarioName
			scenarioItem = scenariosList.Item(i-1)			
			try ( 
			
				if (scenarioItem.Value.Name == ItemTypeName ) do
				(
					scenarioName = scenarioItem.Value.Item["Name"].Value
				)			
			)	catch ( format "Error with load of scenario name from meta file\n" )
			
			-- Find anything with prop_ at the start and add to list to use for drop-down list
			if ( scenarioName != undefined and matchpattern scenarioName pattern:"PROP_*" ) do
			(				
				append RsSpawnTypesList scenarioName
			)
		)	
	)	
	
	-- Load the ScenarioTypeGroups too, for bugstar:771994, which are in a seperate place at the end
	-- of scenarios.meta (the Types within a ScenarioTypeGroup are already all picked up previously so we
	-- just get the actual group name here and add it to the list).
	local scenarioTypeGroupList = MetaDataManager.FindFirstStructureNamed "ScenarioTypeGroups"

	if ( scenarioTypeGroupList.length != 0 ) do
	(
		for i = 1 to scenarioTypeGroupList.length do 
		(		
			local scenarioTypeGroupName
			scenarioTypeGroupItem = scenarioTypeGroupList.Item(i-1)			
			try ( 

				scenarioTypeGroupName = scenarioTypeGroupItem.Item["Name"].Value	
			)	catch ( format "Error with load of scenario type group name from meta file\n" )

			-- Find anything with prop_ at the start and add to list to use for drop-down list
			if ( scenarioTypeGroupName != undefined and matchpattern scenarioTypeGroupName pattern:"PROP_*" ) do
			(				
				append RsSpawnTypesList scenarioTypeGroupName
			)
		)	
	)
		
	sort RsSpawnTypesList
	
	popPrompt()
)

fn RsSpawnPointLoadGroupsFromMeta = 
(
	local MetaDataManager = RsGetMetaDataManager()
	
	-- Hacky path-getter for metafile - dev/dev_ng are different:
	local spManifestFileName
	local CoreProjName = (RsConfigGetProjectName Core:True)

	case gRsCoreBranch.name of 
	(
		"dev_ng":
		(
			spManifestFileName = (RsConfigGetExportDir Core:True) + "levels/" + CoreProjName + "/sp_manifest.pso.meta"
		)
		"dev_gen9":
		(
			spManifestFileName = (RsConfigGetExportDir Core:True) + "levels/" + CoreProjName + "/sp_manifest.pso.meta"
		)
		"dev":
		(
			spManifestFileName = (RsConfigGetCommonDir Core:True) + "data/levels/" + CoreProjName + "/sp_manifest.meta"
		)
		Default:
		(
			gRsUlog.LogError ("gRsCoreBranch.name was not 'dev', 'dev_ng' or 'dev_gen9'.")
			return False
		)
	)

	if (not doesFileExist spManifestFileName) do 
	(
		format "The sp_manifest metafile doesn't exist!: %\n" spManifestFileName
		return false
	)
	
	pushPrompt "Loading spawnpoint group metadata"
	RsSpawnGroupList = #()
	
	try (
		MetaDataManager.LoadWithMetaFile spManifestFileName
	) catch
	(
		gRSULog.LogError "Error loading the sp_manifest meta file"
		gRsULog.validate()
		return false
	)
	local groupsList = MetaDataManager.FindFirstStructureNamed "Groups"
	
	for i = 1 to groupsList.items.count do 
	(		
		local groupName
		groupItem = groupsList.Item(i-1)			
		try ( 
				groupName = groupItem.Item["Name"].Value		
		) catch ( format "Error with load of group names from meta file\n" )

		if ( groupName != undefined ) do
		(				
			appendIfUnique RsSpawnGroupList groupName
		)
	)		
	popPrompt()
	sort RsSpawnGroupList
	-- Add default/no override option as empty string to start of list
	insertItem "" RsSpawnGroupList 1
)

-- Load the model sets entries from the meta file for the drop down list
fn RsSpawnPointLoadModelSetsFromMeta = 
(
	RsSpawnModelSetList = #()

	local modelSetsFileName = ( (RsConfigGetCommonDir Core:True) + "data/ai/ambientpedmodelsets.meta" )	
	if (not doesFileExist modelSetsFileName) do 
	(
		format "The ambientpedmodelsets meta file doesn't exist!: %\n" modelSetsFileName
		return False
	)
	
	pushPrompt "Loading spawnpoint model sets metadata"

	local MetaDataManager = RsGetMetaDataManager()
	MetaDataManager.LoadWithMetaFile modelSetsFileName
	local modelSetsList = MetaDataManager.FindFirstStructureNamed "ModelSets"

	if (modelSetsList == undefined) do 
	(
		popPrompt()
		gRsUlog.LogError ("Error resolving ModelSets member from metadata in file "+modelSetsFileName as string) 
		return false
	)

	for i = 1 to modelSetsList.items.count do 
	(		
		local modelSetName
		modelSetItem = modelSetsList.Item(i-1)			
		try 
		( 
			modelSetName = modelSetItem.Item["Name"].Value		
		) 
		catch 
		(
			format "Error loading model-set names from metafile\n"
		)

		if ( modelSetName != undefined ) do
		(				
			appendIfUnique RsSpawnModelSetList modelSetName
		)
	)		
	-- AJM: This doesn't appear in the Meta file so added manually.  The ped type attribute isn't editable but is always set to
	-- "any", the model set can override this using the drop-down list.  
	appendIfUnique RsSpawnModelSetList "UsePopulation" 
	sort RsSpawnModelSetList
	
	-- Add default/no override option as empty string to start of list
	insertItem "" RsSpawnModelSetList 1
	
	popPrompt()
	
	return RsSpawnModelSetList
)

plugin Helper gtaSpawnPoint
	name:"Gta SpawnPoint"
	classID:#(0x5ae41793, 0x575c0170)
	category:"RS Helpers" 
	extends:dummy
	version:1
( 
	local meshObj, lastType
	local otherRollouts = #()

	-- Notify RsSpawnPointInstance objects of changes to this delegate:
	fn notifyNode param val =
	(
		local slaveInstance = (refs.dependents this)[1]
		if isKindOf slaveInstance ::RsSpawnPointInstance do 
		(
			slaveInstance.handleAttributeChange param
		)
	)
	
	parameters pblock rollout:params
	(
		spawnType type:#string animatable:false default:"PROP_HUMAN_SEAT_BENCH"
		groupType type:#string default:""
		
		on spawnType set val do
		(
			this.params.setLabel setType:val
			notifyNode "spawnType" val
		)
		
		on groupType set val do
		(
			notifyNode "groupType" val	
		)
	)

	rollout params "SpawnPoint Parameters"
	(
		group "SpawnPoint Type:" 
		(
			label lblSpawnType "SpawnPoint Type:" align:#left width:160 height:110 offset:[0,-4]
		)
		button btnChangeType "Change Spawn Type"
		dropdownlist ddlGroup "Group:"
		dropdownlist ddlModelSet "Model Set:"
		
		fn setLabel setType: = 
		(
			local labelString = stringStream ""
			
			if (setType == unsupplied) do 
			(
				setType = spawnType
			)
			
			local typeTokens = filterString setType "_"
			local tabs = ""
			
			for n = 1 to typeTokens.count do 
			(
				format "%%" tabs typeTokens[n] to:labelString
				
				if (n != typeTokens.count) do 
				(
					format "_\n" to:labelString
				)
				
				tabs += " "
			)
			
			lblSpawnType.text = (labelString as string)
		)
		
		on btnChangeType pressed do 
		(
			if ($ == undefined) then 
			( 
				messagebox "Unable to change type.  Please select a spawn point object." 
				return false
			)
				
			rollout RsSpawnPoint_ChangeTypeRoll "Select Spawnpoint Type:" width:360
			(
				listBox lstSpawnType "" height:10
				
				on lstSpawnType selected selNum do 
				(
					if (selNum != 0) do 
					(
						undo "change spawntype" on 
						(
							$.spawnType = lstSpawnType.selected
						)
					)
				)
				
				on RsSpawnPoint_ChangeTypeRoll open do 
				(
					if (RsSpawnTypesList == undefined) do 
					(
						lstSpawnType.items = #("Please wait, loading metadata...")
						RsSpawnPointLoadTypesFromMeta()
					)
					
					if (RsSpawnTypesList == undefined) then 
					(
						lstSpawnType.items = #("FAILED TO LOAD METADATA!")
					)
					else 
					(
						lstSpawnType.items = RsSpawnTypesList
						
						local spawnType = $.spawnType
						local setSel = 0
						for i = 1 to lstSpawnType.items.count while (setSel == 0) do 
						(
							itemName = lstSpawnType.items[i]
							if (matchPattern itemName pattern:spawnType) do (setSel = i)
						)
						
						lstSpawnType.selection = setSel
					)
				)
			)
			
			createDialog RsSpawnPoint_ChangeTypeRoll modal:true
		)
		
		fn removeRolls = 
		(
			for roll in otherRollouts do 
			(
				removeRollout roll
			)
		)
		
		fn addRollouts = 
		(
			removeRolls()
			
			if not (isCreatingObject() or ($ == undefined)) do 
			(
				otherRollouts = RsCreateAttrRoll $ propClassName:"Gta SpawnPoint"
				
				--addRollout RsTimeToolsRoll
				for roll in otherRollouts do 
				(
					addRollout roll
				)
				--append otherRollouts RsTimeToolsRoll
			)
		)
		
		on ddlGroup selected arg do
		(
			groupType = ddlGroup.selected			
		)
		
		on ddlModelSet selected arg do
		(
			for selObj in selection do
			(
				if (getattrclass selObj == "Gta SpawnPoint") do 
				(
					setattr selObj idxSpawnPointModelSet ddlModelSet.selected
					if (isProperty selObj #isSlave) do (selObj.notifyNode "Model Set" ddlModelSet.selected)
				)
			)
		)
		
		on params open do 
		(
			setLabel()
			addRollouts()
			
			-- Load meta file for Model Set options if not done so
			if ( RsSpawnModelSetList == undefined ) do
			(
				RsSpawnPointLoadModelSetsFromMeta()
			)
			ddlModelSet.items = RsSpawnModelSetList	
			
			-- Load meta file for Groups parameter if not already done so
			if ( RsSpawnGroupList == undefined ) do
			(
				RsSpawnPointLoadGroupsFromMeta()
			)
			
			-- Add items to list, unless meta failed to load:
			if ( RsSpawnGroupList != undefined ) do
			(
				ddlGroup.items = RsSpawnGroupList				
			)
			
			local setSel = 0
			local groupSel = 0
			local selObj = $
			if (getattrclass selObj == "Gta SpawnPoint") then
			(
				local modelSetType = getattr selObj idxSpawnPointModelSet				
				for i = 1 to ddlModelSet.items.count while (setSel == 0) do 
				(
					itemName = ddlModelSet.items[i]
					if (matchPattern itemName pattern:modelSetType) do (setSel = i)
				)
				for i = 1 to ddlGroup.items.count while (groupSel == 0) do 
				(
					itemName = ddlGroup.items[i]
					if (matchPattern itemName pattern:groupType) do (groupSel = i)
				)
			)
			else
			(
				setSel = 1
				groupSel = 1
			)
			ddlModelSet.selection = setSel
			ddlGroup.selection = groupSel
		)
		
		on params close do 
		(
			removeRolls()
		)
	)

	fn loadModel dataFilename = 
	(
		local dataFile = openFile dataFilename
		
		if (dataFile == undefined) do return (createInstance box).mesh
		
		local dataArray = #()
		while not eof dataFile do 
		(
			local lineVal = execute (readLine dataFile)
			
			if isKindOf lineVal array do 
			(
				append dataArray lineVal
			)
		)

		local vp = dataArray[1]
		local fl = dataArray[2]
		local fe = dataArray[4]
		
		local newMesh = (createInstance Editable_Mesh).mesh
		setmesh newMesh vertices:vp faces:fl
		
		-- Set edge visibility:
		for i = 1 to fe.count do (for j = 1 to 3 do (setEdgeVis newMesh i j fe[i][j]))
			
		return newMesh
	)

	on update do 
	(
		meshObj = undefined
		lastType = undefined
	)
	
	on getDisplayMesh do 
	(
		if (meshObj == undefined) or (lastType != spawnType) do 
		(
			local useModel = case of 
			(
				(matchPattern spawnType pattern:"*Seat_*"):(&RsSpawnPointModel_Seat)
				Default:(&RsSpawnPointModel_Stand)
			)
			
			if isKindOf (*useModel) string do 
			(
				*useModel = loadModel (*useModel)
			)
			
			meshObj = copy (*useModel)
			lastType = spawnType
		)
		
		copy meshObj
	)
	
	tool create 
	( 
		on mousePoint click do 
		(
			nodeTM.translation = gridPoint
			
			-- Apply values and jittering from Browser:
			--	Does nothing if tool is closed, or isn't in matching object-mode.
			local ThisNode = (refs.dependentNodes This)[1]
			::RSrefBrowserRoll.SetupNewObj ThisNode nodeXform:nodeTM
			
			#stop
		)
	) 
)

plugin Helper RsSpawnPointInstance
	name:"RsSpawnPointInstance"
	classID:#(0x408a2962, 0x6065f7d3)
	category:"RS Helpers" 
	extends:gtaSpawnPoint
	autoPromoteDelegateProps:true
	invisible:true
	version:1
(
	local isSlave -- Lets functions know that this is a standard slave-object via isProperty
	
	local parentNode, parentTransform
	local originalXform, offsetXform
	local transformHandler, deleteHandler, paramHandler
	
	local slaveStruct
	local originalInst, actualOriginalAttribVals
	local originalName = ""
	
	parameters RsRefSlaveParams rollout:RsRefSlaveInstanceRollout
	(
		allChangedValueArray type:#stringtab tabSizeVariable:true

		originalPropValueArray type:#stringtab tabSizeVariable:true
		instancePropNameArray type:#stringtab tabSizeVariable:true
		
		originalAttributeValueArray type:#stringtab tabSizeVariable:true
		instanceAttributeNameArray type:#stringtab tabSizeVariable:true
		
		refHandle type:#string
	)
	
	fn updateColour theNode:undefined = 
	(
		if theNode == undefined do
		(
			local refsArray = (refs.dependents this)
			for i in refsArray while theNode == undefined do 
			(
				if (isvalidnode i) and (isKindof i RsSpawnPointInstance) then
				(
					theNode = i
				)
			)
		)
		local col = if (allChangedValueArray.count == 0) then blue else red
		theNode.wirecolor = col
	)
	
	-- THIS ROLLOUT SHOULD BE KEPT IN-SYNC WITH OTHER INSTANCES OF RsRefSlaveInstanceRollout --
	rollout RsRefSlaveInstanceRollout "Instance Properties:"
	(
		label lblSrcName "Source Object:" align:#left across:2
		hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/RSref_Slave_Objects" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)	
		
		editText txtSrcName "" text:"unknown" readOnly:true offset:[-3,-3]
		
		multilistbox instanceAttributesListBox "Edited Attributes:" offset:[0,-2]
		button resetValButton "Reset selected values" width:120
		button resetValsButton "Reset all" width:120
		
		label lblGuid "GUID:" align:#left
		editText txtGuid "" text:"unknown" readOnly:true height:40 offset:[-3,-4]
		
		fn updateEditsRoll = 
		(
			-- Make sure RsRef database is active:
			if RsRefData.notLoaded do 
			(
				RsRefFuncs.databaseActive toolName:"Slave-object system" debugFuncSrc:"[updateEditsRoll]"
				
				if not RsRefData.notLoaded do 
				(
					select selection
				)
			)
			
			if not RsRefSlaveInstanceRollout.open do return false
			
			if (originalInst != undefined) do (txtSrcName.text = originalInst.originalName)
			
			txtGuid.text = RsWordWrap (refHandle as string) 130 delimiters:#("-", " ")

			local itemList = for item in allChangedValueArray collect item
			sort itemList
			
			local noChange = (itemList.count == instanceAttributesListBox.items.count)
			for n = 1 to itemList.count while noChange do 
			(
				noChange = (instanceAttributesListBox.items[n] == itemList[n])
			)
			
			-- Don't change display-list if it's not changed:
			if not noChange do (instanceAttributesListBox.items = itemList)
 			updateColour()
		)
		
		on resetValButton pressed do 
		(
			for objIndex in instanceAttributesListBox.selection do
			(
				local attrName = instanceAttributesListBox.items[objIndex]
				RsRefSlaveSpawns.resetInstanceChange this attrName updateAttrs:false
			)
			RsRefSlaveSpawns.updateUnchangedAttribs this
			
			updateEditsRoll()
			select selection
		)
		
		on resetValsButton pressed do 
		(
			RsRefSlavesAllowParamHandler = false
			instancePropNameArray = #()
			instanceAttributeNameArray = #()
			allChangedValueArray = #()
			RsRefSlavesAllowParamHandler = true
			
			RsRefSlaveSpawns.updateUnchangedAttribs this
			
			updateEditsRoll()
			select selection
		)
			
		on RsRefSlaveInstanceRollout open do 
		(
			updateEditsRoll()
		)
		on RsRefSlaveInstanceRollout rolledUp down do (updateEditsRoll())
	)

	fn xformHandlerFunc = RsRefSlaveSpawns.xformHandlerFunc this
	fn createChangeHandler = RsRefSlaveSpawns.createChangeHandler this
	fn deleteHandlerFunc = RsRefSlaveSpawns.deleteHandlerFunc this
	fn updateUnchangedAttribs theNode: = RsRefSlaveSpawns.updateUnchangedAttribs this theNode:theNode
	fn handleAttributeChange attrName = RsRefSlaveSpawns.handleAttributeChange this attrName
	
	-- Called by createChangeHandler, if handlers are required:
	fn doSetupCallbacks theNode = 
	(
		-- Set up transform/deletion handlers:
		transformHandler = when transform theNode changes id:#RsRefSlaveSpawns do xformHandlerFunc()
		deleteHandler = when theNode deleted id:#RsRefSlaveSpawns do deleteHandlerFunc()
	)
	
	on postLoad do createChangeHandler()
)

------------------------------------------
-- Set up RsRefSlaveSpawns struct: --
------------------------------------------
(
	fn xmlObjFilter xmlNode = (xmlNode.class == "gta_spawnpoint")

	RsRefSlaveSpawns = RsRefSlaveTypeStruct \
		slaveClass:RsSpawnPointInstance \
		slaveAttrClass:"Gta SpawnPoint" \
		xmlObjFilter:xmlObjFilter \
		objFlagName:#showSpawns \
		refFlagName:#hasScenario \
		slaveArrayName:#spawnPoints
	RsRefSlaveSpawns.init()
	
	-- CALLBACKS --
	callbacks.addScript #nodePreDelete "RsRefSlaveSpawns.preDelete (callbacks.notificationParam())" id:#RsRefSlaveSpawns
)

-- Update slave-objects on script-rerun:
if (RsRefSpawnPointRerun != undefined) do 
(
	RsRefData.refs.spawnpoints = undefined
	RsRefSlaveTypeStruct.updateAllSlaves()
)
RsRefSpawnPointRerun = true

OK
