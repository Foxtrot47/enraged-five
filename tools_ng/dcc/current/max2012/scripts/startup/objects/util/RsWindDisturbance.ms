-- Rockstar Decal effect
-- Rockstar North
-- 14/10/2011
-- by Adam Munson

plugin helper RsWindDisturbance
	name:"RsWindDisturbance"
	classID:#(0x6ff74bc4, 0x51843bfb)
	category:"RS Helpers"
	extends:Dummy
( 
	local meshObj
	
	parameters main rollout:params
	(
		windSize type:#worldunits ui:spnSize default:1.0
	)

	rollout params "Wind Disturbance Parameters"
	(
		spinner spnSize "Size" type:#worldUnits
	)
	
	on getDisplayMesh do
	(
		if ( meshObj == undefined ) do
		(
			meshObj = createInstance sphere
			meshObj.hemisphere = 0.5
			meshObj.Segs = 16
		)
		meshObj.radius = windSize
		theMesh = meshObj.mesh
		
		--The game renders these helpers down the local x axis rather then z so we need to rotate through Y by 90� - JWR
		maxToGameTransform = rotateYMatrix 90.0
		for vi=1 to theMesh.verts.count do setvert theMesh vi ((getvert theMesh vi)*maxToGameTransform)
		
		theMesh
	)
	
	on update do
	(
		meshObj = undefined
	)
	
	tool create
	(
		on mousePoint click do
		(
			nodeTM.translation = gridPoint
			#stop			
		)
	)
)