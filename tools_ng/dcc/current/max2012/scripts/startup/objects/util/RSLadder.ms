global RSLadderTypesList = undefined

struct RSLadderType ( template, canSlide, rungSpacing, ladderWidth)

global RsLaddersFileName = ( RsConfigGetCommonDir() + "data/ai/Ladders.meta" )
global RsLaddersPscDir = RsConfigGetMetadataDefinitionDir() + "game/task/movement/climbing/ladders/"

-----------------------------------------------------------
-- class definition0
-----------------------------------------------------------	
plugin SimpleManipulator RSLadder
name:"RSLadder"
classID:#(0x15812f6e, 0x69342e2a)
category:"RS Helpers"
(
	local helperBoxHeight = 1.8
	
  	parameters main rollout:params
  	(
    	template			type:#string		ui:lstType 			default:"DEFAULT"
		rungSpacing 		type:#worldUnits	ui:spnRungSpacing 	default:0.25
		ladderWidth		type:#worldUnits	ui:spnWidth 		default:0.5
		canSlide				type:#boolean 	ui:chkCanSlide 	default:true
		ladderHeight 		type:#worldUnits	ui:spnHeight		default:3.0
		useOptionalRung	type:#boolean		ui:chkOptionalRung	default:true
		bottomNodePosZ	type:#worldUnits							default:0.0
		materialType 	type:#integer	ui:lstMaterialType	default:1
	)
	
	rollout params "Ladder Parameters"
	(
		--Help Header
		button btnLogo tooltip:"Feedback" border:false align:#left offset:[0, 2] images:#("RockStarNorthLogo.bmp", "RockStarNorthLogo_Mask.bmp", 1, 1, 1, 1, 1, false) across:2
		button btnHelp tooltip:"Help?" border:false align:#right offset:[0, 2] images:#("HelpIcon_32.bmp", "HelpIcon_32a.bmp", 1, 1, 1, 1, 1, false)
		groupbox grpHeader width:150 height:50 pos:[4, 0]
		
		--main controls
		button 			btnRefreshLadderList 	"Refresh Ladder Types" 	width:120	
		dropdownlist 	lstType 				"Type:"					height:5
		spinner 		spnRungSpacing 			"Rung Spacing" 			type:#worldunits enabled:false
		spinner 		spnWidth 				"Width" 				type:#worldunits enabled:false
		checkbox 		chkCanSlide 			"Can Slide" 			enabled:false
		checkbox		chkOptionalRung			"Use Optional Rung"
		spinner			spnHeight				"Ladder Height"			type:#worldUnits
		dropdownlist 	lstMaterialType			"Material Type"					
		
		group "Create Rungs:"
		(
			spinner spnDivisions "Divisions" type:#integer range:[3, 12, 3]
			spinner spnRungRadius "Radius" type:#float range:[0.01, 0.04, 0.025]
			button btnCreateRungs "Create Rungs"
		)

		fn changeLadderType idx = (

			if ( RsLadderTypesList.count != 0 ) do
			(
				lstType.selection	= idx
				template 			= RSLadderTypesList[idx].template
				rungSpacing 		= RSLadderTypesList[idx].rungSpacing
				ladderWidth		= RSLadderTypesList[idx].ladderWidth
				canSlide 			= RSLadderTypesList[idx].canSlide
			)
		)

		-- This is used to find the where in the ladder array that this RSLadder object is
		-- rather than storing the index since it may change order		
		fn findTemplateIndex = (
		
			idx = 1
			found = false
			for ladIdx = 1 to RSLadderTypesList.count while found == false do
			(
				if ( template == RSLadderTypesList[ladIdx].template ) do
				(
					idx = ladIdx
					found = true
				)
			)
			idx
		)

		-- Loads the ladder types into the array, if items are missing from the 
		-- meta file, there are default values in this function to populate.
		fn loadTypesFromMeta = 
		(
			RSLadderTypesList = #()
			
			local MetaDataManager = ::RsGetMetaDataManager()
			
			if not doesFileExist RsLaddersFileName do 
			(
					print ( "The ladder meta file doesn't exist!: common/data/ai/Ladders.meta" )
					return false
			)

			MetaDataManager.LoadWithMetaFile RsLaddersFileName
			local ladderList = MetaDataManager.FindFirstStructureNamed "LadderData"
			
			if ( ladderList.length != 0 ) do
			(
				for i = 1 to ladderList.length do 
				(
					-- Default values
					local template 			= ""
					local ladderCanSlide 		= true
					local ladderRungSpacing 	= 0.25
					local ladderWidth 		= 0.5
					
					local ladderSpecs 			= ladderList.Item(i-1)
					try ( template 			= ladderSpecs.Item["Name"].Value )			catch ( print "Failed to load ladder 'Name' from meta file" )
					try ( ladderCanSlide 	= ladderSpecs.Item["CanSlide"].Value )		catch ( print "Failed to load ladder 'CanSlide' from meta file" )
					try ( ladderRungSpacing = ladderSpecs.Item["RungSpacing"].Value ) 	catch ( print "Failed to load ladder 'RungSpacing' from meta file" )	
					try ( ladderWidth 		= ladderSpecs.Item["Width"].Value )			catch ( print "Failed to load ladder 'Width' from meta file" )
					
					append RSLadderTypesList (RSLadderType template ladderCanSlide ladderRungSpacing ladderWidth)
				)	
			)
		)
		
		--help launch event
		on btnHelp pressed do shellLaunch "https://devstar.rockstargames.com/wiki/index.php/Ladders" ""
			
		on btnRefreshLadderList pressed do
		(
			gRsPerforce.sync #(RsLaddersFileName, (RsLaddersPscDir + "...")) silent:true
			
			if ( loadTypesFromMeta() != false ) do
			(
				lstType.items = for l in RSLadderTypesList collect l.template
				changeLadderType ( findTemplateIndex() )
			)
		)
		
		on lstType selected idx do
		(
			changeLadderType idx
		)
		
		on lstMaterialType selected idx do
		(
			materialType = idx
			print idx
			print materialType
			
		)
		
		on btnCreateRungs pressed do
		(
			undo "Create Rungs Geometry" on
			(
				local theLadder = ($selection)[1]
				if classOf theLadder != RsLadder then return false
				
				local topRungPos = theLadder.pos
				local numRungs = (theLadder.ladderHeight / theLadder.rungSpacing) as Integer
				local rungDivisions = spnDivisions.value as Integer
				local rungRadius = spnRungRadius.value
				local rungList = #()
				
				for r = 1 to numRungs do
				(
					local rungPos = theLadder.pos + [0, 0, theLadder.rungSpacing * -r]
					local theRung = Cylinder pos:rungPos radius:rungRadius height:theLadder.ladderWidth sides:rungDivisions mapCoords:true
					
					theRung.parent = theLadder
					
					local theLadderMx = theLadder.transform
					local rot = (AngleAxis 90.0 [0, 1, 0])
					in coordsys #parent rotate theRung rot
					in coordsys #parent move theRung [theRung.pos.x - (theLadder.ladderWidth/2), 0, 0]
					
					--turn to polymesh
					ConvertToPoly theRung
					
					--delete the caps
					local nfaces = polyop.getnumfaces theRung
					polyop.deleteFaces theRung #{nfaces-1, nfaces}
					
					--rotate the rung so face 1 points up
					theRung.parent = undefined
					local face1Norm = polyop.getfacenormal theRung 1 node:theRung
					local rotAngle = acos(dot face1Norm [0, 0, 1])
					
					--check the ladder local y-axis against the rung normal to determine direction
					if dot theLadder.transform.row2 (polyop.getfacenormal theRung 1 node:theRung) < 0.0 then rotAngle *= -1
					local rot = (angleAxis rotAngle theLadder.transform.row1)
					rotate theRung rot
					
					--randomize uv shell offset
					local uv_offset = [0.5 - (random 0.0 1.0), 0.5 - (random 0.0 1.0), 0.0]
					for mv = 1 to (polyop.getNumMapVerts theRung 1) do
					(
						polyop.setmapvert theRung 1 mv ((polyop.getmapvert theRung 1 mv ) + uv_offset)
					)
				)
			)
		)
		
		on params open do
		(
			if (RSLadderTypesList == undefined) do 
			(
				loadTypesFromMeta()
			)
			
			lstType.items = for l in RSLadderTypesList collect l.template
			
			lstType.selection = findTemplateIndex()
			lstMaterialType.items = #("Metal Solid", "Metal Light", "Wooden")
		)
	)

	fn createLadderHelper = (
		
		r = [1,0,0]
		g = [0,1,0]
		b = [0,0,1]
		y = [1,1,0]
		w = [1,1,1]		

		-- Some position constants
		ladderDimHalved 	= ladderWidth/2
		ptTopLeftLadder 	= [(-ladderDimHalved), 0, 0]
		ptTopRightLadder 	= [ladderDimHalved, 0, 0]
		ptBottomLeftLadder 	= [(-ladderDimHalved), 0, -(ladderHeight)]
		ptBottomRightLadder = [ladderDimHalved, 0, -(ladderHeight)]
		ptLadderHeightOffset= [0, 0, helperBoxHeight]
		ptLadderWidthOffset = [0, ladderWidth, 0]

		leftRail = manip.makeGizmoShape()
		leftRail.addPoint ptTopLeftLadder
		leftRail.addPoint ptBottomLeftLadder

		rightRail = manip.makeGizmoShape()
		rightRail.addPoint ptTopRightLadder
		rightRail.addPoint ptBottomRightLadder

		-- Create Top Box
		topBox = manip.makeGizmoShape()		
		-- Front side at top of ladder
		topBox.addPoint ( ptTopLeftLadder )
		topBox.addPoint ( ptTopRightLadder ) 
		topBox.addPoint ( ptTopRightLadder + 	ptLadderHeightOffset )
		topBox.addPoint ( ptTopLeftLadder + 	ptLadderHeightOffset )
		topBox.addPoint ( ptTopLeftLadder ) 

		-- Back side
		topBox.startNewLine()
		topBox.addPoint ( ptTopLeftLadder + 	ptLadderWidthOffset )
		topBox.addPoint ( ptTopLeftLadder + 	ptLadderWidthOffset + ptLadderHeightOffset )
		topBox.addPoint ( ptTopRightLadder + 	ptLadderWidthOffset + ptLadderHeightOffset ) 
		topBox.addPoint ( ptTopRightLadder + 	ptLadderWidthOffset )
		topBox.addPoint ( ptTopLeftLadder + 	ptLadderWidthOffset )

		-- Remaining lines of box joining front and back sides
		topBox.addPoint ( ptTopLeftLadder )
		topBox.startNewLine()
		topBox.addPoint ( ptTopRightLadder ) 
		topBox.addPoint ( ptTopRightLadder + 	ptLadderWidthOffset )
		topBox.startNewLine()
		topBox.addPoint ( ptTopLeftLadder + 	ptLadderHeightOffset )
		topBox.addPoint ( ptTopLeftLadder + 	ptLadderWidthOffset + ptLadderHeightOffset )
		topBox.startNewLine()
		topBox.addPoint ( ptTopRightLadder + 	ptLadderHeightOffset )
		topBox.addPoint ( ptTopRightLadder + 	ptLadderWidthOffset + ptLadderHeightOffset ) 

		--Create Bottom Box
		bottomBox = manip.makeGizmoShape()
		-- Bottom 3 lines, don't do a line at the ladder side otherwise might be mistaken
		-- for a rung position
		bottomBox.addPoint ( ptBottomLeftLadder ) 
		bottomBox.addPoint ( ptBottomLeftLadder - 	ptLadderWidthOffset ) 
		bottomBox.addPoint ( ptBottomRightLadder - ptLadderWidthOffset ) 
		bottomBox.addPoint ( ptBottomRightLadder ) 

		-- Top 3 lines
		bottomBox.startNewLine()
		bottomBox.addPoint ( ptBottomLeftLadder + 	ptLadderHeightOffset ) 
		bottomBox.addPoint ( ptBottomLeftLadder + 	ptLadderHeightOffset - ptLadderWidthOffset ) 
		bottomBox.addPoint ( ptBottomRightLadder + 	ptLadderHeightOffset - ptLadderWidthOffset ) 
		bottomBox.addPoint ( ptBottomRightLadder + 	ptLadderHeightOffset ) 

		-- Remaining vertical lines
		bottomBox.startNewLine()
		bottomBox.addPoint ( ptBottomLeftLadder + 	ptLadderHeightOffset - ptLadderWidthOffset )
		bottomBox.addPoint ( ptBottomLeftLadder - 	ptLadderWidthOffset ) 

		bottomBox.startNewLine()
		bottomBox.addPoint ( ptBottomRightLadder + 	ptLadderHeightOffset - ptLadderWidthOffset )
		bottomBox.addPoint ( ptBottomRightLadder - 	ptLadderWidthOffset )

		this.addGizmoShape leftRail 	0 r g
		this.addGizmoShape rightRail 	0 r g
		this.addGizmoShape topBox 		0 r g
		this.addGizmoShape bottomBox 	0 r g

		-- Draw the arrow to represent normal
		normalArrow = manip.makeGizmoShape()
		normalArrow.addPoint ( ptBottomLeftLadder - [0, ladderDimHalved, 0] )
		normalArrow.addPoint ( [0, -ladderWidth, -ladderHeight] )
		normalArrow.addPoint ( ptBottomRightLadder - [0, ladderDimHalved, 0] )		
		this.addGizmoShape normalArrow 0 w g 

		addRung = true
		rungPositionOffset = rungSpacing

		-- Draw the rungs, the bottom one is optional and coloured different
		while rungPositionOffset < ladderHeight do
		(				
			colour = r

			newRung = manip.makeGizmoShape()			
			newRung.addPoint ( ptTopLeftLadder - [0,0,rungPositionOffset] ) 
			newRung.addPoint ( ptTopRightLadder - [0,0,rungPositionOffset] ) 
			
			rungPositionOffset += rungSpacing
			-- If this is the last rung colour it yellow
			if ( rungPositionOffset >= ladderHeight ) then 
			(
					colour = y
					-- The next rung would be past the bottom of the ladder helper so change position of 
					-- bottom either to the yellow rung just drawn, or to the previous one above that
					-- if the parameter is not checked.
					if ( useOptionalRung ) then
					(
						bottomNodePosZ = -rungPositionOffset + rungSpacing
					)
					else
					(
						-- If not using the yellow optional rung then ladder bottom is the next rung up
						bottomNodePosZ = (-rungPositionOffset + (2 * rungSpacing))
					)
			)
			
			this.addGizmoShape newRung gizmoDontHitTest colour g
		)
	)
	

	on canManipulate target return (classOf target) == RsLadder	
	
	on updateGizmos do
	(		
		this.clearGizmos()
		createLadderHelper()
	) 	
	
	tool create
	(
    	on mousePoint click do
    	(
			nodeTM.translation = gridPoint
			#stop			
       	)
	)
)