-- Helper to define IMAP Group properties centralised in a scene

filein "RsObjectGroup.ms"

plugin Helper InteriorGroupHelper
extends:ObjectGroupHelper
name:"RS InteriorGroup"
classID:#(0x55c3a92b, 0x6f40be95)
replaceUI:false
category:"RS Helpers"
(

	parameters main rollout:InteriorGroupRoll
	(
		interiorGroupObjects type:#nodeTab tabSizeVariable:true
	)
	
	rollout mloParentRoll "Mlo Parent"
	(
		dropDownList ddl_mloParent
		-- EVENTS 
		on mloParentRoll open do -- FIND SET PARENT MLO
		(	
			mloItems = #("")
			join mloItems (for h in helpers where (classof h == Gta_MILO) collect h.name)
			ddl_mloParent.items = mloItems
			if ($.parent != undefined) do 
			(
				itemNumber = (findItem ddl_mloParent.items $.parent.name)
				if (itemNumber != 0) do ddl_mloParent.selection = itemNumber
			)
		)
		
		on ddl_mloParent selected arg do -- PARENT HELPER TO MLO
		(
			if (arg ==1 ) then $.parent = undefined
			else $.parent = (getnodebyname ddl_mloParent.items[arg]) 
		)
	)
	
	rollout InteriorGroupRoll "Object List"
	(
		hyperlink help "Help!" address:"https://devstar.rockstargames.com/wiki/index.php/RS_InteriorGroup"  color:[0,0,255] align:#right
		multilistbox lstInteriorGroupObjs "" width:135
		button btnAdd "Add" width:65 align:#left across:2
		button btnDelete "Remove" width:65 align:#right
		
		fn updateObjList = 
		(
			lstInteriorGroupObjs.items = for obj in interiorGroupObjects collect obj.name
			lstInteriorGroupObjs.selection = #{}
		)
		
		on InteriorGroupRoll open do
		(
			-- REMOVE UNDEFINED OBJECTS
			interiorGroupObjects = makeUniqueArray (for obj in interiorGroupObjects where (isValidNode obj) collect obj)
			
			-- REFRESH LIST
			updateObjList()
		)
		
		-- Select doubleclicked object:
		on lstInteriorGroupObjs doubleClicked num do 
		(
			local selObj = interiorGroupObjects[num]
			if (isValidNode selObj) do select selObj
		)
		
		on btnAdd pressed do
		(
			fn pickFilter obj = 
			(
				((isKindOf obj.baseObject Editable_Poly) or (isKindOf obj.baseObject Editable_Mesh) or (IsRefObj obj)) and ((findItem interiorGroupObjects obj) == 0)
			)
			
			fn sortObjs v1 v2 = 
			(
				striCmp v1.name v2.name
			)
			
			local newObjList = (for obj in interiorGroupObjects where isValidNode obj collect obj)
			local pickObj = 0
			
			while (pickObj != undefined) do 
			(
				pickObj = pickObject message:"Pick mesh objects" filter:pickFilter
				
				if (isValidNode pickObj) do 
				(
					append newObjList pickObj
					qsort newObjList sortObjs
					interiorGroupObjects = newObjList
					
					updateObjList()
				)
			)
		)
		
		on btnDelete pressed do
		(
			local newObjList = for idx = 1 to interiorGroupObjects.count where not lstInteriorGroupObjs.selection[idx] collect interiorGroupObjects[idx]
			interiorGroupObjects = newObjList
			
			updateObjList()
		)
	)
	
	rollout toolsRoll "Tools" 
	(
		button btnSelectGroup "Select Objects" width:135 align:#left
		button btnShowObjects "Show Objects" width:135 align:#left
		button btnHideObjects "Hide Objects" width:135 align:#left
		label lblGroupTXD "Group TXD Name:" width:135 align:#left
		edittext txtGroupTXD "" width:135 align:#left
		button btnSetObjectTXDs "Set Object TXDs" width:135 align:#left
		
		on btnSelectGroup pressed do
		(
			undo "Select Objects" on 
			(
				clearSelection()
				select (for obj in interiorGroupObjects where (isValidNode obj) collect obj)
			)
			completeRedraw()
		)
		
		on btnShowObjects pressed do 
		(
			undo "Show Objects" on 
			(
				for obj in interiorGroupObjects do 
				(
					unhide obj
				)
			)
			completeRedraw()
		)
		
		on btnHideObjects pressed do 
		(
			undo "Hide Objects" on 
			(
				for obj in interiorGroupObjects do 
				(
					hide obj
				)
			)
			completeRedraw()
		)
		
		on btnSetObjectTXDs pressed do 
		(
			idxTxd = GetAttrIndex "Gta Object" "TXD"
			
			for Obj in interiorGroupObjects do
			(
				SetAttr Obj idxTxd txtGroupTXD.Text
			)
		)
		
		on toolsRoll open do
		(
			--Get all the InteriorGroupHelpers
			InteriorGroupObjList = for HelperObj in Helpers where ClassOf HelperObj == InteriorGroupHelper collect HelperObj
			
			--Get this specific InteriorGroupHelper
			ThisNode = (for Dependant in Refs.Dependents this where isValidNode Dependant collect Dependant)[1]
			
			--Find out what number it is
			InstanceIndex = FindItem InteriorGroupObjList ThisNode
			
			--Use this information to form a default TXD name which will definately be unique and under 20 characters
			DefaultTXDName = "Group_" + ( InstanceIndex as String ) + "_TXD"			
			txtGroupTXD.Text = DefaultTXDName
		)
	)
)
