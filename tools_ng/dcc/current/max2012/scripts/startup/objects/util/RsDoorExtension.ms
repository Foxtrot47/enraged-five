-- Rockstar door extension Object
-- Rockstar North
-- 14/12/2011
-- by Jonny Rivers

-- helper object for extending door properties

plugin SimpleManipulator RsDoorExtension
	name:"RS Door"
	classID:#(0x7d53e2e4, 0x2b3ce1d3)
	category:"RS Helpers"
(
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None
	fn GetNode = 
	(
		retVal = undefined
		local refsArray = (refs.dependents this)
		for ref in refsArray while (retVal == undefined) do 
		(
			if (isvalidnode ref) and (isKindof ref RsDoorExtension) do
			(
				retVal = ref
			)
		)
		retVal
	)

	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------

	--
	-- event: canManipulate
	-- desc:
	--
	on canManipulate target return (classOf target) == RsDoorExtension
	
	--
	-- event: updateGizmos
	-- desc: Refresh gizmo representation.
	--
	on updateGizmos do
	(	
		theNode = GetNode()
		if undefined==theNode then
		(
			return false
		)
		
		
		limitAngleIndex = GetAttrIndex "RS Door" "Limit Angle"
		limitAngleValue = GetAttr theNode limitAngleIndex		
		
		local size = 2
		local orientation = 1
		
		--determine the size and orientation of the helper from the parent object
		if( theNode.parent != undefined ) then
		(
			parentBoundingBox = nodeGetBoundingBox theNode.parent theNode.parent.transform
			sizeInNegXFromPivot = abs parentBoundingBox[1].x
			sizeInPosXFromPivot = abs parentBoundingBox[2].x
			if( sizeInNegXFromPivot > sizeInPosXFromPivot ) then
			(
				size = sizeInNegXFromPivot
				orientation = -1
			)
			else
			(
				size = sizeInPosXFromPivot
				orientation = 1
			)
		)
		
		local LimitMax = limitAngleValue
		local LimitMin = -limitAngleValue
		
		this.clearGizmos()
		local flags = (gizmoActiveViewportOnly)
		local segsize = 9.0
		
		local segs = ( LimitMax - LimitMin ) / segsize
		
		local axisstart, axisend
		local axisarc = manip.makeGizmoShape()
		local axiscolour, markerColour
		
		axiscolour = [0,0,1]
		markerColour = [0.7,0.7,1]
		
		axisstart = [orientation * cos(LimitMin),sin(LimitMin),0] * size
		axisend = [orientation * cos(LimitMax),sin(LimitMax),0] * size
		axisarc.addPoint( [0,0,0] )
		axisarc.addPoint( axisstart )
		for seg = 0 to segs do
		(
			local angle_from = LimitMin + (seg*segsize)
			local xp = [orientation * cos(angle_from),sin(angle_from),0] * size
			axisarc.addPoint( xp )
		)
		axisarc.addPoint( axisend )
		axisarc.addPoint( [0,0,0] )
		
		local orientSize = size
		local orientLine = manip.makeGizmoShape()
		orientLine.addPoint( [0,0,0] )
		local xp = ( [orientation,0,0] * orientSize)
		orientLine.addPoint( xp )
		this.addGizmoShape orientLine flags axiscolour axiscolour
		
		this.addGizmoShape axisarc flags axiscolour axiscolour
		this.addGizmoMarker #bigBox axisstart flags axiscolour axiscolour
		this.addGizmoMarker #bigBox axisend flags axiscolour axiscolour
		this.addGizmoMarker #plusSign axisend flags markerColour markerColour
		this.addGizmoMarker #xMarker axisstart flags markerColour markerColour
		
		if( theNode.parent != undefined ) then
		(
			theNode.transform = theNode.parent.transform
		)
		
		"RSDoorExtension"
	)

	--
	-- event: create
	-- desc: Event handler called on manipulator creation.
	--
	tool create
	(
        on mousePoint click do
           case click of
           (
              1:
				(
					#stop 
				)
           )
	)
)