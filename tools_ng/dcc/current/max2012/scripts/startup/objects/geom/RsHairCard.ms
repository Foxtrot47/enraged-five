plugin simpleObject RsHairCard
name:"RsHairCard"
classID:#(0x4743437b, 0x767c3f09)
category:"RS Objects"
(
	---------------------------------------------------------------------------
	-- LOCALS 
	---------------------------------------------------------------------------
	local pObj, pBend, theNode	
	
	---------------------------------------------------------------------------
	-- PARAMETERS
	---------------------------------------------------------------------------
	parameters main rollout:params
	(
		spline type:#node 
		pLength type:#worldUnits default:0.1
		hLengthPercent type:#float ui:spn_hLengthPercent default:100
		hRootWidth type:#worldUnits ui:spn_hRootWidth default:0.03
		hTipWidth type:#worldUnits ui:spn_hTipWidth default:0.01		
		hLengthSegs type:#integer ui:spn_hLengthSegs default:4
		hWidthSegs type:#integer ui:spn_hWidthSegs default:2
		hBend type:#float ui:spn_hBend default:50
		hRootTwist type:#float ui:spn_hRootTwist default:0
		hTipTwist type:#float ui:spn_hTipTwist default:0
		hRootColour type:#color ui:cbp_root default:(color 20 20 20)
		hTipColour type:#color ui:cbp_tip default:(color 220 220 220)
	)	
	
	parameters uvs rollout:uvs
	(
		uTile type:#float ui:spn_u_Tile default:1
		vTile type:#float ui:spn_v_Tile default:1
		uOffset type:#float ui:spn_u_Offset default:0
		vOffset type:#float ui:spn_v_Offset default:0
		uvRotate type:#float ui:spn_uv_Rotate default:0
	)

	---------------------------------------------------------------------------
	-- FUNCTIONS 
	---------------------------------------------------------------------------
	fn lerp minVal maxVal term multi:1 = ((maxVal - minVal) * term + minVal) * multi
	---------------------------------------------------------------------------
	fn cLerp cA cB term = color (lerp cA.r cB.r term) (lerp cA.g cB.g term) (lerp cA.b cB.b term)
	---------------------------------------------------------------------------
	fn MakeMatrix X Y =  
	( 
		M = matrix3 1
		M.row1 = normalize x   
		M.row3 = normalize (cross X Y) 
		M.row2 = normalize (cross M.row3 X)  
		return M
	)
	---------------------------------------------------------------------------
	fn splineFilter obj = 
	(
		if (classof obj == Line) OR (classof obj == SplineShape) do return true
		return false
	)
	---------------------------------------------------------------------------
	fn GetNode = 
	(	

		local refsArray = (refs.dependents this)
		for ref in refsArray while (theNode == undefined) do 
		(
			if (isvalidnode ref) and (isKindof ref RsHairCard) do theNode = ref			
		)
		return theNode
	)	
	---------------------------------------------------------------------------
	fn MoveToSpline = 
	(
		GetNode()
		if (spline != undefined) AND (theNode != undefined) do theNode.pos = interpCurve3D spline 1 0
	)
	---------------------------------------------------------------------------
	fn getStartingVector = 
	(
		spTangent = normalize (tangentCurve3D spline 1 0)	
		return (normalize (cross spTangent [0,0,1]))
	)
	
	---------------------------------------------------------------------------
	-- ROLLOUT 
	---------------------------------------------------------------------------
	rollout params "Hair Params"
	(
		group "Spline"
		(
			pickbutton btn_pickSpine "Pick..."  filter:splineFilter width:140 
		)
		
		Group "Size"
		(
			spinner spn_hRootWidth "Root Width : " type:#worldUnits scale:0.002
			spinner spn_hTipWidth "Tip Width : " type:#worldUnits scale:0.002		
			spinner spn_hLengthPercent "Length % : " type:#float range:[1,100,100]	
		)
		
		group "Segments"
		(			
			spinner spn_hWidthSegs "Width : " type:#integer range:[1,10,1]
			spinner spn_hLengthSegs "Length : " type:#integer range:[1,100,4]				
		)
		
		group "Twist"
		(
			spinner spn_hRootTwist "Root : " type:#float range:[-999999,999999,0] 
			spinner spn_hTipTwist "Tip : " type:#float range:[-999999,999999,0]
			spinner spn_hBend "Profile Bend : " type:#float range:[-100,100,0]
		)
	
		group "Vertex Colour"
		(
			colorpicker cbp_root "Root : " across:2 align:#left fieldWidth:30
			colorpicker cbp_tip "Tip : " align:#right fieldWidth:30
		)		
		---------------------------------------------------------------------------
		on params open do 
		(
			if (spline != undefined) do btn_pickSpine.text = spline.name
		
		)
		---------------------------------------------------------------------------
		on btn_pickSpine picked arg do
		(
			if (arg != undefined) do 
			(
				spline = arg
				btn_pickSpine.text = spline.name				
			)
		)
	)
	---------------------------------------------------------------------------
	rollout uvs "UV Params" rolledUp:true
	(
		spinner spn_u_tile "U Tile : " range:[-999999,999999,0] scale:0.005
		spinner spn_v_tile "V Tile : " range:[-999999,999999,0] scale:0.005
		spinner spn_u_offset "U Offset : "  range:[-999999,999999,0] scale:0.005
		spinner spn_v_offset "V Offset : "  range:[-999999,999999,0] scale:0.005
		spinner spn_uv_rotate "Rotate : " range:[-180,180,0]
	)
	
	---------------------------------------------------------------------------
	-- EVENTS 
	---------------------------------------------------------------------------
	on buildMesh do
	(
		local upV = [0,0,1]
		
		-- MOVE 
		MoveToSpline()
		
		-- MAKE MESH 
		if pObj == undefined do pObj = createInstance plane() 		

		pObj.length = pLength
		pObj.width = pLength 
		pObj.lengthsegs = hLengthSegs
		pObj.widthsegs = hWidthSegs
		
		tMesh = pObj.mesh
		
		if (spline != undefined) do 
		(
			-- LOOP VERTS 
			local pivOffset = TransMatrix [0,pLength/2,0]
			local nodePos = if (GetNode() != undefined) then theNode.pos else [0,0,0]			
			
			meshop.setNumTVerts tMesh tMesh.numverts
			
			for v=1 to tMesh.numverts do 
			(			
				-- GET VERT 
				vP = getVert tMesh v
				
				-- MOVE FORWARD 
				vP *= pivOffset	
				
				-- CURRENT SPLINE INFO 
				lengthFraction = (amax (amin ((vP.y/pLength) * (hLengthPercent/100)) 0.999) 0.001)
				spPos = interpCurve3D spline 1 lengthFraction		
				spTangent = normalize (tangentCurve3D spline 1 lengthFraction)	

				upV = normalize (cross (cross spTangent upV) spTangent)						
				
				-- WIDTH				
				vP.x *= lerp hRootWidth hTipWidth lengthFraction multi:10 			
				
				-- BEND				
				vP *= rotateYMatrix (hBend * (vP.x/(amax hRootWidth hTipWidth)))			
				
				-- PATH DEFORM 					
				tM = TransMatrix spPos			
				nM = MakeMatrix spTangent upV
				
				-- ADD TWIST 
				prerotateX nM (lerp hRootTwist hTipTwist lengthFraction) 
				
				-- MOVE TO SPLINE
				nM = nM * tM	
				
				-- OFFSET VERTS 
				PreTranslate nM [0,-vP.x,vP.z]
				
				-- SET NEW UP VECTOR 
				upV = normalize (cross (cross spTangent upV) spTangent)				
	
				-- GET POS VALUE 
				vP = nM.pos - nodePos 					
				
				-- SET VERT 
				setVert tMesh v vP	

				-- VERT COLOUR
				meshop.setvertcolor tMesh 0 #{v} (cLerp hRootColour hTipColour lengthFraction)				
			)	

			-- UVS 				
			local trans = (TransMatrix [uOffset,vOffset,0]) * (scaleMatrix [uTile,vTile,1])
			prerotateZ trans uvRotate		
			for v=1 to (meshop.getNumMapVerts tMesh 1) do meshop.setMapVert tMesh 1 v ((meshop.getMapVert tMesh 1 v) * trans) 
		)

		-- SET MESH
		mesh = tMesh
	)
	
	---------------------------------------------------------------------------
	-- TOOL 
	---------------------------------------------------------------------------
	tool create
	(
		on mousePoint click do
		(		
			nodeTM.translation = gridPoint
			#stop
		)		
	)
)

