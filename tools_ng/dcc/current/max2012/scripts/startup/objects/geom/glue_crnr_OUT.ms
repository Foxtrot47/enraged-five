--	Scripted object plugin - generated by Plug-O-Matic v3.2 (originally by martin@breidt.net)
-- (reworked by Neal Corbett, R* Leeds, 2010)
--
--	This will create a new object under 'Geometry > RS scaleObjs' in the creation tab.
global RSgeom_glue_crnr_OUT_data
plugin simpleObject glue_crnr_OUT
name:"glue_crnr_OUT"
category:"RS Useful Geometry"
classID:#(1300321568, 1028006103)
(
	rollout aboutRO "About glue_crnr_OUT" 
	(
		label l1 "Exported on"
		label l2 "16/08/2010 12:43:56"
		label l3 "Plug-O-Matic v3.2 [R*]"
	)
	on buildMesh do (
		-- Load model-data
		if RSgeom_glue_crnr_OUT_data == undefined do (
			local scriptFilename = getSourceFileName()
			local dataFilename = (getFilenamePath scriptFilename) + (getFilenameFile scriptFilename) + ".txt"
			RSgeom_glue_crnr_OUT_data = #()
			local dataFile = openFile dataFilename
			while not eof dataFile do (
				skipToNextLine dataFile
				append RSgeom_glue_crnr_OUT_data (execute (readLine dataFile))
			)
			close dataFile
		)
		local vp = RSgeom_glue_crnr_OUT_data[1]
		local fl = RSgeom_glue_crnr_OUT_data[2]
		local fs = RSgeom_glue_crnr_OUT_data[3]
		local fe = RSgeom_glue_crnr_OUT_data[4]
		-- Create mesh for buildMesh
		setmesh mesh vertices:vp faces:fl
		-- Apply additional face data
		for i = 1 to fs.count do setFaceSmoothGroup mesh i fs[i]
		for i = 1 to fe.count do (for j = 1 to 3 do (setEdgeVis mesh i j fe[i][j]))
	) -- end on buildmesh

	tool create prompt:"Click build object. Hold button to rotate." 
	(
		local placedPos, startViewPoint
		on mousePoint click do (
			case click of (
				1:
				(
					placedPos = nodeTM.translation = gridPoint
					startViewPoint = viewPoint
				)
				2: #stop
			)
		)
		on mouseMove click do (
			nodeTM.rotation = eulerangles 0 0 (((mod ((distance viewPoint startViewPoint) * 0.01) 4) as integer) * 90)
			nodeTM.translation = placedPos
		)
	) -- end create tool
) -- end Plug-O-Matic plugin
