--	Scripted object plugin - generated by Plug-O-Matic v3.2 (originally by martin@breidt.net)
-- (reworked by Neal Corbett, R* Leeds, 2010)
--
--	This will create a new object under 'Geometry > RS scaleObjs' in the creation tab.
global RSgeom_v_boat_data
plugin simpleObject v_boat
name:"v_boat"
category:"RS scaleObjs"
classID:#(996115528, 1424959614)
(
	rollout aboutRO "About v_boat" 
	(
		label l1 "Exported on"
		label l2 "16/08/2010 12:17:14"
		label l3 "Plug-O-Matic v3.2 [R*]"
	)
	on buildMesh do (
		-- Load model-data
		if RSgeom_v_boat_data == undefined do (
			local scriptFilename = getSourceFileName()
			local dataFilename = (getFilenamePath scriptFilename) + (getFilenameFile scriptFilename) + ".txt"
			RSgeom_v_boat_data = #()
			local dataFile = openFile dataFilename
			while not eof dataFile do (
				skipToNextLine dataFile
				append RSgeom_v_boat_data (execute (readLine dataFile))
			)
			close dataFile
		)
		local vp = RSgeom_v_boat_data[1]
		local fl = RSgeom_v_boat_data[2]
		local fs = RSgeom_v_boat_data[3]
		local fe = RSgeom_v_boat_data[4]
		-- Create mesh for buildMesh
		setmesh mesh vertices:vp faces:fl
		-- Apply additional face data
		for i = 1 to fs.count do setFaceSmoothGroup mesh i fs[i]
		for i = 1 to fe.count do (for j = 1 to 3 do (setEdgeVis mesh i j fe[i][j]))
	) -- end on buildmesh

	tool create prompt:"Click build object. Hold button to rotate." 
	(
		local placedPos, startViewPoint
		on mousePoint click do (
			case click of (
				1:
				(
					placedPos = nodeTM.translation = gridPoint
					startViewPoint = viewPoint
				)
				2: #stop
			)
		)
		on mouseMove click do (
			nodeTM.rotation = eulerangles 0 0 (((mod ((distance viewPoint startViewPoint) * 0.01) 4) as integer) * 90)
			nodeTM.translation = placedPos
		)
	) -- end create tool
) -- end Plug-O-Matic plugin
