ProcObjNumSegments = 32

plugin SimpleObject GtaProcObj
	name:"Gta ProcObj"
	classID:#(0x2e205dbb, 0x2d724d25)
	category:"RS Objects"
(
	parameters main rollout:params	
	(
		radiusInner type:#float ui:radiusInner default:0
		radiusOuter type:#float ui:radiusOuter default:0
	)
	
	rollout params "Two Faces Parameters"
	(
		spinner radiusInner "Inner Radius" type:#float range:[-1000,1000,0]
		spinner radiusOuter "Outer Radius" type:#float range:[-1000,1000,0]
	)

	on buildMesh do
	(
		numVerts = (ProcObjNumSegments + 1) * 2
		Count = (ProcObjNumSegments + 1)
		segmentSize = 360.0 / ProcObjNumSegments
		segmentAngle = 0
		vertList = #()
		polyList = #()
		
		actualRadiusInner = radiusInner
		actualRadiusOuter = radiusOuter
		
		if actualRadiusInner < 0 then (
		
			actualRadiusInner = -actualRadiusInner
		)

		if actualRadiusOuter < 0 then (
		
			actualRadiusOuter = -actualRadiusOuter 
		)
		
		for i = 1 to Count do (
		
			innerVert = [actualRadiusInner * (sin segmentAngle),actualRadiusInner * (cos segmentAngle), 0]
			outerVert = [actualRadiusOuter * (sin segmentAngle),actualRadiusOuter * (cos segmentAngle), 0]
			
			append vertList innerVert
			append vertList outerVert
			
			if i != Count then (
			
				append polyList [(i * 2) - 1,(i * 2) + 1,(i * 2)]
				append polyList [(i * 2),(i * 2) + 1,(i * 2) + 2]
			)
			
			segmentAngle = segmentAngle + segmentSize
		)
		
		setmesh mesh numverts:vertList.count numfaces:polyList.count
				
		for i = 1 to vertList.count do (
		
			setvert mesh i vertList[i]
		)
		
		for i = 1 to polyList.count do (
		
			setface mesh i polyList[i]
			setedgevis mesh i 1 true
			setedgevis mesh i 2 true
			setedgevis mesh i 3 true
		)
		
		update mesh
	)

	tool create
	(
		on mousePoint click do
			case click of
			(
				1: nodeTM.translation = gridPoint
				3: #stop
			)
		on mouseMove click do
			case click of
			(
				2: radiusOuter = gridDist.y
				3: radiusInner = gridDist.y
			)
	)
)