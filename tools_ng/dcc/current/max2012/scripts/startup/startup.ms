-- Rockstar Startup
-- Rockstar North
-- 2/2/2005
-- by Greg Smith

-- create the rockstar export and utility menu if it needs to be created

-- This will be set to True for outsource tools by settings_funcs.ms
global gRsIsOutSource = false

-- Load startup assemblies
filein "pipeline/util/RSdotNet.ms"
RS_dotNetLoadStartupAssemblies()

filein "pipeline/util/string.ms"
filein "pipeline/util/file.ms"
filein "pipeline/util/xml.ms"

filein "pipeline/util/array.ms"
filein "rockstar/export/settings_funcs.ms"
filein "rockstar/export/settings.ms"

filein "rockstar/util/menu.ms"

filein "pipeline/util/RsModelFuncs.ms"

filein "rockstar/util/mapobj.ms"

filein "rockstar/util/dialog.ms"
filein "pipeline/ui/RsNotifyBox.ms"

filein "pipeline/util/notenodes.ms"

filein "pipeline/util/RsDebugStack.ms"
fileIn "rockstar/util/RSBanner.ms"
filein "pipeline/util/RsUniversalLog.ms"

-- Clear out any lingering log files from previous sessions, tools, etc.
gRsULog.ClearLogDirectory()
gRsULog.init "startup"
fileIn "pipeline/util/metadata_interface.ms"

filein "pipeline/util/ObjectLinkMgr.ms"

filein "pipeline/util/RsStatedAnimProxy.ms"
filein "pipeline/util/RsStatedAnimIMAPDummy.ms"
filein "pipeline/util/p4.ms"
filein "pipeline/util/config.ms"
filein "pipeline/util/remoteConsoleFuncs.ms"
filein "pipeline/util/container_funcs.ms"
filein "pipeline/util/RsAttributeRolloutCreator.ms"
fileIn "pipeline/helpers/materials/meshTint_funcs.ms"
filein "pipeline/util/RSrefs.ms"

filein "rockstar/util/material.ms"
filein "pipeline/util/ragematerial.ms"

fileIn "pipeline/util/UtilityFunc.ms"
fileIn "pipeline/util/viewport_funcs.ms"
fileIn "pipeline/util/collision_funcs.ms"
filein "pipeline/helpers/materials/mtlSystem.ms"
filein "pipeline/ui/BoneTagOverrides.ms"

filein "pipeline/export/AP3Content.ms"

-- Remove the legacy RS Wildwest  menu
try
(
	menuman.unRegisterMenu (menuMan.findMenu "RS Wildwest")
)catch()

-- Define and install global callbacks:
filein "pipeline/util/global_callbacks.ms"
filein "pipeline/util/FxUtils.ms"

filein "pipeline/helpers/weather.ms"

-- File needed for catenary curve struct dependency
filein "rockstar/util/CatenaryCurve.ms"

filein "RsSceneLinkInit.ms"
filein "RsEditorApplication.ms"

filein "pipeline/util/MaterialComparator.ms"
filein "pipeline/util/GuidTools.ms"


filein "pipeline/util/ExcludedTextures.ms"

-- Only load the menu if settings have initialised properly
format "RsToolsInstalled %\n" RsToolsInstalled

-- Turn off Windows dead-process ghosting for Max:
RS_disableProcessWindowsGhosting()

-- Ensure that Perforce Integration is deactivated for outsource tools:
if gRsIsOutSource do 
(
	RsProjectSetPerforceIntegration false
)

if RsToolsInstalled == true or gRsIsOutSource do 
(
	-- Check that the user has installed the latest version of the tools.
	RsCheckConfigAtStartup()
	
	RsInitialiseEditorApplication()
	
	RsSetRexCollisionList()
	RsSetRexProceduralList()

	RstInitialiseShaderLibrary ( RsConfigGetShadersDir() ) ( RsConfigGetShadersDir() + "/db/" ) "default" "alpha"
	
	--Sync to the material presets
	RsSetupMaterialPresets()
	
	/*
	-- Commenting this out for now, was causing issues because the P4 object was not being disposed of properly and causing a shutdown crash.
	perforceChecker = dotNetObject  "RSG.Base.Configuration.PerforceCurrentChecker"

	if ( perforceChecker != undefined ) then 
	(
		if ( ( perforceChecker.Check 3 ) == false ) then -- param is the number of revisions the user can be out of date before the message box appears
		(
			messageBox "Your tools are more than three revisions out of date. Out of data tools can cause issues with the game. Please note that updating your tools will often require you to have the latest build of the game."
		)
	)
	*/
	
	try
	(
		-- Initialise rageMaxParticle plugin for current project.
		local filename = RsGetEntityFxInfoPath()
		if not (doesFileExist filename) then
			throw "No entityfx.dat found in this project, titleupdate or core."
		else
			rageparticle_setentityfxdat ( filename )
		rageparticle_seteffectroot ( RsConfigGetArtDir core:true + "vfx/rmptfx_tu" )
		RsRebuildParticlesWithReport()
	)
	catch
	(
		format "Exception rebuilding GtaParticle entity descriptions: *** % ***\n" (getCurrentException())
	)

	--try to bring in the deltaMush Skinning modifiers (added by Rennie)
	
	try
	(
		filein "rockstar/util/deltaMush/deltaMushSetup.ms"

		format ("Successfully initialised DeltaMush modifiers.\n")
	)
	catch
	(
		format ("Failed to initialise DeltaMush modifiers. Please contact tech Art.\n")
	)
	
	------------------------------------------------------------------------------------
	-- beginning of execution
	------------------------------------------------------------------------------------

	minHeapSize = 122880000

	if heapsize < minHeapSize then (

		heapsize = minHeapSize
	)

	RstSetNormalMapFlip true
	
	-- Rebuild menus:
	(
		-- Generate 'RS Export' menu:
		RsSetMenu "RS Export" #(
			"RsMapExport",
			"RsSingleIPLExport",
			"RsWaterExport",
			"RsPlayerCoords",
			"",
			"RsPedExport",
			"RsVehicleExport",
			"RsExpressionsExport",
			"RsModelExport",
			"RsExportImportZones",
			"RsExportAmbientScripts",
			"RsExportCutLoc",
			"RsExportViseme",
			"RsExportVectorMap",
			"",
			"RsMapExportReports",
			"ClearUniversalLogDir",
			"",
			"ShowUniversalLogDialog"
		)
		
		-- Generate 'RS Utils' menu:
		RsSetMenu "RS Utils" #(
			"RsWorkBench", 
			"RsWarpPlayer",
			"",
			"ShowSelector", 
			"BatchUpdate", 
			"UVTools",
			"",
			"IgnoreMe",
			"DontExport",
			"DontApplyRadiosity",
			"",
			DataPair MenuName:"RS Organisational" Items:#(
				"GuidTools",
				"VisibilitySet",
				"CreateSelectionSets",
				"ProjMan",
				"Containers",
				"PropGroups",
				"OpenDecisionMgr"
			),
			DataPair MenuName:"RS Placement" Items:#(
				"LiveMapEditing", 
				"LiveMapEditingGameToMax", 
				"Path_Deform", 
				"PlacementToolkit", 
				"ObjPosChecker", 
				"Scatterer", 
				"VegetationDropper" 
			),
			DataPair MenuName:"RS Lighting" Items:#(
				"RsCutsceneLightingTools", 
				"RadiosityLighting", 
				"LightLister", 
				"LightEditor", 
				"SceneBaker"
			),
			DataPair MenuName:"RS Characters" Items:#(
				"RsMirroringPerVertTool", 
				"RsFindDupBoneTags" 
			),
			DataPair MenuName:"RS Interiors" Items:#(
				"PropsMilos", 
				"MiloOperations", 
				"QuickDestructibleObjectSetup", 
				"DefaultEntitySetSelector"
			),
			DataPair MenuName:"RS Collision" Items:#(
				"CollToolkit",
				"RsCollisionSet",
				"RsCollPaint",
				"",
				"SetBoundMaterials",
				"AudioMatCheck", 
				"OptimisePlanar",
				"CollisionBoxCheck"
			),
			DataPair MenuName:"RS Refs" Items:#(
				"XrefToolkit", 
				"RSrefConverter", 
				"RSrefBrowser", 
				"RSrefSettings", 
				"PropViewer"
			),
			DataPair MenuName:"RS Modelling" Items:#(
				"ModelToolkit",
				"RSchannelClearer",
				"RenderProps",
				"FragmentTools"
			),
			DataPair MenuName:"RS Material" Items:#(
				"MaterialToolkit",
				"MaterialComparator",
				"ChangeShaders",
				"ReplaceShaders",
				"BadTex",
				"RsTerrainSetup", 
				"TexturePathRemap", 
				"RsVertColToolkit",
				"VertexColourTools", 
				"EnvironmentMap",
				"ShowMapsInViewport",
				"RsTerrainCbPaint",
				"VertAnglePainter",
				"ProxyTextureSwap",
				"GrassPainter",
				"ShaderEditor",
				"NitrousTools",
				"SceneTextureValidator"
			),
			DataPair MenuName:"RS Info" Items:#(
				"InfoToolkit", 
				"DoShowSceneStats", 
				"DoShowTextureStats",
				"DoShowTXDStats"
			),
			DataPair MenuName:"RS Lod" Items:#( 	
				"LodEditor", 
				"LodToolkit",
				"LodModUtil",
				"SLOD2RefImporter",
				"SimplygonToolkit"
			),
			DataPair MenuName:"RS VFX" Items:#(
				"VFXToolkit",
				"StatedAnimationSetup",
				"WaterAlphaPaint"
			),
			DataPair MenuName:"RS Vehicle" Items:#(
				"VehicleToolkit", 
				"RsImportVehicleRec"
			),
			DataPair MenuName:"RS Cloth" Items:#(
				"RsClothEditor", 
				"RsClothLodTool", 
				"RsClothCollTool", 
				"RsClothMiscTools"
			),
			DataPair MenuName:"RS Debug" Items:#( 
				"Bugstar", 
				"",
				"RsSceneLinkEditor",
				"RsImportIndBoundsFromFile", 
				"SetCameraFromGame", 
				"RsFragmentTuner",
				"ScriptDump",
				"ERotFinder",
				"SetObjectAttr",
				"RsNoteNodes",
				"RSremoveDuplicateMenus",
				"RsDebugOverlay",
				"RsTextSearch",
				"RSshowMemSize",
				"RSResetAnimGroupName"
			),
			DataPair MenuName:"RS Source Control" Items:#(
				"RsPerforceTools",
				"RsPerforceGetLatestContainers",
				"RsPerforceCheckoutContainers",
				"RsPerforceCheckinContainers",
				"RsP4syncSceneTexturesMacro"
			),
			DataPair MenuName:"RS Animation" Items:#(
				"RsAnimationToolsImporter",
				"RsAnimationToolsExporter",
				"RsBoneTagEditor"
			),			
			DataPair MenuName:"RS Terrain" Items:#(
				"RsTerrainProcessing",
				"RsTextureTileGenerator"
			),						
			DataPair MenuName:"RS Audio" Items:#(
				"RsAudioTools"
			)
		)

		-- DEFINE THE ROCKSTAR QUADMENU ---
		--	(top-right quad on shift-rightclick)
		quadmenu = (menuMan.getViewportRightClickMenu #shiftpressed).getmenu 2
		
		-- Clear this menu if it isn't titled 'Rockstar':
		RsEmptyMenu quadmenu firstRun:(quadmenu.getTitle() != "Rockstar")
		quadmenu.setTitle("Rockstar")

		-- EXTRA TABS HAVE BEEN ADDED TO SOME MENU-NAMES TO DIFFERENTIATE MENUS:
		-- The menu-emptying command unassigns any menus with the same names as ones that are being re-created,
		-- but this causes asserts when we have multiple menus with the same name (such as Face Selection or Entire Scene)
		
		RsSetMenu Quadmenu #(
			DataPair MenuName:"Change Texture Scale:  Object Selection" Items:#( "To100pc", "To75pc", "To50pc", "To25pc", "To12pc" ),
			DataPair MenuName:"\t                                       Face Selection" Items:#( "To100pcFace", "To75pcFace", "To50pcFace", "To25pcFace", "To12pcFace" ),
			DataPair MenuName:"\t                                       Entire Scene" Items:#( "To100pcScene", "To75pcScene", "To50pcScene", "To25pcScene", "To12pcScene" ),
			"",
			DataPair MenuName:"Toggle Shader Lighting: Object Selection  " Items:#( "LightObjOn", "LightObjOff" ),
			DataPair MenuName:" \t                                      Face Selection" Items:#( "LightFaceOn", "LightFaceOff" ),
			DataPair MenuName:" \t                                      Entire Scene" Items:#( "LightSceneOn", "LightSceneOff" ),
			"",		
			DataPair MenuName:"Show in Explorer  " Items:#(
				"RsShowFileExportInExplorer",
				"RSshowObjExportInExplorer",
				"RSshowTXDExportInExplorer"
			),	
			DataPair MenuName:"Materials     " Items:#(
				"SwitchSceneDXMats", 
				"SwitchSceneRageMats", 
				"",
				"ShowMapsInViewportQuad",
				"SwitchStdMatsToRage",
				"TextureTools"
			),
			DataPair MenuName:"Show LOD     " Items:#(
				"ShowHiDetail",
				"ShowLodDetail",
				"ShowSuperLodDetail", 
				"ShowSuperLod2Detail", 
				"ShowSuperLod3Detail",
				"",
				"RsLodShowTool"
			),
			DataPair MenuName:"Select     " Items:#(
				"SelectLODChildren",
				"SelectLODParent",
				"SelectLODAllSet",
				"SelectXRefAllSet",
				"SelectNormalAllSet", 
				"SelectHorizonAllSet",
				"",
				"ShowSelector",
				"CreateSelectionSets"
			),
			DataPair MenuName:"Create Collision  " Items:#(
				"CollToolkit",
				"RsCollisionSet",
				"RsCollPaint",
				"",
				"CreateCollCylinder",
				"CreateCollCapsule",
				"CreateCollSphere",
				DataPair MenuName:"Create Collision Mesh" Items:#(
					"CreateCollMesh",
					"CreateCollMeshFromExist",
					"CreateShadMesh"
				),
				DataPair MenuName:"Create Collision Box" Items:#(
					"CreateCollBoxXYZRot",
					"CreateCollBoxYZRot",
					"CreateCollBoxZRot",
					"CreateCollBox"
				)
			)
		) Reversed:True StoreInfo:False 
		-- 'Reversed': Top-quadrant menus need to be added in reverse order to look right.
		-- 'StoreInfo': These commands won't be added to TypeNgo command-list
	)

	-- Remove menus with no content:
	RsCleanMenu #("RS Export", "RS Utils")
	
	menuMan.updateMenuBar()
	
	-- Remove macro-name arrays, they're no longer needed:
	globalVars.remove "RSmacroNameList"
	globalVars.remove "RSmacroCatList"
	
	-- Force changes to autosave for everyone: GTA5 #5307
	-- Changed so that if people want a more frequent autosave time interval
	-- then they can, but it can't be any longer than 15 mins.
	autosave.NumberOfFiles = 10
	if ( autosave.Interval > 15.0 ) do
	(
		autosave.Interval = 15.0
	)
	
	-- Load the default project to start with.
	RsValidateProject()

	RsCheckUnitsSetup()
)

-- Update the auto-generated RsTechArtTools menu:
filein "pipeline/RsTechArtTools.ms"

-- startup/rsutil.ms
