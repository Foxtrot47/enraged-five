--
-- File:: scripts/startup/RsDotNet_ExceptionHandler.ms
-- Description:: .Net Exception Handler function (requires custom mxsdotnet.dlx plugin)
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 8 June 2011
--

--
-- name: RsDotNetGlobalExceptionHandler
-- desc: This is the global handler function that is invoked when a .Net exception occurs.
--
-- The function prints out exception information so we have at least a stacktrace of 
-- where the exception happened in .Net code.
--
fn RsDotNetGlobalExceptionHandler ex = (
	
	format "Rockstar Games MaxScript Triggered .Net Exception Report\n"
	format "\tException Class: %\n" ( ex.GetType() )
	format "\tException Message: %\n" ex.Message
	
	format "\tStackTrace:\n" 
	stackTraceInfo = filterString ex.StackTrace "\n" splitEmptyTokens:false
	for info in stackTraceInfo do
		format "\t\t%" info
	
	format "\n\n"
)

--
-- name: RsDotNetIsExceptionHandlerAvailable
-- desc: Return whether the global exception handler is available (i.e. user has custom mxsdotnet.dlx plugin)
--
fn RsDotNetIsExceptionHandlerAvailable = (
	( undefined != dotNetClass "MXS_dotNet.DotNetExceptionHandler" )
)

--
-- name: RsDotNetSetupGlobalExceptionHandler
-- desc: Setup the global exception handler function.
--
fn RsDotNetSetupGlobalExceptionHandler = (
	local exHandlerClass = dotNetClass "MXS_dotNet.DotNetExceptionHandler"
	dotNet.addEventHandler exHandlerClass "OnException" RsDotNetGlobalExceptionHandler	
)

--
-- name: RsDotNetClearGlobalExceptionHandler
-- desc: Clear the global exception handler function.
--
fn RsDotNetClearGlobalExceptionHandler = (
	local exHandlerClass = dotNetClass "MXS_dotNet.DotNetExceptionHandler"
	dotNet.removeAllEventHandlers exHandlerClass
)

-- Startup
if ( RsDotNetIsExceptionHandlerAvailable() ) then
(
	format "Initialising .Net Global Exception Handler...\n"
	RsDotNetClearGlobalExceptionHandler()
	RsDotNetSetupGlobalExceptionHandler()
	format "Done.\n"
)
else
(
	format ".Net Global Exception Handler: custom mxsdotnet.dlx not available.\n"
)
