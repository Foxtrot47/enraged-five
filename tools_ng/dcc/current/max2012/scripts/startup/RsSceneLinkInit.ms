
global LinkType_ANY = -1
global LinkType_LOD = 0					-- For Scene LODs
global LinkType_DRAWLOD = 1			-- For Drawable LODs
global LinkType_RENDERSIM = 2		-- For Cloth geometry
global LinkType_ClothCollision = 3		-- For Cloth collision (peds)
global LinkType_CombinerMesh = 5	-- For combolodder source-meshes
global LinkType_ShadowMesh = 6		-- For shadow/reflection proxies
global LinkType_CustomHelperLink= 7

-- Objects linked with these link-types will be packed into the same file on export:
global RsObjPackedLinktypes = #(LinkType_DRAWLOD, LinkType_RENDERSIM, LinkType_ShadowMesh)

struct RsSceneLinkTypeStruct 
(
	idx, 						-- Link-channel index
	name = "unused",	-- Channel-name 
	dotnetEnumName = undefined,
	dotnetEnum = undefined,
	info = "unused", 		-- Description of channel
	parentInfo = "", 		-- What does this channel use Parent links for?
	childInfo = "", 			-- What does this channel use Child links for?
	maxChildren = -1,	-- Is there a limit to number of Child links?
	maxParents = -1,		-- Is there a limit to number of Parent links?
	mapObjIsChild = True	-- If True, the main map-object will be the link-child.  (this is the case for LOD-links: high-detail model is child)
)

global gRsSceneLinkTypeData = 
#(
	RsSceneLinkTypeStruct \
		idx:LinkType_LOD \
		name:"Scene_LOD"\
		dotnetEnumName:"LinkChannelSceneLod"\
		info:"Lower detail needs to be PARENT"\
		parentInfo:"Lower Detail Object (1 per obj)"\
		maxParents:1\
		childInfo:"Higher Detail Objects",
	
	RsSceneLinkTypeStruct \
		idx:LinkType_DRAWLOD\
		name:"Drawable_LOD"\
		dotnetEnumName:"LinkChannelDrawableLod"\
		info:"Lower detail needs to be PARENT"\
		parentInfo:"Lower Detail (1 per obj)"\
		maxParents:1 \
		childInfo:"Higher Detail (1 per obj)"\
		maxChildren:1,
	
	RsSceneLinkTypeStruct\
		idx:LinkType_RENDERSIM\
		name:"Render_Sim"\
		dotnetEnumName:"LinkChannelRenderSim"\
		info:"Render - Children; Simulation (map object) - Parent"\
		parentInfo:"Simulation (map object)"\
		childInfo:"Render"\
		mapObjIsChild:False,
		
	RsSceneLinkTypeStruct\
		idx:LinkType_ClothCollision\
		name:"Cloth_Collision"\
		dotnetEnumName:"LinkChannelClothCollision"\
		info:"Collision - Children; Cloth - Parent"\
		parentInfo:"Cloth"\
		childInfo:"Collision"\
		mapObjIsChild:False,
		
	RsSceneLinkTypeStruct\
		idx:LinkType_CombinerMesh\
		name:"CombinerMesh"\
		dotnetEnumName:"LinkChannelCombinerMesh"\
		info:"Used to set up a prop's ComboLodder source-meshes"\
		parentInfo:"1st: Lower Detail  2nd: Tiltable Lower Detail"\
		maxParents:2\
		childInfo:"Higher Detail (1 per obj)"\
		maxChildren:1,
		
	RsSceneLinkTypeStruct\
		idx:LinkType_ShadowMesh\
		name:"ShadowMesh"\
		dotnetEnumName:"LinkChannelShadowMesh"\
		info:"Reflection-proxy/ShadowMesh needs to be PARENT"\
		parentInfo:"ShadowMesh"\
		childInfo:"",
		
	RsSceneLinkTypeStruct\
		idx:LinkType_CustomHelperLink\
		name:"CustomHelperLink"\
		dotnetEnumName:"LinkChannelCustomHelper"\
		info:"Our helper needs to be the CHILD"\
		parentInfo:""\
		childInfo:"Helper"
		
)

(
	-- Re-order list, ordered by item.index:
	local newList = #()
	for item in gRsSceneLinkTypeData do 
	(
		newList[item.idx + 1] = item
	)
	
	-- Fill undefined gaps with default items:
	for n = 1 to newList.count do 
	(
		if (newList[n] == undefined) do 
		(
			newList[n] = RsSceneLinkTypeStruct\
			idx:(n - 1)
		)
	)
	
	gRsSceneLinkTypeData = newList
)

(
	local channelType = dotnet.getType "RSG.ObjectLinks.LinkChannel"
	local enumClass = (dotnetclass "Enum")
	for item in gRsSceneLinkTypeData do 
	(
		RsSceneLink.SetChannelName item.idx item.name
		try (
			if undefined!=item.dotnetEnumName then
				item.dotnetEnum = enumClass.parse channelType item.dotnetEnumName
		)
		catch(
			print (GetCurrentException())
		)
	)
)
