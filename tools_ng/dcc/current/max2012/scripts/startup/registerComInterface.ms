/*
Registers a function with the OLE interface to process external commands.

Author: Jason Hayes <jason.hayes@rockstarsandiego.com>
*/

(
	try (
		-- This registers 3dsmax as an OLE application.  This really only needs to run once after 3dsmax is installed.
		local reg_key
		local max_version = ( (maxVersion() )[ 1 ] / 1000 ) as string
		
		fn createRegKey hkey key_name &reg_key key_value_name key_value_type key_value =
		(
			registry.createKey hkey key_name key:&reg_key
			registry.setValue reg_key key_value_name key_value_type key_value
		)

		fn writeSubKeyData reg_key sub_key_name sub_key_type sub_key_value =
		(
			local sub_key
			registry.createKey reg_key sub_key_name key:&sub_key
			registry.setValue sub_key "" sub_key_type sub_key_value
		)

		-- Establish a root key for generalized Max data
		createRegKey HKEY_CLASSES_ROOT "MAX.Application" &reg_key "" #REG_SZ "OLE Automation MAX Application"

		-- Add the Clsid information
		writeSubKeyData reg_key "Clsid" #REG_SZ "{7FA22CB1-D26F-11d0-B260-00A0240CEEA3}"

		-- Add the CurVer information
		writeSubKeyData reg_key "CurVer" #REG_SZ ( "MAX.Application." + max_version )

		-- Establish a new root key for the version of Max being used
		createRegKey HKEY_CLASSES_ROOT ( "MAX.Application." + max_version ) &reg_key "" #REG_SZ ( "OLE Automation MAX " + max_version + ".0 Application" )

		-- Add the Clsid information
		writeSubKeyData reg_key "Clsid" #REG_SZ "{7FA22CB1-D26F-11d0-B260-00A0240CEEA3}"

		-- Make a new root key for the CLSID data
		createRegKey HKEY_CLASSES_ROOT "CLSID\{7FA22CB1-D26F-11d0-B260-00A0240CEEA3}" &reg_key "" #REG_SZ ( "OLE Automation MAX " + max_version + ".0 Application" )

		-- Add sub key data
		writeSubKeyData reg_key "ProgID" #REG_SZ ( "MAX.Application." + max_version )
		writeSubKeyData reg_key "VersionIndependentProgID" #REG_SZ "MAX.Application"
		
	) catch (
		print "[TechArt] An unknown error occured while attempting to register 3dsmax as an OLE application."
	)
)



fn ComInterfaceReturnValue val = (
	allowedReturnTypes = #( String, Integer, Float, BooleanClass, Array, Color )

	if ( findItem allowedReturnTypes ( classof val ) == 0 ) then (
		val = val as String
		
	) else if ( classof val == Color ) then (
		val = #( ( val.red as integer ), ( val.green as integer ), ( val.blue as integer ) )
	)

	return val
)

fn ComInterfaceCommand cmd = (
	try (
		val = execute cmd
		
	) catch (
		exception = getCurrentException()
		format "[COM Interface Command ERROR] %\n" exception
		return ( "[MXSException] " + exception )
	)

	-- Command executed, so format the value into something that can be returned to Ruby.
	if ( classof val == Array ) or ( classof val == ArrayParameter ) then (
		new_arr = #()
		
		-- Array
		if ( val.count == 0 ) then (
			val = "[MXSEmptyArray]"
			
		) else (
			for i in 1 to val.count do (
				new_arr[ i ] = ComInterfaceReturnValue val[ i ]
			)
			
			val = new_arr
		)
	) else (
		new_val	= ComInterfaceReturnValue val
		val		= new_val
	)

	return val
)

registerOLEInterface #( ComInterfaceCommand )
