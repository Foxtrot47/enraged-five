--
-- File:: pipeline/util/config.ms
-- Description:: Helper functions for dealing with RSG.Base.Configuration config objects.
--
-- Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
-- Date:: 19 October 2012
--

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global gEditorApp

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
--fn	RsInitialiseEditorApplication
--desc	Initialises the editor framework application.
--
fn RsShutdownEditorApplication =
(
	if (gEditorApp != undefined) then
	(
		gEditorApp.OnShutdown()
		gEditorApp = undefined
	)
)

--
--fn	RsInitialiseEditorApplication
--desc	Initialises the editor framework application.
--
fn RsInitialiseEditorApplication = 
(
	if (gEditorApp == undefined) then
	(
		gEditorApp = DotNetObject "RSG.Editor.Autodesk3dsmax.Autodesk3dsmaxApplication"
		gEditorApp.OnEntry()
	)
)
callbacks.removeScripts id:#RsEditorApplication
callbacks.addScript #preSystemShutdown "RsShutdownEditorApplication()" id:#RsEditorApplication

PopPrompt()
