macroScript LoadBitmapInPhotoshop
category:"Grants" toolTip:"LoadBitmapInPhotoshop" 
Icon:#("Grants",5) 
(
	-- make sure you put a shortcut to photoshop in your max directory - named 'Photoshp.exe' (note the spelling)
	a = medit.getcurmtl()
	if classof a == bitmaptexture do 
	(
		shelllaunch (getDir #maxroot + "Photoshp.exe.lnk") a.filename
	)
)
