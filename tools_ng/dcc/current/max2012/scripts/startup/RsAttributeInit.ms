-- Rockstar Attribute callback initialisation
-- Rockstar North
-- 15/5/2006
-- by Gunnar Droege

-- Create and set the function to populate north attribute editor controls

-- enum in x:\gta5\src\dev\rage\base\tools\dcc\max\rageMaxDataStore\script.cpp
--enum eAttrFunctorType
--{
	eFunctorInit = 0
	eFunctorChange = 1
--	eFunctorNum
-- };

-----------------------------------------------------------------------------
-- .Net Initialisation
-----------------------------------------------------------------------------
dotNet.loadAssembly "System.Xml.dll"

fn getControlEntries attributeName = 
(
	case attributeName of
	(
		"Glass type":
		(
			local glasspath = RsConfigGetCommonDir core:true + "data/Glass/*"
			local glassTypes = getdirectories glasspath
			if glassTypes.count==0 then
			(
				format "You don't appear to have the path % on your machine\n" glasspath
				return ""
			)
			local returnString = ""
			for typePath in glassTypes do 
			(
				local folders = filterstring typePath "\\"
				returnString += (folders[folders.count]+",")
			)
			--format "glassTypes: %\n" returnString
			return returnString
		)
		default: return false
	)
)

/*fn getModelSetControlEntries attributeName = 
(
	case attributeName of
	(
		"Model Set":
		(
			local xmlFile = RsConfigGetProjRootDir() + "build/dev/common/data/ai/vehiclemodelsets.meta"
			xmlDoc = dotNetObject "System.Xml.XmlDocument"
			local modelsets = ""
			if doesFileExist xmlFile then
			(
				xmlDoc.Load xmlFile
				setIter = (xmldoc.selectnodes "//CAmbientModelSets/ModelSets/Item/Name").GetEnumerator()
				while setIter.MoveNext() do
				(
					modelsets += (setIter.Current.InnerText + ",")
				)
			)
			
			return modelsets
		)
		
		default: return false
	)
)*/

fn printChange objectName attrname = 
(
	print (objectName+", "+attrName as string)
)

if (SetAttrFunc != undefined) then
(
	SetAttrFunc "Gta Object" eFunctorInit "Glass type" "getControlEntries"
	
	SetAttrFunc "Gta Object" eFunctorChange "looped" "printChange"
	SetAttrFunc "Gta Object" eFunctorChange "animState" "printChange"
	SetAttrFunc "Gta Object" eFunctorChange "groupName" "printChange"
	
	-- AJM: Disabling this for now as we can't set an empty String (the default attribute) to be in the list here
	-- Since it's too close to the end to be changing the plugin, we're going to just make a CarGen rollout to set
	-- this Model Set and Model attributes from.
	--SetAttrFunc "Gta CarGen" eFunctorInit "Model Set" "getModelSetControlEntries"
)