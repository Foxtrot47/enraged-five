--
-- File:: pipeline/import/importnotenodes.ms
-- Description:: Import Note Nodes from an xml file
--
-- Author:: Adam Munson <adam.munson@rockstarnorth.com>
-- Date:: 5/10/10
--
-----------------------------------------------------------------------------
fn RsNoteNodesImport = 
(
	nodeSet = #()
	RsNoteNodeGroup = #()
	commonDir = RsConfigGetCommonDir()
	commonDir += "data/node_routes/"

	nodefilename = getOpenFileName caption:"Note Nodes file to open" filename:commonDir types:"Note Nodes file (*.xml)|*.xml"

	if (nodefilename == null) do
	(
		return false
	)
	
	-- Global used so that the event handler for node creation doesn't fire
	-- when importing
	RsNoteNodeImporting = true
	
	nodefile = XmlDocument()    
	nodefile.init()
	nodefile.load nodefilename

	rootXmlNode = nodefile.document.DocumentElement
	rootSetList = rootXmlNode.ChildNodes

	local nodeLinkNums = #()
	
	for setNum = 1 to ( rootSetList.Count ) do
	(
		local setXmlNode = rootSetList.ItemOf( setNum - 1 )
		if ( "Set" == setXmlNode.name ) then
		(
			local childNodeBList = setXmlNode.ChildNodes
			for setNodeNum = 1 to ( childNodeBList.Count ) do
			(
				local nodeChild = childNodeBList.ItemOf( setNodeNum - 1 )
				if ( "Node" == nodeChild.name ) then
				(
					local value_x 			= ( nodeChild.Attributes.ItemOf("x") ).value as float
					local value_y 			= ( nodeChild.Attributes.ItemOf("y") ).value as float
					local value_z 			= ( nodeChild.Attributes.ItemOf("z") ).value as float
					local value_pos 		= [value_x, value_y, value_z]
					local value_size 		= ( nodeChild.Attributes.ItemOf("size") ).value as float
					local value_label 		= ( nodeChild.Attributes.ItemOf("label") ).value as string
					local value_link 		= ( nodeChild.Attributes.ItemOf("link") ).value as integer
					local value_type 		= ( nodeChild.Attributes.ItemOf("type") ).value as integer
					local value_colour 	= ( nodeChild.Attributes.ItemOf("colour") ).value as integer
					
					local elem_blockName = nodeChild.Attributes.ItemOf("blockName")
					local value_blockName 	= if (elem_blockName == undefined) then "undefined" else (elem_blockName.value as string)

					value_colour 	+= 1	--Increment as colours in game code start at 0
					value_type		+= 1	-- Making everything 1 based to match MAX

					-- Increment only if it's a valid link value
					if (value_link != -1 ) do
					(
						value_link	+= 1
					)

					noteNode = RsNoteNode()
					noteNode.pos 		= value_pos
					noteNode.nodeSize	= value_size
					noteNode.nodeLabel 	= value_label
					noteNode.nodeType	= value_type
					noteNode.wirecolor	= RsNoteNodeSettings.coloursList[value_colour]
					noteNode.colourIdx	= value_colour

					tempBlockName	 		= toUpper(value_blockName)
					noteNode.blockName	= substituteString tempBlockName "_PROPS" ""

					noteNode.setNumber 	= setNum
					noteNode.nodeNumber	= setNodeNum
					RsNoteNodeNum 			= setNodeNum
					nodeLinkNums[setNodeNum] = value_link

					append nodeSet noteNode
				)
				else 
				(
					format "Invalid child found, looking for Node\n"
				)			
			) -- Node loop

			-- Links are only within sets, so check links and setup links here before next set is checked
			for nn = 1 to nodeSet.count do 
			(
				local linkNum = nodeLinkNums[nn]
				
				if (linkNum != -1 ) do
				(
					currentNode = nodeSet[nn]
					linkedNode = nodeSet[linkNum]
					
					-- Add the actual node link in MAX, as now we know the RsNoteNode
					-- object should all exist in this set
					currentNode.nodeLink = linkedNode					
				)
			)
			append RsNoteNodeGroup nodeSet
			nodeSet = #()
		)
		else 
		(
			format "Invalid child found, looking for Set\n"
		)
		
		RsNoteNodeImporting = false
	) -- Set loop
) -- RsNoteNodesImport