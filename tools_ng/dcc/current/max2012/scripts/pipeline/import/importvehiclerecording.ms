--
-- File:: rockstar/import/loadvehiclerecording.ms
-- Description:: Loads vehicle recording .ivr files
--
-- Author:: Adam Munson <adam.munson@rockstarnorth.com
-- Date:: 31/5/2010
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
--filein "pipeline/util/file.ms" -- loaded on startup by startup.ms

-----------------------------------------------------------------------------
-- Structure Definitions
-----------------------------------------------------------------------------

--
-- struct: RsVehRecFrame
-- desc: Stores all info for a frame from a vehicle recording .ivr file.  
--
struct RsVehRecFrame (

	timeInRecording,	-- Unsigned Long
	speed,				-- Point3
	matrix_a,			-- Point3
	matrix_b,			-- Point3
	steerAngle,
	gas,
	brake,
	handBrake,
	coords				-- Point3
)

struct RsVehicleRecording (

	recordingFilename, vehicle, vehRecording = #(),
	--
	--	fn:		RsLoadVehicleRecording
	--	desc:	Loads the passed in .ivr filename, and creates a RsVehRecFrame struct for each
	-- 			frame as well as using the coords to create a spline to show the path
	--
	fn LoadVehicleRecording = (

		frameNumber = 1
		endOfFile = false
		name = (RsRemovePathAndExtension recordingFilename)
		
		-- If recording has already been loaded, get the user to delete the existing spline
		-- rather than delete it for them incase it was a mistake on their part.
		for obj in $objects do (
		
			if (obj.name == name ) do (
				
				messagebox "This vehicle recording has already been loaded - delete existing spline and vehicle then try again"
				delete vehicle
				return false
			)
		)
		
		-- Spline of the vehicle recording path
		vehSpline = SplineShape()		
		vehSpline.name = name
		splineIndex = addNewSpline vehSpline

		try (
		
			file = fopen recordingFilename "rb"
			
			-- There is no eof function for binStreams, only fileStreams
			while (not endOfFile) do (
			
				-- EOF check implemented here so that there isn't an exception when trying to convert
				-- speed X, Y, Z into floats for the Point3 speed.
				timeInRec = (ReadLong file #unsigned)
				if (timeInRec == undefined) do (
				
					endOfFile = true
					continue
				)
				
				-- RsVehRecFrame Struct
				newFrame = RsVehRecFrame timeInRecording:timeInRec \
					speed:[(ReadShort file), (ReadShort file), (ReadShort file)] \	
					matrix_a:[(ReadByte file), (ReadByte file), (ReadByte file)] \
					matrix_b:[(ReadByte file), (ReadByte file), (ReadByte file)] \
					steerAngle:(ReadByte file)	\	
					gas:(ReadByte file)			\
					brake:(ReadByte file)		\
					handBrake:(ReadByte file)	\
					coords:[(ReadFloat file), (ReadFloat file), (ReadFloat file)]
								
				--Uncompress the matrix values, as in record.cpp
				newFrame.matrix_a /= 127				
				newFrame.matrix_b /= 127
				
				vehRecording[frameNumber] = newFrame
				frameNumber += 1				
				
			)
			fclose file	
		)
		catch (
		
			format "Exception reading in .ivr file:  %\n" (getCurrentException())
			messagebox "There was a problem reading the .ivr file, check the Listener"
			fclose file	
			return false
		)		

		-- Animation using each frame loaded from file		
		animate on (
			
			currentFrame = 0f			
			oldAngle = offsetAngle
			for i = 1 to vehRecording.count do (
			
				currentFrame = ((vehRecording[i].timeInRecording / 1000.0) * framerate) as time				
				at time currentFrame (
				
					mat_a = vehRecording[i].matrix_a
					mat_b = vehRecording[i].matrix_b
					mat_c = normalize (cross mat_a mat_b)				
					mat_d = vehRecording[i].coords
					mat = matrix3 mat_a mat_b mat_c mat_d			
									
					vehicle.transform = mat											
				)
				
				-- Use loop to update the spline too
				addKnot vehSpline splineIndex #smooth #line vehRecording[i].coords			
			)
			animationrange = interval 0f currentFrame
			slidertime = 0f
		)
		
		updateShape vehSpline		
	),
	on create do LoadVehicleRecording()
)

--------------------------------------------------------------
-- Beginning of execution
--------------------------------------------------------------
fn RsImportVehicleRecording recordingFilename = (
		
	-- If a vehicle was loaded from the maxmerge then it should be the only object selected.
	-- If nothing is selected, fall back to a basic box
	if ( $ != undefined ) then (
	
		vehRecording = RsVehicleRecording recordingFilename $
	)
	else (
		
		defaultBox = box height:1 width:1 length:2
		defaultBox.name = "vehicleRecordingBox"
		vehRecording = RsVehicleRecording recordingFilename defaultBox
	)
	
)