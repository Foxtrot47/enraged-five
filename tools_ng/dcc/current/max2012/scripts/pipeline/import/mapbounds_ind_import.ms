--
-- File:: mapbounds_ind_import.ms
-- Description:: Map bounds independent file (.bnd) file importer.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 28 October 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
RsMapBoundsVersion = "1.10"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsPlaceHelpersAtSSVerts
-- desc: place heleprs where the second surface verts are
--
fn RsPlaceHelpersAtSSVerts verts fs = (
	
	for i=1 to fs.count do (
		
		if fs[i] != 0 then (
			
			hpos = verts[i]
			hpos.z = hpos.z + fs[i]
			h = Dummy()
			h.boxsize = boxsize = [.5,.5,.5]
			h.pos = hpos
		)
		
	)
)

--
-- name: RsMapBoundsImportSingleBound
-- desc:
--
fn RsMapBoundsImportSingleBound fstream filepath &doSurface:undefined = 
(
	local verts = #()
	local tris = #()
	local matcols = #()
	local matidx = #()
	local paletteids = #()
	local fs = #()
	local boxes = #()
    local capsules = #() -- cylinders are flat capsules so i treat them the same
	local allCollision = #()
	
	local filename = getFilenameFile filepath
	
	-- Parse file line-by-line
	data = readline fstream
	materialtokens = #()
	data = filterstring data " "
	
	while ( not eof fstream ) and ( data[1] != "bound:" ) do (	
		--print data[1] 
		/*if ismaterialline == true then
		(
			idtoken = filterstring materialtokens[6] "_"
			--print ( idtoken[1] as integer )
			append paletteids (( idtoken[1] as integer ))
		)
		else */if ( data[1] == "v" ) then
			append verts [ data[2] as float, data[3] as float, data[4] as float ]
		
		else if ( data[1] == "tri" ) then
		(
			v1 = ( data[2] as integer ) + 1
			v2 = ( data[3] as integer ) + 1
			v3 = ( data[4] as integer ) + 1
			 
			append tris [ v1, v2, v3 ]
			append matidx ((data[5] as integer) + 1)
		)
		else if ( data[1] == "quad" ) then
		(
			v1 = ( data[2] as integer ) + 1
			v2 = ( data[3] as integer ) + 1
			v3 = ( data[4] as integer ) + 1
			v4 = ( data[5] as integer ) + 1
		
			append tris [ v1, v2, v3 ]
			append tris [ v1, v3, v4 ]
		)
		else if ( data[1] == "box" ) then
		(
			v1 = ( data[2] as integer ) + 1
			v2 = ( data[3] as integer ) + 1
			v3 = ( data[4] as integer ) + 1
			v4 = ( data[5] as integer ) + 1			 
			
			append boxes [ v1, v2, v3, v4 ]
			append matidx ((data[6] as integer) + 1)
		)
		else if ( data[1] == "f" ) then
		(
			
			append fs ( data[2] as float )
		)
        else if (( data[1] == "capsule" ) or ( data[1] == "cylinder" )) then
        (
            v1 = ( data[2] as integer ) + 1
            v2 = ( data[3] as integer ) + 1
            radius = ( data[4] as float )
            
            append capsules [ v1, v2, radius ]
        )
		/*
		else if ( data[1] == "materialcolor" ) then
		(
			r = ( data[2] as integer )
			g = ( data[3] as integer )
			b = ( data[4] as integer )
			a = ( data[5] as integer )
			--print( r as string )
			--append matcols [ r, g, b, a ]
			--print r
			--print (r/256.0)
			append matcols (color r g b)
		)
		*/
		-- Read next line
		
		data = readline fstream
		currentline = data
		data = filterstring data " "
		
		/*
		ismaterialline = false
		materialtokens = filterstring currentline "|"
		if materialtokens.count > 5 then ismaterialline = true
		*/
	)
	format "Bound: % verts, % tris\n" verts.count tris.count
	
	coll = mesh vertices:verts faces:tris
	coll.name = filename
	append allCollision coll
	
	for boundbox in boxes do (
		local minVal = 999999
		local maxVal = -999999
		local vmin = [minVal,minVal,minVal]
		local vmax = [maxVal,maxVal,maxVal]
		for t=1 to 4 do
		(
			if verts[boundbox[t]].x <vmin.x then
				vmin.x = verts[boundbox[t]].x
			if verts[boundbox[t]].x >vmax.x then
				vmax.x = verts[boundbox[t]].x

			if verts[boundbox[t]].y <vmin.y then
				vmin.y = verts[boundbox[t]].y
			if verts[boundbox[t]].y >vmax.y then
				vmax.y = verts[boundbox[t]].y

			if verts[boundbox[t]].z <vmin.z then
				vmin.z = verts[boundbox[t]].z
			if verts[boundbox[t]].z >vmax.z then
				vmax.z = verts[boundbox[t]].z
		)
		
		width = abs(vmax.x - vmin.x)
		height = abs(vmax.y - vmin.y)
		depth = abs(vmax.z - vmin.z)
		
		pos = point3 (vmin.x + (width/2)) (vmin.y + (height/2)) (vmin.z + (depth/2))
		mybox = box length:height width:width height:depth 
		CenterPivot mybox
		mybox.pos = pos
		mybox.name = uniqueName (filename+"_box")
		append allCollision mybox
	)
    
    for capsule in capsules do
    (
        radius = capsule[3] -- the radius is stored in the last component of the vector
        
        width   = abs( verts[capsule[1]].x - verts[capsule[2]].x )
		height  = abs( verts[capsule[1]].y - verts[capsule[2]].y )
		depth   = abs( verts[capsule[1]].z - verts[capsule[2]].z )
        
        if ( width  < 0.0001 ) then width  = width  + ( radius * 2 )
        if ( height < 0.0001 ) then height = height + ( radius * 2 )
        if ( depth  < 0.0001 ) then depth  = depth  + ( radius * 2 )
        
        pos = point3 (( verts[capsule[1]].x + verts[capsule[2]].x ) / 2) (( verts[capsule[1]].y + verts[capsule[2]].y ) / 2) (( verts[capsule[1]].z + verts[capsule[2]].z ) / 2)

        mybox = box length:height width:width height:depth 
		CenterPivot mybox
		mybox.pos = pos
		mybox.name = uniqueName (filename+"_capsule")
		append allCollision mybox
    )
	
	/*
	for i=1 to coll.numfaces do (
		if ( paletteids.count >= matidx[i]) do ( 
		
			if (paletteids[matidx[i]] > 0) do (
			
				if matcols[paletteids[matidx[i]]] != undefined then (
					--print i
					meshop.setFaceColor coll 11 #{i} matcols[paletteids[matidx[i]]]
				)
			)
		)
	)
	
	if ((doSurface == undefined) and (doSurface = (querybox "Do you want to import second surface data?" title:"Query: Import second-surfaces?"))) or doSurface do 
	(
		RsPlaceHelpersAtSSVerts verts fs
	)
	*/
	return ( allCollision )
)

--
-- name: RsMapBoundsImport
-- desc:
--
fn RsMapBoundsImport filename &doSurface:undefined = 
(
	if ( undefined == filename ) then
		return ( false )
		
	file = openFile filename
	if ( undefined == file ) then
		return ( false )
	
	colls = #()
	
	-- Parse file line-by-line
	while not eof file do 
	(	
		data = readline file
		data = filterstring data " "
		
		local coll = RsMapBoundsImportSingleBound file filename doSurface:&doSurface
		join colls coll
	)
	
	select colls
	
	close file
	return ( true )
)

-- mapbounds_ind_import.ms

fn RsDebugBNDImport = 
(
	filenames = ::RSgetOpenFilenames caption:"Open independent bounds files" types:"Independent bounds file (*.bnd)|*.bnd" filename:(::RsConfigGetStreamDir())
	
	if (filenames != undefined) do 
	(
		format "Importing bounds file: %\n" filename
		
		local doSurface
		for filename in filenames do 
		(
			::RsMapBoundsImport filename doSurface:&doSurface
		)
	)
)