--
-- File:: pipeline/export/weapons/utils.ms
-- Description:: Weapon exporter utilities.
--
-- Author:: David Evans <david.evans@rockstarnorth.com>
-- Date:: 16 June 2011
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/rb_image.ms"
filein "pipeline/export/weapons/validation.ms"
filein "pipeline/util/rb_script_generator.ms"
filein "pipeline/util/texturemetatag_utils.ms"
filein "pipeline/util/p4_utils.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
--Name:	GetWeaponTCSDir
--Desc:	Returns the TCS directory of the weapon being exported (Also makes sure that this
--             directory exists.
--
fn GetWeaponTCSDir weaponName = (
	
	dir = RsConfigGetAssetsDir()	
	dir += "metadata/textures/weapons/" 
	dir += weaponName + "/"
	
	RsMakeSurePathExists dir
	
	dir
)

--
--Name:	WeaponsTCSFilePerforceCheckout
--Desc:	Syncs to the given tcs directory and makes sure if the overwrite flag is set the
--			textures are checked out for edit. (If any textures exist)
--
fn WeaponsTCSFilePerforceSyncAndCheckout tcsDir overwriteTCS:false = (

	if (RsGenerateWeaponTCSFiles == true) then
	(
		gRsPerforce.run "sync" #( "-f", ( tcsDir + "..." ) ) silent:true 	
		
		if ( true == overwriteTCS ) do
		(
			gRsPerforce.run "edit" #( ( tcsDir + "....tcs" ) ) silent:true
		)
	)
)

--
--name:	GetTCSFilenames
--Desc:	Returns a list of filenames in the right texture metadata
--		folder depending on player or component ped export.
--
fn GetTCSFilenames objectName texMapList  = (

	filenameList = #()
	
	for i = 1 to texMapList.count do
	(
		tcsFilename = RsConfigGetAssetsDir()	
		tcsFilename += "metadata/textures/weapons/"
		tcsFilename += objectName + "/" + ( RsStripTexturePath( texMapList[i] ) )+ ".tcs"
		
		append filenameList tcsFilename
	)
	filenameList
)

--
--Name:	PedTCSFilePerforceAddAndRevert
--Desc:	Passed a directory which it gathers all the .tcs files from and checks
--			if they should be added to perforce in the default changelist.
--Note:	Call this after the RsWeaponExportTextures function as by then tcs files will exist
--
fn WeaponTCSFilePerforceAddAndRevert tcsDir = (
	
	if (RsGenerateWeaponTCSFiles == true) then
	(
		-- See if any new tcs files were created.  Give it an array of files after
		-- finding out what tcs files exist in the folder
		tcsFileList = RsFindFilesRecursive tcsDir "*.tcs"
		if ( tcsFileList.count > 0 ) do
		(
			gRsPerforce.run "add" tcsFileList silent:true
		)
		
		-- Revert unchanged .tcs files which would have been checked out 
		--
		-- Since we turned off editing all tcs files in this ped's folder this isn't really
		-- necessary so disabling just now incase we want to edit the tcs files in future
		-- through this way.
		gRsPerforce.run "revert" #("-a", "-c", "default", ( tcsDir + "....tcs" ) ) silent:true
	)
)

--
-- Name:	RsWeaponExportTextures
-- Desc: 	
--
fn RsWeaponExportTextures texmaplist isbumplist exportDir objectName changelistNumber \
	texmapobjlist:undefined maptypelist:undefined \
	overwriteTCS:false texturetemplatelist:undefined = 
(	
	local texturesFileName = exportDir + "entity.textures"
	
	local idxTemplate = getattrindex "Gta Texture" "Processing Template"
	
	-- Start by checking that the tex dimensions are valid
	if not RsCheckTexDimension texmaplist then
		return false
	if (gRsULog.Validate()) then
	(				
		RsMakeSurePathExists texturesFileName
		texturesFile = openfile texturesFileName mode:"w+"
		
		local fileListForScript = ""

		for i = 1 to texmaplist.count do 
		(			
			texmap = texmaplist[i]
			isbump = isbumplist[i]
			stripName = RsStripTexturePath(texmap)
			singleName = (filterstring texmap "+")[1]
		
			foundFiles = getfiles singleName
			exported = false
			
			format "WEAPON TEXTURE: % [isbump: %] [files: %]\n" texmap isbump foundFiles.count

			if foundFiles.count > 0 then 
			(
				outputFile = (exportDir + stripName + ".dds")
				tcloutputFile = (exportDir + stripName + ".tcl")
				tcsFile = (GetTCSFilenames objectName #(texmap))[1]
				if ( ( RsFileExist tcsFile == false ) or true == overwriteTCS and RsGenerateWeaponTCSFiles == true ) then
				(
					gRsPerforce.add_or_edit #( tcsFile ) silent:true queueAdd:false
					gRsPerforce.addToChangelist changelistNumber tcsFile
				)
				
				if texmapobjlist != undefined then
				(					
					texmapobj = texmapobjlist[i]
					typename = ""
					
					if ( maptypelist != undefined ) do
					(
						typename = RsResolveTexMapTypeName maptypelist[i]
					)
					local usersettypename = getattr texmapobj idxTemplate
					if ( usersettypename != "" ) then
					(
						-- Texmap's 'Process Template' attribute overrides default template:
						typename = usersettypename			
					)
					else
					(
						-- Make sure that RsResolveTexMapTypeName didn't return
						-- undefined as we don't want to set the attribute to that
						if ( undefined != typename ) do
						(
							setattr texmapobj idxTemplate typename
						)
					)
					-- If the typename was set to undefined then put it back to empty string
					if ( undefined == typename ) do ( typename = "" )
					tcstemplate = ""
					if texturetemplatelist != undefined and texturetemplatelist[i] != undefined and texturetemplatelist[i] != "" then (
						if findString texturetemplatelist[i] "/" == undefined then (
							tcstemplate = RsConfigGetTCPWeaponsRoot() + texturetemplatelist[i]
						)
						-- If our path is relative it needs to be applied to the root
						else (
							tcstemplate = RsConfigGetTCPRoot() + texturetemplatelist[i]
						)						
						tcstemplate = substituteString tcstemplate (RsConfigGetAssetsDir()) "$(core_assets)/"
					)
					else (
						--tcstemplate = RsResolveTypeNameToTemplate typename templatesXmlFilename:( RsConfigGetEtcDir() + "/texture/weapontemplates.xml" )	
						gRsULog.LogError ("No template found for " + texmap + " of type " + typename) context:texmap
					)

					if (RsGenerateWeaponTCSFiles == true) then
					(
						if ( overwriteTCS == true ) then
						(						
							RsGenerateTemplateReferenceFile tcsFile tcstemplate sourceTextures:#(texmap)
						)
						else 
						(
							if ( false == doesFileExist tcsFile ) then
							(
								RsGenerateTemplateReferenceFile tcsFile tcstemplate sourceTextures:#(texmap)
							)
						)
					)
					
					-- two levels of indirection: one stored side by side with the texture
					-- the second stored in the templates folder. This is because we don't
					-- really want to be editing the files that are stored side by side with
					-- the textures but we do want to be able to easily change all the glboal templates
					-- without doing a re-export	
					tcsreference = (RsMakeSafeSlashes (RsRemoveExtension tcsFile))
					tcsreference = substituteString tcsreference (RsConfigGetAssetsDir()) "$(assets)/"
					RsGenerateTemplateReferenceFile tcloutputFile tcsreference

					format "% \n" stripName to:texturesFile
					rexExportTexture texmap outputFile raw:true uniLogFile:(gRsUlog.Filename())
					exported = true
				)
				if ( false == exported ) do 
				(
					format "% \n" stripName to:texturesFile
					if isbump then
					(
						RsRexExportBumpTexture texmap outputFile
					) 
					else
					(
						
						RsRexExportTexture texmap outputFile
					)
				)
			)	
			else
			(
				-- Texture does not exist locally so lets error about it to the Universal log.
				gRsULog.LogError ("Texture: " + texmap + " does not exist locally.") context:objectName
			)
		)
		close texturesFile
		
		return (gRsULog.Validate())
	)
	
	print "Returning false"
	false
)

--
--fn	RsWeaponSceneXmlExport
--desc	Exports scene xml for a weapon and checks out for edit/marks for add
--
fn RsWeaponSceneXmlExport sceneName = (

	local sceneXmlFilename = RsConfigGetAssetsDir() + "weapons/" + sceneName + ".xml"
	
	format "Weapon SceneXml: %\n" sceneXmlFilename
	RsMakeSurePathExists sceneXmlFilename
	SceneXmlReloadCfg()

	gRsPerforce.addQueue = #()
	gRsPerforce.add_or_edit sceneXmlFilename queueAdd:true
	
	SceneXmlExport sceneXmlFilename
	format "Weapon SceneXml Exported to %\n" sceneXmlFilename

	gRsPerforce.postExportAdd()
)
