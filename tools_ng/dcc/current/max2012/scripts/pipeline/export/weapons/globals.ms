--
-- globals.ms
-- Weapon Export Global Data
--
-- David Muir <david.muir@rockstarnorth.com>
-- 5 August 2008
--

filein "pipeline/util/shared_texture_list.ms"

RsWeaponObjectList = #()
RsWeaponTagList = #()
global RsWeaponSharedTextureList

RsTextureList = #()
RsTextureLoad = "targa file (*.tga)|*.tga|bitmap file (*.bmp)|*.bmp"

if undefined == RsWeaponSharedTextureList then
	RsWeaponSharedTextureList = SharedTextureList()

RsWeaponsTextureListFilename = RsConfigGetMetadataDir() + "textures/shared_textures/weapons.xml"

-- Flag to see if we should be generating TCS files. RDR3 doesn't require .tcs files to be generated if they are using a material preset template.
global RsGenerateWeaponTCSFiles = (RsProjectConfig.GetBoolParameter "Enable Max Weapon TCS Generation")
if ( RsGenerateWeaponTCSFiles == undefined ) then RsGenerateWeaponTCSFiles = true

-- global.ms
