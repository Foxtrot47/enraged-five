--
-- validation.ms
-- Weapon Export Validation Functions
--
-- David Muir <david.muir@rockstarnorth.com>
-- 5 August 2008
--

filein "pipeline/export/weapons/globals.ms"
filein "pipeline/util/skinutils.ms"
--
-- Name: IsValidBonesRecCheck
-- Desc:
--
fn RsWeaponExportIsValidBonesRecCheck obj = (

	if classof obj == Editable_Mesh or classof obj == Editable_Poly or classof obj == Dummy then (

		objTag = getuserprop obj "tag"

		if objTag != undefined then (

			objTag = RsLowercase(objTag)
			idxFound = finditem RsWeaponTagList objTag

			if idxFound == 0 then 
			(
				append RsWeaponTagList objTag
				append RsWeaponObjectList obj
			) 
			else 
			(
				gRsUlog.LogError ("tag " + objTag + " is repeated in " + obj.name + " and " + RsWeaponObjectList[idxFound].name) context:obj
			)
		)
	)

	for childobj in obj.children do (

		if RsWeaponExportIsValidBonesRecCheck childobj == false then return false
	)

	true
)


fn CheckWeaponRec root = 
(
	local retVal = true
	if "Gta Collision"==(getattrclass root) and (GetCollCostAsFaceCount root)>64 then
	(
		gRsUlog.LogWarning "Collision mesh has more than 64 polys. Check if really required." context:root
		retVal = false
	)

	for childobj in root.children do
		if not CheckWeaponRec childobj then retVal = false
	
	return retVal
)
--
-- Name: IsValidForExport
-- Desc: Check that we have a valid weapon selection for export.
--
fn RsWeaponExportIsValidForExport obj = 
(
	RsWeaponObjectList = #()
	RsWeaponTagList = #()

	if ( "Gta Object" != ( GetAttrClass obj ) ) then (

		gRsUlog.LogError ("object has to be an editable mesh (" + obj.name + ")") context:obj
		return false
	)

	if obj.parent != undefined then (

		gRsUlog.LogError "has to be a root object" context:obj
		return false
	)

	if RsWeaponExportIsValidBonesRecCheck obj == false then 
	(
		return false
	)
	
	if not CheckWeaponRec obj then
		return false

	if (filterstring obj.name " ").count > 1 then (

		gRsUlog.LogError (obj.name + " has spaces in name, not exporting") context:obj
		return false
	)
	
	local errorMessages = #()
 	if not RsSkinUtils.ValidateBonesInHierarchy obj errorMessages do 
	(
--		gRsUlog.LogError ("Unable to export " + selection[1].name )
		for message in errorMessages do
		(
			gRsUlog.LogError message context:obj
		)
 	) 
	
	return true	
)

-- validation.ms
