-- Rockstar Image Building
-- Rockstar North
-- 15/7/2008
-- by Luke Openshaw

-- Greg's original image building script updated to use new content system

include "pipeline/util/rb_script_generator.ms"
include "pipeline/util/ruby.ms"

--------------------------------------------------------------
-- build the independant image and convert it to the platform image
--------------------------------------------------------------
fn RsBuildAndConvertImageUsingScript rbscript projname rubyFileName propExport:false streamedprops:false ironRubyConvertPath:unsupplied = (

	-- build script for creating image
	
	RsMakeSurePathExists rubyFileName
	
	RBGenResourcingScript rubyFileName projname rbscript propExport:propExport streamedprops:streamedprops ironRubyConvertPath:ironRubyConvertPath
	
	if ( RsRunRubyScript rubyFilename ) != 0 then
	(
		messagebox ("Ped resourcing script failed: " + rbscript)
		return false
	)	
	
	-- Support for AP3 for ped exports currently
	if ( ironRubyConvertPath != unsupplied ) do
	(
		args = #()
		if ( RsAutomatedExport ) do ( append args "--nopopups" )
		
		( 0 == ( AP3Invoke.Convert #(ironRubyConvertPath) args:args debug:true) )
	)
	return true
)

--------------------------------------------------------------
-- build the independant image and convert it to the platform image
--------------------------------------------------------------
fn RsBuildAndConvertImage directory rbscript projname dorebuild:false propExport:false streamedprops:false ironRubyConvertPath:unsupplied = (

	if dorebuild then (
	
		RsDeleteFiles (directory + "*.c??")
		RsDeleteFiles (directory + "*.w??")
		RsDeleteFiles (directory + "*.x??")
	)
	
	rubyFileName = RsConfigGetStreamDir() + "/build_and_convert_image.rb"
	
	return (RsBuildAndConvertImageUsingScript rbscript projname rubyFileName propExport:propExport streamedprops:streamedprops ironRubyConvertPath:ironRubyConvertPath)
)