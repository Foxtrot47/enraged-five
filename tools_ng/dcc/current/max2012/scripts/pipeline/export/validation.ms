--
-- File:: pipeline/export/validation.ms
-- Description:: Location for validaiton we might want to perform for any exporter
--
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 10/05/2011
--

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn IsValidCollName collName = (

	retval = false

	if 	collName == "GLASS_WEAK" or \
		collName == "GLASS_MEDIUM" or \
		collName == "GLASS_STRONG" or \
		collName == "WINDSCREEN_WEAK" or \
		collName == "WINDSCREEN" or \
		collName == "WINDSCREEN_STRONG" or \
		collName == "WINDSCREEN_MED_WEAK" or \
		collName == "WINDSCREEN_MED_STRONG" then (
	
		retval = true
	)
	
	retval
)


idxCollType = getattrindex "Gta Collision" "Coll Type"
idxDynamic = getattrindex "Gta Object" "Is Dynamic"
idxFragment = getattrindex "Gta Object" "Is Fragment"

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsObjectSmashableRec model rootobj = (

	glassCount = 0
	foundRexBound = false

	if getattrclass model == "Gta Object" then (
		
		if true then (--RsDoesObjUseGlassShader model then (
		
			for obj in model.children do (

				if getattrclass obj == "Gta Collision" then (

					if classof obj == Col_Mesh then (

						hasGlass = false
						hasOther = false

						if obj.material != undefined then (

							if classof obj.material == MultiMaterial then (

								matList = #()
								RsMatGetMapIdsUsedOnObject obj matList

								for i = 1 to matlist.count do (

									matidx = finditem obj.material.materialIDList matlist[i]

									if matidx != 0 then (

										mat = obj.material.materiallist[matidx]

										if classof mat == RexBoundMtl then (

											foundRexBound = true

											colName = RexGetCollisionName mat									

											if (IsValidCollName colName) then (

												hasGlass = true		
											) else (

												hasOther = true
											)
										)
									)
								)				
							) else if classof obj.material == RexBoundMtl then (

								foundRexBound = true

								colName = RexGetCollisionName obj.material

								if IsValidCollName colName then ( 

									hasGlass = true		
								) else (

									hasOther = true
								)						
							)
						)

						if foundRexBound then (

							if hasGlass then (

								if (hasOther == false) then (

									glassCount = glassCount + 1
								) else (

									gRsUlog.LogError ("glass and non glass collision on object " + model.name) context:model 
								)
							)
						) else (

							colName = getattr obj idxCollType

							if IsValidCollName colName then (

								glassCount = glassCount + 1			
							)
						)
					) else (
					
						colName = getattr obj idxCollType

						if IsValidCollName colName then ( 

							gRsUlog.LogError ("glass set on box collision: " + obj.name) context:obj 
						)
					)
				)
			)
						
			if glassCount == 0 then (
			
				gRsUlog.LogError ("no glass collision on object with glass shader " + model.name) context:model 
			)
						
			if glassCount > 1 then (

				gRsUlog.LogError ("more than one glass collision on object " + model.name) context:model 
				glassCount = 0
			)
		)		
		
		for obj in model.children do (
		
			glassCount = glassCount + RsIsObjectSmashableRec obj rootobj
		)
	)
	
	glassCount
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsObjectSmashable model = (
	
	isSmashable = false

	if getattr model idxDynamic == false then (
	
		gRsUlog.LogWarning ("smashable turned off because " + model.name + " isn't dynamic") context:model 
	) else (

		--if getattr model idxFragment == false then (
		
		--	isSmashable = true
		--) else (
	
			glassCount = RsIsObjectSmashableRec model model
			if glassCount > 0 then (
				isSmashable = true
			) else (
				gRsUlog.LogWarning ("no glass found on " + model.name + " so smashable turned off") context:model 
			)
		--)
	)
	
	isSmashable
)


--
-- name: RsCheckMaterialsRec	
-- desc: Look for standard materials, or nested multimaterials
--
fn RsCheckMaterialsRec obj mat = 
(
	local retVal = true
	
	case classOf mat of 
	(
		MultiMaterial:
		(
			local matIdList = #()
			RsMatGetMapIdsUsedOnObject obj matIdList

			for matNum in matIdList while retVal do 
			(
				matIdx = finditem mat.materialIDList matNum

				if (matIdx != 0) do 
				(
					if (isKindOf mat.materialList[matIdx] Multimaterial) then
					(
						gRsULog.LogError (obj.name + "'s material (" + mat.name + ") has a nested submaterial (" + mat.materialList[matIdx].name + ") - Re-apply the material. (Select the mesh and use the material editor picker to check the exact material on this mesh).") context:obj
						retVal = false
					)
					else
					(
						retVal = RsCheckMaterialsRec obj mat.materialList[matIdx]
					)
				)
			)
		)
		StandardMaterial:
		(
			gRsUlog.logError (obj.name + " has StandardMaterial \"" + mat.name + "\" applied.  Not allowed." ) context:obj
			retVal = false	
		)
	)
	
	return retVal
)

fn RsCheckMaterials obj = 
(
	RsCheckMaterialsRec obj obj.mat
)
