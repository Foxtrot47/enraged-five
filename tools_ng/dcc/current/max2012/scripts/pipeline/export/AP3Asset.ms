--
-- File:: pipeline/export/AP3Asset.ms
-- Description:: AP3 XML Asset Creation
--
-- Author:: Terry Litrenta
--
--

-- Uses
filein "pipeline/util/RsUniversalLog.ms"

--
-- name: AP3AssetFile
-- desc: Helper structure to create an AP3 Asset Pack Schedule file for building
--       with Asset Pipeline 3.
--
struct AP3AssetFile
(
	-- Root XML document elemet that will be written out
	assetXMLDocument,
	-- Root "Assets" XML element that ZipArchive elements are attached to
	assetXmlRoot,
	-- an identifier used to  save a unique file
	assetName = "",
	
	logContext = "Asset Pack",
	
	-- Create the initial document and "Asset" element for everything to be linked to.
	fn Initialize filename: =
	(
		if undefined!=assetXMLDocument then	
			assetXMLDocument.Release()

		assetXMLDocument = XmlDocument()
		assetXMLDocument.init()

		assetXmlRoot = assetXMLDocument.createelement("Assets")
		assetXMLDocument.document.AppendChild assetXmlRoot
		
		if unsupplied!=filename then
			assetName = filename
		else
			assetName = ("Asset_"+(random 0 999) as string)
	),
	
	-- Write out the XML file
	fn WriteXmlFile outputFile =
	(
		local logMessage = stringStream ""
		format "Writing AP3 resource file to %\n" outputFile to:logMessage
		gRsUlog.LogMessage (logMessage as String) context:logContext
		
		RsMakeSurePathExists (getFilenamePath (RsMakeSafeSlashes outputFile))

		assetXMLDocument.save outputFile
		if undefined!=assetXMLDocument then	
			assetXMLDocument.Release()
	),
	
	-- load an XML file for analysis
	fn LoadAssetFile outputFile = 
	(
		-- Parse file to create uber map zip (not sure if this is ideal, or
		-- whether we should build this up as we go through the MaxScript).
		assetXMLDocument.Load( outputFile )
		assetXmlRoot = assetXMLDocument.document.childNodes.item 1
	),
	
	-- Create a new "ZipArchive" element in the XML that directories or files will be attached to.
	fn AddZipArchive filePath forceCreateIfEmpty:false = 
	(
		for zipElemIndex = 0 to assetXmlRoot.childnodes.count-1 collect 
		(
			local zipElem = (assetXmlRoot.childnodes.item zipElemIndex)
			if filePath == ((zipElem.GetAttribute "path") as string) then
			(
				local logMessage = stringStream ""
				format "Asset % was already added. Returning previous XML Element.\n" filePath to:logMessage
				gRsUlog.LogMessage (logMessage as String) context:logContext
				
				return zipElem
			)
		)
		
		zipArchiveElement = assetXMLDocument.createelement("ZipArchive")
		zipArchivePathAttribute = assetXMLDocument.createAttribute("path")
		
		-- Sometimes we'll need to create an empty asset file, ex. empty .itd for ped props with shared textures.
		-- Allow this only if we've forced it on this specific asset.
		if forceCreateIfEmpty then
		(
			zipArchiveEmptyEntriesAttribute = assetXMLDocument.createAttribute("forceCreateIfEmpty")
			zipArchiveEmptyEntriesAttribute.value = "true"
			zipArchiveElement.Attributes.Append zipArchiveEmptyEntriesAttribute
		)
		
		zipArchivePathAttribute.value = filePath
		zipArchiveElement.Attributes.Append zipArchivePathAttribute
		
		assetXmlRoot.AppendChild zipArchiveElement
		
		zipArchiveElement
	),
	
	-- Add a directory to a given "ZipArchive" element.
	-- "Destination" used for drawable dictionaries if you want to specify a folder inside the ZIP file.
	fn AddDirectory archiveElement directory destination: =
	(
		if (archiveElement == undefined) then
		(
			local logMessage = stringStream ""
			format "Invalid XML element passed to AddDirectory" to:logMessage
			gRsUlog.LogError (logMessage as String) context:logContext

			return false
		)
		
		-- Make sure we're attaching to the right element
		if (matchPattern archiveElement.name pattern:"ZipArchive" == false) then
		(
			local logMessage = stringStream ""
			format "Invalid XML element passed to AddDirectory. Need to pass a (ZipArchive) element. (%) element was passed" archiveElement.name to:logMessage
			gRsUlog.LogError (logMessage as String) context:logContext
			
			return false
		)
		
		directoryElement = assetXMLDocument.createelement("Directory")
		directoryPathAtt = assetXMLDocument.createAttribute("path")
		
		directoryPathAtt.value = directory
		directoryElement.Attributes.Append directoryPathAtt
		
		-- Specify if we want the data going into a particular directory inside the ZIP file.
		if (destination != unsupplied) then
		(
			directoryDestinationAtt = assetXMLDocument.createAttribute("destination")
			directoryDestinationAtt.value = destination
			
			directoryElement.Attributes.Append directoryDestinationAtt
		)
				
		archiveElement.AppendChild directoryElement
		
		true
	),
	
	-- Add a file to a given "ZipArchive" element.
	fn AddFile archiveElement filePath destination: =
	(
		if (archiveElement == undefined) then
		(
			local logMessage = stringStream ""
			format "Invalid XML element passed to AddFile" to:logMessage
			gRsUlog.LogError (logMessage as String) context:logContext

			return false
		)
		
		-- Make sure we're attaching to the right element
		if (matchPattern archiveElement.name pattern:"ZipArchive" == false) then
		(
			local logMessage = stringStream ""
			format "Invalid XML element passed to AddFile. Need to pass a (ZipArchive) element. (%) element was passed" archiveElement.name to:logMessage
			gRsUlog.LogError (logMessage as String) context:logContext
			
			return false
		)
		
		fileElement = assetXMLDocument.createelement("File")
		filePathAtt = assetXMLDocument.createAttribute("path")
		filePathAtt.value = filePath
		fileElement.Attributes.Append filePathAtt

		-- Specify if we want the data going into a particular directory inside the ZIP file.
		if (destination != unsupplied) then
		(
			local fileDestinationAtt = assetXMLDocument.createAttribute("destination")
			fileDestinationAtt.value = destination
			fileElement.Attributes.Append fileDestinationAtt
		)
		
		archiveElement.AppendChild fileElement
		
		true
	),
	
	fn AddFilesFromFolder archiveElement folderPath destination: includeExtensions:#() excludeExtensions:#() =
	(
		if folderPath[folderPath.count] != "/" then
			append folderPath "/"
		local allFiles = getFiles (folderPath + "*.*")
		for f in allFiles do
		(
			local fileParts = filterString f "."
			if fileParts.count<=0 then 
				continue
			local fileExt = fileParts[fileParts.count]
			if 	(includeExtensions.count==0 or 0!=findItem includeExtensions fileExt) and
				(excludeExtensions.count==0 or 0==findItem excludeExtensions fileExt) then
			(
				AddFile archiveElement f destination:destination
			)
		)
	),
	
	-- We try to generalise some more automation here.
	-- look fopr textures names, submit changes in <texSourcePath> and only include the tcls...
	--
	--
	-- ** DHM FIX ME: can we please not add Perforce-integration in every single layer of MaxScript.
	--             This should be optional and reserved. **
	--
	fn AddTexDictFromFolder archiveElement folderPath destination: texSourcePath: tcsSourcePath: autoSubmit:true = 
	(
		if folderPath[folderPath.count] != "/" then
			append folderPath "/"
		local texFiles = getFiles (folderPath + "*.dds")
		--B*1398993 disabled automatic data submission
		local clName = "Asset file TXD images"
		local changelistNumber = gRsPerforce.createChangelist clName
		for img in texFiles do
		(
			-- copy image over to global export location
			local filename = getFilenameFile (filenameFromPath img)
			if unsupplied!=texSourcePath then
			(
				local exportImgFile = texSourcePath + filename + ".dds"
				RsMakeSurePathExists texSourcePath
				try(
					gRsPerforce.add_or_edit exportImgFile switches:#("-t", "binary+m")
					gRsPerforce.addToChangelist changelistNumber exportImgFile
					gRsUlog.LogMessage ("Added " + (exportImgFile as string) + " to CL " + (changelistNumber as string))
				)catch(
					gRsUlog.LogWarning ("Texture perforce integration failed:"+getCurrentException())
				)
				local goAhead = true
				if (RsFileExists exportImgFile) then
				(
					local readOnly = getFileAttribute exportImgFile #readOnly
					if readOnly or not deleteFile exportImgFile then 
					(
						gRsUlog.LogWarning ("Couln't delete file "+exportImgFile as string+". Maybe just got added?")
						goAhead = false
					)
				)
				if goAhead then
				(
					if not copyFile img exportImgFile then 
						gRsUlog.LogError ("Couln't copy file from "+img as string+" to "+exportImgFile as string)
					gRsUlog.LogMessage ("Copied "+img as string+" to "+exportImgFile as string)
				)
			)
			-- handle tcs file
			if unsupplied!=tcsSourcePath then
			(
				local exportTcsFile = tcsSourcePath + filename + ".tcs"
				if not doesFileExist exportTcsFile then
				(
					gRsUlog.LogError ("TCS file to add not existent: "+exportTcsFile as string)
				)
				else
				(
					try(
						gRsPerforce.add_or_edit exportTcsFile switches:#("-t", "text")
						gRsPerforce.addToChangelist changelistNumber exportTcsFile
						gRsUlog.LogMessage ("Added " + (exportTcsFile as string) + " to CL " + (changelistNumber as string))
					)catch(
						gRsUlog.LogWarning ("TCS perforce integration failed:"+getCurrentException())
					)
				)
			)
			
			local templateFile = folderPath + filename + ".tcl"
			AddFile archiveElement templateFile destination:destination
			gRsUlog.LogMessage ("Added "+templateFile as string+" to asset XML file.")
		)
		
		--B*1398993 disabled automatic data submission
		if undefined != changelistNumber then
		(
			configExportView = dotnetobject "RSG.Base.ConfigParser.ConfigExportView"
			if( configExportView.GetMaxAutoSubmitsEnabled() ) then
			(
				gRsPerforce.submitOrDeleteChangelist changelistNumber
			)
			else
			(
				gRsPerforce.deleteChangelistIfItHasNoChanges changelistNumber
			)
		)

		gRsPerforce.deleteEmptyChangelists (clName + "*")

	),
	
	fn ValidateXML = 
	(
		local currentElements = #()
		
		for zipElemIndex = 0 to assetXmlRoot.childnodes.count-1 do
		(
			local zipElem = (assetXmlRoot.childnodes.item zipElemIndex)
			
			if zipElem == undefined then
				continue
			
			local pathAttribute = zipElem.Attributes.itemof("path")
			local zipPath = tolower pathAttribute.Value
			
			-- Check to make sure we don't have any duplicate zip files, AP3 will freak out.
			if not appendIfUnique currentElements zipPath then
			(
				gRsULog.LogWarning("Duplicate zip entry in AP3 asset file. Removing - " + zipPath as string)
				local xmlnode = assetXmlRoot.RemoveChild zipElem
			)
			else
			(
				-- Remove the zip element if there are no files unless the "forceCreateIfEmpty" is explicitly set.
				if zipElem.childNodes.count==0 and zipElem.Attributes.itemof("forceCreateIfEmpty") == undefined then
					assetXmlRoot.RemoveChild zipElem
			)
		)
		
		-- DHM FIX ME: log validation disabled.  Any error would cause the Asset Pack XML to fail.
		-- Not ideal... we catch local issues here with warnings and handle them anyway.
		-- gRsULog.Validate()
		true
	),
	
	fn GetCurrentAssetFiles = 
	(
		-- Validate before we return the files because we might end up clearing out some unused assets before building
		ValidateXML()
		
--	potential alternative version for linq analysis
-- 		local zipElements = xmlDoc.document.SelectNodes( "/Assets/ZipArchive/@path" )
-- 		for i = 0 to (zipElements.Count-1) do
-- 		(					
-- 			local zipArchiveAttr = zipElements.ItemOf( i )
-- 			local zipArchiveFilename = zipArchiveAttr.Value
-- 			mapAssetPackScheduleFile.AddFile mapZipArchive zipArchiveFilename
-- 			append map_zip_assets zipArchiveFilename
-- 		)

		for zipElemIndex = 0 to assetXmlRoot.childnodes.count-1 collect 
		(
			local zipElem = (assetXmlRoot.childnodes.item zipElemIndex)
			(zipElem.GetAttribute "path") as string
		)
	),
	
	fn BuildAssetFiles customSavePath: changelist:"default" arguments:#() =
	(
		if unsupplied==customSavePath then
			customSavePath = RsConfigGetCacheDir() + "assetXML/" + assetName+".xml"
		
		ValidateXML()
		RsMakeSurePathExists customSavePath
		WriteXmlFile customSavePath
		
		-- Put together the build arguments we have
		local buildArguments = #("-rebuild")
		join buildArguments arguments
		append buildArguments customSavePath
		
		local buildSuccess = true
		local result = AP3Invoke.Pack #(customSavePath) args:buildArguments debug:true
		print "result"
		print result
		if result!=0 then
			gRsUlog.LogError ("Error while processing command RSG.Pipeline.AssetPack.exe with args: " + (customSavePath as string) + " - is the incredibuild service running?")
		Initialize()
		return gRsUlog.ValidateJustErrors()
	)
	
)

--
-- func: RsAssetPackBuild
-- desc: Run a series of Asset Pack XML files through the build script.
--
fn RsAssetPackBuild filenames = (

	( 0 == AP3Invoke.Pack filenames debug:true )
)

/*
Testing Shit!

assetFile = AP3AssetFile()
assetFile.Initialize()

drawableDictionaryElement = assetFile.AddZipArchive "$(cache)/raw/componentpeds/A_F_M_Beach_01.idd.zip"
textureDictionaryElement = assetFile.AddZipArchive "$(cache)/raw/componentpeds/A_F_M_Beach_01.itd.zip"

assetFile.AddDirectory drawableDictionaryElement "$(cache)/raw/componentpeds/A_F_M_Beach_01/Accs_000_u" destination:"Accs_000_u"
assetFile.AddFile textureDictionaryElement "$(cache)/raw/componentpeds/A_F_M_Beach_01/accs_diff_000_a_uni.tcl"

assetFile.WriteXmlFile "X:\\testAP3.xml"
*/
