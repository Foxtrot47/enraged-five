-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------

global gRsPedZipExtract
global RsPedZipFilters = undefined

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

fn RsPedImageFilename streamedExport:false cutsceneExport:false propExport:false streamedPropExport:false = 
(	
	local filename = case of 
	(
		cutsceneExport:"cutspeds"
		streamedExport:"streamedpeds"		
		streamedPropExport:"streamedpedprops"
		propExport:"pedprops"
		default:"componentpeds"
	)	
	filename
)

fn RSpedImagePaths propExport:false streamedPropExport:false = 
(
	local fileFilters = #()
	local filename = RsPedImageFilename propExport:propExport streamedPropExport:streamedPropExport

	local mapType = "zip"
	local mapInfos = #(RsProjectContentFind filename type:mapType)
	
	if (mapInfos[1] == undefined) do 
	(
		local maps = RsProjectContentFindAll type:mapType
		
		local namePattern = filename + "*"
		
		mapInfos = for mapInfo in maps where (matchPattern mapInfo.name pattern:namePattern) collect mapInfo
	)
	
	local fileList = for mapInfo in mapInfos collect 
	(
		mapInfo.path + "/" + mapInfo.name + ".zip"
	)
	
	if (fileList.count > 1) then 
	(
		sort fileList
	)
	return fileList
)

-- Checks to see if buildfile is writeable, and offers to check it out from Perforce or make it writable if it's readonly:
fn RSpedIsWriteable zipFiles changelistNum: = 
(
	if not isKindOf zipFiles array do 
	(
		zipFiles = #(zipFiles)
	)
	
	local notLocals = for zipFile in zipFiles where not doesFileExist zipFile collect zipFile
		
	if notLocals.count != 0 do 
	(		
		format "Doesn't exist locally, auto-syncing:\n"
		for zipFile in notLocals do (format "\t%\n" zipFile)
		gRsPerforce.sync notLocals force:true
	)
	
	local switches = #()
	if (changelistNum != unsupplied and changelistNum != undefined) then
	(
		switches = #("-c", changelistNum as string)
	)
	
	-- AJM: 637500 was entered for .ped.zips not being asked if the user wanted to check it out (as they had
	-- previously chosen to 'make writable' locally, so this new includeWritable flag looks at all files 
	-- that aren't checked out by the user which should be (in this case it's just for one file, the .ped.zip).
	gRsPerforce.readOnlyP4Check zipFiles switches:switches includeWritable:true
)

struct RsPedZipExtract 
(
	extractContinue = true,
	doProgress = false, 
	progressNow, progressCount, 
	
	statusText = "", 
	
	onlyOverwriteNewer = false,
	
	fn setStatusText filenames success = 
	(
		local newText = stringStream ""
		
		local plural = if filenames.count == 1 then "" else "s"
		
		if success then 
		(
			format "Successfully extracted file%:\n\n" plural to:newText
		)
		else 
		(
			format "Extraction failed on file%:\n\n" plural to:newText
		)
		
		for n = 1 to filenames.count do 
		(
			local filename = filenames[n]
			format "%" (filenameFromPath filename) to:newText
			
			if (n != filenames.count) do 
			(
				format "\n" to:newText
			)
		)
		
		statusText = newText as string
	),
	
	-- Called whenever zip-progress or overwrite-warning events are called by DotNetZip:
	fn zipProgressEvent ev arg =  
	(		
		case of 
		(
			(arg.EventType.equals arg.EventType.Extracting_ExtractEntryWouldOverwrite):
			(
				-- Check to see if existing file's timestamp is the same as the archived one:
				local localFile = arg.ExtractLocation + arg.CurrentEntry.filename
				local zipFileTime = arg.CurrentEntry.LastModified
				local localFileTime = (dotnetClass "System.Io.File").GetLastWriteTimeUtc localFile
				
				local dontOverwrite = if onlyOverwriteNewer then 
				(
					-- Only overwrite if archive-file's timestamp is later than the local one:
					(zipFileTime.compareTo localFileTime) <= 0
				)
				else 
				(
					-- Only overwrite if archive-file's timestamp is different to the local one:
					zipFileTime.equals localFileTime
				)
				
				arg.CurrentEntry.ExtractExistingFile = if dontOverwrite then 
				(
					--format "Keeping: %\n" localFile
					arg.CurrentEntry.ExtractExistingFile.DoNotOverwrite
				)
				else 
				(
					--format "Overwriting: % [local: % - zip:%]\n" (filenameFromPath localFile) (localFileTime.tostring "G") (zipFileTime.tostring "G")
					arg.CurrentEntry.ExtractExistingFile.OverwriteSilently
				)
			)
			
			(arg.EventType.equals arg.EventType.Extracting_AfterExtractEntry):
			(
				if doProgress do 
				(
					--format "[%/%]\n" (progressNow += 1) progressCount
					if not (progressUpdate (100.0 * (progressNow += 1) / progressCount)) do 
					(
						extractContinue = false
						arg.cancel()
					)
				)
			)
		)
	),
	
	-- Asks if user wants to sync filenames:
	fn askToGetQuery filenames = 
	(
		if filenames.count == 0 do return false
		local filePlural = if filenames.count == 1 then "" else "s"
		
		local queryText = stringStream ""
		format "Do you want to update file% from Peforce before extracting?\n\n" filePlural to:queryText
		
		for n = 1 to filenames.count do 
		(
			format "%" (filenameFromPath filenames[n]) to:queryText
			
			if (n != filenames.count) do 
			(
				format "\n" to:queryText
			)
		)
		
		RsQueryBoxTimeOut (queryText as string) title:("Zip Extract: Update File" + filePlural + "?") timeout:20
	),
	
	-- Sync and unzip zipfiles to their extract dirs:
	fn extractFiles filenames extractDirs getLatest:true askToGet:true filters:#() justNewer:false = 
	(
		extractContinue = true
		onlyOverwriteNewer = justNewer
		
		if askToGet do (getLatest = askToGetQuery filenames)

		if getLatest and gRsPerforce.connected() do 
		(
			extractContinue = gRsPerforce.syncChanged filenames silent:true clobberAsk:true
		)
		
		if not extractContinue do 
		(
			statusText = "Sync failed"
			return false
		)

		-- Make a list of files existing before extraction:
		local filesToExtract = #()
		local existingFiles = #()
		local checkedDirs = #()
		
		for dirNum = 1 to extractDirs.count do 
		(
			local extractDir = extractDirs[dirNum]
			
			if (appendIfUnique checkedDirs extractDir) do 
			(
				local dirFilters = #("*.*")
				
				--join existingFiles (RsFindFilesRecursive extractDir dirFilters)
				for dirFilter in dirFilters do 
				(
					join existingFiles (getFiles ( extractDir + dirFilter))
				)
			)
		)
		
		existingFiles = makeUniqueArray existingFiles
		
		for n = 1 to existingFiles.count do 
		(
			existingFiles[n] = toLower (RsMakeBackSlashes existingFiles[n])
		)
		sort existingFiles

		local extractedNums = #{}
		extractedNums.count = existingFiles.count
		
		local zipObjects = #()
		progressNow = 0
		progressCount = 0
		
		local extractFilenums = for fileNum = 1 to filenames.count where (doesFileExist filenames[filenum]) collect fileNum
		
		-- Create dotNetZipObjects, collecting the overall extract-file count:
		for fileNum = extractFilenums do 
		(
			local zipFilename = filenames[filenum]
			
			local zipObj = (dotNetClass "Ionic.Zip.ZipFile").Read zipFilename
			
			progressCount += zipObj.count
			
			zipObjects[fileNum] = zipObj
		)
		
		doProgress = (progressCount > 0)
		
		if doProgress do 
		(
			local progressString = "Extracting " + (progressCount as string) + " archived files:"
			
			progressStart progressString
		)
		
		for fileNum = extractFilenums while extractContinue do 
		(
			local zipObj = zipObjects[fileNum]
			local extractDir = extractDirs[filenum]
			
			try 
			(
				format "Extracting zip: % to: %\n" filenames[filenum] extractDir
				--pushPrompt ("Extracting zip: " + filenames[filenum])
				
				RsDeleteDirectory extractDir
				
				local listEnumerator = zipObj.entryFilenames.getEnumerator()
				for n = 1 to zipObj.entryFilenames.count do 
				(
					listEnumerator.movenext()
					local extractFilename = toLower (RsMakeBackSlashes (extractDir + listEnumerator.current))
					append filesToExtract extractFilename
	
					local existingFileNum = findItem existingFiles extractFilename
					if (existingFileNum != 0) do 
					(
						extractedNums[existingFileNum] = true
					)
				)
				
				dotNet.addEventHandler zipObj "ExtractProgress" zipProgressEvent
				zipObj.ExtractAll extractDir (dotNetClass "Ionic.Zip.ExtractExistingFileAction").InvokeExtractProgressEvent
				
				popPrompt()
			)
			catch 
			(
				format "Extract error: %\n" (getCurrentException())
				extractContinue = false
				popPrompt()
			)

			if (zipObj != undefined) do 
			(
				zipObj.dispose()
			)
		)
		
		if doProgress do 
		(
			progressEnd()
		)
		
		-- Offer to delete any files that weren't found in the archive(s):
		if extractContinue and (extractedNums.numberSet != extractedNums.count) do 
		(
			local extraFiles = for fileNum = -extractedNums collect existingFiles[fileNum]
			
			local filePlural = if extraFiles.count == 1 then "" else "s"
			local dirPlural = if extractDirs.count == 1 then "" else "s"
			
			local queryText = stringStream ""
			format "% unarchived file% found in extraction-folder%:\n\n" extraFiles.count filePlural dirPlural to:queryText
			
			local keepAdding = true
			for n = 1 to extraFiles.count while keepAdding do 
			(
				format "%\n" extraFiles[n] to:queryText

				if (n >= 20) and (n < extraFiles.count) do 
				(
					format "[...more...]\n" to:queryText
					keepAdding = false
				)
			)
			
			local itThem = if extraFiles.count == 1 then "it" else "them"
			local itThey = if extraFiles.count == 1 then "it" else "they"
			
			format "\nDo you want to delete %?\n\n(If not, %'ll be used when you regenerate the zip%)" itThem itThey dirPlural to:queryText
			
			if queryBox (queryText as string) title:"Delete unarchived files?" do 
			(			
				for filename in extraFiles do 
				(
					setFileAttribute filename #readOnly false
					deleteFile filename
				)
				
				-- Remove any empty directories:
				for dir in (makeUniqueArray extractDirs) do 
				(
					RsRemoveEmptyFolders dir
				)
			)
		)

		setStatusText filenames extractContinue
		
		return extractContinue
	),
	
	fn expressionsZip = 
	(
		local zipItem = RsProjectContentFind "expressions" type:"zip"
		zipItem.path + "/" + zipItem.name + ".zip"
	),
	fn expressionsDir = RsConfigGetStreamDir() + "expressions/",	
	fn expressions getLatest:true askToGet:true justNewer:false = 
	(
		local zipFile = expressionsZip()
		local extractDir = expressionsDir()
		
		extractFiles #(zipFile) #(extractDir) getLatest:getLatest askToGet:askToGet justNewer:justNewer
	),
	
	fn preExportExtract getLatest:true askToGet:true pedType: pedName: justNewer:true pedZipExtract:false pedZipExtractDir: pedZipFilename: = 
	(
		if (pedName == undefined) do return false
		
		local extractDirs = #()
		local zipFiles = #()
		
		-- AJM: For per component exports
		if (pedZipExtract and pedZipExtractDir != undefined and pedZipFilename != undefined) do
		(
			append extractDirs (RsConfigGetCacheDir() + pedZipExtractDir)
			append zipFiles pedZipFilename	
		)
		
		format "extracted files:\n"
		print zipFiles
		extractFiles zipFiles extractDirs getLatest:getLatest askToGet:askToGet justNewer:justNewer pedZipExtract:pedZipExtract
	)
)
gRsPedZipExtract = RsPedZipExtract()
