--
-- File:: pipeline/export/peds/ruby_script_generator.ms
-- Description::
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 9 June 2010
--
-- AJM:: Removed the BuildCutsPedsZip function as all the peds use the same function now
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/models/globals.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------


--
-- struct: RsPedScriptGeneratorStruct
-- desc: Ped Ruby script generator container for functions.
--
struct RsPedScriptGeneratorStruct
(

	--
	-- name: GenerateResourceCommonHeader
	-- desc:
	--
	-- note: Should not be called externally.
	--
	fn GenerateResourceCommonHeader rubyFile = (
		
		local projname = RsConfigGetProjectName()
		format "require 'pipeline/config/projects'\n" to:rubyFile
		format "Pipeline::Config::instance().logtostdout = true\n" to:rubyFile
		format "require 'pipeline/config/project'\n" to:rubyFile
		format "require 'pipeline/content/content_core'\n" to:rubyFile
		format "require 'pipeline/gui/exception_dialog'\n" to:rubyFile
		format "require 'pipeline/os/file'\n" to:rubyFile
		format "require 'pipeline/projectutil/data_convert'\n" to:rubyFile
		format "require 'pipeline/projectutil/data_zip'\n" to:rubyFile
		format "require 'pipeline/resourcing/convert'\n" to:rubyFile
		format "require 'pipeline/resourcing/util'\n" to:rubyFile
		format "require 'pipeline/util/string'\n" to:rubyFile
		format "include Pipeline\n\n" to:rubyFile

		format "begin\n\n" to:rubyFile
		format "\tproject = Pipeline::Config.instance.projects[\"%\"]\n" projname to:rubyFile
		format "\tproject.load_config( )\n" to:rubyFile
		format "\tbranch = project.branches[project.default_branch]\n" to:rubyFile
	),

	--
	-- name: GenerateResourceCommonFooter
	-- desc:
	--
	-- note: Should not be called externally.
	--
	fn GenerateResourceCommonFooter rubyFile = (

		format "rescue Exception => ex\n" to:rubyFile
		format "\tGUI::ExceptionDialog::show_dialog( ex )\n" to:rubyFile
		format "end\n\n" to:rubyFile 	
	),	
	
	fn BuildSharedPedTXD rubyFile outname texture_list streamed:false = (
	
		GenerateResourceCommonHeader rubyFile
		
		format "\tp4 = project.scm()\n" to:rubyFile
		format "\titdInputs = Array.new()\n" to:rubyFile

		for texture in texture_list do (
			format "\tputs \"Adding: #{OS::Path::get_filename( '%' )}\"\n" texture to:rubyFile
			format "\titdInputs <<  '%' \n" texture to:rubyFile
		)

		pedTypeDir = if streamed then "streamedpeds/" else "componentpeds/"

		local wholePath = (RsConfigGetExportDir() + "/models/cdimages/" + pedTypeDir + outname + ".itd.zip")
		format "\twholePath = '%'\n" wholePath to:rubyFile
		format "\tp4.run_edit_or_add(wholePath)\n" to:rubyFile
		format "\tp4.run_reopen('-t', 'ubinary', wholePath)\n" to:rubyFile
		format "\tsharedTexZip = ProjectUtil::data_zip_create( wholePath, itdInputs, true)\n" to:rubyFile

		GenerateResourceCommonFooter rubyFile
	),

	--
	-- name: OutputCommonExtractionHeader
	-- desc: Outputs the common header lines in the given ruby file
	--
	fn OutputCommonExtractionHeader rubyFile = (
		
		local projectName = RSConfigGetProjectName()
		
		format "require 'pipeline/config/projects'\n" to:rubyFile
		format "require 'pipeline/config/project'\n" to:rubyFile
		format "require 'pipeline/content/content_core'\n" to:rubyFile
		format "require 'pipeline/gui/exception_dialog'\n" to:rubyFile
		format "require 'pipeline/os/file'\n" to:rubyFile
		format "require 'pipeline/projectutil/data_zip'\n" to:rubyFile
		format "include Pipeline\n\n" to:rubyFile

		format "begin\n" to:rubyFile
		format "\tproject = Pipeline::Config.instance.projects[\"%\"]\n" projectName to:rubyFile
		format "\tproject.load_config( )\n" to:rubyFile
		format "\tproject.load_content( )\n" to:rubyFile
		format "\tbranch = project.branches[project.default_branch]\n" to:rubyFile
		
		format "\t#---------------------------------------------------------------------\n" to:rubyFile
		format "\t# Perforce creation and setup\n" to:rubyFile
		format "\t#---------------------------------------------------------------------\n" to:rubyFile
		format "\tDir::chdir( project.root ) do\n" to:rubyFile
		format "\t\tg_Perforce = Pipeline::SCM::Perforce.new( )\n" to:rubyFile
		format "\t\tg_Perforce.connect( )\n\n" to:rubyFile
		
		format "\t\t#---------------------------------------------------------------------\n" to:rubyFile
		format "\t\t# Get zip file path and stream path\n" to:rubyFile
		format "\t\t#---------------------------------------------------------------------\n" to:rubyFile
	),
	
	--
	-- name: OutputCommonExtractionPathFinding
	-- desc: Outputs the common header lines in the given ruby file
	--
	fn OutputCommonExtractionText rubyFile ZipFile StreamPath = (

		format "\t\t#---------------------------------------------------------------------\n" to:rubyFile
		format "\t\t# Extract the % file\n" ZipFile to:rubyFile
		format "\t\t#---------------------------------------------------------------------\n" to:rubyFile
		format "\t\tputs \"Extract: #{%[0].filename}\"\n" ZipFile to:rubyFile
 		format "\t\t%_files = ProjectUtil::data_zip_extract( %[0].filename, %, true ) do |zipname|\n" ZipFile ZipFile StreamPath to:rubyFile
 		format "\t\t\tputs \"Extracting: #{zipname}\"\n" to:rubyFile
 		format "\t\tend\n\n" to:rubyFile
		
		/*
		AJM: commenting this part out as it extracts all data (mesh, sva etc) to the stream directory, when really 
			all the Extract All needs to do is extract the packfiles.  Leaving this here though incase it turns out
			someone does like all the data to be extracted to their streamDir
		
		format "\t%_files.each do |zipname|\n" ZipFile to:rubyFile
		format "\t\tif ( 'ibd' == OS::Path::get_extension( zipname ) ) then\n" to:rubyFile
		format "\t\t\tProjectUtil::data_extract_zip( r, zipname, %, true ) do |filename|\n" StreamPath to:rubyFile
		format "\t\t\t\tputs \"	Extracting: #{filename}\"\n" to:rubyFile
		format "\t\t\t\tdest = OS::Path::combine( Pipeline::OS::Path::remove_extension( filename ), OS::Path::get_filename( filename ) )\n" to:rubyFile
		format "\t\t\t\tOS::FileUtilsEx::move_file( filename, dest, true )\n" to:rubyFile
		format "\t\t\tend\n" to:rubyFile
		format "\t\telse\n" to:rubyFile
		format "\t\t\toutput_dir = OS::Path::remove_extension(zipname )\n" to:rubyFile
		format "\t\t\tProjectUtil::data_zip_extract( zipname, output_dir, true ) do |filename|\n" to:rubyFile
		format "\t\t\t\tputs \"	Extracting: #{filename}\"\n" to:rubyFile
		format "\t\t\tend\n" to:rubyFile
		format "\t\tend\n" to:rubyFile
		format "\tend\n\n" to:rubyFile
		*/
	),
	
	--
	-- name: ExtractAllPedZIP
	-- desc: Create and run the ped all ZIP extraction script.
	--
	fn ExtractAllPedZIP rubyFileName getLatest = (

		RsMakeSurePathExists rubyFileName
		local projectName = RsConfigGetProjectName()
		local rubyFile = createFile rubyFileName

		OutputCommonExtractionHeader rubyFile
		
 		format "\t\tcomponentpeds_zip = project.content.find_by_script( '\"zip\" == content.xml_type and content.name == \"componentpeds\"')\n" to:rubyFile
		format "\t\tpedprops_zip = project.content.find_by_script( '\"zip\" == content.xml_type and content.name == \"pedprops\"')\n" to:rubyFile
		format "\t\tstreamedpedprops_zip = project.content.find_by_script( '\"zip\" == content.xml_type and content.name == \"streamedpedprops\"')\n" to:rubyFile
		format "\t\tstreamedpeds_zip = project.content.find_by_script( '\"zip\" == content.xml_type and content.name == \"streamedpeds\"')\n" to:rubyFile
 		format "\t\tcomponent_stream_dir = Pipeline::OS::Path::combine( project.netstream, 'componentpeds' )\n" to:rubyFile
		format "\t\tprops_stream_dir = Pipeline::OS::Path::combine( project.netstream, 'pedprops' )\n" to:rubyFile
		format "\t\tstreamed_props_stream_dir = Pipeline::OS::Path::combine( project.netstream, 'streamedpedprops' )\n" to:rubyFile
		format "\t\tstreamed_stream_dir = Pipeline::OS::Path::combine( project.netstream, 'streamedpeds' )\n\n" to:rubyFile
		
		if getLatest == true then
		(
			format "\t\t#---------------------------------------------------------------------\n" to:rubyFile
			format "\t\t# Get latest zip file\n" to:rubyFile
			format "\t\t#---------------------------------------------------------------------\n" to:rubyFile
			format "\t\tg_Perforce.run_sync( \"#{componentpeds_zip[0].filename}\" )\n" to:rubyFile
			format "\t\tg_Perforce.run_sync( \"#{pedprops_zip[0].filename}\" )\n" to:rubyFile
			format "\t\tg_Perforce.run_sync( \"#{streamedpedprops_zip[0].filename}\" )\n" to:rubyFile
			format "\t\tg_Perforce.run_sync( \"#{streamedpeds_zip[0].filename}\" )\n\n" to:rubyFile
		)
		
		OutputCommonExtractionText rubyFile "componentpeds_zip" "component_stream_dir"
		OutputCommonExtractionText rubyFile "pedprops_zip" "props_stream_dir"
		OutputCommonExtractionText rubyFile "streamedpedprops_zip" "streamed_props_stream_dir"
		OutputCommonExtractionText rubyFile "streamedpeds_zip" "streamed_stream_dir"
		
		-- This is the end for the Dir::chdir( project.root ) do
		format "\tend\n" to:rubyFile
		
		format "rescue Exception => ex\n" to:rubyFile
		format "\tGUI::ExceptionDialog.show_dialog( ex )\n" to:rubyFile
		format "end\n" to:rubyFile

		close rubyFile
		RsRunRubyScript rubyFileName
	),
	
	--
	-- name: ExtractComponentZIP
	-- desc: Create and run the ped component ZIP extraction script.
	--
	fn ExtractComponentZIP rubyFileName getLatest = (

		RsMakeSurePathExists rubyFileName
		local projectName = RsConfigGetProjectName()
		local rubyFile = createFile rubyFileName

		OutputCommonExtractionHeader rubyFile
		
 		format "\t\tcomponentpeds_zip = project.content.find_by_script( '\"zip\" == content.xml_type and content.name == \"componentpeds\"')\n" to:rubyFile
 		format "\t\tcomponent_stream_dir = Pipeline::OS::Path::combine( project.netstream, 'componentpeds' )\n\n" to:rubyFile
		
		if getLatest == true then
		(
			format "\t\t#---------------------------------------------------------------------\n" to:rubyFile
			format "\t\t# Get latest zip file\n" to:rubyFile
			format "\t\t#---------------------------------------------------------------------\n" to:rubyFile
			format "\t\tg_Perforce.run_sync( \"#{componentpeds_zip[0].filename}\" )\n\n" to:rubyFile
		)
		
		OutputCommonExtractionText rubyFile "componentpeds_zip" "component_stream_dir"
		
		-- This is the end for the Dir::chdir( project.root ) do
		format "\tend\n" to:rubyFile
		
		format "rescue Exception => ex\n" to:rubyFile
		format "\tGUI::ExceptionDialog.show_dialog( ex )\n" to:rubyFile
		format "end\n" to:rubyFile

		close rubyFile
		RsRunRubyScript rubyFileName
	),
	
	--
	-- name: ExtractPropZIP
	-- desc: Create and run the ped prop and streamed ped prop ZIP extraction script.
	--
	fn ExtractPropZIP rubyFileName getLatest = (

		print ("Extracting with getLatest set to : " + (getLatest as string))
		RsMakeSurePathExists rubyFileName
		local projectName = RsConfigGetProjectName()
		local rubyFile = createFile rubyFileName

		OutputCommonExtractionHeader rubyFile
		
 		format "\t\tpedprops_zip = project.content.find_by_script( '\"zip\" == content.xml_type and content.name == \"pedprops\"')\n" to:rubyFile
 		format "\t\tprops_stream_dir = Pipeline::OS::Path::combine( project.netstream, 'pedprops' )\n\n" to:rubyFile
 		format "\t\tstreamedpedprops_zip = project.content.find_by_script( '\"zip\" == content.xml_type and content.name == \"streamedpedprops\"')\n" to:rubyFile
 		format "\t\tstreamed_props_stream_dir = Pipeline::OS::Path::combine( project.netstream, 'streamedpedprops' )\n\n" to:rubyFile
		
		if getLatest == true then
		(
			format "\t\t#---------------------------------------------------------------------\n" to:rubyFile
			format "\t\t# Get latest zip file\n" to:rubyFile
			format "\t\t#---------------------------------------------------------------------\n" to:rubyFile
			format "\t\tg_Perforce.run_sync( \"#{pedprops_zip[0].filename}\" )\n\n" to:rubyFile
			format "\t\tg_Perforce.run_sync( \"#{streamedpedprops_zip[0].filename}\" )\n\n" to:rubyFile
		)
		
		OutputCommonExtractionText rubyFile "pedprops_zip" "props_stream_dir"
		OutputCommonExtractionText rubyFile "streamedpedprops_zip" "streamed_props_stream_dir"
		
		-- This is the end for the Dir::chdir( project.root ) do
		format "\tend\n" to:rubyFile
		
		format "rescue Exception => ex\n" to:rubyFile
		format "\tGUI::ExceptionDialog.show_dialog( ex )\n" to:rubyFile
		format "end\n" to:rubyFile

		close rubyFile
		RsRunRubyScript rubyFileName
	),
	
	--
	-- name: ExtractStreamedZIP
	-- desc: Create and run the streamed ped ZIP extraction script.
	--
	fn ExtractStreamedZIP rubyFileName getLatest = (

		RsMakeSurePathExists rubyFileName
		local projectName = RsConfigGetProjectName()
		local rubyFile = createFile rubyFileName

		OutputCommonExtractionHeader rubyFile
		
 		format "\t\tstreamedpeds_zip = project.content.find_by_script( '\"zip\" == content.xml_type and content.name == \"streamedpeds\"')\n" to:rubyFile
 		format "\t\tstreamed_stream_dir = Pipeline::OS::Path::combine( project.netstream, 'streamedpeds' )\n\n" to:rubyFile
		
		if getLatest == true then
		(
			format "\t\t#---------------------------------------------------------------------\n" to:rubyFile
			format "\t\t# Get latest zip file\n" to:rubyFile
			format "\t\t#---------------------------------------------------------------------\n" to:rubyFile
			format "\t\tg_Perforce.run_sync( \"#{streamedpeds_zip[0].filename}\" )\n\n" to:rubyFile
		)
		
		OutputCommonExtractionText rubyFile "streamedpeds_zip" "streamed_stream_dir"
		
		-- This is the end for the Dir::chdir( project.root ) do
		format "\tend\n" to:rubyFile
		
		format "rescue Exception => ex\n" to:rubyFile
		format "\tGUI::ExceptionDialog.show_dialog( ex )\n" to:rubyFile
		format "end\n" to:rubyFile

		close rubyFile
		RsRunRubyScript rubyFileName
	),
	
	--
	-- name: ExtractExpressionsRPF
	-- desc: Create and run the expressions RPF extraction script.
	--
	fn ExtractExpressionsRPF rubyFileName getLatest = (

		RsMakeSurePathExists rubyFileName
		local projectName = RsConfigGetProjectName()
		local rubyFile = createFile rubyFileName

		OutputCommonExtractionHeader rubyFile
		
 		format "\t\texpressions_rpf =  project.content.find_by_script( '\"zip\" == content.xml_type and content.name == \"expressions\"')\n" to:rubyFile
 		format "\t\texpressions_stream_dir = Pipeline::OS::Path::combine( project.netstream, 'expressions' )\n\n" to:rubyFile
		
		if getLatest == true then
		(
			format "\t\t#---------------------------------------------------------------------\n" to:rubyFile
			format "\t\t# Get latest rpf file\n" to:rubyFile
			format "\t\t#---------------------------------------------------------------------\n" to:rubyFile
			format "\t\tg_Perforce.run_sync( \"#{expressions_rpf[0].filename}\" )\n\n" to:rubyFile
		)
		
		OutputCommonExtractionText rubyFile "expressions_rpf" "expressions_stream_dir"
		
		-- This is the end for the Dir::chdir( project.root ) do
		format "\tend\n" to:rubyFile
		
		format "rescue Exception => ex\n" to:rubyFile
		format "\tGUI::ExceptionDialog.show_dialog( ex )\n" to:rubyFile
		format "end\n" to:rubyFile

		close rubyFile
		RsRunRubyScript rubyFileName
	)
)


-- Create instances of our script generator struct.
global RsPedScriptGenerator = RsPedScriptGeneratorStruct()

-- pipeline/export/peds/ruby_script_generator.ms
