-- Use this to get the RsPedExportData struct and other variables setup
filein "pipeline/export/ui/ped_ui.ms"

--
-- name:	RsPedBatchExportPed
-- desc:	Opens up each maxfile in pedList and performs some batch export process on them
--
-- NOTE: 	If adding in full ped exports to this, the metadata stuff will need seperated out since
--			a full export does the metadata too anyway.
fn RsPedBatchExport pedList:unsupplied overwriteMeta:false metadataOnly:false = 
(
	RsAutomatedExport = true
	local quietModeSetting = GetQuietMode()
	
	-- Set quiet mode to stop 'Missing external files' dialog, etc
	SetQuietMode true
	
	-- Load from an xml file the filenames of peds for batch export if nothing supplied
	if ( pedList == unsupplied ) do
	(
		pedList = #()		
		local batchPedFileDirectory = RsConfigGetArtDir() + "peds/"
		local batchPedXmlFile = RsConfigGetEtcDir() + "config/characters/batchped.xml" 
		
		if ( RsFileExists batchPedXmlFile ) do
		(		
			local doc = XMlDocument()
			doc.init()
			doc.load batchPedXmlFile
			xmlRoot = doc.document.DocumentElement
			if xmlRoot != undefined then 
			(
				pedFilesNode = RsGetXmlElement xmlRoot "ped_files"
				if pedFilesNode != undefined then 
				(
					items = pedFilesNode.childnodes
					
					if ( pedFilesNode.childnodes.Count > 0 ) do
					(
						for i = 0 to ( items.Count - 1 ) do 
						(
							pedMaxFile = batchPedFileDirectory + ((items.itemof(i)).InnerText)
							appendIfUnique pedList pedMaxFile
						)
					)
				)
			)
		)
	)
	if pedList.count == 0 do return false
	
	-- Get latest on the max files
	gRsPerforce.sync pedList silent:true
	
	-- Checkout all meta files
	-- Will change this to be just the ones being batch exported...
	metadataDir = RsProjectGetMetadataDir() + "/characters/..."
	gRsPerforce.sync metadataDir silent:true
	gRsPerforce.edit metadataDir silent:true	
	
	for pedFile in pedList do
	(
		if ( not RsFileExists pedFile ) do 
		(
			gRsULog.LogWarning ("Ped max file doesn't exist, skipping batch export: " + pedFile)
			continue
		)		
		
		loadMaxFile pedFile		
		local pedDummy
		-- Special player model bollocks
		if (matchpattern pedFile pattern:"*michael*") then
		(
			pedDummy = getnodebyname "player_zero"
		)
		else if (matchpattern pedFile pattern:"*franklin*") then
		(
			pedDummy = getnodebyname "player_one"
		)
		else if (matchpattern pedFile pattern:"*trevor*") then
		(
			pedDummy = getnodebyname "player_two"
		)
		else
		(
			pedDummy = getnodebyname (RsRemoveExtension maxFileName)
		)
		
		if (pedDummy == undefined ) do
		(
			gRsULog.LogWarning ("Couldn't find dummy to select in " + pedFile)
			continue
		)
		select pedDummy
		gRsULog.LogMessage ("Batch exporting ped: " + pedDummy.name)

		if (selection.count != 1) or (not isKindOf selection[1] Dummy) or (selection[1].children.count == 0) do 
		(
			gRsULog.LogWarning ("Selected object in " + pedFile + " is incorrect: " + selection[1].name)
			continue
		)

		-- compExport set to true for the output dir to get generated
		pedExportInfo = RsPedExportData compExport:true overwriteMeta:overwriteMeta
		pedExportInfo.init selection[1]
		local success = gRsPedExport.ExportVariationMetaData pedExportInfo pedExportInfo.compOutputDir
		gRsPerforce.postExportAdd cancel:(not success) exclusive:true
	)
	
	-- Revert unchanged files:
	gRsPerforce.run "revert" metadataDir switches:#("-a") silent:true
	
	SetQuietMode quietModeSetting
	ShowRsUlogDialog()
	
	-- Adding this so other batch stuff can get added after metadata stuff
	if (metadataOnly) do return true
)