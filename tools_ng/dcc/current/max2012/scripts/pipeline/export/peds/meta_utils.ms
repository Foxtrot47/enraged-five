--
-- File:: pipeline/export/peds/meta_utils.ms
-- Description:: Ped meta utils
--
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 17 January 2011
--

-- This needs to match runtimes ePedVarComp enum (/peds/rendering/PedVariationDS.h)

--fileIn "pipeline/util/metadata_interface.ms"

struct RsPedMetaUtilSuite 
(
	expressionModCount = 5,	-- ExpressionMod arrays will be given this many values
	MAX_PED_PROPS = 16,		-- Maximum number of props per attachment-point
	
	MetaDataManager,
	
	MetaComponentMap,
	MetaComponents,
	MetaCompInfos,
	MetaProps,
	
	SceneComponents, SceneComponentAlternatives, SceneProps, 
	CompTexVars, PropTexVars, 
	hasLods = false,
	MetaFilePath = "",
	PedName = "",
	
	texFilenameData,

	-- Enums for objects with name-prefixes on this list:
	-- GTA5_NG requires 12 components.
	-- RDR2 uses 20
	ComponentEnums = 
	#(
		"head",	-- 0
		"berd",	-- 1 
		"hair",	-- 2
		"uppr",	-- 3
		"lowr",	-- 4
		"hand",	-- 5
		"feet", -- 6
		"teef",	-- 7
		"accs",	-- 8
		"task",	-- 9
		"decl",	--10
		"jbib" 	--11
	),
	
	-- Enums for props with name-prefix "p_[prefix]"
	PropEnums = 
	#(
		"head", -- 0
		"eyes", -- 1
		"ears", -- 2
		"mouth", -- 3
		"lhand", -- 4
		"rhand", -- 5
		"lwrist", -- 6
		"rwrist", -- 7
		"hip", -- 8
		"lfoot", -- 9
		"rfoot", -- 10
		"phlhand", -- 11
		"phrhand" -- 12
	),
	
	-- Enums for filename race-identifier suffixes:
	RacialEnums = 
	#(
		"uni",		-- 0 universal
		"whi",		-- 1 white
		"bla",		-- 2 black
		"chi",		-- 3 chinese
		"lat",		-- 4 latino
		"ara",		-- 5 arabic
		"bal",		-- 6 balkan
		"jam",	-- 7 jamaican
		"kor",		-- 8 korean
		"ita",		-- 9 italian
		"pak"		-- 10 pakistani
	),
	
	-- Remember the madness of Max starting at 1 even when bit shifting
	FLAG_DRAWBL_ACTIVE = 1,
	FLAG_DRAWBL_ALPHA = 2,
	FLAG_DRAWBL_DECAL = 3,
	FLAG_DRAWBL_CUTOUT = 4,
	FLAG_DRAWBL_RACE_TEX = 5,
	FLAG_DRAWBL_MATCH_PREV = 6,
	FLAG_DRAWBL_PALETTE = 7,		
	
	fn getEnum enumList findName = 
	(		
		if (findName == undefined) then -1 else 
		(
			(findItem enumList (toLower findName)) - 1
		)
	),
	
	fn setMetaValue valueName val = 
	(
		local foundStruct = MetaDataManager.FindFirstStructureNamed valueName
		if undefined==foundStruct then
		(
			foundStruct = MetaDataManager.CreateNewStructTunableFor valueName
		)
		foundStruct.value = val
	),	
	
	fn setMetaBoolValue valueName val = 
	(
		setMetaValue valueName (dotNetObject "system.boolean" val)
	),
	
	fn addToList arrayTune val = 
	(
		local arrayMember = arrayTune.Definition
		local elementMember = arrayMember.ElementType
		
		local newItem = (dotNetClass "RSG.Metadata.Data.TunableFactory").Create arrayTune elementMember
		newItem.value = val
		
		arrayTune.Add newItem
	),
	
	-- Grows/shrinks node-list as required:
	fn setNodeCount nodeList createStructFor nodeCount defaults:#() = 
	(
		local lengthDiff = nodeList.Length - nodeCount

		if lengthDiff != 0 do 
		(
--			format "%: % -> % (defaults: %)\n" createStructFor nodeList.Length nodeCount (defaults as string)
		)
		
		case of 
		(			
			(lengthDiff > 0):
			(
				-- List has too many nodes, remove the excess:
				for i = 1 to lengthDiff do 
				(
					nodeList.RemoveAt (nodeList.Length - 1)
--format "REMOVE NODE FROM %: %\n" createStructFor i
				)
			)
			(lengthDiff < 0):
			(
				-- List doesn't hav enough nodes, so add some more:
				for i = lengthDiff to -1 do 
				(
					local metaNode = MetaDataManager.CreateNewStructTunableFor createStructFor
					nodeList.Add metaNode
--format "ADD NODE TO %: %\n" createStructFor i
					
					-- Set up any default values:
					for defItem in defaults do 
					(
						local val = defItem.value
						if (isKindOf val BooleanClass) do (val = dotNetObject "system.boolean" defItem.value)
						
						metaNode.item[defItem.name].value = val
					)
				)
			)
		)
	),
	
	fn InitWith metafile = 
	(
		-- We don't want to sync the pedvariations.psc file for example
		MetaDataManager = RsGetMetaDataManager syncDefinitions:false
		
		MetaFilePath = metafile
		PedName = getFilenameFile metafile
		
		if doesFileExist metafile then 
		(
			format "Loading from existing metafile: \n\t%\n" metafile
			
			MetaDataManager.LoadWithMetaFile metafile
		)
		else 
		(
			format "Metafile not found, creating new: \n\t%\n" metafile
			
			MetaDataManager.LoadWithStruct "CPedVariationInfo"
			-- Populate the component index array.  It will allow us to populate the variation data with defaults.
			local availComp = MetaDataManager.FindFirstStructureNamed "availComp"
			
			-- Make sure components-structure has same number of values as ComponentEnums, all set to default 255:
			for i = 1 to ComponentEnums.count do 
			(
				-- Add extra values if default array is shorter than ComponentEnums array:
				if (i > availComp.Length) then 
				(
					addToList availComp 255
				)
				else 
				(
					availComp.Item[i - 1].Value = 255
				)
			)
		)
	),
	
	fn SynthesiseSceneObjMapsFrom dummynode = 
	(
		local failed = false
		
		-- Initialise component/prop object-lists:
		SceneComponents = for item in ComponentEnums collect #()
		SceneComponentAlternatives = for item in ComponentEnums collect #()
		SceneProps = for item in PropEnums collect #()

		-- Get texturenames for this object:
		local texFilenames = #()
		RsGetTexMapsFromObj dummynode texFilenames #() #()
		local modelTexCount = texFilenames.count

		-- Get additional texturenames from this file's .pedtex file:
		local pedTexName = (getFilenameFile maxFilename) + ".pedtex"
		local pedTexFile = openfile (maxFilePath + pedTexName)
		if (pedTexFile != undefined) do 
		(
			while not eof pedTexFile do 
			(
				local getLine = readLine pedTexFile
				appendIfUnique texFilenames (toLower (getFilenameFile getLine))
			)			
			close pedTexFile
		)
		
		struct texFilenameStruct (filename, isProp, enumIdx, meshIdx, varIdx, raceIdx)
		
		-- Filter out non-diffuse texturenames, and parse relevant texturenames out into info-structs.
		-- Raise errors if conflicting texturenames are found.
		texFilenameData = #()
		local checkNames = #()
		
		for fileNum = 1 to texFilenames.count do 
		(
			local filename = toLower texFilenames[fileNum]
			local fromPedtex = (fileNum > modelTexCount)
			
			-- Props have prefix "p_"
			local isProp = matchPattern filename pattern:"p_*"
			
			local readName = if isProp then (substring filename 3 -1) else filename
			local nameTokens = filterstring (toLower readName) "_ \t"
			local texType = nameTokens[2]
			
			-- Only process diffuse textures:
			if (texType != undefined and texType == "diff") do 
			(
				-- Relevant enums/textureVariations arrays are chosen, if texture is for prop or not:
				local enumList = if isProp then PropEnums else ComponentEnums
				
				local enumIdx = getEnum enumList nameTokens[1]
				local meshIdx = if(nameTokens[3] != undefined) then (nameTokens[3] as number) else
				(
					gRsUlog.LogError ("Metadata error, nameTokens[3] was undefined for: " + readName + " check texture name for mesh number") context:dummynode
					failed = true
				)
				local varIdx = if(nameTokens[4] != undefined) then ((bit.charAsInt nameTokens[4]) - 97) else -- Converts lower-case character to 0->25 integer				
				(
					gRsUlog.LogError ("Metadata error, nameTokens[4] was undefined for: " + readName + " check texture name for variation info") context:dummynode
					failed = true
				)				
				local raceIdx = if isProp then 0 else 
				(
					if(nameTokens[5] != undefined) then (getEnum RacialEnums nameTokens[5]) else-- (Props aren't racist)
					(
						gRsUlog.LogError ("Metadata error, nameTokens[5] was undefined for: " + readName + " check texture name for race info") context:dummynode
						failed = true
					)
				)
				
				if failed do return false
				
				-- Add struct with parsed data to texture-array:
				local texItem = texFilenameStruct filename:filename isProp:isProp enumIdx:enumIdx meshIdx:meshIdx varIdx:varIdx raceIdx:raceIdx
				append texFilenameData texItem
				
				-- We'll use this name/array to check for conflicting texturenames:
				local checkName = (if isProp then "p_" else "c_") + (enumIdx as string) + "_" + (meshIdx as string) + "_" + (varIdx as string)
				local conflictNum = findItem checkNames checkName
				
				-- Add error if conflicting texturenames were found:
				if (conflictNum != 0) do 
				(
					local thisSource = if (fileNum > modelTexCount) then pedTexName else "model"
					local thatSource = if (conflictNum > modelTexCount) then pedTexName else "model"
					
					local failMsg = stringStream ""
					format "Conflicting texturenames: % (from %), % (from %)" texFilenameData[conflictNum].filename thatSource filename thisSource to:failMsg
					
					gRsUlog.LogError (failMsg as string) context:dummynode
					failed = true
				)
				
				append checkNames checkName 
			)
		)
		
		if failed do return false
		
		-- Don't include LODs:
		for child in dummynode.children where (not matchPattern child.name pattern:"*LOD") do 
		(
			-- Props have prefix "p_"
			local isProp = matchPattern child.name pattern:"p_*"
			
			local readName = if isProp then (substring child.name 3 -1) else child.name
			local nameTokens = filterstring (toLower readName) "_ \t"
			
			local objNum = nameTokens[2] as integer
			
			local foundAlternative = false
			local alternativeNum = 0
			
			if (not isProp and nameTokens.count > 3) then
			(
				alternativeNum = nameTokens[4] as integer
				if (alternativeNum != undefined and alternativeNum != 0) then
					foundAlternative = true
			)
			
			-- Only include objects with a number:
			if (objNum != undefined) do 
			(
				-- Increment objNum to make it 1-based:
				objNum += 1
				
				local enumName = nameTokens[1]
				local enumList, objList, alternativeList
				
				if isProp then 
				(
					enumList = PropEnums
					objList = SceneProps
				)
				else 
				(
					enumList = ComponentEnums
					objList = SceneComponents
					alternativeList = SceneComponentAlternatives
				)
				
				local enum = getEnum enumList enumName
				if (enum > -1) do 
				(
					if (foundAlternative) then
					(
						local alternativeCount = alternativeList[enum + 1][objNum]
						
						if (alternativeCount == undefined) then
							alternativeList[enum + 1][objNum] = 1
						else
							alternativeList[enum + 1][objNum] = alternativeCount + 1 
					)
					else
					(
						if (not isProp) then
						(
							local alternativeCount = alternativeList[enum + 1][objNum]
							
							if (alternativeCount == undefined) then
								alternativeList[enum + 1][objNum] = 0
						)
						
						objList[enum + 1][objNum] = child
					)
					
					if not hasLods and (getNodeByName (child.name + "_lod") != undefined) do 
					(
						hasLods = true
					)
				)
			)
		)				
		
		-- Check to make sure all pieces have at least 1 texture on it
		for meshObj in dummynode.children do
		(
			local texMapsList = #()
			RsGetTexMapsFromObj meshObj texMapsList #() #()
			if texMapsList.count == 0 do
			(
				local failMsg = stringStream ""
				format "Mesh part (%) has no textures!" meshObj.name to:failMsg					
				gRsUlog.LogError (failMsg as string) context:child				
				failed = true
			)
		)		
		if failed do return false	
		
		-- Look for meshes missing from series of component-objects:
		for compNum = 1 to ComponentEnums.count do 
		(
			local compName = ComponentEnums[compNum]
			local objList = SceneComponents[compNum]
			
			for n = 1 to objList.count where (objList[n] == undefined) do 
			(
				local failMsg = stringStream ""
				local maxindex = n-1
				format "Metadata Export Error: Model missing from enumerated series of \"%\" components in scene. Look for existence and correct linking of %_%_*, please." compName compName (formattedPrint maxindex format:"03d") to:failMsg
				
				gRsUlog.LogError (failMsg as string) context:dummynode
				failed = true
			)
		)
		
		-- Look for a texture missing from series of textures e.g. a,b,d,e as this causes errors later
		-- with undefined values in arrays, such as with raceIdxs.
		-- The Ped textures list is sorted arranged by 
		fn sortByFileName v1 v2 = (striCmp v1.filename v2.filename)
		sortedTextureVarList = deepCopy texFilenameData
		qsort sortedTextureVarList sortByFileName
		
		if ( sortedTextureVarList.count > 0 ) do
		(
			local meshIdx = sortedTextureVarList[1].meshIdx
			local varIdx = sortedTextureVarList[1].varIdx
			for texFilenameIdx = 1 to sortedTextureVarList.count do 
			(
				local texFileStruct = sortedTextureVarList[texFilenameIdx]
				
				--format "meshIdx:%\tvarIdx:%\tstructmeshIdx:%\tstructvarIdx:%\n" meshIdx varIdx texFileStruct.meshIdx texFileStruct.varIdx
				
				if ( texFileStruct.meshIdx == meshIdx ) then
				(
					if ( texFileStruct.varIdx - varIdx > 1 ) do
					(
						gRsUlog.LogError ("Texture variation sequence is missing an entry before: " + texFileStruct.filename)
						failed = true
					)
				)

				meshIdx = texFileStruct.meshIdx
				varIdx = texFileStruct.varIdx					
			)
		)
		
		if failed do return false
		
		fn sortByName v1 v2 = (striCmp v1.name v2.name)
		for objList in SceneComponents do (qsort objList sortByName)
		
		return true
	),
	
	fn LoadMetas = 
	(
		MetaComponentMap = MetaDataManager.FindFirstStructureNamed("availComp")
		format "Loaded comp map: %\n" (MetaComponentMap as string)
		
		MetaComponents = MetaDataManager.FindFirstStructureNamed("aComponentData3")
		format "Loaded comps: %\n" (MetaComponents as string)
		
		MetaCompInfos = MetaDataManager.FindFirstStructureNamed("compInfos")
		format "Loaded compInfos: %\n" (MetaCompInfos as string)
		
		MetaProps = MetaDataManager.FindFirstStructureNamed("aPropMetaData")
		format "Loaded props: %\n" (MetaProps as string)
	),
	
	-- Get variations for this ped's textures:
	fn getTextureVariations dummyNode = 
	(
		CompTexVars = for item in ComponentEnums collect #()
		PropTexVars = for item in PropEnums collect #()
		
		-- "texFilenameData" is generated from model's materials and the .pedtex file by SynthesiseSceneObjMapsFrom:
		for item in texFilenameData do 
		(
			-- (filename, isProp, enumIdx, meshIdx, varIdx, raceIdx)
			
			local varsArray = if item.isProp then PropTexVars else CompTexVars
				
			-- Cancel add if array includes invalid (zeroed) enums:
			local notBad = true
			for val in #(item.enumIdx, item.meshIdx, item.varIdx, item.raceIdx) where (val < 0) while notBad do (notBad = false)

			if notBad do 
			(
				-- Add Texture Variation data to array:
				local useArray = varsArray

				-- Set useArray to variation's enum address:
				local enumNum = (item.enumIdx + 1)
				if (useArray[enumNum] == undefined) do 
				(
					useArray[enumNum] = #()
				)
				useArray = useArray[enumNum]
				
				-- Set useArray to variation's meshIdx address:
				local meshNum = (item.meshIdx + 1)
				if (useArray[meshNum] == undefined) do 
				(
					useArray[meshNum] = dataPair propMask:0 texVarRaces:#()
				)
				meshItem = useArray[meshNum]
				
				-- Compile prop-flags:
			-- PV_DRAWBL_RACE_TEX = 1 << 4, // name of the drawable states if race textures are used on it
				if (item.raceIdx != 0) then 
				(
					meshItem.propMask = bit.set meshItem.propMask FLAG_DRAWBL_RACE_TEX true
				)
				
				meshItem.texVarRaces[item.varIdx + 1] = item.raceIdx
			)
		)
			
		local failed = false 
		-- Check the textures in ped textures and make sure they have a matching mesh
		for textureFilename in RsTextureList do
		(
			textureName = RsRemovePathAndExtension textureFilename			
			local isProp = matchPattern textureName pattern:"p_*"			
			textureNameTokens = filterString textureName "_"
			
			-- Split texture name up to find what mesh part it should look for
			-- e.g. accs_diff_000_a_uni    is split up and uses the 1st and 3rd token
			-- to look for accs_000*
			-- If a prop they begin with p_ e.g:
			-- p_eyes_diff_000_1
			-- so look for meshes called p_eyes_000*
			local meshNamePatternToLookFor
			if (isProp) then
			(
				meshNamePatternToLookFor = textureNameTokens[1] + "_" + textureNameTokens[2] + "_" + textureNameTokens[4] + "*"
			)
			else
			(
				meshNamePatternToLookFor = textureNameTokens[1] + "_" + textureNameTokens[3] + "*"
			)

			local notMatched = true
			for meshObj in dummyNode.children while notMatched do
			(
				if (matchPattern meshObj.name pattern:meshNamePatternToLookFor) do
				(					
					notMatched = false
				)
			)			
			-- Didn't find a matching mesh, so error
			if( notMatched ) do
			(
				local failMsg = stringStream ""
				format "The texture % has no matched mesh part, please check the mesh or texture names" textureName to:failMsg
				gRsUlog.LogError (failMsg as string)				
			)			
		)
		
		failed = not gRsUlog.ValidateJustErrors()
		if failed do return false
		
		return #(CompTexVars, PropTexVars)
	),
	
	-- Get flags from pedVariations data:
	fn getPedVarFlags dataArray = 
	(
		local propFlags = 0
	
		if (dataArray != undefined) do 
		(
			for n = 1 to 5 do 
			(
				local setVal = if (dataArray[n] == 1) then True else False
				bit.set propFlags n setVal
			)
		)
		
		return propFlags
	),
	
	fn getObjMaterialFlags obj texVarList: compIdx: meshIdx: objMats:#() startFlags:0 = 
	(
		--format "Getting material-flags for: %" obj.name
		
		local texFlags = startFlags
		
		if (texVarList != unsupplied) do 
		(
			-- Combine texture flags found for all the texture-variations:
			local texVarData = texVarList[compIdx + 1][meshIdx + 1]

			if (texVarData != undefined) do 
			(
			-- PV_DRAWBL_ACTIVE = 1 << 0, // sometimes the component won't have an active drawable
				
				-- Apply flags acquired from getTextureVariations (which only sets the FLAG_DRAWBL_RACE_TEX currently):
				texFlags = bit.set texFlags FLAG_DRAWBL_RACE_TEX (bit.get texVarData.propMask FLAG_DRAWBL_RACE_TEX)
				-- Previously was doing a bit.or with 1 so it was always set, so changing to a set to make it more explicit
				texFlags = bit.set texFlags FLAG_DRAWBL_ACTIVE true
			)
		)
		
	-- PV_DRAWBL_MATCH_PREV = 1 << 5, // All components must use the same texture-variation id, if meshes have _m suffix:
		local objMainName = (filterString obj.name " ")[1]
		if (matchPattern objMainName pattern:"*_m") do 
		(
			format "  Mesh has variation-match suffix: %\n" obj.name
			texFlags = bit.set texFlags FLAG_DRAWBL_MATCH_PREV true
		)
		
		join objMats (RsGetMaterialsOnObjFaces obj)
		for mat in objMats where (isKindOf mat Rage_Shader) do 
		(
			local shaderName = RstGetShaderName mat
			
		-- PV_DRAWBL_ALPHA = 1 << 1, // has some alpha'ed bits in it (so needs to draw last)
			if (matchPattern shaderName pattern:"*alpha*") do 
			(
				--format "  Alpha shader found: %\n" shaderName
				texFlags = bit.set texFlags FLAG_DRAWBL_ALPHA true
			)
		-- PV_DRAWBL_DECAL = 1 << 2, // uses decal pass (usually ped_decal stuff)
			if (matchPattern shaderName pattern:"*decal*") do 
			(
				--format "  Decal shader found: %\n" shaderName
				texFlags = bit.set texFlags FLAG_DRAWBL_DECAL true
			)			
		-- PV_DRAWBL_CUTOUT = 1 << 3, // uses cutout pass (usually hair)
			if (matchPattern shaderName pattern:"*cutout*") or (matchPattern shaderName pattern:"ped_hair_*") do 
			(
				--format "  Cutout shader found: %\n" shaderName
				texFlags = bit.set texFlags FLAG_DRAWBL_CUTOUT true
			)
		-- PV_DRAWBL_RACE_TEX = 1 << 4, // name of the drawable states if race textures are used on it
			-- (set previously by getTextureVariations)
		-- PV_DRAWBL_MATCH_PREV = 1 << 5, // this component must select the same texture id as used in the previous component for this ped
			-- (set above, from mesh-name)
		-- PV_DRAWBL_PALETTE = 1 << 6, // this drawable can use the colourising palette to recolour masked areas in texture
			if (matchPattern shaderName pattern:"*palette*") do 
			(
				--format "  Palette shader found: %\n" shaderName
				texFlags = bit.set texFlags FLAG_DRAWBL_PALETTE true
			)
		-- PV_DRAWBL_PROXY_TEX = 1 << 7, // this drawable should remap its textures to the set on variation 000
			-- (not set yet...)
		)
		
		--format ": %\n" (texFlags as string)

		return texFlags
	),
	
	fn UpdateComponentMap = 
	(
		local actualComponentCount = 0
		
		for compNum = 1 to MetaComponentMap.Length do 
		(
			local setVal = 255
			
			if (SceneComponents[compNum] != undefined and SceneComponents[compNum].count > 0) then 
			(
				setVal = actualComponentCount
				actualComponentCount += 1
			)
			
			(MetaComponentMap.Item[compNum - 1]).Value = setVal
		)
	),

	fn CombineMetaAndScene dlcName:undefined = 
	(
		------------------------------------------------------------------------------------------------------------
		-- GENERATE PED-COMPONENT LIST
		------------------------------------------------------------------------------------------------------------
		local actualComponentCount = 0
		local delCount = 0
		
		for compNum = 1 to MetaComponentMap.Length do 
		(
			local nodeNum = MetaComponentMap.Item[compNum - 1].Value
			
			if (nodeNum == 255) then 
			(
				-- Add component-node if this enum is currently unmapped:
				if (SceneComponents[compNum] != undefined and SceneComponents[compNum].count > 0) do 
				(
					-- Update component set:
					local compTunable = MetaDataManager.CreateNewStructTunableFor("aComponentData3")
					MetaComponents.Insert actualComponentCount compTunable
				)
			)
			else 
			(
				if (SceneComponents[compNum].count == 0) then 
				(
					idxToRemove = nodeNum - delCount
					if ( idxToRemove > 0 ) do
					(
						MetaComponents.RemoveAt[idxToRemove]
						delCount += 1
					)
				)
				else 
				(				
					actualComponentCount += 1
				)
			)
		)
		
		 -- Make sure our indexes match the node-numbers:
		UpdateComponentMap()
		
		local hasTexVars = false
		local hasDrawVars = false
		
		local drawableArrayName = "aDrawblData3"
		local texVariationArrayName = "aTexData"
		
		-- Now review the component variations
		for compIdx = 0 to (MetaComponentMap.Length - 1) do 
		(
			local variationIdx = MetaComponentMap.Item[compIdx].Value
			local compObjsList = SceneComponents[compIdx + 1]
			local alternativeList = SceneComponentAlternatives[compIdx + 1]
			
			if (MetaComponents.Length > 0) do
			(
				if (variationIdx != 255) do 
				(
					local compNode = MetaComponents.Item(variationIdx)
					local compDrwblList = compNode.Item(drawableArrayName)

					-- Resize drawable-node list if required:
					setNodeCount compDrwblList drawableArrayName compObjsList.count

					local texCount = 0
					local compVars = CompTexVars[compIdx + 1]
					if (compVars != undefined) do 
					(
						for meshIdx = 0 to (compObjsList.count - 1) do 
						(
							hasDrawVars = true

							local drawableObj = compObjsList[meshIdx + 1]
							local alternativeCount = alternativeList[meshIdx + 1]

							local drawableNode = compDrwblList.item[meshIdx]						
							local propMaskNode = drawableNode.Item "propMask"

							local numAlternativesNode = drawableNode.Item "numAlternatives"
							numAlternativesNode.value = alternativeCount

							local propMask = propMaskNode.value						
							local newPropMask = getObjMaterialFlags drawableObj texVarList:CompTexVars compIdx:compIdx meshIdx:meshIdx startFlags:propMask
							propMaskNode.value = newPropMask

							-- See if this drawable has a cloth sim mesh attached
							local clothDataNode = drawableNode.Item "clothData"						
							if (clothDataNode != undefined ) do
							(
								local ownsClothNode = clothDataNode.Item "ownsCloth"
								if (ownsClothNode != undefined and (getattrclass drawableObj == "Gta Object")) do
								(					
									local ownsClothValue = false
									simObject = RsLodDrawable_GetSimulationModel drawableObj
									if (simObject != undefined) do
									(
										ownsClothValue = true
									)
									ownsClothNode.value = (dotNetObject "system.boolean" ownsClothValue)
								)
							)

							-- Update texture-variation nodes:
							local meshTexVarsStruct = compVars[meshIdx + 1]
							local texVarList = drawableNode.Item(texVariationArrayName)

							if (meshTexVarsStruct == undefined) then 
							(
								texVarList.clear()
							)
							else 
							(
								meshTexVars = meshTexVarsStruct.texVarRaces
								local meshTexVarCount = meshTexVars.count

								-- Resize drawable-node list if required:
								setNodeCount texVarList texVariationArrayName meshTexVarCount defaults:#(dataPair name:"distribution" value:255)

								if (meshTexVarCount != 0) then 
								(
									texCount += meshTexVarCount
									hasTexVars = true

									for meshTexVarNum = 1 to meshTexVarCount do 
									(
										local texVarTunable = texVarList.item[meshTexVarNum - 1]
										local texIdVal = meshTexVars[meshTexVarNum]

										if ( texIdVal != undefined ) then
										(
											(texVarTunable.Item "texId").value = texIdVal
										)
										else
										(
											gRsUlog.LogError ("Mesh: " + drawableObj.name + " has no textures listed in the pedtex list!") context:drawableObj
											return false
										)
									)
								)
							)
						)
					)

					(compNode.Item "numAvailTex").value = texCount
				)
			)
		)
		
		------------------------------------------------------------------------------------------------------------
		-- GENERATE COMPINFOS LIST
		------------------------------------------------------------------------------------------------------------
		local compInfoNodesArray = for n = 1 to ComponentEnums.count collect #()
		local removeNodes = #()
		
		-- Work out which nodes need to be retained:
		for nodeIdx = 0 to (MetaCompInfos.length - 1) do 
		(
			local compInfoNode = MetaCompInfos.item[nodeIdx]
			local compId = (compInfoNode.Item "pedXml_compIdx").value + 1
			local drawblId = (compInfoNode.Item "pedXml_drawblIdx").value + 1

			if (SceneComponents[compId][drawblId] == undefined) then 
			(
				append removeNodes compInfoNode
			)
			else 
			(
				compInfoNodesArray[compId][drawblId] = compInfoNode
			)
		)
		
		-- Remove prop-nodes that have no equivalent object:
		for removeNode in removeNodes do 
		(
			MetaCompInfos.remove removeNode
		)
		
		-- Add new compInfo-nodes:
		for compIdx = 0 to (SceneComponents.count - 1) do 
		(
			local compObjList = SceneComponents[compIdx + 1]
			
			for drawblIdx = 0 to (compObjList.count - 1) do 
			(			
				local compInfoNode = compInfoNodesArray[compIdx + 1][drawblIdx + 1]
				
				if (compInfoNode == undefined) do 
				(
					compInfoNode = MetaDataManager.CreateNewStructTunableFor "compInfos"
					
					(compInfoNode.Item "pedXml_compIdx").value = compIdx
					(compInfoNode.Item "pedXml_drawblIdx").value = drawblIdx
				
					MetaCompInfos.Add compInfoNode						
--format "ADDING COMPINFO NODE %,%\n" compIdx compIdx
				)
				
				-- Make sure that expressions array is correct length:
				local exprModsArray = compInfoNode.Item "pedXml_expressionMods"
				local curModCount = exprModsArray.length
				for n = 1 to expressionModCount where (n > curModCount) do 
				(
					addToList exprModsArray 0.0
				)
			)
		)		
		
		------------------------------------------------------------------------------------------------------------
		-- GENERATE PROP-LIST
		------------------------------------------------------------------------------------------------------------
		-- Build array of existing nodes, per enum:
		local propNodesArray = for n = 1 to PropEnums.count collect #()
		local removeNodes = #()
		
		local anchorMax = 0
		local propIDMax = 0
				
		-- Work out which prop-nodes need to be retained:
		for nodeIdx = 0 to (metaProps.length - 1) do 
		(
			local propNode = metaProps.item[nodeIdx]
			local anchorId = (propNode.Item "anchorId").value + 1
			local propId = (propNode.Item "propId").value + 1

			if (SceneProps[anchorId][propId] == undefined) then 
			(
				append removeNodes propNode
			)
			else 
			(
				propNodesArray[anchorId][propId] = propNode
				
				-- Use these to know where to place things in new list
				if(anchorId > anchorMax) do (anchorMax = anchorId)
				if(propId > propIDMax) do (propIDMax = propId)
			)
		)
	
		-- Remove prop-nodes that have no equivalent object:
		for removeNode in removeNodes do 
		(
			metaProps.remove removeNode			
		)		
		
		-- Order list here, there were bugs starting to happen where initial meta files would be out of order
		-- if people started adding in new items from the same prop area e.g:
		--	p_head_000
		--	p_head_001
		--  P_eyes_000
		--	P_ears_000
		--	p_head_002
		-- 
		-- That would cause an assert in game as later when doing the alpha and decal check, that list would be 
		-- out of sync with this list.
		if( anchorMax != 0 and propIDMax != 0 ) do
		(
			metaProps.clear()
			for anchorIdx = 1 to anchorMax do
			(
				for propIdx = 1 to propIDMax do
				(
					if(propNodesArray[anchorIdx][propIdx] != undefined) do
					(
						metaProps.Add propNodesArray[anchorIdx][propIdx]
					)
				)
			)
			-- metaProps is now ordered
		)

		-- AJM:TODO:REMOVE when psc is updated to remove these old prop ID arrays
		local maxPropIdList = MetaDataManager.FindFirstStructureNamed "aMaxPropId"
		if ( maxPropIdList != undefined ) do
		(
			maxPropIdList.clear()		
			local maxPropTexIdArray = for n = 1 to PropEnums.count collect (for n = 1 to MAX_PED_PROPS collect 0)
		)
		-- AJM:TODO:REMOVE
		
		local objNum = 0
		
		-- Bitset array index values
		local BITSET_FLAG_ALPHA = "PRF_ALPHA"
		local BITSET_FLAG_DECAL = "PRF_DECAL"
		local BITSET_FLAG_CUTOUT = "PRF_CUTOUT"
		
		local anchorList = MetaDataManager.FindFirstStructureNamed "aAnchors"
		
		if ( anchorList != undefined ) then 
		(
			anchorList.clear()
		)
		else
		(
			anchorList = dotNetObject "System.Collections.Generic.List`1[System.String]"
		)
		
		-- Set up each anchor-point node:
		for anchorIdx = 0 to (SceneProps.count - 1) do 
		(
			local anchorObjs = SceneProps[anchorIdx + 1]
			local anchorMatFlags = 0
			
			-- Add anchor-point's prop-count to maxPropId list:
			local anchorObjPropCount = (for obj in anchorObjs where (obj != undefined) collect obj).count
			
			-- AJM:TODO:REMOVE when psc is updated to remove these old prop ID arrays
			if ( maxPropIdList != undefined ) do
			(
				addToList maxPropIdList anchorObjPropCount
			)
			-- AJM:TODO:REMOVE
			
			-- Create new CAnchorProps struct
			anchorPropStruct = MetaDataManager.CreateNewStructTunableFor "aAnchors"
			
			-- Get the props array which contains one entry for each prop on this anchor, with the number
			-- of textures.
			local propsArray = anchorPropStruct.Item "props"
			
			if(anchorObjPropCount > 0 ) do
			(					
				(anchorPropStruct.Item "anchor").value = anchorIdx		
				
				for propIdx = 0 to (anchorObjs.count - 1) do 
				(
					local propObj = anchorObjs[propIdx + 1]

					if (propObj != undefined) do 
					(
						objNum += 1

						local propNode = propNodesArray[anchorIdx + 1][propIdx + 1]

						-- Add new prop-nodes:
						if (propNode == undefined) do 
						(
							propNode = MetaDataManager.CreateNewStructTunableFor "aPropMetaData"
							(propNode.Item "anchorId").value = anchorIdx
							(propNode.Item "propId").value = propIdx

							metaProps.Add propNode
						)


						-- Make sure that expressions array is correct length:
						local exprModsArray = propNode.Item "expressionMods"
						local curModCount = exprModsArray.length
						for n = 1 to expressionModCount where (n > curModCount) do 
						(
							addToList exprModsArray 0.0
						)

						------------------------------------------------------------------------------------------------------------
						-- GENERATE PROP-TEXTURES LIST
						------------------------------------------------------------------------------------------------------------

						local objMats = #()
						local propMatFlags = getObjMaterialFlags propObj texVarList:PropTexVars compIdx:anchorIdx meshIdx:propIdx objMats:objMats

						local renderFlagsArray = #()
						local renderFlagsString = ""
						if ( bit.get propMatFlags FLAG_DRAWBL_ALPHA ) do ( append renderFlagsArray BITSET_FLAG_ALPHA )
						if ( bit.get propMatFlags FLAG_DRAWBL_DECAL ) do ( append renderFlagsArray BITSET_FLAG_DECAL ) 
						if ( bit.get propMatFlags FLAG_DRAWBL_CUTOUT ) do ( append renderFlagsArray BITSET_FLAG_CUTOUT ) 

						for bitFlagIdx = 1 to renderFlagsArray.Count do
						(
							renderFlagsString += renderFlagsArray[bitFlagIdx]
							if (bitFlagIdx != renderFlagsArray.Count) do
							(
								renderFlagsString += " "
							)
						)

						(propNode.Item "renderFlags").value = renderFlagsString			

						local texNodesArray = #()
						local removeNodes = #()
						local propTexList = propNode.Item "texData"
						local propTexVarData = PropTexVars[anchorIdx + 1][propIdx + 1]
						
						local propTexCount = 0

						-- Races aren't used for props, this list is just used to indicate if a particular index is used:
						local raceEnumList = propTexVarData.texVarRaces

						-- Work out which texture-nodes need to be retained/removed:
						local usedTexIds = #()
						for texNodeIdx = 0 to (propTexList.length - 1) do 
						(
							local texNode = propTexList.item[texNodeIdx]
							local texIdVal = (texNode.Item "texId").value
							local varIdx = (texIdVal + 1)
							
							-- Have we already seen this texId value?
							local uniqueTexId = (appendIfUnique usedTexIds texIdVal)
							
							if (not uniqueTexId) or (raceEnumList[varIdx] == undefined) then 
							(
								if (not uniqueTexId) do 
								(
									format "* Removing prop texData item with duplicated texId: % (%)\n" texIdVal propObj.name
								)
								
								append removeNodes texNode
							)
							else 
							(
								texNodesArray[varIdx] = texNode
								propTexCount += 1
							)
						)
						
						-- Remove unwanted texture-nodes:
						for removeNode in removeNodes do 
						(
							propTexList.remove removeNode
						)

						-- Add new texture-nodes:

						for varIdx = 0 to (raceEnumList.count - 1) do 
						(
							local texVarData = raceEnumList[varIdx + 1]

							if (texVarData != undefined) do 
							(
								local texNode = texNodesArray[varIdx + 1]

								if (texNode == undefined) do 
								(
									texNode = MetaDataManager.CreateNewStructTunableFor "texData"
									(texNode.Item "distribution").value = 255
									(texNode.Item "texId").value = varIdx

									propTexList.Add texNode
									propTexCount += 1								
								)
							)
						)
						-- AJM:TODO:REMOVE when psc is updated to remove these old prop ID arrays
						-- Set maxPropTexId value for this prop:
						if (maxPropIdList != undefined and maxPropTexIdArray != undefined) do
						(
							maxPropTexIdArray[anchorIdx + 1][propIdx + 1] = propTexCount
						)
						-- AJM:TODO:REMOVE
						
						-- Add the texture count to the list.
						addToList propsArray propTexCount
					)
				) -- for propIdx = 0 to (anchorObjs.count - 1)
				
				-- add the CAnchorProps struct to the aAnchors array
				anchorList.Add anchorPropStruct
			) -- if(anchorObjPropCount > 0 )
		)
		
		-- Do a final sort of all metaProps
		local tempProps = #()
		for anchorIdx = 0 to (SceneProps.count - 1) do
		( 
			local propNodes = for nodeIdx = 0 to (metaProps.length - 1) where (metaProps.item[nodeIdx].Item "anchorId").value == anchorIdx collect metaProps.item[nodeIdx]
			local counter = 0
			for i = 1 to propNodes.count do
			(
				if (propNodes[i].Item "propId").value == counter do
				(
					append tempProps propNodes[i]
					counter += 1
					i = 0
				)
			)
		)
		
		metaProps.clear()
		for prop in tempProps do
			metaProps.Add prop
		
		-- AJM:TODO:REMOVE when psc is updated to remove these old prop ID arrays
		-- Build maxPropTexId array:
		local maxPropTexIdList = MetaDataManager.FindFirstStructureNamed "aMaxPropTexId"
		if ( maxPropTexIdList != undefined and maxPropTexIdArray != undefined ) do
		(
			maxPropTexIdList.clear()
			for list in maxPropTexIdArray do 
			(
				for val in list do 
				(
					addToList maxPropTexIdList val
				)
			)
		)
		-- AJM:TODO:REMOVE
		
		setMetaValue "numAvailProps" objNum
		if ( dlcName == undefined ) do dlcName = ""
		setMetaValue "dlcName" dlcName
	),
	
	fn SerialiseMetaData = 
	(
		MetaDataManager.Metadata.SerialiseAlt(MetaFilePath)
	),
	
	fn SyncComponentMeta dummynode dlcName:undefined = 
	(
		if MetaDataManager == undefined do 
		(
			return false
		)
		
		-- Load the metadata:
		LoadMetas()
		
		-- Map the scene data into a meta comparable structures
		if not (SynthesiseSceneObjMapsFrom dummyNode) do return false
		
		-- Generate texture-variations data-array from ped's texture-directory:
		if getTextureVariations dummynode == false do return false
		
		-- Combine the meta and scene dat
		CombineMetaAndScene dlcName:dlcName
		
		-- Finally, serialise meta back out
		SerialiseMetaData()
		
		format "Variation metadata export complete\n"
		
		return true
	),
	
	-- Function used to test export:
	fn testExport cleanExport:true = 
	(
		local rootobj = (getCurrentSelection())[1]
			
		-- Strip tags from object-names:
		local revertNames = for obj in rootObj.children collect (dataPair obj:obj oldName:obj.name)
		for obj in rootObj.children do 
		(
			local newName = (filterString obj.name " ")[1]
			
			if obj.name != newName do 
			(
				obj.name = newName
			)
		)
		
		local metafilepath = RsConfigGetAssetsDir() + "metadata/characters/" + rootobj.name + ".pso.meta"
		RsMakeSurePathExists metafilepath
		
		if doesFileExist metafilepath do 
		(
			setFileAttribute metafilepath #readOnly false
		)
		
		if cleanExport and (doesFileExist metafilepath) do 
		(
			deleteFile metafilepath
		)
		
		local metaUtils = RsPedMetaUtilSuite()
		metaUtils.InitWith metafilepath

		metaUtils.SyncComponentMeta rootobj
		
		-- Revert names:
		for item in revertNames do 
		(
			if (item.obj.name != item.oldName) do 
			(
				item.obj.name = item.oldName
			)
		)
	)
)

--RsPedMetaUtilSuite.testExport cleanExport:FALSE
