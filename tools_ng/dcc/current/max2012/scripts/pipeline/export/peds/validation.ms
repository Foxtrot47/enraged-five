-- Rockstar Ped Test
-- Rockstar North
-- 23/11/2005
-- by Greg Smith

filein "rockstar/util/anim.ms"
filein "utils.ms"
filein "pipeline/util/utilityfunc.ms"
filein "pipeline/export/ui/exporter_dependency_dialog.ms"

struct pedobjwarning ( object, errmsg )
RsPedWarnings = #()
RsBatchWarnings = #()
RsPedErrors = #() 
global gRsPedBoneTagDict = dotnetobject "RSG.MaxUtils.MaxDictionary"


--------------------------------------------------------------
-- Skeleton validations
--------------------------------------------------------------		


fn ReadInBoneTagXml = 
(
	gRsPedBoneTagDict.clear()
	try(
		local xmlFileName = RsConfigGetContentDir()+"animconfig/bone_tags.xml"
		local xmlDoc = XMLDocument()
		xmlDoc.load xmlFileName
		xmlDoc.ParseIntoMaxscripthierarchy()
		local boneidsTag = xmlDoc.maxXmlObject.children[2].children[1].children[1]
		for idItem in boneidsTag.children do
		(
			local nameAttr = toLower (idItem.attrs.get "name")
			local idAttr = idItem.attrs.get "id"
			if gRsPedBoneTagDict.containsKey nameAttr then
				gRsUlog.LogWarning ("bone_tag.xml contains duplicate entry for "+nameAttr as string)
			else
				gRsPedBoneTagDict.add nameAttr idAttr
		)
	)
	catch(
		gRsUlog.LogWarning ("bone_tag.xml failed to read in properly:"+getCurrentException() as string)
	)
)

-- fn HasFacialRig root =
-- (
-- 	if 	matchPattern root.name pattern:"FACIAL_*" or 
-- 		matchPattern root.name pattern:"FB_*" then
-- 	(
-- 		return true
-- 	)
-- 	
-- 	for c in root.children do
-- 		if HasFacialRig c then
-- 			return true
-- 		
-- 	return false
-- )

fn RsCheckPedSkelRec obj facialSkel:false= 
(
	local nameParts = filterString obj.name " "
	local namecheck = toLower nameParts[1]
	local boneTagVal = getuserprop obj "tag"
	
	if 	facialSkel and
		"Gta Collision" != getattrclass obj and 
		not matchPattern namecheck pattern:"*footsteps*" and
		not matchPattern namecheck pattern:"*nub" and
		undefined == boneTagVal
		then
	(
		gRsULog.LogWarning ("Ped has a facial rig, but bone "+ obj.name + " doesnt have a tag.") context:obj
	)
	
	local boneTagXmlVal = undefined
	gRsPedBoneTagDict.tryGetValue namecheck &boneTagXmlVal
	if (undefined != boneTagVal and
		"DO_NOT_EXPORT" != boneTagVal ) then
	(
		if undefined == boneTagXmlVal then
			gRsUlog.LogWarning ("Bone "+namecheck+" has tag \""+boneTagVal as string+"\" but no entry in bone_tag.xml filexml file") context:obj
		else if boneTagXmlVal != boneTagVal then
			gRsUlog.LogWarning ("Bone tag \""+boneTagVal as string+"\" on object "+namecheck+" does not match value \""+boneTagXmlVal as string+"\" in xml file") context:obj
	)
	
	if 	matchPattern namecheck pattern:"FACIAL_*" or 
		matchPattern namecheck pattern:"FB_*" then
	(
		facialSkel = true
	)

	for childobj in obj.children do 
	(
		RsCheckPedSkelRec childobj facialSkel:facialSkel
	)
)

-- This goes through sibling objects if we're looking at a cloth mesh object, to ensure it has a matching mesh part
fn RsPedMatchClothMesh clothObj = (
	
	if (RsLodDrawable_HasRenderModel clothObj == false) do
	(
		gRsULog.LogError (clothObj.name + " has no render model linked!") context:clothObj
	)
)		
		
--------------------------------------------------------------
-- Test an individual object before exporting
--------------------------------------------------------------	
fn RsPedTestObject obj = 
(
	local retVal = true
	
	local regexp = dotnetobject "System.Text.RegularExpressions.Regex" "[^a-zA-Z0-9_ ]"
	if 	(regexp.isMatch obj.name) and 
		not (matchPattern obj.name pattern:"*-L") then
	(
		gRsUlog.LogError ("Object "+obj.name+" has a name with non-xml-conform characters. This can break all sorts of things. Please correct.") context:obj
		retVal = false	
	)
	
	if isEditPolyMesh obj do 
	(
		local ctrlr = obj.controller
		
		-- Link_Constraint's dont have a scale option, could use hasproperty but this is cleaner:
		if not isKindOf obj.controller Link_Constraint do
		(
			if not (DoesVectorMatch obj.controller.scale [1,1,1] 0.01) then 
			(
				gRsULog.LogError (obj.name + " has scaling, cant export") context:obj
				retVal = false
			)
		)
		
		-- Check if the mesh parts have a correctly named mesh, based on the naming convention
		if ( matchpattern obj.name pattern:"P_*" == false ) then
		(
			-- Mesh names are of the form <partName>_<meshNum>_<race type>
			-- e.g 
			-- uppr_000_u
			-- lowr_001_r
			-- Sometmies they have extra characters on the end e.g:
			-- accs_000_u Necklace
			--
			-- Cloth test peds can have special naming conventions:
			-- CLOTHSIM_hair_000_U
			-- Lowr_000_R_SIM
			-- OldREGEX = local meshNamePattern = "^(CLOTHSIM_)?[a-zA-Z]{4}_[0-9]{3}_[a-zA-Z](_[0-9]{1})?(_SIM)?($|\s)"
			local meshNamePattern = "^(CLOTHSIM_)?[a-zA-Z]{4}_[0-9]{3}_[a-zA-Z](_[0-9]{1,2}?)?(_SIM)?($|\s)"
			rgx = dotNetObject "System.Text.RegularExpressions.Regex" meshNamePattern		
		
			if (rgx.IsMatch obj.name == false) do
			(
				gRsULog.LogError (obj.name + " has an incorrect mesh name, it should be of the form <part>_<num>_u|r <optional text> e.g uppr_000_u") context:obj
				retVal = false	
			)
		)
		else
		(
			-- Check the prop mesh name is correct e.g:
			-- P_head_000
			local propNamePattern = "^(P|p)_[a-zA-Z]+_[0-9]{3}($|\s)"
			rgx = dotNetObject "System.Text.RegularExpressions.Regex" propNamePattern
			if (rgx.IsMatch obj.name == false) do
			(
				gRsULog.LogError (obj.name + " has an incorrect prop mesh name, it should be of the form P|p_<part>_<num> e.g p_head_000") context:obj
				retVal = false	
			)			
		)
		
		-- If this is a cloth mesh, ensure that there's a mesh that it corresponds to
		if ((matchpattern obj.name pattern:"CLOTHSIM_*") or (matchpattern obj.name pattern:"*_SIM*")) do
		(	
			RsPedMatchClothMesh obj					
		)
		
		-- Check to make sure a texture number matches the mesh it's on.  Ugly test to get around
		-- noise textures not having mesh numbers, and wrinkle masks having numbers but only a single one at 
		-- the end
		local texMapsList = #()
		RsGetTexMapsFromObj obj texMapsList #() #()
		if texMapsList.count > 0 do
		(			
			-- Props are P_<part>_<num>
			-- Components are <part>_<num>_<race>
			local meshNum = if (ObjIsProp obj) then (filterstring obj.name "_ ")[3] else (filterstring obj.name "_ ")[2]
			for texmap in texMapsList where matchpattern texmap pattern:"*diff*" do
			(
				local nameTokens = filterstring texmap "_"
				local containsMeshNum = false
				for token in nameTokens do
				(
					if (token as integer != undefined) do containsMeshNum = true
				)
				
				if containsMeshNum do
				(
					if (not matchPattern texmap pattern:("*" + meshNum + "*")) then
					(
						local failMsg = stringStream ""
						format "Mesh part (%) has a texture with a different number (%) in its name!" obj.name texmap to:failMsg					
						gRsUlog.LogError (failMsg as string) context:obj				
						retVal = false				
					)
				)
			)
		)
	)
	
	componentNameArray = #()
	
	for childObj in obj.children do 
	(
		local nameParts = filterString childObj.name " "
		local componentName = tolower nameParts[1]
		
		-- Check to see if we have an identical component in the scene under the same root.
		if not (appendIfUnique componentNameArray componentName) then
		(
			local failMsg = stringStream ""
			format "Duplicate component found (%) under root (%)" childObj.name obj.name to:failMsg
			gRsULog.LogError (failMsg as string) context:obj
			return false
		)
		else
		(
			retVal = (RsPedTestObject childObj) and retVal
		)
	)
	
	return retVal
)

fn FixUpBoneUserPropBufferRec obj = 
(
	local buff = getuserpropbuffer obj
	if buff[buff.count] != '\n' then
		setUserPropBuffer obj (buff+"\n")
	for c in obj.children do
		FixUpBoneUserPropBufferRec c
)

fn ModelExistsInChildren obj children =
(
	for child in children do 
	(
		if obj == child then
		(
			return true
		)
	)
	
	return false
)

--------------------------------------------------------------
-- Pre-export ped-tests:
--------------------------------------------------------------
fn RsPedExportValidate selectedObjs pedExportInfo exporting:false quick:false = 
(
	local pedObj, pedName
	
	local compExport = pedExportInfo.compExport
	local propExport = pedExportInfo.propExport
	local isPreview = pedExportInfo.previewExport
	local buildZips = pedExportInfo.buildZips
	
	gRsULog.init "Ped Validation"

	ReadInBoneTagXml()
	
	local dummyObjs = for obj in selectedObjs where (isKindOf obj Dummy) collect obj
	
	-- Check to see that we have something selected
	case of 
	(
		(selectedObjs.count > 1):
		(
			gRsULog.LogError "Multiple objects selected for export (should be just 1 dummy)"
		)
		(dummyObjs.count == 0):
		(
			gRsULog.LogError "No dummy-object selected for export"
		)
		(dummyObjs[1].children.count == 0):
		(
			gRsULog.LogError "Dummy-object has no children"
		)
	)
	
	if (gRsULog.ValidateJustErrors()) then 
	(
		pedObj = pedExportInfo.obj = dummyObjs[1]
		pedName = pedExportInfo.pedName = pedObj.name
	)
	else 
	(
		return false
	)
	
	gRsULog.LogMessage "Game running check..."
	if exporting do 
	(
		-- Cancel export if changing game-data, and game is still running:
		if buildZips and not isPreview do 
		(
			if RSRemConIsGameRunning queryRestart:true do 
			(
				gRsULog.LogError "Can't export while game is running"
			)
		)
	
		RsP4LogUserLabelStatus()
		
		if not gRsULog.ValidateJustErrors() do return false
	)
	
	-- Set up the export parameters struct:
	pedExportInfo.init pedObj
	
	if quick do
	(
		return true
	)
	
	--------------------------------------------------------------
	-- Is pedName listed in nm.xml?
	--------------------------------------------------------------
	gRsULog.LogMessage "Ragdoll XML list check..."
	(
		local coreAssetsDir = RsConfigGetAssetsDir core:true
		local nmXmlDirectory = coreAssetsDir + "characters"
		local nmXmlFilename = nmXmlDirectory + "/nm.xml"
		
		if ( not doesFileExist nmXmlFilename ) do 
		(
			gRsULog.LogWarning ("Ragdoll XML file not found.\n\tSyncing: " + nmXmlFilename)
            -- RsSyncDepList expects a list of dependencies, so let's build one with nm.xml path
            local syncpath = #(nmXmlFilename)
            RsSyncDepList syncpath
		)
			
		local document = XmlDocument()
		document.Load nmXmlFilename
		
		local matchNodes = document.Document.SelectNodes( "/xmlmatchs/xmlmatch" )
		local matchNodesIter = matchNodes.GetEnumerator()
		local matchFound = false
		
		while ( matchNodesIter.MoveNext() and (not matchFound) ) do
		(
			local matchElem = matchNodesIter.Current
			if ( undefined != matchElem ) then
			(			
				local sourceAttr = matchElem.Attributes.GetNamedItem("source")
				local sourceName = sourceAttr.Value
				matchFound = matchPattern (getFilenameFile sourceName) pattern:pedName
			)
		)
		
		if not matchFound do 
		(
			gRsUlog.LogError ("Ped-name \"" + pedName + "\" was not found in: "+ nmXmlFilename) context:pedObj
		)
	)
	
	local useObjs = #()
	local exportTextures = #()
	local checkedSkeletons = #()

	local bFoundComps = false
	local bFoundProps = false
	
	for dummyObj in #(pedExportInfo.obj, pedExportInfo.lodObj) where (dummyObj != undefined) do 
	(
		RsPedTestObject dummyObj

		--url:bugstar:1196896
		objectsToValidate = #() 

		-- For each child
		for ob in dummyobj.children do
		(	
			found = false
			-- for each itemName in ignoredComponents
			for ignoredComponentName in pedExportInfo.ignoredComponents while (found == false) do
			(
				found = (matchpattern ob.name pattern:(ignoredComponentName + "*"))
			)

			if (found == false) then
			(
				if(ObjIsProp ob) then
					bFoundProps = true
				else
					bFoundComps = true
					
				append objectsToValidate ob
			)
		)

		if (objectsToValidate.count == 0) then 
		(
			objectsToValidate = dummyObj.children
		)
		
		for obj in objectsToValidate do
		(
			local isPropObj = ObjIsProp obj
			local useObj = false
			
			if RsLodDrawable_HasRenderModel obj then
			(
				local renderModel = RsLodDrawable_GetRenderModel obj
				if ModelExistsInChildren renderModel dummyObj.children  == false then
				(
					gRsULog.LogError ("Render model: '" + renderModel.name + "' for: '" + obj.name + "' is not in dummy hierarchy") context:obj	
				)
			)
			
			if RsLodDrawable_HasSimulationModel obj then
			(
				local simModel = RsLodDrawable_GetSimulationModel obj
				if ModelExistsInChildren simModel dummyObj.children  == false then
				(
					gRsULog.LogError ("Simulation model: '" + simModel.name + "' for: '" + obj.name + "' is not in dummy hierarchy") context:obj
				)
			)
			
			case of 
			(
				(compExport and (not isPropObj)):
				(
					useObj = true
					
					local unskinned = true
					for checkMod in obj.modifiers while unskinned do 
					(
						unskinned = not (isKindOf checkMod Skin)
					)
					
					if unskinned then 
					(
						gRsULog.LogError ("Component-mesh is unskinned: " + obj.name) context:obj
					)
					else 
					(					
						local rootbone = rexGetSkinRootBone obj
						print (obj as string +" has skeleton "+ rootbone as string)
						if (rootbone == undefined) then 
						(
							gRsULog.LogError ("Unable to acquire root-bone for " + obj.name) context:obj
						)
						else 
						(
							if 0==findItem checkedSkeletons rootbone then
							(
								RsCheckPedSkelRec rootbone
								append checkedSkeletons rootbone
							)
						)
					)
				)
				(propExport and isPropObj):
				(
					useObj = true
				)
			)
			
			if useObj do 
			(
				append useObjs obj
				
				-- Add object's textures to list:
				RsGetTexMapsFromObjNoStrip obj exportTextures #() #()
				
				-- Adds errors for StandardMats:
				RsCheckMaterials obj
			)
			
			gc light:true
		)
	)
	if(pedExportInfo.isStreamedExport) do
	(
		if bFoundProps == false do
		(
			gRsUlog.LogWarning ("Skipping props export, there are no props selected for export")
			pedExportInfo.propExport = false
		)
		if bFoundComps == false do
		(
			gRsUlog.LogWarning ("Skipping component export, there are no components selected for export")
			pedExportInfo.compExport = false
		)
	)
	for filename in exportTextures where not doesFileExist filename do 
	(
		gRsULog.LogError ("Texture doesn't exist locally: '" + filename + "'")
	)
	print "Ped Validation complete"
	gRsULog.ValidateJustErrors()	
)
