-- fragment helper functions

fn TraverseHierarchyRec root func &params useRsSceneLink:false = 
(
	if "Gta Object"!=(getattrclass root) then
		return false
	
	func root &params
	
	for c in root.children do
		TraverseHierarchyRec c func &params 
	
	if useRsSceneLink then
	(
		local linkChildren = #()
		RsSceneLink.GetChildren -1 root &linkChildren
		for c in linkChildren do
			TraverseHierarchyRec c func &params useRsSceneLink:useRsSceneLink
	)
)

fn CollectMeshes model &meshes = (
	if classof model == Editable_Poly or classof model == Editable_Mesh or classof model == PolyMeshObject then (
		append meshes model
	)
)

fn CollectClothBounds model &clothBounds = 
(
	if ( RsCloth.IsCloth model ) then
	(
		savesel = #()
		local LinkType_ClochColl = 3
		RsSceneLink.getChildren LinkType_ClochColl model &savesel
		join clothBounds savesel
	)
)

fn CollectClothMeshes model &meshes = 
(
	if ( RsCloth.IsCloth model ) then
	(
		appendIfUnique meshes model
	)
)

fn SetLinkUserProps model &params = 
(
--	local isCloth = RsCloth.IsCloth model
	local groupMeshes
	if undefined!=params[4] then
		groupMeshes = RsLodDrawable_SetUserProps model &params[2] hasCloth:params[3] clothtypegroup:params[4] ignoreLeafReset:true
	else
		groupMeshes = RsLodDrawable_SetUserProps model &params[2] hasCloth:params[3] ignoreLeafReset:true

	join params[1] groupMeshes
)

fn CollectGlassNodeNames model &params = 
(
	local idxIsGlass = GetAttrIndex "Gta Object" "Is Breakable Glass"
	local glassNodeString = &params[1]
	local glassType = &params[2]
	if (getattrclass model)=="Gta Object" and (GetAttr model idxIsGlass) then
	(
		if *glassNodeString!="" then
			*glassNodeString += ";" 
		*glassNodeString += model.name
		
		-- glass type
		local idxGlassType = GetAttrIndex "Gta Object" "Glass type"
		local myGlassType = toUpper (GetAttr model idxGlassType)
		if ""!=*glassType and myGlassType != *glassType then
			gRsUlog.LogError (model.name+" has different glass type set than other child in the hierarchy. ("+myGlassType+", "+*glassType+")") context:model
		*glasstype = myGlassType
	)
)

fn CheckForCloth model &params = 
(
	if (RsCloth.IsCloth model) then
		params[1] = true
	else
		params[2] = false
)