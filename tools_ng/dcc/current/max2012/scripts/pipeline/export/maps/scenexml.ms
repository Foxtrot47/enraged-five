--
-- File: pipeline/export/maps/scenexml.ms
-- Desc: SceneXml exporting and map metadata file creation functions.
--
-- Author: David Muir <david.muir@rockstarnorth.com>
-- Date: 19 January 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/globals.ms"
filein "rockstar/export/settings.ms"
--filein "pipeline/util/file.ms" -- loaded on startup by startup.ms
filein "pipeline/util/RSProcess.ms"

useUniLog = true

--
-- name: RsSceneXmlDefaultLogFile
-- desc: Return default SceneXml processing log filename.
--
fn RsSceneXmlDefaultLogFile mapname suffix = (	
	return (RsConfigGetMapStreamDir() + "maps/" + mapname + "/" + mapname + "_" + suffix + ".log" )
)

--
-- name: RsSceneXmlExport
-- desc: Export map container SceneXml XML file.
--
fn RsSceneXmlExport map = (

	-- ensure that the content tree is loaded before trying to do anything with the maps
	if (not RsContentTree.IsContentTreeLoaded()) do RsContentTree.LoadContentTree()

	RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."
	
	-- Ensure that RsRef filenames are up-to-date before exporting:
	RsRefFuncs.setObjFilenames geometry debugFuncSrc:"RsSceneXmlExport"
	
	pushPrompt ("Exporting SceneXML: " + map.name)
	
	local sceneXMLResult = false
	
	local contentNode = RsContentTree.GetContentNode map.filename
	local exportZip = RsContentTree.GetMapExportZipNode contentNode
	local sceneXml = RsContentTree.GetMapExportSceneXMLNode exportZip
	local sceneXmlFilename = sceneXml.AbsolutePath
	
	RsMakeSurePathExists sceneXmlFilename
	gRsPerforce.readOnlyP4Check #(sceneXmlFilename) exclusive:true

	try(
		local scenexmlExportMsg = "Scene XML Export"
		gRsUlog.ProfileStart scenexmlExportMsg
		
		SceneXmlReloadCfg()
		if ( map.is_container() ) then
			sceneXMLResult = SceneXmlExportFrom #( map.cont ) sceneXmlFilename (gRsUlog.Filename())
		else 
			sceneXMLResult = SceneXmlExportFrom map.objects sceneXmlFilename (gRsUlog.Filename())
	
		gRsUlog.ProfileEnd context:scenexmlExportMsg
	)catch
	(
		gRsUlog.ProfileEnd context:scenexmlExportMsg
		gRsUlog.LogError "SceneXML creation failed. Please report to tools."
		gRsUlog.Validate()
		progressend()
	)
	
	popPrompt()
	
	if (sceneXMLResult) then
		format "SceneXml Exported to %\n" sceneXmlFilename
	else
	(
		gRsUlog.LogError "SceneXML creation failed."
		gRsUlog.Validate()
	)
	
	return sceneXMLResult
)


--
-- name: RsSceneXmlExportAs
-- desc: Export entire scene (all containers) to a specified SceneXml XML file.
--
-- param: filename - Export file
--
fn RsSceneXmlExportAs filename = (

	-- Ensure that RsRef filenames are up-to-date before exporting:
	RsRefFuncs.setObjFilenames geometry debugFuncSrc:"RsSceneXmlExportAs"
	
	filename = RsMakeSafeSlashes filename
	RsMakeSurePathExists filename
	gRsPerforce.readOnlyP4Check #(filename) exclusive:true

	local sceneXMLResult = false

	try
	(
		SceneXmlReloadCfg()
		local sceneXMLResult = SceneXmlExport filename (gRsUlog.Filename())
	)
	catch
	(
		gRsUlog.LogError "SceneXML creation failed. Please report to tools."
		gRsUlog.Validate()
		progressend()
	)
	
	if not (sceneXMLResult) then
	(
		gRsUlog.LogError "SceneXML creation failed."
		gRsUlog.Validate()
	)
	gRsUlog.ValidateJustErrors()
	
	return sceneXMLResult
)

--
-- name: RsSceneXmlExportStats
-- desc: Export map statistics to SceneXml file.
--
-- param: filename - SceneXml XML filename (optional).
-- param: log_file - Log file (optional).
-- param: debug    - add --debug switch allowing debugger attach.
--
fn RsSceneXmlExportStats filename:unsupplied log_file:unsupplied debug:false = (

	local bindir = RsConfigGetBinDir() + "MapExport/"
	
	if ( unsupplied == filename ) then
		filename = ( RsMapSceneXmlFilename )
	local debugstr = ""
	if ( debug ) then
		debugstr = "--debug"
	
	local	mapStreamLocation = ( RsConfigGetMapStreamDir() + "maps/" + RsMapName )

	local exeFilename = stringStream ""
	format "%/MapExportIDEIPL.exe" bindir to:exeFilename
	
	local arguments = stringStream ""
	format "--nopopups --genstats --mapStream % % % " mapStreamLocation debugstr filename to:arguments

	gRsUlog.LogMessage ("RsSceneXmlExportStats - Command: " + (exeFilename as string) + " " + (arguments as string))
	
	local sceneXmlStatsProcess = RsProcess()
	sceneXmlStatsProcess.RunProcess (exeFilename as string) (arguments as string) silent:true
	
	if 0!=sceneXmlStatsProcess.exitCode then
	(
		local logFilename = gRsULog.GetProcessLogDirectory() + "/MapExportIDEIPL." + (sceneXmlStatsProcess.processID as string) + ".ulog"
		local go_ahead = gRsULog.ValidateLogFileErrors logFilename
		return go_ahead
	)
	
	return true
)

--
-- name: RsSceneXmlExportIDEForObject
-- desc: Export IDE from SceneXml file for a single object.
--
-- **** NOTE: This is still required for drawable lights ****
--
-- param: idefile - IDE file to write
-- param: filename - SceneXml XML filename (optional)
--
fn RsSceneXmlExportIDEForObject exportdir filename:unsupplied = (

	local bindir = RsConfigGetBinDir() + "MapExport/"
	local builddir = RsConfigGetExportDir()
	if ( unsupplied == filename ) then
		filename = ( RsMapSceneXmlFilename )

	local exeFilename = stringStream ""
	format "%/MapExportIDEIPL.exe" bindir to:exeFilename
	
	local arguments = stringStream ""
	format "--nopopups --build % --ideobject % %" builddir exportdir filename to:arguments
	
	gRsUlog.LogMessage ("RsSceneXmlExportIDEForObject - Command: " + (exeFilename as string) + " " + (arguments as string))
		
	local mapExportProcess = RsProcess()
	mapExportProcess.RunProcess (exeFilename as string) (arguments as string) silent:true
	
	if 0!=mapExportProcess.exitCode then
	(
		-- Get the processes log file and check it for errors
		local logFilename = gRsULog.GetProcessLogDirectory() + "/MapExportIDEIPL." + (mapExportProcess.processID as string) + ".ulog"
		local go_ahead = gRsULog.ValidateLogFileErrors logFilename
		return go_ahead
	)
	
	return (mapExportProcess.exitCode == 0)
)

--
-- name: RsSceneXmlExportIPL
-- desc: Export map single IPL file from SceneXml file.
--
-- **** NOTE: This is still required for Vehicle Nodes/Links ****
--
fn RsSceneXmlExportIPL iplver:6 = (

	-- Export SceneXml File
	local bindir = RsConfigGetBinDir() + "MapExport/"
	local builddir = RsConfigGetExportDir()
	local scenexml_filename = ( getOpenFileName caption:"Open SceneXml..." types:"SceneXml File (*.xml)|*.xml|All Files (*.*)|*.*" )
		
	-- Export IPL File from SceneXml File
	local ipl_filename = ( getSaveFileName caption:"Save IPL As..." types:"IPL File (*.ipl)|*.ipl|All Files (*.*)|*.*" )
	local ss = stringStream ""
	
	local exeFilename = stringStream ""
	format "%/MapExportIDEIPL.exe" binDir to:exeFilename
	
	local arguments = stringStream ""
	format "--nopopups --build % --ipl% % --single %" builddir iplver ipl_filename scenexml_filename to:arguments
	
	gRsUlog.LogMessage ("RsSceneXmlExportIPL - Command: " + (exeFilename as string) + " " + (arguments as string))
	
	RsMakeSurePathExists ipl_filename
	
	local mapExportProcess = RsProcess()
	mapExportProcess.RunProcess (exeFilename as string) (arguments as string) silent:true
	
	if 0!=mapExportProcess.exitCode then
	(
		local logFilename = gRsULog.GetProcessLogDirectory() + "/MapExportIDEIPL." + (mapExportProcess.processID as string) + ".ulog"
		local go_ahead = gRsULog.ValidateLogFileErrors logFilename
		return go_ahead
	)
	
	return (exitCode == 0)
)

--
-- name: RsSceneXmlExportITYPIMAP
-- desc: Export map ITYP and IMAP files from SceneXml file.
--
-- param: filename - SceneXml XML filename (optional).
-- param: log_file - Log file (optional).
-- param: debug    - add --debug switch allowing debugger attach
-- param resourceFiles - Filenames that were generated as part of the export
--
fn RsSceneXmlExportITYPIMAP filename:unsupplied log_file:unsuppled debug:false &resourceFiles: =
(
	local bindir = RsConfigGetBinDir() + "MapExport/"
	local builddir = RsConfigGetExportDir()
	if ( unsupplied == filename ) then
		filename = ( RsMapSceneXmlFilename )
	local streamdir = ( RsConfigGetMapStreamDir() + "maps/" + RsMapName )
	local debugstr = ""
	if ( debug ) then
		debugstr = "--debug"
	local itypFilename = streamdir + "/" + RsMapName + ".ityp"
	local imapFilename = streamdir + "/" + RsMapName + ".imap"
	
	if (resourceFiles != undefined and resourceFiles != unsupplied) then
	(
		if RsMapExportDefinitions then append resourceFiles itypFilename
		if RsMapExportInstances then append resourceFiles imapFilename
	)
	
	RsDeleteFiles ( streamdir + "/*.ityp" )
	RsDeleteFiles ( streamdir + "/*.imap" )
	
	local exeFilename = stringStream ""
	format "%/MapExportIDEIPL.exe" binDir to:exeFilename
	
	local arguments = stringStream ""

	if ( RsMapExportDefinitions and RsMapExportInstances ) then
		format "--nopopups --build % --ityp % --imap % --iplstream % % %" builddir itypFilename imapFilename streamdir debugstr filename to:arguments
	else if ( RsMapExportDefinitions ) then
		format "--nopopups --build % --ityp % % %" builddir itypFilename debugstr filename to:arguments
	else if ( RsMapExportInstances ) then
		format "--nopopups --build % --imap % --iplstream % % %" builddir imapFilename streamdir debugstr filename to:arguments
	
	RsMakeSurePathExists streamdir
	RsMakeSurePathExists itypFilename
	RsMakeSurePathExists imapFilename
	
	gRsUlog.LogMessage ("RsSceneXmlExportITYPIMAP - Command: " + (exeFilename as string) + " " + (arguments as string))
	
	
	local mapExportProcess = RsProcess()
	mapExportProcess.RunProcess (exeFilename as string) (arguments as string) silent:true
	
	local logFilename = gRsULog.GetProcessLogDirectory() + "/MapExportIDEIPL." + (mapExportProcess.processID as string) + ".ulog"
	local go_ahead = gRsULog.ValidateLogFileErrors logFilename
	return go_ahead
)

--
-- name: RsSceneXmlValidate
-- desc: Validate a map file from SceneXml file.
--
-- param: filenames	- SceneXml XML filenames array (optional)
-- param: log_file	- Log file (optional)
-- param: debug		- add --debug switch allowing debugger attach
--
fn RsSceneXmlValidate filenames:unsupplied log_file:unsupplied debug:false = (

	local bindir = RsConfigGetBinDir() + "MapExport/"
	local builddir = RsConfigGetExportDir()
	
	if ( unsupplied == filenames ) then
	(
		filenames = #( RsMapSceneXmlFilename )
	)
	if ( unsupplied == log_file ) then
	(
		log_file = ( RsSceneXmlDefaultLogFile RsMapName "validation" )
	)
	local debugstr = ""
	if ( debug ) then
	(
		debugstr = "--debug" 
	)

	local exeFilename = stringStream ""
	local arguments = stringStream ""
	
	format "%MapExportValidation.exe" bindir to:exeFilename

	format "--nopopups % " (debugstr) to:arguments

	format "-branch \"%\" " gRsBranch.Name to:arguments
	if (RsIsDLCProj()) then
		format "-dlc \"%\" " gRsProject.Name to:arguments
	
	-- filenames as trailing options
	for name in filenames do
	(
		format "% " name to:arguments
	)
	
	gRsUlog.LogMessage ("RsSceneXmlValidate - Command: " + (exeFilename as string) + " " + (arguments as string))

	local mapValidateProcess = RsProcess()

	local mapValidateMsg = ("mapValidateProcess.RunProcess" + (arguments as string))
	gRsUlog.ProfileStart mapValidateMsg
	mapValidateProcess.RunProcess (exeFilename as string) (arguments as string) silent:true
	gRsUlog.ProfileEnd context:mapValidateMsg
			
	if ( mapValidateProcess.exitCode != 0 ) then
	(
		local logFilename = gRsULog.GetProcessLogDirectory() + "/MapExportValidation." + (mapValidateProcess.processID as string) + ".ulog"
		local go_ahead = gRsULog.ValidateLogFileErrors logFilename
		go_ahead
	)
	else
	(
		mapValidateProcess.exitCode == 0
	)
)


--
-- name: RsMapSceneXmlSave
-- desc: Save out the scene xml for each exportable map section 
--
fn RsMapSceneXmlSave = (

	gRsUlog.init "Scene XML" appendToFile:false

	RsP4LogUserLabelStatus()

	gRsMapExportFragments.preExportRename()
	
	local maps = for map in RsMapGetMapContainers() where map.is_exportable() collect map
	
	successfulXmlExport = true
	
	for map in maps while successfulXmlExport do
	(
		RsMapSetupGlobals map

		if (gRsPerforce.readOnlyP4Check #(RsMapSceneXmlFilename) exclusive:true) then 
		(
			successfulXmlExport = RsSceneXmlExport map
		)
		else
		(
			successfulXmlExport = false
		)
	)

	gRsMapExportFragments.postExportRename()
	
	successfulXmlExport
)

-- pipeline/export/maps/scenexml.ms


