--
-- File:: pipeline/export/maps/globals.ms
-- Description:: Map Export Global Variables
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 22 April 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"
filein "rockstar/export/projectconfig.ms"
filein "pipeline/util/p4.ms"
filein "pipeline/export/maps/asset.ms"
filein "pipeline/util/assert.ms"
filein "pipeline/util/scripttimer.ms"
filein "pipeline/export/maps/objects.ms"
filein "pipeline/util/texturemetatag_utils.ms"
filein "pipeline/util/TcsTools.ms"
filein "pipeline/export/AP3Content.ms"
filein "pipeline/helpers/arrays.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------

global RsMapName
global RsMapObjectPrefix
global RsMapPrefixes
global RsMapDir
global RsMapSceneXmlFilename
global RsMapIsGeneric
global RsMapType
global RsMapIsPatch
global RsMapIsContainer
global RsOpenContainers
global RsMapIsPropGroup	-- OBSOLETE
global RsMapExportDefinitions
global RsMapExportInstances
global RsMapContentQueue
global RsMapPedantic		-- Pedantic checks during export?  Defined in maps.xml
global RsMapLevel			-- Which level the current map belongs to
global RsMapLevelDir		-- Level path
global RsMapOverwriteTCS = false	-- Manually overwrite the TCS files that may already exist in perforce
global RsMapRestartConsole = false -- Users can turn this on if they always want their exports to go through..
global RsDontStopForErrors = false

global RsAutomatedExport -- Things like the batch exporter - mainly used as a way to stop any modal dialog boxes coming up as part of export
if (RsAutomatedExport == undefined) do (RsAutomatedExport = false)
global RsAutoSubmitChangelists
if (RsAutoSubmitChangelists == undefined) do (RsAutoSubmitChangelists = true)

-- What the map contains
global RsMapHasIRef 
global RsMapHasXRef
global RsMapHasGeom
global RsMapHasBounds
global RsMapHasAnim

-- Compatibility global for disabling source texture path checks during
-- export (default: true in map.ms).
global RsMapExportTexturePathCheck

-- Map Texture Export Options
global RsMapExportTextureExtension = ".tif"
global RsMapExportTextureExtensionPassthrough = ".dds"
global RsMapExportTintTextureExtension = ".dds"
global RsMapExportTintTextureSuffix = ("_pal" + RsMapExportTintTextureExtension)

global gSupportedTextureTypes = #(".bmp", ".dds", ".tga", ".tif", ".tiff")
global gToleratedModifierTypes = #(Bend, Twist, VertexPaint, Noisemodifier, Taper, MultiRes, FFD_2x2x2, FFD_3x3x3, FFD_4x4x4, FFDBox, FFDCyl, Uvwmap, Edit_Poly, Edit_Mesh, Unwrap_UVW)
global gIgnoredModifierTypes = #(Skin)
global gToleratedSkelBoneTypes = #(Editable_Mesh, Editable_Poly, PolyMeshObject,  Bone, Dummy, Point, Biped_Object,BoneGeometry)

global RsValidMeshClasses = #(Editable_Mesh.classid as string, Editable_Poly.classid as string, PolyMeshObject.classid as string)

global RsValidUVShaderTypes = #(
	"glass_emissive.sps", 
	"glass_emissive_alpha.sps",
	"alpha.sps",
	"cutout.sps",
	"default.sps", 
	"default_noedge.sps",
	"leaves.sps",
	"emissive.sps", 
	"emissive_alpha.sps", 
	"emissivestrong.sps", 
	"emissivestrong_alpha.sps", 
	"normal.sps", 
	"normal_alpha.sps", 
	"normal_cutout.sps",
	"normal_screendooralpha.sps", 
	"water_decal.sps", 
	"water_mesh.sps", 
	"spec.sps",
	"spec_alpha.sps",
	"spec_screendooralpha.sps",
	"ptfx_model.sps",
	-- artists reported ones:
	"normal_decal.sps",
	"normal_spec_decal.sps",
	"emissive_additive_uv_alpha.sps"
	)
global RsValidShadowProxyShaders= 
	#(
		"cutout.sps",
		"cutout_um.sps",
		"trees.sps",
		"cpv_only.sps",
		"trees_shadow_proxy.sps"
	)
global RsAllowedClothCollisionTypes = 
	#(
		Col_Capsule,
		Col_Plane
	)
	
-- A flag that is set when the user cancels a process within the export
-- pipeline. (default = false)
global RsMapExportCancelled = false

global RsXrefCollisionLoading = false	

-- for stated anim PPU flag
global MAX_SUB_MESHES = 16
global MAX_SHADERGROUPS = 48

-- For mesh-tint palettes:
global RsTintPaletteNullColour = (color 255 127 255)

-- Processes the GTXD dependencies; if true, textures will be omitted from the exported assets if they are promoted to the GTXD.
-- Set this to false if the pipeline is intended to omit this promoted textures.
global RsProcessGTXDDependencies = not (RsProjectConfig.GetBoolParameter "Enable Map GTXD Pipeline Process")

--Enable this to have the map exporting pipeline to attempt to rebuild the GTXD on every map export.
global RsEnableMapGTXDExport = false

-- Store map name and their associated .max or ,maxc file.
global RsMapLookup = undefined

-- Flag to see if we should be generating TCS files. RDR3 doesn't require .tcs files to be generated if they are using a material preset template.
global RsGenerateTCSFiles = (RsProjectConfig.GetBoolParameter "Enable Max Map TCS Generation")
if (RsGenerateTCSFiles == undefined ) then RsGenerateTCSFiles = true

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsMapResetGlobals
-- desc: Reset map exporter global variables.
--
fn RsMapResetGlobals map = (

	RsMapDir = ""
	RsMapName = ""
	RsMapObjectPrefix = ""
	RsMapPrefixes = #()
	RsMapSceneXmlFilename = ""
	RsMapIsGeneric = false
	RsMapType = #none
	RsMapIsPatch = false
	RsMapIsContainer = false
	RsMapIsPropGroup = false
	RsMapExportDefinitions = false
	RsMapExportInstances = false
	RsMapPedantic = false
	RsMapLevel = ""
	RsMapLevelDir = ""
	
	RsMapHasIRef = false
	RsMapHasXRef = false
	RsMapHasGeom = false
	RsMapHasBounds = false
	RsMapHasAnim = false
	RsMapHasMetaInfo = false

	RsMapExportCancelled = false
	RsDontStopForErrors = false
	
	OK
)

fn RsGetRelativeMapDir MapName:RsMapName = 
(
	local SourceNode = (RsContentTree.FindSourceFilenodeFromBasename MapName)
	
	if (SourceNode == undefined) do (return "")
	
	-- Get export-node for map:
	local ExportZipNode = (RsContentTree.GetMapExportZipNode SourceNode)
	
	if (ExportZipNode == undefined) do (return "")
	
	-- Find relative-path from zipfile to export-root:
	local ExportRoot = (RsConfigGetExportLevelsDir() + RsConfigGetProjectName() + "/")
	local MapPath = (ExportZipNode.Directory + "/" + ExportZipNode.BaseName + "/")
	
	local RelPath = pathConfig.convertPathToRelativeTo MapPath ExportRoot
	
	-- Hacky as fuck!  But that's the way it was before so just doing that for now too...
	if (not RsIsDLCProj()) then 
	(
		-- Strip './' from start of path:
		RelPath = (SubString RelPath 3 -1)
	)
	else
	(
		-- Strip '../gta5/' from start of path:
		RelPath = (SubString RelPath 9 -1)
	)
	
	return (RsMakeSafeSlashes RelPath)
)

fn RsAddMapReferences maps =
(
	RsMapLookup = dotnetobject "RSG.MaxUtils.MaxDictionary"
	for map in maps do ( RsMapLookup.Add (tolower map.name) map.filename )
)

fn RsAddMapReference map =
(
	if (RsMapLookup == undefined) then
		RsMapLookup = dotnetobject "RSG.MaxUtils.MaxDictionary"
	
	if not RsMapLookup.ContainsKey (tolower map.name) then
		RsMapLookup.Add (tolower map.name) map.filename
)

fn RsGetMapSourceFile mapName =
(
	if RsMapLookup == undefined then
	(
		gRsULog.LogError "RsMapLookup is undefined"
		undefined
	)
	else
	(
		if (RsMapLookup.ContainsKey mapName) then
		(
			RsMapLookup.Item (tolower mapName)
		)
		else
		(
			gRsUlog.LogError ("Could not find source .max/.maxc file for map (" + mapName + ")")
		)
	)
)

--
-- name: RsMapResetCrossMapGlobals
-- desc: Reset map exporter global variables that are used across map containers.
--
fn RsMapResetCrossMapGlobals = (

	RsMapContentQueue = dotnetobject "RSG.MaxUtils.MaxDictionary"
)

--
-- name: RsMapAddContent
-- desc: Add content type for map resourcing queue.
--
fn RsMapAddContent mapname content = (
	
	if ( not RsMapContentQueue.ContainsKey mapname ) then
	(
		RsMapContentQueue.Add mapname #()
	)
	
	local data = ( RsMapContentQueue.Item mapname )
	append data (content as String)
	RsMapContentQueue.Remove mapname
	RsMapContentQueue.Add mapname data
)

--
-- name: RsMapLoadMapPrefixes
-- desc: Load map prefixes from maps.xml
--
fn RsMapLoadMapPrefixes = (

	mapArray = RsContentTree.contentTreeHelper.GetAllMapContainerMaxNodes()
	
	for i = 1 to mapArray.Count do
	(
		local prefix = RsContentTree.GetExportPrefix mapArray[i]
		if (prefix != undefined and prefix != "") then
		(
			append RsMapPrefixes prefix
		)
	)
)

--
-- name: RsMapCheckForOldPrefix
-- desc: Checks a name (which has previously been found to not
--	have the correct prefix for this map) and see if there
--	are any other prefixes attached and return a new name with 
-- 	them removed.
--
fn RsMapCheckForOldPrefix nameToCheck = (

	newName = nameToCheck

	-- Assuming that there won't be a need for recursion as there shouldn't
	-- be anything with more than one prefix due to constraints previously
	-- e.g. DT1_02_DT1_02 shouldn't have been possible to exist
	for prefix in RsMapPrefixes do 
	(
		-- If the name contains an old prefix then remove it from the name
		prefixPattern = "*" + prefix + "_*"
		if ( matchPattern newName pattern:prefixPattern ) do
		(		
			tempPrefix = prefix + "_"
			newName = substituteString newName tempPrefix ""
		)
	)
	newName
)

fn RsObjectNamespacePass o dummyPass:false prefixChangeList:#() &mapMatNames:#() = 
(
	local suspectMergedObjects = #()
	local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
	local idxTXD = ( GetAttrIndex "Gta Object" "TXD" )
	local prefixPattern = RsMapObjectPrefix + "*"

	case of 
	(
		(
			(o.parent != undefined) and 
			not (isKindOf o.parent Gta_MILO) and 
			not (isKindOf o.parent GtaMloRoom) and 
			not (isKindOf o.parent Container) 
		):
		(
			if not dummyPass do 
			(
				 gRsUlog.LogMessage ("RsObjectNamespacePass - Skipped: " + o.name)
			)
		)
		((isKindOf o Dummy) or (isRefObj o)):
		(

		)
		Default:
		(
			-- Check all materials here so that every material name is prefixed if needed 
			-- so that collision materials etc are prefixed too.
			if (o.mat != undefined) do 
			(
				local matName = o.mat.name
				
				if ( not (matchPattern matName pattern:prefixPattern ignoreCase:true ) ) then
				(
					local newMatName = (RsMapObjectPrefix + "_" + (RsMapCheckForOldPrefix matName))
					
					-- Check to see if the name we would change this mat to, already exists (which means MAX
					-- does bad things and just drops one).
					if ( findItem mapMatNames newMatName == 0 ) then
					(
						if (dummyPass == false) then (					
							local ss = stringstream ""
							format "RsMapNamespacePass: Set object %'s material name from % to %\n" o.name matName newMatName to:ss
							gRsUlog.LogMessage ss
							o.mat.name = newMatName
						)
						else 
						(
							append prefixChangeList ("Material Name: " + matName + " will get it's name changed to: " + newMatName)
						)
					)
					else
					(
						append prefixChangeList ("Material Name: " + matName + " WILL NOT GET IT'S NAME CHANGED AS IT WOULD CONFLICT WITH " + newMatName)						
					)
				)
			)
			
			if 
			(
				(( GetAttrClass o ) == "Gta Object") and 
				(( GetAttr o idxDontExport) != True)
			) 
			do
			(
				-- Merged object check
				-- Currently disabled as its slow.  DHM.
				--for prefix in RsMapPrefixes do
				--(
				--	local prefixPattern = prefix + "*"
				--	format "Check: %\n" prefixPattern
				--	if ( matchPattern o.name pattern:prefixPattern ignoreCase:true ) then
				--		append suspectMergedObjects o
				--)
					
				-- Otherwise lets check the object's name.
				if ( not ( matchPattern o.name pattern:prefixPattern ignoreCase:true ) ) do 
				(					
					local newObjName =( RsMapObjectPrefix + "_" + (RsMapCheckForOldPrefix o.name) )		
					if (dummyPass == false) then  
					(
						-- Set new object name.			 
						local ss = stringstream ""
						format "RsMapNamespacePass: Set object % name to %\n" o.name newObjName to:ss
						gRsUlog.LogMessage ss
						o.name = newObjName
					)
					else 
					(
						append prefixChangeList ("Object: " + o.name + " will get prefixed and become: " + newObjName)
					)
				)
				
				local txdname = ( GetAttr o idxTXD )
				if 
				(
					( not ( matchPattern txdname pattern:prefixPattern ignoreCase:true ) ) and
					( "CHANGEME" != txdname ) 
				) 
				do 
				(
					local newTXDName = ( RsMapObjectPrefix + "_" + (RsMapCheckForOldPrefix txdname) )
					if (dummyPass == false) then 
					(
						-- Set object TXD name
						local ss = stringstream ""
						format "RsMapNamespacePass: Set object % TXD to %\n" o.name newTXDName to:ss
						gRsUlog.LogMessage ss
						SetAttr o idxTXD newTXDName
					)
					else
					(
						append prefixChangeList ("Object: " + o.name + " will get it's TXD (" + txdname + ") prefixed and become: " + newTXDName)
					)
				)
			)
		)
	)
)

fn recurseChildren obj dummyPass:false prefixChangeList:#() &mapMatNames:#() =
(
	if (dummyPass == true) then (
	
		RsObjectNamespacePass obj dummyPass:dummyPass prefixChangeList:prefixChangeList mapMatNames:&mapMatNames
	)
	else (
		
		RsObjectNamespacePass obj mapMatNames:&mapMatNames
	)
	if ( Gta_MILO == classof obj ) or ( GtaMloRoom == classof obj ) then (
	
		for o in obj.children do recurseChildren o
	)
)

--
--fn:	RsMapGetMapMatNames
--desc:	Recurses through all objects and children in a map section to get the
--	mat names for use later in prefix checks
--
fn RsMapGetMapMatNames o &mapMatNames = (

	if ( o.mat != undefined ) do
	(
		append mapMatNames o.mat.name
	)
	for c in o.children do 
	(
		RsMapGetMapMatNames c &mapMatNames
	)	
)

--
-- name: RsMapNamespacePass
-- desc: Enforce max object namespace criteria.  This function needs to
--       be called VERY VERY early in the export process.
--
fn RsMapNamespacePass map dummyPass:false prefixChangeList:#() = (

	RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."

	if (dummyPass == true) do (

		append prefixChangeList ("Map Prefix is: " + RsMapObjectPrefix)
	)	
	
	RsMapLoadMapPrefixes()
	
	gRsUlog.ProfileStart "RsMapNamespacePass"
	local timer = ScriptTimer()
	timer.start()
	
	-- Collects the material names for every object with one, in this map section
	mapMatNames = #() 
	for o in map.objects do
	(
		RsMapGetMapMatNames o &mapMatNames
	)
	mapMatNames = makeUniqueArray mapMatNames
	
	-- Process our object list.  The logic in this loop will need
	-- to change should we want to enforce non-Gta Object naming
	-- conventions.
	for o in map.objects do
	(
		recurseChildren o dummyPass:dummyPass prefixChangeList:prefixChangeList mapMatNames:&mapMatNames
	)
	
	-- Put this in a dialog/rollout.
	--format "Suspected Merged Objects:\n"
	--for o in suspectMergedObjects do
	--	format "\t%\n" o.name
	--print suspectMergedObjects
	
	local msg = ""
	msg = if (dummyPass == false) then ("Map namespace verify") else ("Map dummy namespace pass")
	gRsUlog.LogMessage msg
	gRsUlog.ProfileEnd context:"RsMapNamespacePass"
	true
)


--
-- name: RsMapFindLevelForMapRpf
-- desc: Find a level content node for a Map Archive (maprpf/mapzip) node (by walking up content tree)
--
fn RsMapFindLevelForMapArchive maparchive = (

	RsAssert ( ( ContentMapRpf == classof maparchive ) or ( ContentMapZip == classof maparchive ) ) message:"Invalid ContentMapRpf node!"
	
	local levelObj = undefined
	local tempParent = maparchive.parent
	
	if ( "ContentLevel" != (classof tempParent as string) ) then
	(
		while ( ( tempParent != undefined ) and
				"ContentLevel" != (classof tempParent as string) ) do
		(
			--print (classof tempParent)
			tempParent = tempParent.parent
		)
	)
	if ( "ContentLevel" == (classof tempParent as string) ) then
	(
		levelObj = tempParent
	)
	levelObj
)

--
-- name: RsMapLoadGlobalsFromContentTree
-- desc: Load map exporter global variables from rageMaxConfigParser content
--       functions.
--
fn RsMapLoadGlobalsFromContentTree map = (

	if (RsContentTree == undefined) then
		return ( false )
	
	-- AJM: With batch exports/network exports, the map struct sometimes didn't have the filename set.  So 
	-- try and set it using the base name.
	if ( map.filename == undefined or map.filename == "" ) do
	(
		gRsULog.LogWarning "map.filename is not known in RsMapLoadGlobalsFromContentTree, trying to get it from the basename!"
		map.filename = RsContentTree.FindSourceFilenameFromBasename map.name
	)
	
	local contentNode = RsContentTree.GetContentNode map.filename
	if (contentNode == undefined) then
	(
		gRsUlog.LogError ("Could not find content node for map filename (" + (map.filename as string)+ ")")
		return ( false )
	)
	
	local mapExportType = RsContentTree.GetMapType contentNode
	
	local exportNode = RsContentTree.GetMapExportZipNode contentNode
	if (exportNode == undefined) then
	(
		gRsUlog.LogError ("Could not find export zip node for map filename (" + (map.filename as string)+ ")")
		return ( false )
	)
	
	local sceneXMLNode = RsContentTree.GetMapExportSceneXMLNode exportNode
	if (sceneXMLNode == undefined) then
	(
		gRsUlog.LogError ("Could not find scene xml for map filename (" + (map.filename as string)+ ")" )
		return ( false )
	)
	
	RsMapExportDefinitions = RsContentTree.GetExportArchetypes contentNode
	RsMapExportInstances = RsContentTree.GetExportEntities contentNode
	RsMapIsGeneric = (not RsMapExportInstances)
	RsMapObjectPrefix = RsContentTree.GetExportPrefix contentNode
	RsMapPedantic = true --TL TODO - Not sure if we want to add this to the content.xml file for AP3, doesn't currently exist
	RsMapSceneXmlFilename = sceneXMLNode.AbsolutePath
	
	/* TL TODO - These were setup but don't really seem to be used anymore. Going to be a pain with AP3.
	RsMapLevelObj = ( RsMapFindLevelForMapArchive mapArchiveNode )
	RsMapLevel = RsMapLevelObj.name
	RsMapDir = mapArchiveNode.path
	RsMapLevelDir = RsMapLevelObj.path
	*/
		
	-- Initialise RsMapType constant.
	case (tolower mapExportType) of
	(
	"map_container":
		RsMapType = #normal
	"lod_container":
		RsMapType = #normal
	"rejection_container":
		RsMapType = #normal
	"interior":
		RsMapType = #interior
	"props":
		RsMapType = #prop
	"occl_container":
		RsMapType = #normal
	default:
		(
			RsAssert false message:"Unexpected map type!"
			RsMapType = #normal
		)
	)
	
	true
)


--
-- name: RsMapSetupGlobals
-- desc: Setup map exporter global variables.  If a Container is specified
--       then that will be exported as a block (RsMapIsContainer set to true), 
--       otherwise the entire file is exported (RsMapIsContainer set to false).
--
fn RsMapSetupGlobals map contWarning:true preserveContent:false = 
(

	local mapsetglobalsmsg = "Map Setup Globals"
	gRsUlog.ProfileStart mapsetglobalsmsg
	
	

	RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."
	RsMapResetGlobals map

	RsMapName = if (map.name == undefined) then "" else (toLower map.name)

	if not preserveContent then
	(
		if ( undefined == RsMapContentQueue ) then
		(
			RsMapContentQueue = dotnetobject "RSG.MaxUtils.MaxDictionary"
			RsMapContentQueue.Add RsMapName #()
		)
		else if ( RsMapContentQueue.ContainsKey RsMapName ) then
		(
			RsMapContentQueue.Remove RsMapName
			RsMapContentQueue.Add RsMapName #()
		)
		else
		(	
			RsMapContentQueue.Add RsMapName #()
		)
	)
	else
	(
		-- Even if we are set to preserve the content, make sure the dictionary is at least there.
		if ( undefined == RsMapContentQueue ) then
		(
			RsMapContentQueue = dotnetobject "RSG.MaxUtils.MaxDictionary"
			RsMapContentQueue.Add RsMapName #()
		)
	)
	RsMapPreviewQueue = #()
	
	RsMapIsContainer = false
	RsMapIsPropGroup = false
	
	case of
	(
		( map.is_container() ):
		(		
			RsMapIsContainer = true
			-- AJM: RsMapLoadGlobalsFromContentTree below requires the filename in the RsMapContainer struct
			-- otherwise it errors since it can't search for it in the content tree.
			if (map.filename == undefined or map.filename == "") do
			(
				if ( RsMapName != "" ) then
				(
					map.filename = RsContentTree.FindSourceFilenameFromBasename RsMapName
				)	
				else
				(
					gRsUlog.LogError ("RsMapSetupGlobals doesn't have RsMapName setup and we need it to get the maxc filename!")
					return false
				)
			)
		)
		( map.is_propgroup() ):
		(
			RsMapIsPropGroup = true
		)
	)
	


	local globalsFromCntTree = "Loading Globals from content tree"
	gRsUlog.ProfileStart globalsFromCntTree
	if ( not RsMapLoadGlobalsFromContentTree map ) do 
	(
		if contWarning do 
		(
			local mapxmlpath = RsConfigGetContentDir() + "maps.xml"
			local outputxmlpath = RsConfigGetContentDir() + "output.xml"
			local msg = stringStream ""
			
			local fileMsg = "\n\nHave you opened the correct file? e.g.\n\tX:/gta5/art/Models/...\n\t\tvs.\n\tX:/gta5/art/ng/models/...\n"

			if ( RsMapIsContainer ) then
				format "The % Container does not exist or has an incorrect entry in the content tree.\n\nHave you added it to the following files?\n\n\t%\n\t%%" RsMapName mapxmlpath outputxmlpath fileMsg to:msg
			else if ( RsMapIsPropGroup ) then
				format "The % group does not exist or has an incorrect entry in the content tree.\n\nHave you added it to the following files?\n\n\t%\n\t%%" RsMapName mapxmlpath outputxmlpath fileMsg to:msg
			else 
				format "The % map does not exist or has an incorrect entry in the content tree.\n\nHave you added it to the following files?\n\n\t%\n\t%%" RsMapName mapxmlpath outputxmlpath fileMsg to:msg

			gRsUlog.LogError msg
		)
		return false
	)
	gRsUlog.ProfileEnd context:globalsFromCntTree
	gRsUlog.ProfileEnd context:mapsetglobalsmsg
	true
)

--
-- name: RsMapProgress
-- desc: Function to cancel out of map-export processes, while updating the progressbar.
fn RsMapProgress increment totalCount = 
(
	if RsMapExportCancelled then true else 
	(
		RsMapExportCancelled = not (progressUpdate (100.0 * increment / totalCount))
		not RsMapExportCancelled
	)
)

--
-- name: RsMapGenerateTcl
-- desc: Generate the tcl file for a texture to link to tcs file in perforce
--	
fn RsMapGenerateTcl texmapnamestrip tcsFile = (

	tcloutputfile = (RsConfigGetStreamDir())+ "/maps/tcls/" + texmapnamestrip + ".tcl"
	tcsreference = (RsMakeSafeSlashes (RsRemoveExtension tcsFile))
	tcsreference = substituteString tcsreference (RsConfigGetAssetsDir()) "$(assets)/"
	RsGenerateTemplateReferenceFile tcloutputfile tcsreference
)

-- Struct used to pass texture-export data to RsExportMapTextures:
--
-- DHM: resizeExempt is obsolete.  No longer resize in rexMax.
--
struct RsExportMapTexInfo 
(
	tcsDir, texmap, texMapFilename, outputFile, resizeExempt = false, texmapType,
	textureTemplate, templateConfig = (::RsConfigGetEtcDir() + "texture/maptemplates.xml"),
	isBump = false,
	tcsFile = undefined,
	
	-- desc: Returns the tcs filename based on the outputFile (dds) name and the type of the 
	--		 template it is using - Distance Map, CableMap and Tint Palettes are resize exempt
	--		 and go to a different location. 
	fn setTcsFileName = 
	(
		tcsFile = ""
		
		local outputnamestrip = RsStripTexturePath outputFile
		outputnamestrip = (filterstring outputnamestrip ".")[1]
		
		if (not resizeExempt) then
		(			
			local mapPathFile = tcsDir + (RsGetRelativeMapDir MapName:RsMapName) + outputnamestrip + ".tcs"
			if RsFileExists mapPathFile then
				tcsFile = mapPathFile
			else
				tcsFile = (tcsDir + outputnamestrip + ".tcs")	
		)
		else
		(
			tcsFile = RsConfigGetAssetsDir() + "metadata/textures/maps_other/"
			
			case of
			(
				-- The CableMap is done using matchPattern as to find
				-- out if it is a CableMap requires the material which we don't have at this stage.
				(matchPattern textureTemplate pattern:"*Cable*"):
				(
					tcsFile += "cable_map/"
				)
				(RsIsDistanceMap texmaptype):
				(
					tcsFile += "distance_map/"
				)
				(RsIsTintPalette texmaptype):
				(
					tcsFile += "tint_pal/"
				)
				default:
				(
					gRsUlog.LogWarning ("Texture is marked as resize-exempt but its type cannot be determined: " + (outputnamestrip as string) + ".  Skipping")
				)
			)
			
			local mapPathFile = tcsDir + (RsGetRelativeMapDir MapName:RsMapName) + outputnamestrip + ".tcs"
			if RsFileExists mapPathFile then
				tcsFile = mapPathFile
			else
				tcsFile += outputnamestrip + ".tcs"
		)
		
		return tcsFile
	)
)

--
-- name: RsExportMapTexture
-- desc: Wrapper function for texture exporting only currently used by maps so that they get checked out
-- of source control etc along with their metadata
fn RsExportMapTexture map exportInfo forceSetting:RsSettingsRoll.chkForceTexture.checked = 
(
	local tcsDir = exportInfo.tcsDir 
	local texMapFilename = exportInfo.texMapFilename
	local outputFile = exportInfo.outputFile
	local texmaptype = exportInfo.texmaptype
	
	local texPathList = filterstring texMapFilename "+>"
	
	local reexporting = false
	if forceSetting or (not RsFileExist outputFile) do 
	(
		reexporting = true
	)

	-- Do re-export if source-files are newer than output-file:
	if not reexporting do 
	(	
		local outputDate = RsFileModDate outputFile

		for texPath in texPathList do 
		(
			local compareDate = RsFileModDate texPath
			
			local dateTimeClass = dotNetClass "System.DateTime"
			local comp = ( dateTimeClass.Compare compareDate outputDate )
			if (comp > 0) then 
			(
				reexporting = true
			)
		)
	)
	
	-- Actual texture export
	local isTint = RsIsTintPalette texmaptype
	
	if reexporting or isTint then
	(				
		local exportSuccess = true
		
		RsMakeSurePathExists outputFile
		
		if isTint then 
		(
			local extension = getFilenameType texMapFilename
			RSAssert (extension == RsMapExportTintTextureExtension) message:("Tint palette texture output not set to " + RsMapExportTintTextureExtension)
			
			-- Error if source tint pal doesn't exist (auto-gen one should've been created by now too)
			if (not doesFileExist texMapFilename) then
			(
				exportSuccess = false
				gRsUlog.LogError ("Tint palette source file does not exist - " + texMapFilename)
			)
			else
			(
				if (doesFileExist outputFile and (getFileAttribute outputFile #readOnly == true)) do
				(
					gRsUlog.LogWarning (outputFile + " not set to writable, marking writable now (it should've been marked for edit by now).")
					setFileAttribute outputFile #readOnly false
				)
				-- Tint palettes are always force exported, exported as 32-bit ARGB DDS files.
				local oldVal = rexGetForceTextureExport()
				rexSetForceTextureExport true
				exportSuccess = RsRexExportTexture texMapFilename outputFile asTiff:false
				rexSetForceTextureExport oldVal				
			)
		)
		else
		(
			-- Other textures exported as LZW compressed TIFFs; unless their input is DDS
			-- (this allows pre-compressed or optimised DDS files to pass through).
			local extension = getFilenameType outputFile
			case extension of
			(
				RsMapExportTextureExtensionPassthrough: 
				(
					gRsUlog.LogMessage ("Exporting " + (texMapFilename as string) + " to "+ (outputFile as string) + " as DDS")
					exportSuccess = RsRexExportTexture texMapFilename outputFile asTiff:false
				)
				default:
				(
					gRsUlog.LogMessage ("Exporting " + (texMapFilename as string) + " to "+ (outputFile as string) + " as TIFF")
					exportSuccess = RsRexExportTexture texMapFilename outputFile asTiff:true
				)
			)
		)

		if ( not exportSuccess ) do
		(
			local logMessage = stringStream ""
			format "Failed to export texture % to % . Check RS Utils > RS Info > Texture Stats to find objects that use the texture." texMapFilename outputFile to:logMessage
			gRsUlog.LogError (logMessage as String)
		)	
	)
	
	local outputTextureExists = RsFileExists outputFile
	if (outputTextureExists == false) do 
	(
		gRsUlog.LogWarning ("Texture wasn't exported/doesn't exist: " + (outputFile as string))
		return false
	)
	
	local texmapnamestrip = RsStripTexturePath texMapFilename
	texmapnamestrip = (filterstring texmapnamestrip ".")[1]	
	
	-- Always output the tcl files	
	RsMapGenerateTcl texmapnamestrip exportInfo.tcsFile
	
	if (RsGenerateTCSFiles == true) then
	(
		local tcsHelper = dotnetclass "RSG.MaxUtils.Textures.TCSHelper" 
		local tcsFileExistsLocally = RsFileExist exportInfo.tcsFile
		local tcsFileHasResourceTexturesElement = tcsFileExistsLocally and (tcsHelper.HasResourceTexturesElement exportInfo.tcsFile)

		if rexGetForceTextureExport() or RsMapOverwriteTCS or (not tcsFileExistsLocally) then 
		(
			local tcstemplate = ""
			
			-- New system for accessing the texture templates
			local texturetemplate = exportInfo.texturetemplate
			if (texturetemplate != "") and (texturetemplate != undefined) then 
			(			
					 
				if ( not exportInfo.resizeExempt ) then
				(
					if findString texturetemplate "/" == undefined then 
					(
						tcstemplate = RsConfigGetTCPMapsRoot() + texturetemplate
					)
					-- If our path is relative it needs to be applied to the root
					else 
					(
						tcstemplate = RsConfigGetTCPRoot() + texturetemplate
					)
				)
				-- Exempt texture templates are in a different folder
				else
				(
					-- Tint palettes and distance maps come through with texturetemplate already prefixed with 'maps_other/'
					if ( RsIsTintPalette texmaptype or RsIsDistanceMap texmaptype ) then
					(
						tcstemplate = RsConfigGetTCPRoot() + texturetemplate
					)
					else
					(
						tcstemplate = RsConfigGetTCPMapsOtherRoot() + texturetemplate
					)
				)
				
				tcstemplate = substituteString tcstemplate (RsConfigGetAssetsDir()) "$(core_assets)/"
			)
			else 
			(
				gRsULog.LogError ("No template found for " + (texMapFilename as string) + " of type " + (texmaptype as string))
			)
			--Munson - Passing in useSCC false, as I'm confident enough that the sync, and the add or edit that's done in RsExportMapTextures which calls this
			-- function (RsExportMapTextures), has already sorted out the source control stuff for TCS files!
			RsGenerateTemplateReferenceFile exportInfo.tcsFile tcstemplate sourceTextures:#(texMapFilename) exportTexture:outputFile useSCC:false
		)
		else
		(
			-- DHM FIX ME: this is kinda temporary as we transition from DDS to TIFF.
			-- Tints are being left as DDS files (for now at least).
			if ( not isTint ) do RsUpdateTemplateReferenceFile exportInfo.tcsFile exportTexture:outputFile
		)

		gTcsTools.CheckTcsFormat exportInfo.tcsFile texMapFilename outputFile
	)

	return true
)

-- Exports textures, dealing with Perforce as necessary:
fn RsExportMapTextures map exportInfoList materialPresetList = 
(
	RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."
	
	local textureOutputFiles =  for item in exportInfoList collect item.outputFile
	local textureTemplates =  for item in exportInfoList collect item.textureTemplate
	local templateConfigs =  for item in exportInfoList collect item.templateConfig
	local texmapTypes =  for item in exportInfoList collect item.texmapType

	pushPrompt "Getting TCS filenames"
	local tcsFileList = for item in exportInfoList collect 
	(
		local tcsFile = item.setTcsFileName()
		tcsFile
	)
	popPrompt()
	
	-- sync the dds filenames before we check for timestamps and decide if we need to export
	gRsPerforce.sync textureOutputFiles silent:true
	
	local addQueue = #()
	-- Add or edit the texture output filenames with binary+m type
	gRsPerforce.add_or_edit textureOutputFiles switches:#("-t", "binary+m") queue:addQueue silent:true

	local finalPresetList = #()
	
	if (materialPresetList.count != 0) then
	(
		for materialPresetTemp in materialPresetList do
		(
			if (materialPresetTemp != undefined) then appendIfUnique finalPresetList materialPresetTemp
		)
		
		-- Add material template files to changelist
		gRsPerforce.add_or_edit finalPresetList queue:addQueue silent:true
	)	

	local outputFiles = #()
	
	if (RsGenerateTCSFiles == true) then
	(
		outputFiles = textureOutputFiles + tcsFileList + finalPresetList

		if (outputFiles.count == 0) do return true

		-- Although there is a sync to the tcs dir at start, this will force sync files if they
		-- don't exist locally after a sync, to get around situation where someone may have 
		-- manually deleted a file rather than removed it from perforce workspace.
		RsP4SyncWithForceIfRequired tcsFileList silent:true	
		
		-- Add or edit the tcs filenames as text
		gRsPerforce.add_or_edit tcsFileList switches:#("-t", "text") queue:addQueue silent:true
	)
	else
	(
		-- Only deal with textures and presets if we aren't generating .tcs files.
		outputFiles = textureOutputFiles + finalPresetList
		if (outputFiles.count == 0) do return true
	)
		
	-- Add existing files to map changelist:
	gRsPerforce.addToChangelist map.changelist outputFiles
	
	progressStart ("Exporting texture maps:")
	gRsUlog.LogMessage "\nExporting texture maps...\n"
	local itemCount = exportInfoList.count
	for i = 1 to itemCount while (RsMapProgress i itemCount) do 
	(		
		RsExportMapTexture map exportInfoList[i]
	)
	gRsUlog.LogMessage "Texture map export completed\n\n"
	progressEnd()
		
	if not RsMapExportCancelled do 
	(
		-- Add newly-created files, if they now exist:
		gRsPerforce.postExportAdd cancel:RsMapExportCancelled queue:addQueue silent:true
		
		-- Add newly-added files to changelist:
		gRsPerforce.addToChangelist map.changelist outputFiles
				
		-- Add missing sourcefiles to default changelist, if found:
		local sourceFilenames = #()
		
		-- Make sure to get the source filenames for alpha shaders etc which are joined together
		for item in exportInfoList do
		(
			sourceFiles = filterString item.texMapFilename "+>"
			join sourceFilenames sourceFiles
		)
		
		local existingP4FileNums = gRsPerforce.exists sourceFilenames
		local addFiles = for n = -existingP4FileNums where (doesFileExist sourceFilenames[n]) collect sourceFilenames[n]
		if (addFiles.count != 0) do 
		(
			gRsPerforce.add addFiles
			gRsPerforce.addToChangelist map.changelist addFiles
		)
	)
	
	return (not RsMapExportCancelled)
)

--Rebuilds the GTXD asset file.
fn RsExportMapGTXD = 
(
	local gtxdFile = RsConfigGetExportDir() + "/levels/" + RsConfigGetProjectName() + "/generic/gtxd.zip"
	gRsPerforce.sync gtxdFile
	
	local gtxdTCSFiles = RsConfigGetMetadataDir() + "/textures/maps/gtxd/..."
	gRsPerforce.sync gtxdTCSFiles
	
	local args = #("--nopopups")
	build_success = ( 0 == ( AP3Invoke.Convert #(gtxdFile) args:args ) )
)

fn GetToleratedSkelBoneTypeClassIdString = 
(
	local toleratedSkelBoneTypeClassIds = for c in gToleratedSkelBoneTypes collect (c.classid[1] as string+","+c.classid[2] as string)
	local classIdString = ""
	for tcid in toleratedSkelBoneTypeClassIds do
	(
		if classIdString!="" then
			append classIdString "|"
		append classIdString tcid
	)
	return classIdString
)


fn RsToggleSelectionLock = (
	-- It's either this or "max spacebar" and that will fail if someone has reassigned the spacebar to another command.
	actionMan.executeAction 0 "59231"
)

fn RsIsSelectionLocked = (
	local locked = false
	-- Backup selection
	tmpSelection = selection as array
	clearSelection()
	if (selection.count != 0) then (
		-- If it's not empty after clearing then its locked 
		locked = true
	) else (
		-- otherwise theres nothing selected or it's not locked.
		locked = false
	)
	select tmpSelection
	locked
)

fn RsLockSelection = (
	if not (RsIsSelectionLocked()) then RsToggleSelectionLock()
)

fn RsUnlockSelection = (
	if (RsIsSelectionLocked()) then RsToggleSelectionLock()	
)

fn RsGetAnimHandleString objs = 
(
	RsArrayJoin (for o in objs where undefined!=o collect (GetHandleByAnim o)) token:"|"
)

fn RsStrtoB str = (
	if (stricmp str "true" == 0) then true else if (stricmp str "false" == 0) do false
)
-- pipeline/export/maps/globals.ms
