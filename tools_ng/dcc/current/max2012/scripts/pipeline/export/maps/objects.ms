--
-- File:: pipeline/export/maps/objects.ms
-- Description:: Map object test functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 23 July 2009
--
filein "pipeline/util/RsuniversalLog.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global idxExportGeometry = ( GetAttrIndex "Gta Object" "Export Geometry" )
global idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
global idxIsFragment = ( GetAttrIndex "Gta Object" "Is Fragment" )
global idxIsFragProxy = ( GetAttrIndex "Gta Object" "Is FragProxy" )
global idxIsAnimProxy = ( GetAttrIndex "Gta Object" "Is AnimProxy" )
global idxIsDynamic = ( GetAttrIndex "Gta Object" "Is Dynamic" )

-----------------------------------------------------------------------------
-- Structures
-----------------------------------------------------------------------------

--
-- struct: RsMapContainer
-- desc: Hybrid container structure.  See RsMapGetMapContainers.
--
struct RsMapContainer (
	name = "",							-- Name of container.
	cont = undefined,				-- Container object (if applicable).
	propgroup = false,			-- Is Prop Group?
	objects = #(),					-- Array of all objects in container.
	selobjects = #(),				-- Array of selected objects in container.
	exportObjects = #(),		-- Array of objects to export (Dont Export not set)
	changelist = -1,				-- Changelist number for map changes (*never* auto-submitted)
	filename = undefined,		-- Input filename (.max, .maxc)
	
	-- Return true iff this is a Container.
	fn is_container = (
		( ( cont != undefined ) and ( Container == classof cont ) )
	),
	
	-- Return true iff this is a Prop Group.
	fn is_propgroup = (
		( propgroup )
	),
	
	-- Return true iff this map container is exportable (i.e. Container is open)
	fn is_exportable = (
		if ( is_container() ) then
			( cont.isopen() and ( objects.count > 0 ) ) -- Containers only exportable if open.
		else
			true -- Prop Groups, props and interiors etc are always exportable.
	),
	
	-- Return number of objects in container.
	fn count = (
		( objects.count )
	),
	
	-- Return number of selected objects to export.
	fn selcount = (
		( selobjects.count )
	),
	
	fn exportCount = (
		( exportObjects.count )
	)
)

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------


--
-- name: RsMapObjectGetTopLevelOpenContainers
-- desc: Return array of open top-level scene Container objects.
--
-- OBSOLETE 2010/03/24 DHM
--
fn RsMapObjectGetTopLevelOpenContainers = 
(
	local conts = #()
	for o in rootNode.children do
	(
		if ( undefined != o.parent ) then
			continue
		if ( Container != classof o ) then
			continue
		if ( not o.IsOpen() ) then
			continue
	
		append conts o
	)
	
	return conts
)


--
-- name: RsMapObjectGetTopLevelContainerFor
-- desc: Return the top-level container object for the specified object
--       or undefined if there isn't one.
--
fn RsMapObjectGetTopLevelContainerFor o = (
	
	obj = o.parent
	while ( ( undefined != obj ) and ( undefined != obj.parent ) ) do
		obj = obj.parent
		
	if ( Container == classof obj ) then
		return ( obj )
		
	return ( undefined )
)


--
-- name: RsMapObjectGetPropGroups
-- desc: Return array of unique prop group names ("Gta Object", "Prop Group" attribute).
--
fn RsMapObjectGetPropGroups = (

	local attrObjectPropGroupIdx = ( GetAttrIndex "Gta Object" "Prop Group" )
	local attrMiloPropGroupIdx = ( GetAttrIndex "Gta MILOTri" "Prop Group" )
	local attrPropGroupDefault = ( GetAttrDefault "Gta Object" attrObjectPropGroupIdx )

	groups = #()
	for o in rootNode.children do
	(	
		local attrClass = ( GetAttrClass o )
		
		if ( ( "Gta Object" != attrClass ) and 
		     ( "Gta MILOTri" != attrClass ) ) then
			continue
		
		local propGroup = attrPropGroupDefault
		if ( "Gta Object" == attrClass ) then
		(
			propGroup = RsLowercase( GetAttr o attrObjectPropGroupIdx )
		)
		else if ( "Gta MILOTri" == attrClass ) then
		(
			propGroup = RsLowercase( GetAttr o attrMiloPropGroupIdx )
		)
		
		if ( attrPropGroupDefault == propGroup ) then
			continue
		
		if ( 0 == findItem groups propGroup ) then
			append groups propGroup
	)
	
	groups
)


--
-- name: RsMapObjectGetPropGroupObjects
-- desc: Return array of object nodes within a specific group.
--
fn RsMapObjectGetGroupObjects grpname = (

	local attrObjectPropGroupIdx = ( GetAttrIndex "Gta Object" "Prop Group" )
	local attrMiloPropGroupIdx = ( GetAttrIndex "Gta MILOTri" "Prop Group" )
	
	grpname = RsLowercase( grpname )
	group_objects = #()
	for o in rootNode.children do
	(
		local attrClass = ( GetAttrclass o )
		
		if ( "Gta Object" == attrClass ) then
		(		
			if ( grpname == RsLowercase( GetAttr o attrObjectPropGroupIdx ) ) then
				append group_objects o
		)
		else if ( "Gta MILOTri" == attrClass ) then
		(
			if ( grpname == RsLowercase( GetAttr o attrMiloPropGroupIdx ) ) then
				append group_objects o
		)
	)
	
	group_objects
)

--
-- name: RsMapExportObj
-- desc: Checks an object to see if it's flagged as Dont Export
--
fn RsMapExportObj obj = (

	exportObj = true
	objClass = (getattrclass obj)
	if ( objClass != undefined) then
	(
		idxDontExportObj = getattrindex objClass "Dont Export"	
		if ( idxDontExportObj != undefined ) then
		(
			exportObj = not ( getattr obj idxDontExportObj )
		)
	)
	exportObj 
)

fn AppendIfNotDeleted toCollection element =
(
	if ( (isdeleted element) == false ) then
	(
		append toCollection element
	)
)

--
-- name: RsMapGetMapContainers
-- desc: Return an array of RsMapContainer objects to that each object in
--       selset is within a RsMapContainer object (if applicable).
--
-- This supports Container helpers, Prop Groups and also "old-style" maps
-- for interiors and props, and the odd map section which hasn't been
-- brought across yet.
--
fn RsMapGetMapContainers doingExport:false = (

	local containers_map = #() -- Array of RsMapContainer objects
	local conts = #() -- Array of containers, for finding containers_map indexes
	
	local gcontainer = undefined
	
	-- Do a pass on all rootNode children to find all available containers.
	-- Populating their entire objects array.
	for i=1 to rootNode.children.count do
	(
		-- we do the loop this way since this fn is called on load, and when we load huge paths files, the alternative causes a max exception.
		o = rootNode.children[i]
		local cont = undefined 
		if ( Container == classof o ) then
		(
			cont = o
		)
		else
		(
			-- Otherwise try and find parent container.
			cont = ( RsMapObjectGetTopLevelContainerFor o )
		)
		
		if ( undefined != cont ) then
		(
			-- Object is within a 3dsmax 2010 Container Helper
			local idxContainer = ( findItem conts cont )
			
			if ( 0 == idxContainer ) then
			(
				if isSpace cont.name then
				(
					messagebox ("The container name: \"" + cont.name + "\" has whitespace. Please remove it and re-export.")
					return false
				)
				
				exportList = #()
				
				if ( Container == classof o ) then
				(					
					for childObj in cont.children do
					(
						if ( RsMapExportObj childObj ) do
						(
							AppendIfNotDeleted exportList childObj
						)
					)
					
					local filename = cont.localDefinitionFilename
					if filename == "" do
						filename = cont.sourceDefinitionFilename
					
					-- Try and find the source filename to get the content, from the container name
					if filename == "" do
						filename = RsContentTree.FindSourceFilenameFromBasename cont.name
					
					if filename == "" or filename == undefined then
					(
						::gRsULog.LogError("No local or source definition filename found for container, and couldn't find the source filename for " + cont.name)
					)
					else 
					(					
						append containers_map ( RsMapContainer cont.name cont false cont.children #() exportList -1 filename )
						append conts cont
					)
				)
				else
				(
					if ( RsMapExportObj o ) do
						AppendIfNotDeleted exportList o
					
					local filename = cont.localDefinitionFilename
					if filename == "" then
						filename = cont.sourceDefinitionFilename
					
					if filename == undefined then
					(
						::gRsULog.LogError "Unable to export unsaved container.  Please save this container prior to exporting."
					)
					
					append containers_map ( RsMapContainer cont.name cont false #( o ) #() exportList -1 filename )
					append conts cont
				)
			)
			else
			(
				if ( Container != classof o ) then
				(
					AppendIfNotDeleted containers_map[idxContainer].objects o
					if ( RsMapExportObj o ) do
						AppendIfNotDeleted containers_map[idxContainer].exportObjects o
				)
			)
		)
		else
		(
			-- Otherwise we add it to the global container (e.g. non-Container maps, props, interiors).
			if ( undefined == gcontainer ) then
			(
				gcontainer = ( RsMapContainer (RsRemoveExtension maxFileName) undefined false #( o ) #() #() -1 (maxfilepath + maxfilename)  )
			)
			else
			(
				AppendIfNotDeleted gcontainer.objects o
			)
			
			-- See if set to "Dont Export" 
			if ( RsMapExportObj o ) do
			(
				AppendIfNotDeleted gcontainer.exportObjects o
			)
		)
	)
	
	-- Do a pass of our selection to populate our selobjects arrays.
	for o in (getCurrentSelection()) do
	(
		local cont = undefined 
		if ( Container == classof o ) then
		(
			cont = o
		)
		else
		(
			-- Otherwise try and find parent container.
			cont = ( RsMapObjectGetTopLevelContainerFor o )
		)
		
		if ( undefined != cont ) then
		(
			-- Object is within a 3dsmax 2010 Container Helper
			local idxContainer = ( findItem conts cont )
			if ( 0 == idxContainer ) then
			(
				append conts cont
				if ( Container == classof o ) then
					append containers_map ( RsMapContainer cont.name cont false cont.children #() #() -1 cont.sourceDefinitionFilename )
				else
					append containers_map ( RsMapContainer cont.name cont false #( o ) #() #() -1 cont.sourceDefinitionFilename )
			)
			else
			(
				if ( Container != classof o ) then
					AppendIfNotDeleted containers_map[idxContainer].selobjects o
			)
		)
		else
		(

			-- Otherwise we add it to the global container (e.g. non-Container maps, props, interiors).
			if ( undefined == gcontainer ) then
			(
				gcontainer = ( RsMapContainer (RsRemoveExtension maxFileName) undefined false #( o ) #()  #() -1 (maxfilepath + maxfilename)  )
			)
			else
			(
				AppendIfNotDeleted gcontainer.selobjects o
			)
		)
	)	
	
	-- we only append the global container if we have no container groups already.
	maps = ( containers_map )
	if ( ( maps.count > 0 ) and gcontainer != undefined and ( gcontainer.objects.count > 0 ) ) then
	(
		if ( doingExport ) do
		(
			::gRsULog.LogWarning "The scene contains map containers and also root scene geometry.  The root scene geometry will not be exported.  See https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Map_Containers_and_Scene_Geometry for more information."
		)
	)
	
	if ( ( 0 == maps.count ) and ( undefined != gcontainer ) ) then
		append maps gcontainer
	
	maps
)


--
-- name: RsMapObjectGetMapContainerFor
-- desc: Return the RsMapContainer the object belongs to
--
fn RsMapObjectGetMapContainerFor o maps:(RsMapGetMapContainers()) = 
(


	local foundMap
	
	if not isValidNode o then
	(
		gRsUlog.LogError ("Trying to resolve invalid node: "+o as string) context:o
		return undefined
	)
	
	--If there is only one RsMapContainer object and it isn't associated with an actual container then we
	-- we are safe to assume this is the RsMapContainer that we belong to - JWR
	if( foundMap == undefined and maps.count == 1 and maps[1].cont == undefined ) then
	(
		foundMap = maps[1]
	)
	
	local objectParent = o
	while (objectParent != undefined and foundMap == undefined) do
	(
		for map in maps while foundMap == undefined do
		(
			if map.cont != undefined and objectParent == map.cont do
			(
				foundMap = map
			)
		)
		
		objectParent = objectParent.parent
	)
	
	foundMap
)

--
-- name: RSgetMapPrefix
-- desc: Returns the map's prefix-string
--
fn RSgetMapPrefix map = 
(
	if (map != undefined) then 
	(
		RsMapSetupGlobals map contWarning:false
		RsMapObjectPrefix
	)
	else 
	(
		""
	)
)

-----------------------------------------------------------------------------
-- Map Object Test Functions
-----------------------------------------------------------------------------


--
-- name: RsMapObjectIsDontExport
-- desc: Determine whether a node should be exported or not.
--
fn RsMapObjectIsDontExport obj = 
(
	local dontExport = false

	if ( "Gta Object" == ( GetAttrClass obj ) ) then
		dontExport = ( GetAttr obj idxDontExport )

	dontExport
)

--
-- name: RsMapObjectIsGeometry
-- desc: Determine whether a node is a valid geometry object for export.
--
-- note: checks for X/RSrefs, and our "Dont Export" attribute.
--
fn RsMapObjectIsGeometry obj =
(
	case of 
	(
		(undefined == obj ):false
		(::isRefObj obj):false
		(GetAttrClass obj != "Gta Object"):false
		(GetAttr obj idxExportGeometry):true
		default:false
	)
)

--
-- name: RsMapObjectIsFragment
-- desc: Determine whether a node is a valid fragment object for export.
--
fn RsMapObjectIsFragment obj =
(
	(RsMapObjectIsGeometry obj ) and (GetAttr obj idxIsFragment)
)

--
-- name: RsMapObjectIsFragmentProxy
-- desc: Determine whether a node is a valid fragment proxy object.
--
fn RsMapObjectIsFragmentProxy obj =
(
	-- Cannot use IsGeometry because fragment proxies
	-- are set to Dont Export.
	if ( isRefObj obj ) then
		return ( false )
	else if ( "Gta Object" != ( GetAttrClass obj ) ) then
		return ( false )

	-- There is a hidden "Is FragProxy" attribute, which I don't
	-- see getting set by anything but we better check it in case.
	if ( true == ( GetAttr obj idxIsFragProxy ) ) then
		return ( true )

	-- Otherwise we see if there is an object with the same name
	-- and with a "_frag_" suffix.
	local fragmentName = ( obj.name + "_frag_" )
	local fragmentObject = ( getNodeByName fragmentName ignoreCase:true )
	if ( undefined != fragmentObject ) then
		return ( true )

	false
)

--
-- name: RsMapObjectIsDynamic
-- desc: Determine whether an object is a dynamic object.
--
fn RsMapObjectIsDynamic obj = 
(
	(RsMapObjectIsGeometry obj) and (GetAttr obj idxIsDynamic)
)

--
-- name: RsMapObjectIsCollision
-- desc: Determine whether a node is a valid collision object for export.
--
fn RsMapObjectIsCollision obj = 
(
	( "Gta Collision" == ( GetAttrClass obj ) )
)

--
fn RsGetModelSubMeshes obj childArray = 
(
	if "Gta Object"==(getattrClass obj) then 
		append childArray obj
	for c in obj.children do RsGetModelSubMeshes c childArray
)

-- pipeline/export/maps/objects.ms
