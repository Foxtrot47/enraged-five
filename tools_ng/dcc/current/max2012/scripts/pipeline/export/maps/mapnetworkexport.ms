global RsMapNetworkExportParameters = #()
global RsMapNetworkExportServerIdx

fn RsMapNetworkExport = (
				
	local configFactory = dotnetclass "RSG.Base.Configuration.ConfigFactory"
	local autoSerConsumer
	local fileTransferSerConsumer	
	
	local selectedList = #()
	if(selection.count > 0) then
	(
		for item in selection do 
		(
			itemContainer = RsMapObjectGetMapContainerFor item
			if (itemContainer != undefined) then
			(
				-- Have to check the container names as just comparing two container objects wasn't
				-- coming back as true even though they were the same containers.
				local alreadyInList = false
				local itemContainerName = toLower itemContainer.name
				for cont in selectedList while not alreadyInList do
				(					
					if ( itemContainerName == (toLower cont.name) ) do
					(
						alreadyInList = true
					)
				)
				if not alreadyInList do
				(
					appendIfUnique selectedList itemContainer
				)
			)			
		)
	) 

	local maps = for map in selectedList where map.is_exportable() collect map

	RsContentTree.SetupGlobalsFromMapsAndLoadContentTree maps forceReload:true

	-- Scene xml export for latest texture usage to be figured out incase new textures or edited textures
	-- which aren't checked in.  This function already loops through all exportable containers in scene
	pushPrompt "Saving Scene XML"

	sceneXmlSuccess = RsMapSceneXmlSave()
	popPrompt()
	
	if not sceneXmlSuccess do 
	(
		gRsULog.LogError "Scene XML export was not successful, network export queue aborted."
		return false
	)

	local maxCommonFuncsClass = dotNetClass "RSG.Pipeline.Automation.Common.Utils.CommonFuncs"

	if ( maps.count <= 0 ) do
	(
		gRsUlog.LogError "Nothing to send for map network export.  You have to select an open container or an object in it, for it to be sent for network export."
		return false
	)

	if ( RsMapNetworkExportParameters.count < 1 ) do
	(
		gRsUlog.LogError "No server for map network export!"
		return false
	)
	
	-- Make sure a valid network export server selection
	if (RsMapNetworkExportServerIdx < 1) do RsMapNetworkExportServerIdx = 1
	
	try 
	(
		-- Automation service consumer setup (The services aren't in the setup files now)
		automationServiceURI = dotnetobject "System.Uri" (RsMapNetworkExportParameters[RsMapNetworkExportServerIdx].ServerHost.OriginalString + "/automation.svc")
		autoSerConsumer = dotnetobject "RSG.Pipeline.Automation.Consumers.AutomationServiceConsumer" automationServiceURI

		-- File transfer service consumer setup (The services aren't in the setup files now)
		fileTransferServiceURI = dotnetobject "System.Uri" (RsMapNetworkExportParameters[RsMapNetworkExportServerIdx].ServerHost.OriginalString + "/filetransfer.svc")		
		local config = configFactory.CreateConfig()
		fileTransferSerConsumer = dotnetobject "RSG.Pipeline.Automation.Consumers.FileTransferServiceConsumer" config fileTransferServiceURI
	)
	catch
	(
		gRsULog.LogError ("Exception while getting automation config information: " + getCurrentException())
		return false
	)

	local exportNetworkStatService = dotnetobject "RSG.RESTServices.Statistics.MapNetworkExportStatService"
	for map in maps do
	( 		
		-- Save max / container file		
		pushPrompt "Saving map containers"
		RsMapSetupGlobals map

		if ( RsMapType == #prop or RsMapType == #interior ) then 
		(
			if ( checkForSave() ) do
			(
				savemaxfile (maxFilePath + maxFileName)
			)
		)
		else 
		(			
			if ((map.cont.saveContainer false) == false) then
			(
				gRsULog.LogError ("Failed to save container " + map.name + " before queuing network export.  Please ensure you can save the container prior to exporting.  Aborting map network queue.")
				return false
			)
		)			
		popPrompt()

		pushPrompt "Setting up Map Network Export Job"	

		local mapName = dotNetObject "System.String" (RsContentTree.FindSourceFilenameFromBasename map.name)
		local job
		
		try
		(
			jobFactory = dotnetclass "RSG.Pipeline.Automation.Common.Jobs.MapExportJobFactory"
			
			contentFactory = dotnetclass "RSG.Pipeline.Content.Factory" 
			tree = contentFactory.CreateTree gRsBranch
			
			--        public static MapExportJob Create(IBranch branch, IContentTree tree, 
            --			P4API.P4Connection p4, String mapDccFilename, bool network)
			job = jobFactory.Create gRsBranch tree gRsPerforce.p4 mapName true
		)
		catch
		(
			gRsULog.LogError ("Error creating map network export job for " + map.name + ": " + getCurrentException())
			popPrompt()
			return false
		)
		
		try
		(
			local mapNames = #()
			append mapNames map.Name
			exportType = "Everything"
			exportNetworkStatService.MapNetworkExportStarted exportType job.ID mapNames
		)
		catch
		(
			gRsULog.LogWarning ("Failed to start stat tracking for map network export job (" + map.name + "): " + getCurrentException())
		)

		local dccFiles = dotNetObject "System.Collections.Generic.List`1[System.String]"

		for expProcIdx = 0 to (job.ExportProcesses.Count - 1) do
		(
			local dccSourceFile = job.ExportProcesses.Item[expProcIdx].DCCSourceFilename
			local sceneXmlPathname
			
			for expFileIdx = 0 to (job.ExportProcesses.Item[expProcIdx].ExportFiles.Count - 1) do			
			(
				local expFile = job.ExportProcesses.Item[expProcIdx].ExportFiles.Item[expFileIdx]
				if (matchpattern expFile pattern:"*.xml" == true) do
				(
					sceneXmlPathname = expFile
				)
			)			
			
			-- Upload DCC source file and scene xml file
			try
			(
				if ( RsFileExists dccSourceFile and fileTransferSerConsumer != undefined ) do
				(
					-- Got to pass the third parameter...
					-- public bool UploadFile(IJob job, String localFilename, String remoteFilename = "")
					fileTransferSerConsumer.UploadFile job (dotNetObject "System.String" dccSourceFile) (dotNetObject "System.String" "")
					fileTransferSerConsumer.UploadFile job (dotNetObject "System.String" sceneXmlPathname) (dotNetObject "System.String" "")	
				)
			)
			catch
			(
				gRsULog.LogError ("Error uploading DCC Source file (" + (dccSourceFile as string) + ") during map network export request: " + getCurrentException())
				popPrompt()
				return false
			)
			-- Used for the Job trigger later
			dccFiles.Add dccSourceFile

			-- Upload source textures that are new/have edits locally
			local editedSourceTextures = job.ExportProcesses.Item[expProcIdx].EditedSourceTextures
			if ( editedSourceTextures.Count > 0 ) do
			(
				for texId = 0 to (editedSourceTextures.Count - 1) do
				(
					local sourceTexture
					try
					(
						sourceTexture = RsMakeSafeSlashes (editedSourceTextures.Item(texId) as string)
						if ( RsFileExists sourceTexture and fileTransferSerConsumer != undefined ) do
						(
							-- Got to pass the third parameter...
							-- public bool UploadFile(IJob job, String localFilename, String remoteFilename = "")
							fileTransferSerConsumer.UploadFile job (dotNetObject "System.String" sourceTexture) (dotNetObject "System.String" "")
						)
					)
					catch
					(
						-- Don't make texture upload fails critical
						gRsULog.LogWarning ("Error uploading edited texture file (" + sourceTexture + ") during map network export request: " + getCurrentException())
					)
				)	
			)
		)

		try
		(			
			-- public UserRequestTrigger(IEnumerable<String> files)
			job.Trigger = dotNetObject "RSG.Pipeline.Automation.Common.Triggers.UserRequestTrigger" dccFiles
			autoSerConsumer.EnqueueJob job
		)
		catch
		(
			gRsULog.LogError ("Error triggering/queuing job! (" + (dccFiles as string) + "): " + getCurrentException())
			popPrompt()
			return false
		)	
	)

	if (exportStatService != undefined) then
	(
		exportStatService.Dispose()
		exportStatService = undefined
	)

	popPrompt()
	true
) -- RsMapNetworkExport
