------------------------------------------------------------------------------
--
-- File: ptxd.ms
-- Description: Parent TXD functions.
--
-- Author: David Evans <dave.evans@rockstarnorth.com>
--
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Globals
------------------------------------------------------------------------------

--filein "pipeline/util/xml.ms" -- loaded on startup by startup.ms
--filein "pipeline/util/file.ms" -- loaded on startup by startup.ms
dotnet.LoadAssembly "System.Xml.XPath.dll"

------------------------------------------------------------------------------
-- Structure Definitions
------------------------------------------------------------------------------

struct ParentTxd
(
	------------------------------------------------------------------------------------
	-- PUBLIC MEMBER DATA
	------------------------------------------------------------------------------------

	promotedTextureList = #(),
	mapTxdList = #(),
	ancestorTxdList = #(),
	
	------------------------------------------------------------------------------------
	-- PUBLIC FUNCTIONS
	------------------------------------------------------------------------------------

	fn load filename txdList = 
	(	
		local ptxdLoadMsg = "Ptxd - Load"
		gRsUlog.ProfileStart ptxdLoadMsg
		
		pushPrompt "Loading parent txds..."
		gRsUlog.LogMessage ("Loading ptxds: "+filename as string+"\n")
		
		try
		(
			if not RsFileExists filename then
			(
				gRsUlog.LogError ("Unable to load "+filename as string+". The file does not exist.")
				popPrompt()
				return false
			)
		
 			ptxd = XmlDocument()
 			ptxd.init()
 			ptxd.load filename
			document = ptxd.document
			
			navigator = document.CreateNavigator()
			dictionaryIt = navigator.Select(navigator.Compile("/globalDictionaries/globalDictionary"))
						
			-- Collect the mapTXDs from the ParentTxd xml file and store their ancestors as well
			while (dictionaryIt.MoveNext()) do
			(
				dictionaryNavigator = dictionaryIt.Current
				ParentTxd.CollectMapDictionariesAndAncestors mapTxdList ancestorTxdList dictionaryNavigator txdList
			)
			
			-- Using the ancestors collected above make a list of all the promoted textures for each mapTxds
			for i =1 to ancestorTxdList.count do
			(
				ParentTxd.CollectPromotedTextures ancestorTxdList[i] mapTxdList[i] promotedTextureList
			)
		)
		catch
		(
			popPrompt()
			gRsUlog.ProfileEnd context:ptxdLoadMsg
			gRsUlog.LogError ("Fatal Exception during ParentTxd::load:"+( getCurrentException() ))
		)
		
		popPrompt()
		gRsUlog.ProfileEnd context:ptxdLoadMsg
		
		true
	),
	
	fn CollectMapDictionariesAndAncestors mapTxdList ancestorTxdList dictionaryNavigator txdList =	(
		
		

		--loop through all map dictionaries and store the name in the given list.
		mapDictionaryIt = dictionaryNavigator.Select(dictionaryNavigator.Compile("MapChildren/MapDictionary"))
		if mapDictionaryIt.Count == 0 Then
		(
			mapDictionaryIt = dictionaryNavigator.Select(dictionaryNavigator.Compile("sourceDictionaries/sourceDictionary"))
		)
		
		while (mapDictionaryIt.MoveNext()) do
		(
 			mapDictionaryNavigator = mapDictionaryIt.Current
 			mapAttrIt = mapDictionaryNavigator.Select(mapDictionaryNavigator.Compile("@*"))
 			while (mapAttrIt.MoveNext()) do
 			(
 				stringValue = (mapAttrIt.Current.TypedValue as String)
 			
 				if (mapAttrIt.Current.Name == "Name" Or mapAttrIt.Current.Name == "name") Then
				(
					lowercaseString = toLower stringValue
					if (findItem txdList lowercaseString != 0) do
					(
						appendIfUnique mapTxdList lowercaseString
						ancestors = mapDictionaryNavigator.SelectAncestors mapDictionaryNavigator.NodeType false
						append ancestorTxdList ancestors -- not appendIfUnique because two maps can have the same set of ancestors.
					)
				)
 			)
        )

		-- recurse loop through all the child user dictionaries for this dictionary
		childDictionaryIt = dictionaryNavigator.Select(dictionaryNavigator.Compile("UserChildren/UserDictionary"))
		if childDictionaryIt.Count == 0 Then
		(
			childDictionaryIt = dictionaryNavigator.Select(dictionaryNavigator.Compile("globalDictionaries/globalDictionary"))
		)

		while (childDictionaryIt.MoveNext()) do
		(
			childNavigator = childDictionaryIt.Current
			ParentTxd.CollectMapDictionariesAndAncestors mapTxdList ancestorTxdList childNavigator txdList
		)
	),
	
	fn CollectPromotedTextures ancestorTxds mapTxd promotedTextureList  = (
		
		PromotedTextures = #()
		while (ancestorTxds.MoveNext()) do
		(
 			ancestorNavigator = ancestorTxds.Current
 			ancestorTextureIt = ancestorNavigator.Select(ancestorNavigator.Compile("Textures/Texture"))
 			if ( ancestorTextureIt.Count == 0 ) Then
 				ancestorTextureIt = ancestorNavigator.Select(ancestorNavigator.Compile("globalTextures/globalTexture"))
 				
  			while (ancestorTextureIt.MoveNext()) do
  			(
				textureNavigator = ancestorTextureIt.Current
				TextureAttrIt = textureNavigator.Select(textureNavigator.Compile("@*"))
				streamName = ""
				while (TextureAttrIt.MoveNext()) do
				(
					stringValue = (TextureAttrIt.Current.TypedValue as String)
  				
					if (TextureAttrIt.Current.Name == "filename") do
					(
						streamName = RsRemovePathAndExtension stringValue
					)
					if (TextureAttrIt.Current.Name == "alpha") do
					(
						local alphaName = RsRemovePathAndExtension stringValue
						if ( alphaName != undefined ) do
						(
							streamName += alphaName
						)
					)
				)
				
				if (streamName != undefined and streamName != "") do
				(
					append PromotedTextures streamName
				)
  			)
 		)
		append promotedTextureList PromotedTextures
	)
)
