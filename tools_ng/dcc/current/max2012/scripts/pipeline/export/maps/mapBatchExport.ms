/*
-- Example Max commands:
global RsMapBatchExport_newExport = True
fileIn "pipeline/export/maps/mapBatchExport.ms"
RsMapBatchExportStart #("cs1_01", "cs1_01_props", "cs1_02", "cs1_02_props","cs1_03", "cs1_03_props","cs1_04", "cs1_04_props")

-- Example DOS command:
"C:\Program Files\Autodesk\3ds Max 2012\3dsmax.exe" -mxs "global RsMapBatchExport_newExport = True; fileIn \"pipeline/export/maps/mapBatchExport.ms\"; RsMapBatchExportStart #(\"cs1_01\", \"cs1_01_props\") CloseMaxOnFinish:true"
*/

-- Script will restart Max if its memory-usage gets higher than this on reset: 
global RsMapBatchExport_RebootLimit = 1.5 -- GB

-- Path to file used to store export progress between Max-resets, and log export results: 
global RsMapBatchExport_iniFile = "x:/MapBatchExport.txt"
global RsMapExportCache_folder = RsProjectGetRootDir() + "/cache/Workbench/MapExport/"
global RsMapBuild_log = RsConfigGetToolsRootDir() +"/logs/build_maps.ulog"
global RsMapBatchExport_newExport -- If this global is set to True already, the iniFile will be deleted

filein "pipeline/export/maps/maptest.ms"
filein "pipeline/export/ui/exporter_dependency_dialog.ms"

fn RsMapBatchExportRun = 
(
	-- Don't run function if RsMapBatchExport_newExport is True, as the start-command will be coming later.
	-- Remove the ini-file & existing build log, if found.
	if (RsMapBatchExport_newExport == True) do 
	(
		if (doesFileExist RsMapBatchExport_iniFile) do 
		(
			deleteFile RsMapBatchExport_iniFile
		)
		
		if (doesFileExist RsMapBuild_log) do
		(
			deleteFile RsMapBuild_log
		)
		
		RsMapBatchExport_newExport = false
		
		return false
	)
	
	-- Cancel function if ini-file doesn't exist or its job has been completed:
	if (getINISetting RsMapBatchExport_iniFile "ExportProgress").count == 0 do return false
	
	if (RsMapRoll == undefined) do 
	(
		filein "rockstar/export/map.ms"
	)

	local rebootMemLimit = RsMapBatchExport_RebootLimit * (1024L ^ 3)
	
	-- Reset dialog-box's global values:
	global RsMapExportCancelled = false
	global RsDialogYesToAll = true
	
	local inputMapNames = filterString (getINISetting RsMapBatchExport_iniFile "ExportMaps" "mapNames") ","
	print ("inputMapNames:"+inputMapNames as string)
	local startMapNum = (getINISetting RsMapBatchExport_iniFile "ExportProgress" "NextMap") as integer
		
	fn resetOrReboot = 
	(
		resetMaxFile #noPrompt
		
		if (((sysinfo.getMAXMemoryInfo())[3] / (1024.0 ^ 3)) < RsMapBatchExport_RebootLimit) then 
		(
			-- Set up viewport to avoid loading textures:
			viewport.setLayout #layout_1
			viewport.setRenderLevel #Wireframe
			viewport.setGridVisibility 1 false
			MatEditor.Close()
			gc()
		)
		else 
		(
			local maxPathString = "\"" + (GetDir #maxroot) + "3dsmax.exe\""
			local argString = "-U MAXScript \""+ (GetDir #scripts) + "/pipeline/export/maps/mapBatchExport.ms\""
		
			shellLaunch maxPathString argString
			
			return true
		)
		
		return false
	)
	
	local quitFromMax = resetOrReboot()
	
	if not quitFromMax do 
	(
		-- Turn off Perforce integration:
--		local P4activeWas = RsProjectGetPerforceIntegration()
--		RsProjectSetPerforceIntegration false
		
		-- If it is currently auto saving, wait until it finishes so all the correct states are set:
		while ( autosave.isactive() == true ) do ()
		local autosaveWas = autosave.Enable
		autosave.Enable = false
		
		local RSrefsActiveWas = RSrefAllowUpdate
		RSrefAllowUpdate = false
		
		for mapNum = startMapNum to inputMapNames.count while not ::RsMapExportCancelled and not quitFromMax do  
		(
			local mapName = inputMapNames[mapNum]
			local sourcefileInfo, fileName, contObj, isglobal
			local exportStatus = "?"
			local iscontainer = false
			
			exportStatusString = ""
			isglobal = false
			
			gRsULog.ClearLogDirectory()
			
			if (filterstring mapName "/\\:").count > 1 then (
			
				filename = mapName
				
				if ((findstring mapName ".maxc") != undefined) then (
				
					iscontainer = true
				)
				
			) else (
			
				local mapInfo = RsProjectContentFind (toLower mapName) type:"mapzip"
			
				if (mapInfo != undefined) then (
								
					sourcefileInfo = mapInfo.inputs[1]
					
					if (sourcefileInfo == undefined) then (
					
						exportStatusString = "Map input undefined"		
						print exportStatusString
						
					) else (		

						filename = sourcefileInfo.path + "/" + sourcefileInfo.name + ".maxc"

						if(not (doesFileExist filename)) then (				

							filename = sourcefileInfo.path + "/" + sourcefileInfo.name + ".max"
							
							if(not (doesFileExist filename)) then (
							
								exportStatusString = "Not found max file or container (" + filename + ")"
								print exportStatusString
							)
						) else (
							
							iscontainer = true
						)
					)
				) else (
				
					exportStatusString = "Map name not known"
					print exportStatusString
				)
			)
			
			print mapName
		
			local stopwatch = dotNetObject "System.Diagnostics.Stopwatch"
			stopwatch.Start()
				
			if (filename != undefined) then (
											
				filefound = false

				if (iscontainer) then (
				
					contObj = (Containers.CreateInheritedContainer filename)
					
					if (contObj == undefined) then (
					
						exportStatusString = "Container-inherit failed"
						print exportStatusString
					) else (
					
						select contObj				
						filefound = true
					)
				) else (
										
					if(not (loadMaxFile filename useFileUnits:true quiet:true) ) then (
					
						exportStatusString = "(" + filename + ") exists but failed to load"
						print exportStatusString
					) else (
					
						maps = RsMapGetMapContainers()
						
						if maps.count == 1 then (

							filefound = true
							contObj = maps[1]
							isglobal = true
						) else (
						
							exportStatusString = "Error, there should be only 1 container with a max file"
							print exportStatusString
						)
					)
				)				
				
				if filefound then (
				
					format "Exporting: %\n" contObj.name
					
					local mapStructs = RsMapGetMapContainers()	
					if mapStructs.count != 1 do
					(
						gRsULog.LogError "More than 1 container was loaded during batch export, skipping!"
						continue
					)
					local mapStruct = mapStructs[1]
					
					-- Do this here, as if we make unique before we do RsMapGetMapContainers() then there will be no source or local definition
					-- filename, which means it'll just load the main game content tree when this could be DLC.
					if(iscontainer and contObj != undefined) do
					(
						setFileAttribute filename #readOnly false
						RsContFuncs.deleteLocks #(contObj) silent:true
						contObj.MergeSource() 
					)
					
					RsContentTree.SetupGlobalsFromMapsAndLoadContentTree mapStructs forceReload:true requestedBranchName:RsMapBranchRequested				
					
					::RsP4syncSceneTextures()
					
					local contentnode = RsContentTree.GetContentNode mapStruct.filename
					if undefined!=contentnode and "map_container"== RsContentTree.GetMapType contentnode then
					(
						local zipNode = RsContentTree.GetMapExportZipNode contentnode
						local xmlNode = RsContentTree.GetMapExportSceneXMLNode zipNode
						if xmlNode.GetParameter "use_meta_links" false do
						(
							local slod2RsRefs = for o in mapStruct.objects where RsContainerLodRef == classof o collect o
							local lodContainer = undefined
							for slod2Ref in slod2RsRefs do
							(
								if slod2Ref.filename.count <= 0 then
								(
									msg = (slod2Ref as string+" has empty filename property!")
									gRsUlog.LogError msg context:slod2Ref quiet:false
									continue
								)
								local filenamePart = getFilenameFile slod2Ref.filename
								local currentbranchContainerName = RsContentTree.FindSourceFilenameFromBasename filenamePart
								print ("original lod container:"+slod2Ref.filename as string)
								print ("current branch lod container:"+currentbranchContainerName as string)
								if lodContainer!=undefined and lodContainer != currentbranchContainerName then
								(
									gRsUlog.LogError ("SLOD2 references pointing to different containers:\n- "+
										lodContainer as string+"\n -"+
										slod2Ref.filename as string) context:slod2Ref
									lodContainer = undefined
								)
								else
								(
									lodContainer = (RsmakeSafeSlashes currentbranchContainerName)
								)
							)
							
							if lodContainer!=undefined do
							(
								if not (matchPattern lodContainer pattern:((RsmakeSafeSlashes gRsBranch.Art)+"*")) then
									gRsUlog.LogError ("LOD container "+lodContainer as string+" is not in current export branch")
									
								local lodContObj = (Containers.CreateInheritedContainer lodContainer)
								if undefined!=lodContObj do
								(
									setFileAttribute lodContainer #readOnly false
									RsContFuncs.deleteLocks #(lodContObj) silent:true
									lodContObj.MergeSource()

									::RsP4syncSceneTextures()
									
									if ::RsObjLinkFuncs.ReplaceSLOD2Refs() do
									(
										if ::gRsLinkMgr.SaveSceneLinks() do
										(
											lodContObj.localDefinitionFilename = lodContainer
											lodContObj.SaveContainer false
											delete lodContObj
											
											contObj.localDefinitionFilename = filename
											contObj.SaveContainer false
										)
									)
								)
							)
						)
					)
					
					if (::RsMapSetupGlobals mapStruct == true) do 
					(
						pushPrompt ("Deleting stream for " + mapName)
						::RsMapDeleteStream()
						popPrompt()
					)
					objects.ishidden = true
					
					local memNow = (sysinfo.getMAXMemoryInfo())[3] / (1024.0 ^ 3)
					
					local textStream = stringStream ""
					format "Exporting map % of %:\r% (% objects)\r" mapNum inputMapNames.count contObj.name objects.count to:textStream
					format "Memory: %GB\r(will reboot Max at %GB)" memNow RsMapBatchExport_RebootLimit to:textStream
					
					local textString = textStream as string
					
					local textObj = text name:"tempBatchExportText" text:textString wirecolor:yellow
					select textObj
					clearSelection()
					max tool zoomextents all
					forceCompleteRedraw()
					
					-- Reset cross-map globals.
					RsMapResetCrossMapGlobals()
					
					RsAutomatedExport = true
					
					gRsULog.quietMode = true
					local exportSuccess
									
					local expObj = undefined
					
					if isglobal != true then (
									
						expObj = (RsMapObjectGetMapContainerFor contObj)
					)						

					local exportMaps = #(expObj)
					
					-- ExportFiles array will be list of files that will be overwritten:
					local exportFiles = #()
					exportSuccess = RsMapReadOnlyBuildCheck exportMaps exportFiles:exportFiles
				
					-- ExportFiles will be ignored by dependency-getter, even if they're not checked out:
					if exportSuccess do (exportSuccess = RsShowExportDependencyDialogForMap maps:exportMaps forceToHead:true exportFiles:exportFiles)							

					-- Track the export time via the stats server.
					local exportStatService = dotnetobject "RSG.RESTServices.Statistics.MapExportStatService"
					
					local mapNames = for map in exportMaps collect map.Name
					try
					(
				  		local useAP3 = true
						exportType = "Everything"
						exportStatService.MapExportStarted exportType mapNames
					)
					catch
					(
						gRsULog.LogWarning (getCurrentException())
						exportStatService.Dispose()
						exportStatService = undefined
					)							
					
					if (exportStatService != undefined) then
					(
						try
						(
							exportStatService.SectionExportStarted(mapName)
						)
						catch
						(
							gRsULog.LogWarning (getCurrentException())
							exportStatService.Dispose()
							exportStatService = undefined
						)
					)
					
					if exportSuccess do (exportSuccess = ::RsMapRoll.DoExportMapSection expObj exportStatService:&exportStatService)
					
					if (exportStatService != undefined) then
					(
						try
						(
							exportStatService.SectionExportCompleted(exportSuccess)
						)
						catch
						(
							gRsULog.LogWarning (getCurrentException())
							exportStatService.Dispose()
							exportStatService = undefined
						)
					)
					
					if exportSuccess then (
					
						if (exportStatService != undefined) then
						(
							try
							(
								local mapNamesList = #()
								for map in exportMaps do
								(
									print map
									append mapNamesList map.Name
								)

								exportStatService.ImageBuildStarted(mapNamesList)
							)
							catch
							(
								gRsULog.LogWarning (getCurrentException())
								exportStatService.Dispose()
								exportStatService = undefined
							)
						)			
						
 						local msg = ("(mapNum ("+mapNum as string+") == inputMapNames.count ("+inputMapNames.count as string+")):"+(mapNum == inputMapNames.count) as string)
 						gRsUlog.LogMessage msg
 						print msg
 					
 						exportSuccess = ( ::RsMapRoll.DoMapBuildImages \
 																exportMaps \
 																rebuild:true \
 																lookupDependencies:true \
 																patchExport:false \
 																resource:(mapNum == inputMapNames.count)
 													)
					
						if (exportStatService != undefined) then
						(
							try
							(
								exportStatService.ImageBuildCompleted(exportSuccess)
							)
							catch
							(
								gRsULog.LogWarning (getCurrentException())
								exportStatService.Dispose()
								exportStatService = undefined
							)
						)					
					)
					
					exportStatusString =  case of 
					(
						RsMapExportCancelled:"Export cancelled"
						exportSuccess:"Exported"
						default:"Export failed"
					)
					
					-- Put the next map's number to the progress file, or remove that key if export is finished:
					if (mapNum < inputMapNames.count) and not RsMapExportCancelled then 
					(
						setINISetting RsMapBatchExport_iniFile "ExportProgress" "NextMap" ((mapNum + 1) as string)
						quitFromMax = resetOrReboot()
					)
					else 
					(
						quitFromMax = execute (getINISetting RsMapBatchExport_iniFile "ExportProgress" "CloseMaxOnFinish")
						delIniSetting RsMapBatchExport_iniFile "ExportProgress"
					)					
				) else (
					
					quitFromMax = execute (getINISetting RsMapBatchExport_iniFile "ExportProgress" "CloseMaxOnFinish")
				)
			)

			gRsPerforce.postExportAdd cancel:RsMapExportCancelled exclusive:true
			
			-- Update this map's export-status in the progress file:
			setINISetting RsMapBatchExport_iniFile "ExportResults" mapName exportStatusString
			copyFile RsMapBuild_log ( RsMapExportCache_folder + mapName + ".ulog" )
			
			if (exportStatService != undefined) then
			(
				try
				(
					exportStatService.MapExportCompleted(exportSuccess)
				)
				catch
				(
					gRsULog.LogWarning (getCurrentException())
					exportStatService.Dispose()
					exportStatService = undefined
				)
			)

			stopwatch.Stop()
			local reportGenerator = dotnetobject "RSG.RESTServices.Statistics.MapExportReportGenerator"
			if (exportStatService != undefined and exportStatService.Timings != undefined) then
			(
				reportGenerator.GenerateExportSummary(exportStatService.Timings)
				exportStatService.Dispose()
				exportStatService = undefined
			)
			else
			(
				reportGenerator.GenerateExportSummary(stopwatch.ElapsedMilliseconds)
			)			
		)
		
		-- Restore Max settings:
		RSrefAllowUpdate = RSrefsActiveWas
--		RsProjectSetPerforceIntegration P4activeWas
		autosave.Enable = autosaveWas
		autosave.resettimer()
		
		resetMaxFile #noPrompt
		forceCompleteRedraw()
	)
	
	if quitFromMax then 
	(
		quitMAX #noPrompt
	)
	else 
	(		
		if RsMapExportCancelled then 
		(
			messageBox "Map Batch Export Cancelled" title:"Export Cancelled"
		)
		else
		(
			messageBox "Map Batch Export Completed" title:"Export Complete"
		)
	)
)

fn RsMapBatchExportStart mapNames CloseMaxOnFinish:false perforceIntegration:false = 
(
	try
	(
		-- Set up new progress/log files & create the export cache folder:
		deleteFile RsMapBatchExport_iniFile
		deleteFile RsMapBuild_log
		makeDir RsMapExportCache_folder

		-- Remove any existing ulog files.
		local files = getFiles ( RsMapExportCache_folder + "*.ulog" )
		for f in files do (	deleteFile f )

		if (mapNames.count != 0) do 
		(
			-- Cancel export if user cancels save-needed dialog:
			if not checkForSave() do return false

			if (RsMapRoll == undefined) do 
			(
				filein "rockstar/export/map.ms"
			)

			local mapStructs = RsMapGetMapContainers()			
			RsContentTree.SetupGlobalsFromMapsAndLoadContentTree mapStructs forceReload:true
			
			-- Turn off Perforce integration:
			local P4activeWas = RsProjectGetPerforceIntegration()
			
			-- Map Network Export wants perforce integration on still so it can create the changelists as normal
			-- (it doesn't auto-submit them though as RsAutoSubmitChangelists is set to false
			if ( not perforceIntegration ) do
			(
				RsProjectSetPerforceIntegration false
			)

			local success = true

			--If game is running, cancel export
			if ( ::RSRemConIsGameRunning queryRestart:true == true ) do
			(
				buildSuccess = false
				::gRsULog.LogError ("The game is running, export stopped")
			)

			-- Do read-only check on map-files, allowing cancelling:
			success = ::RsMapReadOnlyBuildCheck mapStructs

			-- Cancel export if earlier stages returned false:
			if not success do (

				RsProjectSetPerforceIntegration P4activeWas
				return false 
			)
			
			local sortedMapNames = #()
			for mapName in mapNames do
				if matchPattern mapName pattern:"*lod*" then
					append sortedMapNames mapName
				else
					insertItem mapName sortedMapNames 1
			mapNames = sortedMapNames

			-- Create the export-progress ini-file:
			setINISetting ::RsMapBatchExport_iniFile "ExportMaps"  "mapNames" (RsArrayJoin mapNames)
			setINISetting RsMapBatchExport_iniFile "ExportProgress" "NextMap" "1"
			setINISetting RsMapBatchExport_iniFile "ExportProgress" "CloseMaxOnFinish" (CloseMaxOnFinish as string)
			
			-- Start the export:
			RsMapBatchExportRun()

			-- Reinstating active-Perforce setting
			-- This'll be turned off again by RsMapBatchExportRun, but it'll need to know what its original value was in order to reset it later:
			RsProjectSetPerforceIntegration P4activeWas		
		)
	)
	catch
	(
		::gRsULog.LogError ("Exception occured during automated map export.  This could be a script crash: " + getCurrentException())
	)
)

-- Running this script will auto-run this function
-- It only does something if an export-job ini-file has been generated by RsMapBatchExportStart
RsMapBatchExportRun()

