-- Collision-object Test-Functions
--	(This script is called by MapTest.ms)

-- Neal D Corbett - R* Leeds - 03/2014

-- Collision-exporter will error if a collision-mesh has too many verts/faces:
global RsMaxCollVerts = 32768
global RsMaxCollFaces = 100000

global idxDynamic = getattrindex "Gta Object" "Is Dynamic"
global idxInstanceCollision = getattrindex "Gta Object" "Instance Collision"
global idxFixed = getattrindex "Gta Object" "Is Fixed"
global idxFragment = getattrindex "Gta Object" "Is Fragment"
global idxAttribute = getattrindex "Gta Object" "Attribute"
global idxNoDecal = getattrindex "Gta Collision" "No Decal"

-- Basic object-info values are set when struct is initialised for an object.
--	Struct's functions will then use these values
--	Example Usage:
--	(RsCollObjTester Obj:$ isMilo:False Silent:True).Test()
struct RsCollObjTester 
(
	Obj, 					-- The object being tested
	
	-- OPTIONS:
	isMilo = False,		-- This should be set to True if object is part of a Milo	
	Silent = False,			-- Output errors to Log?
	AddToLists = False,	-- Option to add object to relevant MapTest arrays
	
	isCollision = True,
	isColPrimitive = False,
	isColBox = False,
	isColSphere = False,
	isColCylinder = False,
	isColCapsule = False,
	isColMesh = False,
	
	MeshVerts, MeshFaces, 
	
	-- Struct init:
	on Create do 
	(
		-- Set flags characterising 'Obj'
		case of 
		(
			(GetAttrClass Obj != "Gta Collision"):(isCollision = False)
			(isKindOf Obj Col_Mesh):(isColMesh = True)
			(isKindOf Obj Col_Box):(isColPrimitive = isColBox = True)
			(isKindOf Obj Col_Sphere):(isColPrimitive = isColSphere = True)
			(isKindOf Obj Col_Cylinder):(isColPrimitive = isColCylinder = True)
			(isKindOf Obj Col_Capsule):(isColPrimitive = isColCapsule = True)
		)
		
		if (isColMesh) do 
		(
			local ColTriMesh = (getColMesh Obj)
			
			if (ColTriMesh != undefined) do 
			(
				MeshVerts = GetNumVerts ColTriMesh
				MeshFaces = GetNumFaces ColTriMesh
			)
		)
	),
	
	-- Raise error for collision-objects with modifiers
	fn CheckModifiers = 
	(
		local retVal = True
		
		if (obj.modifiers.count != 0) do
		(
			retVal = False
			
			if (not Silent) do 
			(
				local msg = "Collision-object has modifiers applied."
				gRsULog.LogError msg context:Obj
			)
			
			if AddToLists do 
			(
				append ::RsObjectsWithModifiers Obj
			)
		)
		
		return retVal
	),
	
	fn CheckScale = 
	(
		local retVal = True 
		
		if (::RsIsScaled obj) do 
		(
			retVal = False
			
			if (not Silent) do 
			(
				local msg = stringStream ""
				format "Collision object (%) has scaling: %" obj.name obj.scale to:msg
				gRsULog.LogError (msg as string) context:obj
			)
			
			if AddToLists do 
			(
				append ::RsErrorSetHasScaling obj
			)
		)
		
		return retVal
	),
	
	fn CheckMiloRoom = 
	(
		local retVal = True 
		
		if (not isMilo) and (RsDoesCollMatHaveRooms obj) do 
		(
			retVal = False
			
			if (not Silent) do 
			(
				msg = (obj.name + " is non milo collision with milo room set")
				gRsULog.LogError msg context:obj
			)
		)
		
		return retVal
	),
	
	-- Check collision-object's dimensions:
	fn CheckDimensions = 
	(
		local retVal = True
		local BadColSize = False
		
		if isColBox do 
		(
			if (getUserProp obj "mover" != false) or \
				(getUserProp obj "horse" != false) or \
				(getUserProp obj "cover" != false) or \
				(getUserProp obj "vehicle" != false) do
			(
				if (obj.width < 0.01) or (obj.height < 0.01) or (obj.length < 0.01) do 
				(
					gRsULog.LogError ("Collision box " + obj.name + " has one or more dimensions that are less than 2 cm") context:obj
					retVal = False
				)
			)
		)
		
		if isColSphere or isColCylinder or isColCapsule do 
		(
			if (obj.radius < 0.0001) do 
			(
				BadColSize = True
			)
		)
		if isColCylinder or isColCapsule do 
		(
			if (obj.length < 0.0001) do 
			(
				BadColSize = True
			)
		)
		
		if (BadColSize) do 
		(
			retVal = False
			
			if (not Silent) do 
			(
				gRsULog.LogError ("Collision " + obj.name + " has one or more dimensions that are 0.0") context:obj
			)
		)
		
		return retVal
	),
	
	-- Returns False if non-box collision-primitive's parent doesn't have 'No Decal' flag set:
	fn CheckNoDecal = 
	(
		local retVal = True
		
		if (isColPrimitive) and (not isColBox) do 
		(
			local noDecal = GetAttr obj idxNoDecal	-- B* 888294
			
			if (not noDecal) and ((GetUserProp obj "weapons" == true) or (GetUserProp obj "weapons" == undefined)) do
			(
				local objParent = obj.parent
				
				-- Don't bother triggering if collision's parent is Dynamic or Fragment:
				if ((GetAttrClass objParent) != "Gta Object") or ((not (GetAttr objParent idxDynamic)) and (not (GetAttr objParent idxFragment))) do 
				(
					retVal = False
					
					if (not Silent) do 
					(
						local msg  = ("Non-box collision primitive '" + obj.name + "' is set as weapon collision, but doesn't have the 'no decal' flag set.  This is not allowed.")
						gRsULog.LogError msg context:obj
					)					
				)
			)
		)
		
		retVal = True
	),
	
	-- Check for collision with children:
	fn CheckChildren = 
	(
		local retVal = True
		
		if (Obj.Children.Count > 0) do 
		(
			retVal = False
			
			if (not Silent) do 
			(
				local msg = stringStream ""
				format "Collision object (%) has children." obj.name to:msg
				gRsULog.LogError (msg as string) context:obj
			)
		)
		
		return retVal
	),
	
	-- Test for invalid flags:
	fn CheckFlags = 
	(
		local retVal = True
		
		-- Get collision UDP-flag 'material' if we have a collision-primitive:
		if (getUserProp Obj "material" == True) do
		(
			-- Get collision's parent:
			local Par = Obj.Parent
			
			-- Go up hierarchy to find either a milo or undefined:
			while (isValidNode Par) and not (isKindOf Par Gta_MILO) do 
			(
				Par = Par.Parent
			)
			
			-- If it's not undefined then it's inside a MILO, which isn't allowed:
			if (isValidNode Par) do 
			(
				retVal = False
				
				if (not Silent) do 
				(
					local msg = stringStream ""
					format "Collision object % has 'material' flag - this is not allowed inside an interior" obj.name to:msg
					gRsULog.LogError (msg as string) context:obj
				)
			)
		)
		
		return retVal
	),
	
	-- Tests for invalid collision-materials:
	fn CheckMaterials = 
	(
		-- Only test materials for collision-meshes:
		if (not isColMesh) do return True
		
		-- Invalid-material test:
		local retVal = (::RSCheckMapObjectMaterial obj isColl:True)
		
		return retVal
	),
	
	-- Test validity of object's User Defined Properties:
	fn CheckUserPropValidity = 
	(
		RsCheckUserPropValidity obj
	),	
	
	-- Does Obj have too many/few verts/faces?
	fn CheckVertFaceCounts = 
	(
		if (not isColMesh) do return True
		
		local retVal = True
		
		-- Check vert-count:
		if (MeshVerts > RsMaxCollVerts) do
		(
			retVal = False
			
			if (not Silent) do 
			(
				local msg = (stringStream "")
				format "Collision mesh '%' has more than % verts (%) - Please split this up." Obj.Name RsMaxCollVerts MeshVerts To:Msg
				gRsULog.LogError (msg as string) context:Obj
			)
		)
		
		-- Check face-count:
		if (MeshFaces > RsMaxCollFaces) do 
		(
			retVal = False
			
			if (not Silent) do
			(			
				local msg = (stringStream "")
				format "Collision mesh '%' has more than % faces (%) - Please split this up." Obj.Name RsMaxCollFaces MeshFaces To:Msg
				gRsULog.LogError (msg as string) context:Obj
			)
		)
		
		if (MeshFaces == 0) do 
		(
			retVal = False
			
			if (not Silent) do 
			(
				local msg = (obj.name + " has 0 collision-faces")
				gRsULog.LogError msg context:obj
			)
			
			if AddToLists do 
			(
				append ::RsErrorZeroCollFaces obj
			)
		)
		
		return retVal
	),
	
	-- Run all tests, for maptest on export:
	fn Test = 
	(
		if (not isCollision) do return False
		
		local retVal = True
		
		-- Run each test in turn (stopping if any fail)
		for TestFunc in #(CheckModifiers, CheckScale, CheckMiloRoom, CheckDimensions, CheckNoDecal, CheckChildren, CheckVertFaceCounts, CheckFlags, CheckMaterials, CheckUserPropValidity) while retVal do 
		(
			local FuncVal = TestFunc()
			retVal = (retVal and FuncVal)
		)
		
		-- This is True if Obj passed all tests:
		return retVal
	)
)
