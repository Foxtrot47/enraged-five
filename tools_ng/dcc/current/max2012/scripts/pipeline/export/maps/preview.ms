--
-- File:: pipeline/export/maps/preview.ms
-- Description:: Map exporter preview implementation.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 9 June 2009
--

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------
global RsMapPreviewBits_Geometry = 0x00000001
global RsMapPreviewBits_Textures = 0x00000002
global RsMapPreviewBits_Metadata = 0x00000004
global RsMapPreviewBits_Preserve = 0x00000008
global RsMapPreviewBits_Validate = 0x00000010
global RsMapPreviewBits_Collision = 0x00000020
global RsMapPreviewBits_DoExpChecks=0x00000040
global RsMapPreviewBits_All		= 0x7FFFFFFF	-- Note: SIGNED 32-bit Integer

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- struct: RsMapPreview
-- desc: Map preview namespace struct.
--
struct RsMapPreview
(
	--
	-- name: Export
	-- desc: Map preview export function, uses RsMapPreviewConfig global for 
	--       configuration.
	--
	fn Export previewMask:(bit.or RsMapPreviewBits_Geometry RsMapPreviewBits_Textures) checkOnly: = 
	(
		if (previewMask == 0) then
		(
			MessageBox "No pipeline stages set for Map Preview.  Aborting."
			return ( false )
		)
		
		local success = true

		if (bit.and previewMask RsMapPreviewBits_Preserve) == 0 do 
		(
			RsClearPatchDir core:false
			RsClearPatchDir core:true
		)

		mapNameList = #()
		mapList = #()
		for obj in selection do 
		(
			mapStruct = (RsMapObjectGetMapContainerFor obj)
			if ( mapStruct != undefined ) do
			(
				mapName = mapStruct.name
				-- Have to get the name and check that as appendIfUnique is always true when using
				-- the RsMapContainer structs even if they are identical
				if ( (appendIfUnique mapNameList mapName) == true ) do
				(
					append mapList mapStruct
				)
			)
		)
		
		print ">>>>>>>>>>>"
		print mapList.count
		
		RsContentTree.SetupGlobalsFromMapsAndLoadContentTree mapList forceReload:true
		
		success = RsMapReadOnlyBuildCheck mapList
		
		for map in mapList while success do
		(
			RsMapSetupGlobals map		
			-- dont force preexport checks.
			if ( (bit.and previewMask RsMapPreviewBits_DoExpChecks ) > 0) then (
				success = ::RsPreExport map docheck:false previewMask:previewMask
			)

			-- Jump to the next one if only doing checks
			if checkOnly == true then(
				print ("Map " + map.name + " checked.")
				gRsUlog.LogMessage ("Map " + map.name + " checked.")
				continue
			)

			-- cache selection for all preview export types.
			local selObjs = for obj in map.selObjects where not (( isInternalRef o ) or ( isRefObj o )) collect obj

			if success and ( ( bit.and previewMask RsMapPreviewBits_Geometry ) > 0 ) then 
			(
				local mapgeom = ::RsMapGeometry()
				success = mapgeom.ExportMap map selObjs map.exportObjects filter:true
				-- Reselect original selection
				select selObjs
			)
			else
			(
				map_xml_pathname = RsConfigGetCacheDir() + "script/" + map.name + "/map.xml"
				if ( RsFileExists map_xml_pathname ) do deletefile map_xml_pathname
			)
			
			if success and ( ( bit.and previewMask RsMapPreviewBits_Textures ) > 0 ) then 
			(
				local maptxd = ::RsMapTXD()
				success = maptxd.ExportSelectedTxdsPreview map map.exportObjects
				-- bail if we fail. Heh. 
				if( not success ) then return false
			)
			else
			(
				txd_xml_pathname = RsConfigGetCacheDir() + "script/" + map.name + "/txd.xml"
				if ( RsFileExists txd_xml_pathname ) do deletefile txd_xml_pathname
			)

			if success and ( ( bit.and previewMask RsMapPreviewBits_Collision ) > 0 ) then 
			(
				-- Preserve the RsMapContentQueue because AP3 relies on it when doing the packing.
				RsMapSetupGlobals map preserveContent:true
				success = RsBoundsRoll.ExportBounds map.objects
			)
			else
			(
				bnd_static_xml_pathname = RsConfigGetCacheDir() + "script/" + map.name + "/bnd_static.xml"
				if ( RsFileExists bnd_static_xml_pathname ) do deletefile bnd_static_xml_pathname
				
				bnd_dynamic_xml_pathname = RsConfigGetCacheDir() + "script/" + map.name + "/bnd_dynamic.xml"
				if ( RsFileExists bnd_dynamic_xml_pathname ) do deletefile bnd_dynamic_xml_pathname
			)
			
			success = ::RsPostExport map patchExport:true
		)

		-- Exit if only doing checks
		if checkOnly == true then (
			print ("Finished preview checks")
			gRsUlog.LogMessage "Finished preview checks"
			return true
		)

		if success and ( ( bit.and previewMask RsMapPreviewBits_Metadata ) > 0 ) do 
		(
			-- derive the output names from the incoming maps
			struct NodeMap ( name, map )
			local outputNodes = #()
			
			for map in mapList do
			(
				local mapName = toLower map.name
				format ">>>>>> % \n" mapName
				-- FD WIP : Conversion to AP3
				-- We cant preview metadata until we have Map XML Conversion supported
				-- see bs:1641885
				
				--local sourceMapNode = RsContentTree.GetContentNode map.filename
				--local exportZipNode = RsContentTree.GetMapExportZipNode sourceMapNode
				--local processedMapNode = RsContentTree.GetMapProcessedNode exportZipNode
				--local sceneXmlNode = RsContentTree.GetMapExportSceneXMLNode exportZipNode
				
				gRsULog.LogError "DEPRECATED Preview Metadata : preview.ms Export()" context:map
			)
		)
		
		-- Resource and build all maps preview objects in a single pass.
		patchCollision = ( ( bit.and previewMask RsMapPreviewBits_Collision ) > 0 )
		buildImagesRequired = ( ( ( bit.and previewMask RsMapPreviewBits_Geometry ) > 0 ) or ( ( bit.and previewMask RsMapPreviewBits_Textures ) > 0 ) or patchCollision )

		if success and buildImagesRequired do 
		(
			success = ( ::RsMapBuildImages mapList rebuild:false patchExport:true patchCollision:patchCollision )
		)

		return success
	),
	
	--
	-- name: ClearPreviewFolder
	-- desc: Delete all files in the preview folder.
	--
	fn ClearPreviewFolder = 
	(
		RsClearPatchDir()
	)

) -- RsMapPreview struct

-- pipeline/export/maps/preview.ms
