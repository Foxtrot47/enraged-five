--
-- File:: pipeline/export/maps/maptxd.ms
-- Description:: Map exporter TXD export logic functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 10 June 2009
--
-----------------------------------------------------------------------------
-- HISTORY
-----------------------------------------------------------------------------
-- Rockstar Map Txd Export
-- Rockstar North
-- 1/3/2005
-- by Greg Smith
-- Export a map sections texture dictionaries into the game
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/AP3Asset.ms"
filein "pipeline/export/maps/globals.ms"
filein "pipeline/export/maps/ptxd.ms"
filein "rockstar/helpers/parentiser.ms"
filein "pipeline/util/texturemetatag_utils.ms"

-----------------------------------------------------------------------------
-- Script Globals
-----------------------------------------------------------------------------
--
-- struct: RsMapTXD
-- desc: Map TXD export logic functions.
--
struct RsMapTXD
(
	idxTxd = getattrindex "Gta Object" "TXD",
	idxDontExport = GetAttrIndex "Gta Object" "Dont Export",
	
	--
	-- name: GetAllSceneTXDs
	-- desc: Return array of all scene TXDs
	--
	fn GetAllSceneTXDs = (
	
		txdlist = #()
		
		modelsin = #()
		RsGetMapObjects $objects modelsin exportGeometryCheck:true						
		
		RsGetSortedTxdList modelsin txdlist		
		txdlist
	),

	--
	-- name: GetObjectsWithTxd
	-- desc: Get list of objects in objList using txdName
	--
	fn GetObjectsWithTxd txdName objList:objects = 
	(
		local txdObjs = #()
		
		for obj in objList where (not isRefObj obj) and (getattrclass obj == "Gta Object") do 
		(
			local valDontExport = getattr obj idxDontExport

			if valDontExport == false then 
			(
				local valTxd = getattr obj idxTxd
				
				if matchPattern valTxd pattern:txdName do 
				(
					append txdObjs obj
				)
			)
		)
		
		return txdObjs
	),

	--
	-- name: DeleteTextures
	-- desc:
	--
	fn DeleteTextures map txdlist txdobjlist allModels normalonly:false = 
	(	
		RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."
	
		RsMapExportCancelled = false

		inputobjlist = #()
		RsGetInputObjectList allModels inputobjlist

		for i = 1 to txdlist.count while not RsMapExportCancelled do 
		(
			local txdname = txdlist[i]

			progressStart ("deleting textures for " + txdname)			
			
			local txdobjs = txdobjlist[i]
		
			local texmaplist = #()
			local maxsizelist = #()
			local isbumplist = #()
			local usagelist = #()
			
			RsGetTexMapsByTxdNameNoStrip texmaplist maxsizelist isbumplist usagelist txdname srcobjlist:inputobjlist
			
			local texMapCount = texmaplist.count
			
			for i = 1 to texMapCount 
			where ((not normalonly) or isbumplist[i]) while (RsMapProgress i texMapCount) do 
			(
				local texMapFilename = texmaplist[i]
				local maxsize = maxsizelist[i]
			
				local stripName = RsStripTexturePath texMapFilename
				local singleName = (filterstring texMapFilename "+")[1]
				
				local foundFiles = getfiles singleName
				
				if foundFiles.count > 0 then (
					
					outputFile = (RsConfigGetMapsTexDir() + stripName + RsMapExportTextureExtension)
			
					RsDeleteFiles outputFile
				)
			)
			
			progressEnd()
		)
		
		return (not RsMapExportCancelled)
	),

	--
	-- name: ExportTextures
	-- desc: export the textures require for specific txd's
	--
	fn ExportTextures map txdlist txdobjlist allmodels normalonly:false syncTCSDir:true = 
	(
		local exportTexturesMsg = "ExportTextures"
		gRsUlog.ProfileStart exportTexturesMsg
			
		RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."
	
		tcsDir = RsConfigGetAssetsDir()
		tcsTemplatesDir = tcsDir + "metadata/textures/templates/..."
		tcsDir = RsConfigGetTCSMapsRoot()
		
		if ( syncTCSDir and RsGetModelTexturesPacked == true and RsGenerateTCSFiles == true ) do
		(
			gRsPerforce.run "sync" #( ( tcsDir + "..." ), tcsTemplatesDir ) silent:true
		)
	
		RsMapExportCancelled = false
		local buildSuccess = true
	
		RsMakeSurePathExists (RsConfigGetMapsTexDir())

		texmaplist = #()
		texmaptypelist = #()
		texmapobjlist = #()
		maxsizelist = #()
		isbumplist = #()
		usagelist = #()
		exemptionlist = #()
		texturetemplatelist = #()
		materialpresetlist = #()
		
		lod_texmaplist = #()
		lod_texmaptypelist = #()
		lod_texmapobjlist = #()
		lod_maxsizelist = #()
		lod_isbumplist = #()
		lod_usagelist = #()
		lod_exemptionlist = #()
		lod_texturetemplatelist = #()
		lod_materialpresetlist = #()

		local texExportInfoList = #()
		local materialPresetInfoList = #()
		
		inputobjlist = #()
		RsGetInputObjectList allmodels inputobjlist
		
		progressStart ("compiling texture list")
		local compileTexListMsg = "Compiling texture list"
		gRsUlog.ProfileStart compileTexListMsg
		
		
		for i = 1 to txdlist.count while (RsMapProgress i txdlist.count) do 
		(
			txdname = txdlist[i]
			txdobjs = txdobjlist[i]
			
			if RsIsTXDUsedOnLod txdname then (
			
				RsGetTexMapsByTxdNameNoStrip lod_texmaplist lod_maxsizelist lod_isbumplist lod_usagelist txdname srcobjlist:inputobjlist exemptionlist:lod_exemptionlist texmapobjlist:lod_texmapobjlist texmaptypelist:lod_texmaptypelist texturetemplatelist:lod_texturetemplatelist materialpresetlist:lod_materialpresetlist
			) else (	
			
				RsGetTexMapsByTxdNameNoStrip texmaplist maxsizelist isbumplist usagelist txdname srcobjlist:inputobjlist exemptionlist:exemptionlist texmapobjlist:texmapobjlist texmaptypelist:texmaptypelist texturetemplatelist:texturetemplatelist materialpresetlist:materialpresetlist
			)

			--GunnarD: nasty, due to conflicting "pack textures" attribute we need to scan all objects for light textures that might be to add to this TXD...
			(
				lightmap_texmaplist = #()
				lightmap_maxsizelist = #()
				lightmap_isbumplist = #()
				lightmap_usagelist = #()
				lightmap_texmaptypelist = #()
				lightmap_exemptionlist = #()
				lightmap_texturetemplatelist = #()
				lightmap_materialpresetlist = #()

				-- DHM FIX ME: we do this all over the place... we should know the EXPORTED textures by now; not the source...
				RsGetTexMapsByTxdNameNoStrip lightmap_texmaplist lightmap_maxsizelist lightmap_isbumplist lightmap_usagelist txdname texmaptypelist:lightmap_texmaptypelist exemptionlist:lightmap_exemptionlist  texturetemplatelist:lightmap_texturetemplatelist materialpresetlist:lightmap_materialpresetlist
				
				for li=1 to lightmap_texmaplist.count do
				(
					local type = lightmap_texmaptypelist[li]
					if type == "LightMap" then
					(
						append texmaplist lightmap_texmaplist[li]
						append maxsizelist lightmap_maxsizelist[li]
						append isbumplist lightmap_isbumplist[li]
						append usagelist lightmap_usagelist[li]
						append texmaptypelist lightmap_texmaptypelist[li]
						append exemptionlist lightmap_exemptionlist[li]
						append texturetemplatelist lightmap_texturetemplatelist[li]
						append materialpresetlist lightmap_materialpresetlist[li]
					)
				)
			)
		)
		
		gRsUlog.ProfileEnd context:compileTexListMsg
		progressEnd()
		
		if not RsMapExportCancelled do 
		(

			progressStart ("resolving textures")
			local resolveTexMsg = "Resolving Textures"
			gRsUlog.ProfileStart resolveTexMsg
			
			
			for i = 1 to materialpresetlist.count do
			(
				append materialPresetInfoList materialpresetlist[i]
			)
			
			for i = 1 to texmaplist.count while (RsMapProgress i texmaplist.count) do 
			(			
				texMapFilename = texmaplist[i]
				texmaptype = texmaptypelist[i]
				texmapobj = texmapobjlist[i]
				maxsize = maxsizelist[i]
				isbump = isbumplist[i]
				usage = usagelist[i]
				isresizeexempt = exemptionlist[i]
				texturetemplate = texturetemplatelist[i]
				
				local isLightMap = "LightMap"==texmaptype
				--print("***" + texMapFilename + " usage:" + (usage as string) + " type:"+ texmaptype as string)
				
				-- url:bugstar:1945367 we WANT the light texture to go in the SHARED TXD, and NOT in the drawable.
				if usage == 1 and not isLightMap then (
					
					continue
				)
				
				local extension = getFilenameType texMapFilename
				local stripName = RsStripTexturePath texMapFilename
				local singleName = (filterstring texMapFilename "+>")[1]
				local outputFile = ""
				
				case extension of
				(
					RsMapExportTextureExtensionPassthrough:
						outputFile = (RsConfigGetMapsTexDir() + RsRemoveSpacesFromString(stripName) + RsMapExportTextureExtensionPassthrough)
					default:
						outputFile = (RsConfigGetMapsTexDir() + RsRemoveSpacesFromString(stripName) + RsMapExportTextureExtension)
				)

				if isbump == false and normalonly == true then continue
					
				local texExportInfo = RsExportMapTexInfo tcsDir:tcsDir texmap:texmapobj texMapFilename:texMapFilename outputFile:outputFile \ 
					textureTemplate:texturetemplate resizeExempt:isResizeExempt texmapType:texmaptype
				append texExportInfoList texExportInfo
			)
			gRsUlog.ProfileEnd context:resolveTexMsg
			progressEnd()
		)	
		
		if buildSuccess and not RsMapExportCancelled do 
		(
			progressStart ("resolving lod textures")
			local resolveLodTexMsg = "resolving lod textures"
			gRsUlog.ProfileStart resolveLodTexMsg
			
			for i = 1 to materialpresetlist.count do
			(
				append materialPresetInfoList lod_materialpresetlist[i]
			)
			
			for i = 1 to lod_texmaplist.count while (RsMapProgress i lod_texmaplist.count) do 
			(
				texMapFilename = lod_texmaplist[i]
				texmaptype = lod_texmaptypelist[i]
				texmapobj = lod_texmapobjlist[i]
				maxsize = lod_maxsizelist[i]
				isbump = lod_isbumplist[i]
				usage = lod_usagelist[i]
				isresizeexempt = lod_exemptionlist[i]
				texturetemplate = lod_texturetemplatelist[i]
				
				local isLightMap = "LightMap"==texmaptype
				--print("*** LOD " + texMapFilename + " usage:" + (usage as string) + " type:"+ texmaptype as string)
			
				-- url:bugstar:1945367 we WANT the light texture to go in the SHARED TXD, and NOT in the drawable.
				if usage == 1 and not isLightMap then (
					
					continue
				)
		
				local extension = getFilenameType texMapFilename
				local stripName = RsStripTexturePath texMapFilename
				local outputFile = ""

				case extension of
				(
					RsMapExportTextureExtensionPassthrough:
						outputFile = (RsConfigGetMapsTexDir() + RsRemoveSpacesFromString(stripName) + RsMapExportTextureExtensionPassthrough)
					default:
						outputFile = (RsConfigGetMapsTexDir() + RsRemoveSpacesFromString(stripName) + RsMapExportTextureExtension)	
				)						
				
				local texExportInfo = RsExportMapTexInfo tcsDir:tcsDir texmap:texmapobj texMapFilename:texMapFilename outputFile:outputFile \ 
					textureTemplate:texturetemplate resizeExempt:isResizeExempt texmapType:texmaptype
				append texExportInfoList texExportInfo
			)
			progressEnd()
			gRsUlog.ProfileEnd context:resolveLodTexMsg
		)
		
		if buildSuccess and not RsMapExportCancelled do 
		(	
			local exportMapTexturesMsg = "RsExportMapTextures"
			gRsUlog.ProfileStart exportMapTexturesMsg

			RsExportMapTextures map texExportInfoList materialPresetInfoList
			
			gRsUlog.ProfileEnd context:exportMapTexturesMsg
		)

		if buildSuccess and not RsMapExportCancelled and RsEnableMapGTXDExport do 
		(
			local exportMapGTXDMsg = "RsExportMapGTXDs"
			gRsUlog.ProfileStart exportMapGTXDMsg
			RsExportMapGTXD()
			gRsUlog.ProfileEnd context:exportMapGTXDMsg
		)
		
		gRsUlog.ProfileEnd context:exportTexturesMsg	
		return (buildSuccess and not RsMapExportCancelled)
	),

	--
	-- name: RemovedUnused 
	-- desc: delete any unused texture dictionaries from disk
	--
	fn RemovedUnused = (
		
		local sceneTxdList = GetAllSceneTXDs()
		local sceneTxds = #()
		RsGetFileTxdList sceneTxds
		
		txdFiles = getfiles (RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/*.?td")
		
		for i = 1 to txdFiles.count do (
			
			txdFile = toLower(RsRemovePathAndExtension(txdFiles[i]))

			if (finditem sceneTxdList txdFile == 0) and (finditem sceneTxds txdFile == 0) then (

				RsDeleteFiles txdFiles[i]
			)
		)
	),
	
	------------------------------------------------------------------
	-- Create our AP3 asset pack XML.
	--
	-- in: txdlist: a list of all unique txds in the scene
	-- in: txdobjlist: a list of all unique objects in the scene
	-- in: outfilename: path and filename of output file
	------------------------------------------------------------------
	fn CreateTxdAssetPack txdlist txdobjlist sceneobjlist outFileName = 
	(

		local createTxdAssPackMsg = "CreateTxdAssetPack"
		gRsUlog.ProfileStart createTxdAssPackMsg
		
		RsMapExportCancelled = false
				
		--dirTarget = ("dirStream .. \"maps/" + RsMapName + "/\"")
		dirTargetFolder = (RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/")
		dirBumpTargetFolder = dirTargetFolder + "bumplist" + "/"
		
		RsMakeSurePathExists dirBumpTargetFolder

		-- Create structure to load up the parent txd file and find promoted textures
		local parentTxdStruct = ParentTxd()
		if RsProcessGTXDDependencies then
		(
			local txdFile = (RsConfigGetAssetsDir core:true + "/maps/ParentTxds.xml")
			gRsPerforce.sync txdFile
			if (parentTxdStruct.Load (RsConfigGetAssetsDir core:true + "maps/ParentTxds.xml") txdlist) == false then return false
		)
		
		-- Get scene objects without packed textures
		local inputobjlist = #()
		RsGetInputObjectList sceneobjlist inputobjlist
	
		progressStart "Creating texture hierarchy"
		local texHierarchyMsg = "Creating texture hierarchy"
		gRsUlog.ProfileStart texHierarchyMsg
		
		local txdAssetPackFile = AP3AssetFile()
		txdAssetPackFile.Initialize filename:outFileName
		for i = 1 to txdlist.count while (RsMapProgress i txdlist.count) do 
		(
			bumpFileName = dirBumpTargetFolder + txdlist[i] + ".textures"
			bumpFile = openfile bumpFileName mode:"w+"
			
			txdname = txdlist[i]
			txdobjs = txdobjlist[i]

			texmaplistnostrip = #()
			texmaplist = #()
			maxsizelist = #()
			isbumplist = #()
			usagelist = #()
			texmaptypelist = #()

			-- DHM FIX ME: we do this all over the place... we should know the EXPORTED textures by now; not the source...
			RsGetTexMapsByTxdName texmaplist maxsizelist isbumplist usagelist txdname srcobjlist:inputobjlist texmaptypelist:texmaptypelist texmaplistnostrip:texmaplistnostrip

			--GunnarD: nasty, due to conflicting "pack textures" attribute we need to scan all objects for light textures that might be to add to this TXD...
			(
				lightmap_texmaplistnostrip = #()
				lightmap_texmaplist = #()
				lightmap_maxsizelist = #()
				lightmap_isbumplist = #()
				lightmap_usagelist = #()
				lightmap_texmaptypelist = #()

				-- DHM FIX ME: we do this all over the place... we should know the EXPORTED textures by now; not the source...
				RsGetTexMapsByTxdName lightmap_texmaplist lightmap_maxsizelist lightmap_isbumplist lightmap_usagelist txdname texmaptypelist:lightmap_texmaptypelist texmaplistnostrip:lightmap_texmaplistnostrip
				
				for li=1 to lightmap_texmaplist.count do
				(
					local type = lightmap_texmaptypelist[li]
					if type == "LightMap" then
					(
						append texmaplistnostrip lightmap_texmaplistnostrip[li]
						append texmaplist lightmap_texmaplist[li]
						append maxsizelist lightmap_maxsizelist[li]
						append isbumplist lightmap_isbumplist[li]
						append usagelist lightmap_usagelist[li]
						append texmaptypelist lightmap_texmaptypelist[li]
					)
				)
			)
			-- At this point we need to remove any textures from the above lists that have been promoted up.
			if RsProcessGTXDDependencies then 
			(
				for i = 1 to parentTxdStruct.mapTxdList.count where
						undefined != parentTxdStruct.promotedTextureList[i]
					do
				(
					if ( 0 == stricmp parentTxdStruct.mapTxdList[i] txdname ) do
					(
						for j = 1 to parentTxdStruct.promotedTextureList[i].count do
						(
							result = findItem texmaplist parentTxdStruct.promotedTextureList[i][j]
							if ( result != 0 ) do
							(
								if (texmaplistnostrip[result] != undefined) do deleteItem texmaplistnostrip result
								if (texmaplist[result] != undefined) do deleteItem texmaplist result
								if (maxsizelist[result] != undefined) do deleteItem maxsizelist result
								if (isbumplist[result] != undefined) do deleteItem isbumplist result
								if (usagelist[result] != undefined) do deleteItem usagelist result
								if (texmaptypelist[result] != undefined) do deleteItem texmaptypelist result
							)
						)
					)
				)
				
				
				parenttexmaplist = #()
				RsGetTxdParentTextureList txdname parenttexmaplist
			)	

			filteredtexmaplist = #()
			filteredtexmaplistnostrip = #()

			for j = 1 to texmaplist.count do (

			-- url:bugstar:1945367 we WANT the light texture to go in the SHARED TXD, and NOT in the drawable.
				if 	texmaptypelist[j]=="LightMap" or 
				((usagelist[j] > 1) or (usagelist[j] < 0)) then 
				(
					texmapname = texmaplist[j]
					texmapname = RsRemoveSpacesFromString texmapname

					if not RsProcessGTXDDependencies or finditem parenttexmaplist texmapname == 0 then (

						append filteredtexmaplist texmaplist[j]
						append filteredtexmaplistnostrip texmaplistnostrip[j]

						texname = RsRemoveSpacesFromString ((filterstring texmapname ".")[1])

						if isbumplist[j] then (

							format "% { Flags 1 }\n" texname to:bumpFile
						) else (

							format "%\n" texname to:bumpFile
						)
					)
				)
			)
						
			close bumpFile

			if ( filteredtexmaplist.count == 0 ) do 
			(
				gRsULog.LogMessage ("Skipping empty TXD: " + txdname)
				continue
			)
			
			-- Update the AP3 Asset Pack Schedule
			local txdFilename = dirTargetFolder + "/" + txdname + ".itd.zip"
			local txdZipElement = ( txdAssetPackFile.AddZipArchive txdFilename )

			-- Add our TCL files.
			for i = 1 to filteredtexmaplist.count do
			(
				local texmapname = filteredtexmaplist[i]
				local texmapfilename = filteredtexmaplistnostrip[i]
			
				local textureTcsFilename = RsConfigGetStreamDir() + "/maps/tcls/" + texmapname + ".tcl"
				txdAssetPackFile.AddFile txdZipElement textureTcsFilename
			)
			
			-- Add .textures list file.
			txdAssetPackFile.AddFile txdZipElement bumpFilename
		)

		-- Write out AP3 Asset Pack Schedule file.
		txdAssetPackFile.WriteXmlFile outFileName
		RsMapAddContent RsMapName outFileName
		
		result = true
		if ( not txdAssetPackFile.ValidateXml() ) then
		(
			gRsULog.LogError "Failed to create TXD Asset Pack XML.  See errors above."
			result = false
		)
		gRsUlog.ProfileEnd context:texHierarchyMsg
		progressEnd()
		gRsUlog.ProfileEnd context:createTxdAssPackMsg
		if RsMapExportCancelled then false else 
		(
			result
		)		
			
	),
	
	--
	-- name: ExportTxds
	-- desc: Export out the passed in set of texture dictionaries
	--
	fn ExportTxds map txdlist txdobjlist allmodels syncTCSDir:true = 
	(
		RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."
	
		-- set up map export globals
		RemovedUnused()

		-- export the required textures to the output path (55 secs for all textures in manhat07 when there are none to export)
		ExportTextures map txdlist txdobjlist allmodels syncTCSDir:syncTCSDir

		-- create AP3 Asset Pack Schedule
		local txdAssetPackScheduleFilename = RsConfigGetScriptDir() + RsMapName + "/assetpack_txd.xml"
		RsMakeSurePathExists txdAssetPackScheduleFilename
		
		( CreateTxdAssetPack txdlist txdobjlist allmodels txdAssetPackScheduleFilename )
	),

	--
	-- name: ExportAllTxds
	-- desc: export all the txds
	--
	fn ExportAllTxds map children syncTCSDir = (

		RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."

		txdlist = #()
		txdobjlist = #()
		
		modelsin = #()
		RsGetMapObjects children modelsin exportGeometryCheck:true
		
		if ( RsIsDLCProj() ) then
		(
			-- If exporting DLC modify the list to only include objects
			-- with the "New TXD" attribute checked.
			local idxNewTXD = GetAttrIndex "Gta Object" "TXD DLC"
			modelsin = for model in modelsin where (GetAttr model idxNewTXD == true) collect model
		)
		
		RsGetTxdList modelsin txdlist txdobjlist
		ExportTxds map txdlist txdobjlist children syncTCSDir:syncTCSDir -- returns true/false
	),

	--
	-- name: ExportSelectedTxds
	-- desc: Export selected geometry's texture dictionaries
	--
	fn ExportSelectedTxds map children selObjects syncTCSDir = (
		
		RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."

		txdlist = #()
		txdobjlist = #()
		
		modelsin = #()
		RsGetMapObjects selObjects modelsin exportGeometryCheck:true		
		
		if ( RsIsDLCProj() ) then
		(
			-- If exporting DLC modify the list to only include objects
			-- with the "New TXD" attribute checked.
			local idxNewTXD = GetAttrIndex "Gta Object" "TXD DLC"
			modelsin = for model in modelsin where (GetAttr model idxNewTXD == true) collect model
		)
		
		RsGetTxdList modelsin txdlist txdobjlist
		ExportTxds map txdlist txdobjlist children syncTCSDir:syncTCSDir -- returns true/false
	),
	
	--
	-- name: ExportSelectedTxdsPreview
	-- desc: Export selected geometry's texture dictionaries to the preview folder
	--
	fn ExportSelectedTxdsPreview map children = (
	
		RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."
		
		txdlist = #()
		txdobjlist = #()
		
		modelsin = #()
		RsGetMapObjects selection modelsin exportGeometryCheck:true				
		
		if ( RsIsDLCProj() ) then
		(
			-- If exporting DLC modify the list to only include objects
			-- with the "New TXD" attribute checked.
			local idxNewTXD = GetAttrIndex "Gta Object" "TXD DLC"
			modelsin = for model in modelsin where (GetAttr model idxNewTXD == true) collect model
		)
		
		RsGetTxdList modelsin txdlist txdobjlist
		for aModel in modelsin do(
			local mapList = #()	
			local maxID = if(isKindOf aModel.mat Multimaterial) then aModel.mat.Count else 1
			RsMatGetMapIdsUsedOnObject aModel mapList mat:aModel.mat 
			for mid in mapList do( 
				if mid > maxID then (
					ss = stringstream ""
					format "'%' has one or more polygons with a material id outside of the applied Multimaterial (Material '%'' has % submaterials)" aModel.name aModel.mat.name maxID to:ss
					gRsUlog.LogError ss context:aModel 
					return false
				)
			)
		)
		ExportTxds map txdlist txdobjlist children
	),

	--
	-- name: ExportSingleTXD
	-- desc: Export single texture dictionary from selection
	--
	fn ExportSingleTXD map children txdname = (
	
		RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."
		
		txdlist = #()
		txdobjlist = #()
				
		append txdlist txdname
		append txdobjlist $objects		
		
		if ( RsIsDLCProj() ) then
		(
			-- If exporting DLC modify the list to only include objects
			-- with the "New TXD" attribute checked.
			local idxNewTXD = GetAttrIndex "Gta Object" "TXD DLC"
			txdobjlist = for model in $objects where (GetAttr model idxNewTXD == true) collect model
		)
		
		( ExportTxds map txdlist txdobjlist children )		
	),

	--
	-- name: ExportAll
	-- desc: Export all object's texture dictionaries
	--
	fn ExportAll = (

		local maps = RsMapGetMapContainers()

		for map in maps do
		(
			for o in $objects do
			(
				if ( undefined != o.parent ) then
					continue
				if ( Container != classof o ) then
					continue
				if ( not o.IsOpen() ) then
					continue

				( ExportAllTxds o.children true )
			)
		)
	),
	
	--
	-- name: ExportSelected
	-- desc: Export selected object's texure dictionaries
	--
	fn ExportSelected selObjects = (
		local retval = false
		for o in $objects do
		(
			if ( undefined != o.parent ) then
				continue
			if ( Container != classof o ) then
				continue
			if ( not o.IsOpen() ) then
				continue
			
			retval = ( ExportSelectedTxds map o.children selObjects true )
		)
		retval
	)

) -- RsMapTXD struct

-- pipeline/export/maps/maptxd.ms
