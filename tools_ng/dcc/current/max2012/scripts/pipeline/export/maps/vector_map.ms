--
-- File:: pipeline/export/maps/vector_map.ms
-- Description:: Level vector map export, input for Map Browser application.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 18 June 2010
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
--filein "pipeline/util/xml.ms" -- loaded on startup by startup.ms
filein "pipeline/util/assert.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: ExportVectorMapDataSpline
-- desc: Export a single vector map spline into the XmlDocument.
--
fn ExportVectorMapDataSpline xmldoc xmlroot splineObj = (

	local nsplines = ( numSplines splineObj )
	local xmlelem = xmldoc.CreateElement( "section" )
	local parts = ( filterString splineObj.name "-" )
	RsAssert (parts.count > 0) message:"Invalid spline name."
	
	local containerName = parts[1]
	local owner = undefined
	if ( parts.count > 1 ) then
		owner = parts[2]
	
	xmlelem.SetAttribute "name" parts[1]
	xmlelem.SetAttribute "spline_count" (nsplines as String)
	if ( undefined != owner ) then
		xmlelem.SetAttribute "owner" owner
		
	local xmlelemcolour = xmldoc.CreateElement( "colour" )
	xmlelemcolour.SetAttribute "r" ((splineObj.wireColor.r / 255.0) as String)
	xmlelemcolour.SetAttribute "g" ((splineObj.wireColor.g / 255.0) as String)
	xmlelemcolour.SetAttribute "b" ((splineObj.wireColor.b / 255.0) as String)
	xmlelem.AppendChild( xmlelemcolour )
	
	for s = 1 to nsplines do
	(
		local nknots = ( numKnots splineObj s )
		local xmlelemspline = xmldoc.CreateElement( "spline" )
		xmlelemspline.SetAttribute "point_count" (nknots as String)

		for n = 1 to nknots do
		(
			local p = ( getKnotPoint splineObj s n )
			local xmlelempoint = xmldoc.CreateElement( "point" )
			xmlelempoint.SetAttribute "x" (p.x as String)
			xmlelempoint.SetAttribute "y" (p.y as String)
			xmlelempoint.SetAttribute "z" (p.z as String)
			
			xmlelemspline.AppendChild( xmlelempoint )
		)
		
		xmlelem.AppendChild( xmlelemspline )
	)
	
	xmlelem
)

--
-- name: ExportVectorMapData
-- desc: Export vector map data from current scene to a specified XML file.
--
fn ExportVectorMapData filename = (
	
	local doc = XmlDocument()
	doc.init()
	xmldoc = doc.document
	
	xmlroot = xmldoc.AppendChild( xmldoc.CreateElement( "vector_map" ) )
	for o in rootNode.children do
	(
		if ( undefined != o and 
			 ( Line == classof o ) or ( SplineShape == classof o ) ) then
		(
			format "Exporting: %\n" o.name
			xmlroot.AppendChild( ExportVectorMapDataSpline xmldoc xmlroot o )
		)
	)
	
	format "Writing %...\n" filename
	xmldoc.Save( filename )
	true
)

-- pipeline/export/maps/vector_map.ms
