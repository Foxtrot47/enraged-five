--
-- File:: pipeline/export/maps/collision.ms
-- Description:: Map collision exporter.
--
-- Date:: 11 July 2005 (originally mapbounds.ms)
-- Date:: 31 January 2013 (moved to pipeline, AP3 asset packing)
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/AP3Asset.ms"
filein "pipeline/export/maps/globals.ms"
filein "rockstar/util/attribute.ms"

-----------------------------------------------------------------------------
-- Script Globals
-----------------------------------------------------------------------------

try (destroyDialog RsBoundsRoll) catch ()

idxDynamic = getattrindex "Gta Object" "Is Dynamic"
idxInstanceCollision = getattrindex "Gta Object" "Instance Collision"
idxFixed = getattrindex "Gta Object" "Is Fixed"
idxFragment = getattrindex "Gta Object" "Is Fragment"
idxAttribute = getattrindex "Gta Object" "Attribute"
	
fn RsMiloObjectNeedsOwnCollision obj = 
(
	local doesItNeedToHaveCollision = false

	for childobj in obj.children while not doesItNeedToHaveCollision do 
	(
		if (isKindOf childobj Gta_Particle) or (isKindOf childobj GtaExplosion) or (isKindOf childobj GtaProcObj) or (isKindOf childobj GtaSwayable) do 
		(
			doesItNeedToHaveCollision = true
		)
	)	
	
	return doesItNeedToHaveCollision
)

struct CollisionSection (hidetail = false, objects = #(), name, bucketName, isIPLgroup = false)

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------
rollout RsBoundsRoll "Bounds Export"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	button btnExportBounds "Export Bounds" width:100 enabled:false tooltip:"Use export selected with any collision node selected"
	button btnTestBounds "Test Bounds" width:100
	button btnCreateXrefBounds "Create Xref Bounds" width:100

	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	--
	-------------------------------------------------------------
	fn CreateStaticBoundsForInternalRefs modelsin = 
	(
		RsMapExportCancelled = false
		
		idxPieceType = getattrindex "Gta Collision" "Piece Type"
		idxDayBrightness = getattrindex "Gta Collision" "Day Brightness"
		idxNightBrightness = getattrindex "Gta Collision" "Night Brightness"
		idxCollType = getattrindex "Gta Collision" "Coll Type"
	
		local irefObjList = #()

		for obj in modelsin do 
		(
			if isInternalRef obj then 
			(
				append irefObjList obj
			)
			else if classof obj == Gta_MILO then (
				for child in obj.children do (
					if classof child == GtaMloRoom then (
						for roomChild in child.children do (
							if isInternalRef roomChild then (
								append irefObjList roomChild
							)
						)
					)
				)
			)
		)
		
		local totalCount = irefObjList.count

		local deleteObjs = #()
	
		progressStart "Deleting iRef collision"
		
		local visibleIrefCols = #{}
		visibleIrefCols.count = irefObjList.count
		
		for i = 1 to irefObjList.count while (RsMapProgress i totalCount) do 
		(
			local obj = irefObjList[i]
			
			local hasCols = false
			local visibleCols = false
			
			for childObj in obj.children where (isValidNode childObj) and (getAttrClass childObj == "Gta Collision") do 
			(
				hasCols = true
				
				if not childObj.isHidden do 
				(
					visibleCols = true
				)
				
				append deleteObjs childObj
			)
			
			visibleIrefCols[i] = (hasCols and visibleCols) or ((not hasCols) and (not obj.isHidden))
		)
	
		progressEnd()
		
		delete deleteObjs

		if not RsMapExportCancelled do 
		(
			progressStart "Creating iRef collision"
		
			for i = 1 to irefObjList.count while (RsMapProgress i totalCount) do 
			(
				local obj = irefObjList[i]

				local srcObj = getIRefSource obj
				
				if (srcObj != undefined) and (getattrclass srcObj == "Gta Object") and (getattr srcObj idxDynamic == false) do 
				(					
					for collobj in srcObj.children where (getAttrClass collobj == "Gta Collision") do 
					(
						local newcollobj = copy collobj
						copyattrs collobj
						pasteattrs newcollobj
						local userpropbuff = getUserPropBuffer collobj
						setUserPropBuffer newcollobj (userpropbuff+"\r\n")

						newCollObj.isHidden = not visibleIrefCols[i]
						
						newCollObj.parent = obj
						newCollObj.transform = (collobj.transform * (inverse (collobj.parent.transform))) * obj.transform
						
						newcollobj.parent = undefined
						
						local collCont = RsGetObjContainer obj
						if (collCont != undefined) do 
						(
							collCont.addNodesToContent newcollobj
						)
						
						newcollobj.parent = obj
						
						-- ColMesh materials aren't copied with object, and can only be set when object is geometry:
						if (isKindOf newCollObj Col_Mesh) do 
						(
							col2mesh newCollObj
							newCollObj.material = collObj.material
							mesh2col newCollObj
						)						
					)		
				)
			)	
			
			progressEnd()
		)
		
		return (not RsMapExportCancelled)
	)

	--------------------------------------------------------------
	-- create a set of static bounds representing the xref objects
	--------------------------------------------------------------
	fn CreateStaticBoundsForXrefs modelsin = 
	(
		RsMapExportCancelled = false
		
		if not RsMapIsGeneric then return true
	
		local boundObjList = #()
		local boundObjNameList = #()
	
		local deleteset = #()
	
		for obj in modelsin where isRefObj obj do 
		(
			for childobj in obj.children where (isValidNode childobj) and (isKindOf childobj Col_Mesh)  do 
			(
				append deleteset childobj
			)
		)
		
		delete deleteset
		
		if not RsMapExportCancelled do 
		(
			if RsXrefCollisionLoading == false then return true
		
			i = 0
		
			progressStart "Creating Ref Collision"
		
			for objid = 1 to modelsin.count while (RsMapProgress objid modelsin.count) do 
			(			
				obj = modelsin[objid]
			
				if not isRefObj obj do 
				(			
					continue
				)				

				objectName = (obj.objectname + "_bmesh")
				
				boundObjIdx = finditem boundObjNameList objectName
				newobj = undefined
				
				if boundObjIdx == 0 then (
		
					filename = RsConfigGetNetworkStreamDir() + "maps/" + (RsRemovePathAndExtension obj.filename) + "/bound_import/" + obj.objectname + ".bnd"
					
					maplist = #()
					format "Creating bounds from BND file: % for object %\n" filename objectName
					newobj = CreateBoundsFromBndFile filename objectName maplist
					
					if newobj != undefined then (
					
						submatlist = #()
						
						for map in maplist do (
						
							local mat = trimLeft( map )
							mat = trimRight( mat )
							if ( "" == mat ) then
								continue -- skip
						
							mapTokens = filterstring mat "|"
		
							proc = ""
							roomID = 0
							popMult = 0
							flags = 0
						
							mtl = mapTokens[1]
							
							if mapTokens.count >= 6 then (
								proc = mapTokens[2]
								roomID = mapTokens[3] as integer
								popMult = mapTokens[4] as integer
								flags = mapTokens[5] as integer
															
								if proc == "0" then proc = ""
							)
							else if mapTokens.count == 5 then (
		
								roomID = mapTokens[2] as integer
								popMult = mapTokens[3] as integer
								flags = mapTokens[4] as integer
							)
							else (
								messagebox "Invalid material " + mat + " in bound file " + filename 
							)
						
							newsubmat = RexBoundMtl()		
							RexSetCollisionName newsubmat mtl
							RexSetProceduralName newsubmat proc
							RexSetCollisionFlags newsubmat flags
							
							append submatlist newsubmat
						)
						
						newmat = Multimaterial()
						newmat.numsubs = submatlist.count
						newmat.materialList = submatlist				
					
						col2mesh newobj
						newobj.material = newmat
						mesh2col newobj
					
						append boundObjList newobj
						append boundObjNameList objectName
					)
				
				) else (
					
					newobj = copy boundObjList[boundObjIdx]	
				)
				
				if newobj != undefined then (
		
					newobj.transform = obj.transform
					newobj.parent = obj
					
					i = i + 1
				)
			)
			
			progressEnd()
		)
		
		return (not RsMapExportCancelled)
	)	
	
	--------------------------------------------------------------
	-- Create our AP3 asset pack XML.
	--------------------------------------------------------------
	fn CreateCollisionAssetPack = 
	(		
		-- Find the bound files.
		local staticBoundsDir = RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/bound_static"
		local staticBoundsPathnames = getfiles (staticBoundsDir + "/*.bnd")
		local dynamicBoundsDir = RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/bound_dynamic"
		local dynamicBoundsPathnames = getfiles (dynamicBoundsDir + "/*.bnd")
		
		if ( 0 == staticBoundsPathnames.Count and 0 == dynamicBoundsPathnames.Count ) do
			return (true)		
		
		local result = true
		local collisionAssetPackScheduleFilename = RsConfigGetScriptDir() + RsMapName + "/assetpack_collision.xml"
		RsMakeSurePathExists collisionAssetPackScheduleFilename

		local collisionAssetPackScheduleFile = AP3AssetFile()
		collisionAssetPackScheduleFile.Initialize filename:collisionAssetPackScheduleFilename

		local dirTargetFolder = ( RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/")
		local mapCollisionZipFilename = ( dirTargetFolder + RsMapName + "_collision.zip" )

		local mapCollisionZip = ( collisionAssetPackScheduleFile.AddZipArchive mapCollisionZipFilename )
		
		-- Add the bound files.		
		for staticBoundsPathname in staticBoundsPathnames do 
		(					
			-- Add the .bnd file to our <map>_collision.zip
			collisionAssetPackScheduleFile.AddFile mapCollisionZip staticBoundsPathname
		)	
		for dynamicBoundsPathname in dynamicBoundsPathnames do 
		(		
			-- Add the .bnd file to our <map>_collision.zip
			collisionAssetPackScheduleFile.AddFile mapCollisionZip dynamicBoundsPathname
		)
		
		-- Write out AP3 Asset Pack Schedule file.
		collisionAssetPackScheduleFile.WriteXmlFile collisionAssetPackScheduleFilename
		
		if ( not collisionAssetPackScheduleFile.ValidateXml() ) then
		(
			gRsULog.LogError "Failed to create TXD Asset Pack XML.  See errors above."
			result = false
		)
		RsMapAddContent RsMapName collisionAssetPackScheduleFilename
		
		result
	)
		
	fn IsObjectCollisionObject objectIn =
	(
		if classof objectIn == Col_Mesh or classof objectIn == Col_Box or classof objectIn == Col_Sphere or classof objectIn == Col_Capsule or classof objectIn == Col_Cylinder then
		(
			return true
		)
		
		return false
	)
	
	fn IsClothCollision obj = 
	(
		(true == (getUserProp obj "clothcollision")) or (undefined!=(RsSceneLink.getParent LinkType_ClothCollision obj))
	)

	fn CollectCollisionObjectsRecursive objectIn collisionObjects =
	(
		if (IsObjectCollisionObject objectIn) and not (IsClothCollision objectIn) then
		(
			append collisionObjects objectIn
		)
		
		for childObject in objectIn.children do
		(
			CollectCollisionObjectsRecursive childObject collisionObjects
		)
	)
	
	fn GetExportableDynamicCollisionObjects objectsIn =
	(
		local collisionObjects = #()
		
		-- first pass, gather 'Gta Object's and collision objects
		for objectIn in objectsIn do
		(
			CollectCollisionObjectsRecursive objectIn collisionObjects
		)
		
		collisionObjects
	)
	
	fn SplitObjectContainerIntoBuckets objectsIn maxObjectsPerBucket = 
	(
		local SplitObjectContainerIntoBucketsMsg = ("SplitObjectContainerIntoBuckets (count: " + (objectsIn.count as string) + ")")
		gRsUlog.ProfileStart SplitObjectContainerIntoBucketsMsg
		
		buckets = #()
		
		workingBucket = #()
		workingBucketSize = 0
		
		for objectIn in objectsIn do
		(
			append workingBucket objectIn
			workingBucketSize += 1
			
			if workingBucketSize >= maxObjectsPerBucket then
			(
				append buckets workingBucket
				workingBucket = #()
				workingBucketSize = 0
			)
		)
		
		if workingBucketSize > 0 then
		(
			append buckets workingBucket
		)
		
		gRsUlog.ProfileEnd context:SplitObjectContainerIntoBucketsMsg
		return buckets
	)

	-- shared function between dynamic and static
	fn ExportCollisionBuckets objectsToExport rootExportDir dynamic:false exportAll:true =
	(
		
		RsMapExportCancelled = false
		local result = true
		
		with redraw off 
		(
			-- Remove existing boundary-build zipfiles
			-- Only delete all .bnd files if this is an export all
			if (not dynamic) or exportAll do (
				RsDeleteFiles (rootExportDir + "/*.?bn*")
				RsDeleteDirectory rootExportDir
			)
			-- Remove and recreate bounds-export directory:
			
			RsMakeSurePathExists rootExportDir

			if dynamic != RsMapIsGeneric do  
			(
				return true
			)
			
			local idxMeshTint = getattrindex "Gta Collision" "Use Procedural Tint"
			local idxSplitOnMtlLimit = getattrindex "Gta Collision" "Split On Material Limit"
	
			if dynamic then
				progressStart "Exporting dynamic collision:"
			else
				progressStart "Exporting static collision:"
			-- Make sure that progress-bar appears early on:
			progressUpdate 100.0
			progressUpdate 0.0

			-- We need to split into buckets to prevent the surface data that is passed over getting too big :-( JWR
			local bucketsToExport = SplitObjectContainerIntoBuckets objectsToExport 256
			
			for bucketToExport in bucketsToExport do
			(
				-- Selected objects are exported:
				select bucketToExport
				
				rexReset()
	
				local graphRoot = rexGetRoot()
				local surfaceProperty = ""
				local hasMeshTint = false
				local splitOnMaterialLimit = false
	
				for objectToExport in bucketToExport do 
				(		
					surfaceType = RsGetCollSurfaceTypeString objectToExport
	
					surfaceProperty += objectToExport.name + "="
					surfaceProperty += surfaceType + ":"
					
					if (getattr objectToExport idxMeshTint == true) do (hasMeshTint = true)
					if (getattr objectToExport idxSplitOnMtlLimit == true) do (splitOnMaterialLimit = true)
				)
		
				local bound = rexAddChild graphRoot "Root" "Bound"
				rexSetPropertyValue bound "OutputPath" rootExportDir
				rexSetPropertyValue bound "ExportAsBvhGeom" "true"
				rexSetPropertyValue bound "ApplyBulletShrinkMargin" "false"
				rexSetPropertyValue bound "ExportAsComposite" "false"
				rexSetPropertyValue bound "ForceExportAsComposite" "false"	
				rexSetPropertyValue bound "BoundExportWorldSpace" "true"
				rexSetPropertyValue bound "BoundExportMiloSpace" "false"
				rexSetPropertyValue bound "BoundSetToNodeName" "false"
				rexSetPropertyValue bound "BoundMaxPolyCountToCombine" (octreeSize as string)
				rexSetPropertyValue bound "SurfaceType" surfaceProperty
				rexSetPropertyValue bound "BoundHasSecondSurface" "false"
				rexSetPropertyValue bound "BoundSecondSurfaceMaxHeight" (1.0 as string)
				rexSetPropertyValue bound "BoundHasMeshTint" (hasMeshTint as string)
				rexSetPropertyValue bound "BoundForceComposite" "false"
				rexSetPropertyValue bound "BoundExportPrimitivesInBvh" "true"
				rexSetPropertyValue bound "BoundGenerateMaterials" "false"
				rexSetPropertyValue bound "BoundDisableOptimisation" "true"
				if not dynamic then
					rexSetPropertyValue bound "SplitOnMaterialLimit" (splitOnMaterialLimit as string)
				-- LPXO: While we support CG and NG DLC we can't export per vertex attribute data on bounds.
				-- If necessary we can hack the 32-bit revision of ragebuilder but this is the desired solution.
				
				-- Disabled until ragebuilder change is through QA.
				rexSetPropertyValue bound "BoundExportPerVertAttributes" (((not RsIsDLCProj()) as string))
				
				rexSetNodes bound

				-- Write out the bound-file(s) for this section:
				rexExport false (gRsUlog.Filename())

				if ( not gRsUlog.ValidateJustErrors() ) then
				(
					result = false
				)			
			)
			
			progressEnd()
		)

		((not RsMapExportCancelled) and result)	
	)
		
	fn ExportDynamicCollisionInner objectsIn exportAll:true = 
	(
		local objectsToExport = GetExportableDynamicCollisionObjects objectsIn
		print ("dyn coll objects: "+objectsToExport as string)
		local mapExportDir = RsConfigGetMapStreamDir() + "maps/" + RsMapName
		local rootExportDir = mapExportDir + "/bound_dynamic"
		ExportCollisionBuckets objectsToExport rootExportDir dynamic:true exportAll:exportAll
	)

	--------------------------------------------------------------
	-- exports the collision for the whole map section
	--------------------------------------------------------------
	fn ExportDynamicCollision modelsin exportAll:true = 
	(
		pushPrompt ("Exporting Dynamic Collision: " + RsMapName)

		local buildSuccess = true

		local contentXMLFileName = RsConfigGetScriptDir() + RsMapName + "/bnd_dynamic.xml"
		deletefile contentXMLFileName
		
		buildSuccess = ( ExportDynamicCollisionInner modelsin exportAll:exportAll )
		
		popPrompt()
		
		return buildSuccess
	)
	
	fn IsExclusivelyHiDetail obj = (
		local retval = false
		if (getUserProp obj "weapons" != false) and
			(getUserProp obj "mover" == false) and
			(getUserProp obj "foliage" != true) and
			(getUserProp obj "river" != true) and
			(getUserProp obj "horse" == false) then (
			retval = true
		)
		retval
	)

	--------------------------------------------------------------
	-- used to get the sets of objects for octree generation
	--------------------------------------------------------------
	fn ExportStaticCollisionCreateSets modelsin octreeSize = 
	(
		allObj = #()
	
		idxDontExport = getattrindex "Gta Object" "Dont Export"
		idxDontAddIpl = getattrindex "Gta Object" "Dont Add To IPL"
		idxForceCollision = getattrindex "Gta Object" "Force Collision Export"
		idxIPLgroup = getattrindex "Gta Object" "IPL Group"
		
		format "Modelsin: %\n" modelsin.count
		
		for obj in modelsin do 
		(
			local addObject = true
		
			case of 
			(
				(isKindOf obj Container):
				(				
					addObject = false -- Don't add the Container helper itself
					
					for o in obj.children do 
					(
						if ( "Gta Object" == GetAttrClass o ) do
						(
							if ( ( false == GetAttr o idxDynamic ) and ( false == GetAttr o idxFragment ) and
								( false == GetAttr o idxDontExport ) and ( false == GetAttr o idxDontAddIpl ) ) then
								append allObj o
						)
					)
				)
				(getattrclass obj == "Gta Object"):
				(
					if
					(
						(getattr obj idxDynamic == true) or 
						(getattr obj idxFragment == true) or 
						(
							(RsMapIsPatch == false) and ((getattr obj idxDontExport == true) or (getattr obj idxDontAddIpl == true))
						)
					)
					do 
					(
						addObject = false
					)
				)
				(not ((isKindOf obj Col_Mesh) or (isKindOf obj Col_Box) or (isKindOf obj Col_Sphere) or (isKindOf obj Col_Capsule) or (isInternalRef obj))):
				(
					addObject = false
				)
				((RsSceneLink.GetParent -1 obj) != undefined):
				(
					addObject = false
				)
			)
	
			if addObject do 
			(
				append allObj obj
			)
		)
		
		local hiobjs = #()
		local otherobjs = #()
		
		local IPLgroupNames = #()
		local IPLgroupObjs = #()
		
		format "allobj: %\n" allobj.count
		for obj in allObj do 
		(
			local ishiobj = false
			local isOtherObj = false
			if (isKindOf obj Col_Mesh) or (isKindOf obj Col_Box) or (isKindOf obj Col_Sphere) or (isKindOf obj Col_Capsule) then (
				if IsExclusivelyHiDetail obj do 
				(
					ishiobj = true
				)
			)
			for childobj in obj.children where (isKindOf childobj Col_Mesh) or (isKindOf childobj Col_Box) or (isKindOf childobj Col_Sphere) or (isKindOf childobj Col_Capsule) do 
			(	
				-- An "object" can be both if it has children of both types..   The filtering is done later
				if IsExclusivelyHiDetail childobj then
				(
					ishiobj = true
				)
				else (
					isOtherObj = true
				)
			)
			
			if isOtherObj == true then ishiobj = false
			
			local IPLgroupNum
			if (getattrclass obj == "Gta Object") do 
			(
				local IPLgroup = (getattr obj idxIPLgroup)
				--format "IPL Group: %\n" IPLgroup
				
				if (IPLgroup != undefined) and (IPLgroup != "undefined") do 
				(
					IPLgroupNum = findItem IPLgroupNames IPLgroup
					
					if (IPLgroupNum == 0) do 
					(
						append IPLgroupNames IPLgroup
						append IPLgroupObjs #()
						
						IPLgroupNum = IPLgroupNames.count
					)
				)
			)
			
			case of 
			(
				ishiobj:
				(
					append hiobjs obj
				)
				(IPLgroupNum != undefined):
				(
					append IPLgroupObjs[IPLgroupNum] obj
				)
				isOtherObj:
				(
					append otherobjs obj
				)
				default:
				(
					append otherobjs obj
				)
			)
		)
		
		format "hiobjs:	   %\n" hiobjs.count
		format "IPLGroupObjs: %\n" IPLgroupObjs.count
		format "otherobjs:	%\n" otherobjs.count
		
		local collisionSections = #()
		local globalSectionCount = 1
		local sectionObjs = CreateBinaryTree otherobjs octreeSize
		if ( false != sectionObjs ) then
		(
			-- Add sections to collision-sections list:
			format "sectionObjs:	%\n" sectionObjs.count
			for sectionNum = 1 to sectionObjs.count do 
			(
				append collisionSections (CollisionSection hiDetail:false objects:sectionObjs[sectionNum] name:(RsMapName + "_" + (globalSectionCount as string)) bucketName:"standard")
				globalSectionCount +=1
			)
		)
		
		-- Add IPL Group sections:
		for IPLgrpNum = 1 to IPLgroupObjs.count do 
		(
			local IPLgrpObjs = IPLgroupObjs[IPLgrpNum]
			
			format "octreeSize: %\n" octreeSize 
			local sectionObjs = CreateBinaryTree IPLgrpObjs octreeSize
			
			append collisionSections (CollisionSection hiDetail:false objects:IPLgrpObjs name:IPLgroupNames[IPLgrpNum] bucketName:"standard" isIPLgroup:true)
		)
		
		-- Get hisections, and add them if they were found:
		local hisectionObjs = CreateBinaryTree hiobjs octreeSize

		if (hisectionObjs != false) do 
		(
			for sectionNum = 1 to hisectionObjs.count do 
			(
				append collisionsections (CollisionSection hiDetail:true objects:hisectionObjs[sectionNum] name:(RsMapName + "_" + (globalSectionCount as string)) bucketName:"hidetail")
				globalSectionCount +=1
			)
		)
		
		return collisionsections		
	)
	
	fn IsObjectExportableStaticGtaObject objectIn =
	(
		idxDontExport = getattrindex "Gta Object" "Dont Export"
		idxDontAddIpl = getattrindex "Gta Object" "Dont Add To IPL"
		idxIPLgroup = getattrindex "Gta Object" "IPL Group"
		
		if ( "Gta Object" == GetAttrClass objectIn ) do
		(
			if ( ( false == GetAttr objectIn idxFragment ) and
				 ( false == GetAttr objectIn idxDontExport ) and 
				 ( false == GetAttr objectIn idxDontAddIpl ) ) then
			(
				return true
			)
		)
		
		return false
	)
	
	fn GetExportableStaticCollisionObjectsRecursive objectIn staticCollisionObjects clothExport:false =
	(
		if 	(IsObjectCollisionObject objectIn) and 
			(clothExport == (IsClothCollision objectIn)) then
		(
			appendIfUnique staticCollisionObjects objectIn
			-- we don't recurse past collision objects
		)
		else if ( "Gta Object" == GetAttrClass objectIn ) then
		(
			if IsObjectExportableStaticGtaObject objectIn then
			(
				for childObject in objectIn.children do
				(
					GetExportableStaticCollisionObjectsRecursive childObject staticCollisionObjects clothExport:clothExport
				)
			)
			-- otherwise we ignore the rest of the tree below here
		)
		else
		(
			-- continue the recursion
			for childObject in objectIn.children do
			(
				GetExportableStaticCollisionObjectsRecursive childObject staticCollisionObjects clothExport:clothExport
			)
		)
	)
	
	fn GetExportableStaticCollisionObjects objectsIn clothExport:false =
	(
		--The rules for which objects get exported are
		-- 1) All collision that is at the top level of the container/scene
		-- 2) All collision that is a child of a Gta Object as long as its flagged for export
		
		staticCollisionObjects = #()
		
		for objectIn in objectsIn do
		(
			GetExportableStaticCollisionObjectsRecursive objectIn staticCollisionObjects clothExport:clothExport
		)
		
		staticCollisionObjects
	)
	
	fn ExportStaticCollisionInner objectsIn = 
	(
		local objectsToExport = GetExportableStaticCollisionObjects objectsIn
		print ("static coll objects: "+objectsToExport as string)
		local mapExportDir = RsConfigGetMapStreamDir() + "maps/" + RsMapName
		local rootExportDir = mapExportDir + "/bound_static"
		ExportCollisionBuckets objectsToExport rootExportDir
	)

	fn ExportStaticClothCollisionInner objectsIn = 
	(
		local objectsToExport = GetExportableStaticCollisionObjects objectsIn clothExport:true
		print ("static cloth coll objects: "+objectsToExport as string)
		local mapExportDir = RsConfigGetMapStreamDir() + "maps/" + RsMapName
		local rootExportDir = mapExportDir + "/bound_cloth"
		ExportCollisionBuckets objectsToExport rootExportDir
	)

	--------------------------------------------------------------
	-- exports the collision for the whole map section, with xref bounds creation and deletion
	--------------------------------------------------------------
	fn ExportStaticCollision modelsin = 
	(
		pushPrompt "Exporting Static Collision"

		local contentXMLFileName = RsConfigGetScriptDir() + RsMapName + "/bnd_static.xml"
		deletefile contentXMLFileName
		
		local buildSuccess = true
		local curSel = selection as array

		if buildSuccess and not RsMapExportCancelled do  
		(
			buildSuccess = ( ExportStaticCollisionInner modelsin )  and ( ExportStaticClothCollisionInner modelsin )
		)
				
		-- Revert selection:
		clearSelection()
		select curSel
		
		popPrompt()

		return buildSuccess
	)

	--------------------------------------------------------------
	-- export the passed in models Bounds
	--------------------------------------------------------------
	fn ExportBounds modelsin exportAll:true selModelsIn:#()= 
	(	
		gc()
		local buildSuccess = True
		undo off 
		(
			-- Export static bnd files.
			local exportCollStatic = ("RexExport Static (count: " + (modelsin.count as string) + ")")
			gRsUlog.ProfileStart exportCollStatic
			buildSuccess = ExportStaticCollision modelsin
			gRsUlog.ProfileEnd context:exportCollStatic

			if buildSuccess and not RsMapExportCancelled do 
			(
				-- If not exporting all then only pass in the selected objects
				-- This was added because we need all objects passed in for static collision
				local exportModels = if exportAll then modelsin else selModelsIn
				local exportCollDynamicMsg = ("RexExport Dynamic (count: " + (exportModels.count as string) + ")")
				gRsUlog.ProfileStart exportCollDynamicMsg
				buildSuccess = ExportDynamicCollision exportModels exportAll:exportAll
				gRsUlog.ProfileEnd context:exportCollDynamicMsg
			)
		)
						
		-- Create asset pack data.
		if buildSuccess and not RsMapExportCancelled do
		(
			buildSuccess = CreateCollisionAssetPack()
		)
		
		return (buildSuccess and not RsMapExportCancelled)
	)

	
	mapped fn DoTestBounds parobj = 
	(
		for obj in parobj.children do 
		(
			if isKindOf obj Col_Mesh do 
			(
				col2mesh obj
				
				local checkvert, badVert
				for i = 1 to (getnumverts obj) do 
				(
					checkvert = getvert obj i
					badVert = false
					
					for j = 1 to 3 while (not badVert) do 
					(					
						badVert = (bit.isNan checkvert[j])
					)
					
					if badVert do 
					(					
						print ("vert " + (i as string) + " is bad in " + obj.name)
					)
				)
				
				mesh2col obj
			)
			
			DoTestBounds obj
		)
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	on btnTestBounds pressed do 
	(
		RsMapExportCancelled = false
		local buildSuccess = true
		
		local maps = RsMapGetMapContainers()
		
		for map in maps where (map.is_exportable()) while buildSuccess and (not RsMapExportCancelled) do
		(	
			buildSuccess = (RsPreExport map docheck:false) 
			
			if buildSuccess and (not RsMapExportCancelled) do 
			(
				buildSuccess = ExportBounds map.objects
			)
			
			if buildSuccess and (not RsMapExportCancelled) do 
			(
				DoTestBounds map.objects
			)
		)
		
		return (buildSuccess and (not RsMapExportCancelled))
	)

	on RsBoundsRoll rolledUp down do 
	(
		RsSettingWrite "rsmapbounds" "rollup" (not down)
	)
)

