--
-- File:: pipeline/export/maps/asset.ms
-- Description:: Map exporter data build functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 19 September 2011 (ruby_script_generator.ms)
-- Date:: 4 February 2013 (asset.ms; Asset Pipeline 3)
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
--filein "rockstar/export/settings.ms" loaded by pipeline/export/maps/globals.ms
-- globally loaded from startup: filein "pipeline/export/AP3Content.ms"
filein "pipeline/export/AP3Invoke.ms"
filein "pipeline/util/content.ms"
filein "pipeline/util/ruby.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

struct RsMapAsset
(

	--
	-- name: Build
	-- desc: Map resourcing and image building stage.
	--
	fn Build maps rebuild:false patchExport:false patchCollision:false selectedExport:false resource:true = (

		if ( maps.count <= 0 ) then
			return false

		local map_zip_filenames = #()
		local asset_pack_filenames = #()
		local map_zip_assets = #() -- for preview
		local maps_asset_pack_filename = RsConfigGetScriptDir() + "/assetpack_maps.xml"
		local mapAssetPackScheduleFile = AP3AssetFile()
		mapAssetPackScheduleFile.Initialize filename:maps_asset_pack_filename
		
		-- Only append the main map zip build if we're not doing a patch export.
		if ( not patchExport ) do
			append asset_pack_filenames maps_asset_pack_filename
		
		for map in maps do 
		(
			local mapname = (toLower map.name)
			local maxContentNode = RsContentTree.GetContentNode map.filename
			
			if (maxContentNode == undefined) then
			(
				gRsULog.LogError ("Can't find " + mapZipFilename + " in the content tree.")
				return false
			)
		
			if ( RsMapContentQueue.ContainsKey( mapname ) ) then
			(
				local mapZipFilename = RsContentTree.GetMapExportZipNode maxContentNode
				local mapZipArchive = mapAssetPackScheduleFile.AddZipArchive mapZipFilename.AbsolutePath forceCreateIfEmpty:true
				append map_zip_filenames mapZipFilename.AbsolutePath
				
				local analyseAsset = AP3AssetFile()
				analyseAsset.Initialize()
			
				-- Loop through our map content queue; as we do have some data in it thats relevant.
				-- (i.e. the selected objects).  Ensure those files get build by the pack processor.
				for asset_pack_filename in RsMapContentQueue.item[mapname] do 
				(
					append asset_pack_filenames asset_pack_filename

					analyseAsset.LoadAssetFile asset_pack_filename
					local targets = analyseAsset.GetCurrentAssetFiles()
					for mapFile in targets do
					(
						mapAssetPackScheduleFile.AddFile mapZipArchive  mapFile
						appendIfUnique map_zip_assets mapFile
					)
				)
				
				if ( selectedExport ) then
				(
					-- Selected export; we don't have content queued for everything so additionally we pack
					-- whatever is in the map cache directory.
					local mapCacheDirectory = RSConfigGetCacheDir() + "maps/" + map.name
					local mapFiles = getFiles (mapCacheDirectory + "/*.zip")
					
					for mapFile in mapFiles do 
					(
						mapAssetPackScheduleFile.AddFile mapZipArchive mapFile
						appendIfUnique map_zip_assets mapFile
					)
				)
			)
		)
		
		local build_success = true
		-- Write out AP3 Asset Pack Schedule file.
		mapAssetPackScheduleFile.WriteXmlFile maps_asset_pack_filename
		if ( not mapAssetPackScheduleFile.ValidateXml() ) then
		(
			gRsULog.LogError "Failed to create Map Asset Pack XML.  See errors above."
			build_success = false
		)
		else
		(		
			build_success = ( 0 == ( AP3Invoke.Pack asset_pack_filenames ) )
		)
		
		--if we are patching collision then we need to run the bounds processor
		if( patchCollision ) do
		(
			local unprocessed_collision_zips = #()
			local processed_collision_zips = #()
			
			for map_zip_asset in map_zip_assets do
			(
				local map_zip_asset_basename = RsRemovePathAndExtension map_zip_asset
				local collision_token_index = findString map_zip_asset_basename "_collision"
				
				if( collision_token_index != undefined ) do
				(
					local map_zip_name = Substring map_zip_asset_basename 1 (collision_token_index - 1)
					append unprocessed_collision_zips map_zip_asset
					
					local processed_bounds_output_directory = RsConfigGetToolsDir() + "tmp\\preview_collision\\" + map_zip_name + "\\processed_bounds"
					
					local args = #()
					append args "-nopopups"
					append args "-rebuild"
					append args ("-input " + map_zip_asset)
					append args ("-output " + processed_bounds_output_directory)
					append args "-proc RSG.Pipeline.Processor.Map.CollisionPreview"
					
					build_success = ( 0 == ( AP3Invoke.Convert #() args:args debug:true ) )
					
					local processed_bounds_pathnames = RsFindFilesRecursive processed_bounds_output_directory "*.*"
					for processed_bounds_pathname in processed_bounds_pathnames do
					(
						append processed_collision_zips processed_bounds_pathname
					)
				)
			)
			
			-- remove unprocessed data
			for unprocessed_collision_zip in unprocessed_collision_zips do
			(
				local unprocessed_collision_zip_index = findItem map_zip_assets unprocessed_collision_zip
				deleteItem map_zip_assets unprocessed_collision_zip_index
			)
			
			-- add unprocessed data
			for processed_collision_zip in processed_collision_zips do
			(
				append map_zip_assets processed_collision_zip
			)
		)
		
		if ( build_success and resource and (not patchExport) ) then
		(
			local args = #("--nopopups")	
			if (RsOverridePlatforms != undefined and RsAutomatedExport) do 
			(
				append args "--platforms"
				append args RsOverridePlatforms
			)
			
			build_success = ( 0 == ( AP3Invoke.Convert map_zip_filenames args:args ) )
		)
		else if ( build_success and resource and patchExport ) then
		(
			local args = #()
			append args "--nopopups"
			append args "--preview"
			
			if (RsOverridePlatforms != undefined and RsAutomatedExport) do 
			(
				append args "--platforms"
				append args RsOverridePlatforms
			)
			
			build_success = ( 0 == ( AP3Invoke.Convert map_zip_assets args:args ) )
		)
		
		build_success
	),

	--
	-- name: ExtractMapZipsToCache
	-- desc: Gets a list of maps that are having something export selected, and creates a ruby script to run 
	-- 		 which checks the timestamps from the map zip and extracts it to the cache if it's more recent
	--
	fn ExtractMapZipsToCache maps =
	(
		local success = true
		
		if maps.count <= 0 do return false
		
		for map in maps do
		(
			local mapContentNode = RsContentTree.GetContentNode map.filename
			
			if (mapContentNode == undefined) then
			(
				gRsULog.LogError ("Can't find " + map.filename + " in the content tree.")
				return false
			)
			
			local mapZipContentNode = RsContentTree.GetMapExportZipNode mapContentNode
			
			if (mapZipContentNode == undefined) then
			(
				gRsULog.LogError ("Can't find export zip in the content tree for: " + map.filename)
				return false
			)
			
			local mapType = RsContentTree.GetMapType mapContentNode
			local mapName = tolower map.name
			local mapZipFilename = mapZipContentNode.AbsolutePath
			local cacheDir = (RsConfigGetCacheDir() + "maps/" + mapName + "/")
			
			gRsPerforce.sync mapZipFilename
			
			if (RsFileExists mapZipFilename) then
			(
				zipClass = dotNetClass "RSG.Pipeline.Services.Zip"
				if (zipClass.ExtractNewer mapZipFilename cacheDir == false) then
				(
					success = false
					gRsULog.LogError ("Error extracting map zip filename: " + mapZipFilename)
				)
				
				local boundsZipPathname = RsConfigGetCacheDir() + "maps/" + mapName + "/" + mapName + "_collision.zip"
				
				if (RsFileExists boundsZipPathname) then
				(
					local boundsTargetDirectory = ""
					
					if( (mapType == "interior") or (mapType == "props") ) then
						boundsTargetDirectory = RsConfigGetCacheDir() + "maps/" + mapName + "/bound_dynamic"
					else
						boundsTargetDirectory = RsConfigGetCacheDir() + "maps/" + mapName + "/bound_static"
					
					RsMakeSurePathExists boundsTargetDirectory
					
					if (zipClass.ExtractAll boundsZipPathname boundsTargetDirectory true == false) then
					(
						success = false
						gRsULog.LogError ("Error extracting collision zip filename: " + boundsZipPathname)
					)
				)
			)
			else
			(
				success = false
				gRsULog.LogError ("File doesn't exist locally, skipping extract to cache: " + mapZipFilename)
			)
		)
		
		return success
	)
)


-- pipeline/export/maps/asset.ms
