--
-- File:: pipeline/export/maps/occlusion.ms
-- Description:: Map exporter occlusion related functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 4 October 2011
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/AP3Asset.ms"
filein "pipeline/export/maps/globals.ms"
filein "pipeline/util/xml.ms"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

--
-- name: RsMapOcclusion
-- desc: Map occlusion-related functions.
--
struct RsMapOcclusion
(
	--
	-- name: CreateAssetPack
	-- desc: Export an Asset Pack list of occlusion meshes in
	--       this map to XML; picked up by processing pipeline later.
	--
	fn CreateAssetPack map = 
	(	
		result = true
		-- AJM:	Temp function
		-- name: CreateOcclusionXmlFiles
		-- desc: Temporary function to convert .mesh files to xml files for packing
		--       to be used by serialiser, until we directly pass the mesh files and get
		--		 the serialiser to handle them.
		fn CreateOcclusionXmlFiles map occlusionObjects = 
		(
			if (undefined != map) do
			(

				format "CreateAssetPack->CreateOcclusionXmlFiles\n" 
				local occlusionXmlFilename = ( RsConfigGetScriptDir() + map.name + "/occlusion.xml" )
				RsMakeSurePathExists( occlusionXmlFilename )


				format "occlusionXmlFilename: %\n" occlusionXmlFilename

				-- Initialalise XML Document
				local occlXml = XmlDocument()
				occlXml.init()

				local occlXmlRoot = occlXml.CreateElement( "occlusion" )
				occlXml.document.AppendChild( occlXmlRoot )
				local occlXmlMeshes = occlXml.CreateElement( "occludeMeshes" )
				--local occlXmlBoxes = occlXml.CreateElement( "occludeBoxes" )

				-- Loop through our occlusion meshes.
				format "-- occlusionObjects --\n"
				format " Count = %\n" occlusionObjects.count
				for item in occlusionObjects do(

					format "%\n" item.name
				)
				format "--\n\n"


				for m in occlusionObjects do
				(
					local meshElem = occlXml.CreateElement( m.name )
					occlXmlMeshes.AppendChild( meshElem )
				)		

				occlXmlRoot.AppendChild( occlXmlMeshes )
				
				occlXml.Save occlusionXmlFilename

				local rbFileName = RsConfigGetScriptDir() + "maps/" + map.name + "_occlusion_mesh_to_xml.rb"
				RsMakeSurePathExists rbFileName

				local rbFile = openfile rbFileName mode:"w+"

				format "require 'pipeline/config/projects'\n" to:rbFile
				format "require 'pipeline/config/project'\n" to:rbFile
				format "require 'pipeline/os/path'\n" to:rbFile
				format "require 'pipeline/projectutil/data_convert'\n" to:rbFile
				format "require 'pipeline/projectutil/data_zip'\n" to:rbFile
				format "require 'pipeline/resourcing/convert'\n" to:rbFile
				format "require 'pipeline/resourcing/util'\n" to:rbFile
				format "require 'pipeline/resourcing/converters/converter_map_collision'\n" to:rbFile
				format "include Pipeline\n\n" to:rbFile

				format "config = Pipeline::Config::instance( )\n" to:rbFile
				format "project = config.project\n" to:rbFile
				format "convert = Resourcing::ConvertSystem::instance()\n" to:rbFile
				format "convert.setup( project, nil, true, true, false )\n" to:rbFile		
				format "component_group = Content::Group.new( 'map_content_group' )\n\n" to:rbFile

				-- Hack for mpbusiness dlc
				--format "stream_dir = OS::Path.combine( project.cache, 'raw', 'script', '%' )\n" map.name to:rbFile
				format "stream_dir = OS::Path.combine( '%', 'raw', 'script', '%' )\n" gRsProject.Cache map.name to:rbFile
				format "occl_content_filename = OS::Path::combine( stream_dir, 'occlusion.xml' )\n" to:rbFile
				format "map_name = \"%\"\n" map.name to:rbFile
				--format "occl_content = Content::MapOcclusion::new( occl_content_filename, project, map_name )\n" to:rbFile
				format "occl_content = Content::MapOcclusion::new( occl_content_filename, project, map_name, '%' )\n" gRsProject.Cache to:rbFile
				format "component_group.add_child( occl_content )\n" to:rbFile

				format "component_group.children.each do | content_group |\n" to:rbFile
				format "\tnext unless ( content_group.methods.include?( 'build' ) )\n" to:rbFile
				format "\tcontent_group.build()\n" to:rbFile
				format "end\n" to:rbFile

				close rbFile

				-- Run the Ruby resourcing script for the animation data.
				if ( RsRunRubyScript rbFileName != 0 ) then 
				(
					gRsULog.LogError "Failed to convert occlusion meshes to xml for serialiser"
				)
			)
		)
	
		local idxIsOcclusion = ( GetAttrIndex "Gta Object" "Is Occlusion" )
		-- Loop through our occlusion meshes; occlusion boxes are handled by the 
		-- IMAP serialiser.
		local occlusionObjects = #()
		for o in map.objects do
		(
			if ( ( "Gta Object" == GetAttrClass o ) and ( true == GetAttr o idxIsOcclusion ) ) then
			(
				format "o.name: %\n" o.name
				append occlusionObjects o
			)
		)	
		
		-- We do not create Asset Pack XML for empty content.
		if ( 0 == occlusionObjects.Count ) do
			return true
			
		-- AJM: Temp until we get the serialiser to handle occlusion mesh objects directly
		CreateOcclusionXmlFiles map occlusionObjects
		-- AJM: Temp until we get the serialiser to handle occlusion mesh objects directly
	
		local occlusionAssetPackScheduleFilename = RsConfigGetScriptDir() + map.name + "/assetpack_occlusion.xml"
		RsMakeSurePathExists occlusionAssetPackScheduleFilename
	
		local occlusionAssetPackScheduleFile = AP3AssetFile()
		occlusionAssetPackScheduleFile.Initialize filename:occlusionAssetPackScheduleFilename
		local dirTargetFolder = ( RsConfigGetMapStreamDir() + "maps/" + map.name + "/")
		local mapOcclusionZipFilename = ( dirTargetFolder + map.name + ".occl.zip" )
		local mapOcclusionZip = ( occlusionAssetPackScheduleFile.AddZipArchive mapOcclusionZipFilename )
		
		-- Loop through our occlusion meshes; occlusion boxes are handled by the 
		-- IMAP serialiser.
		for o in occlusionObjects do
		(
			-- AJM: This will get changed back to taking in the .mesh files directly
			local occlusionMeshFilename = ( dirTargetFolder + "occlusion/" + o.name + ".xml")

			if not (RsFileExists occlusionMeshFilename) then(

				 format "occlusionMeshFilename: %\n" occlusionMeshFilename
				 format "occlusionObjects: %\n" occlusionObjects
				 gRsULog.LogError ("File Not Found: " + (occlusionMeshFilename as string))
			)

			occlusionAssetPackScheduleFile.AddFile mapOcclusionZip (toLower occlusionMeshFilename)
		)
				
		-- Write out AP3 Asset Pack Schedule file.
		occlusionAssetPackScheduleFile.WriteXmlFile occlusionAssetPackScheduleFilename

		if ( not occlusionAssetPackScheduleFile.ValidateXml() ) then
		(
			gRsULog.LogError "Failed to create TXD Asset Pack XML.  See errors above."
			result = false
		)
		RsMapAddContent (toLower map.name) occlusionAssetPackScheduleFilename
		result		
	),
	
	--
	-- name: ExportAll
	-- desc: Export all occlusion meshes for a particular RsMapContainer.
	--
	-- Note: RsOcclusionBox objects are already written into SceneXml so they
	--       are serialised from there.
	--
	fn ExportAll map =
	(
		local idxIsOcclusion = ( GetAttrIndex "Gta Object" "Is Occlusion" )
		local exportDir = ( RsConfigGetMapStreamDir() + "maps/" + map.name + "/occlusion" )
				
		for o in map.objects 
			where ( ( "Gta Object" == GetAttrClass o ) and ( true == GetAttr o idxIsOcclusion ) )  do
		(
			rexReset()
			graphRoot = rexGetRoot()			
	
			meshobj = rexAddChild graphRoot "mesh" "Mesh"
			rexSetPropertyValue meshobj "OutputPath" exportDir
			rexSetPropertyValue meshobj "ExportTextures" "false"
			rexSetPropertyValue meshobj "TextureOutputPath" (RsConfigGetMapsTexDir())
			rexSetPropertyValue meshobj "MeshAsAscii" "false"
			rexSetPropertyValue meshobj "MeshSkinOffset" "false"
			rexSetPropertyValue meshobj "MeshExportTextureList" "false"
			rexSetPropertyValue meshobj "MeshRemoveDegenerates" "false"
			rexSetPropertyValue meshobj "MeshExportWorldSpace" "true"
			
			select o
			rexSetNodes meshobj
			
			RsRexExportWithReport()
		)
		
		true
	)
	
) -- RsMapOcclusion struct

-- pipeline/export/maps/occlusion.ms
