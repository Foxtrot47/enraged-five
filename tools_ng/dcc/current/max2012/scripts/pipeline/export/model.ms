--
-- File:: pipeline/export/model.ms
-- Description:: Simple model mesh and material exporter.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 14 November 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/cloth.ms"
filein "pipeline/util/drawablelod.ms"


idxMeshTint = getattrindex "Gta Collision" "Use Procedural Tint"
idxSplitOnMtlLimit = getattrindex "Gta Collision" "Split On Material Limit"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------


--
-- name: RsModelExportMesh
-- desc: Export a simple model mesh, supports fragments and cloth.
--
fn RsModelExportMesh obj asciiMesh:false &dir: = 
(
	local modelDir = ( RsModelOutputDirectory + "/" + obj.name )
	if unsupplied != dir then
		dir = modelDir

	RsDeleteFiles (modelDir + "*.*")

	rexReset()
	graphRoot = rexGetRoot()

	-------------------------------------------------------------------------
	-- Geometry Export
	-------------------------------------------------------------------------
	
	local entity = rexAddChild graphRoot "entity" "Entity"
	rexSetPropertyValue entity "OutputPath" modelDir	
	rexSetPropertyValue entity "SkipBoundSectionOfTypeFile" "true"
	rexSetPropertyValue entity "EntityWriteBoundSituations" "true"
	rexSetPropertyValue entity "EntityFragmentWriteMeshes" "true"

	RsTotalSurfProperty = ""
	RsSetupFragBounds obj entity modelDir
	rexSetPropertyValue entity "SurfaceType" RsTotalSurfProperty		

	select obj
	
	if "Gta Object"==GetAttrClass obj then
	(
		local idxFragment = ( GetAttrIndex "Gta Object" "Is Fragment" )	
		local lodGroupCount = 0 -- added due to changes to RsLodDrawable_SetUserProps
		
		if ( getattr obj idxFragment == true ) then (

			if (RsCloth.IsCloth obj) then 
				RsCloth.IsValidCloth obj

			-- RAGE Drawable LOD Support
			
			local drawobjs = RsLodDrawable_SetUserProps obj &lodGroupCount
			selectMore drawobjs

			mesh = rexAddChild entity "mesh" "Mesh"
			rexSetPropertyValue mesh "OutputPath" modelDir
			rexSetPropertyValue mesh "ExportTextures" "true"
			rexSetPropertyValue mesh "TextureOutputPath" modelDir
			rexSetPropertyValue mesh "MeshForSkeleton" "true"
			rexSetPropertyValue mesh "MeshSkinOffset" "false"
			rexSetPropertyValue mesh "MeshAsAscii" (asciiMesh as String)
			gRsUlog.logMessage ("REX EXPORT MESHES "+(selection as array) as string)
			rexSetNodes mesh

			if ( undefined != obj.modifiers[#Skin] ) then
			(
				skel = rexAddChild entity "skeleton" "Skeleton"
				rexSetPropertyValue skel "OutputPath" modelDir
				rexSetPropertyValue skel "SkeletonUseBoneIDs" "true"
				rexSetPropertyValue skel "SkeletonRootAtOrigin" "true"
				rexSetNodes skel
			)

			fragment = rexAddChild entity "fragment" "Fragment"
			rexSetPropertyValue fragment "OutputPath" modelDir
			rexSetNodes fragment

			-- Cloth Support (if flagged)
			if ( RsCloth.IsCloth obj ) then
				RsCloth.SetupEnvClothForExport obj entity mesh
		) 
		else 
		(
			-- RAGE Drawable LOD Support
			local drawobjs = RsLodDrawable_SetUserProps obj &lodGroupCount
			selectMore drawobjs
			
			mesh = rexAddChild entity "mesh" "Mesh"
			rexSetPropertyValue mesh "OutputPath" modelDir
			rexSetPropertyValue mesh "ExportTextures" "true"
			rexSetPropertyValue mesh "TextureOutputPath" modelDir
			rexSetPropertyValue mesh "MeshAsAscii" (asciiMesh as String)
			gRsUlog.logMessage ("REX EXPORT MESHES "+(selection as array) as string)
			rexSetNodes mesh

			if ( undefined != obj.modifiers[#Skin] ) then
			(
				skel = rexAddChild entity "skeleton" "Skeleton"
				rexSetPropertyValue skel "OutputPath" modelDir
				rexSetPropertyValue skel "SkeletonUseBoneIDs" "true"
				rexSetPropertyValue skel "SkeletonRootAtOrigin" "true"
				rexSetNodes skel
			)
		)
	)
	else if "Gta Collision"==GetAttrClass obj then
	(
		local graphRoot = rexGetRoot()
		local surfaceProperty = ""
		local surfaceType = RsGetCollSurfaceTypeString obj

		surfaceProperty += obj.name + "="
		surfaceProperty += surfaceType + ":"
		
		local hasMeshTint = getattr obj idxMeshTint
		local splitOnMaterialLimit = getattr obj idxSplitOnMtlLimit
		local octreeSize = 32000
		
		local bound = rexAddChild graphRoot "Root" "Bound"
		rexSetPropertyValue bound "OutputPath" modelDir
		rexSetPropertyValue bound "ExportAsBvhGeom" "true"
		rexSetPropertyValue bound "ApplyBulletShrinkMargin" "false"
		rexSetPropertyValue bound "ExportAsComposite" "false"
		rexSetPropertyValue bound "ForceExportAsComposite" "false"	
		rexSetPropertyValue bound "BoundExportWorldSpace" "true"
		rexSetPropertyValue bound "BoundExportMiloSpace" "false"
		rexSetPropertyValue bound "BoundSetToNodeName" "false"
		rexSetPropertyValue bound "BoundMaxPolyCountToCombine" (octreeSize as string)
		rexSetPropertyValue bound "SurfaceType" surfaceProperty
		rexSetPropertyValue bound "BoundHasSecondSurface" "false"
		rexSetPropertyValue bound "BoundSecondSurfaceMaxHeight" (1.0 as string)
		rexSetPropertyValue bound "BoundHasMeshTint" (hasMeshTint as string)
		rexSetPropertyValue bound "BoundForceComposite" "false"
		rexSetPropertyValue bound "BoundExportPrimitivesInBvh" "true"
		rexSetPropertyValue bound "BoundGenerateMaterials" "false"
		rexSetPropertyValue bound "BoundDisableOptimisation" "true"
		rexSetPropertyValue bound "SplitOnMaterialLimit" (splitOnMaterialLimit as string)
		if RsBoundCheckErrors then
		(
			rexSetPropertyValue bound "BoundCheckFlags" "BOUNDCHECK_ERROR:1" -- BOUNDCHECK_ISTHIN:50|BOUNDCHECK_BADNORMAL:0.5|BOUNDCHECK_BADNEIGHBOR:1
		)
		
		rexSetNodes bound

		-- Write out the bound-file(s) for this section:
		rexExport false (gRsUlog.Filename())
	)

	select obj

	rexExport false (gRsUlog.filename())

	return true	
)


--
-- name: RsModelExportGroup
-- desc: Export array of objects.
--
fn RsModelExportGroup objs asciiMesh:false simulate:false dirs: = 
(
	gRsUlog.init "Simple model export" appendToFile:false
	
	if simulate then
		rexDisableSerialisation true
	
	-- curcial try catch, as serialisation HAS to be switched on afterwards
	progressStart ("Export simulation for "+objs.count as string+" objects")
	try
	(
		if unsupplied==dirs then
			dirs = #()
		
		local noErrors = true
		local counter = 0
		for o in objs do
		(
			if ( 	"Gta Object" != ( GetAttrClass o ) and 
					"Gta Collision" != ( GetAttrClass o ) ) then
				continue
			
			local dontExportIdx = getAttrIndex "Gta Object" "Dont Export"
			local dontExportAttr = getAttr o dontExportIdx
			
			-- Don't export low detail models. They should already get picked up with the high detail mesh export.
			local lodParent = RsLodDrawable_GetHigherDetailModel o
			if (lodParent != undefined) then
				continue

			if ((GetAttrClass o) == "Gta Object" and dontExportAttr != undefined and dontExportAttr) then
				continue
					
			format "Export Model: %\n" o.name
			local dir = undefined
			if not RsModelExportMesh o asciiMesh:asciiMesh dir:&dir then
				noErrors = false
			
			if (dir != undefined) do 
			(
				appendIfUnique dirs dir
			)
			gc light:true
			if not (progressUpdate ( (counter as float/objs.count) * 100 )) then
			(
				gRsUlog.LogError ("User aborted")
				exit
			)
			counter+=1
		)
		if not gRsULog.Validate() then
		(
			progressEnd()
			return false
		)
	)
	catch(
		gRsUlog.LogError ("***"+getCurrentException()+"***")
		progressEnd()
		return gRsUlog.Validate()
	)
	progressEnd()
	if simulate then
		rexDisableSerialisation false
	
	return noErrors
)

-- pipeline/export/model.ms
