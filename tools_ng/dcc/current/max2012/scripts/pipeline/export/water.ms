--
-- File:: pipeline/export/water.ms
-- Description:: Water export/import functions.
--
-- Author:: Greg Smith <greg@rockstarnorth.com>
-- Date:: 30/3/2004
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 31 March 2010
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/xml.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global RsWaterErrors = #()
global RsWaterErrorObjs = #()
global gAlphaSaveOrder = #(2,3,0,1)
global ignoreInWaterOcclusionRenderingIdx = getattrindex "Gta Water" "Ignore in water occlusion rendering"
-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------

--
-- rollout: RsWaterErrorsRoll
-- desc:
--
rollout RsWaterErrorsRoll "Water Export Errors" 
(
	---------------------------------------------------------------------------
	-- UI
	---------------------------------------------------------------------------
	listbox lstErrors  	"Errors:" height:14 items:RsWaterErrors

	---------------------------------------------------------------------------
	-- Events
	---------------------------------------------------------------------------
	
	--
	-- event: lstErrors item selected
	-- desc: Select related object that generated the error.
	--
	on lstErrors selected item do (

		if ( undefined != RsWaterErrorObjs[item] ) then (
		
			if isdeleted RsWaterErrorObjs[item] == false then (
				select RsWaterErrorObjs[item]
				max zoomext sel
			)
		)
	)
)

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: ExportWaterTestVert
-- desc:
--
fn ExportWaterTestVert vertTest = (

	for i = 1 to 2 do (

		modval = abs (mod vertTest[i] 2.0)
		
		if modval > 0.0001 then (
			modval = abs(modval - 2.0)
		)

		if modval >= 0.0005 and modval <= 1.9995 then (
		
			print modval
		
			return false
		)
	)

	return true
)


--
-- name: ExportWaterTest
-- desc:
--
fn ExportWaterTest = (

	RsWaterErrors = #()
	RsWaterErrorObjs = #()

	for obj in rootnode.children do (
		
		if ( 	getattrclass obj == "Gta Water" or 
-- 				classof obj == RsWaterWave or 
-- 				classof obj == GtaWaterCalmer or 
				classof obj == RSRiverHelper ) then (
		
			failed = false
		
			width = obj.width / 2.0
			length = obj.length / 2.0
			
			vert1 = [-width,length,0]
			vert2 = [width,length,0]
			vert3 = [-width,-length,0]
			vert4 = [width,-length,0]
			
			vert1 = vert1 * obj.objecttransform
			vert2 = vert2 * obj.objecttransform
			vert3 = vert3 * obj.objecttransform
			vert4 = vert4 * obj.objecttransform
			
			if(obj.rotation != (quat 0 0 0 1)) then
			(
				append RsWaterErrors (obj.name + " is rotated which is not supported at the moment.")
				append RsWaterErrorObjs obj
				failed = true
			)
			if ExportWaterTestVert vert1 == false and failed == false then (
				append RsWaterErrors ("vert1 of " + obj.name + " is not on 2m boundary in xy")
				append RsWaterErrorObjs obj
				failed = true
			)
			if ExportWaterTestVert vert2 == false and failed == false then (
				append RsWaterErrors ("vert2 of " + obj.name + " is not on 2m boundary in xy")
				append RsWaterErrorObjs obj
				failed = true
			)
			if ExportWaterTestVert vert3 == false and failed == false then (
				append RsWaterErrors ("vert3 of " + obj.name + " is not on 2m boundary in xy")
				append RsWaterErrorObjs obj
				failed = true
			)
			if ExportWaterTestVert vert4 == false and failed == false then (
				append RsWaterErrors ("vert4 of " + obj.name + " is not on 2m boundary in xy")
				append RsWaterErrorObjs obj
				failed = true
			)
		)
	)
	
	if RsWaterErrorObjs.count == 0 then (
		return true
	)
	
	try CloseRolloutFloater RsMapWaterErrorsUtil catch()
	
	RsMapWaterErrorsUtil = newRolloutFloater "Water Errors" 450 260 1 136
	addRollout RsWaterErrorsRoll RsMapWaterErrorsUtil
	RsWaterErrorsRoll.lstErrors.items = RsWaterErrors	
	
	return false
)


--
-- name: ExportWaterObjFile
-- desc:
--
fn ExportWaterObjFile filename legacyFormat:false = (

	if ExportWaterTest() == false then (
		return 0
	)

	if filename == undefined do 
		return false
	
	gRsPerforce.sync filename
	gRsPerforce.edit filename

	local xmlDoc = undefined
	local waterData = undefined
	local waterquads = undefined
	local calmingquads = undefined
	local waveQuads = undefined
	
	if not legacyFormat then
	(
		xmlDoc = XmlDocument()
		xmlDoc.init()
		waterData = xmlDoc.createElement "WaterData" appendTo:xmlDoc.document
	)
	else
	(
		file = createfile filename

		if file == undefined do (
			messagebox "Cannot open file" title:"Water Object File Export"
			return false
		)	
	)

	for obj in rootnode.children do 
	(
		-- both types
		if ( 	getattrclass obj == "Gta Water" or 
				classof obj == RsWaterWave or 
				classof obj == GtaWaterCalmer  
				--classof obj == RSRiverHelper 
				) then 
		(
			width = abs(obj.width) / 2.0
			length = abs(obj.length) / 2.0
			
			local isWater = (classof obj == GtaWater)
			local isCalmer = (classof obj == GtaWaterCalmer)
			local isWave = (classof obj == RSWaterWave)
			
			-- AJM: IsRiver attribute has been removed.  Leaving this script here though just incase river
			-- helpers are back in the good books in future.
			local isRiver = false

			local isVisible = true
			local isLimitedlength = false
			local isStandard = false
			local isCalming = false
			
			-- No attribute class for water waves
			if ( not (isWave or isRiver) ) do
			(
				isVisible = not isWater or getattr obj (getattrindex "Gta Water" "Visible")
				isLimitedlength = not isWater or getattr obj (getattrindex "Gta Water" "Limited Depth")
				isStandard = not isWater or getattr obj (getattrindex "Gta Water" "Standard")
				isCalming = not isWater
				valCalmness = if isWater then 0.0 else obj.calming

				vert1flowX = getattr obj (getattrindex "Gta Water" "Point1 Flow X")
				vert1flowY = getattr obj (getattrindex "Gta Water" "Point1 Flow Y")
				vert1strengthX = getattr obj (getattrindex "Gta Water" "Point1 Big Wave Strength")
				vert1strengthY = getattr obj (getattrindex "Gta Water" "Point1 Small Wave Strength")
				vert2flowX = getattr obj (getattrindex "Gta Water" "Point2 Flow X")
				vert2flowY = getattr obj (getattrindex "Gta Water" "Point2 Flow Y")
				vert2strengthX = getattr obj (getattrindex "Gta Water" "Point2 Big Wave Strength")
				vert2strengthY = getattr obj (getattrindex "Gta Water" "Point2 Small Wave Strength")
				vert3flowX = getattr obj (getattrindex "Gta Water" "Point3 Flow X")
				vert3flowY = getattr obj (getattrindex "Gta Water" "Point3 Flow Y")
				vert3strengthX = getattr obj (getattrindex "Gta Water" "Point3 Big Wave Strength")
				vert3strengthY = getattr obj (getattrindex "Gta Water" "Point3 Small Wave Strength")
				vert4flowX = getattr obj (getattrindex "Gta Water" "Point4 Flow X")
				vert4flowY = getattr obj (getattrindex "Gta Water" "Point4 Flow Y")
				vert4strengthX = getattr obj (getattrindex "Gta Water" "Point4 Big Wave Strength")
				vert4strengthY = getattr obj (getattrindex "Gta Water" "Point4 Small Wave Strength")

				outputFlags = 0

				if isVisible == true then (
					outputFlags = outputFlags + 1
				)

				if isLimitedlength == true then (
					outputFlags = outputFlags + 2
				)

				if isStandard == true then (
					outputFlags = outputFlags + 4
				)

				if isCalming == true then (
					outputFlags = outputFlags + 8
				)
			)
			
			if legacyFormat then
			(
			
				vert1 = [-width,length,0]
				vert2 = [width,length,0]
				vert3 = [-width,-length,0]
				vert4 = [width,-length,0]
				
				vert1 = vert1 * obj.objecttransform
				vert2 = vert2 * obj.objecttransform
				vert3 = vert3 * obj.objecttransform
				vert4 = vert4 * obj.objecttransform
				if isWater and obj.type > 0 then (
					format "% % % % % % %   " vert1[1] vert1[2] vert1[3] vert1flowX vert1flowY vert1strengthX vert1strengthY to:file
					format "% % % % % % %   " vert2[1] vert2[2] vert2[3] vert2flowX vert2flowY vert2strengthX vert2strengthY to:file
					format "% % % % % % % " 	vert3[1] vert3[2] vert3[3] vert3flowX vert3flowY vert3strengthX vert3strengthY to:file
				) else (		
					format "% % % % % % %   " vert1[1] vert1[2] vert1[3] vert1flowX vert1flowY vert1strengthX vert1strengthY to:file
					format "% % % % % % %   " vert2[1] vert2[2] vert2[3] vert2flowX vert2flowY vert2strengthX vert2strengthY to:file
					format "% % % % % % %   " vert3[1] vert3[2] vert3[3] vert3flowX vert3flowY vert3strengthX vert3strengthY to:file
					format "% % % % % % % " 	vert4[1] vert4[2] vert4[3] vert4flowX vert4flowY vert4strengthX vert4strengthY to:file
				)
				format "% % %\n" outputFlags valCalmness (if isWater and obj.type>=0 then obj.type else 0) to:file
				
			) -- legacy format
			else
			(
				local typeElem = undefined
				if isWater or isRiver then
				(
					if undefined==waterQuads then
						waterQuads = xmlDoc.createElement "WaterQuads" appendTo:waterData
					typeElem = waterQuads
				)
				else if isWave then
				(
					if undefined==waveQuads then
						waveQuads = xmlDoc.createElement "WaveQuads" appendTo:waterData
					typeElem = waveQuads				
				)
				else
				(
					if undefined==calmingquads then
						calmingquads = xmlDoc.createElement "CalmingQuads" appendTo:waterData
					typeElem = calmingquads
				)
				
				local item = xmlDoc.createElement "Item" appendTo:typeElem
				
				local minX = obj.pos.x - width
				xmlDoc.createElement "minX" appendTo:item attrs:#(#("value", minX as integer))
				local maxX = obj.pos.x + width
				xmlDoc.createElement "maxX" appendTo:item attrs:#(#("value", maxX as integer))
				local minY = obj.pos.y - length
				xmlDoc.createElement "minY" appendTo:item attrs:#(#("value",  minY as integer))
				local maxY = obj.pos.y + length
				xmlDoc.createElement "maxY" appendTo:item attrs:#(#("value", maxY as integer))
					
				if isWater or isRiver then
				(
					local type = if obj.type>=0 then obj.type else 0
					xmlDoc.createElement "Type" appendTo:item attrs:#(#("value", type as integer))
					xmlDoc.createElement "IsInvisible" appendTo:item attrs:#(#("value", not isVisible))
					xmlDoc.createElement "HasLimitedDepth" appendTo:item attrs:#(#("value", isLimitedlength))
					local z = obj.pos.z
					xmlDoc.createElement "z" appendTo:item attrs:#(#("value", z as float))
					
					--AJM: Removed old IsRiver attribute.
					--xmlDoc.createElement "IsRiver" appendTo:item attrs:#(#("value", isRiver as string))
					
					if not isRiver then
					(
						for ai=1 to 4 do
						(
							local n = "a" + ai as string
							local alphaVal = (obj.getAlpha gAlphaSaveOrder[ai])
							xmlDoc.createElement n appendTo:item attrs:#(#("value", alphaVal as integer))
						)
						
						local ignoreInWaterOccRendVal = getattr obj ignoreInWaterOcclusionRenderingIdx
						xmlDoc.createElement "NoStencil" appendTo:item attrs:#(#("value", ignoreInWaterOccRendVal as string))
					)
				)
				else if isWave then
				(
					xmlDoc.createElement "Amplitude" appendTo:item attrs:#(#("value", obj.Amplitude as float))					
					xmlDoc.createElement "XDirection" appendTo:item attrs:#(#("value", cos(obj.WaveDirection) ))
					xmlDoc.createElement "YDirection" appendTo:item attrs:#(#("value", sin(obj.WaveDirection) ))
				)
				else
				(
					xmlDoc.createElement "fDampening" appendTo:item attrs:#(#("value", valCalmness as float))
				)
			)
		)
		
	)
	
	if not legacyFormat and xmlDoc != undefined then
		xmlDoc.save filename
	else
		close file
)

fn SwapValues &v1 &v2 = 
(
	print ("Swapping Values "+v1 as string+", "+v2 as string)
	local v = v1
	v1 = v2
	v2 = v
)

--
-- name: ImportWaterObjFile
-- desc:
--
fn ImportWaterObjFile filename legacyFormat:false = 
(
	if filename == undefined then 
	(
		return 0
	)
	
	if legacyFormat then
	(
		file = openfile filename mode:"r"
		
		if file == undefined then (
			return 0
		)
	
	
		linecount = 0
		
		while eof file == false do (
		
			linecount = linecount + 1
		
			fileLine = readline file
			
			if fileLine != "processed" then
			(
				-- checks
				
				--prepare 
				local index = findString fileLine "   "
				while index!=undefined do
				(
					fileLine = replace fileLine index 3 " | "
					index = findString fileLine "   "
				)
				
				-- separate vertex informationz
				local vertexTokens = filterstring fileLine "|"

				--separate single vertex axis' and shizzy
				local preSplitLastVert = filterstring vertexTokens[vertexTokens.count] " "
				local primInfoTokens = for t=8 to preSplitLastVert.count collect preSplitLastVert[t]
			
				local newObj = GtaWater()
				local calmerObj = undefined

				-- primitive info
				if primInfoTokens.count>0 then
				(
					flagVals = primInfoTokens[1] as integer

					if (bit.and flagVals 1) != 0 then (
						setattr newObj (getattrindex "Gta Water" "Visible") true
					) else (
						setattr newObj (getattrindex "Gta Water" "Visible") false
					)
					
					if (bit.and flagVals 2) != 0 then (
						setattr newObj (getattrindex "Gta Water" "Limited Depth") true
					) else (
						setattr newObj (getattrindex "Gta Water" "Limited Depth") false
					)
				
					if (bit.and flagVals 4) != 0 then (
						setattr newObj (getattrindex "Gta Water" "Standard") true
					) else (
						setattr newObj (getattrindex "Gta Water" "Standard") false
					)
					
					if (bit.and flagVals 8) != 0 then
					(
						calmerObj = GtaWaterCalmer()

						if primInfoTokens.count > 1 then 
						(
							flagVals = primInfoTokens[2] as float
							calmerObj.Calming = flagVals
						)	
					)

					if primInfoTokens.count > 2 then 
					(
						flagVals = primInfoTokens[3] as integer
						newObj.type  = if flagVals>=0 then flagVals else 0
					)
					else
						newObj.type = 0
				)				
				
				local vertPos = #()
				local vertflowX = #()
				local vertflowY = #()
				local vertstrengthX = #()
				local vertstrengthY = #()
				
				-- go through the found vertices
				for v in vertexTokens do
				(
					--separate single vertex axis' and shizzy
					local vertTokens = filterstring v " "
			
					append vertPos [(vertTokens[1] as integer),(vertTokens[2] as integer),(vertTokens[3] as float)] 
					append vertflowX (vertTokens[4] as float)
					append vertflowY (vertTokens[5] as float)
					append vertstrengthX (vertTokens[6] as float)
					append vertstrengthY (vertTokens[7] as float)
				)

				-- calculate position etc
				if vertPos[1][1] == vertPos[3][1] and vertPos[1][2] == vertPos[2][2] then (
					waterPos = (vertPos[2] + vertPos[3]) / 2
					waterDim = vertPos[3] - vertPos[2]
					
					if (vertPos[2][1] < vertPos[3][1] or vertPos[2][2] < vertPos[3][2]) and vertTokens.count != 28 then (
						newObj.rotation = (quat 0 0 -1 0)
					)
				) else (
					if vertPos[2][1] == vertPos[3][1] and vertPos[1][2] == vertPos[2][2] then (
						waterPos = (vertPos[1] + vertPos[3]) / 2
						waterDim = vertPos[3] - vertPos[1]

						if (vertPos[1][1] < vertPos[3][1] or vertPos[1][2] < vertPos[3][2]) and vertTokens.count != 28 then (
							newObj.rotation = (quat 0 0 -0.707107 0.707107)
						) else (
							newObj.rotation = (quat 0 0 0.707107 0.707107)
						)
					)
				)
				waterDim = [abs(waterDim[1]),abs(waterDim[2]),abs(waterDim[3])]
						
				if waterDim[1] == 0 or waterDim[2] == 0 then (
					waterPos = (vertPos[1] + vertPos[3]) / 2
					waterDim = vertPos[3] - vertPos[1]
					waterDim = [abs(waterDim[1]),abs(waterDim[2]),abs(waterDim[3])]
				)
				
				newObj.width = waterDim[1]
				newObj.length = waterDim[2]
				newObj.pos = waterPos
				
				if undefined!=calmerObj then
				(
					calmerObj.width = waterDim[1]
					calmerObj.length = waterDim[2]
					calmerObj.pos = waterPos
				)

				if(vertflowX.count<4) then
				(
					vertflowX[4] = 0.0
					vertflowY[4] = 0.0
					vertstrengthX[4] = 0.0
					vertstrengthY[4] = 0.0
				)
				
				setattr newObj (getattrindex "Gta Water" "Point1 Flow X") vertflowX[1]
				setattr newObj (getattrindex "Gta Water" "Point1 Flow Y") vertflowY[1]
				setattr newObj (getattrindex "Gta Water" "Point1 Big Wave Strength") vertstrengthX[1]
				setattr newObj (getattrindex "Gta Water" "Point1 Small Wave Strength") vertstrengthY[1]
				setattr newObj (getattrindex "Gta Water" "Point2 Flow X") vertflowX[2]
				setattr newObj (getattrindex "Gta Water" "Point2 Flow Y") vertflowY[2]
				setattr newObj (getattrindex "Gta Water" "Point2 Big Wave Strength") vertstrengthX[2]
				setattr newObj (getattrindex "Gta Water" "Point2 Small Wave Strength") vertstrengthY[2]
				setattr newObj (getattrindex "Gta Water" "Point3 Flow X") vertflowX[3]
				setattr newObj (getattrindex "Gta Water" "Point3 Flow Y") vertflowY[3]
				setattr newObj (getattrindex "Gta Water" "Point3 Big Wave Strength") vertstrengthX[3]
				setattr newObj (getattrindex "Gta Water" "Point3 Small Wave Strength") vertstrengthY[3]
				setattr newObj (getattrindex "Gta Water" "Point4 Flow X") vertflowX[4]
				setattr newObj (getattrindex "Gta Water" "Point4 Flow Y") vertflowY[4]
				setattr newObj (getattrindex "Gta Water" "Point4 Big Wave Strength") vertstrengthX[4]
				setattr newObj (getattrindex "Gta Water" "Point4 Small Wave Strength") vertstrengthY[4]
			)
		)
		close file
	)  -- legacyFormat format
	else
	(		
		local document = XmlDocument()
		document.Load filename
		document.ParseIntoMaxscripthierarchy()
		local waterdata = document.maxXmlObject.children[2]
		local waterquads = waterdata.findChild withName:"WaterQuads"
		
		if ( undefined != waterquads ) do
		(
			for quad in waterquads.children do
			(

				local vertPos = #()
				local vertflowX = #()
				local vertflowY = #()
				local vertstrengthX = #()
				local vertstrengthY = #()

				local minX = (quad.findChild withName:"minX").attrs.item "value" as number
				local maxX = (quad.findChild withName:"maxX").attrs.item "value" as number
				local minY = (quad.findChild withName:"minY").attrs.item "value" as number
				local maxY = (quad.findChild withName:"maxY").attrs.item "value" as number
				if minX>maxX then
					SwapValues &minX &maxX
				if minY>maxY then
					SwapValues &minY &maxY
				local z = (quad.findChild withName:"z").attrs.item "value" as number

				local Type = (quad.findChild withName:"Type").attrs.item "value" as number
				local IsInvisible = (quad.findChild withName:"IsInvisible").attrs.item "value" as BooleanClass
				local HasLimitedDepth = (quad.findChild withName:"HasLimitedDepth").attrs.item "value" as BooleanClass
				local RiverChildTag = (quad.findChild withName:"IsRiver")
				local isRiver = undefined!=RiverChildTag and RiverChildTag.attrs.item "value" as BooleanClass
				if (isRiver) do 
				(
					gRsULog.LogWarning "River attribute found in file, this is an old attribute!  Creating River Helper anyway."
				)
				
				noStencilNode = (quad.findChild withName:"NoStencil")
				local ignoreInWaterOcclusionRenderingVal = false
				if (noStencilNode != undefined) do
				(
					ignoreInWaterOcclusionRenderingVal = noStencilNode.attrs.item "value" as BooleanClass
				)

				local width = maxX - minX
				local length = maxY - minY
				local posX = minX + (width/2)
				local posY = minY + (length/2)
				local pos = [posX, posY, z]

				local newObj = \
					if isRiver then 
						RsRiverHelper()
					else
						GtaWater()
				newObj.width = width
				newObj.length = length
				newObj.pos = pos
				newObj.type = if Type>=0 then Type else 0

				if not isRiver then
				(
					for ai=1 to 4 do
					(
						local n = "a" + (gAlphaSaveOrder[ai]+1) as string
						local val = (quad.findChild withName:n)
						if undefined!=val then
							newObj.setAlpha (ai-1) (val.attrs.item "value" as number)
					)

					setattr newObj (getattrindex "Gta Water" "Visible") (not IsInvisible)
					setattr newObj (getattrindex "Gta Water" "Limited Depth") HasLimitedDepth
					setattr newObj ignoreInWaterOcclusionRenderingIdx ignoreInWaterOcclusionRenderingVal
				)
			)
		)
		
		local CalmingQuads = waterdata.findChild withName:"CalmingQuads"

		if undefined!=CalmingQuads then
		(
			for quad in CalmingQuads.children do
			(
				newObj = GtaWaterCalmer()
				
				local vertPos = #()
				local vertflowX = #()
				local vertflowY = #()
				local vertstrengthX = #()
				local vertstrengthY = #()
				
				local minX = (quad.findChild withName:"minX").attrs.item "value" as number
				local maxX = (quad.findChild withName:"maxX").attrs.item "value" as number
				local minY = (quad.findChild withName:"minY").attrs.item "value" as number
				local maxY = (quad.findChild withName:"maxY").attrs.item "value" as number
		
				local calming = (quad.findChild withName:"fDampening").attrs.item "value" as number
				
				local width = maxX - minX
				local length = maxY - minY
				local posX = minX + (width/2)
				local posY = minY + (length/2)
				local pos = [posX, posY, 0]
				
				newObj.width = width
				newObj.length = length
				newObj.pos = pos
				newObj.calming = calming				
			)
		)
		
		local WaveQuads = waterdata.findChild withName:"WaveQuads"

		if undefined!=WaveQuads then
		(
			for quad in WaveQuads.children do
			(
				newObj = RSWaterWave()

				local minX = (quad.findChild withName:"minX").attrs.item "value" as number
				local maxX = (quad.findChild withName:"maxX").attrs.item "value" as number
				local minY = (quad.findChild withName:"minY").attrs.item "value" as number
				local maxY = (quad.findChild withName:"maxY").attrs.item "value" as number

				local amplitude = (quad.findChild withName:"Amplitude").attrs.item "value" as float
				local directionX = (quad.findChild withName:"XDirection").attrs.item "value" as float
				local directionY = (quad.findChild withName:"YDirection").attrs.item "value" as float

				local width = maxX - minX
				local length = maxY - minY
				local posX = minX + (width/2)
				local posY = minY + (length/2)
				local pos = [posX, posY, 0]

				newObj.width = width
				newObj.length = length
				newObj.pos = pos
				newObj.Amplitude = amplitude
				
				ang = atan2 directionY directionX
		
				newObj.WaveDirection = ang
			)
		)
	)
) 

-- pipeline/export/water.ms
