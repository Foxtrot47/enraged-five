--
-- File:: pipeline/export/exportnotenodes.ms
-- Description:: Export Note Node group to an xml file
--
-- Author:: Adam Munson <adam.munson@rockstarnorth.com>
-- Date:: 21/04/11
--
-----------------------------------------------------------------------------
fn RsNoteNodesExport = 
(
	if ( RsNoteNodeGroup.count == 0 ) do
	(
		Messagebox "Nothing to export..."
		return false
	)
	
	nodeList = #()
	nodeSet = #()

	local commonDir = RsConfigGetCommonDir() + "data/node_routes/"

	local nodefilename = getSaveFileName caption:"Note Nodes file to save" filename:commonDir types:"Note Nodes file (*.xml)|*.xml"

	if (nodefilename == null) do
	(
		Messagebox "No file given to export to!"
		return false
	)
	
	nodeDoc = XmlDocument()    
	nodeDoc.init()
	
	rootXmlNode = nodeDoc.document.CreateElement("NoteNodes")
	versionAttr = nodeDoc.CreateAttribute "version"
	versionAttr.value = "1"
	rootXmlNode.Attributes.append( versionAttr )

	for setNum = 1 to ( RsNoteNodeGroup.count ) do
	(
		setElem = nodeDoc.CreateElement("Set")
		
		setNameAttr = nodeDoc.CreateAttribute "name"
		setNameAttr.value = ( setNum ) as string 
		
		setCreationNumberAttr = nodeDoc.CreateAttribute "creation_number"
		setCreationNumberAttr.value = ( setNum - 1 ) as string						-- 0 based for game
		
		setNodeCreationNumberAttr = nodeDoc.CreateAttribute "node_creation_number"
		setNodeCreationNumberAttr.value = ( RsNoteNodeGroup[setNum].count ) as string
		
		setOpenAttr = nodeDoc.CreateAttribute "open"
		setOpenAttr.value = ( ( setNum == RsNoteNodeSet ) as string )
		
		setElem.Attributes.append( setNameAttr )
		setElem.Attributes.append( setCreationNumberAttr )
		setElem.Attributes.append( setNodeCreationNumberAttr )
		setElem.Attributes.append( setOpenAttr )		
		
		-- Set the node-numbers up for export:
		local setNodeList = RsNoteNodeGroup[setNum]
		for setNodeNum = 1 to setNodeList.count do 
		(
			setNodeList[setNodeNum].nodeNumber = setNodeNum - 1
		)
		
		for noteNode in setNodeList do
		(
			local nodeElem = nodeDoc.CreateElement("Node")			
			
			nodeLabelAttr 				= nodeDoc.CreateAttribute "label"
			nodeLabelAttr.value			= noteNode.nodeLabel
			
			nodeCreationNumAttr			= nodeDoc.CreateAttribute "creation_number"
			nodeCreationNumAttr.value 	= ( setNum - 1 ) as string
			
			nodeLinkAttr				= nodeDoc.CreateAttribute "link"
			if (isValidNode noteNode.nodeLink) then
			(
				nodeLinkAttr.value		= noteNode.nodeLink.nodeNumber as string
			)
			else
			(
				nodeLinkAttr.value		= "-1"
			)

			nodeTypeAttr				= nodeDoc.CreateAttribute "type"
			nodeTypeAttr.value			= ( noteNode.nodeType - 1 ) as string

			nodeXAttr					= nodeDoc.CreateAttribute "x"
			nodeXAttr.value				= ( noteNode.pos.x ) as string
			nodeYAttr					= nodeDoc.CreateAttribute "y"
			nodeYAttr.value				= ( noteNode.pos.y ) as string
			nodeZAttr					= nodeDoc.CreateAttribute "z"
			nodeZAttr.value				= ( noteNode.pos.z ) as string
			
			nodeSizeAttr				= nodeDoc.CreateAttribute "size"
			nodeSizeAttr.value			= ( noteNode.nodeSize ) as string
			
			nodeColourAttr				= nodeDoc.CreateAttribute "colour"
			nodeColourAttr.value		= ( noteNode.colourIdx - 1 ) as string
			
			nodeBlockNameAttr			= nodeDoc.CreateAttribute "blockName"
			nodeBlockNameAttr.value		= noteNode.blockName
			
			nodeElem.Attributes.append ( nodeLabelAttr )
			nodeElem.Attributes.append ( nodeCreationNumAttr )
			nodeElem.Attributes.append ( nodeLinkAttr )
			nodeElem.Attributes.append ( nodeTypeAttr )
			nodeElem.Attributes.append ( nodeXAttr )
			nodeElem.Attributes.append ( nodeYAttr )
			nodeElem.Attributes.append ( nodeZAttr )
			nodeElem.Attributes.append ( nodeSizeAttr )
			nodeElem.Attributes.append ( nodeColourAttr )
			nodeElem.Attributes.append ( nodeBlockNameAttr )
			
			setElem.AppendChild( nodeElem ) 
		) -- Node loop
		
		rootXmlNode.AppendChild( setElem )
	) -- Set loop
	
	nodeDoc.document.AppendChild rootXmlNode	
	nodeDoc.save nodefilename
) -- RsNoteNodesImport