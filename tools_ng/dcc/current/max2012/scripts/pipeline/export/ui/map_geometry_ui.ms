--
-- File:: pipeline/export/ui/map_geometry_ui.ms
-- Description:: Map Geometry Exporter Rollout Definition
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 10 June 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/mapgeom.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

rollout RsGeomRoll "Geometry Export"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Map_Export#Geometry_Export" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnExportAll 	"Export All Geom" width:100
	button btnExportMissing "Export Missing" width:100
	button btnExportSelected "Export Selected" width:100
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None (see pipeline/export/maps/mapgeom.ms)

	-------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------
	
	--------------------------------------------------------------
	-- export all button pressed
	--------------------------------------------------------------	
	on btnExportAll pressed do (

		try
		(
			gRsULog.ClearLogDirectory()
			
			if ( RSRemConIsGameRunning queryRestart:true == false ) do
			(
				if ( querybox "Are you sure you want to export all?" title:"sanity check" ) == false then 
					return false

				maps = RsMapGetMapContainers()
			
				for map in maps do 
				(	
					if (map.is_container() and map.is_exportable()) do 
					(						
						if RsPreExport map == false then 
							return false

						local mapgeom = RsMapGeometry()
						mapgeom.ExportAll()
						RsPostExport map

								
					)
				)
				if (querybox "Do you wish to build the map(s)?") == true then (
					RsMapBuildImages maps
				)
			)
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				throw()
			)
		)	
	
	)

	--------------------------------------------------------------
	-- export missing button pressed
	--------------------------------------------------------------
	on btnExportMissing pressed do (

		try
		(
			gRsULog.ClearLogDirectory()
			
			if ( RSRemConIsGameRunning queryRestart:true == false ) do
			(
				maps = RsMapGetMapContainers()
			
				for map in maps do 
				(	
					if (map.is_container() and map.is_exportable()) do 
					(									
						if RsPreExport() == false then return false

						local mapgeom = RsMapGeometry()
						mapgeom.ExportMissing()
						RsPostExport map
					)
				)
				if (querybox "Do you wish to build the map(s)?") == true then (
					RsMapBuildImages maps
				)
			)
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				throw()
			)
		)				
	)

	--------------------------------------------------------------
	-- export selected button pressed
	--------------------------------------------------------------
	on btnExportSelected pressed do (
			
		try
		(
			gRsULog.ClearLogDirectory()
			
			if ( RSRemConIsGameRunning queryRestart:true == false ) do
			(
				maps = RsMapGetMapContainers()
			
				for map in maps do 
				(	
					if (map.is_container() and map.is_exportable()) do 
					(						
						if RsPreExport map == false then return false

						local mapgeom = RsMapGeometry()
						mapgeom.ExportSelected()

						RsPostExport map
					)
				)
				if (querybox "Do you wish to build the map(s)?") == true then (
					RsMapBuildImages maps
				)
			)
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				throw()
			)
		)				
	)
	
	on RsGeomRoll rolledUp down do 
	(
		RsSettingWrite "rsMapGeom" "rollup" (not down)
	)
)

-- pipeline/export/ui/map_geometry_ui.ms
