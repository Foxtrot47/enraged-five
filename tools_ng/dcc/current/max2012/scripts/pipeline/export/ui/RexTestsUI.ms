--
-- File:: RexTestsUI.ms
-- Description:: Simple model export simulation
--
-- Author:: Gunnar Droege
-- Date:: 24/7/12
--

-- Model-specific includes
--filein "pipeline/util/texturemetatag_utils.ms"
filein "pipeline/export/models/globals.ms"
filein "pipeline/export/model.ms"
filein "pipeline/util/skinutils.ms"
filein "pipeline/export/maps/collision.ms"
filein "pipeline/export/maps/mapgeom.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

--
-- rollout: RsSimpleModelRoll
-- desc: Simple model exporter rollout (IDR/IDD and raw export).
--
rollout RsRexTestsRoll "Rex Export Tests"
(

	---------------------------------------------------------------------------
	-- UI
	---------------------------------------------------------------------------
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Rex_export_simulation" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	group "Bound Tests"
	(
		button btnExportAll "Test scene collision" width:120
		button btnExportSel "Test selected collision" width:120
	)
	group "Map Geometry Tests"
	(
		button btnExportAllModels "Test Scene Models" width:120
		button btnExportSelModels "Test Selected Models" width:120
	)
	group "Options:"
	(
		checkbox chkAscii	"Export ASCII"
	)
	
	fn CollectBoundsRec objs collectedBounds = 
	(
		for obj in objs do
		(
			-- limit to non-rpimitive types. Primitives are not subject to these checks
			if Col_mesh == classof obj then
				appendIfUnique collectedBounds obj
			
			CollectBoundsRec obj.children collectedBounds
		)
	)
	
	--
	-- event: btnExportAll pressed
	-- desc: Simulates raw model exporter on all scene objects .
	--
	on btnExportAll pressed do (
		
		gRsULog.ClearLogDirectory()
		gRsUlog.init "Rex Export Tests" appendToFile:false
	
		-- Save our output directory.
		RsModelSetOutputDirectory (GetDir #temp)
		
		RsBoundCheckErrors = true
		
		-- Export all.
		local coll = #()
		CollectBoundsRec rootnode.children coll
		if RsModelExportGroup coll asciiMesh:chkAscii.checked simulate:true then
			messagebox "No errors found"

		RsBoundCheckErrors = false
	)

	--
	-- event: btnExportAll pressed
	-- desc: Simulates raw model exporter on all scene objects .
	--
	on btnExportSel pressed do (

		gRsULog.ClearLogDirectory()
		gRsUlog.init "Rex Export Tests" appendToFile:false
		
		-- Save our output directory.
		RsModelSetOutputDirectory (GetDir #temp)
		
		RsBoundCheckErrors = true

		-- Export all.
		local coll = #()
		CollectBoundsRec selection coll
		if RsModelExportGroup coll asciiMesh:chkAscii.checked simulate:true then
			messagebox "No errors found"

		RsBoundCheckErrors = false
	)
	
	on btnExportAllModels pressed do (
		
		gRsULog.ClearLogDirectory()
		gRsUlog.init "Rex Export Tests" appendToFile:false
		
		local tempDirectory = (GetDir #temp)
		RsModelSetOutputDirectory tempDirectory
		
		local maps = for map in RsMapGetMapContainers doingExport:true where map.is_exportable() collect map
		
		for map in maps do
		(
			RsMapSetupGlobals map
			
			txdList = #()
			RsGetSortedTxdList map.exportObjects txdlist -- All txds for give objects

				-- Create structure to load up the parent txd file and find promoted textures
			local parentTxdStruct = ParentTxd()
			parentTxdStruct.Load (RsConfigGetAssetsDir core:true + "/maps/ParentTxds.xml") txdlist
			
			local mapGeometry = RsMapGeometry()
			if (mapGeometry.ExportModelsInner map map.exportObjects map.exportObjects tempDirectory parentTxdStruct rexExportTests:true) then
				messagebox "No errors found"
			
			gRsULog.Validate()
		)
	)
	
	on btnExportSelModels pressed do (
		
		gRsULog.ClearLogDirectory()
		gRsUlog.init "Rex Export Tests" appendToFile:false
		
		local tempDirectory = (GetDir #temp)
		RsModelSetOutputDirectory tempDirectory
		
		local maps = for map in RsMapGetMapContainers doingExport:true where map.is_exportable() and (map.selCount() != 0) collect map
		
		for map in maps do
		(
			RsMapSetupGlobals map
			
			local selObjs = for obj in map.selObjects where not (( isInternalRef o ) or ( isRefObj o )) collect obj
			local mapGeometry = RsMapGeometry()
			
			txdList = #()
			RsGetSortedTxdList selObjs txdlist -- All txds for give objects

			-- Create structure to load up the parent txd file and find promoted textures
			local parentTxdStruct = ParentTxd()
			parentTxdStruct.Load (RsConfigGetAssetsDir core:true + "/maps/ParentTxds.xml") txdlist
			
			if (mapGeometry.ExportModelsInner map selObjs map.exportObjects tempDirectory parentTxdStruct rexExportTests:true) then
				messagebox "No errors found"
			
			gRsULog.Validate()
		)
	)

)