-- 
-- SharedTextureList.ms
-- Shared Texture List UI Rollout Definition
--
-- This file presents a simple UI wrapper around the SharedTextureList struct
-- defined in ../../util/shared_texture_list.ms.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 21 February 2008
--
-- AJM:	Added new rollout for shared texture list to set the TCS template name
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
fileIn "pipeline/util/shared_texture_list.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global RsTextureFilter = "Textures (*.bmp *.tga *.jpg)|*.bmp;*.tga;*.jpg"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

try (destroyDialog RsSharedTexListRoll) catch ()
try (destroyDialog RsAddSharedTexListRoll) catch ()

rollout RsAddSharedTexListRoll "Add new texture list:" width:220
(
	-- I'm using a dotNet textbox, as it lets me put the selection-caret at the end of the existing text, for easier typing:
	dotNetControl txtNewName "System.Windows.Forms.TextBox" across:2 width:(RsAddSharedTexListRoll.width - 48)
	label lblExtension ".xml" align:#right offset:[0,4]
	
	button btnAddNewList "Add new list" width:80 align:#left offset:[0,8] across:2
	button btnCancel "Cancel" width:80 align:#right offset:[0,8]
	
	fn addNewList = 
	(
		local newListName = txtNewName.text
		
		if (newListName != "") and ::RsSharedTexListRoll.open do 
		(
			local newTexList = sharedTextureListXML()
			newListName += ".xml"
			local newFilename = RsSharedTexListRoll.sharedTxdDir + newListName
			
			newTexList.saveAs newFilename
			
			if doesFileExist newFilename do 
			(
				gRsPerforce.add newFilename
			)
			
			RsSharedTexListRoll.setXmlList filename:newListName
		)
		
		destroyDialog RsAddSharedTexListRoll
	)
	
	on txtNewName KeyPress val do 
	(
		if keyboard.escPressed then 
		(
			destroyDialog RsAddSharedTexListRoll
		)
		else 
		(
			if val.keyChar == "\r" do 
			(
				addNewList()
			)
		)
	)
	
	on btnAddNewList pressed do 
	(
		addNewList()
	)
	
	on btnCancel pressed do 
	(
		destroyDialog RsAddSharedTexListRoll
	)
	
	on RsAddSharedTexListRoll open do 
	(
		-- Allow enter to be pressed in textbox:
		txtNewName.multiLine = true
		txtNewName.acceptsTab = true
		txtNewName.height = 20
		
		-- Set textbox colors to match UI:
		local textCol = (colorMan.getColor #windowText) * 255
		local windowCol = (colorMan.getColor #window) * 255
		local DNcolour = dotNetClass "System.Drawing.Color"
		txtNewName.foreColor = DNcolour.FromArgb textCol[1] textCol[2] textCol[3]
		txtNewName.backColor = DNcolour.FromArgb windowCol[1] windowCol[2] windowCol[3]
		
		local prefixUsed = ::RsSharedTexListRoll.txtPrefix.text
		
		if (prefixUsed != "") do 
		(
			txtNewName.text = prefixUsed + "_"
			txtNewName.SelectionStart = txtNewName.text.count
		)
		
		setFocus txtNewName
	)
)

rollout RsSharedTexListRoll "Shared Texture Lists" width:340
(
	local sharedTxdDir = (RsConfigGetMetadataDir core:true) + "textures/shared_textures/"
	local sharedDLCTxdDir = (RsConfigGetMetadataDir core:false) + "textures/shared_textures/"

	local xmlFiles, textureList
	local filePrefix = ""
	local showVehVariations = false
	local templateXml = undefined
	
	-- Default add-texture folder, changed whenever a new texture is chosen:
	local addFilePath = ((RsProjectGetArtDir core:true) + "/textures/")

	group "Shared-Texture Lists:"
	(
		editText txtPrefix "Prefix:" enabled:false
		dropDownList lstXmlfiles "" width:(RsSharedTexListRoll.width - 70) align:#left tooltip:"Choose texture list to show"
		
		button btnAddList "+" width:19 height:18 pos:(lstXmlfiles.pos + [lstXmlfiles.width + 3,1]) align:#right enabled:false
		button btnDelList "-" width:19 height:18 pos:(btnAddList.pos + [21,0]) align:#right enabled:false
		
		label txdLbl "Txd name:" across:3 align:#left offset:[0,1]
		editText txdNameCtrl "" width:144 align:#right offset:[24,0] enabled:false
		checkButton btnOverrideTxd "Override" align:#right height:16 offset:[0,1]
		
		button btnEdit "Check out" width:110 offset:[0,4] align:#left across:2 tooltip:"Check selected Shared Texture List out of Perforce, and allow editing"
		button btnReload "Reload" width:110 offset:[0,4] align:#right tooltip:"Reload selected Shared Texture List"
		
		timer tmrReadOnlyCheck interval:5000
	)
	
	group "List Textures:"
	(
		multiListBox lstTextures "" height:20
		button btnAddTex "Add" width:100 across:3 align:#left enabled:false
		button btnChange "Change Template" enabled:False
		button btnRemove "Remove" width:100 align:#right enabled:false
		label lblStatus "" align:#left
	)
	
	local editCtrls = #(btnAddList, btnDelList, btnAddTex, btnChange, btnRemove, txdLbl, txdNameCtrl, btnOverrideTxd)
	
	-- Clears status-label after 3 seconds:
	timer tmrStatus interval:3000 active:False
	on tmrStatus tick do 
	(
		lblStatus.text = ""
		tmrStatus.active = False
	)
	
	fn saveTextureList = 
	(
		local filename = "[none]"
		if (textureList != undefined) do 
		(
			filename = (filenameFromPath textureList.filename)
		)
		
		lblStatus.text = "Saving changes to: " + filename
		textureList.save()
		lblStatus.text = "Changes saved"
		
		tmrStatus.active = True
	)
	
	fn enableEditIfWritable filename = 
	(
		editCtrls.enabled = false
		
		case of 
		(
			(filename == undefined):
			(
				for item in editCtrls where (isProperty item "tooltip") do 
				(
					item.tooltip = "Disabled if no XML-file is selected"
				)
				
				if not showVehVariations do 
				(
					btnAddList.tooltip = "Add new texture list"
					btnAddList.enabled = true
				)
			)
			(not doesFileExist filename):
			(
				for item in editCtrls where (isProperty item "tooltip") do 
				(
					item.tooltip = "Disabled if XML-file doesn't exist"
				)
				
				btnAddList.enabled = true
				btnAddList.tooltip = "Add new texture list"
			)
			(RsIsFileReadOnly filename):
			(
				for item in editCtrls where (isProperty item "tooltip") do 
				(
					item.tooltip = "Disabled if selected XML is not writable"
				)
				
				btnAddList.tooltip = "Add new texture list"
				btnDelList.tooltip = "Remove texture list"
				btnAddList.enabled = true
				btnDelList.enabled = true
			)
			default:
			(
				editCtrls.enabled = true
				
				btnAddList.tooltip = "Add new texture list"
				btnDelList.tooltip = "Remove texture list"
				btnAddTex.tooltip = "Add new textures to selected dictionary list"
				btnChange.tooltip = "Change template for selected texture(s)"
				btnRemove.tooltip = "Remove selected texture from selected dictionary list"
				btnOverrideTxd.tooltip = "Set export txd name\n(same as xml-filename by default)"
				
				if showVehVariations do 
				(
					btnAddList.tooltip = "Can't add variations list:\nFile already exists"
					btnAddList.enabled = false
				)
			)
		)
		
		local canEditSel = btnAddTex.enabled and (lstTextures.selection.numberSet != 0)
		btnRemove.enabled = canEditSel
		btnChange.enabled = canEditSel
	)
	
	fn showTextureList = 
	(
		-- Collect pre-change selection:
		local selItems = for n = lstTextures.selection collect lstTextures.items[n]
		selItems = for item in selItems collect (filterString item "[")[1]
		
		local listNum = lstXmlfiles.selection
		
		textureList = sharedTextureListXML()
		
		local filename
		
		if (listNum != 0) and (xmlFiles != undefined) do 
		(
			filename = xmlFiles[listNum]
			
			textureList.load filename
		)
		
		-- Sort texturelist alphabetically:
		fn sortTexList v1 v2 = (striCmp (getFilenameFile v1.file) (getFilenameFile v2.file))
		qsort textureList.textures sortTexList
		
		enableEditIfWritable filename
		
		lstTextures.items = for item in textureList.textures collect ((filenameFromPath item.file) + " [" + item.tcs + "]")

		if not showVehVariations do 
		(
			txdNameCtrl.text = if (textureList.txd == undefined) then "" else textureList.txd
			
			case of 
			(
				(textureList.filename == undefined):
				(
					btnOverrideTxd.checked = false
					txdNameCtrl.enabled = false
					txdNameCtrl.text = ""
				)
				(textureList.txd == undefined):
				(
					btnOverrideTxd.checked = false
					txdNameCtrl.enabled = false
					txdNameCtrl.text = getFilenameFile textureList.filename
				)
				default:
				(
					btnOverrideTxd.checked = true
					txdNameCtrl.enabled = true
					txdNameCtrl.text = textureList.txd
				)
			)
		)
		
		-- Re-select old selection with new list: (comparing first bit of list-lines)
		local newSel = #{}
		local selSearchList = for item in lstTextures.items collect (filterString item "[")[1]
		for selItem in selItems do 
		(
			local selNum = findItem selSearchList selItem
			if (selNum != 0) do 
			(
				newSel[selNum] = True
			)
		)
		
		-- Set selection:
		lstTextures.selection = newSel
	)
	
	fn setXmlListItems = 
	(
		if showVehVariations do 
		(
			if (maxFileName == undefined) or (maxFileName == "") then 
			(
				xmlFiles = undefined
				lstXmlfiles.items = #("No Maxfile loaded")
			)
			else 
			(
				local filename = getFilenameFile maxfilename

				-- Remove "_skinned" from filename.
				if (matchPattern filename pattern:"*_skinned") do 
				(
					filename = substring filename 1 (filename.count - "_skinned".count)
				)
			
				local variationsXml = maxFilePath + filename + ".xml"
				xmlFiles = #(variationsXml)
				
				if (textureList.filename != variationsXml) do 
				(
--					textureList.load variationsXml
					showTextureList()
				)
			)
		)
		
		if (xmlFiles != undefined) do 
		(
			lstXmlfiles.items = for filename in xmlFiles collect 
			(
				local item = (filenameFromPath filename)
				
				case of 
				(
					(not doesFileExist filename):(append item " [doesn't exist]")
					(getFileAttribute filename #readOnly):(append item " [read-only]")
				)
				
				item 
			)
		)
	)
	
	fn setXmlList filename: = 
	(
		if not showVehVariations do 
		(
			if (filename != unsupplied) and (not matchPattern filename pattern:(filePrefix + "*")) do 
			(
				txtPrefix.text = ""
			)
			
			filePrefix = txtPrefix.text
			
			local selPattern = if (filename != unsupplied) then filename else filePrefix
			selPattern += "*"
			
			xmlFiles = getFiles (sharedTxdDir + filePrefix + "*.xml")
			if(RsIsDLCProj()) then(
				local dlcXmlFiles = getFiles (sharedDLCTxdDir + filePrefix + "*.xml")
				join xmlFiles dlcXmlFiles
			)
			
			setXmlListItems()
			
			lstXmlfiles.selection = 0
			for n = 1 to lstXmlfiles.items.count while (lstXmlfiles.selection == 0) do 
			(
				if (matchPattern lstXmlfiles.items[n] pattern:selPattern) do 
				(
					lstXmlfiles.selection = n
				)
			)
		)
			
		showTextureList()
	)
	
	fn getTemplateName = 
	(	
		global RsTextureTemplate = templateXml
		global RsSelectedTexTemplate = ""
		
		-- Get default value from selection:
		if (textureList != undefined) do 
		(
			local currentTemplateSel = for n = lstTextures.selection collect 
			(
				textureList.textures[n].tcs
			)
			
			currentTemplateSel = makeUniqueArray currentTemplateSel
			
			-- Only set value if only one is used by selection:
			if (currentTemplateSel.count == 1) do 
			(
				RsSelectedTexTemplate = currentTemplateSel[1]
			)
		)

		rollout RsTexTemplateSelectionRoll "Texture Compression Template"
		(
			listbox lstTemplates height:15
			button btnOK "OK" width:100 across:2
			button btnCancel "Cancel" width:100
			
			on RsTexTemplateSelectionRoll open do 
			(
				if RsTextureTemplate == undefined then 
				(
					RsTextureTemplate = ( RsConfigGetEtcDir() + "texture/vehicletemplates.xml" )
				)
				
				-- This array is of structs that hold the template name and the path to it:
				local templateList = #()
				RsLoadTexTuneConfig &templateList templatesXmlFilename:RsTextureTemplate
				
				-- Get rid of the paths and create array for list of just the names of template:
				templateList = for item in templateList collect (item.desc as string)
				
				lstTemplates.items = templateList
				
				local findNum = findItem templateList RsSelectedTexTemplate
				if (findNum != 0) do 
				(
					lstTemplates.selection = findNum
				)
			)
			
			fn returnVal cancel:False = 
			(
				if cancel then 
				(
					RsSelectedTexTemplate = undefined
				)
				else 
				(
					RsSelectedTexTemplate = lstTemplates.selected
					
					if (RsSelectedTexTemplate == "") do 
					(
						RsSelectedTexTemplate = "Default"
					)
				)
				
				DestroyDialog RsTexTemplateSelectionRoll
			)
			
			on lstTemplates doubleClicked val do (returnVal())
			on btnOK pressed do (returnVal())
			on btnCancel pressed do (returnVal cancel:True)
		)
		
		createDialog RsTexTemplateSelectionRoll width:300 modal:true style:#(#style_titlebar, #style_border)
		
		local retVal = RsSelectedTexTemplate
		
		-- Get rid of temp globals:
		globalVars.remove #RsTextureTemplate
		globalVars.remove #RsSelectedTexTemplate
		
		return retVal
	)
	
	on lstTextures selected val do 
	(
		local canEditSel = btnAddTex.enabled and (lstTextures.selection.numberSet != 0)
		btnRemove.enabled = canEditSel
		btnChange.enabled = canEditSel
	)
	
	on tmrReadOnlyCheck tick do 
	(
		-- Update read-only suffixes on list-items every so often:
		setXmlListItems()
	)
	
	on txtPrefix changed newText do 
	(
		setXmlList()
	)
	
	on lstXmlfiles selected selNum do 
	(
		showTextureList()
	)
	
	on btnAddList pressed do 
	(
		if showVehVariations then 
		(
			local newTexList = sharedTextureListXML()
			local newFilename = xmlFiles[1]
			
			RsMakeSurePathExists (getFilenamePath newFilename)

			newTexList.saveAs newFilename
			
			if doesFileExist newFilename do 
			(
				gRsPerforce.add newFilename
			)
			
			setXmlList()
		)
		else 
		(		
			try (destroyDialog RsAddSharedTexListRoll) catch ()
		
			createDialog RsAddSharedTexListRoll modal:true
		)
	)
	
	on btnDelList pressed do 
	(
		if (lstXmlfiles.selection != 0) do 
		(
			gRsPerforce.del xmlFiles[lstXmlfiles.selection] silent:true
			
			setXmlList()
		)
	)
	
	on txdNameCtrl entered newText do 
	(
		local textVal = trimRight newText
		
		if (textureList != undefined) do 
		(
			local newTxd = if (textVal == "") then undefined else textVal
			
			if (newTxd != textureList.txd) do 
			(
				textureList.txd = newTxd
				saveTextureList()
			)
		)
		
		txdNameCtrl.text = textVal
	)
	
	on btnOverrideTxd changed override do 
	(
		txdNameCtrl.enabled = override
		
		if (textureList != undefined) do 
		(
			if override then 
			(
				textureList.txd = txdNameCtrl.text
				saveTextureList()
			)
			else 
			(
				textureList.txd = undefined
				saveTextureList()
				
				txdNameCtrl.text = getFilenameFile textureList.filename
			)
		)
	)
	
	on btnEdit pressed do 
	(
		if (lstXmlfiles.selection != 0) and (xmlFiles != undefined) do 
		(
			gRsPerforce.edit xmlFiles[lstXmlfiles.selection]
			showTextureList()
		)
	)
	
	-- Reload texture-list, re-selecting current selection, if possible:
	on btnReload pressed do 
	(
		showTextureList()
	)

	on btnChange pressed do 
	(
		-- Choose a template to apply to selected texture-names:
		local templateName = GetTemplateName()
	
		-- Don't change if template-choice was cancelled:
		if (templateName != undefined) do 
		(
			for n = lstTextures.selection do 
			(
				texturelist.textures[n].tcs = templateName
			)
			
			saveTextureList()
			showTextureList()
		)
	)
	
	on btnAddTex pressed do 
	(
		if (texturelist != undefined) do 
		(
			local getFilenames = RSgetOpenFilenames caption:"Add files to list" filename:addFilePath types:RsTextureFilter
			
			-- If cancelled, drop out:
			if (getFilenames == undefined) do (return false)

			-- Get new default path from first texture in list:
			addFilePath = (getFilenamePath getFilenames[1])
			
			-- Collect filenames that aren't already in list:
			local currentFiles = for item in textureList.textures collect (toLower (filenameFromPath item.file))
			local newFilenames = for item in getFilenames where (appendIfUnique currentFiles (toLower (filenameFromPath item))) collect item
			
			if (newFilenames.count == 0) then 
			(
				if (getFilenames.count == 0) do 
				(
					messageBox "Selected file(s) already exist in this list" title:"No new filenames selected"
				)
			)
			else 
			(
				-- Choose a template to apply to these new texture-names:
				local templateName = GetTemplateName()
			
				-- Don't add new textures if template-choice was cancelled:
				if (templateName != undefined) do 
				(				
					for newItem in newFilenames do 
					(
						texturelist.add newItem templateName
					)
					
					saveTextureList()
					showTextureList()
				)
			)
		)
	)
	
	on btnRemove pressed do 
	(
		if (textureList != undefined) and (lstTextures.selection.numberSet != 0) do 
		(
			local removeTexList = for n = lstTextures.selection collect textureList.textures[n]
			
			for item in removeTexList where (item != undefined) do 
			(
				textureList.remove item
			)
			
			saveTextureList()
			showTextureList()
		)
	)
	
	fn init prefix:"" vehVariations:false templateXmlFile:undefined = 
	(
		gRsPerforce.sync (sharedTxdDir + "...") silent:true
		
		txtPrefix.text = prefix
		showVehVariations = vehVariations
		templateXml = templateXmlFile
		
		if showVehVariations do 
		(
			RsSharedTexListRoll.title = "Texture Variations"
		)
		
		setXmlList()
	)
)

fn RsCreateSharedTexListRoll prefix:"" vehVariations:false templateXmlFile:undefined = 
(
	try (destroyDialog RsSharedTexListRoll) catch ()
	createDialog RsSharedTexListRoll
	RsSharedTexListRoll.init prefix:prefix vehVariations:vehVariations templateXmlFile:templateXmlFile
)

--RsCreateSharedTexListRoll prefix:"vehicles" --vehVariations:true 
