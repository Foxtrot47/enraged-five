---------------------------------------------------------------------------
-- Uses
---------------------------------------------------------------------------
filein "pipeline/export/maps/globals.ms"
filein "pipeline/export/maps/objects.ms"

---------------------------------------------------------------------------
-- Uses
---------------------------------------------------------------------------
global RsValidMapContainersForEditor = #()

---------------------------------------------------------------------------
-- Functions
---------------------------------------------------------------------------
fn FindValidMapContainers mapContainers = 
(	
	if (::RsValidMapContainersForEditor == undefined) then 
	(
		return false
	)
	
	local maps = RsMapGetMapContainers()
	for map in maps do 
	(	
		if (map.is_container() and map.is_exportable() and map.count != 0) do 
		(
			obj = undefined
			for o in ::RsValidMapContainersForEditor do
			(
				if (o.Name ==  map.name) then
					obj = o
			)
			if (obj != undefined) then
				append mapContainers obj
		)
	)
)

rollout RsContainerAttributesEditorRoll "Container Attributes Editor" width:344 height:580
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	dotNetControl tc "System.Windows.Forms.TabControl" width:300 height:25
	subRollout      tcPage height:500
	checkbox 		 chkCountdown "Countdown" width:100 across:2 align:#right
	button 			 btnClose "Close" width:100 align:#right
	timer 			 tmrCount enabled:true
	
	-------------------------------------------------------------------------
	-- Locals
	-------------------------------------------------------------------------
	local currentPage = undefined	
	local timerCount =20
	local mapContainers = #()
	local validObjects = #()
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	fn setTimeYes = 
	(
		btnClose.text = "Close (" + (timerCount as string) + ")"
	)
	
	fn CreateAttruteRollout mapContainer =
	(
		if (mapContainer == undefined) then
		(
			currentPage = undefined
			return undefined
		)
		
		local propClassName = "RS Container"
		local safeName = RsFileSafeString mapContainer.Name
		local rciName = (safeName+"_rollout")
		local rci = rolloutCreator rciName ("Container Attributes (" + mapContainer.Name + ")")
		rci.begin()
		
		for attrIndex=1 to (getnumattr propClassName) do
		(
			local attrName = getattrName propClassName attrIndex
			local type = GetControlType propClassName attrName
			local prop = getattr mapContainer attrIndex
			if undefined==type then 
				continue
			
			local safeVarName = RsFileSafeString attrName
			local varname = ("control_"+safeVarName)
			local handler = #changed
			local stringValue = false
			local dropDownVals = undefined
			case type of
			(
				"checkbox":
				(
					rci.addControl #checkbox varname attrName paramStr:(" triState:"+(if prop then "1" else "0"))
				)
				"editbox":
				(
					rci.addControl #edittext varname attrName paramStr:(" text:\""+(prop as string+"\""))
				)
				"spinner":
				(
					local rangeString = getcontroldata groupName attrName
					if allAttrUniversal then
					(
						try(
							rangeString = getcontroldata groupName attrName nodes[1]
						)catch()
					)
					rci.addControl #spinner varname attrName paramStr:(" type:#"+(classof prop) as string+" range:"+rangeString+" width:110 align:#right offset:[10,0] indeterminate:"+(not allAttrUniversal) as string)
				)
				"combobox":
				(
					local groupName = getattrparentclassname propClassName attrIndex
					dropDownVals = getcontroldata groupName attrName
					if undefined!=dropDownVals then
					(
						local valsArray = filterString dropDownVals ","
						local dropDownValIndex = findItem valsArray prop
						local dropDownValString = "#(\""
						append dropDownValString (RsStringArray valsArray token:("\", \""))
						append dropDownValString "\")"
						rci.addControl #dropdownlist varname attrName paramStr:(" items:"+dropDownValString+" selection:"+(dropDownValIndex as string))
						handler = #selected
						stringValue = true
					)
				)
				undefined:
				(
					continue
				)
			)
			
			if (stringValue == true and dropDownVals != undefined) then
			(
				local thecode = ""
				thecode = append thecode ("setattr ($"+mapContainer.name+") "+attrIndex as string+ " " + varname as string + ".items[val];\n")
				print thecode
				if "" != thecode then
					rci.addHandler varname handler paramStr:"val " codeStr:thecode
			)
			else
			(
				local thecode = ""
				local valString = " val"
				thecode = append thecode ("setattr ($"+mapContainer.name+") "+attrIndex as string+valString+";\n")

				if "" != thecode then
					rci.addHandler varname handler paramStr:"val " codeStr:thecode
			)
		)
		
		local code = "RsSettingWrite @" + rciName + "@ @rollup@ (not down)"
		rci.addHandler rciName #rolledUp paramStr:"down " codeStr:code filter:on
		currentPage = rci.end()
	)
		
	fn SetPage idx =
	(
 		local index = 1
		if currentPage != undefined then
			removeSubRollout tcPage currentPage
		
 		for map in mapContainers do
		(
 			if ( index == idx ) then
 			(
				CreateAttruteRollout map
				if (currentPage != undefined) then
					addSubRollout tcPage currentPage
 			)
 			index = index + 1
		)		
	)
	
	-------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------
	on RsContainerAttributesEditorRoll open do 
	(
		timerCount = 15
		mapContainers = #()
		FindValidMapContainers mapContainers
		
		tc.tabPages.clear()
		for obj in mapContainers do 
		(
			tc.tabPages.add obj.name
		)
		
		chkCountdown.checked = true
		setTimeYes()
		SetPage 1
	)
	
	on tc Click do
	(
		SetPage (tc.SelectedIndex+1)
	)
	
	on btnClose pressed do 
	(
		DestroyDialog RsContainerAttributesEditorRoll 
	)	

	on tmrCount tick do
	(
		if timerCount == 0 then
		(
			DestroyDialog RsContainerAttributesEditorRoll 
		) 
		else
		(
			if chkCountdown.checked == true then 
			(
				timerCount = timerCount - 1
				setTimeYes()
			)
		)			
	)
)

--
-- name: RsContainerAttributesEditorRoll
-- desc: Shows a rollout used to edit the attributes on valid opened map containers.
--
fn RsRsContainerAttributesEditorRoll modal:true countdown:true useSelection:false = 
(
	try destroydialog RsContainerAttributesEditorRoll catch()
	::RsValidMapContainersForEditor = #()
	mapContainers = #()
	if (useSelection == true) then
	(
		::RsValidMapContainersForEditor = $
		FindValidMapContainers mapContainers
	)
	else
	(
		::RsValidMapContainersForEditor = rootscene.world.children
		FindValidMapContainers mapContainers
	)
	
	if (mapContainers.Count <= 0) then
		return false
	
	createdialog RsContainerAttributesEditorRoll escapeEnable:true modal:modal 
	true
)