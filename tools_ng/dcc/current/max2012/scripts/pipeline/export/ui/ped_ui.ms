--
-- File:: pipeline/export/ui/ped_ui.ms
-- Description:: Ped Exporter UI
--
-- Author:: Greg Smith
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 5 August 2009 (refactored)
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- 26/4/2005
-- by Greg Smith
-- by Luke Openshaw 
-- by Marissa Warner-Wu

-- 17/08/2010
-- David Evans: Fixed a problem with one of the global arrays which was causing 
-- data to carry  over between exports.
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/validation.ms"
filein "pipeline/export/peds/validation.ms"
filein "pipeline/export/peds/utils.ms"
filein "pipeline/export/peds/meta_utils.ms"
filein "pipeline/export/rb_image.ms"
filein "pipeline/helpers/materials/texturemetatag.ms"
filein "pipeline/util/cloth.ms"
filein "pipeline/util/rb_script_generator.ms"
filein "rockstar/util/collutil.ms"
filein "rockstar/util/rexreport.ms"
filein "pipeline/util/texturemetatag_utils.ms"
filein "pipeline/util/ruby.ms"
filein "pipeline/util/p4_utils.ms"
filein "pipeline/util/parameters.ms"
filein "pipeline/export/ui/shared_texture_list_ui.ms"
filein "pipeline/export/peds/pedFiles.ms"
filein "pipeline/export/peds/pedExport.ms"
filein "pipeline/ui/SaveDecisionRoll.ms"
filein "pipeline/util/DLCSetup.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global PlayerNamePrefix = "Player*"
global CSName = "CS_*"
global DefaultXML = "5_Male"

global RsTextureList = #()
global RsTextureLoad = "targa file (*.tga)|*.tga|bitmap file (*.bmp)|*.bmp"
global RsPedResourceQueue = #()
global RsMeshToNameMapList = #()
global originalMaterial = undefined

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------

--
-- roll: RsPedDebugging
-- desc: Ped debugging settings rollout.
--
rollout RsPedDebugging "Ped Debug Settings"
(
	---------------------------------------------------------------------------
	-- Widgets
	---------------------------------------------------------------------------
	checkbox chkMeshAsAscii "Export Meshes as ASCII" checked:RsPedMeshAsAscii
	
	---------------------------------------------------------------------------
	-- Events
	---------------------------------------------------------------------------
	on chkMeshAsAscii changed isChecked do
	(
		RsPedMeshAsAscii = isChecked
	)
	
	on RsPedDebugging rolledUp down do 
	(
		RsSettingWrite "rsPedDebugging" "rollup" (not down)
	)
)

--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- PED UTILITIES
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rollout RsPedUtilityRoll "Ped Utilities"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	
	local btnWidth = 160

	hyperlink lnkHelp	"Help?" address:"https://hub.rockstargames.com/display/RSGART/Ped+Exporter#PedExporter-PedUtilities" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	spinner spnCount "Bones" range:[0,100,1] offset:[-2,0] type:#integer across:2
	button btnCheck "Check Count" offset:[0,-3]
	
	button btnRebuild "Rebuild All Images" width:btnWidth

	group "Shared Textures:"
	(
		button btnEditStrmShared "Edit Streamed Shared" width:btnWidth offset:[0,-3] tooltip:"Edit Shared Texture Dictionary lists for Streamed Peds"
		button btnEditCompShared "Edit Component Shared" width:btnWidth offset:[-1,-3] tooltip:"Edit Shared Texture Dictionary lists for Component Peds"
		button btnExportShared 	"Export Shared" width:btnWidth offset:[-1,-3] tooltip:"Export Shared Texture Dictionary lists for Peds" 
		checkbox chkPreviewExport "Preview Export" checked:false
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- check the count of the selected skin
	--------------------------------------------------------------	
	on btnCheck pressed do 
	(
		if (selection.count == 0) do return false
		
		boneroot = rexGetSkinRootBone selection[1]
		
		if boneroot == undefined do 
		(
			boneroot = selection[1]
		)
	
		boneCount = CountFragBones boneroot
		
		if boneCount == spnCount.value then 
		(
			messagebox "Correct number of bones" title:"Correct Bonecount"
		) 
		else 
		(
			messagebox ("There are " + (boneCount as string) + " bones in this hierarchy") title:"Incorrect Bonecount"
		)
	)
	
	--------------------------------------------------------------
	-- rebuild all images
	--------------------------------------------------------------

	on btnRebuild pressed do 
	(
		gRsPedBuildImage.ReBuildAllImages()
	)

	--
	-- Show the Shared Texture Dictionary editor for Peds
	--
	on btnEditStrmShared pressed do 
	(
		RsCreateSharedTexListRoll prefix:"strm_peds" templateXmlFile:( RsConfigGetEtcDir() + "texture/streamedpedtemplates.xml" )
	)
	on btnEditCompShared pressed do 
	(
		RsCreateSharedTexListRoll prefix:"comp_peds" templateXmlFile:( RsConfigGetEtcDir() + "texture/componentpedtemplates.xml" )
	)
	
	--
	-- Export shared texture dictionary for Peds
	--
	on btnExportShared pressed do 
	(
		gRsULog.ClearLogDirectory()
		gRsUlog.init "Ped Export Shared Textures" appendToFile:false
		RsP4LogUserLabelStatus()

		if(RsIsDLCProj()) then
		(
			gRsULog.LogError ("Shared Textures should not be exported with a DLC project open. (" + gRsProject.FriendlyName + ")")
			gRsULog.ValidateJustErrors()
			return false
		)

		--If game is running, cancel export
		if ( RSRemConIsGameRunning queryRestart:true == true ) do
		(
			gRsULog.LogError ("The game is running, export stopped")
			return false
		)
					
		global RsPedSharedTXDsToExport = #()
		
		rollout RsExportSharedTexturesRoll "Texture Compression Template"
		(
			MultiListBox lstSharedTXDs height:15
			button btnExport "Export" width:100 align:#center

			fn exportSharedTXDs = 
			(
				if (RsPedSharedTXDsToExport.count > 0 ) do
				(
					pedtcsclno = gRsPerforce.createChangelist "ped shared tcs and dds auto checkin"	

					local goOn = true
					for filename in RsPedSharedTXDsToExport while goOn do 
					(
						local textureList = sharedTextureListXML()
						textureList.load filename

						-- Get txd-name from xml filename if it wasn't specified in the file itself:
						local txdName = textureList.txd
						if (txdName == undefined) do 
						(
							txdName = (getFilenameFile filename)
						)

						streamed = if ( matchpattern filename pattern:"*strm*" == true ) then true else false
						goOn = RsPedExportSharedTextures txdName textureList.textures overwriteTCS:::RsPedExportRoll.chkOverwriteTCS.checked \
									changelistNum:pedtcsclno streamed:streamed preview:chkPreviewExport.checked
					)
					
					Messagebox "Shared texture dictionary export complete!"
				)
			)

			on RsExportSharedTexturesRoll open do 
			(
				local sharedTxdDir = RsConfigGetMetadataDir() + "textures/shared_textures/"
				local pedSharedXmls = getfiles (sharedTxdDir + "*_peds_*.xml")

				lstSharedTXDs.items = pedSharedXmls
			)
			
			on btnExport pressed do
			(
				RsPedSharedTXDsToExport = for n = lstSharedTXDs.selection collect lstSharedTXDs.items[n]
				exportSharedTXDs()
			)
		)				
		createDialog RsExportSharedTexturesRoll width:600 modal:true
		
		RsPedSharedTXDsToExport = #()
	)
	
	on RsPedUtilityRoll rolledUp down do 
	(
		RsSettingWrite "rspedutils" "rollup" (not down)
	)
)



--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- PED TEXTURES
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout RsPedTexturesRoll "Ped Textures"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////

	hyperlink lnkHelp	"Help?" address:"https://hub.rockstargames.com/display/RSGART/Ped+Exporter#PedExporter-PedTextures" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	listbox lstTextures "Textures"
	button btnAdd "Add" offset:[-50,0]
	button btnEdit "Edit"offset:[-10,-26]
	button btnRemove "Remove"offset:[40,-26]
	button btnAutoFill "Autofill"
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- AJM: skipReadOnlyCheck has been added so that the initial UI
	-- updated from the file doesn't check if readOnly, as no edits
	-- have been made by that point yet
	--------------------------------------------------------------		
	fn setList skipFileCheckAndSave:false = 
	(
		simpleTexList = #()
	
		for tex in RsTextureList do 
		(
			simpleName = RsRemovePathAndExtension tex
			if matchpattern simpleName pattern:"* *" do 
			(
				messagebox ("Warning - texture name contains a space: \n" + simpleName)
			)
			
			append simpleTexList simpleName
		)
		
		lstTextures.items = simpleTexList
		
		if maxfilename == undefined then return 0
		
		savePath = maxfilepath + (RsRemovePathAndExtension maxfilename) + ".pedtex"
		
		if (not skipFileCheckAndSave) then 
		(
			if ( RsIsFileReadOnly savePath == true ) do
			(
				gRsPerforce.add_or_edit savePath silent:true
				Messagebox "pedtex file was read only - it's now been checked out (in your default changelist)"
			)
			saveFile = openfile savePath mode:"wt"

			if saveFile == undefined then return 0

			for texFileName in RsTextureList do 
			(
				print texFileName to:saveFile
			)

			close saveFile
		)
	)

	--------------------------------------------------------------
	--
	--------------------------------------------------------------		
	fn updatePedTex = 
	(
		if maxfilename == undefined then return 0
		if maxfilename == "" then return 0
	
		loadPath = maxfilepath + (RsRemovePathAndExtension maxfilename) + ".pedtex"
		
		loadFile = openfile loadPath mode:"r"
		
		if loadFile == undefined then (
			lstTextures.items = #()			
			return 0
		)
		
		RsTextureList = #()

		while eof loadFile == false do (
		
			fileLine = readline loadFile			
			subLine = substring fileLine 2 (fileLine.count - 2)
			
			if subLine == "" then continue
			
			append RsTextureList subLine
		)
		
		close loadFile
		
		-- Just updating the UI, no need to check if pedTex is readonly just now
		setList skipFileCheckAndSave:true
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	--
	--------------------------------------------------------------		
	on btnAdd pressed do 
	(
		sourcefile = maxfilepath + "textures/tga/in.tga"
	
		newtexturefile = getopenfilename caption:"add a texture file" types:RsTextureLoad filename:sourcefile
		if newtexturefile == undefined then return 0
		
		newtexturefile = RsLowercase(newtexturefile)
		
		if finditem RsTextureList newtexturefile != 0 then (
		
			messagebox "already added"
			return 0
		)
		
		append RsTextureList newtexturefile
		
		setList()
	)
	
	--------------------------------------------------------------
	--
	--------------------------------------------------------------		
	on btnEdit pressed do 
	(
		if lstTextures.items.count == 0 then return 0
		
		newtexturefile = getopenfilename filename:(RsTextureList[lstTextures.selection]) caption:"add a texture file" types:RsTextureLoad
		
		if newtexturefile == undefined then return 0
		
		newtexturefile = RsLowercase(newtexturefile)
			
		if finditem RsTextureList newtexturefile != 0 and finditem RsTextureList newtexturefile != lstTextures.selection then (
		
			messagebox "already added"
			return 0
		)
	
		RsTextureList[lstTextures.selection] = newtexturefile
		
		setList()
	)
	
	--------------------------------------------------------------
	--
	--------------------------------------------------------------		
	on btnRemove pressed do 
	(
		if lstTextures.items.count == 0 then return 0

		deleteitem RsTextureList lstTextures.selection
		
		setList()
	)

	--------------------------------------------------------------
	--
	--------------------------------------------------------------		
	on btnAutoFill pressed do 
	(
		RsTextureList = #()

		textureFolder = getSavePath caption:"Choose your base texture folder" initialDir:(maxfilepath+"textures\highres")
		
		if texturefolder != undefined do (
			textureFolder = textureFolder + "\\"
			rootfolder=textureFolder

			dirs = getDirectories rootfolder
			getTexFromDirectory rootfolder RsTextureList recursive:true searchString:"diff"
		)			
		setList()
	)

	on RsPedTexturesRoll rolledUp down do 
	(
		RsSettingWrite "rspedtex" "rollup" (not down)
	)
	
	on RsPedTexturesRoll open do 
	(
		callbacks.addScript #filePostOpen "RsPedTexturesRoll.updatePedTex()" id:#RsPedTexPostOpen
		updatePedTex()
	)

	on RsPedTexturesRoll close do 
	(
		callbacks.removeScripts #filePostOpen id:#RsPedTexPostOpen
	)
)

--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- PED EXPORT
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout RsPedExportRoll "Ped Export"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////

	hyperlink lnkHelp "Help?" address:"https://hub.rockstargames.com/display/RSGART/Ped+Exporter" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	local btnWidth = 168	
	group "Export Type:" 
	(
		checkbox chkCompExport "Components" checked:true
		checkbox chkPropExport "Props" offset:[0,-2]
		button btnExport "Export" width:52 height:40 pos:[RsPedExportRoll.width - 62, chkCompExport.pos.y - 6]
		
		button btnExportVarMeta "Export Variation Metadata" width:btnWidth tooltip:"Export just the ped .meta file"
		button btnPedValidation "Validate Ped" width:btnWidth offset:[-1,-2]
	)
	
	group "Options:" 
	(
		checkbox chkOverwriteTCS "Overwrite TCS files"
		checkbox chkExportPatch "Preview Export" offset:[0,-5] tooltip:"Export Preview files"
		checkbox chkPreservePreview "Preserve Preview files" offset:[0,-5] tooltip:"Don't clear preview-export folder on export"
		checkbox chkExtractZips "Extract Zips" offset:[0,-5] enabled:true checked:true tooltip:"Extracts the proper zip files prior to export."
		checkbox chkBuildZips "Build Zips" offset:[0,-5] enabled:true checked:true tooltip:"Rebuild zipfiles after export"
		checkbox chkSplitBlends "Split Blends" offset:[0,-5]
		checkbox chkMicroMorphs "Micro Morphs" offset:[0, -5]
		checkbox chkExportFragment "Force Export Fragment" offset:[0,-5] tooltip:"When doing a component export this can force a re-export of the fragment which would otherwise be skipped."
		checkbox chkAutoGenerateLods "Auto-Generate Lods" offset:[ 0, -5 ] checked:false tooltip:"Auto-generate LOD's using Simplygon.\nUses the \"auto_lod_peds\" Simplygon settings."
	)
	
	local btnWidth = 170
	
	group "Export Ped Texture Templates:" 
	(
		button btnTexTemplates "Open" width:btnWidth offset:[0,0]
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	-- Generates a RsPedExportData struct from current control settings:
	fn newPedExportInfo createChangelist:false = 
	(
		if (RsGeneratePedTCSFiles == true) then
			changelistDesc = "Ped TCS and DDS"
		else
			changelistDesc = "Ped DDS"

		RsP4DeleteEmptyChangelists (changelistDesc + "*")
		local clNum = if ( createChangelist ) then ( gRsPerforce.createChangelist changelistDesc 
		) else ( undefined )
		
		if (gPedTextureExportOps != undefined) do (gPedTextureExportOps.changelistNum = clNum)
		
		RsPedExportData compExport:chkCompExport.checked propExport:chkPropExport.checked overwriteTcs:chkOverwriteTCS.checked \
			previewExport:chkExportPatch.checked preservePreview:chkPreservePreview.checked overwriteMeta:false \ 
			extractZips:chkExtractZips.checked buildZips:chkBuildZips.checked splitBlends:chkSplitBlends.checked changelistNum:clNum microMorphs:chkMicroMorphs.checked \
			exportFragment:chkExportFragment.checked
		
	)
	
	fn validSelection =
	(
		if (selection.count != 1) or (not isKindOf selection[1] Dummy) or (selection[1].children.count == 0) do 
		(
			messagebox "Please select the ped dummy to export." title:"Ped Export Error: Invalid selection"
			return false
		)
		
		return true
	)
	
	-- Don't show build-checkbox when Preview is active:
	on chkExportPatch changed state do 
	(
		chkBuildZips.enabled = chkBuildZips.checked = not state
	)
	
	--------------------------------------------------------------
	-- export button pressed
	--------------------------------------------------------------
	on btnExport pressed do 
	(
		-- Warn if p4 integration is off
		if not RsProjectGetPerforceIntegration() do
		(
			choice = RsQueryBoxMultiBtn "Perforce integration is disabled, do you want to continue with a local export?" title:"P4 integration disabled!" \
				timeout:10 width:360 labels:#("Yes", "No") defaultBtn:1
			if choice == 2 do
				return false
		)
		
		if chkAutoGenerateLods.checked do (
			if gRsSimplygonCore.isSdkInstalled() then (
				local obj = ( selection as array )[ 1 ]
				
				if obj != undefined then (
					local oldSelection = getCurrentSelection()
				
					try (
						holdMaxFile()
						
						gRsSimplygonUtils.autoLodCharacter characterName:obj.name showProgress:false
						gRsULog.LogMessage( ( "Successfully auto-generated LOD's for character (" + obj.name + ") using Simplygon." ) )
						
					) catch (
						gRsULog.LogError( ( "An error occurred while auto-generating LOD's for character (" + obj.name + ").\n\n" + getCurrentException() ) )
					
						gRsULog.ValidateJustErrors()
						
						fetchMaxFile quiet:true
						
						return False
					)
					
					select oldSelection
				)
			) else (
				gRsULog.LogError( ( "The Simplygon SDK is not installed!  Cannot auto-generate LOD's." ) )
				
				gRsULog.ValidateJustErrors()
				
				return False
			)
		)
		
		gRsULog.ClearLogDirectory()
		if not (chkCompExport.checked or chkPropExport.checked) do 
		(
			messageBox "Please select an export-type before exporting" title:"Error: no export-type selected"
			return false
		)
		
		gPedTextureExportOps = RsPedTextureExportOps()
		
		gRsPedExport.pedExport (newPedExportInfo())
	)
	
	on btnPedValidation pressed do 
	(
		local pedExportInfo = newPedExportInfo()
		
		RsPedExportValidate (selection as array) pedExportInfo exporting:false
	)
	
	on btnExportVarMeta pressed do 
	(
		gRsULog.ClearLogDirectory()
		-- Check to see that we have something selected
		local sel = selection as array
		gRsULog.init "Ped Validation" appendToFile:false
		
		if not (validSelection()) do 
		(
			return false
		)
		
		pedExportInfo = newPedExportInfo()
		pedExportInfo.init sel[1]
		
		RsMakeSurePathExists pedExportInfo.metaFilePath
		gRsPerforce.sync #(pedExportInfo.metaFilePath) silent:true	
		RSpedIsWriteable #(pedExportInfo.metaFilePath)
		
		-- Strip tags from object-names:
		local renameObjs = #()
		join renameObjs pedExportInfo.obj.children
		if (pedExportInfo.lodObj != undefined) do 
		(
			join renameObjs pedExportInfo.lodObj.children
		)
		if (pedExportInfo.lod2Obj != undefined) do 
		(
			join renameObjs pedExportInfo.lod2Obj.children
		)
		for obj in renameObjs do 
		(
			local newName = (filterString obj.name " ")[1]

			if obj.name != newName do 
			(
				obj.name = newName
			)
		)
		
		-- Don't pass ped props dir in as that's all scooped up for ped prop zips.
		metadataOutputDir = if (pedExportInfo.compExport) then (pedExportInfo.compOutputDir) else "componentpeds"
		local success = gRsPedExport.ExportVariationMetaData pedExportInfo metadataOutputDir
		gRsPerforce.postExportAdd cancel:(not success) exclusive:true
		
		pedExportInfo.revertObjs()
		
		gRsUlog.Validate()
	)
	
	--------------------------------------------------------------
	-- Texture Templates event to start tool
	--------------------------------------------------------------	
	on btnTexTemplates pressed do
	(
		--check selection
		if not (validSelection()) do 
		(
			return false
		)
		
		local pedExportInfo = newPedExportInfo()
		
		pedExportInfo.init selection[1]
		
		local XmlFile, TexFolder
		case of
		(
			(pedExportInfo.isStreamedExport and pedExportInfo.isCutscenePed):
			(
				XmlFile = (RsConfigGetEtcDir() + "texture/streamedpedtemplates.xml")
				TexFolder = "cutspeds"
			)
			(pedExportInfo.isStreamedExport):
			(
				XmlFile = (RsConfigGetEtcDir() + "texture/streamedpedtemplates.xml")
				TexFolder = "streamedpeds"
			)
			Default:
			(
				XmlFile = (RsConfigGetEtcDir() + "texture/componentpedtemplates.xml")
				TexFolder = "componentpeds"
			)
		)
		RsCreateTextureTagTool xmlTemplateFile:XmlFile tcsEdit:True textureFolder:TexFolder
		
		return OK
	)	
	
	on RsPedExportRoll rolledUp down do 
	(
		RsSettingWrite "rsPedExport" "rollup" (not down)
	)
	
	on RsPedExportRoll open do 
	(
		-- Start the Listener Log:
		RsMakeSurePathExists( RsConfigGetLogDir() )
		openLog (RsConfigGetLogDir() + "ListenerLog_PedExporter.txt")
	)
	
	on RsPedExportRoll close do 
	(
		-- Stop the Listener Log:
		closeLog()
	)
)

rollout RsPedExpressionRoll "Ped Expression Export"
(
	label lblMoved1 "Ped Expression Export has moved to"
	label lblMoved2 "RS Export/Expressions Export"
	button btnTakeMeThere "Take me there"

	on btnTakeMeThere pressed do
	(
		filein "pipeline/export/ui/expressions_exporter_ui.ms"
	)
)

rollout RsPedCreateMetadata "Ped Creature Metadata"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	
	dropdownlist lstFileNames "File"
	button btnGenerateCreatureMetadata "Generate Creature Metadata"
	
	local compEnums =#(#("head", "uppr", "lowr", "suse", "feet", "hair", "teef", "p_head", "p_eyes", "p_ears"), #(0, 3, 4, 3, 6, 2, 7, 100, 101, 102))
	local trackidEnums =#(#("track_bone_translation", "track_bone_rotation", "track_facial_control", "track_facial_translation", "track_facial_rotation", "track_generic_control", "track_generic_translation", "track_generic_rotation", "track_animated_normal_maps"), #(0, 1, 24, 25, 26, 33, 34, 35, 23))
	local colourEnums =#(#("r", "g", "b", "a"), #(0, 1, 2, 3))
	
	local FileNameLst = #()
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	fn GetPedCompIDArray node = 
	(
		pedCompIDArray = #()
		property = getUserProp node "WM_Component"

		if property != undefined then
		(
			pedCompIDArray = filterString property ","
		)
		
		pedCompIDArray
	)
	
	fn GetTrackIDArray node = 
	(
		trackIDArray = #()
		translate_property = getUserProp node "translateTrackType"
		rotate_property = getUserProp node "rotateTrackType"

		if translate_property != undefined then
		(
			trackIDArray = filterString (translate_property as String) ","
		)
		
		if rotate_property != undefined then
		(
			trackIDArray = filterString (rotate_property as String) ","
		)
		
		trackIDArray
	)
	
	fn GetShaderEffectArray node =
	(
		shaderEffectArray = #()
		property = getUserProp node "WM_ShaderEffect"

		if property != undefined then
		(
			shaderEffectArray = filterString (property as String) ","
		)
		
		shaderEffectArray
	)
	
	fn GetMaskIDArray node = 
	(
		maskIDArray = #()
		property = getUserProp node "WM_MaskID"

		if property != undefined then
		(
			maskIDArray = filterString (property as String) ","
		)
		
		maskIDArray
	)
	
	fn GetComponentIDArray node =
	(
		componentIDArray = #()
		channel_property = getUserProp node "WM_ChannelID"
		
		if channel_property != undefined then
		(
			componentIDArray = filterString (channel_property as String) ","
		)
		
		componentIDArray
	)
	
	fn GetCommentsArray node = 
	(
		commentsArray = #()
		property = getUserProp node "WM_Comments"
		
		if property != undefined then
		(
			commentsArray = filterString (property as String) ","
		)
		
		commentsArray
	)	
	
	fn GetPedCompID value = 
	(
		compID = -1
		
		if value != undefined then
		(
			for i = 1 to compEnums[1].Count do
			(
				if (toLower value) == compEnums[1][i] then
				(
					compID = compEnums[2][i]
					break
				)
			)
		)
		
		compID
	)
	
	fn GetTrackID value =
	(
		trackID = -1
		
		if value != undefined then
		(
			for i = 1 to trackidEnums[1].Count do
			(
				if (toLower value) == trackidEnums[1][i] then
				(
					trackID = trackidEnums[2][i]
					break
				)
			)
		)
		
		trackID
	)
	
	fn GetComponentID value =
	(
		component_id = -1 
		
		if value != undefined then
		(
			for i = 1 to colourEnums[1].Count do
			(
				if (toLower value) == colourEnums[1][i] then
				(
					component_id = colourEnums[2][i]
					break
				)
			)
		)
		
		component_id
	)
	
	fn LoadDictionaryDescriptions = 
	(
		dictXmlFile = RsConfigGetProjBinConfigDir() + "/characters/metadata_definitions.xml"
		xmlDoc = XmlDocument()	
		xmlDoc.init()
		xmlDoc.load dictXmlFile

		xmlRoot = xmlDoc.document.DocumentElement
		if xmlRoot != undefined then 
		(
			dictelems = xmlRoot.childnodes
			tempDictArray = #()
			for i = 0 to ( dictelems.Count - 1 ) do 
			(
				dictelem = dictelems.itemof(i)
				
				if dictelem.name == "filenames" then
				(
					childdictelems = dictelem.childnodes
					for j = 0 to ( childdictelems.Count - 1 ) do 
					(
						childdictelem = childdictelems.itemof(j)
						shadernameattr = childdictelem.Attributes.ItemOf( "name" )
						
						append FileNameLst shadernameattr.value
					)
				)
			)
		)
	)
	
	fn PopulateUI = 
	(
		LoadDictionaryDescriptions()

		lstFileNames.items = FileNameLst
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
		
	on RsPedCreateMetadata open do 
	(		
		PopulateUI()
	)
	
	on btnGenerateCreatureMetadata pressed do
	(
		xmlDoc = XMlDocument()			
		xmlDoc.init()
					
		xmlRoot = xmlDoc.createelement("CCreatureMetaData")
		xmlDoc.document.AppendChild xmlRoot
		xmlShaderVariableComponents = xmlDoc.createelement("shaderVariableComponents")
		xmlRoot.AppendChild xmlShaderVariableComponents
		
		for node in $objects do
		(
			if classOf node == Circle then --sliders
			(
				pedCompArray = GetPedCompIDArray node
				trackIDArray = GetTrackIDArray node
				maskIDArray = GetMaskIDArray node
				componentIDArray = GetComponentIDArray node
				shaderEffectArray = GetShaderEffectArray node
				commentsArray = GetCommentsArray node
				
				-- user defined data has arrays of data seperated by commas
				for i = 1 to shaderEffectArray.Count do
				(
					xmlItem = xmlDoc.createelement("Item")
					
					xmlComment = xmlDoc.createcomment commentsArray[i]
					xmlItem.AppendChild xmlComment
					
					--pedcompID
					attributearray = #()
					append attributearray (Attribute "value" (GetPedCompID pedCompArray[i]))	
					xmlElem = RsCreateXmlElement "pedcompID" attributearray xmlDoc
					xmlItem.AppendChild(xmlElem);
					
					--maskID
					attributearray = #()
					append attributearray (Attribute "value" maskIDArray[i])
					xmlElem = RsCreateXmlElement "maskID" attributearray xmlDoc
					xmlItem.AppendChild(xmlElem)
					
					--shaderVariableHashString
					xmlElem = xmlDoc.CreateElement("shaderVariableHashString")
					xmlText = xmlDoc.CreateTextNode(shaderEffectArray[i] as String);
					xmlElem.AppendChild(xmlText);
					xmlItem.AppendChild(xmlElem)
					
					--tracks
					attributearray = #()
					append attributearray (Attribute "content" "char_array")
					xmlElem = RsCreateXmlElement "tracks" attributearray xmlDoc
					xmlText = xmlDoc.CreateTextNode((GetTrackID trackIDArray[i]) as String);
					xmlElem.AppendChild(xmlText)
					xmlItem.AppendChild(xmlElem)
					
					--ids
					attributearray = #()
					append attributearray (Attribute "content" "short_array")
					xmlElem = RsCreateXmlElement "ids" attributearray xmlDoc
					hashString = atHash16U(node.name) as String
					index = findString hashString "L"
					hashString = replace hashString index 1 ""
					xmlText = xmlDoc.CreateTextNode(hashString);
					xmlElem.AppendChild(xmlText)
					xmlItem.AppendChild(xmlElem)
					
					--components
					attributearray = #()
					append attributearray (Attribute "content" "char_array")	
					xmlElem = RsCreateXmlElement "components" attributearray xmlDoc
					xmlText = xmlDoc.CreateTextNode((GetComponentID componentIDArray[i]) as String);
					xmlElem.AppendChild(xmlText);
					xmlItem.AppendChild(xmlElem);		
					
					xmlShaderVariableComponents.AppendChild xmlItem
				)
			)
		)
		
		xmlDoc.save(RsConfigGetCommonDir() + "data/anim/creature_metadata/" + lstFileNames.selected + ".xml")
	)
	
	on RsPedCreateMetadata rolledUp down do 
	(
		RsSettingWrite "rsPedCreatureMeta" "rollup" (not down)
	)
)

try CloseRolloutFloater RsPedUtil catch()
RsPedUtil = newRolloutFloater "Rockstar Ped Exporter" 200 430 50 126
addRollout RsSettingsRoll RsPedUtil rolledup:(RsSettingsReadBoolean "rsSettings" "rollup" false)
addRollout RsDecisionControlRoll RsPedUtil rolledup:(RsSettingsReadBoolean "rsdecision" "rollup" false)
addRollout RsPedTexturesRoll RsPedUtil rolledup:(RsSettingsReadBoolean "rsPedTex" "rollup" false)
addRollout RsPedExportRoll RsPedUtil rolledup:(RsSettingsReadBoolean "rsPedExport" "rollup" false)
addRollout RsPedUtilityRoll RsPedUtil rolledup:(RsSettingsReadBoolean "rsPedUtils" "rollup" false)
addRollout RsPedExpressionRoll RsPedUtil rolledup:(RsSettingsReadBoolean "rsPedExpr" "rollup" false)
addRollout RsPedCreateMetadata RsPedUtil rolledup:(RsSettingsReadBoolean "rsPedCreatureMeta" "rollup" false)
addRollout RsPedDebugging RsPedUtil rolledup:(RsSettingsReadBoolean "rsPedDebugging" "rollup" false)
addRollout RsPedDLCCreationRollout RsPedUtil rolledup:(RsSettingsReadBoolean "RsPedDLCCreation" "rollup" false)
