--
-- name: ShowExportReport
-- desc: Shows the export report (one html file for all maps exported)
--	 Each map section only gets a html page written to their stream folder
--	 if the image is built.  The allReportSummaries.html file is written to
--	 when an image is built, with it just copying the entire <body> tag from 
--	 single map section html report into it.
--

---------------------------------------------------------------------------
-- Uses
---------------------------------------------------------------------------
filein "pipeline/export/maps/globals.ms"
filein "pipeline/export/maps/objects.ms"

---------------------------------------------------------------------------
-- Globals
---------------------------------------------------------------------------
global RsMapReportsCount = 60
global RsMapReports = #()
global RsMapReportFilename = undefined
global RsMapReportCountdown = true

--
-- rollout: RsMapExportReportRoll
-- desc: Map export summary report rollout.
--
rollout RsMapExportReportRoll "Map Export Reports" width:600 height:580
(		
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	dotNetControl tc "System.Windows.Forms.TabControl" width:(RsMapExportReportRoll.width - 20) height:25
	dotNetControl wb "System.Windows.Forms.WebBrowser" width:580 height:500
	checkbox 			chkCountdown "Countdown" width:100 across:2 align:#right
	button 				btnClose "Close" width:100 align:#right
	timer 				tmrCount

	-------------------------------------------------------------------------
	-- Locals
	-------------------------------------------------------------------------
	local currentPage = undefined	
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	fn setTimeYes = 
	(
		btnClose.text = "Close (" + (RsMapReportsCount as string) + ")"
	)
	
	fn NavigateToPage filename =
	(
		RsMapReportFilename = filename
		wb.Navigate ( RsMapReportFilename )
		currentPage = RsMapExportReportPage
	)
	
	fn SetPage idx =
	(
		local index = 1
		tc.tabPages.clear()
		for map in RsMapReports do
		(
			local reportFile = (RsConfigGetStreamDir() + "maps/" + map.name + ".html")
			tc.tabPages.Add map.name
			if ( index == idx ) then
			(
				NavigateToPage reportFile
			)
			index = index + 1
		)
		
		tc.tabPages.Add "Export Times"
		if (index == idx) then
		(
			local reportFile = (RsConfigGetStreamDir() + "maps/export_times.html")
			NavigateToPage reportFile
		)
		index = index + 1
		
		tc.SelectedIndex = (idx - 1)
	)
	
	-------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------
	on RsMapExportReportRoll open do 
	(
		global RsMapReportsCount = 60
		chkCountdown.checked = ::RsMapReportCountdown
		SetPage 1
	)
	
	on tc Click do
	(
		SetPage (tc.SelectedIndex+1)
	)
	
	on btnClose pressed do 
	(
		DestroyDialog RsMapExportReportRoll 
	)	

	on tmrCount tick do (

		if RsMapReportsCount == 0 then (
			DestroyDialog RsMapExportReportRoll 
		) else (

			if chkCountdown.checked == true then 
			(
				RsMapReportsCount = RsMapReportsCount - 1
				setTimeYes()
			)
		)			
	)			
)

--
-- name: RsMapShowExportReports
-- desc: Show latest map export reports.
--
fn RsMapShowExportReports maps modal:true countdown:true = 
(
	::RsMapReports = maps
	::RsMapReportCountdown = countdown
		
	try destroydialog RsMapExportReportRoll catch()
	createdialog RsMapExportReportRoll escapeEnable:true modal:modal
	true
)

--
-- name: RsMapDeleteExportReports
-- desc: Deletes the export report summary files.
--	
fn RsMapDeleteExportReports = 
(
	local reportFiles = ( RsConfigGetStreamDir() + "maps/*.html" )
	RsDeleteFiles reportFiles	
)
