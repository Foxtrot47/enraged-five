--
-- File:: pipeline/export/ui/map_txd_ui.ms
-- Description:: Map TXD Exporter Rollout Definition
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 10 June 2009
--

try (destroyDialog RsMapTxdRoll) catch ()
callbacks.removeScripts id:#RsMapTxdExport

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/maptxd.ms"
filein "rockstar/helpers/parentiser.ms"
filein "pipeline/util/p4_utils.ms"
filein "pipeline/export/maps/globals.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

--
-- name: RsMapTxdRoll
-- desc: Map TXD rollout definition.
--
rollout RsMapTxdRoll "Txd Map Export"
(	
	local LRsMapTXD = RsMapTXD()
	local rollWidth = 187
	
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Map_Export#Txd_Map_Export" offset:[6,0] align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	dropdownlist ddlTxdList "Txd's:" width:(rollWidth - 56) align:#left offset:[-9,-20] across:2
	button btnRefresh "Refresh" align:#right width:46 offset:[10,-2]
	
	local btnWidth = (rollWidth / 2) - 6
	
	fn txdPickFilter obj = (getattrclass obj == "Gta Object" )
	pickButton btnPickTxdFromObj "Pick from object" width:btnWidth offset:[-9,-4] align:#left filter:txdPickFilter across:2
	button btnSelectObjUsingTxd "Select by Txd" width:btnWidth offset:[9,-4] align:#right

	group "TXD Actions" 
	(
		button btnExportAllTxds "Export All Txds" width:165
		button btnExportSelTxd "Export Selected Txd" width:165
		button btnExportSelObjsTxds "Export Selected Object(s) Txds" width:165
	)
	group "Texture Actions" 
	(
		button btnExportGTXD "Export Global Txd" width:165
		button btnExportAllTex "Export All Textures" width:165
		button btnExportSelTxdTex "Export Selected Txd Textures" width:165
		button btnExportNMTex "Export NormalMap Textures" width:165
		button btnExportTwiddle "Export Twiddled Normal Map" width:165
	)
	group "Utilities" 
	(
		button btnColourCode "Colour by Txd" align:#left width:165
		button btnColourByCollGroup "Colour by Collision Group" align:#left width:165
	)

	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	
	--
	-- name: RefreshTxds
	-- desc: Refresh Txd list (called from file open callback and during export)
	--
	fn RefreshTxds = 
	(
		local currSel = ddlTxdList.selected
		ddlTxdList.items = LRsMapTXD.GetAllSceneTXDs()
		
		local setSel = findItem ddlTxdList.items currSel
		if (setSel == 0) and (ddlTxdList.items.count != 0) do (setSel = 1)
		ddlTxdList.selection = setSel
	)

	-------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------
	
	--
	-- event: btnRefresh pressed
	-- desc: Refresh our UI list of TXDs in scene.
	--
	on btnRefresh pressed do 
	(	
		RefreshTxds()
	)
	
	on btnPickTxdFromObj picked obj do 
	(
		RefreshTxds()
		
		local selTxd = RsGetTxdFromObject obj includeRefs:true
		
		ddlTxdList.selection = findItem ddlTxdList.items (toLower selTxd)
	)

	--
	-- event: btnExportAllTxds pressed
	-- desc: Export all TXDs
	--
	on btnExportAllTxds pressed do 
	(		
		local maps = for map in RsMapGetMapContainers() where map.is_exportable() collect map
		
		if (maps.count == 0) do 
		(
			messageBox "No maps found to export!"
			return false
		)
		
		if (RSRemConIsGameRunning queryRestart:true) do (return false)
		
		local continueExport = true
		
		for map in maps while continueExport do 
		(
			continueExport = RsPreExport map docheck:false sceneXmlExport:false

			if continueExport do (continueExport = LRsMapTXD.ExportAllTxds map map.objects true)

			if continueExport do (RsPostExport map)
		)
		
		if continueExport and (querybox "Do you wish to build the map(s)?" title:"Build Maps?") and (RsMapReadOnlyBuildCheck maps) do 
		(
			RsMapBuildImages maps
			RsSceneXmlExportStats()
			gRsPerforce.PostExportAdd()
		)
	)
	
	--
	-- event: btnExportSelTxd pressed
	-- desc: Export single highlighted TXD
	--
	on btnExportSelTxd pressed do 
	(	
		local txdIdx = getattrindex "Gta Object" "TXD"
		
		local selectedTxd = ddlTxdList.selected
		if (selectedTxd == undefined) do
		(
			return false
		)
		
		local containerFound = false
		local maps = for map in RsMapGetMapContainers() where map.is_exportable() collect map
		
		if (maps.count == 0) do 
		(
			messageBox "No maps found to export!"
			return false
		)
		
		local txdMap
		
		-- Added for container support, need to find the RsMapContainer that has the Txd
		-- in order to find out which to build again at the end, and for pre and post export checks
		for map in maps while (txdMap == undefined) do 
		(
			for obj in map.objects where (getattrclass obj == "Gta Object" ) while (txdMap == undefined) do 
			(
				if (matchPattern (getattr obj txdIdx) pattern:selectedTxd) do
				(
					txdMap = map
				)
			)
		)
		
		if (txdMap == undefined) or (RSRemConIsGameRunning queryRestart:true) do (return false)

		local continueExport = RsPreExport txdMap docheck:false sceneXmlExport:false

		if continueExport do (continueExport = LRsMapTXD.ExportSingleTXD txdMap txdMap.exportObjects selectedTxd)

		if continueExport do (continueExport = RsPostExport txdMap)
		
		if continueExport and (querybox "Do you wish to build the map?" title:"Build Map?") and (RsMapReadOnlyBuildCheck #(txdMap)) do 
		(
			RsMapBuildImages #(txdMap)
			RsSceneXmlExportStats()
			gRsPerforce.PostExportAdd()
		)
	)
	
	--
	-- event: btnExportSelObjsTxds
	-- desc: Export txd's of selected objects
	--
	on btnExportSelObjsTxds pressed do 
	(
		local maps = for map in RsMapGetMapContainers() where map.is_exportable() and (map.selObjects.count != 0) collect map
		
		if (maps.count == 0) do 
		(
			messageBox "No maps found to export!"
			return false
		)
		
		if (RSRemConIsGameRunning queryRestart:true) do (return false)

		local continueExport = true
		
		for map in maps while continueExport do 
		(	
			continueExport = RsPreExport map docheck:false sceneXmlExport:false

			if continueExport do (continueExport = LRsMapTXD.ExportSelected map.selobjects)

			if continueExport do (continueExport = RsPostExport map)
		)
		
		if continueExport and (querybox "Do you wish to build the map(s)?" title:"Build Maps?") and (RsMapReadOnlyBuildCheck maps) do 
		(
			RsMapBuildImages maps
			RsSceneXmlExportStats()
			gRsPerforce.PostExportAdd()
		)
	)
	
	--
	-- event: btnColourCode pressed
	-- desc: colour code all the objects by which txd's they use
	--
	on btnColourCode pressed do 
	(
		local idxTxd = getattrindex "Gta Object" "TXD"
		
		if idxTxd == undefined do return false

		local txdList = #()
		local txdObjs = #()
		txdcolour = #()
		
		for obj in $objects do 
		(
			if (getattrclass obj) == "Gta Object" do 
			(
				local valTxd = toLower (getattr obj idxTxd)
				
				local txdNum = findItem txdList valTxd
				if (txdNum == 0) do 
				(
					append txdList valTxd
					append txdObjs #()
					txdNum = txdList.count
				)
				append txdObjs[txdNum] obj
			)
		)

		for objList in txdObjs do 
		(
			objList.wireColor = random black white
		)
		
		setNeedsRedraw()
	)
	
	--
	-- event: btnColourByCollGroup pressed
	-- desc: colour code all the objects by which collision group they use
	--
	on btnColourByCollGroup pressed do 
	(
		local idxTxd = getattrindex "Gta Object" "Collision Group"
		
		if idxTxd == undefined do return false

		local txdList = #()
		local txdObjs = #()
		txdcolour = #()
		
		for obj in $objects do 
		(
			if (getattrclass obj) == "Gta Object" do 
			(
				local valTxd = toLower (getattr obj idxTxd)
				
				local txdNum = findItem txdList valTxd
				if (txdNum == 0) do 
				(
					append txdList valTxd
					append txdObjs #()
					txdNum = txdList.count
				)
				append txdObjs[txdNum] obj
			)
		)

		for objList in txdObjs do 
		(
			objList.wireColor = random black white
		)
		
		setNeedsRedraw()
	)

	--
	-- event: btnSelectObjUsingTxd pressed
	-- desc: select all objects that use the current txd
	--
	on btnSelectObjUsingTxd pressed do 
	(
		if (ddlTxdList.selected == undefined) do 
		(
			return false
		)
		txdName = toLower ddlTxdList.selected
		
		local mapObjectList = #()	
		RsGetMapObjects $objects mapObjectList
	
		local txdObjList = LRsMapTXD.GetObjectsWithTxd txdName objList:mapObjectList
		
		clearSelection()
		select txdObjList
		setNeedsRedraw()
	)
	
	--
	-- event: btnExportGTXD pressed
	-- desc: Export the map global texture dictionary.
	--
	on btnExportGTXD pressed do (
	
		gRsULog.ClearLogDirectory()
		gRsUlog.Init "Global Texture Dictionary Export" appendToFile:false
		RsExportMapGTXD()
		gRsUlog.Validate()
	)
	
	--
	-- event: btnExportAllTex pressed
	-- desc: Export all textures for scene objects.
	--
	on btnExportAllTex pressed do (
	
		local maps = RsMapGetMapContainers()	
		gRsUlog.Init "Texture Export" appendToFile:false

		RsP4LogUserLabelStatus()
		
		for map in maps do
		(
			RsPreExport map docheck:false sceneXmlExport:false
			
			txdlist = #()
			txdobjlist = #()
			RsGetTxdList map.objects txdlist txdobjlist

			LRsMapTXD.DeleteTextures map txdlist txdobjlist map.exportObjects
			LRsMapTXD.ExportTextures map txdlist txdobjlist map.exportObjects
		)	
		gRsUlog.Validate()
	)

	--
	-- event: btnExportSelTxdTex pressed
	-- desc: Export all textures for for selected txd
	--	
	on btnExportSelTxdTex pressed do (

		if (ddlTxdList.selected == undefined) do 
		(
			return false
		)
		txdName = toLower(ddlTxdList.selected)		
		txdlist = #(txdName)
		
		local maps = RsMapGetMapContainers()
		gRsUlog.Init "Texture Export" appendToFile:false

		RsP4LogUserLabelStatus()

		for map in maps do
		(
			RsPreExport map docheck:false sceneXmlExport:false
			
			local mapObjectList = #()
			RsGetMapObjects map.objects mapObjectList

			local txdObjList = LRsMapTXD.GetObjectsWithTxd txdName objList:mapObjectList

			LRsMapTXD.DeleteTextures map txdlist txdObjList map.exportObjects
			LRsMapTXD.ExportTextures map txdlist txdObjList map.exportObjects		
		)
		gRsUlog.Validate()
	)

	--
	-- event: btnExportNMTex pressed
	-- desc: Export all normal maps for scene objects.
	--
	on btnExportNMTex pressed do (
	
		local maps = RsMapGetMapContainers()
		gRsUlog.Init "Texture Export" appendToFile:false

		RsP4LogUserLabelStatus()

		for map in maps do
		(
			RsPreExport map docheck:false sceneXmlExport:false
			
			txdlist = #()
			txdobjlist = #()
			
			RsGetTxdList map.objects txdlist txdobjlist
			LRsMapTXD.DeleteTextures map txdlist txdobjlist map.exportObjects normalonly:true
			LRsMapTXD.ExportTextures map txdlist txdobjlist map.exportObjects normalonly:true
		)
		gRsUlog.Validate()
	)
	
	--
	-- event: btnExportTwiddle pressed
	-- desc:
	--
	on btnExportTwiddle pressed do (
	
		inputFilename = getOpenFileName caption:"source file..."
		
		if inputFilename != undefined then (
		
			outputFilename = getSaveFileName caption "target file..." types:"dds files (*.dds)|*.dds" filename:inputFilename

			if outputFilename != undefined then (
				RsRexExportBumpTexture inputFilename outputFilename maxsize:RsOtherTexMaxSize flag:true
			)
		)
	)

	--
	-- event: Rollout open
	-- desc: initialisation of rollout, install event handler to
	--       refresh the TXD list on file open.
	--
	on RsMapTxdRoll open do 
	(
		RefreshTxds()
		callbacks.addScript #filePostOpen "RsMapTxdRoll.RefreshTxds()" id:#RsMapTxdExport
	)
	
	--
	-- event: Rollout close
	-- desc: shutdown of rollout, remove event handler
	--
	on RsMapTxdRoll close do 
	(
		callbacks.removeScripts id:#RsMapTxdExport
	)
	
	on RsMapTxdRoll rolledUp down do 
	(
		RsSettingWrite "rsMapTxd" "rollup" (not down)
	)
)

-- pipeline/export/ui/map_txd_ui.ms
--createDialog RsMapTxdRoll width:200