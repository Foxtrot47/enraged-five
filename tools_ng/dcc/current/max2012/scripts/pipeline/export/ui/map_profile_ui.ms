--
-- Rockstar Debug Exporter For Profiling
-- Rockstar North
-- 1/3/2005
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
--
-- Exporter for map components that doesn't bother with all the map test shit at the start.  Tools use only.
--

-----------------------------------------------------------------------------
-- Global Initialisation
-----------------------------------------------------------------------------
RsMapExportTexturePathCheck = true
RsParentFileList = #() 

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/globals.ms"
filein "pipeline/export/maps/mapgeom.ms"
filein "pipeline/export/maps/collision.ms"

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout RsGeomRoll "Geom Export"
(
	button btnExportSel "Export Selected"
	button btnExportAll "Export All"
	timer tmrClock "Timer" interval:1000
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	--
	-- event: btnExportAll pressed
	-- desc:  Export all geometry
	--
	on btnExportAll pressed do 
	(
		gRsULog.ClearLogDirectory()

		maps = RsMapGetMapContainers()
				
		for map in maps do 
		(	
			RsMapSetupGlobals map
			
			local mapgeom = RsMapGeometry()
			mapgeom.ExportMap map map.exportObjects map.exportObjects

			--RsPostExport map
		)
	)
	
	--
	-- event: btnExportAll pressed
	-- desc:  Export all geometry
	--
	on btnExportSel pressed do 
	(
		maps = RsMapGetMapContainers()
					
		for map in maps do 
		(	
			RsMapSetupGlobals map
			
			local mapgeom = RsMapGeometry()
			mapgeom.ExportSelected()

			--RsPostExport map
		)
	)
)

rollout RsBoundRoll "Bound Export"
(
	button btnExportSel "Export Selected"
	button btnExportAll "Export All"
	
	--
	-- event: btnExportAll pressed
	-- desc:  Export all geometry
	--
	on btnExportAll pressed do 
	(
		gRsULog.ClearLogDirectory()
		local maps = RsMapGetMapContainers()
		for map in maps do 
		(	
			RsMapSetupGlobals map
			RsBoundsRoll.ExportBounds map.objects
		)
	)

	--
	-- event: btnExportAll pressed
	-- desc:  Export all geometry
	--
	on btnExportSel pressed do 
	(
		gRsULog.ClearLogDirectory()
		RsMapResetGlobals()
		RsBoundsRoll.ExportBounds (selection as array)
	)
)

rollout RsAnimRoll "Animation Export"
(
	
)

rollout RsTextureRoll "Texture Export"
(

)

rollout RsTextureRoll "Dependency Checker"
(
	button btnCheckDependencies "Check Dependencies"

	on btnCheckDependencies pressed do 
	(
		local maps = RsMapGetMapContainers()
		RsShowExportDependencyDialogForMap maps:maps
	)
)

--------------------------------------------------------------
-- start here
--------------------------------------------------------------
try CloseRolloutFloater RsDebugExport catch()
RsDebugExport = newRolloutFloater "Rockstar Map Exporter" 200 520 50 126
addRollout RsSettingsRoll RsDebugExport rolledup:false
addRollout RsGeomRoll RsDebugExport rolledup:false
addRollout RsBoundRoll RsDebugExport rolledup:false
addRollout RsAnimRoll RsDebugExport rolledup:false
addRollout RsTextureRoll RsDebugExport rolledup:false
