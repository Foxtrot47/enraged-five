--
-- File:: MapSceneXmlExporter.ms
-- Description:: SceneXml Exporter Rollout Definition
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 11 February 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/scenexml.ms"
filein "pipeline/util/p4_utils.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

rollout RsSceneXmlRoll "SceneXml"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp					"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Map_Export#SceneXml" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button 	btnSaveSceneXml		"Save SceneXml" 			width:120
	button 	btnSaveSceneXmlAs "Save SceneXml As..." width:120
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None	
	
	-------------------------------------------------------------------------
	-- UI Events
	-------------------------------------------------------------------------

	-------------------------------------------------------------------------
	-- "Save SceneXml" pressed
	-------------------------------------------------------------------------
	on btnSaveSceneXml pressed do (
	
		RsMapSceneXmlSave()
	)

	-------------------------------------------------------------------------
	-- "Save SceneXml As" pressed
	-------------------------------------------------------------------------
	on btnSaveSceneXmlAs pressed do (
	
		gRsMapExportFragments.preExportRename()
		
		filename = getSaveFileName caption:"Save SceneXml as..." \
					types:"XML File (*.xml)|*.xml"
		if ( undefined != filename ) then
			RsSceneXmlExportAs filename
		
		gRsMapExportFragments.postExportRename()
	)
	
	on RsSceneXmlRoll rolledUp down do 
	(
		RsSettingWrite "rsSceneXml" "rollup" (not down)
	)
)

-- MapSceneXmlExporter.ms
