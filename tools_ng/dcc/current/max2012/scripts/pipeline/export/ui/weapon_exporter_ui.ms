--
-- File:: weapon_exporter_ui.ms
-- Description:: Weapons exporter UI rollout.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- 
-- Based off of the original by Greg Smith.
--

filein "pipeline/util/p4_utils.ms"
filein "pipeline/export/utils.ms"

-- Weapon-specific includes
filein "pipeline/export/models/weapon.ms"
filein "pipeline/export/ui/weapon_exporter_ui_anim.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

rollout RsWeaponRoll "Weapon Export"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Model_Export#Weapon_Export" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnExport		"Export" 	width:100
	button btnPreviewSel	"Preview Selected" 	width:100
	button btnExtractZIP	"Extract ZIP"				width:100
	button btnBuildZIP 		"Build ZIP" 				width:100
	button btnEditTemplates "Edit Texture TCS Templates" width:150
	
	groupBox boxOptions "Options:" width:(RsWeaponRoll.width - 10)  height:55 offset:[-8,4]
	checkbox chkOverwriteTCS "Overwrite TCS files" offset:[0, 12 - boxOptions.height]
	checkbox chkExportMeshMap	"Export Mesh Map"	checked:false visible:false
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------
	--
	-- event: btnExport pressed
	-- desc: Invokes weapon exporter
	--
	on btnExport pressed do 
	(
		gRsULog.ClearLogDirectory()
		clearSelection()
		local timeStart = timeStamp()
		
		gRsUlog.Init "Weapon Export Selected" appendToFile:false
		RsP4LogUserLabelStatus()
		if ( RSRemConIsGameRunning queryRestart:true == false ) then
		(
			if (RsWeaponExport false chkExportMeshMap.checked $objects overwriteTCS:chkOverwriteTCS.checked) != false do
			(
				Messagebox "Weapon export complete"
			)
		)
		else
		(
			gRsULog.LogError ("The game is running, export selected stopped.")
			gRsUlog.Validate()
			false
		)
		format "Time taken: %s\n" ((timeStamp() - timeStart) / 1000.0)
	)
	
	--
	-- event: btnPreviewSel pressed
	-- desc: Invokes preview weapon exporter on selected weapons
	--
	on btnPreviewSel pressed do 
	(
		RsClearPatchDir()
		if (RsWeaponExport true false selection overwriteTCS:chkOverwriteTCS.checked) != false do
		(
			Messagebox "Preview export complete"
		)
	)
	
	--
	-- event: btnExtractZIP pressed
	-- desc: Extracts the current weapons image into the user's stream.
	--
	on btnExtractZIP pressed do
	(
		RsWeaponExtractZIP()
	)
	
	--
	-- event: btnBuildZIP pressed
	-- desc: Build the weapons image from the configured stream contents.
	--
	on btnBuildZIP pressed do (
		
		gRsUlog.Init "Weapon Build Zip" appendToFile:false
		RsP4LogUserLabelStatus()
		if ( RSRemConIsGameRunning queryRestart:true == false ) then
		(
			if RsWeaponExportBuildZIP() == false then return false
		)
		else
		(
			gRsULog.LogError ("The game is running, build zip skipped.")
			gRsUlog.Validate()
			false
		)		
	)
	
	on btnEditTemplates pressed do 
	(			
		filein "pipeline/helpers/materials/texturemetatag.ms"
		weaponsFolderName = "weapons"
		::RsCreateTextureTagTool xmlTemplateFile:(RsConfigGetEtcDir() + "texture/weapontemplates.xml") tcsEdit:true textureFolder:weaponsFolderName
	)
	
	on RsWeaponRoll rolledUp down do 
	(
		RsSettingWrite "rsWeapon" "rollup" (not down)
	)
)

-- weapon_exporter_ui.ms
