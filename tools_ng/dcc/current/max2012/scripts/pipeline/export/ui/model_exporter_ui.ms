--
-- File:: model_exporter_ui.ms
-- Description:: Simple model mesh and material exporter rollout.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 14 November 2008
--

-- Model-specific includes
filein "pipeline/export/rb_image.ms"
filein "pipeline/util/texturemetatag_utils.ms"
filein "pipeline/export/ui/weapon_exporter_ui.ms"
filein "pipeline/export/model.ms"
filein "pipeline/export/maps/scenexml.ms"
filein "pipeline/util/skinutils.ms"

RsAnimResourceQueue = #()
RsCutPropResourceQueue = #()
RsSelSetArrayAsStrings = #()
RsCutsBatchWarnings = #()

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

--
-- rollout: RsSimpleModelRoll
-- desc: Simple model exporter rollout (IDR/IDD and raw export).
--
rollout RsSimpleModelRoll "Simple Model Export"
(

	---------------------------------------------------------------------------
	-- UI
	---------------------------------------------------------------------------
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Model_Export#Simple_Model_Export" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	group "Raw:"
	(
		button btnExportRawSel			"Export Selected" width:120
		button btnExportRawSelAscii "Export Selected (ASCII)" width:120
		button btnExportRawAll			"Export All" width:120
		button btnExportRawAllAscii "Export All (ASCII)" width:120
	)
	group "Drawable (IDR.ZIP):"
	(
		button btnExportDrawSel "Export Selected..." width:120
	)
	group "Drawable Dictionary (IDD.ZIP):"
	(
		button btnExportDictSel "Export Selected..." width:120
		button btnExportDictAll "Export All..." width:120
	)
	group "Fragment (IFT.ZIP):"
	(
		button btnExportFragSel "Export Selected..." width:120
	)
	group "Options:"
	(
		checkbox chkPreview			"Preview Folder"
		checkbox chkGenerateTXD 	"Generate TXD"
		checkbox chkConvertData 	"Convert Platform Data"
		checkbox chkBuildRPF			"Build RPF"
		label	lblDirectory		"Output Path:"		align:#left
		edittext edtDirectory	text:RsModelOutputDirectory
	)
	
	---------------------------------------------------------------------------
	-- Functions
	---------------------------------------------------------------------------
	
	--
	-- name: GetOutputDirectory
	-- desc: Return output directory based on text entry or preview checkbox.
	--
	fn GetOutputDirectory = (
		
		if ( chkPreview.checked ) then
			RsConfigGetPatchDir()
		else
			edtDirectory.text		
	)
	
	---------------------------------------------------------------------------
	-- Events
	---------------------------------------------------------------------------

	---------------------------------------------------------------------------
	-- Raw
	---------------------------------------------------------------------------

	--
	-- event: btnExportRawSel pressed
	-- desc: Invokes raw model exporter on selected objects.
	--
	on btnExportRawSel pressed do (
	
		if ( 0 == selection.count ) then
			MessageBox "No objects selected." title:"Model Export Error"
	
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
		
		-- Export selected.
		RsModelExportGroup selection asciiMesh:false
	)

	--
	-- event: btnExportRawAllAscii pressed
	-- desc: Invokes raw model exporter on selected objects (ASCII).
	--
	on btnExportRawSelAscii pressed do (
	
		if ( 0 == selection.count ) then
			MessageBox "No objects selected." title:"Model Export Error"
		
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
		
		-- Export selected.
		RsModelExportGroup $selection asciiMesh:true
	)

	--
	-- event: btnExport pressed
	-- desc: Invokes raw model exporter on all scene objects.
	--
	on btnExportRawAll pressed do (
	
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
	
		-- Export all.
		RsModelExportGroup $objects asciiMesh:false
	)
	
	--
	-- event: btnExportRawAllAscii pressed
	-- desc: Invokes raw model exporter on all scene objects (ASCII).
	--
	on btnExportRawAllAscii pressed do (
	
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
		
		-- Export all.
		RsModelExportGroup $objects asciiMesh:true
	)
	
	---------------------------------------------------------------------------
	-- Drawables
	---------------------------------------------------------------------------
	
	--
	-- name: btnExportDrawSel pressed
	-- desc: Export selected object to a drawable (IDD) file.
	--
	on btnExportDrawSel pressed do (
		
		if ( 0 == selection.count ) then
		(
			MessageBox "No objects selected." title:"Model Export Error"
			return false
		)
		
		-- Get Drawable (IDR) filename from user.
		local idr_filename = ( getSaveFileName caption:"Save Drawable (IDR.ZIP) As..." types:"RAGE Drawable (.IDR.ZIP)|*.idr.zip" )
		if ( undefined == idr_filename ) then
		(			
			MessageBox "No Drawable (IDR.ZIP) file specified.  Export aborted." title:"Model Export Error"
			return false
		)
		
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
		
		-- Export selected geometry.
		local drawableDir = #()
		RsModelExportGroup (selection as array) asciiMesh:false dirs:drawableDir
			
		local modelName = RsRemovePathAndExtension drawableDir[1]
		local modelDirectory = (drawableDir[1])
		local outputPath = getFilenamePath idr_filename
		
		local resourceFiles = #()
			
		-- Constuct drawable.
		local buildResult = RsSimpleModelScriptGeneratorStruct.BuildModelResources modelName modelDirectory outputPath generateModelDictionary:false packSeperateTXD:chkGenerateTXD.checked resources:resourceFiles
		
		-- Convert the drawable if set.
		if chkConvertData.checked and buildResult then
			RsSimpleModelScriptGeneratorStruct.ConvertModelResources resourceFiles
	)

	---------------------------------------------------------------------------
	-- Drawable Dictionary
	---------------------------------------------------------------------------	
	
	--
	-- name: btnExportDictSel pressed
	-- desc: Export selected object(s) and create a drawable dictionary (IDD) file.
	--
	on btnExportDictSel pressed do (
	
		if ( 0 == selection.count ) then
		(
			MessageBox "No objects selected." title:"Model Export Error"
			return false
		)
		
		-- Get Drawable Dictionary (IDD) filename from user.
		local idd_filename = ( getSaveFileName caption:"Save Drawable Dictionary (IDD.ZIP) As..." types:"RAGE Drawable Dictionary (.IDD.ZIP)|*.idd.zip" )
		if ( undefined == idd_filename ) then
		(
			MessageBox "No Drawable Dictionary (IDD.ZIP) file specified.  Export aborted." title:"Model Export Error"
			return false
		)
		
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
			
		-- Clear out old export data.
		RsDeleteDirectory (GetOutputDirectory())
		RsMakeSurePathExists (GetOutputDirectory())
		
		-- Export all geometry.
		local drawableDirs = #()
		RsModelExportGroup (selection as array) asciiMesh:false dirs:drawableDirs
		
		local modelName = RsRemovePathAndExtension idd_filename
		local modelDirectory = getFilenamePath (GetOutputDirectory())
		local outputPath = getFilenamePath idd_filename
		
		local resourceFiles = #()
			
		-- Constuct drawable dictionary.
		local buildResult = RsSimpleModelScriptGeneratorStruct.BuildModelResources modelName modelDirectory outputPath generateModelDictionary:true packSeperateTXD:chkGenerateTXD.checked resources:resourceFiles
		
		-- Convert the drawable dictionary if set.
		if chkConvertData.checked and buildResult then
			RsSimpleModelScriptGeneratorStruct.ConvertModelResources resourceFiles
	)
	
	--
	-- name: btnExportDictAll pressed
	-- desc: Export all objects and create a drawable dictionary (IDD) file.
	--
	on btnExportDictAll pressed do (
	
		local asset_filename = undefined
		local buildAssetZip = chkBuildRPF.checked
		
		if (buildAssetZip) then
		(
			asset_filename = ( getSaveFileName caption:"Save (ZIP) As..." types:"ZIP (.zip)|*.zip" )
			if ( undefined == asset_filename ) then
			(
				MessageBox "No Zip file specified.  Export aborted." title:"Model Export Error"
				return false
			)
		)
		else
		(
			asset_filename = ( getSaveFileName caption:"Save Drawable Dictionary (.IDD.ZIP) As..." types:"IDD.ZIP (.idd.zip)|*.idd.zip" )
			if ( undefined == asset_filename ) then
			(
				MessageBox "No drawable dictionary file specified.  Export aborted." title:"Model Export Error"
				return false
			)
		)
		
		-- Clear out old export data.
		RsDeleteDirectory (GetOutputDirectory())
		RsMakeSurePathExists (GetOutputDirectory())
		
		local sceneFiles = #()
		local modelName = RsRemovePathAndExtension asset_filename
		
		-- Export our SceneXML/ITYP file.
		if (buildAssetZip) then
		(
			local exportPath = getFilenamePath (GetOutputDirectory())
			local itypFile = exportPath + "\\" + modelName + ".ityp"
			append sceneFiles itypFile
			
			RsSimpleModelScriptGeneratorStruct.ExportITYPFile itypFile modelName $objects
		)
		
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
		
		-- Export all geometry.
		local drawableDirs = #()
		RsModelExportGroup $objects asciiMesh:false dirs:drawableDirs
		
		local modelDirectory = getFilenamePath (GetOutputDirectory())
		local outputPath = undefined
		local buildTXD = chkGenerateTXD.checked
			
		if buildAssetZip then
		(
			outputPath = GetOutputDirectory()
			buildTXD = true
		)
		else
		(
			outputPath = getFilenamePath asset_filename
		)
		
		local resourceFiles = #()
		
		-- Constuct drawable dictionary.
		local buildResult = RsSimpleModelScriptGeneratorStruct.BuildModelResources modelName modelDirectory outputPath generateModelDictionary:true packSeperateTXD:buildTXD resources:resourceFiles
		
		-- Build our final asset zip or our drawable dictionaries if set.
		if buildAssetZip and buildResult then
		(
			local exportFolder = getFilenamePath asset_filename
			local exportZipFile = exportFolder + "/" + modelName + ".zip"
			
			join resourceFiles sceneFiles
			RsSimpleModelScriptGeneratorStruct.BuildZipFile resourceFiles exportZipFile
			
			local buildFileList = #(exportZipFile)
			
			RsSimpleModelScriptGeneratorStruct.ConvertModelResources buildFileList
		)
		else
		(
			-- Convert the drawable dictionary if set.
			if chkConvertData.checked and buildResult then
				RsSimpleModelScriptGeneratorStruct.ConvertModelResources resourceFiles
		)
	)
	
	
	---------------------------------------------------------------------------
	-- Drawable Dictionary
	---------------------------------------------------------------------------	
	
	--
	-- name: btnExportFragSel pressed
	-- desc: Export selected object and create a fragment (IFT) file.
	--	
	on btnExportFragSel pressed do (

		if ( 0 == selection.count ) then
		(
			MessageBox "No objects selected." title:"Model Export Error"
			return false
		)
		
		-- Get Drawable (IDR) filename from user.
		local ift_filename = ( getSaveFileName caption:"Save Fragment (IFT.ZIP) As..." types:"RAGE Fragment (.IFT.ZIP)|*.ift.zip" )
		if ( undefined == ift_filename ) then
		(			
			MessageBox "No Fragment (IFT) file specified.  Export aborted." title:"Model Export Error"
			return false
		)
		
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
		
		-- Export selected geometry.
		local fragmentDir = #()
		RsModelExportGroup (selection as array) asciiMesh:false dirs:fragmentDir
		
		-- Construct drawable IDR file.
		local script = RsModelOutputDirectory + "/create_ift.rb"
		if ( 0 != ( RsModelScriptGenerator.BuildFragment script ift_filename fragmentDir[1] ) ) then
			MessageBox "Failed to build fragment file." title:"Model Export Error"
	)
	

	---------------------------------------------------------------------------
	-- Options
	---------------------------------------------------------------------------	
	
	--
	-- name: chkPreview state changed
	-- desc: Enable/disable the directry entry box.
	--
	on chkPreview changed state  do
	(
		edtDirectory.enabled = ( not state )
	)
	
	on RsSimpleModelRoll rolledUp down do 
	(
		RsSettingWrite "rsSimpleModelExport" "rollup" (not down)
	)
)

rollout RsPropRoll "Prop Export"
(
	button btnExport "Export"
	button btnPatchExport "Patch Export"
	button btnRebuildAll "Rebuild All"
	button btnBatchExport "Batch Export"
	
--	button btnTagBones "Tag Bones"
	
	
	--------------------------------------------------------------
	-- export the textures require for specific txd's
	--------------------------------------------------------------
	fn ExportTextures texmaplist isbumplist exportDir = (

		RsMakeSurePathExists exportDir

		for i = 1 to texmaplist.count do (

			texmap = texmaplist[i]
			isbump = isbumplist[i]

			stripName = RsStripTexturePath(texmap)
			singleName = (filterstring texmap "+")[1]

			foundFiles = getfiles singleName

			if foundFiles.count > 0 then (

				outputFile = (exportDir + stripName + ".dds")

				if isbump then (

					RsRexExportBumpTexture texmap outputFile
				) else (

					RsRexExportTexture texmap outputFile
				)
			)
		)
	)
	
	fn BuildTexList obj texmaplistexport = (
		idxFragment = getattrindex "Gta Object" "Is Fragment"
		idxTxd = getattrindex "Gta Object" "TXD"
		idxPackTextures = getattrindex "Gta Object" "Pack Textures"
		
		model = obj
				
		if getattrclass model == "Gta Object" then (

			if getattr model idxFragment == true then (

				continue
			)

			if classof model == SpeedTree_4 then (

				continue
			)

			if getattr model idxPackTextures == true then (

				texmaplist = #()
				maxsizelist = #()
				isbumplist = #()

				RsGetTexMapsFromObj model texmaplist maxsizelist isbumplist

				

				for texmapname in texmaplist do (

					append texmaplistexport texmapname
				)

				
			) 
			else 
			(
				objtexmaplist = #()
				objmaxsizelist = #()
				objisbumplist = #()

				RsGetTexMapsFromObj model objtexmaplist objmaxsizelist objisbumplist

				txdname = RsLowercase (getattr model idxTxd)

				texmaplist = #()
				maxsizelist = #()
				isbumplist = #()
				usagelist = #()					


				for j = 1 to objtexmaplist.count do (

					texmapname = objtexmaplist[j]
					maxsize = objmaxsizelist[j]

					addToList = false

					idxFound = finditem texmaplist texmapname

					if idxFound != 0 then (

						if usagelist[idxFound] == 1 then (

							addToList = true	
						)
					)

					if addToList then (

						append texmaplistexport texmapname
					)
				)

				
			)
		)
	)
	
	fn CreateModelLists model = (
		
		cutPropRootDir = RsConfigGetStreamDir() + "cutspeds/"
		cutPropDir = cutPropRootDir + selection[1].name
		
		outfilename = RsConfigGetStreamDir() + "mapscript/cutspeds/" + model.name + "/map.xml"
		
		idxFragment = getattrindex "Gta Object" "Is Fragment"
		idxTxd = getattrindex "Gta Object" "TXD"
		idxPackTextures = getattrindex "Gta Object" "Pack Textures"
		
		-- init xml doc
		modelXml = XmlDocument()
		modelXml.init()


		modelXmlRoot = modelXml.createelement("mapgeom")
		modelXml.document.AppendChild modelXmlRoot
		geoTexElem = modelXml.createelement("geotex")
		geoTexTexElem = modelXml.createelement("geotextex")
		
		modelXmlRoot.AppendChild(geoTexElem)
		modelXmlRoot.AppendChild(geoTexTexElem)
		
		if getattrclass model == "Gta Object" then (
		
			if getattr model idxFragment == true then (

				continue
			)

			if classof model == SpeedTree_4 then (

				continue
			)

			texFilename = cutPropRootDir + "/" + model.name + "/entity.textures"
			RsMakeSurePathExists texFilename

			RsDeleteFiles texFilename
			
			-- Always pack the textures for cutscene props
			--if getattr model idxPackTextures == true then (

				texmaplist = #()
				maxsizelist = #()
				isbumplist = #()

				RsGetTexMapsFromObjNoStrip model texmaplist maxsizelist isbumplist

				texFile = openfile texFilename mode:"w+"

				for j = 1 to texmaplist.count do (

					texmapname = texmaplist[j]
					maxsize = maxsizelist[j]
					texmapnamestrip = RsStripTexturePath texmapname
					texmapnamestrip = (filterstring texmapnamestrip ".")[1]

					outputFile = RsConfigGetTexDir() + texmapnamestrip + ".dds"			

					if isbumplist[j] then (

						RsRexExportBumpTexture texmapname outputFile maxsize:maxsize
					) else (

						RsRexExportTexture texmapname outputFile maxsize:maxsize
					)

					format "%\n" texmapnamestrip to:texFile
				)

				close texFile


				texmaplist = #()
				maxsizelist = #()
				isbumplist = #()

				RsGetTexMapsFromObj model texmaplist maxsizelist isbumplist
				
				objElem = modelXml.CreateElement(model.name)
				geoTexTexElem.AppendChild(objElem)

				for texmapname in texmaplist do (

					texMapElem = modelXml.CreateElement(texmapname)
					objElem.AppendChild(texMapElem)
				)
				
				objElem = modelXml.CreateElement(model.name)
				geoTexElem.AppendChild(objElem)
		
		--	)
			
			
		)
		
		RsMakeSurePathExists outfilename
		result = modelXml.save outfilename
		rubyString = "geo_content = Pipeline::Content::CutscenePropGeo.new(\"" + outfilename + "\", project )\n"
		rubyString = rubyString + "component_group.add_child(geo_content)\n\n"
		append RsCutPropResourceQueue rubyString
		result
		
	)
	
	fn BuildCutPropsQueueContent patchExport:false = (
		print "entered BuildCutPropsQueueContent"
		rubyFileName = RsConfigGetScriptDir() + "/cutspeds/batch_resource.rb"
		RsMakeSurePathExists rubyFileName
		deletefile rubyFileName
		rubyFile = createfile rubyFileName

		RBGenCutPropResourcingScriptPre rubyFile
		for scriptstr in RsCutPropResourceQueue do (
			format scriptstr to:rubyFile
		)
		RBGenResourcingScriptPost rubyFile
		if patchExport == true then (
			RBGenResourcingScriptPatch rubyFile
		)

		close rubyFile

		if ( RsRunRubyScript rubyFileName != 0 ) then (

			messagebox "failed to build model resource"
			return false
		)

		RsCutPropResourceQueue = #()
	)
	
	
	fn ExportProps patchExport:false = (
		
		infoFileName = RsConfigGetBinDir() + "info.rbs"
		
		infoFile = openfile infoFileName mode:"w+"
		format "set_mipmapcount_max(1)\n" to:infoFile 
		format "set_mipmap_interleaving(0)\n" to:infoFile 
		close infoFile

		if selection.count != 1 then (

			messagebox "select just one object to export"
			return 0
		)

		-- Test prop to see that all skin bones belong to the skeleton hierarchy
		errorMessages = #()
		if not RsSkinUtils.ValidateBonesInHierarchy selection[1] errorMessages Then
		(
			ErrorMessage = "Unable to export.\n"
			for message in errorMessages do
			(
				ErrorMessage = ErrorMessage + "\n" + message
			)
			
			messagebox ErrorMessage
			return 0
		)
	
		texmaplist = #()
		maxsizelist = #()
		isbumplist = #()
		
		txdList = #()
		
		txdtexmaplist = #()
		txdmaxsizelist = #()
		txdisbumplist = #()
		txdusagelist = #()
		
		
		RsGetSortedTxdList rootnode.children txdlist
		
		
		for txdname in txdlist do (
				
			inputobjlist = #()
			texmaplist = #()
			maxsizelist = #()
			isbumplist = #()
			usagelist = #()
			
			
			RsGetInputObjectList rootnode.children inputobjlist
			RsGetTexMapsByTxdName texmaplist maxsizelist isbumplist usagelist txdname srcobjlist:inputobjlist isCutsceneObject:true				

			append txdtexmaplist texmaplist
			append txdmaxsizelist maxsizelist
			append txdisbumplist isbumplist
			append txdusagelist usagelist
		)
		
		

		-- export files
		cutPropRootDir = RsConfigGetStreamDir() + "cutspeds/"
		cutPropDir = cutPropRootDir + selection[1].name					

		obj = selection[1]
		objDir = cutPropDir + "/" + (obj.name)

		rexReset()
		graphRoot = rexGetRoot()
		
		
		

		select obj

		entity = rexAddChild graphRoot "entity" "Entity"
		rexSetPropertyValue entity "OutputPath" cutPropDir
--		
		
		select obj
		
		meshobj = rexAddChild entity "mesh" "Mesh"
		rexSetPropertyValue meshobj "OutputPath" cutPropDir
		rexSetPropertyValue meshobj "ExportTextures" "true"
		rexSetPropertyValue meshobj "TextureOutputPath" (RsConfigGetTexDir())
		rexSetPropertyValue meshobj "MeshForSkeleton" "false"
--		rexSetPropertyValue meshobj "MeshAsAscii" "false"
--		rexSetPropertyValue meshobj "MeshSkinOffset" "false"
		rexSetNodes meshobj
		
		RsGetTexMapsFromObj obj texmaplist maxsizelist isbumplist
		
		select obj
		
		skeleton = rexAddChild entity "skeleton" "Skeleton"
		rexSetPropertyValue skeleton "OutputPath" cutPropDir
		rexSetPropertyValue skeleton "ChannelsToExport" "(transX;transY;transZ;)(rotX;rotY;rotZ;)"
		rexSetPropertyValue skeleton "SkeletonWriteLimitAndLockInfo" "true"
		rexSetPropertyValue skeleton "SkeletonRootAtOrigin" "true"
		rexSetPropertyValue skeleton "SkeletonUseBoneIDs" "true"
		rexSetPropertyValue skeleton "SkeletonBoneTypes" (GetToleratedSkelBoneTypeClassIdString())
		rexSetNodes skeleton

			
		select obj
		
		RsSetMapsFlags true diff:true spec:true normal:true
		
		rexExport()
		
		return true
	)
	
	
	fn BatchExportProps projectname = (
		
		-- Load the XML file
		xmlDoc = XmlDocument()	
		xmlDoc.init()
		xmlDoc.load ( RsConfigGetProjBinConfigDir() + "\\anim\\BatchCutscenes.xml" )
	
		gtarootelement = xmlDoc.document.DocumentElement
		rootchildren = gtarootelement.childnodes

		-- Find props node
		for i = 0 to ( rootchildren.Count - 1 ) do (
			child = rootchildren.itemof(i)
			if child.name == "prop_files" then 
			(
				gtarootelement = child
			)
		)

		gtachildren = gtarootelement.childnodes

		SetQuietMode true
		
		-- Loop through every element in the XML group and open and export it.
		for i = 0 to ( gtachildren.Count - 1 ) do (
			elems = gtachildren.itemof(i)
			
			projpath = RsLowercase(RsMakeBackSlashes(RsConfigGetProjRootDir()))
			propfilename =  projpath + "art\\animation\\resources\\props\\cutscene\\" + RsLowercase(elems.GetAttribute( "name" ))
			print propfilename
					
			if RsCheckFileHasLoaded propfilename == true then (

				tokens = filterstring ( RsLowercase(elems.GetAttribute( "name" )) ) "\ ."

				for obj in rootnode.children do (
					if RsLowercase(obj.name) == RsLowercase(tokens[2]) then select obj
				)
				if selection.count == 1 then (
					if ExportProps() == true then (
						CreateModelLists selection[1]
					)
				)
				else append RsCutsBatchWarnings ("Could not find mesh " + RsLowercase(tokens[2]) " in " + elems.GetAttribute( "name" ))
			)
			else append RsCutsBatchWarnings ("Could not load " + elems.GetAttribute( "name" ))
			
		)
		SetQuietMode false
		BuildCutPropsQueueContent()
	)
	
	fn ThrowWarnings batchWarnings = (
		if RsCutsBatchWarnings.count > 0 then (
			global gBatchWarnings = RsCutsBatchWarnings

			rollout RsBatchWarningsRoll "Batch Export Warnings"
			(
				listbox lstMissing items:gBatchWarnings
				button btnOK "OK" width:100 pos:[250,150]

				on btnOK pressed do (

					DestroyDialog RsBatchWarningsRoll
				)	
			)

			CreateDialog RsBatchWarningsRoll width:750 modal:true
		)
	)
	
	
	on btnRebuildAll pressed do (
		rbscript = "content.name == 'cutspeds' and content.xml_type == 'directory'"
		RsBuildAndConvertImage (RsConfigGetStreamDir() + "cutspeds/") rbscript (RsConfigGetProjectName()) dorebuild:true
	)
	
	on btnExport pressed do (
		if ExportProps() == true then (
			CreateModelLists selection[1]
		)
		BuildCutPropsQueueContent()
		if querybox "Wanna build the image?" == true then (
			rbscript = "content.name == 'cutspeds' and content.xml_type == 'directory'"
			RsBuildAndConvertImage (RsConfigGetStreamDir() + "cutspeds/") rbscript (RsConfigGetProjectName())			
		)
	)
	
	on btnPatchExport pressed do (
		RsClearPatchDir()
		if ExportProps patchExport:true == true then (
			CreateModelLists selection[1]
		)
		BuildCutPropsQueueContent patchExport:true
	)
	
	on btnBatchExport pressed do (
		BatchExportProps RsSettingsRoll.lstConfig.selected
		if querybox "Wanna build the image?" == true then (
			rbscript = "content.name == 'cutspeds' and content.xml_type == 'directory'"
			RsBuildAndConvertImage (RsConfigGetStreamDir() + "cutspeds/") rbscript (RsConfigGetProjectName())
			
		)
	)
	
	on RsPropRoll rolledUp down do 
	(
		RsSettingWrite "rsPropExport" "rollup" (not down)
	)
)

-----------------------------------------------------------------------------
-- Entry-Point
-----------------------------------------------------------------------------

try CloseRolloutFloater RsModelUtil catch()
RsModelUtil = newRolloutFloater "RS Model Exporter" 200 500 50 126
addRollout RsSettingsRoll RsModelUtil rolledup:(RsSettingsReadBoolean "rsSettings" "rollup" false)
addRollout RsWeaponRoll RsModelUtil rolledup:(RsSettingsReadBoolean "rsWeapon" "rollup" false)
addRollout RsWeaponAnimRoll RsModelUtil rolledup:(RsSettingsReadBoolean "rsWeaponAnim" "rollup" false)
addRollout RsSimpleModelRoll RsModelUtil rolledup:(RsSettingsReadBoolean "rsSimpleModelExport" "rollup" true)
addRollout RsPropRoll RsModelUtil rolledup:(RsSettingsReadBoolean "rsPropExport" "rollup" false)

-- model_exporter_ui.ms
