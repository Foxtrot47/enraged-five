--
-- File:: pipeline/export/ui/vehicle_settings_ui.ms
-- Description::
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 27 October 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/vehicles/globals.ms"
filein "pipeline/export/vehicles/ruby_script_generator.ms"
filein "pipeline/export/vehicles/settings.ms"
filein "pipeline/util/shared_texture_list.ms"
filein "rockstar/export/settings.ms"

--persistent global pgIsTruckScene

-----------------------------------------------------------------------------
-- UI
-----------------------------------------------------------------------------

--
-- rollout: RsVehicleSettingsRoll
-- desc: Vehicle exporter-specific settings
--
rollout RsVehicleSettingsRoll "Vehicle Settings"
(
	local ColorClass = dotnetclass "System.Drawing.Color"
	---------------------------------------------------------------------------
	-- UI
	---------------------------------------------------------------------------
	dropdownlist 	lstVehicleImage "Vehicle Image:"
	label 				lblIntermDir "Vehicle directory:" align:#left
	edittext			editVehicleDir "" readonly:true --"MaxCustomControls.MaxTextBox" Height:15 --"RSG.Base.Forms.Controls.BorderedColourTextBox" 
	checkbox			chkBuildImage		"Build Image"
	--checkbox			chckIsTruckScene "Is Truck Scene" checked:(pgIsTruckScene!=undefined)
	group "Debug:"
	(
		checkbox		chkAsAscii 		"Export ASCII Meshes"
	)
	
	---------------------------------------------------------------------------
	-- Events
	---------------------------------------------------------------------------

	--
	-- event: lstVehicleImage dropdown item selected
	-- desc:
	--
	on lstVehicleImage selected index do
	(
		RsVehicleImageIndex = index
		RsVehicleSetupGlobals()
		-- RsVehicleDir is set in RsVehicleSetupGlobals()
		editVehicleDir.Text = RsVehicleDir
	)
	
	--
	-- event: chkAsAscii checkbox state changed
	-- desc:
	--
	on chkAsAscii changed state do
	(
		RsVehicleAsAscii = state
	)

	--
	-- event: chkBuildImage checkbox state changed
	-- desc: 
	--
	on chkBuildImage changed state do
	(
		RsVehicleBuildImage = state
	)
	
	/*
	on chckIsTruckScene changed state do
	(
		if state then
			persistent global pgIsTruckScene = true
		else
			persistent global pgIsTruckScene = undefined
		setSaveRequired true
	)
	*/


	--
	-- event: RsVehicleSettingsRoll opened event
	-- desc: On-open event; fetch list of Images to display.
	--
	on RsVehicleSettingsRoll open do
	(	

		if(RsContentTree.ContentTreeHelper == undefined) then 
		(
			RsContentTree.LoadContentTree()
		)

		vehicleNodes = RsContentTree.ContentTreeHelper.GetAllVehiclesExportNodes()
		
		local Images = #()
		
		for node in vehicleNodes do
		(
			friendlyName = undefined
			friendlyName = node.GetParameter "friendlyname" undefined
			
			if ( friendlyname != undefined ) then
			(
				append Images friendlyName
			)
			else
			(
				friendlyName = gRsProject.Name
				gRsUlog.LogWarning ("No 'friendlyname' parameter specified in content tree for vehicle export node.  Using project name instead: " + friendlyName)
				append Images friendlyName
			)
		)

		lstVehicleImage.items = Images
		
		RsVehicleImageIndex = ( RsSettingsReadInteger "rsvehicle" "zipindex" 1 )
		if ( ( RsVehicleImageIndex < 1 ) or ( RsVehicleImageIndex > vehicleNodes.count ) ) then
		(
			format "Vehicle settings have invalid vehicle path index stored: %. Resetting to 1." RsVehicleImageIndex
			RsVehicleImageIndex = 1
		)
		
		RsVehicleAsAscii = ( RsSettingsReadBoolean "rsvehicle" "asascii" false )
		RsVehicleBuildImage = ( RsSettingsReadBoolean "rsvehicle" "buildzip" true )	
		
		-- Validate the range, otherwise set to 1.
		if ( RsVehicleImageIndex < 1 or RsVehicleImageIndex > Images.count ) then
		(
			RsVehicleImageIndex = 1
		)
		RsVehicleDir = vehicleNodes[RsVehicleImageIndex].absolutepath
		if ( not matchpattern RsVehicleDir pattern:"*\\" ) do RsVehicleDir += "\\"
		
		
		lstVehicleImage.selection = RsVehicleImageIndex
		chkAsAscii.checked = RsVehicleAsAscii
		chkBuildImage.checked = RsVehicleBuildImage
		editVehicleDir.Text = RsVehicleDir
		
--		dotNet.addEventHandler (editVehicleDir as dotNetObject) "changed" folderChanged
	)
	
	--
	-- event: RsVehicleSettingsRoll closed event
	-- desc: On-close event; save selected Image index.
	--
	on RsVehicleSettingsRoll close do
	(
		RsSettingWrite "rsvehicle" "zipindex" RsVehicleImageIndex
		RsSettingWrite "rsvehicle" "asascii" RsVehicleAsAscii
		RsSettingWrite "rsvehicle" "buildzip" RsVehicleBuildImage
		RsSettingWrite "rsvehicle" "vehicledir" RsVehicleDir
	)
	/*
	fn UpdateSceneValue = 
	(
		print ("This is a truck scene? "+(pgIsTruckScene!=undefined) as string)
		chckIsTruckScene.checked = (pgIsTruckScene!=undefined)
	)
	*/

	on RsVehicleSettingsRoll rolledUp down do 
	(
		RsSettingWrite "rsVehicleSettings" "rollup" (not down)
	)
)

-- pipeline/export/ui/vehicle_settings_ui.ms
