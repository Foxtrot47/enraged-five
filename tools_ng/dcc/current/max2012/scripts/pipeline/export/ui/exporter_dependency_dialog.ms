--
-- File:: pipeline/export/ui/exporter_dependency_dialog.ms
-- Description:: Pipeline dependency dialog for exporting.
-- 
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 3 November 2011
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/globals.ms"
filein "pipeline/util/assert.ms"
filein "pipeline/util/content.ms"
filein "pipeline/util/p4.ms"
filein "pipeline/util/string.ms"

filein "pipeline/util/scripttimer.ms"
filein "rockstar/export/projectconfig.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global RsExportDependencyDialog = undefined
global RsExportDependencyDialogResult = true
global RsBuildStatus = "" -- information about the status of the build to be output to the top line dependency dialog
global RsRequiredExportDependencies = #()
global RsOptionalExportDependencies = #()
global gSyncAll = 1
global gSyncRequired = 2
global gSyncCancel = 3
global RsSyncFileDecision = gSyncAll
global gForceSync = false

---------------------------------------------------------------------------
-- Structures
---------------------------------------------------------------------------
struct RsExportDependency
(
	pathname,
	labelled,
	pathnameWithLabel,
	
	-- related methods
	fn AddDependency collection pathname labelled:true =
	(
		dependencyExists = false
		
		pathname = RsMakeSafeSlashes (toLower pathname)
		
		for dependency in collection do
		(
			if( dependency.pathname == pathname ) then dependencyExists = true
		)
		
		if( not dependencyExists ) then
		(
			append collection (RsExportDependency pathname:pathname labelled:labelled)
		)
	)
)

---------------------------------------------------------------------------
-- Helper Functions
---------------------------------------------------------------------------

--
-- name: RsSyncDepLogResults
-- desc: Log Perforce result errors and warnings into the Universal Log.
--
fn RsSyncDepLogResults results =
(
	if (results == undefined) then 
	(
		gRsUlog.LogMessage "Perforce Sync Cancelled"
		RsExportDependencyDialogResult = False
	)
	else 
	(
		for result in results do 
		(
			if ( result.HasErrors() ) then
			(
				for error in result.Errors do
				(
					gRsUlog.LogError error
					format "Dependency Sync Error: %\n" error
				)
			)
			if ( result.HasWarnings() ) then
			(
				for warn in result.Warnings do
				(
					gRsUlog.LogWarning warn
					format "Dependency Sync Warning: %\n" warn
				)
			)
		)
	)
)

--
-- name: RsSyncDepList
-- desc: Perforce sync a list of files.
--
fn RsSyncDepList list =
(
	gRsUlog.ProfileStart "Synching Dependencies"
	if ( list.count > 0 ) then
	(
		local switches = if gForceSync then #("-f") else #()

		-- DHM: don't raise exception on warnings or errors.  Handle them if you need to.
		-- Exceptions at this level looks like it kills the sync.  retarded. This should
		-- probably be set on the global gRsPerforce instance upon construction.
		if( gRsPerforce.connected() == false ) then gRsPerforce.connect()
		local P4ExceptionLevel = dotNetClass "P4API.P4ExceptionLevels"
		gRsPerforce.p4.ExceptionLevel = P4ExceptionLevel.NoExceptionOnErrors
		
		local sync_results = gRsPerforce.run "sync" list switches:switches returnResult:true silent:true safeSlashes:false showProgress:true
		RsSyncDepLogResults sync_results
	)
	
	gRsUlog.ProfileEnd context:"Synching Dependencies"
)

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout RsExportDependencyRoll "Export Pipeline Dependencies"
(	
	local help_url = "https://devstar.rockstargames.com/wiki/index.php/ExportDependencyDialog"
	
	---------------------------------------------------------------------------
	-- Widgets
	---------------------------------------------------------------------------
	hyperlink hyperHelp "Help?" color:(color 0 0 255) address:help_url align:#right
	label lblBuildStatus RsBuildStatus align:#left
	label lblRequired "Required Dependencies:" align:#left
	listbox lstRequiredDependencies height:15 items:RsRequiredExportDependencies readonly:true
	label lblOptional "Optional Dependencies:" align:#left 
	listbox lstOptionalDependencies height:15 items:RsOptionalExportDependencies readonly:true
	checkbox chkForce "Force" checked:gForceSync across:4 align:#left
	button btnSyncAll "Sync All" width:120 align:#right
	button btnSyncRequired "Sync Required" width:120 align:#right
	button btnCancel "Cancel" width:120 align:#right
	
	timer tmrCounter active:false interval:1000
	local timerCount = 15

	---------------------------------------------------------------------------
	-- Events
	---------------------------------------------------------------------------
	
	local mousePos
	on tmrCounter tick do 
	(
		-- Pause countdown if mouse has moved:
		if (mousePos == undefined) or (mouse.pos == mousePos) do 
		(
			timerCount -= 1
			
			btnSyncRequired.caption = "Continue (" + timerCount as string + ")"
			
			if (timerCount == 0) do 
			(
				RsExportDependencyDialogResult = true
				DestroyDialog RsExportDependencyRoll
			)
		)
		
		mousePos = mouse.pos
	)
	
	on RsExportDependencyRoll open do
	(
		if ( true == RsAutomatedExport ) then 
		(
			RsExportDependencyDialogResult = true			
			DestroyDialog RsExportDependencyRoll		
		) 
		else
		(
			tmrCounter.active = true
		)
	)
	
	--
	-- event: btnSyncAll pressed
	-- desc: Sync all dependencies.
	--
	on btnSyncAll pressed do
	(
		RsExportDependencyDialogResult = true
		RsSyncFileDecision = gSyncAll
		DestroyDialog RsExportDependencyRoll
	)
	
	--
	-- event: btnSyncRequired pressed
	-- desc: Sync only required dependencies.
	--
	on btnSyncRequired pressed do
	(
		RsExportDependencyDialogResult = true
		RsSyncFileDecision = gSyncRequired
		DestroyDialog RsExportDependencyRoll
	)
	
	--
	-- event: btnCancel pressed
	-- desc: Cancel button pressed; set result and destroy.
	--
	on btnCancel pressed do
	(
		RsExportDependencyDialogResult = false
		RsSyncFileDecision = gSyncCancel
		DestroyDialog RsExportDependencyRoll
	)
	
	on chkForce changed val do
		gForceSync = val
	
) -- RsExportDependencyRoll

-----------------------------------------------------------------------------
-- Entry Point(s)
-----------------------------------------------------------------------------

--
-- name: RsUserHasLabeledData
-- desc: Determines whether or not the user has the latest labelled data
--
fn RsUserHasLabeledData dataDirectory labelType =
(
	local haveExportDataList = dataDirectory + "...#have"
	local labelExportDataList = dataDirectory + "...@" + (RsProjectGetLabelName labelType)
	local diff2_args = #("-q", haveExportDataList, labelExportDataList)

	if( gRsPerforce.connected() == false ) then gRsPerforce.connect()
	local P4ExceptionLevel = dotNetClass "P4API.P4ExceptionLevels"
	gRsPerforce.p4.ExceptionLevel = P4ExceptionLevel.NoExceptionOnErrors
	local diff2_records = ( gRsPerforce.run "diff2" diff2_args returnResult:true silent:true )
	
	local onLabel = true
	
	if undefined==diff2_records then
	(
		gRsUlog.LogMessage ("Error occured while querying record from perforce for "+dataDirectory as string)
		return false
	)
	
	for diff2_record in diff2_records.records while onLabel == true do
	(
		local status = diff2_record.item["status"]
		
		if( status == "content" or status == "types" ) then -- files are different, so check the revisions
		(
			local haveRev = diff2_record.item["rev"] as Integer
			local labelRev = diff2_record.item["rev2"] as Integer
			
			if( haveRev > labelRev ) then -- locally we are beyond the label
			(
				local reason = "not on label because file at " + diff2_record.item["depotFile"] + " is beyond the label"
				gRsUlog.LogMessage reason
				print reason
				onLabel = false
			)
		)
	)
	
	onLabel
)

--
-- name: RsUserHasLabeledBuild
-- desc: Check if the user has the labeled build based on their active platforms
--
fn RsUserHasLabeledBuild =
(
	local activeBuildDirectories = #( RsProjectGetCommonDir() )
	
	-- collect the target directories of all enabled platforms
	local numTargets = RsProjectGetNumTargets()
	for targetIndex = 0 to (numTargets - 1) do
	(
		local targetName = RsProjectGetTargetKey targetIndex
		if( (RsProjectGetTargetEnabled targetName) == true ) then
		(
			append activeBuildDirectories (RsProjectGetTargetDir targetName)
		)
	)
	
	pushPrompt "Checking build-label status..."
	local onLabel = true
	for activeBuildDirectory in activeBuildDirectories while onLabel == true do
	(
		onLabel = RsUserHasLabeledData activeBuildDirectory "current"
	)
	popPrompt()
	
	onLabel
)

--
-- name: RsGetRequiredMapDependencies
-- desc: Return array of mandatory map dependencies.
--
fn RsGetRequiredMapDependencies maps:#() =
(
	local mapDependencies = #()
	
    local mapContainers = maps
    if ( mapContainers == undefined ) then 
        mapContainers = RsMapGetMapContainers()
        
    local dependencies
    
    for map in mapContainers do
    (
        local sourceMapNode = RsContentTree.GetContentNode map.filename

        if ( sourceMapNode != undefined ) then
        (
            dependencies = RsContentTree.contentTreeHelper.GetFilteredDependencies grsBranch sourceMapNode
        )
        
        for dependency in dependencies do
        (
            local pathname = dependency.AbsolutePath
     
            if (pathname != undefined) do 
            (
                RsExportDependency.AddDependency mapDependencies pathname
            )
        )
    )

	mapDependencies
)

--
-- name: RsGetOptionalMapDependencies
-- desc: Return array of optional map dependencies.
--
fn RsGetOptionalMapDependencies =
(
    local mapDependencies = #()
    
    local props = RsContentTree.contentTreeHelper.GetAllPropExportNodes()
    for prop in props do
	(
        local pathname = prop.AbsolutePath
 
        if (pathname != undefined) do 
        (
            RsExportDependency.AddDependency mapDependencies pathname
        )
    )
    
    local interiors = RsContentTree.contentTreeHelper.GetAllInteriorExportNodes()
    for interior in interiors do
	(
        local pathname = interior.AbsolutePath
 
        if (pathname != undefined) do 
        (
            RsExportDependency.AddDependency mapDependencies pathname
        )
    )
    
    mapDependencies
)

--
-- name: RsFilterDependencyListImpl
-- desc: Filter a list of file dependencies; removing those that are up-to-date with the label specified and yielding the pathnames to grab from perforce
--
fn RsFilterDependencyListImpl dependencyList labelToGet progressTitle: =
(
	local filesToGrabAtHead = #()
	local checkFiles = #()
	
	for dependency in dependencyList do
	(
		if( dependency.labelled ) then
		(
			dependency.pathnameWithLabel = dependency.pathname + labelToGet
		)
		else
		(
			dependency.pathnameWithlabel = dependency.pathname + "#head"
			
			append filesToGrabAtHead (toLower (RsMakeBackSlashes dependency.pathname))
		)
		
		append checkFiles dependency.pathnameWithLabel
	)
	
	if not gRsPerforce.connected() do gRsPerforce.connect()
	
	local P4ExceptionLevel = dotNetClass "P4API.P4ExceptionLevels"
	gRsPerforce.p4.ExceptionLevel = P4ExceptionLevel.NoExceptionOnErrors
	local outOfSyncFiles = gRsPerforce.changedFiles checkFiles silent:true showProgress:true progressTitle:progressTitle
	
	if (outOfSyncFiles == undefined) do 
	(
		format "Changed-file filter failed?\n"
		return false
	)

	-- Fix slashes to ensure searchability:
	outOfSyncFiles = for filename in outOfSyncFiles collect (toLower (RsMakeBackSlashes filename))
	
	-- normalise the filtered pathnames & apply the correct label
	local filesToGet = for filename in outOfSyncFiles collect 
	(
		local labelSuffix = if ((finditem filesToGrabAtHead filename) == 0) then labelToGet else "#head"
		filename + labelSuffix
	)
	
	return filesToGet
)

--
-- name: RsFilterDependencyList
-- desc: Filter a list of file dependencies; removing those that are up-to-date.
--
fn RsFilterDependencyList list onLabel progressTitle: =
(
	local filtered_list = #()
	
	if ( list.count != 0 ) then
	(
		if( onLabel == true ) then
		(
			local labelName = RsProjectGetLabelName "current"
			filtered_list = RsFilterDependencyListImpl list ("@" + labelName) progressTitle:progressTitle
		)
		else
		(
			filtered_list = RsFilterDependencyListImpl list "#head" progressTitle:progressTitle
		)
	)
	
	filtered_list
)

--
-- fn	RsExcludePathnames
-- desc	Given a list of paths, this removes any from the pathsToExclude list
--
fn RsExcludePathnames &pathnameList pathsToExclude = 
(
	-- Makes forward slashes
	formattedPathnamesInputList = for pathname in pathnameList collect (RsMakeSafeSlashes pathname)
	formattedPathsToExcludeList = for pathname in pathsToExclude collect (RsMakeSafeSlashes pathname)

	if (formattedPathnamesInputList.count > 0) do
	(
		for pathIdx = formattedPathnamesInputList.count to 1 by -1 do
		(
			for excludePath in formattedPathsToExcludeList do
			(
				-- If there's a match, delete it from the list
				if (findString formattedPathnamesInputList[pathIdx] excludePath != undefined) do
				(
					format "Removing exclude item from input list in RsExcludePathnames: %\n" formattedPathnamesInputList[pathIdx]
					deleteitem pathnameList pathIdx
				)
			)
		)
	)
	
	pathnameList
)

--
-- name: RsShowExportDependencyDialogForMap
-- desc: Show export dependency dialog for a map container.
--
fn RsShowExportDependencyDialogForMap maps:#() forceToHead:false exportFiles: =
(
	if ( RsProjectGetPerforceIntegration() == false ) do ( return true )
	
	format "Show Export Dependencies for maps\n"
	if (exportFiles != unsupplied) do 
	(
		format "Ignoring export files: %\n" (exportFiles as string)
	)
	
	RsAssert (classof maps == Array) message:"Invalid array of RsMapContainer specified."
	
 	RsRequiredExportDependencies = #()
 	RsOptionalExportDependencies = #()
	
	RsRequiredExportDependencies = RsGetRequiredMapDependencies maps:maps 

	join RsOptionalExportDependencies (RsGetOptionalMapDependencies())
 	
	-- Collect list of map names so we add the correct dependencies for the asset combine processor.
	local mapnames = for map in maps collect (toLower map.name)
	
	-- Local reference to struct's AddDependency function:
	local AddDependency = RsExportDependency.AddDependency
	
	-- Add texture-files used by maps, if forcing export then maps are required.
	local mapTextures = RsGetMapTextures maps
	if( rexGetForceTextureExport() ) then 
	(
		for filename in maptextures do 
		(
			AddDependency RsRequiredExportDependencies filename labelled:false
		)
	)else(
		for filename in maptextures do 
		(
			AddDependency RsOptionalExportDependencies filename labelled:false
		)
	)
	
	-- Filter out in-sync Required Dependencies:
	RsRequiredExportDependencies = RsFilterDependencyList RsRequiredExportDependencies onLabel progressTitle:"Filter Required Dependencies:"

	-- Filter out in-sync Optional Dependencies:
	if (RsRequiredExportDependencies != false) do 
	(
		-- This takes the dependency structs and returns a list of strings with perforce label/revisions on the end
		RsOptionalExportDependencies = RsFilterDependencyList RsOptionalExportDependencies onLabel progressTitle:"Filter Optional Dependencies:"
	)
	
	-- filter out any remaining export-files:
	if (exportFiles != unsupplied) do 
	(
		if (RsRequiredExportDependencies != false) do (RsRequiredExportDependencies = RsExcludePathnames RsRequiredExportDependencies exportFiles)
		if (RsOptionalExportDependencies != false) do (RsOptionalExportDependencies = RsExcludePathnames RsOptionalExportDependencies exportFiles)
	)
	
	RsExportDependencyDialogResult = (RsRequiredExportDependencies != false) and (RsOptionalExportDependencies != false)
	
	if RsExportDependencyDialogResult and ( RsRequiredExportDependencies.count > 0 or RsOptionalExportDependencies.count > 0 ) then
	(
		RsSyncFileDecision = gSyncAll
		local message = "Sync Export Pipeline Dependencies?"
		
		if gRsSaveDecMgr.IsEnabledAndRecorded message then
		(
			RsSyncFileDecision = gRsSaveDecMgr.Trigger message
		)
		else
		(
			RsEnableExportDependencyDialog = (RsProjectConfig.GetBoolParameter "Enable Exporter Dependency Dialog")
			
			if RsEnableExportDependencyDialog and not RsAutomatedExport then
			(
				try DestroyDialog RsExportDependencyDialog catch()
				::RsExportDependencyDialog = CreateDialog RsExportDependencyRoll 700 520 modal:true
				
				if gRsSaveDecMgr.IsEnabled() then
					gRsSaveDecMgr.registerDecision message RsSyncFileDecision optionLabels:#("Sync All", "Sync Required", "Cancel")
			)
			else
			(
				--Always sync all dependencies if this feature is toggled on.
				RsSyncFileDecision = gSyncAll
			)
		)
		
		case RsSyncFileDecision of
		(
			gSyncAll:
			(
				if RsExportDependencyDialogResult do RsSyncDepList RsRequiredExportDependencies
				if RsExportDependencyDialogResult do RsSyncDepList RsOptionalExportDependencies
			)
			gSyncRequired:
			(
				if RsExportDependencyDialogResult do RsSyncDepList RsRequiredExportDependencies
			)
		)
	)

	return (gRsULog.Validate() and RsExportDependencyDialogResult)
)

--
-- name: RsShowExportDependencyDialogForAllMaps
-- desc: Show export dependency dialog for all loaded map containers.
--
fn RsShowExportDependencyDialogForAllMaps exportFiles: =
(
	local maps = RsMapGetMapContainers()
	RsShowExportDependencyDialogForMap maps:maps exportFiles:exportFiles
)

-- pipeline/export/ui/exporter_dependency_dialog.ms
--RsShowExportDependencyDialogForAllMaps() -- Test run
