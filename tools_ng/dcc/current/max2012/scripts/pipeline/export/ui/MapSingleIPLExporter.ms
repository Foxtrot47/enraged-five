--
-- File:: MapSingleIPLExporter.ms
-- Description:: Map Sinple IPL Exporter Rollout Definition
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 28 April 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/scenexml.ms"
filein "rockstar/export/settings.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

rollout RsMapSingleIPLRoll "Single IPL Export"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Single_IPL_Export" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button  btnSaveSingleIPL	"Export IPL (single)"	width:140
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------

	--
	-- event: btnSaveSingleIPL clicked
	-- desc: Export a single IPL file.
	--
	on btnSaveSingleIPL pressed do
	(
		-- Export SceneXml File
		local bindir = (RsConfigGetBinDir())
		local ipl_filename = ( getSaveFilename caption:"Save IPL As..." types:"Map IPL File (*.ipl)|*.ipl" )
		
		if ipl_filename != undefined do
		(	
			local scenexml_filename = RsRemoveExtension( ipl_filename ) + ".xml"
	
			-- Export SceneXml XML file
			RsSceneXmlExportAs scenexml_filename
			
			-- Export IPL File from SceneXml File
			local ss = stringStream ""
			format "%/MapExport/MapExportIDEIPL.exe --ipl6 % --single %"  bindir ipl_filename scenexml_filename to:ss 
			format "%\n" (ss as String)

			RsMakeSurePathExists ipl_filename
			if ( 0 != ( DOSCommand (ss as String) ) ) then
			(
				local msg = stringStream ""
				format "IPL export of % failed.\n\nContact tools." ipl_filename to:msg
				MessageBox (msg as string)
			)
		)
	)

	on RsMapSingleIPLRoll rolledUp down do 
	(
		RsSettingWrite "rsMapSingleIPL" "rollup" (not down)
	)
)

--------------------------------------------------------------
-- beginning of execution
--------------------------------------------------------------
try CloseRolloutFloater RsMapSingleIPLUtil catch()
RsMapSingleIPLUtil = newRolloutFloater "Single IPL Exporter" 200 110 50 126
addRollout RsSettingsRoll RsMapSingleIPLUtil rolledup:true
addRollout RsMapSingleIPLRoll RsMapSingleIPLUtil rolledup:false

-- MapSingleIPLExporter.ms
