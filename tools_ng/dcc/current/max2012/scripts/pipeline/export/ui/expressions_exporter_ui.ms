--
-- File:: pipeline/export/ui/expressions_exporter_ui.ms
-- Description:: Expressions Exporter UI
--
-- Author:: Ronny Jivers <jonathan.rivers@rockstarnorth.com>
-- Date:: 10 February 2012
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/validation.ms"
filein "pipeline/export/peds/validation.ms"
filein "pipeline/export/peds/utils.ms"
filein "pipeline/export/peds/meta_utils.ms"
filein "pipeline/export/rb_image.ms"
filein "pipeline/helpers/materials/texturemetatag.ms"
filein "pipeline/util/cloth.ms"
filein "pipeline/util/rb_script_generator.ms"
filein "rockstar/util/collutil.ms"
filein "rockstar/util/rexreport.ms"
filein "pipeline/util/texturemetatag_utils.ms"
filein "pipeline/util/ruby.ms"
filein "pipeline/util/p4_utils.ms"
filein "pipeline/export/ui/shared_texture_list_ui.ms"
filein "pipeline/helpers/arrays.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global gExpressionZipHelper

-----------------------------------------------------------------------------
-- Implementation details
-----------------------------------------------------------------------------
struct RsExpressionZipHelper
(
	extractContinue = true,
	doProgress = false, 
	progressNow, progressCount, 
	
	statusText = "", 
	
	onlyOverwriteNewer = false,
	
	fn setStatusText filenames success = 
	(
		local newText = stringStream ""
		
		local plural = if filenames.count == 1 then "" else "s"
		
		if success then 
		(
			format "Successfully extracted file%:\n\n" plural to:newText
		)
		else 
		(
			format "Extraction failed on file%:\n\n" plural to:newText
		)
		
		for n = 1 to filenames.count do 
		(
			local filename = filenames[n]
			format "%" (filenameFromPath filename) to:newText
			
			if (n != filenames.count) do 
			(
				format "\n" to:newText
			)
		)
		
		statusText = newText as string
	),
	
	-- Called whenever zip-progress or overwrite-warning events are called by DotNetZip:
	fn zipProgressEvent ev arg =  
	(		
		case of 
		(
			(arg.EventType.equals arg.EventType.Extracting_ExtractEntryWouldOverwrite):
			(
				-- Check to see if existing file's timestamp is the same as the archived one:
				local localFile = arg.ExtractLocation + arg.CurrentEntry.filename
				local zipFileTime = arg.CurrentEntry.LastModified
				local localFileTime = (dotnetClass "System.Io.File").GetLastWriteTimeUtc localFile
				
				local dontOverwrite = if onlyOverwriteNewer then 
				(
					-- Only overwrite if archive-file's timestamp is later than the local one:
					(zipFileTime.compareTo localFileTime) <= 0
				)
				else 
				(
					-- Only overwrite if archive-file's timestamp is different to the local one:
					zipFileTime.equals localFileTime
				)
				
				arg.CurrentEntry.ExtractExistingFile = if dontOverwrite then 
				(
					--format "Keeping: %\n" localFile
					arg.CurrentEntry.ExtractExistingFile.DoNotOverwrite
				)
				else 
				(
					--format "Overwriting: % [local: % - zip:%]\n" (filenameFromPath localFile) (localFileTime.tostring "G") (zipFileTime.tostring "G")
					arg.CurrentEntry.ExtractExistingFile.OverwriteSilently
				)
			)
			
			(arg.EventType.equals arg.EventType.Extracting_AfterExtractEntry):
			(
				if doProgress do 
				(
					--format "[%/%]\n" (progressNow += 1) progressCount
					if not (progressUpdate (100.0 * (progressNow += 1) / progressCount)) do 
					(
						extractContinue = false
						arg.cancel()
					)
				)
			)
		)
	),
	
	-- Asks if user wants to sync filenames:
	fn askToGetQuery filenames = 
	(
		local filePlural = if filenames.count == 1 then "" else "s"
		
		local queryText = stringStream ""
		format "Do you want to update file% from Peforce before extracting?\n\n" filePlural to:queryText
		
		for n = 1 to filenames.count do 
		(
			format "%" (filenameFromPath filenames[n]) to:queryText
			
			if (n != filenames.count) do 
			(
				format "\n" to:queryText
			)
		)
		
		RsQueryBoxTimeOut (queryText as string) title:("Zip Extract: Update File" + filePlural + "?") timeout:20
	),
	
	fn haveLatest filenames =
	(
		if array!=classof filenames then
			filenames = #(filenames)
		sync_preview_commands = #( "-n" ) + filenames
		local preview_sync_records = ( gRsPerforce.run "sync" sync_preview_commands returnResult:true silent:true )
		
		return (undefined!=preview_sync_records and (preview_sync_records.records.count == 0))
	),
	
	-- Sync and unzip zipfiles to their extract dirs:
	fn extractFiles filenames extractDirs getLatest:true askToGet:true filters:#() justNewer:false = 
	(
		extractContinue = true
		onlyOverwriteNewer = justNewer
		
		-- only try to get latest if it's necessary
		if getLatest do
		(
			if( haveLatest filenames ) do
			(
				getLatest = false
				askToGet = false
			)
		)
		
		if askToGet do (getLatest = askToGetQuery filenames)

		if getLatest do 
		(
			extractContinue = gRsPerforce.syncChanged filenames silent:true clobberAsk:true
		)
		
		if not extractContinue do 
		(
			statusText = "Sync failed"
			return false
		)

		-- Make a list of files existing before extraction:
		local filesToExtract = #()
		local existingFiles = #()
		local checkedDirs = #()
		
		for dirNum = 1 to extractDirs.count do 
		(
			local extractDir = extractDirs[dirNum]
			
			if (appendIfUnique checkedDirs extractDir) do 
			(
				local dirFilters = filters[dirNum]
				if (dirFilters == undefined) do (dirFilters = #("*.*"))
				
				--join existingFiles (RsFindFilesRecursive extractDir dirFilters)
				for dirFilter in dirFilters do 
				(
					join existingFiles (getFiles ( extractDir + dirFilter))
				)
			)
		)
		
		existingFiles = makeUniqueArray existingFiles
		
		for n = 1 to existingFiles.count do 
		(
			existingFiles[n] = toLower (RsMakeBackSlashes existingFiles[n])
		)
		sort existingFiles

		local extractedNums = #{}
		extractedNums.count = existingFiles.count
		
		local zipObjects = #()
		progressNow = 0
		progressCount = 0
		
		local extractFilenums = for fileNum = 1 to filenames.count where (doesFileExist filenames[filenum]) collect fileNum
		
		-- Create dotNetZipObjects, collecting the overall extract-file count:
		for fileNum = extractFilenums do 
		(
			local zipFilename = filenames[filenum]
			
			local zipObj = (dotNetClass "Ionic.Zip.ZipFile").Read zipFilename
			
			progressCount += zipObj.count
			
			zipObjects[fileNum] = zipObj
		)
		
		doProgress = (progressCount > 0)
		
		if doProgress do 
		(
			local progressString = "Extracting " + (progressCount as string) + " archived files:"
			
			progressStart progressString
		)
		
		for fileNum = extractFilenums while extractContinue do 
		(
			local zipObj = zipObjects[fileNum]
			local extractDir = extractDirs[filenum]
			
			try 
			(
				format "Extracting zip: % to: %\n" filenames[filenum] extractDir
				--pushPrompt ("Extracting zip: " + filenames[filenum])
				
				local listEnumerator = zipObj.entryFilenames.getEnumerator()
				for n = 1 to zipObj.entryFilenames.count do 
				(
					listEnumerator.movenext()
					local extractFilename = toLower (RsMakeBackSlashes (extractDir + listEnumerator.current))
					append filesToExtract extractFilename
	
					local existingFileNum = findItem existingFiles extractFilename
					if (existingFileNum != 0) do 
					(
						extractedNums[existingFileNum] = true
					)
				)
				
				dotNet.addEventHandler zipObj "ExtractProgress" zipProgressEvent
				zipObj.ExtractAll extractDir (dotNetClass "Ionic.Zip.ExtractExistingFileAction").InvokeExtractProgressEvent
				
				popPrompt()
			)
			catch 
			(
				format "Extract error: %\n" (getCurrentException())
				extractContinue = false
				popPrompt()
			)

			if (zipObj != undefined) do 
			(
				zipObj.dispose()
			)
		)
		
		if doProgress do 
		(
			progressEnd()
		)
		
		-- Offer to delete any files that weren't found in the archive(s):
		if extractContinue and (extractedNums.numberSet != extractedNums.count) do 
		(
			local extraFiles = for fileNum = -extractedNums collect existingFiles[fileNum]
			
			local filePlural = if extraFiles.count == 1 then "" else "s"
			local dirPlural = if extractDirs.count == 1 then "" else "s"
			
			local queryText = stringStream ""
			format "% unarchived file% found in extraction-folder%:\n\n" extraFiles.count filePlural dirPlural to:queryText
			
			local keepAdding = true
			for n = 1 to extraFiles.count while keepAdding do 
			(
				format "%\n" extraFiles[n] to:queryText

				if (n >= 20) and (n < extraFiles.count) do 
				(
					format "[...more...]\n" to:queryText
					keepAdding = false
				)
			)
			
			local itThem = if extraFiles.count == 1 then "it" else "them"
			local itThey = if extraFiles.count == 1 then "it" else "they"
			
			format "\nDo you want to delete %?\n\n(If not, %'ll be used when you regenerate the zip%)" itThem itThey dirPlural to:queryText
			
			if queryBox (queryText as string) title:"Delete unarchived files?" do 
			(			
				for filename in extraFiles do 
				(
					setFileAttribute filename #readOnly false
					deleteFile filename
				)
				
				-- Remove any empty directories:
				for dir in (makeUniqueArray extractDirs) do 
				(
					RsRemoveEmptyFolders dir
				)
			)
		)

		setStatusText filenames extractContinue
		
		return extractContinue
	),
	
	fn GetExpressionsZipPathname = 
	(
		local zipItem = RsProjectContentFind "expressions" type:"zip"
		zipItem.path + "/" + zipItem.name + ".zip"
	),
	
	fn GetExpressionsCacheDirectory = RsConfigGetStreamDir() + "expressions/",	

	fn ExtractExpressionsZipToCache getLatest:true askToGet:true justNewer:false = 
	(
		local zipPathname = GetExpressionsZipPathname()
		local outputDirectory = GetExpressionsCacheDirectory()
		
		extractFiles #(zipPathname) #(outputDirectory) getLatest:getLatest askToGet:askToGet justNewer:justNewer
	)
)
gExpressionZipHelper = RsExpressionZipHelper()


-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout RsExpressionsSettingsRoll "Settings"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	
	group "Branch Selection"
	(
		dropdownlist lstBranchSelect
	)
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	fn RefreshBranchDropdown =
	(
		-- Update possible Branches values
		local branchesEnumerator = gRsProject.Branches.GetEnumerator()
		local lstBranchesNames = #()

		-- no collect for all collection types, so iterate
		while branchesEnumerator.MoveNext() do
		(
			append lstBranchesNames branchesEnumerator.Current.Key
		)
		
		lstBranchSelect.Items = lstBranchesNames
		lstBranchSelect.Selection = findItem lstBranchSelect.items (gRsBranch.name)
	)

	on RsExpressionsSettingsRoll open do
	(		
		RefreshBranchDropdown()
	)

	on RsExpressionsSettingsRoll rolledUp down do 
	(
		RsSettingWrite "rsExpressionsSettings" "rollup" (not down)
	)
	
	--------------------------------------------------------------
	-- changed value of branch selection
	--------------------------------------------------------------
	on lstBranchSelect selected idx do
	(
		-- reload project and branch
		local branchName  = lstBranchSelect.items[lstBranchSelect.Selection]
		
		if ( branchName != gRsBranch.name) do
		(
			RsContentTree.SetupProjectAndBranchGlobals dlcMaxFile:(maxfilepath + maxfilename) requestedBranchName:branchName
			RsContentTree.LoadContentTree forceReload:true	
            RsSetRexProceduralList() -- used to reload the procedural list when between branches/core, as they have different procedural.meta files
			RefreshBranchDropdown()
		)
	)	
)
rollout RsExpressionsExportRoll "Expressions Export"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Expressions_Export" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)	
	dropdownlist lstDicts "Dictionaries"
	dropdownlist lstExprs "Expressions"
	checkbox chkP4Integration "Perforce Integration" checked:true
	checkbox chkExportMainFile "Individual Bone Expressions"
	checkbox chkSkipOptimization "Skip Optimization Step"
	checkbox chkExportSkinned "Per Component Expressions"
	checkbox chkPreview "Preview"
	checkbox chkPatch "Patch" checked:true
	checkbox chkFaceInit "FaceInit Expressions"
	checkbox chkBuild "Build" checked:true
	spinner spnFaceInitBlend "FaceInit Blend" range:[0,100,0]
	button btnExportSelectedExpressions "Export Selected Expressions"
	button btnReconstructAllIEDs "Reconstruct All IEDs"
	button btnBuildExpressionsFromIEDs "Build Expressions From IEDs"
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	struct ExpressionDictReference ( name, entries )
	struct ExpressionComponentReference ( name, entries )
	struct ExpressionIgnoredReference ( name, entries )
	local ExpressionDictList = #()
	local ExpressionComponentAdditionList = #()
	local ExpressionIgnoredComponentList = #()
	
	fn compareNode n1 n2 =
	(
		if n1.name > n2.name then
		(
			1
		)
		else if n1.name < n2.name then
		(
			-1
		)
		else
		(
			0
		)
	)
	
	fn GetSkinnedBones garmentObject &boneList = 
	(	
		select garmentObject
		max modify mode
		skinMod = garmentObject.modifiers["skin"]
		
		if skinMod == undefined then(
			gRsUlog.LogWarning("No skin modifier applied to node: " + garmentObject.name)
			return false
		)

		modPanel.setCurrentObject garmentObject.modifiers[#Skin]

		for i = 1 to (skinOps.GetNumberVertices skinMod) do
		(
			for j = 1 to (skinOps.GetVertexWeightCount skinMod i) do
			(
				boneID = (skinOps.GetVertexWeightBoneID skinMod i j)
				boneName = (skinOps.GetBoneName skinMod boneID 0)
				weight = (skinOps.GetVertexWeight skinMod i j)
				
				if weight > 0 then
				(
					boneNode = getNodeByName boneName
					if boneNode != undefined then (
						appendIfUnique boneList boneNode

						-- recurse and get parent bones
						boneNode = boneNode.parent
						while boneNode != undefined and boneNode != garmentObject do
						(
							appendIfUnique boneList boneNode
							boneNode = boneNode.parent
						)	
					)
				)
			)
		)
		
		return true
	)

	fn ResolveExpressionProperties exprName additionalDir:"" =
	(
		local spaceArray = filterString exprName " "
		exprName = spaceArray[1]
		
		local exprDir = RsConfigGetAssetsDir() + "anim/expressions/" + lstDicts.selected + "/"
		exprXML = exprDir + additionalDir + exprName + ".xml"
		exprXMLAttributes = exprDir + additionalDir + exprName + ".xml.attributes"
		exprBin = exprDir + additionalDir + exprName + ".expr"
		exprHashnames = exprDir + additionalDir + exprName + ".expr.hashnames"
		exprBones = exprDir + "Bones/"
		
		return expressionData = #( exprXML, exprXMLAttributes, exprBin, exprHashnames, exprBones)
	)
	
	fn CheckForLatestExprData exprDataArray = 
	(
		local outOfSync = #()
		for exprFileArray in exprDataArray do
			for exprFile in exprFileArray where 
				(getFiles exprFile).count>0 and
				not (gExpressionZipHelper.haveLatest exprFile) 
			do 
				append outOfSync exprFile
		if outOfSync.count>0 then
		(
			local names = for n in outOfSync collect filenameFromPath n
			querybox_message = "You don't have latest source expressions data for \n" + (RsArrayJoin names token:",\n") + ".  Would you like to sync it now?"
			if (querybox querybox_message) then
			(
				gRsPerforce.sync outOfSync
			)
		)
	)
	
	fn GenerateFaceInitControlList = (
		faceInitFile = RsConfigGetProjBinConfigDir() + "/characters/expression_faceinit_controls.dat"
		f = openFile faceInitFile
		boneList = #()
		if f != undefined then
	    	(
	    		while (not eof f) do
	        	(
        			line = readLine f
        			node = getNodeByName line
        			if(node != undefined) then (
        				append boneList node
        			)
        		)
        	)
        	
        	boneList
	)
	
	fn ExportExprs boneList expressionData isFaceInit ignoreControllers:#() = (
		
		exprXML = expressionData[1]
		exprXMLAttributes = expressionData[2]
		exprBin = expressionData[3]
		exprHashnames = expressionData[4]
		exprBones = expressionData[5]
		
		if chkP4Integration.checked then
		(
			exprclno = gRsPerforce.findChangelistByName ("expressions - " + lstDicts.selected)
			if exprclno == undefined then
			(
				exprclno = gRsPerforce.createChangelist ("expressions - " + lstDicts.selected)
			)

			gRsPerforce.add #( "-t", "binary", exprBin ) silent:true
			gRsPerforce.edit exprBin silent:true
			gRsPerforce.add_or_edit exprXML queueAdd:false silent:true
			gRsPerforce.addToChangelist exprclno exprXML
			gRsPerforce.addToChangelist exprclno exprBin
			
			gRsPerforce.add_or_edit exprXMLAttributes queueAdd:false silent:true
			gRsPerforce.addToChangelist exprclno exprXMLAttributes
			gRsPerforce.add_or_edit exprHashnames queueAdd:false silent:true
			gRsPerforce.addToChangelist exprclno exprHashnames
		)
	
		if RsIsFileReadOnly exprXML == true then (
			messageBox (exprXML as string + " is read-only")
			return false
		)
				
		if RsIsFileReadOnly exprBin == true then (
			messageBox (exprBin as string + " is read-only")
			return false
		)
				
		RsMakeSurePathExists exprXML
		RsMakeSurePathExists exprBones
				
		boneList = makeUniqueArray boneList -- make sure the list is unique, dont like dupes
		qsort boneList compareNode --make sure the list is same all time, not in the order of selection	
		
		if not (rageAnimExprExport.Export boneList exprXML "" chkExportMainFile.checked false ignoreControllers) then (
			return false
		)

		if chkExportMainFile.checked then
		(
			return false
		)

		if chkSkipOptimization.checked then
		(
			cmdline = RsConfigGetBinDir() + "anim/makeexpressions.exe -exprdata " + exprXML + " -out " + exprBin + " -nooptimize"
		)
		else
		(
			cmdline = RsConfigGetBinDir() + "anim/makeexpressions.exe -exprdata " + exprXML + " -out " + exprBin
		)

		if isFaceInit == true then
		(
			cmdline = cmdline + " -validateanyinputs -validateblendrate " + (spnFaceInitBlend.value as string)
		)
		
		print cmdline
		if hiddendoscommand cmdline == false then (
		
			messagebox "Failed to build export expression properly"
			return false
		)
	)
	
	fn ExportSelectedExpressionsMain = 
	(
		gRsULog.Init "Expression Export" appendToFile:false

		rageAnimExprExport.SetUlogPath (gRsUlog.Filename())
		
		-- make a list of the faceinit bones and pass them to export
		if chkFaceInit.checked == true then (
			
			if chkExportSkinned.checked then (
				exprData = ResolveExpressionProperties "faceInit" additionalDir:"Components/"
			)
			else (
				exprData = ResolveExpressionProperties "faceInit"
			)
			
			CheckForLatestExprData #(exprData)
			
			faceInitList = GenerateFaceInitControlList()

			if ((ExportExprs faceInitList exprData true) == false) then (
				return false
			)
		)
		
		boneList = selection as array
		if bonelist.count < 1 then (
			gRsUlog.LogError("No objects selected.  Bailing out")
			gRsUlog.Validate()
			return false
		)
		
		-- if we are dealing with skinned objects then we want to iterate these objects not just process them as one chunk as they need seperate xml/expr data
		if chkExportSkinned.checked then
		(
			garmentList = selection as array
			
			-- remove specified components
			for entry in ExpressionIgnoredComponentList do 
			(
				if (tolower entry.name) == (tolower (getFilenameFile maxFileName)) then
				(
					for component in entry.entries do 
					(
						for i = garmentList.count to 1 by -1 where 
						(
							local spaceArray = filterString garmentList[i].name " "
							local trimmedBone = spaceArray[1]
							
							(tolower trimmedBone) == (tolower component)
						) do 
						(
							format "Ignoring Component: %\n" component
							deleteItem garmentList i
						)
					)
				)
			)
			
			result=true
			local expressionData = for g in garmentList collect ResolveExpressionProperties g.name additionalDir:"Components/"
			CheckForLatestExprData expressionData
			
			gRsULog.Init "Expression Export" appendToFile:true skipValidation:true
			
			--progressstart "Exporting expressions"
			for i = 1 to garmentList.count do
			(
				--if not progressUpdate (((i as float) / garmentList.count)*100) do
					--exit

				local ignoreControllers = #()
				if result == false then ( continue )

				boneList.count = 0
				-- grab the bones which are skinned to the object and pass these down
				if GetSkinnedBones garmentList[i] boneList == false then(
					continue
				)
				
				-- we dont want names which have spaces in, fucks things up
				-- find the first space and strip the name down to it
				local spaceArray = filterString garmentList[i].name " "
				local exportname =  spaceArray[1]
				if not matchPattern exportname pattern:"lowr*" then
					ignoreControllers = #("HeelHeight")

				-- add the additions, sliders which dont come under the skinning scope for example
				for j = 1 to ExpressionComponentAdditionList.count do
				(
					-- get the sub string of the component name, this is the component type ie. "lowr_00_u" is "lowr"
					subGarmentName = substring exportname 1 4
					if ((subGarmentName as name) == (ExpressionComponentAdditionList[j].name as name)) then -- case insensitive
					(
						for k = 1 to ExpressionComponentAdditionList[j].entries.count do
						(
							n = getnodebyname ExpressionComponentAdditionList[j].entries[k]
							if n != undefined then
							(
								format "Additional Entry - '%' from '%' on '%'\n" n.name ExpressionComponentAdditionList[j].name exportname
								appendIfUnique boneList n
							)
						)
					)
				)
				
				result = ExportExprs boneList expressionData[i] false ignoreControllers:ignoreControllers
			)
			--progressend()
			
			result = gRsUlog.Validate()
			return result
		)
		else -- non garment processing, so we just do as normal
		(
			local exprData = ResolveExpressionProperties lstExprs.selected
			CheckForLatestExprData #(exprData)
			result = ExportExprs boneList exprData false
			result = gRsUlog.Validate()
			return result
		)
	)
	
	fn GetExpressionSourcePath selectedDict = (
		
		src_dir = RsConfigGetAssetsDir() + "anim/expressions/" + selectedDict + "/"
		if chkExportSkinned.checked then
		(
			src_dir = src_dir + "Components/"
		)
		src_dir
	)
	
	fn GetExpressionsExportDir = (
		--export_dir = (RsConfigGetExportDir core:true)
		export_dir = RsConfigGetExportDir()
		if chkPatch.checked then
		(
			export_dir = export_dir + "Patch/"
		)
		export_dir = export_dir + "anim/expressions/"
		export_dir
	)
	
	fn GetExpressionsWildcard = (
		(GetExpressionsExportDir() + "*.ied.zip")
	)
	
	fn GetExpressionZipPath selectedDict = (
		local exportName = selectedDict
		if (RsIsDLCProj()) do
		(
			exportName = GetPedDlcName()
			if undefined==exportName do
				gRsUlog.LogError("Please provide either an \"outfit_name\" or \"dlc_name\" max-file content node key!")
		)
			
		(GetExpressionsExportDir() + exportName + ".ied.zip")
	)
	
	fn BuildExpressionsDictionary selectedDict preview = (
		
		if ( preview == false ) then (

			--If game is running, cancel export
			if ( RSRemConIsGameRunning queryRestart:true == true ) do
			(
				gRsULog.LogError ("The game is running, export stopped")
				return false
			)
		)

		src_dir = GetExpressionSourcePath selectedDict
		ied_file = GetExpressionZipPath selectedDict 

		--Do not submit changes to the asset pack (e.g. called through a Build All), build the dictionary instead.
		makedir (RsConfigGetScriptDir())
		local expressionsAssetPackScheduleFilename = RsConfigGetScriptDir() + "/assetpack_expressions.xml"
		local expressionScheduleFile = AP3AssetFile()		
		expressionScheduleFile.Initialize()
		local expressionZipArchive = expressionScheduleFile.AddZipArchive ied_file forceCreateIfEmpty:true		
		
		expressionScheduleFile.AddFilesFromFolder expressionZipArchive src_dir			

		if chkP4Integration.checked then
		(
			exprclno = gRsPerforce.findChangelistByName ("expressions - " + lstDicts.selected)
			if exprclno == undefined then
			(
				exprclno = gRsPerforce.createChangelist ("expressions - " + lstDicts.selected)
			)
			
			gRsPerforce.add #( "-t", "ubinary", ied_file ) silent:true
			gRsPerforce.edit ied_file silent:true
			gRsPerforce.addToChangelist exprclno ied_file
		)
		
		buildResult = expressionScheduleFile.BuildAssetFiles()
		if ( buildResult == false ) then
		(
			messagebox "Failed to build expression resources"
			return false
		)

		if ( preview == true ) then (
			AP3Invoke.Convert ied_file args:#("--preview") hidden:true
		)

		return true
	)
	
	fn BuildExpressionsDictionaries dictionaries preview = (
		
		if ( preview == false ) then (

			--If game is running, cancel export
			if ( RSRemConIsGameRunning queryRestart:true == true ) do
			(
				gRsULog.LogError ("The game is running, export stopped")
				return false
			)
		)
		
		local expressionsAssetPackScheduleFilename = RsConfigGetScriptDir() + "/assetpack_expressions.xml"
		local expressionScheduleFile = AP3AssetFile()		
		expressionScheduleFile.Initialize()
		
		local ied_files = #()
		for dictionary in dictionaries do
		(	
			src_dir = GetExpressionSourcePath dictionary
			ied_file = GetExpressionZipPath dictionary
			
			local expressionZipArchive = expressionScheduleFile.AddZipArchive ied_file forceCreateIfEmpty:true		
			expressionScheduleFile.AddFilesFromFolder expressionZipArchive src_dir
			
			append ied_files ied_file
		)
		
		expressionScheduleFile.WriteXmlFile expressionsAssetPackScheduleFilename
			
		if ( not expressionScheduleFile.ValidateXml() ) then
		(
			gRsULog.LogError "Failed to create Expression Asset Pack XML.  See errors above."
			return false
		)
		
		if chkP4Integration.checked then
		(
			exprclno = gRsPerforce.findChangelistByName ("expressions - " + lstDicts.selected)
			if exprclno == undefined then
			(
				exprclno = gRsPerforce.createChangelist ("expressions - " + lstDicts.selected)
			)

			args = #( "-t", "ubinary" )
			join args ied_files
			gRsPerforce.add args silent:true
			gRsPerforce.edit ied_files silent:true
			gRsPerforce.addToChangelist exprclno ied_files
		)
		
		--Creates all of the .zip files
		expressionScheduleFile.BuildAssetFiles()
		
		--Preview all expressions.
		if ( preview == true ) then (
			for dictionary in dictionaries do
			(	
				--	FD: no use for ied_file afterward ?
				--	it seems that GetExpressionsWildcard returns "everything" anyway
				ied_file = GetExpressionZipPath dictionary

				build_success = ( 0 == ( AP3Invoke.Convert #( GetExpressionsWildcard() ) args:#( "--preview" ) debug:true ) )

				if ( build_success == false ) then (
					messagebox "Failed to build expression resources"
					return false
				)
			)
		)
		
		true
	)
	
	-- Checks to see if buildfile is writeable, and offers to check it out from Perforce or make it writable if it's readonly:
	fn IsWriteable zipFiles = 
	(
		if not isKindOf zipFiles array do 
		(
			zipFiles = #(zipFiles)
		)
		
		local notLocals = for zipFile in zipFiles where not doesFileExist zipFile collect zipFile
			
		if notLocals.count != 0 do 
		(		
			format "Doesn't exist locally, auto-syncing:\n"
			for zipFile in notLocals do (format "\t%\n" zipFile)
			gRsPerforce.sync notLocals force:true
		)
		
		gRsPerforce.readOnlyP4Check zipFiles
	)
	
	fn BuildZIP selectedDict = (

		--If game is running, cancel export
		if ( RSRemConIsGameRunning queryRestart:true == true ) do
		(
			gRsULog.LogError ("The game is running, export stopped")
			return false
		)

		if chkP4Integration.checked then
		(
			gRsPerforce.sync #( (GetExpressionsExportDir() + "...") ) silent:true
		)
		
		local ied_file = GetExpressionZipPath selectedDict 
		AP3Invoke.Convert #(ied_file) args:#("--rebuild") hidden:true
		true
	)

	fn BuildZIPs = (

		--If game is running, cancel export
		if ( RSRemConIsGameRunning queryRestart:true == true ) do
		(
			gRsULog.LogError ("The game is running, export stopped")
			return false
		)

		if chkP4Integration.checked then
		(
			gRsPerforce.sync #( (GetExpressionsExportDir() + "...") ) silent:true
		)

		build_success = ( 0 == ( AP3Invoke.Convert #( GetExpressionsWildcard() ) debug:true ) )
			
		if ( build_success == false ) then (
		
			messagebox "Failed to build expression rpf"
			return false
		)
		
		true
	)
	
	fn GetExpressionsForDictionary dictname = (
		for exprDict in ExpressionDictList do (
			if exprDict.name == dictname then return exprDict.entries
		)
		
		undefined 
	)
	
	fn GetDictionaryList = (
		templist = #()
		
		for exprDict in ExpressionDictList do append templist exprDict.name
		
		templist
	)
	
	fn LoadDictionaryDescriptions = (
		dictXmlFile = RsConfigGetProjBinConfigDir() + "/characters/expression_dictionaries.xml"
		xmlDoc = XmlDocument()	
		xmlDoc.init()
		xmlDoc.load dictXmlFile

		xmlRoot = xmlDoc.document.DocumentElement
		if xmlRoot != undefined then (
			dictelems = xmlRoot.childnodes
			tempDictArray = #()
			for i = 0 to ( dictelems.Count - 1 ) do (
				dictelem = dictelems.itemof(i)
				nameattr = dictelem.Attributes.ItemOf( "name" )

				exprDictRef = ExpressionDictReference nameattr.value #()
				append tempDictArray nameattr.value

				exprelems = dictelem.childnodes

				for j = 0 to ( exprelems.Count - 1 ) do (
					exprelem = exprelems.itemof(j)
					exprnameattr = exprelem.Attributes.ItemOf( "name" )
					append exprDictRef.entries exprnameattr.value

				)

				append ExpressionDictList exprDictRef
			)
			
		)
		
	)

	fn LoadComponentAdditions = (
		dictXmlFile = RsConfigGetProjBinConfigDir() + "/characters/expression_components.xml"
		xmlDoc = XmlDocument()	
		xmlDoc.init()
		xmlDoc.load dictXmlFile

		xmlRoot = xmlDoc.document.DocumentElement
		if xmlRoot != undefined then (
			compelems = xmlRoot.childnodes
			for i = 0 to ( compelems.Count - 1 ) do (
				compelem = compelems.itemof(i)
				nameattr = compelem.Attributes.ItemOf( "name" )

				compRef = ExpressionComponentReference nameattr.value #()

				exprelems = compelem.childnodes

				for j = 0 to ( exprelems.Count - 1 ) do (
					exprelem = exprelems.itemof(j)
					exprnameattr = exprelem.Attributes.ItemOf( "name" )
					append compRef.entries exprnameattr.value
				)

				append ExpressionComponentAdditionList compRef
			)
			
		)
	)
	
	fn LoadIgnoredComponents = (
		dictXmlFile = RsConfigGetProjBinConfigDir() + "/characters/ignored_expression_components.xml"
		xmlDoc = XmlDocument()	
		xmlDoc.init()
		xmlDoc.load dictXmlFile

		xmlRoot = xmlDoc.document.DocumentElement
		if xmlRoot != undefined then (
			dictelems = xmlRoot.childnodes
			tempDictArray = #()
			for i = 0 to ( dictelems.Count - 1 ) do (
				dictelem = dictelems.itemof(i)
				nameattr = dictelem.Attributes.ItemOf( "name" )

				exprDictRef = ExpressionIgnoredReference nameattr.value #()
				append tempDictArray nameattr.value

				exprelems = dictelem.childnodes

				for j = 0 to ( exprelems.Count - 1 ) do (
					exprelem = exprelems.itemof(j)
					exprnameattr = exprelem.Attributes.ItemOf( "name" )
					append exprDictRef.entries exprnameattr.value

				)

				append ExpressionIgnoredComponentList exprDictRef
			)
			
		)
		
	)	
	
	fn PopulateExpressionUi = (
		LoadComponentAdditions()
		LoadDictionaryDescriptions()
		LoadIgnoredComponents()
		lstDicts.items = GetDictionaryList()
		
		expressions = GetExpressionsForDictionary lstDicts.selected
		if expressions != undefined then lstExprs.items = expressions
		
		spnFaceInitBlend.value = 4.0
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on RsExpressionsExportRoll open do 
	(
		PopulateExpressionUi()
	)
	
	on lstDicts selected newsel do (
		expressions = GetExpressionsForDictionary lstDicts.selected
		if expressions != undefined then lstExprs.items = expressions
	)
	
	on btnExportSelectedExpressions pressed do 
	(
		if ExportSelectedExpressionsMain() == false then return false
		if BuildExpressionsDictionary lstDicts.selected chkPreview.checked == false then return false
		
		if chkPreview.checked == false and chkBuild.checked == true then (
				BuildZIP lstDicts.selected
		)
	)
	
	on btnReconstructAllIEDs pressed do 
	(
		expressionsSourceDataDirectory = RsConfigGetAssetsDir() + "anim/expressions/..."
		
		if( not (gExpressionZipHelper.haveLatest #(expressionsSourceDataDirectory)) ) then
		(
			if (querybox "You don't have latest source expressions data.  Would you like to sync it now?" title:"Sync source expressions data?") then 
			(
				gRsPerforce.sync #( (expressionsSourceDataDirectory) ) silent:true
			)
		)
		
		directoryList = getDirectories (RsConfigGetAssetsDir() + "anim/expressions/*")
		expressionNames = #()
		
		for directory in directoryList do
		(
			filteredStringLst = filterString directory "\\"
			append expressionNames filteredStringLst[filteredStringLst.count]
		)
		
		okToContinue = true
		
		BuildExpressionsDictionaries expressionNames chkPreview.checked
	)
	
	on btnBuildExpressionsFromIEDs pressed do 
	(
		BuildZIPs()
	)
	
	on RsExpressionsExportRoll rolledUp down do 
	(
		RsSettingWrite "rsExpressionsExport" "rollup" (not down)
	)
)

try CloseRolloutFloater RsExpressionsUtil catch()
RsExpressionsUtil = newRolloutFloater "Rockstar Expressions Exporter" 200 500 50 126
addRollout RsExpressionsSettingsRoll RsExpressionsUtil rolledup:(RsSettingsReadBoolean "rsExpressionsSettings" "rollup" false)
addRollout RsExpressionsExportRoll RsExpressionsUtil rolledup:(RsSettingsReadBoolean "rsExpressionsExport" "rollup" false)
