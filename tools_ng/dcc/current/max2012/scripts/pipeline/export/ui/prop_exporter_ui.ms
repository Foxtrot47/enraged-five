--
-- File:: pipeline/export/ui/prop_exporter_ui.ms
-- Description:: Prop exporter UI rollout.
--
-- Author:: Mike Wilson <mike.wilson@rockstartoronto.com>
-- 
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------

-- Additional includes
filein "rockstar/util/anim.ms"
filein "pipeline/util/p4_utils.ms"
filein "pipeline/export/AP3Asset.ms"

global gRsClipmaxCacheSubDir = "clip_max"
global gRsClipmaxAssetSubDir = "clipmax"
global RsClipMaxAssetDir = RsConfigGetExportDir()+"anim/"+gRsClipmaxAssetSubDir+"/"
global gCurrAnimFolderFiles = #()

rollout RsAnimationSelectRoll "Animation Select"
(
	multilistbox exportItems ""
	button btnOk "OK"
	
	on RsAnimationSelectRoll open do
	(
		exportItems.items = for item in gCurrAnimFolderFiles collect (filenameFromPath item)
		exportItems.selection = #{1..gCurrAnimFolderFiles.count}
	)
	on RsAnimationSelectRoll close do
	(
		local newFileSelection = #()
		for item in (exportItems.selection as array) do 
			append newFileSelection (gCurrAnimFolderFiles[item])
		gCurrAnimFolderFiles = newFileSelection
	)
	
	on btnOk pressed do
	(
		DestroyDialog RsAnimationSelectRoll
	)
)

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------
rollout RsPropAnimationRoll "Prop Animation Exporter"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	spinner spnAnimStartFrame "Animation Start Frame:" height:16 range:[0,10000,1] type:#integer
	spinner spnAnimEndFrame "Animation End Frame:" height:16 range:[0,10000,1] type:#integer
	editText edtAnimName "Animation Name:" height:16
	editText edtDictName "Dictionary Name:" height:16
	editText edtAnimPath "Animation Path:" height:16 width:210
	checkbox chkSpecFileToggle "Generate Spec File" checked:false
	editText edtAnimSpec "Spec File Path:" height:16 width:210
	button btnBrowseAnimation "Browse" width:50 pos:[225,90] height:17
	button btnBrowseSpec "Browse" width:50 pos:[225,130] height:17
	dropdownlist lstCompressionFiles "Compression File:"
	dropdownlist lstSkelFiles "Skel File:"
	button btnExportAnimation "Export" width:100
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------

	on btnBrowseAnimation pressed do (

		if edtAnimPath.text != "" then
		(
			RsMakeSurePathExists edtAnimPath.text
		)

		f = getSavePath caption:"Select Animation Path:" initialDir:edtAnimPath.text
		if f != undefined then
			edtAnimPath.text = f
	)

	on btnBrowseSpec pressed do (
		f = getOpenFileName caption:"Open a Spec File:" types:"Rockstar Spec File(*.xml)|*.xml" filename:edtAnimSpec.text
		if f != undefined then
			if doesFileExist f then
				edtAnimSpec.text = f
	)
	
	fn selectionChanged =
	(
		if selection.count>0 then
			edtDictName.text = selection[1].name
	)

	fn GetCompressionList = (

		templist = #()

		files = getFiles (RsConfigGetProjBinConfigDir() + "\\anim\compression_templates\\*.txt")
		for f in files do (
			append templist (filenameFromPath f)
		)

		templist
	)

	fn GetSkelList = (

		templist = #()

		files = getFiles (RsConfigGetProjBinConfigDir() + "\\anim\skeletons\\*.skel")
		for f in files do (
			append templist (filenameFromPath f)
		)

		templist
	)

	on RsPropAnimationRoll open do (
		spnAnimEndFrame.value = animationRange.end
		spnAnimStartFrame.value = 0

		edtAnimSpec.text = RsConfigGetProjRootDir() + "tools/etc/config/anim/control_templates/Prop_RotTrans_Scale_spec.xml"
		edtAnimPath.text = maxfilepath + "anim/"

		lstCompressionFiles.items = GetCompressionList()
		lstSkelFiles.items = GetSkelList()

-- 		callbacks.removeScripts id:#PropAnimExporterCallback
-- 		callbacks.addscript #selectionSetChanged "::RsPropAnimationRoll.selectionChanged()" id:#PropAnimExporterCallback
--		selectionChanged()
	)
	on RsPropAnimationRoll close do 
	(
--		callbacks.removeScripts id:#PropAnimExporterCallback
	)

	fn GetBoneListRecursive boneList bone = (
		append boneList bone

		for childobj in bone.children do (
			GetBoneListRecursive boneList childobj
		)
	)

	fn GetBoneList root = (
		local boneList = #()
		append boneList root

		GetBoneListRecursive boneList root

		boneList
	)

	fn GetRootBoneRecursive bone = (
		
		if( bone.parent != undefined ) then
		(
			GetRootBoneRecursive bone.parent
		)
		else
		(
			return bone
		)
	)

	fn GetRootBone bone = (
		
		return GetRootBoneRecursive bone
	)

	fn PackAnims fullPath dictionaryName = (
		--If game is running, cancel export
		if ( RSRemConIsGameRunning queryRestart:true == true ) do
		(
			gRsULog.LogError ("The game is running, export stopped")
			return false
		)

		local assetPath = RsClipMaxAssetDir+dictionaryName+".icd.zip"
		
		gRsPerforce.sync (RsClipMaxAssetDir + "...")
		if not gRsPerforce.add_or_edit assetPath do
		(
			gRsUlog.LogWarning ("Couldn't check out asset: "+assetPath as string)
			if not gRsUlog.Validate() then
				return false
		)
		
		local ap3asset = AP3AssetFile()
		ap3asset.Initialize()
		local assetZip = ap3asset.AddZipArchive assetPath
		gCurrAnimFolderFiles = getFiles (fullPath +"*")
		CreateDialog RsAnimationSelectRoll 300 200 modal:true
		local thisObjectsFiles = gCurrAnimFolderFiles
		for f in thisObjectsFiles do
			ap3asset.AddFile assetZip f
		
		if not ap3asset.BuildAssetFiles() then
		(
			gRsUlog.LogError ("Animation asset packing failed in script "+rbFileName)
			return gRsUlog.Validate()
		)
		
		return true
	)

	fn BuildAnims rebuild:false =
	(
		local args = #()
		if rebuild then
			append args "--rebuild"
		
		return ( 0 == ( AP3Invoke.Convert #(RsClipMaxAssetDir) args:args debug:true ) )
	)

	fn PropExportAnimation = (
		
		if $ == undefined then
		(
			return false
		)

		fullanimPath		= ( maxfilepath + "anim/" + $.Name + ".anim" )

		if(edtAnimName.text != "" and edtAnimPath.text != "") then
		(
			fullanimPath = ( edtAnimPath.text + "/" + edtAnimName.text + ".anim" )
		)
		else if(edtAnimName.text != "" and edtAnimPath.text == "") then
		(
			fullanimPath = ( maxfilepath + "anim/" + edtAnimName.text + ".anim" )
		)
		else if(edtAnimName.text == "" and edtAnimPath.text != "") then
		(
			fullanimPath = (edtAnimPath.text + "/"  + $.Name + ".anim" )
		)

		animName 		= ( RsRemovePathAndExtension fullanimPath )
		animPath 		= ( RsRemoveFilename fullanimPath )
		clipFile 				= ( RsRemoveExtension fullanimPath )

		root = GetRootBone $
		
		RsMakeSurePathExists animPath

		rexReset()
		graphRoot = rexGetRoot()
		
		bonelist = GetBoneList root
		select bonelist
		gRsUlog.LogMessage ("boneslist: " + bonelist as string)
		
		-- Try spec file way when there is one main root bone as the parent
		--specfilename = sysInfo.tempdir + "propAnimSpec.xml"
		--RsCreateSpecFile root specfilename useWildcard:false

		specfilename = edtAnimSpec.text

		if chkSpecFileToggle.checked == true then
		(
			specfilename = sysInfo.tempdir + "spec.xml"
			RsCreateSpecFileRecNew root specfilename false false false
		)

		-------------------------------------------------------------------------
		-- Rex Export Animation
		-------------------------------------------------------------------------
		animation = rexAddChild graphRoot animName "Animation"
		rexSetPropertyValue animation "OutputPath" animPath
		rexSetPropertyValue animation "ExportCtrlFile" specfilename
		rexSetPropertyValue animation "AnimMoverTrack" "false"
		rexSetPropertyValue animation "AnimAuthoredOrient" "true"
		rexSetPropertyValue animation "AnimCompressionTolerance" "0.0005"
		rexSetPropertyValue animation "AnimLocalSpace" "true"
		rexSetPropertyValue animation "AnimEndFrame" (spnAnimEndFrame.value as string)
		rexSetPropertyValue animation "AnimStartFrame" (spnAnimStartFrame.value as string)
		gRsUlog.LogMessage ("rex ANIMATION node selection:"+(selection as array) as string)
		rexSetNodes animation

		RexExport false (gRsUlog.Filename())
		clearSelection()
		--deletefile specfilename
		if not gRsUlog.Validate() then
			return false
		
		-------------------------------------------------------------------------
		-- Export Clip
		-------------------------------------------------------------------------
		format "Saving clip %\n" clipFile
			
		clipEditor clipFile
		clipload clipFile
		clipSetProperty "Compressionfile_DO_NOT_RESOURCE" (clipGetStringId()) "Compressionfile_DO_NOT_RESOURCE" lstCompressionFiles.selected
		clipSetProperty "Skelfile_DO_NOT_RESOURCE" (clipGetStringId()) "Skelfile_DO_NOT_RESOURCE" lstSkelFiles.selected
		clipsave clipFile

		dictionaryName = tolower edtDictName.text
		if edtDictName.text == "" then
		(
			dictionaryName = root.Name
		)

		if PackAnims animPath dictionaryName == true then
		(
			BuildAnims()
		)

		select root
	)

	on btnExportAnimation pressed do
	(
		gRsUlog.init "Prop export begin" appendToFile:false

		RsP4LogUserLabelStatus()

		if $selection.count != 1 then
		(
			messagebox "(Only) 1 node must be selected."
			return false
		)

		PropExportAnimation()

		return true
	)

)

-----------------------------------------------------------------------------
-- Entry-Point
-----------------------------------------------------------------------------

try CloseRolloutFloater RsPropUtility catch()
global RsPropUtility = newRolloutFloater "Prop Animation Exporter" 300 300 50 126
addRollout RsPropAnimationRoll RsPropUtility rolledup:false

-- PropExporter.ms
