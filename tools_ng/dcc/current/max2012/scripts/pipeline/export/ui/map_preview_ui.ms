--
-- File:: export/ui/map_preview_ui.ms
-- Description:: Map Preview Exporter Rollout
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 9 June 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
--filein "pipeline/export/maps/preview.ms" -- loaded by rockstar/export/map.ms

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------
rollout RsMapPreviewRoll "Map Preview"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	checkbox chkGeometry 		"Geometry"	across:2 tooltip:"Preview Export outputs geometry for selection"
	hyperlink lnkHelp			"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Map_Export#Preview" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	checkbox chkTextures 		"Textures"	tooltip:"Preview Export outputs textures for selection"
	checkbox chkCollision		"Collision"	tooltip:"Preview Export outputs static collision for open containers"
	checkbox chkIMAPExport		"Export IMAP Files" tooltip:"Preview Export outputs IMAP files for open containers"
	checkbox chkValidate		"Validate for IMAP" tooltip:"Validate scene before an IMAP preview" offset:[7,0]
	checkbox chkPreserve		"Preserve Preview Files"	tooltip:("Don't clear preview-folder on Preview Export:\n" + RsConfigGetPatchDir())
	button	 btnDoCheck			"Do Export Checks Now" tooltip:"Perform export checks now"
	checkbox chkDoChecks		"Do Pre-export checks"	tooltip:"Should we do pre export checks on the objects?"

	local noExpChecksMsg = "Pre-Export checks OFF"
	local expChecksMsg = "Pre-Export checks ON"
	local dnColor = dotNetClass "System.Drawing.Color"

	local sbgc = ((colorman.getcolor #background) * 255) as color
	local dnBGCol = dnColor.FromARGB 255 sbgc.r sbgc.g sbgc.b

 	dotNetControl lblChecksActive "Label" text:"Label" width:180 height:24
	

	button btnPreview	"Preview Export"	width:120
	button btnDelete		"Delete Preview"	width:120
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------

	fn loadSettings = 
	(
		format "Loading Map Preview settings...\n"
		chkGeometry.checked = ( RsSettingsReadBoolean "rsmappreview" "geometry" true )
		chkTextures.checked = ( RsSettingsReadBoolean "rsmappreview" "textures" true )
		chkCollision.checked = ( RsSettingsReadBoolean "rsmappreview" "collision" false )
		chkIMAPExport.checked = false --( RsSettingsReadBoolean "rsmappreview" "imapexport" false )
		chkValidate.checked = false --( RsSettingsReadBoolean "rsmappreview" "validate" true )
		chkPreserve.checked = ( RsSettingsReadBoolean "rsmappreview" "preserve" false )
		chkDoChecks.checked = ( RsSettingsReadBoolean "rsmappreview" "dochecks" false )
		
		lblChecksActive.Backcolor = dnBGCol
		
		if(chkDoChecks.checked) then (
			lblChecksActive.Text = expChecksMsg
			lblChecksActive.ForeColor = dnColor.LimeGreen
		)else(
			lblChecksActive.Text = noExpChecksMsg
			lblChecksActive.ForeColor = dnColor.Red
		)	

        chkIMAPExport.enabled = false
		chkValidate.enabled = false
	)
	
	fn saveSettings = 
	(
		format "Saving Map Preview settings...\n"
		RsSettingWrite "rsmappreview" "geometry" chkGeometry.checked
		RsSettingWrite "rsmappreview" "textures" chkTextures.checked
		RsSettingWrite "rsmappreview" "collision" chkCollision.checked
		RsSettingWrite "rsmappreview" "imapexport" chkIMAPExport.checked
		RsSettingWrite "rsmappreview" "validate" chkValidate.checked
		RsSettingWrite "rsmappreview" "preserve" chkPreserve.checked
	)
	
	fn makePreviewMask = 
	(
		local previewMask = 0

		local dataBits = 
		#(
			dataPair state:chkGeometry.state		bits:RsMapPreviewBits_Geometry,
			dataPair state:chkTextures.state		bits:RsMapPreviewBits_Textures,
			dataPair state:chkIMAPExport.state		bits:RsMapPreviewBits_Metadata,
			dataPair state:chkValidate.state		bits:RsMapPreviewBits_Validate,
			dataPair state:chkPreserve.state		bits:RsMapPreviewBits_Preserve,
			dataPair state:chkCollision.state		bits:RsMapPreviewBits_Collision,
			dataPair state:chkDoChecks.state 		bits:RsMapPreviewBits_DoExpChecks
		)
		
		for item in dataBits where item.state do 
		(
			previewMask = bit.or previewMask item.bits
		)
		
		return previewMask
	)
	
	-------------------------------------------------------------------------
	-- UI Events
	-------------------------------------------------------------------------

	on chkIMAPExport changed checked do
	(
		chkValidate.enabled = chkIMAPExport.checked
	)

	on btnDoCheck pressed do (

		RsMapPreview.export previewMask:(makePreviewMask()) checkOnly:true

	)

	--
	-- event: Preview button pressed
	-- desc: Do our preview export (much like Export Selected, less validation)
	--
	on btnPreview pressed do
	(
		gRsULog.ClearLogDirectory()

		local previewMask = makePreviewMask()
		-- empty selections are ok for IMAP preview exports.
		if ( ( (bit.and previewMask RsMapPreviewBits_Geometry > 0) or (bit.and previewMask RsMapPreviewBits_Textures > 0) ) and 0 == selection.count ) then
		(
			MessageBox "No objects selected.  Select objects to be previewed and click Preview button."
			
		)
		else
		(
			local timer = ScriptTimer()
			timer.start()
			local buildSuccess = RsMapPreview.export previewMask:previewMask
			timer.end()
			format "Preview Export took % ms\n" ( timer.interval() )
			
			if buildSuccess then 
			(
				messageBox "Preview Export completed" title:"Preview Export Success"
			)
			else 
			(
				messageBox "Preview Export failed" title:"Preview Export Failure"
				gRsUlog.Validate()
			)
		)
	)

	on btnDelete pressed do
	(
		-- Clear both preview dirs (bugstar:2112962)
		RsClearPatchDir core:false
		RsClearPatchDir core:true
	)

	on RsMapPreviewRoll open do
	(
		loadSettings()


	)
	
	on RsMapPreviewRoll close do
	(
		saveSettings()
	)
	
	on RsMapPreviewRoll rolledUp down do 
	(
		RsSettingWrite "rsMapPreview" "rollup" (not down)
	)


	on chkDoChecks changed checked do(
		if(checked) then (
			lblChecksActive.Text = expChecksMsg
			lblChecksActive.ForeColor = dnColor.LimeGreen
		)else(
			lblChecksActive.Text = noExpChecksMsg
			lblChecksActive.ForeColor = dnColor.Red
		)	
	)




)

-- export/ui/map_preview_ui.ms
