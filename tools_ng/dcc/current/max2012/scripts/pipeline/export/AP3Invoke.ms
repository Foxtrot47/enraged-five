--
-- File:: pipeline/export/AP3Invoke.ms
-- Description:: Functions to invoke Asset Pipeline 3.0.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 13 September 2013
--

--
-- name: AP3Invoke
-- desc: Helper structure to create an AP3 Asset Pack Schedule file for building
--       with Asset Pipeline 3.
--
struct AP3Invoke
(
	--
	-- name: Convert
	-- desc: Start AP3 conversion process.
	--
	-- filenames: array of filename to convert
	-- args     : array of executable arguments
	--
	fn Convert filenames args:#() debug:false hidden:false =
	(
		local CONVERT = ( RsConfigGetToolsRootDir() + "\\ironlib\\lib\\RSG.Pipeline.Convert.exe" )
		local cmdline = stringStream ""
		
		format "% " CONVERT to:cmdline
		for arg in args do
			format "% " arg to:cmdline

		format "-branch % " gRsBranch.Name to:cmdline
		if gRsProject.isDLC then
			format "-dlc % " gRsProject.Name to:cmdline

		for filename in filenames do
			format "% " filename to:cmdline
		
		if ( debug ) do
			format "*** DEBUG *** AP3Invoke.Convert(): %\n" cmdline
		
		if ( hidden ) then
		(
			local result = 0
			( HiddenDOSCommand cmdline ExitCode:&result )
			result
		)
		else
			( DOSCommand cmdline )
	),
	
	--
	-- name: Pack
	-- desc: Start AP3 asset packing process.
	--
	-- filenames: array of filename to convert
	-- args     : array of executable arguments
	--
	fn Pack filenames args:#() debug:false hidden:false =
	(
		local PACK = ( RsConfigGetToolsRootDir() + "\\ironlib\\lib\\RSG.Pipeline.AssetPack.exe" )
		local cmdline = stringStream ""
		
		format "% " PACK to:cmdline

		format "-branch % " gRsBranch.Name to:cmdline
		if gRsProject.isDLC then
			format "-dlc % " gRsProject.Name to:cmdline

		for arg in args do
			format "% " arg to:cmdline

		for filename in filenames do
			format "% " filename to:cmdline
		
		if ( debug ) do
			format "*** DEBUG *** AP3Invoke.Pack(): %\n" cmdline
		
		if ( hidden ) then
		(
			local result = 0
			( HiddenDOSCommand cmdline ExitCode:&result )
			result
		)
		else
			( DOSCommand cmdline )
	)
)

-- pipeline/export/AP3Invoke.ms
