--
-- File:: pipeline/export/utils.ms
-- Description:: Shared exporter utility functions
--
-- Date:: 20 February 2012
--
--	AJM: Moved RsGenerateAndAssignTexturePalette into here so that weapon exporter can use it too
--		(as well as the map exporter).
--

filein "pipeline/export/maps/fragment.ms"
filein "pipeline/export/maps/objects.ms"

--------------------------------------------------------------
-- Generate and assign texture palettes
--------------------------------------------------------------	
idxFragment = getattrindex "Gta Object" "Is Fragment"

fn RsGenerateAndAssignTexturePalette obj paletteFilename &usedmaterials &reusedMaterials = 
(
	retval = false
	
	local maxPaletteSize = 256
	local minPaletteWidth = 256
	
	
	local objs = #()
	local meshes = #()
	local isFragment = false
	if getattr obj idxFragment == true then (
		isFragment = true
		TraverseHierarchyRec obj CollectMeshes objs
	)
	else (
		append objs obj
	)
	
	
	local matlist = #()
	for obj in objs do (
		if not (isRefObj obj) do appendIfUnique matlist obj.mat
		append meshes obj.mesh
	)
	
	for mat in matlist do (
		if classof mat == Rage_Shader then (
			if finditem usedmaterials submat == 0 then (

				shadername = RstGetShaderName mat
				if matchPattern shadername pattern:"*_tnt*.sps" == true then (
					RsMakeSurePathExists paletteFilename
					rexGenerateColourPalette meshes maxPaletteSize minPaletteWidth paletteFilename

					texvarcount = RstGetVariableCount mat
					retval = true
					append usedmaterials mat
					for i = 1 to texvarcount do (
						varname = RstGetVariableName mat i
						if RsIsTintPalette varname then (
							RstSetVariable mat i paletteFilename
						)
					)
				)
			)
			else (
				append reusedMaterials obj.mat
			)
		)
		else if classof mat == Multimaterial then (
			mapList = #()
			polyCountList = #()
			faceIDLists = #()
			materialList = #()
			
			for obj in objs do (
				local tempMapList = #()
				RsMatGetMapIdsUsedOnObjectWithCount obj tempMapList polycount:polyCountList faceidlists:faceIDLists
				mapList = mapList + tempMapList
			)
			
			for i=1 to mapList.count do (
				if ( mapList[i] > 0 ) do
				(
					-- url:bugstar:343596 - AJM: Added this in for situations where a material has been removed from the 
					-- multisub, so directly accessing the materialList with the ID is offset and 
					-- so can get the wrong material
					matID = findItem mat.materialIDList mapList[i]
					if ( matID == 0 ) do continue
					appendIfUnique materialList mat.materialList[matID]
				)
			)

			foundTntMat = false
			for submat in materialList do (	
				if classof submat == Rage_Shader then (
					shadername = RstGetShaderName submat
					if matchPattern shadername pattern:"*_tnt*.sps" == true then (

						if finditem usedmaterials submat == 0 then (
							if doesFileExist paletteFilename == false then (
								RsMakeSurePathExists paletteFilename
								rexGenerateColourPalette meshes maxPaletteSize minPaletteWidth paletteFilename
							)

							append usedmaterials submat
							texvarcount = RstGetVariableCount submat
							retval = true

							for i = 1 to texvarcount do (
								varname = RstGetVariableName submat i
								if RsIsTintPalette varname then (
									RstSetVariable submat i paletteFilename
								)
							)
						)
						else (
							append reusedMaterials submat
						)
					)
				)
			)
		)
	)
	retval
)

fn RsGetSceneMaps exportAll:false = (
	local maps = for map in RsMapGetMapContainers doingExport:true where map.is_exportable() and ((map.selCount() != 0)) collect map
	maps
)
