--
-- File:: pipeline/export/vehicles/textures.ms
-- Description:: Texture functions for vehicle exporter
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 3 November 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/vehicles/globals.ms"
filein "pipeline/util/rb_script_generator.ms"
filein "pipeline/util/texturemetatag_utils.ms"
filein "pipeline/util/p4_utils.ms"
filein "pipeline/util/ruby.ms"
filein "pipeline/util/TcsTools.ms"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

--
-- struct: RsVehicleTextureStruct
-- desc: Texture export functions for vehicle exporter
--
struct RsVehicleTextureStruct
(
	texmaplist = #(),
	texmapobjlist = #(),
	isbumplist = #(),
	maptypelist = #(),
	maxsizelist = #(),
	texturetemplatelist = #(),

	sourceRootPath = RsConfigGetVehiclesSourceDir(),
	
	fn sourcePathCheck texmap = 
	(			
		if not RsVehicleUtil.RsCheckTexturePath(sourceRootPath) do 
		(
			gRsUlog.LogError ("Texture " + texmap + " not placed under correct texture root " + sourceRootPath) context:texmap
		)
	),
	
	--
	-- name: Export
	-- desc: Export the textures require for specific txd's.
	--
	fn Export exportDir objectName overwriteTCS:false changelistNum:"default" ExportMPModels:false  isMods:false storeOutFiles:undefined= 
	(
		local success = true
		
		local assetDir = RsConfigGetAssetsDir()
		local tcsDir = assetDir + "metadata/textures/vehicles/"	+ objectName + "/"
		if ExportMPModels then
			tcsDir = assetDir + "metadata/textures/vehicles_mp/"	+ objectName + "/"
		local idxTemplate = getattrindex "Gta Texture" "Processing Template"	
		
		local tempBuildDir = assetDir + "textures/vehicles/" + objectName + "/"	
		
		-- First check that the textures have valid dimensions
		success = (RsCheckTexDimension texmaplist)
		
		if success do 
		(
			local bumpFileName = exportDir + "entity.textures"
			RsMakeSurePathExists bumpFileName
			local bumpFile = openfile bumpFileName mode:"w+"
			
			-- This tcs file list is generated from the list of all textures on the vehicle
			tcsFileListFromTexMaps = #()
			-- This list is all the tcs files actually generated, so we don't try and add empty tcs files
			-- if the person doesn't have source textures later
			tcsFilesGenerated = #()
			
			local sourceTextures = #()
			-- Create list of tcs files for perforce use
			for texMapItem = 1 to texmaplist.count do 
			(				
				--Incase there are two texture slots, remove the + and get correct source texture names
				local textureList = filterstring texmaplist[texMapItem] "+"
				local tcsName = ""
				-- Concat the textures (incase multiple) to get correct tcs filename
				for tex in textureList do
				(
					tcsName += (toLower (getFilenameFile tex))
					-- Proper source texture file locations for local check later
					appendIfUnique sourceTextures tex
				)
				append tcsFileListFromTexMaps (tcsDir + tcsName + ".tcs")
			)
			
			if ( (tcsFileListFromTexMaps.count > 0) and RsGenerateVehicleTCSFiles ) do
			(
				-- Get latest tcs-files:
				RsP4SyncWithForceIfRequired tcsFileListFromTexMaps silent:true
				
				-- Get latest textures:
				local missingFiles = RsP4SyncWithForceIfRequired sourceTextures silent:true
				
				if (missingFiles.count != 0) do 
				(
					for filename in missingFiles do 
					(
						gRsULog.LogError ("File not found locally or on Perforce: " + filename) context:filename
					)
					success = false
				)
				
				if success do 
				(
					-- Mark for edit the tcs files
					if ( true == overwriteTCS ) do
					(
						gRsPerforce.run "edit" tcsFileListFromTexMaps silent:true
					)
					
					-- Check if any tcs files exist already which haven't been submitted to perforce before
					tcsInP4 = #()
					tcsInP4 = gRsPerforce.exists &tcsFileListFromTexMaps				
					for tcsIdx = 1 to tcsFileListFromTexMaps.count do
					(
						if ( tcsInP4[tcsIdx] == false and ( doesFileExist tcsFileListFromTexMaps[tcsIdx]) ) do 
						(
							-- Hijack the tcsFilesGenerated array to include any to mark for add which
							-- haven't been previously, which later will include any newly generated ones
							-- or edited ones
							appendIfUnique tcsFilesGenerated tcsFileListFromTexMaps[tcsIdx]
						)
					)
				)
			)
			
			/*
			-- Commented this out for now because deleting unused textures was causing too many problems.
			-- Will need to be much more robust to handle other shared textures (liveries, etc) but disabling it for now.
			
			-- Don't do the perforce check if we dont have any textures in the list otherwise everything will think it needs to be deleted.
			if (texmaplist.count > 0) then
			(
				-- Check the currently checked in textures to see if we need to delete them because they aren't required by the exporter
				local assetTextureDir = assetDir + "vehicles/textures/" + objectName + "/*"
				local p4TcsDir = tcsDir + "*"
				
				local checkedInTextures = gRsPerforce.files assetTextureDir silent:true
				local checkedInTcsFiles = gRsPerforce.files p4TcsDir silent:true
				
				local unusedP4Files = #()
				
				for p4Texture in checkedInTextures do
				(
					local p4TextureFilename = (toLower (getFilenameFile p4Texture))
					local foundInPerforce = false
					for i = 1 to texmaplist.count do
					(
						textureFilename = (toLower (getFilenameFile tcsFileListFromTexMaps[i]))
						if (textureFilename == p4TextureFilename) do (foundInPerforce = true)
					)
					if not foundInPerforce do (append unusedP4Files p4Texture)
				)
				
				for p4TCSFile in checkedInTcsFiles do
				(
					local p4TcsFilename = (toLower (getFilenameFile p4TCSFile))
					local foundInPerforce = false
					for i = 1 to texmaplist.count do
					(
						textureFilename = (toLower (getFilenameFile tcsFileListFromTexMaps[i]))
						if (textureFilename == p4TcsFilename) do (foundInPerforce = true)
					)
					if not foundInPerforce do (append unusedP4Files p4TCSFile)
				)
				
				for unusedFile in unusedP4Files do (gRsPerforce.addDeleteFilesToChangelist changelistNum unusedFile)
			)
			*/
			
			for i = 1 to texmaplist.count while success do 
			(
				texmap = toLower texmaplist[i]
				isbump = isbumplist[i]
				texmapobj = texmapobjlist[i]

				if (findstring texmap ".dds" != undefined) or (undefined==isbump) do 
				(
					isbump = false
				)
				
				sourcePathCheck texmap

				stripName = RsStripTexturePath(texmap)
				tcsFile = tcsFileListFromTexMaps[i]
				export_dds_pathname = RsConfigGetAssetsDir() + "vehicles/textures/" + objectName + "/" + (toLower (getFilenameFile tcsFile)) + ".dds"
				
				singleName = (filterstring texmap "+")[1]

				foundFiles = getfiles singleName

				if (foundFiles.count > 0) do 
				(
					outputFile = (exportDir + stripName + ".dds")
					tcloutputFile = (exportDir + stripName + ".tcl")

					texname = RsRemoveSpacesFromString ((filterstring stripName ".")[1])
					typename = ""					
					if ( maptypelist != undefined ) do
					(
						typename = RsResolveTexMapTypeName maptypelist[i]
					)
					-- AJM: Get the type automatically above, then override if there
					-- has been one set manually
					local usersettypename = getattr texmapobj idxTemplate
					if ( usersettypename != "" ) then
					(
						-- If texmap was tagged with a template, use that as the 'typename':
						typename = usersettypename			
					)
					else
					(
						-- Make sure that RsResolveTexMapTypeName didn't return
						-- undefined as we don't want to set the attribute to that
						if ( undefined != typename ) do
						(
							setattr texmapobj idxTemplate typename
						)
					)	
					-- If the typename was set to undefined then put it back to empty string
					if ( undefined == typename ) do ( typename = "" )
					
					local tcstemplate = ""
					if texturetemplatelist != undefined and texturetemplatelist[i]!=undefined and texturetemplatelist[i] != "" then (
						if findString texturetemplatelist[i] "/" == undefined then (
							tcstemplate = (RsConfigGetTCPVehiclesRoot mp:ExportMPModels) + texturetemplatelist[i]
						)
						-- If our path is relative it needs to be applied to the root
						else (
							tcstemplate = (RsConfigGetTCPVehiclesRoot mp:ExportMPModels) + texturetemplatelist[i]
						)	
						tcstemplate = substituteString tcstemplate (RsConfigGetAssetsDir()) "$(core_assets)/"
					)
					else (
						-- If template name is still "" by this point then Default is used
						--tcstemplate = RsResolveTypeNameToTemplate typename templatesXmlFilename:( RsConfigGetEtcDir() + "texture/vehicletemplates.xml" ) 
						gRsULog.LogError ("No template found for " + texmap + " of type " + maptypelist[i] as string) context:texmap
					)
					--two levels of indirection: one stored side by side with the texture
					--the second stored in the templates folder. This is because we don't
					--really want to be editing the files that are stored side by side with
					--the textures but we do want to be able to easily change all the glboal templates
					--without doing a re-export
					
					-- If checkbox is ticked then generate as the file will be checked out if it
					-- exists in perforce already, to allow manual overriding of tcs files.
					
					local tcsHelper = dotnetclass "RSG.MaxUtils.Textures.TCSHelper"
					local tcsFileExistsLocally = doesFileExist tcsFile
					local tcsFileHasResourceTexturesElement = tcsFileExistsLocally and (tcsHelper.HasResourceTexturesElement tcsFile)
					
					if (RsGenerateVehicleTCSFiles) and ( overwriteTCS or (not tcsFileHasResourceTexturesElement) ) do 
					(
						RsGenerateTemplateReferenceFile tcsFile tcstemplate sourceTextures:#(texmap) exportTexture:export_dds_pathname
						appendIfUnique tcsFilesGenerated tcsFile
						
						gRsPerforce.run "edit" #(tcsFile) silent:true
						if (changelistNum != undefined) do 
						(
							gRsPerforce.addToChangelist changelistNum #(tcsFile) silent:true
						)

						try
						(
							tcsHelper.GenerateResourceTexturesElement tcsFile #(texmap) export_dds_pathname
						)
						catch
						(
							gRsUlog.LogError ("Texture template resourceTexture element creation failed for tcs:"+tcsFile as string+" \ntexmap:"+texmap as string+"\nand dds:"+export_dds_pathname as string+"\n due to dotnet exception:"+(getCurrentException()))
						)
					)
					
					-- Generate the TCL
					tcsreference = (RsMakeSafeSlashes (RsRemoveExtension tcsFile))
					tcsreference = substituteString tcsreference (RsConfigGetAssetsDir()) "$(assets)/"
					RsGenerateTemplateReferenceFile tcloutputFile tcsreference
					
					if isbump then (
						format "% { Flags 1 }\n" texname to:bumpFile
					) else (
						format "%\n" texname to:bumpFile
					)

					if isMods then
					(
						outputFile = export_dds_pathname
					)
					
					-- We need to grab all the files from here to hand back to our export function.
					if (classof storeOutFiles == Array) then
					(
						appendIfUnique storeOutFiles outputFile
					)
					rexExportTexture texmap outputFile raw:true uniLogFile:(gRsUlog.Filename())
				)
			)
			close bumpFile

			if (success and (tcsFilesGenerated.count > 0) and RsGenerateVehicleTCSFiles) do 
			(
				if (classof storeOutFiles == Array) then 
				(
					-- Cut down on p4 calls.
					join storeOutFiles tcsFilesGenerated
				)
				else
				(
					-- See if any new tcs files were created
					gRsPerforce.add tcsFilesGenerated silent:true
					if undefined!=changelistNum then
						gRsPerforce.addToChangelist changelistNum tcsFilesGenerated silent:true
				)
			)
			-- Revert unchanged tcs files from the changelist
			if ((changelistNum != undefined) and RsGenerateVehicleTCSFiles) do 
			(
				pushPrompt "Reverting unchanged TCS files"
				gRsPerforce.run "revert" #("-a", "-c", (changelistNum as string), ( tcsDir + "....tcs" ) ) silent:true
				popPrompt()
			)
		)
		
		return success
	),

	--
	-- name: ExportVariations
	-- desc: Export the textures require for specific txd's.
	--
	fn ExportVariations exportDir objectName overwriteTCS:false changelistNum:undefined ExportMPModels:false = (

		local assetDir = RsConfigGetAssetsDir()
		local tcsDir = assetDir + "metadata/textures/vehicles/" + objectName + "/"
		if ExportMPModels then
			tcsDir = assetDir + "metadata/textures/vehicles_mp/"	+ objectName + "/"
		local tempBuildDir = assetDir + "textures/vehicles/" + objectName + "/"	
		
		-- This tcs file list is generated from the list of all textures on the vehicle
		tcsFileListFromTexMaps = #()
		-- This list is all the tcs files actually generated, so we don't try and add empty tcs files
		-- if the person doesn't have source textures later
		tcsFilesGenerated = #()
			
		-- Create list of tcs files for perforce use
		for texMapItem = 1 to texmaplist.count do 
		(
			append tcsFileListFromTexMaps (tcsDir + ( RsStripTexturePath(toLower texmaplist[texMapItem].file) ) + ".tcs")
		)
		if ( (tcsFileListFromTexMaps.count > 0) and RsGenerateVehicleTCSFiles ) do
		(
			RsP4SyncWithForceIfRequired tcsFileListFromTexMaps silent:true		
			
			-- Check if any tcs files exist already which haven't been submitted to perforce before
			tcsInP4 = #()
			tcsInP4 = gRsPerforce.exists &tcsFileListFromTexMaps				
			for tcsIdx = 1 to tcsFileListFromTexMaps.count do
			(
				if ( tcsInP4[tcsIdx] == false and ( doesFileExist tcsFileListFromTexMaps[tcsIdx]) ) do 
				(
					-- Hijack the tcsFilesGenerated array to include any to mark for add which
					-- haven't been previously, which later will include any newly generated ones
					-- or edited ones
					appendIfUnique tcsFilesGenerated tcsFileListFromTexMaps[tcsIdx]
				)
			)			
		)		
		
		local textureOutputFilenames = #()
		local textureSourceFilenames = #()

		for texnode in texmaplist do (
			texmapname = texnode.file
			typename = texnode.tcs

			texmapname = RsLowercase(RsMakeSafeSlashes texmapname)
			stripName = RsStripTexturePath(texmapname)
			outputddsname = (exportDir + stripName + ".dds")
			RsMakeSurePathExists outputddsname

			stripName = RsStripTexturePath(texmapname)
			tcsFile = (tcsDir + stripName + ".tcs")
			export_dds_pathname = RsConfigGetAssetsDir() + "vehicles/textures/" + objectName + "/" + (toLower (getFilenameFile tcsFile)) + ".dds"

			append textureOutputFilenames export_dds_pathname
			append textureSourceFilenames texmapname

			if typename == "" then 
			(
				RsResolveTexMapTypeName texmapname
				if ( undefined == typename ) then typename = "Default"
				format "No texture template set, so assigning template %\n" typename
			)

			tcstemplate = RsResolveTypeNameToTemplate typename templatesXmlFilename:( RsConfigGetEtcDir() + "texture/vehicletemplates.xml" ) 

			--two levels of indirection: one stored side by side with the texture
			--the second stored in the templates folder. This is because we don't
			--really want to be editing the files that are stored side by side with
			--the textures but we do want to be able to easily change all the glboal templates
			--without doing a re-export

			-- If checkbox is ticked then generate as the file will be checked out if it
			-- exists in perforce already, to allow manual overriding of tcs files.
			local tcsHelper = dotnetclass "RSG.MaxUtils.Textures.TCSHelper"
			local tcsFileExistsLocally = doesFileExist tcsFile
			local tcsFileHasResourceTexturesElement = tcsFileExistsLocally and (tcsHelper.HasResourceTexturesElement tcsFile)
			
			if (RsGenerateVehicleTCSFiles) then
			(
				if tcsFileExistsLocally then
					setFileAttribute tcsFile #readOnly false
				if ( overwriteTCS or (not tcsFileExistsLocally) ) then
				(
					RsGenerateTemplateReferenceFile tcsFile tcstemplate sourceTextures:#(texmapname) exportTexture:export_dds_pathname
					appendIfUnique tcsFilesGenerated tcsFile
				)
				
				try(
					tcsHelper.GenerateResourceTexturesElement tcsFile #(texmapname) export_dds_pathname
					appendIfUnique tcsFilesGenerated tcsFile
				)catch(
					gRsUlog.LogError ("Texture template resourceTexture element creation failed for tcs:"+tcsFile as string+" \ntexmap:"+texmapname as string+"\nand dds:"+export_dds_pathname as string+"\n due to dotnet exception:"+(getCurrentException()))
				)
			)


			-- Generate the TCL
			tcsreference = (RsMakeSafeSlashes (RsRemoveExtension tcsFile))
			tcsreference = substituteString tcsreference (RsConfigGetAssetsDir()) "$(assets)/"
			tcloutputfile = (RsRemoveExtension (outputddsname)) + ".tcl" 

			RsGenerateTemplateReferenceFile tcloutputfile tcsreference
			texmaptclname = tempBuildDir + RsRemovePathAndExtension(texmapname) + ".tcl"
		
			rexExportTexture texmapname outputddsname raw:true uniLogFile:(gRsUlog.Filename())
		

		)		
		if not gRsulog.Validate() then (success = false)
		
		-- add or edit variation textures
		if(textureOutputFilenames.count > 0) do
		(	
			gRsPerforce.add_or_edit textureOutputFilenames silent:true
			gRsPerforce.addToChangelist changelistNum textureOutputFilenames silent:true
		)
	
		-- Moved outside above loop to avoid singular p4 calls
		for i = 1 to textureSourceFilenames.count do(
			
			-- create dir if not already there. Export fails if path doesn't exist.
			RsMakeSurePathExists (RsMakeSafeSlashes (getFilenamePath textureOutputFilenames[i]))

			rexExportTexture textureSourceFilenames[i] textureOutputFilenames[i] raw:true uniLogFile:(gRsUlog.Filename())

			if not RsFileExists textureOutputFilenames[i] then
			(
				gRsULog.LogWarning (textureOutputFilenames[i] +" texture not created")
			)
		)

		-- Needs to be separate as won't work on files that dont exist locally.
		if(textureOutputFilenames.count > 0) do
		(	
			gRsPerforce.reopen textureOutputFilenames filetype:"binary+m"
		)

		if (RsGenerateVehicleTCSFiles) do 
		(
			if ( tcsFilesGenerated.count > 0 ) do 
			(
				-- Add or edit the TCS files list that were generated
				gRsPerforce.add_or_edit tcsFilesGenerated silent:true
				gRsPerforce.addToChangelist changelistNum tcsFilesGenerated silent:true
			)

			
			-- Revert unchanged tcs files
			-- gRsPerforce.run "revert" #("-a", "-c", (changeListNum as string), ( tcsDir + "....tcs" ) ) silent:true
		)
				
		true
	),
	
	--
	-- name: ExportShared
	-- desc: Export a shared texture list to a texture dictionary.
	--
	fn ExportShared outname sharedCache overwriteTCS:false changelistNum:"default" = (
		
		local sharedTxdCache = sharedCache
		local assetDir = RsConfigGetAssetsDir()
		local tcsDir = assetDir + "metadata/textures/vehicles/shared/"
		local tempBuildDir = assetDir + "textures/vehicles/shared/"
		
		-- This tcs file list is generated from the list of all textures on the vehicle
		tcsFileListFromTexMaps = #()
		-- This list is all the tcs files actually generated, so we don't try and add empty tcs files
		-- if the person doesn't have source textures later
		tcsFilesGenerated = #()
			
		-- Create list of tcs files for perforce use
		for texMapItem = 1 to texmaplist.count do 
		(
			append tcsFileListFromTexMaps (tcsDir + ( RsStripTexturePath(toLower texmaplist[texMapItem].file) ) + ".tcs")
		)
		if ( (tcsFileListFromTexMaps.count > 0) and RsGenerateVehicleTCSFiles ) do
		(
			RsP4SyncWithForceIfRequired tcsFileListFromTexMaps silent:true		
			-- Mark for edit the tcs files
			if ( true == overwriteTCS ) do
			(
				gRsPerforce.run "edit" tcsFileListFromTexMaps silent:true
				gRsPerforce.addToChangelist changelistNum tcsFileListFromTexMaps silent:true
			)

			-- Check if any tcs files exist already which haven't been submitted to perforce before
			tcsInP4 = #()
			tcsInP4 = gRsPerforce.exists &tcsFileListFromTexMaps				
			for tcsIdx = 1 to tcsFileListFromTexMaps.count do
			(
				if ( tcsInP4[tcsIdx] == false and ( doesFileExist tcsFileListFromTexMaps[tcsIdx]) ) do 
				(
					-- Hijack the tcsFilesGenerated array to include any to mark for add which
					-- haven't been previously, which later will include any newly generated ones
					-- or edited ones
					appendIfUnique tcsFilesGenerated tcsFileListFromTexMaps[tcsIdx]
				)
			)		
		)		
		
		local stream_dir = ( RsVehicleSettings.GetStreamDir() )
		local script_dir = ( RsVehicleSettings.GetScriptDir() )

		-- Write out .textures file
		local bumpFileName = stream_dir + outname + ".textures"
		RsMakeSurePathExists bumpFileName
		local bumpFile = openfile bumpFileName mode:"w+"
		filelist = #()
		for texnode in texmaplist do (
			texmapname = texnode.file
			typename = texnode.tcs
			
			texmapname = toLower (RsMakeSafeSlashes texmapname)
			
			sourcePathCheck texmapname
			
			outputddsname = tempBuildDir + RsRemovePathAndExtension(texmapname) + ".dds"
			tcloutputname = tempBuildDir + RsRemovePathAndExtension(texmapname) + ".tcl"
			RsMakeSurePathExists outputddsname
			
			stripName = RsStripTexturePath(texmapname)
			tcsFile = (tcsDir + stripName + ".tcs")
			
			if typename == "" then 
			(
				RsResolveTexMapTypeName texmapname
				if ( undefined == typename ) then typename = "Default"
				format "No texture template set, so assigning template %\n" typename
			)
				
			tcstemplate = RsResolveTypeNameToTemplate typename templatesXmlFilename:( RsConfigGetEtcDir() + "texture/vehicletemplates.xml" ) 

			--two levels of indirection: one stored side by side with the texture
			--the second stored in the templates folder. This is because we don't
			--really want to be editing the files that are stored side by side with
			--the textures but we do want to be able to easily change all the glboal templates
			--without doing a re-export

			if (RsGenerateVehicleTCSFiles) do 
			(
				-- If checkbox is ticked then generate as the file will be checked out if it
				-- exists in perforce already, to allow manual overriding of tcs files.
				if ( true == overwriteTCS ) then
				(
					RsGenerateTemplateReferenceFile tcsFile tcstemplate sourceTextures:#(texmapname)
					appendIfUnique tcsFilesGenerated tcsFile
				)
				else
				(
					if ( false == doesFileExist tcsFile ) then
					(
						RsGenerateTemplateReferenceFile tcsFile tcstemplate sourceTextures:#(texmapname)
						appendIfUnique tcsFilesGenerated tcsFile
					)
				)

				gTcsTools.CheckTcsFormat tcsFile texmapname outputddsname
			)

			tcsreference = (RsMakeSafeSlashes (RsRemoveExtension tcsFile))
			tcsreference = substituteString tcsreference (RsConfigGetAssetsDir()) "$(core_assets)/"
			RsGenerateTemplateReferenceFile tcloutputname tcsreference

			format "rexExportRawTexture % %" texmapname outputddsname
			rexExportTexture texmapname outputddsname raw:true uniLogFile:(gRsUlog.Filename())

			texmaptclname = tempBuildDir + RsRemovePathAndExtension(texmapname) + ".tcl"
			append filelist texmaptclname
			
			if not RsFileExists outputddsname then
			(
				gRsULog.LogWarning (outputddsname+" texture not created") context:outputddsname
			)
		)
				
		if not gRsulog.Validate() then
			return false
		
		if (RsGenerateVehicleTCSFiles) do
		(
			if ( tcsFilesGenerated.count > 0 ) do 
			(
				-- See if any new tcs files were created
				gRsPerforce.add tcsFilesGenerated silent:true

				gRsPerforce.addToChangelist changelistNum tcsFilesGenerated silent:true
			)
			
			-- Revert unchanged tcs files
			gRsPerforce.run "revert" #("-a", "-c", (changeListNum as string), ( tcsDir + "....tcs" ) ) silent:true
		)
		
		close bumpFile

		if not RBGenVehicleResourcingScriptTxd (sharedTxdCache + outname + ".itd.zip") filelist then
		(
			return false
		)

		true	
	),
	
	--
	-- name: RemoveSharedFromTexMapList
	-- desc: remove any shared items from the vehicles texmap list
	--
	fn RemoveSharedFromTexMapList shared_texture_list = (

		for i = texmaplist.count to 1 by -1 do
		(
			local simpleName = toLower ( RsRemovePathAndExtension texmaplist[i] )
			local found = false
			for checkTex in shared_texture_list while not found do 
			(
				local simpleCheckName = toLower ( RsRemovePathAndExtension checkTex )
				if simpleCheckName == simpleName then 
				(
					found = true
				)
			)
			
			if found then 
			(
				deleteItem texmaplist i
				deleteItem texmapobjlist i
				deleteItem isbumplist i
				deleteItem maptypelist i
				if undefined!=texturetemplatelist then
					deleteItem texturetemplatelist i
			)
		)
	)
)

-- pipeline/export/vehicles/textures.ms
