--
-- File:: pipeline/export/vehicles/utils.ms
-- Description:: Vehicle Export Utility Functions
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 3 October 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/vehicles/globals.ms"
filein "rockstar/util/attribute.ms"
filein "pipeline/util/constraints.ms"
filein "pipeline/util/skinutils.ms"
filein "pipeline/helpers/animation/hierarchyBB.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

fn RsSelectHierarchy obj apartFromPattern:undefined lookForSuffix:undefined= 
(
	if  (undefined!=apartFromPattern) then
	(
		if not (matchPattern obj.name pattern:apartFromPattern) then
		(
--			print (obj.name + " doesn't match pattern "+apartFromPattern)
			selectMore obj
		)
	)
	else if (undefined!=lookForSuffix) then
	(	
		local lookForNode = getNodeByName (obj.name + lookForSuffix)
		if undefined!=lookForNode then
		(
--			print (obj.name + lookForSuffix + " found ")
			selectMore lookForNode
		)
		else
			selectMore obj
	)
	else
	(
--		print "all criteria undefined"
		selectMore obj
	)
	
	for c in obj.children do RsSelectHierarchy c apartFromPattern:apartFromPattern lookForSuffix:lookForSuffix
)

--
-- name: RsVehicleIsValidBone
-- desc: Determine whether a particular bone is animated.
--
fn RsVehicleIsBoneAnimated rootbone = (

	local retval = true
	
	if ((rootbone.pos.isAnimated == false) and 
		(rootbone.rotation.isAnimated == false) and
		(rootbone.scale.isAnimated == false)) do (
		
		retval = false
	)	
	retval
)

--
-- struct: RsVehicleUtilStruct
-- desc: Vehicle export utility functions.
--
struct RsVehicleUtilStruct
(

	--
	-- name: ExportSceneXml
	-- desc: Export SceneXml file for this vehicle; handling Perforce integration too.
	--
	fn ExportSceneXml filename = 
	(
		RsMakeSurePathExists filename
		SceneXmlReloadCfg()
		
		gRsPerforce.addQueue = #()
		gRsPerforce.add_or_edit filename queueAdd:true
		
		SceneXmlExport filename (gRsUlog.Filename())
		format "Vehicle SceneXml Exported to %\n" filename
		
		gRsPerforce.postExportAdd()
	),

	--
	--
	fn SetBaseMeshPivot obj newPivot = 
	(
		print ("SetBaseMeshPivot :"+obj.name+", "+newPivot as string)
		local mods = for m in obj.modifiers collect m
		while obj.modifiers.count>0 do
			deleteModifier obj 1
		obj.pivot = newPivot
		ResetXForm obj
		CollapseStack obj
		for m in mods while obj.modifiers.count<5 do
			addModifier obj m
	),

	--
	-- name: GetCustomSkinnedObjects
	-- desc: Return array of custom skinned objects (those with Skin modifier)
	--
	fn GetCustomSkinnedObjects rootobj = 
	(
		skinnedObjs = #()
		local allowedObjectClasses = #(PolyMeshObject,Editable_mesh,Editable_Poly)
		if ( 0 != findItem allowedObjectClasses (classof rootobj)) then
		(
			if ( undefined != rootobj.modifiers[#Skin] ) then
			(
				append skinnedObjs rootobj
			)
		)
		-- Recurse through children
		for child in rootobj.children do
		(
			skinnedObjs = skinnedObjs + ( GetCustomSkinnedObjects child )
		)

		skinnedObjs
	),
	
	fn SkinOnAnyChildren root = 
	(
		skinnedObjs = #()
		skinnedObjs = skinnedObjs + ( GetCustomSkinnedObjects root )
		gRsUlog.LogMessage ("skinned objects:"+skinnedObjs as string)
		return (skinnedObjs.count>0)
	),

	--
	-- name: ProcessCustomSkinned
	-- desc: This function updates the custom skinned geometry to use the
	--       newly created skeleton rather than the ones the meshes are
	--       currently skinned to.
	--
	fn ProcessCustomSkinned srcobj customobjs boneList = 
	(
		local vehicleFile = RsVehicleSettings.GetVehicleStreamDir RsVehicleName
		local skinDir = vehicleFile + "/custom_skin/"
		
		-- Bake and save out vertex weight skinning information.
		for o in customobjs do
		(
			local skinFile = skinDir + o.name + ".env"	
			RsSkinUtils.BakeAndSaveWeights o skinFile
			
			RsConvertToPoly o
		)
		
		-- Load vertex weights back using new skeleton.
		for o in customobjs do
		(
			local skinFile = skinDir + o.name + ".env"
			RsSkinUtils.SkinAndLoadWeights o skinFile boneList match_by_name:true
		)
	),

	--
	-- name: CreateSkinnedWithCustomPostProcess
	-- desc: 
	--
	fn CreateSkinnedWithCustomPostProcess srcobj origobj customobjs boneList = (
		local children = #()
		local srcChildren = #()
		local destChildren = #()
		RsVehicleUtil.RecGetChildren srcobj children
		
		-- Clone our meshes and collapse into single mesh.
		local cloneRes = maxOps.CloneNodes children actualNodeList:&srcChildren newNodes:&destChildren
		format "Clone Result: %\n" cloneRes
		format "Src: %\n" srcChildren 
		format "New: %\n" destChildren
		
		-- Remove existing Skin modifier on new clones and collapse to a 
		-- single mesh.
		local n = 0
		for o in destChildren do
		(
			deleteModifier o o.modifiers[#Skin]
			n = n + 1
		
			if ( 1 == n ) then 
				continue
			
			meshOp.attach destChildren[1] o condenseMat:false
		)
		destChildren[1].name = srcChildren[1].name
		srcChildren[1].name = srcChildren[1].name + "_components"
		
		-- Setup and apply our Skin_Wrap modifier
		setCommandPanelTaskMode #modify
		wrapMod = Skin_Wrap()
		wrapMod.Distance = 0.012
		wrapMod.threshold = 0.01
		addModifier destChildren[1] wrapMod
		for o in srcChildren do
		(
			format "Appending mesh % to Skin_Wrap" o.name
			append wrapMod.meshList o
		)
		select destChildren[1]
		destChildren[1].modifiers[#Skin_Wrap].Distance = 0.001
		destChildren[1].modifiers[#Skin_Wrap].ConvertToSkin true

		-- Hide the source children components.
		for o in srcChildren do
			hide o

		true
	),

	--
	-- name: RecAddConstraints
	-- desc:
	--
	fn RecAddConstraints rootobj currobj = (

		if currobj.name == "door_dside_f" then (

			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.rotation = rotateZMatrix 90.0
			newCons.pos = currobj.pos
			newCons.Axis = RsConstraintAxisZ
			newCons.LimitMin = -72.0
			newCons.LimitMax = 0.0
			if RsVehicleIsBoneAnimated currobj then
				newCons.getParentAnimExtent()
		)

		if currobj.name == "door_dside_r" then (
			
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.rotation = rotateZMatrix 90.0
			newCons.pos = currobj.pos
			newCons.Axis = RsConstraintAxisZ
			newCons.LimitMin = -72.0
			newCons.LimitMax = 0.0
			if RsVehicleIsBoneAnimated currobj then
				newCons.getParentAnimExtent()
		)

		if currobj.name == "door_pside_f" then (
			
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.rotation = rotateZMatrix 90.0
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisZ
			newCons.LimitMin = 0.0
			newCons.LimitMax = 72.0
			if RsVehicleIsBoneAnimated currobj then
				newCons.getParentAnimExtent()
		)

		if currobj.name == "door_pside_r" then (
			
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.rotation = rotateZMatrix 90.0
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisZ
			newCons.LimitMin = 0.0
			newCons.LimitMax = 72.0	
			if RsVehicleIsBoneAnimated currobj then
				newCons.getParentAnimExtent()
		)

		if currobj.name == "bonnet" then (
			
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisX
			newCons.LimitMin = 0.0
			newCons.LimitMax = 72.0	
			if RsVehicleIsBoneAnimated currobj then
				newCons.getParentAnimExtent()
		)

		if currobj.name == "boot" then (
			
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisX
			newCons.LimitMin = -72.0
			newCons.LimitMax = 0.0
			if RsVehicleIsBoneAnimated currobj then
				newCons.getParentAnimExtent()
		)	

		if currobj.name == "turret_1base" then (
			
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisZ
			newCons.LimitMin = -180.0
			newCons.LimitMax = 180.0
			if RsVehicleIsBoneAnimated currobj then
				newCons.getParentAnimExtent()
		)	

		if currobj.name == "turret_1barrel" then (
			
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisX
			newCons.LimitMin = -30.0
			newCons.LimitMax = 30.0		
			if RsVehicleIsBoneAnimated currobj then
				newCons.getParentAnimExtent()
		)	

		if ( currobj.name == "gear_f" ) then (
					
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisX
			newCons.LimitMin = -90.0
			newCons.LimitMax = 0.0
			if RsVehicleIsBoneAnimated currobj then
				newCons.getParentAnimExtent()
		)

		if ( currobj.name == "gear_rl" ) then (
					
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisY
			newCons.LimitMin = -90.0
			newCons.LimitMax = 0.0
			if RsVehicleIsBoneAnimated currobj then
				newCons.getParentAnimExtent()
		)
		
		if ( currobj.name == "gear_rr" ) then (
					
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisY
			newCons.LimitMin = 0.0
			newCons.LimitMax = 90.0
			if RsVehicleIsBoneAnimated currobj then
				newCons.getParentAnimExtent()
		)

		for obj in currobj.children do (

			RecAddConstraints rootobj obj
		)
	),

	--
	-- name: RecRemoveLimits
	-- desc: Recursively remove constraint helpers from an object.
	--
	fn RecRemoveLimits currobj = (
		
		-- Base case of recursion.
		if ( ( Constraint == classof currobj ) or 
			( RsRotationConstraint == classof currobj ) or
			( RsTranslationConstraint == classof currobj ) ) then
		(
			gRsUlog.LogWarning ("Delete: "+ currobj as string)
			delete currobj
		)
		else if ( undefined != currobj ) then
		(		
			-- Recurse.
			for obj in currobj.children do 
			(
				RecRemoveLimits obj
			)
		)
	),

	--
	-- name: GetBoneListIndex
	-- desc:
	--
	fn GetBoneListIndex boneList boneName = 
	(
		retval = 0

		for i = 1 to boneList.count do 
		(
			local testName = boneList[i].name
			if HasLODSuffix testName then
				testName = substring testName 1 (testName.count-3)
			if isValidNode boneList[i] and boneList[i].name == boneName then (

				retval = i
			)
		)

		if retval == 0 then 
		(	
			gRsUlog.LogError ("Couldnt find " + boneName + " in list to be used. This probably means naming inconsistencies between bones at different LOD levels.")
		)

		return retval
	),
	
	fn RsAttachMesh attachobj currentobj &parentmat vertexBone boneList:undefined reuseBoneListIndices:true = 
	(
		if undefined==attachobj or undefined==currentobj then
		(
			gRsULog.LogError ("At least one of the input objects is undefined:"+attachobj as string+", "+currentobj as string)
			return false
		)
		local isPoly = false
		
		-- If the object is an editable poly, temporarily convert it
		if ( Editable_Poly == classof currentobj  or  PolyMeshObject == classof currentobj ) then
		(
			isPoly = true
			gRsUlog.LogMessage ("Converting "+currentobj.name+" from an editable poly to an editable mesh") context:currentobj
			convertToMesh currentobj
		)
		
		if ( Editable_Mesh == classof currentobj ) then 
		(
			gRsUlog.LogMessage ("AttachMeshesExistingRec: "+currentobj.name)
		
			if parentmat == undefined then
			(
				parentmat = currentobj.material 
			) 
			else if parentmat != currentobj.material then
			(
				gRsUlog.LogWarning ("Error: object "+currentobj.name+" does not use the material "+parentmat.name+".  All geometry should share the same Multimaterial.") context:currentobj
				if not gRsUlog.Validate() then
					return false
			)

			local attachNumFaces = attachobj.numfaces
			local attachNumVerts = attachobj.numverts
			local currentNumFaces = currentobj.numfaces
			local currentNumVerts = currentobj.numverts

			setnumfaces attachobj (attachNumFaces + currentNumFaces) true
			setnumverts attachobj (attachNumVerts + currentNumVerts) true		
			
			local myAccordingBone = -1
			if undefined!=boneList then
			(
			-- is there a bone defined for us?
				testName = currentobj.name
				if HasLODSuffix testName then 
				(
					testName = substring currentobj.name 1 (currentobj.name.count - 3)
				)
				myAccordingBone = (GetBoneListIndex boneList testName)
			)

			for i = 1 to currentNumVerts do (

				local currentVertex = (getVert currentobj i)
				local currentNormal = (getNormal currentobj i)
				local attachIndex = (i + attachNumVerts)

				setVert attachobj attachIndex currentVertex
				setNormal attachobj attachIndex currentNormal

				if undefined!=boneList then
				(
					if reuseBoneListIndices then
					(
						append vertexBone myAccordingBone
					)
					else
					(
						append vertexBone boneList.count
					)
				) -- boneList != undefined
			)

			for i = 1 to currentNumFaces do (

				local currentFace = getFace currentobj i
				local currentFaceMatID = getFaceMatID currentobj i
				local currentSmoothGroup = getFaceSmoothGroup currentobj i
				local attachFace = (i + attachNumFaces)

				currentFace[1] = currentFace[1] + attachNumVerts
				currentFace[2] = currentFace[2] + attachNumVerts
				currentFace[3] = currentFace[3] + attachNumVerts

				setFace attachobj attachFace currentFace
				setFaceMatID attachobj attachFace currentFaceMatID
				setFaceSmoothGroup attachobj attachFace currentSmoothGroup
				setEdgeVis attachobj attachFace 1 true
				setEdgeVis attachobj attachFace 2 true
				setEdgeVis attachobj attachFace 3 true
			)

			local currentNumMaps = meshop.getnummaps currentobj
			local attachNumMaps = meshop.getnummaps attachobj

			if attachNumMaps < currentNumMaps then (

				meshop.setnummaps attachobj currentNumMaps keep:true

				for i = (attachNumMaps + 1) to currentNumMaps do (

					mapChannel = i - 1

					meshop.setmapsupport attachobj mapChannel true
					meshop.setnummapverts attachobj mapChannel 0
				)
			)

			for i = -2 to (currentNumMaps - 1) do (

				local mapChannel = i

				-- Comment out so that the illumination map channel gets copied
				-- across (which stores the vehicle strength damage)
--  				if ( -1 == mapChannel ) then 
--  					continue
				if ( -2 == mapChannel ) then
				(
					-- Skip if we are processing a particular light mesh,
					-- otherwise we copy the vertex alpha data.
					local lowername = RsLowercase( currentobj.name )
					if ( 0 != ( findItem RsVehicleVertexAlpha lowername ) ) then
					(
						gRsUlog.LogWarning ("Skipping vertex alpha channel for "+currentobj.name)
						continue
					)
				)
				
				gRsUlog.LogMessage ("From "+ currentobj.name +" to "+ attachobj.name)
				if meshop.getmapsupport currentobj mapChannel then 
				(
					if meshop.getmapsupport attachobj mapChannel == false then 
					(
						gRsUlog.LogMessage ("Setting enable channel: " + mapChannel as string)
						meshop.setmapsupport attachobj mapChannel true
						meshop.setnummapverts attachobj mapChannel 0
					)

					local attachNumVerts = meshop.getnummapverts attachobj mapChannel
					local currentNumVerts = meshop.getnummapverts currentobj mapChannel
					gRsUlog.LogMessage ("\tCopying channel "+mapChannel as string+" ["+currentNumVerts as string+", "+attachNumVerts as string+"]")

					meshop.setnummapverts attachobj mapChannel (attachNumVerts + currentNumVerts) keep:true
					meshop.setnummapfaces attachobj mapChannel (attachNumFaces + currentNumFaces) keep:true

					for j = 1 to currentNumVerts do (

						local currentVertex = (meshop.getMapVert currentobj mapChannel j)
						local attachIndex = (j + attachNumVerts)

						meshop.setMapVert attachobj mapChannel attachIndex currentVertex
					)

					for j = 1 to currentNumFaces do (

						local currentFace = meshop.getMapFace currentobj mapChannel j
						local attachFace = (j + attachNumFaces)

						currentFace[1] = currentFace[1] + attachNumVerts
						currentFace[2] = currentFace[2] + attachNumVerts
						currentFace[3] = currentFace[3] + attachNumVerts

						meshop.setMapFace attachobj mapChannel attachFace currentFace
					)

				) else (

					local attachNumVerts = 0
					local currentNumVerts = 1

					if meshop.getmapsupport attachobj mapChannel then 
					(
						attachNumVerts = meshop.getnummapverts attachobj mapChannel
					) 
					else 
					(
						meshop.setmapsupport attachobj mapChannel true
					)

					meshop.setnummapverts attachobj mapChannel (attachNumVerts + currentNumVerts) keep:true
					meshop.setMapVert attachobj mapChannel (attachNumVerts + currentNumVerts) [1,1,1]

					for j = 1 to currentNumFaces do (

						local currentFace = [0,0,0]
						local attachFace = (j + attachNumFaces)

						currentFace[1] = (attachNumVerts + currentNumVerts)
						currentFace[2] = (attachNumVerts + currentNumVerts)
						currentFace[3] = (attachNumVerts + currentNumVerts)

						meshop.setMapFace attachobj mapChannel attachFace currentFace
					)
				)
			)
		)

		-- If this object was originally an editable poly then convert it back
		if isPoly then
		(
			convertTo currentobj (Editable_Poly)
		)
		return true
	),

	--
	-- name: AttachMeshesExistingRec
	-- desc: attach a whole lot of objects and automatically skin
	--
	fn AttachMeshesExistingRec attachobj currentobj parentmat boneList vertexBone = 
	(
		if not RsAttachMesh attachobj currentobj &parentmat vertexBone boneList:boneList reuseBoneListIndices:true then
			return undefined

		for childobj in currentobj.children do (

			if getattrclass childobj != "Gta Collision" then 
			(
				local newmat = AttachMeshesExistingRec attachobj childobj parentmat boneList vertexBone

				if newmat != undefined then parentmat = newmat
			)
		)

		parentmat
	),


	--
	-- name: AttachMeshesRec
	-- desc: Attach a whole lot of objects and automatically skin
	--
	fn AttachMeshesRec attachobj skelobj currentobj parentmat boneList vertexBone = 
	(
		if not RsAttachMesh attachobj currentobj &parentmat vertexBone boneList:boneList reuseBoneListIndices:false then
			return undefined

		for childobj in currentobj.children do (

			if ( Constraint != classof childobj ) and
			   ( RsRotationConstraint != classof childobj ) and 
			   ( RsTranslationConstraint != classof childobj ) then 
			(

				if getattrclass childobj != "Gta Collision" then (

					newskelobj = Dummy()
					newskelobj.name = childobj.name
					newskelobj.transform = childobj.transform
					newskelobj.boxsize = [.2,.2,.2]
					newskelobj.parent = skelobj

					append boneList newskelobj

					newmat = AttachMeshesRec attachobj newskelobj childobj parentmat boneList vertexBone

					if newmat != undefined then parentmat = newmat
				) else (

					col2mesh childobj
					newcollobj = copy childobj
					mesh2col childobj
					mesh2col newcollobj

					newcollobj.name = childobj.name
					newcollobj.parent = skelobj			

					idxCollType = getattrindex "Gta Collision" "Coll Type"
					setattr newcollobj idxCollType (getattr childobj idxCollType)
				)
			)
			else if ( RsRotationConstraint == classof childobj ) then
			(
				newobj = RsRotationConstraint()
				newobj.parent = skelobj
				newobj.transform = childobj.transform
				newobj.Axis = childobj.Axis
				newobj.LimitMin = childobj.LimitMin
				newobj.LimitMax = childobj.LimitMax
			)
			else if ( RsTranslationConstraint == classof childobj ) then
			(
				newobj = RsTranslationConstraint()
				newobj.parent = skelobj
				newobj.transform = childobj.transform
				newobj.Axis = childobj.Axis
				newobj.LimitMin = childobj.LimitMin
				newobj.LimitMax = childobj.LimitMax
			)
		)
		
		parentmat
	),

	--
	-- name: RsAttach
	-- desc: Attach a whole lot of objects and automatically skin
	--
	fn RsAttach attachToObj newObj = 
	(
		local isPoly = false
		
		if attachToObj.modifiers.count>0 then
		(
			while undefined!=attachToObj.modifiers['Skin'] do 
			(
				deletemodifier attachToObj attachToObj.modifiers['Skin']
			)
			gRsUlog.LogMessage ("Colapsing stack of object "+attachToObj as string) context:attachToObj
			collapseStack attachToObj
		)
		if newObj.modifiers.count>0 then
		(
			while undefined!=attachToObj.modifiers['Skin'] do deletemodifier attachToObj attachToObj.modifiers['Skin']
			gRsUlog.LogMessage ("Colapsing stack of object "+newObj as string) context:newObj
			collapseStack newObj
		)

			-- If the object is an editable poly, temporarily convert it
		if ( PolyMeshObject == classof attachToObj or Editable_Poly == classof attachToObj ) then
		(
			isPoly = true
			gRsUlog.LogMessage ("Converting "+attachToObj.name+" from an editable poly to an editable mesh") context:attachToObj
			convertToMesh attachToObj
		)
		print (classof newObj)
		if ( PolyMeshObject == classof attachToObj or Editable_Poly == classof newObj ) then
		(
			format "Converting % from an editable poly to an editable mesh\n" newObj.name
			gRsUlog.LogMessage ("Converting "+newObj.name+" from an editable poly to an editable mesh") context:newObj
			convertToMesh newObj
		)
		local parentmat = undefined
		RsVehicleUtilStruct.RsAttachMesh attachToObj newObj &parentmat vertexBone reuseBoneListIndices:false
		
		-- If this object was originally an editable poly then convert it back
		if isPoly then
		(
			convertTo attachToObj (Editable_Poly)
		)
		
		delete newObj

		attachToObj.material = parentmat
	),

	--
	-- name: DeleteRec
	-- desc: delete a whole hierarchy from the passed in parent down
	--
	fn DeleteRec obj = 
	(
		for childobj in obj.children do 
		(
			DeleteRec childobj
		)
		delete obj
	),


	--
	-- name: RecGetChildren
	-- desc:
	--
	fn RecGetChildren currobj outlist = (

		append outlist currobj

		for obj in currobj.children do (

			RecGetChildren obj outlist
		)
	),

	--
	-- name: AttachRotorToSkeleton
	-- desc: Attach rotor node into vehicle skeleton (attached to chassis node)
	--
	fn AttachRotorToSkeleton skelobj rotorNode = (

		if skelobj.name == "chassis" then (

			if rotorNode != undefined then rotorNode.parent = skelobj
		)

		for obj in skelobj.children do (

			AttachRotorToSkeleton obj rotorNode 
		)		
	),


	--
	-- name: AttachRotor2ToSkeleton
	-- desc: Attach rotor2 node into vehicle skeleton (attached to boot node)
	--
	fn AttachRotor2ToSkeleton skelobj rotorNode = (

		if skelobj.name == "boot" then (

			if rotorNode != undefined then rotorNode.parent = skelobj
		)

		for obj in skelobj.children do (

			AttachRotor2ToSkeleton obj rotorNode 
		)		
	),


	--
	-- name: AttachWheelsToSkeleton
	-- desc:
	--
	fn AttachWheelsToSkeleton skelobj leftWheels rightWheels = (

		if skelobj.name == "wheel_lf" then 
		(
			for w in leftWheels where undefined!=w do
				w.parent = skelobj
		)

		if skelobj.name == "wheel_lr" then 
		(
			for w in rightWheels where undefined!=w do
				w.parent = skelobj
		)	

		for obj in skelobj.children do (

			AttachWheelsToSkeleton obj leftWheels rightWheels
		)
	),

	fn ReplaceLegacyWheels obj = 
	(
		for c in obj.children do 
		(
			ReplaceLegacyWheels c
		)
		local skel = rexGetSkinRootBone obj
		if undefined!=skel then
			for c in skel.children do 
			(
				ReplaceLegacyWheels c
			)

		if classof obj == GtaWheel then
		(
			local meshNode = undefined
			if undefined!=obj.parent then
			(
				local theradius = obj.grid.length/2
				local newNode = CollDisc transform:obj.transform name:obj.name length:obj.grid.width radius:theradius
				newNode.parent = obj.parent
				newNode.pos.z = obj.parent.center.z
				gRsUlog.LogMessage ("Replacing wheel \""+obj.name+"\"with new dics bounds.") context:newNode
				delete obj
			)
			else
				gRsUlog.LogWarning ("not replacing wheel \""+obj.name+"\" as it doesn't have a parent.") context:obj
		)
	),

	--
	-- name: AddExisting
	-- desc:
	--
	fn AddExisting srcobj savetrans setname boneList = 
	(
		gRsUlog.LogMessage ("srcobj:"+srcobj as string)
		if srcobj != undefined then 
		(
			newobjlod1 = Editable_Mesh()
			newobjlod1.name = setname
			newobjlod1.transform = srcobj.transform
			print ("newobjlod1.transform :"+newobjlod1.transform as string)

			vertexBone = #()
			copymat = AttachMeshesExistingRec newobjlod1 srcobj undefined boneList vertexBone
			if not gRsUlog.Validate() then
			(
				gRsUlog.LogError "Error during mesh attaching." context:newobjlod1
				return false
			)
			update newobjlod1 

			newobjlod1.material = copy copymat

			select newobjlod1
			ResetXForm newobjlod1

			setCommandPanelTaskMode #modify
			newSkinLod1 = Skin()
			newSkinLod1.mirrorEnabled = off

			addmodifier newobjlod1 newSkinLod1

			for obj in boneList do (

				skinops.addbone newSkinLod1 obj 1
			)

			if newobjlod1.numverts == vertexBone.count then (

				for i = 1 to vertexBone.count do (	

					skinops.replacevertexweights newSkinLod1 i vertexBone[i] 1.0
				)
			) else (

				gRsUlog.LogError "error in skinning"
			)	

--			srcobj.transform = savetrans
		)	
	),


	--
	-- name: TagBones
	-- desc: Tag bones for cutscene animation
	--
	fn TagBones = 
	(
		cursel = selection[1]
		
		rootSelect = rexGetSkinRootBone selection[1]

		if rootSelect == undefined then rootSelect = selection[1]
		
		alllist = #()
		RecGetChildren rootSelect alllist
		
		select alllist

		for obj in selection do (
			
			if classof obj == Dummy then (
				-- LPXO: Set the boneid tag to the bone name
				setuserprop obj "tag" obj.name
			)
		) 
		
		select cursel
		
		true
	),


	--
	-- name:
	-- desc: setup the ragdoll version of the model's bounds elements
	--
	fn SetupFragBounds objnode rexroot exportDir wheelobjs:undefined wheelids:undefined = 
	(
		if objnode == undefined then 
		(
			gRsUlog.LogError "Object undefined for setting up fragment bounds"
			return false
		)
		
		local allowedCollisionTypes = #(Col_Capsule, Col_Mesh, Col_Box, CollDisc, GtaWheel)

		if 0!=findItem allowedCollisionTypes (classof objnode) then 
		(
			if (classof objnode == GtaWheel) and (wheelobjs != undefined) then 
			(
				append wheelobjs objnode
				append wheelids RsCollCount
			)

			idxSurfaceType = getattrindex "Gta Collision" "Coll Type"
			valSurfaceType = RsUppercase(getattr objnode idxSurfaceType)

			if valSurfaceType == "CAR_PANEL" then setattr objnode idxSurfaceType "CAR_METAL"
			if valSurfaceType == "WINDSCREEN_WEAK" then setattr objnode idxSurfaceType "WINDSCREEN_SIDE_WEAK"
			if valSurfaceType == "WINDSCREEN_MED_WEAK" then setattr objnode idxSurfaceType "WINDSCREEN_SIDE_MED"
			if valSurfaceType == "WINDSCREEN_MED_STRONG" then setattr objnode idxSurfaceType "WINDSCREEN_REAR"
			if valSurfaceType == "WINDSCREEN_STRONG" then setattr objnode idxSurfaceType "WINDSCREEN_FRONT"

			select objnode

			surfaceType = RsGetCollSurfaceTypeString objnode 

			bound = rexAddChild rexroot (RsVehicleName + (RsCollCount as string)) "Bound"
			rexSetPropertyValue bound "OutputPath" exportDir
			rexSetPropertyValue bound "ForceExportAsComposite" "false"
			rexSetPropertyValue bound "ExportAsComposite" "false"				
			bvhIdx = getattrindex "Gta Collision" "BVH Bound"
			rexSetPropertyValue bound "ExportAsBvhGeom" ((getattr objnode bvhIdx) as string)
			rexSetPropertyValue bound "BoundExportWorldSpace" "false"
			rexSetPropertyValue bound "SurfaceType" (objnode.name + "=" + surfaceType)
			gRsUlog.LogMessage ("rex BOUND node selection:"+(selection as array) as string)
			rexSetNodes bound	

			RsCollCount += 1
		)

		for childobj in objnode.children do 
		(
			SetupFragBounds childobj rexroot exportDir wheelobjs:wheelobjs wheelids:wheelids
		)
	),



	--
	-- name: CreateCustomRagebuilderScripts
	-- desc: Create our custom Ragebuilder scripts to be included in our vehicle 
	--       IFT/IDR files for conversion.
	--
	fn CreateCustomRagebuilderScripts exportLodSkels customDir:"" = 
	(
		-- Create Ragebuilder scripts to be included into our independent image.
		local rbsDir = (if customDir == "" then RsVehicleSettings.GetStreamDir() else customDir) + "/"
		luaFileName = (RsMakeSafeSlashes rbsDir) + "custom.rbs"
		RsMakeSurePathExists luaFileName
		try
		(
			luaFile = openfile luaFileName mode:"w+"
			if undefined==luafile then
			(
				gRsUlog.LogError "undefined==luafile"
				return false
			)
			format "postprocess_vehicle(1)\n" to:luaFile
			if exportLodSkels then
				format "lod_skeleton(1)\n" to:luaFile
			close luaFile

			luaFileName = RsVehicleSettings.GetStreamDir() + "custom_finish.rbs"
			luaFile = openfile luaFileName mode:"w+"
			if undefined==luafile then
			(
				gRsUlog.LogError "undefined==luafile"
				return false
			)
			format "postprocess_vehicle(0)\n" to:luaFile
			if exportLodSkels then
				format "lod_skeleton(0)\n" to:luaFile
			close luaFile
		) catch 
		(
			gRsUlog.LogError ("Custom scripts could not be written. Added to p4 by mistake? " + luaFileName)
		)		
		
		return true
	),
	
	fn SetAllSkinVertsNormalised theSkin onOff:false= 
	(
		modpanel.setCurrentObject theSkin
		for k=1 to (skinOps.GetNumberVertices theSkin) do
		(
			skinOps.unNormalizeVertex theSkin k (not onOff)
		)
	),

	fn CreateVehInst skel = 
	(
		local chassisObj = $chassis
		local chassisDummyBone = $chassis_dummy
		if undefined==chassisDummyBone then
		(
			chassisDummyBone = copy chassisObj
			chassisDummyBone.name = "chassis_dummy"
			chassisDummyBone.parent = chassisObj
		)
		
		local bb = RsGetHierarchyBB skel ignorePatterns:#("*_L", "*extra_*", "wheel*")
		local l = bb[2].x - bb[1].x
		local w = bb[2].y - bb[1].y
		local h = bb[2].z - bb[1].z
		local halfdim = [l,w,h]/2
		local center = bb[1] + halfdim
		halfdim *= 0.95 
		
		local vehInst = $VehInstBound
		if undefined==vehInst then
		(
			vehInst = Col_box name:"VehInstBound"
		)
		vehInst.width = halfdim.y 
		vehInst.length = halfdim.x 
		vehInst.height = halfdim.z 
		vehInst.pos = center 
		vehInst.parent = chassisDummyBone
		local idxCollType = getattrindex "Gta Collision" "Coll Type"
		setattr vehInst idxCollType "CAR_METAL"
	),
	
	fn CheckSkelForExpressionsRec skelroot =
	(
		if HasControllerType skelroot types:#(LookAt_Constraint, Float_Expression, RsSpring, Orientation_Constraint) then 
			return true
		
		for child in skelroot.children do
			if CheckSkelForExpressionsRec child then
				return true
		return false
	),
	
	--
	-- name: RsCheckTexturePath 
	-- desc: Check for correct texture path for this project on this file path.
	--
	fn RsCheckTexturePath texmapname = (

		local ngTexPath = RsConfigGetVehiclesSourceDir()
		local nonNgTexPath = ngTexPath
		if matchPattern nonNgTexPath pattern:"*/ng*" then
		(
			local ngIndex = findString nonNgTexPath "/ng"
			nonNgTexPath = replace nonNgTexPath ngIndex 3 ""
		)
		local safeTextPath = (RsMakeSafeTextureName texmapname)
		
		matchPattern safeTextPath pattern:(nonNgTexPath+"*") or matchPattern safeTextPath pattern:(ngTexPath+"*")
	),

	--
	-- name: MoveVehicleMods 
	-- desc: Moves any mods found for the vehicle to the *_mods_ng node or vice versa
	-- param: modNodeFrom - the mod node you are changing from.
	--
	fn MoveVehicleMods modNodeFrom =
	(
		local nodeTo = undefined
		local addSuffix = ""
		if (HasLODSuffix modNodeFrom.name) then (
			-- NG -> Base
			nodeTo = getNodeByName(DeleteLODSuffixes modNodeFrom.name)
			addSuffix = ""
			if(nodeTo == undefined) then return false

		)else(
			nodeTo = getNodeByName(modNodeFrom.name + "_ng")
			addSuffix = "_ng"
			if(nodeTo == undefined) then return false
			-- Base->NG
		)

		if(nodeTo != undefined and isKindOf nodeTO Dummy) then
		(	
			fn objSort o1 o2 = ( stricmp \
											(::DeleteLODSuffixes o1.name ) \
											(::DeleteLODSuffixes  o2.name ))
			local mods = for a in (modNodeFrom.children) collect a 
			qsort mods objSort
			local eMods = for a in (nodeTo.children) collect a
			qsort eMods objSort

			if(mods.count != eMods.count) then 
			(
				gRsUlog.LogError ("Amount of vehicle mod models does not match between LOD levels")
				return false
			)

			for i = 1 to mods.count do(
				amod = mods[i]
				emod = emods[i]
				
				if(matchPattern (DeleteLODSuffixes amod.name) pattern:(DeleteLODSuffixes emod.name)) then(
					for ch in amod.children do (
						local offset = ch.pos - ch.parent.pos
						ch.parent = emod
						if( matchPattern ch.name pattern:"*seat*") then 
						(
							format ">>>>>>>>>>>\nFOUND A SEAT. MOVING SEAT TO PREVENT SAFEZONE BALL ACHE\n>>>>>>>>>>>\n"
							ch.pos = ch.parent.pos + offset
						)
					)						
				)
			)

		)

	),
	
	--
	-- name: HasAnyEmissiveShaders 
	-- desc: Checks a model for any emissive shaders.
	-- param: model - object to check
	-- returns: true if an emissive shader is found for any material on the object
	fn HasAnyEmissiveShaders model = 
	(
		local retVal = false
		local mapList = #()
		RsMatGetMapIdsUsedOnObject model mapList

		local modMat = model.mat

		for mapId in mapList do 
		(
			local actualMat = if classof modMat == MultiMaterial then modMat[mapId] else modMat
			local shaderName = RstGetShaderName actualMat
			if undefined != ( findString shaderName "emissive" ) then 
			(
				 retVal = true
			)
		)
		retVal
	)
) -- RsVehicleUtil utility

-- Global instance
global RsVehicleUtil = RsVehicleUtilStruct()

-- pipeline/export/vehicles/utils.ms
