--
-- File:: pipeline/export/vehicles/globals.ms
-- Description:: Vehicle Export Globals
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 3 October 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"
filein "pipeline/export/vehicles/settings.ms"
filein "pipeline/util/shared_texture_list.ms"

struct sharedVehicleTXDInfo ( filename, texListXML )

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global RsVehicle									-- Vehicle exporter functions
global RsVehicleSettings					-- Vehicle exporter settings functions

global RsVehicleBatchWarnings
global RsVehicleName
global RsVehicleImages							-- Array of ContentVehiclesImage objects
global RsVehicleImageIndex						-- Index into above array
global RsVehicleModsImagePath				-- Name for exported mods image
global RsCollCount = 0
global RsTruckVal
global RsVehicleAsAscii						-- Export vehicle meshes as ASCII
global RsVehicleBuildImage					-- Build Image at end of "Export Skinned"?
global RsVehicleDir            				-- Directory containing all vehicle files, gets shared to and synched against.
global RsVehicleIntermediateModsDir

-- Shared Texture Lists
global RsVehicleSharedTXDStructs = #()

global gAllCurrentVehicleTextures
global RsOverrideTcs
global RsIsLodScene

-- Flag to see if we should be generating TCS files. RDR3 doesn't require .tcs files to be generated if they are using a material preset template.
global RsGenerateVehicleTCSFiles = (RsProjectConfig.GetBoolParameter "Enable Max Vehicle TCS Generation" Default:True)

--
global RsVehicleVertexAlpha = #( 
	-- Light Meshes
	"brakelight_l", "brakelight_r", 
	"headlight_l", "headlight_r", 
	"indicator_lf", "indicator_rf", "indicator_lr", "indicator_rr", 
	"reversinglight_l", "reversinglight_r", 
	"taillight_l", "taillight_r",
	
	-- Window Meshes
	"window_lf", "window_lr",
	"window_rf", "window_rr",
	"windscreen_f", "windscreen_r"
)

global gValidBoneTypes = #(Bone, Dummy, BoneGeometry, Point)

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsVehicleResetGlobals
-- desc: Reset vehicle exporter globals (called at export start).
--
fn RsVehicleResetGlobals = (

	RsVehicleBatchWarnings = #()
	RsVehicleName = "unknown"
	RsVehicleImages = #()
	print "RsVehicleImages reset"
	RsVehicleImageIndex = 0
	RsCollCount = 0
	RsTruckVal = false
	RsIsLodScene = false
	
	RsVehicleSharedTXDStructs = #()

	gAllCurrentVehicleTextures = undefined
	RsOverrideTcs = false

	OK
)

--
-- name: RsVehicleSetupGlobals
-- desc: Setup vehicle exporter globals
--
fn RsVehicleSetupGlobals = (
	

	if not RsContentTree.IsContentTreeLoaded() then(
		RsContentTree.LoadContentTree()
	)

	RsProjectContentReload()
	RsVehicleBatchWarnings = #()
	-- Load ContentVehicleImages
	RsVehicleImages = RsProjectContentFindAll type:"vehiclesdirectory"
	-- Only set this to 1 if it's undefined, as this RsVehicleSetupGlobals function is also called when the Vehicle Image 
	-- selection box is changed in the settings part of the vehicle exporter rollout.
	if RsVehicleImageIndex == undefined do RsVehicleImageIndex = 1

	RsIsLodScene = false
	
	-- Reload shared texture lists, uses wildcard to gather multiple shared TXDs.
	vehSharedXMLFiles = getFiles (RsConfigGetMetadataDir core:true + "textures/shared_textures/vehicles*.xml")
	
	local vehicleNodes = RsContentTree.ContentTreeHelper.GetAllVehiclesExportNodes()
	RsVehicleDir = vehicleNodes[RsVehicleImageIndex].absolutepath
	if ( not matchpattern RsVehicleDir pattern:"*\\" ) do RsVehicleDir += "\\"
	
	for sharedTXDFilename in vehSharedXMLFiles do
	(
		newSharedTXDStruct = sharedVehicleTXDInfo filename:sharedTXDFile texListXML:(SharedTextureListXML sharedTXDFilename)		
		append RsVehicleSharedTXDStructs newSharedTXDStruct
	)
	
	RsVehicleIntermediateModsDir = RsProjectGetAssetsDir() + "/vehicles/intermediate_mods/"
)

-- pipeline/export/vehicles/globals.ms
