--
-- File:: pipeline/export/vehicles/settings.ms
-- Description:: Vehicle exporter specific settings functions
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 4 November 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
--filein "pipeline/util/file.ms" -- loaded on startup by startup.ms
--filein "pipeline/util/xml.ms" -- loaded on startup by startup.ms
filein "pipeline/helpers/arrays.ms" -- loaded on startup by startup.ms
-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

--
-- struct: RsVehicleSettingsStruct
-- desc:
--
struct RsVehicleSettingsStruct
(

	--
	-- name: GetStreamDir
	-- desc: Return vehicle stream directory.
	--
	fn GetStreamDir = 
	(
		local vehicle_image = RsVehicleImages[RsVehicleImageIndex]
		local streamPath = ( RsConfigGetStreamDir() + "vehicles/" + vehicle_image.friendlyname + "/" )
		local spcialArray = (filterstring streamPath " ")
		return (RsArrayJoin spcialArray token:"_")
	),

	--
	-- name: GetVehicleStreamDir
	-- desc: Return vehicle-specific stream directory
	--
	fn GetVehicleStreamDir vehicle_name =
	(
		( GetStreamDir() + vehicle_name + "/" )
	),

	--
	-- name: GetVehicleModsStreamDir
	-- desc: Return vehicle-specific stream directory
	--
	fn GetVehicleModsStreamDir vehicle_name =
	(
		( GetStreamDir() + vehicle_name + "_mods/" )
	),

	--
	-- name: GetGlobalScriptDir
	-- desc: Return global vehicle script directory (not Image specific)
	--
	fn GetGlobalScriptDir =
	(
		( RsConfigGetScriptDir() + "vehicles/" )
	),

	--
	-- name: GetScriptDir
	-- desc: Return Image specific vehicle script directory
	--
	fn GetScriptDir = 
	(
		local vehicle_image = RsVehicleImages[RsVehicleImageIndex]
		( GetGlobalScriptDir() + vehicle_image.friendlyname + "/" )
	),

	--
	-- name: GetVehicleScriptDir
	-- desc: Return vehicle-specific script directory
	--
	fn GetVehicleScriptDir vehicle =
	(
		( GetScriptDir() + "/" + vehicle.name + "/" )
	),
	
	--
	-- name: GetVehicleSceneXmlDir
	-- desc:
	--
	fn GetVehicleSceneXmlDir =
	(
		( RsConfigGetAssetsDir() + "vehicles/" )
	)

)

-- Create global instance of VehicleSettingsStruct
global RsVehicleSettings = RsVehicleSettingsStruct()

-- pipeline/export/vehicles/settings.ms
