--
-- File:: pipeline/export/vehicles/ruby_script_generator.ms
-- Description:: Vehicle exporter Ruby script generation functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 2 November 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/vehicles/globals.ms"
filein "pipeline/util/ruby.ms"
filein "pipeline/export/AP3Asset.ms"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

global gPackagedFiles = #()

--
-- name: RBGenVehicleResourcingScriptModel
-- desc: Create vehicle model resourcing Ruby script body.
--
fn RBGenVehicleResourcingScriptModelFlat = 
(
	local vehicle_name = RsVehicleName
	global assetFile = AP3AssetFile()
	assetFile.Initialize()
	
	local vehicle_stream_dir = (RsVehicleSettings.GetVehicleStreamDir vehicle_name)
	local stream_dir = RsVehicleSettings.GetStreamDir()
	local vehicle_textures_dir = (RsConfigGetAssetsDir() + "vehicles/textures/" + vehicle_name + "/")
	local vehicle_tcs_dir_sp = (RsConfigGetAssetsDir() + "metadata/textures/vehicles/" + vehicle_name + "/")
	local vehicle_tcs_dir_mp = (RsConfigGetAssetsDir() + "metadata/textures/vehicles_mp/" + vehicle_name + "/")
	
	local loddirs = getDirectories (vehicle_stream_dir + "*.*")
	local files = getFiles (stream_dir + "*.*")
	
	local bundleName = (RsVehicleDir + RsVehicleName + ".veh.zip")
	local bundleZip = assetFile.AddZipArchive bundleName
	
	local texturesSubdir = "textures"
	
	for loddir in loddirs do (
		local textureDir = loddir + "/" + texturesSubdir
	
		tokens = (filterString loddir "\\")
		subdir = tokens[tokens.count]

		assetFile.AddDirectory bundleZip loddir destination:subdir
		
		assetFile.AddTexDictFromFolder bundleZip textureDir destination:(subdir + "/" + texturesSubdir) texSourcePath:vehicle_textures_dir tcsSourcePath:vehicle_tcs_dir_sp
	)
	
	for file in files do (
		assetFile.AddFile bundleZip file
	)
	
	local animPath	= ( RsConfigGetAssetsDir() + "vehicles/anims/" + vehicle_name + "/" )
		
	local allAnimFiles = getFiles (animPath+"*.anim")
	allAnimFiles += getFiles (animPath+"*.clip")
	
	for file in allAnimFiles do (
		assetFile.AddFile bundleZip file destination:"anim"
	)
	
	gRsPerforce.add_or_edit #(bundleName) queueAdd:true
	assetFile.BuildAssetFiles()
	gPackagedFiles = join gPackagedFiles #(bundleName)
)

--
-- name: RBGenVehicleResourcingScriptModel
-- desc: Create vehicle model resourcing Ruby script body.
--
fn RBGenVehicleResourcingScriptModel = 
(
	local vehicle_name = RsVehicleName
	global assetFile = AP3AssetFile()
	assetFile.Initialize()
	
	local vehicle_stream_dir = (RsVehicleSettings.GetVehicleStreamDir vehicle_name)
	local stream_dir = RsVehicleSettings.GetStreamDir()
	local vehicle_textures_dir = (RsConfigGetAssetsDir() + "vehicles/textures/" + vehicle_name + "/")
	local vehicle_tcs_dir_sp = (RsConfigGetAssetsDir() + "metadata/textures/vehicles/" + vehicle_name + "/")
	local vehicle_tcs_dir_mp = (RsConfigGetAssetsDir() + "metadata/textures/vehicles_mp/" + vehicle_name + "/")
	
	format "\n##########################################"
	format "\n#models and textures"
	format "\n##########################################\n\n"
	
	local leadingpath_Sp = (vehicle_stream_dir + "sp_zips/")
	local leadingpath_Mp = (vehicle_stream_dir + "mp_zips/")
	local ext = ".ift.zip"
	local itdext = ".itd.zip"
	local icdext = ".icd.zip"
	local zips = #()
	
	local iftBaseName_Sp = RsVehicleName + ext
	local iftFileName_Sp = leadingpath_Sp + iftBaseName_Sp
	local iftZip_Sp = assetFile.AddZipArchive iftFileName_Sp
	assetFile.AddFilesFromFolder iftZip_Sp (vehicle_stream_dir +"low") excludeExtensions:#("dds", "tcl", "tcs")
	assetFile.AddFilesFromFolder iftZip_Sp stream_dir includeExtensions:#("rbs")

	local iftBaseName_SpHi = RsVehicleName + "_hi" + ext
	local iftFileName_SpHi = leadingpath_Sp + iftBaseName_SpHi
	local iftZip_SpHi = assetFile.AddZipArchive iftFileName_SpHi
	assetFile.AddFilesFromFolder iftZip_SpHi (vehicle_stream_dir +"hi") excludeExtensions:#("dds", "tcl", "tcs")
	assetFile.AddFilesFromFolder iftZip_SpHi stream_dir includeExtensions:#("rbs")

	local itdBaseName_Sp = (RsVehicleName + itdext)
	local itdFileName_Sp = (leadingpath_Sp + itdBaseName_Sp)
	local itdZip_Sp = assetFile.AddZipArchive itdFileName_Sp
	assetFile.AddTexDictFromFolder itdZip_Sp (vehicle_stream_dir +"hi") texSourcePath:vehicle_textures_dir tcsSourcePath:vehicle_tcs_dir_sp
	
	local iftBaseName_MpHi = RsVehicleName + "_hi" + ext
	local iftFileName_MpHi = leadingpath_Mp + iftBaseName_MpHi
	local iftZip_MpHi = assetFile.AddZipArchive iftFileName_MpHi
	
	local iftBaseName_Mp = RsVehicleName + ext
	local iftFileName_Mp = leadingpath_Mp + iftBaseName_Mp
	local iftZip_Mp = assetFile.AddZipArchive iftFileName_Mp
	if RsIsLodScene then (
		assetFile.AddFilesFromFolder iftZip_Mp (vehicle_stream_dir +"mp_low") excludeExtensions:#("dds", "tcl", "tcs")
		assetFile.AddFilesFromFolder iftZip_Mp stream_dir includeExtensions:#("rbs")
		
		assetFile.AddFilesFromFolder iftZip_MpHi (vehicle_stream_dir +"mp_hi") excludeExtensions:#("dds", "tcl", "tcs")
		assetFile.AddFilesFromFolder iftZip_MpHi stream_dir includeExtensions:#("rbs")
	)
	else (
		assetFile.AddFilesFromFolder iftZip_Mp (vehicle_stream_dir +"mp_hi") excludeExtensions:#("dds", "tcl", "tcs")
		assetFile.AddFilesFromFolder iftZip_Mp stream_dir includeExtensions:#("rbs")
	)

	local itdBaseName_Mp = (RsVehicleName + itdext)
	local itdFileName_Mp = (leadingpath_Mp + itdBaseName_Mp)
	local itdZip_Mp = assetFile.AddZipArchive itdFileName_Mp
	assetFile.AddTexDictFromFolder itdZip_Mp (vehicle_stream_dir +"mp_hi") texSourcePath:vehicle_textures_dir tcsSourcePath:vehicle_tcs_dir_mp

	assetFile.BuildAssetFiles()

	assetFile = AP3AssetFile()
	assetFile.Initialize()
	local bundleName = (RsVehicleDir + RsVehicleName + ".zip")
	local bundleZip = assetFile.AddZipArchive bundleName

	local icdBaseName = "va_" + RsVehicleName + icdext
	local icdFileName = vehicle_stream_dir + "/" + icdBaseName

	assetFile.AddFile bundleZip iftFileName_Sp destination:("sp/" + iftBaseName_Sp)
	assetFile.AddFile bundleZip iftFileName_SpHi destination:("sp/" + iftBaseName_SpHi)
	if RsFileExists itdFileName_Sp do (
		assetFile.AddFile bundleZip itdFileName_Sp destination:("sp/" + itdBaseName_Sp)
	)

	assetFile.AddFile bundleZip iftFileName_Mp destination:("mp/" + iftBaseName_Mp)
	
	if RsIsLodScene do (
		assetFile.AddFile bundleZip iftFileName_MpHi destination:("mp/" + iftBaseName_MpHi)
	)
	if RsFileExists itdFileName_Mp do (
		assetFile.AddFile bundleZip itdFileName_Mp destination:("mp/" + itdBaseName_Mp)
	)

	if RsFileExists icdFileName do (
		assetFile.AddFile bundleZip icdFileName
	)

	append zips bundleName

	gRsPerforce.add_or_edit zips queueAdd:true
	assetFile.BuildAssetFiles()
	gRsPerforce.PostExportAdd()
	gPackagedFiles = join gPackagedFiles zips
)


--
-- name: RBGenVehicleResourcingScriptMods
-- desc: Create vehicle resourcing Ruby script body for MODS.
--
fn RBGenVehicleResourcingScriptMods = 
(
	-- doing this first for file time comparison in pipeline
 	gRsPerforce.sync RsVehicleModsImagePath
 	gRsPerforce.add_or_edit RsVehicleModsImagePath queueAdd:true
 	gRsPerforce.reopen RsVehicleModsImagePath filetype:"ubinary"
	
	local vehicle_name = RsVehicleName
	global assetFile = AP3AssetFile()
	assetFile.Initialize()
	
	local vehicle_stream_dir = (RsVehicleSettings.GetVehicleStreamDir vehicle_name)
	local vehicle_textures_dir = (RsConfigGetAssetsDir() + "vehicles/textures/" + vehicle_name + "/")
	local vehicle_tcs_dir = (RsConfigGetAssetsDir() + "metadata/textures/vehicles/" + vehicle_name + "/")
	
	local vehicle_stream_mods_dir = (RsVehicleSettings.GetVehicleModsStreamDir vehicle_name)
	
	format "\n##########################################"
	format "\n#models and textures for MODS"
	format "\n##########################################\n\n"

	local dirs = getDirectories (vehicle_stream_mods_dir+"*")
	gRsUlog.LogMessage ("Found"+dirs.count as string+" mod folders in "+vehicle_stream_mods_dir as string)
	for dir in dirs do
	(
		local dirParts = filterString dir "\\"
		local zipName = dirParts[dirParts.count]
		local isFrag = matchPattern zipName pattern:"*_frag"
		local ext = ".idr"
		if isFrag then
		(
			zipName = substring zipName 1 ((findString zipName "_frag")-1)
			ext = ".ift"
		)
		if matchPattern zipName pattern:"*_ng" then
			zipName = substring zipName 1 (zipName.count - 3)
		
		local iftName = (vehicle_stream_mods_dir + zipName + ext + ".zip")
		local iftZip = assetFile.AddZipArchive iftName
		assetFile.AddFilesFromFolder iftZip dir
	)
	
	local zips = assetFile.GetCurrentAssetFiles()
	if not assetFile.BuildAssetFiles() then
		return false
	
	assetFile = AP3AssetFile()
	assetFile.Initialize()
	local wholeModZip = assetFile.AddZipArchive RsVehicleModsImagePath
	for zip in zips do
		assetFile.AddFile wholeModZip zip
	
	append gPackagedFiles RsVehicleModsImagePath
	buildSuccess = assetFile.BuildAssetFiles()
	gRsPerforce.PostExportAdd()
	return buildSuccess
)

--
-- name: RBGenVehicleResourcingScriptTxd
-- desc: Create vehicle shared txd resourcing Ruby script body.
--
fn RBGenVehicleResourcingScriptTxd outputFileName texture_list = (
	
	global assetFile = AP3AssetFile()
	assetFile.Initialize()
	
	format "\n##########################################"
	format "\n#TXD output"
	format "\n#\n##########################################\n\n"
	
	--local wholePath = (RsVehicleDir + outputName + ".itd.zip")
	local itdZip = assetFile.AddZipArchive outputFileName
	
	for texture in texture_list do (
		assetFile.AddFile itdZip texture
	)
	--gRsPerforce.sync wholePath
	--gRsPerforce.add_or_edit wholePath
	--gRsPerforce.reopen wholePath filetype:"ubinary"

	--append gPackagedFiles wholePath
	
	return (true==assetFile.BuildAssetFiles())
)

--
-- name: RBGenVehicleBundleTxds
-- desc: Bundle all hsared txds.
--
fn RBGenVehicleBundleTxds txdlist outputName = (
	global assetFile = AP3AssetFile()
	assetFile.Initialize()
	
	format "\n##########################################"
	format "\n#Bundle TXDs"
	format "\n#\n##########################################\n\n"
	
	local wholePath = (RsVehicleDir + outputName + ".zip")
	local sharedZip = assetFile.AddZipArchive wholePath
	
	for txd in txdlist do (
		assetFile.AddFile sharedZip txd
	)
	gRsPerforce.sync wholePath
	gRsPerforce.add_or_edit wholePath
	gRsPerforce.reopen wholePath filetype:"ubinary"
	
	append gPackagedFiles wholePath
		
	return (true==assetFile.BuildAssetFiles())
)

--
-- name: RBGenVehicleResourcingScriptAnim
-- desc:
--
fn RBGenVehicleResourcingScriptAnim  = (

	local vehicle_name = RsVehicleName
	global assetFile = AP3AssetFile()
	assetFile.Initialize()
	
	format "\n##########################################"
	format "\n#Animation output"
	format "\n#\n##########################################\n\n"
		
	local vehicle_stream_dir = (RsVehicleSettings.GetVehicleStreamDir vehicle_name)
	
	local wholePath = (vehicle_stream_dir + "/" + "va_" + vehicle_name + ".icd.zip")
	local itdZip = assetFile.AddZipArchive wholePath
	
	local animPath	= ( RsConfigGetAssetsDir() + "vehicles/anims/" + vehicle_name + "/" )
	
	local allFiles = getFiles (animPath+"*.anim")
	allFiles += getFiles (animPath+"*.clip")

	for f in allFiles do (
		assetFile.AddFile itdZip f
	)

	append gPackagedFiles wholePath
	
	return (true==assetFile.BuildAssetFiles())

)

fn RBGenVehicleResourcingScriptPatch = (
	patch_dir = RsConfigGetPatchDir()
	RsMakeSurePathExists patch_dir
	
	return ( 0 == ( AP3Invoke.Convert gPackagedFiles args:#("--preview") ) )
)

--
-- name: BuildImage
-- desc: Create and run the vehicle Image resourcing Ruby script parts.
--
fn BuildVehicleImage rebuild:false mods:false =
(
	local image_filename = case mods of
	(
		false:RsVehicleDir
		true:RsVehicleModsImagePath
	)

	local args = #()
	if rebuild then
		append args "--rebuild"
	
	return ( 0 == ( AP3Invoke.Convert #(image_filename) args:args debug:true ) )
)

--
-- name: BuildImage
-- desc: Create and run the vehicle Image resourcing Ruby script parts.
--
fn PreviewVehicle vehiclePath rebuild:false =
(
	local args = #()
	append args "--preview"
	if rebuild then
		append args "--rebuild"
	
	return ( 0 == ( AP3Invoke.Convert #(vehiclePath) args:args debug:true ) )
)

-- pipeline/export/vehicles/ruby_script_generator.ms
