--
-- validation.ms
-- Weapon Export Validation Functions
--
-- David Muir <david.muir@rockstarnorth.com>
-- Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- 5 August 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/vehicles/globals.ms"
filein "pipeline/util/cloth.ms"

global gObjsWithTerrainVertChannel = #()

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn IsValidCollName collName = (

	retval = false

	if matchPattern collName pattern:"*GLASS*" or  matchPattern collName pattern:"*WINDSCREEN*" then retval = true
	
	
	retval
)


idxCollType = getattrindex "Gta Collision" "Coll Type"
idxDynamic = getattrindex "Gta Object" "Is Dynamic"
idxFragment = getattrindex "Gta Object" "Is Fragment"

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsVehicleCheckSmashableCollisionRec model rootobj = (
	
	glassCount = 0
	foundRexBound = false
	
	for obj in model.children do (
		
		if getattrclass obj == "Gta Collision" then (

			if classof obj == Col_Mesh then (

				hasGlass = false
				hasOther = false
				
				if obj.material != undefined then (

					if classof obj.material == MultiMaterial then (

						matList = #()
						RsMatGetMapIdsUsedOnObject obj matList

						for i = 1 to matlist.count do (

							matidx = finditem obj.material.materialIDList matlist[i]

							if matidx != 0 then (

								mat = obj.material.materiallist[matidx]
								
								if classof mat == RexBoundMtl then (

									foundRexBound = true

									colName = RexGetCollisionName mat									
									
									if (IsValidCollName colName) then (
										
										hasGlass = true		
									) else (
										
										hasOther = true
									)
								)
							)
						)				
					) else if classof obj.material == RexBoundMtl then (

						foundRexBound = true

						colName = RexGetCollisionName obj.material

						if IsValidCollName colName then ( 

							hasGlass = true		
						) else (

							hasOther = true
						)						
					)
				)

				if foundRexBound then (

					if hasGlass then (
						if (hasOther == false) then (

							glassCount = glassCount + 1
						) else (

							gRsUlog.LogError ("glass and non glass collision on object " + model.name) context:model 
						)
					)
				) 
			)
		)
	)


	if glassCount > 1 then (

		gRsUlog.LogError ("more than one glass collision on object " + model.name + ". https://devstar.rockstargames.com/wiki/index.php/Vehicle_Export_Errors#Multiple_glass_collision_objects") context:model 
		glassCount = 0
	)		

	for obj in model.children do (

		glassCount = glassCount + RsVehicleCheckSmashableCollisionRec obj rootobj
	)
	
	glassCount
)

--
-- name: RsVehicleCheckSmashableCollision
-- desc:
--
fn RsVehicleCheckSmashableCollision model = (
	print "Enter is smashable"
	print model
	isSmashable = false

	if getattr model idxDynamic == false then (
		gRsUlog.LogWarning ("smashable turned off because " + model.name + " isn't dynamic") context:model 
	) else (

		glassCount = RsVehicleCheckSmashableCollisionRec model model
		if glassCount > 0 then (

			isSmashable = true
		)
	)
	
	isSmashable
)



--
-- name: RsVehicleCheckForValidSkin
-- desc:
--
fn RsVehicleCheckForValidSkin checkNode = 
(		
	if checkNode == undefined then (
		return true
	)

	RsVehicleCheckSmashableCollision (rexGetSkinRootBone checkNode)

	select checkNode

	try
	(
		for i = 1 to checkNode.numverts do (

			boneCount = skinOps.GetVertexWeightCount checkNode.modifiers[1] i

			if boneCount > 1 then (

				local validBones = #()

				for j = 1 to boneCount do 
				(
					local sysIndex = skinOps.GetVertexWeightBoneID checkNode.modifiers[1] i j
					local boneWeight = skinOps.GetVertexWeight checkNode.modifiers[1] i j
					local boneName = skinOps.GetBoneName checkNode.modifiers[1] sysIndex 1

					if boneWeight > 0.0 then (
						append validBones boneName
					)
				)
				if validBones.count > 4 then 
				(
					gRsUlog.LogError (checkNode.name + " has vertices with " + (validBones.count as string) + " bone influences (" + (i as string) + "):\n"+validBones as string)
				)
			)
		)
	)
	catch
	(
		gRsUlog.LogError ("Vehicle skin validation failed: " + ( getCurrentException() ) as string) 
		return false
	)
	
	return true
)

fn CheckMeshForGlassSkinning obj = 
(
	if undefined==obj then
		return false
	
	if classof obj.material != multimaterial then
		gRsULog.LogError ("Material used on "+obj.name+" is not a multi material.")

	local mtlIdFunc = RsGetFaceMatIDFunc obj
	
	local vertsNotRigidlySkinned = #()
	local mtlsNotRigidlySkinned = #()
	
	max modify mode
	modPanel.setCurrentObject obj.skin

	for mi =1 to obj.material.materiallist.count do
	(
		local m = obj.material.materiallist[mi]
		if classof m == Rage_Shader and matchPattern (RstGetShaderName m) pattern:"*glass*" then
		(
			local mId = obj.material.materialIDList[mi]
			local idBitset = #{}
			for f = 1 to obj.faces.count 
				where (mtlIdFunc obj f) == mId do
					idBitset[f] = true
			local vertBitSet = (RsMeshPolyOp obj).getVertsUsingFace obj idBitset
			local vertIndexArray = vertBitSet as array
-- 			for v in vertIndexArray do
-- 				if not (skinOps.RigidVertex obj.skin v true) then
-- 					append vertsNotRigidlySkinned v
 			for v in vertIndexArray do
			(
				local numWeights = skinOps.GetVertexWeightCount obj.skin v

				if (numWeights)>1 then
				(
					-- not necessarily an error - bones might have a zero weight and thus don't affect the glass parts.
					-- we need to check for 0 weighted bones.
					for i = 1 to numWeights do
					(
						local weight = skinOps.GetVertexWeight obj.skin v i
						if weight > 0 and weight < 1 then
						(
							append vertsNotRigidlySkinned v
							appendIfUnique mtlsNotRigidlySkinned mId
						)
					)
					
				)
			)
		)
	)
	
	if vertsNotRigidlySkinned.count>0 then
	(
		local error = stringstream ""
		format "Vertices on glass shader not hard skinned to ONE bone. material ids:" to:error
		for m in mtlsNotRigidlySkinned do
		(
			if m!=mtlsNotRigidlySkinned[1] then
				format ", " to:error
			format "%" m to:error
		)

		local context = stringstream ""
		format "obj:%|maxscript:skinobj = selection[1].skin; max modify mode; modPanel.setCurrentObject skinobj; skinobj.filter_vertices=true; skinOps.SelectVertices skinobj #(" obj.name to:context
		for v in vertsNotRigidlySkinned do
		(
			if v!=vertsNotRigidlySkinned[1] then
				format ", " to:context
			format "%" v to:context
		)
		format "); subObjectLevel = 1;" to:context
		gRsUlog.LogError error context:context quiet:false
	)
)

-- check skinned meshes 
fn CheckSkinnedMeshesRec root = 
(
	if root.modifiers["Skin"]!=undefined then
	(
		if not RsVehicleCheckForValidSkin root then
			return false
	)
	local allValid = true
	for c in root.children do 
		if not CheckSkinnedMeshesRec c then
			allValid = false
	return allValid
)


fn IsOnLine endPoint1 endPoint2 checkPoint =
(
	((checkPoint.y - endPoint1.y) / (checkPoint.x - endPoint1.x) ==
	 (endPoint2.y - endPoint1.y) / (endPoint2.x - endPoint1.x)) and
	((checkPoint.y - endPoint1.y) / (checkPoint.z - endPoint1.z) ==
	 (endPoint2.y - endPoint1.y) / (endPoint2.z - endPoint1.z)) and
	((checkPoint.x - endPoint1.x) / (checkPoint.z - endPoint1.z) ==
	 (endPoint2.x - endPoint1.x) / (endPoint2.z - endPoint1.z))
)

-- url:bugstar:1809282, url:bugstar:1807572
-- Check for empty triangles

fn RsHasMeshFlatTriangles obj = 
(
	facesel = #{}
		
	for faceIndex=1 to getnumfaces obj do
	(
		local face = getface obj faceIndex
		local vert1 = getVert obj face[1] 
		local vert2 = getVert obj face[2]
		local vert3 = getVert obj face[3]
		if IsOnLine vert1 vert2 vert3 then
		(
			facesel[faceIndex] =true
		)
	)

	modPanel.setCurrentObject obj
	setFaceSelection obj facesel
	
	(not facesel.isEmpty)
)

--
-- name: RsVehicleIsValidSkin
-- desc:
--
fn RsVehicleIsValidSkin = (

	setCommandPanelTaskMode mode:#modify

	cursel = #()
	cursel = cursel + selection

	local checkMeshes = #(
		meshNode = cursel[1],
		meshLodNode1 = getnodebyname (RsvehicleName + "_l1_skin") exact:true ignorecase:true,
		meshLodNode2 = getnodebyname (RsvehicleName + "_l2_skin") exact:true ignorecase:true,			
		meshLodNode3 = getnodebyname (RsvehicleName + "_l3_skin") exact:true ignorecase:true,
		meshLodNodeNG = getnodebyname (RsvehicleName + "_ng_skin") exact:true ignorecase:true
	)

	passedTests = true
	
	for obj in checkMeshes where undefined!=obj do
	(
		select obj
		
		CheckMeshForGlassSkinning obj
		
		if RsHasMeshFlatTriangles obj then
		(
			gRsUlog.LogWarning (obj as string+" has degenerate triangles. Check its face selection.") context:obj
		)

		if not RsVehicleCheckForValidSkin obj or meshNode.children.count>0 then 
		(
			gRsUlog.LogError (obj as string+" has children. Aborting.")
		)
	)
	-- cloth
	clearSelection()
	select $cloth_*
	local clothMeshes = selection as array
	if clothMeshes.count>0 then
	for clothMesh in clothMeshes do
	(
		local LinkType_RENDERSIM = 2
		local simModel = RsSceneLink.GetParent LinkType_RENDERSIM clothMesh
		if undefined!=simModel then
			RsCloth.IsValidCloth simModel
		else
			RsCloth.IsValidCloth clothMesh
	)
	
	local wheelmeshes = $wheelmesh* as array
	for wheel in wheelmeshes where wheel.parent!=undefined do
	(
		local materials = RsGetMaterialsOnObjFaces wheel
		for m in materials do
		(
			print materials[1]
			local shaderName = RstGetShaderName materials[1]
			print shaderName
			if "vehicle_tire.sps"!=shaderName then
			(
				if shaderName==false then
					shaderName = "Standard Material"
				gRsUlog.LogError ("Hierarchy linked wheelmesh "+wheel.name+" has non tire shader applied:"+shaderName as string) context:wheel
			)
		)
	)

	select cursel

	return passedTests
)

fn CheckForValidWeldableSkin obj = 
(
	select obj
	setCommandPanelTaskMode mode:#modify
	modPanel.setCurrentObject obj.modifiers[#skin] node:obj ui:true
	local boneCount = skinOps.GetNumberBones obj.modifiers[1]
	local allValid = true

	for j = 1 to boneCount do 
	(
		local boneName = skinOps.GetBoneName obj.modifiers[1] j 1
		local boneObject = getNodeByName boneName

		-- checking for meshes used as bones
		local objClass = classof boneObject
		if 0==findItem gValidBoneTypes (classof boneObject) then
		(
			gRsUlog.LogWarning ("vehicle has a bone (\""+boneObject.name+"\") with invalid type:(\""+objClass as string+"\"). Please use exclusively bone and dummy objects.") context:boneObject
			allValid = false
		)
	)
	return allValid
)

fn CheckForVertexChannels obj = 
(
	local hasBadUVData = false
	if "Gta Object" != getAttrClass obj then
		return hasBadUVData
	
	local intf = RsMeshPolyOp obj
	local badUVChannels = #(9,14)
	for c in badUVChannels where 
		intf.GetMapSupport obj c and 
		intf.GetNumMapVerts obj c > 0 do
	(
		gRsUlog.LogWarning (obj.name+" has bad vertex channel data in channel "+c as string) context:obj quiet:false
		hasBadUVData = true
	)
	
	return hasBadUVData
)

--
-- name: RsVehicleTestObject
-- desc: Test an individual object before skinning
--
fn RsVehicleTestObject obj = (

	local fEpsilon = 0.01f

	if RsContainsSpaces obj.name then (

		gRsUlog.logError (obj.name + " has spaces in its name") context:obj
		return false			
	)

	if ( Editable_mesh == classof obj ) then
	(
		if not (meshop.gethiddenfaces obj).isempty then
			gRsUlog.LogWarning ("Object "+obj.name+" has hidden geometry.") context:obj
	)
	if ( Editable_Poly == classof obj ) then
	(
		if not (polyop.gethiddenfaces obj).isempty then
			gRsUlog.LogWarning ("Object "+obj.name+" has hidden geometry.") context:obj
	)
	
	if obj.isAnimated and obj.pos.controller.keys.count>0 then
	(
		gRsUlog.LogMessage ("Setting userprop \"exportTrans = true\" on object "+obj.name) context:obj
		setuserprop obj "exportTrans" "true"
	)
	
	-- Validate no scaling on meshes.
	if ( "Gta Object"==(getattrclass obj) ) then 
	(
		in coordsys local (

			if ( ( obj.scale.x < ( 1.0 - fEpsilon ) ) or 
				 ( obj.scale.x > ( 1.0 + fEpsilon ) ) or 
				 ( obj.scale.y < ( 1.0 - fEpsilon ) ) or
				 ( obj.scale.y > ( 1.0 + fEpsilon ) ) or 
				 ( obj.scale.z < ( 1.0 - fEpsilon ) ) or 
				 ( obj.scale.z > ( 1.0 + fEpsilon ) ) ) then 
			(
				gRsUlog.LogError (obj.name+" has scaling "+obj.scale as string+", cannot export.") context:obj
			)
		)
	)
	
	-- Might be a pre hand skinned object
	if obj.modifiers["Skin"]!=undefined then
		if not CheckForValidWeldableSkin obj then
			return false
	
	-- Recurse
	local allChildrenValid = true
	for childobj in obj.children do 
	(
		if not RsVehicleTestObject childobj then 
			allChildrenValid = false
	)
	
	return allChildrenValid
)

--
-- name: RsVehicleIsValidForExport
-- desc: check that we have a valid file for export
--
fn RsVehicleIsValidForSkinning obj = 
(
	if obj.parent != undefined then (

		gRsUlog.LogError (obj.name+" has to be a root object") context:obj
		return false
	)
	
	local toleratedObjClasses = #(RsRageParticleHelper, Camera)
	local doubleObjNames = #()
	
	for o in objects where undefined!=o.parent do 
	(
		local objWithSameName = getNodeByName o.name all:true
		for p=1 to objWithSameName.count where 0!=finditem toleratedObjClasses (classof objWithSameName[p]) do
			deleteItem objWithSameName p
		if objWithSameName.count>1 then
			appendIfUnique doubleObjNames o.name
	)
	
	if doubleObjNames.count>0 then
	(
		for n in doubleObjNames do
			gRsuLog.LogError ("Scene has multiple objects with identical name:"+n) context:n
		
		return false
	)
	
	local otherTestResults = RsVehicleTestObject obj

	return otherTestResults
)

fn RsTreatTerrainChannelData = 
(
	if gObjsWithTerrainVertChannel.count>0 then
	(
		local deleteTerrainData = 1
		if not GetQuietMode() then
		(
			deleteTerrainData = RsQueryBoxMultiBtn "There was data found in one fo the the vertex channels that are used for non-vehicle special cases. Do you want to delete the \"terrain painter\" data?" \
				title:"Terrrain Painter data found!" \
				labels:#("Yes", "Cancel") \
				defaultBtn:1 \
				listItems:(for o in gObjsWithTerrainVertChannel collect o.name) \
				timeout:20
		)
		case deleteTerrainData of
		(
			2:
			(
				gRsUlog.LogError ("Bad vertex channel data found on vehicle. Skinnig process aborted.") context:gObjsWithTerrainVertChannel
			)
			default:
			(
				for o in gObjsWithTerrainVertChannel where isvalidNode o do
				(
					local intf = RsMeshPolyOp o
					if intf.getmapsupport o.baseobject 9 then
					(
						intf.setMapSupport o.baseobject 9 false
					)
					if intf.getmapsupport o.baseobject 14 then
					(
						intf.setMapSupport o.baseobject 14 false
					)
				)
			)
		)
	)
)

--
-- name:
-- desc:
--
fn RsVehicleCheckBatchExport batchWarnings = (

	if batchWarnings.count > 0 then (	
		
		global tempArr = #()
		for item in batchWarnings do (
			append tempArr item

		)
		
		rollout RsBatchWarningsRoll "Batch Export Warnings"
		(
			listbox lstMissing1 items:tempArr
			button btnOK "OK" width:100 pos:[150,150]

			
			
			on btnOK pressed do (

				DestroyDialog RsBatchWarningsRoll
				
			)
		)

		CreateDialog RsBatchWarningsRoll width:400 modal:true			
	)
)

-- validation.ms
