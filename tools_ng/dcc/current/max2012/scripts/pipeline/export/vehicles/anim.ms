--
-- File:: pipeline/export/vehicles/anim.ms
-- Description:: Animated vehicle exporter functions.
--
-- Author:: Adam Munson <adam.munson@rockstarnorth.com>
-- Date:: 08 July 2010
--
-- Based off of the export\weapon\anim.ms script
--
-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/util/rexreport.ms"
filein "pipeline/export/models/ruby_script_generator.ms"
filein "rockstar/util/anim.ms"
filein "pipeline/export/vehicles/utils.ms"
filein "pipeline/util/skinutils.ms"
-- 
filein "pipeline/util/ruby.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
--
-- name: RsVehicleIsValidBone
-- desc: Determine whether a particular node is a valid vehicle bone.
--
--global gOnlyExportSelection

fn RsVehicleIsValidBone rootbone = 
(
	local retval = true

	if not (RsSkinUtils.IsBone rootBone) do 
	(
		retval = false
--		print (rootbone.name+":"+retVal as string+". Not used as bone.")
	)
	if ::gOnlyExportSelection and 0==(findItem (selection as array) rootbone) then
	(
		retval = false
		--print (rootbone.name+":"+retVal as string+". not found in selection.")
	)
	
	return retval
)

--
-- name: RsFindVehicleBonesRecursive
-- desc: Recursively finds all bones in the vehicle, also filtered by 
--	 if they are animated (e.g. if saving, only selects animated bones)
--
fn RsFindVehicleBonesRecursive obj boneList animatedFilter:false disregardAnim:false = 
(
 	if obj.scale.controller.keys.count>1 then
 		gRsUlog.LogError ("Object "+obj.name+" has scaling applied. This is not supported at the moment.") context:obj
	
	if (RsVehicleIsValidBone obj == true) do ( 

		if (animatedFilter == true ) then (
		
			if (RsVehicleIsBoneAnimated obj == true ) do (
		
				append boneList obj
				--format "adding: % to the bonelist \n" obj
			)
		)
		else (	
		
			append boneList obj
		)
	)
	if disregardAnim then
	(
		deleteKeys obj.pos.controller
		deleteKeys obj.rotation.controller
		deleteKeys obj.scale.controller
		deleteKeys obj.transform.controller
	)
	for child in obj.children do (
	
		RsFindVehicleBonesRecursive child boneList animatedFilter:animatedFilter disregardAnim:disregardAnim
	)	
)

--
-- name: RsFindVehicleBones
-- desc: Finds all bones for the given vehicle and selects them
--
fn RsFindVehicleBones vehicle animatedFilter:false disregardAnim:false = (

	local boneList = #()
	RsFindVehicleBonesRecursive vehicle boneList animatedFilter:animatedFilter disregardAnim:disregardAnim
	if boneList.count<1 then
		gRsUlog.LogWarning ("No bones found to export! Selected ones not animated?")
	select boneList
	-- always reset this
	gOnlyExportSelection = false
	return boneList
)

--
-- name: RsVehicleAnimSave
-- desc: Save a vehicle animation out to .xaf
--
fn RsVehicleAnimSave animFilename vehicle = (

	gRsULog.Init "Animation saving" appendToFile:false
	
	-- Only select bones that are animated, to save to .xaf
	local fomerSelection = (selection as array)
	bonesList = RsFindVehicleBones vehicle animatedFilter:true

 	if not gRsUlog.Validate() then
 		return false

	attrNames = #()
	attrValues = #()
	
	local newRange = (interval -1 -1)
	-- set global animation range to export
	for obj in bonesList do
	(
		local objRange = RsGetAnimRange obj
		if newRange.start>objRange.start then newRange.start=objRange.start
		if newRange.end<objRange.end then newRange.end=objRange.end
	)
	if ((newRange.end-newRange.start) as Integer)>0 then
	(
		animationRange = newRange
	)
	else
	(
		gRsUlog.LogWarning ("No animation found on objects.")
	)
	LoadSaveAnimation.setUpAnimsForSave bonesList animatedTracks:true includeContraints:true keyable:true
	result = LoadSaveAnimation.saveAnimation animFilename bonesList attrNames attrValues animatedTracks:true includeConstraints:true keyableTracks:true saveSegment:true segInterval:animationRange segKeyPerFrame:false

	select fomerSelection
)

--
-- name: RsVehicleAnimLoad
-- desc: Load a vehicle animation .xaf file
--
fn RsVehicleAnimLoad animFilename vehicle disregardAnim:true isExport:false = (
	-- Select all bones when loading an anim (animatedFilter not used)
	gRsULog.Init "Animation loading" appendToFile:(isExport)
	local bonesList = RsFindVehicleBones vehicle animatedFilter:false disregardAnim:disregardAnim segKeyPerFrame:isExport
	LoadSaveAnimation.setUpAnimsForLoad bonesList
	LoadSaveAnimation.loadAnimation animFilename bonesList
	
	-- checking for errors.
	RsFindVehicleBones vehicle
	
	select vehicle
	
	-- set global animation range to export
	local newRange = RsGetAnimRange vehicle
	if ((newRange.end-newRange.start) as Integer)>0 then
	(
		animationRange = newRange
	)
	else
	(
		gRsUlog.LogWarning ("Loaded animation has no length.")
		if not gRsUlog.validate() then
			return false
	)
	
	if not gRsUlog.Validate() then
		return false
	return true
)

--
-- name: RsGetVehicleAnimationList
-- desc: Returns a list of any animations the vehicle max file has
--
fn RsGetVehicleAnimationList filename filteredList = (

	animDir = maxFilePath + "anim"
	animFilter = animDir + "\\*.xaf"
	animList = getFiles animFilter
	filename = RsRemoveExtension filename
	if matchPattern fileName pattern:"*_skinned" then
	(
		fileName = (substring fileName 1 ((findString fileName "_skinned")-1))
	)
	pattern = "*" + filename + "_*xaf"	
	
	for anim in animList do (
	
		--Check if the animation filename contains the max file name and then any
		--characters after it (the actual animation name)
		if (matchpattern anim pattern:pattern == true) do (
			
			append filteredList anim
		)
	)
)

--
-- name: RsVehicleAnimExport
-- desc: Exports vehicle animation data using RexMax
--
fn RsVehicleAnimExport animFilename vehicleRoot fromUI:true cl:undefined = 
(
	gRsULog.Init "Animation export" appendToFile:(not fromUI)

	local vehicleName 	= ( RsRemoveExtension maxfilename )
	local vehicleFile 	= ( RsVehicleSettings.GetScriptDir() + vehicleName )
	local animName 		= ( RsRemovePathAndExtension animFilename )

	local fileName = (filterstring maxFileName ".")[1]
	if matchPattern fileName pattern:"*_skinned" then
	(
		fileName = (substring fileName 1 ((findString fileName "_skinned")-1))
	)

	local animPath		= ( RsConfigGetAssetsDir() + "vehicles/anims/" + fileName + "/" )
	local clipFile 		= ( RsRemoveExtension animFilename )

	if matchPattern vehicleName pattern:"*_skinned" then
	(
		local charindex = (findString vehicleName "_skinned")-1
		vehicleName = (substring vehicleName 1 charindex)
	)
	if matchPattern animName pattern:("*"+vehicleName+"*") then
	(
		local charindex = (findString animName vehicleName)+vehicleName.count+1
		local newLength = (animName.count-charindex)+1
		animName = substring animName charindex newLength
		if ""==animName then
			animName = "all"
	)
	if matchPattern clipFile pattern:("*"+vehicleName+"*") then
	(
		clipFile = animPath + animName
	)

	RsMakeSurePathExists animPath

	-- add the anim/clip to p4, mark for delete any other anims (potentially old)
	local animResourcePath =  animPath + "/" + animName + ".anim"
	local clipResourcePath =  animPath + "/" + animName + ".clip"
	gRsPerforce.add #( "-t", "binary+m", animResourcePath, clipResourcePath) silent:true
	gRsPerforce.edit #(animResourcePath, clipResourcePath) silent:true
	if cl!=undefined then
		gRsPerforce.addToChangelist cl #(animResourcePath, clipResourcePath)
		

	rexReset()
	local graphRoot = rexGetRoot()
	
	bonelist = RsFindVehicleBones vehicleRoot animatedFilter:true
	if not gRsUlog.Validate() then
		return false
	
	local chassis = for c in vehicleRoot.children while c.name=="chassis" collect c
	if chassis.count==1 then
	(
		chassis = chassis[1]
	)
	else
	(
		gRsUlog.LogError (vehicleRoot.name+" needs \"chassis\" child to wrap the vehicle hierarchy.")
		return false
	)
	append bonelist chassis
	select bonelist	
	gRsUlog.LogMessage ("boneslist: " + bonelist as string)
	
	-- Try spec file way when there is one main root bone as the parent
	specfilename = sysInfo.tempdir + "vehicleAnimSpec.xml"
	RsCreateSpecFile vehicleRoot specfilename useWildcard:false
	
	-------------------------------------------------------------------------
	-- Rex Export Animation
	-------------------------------------------------------------------------
	local animation = rexAddChild graphRoot animName "Animation"
	rexSetPropertyValue animation "OutputPath" animPath
	rexSetPropertyValue animation "ExportCtrlFile" specfilename
	rexSetPropertyValue animation "AnimMoverTrack" "false"
	rexSetPropertyValue animation "AnimAuthoredOrient" "true"
	rexSetPropertyValue animation "AnimCompressionTolerance" "0.0005"
	rexSetPropertyValue animation "AnimLocalSpace" "true"
	gRsUlog.LogMessage ("rex ANIMATION node selection:"+(selection as array) as string)
	rexSetNodes animation

	RexExport false (gRsUlog.Filename())
	clearSelection()
	deletefile specfilename
	if not gRsUlog.Validate() then
		return false
	
	-------------------------------------------------------------------------
	-- Export Clip
	-------------------------------------------------------------------------
	format "Saving clip %\n" clipFile
		
	clipEditor clipFile
	clipload clipFile
	clipSetProperty "Compressionfile_DO_NOT_RESOURCE" (clipGetStringId()) "Compressionfile_DO_NOT_RESOURCE" "Default_Compress.txt"
	clipsave clipFile
	
	-- Reselect vehicle dummy again
	select vehicleRoot
	return true
)

--
-- name: RsVehicleAnimExportResource
-- desc: Resource the vehicle anim data using RageBuilder.
--
fn RsVehicleAnimExportResource vehicle = 
(
	local vehicleName = ( RsRemoveExtension maxfilename )
	if matchPattern vehicleName pattern:"*_skinned" then
	(
		vehicleName = (substring vehicleName 1 ((findString vehicleName "_skinned")-1))
	)

	RBGenVehicleResourcingScriptAnim()
	
	return true
)

--
-- name: RsSelectVehicleSkeleton
-- desc: Selects the vehicle skeleton object
--
fn RsSelectVehicleSkeleton vehicleSkin = (

	local vehicleSkelName
	local vehicleSkel
	
	if (matchpattern vehicleSkin.name pattern:"*_skin" == true) then (
	
		-- Strip the _skin part and add _skel
		vehicleSkelName = substring vehicleSkin.name 1 (vehicleSkin.name.count - 5)
		vehicleSkelName += "_skel"
		vehicleSkel = (getnodebyname vehicleSkelName)
	)
	else (
	
		return false
	)
	
	if (vehicleSkel != undefined) do (
	
		select vehicleSkel
		vehicleSkel
	)
)
