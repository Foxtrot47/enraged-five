-- vehicleGeom.ms
-- geometry functions for vehicle export logic

filein "pipeline/export/maps/fragment.ms"
filein "pipeline/export/maps/mapsetup.ms"

struct RsVehicleGeometry
(
	--------------------------------------------------------------
	-- export the models and fragments for the whole map
	--------------------------------------------------------------
	fn ExportModels models rootfolder exportTextures:true isMods:false clNum:0 storeOutFiles:undefined = 
	(
		local assetsDir = RsConfigGetAssetsDir()
		local RsVehModelExportCancelled = false
		local buildSuccess = true

		specfilename = sysInfo.tempdir + "spec.xml"

		inputobjlist = #()
		txdlist = #()
		txdtexmaplist = #()
		txdtexmaptypelist = #()
		txdmaxsizelist = #()
		txdisbumplist = #()
		txdusagelist = #()
		txdresizeexemptionlist = #()
		txdtexturetemplatelist = #()

		out_tex_tcsdir = #()
		out_texmapobj = #()
		out_texmap = #()
		out_outputfile = #()
		out_templatexml = #()
		out_texmaptype = #()	
		out_texturetemplate = #()
		out_exemptionlist = #()
		
		idxFragment = getattrindex "Gta Object" "Is Fragment"
		idxNewFrag = getattrindex "Gta Object" "New Frag"
		idxDynamic = getattrindex "Gta Object" "Is Dynamic"
		idxPackTextures = getattrindex "Gta Object" "Pack Textures"
		idxTxd = getattrindex "Gta Object" "TXD"
		idxHasUvAnim = GetAttrIndex "Gta Object" "Has UvAnim"
		idxAuthoredOrients = GetAttrIndex "Gta Object" "Authored Orients"
		idxNewAmbient = getattrindex "Gta Object" "Day/Night Ambient"
		idxKeepBoneRot = getattrindex "Gta Object" "Keep Bone Rotations"
		idxGroupName = getattrindex "Gta Object" "groupName"
		idxRemoveDegen = GetAttrIndex "Gta Object" "Remove Degenerate Polys"
		idxWeldWithoutQuant = GetAttrIndex "Gta Object" "Weld Without Quantise"
		idxWeld = GetAttrIndex "Gta Object" "Weld Mesh"
		
		progressStart "exporting additional models"
		garbageCollectCount = 0
		local modelCount = models.count
		
		local buildSuccess = true
		
		for i = 1 to modelCount while buildSuccess and (RsMapProgress i modelCount) do 
		(
			local lodGroupCount = 0
			local model = models[i]
			format "(% of %) %\n" i modelCount model.name
			
			RsSpecularMapHack model model.material

			exportDir = (rootfolder + model.name )
			if matchPattern exportDir pattern:"*_ng" then
				exportDir = substring exportDir 1 (exportDir.count - 3)
			
			RsDeleteDirectory (exportDir + "/")

			if ( ( getattr model idxFragment == true ) or ( getattr model idxNewFrag == true ) ) then 
				append exportDir "_frag"
			append exportDir "/"
			
			RsDeleteDirectory exportDir

			select model

			rexReset()
			graphRoot = rexGetRoot()

			if getattrclass model == "Gta Object" then 
			(
				if ( ( getattr model idxFragment == true ) or ( getattr model idxNewFrag == true ) ) then 
				(
					tunePath = RsConfigGetCommonDir() + "data/fragments/"
					tuneFile = model.name + ".tune" -- (substring model.name 1 (model.name.count-5)) + ".tune"

					safeTunePath = RsMakeBackSlashes tunePath
					safeExportDir = RsMakeBackSlashes exportDir

					makeDir safeExportDir
					copyFile (safeTunePath + tuneFile) (safeExportDir + "\\" + tuneFile) 

					RsTotalSurfProperty = ""

					rexReset()
					graphRoot = rexGetRoot()

					entity = rexAddChild graphRoot "entity" "Entity"
					rexSetPropertyValue entity "OutputPath" exportDir
					rexSetPropertyValue entity "EntityWriteBoundSituations" "true"
					rexSetPropertyValue entity "EntityFragmentWriteMeshes" "true"

					local params = #(hasCloth=false, hasOnlyCloth=true)
					TraverseHierarchyRec model CheckForCloth &params
					local hasCloth = params[1]
					local hasOnlyCloth = params[2]
					print ("model "+model.name+" hascloth:"+hasCloth as string+" hasOnlyCloth:"+hasOnlyCloth as string)
					if hasCloth then
					(
						rexSetPropertyValue entity "EntitySplitLODGroupsByFlags" "true"
						rexSetPropertyValue entity "EntitySplitFlagGroupsToDrawables" "true"

  						rexSetPropertyValue entity "SkipBoundSectionOfTypeFile" "false"
  						rexSetPropertyValue entity "EntityBoundListName" "collisionbounds"
					)
					else
						rexSetPropertyValue entity "SkipBoundSectionOfTypeFile" "true"
					
					rexSetPropertyValue entity "EntityAllowEmptyFragSection" "true"

					if model.modifiers.count > 0 and classof model.modifiers[1] == Skin then 
					(
						boneroot = rexGetSkinRootBone model
					) 
					else 
					(
						boneroot = model 
					)

					---------------------------------------------
					-- Cloth collision is linked with RsSceneLink plugin
					---------------------------------------------
					RsSetupFragBounds boneroot entity exportDir
					rexSetPropertyValue entity "SurfaceType" RsTotalSurfProperty

					select model

					boneroot = rexGetSkinRootBone model
					if boneroot == undefined then boneroot = model
					select boneroot
					RsCreateSpecFile boneroot specfilename foranim:false formap:true
					select model

					skeleton = rexAddChild entity "skeleton" "Skeleton"
					rexSetPropertyValue skeleton "SkeletonAuthoredOrient" "true"
					rexSetPropertyValue skeleton "OutputPath" exportDir
					--rexSetPropertyValue skeleton "ChannelsToExport" "(transX*;transY*;transZ*;)(rotX;rotY;rotZ;)"
					--rexSetPropertyValue skeleton "ChannelsToExport" "(transX;transY;transZ;)(rotX;rotY;rotZ;)"
					rexSetPropertyValue skeleton "ExportCtrlFile" specfilename
					rexSetPropertyValue skeleton "SkeletonWriteLimitAndLockInfo" "true"
					rexSetPropertyValue skeleton "SkeletonRootAtOrigin" "true"
					rexSetNodes skeleton

					-- RAGE Drawable LOD Support
					local allFragMeshes = #()
					local clothLodGroup = if hasOnlyCloth then "lodgroup" else "lodgroupcloth"
					local params = #(allFragMeshes, lodGroupCount, hasCloth, clothLodGroup)
					TraverseHierarchyRec model SetLinkUserProps &params
					if allFragMeshes.count>1 then 
						lodGroupCount = lodGroupCount+1
--					messagebox (allFragMeshes as string)
					selectMore allFragMeshes

					local exportNodeRotations = not hasCloth
					
					meshobj = rexAddChild entity "mesh" "Mesh"
					rexSetPropertyValue meshobj "OutputPath" exportDir
					rexSetPropertyValue meshobj "ExportTextures" "false"
					rexSetPropertyValue meshobj "MeshForSkeleton" (exportNodeRotations as string)
					rexSetPropertyValue meshobj "MeshAsAscii" (RsVehicleAsAscii as string)
					rexSetPropertyValue meshobj "MeshSkinOffset" "false"
					rexSetPropertyValue meshobj "MeshRemoveDegenerates" ((GetAttr model idxRemoveDegen) as string)
					rexSetPropertyValue meshobj "MeshWeldWithoutQuantize" ((GetAttr model idxWeldWithoutQuant) as string)
					rexSetPropertyValue meshobj "MeshWeld" ((GetAttr model idxWeld) as string)
					rexSetPropertyValue meshobj "MeshCustomVertexColour" "illum.y>colour.y?shadvar[#all]=EmissiveMultiplier"

					local savesel = selection as array
					local clothMeshes = #()
					TraverseHierarchyRec model CollectClothMeshes &clothMeshes
					if clothMeshes.count>0 then
					(
						RsCloth.SetupClothExpectedVertCount meshobj meshes:clothMeshes
					)
					select savesel
					
					grsulog.logMessage ("mesh and fragment selection:"+((selection as array) as string)) context:meshobj
					rexSetNodes meshobj

					skeleton = rexAddChild entity "fragment" "Fragment"
					rexSetPropertyValue skeleton "OutputPath" exportDir

					rexSetNodes skeleton

					-- Cloth Support
					local exclusiveClothMeshes = selection as array
					if clothMeshes.count>0 then
					(
						RsCloth.SetupEnvClothForExport (clothMeshes as array) entity meshobj
					)
					
					-- breakable window
					local glassCollectionParams = #("", "")
					TraverseHierarchyRec model CollectGlassNodeNames &glassCollectionParams
					if glassCollectionParams[1].count>0 then
						rexSetPropertyValue skeleton "FragmentBreakableGlass" glassCollectionParams[1]
					
					if glassCollectionParams[2].count>0 then
					(
						setUserProp model "glasstype" glassCollectionParams[2]
					)
					
					select model

				) 
				else 
				(

					entity = rexAddChild graphRoot "entity" "Entity"
					rexSetPropertyValue entity "OutputPath" exportDir

					if getattr model idxHasUvAnim then (

						rexSetPropertyValue entity "MeshOptimiseMaterials" "false"
					) else (

						rexSetPropertyValue entity "MeshOptimiseMaterials" "true"
					)

					--local modellodobjs = #()
					-- RAGE Drawable LOD Support - just for root. naimated objects get traversed through above
					local modellodobjs = RsLodDrawable_SetUserProps model &lodGroupCount
					if modellodobjs.count>1 then lodGroupCount = lodGroupCount+1

					local params = #(hasCloth=false, hasOnlyCloth=true)
					TraverseHierarchyRec model CheckForCloth &params
					local hasCloth = params[1]
					local hasOnlyCloth = params[2]

					if getattr model idxDynamic == true or RsIsObjectAnimated model then 
					(

						skeleton = rexAddChild entity "skeleton" "Skeleton"
						rexSetPropertyValue skeleton "SkeletonAuthoredOrient" "true"
						rexSetPropertyValue skeleton "OutputPath" exportDir
						rexSetPropertyValue skeleton "SkeletonWriteLimitAndLockInfo" "true"
						rexSetPropertyValue skeleton "SkeletonRootAtOrigin" "true"

						boneroot = rexGetSkinRootBone model
						if boneroot == undefined then boneroot = model
						select boneroot
						RsCreateSpecFile boneroot specfilename foranim:false formap:true
						select modellodobjs
						rexSetPropertyValue skeleton "ExportCtrlFile" specfilename

						if getattr model idxAuthoredOrients then (

							rexSetPropertyValue skeleton "SkeletonAuthoredOrient" "true"
						) else (

							rexSetPropertyValue skeleton "SkeletonAuthoredOrient" "false"
						)

						rexSetNodes skeleton	

						local allLodAndHiearchyMeshes = #()
						local clothLodGroup = if hasOnlyCloth then "lodgroup" else "lodgroupcloth"
						local params = #(allLodAndHiearchyMeshes, lodGroupCount, hasCloth, clothLodGroup)
						TraverseHierarchyRec model SetLinkUserProps &params
					)
					select model
					selectMore modellodobjs
					
					meshobj = rexAddChild entity "mesh" "Mesh"
					rexSetPropertyValue meshobj "OutputPath" exportDir
					rexSetPropertyValue meshobj "ExportTextures" "false"
					rexSetPropertyValue meshobj "TextureOutputPath" (RsConfigGetMapsTexDir())
					rexSetPropertyValue meshobj "MeshAsAscii" (RsVehicleAsAscii as string)
					rexSetPropertyValue meshobj "MeshSkinOffset" "false"
					rexSetPropertyValue meshobj "MeshExportTextureList" "false"
					rexSetPropertyValue meshobj "MeshForSkeleton" ((GetAttr model idxKeepBoneRot) as string)
					rexSetPropertyValue meshobj "MeshRemoveDegenerates" ((GetAttr model idxRemoveDegen) as string)
					rexSetPropertyValue meshobj "MeshWeldWithoutQuantize" ((GetAttr model idxWeldWithoutQuant) as string)
					rexSetPropertyValue meshobj "MeshWeld" ((GetAttr model idxWeld) as string)
					rexSetPropertyValue meshobj "MeshCustomVertexColour" "illum.y>colour.y?shadvar[#all]=EmissiveMultiplier"
					
					lodIsTreeBillboard = false
					for lodobj in modellodobjs do (
						if RsIsTreeBillboard lodobj lodobj.mat then lodIsTreeBillboard = true
					)
					if RsIsTreeBillboard model model.mat or lodIsTreeBillboard then (
						rexSetPropertyValue meshobj "ForcedAdditionalUVCount" "2"
					)

					if getattr model idxHasUvAnim then (

						rexSetPropertyValue meshobj "MeshOptimiseMaterials" "false"
					) else (

						rexSetPropertyValue meshobj "MeshOptimiseMaterials" "true"
					)

					texFilename = exportDir + "/entity.textures"
					RsMakeSurePathExists texFilename

					RsDeleteFiles texFilename
					 
					gRsUlog.LogMessage ("rex MESH object input selection:"+(selection as array) as string) context:model
					rexSetNodes meshobj
				)


				buildSuccess = RexExport false (gRsUlog.Filename())

				/*
 				if buildSuccess do 
 				(
 					-- Write out drawable IDE file with packed 2dfx objects.
 					local ideFilename = exportDir + "\\" + model.name + ".ide"
 					local xmlFilename = exportDir + "\\" + model.name + ".xml"
 					buildSuccess = RsSceneXmlExportIDEForObject ideFilename model.name filename:xmlFilename
 				)
				*/
				
				/*
					MOD object processing
					--------------
					There was loads of issues with this. Initially we though mods had to go through ragebuilder vehicle postprocessing to 
					get emissive frags working correctly, veh art reporting it actually doesn't need this - so this is disabled for now.

					The code path is still available - but given that the RB processing screws the damage CPV (as there are no seat bones
					to work from in a mod fragment, and adding them isn't ideal for runtime) it might be better to leave this turned off.
				*/
				/*
				if (RsVehicleUtil.HasAnyEmissiveShaders model) then 
				(
					RsVehicleUtil.CreateCustomRagebuilderScripts false customDir:exportDir
				)
				*/

				if undefined==gAllCurrentVehicleTextures then
					messagebox "Vehicle textures didn't get set before additional model export!" title:"Error!"
				else if buildSuccess and exportTextures do 
				(
					local texmaps = ::RsVehicleTextureStruct()

					RsGetTexMapsFromObjNoStrip model \
						texmaps.texmaplist \
						texmaps.maxsizelist \
						texmaps.isbumplist \
						texmapobjlist:texmaps.texmapobjlist \
						maptypelist:texmaps.maptypelist \
						texturetemplatelist:texmaps.texturetemplatelist
					
					-- Remove shared and main vehicle textures using global list we filler earlier.
					texmaps.RemoveSharedFromTexMapList gAllCurrentVehicleTextures
					texmaps.Export exportDir RsVehicleName overwriteTCS:RsOverrideTcs isMods:isMods storeOutFiles:storeOutFiles
				)
			)
		)

		progressEnd()
		
		if RsVehModelExportCancelled do (buildSuccess = false)
		
		gc()

		buildSuccess
	)
)