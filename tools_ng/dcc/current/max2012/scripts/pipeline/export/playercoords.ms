--
-- File:: pipeline/export/playercoords.ms
-- Description::
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 14 May 2010
--


if ( 0 == selection.count ) then
(
	MessageBox "Select the object whose coordinates you want exported."
)
else
(
	local buildDir = RsConfigGetBuildDir()	
	-- IF ON LIBERTY POINT BACK TO THE GTA5 BUILD FOLDER
	if (matchpattern buildDir pattern:"*gta5_liberty*") do buildDir = substituteString buildDir "gta5_liberty" "gta5"
	
	RsMakeSurePathExists buildDir	
	
	local filess = stringStream ""
	format "%playercoords" buildDir to:filess
	if (isKindOf RsMapLevel string) and (RsMapLevel != "") do 
	(
		format "_%" RsMapLevel to:filess
	)
	format ".txt" to:filess
	
	local filename = ( filess as string )
	local fp = createFile filename
	if ( undefined != fp ) then
	(
		local obj = selection[1]
		local x = obj.position.x
		local y = obj.position.y
		local z = obj.position.z+1.0
		format "% % %" x  y  z to:fp
		close fp
		format "Player coordinates written to: %\n" filename
	)
	else
	(
		local msg = stringStream ""
		format "Failed to create player coordinates file: %." filename
	)
)

-- pipeline/export/playercoords.ms
