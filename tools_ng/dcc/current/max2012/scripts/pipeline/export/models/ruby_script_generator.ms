--
-- File:: pipeline/export/models/ruby_script_generator.ms
-- Description::
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 7 December 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/models/globals.ms"
filein "pipeline/util/ruby.ms"
filein "pipeline/export/AP3Asset.ms"
filein "pipeline/util/drawablelod.ms"
-- globally loaded from startup.ms: filein "pipeline/export/AP3Content.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
--
-- struct: RsModelScriptGeneratorStruct
-- desc: Model Ruby script generator container for functions.
--
struct RsModelScriptGeneratorStruct
(

	--
	-- name: GenerateResourceCommonHeader
	-- desc:
	--
	-- note: Should not be called externally.
	--
	fn GenerateResourceCommonHeader rubyFile = (
		
		local projname = RsConfigGetProjectName()
		format "require 'pipeline/config/projects'\n" to:rubyFile
		format "Pipeline::Config::instance().logtostdout = true\n" to:rubyFile
		format "require 'pipeline/config/project'\n" to:rubyFile
		format "require 'pipeline/content/content_core'\n" to:rubyFile
		format "require 'pipeline/gui/exception_dialog'\n" to:rubyFile
		format "require 'pipeline/os/file'\n" to:rubyFile
		format "require 'pipeline/projectutil/data_convert'\n" to:rubyFile
		format "require 'pipeline/projectutil/data_zip'\n" to:rubyFile
		format "require 'pipeline/resourcing/convert'\n" to:rubyFile
		format "require 'pipeline/resourcing/util'\n" to:rubyFile
		format "require 'pipeline/util/string'\n" to:rubyFile
		format "include Pipeline\n\n" to:rubyFile

		format "begin\n\n" to:rubyFile
		format "\tproject = Pipeline::Config.instance.projects[\"%\"]\n" projname to:rubyFile
		format "\tproject.load_config( )\n" to:rubyFile
		format "\tbranch = project.branches[project.default_branch]\n" to:rubyFile
	),


	--
	-- name: GenerateResourceCommonFooter
	-- desc:
	--
	-- note: Should not be called externally.
	--
	fn GenerateResourceCommonFooter rubyFile = (

		format "rescue Exception => ex\n\n" to:rubyFile
		format "GUI::ExceptionDialog::show_dialog( ex )\n" to:rubyFile
		format "end\n\n" to:rubyFile 	
	),
	
	--
	-- name: BuildDrawable
	-- desc: Build a drawable IDR file from a directory.
	--
	fn BuildDrawable rubyFileName drawableFile drawableDir = (

		RsMakeSurePathExists rubyFileName
		local rubyFile = createFile rubyFileName
		
		GenerateResourceCommonHeader rubyFile
		
		drawableFile = RsMakeSafeSlashes( drawableFile )
		format "\tidr_filename = '%'\n" drawableFile to:rubyFile
		format "\tif ( 0 != 'zip'.casecmp( OS::Path::get_extension( idr_filename ) ) ) then\n" to:rubyFile
		format "\t\tidr_filename = OS::Path::replace_ext( idr_filename, 'zip' )\n" to:rubyFile
		format "\tend\n" to:rubyFile
		format "\tfiles = OS::FindEx::find_files( OS::Path::combine( '%', '*.*' ) )\n" drawableDir to:rubyFile
		format "\tProjectUtil::data_zip_create( idr_filename, files )\n" to:rubyFile
		
		GenerateResourceCommonFooter rubyFile
		
		close rubyFile
		RsRunRubyScript rubyFileName
	),
	
	--
	-- name: BuildFragment
	-- desc: Build a fragment IFT file from a directory.
	--
	fn BuildFragment rubyFileName fragmentFile fragmentDir = (

		RsMakeSurePathExists rubyFileName
		local rubyFile = createFile rubyFileName
		
		GenerateResourceCommonHeader rubyFile
		
		fragmentFile = RsMakeSafeSlashes( fragmentFile )
		format "\tift_filename = '%'\n" fragmentFile to:rubyFile
		format "\tif ( 0 != 'zip'.casecmp( OS::Path::get_extension( ift_filename ) ) ) then\n" to:rubyFile
		format "\t\tift_filename = OS::Path::replace_ext( ift_filename, 'zip' )\n" to:rubyFile
		format "\tend\n" to:rubyFile
		format "\tfiles = OS::FindEx::find_files( OS::Path::combine( '%', '*.*' ) )\n" fragmentDir to:rubyFile
		format "\tProjectUtil::data_zip_create( ift_filename, files )\n" to:rubyFile
				
		GenerateResourceCommonFooter rubyFile
		
		close rubyFile
		RsRunRubyScript rubyFileName	
	),
	
	--
	-- name: BuildDrawableDictionary
	-- desc: Build a drawable dictionary IDD file from a set of directories.
	--
	fn BuildDrawableDictionary rubyFileName drawableDictFile drawableDirs = (

		RsMakeSurePathExists rubyFileName
		local rubyFile = createFile rubyFileName
		
		GenerateResourceCommonHeader rubyFile
		
		drawableDictFile = RsMakeSafeSlashes( drawableDictFile )
		format "\tidd_filename = '%'\n" drawableDictFile to:rubyFile
		format "\tif ( 0 != 'zip'.casecmp( OS::Path::get_extension( idd_filename ) ) ) then\n" to:rubyFile
		format "\t\tidd_filename = OS::Path::replace_ext( idd_filename, 'zip' )\n" to:rubyFile
		format "\tend\n" to:rubyFile
		format "\tfile_list = []\n" to:rubyFile
		for drawableDir in drawableDirs do
		(
			format "\tfiles = OS::FindEx::find_files( OS::Path::combine( '%', '*.*' ) )\n" drawableDir to:rubyFile
			format "\tfiles.each do |filename|\n" to:rubyFile
			format "\t\tentry = {}\n" to:rubyFile
			format "\t\tentry[:src] = filename\n" to:rubyFile
			format "\t\tentry[:dst] = OS::Path::combine( OS::Path::get_trailing_directory( '%' ), OS::Path::get_filename( filename ) )\n" drawableDir to:rubyFile
			format "\t\tfile_list << entry\n" to:rubyFile
			format "\tend\n" to:rubyFile
		)		
		format "\tProjectUtil::data_zip_create( idd_filename, file_list )\n" to:rubyFile
				
		GenerateResourceCommonFooter rubyFile
		
		close rubyFile
		RsRunRubyScript rubyFileName
	)	
)

struct RsSimpleModelScriptGeneratorStruct
(
	fn ExportITYPFile filename rootName objectList =
	(
		assetXMLDocument = XmlDocument()
		assetXMLDocument.init()
		
		assetXmlRoot = assetXMLDocument.createelement("CMapTypes")
		assetXMLDocument.document.AppendChild assetXmlRoot
		
		-- Add the root name
		nameElement = assetXMLDocument.createelement("name")
		nameElement.InnerText = rootName
		assetXmlRoot.AppendChild nameElement
		
		archetypeElement = assetXMLDocument.createelement("archetypes")
		assetXmlRoot.AppendChild archetypeElement
		
		local exportCount = 0
		
		for o in objectList do
		(
			if (GetAttrClass o == "Gta Object") then
			(
				local dontExportIdx = getAttrIndex "Gta Object" "Dont Export"
				local dontExportAttr = getAttr o dontExportIdx
					
				if ((GetAttrClass o) == "Gta Object" and dontExportAttr != undefined and dontExportAttr) then
					continue
				
				-- Don't export low detail models. They should already get picked up with the high detail mesh export.
				local lodParent = RsLodDrawable_GetHigherDetailModel o
				if (lodParent != undefined) then
					continue
			
				exportCount += 1
				
				itemElement = assetXMLDocument.createelement("Item")
				itemAttribute = assetXMLDocument.createAttribute("type")
				itemAttribute.Value = "CBaseArchetypeDef"
				itemElement.Attributes.Append itemAttribute
				
				local idxLodDistance = getattrindex "Gta Object" "LOD distance"
				local lodDistance = GetAttr o idxLodDistance
				local objName = o.name
				local specialAttributes = 31
				local textureDictionary = rootName
				local drawableDictionary = rootName
				local flags = 536870912
				local bsCentre = [1,1,1]
				local bsRadius = 0.0f
				
				-- Name Element
				nameElement = assetXMLDocument.createelement("name")
				nameElement.InnerText = objName
				
				itemElement.AppendChild nameElement
				
				-- LOD Element
				lodElement = assetXMLDocument.createelement("lodDist")
				lodValueAttribute = assetXMLDocument.createAttribute("value")
				lodValueAttribute.Value = (lodDistance as string)
				lodElement.Attributes.Append lodValueAttribute
				
				itemElement.AppendChild lodElement
				
				-- Bounding Box Min Element
				in coordsys local
				(
					bbMin = o.min
					bbMax = o.max
				)
				
				bbMinElement = assetXMLDocument.createelement("bbMin")
				bbMinXAttribute = assetXMLDocument.createAttribute("x")
				bbMinYAttribute = assetXMLDocument.createAttribute("y")
				bbMinZAttribute = assetXMLDocument.createAttribute("z")
				bbMinXAttribute.Value = (bbMin.x as string)
				bbMinYAttribute.Value = (bbMin.y as string)
				bbMinZAttribute.Value = (bbMin.z as string)
				bbMinElement.Attributes.Append bbMinXAttribute
				bbMinElement.Attributes.Append bbMinYAttribute
				bbMinElement.Attributes.Append bbMinZAttribute
				
				itemElement.AppendChild bbMinElement
				
				-- Bounding Box Max Element
				bbMaxElement = assetXMLDocument.createelement("bbMax")
				bbMaxXAttribute = assetXMLDocument.createAttribute("x")
				bbMaxYAttribute = assetXMLDocument.createAttribute("y")
				bbMaxZAttribute = assetXMLDocument.createAttribute("z")
				bbMaxXAttribute.Value = (bbMax.x as string)
				bbMaxYAttribute.Value = (bbMax.y as string)
				bbMaxZAttribute.Value = (bbMax.z as string)
				bbMaxElement.Attributes.Append bbMaxXAttribute
				bbMaxElement.Attributes.Append bbMaxYAttribute
				bbMaxElement.Attributes.Append bbMaxZAttribute
				
				itemElement.AppendChild bbMaxElement
				
				-- Calculate bounding sphere
				distx = ((bbMax.X - bbMin.X) / 2.0) as float
				disty = ((bbMax.Y - bbMin.Y) / 2.0) as float
				distz = ((bbMax.Z - bbMin.Z) / 2.0) as float
				dista = sqrt(((distx * distx) as double) + ((disty * disty) as double)) as double
				
				centre = [bbMin.X + distx, bbMin.Y + disty, bbMin.Z + distz]
				radius = sqrt(((dista * dista) as double) + ((distz * distz) as double)) as float
				
				-- Bounding Sphere Centre Element
				bsCentreElement = assetXMLDocument.createelement("bsCentre")
				bsCentreXAttribute = assetXMLDocument.createAttribute("x")
				bsCentreYAttribute = assetXMLDocument.createAttribute("y")
				bsCentreZAttribute = assetXMLDocument.createAttribute("z")
				bsCentreXAttribute.Value = (centre.x as string)
				bsCentreYAttribute.Value = (centre.y as string)
				bsCentreZAttribute.Value = (centre.Z as string)
				bsCentreElement.Attributes.Append bsCentreXAttribute
				bsCentreElement.Attributes.Append bsCentreYAttribute
				bsCentreElement.Attributes.Append bsCentreZAttribute
				
				itemElement.AppendChild bsCentreElement
				
				-- Bounding Sphere Radius Element
				bsRadiusElement = assetXMLDocument.createelement("bsRadius")
				bsRadiusAttribute = assetXMLDocument.createAttribute("value")
				bsRadiusAttribute.Value = (radius as string)
				bsRadiusElement.Attributes.Append bsRadiusAttribute
				
				itemElement.AppendChild bsRadiusElement
				
				-- Special Attribute Element
				specialAttributeElement = assetXMLDocument.createelement("specialAttribute")
				specialAttributeAttribute = assetXMLDocument.createAttribute("value")
				specialAttributeAttribute.Value = (specialAttributes as string)
				specialAttributeElement.Attributes.Append specialAttributeAttribute
				
				itemElement.AppendChild specialAttributeElement
				
				-- Texture Dictionary Element
				textureDictionaryElement = assetXMLDocument.createelement("textureDictionary")
				textureDictionaryElement.InnerText = textureDictionary
				
				itemElement.AppendChild textureDictionaryElement
				
				-- Drawable Dictionary Element
				drawableDictionaryElement = assetXMLDocument.createelement("drawableDictionary")
				drawableDictionaryElement.InnerText = drawableDictionary
				
				itemElement.AppendChild drawableDictionaryElement
				
				-- Flags Element
				flagsElement = assetXMLDocument.createelement("flags")
				flagsAttributeAttribute = assetXMLDocument.createAttribute("value")
				flagsAttributeAttribute.Value = (flags as string)
				flagsElement.Attributes.Append flagsAttributeAttribute
				
				itemElement.AppendChild flagsElement
				
				-- Extensions Element
				extensionsElement = assetXMLDocument.createelement("extensions")
				itemElement.AppendChild extensionsElement
				
				archetypeElement.AppendChild itemElement
			)
		)
		
		format "Exported % objects to the ITYP file: %\n" exportCount filename
		
		assetXMLDocument.save filename
	),
	
	fn BuildZipFile inputFileList outputFilename =
	(
		assetFile = AP3AssetFile()
		assetFile.Initialize()
		
		local mainZipElement = assetFile.AddZipArchive outputFilename
		
		for inputFile in inputFileList do
		(
			format "Adding %\n" inputFile
			assetFile.AddFile mainZipElement inputFile
		)
		
		buildResult = assetFile.BuildAssetFiles()
	),
	
	fn BuildModelResources modelName modelDirectory outputPath generateModelDictionary:false packSeperateTXD:false &resources: =
	(
		assetFile = AP3AssetFile()
		assetFile.Initialize()

		local itdExt = ".itd.zip"
		local idrExt = ".idr.zip"
		local iddExt = ".idd.zip"

		local buildResult = false
		
		--format "Model Name: %\n" modelName
		--format "Model Directory: %\n" modelDirectory
		--format "Output Path: %\n" outputPath

		-- IDD support
		if generateModelDictionary then
		(
			local txdZip = undefined
			local textureList = #()
			local textureNameList = #()

			local drawableDirectories = getDirectories (modelDirectory + "*.*")
			drawableDictionaryZip = assetFile.AddZipArchive (outputPath + modelName + iddExt)
			
			for drawableDir in drawableDirectories do
			(
				--format "Drawable Dir: %\n" drawableName
				
				local pathSplit = filterString drawableDir "\\"
				local drawableName = pathSplit[pathSplit.count]
				local excludeExtensions = #()

				-- If we're building a separate TXD, remove the .dds from the .idd and add the textures to our TXD list.
				if packSeperateTXD then
				(
					append excludeExtensions "dds"
					local textureFiles = getFiles (drawableDir + "*.dds")
					
					for textureEntry in textureFiles do
					(
						local textureName = tolower (getFilenameFile textureEntry)
						if (appendIfUnique textureNameList textureName) then
						(
							append textureList textureEntry
						)
					)
				)

				assetFile.AddFilesFromFolder drawableDictionaryZip drawableDir destination:drawableName excludeExtensions:excludeExtensions
			)
			
			-- Add all our textures to the TXD zip file.
			if packSeperateTXD then
			(
				txdZip = assetFile.AddZipArchive (outputPath + modelName + itdExt)
				
				for textureEntry in textureList do
					assetFile.AddFile txdZip textureEntry
			)

			-- Get a list of the assets so we can build them after.
			if (resources != undefined and resources != unsupplied) then
				join resources (assetFile.GetCurrentAssetFiles())
			
			buildResult = assetFile.BuildAssetFiles()
		)
		else
		(
			drawableZip = assetFile.AddZipArchive (modelName + idrExt)
			assetFile.AddFilesFromFolder drawableZip modelDirectory

			-- Get a list of the assets so we can build them after.
			if (resources != undefined and resources != unsupplied) then
				join resources (assetFile.GetCurrentAssetFiles())
			
			buildResult = assetFile.BuildAssetFiles()
		)

		return buildResult
	),
	
	fn ConvertModelResources resourceFiles =
	(
		if (resourceFiles == undefined or resourceFiles == unsupplied) then
			return false
		
		local returnCode = ( AP3Invoke.Convert resourceFiles debug:true )
		
		return returnCode
	)
)


--
-- struct: RsWeaponScriptGeneratorStruct
-- desc: Weapon Ruby script generator container for functions.
--
struct RsWeaponScriptGeneratorStruct
(	
	--
	-- name: BuildWeaponResources
	-- desc:
	--	
	fn BuildWeaponResources weaponExportStructs justBounds:false justDrawable:false justTXD:false isPatch:false isFragList:#() = 
	(
		gWeaponPackagedFiles = #()
		
		local doAll = not (justBounds or justDrawable or justTXD)
		local sceneName = getFilenameFile maxFilename		
		
		global assetFile = AP3AssetFile()
		assetFile.Initialize()
		
		local itdExt = ".itd.zip"
		local iftExt = ".ift.zip"
		local idrExt = ".idr.zip"
		local stream_dir = (RsConfigGetStreamDir() + "weapons/" + gRsBranch.name + "/")
		local textureZip = undefined
		
		struct weaponTextureStruct (textureDictionary, textures)
		local weaponTextureEntries = #()
		
		for weaponStructIdx = 1 to weaponExportStructs.count do 
		(
			local isFragment = if (isFragList[weaponStructIdx] == undefined) then false else isFragList[weaponStructIdx]
			local weaponName = weaponExportStructs[weaponStructIdx].weaponMeshName
			local txdName = weaponExportStructs[weaponStructIdx].weaponMeshName
			
			local weaponDir = stream_dir + weaponName
			local weaponParentDir = copy weaponDir
			local weaponExt = if isFragment then iftExt else idrExt
			
			if (weaponExportStructs[weaponStructIdx].isHD or weaponExportStructs[weaponStructIdx].isLODs) then
			(
				weaponDir = stream_dir + weaponName + "/" + weaponExportStructs[weaponStructIdx].LODFolder
				
				if (weaponExportStructs[weaponStructIdx].isHD) then
					weaponName = weaponName + "_hi"
			)
			
			-- Pack all the drawables
			if doAll or justDrawable then
			(
				drawableZip = assetFile.AddZipArchive (stream_dir + weaponName + weaponExt)
				assetFile.AddFilesFromFolder drawableZip weaponDir excludeExtensions:#("dds", "tcl", "bnd")
				assetFile.AddFilesFromFolder drawableZip weaponParentDir includeExtensions:#("bnd")
				
				-- Pack the palette textures in the drawables if they exist
				local allFiles = getFiles (weaponDir + "*.*")
				for f in allFiles do
				(
					if (matchPattern f pattern:"*_pal.dds" or matchPattern f pattern:"*_pal.tcl") then
						assetFile.AddFile drawableZip f
				)
			)
			
			-- Pack all the TXD's
			if (doAll or justTXD) then
			(
				local textureEntry = undefined
			
				-- See if we already have an entry for this weapon name (or LOD)
				for weaponEntry in weaponTextureEntries do
				(
					if (matchPattern weaponEntry.textureDictionary pattern:weaponExportStructs[weaponStructIdx].weaponMeshName) then
						textureEntry = weaponEntry
				)
				-- If not, create a new one.
				if textureEntry == undefined then
				(
					textureEntry = weaponTextureStruct textureDictionary:weaponExportStructs[weaponStructIdx].weaponMeshName textures:#()
					append weaponTextureEntries textureEntry
				)
				
				-- Get all the files in the textures directory
				local folderPath = weaponDir
				if folderPath[folderPath.count] != "/" then
				append folderPath "/"
				local allFiles = getFiles (folderPath + "*.*")
				
				for f in allFiles do
				(
					local fileParts = filterString f "."
					if fileParts.count<=0 then 
						continue
					local fileExt = fileParts[fileParts.count]
					local includedExtensions = #("dds", "tcl")
					local filename = filenameFromPath f
					
					if (findItem includedExtensions fileExt != 0) then
					(
						if matchPattern filename pattern:"*_pal*" then
							continue
						
						-- Check to see if we already have an existing texture name. LOD textures should have the same name but they might not so make sure to pack them all together.
						foundExisting = false
						
						for addedFile in textureEntry.textures do
						(
							if (matchPattern (filenameFromPath addedFile) pattern:filename) then foundExisting = true
						)
						
						if not foundExisting then append textureEntry.textures f
					)
				)
			)
		)
		
		local buildResult = true
		
		if doAll or justTXD do
		(
			for textureEntry in weaponTextureEntries do
			(
				textureZip = assetFile.AddZipArchive (stream_dir + textureEntry.textureDictionary + itdExt)
				
				for textureFile in textureEntry.textures do
				(
					assetFile.AddFile textureZip textureFile
				)
			)
		)
		
		-- Get a list of all the created zip files if we need to do a preview export.
		assetZips = assetFile.GetCurrentAssetFiles()
		
		-- Build the resource zip files.
		if buildResult then
			buildResult = assetFile.BuildAssetFiles()
		
		-- Build preview data.
		if (buildResult and isPatch) then
		(
			buildResult = ( 0 == ( AP3Invoke.Convert assetZips debug:true ) )
		)
		
		return buildResult
	),

	--
	-- name: BuildImage
	-- desc: Create and run the vehicle ZIP resourcing Ruby script body.
	--
	fn BuildZIP rebuild:false =
	(
		local weaponsZipPath = (RsConfigGetExportDir() + "models/cdimages/weapons.zip")
		
		-- Gather all the weapon files in the directory and pack them up.
		local stream_dir = (RsConfigGetStreamDir() + "weapons/" + gRsBranch.name + "/")
		local weaponFiles = RsFindFilesRecursive stream_dir "*.i*"
		
		global assetFile = AP3AssetFile()
		assetFile.Initialize()
		
		local weaponZip = assetFile.AddZipArchive weaponsZipPath
		for weaponFile in weaponFiles do
		(
			assetFile.AddFile weaponZip weaponFile
		)
		
		local result = assetFile.BuildAssetFiles()
		
		-- Build the platform specific asset
		if result then
		(
			local returnCode = ( AP3Invoke.Convert #(weaponsZipPath) debug:true )
			result = (returnCode == 0)
		)
		
		return result
	),
	
	--
	-- name: ExtractZIP
	-- desc: Create and run the vehicle ZIP extraction script.
	--
	fn ExtractZIP rubyFileName =
	(
		if RsIsDLCProj() then
		(
			if (QueryBox "This is a DLC project, are you sure?") do
			(
				return true
			)
		)		
		zipClass = dotNetClass "RSG.Pipeline.Services.Zip"
		
		local zipFilename = (RsConfigGetExportDir() + "models/cdimages/weapons.zip")		
		local outputFolder = (RsConfigGetStreamDir() + "weapons/" + gRsBranch.name + "/")
		
		-- Extract the base weapons.zip file
		return (zipClass.ExtractAll zipFilename outputFolder true)
	)
 
)

-- Create instances of our script generator struct.
global RsWeaponScriptGenerator = RsWeaponScriptGeneratorStruct()
global RsModelScriptGenerator = RsModelScriptGeneratorStruct()

-- pipeline/export/models/ruby_script_generator.ms
