-- enum tranlations
-- author Gunnar Droege
-- enum from X:\gta5\src\dev\rage\framework\tools\src\Libs\RSG.SceneXml.MapExport\Definition.cs
--         public enum #((getattrindex "Gta Object" OBJectFlags
--         {
OBJectFlags = #(
            #((getattrindex "Gta Object" "Light reflection"), 0),
			#((getattrindex "Gta Object" "Do not fade"), 1),
			#((getattrindex "Gta Object" "Draw last"), 2),
			#((getattrindex "Gta Object" "Instanced"), 3),
			--// #((getattrindex "Gta Object" OBJ_IS_SUBWAY			), 4)), // Don't reuse
			#((getattrindex "Gta Object" "Is Fixed"					), 5),
			#((getattrindex "Gta Object" "No Zbuffer write"	), 6),
			#((getattrindex "Gta Object" "Tough for Bullets"	), 7),
			--// #((getattrindex "Gta Object" OBJ_IS_GENERIC			), 8)), // Don't reuse
			#((getattrindex "Gta Object" "Has Anim"							), 9),
			#((getattrindex "Gta Object" "Has UvAnim"						), 10),
			#((getattrindex "Gta Object" "Shadow Only"						), 11),
			--// #((getattrindex "Gta Object" OBJ_IS_DAMAGEABLE				), 12 // Not attribute), implied by prescence of @name_dam model.
			#((getattrindex "Gta Object" "Dont cast shadows"			), 13),
			#((getattrindex "Gta Object" "Cast texture shadows"		), 14),
			#((getattrindex "Gta Object" "Dont Collide With Flyer"	), 15),
			--// #((getattrindex "Gta Object" OBJ_EXPLODESWHENSHOT		), 16)), // Don't reuse
			#((getattrindex "Gta Object" "Is Dynamic"				), 17),
			--// #((getattrindex "Gta Object" OBJ_ISCRANE				), 18 ),// Don't reuse
			--// #((getattrindex "Gta Object" OBJ_DRAWWETONLY			), 19 ),// Don't reuse
			--// #((getattrindex "Gta Object" OBJ_ISTAGGABLE			), 20)), // Don't reuse
			#((getattrindex "Gta Object" "Backface Cull"			), 21),
			#((getattrindex "Gta Object" "Does Not Provide AI Cover"	), 22),
			--// #((getattrindex "Gta Object" OBJ_DOES_NOT_PROVIDE_COVER	), 23)), // Not attribute), implied by #((getattrindex "Gta Object" OBJ_DOES_NOT_PROVIDE_AI_COVER), #((getattrindex "Gta Object" OBJ_REVERSE_COVER_FOR_PLAYER
			#((getattrindex "Gta Object" "Is Ladder"				), 24),
			#((getattrindex "Gta Object" "Is Fire Escape"			), 25),
			#((getattrindex "Gta Object" "Has Door Physics"		), 26),
			#((getattrindex "Gta Object" "Is Fixed For Navigation"	), 27),
			#((getattrindex "Gta Object" "Don't Avoid By Peds"		), 28),
			#((getattrindex "Gta Object" "Enable Ambient Multiplier"	), 29),
            #((getattrindex "Gta Object" "Is Debug"                ), 30)
		)
 --       };
 
		
fn getObjectFlags obj = 
(
	local flags = 0
	for flag in OBJectFlags do
	(
		if getattr obj flag[1] then flags = bit.or flags flag[2]
	)
	return flags
)