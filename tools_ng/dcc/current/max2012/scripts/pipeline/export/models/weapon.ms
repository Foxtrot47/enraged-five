-- Rockstar Weapon Export
-- Rockstar North
-- 16/5/2005
-- by Greg Smith
-- Exports a weapon into the game


-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/models/ruby_script_generator.ms"
filein "pipeline/export/weapons/validation.ms"
filein "pipeline/export/weapons/utils.ms"
filein "rockstar/util/attribute.ms"
filein "pipeline/util/cloth.ms"
filein "rockstar/util/rexreport.ms"
filein "rockstar/util/collutil.ms"
filein "pipeline/util/texturemetatag_utils.ms"
filein "pipeline/util/p4_utils.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
isFragment = false
RsIsWeaponLODScene = false
idxFragment = getattrindex "Gta Object" "Is Fragment"
idxUserDefinedPal = getattrindex "Gta Object" "User Defined Palette"
idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsWeaponExportExtraTextures 
-- desc:
--
fn RsWeaponExportExtraTextures weapDir = (

	for texfile in RsTextureList do (

		ddsfile = weapDir + (RsRemovePathAndExtension texfile) + ".dds"

		RsRexExportTexture texfile ddsfile
	)		
)


--
-- name: RsWeaponExportMeshMap
-- desc: Export a file that maps between mesh name and mesh index
-- note: Should only be called after a weapon export
--
fn RsWeaponExportMeshMap weaponNode =
(
	local entityFile = RsConfigGetStreamDir() + "weapons/" + gRsBranch.name + "/" + weaponNode.name + "/entity.type"
	local cmdline = "x:/tools/util/GenerateLODInfo.py " + entityFile
	format "Command line: %\n" cmdline
	if doscommand cmdline != 0 then (
		gRsULog.LogError "Failed to generate LOD info." context:weaponNode
		return false
	)
)


--
-- name: RsWeaponExportModel
-- desc: Export the specified weapon model using rexMax.
--
fn RsWeaponExportModel objList weaponMeshName fragList changelistNumber overwriteTCS:false LODDir:"" LODsExport:false HDExport:false = 
(
	if not isKindOf objList Array do 
	(
		objList = #(objList)
	)
	
	local weaponFile = (RsConfigGetStreamDir() + "weapons/" + gRsBranch.name + "/" + weaponMeshName)
	-- Other weapon parts (not the main HD model or LODs for it) don't have a hi/low lod folder structure
	local weaponDir = if (LODDir != "") then (weaponFile + "/" + LODDir + "/") else (weaponFile + "/")
	local weaponParentDir = (weaponFile + "/")
	idxWeldWithoutQuant = GetAttrIndex "Gta Object" "Weld Without Quantise"
	idxWeld = GetAttrIndex "Gta Object" "Weld Mesh"
	
	-- Only delete when doing HD weapon model export, or export of mags etc as they go in a different
	-- export location.  LODs exported after the weapon HD model, so don't want to delete what's already
	-- been exported in that case.
	if ( not LODsExport ) do
	(
		-- Ensure we don't delete the ICD.
		RsDeleteFiles (weaponFile + ".ift*")
		RsDeleteFiles (weaponFile + ".itd*")
		RsDeleteFiles (weaponFile + ".idr*")
		RsDeleteFiles (weaponFile + "_hi.idr*")
		RsDeleteDirectory weaponFile
	)

	rexReset()
	local graphRoot = rexGetRoot()
	local origSel = selection as array
	
	-------------------------------------------------------------------------
	-- Tint Palette
	-------------------------------------------------------------------------
	--Check for tint palettes and add to list to pass along for mesh export later
	local hasPalette = false
	local usedTintMaterials = #()
	local reusedTintMaterials = #()
	
	-- On the LOD pass this might have multiple objects to check (rare), in the first HD pass it will just be
	-- the one.  
	for objIdx = 1 to objList.count do
	(
		obj = objList[objIdx]		
		
		if getattr obj idxUserDefinedPal == false then (
			local paletteFilename = RsConfigGetMapsTintTexDir() + obj.name + "_pal.dds"
			-- Used for files that need adding, or newly created that need adding
			local fileInP4 = false

			-- Only bother checking if it exists in perforce if file exists, as this would be done for
			-- all objects otherwise.
			if ( RsFileExists paletteFilename ) do
			(
				-- Returns array with true/false if it's in P4, only going to be 1 element in array
				fileInP4 = (gRsPerforce.exists #(paletteFilename) silent:true)[1]
				if ( fileInP4 ) then
				(
					gRsPerforce.edit paletteFilename silent:true
				)
				else
				(
					setFileAttribute paletteFilename #readOnly false						
				)
				deletefile paletteFilename
			)

			if RsGenerateAndAssignTexturePalette obj paletteFilename usedTintMaterials reusedTintMaterials == true then (
				if ( not fileInP4 ) do 
				(
					gRsPerforce.add paletteFilename silent:true
				)
				gRsPerforce.addToChangelist changelistNumber paletteFilename	
				hasPalette = true
			)
			else
			(
				gRsPerforce.run "revert" #( paletteFilename ) silent:true
			)
		)
		-- User defined palette
		else if getattr obj idxUserDefinedPal == true do
		(
			hasPalette = true
		)	
	)	

	-------------------------------------------------------------------------
	-- Export Geometry
	-------------------------------------------------------------------------

 	entity = rexAddChild graphRoot "entity" "Entity"
	print entity
	print graphRoot
 	rexSetPropertyValue entity "OutputPath" weaponDir
 	rexSetPropertyValue entity "EntityFragmentWriteMeshes" "true"
	
	if HDExport do
	(
		local lodGroupCount = 0 -- added due to changes to RsLodDrawable_SetUserProps
		-- RAGE Drawable LOD Support
		local rootDepth = 0
		local selobjs = RsLodDrawable_SetUserProps objList[1] &lodGroupCount depth:&rootDepth
		selectMore selobjs
	)

	mesh = rexAddChild entity "mesh" "Mesh"
	rexSetPropertyValue mesh "OutputPath" weaponDir
	rexSetPropertyValue mesh "ExportTextures" "false"
	rexSetPropertyValue mesh "TextureOutputPath" weaponDir
	rexSetPropertyValue mesh "MeshSkinOffset" "true"
	rexSetPropertyValue mesh "MeshGenerateTintIndices" (hasPalette as string)
	rexSetPropertyValue mesh "MeshWeldWithoutQuantize" ((GetAttr objList[1] idxWeldWithoutQuant) as string)
	rexSetPropertyValue mesh "MeshWeld" ((GetAttr objList[1] idxWeld) as string)
	
	select objList	
	rexSetNodes mesh

	skel = rexAddChild entity "skeleton" "Skeleton"
	rexSetPropertyValue skel "OutputPath" weaponDir
	rexSetPropertyValue skel "SkeletonUseBoneIDs" "true"
	rexSetPropertyValue skel "SkeletonRootAtOrigin" "true"
	rexSetPropertyValue skel "ChannelsToExport" "(transX;transY;transZ;)(rotX;rotY;rotZ;)"
	rexSetPropertyValue skel "SkeletonWriteLimitAndLockInfo" "true"
	rexSetNodes skel
	
	-- LODs won't be fragments
	if (not LODsExport and (getattr objlist[1] idxFragment)) then
	(
		RsSetupFragBounds objlist[1] entity weaponDir		
		fragment = rexAddChild entity "fragment" "Fragment"
		rexSetPropertyValue fragment "OutputPath" weaponDir
		rexSetNodes fragment
		isFragment = true
	)
	
	-- LODs are the only things done in a group together with multiple entries in objList (if more than
	-- one LOD) - they should never be fragments anyway.  Just one entry in fragList as it is used in resourcing to match the number
	-- of the weaponExportItem structs we're exporting (all LODs in one struct).
	append fragList isFragment

	-------------------------------------------------------------------------
	-- Export Textures
	-------------------------------------------------------------------------	
	tcsDir = GetWeaponTCSDir weaponMeshName
	WeaponsTCSFilePerforceSyncAndCheckout tcsDir overwriteTCS:overwriteTCS

	objtexmaplist = #()
	objmaxsizelist = #()
	objisbumplist = #()
	objtexmapobjlist = #()
	objmaptypelist = #()
	objtexturetemplatelist = #()

	RsSetMapsFlags true diff:true spec:true normal:true
	print ("objList:"+objList as string)
	for weaponMesh in objList do
	(
		RsGetTexMapsFromObjNoStrip weaponMesh objtexmaplist objmaxsizelist objisbumplist texmapobjlist:objtexmapobjlist maptypelist:objmaptypelist texturetemplatelist:objtexturetemplatelist
	)
	RsSetMapsFlags true diff:true spec:true normal:true
	
	-- AJM: This is bloody nasty but putting this in here rather than fiddling with the resourcing script as 
	-- each zip block is used by multiple things.  For the HD weapon and LODs, the textures all get plonked
	-- into the same folder as they end up in one txd.
	gRsUlog.LogMessage ("weaponDir:"+weaponDir as string)
	gRsUlog.LogMessage ("objtexmaplist:"+objtexmaplist as string)
	if (RsWeaponExportTextures objtexmaplist objisbumplist weaponDir weaponMeshName changelistNumber texmapobjlist:objtexmapobjlist \
						maptypelist:objmaptypelist overwriteTCS:overwriteTCS texturetemplatelist:objtexturetemplatelist) == false then return false

	-------------------------------------------------------------------------
	-- Export Bounds
	-------------------------------------------------------------------------
	local collObjs = #()
	if (not LODsExport) then
	(
		collObjs = for childobj in objList[1].children where (getattrclass childobj == "Gta Collision") collect childobj
		if ( collObjs.count > 0 ) then 
		(
			bound = rexAddChild entity weaponMeshName "Bound"
			rexSetPropertyValue bound "OutputPath" weaponParentDir
			rexSetPropertyValue bound "ForceExportAsComposite" "false"
			rexSetPropertyValue bound "ExportAsComposite" "true"
			rexSetPropertyValue bound "BoundExportWorldSpace" "false"
			rexSetPropertyValue bound "BoundZeroPrimitiveCentroids" "true"
			rexSetPropertyValue bound "BoundForceComposite" "true"

			surfaceProperty = ""
			for selobj in collObjs do (

				surfaceType = RsGetCollSurfaceTypeString selobj

				surfaceProperty = surfaceProperty + selobj.name + "="
				surfaceProperty = surfaceProperty + surfaceType + ":"
			)
			rexSetPropertyValue bound "SurfaceType" surfaceProperty
			select collObjs 

			rexSetNodes bound
		)
	)

	select origSel

	rexExport false (gRsUlog.filename())
	if not gRsUlog.Validate() then
		return false
	
	RsWeaponExportExtraTextures weaponDir

	return true
)


--
-- name: RsWeaponExtractZIP
-- desc: Extract the weapon ZIP into the stream.
--
fn RsWeaponExtractZIP =
(
	local rbFileName = RsConfigGetScriptDir() + "weapons/weapon_zip_extract.rb"
	RsMakeSurePathExists rbFileName
	
	if ( true != ( RsWeaponScriptGenerator.ExtractZIP rbFileName ) ) then
	(
		gRsULog.LogError "Failed to extract the weapons ZIP."
		return false
	)
	
	true
)

--
-- name: RsWeaponExportBuildZIP
-- desc: Builds the weapon ZIP.
--
fn RsWeaponExportBuildZIP = (

	if ( true != ( RsWeaponScriptGenerator.BuildZIP() ) ) then
	(
		gRsULog.LogError "Failed to build weapons ZIP."
		return false
	)
	true
)

--
-- func: RsWeaponExport 
-- desc: 
--
fn RsWeaponExport isPatch exportMeshMap objSet overwriteTCS:false = 
(
	local currSel = selection as array
	
	-- Warn if scene contains map-export containers:
	local mapnames = #()
	local mapConts = RsMapGetMapContainers()
	
	for item in mapConts where (item.name != undefined) do 
	(
		local mapName = toLower item.name
		
		if ((RsProjectContentFind mapName type:"mapzip") != undefined) do 
		(
			append mapnames mapName
		)
	)
	
	if (mapnames.count != 0) then
	(
		local warnMsg = stringStream ""
		format "Are you sure you want to do a weapon export?\nScene contains map%:\n" (if mapnames.count == 1 then "" else "s") to:warnMsg
		for mapName in mapnames do (format "%\n" mapName to:warnMsg)
		
		if not (queryBox (warnMsg as string) title:"Warning: Maps found") do (return false)
	)
	else if mapConts.count>1 then
	(
		gRsUlog.LogError "Multiple map containers in a weapon scene!"
		return gRsUlog.Validate()
	)
	
	gRsUlog.init "Weapon export" appendToFile:false

	RsP4LogUserLabelStatus()

	-- Don't collect lod objects here, they're found later.
	local exportList = for obj in objSet where ("Gta Object" == GetAttrClass obj ) and not ( GetAttr obj idxDontExport ) \
													and not (matchPattern obj.name pattern:"*_L?") collect 
	(
		if (obj.parent == undefined) then obj else 
		(
			format "Object % has a parent.  Invalid for weapon export.\n" obj.name
			dontCollect
		)
	)	
	if ( 0 == exportList.count ) then (
		gRsULog.LogWarning  "No exportable objects found"
		if not gRsUlog.Validate() then 
			return false
	)

	local contentNode = RsContentTree.GetContentNode (maxFilePath + maxFileName)
	local zipNode = RsContentTree.GetMapExportZipNode contentNode
	local weaponsZip = (RsConfigGetExportDir() + "models/cdimages/weapons.zip")
	format "Buildfile: %\n" weaponsZip
	
	-- RsQueryBoxTimeOut uses the map-export cancel-global value:
	RsMapExportCancelled = false
	local isNewExport = false
	
	if not doesFileExist weaponsZip then
	(
		if RsProjectGetPerforceIntegration() then
		(
			local depotFile = gRsPerforce.local2depot weaponsZip
			if (gRsPerforce.sync depotFile force:true) then
				gRsPerforce.edit depotFile
			else
				isNewExport = true
		)
	)
	else if (not isPatch) and (RsIsFileReadOnly weaponsZip) do 
	(
		local checkOutFiles = true
		local makeWritable = false
		
		local P4active = RsProjectGetPerforceIntegration()
		
		if P4active do 
		(
			-- Yes/No buttons will represent "Checkout" and "Make Writable" options:
			local msg = "Weapons build-file is readonly:\n" + weaponsZip + "\n\nDo you want to check it out of Perforce, make the files locally writable, or cancel export?\n"
			checkOutFiles = RsQueryBoxTimeOut msg title:"Build-file is readonly" timeout:-1 cancel:true yesText:"Checkout" noText:"Make Writable"
			makeWritable = not checkOutFiles
		)

		if not RsMapExportCancelled do 
		(
			if checkOutFiles do 
			(
				local success = P4active and (::gRsPerforce.edit #(weaponsZip))
				
				if not success do 
				(
					local title = if P4active then 
					(
						title = "Build-file Perforce-checkout errors"
					)
					else 
					(
						title = "Build-file is readonly"
					)
					
					makeWritable = RsQueryBoxTimeOut "Do you want to make weapons.zip locally writable, or cancel export?" title:title timeout:-1 yesText:"Make Writable" noText:"Cancel"
					
					if not makeWritable do (RsMapExportCancelled = true)
				)
			)
			
			if makeWritable do 
			(
				setFileAttribute weaponsZip #readOnly false
			)
		)
	)
	
	if RsMapExportCancelled do return false
	
	if isPatch == false then (
		local weaponName = getFilenameFile maxFileName
		RsWeaponSceneXmlExport weaponName
	)
	
	local continueExport = true
	
	changelistNumber = gRsPerforce.createChangelist "Weapon data"
	gRsPerforce.sync (RsConfigGetMapsTintTexDir() + "...") silent:true	
	local allWeaponHDorLODNodes = #()
	local weaponExportItems = #()
	
	struct weaponExportItem (objList = #(), weaponMeshName = "", LODFolder = "", isLODs = false, isHD = false)
	
	-- weaponMeshPart is any of the HD objects - HD weapon part, or in the case of an attachments file, any HD piece
	for weaponMeshPart in exportList do
	(		
		RsWeaponExportIsValidForExport weaponMeshPart
		continueExport = gRsUlog.Validate()

		RsIsWeaponLODScene = false
		weaponLODNodes = #()

		-- Check if there are LOD objects for the weapon.  Shitey naming convention for now.
		weaponLOD1 = getnodebyname (weaponMeshPart.name + "_L1")
		weaponLOD2 = getnodebyname (weaponMeshPart.name + "_L2")
		weaponLOD3 = getnodebyname (weaponMeshPart.name + "_L3")

		if ( weaponLOD1 != undefined ) do ( setuserprop weaponLOD1 "lod" "0" ; append weaponLODNodes weaponLOD1)
		if ( weaponLOD2 != undefined ) do ( setuserprop weaponLOD2 "lod" "1" ; append weaponLODNodes weaponLOD2)
		if ( weaponLOD3 != undefined ) do ( setuserprop weaponLOD3 "lod" "2" ; append weaponLODNodes weaponLOD3)
		
		local HDLODFolder = ""
		if ( weaponLODNodes.count > 0 ) do 
		( 
			RsIsWeaponLODScene = true 
			setuserprop weaponMeshPart "lod" "0"			
			
			-- HD weapon export item
			append weaponExportItems (weaponExportItem objList:#(weaponMeshPart) weaponMeshName:weaponMeshPart.name LODFolder:"hi" isLODs:false isHD:true)
		
			-- We export all the weapon LODs at the same time in one execution of the RsWeaponExportModel function	
			append weaponExportItems (weaponExportItem objList:weaponLODNodes weaponMeshName:weaponMeshPart.name LODFolder:"low" isLODs:true isHD:false) 	
		
			-- Add to list of all weapon LOD Nodes to get used later to collect other objects
			join allWeaponHDorLODNodes weaponMeshPart
			join allWeaponHDorLODNodes weaponLODNodes
		)
	)
	
	if continueExport then
	(
		-- AJM: Find other export items e.g. weapon mags (no LOD support for these as yet - unsure if there will be)
		for obj in $objects where (("Gta Object" == GetAttrClass obj ) and not ( GetAttr obj idxDontExport )) do 
		(
			-- Find other items marked for export that also aren't the HD weapon or LOD meshes as we've already
			-- collected them above. E.g weapon mags that may have been picked up previously but there was no LOD
			-- object, so was skipped, until here.
			if ((obj.parent == undefined) and (finditem allWeaponHDorLODNodes obj == 0)) then
			(		
				format "Extra mesh piece to export: %\n" obj.name
				-- No LOD support for weapon mags etc, currently.
				append weaponExportItems (weaponExportItem objList:#(obj) weaponMeshName:obj.name LODFolder:"" isLODs:false isHD:false)
			)
		)

		local isFragList = #()
		-- Delete bounds file at start
		-- We use the branch so that we don't end up having titleupdate weapons in with NG weapons etc
		RsDeleteFiles (RsConfigGetStreamDir() + "weapons/" + gRsBranch.name + "/weapons.ibd*")
		
		for expStruct in weaponExportItems do
		(
			if continueExport do 
			(
				-- global isFragment gets set in RsWeaponExportModel, if it is  frag.  Reset here in the loop
				isFragment = false
				continueExport = RsWeaponExportModel expStruct.objList expStruct.weaponMeshName isFragList changelistNumber \
					overwriteTCS:overwriteTCS LODDir:expStruct.LODFolder LODsExport:expStruct.isLODs HDExport:expStruct.isHD
				if continueExport then
					continueExport = gRsUlog.Validate()
			)
		)	
	)

	if continueExport and exportMeshMap then
	(
		RsWeaponExportMeshMap weaponMeshPart
		continueExport = gRsUlog.Validate()
	)

	-- Restore selection:
	select currSel
	
	if not continueExport do return false
	
	if not (RsWeaponScriptGenerator.BuildWeaponResources weaponExportItems isPatch:isPatch isFragList:isFragList) then
		gRsULog.LogError "Failed to build weapon resources."
	if not gRsUlog.Validate() then 
		return false

	configExportView = dotnetobject "RSG.Base.ConfigParser.ConfigExportView"
	if( configExportView.GetMaxAutoSubmitsEnabled() ) then
	(
		gRsPerforce.submitOrDeleteChangelist changelistNumber
	)
	else
	(
		gRsPerforce.deleteChangelistIfItHasNoChanges changelistNumber
	)

	if not gRsUlog.Validate() then 
		return false
	if isPatch == false then (
		RsWeaponExportBuildZip()
		if isNewExport then
			gRsPerforce.add weaponsZip
		
		if not gRsUlog.Validate() then 
			return false
	)
)


-- weapon.ms
