--
-- File:: pipeline/export/AP3Content.ms
-- Desc:: Asset Pipeline 3 content-tree methods.
--
-- Author:: Terry Litrenta
-- Date:: 14 January 2013
--


-- Set of our currently loaded projects (ex. DLC)
global gRsProject
global gRsBranch
global gRsConfig

-- Default set of projects in case we need paths from (ex. GTA5)
global gRsCoreProject
global gRsCoreBranch
global gRsCoreConfig

-- global helper and data for content tree
global RsContentTree

global gRsBugstarConfig

struct AP3Content
(
	contentTree = undefined,
	contentFactory = ( dotNetClass "RSG.Pipeline.Content.Factory" ),
	configFactory = ( dotNetClass "RSG.Base.Configuration.ConfigFactory" ),
	searchClass = (dotNetClass "RSG.Pipeline.Content.Algorithm.Search"),
	contentTreeHelper = undefined,
	loadedContent = false,
	
	fn InitContentTreeHelper =
	(
		if (contentTreeHelper != undefined) then
			return true
		
		contentTreeHelper = dotNetObject "RSG.Pipeline.Content.ContentTreeHelper" contentTree
		
		if (contentTreeHelper == undefined) then
		(
			format "Could not initialize Content Tree Helper."
			return false
		)
		
		return true
	),
	
	fn IsContentTreeLoaded =
	(
		return loadedContent
	),
	
	fn UnloadContentTree =
	(
		loadedContent = false
		contentTree = undefined
		contentTreeHelper = undefined
			
		gc light:true
		(dotnetclass "system.gc").collect()
	),
	
	fn GetDLCProject maxFile =
	(
		if undefined==gRsConfig then
			gRsConfig = configFactory.CreateConfig()
		
		local dlcProjectEnum = gRsConfig.DLCProjects.GetEnumerator()

		maxFile = RsMakeSafeSlashes maxFile
		while dlcProjectEnum.MoveNext() do
		(
			local dlcRoot = RsMakeSafeSlashes dlcProjectEnum.Current.Value.Root
			if (matchPattern maxFile pattern:(dlcRoot+"/*")) then 
			(
				return dlcProjectEnum.Current.Value
			)
		)
		return undefined
	),
	
	fn DoesDLCProjectExist maxFile =
	(
		dlcProject = GetDLCProject maxFile

		(dlcProject != undefined)
	),
	
	fn DoDLCChecks = 
	(
		if not doesFileExist "x:/gta5_dlc" then
			gRsUlog.LogWarning ("DLC root path \"x:/gta5_dlc\" does not exist locally. Wrong perforce path mapping?")
		if gRsConfig.DLCProjects.count<=0 then
			gRsUlog.LogWarning ("No DLC projects loaded. Please check path mapping and config files' existence.")
	),

	fn ResetProjectAndBranchGlobals =
	(
		gRsConfig = undefined
		gRsCoreConfig = undefined -- this is bollocks.
		gRsProject = undefined
		gRsCoreProject = undefined
		gRsBranch = undefined
		gRsCoreBranch = undefined
		OK
	),
	
	fn SetupProjectAndBranchGlobals dlcProjectName:undefined dlcMaxFile:undefined requestedBranchName:"" =
	(
		local previousBranch = gRsBranch
		local previousProject = gRsProject
		
		try
		(
			if ( undefined == gRsConfig ) do gRsConfig = configFactory.CreateConfig()
			gRsCoreConfig = gRsConfig -- gRsCoreConfig is bollocks
		)
		catch	
		(
			-- For ConfigurationVersionException
			messageBox "An important update has been made to the tool chain.\n\nSync head or labelled tools and run the installer.\n\nClicking OK will close Autodesk 3dsmax." title:"Tools Version Updated"
			quitMax #noPrompt
		)	
		
		gRsProject = gRsConfig.Project		
		gRsCoreProject = gRsConfig.Project
		gRsBugstarConfig = RsContentTree.configFactory.CreateBugstarConfig(gRsConfig.project)
		
		if (dlcProjectName != undefined and dlcProjectName != "") then
		(			
			dlcProjects = gRsConfig.DLCProjects
			if (dlcProjects.ContainsKey dlcProjectName) then
			(
				gRsUlog.LogWarning ("DLC Project Found: " +  (dlcProjectName as string))
				gRsProject = dlcProjects.Item[dlcProjectName]
			)
			else
			(
				gRsUlog.LogWarning ("No such DLC project named " + dlcProjectName + ". Defaulting to current project branch") 
			)
		)
		
		if (dlcMaxFile != undefined and dlcMaxFile != "") then
		(
			DoDLCChecks()

			local dlcProject = GetDLCProject dlcMaxFile
			if ( dlcProject != undefined ) then
			(
				gRsProject = dlcProject
			)
			else 
			(
				gRsUlog.LogWarning ("No such DLC project matching maxfile " + dlcMaxFile + ". Defaulting to current project branch")
			)
		)
		
		gRsBranch = gRsProject.DefaultBranch
		gRsCoreBranch = gRsCoreProject.DefaultBranch
		
		-- If we previously had a different branch selected, see if this project has the same branch name
		if (previousBranch != undefined) do
		(
			if (gRsProject.Branches.ContainsKey previousBranch.Name) do
			(
				gRsBranch = gRsProject.Branches.Item[previousBranch.Name]
				format "Setting to previous branch: %\n" gRsBranch.name
			)
		)
		
		if (requestedBranchName != "" ) then 
		(
			if (gRsProject.Branches.ContainsKey requestedBranchName) then
			(
				gRsBranch = gRsProject.Branches.Item[requestedBranchName]
				format "Setting to requested branch: %\n" gRsBranch.name
			)
			else
			(
				gRsUlog.LogWarning ("Project: " + gRsProject.name + " does not contain a branch called '" + requestedBranchName + "' which was requested.")
			)
		) 

		-- match core BRANCH on current branch
		if ( gRsCoreProject.Branches.ContainsKey ( gRsBranch.Name ) ) then
		(
			gRsCoreBranch = gRsCoreProject.Branches.Item[ gRsBranch.Name ]
		)
        
		::DisplayAll()
		-- Update the UI
		if (RsSettingsRoll != undefined) do ::RsSettingsRoll.Refresh()	
		
		-- Update Max title-bar to show current project/branch:
		if (::RsSetMaxTitle != undefined) do 
		(
			RsSetMaxTitle()
		)

		OK
	),
	
	fn LoadContentTree forceReload:false =
	(		
		if (loadedContent and not forceReload) then
		(
			format "Content tree already loaded, not reloading unless forceReload is called..."
			return true
		)
		else if (loadedContent and forceReload) then
		(
			-- If we are forcing a reload then clean up previous data otherwise we compound the memory usage.
			format "Unloading content tree, forceReload was called...\n"
			UnloadContentTree()
		)
		 
		pushPrompt "Loading AP3 Content Tree..."
		try
		(
			contentTree = contentFactory.CreateTree gRsBranch
		)catch
		(
			gRsUlog.LogToolError ("Don't export this asset. The content tree contained errors on loading (see trailing). Contact tools with the universal log. "+(getCurrentException()))
		)
		popPrompt()
		
		if contentTree == undefined then
		(
			MessageBox "Problem loading content tree.  Please contact tools.\n"
			return false
		)
		
		loadedContent = true		
		InitContentTreeHelper()
		
		::InitLinkManager()
		
		true
	),
	
	fn SetupGlobalsFromMapsAndLoadContentTree maps forceReload:false requestedBranchName:""=
	(
		local dlcReferenceContainer = undefined
		local dlcProject = undefined
		local coreContainerLoaded = false 
		local dlcProjectName = undefined

		for map in maps do
		(
			dlcReferenceContainer = map.filename
			if (DoesDLCProjectExist map.filename) then
			(
				projectName = GetDLCProject map.filename
				dlcProjectName = projectName.name
				--look whether project array is not empty and contains different projects:
				if (undefined!=dlcProject and dlcProject!=projectName) then
				(
					gRsULog.LogError "Trying to export containers from different DLC packs. Can only export containers from the same DLC project."
					return false
				)
				dlcProject = projectName
			)
			else
			(
				coreContainerLoaded = true
			)
		)

		-- Check to see if we are trying to export DLC containers and core game containers at the same time.
		if (dlcProject!= undefined and coreContainerLoaded) then
		(
			gRsULog.LogError "Trying to export a core game container and a DLC container."
			return false
		)
	
		-- Check to see if we are trying to export containers from different DLC projects.
		SetupProjectAndBranchGlobals dlcProjectName:dlcProjectName dlcMaxFile:dlcReferenceContainer requestedBranchName:requestedBranchName
		LoadContentTree forceReload:forceReload
	),
	
	-- 
	-- name: GetContentNode
	-- desc: Retrieves the AP3 Content Node for the given .max/maxc file
	--
	fn GetContentNode inputFile =
	(
		if (inputfile == undefined or inputfile == "") do
		(
			gRsULog.LogError "inputFile is undefined. Content tree can not create a file with an empty file name."
			return undefined
		)
	
		if (contentTree == undefined) do
		(
			format "Content tree not loaded. Call LoadContentTree first."
			gRsULog.LogError "Content tree not loaded. Call LoadContentTree first."
			return undefined
		)
		
		return contentTree.CreateFile inputFile
	),
	
	-- 
	-- name: GetMapType
	-- desc: Returns the map type of the given content node (you can obtain a content node by calling GetContentNode on a valid .xml file
	--
	fn GetMapType contentNode =
	(
		local paramMap_mapType = "max_type"
		
		local mapType = "unknown"
		if (contentNode != undefined) then
		(
			mapType = contentNode.GetParameter paramMap_mapType "unknown"
		)

		mapType
	),
	
	fn GetExportArchetypes contentNode =
	(
		local paramMap_exportArchetypes = "archetypes"
		
		local exportArchetypes = false
		if (contentNode != undefined) then
		(
			exportArchetypes = contentNode.GetParameter paramMap_exportArchetypes false
		)
		
		exportArchetypes
	),
	
	fn GetExportEntities contentNode = 
	(
		local paramMap_exportEntities = "entities"
		
		local exportEntities = false
		if (contentNode != undefined) then
		(
			exportEntities = contentNode.GetParameter paramMap_exportEntities false
		)
		
		exportEntities
	),
	
	fn GetExportPrefix contentNode =
	(
		local paramMap_exportPrefix = "prefix"
		local exportPrefix = ""
		
		if (contentNode != undefined) then
		(
			exportPrefix = (contentNode.GetParameter paramMap_exportPrefix "" ) as string
			
			-- if the key exist, but with no data, the parameter's value is "null", which means undefined in maxscript
			-- to avoid undefined related errors for the prefix, override of that case is done here.
			if(exportPrefix == "undefined") then
				exportPrefix = ""
		)

		exportPrefix
	),
	
	-- 
	-- name: GetMapExportZipNode
	-- desc: Returns the .zip associated with the given content node
	--
	fn GetMapExportZipNode contentNode =
	(
		if (contentNode == undefined) then
			return undefined
		
		InitContentTreeHelper()
		return contentTreeHelper.GetExportZipNodeFromMaxFileNode contentNode
	),
	
	fn GetMapExportSceneXMLNode contentNode =
	(
		if (contentNode == undefined) then
			return undefined
		
		InitContentTreeHelper()
		return contentTreeHelper.GetSceneXmlNodeFromExportZipNode contentNode
	),
	
	fn GetMapProcessedNode contentNode =
	(
		if (contentNode == undefined) then
			return undefined
		
		InitContentTreeHelper()
		return contentTreeHelper.GetProcessedZipNodeFromExportZipNode contentNode
	),
	
	fn FindOutputFiles inputFile =
	(
		if (not IsContentTreeLoaded()) do LoadContentTree()
	
		contentNode = contentTree.CreateFile inputFile
		processes = searchClass.FindProcessesWithInput contentTree contentNode
		
		if processes == undefined then
		(
			format "Could not find (%) in the content tree...\n" inputFile
			return #()
		)
		
		outputFiles = #()

		iter = processes.getEnumerator()
		
		while iter.MoveNext() do
		(
			local outputs = iter.Current.Outputs
			
			outputIter = outputs.getEnumerator()
			while outputIter.MoveNext() do
			(
				append outputFiles outputIter.Current.AbsolutePath
			)
		)
		
		return outputFiles
	),
	
	fn FindOutputFilesFromDirectory inputDirectory =
	(
		if (not IsContentTreeLoaded()) do LoadContentTree()
	
		contentNode = contentTree.CreateDirectory inputDirectory
		processes = searchClass.FindProcessesWithInput contentTree contentNode
		
		if processes == undefined then
		(
			format "Could not find (%) in the content tree...\n" inputFile
			return #()
		)
		
		outputFiles = #()

		iter = processes.getEnumerator()
		
		while iter.MoveNext() do
		(
			local outputs = iter.Current.Outputs
			
			outputIter = outputs.getEnumerator()
			while outputIter.MoveNext() do
			(
				append outputFiles outputIter.Current.AbsolutePath
			)
		)
		
		return outputFiles
	),
	
	-- Return list of content-nodes that have the corresponding basename.
	fn FindFilesWithBasename basename =
	(	
	),
	
	-- Finds the source-maxc/max content-node for a given map-name:
	fn FindSourceFilenodeFromBasename BaseName = 
	(
		if (BaseName == undefined) do return undefined
		
		if (not IsContentTreeLoaded()) do LoadContentTree()
		
		local SourceNodes = RsContentTree.ContentTreeHelper.GetAllMapExportNodes()
		local SourceNode = undefined
		
		-- Find sourcefile-node for named map:
		for ThisNode in SourceNodes while (SourceNode == undefined) do 
		(
			if (matchPattern ThisNode.BaseName pattern:BaseName) do 
			(
				SourceNode = ThisNode
			)
		)
		
		return SourceNode
	),
	
	-- Finds the source maxc/max filename from the content tree incase all we have is
	-- the basename, e.g. the container helper in max and no source/local definition
	fn FindSourceFilenameFromBasename basename = 
	(
		local SourceNode = FindSourceFilenodeFromBasename basename
		
		if (SourceNode == undefined) do return undefined
		
		return (RsMakeSafeSlashes SourceNode.AbsolutePath)
	),



	fn SyncContentTree silent:false = 
	(
		if( gRsPerforce.connected() == false ) then gRsPerforce.connect()
		local cf = (dotNetClass "RSG.Pipeline.Content.Factory")
		local rVal = false
		local loop = true 
		
		if not silent then(
			if( 1 == (::RsQueryBoxMultiBtn "Do you want to sync the content tree?" title:"Sync Content Tree" timeout:30 labels:#("Yes","No") defaultBtn:2) ) then
			(
				loop = true
			)else(
				loop = false
			)
		)

		pushPrompt "Syncing Content Tree"

		while loop do (

			local dnFiles = cf.GetFilenames gRsBranch
			
			if(dnFiles.count > 0) then
			(
				local mxsFiles = #()				
				for i = 0 to dnFiles.count - 1 do 
				(
					append mxsFiles (dnFiles.Item i)
				)
			)

			local fmapping = gRsPerforce.getFileMapping mxsFiles
			local depotNames = for f in fmapping collect f.depotFilename

			local runArgs = #("-n")
			join runArgs depotNames
			
			-- Preview sync to find out what files will get sync'd
			local result = gRsPerforce.p4.run "sync" runArgs
			-- Do actual sync of the files.
			gRsPerforce.sync depotNames silent:silent
			oldDepFiles = for res in result.Records where res.Fields.ContainsKey("depotFile") collect
			(
				res.Item "depotFile"
			)

			if oldDepFiles.count == 0 then
			(
				loop = false
				rval = true
			)
		)
		popPrompt()
		rVal
	)

) -- struct AP3Content


-- Global AP3 content tree
if (RsContentTree == undefined) then 
	RsContentTree = AP3Content()

-- 
-- 
fn RsIsDLCProj =
(
	gRsCoreProject != gRsProject
)

--
-- name: RsValidateProject
-- desc: If we don't have a valid project set, then load the default project.
--
fn RsValidateProject = 
(
	if (gRsProject == undefined or 
		gRsBranch == undefined or 
		gRsConfig == undefined or 
		gRsCoreProject == undefined or 
		gRsCoreBranch == undefined or 
		gRsCoreConfig == undefined or
		gRsCoreBranch.Name != gRsBranch.Name ) then
	(
		RsContentTree.SetupProjectAndBranchGlobals()
		RsContentTree.LoadContentTree()
	)
)

-- helper function to see current state of project/branch for core and current selected project (mostly if not only for debug purpose)
fn DisplayAll =
(
	format "Core \n"
	format "\tProject : %  #[%]\n" gRsCoreProject.Name (gRsCoreProject.GetHashCode())
	format "\tBranch  : %  #[%]\n" gRsCoreBranch.Name (gRsCoreBranch.GetHashCode())

	format "Current \n"
	format "\tProject : %  #[%]\n" gRsProject.Name (gRsProject.GetHashCode())
	format "\tBranch  : %  #[%]\n" gRsBranch.Name (gRsBranch.GetHashCode())

)
