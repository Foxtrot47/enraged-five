try( DestroyDialog WaterAlphaPaintRoll ) catch()

filein "pipeline/util/WaterAlphaPainter.ms"

rollout WaterAlphaPaintRoll "Water Paint Tool configuration"
(
	spinner spnIntens "Intensity (ALT)" range:[0,1,1]
	spinner spnRadius "Radius (CTRL)" range:[0,500,10]
	radiobuttons radioPaintDel "paint/subtract (SHIFT)" labels:#("paint", "subtract")
	
	checkbutton btnPaint "Paint" width:150 height:30
	
	on spnIntens changed val do
	(
		if undefined!=gWaterPaintToolInstance then
			gWaterPaintToolInstance.itensity = val
	)
	on spnRadius changed val do
	(
		if undefined!=gWaterPaintToolInstance then
			gWaterPaintToolInstance.radius = val
	)
	
	on radioPaintDel changed val do
	(
		if undefined!=gWaterPaintToolInstance then
			gWaterPaintToolInstance.isAddtitive = val==1
	)
	
	on btnPaint changed val do 
	(
		if val then
		(
			startTool Water_Alpha_Painter
		)
		else 
			stopTool Water_Alpha_Painter
	)
)
CreateDialog WaterAlphaPaintRoll
