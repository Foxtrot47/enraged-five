--
-- File:: pipeline/ui/live_map_editing.ms
-- Description::
--
-- Author:: Jonny Rivers <jonathan.rivers@rockstarnorth.com>
-- Date:: 14 May 2012
--
try (destroyDialog RsLiveMapEditingRoll) catch ()

fileIn "pipeline/util/warp.ms"

global RsLiveEditPortSuffix = ":7890"

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------

rollout RsLiveMapEditingRoll "Live Map Editor (Max -> Game)" width:300 height:420
(
	-------------------------------------------------------------------------
	-- Locals
	-------------------------------------------------------------------------
	local nodesToUpdate = #()
	local liveMapEditingService
	
	local cameraUpdateRequired = false
	local idxGtaObjLODDist = getattrindex "Gta Object" "LOD distance"
	local idxGtaObjInstLODDist = getattrindex "Gta Object" "Instance LOD distance"
	local idxGtaObjChildLODDist = getattrindex "Gta Object" "Child LOD distance"
	local idxGtaObjUseFullMtx = getattrindex "Gta Object" "Use Full Matrix"
	local idxGtaObjDontRdrInShadow = getattrindex "Gta Object" "Dont Render In Shadows"
	local idxGtaObjOnlyRdrInShadow = getattrindex "Gta Object" "Only Render In Shadows"
	local idxGtaObjDontRdrInReflections = getattrindex "Gta Object" "Dont Render In Reflections"
	local idxGtaObjOnlyRdrInReflections = getattrindex "Gta Object" "Only Render In Reflections"
	local idxGtaObjDontRdrInWaterReflections = getattrindex "Gta Object" "Dont Render In Water Reflections"
	local idxGtaObjOnlyRdrInWaterReflections = getattrindex "Gta Object" "Only Render In Water Reflections"
	local idxGtaObjIPLGroup = getattrindex "Gta Object" "IPL Group"
	-------------------------------------------------------------------------
	-- UI Widgets
	-------------------------------------------------------------------------
	dotNetControl RsBannerPanel "Panel" pos:[0,0] height:32 width:RsLiveMapEditingRoll.width
	local bannerStruct = makeRsBanner dn_Panel:RsBannerPanel wiki:"Live Map Editing" filename:(getThisScriptFilename())
	
	checkbutton btnConnect "Connect" checked:false width:280
	checkbutton btnSendCameraUpdates "Send Camera Updates" enabled:false width:280
	checkbutton btnReceiveCameraUpdates "Receive Camera Updates" enabled:false width:280
	group "Interior"
		(
			edittext etMiloName "MILO" width:200 across:2
			checkBox chkActive "Active" offset:[80,0]
			
			label lblPositonOffset "Position Offset" align:#Left
			spinner spnXPosOffset "X" range:[-40000,40000,0] across:3 fieldWidth:60
			spinner spnYPosOffset "Y" range:[-40000,40000,0] fieldWidth:60
			spinner spnZPosOffset "Z" range:[-40000,40000,0] fieldWidth:60
			
			label lblRotationOffset "Rotation Offset" align:#Left
			spinner spnXRotOffset "X" range:[-40000,40000,0] across:3 fieldWidth:60
			spinner spnYRotOffset "Y" range:[-40000,40000,0] fieldWidth:60
			spinner spnZRotOffset "Z" range:[-40000,40000,0] fieldWidth:60
			
			button btnLoadMiloInfo "Load MILO Information" enabled:false
		)
	button btnAddEntity "Add selected objects to game" enabled:false width:280
	button btnForceUpdate "Force update of selected objects" enabled:false width:280
	button btnPickEntities "Pick selected in-game" enabled:false width:280
	button btnSettleProps "Settle selected dynamic props in-game" enabled:false width:280
	button btnWarpToSelection "Warp player to selection" enabled:true width:280
	edittext etStatus "Status:" text:"Not connected to the game" enabled:false width:280 readOnly:True offset:[-3,0]
	timer timerUpdate interval:33 active:false

		
	-------------------------------------------------------------------------
	-- Methods
	-------------------------------------------------------------------------
	
	-- Returns GUID for object, with brackets stripped:
	fn GetObjGuid obj = 
	(
		local attrGuid = RsGuidMgr.GetGuid obj
		attrGuid = substituteString attrGuid "{" ""
		attrGuid = substituteString attrGuid "}" ""
		return attrGuid
	)
	
	-- Returns a DCCEntityEdit item:
	fn CreateDccEntityEdit obj mapContentName mapSectionName containerAttrGuid nodeAttrGuid = 
	(
		local lodDistance
		local childLODDistance = getAttr obj idxGtaObjChildLODDist
		local useFullMatrix = getAttr obj idxGtaObjUseFullMtx
		
		if ((isRSref obj) and (getAttr obj idxGtaObjInstLODDist == false)) then
		(
			lodDistance = -1
		)
		else
		(
			lodDistance = getAttr obj idxGtaObjLODDist
		)
		
		local dccEntityEdit = dotnetobject "RSG.RESTServices.Game.Map.DCCEntityEdit" \
			mapContentName mapSectionName containerAttrGuid nodeAttrGuid \
			obj.pos.x obj.pos.y obj.pos.z \
			-obj.rotation.x -obj.rotation.y -obj.rotation.z obj.rotation.w \ 
			obj.scale.x obj.scale.y obj.scale.z \
			lodDistance childLODDistance useFullMatrix
		
		return dccEntityEdit
	)
	
	--helper methods
	fn DeleteAllTransformChangeHandlers =
	(
		deleteAllChangeHandlers id:#livemapeditTransformChangeHandlers
	)
	
	fn OnNodeTransformChanged nodeChanged =
	(
		-- here we just make a note that this node has moved, and its movement will be integrated on the next update
		append nodesToUpdate nodeChanged
	)
	
	-- callbacks
	fn UpdateTransformHandlers =
	(
		DeleteAllTransformChangeHandlers()
		
		for selectedNode in (getCurrentSelection()) do
		(
			if( GetAttrClass selectedNode == "Gta Object" ) then
			(
				when transform selectedNode change id:#livemapeditTransformChangeHandlers handleAt:#redrawViews selectedNode do OnNodeTransformChanged selectedNode
			)
		)
	)
	
	fn DeleteAllHandlers = 
	(
		timerUpdate.active = False
		callbacks.removeScripts id:#livemapeditSelectionSetChanged
		callbacks.removeScripts id:#livemapeditNodeDeleted
		DeleteAllTransformChangeHandlers()
	)
	fn EnableAllHandlers = 
	(
		UpdateTransformHandlers()
		callbacks.addScript #selectionSetChanged "RsLiveMapEditingRoll.UpdateTransformHandlers()" id:#livemapeditSelectionSetChanged
		callbacks.addScript #selectedNodesPreDelete "RsLiveMapEditingRoll.OnNodeDeleted()" id:#livemapeditNodeDeleted
		timerUpdate.active = True
	)
	
	-- Make sure Rag's Picking mode is enabled:
	fn EnableRagPicking = 
	(
		RemoteConnection.WriteBoolWidget "Picker/Picker Options/Enable picking" True
	)

	-- Make sure that Rag's Physics widget is active:
	fn EnablePhysicsWidget = 
	(
		local CreatePhysicsWidgetName = "Physics/Create physics widgets"
		RemoteConnection.SendCommand ("widget \"" + CreatePhysicsWidgetName + "\"")
		RemoteConnection.SendSyncCommand()
	)
	
	fn OnNodeDeleted =
	(
		local mapContainers = RsMapGetMapContainers()		
		local deletedNodes = callbacks.notificationParam()
		
		-- Do a precheck to see if any containers have been deleted. If they have then don't proceed with the rest of this callback
		local nodesToUpdateInGame = #()
		
		for deletedNode in deletedNodes do
		(
			local ownerMapContainer = RsMapObjectGetMapContainerFor deletedNode maps:mapContainers
					
			if ( (ownerMapContainer != undefined) and (ownerMapContainer.cont != deletedNode) and (getattrclass deletedNode == "Gta Object") ) do
			(
				append nodesToUpdateInGame deletedNode
			)
		)
		
		--
		for nodeToUpdateInGame in nodesToUpdateInGame do
		(	
			local ownerMapContainer = RsMapObjectGetMapContainerFor nodeToUpdateInGame maps:mapContainers
			if( ownerMapContainer != undefined and ownerMapContainer.name != undefined ) then
			(		
				local maxContentNode = RsContentTree.GetContentNode ownerMapContainer.filename				
				if ( maxContentNode != undefined ) do
				(
					local mapZipExportNode = RsContentTree.GetMapExportZipNode maxContentNode
					local mapProcessedNode = RsContentTree.GetMapProcessedNode mapZipExportNode
					
					local mapContentName = mapZipExportNode.name
					local mapSectionName = mapProcessedNode.name
					
					local imapGroupName = toLower (getattr nodeToUpdateInGame idxGtaObjIPLGroup)
					if( imapGroupName != "undefined" and imapGroupName.count > 0 ) then
					(
						mapSectionName = imapGroupName
					)
					
					local containerAttrGuid = undefined
					if( ownerMapContainer.cont != undefined ) then
					(
						containerAttrGuid = GetObjGuid ownerMapContainer.cont
					)
					else
					(
						local sceneXmlNode = RsContentTree.GetMapExportSceneXMLNode mapZipExportNode
						local xmlPathname = sceneXmlNode.absolutepath
						containerAttrGuid = RsMakeBackSlashes xmlPathname
					)
				
					local nodeAttrGuid = GetObjGuid nodeToUpdateInGame
					dccEntityDelete = dotnetobject "RSG.RESTServices.Game.Map.DCCEntityDelete" mapContentName mapSectionName containerAttrGuid nodeAttrGuid
					
					liveMapEditingService.AddEntityDelete dccEntityDelete maxContentNode
				)
			)
		)
	)
	
	fn setControlsEnabled state = 
	(
		if state then 
		(
			EnableAllHandlers()
		)
		else 
		(
			etStatus.text = "Not connected to the game"
			
			DeleteAllHandlers()
			liveMapEditingService = undefined
		)
		
		btnConnect.state = state
		#(btnSendCameraUpdates, btnReceiveCameraUpdates, btnAddEntity, btnForceUpdate, btnPickEntities, btnSettleProps, etStatus, btnLoadMiloInfo).enabled = state
		
		return OK
	)
	
	fn OnConnectionStateChanged connected =
	(
		if ( connected ) then
		(
			etStatus.text = "Connecting..."
			
			if ( not RemoteConnection.IsConnected() ) then
			(
				RemoteConnection.Connect()
			)
			
			local ipAddress = RemoteConnection.GetGameIP()
			
			if ( (ipAddress != undefined) and (RemoteConnection.IsConnected()) ) then
			(
				etStatus.text = ("Connected to game at " + ipAddress)
				setControlsEnabled True
				
				append ipAddress RsLiveEditPortSuffix
				
				liveMapEditingService = dotnetobject "RSG.RESTServices.Game.Map.LiveMapEditingService" ipAddress
			)
			else
			(
				setControlsEnabled False				
				messagebox "No connection to the game could be established" title:"Connection Failure"
			)
		)
		else
		(
			setControlsEnabled False
		)
	)
	
	local CameraSetup = false
	fn SetupCamera =
	(
		-- Only need to do this once:
		if CameraSetup do return False
		
		--Create the camera widgets
		RemoteConnection.WriteBoolWidget "Camera/Create camera widgets" true
		RemoteConnection.SendSyncCommand()
		
		--Override streaming focus
		RemoteConnection.WriteBoolWidget "Camera/Free camera/Override streaming focus" true
		RemoteConnection.SendSyncCommand()
		
		--Overide FOV
		RemoteConnection.WriteBoolWidget "Camera/Frame propagator/Override FOV" True
		RemoteConnection.SendSyncCommand()
		
		--Sync up FOV
		viewport.SetFOV 69.5
		RemoteConnection.WriteFloatWidget "Camera/Frame propagator/Overridden FOV" 45
		RemoteConnection.SendSyncCommand()
		
		--Setup safe frame and aspect ratio (16:9)
		RenderWidth = 16
		RenderHeight = 9
		DisplaySafeFrames = True
		
		CameraSetup = True
	)
	
	fn addCamSyncEvent camXform:(Inverse (getViewTM())) = 
	(
		SetupCamera()
		
		if( chkActive.checked ) then
		(
			RemoteConnection.WriteBoolWidget "Renderer/Portals/FORCE RESCAN - USE WHEN PORTALS BREAK" true
			
			offsetRotation = eulerToQuat( eulerAngles spnXRotOffset.Value spnYRotOffset.Value spnZRotOffset.Value )
 			rotate camXform offsetRotation
			offsetPosition = [ spnXPosOffset.Value, spnYPosOffset.Value, spnZPosOffset.Value ]
			translate camXform offsetPosition
		)
		else
		(
			RemoteConnection.WriteBoolWidget "Renderer/Portals/FORCE RESCAN - USE WHEN PORTALS BREAK" false
		)
		
		local cameraPosition = camXform[4]
		local cameraFrontVector = -camXform[3]
		local cameraUpVector = camXform[2]
			
		RemoteConnection.WriteFreeCameraStream cameraPosition cameraUpVector cameraFrontVector 45.0
	)

	local PreviousViewTM = Matrix3 1
	local DNString = DotNetClass "System.String"
	local DNHttpWebRequest = DotNetClass "System.Net.HttpWebRequest"
	local RequestString
	local LastReceiveTime = TimeStamp()
	local LastSendTime = TimeStamp()
	local GraceTime = 100
	
	fn OnUpdate =
	(
		if (liveMapEditingService == undefined) do 
		(
			timerUpdate.active = False
			return False
		)
		
		-- Try and send a camera update
		if( btnSendCameraUpdates.state and TimeStamp() - LastReceiveTime > GraceTime ) do
		(
			if( viewport.IsPerspView() ) then
			(
				CurrentViewTM = GetViewTM()
				
				-- Only resync camera if view has changed:
				if not ( RsCompareMatrix CurrentViewTM PreviousViewTM Accuracy:0.01 ) do 
				(
					LastSendTime = TimeStamp()
					addCamSyncEvent camXform:( Inverse CurrentViewTM )
				)
				
				PreviousViewTM = CurrentViewTM
			)
			else
			(
				btnSendCameraUpdates.state = false
			)
		)
		
		-- If we didn't send any camera updates then try and receive something instead
		if( btnReceiveCameraUpdates.state and TimeStamp() - LastSendTime > GraceTime ) do
		(
			try
			(
				Request = DNHttpWebRequest.Create RequestString
				Request.Method = "GET"
				
				Response = Request.GetResponse()
				
				ResponseXmlData = DotNetObject "System.Xml.XmlDocument"
				ResponseXmlData.Load( Response.GetResponseStream() )
				
				PositionElement = ( ResponseXmlData.GetElementsByTagName "vPos" ).Item 0
				FrontElement = ( ResponseXmlData.GetElementsByTagName "vFront" ).Item 0
				UpElement = ( ResponseXmlData.GetElementsByTagName "vUp" ).Item 0
				FOVElement = ( ResponseXmlData.GetElementsByTagName "fov" ).Item 0
				
				MaxCameraTM_Up = [ 	(UpElement.GetAttribute "x") as Float, 
									(UpElement.GetAttribute "y") as Float, 
									(UpElement.GetAttribute "z") as Float]
				
				MaxCameraTM_Front = -[	(FrontElement.GetAttribute "x") as Float,
										(FrontElement.GetAttribute "y") as Float,
										(FrontElement.GetAttribute "z") as Float]
				
				MaxCameraTM_Right = Cross MaxCameraTM_Up MaxCameraTM_Front
				
				MaxCameraTM_Translation = [	(PositionElement.GetAttribute "x") as Float,
											(PositionElement.GetAttribute "y") as Float,
											(PositionElement.GetAttribute "z") as Float]
											
				--Final camera matrix
				MaxCameraTM = Matrix3 MaxCameraTM_Right MaxCameraTM_Up MaxCameraTM_Front MaxCameraTM_Translation
			
				--If we have offset active
				if( chkActive.checked ) then
				(
					offsetPosition = -[ spnXPosOffset.Value, spnYPosOffset.Value, spnZPosOffset.Value ]
					translate MaxCameraTM offsetPosition
					
					offsetRotation = ( eulerToQuat( eulerAngles spnXRotOffset.Value spnYRotOffset.Value -(spnZRotOffset.Value) ) )
					rotate MaxCameraTM offsetRotation
				)
				
				--Final view matrix
				MaxViewTM = Inverse MaxCameraTM
				
				CurrentViewTM = GetViewTM()
				
				-- Only alter camera if it's different from the CurrentViewTM
				if not ( RsCompareMatrix CurrentViewTM MaxViewTM Accuracy:0.001 ) do 
				( 
					SetupCamera()
					LastReceiveTime = TimeStamp()
					Viewport.SetTM MaxViewTM 
				)
			)
			catch( btnReceiveCameraUpdates.State = False )
		)
		
		nodesToUpdate = (makeUniqueArray nodesToUpdate)
		nodesToUpdate = for obj in nodesToUpdate where (isValidNode obj) collect obj
		
		-- push any entity edits
		if (nodesToUpdate.count > 0) then
		(
			local mapContainers = RsMapGetMapContainers()

			for nodeToUpdate in nodesToUpdate do
			(
				local ownerMapContainer = RsMapObjectGetMapContainerFor nodeToUpdate maps:mapContainers
				
				if( ownerMapContainer != undefined ) then
				(
					local maxContentNode = RsContentTree.GetContentNode ownerMapContainer.filename
				
					if ( maxContentNode != undefined ) do
					(
						local mapZipExportNode = RsContentTree.GetMapExportZipNode maxContentNode
						local mapProcessedNode = RsContentTree.GetMapProcessedNode mapZipExportNode						
						
						local mapContentName = mapZipExportNode.name
						local mapSectionName = mapProcessedNode.name
						local imapGroupName = toLower (getattr nodeToUpdate idxGtaObjIPLGroup)
						
						if (imapGroupName != "") and (imapGroupName != "undefined") do 
						(
							mapSectionName = imapGroupName
						)
						
						local containerAttrGuid = undefined
						local mapCont = ownerMapContainer.cont
						if( mapCont != undefined ) then
						(
							containerAttrGuid = GetObjGuid mapCont
						)
						else
						(
							local sceneXmlNode = RsContentTree.GetMapExportSceneXMLNode mapZipExportNode
							local xmlPathname = sceneXmlNode.absolutepath
							containerAttrGuid = RsMakeBackSlashes xmlPathname
						)
					
						local nodeAttrGuid = GetObjGuid nodeToUpdate
						local dccEntityEdit = CreateDccEntityEdit nodeToUpdate mapContentName mapSectionName containerAttrGuid nodeAttrGuid
						liveMapEditingService.AddEntityEdit dccEntityEdit maxContentNode
						
						local dontRenderInShadows = getattr nodeToUpdate idxGtaObjDontRdrInShadow
						local onlyRenderInShadows = getattr nodeToUpdate idxGtaObjOnlyRdrInShadow
						local dontRenderInReflections = getattr nodeToUpdate idxGtaObjDontRdrInReflections
						local onlyRenderInReflections = getattr nodeToUpdate idxGtaObjOnlyRdrInReflections
						local dontRenderInWaterReflections = getattr nodeToUpdate idxGtaObjDontRdrInWaterReflections
						local onlyRenderInWaterReflections = getattr nodeToUpdate idxGtaObjOnlyRdrInWaterReflections
						
						dccEntityFlagChange = dotnetobject "RSG.RESTServices.Game.Map.DCCEntityFlagChange" \
											mapContentName mapSectionName containerAttrGuid nodeAttrGuid \
											dontRenderInShadows onlyRenderInShadows \
											dontRenderInReflections onlyRenderInReflections \ 
											dontRenderInWaterReflections onlyRenderInWaterReflections
						
						liveMapEditingService.AddEntityFlagChange dccEntityFlagChange maxContentNode
					)
				)
			)
		)
		
		nodesToUpdate.count = 0
		
		--SendUpdates() is smart and only sends data if its required
		StartStamp = TimeStamp()
		liveMapEditingService.SendUpdates()
		EndStamp = TimeStamp()
		
		--Check if the update took long we have lost our ability to send camera updates to the game
		if( EndStamp - StartStamp > 1000 ) then
		(
			btnSendCameraUpdates.state = false
		)
	)
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------
	
	-- event: The rollout is opening
	on RsLiveMapEditingRoll open do
	(
		-- Initialise tech-art banner:
		bannerStruct.setup()
		
		local mapContainers = RsMapGetMapContainers()
		if (mapContainers.count > 0) then
		(
			RsContentTree.SetupGlobalsFromMapsAndLoadContentTree mapContainers forceReload:true
		)
	)
	
	on RsLiveMapEditingRoll close do
	(
		DeleteAllHandlers()
	)
	
	on btnConnect changed state do
	(
		OnConnectionStateChanged (state == on)
	)
	
	on btnSendCameraUpdates changed state do
	(		
		if not( viewport.IsPerspView() ) then
		(
			btnSendCameraUpdates.state = false
			messagebox "Must be in perspective view to sync the game camera with the viewport"
		)
	)
	
	on btnReceiveCameraUpdates changed state do
	(		
		if not( viewport.IsPerspView() ) then
		(
			btnReceiveCameraUpdates.state = false
			messagebox "Must be in perspective view to sync the game camera with the viewport"
		)
		else
		(
			IPString = ( RemoteConnection.GetGameIP() as String )
			RequestString = DNString.Format @"http://{0}:7890/scene/OutgoingChanges/gameCamera" IPString
		)
	)
	
	on btnWarpToSelection pressed do
	(
		warpPlayerToSelection()
	)
		
	on btnAddEntity pressed do
	(
		for selectedNode in (getCurrentSelection()) do
		(
			if (isKindOf selectedNode RSrefObject ) then
			(
				local nodeAttrGuid = GetObjGuid selectedNode
				
				local lodDistance = getattr selectedNode idxGtaObjLODDist
				local childLODDistance = getattr selectedNode idxGtaObjChildLODDist
				local useFullMatrix = getattr selectedNode idxGtaObjUseFullMtx
				
				dccEntityAdd = dotnetobject "RSG.RESTServices.Game.Map.DCCEntityAdd" \
									selectedNode.objectName nodeAttrGuid \
									selectedNode.pos.x selectedNode.pos.y selectedNode.pos.z \
									-selectedNode.rotation.x -selectedNode.rotation.y -selectedNode.rotation.z selectedNode.rotation.w \ 
									selectedNode.scale.x selectedNode.scale.y selectedNode.scale.z \
									lodDistance childLODDistance useFullMatrix
				
				liveMapEditingService.AddEntityAdd dccEntityAdd
			)
			else
			(
				format "Unable to add node '%' as it isn't an rsref object\n" selectedNode.name
			)
		)
	)
	
	on btnForceUpdate pressed do
	(
		for obj in getCurrentSelection() where (GetAttrClass obj == "Gta Object") do
		(
			append nodesToUpdate obj
		)
	)
	
	fn DoPickSelected = 
	(
		local mapContainers = RsMapGetMapContainers()
		for thisMap in mapContainers do 
		(
			local mapSelObjs = for obj in thisMap.selObjects where (GetAttrClass obj == "Gta Object") collect obj
			
			if (mapSelObjs.count != 0) do 
			(
				local maxContentNode = RsContentTree.GetContentNode thisMap.filename
			
				if ( maxContentNode != undefined ) do
				(	
					local mapZipExportNode = RsContentTree.GetMapExportZipNode maxContentNode
					local mapProcessedNode = RsContentTree.GetMapProcessedNode mapZipExportNode					
					
					local mapContentName = mapZipExportNode.name
					local mapSectionName = mapProcessedNode.name
					
					local containerAttrGuid = undefined
					local mapCont = thisMap.cont
					
					if( mapCont != undefined ) then
					(
						containerAttrGuid = GetObjGuid mapCont
					)
					else
					(
						local sceneXmlNode = RsContentTree.GetMapExportSceneXMLNode mapZipExportNode
						local xmlPathname = sceneXmlNode.absolutepath
						containerAttrGuid = RsMakeBackSlashes xmlPathname
					)
					
					for obj in mapSelObjs do 
					(
						local nodeAttrGuid = GetObjGuid obj
						
						local imapGroupName = toLower (getattr obj idxGtaObjIPLGroup)
						local objGrpName = if (imapGroupName != "") and (imapGroupName != "undefined") then imapGroupName else mapSectionName
					
						local archetypeName = if (isKindOf obj RSrefObject) then obj.objectName else obj.name
						
						dccEntityPick = dotnetobject "RSG.RESTServices.Game.Map.DCCEntityPick" \
							archetypeName mapContentName objGrpName containerAttrGuid nodeAttrGuid
						
						liveMapEditingService.AddEntityPick dccEntityPick maxContentNode
					)
				)
			)
		)
		
		liveMapEditingService.SendUpdates()
	)
	
	on btnLoadMiloInfo pressed do
	(
		try
		(
			miloInfoNameString = remoteConnection.ReadStringWidget "Renderer/Portals/MLO debug/MLO"
			
			miloInfoNameString = substring MiloInfoNameString 10 -1
			miloInfoNameString = (filterstring MiloInfoNameString " ")[1]
			etMiloName.Text = miloInfoNameString
			
			miloInfoPositionString = remoteConnection.ReadStringWidget "Renderer/Portals/MLO debug/Selected MLO Pos"
			
			miloInfoPositionString = filterstring miloInfoPositionString "( ,)"
			spnXPosOffset.Value = miloInfoPositionString[1] as Float
			spnYPosOffset.Value = miloInfoPositionString[2] as Float
			spnZPosOffset.Value = miloInfoPositionString[3] as Float
			
			miloInfoRotationString = remoteConnection.ReadStringWidget "Renderer/Portals/MLO debug/Selected MLO Rot"
			
			miloInfoRotationString = filterstring miloInfoRotationString "( ,)"
			
			miloRotationQuat = Quat (miloInfoRotationString[1] as Float)( miloInfoRotationString[2] as Float)( miloInfoRotationString[3] as Float)( miloInfoRotationString[4] as Float)
			miloRotationEuler = quatToEuler  miloRotationQuat
			
			spnXRotOffset.Value = miloRotationEuler.X
			spnYRotOffset.Value = miloRotationEuler.Y
			spnZRotOffset.Value = -miloRotationEuler.Z
		)
		catch
		(
			MessageBox "Failed to source information from RAG. Ensure you have Renderer->Portals->MLO debug->MLO active with the interior you want to work with selected"
		)
	)
	
	on btnPickEntities pressed do
	(
		DoPickSelected()
	)
	
	on btnSettleProps pressed do 
	(
		-- Disable change-handlers:
		DeleteAllHandlers()
		-- Kill tool if Rsref database isn't/can't be loaded:
		local success = RsRefFuncs.databaseActive toolName:"Live Map Edit" debugFuncSrc:"btnSettleProps"
		if (not success) do 
		(
			EnableAllHandlers()
			return False
		)
		
		pushPrompt "Initialising Game-To-Max Module"
		local ipAddress = RemoteConnection.GetGameIP()
		append ipAddress RsLiveEditPortSuffix
		local gameToMaxLiveEditingServiceClient = dotnetobject "RSG.RESTServices.Game.Map.GameToMaxLiveEditingServiceClient" ipAddress
		
		-- Make sure relevant Rag bits are active:
		EnableRagPicking()
		EnablePhysicsWidget()
		popPrompt()
		
		local objDataList = #()
		struct objDataStruct (obj, handle, newXform, dccEntityPick, dccEntityKey)
		
		pushPrompt "Collecting data for selected objects..."
		
		local mapContainers = RsMapGetMapContainers()
		for thisMap in mapContainers do 
		(
			-- Get map's selected dynamic refs:
			local mapSelObjs = for obj in thisMap.selObjects where (isRsRef obj) and (obj.refDef != undefined) and (obj.refDef.isDynamic) collect obj

			if (mapSelObjs.count != 0) do 
			(
				local maxContentNode = RsContentTree.GetContentNode thisMap.filename
			
				if ( maxContentNode != undefined ) do
				(	
					local mapZipExportNode = RsContentTree.GetMapExportZipNode maxContentNode
					local mapProcessedNode = RsContentTree.GetMapProcessedNode mapZipExportNode					
					
					local mapContentName = mapZipExportNode.name
					local mapSectionName = mapProcessedNode.name
					
					local containerAttrGuid = undefined
					local mapCont = thisMap.cont
					
					if( mapCont != undefined ) then
					(
						containerAttrGuid = GetObjGuid mapCont
					)
					else
					(
						local sceneXmlNode = RsContentTree.GetMapExportSceneXMLNode mapZipExportNode
						local xmlPathname = sceneXmlNode.absolutepath
						containerAttrGuid = RsMakeBackSlashes xmlPathname
					)
				
					for obj in mapSelObjs do 
					(
						local imapGroupName = toLower (getattr obj idxGtaObjIPLGroup)
						local objGrpName = if (imapGroupName != "") and (imapGroupName != "undefined") then imapGroupName else mapSectionName
					
						local nodeAttrGuid = GetObjGuid obj
						
						-- Generate picker-data for object:
						local dccEntityPick = dotNetObject "RSG.RESTServices.Game.Map.DCCEntityPick" obj.objectName mapContentName objGrpName containerAttrGuid nodeAttrGuid
						
						-- Generate and register entity-key for object:
						local dccEntityKey = dotNetObject "RSG.RESTServices.Game.Map.DCCEntityKey" mapContentName mapSectionName containerAttrGuid nodeAttrGuid obj.handle
						gameToMaxLiveEditingServiceClient.RegisterMaxNode dccEntityKey maxContentNode
						
						-- Collect object-data:
						append objDataList (objDataStruct obj:obj handle:obj.handle dccEntityPick:dccEntityPick dccEntityKey:dccEntityKey)
						
						-- Add update-event to ensure that game object starts in same place as Max version:
						local dccEntityEdit = CreateDccEntityEdit obj mapContentName mapSectionName containerAttrGuid nodeAttrGuid
						liveMapEditingService.AddEntityEdit dccEntityEdit maxContentNode
					)
				)
			)
		)
		popPrompt()
		
		-- Quit function if no valid objects were processed:
		if (objDataList.count == 0) do 
		(
			EnableAllHandlers()
			return False
		)
		
		local clusterSize = 50.0
		
		-- Sort objects so that the next object is the one closest to the current object, to minimise major position-changes when skipping around in-game.
		(
			local newList = #()
			
			local objPositions = for item in objDataList collect item.obj.pos
			local objCount = objDataList.count
			
			-- Find midpoint:
			local midPos = [0,0,0]
			for thisPos in objPositions do (midPos += thisPos)
			midPos /= objCount
			
			-- Start with object furthest from midpoint:
			local objDists = for thisPos in objPositions collect (distance thisPos midPos)
			local objNum = findItem objDists (aMax objDists)
			
			append newList objDataList[objNum]
			
			local usedObjNums = #{objNum}
			usedObjNums.count = objCount
			
			local currPos = objPositions[objNum]
			local clusterCount = 0
			
			while (usedObjNums.numberSet < objCount) and (not keyboard.escPressed) do 
			(
				clusterCount += 1
				
				for findObjNum in -usedObjNums do 
				(
					local thisDist = distance currPos objPositions[findObjNum]
					
					if (thisDist < clusterSize) do 
					(
						-- Add nearby object to list:
						append newList objDataList[findObjNum]
						usedObjNums[findObjNum] = True
					)
				)
				
				if (usedObjNums.numberSet < objCount) do 
				(
					-- Add next unsorted object to list:
					local nextNum = ((-usedObjNums) as array)[1]
					
					append newList objDataList[nextNum]
					usedObjNums[nextNum] = True
					currPos = objPositions[nextNum]
				)
			)
			
			-- Use updated object-list:
			objDataList = newList
		)
		
		format "clusterCount: %\n" clusterCount
		
		-- Ensure that game-object transforms match their Max-scene versions:
		pushPrompt "Syncing game objects to Max-scene positions"
		liveMapEditingService.SendUpdates()
		popPrompt()
		
		if not (viewport.IsPerspView()) do 
		(
			viewport.setType #view_persp_user
		)
		
		progressStart "Settling objects..."
		local changeItems = #()
		local itemCount = objDataList.count
		local isConnected = True
		
		local lastCamPos = undefined
		
		for itemNum = 1 to itemCount while isConnected and progressUpdate (100.0 * itemNum / itemCount) do 
		(
			local item = objDataList[itemNum]
			local obj = item.obj
--format "%: %\n" itemNum obj.name
			-- Move camera if this object is a long way away from the last one:
			if (lastCamPos == undefined) or (distance lastCamPos obj.pos > clusterSize) do 
			(
				lastCamPos = obj.pos
				
				local camXform = matrix3 1
				camXform[4] = [obj.pos.X, obj.pos.Y, obj.pos.Z + 60]
				addCamSyncEvent camXform:camXform
				liveMapEditingService.SendUpdates()
				
				-- Give it some time to finish loading after moving camera:
				sleep 1.0
			)
			
			-- Pick object:

			liveMapEditingService.AddEntityPick item.dccEntityPick
			liveMapEditingService.SendUpdates()

			-- Got that?  Wait a mo...
			sleep 0.01
			
			-- Trigger physics on object:
			RemoteConnection.SendCommand ("widget \"Physics/Physics debug tools/Activate picked entity\"")
			RemoteConnection.SendSyncCommand()

			local iterations = 0
			local changedXform = True
			while changedXform and isConnected do 
			(
				-- Give object more time to settle:
				sleep 0.01
				
				iterations += 1
				changedXform = False

				local liveUpdates = gameToMaxLiveEditingServiceClient.GetUpdates()
				local objUpdate = liveUpdates[1]
				
				if (objUpdate != undefined) do 
				(
					-- Convert transform-matrix to max format:
					local getGameXform = objUpdate.LocalToWorldTransform
					local getXform = matrix3 1				
					getXform.row1 = point3 getGameXform.A.X getGameXform.A.Y getGameXform.A.Z
					getXform.row2 = point3 getGameXform.B.X getGameXform.B.Y getGameXform.B.Z
					getXform.row3 = point3 getGameXform.C.X getGameXform.C.Y getGameXform.C.Z
					getXform.row4 = point3 getGameXform.D.X getGameXform.D.Y getGameXform.D.Z
					
--format "%: %\n" obj.name getXform
					
					if not (RsCompareMatrix item.newXform getXform) do 
					(
						if (item.newXform != undefined) or (not (RsCompareMatrix obj.transform getXform)) do 
						(
							item.newXform = getXform
							changedXform = True
						)
					)
				)
				
				isConnected = (iterations < 100) and (RemoteConnection.IsConnected())
			)
			
			if isConnected and (item.newXform != undefined) and (not (RsCompareMatrix item.newXform obj.transform)) do 
			(

local xformA = item.newXform
local xformB = obj.transform
format "%: CHANGED! % (%, %, %, %) \n" obj.name iterations (distance xformA[1] xformB[1]) (distance xformA[2] xformB[2]) (distance xformA[3] xformB[3]) (distance xformA[4] xformB[4])

				append changeItems (dataPair obj:obj transform:item.newXform)
			)

		)
		progressEnd()
		
		gameToMaxLiveEditingServiceClient = undefined
		
		if not isConnected do 
		(
			format "Disconnected\n"
			setControlsEnabled False
			return False
		)
		
		-- Apply any changes:
		if (changeItems.count == 0) then 
		(
			messageBox "No in-game changes detected" title:"No changes detected"
		)
		else 
		(
			local changesListLabels = #("Apply", "Object Name", "Moved/m", "Rotated")
			local changesList = for item in changeItems collect 
			(
				local obj = item.obj
				local startXform = obj.transform
				local endXform = item.transform
				
				local posDiff = distance (startXform.translationPart) (endXform.translationPart)
				
				local angleDiff = ((inverse startXform.rotationPart) * endXform.rotationPart) as eulerAngles
				local angleDiffString = ("[" + (angleDiff.X as string) + "," + (angleDiff.Y as string) + "," + (angleDiff.Z as string) + "]")
				
				#(obj.name, posDiff, angleDiffString)
			)
			
			-- Called when list is double-clicked:
			fn listDoubleClick Sender Args = 
			(
				if (args.RowIndex >= 0) do 
				(
					local objName = Sender.Rows.Item[args.RowIndex].Cells.Item[1].Value
					
					local obj = (getNodeByName objName)
					
					if (obj != undefined) do 
					(
						-- Max select/zoom:
						select obj
						max tool ZoomExtents
						
						-- Move in-game camera:
						local camXform = matrix3 1
						camXform[4] = [obj.pos.X, obj.pos.Y, obj.pos.Z + 60]
						addCamSyncEvent camXform:camXform
						liveMapEditingService.SendUpdates()
						
						-- In-game pick:
						DoPickSelected()
					)
				)
			)
			
			-- Set all boxes as ticked by default:
			local tickedBoxes = #{}
			tickedBoxes.count = changesList.count
			tickedBoxes = - tickedBoxes
			
			local queryVal = RsQueryBoxMultiBtn "Changes were detected in-game.  Apply changes?\n(Double-click to select objects)" title:"Query: Apply changes?" \ 
				width:600 labels:#("Apply Changes", "Cancel") tooltips:#() defaultBtn:1 cancelBtn:2 timeout:-1 \ 
				listItems:changesList listTitles:changesListLabels listCheckStates:tickedBoxes listDoubleClickFunc:listDoubleClick
			
			if (queryVal == 1) and (tickedBoxes.numberSet != 0) do 
			(
				pushPrompt "Syncing Max objects to settled positions"
				undo "settle objects" on 
				(
					for item in changeItems do 
					(
						item.obj.transform = item.transform
					)
				)
				popPrompt()
			)
		)
		
		-- Re-enable change-handlers:
		EnableAllHandlers()
		
		setNeedsRedraw()
		
		return OK
	)
	
	on timerUpdate tick do
	(
		OnUpdate()
	)
	
	fn filePostMerge = 
	(
		local Param = (callbacks.notificationParam())
		if (Param == 1) do (return False) -- Abort if triggered by Xref-system merge

		local mapContainers = RsMapGetMapContainers()
		RsContentTree.SetupGlobalsFromMapsAndLoadContentTree mapContainers forceReload:true
	)
		
	fn setupFileCallbacks = 
	(
		callbacks.removeScripts id:#RsLiveMapEditing
		callbacks.addScript #filePostMerge "RsLiveMapEditingRoll.filePostMerge()" id:#RsLiveMapEditing
	)
)

-----------------------------------------------------------------------------
-- Entry
-----------------------------------------------------------------------------
createDialog RsLiveMapEditingRoll
RsLiveMapEditingRoll.setupFileCallbacks()

-- pipeline/ui/live_map_editing.ms
