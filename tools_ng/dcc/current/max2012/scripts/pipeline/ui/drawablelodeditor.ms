--
-- File:: drawablelodeditor.ms
-- Description:: RAGE Drawable LOD Editor
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 21 April 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
--filein "pipeline/util/drawablelod.ms" -- loaded on startup by rockstar/util/material.ms

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global LinkType_DRAWLOD = 1

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsDrawableLodEditorRoll "Drawable LOD Editor"
(
	--
	-- name: pickCallback
	-- desc: Function triggered when picking for a new node.
	--
	fn pickCallback obj = 
	(
		(obj != selection[1] ) and (GetAttrClass obj == "Gta Object")
	)
	
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/LOD_Editor#Drawable_LOD_Editor" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	edittext 	currentItem 	"Current:" readonly:true labelOnTop:true offset:[0,-20]
	button 		buttLODDIstances "Setup LOD distances"
	
	
	
	group "Higher Detail Object:" 
	(
		edittext 		txtHDItem 		"" readonly:true
		button 			btnSelectHD	"Select" across:3
		pickButton 	btnPickHD		"Pick" filter:pickCallback
		button			btnRemoveHD 	"Remove"
	)
	
	group "Lower Detail Object:" 
	(
		edittext		txtLDItem		"" readonly:true
		button 			btnSelectLD 	"Select" across:3
		pickButton	btnPickLD 		"Pick" filter:pickCallback
		button			btnRemoveLD	"Remove"	
	)
	
	group "Tools:" 
	(
		button			btnExpandInY	"Expand Y-Axis" width:100 offset:[0,-4] across:2
		button			btnExpandInX	"Expand X-Axis" width:100 offset:[0,-4]
		button			btnCollapse		"Collapse" width:140
	)
	
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	--
	-- name: updateSelection
	-- desc: Function triggered when the selection changes and this rollout is open.
	--
	fn updateSelection = 
	(
		local currentPickSelection = selection as array
	
		local invalidSel = True
		
		txtHDItem.text = "None"
		txtLDItem.text = "None"
		
		local enableAlways = #(btnBbUVs)
		
		case currentPickSelection.count of 
		(
			0:
			(
				currentItem.text = "None"
			)
			1:
			(
				invalidSel = False
				local expandEnabled = false
				RsDrawableLodEditorRoll.controls.enabled = true
				
				currentItem.text = currentPickSelection[1].name

				local hdmodel = RsLodDrawable_GetHigherDetailModel currentPickSelection[1]
				if ( undefined != hdmodel ) then 
				(
					txtHDItem.text = hdmodel.name
					expandEnabled = true
				)
				else 
				(
					btnRemoveHD.enabled = false
				)

				local ldmodel = RsLodDrawable_GetLowerDetailModel currentPickSelection[1]
				if ( undefined != ldmodel ) then 
				(
					txtLDItem.text = ldmodel.name
					expandEnabled = true
				)
				else 
				(
					btnRemoveLD.enabled = false
				)
				
				btnExpandInY.enabled = expandEnabled
				btnExpandInX.enabled = expandEnabled
				btnCollapse.enabled = expandEnabled
			)
			Default:
			(
				currentItem.text = "Invalid - multiple"
			)
		)
		
		if invalidSel do 
		(
			for ctrl in RsDrawableLodEditorRoll.controls do 
			(
				if ((isKindOf ctrl buttonControl) or (isKindOf ctrl pickerControl)) and (findItem enableAlways ctrl == 0) do 
				(
					ctrl.enabled = false
				)
			)
		)
	)
	
	--
	-- name: expandHierarchy
	-- desc: Expands a drawable lod hierarchy in the x or y axis.
	--
	fn expandHierarchy obj axis:#xaxis = (
	
		local lowestDetailObj = obj
		while ( RsLodDrawable_HasLowerDetailModel lowestDetailObj ) do
			lowestDetailObj = ( RsLodDrawable_GetLowerDetailModel lowestDetailObj )
		
		format "Lowest detail model: %\n" lowestDetailObj.name
		
		local nextDetailObj = ( RsLodDrawable_GetHigherDetailModel lowestDetailObj )
		local size = ( lowestDetailObj.max - lowestDetailObj.min )
		local newPos = nextDetailObj.pos
		while ( undefined != nextDetailObj ) do
		(			
			if ( #xaxis == axis ) then
			(
				newPos.x = newPos.x + size.x + 0.5
			)
			else if ( #yaxis == axis ) then
			(
				newPos.y = newPos.y + size.y + 0.5
			)
			nextDetailObj.pos = newPos
			
			nextDetailObj = ( RsLodDrawable_GetHigherDetailModel nextDetailObj )
		)
	)
	
	--
	-- name: collapseHierarchy
	-- desc: Collapses a drawable lod hierarchy aligning the pivot points.
	--
	fn collapseHierarchy obj = (
	
		local lowestDetailObj = obj
		while ( RsLodDrawable_HasLowerDetailModel lowestDetailObj ) do
			lowestDetailObj = ( RsLodDrawable_GetLowerDetailModel lowestDetailObj )
			
		format "Lowest detail model: %\n" lowestDetailObj.name
		local nextDetailObj = ( RsLodDrawable_GetHigherDetailModel lowestDetailObj )
		while ( undefined != nextDetailObj ) do
		(
			nextDetailObj.pos = lowestDetailObj.pos
			nextDetailObj = ( RsLodDrawable_GetHigherDetailModel nextDetailObj )
		)
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	--
	-- event: btnSelectHD button pressed
	-- desc: Select the currently set Higher Detail node.
	--
	on btnSelectHD pressed do
	(
		local toSel = ( RsLodDrawable_GetHigherDetailModel selection[1] )
		if ( undefined != toSel ) then
			select toSel
	)
	
	--
	-- event: btnPickHD button pressed.
	-- desc: Pick a new Higher Detail node.
	--
	on btnPickHD picked selHD do
	(
		local selobj = selection[1]		

		local maps = RsMapGetMapContainers()
			
		-- Make sure the two objects are in the same container
		selObjCont = RsMapObjectGetMapContainerFor selobj maps:maps
		selHDCont = RsMapObjectGetMapContainerFor selHD maps:maps		
		if ( selObjCont != undefined and selHDCont != undefined and selObjCont.name == selHDCont.name ) then
		(		
			RsLodDrawable_SetLowerDetailModel selHD selobj

			select selObj
			updateSelection()
		)
		else
		(
			MessageBox ( selHD.name + " is not in the same map container so can't be added to the LOD hierarchy" )
		)
	)
	
	--
	-- event: btnRemoveHD button pressed
	-- desc: Remove the Higher Detail node.
	--
	on btnRemoveHD pressed do
	(
		local selobj = selection[1]
		
		local selHD = ( RsLodDrawable_GetHigherDetailModel selobj )
		if ( undefined != selHD ) then
		(
			RsSceneLink.RemoveContainer linkType_DRAWLOD selHD		
			select selobj
			updateSelection()
		)
	)
	
	--
	-- event: btnSelectLD button pressed
	-- desc: Select the currently set Lower Detail node.
	--
	on btnSelectLD pressed do
	(
		local toSel = ( RsLodDrawable_GetLowerDetailModel selection[1] )
		
		if ( undefined != toSel ) do 
		(
			select toSel
		)
	)
	
	--
	-- event: btnPickLD button pressed
	-- desc: Pick a new Lower Detail node.
	--
	on btnPickLD picked selLD do
	(
		local selobj = selection[1]		

		local maps = RsMapGetMapContainers()
		
		-- Make sure the two objects are in the same container
		selObjCont = RsMapObjectGetMapContainerFor selobj maps:maps
		selLDCont = RsMapObjectGetMapContainerFor selLD maps:maps
		if ( selObjCont != undefined and selLDCont != undefined and selObjCont.name == selLDCont.name ) then
		(
			RsLodDrawable_SetLowerDetailModel selobj selLD

			select selObj
			updateSelection()
		)
		else
		(
			MessageBox ( selLD.name + " is not in the same map container so can't be added to the LOD hierarchy" )
		)			
	)
	
	--
	-- event: btnRemoveLD button pressed
	-- desc: Remove the Lower Detail node.
	--
	on btnRemoveLD pressed do
	(
		local selobj = selection[1]
		
		local selLD = ( RsLodDrawable_GetLowerDetailModel selobj )
		if ( undefined != selLD ) then
		(
			RsSceneLink.RemoveContainer linkType_DRAWLOD selobj
		
			select selobj
			updateSelection()
		)
	)
	
	--
	-- event: btnExpandInY button pressed
	-- desc: Expand drawable hierarchy along y-axis.
	--
	on btnExpandInY	pressed do
	(
		local selobj = selection[1]
		expandHierarchy selobj axis:#yaxis
	)
	
	--
	-- event: btnExpandInX button pressed
	-- desc: Expand drawable hierarchy along x-axis.
	--
	on btnExpandInX	pressed do
	(
		local selobj = selection[1]
		expandHierarchy selobj axis:#xaxis
	)
	
	--
	-- name: btnCollapse button pressed
	-- desc: Collapse drawable hierarchy.
	--
	on btnCollapse pressed do
	(
		local selobj = selection[1]
		collapseHierarchy selobj
	)

	on buttLODDIstances pressed do
	(
		RsLodDrawable_CreateLODSetupDialog()
	)
	
	--
	-- event: Rollout open event.
	-- desc: Add the selection change handler.
	--
	on RsDrawableLodEditorRoll open do (
		updateSelection()
		callbacks.addscript #selectionSetChanged "RsDrawableLodEditorRoll.updateSelection()" id:#DrawableLodEditorSelect
	)
	
	--
	-- event: Rollout close event.
	-- desc: Remove the selection change handler.
	--
	on RsDrawableLodEditorRoll close do (
		callbacks.removescripts id:#DrawableLodEditorSelect
	)
	
	-- Save rolled-up state:
	on RsDrawableLodEditorRoll rolledUp down do 
	(
		RsSettingWrite "RsDrawableLodEditorRoll" "rollup" (not down)
	)
)

-- drawablelodeditor.ms
