--
-- File:: pipeline/ui/SetObjectAttr.ms.ms
-- Description:: Set Object Attributes
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com
-- Date:: 19 March 2010
--

filein "pipeline/util/scene.ms"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
try (destroyDialog setObjAttrRoll) catch()

rollout setObjAttrRoll "Attribute Tools"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Set_Object_Attributes" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	local buttonHeight = 22
	local buttonWidth = 140
	
	group "Attribute setting"
	(
		button btnIPL "Don't Add to IPL" width:buttonWidth height:buttonHeight
		button btnDontExport "Don't Export" width:buttonWidth height:buttonHeight
		button btnApplyRad "Don't Apply Radiosity" width:buttonWidth height:buttonHeight
	)
	
	group "Custom user properties"
	(
		button btnDeleteUserProps "Delete user props" width:buttonWidth height:buttonHeight
	)
	
	--////////////////////////////////////////////////////////////
	-- functions
	--////////////////////////////////////////////////////////////
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	

	on btnIPL pressed do (
		AddIgnoreSelected()
	)
	
	on btnDontExport pressed do (
		SetDontExportSelected()
	)
	
	on btnApplyRad pressed do (
		SetDontApplyRadiosity()
	)
	
	on btnDeleteUserProps pressed do
	(
		DeleteUserProps()
	)
)

createDialog setObjAttrRoll width:250