-- Author : Mike Wilson (mike.wilson@rockstartoronto.com)

try(CloseRolloutFloater RsAudioEmitterFloater)catch()

rollout RsAudioEmitterRollout "Attributes"
(
	dropDownList ddlEmitters "Emitters:"
	edittext edtOverride "Override:"
	button btnRefresh "Refresh" width:100
	button btnSet "Set" width:100

	fn LoadEmitterXML = (
		emitterNames = #()

		files = getFiles ( RsProjectGetAudioDir() "/assets/Objects/Core/Audio/GAMEOBJECTS/AMBIENCE/ENTITY_EMITTERS/*.xml" )
		for f in files do (
			xmlDoc = XmlDocument()	
			xmlDoc.init()
			xmlDoc.load f

			xmlRoot = xmlDoc.document.DocumentElement
			if xmlRoot != undefined then (
				dictelems = xmlRoot.childnodes

				for i = 0 to ( dictelems.Count - 1 ) do (
					dictelem = dictelems.itemof(i)
					if dictelem.name == "EntityEmitter" then (
						nameattr = dictelem.Attributes.ItemOf( "name" )
						if nameattr != undefined then (
							append emitterNames nameattr.value
						)
					)
				)
				
			)
		)

		ddlEmitters.items = emitterNames
	)
	
	on btnRefresh pressed do (
		LoadEmitterXML()
	)
	
	on btnSet pressed do (

		attrString = ddlEmitters.selected

		if edtOverride.text != "" then (
			attrString = edtOverride.text
		)

		effectIndex = GetAttrIndex "Gta AudioEmit" "Effect"
		if effectIndex != undefined then (
			if "Gta AudioEmit" == (getattrClass $) then (
				setAttr $ effectIndex attrString
			)
		)
	)
	
	on RsAudioEmitterRollout open do (
		LoadEmitterXML()
	)
)

global RsAudioEmitterFloater = newRolloutFloater "Audio Emitter" 300 170
AddRollout RsAudioEmitterRollout RsAudioEmitterFloater
