-- time tools
-- 

rollout RsTimeToolsRoll "Time Tools"
(
	spinner spnStart "from" across:2 range:[1,24,1] type:#integer
	spinner spnEnd "to" range:[1,24,24] type:#integer
	button btnOn "Set to ON between hours"
	button btnOff "Set to OFF between hours"
	
	fn setSelectionTo onVal = 
	(
		for o in $selection do
		(
			local updateInstance = (isKindOf o ::RageLightInstance)
			
			for i = 1 to 24 do
			(
				local attrIndex = getattrindex (getattrclass o) (i as string)
				if undefined!=attrIndex then
				(
					if spnStart.value > spnEnd.value then
					(
						if i>spnStart.value or i<spnEnd.value then
							setattr o attrIndex onVal
					)
					else
					(
						if i>=spnStart.value and i<=spnEnd.value then
							setattr o attrIndex onVal
					)
					
					if updateInstance do 
					(
						o.handleAttributeChange (i as string)
					)
				)
			)
		)
		select $selection
	)
	
	on btnOn pressed do
	(
		setSelectionTo true
	)
	on btnOff pressed do
	(
		setSelectionTo false
	)
)

-- try( DestroyDialog RsTimeToolsRoll) catch()
-- CreateDialog RsTimeToolsRoll