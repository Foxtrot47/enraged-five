-- Author : Mike Wilson (mike.wilson@rockstarnorth.com)

-- cutfileLoader plugin

filein "pipeline/ui/cutscenelight.ms"

SetInteractionMode false 
	
inputScenes = getDirectories (RsConfigGetProjRootDir() + "assets/cuts/*")

openLog (RsConfigGetLogDir() + "ListenerLog_CutsceneLightBatch.txt")

ExportCutsceneLightingScenes inputScenes

closeLog()

quitMAX #noprompt
	
