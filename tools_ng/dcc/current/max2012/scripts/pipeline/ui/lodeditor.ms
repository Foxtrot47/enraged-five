--
-- File:: pipeline/ui/lodeditor.ms
-- Description:: LOD Editor
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
-- Date:: 18/9/2003
--
-----------------------------------------------------------------------------

global RsLodEditorUtil
try (CloseRolloutFloater RsLodEditorUtil) catch()

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"
filein "pipeline/ui/drawablelodeditor.ms"
filein "pipeline/ui/treelodeditor.ms"

-----------------------------------------------------------------------------
-- Global
-----------------------------------------------------------------------------
global LinkType_DRAWLOD = 1
global LinkType_LOD = 0

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- SCENE LOD EDITOR
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout RsSceneLodEditorRoll "Scene LOD Editor"
(
	local currentPickSelection = #()
	local lodChildren, parentObjs
	local isPickActive = false
	
	fn getParentObjs obj = 
	(
		local checkObj = obj
		local parentObjs = #()
		
		local checkNext = true
		
		while checkNext do 
		(
			checkObj = RsSceneLink.GetParent LinkType_LOD checkObj
			
			if (checkObj == undefined) then 
			(
				checkNext = false
			)
			else 
			(
				append parentObjs checkObj
			)
		)
		
		parentObjs
	)
	
	fn pickObjFilter obj = 
	(
		local okObj = true
		
		for item in currentPickSelection while okObj do 
		(
			okObj = (item != obj) and ((RsGetObjContainer item) == (RsGetObjContainer obj))
		)
		
		return okObj
	)
	
	fn pickChildFilter obj = 
	(
		((findItem lodChildren obj) == 0) and 
		((findItem parentObjs obj) == 0) and 
		(pickObjFilter obj)
	)
	
	fn stopSelCallback disableAttrCallback:false = 
	(
		callbacks.removescripts id:#RsPickLodSelect
		
		-- Disable/enable Attribute Editor selection-callback:
		if (::RsAttrEditMainRoll != undefined) do 
		(
			RsAttrEditMainRoll.disableCallbacks = disableAttrCallback
		)
	)
	
	fn startSelCallback = 
	(
		stopSelCallback()
		callbacks.addscript #selectionSetChanged "RsSceneLodEditorRoll.updateSelection()" id:#RsPickLodSelect
	)
	
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/LOD_Editor#Scene_LOD_Editor" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	edittext currentItem "Current:" readonly:true
	edittext parentItem "Parent:" readonly:true
	
	button buttonSelectParent "Select Parent" across:2 tooltip:"Select LOD-parents of selected object(s)"
	button buttonSelectChild "Select Children" tooltip:"Select LOD-children of selected object(s)"
	multiListBox childrenItems "Children:"
	pickButton buttonPickParent "Pick Parent" filter:pickObjFilter across:2 tooltip:"Pick new LOD-parent for selected object(s)"
	checkButton buttonAddChildren "Add Children" tooltip:"Pick new LOD-children for selected object"
	button buttonRemoveParents "Remove Parent" across:2 tooltip:"Remove LOD-parent from selected object(s)"
	button buttonRemoveChildren "Remove Children" tooltip:"Remove highlighted LOD-children from selected object(s)"
	
	button buttonLoadFromExported "Load From Export" tooltip:"Load scene LOD-hierarchy from exported data"
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	fn updateSelection = 
	(
		if isPickActive do return false
		
		currentPickSelection = getCurrentSelection()
		
		local hasObjs = (currentPickSelection.count != 0)
		local hasParents = false

		currentItem.text = case currentPickSelection.count of 
		(
			0:"None"
			1:(currentPickSelection[1].name)
			default:"Multiple"
		)
		
		local parentList = #()
		for obj in currentPickSelection do 
		(
			local parNode = RsSceneLink.GetParent LinkType_LOD obj
			appendIfUnique parentList parNode
			
			if isValidNode parNode do 
			(
				hasParents = true
			)
		)
		
		parentItem.text = case of 
		(
			(not hasParents):"None"
			(parentList.count == 1):parentList[1].name
			default:"Multiple"
		)
		
		local childList = #()
		for obj in currentPickSelection do 
		(
			local childs = getLODChildren obj
			join childList childs
		)
		childList = makeUniqueArray childList
		
		local nameList = for obj in childList where isValidNode obj collect obj.name
		sort nameList
		childrenItems.items = nameList
		
		buttonSelectParent.enabled = hasParents
		buttonSelectChild.enabled = hasObjs
		buttonPickParent.enabled = hasObjs
		buttonRemoveParents.enabled = hasParents
		buttonAddChildren.enabled = (currentPickSelection.count == 1)
		
		childrenItems.selection = #{}
		buttonRemoveChildren.enabled = false		
		
		OK
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on childrenItems doubleClicked numClicked do 
	(
		local newSelect = getnodebyname childrenItems.items[numClicked]
		
		if (newSelect == undefined) then 
		(
			clearSelection()
		)
		else 
		(
			select newSelect
		)
		completeRedraw()
	)
	
	on childrenItems selected numClicked do 
	(
		buttonRemoveChildren.enabled = (childrenItems.selection.numberSet != 0)
	)
	
	on buttonSelectParent pressed do 
	(
		undo "select parent" on 
		(
			local selObjs = for obj in currentPickSelection collect (RsSceneLink.GetParent LinkType_LOD obj)
			selObjs = for obj in selObjs where isValidNode obj collect obj

			clearSelection()
			select selObjs
		)
	)
	
	on buttonSelectChild pressed do 
	(
		undo "select children" on 
		(
			local selObjs = #()

			for obj in currentPickSelection do 
			(
				local lodChildren = getLODChildren obj
				
				for childObj in lodChildren where isValidNode childObj do 
				(
					append selObjs childobj
				)
			)

			selectMore selObjs
		)
	)
	
	on buttonPickParent picked parObj do 
	(
		if parObj == undefined do 
		(
			return 0
		)
		
		for obj in currentPickSelection do 
		(
			RsSceneLink.AddContainer LinkType_LOD obj
			RsSceneLink.SetParent LinkType_LOD obj parObj
		)
		
		flashNodes #(parObj)
		select currentPickSelection
	)	
	
	on buttonAddChildren changed pressed do 
	(
		-- Don't do anything if button is being unpressed:
		if isPickActive do 
		(
			buttonAddChildren.checked = true
			return false
		)
		
		stopSelCallback disableAttrCallback:true
		
		local parentObj = currentPickSelection[1]
		parentObjs = getParentObjs parentObj
		lodChildren = getLODChildren parentObj
		
		select lodChildren
		selectMore parentObj
		
		-- Disable controls to stop things from being disturbed mid-picking:
		local ctrlEnableds = for ctrl in RsSceneLodEditorRoll.controls collect ctrl.enabled
		RsSceneLodEditorRoll.controls.enabled = false
		
		local newChildren = #()
		local pickedObject
		
		isPickActive = true
		
		while ( ( pickedObject = pickObject filter:pickChildFilter forceListenerFocus:false) != undefined ) do
		(
			append lodChildren pickedObject
			append newChildren pickedObject
			
			local nameList = for obj in lodChildren where (isValidNode obj) collect obj.name
			sort nameList
			childrenItems.items = nameList
			
			selectmore pickedObject
		)
		
		isPickActive = false
		
		-- Restore control enabledness:
		for ctrlNum = 1 to ctrlEnableds.count do RsSceneLodEditorRoll.controls[ctrlNum].enabled = ctrlEnableds[ctrlNum]
		
		buttonAddChildren.checked = false

		for childObj in newChildren do 
		(
			RsSceneLink.AddContainer LinkType_LOD childObj
			RsSceneLink.SetParent LinkType_LOD childObj parentObj
		)
		
		select parentObj
		updateSelection()
		
		startSelCallback()
		
		flashNodes lodChildren
	)
	
	on buttonRemoveChildren pressed do 
	(
		local selChildNames = for childNum in childrenItems.selection collect childrenItems.items[childNum]
		
		for selobj in selection do 
		(
			lodChildren = getLODChildren selobj
			
			for childObj in lodChildren where (findItem selChildNames childObj.name != 0) do 
			(
				RsSceneLink.RemoveContainer LinkType_LOD childObj
			)
		)	
		
		updateSelection()		
	)
	
	on buttonRemoveParents pressed do (
	
		if selection.count < 1 then (
			MessageBox "please select at least one object"
		)
					
		for selobj in selection do (
			RsSceneLink.RemoveContainer LinkType_LOD selobj
		)	
		
		updateSelection()
	)	
	
	fn getInstances filenameIPL instanceData = (

		fileMainIPL = openfile filenameIPL mode:"r"
		
		if fileMainIPL != undefined then
		(
			mode = #none
			while eof fileMainIPL == false do
			(
				lineMainIPL = readline fileMainIPL
				
				if lineMainIPL == "end" then
				(
					mode = #none
				) 
				else 
				(
					if mode == #none then
					(
						if lineMainIPL == "inst" then 
						(
							mode = #inst
						)
					) 
					else 
					(
						if mode == #inst then
						(
							-- Hack because some collision objects get '\t' attached to them before the string in the ipl file?
							if lineMainIPL[1] == "\t" then
							(
								tokensMainIPL = filterstring lineMainIPL ", \t"
								append instanceData tokensMainIPL
							)
							else
							(
								tokensMainIPL = filterstring lineMainIPL ", "
								append instanceData tokensMainIPL
							)
						)
					)
				)
			)
			close fileMainIPL
		)		
	)

	fn findObjectByNameAndPos parentobj objname objpos = 
	(
		for obj in parentobj.children do
		(
			nameMatch = false
	
			if isRefObj obj then
			(
				if obj.objectname == objname then nameMatch = true
			) 
			else 
			(
				local checkName
				
				case of 
				(
					(isInternalRef obj):
					(
						local srcobj = obj.getSourceObj()
						if (srcobj != undefined) do
						(
							checkName = srcobj.name
						)
					)
					default:
					(
						checkName = obj.name
					)
				)
				
				if matchPattern checkName pattern:objname do nameMatch = true
			)
	
			if nameMatch then
			(
				if doesVectorMatch objpos obj.pos 0.001 then
				(
					return obj
				)
			)
		
			-- search any children of this child object
			foundobj = findObjectByNameAndPos obj objname objpos
	
			if foundobj != undefined then
			(
				return foundobj
			)
		)
		return undefined
	)
	
	fn checkLodSetup instListCheck instListMain = (

		for inst in instListCheck do
		(
			instIndex = inst[10] as integer

			if instIndex != -1 then (
			
				lodinst = instListMain[instIndex + 1]
			
				obj = findObjectByNameAndPos rootnode inst[1] [inst[3] as float,inst[4] as float,inst[5] as float]
				lodobj = findObjectByNameAndPos rootnode lodinst[1] [lodinst[3] as float,lodinst[4] as float,lodinst[5] as float]
			
				if obj != undefined and lodobj != undefined then (
				
					RsSceneLink.RemoveContainer LinkType_LOD obj
					RsSceneLink.AddContainer LinkType_LOD obj
					RsSceneLink.SetParent LinkType_LOD obj lodobj
					print ("Loading lod attributes onto object: " + (obj as string))
				) else (
				
					if obj == undefined then
					(
						print ("not found: " + inst[1] + " at " + ([inst[3] as float,inst[4] as float,inst[5] as float] as string))
					) else
					(
						print ("not found: " + lodinst[1] + " at " + ([lodinst[3] as float,lodinst[4] as float,lodinst[5] as float] as string))
					)
				)
			)
		)		
	)
	
	on buttonLoadFromExported pressed do (
	
		local maps = RsMapGetMapContainers()
		if maps.count < 1 then
		(
			Messagebox "Unable to load from export as no maps exist in the scene"
		)
		else
		(		
			for map in maps do
			(
				RsMapSetupGlobals map
					
				filenameMainIPL = RsMakeBackSlashes(RsMapDir + "\\" + RsMapName + ".ipl")
				filenamesStreamIPL = getfiles (RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/" + "*.ipl")
				instancesMain = #()
		
				getInstances filenameMainIPL instancesMain
 				checkLodSetup instancesMain instancesMain
 				
 				for filenameStreamIPL in filenamesStreamIPL do (
 				
 					print filenameStreamIPL
 			
 					local instancesStream = #()
	
 					getInstances filenameStreamIPL instancesStream
 					checkLodSetup instancesStream instancesMain		
 				)
			)
		)
	)
	
	on RsSceneLodEditorRoll open do 
	(
		updateSelection()
		startSelCallback()
	)
	
	on RsSceneLodEditorRoll close do 
	(
		stopSelCallback()
	)
	
	-- Save rolled-up state:
	on RsSceneLodEditorRoll rolledUp down do 
	(
		RsSettingWrite "RsSceneLodEditorRoll" "rollup" (not down)
	)
)

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- LOD VIEW
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout LodViewRoll "LOD View"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/LOD_Editor#LOD_View" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnSceneView "Scene LOD View" across:2
	button btnDrawableView "Drawable LOD View"
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnSceneView pressed do (
		RsSceneLink.ShowDialog LinkType_LOD
	)
	
	on btnDrawableView pressed do (
		RsSceneLink.ShowDialog linkType_DRAWLOD
	)
	
	-- Save rolled-up state:
	on LodViewRoll rolledUp down do 
	(
		RsSettingWrite "LodViewRoll" "rollup" (not down)
	)
)

rollout RsLodEditorBannerRoll ""
(
	dotNetControl RsBannerPanel "Panel" pos:[6,3] height:32 width:(RsLodEditorBannerRoll.width - 6)
	local bannerStruct = makeRsBanner dn_Panel:RsBannerPanel wiki:"LOD_Editor" filename:(getThisScriptFilename())
	
	-- Refresh banner-icons when floater's rollouts are adjusted:
	on RsBannerPanel Paint Sender Args do 
	(
		for n = 0 to (RsBannerPanel.Controls.Count - 1) do 
		(
			RsBannerPanel.Controls.Item[n].Invalidate()
		)
	)
	
	--------------------------------------------------------------
	-- Rollout open/close
	--------------------------------------------------------------
	on RsLodEditorBannerRoll open do
	(
		-- Initialise tech-art banner:
		bannerStruct.setup()
	)
)

rollout RsLODPivotMatchRoll "Pivot Match"
(
	-- CONTROLS 
	radiobuttons rdb_matchPiv_direction labels:#("LOD to HD", "HD to LOD")
	radiobuttons rdb_matchPiv_Selection labels:#("All Geo", "Selection") default:2
	
	button btn_matchPiv_pivOnly "Match Pivot Only" width:230
	button btn_matchPiv_object "Snap Object By Pivot" width:230
	
	-- FUNCTIONS 	
	fn snap_pivot pivotOnly:false =
	(
		undo on
		(
			-- GET THE SELCTION OF OBJECTS
			local object_array = selection as array
			if (rdb_matchPiv_Selection.state == 1) do object_array = geometry as array
	
			if (object_array.count != 0) then 
			(
				local sel = for i in object_array where (classof_array #(Editable_Poly, Editable_Mesh) i) collect i 				
				local LOD_array = #()
				
				-- GET OBJECTS THAT HAVE A LOD PARENT AND NO CHILDREN 
				for obj in sel do
				(
					local child_array = #()
					RsSceneLink.GetChildren LinkType_LOD obj &child_array
					local lod_parent = RsSceneLink.GetParent LinkType_LOD obj
					
					if (child_array.count == 0 AND lod_parent != undefined) do
					(
						append LOD_array (datapair obj:obj lodParent:lod_parent)
					)		
				)

				-- DO MATCH  
				if (queryBox ("Are you sure you want to move the pivots of " + LOD_array.count as string + " objects?") Title:"Hold up!") do 
				(
					for i in LOD_array do 
					(					
						if (rdb_matchPiv_direction.state == 2) then
						(
							if (pivotOnly) then i.obj.pivot = i.lodParent.pivot 
							else i.obj.pos = i.lodParent.pos 
						)
						else
						(
							if (pivotOnly) then i.lodParent.pivot = i.obj.pivot
							else i.lodParent.pos = i.obj.pos 
						)
					)
				)
			)
			else messageBox "Nothing Selected!"
		)
	)
	
	-- EVENTS 
	on btn_matchPiv_pivOnly pressed do snap_pivot pivotOnly:true
	on btn_matchPiv_object pressed do snap_pivot()
	
)

(
	RsLodEditorUtil = newRolloutFloater "LOD Editor" 250 900 50 96
	addRollout RsLodEditorBannerRoll RsLodEditorUtil border:False

	for thisRoll in #(RsSceneLodEditorRoll, RsDrawableLodEditorRoll, RsComboLodSourceRoll, LodViewRoll,RsLODPivotMatchRoll) do 
	(
		addRollout thisRoll RsLodEditorUtil rolledup:(RsSettingsReadBoolean thisRoll.name "rollup" false)
	)
)