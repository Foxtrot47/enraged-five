--
-- File:: pipeline/util/notenodes.ms
-- Description:: Note Nodes editing/viewing operations
--
-- Author:: Adam Munson <adam.munson@rockstarnorth.com>
-- Date:: 5/10/10
--
-----------------------------------------------------------------------------
try CloseRolloutFloater RsNoteNodesUtil catch()

filein "pipeline/import/importnotenodes.ms"
filein "pipeline/export/exportnotenodes.ms"

Rollout RsNoteNodesSetRoll "Options"
(
	checkbox chkViewAllSets "View All Sets" across:2
	spinner spnSetNumber "Set" range:[1,5,1] type:#integer width:40
	button btnDeleteNodeGroup "Delete Node Group"
	button btnCreateNodeGroup "Create Node Group"
	button btnCreateNoteNodes "Create Note Node(s)"
	
	fn RsNoteNodesViewSet = 
	(
		for s = 1 to RsNoteNodeGroup.count do
		(
			if ( s != spnSetNumber.value ) then
			(
				for nn in RsNoteNodeGroup[s] do
				(
					hide nn
				)
			)
			else
			(
				for nn in RsNoteNodeGroup[s] do
				(
					unhide nn
				)
			)
		)	
	)

	on RsNoteNodesSetRoll open do
	(
		btnDeleteNodeGroup.enabled = false	
		btnCreateNoteNodes.enabled = false
	)
	
	on chkViewAllSets changed state do
	(
		clearSelection()
		if ( false == RsNoteNodeCheckIfGroupExists() ) do return false
		
		-- Show or hide nodes depending on if we have view all sets checked
		-- or not
		if ( true == state ) then
		(
			for s = 1 to RsNoteNodeGroup.count do
			(
				for nn in RsNoteNodeGroup[s] do
				(
					if ( nn != undefined ) do
					(
						unhide nn
					)
				)
			)
		)	
		else
		(
			RsNoteNodesViewSet()	
		)
	)
	
	on spnSetNumber changed val do
	(
		clearSelection()
		RsNoteNodeSet = val
		RsNoteNodeNum = RsNoteNodeGroup[RsNoteNodeSet].count
		if ( 0 == RsNoteNodeNum ) do
		(
			RsNoteNodeNum = 1
		)
		if ( false == chkViewAllSets.state ) do
		(
			RsNoteNodesViewSet()
		)
	)
	
	on btnDeleteNodeGroup pressed do
	(
		RsNoteNodesDeleteGroup()
		btnCreateNodeGroup.enabled = true
		btnDeleteNodeGroup.enabled = false
		btnCreateNoteNodes.enabled = false
	)
	
	on btnCreateNodeGroup pressed do
	(
		if ( true == RsNoteNodeCheckIfGroupExists() ) do
		(
			if (not QueryBox "This will delete the current node group, continue?" ) do
			(
				return false
			)
		)
		RsNoteNodesDeleteGroup()
		btnCreateNodeGroup.enabled = false
		btnDeleteNodeGroup.enabled = true
		btnCreateNoteNodes.enabled = true
	)
	
	on btnCreateNoteNodes pressed do
	(
		startObjectCreation RsNoteNode
	)
	
	on RsNoteNodesSetRoll close do
	(
		RsNoteNodeGroup			= #()			
		RsNoteNodeSel			= undefined
		RsNoteNodeSet			= undefined
		RsNoteNodeNum			= undefined
		RsNoteNodeCreateLink 	= undefined
	)
)

Rollout RsNoteNodesFilterRoll "Filter"
(
	MultiListBox lstBlockNames "Filter by block(s):"
	
	fn RsUpdateBlocksList = 
	(
		blockList = #()
		
		for nodeSet in RsNoteNodeGroup do
		(
			for nn in nodeSet do
			(
				if classof nn == RsNoteNode do 
				(
					appendIfUnique blockList nn.blockName
				)
			)
		)

		sort blockList
		insertItem "All" blockList 1
		lstBlockNames.items = blockList
		-- Default to first option (All)
		lstBlockNames.selection = #{1}
	)
	
	on lstBlockNames selectionEnd do 
	(
		clearSelection()
		-- If the 'All' option is selected then show all nodes
		if ( true == lstBlockNames.selection[1] ) then
		(
			for nodeSet in RsNoteNodeGroup do
			(
				for nn in nodeSet do
				(	
					unhide nn
				)
			)
			-- Unselect any other map section names as All is selected
			lstBlockNames.selection = #{1}
		)
		else if ( lstBlockNames.selection != 0 ) do
		(			
			for nodeSet in RsNoteNodeGroup do
			(
				for nn in nodeSet do
				(
					showNode = false
					
					if classof nn == RsNoteNode do
					(					
						-- If the node's block name matches one of the selected map
						-- section names, then flag it to be shown
						for blockIdx in lstBlockNames.selection do
						(
							if lstBlockNames.items[blockIdx] == nn.blockName then
							(	
								showNode = true
							)

						)
					)
					
					if ( true == showNode ) then
					(
						unhide nn
					)	
					else
					(
						hide nn
					)
				)
			)
		)
	)
)

-- Down here in script so that RsNoteNodesFilterRoll has been defined
Rollout RsNoteNodesImportRoll "Import/Export" 
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Note_Nodes" \
		align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
		
	button btnImportNodes "Import Note Nodes" align:#center
	button btnExportNoteNodes "Export Note Nodes" align:#center
	
	on btnImportNodes pressed do 
	(	
		if ( true == RsNoteNodeCheckIfGroupExists() ) do
		(
			if ( false == QueryBox "This will delete the current node group, continue?" ) do
			(
				return false
			)
		)
		RsNoteNodesDeleteGroup()
		RsNoteNodesImport()
		RsNoteNodesFilterRoll.RsUpdateBlocksList()
		RsNoteNodesSetRoll.btnDeleteNodeGroup.enabled = true
		RsNoteNodesSetRoll.btnCreateNoteNodes.enabled = true
	)		
	
	on btnExportNoteNodes pressed do 
	(
		RsNoteNodesExport()
	)
)

RsNoteNodesUtil = newRolloutFloater "Rockstar Note Nodes" 200 430 50 126
addRollout RsNoteNodesImportRoll RsNoteNodesUtil
addRollout RsNoteNodesSetRoll RsNoteNodesUtil
addRollout RsNoteNodesFilterRoll RsNoteNodesUtil