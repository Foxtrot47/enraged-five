-- Fragment relateed tools
-- @author Gunnar Droege

filein "pipeline/export/maps/mapsetup.ms"

rollout RsFragToolsRoll "Fragment tools"
(
	local buttWdith = 150
	group "Rebuild Frag Proxys"
	(
		button buttRebuildAll "Rebuild all" width:buttWdith
		button buttRebuildSel "Rebuild selected" width:buttWdith
		checkbox chkFreezeFragProxies "Freeze Frag-Proxies" checked:true
	)
	
	on chkFreezeFragProxies changed state do (
		gRsMapExportFragments.freezeProxies = chkFreezeFragProxies.checked
	)
	on buttRebuildAll pressed do
	(
		gRsMapExportFragments.preExportRename()
		gRsMapExportFragments.buildAllFragments silent:true
		gRsMapExportFragments.postExportRename()
	)

	on buttRebuildSel pressed do
	(
		gRsMapExportFragments.preExportRename()
		gRsMapExportFragments.buildAllFragments silent:true objs:selection
		gRsMapExportFragments.postExportRename()
	)

	--------------------------------------------------------------
	-- shutdown of rollout
	--------------------------------------------------------------
	on RsFragToolsRoll rolledUp down do 
	(
		RsSettingWrite "rsFragTools" "rollup" (not down)
	)
)
