--
-- File:: pipeline/ui/GuidTools_ui.ms
-- Description:: Set Object Attributes
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com
-- Date:: 19 March 2010
--

filein "pipeline/util/scene.ms"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
try (destroyDialog guidToolRoll) catch()

rollout guidToolRoll "Guid tools"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	local buttonHeight = 22
	local buttonWidth = 180
	
	label lblHeaderGuid "Object GUID:" width:230
	label lblGuid "<Select an object to show its guid.>" width:230
	label lblHeaderAttrGuid "Attribute GUID:" width:230
	label lblAttrGuid "<Select an object to show its guid.>" width:230
	label spacer "" width:230
	group "Global Attribute tools" 
	(
		button btnResetGuid "Reset Object Guid" width:buttonWidth height:buttonHeight
		button btnDupeGuidCheck "Check scene for dupe GUIDs" width:buttonWidth height:buttonHeight
	)
	
	--////////////////////////////////////////////////////////////
	-- functions
	--////////////////////////////////////////////////////////////
	
	fn CheckforSelectionGuid = 
	(
		if selection.count == 1 then
		(
			lblGuid.text = (RsGuidMgr.getGuid selection[1]) as string
			lblAttrGuid.text = (getAttrGuid selection[1]) as string
		)
		else 
		(
			lblGuid.text = "<Select an object to show its guid.>"
			lblAttrGuid.text = "<Select an object to show its guid.>"
		)
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	on btnResetGuid pressed do
	(
		if not queryBox "This will Reset the ID on these objects. If instanced, the instances will have to be recreated. Please make sure you know what you are doing." title:"Resetting of global persistent identifier" then
			return false
		for o in selection do
		(
			ResetAttrGuid o
			RsGuidMgr.ResetGuid o
			if (RsGuidMgr.getGuid o == "{00000000-0000-0000-0000-000000000000}") then
				RsGuidMgr.setGuid o (genGuid())
		)
		CheckforSelectionGuid()
	)
	
	on btnDupeGuidCheck pressed do 
	(
		gRsUlog.Init "Check for duplicate GUIDs in scene." appendToFile:false
		RsGuidTools.RsCheckForDuplicateObjGuids()
		if gRsUlog.Validate() then
			messagebox "Everything looks guid."
	)
	
	on guidToolRoll open do
	(
		callbacks.removeScripts id:#GuidToolsCallback
		callbacks.addscript #selectionSetChanged "try(::guidToolRoll.CheckforSelectionGuid())catch()" id:#GuidToolsCallback
		CheckforSelectionGuid()
	)
	on guidToolRoll close do
	(
		callbacks.removeScripts id:#GuidToolsCallback
	)
)

createDialog guidToolRoll width:250