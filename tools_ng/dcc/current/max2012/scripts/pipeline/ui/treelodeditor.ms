--
-- File:: treeLodEditor.ms
-- Description:: Lod-hierarchy editor for Combo-Lod Generator tool's source-meshes:
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/treelod_utils.ms" -- Includes channel-number globals

--try (destroyDialog RsComboLodSourceRoll) catch()
-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsComboLodSourceRoll "ComboLodder: Source-Prop Setup" width:250
(
	--
	-- name: pickCallback
	-- desc: Function triggered when picking for a new node.
	--
	fn pickCallback obj = 
	(
		(obj != selection[1] ) and ((isKindOf obj Editable_Poly) or (isKindOf obj Editable_Mesh))
	)
	
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/ComboLodder#Sources" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	edittext currentItem 	"Current:" readonly:true labelOnTop:true offset:[0,-20]
	label lblStatus			"" align:#left height:30 offset:[0,0]
	
	group "Higher Detail Object:" 
	(
		edittext		txtHiDefItem		"" readonly:true
		button 			btnSelectHiDef 	"Select" across:3
		pickButton	btnPickHiDef 		"Pick" filter:pickCallback
		button			btnRemoveHiDef	"Remove"	
	)
	
	group "Lower Detail Object:" 
	(
		edittext 		txtLoDefItem 		"" readonly:true
		button 			btnSelectLoDef	"Select" across:3
		pickButton 	btnPickLoDef		"Pick" filter:pickCallback
		button			btnRemoveLoDef 	"Remove"
		
		button			btnSwapLoDefs 	"v Swap ^" align:#center offset:[0,-1]
		
		label				lblRotAlt				"Tiltable Alternative:" align:#left offset:[0,-2]
		edittext 		txtRotAltItem 		"" readonly:true offset:[0,-3]
		button 			btnSelectRotAlt	"Select" across:3
		pickButton 	btnPickRotAlt		"Pick" filter:pickCallback
		button			btnRemoveRotAlt	"Remove"
	)
	
	group "Tools:"
	(
		button btnLinkSlods "Link Lod-Sources By Name" width:180 offset:[-1,0] tooltip:"Link scene's objects by name:\nSLOD2_  => SLOD_  => HD name"
		button btnSetupBBs "Setup Billboard Faces" width:180 offset:[0,-4] tooltip:"Set selection's billboard-shader UVs to default-scale uvs - [1,0,0] on UV-channel 3 (uvChannel_scale)"
		button btnFixNormals "Fix Normals" width:180 offset:[-1,-4]
		
		checkbox chkAxisAligned "Prop has Axis Aligned Faces" offset:[15,0] tooltip:"Object tagged as having Axis Aligned faces\n(set as red in UV-channel 4, which is \ntransferred to combinedmesh)"
	)
	
	-- These controls don't care about current selection, are always enabled:
	local alwaysEnabled = #(btnLinkSlods)
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	-- Gets higher/lower-detail model for combo-source object:
	fn getHigherDetail obj = 
	(
		local children = #()
		RsSceneLink.GetChildren LinkType_CombinerMesh obj &children
		
		local getObj = children[1]
		
		if (isValidNode getObj) then (return getObj) else (return undefined)
	)
	fn getLowerDetail obj rotAlt:False = 
	(
		local parents = #()
		RsSceneLink.GetParents LinkType_CombinerMesh obj &parents
		
		local getObj = if rotAlt then parents[2] else parents[1]
		
		if (isValidNode getObj) then (return getObj) else (return undefined)
	)
	
	-- Does object have "Axis Aligned Faces" data, set as red verts in uv-channel 4?
	fn hasAxisAlignedFaces obj = 
	(
		local axisAlignVerts = False
		
		local op = RsMeshPolyOp obj
		if op.getMapSupport obj uvChannel_axisAligned do 
		(
			for vertNum = 1 to (op.getNumMapVerts obj uvChannel_axisAligned) while (axisAlignVerts != #Both) do
			(
				local vertVal = op.getMapVert obj uvChannel_axisAligned vertNum
				local isAxisAlignVert = (vertVal == [1,0,0])
				
				if (vertNum == 1) then 
				(
					axisAlignVerts = isAxisAlignVert
				)
				else 
				(
					if (axisAlignVerts != isAxisAlignVert) do 
					(
						axisAlignVerts = #Both
					)
				)
			)
		)
		
		return axisAlignVerts
	)
	
	--
	-- name: updateCtrls
	-- desc: Function triggered when the selection changes and this rollout is open.
	--
	fn updateCtrls doRedraw:True = 
	(
		local currentPickSelection = selection as array
	
		(
			local invalidSel = True
			
			txtLoDefItem.text = "None"
			txtRotAltItem.text = "None"
			txtHiDefItem.text = "None"
			lblStatus.text = ""
			
			local enableAlways = #(btnBbUVs)
			
			lblStatus.text = ""
			
			case currentPickSelection.count of 
			(
				0:
				(
					currentItem.text = "None"
					lblStatus.text = "Select an object to edit ComboLodder\nsource-mesh links"
				)
				1:
				(
					local obj = currentPickSelection[1]
					
					invalidSel = not ((isKindOf obj.baseobject Editable_Poly) or (isKindOf obj.baseobject Editable_Mesh))
					
					if not invalidSel do 
					(
						RsComboLodSourceRoll.controls.enabled = true
						
						currentItem.text = obj.name

						local LoDefmodel = getLowerDetail obj
						if (isValidNode LoDefmodel) then 
						(
							txtLoDefItem.text = LoDefmodel.name
							
							local RotAltmodel = getLowerDetail obj rotAlt:True
							if (isValidNode RotAltmodel) then 
							(
								txtRotAltItem.text = RotAltmodel.name
							)
							else 
							(
								btnSelectRotAlt.enabled = false
								btnRemoveRotAlt.enabled = false
								btnSwapLoDefs.enabled = false
							)
						)
						else 
						(
							btnSelectLoDef.enabled = false
							btnRemoveLoDef.enabled = false

							btnSelectRotAlt.enabled = false
							btnPickRotAlt.enabled = false
							btnRemoveRotAlt.enabled = false
							
							btnSwapLoDefs.enabled = false
						)

						local HiDefmodel =  getHigherDetail obj
						if (isValidNode HiDefmodel) then 
						(
							txtHiDefItem.text = HiDefmodel.name
							expandEnabled = true
						)
						else 
						(
							btnSelectHiDef.enabled = false
							btnRemoveHiDef.enabled = false
						)
						
						if (not btnRemoveLoDef.enabled) and (not btnRemoveHiDef.enabled) do 
						(
							lblStatus.text = "Object has no ComboLodder-source links"
						)
					)
				)
				Default:
				(
					currentItem.text = "Multiple"
				)
			)
			
			if invalidSel do 
			(
				for ctrl in RsComboLodSourceRoll.controls do 
				(
					if ((isKindOf ctrl buttonControl) or (isKindOf ctrl pickerControl)) and (findItem enableAlways ctrl == 0) do 
					(
						ctrl.enabled = false
					)
				)
			)
			
			case of 
			(
				(btnRemoveHiDef.enabled):
				(
					lblStatus.text = "LOD-source mesh for ComboLodder tool.\nWill not be shown in RsRef Browser!"
				)
				(btnRemoveLoDef.enabled):
				(
					lblStatus.text = "Object has linked LOD-source meshes,\nfor use by Combined-LOD Generator"
				)
			)
			
			if (lblStatus.text == "") do 
			(
				lblStatus.text = "Invalid selection, can't edit ComboLod links"
			)
		)
		
		-- Update "Axis Aligned Faces" checkbox:
		(
			chkAxisAligned.enabled = False
			
			local axisAlignObjs = False
			local firstObj = True
			for obj in currentPickSelection where (isKindOf obj Editable_Poly) or (isKindOf obj Editable_Mesh) while (axisAlignObjs != #Both) do 
			(
				local isAxisAlignObj = hasAxisAlignedFaces obj
				
				if firstObj then 
				(
					firstObj = False
					axisAlignObjs = isAxisAlignObj
					
					chkAxisAligned.enabled = True
				)
				else 
				(
					if (axisAlignObjs != isAxisAlignObj) do 
					(
						axisAlignObjs = #Both
					)
				)
			)
			
			chkAxisAligned.triState = case axisAlignObjs of
			(
				False:	0
				True:		1
				#Both:	2
			)
		)
		
		btnLinkSlods.enabled = True

		-- Do redraw after selection-buttons are pressed:
		if doRedraw do 
		(
			redrawViews()
		)
	)
	
	-- Make sure two objects are in the same container:
	fn checkSameContainer objA objB = 
	(
		local maps = RsMapGetMapContainers()
		
		local mapContA = RsMapObjectGetMapContainerFor objA maps:maps
		local mapContB = RsMapObjectGetMapContainerFor objB maps:maps
		
		if (mapContA != undefined) and (mapContB != undefined) and (mapContA.name == mapContB.name ) then 
		(
			return True
		)
		else 
		(
			local msg = stringStream ""
			format "% is not in the same map-container as %, so can't be adding its LOD hierarchy" objA.name objB.name to:msg
			
			messageBox (msg as string) title:"ERROR: Objects in different map-containers"
			return False
		)
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	-- Sets "Axis Aligned Faces" state for selected objects, by applying red/black colour to UV-channel 4:
	on chkAxisAligned changed val do
	(
		local selObjs = for obj in selection where (isKindOf obj Editable_Poly) or (isKindOf obj Editable_Mesh) collect obj
		local setColour = if val then Red else Black
		
		for obj in selObjs do 
		(
			local op = RsMeshPolyOp obj
			op.setFaceColor obj uvChannel_axisAligned #all setColour
		)
		
		redrawViews()
	)
	
	on btnLinkSlods pressed do
	(
		local slodLinks = #()
		
		for obj in geometry where (getAttrClass obj == "Gta Object") and (matchPattern obj.name pattern:"SLOD*_*") do
		(
			local foundParent = undefined
			
			local objName = obj.name
			local underscoreIdx = (findString objName "_")
			
			if (underscoreIdx != undefined) do 
			(
				local prefix = toLower (substring objName 1 (underscoreIdx))
				
				local slodNum = if (prefix == "slod_") then 1 else (prefix[prefix.count - 1] as integer)
				
				if (slodNum != undefined) do 
				(
					local parentName = substring objName (underscoreIdx + 1) -1
					local parentPrefix = case slodNum of 
					(
						1:""
						2:("slod_")
						3:("slod" + (slodNum - 1) as string + "_")
					)
					
					foundParent = getNodeByName (parentPrefix + parentName)
				)
			)
			
			if (foundParent != undefined) do 
			(
				--format "% -> %\n" obj.name foundParent.name
				--RsSceneLink.SetParent LinkType_CombinerMesh foundParent obj
				append slodLinks (dataPair parent:foundParent child:obj)
			)
		)
		
		if (slodLinks.count == 0) then 
		(
			messagebox "No matching \"SLOD*_\" objects found in scene" title:"Nothing to do"
		)
		else 
		(
			local listItems = for item in slodLinks collect 
			(
				--item.child.name + " -> " + item.parent.name
				#(item.parent.name, item.child.name)
			)
			--sort listItems
			
			local msg = stringStream ""
			format "% object% found with names matching possible ComboLodder link-parents.\n\nSet parents?" listItems.count (if listItems.count == 1 then "" else "s") to:msg
			
			local queryVal = RsQueryBoxMultiBtn (msg as string) title:"Set link-parents" timeout:-1 labels:#("Yes", "No") tooltips:#("Set ComboLodder source-object parents", "Do nothing") listItems:listItems listTitles:#("Higher-Detail", "Lower-Detail") width:450
			
			if (queryVal == 1) do 
			(
				for item in slodLinks do 
				(
					RsSceneLink.SetParent LinkType_CombinerMesh item.parent item.child
				)
				
				-- Trigger selection-callback to update controls:
				select selection
			)
		)
	)
	
	-- Set billboard-poly UVs to default scale-uvs - [1,0,0] on channel 3 (uvChannel_scale)
	on btnSetupBBs pressed do 
	(
		local bbCount = 0
		
		undo "setup billboards" on 
		(
			for obj in selection where ((isKindOf obj Editable_Poly) or (isKindOf obj Editable_Mesh)) do 
			(
				local objOp = RsMeshPolyOp obj
				local bbFound = false
				
				local objMats = #()
				local matFaces = #()
				RsGetMaterialsOnObjFaces obj materials:objMats faceLists:matFaces
				
				for matNum = 1 to objMats.count do 
				(
					local subMat = objMats[matNum]
					
					if RsIsBillboardMat subMat do 
					(
						objOp.setFaceColor obj uvChannel_scale matFaces[matNum] [255, 0, 0]
						bbFound = true
					)
				)
				
				if bbFound do 
				(
					bbCount += 1
					
					-- set planar maps
					local op = RsMeshPolyOp obj
					
					op.applyUVWMap obj #planar channel:uvChannel_planar tm:(rotateXMatrix 90)
				)
			)
		)
		
		if (bbCount != 0) then 
		(
			local msg = "Billboard UVs set up on " + bbCount as string + " objects"
			
			messageBox msg title:"UV-setup completed"
		)
		else 
		(
			messageBox "No billboard materials found in selection!" title:"UV-setup failed"
		)
	)
	
	on btnFixNormals pressed do
	(		
		if ( 0 == selection.count ) then
		(
			MessageBox "No objects selected to Fix Normals for." title:"Nothing selected"
		)
		else
		(
			with undo label:"Fix Normals" on
			(
				setCommandPanelTaskMode #modify
				for obj in selection do 
				(
					nomod = false

					if obj.modifiers.count > 1 then (
						
						print "obj can only have 1 modifier"
						return 0
					) 
				
					if obj.modifiers.count == 0 then (
					
						nomod = true
					)
				
					if classof obj == Editable_Poly then (
					
						convertto obj Editable_Mesh
					)
				
					if obj.modifiers.count == 0 then (
					
						addmodifier obj (Edit_Normals())
					)
					
					if (classof obj.modifiers[1]) != Edit_normals then (
					
						print "obj has to have Edit_normals modifier"
						return 0
					)
					
					refMod = obj.modifiers[1]
					
					if refmod.getnumfaces() != getnumfaces(obj) then (
				
						print "number of normal faces doesn't match mesh faces"
						return 0	
					)
					
					numfaces = refmod.getnumfaces()
				
					for i = 1 to numfaces do (
					
						if refmod.getfacedegree i != 3 then (
						
							print "normal face isnt a tri"
							return 0				
						)
					)
				
					flipfaces = #()
								
					for i = 1 to numfaces do (
					
						meshFace = getface obj i
						
						normal_idx_a = refmod.getnormalid i 1
						normal_idx_b = refmod.getnormalid i 2
						normal_idx_c = refmod.getnormalid i 3
						normal_a = refMod.Getnormal normal_idx_a
						normal_b = refMod.Getnormal normal_idx_b
						normal_c = refMod.Getnormal normal_idx_c
							
						normal_check = (normal_a + normal_b + normal_c) / 3.0
						
						vert_a = (getvert obj meshFace[1]) * obj.transform
						vert_b = (getvert obj meshFace[2]) * obj.transform
						vert_c = (getvert obj meshFace[3]) * obj.transform
						
						edge_a = normalize(vert_b - vert_a)
						edge_b = normalize(vert_c - vert_a)
						
						normal_face = cross edge_a edge_b
				
						check_val = dot normal_check normal_face
						
						if check_val < 0.0 then (
						
							append flipfaces i
							--print "flip"
							--print i	
						)
					)
					
					collapsestack obj
					
					for flip in flipfaces do (
					
						meshFace = getface obj flip
						meshTVFace = gettvface obj flip
						meshNewFace = [meshFace[2],meshFace[1],meshFace[3]]
						meshNewTVFace = [meshTVFace[2],meshTVFace[1],meshTVFace[3]]
					
						settvface obj flip meshNewTVFace
						setface obj flip meshNewFace		
					)
					
					update obj
					
					if nomod == false then (
					
						addmodifier obj (Edit_Normals())
					)
				)
			)
		)		
	)
	
	-- Lower Detail node controls:
	on btnSelectLoDef pressed do
	(
		local toSel = getLowerDetail selection[1]
		if (isValidNode toSel) do 
		(
			select toSel
			updateCtrls()
		)
	)
	on btnPickLoDef picked selLoDef do
	(
		local selObj = selection[1]

		if (checkSameContainer selLoDef selobj) do 
		(
			-- Get object's current parents-list, and RotAlt node if set:
			local objParents = #()
			RsSceneLink.GetParents LinkType_CombinerMesh selObj &parents
			objParents[1] = selLoDef
			
			-- Replace parent-link data:
			RsSceneLink.RemoveContainer LinkType_CombinerMesh selObj
			for obj in (makeUniqueArray objParents) where (isValidNode obj) do 
			(
				RsSceneLink.SetParent LinkType_CombinerMesh selObj obj append:True
			)
			
			select selObj
			updateCtrls()
		)
	)
	on btnRemoveLoDef pressed do
	(
		local selObj = selection[1]
		
		-- Get current parents-list:
		local parents = #()
		RsSceneLink.GetParents LinkType_CombinerMesh selObj &parents

		-- Clear object's parent-data:
		RsSceneLink.RemoveContainer LinkType_CombinerMesh selObj
		
		-- Add rest of parents back: (i.e. the Rotable Alternative mesh)
		for n = 2 to parents.count where (isValidNode parents[n]) do 
		(
			RsSceneLink.SetParent LinkType_CombinerMesh selObj parents[n] append:True
		)
		
		select selObj
		updateCtrls()
	)
	
	on btnSwapLoDefs pressed do 
	(
		local selObj = selection[1]
		
		-- Get current parents-list:
		local parents = #()
		RsSceneLink.GetParents LinkType_CombinerMesh selObj &parents
		
		-- Re-order parent-list:
		parents = for obj in #(parents[2], parents[1]) where (isValidNode obj) collect obj
		
		-- Clear object's parent-data:
		RsSceneLink.RemoveContainer LinkType_CombinerMesh selObj
		
		-- Add parent-links back in new order: 
		for obj in parents do 
		(
			RsSceneLink.SetParent LinkType_CombinerMesh selObj obj append:True
		)
		
		select selObj
		updateCtrls()
	)
	
	-- Higher Detail node controls:
	on btnSelectHiDef pressed do
	(
		local toSel = getHigherDetail selection[1]
		
		if (isValidNode toSel) do 
		(
			select toSel
			updateCtrls()
		)
	)
	on btnPickHiDef picked selHiDef do
	(
		local selobj = selection[1]		
		
		if (checkSameContainer selHiDef selobj) do 
		(
			-- Replace HiDef object's parent-link(s)
			RsSceneLink.SetParent LinkType_CombinerMesh selHiDef selobj
			
			select selObj
			updateCtrls()
		)	
	)
	on btnRemoveHiDef pressed do
	(
		local selObj = selection[1]
		local selHiDef = getHigherDetail selObj
		
		if (isValidNode selHiDef) do
		(
			local hiDefParents = #()
			RsSceneLink.GetParents LinkType_CombinerMesh selHiDef &hiDefParents
			
			-- Clear HiDef-object's parents:
			RsSceneLink.RemoveContainer LinkType_CombinerMesh selHiDef
			
			-- Restore the rest of the HiDef object's parents:
			hiDefParents = for obj in hiDefParents where (isValidNode obj) and (obj != selObj) collect obj
			for obj in hiDefParents do 
			(
				RsSceneLink.SetParent LinkType_CombinerMesh selHiDef obj append:True
			)
			
			select selobj
			updateCtrls()
		)
	)
	
	-- Rotatable-Alternative Lower Detail node controls:
	on btnSelectRotAlt pressed do
	(
		local toSel = getLowerDetail selection[1] rotAlt:True
		if (isValidNode toSel) do 
		(
			select toSel
			updateCtrls()
		)
	)
	on btnPickRotAlt picked selRotAlt do
	(
		local selObj = selection[1]		

		if (checkSameContainer selRotAlt selObj) do 
		(
			-- Get current object-parents: (low-def objects)
			local objParents = #()
			RsSceneLink.GetParents LinkType_CombinerMesh selObj &objParents
			objParents[2] = selRotAlt
			
			-- Replace parent-link data:
			RsSceneLink.RemoveContainer LinkType_CombinerMesh selObj
			for obj in (makeUniqueArray objParents) where (isValidNode obj) do 
			(
				RsSceneLink.SetParent LinkType_CombinerMesh selObj obj append:True
			)
			
			select selObj
			updateCtrls()
		)
	)
	on btnRemoveRotAlt pressed do
	(
		local selobj = selection[1]
		
		-- Get current object-parents: (low-def objects)
		local objParents = #()
		RsSceneLink.GetParents LinkType_CombinerMesh selObj &objParents
		
		if (isValidNode objParents[2]) do 
		(
			-- Clear HiDef-object's parents:
			RsSceneLink.RemoveContainer LinkType_CombinerMesh selObj
			
			-- Add low-def parent-link back:
			if (isValidNode objParents[1]) do 
			(
				RsSceneLink.SetParent LinkType_CombinerMesh selObj objParents[1]
			)
			
			select selobj
			updateCtrls()
		)
	)
	
	-- Rollout open event:
	on RsComboLodSourceRoll open do 
	(
		updateCtrls doRedraw:False
		
		-- Add the selection change handler:
		callbacks.addscript #selectionSetChanged "RsComboLodSourceRoll.updateCtrls doRedraw:False" id:#ComboLodSourceEditorSelect
	)
	
	-- Rollout close event:
	on RsComboLodSourceRoll close do 
	(
		-- Remove the selection change handler:
		callbacks.removescripts id:#ComboLodSourceEditorSelect
	)
	
	-- Save rolled-up state:
	on RsComboLodSourceRoll rolledUp down do 
	(
		RsSettingWrite "RsComboLodSourceRoll" "rollup" (not down)
	)
)

--CreateDialog RsComboLodSourceRoll width:236
