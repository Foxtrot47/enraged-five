-- Author : Mike Wilson (mike.wilson@rockstarnorth.com)

-- cutfileLoader plugin

filein "pipeline/ui/cutscenelight.ms"

try(CloseRolloutFloater RsAnimationToolsFloater)catch()

rollout RsCutsceneLightToolsRollout "Cutscene Lights"
(
	button btnImportLight "Import" width:200
	button btnFixupLight "Fixup Lightxml" width:200
	button btnClearAnimated "Clear 'animated' flags" width:200
	button btnReloadLight "Reload Lights" width:200
	button btnExportLight "Export *.lightxml / *.anim" width:200
	button btnZipCutscene "Zip cutscene" width:200
	button btnBuildCutscene "Build cutscene" width:200
	button btnPreviewCutscene "Preview cutscene" width:200
	checkbox chkPerforce "Perforce Integration" checked:true pos:[75,165]
	timer tmrReloadLights "tmrReloadLights" interval:10

	on chkPerforce changed theState do (
		SetPerforceIntegration chkPerforce.checked
	)
	
	on btnClearAnimated pressed do (
		for obj in objects do
		(
			RsUserProp_Clear obj "animated"
		)
	)
	
	on btnFixupLight pressed do (
		if g_lastCutFile != undefined and g_lastLightFile != undefined then
		(
			local fixupExecutable = (systemTools.getEnvVariable "RS_TOOLSROOT") + "\\bin\\anim\\LightXmlFixup.exe"
			
			if g_perforce == true then
			(
				-- Sync exe and light file before checking it out
				gRsPerforce.sync #(fixupExecutable, g_lastLightFile)
				if gRsPerforce.edit g_lastLightFile == false then
				(
					gRsPerforce.add g_lastLightFile
				)
			)
			
			if doesFileExist fixupExecutable then
			(
				-- Call fixup exe
				HiddenDOSCommand (fixupExecutable + " " + g_lastLightFile) ExitCode:&result
				
				if result != 0 then
				(
					messageBox ("Non-zero return code for executable - '" + fixupExecutable + "', contact tools.") title:"Error fixing up light file"
				)
				
				-- reload the file
				LoadCutXMLFile g_lastCutFile
				ImportLight g_lastLightFile
			)
			else
			(
				messageBox ("Missing executable - '" + fixupExecutable + "', even after syncing from perforce") title:"Error, missing fixup executable"
			)
		)
	)
	
	on btnImportLight pressed do (
		-- Disconnect form live edit
		if lightEditorInstanceNG.isConnected == true do
			lightEditorInstanceNG.OnDisconnect()
		
		SetInteractionMode true
		if g_LastDir == undefined then
		(
			g_LastDir = (RsConfigGetAssetsDir() + "cuts\\")
		)
		folder = getSavePath caption:"Open Light XML Folder" initialDir:g_lastDir
		if folder != undefined do
		(
			if doesFileExist (folder + "\\data.cutxml") then
			(
				g_lastDir = folder + "\\"
				LoadCutXMLFile (folder + "\\data.cutxml")
				files = getFiles (folder + "\\*.lightxml")
				if files.count == 1 then
				(
					--Load the file
					ImportLight files[1]
				)
				else if files.count > 1 then
				(
					--Show dialog to choose which file
					f = getOpenFileName caption:"Open LightXML File:" types:"Light XML Files(*.lightxml)|*.lightxml" filename:g_lastDir
					if f != undefined then
					(
						ImportLight f
					)
				)
				else
				(
					messageBox "No valid 'lightxml' exists in that directory" title:"Error, no files found"
				)
			)
			else
			(
				messageBox "No valid 'data.cutxml' exists in that directory" title:"Error, no files found"
			)
		)
	)
	
	on btnReloadLight pressed do (
		if (querybox "Are you sure you want reload lights? \n(this will overwrite any unsaved changes)" title:"Are you sure?") do
		(
			if g_lastCutFile != undefined then
			(
				LoadCutXMLFile g_lastCutFile
				if g_lastLightFile != undefined then
				(
					ImportLight g_lastLightFile
				)
			)
		)
	)
	
	on btnExportLight pressed do (
		SetInteractionMode true
		f = getSavePath initialDir:g_lastDir
		if f != undefined then
		(
			g_lastDir = f
			ExportLight g_lastDir
		)
	)
	
	on btnBuildCutscene pressed do (
		SetInteractionMode true
		f = getSavePath initialDir:g_lastDir
		if f != undefined then
		(
			g_lastDir = f
			BuildCutscene g_lastDir false

		)
	)

	on btnZipCutscene pressed do (
		SetInteractionMode true
		f = getSavePath initialDir:g_lastDir
		if f != undefined then
		(
			g_lastDir = f
			BuildZip g_lastDir false false

		)
	)

	on btnPreviewCutscene pressed do (
		SetInteractionMode true
		f = getSavePath initialDir:g_lastDir
		if f != undefined then
		(
			g_lastDir = f
			BuildCutscene g_lastDir true

		)
	)
	
	on tmrReloadLights tick do
	(
		tmrReloadLights.interval = 5000
		if g_lastCutFile != undefined and g_lastLightFile != undefined do
		(
			--check if our cutfile/light files are out of date?
			local outOfDate = false
			
			local cutFileInfo = dotNetObject "System.IO.FileInfo" g_LastCutFile
			cutfileLastWrite = cutFileInfo.LastWriteTime
			if ((g_DateTime.Compare g_cutFileLastWrite cutfileLastWrite) < 0) then
				outOfDate = true
			else
			(
				local lightFileInfo = dotNetObject "System.IO.FileInfo" g_LastLightFile
				lightfileLastWrite = lightFileInfo.LastWriteTime
				if ((g_DateTime.Compare g_lightFileLastWrite lightfileLastWrite) < 0) then
					outOfDate = true
			)
			
			if outOfDate do
			(
				--prompt to reload files
				if (queryBox "CutFile and/or LightFile are out of date, would you like to reload?" title:"Light data is out of date" beep:true) then
				(
					LoadCutXMLFile g_lastCutFile
					ImportLight g_lastLightFile
				)
				else
				(
					local cutFileInfo = dotNetObject "System.IO.FileInfo" g_LastCutFile
					g_cutFileLastWrite = cutFileInfo.LastWriteTime
					local lightFileInfo = dotNetObject "System.IO.FileInfo" g_LastLightFile
					g_lightFileLastWrite = lightFileInfo.LastWriteTime
				)
			)
		)
	)
	
	on RsCutsceneLightToolsRollout open do 
	(
		if g_lastDir == undefined do g_lastDir = (RsConfigGetAssetsDir() + "cuts/")
	)
)

rollout RsCutsceneLightToolsHelperRollout "Cutscene Lights Helpers"
(
	button btnLightsToRAGELights "Lights -> RAGELights" width:200
	button btnImportExportBuild "Import -> Export -> Build" width:200
	spinner spnCreateExportBuild "" pos:[40,70] width:35 height:25 range:[0,100,0] type:#integer
	button btnCreateExportBuild "Create -> Export -> Build" pos:[80,60] width:165
	button btnBatchCreateExportBuild "Batch Create -> Export -> Build" pos:[80,85] width:165
	button btnBatchExport "Batch Export" width:200
	button btnBatchZip "Batch Zip" width:200
	button btnBatchBuild "Batch Build" width:200
	spinner spnBlendOut "" pos:[40,190] width:35 height:25 range:[0,5000,0] type:#integer
	button btnBlendOut "Blend-Out Selected" pos:[80,185] width:165
	button btnDupeLights "Duplicate Selected Light(s)" width:200
	button btnValidateLights "Make light names unique" width:200
	button btnCamToLight "Snap Camera to Light" width:200
	button btnLightToCam "Snap Light to Camera" width:200
	
	local mainForm
	local checkListBox
	local renameCheckbox
	local radioButtonCopy
	local radioButtonInstance
	local radioButtonReference
	
	on btnCamToLight pressed do 
	(
		local lightArray = for obj in selection where (isKindOf $ light) collect obj
		if lightArray.count != 1 then
		(
			messagebox "Error: Please select a single light object" title:"Error: select a light first"
		)
		else if gw.IsPerspView() then
		(
			local lightObj = lightArray[1]
			if not viewport.setTM(Inverse(lightObj.transform)) then
				messagebox "Error: Failed to set the viewport Transformation Matrix" title:"Error"
			else
				completeredraw()
		)
		else
		(
			messagebox "Error: Active viewport must be in perspective mode" title:"Error: Viewport is not a perspective viewport"
		)
	)
	
	on btnLightToCam pressed do 
	(
		local lightArray = for obj in selection where (isKindOf $ light) collect obj
		if lightArray.count != 1 then
		(
			messagebox "Error: Please select a single light object" title:"Error: select a light first"
		)
		else if gw.IsPerspView() then
		(
			local lightObj = lightArray[1]
			lightObj.transform = Inverse(viewport.getTM())
		)
		else
		(
			messagebox "Error: Active viewport must be in perspective mode" title:"Error: Viewport is not a perspective viewport"
		)
	)
	
	fn ConvertLightsToRAGELights =
	(	
		for node in $objects do
		(
			if superclassof node == light and classof node != RageLight then
			(
				try
				(
					r = RageLight()
					r.name = node.name
					r.CopyFrom node
					return 1
				)
				catch()
			)
		)
		return 0
	)
	
	on btnLightsToRAGELights pressed do (
		
		--NOTE:  When converting all of the Max lights in the scene via this script, disable the prompt within RageLight.ms
		--asking users to convert their current selection.   If RageLight converts the selection
		--internally, a MaxScript exception will occur here because we've changed lights from underneath the process.
		--isConversionRunning is a global declared within RageLight.ms
		isConversionRunning = true
		while (ConvertLightsToRAGELights() == 1) do()		
		isConversionRunning = false
	)

	fn OnBatchExportClick = 
	(
		scenes = #()
		for index=0 to checkListBox.CheckedItems.Count-1 do
		(
			append scenes checkListBox.CheckedItems.item[index]
		)
		
		ExportCutsceneLightingScenes scenes

		mainForm.Close()
	)
	
	fn OnAutoSelectClick = 
	(
		local safeDir = RsMakeSafeSlashes g_LastDir
		while safeDir[safeDir.count] == "/" do
			safeDir = substring safeDir 1 (safeDir.count-1)
		local concatList = GetConcatList safeDir
		local concatFiles = if concatList!=undefined then (filterstring (concatList) ";") else #()
		
		for i=0 to (checkListBox.Items.Count-1) do
		(
			local notFound = true
			local patternToMatch = RsMakeSafeSlashes ((checkListBox.Items.Item(i))+"*")
			for concatFile in concatFiles while notFound do
			(
				concatFile = substituteString (RsMakeSafeSlashes concatFile) "$(assets)/" (RsConfigGetAssetsDir())
				if matchPattern concatFile pattern:patternToMatch do
				(
					notFound = false
					print ("Item: '" + (checkListBox.Items.Item(i)) + "' was checked")
					checkListBox.SetItemChecked i true
				)
			)
			if matchPattern (RsMakeSafeSlashes (g_LastDir+"\\")) pattern:patternToMatch do
			(
				print ("Item: '" + (checkListBox.Items.Item(i)) + "' was checked")
				checkListBox.SetItemChecked i true
			)
		)
	)
	
	fn OnBatchBuildClick = 
	(
		scenes = #()
		for index=0 to checkListBox.CheckedItems.Count-1 do
		(
			append scenes checkListBox.CheckedItems.item[index]
		)
		
		print "--START--"
		
		for scene in scenes do
		(
			msg = "\n*" + scene
			print msg
			
			if BuildCutscene scene false == false then
			(
				msg = "Error: '" + scene + "' build fail."
				print msg
			)
			else
			(
				msg = "'" + scene + "' build success."
				print msg
			)
		)
		
		print "--END--"

		mainForm.Close()
	)
	
	fn OnBatchZipClick = 
	(
		scenes = #()
		for index=0 to checkListBox.CheckedItems.Count-1 do
		(
			append scenes checkListBox.CheckedItems.item[index]
		)
		
		print "--START--"
		
		for scene in scenes do
		(
			msg = "*" + scene
			print msg
			
			if BuildZip scene false false == false then
			(
				msg = "Error: '" + scene + "' build fail."
				print msg
			)
			else
			(
				msg = "'" + scene + "' build success."
				print msg
			)
		)
		
		print "--END--"

		mainForm.Close()
	)

	fn OnBatchCreateExportBuildClick = 
	(
		scenes = #()
		for index=0 to checkListBox.CheckedItems.Count-1 do
		(
			append scenes checkListBox.CheckedItems.item[index]
		)
		
		print "--START--"
		
		CreateExportBuildScenes scenes spnCreateExportBuild.value

		print "\n--END--"
	)
	
	fn OnDupeClick = 
	(
		--collect all selection that are lights
		local lightsToCopy = for node in $selection where superclassOf node == light collect node
		
		targetClips = #()
		for index=0 to checkListBox.CheckedItems.Count-1 do
		(
			append targetClips (getNodeByName(checkListBox.CheckedItems.item[index]))
		)
		format "Copying lights: %\n" lightsToCopy as string
		format "Copying to: %\n" targetClips as string
		
		if radioButtonCopy.Checked then style = #copy
		else if radioButtonInstance.Checked then style = #instance
		else if radioButtonReference.Checked then style = #reference
		DuplicateLights lightsToCopy targets:targetClips rename:renameCheckbox.Checked copyStyle:style
		print "--DUPLICATED--"
		mainForm.Close()
	)
	
	on btnBlendOut pressed do (
		for node in $selection do
		(
			if classof node == RageLight then
			(
				deleteKeys node.lightIntensity.Controller #allKeys
				
				with animate on
				(
					at time spnBlendOut.value node.lightIntensity = node.lightIntensity
					at time animationRange.end node.lightIntensity = 0
				)
				
				if getUserProp node "animated" == undefined then
				(
					setUserProp node "animated" "true"
				)
			)
		)
	)
	
	on btnDupeLights pressed do (
		mainForm = dotNetObject "maxCustomControls.maxForm"
		mainForm.Size = dotNetObject "System.Drawing.Size" 380 300
		mainForm.Text = "Copy Lights to AutoClip"
		infoLabel = dotNetObject "System.Windows.Forms.Label"
		infoLabel.Location = dotNetObject "System.Drawing.Point" 3 3
		infoLabel.size = dotNetObject "System.Drawing.Size" 250 20
		infoLabel.Text = "Select AutoClip(s) to copy selected lights into:"
		mainForm.controls.Add(infoLabel)
		checkListBox = dotNetObject "System.Windows.Forms.CheckedListBox"
		checkListBox.Location = dotNetObject "System.Drawing.Point" 3 25
		checkListBox.size = dotNetObject "System.Drawing.Size" 230 200
		checkListBox.ScrollAlwaysVisible = true
		checkListBox.CheckOnClick = true
		mainForm.controls.Add(checkListBox)
		
		groupBox1 = dotNetObject "System.Windows.Forms.GroupBox"
		groupBox1.Location = dotNetObject "System.Drawing.Point" 250 25
		groupBox1.Size = dotNetObject "System.Drawing.Size" 100 100
		groupBox1.Text = "Copy Style:"
		radioButtonCopy = dotNetObject "System.Windows.Forms.RadioButton"
		radioButtonCopy.Location = dotNetObject "System.Drawing.Point" 5 20
		radioButtonCopy.Size = dotNetObject "System.Drawing.Size" 80 20
		radioButtonCopy.Text = "Copy"
		radioButtonCopy.Checked = true
		radioButtonInstance = dotNetObject "System.Windows.Forms.RadioButton"
		radioButtonInstance.Location = dotNetObject "System.Drawing.Point" 5 40
		radioButtonInstance.Size = dotNetObject "System.Drawing.Size" 80 20
		radioButtonInstance.Text = "Instance"
		radioButtonReference = dotNetObject "System.Windows.Forms.RadioButton"
		radioButtonReference.Location = dotNetObject "System.Drawing.Point" 5 60
		radioButtonReference.Size = dotNetObject "System.Drawing.Size" 80 20
		radioButtonReference.Text = "Reference"
		groupBox1.Controls.Add(radioButtonCopy)
		groupBox1.Controls.Add(radioButtonInstance)
		groupBox1.Controls.Add(radioButtonReference)
		mainForm.controls.Add(groupBox1)

		mainForm.MaximumSize = dotNetObject "System.Drawing.Size" 380 300
		mainForm.MinimumSize = dotNetObject "System.Drawing.Size" 380 300
		for node in objects where matchPattern node.name pattern:"AutoClip*" do
		(
			checkListBox.Items.Add node.name
		)
		renameCheckbox = dotNetObject "System.Windows.Forms.CheckBox"
		renameCheckbox.Size = dotNetObject "System.Drawing.Size" 100 20
		renameCheckbox.Location = dotNetObject "System.Drawing.Point" 100 235
		renameCheckbox.Text = "Rename Lights"
		renameCheckbox.Checked = true
		mainForm.controls.Add(renameCheckbox)
		connectButton = dotNetObject "Button"
		connectButton.Width = 80
		connectButton.Height = 20
		connectButton.Location  = dotNetObject "System.Drawing.Point" 3 235
		connectButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		connectButton.Text = "Duplicate"
		dotNet.AddEventHandler connectButton "Click" OnDupeClick
		mainForm.controls.Add(connectButton)
		
		mainForm.showModeless()
	)
	
	on btnBatchExport pressed do (
	
		SetInteractionMode false
		
		mainForm = dotNetObject "maxCustomControls.maxForm"
		mainForm.Size = dotNetObject "System.Drawing.Size" 500 365
		mainForm.Text = "Batch Export Scenes"
		checkListBox = dotNetObject "System.Windows.Forms.CheckedListBox"
		checkListBox.Location = dotNetObject "System.Drawing.Point" 3 3
		checkListBox.size = dotNetObject "System.Drawing.Size" 475 300
		checkListBox.ScrollAlwaysVisible = true
		checkListBox.CheckOnClick = true
		mainForm.controls.Add(checkListBox)
		mainForm.MaximumSize = dotNetObject "System.Drawing.Size" 500 365
		mainForm.MinimumSize = dotNetObject "System.Drawing.Size" 500 365

		subDirs = getDirectories (RsConfigGetAssetsDir() + "cuts/*")

		for dir in subDirs do
		(
			if not matchPattern dir pattern:"*\\!!*" do
				checkListBox.Items.Add dir
		)

		autoSelectButton = dotNetObject "Button"
		autoSelectButton.Width = 200
		autoSelectButton.Height = 20
		autoSelectButton.Location  = dotNetObject "System.Drawing.Point" 3 300
		autoSelectButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		autoSelectButton.Text = "Select scenes in concat"
		dotNet.AddEventHandler autoSelectButton "Click" OnAutoSelectClick
		mainForm.controls.Add(autoSelectButton)
		
		connectButton = dotNetObject "Button"
		connectButton.Width = 60
		connectButton.Height = 20
		connectButton.Location  = dotNetObject "System.Drawing.Point" 213 300
		connectButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		connectButton.Text = "Export"
		dotNet.AddEventHandler connectButton "Click" OnBatchExportClick
		mainForm.controls.Add(connectButton)

		mainForm.showModeless()
	)
	
	on btnBatchBuild pressed do (
	
		mainForm = dotNetObject "maxCustomControls.maxForm"
		mainForm.Size = dotNetObject "System.Drawing.Size" 500 365
		mainForm.Text = "Batch Build Scenes"
		checkListBox = dotNetObject "System.Windows.Forms.CheckedListBox"
		checkListBox.Location = dotNetObject "System.Drawing.Point" 3 3
		checkListBox.size = dotNetObject "System.Drawing.Size" 475 300
		checkListBox.ScrollAlwaysVisible = true
		checkListBox.CheckOnClick = true
		mainForm.controls.Add(checkListBox)
		mainForm.MaximumSize = dotNetObject "System.Drawing.Size" 500 365
		mainForm.MinimumSize = dotNetObject "System.Drawing.Size" 500 365

		subDirs = getDirectories (RsConfigGetAssetsDir() + "cuts/*")

		for dir in subDirs do
		(
			dirBits = filterString dir "\\"
			
			newDir = ""
			for i = 1 to dirBits.count do
			(
				newDir = newDir + dirBits[i]
				
				if i != dirBits.count then
				(
					newDir = newDir + "\\"
				)
			)
			
			checkListBox.Items.Add newDir
		)
		
		autoSelectButton = dotNetObject "Button"
		autoSelectButton.Width = 200
		autoSelectButton.Height = 20
		autoSelectButton.Location  = dotNetObject "System.Drawing.Point" 3 300
		autoSelectButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		autoSelectButton.Text = "Select scenes in concat"
		dotNet.AddEventHandler autoSelectButton "Click" OnAutoSelectClick
		mainForm.controls.Add(autoSelectButton)
		
		connectButton = dotNetObject "Button"
		connectButton.Width = 60
		connectButton.Height = 20
		connectButton.Location  = dotNetObject "System.Drawing.Point" 213 300
		connectButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		connectButton.Text = "Build"
		dotNet.AddEventHandler connectButton "Click" OnBatchBuildClick
		mainForm.controls.Add(connectButton)

		mainForm.showModeless()
	)
	
	on btnBatchZip pressed do (
	
		mainForm = dotNetObject "maxCustomControls.maxForm"
		mainForm.Size = dotNetObject "System.Drawing.Size" 500 365
		mainForm.Text = "Batch Zip Scenes"
		checkListBox = dotNetObject "System.Windows.Forms.CheckedListBox"
		checkListBox.Location = dotNetObject "System.Drawing.Point" 3 3
		checkListBox.size = dotNetObject "System.Drawing.Size" 475 300
		checkListBox.ScrollAlwaysVisible = true
		checkListBox.CheckOnClick = true
		mainForm.controls.Add(checkListBox)
		mainForm.MaximumSize = dotNetObject "System.Drawing.Size" 500 365
		mainForm.MinimumSize = dotNetObject "System.Drawing.Size" 500 365

		subDirs = getDirectories (RsConfigGetAssetsDir() + "cuts/*")

		for dir in subDirs do
		(
			dirBits = filterString dir "\\"
			
			newDir = ""
			for i = 1 to dirBits.count do
			(
				newDir = newDir + dirBits[i]
				
				if i != dirBits.count then
				(
					newDir = newDir + "\\"
				)
			)
			
			checkListBox.Items.Add newDir
		)
		
		autoSelectButton = dotNetObject "Button"
		autoSelectButton.Width = 200
		autoSelectButton.Height = 20
		autoSelectButton.Location  = dotNetObject "System.Drawing.Point" 3 300
		autoSelectButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		autoSelectButton.Text = "Select scenes in concat"
		dotNet.AddEventHandler autoSelectButton "Click" OnAutoSelectClick
		mainForm.controls.Add(autoSelectButton)
		
		connectButton = dotNetObject "Button"
		connectButton.Width = 60
		connectButton.Height = 20
		connectButton.Location  = dotNetObject "System.Drawing.Point" 213 300
		connectButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		connectButton.Text = "Zip"
		dotNet.AddEventHandler connectButton "Click" OnBatchZipClick
		mainForm.controls.Add(connectButton)

		mainForm.showModeless()
	)
	
	on btnImportExportBuild pressed do (
	
		SetInteractionMode true
		f = getOpenFileName caption:"Open Light XML File:" types:"Light XML Files(*.lightxml)|*.lightxml" filename:g_lastDir
		if f != undefined then
		(
			g_lastDir = GetDirFromFilename f
			if ImportLight f == true then
			(
				if ExportLight g_lastDir == true then
				(
					BuildCutscene g_lastDir false
				)
			)
		)	
	)

	on btnBatchCreateExportBuild pressed do (
	
		mainForm = dotNetObject "maxCustomControls.maxForm"
		mainForm.Size = dotNetObject "System.Drawing.Size" 500 365
		mainForm.Text = "Batch Create - Export - Build Scenes"
		checkListBox = dotNetObject "System.Windows.Forms.CheckedListBox"
		checkListBox.Location = dotNetObject "System.Drawing.Point" 3 3
		checkListBox.size = dotNetObject "System.Drawing.Size" 475 300
		checkListBox.ScrollAlwaysVisible = true
		checkListBox.CheckOnClick = true
		mainForm.controls.Add(checkListBox)
		mainForm.MaximumSize = dotNetObject "System.Drawing.Size" 500 365
		mainForm.MinimumSize = dotNetObject "System.Drawing.Size" 500 365

		subDirs = getDirectories (RsConfigGetAssetsDir() + "cuts/*")

		for dir in subDirs do
		(
			dirBits = filterString dir "\\"
			
			newDir = ""
			for i = 1 to dirBits.count do
			(
				newDir = newDir + dirBits[i]
				
				if i != dirBits.count then
				(
					newDir = newDir + "\\"
				)
			)
			
			checkListBox.Items.Add newDir
		)
		
		connectButton = dotNetObject "Button"
		connectButton.Width = 60
		connectButton.Height = 20
		connectButton.Location  = dotNetObject "System.Drawing.Point" 3 300
		connectButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		connectButton.Text = "Build"
		dotNet.AddEventHandler connectButton "Click" OnBatchCreateExportBuildClick
		mainForm.controls.Add(connectButton)

		mainForm.showModeless()
	)
	
	on btnCreateExportBuild pressed do (
	
		SetInteractionMode true
		f = getSavePath initialDir:g_lastDir
		if f != undefined then
		(
			a = #(f)
			CreateExportBuildScenes a spnCreateExportBuild.value
		)
	)
	
	on btnValidateLights pressed do (
			
		local dir = getSavePath caption:"Open Light XML Folder" initialDir:g_lastDir
		g_lastDir = dir
		dir = RsMakeSafeSlashes dir
		while dir[dir.count] == "/" do
			dir = substring dir 1 (dir.count-1)
		ValidateUniqueLights dir
		print "---DONE---"
	)
)

global RsCutsceneLightToolsFloater = newRolloutFloater "Cutscene Lights" 300 560
AddRollout RsCutsceneLightToolsRollout RsCutsceneLightToolsFloater
AddRollout RsCutsceneLightToolsHelperRollout RsCutsceneLightToolsFloater

