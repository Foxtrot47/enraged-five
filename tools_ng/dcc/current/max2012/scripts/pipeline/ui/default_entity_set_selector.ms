--
-- File:: pipeline/ui/default_entity_set_selector.ms
-- Description::
--
-- Author:: Jonny Rivers <jonny.rivers@rockstarnorth.com>
-- Date:: 18 October 2012
--
try (destroyDialog RsDefaultEntitySetSelectorRoll) catch ()

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------

rollout RsDefaultEntitySetSelectorRoll "Default Entity Set Selector" width:500 height:205
(
	-------------------------------------------------------------------------
	-- Locals
	-------------------------------------------------------------------------
	local idxDefaultEntitySets = getattrindex "Gta Object" "Default Entity Sets"
	local entitySetHelper = undefined
	local selectedMiloRefObject = undefined
	
	-------------------------------------------------------------------------
	-- UI Widgets
	-------------------------------------------------------------------------
	hyperlink 			lnkHelp					"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Default_Entity_Set_Selector" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	label					lblSelectedObject		"Selected object:" align:#left offset:[1,0] across:2
	edittext 			etSelectedObject		"" readonly:true width:300 align:#right offset:[0,-3]
	multilistbox 		mlbEntitySets	 		"Available Entity Sets:" item:#() height:8
	button				btnApply				"Apply" width:100 align:#right
	
	-------------------------------------------------------------------------
	-- Methods
	-------------------------------------------------------------------------
	
	fn UpdateFromSelection =
	(
		local selectionIsValid = false
		if( $selection.count == 0 ) then
		(
			etSelectedObject.Text = "nothing selected"
		)
		else if( $selection.count == 1 ) then
		(
			local selectedObject = $selection[1]
			if (isRSref selectedObject) and (findstring selectedObject.name "_milo_" != undefined) and (getattrclass selectedObject == "Gta Object") then 
			(
				selectedMiloRefObject = selectedObject
				etSelectedObject.Text = selectedObject.Name
			
				mlbEntitySets.Items = entitySetHelper.GetInteriorEntitySetNames selectedObject.filename selectedObject.objectName
			
				-- take into account what is currently set
				local combinedEntitySetNames = getattr selectedObject idxDefaultEntitySets
				local entitySetNames = filterstring combinedEntitySetNames ";"
				local defaultEntitySetNameIndices = #()
				for entitySetName in entitySetNames do
				(
					local listBoxIndex = findItem mlbEntitySets.Items entitySetName
					if( listBoxIndex >= 1 ) then
					(
						appendifunique defaultEntitySetNameIndices listBoxIndex
					)
				)
				mlbEntitySets.Selection = defaultEntitySetNameIndices

				selectionIsValid = true
			)
			else
			(
				etSelectedObject.Text = "object selected is not an RS Ref to a MILO"
			)
		)
		else
		(
			etSelectedObject.Text = "multiple objects selected"
		)
		
		btnApply.enabled = selectionIsValid
		if( not selectionIsValid ) do
		(
			mlbEntitySets.Items = #()
			mlbEntitySets.Selection = #()
		)
	)

	-- name: Startup
	-- desc: Resets the rollout to its default state
	fn Startup = 
	(
		entitySetHelper = dotNetObject "RSG.MaxUtils.Interiors.EntitySetHelper"
		entitySetHelper.Startup()
		
		UpdateFromSelection()
		callbacks.addScript #selectionSetChanged "RsDefaultEntitySetSelectorRoll.UpdateFromSelection()" id:#defaultEntitySetSelectorSelectionSetChanged
	)
	
	fn Shutdown =
	(
		callbacks.removeScripts id:#defaultEntitySetSelectorSelectionSetChanged

		entitySetHelper.Shutdown()
	)
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------
	
	-- event: The rollout is opening
	on RsDefaultEntitySetSelectorRoll open do
	(
		Startup()
	)
	
	on RsDefaultEntitySetSelectorRoll close do
	(
		Shutdown()
	)
	
	on btnApply pressed do
	(
		if( selectedMiloRefObject != undefined ) then
		(
			local combinedEntitySetNames = ""
			
			local selNames = for selNum = mlbEntitySets.selection collect (mlbEntitySets.items[selNum])
			for selNameIndex = 1 to selNames.count do
			(
				combinedEntitySetNames += selNames[selNameIndex]
				if(selNameIndex < selNames.count ) then combinedEntitySetNames += ";"
			)
			
			setattr selectedMiloRefObject idxDefaultEntitySets combinedEntitySetNames
		)
	)
)

-----------------------------------------------------------------------------
-- Entry
-----------------------------------------------------------------------------
createDialog RsDefaultEntitySetSelectorRoll

-- pipeline/ui/default_entity_set_selector.ms
