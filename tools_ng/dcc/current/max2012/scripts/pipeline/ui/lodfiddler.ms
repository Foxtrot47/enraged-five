 --
-- File:: pipeline/ui/lodfiddler.ms
-- Description:: LOD Fiddler
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
 rollout objlodset "LOD Tools" 	( 
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/LOD_Toolkit#LOD_Tools" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	spinner	 lodmult "LOD * " range:[0,100,1]
	spinner	 lodval "LOD value > " range:[1,3000,200]
	button		Lodchange	"Change LOD" width:133 height:26 across:2
	button		colourLOD "Colour LOD" width:133 height:26
	button		colourtxd "Colour TXD" width:133 height:26 across:2
	button		colourfaces "Colour Poly" width:133 height:26
	button selectobj "Select" width:133 height:26
	
	on selectobj pressed do
	(obtxdlist=#()
			for objc =1 to rootNode.children.count do 
				(
				-- ignore objects without Gta Object attribute class
				if (getattrclass rootNode.children[objc]) == "Gta Object" do 
					(
					If not isRefObj objc do (
					currentlod = getattr rootnode.children[objc] (getattrindex "Gta Object" "LOD distance")
					if currentlod >= lodval.value do
						(
						obname=(rootnode.children[objc])
						append obtxdList obname
						)
					)
					)
				)
				select obtxdlist
			)

	on lodchange pressed do
		(
			for lodloop = 1 to selection.count do
			(
				currentlod = getattr selection[lodloop] (getattrindex "Gta Object" "LOD distance")
				newlod = currentlod*lodmult.value
				setattr selection[lodloop] (getattrindex "Gta Object" "LOD distance") newlod
			)
		)
	on colourLOD pressed do
		(
			for lodloop = 1 to selection.count do
			(
				currentlod = getattr selection[lodloop] (getattrindex "Gta Object" "LOD distance")
				lodcol = currentlod as integer
				if currentlod<200 then 	selection[lodloop].wirecolor = [lodcol,lodcol,lodcol]
				if currentlod>200 then 	selection[lodloop].wirecolor = [0,lodcol,lodcol]
				if currentlod>299 then selection[lodloop].wirecolor = red
				if currentlod == 100 then selection[lodloop].wirecolor = green
				print currentlod
			)
		)

	on colourfaces pressed do
		(
			for lodloop = 1 to selection.count do
			(
				polycount=selection[lodloop].faces.count
				selection[lodloop].wirecolor = [polycount,0,0]
			)
		)

	on colourtxd pressed do
		
		(
		txdlist=#()
		txdcolour=#()
			for obj in rootNode.children do (
			-- ignore objects without Gta Object attribute class
			if (getattrclass obj) == "Gta Object" do (
				txdName = getattr obj (getattrindex "Gta Object" "TXD")
				if (findItem txdList txdName) == 0 do (
					append txdList txdName
				)
			)
			)
			---setsup colors

			for txdlists=1 to txdlist.count do
				(
				txdcolour[txdlists] = [(random 0 255),(random 0 255),(random 0 255)]
				)
			for obj =1 to selection.count do 
				(
				txdName = getattr selection[obj] (getattrindex "Gta Object" "TXD")
				objtxd=findItem txdList txdName
				selection[obj].wirecolor = txdcolour[objtxd]
				)
			)
			
	-- Save rolled-up state:
	on objlodset rolledUp down do 
	(
		RsSettingWrite "objlodset" "rollup" (not down)
	)
)