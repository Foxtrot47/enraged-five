--
-- RsSceneLinkEditor
-- Tool for editing all kinds of scene-link
--

try (DestroyDialog RsSceneLinkEditorRoll) catch()

-- Picker-tool:
Tool RsSceneLinkEditor_PickerTool 
(
	local TheTool, Mode, CurrObj
	local CurrentSel = #()
	local AllLodObjs = #()
	local NewLodObjs = #()
	local LodObjs = #()
	local LstObjNames, MaxLinkCount
	
	-- Set up initial values:
	on Start do 
	(
		TheTool = ::RsSceneLinkEditorRoll
		local LinkData = gRsSceneLinkTypeData[TheTool.ShowLinkType + 1]
		
		-- Get mode from temp global:
		Mode = ::RsSceneLinkEditor_PickerTool_Mode
		GlobalVars.Remove #RsSceneLinkEditor_PickerTool_Mode
		
		-- Get mode-specific values:
		LodObjs.Count = 0
		case Mode of 
		(
			#parents:
			(
				LstObjNames = TheTool.LstParentObjs
				MaxLinkCount = LinkData.MaxParents
			)
			#children:
			(
				LstObjNames = TheTool.LstChildObjs
				MaxLinkCount = LinkData.MaxChildren
			)
		)
		
		CurrentSel = for Obj in (GetCurrentSelection()) where (GetAttrClass Obj == "Gta Object") collect Obj
		CurrObj = CurrentSel[1]
		
		LodObjs = (TheTool.GetLodObjs CurrObj mode:mode)
		select LodObjs
		selectMore CurrObj
		
		-- Set list to include all objects in CurrObj's lod-parent/child hierarchy, for picker-filtering purposes:
		AllLodObjs = TheTool.GetAllLodObjs CurrObj
		
		TheTool.isPickActive = true
		
		-- Abort if MaxLinkCount is already maxed out:
		if ((MaxLinkCount == -1) or (LodObjs.Count < MaxLinkCount)) then (return #Continue) else (return #Stop)
	)
	
	-- Is obj a suitable parent/child for selected item?
	fn PickObjFilter Obj = 
	(
		-- Don't match for non-GtaObjs, or objects that have already been linked to this object:
		local okObj = (GetAttrClass Obj == "Gta Object") and ((FindItem AllLodObjs Obj) == 0)
		
		-- Don't match object if it's in CurrentSel, or is in a different container to anything in CurrentSel:
		if okObj do 
		(
			local ObjCont = (RsGetObjContainer Obj)
			for SelObj in CurrentSel while okObj do 
			(
				okObj = (SelObj != Obj) and ((RsGetObjContainer SelObj) == ObjCont)
			)
		)
		
		return okObj
	)
	
	-- Get list of appropriate objects under cursor:
	fn GetPickObjs = 
	(
		local PickObjs = BoxPickNode (Box2 (Mouse.Pos - 5) (Mouse.Pos + 5))
		PickObjs = for Obj in PickObjs where (PickObjFilter Obj) collect Obj
			
		return PickObjs
	)
	
	-- Set cursor to select-cross if hovering of pickable object, otherwise to default arrow;
	fn DoCursorFilter = 
	(
		local PointedObjs = GetPickObjs()
		if (PointedObjs.Count == 0) then (SetSysCur #Arrow) else (SetSysCur #Select)
	)
	
	-- Test objects under cursor whenever mouse is moved:
	on FreeMove do 
	(
		DoCursorFilter()
	)
	on MouseMove ClickNum do 
	(
		DoCursorFilter()
	)
	
	-- Called on mouse-click:
	on MousePoint ClickNum do 
	(
		-- Get valid objects from under cursor:
		local PickObjs = GetPickObjs()
		
		-- Skip if no valid objects were clicked:
		if (PickObjs.Count == 0) do return OK
		
		local NewPicked = False
		for PickedObj in PickObjs do 
		(
			if (appendIfUnique LodObjs PickedObj) do 
			(
				NewPicked = True
				append NewLodObjs PickedObj
				append AllLodObjs PickedObj
				
				selectmore PickedObj
			)
		)
		
		-- Update relevant name-list: (parents or children)
		local nameList = for Obj in LodObjs where (isValidNode Obj) collect Obj.Name
		sort nameList
		LstObjNames.items = nameList
		
		-- Stop if MaxLinkCount has been exceeded:
		if ((MaxLinkCount == -1) or (LodObjs.Count < MaxLinkCount)) then (return #Continue) else (return #Stop)
	)
	
	-- Add links for picked objects on tool-stop:
	on Stop do 
	(
		NewLodObjs = for Obj in NewLodObjs where (isValidNode Obj) collect Obj
		
		-- Add new scenelinks:
		case Mode of 
		(
			#parents:
			(
				for LodObj in NewLodObjs do 
				(
					RsSceneLink.SetParent (TheTool.ShowLinkType) CurrObj LodObj append:True
				)
			)
			#children:
			(
				for LodObj in NewLodObjs do 
				(
					RsSceneLink.SetParent (TheTool.ShowLinkType) LodObj CurrObj append:True
				)
			)
		)
		
		Select CurrObj
		FlashNodes LodObjs
		
		-- Clear no-longer-required values:
		CurrentSel.Count = 0
		AllLodObjs.Count = 0
		NewLodObjs.Count = 0
		LodObjs.Count = 0
		CurrObj = undefined
		TheTool = undefined
		LstObjNames = undefined
		
		SetSysCur #Select
	)
)

rollout RsSceneLinkEditorRoll "RsSceneLink Editor" width:300
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:RsSceneLinkEditorRoll.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"LOD_Editor#LOD_View" versionNum:2.06 versionName:"Defective Zephyr" filename:(getThisScriptFilename())
	
	dotNetControl LodTreeView "TreeView" pos:[0,32] Width:0 Height:0 -- This is moved and expanded out later
		
	label lblLinkType "Link Type:" align:#left offset:[0,4] across:2
	dropDownList lstLinkType "" width:(RsSceneLinkEditorRoll.width - 82) align:#right
	label lblChannelInfo "<Select a channel>" offset:[0,-3]
	
	edittext txtCurrentItem "Current:" readonly:true
	
	multiListBox lstParentObjs "Parent:"
	multiListBox lstChildObjs "Children:"

	local btnWidth = 136
	button btnSelectParents "Select Parents" across:2 width:btnWidth offset:[-1,0]
	button btnSelectChildren "Select Children" width:btnWidth offset:[0,0]
	checkButton btnAddParents "Pick Add Parents" across:2 width:btnWidth offset:[0,-3]
	checkButton btnAddChildren "Pick Add Children" width:btnWidth offset:[1,-3]
	button btnRemoveParents "Remove Parents" across:2 width:btnWidth offset:[-1,-3]
	button btnRemoveChildren "Remove Children" width:btnWidth offset:[0,-3]
	
	local dnColour = (dotNetClass "System.Drawing.Color")
	local WindowClr, TextClr, SelectClr
	local WindowBrush, TextBrush, SelectBrush
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	local currPickSelection = #()
	local allLodObjs -- All objects in selected object's lod-hierarchy
	
	local parentItems, childItems
	
	local isPickActive = False
	local ShowLinkType = -1
	
	fn getLodObjs obj mode:#children linkType:ShowLinkType recurse:False = 
	(
		local retList = #()
		
		local lodFunc = if (mode == #children) then (RsSceneLink.GetChildren) else (RsSceneLink.GetParents)
		
		local objNum = 0
		local checkObj = obj
		
		while (checkObj != undefined) do 
		(
			local getObjs = #()
			lodFunc (linkType) checkObj &getObjs
			join retList getObjs
			
			objNum += 1
			checkObj = if recurse then retList[objNum] else undefined
		)
		
		return retList
	)
	
	-- Returns list of all objects in selected object's lod-hierarchy for a particular linkType:
	fn getAllLodObjs obj linkType:ShowLinkType = 
	(
		allLodObjs = #(obj)
		
		join allLodObjs (getLodObjs obj mode:#children recurse:True linkType:linkType)
		join allLodObjs (getLodObjs obj mode:#parents recurse:True linkType:linkType)
		
		return allLodObjs
	)
	
	-- Recursively adds objects and their scenelink-children to 'LodTreeView':
	fn AddObjToTree ParentNode Obj LinkType SelObjNodes:#() TopLevel:True = 
	(
		-- Get object's lod-children:
		local ObjChildren = GetLodObjs Obj Mode:#children LinkType:LinkType
		
		-- Don't add singleton hierarchies:
		if (TopLevel) and (ObjChildren.Count == 0)  do return False
		
		local NewNode = ParentNode.Nodes.Add Obj.Name
		NewNode.Tag = (DotNetMxsValue Obj)
		
		-- Collect nodes for selected objects:
		if Obj.isSelected do 
		(
			append SelObjNodes NewNode
		)
		
		-- Recursively add child-link objects:
		for Child in ObjChildren do 
		(
			AddObjToTree NewNode Child LinkType SelObjNodes:SelObjNodes TopLevel:False
		)
	)
	
	-- Recursively build flattened tree-node list:
	fn GetFlatNodeList ThisNode OutList:#() = 
	(
		append OutList ThisNode 
		
		for n = 0 to (ThisNode.Nodes.Count - 1) do 
		(
			GetFlatNodeList ThisNode.Nodes.Item[n] OutList:OutList
		)
		
		return OutList
	)
	
	-- Makes sure that at least one selected object-node is visible:
	fn SelObjNodeEnsureVisible SelNodes: = 
	(
		if (SelNodes == unsupplied) do 
		(
			-- Generate flattened tree-node list:
			local NodesList = GetFlatNodeList LodTreeView
			
			-- Collect nodes representing selected objects:
			SelNodes = #()
			for ThisNode in NodesList do 
			(
				if (isKindOf ThisNode.Tag DotNetMxsValue) and (isValidNode ThisNode.Tag.Value) and (ThisNode.Tag.Value.isSelected) then 
				(
					append SelNodes ThisNode
				)
			)
		)
		
		if (SelNodes.Count == 0) do return False
		
		local HasVisibleSelNode = False
		for ThisNode in SelNodes while (not HasVisibleSelNode) do 
		(
			HasVisibleSelNode = ThisNode.isVisible
		)
		
		-- Skip if a selected-object node is visible:
		if HasVisibleSelNode do return True
		
		-- Make first-found selected-object node visible:
		SelNodes[1].EnsureVisible()
		
		return True
	)
	
	-- Update the treeview to show links for the current linktype:
	fn UpdateSceneLinkTree LinkType:ShowLinkType = 
	(
		-- Don't rebuild tree if no change has been detected, just redraw:
		if (LodTreeView.Tag == LinkType) do 
		(
			SelObjNodeEnsureVisible()
			LodTreeView.Refresh()
			return OK
		)
		LodTreeView.Tag = LinkType
		
		LodTreeView.Nodes.Clear()
		
		-- Don't show for all linktypes:
		if (LinkType == -1) do 
		(
			LodTreeView.Nodes.Add "(Choose Link Type to show SceneLink Tree)"
			return OK
		)
		
		local TempNode = LodTreeView.Nodes.Add "Rebuilding SceneLink tree..."
		LodTreeView.Refresh()
		
		-- Find scene's topmost parents for this linktype:
		local LinkParObjs = for Obj in Geometry where (RsSceneLink.Getparent LinkType Obj == undefined) collect Obj
		
		if (LinkParObjs.Count == 0) do return OK
		
		local SelObjNodes = #()
		for Obj in LinkParObjs do 
		(
			AddObjToTree LodTreeView Obj LinkType SelObjNodes:SelObjNodes
		)
		
		-- Remove temp node:
		TempNode.Remove()
		
		if (LodTreeView.Nodes.Count == 0) do 
		(
			LodTreeView.Nodes.Add "No objects found with this Link Type"
			LodTreeView.Refresh()
			return OK
		)
		
		-- Expand all nodes out:
		LodTreeView.ExpandAll()
		
		if (SelObjNodes.Count == 0) then 
		(
			-- Scroll to topmost node:
			LodTreeView.Nodes.Item[0].EnsureVisible()
		)
		else 
		(
			-- Scroll to node for first selected object:
			SelObjNodeEnsureVisible SelNodes:SelObjNodes
		)
		
		LodTreeView.Refresh()
	)
	
	-- Remove listed parents from an object for a specific link-type:
	fn removeParents obj linkType remObjs = 
	(
		local currentParents = getLodObjs obj mode:#parents linkType:(linkType)
		local newParents = for obj in currentParents where (findItem remObjs obj == 0) collect obj
		
		--format "%: %... % => %\n" (linkType) (remObjs as string) (currentParents as string) (newParents as string)
		
		-- Redo parent-list for this linkType, if objects are to be removed:
		if (newParents.count != currentParents.count) do 
		(
			RsSceneLink.RemoveContainer (linkType) obj
			
			for parObj in newParents do 
			(
				RsSceneLink.SetParent (linkType) obj parObj append:True
			)
		)
		
		return OK
	)
	
	-- Remove listed children from an object for a specific link-type:
	fn removeChildren obj linkType remObjs = 
	(
		local childObjs = getLodObjs obj mode:#children linkType:(linkType)
		
		-- Does this object have children to be removed?
		for childObj in childObjs where (findItem remObjs childObj != 0) do 
		(
			-- Remove this object from child-object's parents:
			removeParents childObj linkType #(obj)
		)
		
		return OK
	)
	
	fn stopSelCallback disableAttrCallback:false = 
	(
		callbacks.removescripts id:#RsPickSceneLinkSelect
		
		-- Disable/enable Attribute Editor selection-callback:
		if (::RsAttrEditMainRoll != undefined) do 
		(
			RsAttrEditMainRoll.disableCallbacks = disableAttrCallback
		)
	)
	
	fn startSelCallback = 
	(
		stopSelCallback()
		callbacks.addscript #selectionSetChanged "RsSceneLinkEditorRoll.updateSelection()" id:#RsPickSceneLinkSelect
	)
	
	fn sortLodItems v1 v2 = 
	(
		local retVal = v1.chan - v2.chan
		
		if (retVal == 0) do 
		(
			retVal = striCmp v1.obj.name v2.obj.name
		)
		
		return retVal
	)
	
	fn getListName item showChan:True = 
	(
		if showChan then 
		(
			local retVal = stringStream ""
			format "% [%]" item.obj.name (RsScenelink.getChannelName item.chan) to:retVal
			return (retVal as string)
		)	
		else 
		(
			return item.obj.name
		)
	)
	
	fn updateRemoveButtons = 
	(
		local selCount = lstParentObjs.selection.numberSet
		btnRemoveParents.enabled = (selCount != 0)
		btnRemoveParents.text = if (selCount == 1) then ("Remove Parent") else ("Remove Parents")
		
		local selCount = lstChildObjs.selection.numberSet
		btnRemoveChildren.enabled = (selCount != 0)
		btnRemoveChildren.text = if (selCount == 1) then ("Remove Child") else ("Remove Children")
	)
	
	fn CollectionContainsObj coll searchObj searchChan =
	(
		for item in coll where item.obj==searchObj and item.chan==searchChan do
			return true
		return false
	)
	
	-- Update data shown in tool:
	fn updateSelection = 
	(
		if isPickActive do return false
		
		currPickSelection = (getCurrentSelection())
		
		local hasObjs = (currPickSelection.count != 0)
		local singleSel = (currPickSelection.count == 1)

		txtCurrentItem.text = case currPickSelection.count of 
		(
			0:"None"
			1:(currPickSelection[1].name)
			default:"Multiple"
		)
		
		-- Show name of channels on list?
		local allTypesShown = (ShowLinkType == -1)
		
		-- Get info on selected linktype:
		local linkData
		if (ShowLinkType >= 0) do 
		(
			linkData = gRsSceneLinkTypeData[ShowLinkType + 1]
		)
		
		(
			-- Collect selected-objects' parents:
			parentItems = #()
			
			for obj in currPickSelection do 
			(
				local chans = if (ShowLinkType == -1) then (RsSceneLink.GetNodeParentLinkTypes obj) else #(ShowLinkType)
				
				local lodObjs = #()
				local lodChans = #()
				
				for chan in chans do 
				(
					local chanObjs = getLodObjs obj mode:#parents linkType:chan
					--format "%: %\n" chan (chanObjs as string)
					for obj in chanObjs do 
					(
						append lodObjs obj
						append lodChans chan
					)
				)
				
				join parentItems (
					for n = 1 to lodObjs.count where 
						not (CollectionContainsObj  parentItems lodObjs[n] lodChans[n])
						collect (dataPair obj:lodObjs[n] chan:lodChans[n]))
			)
			
			qsort parentItems sortLodItems
			
			-- Add object-names to list:
			lstParentObjs.items = for item in parentItems collect (getListName item showChan:allTypesShown)
			lstParentObjs.selection = #{}
			
			-- Set enabled for parent-controls:
			btnSelectParents.enabled = (parentItems.count != 0)
			
			-- Only allow Add if one object and a specific linkType is chosen, and object doesn't have max number of linked objects yet:
			btnAddParents.enabled = singleSel and (not allTypesShown) and ((linkData.maxParents == -1) or (linkData.maxParents > parentItems.count))
		)
		
		(
			-- Collect selected-objects' children:
			childItems = #()
			
			for obj in currPickSelection do 
			(
				local lodObjs = #()
				RsSceneLink.GetChildren (ShowLinkType) obj &lodObjs
				local lodChans = (RsSceneLink.GetNodeChildLinkTypes obj)
				
				join childItems (for n = 1 to lodObjs.count collect (dataPair obj:lodObjs[n] chan:lodChans[n]))
			)
			
			qsort childItems sortLodItems
			
			-- Add object-names to list:
			lstChildObjs.items = for item in childItems collect (getListName item showChan:allTypesShown)
			lstChildObjs.selection = #{}
			
			-- Set enabled for children-controls:
			btnSelectChildren.enabled = (childItems.count != 0)
			
			-- Only allow Add if one object and a specific linkType is chosen, and object doesn't have max number of linked objects yet:
			btnAddChildren.enabled = singleSel and (not allTypesShown) and ((linkData.maxChildren == -1) or (linkData.maxChildren > childItems.count))
		)
		
		-- Set link-details text:
		(
			lblChannelInfo.text = "<Select a channel>"
			lstParentObjs.text = "Parent: "
			lstChildObjs.text = "Children: "
			
			if (linkData != undefined) do 
			(
				lblChannelInfo.text = linkData.info
				lstParentObjs.text += linkData.parentInfo
				lstChildObjs.text += linkData.childInfo
			)
		)
		
		-- Update Remove-button text/enabled:
		(
			updateRemoveButtons()
		)
		
		-- Update treeview:
		(
			UpdateSceneLinkTree()
		)
		
		OK
	)
	
	-- Called whenever a change is made to linking:
	fn ChangeMade = 
	(
		-- This causes lod-tree to be redrawn:
		LodTreeView.Tag = undefined
		
		UpdateSelection()
	)
	
	fn AddParentsChildren mode:#children = 
	(		
		-- If picker is already active, don't do anything, and undo click:
		if isPickActive do 
		(
			local btnAdder = case mode of 
			(
				#parents:bntAddParents
				#children:bntAddChildren
			)
			
			btnAdder.checked = (not btnAdder.checked)
			return false
		)
		
		-- Remember object:
		local currObj = currPickSelection[1]
		
		-- Stop selection-callback:
		StopSelCallback disableAttrCallback:true
		
		-- Pass mode-value to picker-tool via global:
		global RsSceneLinkEditor_PickerTool_Mode = Mode
		
		-- Start picker-tool: keep adding them until max link-count is reached, or user cancels:
		isPickActive = True
		StartTool RsSceneLinkEditor_PickerTool Prompt:("Pick " + (Mode as string) + " from Scene")
		isPickActive = False
		
		btnAddParents.checked = false
		btnAddChildren.checked = false
		
		updateSelection()
		startSelCallback()
		
		-- Update treeview:
		LodTreeView.Tag = undefined
		UpdateSceneLinkTree()
	)

	fn selectObjs objs = 
	(
		obj = for obj in objs where (isValidNode obj) collect obj
		
		if (objs.count == 0) then 
		(
			clearSelection()
		)
		else 
		(
			select objs
		)
		completeRedraw()
	)
	
	fn selLodObjs mode:#children = 
	(
		local itemList = if (mode == #children) then childItems else parentItems
		local selObjs = for item in itemList collect item.obj
		selectObjs selObjs
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on lstLinkType selected val do 
	(
		ShowLinkType = (val - 2)
		ChangeMade()
	)
	
	on lstParentObjs selected numClicked do 
	(
		updateRemoveButtons()
	)
	on lstParentObjs doubleClicked numClicked do 
	(
		selectObjs #(parentItems[numClicked].obj)
	)
	
	on lstChildObjs selected numClicked do 
	(
		updateRemoveButtons()
	)
	on lstChildObjs doubleClicked numClicked do 
	(
		selectObjs #(childItems[numClicked].obj)
	)
	
	on btnSelectParents pressed do 
	(
		undo "select parents" on 
		(
			selLodObjs mode:#parents
		)
	)
	
	on btnSelectChildren pressed do 
	(
		undo "select children" on 
		(
			selLodObjs mode:#children
		)
	)
	
	on btnAddParents picked parObj do 
	(
		if parObj == undefined do 
		(
			return 0
		)
		
		for obj in currPickSelection do 
		(
			RsSceneLink.AddContainer (ShowLinkType) obj
			RsSceneLink.SetParent (ShowLinkType) obj parObj
		)
		
		flashNodes #(parObj)
		select currPickSelection
	)
	
	on btnAddParents changed pressed do 
	(
		AddParentsChildren mode:#parents
	)
	on btnAddChildren changed pressed do 
	(
		AddParentsChildren mode:#children
	)
	
	on btnRemoveParents pressed do 
	(
		-- Get items selected in parent-list, collect them per link-type:
		local linkTypes = #()
		local remLinkItems = #()
		
		for selNum = lstParentObjs.selection do 
		(
			local item = parentItems[selNum]
			local findNum = findItem linkTypes item.chan
			
			if (findNum == 0) do 
			(
				append linkTypes item.chan
				append remLinkItems (dataPair objs:#() chan:item.chan)
				findNum = linkTypes.count
			)
			
			append remLinkItems[findNum].objs item.obj
		)
		
		-- Check each selected object:
		for selObj in (getCurrentSelection()) do 
		(
			-- Remove object-parents if they're in remove-objects list:
			for item in remLinkItems do 
			(
				removeParents selObj item.chan item.objs
			)
		)	
		
		ChangeMade()
	)
	
	on btnRemoveChildren pressed do 
	(
		-- Get items selected in child-list, collect them per link-type:
		local linkTypes = #()
		local remLinkItems = #()
		
		for selNum = lstChildObjs.selection do 
		(
			local item = childItems[selNum]
			local findNum = findItem linkTypes item.chan
			
			if (findNum == 0) do 
			(
				append linkTypes item.chan
				append remLinkItems (dataPair objs:#() chan:item.chan)
				findNum = linkTypes.count
			)
			
			append remLinkItems[findNum].objs item.obj
		)
		
		for selObj in (getCurrentSelection()) do 
		(
			-- Remove object-children if they're in remove-objects list:
			for item in remLinkItems do 
			(
				removeChildren selObj item.chan item.objs
			)
		)
		
		ChangeMade()
	)
	
	-- Draw lod-tree node:
	on LodTreeView DrawNode Sender Args do 
	(
		local DrawNode = Args.Node
		
		-- Don't draw if node-bounds haven't been defined yet:
		if (DrawNode.Bounds.Width == 0) do return False
		
		local BoundsX = DrawNode.Bounds.X
		local BoundsY = DrawNode.Bounds.Y
		
		local UseText = DrawNode.Text
		local UseFont = LodTreeView.Font
		local NodeTag = DrawNode.Tag
		
		-- Is node's object selected?
		local isSelected = (NodeTag != undefined) and (isValidNode NodeTag.Value) and (NodeTag.Value.isSelected)
		
		-- Draw background-rectangle:
		local NodeBrush = if isSelected then SelectBrush else WindowBrush
		local StringSize = Args.Graphics.MeasureString UseText UseFont
		local SelectRect = DotNetObject "System.Drawing.Rectangle" BoundsX BoundsY (StringSize.Width + 4) (StringSize.Height + 2)
		Args.Graphics.FillRectangle NodeBrush SelectRect 
		
		-- Draw text:
		Args.Graphics.DrawString UseText UseFont TextBrush BoundsX BoundsY
	)
	
	-- Select object(s) when treeview-nodes are clicked:
	on LodTreeView NodeMouseClick Sender Args do 
	(
		local NodeTag = Args.Node.Tag
		
		if (NodeTag == undefined) do return False
		
		local NodeObj = NodeTag.Value
		
		if (isValidNode NodeObj) do 
		(
			case of 
			(
				(keyboard.controlPressed):
				(
					-- Toggle object's selectedness:
					if (not NodeObj.isSelected) then 
					(
						SelectMore NodeObj
					)
					else 
					(
						Deselect NodeObj
					)
				)
				(keyboard.shiftPressed):
				(
					-- Get flat list of tree-nodes:
					local NodesList = (GetFlatNodeList LodTreeView)
					
					-- Get first/last nodes in shift-selection:
					local SelNums = for ThisNode in #(LodTreeView.SelectedNode, Args.Node) collect 
					(
						FindItem NodesList ThisNode
					)
					
					Sort SelNums
					local StartNum = SelNums[1]
					local EndNum = SelNums[2]
					
					-- Select objects tagged to treenodes between these two indices:
					if (StartNum != 0) and (EndNum != 0) do 
					(
						local SubSel = for i = StartNum to EndNum collect (NodesList[i].Tag.Value)
						SelectMore SubSel
					)
				)
				Default:
				(
					Select NodeObj
				)
			)
			
			CompleteRedraw()
		)
	)
	
	on RsSceneLinkEditorRoll open do 
	(
		-- Resize treeview, and move other controls to fit it:
		(
			local TreeWidth = 300
			LodTreeView.Height = (RsSceneLinkEditorRoll.Height - LodTreeView.Pos.Y) - 10
			LodTreeView.Width = TreeWidth
			LodTreeView.Pos.X += (RsSceneLinkEditorRoll.Width - 5)
			LodTreeView.Pos.Y += 5
			RsSceneLinkEditorRoll.Width += TreeWidth
			
			RsBannerPanel.Width = RsSceneLinkEditorRoll.width
		)
		
		-- Set up 'LodTreeView':
		(
			-- Get dotnet colours for current Max colourscheme:
			WindowClr = (ColorMan.GetColor #window) * 255
			WindowClr = (dnColour.FromArgb WindowClr[1] WindowClr[2] WindowClr[3])
			WindowBrush = (dotNetObject "System.Drawing.SolidBrush" WindowClr)

			TextClr = (ColorMan.GetColor #windowText) * 255
			TextClr = (dnColour.FromArgb TextClr[1] TextClr[2] TextClr[3])
			TextBrush = (dotNetObject "System.Drawing.SolidBrush" TextClr)
			
			SelectClr = (DotNetClass "System.Drawing.KnownColor").MenuHighlight
			SelectClr = (dnColour.FromKnownColor SelectClr)
			SelectBrush = (dotNetObject "System.Drawing.SolidBrush" SelectClr)
			
			LodTreeView.BackColor = WindowClr
			LodTreeView.ForeColor = TextClr
			LodTreeView.DrawMode = LodTreeView.DrawMode.OwnerDrawText
			LodTreeView.Sorted = True
		)
		
		-- Add scenelink types to list:
		lstLinkType.items = #("Any") + (for item in gRsSceneLinkTypeData collect item.name)
		
		banner.setup()
		
		updateSelection()
		startSelCallback()
		
		UpdateSceneLinkTree()
	)
	
	on RsSceneLinkEditorRoll close do 
	(
		stopSelCallback()
		
		if isPickActive do 
		(
			StopTool RsSceneLinkEditor_PickerTool
		)
	)
)

CreateDialog RsSceneLinkEditorRoll --280 600
