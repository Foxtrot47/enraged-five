RsCollectToolUsageData (getThisScriptFilename())
	
global LinkType_ClothCollision = 3

rollout RsClothCollEditorRoll "Cloth Collision Editor"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
--	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/LOD_Editor#Scene_LOD_Editor" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	edittext currentItem "Current:" enabled:false
	group "Problems"
	(
		label lblErrors "" width:270 height:20 align:#left
	)
	multiListBox parentItems "Cloth meshes (Parents):"
	multiListBox childrenItems "Collision objects (Children):"
	button buttonSelectParents "Select Parents" width:100 across:2 
	button buttonSelectChildren "Select Children" width:100
	button buttonAddParents "Pick Parents"  width:100 across:2 
	button buttonAddChildren "Pick Children" width:100
	button buttonRemoveParents "Remove Parents" width:100 across:2 
	button buttonRemoveChildren "Remove Children" width:100
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	fn updateSelection = (
		local parNodes = #()
		local childnodes = #()
		if selection.count>1 then
			currentItem.text = "Multiple"
		else if selection.count==1 then
			currentItem.text = selection[1].name
		else
			currentItem.text = ""
			
		for obj in selection do 
		(
			RsSceneLink.GetParents LinkType_ClothCollision obj &parNodes
			RsSceneLink.GetChildren LinkType_ClothCollision obj &childNodes
		)
		parentItems.items = makeUniqueArray (for o in parNodes collect o.name)
		childrenItems.items = makeUniqueArray (for o in childNodes collect o.name)
			
		lblErrors.text = ""
	)
	
	fn pickCallback obj = (
	
		for pickObj in selection do (
			if obj == pickObj then (
				return false
			)
		)
	
		return true
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on childrenItems doubleClicked numClicked do 
	(
		newselect = getnodebyname childrenItems.items[numClicked]
		
		if newselect != undefined then (
			select newselect
		)
	)
	
	on parentItems doubleClicked numClicked do 
	(
		newselect = getnodebyname parentItems.items[numClicked]
		
		if newselect != undefined then (
			select newselect
		)
	)
	
	on buttonSelectChildren pressed do 
	(
		if childrenItems.selection.numberSet<=0 then
		(
			lblErrors.text = "No nodes to unlink selected in list"
			return false
		)
		lblErrors.text = ""
		local toSelect = #()
		for index in (childrenItems.selection as array) do 
		(
			local thenode = getNodeByName childrenItems.items[index]
			if isValidNode thenode then 
				append toSelect thenode
		)
		select toSelect
	)
	
	on buttonSelectParents pressed do 
	(
		if parentItems.selection.numberSet<=0 then
		(
			lblErrors.text = "No nodes to unlink selected in list"
			return false
		)
		lblErrors.text = ""
		local toSelect = #()
		for index in (parentItems.selection as array) do 
		(
			local thenode = getNodeByName parentItems.items[index]
			if isValidNode thenode then 
				append toSelect thenode
		)
		select toSelect
	)
	
	on buttonAddChildren pressed do (
	
		origText = buttonAddChildren.text
		
		buttonAddChildren.text = "R Click to exit Pick"
-- 		buttonAddChildren.color = red
		callbacks.removescripts id:#pickLODselect

		if selection.count != 1 then (
		
			lblErrors.text = "please select only one object to add children to"
			callbacks.addscript #selectionSetChanged "RsClothCollEditorRoll.updateSelection()" id:#pickLODselect
		)
		lblErrors.text = ""

		parent = selection[1]
		local parentContainer = RsMapObjectGetMapContainerFor parent

		selectlist = #()
		RsSceneLink.GetChildren LinkType_ClothCollision parent &selectlist
		namelist = #()
		pickedObject = undefined
		while ( ( pickedObject = pickObject filter:pickCallback count:1 ) != undefined ) do
		(
			appendIfUnique selectlist pickedObject
			childrenItems.items = for o in selectlist collect o.name
			selectmore pickedObject
		)

		for selobj in selectlist do
		(
			if (RsMapObjectGetMapContainerFor selobj).name != parentContainer.name then (
				gRsUlog.LogError "Trying to set RsSceneLink parent on an object in a different container!" context:selobj
			)else(
				RsSceneLink.SetParent LinkType_ClothCollision selobj parent append:true
			)		
		)	

		flashnodes selectlist
		
		select parent
		
		updateSelection()
		
		callbacks.addscript #selectionSetChanged "RsClothCollEditorRoll.updateSelection()" id:#pickLODselect
		
		buttonAddChildren.text = origText
		
	)

	on buttonAddParents pressed do (
	
		origText = buttonAddParents.text
		buttonAddParents.text = "R Click to exit Pick"
		callbacks.removescripts id:#pickLODselect

		if selection.count != 1 then (
		
			lblErrors.text = "please select only one object to add parents to"
			callbacks.addscript #selectionSetChanged "RsClothCollEditorRoll.updateSelection()" id:#pickLODselect
		)
		lblErrors.text = ""

		obj = selection[1]
		selectlist = #()
		RsSceneLink.GetParents LinkType_ClothCollision obj &selectlist
		namelist = #()
		pickedObject = undefined
		while ( ( pickedObject = pickObject filter:pickCallback count:1 ) != undefined ) do
		(
			appendIfUnique selectlist pickedObject
			parentItems.items = for o in selectlist collect o.name
			selectmore pickedObject
		)

		for selobj in selectlist do 
		(
			RsSceneLink.SetParent LinkType_ClothCollision obj selobj append:true
		)
		
		flashnodes selectlist
		
		select obj
		
		updateSelection()
		
		callbacks.addscript #selectionSetChanged "RsClothCollEditorRoll.updateSelection()" id:#pickLODselect
		buttonAddParents.text = origText
	)
	
	on buttonRemoveChildren pressed do 
	(
		if childrenItems.selection.numberSet<=0 then
		(
			lblErrors.text = "No nodes to unlink selected in list"
			return false
		)
		lblErrors.text = ""
		for index in (childrenItems.selection as array) do 
		(
			local thenode = getNodeByName childrenItems.items[index]
			if isValidNode thenode then 
				RsSceneLink.RemoveContainer LinkType_ClothCollision thenode
		)
		
		updateSelection()		
	)
	
	on buttonRemoveParents pressed do 
	(
		if selection.count != 1 then 
		(
			lblErrors.text = "please select only one object to add parents to"
			callbacks.addscript #selectionSetChanged "RsClothCollEditorRoll.updateSelection()" id:#pickLODselect
		)
		if parentItems.selection.numberSet<=0 then
		(
			lblErrors.text = "No nodes to unlink selected in list"
			return false
		)
		lblErrors.text = ""
		
		for index in (parentItems.selection as array) do 
		(
			local thenode = getNodeByName parentItems.items[index]
			if isValidNode thenode then 
				RsSceneLink.RemoveParent LinkType_ClothCollision selection[1] thenode
		)
		
		updateSelection()
	)
	
	on RsClothCollEditorRoll open do (
		updateSelection()
		callbacks.addscript #selectionSetChanged "RsClothCollEditorRoll.updateSelection()" id:#pickLODselect
	)
	
	on RsClothCollEditorRoll close do (
		callbacks.removescripts id:#pickLODselect
	)
)

fn CreateClothCollEditorDialog = 
(
	try 
		DestroyDialog RsClothCollEditorRoll
	catch()
	CreateDialog RsClothCollEditorRoll 280 470
)
