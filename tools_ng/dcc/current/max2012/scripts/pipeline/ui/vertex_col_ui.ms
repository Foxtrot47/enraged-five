--
-- File:: pipeline/ui/vertex_col_ui.ms
-- Description:: Vertex colour and illum channel darken/lighten utility.
--
-- Author:: Greg Smith (original)
-- Date:: 22 September 2004
--
-- Author:: David Muir <david.muir@rockstarnorth.com> (vertex-selection and some cleanup)
-- Date:: 30 July 2009
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com> (combining with new Radiosity and Lighting tool)
-- Date:: 10 February 2010  
--

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

-- Clamps colour values to be within 0 to 255
-- and can take a Color or Point3
fn RsClampVertexColour vertColour = (

	if ( Color == classof vertColour ) then
	(
		r = vertColour.r
		g = vertColour.g
		b = vertColour.b
	)
	else
	(
		r = vertColour.x
		g = vertColour.y
		b = vertColour.z
	)
	
	-- Clamp
	if r < 0.0 then (
		r = 0.0
	)
	else if r > 255.0 then (
		r = 255.0
	)

	if g < 0.0 then (
		g = 0.0
	)
	else if g > 255.0 then (
		g = 255.0
	)

	if b < 0.0 then (
		b = 0.0
	)
	else if b > 255.0 then (
		b = 255.0
	)
	
	if ( Color == classof vertColour ) then
	(
	
		(Color r g b)
	)
	else
	(
		[r,g,b]
	)
)

fn GtaChangeColAbs obj value doVertex doIllum relative = (

	local numIllumVerts = getNumIVCVerts(obj.mesh)
	local selVerts = ( getVertSelection obj )
	local VERT_CPV = 0
	local VERT_ILLUM = -1
	
	if ( false == doVertex and false == doIllum ) then
		return false
		
	if ( Editable_Mesh != ( classof obj ) ) then 
		return false
	
	if ( true == doIllum ) then 
	(
		if not (meshop.getMapSupport obj VERT_ILLUM) then
		(
			format "no illum channel on this model.\n"
			return false
		)
	)	

	if ( true == doVertex ) then
	(
		if not (meshop.getMapSupport obj VERT_CPV) then
		(
			format "no colour channel on this model.\n"
			return false
		)
	)
	
	format "vert: %; illum: % percentage: %\n" doVertex doIllum value
	
	value /= 100
	colourChange = 255 * value
	
	if ( true == doVertex ) then 
	(	
		if ( BitArray == ( classof selVerts ) and ( false == selVerts.isEmpty ) ) then
		(
			-- Process selection mesh verts
			format "Process selected mesh verts...\n"

			for v in selVerts do
			(				
				-- Find the faces that use this mesh vertex
				faceList = meshop.getFacesUsingVert obj.mesh v				
				
				local vcVertList = #()
				
				-- Go through each face and find the vertex colour face that
				-- corresponds (they have the same index)
				for f in faceList do
				(
					local vertIdx					
					local faceVerts = getFace obj f
					
					-- Find where the selected vert v, is in this face's verts
					-- as we need this when finding the matching vert colour vert
					if ( v == faceVerts.x ) then vertIdx = 1
					else if ( v == faceVerts.y ) then vertIdx = 2
					else if ( v == faceVerts.z ) then vertIdx = 3
					else return false
				
					vcFace = GetVCFace obj.mesh f
					
					-- vcVert used to store the vcVert number that corresponds to
					-- the selected vert, v
					vcVert = vcFace[vertIdx]								
					vertColour = getVertColor obj.mesh vcVert
					
					--format "colour before %\n" vertColour
					
					-- Check if we've already changed the colour of this vertex
					-- colour vert, as since it can be used in multiple faces we 
					-- don't want it being changed multiple times after 1 button
					-- click (although it's only a problem with doing relative)
					if ( 0 == ( findItem vcVertList vcVert ) ) then
					(					
						-- do our shit
						if relative then
						(
							vertColour.r += colourChange
							vertColour.g += colourChange
							vertColour.b += colourChange
						)
						else
						(
							vertColour.r = colourChange
							vertColour.g = colourChange
							vertColour.b = colourChange
						)

						vertColour = RsClampVertexColour vertColour
						setVertColor obj vcVert vertColour
						append vcVertList vcVert
					)
				)
			)
		)
		else
		(
			-- Process all mesh verts
			format "Process all mesh verts...\n"
			for i = 1 to (getNumCPVVerts obj.mesh) do
			(
				local vertColour
				local vertIllum
				
				-- get the colour
				if ( true == doVertex ) do
				(
					vertColour = getVertColor obj.mesh i
				)
				
				if vertColour != undefined then (
					
					-- do our shit
					if relative then
					(
						vertColour.r += colourChange
						vertColour.g += colourChange
						vertColour.b += colourChange
					)
					else
					(
						vertColour.r = colourChange
						vertColour.g = colourChange
						vertColour.b = colourChange
					)
					
					vertColour = RsClampVertexColour vertColour					
					-- set the colour
					setVertColor obj.mesh i vertColour
				)			
			)
		)
	)
	
	if ( true == doIllum ) then 
	(
		if ( BitArray == ( classof selVerts ) and ( false == selVerts.isEmpty ) ) then
		(
			-- Process selection mesh verts
			format "Process selected mesh verts...\n"
			
			for v in selVerts do
			(				
				-- Find the faces that use this mesh vertex
				faceList = meshop.getFacesUsingVert obj.mesh v				
				
				local vcVertList = #()
				
				-- Go through each face and find the vertex colour face that
				-- corresponds (they have the same index)
				for f in faceList do
				(
					local vertIdx					
					local faceVerts = getFace obj f
					
					-- Find where the selected vert v, is in this face's verts
					-- as we need this when finding the matching illum colour
					if ( v == faceVerts.x ) then vertIdx = 1
					else if ( v == faceVerts.y ) then vertIdx = 2
					else if ( v == faceVerts.z ) then vertIdx = 3
					else return false
				
					vcFace = GetIVFace obj.mesh f
					
					-- vcVert used to store the vcVert number that corresponds to
					-- the selected vert, v
					vcVert = vcFace[vertIdx]							
					vertIllum = getIllumVertColor obj.mesh vcVert
					
					-- Check if we've already changed the colour of this illum
					-- vert, as since it can be used in multiple faces we 
					-- don't want it being changed multiple times after 1 button
					-- click (although it's only a problem with doing relative)
					if ( 0 == ( findItem vcVertList vcVert ) ) then
					(					
						-- do our shit
						if relative then
						(
							vertIllum.x += colourChange
							vertIllum.y += colourChange
							vertIllum.z += colourChange
						)
						else
						(
							vertIllum.x = colourChange
							vertIllum.y = colourChange
							vertIllum.z = colourChange
						)
						vertIllum = RsClampVertexColour vertIllum					
						setIllumVertColor obj.mesh vcVert vertIllum						
						append vcVertList vcVert
					)
				)
			)			
		)
		else
		(
			-- Process all mesh verts
			format "Process all mesh verts...\n"
			for i = 1 to numIllumVerts do (

				-- get the colour		
				vertIllum = getIllumVertColor obj.mesh i
				
				if vertIllum != undefined then (
					
					-- do our shit
					if relative then
					(
						vertIllum.x += colourChange
						vertIllum.y += colourChange
						vertIllum.z += colourChange
					)
					else
					(
						vertIllum.x = colourChange
						vertIllum.y = colourChange
						vertIllum.z = colourChange
					)					
					vertIllum = RsClampVertexColour vertIllum					
					-- set the colour				
					setIllumVertColor obj.mesh i vertIllum
				)
			)
		)
	)
	
	update obj 	-- force mesh update to show colour changes
	
	true
)
	
	
fn getSelectedCV obj vertChecked = (
	doVC = false
	
	if getNumCPVVerts(obj.mesh) > 0 and vertChecked then (
		doVC = true
	)

	selVCVerts = #()
	currVCFace = undefined
	for i = 1 to obj.mesh.selectedverts.count do (
		for j = 1 to obj.mesh.faces.count do (
	
			currFace = getface obj.mesh j
			if doVC then currVCFace = getVCFace obj.mesh j
	
			for k = 1 to 3 do (
			
				if currFace[k] == obj.mesh.selectedverts[i].index then (
				
					if doVC then (
						
						append selVCVerts currVCFace[k]
					)
				)
			)
		)
	)
		
	return selVCVerts 
)


fn getSelectedIV obj illumChecked = (
	doIC = false
	
	if getNumIVCVerts(obj.mesh) > 0 and illumChecked then (
		doIC = true
	)
	
	selICVerts = #()
	currIVFace = undefined
	
	for i = 1 to obj.mesh.selectedverts.count do (
		for j = 1 to obj.mesh.faces.count do (
			
			currFace = getface obj.mesh j
			if doIC then currIVFace = getIVFace obj.mesh j
			
			for k = 1 to 3 do (
				
				if currFace[k] == obj.mesh.selectedverts[i].index then (
				
					if doIC then (
						
						append selICVerts currIVFace[k]
					)
				)
			)
		)
	)
		
	return selICVerts 
)


--------------------------------------------------------------
-- Handlers for external events
--------------------------------------------------------------
fn CopyButton vertChecked illumChecked = (
	if selection.count < 1 then (
			return 0
	)
		
	if classof selection[1] != Editable_Mesh then (
		return 0
	)
		
	obj = selection[1]
	colours = #()
	
	selVCVerts = getSelectedCV obj vertChecked
	selICVerts = getSelectedIV obj illumChecked
		
	if selVCVerts.count > 0 then (
		
		avgVertColTemp = [0,0,0]
		
		for i = 1 to selVCVerts.count do (
			
			currVertCol = getVertColor obj.mesh selVCVerts[i]
		
			avgVertColTemp[1] = avgVertColTemp[1] + currVertCol.r
			avgVertColTemp[2] = avgVertColTemp[2] + currVertCol.g
			avgVertColTemp[3] = avgVertColTemp[3] + currVertCol.b
		)
			
		avgVertColTemp = avgVertColTemp / selVCVerts.count	
			
		for i = 1 to 3 do (
			if avgVertColTemp[i] > 255.0 then (
				avgVertColTemp[i] = 255.0
			)
			if avgVertColTemp[i] < 0 then (
				avgVertColTemp[i] = 0
			)
		)
			
		append colours (color avgVertColTemp[1] avgVertColTemp[2] avgVertColTemp[3])
	) else (
		append colours (color 255 255 255)
	)
		
	if selICVerts.count > 0 then (
		
		avgIllumColTemp = [0,0,0]
		
		for i = 1 to selICVerts.count do (
			
			currVertCol = getIllumVertColor obj.mesh selICVerts[i]
				
			avgVertColTemp[1] = avgVertColTemp[1] + currVertCol[1]
			avgVertColTemp[2] = avgVertColTemp[2] + currVertCol[2]
			avgVertColTemp[3] = avgVertColTemp[3] + currVertCol[3]
		)
			
		avgIllumColTemp = avgVertColTemp / selICVerts.count
				
		for i = 1 to 3 do (
			if avgIllumColTemp[i] > 255.0 then (
				avgIllumColTemp[i] = 255.0
			)
			if avgIllumColTemp[i] < 0 then (
				avgIllumColTemp[i] = 0
			)
		)
					
		append colours (color avgIllumColTemp[1] avgIllumColTemp[2] avgIllumColTemp[3])
	) else (
		append colours (color 255 255 255)
	)
	
	return colours
)

fn PasteButton colourVert colourIllum vertChecked illumChecked = (
	if selection.count < 1 then (
			return 0
	)
		
	if classof selection[1] != Editable_Mesh then (
		return 0
	)
		
	obj = selection[1]
	
	selVCVerts = getSelectedCV obj vertChecked
	selICVerts = getSelectedIV obj illumChecked
		
	if selVCVerts.count > 0 then (		
		for i = 1 to selVCVerts.count do (
			setVertColor obj.mesh selVCVerts[i] colourVert
		)
	)
		
	if selICVerts.count > 0 then (
		for i = 1 to selICVerts.count do (
			setIllumVertColor obj.mesh selICVerts[i] [colourIllum.r,colourIllum.g,colourIllum.b]
		)
	)
)

-- pipeline/ui/vertex_col_ui.ms
