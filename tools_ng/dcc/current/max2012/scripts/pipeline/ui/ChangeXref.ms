--
-- File:: pipeline/ui/ChangeXref.ms
-- Description:: Allows the user to change the object name and max file that a selection of xref's
-- 						Point to with pattern matching on the names
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com>
-- Date:: 30/7/2003
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsChangeRefsRoll "Change Selected Ref Objects"
(
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Ref_Toolkit#Change_Xrefs" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	edittext oldobjecttext 		"Change Refs with source-name:   " text:"*"
	edittext newobjecttext 	"Change ObjectNames to:              "
	edittext filenametext 		"Change Xref Filenames to:            "
	button change "Change"
	
	fn MHGetNewStringFromPattern stringIn patternIn patternOut = 
	(
		--this isnt a perfect conversion but should cope with most
		--cases especially as it should only get here if it gets passed matchpattern
		--i hope there isnt a function already to do this...
		
		--get the bits of the string used as a filter
		patternInBits = filterstring patternIn "?*" 

		--now get the bits that will transfer through to the
		--output string (ie the bits not in the filter)
		passedThroughBits = #()	
		currentString = stringIn
		startstring = 1
		endstring = 1
		wildfirst = false
		
		--this gets us the bits of string that will fall through
		--with the wildcards
		for i = 1 to patternInBits.count do (
		
			pattBit = patternInBits[i]
			pattIndex = findstring currentString pattBit
			
			if i == 1 then (
				if pattIndex > 1 then (

					wildfirst = true

					passover = substring currentString 1 (pattIndex - 1)			
					append passedThroughBits passover
					
					currentString = substring currentString (pattIndex + pattBit.count) (currentString.count - pattIndex - pattBit.count + 1)
					startstring = 1
					--print currentString
				) else (
					startstring = pattIndex + pattBit.count
					--print startstring
				)
			) else (

				endstring = pattIndex - 1

				passover = substring currentString startstring (endstring - startstring + 1)
				append passedThroughBits passover
				
				currentString = substring currentString (endstring + 1) (currentString.count - endstring)
				startstring = pattIndex + pattBit.count - endstring
			)
		)
		
		if startstring <= currentString.count then (
			passover = substring currentString startstring (currentString.count - startstring + 1)		
			append passedThroughBits passover
		)

		--this constructs us the new string
		patternOutBits = filterstring patternOut "?*" 
		
		outString = ""
		
		for i = 1 to patternOutBits.count do (
		
			if wildfirst == true then (
				--pattBit = passedThroughBits[i]
				--outString = outString --+ pattBit

				pattBit = patternOutBits[i]	
				outString = outString + pattBit
			) else (
				pattBit = patternOutBits[i]
				outString = outString + pattBit
		
				--pattBit = passedThroughBits[i]
				--outString = outString --+ pattBit
			)
		)

	--	print outString
		
		return outString
	)
	
	fn doChange = 
	(
		matches = 0
		
		local doXrefFilename = (filenametext.text != "")
		
		local RSrefObjs = #()
		local RSrefObjNames = #()
		
		case of 
		(
			(selection.count == 0):
			(
				messagebox "No objects selected"
			)
			(newobjecttext.text == ""):
			(
				messagebox "No ObjectName set"
			)
			Default: 
			(
				local SelObjs = GetCurrentSelection()
				
				-- De-instance objects, to ensure that changes are only applied to selection:
				InstanceMgr.MakeObjectsUnique SelObjs #group
				
				for obj in SelObjs do 
				(				
					case (classof obj) of 
					(
						XRefObject:
						(
							if matchpattern obj.objectname pattern:oldobjecttext.text do 
							(
								-- if a new xref filename has been supplied then set it
								if doXrefFilename do 
								(
									obj.fileName = filenametext.text
								)
								obj.objectname = MHGetNewStringFromPattern obj.objectname oldobjecttext.text newobjecttext.text
								
								matches += 1
							)
						)
						RSrefObject:
						(
							if matchpattern obj.objectname pattern:oldobjecttext.text do 
							(
								local newObjName = MHGetNewStringFromPattern obj.objectname oldobjecttext.text newobjecttext.text
								
								local findNum = findItem RSrefObjNames newObjName
								if (findNum == 0) do 
								(
									append RSrefObjNames newObjName
									append RSrefObjs #()
									findNum = RSrefObjs.count
								)
								append RSrefObjs[findNum] obj
								
								matches += 1
							)
						)
						RsInternalRef:
						(
							if matchpattern obj.objectname pattern:oldobjecttext.text do 
							(
								local newObjName = MHGetNewStringFromPattern obj.objectname oldobjecttext.text newobjecttext.text
								
								obj.setSourceName newObjName
								
								matches += 1
							)
						)
					)
				)
				
				-- Assign new names to RSrefObjects
				if (RSrefObjs.count != 0) do 
				(
					local usedRefs = for objName in RSrefObjNames collect (RSrefFuncs.getRefDefByName objName)
					
					RSrefFuncs.loadModels usedRefs debugFuncSrc:"[ChangeXrefs.change pressed]"
					
					-- Update the objects a bit at a time - Max falls over when bigger numbers of RSrefs are changed at once
					local batchSize = 100
					local objName, refDef, objs, updateObjs
					for nameNum = 1 to RSrefObjNames.count do 
					(
						RSrefFuncs.setObjsToUseName RSrefObjs[nameNum] RSrefObjNames[nameNum]
					)
					
					-- Update single-selected object's rollout
					if (selection.count == 1) do 
					(
						selection[1].params.open = False
						selection[1].params.open = True
					)
				)
				
				if matches == 0 then 
				(
					messagebox "No matches found"
				) 
				else 
				(
					messagebox ("Changed " + (matches as string) + " objects")
				)
			)
		)
	)
	
	-- Remove linebreaks from a string
	fn removeEnters inString = 
	(
		local outString = ""
		for item in (filterstring inString "\n") do 
		(
			append outString item
		)
		outString
	)
	
	on change pressed do 
	(
		doChange()
	)
	
	-- Save rolled-up state:
	on RsChangeRefsRoll rolledUp down do 
	(
		RsSettingWrite "RsChangeRefsRoll" "rollup" (not down)
	)
)