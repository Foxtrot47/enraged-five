--
-- File:: pipeline/ui/ERotFinder.ms
-- Description::  Find non z-rotation refs
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- For Dave C
-- v1.0
--
-- v1.1 Added Z-rotation of object finder
--
-- v2.0 Rewritten, now works with containerised objects, plus option to search for either or refs or geometry
--
-----------------------------------------------------------------------------

try (destroyDialog RSERotFinder_roll) catch ()

rollout RSERotFinder_roll "Rot Find" height:130
(
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Rotation_Finder" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button ERFindem "XY-rot refs" width:140 
	button EZFindem "Z-rot Objects" width:140
	
	checkBox refSel "Ref objects" offset:[0,3] tooltip:"Search X/RSref objects" checked:true
	checkBox geomSel "Geometry" tooltip:"Search non-ref geometry" checked:true
	
	fn getValidObjs = 
	(
		local getRefs = refSel.checked
		local getGeom = geomSel.checked
		
		local validObjs = #()
		
		for obj in objects where (obj.parent == undefined) or (isKindOf obj.parent Container) do 
		(
			case of 
			(
				(isRefObj obj):
				(
					if getRefs do (append validObjs obj)
				)
				-- This should only pick non-reffed geometry, if it's skipped the above option:
				(superClassOf obj == GeometryClass):
				(
					if getGeom do (append validObjs obj)
				)
			)
		)
		
		validObjs
	)
					
	on ERFindem pressed do
	(
		clearSelection()
		local rotObjs = for obj in getValidObjs() where (obj.rotation.x != 0) or (obj.rotation.y != 0) collect obj
		for obj in rotObjs do (join rotObjs obj.children)
		select rotObjs				
	)
	
	on EZFindem pressed do
	(
		clearSelection()
		local rotObjs = for obj in getValidObjs() where (obj.rotation.z != 0) collect obj
		for obj in rotObjs do (join rotObjs obj.children)
		select rotObjs
	)
)

createDialog RSERotFinder_roll