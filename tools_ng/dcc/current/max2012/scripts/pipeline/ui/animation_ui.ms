try(CloseRolloutFloater RsAnimationToolsFloater)catch()

rollout RsAnimationToolsRollout "Apply Rockstar Animation (*.anim)"
(
	edittext edtAnimationFile "Animation File:" text:""	
	button btnBrowse "Browse" width:100
	button btnApply "Apply" width:100
	
	on btnBrowse pressed do (
		f = getOpenFileName caption:"Open an Animation File:" types:"Rockstar Animation File(*.anim)|*.anim|"
		if f != undefined then
			if doesFileExist f then
				edtAnimationFile.text = f
	)
	
	on btnApply pressed do (
		
		if $ == undefined or $ == $selection then
		(
			messageBox "Object must not be NULL or part of a selection." title:"Error"
			return false
		)
			
		ApplyAnimation $ edtAnimationFile.text
		
		return true
	)
)

global RsAnimationToolsFloater = newRolloutFloater "Animation Importer" 300 112
AddRollout RsAnimationToolsRollout RsAnimationToolsFloater