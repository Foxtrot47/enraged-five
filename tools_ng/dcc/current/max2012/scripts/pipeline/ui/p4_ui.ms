try(CloseRolloutFloater RsPerforceToolFloater)catch()

rollout RsPerforceToolRollout "Perforce Tools"
(
	group "Checkout Scene Files:"
	(
		button btnSceneCheckout "Source-files [max/maxc]" width:200 tooltip:"Check out the current scene's container-files and maxfile"
		button btnAssetCheckout "Export-assets [xml/zip]" width:200 tooltip:"Check out scene-map zip and xml files"
	)
	group "Options: (not saved)"
	(
		checkbutton btnP4Active "Max Perforce Integration" width:200 tooltip:"Allow Max Perforce Integration\n\nWARNING: Turning this off will deactivate all Max Perforce functionality for this session" checked:(RsProjectGetPerforceIntegration())
		checkbutton btnP4SaveCheck "Perforce time-check on file save" width:200 tooltip:"Check to see when files were last checked in, when scene/containers are saved" checked:(RsCheckP4OnFileSave)
	)
	group "Connection:"
	(
		button btnLogout "Log out" width:100 tooltip:"Log out of main current Perforce connection"
	)
	
	on btnSceneCheckout pressed do 
	(
		local maps = RsMapGetMapContainers()
		local checkedOutMaxFile = false
		
		local mapFileList = for m in maps collect 
		(
			local mapCont = m.cont
			
			if (isValidNode mapCont) then 
			(
				local mapFilename = RsContFuncs.getContFilename mapCont
				
				if (mapFilename == "") then dontCollect else (mapFilename)
			)
			else if not checkedOutMaxFile then
			(
				if (maxfilename == "") then dontCollect else (maxfilepath+maxfilename)
			)
		)
		
		if (mapFileList.count != 0) do 
		(
			::gRsPerforce.edit mapFileList
		)
	)
	
	on btnAssetCheckout pressed do
	(
		local maps = RsMapGetMapContainers()
		local fileArray = #()
		for map in maps do 
		(
			local mapRpfNode = ( RsProjectContentFind (toLower map.name) type:"maprpf" )
			local mapZipNode = ( RsProjectContentFind (toLower map.name) type:"mapzip" )
			local mapArchiveNode = undefined
			
			if ( ( undefined == mapRpfNode ) and ( undefined == mapZipNode ) ) then 
			(
				messagebox (map.name + " does not have an MAPRPF or MAPZIP entry in the content tree. Have you renamed the file?") context:map
			)
			else
			(
				mapArchiveNode = mapZipNode
				if ( undefined == mapArchiveNode ) then
					mapArchiveNode = mapRpfNode
			
				for file in mapArchiveNode.children do 
				(
					local filename = file.filename
					local fullFilename = (mapArchiveNode.path + "/" + filename)
					if (RsIsFileReadOnly fullFilename) do 
					(
						append fileArray fullFilename
					)
				)
			)
		)
		print fileArray
		
		if (fileArray.count != 0) then 
		(
			::gRsPerforce.edit fileArray
		)
		else
		(
			print "either no image files found, or already checked out."
		)
	)
	
	on btnP4Active changed state do 
	(
		RsProjectSetPerforceIntegration state
	)
	
	on btnP4SaveCheck changed state do 
	(
		RsCheckP4OnFileSave = state
	)
	
	on btnLogout pressed do 
	(
		::gRsPerforce.logout()
	)
	
-- 	on RsPerforceToolRollout entered arg do
-- 	(
-- 		print "entered"
-- 	)
)

global RsPerforceToolFloater = newRolloutFloater "Perforce Tools" 300 380
AddRollout RsPerforceToolRollout RsPerforceToolFloater
AddRollout RsP4PasswordDialog RsPerforceToolFloater