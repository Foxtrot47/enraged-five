--
-- File:: pipeline/util/projman.ms
-- Description:: Project management style attribute setting for gta object and gta animheirarchy objects
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
-- Date:: 2004
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- 15/3/2010
-- by Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Added in Display Notes
--
-- 9/1/2003
-- by Greg Smith <greg.smith@rockstarnorth.com
-- Display Notes
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
GtaProjManAllowSetup = true					-- change this to true to allow editing of attributes
GTAProjManSelectedItem = #()
GtaProjManActive = #()
GtaProjManActiveName = #()

global usr_notes = #()
global notebjs = #()


-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
fn qsortGTAProjManSort objA objB = (

	priorityA = 0.0
	priorityB = 0.0

	if getattrclass objA == "Gta Object" or getattrclass objA == "GtaAnimHierarchy" then (	
		priorityA = getattr objA (getattrindex (getattrclass objA) "Priority")
	)
	
	if getattrclass objB == "Gta Object" or getattrclass objB == "GtaAnimHierarchy" then (	
		priorityA = getattr objB (getattrindex (getattrclass objA) "Priority")
	)
	
	if priorityA > priorityB then (
		return 1
	)

	if priorityA < priorityB then (
		return -1
	)
	
	return 0
)


-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- ACTIVE ITEMS
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout GtaProjManListRoll "Active Items"
(
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Project_Manager" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	listbox lstActive "Active:"

	fn updateSelection = (
		listCheckName = #()
		append listCheckName "Level Design Required"
		append listCheckName "Model Needs Improvement"
		append listCheckName "Texturing Needs Improvement"
		append listCheckName "Geometry Needs Optimising"
		append listCheckName "Textures Needs Optimising"
		append listCheckName "LOD Needs Modelling"
		append listCheckName "LOD Needs Texturing"
		append listCheckName "Collision Needs Optimising"
		append listCheckName "Collision Needs Attributes"
		
		listCheckNameDone = #()
		append listCheckNameDone "LDR Done"
		append listCheckNameDone "MNI Done"
		append listCheckNameDone "TNI Done"
		append listCheckNameDone "GNO Done"
		append listCheckNameDone "TNO Done"
		append listCheckNameDone "LNM Done"
		append listCheckNameDone "LNT Done"
		append listCheckNameDone "CNO Done"
		append listCheckNameDone "CNA Done"
		
		GtaProjManActive = #()
		GtaProjManActiveName = #()
		
		for obj in rootnode.children do (
			if getattrclass obj == "Gta Object" or getattrclass obj == "GtaAnimHierarchy" then (
				for i = 1 to listCheckName.count do (
					if getattr obj (getattrindex (getattrclass obj) listCheckName[i]) == true then (
						if getattr obj (getattrindex (getattrclass obj) listCheckNameDone[i]) == false then (					
							append GtaProjManActive obj
							exit
						)
					)
				)
			)
		)
		
		qsort GtaProjManActive qsortGTAProjManSort
		
		for obj in GtaProjManActive do (
			append GtaProjManActiveName obj.name
		)
		
		lstActive.items = GtaProjManActiveName
	)
	
	on lstActive selected newsel do (
		select GtaProjManActive[newsel]
	)
	
	on btnRefresh pressed do (
		updateSelection()
	)
	
	on GtaProjManListRoll open do (
		updateSelection()
	)
	
	on GtaProjManListRoll close do (
	)
)

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- SELECTED ITEM
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout GtaProjManRoll "Selected Item"
(
	label labObjNameStc "Name:" align:#left
	label labObjName "" align:#left offset:[35,-18]
	spinner spnPriority "Priority" range:[1,100,1] scale:1
	checkbox chkLDR "Level Design Requirement" tristate:0
	checkbox chkLDRDone "Done" tristate:0 offset:[200,-20]
	checkbox chkMNI "Model Needs Improving" tristate:0
	checkbox chkMNIDone "Done" tristate:0 offset:[200,-20]
	checkbox chkTNI "Texturing Needs Improving" tristate:0
	checkbox chkTNIDone "Done" tristate:0 offset:[200,-20]
	checkbox chkGNO "Geometry Needs Optimising" tristate:0
	checkbox chkGNODone "Done" tristate:0 offset:[200,-20]
	checkbox chkTNO "Textures Need Optimising" tristate:0
	checkbox chkTNODone "Done" tristate:0 offset:[200,-20]
	checkbox chkLNM "LOD Needs Modelling" tristate:0
	checkbox chkLNMDone "Done" tristate:0 offset:[200,-20]
	checkbox chkLNT "LOD Needs Texturing" tristate:0
	checkbox chkLNTDone "Done" tristate:0 offset:[200,-20]
	checkbox chkCNO "Collision Needs Optimising" tristate:0
	checkbox chkCNODone "Done" tristate:0 offset:[200,-20]
	checkbox chkCNA "Collision Needs Attributes" tristate:0
	checkbox chkCNADone "Done" tristate:0 offset:[200,-20]
	checkbox chkSignedOff "Signed Off" tristate:0
	edittext txtNotes "Notes:"

	fn updateSelection = (
	
		-- save the current attributes to the previous selection
		
		listCheckBox = #()
		append listCheckBox chkLDR
		append listCheckBox chkLDRDone
		append listCheckBox chkMNI
		append listCheckBox chkMNIDone
		append listCheckBox chkTNI
		append listCheckBox chkTNIDone
		append listCheckBox chkGNO
		append listCheckBox chkGNODone
		append listCheckBox chkTNO
		append listCheckBox chkTNODone
		append listCheckBox chkLNM
		append listCheckBox chkLNMDone
		append listCheckBox chkLNT
		append listCheckBox chkLNTDone
		append listCheckBox chkCNO
		append listCheckBox chkCNODone
		append listCheckBox chkCNA
		append listCheckBox chkCNADone
		append listCheckBox chkSignedOff
	
		listCheckName = #()
		append listCheckName "Level Design Required"
		append listCheckName "LDR Done"
		append listCheckName "Model Needs Improvement"
		append listCheckName "MNI Done"
		append listCheckName "Texturing Needs Improvement"
		append listCheckName "TNI Done"
		append listCheckName "Geometry Needs Optimising"
		append listCheckName "GNO Done"
		append listCheckName "Textures Needs Optimising"
		append listCheckName "TNO Done"
		append listCheckName "LOD Needs Modelling"
		append listCheckName "LNM Done"
		append listCheckName "LOD Needs Texturing"
		append listCheckName "LNT Done"
		append listCheckName "Collision Needs Optimising"
		append listCheckName "CNO Done"
		append listCheckName "Collision Needs Attributes"
		append listCheckName "CNA Done"
		append listCheckName "Signed Off"
		
		for obj in GTAProjManSelectedItem do (		
			if getattrclass obj == "Gta Object" or getattrclass obj == "GtaAnimHierarchy" then (
				if GTAProjManSelectedItem.count == 1 then (
					setattr obj (getattrindex (getattrclass obj) "Priority") (spnPriority.value as integer)
					setattr obj (getattrindex (getattrclass obj) "Notes") (txtNotes.text)
				)
			
				for i = 1 to listCheckBox.count do (
				
					if listCheckBox[i].tristate != 2 then (
					
						setval = false
				
						if listCheckBox[i].tristate == 1 then (
							setval = true	
						)
						
						setattr obj (getattrindex (getattrclass obj) listCheckName[i]) setval
					)
				)
			)
		)
		
		-- reset the attributes in the dialog
	
		labObjName.text = ""
		txtNotes.text = ""
		spnPriority.value = 1.0

		for i = 1 to listCheckBox.count do (
			listCheckBox[i].tristate = 0
		)
	
		GTAProjManSelectedItem = #()
		
		if selection.count > 0 then (
			for newobj in selection do (
				append GTAProjManSelectedItem newobj
			)
		)
		
		if GtaProjManUtil != undefined then (
			GtaProjManUtil.rollouts[1].updateSelection()
		)
		
		-- try and load the currently selected objects attributes
	
		if GTAProjManSelectedItem.count < 1 then (
			labObjName.text = "none selected"
			for i = 1 to listCheckBox.count do (
				listCheckBox[i].enabled = false
			)
			spnPriority.enabled = false
			txtNotes.enabled = false
			return 1
		) else (
			for i = 1 to listCheckBox.count do (
				listCheckBox[i].enabled = true
			)
		)
	
		-- dont allow editng of certain attributes if this flag is set to false
	
		if GtaProjManAllowSetup == false then (
			listCheckBox[1].enabled = false
			listCheckBox[3].enabled = false
			listCheckBox[5].enabled = false
			listCheckBox[7].enabled = false
			listCheckBox[9].enabled = false
			listCheckBox[11].enabled = false
			listCheckBox[13].enabled = false
			listCheckBox[15].enabled = false
			listCheckBox[17].enabled = false
		)
	
		obj = GTAProjManSelectedItem[1]
	
		if GTAProjManSelectedItem.count == 1 then (
			labObjName.text = obj.name
			spnPriority.enabled = true
			txtNotes.enabled = true
		) else (
			labObjName.text = "multiple selected"
			spnPriority.enabled = false
			txtNotes.enabled = false
		)
		
		if getattrclass obj == "Gta Object" or getattrclass obj == "GtaAnimHierarchy" then (
			for i = 1 to listCheckBox.count do (		
				if getattr obj (getattrindex (getattrclass obj) listCheckName[i]) == true then (
					listCheckBox[i].tristate = 1
				) else (
					listCheckBox[i].tristate = 0
				)			
			)
			
			if GTAProjManSelectedItem.count == 1 then (
				spnPriority.value = getattr obj (getattrindex (getattrclass obj) "Priority")
				txtNotes.text = getattr obj (getattrindex (getattrclass obj) "Notes")
			)
		)
		
		if GTAProjManSelectedItem.count > 1 then (
				
			for j = 2 to GTAProjManSelectedItem.count do (
			
				obj = GTAProjManSelectedItem[j]
			
				if getattrclass obj == "Gta Object" or getattrclass obj == "GtaAnimHierarchy" then (
					for i = 1 to listCheckBox.count do (							
						if getattr obj (getattrindex (getattrclass obj) listCheckName[i]) == true then (
							if listCheckBox[i].tristate == 0 then (
								listCheckBox[i].tristate = 2
							)
						) else (
							if listCheckBox[i].tristate == 1 then (
								listCheckBox[i].tristate = 2
							)
						)			
					)
				)				
			)
		)
	)

	on GtaProjManRoll open do (
		updateSelection()
		callbacks.addscript #selectionSetChanged "GtaProjManUtil.rollouts[2].updateSelection()" id:#pickpmselect
	)
	
	on GtaProjManRoll close do (
		callbacks.removescripts id:#pickpmselect
	)
)

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- SCENE NOTES
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout GtaMapNotesRoll "Scene Notes" 
(
	--UI
	listbox usr_notes "Notes:" height:28

	-- events
	on usr_notes selected item do (

		if (notebjs[item] != undefined) then (
			select notebjs[item]
			max zoomext sel
		)
	)

)

fn GtaAddObject obj = (
	note = getuserprop obj "note"
	
	if note != undefined then (
		append usr_notes (obj.name + " " + note)
		append notebjs obj
	)
)

fn GtaDoChildren parent = (
	for obj in parent.children do (
		GtaAddObject obj
		GtaDoChildren obj
	)
)

GtaDoChildren rootnode

try CloseRolloutFloater GtaProjManUtil catch()
GtaProjManUtil = newRolloutFloater "Project Management" 300 540 50 96
addRollout GtaProjManListRoll GtaProjManUtil
addRollout GtaProjManRoll GtaProjManUtil
addRollout GtaMapNotesRoll GtaProjManUtil
GtaMapNotesRoll.usr_notes.items = usr_notes