-------------------------------------------------------------------------------------
-- Rollouts tailored to various object-types:
-------------------------------------------------------------------------------------

global RsAttrEdit_DEBUGGING
if (RsAttrEdit_DEBUGGING == True) do 
(
	callbacks.removeScripts id:#RsAttrEditCallback
	try (destroyDialog RsAttrEditMainRoll) catch()	
)
RsAttrEdit_DEBUGGING = False -- Set to true to open attribute editor when this script is run

-- Custom alternative names for attributes
--	(The original 'official' names don't necessarily match their current purpose)
global RsAttrEdit_AttribAliases = #()

-- Add project-specific attribute-aliases:
if (matchPattern gRsProject.FriendlyName pattern:"*rdr*") do 
(
	append RsAttrEdit_AttribAliases \
	(
		dataPair Type:"VehicleNode" Aliases:\
		#(
			dataPair Name:"Off Road" Alias:"No Wagons", 
			dataPair Name:"Left Turns Only" Alias:"No Horses", 
			dataPair Name:"Slip Lane" Alias:"Single File"
		)
	)
)

-- Array of functions defining the custom tab-names/rollouts for each attrib-class:
--	These rollouts are defined later in this script.
fn RsAttrEdit_getTypeRollTabs = 
(
	-- Set up non-tab-specific attribute-change commands:
	::RsAttrEdit_RageParticles()
	::RsAttrEdit_GtaObj_Markers()
	::RsAttrEdit_GtaTimeCycles()

	-- Set up custom tabs:
	-- ('tabs' items can be links to rollouts, or to functions that will supply them)	
	return \
	#(
		dataPair type:"Gta Object" tabs:#(::RsAttrEdit_GtaObj_lodTabInfo,::RsAttrEdit_GtaObj_tagsTabInfo,::RsAttrEdit_GtaObj_renderTabInfo),
		dataPair type:"Gta Collision" tabs:#(::RsAttrEdit_ColObj_colTabInfo),
		dataPair type:"Gta CarGen" tabs:#(::RsAttrEdit_CarGens)
	)
)

rollout RsAttrEdit_GtaObj_renderRoll "Render:"
(
	local flagNames = #("Dont cast shadows", "Shadow Only", "Dont Render In Shadows", "Only Render In Shadows",			--4
						"Static Shadows", "Dynamic Shadows",															--6
						"Dont Render In Reflections", "Only Render In Reflections", "Dont Render In Reflections On PC",	---9
						"Dont Render In Water Reflections", "Only Render In Water Reflections",							--11
						"Dont Render In Mirror Reflections", "Only Render In Mirror Reflections",						--13
						"Mesh", "Shadow", "Paraboloid Reflection", "Mirror", "Water",									--18
						"Has Pre-Reflected Water Proxy", "Has Drawable Proxy For Water Reflections")					--20
						
	
	local flagAttrIdx = #()
						
	local exclusiveFlags = #{2, 3, 4, 7, 8, 10, 11, 12, 13}
	local exclusionList = #(	DataPair idx:2 exclusion:#(1),								--Shadow Only
								DataPair idx:3 exclusion:#(4),								--Dont Render In Shadows
								DataPair idx:4 exclusion:#(3,8,9,11,13),					--Only Render In Shadows
								DataPair idx:7 exclusion:#(8),								--Dont Render In Reflections
								DataPair idx:8 exclusion:#(7,9,11,13),						--Only Render In Reflections
								DataPair idx:10 exclusion:#(11),							--Dont Render In Water Reflections
								DataPair idx:11 exclusion:#(3,4,8,10,13),					--Only Render In Water Reflections
								DataPair idx:12 exclusion:#(13),							--Dont Render In Mirror Reflections
								DataPair idx:13 exclusion:#(3,4,8,9,11,12)					--Only Render In Mirror Reflections
							)
								
	local attrFlags = #{}
								
	--mesh shadow reflect mirror water
	local flagSwitches = #{}
								
	group "EntityType"
	(
		--Shadows
		checkbox chkDontRenderInShadows "Dont Render In Shadows" across:2 offset:[0,25]
		checkbox chkOnlyRenderInShadows "Only Render In Shadows" offset:[2,25]
		
		checkbox chkStaticShadows "Static Shadows" across:2
		checkbox chkDynamicShadows "Dynamic Shadows" offset:[2, 0]
		
		groupbox grpShadows "Shadows:" width:(RsAttrEdit_GtaObj_renderRoll.width - 16) height:65 offset:[-5, -60]
		
		--Reflections
		checkbox chkDontRenderInReflections "Dont Render In Reflections" across:2 offset:[0,25]
		checkbox chkOnlyRenderInReflections "Only Render In Reflections" offset:[2,25]
		
		checkbox chkDontRenderInWaterReflections "Dont Render In Water Reflections" across:2 width:180
		checkbox chkOnlyRenderInWaterReflections "Only Render In Water Reflections" offset:[2, 0]
		
		checkbox chkDontRenderInMirrorReflections "Dont Render In Mirror Reflections" across:2 width:180
		checkbox chkOnlyRenderInMirrorReflections "Only Render In Mirror Reflections" offset:[2, 0]
		
		checkbox chkHasPreReflectedWaterProxy "Has Pre-Reflected Water Proxy"
		checkbox chkHasDrawableProxyForWaterReflections "Has Drawable Proxy For Water Reflections"
		
		groupbox grpReflections "Reflections:" width:(RsAttrEdit_GtaObj_renderRoll.width - 16) height:125 offset:[-5, -120]
	)
	
	group "Archetype"
	(
		checkbox chkDontCastShadows "Dont cast shadows" across:2
		checkbox chkShadowOnly "Shadow Only" offset:[2, 0]
	)
	
	group "Model"
	(
		checkbox chkMesh "Mesh"
		checkbox chkShadow "Shadow"
		checkbox chkParaboloid "Paraboloid Reflection"
		checkbox chkMirror "Mirror"
		checkbox chkWater "Water"
	)
	
	
	--////////////////////////////////////////////////////////////
	--	FUNCTIONS
	--////////////////////////////////////////////////////////////
	
	--////////////////////////////////////////////////////////////
	--
	--////////////////////////////////////////////////////////////
	fn getRenderAttrs =
	(
		if selection.count == 0 then return #()
		local obj = selection[1]
		local attrVals = for val=1 to flagAttrIdx.count collect getAttr obj flagAttrIdx[val]
			
		local attrFlags = #{}
		for i=1 to attrVals.count where (attrVals[i] == true) do
		(
			append attrFlags i
		)
		
		--return
		attrFlags		
	)
	
	--////////////////////////////////////////////////////////////
	-- Match the set flags to the related control
	-- and set the state
	--////////////////////////////////////////////////////////////
	fn setControls currentSet =
	(
		local controls = for item in RsAttrEdit_GtaObj_renderRoll.controls where (isKindOf item CheckBoxControl) collect item
		
		for c=1 to controls.count do
		(
			local findIdx = findItem flagNames controls[c].caption
			if (findIdx != 0) then
			(
				if currentSet[findIdx] then
				(
					controls[c].state = true
					controls[c].enabled = true
				)
				else
				(
					controls[c].state = false
				)
			)
			else
			(
				controls[c].state = false
			)
		)
	)
	
	--////////////////////////////////////////////////////////////
	--
	--////////////////////////////////////////////////////////////
	fn exclusiveSwitch obj idx =
	(
		if exclusiveFlags[idx] then
		(
			--find the exclusionlist for this attr idx
			excludeIndices = (for item in exclusionList where (item.idx == idx) collect item.exclusion)[1]
			if (excludeIndices == undefined) do
			(
				--print "excludeIndices == undefined"
				return false
			)
			
			for ex in excludeIndices do
			(
				if(getAttr obj flagAttrIdx[ex]) then
				(
					setAttr obj flagAttrIdx[ex] false
				)
			)
		)
		
		setControls (getRenderAttrs())
	)
	
	--////////////////////////////////////////////////////////////
	--
	--////////////////////////////////////////////////////////////
	fn setRenderAttr control =
	(
		local flagNameIdx = findItem flagNames control.caption
		if flagNameIdx == 0 do
		(
			--print "flagNameIdx == 0"
			return false
		)
			
		for obj in selection where (getAttrClass obj == "Gta Object") do
		(
			setAttr obj flagAttrIdx[flagNameIdx] control.state
			
			--if  exclusive attrs
			exclusiveSwitch obj flagNameIdx
		)
	)
	
	--////////////////////////////////////////////////////////////
	--
	--////////////////////////////////////////////////////////////
	fn getFlagsRenderSwitch flags =
	(
		local names = #()
		for item in renderSwitches do
		(
			if ((item.flags * flags).numberSet != 0) do append names item.name
		)
		
		names
	)
	
	--////////////////////////////////////////////////////////////
	--
	--////////////////////////////////////////////////////////////
	fn getRenderSwitchFlags switchName =
	(
		for item in renderSwitches where (item.name == switchName) do
		(
			return item.flags
		)
		
		--return
		return #{}
	)
	
	--////////////////////////////////////////////////////////////
	--	EVENTS
	--////////////////////////////////////////////////////////////
	on chkDontRenderInShadows changed active do ( setRenderAttr chkDontRenderInShadows )
	on chkOnlyRenderInShadows changed active do ( setRenderAttr chkOnlyRenderInShadows )
	on chkStaticShadows changed active do ( setRenderAttr chkStaticShadows )
	on chkDynamicShadows changed active do ( setRenderAttr chkDynamicShadows )
		
	on chkDontRenderInReflections changed active do ( setRenderAttr chkDontRenderInReflections )
	on chkOnlyRenderInReflections changed active do ( setRenderAttr chkOnlyRenderInReflections )
		
	on chkDontRenderInWaterReflections changed active do ( setRenderAttr chkDontRenderInWaterReflections )
	on chkOnlyRenderInWaterReflections changed active do ( setRenderAttr chkOnlyRenderInWaterReflections )
		
	on chkDontRenderInMirrorReflections changed active do ( setRenderAttr chkDontRenderInMirrorReflections )
	on chkOnlyRenderInMirrorReflections changed active do ( setRenderAttr chkOnlyRenderInMirrorReflections )
	
	on chkHasPreReflectedWaterProxy changed active do ( setRenderAttr chkHasPreReflectedWaterProxy )
	on chkHasDrawableProxyForWaterReflections changed active do ( setRenderAttr chkHasDrawableProxyForWaterReflections )
	
	on chkDontCastShadows changed active do ( setRenderAttr chkDontCastShadows )
	on chkShadowOnly changed active do ( setRenderAttr chkShadowOnly )
	
	on chkMesh changed active do ( setRenderAttr chkMesh )
	on chkShadow changed active do ( setRenderAttr chkShadow )
	on chkParaboloid changed active do ( setRenderAttr chkParaboloid )
	on chkMirror changed active do ( setRenderAttr chkMirror )
	on chkWater changed active do ( setRenderAttr chkWater )
	
	
	--////////////////////////////////////////////////////////////
	--	On open
	--////////////////////////////////////////////////////////////
	on RsAttrEdit_GtaObj_renderRoll open do
	(
		--get the attr indices
		flagAttrIdx = for attr in flagNames collect (getAttrIndex "Gta Object" attr)
		
		local currentSet = getRenderAttrs()
		--format "currentSet: %\n" currentSet
		
		setControls currentSet
	)
)

rollout RsAttrEdit_GtaObj_tagsRoll "Tags:"
(
	local sceneTags, sceneTagObjs, cellTagString
	
	editText txtObjTags "Add tag(s):" align:#left width:(RsAttrEdit_GtaObj_tagsRoll.width - 9) offset:[-8,-4]
	dotNetControl tagGridView "System.Windows.Forms.DataGridView" align:#left width:(RsAttrEdit_GtaObj_tagsRoll.width - 7) height:160 offset:[-10,0]
	
	local tagIdx = getattrindex "Gta Object" "Tags"
	
	local textCol = (colorMan.getColor #windowText) * 255
	local windowCol = (colorMan.getColor #window) * 255
	
	local DNknownColour = dotNetClass "system.Drawing.KnownColor"
	local DNcolour = dotNetClass "System.Drawing.Color"
	
	local textFont
	local textColour = DNcolour.FromArgb textCol[1] textCol[2] textCol[3]
	local backColour = DNcolour.FromArgb windowCol[1] windowCol[2] windowCol[3]
	local selColour = DNcolour.fromKnownColor DNknownColour.MenuHighlight
	local halfSelColour = DNcolour.FromArgb ((backColour.r + selColour.r) / 2) ((backColour.g + selColour.g) / 2) ((backColour.b + selColour.b) / 2)
	
	local textBrush = dotNetObject "System.Drawing.SolidBrush" textColour
	local backBrush = dotNetObject "System.Drawing.SolidBrush" backColour
	local selectBrush = dotNetObject "System.Drawing.SolidBrush" selColour
	local halfSelBrush = dotNetObject "System.Drawing.SolidBrush" halfSelColour
	
	-- Number of tag-columns to use: (plus their checkboxes)
	local tagColumnCount = 3
	
	fn getObjTags objs tagCount:#() tagNodes:#() = 
	(
		local tags = #()
		
		for obj in objs do 
		(
			local isSelected = obj.isSelected
			local tagString = getattr obj tagIdx
			
			if (tagString != undefined) do 
			(
				local tagList = makeUniqueArray (filterString tagString ", ")
				
				for tag in tagList do 
				(
					local tagNum = finditem tags tag
					
					if (tagNum == 0) do 
					(
						append tags tag
						append tagCount 0
						append tagNodes #()
						tagNum = tags.count
					)
					
					append tagNodes[tagNum] obj
					
					if isSelected do 
					(
						tagCount[tagNum] += 1
					)
				)
			)
		)
		
		return tags
	)
	
	fn init = 
	(
		tagGridView.rows.clear()
		
		local selTagCount = #()
		local sceneObjs = for obj in geometry where (getattrclass obj == "Gta Object") collect obj
		sceneTagObjs = #()
		sceneTags = getObjTags sceneObjs tagCount:selTagCount tagNodes:sceneTagObjs
		
		local sortedTags = sort (deepCopy sceneTags)
		local sortedTagCount = for tag in sortedTags collect selTagCount[findItem sceneTags tag]
		
		local colNum = 0
		local tagRow = #()
		local rowSel = #()
		
		for tagNum = 1 to sceneTags.count do 
		(
			local tag = sortedTags[tagNum]
			local selTagCountVal = sortedTagCount[tagNum]

			local tagVal = case of 
			(
				(selTagCountVal == 0):false
				(selTagCountVal == RsAttrEditMainRoll.nodes.count):true
				default:undefined
			)
			
			append tagRow tag
			append rowSel tagVal

			-- Add tags to grid every time the end of a row has been reached:
			if ((colNum +=1) == tagColumnCount) or (tagNum == sceneTags.count) do 
			(
				local rowIdx = (tagGridView.rows.add tagRow)
				
				-- Add tag-values to cells:
				local row = tagGridView.rows.item[rowIdx]
				local rowTagNum = 0
				for cellNum = 1 to tagRow.count do 
				(
					row.cells.item[cellNum - 1].tag = rowSel[cellNum]
				)

				colNum = 0
				tagRow = #()
				rowSel = #()
			)
		)
		
		tagGridView.AutoResizeColumns()
		
		-- Space out columns:
		local extraWidth = tagGridView.width - tagGridView.preferredsize.width
		if (extraWidth > 0) do 
		(
			local extraCellWidth = extraWidth / (tagColumnCount - 1)
			
			for colIdx = 0 to (tagColumnCount - 2) do 
			(
				tagGridView.columns.item[colIdx].width += extraCellWidth
			)
		)
	)
	
	fn setTags nodes tags deleteTag:false = 
	(
		local changeMade = false
		
		for obj in nodes do 
		(
			local tagString =  getAttr obj tagIdx
			
			if (tagString != undefined) do 
			(
				local tagsArray = filterString (toLower tagString) ", "
				
				if deleteTag then 
				(
					for tag in tags do 
					(
						local findTag = -1
						while (findTag != 0) do 
						(
							findTag = findItem tagsArray tag
							
							if (findTag != 0) do 
							(
								deleteItem tagsArray findTag
								changeMade = true
							)
						)
					)
				)
				else 
				(
					for tag in tags do 
					(
						if (appendIfUnique tagsArray tag) do 
						(
							changeMade = true
						)
					)
				)
				
				local tagString = RsStringArray tagsArray
				setAttr obj tagIdx tagString
			)
		)
		
		if changeMade do 
		(
			init()
		)
	)
	
	on tagGridView CellPainting arg do
	(
		if (arg.RowIndex != -1) and (arg.ColumnIndex != -1) do 
		(
			local drawCell = tagGridView.rows.item[arg.RowIndex].cells.item[arg.ColumnIndex]
			
			if (drawCell.value != undefined) do 
			(
				arg.handled = true
				
				local cellRect = arg.CellBounds
				
				if (arg.ColumnIndex == (tagGridView.columnCount - 1)) do 
				(
					cellRect.width = tagGridView.width - cellRect.left
				)

				local cellBrush = case drawCell.tag of 
				(
					undefined:halfSelBrush
					true:selectBrush
					false:backBrush
				)
				
				arg.graphics.FillRectangle backBrush cellRect
				arg.graphics.FillRectangle cellBrush (cellRect.left + 1) (cellRect.top + 1) (cellRect.width - 2) (cellRect.height - 2)

				local writePos = [cellRect.x + 3, cellRect.y + 4]
				
				arg.graphics.drawString drawCell.value textFont textBrush writePos.x writePos.y
			)
		)
	)
	
	on tagGridView mouseDown args do 
	(
		local clickPoint = dotNetObject "System.Drawing.Point" args.x args.y
		local clickedItem = tagGridView.hitTest args.x args.y
		
		if (clickedItem != undefined) do 
		(
			local colNum = clickedItem.columnIndex
			local rowNum = clickedItem.rowIndex
			
			if (colNum != -1) and (rowNum != -1) do 
			(			
				local clickedCell = tagGridView.rows.item[rowNum].cells.item[colNum]
				cellTagString = clickedCell.value
				
				if (cellTagString != undefined) do 
				(
					local rightClick = args.button.equals args.button.right
					
					if rightClick then 
					(
						-- Rightclick menu:
						rcmenu RSmenu_tagEditor
						(
							local thisRoll
							
							menuItem itmSelTagObjs "Select objects with tag"
							separator sepA
							menuItem itmTagDel "Remove tag from all objects"
							menuItem itmTagsDelAll "Remove all tags from objects"
							
							on itmSelTagObjs picked do 
							(
								local tagNum = findItem thisRoll.sceneTags thisRoll.cellTagString
								
								select thisRoll.sceneTagObjs[tagNum]
							)
							
							on itmTagDel picked do 
							(
								thisRoll.setTags geometry #(thisRoll.cellTagString) deleteTag:true
							)
							
							on itmTagsDelAll picked do 
							(
								thisRoll.setTags geometry thisRoll.sceneTags deleteTag:true
							)
							
							on RSmenu_tagEditor open do 
							(
								thisRoll = ::RsAttrEdit_GtaObj_tagsRoll
								itmSelTagObjs.text = ("Select objects with tag \"" + thisRoll.cellTagString + "\"")
								itmTagDel.text = ("Remove tag \"" + thisRoll.cellTagString + "\" from all objects")
							)
						)
						popUpMenu RSmenu_tagEditor
					)
					else 
					(
						local addTag = case clickedCell.tag of 
						(
							undefined:true
							true:false
							false:true
						)
						
						setTags RsAttrEditMainRoll.nodes #(cellTagString) deleteTag:(not addTag)
					)
				)
			)
		)
		
		tagGridView.ClearSelection()
	)
	
	on txtObjTags entered newText do 
	(
		setTags RsAttrEditMainRoll.nodes (makeUniqueArray (filterString (toLower newText) ", \t\n\r"))
		txtObjTags.text = ""
	)
	
	on RsAttrEdit_GtaObj_tagsRoll open do 
	(
		textFont = tagGridView.font
		tagGridView.AllowUserToAddRows = false
		tagGridView.AllowUserToDeleteRows = false
		tagGridView.AllowUserToOrderColumns = false
		tagGridView.AllowUserToResizeRows = false
		tagGridView.AllowUserToResizeColumns = false
		tagGridView.AllowDrop = false
		tagGridView.MultiSelect = false
		
		tagGridView.backgroundColor = backColour
		tagGridView.gridColor = backColour
		
		tagGridView.DefaultCellStyle.foreColor = textColour
		tagGridView.DefaultCellStyle.backColor = backColour
		
		tagGridView.rowHeadersVisible = false
		tagGridView.columnHeadersVisible = false
		
		-- Make tag-columns:
		tagGridView.columnCount = tagColumnCount
		for colIdx = 0 to (tagColumnCount - 1) do 
		(
			tagGridView.columns.item[colIdx].ReadOnly = true
		)
		
		init()
	)
	
	on RsAttrEdit_GtaObj_tagsRoll rolledUp down do 
	(
		RsSettingWrite "RsAttrEdit_GtaObj_tagsRoll" "rollup" (not down)
	)
)

-- Returns tab/rollout-list for Tags tab:
fn RsAttrEdit_GtaObj_tagsTabInfo = 
(
	dataPair name:"Tags" rollouts:#(RsAttrEdit_GtaObj_tagsRoll)
)

-- Returns tab/rollout-list for Render tab:
fn RsAttrEdit_GtaObj_renderTabInfo =
(
	dataPair name:"Render" rollouts:#(RsAttrEdit_GtaObj_renderRoll)
)

-- Returns tab/rollout-list for Collision tab:
fn RsAttrEdit_ColObj_colTabInfo = 
(
	dataPair name:"Collision" rollouts:#(::RsCollEditAttrRollDef)
)

-- Sets up extra commands to run when Rage Particle attributes are edited:
fn RsAttrEdit_RageParticles = 
(
	-- Refresh selected particle-objects every time one of their attributes is changed:
	local cmd = "
	local selObjs = for thatObj in selection where (getAttrClass thatObj == \"RAGE Particle\") collect thatObj
	selObjs.meshObj = undefined
	completeRedraw()
	"
	
	-- Empty 'attrs' list makes command be used for all of class's edited attributes
	-- Priority:10 makes this command be inserted last
	gRsExtraAttrCommands.setCmd #updateParticles class:"RAGE Particle" attrs:#() cmd:cmd priority:10 
)

fn RsAttrEdit_GtaObj_Markers = 
(
	-- Reset viewport-draw 'Markers' list whenever these attributes are changed:
	local cmd = "
	gRsObjViewMarkers.Markers = undefined
	completeRedraw()
	"
	
	-- Priority:0 makes this command be inserted early:
	gRsExtraAttrCommands.setCmd #updateMarkers class:"Gta Object" attrs:#("Level Design Object") cmd:cmd priority:0
	
	gRsObjViewMarkers.Markers
)

fn RsAttrEdit_GtaTimeCycles =
(
	-- Refresh selected particle-objects every time one of their attributes is changed:
	local cmd = "
	for thatObj in selection where (getAttrClass thatObj == \"Gta TimeCycle\") or (getAttrClass thatObj == \"Gta TC Sphere\") do
	(
		thatObj.pushAttrUpdate()
	)
	completeRedraw()
	"
	
	-- Empty 'attrs' list makes command be used for all of class's edited attributes
	-- Priority:10 makes this command be inserted last
	gRsExtraAttrCommands.setCmd #updateTimeCycle class:"Gta TimeCycle" attrs:#() cmd:cmd priority:10
	gRsExtraAttrCommands.setCmd #updateTimeCycleSphere class:"Gta TC Sphere" attrs:#() cmd:cmd priority:10 
)


rollout RsAttrEdit_CarGen_modelsRoll "CarGens:"
(
	-- Lets RsCarGenUIFuncs's functions know that this isn't an object's modifier-tab rollout:
	local IsAttributeEditor
	
	group "Dimensions:"
	(
		spinner SpnWidth "Width:" range:[0.1,500,0.1] across:3
		spinner SpnDepth "Depth:" range:[0.1,500,0.1]
		spinner SpnHeight "Height:" range:[0.1,500,0.1]
		button BtnDefaults "Use Default" tooltip:"Use default Car-Generator dimensions for selected CarGens" width:130 across:2 align:#right offset:[-2,0]
		button BtnUpdate "Load for Model/Set" tooltip:"Use loaded dimensions for selected CarGens' Models/Model Sets" width:130 align:#left offset:[2,0]
	)
	
	group "Model:"
	(
		dotNetControl lstModels "ComboBox" height:21 offset:[0,-2]
	)
	
	group "Model Set:"
	(
		dotNetControl lstModelSets "ComboBox" height:21
		listbox lstModelsInSet "Vehicles in set:" height:10 readonly:true
	)
	
	button btnReloadMetaFiles "Reload metafiles" Tooltip:"Reload model/set-lists and update bounding-boxes" width:120
	
	-----------------------------------------------------------------------------
	-- Handle dimension-spinners:
	-----------------------------------------------------------------------------
	on SpnWidth changed NewVal do (RsCarGenUIFuncs.SpinnerChanged #Width NewVal)
	on SpnDepth changed NewVal do (RsCarGenUIFuncs.SpinnerChanged #Depth NewVal)
	on SpnHeight changed NewVal do (RsCarGenUIFuncs.SpinnerChanged #Height NewVal)
	
	-----------------------------------------------------------------------------
	-- Don't allow non-navigation keypresses for Comboboxes:
	-----------------------------------------------------------------------------
	on lstModels KeyDown Sender Args do
	(
		RsCarGenUIFuncs.listKeyDown Sender Args
	)
	on lstModelSets KeyDown Sender Args do
	(
		RsCarGenUIFuncs.listKeyDown Sender Args
	)

	-----------------------------------------------------------------------------
	-- Handle selection-changes to lists:
	-----------------------------------------------------------------------------
	on lstModels SelectionChangeCommitted Sender Args do
	(
		RsCarGenUIFuncs.listSelected Sender Args
	)
	on lstModelSets SelectionChangeCommitted Sender Args do
	(
		RsCarGenUIFuncs.listSelected Sender Args
	)
	
	-- Give selected cargens default dimensions:
	on BtnDefaults pressed do 
	(
		RsCarGenUIFuncs.DefaultObjBounds ThisRoll:RsAttrEdit_CarGen_modelsRoll
	)
	-- Give selected cargens dimensions loaded from metafiles:
	on BtnUpdate pressed do 
	(
		RsCarGenUIFuncs.UpdateObjBounds ThisRoll:RsAttrEdit_CarGen_modelsRoll
	)
	-- Force a data-reload:
	on btnReloadMetaFiles pressed do
	(
		RsCarGenUIFuncs.ReloadMetafiles ThisRoll:RsAttrEdit_CarGen_modelsRoll
	)
	
	on RsAttrEdit_CarGen_modelsRoll open do
	(
		RsCarGenUIFuncs.initCtrls ThisRoll:RsAttrEdit_CarGen_modelsRoll
		RsCarGenUIFuncs.UpdateCtrls()
	)
)

-- Sets up extra commands to run when vehicle-generator attributes are edited:
fn RsAttrEdit_CarGens = 
(
	-- Tab-definition:
	local tabRollsInfo = dataPair name:"CarGens" rollouts:#(RsAttrEdit_CarGen_modelsRoll)
	
	-- Refresh cargens whenever their 'model' or 'modelSet' attributes are changed:
	local cmd = "
	for thatObj in getCurrentSelection() where (getAttrClass thatObj == \"Gta CarGen\") do 
	(
		thatObj.updateMesh()
		RsCarGenUIFuncs.UpdateCtrls()
	)
	"
	
	-- Empty 'attrs' list makes command be used for all of class's edited attributes
	-- Priority:10 makes this command be inserted last
	gRsExtraAttrCommands.setCmd #updateParticles class:"Gta CarGen" attrs:#("Model","Model Set") cmd:cmd priority:10
	
	return tabRollsInfo
)


-- Returns tab/rollout-list for LOD tab:
fn RsAttrEdit_GtaObj_lodTabInfo = 
(
	-- Tab-definition:
	local tabRollsInfo = dataPair name:"LODs" rollouts:#()
	
	-- Add lod-attributes rollout-generation options to tab's rollout-list:
	append tabRollsInfo.rollouts \
	(
		RsCreateObjEditRollDef propClassName:"Gta Object" rollType:#attr rollName:"LOD Attributes" \
			includeAttrs:#("LOD distance", "Instance LOD distance", "Low Priority", "Priority", "Child LOD distance", "HD Textures Distance", "Generate Water Reflective LODs", "Ignored By ComboLodder", "Can ComboLod Animation")
	)
	-- (These attributes also need to be synced with list in: RsCreateObjEditRoll_AttrCommands)
	
	-- Set up extra commands for LOD-attribs, injected into generated rollouts when these attribute-names are used.
	--	(these extra commands are used in all generated rollout, not just for LODs tab)
	
	-- Updates the LOD-viewer when any related attributes are changed:
	(
		local cmd = "
		if ((::RsLodViewToolRoll != undefined) and ::RsLodViewToolRoll.open) do 
		(
			::RsLodViewToolRoll.updateLodView()
		);
		"
		gRsExtraAttrCommands.setCmd #updateLodViewer class:"Gta Object" attrs:#("LOD distance", "Instance LOD distance", "Low Priority", "Priority", "Child LOD distance") cmd:cmd priority:10 -- Priority makes this command be inserted last
	)
	
	-- Updates the ComboLodder when any of its related attributes are changed:
	(
		local cmd = "
		if ((::RsLodCombinerTool != undefined) and ::RsLodCombinerTool.open) do 
		(
			::RsLodCombinerTool.nodeEventFunc 0 0 objs:(selection as array)
			::RsLodCombinerTool.nodeEventEndFunc 0 0
		);
		"
		gRsExtraAttrCommands.setCmd #updateComboLodder class:"Gta Object" attrs:
		#("LOD distance", "Child LOD distance", "AO Multiplier", "Artificial AO Multiplier", "Enable Ambient Multiplier", 
				"Low Priority", "Priority", 
				"Generate Water Reflective LODs","Ignored By ComboLodder", 
				"Dont Export", "Dont Add To IPL", "IPL Group") cmd:cmd priority:9
	)

	-- Changing LOD distance will also change for LOD-siblings, and parent's Child Lod Distance:
	(
		local cmd = "
		local lodParents = #(); 
		local lodSibs = #(); 

		for obj in objs do 
		(
			local lodPar = RsSceneLink.getparent LinkType_LOD obj; 
			if (lodPar != undefined) do 
			(
				append lodParents lodPar;
				join lodSibs (for childObj in (RsGetLODChildren lodPar) where (childObj != obj) collect childObj);
			)
		)

		for lodSib in lodSibs where (findItem objs lodSib == 0) do 
		(
			setAttr lodSib idxLodDistance newVal;
		)
		
		lodParents = makeUniqueArray lodParents;
		for lodPar in lodParents do 
		(
			setAttr lodPar idxChildLodDistance newVal;
		)
		"
		gRsExtraAttrCommands.setCmd #changedLodDist class:"Gta Object" attrs:#("LOD distance") cmd:cmd
	)
	
	-- Changing Child LOD Distance will cause objects' LOD children to be updated:
	(
		local cmd = "
			for obj in objs do 
			(
				for childObj in (RsGetLODChildren obj) do 
				(
					setAttr childObj idxInstLodDistance true; 
					setAttr childObj idxLodDistance newVal;
				)
			)
		"
		gRsExtraAttrCommands.setCmd #changedChildLodDist class:"Gta Object" attrs:#("Child LOD distance") cmd:cmd
	)

	return tabRollsInfo
)

if (RsAttrEdit_DEBUGGING) do 
(
 	createDialog RsAttrEditMainRoll style:#(#style_titlebar, #style_resizing, #style_sysmenu)
)
