-- RsGuidCompareDialog
-- @Author Gunnar Droege

global gGuidComparatorFloater = undefined

rollout RsGuidComparisonLabelRoll "What's going on?"
(
	label lbl1 "The scene contains objects with duplicate guids. \nPlease revise carefully which objects are newly merged \nto the scene and can have their guids reset. \n\nNOTE:If you reset guids on objects that have been \nexported before, you might invalidate important data!" height:80 align:#left
	hyperlink help "More info..." address:"https://devstar.rockstargames.com/wiki/index.php/GUID_Compare" color:(color 0 0 255)
)

fn CreateGuidCompareDialog objectPairs = 
(
	local rci = rolloutCreator "RsGuidCompareRoll" "Choose the nodes to RESET the guids on."
	rci.begin()
	local initString = "#("
	local addComma = false
	for dp in objectPairs do 
	(
		if addComma then
			append initString ","
		addComma = true
		append initString ("#(\""+dp.v1+"\",\""+dp.v2+"\")")
	)
	append initString ")"
	rci.addLocal "pairs" init:initString
	local controlindex = 1
	for objPair in objectPairs do
	(
		local controlName = ("control_"+controlindex as string)
		rci.addControl #radioButtons controlName "" paramStr:("labels:pairs["+controlindex as string+"] columns:2")
		rci.addHandler controlName #changed paramStr:"val " codeStr:(\
			"clearSelection();"+
			"for pi=1 to pairs.count do (\n"+
			"	local p = pairs[pi];\n"+
			"	local c =  RsGuidCompareRoll.controls[pi];\n"+
			"	local theNode = getNodeByName p[c.state];"+
			"	try( selectMore theNode )catch(); \n"+
			")")
		controlindex+=1
	)

	rci.addHandler #RsGuidCompareRoll #close fiter:on codeStr:\
		(	"for ci=1 to RsGuidCompareRoll.controls.count where "+
			"	(RadioControl==(classof RsGuidCompareRoll.controls[ci])) do ("+
			"		local c = RsGuidCompareRoll.controls[ci]; "+
			"		local n = (getNodeByName (pairs[ci][c.state])); "+
			"		if (isValidNode n) then resetAttrguid n; "+
			"			print (getattrGuid n);"+
			"		)")
	
	rci.addControl #button #btnOk "RESET" paramStr:("align:#center width:100 height:30")
	rci.addHandler #btnOk #pressed codeStr:("try ( closeRolloutFloater gGuidComparatorFloater ) catch()")
	local theRollout = rci.end()
	try ( closeRolloutFloater gGuidComparatorFloater ) catch()
	
	local height = 150+(objectPairs.count*25)
	if height>800 then
		height = 800
	
	gGuidComparatorFloater = NewRolloutFloater "Attribute GUID Compare" 400 height
	addRollout RsGuidComparisonLabelRoll gGuidComparatorFloater
	addRollout theRollout gGuidComparatorFloater
)