-- 
-- SaveDecisionMgr interface
-- 

filein "pipeline/util/SaveDecisionMgr.ms"
filein "pipeline/util/RsDotnet.ms"

global gDefaultEntryNames = #(
"Collision export?",
"Anim export?",
"Geometry export?",
"TXD export?",
"Occlusion export?",
"Sync Export Pipeline Dependencies?",
"These files have changed on Perforce since you last synced.

Would you like to download these files now?

(rightclick list for more options)",
"These files are not checked out.

Do you want to fix this, or cancel export?",
"rebuild iref collision?",
"rebuild xref collision?"
)
global gDefaultEntryOptions = #(
	#(
		"Yes To All",
		"Yes",
		"No",
		"Cancel"
	),
	#(
		"Yes To All",
		"Yes",
		"No",
		"Cancel"
	),
	#(
		"Yes To All",
		"Yes",
		"No",
		"Cancel"
	),
	#(
		"Yes To All",
		"Yes",
		"No",
		"Cancel"
	),
	#(
		"Yes To All",
		"Yes",
		"No",
		"Cancel"
	),
	#(
		"Sync All",
		"Sync Required",
		"Cancel"
	),
	#(
		"Yes",
		"No",
		"No (for rest of session)",
		"Cancel RsRef Load"
	),
	#(
		"Checkout",
		"Make Writable",
		"Cancel"
	),
	#(
		"Yes To All",
		"Yes",
		"No",
		"Cancel"
	),
	#(
		"Yes To All",
		"Yes",
		"No",
		"Cancel"
	)
)
global gDefaultEntryVals = #(
	2,
	2,
	2,
	2,
	2,
	2,
	1, -- load changed files on perforce
	1, -- check out map
	2,
	2
)

try (DestroyDialog RsSaveDecisionRoll) catch()

rollout RsSaveDecisionRoll "SaveDecision Manager"
(
	button buttClear "Delete all entries" align:#left across:3
	button buttDefEntries "Add default entries"
	button buttReload "Refresh display"
	button buttSave "Save to disc" across:2 width:150
	button buttLoad "Load from disc" width:150
	hyperlink helpLink "help" address:"https://devstar.rockstargames.com/wiki/index.php/Decision_Manager"
	dotNetControl decisionListCtrl "RsCustomDataGridView" width:10 height:10 visible:true
	
	local textFont, dingFont, textFontBold
	
	local DNknownColour = dotNetClass "system.Drawing.KnownColor"
	local DNcolour = dotNetClass "System.Drawing.Color"
	
	local selColour = DNcolour.fromKnownColor DNknownColour.MenuHighlight
	
	local textCol = (colorMan.getColor #windowText) * 255
	local windowCol = (colorMan.getColor #window) * 255
	local notLoadedCol = if (windowCol[1] < 128) then (windowCol * 1.5) else (windowCol * 0.85)
	
	local textColour = DNcolour.FromArgb textCol[1] textCol[2] textCol[3]
	local backColour = DNcolour.FromArgb windowCol[1] windowCol[2] windowCol[3]
	
	----------------------------------------------------------------------------------------------------------------
	-- Helper functions
	----------------------------------------------------------------------------------------------------------------
	fn setTexListSize size:[RsSaveDecisionRoll.width, RsSaveDecisionRoll.height] = 
	(
		local bufferVal = decisionListCtrl.pos.x
		decisionListCtrl.width = RsSaveDecisionRoll.width - decisionListCtrl.pos.x - bufferVal
		decisionListCtrl.height = RsSaveDecisionRoll.height - decisionListCtrl.pos.y - bufferVal
		
-- 		lnkHelp.pos.x = decisionListCtrl.width - 20
	)
	fn isCombobox ctrl = 
	(
		(ctrl != undefined) and ((ctrl.gettype()).name == "DataGridViewComboBoxEditingControl")
	)
	fn isButton ctrl = 
	(
		(ctrl != undefined) and ((ctrl.gettype()).name == "DataGridViewButtonEditingControl")
	)
		
	----------------------------------------------------------------------------------------------------------------
	-- functionality
	----------------------------------------------------------------------------------------------------------------
	fn Init = 
	(
		decisionListCtrl.AllowUserToAddRows = false
		decisionListCtrl.AllowUserToDeleteRows = false
		decisionListCtrl.AllowUserToOrderColumns = true
		decisionListCtrl.AllowUserToResizeRows = false
		decisionListCtrl.AllowUserToResizeColumns = false
		decisionListCtrl.AllowDrop = false
		decisionListCtrl.MultiSelect = false

		decisionListCtrl.dock = decisionListCtrl.dock.fill
		decisionListCtrl.DefaultCellStyle.backColor = backColour
		decisionListCtrl.DefaultCellStyle.foreColor = textColour

		textFont = decisionListCtrl.font
		dingFont = dotNetObject "System.Drawing.Font" "Webdings" textFont.size
		textFontBold = dotnetobject "system.drawing.font" textFont (dotnetclass "system.drawing.fontstyle").bold

		decisionListCtrl.columnCount = 1
		decisionListCtrl.EnableHeadersVisualStyles = false
		decisionListCtrl.ColumnHeadersDefaultCellStyle.backColor = backColour
		decisionListCtrl.ColumnHeadersDefaultCellStyle.foreColor = textColour
		decisionListCtrl.ColumnHeadersDefaultCellStyle.font = textFontBold
		
		-- Set up texmap-name column
		local decIDCol = decisionListCtrl.columns.item[0]
		decIDCol.ReadOnly = true
		decIDCol.AutoSizeMode = decIDCol.AutoSizeMode.Fill
		decIDCol.HeaderText = "Decision ID"

		-- Set up texmap-type combobox colum:
		local deleteDecCol = dotNetObject "System.Windows.Forms.DataGridViewButtonColumn"
		deleteDecCol.HeaderText = "Del"
		deleteDecCol.width = 30
		deleteDecCol.DefaultCellStyle.backColor = backColour
		deleteDecCol.DefaultCellStyle.ForeColor = (dotnetClass "System.Drawing.Color").red

		decisionListCtrl.columns.add deleteDecCol

		-- Set up texmap-type combobox colum:
		local decOptionsCol = dotNetObject "System.Windows.Forms.DataGridViewComboBoxColumn"
		decOptionsCol.HeaderText = "Options"
		decOptionsCol.width = 160
		decOptionsCol.DefaultCellStyle.backColor = backColour

		decisionListCtrl.columns.add decOptionsCol

		setTexListSize()
	)
	
	fn AddDecisionRow key decision = 
	(
		local rowData = #(key, "X")
		local newRowNum = decisionListCtrl.rows.add rowData
		local newRow = decisionListCtrl.rows.item newRowNum
		local optionCell = newRow.Cells.item[2]
		optionCell.items.addRange decision.optionLabels
		optionCell.value = decision.optionLabels[decision.val]
	)
	
	fn Update = 
	(
		decisionListCtrl.rows.Clear()
		local decs = ::gRsSaveDecMgr.GetDecisions()
		local enum = decs.GetEnumerator()
		while enum.MoveNext() do
		(
			local decValPair = enum.Current
			AddDecisionRow decValPair.key decValPair.value
		)
	)
	
	fn AddStandardEntries = 
	(
		for entryIndex=1 to gDefaultEntryNames.count do
		(
			::gRsSaveDecMgr.registerDecision gDefaultEntryNames[entryIndex] gDefaultEntryVals[entryIndex] optionLabels:gDefaultEntryOptions[entryIndex]
		)
		Update()
	)
	
	----------------------------------------------------------------------------------------------------------------
	-- Events
	----------------------------------------------------------------------------------------------------------------
	
	fn onDropDownClosed ev arg =
	(		
		local eventRow = decisionListCtrl.rows.item[ev.EditingControlRowIndex]
		local decIdx = eventRow.cells.item[0].value
		local decs = ::gRsSaveDecMgr.GetDecisions()
		local eventCell = eventRow.cells.item[2]
		local newVal = eventCell.items.indexOf eventCell.EditedFormattedValue
		if -1 != newVal then
			(decs.get decIdx).val = newVal+1
	)
	
	on decisionListCtrl CellContentClick ev arg do
	(
		if arg.ColumnIndex == 1 and querybox "Are you sure you want to delete the decision entry?" then
		(
			local eventRow = decisionListCtrl.rows.item[arg.RowIndex]
			local decIdx = eventRow.cells.item[0].value
			local decs = ::gRsSaveDecMgr.GetDecisions()
			decs.Remove decIdx
		)
		Update()
	)

	on decisionListCtrl EditingControlShowing ev arg do
	(
		local ctrl = arg.control
		case (ctrl.gettype()).name of
		(
			"DataGridViewComboBoxEditingControl":
			(
				dotNet.removeAllEventHandlers ctrl
				dotnet.addEventHandler ctrl "DropDownClosed" onDropDownClosed
			)
		)
	)
	
	on RsSaveDecisionRoll open do 
	(
		Init()
		Update()
	)
	on buttClear pressed do
	(
		if querybox "Are you sure you want to delete all decision entries?" then
		(
			local decs = ::gRsSaveDecMgr.GetDecisions()
			decs.Clear()
			Update()
		)
	)
	on RsSaveDecisionRoll resized size do 
	(
		setTexListSize size:size
	)
	on buttDefEntries pressed do
	(
		AddStandardEntries()
	)
	on buttReload pressed do
	(
		Update()
	)
	on buttSave pressed do
	(
		::gRsSaveDecMgr.Serialise()
		Update()
	)
	on buttLoad pressed do
	(
		local decs = ::gRsSaveDecMgr.GetDecisions()
		decs.Clear()
		::gRsSaveDecMgr.Deserialise()
		Update()
	)
)

fn RsCreateDecisionRoll = 
(
	try (DestroyDialog RsSaveDecisionRoll) catch()
	RS_CustomDataGrid forceRecompile:true
	CreateDialog RsSaveDecisionRoll 400 300 style:#(#style_titlebar, #style_resizing, #style_sysmenu, #style_minimizebox, #style_maximizebox)
)

rollout RsDecisionControlRoll "Decision Control"
(
	checkbox checkDecisionsRecording "Recording decisions" checked:(::gRsSaveDecMgr.IsRecording())
	checkbox checkDecisionsActive "Apply decisions" checked:(::gRsSaveDecMgr.IsEnabled())
	button buttShowDecisions "Show decisions" width:160
	
	on checkDecisionsActive changed val do
	(
		::gRsSaveDecMgr.SetEnabled val
	)
	on checkDecisionsRecording changed val do
	(
		::gRsSaveDecMgr.SetRecording val
	)
	on buttShowDecisions pressed do
	(
		RsCreateDecisionRoll()
	)
	on RsDecisionControlRoll rolledUp down do 
	(
		RsSettingWrite "rsdecision" "rollup" (not down)
	)
	on RsDecisionControlRoll close do
	(
		::gRsSaveDecMgr.Serialise()
	)
	on RsDecisionControlRoll open do
	(
		::gRsSaveDecMgr.Deserialise()
	)
)