--
-- File:: drawablelodeditor.ms
-- Description:: RAGE Drawable LOD Editor
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 21 April 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
--filein "pipeline/util/drawablelod.ms" -- loaded on startup by rockstar/util/material.ms

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global LinkType_DRAWLOD = 1
global LinkType_RENDERSIM = 2
-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsClothLodEditorRoll "Cloth LOD Editor"
(
-- 	local assembly = dotNetClass "System.Reflection.Assembly"
-- 	local r = assembly.loadfrom (RsConfigGetBinDir()+"TwoThumbSlider.dll")

	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Cloth_LOD_Editor" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	edittext 	currentItem 	"Current:" enabled:false
	--dotNetControl f1 "System.Windows.Controls.Slider, System.Windows.Controls, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"-- align:#left height:180
	--dotNetControl dnButton "Trackbar" Height:23
	--spinner		spnLodDisthigh 	"LOD distance"
	
	group		"Lower/Higher Detail Object:" (
		label 		dummyLabel_1	"" across:5
		edittext 	txtHDItem 		"higher" enabled:false width:135		offset:[-55,0]
		button 		btnSelectHD	"Select"									 	offset:[30,0]
		button 		btnPickHD		"Pick"									 		offset:[17,0]
		button		btnRemoveHD 	"Remove"									offset:[8,0]
		label 		dummyLabel_2	"" across:5
		edittext	txtLDItem		" lower " enabled:false width:135		offset:[-55,0]
		button 		btnSelectLD 	"Select"									 	offset:[30,0]
		button		btnPickLD 		"Pick"									 		offset:[17,0]
		button		btnRemoveLD	"Remove"									offset:[8,0]
	)
	
	group		"Render mesh chain ref object:" (
		label 		dummyLabel_3	"" across:5
		edittext 	txtItemRender 		"render" enabled:false width:135		offset:[-55,0]
		button 		btnSelectRender		"Select"									 	offset:[30,0]
		button 		btnPickRender			"Pick"									 		offset:[17,0]
		button		btnRemoveRender 	"Remove"									offset:[8,0]
		label 		dummyLabel_4			"" across:5
		edittext 	txtItemSimulation 	"sim      " enabled:false width:135		offset:[-55,0]
		button 		btnSelectSimulation	"Select"									 	offset:[30,0]
		button 		btnPickSimulation		"Pick"									 		offset:[17,0]
		button		btnRemoveSimulation "Remove"									offset:[8,0]
	)

	group 	"Tools:" (
		button 		buttLODDIstances "Setup LOD distances" width:250
		button		buttSetupAttrs "Set cloth attributes on all objects" width:250
		button		btnExpandInY	"Expand Y-Axis" width:250
		button		btnExpandInX	"Expand X-Axis" width:250
		button		btnCollapse		"Collapse" width:250
	)
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	--
	-- name: updateSelection
	-- desc: Function triggered when the selection changes and this rollout is open.
	--
	fn updateSelection = (
		local currentPickSelection = #()
	
		for obj in selection do (
			append currentPickSelection obj
		)	
		
		if ( currentPickSelection.count == 1 and currentPickSelection[1] != undefined ) then 
		(
			currentItem.text = currentPickSelection[1].name

			local hdmodel = RsLodDrawable_GetHigherDetailModel currentPickSelection[1]
			if ( undefined != hdmodel ) then
				txtHDItem.text = ( hdmodel.name )
			else
				txtHDItem.text = "None"

			local ldmodel = RsLodDrawable_GetLowerDetailModel currentPickSelection[1]
			if ( undefined != ldmodel ) then
				txtLDItem.text = ( ldmodel.name )
			else
				txtLDItem.text = "None"

			local rendermodel = RsLodDrawable_GetRenderModel (RsLodDrawable_GetLODGroupHead currentPickSelection[1])
			if ( undefined != rendermodel ) then
				txtItemRender.text = ( rendermodel.name )
			else
				txtItemRender.text = "None"

			local simulationmodel = RsLodDrawable_GetSimulationModel (RsLodDrawable_GetLODGroupHead currentPickSelection[1])
			if ( undefined != simulationmodel ) then
				txtItemSimulation.text = ( simulationmodel.name)
			else
				txtItemSimulation.text = "None"
			
			local expandEnabled = ( undefined != hdmodel or undefined != ldmodel )
			btnExpandInY.enabled = expandEnabled
			btnExpandInX.enabled = expandEnabled
			btnCollapse.enabled = expandEnabled
		) 
		else 
		(
			currentItem.text = "Invalid"
			txtHDItem.text = "None"
			txtLDItem.text = "None"
			btnExpandInY.enabled = false
			btnExpandInX.enabled = false
			btnCollapse.enabled = false
		)
	)
	
	--
	-- name: pickCallback
	-- desc: Function triggered when picking for a new node.
	--
	fn pickCallback obj = (
	
		if ( obj == selection[1] ) then
			return ( false )
		if ( "Gta Object" != ( GetAttrClass obj ) ) then
			return ( false )
			
		return ( true )
	)
	
	--
	-- name: expandHierarchy
	-- desc: Expands a drawable lod hierarchy in the x or y axis.
	--
	fn expandHierarchy obj axis:#xaxis = (
	
		local lowestDetailObj = obj
		while ( RsLodDrawable_HasLowerDetailModel lowestDetailObj ) do
			lowestDetailObj = ( RsLodDrawable_GetLowerDetailModel lowestDetailObj )
		
		format "Lowest detail model: %\n" lowestDetailObj.name
		
		local nextDetailObj = ( RsLodDrawable_GetHigherDetailModel lowestDetailObj )
		local size = ( lowestDetailObj.max - lowestDetailObj.min )
		local newPos = nextDetailObj.pos
		while ( undefined != nextDetailObj ) do
		(			
			if ( #xaxis == axis ) then
			(
				newPos.x = newPos.x + size.x + 0.5
			)
			else if ( #yaxis == axis ) then
			(
				newPos.y = newPos.y + size.y + 0.5
			)
			nextDetailObj.pos = newPos
			
			nextDetailObj = ( RsLodDrawable_GetHigherDetailModel nextDetailObj )
		)
	)
	
	--
	-- name: collapseHierarchy
	-- desc: Collapses a drawable lod hierarchy aligning the pivot points.
	--
	fn collapseHierarchy obj = (
	
		local lowestDetailObj = obj
		while ( RsLodDrawable_HasLowerDetailModel lowestDetailObj ) do
			lowestDetailObj = ( RsLodDrawable_GetLowerDetailModel lowestDetailObj )
			
		format "Lowest detail model: %\n" lowestDetailObj.name
		local nextDetailObj = ( RsLodDrawable_GetHigherDetailModel lowestDetailObj )
		while ( undefined != nextDetailObj ) do
		(
			nextDetailObj.pos = lowestDetailObj.pos
			nextDetailObj = ( RsLodDrawable_GetHigherDetailModel nextDetailObj )
		)
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	--
	-- event: btnSelectHD button pressed
	-- desc: Select the currently set Higher Detail node.
	--
	on btnSelectHD pressed do
	(
		local toSel = ( RsLodDrawable_GetHigherDetailModel selection[1] )
		if ( undefined != toSel ) then
			select toSel
	)
	
	--
	-- event: btnPickHD button pressed.
	-- desc: Pick a new Higher Detail node.
	--
	on btnPickHD pressed do
	(
		if selection.count!=1 then
		(
			messagebox "Select one and just one object."
			return false
		)
		local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
		local selobj = selection[1]
		callbacks.removescripts id:#DrawableLodEditorSelect
		
		local selHD = pickObject count:1 filter:pickCallback
		if ( undefined != selHD ) then
		(		
			RsLodDrawable_SetLowerDetailModel selHD selobj

			select selObj
			updateSelection()
		)
		callbacks.addscript #selectionSetChanged "RsClothLodEditorRoll.updateSelection()" id:#DrawableLodEditorSelect
	)
	
	--
	-- event: btnRemoveHD button pressed
	-- desc: Remove the Higher Detail node.
	--
	on btnRemoveHD pressed do
	(
		if selection.count!=1 then
		(
			messagebox "Select one and just one object."
			return false
		)
		local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
		local selobj = selection[1]
		callbacks.removescripts id:#DrawableLodEditorSelect
		
		local selHD = ( RsLodDrawable_GetHigherDetailModel selobj )
		if ( undefined != selHD ) then
		(
			RsSceneLink.RemoveContainer linkType_DRAWLOD selHD		
			local idxExprt = getattrindex "Gta Object" "Dont Export"
			local idxIpl = getattrindex "Gta Object" "Dont Add To IPL"
			setattr selobj idxExprt false
			setattr selobj idxIpl false
			select selobj
			updateSelection()
		)
		callbacks.addscript #selectionSetChanged "RsClothLodEditorRoll.updateSelection()" id:#DrawableLodEditorSelect
	)
	
	--
	-- event: btnSelectHD button pressed
	-- desc: Select the currently set Higher Detail node.
	--
	on btnSelectRender pressed do
	(
		local toSel = ( RsLodDrawable_GetRenderModel (RsLodDrawable_GetLODGroupHead selection[1] ))
		if ( undefined != toSel ) then
			select toSel
	)
	
	--
	-- event: btnPickHD button pressed.
	-- desc: Pick a new Higher Detail node.
	--
	on btnPickRender pressed do
	(
		if selection.count!=1 then
		(
			messagebox "Select one and just one object."
			return false
		)
		local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
		local formerSel = selection[1]
		local selobj = RsLodDrawable_GetLODGroupHead formerSel
		callbacks.removescripts id:#DrawableLodEditorSelect
		
		local selRender = pickObject count:1 filter:pickCallback
		if ( undefined != selRender ) then
		(
			selRender = RsLodDrawable_GetLODGroupHead selRender
			RsLodDrawable_SetRenderChildModel selobj selRender

			select formerSel
			updateSelection()
		)
		callbacks.addscript #selectionSetChanged "RsClothLodEditorRoll.updateSelection()" id:#DrawableLodEditorSelect
	)
	
	--
	-- event: btnRemoveHD button pressed
	-- desc: Remove the Higher Detail node.
	--
	on btnRemoveRender pressed do
	(
		if selection.count!=1 then
		(
			messagebox "Select one and just one object."
			return false
		)
		local formerSel = selection[1]
		local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
		local selobj = RsLodDrawable_GetLODGroupHead formerSel
		callbacks.removescripts id:#DrawableLodEditorSelect
		
		local selRender = ( RsLodDrawable_GetRenderModel selobj )
		if ( undefined != selRender ) then
		(
			RsSceneLink.RemoveContainer LinkType_RENDERSIM  selRender
			select formerSel
			updateSelection()
		)
		callbacks.addscript #selectionSetChanged "RsClothLodEditorRoll.updateSelection()" id:#DrawableLodEditorSelect
	)

		--
	-- event: btnSelectHD button pressed
	-- desc: Select the currently set Higher Detail node.
	--
	on btnSelectSimulation pressed do
	(
		local toSel = ( RsLodDrawable_GetSimulationModel (RsLodDrawable_GetLODGroupHead selection[1] ))
		if ( undefined != toSel ) then
			select toSel
	)
	
	--
	-- event: btnPickHD button pressed.
	-- desc: Pick a new Higher Detail node.
	--
	on btnPickSimulation pressed do
	(
		if selection.count!=1 then
		(
			messagebox "Select one and just one object."
			return false
		)
		local formerSel = selection[1]
		local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
		local selobj = RsLodDrawable_GetLODGroupHead formerSel
		callbacks.removescripts id:#DrawableLodEditorSelect
		
		local selSim = pickObject count:1 filter:pickCallback
		print selSim
		if ( undefined != selSim ) then
		(
			selSim = RsLodDrawable_GetLODGroupHead selSim
			RsLodDrawable_SetRenderChildModel selSim selobj

			select formerSel
			updateSelection()
		)
		callbacks.addscript #selectionSetChanged "RsClothLodEditorRoll.updateSelection()" id:#DrawableLodEditorSelect
	)
	
	--
	-- event: btnRemoveHD button pressed
	-- desc: Remove the Higher Detail node.
	--
	on btnRemoveSimulation pressed do
	(
		if selection.count!=1 then
		(
			messagebox "Select one and just one object."
			return false
		)
		local formerSel = selection[1]
		local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
		local selobj = RsLodDrawable_GetLODGroupHead formerSel
		callbacks.removescripts id:#DrawableLodEditorSelect
		
		local selSim = ( RsLodDrawable_GetSimulationModel selobj )
		if ( undefined != selSim ) then
		(
			RsSceneLink.RemoveContainer LinkType_RENDERSIM  selobj
			select formerSel
			updateSelection()
		)
		callbacks.addscript #selectionSetChanged "RsClothLodEditorRoll.updateSelection()" id:#DrawableLodEditorSelect
	)

	--
	-- event: btnSelectLD button pressed
	-- desc: Select the currently set Lower Detail node.
	--
	on btnSelectLD pressed do
	(
		if selection.count!=1 then
		(
			messagebox "Select one and just one object."
			return false
		)
		local toSel = ( RsLodDrawable_GetLowerDetailModel selection[1] )
		if ( undefined != toSel ) then
			select toSel
	)
	
	--
	-- event: btnPickLD button pressed
	-- desc: Pick a new Lower Detail node.
	--
	on btnPickLD pressed do
	(
		if selection.count!=1 then
		(
			messagebox "Select one and just one object."
			return false
		)
		local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
		local selobj = selection[1]
		callbacks.removescripts id:#DrawableLodEditorSelect
		
		local selLD = pickObject count:1 filter:pickCallback
		if ( undefined != selLD ) then
		(		
			RsLodDrawable_SetLowerDetailModel selobj selLD

			select selObj
			updateSelection()
		)
		callbacks.addscript #selectionSetChanged "RsClothLodEditorRoll.updateSelection()" id:#DrawableLodEditorSelect
	)
	
	--
	-- event: btnRemoveLD button pressed
	-- desc: Remove the Lower Detail node.
	--
	on btnRemoveLD pressed do
	(
		if selection.count!=1 then
		(
			messagebox "Select one and just one object."
			return false
		)
		local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
		local selobj = selection[1]
		callbacks.removescripts id:#DrawableLodEditorSelect
		
		local selLD = ( RsLodDrawable_GetLowerDetailModel selobj )
		if ( undefined != selLD ) then
		(
			RsSceneLink.RemoveContainer linkType_DRAWLOD selobj
		
			local idxExprt = getattrindex "Gta Object" "Dont Export"
			local idxIpl = getattrindex "Gta Object" "Dont Add To IPL"
			setattr selLD idxExprt false
			setattr selLD idxIpl false
			select selobj
			updateSelection()
		)
		callbacks.addscript #selectionSetChanged "RsClothLodEditorRoll.updateSelection()" id:#DrawableLodEditorSelect
	)
	
	--
	-- event: btnExpandInY button pressed
	-- desc: Expand drawable hierarchy along y-axis.
	--
	on buttSetupAttrs pressed do
	(
		if selection.count!=1 then
		(
			messagebox "Select one and just one object."
			return false
		)
		local selobj = selection[1]
		if undefined==selobj then return false
		selobj = RsLodDrawable_GetLODGroupHead selobj
		local simChainLink = RsLodDrawable_GetSimulationModel selobj
		if undefined!=simChainLink then
			selobj = simChainLink
		local numGroups = 0
		RsLodDrawable_SetUserProps selobj &numGroups
	)
	
	--
	-- event: btnExpandInY button pressed
	-- desc: Expand drawable hierarchy along y-axis.
	--
	on btnExpandInY	pressed do
	(
		if selection.count!=1 then
		(
			messagebox "Select one and just one object."
			return false
		)
		local selobj = selection[1]
		expandHierarchy selobj axis:#yaxis
	)
	
	--
	-- event: btnExpandInX button pressed
	-- desc: Expand drawable hierarchy along x-axis.
	--
	on btnExpandInX	pressed do
	(
		if selection.count!=1 then
		(
			messagebox "Select one and just one object."
			return false
		)
		local selobj = selection[1]
		expandHierarchy selobj axis:#xaxis
	)
	
	--
	-- name: btnCollapse button pressed
	-- desc: Collapse drawable hierarchy.
	--
	on btnCollapse pressed do
	(
		if selection.count!=1 then
		(
			messagebox "Select one and just one object."
			return false
		)
		local selobj = selection[1]
		collapseHierarchy selobj
	)
	
	on buttLODDIstances pressed do
	(
		RsLodDrawable_CreateLODSetupDialog()
	)
	
	--
	-- event: Rollout open event.
	-- desc: Add the selection change handler.
	--
	on RsClothLodEditorRoll open do (
		updateSelection()
		callbacks.addscript #selectionSetChanged "RsClothLodEditorRoll.updateSelection()" id:#DrawableLodEditorSelect
	)
	
	--
	-- event: Rollout close event.
	-- desc: Remove the selection change handler.
	--
	on RsClothLodEditorRoll close do (
		callbacks.removescripts id:#DrawableLodEditorSelect
	)
)


fn CreateClothLodEditorDialog = 
(
	try 
		DestroyDialog RsClothLodEditorRoll
	catch()
	CreateDialog RsClothLodEditorRoll 280 350
)
-- drawablelodeditor.ms
