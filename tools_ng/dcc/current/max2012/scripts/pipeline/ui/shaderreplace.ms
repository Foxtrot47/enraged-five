--
-- File:: pipeline/ui/shaderreplace.ms
-- Description:: RAGE Shader Replacement Utility
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 25 June 2007 (original)
-- Date:: 2 Septemebr 2009 (added batch mode)
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
--filein "rockstar/util/material.ms" -- loaded on startup by startup.ms

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

--
-- rollout: ShaderReplacerRoll
-- desc:
--
rollout ShaderReplacerRoll "Rage To Rage Shader Replacer"
(
	-------------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------------
	dropdownlist lstAvailableShaders "Selection Shaders:" items:#() width:150 across:2
	button btnRefresh "Refresh" width:80 align:#right
	dropdownlist lstReplaceShader "Replace with:" items:#()
	button btnReplace "Replace in Selection"
		
	-------------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------------
	on ShaderReplacerRoll open do
	(
		local mapShaders = RsGetShaderListForObjects selection
		lstAvailableShaders.items = mapShaders
		
		local shaders = RsGetShaderList()
		-- Post process shaders list
		for i = 1 to shaders.count do
		(
			if ( undefined == findString shaders[i] ".sps" ) then
				shaders[i] += ".sps"
		)
		
		lstReplaceShader.items = shaders
	)

	-- Refresh scene shaders list
	on btnRefresh pressed do
	(		
		local mapShaders = RsGetShaderListForObjects selection
		lstAvailableShaders.items = mapShaders	
	)

	-- Replace shaders in selected objects
	on btnReplace pressed do
	(
		fromShader = lstAvailableShaders.selected
		toShader = lstReplaceShader.selected
		
		format "Replacing all selected uses of Shader: % to Shader: %\n" fromShader toShader
		
		-- Loop through all objects
		for o in selection do
		(
			format "Processing %...\n" o.name
		
			if ( Rage_Shader == classof o.material ) then
			(
				shaderName = RstGetShaderName o.material
				if ( shaderName == fromShader ) then
				(
					format "Updating % material % (% --> %)\n" o.name o.material.name fromShader toShader
					RstSetShaderName o.material toShader
				)
			)
			else if ( Multimaterial == classof o.material ) then
			(
				for submat in o.material.materialList do
				(
					if ( Rage_Shader == classof submat ) then
					(
						shaderName = RstGetShaderName submat
						if ( shaderName == fromShader ) then
						(
							format "Updating % material % (% --> %)\n" o.name o.material.name fromShader toShader
							RstSetShaderName submat toShader
						)
					)
				)
			)
		) -- For each object in selection
		
		local mapShaders = RsGetShaderListForObjects selection
		lstAvailableShaders.items = mapShaders	
	)
)


--
-- rollout: GTAShaderReplacerRoll
-- desc: Utility that replaces GTA4 shaders (gta_ SPS prefix) with non prefix
--       counterpart.
--
rollout GTAShaderReplacerRoll "Batch GTA4 Shader Replacer"
(
	-------------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------------
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Replace_Shaders#Batch_GTA4_Shader_Replacer" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	button btnConvert "Convert" width:100 

	-------------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------------
	
	--
	-- name: ConvertMaterial
	-- desc:
	--
	fn ConvertMaterial validShaders mat errors = (
	
		if ( Rage_Shader == classof mat ) then
		(
			local shaderName = RstGetShaderName mat
			if ( matchPattern shaderName pattern:"gta_*" ignoreCase:false ) then
			(
				-- Determine new name and validate that its available.
				local newShaderName = ( substring shaderName 5 -1 )
				local available = ( findItem validShaders newShaderName )
				if ( -1 == available ) then
				(
					ss = stringStream ""
					format "Cannot switch shader % for % as shader isn't valid." shaderName newShaderName to:ss
					append errors (ss as string)
				)
				else
				(
					RstSetShaderName mat newShaderName
				)
			)
		)
		else if ( Multimaterial == classof mat ) then
		(
			for submat in mat.materialList do
			(
				-- Recursive call
				ConvertMaterial validShaders submat errors
			)
		)	
	)
	
	-------------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------------
	on btnConvert pressed do
	(
		if ( not queryBox "Are you sure you want to convert all materials from GTA4 shaders?" title:"Confirmation" ) then
		(
			return ( false )
		)
	
		local validShaders = ( RsGetShaderList() )	
		local errors = #()
		for m in sceneMaterials do
		(
			ConvertMaterial validShaders m errors
		)
		
		if ( 0 == errors.count ) then
			MessageBox "Conversion successful.  No errors reported."
		else
		(
			MessageBox "Conversion failed.  See MaxScript listener for details."
			for e in errors do
				format "Error: %\n" e
		)
	)
)
-- pipeline/ui/shaderreplace.ms
