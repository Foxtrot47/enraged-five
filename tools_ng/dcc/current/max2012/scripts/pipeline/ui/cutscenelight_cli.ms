-- Author : Mike Wilson (mike.wilson@rockstarnorth.com)

-- cutfileLoader plugin

filein "pipeline/ui/cutscenelight.ms"

inputFileName = systemTools.getEnvVariable "RS_PROJROOT" + "\\tools\\tmp\\cutscenelighting.txt"
inputFile = openFile inputFileName mode:"r"

outputFileName = systemTools.getEnvVariable "RS_PROJROOT" + "\\tools\\tmp\cutscenelighting_results.txt"
outputFile = openFile outputFileName mode:"w"

SetInteractionMode false 
if inputFile != undefined then
(	
	inputScenes = #()
	while eof inputFile == false do
	(
		inputLine = readLine inputFile		
		append inputScenes inputLine
	)

	ExportCutsceneLightingScenes inputScenes
	
	for scene in inputScenes do
	(
		msg = "\n*" + scene
		print msg
		
		if BuildCutscene scene false == false then
		(
			format "% - failed\n" scene to:outputFile 
		)
		else
		(
			format "% - success\n" scene to:outputFile 
		)
	)
	
	close inputFile
)
else
(
	print ("Unable to open file " + inputFileName + ".")
)

close outputFile

--Before finishing, write a temporary file marking that this process has completed.
tempFileName = (systemTools.getEnvVariable "RS_TOOLSROOT") + "\\tmp\\3dsmax_marker.txt"
tempFile = createFile tempFileName 
format "This is a marker text file to denote when a process in 3D Studio Max has finished." to:tempFile
close tempFile 

quitMAX #noprompt
	
