--
-- File:: pipeline/ui/slod2refimporter.ms
-- Description::
--
-- Author:: Jonny Rivers <jonathan.rivers@rockstarnorth.com>
-- Date:: 22 February 2012
--
try (destroyDialog RsSLOD2RefImporterRoll) catch ()

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------

rollout RsSLOD2RefImporterRoll "SLOD2 Ref Importer" width:345 height:495 -- we need to maintain the 23:33 aspect ratio here - JWR
(
	-------------------------------------------------------------------------
	-- Locals
	-------------------------------------------------------------------------
	local idxAdoptMe = getattrindex "Gta Object" "Adopt Me"
	
	-- Containers named in list 'ddlTargetContainer', and their areas (parent-folder names)
	local _sceneConts = #()
	local _sceneContAreas = #()
	
	-- Tree of Map Areas -> Lod_Container files -> Container-LOD objects
	local _contLodData = (DataPair areaNames:#() areaItems:#())
	
	-- The last Area-name 'lbSLOD2Objects' was filtered for, so it doesn't always have to be redone:
	local _shownFilters = Undefined
	local _shownObjNames = #()
	
	local _prevWinSize = Undefined
	
	-------------------------------------------------------------------------
	-- UI Widgets
	-------------------------------------------------------------------------
	-- BANNER --
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:RsSLOD2RefImporterRoll.width
	local bannerStruct = makeRsBanner dn_Panel:rsBannerPanel versionNum:1.15 versionName:"Existing Pram" wiki:"SLOD2_Ref_Importer" filename:(getThisScriptFilename())
	
	label					lblTargetContainer	"Target container:" align:#left offset:[1,0] across:2
	dropdownlist 	ddlTargetContainer	"" readonly:True width:200 align:#right offset:[0,-3]
	multilistbox 		lbSLOD2Objects	 	"Available SLOD2 Objects:" align:#left width:(RsSLOD2RefImporterRoll.width - 27) height:27 across:2
	checkBox			chkFilterByArea		"Filter By Target Container Area" checked:True align:#right offset:[4,-1] tooltip:"Only show container-LODs for selected container's area"
	edittext 			txtFilterName			"Filter By Name:" Text:"*"
	button				btnRefresh				"Refresh" width:100 align:#left across:2 tooltip:"Update lists to match scene"
	button				btnImport				"Import Selected" width:100 enabled:False align:#right tooltip:"Add refs for selected SLOD2 objects to the target container"
	
	-------------------------------------------------------------------------
	-- Methods
	-------------------------------------------------------------------------
	
	
	-- name: GetSLOD2ObjectDefsRecursive
	-- desc: Given an array of Scene.ObjectDef items in <objectDefArray> this method appends any SLOD2 items into <SLOD2ObjectDefArray> and recurses (depth first)
	fn GetSLOD2ObjectDefsRecursive objectDefArray SLOD2ObjectDefArray:#() =
	(
		for objectDef in objectDefArray where (not objectDef.DontExport()) do
		(
			-- is there another way to get the SLOD2 ?
			--	waiting for valid data to test against
			if(objectDef.LODLevel.value__ == objectDef.LODLevel.SLOD2.value__) do -- this conditional is ugly :-( 
			(
				append SLOD2ObjectDefArray objectDef
			)
			
			-- Recurse through object's children:
			GetSLOD2ObjectDefsRecursive objectDef.Children SLOD2ObjectDefArray:SLOD2ObjectDefArray
		)
		
		return SLOD2ObjectDefArray
	)
	
	-- name: GetSLOD2ObjectForMapContent
	-- desc: Given a map content node <containerLODMapNode> this method returns an array of SLOD2 object defs in that scene
	fn GetSLOD2ObjectForMapContent containerLODMapNode =
	(
		local sceneXmlPathname = containerLODMapNode.AbsolutePath
		
		if (not DoesFileExist sceneXmlPathname) do 
		(
			format "WARNING: SceneXml not found: %\n" sceneXmlPathname
			return #()
		)
		
		-- load the scenexml file and find everything with a LOD parent but no LOD children
		local containerLODScene = RsRefFuncs.LoadSceneXml SceneXmlPathname
		
		-- get all the SLOD2 object defs
		local SLOD2ObjectDefArray = #()
		if( containerLODScene.Objects != undefined ) do 
		(
			SLOD2ObjectDefArray = GetSLOD2ObjectDefsRecursive containerLODScene.Objects
		)
		
		return SLOD2ObjectDefArray
	)
	
	fn SyncXmls MapXmlPaths = 
	(
		local RetVal = True
		
		PushPrompt "Checking Perforce for updated XMLs..."
		local ChangedXmls = gRsPerforce.ChangedFiles MapXmlPaths
		PopPrompt()
		
		-- Sync latest xml-files:
		if (ChangedXmls.Count != 0) do 
		(
			local queryText = stringstream ""
			local singular = (ChangedXmls.count == 1)
			format "% a newer version on Perforce.\n\nWould you like to sync % now?" (if singular then "This file has" else "These files have") (if singular then "it" else "them") to:queryText
			
			local SyncChecked = #{1..ChangedXmls.Count}
			
			local queryVal = RsQueryBoxMultiBtn (queryText as string) title:"Updated xmls found on Perforce:" timeout:15 width:540 \ 
				labels:#("Yes", "No", "Cancel Process") defaultBtn:2 cancelBtn:3 \ 
				tooltips:#("Update files from Perforce", "Don't update files from Perforce", "Cancel xml-load process") \ 
				listCheckStates:SyncChecked listItems:ChangedXmls listTitles:#("Sync", "Filename")
			
			case queryVal of 
			(
				-- Sync checked filenames:
				1:
				(
					local SyncFiles = for n = SyncChecked collect ChangedXmls[n]
					
					if (SyncFiles.Count != 0) do 
					(
						local success = gRsPerforce.Sync ChangedXmls clobberAsk:True silent:True showProgress:True progressTitle:"Syncing exterior-map sceneXml files..."
						
						-- Abort if sync was cancelled:
						if (not success) do (RetVal = False)
					)
				)
				-- Abort if 'Cancel' was pressed:
				3:(RetVal = False)
			)
		)
		
		return RetVal
	)
	
	-----------------------------------------------------------------------------------------------
	-- Get list of 'lod_container' files, and list their objects in tree 'contLodData'
	-----------------------------------------------------------------------------------------------
	fn loadContLodData = 
	(
		local areaNames = _contLodData.areaNames
		local areaItems = _contLodData.areaItems
		
		-- Clear existing data:
		AreaNames.count = 0
		AreaItems.count = 0
		
		-- Get list of lod_container xml-nodes:
		local lodContNodes = RsContentTree.ContentTreeHelper.GetAllLodSceneXmlNodes()
		if (lodContNodes.Count == 0) do return #()
		
		-- Ask if user wants to sync xmls:
		local XmlPaths = for Item in LodContNodes collect Item.AbsolutePath
		local Success = (SyncXmls XmlPaths)
		
		if (not Success) do (return False)
		
		ProgressStart ("Reading Lod_Container XMLs")
		
		for n = 1 to LodContNodes.Count while (ProgressUpdate (100.0 * n / LodContNodes.Count)) do 
		(
			local XmlNode = LodContNodes[n]
			
			-- Use lod_container-node's parent-folder as its Area Name:
			local areaName = toLower (pathConfig.stripPathToLeaf xmlNode.Directory)
			
			-- Get/Add item for this Area:
			local areaIdx = findItem areaNames areaName
			if (areaIdx == 0) do 
			(
				append AreaNames AreaName
				append AreaItems #()
				areaIdx = areaNames.count
			)
			
			local lodObjDefs = GetSLOD2ObjectForMapContent xmlNode
			
			--format "%/% (%)\n" areaName xmlNode.name lodObjDefs.count
			
			-- Collect data if xml lists valid SLOD2 objects:
			if (lodObjDefs.count != 0) do 
			(
				local lodNames = for item in lodObjDefs collect item.name
				sort lodNames
				
				append AreaItems[areaIdx] (dataPair Filepath:xmlNode.AbsolutePath LodNames:lodNames)
			)
		)
		
		ProgressEnd()
		
		return _contLodData
	)
	
	--name: IsValidTargetContainerContent
	--desc: Returns true if <targetContainerMapContentNode> is of a valid maptype (container or container_props)
	fn IsValidTargetContainerContent targetContainerMapContentNode =
	(	
		local result = false
	
		if (targetContainerMapContentNode != undefined) do 
		(
			local maptype = RsContentTree.GetMapType (targetContainerMapContentNode)
			
			result = (maptype == "map_container")
		)
		
		return result
	)
	
	-- name: OnSLOD2ObjectsSelectionChanged
	-- desc: Sets up UI other UI elements based on the current SLOD2 object selection
	fn OnSLOD2ObjectsSelectionChanged =
	(
		btnImport.enabled = (ddlTargetContainer.Selection != 0) and ( lbSLOD2Objects.selection.numberSet > 0 )
	)
	
	-- name: showFilteredLodList
	-- desc: Populates lbSLOD2Objects.items based on the selected target container
	fn showFilteredLodList filterByArea:(chkFilterByArea.checked) NameFilter:(TxtFilterName.text) = 
	(
		-- Get area-filter value:
		local newAreaFilter = undefined
		
		if filterByArea then 
		(
			local contNum = ddlTargetContainer.Selection
			if (contNum != 0) do 
				newAreaFilter = _sceneContAreas[contNum]
		)
		else 
		(
			newAreaFilter = -1
		)
		
		-- Use this value to compare against previous run:
		local NewFilters = (newAreaFilter as String) + ":"
		
		-- Are we filtering lodnames?
		local DoNameFilter = ((NameFilter != "") and (NameFilter != "*"))
		if DoNameFilter do 
		(
			NameFilter = ("*" + NameFilter + "*")
			append NewFilters NameFilter
		)
		
		-- Only bother changing object-list on target-change if filter-values have changed:
		if (newFilters == _shownFilters) do return OK
		
		_shownFilters = newFilters
		
		lbSLOD2Objects.items = #()
		_shownAreaFilter = newAreaFilter
		
		local showAreaIdxs = #{}
		
		-- Get indexes of lodfile-items that match current filter:
		if (not filterByArea) then 
		(
			-- Show for all Areas:
			showAreaIdxs.count = _contLodData.areaItems.count
			showAreaIdxs = (-showAreaIdxs)
		)
		else 
		(
			-- Show for selected Area:
			local areaIdx = FindItem _contLodData.AreaNames newAreaFilter
			if (areaIdx != 0) do 
			(
				showAreaIdxs = #{areaIdx}
			)
		)
		
		local lodItems = #()
		
		for areaIdx in showAreaIdxs do 
		(
			local AreaName = _contLodData.AreaNames[areaIdx]
			local AreaSuffix = ("[" + AreaName + "][")
			local FileItems = _contLodData.AreaItems[areaIdx]
			
			for FileItem in FileItems do 
			(
				local FilePath = FileItem.FilePath
				local Filename = (getFilenameFile FilePath)
				local FileSuffix = (AreaSuffix + Filename + "] ")
				
				-- Add 'DLC' prefix, if this from a DLC file:
				local dlcProj = (RsContentTree.GetDLCProject FilePath)
				if (dlcProj != undefined) and dlcProj.IsDLC do 
				(
					FileSuffix = ("[DLC]" + FileSuffix)
				)
				
				for LodName in FileItem.LodNames where ((not DoNameFilter) or (matchPattern LodName Pattern:NameFilter)) do 
				(
					append lodItems (dataPair Name:LodName ShowName:(FileSuffix + LodName))
				)
			)
		)
		
		-- Sort lod-objects list alphabetically:
		fn sortShowNames v1 v2 = 
		(
			striCmp v1.ShowName v2.ShowName
		)
		qsort lodItems sortShowNames
		
		_shownObjNames = (for item in lodItems collect item.Name)
		lbSLOD2Objects.Items = (for item in lodItems collect item.ShowName)
		lbSLOD2Objects.Selection = #{}
		
		OnSLOD2ObjectsSelectionChanged()
	)
	
	-- Returns list of open containers in scene:
	fn GetOpenConts = 
	(
		for Obj in Helpers where (isKindOf Obj Container) and (Obj.IsOpen()) collect Obj
	)
	
	-- name: Init
	-- desc: Resets the rollout to its default state
	fn Init = 
	(
		_shownAreaFilter = undefined
		
		-- Load xml-data into tree 'contLodData':
		LoadContLodData()
		
		-- Get list of containers in scene that can be given container-lods:
		local contDataItems = #()
		
		for ContObj in (GetOpenConts()) do 
		(
			-- Get content-node for this containername:
			local ContentNode = RsContentTree.FindSourceFilenodeFromBasename ContObj.Name
			
			-- Is this a 'map_container' map?
			if (IsValidTargetContainerContent ContentNode) do 
			(
				local areaName = toLower (pathConfig.stripPathToLeaf contentNode.Directory)
				local listName = ("[" + areaName + "] " + ContObj.Name)
				
				-- Collect listName, container and area-name:
				append contDataItems (dataPair name:listName data:(dataPair Cont:ContObj Area:areaName))
			)
		)
		
		-- Sort container-data list alphabetically:
		fn sortNames v1 v2 = 
		(
			striCmp v1.Name v2.Name
		)
		qsort contDataItems sortNames
		
		-- Store data in list to arrays, and to 'ddlTargetContainer' control's list:
		_sceneConts = for item in contDataItems collect item.Data.Cont
		_sceneContAreas = for item in contDataItems collect item.Data.Area
		ddlTargetContainer.items = for item in contDataItems collect item.Name
		
		-- Initially select first item on target-containers list, if it has any:
		if (contDataItems.count > 0) do 
		(
			ddlTargetContainer.selection = 1
		)
		
		-- Update LOD-objects list:
		showFilteredLodList()
		
		return OK
	)
	
	-- name: GetAllLODChildren
	-- desc: given a list of objects, returns all LOD objects at the next level down
	fn GetAllLODChildren objlist =
	(
		allLODChildren = #()
		
		for obj in objlist do
		(
			LODChildrenThisObj = #()
			RsSceneLink.getchildren LinkType_LOD obj &LODChildrenThisObj
			
			for LODChildThisObj in LODChildrenThisObj do
			(
				append allLODChildren LODChildThisObj
			)
		)
		
		allLODChildren
	)
	
	-- name: GetLodLevel
	-- desc: gets the lod level for a given node (0 - orphaned, 1 - HD, 2 - LOD, 3 - SLOD1, etc)
	fn GetLODLevel obj =
	(
		local levelsBelow = 0
		
		lodChildren = GetAllLODChildren #( obj )
		while lodChildren.count > 0 do
		(
			levelsBelow += 1
			lodChildren = GetAllLODChildren lodChildren
		)
		
		lodlevel = 0
		if( levelsBelow > 0 ) then
		(
			lodlevel = levelsBelow + 1
		)
		else if (RsSceneLink.getparent != undefined) then
		(
			lodlevel = 1
		)

		lodlevel
	)
	
	--name: GetContainerObjects
	--desc: gets all exportable objects below the container 'containerObj'
	fn GetContainerObjects containerObj = 
	(
		local contObjs = undefined
		
		for Item in (RsMapGetMapContainers()) where (Item.Cont == containerObj) while (contObjs == undefined) do
		(
			contObjs = Item.Objects
		)
		
		if (contObjs == undefined) do 
		(
			contObjs = #()
		)
		
		return contObjs
	)
	
	-- name: ImportNamedRsContainerLODRefNodes
	-- desc: brings a container lod ref node for each entry in <containerLodNodeNames> into the container <targetContainer>
	fn ImportNamedRsContainerLODRefNodes containerLodNodeNames containerObj =
	(
		local result = #()
		
		local RefsActive = (RsRefFuncs.databaseActive toolName:"SLOD2 Ref Importer" LoadAllRefs:False doP4Update:False)
		
		if RefsActive and (containerLodNodeNames.count != 0) do 
		(
			local containerObjects = GetContainerObjects containerObj
			
			local existingObjs = for obj in containerObjects where (isKindOf obj RsContainerLodRef) collect obj
			local existingObjNames = for obj in existingObjs collect (toLower obj.objectName)
			
			suspendEditing()
			local slodObjs = for containerLodNodeName in containerLodNodeNames collect 
			(
				local findNum = findItem existingObjNames containerLodNodeName
				
				if (findNum != 0) then 
				(
					-- Object already exists:
					existingObjs[findNum]
				)
				else 				
				(
					-- Create new object:
					local newObj = RsContainerLodRef()
					
					RSrefFuncs.setObjsToUseName #(newObj) containerLodNodeName includeDelegates:true finalise:false
					
					newObj
				)
			)
			resumeEditing()
			
			local addToContObjs = for obj in slodObjs where (obj.parent != containerObj) collect obj
			containerObj.AddNodesToContent addToContObjs
			
			slodObjs.rebuildMesh = true
			
			result = slodObjs
		)
		
		return result
	)
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------
	
	-- event: Refresh button pressed
	on btnRefresh pressed do
	(
		_shownAreaFilter = undefined
		Init()
	)
	
	-- event: Target container selection changed
	on ddlTargetContainer selected val do
	(
		showFilteredLodList()
	)
	
	-- Filters changed:
	on chkFilterByArea changed State do 
	(
		showFilteredLodList()
	)
	on txtFilterName changed NewVal do 
	(
		showFilteredLodList()
	)
	
	-- event: SLOD2 object selection changed
	on lbSLOD2Objects selected SLOD2Object do
	(
		OnSLOD2ObjectsSelectionChanged()
	)
	
	-- Add selected names as ContainerLod refs:
	on btnImport pressed do
	(
		local SelNum = ddlTargetContainer.selection
		local selCont = _sceneConts[selNum]
		local selNames = for selNum = lbSLOD2Objects.selection collect (ToLower _shownObjNames[selNum])
		
		local importedNodes = ImportNamedRsContainerLODRefNodes selNames selCont
		
		select importedNodes
	)
	
	-- Resize controls to match size
	on rsSLOD2RefImporterRoll resized newSize do 
	(
		local sizeDiff = (newSize - _prevWinSize)
		_prevWinSize = newSize
		
		bannerStruct.width = newSize.x
		lbSLOD2Objects.width += sizeDiff.x
		lbSLOD2Objects.height += sizeDiff.y
		
		for ctrl in #(txtFilterName, btnRefresh, btnImport) do 
			ctrl.pos.y += sizeDiff.y
	)
	
	-- event: The rollout is opening
	on rsSLOD2RefImporterRoll open do
	(
		_prevWinSize = GetDialogSize RsSLOD2RefImporterRoll
		
		-- Initialise tech-art banner:
		bannerStruct.Setup()
		
		Init()
	)
)

-----------------------------------------------------------------------------
-- Entry
-----------------------------------------------------------------------------
CreateDialog RsSLOD2RefImporterRoll style:#(#Style_Resizing, #Style_Titlebar, #Style_SysMenu)
-- pipeline/ui/slod2refimporter.ms
