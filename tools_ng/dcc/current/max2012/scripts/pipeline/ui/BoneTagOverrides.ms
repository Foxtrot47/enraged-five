global RsBoneTagMap = dotnetobject "RSG.MaxUtils.MaxDictionary"
global RsBoneTagMapXml = (::RsConfigGetEtcDir() + "/config/maps/gta5/boneTagMap.xml")

rollout RsBoneTagRoll "Overwrite Bone Tag Definitions"
(
	listbox lstBonetags ""

	group "New Entry"
	(
		editText edtNewBoneTag "Tag:" across:4 width:160
		editText edtNewBonetagID "" width:80 offset:[65,0]
		button btnAddID "Add/Change" offset:[45,0]
		button btnRemove "Remove" offset:[25,0]
	)
	
	fn ReadXml = 
	(
		local xmlDoc = XmlDocument()
		if not doesFileExist RsBoneTagMapXml then
			return false

		RsBoneTagMap.clear()
		xmlDoc.load RsBoneTagMapXml
		rootElem = xmlDoc.document.childNodes.item 1
		local lstItems = #()
		for ci=0 to rootElem.childNodes.count-1 do
		(
			local tagnode = rootElem.childNodes.item ci
			if undefined!=tagnode then
			(
				append lstItems tagnode.name
				edtNewBoneTag.text = tagnode.name
				edtNewBonetagID.text = tagnode.innerText
				RsBoneTagMap.add tagnode.name tagnode.innerText
			)
		)
		lstBonetags.items = lstItems
	)
	
	fn AddToXml = 
	(
		if edtNewBoneTag.text=="" then
		(
			return false
		)
		
		local xmlDoc = XmlDocument()
		local rootElem = undefined
		if doesFileExist RsBoneTagMapXml then
		(
			xmlDoc.load RsBoneTagMapXml
			rootElem = xmlDoc.document.childNodes.item 1
		)
		else
		(
			xmlDoc.init()
			rootElem = xmlDoc.createelement "bonetags" appendTo:xmlDoc.document
		)
		if undefined!=rootElem then
		(
			local foundNode = false
			for ci=0 to rootElem.childNodes.count-1 while not foundNode do
			(
				local tagnode = rootElem.childNodes.item ci
				if tagnode.name == edtNewBoneTag.text then
				(
					tagnode.innerText = edtNewBonetagID.text
					foundNode = true
				)
			)
			if not foundNode then
				xmlDoc.createelement edtNewBoneTag.text appendTo:rootElem value:edtNewBonetagID.text
		)
		
		xmlDoc.save RsBoneTagMapXml
	)
	
	fn RemoveFromXml = 
	(
		if edtNewBoneTag.text=="" then
		(
			return false
		)
		
		local xmlDoc = XmlDocument()
		local rootElem = undefined
		if not doesFileExist RsBoneTagMapXml then
		(
			return false
		)
		xmlDoc.load RsBoneTagMapXml
		rootElem = xmlDoc.document.childNodes.item 1
		if undefined!=rootElem then
		(
			local foundNode = false
			for ci=0 to rootElem.childNodes.count-1 while not foundNode do
			(
				local tagnode = rootElem.childNodes.item ci
				if tagnode.name == edtNewBoneTag.text then
				(
					rootElem.RemoveChild tagnode
					foundNode = true
				)
			)
		)
		
		xmlDoc.save RsBoneTagMapXml
	)
	
	on btnAddID pressed do 
	(
		if RsBoneTagMap.containsKey edtNewBoneTag.text then
			RsBoneTagMap.remove edtNewBoneTag.text
		RsBoneTagMap.add edtNewBoneTag.text edtNewBonetagID.text
		AddToXml()
		ReadXml()
	)
	on btnRemove pressed do 
	(
		if RsBoneTagMap.containsKey edtNewBoneTag.text then
			RsBoneTagMap.remove edtNewBoneTag.text
		RemoveFromXml()
		ReadXml()
	)
	
	on RsBoneTagRoll open do
	(
		ReadXml()
	)
	on lstBonetags selected val do
	(
		if not RsBoneTagMap.containsKey lstBonetags.selected then
		(
			messagebox "Key not kin global map!"
			return false
		)
		edtNewBoneTag.text = lstBonetags.selected
		edtNewBonetagID.text = RsBoneTagMap.item lstBonetags.selected
	)
)

/*
RsRotationBoneTagMap.add "BONETAG_X_AXIS_SLOW" 421
RsRotationBoneTagMap.add "BONETAG_Y_AXIS_SLOW" 422
RsRotationBoneTagMap.add "BONETAG_Z_AXIS_SLOW" 423
RsRotationBoneTagMap.add "BONETAG_X_AXIS_FAST" 424
RsRotationBoneTagMap.add "BONETAG_Y_AXIS_FAST" 425
RsRotationBoneTagMap.add "BONETAG_Z_AXIS_FAST" 426
*/