--
-- File:: pipeline/ui/live_map_editing_game_to_max.ms
-- Description::
--
-- Author:: Jonny Rivers <jonny.rivers@rockstarnorth.com>
-- Date:: 20 February 2013
--
try (destroyDialog RsLiveMapEditingGameToMaxRoll) catch ()
try (unregisterRedrawViewsCallback ::RsLiveMapEditingGameToMaxRoll.OnViewportDraw) catch ()

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------

rollout RsLiveMapEditingGameToMaxRoll "Live Map Editor (Game -> Max)" width:330 height:136
(
	-------------------------------------------------------------------------
	-- Locals
	-------------------------------------------------------------------------
	local gameToMaxLiveEditingServiceClient
	local registeredGTAObjects, registeredObjHandles
	
	local idxGtaObjIPLGroup = getattrindex "Gta Object" "IPL Group"

	-------------------------------------------------------------------------
	-- UI Widgets
	-------------------------------------------------------------------------
	dotNetControl RsBannerPanel "Panel" pos:[0,0] height:32 width:RsLiveMapEditingGameToMaxRoll.width
	local bannerStruct = makeRsBanner dn_Panel:RsBannerPanel wiki:"Live Map Editing (Game to Max)" filename:(getThisScriptFilename())
	
	checkbutton btnConnect "Connect" checked:false width:310
	button btnSync "Sync" enabled:false width:310
	edittext etConnectionStatus "Connection Status:" text:"Not connected to the game" enabled:false readOnly:True width:310
	edittext etUpdateStatus "Update Status:" text:"" enabled:false readOnly:True width:310
	timer timerUpdate interval:200 active:false
	
	-------------------------------------------------------------------------
	-- Methods
	-------------------------------------------------------------------------
	
	-- Returns GUID for object, with brackets stripped:
	fn GetObjGuid obj = 
	(
		local attrGuid = getAttrGuid obj
		attrGuid = substituteString attrGuid "{" ""
		attrGuid = substituteString attrGuid "}" ""
		return attrGuid
	)
	
	fn RegisterAllNodes =
	(
		registeredGTAObjects = #()
		registeredObjHandles = #()

		for thisMap in RsMapGetMapContainers() where (thisMap.name != undefined) do 
		(
			local mapObjs = for obj in thisMap.objects where (isRsRef obj) collect obj
			
			if (mapObjs.count != 0) do 
			(	
				local maxContentNode = RsContentTree.GetContentNode thisMap.filename				
				if ( maxContentNode != undefined ) do
				(
					local mapZipExportNode = RsContentTree.GetMapExportZipNode maxContentNode
					local mapProcessedNode = RsContentTree.GetMapProcessedNode mapZipExportNode
					
					local mapContentName = mapZipExportNode.name
					local mapSectionName = mapProcessedNode.name

					local containerAttrGuid = undefined
					if( thisMap.cont != undefined ) then
					(
						containerAttrGuid = GetObjGuid thisMap.cont
					)
					else
					(
						local sceneXmlNode = RsContentTree.GetMapExportSceneXMLNode mapZipExportNode
						local xmlPathname = sceneXmlNode.absolutepath
						containerAttrGuid = RsMakeBackSlashes xmlPathname
					)
				
					for obj in mapObjs do 
					(
						local nodeAttrGuid = GetObjGuid obj
						
						local objGrpName = mapSectionName
						local imapGroupName = toLower (getattr obj idxGtaObjIPLGroup)
						if( imapGroupName != "undefined" and imapGroupName.count > 0 ) then
						(
							objGrpName = imapGroupName
						)
						
						local dccEntityKey = dotnetobject "RSG.RESTServices.Game.Map.DCCEntityKey" mapContentName objGrpName containerAttrGuid nodeAttrGuid obj.handle
						gameToMaxLiveEditingServiceClient.RegisterMaxNode dccEntityKey maxContentNode
						
						append registeredGTAObjects obj
						append registeredObjHandles obj.handle
					)
				)
			)
		)
	)
	
	fn OnViewportDraw = 
	(
		-- Reset timer-tick:
		timerUpdate.active = True
	)
	
	fn SetControlsEnabled state = 
	(
		unregisterRedrawViewsCallback OnViewportDraw
		
		if state then 
		(
			registerRedrawViewsCallback OnViewportDraw
		)
		else 
		(
			etConnectionStatus.text = "Not connected to the game"
			etUpdateStatus.text = ""
			gameToMaxLiveEditingServiceClient = undefined
		)
		
		btnConnect.state = state
		timerUpdate.active = state
		#(btnSync, etConnectionStatus, etUpdateStatus).enabled = state
		
		return OK
	)
	
	fn OnConnectionStateChanged connected =
	(
		if (connected) then
		(
			if ( not RemoteConnection.IsConnected() ) then
			(
				RemoteConnection.Connect()
			)
			
			local ipAddress = RemoteConnection.GetGameIP()
			
			if ( RemoteConnection.IsConnected() and (ipAddress != undefined) ) then
			(
				append ipAddress RsLiveEditPortSuffix
				etConnectionStatus.text = ("Connected to game at " + ipAddress)
				gameToMaxLiveEditingServiceClient = dotnetobject "RSG.RESTServices.Game.Map.GameToMaxLiveEditingServiceClient" ipAddress
				SetControlsEnabled True
				
				RegisterAllNodes()
			)
			else
			(
				SetControlsEnabled False
				messagebox "No connection to the game could be established"
				btnConnect.state = off
			)
		)
		else
		(
			SetControlsEnabled False
		)
		
		return OK
	)
	
	fn OnSync =
	(
		RegisterAllNodes()
	)
	
	fn OnUpdate =
	(
		format "OnUpdate: %\n" (random 1111 2222)
		local liveUpdates = gameToMaxLiveEditingServiceClient.GetUpdates()

		local numUpdatesApplied = 0
		
		for liveUpdate in liveUpdates do
		(
			local findNum = findItem registeredObjHandles liveUpdate.NodeHandle
			
			if (findNum != 0) do 
			(
				local obj = registeredGTAObjects[findNum]
				
				-- we have to use a temporary to update the transform because matrix3.row# properties are read-only
				workingTransform = matrix3 1
				workingTransform.row1 = point3 liveUpdate.LocalToWorldTransform.A.X liveUpdate.LocalToWorldTransform.A.Y liveUpdate.LocalToWorldTransform.A.Z
				workingTransform.row2 = point3 liveUpdate.LocalToWorldTransform.B.X liveUpdate.LocalToWorldTransform.B.Y liveUpdate.LocalToWorldTransform.B.Z
				workingTransform.row3 = point3 liveUpdate.LocalToWorldTransform.C.X liveUpdate.LocalToWorldTransform.C.Y liveUpdate.LocalToWorldTransform.C.Z
				workingTransform.row4 = point3 liveUpdate.LocalToWorldTransform.D.X liveUpdate.LocalToWorldTransform.D.Y liveUpdate.LocalToWorldTransform.D.Z

				if not (RsCompareMatrix workingTransform obj.transform) do 
				(
					obj.transform = workingTransform
					setNeedsRedraw()
				)
				
				numUpdatesApplied += 1
			)
		)
		
		local msg = stringStream ""
		format "Received % updates.  Applied % updates." liveUpdates.Count numUpdatesApplied to:msg
		
		etUpdateStatus.text = (msg as string)
	)
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------
	
	-- event: The rollout is opening
	on RsLiveMapEditingGameToMaxRoll open do
	(
		-- Initialise tech-art banner:
		bannerStruct.setup()
	)
	
	on RsLiveMapEditingGameToMaxRoll close do
	(
		OnConnectionStateChanged False
	)
	
	on btnConnect changed state do
	(
		OnConnectionStateChanged (state == on)
	)
	
	on btnSync clicked do
	(
		OnSync()
	)
	
	on timerUpdate tick do
	(
		OnUpdate()
	)
)

-----------------------------------------------------------------------------
-- Entry
-----------------------------------------------------------------------------
createDialog RsLiveMapEditingGameToMaxRoll

-- pipeline/ui/live_map_editing_game_to_max.ms
