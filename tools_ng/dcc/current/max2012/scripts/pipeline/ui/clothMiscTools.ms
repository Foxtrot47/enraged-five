filein "pipeline/util/cloth.ms"
filein "pipeline/helpers/cloth_helpers.ms"
filein "pipeline/util/MeshUtil.ms"

global idxIsClothColl = getattrindex "Gta Collision" "Is Cloth Collision"
global gSkinModSave = undefined

try DestroyDialog RsClothMiscTools catch()

rollout RsClothMiscTools "Cloth Misc Tools"
(
	button buttSetClothColl "Set To Cloth Collision" width:180
	button buttUnSetClothColl "Unset To Cloth Collision" width:180
	group "Ditch cloth channels:" 
	(
		MultiListBox clothChannelSupport "Select to delete channel:"
		button buttDitchMapChannels "go" width:180
	)
	group "Channel painting"
	(
		radiobuttons radioPaintType "Paint channel" labels:#("pin on/off", "pin radius", "vertex resistance") columns:1
		button butStartPaint "start" width:130 across:2
		button butEndPaint "end and bake" width:130 enabled:false
	)
	
	on RsClothMiscTools open do
	(
		local items = #()
		for te in RAGEClothChannelTable do 
			append items te.userName
		clothChannelSupport.items = items
	)
	
	on buttSetClothColl pressed do
	(
		for o in selection do
		(
			if "Gta Collision" == (getattrclass o) then
			(
				setattr o idxIsClothColl true
			)
		)
	)
	on buttUnSetClothColl pressed do
	(
		for o in selection do
		(
			if "Gta Collision" == (getattrclass o) then
			(
				setattr o idxIsClothColl false
			)
		)
	)
	
	on buttDitchMapChannels pressed do
	(
		for o in selection do
		(
			if ::RsCloth.IsCloth o then 
				RsCloth.SetClothMapsSupport o (clothChannelSupport.selection as array)
		)
	)

	on butStartPaint pressed do 
	(
		gSkinModSave = undefined
		if selection.count != 1 or (getattrclass selection[1])!="Gta Object" then
		(
			messagebox "Select one mesh object"
			return false
		)

		obj = selection[1]
		
		hide objects
		unhide obj
		
		obj.showvertexcolors = true
		obj.vertexColorType = #color
		
		gSkinModSave = obj.modifiers[#skin]
		if undefined!=gSkinModSave then
			deleteModifier obj gSkinModSave

		ApplyPaintPodifier obj radioPaintType.state
		
		butEndPaint.enabled = true
		butStartPaint.enabled = false
	)
	
	on butEndPaint pressed do 
	(
		if selection.count != 1 or (getattrclass selection[1])!="Gta Object" then
		(
			messagebox "Select one mesh object"
			return false
		)

		butEndPaint.enabled = false
		butStartPaint.enabled = true
		
		local colourSrcChannel = paintClothMapChannels[radioPaintType.state]
		local bakeChannel = RAGEClothChannelTable[radioPaintType.state].vDataChn

		obj = selection[1]
		CollapseStack obj
		unhide objects
		
		gVertBakeProgresssDialog = dotNetObject "MaxCustomControls.ProgressDialog"
		gVertBakeProgresssDialog.Show()
		gVertBakeProgresssDialog.text = "Baking vertex channels"
		gVertBakeProgresssDialog.Controls.Item[2].value = 0
		for vertIndex = 1 to getnumverts obj do
		(
			local point3val = RsGetColourAverageForMeshVertIndex obj.mesh colourSrcChannel vertIndex
			local averagedVal = (point3val as color).value
			if radioPaintType.state==1 then
			(
				print point3val
				print averagedVal
				if averagedVal<0.9 then
					averagedVal = 0.0
				else
					averagedVal = 1.0
			)
			
			SetChannelSupport obj bakeChannel true
			SetChannelValue obj bakeChannel vertIndex averagedVal
			
			gVertBakeProgresssDialog.Controls.Item[2].value = (100.0/getnumverts obj) * (vertIndex as float)
		)
		gVertBakeProgresssDialog.Close()

		if undefined!=gSkinModSave then
			addModifier obj gSkinModSave
		gSkinModSave = undefined

	)
	
)

CreateDialog RsClothMiscTools width:300 height:400