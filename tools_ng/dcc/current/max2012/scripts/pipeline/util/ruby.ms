--
-- File:: pipeline/util/ruby.ms
-- Description:: Ruby interpreter utility functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 8 april 2011
--

--
-- name: RsRunRubyScript
-- desc: Run Ruby script with a set of arguments.
--
fn RsRunRubyScript filename args:#() = (

	local cmdline = stringStream ""
	
	format "% -I % -rubygems \"%\" " (RsConfigGetToolsRuby()) (RsConfigGetToolsLibDir()) filename to:cmdline
	for arg in args do
		format "% " arg to:cmdline

	( DOScommand cmdline )
)

--
-- name: RsRunIronRubyScript
-- desc: Run IronRuby script with a set of arguments.
--
-- example:
--   RsRunIronRubyScript "x:\\gta5\\tools\\ironlib\\util\\data_convert_file.rb" args:#( "x:\\gta5\\assets\\export\\anim\\expressions\\A_C_Horse.ied.zip" )
--
fn RsRunIronRubyScript filename args:#() debug:false = (

	-- PROMPT batch file currently sets up the environment so we can straddle both systems.
	local PROMPT = ( RsConfigGetToolsRootDir() + "/ironlib/prompt.bat" )
	local IREXE = ( RsConfigGetToolsBinDir() + "/ironruby/bin/ir64.exe" )
	local cmdline = stringStream ""

	if ( debug ) then
	(
		format "DEBUG RsRunIronRubyScript: %\n" PROMPT
		format "DEBUG RsRunIronRubyScript: %\n" IREXE
	)
	
	format "% % \"%\" " PROMPT IREXE filename to:cmdline
	for arg in args do
		format "% " arg to:cmdline

	if ( debug ) then
	(
		format "DEBUG RsRunIronRubyScript: %\n" cmdline
	)
	
	( DOScommand cmdline )
)

-- pipeline/util/ruby.ms
