
-- Return index of subMat in parentMat.materialList
fn GetParentIndex parentMat subMat = (

	if undefined == parentMat then
		return undefined

	for i = 1 to parentMat.materialList.count do
	(
		if ( parentMat.materialList[i] == subMat ) then
			return i
	)

	return undefined
)

fn ReplaceRageMatWithRageNitroMat idx mapMats mapParentMats objectList = (
	local parentMat = mapParentMats[idx]
	local msub = true
	if ( undefined == parentMat ) then (
		msub = false
	)
	
	
	
	local toReplace = mapMats[idx]
	local toReplaceName = toReplace.name
	local nitromat = Rage_Nitro()
	local isAlpha = RstGetIsAlphaShader toReplace
	
	nitromat.name = toReplace.name	
	RNMSetShaderName nitromat (RstGetShaderName toReplace)
	local diffuseCount = 0
	local idxAlphaMap
	for i=1 to RstGetVariableCount toReplace do (
		local varType = RstGetVariableType toReplace i
		RNMSetVariable nitromat i (RstGetVariable toReplace i)
		if isAlpha == true and varType == "texmap" then (
			diffuseCount = diffuseCount + 1
			idxAlphaMap = diffuseCount + ( ( getNumSubTexmaps toReplace ) / 2 )
			local alphaTexMap = getSubTexMap toReplace idxAlphaMap
			if( alphaTexMap != undefined ) then (
				local filename = (getSubTexMap toReplace idxAlphaMap).filename
				local bmptex = BitmapTexture()
				bmptex.filename = filename
				setSubTexMap nitromat idxAlphaMap bmptex
			)
		)
	)
	RNMSetIsTwoSided nitromat (RstGetIsTwoSided toReplace)
	
	if msub == true then (
		idxParent = ( GetParentIndex parentMat toReplace )		
		if ( undefined == idxParent ) then
		(
			MessageBox "Error reading parent material.  Contact tools."
			return false
		)
		mapParentMats[idx].materialList[idxParent] = nitromat
	)
	else (
		objectList[idx].mat = nitromat
	)
	
	mat_index = finditem scenematerials toReplace
	if( mat_index > 0 ) then
	(
		deleteitem scenematerials mat_index
	)
)

fn ReplaceRageNitroMatWithRageMat idx mapMats mapParentMats objectList = (
	local parentMat = mapParentMats[idx]
	local msub = true
	if ( undefined == parentMat ) then (
		msub = false
	)
	
	local toReplace = mapMats[idx]
	local toReplaceName = toReplace.name
	local ragemat = Rage_Shader()
	local isAlpha = RNMGetIsAlphaShader toReplace
	
	ragemat.name = toReplace.name	
	RstSetShaderName ragemat (RNMGetShaderName toReplace)
	local diffuseCount = 0
	local idxAlphaMap
	for i=1 to RNMGetVariableCount toReplace do (
		local varType = RNMGetVariableType toReplace i
		
		RstSetVariable ragemat i (RNMGetVariable toReplace i)
		if isAlpha == true and varType == "texmap" then (
			diffuseCount = diffuseCount + 1
			idxAlphaMap = diffuseCount + ( ( getNumSubTexmaps toReplace ) / 2 )
			local alphaTexMap = getSubTexMap toReplace idxAlphaMap
			if( alphaTexMap != undefined ) then (
				local filename = (getSubTexMap toReplace idxAlphaMap).filename
				local bmptex = BitmapTexture()
				bmptex.filename = filename
				setSubTexMap ragemat idxAlphaMap bmptex
			)
		)
	)
	RstSetIsTwoSided ragemat (RNMGetIsTwoSided toReplace)
	
	if msub == true then (
		idxParent = ( GetParentIndex parentMat toReplace )		
		if ( undefined == idxParent ) then
		(
			MessageBox "Error reading parent material.  Contact tools."
			return false
		)
		mapParentMats[idx].materialList[idxParent] = ragemat
	)
	else (
		objectList[idx].mat = ragemat
	)
	
	mat_index = finditem scenematerials toReplace
	if( mat_index > 0 ) then
	(
		deleteitem scenematerials mat_index
	)
)
