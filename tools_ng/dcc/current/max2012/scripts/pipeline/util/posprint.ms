--
-- posprint.ms
-- Simple utility to list position and rotation information for a set of 
-- selectedobjects.
--
-- David Muir <david.muir@rockstarnorth.com>
-- Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- 10 October 2008
--

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

rollout RsPosPrintRoll "Position / Rotation Print"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Info_Toolkit#Position/Rotation_Print" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnRefresh "Refresh" width:100 pos:[70,20]
	button btnCopy "Copy to clipboard" width:130 pos:[190, 20]
	edittext txtOutput fieldWidth:300 height:60 readOnly:true align:#center
	label labelHelp "For multiple items, view output in the MAXScript Listener (press F11)."
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------

	--
	-- event: btnRefresh pressed
	-- desc: Refresh the text field with info about the selected objects
	--
	on btnRefresh pressed do (
	
		for o in $selection do (
			txtOutput.text = o.name + "\npos:" + (o.pos as string) + "\nrot:" + (( quatToEuler o.rotation ) as string) + "(" + (o.rotation as string) + ")\n\n"
			format "% pos:%, rot:% (%)\n" o.name o.pos ( quatToEuler o.rotation ) o.rotation
		)
	)
	
	--
	-- event: btnCopy pressed
	-- desc: Copy the text field to the clipboard
	--
	on btnCopy pressed do (
	
		local str = txtOutput.text
		retCode = setclipboardText str
		local cliperror = "Clipboard functions didn't work, Return Code: " + ( retCode as string )
		local result = getclipboardText()
		if (result != str) then 
			throw cliperror
	)
)

-- posprint.ms
