--
-- File:: pipeline/util/vfx.ms
-- Description:: VFX Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com
-- Date:: 19 March 2010
--
-----------------------------------------------------------------------------

global RsVFXToolkitUtil
global gExpEditorFloater

try closeRolloutFloater RsVFXToolkitUtil catch()
try(closeRolloutFloater gExpEditorFloater)catch()
-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/helpers/decal.ms"
filein "rockstar/helpers/explosion_cat.ms"
filein "rockstar/helpers/particle_cat.ms"
filein "rockstar/helpers/VFXParticleConvert.ms"

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------

rollout RsVFXHelperCreation "Create helpers" 
(
	button btnCreateParticle "RSN_Particle" width:75 across:2
	button btnCreateExplosion "Gta Explosion" width:75
	
	fn attachToObject parentObj newHelper = (

		if (parentObj != undefined and getattrclass parentObj == "Gta Object" ) do
		(
			newHelper.parent = parentObj
		)
	)
	
	on btnCreateParticle pressed do
	(
		selectionBeforeCreation = selection[1]
		newHelper = startObjectCreation RsRageParticleHelper returnNewNodes:true
		attachToObject selectionBeforeCreation newHelper
	)
	
	on btnCreateExplosion pressed do
	(
		selectionBeforeCreation = selection[1]
		newHelper = startObjectCreation GtaExplosion returnNewNodes:true
		attachToObject selectionBeforeCreation newHelper
	)
	
	-- Save rolled-up state:
	on RsVFXHelperCreation rolledUp down do 
	(
		RsSettingWrite "RsVFXHelperCreation" "rollup" (not down)
	)
)

-- Rollout holding toolkit's techart-banner:
rollout RsVFXToolkitBannerRoll ""
(
	dotNetControl RsBannerPanel "Panel" pos:[6,3] height:32 width:(RsVFXToolkitBannerRoll.width - 6)
	local bannerStruct = makeRsBanner dn_Panel:RsBannerPanel wiki:"VFX_Toolkit" filename:(getThisScriptFilename())
	
	-- Refresh banner-icons when floater's rollouts are adjusted:
	on RsBannerPanel Paint Sender Args do 
	(
		for n = 0 to (RsBannerPanel.Controls.Count - 1) do 
		(
			RsBannerPanel.Controls.Item[n].Invalidate()
		)
	)

	on RsVFXToolkitBannerRoll open do
	(
		-- Initialise tech-art banner:
		bannerStruct.setup()
	)
)



rollout ExplosionEditor "Explosion Editor"
(
	button btnLaunchEditor "Open Explosion Editor"

	on btnLaunchEditor pressed do(
		
		try(closeRolloutFloater gExpEditorFloater)catch()
		
		gExpEditorFloater = newRolloutFloater "Explosion Editor" 200 300 275 126
		addRollout RsExpSetupRoll gExpEditorFloater 

	)
)

(
	RsVFXToolkitUtil = newRolloutFloater "VFX Toolkit" 200 700 50 126
	addRollout RsVFXToolkitBannerRoll RsVFXToolkitUtil border:False
	for thisRoll in
	#(
		RsVFXHelperCreation
		,ExplosionEditor
		,RsPartSetupRoll
		,RsVFXConversionWizRoll
		,RsDecalSetupRoll
		,RsPartDBRoll
	) do 
	(
		addRollout thisRoll RsVFXToolkitUtil 
	)
	
	
)
