-------------------------------------------------------------------------------------
-- Functions to generate Attribute Editor/Viewer rollouts:
-------------------------------------------------------------------------------------

-- Attribute-specific command-strings are added to this struct's array:
struct RsExtraAttrCommands 
(
	-- New commands are added per attribute-class via function 'setCmd'
	commandClasses = #(),	-- List of attribute-classes
	classCommands = #(),		-- List of attribute-commands per class
	
	-- Orders commands by priority:
	fn sortCmds v1 v2 = (v1.priority - v2.priority),
	
	-- Adds/replaces attribute-specific commands:
	fn setCmd cmdName class:"Gta Object" attrs: cmd: priority:5 = 
	(
		local classNum = (findItem commandClasses class)
		if (classNum == 0) do 
		(
			append commandClasses class
			append classCommands #()
			classNum = commandClasses.count
		)
		
		-- Get class-specific command-list:
		local commands = classCommands[classNum]
		
		local currentCmdNames = for cmd in commands collect cmd.name
		local findNum = findItem currentCmdNames cmdName
		
		if (not isKindOf attrs Array) or (not isKindOf cmd String) then 
		(
			-- Delete unwanted command:
			if (findNum != 0) do (deleteItem commands findNum)
		) 
		else 
		(		
			struct extraCmd (name, attrs, cmd, priority)
			local newCmd = (extraCmd name:cmdName attrs:attrs cmd:cmd priority:priority)
			
			-- Add/replace command:
			if (findNum == 0) then (append commands newCmd) else (commands[findNum] = newCmd)
			qsort commands sortCmds
		)
		
		return commands
	)
)
global gRsExtraAttrCommands = RsExtraAttrCommands()

-- Lists the R*-attribute-classes used by a set of nodes:
fn RsGetAttrClassNames nodes = 
(
	local propClassNames = #("undefined")
	
	for obj in nodes do 
	(
		appendIfUnique propClassNames ((getattrclass obj) as string)
	)
	
	if (propClassNames.count != 1) do 
	(
		deleteItem propClassNames 1
	)

	return propClassNames
)

fn RsValidateFloatSpinnerPrecision getVal attrClass attrIndex spinnerScale: =
(
	local attrVarType = GetAttrType attrClass attrIndex
	local attrName = GetAttrName attrClass attrIndex
	-- Logic to limit to certain precision confirming to native MFC dialog.
	if attrVarType=="float" then
	(
		local rangeVal = execute (getcontroldata attrClass attrName)
		local precision = 0
		local m_max  = rangeVal[2]
		-- logic copied straight from plugin
		if(m_max > 9999.999f) then
			precision = 1
		else if(m_max > 999.999f) then
			precision = 2
		else
			precision = 3
		if precision>0 then -- after all we're a float...
		(
			local decimal = 1; for i=1 to precision do decimal *= 10
			if unsupplied!=spinnerScale then
				*spinnerScale = 1.0 / decimal
			local tempVal = (ceil (getVal * decimal)) /decimal
			getVal = tempVal
		)
	)
	getVal
)
-- Lists the object-classes used by a set of nodes:
fn RsGetPropClassNames nodes = 
(
	local propClassNames = #("undefined")
	
	for obj in nodes do 
	(
		appendIfUnique propClassNames ((classof obj) as string)
	)
	
	if (propClassNames.count != 1) do 
	(
		deleteItem propClassNames 1
	)

	return propClassNames
)

-- Struct used to encapsulate and store RsCreateObjEditRoll options:
struct RsCreateObjEditRollDef (propClassName=unsupplied, rollType=#attr, includeGrps=unsupplied, excludeGrps=unsupplied, includeAttrs=unsupplied, rollName=unsupplied, notifyFunc=unsupplied, selector=false)

global RsComparablePropTypes = #(float, integer, string, booleanClass, color)

-- Creates a set of rollouts for a particular object-class name.
-- rolltype can be either #attr (for an attribute-class, i.e. "Gta Object") or #prop (for a max-class, i.e. Editable_Mesh)
-- If includeGrps is set, then only the listed group-names will have rollouts generated.
-- If excludeGrps is set, then the listed group-names will not have rollouts generated.
-- If includeAttrs is set, then only the named attributes will be shown.
-- If rollName is set, all attribs are added to one rollout with that name
-- If notifyFunc string is set, a notification-action is added for each control.
-- Selector argument makes this function generate selection-controls instead of value-setter/showers
fn RsCreateObjEditRoll nodes optionStruct: propClassName: rollType:#attr includeGrps: excludeGrps: includeAttrs: rollName: notifyFunc: selector:false filterName: platform: onlyChanged:false = 
(
	local rolloutArray = #()
	
	-- optionStruct overrides other args:
	if (optionStruct != unsupplied) do 
	(
		propClassName = optionStruct.propClassName
		rollType = optionStruct.rollType
		includeGrps = optionStruct.includeGrps
		excludeGrps = optionStruct.excludeGrps
		includeAttrs = optionStruct.includeAttrs
		rollName = optionStruct.rollName
		notifyFunc = optionStruct.notifyFunc
		selector = optionStruct.selector
	)
	
	local isProps = (rollType == #prop)
	
	case propClassName of 
	(	
		unsupplied:
		(
			propClassName = case rollType of 
			(
				#attr:((RsGetAttrClassNames nodes)[1])
				#prop:((RsGetPropClassNames nodes)[1])
			)
		)
		"undefined":
		(
			return #()
		)
	)
	
	-- Get Extra Attribute Commands list for this object class:
	--	(these add extra callbacks for when specified parameters are changed)
	local extraAttrCommands = #()
	local classNum = findItem gRsExtraAttrCommands.commandClasses propClassName
	if (classNum != 0) do 
	(
		extraAttrCommands = gRsExtraAttrCommands.classCommands[classNum]
	)
	
	-- Get attribute-aliases for this object class:
	--	(used to help de-confuse attributes with unhelpful 'real' names)
	local hasAttribAliases = False
	local AttribAliases = undefined
	
	-- 'RsAttrEdit_AttribAliases' will only be defined if 'RsAttributeEditor_ManagedRolls.ms' has been called, 
	--	which may not be the case if this function was called by Object Selector tool.
	if (::RsAttrEdit_AttribAliases != undefined) do 
	(
		for Item in RsAttrEdit_AttribAliases do 
		(
			if (Item.Type == propClassName) do 
			(
				-- Convert alias-datapairs into searchable name-lists pair:
				AttribAliases = dataPair Names:(for AliasItem in Item.Aliases collect AliasItem.Name) Aliases:(for AliasItem in Item.Aliases collect AliasItem.Alias)
				hasAttribAliases = True
			)
		)
	)
	
	if (platform == unsupplied) do 
	(
		platform = "Independent"
	)
	
	-- Filter nodes to selected attrib/prop class:
	case rollType of 
	(
		#attr:
		(
			nodes = for obj in nodes where (getattrclass obj == propClassName) collect obj
		)
		#prop:
		(
			nodes = for obj in nodes where ((classof obj) as string == propClassName) collect obj
		)
	)
	
	-- If include/excludeGrps are used, set a flag and convert them to searchable Names.
	local doInclude = false
	local doExclude = false
	case of 
	(
		(includeGrps != unsupplied):
		(
			doInclude = true
			includeGrps = for item in includeGrps collect (item as name)
		)
		(excludeGrps != unsupplied):
		(
			doExclude = true
			excludeGrps = for item in excludeGrps collect (item as name)
		)
	)
	local doAllGrps = not (doInclude or doExclude)
	
	-- Set flag if includeAttrs list is used:
	local doIncludeAttrs = (includeAttrs != unsupplied)
	
	local doNotifyFunc = (notifyFunc != unsupplied)
	
	struct objValStruct (name, alias, group, type, value, indeterminate, isString = False, hasPlatformAttributes = false, rangeString = [-1000, 1000, 0], idx = 0, varType, step=0.1)
	local objVals = #()
	
	-- Has a name-filter been supplied?
	local doNameFilter = (filterName != unsupplied)
	local FilterPattern = if doNameFilter then ("*" + filterName + "*") else undefined
	
	case rollType of 
	(
		-- Get attribute-values for nodes:
		#attr:
		(
			-- Get values for each attribute:
			for attrIndex=1 to (getnumattr propClassName) do
			(
				local groupName = getattrparentclassname propClassName attrIndex
				
				-- Ignore specified groupnames:
				local useGrp = doAllGrps or 
					(doInclude and ((findItem includeGrps (groupName as name)) != 0)) or 
					(doExclude and ((findItem excludeGrps (groupName as name)) == 0))

				local attrName = GetattrName propClassName attrIndex
				
				-- Should this attribute's control be hidden by name-filter?
				local nameFiltered = (doNameFilter) and (not matchPattern attrName pattern:FilterPattern)
				
				-- Get name-alias, if applicable:
				local attrAlias = undefined
				if hasAttribAliases do 
				(
					local AliasNum = findItem AttribAliases.Names attrName
					
					if (AliasNum != 0) do 
					(
						attrAlias = AttribAliases.Aliases[AliasNum]
						
						-- Unfilter this control if its alias matches matches the name-filter pattern:
						if nameFiltered do 
						(
							nameFiltered = (not matchPattern attrAlias pattern:FilterPattern)
						)
					)
				)
				
				local useAttr = (not nameFiltered) and ((not doIncludeAttrs) or ((findItem includeAttrs attrName) != 0))
--				print ("filterName:"+filterName as string+" useAttr:"+useAttr as string+" | filtered:"+nameFiltered as string+" | attrName:"+attrName as string)
				
				if useGrp and useAttr do 
				(
					-- instead of putting our controls in a rollout thats named after the archtype/entitytype we want it to be named
					-- after the top parent
					local attrBaseClass = (GetAttrBaseClassName propClassName attrIndex)
					local attrParentClass = (GetAttrImmediateParentClassName attrBaseClass)
					local objParentClass = (GetAttrParentClassName propClassName attrIndex)
					
					local newGrpName = attrBaseClass
					
					if (newGrpName != undefined) do 
					(
						groupName = newGrpName
					)

					local attrType = GetControlType propClassName attrName
					local attrVarType = GetAttrType propClassName attrIndex
					local getVal = undefined
					local allValsSame = true
					
					local defaultVal = GetAttrDefault propClassName attrIndex
					local isString = isKindOf defaultVal string
					
					local stateFiltered = true
					
					if selector then 
					(
						-- Don't hide attributes for Object Selector rollouts:
						stateFiltered = False
						
						-- Don't bother getting object-attributes if generating rollout for Object Selector:
						getVal = false
					)						
					else 
					(
						-- Get attribute-value for all objects (not case-sensitive)
						for n in nodes while allValsSame do
						(
							local newVal = undefined

							if (matchPattern platform pattern:"independent") then
							(
								newVal = getattr n attrIndex
							)
							else
							(
								newVal = getPlatformAttr n attrIndex platform
							)
							
							if (getVal != undefined) and ((isString and (not matchPattern getVal pattern:newVal)) or (newVal != getVal)) then
							(
								allValsSame = false
								getVal = undefined
							)
							else
							(
								getVal = newVal
							)
							
							-- Don't show control if value is default and "onlyChanged" is true:
							if (not onlyChanged) or (newVal != (GetAttrDefault propClassName attrIndex)) do 
							(
								stateFiltered = false
							)
						)
					)
					
					-- If attrib wasn't found on objects, supply default:
					if (selector) or (getVal == undefined) do 
					(
						getVal = defaultVal
					)
					
					if (attrType != undefined) and (not stateFiltered) do 
					(
						local valItem = objValStruct name:attrName alias:attrAlias group:groupName type:attrType value:getVal indeterminate:(not allValsSame) idx:attrIndex varType:attrVarType isString:isString
						
						if 	(attrType == "spinner") do 
						(
							local rangeString = getcontroldata groupName attrName
							
							-- Insert current value as control-default:
							if (IsKindOf GetVal Number) do 
							(
								local rangeVal = execute rangeString
								local tempVal = RsValidateFloatSpinnerPrecision getVal propClassName attrIndex spinnerScale:&valItem.step
								if tempVal!=getVal then
								(
									for n in nodes do
										setattr n attrIndex tempVal
									gRsUlog.LogMessage ("Replacing wrong precision value for attribute \""+attrName+"\": "+getVal as string+" with "+tempVal as string) context:nodes quiet:false
									getVal = tempVal
								)
								rangeVal[3] = getVal
							)
							
							valItem.rangeString = (rangeVal as string)
						)
						
						if (attrType == "checkbox") do
						(
							for n in nodes while (not valItem.hasPlatformAttributes) do
							(
								if (matchPattern platform pattern:"independent" and (HasPlatformAttr n attrIndex)) do 
								(
									valItem.hasPlatformAttributes = True
								)
							)
						)
						
						append objVals valItem
					)
				)
			)
		)
		
		-- Get property-values for nodes:
		#prop:
		(
			local proplist = for propName in (getPropNames nodes[1]) collect (propName as string)

			for attrName in proplist where
					not (nameFiltered = unsupplied!=filterName and not matchPattern attrName pattern:("*"+filterName+"*"))
				do
			(
				local getVal = undefined
				local allValsSame = true
				
				-- Get property-value for all objects (not case-sensitive)
				local allValsSame = true
				local lastType = UndefinedClass
				for n in nodes while allValsSame where isproperty n attrName do
				(
					local newVal = getproperty n attrName

					if (isString == undefined) do 
					(
						isString = isKindOf getVal string
					)
					lastType = classof newVal
					
					local propType = classof newVal
					local isComparableType = (findItem RsComparablePropTypes propType)!=0
					if isComparableType and (getVal != undefined) and ((isString and (not matchPattern getVal pattern:newVal)) or (newVal != getVal)) then
					(
						allValsSame = false
						getVal = undefined
					)
					else 
					(					
						getVal = newVal
					)
				)
				
				local valItem = objValStruct name:attrName group:"Properties" type:(lastType) hasPlatformAttributes:hasPlatformAttributes value:getVal indeterminate:(not allValsSame) idx:attrIndex isString:isString
					
				if isKindOf getVal number do 
				(
					valItem.rangeString[3] = getVal
				)
				valItem.rangeString = valItem.rangeString as string
					
				append objVals valItem
			)
		)
	)
	
	-- Create per-attrib-group value-array:
	local grpNames = #()
	local valGrps = #()
	
	-- If includeAttrs was set, order controls to match list:
	if doIncludeAttrs do 
	(
		fn requestedOrder v1 v2 includeAttrs:#() = 
		(
			(findItem includeAttrs v1.name) - (findItem includeAttrs v2.name)
		)
		objVals
		
		qsort objVals requestedOrder includeAttrs:includeAttrs
	)
	
	if (rollName == unsupplied) then 
	(
		for item in objVals do 
		(
			local grpNum = findItem grpNames item.group
			
			if grpNum == 0 do 
			(
				append grpNames item.group
				append valGrps (dataPair name:item.group items:#())
				grpNum = valGrps.count
			)
			
			append valGrps[grpNum].items item
		)
	)
	else 
	(
		-- If rollName is supplied, all attribs are added to a single rollout with that name:
		append grpNames rollName
		append valGrps (dataPair name:undefined items:objVals)
	)
	
	local rollSuffix = if selector then "_selector" else ""
	
	-- Generate a rollout for each control-group:
	for grp in valGrps do 
	(
		local groupRollName = grp.name
		local groupName = grp.name
		
		if (grp.name == undefined) do  
		(
			groupRollName = rollName
			groupName = propClassName
		)
		
		-- Start generating new subrollout:
		local safeName = RsFileSafeString groupRollName
		local rciName = ("RsAttrEdit_" + safeName + "_roll" + rollSuffix)
		
		local rci = rolloutCreator rciName groupRollName
		rci.begin()
		
		-- Add selection-function, used by rollouts generated for Object Selector tool:
		if selector do 
		(
			local selectorCode = stringStream ""
			format "fn selObjs attr val = (\n" to:selectorCode
			format "local considerObjs = if ((RsSelToolGeneralRoll != undefined) and RsSelToolGeneralRoll.open) then (RsSelToolGeneralRoll.objectList()) else (objects as array)\n" to:selectorCode
			format "local selObjs = for obj in considerObjs where " to:selectorCode
			
			if isProps then 
			(
				format "(isKindOf obj %) and (getProperty obj attr == val) " propClassName to:selectorCode
			)
			else 
			(
				format "(getAttrClass obj == \"%\") and (getattr obj attr == val)" propClassName to:selectorCode
			)
			
			format "collect obj; clearSelection(); select selObjs" to:selectorCode
			
			format ")\n" to:selectorCode
			rci.addText (selectorCode as string) filter:on
		)
		
		-- Generate controls for each group:
		for item in grp.items do 
		(
			local attrName = item.name
			local attrAlias = item.alias
			local attrVal = item.value
			local attrType = item.type
			local hasPlatformAttributes = item.hasPlatformAttributes
			
			-- Set up Maxscript-name for control:
			local safeVarName = RsFileSafeString attrName
			local varname = "control_" + safeVarName
			local handler = #changed
			
			-- Generate string to use as label: (using alias if found)
			local showAttrName = if (attrAlias == undefined) then (attrName) else 
			(
				attrAlias + " [alias for '" + attrName + "']"
			)
			
			local isHandled = true
			local makeZeroBased = false
			local isList = false
			local stringVal = Item.isString
			
			local paramStr = ""			
			if selector do 
			(
				append paramStr (" offset:[42,0]")
			)
			
			case of
			(
				((attrType == "checkbox") and (not isProps) and hasPlatformAttributes):
				(
					append paramStr " triState:2"
					rci.addControl #checkbox varname showAttrName paramStr:paramStr
				)
				((isProps and (attrType == booleanClass)) or (attrType == "checkbox")):
				(
					(attrType = "checkbox")
					
					local rollVal = case of 
					(
						(item.indeterminate):"2"
						(attrVal):"1"
						default:"0"
					)
					
					append paramStr (" triState:" + rollVal)
					
					rci.addControl #checkbox varname showAttrName paramStr:paramStr
				)
				((isProps and (attrType == string)) or (attrType == "editbox")):
				(
					(attrType = "editbox")
					
					local rollVal = if item.indeterminate then "<various>" else (attrVal as string)
					
					append paramStr (" text:\"" + rollVal + "\"")

					rci.addControl #edittext varname showAttrName paramStr:paramStr
				)
				((isProps and ((attrType == integer) or (attrType == float))) or (attrType == "spinner")):
				(
					(attrType = "spinner")
					
					local numType = attrType as string
					if numType=="spinner" then
						numType = (classof attrVal) as string
					
					if(((tolower numType) == "undefinedclass")) then 
						numType = "float"

					local rangeString = item.rangeString
					local indetString = item.indeterminate as string
					local scaleString = item.step as string
					
					append paramStr (" type:#"+ numType +" range:" + rangeString+" width:110 align:#left offset:[10,0] indeterminate:" + indetString + " scale:"+ scaleString)
					
					rci.addControl #spinner varname showAttrName paramStr:paramStr
				)
				(not isProps and (attrType == "combobox")):
				(
					isList = true
					
					local dropDownVals = GetControlData groupName attrName
					local dropDownOverride = ::GetControlEntries attrName
					if dropDownOverride!=false then
						dropDownVals = dropDownOverride
					local dropDownValString, dropDownValIndex
					
					if (dropDownVals == undefined) then 
					(
						format "WARNING: List-items undefined: %\n" attrName
						
						isHandled = false
					)
					else 
					(
						-- List-attributes that return String use 1-based index; if they're index-values, then they'll be zero-based.
						if (not stringVal) do 
						(
							makeZeroBased = true
						)
						
						local valsArray = filterString dropDownVals ","
						local dropDownValIndex = case of 
						(
							(item.indeterminate):0
							stringVal:
							(
								-- Case-insensitive search:
								local findNum = 0
								for n = 1 to valsArray.count while (findNum == 0) do 
								(
									if (matchPattern valsArray[n] pattern:attrVal) do 
									(
										findNum = n
									)
								)
								findNum
							)
							makeZeroBased:(attrVal + 1)
							default:(attrVal)
						)
						
						dropDownValString = "#(\""
						append dropDownValString (RsStringArray valsArray token:("\", \""))
						append dropDownValString "\")"
					)
					
					if (dropDownValString != undefined) do 
					(
						local ListOffset = if Selector then [-60,4] else [-126,0]
						
						rci.addControl #label ("lbl" + varname) (showAttrName + ":") paramStr:(" offset:" + (ListOffset as string) + " align:#right across:2")
						
						local selVal = if item.indeterminate then "0" else (dropDownValIndex as string)
						
						ListOffset += [2,-2]						
						rci.addControl #dropdownlist varname "" paramStr:(" offset:" + (ListOffset as string) + " align:#left items:" + dropDownValString + " selection:" + selVal)
						handler = #selected
					)
					
--					makeZeroBased = true
				)
				(attrType == Color):
				(
					local paramString = (" align:#left ")
					if item.indeterminate then
						append paramString "color:(color 0 0 0) across:2"
					else
						append paramString ("color:"+attrVal as string)
					rci.addControl #colorpicker varname attrName paramStr:paramString
					if item.indeterminate then
						rci.addControl #label (varname+"_varLabel") " <various>" paramStr:"offset:[-120,3]"
				)
				default:
				(
					rci.addControl #label varname (attrName + "'s type "+attrType as string+": Not supported")
					isHandled = false
				)
			)

			-- Add attribute-change handler:
			if isHandled do 
			(
				-- Add selection-button if value-control has been added:
				if selector then 
				(
					local selectorName = varname + "_selector"
					
					-- Add selection-button:
					rci.addControl #button selectorName "Select >" paramStr:("width:48 height:18 pos:[3," + varname + ".pos.y - 1] tooltip:\"Select objs with this value\"")
					
					local handlerCode = stringStream ""
					
					-- Name of property used by this control:
					local CtrlValName = case AttrType of 
					(
						"checkbox":".State"
						"editbox":".Text"
						"combobox":
						(
							case of 
							(
								stringVal:(".Selected")
								makeZeroBased:(".Selection - 1")
								Default:".Selection"
							)
						)
						Default:".Value"
					)
					
					-- Get value from control:
					format "local selVal = (%%);" varname CtrlValName to:handlerCode
					
					if isProps then 
					(
						format "selObjs #% selVal" (attrName as string) to:handlerCode				
					)
					else 
					(
						format "selObjs % selVal" item.idx to:handlerCode				
					)
					
					rci.addHandler selectorName #pressed codeStr:(handlerCode as string)
				)
				else 
				(
					local handlerCode = stringStream ""
					
					if isList do 
					(
						format "if (val != 0) do (\n" to:handlerCode
					)
					
--					format "try (\n" to:handlerCode
					
					-- Collect objs::
					format "local objs = for obj in selection where (isValidNode obj) and " to:handlerCode
					if isProps then 
					(
						format "(isKindOf obj %) collect obj;\n" propClassName to:handlerCode
					)
					else 
					(
						format "(getAttrClass obj == \"%\") collect obj;\n" propClassName to:handlerCode
						
						-- Don't let the user enter any double quotes into the string, 34 is ascii code for "
						format "\t\tif (classof val == String) do (val = substituteString val (bit.intAsChar 34) \"\");\n" to:handlerCode
					)
					
					local valString = case of 
					(
						(isList and stringVal):(varname + ".items[val]")
						makeZeroBased:"(val-1)"
						default:"val"
					)
					
					-- Set values on objects:
					if isProps then 
					(
						format "\t\t for obj in objs do (obj.% = %);\n" (attrName as string) valString to:handlerCode
					)
					else 
					(
						-- Start obj-loop:
						if item.varType == "float" then
						(
							format "\t\t val = (RsValidateFloatSpinnerPrecision val \"%\" %)\n" propClassName item.idx to:handlerCode
							format "\t\t %.value = val" varname to:handlerCode
						)
						format "\t\t for obj in objs do (\n" to:handlerCode

						if (matchPattern platform pattern:"independent") then
						(
							-- Apply attribute to objects:
							format "\t\t\t(setattr obj % %);\n" item.idx valString to:handlerCode
						)
						else
						(	
							-- Apply platform attribute to objects
							format "\t\t\t(setPlatformAttr obj % % (toLower \"%\") );\n" item.idx valString platform to:handlerCode
						)
						
						-- Trigger object's NotifyNode function if it's a Slave Object instance:
						format "\t\t\tif (isProperty obj #isSlave) do (obj.notifyNode @%@ %);\n" attrName valString to:handlerCode
						
						-- End obj-loop:
						format "\t\t)\n" to:handlerCode
					)
					
					-- Add notification-function to all handlers (ie. let a rollout know to update) if notifyFunc was set:
					if doNotifyFunc do 
					(
						format "\t\t\t%\n" notifyFunc to:handlerCode
					)
					
					-- Add extra commands, if attribute was found in this list:
					local commandAdded = false
					for item in extraAttrCommands do 
					(
						-- This Extra Attribute Command will be applied if attribute name is on its list 
						--	(or if its attribute list has been left empty)
						if (item.attrs.count == 0) or ((findItem item.attrs attrName) != 0) do 
						(
							-- Add this bit once, if any commands are being added for this attribute:
							if not commandAdded do 
							(
								format "\t\t\tlocal newVal = %\n" valString to:handlerCode
								commandAdded = true
							)
							
							format "%\n" item.cmd to:handlerCode
						)
					)
					
--					format "\t\t\n) catch (format \"Failed to set % to \%\\n\" (val as string));\n" attrName to:handlerCode
					
					if isProps do 
					(
						format "\tredrawViews();\n" to:handlerCode
					)
					
					if isList do 
					(
						format ")\n" to:handlerCode
					)
					
					handlerCode = handlerCode as string

					if (handlerCode != "") do 
					(
						rci.addHandler varname handler paramStr:"val " codeStr:handlerCode
					)
				)
			)
		)
		
		-- Finalise rollout handlers:
		local rollUpCode = "RsSettingWrite @" + rciName + "@ @rollup@ (not down)"
		rci.addHandler rciName #rolledUp paramStr:"down " codeStr:rollUpCode filter:on
		
		-- Generate rollout:
		local newAttrRoll = rci.end()
		append rolloutArray newAttrRoll
	)

	-- Make sure that rollout with title same as propClassName goes to top, if found:
	fn sortRollouts v1 v2 propClassName: = 
	(
		case of 
		(
			(v1.title == propClassName):-1
			(v2.title == propClassName):1
			default:0
		)
	)

	qsort rolloutArray sortRollouts propClassName:propClassName
	
	return rolloutArray
)

fn RsCreateAttrRoll nodes propClassName: =
(
	RsCreateObjEditRoll nodes propClassName:propClassName rollType:#attr
)

fn RsCreatePropRoll nodes propClassName: =
(
	RsCreateObjEditRoll nodes propClassName:propClassName rollType:#prop
)
