--
-- File:: userprops.ms
-- Description:: User property utility functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 23 April 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
-- None

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsUserProp_Clear
-- desc: Clear an object user property.
--
fn RsUserProp_Clear obj property = (

	local propBuffer = ( getUserPropBuffer obj )
	local newPropBuffer = ""
	local propBufferComponents = ( filterString propBuffer "\r\n" )
	for propComponent in propBufferComponents do
	(
		local propParts = ( filterString propComponent " = " )
		
		-- If our buffer name does not equal our property to remove then
		-- we re-add it into our newPropBuffer.
		if ( property != propParts[1] ) then
		(
			newPropBuffer = newPropBuffer + ( propComponent + "\r\n" )
		)
	)
	
	-- Update the user property buffer.
	setUserPropBuffer obj newPropBuffer
	true
)

fn RsGetUserProps obj = 
(
	local retArray = #()
	local propBuffer = ( getUserPropBuffer obj )
	local newPropBuffer = ""
	local propBufferComponents = ( filterString propBuffer "\r\n" )
	for propComponent in propBufferComponents do
	(
		if ""==propComponent then
			continue
		local propParts = ( filterString propComponent " =" )
		append retArray propParts
	)
	return retArray
)

fn RsCheckUserProps obj = 
(
	local propBuffer = ( getUserPropBuffer obj )
	local propBufferComponents = ( filterString propBuffer "\r\n" )
	for propComponent in propBufferComponents do
	(
		if ""==propComponent then
			continue
		local propParts = ( filterString propComponent " =" )
		if propParts.count!=2 then 
		(
			gRsULog.LogError ("A User Property is malformed on object '" + obj.name + "'. Line must contain only one equality sign and must have a value (" + propComponent + ")") context:obj
			return false
		)
	)
	return true
)
fn RsCopyUserProps src dst =
(
	local ups = RsGetUserProps src
	for up in ups where up.count>1 do
		setuserprop dst up[1] up[2]
)

-- userprops.ms
