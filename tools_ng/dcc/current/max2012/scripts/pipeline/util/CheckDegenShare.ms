--
-- File:: pipeline/util/CheckDegenShare.ms
-- Description::  Check for Degenerate Polys (used for maps and vehicles)
--
-- 12/09/2003
-- by Greg Smith <greg.smith@rockstarnorth.com>
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
GtaCheckTolerance = 0.015
GtaTotalSelectionCount = 0

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
fn GtaCheckVector vecA vecB = (

	for index = 1 to 3 do (
		if vecB[index] > vecA[index] + GtaCheckTolerance then (
			return 0
		)

		if vecB[index] < vecA[index] - GtaCheckTolerance then (
			return 0
		)		
	)
	
	return 1
)

fn GtaCheckObjectForDegenerates obj faceOnly precision = (
		
	if (classof obj) != Editable_mesh then (
		return 0
	)
	
	collapsestack obj
	
	objmesh = obj.mesh
	
	if objmesh == undefined then (
		return 0
	)	
	
	select obj
	
	subobjectLevel = 3
	
	selectlist = #()
	
	-- loop through each face of the object
	for i = 1 to objmesh.numfaces do (
	
		objface = getface objmesh i
		
		vertA = objmesh.verts[objface[1]].pos
		vertB = objmesh.verts[objface[2]].pos
		vertC = objmesh.verts[objface[3]].pos
		
		vecOne = vertB - vertA
		vecTwo = vertC - vertA
		
		tempVec = cross vecOne vecTwo
		tempVec = tempVec / length(vecOne)
		tempVec = tempVec / length(vecTwo)
		
		print faceOnly
		
		--if abs(length(tempVec)) < 0.00001 and faceOnly == false then (
		if abs(length(tempVec)) < precision and faceOnly == false then (
			append selectlist i
		) else (
			if objface[1] == objface[2] or objface[3] == objface[2] or objface[1] == objface[3] then (
				append selectlist i
			)
		)
	)
	
	obj.selectedfaces = selectlist
	
	GtaTotalSelectionCount = GtaTotalSelectionCount + selectlist.count
)

-----------------------------------------------------------------------------
-- On running the script
-----------------------------------------------------------------------------
if selection.count < 1 then (

	MessageBox "Please select some objects first"
	
) else (

	GtaTotalSelectionCount = 0
	
	for objSel in selection do (
		GtaCheckObjectForDegenerates objSel false 0.001
	)
	
	if GtaTotalSelectionCount  > 0 then (
		message = stringstream ""
		format "% degenerate poly(s) have been found and selected" GtaTotalSelectionCount to: message
		MessageBox message
	)
)