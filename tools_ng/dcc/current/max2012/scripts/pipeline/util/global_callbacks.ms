--
-- File:: pipeline/util/global_callbacks.ms
-- Description:: Global callback functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 1 July 2009
--

filein "rockstar/helpers/VFXParticleConvert.ms"
filein "pipeline/util/scene.ms"
filein "rockstar/util/lights.ms"
filein "pipeline/util/p4_utils.ms"

-- Activate viewport object-markers:
filein "pipeline/util/ObjViewMarker.ms"

-- Put current project/toolset in Max toolbar:
fn RsSetMaxTitle = 
(
	local projName = gRsProject.FriendlyName
	local branchName = (toUpper gRsBranch.Name)
	
	local toolsPathTokens = (filterString (gRsConfig.ToolsRoot as string) "\\/")
	local toolsName = toUpper toolsPathTokens[toolsPathTokens.count]
	
	local filename = maxFilename
	if (filename == "") do 
	(
		filename = "Untitled"
	)
	
	local maxTitle = stringStream ""
	format "Project : %      Branch : %      Tools : %      %" projName branchName toolsName filename to:maxTitle
	maxTitle = (maxTitle as string)
	
	cui.setAppTitle maxTitle
)
global RsOpenContainers = #()
global gIgnorePostmergeGuidCheck = false

-- Returns True if filename is maxfile in default map-models folder:
fn RsIsArtModelsMaxfile filename maxc:False = 
(
	local matchPat = RsMakeBackSlashes (RsConfigGetArtDir() + "Models/*.max")
	if maxc do (append matchPat "c")
	
	matchPattern filename pattern:matchPat
)

--
-- name: RsGlobalCallbackProjectCheck
-- desc: This function is called on max file load, to warn artists when they load 
--       a file from outside the current project's path.
--	Sets appropriate dlc-project for incoming file (needed by RsSetMaxTitle)
--
fn RsGlobalCallbackProjectCheck = 
(

	if false==gBatchExportIsRunning then
		try (
			gRsULog.ClearLogDirectory()
		) catch (
			print (getCurrentException())
		)
	
	gRsUlog.init "Project loading"
	
	try
	(
		local params = callbacks.notificationParam()
		--local params = #(1, maxfilepath + maxfilename)
		
		-- This is 1 if doing a normal file load, or 2 if doing an Edit>Fetch
		if ( (Array == classof params) and (1 == params[1]) ) do 
		(	
			local filename = (RsMakeSafeSlashes params[2])
			RsContentTree.SetupProjectAndBranchGlobals dlcMaxFile:filename
			RsContentTree.LoadContentTree forceReload:true
			
			local projName = RsMakeSafeSlashes (RsConfigGetProjectName())
			local rootDir = RsMakeSafeSlashes (RsConfigGetProjRootDir())
		
			format "RsGlobalCallbackProjectCheck:\n"
			format "\tProject:  %\n" projName
			format "\tRoot:     %\n" rootDir
			format "\tFilename: %\n" filename

			if (matchPattern filename pattern:"*/maxstart.max") or ( matchPattern (tolower filename) pattern:(rootDir+"*") ) then
			(
				RsNotifyPopUp_cleanup()
			)
			else 
			(
				local titleText = stringStream ""
				local bodyText = stringStream ""
				
				format "WARNING: File not in % project directory" (toUpper projName) to:titleText
				
				format "DO NOT save this file if it has to be used for another project, attributes may be lost!\n\n" to:bodyText
				format "Project root:\n  %\n\n" (RsMakeBackSlashes rootDir) to:bodyText
				format "File path:\n  %" (getFilenamePath filename) to:bodyText
				
				RsNotifyPopUp_create text:(bodyText as string) title:(titleText as string) width:340 timeOut:8000
			)
			
			if ( RsSettingsRoll != undefined ) then
			(
				RsSettingsRoll.Refresh()
			)
		)

		-- Reload the material preset templates before a file open happens in case there have been changes
		-- since the user opened Max.
		RstReloadMaterialPresets()
	)
	catch
	(
		format "Exception: %\n" ( getCurrentException() )
		--MXSDebugger.openDialog break:true message:"Exception during project check callback."
	)
)

--
-- name: RsGlobalCallbackContCheck
-- desc: Triggered post file-save, warns if artist hasn't saved a container for 2 hours
--
global RsCheckP4OnFileSave = True
fn RsSaveCallbackContCheck = 
(
	-- If true, all save/perforce-times are seen as being too old:
	local debugTest = false
	
	-- Only allow callback for map-folder maxfiles:
	local Param = callbacks.notificationParam()
	if (not RsIsArtModelsMaxfile Param[2]) do (return OK)
	
	if RsProjectGetPerforceIntegration() == false then
	(
		return OK
	)
	
	local maxFileFullPath = maxFilepath + maxFilename
	
	local openConts = for map in RsMapGetMapContainers() where (map.is_container()) and (map.cont.IsOpen()) collect map.cont
	local contFiles = for obj in openConts collect (RsContFuncs.getContFilename obj)
	insertItem undefined openConts 1
	insertItem maxFileFullPath contFiles 1
	
	-- Ensure that filenames are always searchable:
	contFiles = for filename in contFiles collect (RsMakeSafeSlashes (toLower filename))
	
	local warningConts = #()
	local unSavedConts = #()
	local unSavedContTimes = #()
	local nonSubmitConts = #()
	local nonSubmitContTimes = #()
		
	local checkedOutConts = #{}
	
	-- Define the relevant time-spans:
	local timeSpan = dotNetClass "system.timeSpan"
	local tenMins = timeSpan.fromMinutes 10
	local twoHours = timeSpan.fromHours 2
	local oneDay = timeSpan.fromDays 1
	
	-- Invalid filenames will give a 17th-century time, which is fine for our purposes:
	local maxFileTime = if (maxFileFullPath == "") then False else 
	(
		(dotnetClass "System.Io.File").GetLastWriteTime maxFileFullPath
	)
	
	-- Function to convert DotNet DateTime into a nicer format for Max:
	fn makeTimeArray dnTime = 
	(
		for item in (filterString (dnTime.tostring "G") ":-,.") collect (item as integer)
	)

	-- Use the gRsPerforce ignore-list:
	local ignoreFiles = ::gRsPerforce.readOnlyP4Check_ignoreFiles
	
	local checkContNums = 	#{}
	local checkContFilenames = #()
	for contNum = 1 to openConts.count do 
	(
		local contFile = contFiles[contNum]
		local isSceneFile = (openConts[contNum] == undefined)
		
		-- Only check containers that have a lock-file, not temporary merged-in ones:
		-- Don't check files on the ignore-list
		if ((findItem ignoreFiles contFile) == 0) and (doesFileExist contFile) and (isSceneFile or (doesFileExist (contFile + ".lock"))) do 
		(
			checkContNums[contNum] = true
			append checkContFilenames contFile
		)
	)

	-- Skip the rest if no containers are lined up for checking:
	if (checkContFilenames.count == 0) do 
	(
		return true
	)
	
	pushPrompt "Performing time-checks on open containers..."
	local isConnected = False
	
	if RsCheckP4OnFileSave and RsProjectGetPerforceIntegration() do 
	(
		isConnected = gRsPerforce.connect()
		local notCheckedOutFiles = #()
		
		if isConnected do 
		(
			pushPrompt "Checking for Perforce checked-out containers..."
			checkedOutConts = gRsPerforce.checkedOutForEdit checkContFilenames silent:true
			popPrompt()

			notCheckedOutFiles = for fileNum = -checkedOutConts collect (RsMakeSafeSlashes (toLower checkContFilenames[fileNum]))
		)
	
		if (notCheckedOutFiles.count != 0) do 
		(
			local plural = (notCheckedOutFiles.count == 1)
			
			local pluralA = if plural then "This open container is" else "These open containers are"
			local pluralB = if plural then "it" else "them"
			local pluralC = if plural then "this file" else "these files"
			
			local msg = stringStream ""
			format "% not checked out of Perforce.\n\nWould you like to checkout/add now?" pluralA to:msg
			
			local checkboxStates = #{}
			local queryVal = 1  --Assume users want ot check out this file.
			if ( getQuietMode() == false ) then
			(
				queryVal = RsQueryBoxMultiBtn (msg as string) title:("Warning: Open Containers Not Checked Out") labels:#("Yes", "No") \ 
					listItems:notCheckedOutFiles checkLabels:#("Don't ask again for " + pluralC) checkStates:checkboxStates defaultBtn:2 timeout:15
			)

			if (queryVal == 1) do 
			(
				gRsPerforce.add_or_edit notCheckedOutFiles exclusive:true
				checkedOutConts = gRsPerforce.checkedOutForEdit checkContFilenames
			)

			-- Add files to ignore-list if "Dont Ask Again" checkbox was clicked:
			if checkboxStates[1] do 
			(
				join ignoreFiles notCheckedOutFiles
			)
		)
	)
	
	for contNum = checkContNums do 
	(
		local contFile = contFiles[contNum]
		local isSceneFile = (openConts[contNum] == undefined)
		
		local differentSaveTimes = true
		local contSaveTime = (dotnetClass "System.Io.File").GetLastWriteTime contFile
		
		-- Don't trigger warning if Maxfile/container file-times are less than ten minutes apart.
		-- Only trigger for scene-file if dirty-flag is set
		-- Don't trigger if file is read-only still.
		if (getFileAttribute contFile #readOnly) or (isSceneFile and not (getSaveRequired())) then 
		(
			differentSaveTimes = false
		)
		else 
		(
			if (not debugTest) and (maxFileTime != False) do 
			(				
				local maxContTimeDiff = (maxFileTime.subtract contSaveTime).duration()
				local maxContSaveCheck = maxContTimeDiff.compareTo tenMins
				
				differentSaveTimes = (maxContSaveCheck == 1)
			)
		)
		
		if differentSaveTimes do 
		(
			local contSaveTimeDiff = contSaveTime.Now.subtract contSaveTime
			local contSaveCheck = contSaveTimeDiff.compareTo twoHours
			
			-- Make note of containers that haven't been saved for two hours:
			if debugTest or (contSaveCheck == 1) do 
			(
				append unSavedConts contNum
				append unSavedContTimes (makeTimeArray contSaveTimeDiff)
				appendIfUnique warningConts contNum
			)
		)
		
		-- Do Perforce check, if checked-out:
		if isConnected and checkedOutConts[contNum] do 
		(
			-- Find out when container-file was last submitted
			-- (returns undefined if file isn't in Perforce)
			local p4FileTime = gRsPerforce.fileSubmitTime contFile
			
			if (p4FileTime != undefined) do 
			(
				local p4TimeDiff = p4FileTime.Now.subtract p4FileTime
				local p4TimeCheck = p4TimeDiff.compareTo oneDay

				if debugTest or (p4TimeCheck == 1) do 
				(
					append nonSubmitConts contNum
					append nonSubmitContTimes (makeTimeArray p4TimeDiff)
					appendIfUnique warningConts contNum
				)
			)
		)
	)
	
	popPrompt()
	
	local msgTitle = "Warning: "
	local recommendMsg = ""
	local showMessage = false
	local msg = stringStream ""
	local fileTextList = #()
	
	fn makeFileList contNameList contTimes reason fileTextList = 
	(
		for objNum = 1 to contNameList.count do 
		(
			local timeMsg = stringStream ""
			
			local timeArray = contTimes[objNum]
			local timeNames = #("day", "hr", "min", "sec")
			for n = 1 to timeNames.count do 
			(
				local val = timeArray[n]
				if (val != 0) do 
				(
					local plural = if (val == 1) then "" else "s"
					format "%%% " val timeNames[n] plural to:timeMsg
				)
			)
			
			format "ago" to:timeMsg
			
			append fileTextList #(contNameList[objNum], reason, timeMsg as string)
		)
	)
	
	local querySave = false
	local queryP4 = false

	local EnableAutoSubmit = (RsProjectConfig.GetBoolParameter "Enable Automated Max Scene Submission" Default:True)

	-- Bring up popup if we were able to find containers that haven't been saved or haven't been submitted in a while:
	if ((EnableAutoSubmit) and (unSavedConts.count != 0)) do 
	(
		querySave = true
		
		append msgTitle "Unsaved"
		recommendMsg = "save"
		showMessage = true
		
		local filenames = for contNum in unSavedConts collect contFiles[contNum]
		
		makeFileList filenames unSavedContTimes "Not SAVED for at least 2 hours" fileTextList
	)
	
	case of 
	(
		((EnableAutoSubmit) and (nonSubmitConts.count != 0)):
		(
			queryP4 = true
			
			if showMessage do 
			(
				append msgTitle "/"
				append recommendMsg "/"
				format "\n" to:msg
			)
			append msgTitle "Unsubmitted"
			append recommendMsg "submit"
			showMessage = true

			local filenames = for contNum in nonSubmitConts collect contFiles[contNum]
			makeFileList filenames nonSubmitContTimes "Not SUBMITTED for at least 24 hours" fileTextList
		)
		(querySave and not isConnected):
		(
			format "\n(P4 timecheck not performed: Max isn't logged in)\n" to:msg
		)
	)
	
	if showMessage do 
	(
		local pronoun = if (warningConts.count == 1) then "this" else "these"
		
		format "Do you want to % % now?" recommendMsg pronoun to:msg
		
		local buttonLabels = #("Yes", "No")
		local saveResponse = #{}
		local p4Response = #{}
		
		-- Set custom button-labels and response-values for query-dialog:
		case of 
		(
			(querySave and queryP4):
			(
				buttonLabels = #("Save/Submit", "Save", "No")
				saveResponse = #{1,2}
				p4Response = #{1}
			)
			querySave:
			(
				saveResponse = #{1}
			)
			queryP4:
			(
				p4Response = #{1}
			)
		)
		
		local checkboxStates = #{}
		local queryVal = RsQueryBoxMultiBtn (msg as string) title:(msgTitle + " Containers") timeout:15 width:600 \ 
			labels:buttonLabels defaultBtn:(buttonLabels.count) listItems:fileTextList \ 
			checkLabels:#("Don't ask again for " + pronoun) checkStates:checkboxStates \
			cancelBtn:9 -- Set cancel-button to non-button, to allow checkbox to work when "No" is clicked"

		-- Add files to ignore-list if "Dont Ask Again" checkbox was clicked:
		if checkboxStates[1] do 
		(
			local filenames = for item in fileTextList collect item[1]
			join ignoreFiles filenames
		)
		
		if saveResponse[queryVal] do 
		(
			-- Save containers that haven't been saved in a while:
			for contNum in unSavedConts where (openConts[contNum] != undefined) do 
			(
				openConts[contNum].saveContainer false
			)
		)
		
		if p4Response[queryVal] do 
		(
			-- Submit files that haven't been submitted in a while:
			local submitFiles = for contNum = nonSubmitConts collect contFiles[contNum]
			
			local success = gRsPerforce.submitFilesGetText submitFiles
			
			-- Checkout files again after submit:
			if success do 
			(
				success = gRsPerforce.edit submitFiles exclusive:true
			)
		)
	)
	
	OK
)

--
--fn	RsCheckUnitsSetup
--desc	Makes sure the system units are setup correct
--
fn RsCheckUnitsSetup exporting:false = 
(
	if ( units.SystemScale != 1.0 or units.SystemType != #meters ) do
	(		
		local unitsSetupText = "MAX System Units are setup incorrectly, change to Metres and 1 unit = 1.0 Metre (Customize -> Units Setup -> System Unit Setup)"
		if ( exporting ) then
		(
			gRsULog.LogError (unitsSetupText)
		)
		else
		(		
			RsNotifyPopUp_create text:unitsSetupText title:"MAX Units Setup" width:300 timeOut:8000
		)
	)
)

fn RsCheckSceneForLegacyObjects = 
(
	local legacyParticles = #()
	for o in objects do
	(
		local objectClass = classof o
		case objectClass of
		(
			RAGE_Particle: appendifUnique legacyParticles o
		)
	)
	if legacyParticles.count>0 then
	(
		if (querybox "Do you want to replace them with valid ones?" Title:"Deprecated particle objects found") do
		(
			ConvertToRSNParticleHelper allSelection:legacyParticles
		)
	)
)

-- Turn on transform-locks for all containers in scene:
fn RsSetContXformLocks = 
(
	local Param = callbacks.notificationParam()
	if (Param == 1) do (return False) -- Abort if triggered by Xref-system merge
	
	local contObjs = for obj in helpers where (isKindOf obj container) collect obj
	local nonLockeds = for obj in contObjs where ((getTransformLockFlags obj).numberSet != 9) collect obj
	
	if (nonLockeds.count != 0) do 
	(
		format "Locking container xforms:\n"
		for obj in nonLockeds do (format "  %\n" obj.name)
		setTransformLockFlags nonLockeds #{1..9}
	)
	
	OK
)

-- Converted Collision Check
-- Triggers warning if scene contains CollisionConvert data-nodes:
fn RsConvertedCollisionCheck = 
(
	local convertedObjs = #()
	for obj in helpers where ((isKindOf obj ::RsGenericDataNode) and (obj.dataClass == "CollisionConvert")) do 
	(
		for listObj in (obj.getNodeList()) where (isValidNode listObj) and (isKindOf listObj GeometryClass) do 
		(
			append convertedObjs listObj
		)
	)
	
	if (convertedObjs.count != 0) do 
	(
		local titleText = "WARNING: Scene contains Temp-Converted Collision!"
		local bodyText = stringStream ""
		format "Scene contains % objects that have been temporarily converted from Collision to Geometry.\n\n" convertedObjs.count to:bodyText
		format "These objects need to be reverted before exporting.\n\n" to:bodyText
		format "Open Collision Converter? (in Collision Toolkit)" to:bodyText
		gRsULog.LogWarning (titleText) context:convertedObjs
		
		fn openConvTool = 
		(
			filein (::RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Collision/coll_toolkit.ms")
		)
		
		RsNotifyPopUp_create text:(bodyText as string) title:(titleText as string) func:openConvTool objs:convertedObjs width:340 timeOut:8000
	)
	
	return hasConvCollNodes
)

--
--fn RsCheckSceneIsP4Locked
--desc	Check the scene being loaded to see if it is exclusively checked out by someone already
--
fn RsCheckSceneIsP4Locked =
(
	-- Abort if triggered by batch-export:
	if (RsAutomatedExport == True) do return OK
	
	--check scene
	local scenePath =  maxFilePath + maxFileName
	
	if (not RsIsArtModelsMaxfile scenePath) do return OK
		
	if (gRsPerforce.isCheckedOutExclusive #(scenePath))[1] then
	(
		local them = (filterString (RsP4ExclusiveCheckedOutByWho #(scenePath))[1] "@")[1]
		if them == undefined then them = ""
		
		local msg = StringStream ""
		format "This scene is checked out from perforce by % and LOCKED! \n\n" them to:msg
		format "Get the scene checked out first to avoid losing work." to:msg
		
		RsQueryBoxMultiBtn (msg as String) title:"Warning: Exclusive Lock" timeout:20 labels:#("OK")
		gRsULog.LogMessage (msg as String) context:"Scene Load"
	)
	
	--check for latest revision
	--check for newest version
	local fstat = gRsPerforce.getFileStats scenePath
	if fstat != undefined and fstat.count != 0 then
	(	
		fstat = fstat[1]
		local haveRev = fstat.Item[ "haveRev" ]
		local headRev = fstat.Item[ "headRev" ]
		
		--Early exit if we dont have results for haveRev or headRev
		if haveRev == undefined or headRev == undefined then return OK
			
		--Sync if user agrees
		if (haveRev as integer) < (headRev as integer) then
		(
			local syncIt = queryBox "You don't have the latest version of this scene.\n Would you like to sync to head revision?"
			
			if syncIt then
			(
				gRsPerforce.sync scenePath
				
				--reload the scene
				loadMaxFile scenePath quiet:true
			)
		)
	)
	OK
)

--
--fn RsCheckContainerIsP4Locked
--desc	Check the containers being merged to see if it is exclusively checked out by someone already
--
fn RsCheckContainerIsP4Locked =
(
	-- Abort if triggered by batch-export:
	if (RsAutomatedExport == True) do return OK
	
	gRsUlog.init "File load"
	--check containers
	local contObjs = for obj in helpers where (isKindOf obj container) collect obj
	for cont in contObjs do
	(
		local sourceDef = cont.sourceDefinitionFilename
		if (sourceDef != undefined) and (RsIsArtModelsMaxfile sourceDef maxc:True) do
		(
			if (gRsPerforce.isCheckedOutExclusive #(sourceDef))[1] then
			(
				local contName = getFilenameFile sourceDef
				
				local them = (filterString (RsP4ExclusiveCheckedOutByWho #(sourceDef))[1] "@")[1]
				if them == undefined then them = ""
			
				local msg = StringStream ""
				format "This container % is checked out from perforce by % and LOCKED!\t" contName them to:msg
				format "Get the container checked out first to avoid losing work." to:msg
				
				gRsULog.LogWarning (msg as String)
			)
		)
	)
	
	OK
)


global gIgnorePostmergeGuidCheck = false

fn ResetGuidOnMergedObjects = 
(
	if not gIgnorePostmergeGuidCheck then
		::CheckSceneForLightIDProbs showComparisonDialog:true
)


-- Rename materials on containered objects before/after merges, to ensure that they won't replace each other
-- Otherwise, same-named materials are replaced by inherited containers.  Thanks Max.
--# IS THIS REQUIRED FOR MAX2014?
unregisterRedrawViewsCallback RsContMatRenamer
fn RsContMatRenamer triggerEvent:#redraw = 
(
	local EnableMaterialRenaming = (RsProjectConfig.GetBoolParameter "Enable Max Container Material Renaming" Default:True)
	if (not EnableMaterialRenaming) do return OK
	
	-- Don't bother triggering function for Xref-merges 
	-- (container-inherits do these as well as their initial non-xref merge)
	local Param = callbacks.notificationParam()
	if (Param != undefined) do return False
	
	--format "%: %\n" triggerEvent (params as string)
	local checkConts = #()

	case triggerEvent of 
	(
		-- Make sure materials in scene have container-name prefixes before merging new objects in:
		#preMerge:
		(
			if (RsContMatRenamer_preMergeList == undefined) do 
			(
				-- Collect list of open containers in scene, pre-merge:
				checkConts = (for contObj in helpers where (isKindOf contObj Container) and (contObj.children.count != 0) collect contObj)
				
				global RsContMatRenamer_preMergeConts = checkConts
			)
		)
		-- After a file has been merged, lay down callback that'll trigger once when the viewport is next redrawn, 
		-- which should happen after all merges have finished (if several are being done)
		#postMerge:
		(
			unregisterRedrawViewsCallback RsContMatRenamer
			registerRedrawViewsCallback RsContMatRenamer
		)
		-- Make sure material-names are correctly prefixed in newly-opened containers.
		-- This is default argument, so will be used by redrawViewsCallback.
		#redraw:
		(
			unregisterRedrawViewsCallback RsContMatRenamer
			
			-- Get list of containers that weren't in scene or open pre-merge:
			checkConts = 
			(
				for contObj in helpers where (isKindOf contObj Container) and (contObj.children.count != 0) and 
					(appendIfUnique RsContMatRenamer_preMergeConts contObj) collect contObj
			)
			
			-- Clear temp global value:
			globalVars.remove #RsContMatRenamer_preMergeConts
		)
	)
		
	-- Run prefix-check on containered objects before/after merging:
	for contObj in checkConts do 
	(
		local contPrefix = (contObj.name + "_")
		local contPrefixPattern = (contPrefix + "*")
		
		local containerNodes = #()
		contObj.GetContentNodes true &containerNodes
		
		local matList = #()
		local renameMats = #()
		
		-- Make sure that material-name prefixes match their container-name:
		for obj in containerNodes where (isProperty obj #material) do 
		(
			local objMat = obj.material
			
			if (objMat != undefined) and (not matchPattern objMat.name pattern:contPrefixPattern) do 
			(
				append renameMats objMat
			)
		)
		
		if (renameMats.count != 0) do 
		(
			local timeStart = timestamp()
			undo off 
			(
				renameMats = makeUniqueArray renameMats
				format "Adding '%' prefix to % materials:\n" contPrefix renameMats.count
				
				for mat in renameMats do 
				(
					local newMatName = (contPrefix + mat.name)
					format "* Renaming material: % => %\n" mat.name newMatName
					mat.name = newMatName
				)
			)
			format "Time taken to rename: %s\n" ((timestamp() - timeStart) / 1000.0)
		)
	)
		
	return OK
)

-- Setup Material Presets
-- Syncs from Perforce all the newest material preset files.
-- Sets the $(asset) directory to the material preset plugin based on the current branch.
fn RsSetupMaterialPresets = 
(
	local message = stringstream ""
	
	local materialPresetFolder = ((RsConfigGetAssetsDir core:true) + "metadata/materials/...")
	format "Syncing material presets: %\n" materialPresetFolder to:message
	
	pushPrompt message
	
	local result = gRsPerforce.sync #(materialPresetFolder) silent:true
	if not result do 
	(
		format "Failed to sync material presets: %\n" materialPresetFolder
	)
	
	popPrompt()

	-- Make sure the material presets are coming from the right location so update the plugin.
	if not RstSetMaterialPresetAssetDirectory( RsConfigGetAssetsDir() ) then gRsULog.LogError "Could not update material preset plugin with current asset directory path"
)

-----------------------------------------------------------------------------
-- Main Callback Functions, which call subfunctions:
-----------------------------------------------------------------------------

fn RsFilePreOpenCB = 
(
	with undo off with redraw off
	(
		RsGlobalCallbackProjectCheck()
	)
)


fn RsFilePostOpenCB = 
(
	with undo off with redraw off
	(
		RsSetMaxTitle()
		
		gRsUlog.init "File load"
		
		RsCheckSceneIsP4Locked()
		RsCheckUnitsSetup()
		RsCheckSceneForLegacyObjects()
		RsSetContXformLocks()
		RsConvertedCollisionCheck()
		RsLightsMigrateFlashinessFlags()
		RsSetupMaterialPresets()
		RsOpenContainers = RsMapGetMapContainers()
		gRsLinkMgr.FlushCache()
		
		return gRsUlog.ValidateJustErrors()
	)
)

fn ResetGuidOnMergedObjects = 
(
	if not gIgnorePostmergeGuidCheck then
		::CheckSceneForLightIDProbs showComparisonDialog:true
)

fn RsFilePreMergeCB = 
(
	local Param = callbacks.notificationParam()
	if (Param == 1) do (return False) -- Abort if triggered by Xref-system merge
	
	with undo off with redraw off
	(
		RsContMatRenamer triggerEvent:#preMerge
	)
)

fn RsFilePostMergeCB = 
(
	local Param = callbacks.notificationParam()
	if (Param == 1) do (return False) -- Abort if triggered by Xref-system merge
	
	with undo off with redraw off
	(
		gRsUlog.init "File load"
		
		-- Don't run these checks for RSref-system file-merges:
		if not RSnonUserMerge do 
		(
			RsSetContXformLocks()
			RsContMatRenamer triggerEvent:#postMerge
			ResetGuidOnMergedObjects()
			RsConvertedCollisionCheck()
			--RsCheckContainerIsP4Locked()
			RsOpenContainers = RsMapGetMapContainers()
			gRsLinkMgr.FlushCache()
			
			RsContentTree.SetupGlobalsFromMapsAndLoadContentTree RsOpenContainers forceReload:true
			RsSetRexProceduralList() -- used to reload the procedural list when between branches/core, as they have different procedural.meta files		
		)
		
		return gRsUlog.ValidateJustErrors()
	)
)

fn RsFilePostSaveCB = 
(
	local Param = callbacks.notificationParam()
	if (Param == 1) do (return False) -- Abort if triggered by Xref-system merge
	
	with undo off with redraw off
	(
		RsSetMaxTitle()
		RsSaveCallbackContCheck()
	)
)
fn RsFilePreSaveCB = 
(
	local Param = callbacks.notificationParam()
	if (Param == 1) do (return False) -- Abort if triggered by Xref-system merge
	
	with undo off with redraw off
	(
		gRsLinkMgr.SaveCallback()
	)
)

fn RsNodeClonedCB = 
(
	local Params = callbacks.notificationParam()
	
	-- 0==copy, 1==instance, 2==reference NOT name type as described in the reference
	if (Params[3] == 0) then
	(
		local originals = Params[1]
		local clones = Params[2]
		
		for objIndex = 1 to originals.count where
		(
			isValidNode originals[objIndex] and
			isValidNode clones[objIndex]
		)
		do
		(
			case (getattrclass originals[objIndex]) of
			(
				"Gta Collision":
				(
					copyAttrs originals[objIndex]
					pasteAttrs clones[objIndex]
				)
			)
			
			resetAttrGuid clones[objIndex]
		)
	)
)

fn RsFileResetCB = 
(
	RsSetMaxTitle()
)

fn RsPreShutdownCB =
(
	--Make sure we disconnect any remote connections
	RemoteConnection.Disconnect()
	
	try (gRsULog.Shutdown()) catch (print (getCurrentException()))
	gRsLinkMgr.FlushCache()
)

-----------------------------------------------------------------------------
-- Install Callbacks
-----------------------------------------------------------------------------
callbacks.removeScripts id:#RsGlobalCallbacks
callbacks.addScript #filePreOpenProcess "RsFilePreOpenCB()" id:#RsGlobalCallbacks
callbacks.addScript #filePostOpen "RsFilePostOpenCB()" id:#RsGlobalCallbacks
callbacks.addScript #filePreMerge "RsFilePreMergeCB()" id:#RsGlobalCallbacks
callbacks.addScript #filePostMerge "RsFilePostMergeCB()" id:#RsGlobalCallbacks
callbacks.addScript #filePreSaveProcess "RsFilePreSaveCB()" id:#RsGlobalCallbacks
callbacks.addScript #filePostSaveProcess "RsFilePostSaveCB()" id:#RsGlobalCallbacks
callbacks.addScript #postNodesCloned "RsNodeClonedCB()" id:#RsGlobalCallbacks
callbacks.addScript #systemPostReset	"RsFileResetCB()" id:#RsGlobalCallbacks
callbacks.addScript #systemPostNew "RsFileResetCB()" id:#RsGlobalCallbacks
callbacks.addScript #preSystemShutdown "RsPreShutdownCB()" id:#RsGlobalCallbacks
RsSetMaxTitle()

-- pipeline/util/global_callbacks.ms
