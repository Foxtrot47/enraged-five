plugin Helper statedAnimIPLDummy
extends:Dummy
name:"RS IMAP Group"
classID:#(0x3fceb5, 0x703f3)
invisible:true
replaceUI:true
category:"RS Objects"
(
	parameters main rollout:IPLDummyRoll
	(
		imapName type:#string default:"" ui:imapNameEdit
	)
		
	rollout IPLDummyRoll "Properties"
	(
		label lbl1 "IMAP group Name:"
		edittext imapNameEdit ""
	)
	
	on create do
	(
		delegate.boxsize = [1,1,1]
	)
	
	tool create
	(
		on mousePoint click do
		(
			return #stop
		)		
	)
)