--
-- File:: pipeline/util/UtilityFunc.ms
-- Description:: Small gta max utils for interface
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

fn doesVectorMatch check checkAgainst tolerance = 
(
	return ((distance check checkAgainst) <= tolerance)
)

fn RsCheckFileHasLoaded testfilename = 
(
	if (loadMaxFile testfilename == false) then false else true
)

--------------------------------------------------------------
-- Check that the texture has alright dimensions (power of 2)
--------------------------------------------------------------	
fn RsCheckTexDimension texlist = 
(
	local retVal = true
	
	for tex in texlist where RsFileExist tex do 
	(
		local b = openBitmap tex
		
		if undefined==b then
		(
			gRsUlog.LogError ("Couldn't open bitmap texture \""+tex as string+"\"")
			retVal = false
		)
		-- Check the dimensions
		else if ( ( bit.and b.width (b.width - 1) ) != 0 ) or ( ( bit.and b.height (b.height - 1) ) != 0 ) do 
		(
			message = "Texture width or height is not a power of 2 - " + tex + " ( " + (b.width as string) + "x" + (b.height as string) + " )"
			gRsULog.LogError message context:tex
			retVal = false
		)
	)
	
	return retVal
)

fn GtaCopyVertColours = 
(
	fn isObjEditableMesh obj = 
	(
		isKindOf obj Editable_Mesh
	)
	
	messagebox "Select Object To Copy From"
	objFrom = pickObject filter:isObjEditableMesh
	
	messagebox "Select Object To Copy To"
	objTo = pickObject filter:isObjEditableMesh
	
	if classof objFrom != Editable_Mesh then (
		return 0
	)

	if classof objTo != Editable_Mesh then (
		return 0
	)
	
	copyvertcoloursexact objFrom objTo
)

fn roundVector vector = (

	vecRound = copy vector

	for i = 1 to 3 do (
	
		roundVal = vecRound[i]
		
		roundVal = roundVal * 128
		roundVal = floor roundVal
		roundVal = roundVal / 128
		
		vecRound[i] = roundVal
	)
	
	return vecRound
)

-- Searches the Attribute list-control array for a particular name, and returns its (0-based) index
fn RsGetAttributeListIndex attribName = 
(
	local attribsList = filterString (getControlData "Gta Object" "Attribute") ","

	-- Returns -1 if not found:
	return ((findItem attribsList attribName) - 1)
)

fn selectLODParent = (

	obj = selection[1]
	clearSelection()
	
	if (obj == undefined) do (
	
		return 0
	)
	parent = RsSceneLink.getparent LinkType_LOD obj
	
	if parent == undefined then (
		clearselection()
	) else (
		select parent	
	)
)

fn getLODChildren lodparent linkType:LinkType_LOD = 
(
	local children = #()
	local LinkType_LOD = 0
	RsSceneLink.GetChildren linkType lodparent &children
	return children
)

fn getMaxLODLevels lodparent = (
	
	childnodes = getLODChildren lodparent
	
	maxinchild = 0
	
	for childnode in childnodes do (
	
		inchild = getMaxLODLevels childnode
	
		if inchild > maxinchild then (
			maxinchild = inchild
		)
	)
	
	maxinchild = maxinchild + 1
	
	return maxinchild 
)

fn selectLODChildren = (

	if (selection[1] == undefined) do (
	
		return 0
	)
	lodChildObjs = getLODChildren selection[1]
	
	select lodChildObjs
)

fn selectAllLODChildrenRecursive obj currlevel = (
	
	colval = 255 - (30 * currlevel)

	obj.wirecolor = color colval colval colval

	refobjs = getLODChildren obj

	newlevel = currlevel + 1

	for refObj in refobjs do 
	(
		selectmore refObj
		
		selectAllLODChildrenRecursive refObj newlevel
	)
)

fn selectAllLODChildren = (

	selall = #()

	level = 0

	for obj in selection do (
		append selall obj
	)

	clearSelection()

	for obj in selall do (
		selectAllLODChildrenRecursive obj level
	)
)

-- Selects all reference-objects --
fn selectAllXref = 
(
	clearSelection()
	select (for obj in $objects where (::isRefObj obj) collect obj)
)

fn selectAllNormal = (

	selectSet = #()
	
	for obj in $objects do (
		if getattrclass	obj == "Gta Object" then (
			append selectSet obj
		)
	)
	
	select selectSet
)

fn selectAllHorizon = 
(
	local horizonIndex = RsGetAttributeListIndex "Horizon Object"
	
	if ( horizonIndex == -1 ) do (
	
		messagebox "No Horizon Object attribute found"
		return false
	)	 

	local selectSet = #()
	
	for obj in $objects do 
	(
		if ( getattrclass obj == "Gta Object" ) do 
		(
			-- Find out the index of Attribute and then find the 
			-- attribute this object has and see if it is Horizon Object
			attributeIndex = getattrindex "Gta Object" "Attribute"
			objAttribute = getattr obj attributeIndex
			
			if (objAttribute == horizonIndex) do 
			(
				append SelectSet obj
			)
		)
	)
	
	clearSelection()
	select selectSet
)

fn ColourLightWireByLightColour = (

	maxLightMult = 0.0

	for lights = 1 to selection.count do
	(
		multVal = 1.0
	
		if isproperty selection[lights] "multiplier" then (
		
			multVal = selection[lights].multiplier
		)
	
		if(multVal > maxLightMult) then (
		
			maxLightMult = multVal
		)
	)

	for lights = 1 to selection.count do
	(
		try (
		
			multVal = 1.0

			if isproperty selection[lights] "multiplier" then (

				multVal = selection[lights].multiplier
			)		
		
			setval = (0.5 * selection[lights].rgb) + (0.5 * selection[lights].rgb * (multVal / maxLightMult))
			setval.a = 255.0
				
			selection[lights].wirecolor = setval
			selection[lights].wirecolor = setval
		)
	    	catch()
       )
)

-- applies the optimise modifier to 
fn OptimisePlanarPolys = (
	if selection.count != 1 then (
		messagebox "Please select one object"
	)
	else (
		newopt = optimize()
		
		newopt.facethreshold1 = 0.01
		newopt.edgethreshold1 = 0.0
		newopt.bias1 = 0.0

		addModifier selection[1] newopt
	)
)

fn GetTXDRecursive obj txdList = (
	
	-- ignore objects without Gta Object attribute class
	if (getattrclass obj) == "Gta Object" do (
	
		local index = getattrindex "Gta Object" "TXD"

		valDontExport = getattr obj (getattrindex "Gta Object" "Dont Export")
		
		if valDontExport == false then (
			If not ::isRefObj obj do (
				txdName = toLower(getattr obj index)
				if (findItem txdList txdName) == 0 do (
					append txdList txdName
				)
			)
		)
	)

	if (getattrclass obj) == "GtaAnimHierarchy" do (
	
		local index = getattrindex "GtaAnimHierarchy" "TXD"
		
		valDontExport = getattr obj (getattrindex "GtaAnimHierarchy" "Dont Export")
		
		if valDontExport == false then (
			If not ::isRefObj obj do(
				txdName = toLower(getattr obj index)
				if (findItem txdList txdName) == 0 do (
					append txdList txdName
				)
			)
		)
	)

	if classof obj == Gta_MultiBldg then (
		for childobj in obj.children do (
			GetTXDRecursive childobj txdList
		)
	)
)

-- get TXDs from scene
fn GetTxdList txdList = (

	for obj in $objects do (
		GetTXDRecursive obj txdList
	)
	
	if txdList.count == 0 do (
		MessageBox "TXD list is empty"
		return false
	)
	
	qsort txdList stricmp
	
	return true
)

-- Outputs a list of unique names used in attribute-field "fieldname"
-- Will also put lists of objects that use those names to optional array sortedObjs
fn RsGetUniqueNames fieldname objListsOut:#() includeRefs:false = 
(
	local nameList = #()
	local objList = #()

	local idxObjectField = GetAttrIndex "Gta Object" fieldname
	local idxAnimHierField = GetAttrIndex "GtaAnimHierarchy" fieldname
	
	local idxDontExportObj = GetAttrIndex "Gta Object" "Dont Export"
	local idxDontExportAnim = GetAttrIndex "GtaAnimHierarchy" "Dont Export"
	
	local idxField, idxDontExport, findNum
	for obj in $objects where (includeRefs or (not ::isRefObj obj)) do 
	(
		idxField = -1
		idxDontExport = -1

		case (GetAttrClass obj) of 
		(
			"Gta Object":
			(
				idxField = idxObjectField
				idxDontExport = idxDontExportObj
			)
			"GtaAnimHierarchy":
			(
				idxField = idxAnimHierField
				idxDontExport = idxDontExportAnim
			)
		)

		if (idxField != undefined) and (idxDontExport != -1) and (idxField != -1) do 
		(
			if GetAttr obj idxDontExport == false do 
			(
				local fieldName = toLower (GetAttr obj idxField)
				
				findNum = findItem nameList fieldName
				if (findNum == 0) do 
				(
					append nameList fieldName
					append objList #()
					findNum = nameList.count
				)
				
				append objList[findNum] obj
			)
		)
	)
	
	local oldNameList = deepCopy nameList
	
	-- Sort name-list
	qsort nameList stricmp
	
	-- Rearrange object-lists to match sorted name-list
	for item in nameList do 
	(
		append objListsOut objList[findItem oldNameList item]
	)
	
	return nameList
)

fn SetDontExportSelected = (
	--selectionsets["GtaDontExport"] = selection
	
	idxDontExportGtaObj = GetAttrIndex "Gta Object" "Dont Export"
	idxDontExportGtaAnim = GetAttrIndex "GtaAnimHierarchy" "Dont Export"
	
	for obj in $selection do (
		
		if GetAttrClass obj =="Gta Object" then (
			idxDontExport = idxDontExportGtaObj
		)
		
		if GetAttrClass obj =="GtaAnimHierarchy" then (
			idxDontExport = idxDontExportGtaAnim
		)
		
		if idxDontExport != undefined then (

			found = false
		
			for checkobj in selection do (
			
				if checkobj == obj then (
					found = true
					exit
				)
			)
			
			if found then (
				SetAttr obj idxDontExport true
			) else (
				SetAttr obj idxDontExport false
			)
		)
	)
)

fn AddIgnoreSelected = (
	--selectionsets["GtaIgnore"] = selection
	
	idxDontAddToIPLGtaObj = GetAttrIndex "Gta Object" "Dont Add To IPL"
	idxDontAddToIPLGtaAnim = GetAttrIndex "GtaAnimHierarchy" "Dont Add To IPL"
	
	for obj in $selection do (
				
		if GetAttrClass obj =="Gta Object" then (
			idxDontAddToIPL = idxDontAddToIPLGtaObj
		)
		
		if GetAttrClass obj =="GtaAnimHierarchy" then (
			idxDontAddToIPL = idxDontAddToIPLGtaAnim
		)
		
		if idxDontAddToIPL != undefined then (

			found = false
		
			for checkobj in selection do (
			
				if checkobj == obj then (
					found = true
					exit
				)
			)
			
			if found then (
				SetAttr obj idxDontAddToIPL true
			) else (
				SetAttr obj idxDontAddToIPL false
			)
		)
	)
)

fn SetDontApplyRadiosity = (

	idxDontApplyRadiosityGtaObj = GetAttrIndex "Gta Object" "Dont Apply Radiosity"
	idxDontApplyRadiosityGtaAnim = GetAttrIndex "GtaAnimHierarchy" "Dont Apply Radiosity"

	for obj in $selection do (
		
		if GetAttrClass obj =="Gta Object" then (
			idxDontApplyRadiosity = idxDontApplyRadiosityGtaObj
		)
		
		if GetAttrClass obj =="GtaAnimHierarchy" then (
			idxDontApplyRadiosity = idxDontApplyRadiosityGtaAnim
		)
		
		if idxDontApplyRadiosity != undefined then (

			found = false
		
			for checkobj in selection do (
			
				if checkobj == obj then (
					found = true
					exit
				)
			)
			
			if found then (
				SetAttr obj idxDontApplyRadiosity true
			) else (
				SetAttr obj idxDontApplyRadiosity false
			)
		)
	)
)

fn DeleteUserProps = 
(
	for o in $selection do setuserpropbuffer o ""
)

-- reset the gta id for the selected objects. they will reacquire them on next export
fn removedUniqueIDFromSelected = (
	for object = 1 to selection.count do (
		RemoveUniqueID selection[object]
	)
)

fn ShowSuperLodObjects objset = (
	for object = 1 to objset.count do 				
	(
		if getattrclass objset[object] == "Gta Collision" then (
				unhide objset[object]
		) else (
	
			if ::isRefObj objset[object] then (
				currentname = toLower(objset[object].objectname)
			) else (
				currentname = toLower(objset[object].name)
			)

			found = (findstring currentname "superlod") 

			if found == undefined and (classof objset[object] != Sphere) then  (
				hide objset[object] 
			) else (
				unhide objset[object]
			)
		)
		
		if objset[object].children != undefined then (
			ShowSuperLodObjects objset[object].children
		)
	)
)

fn ShowLodObjects objset = (
	for object = 1 to objset.count do 				
	( 	
		if getattrclass objset[object] == "Gta Collision" then (
				unhide objset[object]
		) else (
	
			if ::isRefObj objset[object] then (
				currentname = toLower(objset[object].objectname)
			) else (
				currentname = toLower(objset[object].name)
			)

			found = (findstring currentname "superlod") 

			if found != undefined then  (
				hide objset[object] 
			) else (
				found = (findstring currentname "lod") 

				if found == undefined and (classof objset[object] != Sphere) then (

					if not ::isRefObj objset[object] then (

						valLodDistance = RsGetObjLodDistance objset[object]

						if valLodDistance > 300 then (
							unhide objset[object]
						) else (
							hide objset[object] 
						)
					) else (
						hide objset[object] 
					)

				) else (
					unhide objset[object]
				)
			)
		)
		
		if objset[object].children != undefined then (
			ShowLodObjects objset[object].children
		)
	)
)

fn ShowHiDetailObjects objset = (
	for object = 1 to objset.count do 				
	( 		
		if getattrclass objset[object] == "Gta Collision" then (
			unhide objset[object]
		) else (
	
			if ::isRefObj objset[object] then (
				currentname = toLower(objset[object].objectname)
			) else (
				currentname = toLower(objset[object].name)
			)

			found = (findstring currentname "lod") 

			if classof objset[object] == Sphere then (
				unhide objset[object]
			) else (

				if found != undefined then  (
					hide objset[object] 
				) else (

					if not ::isRefObj objset[object] then (

						valLodDistance = RsGetObjLodDistance idxLodDistance

						if valLodDistance < 300 then (
							unhide objset[object]
						)
					) else (
						unhide objset[object]
					)
				)
			)
		)
		
		if objset[object].children != undefined then (
			ShowHiDetailObjects objset[object].children
		)
	)
)

--hides or displays objects containing objname
fn HideNamedObjects objname hideme = (
	for object = 1 to $objects.count do 				
	( 				
		currentname = $objects[object].name 				
		found = (findstring currentname objname) 
		if hideme == true then(
			if found != undefined  then hide $objects[object] else unhide $objects[object]
		) else(
			if found != undefined  then unhide $objects[object] else hide $objects[object]	
		)
	)
)

--setwirecolour of all objects in the scene based on its TXD value
fn SetWireColourByTXD = (
	local classname = "Gta Object"
	local attrindex = getattrindex classname "TXD"
	if attrindex == undefined do return false
	
	txdlist=#()
	txdcolour=#()
	for obj in $objects do (
		-- ignore objects without Gta Object attribute class
		if (getattrclass obj) == "Gta Object" do (
			txdName = getattr obj attrindex -- this is the txd name
			if (findItem txdList txdName) == 0 do (
				append txdList txdName
			)
		)
	)
	---setsup colors

	for txdlists=1 to txdlist.count do
	(
		txdcolour[txdlists] = [(random 0 255),(random 0 255),(random 0 255)]
	)
	for obj in $objects do 
	(
		try (
			txdName = getattr obj attrindex 
			objtxd=findItem txdList txdName
			if objtxd > 0 then obj.wirecolor = txdcolour[objtxd]
		)
		catch()
	)
)

-- Gta Test for objects have LODs
fn GtaTestForLodName = (
	for loopa = 1 to $objects.count do (
		name = $objects[loopa].name
		$objects[loopa].wirecolor = white
		newname = "LOD"+(substring name 4 -1)
		if (findstring name "LOD" ==undefined) do(
			for testb = 1 to $objects.count do (
				if ($objects[testb].name == newname) do $objects[testb].wirecolor = red
				print newname
			)
		)
	)
)

fn GtaTestForLodNameWithBar pBar = (
	if pBar != undefined do pBar.value = 0
	print "Inside test function"
	
	for obj = 1 to $objects.count do
	(
		objectName = $objects[obj].name
		$objects[obj].wirecolor = white
		if (findstring objectName "LOD" == undefined) do
		(
			lodObjectName = "LOD"+(substring objectName 4 -1)
			for testObject in $objects do
			(
				if (testObject.name == lodObjectName) then
				(
					testObject.wirecolor = red
					print testObject.name
				)
			)
		)

		if pBar != undefined do pBar.value = 100.0 * obj / $objects.count
	)
)

fn ResetDiffuseToWhiteForMat setmat = 
(
	local matEditWasOpen = MatEditor.isOpen()
	MatEditor.Close()
	
	case (classOf setmat) of 
	(
		standard:
		(
			setmat.diffuse=[255,255,255]
		)
		rwmaterial:
		(
			setmat.defmtl_color=[255,255,255]
		) 
		multimaterial:
		(
			for subMat in setmat.materialList do
			(
				ResetDiffuseToWhiteForMat subMat
			)
		)
	)
	
	if matEditWasOpen do MatEditor.Open()
)

-- set diffuse on all the scene materials to be white
fn ResetDiffuseToWhite = 
(
	local matEditWasOpen = MatEditor.isOpen()
	MatEditor.Close()
	
	for mat in scenematerials do 
	(
		ResetDiffuseToWhiteForMat mat
	)
	
	if matEditWasOpen do MatEditor.Open()
)

-- functions for switching the viewed map for light maps

lightmapChangeType = undefined

fn lightmapLightMapRW mat = (		
	if mat.defmtl_matfxeffect != 4 then (
		return 0
	)
	
	if lightmapChangeType == 1 then (
		showTextureMap mat true
	)
	
	if lightmapChangeType == 2 then (
		showTextureMap mat mat.defmtl_texmap_texture true
	)
	
	if lightmapChangeType == 3 then (
		showTextureMap mat mat.defmtl_texmap_pass2 true
	)
	
	return 0
)

fn lightmapLightMapMulti mat = (
	for i = 1 to mat.materialList.count do (
	
		submat = mat.materialList[i]
	
		if classof(submat) == RwMaterial then (
			lightmapLightMapRW submat
		)

		if classof(submat) == Multimaterial then (
			lightmapLightMapMulti submat
		)
	)
	
	return mat
)

fn lightmapLightMapDoMat mat = (
	if classof(mat) == RwMaterial then (
		lightmapLightMapRW mat
	)
	
	if classof(mat) == Multimaterial then (
		lightmapLightMapMulti mat
	)
	
	return mat
)

fn lightmapLightMapDoChildren parentnode = (

	for childnode in parentnode do (
		lightmapLightMapDoMat childnode.material
		
		if childnode.children != undefined then (
			lightmapLightMapDoChildren childnode.children
		)
	)
)

fn lightMapShowAll = (
	lightmapChangeType = 1
	lightmapLightMapDoChildren selection
)

fn lightMapShowBase = (
	lightmapChangeType = 2
	lightmapLightMapDoChildren selection
)

fn lightMapShowLightMap = (
	lightmapChangeType = 3
	lightmapLightMapDoChildren selection
)