--
-- RSref system
-- System for creating reference-objects in map-scenes
--
-- Neal D Corbett
-- Rockstar Leeds
-- 08/06/2011
--

filein "pipeline/util/RSrefs_settings.ms"
filein "pipeline/util/RSrefs_browser.ms"

-- Update any existing settings, if this script is being re-run:
RSrefSettingFuncs.RSrefScriptReset()

filein "pipeline/util/RSrefs_funcs.ms"

-- Define new RSref data-struct if one hasn't been set up yet:
if (RSrefData == undefined) do (RSrefFuncs.reset debugFuncSrc:"[fileIn RSrefs.ms]")

filein "pipeline/util/RSrefs_DataStore.ms"
filein "pipeline/util/RSrefs_slaves.ms"
filein "pipeline/util/RSrefs_objects.ms"
filein "pipeline/util/RSrefs_callbacks.ms"

if RSrefFunctionDebug or RSrefsRerun do 
(
	if RSrefsRerun do 
	(
		globalVars.remove "RSrefsRerun"
		(for obj in objects where (isRSrefSuperClass obj) collect obj).rebuildMesh = true
	)
	format "[RSREF SCRIPT-LOAD COMPLETED]\n"
)
