-- Utils for tagging up texures
-- Rockstar North
-- 12/10/2010
-- Luke Openshaw
--
-- 

global gTCSTaggerLookup

struct xmlStreamHandler
(
	--------------------------------------------------------------
	-- USAGE: xmlStream = xmlStreamHandler xmlFile:"c:/test.xml"
	-- xmlStream.open()
	-- DO STUFF WITH THE XML USING xmlStream.root
	-- xmlStream.close()
	--------------------------------------------------------------
	stream = undefined,
	xmlFile = undefined,
	root = undefined, -- ROOT NODE OF THE XML FILE
	XML = undefined,
	error = false,
	
	fn open =
	(
		-- Open the xml with the streamReader 
		stream = dotNetObject "System.IO.StreamReader" xmlFile
		XML = dotNetObject "System.Xml.XmlDocument"
		try (XML.load stream) catch -- ERROR CHECKING
		(
				messageBox (xmlFile) title:"XML Formatting error!"
				error = true
				this.close() -- CLOSE OUT ALL THE VARIBLES AND FILES
		)
		if not(error) do root = XML.documentElement  
	),

	fn close =
	(
		-- Close/Dispose the the steam and xml varbiles
		-- this is all needed to make sure the memory used is freed from big XML files
		XML = undefined 
		stream.close() 
		stream.dispose() 
		stream = undefined 
		gc() 
	)
)

-------------------------------------------------------------------------
-- Parse the ParentTxd.xml
--	Building list of texturenames belonging to global ParentTxd
-------------------------------------------------------------------------
fn RsCreateParentTxdLookup =
(
	PushPrompt "Creating Parent Txd lookup"
	
	local parentTxdList = #()
	local parentTxd_XMLPath = (RsConfigGetAssetsDir core:true + "maps/ParentTxds.xml")
	
	-- Sync ParentTxds.xml, force-getting if it isn't found locally:
	local forceGet = (not doesFileExist parentTxd_XMLPath)
	gRsPerforce.sync #(parentTxd_XMLPath) Force:forceGet Silent:True
	
	if (doesFileExist parentTxd_XMLPath) then
	(
		local xmlStream = xmlStreamHandler xmlFile:parentTxd_XMLPath
		xmlStream.open()
		
		-- Find all 'globalTexture' nodes:
		local theRoot = xmlStream.root
		local GlobalTextureNodes = theRoot.selectNodes "//globalTexture"
		
		-- Collect names of texmaps that are in parentTxds:
		parentTxdList = for n = 0 to (GlobalTextureNodes.count - 1) collect
		(
			local ThisNode = GlobalTextureNodes.Item[n]
			local TexFilename = ThisNode.GetAttribute "filename"
			local TexName = (getFilenameFile TexFilename)
			
			-- Add alpha-filename, if supplied:
			local TexAlpha = ThisNode.GetAttribute "alpha"
			if (TexAlpha != "") do 
			(
				TexName += ("+" + (getFilenameFile TexAlpha))
			)
			
			(toLower TexName)
		)
		
		xmlStream.close()

		-- Get rid of duplicate texmap-names:
		parentTxdList = (MakeUniqueArray parentTxdList)
	)
	else
	(
		messagebox ("You are missing " + parentTxd_XMLPath + "\nPlease sync from perforce.") title:"Missing file"
	)
	
	PopPrompt()
	
	return parentTxdList
)


fn RsExtractTemplateFromFile tcsfile =
(
	--Check for file exists locally
	if (not RsFileExists tcsfile) do (return undefined)
	
	--Load the tcs
	local xmlDoc = XMlDocument()
	xmlDoc.init()
	xmlDoc.load tcsfile

	local retVal = undefined
	local xmlRoot = xmlDoc.document.DocumentElement
	
	if (xmlRoot != undefined) do 
	(
		refnode = RsGetXmlElement xmlRoot "parent"
		if (refnode != undefined) do 
		(
			retVal = refnode.InnerText
		)
	)
	return retVal
)

-- Return default template-name for a given texmap-slot type:
fn RsResolveTexMapTypeName texmaptype = 
(
	case of 
	(
		(RsIsDiffuseMap texmaptype):"Diffuse Map"
		(RsIsSpecMap texmaptype):"Specular Map"
		(RsIsBumpMap texmaptype):"Normal Map"
		Default:Undefined
	)	
)

-- Does the reverse of RsResolveTypeNameToTemplate: 
--	Returns template-path corresponding to a given UI TypeName:
fn RsResolveTypeNameToTemplate typename templatesXmlFilename:( RsConfigGetEtcDir() + "texture/templates.xml" ) = (
	
	xmlDoc = XMLDocument()
	xmlDoc.init()
	
	templateDefault = ""
	
	-- Default global templates xml file used if none specified
	if ( doesFileExist templatesXmlFilename ) then
	(
		xmlDoc.load (templatesXmlFilename )
		
		xmlRoot = xmlDoc.document.DocumentElement
		if xmlRoot != undefined then (
			refnode = RsGetXmlElement xmlRoot "references"
			items = refnode.childnodes
			for i = 0 to ( items.Count - 1 ) do (
				item = items.itemof(i)
				namenode = RsGetXmlElement item "name"
				pathnode = RsGetXmlElement item "path"	
				if ( typename == namenode.InnerText ) then
				(
					return ( pathnode.InnerText )
				)
				-- Check if it's the default template for this script to store incase
				-- nothing is found, so that it's returned as a fallback
				else if ( "Default" == namenode.InnerText  ) do
				(
					templateDefault = pathnode.InnerText
				)
			)
		)
	) 
	else
	(
		ss = stringstream ""
		format "% does not exist,\ntextures/templates/globals/Default returned for all" templatesXmlFilename to:ss
		Messagebox ss
	)
	
	if ( "" != templateDefault ) then
	(
		templateDefault
	)
	else
	(
		-- Fallback
		"$(core_assets)/metadata/textures/templates/globals/Default" 
	)
)

-- Gets the UI TypeName given the template (which should contain the full path with ${assets} too.
--	(Does the reverse of RsResolveTypeNameToTemplate)
fn RsResolveTemplateToTypeName template templatesXmlFilename:( RsConfigGetEtcDir() + "texture/templates.xml" ) = (
	
	xmlDoc = XMLDocument()
	xmlDoc.init()	
	typeName = "Default"
	
	--get the template name from the template string parameter
	local templateBits = filterString template "/"
	local templateName = typeName
	if (templateBits.count != 0) then
	(
		templateName = templateBits[templateBits.count]
	)
	
	--build the global lookup dictionary if it has no entries and has therefore not been built yet.
	if (gTCSTaggerLookup == undefined) then
	(
		global gTCSTaggerLookup = dotnetobject "RSG.MaxUtils.MaxDictionary"
		
		if ( doesFileExist templatesXmlFilename ) then
		(
			xmlDoc.load (templatesXmlFilename )
			
			xmlRoot = xmlDoc.document.DocumentElement
			if xmlRoot != undefined then (
				refnode = RsGetXmlElement xmlRoot "references"
				items = refnode.childnodes
				for i = 0 to ( items.Count - 1 ) do (
					item = items.itemof(i)
					namenode = RsGetXmlElement item "name"
					pathnode = RsGetXmlElement item "path"
					
					--extract the template name from both strings and compare them.
					local itemBits = filterString pathnode.InnerText "/"
					
					if (itemBits.count != 0) then
					(
						gTCSTaggerLookup.Add itemBits[itemBits.count] namenode.InnerText 
					)
				)
			)
		)
	)
	
	if (gTCSTaggerLookup != undefined) and (gTCSTaggerLookup.count != 0) and (gTCSTaggerLookup.ContainsKey templateName) then
	(
		local val = undefined
		gTCSTaggerLookup.TryGetValue templateName &val
		
		if (val != undefined) then
		(
			typeName = val
		)
	)
	
	typeName
)

--
-- name: RsUpdateTemplateReferenceFile
-- desc: Update a texture template file; for changed export textures.
--
--  *** DHM: this gets invoked per-TCL/TCS.  We shouldn't be doing Perforce ops here!! ***
--
-- DHM: really only required because we're changing from DDS export textures
--      to TIFFs and the "exportTextures" array needs updating.
--
fn RsUpdateTemplateReferenceFile templatefile exportTexture: changelistNum: = (

	local extension = getFilenameType templatefile
	local outputFile = RsMakeBackSlashes templatefile
	
	if ( ".tcl" == extension ) then
		return ( false )
	
	if ( unsupplied == exportTexture ) then
		return ( true ) -- nothing to do
	
	local xmlDoc = XmlDocument()
	xmlDoc.load outputFile
	
	-- Find our TCP template and update the reference (if its maps_half).
	local parentElements = xmlDoc.document.SelectNodes( "/rage__CTextureConversionSpecification/parent" )
	for i = 0 to (parentElements.Count-1) do
	(
		local parentElement = parentElements.ItemOf( i )
		local oldTemplatePath = "maps_half"
		local newTemplatePath = "maps"
		local templateFilename = parentElement.InnerText
		local mapsHalfIndex = ( findString templateFilename oldTemplatePath )
		if ( undefined != mapsHalfIndex ) then
		(	
			local newTemplateFilename = ( replace templateFilename mapsHalfIndex oldTemplatePath.count newTemplatePath )
			parentElement.InnerText = newTemplateFilename
		)
	)
	
	-- Find our resourceTexture items and update the reference.
	local resourceTextureElements = xmlDoc.document.SelectNodes( "/rage__CTextureConversionSpecification/resourceTextures/Item/pathname" )
	for i = 0 to (resourceTextureElements.Count-1) do
	(
		local resourceTextureElement = resourceTextureElements.ItemOf( i )
		resourceTextureElement.InnerText = exportTexture
	)
	
	xmlDoc.save outputfile
	
	true
)

--
-- name: RsGenerateTemplateReferenceFile
-- desc: Generate a texture template file.
--
--  *** DHM: this gets invoked per-TCL/TCS.  We shouldn't be doing Perforce ops here!! ***
--
-- 	LPXO:  This was written for the ped exporter for the orignal texture pipeline.  Looks like it's just been re-used in a different way for maps and vehicles.
--		Ped export has been updated to reflect changes.
--	
fn RsGenerateTemplateReferenceFile templatefile templatevalue noop:false sourceTextures: exportTexture: changelistNum: useSCC:true = (

	local extension = getFilenameType templatefile
	local outputfile = RsMakeBackSlashes templatefile
	
	RsMakeSurePathExists templatefile
	
	local xmlDoc = XmlDocument()
	xmlDoc.init()
	
	local xmlRoot = xmlDoc.createelement("rage__CTextureConversionSpecification")
	xmlDoc.document.AppendChild xmlRoot
	
	if true == noop then (
		texElem = xmlDoc.createelement("skipProcessing")
		attr = xmlDoc.createattribute "value"
		attr.value = "true"
		texElem.Attributes.append(attr)
	)
	else (
		texElem = xmlDoc.createelement("parent")
		texElem.InnerText = templatevalue
	)
	xmlRoot.AppendChild(texElem)
	
	-- Add resource textures element (if not a TCL file).
	if ( ( ".tcl" != extension ) and ( unsupplied != exportTexture ) ) then
	(
		local ddsHeader_class = dotnetclass "RSG.Base.Textures.DDS.Header"
		local exportTextureCompressedFormat = "Unknown"
		try
		(
			local exportTextureHeader = ddsHeader_class.Load exportTexture
			exportTextureCompressedFormat = exportTextureHeader.PixelFormat.CompressedFormat.ToString()
		)
		catch
		(
			gRsUlog.LogWarning ("Unable to get texture compressed format for "+exportTexture as string)
		)
		
		local resourceTexturesElem = xmlDoc.CreateElement( "resourceTextures" )
		local resourceTextureItemElem = xmlDoc.CreateElement( "Item" )
		local itemTypeAttribute = xmlDoc.CreateAttribute( "type" )
		itemTypeAttribute.value = "rage__CTextureConversionResourceTexture"
		resourceTextureItemElem.Attributes.append( itemTypeAttribute )
		
		local pathnameElem = xmlDoc.CreateElement( "pathname" )
		pathnameElem.InnerText = gRsBranch.Environment.ReverseSubst exportTexture
		resourceTextureItemElem.AppendChild( pathnameElem )
		
		local usageElem = xmlDoc.CreateElement( "usage" )
		usageElem.InnerText = "usage_Default"
		resourceTextureItemElem.AppendChild( usageElem )
		
		local compressionFormatElem = xmlDoc.CreateElement( "compressionFormat" )
		compressionFormatElem.InnerText = exportTextureCompressedFormat
		resourceTextureItemElem.AppendChild( compressionFormatElem )
		
		if ( unsupplied != sourceTextures ) then
		(
			local sourceTexturesElem = xmlDoc.CreateElement( "sourceTextures" )
			for sourceTexture in sourceTextures do
			(
				local sourceTextureItemElem = xmlDoc.CreateElement( "Item" )
				itemTypeAttribute = xmlDoc.CreateAttribute( "type" )
				itemTypeAttribute.value = "rage__CTextureConversionSourceTexture"
				sourceTextureItemElem.Attributes.append( itemTypeAttribute )
				local sourcePathnameElem = xmlDoc.CreateElement( "pathname" )
				sourcePathnameElem.InnerText = gRsBranch.Environment.ReverseSubst sourceTexture
				sourceTextureItemElem.AppendChild( sourcePathnameElem )
				sourceTexturesElem.AppendChild( sourceTextureItemElem )
			)
			
			resourceTextureItemElem.AppendChild( sourceTexturesElem )
			
		)
		
		resourceTexturesElem.AppendChild( resourceTextureItemElem )
		
		xmlRoot.AppendChild( resourceTexturesElem )
		
	)
	
	if ( ".tcl" != extension and useSCC ) then
	(
		-- DHM FIX ME: this sync-for-a-single-file isn't cool.
		gRsPerforce.sync #(outputfile) silent:true
		gRsPerforce.add_or_edit #(outputfile) silent:true
		gRsPerforce.addToChangelist changelistNum #(outputfile) silent:true
		format "Adding to changelist %\n" changelistNum
	)
	xmlDoc.save outputfile
	if ( ".tcl" != extension ) then
	(
		-- DHM FIX ME: this shouldn't be required.
		gRsPerforce.add_or_edit #(outputfile) silent:true
	)
	
	-- Not sure if we'll need debug elems yet so ill leave a sample in here
	/*
	<?xml version="1.0" encoding="UTF-8"?>
	
	<rage__CTextureConversionSpecification>
		<m_parent>$(core_assets)/metadata/textures/templates/globals/Default</m_parent>
		<debugParams>
			<mipFillAmount value="0.500000"/>
			<mipFillStart value="1"/>
			<mipFillBorderThickness value="4"/>
			<testAllImageFormats value="true"/>
			<testColourGradients value="false"/>
			<dumpTarget>dt_None</dumpTarget>
			<dumpPath/>
			<dumpAutoOpen value="true"/>
			<dumpMipsToPage value="false"/>
			<dumpAlpha value="false"/>
			<dumpAlphaToLuminance value="false"/>
			<dumpAlphaSeparate value="false"/>
		</debugParams>
	</rage__CTextureConversionSpecification>
	*/
)

-- Collects list of TCS template-descriptions and paths
fn RsLoadTexTuneConfig &texTuneMapList templatesXmlFilename:( RsConfigGetEtcDir() + "texture/templates.xml" ) = ( --directTCSEdit:false templatesDir:undefined = (
	local xmlDoc = XMlDocument()
	xmlDoc.init()
	xmlDoc.load ( templatesXmlFilename )

	local xmlRoot = xmlDoc.document.DocumentElement
	
	if (xmlRoot != undefined) do  
	(
		refnode = RsGetXmlElement xmlRoot "references"
		items = refnode.childnodes
		for i = 0 to ( items.Count - 1 ) do 
		(
			item = items.itemof(i)
			namenode = RsGetXmlElement item "name"
			pathnode = RsGetXmlElement item "path"

			append texTuneMapList (DataPair desc:namenode.InnerText template:pathnode.InnerText)
		)
	)
)

--
-- func: RsRexExportTexture
-- desc: Wrapper around rex texture export functions.
--
fn RsRexExportTexture texmap outputFile asTiff:false maxsize:undefined = (
	
	local retVal = false
	rexSetULogFile (gRsUlog.Filename())
	if ( true == RsClampTextures and undefined != maxsize ) then
	(
		retVal = rexExportTexture texmap outputFile sizeLimit:maxsize raw:true
	)
	else if ( true == asTiff ) then
	(
		retval = rexExportRawTextureAsTiff texmap outputFile
	)
	else
	(
		retVal = rexExportTexture texmap outputFile raw:true
	)
	retVal
)

fn RsRexExportBumpTexture texmap outputFile maxsize:undefined flag:undefined = (

	local retVal = false
	rexSetULogFile (gRsUlog.Filename())
	if ( true == RsClampTextures and undefined != maxsize and undefined != flag ) then
	(
		retVal = rexExportTexture texmap outputFile sizeLimit:maxsize isBump:flag
	)
	else if ( true == RsClampTextures and undefined != maxsize ) then
	(
		retVal = rexExportTexture texmap outputFile sizeLimit:maxsize isBump:true
	)
	else
	(
		retVal = rexExportTexture texmap outputFile isBump:true
	)
	retVal
)


-- This is used to update TCS files for source, export or template references in them.
-- Optional to provide specific items to change.  This keeps other TCS options in the file (skip processing, etc), 
-- as opposed to just generating again, as we might lose hand-tuned options in the TCS file.
-- Also Kills RS_Assets if it appears.
-- This won't do anything if it can't find existing elements in the xml file - e.g. if all of the resourceTextures part is missing it just skips
-- doing anything with that.
-- forceResourceTexturesToAssets - this is so that if reverseSubst would change it to $(core_assets) if we're in a DLC project, we force it to $(assets)
fn RsUpdateTCSFilePathsWithReverseSubst tcsFilename updateTemplateFilename:true updateResourceTextures:true updateSourceTextures:true forceResourceTexturesToAssets:false forceSourceTexturesToArt:false = (

	-- Nothing to update
	if (updateTemplateFilename == false and updateResourceTextures == false and updateSourceTextures == false) do ( return true ) 
	
	-- File checks
	if (not doesFileExist tcsFilename) do
	(
		gRsULog.LogWarning ("TCS file doesn't exist, so can't update in RsUpdateTCSFilePathsWithReverseSubst! (Is it a new texture that doesn't exist in the main game?):" + tcsFilename)
		return false
	)	
	if (getFileAttribute tcsFilename #readOnly == true) do
	(
		gRsULog.LogMessage ("Setting file to writable, in RsUpdateTCSFilePathsWithReverseSubst: " + tcsFilename)
		setFileAttribute tcsFilename #readOnly false
	)
	
	local xmlDoc = XmlDocument()
	xmlDoc.load tcsFilename
	
	-- Find our TCP template and update
	if (updateTemplateFilename) do 
	(
		local parentElements = xmlDoc.document.SelectNodes( "/rage__CTextureConversionSpecification/parent" )
		
		if (parentElements != undefined) then 
		(
			for i = 0 to (parentElements.Count-1) do
			(
				local parentElement = parentElements.ItemOf(i)
				if (parentElement == undefined) do continue
				-- If updateTemplateFilename provided, use that otherwise load one that's in file already
				local newupdateTemplateFilename = parentElement.InnerText
				
				-- Kill RS_Assets!  Core assets is always used for template .tcp files.
				if (matchpattern newupdateTemplateFilename pattern:"*${RS_ASSETS}*") do
				(
					newupdateTemplateFilename = substituteString newupdateTemplateFilename "${RS_ASSETS}" "$(core_assets)"
				)		
				
				-- $(core_metadata) is not liked by ragebuilder currently.  Core assets is always used for template .tcp files.
				if (matchpattern newupdateTemplateFilename pattern:"*$(core_metadata)*") do
				(
					newupdateTemplateFilename = substituteString newupdateTemplateFilename "$(core_metadata)" "$(core_assets)/metadata"
				)			
				
				-- Don't try and subst if there's already $ in the name otherwise it puts max folder infront
				if (not matchpattern newupdateTemplateFilename pattern:"*$*") do 
				(
					newupdateTemplateFilename = gRsBranch.Environment.ReverseSubst newupdateTemplateFilename
				)
				parentElement.InnerText = newupdateTemplateFilename
			)
		)
		else
		(
			gRsULog.LogError ("TCS file doesn't contain parent element, so can't update in RsUpdateTCSFilePathsWithReverseSubst: " + tcsFilename)
		)
	)
	
	-- Updates the source textures elements, substituting out hardcoded paths
	if (updateResourceTextures) do
	(
		local updateResourceTexturesElements = xmlDoc.document.SelectNodes( "/rage__CTextureConversionSpecification/resourceTextures/Item/pathname" )
		if (updateResourceTexturesElements != undefined ) then
		(
			for i = 0 to (updateResourceTexturesElements.Count-1) do
			(
				local resourceTexturesElement = updateResourceTexturesElements.ItemOf(i)	
				if (resourceTexturesElement == undefined) do continue
				
				-- If updateTemplateFilename provided, use that otherwise load one that's in file already
				local newUpdateResourceTexturesFilename = resourceTexturesElement.InnerText

				-- Kill RS_Assets!
				if (matchpattern newUpdateResourceTexturesFilename pattern:"*${RS_ASSETS}*") do
				(
					newUpdateResourceTexturesFilename = substituteString newUpdateResourceTexturesFilename "${RS_ASSETS}" "$(assets)"
				)		
				
				-- Don't try and subst if there's already $ in the name otherwise it puts max folder infront
				if (not matchpattern newUpdateResourceTexturesFilename pattern:"*$*") do 
				(
					newUpdateResourceTexturesFilename = gRsBranch.Environment.ReverseSubst newUpdateResourceTexturesFilename
				)
				if (forceResourceTexturesToAssets) do
				(
					newUpdateResourceTexturesFilename = substituteString newUpdateResourceTexturesFilename "$(core_assets)" "$(assets)"
				)
				resourceTexturesElement.InnerText = newUpdateResourceTexturesFilename
			)
		)
		else
		(
			gRsULog.LogWarning ("TCS file doesn't contain resource textures element, so can't update in RsUpdateTCSFilePathsWithReverseSubst: " + tcsFilename)
		)
	)
	
	-- Updates the source textures elements, substituting out hardcoded paths
	if (updateSourceTextures) do
	(
		local updateSourceTexturesElements = xmlDoc.document.SelectNodes( "/rage__CTextureConversionSpecification/resourceTextures/Item/sourceTextures/Item/pathname" )
		if (updateSourceTexturesElements != undefined ) then
		(
			for i = 0 to (updateSourceTexturesElements.Count-1) do
			(
				local sourceTexturesElement = updateSourceTexturesElements.ItemOf(i)
				if (sourceTexturesElement == undefined) do continue
				
				local sourceTexturesFilenames = filterstring sourceTexturesElement.InnerText "+" 
				local newSourceTexturesFilename = ""
				
				-- Might be a combined texture so split, sub and add together again
				for sourceTexturesFilename in sourceTexturesFilenames do
				(
					-- Don't try and subst if there's already $ in the name otherwise it puts max folder infront
					if (not matchpattern sourceTexturesFilename pattern:"*$*") do 
					(
						sourceTexturesFilename = gRsBranch.Environment.ReverseSubst sourceTexturesFilename
					)
					if (forceSourceTexturesToArt) do
					(
						sourceTexturesFilename = substituteString sourceTexturesFilename "$(core_art)" "$(art)"
					)
										
					newSourceTexturesFilename += sourceTexturesFilename
					
					-- Add the plus back in...
					if (sourceTexturesFilenames.count == 2) do ( newSourceTexturesFilename += "+" )
				)
				
				sourceTexturesElement.InnerText = newSourceTexturesFilename
			)
		)
		else
		(
			gRsULog.LogWarning ("TCS file doesn't contain source textures element, so can't update in RsUpdateTCSFilePathsWithReverseSubst: " + tcsFilename)
		)			
	)
	
	xmlDoc.save tcsFilename	
	true
)

-- Same as above but supplied with two asset directories, one from and one to, this lets you reverseSubst a path from a different project.
-- Do not use for important things, it's hacky and takes two hard coded asset paths.
fn RsUpdateTCSFilePathsWithReverseSubstCrossProjectHack tcsFilename assetFrom assetTo updateTemplateFilename:true updateResourceTextures:true updateSourceTextures:true forceResourceTexturesToAssets:false forceSourceTexturesToArt:false = (

	-- Nothing to update
	if (updateTemplateFilename == false and updateResourceTextures == false and updateSourceTextures == false) do ( return true ) 
	
	-- File checks
	if (not doesFileExist tcsFilename) do
	(
		gRsULog.LogWarning ("TCS file doesn't exist, so can't update in RsUpdateTCSFilePathsWithReverseSubst! (Is it a new texture that doesn't exist in the main game?):" + tcsFilename)
		return false
	)	
	if (getFileAttribute tcsFilename #readOnly == true) do
	(
		gRsULog.LogMessage ("Setting file to writable, in RsUpdateTCSFilePathsWithReverseSubst: " + tcsFilename)
		setFileAttribute tcsFilename #readOnly false
	)
	




	local xmlDoc = XmlDocument()
	xmlDoc.load tcsFilename
	
	-- Find our TCP template and update
	if (updateTemplateFilename) do 
	(
		local parentElements = xmlDoc.document.SelectNodes( "/rage__CTextureConversionSpecification/parent" )
		
		if (parentElements != undefined) then 
		(
			for i = 0 to (parentElements.Count-1) do
			(
				local parentElement = parentElements.ItemOf(i)
				if (parentElement == undefined) do continue
				-- If updateTemplateFilename provided, use that otherwise load one that's in file already
				local newupdateTemplateFilename = parentElement.InnerText
				
				-- Kill RS_Assets!  Core assets is always used for template .tcp files.
				if (matchpattern newupdateTemplateFilename pattern:"*${RS_ASSETS}*") do
				(
					newupdateTemplateFilename = substituteString newupdateTemplateFilename "${RS_ASSETS}" "$(core_assets)"
				)		
				
				-- $(core_metadata) is not liked by ragebuilder currently.  Core assets is always used for template .tcp files.
				if (matchpattern newupdateTemplateFilename pattern:"*$(core_metadata)*") do
				(
					newupdateTemplateFilename = substituteString newupdateTemplateFilename "$(core_metadata)" "$(core_assets)/metadata"
				)			
				
				-- Don't try and subst if there's already $ in the name otherwise it puts max folder infront
				if (not matchpattern newupdateTemplateFilename pattern:"*$*") do 
				(
					newupdateTemplateFilename = gRsBranch.Environment.ReverseSubst newupdateTemplateFilename
				)
				parentElement.InnerText = newupdateTemplateFilename
			)
		)
		else
		(
			gRsULog.LogError ("TCS file doesn't contain parent element, so can't update in RsUpdateTCSFilePathsWithReverseSubst: " + tcsFilename)
		)
	)
	
	-- Updates the source textures elements, substituting out hardcoded paths
	if (updateResourceTextures) do
	(
		local updateResourceTexturesElements = xmlDoc.document.SelectNodes( "/rage__CTextureConversionSpecification/resourceTextures/Item/pathname" )
		if (updateResourceTexturesElements != undefined ) then
		(
			for i = 0 to (updateResourceTexturesElements.Count-1) do
			(
				local resourceTexturesElement = updateResourceTexturesElements.ItemOf(i)	
				if (resourceTexturesElement == undefined) do continue
				
				-- If updateTemplateFilename provided, use that otherwise load one that's in file already
				local newUpdateResourceTexturesFilename = resourceTexturesElement.InnerText


				--------------------------------------------------------------
				-- New super hacky bit
				--------------------------------------------------------------
				format "newUpdateResourceTexturesFilename: %\n" (newUpdateResourceTexturesFilename as string)

				if( newUpdateResourceTexturesFilename != undefined and isKindOf newUpdateResourceTexturesFilename string)then (
					newUpdateResourceTexturesFilename = substituteString newUpdateResourceTexturesFilename assetFrom assetTo
				)

				format "newUpdateResourceTexturesFilename: %\n" (newUpdateResourceTexturesFilename as string)
				

				--------------------------------------------------------------
				-- End super hacky bit
				--------------------------------------------------------------

				
				-- Kill RS_Assets!
				if (matchpattern newUpdateResourceTexturesFilename pattern:"*${RS_ASSETS}*") do
				(
					newUpdateResourceTexturesFilename = substituteString newUpdateResourceTexturesFilename "${RS_ASSETS}" "$(assets)"
				)		
				
				-- Don't try and subst if there's already $ in the name otherwise it puts max folder infront
				if (not matchpattern newUpdateResourceTexturesFilename pattern:"*$*") do 
				(
					newUpdateResourceTexturesFilename = gRsBranch.Environment.ReverseSubst newUpdateResourceTexturesFilename
				)
				if (forceResourceTexturesToAssets) do
				(
					newUpdateResourceTexturesFilename = substituteString newUpdateResourceTexturesFilename "$(core_assets)" "$(assets)"
				)
				resourceTexturesElement.InnerText = newUpdateResourceTexturesFilename
			)
		)
		else
		(
			gRsULog.LogWarning ("TCS file doesn't contain resource textures element, so can't update in RsUpdateTCSFilePathsWithReverseSubst: " + tcsFilename)
		)
	)
	
	-- Updates the source textures elements, substituting out hardcoded paths
	if (updateSourceTextures) do
	(
		local updateSourceTexturesElements = xmlDoc.document.SelectNodes( "/rage__CTextureConversionSpecification/resourceTextures/Item/sourceTextures/Item/pathname" )
		if (updateSourceTexturesElements != undefined ) then
		(
			for i = 0 to (updateSourceTexturesElements.Count-1) do
			(
				local sourceTexturesElement = updateSourceTexturesElements.ItemOf(i)
				if (sourceTexturesElement == undefined) do continue
				
				local sourceTexturesFilenames = filterstring sourceTexturesElement.InnerText "+" 
				local newSourceTexturesFilename = ""
				
				-- Might be a combined texture so split, sub and add together again
				for sourceTexturesFilename in sourceTexturesFilenames do
				(
					-- Don't try and subst if there's already $ in the name otherwise it puts max folder infront
					if (not matchpattern sourceTexturesFilename pattern:"*$*") do 
					(
						sourceTexturesFilename = gRsBranch.Environment.ReverseSubst sourceTexturesFilename
					)
					if (forceSourceTexturesToArt) do
					(
						sourceTexturesFilename = substituteString sourceTexturesFilename "$(core_art)" "$(art)"
					)
										
					newSourceTexturesFilename += sourceTexturesFilename
					
					-- Add the plus back in...
					if (sourceTexturesFilenames.count == 2) do ( newSourceTexturesFilename += "+" )
				)
				
				sourceTexturesElement.InnerText = newSourceTexturesFilename
			)
		)
		else
		(
			gRsULog.LogWarning ("TCS file doesn't contain source textures element, so can't update in RsUpdateTCSFilePathsWithReverseSubst: " + tcsFilename)
		)			
	)
	
	xmlDoc.save tcsFilename	
	true
)