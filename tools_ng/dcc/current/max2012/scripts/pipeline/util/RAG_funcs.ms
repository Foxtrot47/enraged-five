--
-- File:: pipeline/util/RAG_funcs.ms
-- Description:: RAG game-access functions
--
-- Author:: Neal D Corbett <neal.corbett@rockstarleeds.com>
-- Date:: 05 October 2011
--

global RsRagFuncs

struct RsRagFuncsStruct 
(
	isConnected = false,
	connectionTimer = undefined, --Timer used to periodically verify that a connection has occurred.
	timerInterval = 5000, --Tick every 5,000 milliseconds 

	-- Called when the user disconnects from the RAG proxy
	fn OnDisconnect =
	(	
		if connectionTimer != undefined then
		(
			--Stop the ongoing timer.
			connectionTimer.Stop()
		)
		
		RemoteConnection.Disconnect()			
		isConnected = false
	),
	
	-- Tick called every 5 seconds to enure the remote connection is still valid
	fn OnTick s e =
	(
		if (RemoteConnection.IsConnected() == false and isConnected == true) then
		(
			--If connection has faltered, then notify the UI.
			format "Lost connection to game!\n"
			OnDisconnect()
		)
	),

	-- Called when the user attempts to connect to the RAG proxy
	fn OnConnect =
	(
		isConnected = false
		
		-- Call into DLL to establish connection.	
		connectionResult = RemoteConnection.Connect()
		
		print (connectionResult as string)
		
		if connectionResult == false then
		(
			format "Unable to establish connection to game!\n"
		)
		else
		(
			--Create a periodic timer to ping the connection status in the assembly.
			connectionTimer = dotNetObject "System.Windows.Forms.Timer"
			dotnet.addEventHandler connectionTimer "tick" OnTick
			connectionTimer.Interval = timerInterval
			connectionTimer.Start()
			
			isConnected = true
		)
		
		return isConnected
	),
	
	fn runString cmd = 
	(
		RemoteConnection.Connect()
		if( RemoteConnection.IsConnected() ) then
		(
			RemoteConnection.SendCommand ( cmd as String )
		)	
	),
	
	fn setPlayerPos pos dir:0.0 velocity:[0.0,0.0,0.0] = 
	(
		local cmd = stringStream ""
		format "widget \"\_LiveEdit_\Bugstar\Warp Player x y z h vx vy vz\" \"% % % % % % %\"\n" pos.x pos.y pos.z -dir velocity.x velocity.y velocity.z to:cmd
		
		runString cmd
	),
	
	--/////////////////////////////////////////////////////////////////////////////
	-- Ensures that the camera widgets exists
	--/////////////////////////////////////////////////////////////////////////////
	fn CreateCameraWidgets =
	(
		RemoteConnection.WriteBoolWidget "Camera/Create camera widgets" true
		RemoteConnection.SendSyncCommand()	
	),
	
	camPosWidgetX = "Camera/Free camera/Position X",
	camPosWidgetY = "Camera/Free camera/Position Y",
	camPosWidgetZ = "Camera/Free camera/Position Z",
	
	--/////////////////////////////////////////////////////////////////////////////
	-- Return the position of the game free(debug) camera
	--/////////////////////////////////////////////////////////////////////////////
	fn getGameDebugCamPos =
	(
		RemoteConnection.Connect()
		if( RemoteConnection.IsConnected() ) then
		(
			CreateCameraWidgets()
			local camXPos = RemoteConnection.ReadFloatWidget camPosWidgetX
			local camYPos = RemoteConnection.ReadFloatWidget camPosWidgetY
			local camZPos = RemoteConnection.ReadFloatWidget camPosWidgetZ
			
			return [camXPos, camYPos, camZPos]
		)
		else
		(
			return [0,0,0]
		)
				
	),
	
	camRotWidgetX = "Camera/Free camera/Orientation X",
	camRotWidgetY = "Camera/Free camera/Orientation Y",
	camRotWidgetZ = "Camera/Free camera/Orientation Z",
	
	--/////////////////////////////////////////////////////////////////////////////
	-- Return the rot of the game free(debug) camera
	--/////////////////////////////////////////////////////////////////////////////
	fn getGameDebugCamRot =
	(
		RemoteConnection.Connect()
		if( RemoteConnection.IsConnected() ) then
		(
			CreateCameraWidgets()
			local camXRot = RemoteConnection.ReadFloatWidget camRotWidgetX
			local camYRot = RemoteConnection.ReadFloatWidget camRotWidgetY
			local camZRot = RemoteConnection.ReadFloatWidget camRotWidgetZ
			
			return eulerAngles (camXRot + 90) camYRot camZRot
		)
		else
		(
			return eulerAngles 0 0 0
		)
	),
	
	--/////////////////////////////////////////////////////////////////////////////
	-- Sets the game debug camera position
	--/////////////////////////////////////////////////////////////////////////////
	fn setGameDebugCamPos InputPos =
	(	
		RemoteConnection.Connect()
		if( RemoteConnection.IsConnected() ) then
		(
			CreateCameraWidgets()
			RemoteConnection.WriteBoolWidget "Camera/Free camera/Override streaming focus" true
			RemoteConnection.WriteFloatWidget camPosWidgetX InputPos.X
			RemoteConnection.WriteFloatWidget camPosWidgetY InputPos.Y
			RemoteConnection.WriteFloatWidget camPosWidgetZ InputPos.Z
		)
	),
	
	--/////////////////////////////////////////////////////////////////////////////
	-- Sets the game debug camera rotation
	--/////////////////////////////////////////////////////////////////////////////
	fn setGameDebugCamRot InputRot =
	(	
		RemoteConnection.Connect()
		if( RemoteConnection.IsConnected() ) then
		(
			CreateCameraWidgets()
			RemoteConnection.WriteFloatWidget camRotWidgetX (InputRot.X - 90)
			RemoteConnection.WriteFloatWidget camRotWidgetY InputRot.Y
			RemoteConnection.WriteFloatWidget camRotWidgetZ InputRot.Z
		)
	)
)

RsRagFuncs = RsRagFuncsStruct()
--RsRagFuncs.setPlayerPos $.pos dir:-($.rotation as eulerangles).z velocity:[0.0, 0.0, 0.0]
