--
-- File:: pipeline/util/radiosity_lighting.ms
-- Description::  Radiosity and lighting tool
--
-- 9/2/10
-- by Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
--
-----------------------------------------------------------------------------


-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/ui/vertex_col_ui.ms"
filein "pipeline/helpers/maps/ZColourer.ms"
filein "pipeline/util/radiosity.ms"

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout SetupRoll "Setup"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Radiosity_and_Lighting" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	button btnPrepScene "Prepare Scene" width:80 height:25 tooltip:"Set up the scene" pos:[5,30]
	button btnPrepLights "Prepare Lights" width:80 height:25 tooltip:"Set up the lights correctly for emissive shaders" pos:[93,30]
	button btnCopyVertIllum "Copy Vert to Illum" width:100 height:25 tooltip:"Copy the vertex colouring to the illumination" pos:[180,30]
	checkbutton chkbtnToggleRageLights "Toggle Rage Lights: " width:180 height:25 checked:false
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	fn PrepareScene = (
		local fileClass = dotNetClass "System.IO.File"
		
		-- Create and assign the log exposure control
		log_exposure = Logarithmic_Exposure_Control()
		SceneExposureControl.exposureControl = log_exposure
		
		-- Set the exposure settings
		log_exposure.active = true
		log_exposure.exteriorDaylight = true
		log_exposure.brightness = 95.0
		log_exposure.contrast = 100.0
		log_exposure.midTones = 0.2
		log_exposure.physicalScale = 1500.0
		
		while undefined!=$RsRadiosityLight do delete $RsRadiosityLight
		local lightFile = ""
		
		-- Different locations depending on if outsource or not
		if ( not gRsIsOutSource ) then
		(
			lightFile = (( RsConfigGetArtDir() ) + "Models/Handy objects/New_Radiosity_Setup.max" ) 
			gRsPerforce.sync #( lightFile ) silent:true
		)
		else
		(
			lightFile = (( getdir #scripts ) + "/maxfiles/New_Radiosity_Setup.max" ) 
		)
		
		if not (doesFileExist lightFile) then 
		(		
			messagebox (lightFile + " file not found")
		)
		else
		(
			-- Max file RsRadiosityLight merged in
			mergeMAXFile lightFile
		)
	)
	
	fn ToggleRageLights state = (
	
		for obj in $objects do
		(
			if ( classof obj == RageLight ) do
			(
				obj.lightEnabled = state
				obj.enabled = state
				obj.isHidden = not state
			)
		)
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnPrepScene pressed do (
		PrepareScene()
	)
	
	on btnPrepLights pressed do (
		LightsOn radiositySetTime
	)
	
	on btnCopyVertIllum pressed do (
		ButtonCopyVertIllum()
	)
	
	on chkbtnToggleRageLights changed newState do (
		
		if ( newState == true ) then 
		(
			chkbtnToggleRageLights.text = "Toggle Rage Lights: On/Visible"
		)
		else
		(
			chkbtnToggleRageLights.text = "Toggle Rage Lights: Off/Hidden"
		)
		ToggleRageLights newState
	)
	
) --rollout SettingsRoll


rollout RadiosityRoll "Radiosity"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	button btnPrepMat "Prepare Materials" pos:[10,10] width:110 height:25
	checkbox chkIgnorePolys "Ignore Emissive Polys" checked:true align:#left pos:[140,15]
	
	
	button btnResetAll "Reset All" pos:[10,45] width:60 height:25 enabled:false
	button btnReset "Reset" 		pos:[80,45] width:60 height:25 enabled:false
	button btnStart "Start" 		pos:[150,45] width:60 height:25
	button btnStop "Stop" 			pos:[220,45] width:60 height:25
	
	groupBox grpSettings "Settings" pos:[10,80] width:270 height:130
	spinner spnQuality "" pos:[210,100] range:[0.0,100.0,90.0] width:50 height:16
	label lblQuality "Initial Quality(%):" pos:[20,100] width:100 height:15
	--label lblPercent "%" pos:[408,130] width:20 height:18
	
	spinner spnRefineAll "" pos:[210,120] width:50 height:16 range:[0,100,0] type:#integer
	label lblRefineAll "Refine Iterations (All Objects):" pos:[20,120] width:155 height:15
	
	spinner spnRefineSelected "" pos:[210,140] width:50 height:16 range:[0,100,0] type:#integer
	label lblRefineSelect "Refine Iterations (Selected Objects):" pos:[20,140] width:191 height:15
	
	spinner spnIndirectLight "" pos:[210,160] width:50 height:16 range:[0,100,3] type:#integer
	label lblIndirectLight "Indirect Light Filtering:" pos:[20,160] width:118 height:15
	
	spinner spnDirectLight "" pos:[210,180] width:50 height:16 range:[0,100,3] type:#integer
	label lblDirectLight "Direct Light Filtering:" pos:[20,180] width:118 height:15
	
	checkbox chkZeroIllum "Blacken Illum Channel" checked:true pos:[10, 220]
	button btnApply "Apply Radiosity" pos:[10,250] width:130 height:25 
	button btnRestore "Restore Materials" pos:[150,250] width:130 height:25
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	fn CheckMaterials = (
		if RsStoreHandles.count > 0 then (
			messagebox "Restoring materials"
			RestoreMaterials()
		)
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Prepare/Restore Materials
	--------------------------------------------------------------
	on btnPrepMat pressed do (
		ButtonPrepareMat chkIgnorePolys.checked
	)
	
	on btnRestore pressed do (
		RestoreMaterials()
	)
	
	--------------------------------------------------------------
	-- Apply Radiosity
	--------------------------------------------------------------
	on btnApply pressed do (
		ButtonApplyRadiosity chkIgnorePolys.checked chkZeroIllum.checked
	)
	
	--------------------------------------------------------------
	-- Radiosity
	--------------------------------------------------------------
	on btnStart pressed do (
		btnStart.enabled = false
		
		SceneRadiosity.radiosity.Start()
		
		btnResetAll.enabled = true
		btnReset.enabled = true
		
	)
	
	on btnStop pressed do (
		SceneRadiosity.radiosity.Stop()
	)
	
	on btnResetAll pressed do (
		SceneRadiosity.radiosity.Reset true false
		
		btnStart.enabled = true
		btnResetAll.enabled = false
		btnReset.enabled = false
	)
	
	on btnReset pressed do (
		SceneRadiosity.radiosity.Reset false false
		
		btnStart.enabled = true
		btnResetAll.enabled = true
		btnReset.enabled = false
	)
	
	on spnQuality changed newValue do (
		SceneRadiosity.radiosity.radInitialQuality = newValue
	)
	
	on spnRefineAll changed newValue do (
		SceneRadiosity.radiosity.radGlobalRefineSteps = newValue
	)
	
	on spnRefineSelected changed newValue do (
		SceneRadiosity.radiosity.radSelectionRefineSteps = newValue
	)
	
	on spnIndirectLight changed newValue do (
		SceneRadiosity.radiosity.radFiltering = newValue
	)
	
	on spnDirectLight changed newValue do (
		SceneRadiosity.radiosity.radDirectFiltering = newValue
	)
	
	--------------------------------------------------------------
	-- Rollout open/close
	--------------------------------------------------------------
	on RadiosityRoll open do (
		callbacks.addScript #filePreSave "RadiosityRoll.CheckMaterials()" id:#radiositySave
		
		-- Create radiosity
		SceneRadiosity.radiosity = Radiosity()
	)
	
	on RadiosityRoll close do (
		callbacks.removeScripts id:#radiositySave
		CheckMaterials()
	)
) --rollout RadiosityRoll


rollout VertRoll "Vertex Lighting"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	groupBox grpBrighten "Vertex Brighten" pos:[10,10] width:270 height:60
	checkbox chkBrightVert "Vert Colour" pos:[20,35]  enabled:true checked:true
	checkbox chkBrightIllum "Illum" pos:[98,35]  checked:true
	hyperlink lnkHelp "Help?" pos:[250,35] address:"https://devstar.rockstargames.com/wiki/index.php/Radiosity_and_Lighting#Vertex_Lighting" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	spinner spnBrightAmount "" pos:[142,25] width:50 height:20 range:[0,100,0] scale:1.0
	button btnBrightGo "Abs" pos:[195,23] width:50 height:20 tooltip:"Modify all of the vert colours by the given absolute amount"
	spinner spnBrightAmountRel "" pos:[142,47] width:50 height:20 range:[-100,100,0] scale:1.0
	button btnBrightGoRel "Rel" pos:[195,45] width:50 height:20 tooltip:"Modify all of the vert colours by the given amount relative to the current value"
	
	groupBox grpCopyPaste "Copy/Paste" pos:[10,85] width:270 height:90
	checkbox chkCPVert "Vert Colour" pos:[20,113] checked:true
	checkbox chkCPIllum "Illum Colour" pos:[20,143] checked:true
	colorPicker cpVert "" pos:[110,110] width:40 height:25 color:(color 0 0 155)
	colorPicker cpIllum "" pos:[110,140] width:40 height:25 color:(color 0 0 155)
	button btnCPCopy "Copy" pos:[180,110] width:80 height:25 tooltip:"Copy the selected verts' colours"
	button btnCPPaste "Paste" pos:[180,140] width:80 height:25 tooltip:"Paste the selected verts' colours"
	
	
	groupBox grpZColouring "Z Colouring" pos:[10,190] width:270 height:120
	colorPicker cpFade "Fade colour: " pos:[20,220] width:100 height:25 color:(color 0 0 155)
	spinner spnTopZ "Top Fade" pos:[210,218] width:55 height:16 range:[0,100,0]
	spinner spnBottomZ "Bottom Fade" pos:[210,240] width:55 height:16 range:[0,100,0]
	radioButtons rdoType "Type" pos:[20,260] width:120 height:46 labels:#("Vertex colour", "Illumination") 
	button btnZGo "Go" pos:[220,270] width:50 height:25 tooltip:"Set the vert colours of an object to a value across a Z distance"
	
	groupBox grpMatchVert "Match Vertex Colours" pos:[10,310] width:270 height:70
	dropdownlist cboSmthGrps "Smoothing Group" items:#("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20") pos:[20,330] width:100 
	button btnMatchGo "Go" pos:[220,340] width:50 height:25 tooltip:"Evens out vert lighting of matching verts on different objects"
	
	button btnCopyVertColour "Copy Vertex Colours" pos:[50,390] width:200 height:25 tooltip:"Copies Vertex Lighting to Vertex Illumination"
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Brighten
	--------------------------------------------------------------
	on btnBrightGo pressed do (
		for obj in selection do (
	
			if classof obj == Editable_Mesh then 
			(
				GtaChangeColAbs obj spnBrightAmount.value chkBrightVert.checked chkBrightIllum.checked false
			)
			else
			(
				local ss = stringStream ""
				format "Error: object % vertices not changed because its not an Editable_Mesh." obj.name to:ss
				MessageBox (ss as string) title:"Error"
			)
		)
	)
	on btnBrightGoRel pressed do (
		for obj in selection do (
	
			if classof obj == Editable_Mesh then 
			(
				GtaChangeColAbs obj spnBrightAmountRel.value chkBrightVert.checked chkBrightIllum.checked true
			)
			else
			(
				local ss = stringStream ""
				format "Error: object % vertices not changed because its not an Editable_Mesh." obj.name to:ss
				MessageBox (ss as string) title:"Error"
			)
		)
	)
	
	--------------------------------------------------------------
	-- Copy/Paste
	--------------------------------------------------------------
	on btnCPCopy pressed do (
		colours = CopyButton chkCPVert.checked chkCPIllum.checked
		cpVert.color = colours[1]
		cpIllum.color = colours[2]
	)
	
	on btnCPPaste pressed do (
		PasteButton cpVert.color cpIllum.color chkCPVert.checked chkCPIllum.checked
	)
	
	--------------------------------------------------------------
	-- Z Colourer
	--------------------------------------------------------------
	on btnZGo pressed do (
		GoZColourButton cpFade.color rdoType.state spnTopZ.value spnBottomZ.value
	)
	
	--------------------------------------------------------------
	-- Match Vertex Colours
	--------------------------------------------------------------
	on btnMatchGo pressed do (
		matchvertcolours (selection as array) (cboSmthGrps.selected as integer)
			for sel in (selection as array) do (
				print sel
				sel.vertexColorType = 1
				forceCompleteRedraw()
				sel.vertexColorType = 0
				forceCompleteRedraw()

			)
	)
	
	--------------------------------------------------------------
	-- Copy Vertex Colours
	--------------------------------------------------------------
	on btnCopyVertColour pressed do (
		GtaCopyVertColours()
	)
) --rollout VertRoll


rollout LightingRoll "Lighting"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	groupBox grpLightIntensify "Light Intensify" pos:[10,10] width:270 height:60
	spinner spnPercentage "%: " pos:[30,35] width:60 height:16
	button btnIntensity "Light Intensity" pos:[100,30] width:80 height:27 tooltip:"Modify the light intensity by the given percentage"
	button btnColour "Light Colour" pos:[190,30] width:80 height:27 tooltip:"Modify the light colour by the given percentage"
	
	button btnLightWire "Light Wire by Light Colour" pos:[60,90] width:160 height:25 tooltip:"Changes wireframe colour to match the light colour"
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Light Intensify
	--------------------------------------------------------------
	on btnIntensity pressed  do (
		for repeat = 1 to selection.count do (
			try  (
				oldmult = selection[repeat].multiplier 
				newmult = oldmult*spnPercentage.value / 100.0
				selection[repeat].multiplier = newmult
			)
				
			catch ()
		)
	)
	
	on btnColour pressed  do (
		for repeat = 1 to selection.count do (
			try (
				oldmult = selection[repeat].rgb 
				newmult = oldmult*spnPercentage.value / 100.0
				selection[repeat].rgb = newmult
			)
			
			catch ()
		)
	)
	
	--------------------------------------------------------------
	-- Light Wire by Light Colour
	--------------------------------------------------------------
	on btnLightWire pressed do (
		ColourLightWireByLightColour()
	)
) --rollout LightingRoll

try CloseRolloutFloater RadiosityMainRoll catch()
RadiosityMainRoll = newRolloutFloater "Radiosity and Lighting" 300 500 50 126
addRollout SetupRoll RadiosityMainRoll rolledup:false
addRollout RadiosityRoll RadiosityMainRoll rolledup:false
addRollout VertRoll RadiosityMainRoll rolledup:false
addRollout LightingRoll RadiosityMainRoll rolledup:false
