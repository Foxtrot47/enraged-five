--
-- RSref settings
-- Rockstar Leeds
-- 28/02/2011
-- by Neal D Corbett
--
-- Values and functions for the RSref system's main settings
--

-----------------------------------------------------------------------------
-- GLOBAL VARIABLES 
-----------------------------------------------------------------------------
global RSrefVersion = 24 -- Scripted objects use this version-number.  Changing this causes cached models and Metafiles to be invalidated and re-read.
global RSrefUpdateCacheVersion = 18 -- (cache-object format version) Cache-files are all updated when this changes, and earlier object-update cache-objects are ignored
global RSrefUpdateCacheFileVersion = 20 -- (cache-file format version) Object-update cache-files are all updated when this changes

global RSmaxfilingNow = false -- Used to let functions know that maxfile-loading/saving/etc is underway, and it's not safe for them to run
global RSnonUserMerge = false -- Used to differentiate a file-merge called by the RSref system and by the user
global RSrefMidFileEvent = false -- Set to true by pre-file-events, set to false by post-file-events
global RsRefsWaitingForRedrawLoad = false -- Set to true when waiting for RsRefFuncs.startLoadRSrefData() to be triggered by redraw
global RSrefShuttingDown = false -- Set to true by #preSystemShutdown callback
global RSrefScriptsOutOfDate = false -- Set to true if an object is loaded that has a higher version-number than this script

-- Is Rsref system active by default?
global RSrefAllowUpdate = (not gRsIsOutSource)

-- Allow Rsref-system to check for updates on Perforce?
global RSrefAllowP4Check = true

-- These arrays are maintained by node-callback function RSobjNodeEventCallback
global RSNodeMeshChangeTimes
global RSNodeMatChangeTimes

-- Loading-options
global RSrefTurnOffXrefs = false			-- [default: False] Tells function RSrefFileCallbacks.xrefPreMerge() to deactivate all xref-records on maxfile-load
global RSrefDelStoresOnLoad = true		-- [default: True]
global RSrefUseFileCallbacks = true		-- [default: True] Use to turn off file-access callbacks for debug purposes

-- Debug-text switches [default: False]
global RSrefFunctionDebug = false				-- Prints debug info for main functions
global RSrefBuildMeshDebug = false			-- Prints buildMesh debug info for RSref objects that have their DebugPrint tickybox ticked
global RSrefFileCallbackDebugPrint = false	-- Prints debug info for file-access callbacks
global RSrefNodeCallbackDebugPrint = false	-- Prints debug info when node callbacks are triggered (ie. mesh/material changes)
global RSrefSlavesDebug = false				-- Prints debug info when RsRef slave-object functions are called

global RSrefSettingsShowDebug	-- Sets whether or not the the Debug settings are visible on Settings dialog
if (RSrefSettingsShowDebug == undefined) do (RSrefSettingsShowDebug = false)

global RSrefDisplayRoll 					-- Allow this rollout to be global
global RSrefIgnoreDisplayWarnings	-- Turned on by default if Outsource tools, otherwise turned off
if (RSrefIgnoreDisplayWarnings == undefined) do (RSrefIgnoreDisplayWarnings = gRsIsOutSource)

----------------------------------------------------------
-- RsRefDef
--	This struct is used to define object-references
----------------------------------------------------------
struct RSrefDef 
(
	name = "",		-- Name of the object; capitalisation is taken from the source Maxfile
	num,				-- Index of RSref in RSrefData.refs list

	-- SceneXml data for child-objects:
	childElems,
	
	maxfileNum,	-- Index of object's source Maxfile, in array RSrefData.maxfiles
	meshObj, material,	-- Stores the object's mesh and material
	altMeshObj, -- Stores the end-state of a stated animated object
	meshTime = 0, matTime = 0,	-- last-changed timestamps for mesh and material, retrieved from source Maxfile
	meshOutOfDate = True, matOutOfDate = True,	-- indicates that the mesh or material needs to be updated from its Maxfile

	objType = #default,	-- Set to non-default for special objecttypes, such as MILOs or stated-animation objects
	badXform = #{},					-- Bits are set to true if the source-model's pos/rot/scale xforms need resetting
	offsetXform = (matrix3 1),	-- Stores offset pivot-Xforms, if found
	
	comboLod, 						-- Used to store general ComboLodder data (data is defined by that tool)
	slodRefs = #(), 				-- Stores Lod-refs used by the ComboLod Generator
	drawableLodRefs = #(), 	-- Stores drawable-lod refs, used by ComboLodder to get these lod-distances
	hdRef, 							-- Used to store link to hd-object, if this is a lod-ref.
	
	drawRef, 						-- Set to subref whose model will be drawn (undefined if drawing this ref's model)

	maxFaceSize, -- (soon to be obsolete, due to be added to "comboLod" value)
	
	-- Object properties loaded from metafiles:
	guid = 0, 
	txd, iplGroup,
	tags = #(),
	isDynamic = false,
	isFragment = false,
	isLOD = false, 
	lodDistance, childLodDistance,
	
	-- Slave-objects data:
	hasLights = false, hasScenario = false,	
	lights, bakeLights, spawnPoints,
	
	allowNonUniScale = false,
	
	nodeMatchData, -- Source-object's node-names/guids, loaded by RsRefFuncs.getRefNodeNames
	
	-- Values used to change how object is dealt with in RSref Browser:
	isPrefab = false, isPlaceable = true, isBrowsable = true,

	obsolete = False,					-- Set to true if objectname is found by readPropSwapsList
	swapData,							-- Data for doing obsolete-objects swap/delete
	
	badRef = False,					-- True if the model failed to load for some reason
	notFoundInMaxfile = False,	-- True if the model specifically failed to load because it wasn't in its source Maxfile
	
	paletteFile,				-- Object's palette-file
	userPalFile = False,		-- If palette-file isn't in "user_defined_palettes" folder, it won't contain multiple palettes
	paletteCount = 0,		-- Number of palettes in this object's palette-file (0 if no file has been found, or file only has default palette)
	tintMeshObjs,			-- Stores a seperate mesh for each palette

	sourceXform,			-- Position of source-object in its scene (used by RsContainerLodRef objects)

	boundingBox = #(-1,-1,0,1,1,2), 	-- Used for drawing bounding-boxes, taken from the metafiles
	volume,

	thumbFile, 			-- Used by RSrefFuncs.getThumbFile to store object's thumbnail path
	scaleThumbFile,		-- Store object's scale-object thumbnail path, if it has one
	
	-- Functions look up this refdef's source-Maxfile/Metafile:
	fn GetMaxFilename = 
	(
		if (MaxFileNum == undefined) or (MaxFileNum == 0) then undefined else ::RsRefData.AllMaxFiles[MaxFileNum]
	),
	fn GetMetaFilename = 
	(
		if (MaxFileNum == undefined) or (MaxFileNum == 0) then undefined else ::RsRefData.MetaFiles[MaxFileNum]
	)
)

--------------------------------------------------------------------------------------------------
-- RsRefData
-- 	An instance of this struct is used to collect all current RSref-related info
-- 	  (equivalent to Xref-system's file-records)
-- 	'RSrefFuncs.reset' (re)initialises instance of this struct to global 'RSrefData'
--------------------------------------------------------------------------------------------------
(
	-- 'RSrefData' is instance of 'RSrefDataStruct', initially defined by function 'RSrefFuncs.Reset'
	--   (we're defining it in brackets like this to stop it printing to the Listener every time this script is re-run)
	global RSrefData
	OK
)
struct RSrefDataStruct 
(
	refs = #(), 					-- List of all available object RSrefDefs, originally loaded from the metafiles
	refNames = #(), 				-- Lower-case names of all the refs
	
	notLoaded = True, 			-- Indicates whether the RSref database has been activated or not. Set to 'False' by RSrefFuncs.loadRefDefData()
	dontLoadData = False, 		-- Used to only allow one event-triggered data-load at once
	
	objWarnings,					-- Used to mark bad-ref objects in viewport
	
	maxfiles = #(), 				-- List of maxfiles that go with current-maxfile's appropriate metafiles
	FileIsDLC = #{},				-- Indices of DLC maxfiles
	
	DontAskSyncFilenames = #(),	-- List of filenames/indices that user has already synced or has chosen not to sync
	DontAskSyncMaxIdxs = #{},
	DontAskSyncMetaIdxs = #{},
	
	allMaxfiles = #(), 			-- List of Maxfiles (including prefab-sources)
	
	maxfileTimes = #(),  		-- Change-times for maxfiles
	mapTypes = #(),				-- Maptype for each maxfile
	
	Metafiles = #(),				-- List of metafiles that go with the maxfiles
	LoadedMetaIdxs = #{},	-- List of metafile-indices that have been loaded.
	
	maxfileRefDefs = #(), 		-- Contains lists of objects, one for each maxfile, allowing objects to be addressed by file.
	
	drawLodLevel = 0,			-- Draw scene's rsrefs with this drawable-mesh (0 for HD model)
	
	tags = #(),					-- Tags used by refs
	tagRefs = #(),				-- Refs used by tags
	
	-- These values are saved per-scene:
	showAsBoxes = false,
	showMaterials = true,
	textureMult = 0.125,	 	-- Rage textures on RSrefObjects will be reduced by this multiplier
	
	addToCont, 					-- If user has specified a particular container to add RSrefObjects to, store it here
	
	objDataStoreNodeName = "_RSref_objDataStoreNode_",	-- Standard name for object-change-data store-object
	refDataStoreNodeName = "_RSref_refDataStoreNode_", 	-- Standard name for reference-data store-object
	refDataStoreNode,													-- refDef-storage node is put here if found
	
	mapThumbDirs = #(),	-- thumbnail-directories for each maxfile are added to this list when requested by RSrefFuncs.getThumbDir
	
	swapFromNames = #(),	-- Filled by RSrefSettingFuncs.readPropSwapsList, used to swap out obsolete objects
	swapToData = #(),
	
	defaultMeshBox = (createInstance box length:2 width:2 height:2),  -- Mesh to use for missing references
	blankLodRef = 
	(
		RsRefDef meshObj:(createInstance plane length:0.5 width:0.5 lengthSegs:1 widthSegs:1)
	), -- Used by "RsRef LOD Draw" to show props where lod-distance is beyond that of its last drawableLodRef.
	
	-- These variables are set by RSrefFuncs.setUsedLists() when needed
	refObjs, refDefsUsed, refsUsedNums, refsUsedNames, refsUsedObjs, refsUsedIndex, 
	irefObjs, irefSources, irefObjLists, contLodObjs, 
	handledIrefSources = #(), irefSourceHandlers = #(),
	
	rememberNames = #(), -- Used by RSrefObject rename-lock system, storing the correct names for each object
	createNode,	 			-- Used by RSrefBrowserRoll rollout to keep track of RSrefObjects that are being created
	preMergeRefs = #(),	-- Used by filemerge callbacks to work out what RSrefs have just been merged in

	allowAttachToNode = true,
	updateSlavesActive = false, -- Stops RsRefFuncs.updateSlaves from running multiple times
	
	----------------------------------------
	-- RSREF PATHS 
	-- Folders used by rsref system:
	----------------------------------------
	dataCacheDir,			-- Maxfile object-lists are saved to this folder
	thumbsDir,				-- Prop Viewer thumbnails are found under this path
	
	PropSetsFile,			-- Path to list of Prop Sets shown in Rsref Browser
	PrefabCfgFile,			-- Path to list of Prefab folders to be shown in Rsref Browser
	SwapsCfgFile,			-- Path to list of obsolete objects, and the objects they should be swapped to
	
	NonMapFiles = #(),	-- List of non-level sourcefiles used by RSref system
	
	-- Set up folders every time struct is initialised:
	fn setupPaths = 
	(
		local mapAssetDir = if gRsIsOutSource then ("") else (RsMakeSafeSlashes ((RsConfigGetAssetsDir core:True) + "maps/"))
		PropSetsFile = mapAssetDir + "PropSets.txt"
		PrefabCfgFile = mapAssetDir + "Prefabs.txt"
		SwapsCfgFile = mapAssetDir + "PropSwap.txt"
		
		-- List of non-level sourcefiles used by RSref system:
		NonMapFiles = 
		(
			if gRsIsOutSource then #()  else 
			(
				local listXmlDir = RsConfigGetToolsConfigDir() + "/content/"
				
				#(PropSetsFile, PrefabCfgFile, SwapsCfgFile) -- (listXmlDir + "maps.xml"), (listXmlDir + "output.xml"),
			)
		)
		
		dataCacheDir = RsMakeSafeSlashes ((RsConfigGetStreamDir core:True) + "/RSref/")
		thumbsDir = RsMakeSafeSlashes ((RsConfigGetAssetsDir core:True) + "/maps/propRenders/")
	),
	
	
	-- Commands used to initialise new instance of 'RSrefDataStruct':
	fn init = 
	(
		setupPaths()
	)
)

-----------------------------------------------------------------------------
-- SETTINGS FUNCTIONS
-----------------------------------------------------------------------------
struct RSrefSettingFuncs 
(
	-- If this script is being re-evaluated:
	--	Remove any leftover callbacks or rollouts that might be hanging around
	--	Transfer existing 'RSrefDef' data over to instances using the redefined archetype
	fn RSrefScriptReset = 
	(
		::RSrefsRerun = false
		if (RSrefFuncs != undefined) do 
		(
			format "\n[Updating RSref scripts]\n\n"
			
			RSrefsRerun = true
			removeRollout RSrefAddToContRoll
			unregisterRedrawViewsCallback RSrefFuncs.startLoadRSrefData
			callbacks.removeScripts id:#RSrefs
			
			-- Replace object-refdefs with latest version of the struct:
			pushPrompt "UPDATING RSREF STRUCTS"
			
			-- Reset these RsRefData values to defaults:
			local defaultData = RSrefDataStruct()
			RsRefData.allowAttachToNode = defaultData.allowAttachToNode
			RsRefData.updateSlavesActive = defaultData.updateSlavesActive
			
			-- Get non-function struct-names:
			local defaultRef = RSrefDef()
			local propNames = for item in (getpropnames defaultRef) where (not isKindOf (getProperty defaultRef item) MAXScriptFunction) collect item
			
			local oldRefDef, newRefDef
			for n = 1 to RSrefData.refs.count do 
			(
				oldRefDef = RSrefData.refs[n]
				newRefDef = RSrefDef()
				
				for propName in propNames where (hasProperty oldRefDef propName) do 
				(
					setProperty newRefDef propName (getProperty oldRefDef propName)
				)
				
				RSrefData.refs[n] = newRefDef
			)
			
			-- Point object refdef-values at the new structs:
			local refObjs = for obj in objects where (isRSref obj includeDelegates:true) collect obj 
			for obj in refObjs where (obj.refDef != undefined) do 
			(
				obj.refDef = RSrefData.refs[obj.refDef.num]
			)
			
			-- Point maxfile-refdef lists at the new structs:
			for refList in RSrefData.maxfileRefDefs do 
			(
				for n = 1 to refList.count do 
				(
					if (refList[n].num == undefined) then 
					(
						format "Unnumbered ref! %\n" (refList[n] as string)
					)
					else 
					(
						refList[n] = RSrefData.refs[refList[n].num]
					)
				)
			)
			
			popPrompt()
			
			-- Update RsRefObject objectNames, if script wants them named differently now:
			RSrefData.rememberNames = #()
			RSrefFuncs.setObjNames refObjs prefiltered:true debugFuncSrc:"[RSrefSettingFuncs.RSrefScriptReset]"
			
			gc()
		)
	),
	-- True if non-default RSref settings are active:
	fn usingNonDefaults = 
	(
		(not RSrefAllowUpdate) or (not RSrefData.showMaterials) or RSrefData.showAsBoxes
	),
	-- Toggle RSref-system's update-functions on/off
	fn systemActive state silent:true = 
	(
		RSrefAllowUpdate = state
		
		if (isKindOf RSrefDisplayRoll RolloutClass) and RSrefDisplayRoll.open do 
		(
			RSrefDisplayRoll.btnAllowUpdates.checked = state
		)
		
		if state do 
		(
			if silent or (queryBox "Load RsRef database?" title:"Activating RsRef System") do 
			(
				local timeStart = timeStamp()
				
				RSrefFuncs.startLoadRSrefData forceStart:true debugFuncSrc:"[RSrefSettingFuncs.systemActive()]"
				
				format "RSref database activation took % seconds\n" ((timeStamp() - timeStart) / 1000.0)
			)
		)
	),
	-- Toggles "Show materials" for scene's RSrefObjects
	fn materialsActive state = 
	(
		RSrefData.showMaterials = state
		
		if (isKindOf RSrefDisplayRoll RolloutClass) and RSrefDisplayRoll.open do 
		(
			RSrefDisplayRoll.btnShowMats.checked = state
		)
		
		if not state do 
		(
			RsRefData.refs.matOutOfDate = true
		)
		
		local updateObjs = for obj in objects where (isRSrefSuperClass obj) collect obj
		updateObjs = RsGetInstGroupLeaders updateObjs
		updateObjs.rebuildMesh = true
		
		completeRedraw()
	),
	-- Toggles "Show models as boxes" for scene's RSrefObjects
	fn boxViewActive state = 
	(
		RSrefData.showAsBoxes = state

		if (isKindOf RSrefDisplayRoll RolloutClass) and RSrefDisplayRoll.open do 
		(
			RSrefDisplayRoll.btnShowAsBoxes.checked = state
		)
		
		-- Set boxmode for refs:
		local refObjs = (for obj in geometry where isRSrefSuperClass obj collect obj)
		refObjs.boxMode = state
		
		-- Redraw meshes:
		refObjs = (RsGetInstGroupLeaders refObjs)
		refObjs.rebuildMesh = true
	),
	-- Shows rollout showing current state of RSref display-settings, if non-default
	fn showRSrefSettings autoOpen:false atMouse:true = 
	(
		try (destroyDialog RSrefDisplayRoll) catch ()
		
		global RSrefSettingsAutoOpen = autoOpen
		global RSrefSettingsNonDefaults = RSrefSettingFuncs.usingNonDefaults()
		
		rollout RSrefDisplayRoll "RSref System Settings" width:260
		(
			local defaultStruct = RSrefDataStruct()
			
			local autoOpen = RSrefSettingsAutoOpen
			local nonDefaults = RSrefSettingsNonDefaults
			
			-- Highlight-colour for pressed-in default-display buttons:
			local btnColour = (colorMan.getColor #background) * 0.8 * white
			
			fn tooltipString state = 
			(
				if state then "Default: On" else "Default: Off"
			)
			
			groupBox warningBox "Please note:" offset:[1,4] width:230 height:50 visible:nonDefaults
			label warningLabel "This scene/session has non-default \nRSref settings:" align:#left width:220 height:30 pos:(warningBox.pos + [8,16]) visible:nonDefaults
			
			groupBox sceneCtrlBox "Per-Scene Settings:" pos:(warningBox.pos + [0,warningBox.height + 6]) width:230 height:54
			
			checkButton defShowMats "" width:14 height:14 pos:(sceneCtrlBox.pos + [-6,16]) checked:defaultStruct.showMaterials enabled:false highlightColor:btnColour tooltip:(tooltipString defaultStruct.showMaterials)
			checkBox btnShowMats "Show materials" pos:(defShowMats.pos + [15,0]) checked:RSrefData.showMaterials
			
			checkButton defShowAsBoxes "" width:14 height:14 pos:(defShowMats.pos + [0,18]) checked:defaultStruct.showAsBoxes enabled:false highlightColor:btnColour tooltip:(tooltipString defaultStruct.showAsBoxes)
			checkBox btnShowAsBoxes "Show models as boxes" pos:(defShowAsBoxes.pos + [15,0]) checked:RSrefData.showAsBoxes
			
			groupBox sessionCtrlBox "Per-Session Settings:" pos:(sceneCtrlBox.pos + [0,sceneCtrlBox.height + 6]) width:sceneCtrlBox.width height:54
			
			checkButton defAllowUpdates "" width:14 height:14 pos:(sessionCtrlBox.pos + [-6,16]) checked:False enabled:false highlightColor:btnColour tooltip:(tooltipString true)
			checkBox btnAllowUpdates "RsRef System Active" pos:(defAllowUpdates.pos + [15,0]) checked:RSrefAllowUpdate
			
			local p4Active = RsProjectGetPerforceIntegration()
			checkButton defFileTimeCheck "" width:14 height:14 pos:(defAllowUpdates.pos + [0,18]) checked:true enabled:false highlightColor:btnColour tooltip:(tooltipString true)
			checkBox btnFileTimeCheck "Check Perforce for changes" pos:(defFileTimeCheck.pos + [15,0]) checked:(p4Active and RSrefAllowP4Check) enabled:p4Active
			
			
			groupBox debugCtrlBox "Debug settings:" pos:(sessionCtrlBox.pos + [0,sessionCtrlBox.height + 6]) width:230 height:22 enabled:false
			checkButton btnDebugCtrlsActive "" width:10 height:10 pos:(debugCtrlBox.pos + [debugCtrlBox.width - 14,2]) checked:RSrefSettingsShowDebug
			
			--------------------------------
			-- Debug-option controls:
			--------------------------------
			checkButton defFunctionDebug "" width:14 height:14 pos:(debugCtrlBox.pos + [-6,16]) checked:false enabled:false highlightColor:btnColour tooltip:(tooltipString false)
			checkBox btnFunctionDebug "Debug Print: RSref Functions" pos:(defFunctionDebug.pos + [15,0]) checked:RSrefFunctionDebug
			
			checkButton defFileCallbackDebugPrint "" width:14 height:14 pos:(defFunctionDebug.pos + [0,18]) checked:false enabled:false highlightColor:btnColour tooltip:(tooltipString false)
			checkBox btnFileCallbackDebugPrint "Debug Print: File Callbacks" pos:(defFileCallbackDebugPrint.pos + [15,0]) checked:RSrefFileCallbackDebugPrint
			
			checkButton defSlavesDebug "" width:14 height:14 pos:(defFileCallbackDebugPrint.pos + [0,18]) checked:false enabled:false highlightColor:btnColour tooltip:(tooltipString false)
			checkBox btnSlavesDebug "Debug Print: Slave Objects" pos:(defSlavesDebug.pos + [15,0]) checked:RSrefSlavesDebug
			
			checkButton defBuildMeshDebug "" width:14 height:14 pos:(defSlavesDebug.pos + [0,18]) checked:false enabled:false highlightColor:btnColour tooltip:(tooltipString false)
			checkBox btnBuildMeshDebug "Debug Print: RSrefObject Buildmesh" pos:(defBuildMeshDebug.pos + [15,0]) checked:RSrefBuildMeshDebug
			
			checkButton defNodeCallbackDebugPrint "" width:14 height:14 pos:(defBuildMeshDebug.pos + [0,18]) checked:false enabled:false highlightColor:btnColour tooltip:(tooltipString false)
			checkBox btnNodeCallbackDebugPrint "Debug Print: Node Callbacks" pos:(defNodeCallbackDebugPrint.pos + [15,0]) checked:RSrefNodeCallbackDebugPrint
			
			checkButton defTurnOffXrefs "" width:14 height:14 pos:(defNodeCallbackDebugPrint.pos + [0,18]) checked:false enabled:false tooltip:(tooltipString false)
			checkBox btnTurnOffXrefs "Turn off Xref records on load" pos:(defTurnOffXrefs.pos + [15,0]) checked:RSrefTurnOffXrefs
			
			checkButton defDelStoresOnLoad "" width:14 height:14 pos:(defTurnOffXrefs.pos + [0,18]) checked:true enabled:false highlightColor:btnColour tooltip:(tooltipString true)
			checkBox btnDelStoresOnLoad "Delete datastore nodes on load" pos:(defDelStoresOnLoad.pos + [15,0]) checked:RSrefDelStoresOnLoad
			
			checkButton defUseFileCallbacks "" width:14 height:14 pos:(defDelStoresOnLoad.pos + [0,18]) checked:true enabled:false highlightColor:btnColour tooltip:(tooltipString true)
			checkBox btnUseFileCallbacks "RSref file-callbacks active" pos:(defUseFileCallbacks.pos + [15,0]) checked:RSrefUseFileCallbacks
			--------------------------------
			
			checkBox btnIgnoreWarns "Don't show this on open" checked:RSrefIgnoreDisplayWarnings offset:[12,12] align:#right visible:autoOpen

			fn setDebugsVisible state = 
			(
				RSrefSettingsShowDebug = state
				debugCtrlBox.enabled = state
				
				local debugCtrls
				local notDone = true
				
				-- Build list of controls between btnDebugCtrlsActive and btnIgnoreWarns:
				for ctrl in RSrefDisplayRoll.controls while notDone do 
				(
					case of 
					(
						(ctrl == btnDebugCtrlsActive):(debugCtrls = #())
						(ctrl == btnIgnoreWarns):(notDone = false)
						(isKindOf debugCtrls array):(append debugCtrls ctrl)
					)
				)
				
				debugCtrls.visible = state
				
				debugCtrlBox.height = if state then 
				(
					debugCtrls[debugCtrls.count].pos.y - debugCtrlBox.pos.y + 20
				)
				else 20
				
				btnIgnoreWarns.pos.y = debugCtrlBox.pos.y + debugCtrlBox.height + 6
				
				RSrefDisplayRoll.height = btnIgnoreWarns.pos.y
				RSrefDisplayRoll.height += (if btnIgnoreWarns.visible then 20 else 6)
				
				-- Make sure all visible controls are drawn on top of the groupboxes:
				for ctrl in RSrefDisplayRoll.controls where not (isKindOf ctrl GroupBoxControl) and ctrl.visible do 
				(
					ctrl.visible = false
					ctrl.visible = true
				)
			)

			on btnDebugCtrlsActive changed state do 
			(
				setDebugsVisible state
			)

			on btnShowMats changed state do 
			(
				RSrefSettingFuncs.materialsActive state
			)
			
			on btnShowAsBoxes changed state do 
			(
				RSrefSettingFuncs.boxViewActive state
			)
			
			on btnAllowUpdates changed state do 
			(
				RSrefSettingFuncs.systemActive state silent:false
			)
			
			on btnFileTimeCheck changed state do 
			(
				RSrefAllowP4Check = state
			)
			
			on btnTurnOffXrefs changed state do 
			(
				RSrefTurnOffXrefs
			)

			on btnDelStoresOnLoad changed state do 
			(
				RSrefDelStoresOnLoad = state
			)

			on btnUseFileCallbacks changed state do  
			(
				RSrefUseFileCallbacks = state
				RSrefFileCallbacks.setupFileCallbacks()
			)

			on btnFunctionDebug changed state do  
			(
				-- Start listener-log:
				if state then 
				(
					openLog (RsConfigGetLogDir() + "ListenerLog_RsRef.txt") outputOnly:true
				)				
				RSrefFunctionDebug = state
			)

			on btnBuildMeshDebug changed state do  
			(
				RSrefBuildMeshDebug = state
			)

			on btnFileCallbackDebugPrint changed state do  
			(
				RSrefFileCallbackDebugPrint = state
			)

			on btnNodeCallbackDebugPrint changed state do  
			(
				RSrefNodeCallbackDebugPrint = state
			)
			
			on btnIgnoreWarns changed state do 
			(
				RSrefIgnoreDisplayWarnings = state
				destroyDialog RSrefDisplayRoll
			)
			
			on RSrefDisplayRoll open do 
			(
				-- Clear value, it's only needed by initial control-setup:
				defaultStruct = undefined
				
				local shiftUp = 0
				
				if not nonDefaults do 
				(
					shiftUp = sceneCtrlBox.pos.y - warningBox.pos.y
					
					for ctrl in RSrefDisplayRoll.controls do 
					(
						ctrl.pos.y -= shiftUp
					)
				)
				
				if not autoOpen do 
				(
					shiftUp += 20
				)
				
				RSrefDisplayRoll.height -= shiftUp
				
				setDebugsVisible RSrefSettingsShowDebug
			)
		)
		
		if (not autoOpen) or ((not RSrefIgnoreDisplayWarnings) and (RSrefSettingFuncs.usingNonDefaults())) do 
		(
			local dialogPos = if atMouse then 
			(
				-- Create dialog near mousepoint:
				mouse.screenpos - [RSrefDisplayRoll.width / 2, 10]
			)
			else 
			(
				-- Create dialog near the top-left of Max window:
				[(getMAXWindowPos()).x +100, (getMAXWindowPos()).x + 100]
			)
			
			createDialog RSrefDisplayRoll pos:dialogPos
			
			-- Clean up these global variables once the dialog is open:
			globalVars.remove "RSrefSettingsAutoOpen"
			globalVars.remove "RSrefSettingsNonDefaults"
		)
	),
	
	-- Read data from 'RsRefData.SwapsCfgFile', and fill out lists:
	fn readPropSwapsList p4update:true = 
	(
		local SwapsCfgFile = RsRefData.SwapsCfgFile
		
		-- Get latest swaps-list file from Perforce:
		if p4update and RSrefAllowUpdate do 
		(
			gRsPerforce.sync SwapsCfgFile silent:true
		)
		
		local readFile = openFile SwapsCfgFile
		if (readFile == undefined) then 
		(
			return false
		)
		
		local swapFromNames = #()
		local swapToData = #()

		if readFile == undefined then
		(
			gRsULog.LogWarning ("RsRefs: " + SwapsCfgFile + ": Unable to open file, no prop swapping will occur.")
			gRsULog.validate()
			
			return swapFromNames
		)
		
		struct swapDataStruct (string = "", oldName = "", newName = "", deleteMe = false, attribchanges = #(), scaleMult)
		local RSrefObjClass = "Gta Object"

		local lineNum = 0
		local nameAndAttribs, oldNewNames, attribs, attribName, attribBits, attribIdx, attribClass, swapData
		while not eof readFile do 
		(
			lineNum += 1
			thisLine = trimLeft (readLine readFile)
			
			case of 
			(
				(thisLine == ""):()	-- Ignore empty lines, or ones with whitespace as first character
				(thisLine[1] == "#"):()	-- Ignore hashed lines
				Default:						-- Add to lists
				(
					swapData = swapDataStruct string:thisLine
					
					namesAndAttribs = filterString thisLine "[]"
					oldNewNames = for nameString in (filterString namesAndAttribs[1] ": ") collect (trimRight (trimLeft nameString))

					swapData.oldName = oldNewNames[1]
					if (oldNewNames.count == 1) then 
					(
						swapData.deleteMe = true
					)
					else 
					(
						swapData.newName = oldNewNames[2]
					)
					
					changeAttribs = namesAndAttribs[2]
					
					local attribsOk = true
					
					-- Parse the attribute-changes:
					if (changeAttribs != undefined) do 
					(
						changeAttribs = filterString changeAttribs ","
						
						for item in changeAttribs do 
						(
							attribBits = filterString item ":"
							attribName = attribBits[1]

							if attribName[1] == "$" then 
							(
								-- Parse special swap-attribute names:
								case of 
								(
									(matchPattern attribName pattern:"$scaleMult"):
									(
										swapData.scaleMult = attribBits[2] as number
										
										if (swapData.scaleMult == undefined) do 
										(
											attribsOk = false
											local msg = stringStream ""
											format "RsRefs: %: Unable to parse \"scaleMult\" value:  %" SwapsCfgFile attribBits[2] to:msg
											gRsULog.LogWarning (msg as string)
										)
									)
									Default:
									(
										attribsOk = false
										local msg = stringStream ""
										format "RsRefs: %: Unknown special attrib-name: \"%\"" SwapsCfgFile attribName to:msg
										gRsULog.LogWarning (msg as string)
									)
								)
							)
							else 
							(
								-- Get attribute-index from attrib-name:
								attribIdx = getattrindex RSrefObjClass attribName
								
								if (attribIdx == undefined) then 
								(
									attribsOk = false
									local msg = stringStream ""
									format "RsRefs: %: Attrib-name not defined for \"%\" class: \"%\"\n" SwapsCfgFile RSrefObjClass attribName to:msg
									gRsULog.LogWarning (msg as string)
								)
								else 
								(
									attribClass = case (GetAttrType RSrefObjClass attribIdx) of 
									(
										"bool":BooleanClass
										"string":String
										"int":Integer
										"float":Float
										default:undefined
									)
									
									if (attribClass == undefined) then 
									(
										attribsOk = false
										local msg = stringStream ""
										format "RsRefs: %: Attrib-type not defined for %: %\n" SwapsCfgFile attribName (GetAttrType RSrefObjClass attribIdx) to:msg
										gRsULog.LogWarning (msg as string)
									)
									else 
									(
										attribBits[1] = attribIdx
										
										local setVal = try (attribBits[2] as attribClass) catch undefined
										
										if (setVal == undefined) then 
										(
											attribsOk = false
											local msg = stringStream ""
											format "RsRefs: %: Unable to convert \"%\" to %\n" SwapsCfgFile attribBits[2] (attribClass as string) to:msg
											gRsULog.LogWarning (msg as string)
										)
										else 
										(
											attribBits[2] = setVal									
											append swapData.attribChanges attribBits
										)
									)
								)
							)
						)
					)

					-- Don't swap objects with parsing-errors in attributes:
					if attribsOk do 
					(
						append swapFromNames (toLower oldNewNames[1])
						append swapToData swapData
					)
				)
			)
		)
		close readFile
		
		RSrefData.swapFromNames = swapFromNames
		RSrefData.swapToData = swapToData
		
		gRsULog.validate()

		return swapFromNames
	)
)

--RSrefSettingFuncs.showRSrefSettings autoOpen:false atMouse:false
