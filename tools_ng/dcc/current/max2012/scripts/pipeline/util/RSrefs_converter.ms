--
-- Xref2Rsref converter
-- Rockstar Leeds
-- 04/12/2011
-- by Neal D Corbett
--
-- Replaces Xrefs with RSrefs
--

try (destroyDialog RSconvertRefsRollout) catch ()

rollout RSconvertRefsRollout "Convert Xrefs to RSrefs" width:300
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/RSref#Converting_Xrefs_to_RSrefs" align:#right offset:[6,0] color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	local btnWidth = 130
	button convSelBtn "Convert Selected Xrefs" width:btnWidth across:2
	button convAllBtn "Convert All Xrefs" width:btnWidth
	
	label selInfoLbl "xxx" width:btnWidth height:40 pos:(convSelBtn.pos + [6,24])
	label allInfoLbl "xxx" width:btnWidth height:40 pos:(convAllBtn.pos + [6,24])
	
	local propGrpListSize = 5
	listBox propGrpList "Xref Prop Groups:                |             Auto-containerable?" height:propGrpListSize offset:[0,8]
	label propGrpSubLbl "        Groups containing selected objects are in [brackets]\n                              Click list to select prop-group's objects" align:#left height:28 offset:[0,-6]
	
	group "Options"
	(
		checkbox addToContsChk "Auto-containerise converted objects\nblah" checked:true offset:[0,-4]
		checkBox fixFragsChk "Remove \"_frag_\" suffixes from objectnames" checked:true
		checkBox useOffsetsChk "Apply pivot-fix transforms" checked:true
		checkBox deleteXrefsChk "Delete Xrefs on conversion" checked:true
	)
	
	timer checkTimer "checkTimer" interval:1500 active:false
	
	local allXrefObjs
	local selXrefObjs
	local allXrefObjNames
	local selXrefObjNames
	
	local propGrps = #()
	local selPropGrps
	local selPropGrpNames
	local propGrpObjs
	local selPropGrpObjs
	
	-- Keep a track of which propgroup names have been tested, so that they don't have to be tested again:
	local checkedPropGrpNames = #()
	local propGrpUseNames = #()
	local propGrpAreaNames = #()
	local propGrpAreaPaths = #()
	local propGrpPaths = #()
	local propGrpErrors = #()	

	-- Look-up lists for finding current propGrp data on checkedPropGrpNames lists
	local allPropGrpDataNums = #()
	local selPropGrpDataNums = #()

	local autoContainerables = #{}
	local containerObjs = #()
	
	local objPropGrpIdx = getattrindex "Gta Object" "Prop Group"
	local animPropGrpIdx = getattrindex "RsStatedAnim" "Prop Group"
	local miloPropGrpIdx = getattrindex "Gta MILOTri" "Prop Group"
	
	-- Update this rollout's controls to match the current scene-objects and selection:
	fn updateAllCtrls = 
	(
		if RSconvertRefsRollout.open do 
		(
			checkTimer.active = false
			
			local objName, propGrpName, propGrpNum, propGrpIdx, checkedListIdx

			allXrefObjs = for obj in objects where (isKindOf obj XRefObject) collect obj
			selXrefObjs = #()
			
			allXrefObjNames = #()
			selXrefObjNames = #()
			local checkedListFilesUsed = #{}
			
			propGrpObjs = for n = 1 to propGrps.count collect #()
			selPropGrpObjs = for n = 1 to propGrps.count collect #()
			
			autoContainerables = #{}
			containerObjs = #()
			
			selPropGrps = #{}
			
			for obj in allXrefObjs do 
			(
				objName = toLower obj.objectName
				
				propGrpIdx = case (getattrclass obj) of 
				(
					"Gta Object":objPropGrpIdx
					"RsStatedAnim":animPropGrpIdx
					"Gta MILOTri":miloPropGrpIdx
					Default:0
				)
				
				if (propGrpIdx == 0) then 
				(
					propGrpNum = 0
				)
				else 
				(
					propGrpName = toLower (getattr obj propGrpIdx)
					propGrpNum = findItem propGrps propGrpName
					
					if (propGrpNum == 0) do 
					(
						append propGrps propGrpName
						append propGrpObjs #()
						append selPropGrpObjs #()
						
						propGrpNum = propGrps.count
						
						-- Only check map-validity once, as it can be slow if repeated many times:
						checkedListIdx = findItem checkedPropGrpNames propGrpName
						
						if (checkedListIdx == 0) do 
						(
							append checkedPropGrpNames propGrpName
							append propGrpUseNames propGrpName
							
							checkedListIdx = checkedPropGrpNames.count
							
							local errorMessage = ""
							local propMap = RsProjectContentFind propGrpName type:"map"
							
							if (propMap == undefined) do 
							(
								errorMessage = "Not valid mapname [X]"
							)
							
							local propSuffix
							if (errorMessage == "") do 
							(
								propSuffix = findString propGrpName "_Props"	
								
								if (propSuffix == undefined) then 
								(
									errorMessage = "Needs _Prop suffix [X]"
								)
								else 
								(
									propGrpPaths[checkedListIdx] = propMap.path + "/" + propGrpName + ".maxc"
								)
							)
							
							local areaMapName, areaMap
							if (errorMessage == "") do  
							(
								areaMapName = substring propGrpName 1 (propSuffix - 1)
								
								-- Change capitalisation to give created containers appropriate names:
								propGrpUseNames[checkedListIdx] = (toUpper areaMapName) + "_Props"

								areaMap = RsProjectContentFind areaMapName type:"map"
								
								if (areaMap == undefined) then 
								(
									errorMessage = "\"" + areaMapName + "\" not valid mapname [X]"
								)
								else 
								(
									propGrpAreaNames[checkedListIdx] = areaMapName
									propGrpAreaPaths[checkedListIdx] = areaMap.path + "/" + areaMapName + ".maxc"
								)
							)
							
							append propGrpErrors errorMessage
						)
					)

					checkedListFilesUsed[propGrpNum] = true
					
					append propGrpObjs[propGrpNum] obj
				)	
				
				appendIfUnique allXrefObjNames objName
				
				if obj.isSelected then 
				(
					append selXrefObjs obj
					appendIfUnique selXrefObjNames objName
					
					if (propGrpNum != 0) do 
					(
						selPropGrps[propGrpNum] = true
						append selPropGrpObjs[propGrpNum] obj
					)
				)
			)
			
			selPropGrpNames = for n = selPropGrps collect propGrps[n]

			local labelText = "" as stringstream
			format "% Xref objects in scene\n  % object-names\n  % prop-groups" allXrefObjs.count allXrefObjNames.count propGrps.count to:labelText
			allInfoLbl.text = labelText as string
			
			local labelText = "" as stringstream
			format "% Xref objects selected\n  % object-names\n  % prop-groups" selXrefObjs.count selXrefObjNames.count selPropGrps.numberSet to:labelText
			selInfoLbl.text = labelText as string
			
			allPropGrpDataNums = #()
			selPropGrpDataNums = #()
			
			local itemNum = 0
			
			local showPropGrpList = for checkedListIdx in checkedListFilesUsed collect 
			(
				append allPropGrpDataNums checkedListIdx
				
				local selGrp = selPropGrps[checkedListIdx]
				
				if selGrp do (append selPropGrpDataNums checkedListIdx)
				
				--local checkedListIdx = checkedListIndices[n]
				local itemString = (if selGrp then "[" else " ") + propGrpUseNames[checkedListIdx] + (if selGrp then "]" else " ")
				
				local containerableText = propGrpErrors[checkedListIdx]

				local containerable = (containerableText == "")
				
				-- Do final container-checks, for things that might have changed:
				if containerable do 
				(
					if not (doesFileExist propGrpAreaPaths[checkedListIdx]) do 
					(
						containerableText = "Area-container not found [X]"
						containerable = false
					)

					local fileExists = false
					local propContPath
					if (containerableText == "") do 
					(
						propContPath = propGrpPaths[checkedListIdx]
						fileExists = (doesFileExist propContPath)
						
						if fileExists and (getFileAttribute propContPath #readOnly) do 
						(
							containerableText = "Container file is read-only [X]"
							containerable = false
						)
					)
					
					local contNode
					if (containerableText == "") do 
					(
						for obj in (getNodeByName propGrps[checkedListIdx] all:true) where (isKindOf obj container) do (contNode = obj)
						
						if (contNode != undefined) do 
						(
							if not contNode.CanEditInPlace() then 
							(
								containerableText = "Container not Edit In Place? [X]"
							)
							else 
							(
								containerableText = "Will add to container [OK]"
								containerObjs[checkedListIdx] = contNode
							)
						)
					)
					
					if (containerableText == "") do 
					(
						if (doesFileExist (propContPath + ".lock")) do 
						(
							containerableText = "Container lock-file exists [X]"
							containerable = false
						)
					)
					
					if (containerableText == "") and fileExists do 
					(
						containerableText = "Will overwrite existing [OK]"
					)
					
					if (containerableText == "") do (containerableText = "Will create new file [OK]")
				)
				
				autoContainerables[checkedListIdx] = containerable

				local maxTextWidth = propGrpList.width - 15 - (getTextExtent containerableText).x
				if (checkedListFilesUsed.numberSet > propGrpListSize) do (maxTextWidth -= 18)
				
				-- Add characters until error-text is at end of line
				local textWidth, addChar
				local doDivider = true
				while ((getTextExtent itemString).x <= maxTextWidth) do 
				(
					itemString += " "
				)
				
				itemString + containerableText
			)
			propGrpList.items = showPropGrpList
			propGrpList.selection = 0

			checkTimer.active = true
		)
	)
	
	fn RSconvertXrefsToRSrefs_do convertSelected:false = 
	(
		-- Make sure that all lists are up-to-date:
		updateAllCtrls()

		-- Deactivate rollout updater stuff:
		checkTimer.active = false
		
		local xrefObjs = if convertSelected then selXrefObjs else allXrefObjs
		local xrefObjCount = xrefObjs.count
				
		for obj in xrefObjs do 
		(
			obj.name = obj.name + "_XCONVERTING_"
		)
		
		local uniqueObjNames = #()
		local objRefIndexList = #()
		local fragFixedNames = #()
		
		-- Collect list of objectnames used:
		local findNum
		for obj in xrefObjs do 
		(
			local objName = toLower obj.objectname
			
			if fixFragsChk.checked do 
			(
				-- Check for objectnames with "_frag_" at the end, and fix them:
				if (matchPattern objName pattern:"*_frag_") do 
				(
					obj.objectname = substring obj.objectname 1 (obj.objectname.count - 6)
					objName = toLower obj.objectname
					
					appendIfUnique fragFixedNames objName
				)
			)
			
			findNum = findItem uniqueObjNames objName
			if (findNum == 0) do 
			(
				append uniqueObjNames objName
				findNum = uniqueObjNames.count
			)
			append objRefIndexList findNum
		)
		
		-- Find out if any Milo portals in the scene use these xref objects:
		local miloPortals = for obj in helpers where (isKindOf obj GtaMloPortal) collect obj
		local xrefPortals = for obj in xrefObjs collect #()
		local findNum
		for portalObj in miloPortals do 
		(
			for obj in portalObj.portalObjects where ((findItem xrefObjs obj) != 0) do 
			(
				findNum = findItem xrefObjs obj
				if (findNum != 0) do 
				(
					append xrefPortals[findNum] portalObj
				)
			)
		)
		
		if (xrefObjCount != 0) do 
		(
			undo on 
			(
				local timeStart
				suspendEditing()
				convertContinue = true

				-- Temporarily turn off AutoBackup, so it won't try to do anything while we're working
				local AutoBakWas = autoBackup.enabled
				autoBackup.enabled = False
				
				-- Set all viewports to Wireframe mode, so it doesn't slow down when temporarily loading in containers:
				local viewSave = RSrefFuncs.viewportsToWire()
				
				-- Stop events from triggering data-load, as we'll trigger it at the right time
				RSrefData.dontLoadData = True
				
				-- Xref records have to be turned on if prop-groups are to be read
				progressStart "Activating Xref Records:"
				
				-- Keep copy of record-list at start of process, and its enabled-states:
				local startXrefRecordList = for n = 1 to objXRefMgr.recordCount collect (objXRefMgr.GetRecord n)
				local startRecordEnableList = for record in startXrefRecordList collect record.enabled
				
				for n = 1 to startXrefRecordList.count 
				where (not startXrefRecordList[n].enabled)
				while (progressUpdate ((100.0 * n) / startXrefRecordList.count)) do
				(
					startXrefRecordList[n].enabled = true
				)
				progressEnd()
				
				-- Get the propgroups to be converted, and their objects
				local propGrpNames = if convertSelected then selPropGrpNames else propGrps
					
				local getPropGrpObjs = if convertSelected then selPropGrpObjs else propGrpObjs
				local propGrpObjNums = for grpList in getPropGrpObjs collect (for obj in grpList collect (findItem xrefObjs obj))
					
				local propGrpDataNums = if convertSelected then selPropGrpDataNums else allPropGrpDataNums

				if convertContinue do 
				(
					local convertedCount = 0
					if convertContinue do 
					(						
						-- Turn off Xref records to free up some resources
						if deleteXrefsChk.checked do 
						(
							for n = 1 to objXRefMgr.recordCount do
							(
								(objXRefMgr.GetRecord n).enabled = false
							)
						)
						gc light:true
						
						-- Pre-load RSref data to ensure that pivot-offset data is captured:
						RSrefFuncs.loadModels uniqueObjNames filterDupes:false debugFuncSrc:"RSconvertRefsRollout.RSconvertXrefsToRSrefs_do()"

						local objRefs = for objectName in uniqueObjNames collect
						(
							RSrefFuncs.getRefDefByName objectName
						)

						-- Create the xref-replacer RSref objects, and put them in the relevant prop-group arrays
						progressStart "RSrefs: Replacing Xrefs"
						timeStart = timeStamp()

						local useOffsets = useOffsetsChk.checked
						
						local oldObj, objRefDef, offsetXform
						for grpNum = 1 to propGrpObjNums.count while convertContinue do 
						(
							local grpObjs = propGrpObjNums[grpNum]
							
							for objNum in grpObjs while convertContinue do 
							(
								convertContinue = progressUpdate ((100.0 * (convertedCount += 1)) / xrefObjCount)
								if convertContinue do 
								(
									local oldObj = xrefObjs[objNum]
									local objRefDef = objRefs[objRefIndexList[objNum]]
									
									local objPortals = xrefPortals[objNum]
									local objPortalIdxs = for portal in objPortals collect (findItem portal.portalObjects oldObj)
									
									if (isValidNode oldObj) do 
									(
										resetPivot oldObj
										
										local newObj = RSRefObject objectName:oldObj.objectname transform:(oldObj.transform) refDef:objRefDef
										if (objRefDef != undefined) do (newObj.meshObj = objRefDef.meshObj)
										
										xrefObjs[objNum] = newObj
										
										if useOffsets and (objRefDef != undefined) and not (isIdentity objRefDef.offsetXform) do 
										(
											offsetXform = objRefDef.offsetXform

											in coordSys newObj scale newObj (inverse offsetXform).scalepart
											in coordSys newObj rotate newObj -offsetXform.rotationpart
											in coordSys newObj move newObj -offsetXform.translationpart
										)
										
										select oldObj
										CopyAttrs()
										
										newObj.parent = oldObj.parent
										setGroupMember newObj (isGroupMember oldObj)
										for child in oldObj.children do (child.parent = newObj)
										oldObj.layer.addnode newObj
										
										-- Re-point portal object-lists:
										for n = 1 to objPortals.count do 
										(
											objPortals[n].portalObjects[objPortalIdxs[n]] = newObj
										)

										if deleteXrefsChk.checked do 
										(
											delete oldObj
										)
										
										select newObj
										PasteAttrs()
										
										-- Clear prop-group attribute
										setattr newObj objPropGrpIdx "undefined"
									)
								)
							)
						)
						progressEnd()
						clearSelection()

						-- Load missing data for RSrefs in scene, update all models:
--						RSrefFuncs.loadRSrefData debugFuncSrc:"RSconvertRefsRollout.RSconvertXrefsToRSrefs_do()"

						-- Create prop-containers, if option has been selected and suitable prop-groups are present:
						local grpsToContainerise = #{}
						local makeContainerFails = #{}
						
						if addToContsChk.checked do 
						(
							grpsToContainerise = autoContainerables
						)
						
						local contCount = 0
						if (grpsToContainerise.numberSet != 0) and convertContinue do 
						(
							--progressStart "Auto-containerising prop-groups:"
							for grpIdx in grpsToContainerise while (convertContinue = progressUpdate ((100.0 * (contCount += 1)) / grpsToContainerise.numberSet)) do 
							(
								local grpName = checkedPropGrpNames[grpIdx]
								local areaName = propGrpAreaNames[grpIdx]
								local areaContPath = propGrpAreaPaths[grpIdx]
								local propContPath = propGrpPaths[grpIdx]
								
								local propContObj = containerObjs[grpIdx]
								local grpObjs = for objNum in propGrpObjNums[grpIdx] collect xrefObjs[objNum]
									
								if (propContObj != undefined) then 
								(
									format "ADDING TO EXISTING CONTAINER: %\n" areaName
									
									-- Add objects to existing container:
									if (propContObj.localDefinitionFilename == "") or (propContObj.localDefinitionFilename == undefined) do 
									(
										-- Update and make helper editable, if it isn't already:
										propContObj.UpdateContainer()
										propContObj.MergeSource()
										propContObj.localDefinitionFilename = propContPath
									)
								)
								else 
								(
--									format "MAKING NEW CONTAINER: %\n" areaName

									-- Temporarily bring in area-container:
									RSnonUserMerge = RSmaxfilingNow = True
									local areaContObj = containers.createInheritedContainer areaContPath
									RSnonUserMerge = RSmaxfilingNow = False							

									if ((classOf areaContObj) != Container) then 
									(
										makeContainerFails[grpIdx] = true
									)
									else 
									(
										-- Container queries user if saving a new file; create a dummy file to get around this:
										close (createFile propContPath)

										propContObj = container name:grpName \
											displayLabel:true \
											size:(0.75 * areaContObj.size) \ -- make the props-node smaller than the area-node
											wirecolor:(white - areaContObj.wirecolor) \ -- invert the object's colour, to make it different to area-container
											pos:(areaContObj.pos + [0,0,45])\ -- move it over the area-node
											localDefinitionFilename:propContPath
										
										propContObj.SetAccessType #onlyEditInPlace
										
										delete areaContObj
										
										/*
										-- Add props to container:
										propContObj.addNodesToContent grpObjs

										-- Close the container, delete it, then recreate to make "Edit in Place" work:
										propContObj.setOpen false
										delete propContObj
										propContObj = containers.CreateInheritedContainer propContPath
										*/
									)
								)

								-- Add props to container, save it, delete helper:
								propContObj.addNodesToContent grpObjs
								
								RSrefFuncs.setObjNames grpObjs prefiltered:true
								
								propContObj.SaveContainer false
								delete propContObj

								-- Recreate the container from its newly-saved file, to keep correct helper settings:
								propContObj = containers.CreateInheritedContainer propContPath
							)
							progressEnd()
						)
					)
				)
				
				-- If any Xref records still remain, return them to their previous enabledness
				if (objXRefMgr.recordCount != 0) do 
				(
					for n = 1 to objXRefMgr.recordCount do 
					(
						local findRecord = objXRefMgr.GetRecord n
						local listRecordNum = findItem startXrefRecordList findRecord
						
						if (listRecordNum != 0) do 
						(
							if (startRecordEnableList[listRecordNum] != findRecord.enabled) do (findRecord.enabled = startRecordEnableList[listRecordNum])
						)
					)
				)
				
				-- Put RSrefObjects into instance-groups:
				RSrefFuncs.instantiateRefs()
				
				-- Remind orphan objects to redraw themselves:
				-- (containerised ones will already have been coloured)
				local RSrefObjs = for obj in xrefObjs where (isRSref obj) collect obj
				RSrefObjs.rebuildMesh = true

				autoBackup.enabled = AutoBakWas
				resumeEditing()
				
				RSrefData.dontLoadData = False

				RSrefFuncs.restoreViewports viewSave

				if (timeStart != undefined) do 
				(
					local textWords = stringstream ""
					format "% of % Xrefs replaced with RSrefs, using % reference-models.\n\n" convertedCount xrefObjCount uniqueObjNames.count  to:textWords

					if addToContsChk.checked then 
					(
						local contFailCount = makeContainerFails.numberSet
						local contCount = grpsToContainerise.count
						format "% prop-group containers successfully created\n\n" (contCount - contFailCount) to:textWords
					)
					else 
					(
						format "(prop-group containers were not generated)\n\n" to:textWords
					)
					
					if (fragFixedNames.count != 0) do 
					(
						format "_frag_ suffix removed from objectnames:\n" to:textWords
						
						for fragName in fragFixedNames do 
						(
							format "   %_frag_\n" fragName to:textWords
						)
						
						format "\n" to:textWords
					)		

					local timeString = "Process took " + ((timeStamp() - timeStart) / 1000.0) as string + " seconds." 
					format "%" timeString to:textWords
					format "%\n" timeString
					messageBox (textWords as string) title:"Replace Completed" beep:False
				)
			)
		)
		
		-- Update rollout controls:
		updateAllCtrls()
	)
	
	-- Updates controls every five seconds, if they've not been updated in that time.
	-- This will allow file-changes to be noticed.
	on checkTimer tick do 
	(
		updateAllCtrls()
	)
	
	on convSelBtn pressed do 
	(
		RSconvertXrefsToRSrefs_do convertSelected:true
	)
	
	on convAllBtn pressed do 
	(
		RSconvertXrefsToRSrefs_do convertSelected:false
	)
	
	on propGrpList selected itemNum do 
	(
		if (itemNum != 0) do 
		(
			select propGrpObjs[allPropGrpDataNums[itemNum]]
		)
	)
	
	on RSconvertRefsRollout open do 
	(
		updateAllCtrls()
	)
)

createDialog RSconvertRefsRollout
