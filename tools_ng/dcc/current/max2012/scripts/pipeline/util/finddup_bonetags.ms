--
-- File:: finddup_bonetags.ms
-- Description:: Find duplicate bone tags and bone tag hashes in a skeleton
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 17 January 2011
--

clearListener()

bone_tags = #()
bone_objs = #()
bone_hash = #()

for o in $objects do 
(	
	local tag = ( getUserProp o "tag" )
	if ( undefined == tag ) then
		continue
	local tag_hash = ( atHash16U tag )
	--format "Bone: %; Tag: % (%)\n" o.name tag tag_hash
	
	-- Find dups
	local dup_index = ( findItem bone_tags tag )
	if ( 0 != dup_index ) then
	(
		format "Duplicate tag name found: % (%) on bones % and %\n" tag tag_hash bone_objs[dup_index].name o.name 
	)
	dup_index = ( findItem bone_hash tag_hash )
	if ( 0 != dup_index ) then
	(
		format "Duplicate tag hash found: % (%) on bones % and %\n" tag tag_hash bone_objs[dup_index].name o.name 
	)
	
	-- Append
	append bone_tags tag
	append bone_objs o
	append bone_hash tag_hash
)

-- finddup_bonetags.ms
