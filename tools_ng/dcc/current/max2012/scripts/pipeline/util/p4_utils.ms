filein "pipeline/util/content.ms"
filein "rockstar/export/projectconfig.ms"

--Enables a check at the front of an export to determine if the user is on the latest or the stable.
--Since RDR3 already has a mechanism for this in the ShortcutMenu, this can be disabled. 
--Additionally, the check takes approximately 30-40 seconds to complete in external studios.
RsPerformLogLabelStatusCheck = (RsProjectConfig.GetBoolParameter "Enable Perforce Log Status Check")

struct RsPerforceUtils
(
	-----------------------------------------------------------------------------
	-- GetTimeSinceLastImageCommit
	-- descr: calculates times for all maps images in scene since last check-in
	-- return: name of map and dotnet timespan object
	-----------------------------------------------------------------------------
	fn GetTimeSinceLastImageCommit maps: =
	(
		local spanArray = #()
		try(
			if (maps == unsupplied) do 
			(
				maps = RsMapGetMapContainers()
			)
			
			local origin = dotnetobject "DateTime"  1970 1 1 0 0 0 0
			local now = (dotnetclass "DateTime").Now
			for map in maps do
			(
				local mapContentNode = RsContentTree.GetContentNode map.filename
				
				if (undefined == mapContentNode) then
				(
					gRsULog.LogError (map.filename + " does not have an entry in the content tree. Have you renamed the file?") context:map
					continue
				)
				
				local exportZipMapNode = RsContentTree.GetMapExportZipNode mapContentNode
				
				if (undefined == exportZipMapNode ) then 
				(
					gRsULog.LogError (map.name + " does not have an entry in the content tree. Have you renamed the file?") context:map
				)
				else 
				(
					local filename = (exportZipMapNode.Basename + exportZipMapNode.Extension)
					local fullFilename = RsMakeSafeSlashes (exportZipMapNode.AbsolutePath)
					if (gRsPerforce.run "fstat" #(fullFilename)) then
					(
						local result = gRsPerforce.result
						local seconds = result.records[1].item["headModTime"]
						local changedate = origin.AddSeconds (seconds as Number)
						append spanArray #(filename, now.Subtract(changedate))
					)
				)
			)
		)catch
		(
			print (getCurrentException())
		)
		return spanArray
	)
)

--
-- fn	RsP4SyncWithForceIfRequired
-- desc	This function is a wrapper around the sync function, for situations such as if
--		perforce thinks files are on the users computer but they may have deleted it 
--		manually, but not removed it from the perforce workspace
--
fn RsP4SyncWithForceIfRequired filenames silent:false showProgress:false progressTitle: = 
(
	local filesNotLocal = #()
	
	if ( filenames.count > 0 ) do
	(
		gRsPerforce.sync filenames force:false silent:silent showProgress:showProgress progressTitle:progressTitle

		fn nonLocalFiles fileList = 
		(
			local nonLocalList = for filename in fileList where not doesFileExist filename collect filename
			makeUniqueArray nonLocalList
		)
		
		-- Make list of non-synced files:
		filesNotLocal = nonLocalFiles filenames

		-- Force sync any files that don't exist:
		if (filesNotLocal.count > 0) do
		(
			gRsPerforce.sync filesNotLocal force:true silent:silent showProgress:showProgress progressTitle:progressTitle
		)
		
		-- Return list of files that still don't exist locally:
		filesNotLocal = nonLocalFiles filenames
	)
	
	return filesNotLocal
)

--
-- fn	RsP4SubmitOrDeleteChangelist
-- desc	Gets given a changelist number and checks to see if there are any files in it.  If so, then it
-- 		submits that changelist, otherwise it deletes it.  Returns true or false if it succeeded in the submit or delete
--
fn RsP4SubmitOrDeleteChangelist changelistNum = 
(
	retVal = gRsPerforce.submitOrDeleteChangelist changelistNum
)		

fn RsP4GetLocalLabelStatus dataDirectories labelType =
(
	local diff2_records_batches = #()
	
	for dataDirectory in dataDirectories do
	(
		local haveExportDataList = dataDirectory + "...#have"
		local labelExportDataList = dataDirectory + "...@" + (RsProjectGetLabelName labelType)
		local diff2_args = #("-q", haveExportDataList, labelExportDataList)

		local diff2_records_this_directory = ( gRsPerforce.run "diff2" diff2_args returnResult:true silent:true )
		append diff2_records_batches diff2_records_this_directory
	)
	
	local filesBehindLabel = 0
	local filesBeyondLabel = 0
	
	for diff2_records_batch in diff2_records_batches where undefined!=diff2_records_batch do
	(
		for diff2_record in diff2_records_batch.records do
		(
			local status = diff2_record.item["status"]
			
			if( status == "content" or status == "types" ) then -- files are different, so check the revisions
			(
				local haveRev = diff2_record.item["rev"] as Integer
				local labelRev = diff2_record.item["rev2"] as Integer
				
				if( haveRev < labelRev ) then
				(
					filesBehindLabel += 1
				)
				else if( haveRev > labelRev ) then
				(
					filesBeyondLabel += 1
				)
			)
		)
	)
	
	local status = "on label"
	if( filesBehindLabel > 0 and filesBeyondLabel > 0 ) then
	(
		status = "exotic (" + filesBehindLabel as string + " file(s) behind & " + filesBeyondLabel as string + " file(s) beyond)"
	)
	else if( filesBehindLabel > 0 ) then
	(
		status = "behind label (" + filesBehindLabel as string + " file(s))"
	)
	else if( filesBeyondLabel > 0 ) then
	(
		status = "beyond label (" + filesBeyondLabel as string + " file(s))"
	)
	
	status
)

fn RsP4GetLabeledBuildStatus =
(
	local activeBuildDirectories = #( RsProjectGetCommonDir() )
	
	-- collect the target directories of all enabled platforms
	local numTargets = RsProjectGetNumTargets()
	for targetIndex = 0 to (numTargets - 1) do
	(
		local targetName = RsProjectGetTargetKey targetIndex
		if( (RsProjectGetTargetEnabled targetName) == true ) then
		(
			append activeBuildDirectories (RsProjectGetTargetDir targetName)
		)
	)
	
	local status = RsP4GetLocalLabelStatus activeBuildDirectories "current"
	
	status
)

fn RsP4GetLabeledToolsStatus =
(
	local toolsDirectory = RsConfigGetToolsRootDir()
	local directories = #( toolsDirectory )

	local status = RsP4GetLocalLabelStatus directories "tools_current"
	
	status
)

fn RsP4LogUserLabelStatus =
(
	if ( RsProjectGetPerforceIntegration() and RsPerformLogLabelStatusCheck  ) do
	(
		pushPrompt "Getting P4 label status..."

		local buildStatus = "Build status: " + RsP4GetLabeledBuildStatus()
		local toolsStatus = "Tools status: " + RsP4GetLabeledToolsStatus()

		gRsULog.LogMessage buildStatus
		gRsULog.LogMessage toolsStatus

		popPrompt()
	)
)

fn RsP4DeleteEmptyChangelists description = 
(
	if ( RsProjectGetPerforceIntegration() ) do
	(
		if ( description != undefined ) do
		(
			gRsPerforce.deleteEmptyChangelists description
		)
	)
)

fn RsP4ExclusiveCheckedOutByWho files =
(
	local records = gRsPerforce.getFileStats files
	local user = ""
	
	if records != undefined then
	(
		user = 	for record in records
				where (record.ArrayFields.get_Item "otherOpen")[1] != undefined 
				collect
				(
					local otherOpen = (record.ArrayFields.get_Item "otherOpen")[1]
					if (classOf otherOpen) == String then otherOpen else dontCollect
				)
	)
	
	--return
	user

)
