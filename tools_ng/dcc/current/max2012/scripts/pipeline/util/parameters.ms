--
-- File:: pipeline/util/parameters.ms
-- Description:: Pipeline paramater utilities
--
-- Author:: Luke Openshaw
-- Date:: 22 January 2013
--

-- Struct for pipeline parameter 
struct RsPipelineParameters 
(
	params = undefined,	
	fn Load filename = (
		params = dotnetobject "System.Collections.Generic.Dictionary`2[System.String,System.Object]"
		local ParametersClass = dotnetclass "RSG.Base.Xml.Parameters"
		
		if ParametersClass.Load filename params false false do (
			return true
		)
		false
	)	
)
