filein "pipeline/export/peds/pedExport.ms"
filein "pipeline/export/peds/utils.ms"
filein "pipeline/util/pedBatchExporter.ms"


setQuietMode true
if ( gRsBatchJobCurrentMaxFile != undefined ) then
(
	loadMaxFile gRsBatchJobCurrentMaxFile
)
gPedBatchExporter = RsPedBatchExporter()
gPedBatchExporter.extractZips()
--setQuietMode false
