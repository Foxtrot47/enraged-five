-- 
-- Rockstar GW Functions
--   Functions for use with ViewPort Drawing Methods
--
-- 12/08/2011
-- Neal D Corbett
-- R* Leeds

------------------------------------------------------------------------------------
-- Is "pointPos" visible in the current viewport?
------------------------------------------------------------------------------------
fn RsIsPointInFrustrum pointPos screenSize:(getViewSize()) isPersp:(viewport.IsPerspView()) = 
(
	local screenPos = pointPos * viewport.getTM()
	
	if isPersp and (screenPos.z > 0) then false else 
	(	
		local screen_origin = mapScreenToView [0,0] screenPos.z screenSize
		local screen_end = mapScreenToView screenSize screenPos.z screenSize

		local world_size = screen_origin - screen_end
		local screen_aspect = screenSize / [abs world_size.x, abs world_size.y]
				
		local screen_coords = [screen_aspect.x * (screenPos.x - screen_origin.x), -screen_aspect.y * (screenPos.y - screen_origin.y)]
		
		contains (box2 [0,0] screenSize) screen_coords
	)
)
