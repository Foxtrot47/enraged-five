filein "pipeline/export/peds/pedExport.ms"
filein "pipeline/export/peds/utils.ms"
filein "pipeline/util/pedBatchExporter.ms"

print "Invoking ped exporter step..."
setQuietMode true
loadMaxFile gRsBatchJobCurrentMaxFile

gRsPerforce.run "edit" #(gRsBatchJobCurrentMaxFile) silent:true
gPedBatchExporter = RsPedBatchExporter()
gPedBatchExporter.generatePedLODS()

saveMaxFile gRsBatchJobCurrentMaxFile

--setQuietMode false