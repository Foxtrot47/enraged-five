-- Ruby script generator 
-- Rockstar North
-- 15/7/2008
-- by Luke Openshaw

------------------------------------------------------------------------------
-- Uses
------------------------------------------------------------------------------
include "rockstar/export/settings.ms"
--filein "pipeline/util/file.ms" -- loaded on startup by startup.ms
filein "pipeline/util/ruby.ms"

------------------------------------------------------------------------------
-- Functions
------------------------------------------------------------------------------

--
-- Description: Generic function for creating a resourcing script for content system
--
-- In: rubyfilename - outputfilename
-- In: projname - project name
-- In: contentname 
--
fn RBGenResourcingScript rubyFileName projname rb_script filefilters:undefined propExport:false streamedprops:false ironRubyConvertPath:unsupplied =  (
	rubyFile = createfile rubyFileName--mode:"w+"

	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/os/file'\n" to:rubyFile
	format "require 'pipeline/projectutil/data_convert'\n" to:rubyFile
	format "require 'pipeline/projectutil/data_zip'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n" to:rubyFile
	format "require 'pipeline/util/rage'\n" to:rubyFile
	format "include Pipeline\n\n" to:rubyFile

	format "begin\n" to:rubyFile
	format "\tproject = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile

	format "\tproject.load_content( )\n" to:rubyfile
	format "\tcontent_list = project.content.find_by_script( \"%\" )\n\n" rb_script to:rubyFile
	format "\tif content_list.count == 0 then throw RuntimeError.new( \"Couldn't find content for script: %\" ) end\n" rb_script to:rubyFile
	
	if (propExport) then
	(
		format "\toutput_filename = content_list[0].filename\n" to:rubyFile
		if (not streamedprops) then
		(
			format "\tinput_path = OS::Path::combine( project.cache, 'raw', content_list[0].name, '*.*' )\n" to:rubyFile
			format "\tfiles = OS::FindEx::find_files( input_path )\n" to:rubyFile
		)
		else
		(
			format "\tinput_path = OS::Path::combine( project.cache, 'raw', content_list[0].name, '*.i*' )\n" to:rubyFile
			format "\tfile_list = OS::FindEx::find_files_recurse( input_path )\n" to:rubyFile
			format "\tfiles = []\n" to:rubyFile
			format "\tfile_list.each do |ped_prop_file|\n" to:rubyFile
			format "\t\tentry = {}\n" to:rubyFile
			format "\t\tentry[:src] = ped_prop_file\n" to:rubyFile
			format "\t\tentry[:dst] = OS::Path::combine( OS::Path::get_trailing_directory(ped_prop_file) , OS::Path::get_filename(ped_prop_file) )\n" to:rubyFile
			format "\t\tfiles << entry\n" to:rubyFile
			format "\tend\n" to:rubyFile
		)
		format "\tProjectUtil::data_zip_create( output_filename, files )\n" to:rubyFile
		
		-- Leave support in for model and animation exports
		if ( ironRubyConvertPath == unsupplied ) do
		(
			format "\tProjectUtil::data_convert_file( output_filename )\n" to:rubyFile
		)
	)
	-- Leave support in for model and animation exports as they aren't using AP3 yet
	else if ( ironRubyConvertPath == unsupplied ) do
	(
		format "\tProjectUtil::data_convert_file( content_list.shift.absolute_path )\n" to:rubyFile
	)		

	format "rescue Exception => ex\n" to:rubyFile	
	format "\tputs \"Unhandled exception: #{ex.message}\"\n" to:rubyFile
	format "\tputs ex.backtrace.join( \"\\n\" )\n" to:rubyFile			
	format "\t#log.error( ex, 'Unhandled exception in projbuild' )\n" to:rubyFile
	format "\tGUI::ExceptionDialog::show_dialog( ex, 'Unhandled exception in projbuild' )\n" to:rubyFile
	format "end\n" to:rubyFile
	close rubyFile
)

--
-- Description: Generic function for creating a resourcing script for content system.  This should be used for all image generation
--
-- In: rubyfile - file object
-- In: projname - project name
-- In: imagename - output image name 
-- In: subdir - additional directory structure to append to the stream
--
fn RBGenDynamicResourcingScript rubyfile projname imagename subdir rebuild:false =  (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n" to:rubyFile
	format "include Pipeline\n\n" to:rubyFile

	format "project = Pipeline::Config.instance.projects[\"%\"]\n" projname to:rubyFile
	format "project.load_content( )\n" to:rubyFile
	format "image_name = \"%\"\n" imagename to:rubyFile

	format "ind_output_node = nil\n" to:rubyFile
	format "plat_output_list = []\n" to:rubyFile

	format "img_output_list = project.content.find_by_script(\"content.name == '\" + image_name + \"' and content.xml_type == 'image'\" )\n" to:rubyFile
	format "img_output_list.each do | img_output_item |\n" to:rubyFile
	format "\tif img_output_item.target == project.ind_target\n" to:rubyFile
	format "\t\tind_output_node = img_output_item\n" to:rubyFile
	format "\telse\n" to:rubyFile
	format "\t\tplat_output_list << img_output_item\n" to:rubyFile
	format "\tend\n" to:rubyFile
	format "end\n\n" to:rubyFile


	format "stream_dir = project.netstream\n" to:rubyFile
	format "stream_dir = Pipeline::OS::Path.combine(stream_dir, \"%\")\n" subdir to:rubyFile

	format "pack_file_list = Pipeline::OS::FindEx.find_files(stream_dir + \"/*.i*\", false)\n" to:rubyFile
	format "pack_file_list.each do | pack_file |\n" to:rubyFile
	format "\tind_output_node.add_input(Pipeline::Content::RPF.new( Pipeline::OS::Path.get_basename( pack_file ), Pipeline::OS::Path.get_directory( pack_file ), Pipeline::OS::Path.get_extension( pack_file ), project.ind_target ))\n" to:rubyFile
	format "end\n\n" to:rubyFile

--	format "\tLogSystem::instance().ulog_filename = \"%\"\n" (gRsULog.Filename()) to:rubyFile

	format "convert = Resourcing::ConvertSystem.instance()\n" to:rubyFile
	format "convert.setup( project, nil, false, %, false)\n" rebuild to:rubyFile
	format "convert.build(ind_output_node)\n" to:rubyFile
	format "plat_output_list.each do | map_output_item |\n" to:rubyFile
	format "\tconvert.build(map_output_item)\n\n" to:rubyFile

	format "end\n" to:rubyFile
)


--
-- name: RBGenDynamicRPFResourcingScript
-- desc: Generic function for creating a resourcing script for content system.  This should be used for all RPF generation
--
-- In: rubyfile - file object
-- In: projname - project name
-- In: imagename - output RPF name 
-- In: subdir - additional directory structure to append to the stream
--
fn RBGenDynamicRPFResourcingScript rubyfile projname imagename subdir rebuild:false =  (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n" to:rubyFile
	format "include Pipeline\n\n" to:rubyFile

	format "project = Pipeline::Config.instance.projects[\"%\"]\n" projname to:rubyFile
	format "project.load_content( )\n" to:rubyFile
	format "image_name = \"%\"\n" imagename to:rubyFile

	format "ind_output_node = nil\n" to:rubyFile
	format "plat_output_list = []\n" to:rubyFile
	
	format "begin\n" to:rubyFile

	format "\timg_output_list = project.content.find_by_script(\"content.name == '\" + image_name + \"' and content.xml_type == 'rpf'\" )\n" to:rubyFile
	format "\timg_output_list.each do | img_output_item |\n" to:rubyFile
	format "\t\tif img_output_item.target == project.ind_target\n" to:rubyFile
	format "\t\t\tind_output_node = img_output_item\n" to:rubyFile
	format "\t\telse\n" to:rubyFile
	format "\t\t\tplat_output_list << img_output_item\n" to:rubyFile
	format "\t\tend\n" to:rubyFile
	format "\tend\n\n" to:rubyFile


	format "\tstream_dir = project.netstream\n" to:rubyFile
	format "\tstream_dir = Pipeline::OS::Path.combine(stream_dir, \"%\")\n" subdir to:rubyFile

	format "\tpack_file_list = Pipeline::OS::FindEx.find_files(stream_dir + \"/*.i*\", false)\n" to:rubyFile
	format "\tpack_file_list.each do | pack_file |\n" to:rubyFile
	format "\t\tind_output_node.add_input(Pipeline::Content::RPF.new( Pipeline::OS::Path.get_basename( pack_file ), Pipeline::OS::Path.get_directory( pack_file ), Pipeline::OS::Path.get_extension( pack_file ), project.ind_target ))\n" to:rubyFile
	format "\tend\n\n" to:rubyFile

--	format "\tLogSystem::instance().ulog_filename = \"%\"\n" (gRsULog.Filename()) to:rubyFile

	format "\tconvert = Resourcing::ConvertSystem.instance()\n" to:rubyFile
	format "\tconvert.setup( project, nil, false, %, false)\n" rebuild to:rubyFile
	format "\tconvert.build(ind_output_node)\n" to:rubyFile
	format "\tplat_output_list.each do | map_output_item |\n" to:rubyFile
	format "\t\tconvert.build(map_output_item)\n" to:rubyFile
	format "\tend\n\n" to:rubyFile
	
	format "rescue Exception => ex\n" to:rubyFile
	format "\tGUI::ExceptionDialog.show_dialog( ex )\n" to:rubyFile
	format "end\n" to:rubyFile
)

--
-- name: RBGenDynamicZIPResourcingScript
-- desc: Generic function for creating a resourcing script for content system.  This should be used for all RPF generation
--
-- In: rubyfile - file object
-- In: projname - project name
-- In: imagename - output RPF name 
-- In: subdir - additional directory structure to append to the stream
--
fn RBGenDynamicZIPResourcingScript rubyfile projname imagename subdir rebuild:false =  (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n" to:rubyFile
	format "include Pipeline\n\n" to:rubyFile

	format "project = Pipeline::Config.instance.projects[\"%\"]\n" projname to:rubyFile
	format "project.load_content( )\n" to:rubyFile
	format "image_name = \"%\"\n" imagename to:rubyFile

	format "ind_output_node = nil\n" to:rubyFile
	format "plat_output_list = []\n" to:rubyFile
	
	format "begin\n" to:rubyFile

	format "\timg_output_list = project.content.find_by_script(\"content.name == '\" + image_name + \"' and content.xml_type == 'zip'\" )\n" to:rubyFile
	format "\timg_output_list.each do | img_output_item |\n" to:rubyFile
	format "\t\tif img_output_item.target == project.ind_target\n" to:rubyFile
	format "\t\t\tind_output_node = img_output_item\n" to:rubyFile
	format "\t\telse\n" to:rubyFile
	format "\t\t\tplat_output_list << img_output_item\n" to:rubyFile
	format "\t\tend\n" to:rubyFile
	format "\tend\n\n" to:rubyFile


	format "\tstream_dir = project.netstream\n" to:rubyFile
	format "\tstream_dir = Pipeline::OS::Path.combine(stream_dir, \"%\")\n" subdir to:rubyFile

	format "\tpack_file_list = Pipeline::OS::FindEx.find_files(stream_dir + \"/*.i*\", false)\n" to:rubyFile
	format "\tpack_file_list.each do | pack_file |\n" to:rubyFile
	format "\t\tind_output_node.add_input(Pipeline::Content::Zip.new( Pipeline::OS::Path.get_basename( pack_file ), Pipeline::OS::Path.get_directory( pack_file ), Pipeline::OS::Path.get_extension( pack_file ), project.ind_target ))\n" to:rubyFile
	format "\tend\n\n" to:rubyFile

--	format "\tLogSystem::instance().ulog_filename = \"%\"\n" (gRsULog.Filename()) to:rubyFile

	format "\tconvert = Resourcing::ConvertSystem.instance()\n" to:rubyFile
	format "\tconvert.setup( project, nil, false, %, false)\n" rebuild to:rubyFile
	format "\tconvert.build(ind_output_node)\n" to:rubyFile
	format "\tplat_output_list.each do | map_output_item |\n" to:rubyFile
	format "\t\tconvert.build(map_output_item)\n" to:rubyFile
	format "\tend\n\n" to:rubyFile
	
	format "rescue Exception => ex\n" to:rubyFile
	format "\tGUI::ExceptionDialog.show_dialog( ex )\n" to:rubyFile
	format "end\n" to:rubyFile
)

-- These should be temporary.  map content needs to be added as image inputs and only loaded
-- in resourcing scenarios
fn RBGenGtxdResourcingScriptPre rubyFile = (
	
	format "require 'pipeline/log/log'\n" to:rubyFile
	format "Pipeline::Config::instance()::logtostdout = true\n" to:rubyFile
	format "\n" to:rubyFile
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n\n" to:rubyFile
	format "require 'pipeline/content/content_map'\n\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n\n" to:rubyFile
	
	projname = RsConfigGetProjectName()
	format "project = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile
	
	format "project.load_content( )\n" to:rubyfile
	format "map_name = \"gtxd\"\n"  to:rubyfile
	
	-- Dont really need to do this, could just call RsIsGeneric but it keeps it contained
	format "stream_dir = project.netstream\n" to:rubyfile
	format "stream_dir = Pipeline::OS::Path.combine(stream_dir, 'script', map_name)\n" to:rubyFile
	format "# Create a container for the map conponent content\n" to:rubyFile
	format "component_group = Pipeline::Content::Group.new(\"map_content_group\")\n\n" to:rubyFile
	format "print(\"Resourcing...\")\n" to:rubyFile 
  
)

fn RBGenResourcingScriptPost rubyFile buildPedZip:false = (
	format "if component_group.children.size > 0\n" to:rubyFile
	format "\tconvert = Resourcing::ConvertSystem.instance()\n" to:rubyFile

--	format "\tLogSystem::instance().ulog_filename = \"%\"\n" (gRsULog.Filename()) to:rubyFile

	format "\tconvert.setup( project, nil, true, true, false)\n" to:rubyFile
	
	format "\tcomponent_group.children.each do | content_group |\n" to:rubyFile
	format "\t\tif content_group.methods.include?( 'build' )\n" to:rubyFile
	format "\t\t\tcontent_group.build()\n" to:rubyFile 

	format "\t\tend\n" to:rubyFile
	format "\tend\n" to:rubyFile
	
	format "\tconvert.build(component_group)\n" to:rubyFile 
	
	if buildPedZip do
	(
		format "\tcomponent_group.children.each do | content_group |\n" to:rubyFile
		format "\t\tif content_group.methods.include?( 'build_zip' )\n" to:rubyFile
		format "\t\t\tcontent_group.build_zip() unless content_group.is_a?(Pipeline::Content::PedProp)\n" to:rubyFile
		format "\t\tend\n" to:rubyFile
		format "\tend\n\n" to:rubyFile
	)
	
	format "end\n\n" to:rubyFile 
)

fn RBGenResourcingScriptPatch rubyFile streamed:false = (
	
	local contentGroupName
	
	-- Streamed preview exports get handled differently - exporting fragment to main preview folder
	-- but everything else to a subdirectory for that ped (like in streamedpeds.zip)
	if ( streamed ) then
	(
		contentGroupName = "preview_ped_content"
		format "% = ped_content.preview_content( project.preview )\n\n" contentGroupName to:rubyFile		
	)
	else
	(
		contentGroupName = "target_content_group"
		format "% = Pipeline::Content::Group.new(\"patch_group\")\n" contentGroupName to:rubyFile
		format "Pipeline::Resourcing::create_target_content_from_indepenent( component_group, %, project, project.preview )\n\n" contentGroupName to:rubyFile		
	)
	
	format "meta_file = Pipeline::Content::Target::from_filename( \"%#{ped_name}.pso.meta\", project.ind_target )\n" (RsConfigGetMetadataDir() + "characters/") to:rubyFile
	format "component_group.add_child(meta_file)\n\n" to:rubyFile
	
	format "branch.targets.each_pair do |platform, target|\n" to:rubyFile
	format "\tnext unless ( target.enabled )\n\n" to:rubyFile
	format "\tplatform_filename = OS::Path::combine( branch.preview, OS::Path::get_filename( meta_file.filename ) )\n" to:rubyFile
	format "\tplatform_filename = Pipeline::Resourcing::convert_independent_filename_to_platform( platform_filename, target )\n\n" to:rubyFile
	format "\ttarget_node = Pipeline::Content::Target::from_filename( platform_filename, target )\n" to:rubyFile
	format "\ttarget_node.add_input( meta_file )\n" to:rubyFile
	format "\t%.add_child( target_node )\n" contentGroupName to:rubyFile
	format "end\n" to:rubyFile
	
	format "convert.build( % )\n" contentGroupName to:rubyFile
)



fn RBGenPedResourcingScriptPre rubyFile = (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n" to:rubyFile
	format "require 'pipeline/content/content_ped'\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n" to:rubyFile
	format "require 'pipeline/resourcing/path'\n\n" to:rubyFile
	
	projname = RsConfigGetProjectName()
	format "project = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile	
	format "project.load_content( )\n" to:rubyFile
	format "branch = project.branches[project.default_branch]\n" to:rubyFile
	format "component_group = Pipeline::Content::Group.new(\"ped_content_group\")\n\n" to:rubyFile
)

-----------------------------------------------------------------------------
-- Misc Scripts
-----------------------------------------------------------------------------

-- Clip content generator
fn RBGenClipResourcingScriptPre rubyFile = (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n\n" to:rubyFile
	format "require 'pipeline/content/content_clip'\n\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n\n" to:rubyFile

	projname = RsConfigGetProjectName()
	format "project = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile

	format "project.load_content( )\n" to:rubyfile
	format "component_group = Pipeline::Content::Group.new(\"ped_content_group\")\n\n" to:rubyFile
)

-- Anim content generator
fn RBGenAnimResourcingScriptPre rubyFile = (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n\n" to:rubyFile
	format "require 'pipeline/content/content_anim'\n\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n\n" to:rubyFile

	projname = RsConfigGetProjectName()
	format "project = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile

	format "project.load_content( )\n" to:rubyfile
	format "component_group = Pipeline::Content::Group.new(\"ped_content_group\")\n\n" to:rubyFile
)

-- Cut prop content generator
fn RBGenCutPropResourcingScriptPre rubyfile = (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n\n" to:rubyFile
	format "require 'pipeline/content/content_cutscene'\n\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n\n" to:rubyFile
	
	projname = RsConfigGetProjectName()
	format "project = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile
	
	format "project.load_content( )\n" to:rubyfile
	format "component_group = Pipeline::Content::Group.new(\"cutprops_content_group\")\n\n" to:rubyFile
)

-- Cut prop content generator
fn RBGenExpressionResourcingScriptPre rubyfile = (
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n\n" to:rubyFile
	format "require 'pipeline/content/content_expression'\n\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n\n" to:rubyFile
	
	projname = RsConfigGetProjectName()
	format "project = Pipeline::Config.instance.projects[\"%\"]\n\n" projname to:rubyFile
	
	format "project.load_content( )\n" to:rubyfile
	format "component_group = Pipeline::Content::Group.new(\"expression_content_group\")\n\n" to:rubyFile
)


-----------------------------------------------------------------------------
-- Model Exporter Resourcing Scripts
-----------------------------------------------------------------------------

--
-- name: RBGenModelResourcingScriptPre
-- desc: 
--
fn RBGenModelResourcingScript rubyFile outdir objname isFrag = (

	projname = RsConfigGetProjectName()
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/config/project'\n" to:rubyFile
	format "require 'pipeline/content/content_core'\n" to:rubyFile
	format "require 'pipeline/gui/exception_dialog'\n" to:rubyFile
	format "require 'pipeline/os/file'\n" to:rubyFile
	format "require 'pipeline/projectutil/data_convert'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n" to:rubyFile
	format "require 'pipeline/util/string'\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n" to:rubyFile
	format "include Pipeline\n\n" to:rubyFile

	format "begin\n" to:rubyFile
	format "\tproject = Pipeline::Config.instance.projects[\"%\"]\n" projname to:rubyFile
	format "\tproject.load_config( )\n" to:rubyFile
	format "\tbranch = project.branches[project.default_branch]\n\n" to:rubyFile
	
	format "\tmodelname = '%'\n" objname to:rubyFile
	format "\tstream_dir = '%'\n" outdir to:rubyFile
	
	if ( isFrag ) then
	(
		format "\tift = Content::RPF.new( modelname, stream_dir, 'ift', branch.ind_target )\n" to:rubyFile
	)
	else
	(
		format "\tidr = Content::RPF.new( modelname, stream_dir, 'idr', branch.ind_target )\n" to:rubyFile
	)
	
	format "\tall_files = OS::FindEx::find_files( OS::Path::combine( '%/%', '*.*' ) )\n" outdir objname to:rubyFile
	format "\tall_files.each do |file|\n" to:rubyFile
	format "\t\tputs \"Adding: #{file}\"\n" to:rubyFile
	
	if ( isFrag ) then
	(
		format "\t\tift.inputs << Content::File::from_filename( file )\n" to:rubyFile
	)
	else
	(
		format "\t\tidr.inputs << Content::File::from_filename( file )\n" to:rubyFile
	)
	
	format "\tend\n" to:rubyFile
	
	format "component_group = Pipeline::Content::Group.new(\"map_content_group\")\n\n" to:rubyFile
	if ( isFrag ) then
		format "\tcomponent_group.add_child(ift)\n" to:rubyFile
	else
		format "\tcomponent_group.add_child(idr)\n" to:rubyFile

	RBGenResourcingScriptPost rubyfile

	format "rescue Exception => ex\n" to:rubyFile
	format "\tPipeline::GUI::ExceptionDialog.show_dialog( ex )\n" to:rubyFile
	format "\texit( 1 )\n" to:rubyFile
	format "end\n" to:rubyFile
)
