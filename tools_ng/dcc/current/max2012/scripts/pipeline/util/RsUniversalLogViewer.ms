-----------------------------------------------------------------------
-- globals
-----------------------------------------------------------------------

global gKeepWindowInBg = false
global gUniversalLogWindow = undefined

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

fn openULogViewer modal:false allowContinue:true logDirectory:"" =
(
	--( Create the UI
	gc()
	
	-- Get the list of files we should be opening.
	filenames = dotNetObject "System.Collections.Generic.List`1[System.String]"			
	if (logDirectory != "") then
	(
		local logFileList = getFiles (logDirectory + "\*.ulog")
		for filename in logFileList do filenames.Add(filename)
	)
	else
	(
		filenames.Add (RsConfigGetLogDir() + "unilog.ulog")
	)
	print filenames
	
	-- Create the new window.
	showContinue = modal and allowContinue
	gUniversalLogWindow = gEditorApp.CreateUniversalLogWindow filenames modal showContinue
	
	-- Used to maintain focus on the .Net Window
	-- Because we have a new window we have to tell Max when we want to collect keyboard strokes and return control when we are done
	fn disable_accelerators =
	(
		enableAccelerators = false
	)
	--Setting the focus of the window to capture all Keyboard events while the window is "Active" or focused
	dotNet.addEventHandler gUniversalLogWindow "Activated" disable_accelerators
	
	fn enable_accelerators =
	(
		enableAccelerators = true
	)
	--Make sure we return control when we are done
	dotNet.addEventHandler gUniversalLogWindow "Deactivated" enable_accelerators
	
	-- Hook up an event handler for whenever the selection changes.
	fn selectionChanged obj args = 
	(
		-- Turn off timer if user clicks something:
		gUniversalLogWindow.DisableCountdown()
		
		if args.SelectedItems == undefined then return
		local selNodes = #()
		
		slist = args.SelectedItems		
		for msg in slist where (msg != undefined) do
		(
			if msg.MessageContext != undefined then
			(
				if classof msg.MessageContext == string and matchpattern msg.MessageContext pattern:"*maxscript*" then 	
				(
					local tupels = filterstring msg.MessageContext "|"
					for tupel in tupels do
					(
						local tokens = filterstring tupel ":"
						case tokens[1] of
						(
							"obj":
							(
								local nameList = filterString tokens[2] ","
								
								for objName in nameList do 
								(
									local theNode = getnodebyname objName
									if isValidNode theNode do 
									(
										if selNodes == undefined then
										(
											selNodes = #(theNode)
										)
										else
										(
											append selNodes theNode
										)
									)
								)
							)
							"maxscript":
							(
								print (execute tokens[2])
							)
						)
					)	
				)
				else
				(
					for cn in filterstring msg.MessageContext "," do
					(
						local theNode = getnodebyname cn
						if isValidNode theNode then
						(
							if selNodes == undefined then
							(
								selNodes = #(theNode)
							)
							else
							(
								append selNodes theNode
							)
						)
					)
				)
			)
		)
		
		clearSelection()
		if not selNodes == undefined then
		(
			select selNodes
		)
	)
	dotNet.addEventHandler gUniversalLogWindow "SelectedItemsChanged" selectionChanged
	
	--New to Max 2010, we use this to make sure Maxscript Garbage collection does not destory our Functions and events before we are done with them
	dotNet.setLifetimeControl gUniversalLogWindow #dotnet
	
	--Show what we got.
	if modal then
	(
		gUniversalLogWindow.showDialog()
		gUniversalLogWindow.Focus()
	)
	else
	(
		gUniversalLogWindow.show()
	)
	
	result = (gUniversalLogWindow.DialogResult == true)
	return result
)

