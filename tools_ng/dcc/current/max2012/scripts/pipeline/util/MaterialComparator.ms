-- Material Comparison tool wrapper
-- @Author Gunnar Droege

struct RsMaterialComparator
(
	compObj = dotnetobject "RSG.MaxUtils.MaterialComparatorView",
	model = compObj.Model,
	
	fn Show =
	(
		compObj.ShowWindow()
	),
	
	fn AddRageShader mtl mtlId:0 = 
	(
		local rowDict = dotnetobject "RSG.MaxUtils.MaxDictionary"
		rowDict.add "preset" (RstGetShaderName mtl)
		rowDict.add "Mtl ID" mtlId
		
		for i=1 to (RstGetVariableCount mtl) do
		(
			local shaderName = (RstGetVariableName mtl i)
			local shaderVal = (RstGetVariable mtl i)
			shaderVal = shaderVal as string
			rowDict.add shaderName shaderVal
		)
		
		model.AddRow (mtl.name as string) rowDict
	),
	
	fn AddMtl mtl mtlId:0 = 
	(
		case (classof mtl) of
		(
			Rage_Shader:
			(
				AddRageShader mtl mtlId:mtlId
			)
			Multimaterial:
			(
				for mi=1 to mtl.materiallist.count do
				(
					AddMtl mtl.materiallist[mi] mtlId:mtl.materialIdList[mi]
				)
			)
		)
	),
	
	fn SetFromSelection =
	(
		model.clear()
		if compObj.IsVisible then
		(
			for s in selection do
			(
				if undefined!=s.material do
					AddMtl s.material
			)
		)
	)
	
	
)

global gRsMtlComp = RsMaterialComparator()
callbacks.removeScripts id:#mtlComparator
callbacks.addScript #selectionSetChanged "gRsMtlComp.SetFromSelection()" id:#mtlComparator