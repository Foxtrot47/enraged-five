--
-- RSref Data Storage
-- Neal D Corbett (R* Leeds) 4/2014
--
-- Objects and functions for dealing with data-storage
--

global gRsRefStore = undefined

-----------------------------------------------------------------------------
-- General data-storage node:
--	Data is stored as an indexed array, which takes up less space, and 
--	can be quickly output to/retrieved from a textfile if required.
--	(I hope to replace this with 'RsGenericDataNode' at some point)
-----------------------------------------------------------------------------
plugin helper RSrefDataStoreNode
	name:"_RSrefDataStoreNode_"
	category:"RS Objects"
	classID:#(0x5f4d9171, 0x3501fe72)
	extends:dummy
	invisible:True
	version:RSrefVersion
(
	-- Set to true by file-open event
	-- Allows merge-event to quickly tell if a node has just been merged in
	local loadedOnOpen = false
	
	parameters main
	(
		-- Tracks the version of the parameter-saving function last used
		saveVersion			type:#integer		animatable:false default:1
		
		-- The names of items that have parameters
		itemNames				type:#stringTab	animatable:false	tabSizeVariable:true

		-- The parameters that the objects share
		paramNames			type:#stringTab	animatable:false	tabSizeVariable:true

		-- The unique values that the saved objects' parameters have taken
		paramStrings			type:#stringTab	animatable:false	tabSizeVariable:true

		-- Index-numbers indicating which paramString value each object/parameter uses.
		-- Stored in this order: #(obj1param1, obj2param1, ... ,obj1param2,obj2param2, etc)
		paramStringIndices	type:#intTab		animatable:false	tabSizeVariable:true
		
		-- Used to store meshes and materials
		meshObjs		type:#maxObjectTab	animatable:false	tabSizeVariable:true
		materials			type:#materialTab		animatable:false	tabSizeVariable:true
		dataIndex		type:#intTab				animatable:false	tabSizeVariable:true
		
		-- Used to store per-scene data (instead of per-object)
		sceneParamNames	type:#stringTab	animatable:false	tabSizeVariable:true
		sceneParamStrings	type:#stringTab	animatable:false	tabSizeVariable:true
	)
	
	local idxFragProxy = getattrindex "Gta Object" "Is FragProxy"
	
	-- Sets values to default empty arrays, just in case this node is being re-used
	fn resetValues = 
	(
		itemNames = #()
		paramNames = #()
		paramStrings = #()
		paramStringIndices = #()
		dataIndex = #()
		meshObjs = #()
		materials = #()
		sceneParamNames = #()
		sceneParamStrings = #()
	)

	-- Retrieves value-array for ParamName from store
	-- will convert to Type if specified, and allows for specifying a range of item-numbers to retrieve data for.
	fn getParamValues ParamName Type:undefined FromItem:1 ToItem: = 
	(
		if (ToItem == unsupplied) do (ToItem = itemNames.count)
		
		--local ToItemNum = if (ToItem == undefined) then (itemNames.count) else ToItem
		local ParamNum = findItem paramNames (toLower paramName)
		local outArray
		
		if (ParamNum == 0) then (undefined) 
		else 
		(
			local IndexStart = (ParamNum - 1) * itemNames.count
			local ConvertType = (Type != undefined)
			local readVal
			
			outArray = for n = (IndexStart + FromItem) to (IndexStart + ToItem) collect 
			(
				readVal = paramStrings[paramStringIndices[n]]
				
				if ConvertType then 
				(
					case Type of 
					(
						array:(execute readVal)
						bitarray:(execute readVal)
						matrix3:(execute readVal)
						color:(execute readVal)
						default:(readVal as Type)
					)
				) else 
				(
					readVal
				)
			)
		)
		
		return outArray
	)

	-- Encodes the data to be stored by this object
	-- newParamNames is an array of parameter-names, newParamData is an array of value-arrays
	-- The object's ItemNames are stored seperately, as they don't need any special encoding.
	-- By default, data is compressed by only including unique strings once, but this can be slow for large data-sets - "compressed" controls this feature
	fn storeData newParamNames newParamData compressed:true = 
	(
		local newItems = #()
		local newItemIndices = #()
		
		if (newParamData.count != 0) do 
		(
			local valueIndex, valueArray, valueString, isArray
			for paramNum = 1 to newParamNames.count do 
			(
				--pushPrompt ("Storing strings (" + (paramNum as string) + "/" + (newParamNames.count as string) + ")")
				
				valueArray = newParamData[paramNum]
				isArray = (isKindOf valueArray[1] Array)

				for value in valueArray do 
				(
					local value = if isArray then (with PrintAllElements on (value as string)) else value
					
					valueIndex = if compressed then (findItem newItems value) else 0
					
					if (valueIndex == 0) do 
					(
						append newItems value
						valueIndex = newItems.count
					)
					append newItemIndices valueIndex
				)
				
				--popPrompt()
			)
		)

		-- Put the collected object-data into this node's params:
		paramNames = for paramName in newparamNames collect (toLower paramName)
		paramStrings = for item in newItems collect (item as string)
		paramStringIndices = newItemIndices
	)

	-- Saves last-changed times for editable objects in scene
	fn StoreObjChangeData = 
	(
		resetValues()
		saveVersion = RSrefUpdateCacheVersion
		
		fn isStoreable obj = 
		(
			local baseObj = obj.baseObject
			
			(isProperty baseObj "mesh") and -- Saves for any meshed object
			(not isRSrefSuperClass obj) and (not isRefObj obj) and (not isInternalRef obj) -- All ref-type objects are ignored
			(
				local attrClass = getAttrClass obj
				(attrClass != undefined) and 
				(
					-- Don't include "Dont Export" objects...
					local attrIdx = getAttrIndex attrClass "Dont Export"
					
					(attrIdx == undefined) or (not (getAttr obj attrIdx)) or 
					((attrClass == "Gta Object") and (getAttr obj idxFragProxy)) or -- ...unless the object is a Frag Proxy
					(
						-- ...or is a ComboLodder source:
						local childObjs = #()
						RsSceneLink.GetChildren ::LinkType_CombinerMesh obj &childObjs
						(childObjs.count != 0)
					)
				)
			)
		)
		
		-- Save change-data for objects that have meshes and are not tagged as "Dont Export"
		local saveObjs = #() 
		for obj in geometry do 
		(
			if (isStoreable obj) then 
			(
				append saveObjs obj
			)
			else 
			(
				-- Handy Objects are often linked under a helper-object:
				for childObj in obj.children where (isStoreable childObj) do 
				(
					append saveObjs childObj
				)
			)
		)

		itemNames = for Obj in saveObjs collect Obj.name
			
		if RSrefFunctionDebug do 
		(
			for item in itemNames do 
			(
				format "\t\t%\n" item
			)
		)
		
		local newParamNames = #()
		local paramArrays = #()
		
		-- Save change-times
		local ObjHandles = for Obj in SaveObjs collect (GetHandleByAnim Obj)
		local ExportValue, paramArray

		append newParamNames "ObjMeshChanged"
		append paramArrays #()
		paramArray = paramArrays[paramArrays.count]
		for ObjHandle in ObjHandles do 
		(
			ExportValue = RSgetBigArrayVal RSNodeMeshChangeTimes ObjHandle
			if (ExportValue == undefined) or (ExportValue == 0) do 
			(
				-- If no change-timestamp has been set, choose a random number
				-- This way, un-timestamped versions of the same object aren't thought to be the same version
				ExportValue = random 1 100000
				RSsetBigArrayVal RSNodeMeshChangeTimes ObjHandle ExportValue
			)
			append paramArray (ExportValue as string)
		)

		append newParamNames "ObjMatChanged"
		append paramArrays #()
		paramArray = paramArrays[paramArrays.count]
		for ObjHandle in ObjHandles do 
		(
			ExportValue = RSgetBigArrayVal RSNodeMatChangeTimes ObjHandle
			if (ExportValue == undefined) or (ExportValue == 0) do 
			(
				ExportValue = random 1 100000
				RSsetBigArrayVal RSNodeMatChangeTimes ObjHandle ExportValue
			)
			append paramArray (ExportValue as string)
		)

		storeData newParamNames paramArrays
		OK
	)
	
	-- Saves RSref database, so we don't have to load it again if files haven't changed
	fn StoreRSrefObjData = 
	(
		pushPrompt "Storing RSref database cache..."

		resetValues()
		
		if RSrefFunctionDebug do (format "[Storing RSref data:]\n")
		saveVersion = RSrefVersion

		local newItemNames = for ref in RSrefData.refs collect ref.name
		
		local refDefs = RSrefData.refs
		local filesCount = RSrefData.maxfiles.count
		local newParamNames = #("maxfileNum","meshTime","matTime","badXform","offsetXform")

		-- Builds an array of arrays of all values for each stored parameter.
		local paramArrays = for paramName in newParamNames collect  
		(
			for refDef in refDefs collect 
			(
				getProperty refDef paramName
			)
		)
		
		-- Store the object-data:
		itemNames = newItemNames
		storeData newParamNames paramArrays compressed:false
		
		-- Save loaded model/material data to RSrefStoreNode
		RSrefFuncs.setUsedLists debugFuncSrc:"RSrefDataStoreNode.storeRSrefObjData()"
		
		local savedataIndex = RSrefData.refsUsedNums as array
		local saveMeshObjs = for n in RSrefData.refsUsedNums collect refDefs[n].meshObj
		local saveMaterials = for n in RSrefData.refsUsedNums collect refDefs[n].material
			
		-- Add alt-meshes to the end of list:
		for refDef in RSrefData.refDefsUsed where (refDef != undefined) and (refDef.altMeshObj != undefined) do 
		(
			append savedataIndex refDef.num
			append saveMeshObjs refDef.altMeshObj
		)
		
		dataIndex = savedataIndex
		meshObjs = saveMeshObjs
		materials = saveMaterials
		
		-- Store the per-scene data:
		sceneParamNames = #("maxfiles","maxFileTimes","textureMult","showAsBoxes","showMaterials")
		local newSceneParamStrings = #()
		with printAllElements on 
		(
			append newSceneParamStrings ((for filename in RSrefData.maxfiles collect (toLower (getFilenameFile filename))) as string)
			append newSceneParamStrings ((for n = 1 to RSrefData.maxfiles.count collect RSrefData.maxFileTimes[n]) as string)
			append newSceneParamStrings (RSrefData.textureMult as string)
			append newSceneParamStrings (RSrefData.showAsBoxes as string)
			append newSceneParamStrings (RSrefData.showMaterials as string)
		)
		sceneParamStrings = newSceneParamStrings
		
		popPrompt()
		
		if RSrefFunctionDebug do (format "[RSref data stored]\n")
		OK
	)
	
	-- Loads last-changed times for editable objects in scene
	fn RetrieveTimes = 
	(
		RSNodeMeshChangeTimes = #()
		RSNodeMatChangeTimes = #()
		
		local StoredMeshChangeTimes = GetParamValues "ObjMeshChanged" Type:Integer
		local StoredMatChangeTimes = GetParamValues "ObjMatChanged" Type:Integer
		
		for n = 1 to itemNames.count do 
		(
			local ObjNode = getNodeByName itemNames[n]
			if (isValidNode ObjNode) do 
			(
				local ObjHandle = getHandleByAnim ObjNode
				RSsetBigArrayVal RSNodeMeshChangeTimes ObjHandle StoredMeshChangeTimes[n]
				RSsetBigArrayVal RSNodeMatChangeTimes ObjHandle StoredMatChangeTimes[n]
			)
		)
		return OK
	)
	
	-- Saves node's data to cache textfile
	fn SaveCacheFile sourceFilePath optionsHash:0 = 
	(
		local saveFilename, sourceTime
		
		if sourceFilePath == "" then 
		(
			saveFilename = "x:/temp.txt" 
			sourceTime = ""
		)
		else 
		(
			makeDir RSrefData.dataCacheDir
			saveFilename = RSrefFuncs.getCacheFileName sourceFilePath
			sourceTime = if (doesFileExist sourceFilePath) then (getFileModDate sourceFilePath) else ""
		)

		if RSrefFunctionDebug do (format "Saving cache-data for: % to: %\n" sourceFilePath saveFilename)
		if (doesFileExist saveFilename) do (setFileAttribute saveFilename #readOnly false)
		
		local saveStream = stringStream ""
		format "%\n%\n%\n%\n" RSrefUpdateCacheFileVersion (sourceTime) (optionsHash) (sourceFilePath) to:saveStream
		
		format "\nnumItems=%\n" itemNames.count to:saveStream
		
		for itemName in itemNames do (
			format "%\n" itemName to:saveStream
		)
		
		format "\nnumParams=%\n" paramNames.count to:saveStream
		
		for paramName in paramNames do (
			format "%\n" paramName to:saveStream
		)
		
		format "\nnumStrings=%\n" paramStrings.count to:saveStream
		
		for paramString in paramStrings do (
			format "%\n" paramString to:saveStream
		)
		
		for item in paramStringIndices do (format "% " item to:saveStream)
		
		local fstream = createFile saveFilename
		format "%" ( saveStream as string ) to:fstream
		close fstream
		
		return saveFilename
	)
	
	-- Loads cached data from textfile
	fn LoadCacheFile sourceFilePath optionsHash:0 = 
	(
		local loadFilename = RSrefFuncs.getCacheFileName sourceFilePath
		local readSuccess = False
		
		-- Load cached object-list if file exists:
		if (doesFileExist sourceFilePath) and (doesFileExist loadFilename) do try 
		(
			local readStream = (replace_CRLF_with_LF ((dotNetClass "System.IO.File").ReadAllText loadFilename)) as stringStream
			
			local readSuccess = False
			local removeOldFile = False
			
			case of 
			(
				-- First line of cache-file is the version-number it was saved under; fails if different to current version
				((ReadLine readStream) as Integer != (RSrefUpdateCacheFileVersion)):(removeOldFile = true)
				-- Second line of cache-file is the source-maxfile's last-modified date; fails if it doesn't match the maxfile
				((ReadLine readStream) != (GetFileModDate sourceFilePath)):(removeOldFile = true)		
				-- Third line of cache-file will be hash of options used to generate save-file, so any changes will invalidate it:
				((ReadLine readStream) as Integer != optionsHash):(removeOldFile = true)	
				-- If those checks pass, read the file:
				default: 
				(
					-- Read item names
					skipToString readStream "numItems="
					local numItems = (readline readStream) as integer
					local fileItemNames = for i = 1 to numItems collect
					(
						readLine readStream
					)

					-- Read param names
					skipToString readStream "numParams="
					local numParams = (readline readStream) as integer
					local fileParamNames = for i = 1 to numParams collect  
					(
						readLine readStream
					)

					-- Read string names
					skipToString readStream "numStrings="
					local numStrings = (readline readStream) as integer
					local fileParamStrings = for i = 1 to numStrings collect  
					(
						readLine readStream
					)
					
					-- Assign to paramblock
					itemNames = fileItemNames
					paramNames = fileParamNames
					paramStrings = fileParamStrings
					
					paramStringIndices = if (itemNames.count == 0) or (paramNames.count == 0) then #() else 
					(
						local stringLine = readLine readStream
						for item in (filterString stringLine " ") collect (item as integer)
					)
					
					readSuccess = True
				)
			)
			
			if removeOldFile do 
			(
				deleteFile loadFilename
			)
		)
		catch 
		(
			Format "WARNING: Failed to read Rsref cache-file: %\n  %\n" loadFilename (GetCurrentException())
			
			readSuccess = False
		)
		
		if RSrefFunctionDebug do 
			(format "loadCacheFile: % : %\n" loadFilename readSuccess)
		
		return readSuccess
	)

	on update do
	(
		if (version > RSrefVersion) do (RSrefScriptsOutOfDate = True)
	)

	rollout DebugRoll "DEBUG" 
	(
		button StoreTimesBtn "Store change-data to node"
		button RetrieveTimesBtn "Retrieve change-data from node"
		button StoreRsRefBtn "Store RSref-data to node"
		button printToWindowLbl "Print data to new window"
		button saveToFileBtn "Save data to file"
		button loadFromFileBtn "Load data from file"
		local textLblHeight = 800
		label textLbl "" width:130 height:textLblHeight
		
		fn TimeConvert TimeInt = 
		(
			local Seconds = TimeInt / 1000.0
			local Minutes = Seconds / 60.0 
			local Hours = floor (Minutes / 60) as integer
			Minutes = floor (mod Minutes 60) as integer
			Seconds = floor (mod Seconds 60) as integer
			Hours as string + ":" + (formattedPrint Minutes format:"02i") + ":" + (formattedPrint Seconds format:"02i") + "\n"
		)

		fn writeText stringLimit:120 = 
		(
			local output = stringstream ""
			local maxLines = textLblHeight / 13
			
			format "dataIndex: %\n" (dataIndex as string) to:output
			format "meshObjs:% materials:%\n\n" meshObjs.count materials.count to:output
			
			local dontLimitLength = (stringLimit == -1)
			
			for n = 1 to sceneParamNames.count do 
			(
				format "%:\n" sceneParamNames[n] to:output
				format "  %" (substring sceneParamStrings[n] 1 stringLimit) to:output
				if (not dontLimitLength) and (sceneParamStrings[n].count > stringLimit) do (format " [...]" to:output)
				format "\n\n" to:output
			)
			
			local lineNum = 3
			local paramArrays = for paramName in paramNames collect (getParamValues paramName)
			for itemNum = 1 to itemNames.count while dontLimitLength or (lineNum < textLblHeight) do 
			(
				format "%:\n" itemNames[itemNum] to:output
				lineNum += 1
				for paramNum = 1 to paramNames.count do 
				(
					format "  %:  %\n" paramNames[paramNum] paramArrays[paramNum][itemNum] to:output
					lineNum += 1
				)
				format "\n" to:output
				lineNum += 1
			)
			
			output as string
		)

		fn UpdateDebugText = 
		(
			local debugText = writeText stringLimit:120
			TextLbl.text = debugText
		)
		
		on printToWindowLbl pressed do 
		(
			RScreateTextWindow title:"RSrefDataStoreNode contents" text:(writeText stringLimit:600)
		)
		on StoreTimesBtn pressed do (storeObjChangeData(); UpdateDebugText())
		on StoreRsRefBtn pressed do 
		(
			local timeStart = timeStamp()
			storeRSrefObjData()
			format "RSref data-store took % seconds\n" ((timeStamp() - timeStart) / 1000.0)
			UpdateDebugText()
		)
		on RetrieveTimesBtn pressed do (RetrieveTimes())
		on saveToFileBtn pressed do 
		(
			print (saveCacheFile (maxfilepath + maxfilename))
		)
		on loadFromFileBtn pressed do 
		(
			print (loadCacheFile (maxfilepath + maxfilename))
			UpdateDebugText()
		)
		
		on DebugRoll open do (UpdateDebugText())
	)
) -- END: RSrefDataStoreNode

struct RsRefStore
(
	--------------------------------------------------------------------------------------------------------------------
	-- isStoreNode:
	--	Is this an Rsref-system data-store node?
	--------------------------------------------------------------------------------------------------------------------
	fn isStoreNode Obj = 
	(
		isKindOf Obj RSrefDataStoreNode
	),
	
	-----------------------------------------------------------------------------
	-- Returns list of 'RSrefDataStoreNode' objects:
	-----------------------------------------------------------------------------
	fn GetStoreNodes = 
	(
		for Obj in Helpers where (isStoreNode Obj) collect Obj
	),
	
	--------------------------------------------------------------------------------------------------------------------
	-- CreateStoreNode:
	--	Create data-store node
	--	(done as separate function to make it easier to swap to new object-class later)
	--------------------------------------------------------------------------------------------------------------------
	fn CreateStoreNode ObjName = 
	(
		RSrefDataStoreNode Name:ObjName isHidden:True isFrozen:True
	),
	
	--------------------------------------------------------------------------------------------------------------------
	-- CreateChangeStoreObj:
	--	Creates an RSrefDataStoreNode object if appropriate for current scene.
	--	Object-list and change-tracker data is saved to this node.
	--------------------------------------------------------------------------------------------------------------------
	fn createChangeStoreObj = 
	(
		-- Remove any existing change-store object: --
		delete (getNodeByName RSrefData.objDataStoreNodeName all:true)

		-- We only want to store data if scene contains meshes:
		local doStore = False
		for obj in geometry where (isProperty obj "mesh") while (not doStore) do (doStore = True)
		
		-- Does scene define props or prefabs?
		local msg = stringStream ""
		if (not doStore) then 
		(
			format "Scene contains no meshes" to:msg
		)
		else
		(
			-- Save object-list nodes for prefab-files:
			if (RSrefFuncs.isPrefabFile (maxfilepath + maxfilename)) then 
			(
				format "This is a Prefab Meshes scene" to:msg
			)
			else 
			(
				if (RsContentTree.contentTreeHelper == undefined) then 
				(
					format "Project's content-tree is undefined!" to:msg
					doStore = False
				)
				else 
				(					
					-- Is this maxfile on project's export-list?
					local mapName = (getFilenameFile maxFilename)
					local mapFilename = RsMakeSafeSlashes (maxFilepath + maxFilename)

					-- Look through content-tree for this filename:
					local maxNodes = RsContentTree.contentTreeHelper.GetAllMapExportNodes()
					local mapType = undefined
					
					for nodeIdx = 1 to maxNodes.count while (foundNode == undefined) do
					(
						local thisNode = maxNodes[nodeIdx]
						
						-- Node is a match if name and path match:
						if (matchPattern thisNode.Name pattern:mapName) and (matchPattern (RsMakeSafeSlashes thisNode.AbsolutePath) pattern:mapFilename) do 
						(
							mapType = RsContentTree.GetMapType thisNode
						)
					)
					
					if (mapType == undefined) then 
					(
						msg = stringStream ""
						format "Maxfile '%' not found in project's content-tree" mapName to:msg
						doStore = False
					)
					else 
					(
						-- Store change-data if maxfile is flagged as being reffable:
						if (RsRefFuncs.isRefMapType mapType) then 
						(
							format "Maxfile '%' maptype (%) is allowed to be referenced" mapName mapType to:msg
						)
						else 
						(
							format "Maxfile '%' maptype (%) is not allowed to be referenced" mapName mapType to:msg
							doStore = False
						)
					)
				)
			)
		) 
		
		-- Create object-list nodes for files that define props or prefabs:
		if (doStore) then 
		(
			pushPrompt "Storing object-change data..."
			
			if RSrefFunctionDebug do 
			(
				format "\t%\n\tSaving object-list data to node %\n" (msg as string) RSrefData.objDataStoreNodeName
			)

			local HideState = hideByCategory.helpers -- Creating new helper will turn this option off, if it's turned on
			local StoreNode = CreateStoreNode RSrefData.objDataStoreNodeName
			hideByCategory.helpers = HideState
			
			-- Put new object in default layer
			local defaultLayer = layerManager.getLayer 0
			defaultLayer.addnode StoreNode

			StoreNode.storeObjChangeData()
			popPrompt()
			
			return StoreNode
		)
		else 
		(
			if RSrefFunctionDebug do 
			(
				format "\tSkipping object-change store-node creation: %\n" (msg as string)
			)
			
			return undefined
		)
		
		return StoreNode
	),
	
	--------------------------------------------------------------------------------------------------------------------
	-- CreateRSrefStoreObj:
	--	Create RSref data-store node, including mesh and material data
	--------------------------------------------------------------------------------------------------------------------
	fn createRSrefStoreObj = 
	(
		delete (getNodeByName RSrefData.refDataStoreNodeName all:true)
		
		RSrefFuncs.setUsedLists debugFuncSrc:"RsRefStore.createRSrefStoreObj"
		local refDefs = RSrefData.refs
		local refDefsUsed = RSrefData.refDefsUsed
		local refsUsedObjs = RSrefData.refsUsedObjs
		
		-- Save RefDef data if there are RSref objects in the scene
		local saveRefDefData = (refDefsUsed.count != 0)
		
		-- If no objects were found, but RefDef data exists, then this scene may have RSrefs in unloaded containers.
		-- If there are containers, save RefDef data, saving us from reloading data when a container is loaded back in.
		if not saveRefDefData and not RSrefData.notLoaded do 
		(
			for obj in helpers while (not saveRefDefData) 
			where (isKindOf obj container) and (obj.unloaded)
			do (saveRefDefData = true)
		)
		
		if saveRefDefData then 
		(
			if RSrefFunctionDebug do (format "\tFile uses RSref objects.\n\tSaving ref-data cache to node %\n" RSrefData.refDataStoreNodeName)
			
			-- Copy relevant model-change timestamps to all RSrefObjects, so that they can remember their stamps without a data-store, such as when merged in
			-- Use hash of maxfile change-time if change-time is unknown
			local objRefDef
			for n = 1 to refDefsUsed.count do 
			(
				objRefDef = refDefsUsed[n]
				if (objRefDef != undefined) do 
				(
					local objInsts = RsGetInstGroupLeaders refsUsedObjs[n]
					
					for obj in objInsts do 
					(
						if (obj.meshTime != objRefDef.meshTime) do (obj.meshTime = objRefDef.meshTime)
						if (obj.matTime != objRefDef.matTime) do (obj.matTime = objRefDef.matTime)
					)
				)
			)
			
			local HideState = hideByCategory.helpers -- Creating new helper will turn this option off, if it's turned on
			local RSrefStoreNode = CreateStoreNode RSrefData.refDataStoreNodeName
			hideByCategory.helpers = HideState
			
			-- Put new object in default layer
			local defaultLayer = layerManager.getLayer 0
			defaultLayer.addnode RSrefStoreNode

			RSrefStoreNode.storeRSrefObjData()
			
			return RSrefStoreNode
		) else 
		(
			return undefined
		)
	)
)
gRsRefStore = RsRefStore()
