-- Settings
-- Rockstar North
-- 2/2/2005

-- allows settings to be saved in a generic way

-------------------------------------------------------------
-- returns the name of the file to save the setting information
-------------------------------------------------------------
fn RsSettingGetIniFile = 
(
	local LocalAppPath = (dotNetClass "System.Environment").GetEnvironmentVariable "LocalAppData"
	return (LocalAppPath + "\\Autodesk\\3dsMax\\Rs3dsMax.ini")
)

-------------------------------------------------------------
-- write a setting to the ini file
-------------------------------------------------------------
fn RsSettingWrite group variableName val = 
(
	setINISetting (RsSettingGetIniFile()) group variableName (val as string)
)

-------------------------------------------------------------
-- Restore a stored screen-position:
-------------------------------------------------------------
fn RsSettingReadScreenPos group variableName windowSize: defaultVal:[100,100] = 
(
	local retval = getINISetting (RsSettingGetIniFile()) group variableName
	retval = execute retval
	
	if not isKindOf retval point2 then 
	(
		retval = defaultVal
	)
	
	local hasWindowSize = (windowSize != unsupplied)
	
	-- Don't allow window-pos to be put outside screenspace:
	for n = 1 to 2 do 
	(
		case of 
		(
			(retval[n] < 0):(retval[n] = 0)
			(hasWindowSize and ((retval[n] + windowSize[n]) > sysInfo.DesktopSize[n])):
			(
				retval[n] = (sysInfo.DesktopSize[n] - windowSize[n])
			)
			(not hasWindowSize and (retval[n] > sysInfo.DesktopSize[n])):
			(
				retval[n] = sysInfo.DesktopSize[n]
			)
		)	
	)
	
	return retval
)

-------------------------------------------------------------
-- read a setting from the ini file. if no value is found
-- the default value is returned
-------------------------------------------------------------
fn RsSettingRead group variableName defaultVal = (

	retval = getINISetting (RsSettingGetIniFile()) group variableName
	
	if retval == "" or retval == "undefined" then (
		retval = defaultVal
	)
	
	return retval
)

-------------------------------------------------------------
-- read a setting from the ini file and force it to a boolean
-------------------------------------------------------------
fn RsSettingsReadBoolean group variableName defaultVal = (
	return (RSSettingRead group variableName defaultVal) as BooleanClass
)

-------------------------------------------------------------
-- read a setting from the ini file and force it to a string
-------------------------------------------------------------
fn RsSettingsReadString group variableName defaultVal = (
	return (RSSettingRead group variableName defaultVal)
)

-------------------------------------------------------------
-- read a setting from the ini file and force it to an integer
-------------------------------------------------------------
fn RsSettingsReadInteger group variableName defaultVal = (
	return (RSSettingRead group variableName defaultVal) as integer
)

-------------------------------------------------------------
-- read a generic non-string value:
-------------------------------------------------------------
fn RsSettingsReadValue group variableName defaultVal = 
(
	local getVal = RSSettingRead group variableName defaultVal
	
	if isKindOf getVal String do 
	(
		local stream = stringStream getVal
		getVal = readValue stream
	)
	
	return getVal
)