--
-- File:: pipeline/util/constraints.ms
-- Description:: Rotation and translation constraint helper script.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 11 November 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
-- None

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global RsConstraintAxisX = 1
global RsConstraintAxisY = 2
global RsConstraintAxisZ = 3

-- pipeline/util/constraints.ms
