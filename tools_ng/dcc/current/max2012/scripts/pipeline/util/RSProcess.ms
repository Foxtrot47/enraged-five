
struct RsProcess
(
	processObj = undefined,
	processID = 0,
	exitCode = -1,
	
	fn RunProcess filename args silent:false waitForExit:true workingDir:undefined =
	(
		processObj = dotNetObject "System.Diagnostics.Process"
		windowStyle = dotNetClass "System.Diagnostics.ProcessWindowStyle"
		
		if (processObj != undefined) then
		(
			processObj.StartInfo.FileName = filename
			processObj.StartInfo.Arguments = args
			if (workingDir != undefined) then
			(
				processObj.StartInfo.WorkingDirectory = workingDir
			)
			
			if silent then
			(
				processObj.StartInfo.WindowStyle = windowStyle.Hidden
				processObj.StartInfo.CreateNoWindow = true
			)
			
			processObj.Start()
			processID = processObj.Id
			
			if waitForExit then
			(
				processObj.WaitForExit()
				exitCode = processObj.ExitCode
			)
		)
	)
)