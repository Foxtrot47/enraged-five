--
-- File:: pipeline/util/p4_containers.ms
-- Description:: Perforce map container support.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 27 July 2010
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/assert.ms"
--filein "pipeline/util/file.ms" -- loaded on startup by startup.ms

struct RsP4Container 
(
	CONTAINER_STATUS_EDITABLE      = "Editable",
	CONTAINER_STATUS_EDIT_IN_PLACE = "Editing in place",

	--
	-- name: contGetLatest
	-- desc: Get latest maxc and independent data files for a max container.
	--
	fn contGetLatest c mapFilename independentFiles = 
	(
		local edited = false
		
		-- Switch off edit in place so it can be fetched.
		if ( ( not RsIsFileReadOnly mapFilename ) and
			 ( c.GetStatusString() == CONTAINER_STATUS_EDIT_IN_PLACE ) ) then
		(
			-- Containers: Edit Container
			actionMan.executeAction -1172021248 "13"  
			edited = true
		)
		
		local fileList = #(mapFilename)
		join fileList independentFiles
		
		local result = 	gRsPerforce.sync fileList
		
		if ( result and edited ) then
		(
			-- Containers: Edit Container
			actionMan.executeAction -1172021248 "13"  
		)
		result
	),


	--
	-- name: contCheckout
	-- desc: Checkout maxc and independent files (optional) for a max container.
	--
	fn contCheckout c mapFilename independentFiles DoCheckoutSync:True DoAssets:True = 
	(
		-- Switch off edit in place so it can be checked out.
		if ( ( not RsIsFileReadOnly mapFilename ) and
			 ( c.GetStatusString() == CONTAINER_STATUS_EDIT_IN_PLACE ) ) then
		(
			-- Containers: Edit Container
			actionMan.executeAction -1172021248 "13"  
		)
		
		local fileList = #(mapFilename)
		
		-- Add export-assets to list, if requested:
		if DoAssets do 
		(
			join fileList independentFiles
		)
		
		-- Sync files before checkout, if requested:
		if DoCheckoutSync do 
		(
			gRsPerforce.sync fileList
		)
		
		local exclusiveOuts = gRsPerforce.isCheckedoutExclusive fileList
		
		if (exclusiveOuts.numberSet != 0) do 
		(
			local warnMsg = stringStream ""
			format "Can't check out these files, they're checked out by another user:\n" to:warnMsg

			for fileNum in exclusiveOuts do 
			(
				format "%\n" fileList[fileNum] to:warnMsg
			)
			
			warnMsg = warnMsg as string
			format "%" warnMsg
			
			messageBox warnMsg title:"Error: Exclusive checkouts"
		)
		
		if (exclusiveOuts.numberSet != fileList.count) do 
		(
			local fileList = for fileNum in -exclusiveOuts collect fileList[fileNum]
			
			gRsPerforce.add_or_edit fileList exclusive:true
		)
	),

	--
	-- name: contCheckin 
	-- desc:
	--
	fn contCheckin c mapFilename independentFiles DoAssets:True = 
	(
		-- Switch off edit in place so it can be checked out.
		if ( ( not RsIsFileReadOnly mapFilename ) and
			 ( c.GetStatusString() == CONTAINER_STATUS_EDIT_IN_PLACE ) ) then
		(
			-- Containers: Edit Container
			actionMan.executeAction -1172021248 "13"  
		)
		
		-- Get changelist description.
		global RsP4DescriptionStatus = false
		global RsP4Description = ""
		if ( true ) then
		(
			rollout RsChangelistDescription "Enter changelist description:"
			(
				edittext 	txtDescription 
				button 		btnOK "OK" across:2 width:100
				button		btnCancel "Cancel" width:100
					
				on btnCancel pressed do (
					RsP4DescriptionStatus = false
					DestroyDialog RsChangelistDescription
				)
				
				on btnOK pressed do (
					RsP4DescriptionStatus = true
					RsP4Description = txtDescription.text
					DestroyDialog RsChangelistDescription
				)
			)
		)

		CreateDialog RsChangelistDescription width:600 modal:true

		local files = #(RsMakeSafeSlashes( mapFilename ))
		if DoAssets do 
		(
			for file in independentFiles do
			(
				append files (RsMakeSafeSlashes file)
			)
		)
		
		for f in files do
		(
			if not gRsPerforce.sync f then
			(
				messagebox ("Couldn't get latest of \""+f as string+"\"")
				return false
			)
			if not gRsPerforce.edit f then
			(
				messagebox ("Couldn't check out \""+f as string+"\"")
				return false
			)
		)
		
		if ( RsP4DescriptionStatus ) then
		(
			local args = join #("-d", RsP4Description) files
			format "arguments %\n" args
			return gRsPerforce.run "submit" args
		)
		else
			return false
	),

	-- Runs function func on containers containing objs:
	fn runFunc func objs = 
	(
		objs = case (classOf objs) of 
		(
			UndefinedClass:(#())
			ObjectSet:(objs as array)
			Array:(objs)
			Default:(#(objs))
		)
		
		local contObjs = #()
		for obj in objs do 
		(
			local objCont = RsGetObjContainer obj
			
			if objCont != undefined do 
			(
				appendIfUnique contObjs objCont
			)
		)

		if (contObjs.count < 1) then
		(
			messageBox "No containers or containerised objects are selected" title:"Invalid selection"
		)
		else 
		(
			local DoCheckoutSync = True
			local DoAssets = True
			
			-- Ask user about optional stuff: (with cancel available)
			case Func of 
			(
				ContCheckout:
				(
					local QueryVal = RsQueryBoxMultiBtn "Sync files before checkout?" Title:"Query: Sync files?" TimeOut:-1
					if (QueryVal == 3) then (return False) else (DoCheckoutSync = (QueryVal == 1))
						
					local QueryVal = RsQueryBoxMultiBtn "Checkout map export-asset data too?" Title:"Query: Checkout assets?" TimeOut:-1
					if (QueryVal == 3) then (return False) else (DoAssets = (QueryVal == 1))
				)
				ContCheckin:
				(
					local QueryVal = RsQueryBoxMultiBtn "Checkin map export-asset data too?" Title:"Query: Checkin assets?" TimeOut:-1
					if (QueryVal == 3) then (return False) else (DoAssets = (QueryVal == 1))
				)
			)
			
			for ContObj in ContObjs do 
			(
				-- Get Maxfile content-node for container:
				local MapName = ContObj.Name
				local MaxNode = (RsContentTree.FindSourceFilenodeFromBasename MapName)
				
				if (MaxNode != undefined) do 
				(
					local MapFilename = MaxNode.AbsolutePath
					
					-- Collect zip/xml filenames:
					local ZipNode = RsContentTree.GetMapExportZipNode MaxNode
					local XmlNode = RsContentTree.GetMapExportSceneXMLNode ZipNode
					
					local ExportFilenames = for ThisNode in #(ZipNode, XmlNode) where (ThisNode != undefined) collect 
					(
						ThisNode.AbsolutePath
					)
					
					-- Sync/Checkout/Checkin:
					Func ContObj MapFilename ExportFilenames DoCheckoutSync:DoCheckoutSync DoAssets:DoAssets
				)
			)
		)
	),
	
	fn getLatestFor objs = runFunc contGetLatest objs,
	fn checkinFor objs = runFunc contCheckin objs,
	fn checkoutFor objs = runFunc contCheckout objs
)

global gRsP4Container = RsP4Container()

-- pipeline/util/p4_containers.ms
