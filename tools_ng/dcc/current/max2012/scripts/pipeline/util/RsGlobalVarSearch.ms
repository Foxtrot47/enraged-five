rollout RsFindGlobalvar "Find global"
(
	edittext edittxt "search:" across:2
	checkbox checkIgnoreCase "ignore case" checked:true
	listbox listResults "results:"
	edittext editinfo "info:" height:50
	button buttReplace "copy and close" readonly:true
	local results = #()
	
	fn updateinfo = 
	(
		local info = stringstream ""
		if listResults.selection>0 then
		(
			apropos results[listResults.selection] to:info
			editinfo.text = info
		)
		else
			editinfo.text = ""
	)
	
	fn updateResults val = 
	(
		results = #()
		for var in (globalVars.gather()) where (matchPattern var pattern:("*"+val+"*") ignoreCase:(checkIgnoreCase.checked)) do (append results var)
		listResults.items = results
		updateinfo()
	)

	on edittxt changed val do
	(
		updateResults val
	)
	on RsFindGlobalvar open do
	(
		setfocus edittxt
	)
	on listResults doubleClicked arg do
	(
		setclipboardText results[arg]
		DestroyDialog RsFindGlobalvar
	)
	on buttReplace pressed do
	(
		setclipboardText results[listResults.selection]
		DestroyDialog RsFindGlobalvar
	)
	on listResults selected arg do
	(
		updateinfo()
	)
	on checkIgnoreCase changed val do 
	(
		updateResults edittxt.text
	)
)

try (DestroyDialog RsFindGlobalvar) catch()
CreateDialog RsFindGlobalvar 600 280