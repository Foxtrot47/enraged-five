-- Rockstar Fragment Tuning
-- Rockstar North
-- 15/5/2006
-- by Luke Openshaw
-- rewritten by Gunnar Droege

-- Read and write fragment params
-- dependent on structures in RsFragmentTunungGlobals.ms in startup folder
-- note: children are bound objects, groups are the actual breaking bits and might have more chidren

filein "rockstar/export/settings.ms"
filein "string.ms"

try CloseRolloutFloater RsFragTuner catch()
global RsFragTuner = newRolloutFloater "Rockstar Fragment Tuner" 300 1100 50 126

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- global UI code
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
rollout RsBasicEditRollout "Quick edit"
(
	label lblBullt "bullet      ped       car     truck" align:#right
	label comboLabel "Only damaged by:" across:2 align:#left
	slider sliderDamagedBy "" range:[0,16,8] type:#integer --items:#("bullet", "ped", "car")
	label checkboxLabel "Disappear when dead" across:2 align:#left
	checkbox checkDisappear ""
	
	on sliderDamagedBy changed val do
	(
		local threshold = 1
		for i=1 to val do threshold = threshold * 2
		if gFragTuner!=undefined then 
		(
			gFragTuner.setGroupValueDirect gFragTuner.currFragRoot "minDamageForce" threshold setAllChildren:true
		)
	)
	on checkDisappear changed val do
	(
		if gFragTuner!=undefined then 
		(
			gFragTuner.setGroupValueDirect gFragTuner.currFragRoot "disappearsWhenDead" val setAllChildren:true
		)
	)
)

fn addControlAndHandler creator type varName readableName paramString groupName attributeValue childindex:undefined cmbBoxItems:undefined customCallback:undefined = 
(
	creator.addControl #label (varName+"_label") readableName paramStr:("align:#left across:3")
	local checkBoxName = (varName+"_expChck")
	local params = (paramString+" width:60 offset:(point2 55 0)")
	local codeString = "gFragTuner.setGroupValue @"+groupName+"@ @"+attributeValue.attrName+"@ val undefined; "
	if undefined!=childindex then codeString = "gFragTuner.setChildValue @"+groupName+"@ @"+(childindex as string)+"@ @"+attributeValue.attrName+"@ val; "
	if undefined!=cmbBoxItems then
	(
		codeString = "gFragTuner.setGroupValue @"+groupName+"@ @"+attributeValue.attrName+"@ "+varName+".selected undefined; "
		append params (" items:"+cmbBoxItems as string)
	)
	if undefined!=customCallback then
	(
		append codeString ("\n::" + customCallback + " " + (creator.name) as string + " "+varName+".selected @" + groupName + "@; ")
	)
	creator.addControl type varName "" paramStr:params
	creator.addControl #checkbox checkBoxName "" paramStr:("width:10 align:#right checked:"+attributeValue.attrExportFlag as string)
--	print (varName +", "+attributeValue.attrExportFlag as string)
	local handlerType = #changed
	if type==#dropdownlist then
		handlerType = #selected

	append codeString (checkBoxName+".checked = true; ")
	creator.addHandler varName handlerType paramStr:"val " codeStr:(codeString)
	creator.addHandler checkBoxName #changed paramStr:"val " codeStr:("gFragTuner.setGroupExportFlag @"+groupName+"@ @"+attributeValue.attrName+"@ val")
)

fn ResetRollouts groupName = (
	for r in RsFragTuner.rollouts do
	(
		try(
			if 	findString r.name "RsTree"==undefined and 
				findString r.name "RsFrag"==undefined and
				findString r.name "RsButton"==undefined then
			(
--				format "Remove rollout: % [%]\n" r r.name
				removeRollout r RsFragTuner
			)
		)catch()
	)
	
	local rolloutsToAdd = #()
	
	if undefined!=gFragTuner.currHirarchyErrors then
	(
-- 		local stream = stringstream ""
-- 		theClass = dotNetClass "System.Drawing.Color" 
-- 		showMethods theClass to:stream
-- 		messagebox stream
-- 		theClass.fromARGB 255 0 0
		rollout ErrorRollout "Hierarchy errors:"
		(
			dotNetControl f1 "System.Windows.Forms.Textbox" text:gFragTuner.currHirarchyErrors readonly:true multiline:true height:60
			--edittext lbl "" text:gFragTuner.currHirarchyErrors readOnly:true height:32
			
			on ErrorRollout open do
			(
				local sb = dotnetclass "System.Windows.Forms.ScrollBars"
				f1.scrollbars = sb.both
			)
		)
		append rolloutsToAdd ErrorRollout
	)
	
	-- search for groupNode
	local rootGroup = gFragTuner.currFragGroup
-- 	if undefined!=RsButtonRoll then
-- 	(
-- 		if groupName==undefined then
-- 			::RsButtonRoll.btnCreate.enabled = true
-- 		else
-- 			::RsButtonRoll.btnCreate.enabled = false
-- 	)

	if groupName==undefined then
	(
		return false
	)
	if gFragEditMode==#basic then
	(
		append rolloutsToAdd RsBasicEditRollout
		return true
	)
	
	child = rolloutCreator "RsDynFragRoll" "Root fragment values"
	child.begin()
	local index=0
	for c in gFragTuner.currFragRoot.groupValues do
	(
		local varName = c.attrName --"var"+index as string
		local readableName = gRsFragAttribLUT.getValue c.attrName
		if (undefined==readableName) then continue
		case c.attrType of 
		(
			#typeScalar: 
			(
				addControlAndHandler child #spinner varName readableName (" type:#float range:[-100,100000,"+c.attrValue as string+"]") "fragments" c
			)
			#typeArray:
			(
				arrayVarName = varname+"a0"
				addControlAndHandler child #label arrayVarName readableName "" "" c
				arrayVarName = varname+"a1"
				child.addControl #spinner arrayVarName "" paramStr:(" across:3 type:#float range:[-100,100000,"+c.attrValue[1] as string+"]")
				child.addHandler arrayVarName #changed paramStr:"val " codeStr:("gFragTuner.setGroupValue @fragments@ @"+c.attrName+"@ val 1")
				arrayVarName = varname+"a2"
				child.addControl #spinner arrayVarName "" paramStr:(" type:#float range:[-100,100000,"+c.attrValue[2] as string+"]")
				child.addHandler arrayVarName #changed paramStr:"val " codeStr:("gFragTuner.setGroupValue @fragments@ @"+c.attrName+"@ val 2")
				arrayVarName = varname+"a3"
				child.addControl #spinner arrayVarName "" paramStr:(" type:#float range:[-100,100000,"+c.attrValue[3] as string+"]")
				child.addHandler arrayVarName #changed paramStr:"val " codeStr:("gFragTuner.setGroupValue @fragments@ @"+c.attrName+"@ val 3")
			)
			#typeBool:
			(
				addControlAndHandler child #checkbox varName readableName ("checked:"+c.attrValue as string) "fragments" c
			)
			#typeString: 
			(
				local dataFunc = gRsFragAttribLUT.getDataFunc c.attrName
				local data = dataFunc()
				local selIndex = 1
				if undefined!=data then
				(
					local foundInData = findItem data c.attrValue
					if 0!=foundInData then selIndex = foundInData
				)
				local customCallback = gRsFragAttribLUT.getCallback c.attrName
				addControlAndHandler child #dropdownlist varName readableName (" align:#right selection:"+selIndex as string)  "fragments" c cmbBoxItems:data customCallback:customCallback
			)
		)
		index+=1
	)
	child.end()
	append rolloutsToAdd child.def

	if groupName==undefined or groupName==gFragTuner.currFragRoot.groupName or gFragEditMode==#basic then return true
		
	child = rolloutCreator "RsGroupRoll" "Current group values"
	child.begin()
	local index=0
	local isInScaleGroup = false
	for c in rootGroup.groupValues do
	(
		local varName = c.attrName --"var"+index as string
		local readableName = gRsFragAttribLUT.getValue c.attrName
		if (undefined==readableName) then continue
		if matchPattern c.attrName pattern:"*scale" then
		(
			if not isInScaleGroup then
			(
				isInScaleGroup = true
				child.addText "group @Damage Scales@ (" filter:true
			)
		)
		else if isInScaleGroup then
		(
			child.addText ")"
			isInScaleGroup = false
		)
		case c.attrType of 
		(
			#typeScalar: 
			(
				addControlAndHandler child #spinner varName readableName (" type:#float range:[-100,100000,"+c.attrValue as string+"]") groupName c
			)
			#typeArray:
			(
				arrayVarName = varname+"a0"
				addControlAndHandler child #label arrayVarName readableName "" "" c
				arrayVarName = varname+"a1"
				child.addControl #spinner arrayVarName "" paramStr:(" across:3 type:#float range:[-100,100000,"+c.attrValue[1] as string+"]")
				child.addHandler arrayVarName #changed paramStr:"val " codeStr:("gFragTuner.setGroupValue @"+groupName+"@ @"+c.attrName+"@ val 1")
				arrayVarName = varname+"a2"
				child.addControl #spinner arrayVarName "" paramStr:(" type:#float range:[-100,100000,"+c.attrValue[2] as string+"]")
				child.addHandler arrayVarName #changed paramStr:"val " codeStr:("gFragTuner.setGroupValue @"+groupName+"@ @"+c.attrName+"@ val 2")
				arrayVarName = varname+"a3"
				child.addControl #spinner arrayVarName "" paramStr:(" type:#float range:[-100,100000,"+c.attrValue[3] as string+"]")
				child.addHandler arrayVarName #changed paramStr:"val " codeStr:("gFragTuner.setGroupValue @"+groupName+"@ @"+c.attrName+"@ val 3")
			)
			#typeBool:
			(
				addControlAndHandler child #checkbox varName readableName ("checked:"+c.attrValue as string) groupName c
			)
			#typeString: 
			(
				local dataFunc = gRsFragAttribLUT.getDataFunc c.attrName
				local data = dataFunc()
				local selIndex = 1
				if undefined!=data then
				(
					local foundInData = findItem data c.attrValue
					if 0!=foundInData then selIndex = foundInData
				)
				local customCallback = gRsFragAttribLUT.getCallback c.attrName
				addControlAndHandler child #dropdownlist varName readableName (" align:#right selection:"+selIndex as string)  groupName c cmbBoxItems:data customCallback:customCallback
			)
		)
		index+=1
	)
	if isInScaleGroup then
	(
		child.addText ")"
		isInScaleGroup = false
	)
	child.end()
	append rolloutsToAdd child.def
	
	local index=1
	for c in rootGroup.childArray do
	(
		local readableName = gRsFragAttribLUT.getValue c.childValues[1].attrName
		if (undefined==readableName) then continue
		child = rolloutCreator ("Child"+(index as string)) ("Current group collision child "+(index as string))
		-- Start creating the rollout
		child.begin()
		addControlAndHandler child #spinner "myEdit1" readableName (" type:#float range:[-100,100000,"+c.childValues[1].attrValue as string+"]") groupName c.childValues[1] childindex:index
		
		readableName = gRsFragAttribLUT.getValue c.childValues[2].attrName
		if (undefined==readableName) then continue
		addControlAndHandler child #spinner "myEdit2" readableName (" type:#float range:[-100,100000,"+c.childValues[2].attrValue as string+"]") groupName c.childValues[2] childindex:index
		child.end()
		index+=1
		append rolloutsToAdd child.def
	)
	
	for r in rolloutsToAdd do
		addRollout r RsFragTuner
)


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- rollouts
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

rollout RsFragRoll "Fragment"
(
	hyperlink hyperHelp "HELP?" color:(color 100 100 255) address:"https://devstar.rockstargames.com/wiki/index.php/Fragment_tuner" align:#right
	editText edtFragName "Frag Name:"
	
	on RsFragRoll open do (
		callbacks.removeScripts id:#loadFrag
		local script = 
			"gFragTuner.RefreshUI()"
		callbacks.addscript #selectionSetChanged script id:#loadFrag
	)
	on RsFragRoll close do (
		callbacks.removeScripts id:#loadFrag
	)
	
	fn setFragName newName = 
	(
		edtFragName.text = newName
	)
)

rollout RsButtonRoll "Update"
(
	local values = #(#basic, #default, #verbose)
	local buttwidth = 68
	local buttwidth2 = 120
	local buttwidth3 = 85
	radiobuttons radioMode "Editing mode" labels:values columns:3 default:(findItem values gFragEditMode)
	button btnCreate "Create/Reset Tune" across:2 width:buttwidth2
	button btnUpdate "Save Tune" width:buttwidth2
	group "Advanced"
	(
		checkbox checkReloadFile "Reload tune on select" checked:true
		checkbox checkPrvntCollSel "Prevent collision selection" checked:true
		label lblTickButts "Value export/copy selection" align:#left
		button btnTickNone "Uncheck all" across:4 width:buttwidth
		button btnTickAll "Check all" width:buttwidth
		button btnTickDefault "Default" width:buttwidth
		button btnTickInvert "Inverse" width:buttwidth
		button btnCopy "Copy hierarchy" across:3 width:buttwidth3
		checkbox checkCpyChilds "curr group" offset:[0,4] checked:(not gCpyChildren)
		button btnPaste "Paste hierarchy" width:buttwidth3
	)
	
	on btnCreate pressed do (
		local tunePath = gFragTuner.GetTunePath()
		if doesFileExist tunePath then
		(
			if queryBox "Tune file already exists! This will delete it and create a new definition from scratch, which you will have to save." then
			(
				if not gRsPerforce.edit tunePath then
					messagebox "Tune file could not be checked out." title:"Warning"
					
				if not deleteFile tunePath then
				(
					messagebox "Tune file could not be deleted. If it's checked out, try restarting max." title:"Error!"
					return false
				)
			)
			else
				return false
		)
		if not gFragTuner.CreateHierarachyFromNodes() then
			return false
		ResetRollouts gFragTuner.currFragGroup.groupName
		if undefined!=RsTreeViewRoll then
			RsTreeViewRoll.updateTV gFragTuner.currFragRoot gFragTuner.currFragGroup
	)
	
	on btnUpdate pressed do (
		gFragTuner.WriteFragTuneData()
	)
	
	on btnTickNone pressed do (
		gFragTuner.tickFragHierarchy #ticknone
	)
	on btnTickAll pressed do (
		gFragTuner.tickFragHierarchy #tickall
	)
	on btnTickDefault pressed do (
		gFragTuner.tickFragHierarchy #tickdefault
	)
	on btnTickInvert pressed do (
		gFragTuner.tickFragHierarchy #tickinvert
	)

-- 	on btnLoad pressed do (
-- 		local f = getOpenFileName caption:"Open A Test File:" filename:(RsProjectGetCommonDir()+"/data/fragments/")
-- 		if undefined!=f then
-- 		(
-- 			gFragTuner.ReadInAttrib f
-- 			gFragTuner.RefreshUI()
-- 		)
-- 	)
	
	on checkReloadFile changed val do 
	(
		gReloadFileMode = val
	)
	
	on checkPrvntCollSel changed val do 
	(
		gPrvntCollSel = val
	)
	
	on checkCpyChilds changed val do 
	(
		gCpyChildren = not val
	)
	
	on radioMode changed val do
	(
		gFragEditMode = values[val]
		gFragTuner.PopulateFragTuner()
	)
	on btnCopy pressed do
	(
		gFragTuner.copyFragHierarchy()
	)
	on btnPaste pressed do
	(
		gFragTuner.PasteFragHierarchy()
	)
)

rollout RsTreeViewRoll "Node hirarchy"
(
	button buttLevelUp "select parent node" width:280 align:#center
	dotNetControl tv "TreeView" width:280 height:200 align:#center
	local currentSelected = undefined
	
	fn addChildren theNode theChildren selected = 
	(
		if not RsTreeViewRoll.open then return false

		local bgColorClass = (dotNetclass "System.Drawing.Color")
		local bgColor = bgColorClass.FromArgb 255 0 100 255
		local fgColor = bgColorClass.FromArgb 255 255 255 255
		for c in theChildren do
		(
			newNode = theNode.Nodes.add c.groupName
			if selected.groupName==c.groupName then
			(
				newNode.backcolor = bgcolor
				newNode.foreColor = fgColor
			)
			addChildren newNode c.groupArray selected
		)
	)
	fn expandAll =
	(
		if not RsTreeViewRoll.open then return false
			
		tv.ExpandAll()
	)
	fn updateTV root selected =
	(
		if not RsTreeViewRoll.open then return false
			
		currentSelected = undefined
		tv.Nodes.clear()
		local theChildren = root.groupArray
		if theChildren.count<=0 then return false
		theRoot = tv.Nodes.add "ROOT"
		rootNodes = for o in theChildren collect o --where o.parent == undefined 
		addChildren theRoot rootNodes selected
		expandAll()
	)
	on tv Click arg do 
	(
		hitNode = tv.GetNodeAt (dotNetObject "System.Drawing.Point" arg.x arg.y)
		if hitNode != undefined and hitNode.Text!="ROOT" do 
		try(
			local theNodes = getNodeByName (hitNode.Text+"_frag_") all:true
			if(theNodes.count<=0) then
				theNodes = getNodeByName (hitNode.Text) all:true
			clearSelection()
			for aNode in theNodes where gFragTuner.isObjInCurrHierarchy aNode do
			(
				selectMore aNode
			)
		)catch(print ("error selecting node"+(getCurrentException())))
	)
	
	on buttLevelUp pressed do 
	(
		if selection[1]!=undefined and selection[1].parent!=undefined then
		(
			select selection[1].parent
		)
	)
	
	fn reset =
	(
		if not RsTreeViewRoll.open then return false

		tv.nodes.Clear()
	)
)

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- invoking code
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

addRollout RsFragRoll RsFragTuner 
addRollout RsTreeViewRoll RsFragTuner
addRollout RsButtonRoll RsFragTuner
gFragTuner.PopulateFragTuner()