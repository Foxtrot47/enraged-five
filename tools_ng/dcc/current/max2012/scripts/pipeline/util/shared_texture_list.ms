--
-- File:: shared_texture_list.ms
-- Description:: Shared Texture List Text File Management
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 6 February 2008
--
-- This file defines a reusable list structure for maintaining an text file
-- list of shared texture files.
--


-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- 18/11/2010
--
-- Rewritten to use XML files to allow us to store texture metadata
--
-- Example Usage
--
--   texlist = SharedTextureListXML()
--   texlist.Load <filename>
--

filein "pipeline/util/texturemetatag_utils.ms" -- For new RsRexExportTexture function

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------
struct TextureNode (file, tcs)

--
-- struct: SharedTextureListXML
-- desc: Same as SharedTextureList but uses XML format for storing metadata with texture
--
struct SharedTextureListXML
(
	filename = undefined,	-- Shared texture list filename
	txd = undefined, -- TXD-name for this texturelist
	textures = #(),	-- Shared texture node array
	xmlDoc = undefined,
	textures = #(),
	
	--
	-- name: load
	-- desc: Load list from a file (if exists)
	--
	fn load filePath = (
		try (
			filename = filePath
			textures = #()
			local fileExists = ( ( getFiles filename ).count != 0 )

			if ( ( undefined != filename ) and fileExists ) then
			(
				format "Loading shared texture xml: %\n" filename
				xmlDoc = XMlDocument()

				xmlDoc.init()
				xmlDoc.load filename

				xmlRoot = xmlDoc.document.DocumentElement
				rootchildren = xmlRoot.childnodes

				for i = 0 to ( rootchildren.Count - 1 ) do 
				(
					child = rootchildren.itemof(i)
					
					case child.name of 
					(
						"info":
						(
							txd = (child.Attributes.ItemOf( "txd" )).Value
						)
						"texture":
						(
							texnode = TextureNode()
							texnode.file = (child.Attributes.ItemOf( "file" )).Value
							texnode.tcs = (child.Attributes.ItemOf( "tcs" )).Value

							append textures texnode
						)
					)
				)

				true					
			)
			else
			(
				false
			)
		)
		catch 
		(
			format "%\n" (getCurrentException())
			false
		)
	),
	
	--
	-- name: reload
	-- desc: Reload list from XML file
	--
	fn reload = (
	
		( load filename )
	),
	
	--
	-- name: save
	-- desc: Save list to current text file.
	--
	fn save = 
	(
		try
		(
			format "Saving shared texture xml: %\n" filename
			xmlDoc = XMlDocument()			
			xmlDoc.init()
			
			xmlRoot = xmlDoc.createelement("textures")
			xmlDoc.document.AppendChild xmlRoot
			
			if (txd != undefined) do 
			(
				local newElem = xmlDoc.createelement "info"
				local txdAttr = xmlDoc.createAttribute "txd"
				txdAttr.value = txd
				
				newElem.Attributes.append txdAttr
				xmlRoot.AppendChild newElem
			)
			
			for texnode in textures do
			(
				attributearray = #()
				newElem = xmlDoc.createelement "texture"

				fileAttr = xmlDoc.createAttribute "file"
				fileAttr.value = texnode.file
				
				newElem.Attributes.append( fileAttr )
				
				tcsAttr = xmlDoc.createAttribute "tcs"
				tcsAttr.value = texnode.tcs
				
				newElem.Attributes.append( tcsAttr )
				
				xmlRoot.AppendChild( newElem )
			)
			xmlDoc.save filename
			
			true
		)
		catch
		(
			format "%\n" (getCurrentException())
			false
		)	
	),
	
	--
	-- name: saveas
	-- desc: Save texture list as new text file.
	--
	fn saveas newfilename = (
	
		filename = newfilename
		save()
	),
	
	--
	-- name: count
	-- desc: Return number of textures in list
	--
	fn count = (
	
		textures.count
	),
	
	--
	-- name: contains
	-- desc: Returns true iff texture list contains filename, false otherwise.
	--
	fn contains texfile = (
		for texnode in textures do (
			if texnode.file == texfile then return true
		)
		false
	),
	
	--
	-- name: add
	-- desc: Add texture to list
	--
	fn add texfile textemplate = 
	(
		if ( contains texfile ) then
		(
			format "List already contains: %\n" (filenameFromPath texfile)
			false
		)
		else
		(
			texnode = TextureNode()
			texnode.file = texfile
			texnode.tcs = textemplate
			format "Added to list: %\n" (filenameFromPath texfile)
			append textures texnode
			true
		)
	),
	
	--
	-- name: remove
	-- desc: Remove texture from list
	--
	fn remove texnode = (
		
		
		texfile = texnode.file
		local index = -1
		for i=1 to textures.count do (
			if textures[i].file == texfile then index = i
		)
		
		if ( -1 == index ) then
			false
		else
		(
			format "Removed from list: %\n" (filenameFromPath texfile)
			deleteItem textures index
			true
		)
		
	),
	
	--
	-- name: export
	-- desc: Export all of the textures to a specific directory
	--
	fn export directory = (
		
		format "Exporting textures:\n"
		for texnode in textures do
		(
			local infilename = ( RsRemovePath texnode.file )
			local infile = texnode.file
			local outfile = RsMakeSafeSlashes (directory+"/"+ (RsStripTexturePath infilename) + ".dds")
			format "\ntexnode: %" texnode
			format "\nExport texture  : % to %\n" infile outfile
			if texnode.tcs != "" then (
				RsGenerateTemplateReferenceFile outfile texnode.tcs sourceTextures:#(infile)
				rexExportTexture infile outfile raw:true uniLogFile:(gRsUlog.Filename())
			)
			else (
				RsRexExportTexture infile outfile maxsize:512
			)
		)
	)
	
)

--
-- struct: SharedTextureList 
-- desc: need to keep this for weapons export
--
struct SharedTextureList
(

	filename = undefined,	-- Shared texture list filename
	textures = #(),			-- Shared texture filename array
	
	--
	-- name: load
	-- desc: Load list from a file (if exists)
	--
	fn load filePath = (
	
		filename = filePath
		textures = #()
		local fileExists = ( ( getFiles filename ).count != 0 )

		if ( ( undefined != filename ) and fileExists ) then
		(
--			try
			(
				format "Loading shared texture list: %\n" filename
				local fsTextures = ( openFile filename mode:"r" )
				while ( not eof( fsTextures ) ) do
				(
					local lineData = ( readLine fsTextures )
					lineData = ( RsMakeSafeSlashes lineData )
					append textures lineData
				)
				close fsTextures
				
				true
			)
-- 			catch
-- 			(
-- 				false
-- 			)			
		)
		else
		(
			false
		)
	),
	
	--
	-- name: reload
	-- desc: Reload list from XML file
	--
	fn reload = (
	
		( load filename )
	),
	
	--
	-- name: save
	-- desc: Save list to current text file.
	--
	fn save = (	
	
		try
		(
			local fsTextures = ( openFile filename mode:"w" )
			for t in textures do
			(
				format "Write: %\n" t
				format "%\n" t to:fsTextures 
			)
			close fsTextures

			true
		)
		catch
		(
			false
		)	
	),
	
	--
	-- name: saveas
	-- desc: Save texture list as new text file.
	--
	fn saveas newfilename = (
	
		filename = newfilename
		save()
	),
	
	--
	-- name: count
	-- desc: Return number of textures in list
	--
	fn count = (
	
		textures.count
	),
	
	--
	-- name: contains
	-- desc: Returns true iff texture list contains filename, false otherwise.
	--
	fn contains texfile = (
	
		texfile = RsMakeSafeSlashes( texfile )
		( 0 != ( findItem textures texfile ) )
	),
	
	--
	-- name: add
	-- desc: Add texture to list
	--
	fn add texfile = (
	
		texfile = RsMakeSafeSlashes( texfile )
		if ( contains texfile ) then
		(
			false
		)
		else
		(
			append textures texfile
			true
		)
	),
	
	--
	-- name: remove
	-- desc: Remove texture from list
	--
	fn remove texfile = (
	
		texfile = RsMakeSafeSlashes( texfile )
		local index = ( findItem textures texfile )
		if ( -1 == index ) then
			false
		else
		(
			deleteItem textures index
			true
		)
	),
	
	--
	-- name: export
	-- desc: Export all of the textures to a specific directory
	--
	fn export directory = (
		
		format "Exporting textures:\n"
		for texmap in textures do
		(
			local infilename = ( RsRemovePath texmap )
			local infile = texmap
			local outfile = RsMakeSafeSlashes (directory+"/"+infilename)
			
			format "\nExport texture  : % to %\n" infile outfile
			RsRexExportTexture infile outfile maxsize:512
		)
	)
	
)


-- End of shared_texture_list.ms
