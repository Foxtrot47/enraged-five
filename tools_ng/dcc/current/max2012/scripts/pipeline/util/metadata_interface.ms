--
-- File:: pipeline/util/metadata_interface.ms
-- Description:: Lightweight interface for meta data system
--
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 14/01/2011
--

filein "pipeline/util/p4.ms"

global gRsMetaData = undefined

struct RsMetaData 
(
	Metadata,
	TunableUtils,
	MemberUtils,
	StructDict,
	StructDictPath,
	structsHash,
	metadataClass = dotnetclass "RSG.Metadata.Metadata",
	logFactoryClass = dotnetclass "RSG.Base.Logging.LogFactory",
	logClass = dotnetclass "RSG.Base.Logging.Log",
		
	fn ToggleLogging enabled = (
		metadataClass.Log.Enabled = enabled
		logFactoryClass.ApplicationLog.Enabled = enabled
		logClass.StaticLog.Enabled = enabled
	),
	
	fn LoadAssemblies = 
	(
		-- Assemblies are loaded on startup by RS_dotNetLoadStartupAssemblies()
		
		-- Create an reference to static util class for tunables
		if TunableUtils == undefined do
		(
			TunableUtils = dotNetClass "RSG.Metadata.Util.Tunable" 
			MemberUtils = dotNetClass "RSG.Metadata.Util.Member" 
		)
	),
	
	fn init structsPath:(RsConfigGetMetadataDefinitionDir()) reloading:false = 
	(
		LoadAssemblies()
		-- All structs under the struct path will be loaded
		try 
		(
			local timeStart = timeStamp()
			
			-- AJM:Temporarily disable the log that will get errors in it when loading metadata	
			ToggleLogging false
			
			if reloading then 
			(
				pushPrompt "Metadata Dictionary files changed - Reloading"
			)
			else 
			(
				pushPrompt "Loading Metadata Dictionary"
			)
			
			local configFactory = ( dotNetClass "RSG.Base.Configuration.ConfigFactory" )
			local config = configFactory.CreateConfig()
			local branch = config.Project.DefaultBranch
			StructDictPath = structspath
			
			structDict = dotNetObject "RSG.Metadata.Model.StructureDictionary"
			structPathNet = dotNetObject "System.String" structspath
			structDict.Load branch structPathNet
			ToggleLogging true
			
			popPrompt()
			format "Metadata PSC load took % seconds\n" ((timeStamp() - timeStart) / 1000.0)
			return true
		)
		catch 
		(
			popPrompt()
			print ("PSC LOAD FAILURE: " + (getCurrentException()))
			ToggleLogging true
			
			return false
		)
	),
	
	fn load loadName withStruct:false = 
	(
		-- AJM:Temporarily disable the log that will get errors in it when loading metadata	
		ToggleLogging false
		try 
		(
			Metadata = if withStruct then 
			(
				-- Generate the metafile
				local pedVarStruct = structDict.Item(loadName)
				dotNetObject "RSG.Metadata.Model.Metafile" pedVarStruct
			)
			else 
			(
				-- Load the metafile
				dotNetObject "RSG.Metadata.Model.Metafile" loadName structDict
			)
		)
		catch 
		(
			local msg = "Exception loading meta data file "+loadName as string+":" + ( getCurrentException())
			grsulog.LogError msg context:loadName
			ToggleLogging true
		)	
		
		ToggleLogging true
	),
	
	-- Load a metafile along with the path to associated pcs files
	fn LoadWithMetaFile metafile = 
	(
		load metafile withStruct:false
	),
	
	-- Create a default metafile from a struct
	fn LoadWithStruct structname = 
	(
		load structname withStruct:true
	),
	
	--Save the specified metadata file.
	fn SaveMetaData filename = 
	(
		Metadata.SerialiseAlt(filename)
	),
	
	-- Retrieve first tunable of given name within loaded metafile
	fn FindFirstStructureNamed structName = (
		TunableUtils.FindFirstStuctureNamed structName Metadata.Members
	),
	
	-- Retrieve all tunable's of given name within loaded metafile.  Store in a maxscript 
	-- array for easier traversal in MXS (this might be a pipe dream since we want to 
	-- operate on dotnet objects 'in place')
	fn FindAllStructuresNamed structName = (
		structureList = #()
		dotNetList = TunableUtils.FindAllStucturesNamed structName Metadata.Members
		dotNetList
	),
	
	fn CreateNewStructTunableFor structName = (
		member  = MemberUtils.FindStructMemberByName Metadata.Definition.Members structName
		-- Undefined is passed here so that the correct constructor is used bugstar:498805
		structTunable = dotNetObject "RSG.Metadata.Data.StructureTunable" member undefined
		structTunable
	),
	
	fn CreateNewPointerTunableFor structName = (
		member  = MemberUtils.FindStructMemberByName Metadata.Definition.Members structName
		structTunable = dotNetObject "RSG.Metadata.Data.StructureTunable" member
		
		pointerMember  = MemberUtils.FindPointerMemberByName Metadata.Definition.Members structName
		pointerTunable = dotNetObject "RSG.Metadata.Data.PointerTunable" pointerMember
		pointerTunable.Value = structTunable
		pointerTunable.Definition.Name = structName
		pointerTunable
	)
)

fn RsUserHasLabeledMetadata dataDirectory labelType =
(	
	local haveExportDataList = dataDirectory + "...#have"
	local labelExportDataList = dataDirectory + "...@" + (RsProjectGetLabelName labelType)
	local diff2_args = #("-q", haveExportDataList, labelExportDataList)
	
	if( gRsPerforce.connected() == false ) then gRsPerforce.connect()
	P4ExceptionLevel = dotNetClass "P4API.P4ExceptionLevels"
	gRsPerforce.p4.ExceptionLevel = P4ExceptionLevel.NoExceptionOnErrors
	local diff2_records = ( gRsPerforce.run "diff2" diff2_args returnResult:true silent:true )
	
	local onLabel = true
	
	if undefined==diff2_records then
	(
		gRsUlog.LogMessage ("Error occured while querying record from perforce for "+dataDirectory as string)
		return false
	)
	
	for diff2_record in diff2_records.records while onLabel == true do
	(
		local status = diff2_record.item["status"]
		
		if( status == "content" or status == "types" ) then -- files are different, so check the revisions
		(
			local haveRev = diff2_record.item["rev"] as Integer
			local labelRev = diff2_record.item["rev2"] as Integer
			
			if( haveRev > labelRev ) then -- locally we are beyond the label
			(
				onLabel = false
			)
		)
	)
	
	onLabel
)

fn RsSyncMetaDataDefinitions =
(
	if ( RsProjectGetPerforceIntegration() == false ) do ( return true )
	
	local labelType = "current"
	local labelMetaDataDirectory = RsConfigGetMetadataDefinitionDir core:true + "...@" + (gRsConfig.Version.LocalVersion.LabelName)
	
	pushPrompt "Checking build-label status..."
	local onLabel = RsUserHasLabeledMetadata (RsConfigGetMetadataDefinitionDir core:true) labelType
	popPrompt()
	
	pushPrompt "Syncing PSC metadata files..."
	if (onLabel == true) then
	(
		gRsPerforce.sync labelMetaDataDirectory silent:true
		--print "We are on the current label. Syncing PSC files to labeled build."
	)
	else
	(
		gRsPerforce.sync #((RsConfigGetMetadataDefinitionDir core:true + "/...")) silent:true
		--print "We are not on the current label. Syncing PSC files to head."
	)
	popPrompt()
)

-- Returns master-copy of metadata manager
-- (Initialised and loaded on first run)
fn RsGetMetaDataManager syncDefinitions:true = 
(
	local structsPath = RsConfigGetMetadataDefinitionDir core:true
	local structsHash = RsPathTimeHash structsPath
	
	local retVal
	
	if (syncDefinitions) do
	(
		-- Sync the definitions before building the global metadata structure
		RsSyncMetaDataDefinitions()
	)
	
	-- Create new global struct if undefined or its dictionary-files have changed:
	if (gRsMetaData == undefined) then --or (gRsMetaData.structsHash != structsHash) then -- TEMP REMOVED, until dictionary-reloading crash is fixed
	(
		local reloading = (gRsMetaData != undefined)
		
		retVal = RsMetaData structsHash:structsHash
		
		if not (retVal.init structsPath:structsPath reloading:reloading) do 
		(
			retVal = undefined
		)
		
		gRsMetaData = retVal
	)
	else 
	(
		retVal = gRsMetaData
	)
	
	return retVal
)
