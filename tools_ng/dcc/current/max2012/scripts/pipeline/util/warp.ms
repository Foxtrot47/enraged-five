fileIn "pipeline/util/RAG_funcs.ms"

fn warpPlayerToSelection = 
(
	/*
	Description:
		Warps the player to the current selection.  Automatically detects if we are in sub-object mode, and
		determines a position based on what's selected.
	
	Author:
		Jason Hayes <jason.hayes@rockstarsandiego.com>
	*/
	
	local selObjs = GetCurrentSelection()
	
	if (SelObjs.Count != 1) do 
	(
		messageBox "Please select one object!" title:"Invalid Selection"
		return False
	)
	
	local Obj = SelObjs[1]
	local WarpPos = undefined
	
	if (subObjectLevel == undefined) or (subObjectLevel == 0) or (not isProperty Obj #mesh) then 
	(
		-- Use object position if no mesh subobjects are selected:
		WarpPos = Obj.Pos
	)
	else 
	(
		-- Get current subobject-selection as vertex-list (converted from other subobj-types if required)
		local UseMesh = undefined
		local SelVerts = RsGetSelVertNums Obj useMesh:&useMesh
		
		-- Get centre of bounding-box for selected verts:
		local SelBox = RsFindMinBox UseMesh node:Obj useVerts:SelVerts
		WarpPos = SelBox.Transform.Translationpart
		
		-- Try dropping an intersection-ray onto this position:
		local RayMesh = RayMeshGridIntersect()
		RayMesh.Initialize 10
		RayMesh.AddNode Obj
		RayMesh.BuildGrid()
		
		-- Cast ray down at object:
		local DropPos = [WarpPos.X, WarpPos.Y, 99999]
		local Hits = RayMesh.IntersectRay DropPos [0,0,-1] False
		
		-- Replace WarpPos with surface-position, if found:
		if (Hits != 0) do 
		(
			WarpPos = DropPos - [0,0,(RayMesh.GetHitDist 1)]
		)
	)
	
	if (warpPos != undefined) then 
	(
		format "Warping to: %\n" WarpPos
		::RsRagFuncs.setPlayerPos warpPos dir:( obj.rotation as eulerangles ).z
	) 
	else 
	(
		messageBox "Could not determine a position to warp the player!  Please try a different selection." title:"Warp Failed"
	)
)
