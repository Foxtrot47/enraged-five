filein "pipeline/helpers/arrays.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- utility: RsTreeLodGenerator
-- desc: Tree LOD generator functions.
--

global uvChannel_planar = 1
global uvChannel_texmap = 2
global uvChannel_scale = 3
global uvChannel_axisAligned = 4

-- Increment this when major changes are made to RsCopyBillboardDimensionsToShaders,
-- to let the ComboLodder know that existing billboard-models need to be regenerated:
global RsComboLod_billboardMatVersion = 3

--------------------------------------------------------------------------------
-- helpers 
--------------------------------------------------------------------------------
fn RsWalkBillboardMtls obj bbFaces:#() bbIds:#() = 
(
	local matFaces = #()
	local matIds = #()
	local bbFacesGet = #{}

	local matList = RsGetMaterialsOnObjFaces obj material:obj.material faceLists:matFaces mtlIdList:matIds
	
	for matNum = 1 to matList.count do 
	(
		local treeMat = matList[matNum]
		
		if (RsIsBillboardMat treeMat) do
		(
			append bbIds matIds[matNum]
			bbFacesGet += matFaces[matNum]
		)
	)
	
	join bbFaces (bbFacesGet as array)
	
	-- Set billboard flag on combined model, if bb materials were found:
	local hasBillboards = (bbIds.count > 0)
	if hasBillboards then
	(
		setUserProp obj "collapseBBVerts" (RsArrayJoin bbIds)
	)
	else
	(
		RsUserProp_Clear obj "collapseBBVerts"
	)
	
	return hasBillboards
)

fn RSCheckForLegacySlodUDF objs: = 
(
	if (objs == unsupplied) do 
	(
		objs = (geometry as array)
	)
	
	pushPrompt "Checking Axis-Aligned Billboards data..."
	
	local validobjs = for obj in objs where (getattrclass obj == "Gta Object") and ((isKindOf obj Editable_Mesh) or (isKindOf obj Editable_Poly)) collect obj
	
	for obj in validobjs do
	(
		local objOp = RsMeshPolyOp obj

		local bbFaces = #()
		local hasBillboards = RsWalkBillboardMtls obj bbFaces:bbFaces
		
		if hasBillboards do 
		(
			local objGetMapFace = RsGetMapFaceFunc obj
			
			if not objOp.getMapSupport obj uvChannel_axisAligned then 
			(
				objOp.setFaceColor obj uvChannel_axisAligned #all black
			)
			else 
			(
				bbFaces = (bbFaces as bitArray)

				local invalidFaces = #{}
				invalidFaces.count = bbFaces.count
				
				for faceNum = bbFaces do 
				(
					local faceMapVerts = (objGetMapFace obj uvChannel_axisAligned faceNum)
					local validFace = True

					local firstVal
					
					-- Does this face have any invalid/mixed vert-values?
					for vertNum = faceMapVerts while validFace do 
					(
						-- Check for invalid values:
						local thisValue = objOp.getMapVert obj uvChannel_axisAligned vertNum
						validFace = (thisValue == [1,0,0]) or (thisValue == [0,0,0])

						-- Checked for mixed values on a face:
						if validFace do 
						(
							if (firstVal == undefined) then 
							(
								firstVal = thisValue
							)
							else 
							(
								validFace = (thisValue == firstVal)
							)
						)
					)
					
					invalidFaces[faceNum] = (not validFace)
				)
					
				-- Set invalid faces to non-aligned:
				if (invalidFaces.numberSet != 0) do 
				(
					local msg = stringStream ""
					format "Setting invalid vert-colours to [0,0,0] on %, faces %" (obj as string) (invalidFaces as string)
					gRsUlog.LogMessage (msg as string) context:obj quiet:false
					
					objOp.setFaceColor obj uvChannel_axisAligned invalidFaces [0,0,0]
				)
			)
		)
	)
	
	popPrompt()
	
	return OK
)

fn RSCheckForInvalidSlodObj o = 
(
	if ( 	not RsMapIsGeneric and 
			not isRefObj o and 
			matchpattern o.name pattern:"*_s_*" and
			not matchpattern o.name pattern:"*_s_frag_"
		) do
	(
		if "" == getUserProp o "collapseBBVerts" then
		(
			local errorMesage = "SLOD2 object " + o.name + " doesn't have a 'collapseBBVerts' user defined property.  " + 
				"If this object was created via the Tree LOD Generator then you need to recreate it.  " +
				"If this object wasn't intended to be an SLOD2 object then remove the '_s_' from its name."
			gRsUlog.LogError errorMesage context:o quiet:false
		)
		if undefined==o.material then
		(
			gRsUlog.LogError ("SLOD2 object " + o.name + " doesn't have a material applied to it.") context:o quiet:false
		)
		if "None"==RsScenelink.GetLinkType o then
		(
			local warningMesage = "SLOD2 object " + o.name + " is not in any LOD hierarchy.  " + 
				"If this object wasn't intended to be an SLOD2 object then remove the '_s_' from its name."
			gRsUlog.LogWarning warningMesage context:o quiet:false
		)
	)
)

fn HelperGetTexAlphaTexCombination obj mtl index = 
(
	local txs=#()
	local bmps = #()
	RsGetTexMapsFromMaterialWithMap obj mtl txs bmps
	return txs[index]
)

-- Automatically sets "BB Width/Height" value on billboard-tree shaders to average size of their objects' faces:
fn RsCopyBillboardDimensionsToShaders objs optimiseMultiMtls:true = 
(
	pushprompt "Copying Billboard Dimensions to Shaders"
	
	-- This idx is set when the first tree-shader is processed:
	local bbSizeIdx
	
	local treeObjs = for obj in objs where ((isKindOf obj Editable_Mesh) or (isKindOf obj Editable_Poly)) and (RsIsTreeBillboard obj obj.material) collect obj
	
	-- Collect materials used by objs with tree-shaders:
	local treeObjMats = for obj in treeObjs collect obj.material
	treeObjMats = makeUniqueArray treeObjMats
	
	-- Don't allow tree-shader objects to share materials, as that'll overwrite each others' bb-size values:
	for mat in treeObjMats do 
	(
		local matObjs = refs.dependentNodes mat
		for n = 2 to matObjs.count do 
		(
			matObjs[n].material = copy mat
		)
	)
	treeObjMats.count = 0
	
	gc light:true
	
	for obj in treeObjs do
	(
		try
		(
			-- Get functions suitable dealing with this object-type:
			local objGetFaceMatID = RsGetFaceMatIDFunc obj
			local objGetFace = RsGetFaceFunc obj
			local objGetVert = RsGetVertFunc obj
			local objGetMapFace = RsGetMapFaceFunc obj
			
			local objOp = RsMeshPolyOp obj
			
			local objMat = obj.material
			local singleMat = not (isKindOf objMat MultiMaterial)
			
			local hasAxisAlignChan = (objOp.getMapSupport obj uvChannel_axisAligned)
			
			local objTreeMats = #()
			RsFindTreeShader obj objMat treeShaderList:objTreeMats
			--format "Found % tree shaders on object %\n" objTreeMats.count obj.name
			
			-- Build material-id lookup-list for object:
			local matIdFaces = #()
			local faceCount = getNumFaces obj
			local faceError = false
			if not singleMat do 
			(
				for faceNum = 1 to faceCount do 
				(
					local matId = objGetFaceMatID obj faceNum
					local matIndex = findItem objMat.materialIdList matId
					local treeMat
					
					if (matIndex == 0) then
					(
						gRsUlog.LogError ("Face "+faceNum as string+" on object "+obj.name+" has a material ID that doesn't exist in its multimaterial!") context:obj
						throw "face id calculation error"
					)
					else 
					(
						treeMat = objMat.materiallist[matIndex]
					)
					
					if optimiseMultiMtls and (RsIsBillboardMat treeMat) then 
					(
						local treeMatTexVar = HelperGetTexAlphaTexCombination obj treeMat 1 withAlpha:true
						for innerTreeMat in objTreeMats do
						(
							local treeMatTexVarInner = HelperGetTexAlphaTexCombination obj innerTreeMat 1 withAlpha:true
							--format "%:% == %:%" matIDInner treeMatTexVarInner matId treeMatTexVar
							if treeMatTexVarInner == treeMatTexVar then
							(
								local matIndexInner = findItem objMat.materiallist innerTreeMat
								local matIDInner = objMat.materialIdList[matIndexInner]
								if (RstGetShaderName treeMat) == (RstGetShaderName innerTreeMat) and
									matIDInner != matId then
								(
									--gRsUlog.LogMessage ("Optimising face "+faceNum as string+" to use sub material number "+matIndexInner as string+" istead of "+matIndex as string) context:obj
									local treeShaderListIndex = findItem objTreeMats treeMat
									if 0!=treeShaderListIndex then
										deleteItem objTreeMats treeShaderListIndex

									matIndex = matIndexInner
									matId = matIDInner
									(RsSetFaceMatIDFunc obj) obj faceNum matIndex
								)
								exit
							)
						)
					)
					
					if matIdFaces[matId] == undefined do 
					(
						matIdFaces[matId] = #{}
						matIdFaces[matId].count = faceCount
					)
					
					matIdFaces[matId][faceNum] = true 
				)
			)
			
			for treeMat in objTreeMats do
			(
				-- begin BB func:
				local hasBBmats = false
				local matBBmaxX = 0.0
				local matBBmaxZ = 0.0
				local matBBList = #()
				local matBBAxisList = #()
				local sumBbSize = [0,0]
				
				-- Get dimension-value index from shader, if not found yet:
				for varNum = 1 to (RstGetVariableCount treeMat) while (bbSizeIdx == undefined) do 
				(
					local varName = RstGetVariableName treeMat varNum

					if (matchPattern varName pattern:"BB Width/Height") do 
					(
						bbSizeIdx = varNum
					)						
				)
				
				-- Find the matId used by this material (if a multimaterial)
				local matId = 0
				if not singleMat do 
				(
					local matNum = findItem objMat.materialList treeMat
					matId = objMat.materialIdList[matNum]
				)
				
				-- Get average size of faces used by material:
				local matFaces = if singleMat then #{1..faceCount} else matIdFaces[matId]
				for faceNum = matFaces do 
				(
					-- Find face's bounding-box:
					local hasAxisAlignedVert = false
					local vertPosList = #()
					
					local faceVerts = (objGetFace obj faceNum)
					local faceMapVerts
					
					if hasAxisAlignChan do 
					(
						faceMapVerts = (objGetMapFace obj uvChannel_axisAligned faceNum)
					)
					
					-- Get vert positions.  Does this face have axis-align-value verts?
					for vertIdx = 1 to faceVerts.count do
					(
						append vertPosList (objGetVert obj faceVerts[vertIdx])

						if hasAxisAlignChan and (not hasAxisAlignedVert) do 
						(
							local thisValue = objOp.getMapVert obj uvChannel_axisAligned faceMapVerts[vertIdx]
							
							if ([0,0,0] != thisValue) do 
							(
								hasAxisAlignedVert = true
							)
						)
					)

					local faceBBmin = copy vertPosList[1]
					local faceBBmax = copy vertPosList[1]
					
					for vertNum = 2 to vertPosList.count do 
					(
						local vertPos = vertPosList[vertNum]
						
						for n = 1 to 3 do 
						(
							if (vertPos[n] < faceBBmin[n]) do (faceBBmin[n] = vertPos[n])
							if (vertPos[n] > faceBBmax[n]) do (faceBBmax[n] = vertPos[n])
						)
					)
					
					local bbSize = faceBBmax - faceBBmin
					local matBBVal = [length [bbSize.x, bbSize.y], bbSize.z]
					matBBList[faceNum] = matBBVal
					matBBAxisList[faceNum] = hasAxisAlignedVert
					
					-- Our BB is XY since it is a max to our UV but really we are taking height (Z) into account
					if matBBmaxX < matBBVal.x do matBBmaxX = matBBVal.x
					if matBBmaxZ < matBBVal.y do matBBmaxZ = matBBVal.y
				)
				
				-- Get average size of faces used by material:
				local matFaces = if singleMat then #{1..faceCount} else matIdFaces[matId]
				for faceNum = matFaces do 
				(
					-- Find face's bounding-box:
					local matBBVal = matBBList[faceNum]
					sumBbSize += matBBVal
					if (matBBVal != undefined) then 
					(
						local uv2scalex = (matBBVal.x*obj.scale.x) / matBBmaxX
						local uv2scaley = (matBBVal.y*obj.scale.z) / matBBmaxZ
						
	-- 					print matBBVal.x
	-- 					print matBBVal.y
	-- 					print matBBmaxX
	-- 					print matBBmaxZ
	-- 					print uv2scalex
	-- 					print uv2scaley
	-- 					print "*******"
						
						objOp.setFaceColor obj uvChannel_scale faceNum [uv2scalex*255, uv2scaley*255, 0]
						
						-- Set axis-align colour for face to black/red (so we avoid having partially-tagged faces)
						local axisAlignColour = if matBBAxisList[faceNum] then [255,0,0] else [0,0,0]
						objOp.setFaceColor obj uvChannel_axisAligned faceNum axisAlignColour
						--format "facenum: %, matBBVal:%\n" faceNum matBBVal
					)
				)
	--			local avgBbSize = (sumBbSize /matFaces.numberset)
				RstSetVariable treeMat bbSizeIdx (point4 matBBmaxX matBBmaxZ 0 0)
				--print (RstGetVariable treeMat bbSizeIdx)
			)
		)
		catch
		(
			grsULog.LogError (getCurrentException()) context:"RsCopyBillboardDimensionsToShaders"
		)
	)
	
	popPrompt()
	
	-- return whether any billboard mtls have been found
	return (treeObjs.count > 0)
)
