-- 
-- GUID tools
-- Author: Gunnar Droege
-- 
-- 

struct sGuidTools 
(
	fn GetObjByGuid guid lookupname:undefined = 
	(
		for o in objects where 
			(matchPattern (RsGuidMgr.GetGuid o) pattern:("*"+guid+"*")) do
			return o
		return undefined
	),
	
	fn CompareGuids guid1 guid2 = 
	(
		guid1 = dotnetobject "System.Guid" guid1
		guid2 = dotnetobject "System.Guid" guid2
		guid1 == guid2
	),
	
	-- RsCheckForDuplicateObjGuids
	-- Check aaaaaall objects in this scene for their guids. All of them get serialised to and from SceneXML and break stuff if duplicate
	fn RsCheckForDuplicateObjGuids = 
	(
		local guidMap = dotnetobject "RSG.MaxUtils.MaxDictionary"
		for o in objects do
		(
			local myGuid = RsGuidMgr.GetGuid o
			if undefined==myGuid then
				continue
			
			local alreadyCollectedItems = #()
			if guidMap.containsKey myGuid then
			(
				alreadyCollectedItems = guidMap.item myGuid
				if 0!=findItem alreadyCollectedItems o.name then
					continue
					
				-- GD TODO -- THIS IS TEMPORARY UNTIL WE ENABLE SERIALISATION OF  GUIDS. 
				-- AFTER THAT THE DECISION MUST BE MANUAL AS TO WHICH OBJECT IS DEEMED VALID
				if ("Gta Collision" == (GetAttrClass o)) or
					(isRsRef o) then
				(
					resetAttrGuid o
					continue
				)
			)

			
			append alreadyCollectedItems (o.name as string)
			guidMap.put myGuid alreadyCollectedItems
			
		)
		local guidEnum = guidMap.GetEnumerator()
		while guidEnum.MoveNext() do
		(
			if guidEnum.Current.Value.Count>1 then
				gRsUlog.LogError ("Following objects share the GUID \""+guidEnum.Current.Key as string+"\". Select the ones that are not needed for live editing and use \“RsOrganisational -> GUID tools -> Reset Object GUID\" to reset: "+guidEnum.Current.Value as string) \
					context:guidEnum.Current.Value
		)
		gRsUlog.Validate()
	)
)

RsGuidTools = sGuidTools()