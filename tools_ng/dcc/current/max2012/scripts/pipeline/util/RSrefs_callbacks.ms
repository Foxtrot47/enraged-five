--
-- RSref callbacks
-- Sets up callbacks used by the RSref system, and associated rollouts
--
-- Neal D Corbett
-- Rockstar Leeds
-- 08/06/2011
--

-----------------------------------------------------------------------------
-- NODE CALLBACK FUNCTIONS:
-----------------------------------------------------------------------------
-- Removes RS NodeEventCallbacks if re-running this script
if (RSobjNodeEventCallback != undefined) do 
(
	format "RESETTING RSREF CALLBACK FUNCTIONS\n"
	
	unregisterRedrawViewsCallback RsRefViewportDrawMarkers
	RsRefData.objWarnings = undefined
	
	RSobjNodeEventCallback = undefined
	RSrefNodeEventCallback = undefined
	gc light:true
)

-- Redraw-callback marks any refs that need marking:
fn RsRefViewportDrawMarkers = 
(
	if RSmaxfilingNow or RsRefsWaitingForRedrawLoad or RsRefData.notLoaded do return false
	
	-- Check for invalid objects in warning-list:
	if (RsRefData.objWarnings != undefined) do 
	(
		local allGood = true
		for item in RsRefData.objWarnings while allGood do 
		(
			allGood = isValidNode item.obj
		)
		
		if not allGood do 
		(
			RsRefData.objWarnings = undefined
		)
	)
	
	-- Generate warning-list if required:
	if (RsRefData.objWarnings == undefined) do 
	(
		RsRefData.objWarnings = for obj in geometry where isRsRef obj collect 
		(
			local isInternalRef = isRsInternalRef obj
			local isContainerLod = (not isInternalRef) and (isKindOf obj RsContainerLodRef)
			
			local refDef = if isInternalRef then undefined else obj.refDef
			local parentObj = obj.parent
			
			local warning = case of 
			(
				isInternalRef:
				(
					case of 
					(
						(obj.delegate == undefined):undefined
						(not isValidNode obj.irefSourceObj):"INVALID SOURCE"
						((obj.irefSourceObj.parent != obj.parent) and ((isKindOf obj.irefSourceObj.parent container) or (isKindOf obj.parent container))):"DIFFERENT CONTAINER TO SOURCE"
						default:undefined
					)
				)
				((refDef == undefined) and ((not isContainerLod) or (obj.delegate != undefined))):"UNDEFINED"
				(refDef.notFoundInMaxfile):"NOT IN MAXFILE"
				(refDef.obsolete):"OBSOLETE"
				(refDef.badRef):"BAD REF"
				(not refDef.isBrowsable):"NON-BROWSABLE"
				(not refDef.isPlaceable):"NON-PLACEABLE"
				(obj.meshObj == undefined):"UNDEFINED MESH"
				(refDef.badXform.numberSet != 0):"SOURCE HAS XFORMS"
				default:undefined
			)
			
			if (warning == undefined) then dontCollect else (dataPair obj:obj text:warning)
		)
	)
	
	-- Draw warning-list:
	gw.setTransform (Matrix3 1)
	local clr = [255,0,0]
	for item in RsRefData.objWarnings where (not item.obj.isHidden) do 
	(
		local pos = gw.wtranspoint item.obj.pos
		
		gw.wtext pos item.text color:clr
		gw.wMarker pos #xMarker color:clr
	)
)
registerRedrawViewsCallback RsRefViewportDrawMarkers

global RSgeomChanged = false
global RSmatChanged = false
global RSrefAllowEvents = true

-- Triggered at the start of a geometry/material change-event callback
-- allows other callbacks to only trigger once each per event-call
fn RSstartEvent ev nd = 
(
	RSgeomChanged = false
	RSmatChanged = false
)
-- Triggered when geometry is changed
fn RSgeomEvent ev Nodes = 
(
	if RSgeomChanged do
	(
		local changeTime = timeStamp()
		for n in nodes do (RSsetBigArrayVal RSNodeMeshChangeTimes n changeTime)

		if RSrefNodeCallbackDebugPrint do 
		(
			format "Node geometries changed: %\n" nodes.count
			--for n in nodes do (format "%:% " (getAnimByHandle n).name (RSgetBigArrayVal RSNodeMeshChangeTimes n))
			--format "\n"
			format "Geometry change-time store took % seconds\n" ((timeStamp() - changeTime) / 1000.0)
		)

		RSgeomChanged = true
	)
)
-- Triggered when a node's material is changed
fn RSmatEvent ev Nodes = 
(
	if not RSmatChanged do 
	(
		local changeTime = timeStamp()
		for n in nodes do (RSsetBigArrayVal RSNodeMatChangeTimes n changeTime)
			
		if RSrefNodeCallbackDebugPrint do 
		(
			format "Node materials changed: %\n" nodes.count
			--for n in nodes do (format "%:% " (getAnimByHandle n).name (RSgetBigArrayVal RSNodeMatChangeTimes n))
			--format "\n"
			format "Material change-time store took % seconds\n" ((timeStamp() - changeTime) / 1000.0)
		)
		
		RSmatChanged = True
	)
)

-- This callback keeps track of when object geometry or materials change
RSobjNodeEventCallback = NodeEventCallback mouseUp:true delay:1000 \
	callbackBegin:RSStartEvent materialStructured:RSMatEvent \
	geometryChanged:RSGeomEvent nameChanged:RSGeomEvent

-- RSrefStartEvent --
-- Run at the start of a RSrefNodeEventCallback call
fn RSrefStartEvent ev nd = 
(
	RSrefAllowEvents = True
)
-- RSrefStopFurtherEvents --
-- Will stop further event-functions from being run
-- specifically to avoid re-triggering an event after it's just rebuilt a mesh
fn RSrefStopFurtherEvents ev nodes = 
(
	RSrefAllowEvents = False
)
-- RSrefLinkEvent --
-- Callback function run whenever an RSref's link-hierarchy changes:
fn RSrefLinkEvent ev nodes = 
(
	local objs = for nodeHandle in nodes collect  
	(
		getAnimByHandle nodeHandle
	)
	
	-- Update any RsInternalRefs, as their source-objects may no longer be in the same container now:
	local changedIRefs = for obj in objs where isRsInternalRef obj collect obj
	if (changedIRefs.count != 0) do 
	(
		-- Stops nodes from spazzing out when instanceMgr does its thing:
		local allowAttachWas = RSrefData.allowAttachToNode
		RSrefData.allowAttachToNode = false
		
		instanceMgr.MakeObjectsUnique changedIRefs #group
		changedIRefs = RsGetInstGroupLeaders changedIRefs
		changedIRefs.rebuildMesh = true
		
		RSrefData.allowAttachToNode = allowAttachWas
		
		redrawViews()
	)
)
-- RSrefNameEvent --
-- Callback function stops user from renaming RSref objects
fn RSrefNameEvent ev nodes = 
(
	-- RSref objects are automatically named after their ref-definition, plus _RSref suffix
	local objs = for handle in nodes collect (getAnimByHandle handle)
	
	RSrefFuncs.setObjNames objs prefiltered:false debugFuncSrc:"[RSrefNameEvent]"
	
	-- If a renamed object has RsInternalRef dependents, update their source-names:
	for obj in objs do 
	(
		for depObj in (refs.dependentNodes obj) where (isRsInternalRef depObj) and (depObj.irefSourceObj == obj) do 
		(
			format "  %\n" depObj.name
			depObj.setSourceName obj.name
		)
	)
)
-- Warns whenever an RSref object has been converted into a non-RSref one
fn RSrefModelStructuredEvent ev nodes = 
(
	local objs = for nodeHandle in nodes collect (getAnimByHandle nodeHandle)
	local nonRefs = for obj in objs where (isKindOf obj GeometryClass) and (not isRSrefSuperClass obj) collect obj

	-- Collect changed non-ref objects that have RS/Iref names:
	local refNamedNonRefs = for obj in nonRefs where (matchPattern obj.name pattern:"*_RSref*") or (matchPattern obj.name pattern:"*_Iref*") collect obj
	
	if (refNamedNonRefs.count != 0) do 
	(
		fn refNamedNonRefFilter obj = 
		(
			(isKindOf obj GeometryClass) and (not isRefObj obj) and (not isInternalRef obj) and ((matchPattern obj.name pattern:"*_RSref*") or (matchPattern obj.name pattern:"*_Iref*"))
		)
		
		-- Update list to include previously-existing examples too:
		refNamedNonRefs = for obj in objects where refNamedNonRefFilter obj collect obj
		
		local plural = if refNamedNonRefs.count == 1 then "" else "s"
		local warningText = stringStream ""
		format "% non-ref object% found with ref-object suffix" refNamedNonRefs.count plural plural to:warningText
		
		RsNotifyPopUp_create text:(warningText as string) title:"Warning: Non-refs with ref suffixes" selFunc:refNamedNonRefFilter
	)
)
-- Warns whenever an RSref object is given non-uniform scaling
fn RSrefControllerEvent ev nodes = 
(
	local objs = for nodeHandle in nodes collect (getAnimByHandle nodeHandle)

	-- Returns True if ref-object with bad scaling, and adds to appropriate array-arg:
	fn badRefScaling obj scaledDyn:#() nonUni:#() nonUniHoriz:#() = 
	(
		(isRsRef obj) and 
		(
			local objScale = obj.scale
			
			case of 
			(
				(obj.refDef == undefined):(False)
				(obj.refDef.isDynamic):
				(
					if ((distance objScale [1,1,1]) > 0.00001) then 
					(
						append scaledDyn obj
						True
					) 
					else False
				)
				(obj.refDef.allowNonUniScale):
				(
					if ((abs (objScale.X - objScale.Y)) > 0.00001) then 
					(
						append nonUniHoriz obj
						True
					) 
					else False
				)
				Default:
				(
					if ((abs (objScale.X - objScale.Y)) > 0.00001) or ((abs (objScale.X - objScale.Z)) > 0.00001) then 
					(
						append nonUni obj
						True
					) 
					else False
				)
			)
		)
	)
	
	-- Test changed objects:
	local badScaleRefs = for obj in objs where (badRefScaling obj) collect obj
	
	if (badScaleRefs.count != 0) do 
	(
		local scaledDyn = #()
		local nonUni = #()
		local nonUniHoriz = #()
		
		-- Update list to include previously-existing examples in error-popup, splitting them into error-category arrays:
		badScaleRefs = for obj in objects where (badRefScaling obj scaledDyn:scaledDyn nonUni:nonUni nonUniHoriz:nonUniHoriz) collect obj
		
		local warningText = stringStream ""
		format "Ref-object% found with inappropriate scaling:\n\n" (if badScaleRefs.count == 1 then "" else "s") to:warningText
		
		if (scaledDyn.count != 0) do 
		(
			format "% scaled Dynamic prop%\n" scaledDyn.count (if scaledDyn.count == 1 then "" else "s") to:warningText
		)
		if (nonUni.count != 0) do 
		(
			format "% prop% with non-uniform scaling\n    \"Allow Non-Uniform...\" not set on source\n" nonUni.count (if nonUni.count == 1 then "" else "s") to:warningText
		)
		if (nonUniHoriz.count != 0) do 
		(
			format "% prop% with different X/Y scales\n    \"Allow Non-Uniform...\" is set on source,\n     but X/Y still need to be uniform\n" nonUniHoriz.count (if nonUniHoriz.count == 1 then "" else "s") to:warningText
		)
		
		RsNotifyPopUp_create text:(warningText as string) title:"Warning: Bad ref-scaling" selFunc:badRefScaling
	)
)
-- RSrefDisplayEvent --
-- Lets RSref objects know when their Box-Mode display-option has been toggled, 
-- causing them to draw their proper mesh instead of a simple bounding-box
fn RSrefDisplayEvent ev nodes = 
(
	local obj
	local doRedraw = false
	
	if RSrefAllowEvents do 
	(
		local objs = for nodeHandle in nodes collect (getAnimByHandle nodeHandle)
		
		for obj in objs where (isRSref obj includeDelegates:true) do 
		(
			-- Don't allow object to be taken out of box-mode if RSrefData.showAsBoxes active
			if RSrefData.showAsBoxes and not obj.boxMode do 
			(
				obj.boxMode = true
				doRedraw = true
			)
			
			-- Rebuild the mesh if the boxmode is different to what it last was
			if (obj.boxMode != obj.boundBoxDrawn) do 
			(
				obj.rebuildMesh = True
			)
		)
	)
	
	if doRedraw do 
	(
		redrawViews()
	)
)

-- This callback is intended to track changes to RSref objects, 
-- and is triggered when an object is renamed or has its boxMode turned on/off
RSrefNodeEventCallback = NodeEventCallback mouseUp:true delay:0 \
	callbackBegin:RSrefStartEvent geometryChanged:RSrefStopFurtherEvents \
	nameChanged:RSrefNameEvent displayPropertiesChanged:RSrefDisplayEvent \ 
	linkChanged:RSrefLinkEvent modelStructured:RSrefModelStructuredEvent \ 
	controllerOtherEvent:RSrefControllerEvent

-----------------------------------------------------------------------------
-- FILE CALLBACK FUNCTIONS:
-----------------------------------------------------------------------------
struct RSrefFileCallbacksStruct
(
	fn filePreOpenProc = 
	(
		
		if RSrefFileCallbackDebugPrint do (format "[CALLBACK] RSrefFileCallbacks.filePreOpenProc %\n" Param)

		RSrefMidFileEvent = True
		RSmaxfilingNow = True
		undo off 
		(
			destroyDialog RSrefBrowserRoll
			
			-- Resets RSref data before new file loads
			RSrefFuncs.reset debugFuncSrc:"filePreOpenProc()"
		)

		if RSrefFileCallbackDebugPrint do (format "[CALLBACK FINISHED] RSrefFileCallbacks.filePreOpenProc %\n" (Param as string))
		OK
	),
	-- Triggered after all maxfile-open processes are completed
	fn filePostOpenProc = 
	(
		local Param = (callbacks.notificationParam())
		if RSrefFileCallbackDebugPrint do (format "[CALLBACK] RSrefFileCallbacks.filePostOpenProc %\n" param)

		local OpenTimeStart = TimeStamp()
		undo off 
		(
			local storeObjs = (gRsRefStore.GetStoreNodes())
			storeObjs.loadedOnOpen = true -- Allows us to track which store-nodes were brought in via merges later
			
			-- Make sure stores are hidden and frozen:
			storeObjs.isHidden = True
			storeObjs.isFrozen = True
			
			-- Get object mesh/material last-changed times from data-node and restore their arrays
			-- (these are updated by node-callback RSNodeEventCallback)
			local objDataNode = getNodeByName RSrefData.objDataStoreNodeName
			if isValidNode objDataNode do 
			(
				objDataNode.RetrieveTimes()
				if RSrefDelStoresOnLoad do (delete objDataNode)
			)
			
			if RSrefScriptsOutOfDate do 
			(
				messageBox ("This file, or one of the files it references, was saved with a newer version of the RSref scripts.\n\nYour RSref version is " + RSrefVersion as string +"\n\nSome data may be lost if you save this file now, please update the tools before continuing!") title:"Your tools are out-of-date"
				RSrefScriptsOutOfDate = false
			)
			
			RSrefFuncs.loadRSrefDataOnRedraw debugFuncSrc:"RSrefFileCallbacks.filePostOpenProc"
		)
		RSmaxfilingNow = False
		RSrefMidFileEvent = False

		-- Ensure that the redraw-event is triggered after opening
		completeRedraw()
		
		-- Show the Display Settings warning-rollout, if required
		RSrefSettingFuncs.showRSrefSettings autoOpen:true atMouse:false
		setSaveRequired false
		
		if RSrefFunctionDebug do (format "[RSref post-open process took % seconds]\n" ((timeStamp() - OpenTimeStart) / 1000.0))
		OK
	),
	
	-- True for filenames that are allowed to have cached RSref data:
	fn isSuitableForCaching filename = 
	(
		-- Don't save data if this is turned off:
		RSrefAllowUpdate and 
		-- Don't run for containers:
		not (matchPattern filename pattern:"*.maxc") and 
		-- Don't run for backups, but allow for The Hold:
		(
			(matchPattern filename pattern:"*.mx") or 
			not (matchPattern filename pattern:((GetDir #autoBack) + "\*.*"))
		) and 
		(
			-- Don't run if savefile exists and is read-only; save will fail after this callback anyway, if that's the case
			not (doesFileExist filename) or 
			not (getFileAttribute filename #readOnly)
		)
	),
	
	fn filePreSaveProc = 
	(
		local Param = (callbacks.notificationParam())
		if RSrefFileCallbackDebugPrint do (format "[CALLBACK] RSrefFileCallbacks.filePreSaveProc %\n" Param)

		undo off 
		(
			RSrefMidFileEvent = True
			RSmaxfilingNow = True
			local saveFilename = Param[2]
			local saveCache = isSuitableForCaching saveFilename
			
			if RSrefFileCallbackDebugPrint and not saveCache do (format "\t(filename not suitable for cache-save)\n" param)
			
			if saveCache do with redraw off 
			(
				local SaveTimeStart = TimeStamp()
				
				-- Put RSrefObjects into instance-groups, if matched by objectname and attributes:
				RSrefFuncs.instantiateRefs debugFuncSrc:"[RSrefFileCallbacks.filePreSaveProc]"
				
				-- Remove existing data-store nodes
				delete (gRsRefStore.GetStoreNodes())

				-- Create temporary data-store nodes if relevant conditions are met:
				local storeAcreated = (gRsRefStore.createChangeStoreObj() != undefined)
				local storeBcreated = (gRsRefStore.createRSrefStoreObj() != undefined)
				
				if RSrefFunctionDebug and (storeAcreated or storeBcreated) do 
				(
					format "[RSref data-store process took % seconds]\n" ((timeStamp() - SaveTimeStart) / 1000.0)
				)
			)
		)
		OK
	),
	fn filePostSaveProc = 
	(
		local Param = (callbacks.notificationParam())
		if RSrefFileCallbackDebugPrint do (format "[CALLBACK] RSrefFileCallbacks.filePostSaveProc %\n" param)

		undo off
		(
			RSmaxfilingNow = False
			local saveFilename = Param[2]
			local saveCache = isSuitableForCaching saveFilename
			
			if RSrefFileCallbackDebugPrint and not saveCache do (format "\t(filename not suitable for cache-save)\n" param)
			
			if saveCache do with redraw off 
			(
				-- Remove temporary data-store nodes
				delete (gRsRefStore.GetStoreNodes())
				setSaveRequired False
			)
			RSrefMidFileEvent = False
		)

		OK
	),
	fn objectXrefPreMerge = 
	(
		local Param = (callbacks.notificationParam())
		if RSrefFileCallbackDebugPrint do (format "[CALLBACK] RSrefFileCallbacks.objectXrefPreMerge %\n" param)

		-- Turns off Xref records before they have a chance to be loaded
		if RSrefTurnOffXrefs do 
		(
			for n = 1 to objXRefMgr.recordCount do
			(
				format "[Turning off Xref record %]\n" n
				(objXRefMgr.GetRecord n).enabled = false
			)
		)

		if RSrefFileCallbackDebugPrint do (format "[CALLBACK FINISHED] RSrefFileCallbacks.objectXrefPreMerge %\n" param)
	),
	fn objectXrefPostMerge = 
	(
		local Param = (callbacks.notificationParam())
		if RSrefFileCallbackDebugPrint do (format "[CALLBACK] RSrefFileCallbacks.objectXrefPostMerge %\n" param)
	),
	fn filePreMerge = 
	(
		local Param = (callbacks.notificationParam())
		if (Param == 1) do (return False) -- Abort if triggered by Xref-system merge
		
		if RSrefFileCallbackDebugPrint do (format "[CALLBACK] RSrefFileCallbacks.filePreMerge %\n" (Param as string))
		RSrefMidFileEvent = True
			
		undo off 
		(
			if not RSmaxfilingNow and not RSnonUserMerge do 
			(
				if RSrefFileCallbackDebugPrint do (format "\t[user/container pre-merge]\n")
				RSrefData.preMergeRefs = RSrefFuncs.getRefObjs()
			)
		)

		if RSrefFileCallbackDebugPrint do (format "[CALLBACK FINISHED] RSrefFileCallbacks.filePreMerge %\n" (Param as string))
		OK
	),
	fn filePostMerge = 
	(
		local Param = (callbacks.notificationParam())
		if (Param == 1) do (return False) -- Abort if triggered by Xref-system merge
		
		if RSrefFileCallbackDebugPrint do 
		(
			format "[CALLBACK] RSrefFileCallbacks.filePostMerge %\n" (Param as string)
			format "\tRSmaxfilingNow:% RSnonUserMerge:% RSrefData.dontLoadData:%\n" RSmaxfilingNow RSnonUserMerge RSrefData.dontLoadData
		)

		undo off 
		(
			local isDataMerge = (RSmaxfilingNow or RSnonUserMerge)
			
			-- Don't trigger this during a data-merge, or if Rsrefs are deactivated:
			if (not isDataMerge) and (not RSrefData.dontLoadData) do 
			(
				if RSrefFileCallbackDebugPrint do (format "\t[user/container post-merge]\n")

				RSmaxfilingNow = True -- Turn this on to stop objects from drawing as soon as they're queried

				local preMergeRefs = RSrefData.preMergeRefs
				local postMergeRefs = RSrefFuncs.getRefObjs()
				local mergedRefObjs = for obj in postMergeRefs where ((findItem preMergeRefs obj) == 0) collect obj

				if RSrefFileCallbackDebugPrint do 
				(
					format "\t% new RSref objects merged in\n" mergedRefObjs.count
					--for obj in mergedRefObjs do (format "\t\t%\n" obj.name)
				)

				if (mergedRefObjs.count != 0) do 
				(
					-- If RSref database is loaded, get up-to-date models from merged objects if they've not been loaded by scene yet:
					if (not RSrefData.notLoaded) do 
					(
						local foundNames = #()
						local refDef
						for obj in mergedRefObjs where (findItem foundNames obj.objectname) == 0 do 
						(
							refDef = RSrefFuncs.getRefDefByName obj.objectname
							
							if refDef != undefined do 
							(
								obj.refDef = refDef
								
								if refDef.meshOutOfDate and (not obj.showAltMesh) and (refDef.meshTime == obj.meshTime) do 
								(
									refDef.meshObj = obj.meshObj
									refDef.meshOutOfDate = false

									if RSrefFileCallbackDebugPrint do (format "\t\tGetting updated RSref model from merged object: % (% == %)\n" obj.name refDef.meshTime obj.meshTime)
								)
								if refDef.matOutOfDate and (refDef.matTime == obj.matTime) do 
								(
									refDef.material = obj.material
									refDef.matOutOfDate = false
									
									if RSrefFileCallbackDebugPrint do (format "\t\tGetting updated RSref material from merged object: % (% == %)\n" obj.name refDef.matTime obj.matTime)
								)
							)
							append foundNames obj.objectname
						)
					)
					
					-- Set missing RSref data to load at the next viewport-redraw
					RSrefFuncs.loadRSrefDataOnRedraw debugFuncSrc:"RSrefFileCallbacks.filePostMerge"
				)
			)
			
			RSmaxfilingNow = False
			RSrefMidFileEvent = False
		)

		if RSrefFileCallbackDebugPrint do (format "[CALLBACK FINISHED] RSrefFileCallbacks.filePostMerge %\n" (Param as string))
		OK
	),
	fn fileReset = 
	(
		local Param = (callbacks.notificationParam())
		if RSrefFileCallbackDebugPrint do (format "running: RSrefFileCallbacks.fileReset %\n" param)
		undo off 
		(
			destroyDialog RSrefBrowserRoll
			unregisterRedrawViewsCallback RSrefFuncs.startLoadRSrefData
			enableSceneRedraw()
			RSrefFuncs.reset debugFuncSrc:"fileReset()"
		)
		OK
	),
	-- Allows objects to know that they don't need to redraw
	fn preSystemShutdown = 
	(
		local Param = (callbacks.notificationParam())
		if RSrefFileCallbackDebugPrint do (format "[CALLBACK] RSrefFileCallbacks.preSystemShutdown %\n" (Param as string))
		RSrefShuttingDown = true

		callbacks.removeScripts id:#RSrefs
		--resetMaxFile #noPrompt
	),
	fn setupFileCallbacks = 
	(
		callbacks.removeScripts id:#RSrefs
		if RSrefUseFileCallbacks do 
		(
			callbacks.addScript #filePreOpenProcess		"RSrefFileCallbacks.filePreOpenProc()"			id:#RSrefs
			callbacks.addScript #filePostOpenProcess		"RSrefFileCallbacks.filePostOpenProc()"			id:#RSrefs

			callbacks.addScript #filePreSaveProcess 		"RSrefFileCallbacks.filePreSaveProc()"			id:#RSrefs
			callbacks.addScript #filePostSaveProcess		"RSrefFileCallbacks.filePostSaveProc()"			id:#RSrefs

			callbacks.addScript #objectXrefPreMerge		"RSrefFileCallbacks.objectXrefPreMerge()"		id:#RSrefs
			callbacks.addScript #objectXrefPostMerge	"RSrefFileCallbacks.objectXrefPostMerge()"	id:#RSrefs

			callbacks.addScript #filePreMerge				"RSrefFileCallbacks.filePreMerge()"				id:#RSrefs
			callbacks.addScript #filePostMerge				"RSrefFileCallbacks.filePostMerge()"				id:#RSrefs

			callbacks.addScript #systemPostReset			"RSrefFileCallbacks.fileReset()"					id:#RSrefs
			callbacks.addScript #systemPostNew			"RSrefFileCallbacks.fileReset()"					id:#RSrefs
			
			callbacks.addScript #preSystemShutdown	"RSrefFileCallbacks.preSystemShutdown()"	id:#RSrefs
		)
	)
)

global RSrefFileCallbacks = RSrefFileCallbacksStruct()
RSrefFileCallbacks.setupFileCallbacks()

-- Callback used when objects are cloned, to keep Rsrefs as instances, and to transfer some attributes:
global RsRefReInstancing = #()

fn RsRefCloneInstancer = 
(
	unregisterRedrawViewsCallback RsRefCloneInstancer
--print "COPY CALLBACK CALLBACK"
	
	suspendEditing()
	RSrefData.allowAttachToNode = false
	
	for item in RsRefReInstancing do 
	(
		local original = item.original
		local clone = item.clone
		
		if (isValidNode clone) and (isValidNode original) do 
		(
			--format "Instancing: % -> %\n"  original.name clone.name
			
			instanceReplace clone original
			
			local getVal = getAttr original idxTint

			if (getVal != 0) do 
			(
				setAttr clone idxTint getVal
			)
		)
	)
	
	RSrefData.allowAttachToNode = true
	resumeEditing()
	
	RsRefReInstancing = #()
)

fn RsRefClonedCallback = 
(
	local Params = (callbacks.notificationParam())
	
	-- We don't care if the objects are being instanced, only if they're being copied:
	if not (RSrefAllowUpdate) or (params[3] != 0) or (RsRefReInstancing.count != 0) do return OK

	unregisterRedrawViewsCallback RsRefCloneInstancer
	local preClones = params[1]
	local postClones = params[2]
	
	for objNum = 1 to preClones.count do 
	(
		local objA = preClones[objNum]
		
		if (isRSref objA includeDelegates:true) do 
		(
			local objB = postClones[objNum]
			
			append RsRefReInstancing (dataPair original:objA clone:objB)
		)
	)
	
	if (RsRefReInstancing.count != 0) do 
	(
		registerRedrawViewsCallback RsRefCloneInstancer
	)
)

callbacks.removeScripts id:#RSrefsCopy
callbacks.addScript #postNodesCloned "RSrefClonedCallback()" id:#RSrefsCopy
