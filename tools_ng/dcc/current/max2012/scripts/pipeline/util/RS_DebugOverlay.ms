-- Debug Overlay Tools
-- Neal D Corbett, R*Leeds
-- 12/10/2010

try (destroyDialog RSLDSdebugOverlayRoll) catch ()
unregisterRedrawViewsCallback RSLDSdrawOverlayNumsCallback

-- Debug functions for drawing vert/edge/face-numbers
global RSLDSdrawSubObjNumsSelected = true
fn RSLDSdrawSubObjNums = 
(
	if (subObjectLevel != 0) do 
	(
		gw.setTransform (Matrix3 1)
		for ThisObj in selection do 
		(
			case of
			(
				(isKindOf ThisObj Editable_Poly):
				(
					case (getSelectionLevel ThisObj) of 
					(
						#vertex:
						(
							local toLabel = 
							(
								if RSLDSdrawSubObjNumsSelected then 
								(
									polyop.getVertsByFlag ThisObj 1
								)
								else 
								(
									#{1..ThisObj.numVerts}
								)
							)
							
							for n = toLabel do
							(
								gw.wtext (gw.wtranspoint (polyop.getvert ThisObj n)) (n as string)
							)
						)
						#edge:
						(
							local toLabel = 
							(
								if RSLDSdrawSubObjNumsSelected then 
								(
									polyop.getEdgesByFlag ThisObj 1
								)
								else 
								(
									#{1..ThisObj.edges.count}
								)
							)

							for n = toLabel do
							(
								EdgeVerts = polyop.getEdgeVerts ThisObj n
								gw.wtext \
								(
									gw.wtranspoint (((polyop.getvert ThisObj EdgeVerts[1]) + (polyop.getvert ThisObj EdgeVerts[2])) * 0.5) 
								) (n as string)
							)
						)
						#face:
						(
							local toLabel = 
							(
								if RSLDSdrawSubObjNumsSelected then 
								(
									polyop.getFacesByFlag ThisObj 1
								)
								else 
								(
									#{1..ThisObj.numFaces}
								)
							)
							
							for n = toLabel do
							(
								gw.wtext (gw.wtranspoint (polyop.getFaceCenter ThisObj n)) (n as string)
							)
						)
					)
				)
				(isKindOf ThisObj Editable_mesh):
				(
					case (getSelectionLevel ThisObj) of 
					(
						#vertex:
						(
							local toLabel = 
							(
								if RSLDSdrawSubObjNumsSelected then 
								(
									getVertSelection ThisObj
								)
								else 
								(
									#{1..ThisObj.numVerts}
								)
							)
							
							for n = toLabel do
							(
								gw.wtext (gw.wtranspoint (getvert ThisObj n)) (n as string)
							)
						)
						#edge:
						(
							local toLabel = 
							(
								if RSLDSdrawSubObjNumsSelected then 
								(
									getEdgeSelection ThisObj
								)
								else 
								(
									#{1..ThisObj.edges.count}
								)
							)
							
							for n = toLabel do
							(
								EdgeVerts = (meshop.getVertsUsingEdge ThisObj n) as array
								gw.wtext \
								(
									gw.wtranspoint (((getvert ThisObj EdgeVerts[1]) + (getvert ThisObj EdgeVerts[2])) * 0.5) 
								) (n as string)
							)
						)
						#face:
						(
							local toLabel = 
							(
								if RSLDSdrawSubObjNumsSelected then 
								(
									getFaceSelection ThisObj
								)
								else 
								(
									#{1..ThisObj.numFaces}
								)
							)
							
							for n = toLabel do 
							(
								gw.wtext (gw.wtranspoint (meshop.getFaceCenter ThisObj n)) (n as string)
							)
						)
					)
				)
			)
		)
	)
)
fn RSLDSdrawOverlayNumsCallback = (RSLDSdrawSubObjNums())

rollout RSLDSdebugOverlayRoll "Debug Overlay"
(
	checkbutton showSubObjNumsBtn "Show SubObject Nums" width:RSLDSdebugOverlayRoll.width height:30 pos:[0,0]
	radiobuttons selOrAllSubObjsBtns "Label Subobjects:" labels:#("Selected", "All") default:(if RSLDSdrawSubObjNumsSelected then 1 else 2) align:#left
	
	on ShowSubObjNumsBtn changed checked do 
	(
		if checked then (registerRedrawViewsCallback RSLDSdrawOverlayNumsCallback)
		 else (unregisterRedrawViewsCallback RSLDSdrawOverlayNumsCallback)
		completeRedraw()
	)
	
	on selOrAllSubObjsBtns changed num do  
	(
		RSLDSdrawSubObjNumsSelected = (num == 1)
		
		if ShowSubObjNumsBtn.checked do 
		(
			completeRedraw()
		)
	)

	on RSLDSdebugOverlayRoll close do 
	(
		unregisterRedrawViewsCallback RSLDSdrawOverlayNumsCallback
		completeRedraw()
	)
)
createDialog RSLDSdebugOverlayRoll