--
-- tcs helper functions

global gTcsTools

struct sTcsTools 
(
	tcsHelper = dotnetclass "RSG.MaxUtils.Textures.TCSHelper",

	fn CheckTcsFormat tcsFile texMapFilename outputFile = 
	(
		local tcsFileExistsLocally = RsFileExist tcsFile
		local tcsFileHasResourceTexturesElement = tcsFileExistsLocally and (tcsHelper.HasResourceTexturesElement tcsFile)

		-- If the TCS is missing the resourceTextures element we need to regenerate it
		if( tcsFileExistsLocally and (not tcsFileHasResourceTexturesElement) ) then
		(
			gRsUlog.LogMessage ("Adding Resource texture Element "+outputFile as string+" to "+tcsFile as string)
		
			try(
				tcsHelper.GenerateResourceTexturesElement tcsFile #(texMapFilename) outputFile
			)catch(
				gRsUlog.LogError ("Texture template resourceTexture element creation failed for tcs:"+tcsFile as string+" \ntexmap:"+texMapFilename as string+"\nand dds:"+outputFile as string+"\n due to dotnet exception:"+(getCurrentException()))
			)
		)

		-- Check to make sure our resource texture is the same format as the one specified in the TCS file
		local tcsFileHasTextureCompressionFormat = tcsHelper.HasTextureCompressionFormatElement tcsFile
		if (tcsFileHasTextureCompressionFormat) then
		(
			local tcsTextureCompressionFormat = tcsHelper.GetTextureCompressionFormat tcsFile
			
			-- format "TCS file:(%) has compressed image format:(%) set\n" tcsFile tcsTextureCompressionFormat

			if (0 != stricmp tcsTextureCompressionFormat "Unknown") then
			(
				local ddsHeader_class = dotnetclass "RSG.Base.Textures.DDS.Header"
				local exportTextureCompressedFormat = "Unknown"

				try
				(
					local extension = ( getFilenameType outputFile )
					case extension of
					(
						".dds":
						(
							local exportTextureHeader = ddsHeader_class.Load outputFile
							exportTextureCompressedFormat = exportTextureHeader.PixelFormat.CompressedFormat.ToString()
						)
						default:
						(
							exportTextureCompressedFormat = "None"
						)
					) 
				)
				catch
				(
					gRsUlog.LogWarning("Unable to get compressed texture format for " + outputFile as string + " " + getCurrentException())
				)

				-- format "Texture:(%) uses compressed image format:(%)\n" outputFile exportTextureCompressedFormat

				if (0 != stricmp tcsTextureCompressionFormat exportTextureCompressedFormat) then
				(
					local logMessage = stringStream ""
					format "Texture (%) uses compressed texture format (%) but the .tcs file (%) uses (%). https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Conflicting_compression_formats" \
						outputFile exportTextureCompressedFormat tcsFile tcsTextureCompressionFormat to:logMessage
				
					gRsUlog.LogError (logMessage as String)
				)
			)
		)	
	)
)

if undefined==gTcsTools then
	gTcsTools = sTcsTools()