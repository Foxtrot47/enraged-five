-- Opens an Explorer window with Filename selected, or sub-file filenameInZip if specified
fn RSshowInExplorer filename filenameInZip: silent:false = 
(
	-- Fail if file wasn't found:
	if not doesFileExist filename do 
	(
		if not silent do 
		(
			messageBox ("File not found:\n" + filename) title "Warning: File not found"
		)
		return false
	)
	
	local selectFilename = RsMakeBackSlashes filename
	local foundInZip = false
	
	if (filenameInZip != unsupplied) do 
	(
		-- Fail if sub-file wasn't in zipfile, but select the zip instead:
		local zipFile = (dotNetClass "Ionic.Zip.ZipFile").Read filename
		foundInZip = zipfile.containsEntry filenameInZip
		zipFile.dispose()
		
		if foundInZip then 
		(
			append selectFilename ("\\" + (RsMakeBackSlashes filenameInZip))
		)
		else if not silent do 
		(
			messageBox ("File not found in " + (filenameFromPath  filename) + ":\n" + filenameInZip + "\n\n(Showing zip instead)") title:"Warning: File not found in zip"
		)
	)
	
	local explorerArgs = "/select," + selectFilename
	
	if not silent do 
	(
		format "explorer.exe %\n" explorerArgs	
	)
	shellLaunch "explorer.exe" explorerArgs
	
	foundInZip
)

-- Provides the current Maxfile's export-zip filename:
fn RSgetMapExportZip silent:false = 
(
	-- Is this an exportable map?
	local mapName = getFilenameFile (toLower maxfilename)
	local exportMap = RsProjectContentFind mapName type:"mapzip"
	
	if exportMap == undefined then 
	(
		if not silent do 
		(
			messageBox "This maxfile doesn't export assets!" title:"Warning: Non-export maxfile"
		)
		
		return undefined
	)
	else 
	(
		exportMap.path + "/" + mapName + ".zip"
	)
)

-- Selects the current Maxfile's export-zip in Explorer:
fn RSshowFileExportInExplorer = 
(
	local exportFilename = RSgetMapExportZip()
	
	if (exportFilename != undefined) do 
	(
		RSshowInExplorer exportFilename
	)
)

-- Shows an object's export-file in Explorer, inside it's map's zipfile:
fn RSshowObjExportInExplorer objs silent:false = 
(
	if (objs.count != 1) do
	(	
		if not silent do 
		(
			if (objs.count == 0) then 
			(
				messageBox "Please select an object first" title:"Warning: No object selected"
			)
			else 
			(
				messageBox "Please select only one object" title:"Warning: Too many objects selected"
			)
		)

		return false
	)
	
	local exportFilename = RSgetMapExportZip silent:silent
	
	if (exportFilename != undefined) do 
	(
		RSshowInExplorer exportFilename filenameInZip:(objs[1].name + ".idr.zip") silent:silent
	)
)

-- Shows an object's TXD's export-file in Explorer, inside it's map's zipfile:
fn RSshowTXDExportInExplorer objs silent:false = 
(
	local TXDnames = #()
	
	for obj in objs do 
	(
		objClass = GetAttrClass obj
		
		if (objClass != undefined) do 
		(
			txdIdx = getAttrIndex objClass "TXD"
			
			if (txdIdx != undefined) do 
			(
				appendIfUnique TXDnames (getAttr obj txdIdx)
			)
		)
	)
	
	if (TXDnames.count != 1) do
	(	
		if not silent do 
		(
			if (TXDnames.count == 0) then 
			(
				messageBox "Please select an object with a TXD" title:"Warning: No TXDs on selection"
			)
			else 
			(
				messageBox "Please select objects with only one TXD set" title:"Warning: Too many TXDs selected"
			)
		)

		return false
	)
	
	local exportFilename = RSgetMapExportZip silent:silent
	
	if (exportFilename != undefined) do 
	(
		RSshowInExplorer exportFilename filenameInZip:(TXDnames[1] + ".itd.zip") silent:silent
	)
)
