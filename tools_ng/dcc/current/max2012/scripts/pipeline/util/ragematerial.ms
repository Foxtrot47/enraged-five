-- Rockstar Material Functions
-- Rockstar North
-- 8/5/2009
-- by Luke Openshaw

-- utilities for DX rage shader materials

--filein "rockstar/util/material.ms" -- loaded on startup by startup.ms

-----------------------------------------------------------------
-- Build a list of Rage materials that are applied to an array of objects
-----------------------------------------------------------------
fn RsGetRageMaterialsAppliedToObjs objs = 
(
	local matlist = #()
	
	for obj in objs do 
	(
		for mat in (RSgetMatClassSubMats obj.material Rage_Shader) do (appendIfUnique matList mat)
	)
	return matlist
)

-----------------------------------------------------------------
-- Redraw all shaders
-----------------------------------------------------------------
fn RstRefreshShader m = 
(
	if rage_shader == classof m then
	(
		RstRefreshMtl m
	)
	else if multimaterial == classof m then
	(
		for mtl in m.materiallist do
		(
			RstRefreshShader mtl
		)
	)
)
fn RstUpdateRageShaders = 
(
	for m in scenematerials do
	(
		RstRefreshShader m
	)
)

-----------------------------------------------------------------
-- Convert 
-----------------------------------------------------------------
fn RsConvertStdMaterialsOnSelected objs = (
	matlist = #()
	for obj in objs do (
		if classof obj.material == Standardmaterial then (
			newMat = RstCreateRstMtlFromStdMtl obj.mat
			if newmat != undefined then obj.mat = newMat
		)
		else if classof obj.material == MultiMaterial then (
			idx = 1
			mapList = #()
			RsMatGetMapIdsUsedOnObjectWithCount obj mapList
			for submat in obj.material.materiallist do (
				if classof submat == Standardmaterial then (
					if (finditem mapList idx) != 0 then (
						newMat = RstCreateRstMtlFromStdMtl submat
						if newMat != undefined then obj.material.materiallist[idx] = newMat
					)
					else print ("id "+(idx as string)+" is not used on object")
				)
				else print ("number "+(idx as string)+" is not a standardmaterial")
				
				idx = idx + 1
			)
		)
	)
)

-----------------------------------------------------------------
-- Build a list of materials that are applied to selected faces
-----------------------------------------------------------------
fn RsGetMaterialsAppliedToFaces allFaces:false = (
	matlist = #()
	if selection.count != 1 then (
		messagebox "Multiple or no objects selected"
		return false
	)

	obj = selection[1]
	mat = obj.mat


	-- If the object only has one material applied to it then that is the only
	-- material we need to operate on.  Othersiwe work out which materials
	-- in the MultiMaterial need to have the textures scaled
	if classof mat == Rage_Shader then append matlist mat
	else if classof mat == MultiMaterial then 
	(
		matidlist = #() -- List of participant material ids
		
		if (classof obj)==Editable_mesh then
		(
			polysel = getFaceSelection obj
			if allFaces then 
				polysel = obj.faces as bitarray

			for polyidx in polysel do 
			(
				matidx = getFaceMatID obj polyidx
				appendIfUnique matidlist matidx
			)
		)
		else
		(
			polysel = polyop.getFaceSelection obj
			if allFaces then 
				polysel = obj.faces as bitarray

			for polyidx in polysel do (
				matidx = polyop.getFaceMatID obj polyidx
				appendIfUnique matidlist matidx
			)
		)

		for matidx in matidlist do (
			append matlist mat.materiallist[matidx]
		)
	)
	return matlist
)

-----------------------------------------------------------------
-- Set lighting on selected materials
-----------------------------------------------------------------
fn RsSetDxLightingOnMaterials matList state quiet:false = 
(
	local onOff = if state then "on" else "off"
	
	local startTime = TimeStamp()
	if not quiet do 
	(
		progressStart ("Turning material lighting " + onOff + ":")
	)
	
	local renderLevelWas = viewport.GetRenderLevel()
	viewport.SetRenderLevel #wireFrame
	
	local matNum = 0
	for mat in matList while quiet or progressUpdate (100.0 * matNum / matlist.count) do 
	(
		if ((RstGetLightingMode mat) != state) do 
		(
			RstSetLightingMode mat state
		)
		matNum += 1
	)
	
	viewport.SetRenderLevel renderLevelWas
	redrawViews()
	
	if not quiet do 
	(
		progressEnd()
		
		local timeTook = ((TimeStamp() - StartTime) / 1000.0) as string
		
		if (matNum == matlist.count) then 
		(
			messagebox ("Finished turning lighting " + onOff + " for " + (matlist.count as string) + " materials.\n\n (took " + timeTook + " seconds)") title:"Light-switching completed"
		)
		else 
		(
			messagebox ("Turned lighting " + onOff + " for " + (matNum as string) + " of " + (matlist.count as string) + " materials.\n\n (took " + timeTook + " seconds)") title:"Light-switching cancelled"
		)
	)
)

-----------------------------------------------------------------
-- Turn lighting on/off on selected object materials
-----------------------------------------------------------------
fn RsToggleDxLightingOnObjs objs state quiet:false = 
(
	local matlist = RsGetRageMaterialsAppliedToObjs objs
	
	RsSetDxLightingOnMaterials matlist state quiet:quiet
)

-----------------------------------------------------------------
-- Turn lighting on/off on selected face-materials
-----------------------------------------------------------------
fn RsToggleDxLightingOnFaces state quiet:false = 
(
	local matlist = RsGetMaterialsAppliedToFaces()
	
	RsSetDxLightingOnMaterials matlist state quiet:quiet
)

-----------------------------------------------------------------
-- Scale textures on selected materials
-----------------------------------------------------------------
fn RsScaleTexturesOnMaterials matList multiplier quiet:false = 
(
	local startTime = TimeStamp()
	if not quiet do 
	(
		progressStart "Scaling textures on materials"
	)
	
	local renderLevelWas = viewport.GetRenderLevel()
	viewport.SetRenderLevel #wireFrame
	
	local matNum = 0
	for mat in matList while quiet or progressUpdate (100.0 * matNum / matlist.count) do 
	(
		if (isKindOf mat Rage_Shader) do 
		(
			RstSetTextureScaleValue mat multiplier		
		)
		matNum += 1
	)
	
	viewport.SetRenderLevel renderLevelWas
	redrawViews()
	
	if not quiet do 
	(
		progressEnd()
		
		local timeTook = ((TimeStamp() - StartTime) / 1000.0) as string
		
		if (matNum == matlist.count) then 
		(
			messagebox ("Finished scaling textures on " + (matlist.count as string) + " materials.\n\n (took " + timeTook + " seconds)") title:"Scaling completed"
		)
		else 
		(
			messagebox ("Scaled textures on " + (matNum as string) + " of " + (matlist.count as string) + " materials.\n\n (took " + timeTook + " seconds)") title:"Scaling cancelled"
		)
	)
)
-- Temp alias for old mis-spelled function-name, for use by TechArt until RsScaleTexturesOnMaterials appears in label:
global RsScaleTexuresOnMaterials = RsScaleTexturesOnMaterials

-----------------------------------------------------------------
-- Scale textures on selected object materials
-----------------------------------------------------------------
fn RsScaleTexturesOnSelectedObjs multiplier objs quiet:false = 
(
	local matlist = RsGetRageMaterialsAppliedToObjs objs
	
	RsScaleTexturesOnMaterials matlist multiplier quiet:quiet
)

-----------------------------------------------------------------
-- Scale textures on selected face-materials
-----------------------------------------------------------------
fn RsScaleTexturesOnMaterialsForSelectedFaces multiplier quiet:false = 
(
	local matlist = RsGetMaterialsAppliedToFaces()
	
	RsScaleTexturesOnMaterials matlist multiplier quiet:quiet
)

fn RsSetHardwareRendering state mats: = 
(
	if (mats == unsupplied) then 
	(
		mats = for mat in getClassInstances rage_shader collect mat
	)
	else 
	(
		mats = for mat in mats where isKindOf mat rage_shader collect mat
	)
	
	for mat in mats do 
	(
		enablehardwarematerial mat state
		RstSetHwRenderMode mat state
	)

	forceCompleteRedraw()
)


