--
-- File:: pivot_reset.ms
-- Description:: Pivot transform reset functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 25 November 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
-- None

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: pivot_reset_transform
-- desc: Reset XForm for object and all children.
--
-- Do not override the default value of new_undo_context parameter as this is 
-- used to maintain a single undo context for the recursive nature of the 
-- function.
--
fn pivot_reset_transform obj new_undo_context:true = (

	if ( new_undo_context ) then
	(

		undo "Reset Transform" on
		(
			ResetTransform obj
			ResetScale obj

			-- Recurse
			if ( obj.children.count > 0 ) then 
			(
				for o in obj.children do
					pivot_reset_transform o new_undo_context:false
			)
		)
	)
	else
	(
	
		ResetTransform obj
		ResetScale obj

		-- Recurse
		if ( obj.children.count > 0 ) then 
		(
			for o in obj.children do
				pivot_reset_transform o new_undo_context:false
		)
	)
	
	true
)

-- pivot_reset.ms
