--
-- File:: hierarchy.ms
-- Description:: Useful selection functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 29 April 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
-- None

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsHierarchy_AllRootChildren
-- desc: Return all max nodes in hierarchy from a specified node.
--
-- An array of nodes is returned.
--
fn RsHierarchy_AllRootChildren n depth:0 = (

	local selset = #()
	append selset n
	
	-- Handle children and recurse.
	if ( ( undefined != n.children ) and ( n.children.count > 0 ) ) then
	(
		for child in n.children do
		(
			selset = selset + ( RsHierarchy_AllRootChildren child depth:(depth+1) )
		)
	)
	
	if ( 0 == depth ) then
		select selset
	
	return ( selset )
)


-- selection.ms
