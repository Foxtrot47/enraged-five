--
-- Terrain Utilities
-- This is a command ilne interface MaxScript to export a particular container exporting for this particular file.
--
--
filein "pipeline/export/maps/globals.ms"

filein "pipeline/util/terrain_utilities.ms"

filein "rockstar/export/settings.ms"
filein "rockstar/export/maputil.ms"
filein "pipeline/export/maps/maptest.ms"
filein "pipeline/export/maps/mapsetup.ms"
filein "rockstar/export/mapbounds.ms"
filein "rockstar/export/mapblocks.ms"
filein "rockstar/export/map.ms"

filein "pipeline/export/maps/maptxd.ms"
filein "pipeline/export/maps/mapgeom.ms"
filein "rockstar/export/mapanim.ms"
filein "pipeline/export/maps/objects.ms"
filein "pipeline/export/maps/preview.ms"

filein "pipeline/export/ui/map_geometry_ui.ms"

container_variable = systemTools.getEnvVariable "MAX_CONTAINER_FILE"
container_node_file = "X:\\rdr3\\art\\Models\\Terrain\\i_04_.maxc"
if ( container_variable != undefined ) then
	container_node_file = container_variable

global RsMapBatchExport_newExport = True;
filein "pipeline/export/maps/mapBatchExport.ms";
map_filename = getFilenameFile container_node_file
RsMapBatchExportStart #(map_filename)

--Before finishing, write a temporary file marking that this process has completed.
tempFileName = (systemTools.getEnvVariable "RS_TOOLSROOT") + "\\tmp\\3dsmax_marker.txt"
tempFile = createFile tempFileName 
format "This is a marker text file to denote when a process in 3D Studio Max has finished." to:tempFile
close tempFile 

quitMAX #noprompt