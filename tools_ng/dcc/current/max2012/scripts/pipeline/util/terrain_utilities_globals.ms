--
-- Terrain Utilities Globals
--
--
--
struct RsTerrainGlobalsStruct
(
	MinBounds = undefined,
	MaxBounds = undefined,
	
	CollisionBoundsArray = #(),
	TerrainMeshBoundsArray = #(),
	
	ContainerPosition = undefined,
	
	VegetationData = undefined,
	
	EdgeNormalisationThreshold = 0.01,
	VertexMatchingThreshold = 0.01,
	VertexBoundaryThreshold = 0.1,
	
	ProcMaterialName = "VegetationDropper",
	ProcGrassMapChannel = 27,
	
	DropModeRsRefs = "RsRefs",
	DropModeCollisionPainter = "CollisionPainter",
	DropModeRexBoundMtl = "RexBoundMtl",
	
	EnableDebugOutput = false,
	
	UniversalLog = undefined
)

global RsTerrainGlobals = RsTerrainGlobalsStruct()