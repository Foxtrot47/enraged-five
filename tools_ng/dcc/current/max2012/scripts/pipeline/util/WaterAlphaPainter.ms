filein "pipeline/util/scene.ms"

-- macroScript Water_Alpha_Painter 
-- 	category:"Rs Utils" 
-- 	tooltip:"Water Alpha Painter"

global gWaterPaintToolInstance = undefined
global WaterAlphaPaintRoll = undefined

tool Water_Alpha_Painter --"Water Alpha Painter"
(
	local paintingMode = off
	local paintBucket = undefined
	local toolCircle = undefined
	local radius = 10.0
	local mouseScreenSavePos = undefined
	local mouseGridSavePos = undefined
	local intensity = 1.0
	local isAddtitive = true
	local WaterAlphaPaintRoll = undefined
	
	fn toggleActive = 
	(
		paintingMode = not paintingMode
		if paintingMode then
			start()
		else
			stop()
	)
	
	fn GetColour = 
	(
		if isAddtitive then
			return white
		else
			return red
	)
	
	fn getIntensity pos = 
	(
		local factor = distance pos toolCircle.pos
		factor = factor / radius
		return (1.0 - factor) * intensity
	)
	
	fn Draw = 
	(
		for obj in objects 
			where classof obj == GtaWater
			do
		(
			local m = obj.getmesh()
			for vi=1 to getnumverts m do
			(
				local v = getvert m vi
				v = obj.pos + v
				v.z = 0
-- 				if 1==vi then
-- 				(
-- 					print ("v:"+v as string)
-- 					print ("toolCircle.pos:"+toolCircle.pos as string)
-- 					print ("distance:"+(distance v toolCircle.pos) as string)
-- 				)
				if (distance v toolCircle.pos)<radius then
				(
					local alphaIndex = vi-1
					if isAddtitive then
						obj.setAlphaFloat alphaIndex ((obj.getAlphaFloat alphaIndex) + getIntensity v)
					else
						obj.setAlphaFloat alphaIndex ((obj.getAlphaFloat alphaIndex) - getIntensity v)
				)
			)
		)
	)
	
	fn GetMouseDiff &value divident= 
	(
		if undefined!=mouseScreenSavePos then
		(
			value = value - (mouse.screenpos.y - mouseScreenSavePos.y)/divident
			if intensity<0 then 
				intensity = 0.0
			else if intensity>1 then
				intensity = 1.0
			::WaterAlphaPaintRoll.spnIntens.value = intensity
		)
		if undefined==mouseGridSavePos then
			mouseGridSavePos = gridPoint

		mouseScreenSavePos = mouse.screenpos
		
	)
	
	fn CheckShift shiftDown = 
	(
		if shiftDown then
		(
			isAddtitive = false
			::WaterAlphaPaintRoll.radioPaintDel.state = 2
			mouseGridSavePos = toolCircle.pos
			delete toolCircle
			toolCircle = circle radius:radius pos:mouseGridSavePos wireColor:(GetColour() * intensity)
		)
		else
		(
			if not isAddtitive then
			(
				isAddtitive = true
				::WaterAlphaPaintRoll.radioPaintDel.state = 1
				mouseGridSavePos = toolCircle.pos
				delete toolCircle
				toolCircle = circle radius:radius pos:mouseGridSavePos wireColor:(GetColour() * intensity)
			)
		)
	)
	
	on freeMove do coordsys grid
	(
--			print ("hovering "+(random 1 255) as string)
-- 			if undefined==paintBucket then
-- 				paintBucket = RsGWPrimMgr.getPrim()
-- 			
-- 			paintBucket.AddCircle 50
-- 			paintBucket.setOrigin (transmatrix (gridPoint)) --RsMousePosGridIntersect()
-- 			paintBucket.Draw true

		if undefined==toolCircle then
			toolCircle = circle radius:radius wireColor:(GetColour() * intensity)
		
		CheckShift shiftKey
		
		toolCircle.pos = gridPoint
	)
	
	on mouseMove clickno do
	(
--		print ("painting "+(random 1 255) as string)
		
		if undefined==toolCircle then
			toolCircle = circle radius:50 wireColor:(GetColour() * intensity)

		-- non exclusive
		CheckShift shiftKey
		
		-- exclusive
		if ctrlKey then
		(
			GetMouseDiff &radius 2

			::WaterAlphaPaintRoll.spnRadius.value = radius
			delete toolCircle
			toolCircle = circle radius:radius pos:mouseGridSavePos wireColor:(GetColour() * intensity)
		)
		else if altKey then
		(
			GetMouseDiff &intensity 100
			if intensity<0 then 
				intensity = 0.0
			else if intensity>1 then
				intensity = 1.0
			::WaterAlphaPaintRoll.spnIntens.value = intensity
			delete toolCircle
			toolCircle = circle radius:radius pos:mouseGridSavePos wireColor:(GetColour() * intensity)
		)
		else
		(
			mouseGridSavePos = undefined
			mouseScreenSavePos = undefined
			toolCircle.pos = gridPoint
			
			if lButton then
				Draw()
		)
	)
	
	on mousePoint clickno do
	(
--		format "Point: %\n" clickno
		Draw()
	)
	
	on mouseAbort clickno do
	(
		if mButton then
			return #continue
		else
		(
			format "Abort: %\n" clickno
			delete toolCircle
			
			gWaterPaintToolInstance = undefined
			::WaterAlphaPaintRoll.btnPaint.state = off
		)
	)
	
	on start do
	(
		gWaterPaintToolInstance = this
		intensity = ::WaterAlphaPaintRoll.spnIntens.value
		radius = ::WaterAlphaPaintRoll.spnRadius.value
		isAddtitive = ::WaterAlphaPaintRoll.radioPaintDel.state==1 
	)
	
	on stop do
	(
		try( delete toolCircle ) catch()
		gWaterPaintToolInstance = undefined
		::WaterAlphaPaintRoll.btnPaint.state = off
	)		
)