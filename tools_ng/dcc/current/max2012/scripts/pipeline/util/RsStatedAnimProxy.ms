filein "pipeline/util/statedAnim.ms"

global RSstatedAnimProxyMat = undefined

fn RSgetStatedAnimProxyMat = 
(
	-- Only generates global material if statedAnimProxy objects are being used:
	if (RSstatedAnimProxyMat == undefined) do 
	(
		RSstatedAnimProxyMat = multiMaterial name:"StatedAnimProxyMaterial" numsubs:2
		RSstatedAnimProxyMat.materiallist[1] = standard diffuse:green
		RSstatedAnimProxyMat.materiallist[2] = standard diffuse:red
	)
	
	RSstatedAnimProxyMat
)

-- Using ViewCallback to set materials, as Max will hang if the object sets its material directly when merging:
global RsStatedAnimProxyMatsToSet = #()
unregisterRedrawViewsCallback RsStatedAnimProxyMatSet
fn RsStatedAnimProxyMatSet = 
(
	unregisterRedrawViewsCallback RsStatedAnimProxyMatSet
	(for obj in RsStatedAnimProxyMatsToSet where isValidNode obj collect obj).material = RSstatedAnimProxyMat
	RsStatedAnimProxyMatsToSet = #()
)

plugin SimpleObject statedAnimProxy
name:"StatedAnimProxy"
classID:#(0x21417f9b, 0x7d5e06d0)
invisible:true
category:"RS Objects"
( 
	parameters main rollout:params
	(
		nodeTab type:#maxObjectTab tabSizeVariable:true
		showBegin type:#boolean ui:showBeginCtrl default:true animatable:false
		showEnd type:#boolean ui:showEndCtrl default:true animatable:false
	)
	
	local firstBuild = true
	
	fn updateMyMesh = 
	(
		-- Clear mesh:
		setMesh mesh verts:#() faces:#()
		
		-- Attach start-mesh:
		local secondMatFaceIdStart = 1
		if showBegin then
		(
			local startProxy = nodeTab[1]
			if (isProperty startProxy "mesh") do
			(
				meshop.attach mesh startProxy.mesh deleteSourceNode:false
				for f=1 to mesh.faces.count do
				(
					setFaceMatID mesh f 1
				)
				secondMatFaceIdStart = (mesh.faces.count + 1)
			)
		)
		
		-- Attach end-mesh:
		if showEnd do 
		(
			local endProxy = nodeTab[2]
			if (isProperty endProxy "mesh") do
			(
				meshop.attach mesh endProxy.mesh deleteSourceNode:false
				for f=secondMatFaceIdStart to mesh.faces.count do
				(
					setFaceMatID mesh f 2
				)
			)
		)
		
		-- Using ViewCallback to set materials, as Max will hang if the object sets its material directly when merging:
		local theNode = (refs.dependents this)[1]
		if (isKindOf theNode statedAnimProxy) and (theNode.material != (RSgetStatedAnimProxyMat())) do 
		(
			unregisterRedrawViewsCallback RsStatedAnimProxyMatSet
			append RsStatedAnimProxyMatsToSet theNode
			registerRedrawViewsCallback RsStatedAnimProxyMatSet
		)
		
		firstBuild = false
	)
	
	fn attachMeshesRec &themesh root refPos = 
	(
		if "Gta Object" == getattrclass root then
		(
			local meshObj = copy root
			update meshobj
			meshObj.parent = undefined
			if meshObj.modifiers.count>0 then
			(
				collapseStack meshobj
			)
			meshObj.pos = meshObj.pos  - refPos
			meshObj.material = undefined
			attach themesh meshObj
		)		
		for c in root.children do
			attachMeshesRec &themesh c refpos
	)
	
	fn testNodeForAnimation targ= 
	(
		local formerSelection = selection
		select targ
		local foundAnim = gRsStatedAnimTool.selectionChanged()
		if foundAnim then
		(
			local theNode = (refs.dependents this)[1]
			local referenceState = gRsStatedAnimTool.currAnim.objectArray[1]
			local animmeshes = gRsStatedAnimTool.currAnim.getAnimationStates()
			for o in animmeshes do print o
			local modelMeshes = #()
			local startMesh = editable_mesh name:"tempStartObject" pos:[0,0,0]
			local animrange = gRsStatedAnimTool.getAnimationRange()
			at time animrange.start for amesh in animmeshes do
			(
				attachMeshesRec &startMesh amesh amesh.pos
			)
			modelMeshes[1] = startMesh
			
			local endMesh = editable_mesh name:"tempEndObject" pos:[0,0,0]
			at time animrange.end for amesh in animmeshes do
			(
				attachMeshesRec &endMesh amesh amesh.pos
			)
			modelMeshes[2] = endMesh

			nodeTab = modelMeshes
			delete modelMeshes
			
			updateMyMesh()
			local idxGroupName = getAttrIndex (getattrclass referenceState) "groupName"
			theNode.name = (getattr referenceState idxGroupName)+"_anim"
			theNode.pos = referenceState.pos
			select theNode
			return true
		)
		select formerSelection
		return false
	)
	
	rollout params "StatedAnim Parameters"
	(
		checkbox showbeginCtrl "Show begin state" checked:true
		checkbox showEndCtrl "Show end state" checked:true
		pickbutton pickAnimation "pick an animation state"
		
		on pickAnimation picked targ do
		(
			testNodeForAnimation targ
		)
	)
	on update do 
	(
		-- Trigger buildMesh:
		(showBegin = showBegin)
	)
	on buildMesh do
	(
		updateMyMesh()
	)
	
	tool create
	(
		on mousePoint click do coordsys screen
		(
			local worldRay = mapScreenToWorldRay viewPoint
			local alltargets = intersectRayScene worldRay
			if undefined!=alltargets and alltargets.count>0 then
			(
				local targ = alltargets[1][1] -- first hit, node part of result
				if testNodeForAnimation targ then 
					return #stop
			)
		)
	)
)
