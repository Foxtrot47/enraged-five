filein "pipeline/export/peds/pedExport.ms"
filein "pipeline/export/peds/utils.ms"
filein "pipeline/util/pedBatchExporter.ms"

setQuietMode true
gPedBatchExporter = RsPedBatchExporter()
loadMaxFile gRsBatchJobCurrentMaxFile
gPedBatchExporter.buildZips()