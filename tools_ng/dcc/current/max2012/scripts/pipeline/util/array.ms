-- Returns theArray as a token-separated string:
fn RsStringArray theArray token:"," property: = 
(
	if not ((isKindOf theArray array) or (isKindOf theArray point3)) do 
	(
		messagebox "First parameter to RsStringArray not an array or point3."
		return false
	)
	
	local arrayCount = if (isKindOf theArray array) then theArray.count else 3
	local arrayStream = stringStream ""
	
	local notFirst = false
	for n = 1 to arrayCount do 
	(
		if notFirst do 
		(
			format "%" token to:arrayStream
		)
		notFirst = true
		
		local val = theArray[n]
		if unsupplied!=property and hasProperty val property then
			format "%" (getProperty val property) to:arrayStream
		else
			format "%" val to:arrayStream
	)

	return (arrayStream as string)
)

-- Removes items in toSubtract from fromArray:
fn RsSubtractArray fromArray toSubtract = 
(
	local keepItems = #{}
	keepItems.count = fromArray.count
	
	for idx = 1 to keepItems.count do 
	(
		keepItems[idx] = (findItem toSubtract RsSubtractArray[idx] == 0)
	)
	
	return (for idx = keepItems collect fromArray[idx])
)
