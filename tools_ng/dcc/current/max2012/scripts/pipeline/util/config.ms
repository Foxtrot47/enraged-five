--
-- File:: pipeline/util/config.ms
-- Description:: Helper functions for dealing with RSG.Base.Configuration config objects.
--
-- Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
-- Date:: 19 October 2012
--

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
--fn	RsCheckConfigAtStartup
--desc	Checks that the user has a valid configuration.
--
fn RsCheckConfigAtStartup = 
(
	configChecker = dotnetclass "RSG.Base.Configuration.Util.Max"
	message = configChecker.ConfigVersionCheck(false)
	result = true
	
	if (message != undefined) then
	(
		message = "Exporting has been disabled as we were unable to create a valid configuration object.  You may continue to use 3ds max however you won't be able to export.\n\n" + message
		messageBox message title:"Exporting Disabled"
		result = false
	)
	
	result
)

--
--fn	RsCheckConfigForExport
--desc	Checks that the user has a valid configuration.
--
fn RsCheckConfigForExport =
(
	configChecker = dotnetclass "RSG.Base.Configuration.Util.Max"
	message = configChecker.ConfigVersionCheck(true)
	result = true
	
	if (message != undefined) then
	(
		message = "Unable to continue with export due to the following configuration exception:\n\n" + message
		messageBox message title:"Export Aborted"
		result = false
	)
	
	result
)