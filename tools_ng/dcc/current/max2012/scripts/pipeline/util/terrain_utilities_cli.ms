--
-- Terrain Utilities
-- This is a command ilne interface MaxScript to invoke the vegetation exporting for this particular file.
--
--
filein "pipeline\\util\\terrain_utilities.ms"

vegetationData = LoadVegetation()

if ( vegetationData != undefined ) then
(
	local exportDirectory = RsConfigGetExportDir()
	local levelName = getFilenameFile maxFileName
	local filePath = exportDirectory + "levels/rdr3/terrain/" + levelName + "_vegetation.ipl"  --TODO:  Hardcoded Level Name		
	
	ExportVegetation vegetationData filePath undefined false false
)

--Before finishing, write a temporary file marking that this process has completed.
tempFileName = (systemTools.getEnvVariable "RS_TOOLSROOT") + "\\tmp\\3dsmax_marker.txt"
tempFile = createFile tempFileName 
format "This is a marker text file to denote when a process in 3D Studio Max has finished." to:tempFile
close tempFile 

quitMAX #noprompt