RsFragBoundList = #()
RsTotalSurfProperty = ""
global RsBoundCheckErrors = false

--------------------------------------------------------------
-- setup the ragdoll version of the ped model's bounds elements
--------------------------------------------------------------		
fn RsSetupFragBoundsInner objnode rexroot pedDir compulsoryAttributes:undefined appendParent:false = (

	numBones = 0

	if objnode == undefined then (
	
		return 0
	)
	
	local allowedColltypes = #(Col_Capsule, Col_Mesh, Col_Box, Col_Sphere, Col_Cylinder)
	
	local hasAllCompulsoryAttributes = true
	if "Gta Collision"==(getattrclass objnode) and compulsoryAttributes!=undefined then
	(
		for a in compulsoryAttributes do
		(
			local index = getattrindex "Gta Collision" a[1]
			if a[2] != getattr objnode index then
			(
				hasAllCompulsoryAttributes = false
				gRsUlog.LogMessage ("Not appending bound "+objnode.name+" attribute "+a[1] as string+" has value "+(getattr objnode index) as string)
				exit
			)
		)
	)		

	if 0 != (findItem allowedColltypes (classof objnode)) and hasAllCompulsoryAttributes then (

		surfaceType = RsGetCollSurfaceTypeString objnode
		RsTotalSurfProperty = RsTotalSurfProperty + objnode.name + "=" + surfaceType + ":"
--		print ("append RsFragBoundList "+objnode.name)
		append RsFragBoundList objnode
		if appendParent and undefined!=objnode.parent then
			append RsFragBoundList objnode.parent
	) else (
	
		nameCheck = RsLowercase objnode.name
	
		subnameCheck = ""
		
		if nameCheck.count >= 3 then (
	
			subnameCheck = (substring nameCheck (nameCheck.count - 2) 3)
		)
	
		if (findstring namecheck "footsteps" == undefined) and (subnameCheck != "nub") then (
		
			numBones += 1
		)
		
--		print numBones
	)

	for childobj in objnode.children do (
	
		numBones += RsSetupFragBoundsInner childobj rexroot pedDir compulsoryAttributes:compulsoryAttributes appendParent:false
	)
	local clothCollChildren = #()
	RsSceneLink.GetChildren 3 objnode &clothCollChildren
	for childobj in clothCollChildren do (
	
		numBones += RsSetupFragBoundsInner childobj rexroot pedDir compulsoryAttributes:compulsoryAttributes appendParent:false
	)
	
	numBones
)


--------------------------------------------------------------
-- setup the ragdoll version of the ped model's bounds elements
--------------------------------------------------------------		
fn RsSetupFragBounds objnode rexroot pedDir compulsoryAttributes:undefined = (

	RsFragBoundList = #()

	ret = RsSetupFragBoundsInner objnode rexroot pedDir \
				compulsoryAttributes:compulsoryAttributes \
				appendParent:false
	
	if RsFragBoundList.count > 0 then (
	
		bound = rexAddChild rexroot selection[1].name "Bound"
		rexSetPropertyValue bound "OutputPath" pedDir
		rexSetPropertyValue bound "ForceExportAsComposite" "false"
		rexSetPropertyValue bound "ExportAsComposite" "false"
		rexSetPropertyValue bound "BoundZeroPrimitiveCentroids" "true"
		rexSetPropertyValue bound "BoundExportWorldSpace" ((RsCloth.IsCloth objnode) as string)
		rexSetPropertyValue bound "SurfaceType" RsTotalSurfProperty
		if RsBoundCheckErrors then
		(
			rexSetPropertyValue bound "BoundCheckFlags" "BOUNDCHECK_ERROR:1" -- BOUNDCHECK_ISTHIN:50|BOUNDCHECK_BADNORMAL:0.5|BOUNDCHECK_BADNEIGHBOR:1
		)
		gRsULog.LogMessage ("BOUNDS SELECTION: "+(RsFragBoundList as string))
		rexSetNodes bound exportnodes:(RsGetAnimHandleString RsFragBoundList) 
	)
	
	ret
)

--------------------------------------------------------------
-- setup the ragdoll version of the ped model's bounds elements
--------------------------------------------------------------		
fn RsSetupPreSelectedBounds rexroot pedDir savesel = (

	if savesel.count > 0 then (
	
		bound = rexAddChild rexroot selection[1].name "Bound"
		rexSetPropertyValue bound "OutputPath" pedDir
		rexSetPropertyValue bound "ForceExportAsComposite" "false"
		rexSetPropertyValue bound "ExportAsComposite" "false"
		rexSetPropertyValue bound "BoundZeroPrimitiveCentroids" "true"
		rexSetPropertyValue bound "BoundExportWorldSpace" "false"
		rexSetPropertyValue bound "SurfaceType" RsTotalSurfProperty
		gRsULog.LogMessage ("BOUNDS SELECTION: "+(savesel as string))
		rexSetNodes bound exportnodes:(RsGetAnimHandleString savesel) 
	)
	
	ret
)