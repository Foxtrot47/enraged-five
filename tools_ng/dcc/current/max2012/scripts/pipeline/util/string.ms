-- Rockstar string utilities
-- Rockstar North
-- 1/3/2005
-- by Greg Smith

-- various functions for transforming strings

----------------------------------------------------------------------------------------
-- removes all the spaces from a string
----------------------------------------------------------------------------------------
global RsRemoveSpacesFromStartOfString = trimLeft

--------------------------------------------------------------
-- returns true if the string contains spaces
--------------------------------------------------------------
fn RsContainsSpaces instring = 
(
	(findstring instring " ") != undefined
)

--------------------------------------------------------------
-- returns a lowercase version of the string
--------------------------------------------------------------
global RsUppercase = toUpper

--------------------------------------------------------------
-- returns a lowercase version of the string
--------------------------------------------------------------
global RsLowercase = toLower

--------------------------------------------------------------
-- returns a version of the string with spaces converted to underscores
--------------------------------------------------------------
fn RsRemoveSpaces instring = 
(
	local outstring=copy instring 

	for i = 1 to outstring.count where (outstring[i] == " ") do 
	(	
		outstring[i] = "_"
	) 
	
	outstring
)

--------------------------------------------------------------
-- returns a list of string tokens 
-- (ignores delimiters in speech marks)
--------------------------------------------------------------
fn RsFilter parse delimit = 
(
	quoted = false
	tokenList = #()
	token = ""
	
	local char
	for n = 1 to parse.count  do 
	(
		char = parse[n]
		if char == delimit and not quoted then 
		(
			append tokenList token
			token = ""
		) 
		else 
		(
			if char == "\"" do 
			(
				if quoted then 
				(
					quoted = false
				) 
				else 
				(
					quoted = true
				)
			) 
			
			append token char
		)
	)
	
	if token != "" do 
	(
		append tokenList token
	)
	
	tokenList
)

----------------------------------------------------------------------------------------
-- removes all the given character from a string
----------------------------------------------------------------------------------------
fn RsRemoveCharacterFromString instring char = 
(
	local outstring = ""

	for i = 1 to instring.count where (instring[i] != char) do 
	(	
		append outstring instring[i]
	) 
	
	outstring
)

----------------------------------------------------------------------------------------
-- removes all the spaces from a string
----------------------------------------------------------------------------------------
fn RsRemoveSpacesFromString instring = 
(
	RsRemoveCharacterFromString instring " "
)

--------------------------------------------------------------
-- removes all tabs from a string
--------------------------------------------------------------
fn RsRemoveTabsFromString instring = 
(
	RsRemoveCharacterFromString instring "\t"
)

--------------------------------------------------------------
-- replaces all char1 in a string with char2
--------------------------------------------------------------
fn RsReplaceChar instring char1 char2= 
(
	local outstring=copy instring 
		
	for i = 1 to outstring.count where (outstring[i] == char1) do 
	(	
		outstring[i] = char2
	) 
	
	outstring
)

----------------------------------------------------------------------------------------
-- replaces non-alphanumerics in a string with underscores
----------------------------------------------------------------------------------------
fn RsFileSafeString instring = 
(
	local goodChars="abcdefghijklmnopqrstuvwxyz1234567890"
	
	local outstring=copy instring 
		
	for i=1 to outstring.count do 
	(
		j = findString goodChars outstring[i] 
		
		if (j == undefined) do 
		(
			outstring[i] = "_"
		)
	) 
	
	outstring
)

----------------------------------------------------------------------------------------
-- Gives the onscreen size of a string, including new-lines
----------------------------------------------------------------------------------------
fn RsGetTextExtent inString font: = 
(
	local outVal = [0,0]
	
	-- If font is supplied, use dotnet method:
	if (font != unsupplied) then 
	(
		local size = (dotNetClass "TextRenderer").MeasureText inString font
		outVal = [size.width, size.height]
	)
	else 
	(
		
		local charHeight = (getTextExtent " ").y
		
		local stringExtents
		for lineString in (filterString inString "\n\r" splitEmptyTokens:true) do 
		(
			stringExtents = getTextExtent lineString

			if (stringExtents.x > outVal.x) do (outVal.x = stringExtents.x)
			outVal.y += charHeight
		)
	)
	
	return outVal
)

----------------------------------------------------------------------------------------
-- Fits a string to a fixed on-screen width, adding new-lines where necessary
----------------------------------------------------------------------------------------
fn RsWordWrap inString width delimiters:#(" ","\t") font: = 
(
	local outString = "" as stringstream
	
	for lineString in (filterString inString "\n\r" splitEmptyTokens:true) do 
	(
		local doSpaces = true
		local newToken = ""
		local lineTokens = #()
		
		-- Chop line up into space\word tokens:
		for charNum = 1 to lineString.count do 
		(
			getChar = lineString[charNum]
			
			if ((findItem delimiters getChar) != 0) then 
			(
				if doSpaces then 
				(
					newToken += getChar
				)
				else 
				(
					if (isSpace getChar) then 
					(					
						append lineTokens newToken
						newToken = getChar
					)
					else 
					(
						append lineTokens (newToken + getChar)
						newToken = ""
					)
					doSpaces = true
				)
			)
			else 
			(
				if doSpaces then 
				(
					append lineTokens newToken
					newToken = getChar
					doSpaces = false
				)
				else 
				(
					newToken += getChar
				)
			)
		)
		append lineTokens newToken
		
		local spaceTokens = for n = 1 to lineTokens.count by 2 collect lineTokens[n]
		local wordTokens = for n = 2 to lineTokens.count by 2 collect lineTokens[n]

		if (wordTokens.count != 0) then 
		(
			local newLine = spaceTokens[1] + wordTokens[1]
			local lineWidth = (RsGetTextExtent newLine font:font).x
				
			for n = 2 to wordTokens.count do 
			(
				local newWord = spaceTokens[n] + wordTokens[n]
				local wordWidth = (RsGetTextExtent newWord font:font).x
				
				local addWidths = lineWidth + wordWidth
				if (addWidths <= width)  then 
				(
					lineWidth = addWidths
					newLine += newWord
				)
				else 
				(
					format "%\n" newLine to:outString
					newLine = wordTokens[n]
					lineWidth = (RsGetTextExtent wordTokens[n] font:font).x
				)
			)
			format "%\n" newLine to:outString
		)
		else 
		(
			format "\n" to:outString
		)
	)
	
	local outVal = outString as string
	
	if inString[inString.count] != "\n" do  
	(
		outVal = substring outVal 1 (outVal.count - 1)
	)
	
	return outVal
)

----------------------------------------------------------------------------------------
-- Converts a byte-count into a string with more-readable units:
----------------------------------------------------------------------------------------
fn RsGetMemString val showPlus:false =
(
	local gigabyte = 2.0^30
	local megabyte = 2.0^20
	local kilobyte = 2.0^10
	
	local outVal = ""
	
	if showPlus and (val >= 0) do 
	(
		outVal += "+"
	)
	
	outVal += case of 
	(
		((abs val) > (gigabyte / 2)):((val / gigabyte) as string) + " GB"
		((abs val) > (megabyte / 2)):((val / megabyte) as string) + " MB"
		((abs val) > (kilobyte / 2)):((val /kilobyte) as string) + " KB"
		default:((val as integer) as string) + " B"
	)
)

----------------------------------------------------------------------------------------
-- Removes number-suffix from a string:
----------------------------------------------------------------------------------------
fn RsStripSuffixNums inString = 
(
	trimRight inString "0123456789" 
)

----------------------------------------------------------------------------------------
-- Removes a specific suffix from a string, and up to 10 multiples if found:
----------------------------------------------------------------------------------------
fn RsStripSuffix inString suffix = 
(
	local outVal = inString
	local suffixLength = suffix.count
	
	local searchPattern = "*" + suffix
	local safety = 0
	local stringLength = inString.count
	
	while matchPattern outVal pattern:searchPattern and safety<10 do
	(
		stringLength -= suffixLength
		outVal = subString outVal 1 stringLength
		safety += 1
	)
	
	return outVal
)

----------------------------------------------------------------------------------------
-- Print out a struct's values, for debug purposes:
----------------------------------------------------------------------------------------
fn RsPrintStruct thisStruct limitLength:200 showStructs:False = 
(
	local propNames = try (getPropNames thisStruct) catch 
	(
		format "%\n" (thisStruct as string)
		return OK
	)
	
	local doLimit = (isKindOf limitLength Number)
	
	for propName in propNames do 
	(
		local val = getProperty thisStruct propName
		
		if (not isKindOf val MAXScriptFunction) do 
		(
			if (not showStructs) do 
			(
				local printClass
				
				case of 
				(
					(printClass = (classOf val); isKindOf printClass StructDef):
					(
						val = "(struct)"
					)
					((isKindOf val array) and (printClass = (classOf val[1]); isKindOf printClass StructDef)):
					(
						val = "(array: " + (val.count as string) + " structs)"
					)					
				)
			)
			
			val = val as string
			
			if doLimit and (val.count > limitLength) do 
			(
				val = substring val 1 limitLength
				val += " ..."
			)
			
			format "  %: %\n" propName (val as string)
		)
	)
	
	return OK
)
