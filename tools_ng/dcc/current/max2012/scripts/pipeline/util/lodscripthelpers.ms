--
-- File:: pipeline/util/lodscripthelpers.ms
-- Description:: Functions for the LOD Toolkit Script Helpers section
--
-- Author:: Adam Munson <adam.munson@rockstarnorth.com>
-- Date:: 20/01/2011
--
-----------------------------------------------------------------------------

-- Finds furthest vert distance from object's pivot:
fn RsGetMaxObjRadius obj ignoreZ:true = 
(
	local objMesh = copy obj.mesh
	local objPos = obj.pos
	local objXform = obj.objectTransform
	
	local maxDist = 0
	
	for vertNum = 1 to (getNumVerts objMesh) do 
	(
		local vert = (getVert objMesh vertNum) * objXform
		
		if ignoreZ do 
		(
			vert = [vert.x, vert.y]
		)
		
		local vertDist = distance vert objPos
		
		if (vertDist > maxDist) do 
		(
			maxDist = vertDist
		)
	)
	
	return (maxDist as integer)
)

--
--fn:	RsLodGuessLodDist
--desc:	Calculates the LOD distance for an object, the minimum LOD distance
--	is whatever the longest dimension of the object's bounding box is.
--
fn RsLodGuessLodDist objList extraDistType ignoreZ unitsValue percentageValue = 
(
	local idxLodDistance = getattrindex "Gta Object" "LOD distance"
	if objlist.count == 0 do 
	(
		messagebox "No objects selected"
		return false
	)
	
	for obj in objList do 
	(
		if ( "Gta Object" == getattrclass obj ) do
		(
			local lodDist = RsGetMaxObjRadius obj ignoreZ:ignoreZ
			
			-- The LOD distance can be calculated in different ways
			-- depending on what is selected on the UI:
			-- 
			-- extraDistType:
			--	1 = No additional calculation
			--	2 = Percentage
			--	3 = Units
			--	4 = Percentage + Units
			--
			if ( 2 == extraDistType or 4 == extraDistType ) do
			(
				lodDist = ( lodDist / 100.0 ) * percentageValue
			)
			if ( 3 == extraDistType or 4 == extraDistType )do
			(
				lodDist += unitsValue as float
			)
			
			lodDist = ceil lodDist
			
--format "RsLodGuessLodDist \n  %\n  type:%, ignoreZ:%, units:%, percentage:%\n  lodDist:%\n" (objList as string) extraDistType ignoreZ unitsValue percentageValue lodDist
			
			-- Set the LOD distance, also means really small objects
			-- will at least end up with a LOD distance > 0
			setattr obj idxLodDistance lodDist
		)
	)
	
	return true
)
