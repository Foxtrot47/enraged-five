-- Mathematical helper functions

-- floatToIntWithRound rounds an incoming float and returns an int so that:
-- -1.0 => -1
-- -0.5 => -1
-- 0 => 0
-- 0.5 => 1
-- 1.0 => 1
-- to test: for i = -1000 to 1000 do format "% => %\n" ((float)i / 100) (floatToIntWithRound ((float)i / 100))
fn floatToIntWithRound floatValue =
(
	result = 0
	
	if( floatValue >= 0 ) then
	(
		result = (int)( floor( floatValue + 0.5 ) )
	)
	else
	(
		result = (int)( ceil( floatValue - 0.5 ) )
	)
	
	result
)

fn matrix3transpose mtx = 
(
	local bigmtx = bigmatrix 3 3
	for i=1 to 3 do bigmtx[1][i] = mtx.row1[i]
	for i=1 to 3 do bigmtx[2][i] = mtx.row2[i]
	for i=1 to 3 do bigmtx[3][i] = mtx.row3[i]
	local invmtx = transpose bigmtx
	local newMtx = mtx
	newMtx.row1 = [invmtx[1][1],invmtx[1][2],invmtx[1][3]]
	newMtx.row2 = [invmtx[2][1],invmtx[2][2],invmtx[2][3]]
	newMtx.row3 = [invmtx[3][1],invmtx[3][2],invmtx[3][3]]
	newMtx
)