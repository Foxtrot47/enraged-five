filein "pipeline/export/peds/pedExport.ms"
filein "pipeline/export/peds/utils.ms"

filein ( RsConfigGetWildwestDir() + "script/3dsmax/simplygon/simplygon.ms" )

struct RsPedBatchExporter
(	
public

	-- Generates a RsPedExportData struct from current control settings:
	fn newPedExportInfo createChangelist:false extractZips:false buildZips:false = 
	(
		if (RsGeneratePedTCSFiles == true) then
			changelistDesc = "Ped TCS and DDS"
		else
			changelistDesc = "Ped DDS"
		RsP4DeleteEmptyChangelists (changelistDesc + "*")
		local clNum = if ( createChangelist ) then ( gRsPerforce.createChangelist changelistDesc 
		) else ( undefined )
		
		if (gPedTextureExportOps != undefined) do (gPedTextureExportOps.changelistNum = clNum)
	
		RsPedExportData compExport:true propExport:true overwriteTcs:false \
			previewExport:false preservePreview:false overwriteMeta:false \ 
			extractZips:extractZips buildZips:buildZips splitBlends:false changelistNum:clNum
	),
	
	fn selectPedNode =
	(
		local nodeName = getFilenameFile maxFileName
		local pedNode = getNodeByName nodeName
		if pedNode != undefined and (classof pedNode ) == Dummy  then
		(
			select pedNode	
			true
		)
		else
			false
	),
	
	fn generatePedLODS =
	(
		local processedINI = ((Rsmakesafeslashes (GetDir #temp))+"/processed.ini")
		if (doesfileexist processedINI) == false then createfile processedINI
		
		if (selectPedNode()) then (
			
			try (
					local generateLods = gRsSimplygonUtils.autoLodCharacter()
				if generateLods == true then setinisetting processedINI "Files" (getFilenameFile maxFileName) "LOD_processed" else
				setinisetting processedINI "Files" (getFilenameFile maxFileName) "LOD_failed"	
			) catch (setinisetting processedINI "Files" (getFilenameFile maxFileName) "LOD_failed")
		)
	),
	
	fn addIKBones = 
	(
		filein "x:\rdr3\tools_migrate\wildwest\script\3dsMax\Characters\Rigging\addExtraIKbones.ms"
	),
	
	
	fn exportMetadata =
	(
		filein "pipeline/export/ui/ped_ui.ms"
		print ("Exporting " + maxFileName + "...")
		setQuietMode true
		if selectPedNode()  == true then
		(
			gPedTextureExportOps = RsPedTextureExportOps()
			pedExportInfo = newPedExportInfo createChangelist:true buildZips:false overwriteMeta:true
			
			nodeName = getFilenameFile maxFileName
			actualNode = getNodeByName nodeName
			select actualNode
			pedExportInfo.init selection[1]
			gRsPedExport.ExportVariationMetaData pedExportInfo pedExportInfo.compOutputDir
		)
		else
		(
			print "Unable to select ped root node to export."
		)
		
		setQuietMode false
		
		print ("Completed exporting " + maxFileName + ".")
	),
	
	fn export =
	(
		filein "pipeline/export/ui/ped_ui.ms"
		print ("Exporting " + maxFileName + "...")
		setQuietMode true
		if selectPedNode()  == true then
		(
			gPedTextureExportOps = RsPedTextureExportOps()
			pedExportInfo = newPedExportInfo createChangelist:true buildZips:false
			gRsPedExport.pedExport pedExportInfo		
		)
		else
		(
			print "Unable to select ped root node to export."
		)
		
		setQuietMode false
		
		print ("Completed exporting " + maxFileName + ".")
	),
	
	fn extractZips =
	(
		print ("Extracting zips...")
		setQuietMode true
		if selectPedNode() == true then
		(
			gPedTextureExportOps = RsPedTextureExportOps()
			pedExportInfo = newPedExportInfo createChangelist:true extractZips:true buildZips:false
			gRsPedExport.pedExport pedExportInfo		
		)
		else
		(
			print "Unable to select ped root node to export."
		)
		
		setQuietMode false
		print ("Completed extracting zips.")
	),
	
	fn buildZips = 
	(
		print ("Building zips...")
		setQuietMode true
		selectPedNode()
		
		gPedTextureExportOps = RsPedTextureExportOps()
		pedExportInfo = newPedExportInfo createChangelist:true extractZips:false buildZips:true
		gRsPedExport.pedExport pedExportInfo		
		setQuietMode false
		
		print ("Completed building zips.")
	)
)

