--
-- File:: pipeline/util/IdeUtils.ms
-- Description:: Allows loading and editing of ide/ipl files in script
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com>
-- Date:: 20/4/2004
--
-----------------------------------------------------------------------------
-- ChangeLog
--
-- 26/06/2007 -- DHM -- GTA4 compatibility update
-- 		Updated IDE loading to "GtaLoadIDEObjects" function
-- 		Now correctly parses *some* of the IDE files.
--		Added GtaLoadIDEFiles function and GTA_MapsRoot global path string
--		IPL data loading not touched
--	
-- 25/03/2011 -- NDC -- Removed unused functions
--		The rest isn't really used either, but need to be 
--		weeded out of other scripts before deletion
-------------------------------------------------------------------------------------------			 

-------------------------------------------------------------------------------------------
-- Structure Definitions
-------------------------------------------------------------------------------------------

struct ideobjectclass (

	---------------------------------------------------------------------------------------
	-- Constants
	---------------------------------------------------------------------------------------
	FLAG_LIGHTREFLECTION 		= 0x00000001,
	FLAG_DONTFADE				= 0x00000002,
	FLAG_DRAWLAST				= 0x00000004,
	FLAG_INSTANCED				= 0x00000008,
	--FLAG_RESERVED1			= 0x00000010,
	FLAG_ISFIXED				= 0x00000020,
	FLAG_NOZBUFFERWRITE			= 0x00000040,
	FLAG_NOSHADOWS				= 0x00000080,
	FLAG_ISGENERIC				= 0x00000100,
	FLAG_HASANIM				= 0x00000200,
	FLAG_HASUVANIM				= 0x00000400,
	FLAG_SHADOWONLY				= 0x00000800,
	FLAG_ISDAMAGEABLE			= 0x00001000,
	FLAG_DONTCASTSHADOWS		= 0x00002000,
	FLAG_CASTTEXSHADOWS			= 0x00004000,
	FLAG_NOFLYERCOLLIDE			= 0x00008000,
	--FLAG_RESERVED2			= 0x00010000,
	FLAG_ISDYNAMIC				= 0x00020000,
	--FLAG_RESERVED3			= 0x00040000,
	--FLAG_RESEVRED4			= 0x00080000,
	--FLAG_RESERVED5			= 0x00100000,
	FLAG_NOBACKFACECULLING		= 0x00200000,
	FLAG_DOESNOTPROVIDECOVER	= 0x00400000,
	--FLAG_RESERVED6			= 0x00800000,
	FLAG_ISLADDER				= 0x01000000,
	FLAG_ISFIREESCAPE			= 0x02000000,
	FLAG_HASDOORPHYSICS			= 0x04000000,
	FLAG_ISFIXEDFORNAV			= 0x08000000,
	FLAG_NOPEDCOLLISION			= 0x10000000,
	FLAG_AMBIENTMULTPLIER		= 0x20000000,

	---------------------------------------------------------------------------------------
	-- Properties
	---------------------------------------------------------------------------------------
	
	name,		-- Object Name (string)
	txd, 		-- TXD Name (string)
	loddist, 	-- LOD Distance (float)
	flags,		-- Flags (bitfield)
	
	---------------------------------------------------------------------------------------
	-- Methods
	---------------------------------------------------------------------------------------

	-- Flag check status
	fn hasFlag flag = (
	
		return ( ( bit.and flags flag ) == flag )
	),

	-- Compare two ideObjectClass objects
	fn cmp o1 o2 = (
		if ( classof o1 != ideObjectClass ) then
			throw "o1 must be of type ideObjectClass"
		if ( classof o2 != ideObjectClass ) then
			throw "o2 must be of type ideObjectClass"
		
		if ( o1.name == o2.name ) then
			return 0
		else if ( o1.name < o2.name ) then
			return -1
		else
			return 1
	)
)

-------------------------------------------------------------------------------------------
-- Global Data
-------------------------------------------------------------------------------------------

global GTA_MapsRoot = "x:/gta/build/independent/data/maps/"


-------------------------------------------------------------------------------------------
-- name:		GtaLoadIDEObjects
-- description:	Load an IDE file adding object definitions to the loadList,
--				** Note not all objects nor data is parsed **
--				
-- Reference:	FileLoader.cpp : CFileLoader::LoadObject(const char* pLine)
--
-------------------------------------------------------------------------------------------

fn GtaLoadIDEObjects filename loadList = (
	
	ideFile = openfile filename mode:"rt"
	
	ideLine = readLine ideFile
	
	-- find the beginning of the objects
	
	while(eof ideFile == false) do (
		
		if ideLine == "objs" then (
			exit
		)
		
		ideLine = readLine ideFile	
	)
	
	ideLine = readLine ideFile	
	
	-- read the objects in

 	while(eof ideFile == false) do (
		
		if ideLine == "end" then (
			exit
		)
		
		ideTokens = filterstring ideLine ", "
		
		objName = RsLowercase(ideTokens[1])
		objTXDName = RsLowercase(ideTokens[2])
		objLODDist = ideTokens[3] as float
		objFlags = ideTokens[4] as integer
		
		newObjClass = ideobjectclass name:objName txd:objTXDName loddist:objLODDist flags:objFlags
		
		append loadList newObjClass
		
		ideLine = readLine ideFile	
	)
	
	-- time objects
	
	while(eof ideFile == false) do (
		
		if ideLine == "tobj" then (
			exit
		)
		
		ideLine = readLine ideFile	
	)
	
	ideLine = readLine ideFile	
	
	-- read the objects in

 	while(eof ideFile == false) do (
		
		if ideLine == "end" then (
			exit
		)
		
		ideTokens = filterstring ideLine ", "
		
		objName = RsLowercase(ideTokens[1])
		objTXDName = RsLowercase(ideTokens[2])
		objLODDist = ideTokens[3] as float
		objFlags = ideTokens[4] as integer
		
		newObjClass = ideobjectclass name:objName txd:objTXDName loddist:objLODDist flags:objFlags
		
		append loadList newObjClass
		
		ideLine = readLine ideFile	
	)
	
	-- anim objects
	
	while(eof ideFile == false) do (
		
		if ideLine == "anim" then (
			exit
		)
		
		ideLine = readLine ideFile	
	)
	
	ideLine = readLine ideFile	
	
	-- read the objects in

 	while(eof ideFile == false) do (
		
		if ideLine == "end" then (
			exit
		)
		
		ideTokens = filterstring ideLine ", "
		
		objName = RsLowercase(ideTokens[1])
		objTXDName = RsLowercase(ideTokens[2])
		-- Anim file name
		objLODDist = ideTokens[4] as float
		objFlags = ideTokens[5] as integer
		
		newObjClass = ideobjectclass name:objName txd:objTXDName loddist:objLODDist flags:objFlags
		
		append loadList newObjClass
		
		ideLine = readLine ideFile	
	)
	
	close ideFile
)

-------------------------------------------------------------------------------------------
-- name:		GtaLoadIDEFiles
-- description:	Load an IDE file adding object definitions to the ideObjectList
--				Both object class name and TXD name are added
--
--				IMPORTANT: ideObjectList returned is SORTED.  This is required as it speeds
--						   up GtaFindIDEObject considerably.  Peace out.
-------------------------------------------------------------------------------------------
fn GtaLoadIDEFiles rootpath &ideFileList &ideObjectList = (

	ideObjectList = #()
	ideFileList = #()
	ideFileList = RsFindFilesRecursive rootpath "*.ide"

	for f in ideFileList do
	(
		GtaLoadIDEObjects f ideObjectList
	)
	
	sort ideFileList
	qsort ideObjectList ideObjectClass.cmp
)

-------------------------------------------------------------------------------------------
-- name:		GtaFindIDEObject
-- description:	Find and return object definition based on name, given ideObjectList as
--				filled in by GtaLoadIDEFiles / GtaLoadIDEObjects
--
--				IMPORTANT: ideObjectList must be SORTED.  GtaLoadIDEFiles does this :P
--						   so use it.
-------------------------------------------------------------------------------------------
fn GtaFindIDEObject ideObjectList objName = (

	local low = 1
	local high = ideObjectList.count
	
	while ( low <= high ) do
	(
		mid = (low+high) / 2
		
		if ( ideObjectList[mid].name > RsLowercase(objName) ) then
			high = mid - 1
		else if ( ideObjectList[mid].name < RsLowercase(objName) ) then
			low = mid + 1
		else
			return ( ideObjectList[mid] )
	)
	return undefined
)
