--------------------------------------------------------------
/*
    Copyright © 2015 Rockstar North
    File:       pipeline/util/vehicle_lod_utils.ms
    Author:     Neil Finlay
    Date:       Aug 31st 2015
    Info:       Functons to automatically hook up mod lod items
                Written like a static class using a struct, e.g. RsVehicleLodUtils.RsVehicleModAutoLodObject is required to call RsVehicleModAutoLodObject
*/
--------------------------------------------------------------

struct RsVehicleLodUtils
(
    /*
    -------------------------------------------------------------
    name:   RsVehicleLodUtils.RsVehicleModAutoLodObject
    desc:   Attempts to link up an object to it's lod equivalents.
    params: obj - a node which is the "base" of the lod chain, e.g. "blah" (not "blah_ng" or "blah_l1" etc.)
    -------------------------------------------------------------
    */
    fn RsVehicleModAutoLodObject obj = 
    (
        local baseName = obj.name
        format "Setting LODs for : '%'\n" (baseName as string)

        ng = getNodeByName (baseName + "_ng")
        lod0=getNodeByName (baseName)
        lod1=getNodeByName (baseName + "_l1")
        lod2=getNodeByName (baseName + "_l2")
        lod3=getNodeByName (baseName + "_l3")
        lod4=getNodeByName (baseName + "_l4")

        lodArray = #(lod4,lod3,lod2,lod1,lod0,ng)

        for i = 1 to lodArray.count do 
        (
            if lodArray[i] == undefined then continue
          
            try
            (
                me = lodArray[i]
                theChild = lodArray[i+1]
                if me == undefined or theChild == undefined then continue
                RsSceneLink.SetParent linkType_DRAWLOD theChild me 
            )
            catch 
            (
                local ex = getCurrentException()
                format "All gone wrong: %\n" (ex as string)
            )
        )
            
    ),

    /*
    -------------------------------------------------------------
    name:   RsVehicleLodUtils.GrabChildren
    desc:   Goes through an objects children and grabs them all up into an array. This then selecting the array is actually faster that RsSelectHierarchy :)
    params: obj - a node or array of nodes to begin the search
            out - an array for the output which will contain all the children recursively of items in obj
    -------------------------------------------------------------
    */
    fn GrabChildren obj out:#() = 
    (
        local objArr = if classof obj != Array then #(obj) else obj
        
        for o in objArr do 
        (
            append out o
            for ch in o.children where o.children.count > 0 do 
            (
                RsVehicleLodUtils.GrabChildren ch out:out
            )
        )
    ),

    /*
    -------------------------------------------------------------
    name:   RsVehicleLodUtils.RsVehicleModAutoLod
    desc:   Attempts to link up every mod in the selection to it's lod equivalents.
            Can be give all of the parts of the lod ("blah_ng" and "blah_l1" as well as the "base" - "blah" )
    params: <none>
    -------------------------------------------------------------
    */
    fn RsVehicleModAutoLod = 
    (

        local modDummies = for o in objects where matchPattern o.name pattern:"*_mods*" collect o

        local allModObjects = #()
        RsVehicleLodUtils.GrabChildren modDummies out:allModObjects

        local itemsToAutoLod = #()

        for o in allModObjects where getAttrClass o == "Gta Object" do 
        (
            local baseName = 
            if  (matchPattern o.name pattern:"*_ng") or 
                (matchPattern o.name pattern:"*_l1") or
                (matchPattern o.name pattern:"*_l2") or
                (matchPattern o.name pattern:"*_l3") or
                (matchPattern o.name pattern:"*_l4") then 
            (
                (substring (o.name) 1 (o.name.count - 3))
            )else(
                o.name
            )

            local theNode = (getNodeByName baseName)
            if theNode == undefined then
            (
                gRsUlog.LogError ("Error: node == undefined, looking for " + (baseName as string))
                continue
            )
           
            local found = false
            for k in itemsToAutoLod while not found do
            (
                found = (k.name == theNode.name)
            )

            if not found then 
            (
                append itemsToAutoLod theNode
            )

        )

        for item in itemsToAutoLod do 
        (
            try
            (
                RsVehicleLodUtils.RsVehicleModAutoLodObject item
            )
            catch
            (
                ex = getCurrentException()
                gRsUlog.LogError ("Something has gone wrong:\n" + (ex as string))

            )
        )

        format "Complete\n"
        return gRsUlog.Validate()

    )
)