--
-- File:: pipeline/util/container_funcs.ms
-- Description:: Container Functions
--
-- Author:: Neal D Corbett <neal.corbett@rockstarleeds.com>
-- Date:: 27 September 2011
--

fileIn "pipeline/util/p4_containers.ms"

struct RsContFuncs 
(
	fn getContFilename obj = 
	(
		local filename = obj.localDefinitionFilename

		if (filename == undefined) or (filename == "") do 
		(
			filename = obj.sourceDefinitionFilename
		)
		
		if (filename == undefined) or (filename == "") do 
		(
			local map = RsProjectContentFind (toLower obj.name) type:"map"
			
			if (map != undefined) do 
			(
				filename = map.path + "/" + map.name + ".maxc"
			)
		)
		
		if (filename == undefined) then (return "") else (return filename)
	),
	
	fn inheritFiles contFiles showProgress:true = 
	(
		clearSelection()
		
		-- Manually call RSref pre-merge callback:
		::RSrefFileCallbacksStruct.filePreMerge()
		
		if showProgress do 
		(
			progressStart "Inheriting containers:"
			progressUpdate 100.0
			progressUpdate 0.0
		)
		
		with redraw off 
		(
			local itemNum = -1
			local contObjs = for filename in contFiles while not showProgress or (progressUpdate (100.0 * (itemNum += 1 )/ contFiles.count)) collect 
			(
				-- Don't trigger the RSref-load system (yet)
				RSnonUserMerge = true
				
				local cont = (Containers.CreateInheritedContainer filename)
				if (cont == undefined) then dontCollect else cont
			)
		)
			
		if showProgress do 
		(
			progressUpdate 100.0
			progressEnd()
		)
		
		-- Trigger any post-inherit viewport-callbacks:
		completeRedraw()
		
		RSnonUserMerge = false
		::RSrefFileCallbacksStruct.filePostMerge()
			
		return contObjs
	),
	
	fn inheritBrowsedFiles = 
	(
		local contFiles = RSgetOpenFilenames caption:"Inherit Container Files" filename:"" types:"3ds Max Container Definition (*.maxc)|*.maxc"
		if (contFiles != undefined) do 
		(
			local contObjs = RsContFuncs.inheritFiles contFiles

			select contObjs
		)
		
		return contObjs
	),
	
	fn reInherit objs showProgress:true = 
	(
		local oldConts = for obj in objs where (isKindOf obj Container) collect obj
		
		if (oldConts.count == 0) do return #()
		
		local newConts = #()
		
		-- Manually call RSref pre-merge callback:
		::RSrefFileCallbacksStruct.filePreMerge()
		
		if showProgress do 
		(
			progressStart "Re-inheriting containers:"
			progressUpdate 100.0
			progressUpdate 0.0
		)
		
		for contNum = 1 to oldConts.count while not showProgress or (progressUpdate (100.0 * (contNum - 1)/ oldConts.count)) do 
		(
			local oldCont = oldConts[contNum]
			
			-- Don't trigger the RSref-load system (yet)
			RSnonUserMerge = true
			
			local filename = RsContFuncs.getContFilename oldCont
			
			-- If container has been merged, it still won't have found a filename:
			if (doesFileExist filename) do 
			(
				local newCont = containers.createInheritedContainer filename
				
				if (newCont != undefined) do 
				(
					append newConts newCont
					delete oldCont
				)
			)
		)
		
		if showProgress do 
		(
			progressUpdate 100.0
			progressEnd()
		)
		
		RSnonUserMerge = false
		::RSrefFileCallbacksStruct.filePostMerge()
		
		return newConts
	),
	
	fn deleteLocks objs silent:false = 
	(
		local delFiles = #()
		local contObjs = for obj in objs where (isKindOf obj Container) collect obj
		
		for obj in contObjs do 
		(
			local contFilename = RsContFuncs.getContFilename obj

			if (contFilename != "") do 
			(
				local lockFilename = contFilename + ".lock"

				if doesFileExist lockFilename do 
				(
					print lockFilename
					deleteFile lockFilename
					append delFiles lockFilename
				)
			)
		)
		
		case of 
		(
			silent:()
			(delFiles.count == 0):
			(
				local msg = "No .lock files were found for containers:\n\n"
				
				for n = 1 to contObjs.count do 
				(
					append msg contObjs[n].name
					
					if n != objs.count do 
					(
						append msg "\n"
					)
				)
				
				messageBox msg title:"No lock-files found"
			)
			default:
			(
				local msg = "These files have been removed:\n\n"
				
				for n = 1 to delFiles.count do 
				(
					append msg delFiles[n]
					
					if n != delFiles.count do 
					(
						append msg "\n"
					)
				)
				
				messageBox msg title:"Files deleted"
			)
		)
		
		return OK
	),
	
	-------------------------------------------------------------------------------------
	-- GetContPrefix:
	--	Returns map-prefix string for a given container
	-------------------------------------------------------------------------------------
	fn GetContPrefix ThisCont = 
	(
		local Prefix = ""
		
		if (isKindOf ThisCont Container) do 
		(
			-- Get container filename:
			local ContName = ThisCont.LocalDefinitionFilename
			if (ContName == undefined) or (ContName == "") do 
			(
				ContName = ThisCont.SourceDefinitionFilename
			)
			
			if (ContName == undefined) or (ContName == "") then 
			(
				-- Get container-name from object, if filename wasn't found:
				ContName = ThisCont.Name
			)
			else 
			(
				-- Get container-name from filename:
				ContName = GetFilenameFile ContName
			)
			
			-- Get map-content node for container-name:
			local Map = RsProjectContentFind (toLower ContName) type:"map"
			
			-- Get prefix for this container, if it has one:
			if (Map != undefined) do 
			(
				Prefix = Map.Prefix
			)
		)
		
		return Prefix
	)
)
