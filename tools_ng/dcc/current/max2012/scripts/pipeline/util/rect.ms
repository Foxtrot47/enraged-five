
struct RsRect
(
	public
	
	/*
	
	This represents where bbox_min and bbox_max live on 
	the coordinate system.
	
	
			    -----------------------------+ bbox_max
			    |							|
			    |			x				|
			    |			|				|
			    |			|_ _y		     |
			    |							|
			    |							|
	bbox_min +-----------------------------
	
	*/
	
	bbox_min			= [ -1.0, -1.0 ],
	bbox_max			= [ 1.0, 1.0 ],
	
	
	fn top = (
		bbox_max.y
	),
	
	fn bottom = (
		bbox_min.y
	),
	
	fn left = (
		bbox_min.x
	),
	
	fn right = (
		bbox_max.x
	),
	
	fn width = (
		abs ( bbox_max.x - bbox_min.x )
	),
	
	fn height = (
		abs ( bbox_min.y - bbox_max.y )
	),
	
	fn bottomLeft = (
		bbox_min
	),
	
	fn bottomRight = (
		Point2 bbox_max.x bbox_min.y
	),
	
	fn topRight = (
		bbox_max
	),
	
	fn topLeft = (
		Point2 bbox_min.x bbox_max.y
	),
	
	fn center = (
		( ( bbox_min + bbox_max ) / 2.0 )
	),
	
	fn topLine = (
		topLeft() - topRight()
	),
	
	fn rightLine = (
		topRight() - bottomRight()
	),
	
	fn bottomLine = (
		bottomLeft() - bottomRight()
	),
	
	fn leftLine = (
		topLeft() - bottomLeft()
	),
	
	-- Does the supplied rect intersect with this rect.
	fn intersects rect = (
		RsAssert ( classOf rect == RsRect ) message:"You must supply a RsRect object!"
		
		not ( left() > rect.right() or right() < rect.left() or top() < rect.bottom() or bottom() > rect.top() )
	),
	
	-- Is the supplied rect inside of this rect.
	fn contains rect = (
		RsAssert ( classOf rect == RsRect ) message:"You must supply a RsRect object!"
		
		( left() < rect.left() and right() > rect.right() and top() > rect.top() and bottom() < rect.bottom() )
	),
	
	-- Boolean this rect to the overlapping area of the supplied rect.
	fn boolean rect = (
		RsAssert ( classOf rect == RsRect ) message:"You must supply a RsRect object!"
		
		local result = false
		
		if ( intersects rect ) do (
			max_x = amin ( right() ) ( rect.right() )
			max_y = amin ( top() ) ( rect.top() )
			
			min_x = amax ( left() ) ( rect.left() )
			min_y = amax ( bottom() ) ( rect.bottom() )
			
			bbox_min = Point2 min_x min_y
			bbox_max = Point2 max_x max_y
			
			result = true
		)
		
		result
	),
	
	-- Return which compass direction the supplied rect intersects with this rect.
	/*
	fn intersectCompass rect = (
		RsAssert ( classOf rect == RsRect ) message:"You must supply a RsRect object!"
		
		if not ( contains rect ) and ( intersects rect ) then (
			
			-- Determine which side of the rectangle is the closest.
			local disNorth = distance ( ( topLeft() ) + ( topRight() ) / 2.0 ) ( rect.center() )
			local disEast = distance ( ( topRight() ) + ( bottomRight() ) / 2.0 ) ( rect.center() )
			local disSouth = distance ( ( bottomLeft() ) + ( bottomRight() ) / 2.0 ) ( rect.center() )
			local disWest = distance ( ( topLeft() ) + ( bottomLeft() ) / 2.0 ) ( rect.center() )
			
			print disNorth
			print disEast
			print disSouth
			print disWest
			
			local closest = amin disNorth disEast disSouth disWest
			print closest
			
			if closest == disNorth then return #north
			if closest == disEast then return #east
			if closest == disSouth then return #south
			if closest == disWest then return #west
		)
		
		undefined
	),
	*/
	
	-- Is this supplied point inside of this rect.
	fn pointInside pt = (
		( pt.x > left() and pt.x < right() and pt.y > bottom() and pt.y < top() )
	),
	
	fn expand factor = (
		bbox_min.x -= factor
		bbox_min.y -= factor
		bbox_max.x += factor
		bbox_max.y += factor
	),
	
	fn scale x y = (
		bbox_min.x -= x
		bbox_min.y -= y
		bbox_max.x += x
		bbox_max.y += y
	),
	
	fn union rect = (
		RsAssert ( classOf rect == RsRect ) message:"You must supply a RsRect object!"
		
		min_x = amin #( bbox_min.x, rect.bbox_min.x )
		min_y = amin #( bbox_min.y, rect.bbox_min.y )
		max_x = amax #( bbox_max.x, rect.bbox_max.x )
		max_y = amax #( bbox_max.y, rect.bbox_max.y )
		
		bbox_min = Point2 min_x min_y
		bbox_max = Point2 max_x max_y
	),
	
	fn intersectLine vec = (
		
	),
	
	fn createPlaneObject = (
		local centerPos = center()
		
		Plane name:( uniqueName "RsRect Plane" ) width:( width() ) length:( height() ) pos:[ centerPos.x, centerPos.y, 0 ] lengthSegs:2 widthSegs:2 wireColor:( color 255 255 0 )
	),
	
	fn createFromObject object transform:( matrix3 1 ) = (
		local bbox = nodeGetBoundingBox object transform
		bbox_min = Point2 bbox[ 1 ][ 1 ] bbox[ 1 ][ 2 ]
		bbox_max = Point2 bbox[ 2 ][ 1 ] bbox[ 2 ][ 2 ]
	)
)

/*
fn RsCheckObjectsWithinTile objectsToCheck tileRect = 
(
	RsAssert ( classOf tileRect == RsRect ) message:"You must supply an RsRect object for the tileRect argument!"
	
	local north = #()
	local east = #()
	local south = #()
	local west = #()
	
	for obj in objectsToCheck do (
		local objRect = RsRect()
		objRect.createFromObject obj
		
		--if not tileRect.contains objRect then append results obj
		
		dir = tileRect.intersectCompass objRect
		print dir
	)
	
	results
)
*/
