
-- R* Object-Marker Manager:
-- 	Draws markers and text in viewport over objects with specific attributes

-- 21/01/2014 - Neal D Corbett, Rockstar Leeds

-- Remove pre-existing callbacks:
if (gRsObjViewMarkers != undefined) do 
(
	gRsObjViewMarkers.ClearCallbacks()
	gRsObjViewMarkers = undefined
)

-- Sub-struct used to define a marker for an object:
struct RsObjViewMarker (Obj, Text, Marker = #hollowBox, Colour = [0,200,200])

struct RsObjViewMarkers 
(
	Markers = undefined,
	Enabled = True,
	
	idxLevelDesignObject = (GetAttrIndex "Gta Object" "Level Design Object"),
	
	fn InitMarkers = 
	(
		-- Find scene-objects with markable attributes:
		Markers = for obj in Objects collect 
		(
			case (GetAttrClass obj) of 
			(
				"Gta Object":
				(
					case of 
					(
						(GetAttr obj idxLevelDesignObject):(RsObjViewMarker Obj:obj Text:"Level Design Object")
						Default:DontCollect
					)
				)
				Default:DontCollect
			)
		)
	),
	
	fn DrawMarkers = 
	(
		-- Skip function if this system has been disabled, or if Hide Helpers is active:
		if (HideByCategory.Helpers) or (not Enabled) do return False
		
		-- Check for invalid objects in marker-list:
		if (Markers != undefined) do 
		(
			local AllGood = True
			for Item in Markers while AllGood do 
			(
				AllGood = isValidNode Item.Obj
			)
			
			-- Reset 'Markers' list if any listed objects are invalid:
			if (not AllGood) do 
			(
				Markers = undefined
			)
		)
		
		-- Generate Markers list if it's undefined:
		if (Markers == undefined) do 
		(
			InitMarkers()
		)
		
		-- Draw listed markers:
		if (Markers.Count != 0) do 
		(
			gw.setTransform (Matrix3 1)
			
			for Item in Markers do 
			(
				local obj = Item.Obj
				
				if (not obj.IsHidden) do 
				(
					local pos = gw.wtranspoint Item.Obj.Pos
					local clr = Item.Colour
					
					gw.wtext pos Item.Text color:clr
					gw.wMarker pos Item.Marker color:clr
				)
			)
		)
	),
	
	-- Turn off marker-draw system:
	fn Disable = 
	(
		unregisterRedrawViewsCallback DrawMarkers
		Enabled = False
		Markers = undefined
	),
	-- Turn on marker-draw system:
	fn Enable = 
	(
		Enabled = True
		Markers = undefined
		registerRedrawViewsCallback DrawMarkers
	),
	
	fn ClearCallbacks = 
	(
		callbacks.removeScripts id:#RsObjViewMarkers
		Disable()
	),
	
	fn SetupCallbacks = 
	(
		ClearCallbacks()
		
		callbacks.addScript #filePreOpenProcess		"gRsObjViewMarkers.Disable()"	id:#RsObjViewMarkers
		callbacks.addScript #filePostOpenProcess		"gRsObjViewMarkers.Enable()"		id:#RsObjViewMarkers

		callbacks.addScript #objectXrefPreMerge		"gRsObjViewMarkers.Disable()"	id:#RsObjViewMarkers
		callbacks.addScript #objectXrefPostMerge	"gRsObjViewMarkers.Enable()"		id:#RsObjViewMarkers

		callbacks.addScript #filePreMerge				"gRsObjViewMarkers.Disable()"	id:#RsObjViewMarkers
		callbacks.addScript #filePostMerge				"gRsObjViewMarkers.Enable()"		id:#RsObjViewMarkers

		callbacks.addScript #systemPostReset			"gRsObjViewMarkers.Enable()"		id:#RsObjViewMarkers
		callbacks.addScript #systemPostNew			"gRsObjViewMarkers.Enable()"		id:#RsObjViewMarkers
		
		callbacks.addScript #preSystemShutdown	"gRsObjViewMarkers.Disable()"	id:#RsObjViewMarkers
		
		-- Activate markers:
		Enable()
		DrawMarkers()
	)
)

-- Set up an initialise marker-manager struct:
global gRsObjViewMarkers = RsObjViewMarkers()
gRsObjViewMarkers.SetupCallbacks()
