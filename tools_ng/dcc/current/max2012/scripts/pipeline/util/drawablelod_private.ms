--
-- File:: drawablelod_private.ms
-- Description:: Drawable LOD Hierarchy Private Functions
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 24 March 2009
-- 

-----------------------------------------------------------------------------
-- PRIVATE FUNCTIONS -- DO NOT USE
-----------------------------------------------------------------------------


--
-- name: __RsLodDrawable_GetChildren (private)
-- desc: Return Array of Drawable LOD children.
--
fn __RsLodDrawable_GetChildren lodparent = 
(
	local item
	local refobjs2
	for refObj in (refs.dependents lodparent) collect 
	(
		item = dontCollect
		if (not isDeleted refObj) do 
		(	
			if (isKindOf refObj LodDrawableAttributes) do
			(
				refobjs2 = (refs.dependents refObj)[1]
			
				if isProperty refobjs2 "name" then 
				(			
					item = refobjs2
				)
			)
		)
		item
	)
)

fn __RsLodDrawableChild_GetChildren lodparent = 
(
	local item
	local refobjs2
	for refObj in (refs.dependents lodparent) collect 
	(
		item = dontCollect
		if (not isDeleted refObj) do 
		(	
			if (isKindOf refObj LODDrawablChildAttributes) do
			(
				refobjs2 = (refs.dependents refObj)[1]
			
				if isProperty refobjs2 "name" then 
				(			
					item = refobjs2
				)
			)
		)
		item
	)
)

-- drawablelod_private.ms
