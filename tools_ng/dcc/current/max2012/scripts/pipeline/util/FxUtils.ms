filein "pipeline/util/p4.ms"

global gFxErrorMsg

fn ShowErrorRollout = 
(
	try(DestroyDialog ParticleErrorRollout)catch()
	if undefined==::gFxErrorMsg or ""==::gFxErrorMsg then
	(
		messagebox "No actual errors returned. Contact tools."
		return false
	)
	rollout ParticleErrorRollout "Resolve effect build errors"
	(
		dotNetControl f1 "MaxCustomControls.MaxTextBox" height:200
-- 		edittext errorText "Errors:" text:gFxErrorMsg readonly:true multiline:true height:200
		group "1) Effect files"
		(
			label lbl1 "The effect files are in the art folder and game independent. \nThis is the first step." height:30 align:#left
			button btnEffectsDef "Get art effect definitions" across:2 align:#left
			checkbox chckForceGet "force" 
		)
		group "2) Effect definition file"
		(
			label lbl2 "The effect definition file (entityfx.dat) and the ptfx.zip/rpf files are game\nDEPENDENT. That means, getting latest on this might break your game. \nRevert to older/labelled revisions if this happens." height:45 align:#left
			button btnEffectsFile "Get EntityFX.dat" align:#left
		)
		group "3) Potential format change"
		(
			label lbl3 "If the above don't work, the files from code potentially have changed in their format. \nPlease contact tools." height:30 align:#left
		)
		button btnRebuild "Try again" width:380
		
		on ParticleErrorRollout open do
		(
			f1.text = ::gFxErrorMsg 
			f1.height = 200 
			f1.multiline = true 
			local sb = dotnetclass "System.Windows.Forms.ScrollBars"
			f1.scrollbars = sb.both  
			f1.readonly = true
		)
	
		on btnEffectsDef pressed do
		(
			local p = #()
			if chckForceGet.state then 
				append p "-f"
			append p (RsConfigGetArtDir +"/VFX/rmptfx/...")
			append p (RsConfigGetArtDir core:true +"/VFX/rmptfx_tu/...")
			gRsPerforce.sync p
		)

		on btnEffectsFile pressed do
		(
			-- This syncs latest on the entityfx.dat, ptfx.zip and rpfs for all enabled targets
			syncList = #()
			
			local filename = RsGetEntityFxInfoPath()
			
			if not (doesFileExist filename) then
			(
				gRsUlog.LogWarning "No entityfx.dat found in this project or its core." context:filename
			)
			else
			(
				append syncList filename
			)
				
			append syncList (RsConfigGetAssetsDir()+"export/data/effects/ptfx.zip")
			
			numTargets = RsProjectGetNumTargets()			
			for idx = 0 to (numTargets-1) do
			(
				platformName = (RsProjectGetTargetKey idx)
				if (RsConfigGetTargetEnabled platformName) do
				(					
					append syncList ((RsProjectGetTargetDir platformName) + "/data/effects/ptfx.rpf")
				)					
			)
			gRsPerforce.sync syncList
		)
		
		on btnRebuild pressed do
		(
			if ::RsRebuildParticlesWithReport confirm:true then
				try(DestroyDialog ParticleErrorRollout)catch()
		)
	
	)
	CreateDialog ParticleErrorRollout width:400 height:500
)


fn RsGetEntityFxInfoPath confirm:false=
(
	filename = ((RsConfigGetProjRootDir core:true) + "titleupdate/" + gRsBranch.name + "/common/data/effects/entityfx.dat")
	if not (RsFileExists filename) then (
			gRsULog.LogWarning("File not found: "+ filename + ". Trying to sync from perforce")
			gRsPerforce.sync filename
	)

	if not (doesFileExist filename) do
	(
		filename = ( (RsConfigGetCommonDir core:true) + "/data/effects/entityFx.dat" )
		if not (RsFileExists filename) then (
				gRsULog.LogWarning("File not found: "+ filename + ". Trying to sync from perforce")
				gRsPerforce.sync filename
		)
	)
	
	if not (RsFileExists filename) then (
				gRsULog.LogWarning("File not found: "+ filename + ". Unable to sync from perforce. ")
				return false
	)
	
	gRsUlog.LogMessage ("Entityfx dat path found: " + filename)
	
	-- Return the path
	filename
)

fn RsRebuildParticlesWithReport confirm:false=
(
	try(DestroyDialog ParticleErrorRollout)catch()
	local errors = #()
	rageparticle_setValidEffectBlocks gTriggerKeys
	rageparticle_rebuild errors:errors
	::gFxErrorMsg = undefined

	local fxPath = RsGetEntityFxInfoPath()
	local files = #(fxPath, (RsConfigGetAssetsDir core:true) + "export/data/effects/ptfx.zip" )
	gRsPerforce.sync files silent:true

	if errors.count>0 then
	(
		::gFxErrorMsg = stringstream ""
		for e in errors do
		(
			format "%\n\n\r" e to:::gFxErrorMsg
		)
		local errtxt = "There have been errors building the effect database. You can\n1) Choose to ignore this if you're not working with particles, or \n2) Resolve the issue (YES)."
		RsNotifyPopUp_create text:errtxt title:"Effect build errors!" func:ShowErrorRollout TimeOut:10000
		return false
	)
	else if confirm then
	(
		RsNotifyPopUp_create text:"All effects built without problems." title:"Success!"
	)
	return true
)