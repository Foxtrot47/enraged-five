--
-- RSref Scene Info Browser
-- Rockstar Leeds
-- 31/01/2011
-- by Neal D Corbett
--
-- Tool to view scene's RSref stats
--

try (destroyDialog RSrefInfoRollout) catch ()

rollout RSrefInfoRollout "RSref Scene Info" width:320 height:300
(
	timer updateTimer "" interval:2000 active:false
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/RSref_Scene_Info" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255) offset:[8,-2]
	
	dotNetControl infoTreeCtrl "TreeView" align:#center pos:[5,20]
	
	struct objListNodeStruct (rootNode, rootNodeObj=undefined, rootSelObjs=#(), defNodes=#(), defs=#(), defNames = #(), defSelObjs=#(), defObjCounts=#())

	local DNknownColour = dotNetClass "system.Drawing.KnownColor"
	local DNcolour = dotNetClass "System.Drawing.Color"
	
	local selColour = DNcolour.fromKnownColor DNknownColour.MenuHighlight

	local textCol = (colorMan.getColor #windowText) * 255
	local windowCol = (colorMan.getColor #window) * 255
	
	local DNtextColour = DNcolour.FromArgb textCol[1] textCol[2] textCol[3]
	local DNbackColour = DNcolour.FromArgb windowCol[1] windowCol[2] windowCol[3]
		
	local hitNode, treeFont, treeFontBold
		
	local sceneList, overlapList, contsNode
	local contNames, contLists, contHelpers

	fn plural num = 
	(
		if (num == 1) then "" else "s"
	)
	
	fn treeCtrlSize size:[RSrefInfoRollout.width, RSrefInfoRollout.height] = 
	(
		infoTreeCtrl.width = size.x - 10
		infoTreeCtrl.height = size.y - 25
	)

	fn initTree = 
	(
		infoTreeCtrl.beginUpdate()
		treeCtrlSize()
		
		infoTreeCtrl.backColor = DNbackColour
		infoTreeCtrl.foreColor = DNtextColour
		
		sceneList = objListNodeStruct rootNode:(infoTreeCtrl.nodes.add "[RSrefs in scene]")
		
		contsNode = infoTreeCtrl.nodes.add "[Containers]"
		local nonCont = objListNodeStruct rootNode:(contsNode.nodes.add "[non-container]")
		contsNode.expand()
		
		contLists = #(nonCont)
		contNames = #("")
		contHelpers = #(undefined)

		overlapList = objListNodeStruct rootNode:(infoTreeCtrl.nodes.add "[RSrefs in multiple containers]")

		infoTreeCtrl.endUpdate()
	)
	
	fn getContNum contObj = 
	(
		if not isKindOf contObj container then 1 else
		(
			local findNum = findItem contHelpers contObj

			if (findNum == 0) do 
			(
				local newStruct = objListNodeStruct rootNode:(contsNode.nodes.add contObj.name) rootNodeObj:contObj
				newStruct.rootNode.collapse()
				
				append contNames contObj.name
				sort contNames
				findNum = findItem contNames contObj.name
				
				insertItem newStruct contLists findNum
				insertItem contObj contHelpers findNum
			)
			
			findNum
		)
	)
	
	fn addObjToList listStruct obj refname = 
	(
		local defNum = findItem listStruct.defs obj.refDef
		
		if (defNum == 0) do 
		(
			append listStruct.defNames refName
			sort listStruct.defNames
			defNum = findItem listStruct.defNames refName
			
			insertItem obj.refDef listStruct.defs defNum
			insertItem (listStruct.rootNode.Nodes.insert (defNum - 1) refName) listStruct.defNodes defNum
			insertItem 0 listStruct.defObjCounts defNum
			insertItem #() listStruct.defSelObjs defNum
		)
		
		append listStruct.rootSelObjs obj
		append listStruct.defSelObjs[defNum] obj
	)
	
	fn countsString listStruct nonCont:false = 
	(
		local objCount = 0
		for num in listStruct.defObjCounts do (objCount += num)
		local defCount = listStruct.defs.count
		
		if nonCont then 
		(
			"[" + objCount as string + " non-containered object" + plural objCount + ", " + defCount as string + " objectname" + plural defCount + "]"
		)
		else 
		(
			" (" + objCount as string + " object" + plural objCount + ", " + defCount as string + " objectname" + plural defCount + ")"
		)
	)
	
	fn updateObjList listStruct isCont:false = 
	(
		listStruct.rootnode.tag = dotNetMXSValue listStruct.rootSelObjs
		
		-- Check for obsolete tree-nodes: (in reverse, so it doesn't trip itself up when removing)
		for n = listStruct.defs.count to 1 by -1 do 
		(
			if (listStruct.defSelObjs[n].count == 0) then 
			(
				listStruct.defNodes[n].remove()
				deleteItem listStruct.defs n
				deleteItem listStruct.defNames n
				deleteItem listStruct.defObjCounts n
				deleteItem listStruct.defNodes n
				deleteItem listStruct.defSelObjs n
			)
			else 
			(
				listStruct.defNodes[n].tag = dotNetMXSValue listStruct.defSelObjs[n]
				local objCount = listStruct.defSelObjs[n].count

				-- Only update label if the object-count has changed:
				if listStruct.defObjCounts[n] != objCount do 
				(
					listStruct.defObjCounts[n] = objCount
					listStruct.defNodes[n].text = listStruct.defNames[n] + " ("+ objCount as string + " object" + plural objCount + ")"
				)
			)
		)
		
		if (listStruct.defs.count == 0) then  
		(
			case isCont of 
			(
				unsupplied:()
				true:
				(
					listStruct.rootnode.text = if 
					(
						case listStruct.rootNodeObj.getStatusString() of 
						(
							"Editing in place":false
							"Open":false
							"Unsaved":false
							Default:true
						)
					)
					then 
					(
						listStruct.rootNodeObj.name + " [closed, object-count unknown]"
					)
					else 
					(
						listStruct.rootNodeObj.name + " [contains no RSrefs]"
					)
				)
				false:
				(
					listStruct.rootnode.text = "[No non-containered RSrefs in scene]"
				)
			)
		)
		else 
		(
			case isCont of 
			(
				unsupplied:()
				true:
				(
					local objCount = 0
					for item in listStruct.defSelObjs do (objCount += item.count)
					
					listStruct.rootnode.text = listStruct.rootNodeObj.name + (countsString listStruct)
				)
				false:
				(
					listStruct.rootnode.text = countsString listStruct nonCont:true
				)
			)
		)
	)
	
	fn updateContsList = 
	(
		local contData, keepContData, isUnCont
		for contNum = contLists.count to 1 by -1 do 
		(
			contData = contLists[contNum]
			isUnCont = (contNum == 1)
			
			if isUnCont or ((contData.rootNodeObj != undefined) and (isValidNode contData.rootNodeObj)) then 
			(
				updateObjList contData isCont:(not isUnCont)
			)
			else 
			(
				-- Remove list-entry for missing containers:
				contData.rootnode.remove()
				deleteItem contLists contNum
				deleteItem contHelpers contNum
			)
		)
		
		local contCount = contLists.count - 1
		if (contCount == 0) then 
		(
			contsNode.text = "No containers found in scene"
		)
		else 
		(
			contsNode.text = ("Containers: " + contCount as string)
		)
	)
	
	fn updateCtrls = 
	(
		if not RSmaxfilingNow and RSrefInfoRollout.open do 
		(
			infoTreeCtrl.beginUpdate()
			
			for obj in helpers where isKindOf obj container do (getContNum obj)
			
			local refObjs = for obj in objects where (isRSref obj includeDelegates:true) collect obj
				
			-- Add object-lists for full-scene nodes:
			local allListStructs = join #(sceneList, overlapList) contLists
			
			for listStruct in allListStructs do 
			(
				listStruct.rootSelObjs = #()
				listStruct.defSelObjs = for item in listStruct.defs collect #()
			)
			
			-- Sort RSref objects into their relevant lists:
			local contNum, refName
			for obj in refObjs do 
			(
				local refName = case of 
				(
					(obj.refDef != undefined):(obj.refDef.name)
					(obj.objectName == ""):("[undefined]")
					default:(toLower obj.objectName)
				)

				addObjToList sceneList obj refName

				-- Add new container to list?
				contNum = getContNum obj.parent
				
				-- Add new unique object to container's list?
				addObjToList contLists[contNum] obj refName
			)

			-- Update scene-objects tree:
			updateObjList sceneList
			if (refObjs.count == 0) then 
			(
				sceneList.rootnode.text = "No RSref objects found in scene"
			)
			else 
			(
				sceneList.rootnode.text = ("RSrefObjects " + (countsString sceneList))
			)
			
			-- Update container-trees:
			updateContsList()
			
			-- Generate and update  multi-container-objects tree:
			local sceneListDefs = sceneList.defs
			local sceneListNames = sceneList.defNames
			
			local defContUseCounts = for item in sceneListDefs collect 0
			local findNum
			for n = 2 to contLists.count do 
			(
				for def in contLists[n].defs do 
				(
					findNum = findItem sceneListDefs def
					
					if (findNum != 0) do 
					(
						defContUseCounts[findNum] += 1
					)
				)
			)
			local overlapObjNums = (for n = 1 to sceneListDefs.count where (defContUseCounts[n] > 1) collect n) as bitarray
			local refDef, refName
			for n = overlapObjNums do 
			(
				refDef = sceneListDefs[n]
				refName = sceneListNames[n]
				
				local defNum = findItem sceneListDefs refDef
				for obj in sceneList.defSelObjs[n] do 
				(
					addObjToList overlapList obj refName
				)				
			)
			updateObjList overlapList
			if (overlapObjNums.numberSet == 0) then 
			(
				overlapList.rootnode.text = "No cross-container objectnames found"
			)
			else 
			(
				overlapList.rootnode.text = ("Cross-container objects " + (countsString overlapList))
			)

			infoTreeCtrl.endUpdate()
		)
	)
	
	fn selectObjs = 
	(
		local selObjs = hitNode.tag.value
		if (selObjs.count == 0) then 
		(
			clearSelection()
		)
		else 
		(
			select selObjs
		)
	)
	
	on infoTreeCtrl mouseDown arg do 
	(
		hitNode = infoTreeCtrl.GetNodeAt (dotNetObject "System.Drawing.Point" arg.x arg.y)

		if (hitNode != undefined) do --if a TreeView node was clicked,
		(
			-- on rightclick:
			if (arg.button.equals arg.button.right) do 
			(
				-- Highlight rightclicked node:
				infoTreeCtrl.selectedNode = hitNode
				
				rcmenu RSrefView_rclick
				(
					fn showSel = 
					(
						(hitNode.tag != undefined)
					)
					
					fn showList = 
					(
						showSel() and (hitNode.nodes.count != 0)
					)
					
					menuItem selObjsItm "Select objects" filter:showSel
					menuItem printListItm "Generate spreadsheet" filter:showList
					
					on selObjsItm picked do 
					(
						selectObjs()
					)
					
					on printListItm picked do 
					(
						local listItem, subItem, objNode, objList, objectName, refDef

						-- Build object-list						
						local objList = for n = 0 to (hitNode.nodes.count - 1) collect 
						(
							objNode = hitNode.nodes.item[n]
							objList = objNode.tag.value
							objectName = objList[1].objectname
							
							#(objectName, objList.count)
						)
						
						fn sortObjnameList v1 v2 = 
						(
							case of 
							(
								(v1[2] > v2[2]):-1
								(v1[2] < v2[2]):1
								default:0
							)
						)
						
						qsort objList sortObjnameList

						-- Excel enums:
						xlContinuous = 1; xlDash = -4115; xlDashDot = 4; xlDashDotDot = 5; xlDot = -4118; xlDouble = -4119; xlLineStyleNone = -4142; xlSlantDashDot = 13
						xlHairline = 1; xlMedium = -4138; xlThick = 4; xlThin = 2						
						xlDiagonalDown = 5; xlDiagonalUp = 6; xlEdgeBottom = 9; xlEdgeLeft = 7; xlEdgeRight = 10; xlEdgeTop = 8; xlInsideHorizontal = 12; xlInsideVertical = 11
						
						-- Create a new Excel document:
						local excel = CreateOLEObject "Excel.Application"
						excel.application.Workbooks.Add
						excel.ActiveSheet.Name = "RSref Scene Info"
						excel.visible = true
						
						-- Send the object-data to Excel:
						local columnNames = #("Objectname", "Count")
						local excelData = for n = 1 to columnNames.count collect (for item in objList collect item[n])
						
						(excel.application.Range("A1:B1")).value = columnNames
						(excel.application.Range("A2:B" + ((objList.count + 1) as string))).value = excelData
							
						-- Set up cell borders:
						local borders = (excel.application.Range("A1:B1")).Borders(xlEdgeBottom)
						borders.LineStyle = xlDouble
						borders.Weight = xlThick
						borders = (excel.application.Range("A1:A" + ((objList.count + 1) as string))).Borders(xlEdgeRight)
						borders.LineStyle = xlContinuous
						borders = (excel.application.Range("B1:B" + ((objList.count + 1) as string))).Borders(xlEdgeRight)
						borders.LineStyle = xlContinuous
						borders = (excel.application.Range("A1:B" + ((objList.count + 1) as string))).Borders(xlEdgeBottom)
						borders.LineStyle = xlContinuous

						-- Resize the columns to fit:
						(excel.application.Range("A1:B1")).entireColumn.AutoFit()
					)
				)
				
				popUpMenu RSrefView_rclick
			)
		)
	)
	
	on RSrefInfoRollout resized newSize do 
	(
		treeCtrlSize size:newSize
	)		
	
	on updateTimer tick do 
	(
		--try (updateCtrls()) catch (updateTimer.active = false)
		if not RSmaxfilingNow do (updateCtrls())
	)
	
	on RSrefInfoRollout open do 
	(
		initTree()
		updateCtrls()
		
		updateTimer.active = true
	)
	
	on RSrefInfoRollout close do 
	(
		gc light:true
		(dotnetClass "System.GC").Collect()
	)
)

createDialog RSrefInfoRollout style:#(#style_titlebar, #style_resizing, #style_sysmenu, #style_minimizebox)
