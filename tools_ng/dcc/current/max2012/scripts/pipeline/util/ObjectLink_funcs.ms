--
-- Object Link utility functions
-- @Author Gunnar Droege
-- @Description function to work with the Object Link Manager struct
--

------------------------------------------------------------------------
-- helpers
------------------------------------------------------------------------

struct ObjectLinkFuncs
(
	linkEnumType = (dotnetclass "RSG.ObjectLinks.LinkChannel").LinkChannelClothCollision.GetType(),
	enumVals = (dotnetclass "System.Enum").getValues linkEnumType,

	fn MakeGuid guidstr = 
	(
		if undefined==guidstr then
			undefined
		else
			dotnetobject "Guid" guidstr
	),

	fn GetEnumMember index = 
	(
		enumVals[index+1]
	),


	fn GetChannelEnumByName channelName =
	(
		for item in gRsSceneLinkTypeData do 
		(
			if channelName==item.name then
				return item.dotnetEnum
		)
		return undefined
	)
)
RsObjLinkFuncs = ObjectLinkFuncs()

------------------------------------------------------------------------
-- initialisation
------------------------------------------------------------------------

fn InitLinkManager force:false = 
(
	if undefined==::gRsLinkMgr or force then
	(
		::gRsLinkMgr = RsObjLinkMgr()
		::gRsLinkMgr.dotnetMgr = (dotnetobject "RSG.MaxUtils.MaxObjectLinkMgr" RsContentTree.contentTreeHelper gRsBranch gRsUlog.universalLogObj.logObj)
	)
	else
	(
		::gRsLinkMgr.SetBranch gRsBranch
	)
)