-- TextSearch
-- by Neal D Corbett, R*Leeds
-- 29/10/2010

global RSTextSearchResults
try (destroyDialog RSTextSearchResults) catch ()
try (destroyDialog RStextSearchBottom) catch ()
try (destroyDialog RStextSearchDebug) catch ()

rollout RStextSearchDebug "" ( )
rollout RStextSearchResults "" ( )

rollout RStextSearchBottom "File Contents"
(
	local minPaneHeight = 70
	local userPaneHeight = 160
	
	local clickItemNum
	
	listBox linesDisplayCtrl readOnly:false offset:[-10,0] items:#("")
	
	on linesDisplayCtrl selected clickNum do 
	(
		linesDisplayCtrl.selection = 0
	)
	
	on linesDisplayCtrl rightClick clickNum do 
	(
		if (clickNum != 0) do 
		(
			clickItemNum = clickNum
			
			rcmenu RStxtSearch_lineRtClk
			(
				menuItem copyLineItm "Copy line"
				
				fn clickedFileData = (::RStextSearchResults.foundFileData[::RStextSearchResults.fileNameList.selection])
				
				on copyLineItm picked do 
				(
					case of 
					(
						(clickItemNum == 1):
						(
							setclipboardText linesDisplayCtrl.selected
						)
						(clickItemNum > 2):
						(
							local clickItemNum = clickItemNum - 2
							local fileLines = (clickedFileData()).lines
							setclipboardText fileLines[clickItemNum].string
						)
					)
					linesDisplayCtrl.selection = 0
				)
			)
			
			if (clickNum != 2) do 
			(
				linesDisplayCtrl.selection = clickNum
				popUpMenu RStxtSearch_lineRtClk
			)
		)
	)
	
	on RStextSearchBottom resized newSize do 
	(
		if (linesDisplayCtrl.items.count > 1) do 
		(
			userPaneHeight = if (newSize.y < minPaneHeight) then minPaneHeight else newSize.y
		)
		
		if (RStextSearchResults.width != newSize.x) do 
		(
			RStextSearchResults.width = newSize.x
		)
		
		::RStextSearchResults.arrangeCtrls()
	)
)

rollout RStextSearchResults "R* Text Search" width:665
(
	local maxListDisplay = 30
	
	local dnIOFile = dotNetClass "System.IO.File"

	local minPaneSize = [356,129]
	local minFileNameListHeight = 46
	
	local linesDisplayCtrl = RStextSearchBottom.LinesDisplayCtrl
	local linesDisplayItems = #()
	local showBottom = false
	
	local foundFileData = #()
	
	dotNetControl RsBannerPanel "Panel" pos:[0,0] height:32 width:RStextSearchResults.width 
	local banner = makeRsBanner dn_Panel:RsBannerPanel wiki:"TextSearch" versionNum:1.10 versionName:"Last Butter" filename:(getThisScriptFilename())
	
	edittext searchTextBox "Search for:" across:2 height:18 -- height sets this to multi-line, allowing user to hit enter
	edittext searchFileBox "filename:" height:18
	edittext searchDirBox "folder:" text:"" offset:[0,-3] height:18
	
	button Searchbtn "Search" width:50 height:40 align:#right pos:SearchTextBox.pos
	
	local presetBtnWidth1 = 76
	local PresetFile1 = "*.ms;*.mcr"
	local PresetDir1 = @"$scripts\;$userMacros\;$wildwest\;$dcc\common\"
	button PresetBtn1 "R* Maxscript" align:#left tooltip:(substituteString PresetDir1 ";" "\n") width:presetBtnWidth1 across:5
		on PresetBtn1 pressed do (searchFileBox.text = PresetFile1; searchDirBox.text = PresetDir1)
	
	local presetBtnWidth2 = 56
	local PresetFile2 = "*.rb"
	local PresetDir2 = @"$toolslib\"
	button PresetBtn2 "R* Ruby" tooltip:(substituteString PresetDir2 ";" "\n") width:presetBtnWidth2 pos:(PresetBtn1.pos + [presetBtnWidth1, 0])
		on PresetBtn2 pressed do (searchFileBox.text = PresetFile2; searchDirBox.text = PresetDir2)
	
	local presetBtnWidth3 = 70
	local PresetFile3 = "*.ms;*.mcr"
	local PresetDir3 = @"X:\Art_Tools\3dsmax\scripts\;X:\Art_Tools\3dsmax\UI\MacroScripts\RSL\"
	button PresetBtn3 "Leeds script" tooltip:(substituteString PresetDir3 ";" "\n") width:presetBtnWidth3 pos:(PresetBtn2.pos + [presetBtnWidth2, 0])
		on PresetBtn3 pressed do (searchFileBox.text = PresetFile3; searchDirBox.text = PresetDir3)
	
	local presetBtnWidth4 = 50
	local PresetFile4 = "*.ms;*.mcr"
	local PresetDir4 = @"$max\"
	button PresetBtn4 "3DSMax" tooltip:(substituteString PresetDir4 ";" "\n") width:presetBtnWidth4 pos:(PresetBtn3.pos + [presetBtnWidth3, 0])
		on PresetBtn4 pressed do (searchFileBox.text = PresetFile4; searchDirBox.text = PresetDir4)
	
	local presetBtnWidth5 = 64
	local PresetFile5 = "*.xml"
	local PresetDir5 = @"$assets\export\levels\"
	button PresetBtn5 "Level XMLs" tooltip:(substituteString PresetDir5 ";" "\n") width:presetBtnWidth5 pos:(PresetBtn4.pos + [presetBtnWidth4 + 4, 0])
		on PresetBtn5 pressed do (searchFileBox.text = PresetFile5; searchDirBox.text = PresetDir5)
	
	button outputBtn "Print output" enabled:false width:70 align:#left tooltip:"Output found filenames to new text window" pos:[0,PresetBtn1.pos.y]

	progressbar searchProgress "" pos:(PresetBtn1.pos + [0,-2]) height:29 visible:false

	listbox fileNameList width:640 offset:[-8,2]
	
	fn doGC = 
	(
		gc light:true
		(dotnetclass "system.gc").collect()
	)
	
	fn arrangeCtrls = 
	(
		if not RStextSearchResults.open do (return false)
		
		if (RStextSearchResults.width < minPaneSize.x) do 
		(
			RStextSearchResults.width = minPaneSize.x
		)
		if (RStextSearchResults.height < minPaneSize.y) do 
		(
			RStextSearchResults.height = minPaneSize.y
		)
		
		RsBannerPanel.width = RStextSearchResults.width
		
		searchbtn.pos.x = RStextSearchResults.width - 57
		outputBtn.pos.x = RStextSearchResults.width - 77
		
		searchTextBox.width = ((searchbtn.pos.x - searchTextBox.pos.x) / 2) - 4
		searchFileBox.pos.x = searchTextBox.pos.x + searchTextBox.width + 54
		searchFileBox.width = searchbtn.pos.x - searchFileBox.pos.x - 4
		searchDirBox.width = searchbtn.pos.x - searchTextBox.pos.x + 18
		
		local newFileListHeight = RStextSearchResults.height - fileNameList.pos.y - 6
		if (newFileListHeight < minFileNameListHeight) do (newFileListHeight = minFileNameListHeight)
		
		fileNameList.height = newFileListHeight
		fileNameList.width = RStextSearchResults.width - fileNameList.pos.x - 6
		
		if not showBottom then 
		(
			destroyDialog RStextSearchBottom 
		)
		else
		(
			if not RStextSearchBottom.open do 
			(
				createDialog RStextSearchBottom style:#(#style_resizing)
			)
		)
		
		-- Arrange controls in bottom pane:
		if RStextSearchBottom.open do 
		(
			local newWidth = RSTextSearchResults.width-- + 1
			if (RStextSearchBottom.width != newWidth) do 
			(
				RStextSearchBottom.width = newWidth
				LinesDisplayCtrl.width = RStextSearchBottom.width - LinesDisplayCtrl.pos.x - 6
			)
			
			local newBottomPos = (getDialogPos RStextSearchResults) + [0, RStextSearchResults.height + 42]
			if ((getDialogPos RStextSearchBottom) != newBottomPos) do 
			(
				setDialogPos RStextSearchBottom newBottomPos
			)
			
			linesDisplayCtrl.items = linesDisplayItems
			
			if (linesDisplayItems.count <= 1) then 
			(
				LinesDisplayCtrl.height = 20
				RStextSearchBottom.height = LinesDisplayCtrl.pos.y + LinesDisplayCtrl.height + 5
			)
			else 
			(
				if (RStextSearchBottom.height != RStextSearchBottom.userPaneHeight) do 
				(
					RStextSearchBottom.height = RStextSearchBottom.userPaneHeight
				)
				
				LinesDisplayCtrl.height = RStextSearchBottom.height - LinesDisplayCtrl.pos.y - 5
			)
			
			LinesDisplayCtrl.width = RStextSearchBottom.width - LinesDisplayCtrl.pos.x - 5
		)
	)
	
	on RSTextSearchResults open do 
	(
		banner.setup()
		
		arrangeCtrls()
		
		SearchFileBox.text = PresetFile1
		SearchDirBox.text = PresetDir1
		setFocus SearchTextBox
	)
	
	on RSTextSearchResults resized val do (arrangeCtrls())
	on RSTextSearchResults moved val do (arrangeCtrls())

	on RSTextSearchResults close do 
	(
		destroyDialog RStextSearchBottom
	)

	fn GetSubDirFiles ThisDir Filenames = 
	(
		local SearchFiles = #()
		for Filename in Filenames do 
		(
			join SearchFiles (for File in (getFiles (ThisDir + Filename)) collect (RsMakeBackSlashes File))
		)
		for SubDir in (getDirectories (ThisDir + "*.*")) do (join SearchFiles (GetSubDirFiles SubDir Filenames ))
		SearchFiles
	)
	
	fn showFileLines num = 
	(
		local fileData = foundFileData[num]
		local newItems = #(fileData.filenames.long)		

		if (fileData.lines.count != 0) do 
		(
			append newItems ""
			
			for item in fileData.lines do 
			(
				append newItems ((formattedPrint item.num format:"-5d") + ":   " + item.string)
			)
		)

		linesDisplayItems = newItems
		arrangeCtrls()
	)
	
	-- Clicking found files in top pane will show the search-info in the bottom pane	
	on fileNameList selected num do 
	(
		if (num != 0) do 
		(
			showFileLines num
		)
	)
	-- Double-clicking found files will open them
	on fileNameList doubleClicked clickNum do 
	(
		if (clickNum != 0) do 
		(
			edit foundFileData[clickNum].filenames.long
		)
	)
	
	on fileNameList rightClick clickNum do 
	(
		if (clickNum != 0) do 
		(
			fileNameList.selection = clickNum
			showFileLines clickNum
			
			rcmenu RStxtSearch_fileRtClk
			(
				menuItem editItm "Edit File"
				menuItem copyPathItm "Copy Path"
				menuItem explorePathItm "Explore Path"
				menuItem p4Itm "Perforce check-out"
				
				fn clickedFile = foundFileData[fileNameList.selection].filenames.long

				on editItm picked do 
				(
					edit (clickedFile())
				)
				
				on p4Itm picked do 
				(
					::gRsPerforce.edit #(clickedFile())
				)
				
				on copyPathItm picked do 
				(
					setclipboardText (clickedFile())
				)

				on explorePathItm picked do 
				(
					shellLaunch "explorer.exe" ("/select," + (clickedFile()))
				)
			)
			
			popUpMenu RStxtSearch_fileRtClk
		)
	)
		
	local SearchString
	local SearchFiles
	local SearchDirs
	local SearchDirNums
	local timestart
	local TimeTaken
	local NextProgressUpdate
	local searchThreads
	
	local completedTasks
	
	on outputBtn pressed do 
	(
		local printText = stringStream ""
		format "% files found containing: \"%\" (search took % seconds)\n" foundFileData.count SearchTextBox.text TimeTaken to:printText

		for fileData in foundFileData do (format "%\n" fileData.filenames.long to:printText)
		
		RScreateTextWindow text:printText title:"R* Text Search Results"
	)
	
	fn enableControls Value = 
	(
		(
			for ctrl in RSTextSearchResults.controls where 
			(
				(matchPattern ctrl.name pattern:"PresetBtn*")
			) collect ctrl
		).visible = Value
		(
			for ctrl in RSTextSearchResults.controls where 
			(
				(isKindOf ctrl ButtonControl) or 
				(isKindOf ctrl EditTextControl and not ctrl.readOnly)
			) collect ctrl
		).enabled = Value
	)
	
	fn textSearch_internal justFiles:false = 
	(
		local searchContinue = True
		SearchProgress.visible = True
		
		for taskNum = 1 to searchFiles.count while (searchContinue and RSTextSearchResults.open) do 
		(
			local progressAmt = (taskNum as float)/SearchFiles.count
			if (progressAmt >= nextProgressUpdate) do 
			(
				searchProgress.value = 100.0 * progressAmt
				nextProgressUpdate += 0.01 -- Only update progress-bar every 1%
			)
			
			local thisFileName = searchFiles[taskNum]
			
			local filenames = (dataPair long:thisFileName short:"")
			fileData = (dataPair filenames:filenames lines:#())
			
			if justFiles then 
			(
				append foundFileData fileData
			)
			else 
			(
				local searchLines = dnIOFile.ReadAllLines thisFileName
				
				if (searchLines != undefined) do 
				(
					local foundLines = #()
					
					for lineNum = 1 to searchLines.count while searchContinue do
					(
						local thisLine = searchLines[lineNum]
						
						if (matchPattern thisLine pattern:searchString) do 
						(
							thisLine = trimleft ThisLine
							
							append fileData.lines (dataPair num:lineNum string:thisLine)
						)
						
						-- Clear away loaded lines as we go
						searchLines[lineNum] = undefined
					)
				)
				
				if (fileData.lines.count != 0) do 
				(
					append foundFileData fileData
				)
			)
			
			searchLines = undefined
			completedTasks[taskNum] = True
			
			searchContinue = searchContinue and (not keyboard.escPressed)
		)
		
		return searchContinue
	)
	
	fn textSearch_finish = 
	(
		-- Set short dir-names:
		for item in foundFileData do 
		(
			local foundDir = false
			local filename = item.filenames.long
			
			for searchDir in searchDirs while not foundDir do 
			(
				local matchDir = (RsMakeBackSlashes searchDir) + "*"
				
				if matchPattern filename pattern:matchDir do 
				(
					foundDir = true
					item.filenames.short = (substring filename matchDir.count -1)
				)
			)
		)
		
		enableControls true
		
		-- File-list has been searched, so show the results:
		searchThreads = #()
		SearchProgress.value = 100.0
		SearchProgress.visible = False

		setFocus RSTextSearchResults
		FileNameList.selection = 0
		
		-- Show the found-files list:
		if (foundFileData.count == 0) then (FileNameList.items = #()) else 
		(
			fileNameList.items = for fileData in foundFileData collect 
			(
				local lineCount = fileData.lines.count
				lineCount = if (lineCount == 0) then " " else ((formattedPrint lineCount format:"-4d") +  ":    ")
				
				lineCount + fileData.filenames.short
			)
		)
		
		-- Show the search-end status in the bottom pane:
		TimeTaken = ((timestamp() - timestart) * .001) as string
		linesDisplayItems = #((foundFileData.count as string) + " files searched in " + TimeTaken + " seconds")
		LinesDisplayCtrl.selection = 0
		
		arrangeCtrls()
	)
	
	fn textSearch = 
	(
		SearchTextBox.text = RsRemoveCharacterFromString SearchTextBox.text "\n"
		
		timestart = timestamp()
		OutputBtn.enabled = false -- Results-printing is only allowed if a search has been completed
		SetFocus Searchbtn -- Stops Escape from closing rollout, which it'll do if an EditText has focus, for some reason.
		SearchProgress.value = NextProgressUpdate = 0.0
		FileNameList.items = #()
		
		showBottom = true
		linesDisplayItems = #("(Esc to stop)")
		arrangeCtrls()
		
		SearchString = "*" + SearchTextBox.text + "*"
		SearchFiles = #()
		SearchDirs = #()
		SearchDirNums = #()
		SearchContinue = True
		
		local isOutsource = (gRsIsOutSource == true)
		
		-- Get filename definitions:
		if (searchFileBox.text == "") do (searchFileBox.text = "*.*")
		local fileNames = filterstring searchFileBox.text ";"
		for n = 1 to fileNames.count do 
		(
			-- If a filename-specification doesn't have an extension, assume it's a partial-name search and add wildcards:
			if not matchPattern fileNames[n] pattern:"*.*" do 
			(
				fileNames[n] = "*" + fileNames[n] + "*.*"
			)
		)

		for SearchPath in (filterString SearchDirBox.text ";") do 
		(
			local searchDir = RsMakeBackSlashes SearchPath
			
			-- Swap out symbolic directory names for the real things; I use some custom ones that Max can't otherwise handle
			if (matchPattern searchDir pattern:"$*") do 
			(
				local filteredPath = filterString searchDir "\\/"
				local pathStart = filteredPath[1]
				
				local realDir, nameLength
				case of 
				(
					(matchPattern pathStart pattern:"$wildwest"):
					(
						realDir = if isOutsource then undefined else RsConfigGetWildWestDir()
					)
					(matchPattern pathStart pattern:"$dcc"):
					(
						realDir = if isOutsource then undefined else RsGetRootDirForConfigSwitch()
					)
					(matchPattern pathStart pattern:"$toolslib"):
					(
						realDir = if isOutsource then undefined else RsConfigGetLibDir()
					)
					(matchPattern pathStart pattern:"$assets"):
					(
						realDir = if isOutsource then undefined else RsConfigGetAssetsDir()
					)
					Default:
					(
						realDir = (symbolicPaths.getPathValue pathStart) + "\\"
					)
				)
				
				if (realDir == undefined) then (searchDir = undefined) else 
				(
					searchDir = realDir
					for n = 2 to filteredPath.count do (append searchDir (filteredPath[n] + "\\"))
				)
			)
			
			searchDir = RsMakeSafeSlashes (getFilenamePath (searchDir + "/"))
			append searchDirs searchDir
				
			if (searchDir != undefined) do 
			(
				local getThoseFiles = getSubDirFiles searchDir fileNames

				join searchFiles getThoseFiles
				join searchDirNums (for n = 1 to getThoseFiles.count collect searchDirs.count)
			)
		)

		-- Set up the build threads:
		completedTasks = #{}
		
		enableControls false			
		
		SearchProgress.value = 0.0
		foundFileData = #()
		
		local isEmptySearch = (searchTextBox.text == "")
		
		textSearch_internal justFiles:isEmptySearch
		
		textSearch_finish()
	)
	
	on SearchBtn pressed do (TextSearch())
	
	on SearchTextBox changed newText do 
	(
		if ((matchpattern newText pattern:"*\n*") and (newText.count > 1)) do 
		(
			TextSearch()
		)
	)
	
	on SearchFileBox changed newText do 
	(
		if ((matchpattern newText pattern:"*\n*") and (newText.count > 1)) do 
		(
			SearchFileBox.text = RsRemoveCharacterFromString newText "\n"
			TextSearch()
		)
	)
	
	on SearchDirBox changed newText do 
	(
		if ((matchpattern newText pattern:"*\n*") and (newText.count > 1)) do 
		(
			SearchDirBox.text = RsRemoveCharacterFromString newText "\n"
			TextSearch()
		)
	)
	
	on RSTextSearchResults closed do 
	(
		doGC()
	)
)

createDialog RSTextSearchResults style:#(#style_titlebar, #style_resizing, #style_sysmenu, #style_minimizebox)
