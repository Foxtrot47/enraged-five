------------------------------------------------------------------------------
-- Globals
------------------------------------------------------------------------------
RS_dotNetLoadAssembly "DevIL.NET.dll"

------------------------------------------------------------------------------
-- Structure Definitions
------------------------------------------------------------------------------

struct RsDevilManager
(
	devil, 		
	
	-- call this before you start any I/O operations 
	fn init force:false = 
	(	
		devil = dotNetClass "DevIL.DevIL"
	),
	
	fn loadbitmap filename = 
	(
		bmpd = devil.LoadBitMap filename
		bmpd
	),
	
	fn savebitmap outfilename bmpd = 
	(
		devil.SaveBitMap outfilename bmpd
	),
	
	fn newbitmap outfilename width height = 
	(
		devil.NewBitMap outfilename width height
	),
	
	fn blit src dest destx desty destz srcx srcy scrz width height depth = 
	(
		devil.Blit src dest destx desty destz srcx srcy scrz width height depth
	)
)

global gRsDevilManager = RsDevilManager()