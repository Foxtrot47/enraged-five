--
-- File:: pipeline/util/SetPivotForBBox.ms
-- Description::
-- A quick way of finding a best fit pivot axis for a better aligned bounding box.
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- 01/09/2011
-- by Neal D Corbett
-- Replaced orientation technique with more-accurate minimal-box finding-method
--
-- 29/3/2010
-- by Marissa Warner-Wu
-- Adding in editable poly support.
--
-- 11/8/2003
-- by Greg Smith
-----------------------------------------------------------------------------

---------------------------------------------------------------------------
-- Go through all the objects in the selection on running the script
---------------------------------------------------------------------------
undo "set best pivot" on 
(
	for obj in selection where (isProperty obj "mesh") do 
	(
		local objChilds = for childObj in obj.children collect childObj
		
		-- Unparent child-objects:
		local objCont = RsGetObjContainer obj
		objChilds.parent = undefined
		
		local bestBox = RsFindMinBox obj rotZ:true

		-- Set the object's rotation to match the best bounding-box:
		local originalXform = obj.objecttransform
		obj.rotation = (inverse bestBox.transform).rotationpart
		obj.position = originalXform.translationpart
			
		obj.objectoffsetpos = point3 0 0 0 
		obj.objectoffsetrot = quat 0 0 0 1
		obj.objectoffsetscale = point3 1 1 1
		
		local newXform = inverse obj.objecttransform
		local newObjTrans = originalXform * newXform;
		
		obj.objectoffsetpos = newObjTrans.translationpart
		obj.objectoffsetrot = newObjTrans.rotationpart
		obj.objectoffsetscale = newObjTrans.scalepart
		
		local objXformWas = obj.transform

		-- Rotate object to align boundingbox with world-axis:	
		obj.rotation = eulerangles 0 0 0
		addModifier obj (Mesh_Select())
		
		resetXForm obj
		obj.transform = objXformWas
		
		-- Reparent child-objects:
		if (objCont != undefined) do 
		(
			objCont.addNodesToContent objChilds
		)
		objChilds.parent = obj
		
		OK
	)
)