--Script globals
idxCollisionType = getattrindex "Gta Collision" "Coll Type"

--
-- name: RsApplyCollisionTypeMappingToRexBoundMtl
-- desc: Updates collision type for a RexBoundMtl instance if there is a remapping
--
fn RsApplyCollisionTypeMappingToRexBoundMtl rex_bound_material collision_type_mappings_map = 
(
	collision_type = RexGetCollisionName rex_bound_material
	if( collision_type_mappings_map.containskey collision_type ) do-- do we have a remapping?
	(
		-- set the collision type to the value in the map
		target_collision_type = collision_type_mappings_map.item collision_type
		RexSetCollisionName rex_bound_material target_collision_type
	)
)

--
-- name: RsApplyCollisionTypeMappingToMultiMaterial
-- desc: Updates collision type for all RexBoundMtl instances in a MultiMaterial
--
fn RsApplyCollisionTypeMappingToMultiMaterial multi_material collision_type_mappings_map = 
(
	for sub_material in multi_material.materiallist do
	(
		if classof sub_material == RexBoundMtl then
		(
			RsApplyCollisionTypeMappingToRexBoundMtl sub_material collision_type_mappings_map
		)
	)
)

--
-- name: RsApplyCollisionTypeMappingToCollisionPrimitive
-- desc: Updates collision type in the attributes of a collision primitive
--
fn RsApplyCollisionTypeMappingToCollisionPrimitive obj collision_type_mappings_map = 
(
	collision_type = getattr obj idxCollisionType
	if( collision_type_mappings_map.containskey collision_type ) do-- do we have a remapping?
	(
		-- set the collision type to the value in the map
		target_collision_type = collision_type_mappings_map.item collision_type
		setattr obj idxCollisionType target_collision_type
	)
)

--
-- name: RsApplyCollisionTypeMappingsRecursive
-- desc: Updates gta collision types based on the remapping data recursively
--
fn RsApplyCollisionTypeMappingsRecursive objlist collision_type_mappings_map = 
(
	for obj in objlist do 
	(
		if( getattrclass obj == "Gta Collision" ) do
		(
			if( classof obj == Col_Mesh ) then
			(
				if obj.material != undefined then 
				(
					if classof obj.material == MultiMaterial then 
					(
						RsApplyCollisionTypeMappingToMultiMaterial obj.material collision_type_mappings_map
						
					)
					else if classof obj.material == RexBoundMtl then
					(
						RsApplyCollisionTypeMappingToRexBoundMtl obj.material collision_type_mappings_map
					)
				)
			)
			else
			(
				RsApplyCollisionTypeMappingToCollisionPrimitive obj collision_type_mappings_map
			)
		)
		
		if( obj.children.count > 0 ) then
		(
			RsApplyCollisionTypeMappingsRecursive obj.children collision_type_mappings_map
		)
	)
)

--
-- name: ParseCollisionTypeMappings
-- desc: Loads an xml document containing collision type mappings and returns a MaxDictionary which maps source types to target types
--
fn ParseCollisionTypeMappings filename = 
(
	result = dotnetobject "RSG.MaxUtils.MaxDictionary"
	
	local collisionTypeMappingsDoc = XmlDocument()
	try
	(
		collisionTypeMappingsDoc.load filename
		collisionTypeMappingsDoc.ParseIntoMaxscripthierarchy()
	)
	catch
	(
		format "Unable to load collision type mappings from file at %\n" filename
		--if anything goes wrong in the load we can safely return an empty container
	)
	
	for child in collisionTypeMappingsDoc.maxXmlObject.children do 
	(
		if child != undefined and child.tagName == "collision_type_mappings" do
		(
			for mappingElement in child.children do
			(
				if (mappingElement.tagName == "collision_type_mapping") and (mappingElement.attrs.containskey "source") and (mappingElement.attrs.containskey "target") do
				(
					result.add (mappingElement.attrs.item "source") (mappingElement.attrs.item "target")
				)
			)
		)
	)
	
	result
)

--
-- name: RsApplyCollisionTypeMappings
-- desc: Updates gta collision collision types based on the remapping data
--
fn RsApplyCollisionTypeMappings objlist = 
(
	collision_type_mappings_pathname = RsConfigGetEtcDir() + "config/generic/collision_type_mappings.xml"
	collision_type_mappings_map = ParseCollisionTypeMappings collision_type_mappings_pathname
	
	RsApplyCollisionTypeMappingsRecursive objlist collision_type_mappings_map
)
