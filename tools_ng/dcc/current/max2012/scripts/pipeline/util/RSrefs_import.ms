--
-- RSref map-importer
-- Creates RSrefs in positions loaded from a specified map metafile
--
-- Neal D Corbett
-- Rockstar Leeds
-- 29/03/2011
--

global RSrefMapImportFolder

(
	if (RSrefMapImportFolder == undefined) do 
	(
		global RSrefMapImportFolder = (RsConfigGetAssetsDir core:true) + "export/levels/*.xml"
	)
	
	local filename = getOpenFileName caption:"Import map-file:" filename:RSrefMapImportFolder types:"Map-export Metafile (*.xml)|*.xml" historyCategory:"RSrefMapImport"
	
	if (filename != undefined) do 
	(
		gc()
		RSrefMapImportFolder = (getFilenamePath filename) + "*.xml"
		
		local timeStart = timeStamp()
		
		local sceneXml = RsRefFuncs.loadSceneXml filename
		if (sceneXml == false) do (return false)
		
		local idxLodDistance = getattrindex "Gta Object" "LOD distance"
		
		local objList = #()
		local refObjList = #()
		local uniqueObjNames = #()
		
		local newObjs = #()
		
		fn buildObjList list = 
		(
			join objList list
			
			for obj in list do 
			(
				buildObjList obj.children
				
				if ((obj.class == "rsrefobject") or (obj.class == "xref_object")) do 
				(
					append refObjList obj
					appendIfUnique uniqueObjNames obj.refName
				)
			)
		)
		
		-- Build object-list, and load required models:
		buildObjList sceneXml.objects
		
		if (uniqueObjNames.count != 0) do 
		(
			-- Set all viewports to Wireframe mode, so it doesn't slow down while it's working:
			local viewSave = RSrefFuncs.viewportsToWire()
			
			RSrefFuncs.loadModels uniqueObjNames filterDupes:false debugFuncSrc:"[xml-loader]"

			progressStart "Importing RSrefs from XML:"
			
			local itemNum = 0
			local objCount = refObjList.count

			local notCancelled = true
			for obj in refObjList while (notCancelled = progressUpdate (100.0 * (itemNum += 1) / objCount)) do 
			(
				local objXform = RSrefFuncs.convertSceneXmlMatrix obj.nodeTransform
				
				local newObj = RSRefObject objectname:obj.refName transform:objXform
				
				setattr newobj idxLodDistance (obj.getLODDistance())
					
				append newObjs newObj
			)

			progressEnd()

			RSrefFuncs.instantiateRefs showProgress:false debugFuncSrc:"[xml-loader]"
			
			clearSelection()
			select newObjs
			gc()
			
			-- Retrieve previous viewport-settings:
			RSrefFuncs.restoreViewports viewSave
		)
		
		sceneXml = undefined
		
		local textWords = stringStream ""
		format "%\n% RSref objects imported\nImport took % seconds" filename newObjs.count ((timeStamp() - timeStart) / 1000.0) to:textWords
		
		format "%\n" textWords
		messageBox (textWords as string) title:"Import Completed" beep:False
	)
	
	OK
)