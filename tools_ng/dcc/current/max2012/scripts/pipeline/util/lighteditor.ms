--Light Editor
--Kevin Weinberg �2010 Rockstar San Diego
--Tool connects to the Remote Console, sending data into the game for real-time updating for lights.

filein "pipeline/util/lighteditor_cg.ms"
filein "pipeline/util/lighteditor_ng.ms"

-- Make sure both types are currently closed.
if lightEditorInstanceCG != undefined and lightEditorInstanceCG.mainForm != undefined then
(
	lightEditorInstanceCG.mainForm.Close()
)

if lightEditorInstanceNG != undefined and lightEditorInstanceNG.mainForm != undefined then
(
	lightEditorInstanceNG.mainForm.Close()
)

fn ShowLightEditorDialog = 
(
	-- Determine which one we should show.
	showCurrentGen = false

	connectionResult = RemoteConnection.Connect()
	if connectionResult == true then
	(
		consolePlatform = RemoteConnection.GetPlatform()
		consoleIP = RemoteConnection.GetGameIP()

		if ( consolePlatform != undefined and consoleIP != undefined ) then
		(
			if ( consolePlatform == "PlayStation3" or consolePlatform == "Xbox360") then
			(
				showCurrentGen = true
			)
		)
		
		print consolePlatform
		print consoleIP
	)
	
	if showCurrentGen == true then
	(
		lightEditorInstanceCG.CreateUI()

		callbacks.addScript #selectionSetChanged "lightEditorInstanceCG.OnSelectionChange()" id:#consoleSelectionChanged
		callbacks.addScript #selectedNodesPreDelete "lightEditorInstanceCG.OnNodeDeleted()" id:#consoleDelete	
		callbacks.addScript #nodeCreated "lightEditorInstanceCG.OnNodeCreated()" id:#consoleNodeCreated
		callbacks.addScript #nodeLinked "lightEditorInstanceCG.OnNodeLinked()" id:#consoleNodeLinked
		callbacks.addScript #nodeUnlinked "lightEditorInstanceCG.OnNodeUnlinked()" id:#consoleNodeUnlinked
		callbacks.addScript #filePreOpen "lightEditorInstanceCG.OnFileOpened()" id:#consoleFilePreOpen
	)
	else
	(
		-- Assume next gen.
		lightEditorInstanceNG.CreateUI()

		callbacks.addScript #selectionSetChanged "lightEditorInstanceNG.OnSelectionChange()" id:#consoleSelectionChanged
		callbacks.addScript #selectedNodesPreDelete "lightEditorInstanceNG.OnNodeDeleted()" id:#consoleDelete	
		callbacks.addScript #nodeCreated "lightEditorInstanceNG.OnNodeCreated()" id:#consoleNodeCreated
		callbacks.addScript #nodeLinked "lightEditorInstanceNG.OnNodeLinked()" id:#consoleNodeLinked
		callbacks.addScript #nodeUnlinked "lightEditorInstanceNG.OnNodeUnlinked()" id:#consoleNodeUnlinked
		callbacks.addScript #filePreOpen "lightEditorInstanceNG.OnFileOpened()" id:#consoleFilePreOpen
	)
)

ShowLightEditorDialog()
