--
-- File:: pipeline/util/lod_toolkit.ms
-- Description:: LOD Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 19 March 2010
--
-----------------------------------------------------------------------------

global RsLodToolkit
try CloseRolloutFloater RsLodToolkit catch()

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/helpers/TerrainLodder.ms"
filein "pipeline/ui/lodfiddler.ms"
filein "pipeline/util/lodscripthelpers.ms"
filein "pipeline/util/LodViewManipTool.ms"

struct RsLodSwapDataStruct 
(
	mat, matId, matName, hdShader, lodShader, 
	doFilenames = True, doShaders = True, doNormals = True,
	objMatNum = 0,
	
	-- Set default flags based on shader-data:
	fn setFlags = 
	(
		doShaders = (lodShader != undefined)
		
		doNormals = False
		for normName in #("normal_tnt", "terrain_cb_4lyr_cm_tnt") while (not doNormals) do 
		(
			doNormals = ((hdShader == normName) or (lodShader == normName))
		)
		
		OK
	),
	
	fn hasFlagged = (doFilenames or doShaders or doNormals)
)

struct RsLodSwapStruct
(
	queryForm = undefined,

	-- List of RsLodSwapDataStruct items:
	listItems = #(),
	
	-- Column-titles:
	listTitles = #("Shader", "Normals", "Filenames", "MatId", "Material", "Current Shader", "New Shader"),
	-- Values used for those columns:
	listItemValues = #(#doShaders, #doNormals, #doFilenames, #matId, #matName, #hdShader, #lodShader),
	tickBoxCount = 3, -- Those first columns are tickyboxes
	
	title = "Convert to LOD Materials",
	message = "Convert selected objects' materials to LOD versions:\n\nShaders: Change shaders to LOD-suitable equivalents.\nNormals: Do normal-related shader-tweaks.\nFilenames: Set texture filenames to _LOD equivalents, if found.",
	width = 600,
	
	-- This file lists shader/lod-shader mappings:
	LODShaderMapPath = (RsConfigGetToolsDir() + "etc/config/maps/" + (RsConfigGetProjectName core:True) + "/LODShaderMap.ini"),
	LODShaderMappings,
	
	fn showUI width:600 labels:#("Process", "Cancel") tooltips:#() defaultBtn:1 cancelBtn: = 
	(
		local padding = 10

		local buttonLabels = #("Process", "Cancel")
		if (cancelBtn == unsupplied) do 
		(
			cancelBtn = buttonLabels.count
		)
		
		-- Define form:
		queryForm = dotNetObject "MaxCustomControls.MaxForm"

		queryForm.tag = cancelBtn -- Choose cancel-button value by default
		queryForm.Text = title
		queryForm.Width = width
		queryForm.Height = 200
		
		-- Form is fixed-size by default:
		queryForm.FormBorderStyle = queryForm.FormBorderStyle.FixedDialog
		
		queryForm.MinimizeBox = false
		queryForm.MaximizeBox = false
		queryForm.StartPosition = queryForm.StartPosition.CenterScreen
		
		-- Button-press event transfers button-tag id to parent-form tag, allowing us to extract our return-value:
		fn buttonPressed sender arg = 
		(
			--print "Button Press"
			sender.Parent.Tag = sender.Tag
			sender.Parent.Close()   
		)
		
		-- Set up label-text:
		local lblMsg = dotNetObject "Label"
		(
			lblMsg.Font = lblMsg.DefaultFont
			lblMsg.Location.x = padding
			lblMsg.Location.y = padding
			
			lblMsgWidth = queryForm.Width - (2 * padding)
			lblMsg.Width = lblMsgWidth + 200 -- Allow for bits where our wrapping doesn't match label's auto-wrapping
			lblMsg.Height = 40
			lblMsg.Anchor = dotnet.combineEnums lblMsg.Anchor.Top lblMsg.Anchor.Left
			
			local wrappedMsg = RsWordwrap message lblMsgWidth font:lblMsg.Font delimiters:#(" ","\t","/")
			lblMsg.text = wrappedMsg
			
			local textSize = RsGetTextExtent wrappedMsg font:lblMsg.font
			lblMsg.Height = textSize.y
			
			queryForm.Controls.Add lblMsg
		)
		
		-- Add list:
		local itemList
		(
			-- Make list-dialog resizable:
			queryForm.FormBorderStyle = queryForm.FormBorderStyle.Sizable
			
			-- Make sure this is defined:
			RS_CustomDataGrid()
			
			local itemList = dotNetObject "RsCustomDataGridView"
			itemList.name = "itemList"
			
			itemList.Location.X = padding
			itemList.Location.Y = (lblMsg.Bottom + padding)
			itemList.Width = queryForm.Width - (2 * padding) - 16
			itemList.Height = 160

			itemList.ReadOnly = false
			itemList.SelectionMode = itemList.SelectionMode.FullRowSelect
			itemList.AllowUserToAddRows = false
			itemList.AllowUserToDeleteRows = false
			itemList.AllowUserToOrderColumns = false
			itemList.AllowUserToResizeColumns = false
			itemList.AllowUserToResizeRows = false
			itemList.AllowDrop = false
			itemList.MultiSelect = true
			
			itemList.DefaultCellStyle.WrapMode = itemList.DefaultCellStyle.WrapMode.True
			itemList.AutoSizeRowsMode = itemList.AutoSizeRowsMode.AllCells
			
			local textClr = (colorMan.getColor #windowText) * 255
			local windowClr = (colorMan.getColor #window) * 255
			itemList.DefaultCellStyle.backColor = (dotNetClass "System.Drawing.Color").FromArgb windowClr[1] windowClr[2] windowClr[3]
			itemList.DefaultCellStyle.foreColor = (dotNetClass "System.Drawing.Color").FromArgb textClr[1] textClr[2] textClr[3]
			itemList.AutoSizeColumnsMode = itemList.AutoSizeColumnsMode.Fill
			
			itemList.ColumnHeadersVisible = True
			
			-- Add checkbox/text columns:
			for n = 1 to listTitles.count do 
			(
				local headerText = listTitles[n]
				
				if (n <= tickBoxCount) then 
				(
					local checkCol = dotNetObject "DataGridViewCheckBoxColumn"
				
					checkCol.AutoSizeMode = checkCol.AutoSizeMode.None
					itemList.Columns.Add checkCol
					
					checkCol.width = 0
					
					checkCol.HeaderText = headerText
					checkCol.width = ((dotNetClass "TextRenderer").MeasureText headerText itemList.font).width + 8
					checkCol.SortMode = checkCol.SortMode.Automatic
				
					if (checkCol.width < 20) do 
					(
						checkCol.width = 20
					)
				)
				else 
				(
					local textCol = dotNetObject "DataGridViewTextBoxColumn"
					textCol.ReadOnly = true
					
					-- Set last column-size to fit to text:
					if (n != 1) and (n == listTitles.count) do 
					(
						textCol.AutoSizeMode = textCol.AutoSizeMode.AllCells
					)
					
					itemList.Columns.Add textCol
					textCol.HeaderText = listTitles[n]
				)
			)
			
			-- Add list-rows:
			for item in listItems do 
			(
				-- Get row-values from struct:
				local rowData = for propName in listItemValues collect (getProperty item propName)
				
				local rowIdx = itemList.Rows.Add rowData
				
				-- Tag row with struct:
				itemList.Rows.Item[rowIdx].tag = (dotNetMXSValue item)
			)
			
			-- Add callback that clears list-selection when the form is activated:
			fn clearSel sender args = 
			(
				for n = 0 to (sender.Controls.Count - 1) do 
				(
					local ctrl = sender.Controls.Item[n]
					
					if (ctrl.name == "itemList") do 
					(
						ctrl.clearSelection()
					)
				)
			)
			dotNet.addEventHandler queryForm "Activated" clearSel
			
			queryForm.Controls.Add itemList
			queryForm.Height = (itemList.Height + itemList.Location.Y + 72)
		)
		
		-- Define buttons:
		local lastBtn, timedBtn, timedBtnInfo
		local btnList = #()
		for btnNum = 1 to buttonLabels.count do 
		(
			local newBtn = dotNetObject "Button"
			newBtn.FlatStyle = newBtn.FlatStyle.System
			newBtn.AutoSize = True
			newBtn.text = buttonLabels[btnNum]
			newBtn.tag = btnNum
			
			-- Add tooltip to button, if specified:
			local btnTooltip = tooltips[btnNum]
			if (btnTooltip != undefined) do 
			(
				local tooltipObj = dotnetobject "System.Windows.Forms.ToolTip"
				tooltipObj.SetToolTip newBtn btnTooltip
			)
			
			-- Set button-focus order, so that default button has it first:
			local tabIdx = btnNum - defaultBtn
			if (tabIdx < 0) do (tabIdx += buttonLabels.count)
			newBtn.TabIndex = tabIdx
			newBtn.TabStop = True
			
			if (lastBtn == undefined) then 
			(
				local lastCtrl = queryForm.Controls.Item[queryForm.Controls.Count - 1]
				
				newBtn.Location.X = lastCtrl.Location.X
				newBtn.Location.Y = lastCtrl.Bottom + padding
			)
			else 
			(
				newBtn.Location = lastBtn.location
				newBtn.Location.X += lastBtn.width + padding
			)			
			
			-- If escape is pressed, the cancel-button's value is returned by form:
			if (btnNum == cancelBtn) then 
			(
				queryForm.CancelButton = newBtn
			)
			else 
			(
				dotnet.addEventHandler newBtn "Click" buttonPressed
			)
			
			append btnList newBtn
			lastBtn = newBtn
			queryForm.Controls.Add newBtn
		)
		
		queryForm.Height = queryForm.Controls.Item[queryForm.Controls.Count - 1].Bottom + Padding + 38
		
		-- Checkbox-change event sets bit in checkStates array:
		fn checkPressed sender arg = 
		(
			local tagInfo = sender.Tag.Value
			tagInfo.states[tagInfo.num] = sender.checked
		)
		
 		queryForm.MinimumSize = dotNetObject "System.Drawing.Size" queryForm.Width queryForm.Height
			
		-- Anchor controls to form's new size:
		for btn in btnList do
		(
			btn.Anchor = dotnet.combineEnums btn.Anchor.Bottom btn.Anchor.Left
		)
		
		if (itemList != undefined) do
		(
			itemList.Anchor = dotnet.combineEnums itemList.Anchor.Top itemList.Anchor.Left itemList.Anchor.Bottom itemList.Anchor.Right
		)
		
		-- Show form and wait for input:
		queryForm.ShowModal()
		
		local queryVal = queryForm.Tag
		queryForm.Dispose()
		queryForm = undefined

		local retVal = False
		
		if (queryVal == 1) do 
		(
			-- Transfer checkbox-states to list-items:
			local rows = itemList.rows
			
			for rowIdx = 0 to (rows.count - 1) do
			(
				local row = rows.Item[rowIdx]
				local rowData = row.Tag.Value
				
				-- Transfer checkbox-values to data-struct:
				for chkNum = 1 to tickBoxCount do 
				(
					local propName = listItemValues[chkNum]
					local val = row.cells.item[chkNum - 1].value
					
					setProperty rowData propName val
				)
			)
			
			retVal = True
		)
		
		return retVal
	),
	
		--////////////////////////////////////////////////////////////
	-- Swap shadder normal maps
	--////////////////////////////////////////////////////////////
	fn terrainNormalLODTextures mat = 
	(
		if (isKindOf mat Rage_Shader) do 
		(
			local shaderName = toLower (getFilenameFile (RstGetShaderName mat))
			case shaderName of
			(
				"normal_tnt":	--remove the normal map for this material
				(
					local bumpMap = for n=1 to (RstGetVariableCount mat) \
									where
									(
										RstGetVariableType mat n == "texmap" and \
										matchPattern (RstGetVariableName mat n) pattern:"Bump*"
									) \
									collect n
									
					if bumpMap.count > 1 then format "is this really a normal_tnt shader" --thats odd				
					
					--clear the normal map out
					RstSetVariable mat bumpMap[1] ""
									
				)
				
				"terrain_cb_4lyr_cm_tnt":	--swap the normal for a tiny proxy
				(
					local tinyFlatNormal = RsProjectGetArtDir() + "/textures/_Core Textures/TinyFlatNormal_n.bmp"
					
					if doesFileExist tinyFlatNormal then
					(
						local bumpMaps = for n=1 to (RstGetVariableCount mat) \
										where
										(
											RstGetVariableType mat n == "texmap" and \
											matchPattern (RstGetVariableName mat n) pattern:"Bump*"
										) \
										collect n
										
						for m=1 to bumpMaps.count do
						(
							RstSetVariable mat bumpMaps[m] tinyFlatNormal
						)
					)
					else 
					(
						procLogUpdate ("Missing TinyFlatNormal " + tinyFlatNormal)
					)
				)
			)
		)
	),
	
	--////////////////////////////////////////////////////////////
	-- Replace material old for new
	--////////////////////////////////////////////////////////////
	fn assignLODMaterial oldMat: id: newMat: =
	(
		--format "oldMat: % id: % newMat: %\n" oldMat id newMat
		if (isKindOf oldMat Multimaterial) then --its a multisub
		(
			oldMat.materialList[id] = newMat
		)
		else	--single
		(
			for node in refs.dependentNodes oldMat do
			(
				print (classOf node)
				node.material = newMat
			)
		)
	),
	
	fn setLodShader mat newShader = 
	(
		if (newShader == undefined) do return False
		
		-- Grab the first found diffuse/bump maps:
		local bestDiffuseMap, bestBumpMap

		for n=1 to (RstGetVariableCount mat) while (bestDiffuseMap == undefined) do 
		(
			if (RstGetVariableType mat n == "texmap") and (matchPattern (RstGetVariableName mat n) pattern:"Diffuse*") do 
			(
				bestDiffuseMap = RstGetVariable mat n
			)
		)
		for n=1 to (RstGetVariableCount mat) while (bestBumpMap == undefined) do 
		(
			if (RstGetVariableType mat n == "texmap") and (matchPattern (RstGetVariableName mat n) pattern:"Bump*") do 
			(
				bestBumpMap = RstGetVariable mat n
			)
		)
		
		-- Change the shader
		RstSetShaderName mat newShader
		
		--stuff the new shader with the maps based on the type
		case of
		(
			(newShader == "default"):
			(
				if (bestDiffuseMap != undefined) do (RstSetVariable mat 1 bestDiffuseMap)
			)					
			(matchPattern newShader pattern:"*normal*"):
			(
				if (bestDiffuseMap != undefined) do (RstSetVariable mat 1 bestDiffuseMap)
				if (bestBumpMap != undefined) do (RstSetVariable mat 7 bestBumpMap)
			)
		)
		
		return True
	),
	
	-- Load specifically-assigned shader/lod-shader mappings from 'LODShaderMapPath'
	fn GetLODShaderMappings = 
	(
		-- Skip if load has already been done:
		if (LODShaderMappings != undefined) do (return OK)
		
		-- Get list of shaders and the lod-shaders they should map to
		--	(empty arrays if 'LODShaderMapPath' can't be found)
		local MappedShaders = GetIniSetting LODShaderMapPath "lod_shader_map"
		local LodShaders = for Item in MappedShaders collect (GetIniSetting LODShaderMapPath "lod_shader_map" Item)
		
		LODShaderMappings = (DataPair Shaders:(for Item in MappedShaders collect (ToLower Item)) LodShaders:LodShaders)
	),
	
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--Find if there is a lod version of the input material shader 
	--Return an array if valid lod shaders are found
	--else return undefined
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn findShaderLodNames matIn objMatNum:0 =
	(
		local matData = #()
		if (isKindOf matIn MultiMaterial) then 
		(
			local mats = matIn.materialList
			local matIds = matIn.materialIdList
			
			join matData (for n = 1 to mats.count collect (dataPair mat:mats[n] matId:matIds[n]))
		)
		else 
		(
			append matData (dataPair mat:mat matId:"")
		)
		
		local outList = for item in matData where (isKindOf item.mat Rage_Shader) collect 
		(
			local mat = item.mat
			local matId = item.matId
			
			local HDName = toLower (getFilenameFile (RstGetShaderName mat))
			
			local LODName = HDName + "_lod"
				
			if not (RsShaderExists LODName) do 
			(
				-- Update mappings if required:
				GetLODShaderMappings()
				
				-- Was HDName specified by file LODShaderMapPath?
				local FindNum = (findItem LODShaderMappings.Shaders HDName)
				
				-- Special cases:
				case of
				(
					(FindNum != 0):
					(
						LODName = LODShaderMappings.LodShaders[FindNum]
					)
					(HDName == "terrain_cb_4lyr_2tex"):
					(
						LODName = "terrain_cb_4lyr_lod"
					)
					-- Swap all decal-shaders to "decal":
					(matchPattern HDName pattern:"*decal*"):
					(
						LODName = "decal"
					)
					-- Specular shaders:
					(matchPattern HDName pattern:"*spec*"):
					(
						LODName = "spec"
					)
					-- Swap all other shaders to Default:
					Default:
					(
						LODName = "default"
					)
				)
			)
			
			if (LODName == HDName) do 
			(
				LODName = undefined
			)
			
			local matItem = (RsLodSwapDataStruct mat:mat matName:mat.name matId:matId hdShader:HDName lodShader:LODName objMatNum:objMatNum)
			matItem.setFlags()
			
			matItem
		)
		
		return outList
	),
	
	fn convertToLodMats lodLevel:#lod = 
	(
		local selObjs = for obj in selection where (not isKindOf obj container) and (obj.material != undefined) collect obj
		
		if (selObjs.count == 0) do return false
		
		-- Collect materials from selected objects:
		local selMats = #()
		local selMatObjs = #()
		for obj in selObjs do 
		(
			--obj.mat = copy obj.mat
			local matNum = findItem selMats obj.mat
			
			if (matNum == 0) do 
			(
				append selMats obj.mat
				append selMatObjs #()
				matNum = selMats.count
			)
			
			append selMatObjs[matNum] obj
		)
		
		-- Set LOD/SLOD-related bits:
		local SLOD, suffix
		case lodLevel of 
		(
			#lod:
			(
				SLOD = false
				suffix = "_LOD"
			)
			#slod:
			(
				SLOD = True
				suffix = "_SLOD"
				
				title = substituteString title "LOD" "SLOD"
				message = substituteString message "LOD" "SLOD"
			)
		)

		-- Let's list what possibilities we've got:
		listItems = #()
		for matNum = 1 to selMats.count do
		(
			local swapDataList = findShaderLodNames selMats[matNum] objMatNum:matNum
			join listItems swapDataList
		)
		
		-- Show report: (abort function if cancelled)
		if not showUI() do return False

		-- Only process items that have flagged values:
		local processList = for item in listItems where (item.hasFlagged()) collect item
		
		-- Which object-materials have changed?
		local changedMatNums = #{}
		local changedMatsPerMat = #()
			
		for item in listItems where (item.hasFlagged()) do 
		(
			local objMatNum = item.objMatNum
			
			changedMatNums[objMatNum] = True
			
			if changedMatsPerMat[objMatNum] == undefined do 
			(
				changedMatsPerMat[objMatNum] = #()
			)
			
			append changedMatsPerMat[objMatNum] item
		)
		
		-- Process objects/multimats with ticked materials:
		for objMatNum = changedMatNums do 
		(
			local objs = selMatObjs[objMatNum]
			
			local processItems = changedMatsPerMat[objMatNum]
			local processItemMats = for item in processItems collect item.mat
			
			local objsMat = objs[1].material
			
			-- Get material-list:
			local isMultiMat = (isKindOf objsMat MultiMaterial)
			local subMats = if isMultiMat then 
			(
				objsMat.MaterialList
			)
			else 
			(
				#(objsMat)
			)
			
			local newSubMats = for thisMat in subMats collect 
			(
				local processIdx = findItem processItemMats thisMat
				
				-- Collect processed copy of submaterial (or original if it isn't due to be processed)
				if (processIdx == 0) then thisMat else 
				(
					local processItem = processItems[processIdx]
					
					--copy of the material to work on
					local copyMat = copy thisMat
					
					--Check the name if it needs a lod suffix
					local matName = thisMat.name
					if not matchpattern matName pattern:("*" + suffix) then
					(
						copyMat.name = matName + suffix
					)
					else
					(
						copyMat.name = matName
					)
					
					if (processItem.doShaders) do 
					(
						setLodShader copyMat processItem.lodShader
					)

					-- Swap filenames for lod-versions:
					if (processItem.doFilenames) do 
					(
						RsSetMaterialTexturesToLod copyMat SLOD:SLOD
					)
					
					-- Set up terrain-normal bits:
					if (processItem.doNormals) do 
					(
						terrainNormalLODTextures copyMat
					)
					
					copyMat
				)
			)
			
			-- Apply changed material to objects:
			if isMultiMat then 
			(
				local newMat = (copy objsMat)
				newMat.materialList = newSubMats
				objs.material = newMat
			)
			else 
			(
				objs.material newSubMats[1]
			)
		)
		
		--gRsULog.Flush()
		--gRsULog.ShowULogFile()
		messagebox "LOD Material Conversion finished" title:"Done"

		return OK
	),
	
	on create do
	(
		gRsPerforce.sync #(LODShaderMapPath)
	)
	
)--end Struct

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- Banner-rollout
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout RsLodToolkitBannerRoll ""
(
	dotNetControl RsBannerPanel "Panel" pos:[6,3] height:32 width:(RsLodToolkitBannerRoll.width - 6)
	local bannerStruct = makeRsBanner dn_Panel:RsBannerPanel wiki:"LOD_Toolkit"
	
	-- Refresh banner-icons when floater's rollouts are adjusted:
	on RsBannerPanel Paint Sender Args do 
	(
		for n = 0 to (RsBannerPanel.Controls.Count - 1) do 
		(
			RsBannerPanel.Controls.Item[n].Invalidate()
		)
	)
	
	--------------------------------------------------------------
	-- Rollout open/close
	--------------------------------------------------------------
	on RsLodToolkitBannerRoll open do
	(
		-- Initialise tech-art banner:
		bannerStruct.setup()
	)
)

rollout RsTreeLodGeneratorRoll "Tree LOD Generator"
(
	label lblComboLodder "This tool has been replaced with the\n     Combined LOD Generator tool:" height:26 offset:[90,-2]
	button btnComboLodder "Combo-Lodder" width:128 height:24
	
	on btnComboLodder pressed do 
	(
		-- Runs ComboLodder menu-command directly:
		RsRunMacro #lodcombinertool_ui
	)
		
	-- Save rolled-up state:
	on RsTreeLodGeneratorRoll rolledUp down do 
	(
		RsSettingWrite "RsTreeLodGeneratorRoll" "rollup" (not down)
	)
)

rollout RsLodHierarchySeperator "LOD Hierarchy Seperator"
(
	label lblLODHierSep "This will physically seperate the\n various levels of scene LOD\n (Orphan, HD, LOD, SLOD1)" height:45 offset:[150,-2]
	 
	slider sdrLODHierSep "Distance" range:[-100,100,0] ticks:20 type:#integer
	
	local PreviousValue = 0
	
	--Note we don't move the HD geo at all
	on sdrLODHierSep changed NewValue do
	(
		MoveValue = NewValue - PreviousValue
		
		--Move the Orphan level first
		for Obj in Objects where RsIsOrphan Obj do
		(
			SetTransformLockFlags Obj #None
			Move Obj [0,0,-(MoveValue)]
		)
		
		--Then LOD level
		for Obj in Objects where RsIsLOD Obj do
		(
			SetTransformLockFlags Obj #None
			Move Obj [0,0,MoveValue]
		)
				
		--Then SLOD level
		SLODMoveValue = MoveValue * 2
		for Obj in Objects where RsIsSuperLOD Obj do
		(
			SetTransformLockFlags Obj #None
			Move Obj [0,0,SLODMoveValue]
		)
		
		--Then SLOD2 ( RsContainerLodRef )
		SLOD2MoveValue = MoveValue * 2
		for Obj in Objects where RsIsSuperLOD Obj do
		(
			local lodParent = RsSceneLink.getparent LinkType_LOD Obj
			if( lodParent != Undefined ) then
			(
				SetTransformLockFlags Obj #None
				print SLOD2MoveValue
				print lodParent.Position
				Move lodParent [0,0,SLOD2MoveValue]
				print lodParent.Position
			)
		)
		
		PreviousValue = NewValue
	)
		
-- 	on btnComboLodder pressed do 
-- 	(
-- 		-- Runs ComboLodder menu-command directly:
-- 		RsRunMacro #lodcombinertool_ui
-- 	)
-- 		
-- 	-- Save rolled-up state:
-- 	on RsTreeLodGeneratorRoll rolledUp down do 
-- 	(
-- 		RsSettingWrite "RsTreeLodGeneratorRoll" "rollup" (not down)
-- 	)
)

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- SCRIPT HELPERS
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout LodScriptsRoll "Script Helpers"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/LOD_Toolkit#Script_Helpers" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	local btnWidth = 128
	local btnHeight = 24
	
	group "LOD Materials:"
	(
		button btnConvertToLodMats "Convert to LOD mats" width:btnWidth height:btnHeight offset:[-2,0] across:2 tooltip:"Convert materials for selected objects to use appropriate LOD shaders and textures (if found)"
		button btnConvertToSlodMats "Convert to SLOD mats" width:btnWidth height:btnHeight offset:[2,0] tooltip:"Convert materials for selected objects to use appropriate SLOD/SLOD1 shaders and textures (if found)"
	)
	
	group "Guess LOD Distance:"
	(
		spinner spnPercentage "%:" range:[100,1000,180] offset:[0,0] type:#integer across:2 align:#left
		spinner spnUnits "Units:" range:[0,5000,30] offset:[-55,0] type:#integer align:#center
		radiobuttons rbtnExtraDistType "LOD Distance Calculation:" width:150 height:50 align:#left across:2 labels:#("Default", "%", "Units", "% and Units") default:4 columns:2
		checkbox chkIgnoreZ "Ignore Z" checked:false align:#right offset:[0,25]
		button btnGuess "Guess LOD Distance" width:btnWidth height:btnHeight offset:[0,-4]
	)
	
	button btnTest "Test LOD Name" width:btnWidth height:btnHeight align:#center
	button btnParent "Parent by Name & Pos" width:btnWidth height:btnHeight across:2 align:#left
	
	progressbar pbarHelper
	
	
	--////////////////////////////////////////////////////////////
	-- FUNCTIONS
	--////////////////////////////////////////////////////////////

	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	--////////////////////////////////////////////////////////////
	--
	--////////////////////////////////////////////////////////////
	on btnConvertToLodMats pressed do undo "convert to LOD materials" on 
	(
		local lodSwapper = RsLodSwapStruct()
		lodSwapper.convertToLodMats lodLevel:#lod
	)
	
	--////////////////////////////////////////////////////////////
	--
	--////////////////////////////////////////////////////////////
	on btnConvertToSlodMats pressed do undo "convert to SLOD materials" on 
	(
		local lodSwapper = RsLodSwapStruct()
		lodSwapper.convertToLodMats lodLevel:#slod
	)
	
	--////////////////////////////////////////////////////////////
	--
	--////////////////////////////////////////////////////////////
	on btnGuess pressed do (
	
		RsLodGuessLodDist selection rbtnExtraDistType.state chkIgnoreZ.state spnUnits.value spnPercentage.value
	)
	
	--////////////////////////////////////////////////////////////
	--
	--////////////////////////////////////////////////////////////
	on btnTest pressed do (
		pbarHelper.value = 0		
		GtaTestForLodNameWithBar pbarHelper
		pbarHelper.value = 0
	)
	
	--////////////////////////////////////////////////////////////
	--
	--////////////////////////////////////////////////////////////
	on btnParent pressed do (
		filein "pipeline/util/setlodbynameandpos.ms"
	)
	
	--////////////////////////////////////////////////////////////
	---- Save rolled-up state:
	--////////////////////////////////////////////////////////////
	on LodScriptsRoll rolledUp down do 
	(
		RsSettingWrite "LodScriptsRoll" "rollup" (not down)
	)
)

-- Rollout added for GTA5 Bugstar #11354, this is just a cc of the rollour found in the object selection tool
rollout LodValueToolKit "LOD Value"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	spinner minlod "Minimum" range:[0,4000,0]
	spinner maxlod "Maximum" range:[0,4000,4000]
	button buttonLODValue "Select by" offset:[-50,0]
	button buttonColourLODHierarchy "Colour Wire by" offset:[30,-27]
	button buttonSelectNoChildren "Select With No Children"
	button buttonSelectUnlodded "Select Unlodded"
	
	fn colourlodchildren parent level colmultiple = (
	
		parent.wirecolor = (color 255 0 0) + (level * (color 0 colmultiple colmultiple))
		
		childnodes = getLODChildren parent
		
		for child in childnodes do (
			colourlodchildren child (level + 1) colmultiple 
		)
	)
	
	fn GtaGetConsideredSet setToAdd = (
		if GtaSelectaSelectedOnly == false then (
			GtaGetEverything setToAdd rootnode
		) else (
			for selobj in selection do (
				append setToAdd selobj
			)
		)
	)
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on buttonColourLODHierarchy pressed do (
		for obj in $objects do (
			parentnode = RsSceneLink.getparent LinkType_LOD obj
			childnodes = getLODChildren obj
			
			if parentnode == undefined then (
			
				if childnodes.count == 0 then (
					obj.wirecolor = (color 0 0 255)
				) else (
					
					maxlevels = getMaxLODLevels obj
				
					maxlevels = maxlevels - 1
				
					colmultiple = 255 / maxlevels
				
					colourlodchildren obj 0 colmultiple
				)
			)
		)
	)
	
	on buttonSelectNoChildren pressed do (
		selset = #()
	
		for obj in $objects do (
			childnodes = getLODChildren obj
			
			if childnodes.count == 0 then (
				append selset obj
			)
		)	
		
		select selset
	)
	
	on buttonSelectUnlodded pressed do (
	
		selset = #()
	
		for obj in $objects do (
			childnodes = getLODChildren obj
			
			if childnodes.count == 0 and (RsSceneLink.getparent LinkType_LOD obj == undefined) then (
				append selset obj
			)
		)	
		
		select selset		
	)
	
	on buttonLODValue pressed do (

		selset = #()

		considerSet = #()
		GtaGetConsideredSet considerSet

		for obj in considerSet do (
			
			fieldValue = RsGetObjLodDistance obj

			if fieldValue > minlod.value and fieldValue < maxlod.value then (

				append selset obj
			)
		)

		select selset
	)
	
	-- Save rolled-up state:
	on LodValueToolKit rolledUp down do 
	(
		RsSettingWrite "LodValueToolKit" "rollup" (not down)
	)
)

(
	RsLodToolkit = newRolloutFloater "LOD Toolkit" 300 750 50 96
	
	addRollout RsLodToolkitBannerRoll RsLodToolkit border:False
	
	for thisRoll in #(LodScriptsRoll, RsLodViewToolRoll, RsLodHierarchySeperator, TerrainLodder_roll, RsTreeLodGeneratorRoll, objlodset, LodValueToolKit) do 
	(
		addRollout thisRoll RsLodToolkit rolledup:(RsSettingsReadBoolean thisRoll.name "rollup" false)
	)
)