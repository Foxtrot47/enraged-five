
global gRsBatchFramework

-- The current max file that is being processed by a job.
global gRsBatchJobCurrentMaxFile	= undefined

-- Tracks the state of the current job.  If true, the job is successful, if false, something went wrong.
global gRsBatchJobCurrentState		= true

-- If set to true, sends a signal to the batch framework to stop the entire queue.
global gRsBatchJobCancel			= false


struct RsBatchJob
(
	/*
	Defines a job for the batch framework.
	*/
	
	public
	
	-- The name of the job.
	name				= undefined,
	
	-- If true, job processed successfully.  If false, it either failed or has yet to be processed.
	status				= false,
	
	-- Script that will be run on each max file.
	jobMaxscript		= undefined,
	
	-- Scripts that will be run before and after the entire job.
	preJobMaxscript		= undefined,
	postJobMaxscript	= undefined,
	
	-- Scripts that will be run before and after each max file is processes.
	preTaskMaxscript	= undefined,
	postTaskMaxscript	= undefined,
	
	-- The max files to process for the job.
	maxFiles			= #(),
	
	-- Store the current exception for the job if something failed.
	currentException	= undefined,
	
	
	private
	
	autoSaveState 		= undefined,
	rsRefsUpdateState	= undefined,
	
	fn getLatestJobMaxscriptFiles = (
		local mxsFiles = #()
		
		if this.jobMaxscript != undefined then (
			appendIfUnique mxsFiles this.jobMaxscript
		)
		
		if this.preJobMaxscript != undefined then (
			appendIfUnique mxsFiles this.preJobMaxscript
		)
		
		if this.postJobMaxscript != undefined then (
			appendIfUnique mxsFiles this.postJobMaxscript
		)
		
		if this.preTaskMaxscript != undefined then (
			appendIfUnique mxsFiles this.preTaskMaxscript
		)
		
		if this.postTaskMaxscript != undefined then (
			appendIfUnique mxsFiles this.postTaskMaxscript
		)
	),
	
	fn resetScene = (
		resetMaxFile #noPrompt
		
		viewport.setLayout #layout_1
		viewport.setRenderLevel #Wireframe
		viewport.setGridVisibility 1 false
		
		-- Close any open dialogs
		MatEditor.Close()
		renderSceneDialog.close()
		
		-- Close any open Scene Explorers
		local numSceneExplorers = SceneExplorerManager.GetExplorerCount()
		
		for i = 1 to numSceneExplorers do (
			local explorerName = SceneExplorerManager.GetExplorerName i
			SceneExplorerManager.CloseExplorer explorerName
		)
		
		gc()
	),
	
	fn preJob = (
		local result = true
		
		-- Make sure we have the latest versions of any job maxscript files.
		this.getLatestJobMaxscriptFiles()
		
		-- Deal with autosave.
		while ( autosave.isactive() == true ) do ()
		this.autoSaveState = autosave.Enable
		autosave.Enable = false
		
		-- Store RsRef update state and then turn it off.
		this.rsRefsUpdateState = RSrefAllowUpdate
		RSrefAllowUpdate = false
		
		if this.preJobMaxscript != undefined then (
			try (
				filein this.preJobMaxscript
				
			) catch (
				this.currentException = "[Pre Job] " + ( getCurrentException() )
				print ("Pre Job: "+ this.currentException)
				result = false
			)
		)
		
		result
	),
	
	fn postJob = (
		local result = true
		
		if this.postJobMaxscript != undefined then (
			try (
				filein this.postJobMaxscript
				
			) catch (
				this.currentException = "[Post Job] " + ( getCurrentException() )
				result = false
			)
		)
		
		-- Reset pre-job autosave state.
		autosave.Enable = this.autoSaveState
		autosave.resettimer()
		
		-- Reset the current batch max filename.
		gRsBatchJobCurrentMaxFile = undefined
		
		-- Reset the job state.
		gRsBatchJobCurrentState = true
		
		-- Reset RsRef system update to previous state.
		RSrefAllowUpdate = this.rsRefsUpdateState
		
		resetMaxFile #noPrompt
		
		result
	),
	
	fn preTask = (
		local result = true
		
		this.resetScene()
		
		print ("Pre Task: " + this.preTaskMaxScript as string)
		if this.preTaskMaxscript != undefined then (
			try (
				filein this.preTaskMaxscript
				
			) catch (
				
				this.currentException = "[Pre Task] " + ( getCurrentException() )
				print this.currentException
				result = false
			)
		)
		
		result
	),
	
	fn postTask = (
		local result = true
		
		if this.postTaskMaxscript != undefined then (
			try (
				filein this.postTaskMaxscript
				
			) catch (
				this.currentException = "[Post Task] " + ( getCurrentException() )
				result = false
			)
		)
		
		result
	),
	
	
	public
	
	fn addMaxFile maxFile = (
		appendIfUnique this.maxfiles maxFile
	),
	
	fn run = (
		if ( this.preJob() ) == true then (
			print ("job MaxScript: " + this.jobMaxScript as string)
			if this.jobMaxscript != undefined then (
				if this.maxFiles.count > 0 then (
					for maxFile in this.maxFiles where gRsBatchJobCurrentState == true do (
						gRsBatchFramework.logMessage ( "[Job: " + this.name + "] Processing task: " + maxFile )
						gRsBatchJobCurrentMaxFile = maxFile
						
						if ( this.preTask() ) == true then (
						
							try (
								print ("MaxScript: " + this.jobMaxScript as string)
								filein this.jobMaxscript
								
							-- Capture exception so that we can debug what went wrong later.
							) catch (
								this.currentException = "[Task: " + ( getFilenameFile maxFile ) + "]" + ( getCurrentException() )
								print ("Job Exception: " + this.currentException)
								gRsBatchJobCurrentState = false
							)
							
							if gRsBatchJobCurrentState == true then (
								gRsBatchJobCurrentState = this.postTask()
							)
							
						) else (
							gRsBatchJobCurrentState = false
						)
					)
					
				) else (
					gRsBatchFramework.logError ( "No maxfiles to process for job (" + this.name + ")." )
					gRsBatchJobCurrentState = false
				)
			)
			
			if gRsBatchJobCurrentState == true then (
				gRsBatchJobCurrentState = this.postJob()
			)
			
		) else (
			gRsBatchJobCurrentState = false
		)
		
		gRsBatchJobCurrentState
	)
)

struct RsBatchFramework
(
	private
		
	currentJob		= undefined,
	ulog			= sRsULog(),
	ulogFilename	= ( RsConfigGetLogDir() + "maxBatchFramework.ulog" ),
	
	localStatusFile	= ( PathConfig.GetDir #temp ) + "\\localBatchFrameworkStatus.xml",
	
	

	
	fn saveJobsToXml filename useJobStatus:false = (
		local fstream = createFile filename
		
		format "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" to:fstream
		format "<jobs>\n" to:fstream
		
		if useJobStatus == true then (
			format "\t<!-- These jobs did not get processed or failed. -->\n" to:fstream
		)
			
		for job in this.jobs do (
			local writeJob = true
			
			-- Don't write job if we are writing this as a current status of the overall process.
			if useJobStatus == true and job.status == true then (
				writeJob = false
			)
			
			if writeJob == true then (
				format "\t<job name=\"%\">\n" job.name to:fstream
				
				local preJobMaxScript = ""
				local postJobMaxscript = ""
				local jobMaxscript = ""
				local preTaskMaxscript = ""
				local postTaskMaxscript = ""
				
				if job.preJobMaxScript != undefined then preJobMaxScript = job.preJobMaxScript
				if job.postJobMaxscript != undefined then postJobMaxscript = job.postJobMaxscript
				if job.jobMaxscript != undefined then jobMaxscript = job.jobMaxscript
				if job.preTaskMaxscript != undefined then preTaskMaxscript = job.preTaskMaxscript
				if job.postTaskMaxscript != undefined then postTaskMaxscript = job.postTaskMaxscript
				
				format "\t\t<preJobMaxscript>%</preJobMaxscript>\n" preJobMaxscript to:fstream
				format "\t\t<postJobMaxscript>%</postJobMaxscript>\n" postJobMaxscript to:fstream
				format "\t\t<jobMaxscript>%</jobMaxscript>\n" jobMaxscript to:fstream
				format "\t\t<preTaskMaxscript>%</preTaskMaxscript>\n" preTaskMaxscript to:fstream
				format "\t\t<postTaskMaxscript>%</postTaskMaxscript>\n" postTaskMaxscript to:fstream
				
				format "\t\t<maxFiles>\n" to:fstream
				
				for maxFile in job.maxFiles do (
					format "\t\t\t<maxFile>%</maxFile>\n" maxFile to:fstream
				)
				
				format "\t\t</maxFiles>\n" to:fstream
				
				if useJobStatus == true and job.currentException != undefined then (
					format "\t\t<error>\n\t\t%\n\t\t</error>\n" job.currentException to:fstream
				)
				
				format "\t</job>\n" to:fstream
			)
		)
		
		format "</jobs>\n" to:fstream
		
		close fstream
	),
	
	fn saveCurrentBatchStatus = (
		this.saveJobsToXml this.localStatusFile useJobStatus:true
	),
	
	fn restoreUnfinishedJobsQueue = (
		local unfinishedJobs = this.loadJobsFromXml this.localStatusFile
		
		if unfinishedJobs.count > 0 then (
			this.jobs = unfinishedJobs
		)
	),
	
	fn hasUnfinishedJobs = (
		local result = false
		local unfinishedJobs = this.loadJobsFromXml this.localStatusFile
		
		if unfinishedJob != undefined and unfinishedJobs.count > 0 then (
			result = true
		)
		
		result
	),
	
	
	public
	 ------------------------------ MADE THESE PUBLIC SO I COULD ACCESS THEM FROM GUI -----------------------------
	--------------------------------------------------------------------------------------------------------------------------------------
	jobs			= #(),
	
	fn loadJobsFromXml filename = 
	(
		if ( doesFileExist filename ) == false then 
		(
			return undefined
		)
		
		local xmlJobs = #()
		
		stream = dotNetObject "System.IO.StreamReader" filename
		doc = dotNetObject "System.Xml.XmlDocument"
		doc.load stream
		
		root = doc.documentElement
		
		local jobNodes = ( root.SelectNodes ".//job" )
		
		for i = 0 to ( jobNodes.count - 1 ) do (
			local jobNode = jobNodes.item[ i ]
			
			local job = RsBatchJob()
			job.name = jobNode.attributes.ItemOf[ 0 ].value
			job.preJobMaxscript = ( jobNode.SelectNodes ".//preJobMaxscript" ).item[ 0 ].innerXml
			job.postJobMaxscript = ( jobNode.SelectNodes ".//postJobMaxscript" ).item[ 0 ].innerXml
			job.preTaskMaxscript = ( jobNode.SelectNodes ".//preTaskMaxscript" ).item[ 0 ].innerXml
			job.postTaskMaxscript = ( jobNode.SelectNodes ".//postTaskMaxscript" ).item[ 0 ].innerXml
			job.jobMaxscript = ( jobNode.SelectNodes ".//jobMaxscript" ).item[ 0 ].innerXml
			
			if job.preJobMaxscript == "" then job.preJobMaxscript = undefined
			if job.postJobMaxscript == "" then job.postJobMaxscript = undefined
			if job.preTaskMaxscript == "" then job.preTaskMaxscript = undefined
			if job.postTaskMaxscript == "" then job.postTaskMaxscript = undefined
			if job.jobMaxscript == "" then job.jobMaxscript = undefined
			
			local maxFileNodes = ( jobNode.SelectNodes ".//maxFiles/maxFile" )
			
			for maxFileIdx = 0 to ( maxFileNodes.count - 1 ) do (
				local maxFileNode = maxFileNodes.item[ maxFileIdx ]
				job.addMaxFile maxFileNode.innerXml
			)
			
			append xmlJobs job
		)
		
		doc = undefined
		stream.close()
		stream.dispose()
		stream = undefined
		
		gc()
		
		xmlJobs
	),
	--------------------------------------------------------------------------------------------------------------------------------------
	 -------------------------------------------------------------------------------------------------------------------------------------
	fn logMessage msg = (
		print msg
		this.ulog.LogMessage msg appendToFile:false
	),
	
	fn logWarning msg = (
		print msg
		this.ulog.LogWarning msg appendToFile:false
	),
	
	fn logError msg = (
		print msg
		this.ulog.LogError msg appendToFile:false
	),
	
	fn addJob job = (
		append this.jobs job
	),
	
	fn removeJob job = (
		
	),
	
	fn removeAllJobs = (
		this.jobs = #()
	),
	
	fn promoteJob job = (
		
	),
	
	fn stop = (
		
	),
	
	fn start jobsList:#() = (
		clearListener()
		
		if ( this.hasUnfinishedJobs() ) == true then (
			print "There are unfinished jobs!"
		)
		
		local jobs = this.jobs
		
		this.ulog.Init "Batch Framework" appendToFile:false forceLogFile:this.ulogFilename
		
		-- Process a specific list of jobs in the queue.
		if jobsList.count > 0 then (
			jobs = #()
			
			local jobNames = for job in this.jobs collect job.name
				
			for jobName in jobsList do (
				local jobIdx = findItem jobNames jobName
				
				if jobIdx != 0 then (
					append jobs this.jobs[ jobIdx ]
				)
			)
		)
		
		-- Process jobs.
		if jobs.count > 0 then (
			progressStart "Processing jobs..."
			
			local jobProgress = 0
			
			for job in jobs where job.status == false and gRsBatchJobCancel == false do (
				this.logMessage ( "Processing Job: " + job.name )
				this.currentJob = job
				
				if ( job.run() ) == false then (
					job.status = false
					
				) else (

					-- Job is finished, and doesn't need to be processed again.
					job.status = true
				)
				
				this.saveCurrentBatchStatus()
				
				jobProgress += 1
				progressUpdate ( 100 * jobProgress / jobs.count )
			)
			
			this.currentJob = undefined
			
			this.logMessage "Finished processing jobs."
			this.ulog.validate()
			
			progressEnd()
			
		) else (
			this.logWarning "No jobs to process!"
		)
	),
	
	fn saveJobsQueue filename = (
		this.saveJobsToXml filename
	),
	
	fn importJobsQueue filename = (
		if ( doesFileExist filename ) == true then (
			local xmlJobs = this.loadJobsFromXml filename
			
			for job in xmlJobs do (
				append this.jobs job
			)
		)
	)
)

gRsBatchFramework = RsBatchFramework()

--jobsFileName = "X:\\rdr3\\jobs_metadata.xml"
--gRsBatchFramework.importJobsQueue jobsFileName

--Commenting out the automatic start so that this can be called from a GUI
--gRsBatchFramework.Start()

/*
rollout RsBatchFrameworkRollout "Batch"
(
	listbox lbJobsQueue "Jobs Queue"
	
)

createDialog RsBatchFrameworkRollout width:400 height:400
*/