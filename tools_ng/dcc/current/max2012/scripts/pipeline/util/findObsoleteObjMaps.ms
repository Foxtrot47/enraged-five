--
-- Obsolete-instance finder
-- Displays list of metafiles containing obsolete objects (as defined by propSwap.txt)
--
-- Neal D Corbett
-- Rockstar Leeds
-- 27/04/2011
--

fn RsFindObsoleteObjMaps = 
(
	local timestart = timestamp()
	
	RSrefFuncs.setObsoleteRefs()
	local oldNames = RSrefData.swapFromNames
	
	local mapItems = RsProjectContentFindAll type:"mapzip"
	
	pushPrompt "Getting map filenames"
	local isContainer = #{}
	local mapFilenames = #()
	
	for item in mapItems do 
	(
		local mapMaxNode = item.inputs[1]
		
		if (mapMaxNode != undefined) and mapMaxNode.instances do 
		(
			local filename = item.path + "/" + item.name + ".xml"
			
			if doesFileExist filename do 
			(
				append mapFilenames filename
				
				if (matchPattern mapMaxNode.mapType pattern:"*container*") do 
				(
					isContainer[mapFilenames.count] = true
				)
			)
		)
	)
	popPrompt()
	
	mapItems = undefined	
	local filesizes = for filename in mapFilenames collect (getFileSize filename)
	
	local totalSize = 0
	for fileSize in filesizes do (totalSize += filesize)
	
	local mapCount = mapFilenames.count
	
	progressStart "Reading maps"

	local sizeSoFar = 0.0
	local memWas = 0
	
	local obosoletesFound = #()
		
	local sceneXml, searchName
	for n = 1 to mapCount while progressUpdate (100.0 * (sizeSoFar += filesizes[n]) / totalSize) do 
	(
		-- Garbage-collect every time the memory-size creeps up by 100mb:
		if ((sysinfo.getMAXMemoryInfo())[3] - memWas) > 100000000 do 
		(
			gc light:true
			(dotnetclass "system.gc").collect()
			memWas = (sysinfo.getMAXMemoryInfo())[3]
		)
		
		local filename = mapFilenames[n]
		
		local loadOptions = (dotnetobject "RSG.SceneXml.Scene").LoadOptions.Objects
		sceneXml = dotnetobject "RSG.SceneXml.Scene" filename loadOptions true

		local readElems = #()

		for elem in sceneXml.objects do 
		(
			if elem.isContainer() then 
			(
				join readElems elem.children
			)
			else 
			(
				append readElems elem
			)
		)
		
		local obsoleteObjNames = #()
		local objCount = 0
		
		for obj in readElems where obj.IsRefObject() or obj.IsXRef() do 
		(
			objCount += 1
			searchName = toLower obj.name
			if (findItem oldNames searchName) != 0 do 
			(
				appendIfUnique obsoleteObjNames searchName
			)
		)
		--format "% - %\n" (getfilenamefile filename) objCount
		
		if (obsoleteObjNames.count != 0) do 
		(
			append obosoletesFound (dataPair filename:filename objNames:obsoleteObjNames)
		)
	)
	sceneXml = undefined

	gc light:true
	(dotnetclass "system.gc").collect()
	progressEnd()
	
	local outputText = stringstream ""
	format "% maps found containing obsolete objects (as defined by %)\n" obosoletesFound.count RsRefData.SwapsCfgFile to:outputText
	format "Search took % seconds\n\n" ((timeStamp() - timeStart) / 1000.0) to:outputText
	
	for item in obosoletesFound do 
	(
		format "%\nContains:\n" item.filename to:outputText
		for objName in item.objNames do 
		(
			format "  %\n" objName to:outputText
		)
		format "\n" to:outputText
	)
	
	RScreateTextWindow text:(outputText as string) title:"Search Results: Obsolete objects"

	ok
)

RsFindObsoleteObjMaps()