--
-- File:: pipeline/util/colours.ms
-- Description:: Functions for MAX colors
--
-- Author:: Adam Munson
-- Date:: 20 December 2010
--

--
--fn:	RsColourInRange
fn RsColourInRange colour1 colour2 eps = (
	abs ((colour1.r as float)-(colour2.r as float)) < eps and
	abs ((colour1.g as float)-(colour2.g as float)) < eps  and
	abs ((colour1.b as float)-(colour2.b as float)) < eps 
)


--
--fn:	RsMatchColours
--desc:	Compares two colours
--
fn RsMatchColours colour1 colour2 = 
(
	(distance (colour1 as point3) (colour2 as point3)) < 0.0001
)

--
--fn:	RsSysDrawColorToColor
--desc:	Quick function to convert between the .net system drawing color type to color
--
fn RsSysDrawColorToColor sysDrawCol = (

	( color sysDrawCol.r sysDrawCol.g sysDrawCol.b )
)

--
--fn:	RsColorToSysDrawColor
--desc:	Quick function to convert between the color and system drawing color to change button
--	colours etc
--
fn RsColorToSysDrawColor col = (

	(dotNetClass "system.drawing.color").fromArgb col.r col.g col.b
)

--
--fn:	RsHexARGBToColor
--desc:	Function takes a Hex ARGB colour value in and then figures out the R,G,B values, returning a 'color'
--
fn RsHexARGBToColor hexColour = (

		colourInt = (bit.hexAsInt hexColour)		
		colRMask = (bit.hexAsInt "0x00FF0000")
		colGMask = (bit.hexAsInt "0x0000FF00")
		colBMask = (bit.hexAsInt "0x000000FF") 
		colR = (bit.and colRMask colourInt)
		colG = (bit.and colGMask colourInt)
		colB = (bit.and colBMask colourInt)
		colR = bit.shift colR -16
		colG = bit.shift colG -8
		
		(color colR colG colB)	
)