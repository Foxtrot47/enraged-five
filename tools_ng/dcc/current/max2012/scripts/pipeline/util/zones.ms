--
-- File: zones.ms
-- Desc: Gta Zone import/export functions.
--
-- Author: David Muir <david.muir@rockstarnorth.com>
-- Date: 20 January 2009
--
-- The file has common functionality between the Import and Export Zones.
-- Based off of the original <scripts>/GTA/ImportZones.ms and ExportZones.ms.
--

-- Global attribute classes.
RsGtaZoneAttributeClass = "Gta Zone"
RsPopZoneAttributeClass = "RsPopZone"
RsMapZoneAttributeClass = "RsMapZone"

idxZoneName = getAttrIndex RsGtaZoneAttributeClass "Zone name"
idxZoneType = getAttrIndex RsGtaZoneAttributeClass "Zone type"	

idxRsPopZoneName = getAttrIndex "RsPopZone" "Zone name"
idxRsPopZoneType = getAttrIndex "RsPopZone" "Zone type"	

idxRsMapZoneName = getAttrIndex "RsMapZone" "Zone name"
idxRsMapZoneArea = getAttrIndex "RsMapZone" "Zone area"

-- Copies across the box2 parameters from a Gta Zone object to a RsPopZone or RsMapZone object
fn RsCopyBox2ParamsToNewZoneObj box2Obj newZoneObj = (
	
	if ( getattrclass box2Obj == "Gta Zone" and (classof newZoneObj == RsPopZone or classof newZoneObj == RsMapZone ) ) then
	(		
		newZoneObj.Box2.Length 			= box2Obj.Length
		newZoneObj.Box2.Width 			= box2Obj.Width
		newZoneObj.Box2.Height 			= box2Obj.Height
		newZoneObj.Box2.LengthSegments 	= box2Obj.LengthSegments
		newZoneObj.Box2.WidthSegments 	= box2Obj.WidthSegments
		newZoneObj.Box2.HeightSegments 	= box2Obj.HeightSegments
		newZoneObj.Box2.GenerateUVs 	= box2Obj.GenerateUVs
		newZoneObj.Box2.Front 			= box2Obj.Front
		newZoneObj.Box2.Top 			= box2Obj.Top
		newZoneObj.Box2.Left 			= box2Obj.Left
		newZoneObj.Box2.Back 			= box2Obj.Back
		newZoneObj.Box2.Bottom 			= box2Obj.Bottom
		newZoneObj.Box2.Right 			= box2Obj.Right
		newZoneObj.transform 			= box2Obj.transform
		newZoneObj.name 				= box2Obj.name
		
		oldObjAttrName = getattr box2Obj idxZoneName
		oldObjAttrType = getattr box2Obj idxZoneType
		
		if ( classof newZoneObj == RsPopZone ) then
		(
			setattr newZoneObj idxRsPopZoneName oldObjAttrName
			setattr newZoneObj idxRsPopZoneType oldObjAttrType
		)
		else
		(
			setattr newZoneObj idxRsMapZoneName oldObjAttrName	
		)
	)	
	else
	(
		format "Couldn't copy parameters from %:% to %:% as they aren't both the required type (box2Geometry and RsPopZone or \
				RsMapZone)\n" box2Obj (classof box2Obj) newZoneObj (classof newZoneObj)
	)	
)

fn RsMapConvertGtaZoneToNewZone newClass = (
	
	objectsToDelete = #()
	
	for obj in $objects do
	(
		if ( getAttrClass obj == "Gta Zone" ) do
		(
			newObj = newClass()
			RsCopyBox2ParamsToNewZoneObj obj newObj
			append objectsToDelete obj
		)
	)
	
	-- Delete the old Gta Zone objects from scene
	for i = objectsToDelete.count to 1 by -1 do ( delete objectsToDelete[i] )
)

--
-- name: RsMapImportPopZoneFile
-- desc: Import RsPopZone helpers from a Population Zone IPL file.
--
fn RsMapImportPopZoneFile filename = 
(
	undo "import PopZones" on 
	(
		local file
		local line
		local elements
		local minV, maxV
		local zone
		
		local bReadingZones = false

		if ( undefined == filename ) then
			return false
		
		local defZoneType = getAttrDefault RsPopZoneAttributeClass idxZoneType
		if ( undefined == idxZoneName ) then 
		(
			messagebox "Zone attributes (Zone name) are not setup" title:"Zone file import"
			return false
		)
		if ( undefined == idxZoneType ) then
		(
			messageBox "Zone attributes (Zone type) are not setup" title:"Zone file import"
			return false
		)
		
		file = openfile filename mode:"r"
		
		while (not eof file) do (
			line = readline file
			if line == "zone" do ( 
				bReadingZones = true
				continue
			)	
			if line == "end" do bReadingZones = false

			if bReadingZones == true do (
				elements = filterString line ", "
				zone = RsPopZone()
				zone.name = elements[1]
				minV = (Point3 (elements[2] as float) (elements[3] as float) (elements[4] as float))
				maxV = (Point3 (elements[5] as float) (elements[6] as float) (elements[7] as float))
				
				zone.pos.x 			= (maxV.x + minV.x)/2
				zone.pos.y 			= (maxV.y + minV.y)/2
				zone.pos.z 			= minV.z
				zone.box2.width 	= maxV.x - minV.x
				zone.box2.length 	= maxV.y - minV.y
				zone.box2.height 	= maxV.z - minV.z
				
				-- 9th parameter: zone area
				if (undefined != elements[8] ) then (
					setAttr zone idxRsPopZoneName (elements[8] as string)
				) else (
					setAttr zone idxRsPopZoneName (elements[1] as string)
				)
				
				-- 10th parameter: zone type (see Bug #127078)
				if ( undefined != elements[9] ) then (
					setAttr zone idxRsPopZoneType (elements[9] as integer)
				)
				else
				(
					setAttr zone idxRsPopZoneType 0
				)
			)
		)
		
		close file
	)
	
	completeRedraw()
	
	return true
)

--
-- name: RsMapExportPopZoneFile
-- desc: Export Gta Zone helpers to a Population Zone IPL file.
--
fn RsMapExportPopZoneFile filename = (
	local file
	
	if ( undefined == filename ) then
		return false	
	
	if ( undefined == idxRsPopZoneName or undefined == idxRsPopZoneType) then 
	(
		messagebox "RsPopZone attributes not found" title:"RsPopZone file export"
		return false
	)
		
	file = createfile filename
	if ( undefined == file ) then 
	(
		local ss = stringStream ""
		format "Cannot create file: %" filename to:ss
		messagebox ( ss as string ) title:"RsPopZone file export"
		return false
	)	
	format "zone\n" to:file
	
	for obj in rootNode.children do (
	
		if ( RsPopZoneAttributeClass == GetAttrClass obj ) then 
		(
			local zonename = substring obj.name 1 8

			local minvalue = [obj.pos.x - obj.box2.width/2, obj.pos.y - obj.box2.length/2, obj.pos.z]
			local maxvalue = [obj.pos.x + obj.box2.width/2, obj.pos.y + obj.box2.length/2, obj.pos.z + obj.box2.height]

			local zonename2 = getattr obj idxRsPopZoneName
			local zoneType = getAttr obj idxRsPopZoneType
			
			format "%, " zonename to:file
			format "%, %, %, " minvalue.x minvalue.y minvalue.z to:file
			format "%, %, %, " maxvalue.x maxvalue.y maxvalue.z to:file
			format "%, %\n" zonename2 zoneType to:file
		)
	)
	format "end\n" to:file
	
	close file
)

--
-- name: RsMapImportMapZoneFile
-- desc: Import Gta Zone helpers from a named file.
--
fn RsMapImportMapZoneFile filename = 
(
	undo "import MapZone" on 
	(
		local file
		local line
		local str
		local elements
		local minV, maxV
		local zone
		
		local bReadingZones = false

		if ( undefined == filename ) then
			return false
		
		if ( undefined == idxRsMapZoneName or undefined == idxRsMapZoneArea ) then 
		(
			messagebox "RsMapZone attributes are not setup" title:"RsMapZone file export"
			return false
		)
		
		-- Get a list of the current mapzone area hashes to use later when importing, to see what attribute to set
		hashList = for zoneAreaHash in (filterString (getcontroldata "RsMapZone" "Zone area") "," ) \
						collect (formattedPrint (atStringHash zoneAreaHash) format:"u")
		
		file = openfile filename mode:"r"
		
		while (not eof file) do (
			line = readline file
			if line == "mzon" do ( 
				bReadingZones = true
				continue
			)	
			if line == "end" do bReadingZones = false

			if bReadingZones == true do (
				elements = filterString line ", "
				zone = RsMapZone()
				zone.name = elements[7]
				minV = (Point3 (elements[1] as float) (elements[2] as float) (elements[3] as float))
				maxV = (Point3 (elements[4] as float) (elements[5] as float) (elements[6] as float))
				
				zone.pos.x 			= (maxV.x + minV.x)/2
				zone.pos.y 			= (maxV.y + minV.y)/2
				zone.pos.z 			= minV.z
				zone.box2.width 	= maxV.x - minV.x
				zone.box2.length 	= maxV.y - minV.y
				zone.box2.height 	= maxV.z - minV.z
				
				if elements[7] != undefined then (
					setattr zone idxRsMapZoneName (elements[7] as string)
				) else (
					setattr zone idxRsMapZoneName "unknown"
				)
				
				if elements[8] != undefined do (
				
					format "checking for %\n" elements[8]
					print "hashlist:"
					print hashList
					
					local zonearea = (findItem hashList elements[8]) - 1
					format "zonearea: %\n" zonearea
					-- Find the zone area int attribute value to set from the hash in the file
					if ( zonearea < 0 ) do
					(
						format "No zone area found for RsMapArea:%\n" zone.name
						zonearea = 0
					)
					
					setattr zone idxRsMapZoneArea zonearea
				)
			)
		)
		
		close file
	)
	
	completeRedraw()
	
	return true
)

--
-- name: RsMapExportMapZoneFile
-- desc: Export Gta Zone helpers to a Map Zone IPL file.
--
fn RsMapExportMapZoneFile filename = (
	local file
	
	if ( undefined == filename ) then
		return false
	
	if ( undefined == idxRsMapZoneName or undefined == idxRsMapZoneArea ) then 
	(
		messagebox "RsMapZone attributes not found" title:"RsMapZone export"
		return false
	)
	
	file = createfile filename
	if ( undefined == file ) then 
	(
		locla ss = stringStream ""
		format "Cannot create file: %" filename to:ss
		messagebox ( ss as string ) title:"RsMapZone file export"
		return false
	)	
	format "mzon\n" to:file
	
	for obj in rootNode.children do (
	
		if ( RsMapZoneAttributeClass == GetAttrClass obj ) then 
		(
			local minvalue = [obj.pos.x - obj.box2.width/2, obj.pos.y - obj.box2.length/2, obj.pos.z]
			local maxvalue = [obj.pos.x + obj.box2.width/2, obj.pos.y + obj.box2.length/2, obj.pos.z + obj.box2.height]

			local zonename = getattr obj idxRsMapZoneName
			local idxzonearea = (getattr obj idxRsMapZoneArea) + 1
			
			-- Find the string for the zone area, and hash it
			zoneAreaControlData = getcontroldata "RsMapZone" "Zone area" 
			zoneAreaTypes = filterstring zoneAreaControlData ","
			zonearea = atStringHash zoneAreaTypes[idxzonearea]
			
			format "%, %, %, " minvalue.x minvalue.y minvalue.z to:file
			format "%, %, %, " maxvalue.x maxvalue.y maxvalue.z to:file
			format "%, %\n" zonename (formattedPrint zonearea format:"u") to:file
		)
	)
	format "end\n" to:file
	
	close file
)

-- zones.ms
