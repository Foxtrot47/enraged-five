--
-- SaveDecisionMgr
-- @author Gunnar Droege
-- @descr allows registration, saving and triggering of artists decisions for export
-- 11/10/2012

global gRsSaveDecMgr

struct sRsSaveDecisionMgr
(
private
	serialisePath = (getdir #maxData) + "RsDecisionDump.osl",
	mDecisions = dotnetObject "RSG.MaxUtils.MaxDictionary",			-- hash array of decision identifiers and functions 
	mEnabled = false,
	mRecording = false,

public
	fn Init = 
	(
	),
	
	fn registerDecision id returnVal optionVals:undefined optionLabels:undefined comment:"no description" = 
	(
		gRsUlog.LogMEssage ("Register decision on \""+id+"\" with return value "+returnVal as string)
		local theDecision = dotnetObject "RSG.MaxUtils.RsMaxDecision" 
		theDecision.val = returnVal 
		theDecision.optionVals = optionVals 
		theDecision.optionLabels = optionLabels  
		theDecision.comment = comment
		
		if mDecisions.containsKey id then
			mDecisions.put id theDecision
		else
			mDecisions.add id theDecision
	),
	
	fn trigger id =
	(
		if not mEnabled then
			return #notActive
		if not mDecisions.containsKey id then
			return #notRecorded
		
		local theDecision = mDecisions.get id
		local valIndex = theDecision.val
		local retValue = valIndex
		if undefined!=theDecision.optionVals and theDecision.optionVals.count==0 then
			retValue = optionVals[valIndex+1]
		
		gRsUlog.LogMEssage ("Triggering formerly recorded decision on \""+id+"\" with return value "+retValue as string) quiet:false
		
		return retValue
	),
	
	fn IsEnabled = mEnabled,
	fn SetEnabled onOff = (mEnabled = onOff),
	fn IsRecorded id = 
	(
		return (mDecisions.containsKey id)
	),
	fn IsRecording = mRecording,
	fn SetRecording onOff = (mRecording = onOff),
	fn IsEnabledAndRecorded id = 
	(
		return (mEnabled and mDecisions.containsKey id)
	),
	
	fn GetDecisions = return mDecisions,
		
	fn Serialise = 
	(
		local stream =  (dotnetclass "System.IO.File").Open serialisePath ((dotnetclass "System.IO.FileMode").OpenOrCreate)
		bf = dotnetobject "System.Runtime.Serialization.Formatters.Binary.BinaryFormatter"
		bf.AssemblyFormat = (dotnetclass "System.Runtime.Serialization.Formatters.FormatterAssemblyStyle").Simple;

		bf.Serialize stream mDecisions
		stream.Close()
	),
	fn Deserialise = 
	(
		--Open the file written above and read values from it.
		if RsFileExists serialisePath then
		(
			local stream = (dotnetclass "System.IO.File").Open serialisePath ((dotnetclass "System.IO.FileMode").Open)
			bf = dotnetobject "System.Runtime.Serialization.Formatters.Binary.BinaryFormatter"
			bf.AssemblyFormat = (dotnetclass "System.Runtime.Serialization.Formatters.FormatterAssemblyStyle").Simple;
			bf.Binder = dotnetObject "RSG.MaxUtils.MaxAssemblyBinder"
			
			mDecisions = bf.Deserialize stream
			stream.Close()
		)
	)
)

if undefined==gRsSaveDecMgr then
	gRsSaveDecMgr= sRsSaveDecisionMgr()