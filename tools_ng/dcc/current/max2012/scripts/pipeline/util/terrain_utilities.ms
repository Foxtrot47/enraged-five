--
-- Terrain Utilities
--
--
--

filein "pipeline\export\maps\globals.ms"
filein "pipeline\export\maps\scenexml.ms"
filein "pipeline\util\RsModelFuncs.ms"
fileIn (::RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Collision/ProcPainter_Funcs.ms")
filein "pipeline/util/terrain_utilities_globals.ms"
filein "pipeline/util/RsUniversalLogViewer.ms"
filein "pipeline/util/RsRefs_Funcs.ms"

idxGenerateCollision = getattrindex "Gta Object" "Generate Collision From Mesh"

struct ProceduralObjDictionaryEntry
(
	proceduralObject = undefined,
	faces = #(),
	vertices = #(),
	scaleValue = 8, -- Default value
	densityValue = 4,
	maxScale = undefined
)

fn LogOutput message = 
(
	RsTerrainGlobals.UniversalLog.LogMessage message
	print message
)

fn LogDebugOutput debugOutput = 
(
	if ( RsTerrainGlobals.EnableDebugOutput == true ) then
		RsTerrainGlobals.UniversalLog.LogMessage debugOutput
)

-- Takes a visual mesh and creates a copy to convert it to a collision mesh.
fn GenerateBounds objMesh = 
(
	newMesh = copy objMesh
	newMesh.name = objMesh.name + "_collision"
	
	--Convert to a Collision Mesh
	CollapseStack newMesh
	mesh2col  newMesh
	append objMesh.children newMesh
	
	print ("Created: " + newMesh.name)
	true
)

fn ExpandBounds minBB maxBB = 
(
	if ( minBB.x < RsTerrainGlobals.MinBounds.x ) then 
	(
		RsTerrainGlobals.MinBounds.x  = minBB.x
	)
	
	if ( minBB.y < RsTerrainGlobals.MinBounds.y ) then 
	(
		RsTerrainGlobals.MinBounds.y  = minBB.y
	)
	
	if ( minBB.z < RsTerrainGlobals.MinBounds.z ) then 
	(
		RsTerrainGlobals.MinBounds.z = minBB.z
	)
	
	if ( maxBB.x > RsTerrainGlobals.MaxBounds.x ) then 
	(
		RsTerrainGlobals.MaxBounds.x = maxBB.x
	)
	
	if ( maxBB.y > RsTerrainGlobals.MaxBounds.y ) then 
	(
		RsTerrainGlobals.MaxBounds.y = maxBB.y
	)
	
	if ( maxBB.z > RsTerrainGlobals.MaxBounds.z ) then 
	(
		RsTerrainGlobals.MaxBounds.z = maxBB.z
	)
)

fn GetMaxObjectBounds obj = 
(
	local boundingbox = nodeGetBoundingBox obj (matrix3 1)
	
	local minBB = boundingbox[1]
	local maxBB = boundingbox[2]
	
	ExpandBounds minBB maxBB
	
	for child in obj.children do
	(
		GetMaxObjectBounds child
	)
)

fn GetMapBounds = 
(	
	RsTerrainGlobals.MinBounds = undefined
	RsTerrainGlobals.MaxBounds = undefined 
	for child in rootnode.children do
	(
		local boundingbox = nodeGetBoundingBox child (matrix3 1)
		
		local minBB = boundingbox[1]
		local maxBB = boundingbox[2]
		
		if ( RsTerrainGlobals.MinBounds == undefined and RsTerrainGlobals.MaxBounds == undefined ) then
		(
			RsTerrainGlobals.MinBounds = minBB
			RsTerrainGlobals.MaxBounds = maxBB
		)

		GetMaxObjectBounds child
	)
)


tool DropperTester
(		
	on mousePoint clickNumber do 
	(
		--Cast that point downward 
		print ("World Point: " + worldPoint as string)
		local pixelInfo = RsTerrainGlobals.VegetationData.GetPixelInfo worldPoint.x worldPoint.y
		print ("Pixel Info: " + pixelInfo as string)
	)		
)
stopTool DropperTester  --Ensure that the tester doesn't start automatically.

--Separated into a function so the MaxScript-savvy can use this.
fn QueryPixelInfo x y = 
(
	GetMapBounds()
	 RsTerrainGlobals.VegetationData.LoadTexturesFromBound RsTerrainGlobals.MinBounds.x RsTerrainGlobals.MinBounds.y RsTerrainGlobals.MaxBounds.x RsTerrainGlobals.MaxBounds.y
	
	-- NOTE:  Since this is meant to be called directly through the Listener, the string output will go to the Listener
	-- and doesn't need to be printed out again.
	local pixelInfo =  RsTerrainGlobals.VegetationData.GetPixelInfo x y
	--print ("Pixel Info: " + pixelInfo as string)
)

fn QueryLoadedTextures =
(
	GetMapBounds()
	GetCollisionBounds useSelection
	
	 RsTerrainGlobals.VegetationData.LoadTexturesFromBound RsTerrainGlobals.MinBounds.x RsTerrainGlobals.MinBounds.y RsTerrainGlobals.MaxBounds.x RsTerrainGlobals.MaxBounds.y
	
	print "Loaded Textures: "
	local textureOutput =  RsTerrainGlobals.VegetationData.GetLoadedTexturesFromBound RsTerrainGlobals.MinBounds.x RsTerrainGlobals.MinBounds.y RsTerrainGlobals.MaxBounds.x RsTerrainGlobals.MaxBounds.y 
	print (textureOutput as string)
)

fn StartDropperTester vegetationData = 
(
	GetMapBounds()
	vegetationData.LoadTexturesFromBound RsTerrainGlobals.MinBounds.x RsTerrainGlobals.MinBounds.y RsTerrainGlobals.MaxBounds.x RsTerrainGlobals.MaxBounds.y
	
	 RsTerrainGlobals.VegetationData = vegetationData
	
	startTool DropperTester
)

fn ProcessNodeForBounds currentNode = 
(
	for child in currentNode.children do
	(
		if ( Col_Mesh == classof child ) then 
		(
			append RsTerrainGlobals.CollisionBoundsArray child
		)
		else if ( (getattrclass child) == "Gta Object" and (GetAttr child idxGenerateCollision) == true ) then
		(
			append RsTerrainGlobals.TerrainMeshBoundsArray child
		)
		
		ProcessNodeForBounds child
	)
)

fn GetCollisionBounds useSelection = 
(
	RsTerrainGlobals.CollisionBoundsArray = #()
	RsTerrainGlobals.TerrainMeshBoundsArray = #()
	
	if useSelection == true then
	(
		for selectionIndex = 1 to selection.count do
		(
			local selectedNode = selection[selectionIndex]
			if ( Col_Mesh == classof selectedNode ) then 
			(
				append RsTerrainGlobals.CollisionBoundsArray selectedNode
			)
			else if ( (getattrclass selectedNode) == "Gta Object" and (GetAttr selectedNode idxGenerateCollision) == true ) then
			(
				append RsTerrainGlobals.TerrainMeshBoundsArray selectedNode
			)
			
			ProcessNodeForBounds selectedNode
		)
	)
	else
	(
		for child in rootnode.children do
		(
			if ( Col_Mesh == classof child ) then 
			(
				append RsTerrainGlobals.CollisionBoundsArray child
			)
			else if ( (getattrclass selectedNode) == "Gta Object" and (GetAttr selectedNode idxGenerateCollision) == true ) then
			(
				append RsTerrainGlobals.TerrainMeshBoundsArray child
			)
			
			ProcessNodeForBounds child
		)
	)
	
	RsTerrainGlobals.CollisionBoundsArray
)

fn ConvertCollisionBoundsToMesh = 
(
	for boundIndex = 1 to RsTerrainGlobals.CollisionBoundsArray.count do
	(
		col2mesh RsTerrainGlobals.CollisionBoundsArray[boundIndex]
	)
)

fn RevertCollisionMeshes = 
(
	for boundIndex = 1 to RsTerrainGlobals.CollisionBoundsArray.count do
	(
		mesh2col RsTerrainGlobals.CollisionBoundsArray[boundIndex]
	)
)

fn FindZIntersection x y = 
(	
	local rayCast = ray [x, y, 10000] [0, 0, -1]
	local intersectionZ = undefined
	
	for boundIndex = 1 to RsTerrainGlobals.CollisionBoundsArray.count do 
	(
		local intersection =	intersectRay RsTerrainGlobals.CollisionBoundsArray[boundIndex] rayCast
		
		if ( intersection != undefined ) then
		(
			--Optimization:  To reduce the number of intersection tests we do, push the bound
			-- currently intersecting with the ray to the front.  
			if ( boundIndex > 1 ) then 
			(
				local tempCollisionBound = RsTerrainGlobals.CollisionBoundsArray[1]
				RsTerrainGlobals.CollisionBoundsArray[1] = RsTerrainGlobals.CollisionBoundsArray[boundIndex]
				RsTerrainGlobals.CollisionBoundsArray[boundIndex] = tempCollisionBound
			)
			
			intersectionZ = intersection.pos.z
			exit 
		)
	)
	
	for boundIndex = 1 to RsTerrainGlobals.TerrainMeshBoundsArray.count do 
	(
		local intersection =	intersectRay RsTerrainGlobals.TerrainMeshBoundsArray[boundIndex] rayCast
		
		if ( intersection != undefined ) then
		(
			--Optimization:  To reduce the number of intersection tests we do, push the bound
			-- currently intersecting with the ray to the front.  
			if ( boundIndex > 1 ) then 
			(
				local tempCollisionBound = RsTerrainGlobals.TerrainMeshBoundsArray[1]
				RsTerrainGlobals.TerrainMeshBoundsArray[1] = RsTerrainGlobals.TerrainMeshBoundsArray[boundIndex]
				RsTerrainGlobals.TerrainMeshBoundsArray[boundIndex] = tempCollisionBound
			)
			
			intersectionZ = intersection.pos.z
			exit 
		)
	)
	
	if ( intersectionZ != undefined ) then 
	(
		#(true, intersectionZ)
	)
	else
	(
		#(false, 0.0)
	)
)

fn DropVegetation vegetationData dropMode useSelection fileName = 
(	
	local containerNode = undefined
	if ( dropMode == RsTerrainGlobals.DropModeCollisionPainter ) then 
	(
		RsProcDataLoad()
	)
	else
	(
		if (RsRefFuncs.databaseActive toolName:"Terrain Utilities" debugFuncSrc:"DropVegetation") == false then
			return false

		local containerNode = getNodeByName fileName
		if ( containerNode != undefined ) then 
		(
			RsTerrainGlobals.UniversalLog.LogMessage ("Deleting old container " + fileName + ".")
			delete containerNode
		)
		
		RsTerrainGlobals.UniversalLog.LogMessage ("Creating Vegetation Container " + fileName + ".")
		
		newContainer = getNodeByName fileName 
		newContainer = Container()
		newContainer.name = fileName
		if ( RsTerrainGlobals.ContainerPosition != undefined ) then
		(
			newContainer.pos = RsTerrainGlobals.ContainerPosition
		)
		else
		(
			newContainer.pos.x = ((RsTerrainGlobals.MaxBounds.x - RsTerrainGlobals.MinBounds.x) / 2) + RsTerrainGlobals.MinBounds.x
			newContainer.pos.y = ((RsTerrainGlobals.MaxBounds.y - RsTerrainGlobals.MinBounds.y) / 2) + RsTerrainGlobals.MinBounds.y
			newContainer.pos.z = ((RsTerrainGlobals.MaxBounds.z - RsTerrainGlobals.MinBounds.z) / 2) + RsTerrainGlobals.MinBounds.z
		)
	)
	
	GetMapBounds()
	GetCollisionBounds useSelection
	ConvertCollisionBoundsToMesh()
	
	result = vegetationData.LoadTexturesFromBound RsTerrainGlobals.MinBounds.x RsTerrainGlobals.MinBounds.y RsTerrainGlobals.MaxBounds.x RsTerrainGlobals.MaxBounds.y
	if ( result == false ) then
	(
		local message = vegetationData.GetLastError() as string
		RsTerrainGlobals.UniversalLog.LogError message		
		return false
	)
	
	local loadedTextures = vegetationData.GetLoadedTexturesFromBound RsTerrainGlobals.MinBounds.x RsTerrainGlobals.MinBounds.y RsTerrainGlobals.MaxBounds.x RsTerrainGlobals.MaxBounds.y 

	local maxAngle = vegetationData.GetMaxAngle()
	if maxAngle > 0.0f then
	(
		maxAngle = cos maxAngle  
	)
	
	local upVector = point3 0 0 1
	local timer = ScriptTimer()
	timer.start()
	
	boundsArray = #()
	for bound in RsTerrainGlobals.CollisionBoundsArray do append boundsArray bound 	
	for bound in RsTerrainGlobals.TerrainMeshBoundsArray do append boundsArray bound
	
	local prevRSrefAllowUpdate = RSrefAllowUpdate
	RSrefAllowUpdate = false
	
	local stop = false
	local enableLimit = false
	local objectLimit = 100
	LogDebugOutput ("Bounds Array: " + boundsArray.count as string) 
	for boundIndex = 1 to boundsArray.count do 
	(
		if stop == true then
			exit

		local objectDictionary = #()
		local currentBound = boundsArray[boundIndex]
		local faces = undefined 
		
		LogDebugOutput ("Current Bound: " + currentBound.name)
		
		if ( useSelection == true ) then
		(
			faces = currentBound.selectedFaces
		)
		else
			faces = currentBound.faces
		
		if ( dropMode == RsTerrainGlobals.DropModeCollisionPainter ) then
		(			
			--Reset the channel entirely.
			gRsCollPaintTool.setCurrentObj currentBound
			gRsCollPaintTool.storeObjState currentBound
			setSelectionLevel currentBound #face		
			selectedFaces = for i in currentBound.faces collect i.index
			setFaceSelection currentBound selectedFaces
			selectedFaces = getFaceSelection currentBound
							
			gRsCollPaintTool.applyMat (RsCollPaintProcMatData()) selectedFaces
		
			-- Delete entry from main material list:
			local matDefNum = findItem gRsCollPaintTool.matDefList matDef
			if (matDefNum != 0) do 
			(
				deleteItem gRsCollPaintTool.matDefList matDefNum
			)
		)
		
		objOp = case of 
		(
			(isKindOf currentBound Editable_Poly):polyop 
			(isKindOf currentBound Editable_Mesh):meshop
			default:(return undefined)
		)
		
		objFaceNormalOp = case of 
		(
			(isKindOf currentBound Editable_Poly):polyop.getFaceNormal
			(isKindOf currentBound Editable_Mesh):getFaceNormal
			default:(return undefined)
		)
	
		if (objOp.getMapSupport  currentBound RsTerrainGlobals.ProcGrassMapChannel) == true then
		(
			print "Erasing rexMtlBound map channels..."
			objOp.setMapSupport currentBound RsTerrainGlobals.ProcGrassMapChannel false
		)
		
		local vertices = currentBound.vertices
		LogDebugOutput("Face Count: " + faces.count as string)
		for faceIndex = 1 to faces.count do
		(
			local face = faces[faceIndex]		
			
			local faceNormal = objFaceNormalOp currentBound face.index
			local dotProduct = dot faceNormal upVector
			if maxAngle > 0.0f and dotProduct < maxAngle then 
			(
				continue
			)
			
			faceverts = (objOp.getVertsUsingFace currentBound face) as array
			verts = #( currentBound.verts[faceverts[1]], currentBound.verts[faceverts[2]], currentBound.verts[faceverts[3]] )
			
			if ( enableLimit and objectLimit <= 0 ) then exit
		
			if ( dropMode == RsTerrainGlobals.DropModeCollisionPainter or dropMode == RsTerrainGlobals.DropModeRexBoundMtl ) then
			(
				vegetationData.DropProceduralsOnFace verts[1].pos.x verts[1].pos.y verts[1].pos.z verts[2].pos.x verts[2].pos.y verts[2].pos.z verts[3].pos.x verts[3].pos.y verts[3].pos.z
				
				if vegetationData.LastInstance == undefined then continue
					
				objectName = vegetationData.LastInstance.Definition.Name
				
				-- Paint this particular face with a procedural object.
				--print ("Object Dropped: " + objectName as string)
				local procNum = findItem (RexGetProceduralList()) objectName
				if ( procNum > 0 ) then 
				(	
					--The Collision Painter assumes that there is a 1-16 range between the min and max scale, unfortunately.
					--In the interest of getting something working, wedging the randomized scaling value to be in this range.
					local newScaleValue = ((vegetationData.LastInstance.Scale * 15) + 1) as integer
					
					--The Collision Painter uses 2 bits to define density. (1-based because it's MaxScript)
					local newDensityValue = ((vegetationData.LastInstance.Density * 3) + 1) as integer
					
					foundDictionary = undefined
					for entry in objectDictionary do
					(
						if ( entry.proceduralObject == objectName and entry.scaleValue == newScaleValue and entry.densityValue == newDensityValue ) then
						(
							foundDictionary = entry
							exit
						)
					)
						
					if ( foundDictionary == undefined ) then
					(
						LogDebugOutput ("Adding new dictionary entry for " + objectName as string + ".")
						foundDictionary = ProceduralObjDictionaryEntry()
						append objectDictionary foundDictionary
						
						objectDictionary[objectDictionary.count].faces = #()
						objectDictionary[objectDictionary.count].vertices = #()
						objectDictionary[objectDictionary.count].proceduralObject = objectName
						objectDictionary[objectDictionary.count].scaleValue = newScaleValue
						objectDictionary[objectDictionary.count].densityValue = newDensityValue
					)
					
					append foundDictionary.faces faceIndex
					appendIfUnique foundDictionary.vertices faceverts[1]
					appendIfUnique foundDictionary.vertices faceverts[2]
					appendIfUnique foundDictionary.vertices faceverts[3]
				)
				else
				(
					RsTerrainGlobals.UniversalLog.LogError ("Unable to find procedural object material: " + objectName as string + ".  Aborting process...")
					stop = true
					exit
				)
			)
			else
			(
				objectsDropped = vegetationData.DropReferencesOnFace verts[1].pos.x verts[1].pos.y verts[1].pos.z verts[2].pos.x verts[2].pos.y verts[2].pos.z verts[3].pos.x verts[3].pos.y verts[3].pos.z
				if objectsDropped == undefined then continue
				
				objectArray = filterString objectsDropped ","
				objectCount = objectArray.count / 4  -- 4 is the number of tokens to describe each object.
				
				for objectIndex = 1 to objectCount do 
				(
					offset =  (4 * (objectIndex-1))
					definitionName = objectArray[offset + 1]
					x = objectArray[offset + 2] as float
					y = objectArray[offset + 3] as float
					z = objectArray[offset + 4] as float
											
					local newRef = RsrefObject objectname:definitionName
					newRef.pos = point3 x y z
					newContainer.AddNodesToContent newRef
				)
			)
			
			objectLimit = objectLimit - 1
		)
		
		if ( dropMode == RsTerrainGlobals.DropModeCollisionPainter ) then
		(
			for entry in objectDictionary do
			(
				LogDebugOutput ("Painting " + entry.proceduralObject as string + " onto " + entry.faces.count as string + " faces." )
				setFaceSelection currentBound entry.faces
				mat = gRsCollPaintTool.getDefaultProc entry.proceduralObject
				
				mat.scaleXYZidx = entry.scaleValue 
				mat.densityIdx = entry.densityValue
				
				selectedFaces = getFaceSelection currentBound
				gRsCollPaintTool.applyMat mat selectedFaces
			)

		)
		else if ( dropMode == RsTerrainGlobals.DropModeRexBoundMtl ) then
		(
			entryIndex = 1
			for entry in objectDictionary do
			(
				LogDebugOutput("Processing bound material for " + entry.proceduralObject as string)
				
				targetSubMaterialName = ("Procedural_" + entry.proceduralObject as string) 

				dropperMultiMaterial = undefined
				for sceneMaterial in sceneMaterials do
				(
					if ( sceneMaterial.name == RsTerrainGlobals.ProcMaterialName ) then
					(
						dropperMultiMaterial = sceneMaterial 
						exit
					)					
				)
				
				if dropperMultiMaterial == undefined then
				(
					dropperMultiMaterial = MultiMaterial()
					dropperMultiMaterial.name = RsTerrainGlobals.ProcMaterialName
					dropperMultiMaterial.numsubs = objectDictionary.count
					append sceneMaterials dropperMultiMaterial
				)
				
				for subMaterial in dropperMultiMaterial.materialList do
				(
					if targetSubMaterialName == subMaterial.name then
					(
						rexBoundMat = subMaterial 
						exit
					)
				)
				
				if ( rexBoundMat == undefined ) then
				(
					rexBoundMat = RexBoundMtl()
					rexBoundMat.name = targetSubMaterialName
					rexSetProceduralName rexBoundMat entry.proceduralObject
					dropperMultiMaterial[entryIndex] = rexBoundMat
				)

				objOp = case of 
				(
					(isKindOf currentBound Editable_Poly):polyOp
					(isKindOf currentBound Editable_Mesh):meshOp
					default:(return undefined)
				)
				
				targetMaterialHash = getHashValue targetSubMaterialName 1
				targetMaterialHashPoint = RsGetHashAsPoint targetMaterialHash

				--Propagate these materials down to procedurally generated meshes				
				if ( (objOp.getNumMaps currentBound) < (RsTerrainGlobals.ProcGrassMapChannel+1) ) then
					objOp.setNumMaps currentBound (RsTerrainGlobals.ProcGrassMapChannel+1) keep:true
				
				objOp.setMapSupport currentBound RsTerrainGlobals.ProcGrassMapChannel true
				for vertex in entry.vertices do
				(
					objOp.setMapVert currentBound RsTerrainGlobals.ProcGrassMapChannel vertex targetMaterialHashPoint
				)
				
				entryIndex = entryIndex + 1
			)
		)
		else if ( dropMode == RsTerrainGlobals.DropModeRsRefs ) then
		(			
			select newContainer
		)
	)
	
	--ScanUnregisteredPixels()
	
	
	RSrefAllowUpdate = prevRSrefAllowUpdate
				
	timer.end()
	LogOutput "Vegetation dropping has completed." 
	LogOutput ("Total Time: " + timer.interval() as string + " ms")
	
	RevertCollisionMeshes()
)

-- Scans through a list of all pixels that haven't been registered to be dropped.
-- This is meant to be a failsafe for dropping any vegetation that may have been overlooked.
/*fn ScanUnregisteredPixels =
(
	unregisteredPixels = vegetationData.GetUnregisteredPixels RsTerrainGlobals.MinBounds.x RsTerrainGlobals.MinBounds.y RsTerrainGlobals.MaxBounds.x RsTerrainGlobals.MaxBounds.y
	
	print ("Number of Unregistered Pixels: " + unregisteredPixels.Count as string)
	for pixelIndex = 1 to unregisteredPixels.Count do
	(
		local returnIntersection = FindZIntersection unregisteredPixels[pixelIndex].X unregisteredPixels[pixelIndex].Y
		
		if ( returnIntersection[1] == true ) then
		(
			pixelColor = vegetationData.GetPixelFromVector unregisteredPixels[pixelIndex].X unregisteredPixels[pixelIndex].Y
			definitionName = vegetationData.GetObjectFromPixel(pixelColor)

			if ( definitionName != undefined ) then
			(
				if file != undefined then 
				(						
					format "%, %, %, %, %, %, %, %, %, %, %, %, %, %\n" definitionName flags unregisteredPixels[pixelIndex].X unregisteredPixels[pixelIndex].Y returnIntersection[2] rotationX rotationY rotationZ lodParentIndex extraFlags lodDistance lodLevel children aoMultiplier to:file
				)
				
				if dropObjects == true then
				(
					local newRef = RsrefObject objectname:definitionName
					newRef.pos = point3 unregisteredPixels[pixelIndex].X unregisteredPixels[pixelIndex].Y returnIntersection[2]
					
					--Set the IPL group of this object.
					--idxIPLGroup = getattrindex "Gta Object" "IPL Group"
					--valIPLGroup = setattr newRef idxIPLGroup vegetationIPLGroup
					
					append vegetationSelectionSet newRef
				)
			)
		)
	)
)*/

-- This function will create an IPL-formatted file containing all vegetation data
-- for the given tile.   This is still a work in progress -- and will have to be tidied up
-- properly -- maybe we want to create our own SceneXML?
fn ExportVegetation vegetationData containerName dropMode useSelection = 
(
	GetMapBounds()  --This generates the terrainUtil_*Bounds objects.
	
	if ( RsTerrainGlobals.MinBounds == undefined or RsTerrainGlobals.MaxBounds == undefined ) then 
		return false 
	
	DropVegetation vegetationData dropMode useSelection containerName
)

-- Load the Vegetation data XML file.
fn LoadVegetation =
(
	local vegetationDataName = "RSG.MaxUtils.VegetationData"  --.NET Object Name
	vegetationData = dotNetObject vegetationDataName 
	if vegetationData == undefined then 
	(
		print ("Unable to instantiate " + vegetationDataName + " object.")
		return undefined
	)
	
	local filename = RsConfigGetMetadataDir() + "terrain/Vegetation.xml"
	vegetationData = vegetationData.Load filename
	
	if ( vegetationData == undefined ) then
	(
		print ("Unable to load " + filename + "!");
		return false
	)
	
	print ("Vegetation Data: " + vegetationData as string)
	print ("Successfully loaded " + filename + ".")
	RsTerrainGlobals.VegetationData = vegetationData
	vegetationData
)
