--
--	RsExcludedTextureNamesStruct
--	Reads filenames from an xml file e.g.(X:\gta5\assets_ng\maps\excluded_textures.xml) which are not allowed through the export process.
--

filein "pipeline/util/xml.ms"

global RsExcludedTextures
global gRsExcludedTexturesInit
struct RsExcludedTextureNamesStruct
(
	--------------------------------------------------------------
	-- our list of names which should be excluded
	--------------------------------------------------------------
	textureNames = #(),
	--------------------------------------------------------------
	-- Load up the xml and get our list of textures
	--------------------------------------------------------------
	fn Init = (

		local fname = ((RsConfigGetAssetsDir core:true) + "maps/excluded_textures.xml")
		grsPerforce.sync #(fname) progressTitle:"Texture exclusion list..."

		if( not RsFileExists fname ) then(
			messagebox ("Cannot find or sync file for texture exclusion: " + fname) title:"Warning!"
			gRsExcludedTexturesInit = false
			return false
		)

		doc = XmlDocument()
		doc.Init()	
		doc.Load fname

		-- item 0 is declaration, 1 is first item ("textures")
		rootElem = (doc.document.childnodes.item 1)
		for i = 0 to rootElem.childnodes.count - 1 do(
			texElem = rootElem.childnodes.item i
			if(texElem.name == "texture") do(
				append textureNames (texElem.attributes.item 0).Value
			)
		)

		doc.Release()

		gRsExcludedTexturesInit = true

		gRsExcludedTexturesInit
	),


	--------------------------------------------------------------
	-- Check a string for the excluded filenames
	--------------------------------------------------------------
	fn CheckName texName = (
		if(classof texName != String) or not gRsExcludedTexturesInit then (
			return false
		)
		
		0!=(findItem textureNames (getFilenameFile texName))
	)
)

if ( gRsExcludedTexturesInit == undefined) or (gRsExcludedTexturesInit == false) do (
	(RsExcludedTextures = RsExcludedTextureNamesStruct())
	(RsExcludedTextures.Init())
)