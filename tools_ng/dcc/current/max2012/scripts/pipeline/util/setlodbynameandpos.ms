--
-- File:: pipeline/util/setlodbynameandpos.ms
-- Description:: Sets the lod by the name of the object and its position
--
-----------------------------------------------------------------------------

global LinkType_LOD = 0

for obj in selection do
(	
	if ( classof obj == editable_mesh or classof obj == editable_poly or
		isRefObj obj or isInternalRef obj) do
	(
		if ( matchpattern obj.name pattern:"*lod*" == false ) do
		(
			for lods in $objects do
			(				
				if ( classof lods == editable_mesh or classof obj==editable_poly or
					isRefObj lods or isInternalRef lods) do
				(
					if ( matchpattern lods.name pattern:"*lod*" == true ) do
					(		
						-- If the objects are close then use as lod parent
						if ( distance obj.position lods.position <= 1.0 ) do
						(
							RsSceneLink.AddContainer LinkType_LOD obj
							RsSceneLink.SetParent LinkType_LOD obj lods
						)
					)
				)
			)
		)
	)
)
