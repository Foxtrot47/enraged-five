-- DLC Creation Script
-- Terry Litrenta
-- May 6 / 2013

-- Creates the XML files and associated file structure for a given DLC project.

fn WriteDLCContentHeader projectName xmlFile =
(	
	format "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\"
	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
	xmlns:xi=\"http://www.w3.org/2003/XInclude\"
	xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\">
    
	<xi:include href=\"\%RS_TOOLSROOT\%/etc/content/content_attributes.xml\" />\n\n" to:xmlFile
	
	format "\t<!--\n\t\tGTA5 DLC Project: %\n\t-->\n" projectName to:xmlFile
	format "\t<graph id=\"%\" edgedefault=\"directed\">\n" projectName to:xmlFile
)

fn WriteDLCContentFooter xmlFile = 
(
	format "\t</graph>\n" to:xmlFile
	format "</graphml>" to:xmlFile
)

fn CreateNodeEntries nodes xmlFile = 
(
	format "\t\t<!-- Nodes -->\n" to:xmlFile
	
	for pedNode in nodes do
	(
		format "\t\t<node id=\"%\">\n" pedNode.name to:xmlFile
		format "\t\t\t<data key=\"content_type\">file</data>\n" to:xmlFile
		format "\t\t\t<data key=\"path\">%</data>\n" pedNode.path to:xmlFile
		format "\t\t</node>\n" to:xmlFile 
	)
	
	format "\n" to:xmlFile
)

fn CreateProcessEntries processes xmlFile =
(
	format "\t\t<!-- Processes -->\n" to:xmlFile
	
	for process in processes do
	(
		local source = process[1]
		local target = process[2]
		local processor = process[3]
		
		format "\t\t<edge source=\"%\" target=\"%\">\n" source target to:xmlFile
		format "\t\t\t<data key=\"processor\">%</data>\n" processor to:xmlFile
		format "\t\t</edge>\n" to:xmlFile
	)
)

fn CreateMainDLCFile projectName xmlFile = 
(
	-- May god have mercy on my soul, I had a NIGHTMARE trying to make this into a string literal because of the %, ! and # in the string so brought the file into EditPlus and did a quick
	-- key recording to generate this. Not the nicest but it'll do!
	
	format "<?xml version='1.0'?>\n" to:xmlFile
	format "<!-- \n" to:xmlFile
	format "	File: //gta5_dlc/dlc_%/dlc_%.xml\n" projectName projectName to:xmlFile
	format "	Description: GTA5 DLC project configuration.\n" to:xmlFile
	format "	\n" to:xmlFile
	format "	Author: Auto-Generated in Max\n" to:xmlFile
	format "-->\n" to:xmlFile
	format "<project name=\"dlc_%\" cache=\"$(root)/cache\">\n" projectName to:xmlFile
	format "\n" to:xmlFile
	format "  <branches default=\"dev\">\n" to:xmlFile
	format "    <!--\n" to:xmlFile
	format "			Main Development Branch\n" to:xmlFile
	format "		-->\n" to:xmlFile
	format "    <branch name=\"dev\" art=\"$(root)/art\" anim=\"$(art)/anim\" assets=\"$(root)/assets\" export=\"$(assets)/export\" processed=\"$(assets)/processed\" build=\"$(root)/build/$(branch)\" metadata=\"$(assets)/metadata\" common=\"$(build)/common\" shaders=\"$(common)/shaders\" code=\"$(root)/src/$(branch)\" ragecode=\"$(root)/src/$(branch)/rage\" script=\"$(root)/script/dev\" preview=\"$(build)/preview\" audio=\"$(root)/audio/$(branch)\" tune=\"$(assets)/fragments\" text=\"$(assets)/GameText\" maptexdir=\"maps_half\">\n" to:xmlFile
	format "			<targets>\n" to:xmlFile
	format "				<target platform=\"win32\" path=\"$(build)/pc\" shaders=\"$(shaders)/win32_30\" shader_ext=\"fxc\" code_platform=\"PC\" build_label_prefix=\"pc\" />\n" to:xmlFile
	format "				<target platform=\"win64\" path=\"$(build)/x64\" shaders=\"$(shaders)/win32_30\" shader_ext=\"fxc\" code_platform=\"x64\" build_label_prefix=\"x64\" />\n" to:xmlFile
	format "				<target platform=\"xbox360\" path=\"$(build)/xbox360\" shaders=\"$(shaders)/fxl_final\" shader_ext=\"fxc\" code_platform=\"Xbox 360\" build_label_prefix=\"360\" />\n" to:xmlFile
	format "				<target platform=\"ps3\" path=\"$(build)/ps3\" shaders=\"$(shaders)/psn\" shader_ext=\"cgx\" code_platform=\"PS3\" build_label_prefix=\"PS3\" />\n" to:xmlFile
	format "			</targets>\n" to:xmlFile
	format "			<rage>\n" to:xmlFile
	format "				<ragebuilders>			\n" to:xmlFile
	format "					<ragebuilder platform=\"xbox360\" exe=\"$(toolsbin)/ragebuilder_0378.exe\" options=\"compressResources!1#texMinValue!32#triStrip!0#\" />\n" to:xmlFile
	format "					<ragebuilder platform=\"ps3\" exe=\"$(toolsbin)/ragebuilder_0378.exe\" options=\"compressResources!1#texMinValue!1#triStrip!0#\" />\n" to:xmlFile
	format "					<ragebuilder platform=\"win32\" exe=\"$(toolsbin)/ragebuilder_0378.exe\" options=\"compressResources!1#texMinValue!32#texMaxValue!1024#triStrip!0#\" />\n" to:xmlFile
	format "					<ragebuilder platform=\"win64\" exe=\"$(toolsbin)/ragebuilder_x64_0378.exe\" options=\"compressResources!1#texMinValue!32#texMaxValue!512#triStrip!0#\" />\n" to:xmlFile
	format "				</ragebuilders>\n" to:xmlFile
	format "			</rage>\n" to:xmlFile
	format "			<content>$(root)/dlc_content.xml</content>\n" to:xmlFile
	format "		</branch>\n" to:xmlFile
	format "				\n" to:xmlFile
	format "		<!--\n" to:xmlFile
	format "			Main Release Branch\n" to:xmlFile
	format "		-->\n" to:xmlFile
	format "		<branch name=\"release\" art=\"$(root)/art\" anim=\"$(art)/anim\" assets=\"$(root)/assets\" build=\"$(root)/build/$(branch)\" export=\"$(assets)/export\" processed=\"$(assets)/processed\" metadata=\"$(assets)/metadata\" common=\"$(build)/common\" shaders=\"$(common)/shaders\" code=\"$(root)/src/$(branch)\" ragecode=\"$(root)/src/$(branch)/rage\" script=\"$(root)/script/dev\" preview=\"$(build)/preview\" audio=\"$(root)/audio/$(branch)\" tune=\"$(assets)/fragments\" text=\"$(assets)/GameText\" maptexdir=\"maps_half\">\n" to:xmlFile
	format "			<targets>\n" to:xmlFile
	format "				<target platform=\"win32\" path=\"$(build)/pc\" shaders=\"$(shaders)/win32_30\" shader_ext=\"fxc\" code_platform=\"PC\" build_label_prefix=\"pc\" />\n" to:xmlFile
	format "				<target platform=\"win64\" path=\"$(build)/x64\" shaders=\"$(shaders)/win32_30\" shader_ext=\"fxc\" code_platform=\"x64\" build_label_prefix=\"x64\" />\n" to:xmlFile
	format "				<target platform=\"xbox360\" path=\"$(build)/xbox360\" shaders=\"$(shaders)/fxl_final\" shader_ext=\"fxc\" code_platform=\"Xbox 360\" build_label_prefix=\"360\" />\n" to:xmlFile
	format "				<target platform=\"ps3\" path=\"$(build)/ps3\" shaders=\"$(shaders)/psn\" shader_ext=\"cgx\" code_platform=\"PS3\" build_label_prefix=\"PS3\" />\n" to:xmlFile
	format "			</targets>\n" to:xmlFile
	format "			<rage>\n" to:xmlFile
	format "				<ragebuilders>			\n" to:xmlFile
	format "					<ragebuilder platform=\"xbox360\" exe=\"$(toolsbin)/ragebuilder_0378.exe\" options=\"compressResources!1#texMinValue!32#triStrip!0#\" />\n" to:xmlFile
	format "					<ragebuilder platform=\"ps3\" exe=\"$(toolsbin)/ragebuilder_0378.exe\" options=\"compressResources!1#texMinValue!1#triStrip!0#\" />\n" to:xmlFile
	format "					<ragebuilder platform=\"win32\" exe=\"$(toolsbin)/ragebuilder_0378.exe\" options=\"compressResources!1#texMinValue!32#texMaxValue!512#triStrip!0#\" />\n" to:xmlFile
	format "          <ragebuilder platform=\"win64\" exe=\"$(toolsbin)/ragebuilder_x64_0378.exe\" options=\"compressResources!1#texMinValue!32#texMaxValue!512#triStrip!0#\" />\n" to:xmlFile
	format "        </ragebuilders>\n" to:xmlFile
	format "			</rage>\n" to:xmlFile
	format "			<content>$(root)/dlc_content.xml</content>\n" to:xmlFile
	format "		</branch>	\n" to:xmlFile
	format "	</branches>\n" to:xmlFile
	format "\n" to:xmlFile
	format "</project>\n" to:xmlFile
)

fn CreateMaxFile maxFile = 
(
	-- Don't blow away an already created file.
	if (RsFileExists maxFile) then
	(
		format "DLC Maxfile (%) already exists. Not creating a new one.\n" maxfile
		return false
	)
	
	RsMakeSurePathExists maxfile
	
	resetMaxFile()
	saveMaxFile maxFile
	
	return true
)

fn UpdateContentXML projectName =
(
	format "Please copy the line below into the <projects> section of the X:\\gta5\\tools\\config.xml file\n"
	format "\n\n<dlc name=\"dlc_%\" uiname=\"<Please Update Me With Friendly Name>\" root=\"x:/gta5_dlc/$(name)\" config=\"$(root)/dlc_%.xml\" />\n\n" projectName projectName
)

fn SetupDLCPedEntry projectName streamed xmlFile = 
(
	local pedFolder = "componentpeds"
	local propFolder = "pedprops"
	
	if (streamed) then
	(
		pedFolder = "streamedpeds"
		propFolder = "streamedpedprops"
	)
	
	local pedSourceMaxFileEntry = "$(art)/peds/" + projectName + "/" + projectName + ".max"
	local pedExportFileEntry = "$(export)/models/cdimages/" + pedFolder + "/" + projectName + ".ped.zip"
	local pedPlatformFileEntry = "$(target)/models/cdimages/" + pedFolder + "/" + projectName + ".rpf"
	
	local propExportFileEntry = "$(export)/models/cdimages/" + propFolder + "/" + projectName + "_p.prop.zip"
	local propPlatformFileEntry = "$(target)/models/cdimages/" + propFolder + "/" + projectName + "_p.rpf"
	
	local nodeArray = #()
	
	-- Ped Nodes
	append nodeArray (dataPair name:"ped:source" path:pedSourceMaxFileEntry)
	append nodeArray (dataPair name:"ped:export" path:pedExportFileEntry)
	append nodeArray (dataPair name:"ped:platform" path:pedPlatformFileEntry)
	
	-- Prop Nodes
	append nodeArray (dataPair name:"prop:source" path:pedSourceMaxFileEntry)
	append nodeArray (dataPair name:"prop:export" path:propExportFileEntry)
	append nodeArray (dataPair name:"prop:platform" path:propPlatformFileEntry)
	
	CreateNodeEntries nodeArray xmlFile
	
	-- Processors
	
	local processorArray = #()
	
	-- Ped Processes
	append processorArray #( "ped:source", "ped:export", "RSG.Pipeline.Processor.Common.Dcc3dsmaxExportProcessor" )
	append processorArray #( "ped:export", "ped:platform", "RSG.Pipeline.Processor.Common.CharacterProcessor" )
	
	-- Prop Processes
	append processorArray #( "prop:source", "prop:export", "RSG.Pipeline.Processor.Common.Dcc3dsmaxExportProcessor" )
	append processorArray #( "prop:export", "prop:platform", "RSG.Pipeline.Processor.Common.CharacterProcessor" )
	
	CreateProcessEntries processorArray xmlFile
)

fn CreateNewDLCPed projectName streamed =
(
	local dlcContentXMLFile = "X:/gta5_dlc/dlc_" + projectName + "/dlc_content.xml"
	RsMakeSurePathExists dlcContentXMLFile
	local xmlFile = createFile dlcContentXMLFile
	
	local dlcXMLFile = "X:/gta5_dlc/dlc_" + projectName + "/dlc_" + projectName + ".xml"
	RsMakeSurePathExists dlcXMLFile
	local xmlProjectFile = createFile dlcXMLFile
	
	CreateMainDLCFile projectName xmlProjectFile
	
	WriteDLCContentHeader projectName xmlFile
	SetupDLCPedEntry projectName true xmlFile
	WriteDLCContentFooter xmlFile
	
	flush xmlProjectFile
	close xmlProjectFile
	
	flush xmlFile
	close xmlFile
	
	local pedFolder = "componentpeds"
	if (streamed) then pedFolder = "streamedpeds"
	
	local dlcContentMaxFile = "X:/gta5_dlc/dlc_" + projectName + "/art/peds/" + pedFolder + "/" + projectName + ".max"
	CreateMaxFile dlcContentMaxFile
	
	UpdateContentXML projectName
	
	local message = stringStream ""
	format "Created new DLC Project: %\nSee MAXScript Listener window for information about adding this to config.xml" projectName to:message
	messagebox message
)

fn CheckForExistingDLC projectName =
(
	local dlcContentXMLFile = "X:/gta5_dlc/dlc_" + projectName + "/dlc_content.xml"
	
	if (RsFileExists dlcContentXMLFile ) then
		return true
	
	return false
)

fn CreateNewDLCMap projectName =
(
	-- Might use this at some point, but will be called from a "map" rollout.
)

rollout RsPedDLCCreationRollout "DLC Ped Project"
(
	checkbox chkStreamedPed "Streamed Ped" checked:true
	edittext edtDLCProjectName "DLC Name"
	button btnCreateDLCProject "Create DLC Project"
	
	on btnCreateDLCProject pressed do
	(
		local projectName = edtDLCProjectName.text
		local dlcFolder = ""
		
		-- Strip out the "dlc_" for the project name
		if (matchPattern projectName pattern:"dlc_*") then
			projectName = substituteString projectName "dlc_" ""
		
		local existingDLC = CheckForExistingDLC projectName
		
		if (existingDLC) then
		(
			local message = stringStream ""
			format "A DLC project with the name (dlc_%) already exists.\nPlease choose a different name." projectName to:message
			messagebox message
		)			
		else
		(
			CreateNewDLCPed projectName chkStreamedPed.checked
		)
	)
	
	on RsPedDLCCreationRollout rolledUp down do 
	(
		RsSettingWrite "RsPedDLCCreation" "rollup" (not down)
	)
)