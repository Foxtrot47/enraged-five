-- 
-- File:: pipeline/util/remoteConsole.ms
-- Description:: Wrapper functions for use with the RemoteConnection interface
--


--	
-- fn	RSRemConIsGameRunning
-- desc	Checks to see if the game is running, and if queryRestart is true
-- 		it asks if they want to restart the console with a timer query box.
--		Another way to call is with doRestart set to true, which ignores the
--		querybox and goes ahead with a console restart if connected.
--
fn RSRemConIsGameRunning queryRestart:false doRestart:false = 
(
	pushPrompt "Checking for running game..."

	retVal = false
	if ( RemoteConnection.Connect() ) then
	(
		if ( RemoteConnection.IsConnectedToGame() ) then
		(
			retVal = true
			consoleIP = RemoteConnection.GetGameIP()
			consolePlatform = RemoteConnection.GetPlatform()
			exeName = RemoteConnection.GetExecutableName()

			if ( consolePlatform != undefined and consoleIP != undefined ) then
			(
				if ( queryRestart ) do
				(
					if ( RsQueryBoxTimeOut "The game is running, do you wish to restart the console?" timeout:10 == true ) then doRestart = true
				)
				-- If we want to do the restart (either from querybox or the doRestart param given as true,
				-- restart the console
				if (doRestart == true ) do
				(	
					format "Resetting %:%\n" consolePlatform consoleIP
					if ( consolePlatform == "PlayStation3" ) then
					(
						doscommand ( "ps3run -r -t " + consoleIP )
					)
					else if ( consolePlatform == "Xbox360" ) then
					(					
						xboxSDKDir = "%xedk%\bin\win32"
						cmd = ( "pushd " + xboxSDKDir + " && xbReboot && popd && TASKKILL /F /IM \"xbwatson.exe\"" )
						doscommand cmd
					)
					else if ( consolePlatform == "PlayStation4" ) then
					(
						orbisSDKDir = "%SCE_ROOT_DIR%\ORBIS\Tools\Target Manager Server\bin"
						cmd = ( "pushd " + orbisSDKDir + " && orbis-ctrl reboot && popd" )
						HiddenDOSCommand cmd donotwait:true
					)
					else if ( consolePlatform == "XboxOne" ) then
					(
						durangoSDKDir = "%DurangoXDK%\bin"
						cmd = ( "pushd " + durangoSDKDir + " && xbreboot && popd" )
						HiddenDOSCommand cmd donotwait:true
					)
					else if ( consolePlatform == "Win64" ) then
					(
						doscommand ( "taskkill /F /IM " + exeName )
					)
					else
					(
						MessageBox ("Unable to determine the appropriate action for restarting the game.  Please contact tools and let them know that the '" + consolePlatform + "' isn't currently being handled.\n\nThe export will attempt to continue but will likely fail further down the line.")
					)
					retVal = false
				)
			)
			else
			(
				retVal = false
			)
		)			
		RemoteConnection.Disconnect()
	)
	
	popPrompt()
	
	retVal
)


--	
-- fn	RSRemConIsGameRunning
-- desc	Checks to see if the game is running, and if queryRestart is true
-- 		it asks if they want to restart the console with a timer query box.
--		Another way to call is with doRestart set to true, which ignores the
--		querybox and goes ahead with a console restart if connected.
--
fn RSRemConIsGameRunningWait queryRestart:false = 
(
	pushPrompt "Checking for running game..."

	retVal = false
	if ( RemoteConnection.Connect() ) then
	(
		if ( RemoteConnection.IsConnectedToGame() ) do
		(
			retVal = true
			consoleIP = RemoteConnection.GetGameIP()
			consolePlatform = RemoteConnection.GetPlatform()
			exeName = RemoteConnection.GetExecutableName()

			if ( consolePlatform != undefined and consoleIP != undefined ) then
			(
				if ( queryRestart ) do
				(
					if ( RsQueryBoxTimeOut "The export can not continue without restarting the currently running game. Do you wish to restart the console?" title:"Game Running" timeout:-1 == true ) then
					(
						doRestart = true
					)
					else
					(
						break
					)
				)
				-- If we want to do the restart (either from querybox or the doRestart param given as true,
				-- restart the console
				format "Resetting %:%\n" consolePlatform consoleIP
				if ( consolePlatform == "PlayStation3" ) then
				(
					doscommand ( "ps3run -r -t " + consoleIP )
				)
				else if ( consolePlatform == "Xbox360" ) then
				(					
					xboxSDKDir = "%xedk%\bin\win32"
					cmd = ( "pushd " + xboxSDKDir + " && xbReboot && popd && TASKKILL /F /IM \"xbwatson.exe\"" )
					doscommand cmd
				)
				else if ( consolePlatform == "PlayStation4" ) then
				(
					orbisSDKDir = "SCE_ROOT_DIR%\ORBIS\Tools\Target Manager Server\bin"
					cmd = ( "pushd " + orbisSDKDir + " && orbis-ctrl reboot && popd" )
					doscommand cmd
				)
				else if ( consolePlatform == "PlayStation4" ) then
				(
					orbisSDKDir = "%SCE_ROOT_DIR%\ORBIS\Tools\Target Manager Server\bin"
					cmd = ( "pushd " + orbisSDKDir + " && orbis-ctrl reboot && popd" )
					HiddenDOSCommand cmd donotwait:true
				)
				else if ( consolePlatform == "XboxOne" ) then
				(
					durangoSDKDir = "%DurangoXDK%\bin"
					cmd = ( "pushd " + durangoSDKDir + " && xbreboot && popd" )
					HiddenDOSCommand cmd donotwait:true
				)
				else if ( consolePlatform == "Win64" ) then
				(
					doscommand ( "taskkill /F /IM " + exeName )
				)
				else
				(
					MessageBox "Unable to determine the appropriate action for restarting the game.  Please contact tools and let them know that the '" + consolePlatform + "' isn't currently being handled.\n\nThe export will attempt to continue but will likely fail further down the line."
				)
				retVal = false
			)
			else
			(
				retVal = false
			)
		)			
		RemoteConnection.Disconnect()
	)
	
	popPrompt()
	
	retVal
)
