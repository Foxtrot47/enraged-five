--
-- File:: pipeline/helpers/vehicles/vehicle.ms
-- Description:: Vehicle Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 19 March 2010
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/helpers/vehicles/stray_vert_cleaner.ms"
filein "pipeline/util/pivot_reset.ms"
filein "pipeline/export/vehicles/utils.ms"
fileIn "pipeline/util/vehicle_lod_utils.ms"
-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout ResetRoll "Reset Pivot Transform and Scale"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Vehicle_Toolkit#Reset_Pivot_Transform_and_Scale" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnGo "Go" width:150 height:30
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnGo pressed do (
		for o in $selection do 
			pivot_reset_transform o
	)
)

rollout ReplacementRoll "Replace items"
(
	button btnLegacyWheels "Replace legacy wheels" width:150
	
	on btnLegacyWheels pressed do
	(
		if selection[1]==undefined then
		(
			messagebox "Select an object!"
			return false
		)
		
		local obj = selection[1]
		while undefined!=obj.parent do obj = obj.parent
			
		RsVehicleUtil.ReplaceLegacyWheels obj
	)
)



rollout ModToolRoll "Mod Tools"
(
	button btnAutoLod "AutoLod Vehicle Mods" tooltip:"Select all parts that need lodding and click this to attempt to automatically set up the lod chains for mods."

	on btnAutoLod pressed do 
	(
		RsVehicleLodUtils.RsVehicleModAutoLod()
	)
)

try CloseRolloutFloater VehicleToolkit catch()
VehicleToolkit = newRolloutFloater "Vehicle Toolkit" 200 400 50 96
addRollout ResetRoll VehicleToolkit
addRollout strayverts VehicleToolkit
addRollout ReplacementRoll VehicleToolkit
addRollout ModToolRoll VehicleToolkit