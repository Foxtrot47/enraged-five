--------------------------------------------------------
-- Read in interprete and store materials.dat
----------------------------------------------------------

--includes
filein "rockstar/export/settings.ms"

-- globals
global gCollProperties

-- class
struct sCollProperties
(
	propTypes = #(#MATERIAL,#FILTER,#FX_GROUP,#VFX_DISTURBANCE_TYPE,#RUMBLE_PROFILE,#REACT_WEAPON_TYPE,#RAGE_FRICTION,#RAGE_ELASTICITY,#DENSITY,#TYRE_GRIP,#WET_GRIP,#TYRE_DRAG,#TOP_SPEED_MULT,#SOFTNESS,#NOISINESS,#PENETRATION_RESISTANCE,#SEE_THRU,#SHOOT_THRU,#SHOOT_THRU_FX,#NO_DECAL,#POROUS,#HEATS_TYRE,#MATERIAL),
	
	values = #(),
	hashes = dotnetobject "RSG.MaxUtils.MaxDictionary",
	
	fn read = 
	(
		local url = RsConfigGetCommonDir()+"/data/materials/materials.dat"
		local matFile = openFile url mode:"r"
		
		if undefined==matFile then
		(
			messagebox ("Could not find material file \""+url as string+"\". Please get the common folder for your dev configuration.")
			return false
		)
		
		local readError
		local errorRows = #()
		
		while not eof matFile do
		(
			local currLine = readLine matFile
			if currLine.count==0 or currLine[1]=="#" then 
				continue
				
			local datarow = filterstring currLine " \t"
			if datarow.count<2 then
				continue
			if datarow.count!=propTypes.count then 
			(
				readError = datarow.count
				append errorRows dataRow[1]
				continue
			)
--			print ("Number of elements of "+datarow[1]+":"+datarow.count as string)
			for dataindex=1 to datarow.count do
			(
				local parseVal = datarow[dataindex] as number
				if undefined != parseVal then
					datarow[dataindex] = parseVal
			)
			append values datarow
			if datarow.count>1 then
			(
				hashes.add datarow[1] (values.count)
			)
		)
		close matFile
		
		if (readError != undefined) do 
		(
			format "ERROR READING: %\nREAD IN VALUES DON'T MATCH NUMBER OF PROPERTIES! (% items on line vs % known headers)\nError rows:" url readError propTypes.count
			for item in errorRows do 
			(
				format " %," item
			)
			format "\n"
		)
	),
	
	fn get name propName =
	(
		if String == classof name then
			name = hashes.item name
		return values[name][finditem propTypes propName]
	)
)

-- entry point
--if(undefined==gCollProperties) then
(
	gCollProperties = sCollProperties()
	gCollProperties.read()
)