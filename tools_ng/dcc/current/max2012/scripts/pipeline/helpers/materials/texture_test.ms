-- Rockstar Texture Test
-- Rockstar North
-- 10/5/2013

-- various scene level texture checks

fn RsTexTest_ObjWillHaveMaterialsExported obj =
(
	result = false

	objAttrClass = getAttrClass obj
	if( objAttrClass == "Gta Object" and obj.material != undefined ) do
	(
		idxDontExport = getattrindex "Gta Object" "Dont Export"
		dontExport = getattr obj idxDontExport

		result = not dontExport
	)

	result
)

fn RSTexTest_GetUniqueSceneMaterials =
(
	uniqueSceneMaterials = #()
	
	for obj in $objects do
	(
		if( RsTexTest_ObjWillHaveMaterialsExported obj ) do
		(
			appendIfUnique uniqueSceneMaterials obj.material
		)
	)
	
	uniqueSceneMaterials
)

fn RSTexTest_GetSourceTexturePathnamesRecursive mat  uniqueTexturePathnames =
(
	local matClass = classof mat
	if( matClass == Multimaterial ) then
	(
		for subMat in mat.materialList do 
		(
			if( subMat != undefined ) then
			(
				RSTexTest_GetSourceTexturePathnamesRecursive subMat uniqueTexturePathnames
			)
		)
	)
	else if ( matClass == Rage_Shader ) then
	(
		local varCount = RstGetVariableCount mat
		for varIdx = 1 to varCount do
		(
			if( RstGetVariableType mat varIdx == "texmap" ) do
			(
				local texturePathname = RstGetVariable mat varIdx
				appendIfUnique uniqueTexturePathnames texturePathname
			)
		)
	)
)

fn RSTexTest_GetUniqueSourceTexturePathnames materials =
(
	uniqueTexturePathnames = #()
	
	for mat in materials do
	(
		RSTexTest_GetSourceTexturePathnamesRecursive mat uniqueTexturePathnames
	)
	
	uniqueTexturePathnames
)

fn RSTexTest_CheckForTextureFilenameCollisions texturePathnames =
(
	result = true
	
	filenameLocationHashtable = dotnetobject "System.Collections.Hashtable"
	
	for texturePathname in texturePathnames do
	(
		textureFilename = getFilenameFile texturePathname
		
		if( filenameLocationHashtable.ContainsKey(textureFilename) ) then
		(
			oldCollection = filenameLocationHashtable.item[textureFilename]
			filenameLocationHashtable.Remove textureFilename
			filenameLocationHashtable.Add textureFilename (append oldCollection texturePathname)
		)
		else
		(
			filenameLocationHashtable.Add textureFilename #(texturePathname)
		)
	)
	
	local enumerator = filenameLocationHashtable.GetEnumerator()
	while enumerator.MoveNext() do
	(
		if( enumerator.value.count > 1 ) do
		(
			gRsULog.LogError("Texture with filename " + enumerator.key + " is referenced from more than one location:")
			for texPathname in enumerator.value do
			(
				gRsULog.LogError("\t" + texPathname)
			)
			result = false
		)
	)
	
	result
)

fn RSTexTest_CheckScene =
(
	gRsULog.ClearLogDirectory()

	gRsULog.LogMessage("Checking scene textures")
	uniqueSceneMaterials = RSTexTest_GetUniqueSceneMaterials()
	
	gRsULog.LogMessage("Checking " + uniqueSceneMaterials.count as string + " unique exportable scene materials")
	uniqueTexturePathnames = RSTexTest_GetUniqueSourceTexturePathnames uniqueSceneMaterials

	gRsULog.LogMessage("Checking " + uniqueTexturePathnames.count as string + " unique exportable texture pathnames")
	result = RSTexTest_CheckForTextureFilenameCollisions uniqueTexturePathnames
	
	gRsULog.Validate()
)