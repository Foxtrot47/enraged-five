-- material templating
-- automation for managing material 
-- @author Gunnar Droege

-- rules for variable automation
global 	gRuleDocFilename = (::RsConfigGetEtcDir() + "/config/maps/gta5/wetnessMap.xml")

struct sCompareNode (type, varName=undefined, varType=undefined, varValue=undefined)

fn SortComparator a b = (
	if a.rule.varValue > b.rule.varValue then 1
	else if a.rule.varValue < b.rule.varValue then -1
	else 0
)

struct sMtlSystem
(
	rulesMaterials = #("terrain_cb_*"),
	ruleList = #(), -- list of DataPair witth key-value pairs for test and result respectively
	
	fn FindMap ruleName loading:false = 
	(
		if undefined == ruleName do
			ruleName = "undefined"
		local Node = undefined
		for ruleType in ruleList while undefined==Node do
		(
			if (matchPattern ruleName pattern:ruleType.name) then
				Node = ruleType
			else if (loading and matchPattern ruleType.name pattern:ruleName) do
			(
				--in the rare case our pattern actually matches the name, then the name is obviously the more general case, so swap to using it.
				--(only want this to happen if we are using this to read in new values from xml)
				Node=ruleType
				ruleType.name=ruleName
			)
		)
		Node
	),
	
	fn ReadRuleDoc = 
	(
 		local ruleXmlDoc = XmlDocument()
 		ruleXmlDoc.load gRuleDocFilename
		local rootElem = RsGetXmlElement ruleXmlDoc.document "Root"
		ruleList = #()
		for i=0 to (rootElem.ChildNodes.Count-1) do
		(
			local ruleNode = rootElem.ChildNodes.item i
			local searchNode = ruleNode.ChildNodes.item 0
			local comparisonTestNode = sCompareNode type:searchNode.name varName:(searchNode.Attributes.itemof "name") varType:(searchNode.Attributes.itemof "type") varValue:searchNode.innerText
			local resultNode = ruleNode.ChildNodes.item 1
			local comparisonResultNode = sCompareNode type:resultNode.name varName:(resultNode.Attributes.itemof "name") varType:(resultNode.Attributes.itemof "type") varValue:resultNode.innerText
			
			local ruleType = FindMap comparisonTestNode.varName.value loading:true
			
			if undefined!=ruleType then --we have a map for this type already
				append ruleType.rules (DataPair rule:comparisonTestNode result:comparisonResultNode)
			else 						--we need to add a new datapair
				append ruleList (DataPair name:comparisonTestNode.varName.value rules:#(DataPair rule:comparisonTestNode result:comparisonResultNode))
		)
	),
	
	fn Init = 
	(
		local depotFile = gRsPerforce.local2depot gRuleDocFilename
		if undefined!=depotFile then
			gRsPerforce.sync depotFile
		if not doesFileExist gRuleDocFilename then
			return false
		
		ReadRuleDoc()
		--sort each list of rules
		for ruleType in ruleList do
		(
			qsort ruleType.rules SortComparator
		)
		return true
	),
	
	fn TestRules compareInstNode = 
	(
		local result = undefined
		local ruleNode = FindMap compareInstNode.varName
		
		if undefined!=ruleNode then
		(
			--See if we can quick find the actual value first
			local lookupInstNode = (DataPair rule:compareInstNode result:undefined)
			local matched = bsearch lookupInstNode ruleNode.rules SortComparator
			if matched != undefined and (matched.rule.varType==undefined or compareInstNode.varType == matched.rule.varType.value) do
			(
				result = matched.result
			)
			
			--If we still don't have a result, we couldn't match a value, but we have matched name already. lin search for a type match
			if result==undefined do
			(
				local found = false
				for rulePair in ruleNode.rules while not found do
				(
					if (rulePair.rule.varType==undefined or compareInstNode.varType == rulePair.rule.varType.value) do
						result = sCompareNode type:"resultNode"
				)
			)
		)
		
		result
	),
	
	fn DriveVariables mtl = 
	(
		if not isKindof mtl Rage_Shader then
			return false
		
		local shaderName = RstGetShaderName mtl
		local isRuleMtl = false
		for ruleMtlPattern in rulesMaterials while not isRuleMtl do
		(
			if (matchPattern shaderName pattern:ruleMtlPattern) then
			(
				isRuleMtl=true
				local index = findString shaderName "_cb_"
				if undefined!=index and shaderName[index+4]!="w" then
				(
					local newshaderName = replace shaderName (index+4) 0 "w_"
					gRsUlog.LogMessage ("Auto setting material \""+mtl.name+"\" preset type \""+shaderName as string+"\" to \""+newshaderName as string+"\"") context:mtl
					shaderName = newshaderName 
					RstSetShaderName mtl shaderName
				)
			)
		)
		if not isRuleMtl then
			return false
		
		local varCount = RstGetVariableCount mtl
		local shaderVarNameValueMap = dotnetObject "RSG.MaxUtils.MaxDictionary"
		local shaderVarNameIndexMap = dotnetObject "RSG.MaxUtils.MaxDictionary"
		local shaderVarNameTypeMap = dotnetObject "RSG.MaxUtils.MaxDictionary"
		for varIndex = 1 to varCount do
		(
			local varName = tolower (RstGetVariableName mtl varIndex)
			local varType = tolower (RstGetVariableType mtl varIndex)
			local varValue = (RstGetVariable mtl varIndex)
			if isProperty varValue "x" then
			(
				varType = "string"
				varValue = varValue as string
			)
			if varType=="texmap" then
			(
				varValue = tolower (getFilenameFile varValue)
				varType = "texture"
			)
			shaderVarNameValueMap.add varName varValue
			shaderVarNameIndexMap.add varName varIndex
			shaderVarNameTypeMap.add varName varType
		)
		
		local varEnum = shaderVarNameValueMap.GetEnumerator()
		local testSuccessCounter = 1
		local vectorVal = (point4 0 0 0 0)
		local targetIndex = -1
		while varEnum.MoveNext() do
		(
			if not (shaderVarNameTypeMap.containsKey varEnum.Current.key) then
			(
				gRsUlog.LogWarning ("Automated variable setting failed: Couldn't determine variable type of \""+varEnum.Current.key+"\" not in shader \""+shaderName+"\"") context:mtl
				exit
			)
			local type = (shaderVarNameTypeMap.item varEnum.Current.key)
			local comparisonTestNode = sCompareNode type:"instance" varName:varEnum.Current.key varType:type varValue:varEnum.Current.value
			local result = TestRules comparisonTestNode
--			print ("var:"+varEnum.Current.value as string+" test result:"+result as string)
			if undefined!=result then
			(
				if undefined==result.varName then
					gRsUlog.LogMessage ("Automated variable setting skipped component:Type or name test met, but value not mapped:"+varEnum.Current.value as string) context:mtl
				else if not (shaderVarNameIndexMap.containsKey result.varName.value) then
					gRsUlog.LogWarning ("Automated variable setting failed: Target variable \""+result.varName.value+"\" not in shader \""+shaderName+"\"") context:mtl
				else
				(
					if targetIndex == -1 then
					(
						targetIndex = (shaderVarNameIndexMap.item result.varName.value)
						vectorVal = (RstGetVariable mtl targetIndex)
					)
					vectorVal[testSuccessCounter] = (result.varValue as float)
				)
				testSuccessCounter += 1
			)
		)
		if targetIndex != -1 then
		(
			gRsUlog.LogMessage ("Auto setting material \""+mtl.name+"\" variable \""+(RstGetVariableName mtl targetIndex) as string+"\" to "+vectorVal as string) context:mtl 
			RstSetVariable mtl targetIndex vectorVal
		)
	)
)

global gMtlSystem = sMtlSystem()
