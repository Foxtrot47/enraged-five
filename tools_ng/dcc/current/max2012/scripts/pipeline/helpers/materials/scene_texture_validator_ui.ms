--
-- File:: pipeline/ui/scene_texture_validator_ui.ms
-- Description::
--
-- Date:: 17 May 2013
--
try (destroyDialog RsSceneTextureValidatorRoll) catch ()

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/helpers/materials/texture_test.ms"

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------

rollout RsSceneTextureValidatorRoll "Scene Texture Validator" width:300 height:55
(
	-------------------------------------------------------------------------
	-- Locals
	-------------------------------------------------------------------------
	
	-------------------------------------------------------------------------
	-- UI Widgets
	-------------------------------------------------------------------------
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Scene Texture Validator" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnValidate "Validate" width:280
	
	-------------------------------------------------------------------------
	-- Methods
	-------------------------------------------------------------------------
	
	-- event handlers
	fn OnOpen =
	(
		
	)
	
	fn OnClose =
	(
		
	)

	fn OnValidate =
	(
		local result = RSTexTest_CheckScene()
		if result then
		(
			messageBox "No problems found" beep:false
		)
	)
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------
	
	on RsSceneTextureValidatorRoll open do
	(
		OnOpen()
	)
	
	on RsSceneTextureValidatorRoll close do
	(
		OnClose()
	)
	
	on btnValidate pressed do
	(
		OnValidate()
	)
)

-----------------------------------------------------------------------------
-- Entry
-----------------------------------------------------------------------------
createDialog RsSceneTextureValidatorRoll

-- pipeline/ui/scene_texture_validator_ui.ms
