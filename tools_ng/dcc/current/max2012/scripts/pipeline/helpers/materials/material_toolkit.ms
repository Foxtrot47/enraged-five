--
-- File:: pipeline/helpers/material/material_toolkit.ms
-- Description:: Material Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 18 March 2010
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/helpers/materials/viewtxd.ms"
filein "rockstar/helpers/specfixup.ms"
filein "pipeline/helpers/materials/CreateMultiSub.ms"
filein "pipeline/helpers/materials/billboardViewer.ms"

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- SCRIPT HELPERS
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout RsMatScriptRoll "Material Helper Scripts" width:280 height:300
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Material_Toolkit#Material_Helper_Scripts" align:#right 
		color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255) offset:[4,-4]

	button btnTexInfo "Texture Map Info" width:124 height:26 across:2
		tooltip:"Output list of textures used by selected objects"
	button btnResetDiff "Reset all Diffuse colours" width:124 height:26 across:2
		tooltip:"Set diffuse colour for all materials in scene to white"
	button btnP4texSync "P4 Sync Scene Textures" height:26 offset:[0,-2]
		tooltip:"Perforce-sync all files used by the scene's materials"

	local multiSubBtnWidth = 180
	groupBox grpMultiSub "Combine/Clean Obj Materials:"	width:(multiSubBtnWidth + 8) height:136 align:#center
	button btnNewSub 		"Create combined multiMat" 				width:multiSubBtnWidth pos:(grpMultiSub.pos + [4,15])
		tooltip:"Create new combined multiMaterial for selected objects, removing any unused submaterials"
	button btnSubObject 	"Create multiMat per object"				width:multiSubBtnWidth pos:(btnNewSub.pos + [0,24])
		tooltip:"Create new multiMaterial for each selected object, removing any unused submaterials"
	button btnSubTXD 		"Create multiMat per TXD" 					width:multiSubBtnWidth pos:(btnSubObject.pos + [0,24])
		tooltip:"Create new combined multiMaterial for each TXD-name used by selected objects, removing any unused submaterials"
	checkBox chkKeepUnique "Keep submats unique" pos:(btnSubTXD.pos + [0,24]) checked:true
		tooltip:"If unticked, submaterials of new multimaterials will be instances of originals"
		
	button btnStripTints "Strip Tint-Palettes" width:multiSubBtnWidth pos:(chkKeepUnique.pos + [0,24])
		tooltip:"Strip tint-palettes from the selected objects materials"

	groupBox grpSubMat "Change Selected's Material:"	width:(multiSubBtnWidth + 8) pos:(grpMultiSub.pos + [0,grpMultiSub.height + 4]) height:114
	
	button btnInsertSubMat	"Add Submat at:" width:(multiSubBtnWidth - 42) pos:(grpSubMat.pos + [4,15])
		tooltip:"Add new submaterial to selected object's multiMaterial, changing material-IDs for all objects that use that material"
	spinner spnInserSubMat	"" width:40 pos:(btnInsertSubMat.pos + [(multiSubBtnWidth - 40),2]) type:#integer range:[1,999,1]
	
	local upDnBtnWidth = (multiSubBtnWidth - 41) / 2
	button btnSubMatUp	"- MatID" width:upDnBtnWidth pos:(btnInsertSubMat.pos + [0,24])
		tooltip:"Move numbered submaterial up its multimaterial, changing material-IDs for all objects that use that material"
	spinner spnMoveSubMat	"" width:40 pos:(btnSubMatUp.pos + [upDnBtnWidth,2]) type:#integer range:[1,999,1]
	button btnSubMatDn	"MatID +" width:upDnBtnWidth pos:(spnMoveSubMat.pos + [14,-2])
		tooltip:"Move numbered submaterial up its multimaterial, changing material-IDs for all objects that use that material"
	
	button btnSwapSubMat	"Swap Submats:" width:(multiSubBtnWidth - 68) pos:(btnSubMatUp.pos + [0,24])
		tooltip:"Swap submaterials for selected object's multiMaterial, changing material-IDs for all objects that use that material"
	spinner spnSwapSubMatA	"" width:34 pos:(btnSwapSubMat.pos + [(multiSubBtnWidth - 67),2]) type:#integer range:[1,999,1]
	spinner spnSwapSubMatB	"" width:34 pos:(spnSwapSubMatA.pos + [11,0]) type:#integer range:[1,999,1]
	
	button btnReOrderMultisub "Re-order Submaterials" width:multiSubBtnWidth pos:(btnSwapSubMat.pos + [0,25])
		tooltip:"Reorder selected object's material to put all alpha textures to bottom of list, to make them sort better in a viewport"
	
	--///////////////////////////////////////////////////////////
	-- functions
	--///////////////////////////////////////////////////////////
	
	--///////////////////////////////////////////////////////////
	-- clear tint palettes from selected object mutimaterials 
	--///////////////////////////////////////////////////////////
	fn clearTintPaletteFromMultMat =
	(
		for obj in selection do
		(
			local multiMat = if (obj.material != undefined) then obj.material else false
			if not (isKindOf multiMat Multimaterial) then continue
			
			for m=1 to multiMat.materialList.count do
			(
				local mat = multiMat.materialList[m]
				
				local shaderName = RstGetShaderName mat
				if shaderName != false then
				(
					if (matchPattern shaderName pattern:"*tnt*") then
					(
						--find the slot for the tint palette
						local rstTex = undefined
						local index = 0
						
						numVars = RstGetVariableCount mat
						for i = 1 to numVars while rstTex == undefined do
						(
							if ((RstGetVariableType mat i) == "texmap") then index += 1
							if ((RstGetVariableName mat i) == "Tint Palette") then rstTex = index
						)
						
						setSubTexmap mat rstTex undefined
					)
				)
			)
		)
		return OK
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnP4texSync pressed do 
	(
		RsP4syncSceneTextures()
	)
	
	on btnNewSub pressed do 
	(
		RScreateNewMultiSub uniqueMats:chkKeepUnique.checked
	)
	
	on btnSubTXD pressed do 
	(
		RScreateNewMultiSubTxd uniqueMats:chkKeepUnique.checked
	)
	
	on btnSubObject pressed do 
	(
		RScreateNewMultiSubObject uniqueMats:chkKeepUnique.checked
	)
	
	on btnTexInfo pressed do (
		filein "rockstar/helpers/texmaps.ms"
	)
	
	on btnResetDiff pressed do 
	(
		ResetDiffuseToWhite()
	)
	
	on btnReOrderMultisub pressed do 
	(
		RsReOrderMultisub()
	)
	
	on btnInsertSubMat pressed do 
	(
		RsInsertSubMatToSel spnInserSubMat.value
	)
	
	on btnSwapSubMat pressed do 
	(
		RsSwapSubMatsForSel spnSwapSubMatA.value spnSwapSubMatB.value
	)
	
	on btnSubMatUp pressed do 
	(
		local newMatNum = spnMoveSubMat.value - 1
		if (RsSwapSubMatsForSel spnMoveSubMat.value newMatNum showErrors:false) do 
		(
			spnMoveSubMat.value = newMatNum
		)
	)
	
	on btnSubMatDn pressed do 
	(
		local newMatNum = spnMoveSubMat.value + 1
		if (RsSwapSubMatsForSel spnMoveSubMat.value newMatNum showErrors:false) do 
		(
			spnMoveSubMat.value = newMatNum
		)
	)
	
	on RsMatScriptRoll rolledUp down do 
	(
		RsSettingWrite "rsMatScriptRoll" "rollup" (not down)
	)
	
	on btnStripTints pressed do
	(
		clearTintPaletteFromMultMat()
	)
)

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- UV MAPPING
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout RsUVMapRoll "UV Mapping" width:286 height:293
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	HyperLink lnkHelp "Help?" pos:[250,3] width:28 height:15 address:"https://devstar.rockstargames.com/wiki/index.php/Material_Toolkit#UV_Mapping" color:(color 0 0 255) hovercolor:(color 0 0 255) visitedcolor:(color 0 0 255)
	GroupBox grpBox "Box Mapper" pos:[8,16] width:264 height:89
	button btnMapsize2 "2x2x2" pos:[15,57] width:55 height:16
	button btnMapsize3 "3x3x3" pos:[15,81] width:55 height:16
	button btnMapsize4 "4x4x4" pos:[79,57] width:55 height:16
	button btnMapsize5 "5x5x5" pos:[79,81] width:55 height:16
	button btnMapsizefit "To Fit" pos:[143,57] width:64 height:16
	button btnMapsizecust "Apply" pos:[231,81] width:32 height:16
	spinner spnMapsiz "Custom " pos:[169,81] width:60 height:16 range:[0,1000,10]
	GroupBox grpTexTools "Texture Tools" pos:[9,108] width:160 height:87
	spinner spnXRep "X Repeat: " pos:[37,126] width:109 height:16
	spinner spnYRep "Y Repeat: " pos:[37,145] width:109 height:16 range:[0,100,0]
	button btnTexture "Texture" pos:[24,168] width:62 height:21
	button btnFacemap "Facemap" pos:[94,168] width:62 height:21
	GroupBox grpMatTools "Material Tools" pos:[173,108] width:99 height:87
	label lblMatID "Mat# to face sel:" pos:[180,133] width:85 height:17
	spinner spnMatID "" pos:[184,150] width:76 height:16 range:[0,100,1] type:#integer
	spinner spnBoxMatChannel "Tex channel: " pos:[153,34] width:104 height:16 range:[1,99,1] type:#integer
	radiobuttons radUnits "" pos:[15,33] width:119 height:16 labels:#("meters", "tiles") columns:2
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- Box Mapper
	--------------------------------------------------------------
	fn bungonboxmap bmapsize = (
		mapmod = uvwmap()

		--set up box map
		mapmod.maptype = 4
		mapmod.mapChannel = spnBoxMatChannel.value
		if radUnits.state==1 then
		(
--			print "maters"
			mapmod.length = bmapsize
			mapmod.width = bmapsize
			mapmod.height = bmapsize
		)
		else
		(
--			print "tiles"
			mapmod.utile = bmapsize
			mapmod.vtile = bmapsize
			mapmod.wtile = bmapsize
		)
		--add modifier to selection
		modPanel.addModtoSelection( mapmod )
	)

	fn bungonboxmapfit = (
		mapmod = uvwmap()

		--set up box map
		mapmod.maptype = 4
			
		--add modifier to selection
		modPanel.addModtoSelection( mapmod )
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////	

	on btnMapsize2 pressed do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 2
				collapseStack $
			)
		)
	on btnMapsize2 rightClick do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 2
				modPanel.addModtoSelection( unwrap_uvw() )
				$.unwrap_uvw.edit()
				$.unwrap_uvw.fitSelected()
			)
		)
	on btnMapsize3 pressed do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 3
				collapseStack $
			)
		)
	on btnMapsize3 rightClick do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 3
				modPanel.addModtoSelection( unwrap_uvw() )
				$.unwrap_uvw.edit()
				$.unwrap_uvw.fitSelected()
			)
		)
	on btnMapsize4 pressed do
	(	
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 4
				collapseStack $
			)
		)
	on btnMapsize4 rightClick do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 4
				modPanel.addModtoSelection( unwrap_uvw() )
				$.unwrap_uvw.edit()
				$.unwrap_uvw.fitSelected()
			)
		)
	on btnMapsize5 pressed do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 5
				collapseStack $
			)
		)
	on btnMapsize5 rightClick do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap 5
				modPanel.addModtoSelection( unwrap_uvw() )
				$.unwrap_uvw.edit()
				$.unwrap_uvw.fitSelected()
			)
		)
	on btnMapsizefit pressed do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmapfit()
				collapseStack $
			)
		)
	on btnMapsizefit rightClick do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmapfit()
				modPanel.addModtoSelection( unwrap_uvw() )
				$.unwrap_uvw.edit()
				$.unwrap_uvw.fitSelected()
			)
		)
	on btnMapsizecust pressed do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap spnMapsiz.value
				collapseStack $
			)
		)
	on btnMapsizecust rightClick do
	(
			if selection.count != 1 then (
				messagebox "Please select one object."
			)
			else (
				bungonboxmap cmapsiz.value
				modPanel.addModtoSelection( unwrap_uvw() )
				$.unwrap_uvw.edit()
				$.unwrap_uvw.fitSelected()
			)
		)
	on spnMapsiz changed val do
	(
		if selection.count != 1 then (
			messagebox "Please select one object."
		)
		else (
			bungonboxmap val
			collapseStack $
		)
	)
	on btnTexture pressed do
	(
		if selection.count != 1 then (
			messagebox "Please select one object."
		)
		else (
			modPanel.addModToSelection (Uvwmap ())
			$.modifiers[#UVW_Mapping].maptype = 4
			$.modifiers[#UVW_Mapping].utile = spnXRep.value
			$.modifiers[#UVW_Mapping].vtile =spnYRep.value
			macros.run "Modifier Stack" "Convert_to_Mesh"
			subobjectLevel = 4
		)
	)
	on btnFacemap pressed do
	(
		if selection.count != 1 then (
			messagebox "Please select one object."
		)
		else (
			modPanel.addModToSelection (Uvwmap ())
			$.modifiers[#UVW_Mapping].maptype = 5
			$.modifiers[#UVW_Mapping].utile = spnXRep.value
			$.modifiers[#UVW_Mapping].vtile =spnYRep.value
			macros.run "Modifier Stack" "Convert_to_Mesh"
			subobjectLevel = 4
		)
	)
	on spnMatID changed newmatid do
	(
		if selection.count != 1 then (
			messagebox "Please select one object."
		)
		else (
			modPanel.addModToSelection (Materialmodifier ())
			$.modifiers[#Material].materialID = newmatid
			macros.run "Modifier Stack" "Convert_to_Mesh"
			subobjectLevel = 4
		)
	)
	
	on RsUVMapRoll rolledUp down do 
	(
		RsSettingWrite "rsUVmapRoll" "rollup" (not down)
	)
)

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- OPTIMISATION
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout RsMatOptRoll "Optimisation"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Material_Toolkit#Optimisation" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	groupBox grpMulti "MultiSubs in Scene" pos:[6,20] width:131 height:107
	spinner spnMultiSubLimit "" pos:[26,51] width:91 height:16
	button btnCheckMultiSubCount "Check Count" pos:[29,78] width:85 height:29
	
	groupBox grpViewTXD "View TXD" pos:[142,20] width:131 height:107
	button btnViewITD "View ITD" pos:[163,40] width:85 height:24
	button btnViewIDR "View IDR" pos:[164,68] width:85 height:24
	checkbox chkUseStream "Use the stream" checked:true pos:[160,97] width:109 height:24
	
	groupBox grpComposite "Composite Specular Setup" pos:[6,128] width:267 height:61
	button btnMerge "Merge" pos:[21,148] width:73 height:29
	button btnList "List" pos:[104,148] width:73 height:29
	button btnRestore "Restore" pos:[186,148] width:73 height:29
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	-----------------------------------------------------------------------------
	-- MultiSubs in Scene
	-----------------------------------------------------------------------------
	on btnCheckMultiSubCount pressed do (
		global mswarningList = #()

		for obj in rootnode.children do (
			mapList = #()
			polyCount = #()

			if classof obj == Editable_mesh or classof obj == Editable_Poly then (
				RsMatGetMapIdsUsedOnObjectWithCount obj mapList polycount:polyCount
				multiSubLimit = spnMultiSubLimit.value as integer
				
				if mapList.count > multiSubLimit then (
					if obj.material != undefined then (
						warningstring = obj.name + " is using " + (mapList.count as string) + " unique submaterial IDs from it's material " + obj.material.name
					) else (
						warningstring = obj.name + " is using " + (mapList.count as string) + " unique submaterial IDs from it's material."
					)
					append mswarningList warningstring
				)
			)
		)

		rollout RsMapCritErrors "Number of Multisubs"
		(
			listbox lstMissing items:mswarningList
			button btnOK "OK" width:100 pos:[150,150]

			on btnOK pressed do (
				DestroyDialog RsMapCritErrors
			)	
		)
		CreateDialog RsMapCritErrors width:600 modal:true
	)
	
	-----------------------------------------------------------------------------
	-- View TXD
	-----------------------------------------------------------------------------
	on btnViewITD pressed do (
		ViewITD chkUseStream.checked
	)
	
	on btnViewIDR pressed do (
		ViewIDR chkUseStream.checked
	)
	
	-----------------------------------------------------------------------------
	-- Composite Specular Setup
	-----------------------------------------------------------------------------
	on btnMerge pressed do (
		DoFixup()
	)	
	
	on btnList pressed do (
		DoList()
	)

	on btnRestore pressed do (
		DoRemove()
	)
	
	on RsMatOptRoll rolledUp down do 
	(
		RsSettingWrite "rsMatOptRoll" "rollup" (not down)
	)
)

try CloseRolloutFloater MaterialToolkit catch()
MaterialToolkit = newRolloutFloater "Material Toolkit" 300 605 50 96
addRollout RsMatScriptRoll MaterialToolkit rolledup:(RsSettingsReadBoolean "rsMatScriptRoll" "rollup" false)
addRollout RsUVMapRoll MaterialToolkit rolledup:(RsSettingsReadBoolean "rsUVmapRoll" "rollup" false)
addRollout RsMatOptRoll MaterialToolkit rolledup:(RsSettingsReadBoolean "rsMatOptRoll" "rollup" false)
addRollout RsBillboardViewerRoll MaterialToolkit rolledup:(RsSettingsReadBoolean "rsBillboardViewerRoll" "rollup" false)