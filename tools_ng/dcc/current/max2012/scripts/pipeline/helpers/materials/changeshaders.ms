--
-- File:: pipeline/helpers/materials/changeshaders.ms
-- Description:: Change Shaders
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 17 March 2010
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/helpers/shaderchange.ms"
filein "rockstar/util/matmapset.ms"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
try CloseRolloutFloater ShaderChangeUtil catch()
ShaderChangeUtil = newRolloutFloater "Change Shaders" 320 400 50 126
addRollout ShaderChangeRoll ShaderChangeUtil 
addRollout RsMaterialMapSetRoll ShaderChangeUtil 