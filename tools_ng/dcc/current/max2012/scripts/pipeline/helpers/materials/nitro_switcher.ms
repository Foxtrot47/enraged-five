--
-- File:: pipeline/helpers/materials/nitro_switcher.ms
-- Description:: Replace Shaders
--
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 15 February 2012
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/util/material.ms"
filein "pipeline/util/nitro_switch_utils.ms"


-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsRageNitroMaterialSwitcherRoll "Nitro Lubrication" 
(
	local MapMats = #()
	local MapParentMats = #()
	local ObjectList = #()
	---------------------------------------------------------------------------
	-- UI
	---------------------------------------------------------------------------
	button btnRageToRageNitro  	"Lube: Rage To Rage Nitro"
	button btnRageNitroToRage  	"No Lube: Rage Nitro To Rage"
	
	---------------------------------------------------------------------------
	-- Events
	---------------------------------------------------------------------------
	on btnRageToRageNitro pressed do (
		if selection.count == 0 then (
			messagebox "Nothing selected"
		)
		else (
			RsGetRageShaderMatListForObjects selection &MapMats &MapParentMats objectList:ObjectList
			for i = 1 to mapMats.count do (
				ReplaceRageMatWithRageNitroMat i MapMats MapParentMats ObjectList
			)
			messagebox ("Conversion complete on " + (selection.count as string) + " objects")
		)
	)
	
	on btnRageNitroToRage pressed do (
		if selection.count == 0 then (
			messagebox "Nothing selected"
		)
		else (
			RsGetRageNitroShaderMatListForObjects selection &MapMats &MapParentMats objectList:ObjectList
			for i = 1 to mapMats.count do (
				ReplaceRageNitroMatWithRageMat i MapMats MapParentMats ObjectList
			)
			messagebox ("Conversion complete on " + (selection.count as string) + " objects")
		)
	)
)

try CloseRolloutFloater RageNitroMaterialSwitcher catch()
RageNitroMaterialSwitcher = newRolloutFloater "Rage Nitro" 320 100 50 126
addRollout RsRageNitroMaterialSwitcherRoll RageNitroMaterialSwitcher 