mtls = #()
searchFileName = undefined

fn CheckMtlForTexRec m bits: = 
(
	local searchInMtlName = true
	local searchInShaderName = true
	local searchIntexName = true
	if unsupplied!=bits then
	(
		searchInMtlName = bits[1]==true
		searchInShaderName = bits[2]==true
		searchIntexName = bits[3]==true
	)
	
	local pattern = "*"+searchFileName+"*"
	if undefined!=m and searchInMtlName and matchPattern m.name pattern:pattern then
		appendifunique mtls m

	case classof m of
	(
		multimaterial:
		(
			for submtl in m.materiallist do
			(
				if undefined!=submtl and searchInMtlName and matchPattern submtl.name pattern:pattern then
					appendifunique mtls submtl
				CheckMtlForTexRec submtl bits:bits
			)
		)
		Rage_shader:
		(
			if searchInShaderName and matchPattern (RstGetShaderName m) pattern:pattern then
				appendifunique mtls m
			
			if searchIntexName then
				for i=1 to RstGetVariableCount m 
					where "texmap"==RstGetVariableType m i and
					matchPattern (RstGetVariable m i) pattern:("*"+searchFileName+"*")
				do
					appendifunique mtls m
		)
	)
)

try( DestroyDialog SearchTexInSceneResultRoll )catch()
rollout SearchTexInSceneResultRoll "Search results"
(
	listbox resultsList "doubleclick to load into selected material editor slot and select objects with mtl applied" height:5
	button buttMultiMats "Find referencing multi materials" across:2
	button buttNewSearch "open new search"
	
	on SearchTexInSceneResultRoll open do
	(
		resultsList.items = for m in mtls collect m.name
	)
	on resultsList doubleclicked val do 
	(
		meditmaterials[activemeditslot] = mtls[val]
		clearSelection()
		for o in objects where o.material == mtls[val] do selectMore o
	)
	on buttMultiMats pressed do
	(
		if 0==SearchTexInSceneResultRoll.resultsList.selection then
		(
			messagebox "Nothing selected!"
			return false
		)
		
		local selMat = mtls[SearchTexInSceneResultRoll.resultsList.selection]
		if undefined==selMat then
			return false
		
		resultsList.items = #()
		mtls = #()
		for sm in sceneMaterials where 
			classof sm == multimaterial and 
			(0!=findItem sm.materiallist selMat) do 
		(
			append mtls sm
		)
		resultsList.items = for m in mtls collect m.name
	)
	
	on buttNewSearch pressed do
	(
		try( DestroyDialog SearchTexInSceneResultRoll )catch()
		::SearchTexInScene()
	)
)

fn SearchMtlsWithTexture tex bits: = 
(
	searchFileName = tex as string
	for sm in sceneMaterials do
	(
		CheckMtlForTexRec sm bits:bits
	)
	
	CreateDialog SearchTexInSceneResultRoll 500 120
)

try( DestroyDialog SearchTexInSceneRoll )catch()
rollout SearchTexInSceneRoll "Enter texture name"
(
	dotNetControl txtToSearch "MaxCustomControls.MaxTextBox" across:2 width:140 offset:[-10,0]
	button buttOK "Search" offset:[30,0]
	checkbox searchInMtlName "Search In Mtl Name" checked:true
	checkbox searchInShaderName "Search In Shader Preset Name" checked:true
	checkbox searchIntexName "Search In Tex Name" checked:true

	fn dosearch = 
	(
		if txtToSearch.text!="" then
		(
			SearchMtlsWithTexture txtToSearch.text bits:#(searchInMtlName.checked, searchInShaderName.checked, searchIntexName.checked)
			try( DestroyDialog SearchTexInSceneRoll )catch()
		)
	)
	on buttOK pressed do
	(
		dosearch()
	)
	
	on SearchTexInSceneRoll open do
	(
		txtToSearch.AcceptsReturn = true
		txtToSearch.focus()
	)
)

fn SearchTexInScene = 
(
	try( DestroyDialog SearchTexInSceneRoll )catch()
	CreateDialog SearchTexInSceneRoll 200 100 modal:true
)
