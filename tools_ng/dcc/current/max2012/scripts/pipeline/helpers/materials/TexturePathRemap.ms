--
-- File:: pipeline/helpers/materials/texturepathremap.ms
-- Description:: Utility plugin to remap texture paths. Replaces
--							string in original texture to new string
--
-- Author:: Adam Fowler <adam.fowler@rockstarnorth.com>
--

-----------------------------------------------------------------------------
-- History
-----------------------------------------------------------------------------
--
-- 11/2/10
-- Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- 	Adding support for Rage shaders
--
-- 15/10/13
-- Neal D Corbett <neal.corbett@rockstarleeds.com>
-- 	Rewritten to make use of new 'RsGetSceneTexMaps' function; 
--	Added banner; Proper resizing; double/rightclick functionality

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
try destroyDialog RsTexRemapRoll catch ()

rollout RsTexRemapRoll "Texture Path Remap" width:620 height:284
(
	local minWidth = 620
	local minHeight = 140
	
	local texMapList = #()
	local texPathList = #()
	local shownMapNums = #()
	
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:RsTexRemapRoll.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Texture_Path_Remap" versionNum:1.24 versionName:"Knotty Snow" filename:(getThisScriptFilename())
	
	local ctrlPadding = 4
	local btnWidth = 80
	local btnHeight = 22
	local txtWidth = 220
	
	label lblSortOrder "Sort by:" align:#left across:4 offset:[-4,1]
	radiobuttons rdoSortOrder labels:#("Path", "Filename") tooltip:#("Sort textures by full path", "Sort textures by just the texture filenames") align:#left pos:(lblSortOrder.pos + [42,0])
	
	button refreshBtn "Refresh" align:#left pos:[0, rsBannerPanel.height + 2] width:btnWidth height:btnHeight across:4
	
	MultiListBox textureList width:0 height:0 pos:[ctrlPadding, lblSortOrder.pos.y + 21]
	
	edittext findEdit "Find:" tooltip:"Filter list to show paths containing this text" fieldWidth:txtWidth pos:[8,0]
	edittext replaceEdit "Replace:" fieldWidth:txtWidth pos:[findEdit.pos.x + findEdit.width + 8, 0]
	
	button replaceAllBtn "Replace in All" tooltip:"Replace 'Find' text with this text,\nfor all items" align:#left pos:[replaceEdit.pos.x + replaceEdit.width + 9, 0] width:btnWidth height:btnHeight
	button replaceSelBtn "Replace in Sel" tooltip:"Replace 'Find' text with this text,\non selected items" align:#left pos:[replaceAllBtn.pos.x, 0] width:btnWidth height:btnHeight
	
	label lblFindOpts "Find/Replace on:" align:#left across:2 pos:[8,0]
	radiobuttons rdoFindOpts labels:#("Folders","Filename","Full Path") tooltip:#("Match 'Find' text to texture folders","Match 'Find' text to texture filenames","Match 'Find' text to full paths") columns:3 pos:(lblFindOpts.pos + [(getTextExtent(lblFindOpts.text)).x + 4,0])
	checkBox cbCopyTextures "Copy textures to new location." pos:(lblFindOpts.pos + [(getTextExtent(lblFindOpts.text)).x + 250, 0])
	
	-- Used to stop list-filter from running for every keypress:
	timer filterTimer "" active:False interval:500
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	-- Disable 'Replace' controls if text-fields are invalid:
	fn setCtrlsEnabled = 
	(
		local canReplace = (findEdit.text != "") and (textureList.Items.Count != 0)
		replaceAllBtn.enabled = canReplace
		replaceSelBtn.enabled = canReplace and (textureList.selection.numberSet != 0)
	)
	
	-- Arrange controls to fit window's current size:
	fn arrangeCtrls winSize:[RsTexRemapRoll.width, RsTexRemapRoll.height] = 
	(
		-- Enforce minimum dimensions:
		if (winSize.x < minWidth) do 
		(
			winSize.x = minWidth
			RsTexRemapRoll.width = minWidth
		)
		if (winSize.y < minHeight) do 
		(
			winSize.y = minHeight
			RsTexRemapRoll.height = minHeight
		)
		
		rsBannerPanel.width = winSize.x
		replaceSelBtn.pos.X = replaceAllBtn.pos.X = refreshBtn.pos.X = (winSize.x - btnWidth - ctrlPadding + 1)
		
		local btnY = winSize.y - btnHeight - ctrlPadding - 24
		replaceAllBtn.pos.Y = btnY
		replaceSelBtn.pos.Y = btnY + btnHeight + 2
		
		replaceEdit.pos.Y = findEdit.pos.Y = btnY + 2
		rdoFindOpts.pos.Y = lblFindOpts.pos.Y = btnY + btnHeight + 5
		cbCopyTextures.pos.Y = lblFindOpts.pos.Y = btnY + btnHeight + 5
		
		textureList.width = winSize.x - (2 * ctrlPadding)
		textureList.height = btnY - textureList.pos.y - ctrlPadding
	)
	
	-- Generate list to show on rollout, and list of indexes those names refers to on 'texPathList'
	-- 	Only show textures with the required string, and sort as required
	fn FilterTextureList findText:findEdit.text = 
	(
		-- Deactivate deferred-filter timer, if active:
		filterTimer.Active = False
		
		-- Collect list of currently-selected texmaps:
		local selTexMaps = for listNum in textureList.selection collect texMapList[shownMapNums[listNum]]
		
		local newMapNums = #()
		
		if (findText == "") then 
		(
			newMapNums = for n = 1 to texPathList.count collect n
		)
		else 
		(
			local newTextureList = #()
			local newFilenameList = #()
			
			local findPatt = ("*" + findText + "*")
			local findOption = rdoFindOpts.state
			
			-- create new list of textures
			for i = 1 to texPathList.count do 
			(
				local searchName = texPathList[i]
				case findOption of 
				(
					1:(searchName = getFilenamePath searchName)
					2:(searchName = filenameFromPath searchName)
				)
				
				if (matchPattern searchName pattern:findPatt) do 
				(
					append newMapNums i
				)
			)
		)
		
		-- Sort items by path/name:
		local sortType = rdoSortOrder.state
		local sortItems = for idx in newMapNums collect 
		(
			local itemPath = toLower texPathList[idx]
			if (sortType == 2) do 
			(
				itemPath = filenameFromPath itemPath
			)
			
			dataPair idx:idx name:itemPath
		)
		
		fn texPathSort v1 v2 = (striCmp v1.name v2.name)
		qsort sortItems texPathSort
		
		-- Set up textureList control's list, and its lookup array:
		shownMapNums = (for item in sortItems collect item.idx)
		textureList.items = (for idx in shownMapNums collect texPathList[idx])
		
		-- Revert selection:
		local setSel = #{}
		for texMap in selTexMaps do 
		(
			local texNum = findItem texMapList texMap
			
			if (texNum != 0) do 
			(
				local listNum = findItem shownMapNums texNum
				
				if (listNum != 0) do 
				(
					setSel[listNum] = True
				)
			)
		)
		textureList.selection = setSel
		
		-- Set enabled-values whenever this is called:
		setCtrlsEnabled()
	)
	
	fn ConstructTextureList = 
	(
		texMapList = RsGetSceneTexMaps()		
		texPathList = for item in texMapList collect item.filename
		
		FilterTextureList()
	)
	
	-- Replace string in current list of files
	fn ReplaceTextureList replaceAll:True = 
	(
		local findText = (toLower findEdit.text)
		local replaceText = replaceEdit.text
		
		local findTextLength = findText.count
		
		-- Abort if 'Find' field is empty:
		if (findTextLength == 0) do 
		(
			MessageBox "Please enter some text in the 'Find:' field" title:"Texture Path Remap"
			return false
		)
		
		-- Check for invalid characters in replaceText:
		(
			local goodChars=":\\/ .-_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
			local badChars = #()
			for i = 1 to replaceText.count do 
			(
				local thisChar = replaceText[i]
				local goodNum = findString goodChars thisChar
				
				if (goodNum == undefined) do 
				(
					appendIfUnique badChars thisChar
				)
			)
			
			if (badChars.count != 0) do 
			(
				local msg = stringStream ""
				
				local plural = if (badChars.count == 1) then "" else "s"
				format "Error: 'Replace' text contains invalid character%:\n\n" plural to:msg
				
				for thisChar in badChars do 
				(
					format "'%' " thisChar to:msg
				)
				
				format "\n\nAborting replace!" to:msg
				
				messageBox (msg as string) title:"Texture Path Remap"
				return false
			)			
		)
		
		-- Check replace-text for spaces:
		if (matchPattern replaceText pattern:"* *") do
		(
			local msg = stringStream ""
			format "Warning: 'Replace' text contains a space:\n\n\"%\"\n\nContinue with replace?" replaceText to:msg
			
			if (not queryBox (msg as string) title:"Texture Path Remap") do 
			(
				return false
			)
		)
		
		-- Ensure that trailing slashes match up:
		--	Otherwise you could accidentally remove a slash from your scene's texturenames, which can be very messy to fix!
		(
			local LastFindChar, LastReplaceChar
			if (FindText != "") do (LastFindChar = FindText[FindText.Count])
			if (ReplaceText != "") do (LastReplaceChar = ReplaceText[ReplaceText.Count])
			
			if ((LastFindChar == "\\") or (LastFindChar == "/")) and ((LastReplaceChar != "\\") and (LastReplaceChar != "/")) do 
			(
				local msg = stringStream ""
				format "Warning: 'Find' text has a trailing slash that isn't matched in the 'Replace' text - this can make a mess of your scene!\n\nContinue with replace?" to:msg
				
				if (not queryBox (msg as string) title:"Texture Path Remap") do 
				(
					return false
				)
			)
		)
		
		local findOption = rdoFindOpts.state
		
		for listIdx = 1 to shownMapNums.count where (replaceAll or textureList.selection[listIdx]) do 
		(
			local texNum = shownMapNums[listIdx]
			local texPath = texPathList[texNum]
			
			local newPath = ""
			local searchThis = ""
			
			case findOption of 
			(
				1:(searchThis = getFilenamePath texPath)
				2:(searchThis = filenameFromPath texPath)
				3:(searchThis = texPath)
			)
			
			-- Run through search-string replacing found bits, until there's no more string left:
			local strPos = findString (toLower searchThis) findText
			while (strPos != undefined) do 
			(
				append newPath (substring searchThis 1 (strPos - 1))
				append newPath (replaceText)
				
				searchThis = substring searchThis (strPos + findTextLength) -1
				strPos = findString (toLower searchThis) findText
			)
			append newPath searchThis
			
			case findOption of 
			(
				1:(newPath = newPath + "\\" + (filenameFromPath texPath))
				2:(newPath = (getFilenamePath texPath) + newPath)
			)
			
			-- Avoid adding double slashes:
			newPath = RsMakeBackSlashes newPath
			
			texMapList[texNum].filename = newPath
			texPathList[texNum] = newPath
			
			if (cbCopyTextures.checked) do 
			(
				--break path into tokens
				local texPathTokens = filterString texPath "\\"
				local newPathTokens = filterString newPath "\\"
				
				--add double slashes between tokens, create full directory
				local newPathDir = ""
				for i = 1 to (newPathTokens.count - 1) do append newPathDir (newPathTokens[i] + "\\\\")
 				makeDir newPathDir all:true
				
				--copyFile likes double slashes
 				local dblSlashNewPath = newPathDir + newPathTokens[newPathTokens.count]
 				local dblSlashTexPath = ""
 				for i = 1 to (texPathTokens.count - 1) do append dblSlashTexPath (texPathTokens[i] + "\\\\")
 				dblSlashTexPath = dblSlashTexPath + texPathTokens[texPathTokens.count]
 				
 				copyFile dblSlashTexPath dblSlashNewPath
			)
		)
		
		textureList.items = (for idx in shownMapNums collect texPathList[idx])
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	on refreshBtn pressed do 
	(
		textureList.items = #()
		ConstructTextureList()
	)
	
	on rdoSortOrder changed state do 
	(
		FilterTextureList()
	)
	on rdoFindOpts changed state do 
	(
		FilterTextureList()
	)
	
	-- Run filter-function if findEdit hasn't been changed during timer's interval:
	on filterTimer tick do 
	(
		filterTimer.Active = False
		FilterTextureList()
	)
	-- Start filtertimer when find-text is changed:
	on findEdit changed newText do 
	(
		filterTimer.Active = True
	)
	
	-- Ensure that replace-text is file-safe:
	on replaceEdit changed newText do 
	(
		setCtrlsEnabled()
	)
	
	-- Replace Find text with Replace:
	on replaceAllBtn pressed do 
	(
		ReplaceTextureList replaceAll:True
	)
	on replaceSelBtn pressed do 
	(
		ReplaceTextureList replaceAll:False
	)
	
	-- Arrange controls to fit window's new size:
	on RsTexRemapRoll resized newSize do 
	(
		arrangeCtrls winSize:newSize
	)
	
	-- Show texturemap for selected path in Material Editor:
	fn showSelInEditor = 
	(
		setCtrlsEnabled()
		
		local selIdx = (textureList.selection as array)[1]
		if (selIdx == undefined) do return False
		
		local texNum = shownMapNums[selIdx]
		local texMap = texMapList[texNum]
		
		-- If texturemap is still valid, show it in editor:
		if (isKindOf texMap textureMap) do 
		(
			-- Open new material-editor (closing first, as existing one can't be focused directly)
			-- 	(also sets to non-Slate mode, as I don't think Slate can be controlled from script)
			MatEditor.Close()
			MatEditor.mode = #basic
			MatEditor.Open()

			local matEditSlot = medit.GetActiveMtlSlot()
			medit.PutMtlToMtlEditor texMap matEditSlot
		)
	)

	on textureList selected idx do 
	(	
		setCtrlsEnabled()
	)
	
	-- Show texturemap in Material Editor on doubleclick:
	on textureList doubleClicked idx do 
	(
		showSelInEditor()
	)
	
	-- Show rightclick-menu:
	on textureList rightClick idx do 
	(
		if (idx == 0) do return false
		
		-- Set selection to idx if it isn't currently selected:
		if (not textureList.selection[idx]) do 
		(
			textureList.selection = idx
		)
		
		setCtrlsEnabled()
		
		rcMenu RSmenu_TexRemap
		(
			fn singleSel = (RsTexRemapRoll.textureList.selection.numberSet == 1)
			
			menuItem itmCopy "Copy Selected to Clipboard"
			menuItem itmShow "Show Map in Material Editor" filter:singleSel
			
			on itmCopy picked do 
			(
				local lstCtrl = RsTexRemapRoll.textureList
				local selItems = for n = lstCtrl.selection collect lstCtrl.items[n]
				
				local txt = stringStream ""
				local startChar = ""
				
				for item in selItems do 
				(
					format "%%" startChar item to:txt
					startChar = "\n"
				)				
				
				setclipboardText (txt as string)
			)
			on itmShow picked do 
			(
				RsTexRemapRoll.showSelInEditor()
			)
		)
		
		popUpMenu RSmenu_TexRemap
	)
	
	on RsTexRemapRoll open do 
	(
		-- Arrange controls to fit window's current size:
		arrangeCtrls()
		banner.setup()
		
		ConstructTextureList()
	)
)

createDialog RsTexRemapRoll style:#(#style_titlebar, #style_resizing, #style_sysmenu, #style_minimizebox, #style_maximizebox)
