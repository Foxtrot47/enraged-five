--
-- File:: pipeline/helpers/materials/texturepalette.ms
-- Description::  Palette for showing and applying textures in a scene.
--
-- 21/1/10
-- by Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
include "rockstar/util/material.ms"
include "rockstar/export/settings.ms"
include "pipeline/util/string.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
-- Hack to get around the rolloutCreator limitations
global TexTemps = #()
global TexTooltips = #()

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Rollouts for different categories of textures
-----------------------------------------------------------------------------
rollout RsGlobalTextures "Free Textures (Globals)"
(
	imgTag BitmapImage bitmap:(bitmap 50 50 color:blue)
)

rollout RsParentTextures "Cheap Textures (Parented to this TXD)"
(
	imgTag BitmapImage bitmap:(bitmap 50 50 color:blue)
)

rollout RsCurrentTextures "Current TXD (Already using these)"
(
	imgTag BitmapImage bitmap:(bitmap 50 50 color:blue)
)

rollout RsCurrentArea "Current Area - try to use these"
(
	imgTag BitmapImage bitmap:(bitmap 50 50 color:blue)
)

-----------------------------------------------------------------------------
-- Texture Palette (main rollout)
-----------------------------------------------------------------------------
rollout RsTexPalette "Texture Palette"
(
	--////////////////////////////////////////////////////////////
	-- local variables
	--////////////////////////////////////////////////////////////
	local CurrentObj
	local GlobalTXDs = #()
	local ParentTXDs = #()
	local CurrentTXDs = #()
	local AreaTXDs = #()
	
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Texture_Palette" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	groupBox grpType "Type" pos:[5,20] width:210 height:41
	radioButtons radTexType pos:[9,35] width:248 height:16 labels:#("normal", "spec", "diffuse", "alpha")
	button btnRefresh "Refresh" pos:[220,26] width:100 height:35
	
	button btnSelectObjects "Select Objects" pos:[5,70] width:150 height:20
	button btnSelectPolys "Select Polys" pos:[170,70] width:150 height:20
	
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	----------------------------------------------------------------------------------------
	-- Get a list of all the parent textures
	----------------------------------------------------------------------------------------
	fn GetParentTex filepath txdname = (
		-- Open and parse the parent file
		local fileClass = dotNetClass "System.IO.File"
		local lines = #()
		
		lines = fileClass.ReadAllLines(filepath)
		found = false
		parent = ""
		
		-- Iterate through all the lines looking for the parent
		for thisLine in lines do (
			if found = true then (
				-- If we have found the parent then record its name and break out
				parentline = findString thisLine "parent"
				
				if parentline != undefined then (
					words = filterString thisLine " "
					parent = words[3]
					exit
				)
			)
			else (
				-- If we haven't found the parent, keep looking
				nameline = findString thisLine "name"
				
				if nameline != undefined then (
					words = filterString thisLine " "
					if words[3] == txdname then found = true
				)
			) --if found = true
		)  --for thisLine in lines do
		
		parentTxds = #()
		-- GET THE OTHER TXDS AND COLLECT TEXTURE NAMES
	)
	
	----------------------------------------------------------------------------------------
	-- Regenerate a rollout with a given list of textures
	----------------------------------------------------------------------------------------
	fn RegenTexRollout roll txdlist = (
		-- Remove the old rollout
		removeRollout roll
		
		-- Get the memory usage
		memusage = 0
		
		-- Start generating the new rollout
		currRoll = rolloutCreator roll.name (roll.title)
		currRoll.begin()
		TexTemps = #()
		TexTooltips = #()
		
		-- Create images
		for i=1 to txdlist.count do (
			print txdlist[i].filename
			
			-- Open and store the bitmap
			append TexTemps (openBitMap txdlist[i].filename)
			
			-- Construct the tooltip
			texmem = (GetFileSize txdlist[i].filename)/1024
			memusage = memusage + texmem
			append TexTooltips ((txdlist[i].filename) + " " + (texmem as string) + "k")
			
			-- Create the parameter string (if this is the first control in a row, also specify the layout)
			if (mod i 4) == 1 then (
				imgParamStr = "width:64 height:64 across:4 bitmap:TexTemps[" + (i as string) + "] tooltip:TexTooltips[" + (i as string) + "]"
			)
			else (
				imgParamStr = "width:64 height:64 bitmap:TexTemps[" + (i as string) + "] tooltip:TexTooltips[" + (i as string) + "]"
			)
			print imgParamStr
			
			-- Create the control
			imgName = "Tex" + (i as string)
			currRoll.addControl #imgTag imgName "caption" paramStr:imgParamStr
			
		) -- for i=1 to txdlist.count
		currRoll.end()
		
		newRoll = currRoll.def
		addRollout newRoll RsTexturePaletteRollout
		newRoll.title = newRoll.title + " " + (memusage as string) + "k"
	)
	
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	on btnRefresh pressed do (
		-- Set the selection
		sel = selection as array
		if sel.count != 1 then (
			messagebox "Please select only one object."
		)
		else (
			CurrentObj = sel[1]
			
			-- Refresh the current TXDs:
			RsGetMainTexMapsFromMaterial CurrentObj CurrentObj.material CurrentTXDs
			RegenTexRollout RsCurrentTextures CurrentTXDs
			
			-- Refresh the parent TXDs:
			-- Get the TXD parent file
			mapname = RsLowercase( (filterstring maxfilename ".")[1] )
			parentFilePath = ( RsConfigGetContentDir() + "txdparents/" + mapname + ".txt" )
			if not (doesFileExist parentFilePath) then (
				messagebox "No TXD parent .txt file found for this object"
			)
			else (
				-- If the file exists, get the texture names from it
				txdname = getattr $ (getattrindex "Gta Object" "TXD")
				ParentTXDs = GetParentTex parentFilePath txdname
			)
		)  -- if sel.count != 1
	)
	
)

try CloseRolloutFloater RsTexturePaletteRollout catch()
RsTexturePaletteRollout = newRolloutFloater "Texture Palette" 415 500 50 126
addRollout RsTexPalette RsTexturePaletteRollout
addRollout RsGlobalTextures RsTexturePaletteRollout
addRollout RsParentTextures RsTexturePaletteRollout
addRollout RsCurrentTextures RsTexturePaletteRollout
addRollout RsCurrentArea RsTexturePaletteRollout

