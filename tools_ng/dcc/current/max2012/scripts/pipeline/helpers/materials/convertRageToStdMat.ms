

struct RsConvertRageToStdMat
(
	mapParentMats = #(),
	mapMats	= #(),
	idxStdMatDiffuseMap = 2, -- See MaxScript docs
	idxStdMatSpecMap = 3, -- See MaxScript docs
	idxStdMatBumpMap = 9, -- See MaxScript docs
	
	fn GetAvailMats mats = (

		matNames = #()
		for m in mapMats do
			append matNames m.name
		matNames
	),

	-- Return index of subMat in parentMat.materialList
	fn GetParentIndex parentMat subMat = (
	
		if undefined == parentMat then
			return undefined
	
		for i = 1 to parentMat.materialList.count do
		(
			if ( parentMat.materialList[i] == subMat ) then
				return i
		)
		
		return undefined
	),

	-- Return variable index of Diffuse Texture Parameter
	fn GetRageShaderTextureIndex mat textype = (
	
		local varCount = RstGetVariableCount mat
		
		for i = 1 to varCount do
		(
			local varName = ( RstGetVariableName mat i )
			print varName
			if ( varName == textype ) then return i
		)
				
		return undefined
	),
	
	-- Return whether Rage Shader material is emissive
	fn IsRageShaderEmissive mat = (
	
		local shaderName = RstGetShaderName mat
		return ( undefined != ( findString shaderName "emissive" ) )
	),
	
	fn GetRageShaderEmissiveHDRMultiIndex mat = (
			
		local varCount = RstGetVariableCount mat
		for i = 1 to varCount do
		(

			if ( "Emissive HDR Multiplier" == RstGetVariableName mat i ) then
			(
				return i
			)
		)
			
		return undefined
	),
	
	fn ConvertHDRMultiplier x = (
	
		local f = ( 27.0 * sqrt( x ) )
		-- Clamp
		if ( f < 0.0 ) then f = 0.0
		else if ( f > 100.0 ) then f = 100.0
		
		return ( f )
	),

	fn ReplaceShader idx prompteach = (
		local parentMat = mapParentMats[idx]
		if ( undefined == parentMat ) then (
			messagebox "Invaid parent material"
			print mapParentMats
			return false
		)

		local toReplace = mapMats[idx]
		local toReplaceName = toReplace.name
		
		idxParent = ( GetParentIndex parentMat toReplace )		
		if ( undefined == idxParent ) then
		(
			MessageBox "Error reading parent material.  Contact tools."
			return false
		)		
		
		if ( prompteach ) then
			if ( not QueryBox ( "Are you sure you want to replace " + toReplaceName + " with a Standard Material?" ) ) then
				return false
		
		-- Setup Standard Material
		local isAlpha = ( RstGetIsAlphaShader toReplace )
		local idxDiffMap = ( GetRageShaderTextureIndex toReplace  "Diffuse Texture")
		local idxSpecMap = ( GetRageShaderTextureIndex toReplace  "Specular Texture")
		local idxBumpMap = ( GetRageShaderTextureIndex toReplace  "Bump Texture")
		
		-- If the Rage Shader does not have a defined Diffuse Texture variable then don't convert
		if ( undefined == idxDiffMap ) then
			return false
		
		local diffMap = undefined
		local specMap = undefined
		local bumpMap = undefined
		
		if idxDiffMap != undefined then diffMap = ( RstGetVariable toReplace idxDiffMap )
		if idxSpecMap != undefined then specMap = ( RstGetVariable toReplace idxSpecMap )
		if idxBumpMap != undefined then bumpMap = ( RstGetVariable toReplace idxBumpMap )
		
		local idxAlphaMap = idxDiffMap + ( ( getNumSubTexmaps toReplace ) / 2 )
		local isEmissive = ( IsRageShaderEmissive toReplace )
		local idxEmissiveHDRMulti = ( GetRageShaderEmissiveHDRMultiIndex toReplace )
		
		format "Parent Index: %\n" idxParent
		format "Is Material % Alpha: %\n" toReplaceName isAlpha
		format "Is Material % Emissive: %\n" toReplaceName isEmissive
		format "Index of Diffuse Map: %\n" idxDiffMap
		format "Index of Alpha Map: %\n" idxAlphaMap
		format "Diffuse Map: %\n" diffMap
		
		/*
		if ( isAlpha and chkEnableAlpha.checked ) then
			format "Alpha Map: %\n" ( getSubTexMap toReplace idxAlphaMap )
		if ( isEmissive and chkEnableEmissive.checked ) then
			format "Emissive HDR Multiplier: %\n" ( RstGetVariable toReplace idxEmissiveHDRMulti )
		*/
		
		-- Create Standard Material
		stdMat = StandardMaterial()
		stdMat.name = "StdMat_" + toReplaceName

		-- Diffuse Map
		if diffMap != undefined then (
			stdMatDiffMap = BitmapTexture()
			stdMatDiffMap.filename = diffMap
			stdMat.maps[idxStdMatDiffuseMap] = stdMatDiffMap
		)
		
		-- Spec Map
		if specMap != undefined then (
			stdMatSpecMap = BitmapTexture()
			stdMatSpecMap.filename = specMap
			stdMat.maps[idxStdMatSpecMap] = stdMatSpecMap
		)
		
		-- Bump map
		if bumpMap != undefined then (
			stdMatBumpMap = BitmapTexture()
			stdMatBumpMap.filename = bumpMap
			stdMat.maps[idxStdMatBumpMap] = stdMatBumpMap
		)
		
		/*
		-- Alpha Map (if enabled)
		if ( isAlpha and chkEnableAlpha.checked ) then
		(
			alphaTexMap = ( getSubTexMap toReplace idxAlphaMap )
			
			if ( undefined != alphaTexMap ) then
			(
				alphMap = ( getSubTexMap toReplace idxAlphaMap ).filename
				if ( undefined != alphMap ) then
				(
					stdMatAlphaMap = BitmapTexture()
					stdMatAlphaMap.filename = alphMap

					-- Set alpha map
					stdMat.opacityMap = stdMatAlphaMap
					stdMat.opacityMapEnable = true
				)
			)
		)
		
		-- Emissive HDR Multiplier (if enabled)
		if ( isEmissive and chkEnableEmissive.checked ) then
		(
			hdrMultiplier = ( RstGetVariable toReplace idxEmissiveHDRMulti )
			hdrMultiplierConv = ConvertHDRMultiplier hdrMultiplier
			
			format "HDR Multiplier %: % self-illumination amount\n" hdrMultiplier hdrMultiplierConv
			stdMat.selfIllumAmount = hdrMultiplierConv
		)
		*/

		-- Set material		
		format "Replacing parent mat: % [%] with : %\n" parentMat.name idxParent stdMat.name
		mapParentMats[idx].materialList[idxParent] = stdMat
		
	),
	
	fn convertObject obj = (
		RsGetRageShaderMatListForObjects obj &mapMats &mapParentMats
		
		local numMats = mapMats.count
	
		for i = 1 to numMats do (
			ReplaceShader i false
		)
	)
)