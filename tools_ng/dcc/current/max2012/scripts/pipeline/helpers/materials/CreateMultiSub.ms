--
-- File:: pipeline/helpers/materials/CreateMultiSub.ms
-- Description::  Functions for working with multisubs
--
-- 29/10/2003
-- by Greg Smith <greg.smith@rockstarnorth.com>
--
-----------------------------------------------------------------------------

--filein "pipeline/export/maps/objects.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
-- RsAddNewSubMat:
--	Finds submat in newSubMatList, or adds it to list if new.
--	Returns index of added/found submaterial.
-------------------------------------------------------------------------------------------------------
fn RSaddNewSubMat subMat &newSubMatList silent:true uniqueMats:true = 
(
	local outNum
	local isColsubMat = (isKindOf subMat RexBoundMtl)
	
	for matNum = 1 to newSubMatList.count while (outNum == undefined) do 
	(
		if rexAreMatsEqual subMat newSubMatList[matNum] do
		(
			outNum = matNum
		)
	)
	
	--check for any uv animation
	local hasUVAnim = false
	for idx = 1 to subMat.numSubs while (not hasUVAnim) do
	(
		local animProps = getSubAnim subMat idx
		
		if (hasProperty animProps #coords) do
		(
			local coordsProp = getProperty animProps #coords
			if (coordsProp.U_Offset.isAnimated) or (coordsProp.V_Offset.isAnimated) then
			(
				outNum = undefined
				hasUVAnim = true
			)
		)
	)
	
	if outNum == undefined do 
	(
		if isColsubMat do 
		(
			subMat.name = (RexGetCollisionName subMat)
		)
		
		-- Add new materials to end of list
		local addMat = if uniqueMats then (copy subMat) else subMat
		append newSubMatList addMat
		outNum = newSubMatList.count
		
		local subMatName = if (subMat == undefined) then "Undefined material" else ("\"" + subMat.name + "\"")
		
		if not silent do (format "\t(%)\t%\n" outNum subMatName)
	)

	outNum
)

-------------------------------------------------------------------------------------------------------
-- RsCreateNewMultiSubForObjs:
--	Creates an optimised multimaterial for a set of objects
-------------------------------------------------------------------------------------------------------
fn RScreateNewMultiSubForObjs createset prefix:"" showProgress:true startProgress:true objNumStart:0 objCount:undefined silent:true hwMode: uniqueMats:true = 
(
	-- Filter out zero-face/non-materialed meshes:
	createset = for obj in createset where (obj.numfaces != 0) and (obj.material != undefined) collect obj
	
	local newSubMatList = #()
	local notCancelled = true
	
	local matEditWasOpen = MatEditor.isOpen()
	MatEditor.Close()
	
	-- Turning off "startProgress" allows external functions to start their own progressBars instead
	if showProgress and startProgress do (progressStart "Creating new multiSub:")
	
	if not silent do (format "Adding materials: %\n" prefix)

	local objsDone = #()
	local objNum = objNumStart
	if objCount == undefined do (objCount = createset.count)
	
	local objNames = #()
	
	local ProgressPerObj = (100.0 / ObjCount)

	for obj in createset while (not showProgress) or (notCancelled and (notCancelled = progressUpdate (ProgressPerObj * ObjNum))) do 
	(
		local ObjProgress = (ProgressPerObj * ObjNum)
		ObjNum += 1
		
		append objNames obj.name
		
		-- Get appropriate matid get/set command for object:
		local ObjGetFaceMatID = (RsGetFaceMatIDFunc Obj)
		local ObjSetFaceMatIDs = (RsSetFaceMatIDsFunc Obj)
		local ObjOp = (RsMeshPolyOp Obj)
		
		-- Clear dead structs from poly-object:
		if (ObjOp == PolyOp) do 
		(
			polyOp.collapseDeadStructs obj
		)
		
		-- Get number of faces:
		local numFaces = (getNumFaces obj)
		
		-- Amount of progress per object-face, for the getting/setting processes:
		local ProgPerFace = (ProgressPerObj / NumFaces)
		
		local ObjMat = Obj.Material 
		local isMultiMat = (isKindOf ObjMat Multimaterial)
		local MatCount = if isMultiMat then ObjMat.NumSubs else 1
	
		-- collect list of the object's faces' matIDs, corrected
		if isMultiMat then 
		(
			-- MatIds used by object:
			local UsedMatIDs = #{}
			-- Face used by each used MatId:
			local MatIDfaces = #()
			
			-- Collect faces per MatId:
			for faceNum = 1 to numFaces while (not showProgress) or (notCancelled = progressUpdate (ObjProgress += ProgPerFace)) do 
			(
				local MatNum = objGetFaceMatID obj faceNum
				
				-- Deal with matIDs that are higher than the material-count:
				if (matNum > matCount) do 
				(
					matNum = (mod matNum matCount) as integer
					if (matNum == 0) do (matNum = matCount)
				)
				
				-- Add bitarray for tracking matid's faces:
				if (not usedMatIDs[matNum]) do 
				(
					matIDfaces[matNum] = #{}
					matIDfaces[matNum].Count = NumFaces
					
					usedMatIDs[matNum] = True
				)
				
				-- Add to this matID's face-list:
				matIDfaces[matNum][faceNum] = true
			)
			
			-- Add submaterials to multimaterial, and apply new matIds to faces:
			if (notCancelled) do 
			(
				for matNum = usedMatIDs while (notCancelled) do 
				(					
					-- Find the matID on the material's ID-list, as this may have been reordered
					local oldMatListNum = findItem ObjMat.MaterialIDList matNum
					
					local useMat = if (oldMatListNum == 0) then undefined else ObjMat.MaterialList[oldMatListNum]

					if (not isKindOf useMat UndefinedClass) then --this should catch a crash we found in bug 758779
					(
						-- Gets new MatId for this material, adding it to list if new:
						local NewMatNum = RSaddNewSubMat useMat &newSubMatList silent:silent uniqueMats:uniqueMats
						
						-- Set the faces for this matID to this new matID number
						ObjSetFaceMatIDs Obj MatIDfaces[MatNum] NewMatNum
					)
					else
					(
						messageText = "There is a rogue shader on " + obj.name + ".\r\nMaterial " + matNum as string + ", subMaterial " + matNum as string + "."
						messageBox messageText
					)
				)
			)
		)
		else 
		(
			-- Single-material objects are simpler to process than multimaterials: all faces will all be given the same MatId:
			local newMatNum = RSaddNewSubMat ObjMat &newSubMatList silent:silent uniqueMats:uniqueMats
			ObjSetFaceMatIDs Obj #All NewMatNum
		)
		
		append objsDone obj
	)
	
	-- Generate name for newly-combined material:
	if (prefix != "") do (prefix = prefix + ":")
	local objName = if (objNames.count == 1) then objNames[1] else ("#" + ((getHashValue objNames 1) as string))
	local newMatName = (prefix + objName + " - Multimaterial")
	
	-- Create the new material:
	local newmat = (Multimaterial name:newMatName)
	
	newmat.numsubs = newSubMatList.count
	newmat.materialList = newSubMatList

	local rageMats = for mat in newSubMatList where isKindOf mat rage_shader collect mat
	
	-- Dereference argument if required:
	local refVals = isKindOf hwMode ValueRef
	local objHwMode = if refVals then *hwMode else hwMode
		
	local prevHwMode
	
	-- Check to see that the created multimaterial only uses one hardware-render type:
	for mat in rageMats while (objHwMode == unsupplied) do 
	(
		local matHwMode = RstGetHwRenderMode mat

		case of 
		(
			(prevHwMode == undefined):(prevHwMode = matHwMode)
			(prevHwMode != matHwMode):
			(
				objHwMode = RsQueryBoxTimeOut "Object-materials use multiple hardware-modes.\n\nWhich do you want them to use?" title:"Warning: Multiple Material-Render Modes" timeout:-1 yesText:"DX Rage" noText:"Rage"
			)
		)
	)
	
	if (objHwMode != unsupplied) do 
	(
		RsSetHardwareRendering objHwMode mats:rageMats
	)
	
	-- Update reference-value:
	if refVals do 
	(
		*hwMode = objHwMode
	)
	
	-- Apply new material to objects that have been processed
	-- (some won't have been processed if cancelled partway)
	objsDone.material = newmat
	
	if showProgress and startProgress do (progressEnd())
	if matEditWasOpen do MatEditor.Open()
	
	notCancelled
)

-------------------------------------------------------------------------------------------------------
-- RsMultiSubObjCont:
--	Checks objects to make sure that they're all in the same container, or are all orphans
--	Returns false if failed, otherwise the objects' container (or undefined, if none)
-------------------------------------------------------------------------------------------------------
fn RSmultiSubObjCont objs = 
(
	local ObjConts = #()
	
	-- Collect Objects' different containers, until more than one is found:
	for Obj in Objs while (ObjConts.Count < 2) do 
	(
		local ObjCont = RsGetObjContainer Obj
		appendIfUnique ObjConts ObjCont
	)
	
	local retVal
	
	if (ObjConts.Count < 2) then 
	(
		-- Return container/undefined:
		retVal = ObjConts[1]
	)
	else 
	(
		-- Return False if objects are in multiple containers:
		retVal = False
		
		messagebox "Unable to create multisub over multiple containers" Title:"Error: Objects in Multiple Containers"
	)

	return retVal
)

-----------------------------------------------------------------------------------------------------------
-- RsModifierObjsCheck:
--	Returns False if any specified object has modifiers, and the user doesn't want to continue
-----------------------------------------------------------------------------------------------------------
fn RsModifierObjsCheck objs = 
(
	local modifierObjs = for obj in objs where (obj.modifiers.count != 0) collect obj
	
	if (modifierObjs.count == 0) do return True
	
	local msg = stringStream ""
	local plural = if (modifierObjs.count == 1) then " has" else "s have"
	
	format "% object% modifiers, and will be ignored by material-combiner:\n\n" modifierObjs.count plural to:msg
	
	for obj in modifierObjs do 
	(
		format "%\n" obj.name to:msg
	)
	
	local validObjsCount = (objs.count - modifierObjs.count)
	local retVal = True
	
	if (validObjsCount == 0) then 
	(
		format "\nNo non-modifier objects are selected: nothing to combine!" to:msg
		messageBox (msg as string) title:"Warning: No valid objects"
		retVal = False
	)
	else 
	(
		plural = if (validObjsCount == 1) then "" else "s"
		format "\nContinue with remaining % object%?" validObjsCount plural to:msg
		retVal = queryBox (msg as string) title:"Warning: Objects with Modifiers found"
	)
	
	return retVal
)

-----------------------------------------------------------------------------------------------------------
-- RsCreateNewMultiSub:
--	Create an optimised multimaterial, combined from a given set of objects (with checks)
-----------------------------------------------------------------------------------------------------------
fn RScreateNewMultiSub objs:(getCurrentSelection()) showProgress:true silent:true uniqueMats:true = 
(
	if (not RsModifierObjsCheck objs) do return False
	
	suspendEditing()

	local newSubMatObject = #()

	local orphanCount = 0
	local childCount = 0
	
	local collObjects = for obj in objs where (isKindOf obj Col_Mesh) collect obj
	for obj in collObjects do 
	(
		col2mesh obj
	)
	
	local selObjs = for obj in objs where ( ((isKindOf obj Editable_mesh) or (isKindOf obj Editable_poly)) and obj.modifiers.count == 0 ) collect obj
	
	local objContainer = RSmultiSubObjCont selObjs

	if (objContainer != False) do 
	(
		local prefix  = RsContFuncs.GetContPrefix ObjContainer		
		RScreateNewMultiSubForObjs selObjs prefix:prefix showProgress:showProgress silent:silent uniqueMats:uniqueMats
	)
	
	for obj in collObjects do 
	(
		mesh2col obj
	)
	
	resumeEditing()
)

-----------------------------------------------------------------------------------------------------------
-- RsCreateNewMultiSubTxd:
--	Create optimised multimaterials per TXD for a given set of objects
-----------------------------------------------------------------------------------------------------------
fn RScreateNewMultiSubTxd objs:(getCurrentSelection()) showProgress:true silent:true uniqueMats:true = 
(
	if (not RsModifierObjsCheck objs) do return False
	
	local notCancelled = true
	suspendEditing()

	local matEditWasOpen = MatEditor.isOpen()
	MatEditor.Close()
	
	local selObjs = for obj in objs where ( ((isKindOf obj Editable_mesh) or (isKindOf obj Editable_poly)) and obj.modifiers.count == 0 ) collect obj

	local objContainer = RSmultiSubObjCont selObjs
	
	if (objContainer == False) then (notCancelled = false) else 	
	(
		local selTxdList = #()
		local selTxdListObjs = #()
		
		-- create a list of the unique txd names in the selection, and lists of objects that use them
		local txdIdx, txdName, txdNum
		for obj in selObjs do 
		(
			txdIdx = case (getattrclass obj) of 
			(
				"Gta Object":
				(
					txdIdx = GetAttrIndex "Gta Object" "TXD"
				)
				"GtaAnimHierarchy":
				(			
					txdIdx = GetAttrIndex "GtaAnimHierarchy" "TXD"
				)
				Default:(-1)
			)
			
			if (txdIdx != -1) then 
			(		
				txdName = GetAttr obj txdIdx
				txdNum = findItem selTxdList txdName
				
				if (txdNum == 0) do 
				(
					append selTxdList txdName
					append selTxdListObjs #()
					txdNum = selTxdList.count
				)
				
				append selTxdListObjs[txdNum] obj
			)
		)
		
		-- run through each unique txd name, creating a new multisub that
		-- has the textures of each object with that txd name
		local objNumStart = 0
		local prefix = RsContFuncs.GetContPrefix objContainer 

		if showProgress do 
		(
			progressStart "Creating new TXD multiSubs:"
		)
		
		local hwMode = unsupplied
		
		for txdNum = 1 to selTxdList.count while notCancelled do 
		(
			local txdName = selTxdList[txdNum]
			local txdObjs = selTxdListObjs[txdNum]

			notCancelled = RScreateNewMultiSubForObjs txdObjs prefix:(prefix + "[txd:" + txdName + "]") showProgress:showProgress startProgress:false objNumStart:objNumStart objCount:selObjs.count silent:silent hwMode:&hwMode uniqueMats:uniqueMats
			
			objNumStart += txdObjs.count
		)
		if showProgress do 
		(
			progressEnd()
		)
	)
	if matEditWasOpen do MatEditor.Open()
	
	resumeEditing()
	notCancelled
)

-----------------------------------------------------------------------------------------------------------
-- RsCreateNewMultiSubObject:
--	Create a optimised multimaterial per object, for a given set of objects
-----------------------------------------------------------------------------------------------------------
fn RScreateNewMultiSubObject objs:(getCurrentSelection()) showProgress:true silent:true uniqueMats:true = 
(
	if (not RsModifierObjsCheck objs) do return False
	
	suspendEditing()
	
	local matEditWasOpen = MatEditor.isOpen()
	MatEditor.Close()
	
	local collObjects = for obj in objs where (isKindOf obj Col_Mesh) collect obj
	for obj in collObjects do 
	(
		col2mesh obj
	)

	local selObjs = for obj in objs where ( ((isKindOf obj Editable_mesh) or (isKindOf obj Editable_poly)) and obj.modifiers.count == 0 ) collect obj

	local notCancelled = true
	local objNum = 0
	
	local hwMode = unsupplied
	
	if showProgress do 
	(
		progressStart "Creating new object multiSubs:"
	)
	
	local objConts = #()
	local prefixes = #()
	
	for obj in selObjs while notCancelled do 
	(
		local ObjContainer = RsGetObjContainer Obj
		
		local Prefix = ""
		local PrefixNum = findItem ObjConts ObjContainer
		
		if (prefixNum == 0) do 
		(
			prefix = RsContFuncs.GetContPrefix ObjContainer
			
			append Prefixes Prefix
			append ObjConts ObjContainer
			PrefixNum = Prefixes.Count
		)
		
		Prefix = Prefixes[PrefixNum]
		
		notCancelled = RScreateNewMultiSubForObjs #(obj) prefix:prefix showProgress:showProgress startProgress:false objNumStart:objNum objCount:selObjs.count silent:silent hwMode:&hwMode uniqueMats:uniqueMats
		
		objNum += 1
	)
	
	for obj in collObjects do 
	(
		mesh2col obj
	)
	
	if showProgress do 
	(
		progressEnd()
	)
	if matEditWasOpen do MatEditor.Open()
	
	resumeEditing()
	notCancelled
)

-----------------------------------------------------------------------------------------------------------
-- RsMultiMatObjCheck:
--	Warns if user has:
--		More than one object selected
--		Object has non-multimaterial
--		Any objects using that material have modifiers
--	Otherwise returns True, and fills in objMat and objs.
-----------------------------------------------------------------------------------------------------------
fn RSmultiMatObjCheck &objMat = 
(
	local outVal = false
	
	case of 
	(
		-- Check for empty selection:
		(selection.count == 0):
		(	
			messagebox "No object selected" title:"Multimaterial tool error"
		)
		
		-- Check for non-single selection:
		(selection.count != 1):
		(	
			messagebox "Please select just one object" title:"Multimaterial tool error"
		)
		
		-- Check for invalid selection object-type:
		(
			not ((isKindOf $ Editable_mesh) or (isKindOf $ Editable_poly))
		):
		(
			messagebox "Selected object must be either Editable_Mesh or Editable_Poly" title:"Multimaterial tool error"
		)
		
		-- Get object's material, and check for invalid material-type:
		(
			objMat = $.material
			
			not isKindOf objMat Multimaterial
		):
		(
			messagebox "Selection must have multimaterial" title:"Multimaterial tool error"
		)
		
		-- Collect all other geometry sharing this material, and check to see if any have modifiers:
		(
			local objs = for obj in geometry 
			where (obj.mat == objMat) and ((isKindOf obj Editable_mesh) or (isKindOf obj Editable_poly) or (isKindOf obj PolyMeshObject)) 
			collect obj
			
			local modObjs = for obj in objs where (obj.modifiers.count != 0) collect obj
			local hasModObjs = (modObjs.count != 0)
			
			-- Only make array global if it needs to be:
			if hasModObjs do 
			(
				global RSmultiMatModifiedObjs = modObjs
			)

			hasModObjs
		):
		(
			try (destroyDialog RsMultiMatWarningRoll) catch ()
			rollout RsMultiMatWarningRoll "Multimaterial tool error, cancelling" width: 300
			(
				MultiListBox lstBad "Modifiers found on objects using this material!" items:(for obj in RSmultiMatModifiedObjs collect obj.name) selection:#{}
				button btnOK "OK" width:100 align:#right
				
				on lstBad selectionEnd do 
				(
					clearSelection()

					select \
					(
						for item in lstBad.selection 
							where (isValidNode RSmultiMatModifiedObjs[item]) 
							collect RSmultiMatModifiedObjs[item]
					)
					max zoomext sel
				)
				on btnOK pressed do (destroyDialog RsMultiMatWarningRoll)
				on RsMultiMatWarningRoll close do 
				(
					-- Remove global array on rollout-close:
					globalVars.remove #RSmultiMatModifiedObjs
				)
			)
			createDialog RsMultiMatWarningRoll
		)
		
		-- If no warning has been triggered, output True
		default: (outVal = true)
	)
	outVal
)


--
--fn:	RsGetMultisubPointers (called by RsReOrderMultisub)
--desc:	Returns two array pointers to indicate where the next non-alpha shader will be moved to,
--	and also where the last non-alpha shader is in the multiSub.materiallist array
--
fn RsGetMultisubPointers multiSub &nonAlphaPointer &alphaPointer = (

	if (classof multiSub != Multimaterial) then return false
	
	nonAlphaPointer = 0
	alphaPointer = 0
	id = 1

	-- Find the initial position to move a non-alpha shader down to
	while (nonAlphaPointer == 0) do (

		if (RstGetIsAlphaShader multisub.materiallist[id] == true) then (

--			format "NonAlpha Pointer found: id: % name: %\n\n" id (RstGetShaderName multiSub.materialList[id])
			nonAlphaPointer = id			
		)
		else (
--			format "NonAlpha Pointer: id: % mat is non alpha: %\n" id (RstGetShaderName multiSub.materialList[id])
			if (id == multiSub.count) do (
			
				-- If no alpha shaders found, pointer is the same as the total count
				nonAlphaPointer = id
			)
			id += 1
		)
	)
	
	id = multiSub.count
	
	while (alphaPointer == 0) do (

		if (RstGetIsAlphaShader multisub.materiallist[id] == false) then (

--			format "Alpha Pointer found: id: % name: %\n\n" id (RstGetShaderName multiSub.materialList[id])
			alphaPointer = id			
		)
		else (
--			format "Alpha Pointer: id: % mat is alpha: %\n" id (RstGetShaderName multiSub.materialList[id])
			if (id == 1) do (

				-- If no non-alpha shaders found, pointer is the same as the first index
				alphaPointer = id
			)
			id -= 1
		)
	)
)

--
--fn:	RsReOrderMultisub
--desc:	Re-orders a Multisub materials, so that non-alpha shaders are moved up the list and the alpha shaders down the list
--	but keeping the order they were in within their own subset (e.g. the non-alpha shaders are ordered the same as they were before)
--	
--	This uses two pointers to track where the next non-alpha submaterial should be moved to, as well as where the last non-alpha
--	submaterial is:
--
--	N = Non-alpha shader		* = nonAlphaPointer
--	A = Alpha shader		^ = alphaPointer
--
--	Initial Array:		N N A N A A N N A
--				    *         ^ 
--				1 2 3 4 5 6 7 8 9  
--	
--	It uses a couple of loops, the first is to check if sorting is finished.  The next is to check from the first alpha 
--	shader in the list, up to find a non-alpha shader.  If another non-alpha shader is found along the array, then the next
--	loop swaps it down with the value next to it (to keep the ordering of the alpha shaders amongst themselves, and same with
--	the non-shaders).
--	When that is done, it gets new pointers for the new list state and checks again if the two pointers aren't pointing at the
--	same index.
--	
--
fn RsReOrderMultisub = 
(
	local multiSub

	if (RSmultiMatObjCheck &multiSub) then 
	(
		-- Get the initial pointers for the list (as it may already be ordered a lot, the way we want) to cut down cycles
		RsGetMultisubPointers multiSub &nonAlphaPointer &alphaPointer
		
		local reOrdered = false
		local keepGoing = true
		
		local matCount = 0
		progressStart "Re-ordering Multisub"
		progressUpdate 0.0
		
		while not reOrdered and (keepGoing = progressUpdate (100.0*matCount/multiSub.count)) do 
		(			
			if (nonAlphaPointer < multiSub.count and nonAlphaPointer < alphaPointer) then 
			(
				-- This is used to find the next non-alpha shader in the list, as it will increment
				-- when new pointers are found
				matIDToCheck = nonAlphaPointer + 1				
				-- mat1 is the mat with lower ID
				mat1 = multiSub.materialList[nonAlphaPointer]				
				
				movedMat = false
				-- alphaPointer + 1 as the entry at alphaPointer is a non-alpha shader which
				-- we want to check
				while (matIDToCheck < (alphaPointer + 1) and movedMat == false) do 
				(
					mat2 = multiSub.materialList[matIDToCheck]
					
					if (RstGetIsAlphaShader mat2 == false) then (

						-- pos is incremented as we go through the array until the next
						-- non-alpha shader is found
						pos = matIDToCheck

						while ( pos != nonAlphaPointer and pos != 1) do (					

							RsMatSwapSubMats multiSub (pos-1) pos
							pos -= 1
						)

						RsGetMultisubPointers multiSub &nonAlphaPointer &alphaPointer
						-- A material has been moved down the list so break out of the parent while loop
						-- so that the sort can continue with the new pointer values
						movedMat = true
					)
					else (

						matIDToCheck += 1						
					)
				)
				matCount += 1
			)
			else 
			(
				reOrdered = true
			)
		)
		
		progressEnd()

		if reOrdered and (matCount == 0) do 
		(
			messagebox "Multisub already ordered"
		)
		
		true
	)
	else
	false
)

-----------------------------------------------------------------------------------------------------------
-- RsInsertSubMatToSel:
--	Gives selected object a new blank matId, editing meshes and multimaterial accordingly:
-----------------------------------------------------------------------------------------------------------
fn RsInsertSubMatToSel newMatId = 
(
	local multiSub
	if (RSmultiMatObjCheck &multiSub) do 
	(
		RsMatInsertSubMat multiSub newMatId
	)
)

-----------------------------------------------------------------------------------------------------------
-- RsSwapSubMatsForSel:
--	Swap two matIds in selected objects' multimaterial:
-----------------------------------------------------------------------------------------------------------
fn RsSwapSubMatsForSel matIDA matIDB showErrors:true =
(
	local multiSub
	if (matIDA != matIDB) and (RSmultiMatObjCheck &multiSub) then 
	(
		case of 
		(
			-- Warn if material is missing either of the relevant material-ids for swapping:
			((findItem multiSub.materialIDList matIDA) == 0):
			(
				if showErrors do 
				(
					messagebox ("Selected object's multimaterial has no submaterial " + (matIDA as string)) title:"Multimaterial swap error"
				)
				false
			)
			((findItem multiSub.materialIDList matIDB) == 0):
			(
				if showErrors do 
				(
					messagebox ("Selected object's multimaterial has no submaterial " + (matIDB as string)) title:"Multimaterial swap error"
				)
				false
			)
			default:
			(
				RsMatSwapSubMats multiSub matIDA matIDB
			)
		)
	)
	else false
)
