--
-- Billboard Viewer
-- Temporary correctly-size view-facing objects are show for billboard-shadered tree-lod objects
-- while tool is open
--
-- Neal D Corbett
-- Rockstar Leeds
-- 13/01/2012
--
--try (destroyDialog RsBillboardViewerRoll) catch ()
rollout RsBillboardViewerRoll "Billboard Viewer"
(
	checkButton btnActive "Active" width:((RsBillboardViewerRoll.width / 2) - 20) across:2
	button btnRefresh "Manual Refresh" width:((RsBillboardViewerRoll.width / 2) - 20) enabled:btnActive.checked
	button btnSelBbObjs "Select Billboards"
	
	local bbSizeIdx
	local sourceObjs = #()
	local viewObjs = #()
	local bbSizesWas = #()
	local nodeCallback
	local idxDontExport = GetAttrIndex "Gta Object" "Dont Export"
	
	fn getObjMat obj = 
	(
		(RsGetMaterialsOnObjFaces obj)[1]
	)
	
	fn isBillboardObj obj = 
	(
		(isEditPolyMesh obj) and 
		(obj.mesh.numFaces == 2) and 
		(RsIsBillboardMat (getObjMat obj))
	)
	
	-- Run by redraw callback:
	fn viewerUpdate = 
	(
		local suspendedEditing = false
		
		for objNum = 1 to sourceObjs.count do 
		(
			local sourceObj = sourceObjs[objNum]
			local viewObj = viewObjs[objNum]
			local bbSizeWas = bbSizesWas[objNum]
			
			if (not isValidNode sourceObj) then 
			(
				if isValidNode viewObj do 
				(
					delete viewObj
				)
			)
			else 
			(
				if (not isValidNode viewObj) do 
				(
					bbSizesWas[objNum] = bbSizeWas = undefined
					
					viewObj = editable_Mesh wirecolor:sourceObj.wirecolor name:("TEMP_BBVIEW_" + sourceObj.name)
					viewObj.mesh = sourceObj.mesh
					setAttr viewObj idxDontExport True
					
					local objCont = Containers.IsInContainer sourceObj
					if (objCont != undefined) do (objCont.AddNodesToContent viewObj)
					viewObj.parent = sourceObj
					
					-- Transfer the mapping-data:
					if not suspendedEditing do 
					(
						suspendEditing()
						suspendedEditing = true
					)
					channelInfo.copyChannel viewObj 3 2
					channelInfo.pasteChannel viewObj 3 1
					convertToMesh viewObj
					
					viewObjs[objNum] = viewObj
				)
				
				if viewObj.isHidden do (viewObj.isHidden = false)
				
				local coordSysTM = Inverse(getViewTM())		
				local lodXform = coordSysTM
				lodXform[4] = sourceObj.center
				
				if not RsCompareMatrix viewObj.transform lodXform do 
				(
					viewObj.transform = lodXform
				)
					
				local objMat = getObjMat sourceObj
				
				if RsIsBillboardMat objMat then 
				(
					if (viewObj.material != objMat) do 
					(
						viewObj.material = objMat
					)
					
					-- Get shader's billboard-size index:
					if (bbSizeIdx == undefined) do 
					(
						local notFound = true
						
						for varNum = 1 to RstGetVariableCount objMat while notFound do 
						(
							local varName = RstGetVariableName objMat varNum

							if (matchPattern varName pattern:"BB Width/Height") do 
							(
								bbSizeIdx = varNum
								notFound = false
							)						
						)
					)
					
					local bbSizeVal = RstGetVariable objMat bbSizeIdx
					local bbSize = [bbSizeVal[1], bbSizeVal[2]] * 0.5
					
					-- Only edit viewmesh if size has changed:
					if (bbSize != bbSizeWas) do 
					(
						bbSizesWas[objNum] = bbSize

						local vertPosArray = #([-bbSize.x,-bbSize.y,0], [bbSize.x,-bbSize.y,0], [-bbSize.x,bbSize.y,0], [bbSize.x,bbSize.y,0])
						
						for vertNum = 1 to 4 do 
						(
							setVert viewObj.mesh vertNum vertPosArray[vertNum]
						)
						update viewObj
					)
				)
				else 
				(
					delete viewObj
				)
			)
		)
		
		if suspendedEditing do 
		(
			resumeEditing()
		)
	)
	
	fn stop redraw:true = 
	(
		unregisterRedrawViewsCallback viewerUpdate
		
		for obj in viewObjs where isValidNode obj do 
		(
			delete obj
		)
		
		sourceObjs = #()
		viewObjs = #()
		bbSizesWas = #()
		
		callbacks.removeScripts id:#RsBillboardViewer
		nodeCallback = undefined
		gc light:true
		
		if redraw do 
		(
			forceCompleteRedraw()
		)
	)
	
	fn setupCallbacks = 
	(
		callbacks.removeScripts id:#RsBillboardViewer
		
		callbacks.addScript #filePreSave "RsBillboardViewerRoll.stop()" id:#RsBillboardViewer
		callbacks.addScript #filePostSave "RsBillboardViewerRoll.start()" id:#RsBillboardViewer
		callbacks.addScript #filePreOpen "RsBillboardViewerRoll.stop()" id:#RsBillboardViewer
		callbacks.addScript #filePostOpen "RsBillboardViewerRoll.start()" id:#RsBillboardViewer
		callbacks.addScript #filePreMerge "RsBillboardViewerRoll.stop()" id:#RsBillboardViewer
		callbacks.addScript #filePostMerge "RsBillboardViewerRoll.start()" id:#RsBillboardViewer
		callbacks.addScript #systemPreNew "RsBillboardViewerRoll.stop()" id:#RsBillboardViewer
		
		callbacks.addScript #nodeHide "RsBillboardViewerRoll.hiding (callbacks.notificationParam())" id:#RsBillboardViewer
		callbacks.addScript #nodeUnhide "RsBillboardViewerRoll.unhiding (callbacks.notificationParam())" id:#RsBillboardViewer
		
		registerRedrawViewsCallback viewerUpdate
	)
	
	fn start = 
	(
		stop redraw:false
		
		if btnActive.checked do 
		(
			sourceObjs = for obj in geometry where (not obj.isHidden) and (isBillboardObj obj) collect obj
			
			setupCallbacks()
		)
		
		forceCompleteRedraw()
	)
	
	fn hiding theNode = 
	(
		local nodeNum = findItem sourceObjs theNode
		
		if (nodeNum != 0) do 
		(
			local displayObj = viewObjs[nodeNum]
			if (isValidNode displayObj) do 
			(
				delete displayObj
			)
			
			deleteItem sourceObjs nodeNum
			deleteItem viewObjs nodeNum
			deleteItem bbSizesWas nodeNum
		)
	)
	
	fn unhiding theNode = 
	(
		if isBillboardObj theNode do 
		(
			append sourceObjs theNode
			append viewObjs undefined
			append bbSizesWas undefined
		)
	)
	
	on btnRefresh pressed do 
	(
		start()
	)
	
	on btnSelBbObjs pressed do 
	(
		clearListener()
		
		local selObjs = for obj in geometry where (isBillboardObj obj) collect obj
		
		select selObjs
		forceCompleteRedraw()
	)
	
	on btnActive changed state do 
	(
		btnRefresh.enabled = state
		
		if state then 
		(
			start()
		) else 
		(
			stop()
		)
	)
	
	on RsBillboardViewerRoll close do 
	(
		stop()
	)
	
	on RsBillboardViewerRoll open do 
	(
		delete $TEMP_BBVIEW_*
	)
	
	on RsMatOptRoll rolledUp down do 
	(
		RsSettingWrite "rsBillboardViewerRoll" "rollup" (not down)
	)
)
--createDialog RsBillboardViewerRoll width:300