-- QuickDestructibleObjectSetup.ms
-- Setup for interior guys to bump up

filein "startup/objects/util/RsRageParticleObject.ms"

try DestroyDialog QuickDestructibleSetup catch()

rollout QuickDestructibleSetup "Quick destructible object setup"
(
	local damagedBy = 8
	local disappearsWhenDead = true
	local effectNames = #()
	local myColour = (color 0 0 0)
	
	local presetnames = #("very light", "very heavy")
	local presets = #(
		#(1,true, "AMB_FBI_CEILING", (color 255 0 0)),
		#(16,false, "AMB_FBI_SMALL_FLAMES", (color 0 255 0))
	)
	
	group "Presets"
	(
		dropdownlist dropDownPresets "" items:presetnames selection:0
	)
	group "fragment setup part"
	(
		label lblBullt "bullet      ped       car     truck" align:#right
		label comboLabel "Only damaged by:" across:2 align:#left
		slider sliderDamagedBy "" range:[0,16,damagedBy] type:#integer
		label lbldisplay ""
		label checkboxLabel "Disappear when dead" across:2 align:#left
		checkbox checkDisappear "" checked:disappearsWhenDead
	)
	group "Effect setup part"
	(
		dropdownlist listEffects "Effect type:"
		colorpicker pickColour "tint" color:myColour
	)
	button buttApply "Apply" align:#center

	fn apply = 
	(
		local preCheck = true
		if $selection.count!=1 then preCheck=false
		local obj = selection[1]
		if "Gta Object"!=getattrclass obj then preCheck=false
		if not preCheck then
		(
			messagebox "Select only one \"Gta Object\""
			return false
		)
			
		if gFragTuner!=undefined then 
		(
			print "creating frag"
			gFragTuner.RefreshUI()
			print (gFragTuner.CreateHierarachyFromNodes overWriteIfExistent:true)
			local threshold = 1
			for i=1 to damagedBy do threshold = threshold * 2
			print (gFragTuner.setGroupValueDirect gFragTuner.currFragRoot "minDamageForce" threshold setAllChildren:true)
			print (gFragTuner.setGroupValueDirect gFragTuner.currFragRoot "disappearsWhenDead" disappearsWhenDead setAllChildren:true)
			print (gFragTuner.WriteFragTuneData())
		)
		
		local particleInfo = undefined
		for c in CollParts where c.name==listEffects.items[listEffects.selection] do 
		(
			particleInfo=c
			exit
		)
		local particle = RsRageParticleHelper myType:particleInfo.name trigger:(findItem triggerStates particleInfo.type) myColour:myColour
		particle.testNodeForAnimation obj
	)
	
	on sliderDamagedBy changed val do
	(
		damagedBy = val
		local threshold = 1
		for i=1 to damagedBy do threshold = threshold * 2
		lbldisplay.caption = threshold as string
	)
	on checkDisappear changed val do
	(
		disappearsWhenDead = val
	)
	on pickColour changed val do
	(
		myColour = val
	)
	on buttApply pressed do
	(
		apply()
	)
	on dropDownPresets selected val do
	(
		local preset = presets[val]
		sliderDamagedBy.value = damagedBy = preset[1]
		checkDisappear.checked = disappearsWhenDead = preset[2]
		listEffects.selection = findItem effectNames preset[3]
		pickColour.color = myColour = preset[4]
	)
	
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn LoadEntries = (
		newType = #none

		filename = RsGetEntityFxInfoPath()
		
		if not (doesFileExist filename) then
		(
			gRsUlog.LogError "No entityfx.dat found in this project or its core." context:filename
			return false
		)
			
		intFile = openfile filename mode:"rb"
		while eof intFile == false do (
			intLine = RsRemoveSpacesFromStartOfString(readline intFile)
			if intLine.count > 0 and intLine[1] != "#" then (
				intTokens = filterstring intLine " \t"
				if intTokens[1] == "ENTITYFX_AMBIENT_PTFX_START" then (
					newType = #ambient
				) else if intTokens[1] == "ENTITYFX_COLLISION_PTFX_START" then (
					newType = #collision
				) else if intTokens[1] == "ENTITYFX_SHOT_PTFX_START" then (
					newType = #shot
				) else if intTokens[1] == "FRAGMENTFX_BREAK_PTFX_START" then (
					newType = #break
				) else if intTokens[1] == "FRAGMENTFX_DESTROY_PTFX_START" then (
					newType = #destruction
				) else if intTokens[1] == "ENTITYFX_ANIM_PTFX_START" then (
					newType = #anim
				) 
				else if intTokens[1] == "ENTITYFX_SHOT_PTFX_END" or  intTokens[1] == "ENTITYFX_AMBIENT_PTFX_END" or  intTokens[1] == "ENTITYFX_COLLISION_PTFX_END" or intTokens[1] == "FRAGMENTFX_BREAK_PTFX_END" or intTokens[1] == "FRAGMENTFX_DESTROY_PTFX_END" or intTokens[1] == "ENTITYFX_ANIM_PTFX_END" then (
					newType = #none
				) else  if intTokens.count > 1 then (
					newItem = RsCollParticle intTokens[1] newType
					append CollParts newItem
					append effectNames intTokens[1]
				)
			)
		)
		close intFile
	)
	on QuickDestructibleSetup open do
	(
		LoadEntries()
		listEffects.items = effectNames
	)
)

CreateDialog QuickDestructibleSetup 300 300