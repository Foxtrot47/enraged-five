-- Rockstar FogBall Object
-- Feb 2014
-- Neil Finlay (North)

gRsFogBallDrawCallbackActive = false
fn RsFogBallDrawCallback =
(
	gw.setTransform (matrix3 1)
	for obj in objects where (classof obj == RsFogBall) do
	(
		gw.text obj.pos ("Range: " + (obj.range as string) + "%") color:yellow
	)
	gw.enlargeUpdateRect #whole  
)

gRsFogBallLightingTypes = #("FOGHDR", "DIRECTIONAL")

plugin helper RsFogBall
name:"FogBall"
category:"RS Helpers"
extends: Dummy
replaceUI: true
classID:#(0xceaffc4, 0x608dffc7)
(

	local lightingItemsArr = #("FOGHDR", "DIRECTIONAL")
	
	
	
	fn GetNode = (
		refsArray = (refs.dependents this)
		local theNode = for i in refsArray where isvalidnode i collect i
		if (classof theNode)==Array then theNode = theNode[1]
		theNode
	)		

	fn ReloadLightingTypes lightingTypeArray = (

		gRsFogBallHaveLightingTypesBeenCached = true
		if(lightingTypeArray == undefined) then
		(
			gRsFogBallCachedLightingTypes = #("FOGHDR", "DIRECTIONAL")
			lightingItemsArr = gRsFogBallCachedLightingTypes
		)
	)

	parameters main rollout:params
	(
		radius			type:#float		ui:spRadius		default:1
		range			type:#percent 	ui:spRange	default:50
		colour			type:#color		ui:cpColour	default:[255,255,200]
		lightType		type:#string	ui:ddbLtype tabSizeVariable:true
		useTCFogNearCol type:#boolean	ui:cbUseTCFogNearCol default:false
		useTCFogVolumeDensityScalar type:#boolean ui:cbuseTCFogVolumeDensityScalar default:false
	)
	
	rollout params "FogBall"
	(
		spinner spRadius "Radius" range:[0,10000,1]
		spinner spRange "Softness %" range:[0,100,10]
		colorPicker cpColour "Colour" 
 		dropdownList ddbLtype "Light Type" items:gRsFogBallLightingTypes
 		checkbox cbUseTCFogNearCol "Use TC Fog Near Col"
		checkbox cbuseTCFogVolumeDensityScalar "Use TC Fog Volume Density Scalar"
		on colour changed col do
		(
			n = GetNode()
			if not (n == undefined) then (n.wirecolor = col)
		)

		on ddbLtype selected sel do
		(
			lightType = gRsFogBallLightingTypes[sel]
		)
	)

	on getDisplayMesh do
	(

		if not gRsFogBallDrawCallbackActive then
		(
			gRsFogBallDrawCallbackActive = true;
			unregisterRedrawViewsCallback RsFogBallDrawCallback
			registerRedrawViewsCallback RsFogBallDrawCallback
		)
	
		if meshObj == undefined do
		(
			meshObj = createInstance Sphere radius:radius segs:12
		)
		meshObj.mesh

		
	)

	tool create
	(
		on mousePoint click do
		(
			case click of
			(
				1: coordsys grid (nodeTM.translation = gridPoint)
			)
		)
		on mouseMove click do
		(
			case click of
			(
				2: (radius = abs(gridDist.y))
				3: #stop
			)
		)
	)

)