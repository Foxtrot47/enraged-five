------------------------------------------------------------------------
-- physics calculations
------------------------------------------------------------------------

filein "collision/CollPropStore.ms"

global defaultDensity = 10.0

-- stolen from maxscript docu content "How do I calculate the Volume of an Object?" -- seems somewhat suspicious in its results...
fn CalculateVolumeAndCenterOfMass obj = 
( 
	local Volume= 0.0 
	local Center= [0.0, 0.0, 0.0] 
	local theMesh = snapshotasmesh obj
	local numFaces = theMesh.numfaces 
	for i = 1 to numFaces do 
	( 
		local Face= getFace theMesh i 
		local vert2 = getVert theMesh Face.z 
		local vert1 = getVert theMesh Face.y 
		local vert0 = getVert theMesh Face.x 
		local dV = Dot (Cross (vert1 - vert0) (vert2 - vert0)) vert0
		Volume+= dV 
		Center+= (vert0 + vert1 + vert2) * dV 
	) 
	delete theMesh
	Volume /= 6 
	Center /= 24 
	Center /= Volume 
	print (obj.name+" volume:"+Volume as string)
	#(Volume,Center) 
)

fn getDensity obj = 
(
	local collTypeIdx = getattrindex "Gta Collision" "Coll Type"
	if "Gta Collision"==(getattrclass obj) then
	(
		local type = getattr obj collTypeIdx
		local r = undefined
		try(
			type = toUpper type
			r = gCollProperties.get type #DENSITY
		)catch(print (getCurrentException()+":"+type as string))
		print (type+" density:"+r as string)
		return r
	)
	else
		return defaultDensity
)

-- typeChild.cpp 
-- fragTypeChild::ComputeMassFromVolume(fragDrawable&)
-- default mass
fn calculateMass obj = 
(
	local mass = undefined
--	print ("calculateMass with class :"+(classof obj) as string)
	case (classof obj) of
	(
		Col_Box:
		(
			mass = obj.width * obj.height * obj.length
--			print (obj.name+" volume:"+Volume as string)
			mass = (obj.width * obj.height * obj.length * (getDensity obj))
		)
		Editable_Mesh:
		(
			mass = ((CalculateVolumeAndCenterOfMass obj)[1] * (getDensity obj))
		)
		Editable_Poly:
		(
			mass = ((CalculateVolumeAndCenterOfMass obj)[1] * (getDensity obj))
		)
		Col_Mesh:
		(
			col2mesh obj
			mass = ((CalculateVolumeAndCenterOfMass obj)[1] * (getDensity obj))
			mesh2col obj
		)
		default:
			print "unknown object class"
	)
	-- GD: bug 285866: Runtime needs minimum mass:
	if undefined==mass then
	(
		gRsUlog.LogError (obj.name+"'s mass could not be determined! not supported primitive?")
		return 1.0
	)
	if mass < 1.0 then
		mass = 1.0
	print (obj.name+"'s mass: "+mass as string)
	return mass
)
