filein "utils.ms"

global gRootModel = undefined
global gMesh = undefined
global gBoneArray = #()
global gVertIndeces = #()
global gRootBone = undefined
global gCurrRecurseLevel = undefined
global groupIdx = getattrindex "Gta Object" "groupName"
global stateIdx = getattrindex "Gta Object" "animState"
global txdIdx = getattrindex "Gta Object" "TXD"
global animIdx = getattrindex "Gta Object" "Has Anim"


fn goThroughNodesRec root thefunction &extraparams:undefined deleteNode:false= 
(
	local level = gRootBone
	if not thefunction root &extraparams level:&level then
	(
		return false
	)
	local replacedObject = if undefined != level then level else root
	for child in replacedObject.children do
	(
		if isDeleted child then continue
		gCurrRecurseLevel = level
		local ret = goThroughNodesRec child thefunction extraparams:&extraparams deleteNode:deleteNode
		if not ret then 
		(
			return false
		)
	)
	if deleteNode then delete root
	return true
)

fn compareMaterials thechild &originalMaterial level:&level =
(
	if "Gta Object"==(getattrclass thechild)  then
	(
		if undefined==originalMaterial then
		(
			originalMaterial = thechild.material
		)
		else if originalMaterial!=thechild.material then 
		(
			return false
		)
	)
	return true
)

fn bake thechild &compoundMesh level:&level = 
(
	if "Gta Object"!=(getattrclass theChild) then
	(
		local newChild = copy theChild
		select theChild
		copyAttrs()
		select newChild
		pasteAttrs()
		newChild.parent = gCurrRecurseLevel
		for c in thechild.children do
			c.parent = newChild
		newChild.name = theChild.name
		RsCopyUserProps theChild newChild
		copyTransforms theChild newChild
		if "Gta Collision"==getattrclass theChild then
		(
			col2mesh newChild
			newChild.material = theChild.material
			mesh2col newChild
		)
		*level = newChild
		
		if gRootBone==theChild then
			gRootBone = newChild
		
		return true
	)
	
	polyop.attach compoundMesh (copy thechild)

	-- baking animation onto point helpers
	local thename = (thechild.name)
	local bound = nodeGetBoundingBox thechild (matrix3 1)
	bound = bound[2]-bound[1]
	local newBone = dummy name:thename boxsize:bound
	gRsUlog.LogMessage ("creating bone with name "+thename) context:newBone
	newBone.pos = thechild.center
	newBone.objectoffsetrot = inverse thechild.transform
	newBone.pivot = thechild.pivot
	newBone.transform = thechild.transform
	newBone.parent = gCurrRecurseLevel
	for c in thechild.children do
		c.parent = newBone
	append gBoneArray newBone
	RsCopyUserProps theChild newBone
	copyTransforms theChild newBone

	-- actach to mesh and save off
	local currVertCount = getnumverts compoundMesh
	append gVertIndeces currVertCount
	
	*level = newBone

	if theChild==gRootBone then
		gRootBone = newBone

	return true
)

-- starting point of the process
fn bakeAnimToPoints &rootModels =
(
	gMesh = undefined
	gRootBone = undefined
	gBoneArray = #()
	gVertIndeces = #()
	local recurse = true
	gRootModel = rootModels
	if undefined==rootModels then
	(
		gRsUlog.LogError "Select a model of the animated hirarchy."
		return false
	)
	if Array==(classof rootModels) or ObjectSet==(classof rootModels) then
	(
		gRsUlog.LogMessage "Multiple models given to auto skin. Switching off recursion."
		gRootModel = rootModels[1]
		recurse = false
	)
	if undefined==gRootModel then
		return false

	local rootParentModel = gRootModel.parent
	
	if undefined!=RsStatedAnimRollout and RsStatedAnimRollout.open then 
		RsStatedAnimRollout.enableCallbacks false
	
	local originalMaterial = gRootModel.material
	local rootname = "rig_root"
	if recurse then
	(
		while undefined!=gRootModel.parent do gRootModel = gRootModel.parent
	
		-- deal with two roots setup for vehicles (look for chassis and use as root)
		local chassis = for c in gRootModel.children while c.name=="chassis" collect c
		if chassis.count>0 then
		(
			chassis = chassis[1]
			rootname = gRootModel.name
			gRootModel = chassis
		)
	)
	
	local succeeded = true
	if recurse then
	(
		if not goThroughNodesRec gRootModel compareMaterials extraparams:&originalMaterial then
			succeeded = false
	)
	else
	(
		for m in rootModels do
		(
			if not compareMaterials m &originalMaterial then
				succeeded = false
		)
	)
	if not succeeded then
	(
		if (queryBox ("A child material is not the same as on the root node. Abort?")) then
		(
			return false
		)
	)

	if recurse then
	(
		local rootRig = dummy name:(rootname+"_skel") pos:gRootModel.pos boxsize:[.2,.2,.2]
		converttomesh rootRig
		gCurrRecurseLevel = rootRig
	)
	
	-- on baking, if the models equals this, this gets updated with the new bone.
	gRootBone = gRootModel
	
	local compoundMesh = Editable_mesh name:(rootname+"_skin") pos:gRootModel.pos
	convertTo compoundMesh Editable_Poly
	compoundMesh.material = originalMaterial

	if "Gta Object"==(getattrclass gRootModel) then
	(
		setAttr compoundMesh groupIdx (getAttr gRootModel groupIdx)
		setAttr compoundMesh txdIdx (getAttr gRootModel txdIdx)
	)
	setAttr compoundMesh stateIdx "Animation"
	setAttr compoundMesh animIdx true
	
	local succeeded = true
	if recurse then
	(
		if not (goThroughNodesRec gRootModel bake extraparams:&compoundMesh deleteNode:true) then
			succeeded = false
	)
	else
	(
		for m in rootModels do
		(
			gCurrRecurseLevel = m.parent
			if not bake m &compoundMesh then
				succeeded = false
			else
				delete m
		)
	)
		
	if not succeeded then
	(
		messagebox ("Error in conversion")
		return false
	)

	local theSkin = Skin()
	theSkin.mirrorEnabled = off
--	theSkin.alwaysDeform = off
	converttomesh compoundMesh
	compoundMesh.material = originalMaterial
	select compoundMesh
	setCommandPanelTaskMode #modify
	addModifier compoundMesh (theSkin)
	update compoundMesh
	
	--progressStart "skinning" SKINOPS DOESNT LIKE THIS
	local lastIndex = 1
	local rootBoneInSkin = false
	for boneIndex=1 to gVertIndeces.count do
	(
		skinOps.addbone theSkin gBoneArray[boneIndex] 0
		
		if gBoneArray[boneIndex] == gRootBone then 
			rootBoneInSkin = true
		
		if compoundMesh.numverts != gVertIndeces[gVertIndeces.count] then 
		(
			messagebox ("not equal vert numbers in meshes: "+ compoundMesh.numverts as string +","+ gVertIndeces[gVertIndeces.count] as string)
			return false
		)
		
 		for i=lastIndex to gVertIndeces[boneIndex] do
		(
 			skinOps.replacevertexweights theSkin i boneIndex 1.0
		
			-- auto normal all verts
			skinOps.unNormalizeVertex theSkin i false
		)
		lastIndex = gVertIndeces[boneIndex]+1
	)
	if not rootBoneInSkin then
	(
		skinOps.addbone theSkin gRootBone 0
	)
	
	if isValidNode rootParentModel then
		compoundMesh.parent = rootParentModel
	
	gRsStatedAnimTool.Deinit()
	if undefined!=RsStatedAnimRollout and RsStatedAnimRollout.open then
	(
		RsStatedAnimRollout.enableCallbacks true
	)
--	theSkin.alwaysDeform = on
	
	rootModels = compoundMesh
	
	return true
)