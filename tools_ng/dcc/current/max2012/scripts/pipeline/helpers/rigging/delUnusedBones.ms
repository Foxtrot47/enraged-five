-- delete unused bones from selected objects with skin modifiers
-- @author Gunnar Droege

fn RsSelDeleteUnusedBones = 
(
	for s in (selection as array) where "Gta Object"==getattrclass s do
	(
		local mySkin = s.modifiers[#skin]
		if undefined==mySkin then
			continue
		
		local usedSystemBoneIds = #()
		setCommandPanelTaskMode #modify
		modPanel.setCurrentObject mySkin ui:true
		for k=1 to (skinOps.GetNumberVertices mySkin) do
		(
			for i=1 to (skinOps.GetVertexWeightCount mySkin k) do
			(
				local systemBoneIndex = skinOps.GetVertexWeightBoneID mySkin k i
				local myWeight = (skinOps.GetVertexWeight mySkin k i)
				if myWeight> 0 then
				(
					appendIfUnique usedSystemBoneIds systemBoneIndex
					isused = true
				)
			)
		)
		for m=(skinOps.GetNumberBones mySkin) to 1 by -1 
			where "SKEL_ROOT"!=(skinOps.GetBoneName mySkin m 1)
		do
		(
			if 0==findItem usedSystemBoneIds m then
			(
				skinOps.removebone mySkin m
			)
		)
		gc light:true
	)
)