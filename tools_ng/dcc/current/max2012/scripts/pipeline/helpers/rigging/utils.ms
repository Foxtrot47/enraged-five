-- 
--
-- Baking helpers
-- GunnarD: meant for vehicle and statedAnim baking/skin-welding.

fn copyTransforms theChild newBone = 
(
	--gRsUlog.LogMessage ("copying transforms from "+theChild as string+" to "+newBone as string)
	if thechild.rotation.isanimated or thechild.pos.isanimated then
	(
		if Euler_XYZ == classof thechild.rotation.controller then
		(
			newBone.rotation.controller = Euler_XYZ()
			for theKey in thechild.rotation.controller.X_Rotation.keys do
				appendKey newBone.rotation.controller.X_Rotation.keys theKey
			for theKey in thechild.rotation.controller.Y_Rotation.keys do
				appendKey newBone.rotation.controller.Y_Rotation.keys theKey
			for theKey in thechild.rotation.controller.Z_Rotation.keys do
				appendKey newBone.rotation.controller.Z_Rotation.keys theKey
		)
		else
		(
			thechild.rotation.controller = TCB_Rotation()
			newBone.rotation.controller = TCB_Rotation()
			local childRotationKeys = thechild.rotation.controller.keys
	--		local boneRotationKeys = &
			for theKey in childRotationKeys do
			(
				appendKey newBone.rotation.controller.keys theKey
			)
		)

		if Position_XYZ == classof thechild.pos.controller then
		(
			newBone.pos.x_position.controller = (classof thechild.pos.x_position.controller)()
			newBone.pos.y_position.controller = (classof thechild.pos.y_position.controller)()
			newBone.pos.z_position.controller = (classof thechild.pos.z_position.controller)()
			for theKey in thechild.pos.x_position.controller.keys do
				appendKey newBone.pos.x_position.controller.keys theKey
			for theKey in thechild.pos.y_position.controller.keys do
				appendKey newBone.pos.y_position.controller.keys theKey
			for theKey in thechild.pos.z_position.controller.keys do
				appendKey newBone.pos.z_position.controller.keys theKey
		)
		else
		(
--			thechild.pos.controller = TCB_Position()
			newBone.pos.controller = TCB_Position()
			local childRotationKeys = thechild.pos.controller.keys
	--		local boneRotationKeys = &
			for theKey in childRotationKeys do
			(
				print (newBone.name+": "+theKey as string)
				appendKey newBone.pos.controller.keys theKey
			)
		)
	)
)


fn substObjInIKChain obj subst =
(
	local references = refs.dependents obj
	for r in references where classof r == IK_Chain_Object do
	(
		if obj == r.controller.startJoint then 
			r.controller.startJoint = subst
		else if obj == r.controller.endJoint then 
			r.controller.endJoint = subst
		exit
	)
)

fn DeleteLODSuffixes str = 
(
	local nameToCheck = str
	if HasLODSuffix nameToCheck then
		nameToCheck = substring nameToCheck 1 (nameToCheck.count-3)
	-- if on a lod and the parent bone name linked to has n't got a bone 
	if matchPattern nameToCheck pattern:"*model" then
		nameToCheck = substring nameToCheck 1 (nameToCheck.count-5)
	if nameToCheck[nameToCheck.count]=="_" then
		nameToCheck = substring nameToCheck 1 (nameToCheck.count-1)
	return nameToCheck
)

fn GetHighestWeightedBone skinobj = 
(
	local highestWeightedBone = undefined
	local highestWeight = 0
	local addBoneIt = 1
	for k=1 to (skinOps.GetNumberVertices skinobj) do
	(
		local thebones  = #()
		local boneWeights  = #()
		for i=1 to (skinOps.GetVertexWeightCount skinobj k) do
		(
			local systemBoneIndex = skinOps.GetVertexWeightBoneID skinobj k i
			local myWeight = (skinOps.GetVertexWeight skinobj k i)
			local boneName = skinOps.GetBoneName skinobj systemBoneIndex 0
			boneName = DeleteLODSuffixes boneName
			if myWeight>highestWeight then
			(
				highestWeight=myWeight
				highestWeightedBone = getNodeByName (DeleteLODSuffixes boneName)
			)
		)
	)
	return highestWeightedBone
)

	
-- Look for LOD model suffix 
fn HasLODSuffix n =
(
	if (n.count<=3) then
		return false
	local suffixes = #("_l0","_l1","_l2","_l3", "_ng")
	if 0!=(findItem suffixes (substring n (n.count-2) (n.count))) then
		return true
	return false
)

fn DeleteLodChildren root = 
(
	if not isValidNode root then
	(
		gRsUlog.LogError ("Filed to delete object hierarchy:"+(ss=stringstream"";stack to:ss;ss))
		return false
	)
	for c in root.children where (isValidNode c) do
		DeleteLodChildren c
	
	if HasLODSuffix root.name then
	(
		if root.children.count>0 then
			print ("Not deleting lod node "+root.name+" as it contains non-LOD children.")
		else
		(
			print ("DLETING "+root as string)
			delete root
		)
	)
)

fn findBoneWithNameRec root original &newRoot = 
(
	-- deleting lod prefix
	local nameToCheck = DeleteLODSuffixes original.name
	if 		(	root.name==nameToCheck or 
				matchPattern nameToCheck pattern:(root.name+"*model*") ) and
			not HasLODSuffix root.name and -- name check
			"Gta Object"!=(getattrclass root) 
		then -- type check
	(
--		print ("+++++++++++++++++FOUND bone with name "+nameToCheck)
		newRoot = root
		return true
	)
	else for c in root.children do 
	(
		if (findBoneWithNameRec c original &newRoot) then
			return true
	)
--	print ("+++++++++++++++++FAILED to find bone with name "+nameToCheck)
	return false
)