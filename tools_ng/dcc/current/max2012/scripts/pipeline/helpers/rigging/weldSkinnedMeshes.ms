filein "pipeline/export/vehicles/utils.ms"
filein "pipeline/helpers/rigging/utils.ms"

global gSkinWeldProgresssDialog = undefined
global allDueProcesses = 0
global processed = 0
global gNewName = undefined
global gSkinWeldPerObjCalback = undefined
global gSkinWeldSkelObject = undefined
global formerSkinObj = #()
global gBonesAlreadyAdded = #()
global gBoneIdsAlreadyAdded = #()


fn increaseProgress = 
(
	processed = processed + 1
	if undefined==gSkinWeldProgresssDialog then return false
	try(
		gSkinWeldProgresssDialog.Controls.Item[2].value = ((processed as float)/allDueProcesses)*100
	)catch()
	gSkinWeldProgresssDialog.Focus()
	gSkinWeldProgresssDialog.Refresh()
--		print (processed as string+", "+allDueProcesses as string)
)

fn stopButtonPressed =
(
	print "3. Button Pressed - No Arguments"
)

fn getHierDepth obj = 
(
	local depth = 0
	while undefined!=obj.parent do (obj = obj.parent; depth = depth+1)
	return depth
)

fn compare v1 v2 =
(
	local d = (getHierDepth v2)-(getHierDepth v1)
	case of
	(
		(d < 0.): -1
		(d > 0.): 1
		default: 0
	)
)

fn boneIdIfAlreadyAdded objname newBoneIndex:undefined = 
(
	local boneIndex = findItem gBonesAlreadyAdded objname
	if 0!=boneIndex then
	(
		local newBoneIndex = gBoneIdsAlreadyAdded[boneIndex]
		boneIndex = newBoneIndex
	)
	else
	(
		if newBoneIndex!=undefined then
		(
			append gBonesAlreadyAdded objname
			append gBoneIdsAlreadyAdded newBoneIndex
		)
	)
	return boneIndex
)

fn weldSkinnedObjects objs = 
(
	if objs.count<2 then
	(
		messagebox "select at least two skinned GTA objects"
		return false
	)
	
-------------------------------------------------------------------------------------------------------
-- Checking for applied materials to be the same
-------------------------------------------------------------------------------------------------------
	local usedMaterials = #()
	local usedMtlObjs = #()
	for obj in objs 
		where 
			"Gta Object"==(getattrclass obj) and 
			undefined!=obj.material
		do
	(
		local foundIndex = findItem usedMaterials obj.material
		if 0!=foundIndex then
		(
			append usedMtlObjs[foundIndex] obj.name
		)
		else
		(
			append usedMaterials obj.material
			append usedMtlObjs #(obj.name)
		)
	)
	if usedMaterials.count>1 then
	(
		gRsUlog.logError "Multiple materials found on object selection. Click on following errors to select objects."
		for mtlIndex = 1 to usedMaterials.count do
		(
			gRsUlog.logError (usedMaterials[mtlIndex].name) context:(RsStringArray usedMtlObjs[mtlIndex])
		)
		return false
	)

		
	slidertime = 0
	--try
	(
		gSkinWeldProgresssDialog = dotNetObject "MaxCustomControls.ProgressDialog"
		gSkinWeldProgresssDialog.Show()
		gSkinWeldProgresssDialog.text = "Create Skinning Progress"
		gSkinWeldProgresssDialog.Controls.Item[0].text = "stop"
		gSkinWeldProgresssDialog.Controls.Item[1].text = "preparing..."
		dotNet.addEventHandler gSkinWeldProgresssDialog.Controls.Item[0] "click" stopButtonPressed
		gSkinWeldProgresssDialog.Controls.Item[2].value = 0

		local groupIdx = getattrindex "Gta Object" "groupName"
		local stateIdx = getattrindex "Gta Object" "animState"
		local txdIdx = getattrindex "Gta Object" "TXD"
		local animIdx = getattrindex "Gta Object" "Has Anim"
		
		local firstGtaObject = undefined
		formerSkinObj = #()
		for iobj=objs.count to 1 by -1 do
		(
			-- filterting out unwanted objs
			local theObj = objs[iobj]
			if "Gta Object"!=getattrclass theObj then
			(
				gRsUlog.LogMessage ("deleting "+theObj.name)
				objs = (deleteItem objs iobj)
			)
		)
		
		if objs.count<1 then
		(
			messagebox "No objects to convert."
			return false
		)

		-- sort by hierarchy depth
		-- qsort objs compare

		-- then based on actual amount of suuported objs, call for skin.
		for iobj=1 to objs.count do
		(
			local theObj= objs[iobj]
			if "Gta Object"==(getattrclass objs[iobj]) then
			(
				local theSkin = formerSkinObj[iobj]  = objs[iobj].modifiers[#Skin]
				if undefined==theSkin then
				(
					local theBone = undefined
					if undefined!=gSkinWeldSkelObject and (findBoneWithNameRec gSkinWeldSkelObject objs[iobj] &theBone) then
					(
						gRsUlog.LogMessage ("Found bone to use before creating new one:"+theBone.name)
					)
					else
					(
						-- trying to find bone with name
						local objsWithBoneName = getNodeByName (DeleteLODSuffixes objs[iobj].name) all:true
						print ("objsWithBoneName:"+objsWithBoneName as string)
						local theBone = undefined
						for obj in objsWithBoneName where "Gta Object"!=(getattrclass obj) while theBone == undefined do
							theBone = obj

						-- not found create a new one
						if undefined==theBone then
						(
							theBone = dummy()
							theBone.name = objs[iobj].name
							gRsUlog.LogMessage ("created dummy bone with name:"+theBone.name)
							theBone.boxsize = [.2,.2,.2]
							copyTransforms objs[iobj] theBone
							theBone.transform = objs[iobj].transform
							RsCopyUserProps objs[iobj] theBone
							if undefined!=objs[iobj].parent then
								theBone.parent = objs[iobj].parent
						)
					)
					formerSkinObj[iobj] = theBone
				)
			)
		)
-- 		select objs
-------------------------------------------------------------------------------------------------------
-- Finding a new skeleton root in none was given
-------------------------------------------------------------------------------------------------------
		if undefined==gSkinWeldSkelObject then
		(
			if undefined!=formerSkinObj[1] then
			(
				local lastBoneParent = undefined
				select objs[1]
				setCommandPanelTaskMode #modify
				modpanel.setCurrentObject formerSkinObj[1]
				for i=1 to skinOps.GetNumberBones formerSkinObj[1] do
				(
					local theBone = getNodeByName (skinOps.GetBoneName formerSkinObj[1] i 0)
					if undefined!=theBone and undefined!=theBone.parent then
					(
						if lastBoneParent==theBone.parent then
							gSkinWeldSkelObject = theBone.parent
						lastBoneParent = theBone.parent
					)
				)
			)
			if undefined==gSkinWeldSkelObject then
			(
				gSkinWeldSkelObject = dummy name:"NewSkelRoot" boxsize:[.2,.2,.2]
--				gSkinWeldSkelObject.width = gSkinWeldSkelObject.length = gSkinWeldSkelObject.height = 0.2
--				gSkinWeldSkelObject.widthsegs = gSkinWeldSkelObject.lengthsegs = gSkinWeldSkelObject.heightsegs = 1
--				converttomesh gSkinWeldSkelObject
			)
		)

-------------------------------------------------------------------------------------------------------
-- Welding meshes and setting up progress info
-------------------------------------------------------------------------------------------------------
		allDueProcesses = 0
		local compObj = Editable_Mesh pos:gSkinWeldSkelObject.pos
		compObj.name = "RENAME THIS OBJECT"
		if undefined!=gNewName then
			compObj.name = gNewName
		for objIndex=1 to objs.count do
		(
			if "Gta Object"!=(getattrclass objs[objIndex]) then continue
			
			firstGtaObject = objs[objIndex]
			maxops.clonenodes #(objs[objIndex]) actualNodeList:&c newNodes:&d
			local compObjTemp = d[1]
			compObjTemp.material = undefined
			select compObjTemp
			setCommandPanelTaskMode #modify
			converttomesh compObjTemp
			RsVehicleUtilStruct.RsAttach compObj compObjTemp

			if 	undefined!=formerSkinObj[objIndex] and 
				"Gta Object"==(getattrclass formerSkinObj[objIndex]) then
			(
				modPanel.setCurrentObject formerSkinObj[objIndex] ui:true node:objs[objIndex]
				allDueProcesses = allDueProcesses +
					(skinOps.GetNumberBones formerSkinObj[objIndex]) + 
					((skinOps.GetNumberVertices formerSkinObj[objIndex])*2)
				gRsUlog.LogMessage ("number of allDueProcesses:"+allDueProcesses as string)
			)
			else allDueProcesses = allDueProcesses +  objs[objIndex].numverts
		)
		
		
		local theSkin = Skin()
		theSkin.alwaysDeform = off
		addModifier compObj theSkin
		select compObj
		setCommandPanelTaskMode #modify
		gRsULog.LogMessage ("First object in list to take material from: "+firstGtaObject as string)
		compObj.material = firstGtaObject.material

		gBonesAlreadyAdded = #()
		gBoneIdsAlreadyAdded = #()
		local vertexBones = #()
		local vertexWeights = #()
		local boneoffset = 0
		processed = 0
		
-------------------------------------------------------------------------------------------------------
-- Copy weight info
-------------------------------------------------------------------------------------------------------
		for objIndex=1 to objs.count do
		(
			gSkinWeldProgresssDialog.Controls.Item[1].text = ("Getting skin vertexWeights for object "+(objIndex as string)+" ...")
------------------------------------------------------
-- undefined cases are unlinked (linking causes chassis to screw up)
------------------------------------------------------
			if undefined==formerSkinObj[objIndex] then
			(
				if undefined!=gSkinWeldPerObjCalback then
				(
-- 					local linkto = gSkinWeldSkelObject
-- 					findBoneWithNameRec gSkinWeldSkelObject theName &linkto
-- 					for c in objs[objIndex].children do
-- 						gSkinWeldPerObjCalback c linkto
					gRsUlog.LogDebug "undefined==formerSkinObj[objIndex]?"
				)
			)
------------------------------------------------------
-- Skinned objects are what we're after here
------------------------------------------------------
			else if Skin==(classof formerSkinObj[objIndex]) then
			(
				boneoffset = skinOps.GetNumberBones theSkin
				-- add bones
				local addedBonesPerObject = #()
				modpanel.setCurrentObject formerSkinObj[objIndex]
				gRsUlog.LogMessage ("next process count:"+(skinOps.GetNumberBones formerSkinObj[objIndex]) as string)
				for i=1 to skinOps.GetNumberBones formerSkinObj[objIndex] do
				(
					local boneName = skinOps.GetBoneName formerSkinObj[objIndex] i 0
					boneName = DeleteLODSuffixes boneName
					local objsWithBoneName = getNodeByName boneName all:true
					print ("objsWithBoneName:"+objsWithBoneName as string)
					if objsWithBoneName.count<=0 then
					(
						messagebox "no obj with bone name."
						continue
					)
					local newBone = objsWithBoneName[1]
					for obj in objsWithBoneName where "Gta Object"!=(getattrclass obj) while newBone == objsWithBoneName[1] do
						newBone = obj
					local oldBone = newBone
-- 					if objsWithBoneName.count>1 then
-- 					(
-- 						for o in objsWithBoneName do
-- 						(
-- 							if "Gta Object"!=(getattrclass o) then
-- 								newBone = o
-- 							else
-- 								oldBone = o
-- 						)
-- 					)
					-- only add if not already in the list...
					if 0==(boneIdIfAlreadyAdded newBone.name) then
					(
						skinops.addBone theSkin newBone 1
						append addedBonesPerObject newBone.name
					)
					
					if (gSkinWeldSkelObject!=oldBone) then
					(
						local linkto = gSkinWeldSkelObject
-- 						print ("trying to find bone "+oldBone.parent as string+" underneath root "+gSkinWeldSkelObject as string)
						if undefined!=oldBone.parent then
						(
							--findBoneWithNameRec gSkinWeldSkelObject oldBone.parent &linkto
							local objsWithName = getNodeByName (DeleteLODSuffixes oldBone.parent.name) all:true
							for obj in objsWithName where "Gta Object"!=(getattrclass obj) while linkTo==gSkinWeldSkelObject do
								linkTo = obj
						)
						if linkto != newBone then
						(
							gRsUlog.LogMessage ("relinking "+newBone as string+" to "+linkto as string+". theBone.parent:"+oldBone.parent as string+", gSkinWeldSkelObject:"+gSkinWeldSkelObject as string)
							newBone.parent = linkto
						)
					)
					if oldBone!=newBone then
					(
						copyTransforms oldBone newBone
						substObjInIKChain oldBone newBone
					)
					
					increaseProgress()
				)
				-- bone offset from first one
				modPanel.setCurrentObject formerSkinObj[objIndex] ui:true node:objs[objIndex]
				gRsUlog.LogMessage ("next process count:"+(skinOps.GetNumberVertices formerSkinObj[objIndex]) as string)
				local highestWeightedBone = gSkinWeldSkelObject
				local highestWeight = 0
				local addBoneIt = 1
				for k=1 to (skinOps.GetNumberVertices formerSkinObj[objIndex]) do
				(
					local thebones  = #()
					local boneWeights  = #()
					for i=1 to (skinOps.GetVertexWeightCount formerSkinObj[objIndex] k) do
					(
						local systemBoneIndex = skinOps.GetVertexWeightBoneID formerSkinObj[objIndex] k i
						local myWeight = (skinOps.GetVertexWeight formerSkinObj[objIndex] k i)
						local boneName = skinOps.GetBoneName formerSkinObj[objIndex] systemBoneIndex 0
						boneName = DeleteLODSuffixes boneName
						local actuallyAddedIndex = findItem addedBonesPerObject boneName
						local newindex = (boneoffset + actuallyAddedIndex)
--						print ("boneName:"+boneName+", newindex:"+newindex as string+", boneoffset:"+boneoffset as string+", addBoneIt:"+addBoneIt as string)
						if myWeight>highestWeight then
						(
							highestWeight=myWeight
							highestWeightedBone = getNodeByName boneName
						)
						append boneWeights myWeight
						local foudnIndx = boneIdIfAlreadyAdded boneName newBoneIndex:newindex
						if foudnIndx!=0 then
							newindex = foudnIndx
						append thebones newindex
					)
					append vertexBones thebones
					append vertexWeights boneWeights
					increaseProgress()
				)
 				if undefined!=gSkinWeldPerObjCalback then
				(
					for c in objs[objIndex].children do
					(
						local linkTo = highestWeightedBone
--						print ("trying to find bone "+objs[objIndex] as string+" underneath root "+gSkinWeldSkelObject as string)
						local found = findBoneWithNameRec gSkinWeldSkelObject objs[objIndex] &linkto
						gSkinWeldPerObjCalback c linkTo
					)
				)
			)
------------------------------------------------------
-- dummy vertexWeights for unskinned object
------------------------------------------------------
			else if dummy==(classof formerSkinObj[objIndex]) then
			(
				-- bone index
				boneoffset = skinOps.GetNumberBones theSkin
				local newIndex = (boneoffset+1)
				local foundIndex = boneIdIfAlreadyAdded formerSkinObj[objIndex].name newBoneIndex:newIndex
				if foundIndex==0 then
				(
					local obj = objs[objIndex]
					modpanel.setCurrentObject theSkin
					gRsUlog.LogMessage ("add bone "+formerSkinObj[objIndex] as string+" to skin of object "+obj as string) context:(obj as string)
 					skinops.addBone theSkin formerSkinObj[objIndex] 1
 					foundIndex = newIndex
 				)
				local thebones  = #(foundIndex)
				
				-- hierarchy linking
				local linkto = gSkinWeldSkelObject
				if undefined!=objs[objIndex].parent then
				(
-- 					print ("trying to find bone "+objs[objIndex].parent as string+" underneath root "+gSkinWeldSkelObject as string)
					findBoneWithNameRec gSkinWeldSkelObject objs[objIndex].parent &linkto
				)
				gRsUlog.LogMessage ("link dummy "+formerSkinObj[objIndex] as string+" to:"+linkto as string)
				if formerSkinObj[objIndex] != linkto then
					formerSkinObj[objIndex].parent = linkto
				local boneWeights  = #((1.0))
				
				for k=1 to objs[objIndex].numverts do
				(
					append vertexBones thebones
					append vertexWeights boneWeights
				)
 				if undefined!=gSkinWeldPerObjCalback then
				(
					for c in objs[objIndex].children do
						gSkinWeldPerObjCalback c formerSkinObj[objIndex]
				)
			)
		)
		-- set vertexWeights
		select compObj
		modpanel.setCurrentObject theSkin
		completeRedraw()
		gRsUlog.LogMessage ("next process count:"+(skinOps.GetNumberVertices theSkin) as string)
		gSkinWeldProgresssDialog.Controls.Item[1].text = ("Copying vertexWeights into new object...")
		for k=1 to (skinOps.GetNumberVertices theSkin) do
		(
			skinops.ReplaceVertexWeights theSkin k vertexBones[k] vertexWeights[k]
			
			-- auto normal all verts
			skinOps.unNormalizeVertex theSkin k false
			increaseProgress()
		)

-------------------------------------------------------------------------------------------------------
-- We have to make the bones meshes for the map exporter to export it.
-------------------------------------------------------------------------------------------------------
		select compObj
		if undefined!=firstGtaObject then
		(
			gRsUlog.LogMessage ("copying attributes from "+firstGtaObject.name)
			setAttr compObj groupIdx (getAttr firstGtaObject groupIdx)
			setAttr compObj txdIdx (getAttr firstGtaObject txdIdx)
			setAttr compObj stateIdx (getAttr firstGtaObject stateIdx)
			setAttr compObj animIdx true
		)

		theSkin.alwaysDeform = on
	)
-- 	catch
-- 	(
-- 		print (getCurrentException())
-- 		gSkinWeldProgresssDialog.Close()
-- 		return false
-- 	)
	gSkinWeldProgresssDialog.Close()
	return true
)

fn weldSelection name:undefined skelRoot:undefined perObjCallback:undefined= 
(
	gSkinWeldSkelObject = skelRoot
	gSkinWeldPerObjCalback = perObjCallback
	gNewName = name
	
	currentSelection = (selection as array)
	if not weldSkinnedObjects currentSelection then
	(
		return false
	)
	local allNodesToDelete = for n in currentSelection collect (n as string+", ")
	gRsUlog.LogMessage ("deleting "+allNodesToDelete as string)
	delete currentSelection
	
	gSkinWeldSkelObject = undefined
	gSkinWeldPerObjCalback = undefined

	return true
)