fn RsRootReplace = 
(
	for o in selection do
	(
		local bb = nodeGetBoundingBox o (matrix3 1)
		bb = bb[2] - bb[1]
		local newRoot = dummy boxsize:[bb.x, bb.y, bb.z] name:o.name transform:o.transform
		newRoot.parent = o.parent
		for c in o.children do c.parent = newRoot
		delete o
	)
)