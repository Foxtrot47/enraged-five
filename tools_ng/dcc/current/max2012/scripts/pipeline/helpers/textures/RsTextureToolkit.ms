filein "pipeline/util/RsUniversalLog.ms"
filein ( RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetUIOps.ms" )

global gTextureToolkitLogFile = ( RsConfigGetLogDir() + "textureToolkit.ulog" )
global gRsTextureToolkitULog = sRsULog()
	

-- Represents a texture object for the toolkit.  Keeps pointers to which objects and materials the 
-- texture is used.  Also stores rename settings.
struct RsTextureToolkitObject
(
	private
	
	VAR_TYPE_TEXTURE = "texmap",
	
	
	public
	
	-- The filename found on the material.
	filename 				= undefined,
	
	-- Whether or not the texture exists on the local machine.
	existsLocally			= false,
	
	-- is an alpha texture
	isAlpha = false,
	
	-- Rename settings.  This allows a user to go through and setup unique settings
	-- for each texture, and then process them all at once.
	doRenameFile			= false,
	newFilename				= undefined,
	prefix					= "",
	suffix					= "",
	removeFirstNumChars		= 0,
	removeLastNumChars		= 0,
	
	-- Pointers
	materials				= #(),
	objs					= #(),
	
	-- Choosing MultipleFiles for editing large amounts of textures
	MultipleFiles					= false,
	
	removeOldFile 					= true,
	
	-- Functions
	
	fn clearRenameSettings = (
		this.doRenameFile			= false
		this.newFilename			= undefined
		this.prefix					= ""
		this.suffix					= ""
		this.removeFirstNumChars	= 0
		this.removeLastNumChars		= 0
	),
	
	fn getRenamedFile = (
		local oldTextureName = toLower this.filename
		local newTextureName = undefined
		
		if this.newFilename == undefined then (
			newTextureName = oldTextureName
			
		) else (
			newTextureName = toLower this.newFilename
		)
		
		local justPath = getFilenamePath newTextureName
		local justFilename = getFilenameFile newTextureName
		local justExtension = getFilenameType newTextureName
		
		-- Add prefix, suffix, remove first and last characters.
		if this.removeFirstNumChars > 0 then (
			justFilename = substring justFilename ( removeFirstNumChars + 1 ) justFilename.count
		)
		
		if this.removeLastNumChars > 0 then (
			justFilename = substring justFilename 1 ( justFilename.count - ( removeLastNumChars + 1 ) )
		)
		
		if this.prefix != "" then (
			justFilename = this.prefix + justFilename
		)
		
		if this.suffix != "" then (
			justFilename += this.suffix
		)
		
		-- Reconstruct name.
		newTextureName = justPath + justFilename + justExtension
		
		if ( getFilenamePath newTextureName ) == "" then (
			newTextureName = ( getFilenamePath oldTextureName ) + newTextureName
		)
		
		newTextureName
	),
	
	
	fn getRenamedFileMultipleFiles = (
		newFileName		
	),

	
	-- When called, will update all of the materials for this texture object to use whatever
	-- filename is supplied.
	fn updateAllMaterials newTextureFilename = (
		local result = true
		
		for mtl in materials do (
			
			local isAlphaShader = RstGetIsAlphaShader mtl
			
			local numVars = RstGetVariableCount mtl
			
			for varIdx = 1 to numVars do (
				local varType = RstGetVariableType mtl varIdx
				local varName = RstGetVariableName mtl varIdx
				
				if varType == this.VAR_TYPE_TEXTURE then (
					
					if not this.isAlpha then 
					(
					
						local val = RstGetVariable mtl varIdx
						
						if ( toLower ( filenameFromPath val ) ) == ( toLower ( filenameFromPath filename ) ) then (
							if ( RstSetVariable mtl varIdx newTextureFilename ) == true then (
								local msg = "Set shader variable (" + varName + ") on material (" + mtl as string + ") to use texture (" + newTextureFilename + ")."   
								gRsTextureToolkitULog.logMessage msg
								
							) else (
								local msg = "An error occurred while attempting to set shader variable (" + varName + ") on material (" + mtl as string + ") to use texture (" + newTextureFilename + ")."   
								gRsTextureToolkitULog.logError msg
								
								result = false
							)
						)
						
					) 
					else if isAlphaShader then 
					(
						
						
						local val = RstGetAlphaTextureName mtl varIdx	
						if val == undefined then format"No alpha for % : %\n" filename varIdx
						if filename != undefined and val != undefined then 
						(
							if ( toLower ( filenameFromPath val ) ) == ( toLower ( filenameFromPath filename ) ) then (
								if ( RstSetAlphaTextureName mtl varIdx newTextureFilename ) == true then (
									local msg = "Set shader variable (" + varName + ") on material (" + mtl as string + ") to use texture (" + newTextureFilename + ")."   
									gRsTextureToolkitULog.logMessage msg
									
								) else (
									local msg = "An error occurred while attempting to set shader variable (" + varName + ") on material (" + mtl as string + ") to use texture (" + newTextureFilename + ")."   
									gRsTextureToolkitULog.logError msg
									
									result = false
								)
							)
						)

					)
				)
			)
		)
		
		this.filename = newTextureFilename
		
		result
	),	

	fn rename usePerforce repathOnly newChangelist backUp:false = (

		local result = false
		
		if this.doRenameFile == true then (

			-- Lower to be safe.
			local oldTextureName = toLower this.filename
			local newTextureName = ""
			if this.MultipleFiles then (
				
				newTextureName = this.getRenamedFileMultipleFiles() 
			) else newTextureName = this.getRenamedFile()
		
			local proceed = true
			local tryAddEditPerforce = false
			-- The texture doesn't exist locally.
			if not this.existsLocally then (
				gRsTextureToolkitULog.logError ( "Could not find the texture (" + oldTextureName + ") on the local harddrive to rename!" )
				
				-- Attempt to sync the file from Perforce.
				if usePerforce then (
					
					gRsPerforce.sync oldTextureName force:true
					
				)
				
				proceed = true

			)
			
			if proceed then (
				local renameSuccessful = false
		
				-- Perforce Rename
				if usePerforce == true then (
						
					local oldDepotTextureName = gRsPerforce.local2depot oldTextureName
					local newDepotTextureName = gRsPerforce.local2depot newTextureName

					if this.removeOldFile then (
						
						if ( gRsPerforce.edit oldDepotTextureName ) then (
						
							
							if ( gRsPerforce.run "rename" #( oldDepotTextureName, newDepotTextureName ) switches:#( "-c", newChangelist as string ) ) then (
								renameSuccessful = true
								
							) else (
								gRsTextureToolkitULog.logError ( "A unknown Perforce error occurred while trying to rename (" + oldTextureName + ")!" )
							)
						) else   (

							-- Check to see if the directory exists.  If it doesn't, then we need to create it first.
							local newTextureDir = getDirectories ( getFilenamePath newTextureName )
							
							if newTextureDir.count == 0 then (
								makeDir ( getFilenamePath newTextureName )
							)

							-- Rename and potentially move the file.
							result = renameFile oldTextureName newTextureName
							if result == true then (
								
								print "Trying Perforce Add"					
								result = gRsPerforce.add_or_edit newTextureName silent:true switches:#("-c", newchangelist as string)
								
							) else (
								print "Couldn't rename file"
								gRsTextureToolkitULog.logError ( "Could not rename \"" +oldTextureName+"\" to \"" + newTextureName + "\"! The texture\""+newTextureName +"\" may already Exist." )
								
							)
							
							-- Update the texture object to use the new texture.
							if result == true then (
								renameSuccessful = true
							)
						
						
						)
					
				) else (
						
							-- Check to see if the directory exists.  If it doesn't, then we need to create it first.
							local newTextureDir = getDirectories ( getFilenamePath newTextureName )
							local oldTextureDir = getDirectories ( getFilenamePath oldTextureName ) 
														
							if newTextureDir.count == 0 then (
								makeDir ( getFilenamePath newTextureName )
							)

							local uniqueDir = false 					
							if ( getFilenamePath newTextureName ) != ( getFilenamePath oldTextureName )  then uniqueDir = true
							format "Is Unique Directory = %\n" uniqueDir
								
								
							if backUp != false and uniqueDir == false then (
								
								print "Trying Perforce Add"					
								result = gRsPerforce.add_or_edit oldTextureName silent:true switches:#("-c", newchangelist as string)
								
								if result then (

									grsperforce.run "shelve" oldTextureName switches:#("-c", newchangelist as string, "-f")
									grsperforce.revert oldTextureName cl:newchangelist
								)		
								
							)

							-- Rename and potentially move the file.
							if uniqueDir == false then (
								result = renameFile oldTextureName newTextureName
							) else (
								
								result = copyFile oldTextureName newTextureName
								
							)
							
							
							if result == true then (
								
								print "Trying Perforce Add"					
								result = gRsPerforce.add_or_edit newTextureName silent:true switches:#("-c", newchangelist as string)
								
							) else (
								print "Couldn't rename file"
								gRsTextureToolkitULog.logError ( "Could not rename \"" +oldTextureName+"\" to \"" + newTextureName + "\"! The texture\""+newTextureName +"\" may already Exist." )
								
							)
							
							-- Update the texture object to use the new texture.
							if result == true then (
								renameSuccessful = true
							)
						
						
				)
					
				-- Local Rename
				) else (

					if not repathOnly then (
						-- Check to see if the directory exists.  If it doesn't, then we need to create it first.
						local newTextureDir = getDirectories ( getFilenamePath newTextureName )
						
						if newTextureDir.count == 0 then (
							makeDir ( getFilenamePath newTextureName )
						)

						-- Rename and potentially move the file.
						result = renameFile oldTextureName newTextureName
						
						-- Update the texture object to use the new texture.
						if result == true then (
							renameSuccessful = true
						)
					) else (
						if doesfileexist newTextureName then (
							format "\nOnly Repathing:% to:%\n" oldTextureName newTextureName
							renameSuccessful = true
						) else (							
							msg = ("Repath Failed. \""+newTextureName+"\" could not be found")							
							gRsTextureToolkitULog.logError msg
							renameSuccessful = false
							
							
						)
						
						
					)
					

				)
				
				-- Update all materials to point to the new texture.
				if renameSuccessful == true then (
					if ( updateAllMaterials newTextureName ) then (
						result = true
						gRsTextureToolkitULog.logMessage ( "Successfully renamed and updated materials to use (" + oldTextureName + ") to (" + newTextureName + ")." )
						
					) else (
						result = false
						gRsTextureToolkitULog.logError "An error occurred while trying to update the materials!  Please revert your 3dsmax file back to the state it was before you attempted to rename a texture."
					)
				)
			)
		)
		
		if result == true then (
			this.clearRenameSettings()
		)
		
		result
	)
)


struct RsTextureToolkitStruct
(
	private
	
	-- Rage Shader variable types.
	VAR_TYPE_TEXTURE = "texmap",
	tintPaletteDir = RsConfigGetArtDir()+"Textures/tint_palettes/",
	
	public
	-- Store the collected textures.
	textures = dotNetObject "System.Collections.Hashtable",
	errorTextures = dotNetObject "System.Collections.Hashtable",
	
	texturesArray = #(),
	errorTexturesArray = #(),
	displayTexturesArray = #(),
	
	MultipleFiles = false,
	doErrors = false,
	refreshNoMissingTextureErrors = false,
	DisplayingDirtyFiles = False,
	
	-- Functions
	private
	fn clearAll = (
		this.textures.clear()
		this.errorTextures.clear()
		gc light:true
	),
	
	public
	fn getTextureObj textureName errorTextures:False = (
		local textureObj = undefined
		if this.doErrors then errorTextures = true
		if  errorTextures then (
			
			if ( this.errorTextures.Contains textureName ) then (
				textureObj = this.errorTextures.Item[ textureName ].value
			)
			
		) else (
			
			if ( this.textures.Contains textureName ) then (
				textureObj = this.textures.Item[ textureName ].value
			)
			
		)
			
		textureObj
	),
	
	private
	fn collectTexturesFromMaterial mat obj = (
		
		if classof obj != RSrefObject then (
			
			local errorArray = #()
			
			if ( classof mat ) == Rage_Shader then (
				
				local isAlphaShader = RstGetIsAlphaShader mat
				local numVars = RstGetVariableCount mat
				
				for varIdx = 1 to numVars do 
				(
					-- get the variable type
					local varType = RstGetVariableType mat varIdx
					
					-- The variable is a type  texture map 
					if varType == this.VAR_TYPE_TEXTURE then 
					(
						local val = ""
						try 
						(
							-- Get the filename being used
							local val = RstGetVariable mat varIdx
						)
						catch ( val = "") 
					
						-- If it's not a empty slot then....
						if val != "" then 
						(
							-- get just the name
							textureName = toLower ( filenameFromPath val )
							
							-- See if the texture is on disk.
							local textureExistsLocally = false			
							if ( getFileSize val ) != 0 then 
							(
								textureExistsLocally = true
							)
							
							-- If it doesnt exist on disk then....
							if not textureExistsLocally then 
							(
								local msg = "A texture named (" + textureName + ") could not be found!  The texture is used on object (" + obj.name + ") on material (" + mat.name + ")."
								appendIfUnique errorArray msg

								
								-- Is it a tint Palette?
								local tintPal= false			
								local checkPath = substring  (getfilenamepath val) 1 this.tintPaletteDir.count
								if (tolower (rsmakesafeslashes (checkPath))) == (tolower (rsmakesafeslashes this.tintPaletteDir)) then tintPal = true

								
								--------- Collecting Error Textures 
								-- Texture does not yet exist in the hashtable, so create a new one and add it.
								if not ( this.errorTextures.Contains textureName ) and not tintPal then 
								(
									local textureObj = RsTextureToolkitObject()
									
									textureObj.filename = ("\\UNDEFINED\\" +toLower textureName)									
									textureObj.existsLocally = textureExistsLocally
									if this.MultipleFiles then textureObj.newfilename =  ("\\***Undefined***\\" +toLower textureName)		
									append textureObj.materials mat
									append textureObj.objs obj
									
									this.errorTextures.Add textureName ( dotNetMXSValue textureObj )
									
								-- Texture already exists, so update its information.
								) 
								else 
								(
									local textureObj = this.getTextureObj textureName errorTexture:true

									if textureObj != undefined then 
									(
										appendIfUnique textureObj.materials mat
										appendIfUnique textureObj.objs obj
										
									)
								)
							) 
							-- Texture does exist locally so.....
							else 
							(								
								-- check to see is it a tint Palette?
								local tintPal= false									
								local checkPath = substring  (getfilenamepath val) 1 this.tintPaletteDir.count
								if (tolower (rsmakesafeslashes (checkPath))) == (tolower (rsmakesafeslashes this.tintPaletteDir)) then tintPal = true

								-- Its not a tintpal and needs to be added to the hashtable.
								-- Texture does not yet exist in the hashtable, so create a new one and add it.
								if not ( this.textures.Contains textureName ) and not tintPal then
								(
									local textureObj = RsTextureToolkitObject()
									
									textureObj.filename = toLower val
									textureObj.existsLocally = textureExistsLocally
									if this.MultipleFiles then textureObj.newfilename =  toLower val
									append textureObj.materials mat
									append textureObj.objs obj
									
									this.textures.Add textureName ( dotNetMXSValue textureObj )
								)
								-- Texture already exists, so update its information.
								else 
								(
									local textureObj = this.getTextureObj textureName

									if textureObj != undefined then 
									(
										appendIfUnique textureObj.materials mat
										appendIfUnique textureObj.objs obj
									)
								)
							)
							
							-- Let's check for Alpha Textures
							if  isAlphaShader then
							(								
								-- Get the Alpha filename being used
								local alphaVal = RstGetAlphaTextureName mat varIdx
							
								-- If it's not an empty slot then....
								if alphaVal != "" and  alphaVal != undefined then 
								(
									-- get just the name
									try (alphaTextureName = toLower ( filenameFromPath alphaVal ) )
									catch ( 
										messagebox (getcurrentexception())

										)
									
									-- See if the texture is on disk.
									local textureExistsLocally = false			
									if ( getFileSize alphaVal ) != 0 then 
									(
										textureExistsLocally = true
									)
									
									-- If it doesnt exist on disk then....
									if not textureExistsLocally then 
									(
										local msg = "A texture named (" + alphaTextureName + ") could not be found!  The texture is used on object (" + obj.name + ") on material (" + mat.name + ")."
										appendIfUnique errorArray msg

										--------- Collecting Error Textures 
										-- Texture does not yet exist in the hashtable, so create a new one and add it.
										if not ( this.errorTextures.Contains alphaTextureName ) and not tintPal then 
										(
											local textureObj = RsTextureToolkitObject()
											
											textureObj.filename = ("\\UNDEFINED\\" +toLower alphaTextureName)	
											textureObj.isAlpha = true											
											textureObj.existsLocally = textureExistsLocally
											if this.MultipleFiles then textureObj.newfilename =  ("\\***Undefined***\\" +toLower alphaTextureName)		
											append textureObj.materials mat
											append textureObj.objs obj
											
											this.errorTextures.Add alphaTextureName ( dotNetMXSValue textureObj )
											
										-- Texture already exists, so update its information.
										) 
										else 
										(
											local textureObj = this.getTextureObj alphaTextureName errorTexture:true

											if textureObj != undefined then 
											(
												textureObj.isAlpha = true
												appendIfUnique textureObj.materials mat
												appendIfUnique textureObj.objs obj
												
											)
										)
									) 
									-- Texture does exist locally so.....
									else 
									(		
										-- Texture does not yet exist in the hashtable, so create a new one and add it.
										if not ( this.textures.Contains alphaTextureName )  then
										(
											local textureObj = RsTextureToolkitObject()
											
											textureObj.filename = toLower alphaVal
											textureObj.isAlpha = true
											textureObj.existsLocally = textureExistsLocally
											if this.MultipleFiles then textureObj.newfilename =  toLower alphaVal
											append textureObj.materials mat
											append textureObj.objs obj
											
											this.textures.Add alphaTextureName ( dotNetMXSValue textureObj )
										)
										-- Texture already exists, so update its information.
										else 
										(
											local textureObj = this.getTextureObj alphaTextureName

											if textureObj != undefined then 
											(
												textureObj.isAlpha = true
												appendIfUnique textureObj.materials mat
												appendIfUnique textureObj.objs obj
											)
										)
									)
									
								)

							)
						)
					)
				)
			)
			
			if this.refreshNoMissingTextureErrors == false then (
				for error in errorArray do gRsTextureToolkitULog.LogError error context:obj
			)					
			
		)
	),
	
	fn collectTexturesFromObject obj = (
		
		if classof obj != RSrefObject then (
		
			local mat = obj.material
			
			if mat != undefined then (
				if ( classof mat ) == Multimaterial then (
					for subMtl in mat.materialList do (
						if subMtl != undefined then (
							collectTexturesFromMaterial subMtl obj
						)
					)
					
				) else if ( classof mat ) == Rage_Shader then (
					collectTexturesFromMaterial mat obj
				)
			)		
		)	
	),
	
	
	public
	
	p4Changelist = undefined,
	
	-- Get a RsTextureToolkitObject for a particular texture.
	fn getTexture textureName = (
		( this.getTextureObj textureName )
	),
	
	-- Return all materials using a texture.
	fn getMaterialsUsingTexture textureName = (
		local textureObj = this.getTextureObj textureName
		local mats = undefined
		
		if textureObj != undefined then (
			mats = textureObj.materials
		)
		
		mats
	),
	
	-- Return all objects using a texture.
	fn getObjectsUsingTexture textureName = (
		local textureObj = this.getTextureObj textureName
		local objs = undefined
		
		if textureObj != undefined then (
			objs = textureObj.objs
		)
		
		objs
	),
	
	-- Return all textures in the hashtable as a flat list.
	fn getTexturesList errorTextures:false alphaTextures:false = (
		
		if this.doErrors then errorTextures = true
		local texturesList = #()
		local enum = undefined
		
		if errorTextures then (			
			enum = this.errorTextures.GetEnumerator() 
		) else (		
			enum = this.textures.GetEnumerator()
			
		)
		
		while ( enum.MoveNext() ) do (
			
			if alphaTextures then 
			(
				if (this.textures.Item[ enum.Key ].value).isAlpha == true then
				(
					 append texturesList enum.Key
				)
				
			)
			else append texturesList enum.Key
		)
		
		sort texturesList
		
		texturesList
		
	),
	
	fn genChangeList msg:undefined = (
		
		if msg == undefined then msg = "Rename Textures"
		msg = (msg + "	[Auto-Generated]")
		local newChangelist = gRsPerforce.createChangelist msg
		this.p4Changelist = newChangelist
		newChangelist
		
	),
	
	
	-- Rename a texture based on its settings.
	fn renameTexture textureName usePerforce repathOnly newChangeList backUp:false = (
		local result = false
		local textureObj = this.getTextureObj textureName
		
		-- Have to redirect usePerforce to this local scope for some reason.  If passed directly to the texture object,
		-- the texture object is receiving undefined.  Not clear why yet.
		local doUsePerforce = usePerforce
		
		if textureObj != undefined then (
			
			-- If any of the rename settings on the texture object are set, then
			-- that texture will be renamed according to its settings.
			result = textureObj.rename doUsePerforce repathOnly newChangeList backUp:backUp
			
			if result == true then (
				
				-- Update the hashtable of textures to point to the renamed texture.
				this.textures.Remove textureName
				try (this.textures.Add ( toLower ( filenameFromPath textureObj.filename ) ) ( dotNetMXSValue textureObj ))
					catch()
			)
		)

		result
	),
	
	-- Rename all textures that have been flagged to be renamed.
	fn renameAllTextures usePerforce repathOnly newChangeList texturesList:undefined backUp:false = (	
		
		if not usePerforce then print "Repathing Only" 
			
		if texturesList == undefined then texturesList = this.getTexturesList()
		local result = false
		
		-- Have to redirect usePerforce to this local scope for some reason.  If passed directly to the texture object,
		-- the texture object is receiving undefined.  Not clear why yet.
		local doUsePerforce = usePerforce	
		
		for textureName in texturesList do (
			result = this.renameTexture textureName doUsePerforce repathOnly newChangeList backUp:backUp
		) 
		
		result = true
	),
	
	-- Make list of texture-names for display in UI:
	fn makeDisplayArray DisplayMode textureArray: displayOnly:false errorTextures:false SelObjs:False = 
	(
		--format "makeDisplayArray % textureArray:% displayOnly:% errorTextures:% SelObjs:%\n  " DisplayMode (textureArray as string) displayOnly errorTextures SelObjs
		--gRsDebugStack.PrintCallLine()
		
		local OutputArray = #()
		DisplayTexturesArray.Count = 0
		
		DisplayingDirtyFiles = False
		
		if (this.doErrors) do 
		(
			errorTextures = true
		)
 		if (textureArray == unsupplied) do 
		(
			textureArray = this.texturesArray
		)
		
		for tx in textureArray do 
		(
			local TexObj = this.getTextureObj tx errorTextures:errorTextures
			
			-- Only show texture if one of its objects is selected, if 'SelObjs' is turned on:
			local ShowTex = (not SelObjs)
			for Obj in TexObj.Objs while (not ShowTex) do (ShowTex = Obj.isSelected)
			
			if ShowTex do 
			(
				local toAppend = case DisplayMode of 
				(
					-- New filenames: (no paths) - same as old names if unaltered...
					#NewNames: (filenameFromPath TexObj.newfilename)
					
					-- New paths:
					#NewPaths: (TexObj.newfilename)
					
					-- Current filenames: (no paths)
					#OldNames: (filenameFromPath TexObj.fileName)
				)
				
				-- Convert value is string, if required:
				toAppend = (toAppend as string)
				
				-- Append asterisk if file is marked for rename: 
				--	(only if list is going to be shown in UI)
				if (DisplayOnly and TexObj.doRenameFile and (not matchPattern toAppend pattern:"* \*")) do 
				(
					DisplayingDirtyFiles = True
					toAppend = (toAppend + " *")
				)
				
				append OutputArray toAppend
				append DisplayTexturesArray TexObj
			)
		)
			
		return OutputArray
	),
	
	fn removePrefixTexOBj selectedArray thePrefixs:#("") newPrefix:"" = (
		
		if newPrefix != "" then (
			local theTexObjs = for tx in selectedArray collect (this.getTextureObj tx)		
			for tx in theTexObjs do (
				tx.newFileName = tx.fileName
				for prfx in thePrefixs do (
					
					local hasPrefix = findstring (tolower (getfilenamefile tx.fileName)) (tolower prfx)
					if hasPrefix == 1 then (
						
						local filePath = getfilenamepath tx.newFileName
						local replaceText = (getfilenamefile tx.fileName)
						local filetype = getfilenametype tx.newFileName	
						
						local newText = replace (tolower replaceText) 1 prfx.count  (tolower newPrefix)
						
						tx.newFileName = filePath + newText + filetype
						tx.doRenameFile = true
					)
					
				)
			)
			
		)		
	),
	
	fn changePathTexOBj selectedArray newPath:"" = (
		
			local theTexObjs = for tx in selectedArray collect (this.getTextureObj tx)		
		
			for tx in theTexObjs do (
				
				tx.newFilename = (getfilenamepath tx.filename + getfilenamefile tx.newfilename + getfilenametype tx.filename)
				if newPath != "" then (
					tx.newFilename = ( newPath + getfilenamefile tx.newfilename + getfilenametype tx.filename)
					tx.doRenameFile = true
				)		
			)	
		
	),
	
	fn getTexObjs = (
		
		local theTextures = #()
		
		for tx in this.getTexturesList() do (
			
			append theTextures (this.getTextureObj tx)			
		)	

		theTextures		
	),
	
	fn update = (
		
		-- Clear out the existing hashtable data.
		this.clearAll()
		
		local objs = objects as array
		
		for obj in objs do (
			this.collectTexturesFromObject obj
		)
	),
	
	on create do (
		gRsTextureToolkitULog.Init "Texture Toolkit" appendToFile:false forceLogFile:gTextureToolkitLogFile
	)
)
--clearlistener()
