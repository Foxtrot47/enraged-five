--
-- File:: pipeline/helpers/models/decalCutter.ms
-- Description:: Tool to allow user to draw outline for a decal object, which is then copied and cut out from selected geometry
--
-- Author:: Neal D Corbett <neal.corbett@rockstarleeds.com>
-- Date:: 12 October 2010
--

unregisterRedrawViewsCallback RSdecalCutDraw
global RSdecalCutDrawPoints
global RSdecalCutColour = green

fn RSdecalCutDraw = 
(
	local pointCount = RSdecalCutDrawPoints.count
	if pointCount > 0 do 
	(
		gw.setColor #line RSdecalCutColour
		gw.wPolyline RSdecalCutDrawPoints true
	)
)

try (destroyDialog RSdecalCutterSettingsRoll) catch ()
rollout RSdecalCutterSettingsRoll "" width:130
(
	label decalLabel "Cutting out decal:\n  Click outline points\n  Right-click to finish" align:#left height:40 offset:[-4,0]
)

tool RSdecalCutterTool 
(
	local sourceObjs
	local sourcePivot
	
	local offPlane
	local viewHelper
	
	local firstClick
	local clickRays = #()
	
	-- Returns false if one of the selected decal-source objects has been deleted
	fn validSrcObjs = 
	(
		local validObjs = (sourceObjs.count != 0)
		
		for obj in sourceObjs 
		while validObjs 
		where (not isValidNode obj)
		do 
		(
			validObjs = false
		)
		
		validObjs
	)
	
	fn getIntersect obj = 
	(
		-- Intersect view-ray with plane at pivot
		local pointRay = mapScreenToWorldRay viewPoint
		local intersectPos = [0,0,0]
		offPlane.intersect pointRay &intersectPos
		ray intersectPos pointRay.dir
	)
	
	-- Function to find the maximum radius of object (+1 for good measure)
	fn getMaxRadius = 
	(
		local maxRadius = 0
		
		for obj in sourceObjs do 
		(
			local objCorners = 
			#(
				[obj.min.x, obj.min.y, obj.min.z],	[obj.min.x, obj.min.y, obj.max.z],
				[obj.min.x, obj.max.y, obj.min.z], [obj.min.x, obj.max.y, obj.max.z],
				[obj.max.x, obj.min.y, obj.min.z], [obj.max.x, obj.min.y, obj.max.z],
				[obj.max.x, obj.max.y, obj.min.z], [obj.max.x, obj.max.y, obj.max.z]
			)
			
			local cornerDist
			for objCorner in objCorners do 
			(
				cornerDist = distance sourcePivot objCorner
				if (cornerDist > maxRadius) do 
				(
					maxRadius = cornerDist
				)
			)
		)		

		-- Increase radius a little
		maxRadius + 1
	)
	
	fn displayPromptRoll = 
	(
		if not RSdecalCutterSettingsRoll.open do 
		(
			createDialog RSdecalCutterSettingsRoll style:#() --bgcolor:((colorMan.getColor #toolTipBackground) * white)
		)
		
		setDialogPos RSdecalCutterSettingsRoll (mouse.screenpos + [12,-12 - RSdecalCutterSettingsRoll.height])
	)
	
	on start do 
	(
		sourceObjs = for obj in selection where (isKindOf obj Editable_Poly) or (isKindOf obj Editable_mesh) collect obj
		
		if sourceObjs.count != 0 then 
		(			
			replacePrompt "Decal Cutter: Draw decal outline"

			undo off 
			(
				RSdecalCutDrawPoints = #()
				registerRedrawViewsCallback RSdecalCutDraw
				
				sourcePivot = [0,0,0]
				for obj in sourceObjs do (sourcePivot += obj.pos)
				sourcePivot /= sourceObjs.count
			)
		)
		else 
		(
			messageBox "No valid object selected" title:"Decal Cutter Error"
			#stop
		)
	)
	
	on mousePoint clickNum do 
	(
		if (clickNum == 1) then 
		(
			local coordSysTM = Inverse(getViewTM())
			local viewDir = -coordSysTM.row3
			offPlane = manip.makePlaneFromNormal viewDir sourcePivot
		)
		else 
		(
			local intersect = getIntersect sourceObj
			
			if intersect != undefined do 
			(				
				if (clickNum > 1) and (firstClick == undefined) then 
				(
					firstClick = viewPoint
					
					local curPoint = [viewPoint.x, viewPoint.y, 0]
					RSdecalCutDrawPoints[1] = curPoint
					RSdecalCutDrawPoints[2] = curPoint
				)

				append clickRays intersect
				
				local curPoint = [viewPoint.x, viewPoint.y, 0]
				RSdecalCutDrawPoints[RSdecalCutDrawPoints.count] = curPoint
				append RSdecalCutDrawPoints curPoint
			)
		)
	)

	on freeMove do 
	(
		if (validSrcObjs()) then 
		(
			displayPromptRoll()
		)
		else 
		(
			-- Cancel if sourceObj is deleted
			#stop
		)
	)
	
	on mouseMove clickNum do 
	(
		if (validSrcObjs()) then 
		(
			local pointCount = RSdecalCutDrawPoints.count
			if (pointCount > 0) do 
			(
				RSdecalCutDrawPoints[pointCount] = [viewPoint.x, viewPoint.y, 0]
				completeRedraw()
			)
			displayPromptRoll()
		)
		else 
		(
			-- Cancel if sourceObj is deleted
			#stop
		)
	)
	
	on stop do 
	(
		destroyDialog RSdecalCutterSettingsRoll
		unregisterRedrawViewsCallback RSdecalCutDraw
		popPrompt()
		
		-- Do the decal-cutting if the source-objects are still valid:
		if (validSrcObjs()) do 
		(
			local rayCount = clickRays.count
			if (rayCount == 0) do return false
			
			suspendEditing()
			local viewTM = inverse (viewport.getTM())
				
			-- Create cutter-object:
			local cutterObj = Editable_Mesh pos:sourcePivot
			convertToPoly cutterObj
			
			local maxRadius = getMaxRadius()
			for ray in clickRays do (polyop.createVert cutterObj (ray.pos - maxRadius * ray.dir))
			for ray in clickRays do (polyop.createVert cutterObj (ray.pos + maxRadius * ray.dir))
			
			local faceVertsA = (for n = 1 to clickRays.count collect n)
			local faceVertsB = (for n = (polyOp.getNumVerts cutterObj) to (clickRays.count + 1) by -1 collect n)
			polyop.createPolygon cutterObj faceVertsA
			polyop.createPolygon cutterObj faceVertsB
			
			local otherVert = rayCount
			for n = 1 to (clickRays.count - 1) do 
			(
				otherVert += 1
				polyop.createPolygon cutterObj #(n, n + 1, otherVert + 1, otherVert)
			)
			polyop.createPolygon cutterObj #(rayCount, 1, rayCount + 1, rayCount * 2)
			
			-- If the cutter is inside-out, flip its normals:
			if ((in coordsys viewTM polyOp.getFaceNormal cutterObj 1).z <= 0) do 
			(
				addModifier cutterObj (Normalmodifier flip:on)
				collapseStack cutterObj
			)
			
			-- Make copy of source-object, then remove its back-faces
			local decalObj = Editable_Mesh pos:sourcePivot
			convertToPoly decalObj
			for obj in sourceObjs do 
			(
				polyop.attach decalObj (copy obj)
			)
			
			local decalObjName = ""
			for obj in sourceObjs do (append decalObjName (obj.name + "_"))
			decalObjName = uniqueName (decalObjName + "decal")
			decalObj.name = decalObjName
			
			local backFaces = #{}
			for n = 1 to (polyOp.getNumFaces decalObj) do 
			(
				backFaces[n] = ((in coordsys viewTM polyOp.getFaceNormal decalObj n).z <= 0)
			)
			polyop.deleteFaces decalObj backFaces
			
			-- Boolean cutter-object with object-copy to create decal
			-- Scale the objects up first to avoid excessive welding
			scale decalObj [10,10,10]
			scale cutterObj [10,10,10]
			decalObj * cutterObj
			scale decalObj [0.1,0.1,0.1]
			delete cutterObj
			
			if (decalObj.numFaces == 0) then 
			(
				-- Delete decalObj if no faces intersected with cutter
				delete decalObj
			)
			else 
			(
				-- Push the decal object out by 2mm
				addModifier decalObj (push Push_Value:0.002)
				collapseStack decalObj
				
				select decalObj
			)
			resumeEditing()
		)
	)
)
	
startTool RSdecalCutterTool

