--
-- File:: pipeline/helpers/models/modelSlicer.ms
-- Description:: Tool to allow user to draw outline for a decal object, which is then copied and cut out from selected geometry
--
-- Author:: Neal D Corbett <neal.corbett@rockstarleeds.com>
-- Date:: 1 June 2012
--

unregisterRedrawViewsCallback RsModelSliceDraw
global RsModelSliceDrawPoints
global RsModelSliceColour = green

fn RsModelSliceDraw = 
(
	local pointCount = RsModelSliceDrawPoints.count
	if pointCount > 0 do 
	(
		gw.setColor #line RsModelSliceColour
		gw.wPolyline RsModelSliceDrawPoints true
	)
)

try (destroyDialog RsModelSlicerSettingsRoll) catch ()
rollout RsModelSlicerSettingsRoll "" width:130
(
	label lblHelp "Slicing up mesh:\n  Click outline points\n  Right-click to finish" align:#left height:40 offset:[-4,0]
)

tool RsModelSlicerTool prompt:"Decal Cutter: Draw decal outline"
(
	local sourceObjs = #()
	local sourcePivot
	
	local offPlane
	local viewHelper
	
	local firstClick
	local clickRays = #()
	
	local chopToObjs = true
	local randomColours = true
	
	-- Returns false if one of the selected decal-source objects has been deleted
	fn validSrcObjs = 
	(
		local validObjs = (sourceObjs.count != 0)
		
		for obj in sourceObjs while validObjs where (not isValidNode obj) do 
		(
			validObjs = false
		)
		
		validObjs
	)
	
	fn getIntersect obj = 
	(
		-- Intersect view-ray with plane at pivot
		local pointRay = mapScreenToWorldRay viewPoint
		local intersectPos = [0,0,0]
		offPlane.intersect pointRay &intersectPos
		ray intersectPos pointRay.dir
	)
	
	-- Function to find the maximum radius of object (+1 for good measure)
	fn getMaxRadius = 
	(
		local maxRadius = 0
		
		for obj in sourceObjs do 
		(
			local objCorners = 
			#(
				[obj.min.x, obj.min.y, obj.min.z],	[obj.min.x, obj.min.y, obj.max.z],
				[obj.min.x, obj.max.y, obj.min.z], [obj.min.x, obj.max.y, obj.max.z],
				[obj.max.x, obj.min.y, obj.min.z], [obj.max.x, obj.min.y, obj.max.z],
				[obj.max.x, obj.max.y, obj.min.z], [obj.max.x, obj.max.y, obj.max.z]
			)
			
			local cornerDist
			for objCorner in objCorners do 
			(
				cornerDist = distance sourcePivot objCorner
				if (cornerDist > maxRadius) do 
				(
					maxRadius = cornerDist
				)
			)
		)		

		-- Increase radius a little
		maxRadius + 1
	)
	
	fn displayPromptRoll = 
	(
		if not RsModelSlicerSettingsRoll.open do 
		(
			createDialog RsModelSlicerSettingsRoll style:#() --bgcolor:((colorMan.getColor #toolTipBackground) * white)
		)
		
		setDialogPos RsModelSlicerSettingsRoll (mouse.screenpos + [12,-12 - RsModelSlicerSettingsRoll.height])
	)
	
	-- Splits a poly-object up into objects by element, returns the new objects:
	fn splitToElementObjs obj = 
	(
		convertToPoly obj
		
		local newObjs = #()
		local faceCount = obj.numFaces
		
		-- Find seperate face-elements:
		local facesArray = #{1..faceCount}
		local checkFaces = copy facesArray
		local faceElements = #()
		
		for faceNum = facesArray where checkFaces[faceNum] do 
		(
			local elFaces = polyOp.getElementsUsingFace obj faceNum
			append faceElements elFaces
			checkFaces -= elFaces
		)
		
		-- Split object up if it has seperate elements:
		if (faceElements.count != 1) do 
		(
			local objsName = obj.name + "_"

			for elemNum = 2 to faceElements.count do 
			(
				local newObj = copy obj
				newObj.name = (uniqueName objsName)
				newObj.wireColor = obj.wirecolor
				
				polyOp.deleteFaces newObj (facesArray - faceElements[elemNum])
				
				append newObjs newObj
			)
			
			polyOp.deleteFaces obj (facesArray - faceElements[1])
		)
		
		return newObjs
	)
	
	on start do 
	(
		-- Get settings from rollout:
		if (::RsModelToolDivideRoll != undefined) and (RsModelToolDivideRoll.open) do 
		(
			chopToObjs = (RsModelToolDivideRoll.btnsSliceObjElem.state == 1)
			randomColours = RsModelToolDivideRoll.chkRandomClr.state
		)
		
		sourceObjs = for obj in selection where (isKindOf obj Editable_Poly) or (isKindOf obj Editable_mesh) collect obj
		
		if sourceObjs.count != 0 then 
		(
			undo off 
			(
				RsModelSliceDrawPoints = #()
				registerRedrawViewsCallback RsModelSliceDraw
				
				sourcePivot = [0,0,0]
				for obj in sourceObjs do (sourcePivot += obj.pos)
				sourcePivot /= sourceObjs.count
			)
		)
		else 
		(
			messageBox "No valid object selected" title:"Model Slicer Error"
			#stop
		)
	)
	
	on mousePoint clickNum do 
	(
		if (clickNum == 1) then 
		(
			local coordSysTM = Inverse(getViewTM())
			local viewDir = -coordSysTM.row3
			offPlane = manip.makePlaneFromNormal viewDir sourcePivot
		)
		else 
		(
			local intersect = getIntersect sourceObj
			
			if intersect != undefined do 
			(				
				if (clickNum > 1) and (firstClick == undefined) then 
				(
					firstClick = viewPoint
					
					local curPoint = [viewPoint.x, viewPoint.y, 0]
					RsModelSliceDrawPoints[1] = curPoint
					RsModelSliceDrawPoints[2] = curPoint
				)

				append clickRays intersect
				
				local curPoint = [viewPoint.x, viewPoint.y, 0]
				RsModelSliceDrawPoints[RsModelSliceDrawPoints.count] = curPoint
				append RsModelSliceDrawPoints curPoint
			)
		)
	)

	on freeMove do 
	(
		if (validSrcObjs()) then 
		(
			displayPromptRoll()
		)
		else 
		(
			-- Cancel if sourceObj is deleted
			#stop
		)
	)
	
	on mouseMove clickNum do 
	(
		if (validSrcObjs()) then 
		(
			local pointCount = RsModelSliceDrawPoints.count
			if (pointCount > 0) do 
			(
				RsModelSliceDrawPoints[pointCount] = [viewPoint.x, viewPoint.y, 0]
				completeRedraw()
			)
			displayPromptRoll()
		)
		else 
		(
			-- Cancel if sourceObj is deleted
			#stop
		)
	)
	
	on stop do 
	(
		destroyDialog RsModelSlicerSettingsRoll
		unregisterRedrawViewsCallback RsModelSliceDraw
		
		-- Do the decal-cutting if the source-objects are still valid:
		if (validSrcObjs()) do 
		(
			local rayCount = clickRays.count
			if (rayCount == 0) do return false
			
			suspendEditing()
			local viewTM = inverse (viewport.getTM())
				
			local origSel = selection as array
				
			-- Create cutter-object:
			local cutterObj = Editable_Mesh pos:sourcePivot
			convertToPoly cutterObj
			
			local maxRadius = getMaxRadius()
			for ray in clickRays do (polyop.createVert cutterObj (ray.pos - maxRadius * ray.dir))
			for ray in clickRays do (polyop.createVert cutterObj (ray.pos + maxRadius * ray.dir))
			
			local faceVertsA = (for n = 1 to clickRays.count collect n)
			local faceVertsB = (for n = (polyOp.getNumVerts cutterObj) to (clickRays.count + 1) by -1 collect n)
			polyop.createPolygon cutterObj faceVertsA
			polyop.createPolygon cutterObj faceVertsB
			
			local otherVert = rayCount
			for n = 1 to (clickRays.count - 1) do 
			(
				otherVert += 1
				polyop.createPolygon cutterObj #(n, n + 1, otherVert + 1, otherVert)
			)
			polyop.createPolygon cutterObj #(rayCount, 1, rayCount + 1, rayCount * 2)
			
			-- If the cutter is inside-out, flip its normals:
			if ((in coordsys viewTM polyOp.getFaceNormal cutterObj 1).z <= 0) do 
			(
				addModifier cutterObj (Normalmodifier flip:on)
				collapseStack cutterObj
			)
			
			local newSel = #()
			local changeColourObjs = #()
			
			-- Split objects up by element first, to stop them sticking together:
			local addObjs = #()
			local rejoinList = #()
			for obj in sourceObjs where (intersects obj cutterObj) do 
			(
				local splitObjs = splitToElementObjs obj
				
				if (splitObjs.count != 0) do 
				(
					join addObjs splitObjs
					join changeColourObjs splitObjs
					
					local objElemList = #(obj)
					join objElemList splitObjs
					append rejoinList objElemList
				)
			)
			join sourceObjs addObjs

			-- Don't bother booling objects outside the cutter's bounding-box:
			for obj in sourceObjs where (intersects obj cutterObj) do 
			(
				local objsName = obj.name + "_"
				local objColour = obj.wirecolor
				
				local newObjA, newObjB
				
				if chopToObjs then 
				(
					newObjA = copy obj
					newObjB = copy obj
					
					delete obj
					append changeColourObjs newObjA
				)
				else 
				(
					newObjA = obj
					newObjB = copy obj
				)
				
				-- Don't allow ProBoolean to clutter the undo-list:
				undo off 
				(
					-- Boolean cutter with object to create split-objects:
					ProBoolean.CreateBooleanObject newObjA (copy cutterObj) 1 2 1
					ProBoolean.CreateBooleanObject newObjB (copy cutterObj) 2 2 1
				)

				local splitObjs = #(newObjA, newObjB)
				splitObjs.wireColor = objColour
				
				for splitObj in splitObjs do 
				(
					convertToPoly splitObj
					
					if chopToObjs then 
					(
						local faceCount = splitObj.numFaces
						
						if (faceCount == 0) then 
						(
							-- Delete empty objects:
							delete splitObj
						)
						else 
						(
							splitObj.name = (uniqueName objsName)
							append newSel splitObj
							
							-- If split-object has multiple elements, split them off:
							local newElementObjs = splitToElementObjs splitObj
							
							for elemObj in newElementObjs do 
							(
								elemObj.name = (uniqueName objsName)
							)
							
							join changeColourObjs newElementObjs
						)
					)
					else 
					(
						if (splitObj != obj) do 
						(
							polyop.attach obj splitObj
						)
					)
				)
			)
			
			delete cutterObj
			
			-- Recombine objects that were split earlier:
			if not chopToObjs do 
			(
				for elemList in rejoinList do 
				(
					local startObj = elemList[1]
					
					for elemNum = 2 to elemList.count do 
					(
						polyOp.attach startObj elemList[elemNum]
					)
				)
			)

			-- Set random colours on newly-split objects:
			if chopToObjs and randomColours do 
			(
				local coloursUsed = for obj in newSel collect obj.wirecolor
				coloursUsed = makeUniqueArray coloursUsed
				
				for obj in (makeUniqueArray changeColourObjs) where (isValidNode obj) do 
				(
					local newColour = RsGetRandomColour excludes:coloursUsed
					obj.wireColor = newColour
					append coloursUsed newColour
				)
			)
			
			origSel = for obj in origSel where isValidNode obj collect obj
			join newSel origSel

			select newSel
			
			resumeEditing()
		)
	)
)

startTool RsModelSlicerTool
