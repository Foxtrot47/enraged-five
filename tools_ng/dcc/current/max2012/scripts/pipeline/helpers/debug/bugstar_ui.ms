--
-- Tool to create Bugstar bugs as nodes in scene
--
-- Neal D Corbett
-- Rockstar Leeds
-- 21/06/2011
--
-- 
-- Updated 18/11/2012
-- Mark Harrison-Ball
--
-- Update 18/01/2013
-- Andy Davis

-- Forward Declaration - Don't delete
RsBugstarUI

-- Reloading Bugstar struct will reset user/password data:
if (bugstar == undefined) do 
(
	fileIn "pipeline/helpers/debug/bugstar.ms"
	fileIn "pipeline/helpers/debug/bugstar_edit.ms"
	
)
global bugStarInit = false

rollout RsBugstarFilters "Filters" width:380
(
	local bIsActive= false
	
	group "BugNode Filter Visibility:"
	(
		checkbox chkClassA "A" width:25 checked:true tooltip:"Show A Class Bugs" --pos:(btnRefreshFilter.pos + [90,0])
		checkbox chkClassB "B" width:25 checked:true tooltip:"Show B Class Bugs" pos:(chkClassA.pos + [40,0])
		checkbox chkClassC "C" width:25 checked:true tooltip:"Show C Class Bugs" pos:(chkClassB.pos + [40,0])
		checkbox chkClassD "D" width:25 checked:true tooltip:"Show D Class Bugs" pos:(chkClassC.pos + [40,0])
		checkbox chkClassTASK "TASK" checked:true align:#left tooltip:"Show TASK Bugs" pos:(chkClassD.pos + [56,0])
		checkbox chkClassTODO "TODO" checked:true tooltip:"Show TODO Bugs" pos:(chkClassTASK.pos + [50,0])
		checkbox chkClassWISH "WISH" checked:true tooltip:"Show WISH Bugs" pos:(chkClassTODO.pos + [50,0])		
		label lblcomponet "Components" align:#left across:2 offset:[0,2]
		dropDownList lstComponents "" width:180 height:20 offset:[-112,0]  align:#left
		multilistbox mlbTagList "Tag List" height:20 width:200 across:2
		button btnClearSel "Clear Selection"
	)	
	
	
	local showList = #()
	local hideList = #()
	local components = #()  -- Thse hold a dictionary of component ID's
	local taglist = #()
	
	
	fn TagFilters = (		
		local bugNodes = for o in $helpers where classOf o == RsBugNode collect o
		for bug in bugNodes do (

			for tag in bug.taglist do (
				appendifunique taglist tag
			)
		)
		mlbTagList.items = 	taglist;
		
	)
	
	-- INIT CONTROLS / FUNCTIONS
	-- Shoudl be moved into a dictionary 
	fn updateinitCtrls = (
		ComponentItems = #()
		bugstar.getComponents();
		for c in 	bugstar.ComponentTypes do (
			if (c.parentID != "") then ( -- HAs a parent ID so lets get that
				for p in bugstar.ComponentTypes do (
					if (p.id == c.parentID) then
					(
						append ComponentItems (p.Name +" " +c.Name)
					)
				)
			)
			else (
				append ComponentItems (c.Name)
			)
		)
		sort ComponentItems
		;
		

		lstComponents.items = (join #("All") ComponentItems )
		
		TagFilters()
	)
	
	-- Tag Filter --
	-- Used to filter out Tags in the scene
	fn FilterTags obj=-- obj =
	(
		
		if ((mlbTagList.selection as array).count ==0) then (print "goat";return True);

		result = false;
		for index in mlbTagList.selection do (
			if (findItem obj.taglist (mlbTagList.items[index]) != 0) then
				return True;
		)
		result;
		
	)
	
	
	fn ComponentFilter obj =
	(
		local result = false;
		if (lstComponents.selection == 1) then return True;
		if (bugstar.lookupBugComponent obj.bugComponent == lstComponents.selected ) then result = True;
		result;
		
	)
	
	fn showHide obj= (
		if ComponentFilter obj and FilterTags obj then (
			append showList obj
		)
		else (
			append hideList obj
		)
	)
	
	--- Filter visibility
	mapped fn FilterVisibility objList =
	(		
		case of
			(
				(objList.bugclass == "A" and chkClassA.checked ):showHide objList
				(objList.bugclass == "B" and chkClassB.checked ):showHide objList
				(objList.bugclass == "C" and chkClassC.checked ):showHide objList
				(objList.bugclass == "D" and chkClassD.checked ):showHide objList				
				(objList.bugclass == "TASK" and chkClassTASK.checked ):showHide objList		
				(objList.bugclass == "TODO" and chkClassTODO.checked ):showHide objList		
				(objList.bugclass == "WISH" and chkClassWISH.checked ):showHide objList	
			
				default:append hideList objList
			)		
	)

	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	-- Filter the bugs placed in the ui by hiding any types that have there checkboxes
	-- set to false in the filter UI
	-- HB Refactored to speed up function.
	-- #Hiding/showing objects is faster when applying to a group of objects
	-- #Use one loop and check for bug class instead of looping for each class type
	-- #Use ampped functions on collections
	
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn Filterbugs = 
	(
		local bugNodes = for o in $objects where classOf o == RsBugNode collect o
		showList = #()
		hideList = #()	
		FilterVisibility bugNodes
		-- Much quicker, ignores callbacks
		unhide  showList; 
		hide hideList;
		completeRedraw()		
		
	)
	
		-- Update Filter on checkbox states
	on chkClassA changed state do
		Filterbugs();
	on chkClassB changed state do
		Filterbugs();
	on chkClassC changed state do
		Filterbugs();
	on chkClassD changed state do
		Filterbugs();
	on chkClassTASK changed state do
		Filterbugs();
	on chkClassTODO changed state do
		Filterbugs();
	on chkClassWISH changed state do
		Filterbugs();
	on lstComponents selected val do 	
		Filterbugs();

	on mlbTagList selectionEnd  do
		--FilterTags()
		Filterbugs();
		
	on btnClearSel pressed do (
		mlbTagList.selection = #();
		Filterbugs();
	)
		
	on RsBugstarFilters open do (
		updateinitCtrls() -- Only need to be init once
	)
	
	on RsBugstarFilters rolledUp state do
	(
		RsBugstarMainRoll.SetWindowHeight()
	)

)

rollout RsBugstarToolRoll "Connection" width:380
(
	local bIsActive= false;

	--dotNetControl RsBannerPanel "Panel" pos:[0,0] height:32 width:RsBugstarToolRoll.width
	--local bannerStruct = makeRsBanner dn_Panel:RsBannerPanel wiki:"Bugstar_Max_Tool" filename:(getThisScriptFilename())
	
	group "Connection:"
	(
		label lblConnectStatus "Logging in..." align:#left across:2
		button btnLogin "Change Login" height:24 align:#right offset:[3,-6]
	)
	
	local searchLabelWidth = 250
	
	group "Searches:"
	(
		label lblSearch "Search:" offset:[12,-4] align:#left across:2
		checkbox chkUserFilter "Current User" checked:true enabled:false pos:(lblSearch.pos + [44,0])
		checkButton btnRefresh "Refresh:" tooltip:"Update Search-list and bug-count" width:54 height:20 align:#left offset:[-2,0] across:4
		dropDownList lstSearches "" width:200 pos:(btnRefresh.pos + [58,0])
		label lblSearchCount "" width:70 align:#right pos:(lstSearches.pos + [lstSearches.width + 3, 3]) 
		button btnFav "*"  align:#right
		label lblSearchDesc "" width:searchLabelWidth height:30 align:#left
	)
	
	group "Filter Placement"
	(
		checkbox chkOpen "Only add Open Bugs" checked:true
	)
	
	group "Search by position parameters:"
	(
		button btnGetPos "Pos from selection" align:#left tooltip:"Set Position for Location Search to selection-centre" across:3
		editText txtPos "Position:" text:"[0,0,0]" width:150 align:#left 
		spinner spnPosRange "Range:" range:[0,10000,100] type:#integer width:75 align:#right tooltip:"Range to search around the position"
	)
	
	local posCtrls = #(btnGetPos, txtPos, spnPosRange)
	
	group ""
	(
		checkButton btnPlaceBugs "Place bug-nodes" tooltip:"Create Note Nodes at each bug-position returned by selected search" width:162 height:30 align:#left across:2 offset:[-3,-6] 
		button btnDeleteBugs "Delete bug-nodes" tooltip:"Delete all bug-nodes in scene " width:162 height:30 align:#right offset:[3,-6]
	)
	
	local userMenuItems, mapMenuItems	
	--local preLoginCtrls = #(RsBannerPanel, btnLogin, lblConnectStatus, btnDeleteBugs)
	local preLoginCtrls = #( btnLogin, lblConnectStatus, btnDeleteBugs)
	local postLoginCtrls
	
	------------------------------------------
	local cFilter = datapair() -- Used to hold selected component ID and parent ID
	----------------------------------------------
	
	fn showSearchBugsCount bugcount: = 
	(
		lblSearchCount.text = "(searching)"
		
		local searchIdx = lstSearches.selection - 3
		
		bugcount = case of 
		(
			(bugcount != unsupplied):bugcount
			(searchIdx > 0):
			(
				chkUserFilter.enabled = false
				local searchId = bugstar.searches[searchIdx].id
				bugstar.getSearchBugCount searchId
			)
			(searchIdx == -2):
			(
				chkUserFilter.enabled = false
				bugstar.getPosBugsCount (execute txtPos.text) spnPosRange.value
			)
			(searchIdx == -1):
			(
				chkUserFilter.enabled = true
				"?"
			)
			default:0
		)
		
		if (bugCount == undefined) do 
		(
			bugCount = "?"
		)
		
		local pluralise = if (bugCount == 1) then "" else "s"
		lblSearchCount.text = "(" + bugCount as string + " bug" + pluralise + ")"
	)
	
	fn setSearchDesc num = 
	(
		posCtrls.enabled = false
		
		local searchIdx = num - 3

		case of 
		(
			(searchIdx > 0):
			(
				local newText = RsWordWrap bugstar.searches[searchIdx].description searchLabelWidth
				
				if (newText == "") do 
				(
					newText = "(no description)"
				)
				
				lblSearchDesc.text = newText
			) 
			------------- CSUTOM SEARCHES ---------------------
			(searchIdx == -2):
			(
				lblSearchDesc.text = "Set position parameters"
				posCtrls.enabled = true
			)
			(searchIdx == -1):
			(
				lblSearchDesc.text = "Place bugs for all maps found in scene"
			)
			default:
			(
				lblSearchDesc.text = ""
			)
		)
		
		showSearchBugsCount()
	)
	

	-- Will update the searches --
	fn updateCtrls = 
	(	
		-- Set up searches list:
		local setSearchSel = RsBugstarToolRoll.lstSearches.selected
		
		local searches = bugstar.getSearches()
		
		
		
		if (searches == undefined) then 
		(
			lstSearches.items = #("DOWNLOAD FAILED")
			lstSearches.selection = 1
		)
		else 
		(
			---- LOCAL FAV SEarches
			--local newSearchItems = #("My Bugs Requiring Action", "Search by position", "Search by map", "-------------")
			local newSearchItems = #("Search by position", "Search by map", "-------------")
			
			join newSearchItems (for item in searches collect item.name)
			lstSearches.items = newSearchItems
			lstSearches.selection = 0
				
			local selNum = findItem lstSearches.items setSearchSel
			if (selNum == 0) do 
			(
				selNum = findItem lstSearches.items "My Bugs Requiring Action"
			)
			
			lstSearches.selection = selNum
			setSearchDesc selNum
			
			--join newSearchItems #("PIGS")
		)
	)
	
	fn startLogin useExisting:false = 
	(
		lblConnectStatus.text = "Logging in..."
		
		if (useExisting and bugstar.hasValidPassword()) or bugstar.login() or bugstar.loginCancelled then 
		(
			if bugstar.loginCancelled then 
			(
				bugstar.loginCancelled = false
				lblConnectStatus.text = "Login cancelled"
			)
			else 
			(
				postLoginCtrls.enabled = true
				lblConnectStatus.text = "Connection successful"
				btnLogin.text = "Change Login"
				updateCtrls()
				
			)
		)
		else 
		(
			postLoginCtrls.enabled = false
			lblConnectStatus.text = bugstar.lastError
			btnLogin.text = "Login"
		)
	)
	

	-------------------------------
	--- EVENT HANDLERS  ---
	-------------------------------
	
	on btnLogin pressed do (startLogin())

	on txtPos entered newVal do 
	(
		local setVal = "[0,0,0]"
		
		-- Make sure that entered value is a point3:
		try 
		(
			local getVal = execute newVal
			
			if (isKindOf getVal Point3) do 
			(
				setVal = getVal as string
			)
		)
		catch ()
		
		txtPos.text = setVal
		
		showSearchBugsCount()
	)
	
	on spnPosRange changed newVal do 
	(
		showSearchBugsCount()
	)
	
	on lstSearches selected num do 
	(
		txtPos.text = selection.center as string
		setSearchDesc num
	)
	
	on btnGetPos pressed do 
	(
		txtPos.text = selection.center as string
		showSearchBugsCount()
	)
	
	on btnRefresh changed state do 
	(
		updateCtrls()
		btnRefresh.checked = false
	)
	
	on btnPlaceBugs changed state do 
	(
		undo "place bug-nodes" on 
		(
			-- Remove existing bug-nodes:
			delete (for obj in objects where (isKindOf obj RsBugNode) collect obj)
			completeRedraw()
			
			local timeStart = timeStamp()
			
			local foundBugs
			local searchIdx = lstSearches.selection - 3
			
			
			case of 
			(
				(searchIdx > 0):
				(
					foundBugs = bugstar.getSearchBugs bugstar.searches[searchIdx].id
				)
				(searchIdx == -2):
				(
					foundBugs = bugstar.getPosBugs (execute txtPos.text) spnPosRange.value
				)
				(searchIdx == -1):
				(
					foundBugs = bugstar.getMapBugs byuser:chkUserFilter.checked
				)
			)
			
			format "Bug-list download took %s\n" ((timeStamp() - timeStart) / 1000.0)
			
			if (isKindOf foundBugs array) do 
			(
				bugstar.placeBugs foundBugs openBugsOnly:chkOpen.state
			)
		)
		
		btnPlaceBugs.checked = false
	)
	
	on btnDeleteBugs pressed do 
	(
		undo "delete bug-nodes" on 
		(
			delete (for obj in objects where (isKindOf obj RsBugNode) collect obj)
			completeRedraw()
		)
	)
	
	on RsBugstarToolRoll rolledUp state do
	(
		RsBugstarMainRoll.SetWindowHeight()
	)
	
	--- INITIALIZE __
	on RsBugstarToolRoll open do 
	(
		-- Initialise tech-art banner:		
		postLoginCtrls = for ctrl in RsBugstarToolRoll.controls where (findItem preLoginCtrls ctrl == 0) collect ctrl
		postLoginCtrls.enabled = false
		
		startLogin useExisting:true
		bugStarInit = true;
	)
	
-- 	Clean up
	on RsBugstarToolRoll close do 
	(
		destroydialog RsBugstarImagePreview	
	)
)

rollout RsBugstarMainRoll "Bugstar Max Tool" width:420 height:1200
(
	-- Constants
	local rollWidth	
	local bBugStarDeleteNodeSub = false;
	local bBugRemember = false; local sLastBugNode = undefined
	local bBugInView = false;
	local bZoomViewBug = true;
	local tabIndex = 1
		
	-- Header
	dotNetControl RsBannerPanel "Panel" pos:[0,0] height:32 width:RsBugstarMainRoll.width
	local bannerStruct = makeRsBanner versionNum:1.02 versionName:"Fateful Wire" dn_Panel:RsBannerPanel wiki:"Bugstar_Max_Tool"
	
	dotNetControl dnTabs "System.Windows.Forms.TabControl" width:(RsBugstarMainRoll.width) height:20
	subRollout theSubRollout height:1120 offset:[-12,0] width:(RsBugstarMainRoll.width)
		
	local rolloutPages = 
	#(
		DataPair name:"Connection" rollouts:#(RsBugstarToolRoll),
		DataPair name:"Filter" rollouts:#(RsBugstarFilters),
		DataPair name:"Edit" rollouts:#(RsBugstarPropertys,RsBugstarAttachmentList,RsBugstarSummary, RsBugstarEditDialog),
		DataPair name:"Bug Session" rollouts:#(RsBugSessionList,RsBugstarEditDialog), --RsBugstarEditDialog
		DataPair name:"Preferences" rollouts:#(RsBugstarPreferences)
	)
	local rolloutCurrentPages = #()
	
	fn SetPage idx = 
	(	
		local thisheight;
		if ( idx <= rolloutPages.count ) then
		(
			thisheight = 0;
			for roll in rolloutCurrentPages do
			(
				removeSubRollout theSubRollout roll
				roll.bIsActive = false
			)
			rolloutCurrentPages = #()
		
			
			for roll in rolloutPages[idx].rollouts do
			(			
				
				addSubRollout theSubRollout roll
				append rolloutCurrentPages roll
				thisheight +=roll.height+27;
				roll.bIsActive = true			
				
			)
			-- Auto set height of Control
-- 			if (idx == 3) then RsBugstarMainRoll.SetWindowHeight()
-- 			else	RsBugstarUI.size = [400, thisheight+84]
			RsBugstarMainRoll.SetWindowHeight()
		)
		else
		(
			MessageBox "Internal Error.  Tabs misconfigured.  Contact tools."
		)
		
	)
	
	fn arrangeCtrls = 
	(
		theSubRollout.height = RsBugstarMainRoll.height - theSubRollout.pos.y - theSubRollout.pos.x
	)
	
	fn readGlobalSettings =
	(
		RsBugNodeAutoWarp = RsSettingRead "rsMaxBugStar" "AutoWarpOnSelection" true as BooleanClass
		bBugStarDeleteNodeSub = RsSettingRead "rsMaxBugStar" "DeleteOnSubmission" false as BooleanClass
		bBugInView = RsSettingRead "rsMaxBugStar" "bugPersistant" false as BooleanClass
		bBugRemember = RsSettingRead "rsMaxBugStar" "AutoRememberLastBug" false as BooleanClass
		sLastBugNode =  RsSettingRead "rsMaxBugStar" "LastBugName" ""
		bZoomViewBug = RsSettingRead "rsMaxBugStar" "ZoomViewBug" true as BooleanClass
	)
	
-- 	function to control window height, used when the edit window is up
	fn SetWindowHeight =
	(
		local height = 0
		local headerHeight = 60
		local gubbins = 40	--height of the other stuff
		local editRollouts = #(RsBugstarPropertys, RsBugstarAttachmentList, RsBugstarSummary, RsBugstarEditDialog)
		local scr = (dotNetClass "System.Windows.Forms.Screen").PrimaryScreen.Bounds
        local maxHeight = scr.Height - 150
		
		if (RsBugstarMainRoll.open) then
		(
			local toolHeight = headerHeight + gubbins + 30
			local rolloutHeadHeight = 16
			
			for sub in rolloutCurrentPages do
			(
				if (sub.open) then height += sub.height + rolloutHeadHeight
				else height+= rolloutHeadHeight
			)
			
			toolHeight += height
			
			if (toolHeight > maxHeight) then toolHeight = maxHeight
			
			RsBugstarUI.size = [400, toolHeight]
			RsBugstarMainRoll.height = height + gubbins + headerHeight
			RsBugstarMainRoll.theSubRollout.height = height + gubbins
		)
		
		else
		(
			RsBugstarUI.size = [400, 32]
		)
	)
	
	-- Refresh banner-icons when floater's rollouts are adjusted:
	on RsBannerPanel Paint Sender Args do 
	(		
		for n = 0 to (RsBannerPanel.Controls.Count - 1) do 
		(
			RsBannerPanel.Controls.Item[n].Invalidate()
		)		
	)

	on dnTabs Click do
	(
		local tabNum = dnTabs.SelectedIndex + 1
		
		--refresh the page only if a new tab is selected
		if not (tabNum == RsBugstarMainRoll.tabIndex) then
		(
			SetPage tabNum
			RsBugstarMainRoll.tabIndex = tabNum
		)
	)
	
	on RsBugstarMainRoll open do (
		-- Initialise tech-art banner:
		bannerStruct.setup()
		
		rollWidth = RsBugstarMainRoll.width
		
		arrangeCtrls()
		readGlobalSettings()
		
		-- Set up our Tab Pages
		dnTabs.tabPages.clear()
		for item in rolloutPages do 
		(
			dnTabs.tabPages.add item.name
			-- Way to track which rollouts are active ---
			item.rollouts.bisactive = false;
		)
		
		-- Set Page to 1
		SetPage 1
	)
	
	on RsBugstarMainRoll rolledUp state do
	(
		RsBugstarMainRoll.SetWindowHeight()
	)
	
	on RsBugstarMainRoll close do (
		RsBugstarPreferences.RsSaveSettings();
	)
)

try (	
	CloseRolloutFloater RsBugstarUI
)
catch()

RsBugstarUI = newRolloutFloater("Rockstar Bugstar_Max_Tool") 400 420 50 50
addRollout RsBugstarMainRoll RsBugstarUI


