try destroyDialog (RSmemViewer) catch ()

rollout RSmemViewer "3DSMax Memory:" width:206
(
	local btnWidth = 24
	local textWidth = 90
	
	checkbutton showMemBtn "*" height:20 width:btnWidth checked:true 
		tooltip:"Show Max's total memory-use" align:#left
	label memText "XXXXXXXXXXXXXX" pos:(showMemBtn.pos + [btnWidth + 6, 3]) width:textWidth
	checkbutton showOffsetBtn "+/-" height:20 width:btnWidth pos:([memText.pos.x + textWidth, showMemBtn.pos.y])
		tooltip:"Show change in Max's memory-use since this button was last pressed"
	
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/MemSize"
		align:#right pos:(showOffsetBtn.pos + [btnWidth + 10, 3])
		color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	timer ticker "" interval:1000
	
	local baseMem
	
	fn showMem = 
	(
		local memVal = (sysinfo.getMAXMemoryInfo())[3]

		if (baseMem == undefined) do  
		(
			baseMem = memVal
		)
		
		if showOffsetBtn.checked do 
		(
			memVal = memVal - baseMem
		)
		
		memText.text = RsGetMemString memVal showPlus:showOffsetBtn.checked
	)
	
	on ticker tick do 
	(
		showMem()
	)
	
	on showMemBtn changed val do 
	(
		showMemBtn.checked = true
		showOffsetBtn.checked = false
		showMem()
	)
	
	on showOffsetBtn changed val do 
	(
		baseMem = undefined
		showMemBtn.checked = false
		showOffsetBtn.checked = true
		showMem()
	)
	
	on RSmemViewer open do 
	(
		showMem()
	)	
)

createDialog RSmemViewer