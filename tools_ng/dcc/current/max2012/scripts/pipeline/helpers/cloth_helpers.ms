-- 
-- Cloth helpers
-- author: Gunnar Droege
-- date: 22/8/11

global paintClothMapChannels = #(16,17,12)
global gCurrVariationChannel = 0
global gVariationChannelsPerChannel = 9

struct RAGEClothDataEntryUI
(
	spinCtrl,
	varChannelUse,
	varChannelDismiss
)

struct RAGEClothDataEnty
(
	vDataChn,
	userName,
	defaultVal,
	range,
	uiEntry,
	vDataVariationOffset,
	fn GetVDataChannel = 
	(
		local channelToUse = vDataChn
		if gCurrVariationChannel>0 and undefined!=vDataVariationOffset then
			channelToUse = vDataVariationOffset + gCurrVariationChannel
		return channelToUse
	)
)

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
	
--Note the vDataChn values must remain the same as what REX is expecting to export
global RAGEClothChannelTable

if undefined==RAGEClothChannelTable then
(
	RAGEClothChannelTable = #( 
		(RAGEClothDataEnty userName:"Pin" 								vDataChn:10 defaultVal:0.0 	range:[0,1,0] 								),
		(RAGEClothDataEnty userName:"Pin Radius" 						vDataChn:11 defaultVal:0.0 	range:[0,1,0] 								vDataVariationOffset:30),
		(RAGEClothDataEnty userName:"Vertex Resistance" 			vDataChn:12 defaultVal:0.2 range:[0,1,0] 								),
		(RAGEClothDataEnty userName:"Edge Compression" 		vDataChn:13 defaultVal:0.25 	range:[0,1,0] 								),
		(RAGEClothDataEnty userName:"Bend Spring Length" 		vDataChn:14 defaultVal:0.0 	range:[0,1,0] 								),
		(RAGEClothDataEnty userName:"Bend Spring Strength" 	vDataChn:15 defaultVal:0.0 	range:[0,1,0] 								),
		(RAGEClothDataEnty userName:"Weight" 							vDataChn:16 defaultVal:1.0 	range:[0,10,0] 							),
		(RAGEClothDataEnty userName:"Mapping Tolerance" 		vDataChn:17 defaultVal:0.05 	range:[0.001,0.5,0.05] 				),
		(RAGEClothDataEnty userName:"Mapping Error Distance" vDataChn:18 defaultVal:1.04 	range:[0.001,10.0,1.04] 			),
		(RAGEClothDataEnty userName:"Dynamic pinning" 			vDataChn:19 defaultVal:0 		range:#("disabled", "enabled") ),
		(RAGEClothDataEnty userName:"Inflation scale" 				vDataChn:20 defaultVal:0.0f 	range:[0,1,0] 								),
		(RAGEClothDataEnty userName:"Custom edge" 				vDataChn:21 defaultVal:0 		range:#("disabled", "enabled") )
	)
)
							
fn GetChannelTableEntryByUserName userName = 
(
	for i in 1 to RAGEClothChannelTable.count do
	(
		if(RAGEClothChannelTable[i].userName == userName) then
			return i
	)
	return undefined
)

fn RsGetClothTuningChannel tableOffset =
(
	local channelToUse = RAGEClothChannelTable[tableOffset].vDataChn
	if gCurrVariationChannel>0 and undefined!=RAGEClothChannelTable[tableOffset].vDataVariationOffset  then
		channelToUse = RAGEClothChannelTable[tableOffset].vDataVariationOffset + (gCurrVariationChannel-1)
	return channelToUse
)

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

fn GetChannelSupport obj channelIndex =
(
	local interfaze = RsMeshPolyOp obj
	if undefined!=interfaze and interfaze.getVDataChannelSupport obj channelIndex then 
		return true
	
	return false
)

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

fn SetChannelSupport obj channelIndex isSupported =
(
	local interfaze = RsMeshPolyOp obj
	if undefined!=interfaze then
	(
		if not isSupported then
			interfaze.freeVData obj channelIndex
		interfaze.setVDataChannelSupport obj channelIndex isSupported
	)
)

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

fn GetChannelValue obj channelIndex vertIndex =
(
	local interfaze = RsMeshPolyOp obj
	if undefined!=interfaze and interfaze.getVDataChannelSupport obj channelIndex then 
		return interfaze.getVDataValue obj channelIndex vertIndex

	return undefined
)


----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

fn SetChannelValue obj channelIndex vertIndex value =
(
	local interfaze = RsMeshPolyOp obj
	if undefined!=interfaze and interfaze.getVDataChannelSupport obj channelIndex then
	(
		interfaze.setVDataValue obj channelIndex vertIndex value
	)
)


fn EnableAndInitMapChannel obj tableIndex = 
(
	if(not (GetChannelSupport obj (RsGetClothTuningChannel tableIndex))) then
	(
		SetChannelSupport obj (RsGetClothTuningChannel tableIndex) true
		local nVerts = obj.vertices.count
		for vIdx in 1 to nVerts do
		(
			SetChannelValue obj (RsGetClothTuningChannel tableIndex) vIdx RAGEClothChannelTable[tableIndex].defaultVal
		)
	)
)
fn DisableAndDenitMapChannel obj tableIndex = 
(
	if(obj != undefined) then
	(
		SetChannelSupport obj (RsGetClothTuningChannel tableIndex) false
	)
)

fn ApplyPaintPodifier obj paintType =
(
	if undefined!=obj.modifiers[#VertexPaint] then 
	(
		messagebox "Vertex paint modifier already present. Please collapse or delete before proceeding."
		return false
	)
	
	max modify mode
	obj.vertexColorType = #map_channel
	
	local p = VertexPaint()
	obj.vertexColorMapChannel = paintClothMapChannels[paintType]
	p.mapchannel = paintClothMapChannels[paintType]
	addmodifier obj p
	
	local vPainttool = VertexPaintTool()
	vPainttool.mapDisplayChannel = paintClothMapChannels[paintType]
	vPainttool.curPaintMode = 2
	vPainttool.paintColor = color 0.0 0.0 0.0 
			
	aaa = vPainttool.keepToolboxOpen	
	vPainttool.keepToolboxOpen = aaa
	
	update obj
)

fn hasAnyPinnedVerts obj =
(
	local interfaze = RsMeshPolyOp obj
	local iDataChannelPin = RAGEClothChannelTable[1].vDataChn
	local numSimVerts = interfaze.getnumverts obj
	local foundAny = false
	for i = 1 to numSimVerts while not foundAny do
	(
		if ( ( interfaze.getVDataChannelSupport obj iDataChannelPin ) and (interfaze.getVDataValue obj iDataChannelPin i)> 0.0) then
			foundAny = true
	)
	return foundAny
)

fn GetAffectedVerts target renderMesh onlySelectedVerts:false = 
(
	local interfaze = RsMeshPolyOp target
	local renderMeshInterfaze = RsMeshPolyOp renderMesh

	local iDataChannelMapping = RAGEClothChannelTable[8].vDataChn
-- 	local iDataChannelError = RAGEClothChannelTable[9].vDataChn
	local numSimVerts = interfaze.getnumverts target
	local numRenderVerts = renderMeshInterfaze.getnumverts renderMesh

	local verts = #{}
	local selVerts = #{}
	if meshop!=interfaze then
		selVerts = interfaze.getVertSelection target
	else
		selVerts = getVertSelection target
	for i = 1 to numSimVerts do
	(
		if onlySelectedVerts and not (selVerts[i]) then
			continue
		local vertPos = interfaze.getVert target i
		-- vertPos = target.getVertex (target.getFaceVertex i 1)
		-- print vertPos
		local fPinValue = 9999.0
		if ( interfaze.getVDataChannelSupport target iDataChannelMapping ) then
			fPinValue = interfaze.getVDataValue target iDataChannelMapping i
-- 		if ( interfaze.getVDataChannelSupport target iDataChannelError ) then
-- 		(
-- 			local errorVal = interfaze.getVDataValue target iDataChannelError i
-- 			fPinValue = if errorVal > fPinValue then errorVal else fPinValue
-- 		)
		for vi=1 to numRenderVerts do
		(
			local v = renderMeshInterfaze.getvert renderMesh vi
			if (length (vertPos - v))<=fPinValue then
			(
				append verts vi
			)
		)
	)
	return verts
)

fn ValidateMappingTolerances = 
(
	local target = RageClothChannelTuning.curObjSel
	if undefined==target then
		return false
	local renderMesh = RsLodDrawable_GetRenderModel target
	if undefined==target then
	(
		messagebox "No linked rendermesh found."
		return false
	)

	local interfaze = RsMeshPolyOp target
	local renderMeshInterfaze = RsMeshPolyOp renderMesh

	renderMesh.showVertexColors = true
	renderMesh.vertexColorType = #map_channel
	renderMesh.vertexColorMapChannel = 30
	if renderMeshInterfaze.getNumMaps renderMesh < 31 then
	(
		renderMeshInterfaze.setNumMaps renderMesh 31
	)
	renderMeshInterfaze.setMapSupport renderMesh 30 true
	local mapChannelType = 3
	ChannelInfo.NameChannel renderMesh mapChannelType 30 "temp cloth mapping validation"
	
	local numRenderVerts = renderMeshInterfaze.getnumverts renderMesh
	renderMeshInterfaze.setVertColor renderMesh 30 #{1..numRenderVerts} red

	local affectedVerts = GetAffectedVerts target renderMesh
	renderMeshInterfaze.setVertColor renderMesh 30 affectedVerts green
)