
filein ( RsConfigGetWildwestDir() + "script/3dsmax/simplygon/simplygon.ms" )
filein "pipeline/export/peds/utils.ms"
filein "pipeline/helpers/terrain/terrain_processing.ms"
filein "rockstar/export/settings.ms"
filein "pipeline/helpers/terrain/edgenormalisation.ms"
filein "pipeline/helpers/terrain/edgeprocessing.ms"
filein "pipeline/helpers/terrain/terrain_lod.ms"
filein "pipeline/helpers/terrain/terrain_general.ms"
filein "pipeline/ui/containers.ms"


try (destroyDialog RsContainersRoll) catch ()

struct terrainFileNameObj (
	
	theFile = undefined,
	filePath = undefined,
	fileName = undefined,
	fileType = undefined,
	DisplayName = undefined,
	adjacencyList = #(),
	--adjacencyListFiles = undefined,
	TileType = undefined
)

struct terrainFileObjTools (
	
	fileArray = #(),
	filteredArray = #(),

	fn filterText textArray theText prefix:"" suffix:"" = 
	(
		
		filteredArray = #()
		
		if theText == "" then 
		(
			this.filteredArray = this.fileArray		
			
		) else (
			
			local theArray = this.fileArray	
			local displayNameList = for f in theArray collect f.DisplayName	
			
			for i in 1 to displayNameList.count do
			(
				lookForString = findString displayNameList[i] theText
				if lookForString != undefined then append this.filteredArray theArray[i]
			)
				
		)
		
		qsort this.filteredArray this.compareFN
		local filtList = #()

		for f in this.filteredArray do (
			local theType = ""
			local theName = ""
			if prefix != "" then append theName prefix
			if f.TileType == true then theType = "Receiver" else theType = "Giver"
			append theName (f.DisplayName +"  :  " + theType)					
			if suffix != "" then append theName suffix
			append filtList theName		
		)
		filtList
	),
	
	fn compareFN f1 f2 =
	(
		local f1Name = f1.DisplayName
		local f2Name = f2.DisplayName
		local sortOrder = sort #(f1Name,f2Name)
		
		case of
		(
			(sortOrder[1] == f1Name): -1
			(sortOrder[1] == f2Name): 1
			default: 0
		)
	),
	
	fn genTileXml = (
		
		theArray = theAdjacencyList.entries
		theArray = #()
		
		for f in 1 to theArray.count do (
			
			newFileObj = terrainFileNameObj()
			newFileObj.theFile = rsTP.getTerrainContainerFullFilename theArray[f].containerName
			newFileObj.filePath = getfilenamepath newFileObj.theFile
			newFileObj.fileName = getfilenamefile newFileObj.theFile
			newFileObj.fileType = getfilenametype newFileObj.theFile
			newFileObj.DisplayName = getfilenamefile newFileObj.theFile
			newFileObj.adjacencyList = theAdjacencyList.GetAdjacencyList(getfilenamefile newFileObj.theFile)

			newFileObj.tileType = theAdjacencyList.IsReceiver(getfilenamefile newFileObj.theFile)
			append this.fileArray newFileObj
			
		
			
		)		
		qsort this.fileArray this.compareFN
		this.filteredArray = this.fileArray	
		ns = newscript()
		format "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<tiles>\n" to:ns
		for item in  this.fileArray	do (
		
			format "\t<tile filename=\"%\" tileType=\"%\">%</tile>\n" item.theFile item.tileType item.adjacencyList to:ns

		)
		format "</tiles>\n" to:ns
		
	),
	
	fn makeFileArray = (
		
		start = timeStamp()
		
		local filename = rsmakesafeslashes (RsConfigGetToolsConfigDir()+"/config/generic/terrainProcessingTiles.xml")

		if ( doesFileExist filename ) == false then 
		(
			print "No XML file"
			return undefined
		)		

		stream = dotNetObject "System.IO.StreamReader" filename
		doc = dotNetObject "System.Xml.XmlDocument"
		doc.load stream
		
		root = doc.documentElement			

		local  tileFileNodes = root.childnodes
		
		for tileFileIdx = 0 to ( tileFileNodes.count - 1 ) do (
			
			local tileFileNode = tileFileNodes.item[ tileFileIdx ]
			local newFileObj = terrainFileNameObj()
			newFileObj.theFile = tileFileNode.attributes.Itemof[0].value
			newFileObj.filePath = getfilenamepath newFileObj.theFile
			newFileObj.fileName = getfilenamefile newFileObj.theFile
			newFileObj.fileType = getfilenametype newFileObj.theFile
			newFileObj.DisplayName = getfilenamefile newFileObj.theFile
			newFileObj.adjacencyList = tileFileNode.innerXml
			
			if tileFileNode.attributes.Itemof[1].value == "true" then isGiver = true else isGiver = false 
			newFileObj.tileType = isGiver
			append this.fileArray newFileObj
			
		)
		
		doc = undefined
		stream.close()
		stream.dispose()
		stream = undefined
		
		gc()		

		qsort this.fileArray this.compareFN
		this.filteredArray = this.fileArray
		end = timeStamp()
		format "Creating Tile List took % seconds\n" ((end - start) / 1000.0)
	),
	
	fn makeDisplayNameArray theArray = (	
		
		local displayNameArray = for f in theArray collect f.DisplayName
		displayNameArray
	),

	fn makeSelectedArray theItem outputArray = (		

		local displayNameArray = makeDisplayNameArray outputArray 

		if classof theItem == integer then (
			local inList = finditem displayNameArray this.filteredArray[theItem].displayName
			if inList == 0 then (
				append outputArray this.filteredArray[theItem]
				
			)
			qsort outputArray this.compareFN
			displayNameArray = for f in outputArray collect f.displayname
		)
		
		displayNameArray		
	)
	
)

global rsTerrain_Processing_Rollout
global terrainTabs_Rollout
global  gRsTerrainLoadAdjacencyList= RsTerrainLoadAdjacencyList()
global gterrainFileObjTools = terrainFileObjTools()	 

(try destroydialog terrainTabs_Rollout catch ())
(
	try (cui.UnRegisterDialogBar terrainTabs_Rollout)catch()
	try(destroyDialog terrainTabs_Rollout)catch()
	local LastSubRollout = 1


	rollout rsTerrain_Processing_Rollout "Terrain Processing"
	(	

		local exportArray =#()	
		local jobsFileName =  (RsConfigGetToolsDir()+"etc/config/simplygon/peds_all.xml")	 	
		local jobLocation = (RsConfigGetToolsDir()+"dcc/current/max2012/scripts/pipeline/util/")
		local doJob = ""
		
		--dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:rsTerrain_Processing_Rollout.width
		--local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Terrain Processing" filename:(getThisScriptFilename())	
			
		groupbox grpTT "Terrain Tiles"	pos:[5,5] width:241 height:275	
		button btnRefreshTileList "Load/Refresh Tile List" width:230 pos:[grpTT.pos.x+5,grpTT.pos.y+20]
		checkbox chkFilterCheck ""  pos:[grpTT.pos.x+40,grpTT.pos.y+45]
		editText edFilterList "Filter:" labelontop:true width:227   pos:[grpTT.pos.x+7,grpTT.pos.y+45]
		multiListBox mlbTerrainTiles "Terrain Tiles:" labelontop:true  height:10 width:227 pos:[grpTT.pos.x+7,grpTT.pos.y+85]
		button btnAddToProcess "Add Selected" width:100 align:#right pos:[grpTT.pos.x+135,grpTT.pos.y+245]
	
		groupbox grpTT1 "Process Tiles"	pos:[253,5] width:241 height:340	
		label lblProcessNumber "0"  align:#left offset:[84,0]  pos:[grpTT1.pos.x+105,grpTT1.pos.y+20]
		multilistbox lbProcessList "Tiles to Process:"  labelontop:true  height:10 offset:[0,-18] width:227 pos:[grpTT1.pos.x+7,grpTT1.pos.y+20]
		button btnRemoveSelected "Remove Selected" width:100 align:#right pos:[grpTT1.pos.x+135,grpTT1.pos.y+180]		
		editText edFilePath "File Path:"  labelontop:true  align:#left offset:[0,-10] readonly:true height:20 width:227  pos:[grpTT1.pos.x+7,grpTT1.pos.y+195]
		editText edTileClass "Tile is  :   "  align:#left readonly:true offset:[0,0] height:20 labelontop:true width:100 pos:[grpTT1.pos.x+7,grpTT1.pos.y+240]
		editText edAdjacentfiles "Adjacent Tiles:"  labelontop:true  align:#right readonly:true height:20 width:120 pos:[grpTT1.pos.x+115,grpTT1.pos.y+240]
		button btnLoadCont "Load Containers" width:90 align:#left height:28 pos:[grpTT1.pos.x+7,grpTT1.pos.y+290]
		button btnLoadRaw "Load Raw" width:80 align:#left height:28	pos:[grpTT1.pos.x+102,grpTT1.pos.y+290]	
		checkbox chkLoadAdj "Load" align:#right  pos:[grpTT1.pos.x+191,grpTT1.pos.y+290]	
		label lbl_adj "Adjacent"  align:#right  pos:[grpTT1.pos.x+189,grpTT1.pos.y+305]	
		
		groupbox grpPO "Process Options"	pos:[5,290] width:241 height:165
		checkbox chkDoMaps "Process Maps" checked:true pos:[grpPO.pos.x+5,grpPO.pos.y+20]
		label lblMapsize "Map Size" pos:[grpPO.pos.x+180,grpPO.pos.y+20]
		spinner spnMapSizeX "X:" range:[16,4096,2048] type:#integer width:60 pos:[grpPO.pos.x+162,grpPO.pos.y+35]
		spinner spnMapSizeY "Y:" range:[16,4096,2048] type:#integer width:60  pos:[grpPO.pos.x+162,grpPO.pos.y+55]
		checkbutton btnLock "[" checked:true width:20 height:20 pos:[grpPO.pos.x+132,grpPO.pos.y+43]
		checkbox chkVfb "Show Rendering" checked:false  pos:[grpPO.pos.x+5,grpPO.pos.y+40]
		checkbox chkMapQual "High Quality Maps" checked:true  pos:[grpPO.pos.x+5,grpPO.pos.y+60]
		checkbox chkDoLOD "Create LOD"checked:false  pos:[grpPO.pos.x+5,grpPO.pos.y+80]
		checkbox chkDoCheckOutCont "Check Out Containers" checked:true pos:[grpPO.pos.x+5,grpPO.pos.y+100]
		checkbox chkDoFinalBake "Do Final Bake" checked:true pos:[grpPO.pos.x+5,grpPO.pos.y+120]
		checkbox chkSilent "Silent" offset:[0,0] checked:false pos:[grpPO.pos.x+5,grpPO.pos.y+140]
		checkbox chkDebug "Debug" offset:[0,0] checked:false pos:[grpPO.pos.x+55,grpPO.pos.y+140]
		checkbox chkProceedWithErrors "Proceed with Errors" pos:[grpPO.pos.x+115,grpPO.pos.y+140] checked:true
	
		
		groupbox grpP "Process"	pos:[253,355] width:240 height:100
		button btnProcessSelected "Process Files" width:230	height:70 pos:[grpP.pos.x+5,grpP.pos.y+20]

		
		
		on btnLoadRaw pressed do (		
			
			local loadTiles = lbProcessList.selection as array		
			for val in loadTiles do (		
				rsTP.loadCachedTile exportArray[val].fileName
				if chkLoadAdj.state then (
					
					local theTiles =  (filterstring exportArray[val].adjacencyList "#(\" ,)")
					
					for adj in theTiles do rsTP.loadCachedTile adj
				)
			)		
			max tool zoomextents	
		)
		
		on btnLoadCont pressed do (		
			
			local loadTiles = lbProcessList.selection as array		
			for val in loadTiles do (				
		
				rsTP.inheritContainer(rsTP.getTerrainContainerFullFilename  exportArray[val].fileName)
				if chkLoadAdj.state then for adj in exportArray[val].adjacencyList do (
					
					rsTP.inheritContainer (rsTP.getTerrainContainerFullFilename adj)
				)
			)		
			max tool zoomextents	
			actionMan.executeAction 427416257 "0"
		)

		on chkDebug changed state do (
			
			if state then (
				
				chkMapQual.state = false
				spnMapSizeX.value = 16
				spnMapSizeY.value = 16
				chkDoLOD.state = false
				
			) else (
				
				chkMapQual.state = true
				spnMapSizeX.value = 2048
				spnMapSizeY.value = 2048
				chkDoLOD.state = true
				
				
			)
				
		)
		
		on spnMapSizeX changed val do if btnLock.state then spnMapSizeY.value = val
			
		on spnMapSizeY changed val do if btnLock.state then spnMapSizeX.value = val
			
		on btnLock changed  val do (
			
			if val then spnMapSizeY.value = spnMapSizeX.value 
		)	
			
		on chkFilterCheck changed val do
		(
			if val == false then (
				
				edFilterList.text = ""
					local newArray = gterrainFileObjTools.filterText mlbTerrainTiles.items "" 
				mlbTerrainTiles.items = newArray
				
			)
			
		)
		
		on btnRemoveSelected  pressed do (
			
			local val = lbProcessList.selection as array
			if val.count > 0 then (		
				
				for i in val.count to 1 by -1  do (
					
					deleteitem exportArray val[i]
				)		
				lbProcessList.items = gterrainFileObjTools.makeSelectedArray val exportArray				
			)
			lblProcessNumber.text = (exportArray.count as string)
			lbProcessList.selection = 0
			edFilePath.text = ""
			edAdjacentfiles.text = ""
			edTileClass.text = ""
		)
		
		on btnAddToProcess pressed do (
			
				if  ( mlbTerrainTiles.selection as array).count > 0 then (
				local displayNameArray = gterrainFileObjTools.makeDisplayNameArray exportArray 		
				
				for val in (mlbTerrainTiles.selection as array) do (			

					local inList = finditem displayNameArray gterrainFileObjTools.filteredArray[val].displayName
					if inList == 0 then (
						append exportArray gterrainFileObjTools.filteredArray[val]
					)				
				)		
				lbProcessList.items = gterrainFileObjTools.makeSelectedArray false exportArray
			)
			lblProcessNumber.text = (exportArray.count as string)
			mlbTerrainTiles.selection = 0
			if lbProcessList.items.count > 1 then chkSilent.state = true
		)
		
		on mlbTerrainTiles doubleclicked val do
		(
			lbProcessList.items = gterrainFileObjTools.makeSelectedArray val exportArray
			lblProcessNumber.text = (exportArray.count as string)
			if lbProcessList.items.count > 1 then chkSilent.state = true
		)
		
		on mlbTerrainTiles rightclick do
		(
			local val = mlbTerrainTiles.selection
			if (val  as array).count == 1 then (	
				val =  (val as array)[1]
				loadmaxfile gterrainFileObjTools.filteredArray[val].thefile
			)
		)
		
		on lbProcessList selected val do
		(
			vals = (lbProcessList.selection as array)
			if vals.count == 1  then (
				val = vals[1]
				edFilePath.text = exportArray[val].theFile
				edAdjacentfiles.text =(substring (exportArray[val].adjacencyList as string) 3 ( (exportArray[val].adjacencyList as string).count - 3))
				if exportArray[val].tiletype then edTileClass.text = "Receiver" else edTileClass.text = "Giver" 
			) else (
				edFilePath.text = "[Multiple Selection]"
				edAdjacentfiles.text = "[Multiple Selection]"
				edTileClass.text = "[Multiple Selection]"
			)
		)
		
		on lbProcessList doubleclicked val do
		(

			rsTP.loadCachedTile exportArray[val].fileName
			max tool zoomextents
				
		)
		
		on edFilterList changed val do
		(
			if val != "" then chkFilterCheck.checked = true else chkFilterCheck.checked = false
			local newArray = gterrainFileObjTools.filterText mlbTerrainTiles.items val 
			mlbTerrainTiles.items = newArray
		)	
		
		on btnProcessSelected pressed do (		
			
			filein "pipeline/helpers/terrain/terrain_processing.ms"
			clearListener()
			
			rsTP.settings.silent = chkSilent.state
			rsTP.settings.vfb = chkVfb.state
			rsTP.settings.processBakingTextures = chkDoMaps.state
			rsTP.settings.processCreateLod = chkDoLOD.state
			rsTP.settings.imageHeight	= spnMapSizeX.value
			rsTP.settings.imageWidth	= spnMapSizeY.value
			rsTP.settings.mapFinalQuality = chkMapQual.state
			rsTP.settings.debug = chkDebug.state
			rsTP.settings.checkOutContainer = chkDoCheckOutCont.state
			rsTP.settings.proceedWithErrors = chkProceedWithErrors.state
			rsTP.settings.finalBake = chkDoFinalBake.state
			
			local BatchStart = timeStamp()
			local theProcessFiles = lbProcessList.items 
			local taskComplete = true
			
			local exportGivers = #()
			local exportReceivers = #()
			for item in 1 to lbProcessList.items.count do (
				
				if exportArray[item].tiletype then append exportReceivers exportArray[item] else
					append exportGivers exportArray[item]
				
			)
				
			
			for f in 1 to exportGivers.count do 
			(
				try (			
					rsTP.pairedMeshes = #()	
					rsTP.settings.hasError = false
					rsTP.process exportGivers[f].theFile
				) catch (-- 					
					local msg = ( "*** " +  (getCurrentException()) + " ***\n")
					if exportGivers[f] != undefined then (
						rsTP.doWarningsErrors msg toListener:true theProcessedMesh:exportGivers[f].theFile
					)
				)		
			)
			
			for f in 1 to exportReceivers.count do 
			(
				try (		
					rsTP.pairedMeshes = #()	
					rsTP.settings.hasError = false
					rsTP.process exportReceivers[f].theFile
				) catch (-- 					
					local msg = ( "*** " +  (getCurrentException()) + " ***\n")
					if exportReceivers[f] != undefined then (
						rsTP.doWarningsErrors msg toListener:true theProcessedMesh:exportReceivers[f].theFile
					)
				)			
			)

			local BatchEnd = timeStamp()
			
			if taskcomplete == true then (
				format "\n****************************************************************\n"	
				format "****************************************************************\n"
				format "****************************************************************\n\n"
				format "Batch Processed % Terrain Files in % Seconds\n"  theProcessFiles.count ((BatchEnd - BatchStart)/ 1000.0)
				format "\n****************************************************************\n"	
				format "****************************************************************\n"
				format "****************************************************************\n\n"

				max tool zoomextents all		

			)
			grsTPULog.validate()
		)		
		
		on btnRefreshTileList pressed do (	
				mlbTerrainTiles.items = #()
				gterrainFileObjTools.fileArray = #()
				gterrainFileObjTools.filteredArray = #()			
				gterrainFileObjTools.makeFileArray()
				mlbTerrainTiles.items = gterrainFileObjTools.makeDisplayNameArray gterrainFileObjTools.filteredArray
				
				chkFilterCheck.state = false
				edFilterList.text = ""
				local newArray = gterrainFileObjTools.filterText mlbTerrainTiles.items ""
				mlbTerrainTiles.items = newArray		
		)
		
		on rsTerrain_Processing_Rollout open do
		(
			--banner.filename = ""
			--banner.setup()	
			gterrainFileObjTools.fileArray = #()
			gterrainFileObjTools.filteredArray = #()
			gterrainFileObjTools.makeFileArray()
		
			mlbTerrainTiles.items = gterrainFileObjTools.makeDisplayNameArray gterrainFileObjTools.filteredArray
			
			chkFilterCheck.state = false
			edFilterList.text = ""
			local newArray = gterrainFileObjTools.filterText mlbTerrainTiles.items "" 
			mlbTerrainTiles.items = newArray
		
		)	
		
		on rsTerrain_Processing_Rollout close do
		(
			gterrainFileObjTools.filteredArray = #()
			exportArray =#()	
		)

	)

	rollout terrainToolPanel "" width:250 height:400
	(

		local setErrors =  (if terrainTabs_Rollout != undefined then terrainTabs_Rollout.setErrors  else #())
		
		groupbox grpC "Copy Vert Info "pos:[5,5] width:240 height:230
		label lblcopy1 "Copies vert info between two meshes." pos:[grpC.pos.x+10,grpC.pos.y+20]	 
		label lblcopy2 "Selection[1] Is Giver." pos:[grpC.pos.x+10,grpC.pos.y+40]	 
		label lblcopy3 "Selection[2] Is Receiver." pos:[grpC.pos.x+10,grpC.pos.y+55]	
			
		label vertSelectLbl "Vert Test:" pos:[grpC.pos.x+10,grpC.pos.y+75]
		checkbox checkBordersOnlyChk1 "Border" width:60 checked:false pos:[grpC.pos.x+10,grpC.pos.y+90]
		checkbox checkBordersOnlyChk2 "Selected" width:60 checked:true pos:[grpC.pos.x+90,grpC.pos.y+90]
		checkbox checkBordersOnlyChk3 "All" width:50 checked:false pos:[grpC.pos.x+180,grpC.pos.y+90]	
			
		checkbox copyCPVBoolChk "Copy CPVs" width:90 checked:true pos:[grpC.pos.x+10,grpC.pos.y+120]
		checkbox copyNormalsBoolChk "Copy Normals" width:90 checked:true pos:[grpC.pos.x+10,grpC.pos.y+140]
			
		button applyEditPolyBtn "Apply Edit Poly" width:120 pos:[grpC.pos.x+105,grpC.pos.y+115]
	
		checkbox ignoreWorldBoundsChk "Ignore World Bounds " width:130 checked:true pos:[grpC.pos.x+105,grpC.pos.y+140]
		checkbox snapvertsChk "Snap Verts" width:90 checked:true pos:[grpC.pos.x+10,grpC.pos.y+160]
		spinner toleranceSpn "Tolerance" pos:[grpC.pos.x+144,grpC.pos.y+161] width:80 range:[0.001,100.000,0.010] type:#float
			
		button processMeshesBtn "Copy Vert info with Selected Meshes" width:215 pos:[grpC.pos.x+10,grpC.pos.y+190] height:30	 
		
		groupbox grpD "Display"pos:[5,245] width:240 height:145
		button btnToggleVColor "Toggle Display Vertex Colors on Selected" width:215 pos:[grpD.pos.x+10,grpD.pos.y+20]
		button btnApplyChecker "Assign Checker Material" width:215 pos:[grpD.pos.x+10,grpD.pos.y+45]
		button btnHideLods "Hide LODs"  width:215 pos:[grpD.pos.x+10,grpD.pos.y+70]
		button btnopenExplorer "New Scene Explorer" width:215 pos:[grpD.pos.x+10,grpD.pos.y+95]

		groupbox grpV "Validate"pos:[5,400] width:240 height:100
		button btnValidate "Validate Container(s)" width:215  pos:[grpV.pos.x+10,grpV.pos.y+20] height:70
		
		subrollout containerTools "" pos:[250,10] width:240 height:490

		on checkBordersOnlyChk1 changed state do if state then (checkBordersOnlyChk2.state = false ; checkBordersOnlyChk3.state = false)		
		on checkBordersOnlyChk2 changed state do if state then (checkBordersOnlyChk1.state = false ; checkBordersOnlyChk3.state = false)
		on checkBordersOnlyChk3 changed state do if state then (checkBordersOnlyChk1.state = false ; checkBordersOnlyChk2.state = false)
		
		on btnApplyChecker pressed do
		(
			
			newMat = standardmaterial()
			newmat.diffusemap = checker()
			newmat.diffusemap.coords.U_Tiling = 50 
			newmat.diffusemap.coords.V_Tiling = 50 
			
			$.material = newMat
			showTextureMap$.material $.material.diffusemap on
			displayColor.shaded = #material				
		)
		
		on applyEditPolyBtn pressed do (
			
			-- Applies edit poly modifier and goes to vertex sublevel	
			
			-- Makes sure selection count is two
			if $selection.count == 2 then (
				
				max modify mode
				modPanel.addModToSelection (Edit_Poly ()) ui:on
				subobjectLevel = 1
					
			)  else messagebox "Please select only 2 objects [1] Is Giver [2] Is Receiver" title:"Error"
			
		)

		on processMeshesBtn pressed do
		(
			
			-- Makes sure selection count is two
			if $selection.count == 2 then (
				
				-- setup terrain processor
				filein "pipeline/helpers/terrain/terrain_processing.ms"
				
				local terrainProcessObj = rsTPStruct()
				
				terrainProcessObj.settings.proceedWithErrors = true
				terrainProcessObj.settings.silent = false
				
				local giverMesh = (selection as array)[1]
						
				if giverMesh != undefined then
				(
					local recMesh = (selection as array)[2]
					if recMesh != undefined then
					(
						
						undo on (
							maxOps.CollapseNode recMesh off
							maxOps.CollapseNode giverMesh off
						)
						
						-- Lets get the list of giver verts							
						local vertexPairsToProcess = terrainProcessObj.getVertPairs recMesh giverMesh toleranceSpn.value allVerts:checkBordersOnlyChk3.state selectUnpairedVerts:false onlyBorderVerts:checkBordersOnlyChk1.state testGrid:ignoreWorldBoundsChk.state

						format "\n\n------------>%\n\n" vertexPairsToProcess
					
						if vertexPairsToProcess.count > 0 then (
							
							local datapairsToProcess = terrainProcessObj.vertPairsInfo2dataPairs vertexPairsToProcess

							-- do selected processes 
							for item in datapairsToProcess do (			

								if copyCPVBoolChk.state then terrainProcessObj.copyNormals recMesh (getnodebyName item.gMeshName) item.gArray[1]  item.gArray[2]
								if copyNormalsBoolChk.state then terrainProcessObj.copyCPVs recMesh (getnodebyName item.gMeshName) item.gArray[1]  item.gArray[2]		
								if snapvertsChk.state then terrainProcessObj.snapVerts recMesh (getnodebyName item.gMeshName) item.gArray[1]  item.gArray[2]		
							)												
						)	else max undo	
	
					)
				)	
				
			) else messagebox "Please select only 2 objects [1] Is Giver [2] Is Receiver" title:"Error"
		)
		
		on btnToggleVColor pressed do (
			
			-- quick swap to show vertex colors
			displayColor.shaded = #object
			for sel in ($selection as array) do
			(
				try (
					if sel.showVertexColors == true then sel.showVertexColors = false else sel.showVertexColors = true
					sel.vertexColorType =  #color
				) catch ()				
			)
			
		)		
		
		on btnopenExplorer pressed do (
			
			SceneExplorerManager.CreateADefaultExplorer "Terrain"
		)
		
		on btnValidate pressed do (
			
			-- Clear and run terrain processing struct 
			filein "pipeline/helpers/terrain/terrain_processing.ms"
			
			--clear ulog 
			grsTPULog.clearProcessLogDirectory()
			rsTP = rsTPStruct()				
			rsTP.settings.silent = true
			rsTP.settings.debug = false
			rsTP.settings.proceedWithErrors = true
			
			
			
			local allContainers = #() 

			for o in $objects do if classof o == container then append allContainers o				
				
			for cont in allContainers do (
				
				if cont.IsOpen()  != false then (
					
					foundContainers = false
					local containerNode = cont 
					
					if containerNode.localDefinitionFilename != "" or containerNode.localDefinitionFilename != undefined then (
						
						foundContainers = true
						-- Load adjacency list info into memory based on loaded container.
						RsTerrainLoadAdjacencyList()		
						
					) else grsTPULog.LogError "Error with Container. Is it Open for Edit?" 
					
					if foundContainers then (
						
						-- Determine if container is giver or receiver.
						local isReceiver = rsTP.determineReceiverState containerNode
						
						(
							if (isReceiver) then tileType = "Receiver" else 
							if not (isReceiver) then tileType = "Giver" 	
							
						)
						
						format "Tile % is: %\n" containerNode.name tileType		
			
						-- If receiver, then bring in adjacent cached tiles and then do tests.
						if isReceiver then
						(
							
							local givers = #()			
								
							-- Create combined terrain tile.
							global combinedTerrain = rsTP.combineTerrain containerNode
							
							if combinedTerrain != undefined then (								

								-- Weld vertices of combined terrain.
								rsTP.weldVertices combinedTerrain
								
								select combinedTerrain		
									
									
							)  else grsTPULog.LogError "Error Combining Terrain. Check Container is Open?"
							
							-- test the object bounds
							local  testObj = rsTP.boundInfoCheck combinedTerrain
							local passTest = testobj.passtest
							local failedVerts =testobj.failedverts
								
							-- we found some errors. Lets make some geo that shows the errors.
							if not passTest and failedVerts.count > 0 then (
									
								polyop.setvertselection combinedTerrain failedVerts
								rsTP.makeErrorDisplay combinedTerrain SetErrors[2][1] SetErrors[2][2]  (containerNode.name + "_" +SetErrors[2][3]) 
								

							)	
							
							local adjacencyInfo = TerrainAdjacencyInfoArray.GetInfo containerNode									
								
							for adjacentContainerName in adjacencyInfo.adjacencyList do (
									
								local adjacentContainer = rsTP.loadCachedTile adjacentContainerName
								append givers adjacentContainer
									
							)				

							for g in givers do (format "Giver: %\n" g.name)							

							
							--find the matching pairs of verts from the givers to process
							format "Testing for non Vert Matches\n"
							local vertexPairsToProcess = rsTP.getVertPairs combinedTerrain givers 0.01 selectUnpairedVerts:true onlyBorderVerts:true  testGrid:true MultipleMatch:false
							if vertexPairsToProcess != false then (
								polyop.setvertselection combinedTerrain (polyop.getvertselection combinedTerrain - failedVerts as bitarray)
								rsTP.makeErrorDisplay combinedTerrain SetErrors[3][1] SetErrors[3][2]  (containerNode.name + "_" +SetErrors[3][3]) 
							)

							-- Test for multiple vert matches.		
							format "Testing for Multiple Vert Matches\n"
							local vertexPairsToProcess1 = rsTP.getVertPairs combinedTerrain givers 0.01 selectUnpairedVerts:true onlyBorderVerts:true  testGrid:true MultipleMatch:true
							if vertexPairsToProcess1 != false then (							
								polyop.setvertselection combinedTerrain (polyop.getvertselection combinedTerrain - failedVerts as bitarray)
								rsTP.makeErrorDisplay combinedTerrain SetErrors[4][1] SetErrors[4][2]  (containerNode.name + "_" +SetErrors[4][3]) 
							)

							
							allTemps = join #(combinedTerrain) givers 
							delete allTemps
						)
						-- The container to process is a giver just check bounds
						else 
						(		
							
							-- Create combined terrain tile.
							local combinedTerrain = rsTP.combineTerrain containerNode
							
							if combinedTerrain != undefined then (
								
								-- Weld vertices of combined terrain.
								rsTP.weldVertices combinedTerrain
								select combinedTerrain		

								-- test the object bounds
								local  testObj = rsTP.boundInfoCheck combinedTerrain
								local failedVerts = testObj.getFailedVerts()

								-- we found some errors. Lets make some geo that shows the erros.
								if failedVerts.count > 0 then (
									
									polyop.setvertselection combinedTerrain failedVerts
									rsTP.makeErrorDisplay combinedTerrain SetErrors[2][1] SetErrors[2][2]  (containerNode.name + "_" +SetErrors[2][3]) 
									delete combinedTerrain
									
									
								) 
								-- no errors. All is good.
								else (
									delete combinedTerrain
									format "No Errors\n"
								)
								
							) else grsTPULog.LogError "Error Combining Terrain. Check Container is Open?"
							
						)
						
					)	else  grsTPULog.LogError "No Valid Containers Found" 	
					
				
				)	else grsTPULog.LogError "Could not access container. Is it open?" 	
			)
			
			if allContainers.count == 0  then grsTPULog.LogError "No Open Containers Found" 	
			grsTPULog.validate()
			
		)
					
		on btnHideLods pressed do (		
			
			for obj in $objects where ( classof obj != Col_Mesh ) do (
				
				-- hide LOD meshes.
				if  ( matchPattern obj.name pattern:"*_LOD*" ignoreCase:true ) then (
					hide obj
				)
			)
		)

		
		on terrainToolPanel open do (
			try (destroyDialog RsContainersRoll) catch ()
			addSubrollout ContainerTools RsContainersRoll
			
		)
		
	)
	
	global  terrainTab_array = #(#("Terrain Processing",#(rsTerrain_Processing_Rollout),650),#("Terrain Tools",#(terrainToolPanel),650))   

	rollout terrainTabs_Rollout "Tab Rollout"
	(	
		-- array of errors to set error geometry		
		local setErrors = #(
			#((color 0 0 0),  (color 0 255 0), "ERRORS_UNDEFINED"),
			#((color 0 0 0),  (color 255 0 255), "ERRORS_BAD_BOUNDS"),
			#((color 0 0 0),  (color 255 0 0), "ERRORS_NO_MATCH"),
			#((color 0 0 0),  (color 255 255 0), "ERRORS_MUlTIPLE_VERT_MATCH")
		)
		
		local Error1 =  bitmap 10 10 color:setErrors[1][2]
		local Error2 =  bitmap 10 10 color:setErrors[2][2]
		local Error3 =  bitmap 10 10 color:setErrors[3][2]
		local Error4 =  bitmap 10 10 color:setErrors[4][2]
		
		dotNetControl rsBannerPanel "Panel" epos:[0,0] height:32 width:terrainTabs_Rollout.width offset:[-13,-5]
		local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"" 
			
		dotNetControl dn_tabs "System.Windows.Forms.TabControl" height:20 width:200 align:#left
		subRollout theSubRollout width:510 height:540 align:#center offset:[0,0]
		
	
		Label ErrorsColors "Error Key:"  pos:[10,605]
		imgTag  imgError1 ""  bitmap:Error1 pos:[70,610]
		label lblError1 ": Undefined Error" pos:[85,608]
		imgTag  imgError2 ""  bitmap:Error2 pos:[175,610]
		label lblError2 ": Not On Bounds" pos:[190,608]
		imgTag  imgError3 ""  bitmap:Error3 pos:[280,610]
		label lblError3 ": No Matching Vert" pos:[295,608]
		imgTag  imgError4 ""  bitmap:Error4 pos:[70,630]
		label lblError4 ": Multiple Matching Vert" pos:[85,628]
		
		on dn_tabs Selected itm do
		(
			if LastSubRollout != (itm.TabPageIndex+1) do --do not update if the same tab clicked twice
			(
				for subroll in terrainTab_array[LastSubRollout][2] do
								removeSubRollout theSubRollout subroll
				for subroll in terrainTab_array[LastSubRollout = itm.TabPageIndex+1][2] do                                                                                                                                      
								addSubRollout theSubRollout subroll
				terrainTabs_Rollout.height = terrainTab_array[LastSubRollout = itm.TabPageIndex+1][3]                                                          
			) 
		)--end tabs clicked
-------------------------------------------------------------------------------------------------------------------------
		on terrainTabs_Rollout open do
		(
			try (destroyDialog RsContainersRoll) catch ()
			for aTab in terrainTab_array do
							(dn_tabs.TabPages.add aTab[1])
			for subroll in terrainTab_array[1][2] do
							addSubRollout theSubRollout subroll
			banner.setup()
		)
	)
)

createdialog terrainTabs_Rollout 510 650 style:#(#style_resizing,#style_titlebar, #style_toolwindow, #style_sysmenu)
