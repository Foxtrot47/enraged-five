--
-- Terrain Functions 
-- A set of helper functions to determine if the given mesh is marked as terrain.
--

-- General function to determine if a mesh is marked as terrain.  Currently, we determine if the rageShader
global gTerrainProcessor = dotNetObject "RSG.MaxUtils.TerrainProcessing"
global gTerrainProcessingMode = (dotNetClass "RSG.MaxUtils.ProcessingMode")	

-- Determines if this mesh uses a terrain-material:
fn RsIsTerrainMesh meshObj =
(
	-- Get materials used on object's faces:
	local ObjMats = RsGetMaterialsOnObjFaces meshObj
	
	local isTerrain = False
	
	-- Check each material, stop when a terrain-shader is found:
	for ThisMat in ObjMats while (not isTerrain) do 
	(
		if (isKindOf ThisMat Rage_Shader) do 
		(
			isTerrain = (matchPattern (rstGetShaderName ThisMat) pattern:"*terrain*")
		)
	)
	
	return isTerrain
)

fn RsTerrainLoadContainer containerObject = 
(
	if ( containerObject.unloaded == true or containerObject.IsOpen() == false) then
	(
		print ("Loading container " + containerObject.name + "...")
		containerObject.LoadContainer()
		containerObject.SetOpen true
		containerObject.MakeUnique()
	)
)

fn RsTerrainUnloadContainer containerObject =
(
	print ("Unloading container " + containerObject.name + "...")
	containerObject.UnloadContainer()
)

fn RsTerrainIsContainerLoaded containerObject =
(
	(containerObject.unloaded and containerObject.IsOpen())
)

-- Acquires a list of all terrain meshes from a root object (generally a container).
fn RsTerrainGatherMeshes rootObject = 
(
	if rootObject == undefined then
	(
		#()
	)
	else
	(			
		local meshArray = #()
		for child in rootObject.children do 
		(
			local childClass = classof child
			
			if ( Editable_Poly == childClass or Editable_Mesh == childClass or PolyMeshObject == childClass) then
			( 
				--Only process non-LOD meshes and meshes marked as terrain (using the terrain shader).
				if ( findString child.name "_LOD" == undefined and (RsIsTerrainMesh child)  == true ) then
				(
					ConvertToMesh child
					CollapseStack child
					append meshArray child
				)
			)
		)
		
		meshArray
	)
)

fn RsTerrainExpandBounds minBB maxBB currentBounds = 
(
	local minBounds = currentBounds[1]
	local maxBounds = currentBounds[2]
	
	if ( minBB.x < minBounds.x ) then 
	(
		minBounds.x  = minBB.x
	)
	
	if ( minBB.y < minBounds.y ) then 
	(
		minBounds.y  = minBB.y
	)
	
	if ( minBB.z < minBounds.z ) then 
	(
		minBounds.z = minBB.z
	)
	
	if ( maxBB.x > maxBounds.x ) then 
	(
		maxBounds.x = maxBB.x
	)
	
	if ( maxBB.y > maxBounds.y ) then 
	(
		maxBounds.y = maxBB.y
	)
	
	if ( maxBB.z > maxBounds.z ) then 
	(
		maxBounds.z = maxBB.z
	)
	
	#(minBounds, maxBounds)
)

fn RsTerrainGetMaxObjectBounds obj currentBounds = 
(
	local boundingbox = nodeGetBoundingBox obj (matrix3 1)
	
	local minBB = boundingbox[1]
	local maxBB = boundingbox[2]
	
	currentBounds = RsTerrainExpandBounds minBB maxBB currentBounds
	
	for child in obj.children do
	(
		currentBounds = RsTerrainGetMaxObjectBounds child currentBounds
	)
	
	currentBounds
)

-- Acquires the list of all terrain meshes and determines their combined bounds.
fn RsTerrainGetContainerBounds containerObject = 
(	
	meshArray = RsTerrainGatherMeshes containerObject

	minBounds = undefined
	maxBounds = undefined 
	for meshObject in meshArray do
	(
		local boundingbox = nodeGetBoundingBox meshObject (matrix3 1)
		
		local minBB = boundingbox[1]
		local maxBB = boundingbox[2]
		
		if ( minBounds == undefined and maxBounds == undefined ) then
		(
			minBounds = minBB
			maxBounds = maxBB
		)

		local objectBounds = RsTerrainGetMaxObjectBounds meshObject #(minBounds, maxBounds)
		minBounds = objectBounds[1]
		maxBounds = objectBounds[2]
	)
	
	#(minBounds, maxBounds)
)

---------------------------------------------------------
--- Adjacency List
---------------------------------------------------------
global TerrainAdjacencyListFile = RsProjectGetMetadataDir() + "/terrain/AdjacencyList.xml"
global TerrainAdjacencyInfoArray = undefined

struct TerrainAdjacencyInfo
(
	containerObject = undefined,
	receiveState = undefined,
	adjacencyList = undefined
)

struct AdjacencyInfoArray
(
	infoArray = #(),
	
	fn AddInfo adjInfo = 
	(
		append infoArray adjInfo
	),
	
	fn GetInfo containerNode = 
	(
		for normalIndex=1 to infoArray.count do
		(
			if ( infoArray[normalIndex].containerObject.name == containerNode.name ) then
				return infoArray[normalIndex]
		)
		
		undefined
	)
)

-- Inherit adjacent containers into the scene of the supplied container node.
fn RsInheritAdjacentContainers containerNode = 
(
	
	local success = false

	-- Make sure we passed in a valid container node.
	if containerNode != undefined and ( classOf containerNode == Container ) then (

		-- Load in the adjacency list.
		RsTerrainLoadAdjacencyList()
		
		local adjacencyInfo = TerrainAdjacencyInfoArray.GetInfo containerNode
		
		if adjacencyInfo != undefined then (

			-- Iterate over each adjacent container and attempt to inherit it into the current scene.
			for adjacentContainerName in adjacencyInfo.adjacencyList do (
				
				-- Double check the container doesn't already exist in the scene.
				local adjacentContainerNode = getNodeByName adjacentContainerName
				
				-- If the container is already in the scene, skip trying to load it.
				if ( classOf adjacentContainerNode == Container ) then (
					print ( "Container (" + adjacentContainerName + ") is already loaded." )
					
					continue
					
				-- There is an object in the scene that is not a container, but named the same as a container we are trying to load.
				-- Warn the user to fix this.
				) else if not adjacentContainerNode == undefined then (
					local errorMsg = "There is an object in the scene named (" + adjacentContainerName + ") but is not a container!  This name is reserved for a container object and you should fix this immediately.\n\nHalting." 
					messageBox errorMsg
					
					return undefined
				)
					
				local adjacentContainerExists = true
				local alreadySynced = false
				
				-- TODO: Will probably need to recursively look for the container file, since it might not always be under the terrain or models folder.
				local adjacentContainerFilename = RsConfigGetModelSourceDir() + "terrain\\" + adjacentContainerName + ".maxc"
				
				-- The container file doesn't exist on the local drive, so attempt to find it in Peforce.
				if ( getFiles adjacentContainerFilename ).count == 0 then (
					
					-- See if the file is in Perforce.  If it is, sync.
					if ( gRsPerforce.exists #( adjacentContainerFilename ) ).count > 0 then (
						print ( "Syncing adjacent container (" + adjacentContainerFilename + ") because it could not be found on your local harddrive." )
						
						gRsPerforce.sync adjacentContainerFilename force:true
						
						-- Avoid another P4 sync if possible.
						alreadySynced = true
					
					-- Otherwise, file doesn't exist.						
					) else (
						print ( "Adjacent container (" + adjacentContainerFilename + ") could not be found in Perforce or on disk!" )
						
						adjacentContainerExists = false
					)
				)
				
				-- Inherit the container.
				if adjacentContainerExists == true do (
					
					-- Sync to the latest version.
					if not alreadySynced then (
						local fstats = ( gRsPerforce.getFileStats adjacentContainerFilename )[ 1 ]
						local haveRev = fstats.Item[ "haveRev" ] as integer
						local headRev = fstats.Item[ "headRev" ] as integer
						
						if haveRev <  headRev then (
							print ( "Syncing to head revision of adjacent container (" + adjacentContainerFilename + ").  You had revision #" + haveRev as string + " of #" + headRev as string + "." )
							
							gRsPerforce.sync adjacentContainerFilename
						)
					)
					
					Containers.CreateInheritedContainer adjacentContainerFilename
					success = true
				)
			)

		-- There was no adjacency info found for the supplied container.
		) else (
			local msg = "There were no adjacent containers found for the container (" + containerNode.name + ")!"
			messageBox msg
		)
	) else (
		print ( "Object (" + containerNode.name + ") is not a valid container object!" )
		
	)

	success
)

fn RsTerrainLoadAdjacencyList = 
(
	if (getfiles TerrainAdjacencyListFile).count == 0 then 
	(
		print (TerrainAdjacencyListFile + " does not exist.")
		return undefined
	)
 
	adjacencyList = dotNetObject "RSG.MaxUtils.AdjacencyList"
	adjacencyList = adjacencyList.Load TerrainAdjacencyListFile
	
	if ( adjacencyList == undefined ) then
	(
		print ("Unable to load " + TerrainAdjacencyListFile + ".")
		return undefined
	)

	TerrainAdjacencyInfoArray = AdjacencyInfoArray()
	
	for child in rootnode.children do 
	(
		local childClass = classof child
		if ( Container == childClass ) then
		(
			local adjList = adjacencyList.GetAdjacencyList(child.name)
			local isReceiver = adjacencyList.IsReceiver(child.name)
		
			adjacencyInfo = TerrainAdjacencyInfo containerObject:child receiveState:isReceiver adjacencyList:adjList
			TerrainAdjacencyInfoArray.AddInfo adjacencyInfo
		)
	)
	
	adjacencyList
)

fn RsTerrainSaveAdjacencyList containerAdjacencyList =
(
	adjacencyList = dotNetObject "RSG.MaxUtils.AdjacencyList"
	
	for adjIndex=1 to containerAdjacencyList.count do 
	(
		local entry = containerAdjacencyList[adjIndex]
		local containerNode = getNodeByName entry[1]
		local normalInfo = GetNormalisationInfo containerNode
		
		local entryItem = adjacencyList.Add entry[1] normalInfo.receiveState
		
		print ("Container: " + entry[1] as string)
		
		for adjListIndex=1 to entry[2].count do
		(
			print ("\t" + entry[2][adjListIndex])
			entryItem.AddAdjacentContainer(entry[2][adjListIndex])
		)
	)
	
	adjacencyList.Save adjacencyList TerrainAdjacencyListFile
)

fn RsTerrainPrintAdjacencyList adjacentList = 
(
	for adjIndex=1 to adjacentList.count do 
	(
		local entry = adjacentList[adjIndex]
		print ("Container: " + entry[1] as string)
		
		for adjListIndex=1 to entry[2].count do
		(
			print ("\t" + entry[2][adjListIndex])
		)
	)
)

