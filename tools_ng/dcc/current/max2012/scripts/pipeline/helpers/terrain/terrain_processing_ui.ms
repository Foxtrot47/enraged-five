
filein ( RsConfigGetWildwestDir() + "script/3dsmax/simplygon/simplygon.ms" )
filein "pipeline/export/peds/utils.ms"
filein "pipeline/helpers/terrain/terrain_processing.ms"
filein "rockstar/export/settings.ms"
filein "pipeline/helpers/terrain/edgenormalisation.ms"
filein "pipeline/helpers/terrain/edgeprocessing.ms"
filein "pipeline/helpers/terrain/terrain_lod.ms"
filein "pipeline/helpers/terrain/terrain_general.ms"




struct terrainFileNameObj (
	
	theFile = undefined,
	filePath = undefined,
	fileName = undefined,
	fileType = undefined,
	DisplayName = undefined,
	adjacencyList = #(),
	TileType = undefined
)


struct terrainFileObjTools (
	
	fileArray = #(),
	filteredArray = #(),
		
	usePrefix = false,
	useSuffix = false	,	

	fn filterText textArray theText prefix:false suffix:false= 
	(
		
		filteredArray = #()
		
		if theText == "" then 
		(
			this.filteredArray = this.fileArray		
			
		) else (
			
			local theArray = this.fileArray	
			local displayNameList = for f in theArray collect f.DisplayName			
			for i in 1 to displayNameList.count do
			(
				lookForString = findString displayNameList[i] theText
				if lookForString != undefined then append this.filteredArray theArray[i]
			)			
		)
		
		qsort this.filteredArray this.compareFN
		local filtList = #()
		for f in this.filteredArray do (
			local theName = ""
			if prefix then append theName f.DisplayPrefix
			append theName f.DisplayName
			append filtList theName
			if suffix then append theName f.DisplaySuffix
		)

		filtList
	),
	
	fn compareFN f1 f2 =
	(
		local f1Name = f1.DisplayName
		local f2Name = f2.DisplayName
		local sortOrder = sort #(f1Name,f2Name)
		
		case of
		(
			(sortOrder[1] == f1Name): -1
			(sortOrder[1] == f2Name): 1
			default: 0
		)
	),
	
	fn makeFileArray theAdjacencyList = (
		
		theArray = theAdjacencyList.entries
		progressStart "Creating Terrain List" 
		for f in 1 to theArray.count do (
			
			progressUpdate ((f as float)/(theArray.count as float) * 100.0)
			newFileObj = terrainFileNameObj()
			newFileObj.theFile = rsTP.getTerrainContainerFullFilename theArray[f].containerName
			newFileObj.filePath = getfilenamepath newFileObj.theFile
			newFileObj.fileName = getfilenamefile newFileObj.theFile
			newFileObj.fileType = getfilenametype newFileObj.theFile
			newFileObj.DisplayName = getfilenamefile newFileObj.theFile
			newFileObj.adjacencyList = theAdjacencyList.GetAdjacencyList(getfilenamefile newFileObj.theFile)
			newFileObj.tileType = theAdjacencyList.IsReceiver(getfilenamefile newFileObj.theFile)
			append this.fileArray newFileObj
		)		
		qsort this.fileArray this.compareFN
		this.filteredArray = this.fileArray	
		progressEnd()		
	),
	
	fn makeDisplayNameArray theArray = (	
		
		local displayNameArray = for f in theArray collect f.DisplayName
		displayNameArray
	),

	fn makeSelectedArray theItem outputArray = (		

		local displayNameArray = makeDisplayNameArray outputArray 

		if classof theItem == integer then (
			local inList = finditem displayNameArray this.filteredArray[theItem].displayName
			if inList == 0 then (
				append outputArray this.filteredArray[theItem]
				
			)
			qsort outputArray this.compareFN
			displayNameArray = for f in outputArray collect f.displayname
		)
		displayNameArray		
	)
	
)

global  gRsTerrainLoadAdjacencyList= RsTerrainLoadAdjacencyList()
global gterrainFileObjTools = terrainFileObjTools()	 

try destroydialog rsTerrain_Processing_Rollout catch()

rollout rsTerrain_Processing_Rollout "Terrain Processing"
(	

	local exportArray =#()	
	local jobsFileName =  (RsConfigGetToolsDir()+"etc/config/simplygon/peds_all.xml")	 	
	local jobLocation = (RsConfigGetToolsDir()+"dcc/current/max2012/scripts/pipeline/util/")
	local doJob = ""
	
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:rsTerrain_Processing_Rollout.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Terrain Processing" filename:(getThisScriptFilename())	
		
	group "Terrain Processing" (
		
		button btnRefreshTileList "Load/Refresh Tile List" width:(rsTerrain_Processing_Rollout.width * 0.9)
		checkbox chkFilterCheck ""  align:#right 
		editText edFilterList "Filter:" labelontop:true  align:#left offset:[0,-20]		
		multiListBox mlbTerrainTiles "Terrain Tiles:" labelontop:true  height:10
		label lblProcessNumber "0"  align:#left offset:[84,0] 
		multilistbox lbProcessList "Tiles to Process:"  labelontop:true  height:10 offset:[0,-18] 
	
		button btnRemoveSelected "Remove Selected" width:100 align:#right offset:[0,0]  
		
		editText edFilePath "File Path:"  labelontop:true  align:#left offset:[0,-10] readonly:true height:20
		
		checkbox chkLoadAdj "Load Adjacent" align:#left 
		editText edAdjacentfiles "Adjacent Tiles:"  labelontop:true  align:#left readonly:true height:20 
		
		editText edTileClass "Tile is  :   "  align:#left readonly:true offset:[0,5] height:20
		checkbox chkDoMaps "Process Maps"  offset:[0,10] checked:true
		checkbox chkVfb "vfb" checked:false
		spinner spnMapSizeX "Map Size X:"  align:#right range:[16,4096,2048] type:#integer width:80 offset:[0,-20]
		checkbutton btnLock "[" align:#left offset:[90,-15] checked:true
		spinner spnMapSizeY "Map Size Y:" align:#right range:[16,4096,2048] type:#integer width:80 offset:[0,-13]
		checkbox chkDoLOD "Create LOD" offset:[0,00] checked:true
		checkbox chkSilent "Silent" offset:[0,10] checked:false
		
		button btnProcessSelected "Process Files" offset:[ 0,5] align:#center	width:(rsTerrain_Processing_Rollout.width * 0.9)
	)	
	
	group "Display" (
		
		button btnToggleVColor "Toggle Display Vertex Colors on Selected" offset:[ 0,0] align:#center	width:(rsTerrain_Processing_Rollout.width * 0.9)
		
	)
	
	on btnToggleVColor pressed do (
		
		displayColor.shaded = #object
		for sel in ($selection as array) do
		(
			try (
				if sel.showVertexColors == true then sel.showVertexColors = false else sel.showVertexColors = true
				sel.vertexColorType =  #color
			) catch ()				
		)
		
	)

	on spnMapSizeX changed val do if btnLock.state then spnMapSizeY.value = val
		
	on spnMapSizeY changed val do if btnLock.state then spnMapSizeX.value = val
		
	on btnLock changed  val do (
		
		if val then spnMapSizeY.value = spnMapSizeX.value 
	)	
		
	on chkFilterCheck changed val do
	(
		if val == false then (
			
			edFilterList.text = ""
				local newArray = gterrainFileObjTools.filterText mlbTerrainTiles.items "" prefix:false suffix:false
			mlbTerrainTiles.items = newArray
			
		)
		
	)
	
	on btnRemoveSelected  pressed do (
		
		local val = lbProcessList.selection as array
		if val.count > 0 then (		
			
			for i in val.count to 1 by -1  do (
				
				deleteitem exportArray val[i]
			)		
			lbProcessList.items = gterrainFileObjTools.makeSelectedArray val exportArray				
		)
		lblProcessNumber.text = (exportArray.count as string)
		lbProcessList.selection = 0
		edFilePath.text = ""
		edAdjacentfiles.text = ""
		edTileClass.text = ""
	)

 	on mlbTerrainTiles selectionEnd do (	
		
		if  ( mlbTerrainTiles.selection as array).count > 1 then (
			local displayNameArray = gterrainFileObjTools.makeDisplayNameArray exportArray 		
			
			for val in (mlbTerrainTiles.selection as array) do (			

				local inList = finditem displayNameArray gterrainFileObjTools.filteredArray[val].displayName
				if inList == 0 then (
					append exportArray gterrainFileObjTools.filteredArray[val]
				)				
			)		
			lbProcessList.items = gterrainFileObjTools.makeSelectedArray false exportArray
		)
		lblProcessNumber.text = (exportArray.count as string)
 	)
		
	on mlbTerrainTiles doubleclicked val do
	(
		lbProcessList.items = gterrainFileObjTools.makeSelectedArray val exportArray
		lblProcessNumber.text = (exportArray.count as string)
	)
	
	on mlbTerrainTiles rightclick do
	(
		local val = mlbTerrainTiles.selection
		if (val  as array).count == 1 then (	
			val =  (val as array)[1]
			loadmaxfile gterrainFileObjTools.filteredArray[val].thefile
		)
	)
	
	on lbProcessList selected val do
	(
		vals = (lbProcessList.selection as array)
		if vals.count == 1  then (
			val = vals[1]
			edFilePath.text = exportArray[val].theFile
			edAdjacentfiles.text =(substring (exportArray[val].adjacencyList as string) 3 ( (exportArray[val].adjacencyList as string).count - 3))
			if exportArray[val].tiletype then edTileClass.text = "Receiver" else edTileClass.text = "Giver" 
		) else (
			edFilePath.text = "[Multiple Selection]"
			edAdjacentfiles.text = "[Multiple Selection]"
			edTileClass.text = "[Multiple Selection]"
		)
	)
	
	on lbProcessList doubleclicked val do
	(
		print exportArray[val].fileName
		rsTP.loadCachedTile exportArray[val].fileName
		max tool zoomextents
	)
	
	on edFilterList changed val do
	(
		if val != "" then chkFilterCheck.checked = true else chkFilterCheck.checked = false
		local newArray = gterrainFileObjTools.filterText mlbTerrainTiles.items val prefix:false suffix:false
		mlbTerrainTiles.items = newArray
	)	
	

	on btnProcessSelected pressed do (		
		
		filein "pipeline/helpers/terrain/terrain_processing.ms"
		clearListener()
		
		rsTP.settings.silent = chkSilent.state
		rsTP.settings.vfb = chkVfb.state
		rsTP.settings.processBakingTextures = chkDoMaps.state
		rsTP.settings.processCreateLod = chkDoLOD.state
		rsTP.settings.imageHeight	= spnMapSizeX.value
		rsTP.settings.imageWidth	= spnMapSizeY.value
		
		local BatchStart = timeStamp()
		local theProcessFiles = lbProcessList.items 
		local taskComplete = true
		if theProcessFiles.count > 1 then rsTP.settings.silent = true
		for f in 1 to theProcessFiles.count do 
			(

				try (
					rsTP.process exportArray[f].theFile
				) catch (
					
					local msg = ( "*** " +  (getCurrentException()) + " ***\n")
					rsTP.doWarningsErrors msg toListener:true theProcessedMesh:exportArray[f].theFile
					taskComplete = false	
				)
			)
			
		local BatchEnd = timeStamp()
		
		if taskcomplete == true then (
			format "\n****************************************************************\n"	
			format "****************************************************************\n"
			format "****************************************************************\n\n"
			format "Batch Processed % Terrain Files in % Seconds\n"  theProcessFiles.count ((BatchEnd - BatchStart)/ 1000.0)
			format "\n****************************************************************\n"	
			format "****************************************************************\n"
			format "****************************************************************\n\n"

			max tool zoomextents all		

		)
		grsTPULog.validate()
	)		
	
	on btnRefreshTileList pressed do (				
			gterrainFileObjTools.makeFileArray gRsTerrainLoadAdjacencyList			
			mlbTerrainTiles.items = gterrainFileObjTools.makeDisplayNameArray gterrainFileObjTools.filteredArray
			
			chkFilterCheck.state = false
			edFilterList.text = ""
			local newArray = gterrainFileObjTools.filterText mlbTerrainTiles.items "" prefix:false suffix:false
			mlbTerrainTiles.items = newArray		
	)
	
	
	on rsTerrain_Processing_Rollout open do
	(
		banner.setup()	
		if gterrainFileObjTools != undefined then (
			if gterrainFileObjTools.fileArray.count == 0 then (	
				
				gterrainFileObjTools.makeFileArray gRsTerrainLoadAdjacencyList
			) else (
				
				gterrainFileObjTools.filteredArray = gterrainFileObjTools.fileArray
			)
			
			mlbTerrainTiles.items = gterrainFileObjTools.makeDisplayNameArray gterrainFileObjTools.filteredArray
			
			chkFilterCheck.state = false
			edFilterList.text = ""
			local newArray = gterrainFileObjTools.filterText mlbTerrainTiles.items "" prefix:false suffix:false
			mlbTerrainTiles.items = newArray
		)
			
	)	
	
	on rsTerrain_Processing_Rollout close do
	(
		 gterrainFileObjTools.filteredArray = #()
		exportArray =#()	
	)

)



createdialog rsTerrain_Processing_Rollout 250 800 style:#(#style_resizing,#style_titlebar, #style_toolwindow, #style_sysmenu)
