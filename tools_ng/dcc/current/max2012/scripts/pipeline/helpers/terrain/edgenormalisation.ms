--
-- Edge Normalization Utility
--A utility that will scan through a collection of containers.  Each container's adjacent containers will 
--be generated to then determine if the container will give or receive information.
--More specifically, once a container is determine to give its information, its edges' normals
--will be propagated to all adjacent containers.
--
--
filein "pipeline/helpers/terrain/terrain_funcs.ms"
filein "pipeline/helpers/terrain/edgenormalisation_helpers.ms"

global NormalisationAdjacencyListFile = RsProjectGetMetadataDir() + "/terrain/AdjacencyList.xml"
fn CreateMeshOutline meshObject = 
(
	ConvertToPoly meshObject
	
	local newEdges = polyop.getOpenEdges meshObject	
	local shapeBoundaryName = meshObject.name + "_temp"	
	polyop.createShape meshObject newEdges name:shapeBoundaryName
	(getNodeByName shapeBoundaryName)
)

fn GetAdjustedNormal resultInfos currentVertex =
(
	local normalsArray = #()
	
	for resultInfo in resultInfos do
	(
		local bestContainerName = resultInfo.GetBestContainer()
		
		if bestContainerName == undefined then
			continue
		
		local bestContainerNode = getNodeByName bestContainerName
		local bestMeshName = resultInfo.GetBestMesh()
		
		if bestMeshName == undefined then
			continue
		
		local bestMesh = FindMeshInContainer bestContainerNode bestMeshName
		if bestMesh == undefined then
		(
			print ("Unable to find best mesh " + bestMeshName as string + ".")
			continue
		)
		
		local bestVertexIndex = resultInfo.GetBestVertexIndex()
		local bestSecondVertexIndex = resultInfo.GetBestSecondVertexIndex()
		
		--print ("Best Mesh: " + bestMeshName as string)
		--print ("Best Vertex: " + bestVertexIndex as string)
		--print ("Best Second Vertex: " + bestSecondVertexIndex as string)
		
		if bestVertexIndex == undefined then
		(
			print ("Unable to find best vertex.")
			continue
		)
		
		select bestMesh --Need to select the mesh to give the vertex to get information from its modifier.
		modPanel.setCurrentObject bestMesh.modifiers[#Edit_Normals]
		local adjMeshNormalInfo = GetNormalisationInfo bestContainerNode
		local adjMeshNormalModifier = adjMeshNormalInfo.GetEditNormalModifier bestMesh	
		if adjMeshNormalModifier == undefined then
		(
			print ("No \"Edit Normals\" modifier was found on mesh " + bestMesh.name as string)
			return undefined	
		)
		
		local matchingNormal = undefined  --The normal to apply to this vertex.
		
		if ( bestSecondVertexIndex != -1 ) then
		(
			--If there are two vertices, then interpolate between the two to find the proper normal.
			firstNormalIndex = adjMeshNormalInfo.GetNormalIndex bestVertexIndex
			secondNormalIndex = adjMeshNormalInfo.GetNormalIndex bestSecondVertexIndex
			
			local firstNormal = adjMeshNormalModifier.GetNormal firstNormalIndex
			local secondNormal = adjMeshNormalModifier.GetNormal secondNormalIndex
			
			local firstVertex = meshop.getVert bestMesh bestVertexIndex
			local secondVertex = meshop.getVert bestMesh bestSecondVertexIndex
			
			local firstDistance = distance currentVertex firstVertex
			local secondDistance = distance currentVertex secondVertex
			
			local totalDistance = firstDistance + secondDistance
			
			local firstPercent = firstDistance / totalDistance
			local secondPercent = secondDistance / totalDistance
			
			matchingNormal = (firstNormal * firstPercent) + (secondNormal * secondPercent)	
		)
		else
		(		
			--Need to determine if the best fit is in the interior, which determines how we can find the normal data.
			normalIndex = adjMeshNormalInfo.GetNormalIndex bestVertexIndex		
			matchingNormal = adjMeshNormalModifier.GetNormal normalIndex
		)
		
		append normalsArray matchingNormal 
	)

	if normalsArray.count == 0 then
	(
		undefined
	)
	else
	(
		local adjustedNormal = point3 0 0 0 
		for normal in normalsArray do
		(
			adjustedNormal = adjustedNormal + normal
		)
		
		adjustedNormal = adjustedNormal / normalsArray.count
		adjustedNormal
	)
)

fn ProcessContainer containerObject = 
(	
	local normalInfo = GetNormalisationInfo containerObject
	
	if ( normalInfo == undefined ) then
	(
		print ("No Normalisation Info was found for " + containerObject.name as string)
		return false
	)
	
	if normalInfo.adjacencyList.count == 0 then
	(
		print ("No adjacent containers were found.")
		return true
	)
	
	--Print the adjacency list
	print ("Normalising edges in " + containerObject.name + " with adjacent containers ")
	for adjacent in normalInfo.adjacencyList do
	(
		print ("\t" + adjacent)
	)	
	
	project = true
	
	if ( normalInfo.receiveState == true ) then
	(
		print ("Container " + containerObject.name + " is a receiver.  Processing its normals.")
		
		--Load the container being processed.
		local initUnloadedState = containerObject.unloaded and containerObject.IsOpen()
		RsTerrainLoadContainer containerObject

		normalInfo.Process()
		
		NormalisationProcessor.SetTarget containerObject.name
		
		for adjacentNodeName in normalInfo.adjacencyList do
		(
			--Ensure that all adjacent tiles are loaded.
			local adjNode = getNodeByName adjacentNodeName
			if ( adjNode.unloaded == true or adjNode.IsOpen() == false) then
			(
				print ("Loading container " + adjNode.name + "...")
				adjNode.LoadContainer()
				adjNode.SetOpen true
				adjNode.MakeUnique()
			)
			
			adjNormalInfo = GetNormalisationInfo adjNode
			adjNormalInfo.Process()
			
			--Add this adjacent tile to the C# processor.
			NormalisationProcessor.AddAdjacent adjacentNodeName
		)	

		for meshObject in normalInfo.meshOpenVertices do
		(
			print ("Processing mesh " + meshObject[1].name )
			
			local normalsModifier = meshObject[1].modifiers[#Edit_Normals]
			
			NormalisationProcessor.ClearVertexList()
			for openVertexIndex in meshObject[2] do
			(
				local openVertex = meshop.getVert meshObject[1] openVertexIndex				
				openVector = dotNetObject "RSG.Base.Math.Vector3f" openVertex.x openVertex.y openVertex.z	

				NormalisationProcessor.AddVertex openVertexIndex openVector
			)
			
			NormalisationProcessor.Process meshObject[1].name
			
			--Process all exterior vertices.  The resultant normals propagated into the mesh itself via the Edit_Normals modifier.
			for openVertexIndex in meshObject[2] do
			(
				--print ("Current Vertex: " + openVertexIndex as string)
				local resultInfo = NormalisationProcessor.GetResultInfo openVertexIndex	
				local currentVertex = meshop.getvert meshObject[1] openVertexIndex
				local matchingNormal = GetAdjustedNormal resultInfo currentVertex
				
				if matchingNormal == undefined then
					continue

				select meshObject[1]
				modPanel.setCurrentObject meshObject[1].modifiers[#Edit_Normals]			
				vertex_selection = #{openVertexIndex}
				normal_selection = #{}
				meshObject[1].modifiers[#Edit_Normals].ConvertVertexSelection &vertex_selection &normal_selection
				
				normalIndexArray = normal_selection as array
				
				for i=1 to normalIndexArray.count do
				(
					meshObject[1].modifiers[#Edit_Normals].SetNormalExplicit normalIndexArray[i]
					meshObject[1].modifiers[#Edit_Normals].SetNormal normalIndexArray[i] matchingNormal
				)
			)
						
			if ( project == true ) then
			(				
				ProjectMesh meshObject[1] normalInfo	
			)
		)
		
		--For each mesh projected to, take the mapChannel and propagate vert
		for originalMesh in normalInfo.originalMeshArray do
		(
			collapseStack originalMesh 
			
			if ( project == true ) then
			(
				editNormalModifier = Edit_Normals()
				addModifier originalMesh editNormalModifier
				
				select originalMesh
				modPanel.setCurrentObject originalMesh.modifiers[#Edit_Normals]

				local openEdgesArray = meshop.getOpenEdges originalMesh
				local numVerts = meshop.getNumVerts originalMesh	
				edgeVertices = (meshop.getVertsUsingEdge originalMesh openEdgesArray)

				for vertexIndex =1 to numVerts do
				(
					local isOpenEdge = edgeVertices[vertexIndex]
					if ( isOpenEdge == true ) then
					(					
						vertex_selection = #{vertexIndex}
						normal_selection = #{}
						originalMesh.modifiers[#Edit_Normals].ConvertVertexSelection &vertex_selection &normal_selection
						
						vertexIndexArray = vertex_selection as array
						normalIndexArray = normal_selection as array
						for currentIndex=1 to normalIndexArray.count do
						(
							vertColor = meshop.getMapVert originalMesh NormalisationMapChannel vertexIndexArray[currentIndex]
							vertColor = (vertColor * 255) - 1.0  --The projection modifier seems to scale colors down between 0 and 1.
							originalMesh.modifiers[#Edit_Normals].SetNormalExplicit normalIndexArray[currentIndex]
							originalMesh.modifiers[#Edit_Normals].SetNormal normalIndexArray[currentIndex] (vertColor as point3)
						)
					)
				)

				collapseStack originalMesh 
			)
		)
		
		if ( initUnloadedState == true ) then
		(
			RsTerrainUnloadContainer containerObject
		)
		
		--Don't close the adjacent containers; they may be necessary later.
	)
	else
	(
		print ("Container " + containerObject.name + " is a giver.  Only its interior normals will be processed.")
		
		normalInfo.Process()
		for meshObject in normalInfo.meshOpenVertices do
		(
			--print ("Mesh Object: " + meshObject[1].name as string)
			if ( project == true ) then
			(
				ProjectMesh meshObject[1] normalInfo	
			)
		)
		
		--For each mesh projected to, take the mapChannel and propagate vert
		for originalMesh in normalInfo.originalMeshArray do
		(
			collapseStack originalMesh 
			
			if ( project == true ) then
			(
				editNormalModifier = Edit_Normals()
				addModifier originalMesh editNormalModifier
				
				select originalMesh
				modPanel.setCurrentObject originalMesh.modifiers[#Edit_Normals]

				local openEdgesArray = meshop.getOpenEdges originalMesh
				local numVerts = meshop.getNumVerts originalMesh	
				edgeVertices = (meshop.getVertsUsingEdge originalMesh openEdgesArray)

				for vertexIndex =1 to numVerts do
				(
					local isOpenEdge = edgeVertices[vertexIndex]
					if ( isOpenEdge == true ) then
					(					
						vertex_selection = #{vertexIndex}
						normal_selection = #{}
						originalMesh.modifiers[#Edit_Normals].ConvertVertexSelection &vertex_selection &normal_selection
						
						vertexIndexArray = vertex_selection as array
						normalIndexArray = normal_selection as array
						for currentIndex=1 to normalIndexArray.count do
						(
							vertColor = meshop.getMapVert originalMesh NormalisationMapChannel vertexIndexArray[currentIndex]
							vertColor = (vertColor * 255) - 1.0
							originalMesh.modifiers[#Edit_Normals].SetNormalExplicit normalIndexArray[currentIndex]
							originalMesh.modifiers[#Edit_Normals].SetNormal normalIndexArray[currentIndex] (vertColor as point3)
						)
					)
				)

				collapseStack originalMesh 
			)
		)
	)
)

-- Accumulate the maximum bounds between two bounding boxes.
-- If the actual height and width are less than the minimum width, then we have an intersection.
--
fn BoundingBoxesIntersectTest boundingBox1 boundingBox2 debug:false=
(
	local totalWidth = (boundingBox1[2][1] - boundingBox1[1][1]) + (boundingBox2[2][1] - boundingBox2[1][1])
	local totalHeight = (boundingBox1[2][2] - boundingBox1[1][2]) + (boundingBox2[2][2] - boundingBox2[1][2])
		
	local actualWidth1 = boundingBox2[2][1] - boundingBox1[1][1]
	local actualWidth2 = boundingBox1[2][1] - boundingBox2[1][1]
	
	local actualWidth = actualWidth1
	if actualWidth2 > actualWidth1 then
	(
		actualWidth = actualWidth2
	)
	
	local actualHeight1 = (boundingBox2[2][2] - boundingBox1[1][2])
	local actualHeight2 = (boundingBox1[2][2] - boundingBox2[1][2])
	
	if ( debug == true ) do
	(
		print ("1: " + actualHeight1 as string)
		print ("2: " + actualHeight2 as string)
	)
	
	local actualHeight = actualHeight1
	if actualHeight2 > actualHeight1 then
	(
		actualHeight = actualHeight2
	)
	
	if ( debug == true ) do 
	(
		print ("Actual Width: " + actualWidth as string)
		print ("Total Width: " + totalWidth as string)
		print ("Actual Height: " + actualHeight as string)
		print ("Total Height: " + totalHeight as string)
		print ("Actual Class: " + (classof actualWidth) as string)
		print ("Total Class: " + (classof totalWidth ) as string)
		print ("First: " + (actualWidth as string == totalWidth as string) as string)
		print ("Second: " + (actualHeight as float  <= totalHeight as float ) as string)
		print ("Single Point: " + abs (actualWidth - totalWidth) as string + " and " + abs ( actualHeight - totalHeight ) as string)
	)
	
	if ( abs (actualWidth - totalWidth) < 0.1 and abs ( actualHeight - totalHeight ) < 0.1 ) then 
	(
		--This object doesn't share an actual edge; only a single point.
		--print "Shares single point."
		if debug == true do print "False. Single Point."
		false
	)
	else if ( (actualWidth <= totalWidth or (abs(totalWidth - actualWidth) < 0.01)) and (actualHeight < totalHeight or (abs(totalHeight - actualHeight) < 0.01)) ) then
	(
		if debug == true do print "True."
		true
	)
	else
	(
		if debug == true do print "False."
		false
	)
)

fn GetBoundingBoxArray = 
(
	local containerBoundingBoxArray = #()
	for child in rootnode.children do 
	(
		local childClass = classof child
		if ( Container == childClass ) then
		(
			local unloadedState = child.unloaded
			RsTerrainLoadContainer child
			
			boundingBox = RsTerrainGetContainerBounds child
			CollapseMeshModifiers child
			
			append containerBoundingBoxArray #(child.name, boundingBox)
			
			if ( unloadedState == true ) then
			(
				RsTerrainUnloadContainer child
			)
		)
	)
	
	containerBoundingBoxArray
)

fn CloseAllContainers = 
(
	for child in rootnode.children do 
	(
		local childClass = classof child
		if ( Container == childClass ) then
		(
			if child.unloaded == false then
			(
				RsTerrainUnloadContainer child
			)
		)
	)
)

fn GenerateAdjacencyList =
(
	local containerBoundingBoxArray = GetBoundingBoxArray()
	local containerAdjacencyList = #()
	for containerIndex=1 to containerBoundingBoxArray.count do
	(
		local currentContainer = containerBoundingBoxArray[containerIndex]
		adjacencyList = #()
		
		--print ("Processing Container " + currentContainer[1] + "...")

		for testContainerIndex=1 to containerBoundingBoxArray.count do
		(
			if ( containerIndex != testContainerIndex ) then
			(
				local testContainer = containerBoundingBoxArray[testContainerIndex]
				
				debug = false
				/*if ( currentContainer[1] == "h_15_" ) do
				(
					print (currentContainer[1] as string + ": " + currentContainer[2] as string)
					print (testContainer[1] as string + ": " + testContainer[2] as string)
					--debug = true
				)*/
				
				if (BoundingBoxesIntersectTest currentContainer[2] testContainer[2] debug:debug ) == true then
				(
					append adjacencyList testContainer[1]
				)

			)
		)			
					
		append containerAdjacencyList #(currentContainer[1], adjacencyList)
			
	)

	containerAdjacencyList
)

fn LoadAdjacencyList = 
(
	if (getfiles NormalisationAdjacencyListFile).count == 0 then 
	(
		print (NormalisationAdjacencyListFile + " does not exist.")
		return undefined
	)
	adjacencyList = dotNetObject "RSG.MaxUtils.AdjacencyList"
	adjacencyList = adjacencyList.Load NormalisationAdjacencyListFile
	if ( adjacencyList == undefined ) then
	(
		print ("Unable to load " + NormalisationAdjacencyListFile + ".")
		return undefined
	)
	NormalisationProcessor = undefined
	NormalisationInfoArray = #()
	for child in rootnode.children do 
	(
		local childClass = classof child
		if ( Container == childClass ) then
		(
			local adjList = adjacencyList.GetAdjacencyList(child.name)
			local isReceiver = adjacencyList.IsReceiver(child.name)
			newInfo = NormalisationInfo containerObject:child receiveState:isReceiver adjacencyList:adjList
			append NormalisationInfoArray newInfo
		)
	)
)
fn SaveAdjacencyList containerAdjacencyList =
(
	adjacencyList = dotNetObject "RSG.MaxUtils.AdjacencyList"
	for adjIndex=1 to containerAdjacencyList.count do 
	(
		local entry = containerAdjacencyList[adjIndex]
		local containerNode = getNodeByName entry[1]
		local normalInfo = GetNormalisationInfo containerNode
		local entryItem = adjacencyList.Add entry[1] normalInfo.receiveState
		--print ("Container: " + entry[1] as string)
		for adjListIndex=1 to entry[2].count do
		(
			--print ("\t" + entry[2][adjListIndex])
			entryItem.AddAdjacentContainer(entry[2][adjListIndex])
		)
	)
	adjacencyList.Save adjacencyList NormalisationAdjacencyListFile
	print ("Adjacency List has been saved to " + NormalisationAdjacencyListFile + ".")
)

rollout RsEdgeNormalisationRollout "Edge Normalisation"
(
	--button btnStartNormalise "Start" width:120 height:40
	button btnNormaliseContainer "Normalise Selection" width:(RsEdgeNormalisationRollout.width * .95) height:40
	button btnCloseAllContainers "Close All Containers" width:(RsEdgeNormalisationRollout.width * .95) height:40
	label seedNodeLabel "Seed Node Name:" align:#left
	edittext seedNodeText
	button btnGenerateAdjacencyList "Generate Adjacency List" width:(RsEdgeNormalisationRollout.width * .95) height:40
	
	/*on btnStartNormalise pressed do
	(
		local timer = ScriptTimer()
		timer.start()
		
		setCommandPanelTaskMode mode:#Modify
		
		LoadAdjacencyList()
		
		--Process all containers
		for child in rootnode.children do 
		(
			local childClass = classof child
			if ( Container == childClass ) then
			(
				ProcessContainer child
			)
		)
		
		CleanupNormalisationInfo()
		
		timer.end()
		format "Total Time: % ms\n" ( timer.interval() )
	)*/
	
	on btnNormaliseContainer pressed do 
	(
		local timer = ScriptTimer()
		timer.start()
		
		setCommandPanelTaskMode mode:#Modify
		
		for child in selection do 
		(
			RsInheritAdjacentContainers child
		)
		
		LoadAdjacencyList()
				
		--Process all containers
		for child in selection do 
		(
			local childClass = classof child
			if ( Container == childClass ) then
			(
				ProcessContainer child
			)
		)
		
		CleanupNormalisationInfo()
		
		timer.end()
		format "Total Time: % ms\n" ( timer.interval() )
	)
	
	on btnCloseAllContainers pressed do 
	(
		CloseAllContainers()
	)
	
	on btnGenerateAdjacencyList pressed do 
	(
		local seedNode = getNodeByName seedNodeText.text
		if seedNode == undefined then 
		(
			MessageBox ("Unable to find node \"" + seedNodeText.text + "\" in the scene.  Specify node that exists for the seed.")
			return undefined
		)
		
		local seedNodeClass = classof seedNode
		if ( Container != seedNodeClass ) then
		(
			MessageBox ("The node " + seedNodeText.text + " was not a container.  This node is a " + seedNodeClass as string + ".")
			return undefined
		)
		
		setCommandPanelTaskMode mode:#Modify
		
		local containerAdjacencyList = GenerateAdjacencyList()
		PrintAdjacencyList containerAdjacencyList
		
		--Note that the node name is a seed to establish the receivers and givers based on the adjacency list.
		--
		seedNodeName = seedNodeText.text
		CreateNormalisationInfo seedNodeName containerAdjacencyList
		
		SaveAdjacencyList containerAdjacencyList
	)
	
	on RsEdgeNormalisationRollout open do 
	(	
		seedNodeText.text = "f_02_"
	)		
	
	on RsEdgeNormalisationRollout rolledUp down do 
	(
		RsSettingWrite "rsTerrainNormalisation" "rollup" (not down)
	)
)

/*
try CloseRolloutFloater RsEdgeNormalisation catch()
RsEdgeNormalisation = newRolloutFloater "Rockstar Edge Normalisation" 200 430 50 126
addRollout RsEdgeNormalisationRollout RsEdgeNormalisation rolledup:false
*/