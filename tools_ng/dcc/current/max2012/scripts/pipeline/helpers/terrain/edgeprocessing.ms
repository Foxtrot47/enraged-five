--
-- Edge Normalization Utility
--A utility that will scan through a collection of containers.  Each container's adjacent containers will 
--be generated to then determine if the container will give or receive information.
--More specifically, once a container is determine to give its information, its edges' normals
--will be propagated to all adjacent containers.
--
--
filein "pipeline/util/terrain_utilities_globals.ms"
filein "pipeline/helpers/terrain/terrain_funcs.ms"
filein "pipeline/helpers/terrain/edgenormalisation_helpers.ms"

global RsUnmatchedVertexResultItems = #()
global RsUnmatchedVertexResults = #()

rollout RsEdgeProcessingRollout "Edge Processing"
(
	checkbox checkVertexBoundaries "Check Vertex Boundaries"
	checkbox checkMatchingVertices "Check Matching Vertices"
	
	button btnProcessAll "Process All" width:(RsEdgeProcessingRollout.width * .95) height:40
	button btnProcessSelection "Process Selection"  width:(RsEdgeProcessingRollout.width * .95) height:40
	
	listbox lstResults "Unmatched Vertex Results:" items:RsUnmatchedVertexResults
	
	--This function will process the container by first checking its extents and verifying
	--that it exists on a 512 by 512 boundary. 
	--Every vertex edge, if close enough, will be moved to be exactly on the boundary 
	--if its distance is less than the specified distanceThreshold.
	fn ProcessVertexBoundaries containerNode = 
	(
		boundsArray = RsTerrainGetContainerBounds containerNode
		
		local minimumBounds = boundsArray[1]
		local maximumBounds = boundsArray[2]
		
		local distanceThreshold = RsTerrainGlobals.VertexBoundaryThreshold
				
		local terrainMeshArray = RsTerrainGatherMeshes containerNode
		
		for terrainMesh in terrainMeshArray do
		(
			local openEdgesArray = meshop.getOpenEdges terrainMesh
			local numVerts = meshop.getNumVerts terrainMesh
			edgeVertices = (meshop.getVertsUsingEdge terrainMesh openEdgesArray)
			
			invalidVertices = #()
			for edgeIndex =1 to numVerts do
			(
				local isOpenEdge = edgeVertices[edgeIndex]
				if ( isOpenEdge == true ) then
				(
					local edgeVertex = meshop.getVert terrainMesh edgeIndex
					invalid = false
					xAlignment = undefined
					yAlignment = undefined
					
					/*if debugOutput == true then
					(
						print ("Minimum Bounds: " + minimumBounds as string)
						print ("Maximum Bounds: " + maximumBounds as string)
						print ("Edge Vertex: " + edgeVertex as string)
						print ("X1 Distance: " + (abs (edgeVertex.x - minimumBounds[1])) as string)
						print ("Y1 Distance: " + abs (edgeVertex.y - minimumBounds[2]) as string)
						print ("X2 Distance: " + abs (edgeVertex.x - maximumBounds[1]) as string)
						print ("Y2 Distance: " + abs (edgeVertex.y - maximumBounds[2]) as string)
					)*/
					
					if abs (edgeVertex.x - minimumBounds[1]) <= distanceThreshold then
					(
						if edgeVertex.x != minimumBounds[1] then
						(
							invalid = true
							xAlignment = minimumBounds[1]
						)
					)
					else if abs (edgeVertex.x - maximumBounds[1]) <= distanceThreshold then 
					(
						if edgeVertex.x != maximumBounds[1] then
						(
							invalid = true
							xAlignment = maximumBounds[1]
						)
					)
					
					if abs (edgeVertex.y - minimumBounds[2]) <= distanceThreshold then 
					(
						if edgeVertex.y != minimumBounds[2] then
						(
							invalid = true
							yAlignment = minimumBounds[2]
						)
					)
					else if abs (edgeVertex.y - maximumBounds[2]) <= distanceThreshold then
					(
						if edgeVertex.y != maximumBounds[2] then
						(
							invalid = true
							yAlignment = maximumBounds[2]
						)
					)
					
					if invalid == true then
					(
						newVertexPosition = meshop.getVert terrainMesh edgeIndex
						if ( xAlignment != undefined ) then
						(
							newVertexPosition.x = xAlignment
						)
						
						if ( yAlignment != undefined ) then
						(
							newVertexPosition.y = yAlignment
						)
						meshop.setVert terrainMesh edgeIndex newVertexPosition
						append invalidVertices edgeIndex
					)
				)
			)
			
			print (invalidVertices.count as string + " vertices updated on mesh " + terrainMesh.name)

			/*
			--To select the target vertex.
			select terrainMesh
			setSelectionLevel terrainMesh #vertex
			select terrainMesh.verts[invalidVertices]
			*/
		)
	)
	
	-- Function that will find the specified container's adjacent nodes,
	-- ensure that every boundary vertex has a matching vertex in the adjacent container.
	fn ProcessMatchingVertices containerNode = 
	(
		local vertexMatchingThreshold = RsTerrainGlobals.VertexMatchingThreshold
		RsTerrainLoadAdjacencyList()
		
		results_array = #()
		
		--For this container, find all adjacent nodes.
		adjacencyInfo = TerrainAdjacencyInfoArray.GetInfo containerNode
		
		local gTerrainProcessor = dotNetObject "RSG.MaxUtils.TerrainProcessing"
		if gTerrainProcessor == undefined then 
		(
			print ("Unable to instantiate terrain processor object.")
			return undefined
		)
		
		invalidVertexCount = 0 --Tracker for all vertices across all containers, meshes.
		
		if ( adjacencyInfo == undefined or adjacencyInfo.adjacencyList == undefined ) then
		(
			print ("Unable to find adjacency information for " + containerNode.name + ".  Verify this container is in the adjacency list.  This container will not be processed.")
			return undefined;
		)
			
		gTerrainProcessor.SetVertexMatchingThreshold vertexMatchingThreshold
		gTerrainProcessor.SetTarget containerNode.name

		containersList = deepcopy adjacencyInfo.adjacencyList
		append containersList containerNode.name
		for adjContainerName in containersList do
		(		
			local adjContainer = getNodeByName adjContainerName
			if adjContainer == undefined then
				continue
			
			RsTerrainLoadContainer adjContainer

			boundsArray = RsTerrainGetContainerBounds adjContainer
		
			local minimumBounds = boundsArray[1]
			local maximumBounds = boundsArray[2]
			
			local isRegistered = gTerrainProcessor.IsRegistered adjContainer.name
			gTerrainProcessor.AddAdjacent adjContainerName		
			containerInfo = gTerrainProcessor.GetInfo adjContainer.name
			
			if isRegistered == false then
			(			
				local terrainMeshArray = RsTerrainGatherMeshes adjContainer
					
				for terrainMesh in terrainMeshArray do
				(
					local newMeshInfo = containerInfo.GetMesh terrainMesh.name 
					
					local openEdgesArray = meshop.getOpenEdges terrainMesh
					local numVerts = meshop.getNumVerts terrainMesh
					edgeVertices = (meshop.getVertsUsingEdge terrainMesh openEdgesArray)

					invalidVertices = #()
					for vertexIndex =1 to numVerts do
					(
						local isOpenEdge = edgeVertices[vertexIndex]
						if ( isOpenEdge == true ) then
						(
							local edgeVertex = meshop.getVert terrainMesh vertexIndex
												
							edgeVector = dotNetObject "RSG.Base.Math.Vector3f" edgeVertex.x edgeVertex.y edgeVertex.z			
							newMeshInfo.AddVertex vertexIndex edgeVector
						)
					)
					
					newMeshInfo.Sort()
				)
			)
		)
		
		--This process should process the source container, but we have already added
		--this container to the processor in the above code.  Setting this to 'false'	
		gTerrainProcessor.SetProcessSourceContainer false
		gTerrainProcessor.SetMode (gTerrainProcessingMode.VertexMatching)
		
		-- Prepare this container to be processed.  Add all the vertices into the main processor.
		RsTerrainLoadContainer containerNode
		boundsArray = RsTerrainGetContainerBounds containerNode
		
		local minimumBounds = boundsArray[1]
		local maximumBounds = boundsArray[2]
		
		local edgeVertexIndices = #()
		local terrainMeshArray = RsTerrainGatherMeshes containerNode
		for terrainMesh in terrainMeshArray do
		(			
			invalidVertexCount = 0
			edgeVertexIndices = #()
			gTerrainProcessor.ClearVertexList()
			
			local openEdgesArray = meshop.getOpenEdges terrainMesh
			local numVerts = meshop.getNumVerts terrainMesh
			edgeVertices = (meshop.getVertsUsingEdge terrainMesh openEdgesArray)

			invalidVertices = #()
			for vertexIndex = 1 to numVerts do
			(
				local isOpenEdge = edgeVertices[vertexIndex]
				if ( isOpenEdge == true ) then
				(
					local edgeVertex = meshop.getVert terrainMesh vertexIndex

					edgeVector = dotNetObject "RSG.Base.Math.Vector3f" edgeVertex.x edgeVertex.y edgeVertex.z			
					gTerrainProcessor.AddVertex vertexIndex edgeVector
					append edgeVertexIndices vertexIndex
				)
			)
			
			--After we have loaded the container with all the needed data.  Process it.
			print ("Processing " + terrainMesh.name + "...")		
			gTerrainProcessor.Process terrainMesh.name
			
			unmatched_vertices = #()
			
			for vertexIndex = 1 to edgeVertexIndices.count do
			(
				resultInfo = gTerrainProcessor.GetResultInfo(edgeVertexIndices[vertexIndex])

				if ( (resultInfo[1].BestDistance) <= vertexMatchingThreshold ) then
				(
					local bestMesh = getNodeByName resultInfo[1].BestMesh
					
					if bestMesh != undefined do 
					(
						local bestVertexPosition = meshop.getVert bestMesh resultInfo[1].BestVertexIndex
						
						-- Move the current vertex to the target vertex.
						meshop.setVert terrainMesh #{edgeVertexIndices[vertexIndex]} bestVertexPosition
					)
				)
				else
				(
					print ("No match found for vertex " + edgeVertexIndices[vertexIndex] as string)
					--print ("  Closest Distance: " + resultInfo[1].BestDistance as string)
					invalidVertexCount = invalidVertexCount + 1
					append unmatched_vertices edgeVertexIndices[vertexIndex]
				)
			)
			
			print (invalidVertexCount as string + " boundary vertices are not matched.")
			
			append results_array #(terrainMesh, unmatched_vertices)
		)
		
		return results_array
	)
	
	fn ProcessArray objectArray = 
	(
		undo "Edge Processing" on
		(
			local timer = ScriptTimer()
			timer.start()
		
			RsUnmatchedVertexResults = #()
			lstResults.items = RsUnmatchedVertexResults
			
			--Process all containers
			for child in objectArray do 
			(
				local childClass = classof child
				if ( Container == childClass ) then
				(
					print ("Processing container " + child.name)
			
					local initUnloadedState = child.unloaded and child.IsOpen()				
					RsTerrainLoadContainer child
					
					if (checkVertexBoundaries.checked == true) then
					(
						print "Processing vertex boundaries..."
						ProcessVertexBoundaries child
					)
					
					if ( checkMatchingVertices.checked == true ) then
					(
						print "Processing matching vertices..."
						results_array = ProcessMatchingVertices child
						RsUnmatchedVertexResultItems = results_array
						for result in results_array do
						(
							append RsUnmatchedVertexResults (result[1].name + " - Verts: " + result[2].count as string)
						)
					)
					
					if ( initUnloadedState == true ) then
					(
						RsTerrainUnloadContainer child
					)
				)
			)
			
			lstResults.items = RsUnmatchedVertexResults
			
			timer.end()
			format "Total Time: % ms\n" ( timer.interval() )
		)
	)
	
	on btnProcessAll pressed do
	(
		ProcessArray rootnode.children
	)
	
	on btnProcessSelection pressed do 
	(
		ProcessArray selection
	)
	
	on RsEdgeProcessingRollout open do 
	(	
		checkVertexBoundaries.checked = true
		checkMatchingVertices.checked = true
	)
	
	on RsEdgeProcessingRollout rolledUp down do 
	(
		RsSettingWrite "rsTerrainEdgeProcessing" "rollup" (not down)
	)
	
	on lstResults selected item do 
	(
		if ( undefined != RsUnmatchedVertexResults[item] and undefined != RsUnmatchedVertexResultItems[item]) then (
		
			if isdeleted RsUnmatchedVertexResultItems[item][1] == false then 
			(
				select RsUnmatchedVertexResultItems[item][1]
				subObjectLevel = 1  -- Vertex Selection mode
				select RsUnmatchedVertexResultItems[item][1].verts[RsUnmatchedVertexResultItems[item][2]] --Select the vert.
			)
		)
	)
)

/*
try CloseRolloutFloater RsEdgeProcessingRollout catch()
RsEdgeProcessing = newRolloutFloater "Rockstar Edge Processing" 200 430 50 126
addRollout RsEdgeProcessingRollout RsEdgeProcessing rolledup:false
*/