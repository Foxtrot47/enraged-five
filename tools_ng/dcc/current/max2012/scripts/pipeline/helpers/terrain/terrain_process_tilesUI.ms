
filein "pipeline/helpers/terrain/terrain_processing.ms"


-- recursively search for a string in filenames, return full path
fn recurseFindContainerFile currentDir containerName &foundContainerFilename = 
(
	containerFiles = getFiles ( currentDir + "*.maxc" )
	
	for containerFile in containerFiles do (
		fileNameToCompare = getFilenameFile containerFile

		if (findstring fileNameToCompare containerName) == 1 then
		(
			--print ("Found: " + fileNameToCompare as string)
			append foundContainerFilename (currentDir + (getFilenameFile containerFile) + ".maxc")
		)
	)

	subDirs = getDirectories ( currentDir + "*" )
	
	for subDir in subDirs do (
		recurseFindContainerFile subDir containerName &foundContainerFilename
	)
)

-- find files that start with the given searchstring, start path based off of terrain tools
fn getContainerFromString searchString=
(
	global g_RsTerrainProcessingStructInstance
	local foundContainerFilename = #()
	local sourceDirPath = g_RsTerrainProcessingStructInstance.settings.terrainSourceDir
	recurseFindContainerFile sourceDirPath searchString foundContainerFilename
		
	foundContainerFilename

	
)


fn RsTerrainProcessTilesUI=
(
	
	
	rollout RsTerrainProcessTilesRollout "Terrain Processing Panel"
	(
		
		global g_RsTerrainProcessingStructInstance = RsTerrainProcessingStruct()
		
		local terrainContainerDirectory = g_RsTerrainProcessingStructInstance.settings.terrainSourceDir
		
		MultiListBox lstTerrainTileList "Terrain Tiles" width:375 tooltip:"List of tiles that will be processed." align:#left 
		button btnSaveListedContainers "Save List..." width:180 tooltip:"Save the current list of tiles to disk." align:#left
		button btnLoadTerrainContainer "Add File..." width:85 tooltip:"Load a container to process." align:#right offset:[-95,-26]
		button btnSelectAll "Select All" width:90 tooltip:"Load a container to process." align:#right offset:[0,-26]
		
		button btnLoadTerrainProcessingList "Load List..." width:180 tooltip:"Load a list of terrain tiles to process, replaces files in the list." align:#left 
		button btnClearList "Clear List" width:85 tooltip:"Clear the list of all entries."  align:#right offset:[-95,-26]
		button btnDeleteSelected "Delete" width:90 tooltip:"Remove a selected item in the Terrain Tile list below."  align:#right offset:[0,-26]
		
		edittext txtAddContainerByString "" width:270 tooltip:"Enter tile name" align:#left offset:[-5,5]
		button btnAddContainerByName "Add Container" width:100 tooltip:"Uses the text to search through the terrain folder and find the tile container." align:#right offset:[0,-25]
		dropdownlist dplSearchResults "" width:378 enabled:false visible:false align:#left offset:[-2,0]

		group "Options"
		(
			checkbox chkProcessOptionEdges "Process Edges" width:100 tooltip:"Sew up edges and fix normals when processing the tiles." align:#left checked:true
			
			checkbox chkProcessOptionBake "Bake Textures" width:100 tooltip:"Bake out the color and channel maps when processing the tiles." align:#left checked:true
			checkbox chkProcessOptionLOD "Create LOD's" width:80 tooltip:"Auto generate LOD's when processing the tiles." align:#left checked:true
			spinner spnNumLodIterations "" width:50 range:[1,5,1] type:#integer tooltip:"How many lod levels will be generated when processing tiles." align:#left offset:[90,-20] 
		)
		button btnProcessColorTex "Bake Color Map For Selected" width:180 tooltip:"Will bake the color map to a texture for the selected tiles." align:#left 
		button btnProcessChannelTex "Bake Channel Map For Selected" width:180 tooltip:"Will bake the channel map to a texture for the selected tiles." align:#right offset:[0,-26]

		button btnProcessSelection "Process Selected" width:375 tooltip:"Start the processing on the selected tiles in the list above." 
		button btnProcessScene "Process All" width:375 tooltip:"Start the processing on the tiles in the list above." 
			
		button btnBatchTerrainGivers "Batch Giver Tiles" width:375  tooltip:"Run the terrain processor on all giver tiles." offset:[0,30]
		
		on btnDeleteSelected pressed do
		(
			lstTerrainTileList.items = for itemIndex in (-lstTerrainTileList.selection) collect lstTerrainTileList.items[itemIndex]			
		)
		
		on btnSelectAll pressed do
		(
				lstTerrainTileList.selection = #{1..(lstTerrainTileList.items.count)}
		)
		
		on btnClearList pressed do
		(
			lstTerrainTileList.items = #()
		)
		
		on btnSaveListedContainers pressed do
		(
			local containerListFilepath = getSaveFileName \
			types:"Text Document(*.txt)|*.txt|"
			print containerListFilepath
			if containerListFilepath != undefined then
			(
				local newFileOutStream = createFile containerListFilepath
				for item in lstTerrainTileList.items do
				(
					local baseN = getFilenameFile item
					--print  baseN to: newFileOutStream
					format "%\n" baseN to:newFileOutStream
				)
				close newFileOutStream
			)
		)
		
		on txtAddContainerByString changed txtInControl do
		(
			local foundFiles = getContainerFromString txtInControl

			dplSearchResults.enabled = true
			dplSearchResults.visible = true
			dplSearchResults.height = (foundFiles.count * 15)
			dplSearchResults.items = #()
			dplSearchResults.selection = 1
			dplSearchResults.items = foundFiles
			
		)
				
		on txtAddContainerByString entered txtInControl do
		(
			if txtInControl == "" then
			(
				dplSearchResults.enabled = false
				dplSearchResults.visible = false
			)
		)

		on dplSearchResults selected itemIndex do
		(
			txtAddContainerByString.text = dplSearchResults.items[itemIndex]
			
			dplSearchResults.enabled = false
			dplSearchResults.visible = false
		)
		
		on btnAddContainerByName pressed do
		(
			if txtAddContainerByString.text != "" then 
			(
				if dplSearchResults.items.count != 0 then
				(
					local findMatches = for filePath in dplSearchResults.items where filePath == txtAddContainerByString.text collect filePath
					if findMatches.count >= 1 then
					(
						lstTerrainTileList.items = append lstTerrainTileList.items txtAddContainerByString.text
						txtAddContainerByString.text = ""
					)
					else
					(
						lstTerrainTileList.items = append lstTerrainTileList.items dplSearchResults.items[dplSearchResults.selection]
						txtAddContainerByString.text = ""
					)
					dplSearchResults.enabled = false
					dplSearchResults.visible = false
				)
			)
		)
		
		
		on RsTerrainLODRollout rolledUp down do 
		(
			RsSettingWrite "RsTerrainProcessTilesTagging" "rollup" (not down)
			
		)

		on btnLoadTerrainContainer pressed do
		(
			local containerFilepath = getOpenFileName filename:terrainContainerDirectory 

			if containerFilepath != undefined then
			(
				lstTerrainTileList.items = append lstTerrainTileList.items containerFilepath
			)		
		)

		on btnLoadTerrainProcessingList pressed do
		(
			local containerListFilepath = getOpenFileName types:"Text Document(*.txt)|*.txt|"
			local fileLines = #()
			local fileList = #()
			
			
			if containerListFilepath != undefined then
			(
				local parseFile = openfile containerListFilepath
				while NOT eof parseFile do 
				(
					local textLine = readline parseFile
					local foundFiles = getContainerFromString textLine
					if foundFiles != undefined then
					(
						if foundFiles.count >= 1 then
						(
							append fileList foundFiles[1]
						)
					)
				)
				close parseFile
				lstTerrainTileList.items = fileList
			)
		)
	

	
		on btnBatchTerrainGivers pressed do
		(
			g_RsTerrainProcessingStructInstance.batchCacheGiverTiles()
			
		)

		on btnProcessScene pressed do
		(
			
			g_RsTerrainProcessingStructInstance.processBakingTextures 	= chkProcessOptionBake.checked 
			g_RsTerrainProcessingStructInstance.processCreateLod			= chkProcessOptionLOD.checked
			g_RsTerrainProcessingStructInstance.processLodInterations   	= spnNumLodIterations.value
			g_RsTerrainProcessingStructInstance.processEdges				= chkProcessOptionEdges.checked 
			
			for containerIndex=1 to lstTerrainTileList.items.count do
			(
				if (doesFileExist  lstTerrainTileList.items[containerIndex]) then
				(
					g_RsTerrainProcessingStructInstance.process lstTerrainTileList.items[containerIndex]
				)
			)
		)
	)
	
	createDialog RsTerrainProcessTilesRollout 400 600
	RsTerrainProcessTilesRollout
)

if rsTerrainProcessingUI != undefined then
(
	DestroyDialog rsTerrainProcessingUI
)
rsTerrainProcessingUI = RsTerrainProcessTilesUI()