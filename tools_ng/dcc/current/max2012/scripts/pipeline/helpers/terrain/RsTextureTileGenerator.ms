/*
Constructs a virtual grid around the selected objects, and creates a set of tiled images and re-constructs them to a final image.  The final image
is returned when calling the method 'process'.

Author: Jason Hayes (jason.hayes@rockstarsandiego.com)
*/

filein "pipeline/util/rect.ms"


struct RsTextureTileGenerator
(
	public
	
	tileImagesPath 		= undefined,
	tileImageName			= undefined,
	tileImageFileType		= "bmp",
	tileSize 					= 50,
	tileResolution 			= 512,
	
	debug					= false,
	
	
	private
	
	-- Multi-dimensional array.  Each array inside consists of a row, and each item in the array is a column.
	tileImages				= #(),
	
	selectedObjs 			= undefined,
	tileCamera 				= undefined,
	
	numTilesX 				= undefined,
	numTilesY 				= undefined,
	
	-- Viewport configuration
	activeVp 				= undefined,
	layout 					= undefined,
	type 						= undefined,
	gridVisibility 			= undefined,
	renderLevel 			= undefined,
	size 						= undefined,
	viewCubeVisibility		= undefined,
	safeFrames				= undefined,
	showEdgeFaces		= undefined,
	
	
	-- Compile a run-time C# dll to deal with resizing the 3dsmax viewport.
	fn compileCSharpAssembly = (
		source = "using System;\n"
		source += "using System.Runtime.InteropServices;\n"
		source += "using System.Text;\n"
		source += "class assembly\n"
		source += "{\n"
		source += " [DllImport(\"user32.dll\")]\n"
		source += " public static extern bool SetWindowPos(IntPtr hWnd, int hWndArg, int Left, int Top, int Width, int Height, int hWndFlags);\n"
		source += " [DllImport(\"user32.dll\")]\n"
		source += "	static extern bool GetWindowRect(IntPtr hWnd, out RECT rect);\n"
		source += "	public struct RECT\n"
		source += "	{\n"
		source += "	 public int Left;\n"
		source += "	 public int Top;\n"
		source += "	 public int Right;\n"
		source += "	 public int Bottom;\n"
		source += "	}\n"
		source += "	public int[] getWindowRect(IntPtr hWnd)\n"
		source += "	{\n"
		source += "	 RECT rect;\n"
		source += "	 if ( GetWindowRect(hWnd, out rect) )\n"
		source += "	 {\n"
		source += "	 return new int[] { rect.Left, rect.Top, rect.Right - rect.Left, rect.Bottom - rect.Top };\n"
		source += "	 }\n"
		source += "	 return null;\n"
		source += "	}\n"
		source += "}\n"
		
		local csharpProvider = dotnetobject "Microsoft.CSharp.CSharpCodeProvider"
		local compilerParams = dotnetobject "System.CodeDom.Compiler.CompilerParameters"
		compilerParams.GenerateInMemory = on
		local compilerResults = csharpProvider.CompileAssemblyFromSource compilerParams #( source )
		global assembly = compilerResults.CompiledAssembly.createInstance "assembly"
	),
	
	fn setCheckBoxState hwnd state = 
	(	
		local BN_CLICKED =0
		local BM_SETCHECK = 241
		local WM_COMMAND = 273

		local parent = UIAccessor.getParentWindow hwnd
		local id = UIAccessor.getWindowResourceID hwnd

		windows.sendMessage hwnd BM_SETCHECK (if state then 1 else 0) 0
		windows.sendMessage parent WM_COMMAND ((bit.shift BN_CLICKED 16) + id) hwnd	
		ok
	),

	fn getButtonHwnd hnd =
	(
		for i in (windows.getChildrenHWND hnd) where matchPattern i[5] pattern:"Display World*" do return i[1]
		0
	),
	
	fn changeTab hnd =
	(
		TCM_SETCURFOCUS = 0x1330
		for kidHWND in (UIAccessor.GetChildWindows hnd) where ((UIAccessor.GetWindowClassName kidHWND) == "SysTabControl32") do (
			UIAccessor.SendMessage kidHWND TCM_SETCURFOCUS 2 0 
		)
	),
	
	fn showWorldAxisOn = 
	(
		local hnd = dialogmonitorops.getwindowhandle()
		changeTab hnd
		setCheckBoxState (getButtonHwnd hnd) on
		uiaccessor.pressButtonByName hnd "OK"
		true
	),

	fn showWorldAxisOff = 
	(
		local hnd = dialogmonitorops.getwindowhandle()
		changeTab hnd
		setCheckBoxState (getButtonHwnd hnd) off
		uiaccessor.pressButtonByName hnd "OK"
		true
	),

	fn showWorldAxis state =
	(
		DialogMonitorOPS.unRegisterNotification id:#ShowWorldAxis
		DialogMonitorOPS.enabled = off
		DialogMonitorOPS.enabled = on	
		DialogMonitorOPS.RegisterNotification (if state then showWorldAxisOn else showWorldAxisOff) id:#ShowWorldAxis
		actionMan.executeAction 0 "40108" 
		DialogMonitorOPS.unRegisterNotification id:#ShowWorldAxis
		DialogMonitorOPS.enabled = off
	),
	
	fn resizeViewport width height =
	(
		compensation = 4
		ViewportHwnd = for w in ( windows.getChildrenHWND #max ) where w[ 4 ] == "ViewPanel" do exit with w[ 1 ]
		assembly.setwindowpos ( dotNetObject "System.IntPtr" ViewportHwnd ) 0 0 0 ( width + compensation ) ( height + compensation ) 0x0026
		ForcecompleteRedraw()
	),
	
	fn storeCurrentViewportSettings = (
		activeVp = viewport.activeViewport
		layout = viewport.getLayout()
		type = viewport.getType()
		gridVisibility = viewport.getGridVisibility activeVp
		renderLevel = viewport.getRenderLevel()
		viewCubeVisibility = ViewCubeOps.Visibility
		safeFrames = displaySafeFrames
		showEdgeFaces = viewport.GetShowEdgeFaces()
	),
	
	fn restorePreviousViewportSettings = (
		
		-- Must resize the viewport first!
		resizeViewport size.x size.y
		
		-- Now we can reset to the way things were.
		viewport.setLayout layout
		viewport.setType type
		viewport.setGridVisibility activeVp gridVisibility
		viewport.setRenderLevel renderLevel
		viewport.SetShowEdgeFaces showEdgeFaces
		viewport.activeViewport = activeVp
		
		ViewCubeOps.Visibility = viewCubeVisibility
		ViewportButtonMgr.EnableButtons = true
		
		showWorldAxis true
		
		displaySafeFrames = safeFrames
	),
	
	fn setupViewportForTiling = (
		viewport.setLayout #layout_1
		viewport.setRenderLevel #smoothhighlights
		viewport.SetShowEdgeFaces false
		ViewportButtonMgr.EnableButtons = false
		displaySafeFrames = false
		showWorldAxis false
		
		-- Only store the viewport size once we've set the layout to a single window.
		size = getViewSize()
	),
	
	-- Deletes the existing tile images.
	fn deleteTileImages = (
		if tileImagesPath != undefined then (
			local tileImages = getFiles tileImagesPath
			
			for tileImg in tileImages do (
				deleteFile tileImg
			)
		)
	),
	
	-- Constructs a RsRect object from the supplied objects.
	fn constructRectFromObjects objs = (
		
		-- The rect to return.
		local rect = RsRect()
		
		-- Create a unionized rect from the supplied objects.
		rect.createFromObject objs[ 1 ]
		
		for idx = 2 to objs.count do (
			local objRect = RsRect()
			objRect.createFromObject objs[ idx ]
			
			rect.union objRect
		)
		
		-- Determine and store the number of tiles along each axis.
		numTilesX = ( ceil ( rect.width() / tileSize ) + 0.5 ) as integer
		numTilesY = ( ceil ( rect.height() / tileSize ) + 0.5 ) as integer
		
		-- Create new rect based on number of tiles, so that we can re-center and start at the correct spot.
		local tempRect = RsRect()
		tempRect.bbox_min = [ 0, 0 ]
		tempRect.bbox_max = [ tileSize * numTilesX, tileSize * numTilesY ]

		-- Construct a temporary plane and re-center it.
		tempPlane = tempRect.createPlaneObject()
		tempPlane.pos = [ ( rect.center() ).x, ( rect.center() ).y, 0 ]
		
		-- Create a new rect from the temporary plane.
		rect.createFromObject tempPlane
		
		-- Delete the temporary plane.
		delete tempPlane
		
		rect
	),
	
	-- Setup the tile camera, with the correct FOV to match the tile.
	fn setupTileCamera = (
		tileCamera = FreeCamera()
		tileCamera.orthoProjection = true
		
		local tileFov = atan ( ( 0.5 * tileSize ) / tileCamera.target_distance ) * 2.0
		
		tileCamera.fov = tileFov
		
		viewport.setCamera tileCamera
	),
	
	-- Saves current tile to disk.
	fn saveTileImg row column = (
		local vp = gw.getViewportDib()
		vp.filename = tileImagesPath + tileImageName + "_" + row as string + "_" + column as string + "." + tileImageFileType
	
		save vp
		
		vp
	),
	
	fn setup = (
		tileImages = #()
		
		compileCSharpAssembly()
		
		renderWidth = tileSize
		renderHeight = tileSize
		
		selectedObjs = selection as array
		clearSelection()
		
		deleteTileImages()
		
		storeCurrentViewportSettings()
		setupViewportForTiling()
		
		resizeViewport tileResolution tileResolution
		
		setupTileCamera()
	),
	
	fn tearDown = (
		restorePreviousViewportSettings()
		
		delete tileCamera
		
		select selectedObjs
		
		gc light:true
	),
	
	-- Stitch all of the tile images together and save out as a final bitmap.
	fn stitchTiles = (
		progressStart "Stitching Tiles..."
		
		local imgSize = [ tileResolution * numTilesX, tileResolution * numTilesY ]
		
		local stitchedImg = bitmap imgSize.x imgSize.y
		stitchedImg.filename = tileImagesPath + tileImageName + "." + tileImageFileType
		
		local currentX = 0
		local currentY = imgSize.y - tileResolution
		
		local pct = 0
		
		for row = 1 to tileImages.count do (
			local imgRow = tileImages[ row ]
			
			for column = 1 to imgRow.count do (
				local tileImg = imgRow[ column ]
				
				-- Read pixels of the current tile and commit them to the larger image.
				for y = 1 to tileResolution do (
					local pixels = getPixels tileImg [ 0, y - 1 ] tileResolution
					
					setPixels stitchedImg [ currentX, currentY + y ] pixels
				)
				
				currentX += tileResolution
			)
			
			progressUpdate ( 100 * row / tileImages.count )
			
			currentX = 0
			currentY -= tileResolution
		)
		
		save stitchedImg
		
		progressEnd()
		
		display stitchedImg
		
		stitchedImg
	),
	
	
	public
	
	fn process objs tSize tResolution tImageName tImagePath tImageFileType = (
		tileSize 					= tSize
		tileResolution 			= tResolution
		tileImageName 		= tImageName
		tileImagesPath 		= tImagePath
		tileImageFileType 	= tImageFileType
		
		-- Setup the viewport and camera.
		setup()
		
		-- Construct a RsRect from the supplied objects.
		local rect = constructRectFromObjects objs
		
		-- Start at bottom left and work our way to the right, up along the grid.  Positiong the tile camera at each
		-- grid quadrant and take a viewport snapshot.
		local startPos = rect.bottomLeft()
	
		local currentX = startPos.x
		local currentY = startPos.y
		
		for y = 1 to numTilesY do (
			
			local imgRow = #()
			
			for x = 1 to numTilesX do (
				local tileRect = RsRect()
				tileRect.bbox_min = [ currentX, currentY ]
				tileRect.bbox_max = [ currentX + tileSize, currentY + tileSize ]

				tileCenter = [ ( tileRect.center() ).x, ( tileRect.center() ).y, 0 ]
				
				-- This will construct a plane object and camera for each tile grid, for debugging purposes.
				if debug == true then (
					tileRect.createPlaneObject()
					
					local tempCam = FreeCamera()
					tempCam.orthoProjection = true
					tempCam.fov = atan ( ( 0.5 * tileSize ) / tempCam.target_distance ) * 2.0
					tempCam.pos = tileCenter
				)
				
				-- Move the tile camera to the center of the quadrant.
				tileCamera.pos = tileCenter
				
				completeRedraw()
				
				-- Take a screenshot and save it to disk.
				local tileImg = saveTileImg y x
				append imgRow tileImg
				
				currentX += tileSize
			)
			
			currentX = startPos.x
			currentY += tileSize
			
			append tileImages imgRow
		)
		
		tearDown()
		
		local stitchedImg = stitchTiles()
		
		stitchedImg
	)
)