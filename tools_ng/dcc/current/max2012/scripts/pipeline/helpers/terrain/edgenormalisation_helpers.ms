--
-- Edge Normalization Helpers Utility
--A utility that will scan through a collection of containers.  Each container's adjacent containers will 
--be generated to then determine if the container will give or receive information.
--More specifically, once a container is determine to give its information, its edges' normals
--will be propagated to all adjacent containers.
--
--
filein "pipeline/helpers/terrain/terrain_funcs.ms"

global NormalisationProcessor
global NormalisationInfoArray
global NormalisationAdjacencyList
global NormalisationMapChannel = 19

--Welds all meshes together.
fn CombineMeshes meshArray = 
(
	local startObject = undefined
	if meshArray.count > 0 then 
	(
		startObject = copy meshArray[1]
		ConvertToPoly startObject
		
		for boundsIndex = 2 to meshArray.count do
		(
			local newMesh = copy meshArray[boundsIndex]
			ConvertToPoly newMesh
			polyop.attach startObject newMesh 
		)
		
		ConvertToMesh startObject
		
		--Weld any vertices on top of each other.
		meshop.weldVertsByThreshold startObject startObject.verts 0.01
	)
	
	startObject
)


fn FindMeshInContainer containerObject meshName = 
(
	if containerObject == undefined then
	(
		undefined
	)
	else
	(			
		for child in containerObject.children do 
		(
			local childClass = classof child
			
			if ( child.name == meshName ) then
			(
				return child
			)
		)
	)
)

-- Propagated all normals down to the vertex color channel of the projection mesh.
-- Uses the projection modifier to then project the vertex channel on an arbitary map channel.
-- Then we traverse the original mesh's map channel data to propagate this back into the normal channel.
-- These are hoops jumped through because the projection modifier doesn't provide a mechanism to project normals directly.
fn ProjectMesh meshObject normalInfo = 
(
	select meshObject
	modPanel.setCurrentObject meshObject.modifiers[#Edit_Normals]

	--Propagate all of the normals down to the vertex color channel.
	vertex_colors_array = #()
	for vertexIndex=1 to meshObject.verts.count do
	(
		vertex_selection = #{vertexIndex}
		normal_selection = #{}
	
		meshObject.modifiers[#Edit_Normals].ConvertVertexSelection &vertex_selection &normal_selection
		normalIndexArray = normal_selection as array
		for i=1 to normalIndexArray.count do
		(	
			matchingNormal = meshObject.modifiers[#Edit_Normals].GetNormal normalIndexArray[i]		
			matchingNormal = matchingNormal + 1.0
			append vertex_colors_array matchingNormal
		)	
	)

	collapsestack meshObject
	
	--NOTE:  The vertex_colors_array is cached because MaxScript will not allow you to setVertColor
	--while there are modifiers on the target mesh.   The Edit_Normals modifier needs to be removed first.				
	for i=1 to vertex_colors_array.count do
	(	
		meshop.setVertColor meshObject 0 #(i) vertex_colors_array[i]  -- 0 equates to the vertex color channel.			
	)	
		
	projectionModifier = (Projection())
	addModifier meshObject projectionModifier
	modPanel.setCurrentObject meshObject.modifiers[#Projection]
		
	for originalMesh in normalInfo.originalMeshArray do
	(
		print ("Projecting onto " + originalMesh.name as string)
					
		meshop.setNumMaps originalMesh (NormalisationMapChannel+1) keep:false
		meshop.setMapSupport originalMesh NormalisationMapChannel true
		local numVerts = meshop.getNumVerts originalMesh
		meshop.setVertColor originalMesh NormalisationMapChannel #{1..numVerts} (color 255 255 255)  --Reset everything to white.
	
		projectionModifier.addObjectNode originalMesh
	)

	projectionModifier.addRegisteredProjectionType 1
	projectMapping = (projectionModifier.getProjectionType 1)
	
	projectMapping.sameAsSource = false
	projectMapping.sourceMapChannel = 0 --Vertex Color -- This is messed up; in order to select anything outside of the channel map option, you specify a negative number.
	projectMapping.targetMapChannel = NormalisationMapChannel
	projectionModifier.projectAll()
)

struct NormalisationInfo
(
	containerObject = undefined,
	receiveState = undefined,
	adjacencyList = undefined,
	meshOpenVertices = #(),
	originalMeshArray = #(),
	createdMeshArray = #(),
	vertexMapArray = #(), --A cached list of edge vertices.
	normalMapArray = #(), --A cached list of mappings between the vertex index and the normal index.
	processed = false,
	
	processor = undefined,
		
	fn AddNormalModifier meshObject = 
	(
		if ( validModifier meshObject Edit_Normals == true) then
		(
			if ( meshObject.modifiers[#Edit_Normals] != undefined ) then
			(
				meshObject.modifiers[#Edit_Normals]
			)
			else
			(
				editNormalModifier = Edit_Normals()
				addModifier meshObject editNormalModifier
				
				editNormalModifier
			)
		)
		else
		(
			print ("Unable to add normal modifier to " + meshObject.name as string + ".")
		)		
	),
	
	fn RemoveCreatedMeshes =
	(
		for createdMesh in createdMeshArray do
		(
			delete createdMesh
		)
	),

	fn RemoveNormalModifiers =
	(
		for child in containerObject.children do 
		(
			local childClass = classof child
			
			if ( Editable_Poly == childClass or Editable_Mesh == childClass or PolyMeshObject == childClass) then
			(
				collapsestack child
			)
		)
	),

	fn GetEditNormalModifier meshObject =
	(
		return meshObject.modifiers[#Edit_Normals]
	),
	
	fn GetNormalIndex vertexIndex =
	(
		local minimum = 1
		local maximum = vertexMapArray.count
		do
		(
			local mid = (maximum + minimum)/2

			if ( vertexMapArray[mid] == vertexIndex ) then
			(
				return normalMapArray[mid]
			)
			else if ( vertexMapArray[mid] > vertexIndex ) then
			(
				maximum = mid - 1
			)
			else if ( vertexMapArray[mid] < vertexIndex ) then
			(
				minimum = mid + 1 
			)
		)while( maximum >= minimum )
		
		undefined
	),

	fn Process =
	(
		if ( processed == true ) then
			return true
				
		if ( NormalisationProcessor == undefined ) then
		(
			NormalisationProcessor = dotNetObject "RSG.MaxUtils.TerrainProcessing"  --.NET Object Name 

			terrainProcessingMode =  gTerrainProcessingMode.EdgeNormalisation
			NormalisationProcessor.SetMode(terrainProcessingMode)
			NormalisationProcessor.SetEdgeNormalisationThreshold RsTerrainGlobals.EdgeNormalisationThreshold
			NormalisationProcessor.SetProcessSourceContainer false
			
			if NormalisationProcessor == undefined then 
			(
				print ("Unable to instantiate " + processor + " object.")
				return undefined
			)
		)	

		if NormalisationProcessor.IsRegistered containerObject.name == false then
		(
			print ("Gathering meshes from " + containerObject.name as string)
			originalMeshArray = RsTerrainGatherMeshes containerObject
			
			--Disabled mesh combination to allow individual meshes' interior borders to be processed.
			combinedObject = CombineMeshes originalMeshArray
			append createdMeshArray combinedObject
			meshArray = #(combinedObject)
			--meshArray = originalMeshArray
			
			for meshIndex=1 to meshArray.count do
			(					
				local currentMesh = meshArray[meshIndex]				
				local currentMeshClass = classof currentMesh
				
				--Editable_Mesh objects may not need to access the normal modifiers.
				newModifier = AddNormalModifier currentMesh 
				select currentMesh
				modPanel.setCurrentObject currentMesh.modifiers[#Edit_Normals]

				--Clear the vertex color channel.
				local numVerts = meshop.getNumVerts currentMesh
				meshop.setVertColor currentMesh 0 #{1..numVerts} (color 255 255 255)  --Reset everything to white.
				
				openVertices = #()
				
				if currentMeshClass == Editable_Poly or currentMeshClass == PolyMeshObject then
				(	
					processor = NormalisationProcessor.GetInfo containerObject.name
					local newMeshInfo = processor.GetMesh currentMesh.name 
										
					-- Load all edge information into the assembly.
					local openEdgesArray = polyop.getOpenEdges currentMesh
					for edgeIndex =1 to openEdgesArray.count do
					(
						local isOpenEdge = openEdgesArray[edgeIndex]
						if ( isOpenEdge == true ) then
						(
							local edgeVertices = polyop.getEdgeVerts currentMesh edgeIndex				

							for vertexIndex=1 to edgeVertices.count do
							(
								found = false
								for openVertexIndex in openVertices do
								(
									if ( openVertexIndex == edgeVertices[vertexIndex] ) then
									(
										found = true
										break
									)
								)
								
								if ( found == false ) then
								(
									local vertex = polyop.getVert currentMesh edgeVertices[vertexIndex]
									vector = dotNetObject "RSG.Base.Math.Vector3f" vertex.x vertex.y vertex.z
									newMeshInfo.AddVertex edgeVertices[vertexIndex] vector
									append openVertices edgeVertices[vertexIndex]
									
									vertex_selection = #{vertexIndex}
									normal_selection = #{}
									newModifier.ConvertVertexSelection &vertex_selection &normal_selection
									append vertexMapArray vertexIndex
									
									for i in normal_selection do
									(
										append normalMapArray i
										break
									)
								)
							)
						)
					)
					
					newMeshInfo.Sort()
					
					append meshOpenVertices #(currentMesh, openVertices)
				)
				else if (currentMeshClass == Editable_Mesh) then
				(
					local openEdgesArray = meshop.getOpenEdges currentMesh
					local numVerts = meshop.getNumVerts currentMesh
					edgeVertices = (meshop.getVertsUsingEdge currentMesh openEdgesArray)
					for edgeIndex =1 to numVerts do
					(
						local isOpenEdge = edgeVertices[edgeIndex]
						if ( isOpenEdge == true ) then
						(
							--local edgeVertex = meshop.getVert currentMesh edgeIndex
							append openVertices edgeIndex
						)
					)
					
					append meshOpenVertices #(currentMesh, openVertices)
					
					--------------------------------------------------------
					-- Optimized Method
					--------------------------------------------------------
					processor = NormalisationProcessor.GetInfo containerObject.name
					local newMeshInfo = processor.GetMesh currentMesh.name 
					
					-- Load all edge information into the assembly.
					local openEdgesArray = meshop.getOpenEdges currentMesh
					local numVerts = meshop.getNumVerts currentMesh
					edgeVertices = (meshop.getVertsUsingEdge currentMesh openEdgesArray)
					for vertexIndex =1 to numVerts do
					(
						local isOpenEdge = edgeVertices[vertexIndex]
						if ( isOpenEdge == true ) then
						(
							local vertex = meshop.getVert currentMesh vertexIndex
							vector = dotNetObject "RSG.Base.Math.Vector3f" vertex.x vertex.y vertex.z
							newMeshInfo.AddVertex vertexIndex vector
							
							vertex_selection = #{vertexIndex}
							normal_selection = #{}
							newModifier.ConvertVertexSelection &vertex_selection &normal_selection
							append vertexMapArray vertexIndex

							for i in normal_selection do
							(
								append normalMapArray i
								break
							)
						)
					)
						
					newMeshInfo.Sort()
				)
				
				--In order to get the EditNormalModifier's normal vertices to map correctly to the mesh's vertices, we
				--need to take all normals and make them explicit.
				local numNormals = newModifier.GetNumNormals()
				normalSelection = #{1..numNormals}
				local selectionResult = newModifier.SetSelection normalSelection
				newModifier.MakeExplicit()
			)
			
			processed = true	
		)
	)
)

--Helper function to prepare the containers for proper exporting.
fn CollapseMeshModifiers rootObject = 
(
	if rootObject == undefined then
	(
		#()
	)
	else
	(			
		local meshArray = #()
		for child in rootObject.children do 
		(
			local childClass = classof child

			if ( Editable_Poly == childClass or Editable_Mesh == childClass or PolyMeshObject == childClass) then
			(
				CollapseStack child
				append meshArray child
			)
		)
		
		meshArray
	)
)

fn PrintAdjacencyList adjacentList = 
(
	for adjIndex=1 to adjacentList.count do 
	(
		local entry = adjacentList[adjIndex]
		print ("Container: " + entry[1] as string)
		for adjListIndex=1 to entry[2].count do
		(
			print ("\t" + entry[2][adjListIndex])
		)
	)
)
fn GetAdjacencyList objNode =
(
	for adjIndex=1 to NormalisationAdjacencyList.count do 
	(
		local entry = NormalisationAdjacencyList[adjIndex]
		if ( entry[1] == objNode.name ) then
		(
			return entry[2]
		)
	)
	
	undefined

)

fn CleanupNormalisationInfo =
(
	for info in NormalisationInfoArray do
	(
		info.RemoveCreatedMeshes()
		info.RemoveNormalModifiers()
	)
)

fn GetNormalisationInfo adjNode = 
(
	for normalIndex=1 to NormalisationInfoArray.count do
	(
		if ( NormalisationInfoArray[normalIndex].containerObject.name == adjNode.name ) then
			return NormalisationInfoArray[normalIndex]
	)
	
	undefined
)

--Helper Function for the NormalisationAdjacencyList
fn TraverseAdjacencyList nodeName adjList initState = 
(	
	--Do the opposite of the initState.  This means from the calling node, the adjacent nodes are the opposite.
	--For example, if the current node is considered a "Giver", then all adjacenct nodes are "Receivers"
	local currentState = not initState
	
	--print ("Node Name: " + nodeName as string)
	--print ("Adjacency List: " + adjList as string)
	traverseList = #()
	for adjIndex=1 to adjList.count do
	(
		local adjNodeName = adjList[adjIndex]
		local adjNode = getNodeByName adjNodeName
		
		if ( adjNode != undefined ) then
		(
			local normalInfo = GetNormalisationInfo adjNode 
			if ( normalInfo == undefined ) then
			(
				containerAdjacencyList = GetAdjacencyList adjNode
				newInfo = NormalisationInfo containerObject:adjNode receiveState:currentState adjacencyList:containerAdjacencyList
				append NormalisationInfoArray newInfo
				append traverseList adjNodeName
			)
			else
			(
				if ( normalInfo.receiveState != currentState ) then
				(
					print ("Incompatible states between " + adjNodeName + " and " + nodeName.name + "!  Fatal error.")
				)
			)
		)
	)
		
	--Now for all the contents of the current list to traverse, find it's adjacency list
	--and acquire any other normal information.
	for adjIndex=1 to traverseList.count do
	(
		--Find the adjacency list for the specific container.
		local adjContainerName = traverseList[adjIndex]
		local currentAdjacentNode = getNodeByName adjContainerName
		local currentAdjacencyList = undefined
		for adjIndex=1 to NormalisationAdjacencyList.count do 
		(
			local entry = NormalisationAdjacencyList[adjIndex]
			if ( entry[1] == adjContainerName ) then
			(
				currentAdjacencyList = entry[2]
			)
		)
		
		if ( currentAdjacencyList != undefined ) then
		(
			TraverseAdjacencyList currentAdjacentNode currentAdjacencyList currentState
		)
		else
		(
			print ("Unable to find an adjacency list for " + adjContainerName as string)
		)		
	)
)

fn CreateNormalisationInfo startNodeName adjacencyList =
(
	local startNode = getNodeByName startNodeName
	
	if ( startNode == undefined ) then
	(
		#()
	)
	else
	(
		--Find the start node in the adjacency list.
		local startNodeAdjList = undefined
		for adjIndex=1 to adjacencyList.count do 
		(
			local entry = adjacencyList[adjIndex]
			if ( entry[1] == startNodeName ) then
			(
				startNodeAdjList = entry[2]
				break
			)
		)
		
		--Now with the starting adjacency list, start creating the Normalisation Info.
		NormalisationProcessor = undefined
		NormalisationInfoArray = #()
		NormalisationAdjacencyList = adjacencyList
		
		containerAdjacencyList = GetAdjacencyList startNode
		
		newInfo = NormalisationInfo containerObject:startNode receiveState:false adjacencyList:containerAdjacencyList
		append NormalisationInfoArray newInfo
		
		TraverseAdjacencyList startNode startNodeAdjList false
		
		for infoIndex=1 to NormalisationInfoArray.count do
		(
			--print ("Name: " + NormalisationInfoArray[infoIndex].containerObject.name + " State: " + NormalisationInfoArray[infoIndex].receiveState as string)
		)
	)
)
