--
-- Terrain LD Utility
--Automatically tag terrain LODs.
--
--
filein "pipeline/helpers/terrain/terrain_funcs.ms"
filein "pipeline/helpers/terrain/edgenormalisation_helpers.ms"

rollout RsTerrainLODRollout "Terrain LOD Utilities"
(
	button btnSetLODAttributes "Set LOD TXD Attributes" width:(RsTerrainLODRollout.width * .95) height:40
	
	fn ProcessTxdAttributesArray objectArray = 
	(
		local timer = ScriptTimer()
		--timer.start()
		
		local txdAttributeName = "TXD" 
		for obj in objectArray do 
		(
			local childClass = classof obj
			if ( Editable_Mesh == childClass or Editable_Poly == childClass) then
			(
				local lodChildren = getLODChildren obj
				if lodChildren.count > 0 then
				(
					--Then this has a LOD.  Adopt the first child's texture dictionary.get
					local attrClass = getAttrClass lodChildren[1]
					local attrIndex = getAttrIndex attrClass txdAttributeName
					
					local txdValue = getAttr lodChildren[1] attrIndex
					txdValue = txdValue + "_LOD"
					print ("Setting " + obj.name + "'s " + txdAttributeName + " attribute to " + txdValue as string + ".")
					setAttr obj attrIndex txdValue
				)
			)
		)

		--timer.end()
		--format "Total Time: % ms\n" ( timer.interval() )
	)
	
	on btnSetLODAttributes pressed do 
	(
		ProcessTxdAttributesArray selection
	)
	
	on RsTerrainLODRollout open do 
	(	

	)
	
	on RsTerrainLODRollout rolledUp down do 
	(
		RsSettingWrite "rsTerrainLODTagging" "rollup" (not down)
	)
)

/*
try CloseRolloutFloater RsTerrainLODRollout catch()
RsTerrainLOD = newRolloutFloater "Rockstar Terrain LOD" 200 430 50 126
addRollout RsTerrainLODRollout RsTerrainLOD rolledup:false
*/