filein "pipeline/helpers/terrain/terrain_funcs.ms"

rollout RsTerrainProcessingGeneralRollout "General"
(
	
	button btnInheritAdjacentContainers "Inherit Adjacent Containers" tooltip:"From Selected Containers"  width:(RsTerrainProcessingGeneralRollout.width *.95) height:32
	
	on btnInheritAdjacentContainers pressed do (
		
		-- This won't scale well if lot's of objects are selected in the scene.
		for obj in $selection do (
			RsInheritAdjacentContainers obj
		)
	)
)