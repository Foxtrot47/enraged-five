filein "pipeline/helpers/terrain/RsTextureTileGenerator.ms"
try( destroyDialog RsTextureTileGeneratorRollout ) catch()

rollout RsTextureTileGeneratorRollout "Texture Tile Generator"
(
	--Rockstar Banner
	dotNetControl rsBannerPanel "System.Windows.Forms.Panel" pos:[0,0] width:RsTextureTileGeneratorRollout.width height:32
	local banner = makeRsBanner dn_panel:rsBannerPanel wiki:"Texture_Tile_Generator" filename:(getThisScriptFilename())
	
	spinner spTileSize "Tile Size (meters):" range:[ 1, 4096, 128 ] type:#integer width:86 align:#right
	spinner spTileResolution "Tile Output Resolution (pixels):" range:[ 1, 4096, 512 ] type:#integer width:114 align:#right
	edittext etTileName "Tile Name:" text:"tile" labelOnTop:true
	
	edittext etOutputFolder "Output Folder:" text:"" labelOnTop:true width:160 across:2 readonly:true
	button btnPickOutputFolder "..." offset:[ 36, 16 ]
	
	button btnProcess "Process" width:200 height:32
	
	on btnPickOutputFolder pressed do (
		local dir = getSavePath "Choose output folder"
		
		if dir != undefined do (
			etOutputFolder.text = dir + "\\"
		)
	)
	
	on btnProcess pressed do (
		if etOutputFolder.text != "" then (
			if etTileName.text != "" then (
				objs = selection as array

				if objs.count > 0 then (
					
					if queryBox ( "This will delete all images under the following directory!\n\n" + etOutputFolder.text + "\n\nDo you want to continue?" ) title:"Texture Tile Generator" beep:true then (
						local tileGenerator = RsTextureTileGenerator()
						tileGenerator.process objs spTileSize.value spTileResolution.value etTileName.text etOutputFolder.text "bmp"
					)
					
				) else (
					messageBox "You must select at least one object!" title:"Texture Tile Generator"
				)
				
			) else (
				messageBox "Please enter a tile name!" title:"Texture Tile Generator"
			)
			
		) else (
			messageBox "Please choose an output folder!" title:"Texture Tile Generator"
		)
	)
	
	on RsTextureTileGeneratorRollout open do (
		banner.setup()
	)
)

createDialog RsTextureTileGeneratorRollout width:220 height:214