--
-- Terrain Utilities
--
-- 
--
filein "pipeline/util/terrain_utilities.ms"
filein "pipeline/helpers/terrain/terrain_funcs.ms"
filein "pipeline/util/RsUniversalLogViewer.ms"

fileIn (::RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Collision/ProcPainter_Live.ms")

rollout RsTerrainUtilitiesRollout "Terrain Utilities"
(
	checkbox targetSelectedOnly "Target Selection Only" width:120 height:20
	checkbox checkboxLiveUpdate "Live Update After Export" width:120 height:20 
	label outputContainer "Output Container:" align:#left
	edittext textOutputContainer ""
	dropdownlist categorySelection "Category" 
	button btnStartDropper "Start Drop Tester" width:160 height:40
	--button btnCreateVegetationContainer "Create Container (Refs Only)" width:160 height:40
	button btnExportCategory "Export Category" width:160 height:40
	button btnForceLiveUpdate "Force Live Update" width:160 height:40
	
	local vegetationData = undefined
	
	fn CreateContainerFileName containerName =
	(
		(containerName + "vegetation")
	)
	
	fn UpdateSelection =
	(
		if selection.count > 0 then
		(
			textOutputContainer.text = CreateContainerFileName selection[1].name
		)
	)
	
	fn LiveUpdate = 
	(
		GetMapBounds()
		GetCollisionBounds true
		
		idxGenerateCollision = getattrindex "Gta Object" "Generate Collision From Mesh"
		idxCollisionIsTemporary = getattrindex "Gta Collision" "Is Temporary"
		
		setQuietMode true
		for terrainMesh in RsTerrainGlobals.TerrainMeshBoundsArray do
		(
			if (GetAttr terrainMesh idxGenerateCollision)== true then
			(
				print ("Live updating " + terrainMesh.name as string + ".")
				
				--Create a collision mesh that can be used as a dummy to send this data over.
				collisionMesh = copy terrainMesh
				collapsestack collisionMesh
				resetxform collisionMesh
				collapsestack collisionMesh
				
				mesh2col collisionMesh
				SetAttr collisionMesh idxCollisionIsTemporary true 		
				collisionMesh.parent = terrainMesh
				
				--Assign collision materials.
				select terrainMesh
				
				--Apply any procedural object data to the collision mesh for export.
				gRsCollPaintTool.storeObjState terrainMesh
				gRsCollPaintTool.applyGeometryToCollision #(terrainMesh)
				
				gRsCollPaintToolLive.OnConnect()
				gRsCollPaintToolLive.UpdateLiveData collisionMesh
				gRsCollPaintToolLive.OnDisconnect()
				
				delete collisionMesh -- Delete the temporary collision mesh.
			)			
		)
		
		setQuietMode false
		messagebox "Live updating has completed."
	)

	on btnExportCategory pressed do 
	(
		terrainUtilLogFile = (RsConfigGetLogDir() + "terrainutilities.ulog")		
		RsTerrainGlobals.UniversalLog = gRsMiscULog
		RsTerrainGlobals.UniversalLog.Init "Vegetation Dropper" appendToFile:false forceLogFile:terrainUtilLogFile
		
		selectedContainer = undefined
		for selectedNode in selection do
		(
			local nodeClass = (classof selectedNode)
			if nodeClass  == Container then
			(
				selectedContainer = selectedNode
				RsTerrainGlobals.ContainerPosition = selectedContainer.pos
				
				local initUnloadedState = RsTerrainIsContainerLoaded selectedContainer
				RsTerrainLoadContainer selectedContainer
				
				local dropMode = vegetationData.GetDropType(categorySelection.selection-1)
				
				local vegetationContainerName = CreateContainerFileName selectedContainer.name
				vegetationData.SetSelectedCategoryIndex (categorySelection.selection-1)
				ExportVegetation vegetationData vegetationContainerName dropMode targetSelectedOnly.checked

				if ( initUnloadedState == true ) then
				(
					RsTerrainUnloadContainer containerObject
				)
			)
		)
		
		if RsTerrainGlobals.UniversalLog.Validate() == true or RsTerrainGlobals.EnableDebugOutput == true then
		(
			::openULogViewer modal:false allowContinue:false forceulog:RsTerrainGlobals.UniversalLog 
		)
		
		
		if checkboxLiveUpdate.checked == true then
		(
			LiveUpdate()
		)
	)
		
	on btnStartDropper pressed do 
	(
		vegetationData.SetSelectedCategoryIndex (categorySelection.selection-1)
		StartDropperTester vegetationData
	)
	
	on btnQueryLoadedTextures pressed do
	(
		QueryLoadedTextures()
	)
	
	on btnForceLiveUpdate pressed do 
	(
		LiveUpdate()
	)
	
	on RsTerrainUtilitiesRollout open do 
	(		
		vegetationData = LoadVegetation()
		
		if ( vegetationData != undefined and vegetationData != false) then
		(
			local version = vegetationData.GetVersion()
			local currentVersion = vegetationData.GetVersion()
			
			if ( version < currentVersion ) then
			(
				messagebox ("Mismatched versions.  Please update " + RsConfigGetMetadataDir() + "terrain/Vegetation.xml.")
			)
			else if (version > currentVersion ) then
			(
				messagebox ("Mismatch versions.  Please update your 3D Studio Max plug-ins.")
			)
			else
			(			
				categories = vegetationData.GetCategories()
				categorySelection.items = categories
			)
		)
		
		if selection.count > 0 then
		(
			textOutputContainer.text = CreateContainerFileName selection[1].name
		)
		
		callbacks.addscript #selectionSetChanged "RsTerrainUtilitiesRollout.updateSelection()" id:#TerrainUtilitiesSelectionChanged
	)
	
	on RsTerrainUtilitiesRollout close do 
	(
		callbacks.removescripts id:#TerrainUtilitiesSelectionChanged
	)
)

try CloseRolloutFloater RsTerrainUtilities catch()
RsTerrainUtilities = newRolloutFloater "Rockstar Terrain Utilities" 200 430 50 126
addRollout RsTerrainUtilitiesRollout RsTerrainUtilities rolledup:false
