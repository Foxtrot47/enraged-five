-- read in weather xml from runtime data

filein "pipeline/util/xml.ms"

global gWeatherData = undefined
global gWeatherTypes = undefined

try(
	gWeatherData = XmlDocument()
	gWeatherData.load (RsProjectGetCommonDir() + "/data/levels/" + RsProjectGetName()  + "/weather.xml")
	gWeatherData.ParseIntoMaxscripthierarchy()
--	gWeatherData = xmlWeatherData.maxXmlObject
	
	local content = RsGetXmlElement gWeatherData.document "CContentsOfWeatherXmlFile"
	local itemRoot = RsGetXmlElement content "WeatherTypes"
	local allItems = RsGetXmlElement itemRoot "Item" all:true
	gWeatherTypes = for item in allItems collect (RsGetXmlElement item "Name").InnerText
)catch
(
	messagebox ("Error when trying to load weather data:"+(getCurrentException())) title:"Error"
)