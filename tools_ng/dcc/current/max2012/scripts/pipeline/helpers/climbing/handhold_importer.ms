-- Rockstar Handhold Importer
-- Rockstar North
-- 11/12/2009

-- by Luke Openshaw

-- Hacked together handhold parser before i fuck off on holiday for xmas

	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	fn LoadHandholdFile file = (
		
	
		xmlDoc = XmlDocument()	
		xmlDoc.init()
		xmlDoc.load ( file )

		rootelement = xmlDoc.document.DocumentElement
		groups = rootelement.childnodes

		-- For each goup
		for i = 0 to ( groups.Count - 1 ) do (
			group = groups.itemof(i)
			
			objectList = #()
			
			handholds = group.childnodes
			for j = 0 to ( handholds.Count - 1 ) do (
				handhold = handholds.itemof(j)
				
				points = handhold.childnodes
				for k = 0 to ( points.Count - 1 ) do (
					point = points.itemof(k)
					pos = RsGetXmlElement point "pos"
					norm = RsGetXmlElement point "normal"
					
					if pos != undefined and norm != undefined then (
						attrPosX = pos.Attributes.ItemOf( "x" )
               					attrPosXVal = (attrPosX.Value as number)
               					
               					attrPosY = pos.Attributes.ItemOf( "y" )
               					attrPosYVal = (attrPosY.Value as number)
               					
               					attrPosZ = pos.Attributes.ItemOf( "z" )
               					attrPosZVal = (attrPosZ.Value as number)
               					
               					handHoldPos = point3 attrPosXVal attrPosYVal attrPosZVal
               					
               					attrNormX = norm.Attributes.ItemOf( "x" )
						attrNormXVal = (attrNormX.Value as number)

						attrNormY = norm.Attributes.ItemOf( "y" )
						attrNormYVal = (attrNormY.Value as number)

						attrNormZ = norm.Attributes.ItemOf( "z" )
               					attrNormZVal = (attrNormZ.Value as number)
               					
               					handHoldNorm = point3 attrNormXVal attrNormYVal attrNormZVal
               					
               					tempObj = Sphere()
               					tempObj.Position = handHoldPos
               					tempObj.radius = 0.01
               					
               					append objectList tempObj
               					
					)
				)
				
			)
			
			finalObj = undefined
			
			for obj in objectList do (
				if finalObj == undefined then finalObj = copy obj
				else finalObj = finalObj + obj
			)
			
			for obj in objectList do delete obj
					
			finalObj.name = ("HANDHOLD_STRUCTURE0" + (i as string))
			
			idxDontExport = GetAttrIndex "Gta Object" "Dont Export"
			setattr finalObj idxDontExport true
		)
		
		messagebox ("Completed.  " + (groups.Count as string) + " structures created")
	)
