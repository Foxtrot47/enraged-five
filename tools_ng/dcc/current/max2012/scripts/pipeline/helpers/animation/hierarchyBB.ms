-- HierarchyBB.ms
-- Helper functions for hierarchy analysis of bounds
-- Author: Gunnar Droege

global idxObjAnimBB = getattrIndex "Gta Object" "AnimatedBB"

fn RsExpandBB srcdst ext =
(
	if ext[1].x < srcdst[1].x then srcdst[1].x = ext[1].x
	if ext[1].y < srcdst[1].y then srcdst[1].y = ext[1].y
	if ext[1].z < srcdst[1].z then srcdst[1].z = ext[1].z

	if ext[2].x > srcdst[2].x then srcdst[2].x = ext[2].x
	if ext[2].y > srcdst[2].y then srcdst[2].y = ext[2].y
	if ext[2].z > srcdst[2].z then srcdst[2].z = ext[2].z
)

allowedNodeAttrClasses = #("Gta Object", "Gta Collision")
allowedNodeClasses = #(dummy, point)

fn StringContainsAnyOfPatterns theString patterns = 
(
	if patterns == unsupplied then
	(
		return false
	)

	for p in patterns where (matchPattern theString pattern:p) do
	(
--		print (theString as string+  " contains "+p as string)
		return true
	)
	return false
)

fn RsGetHierarchyBBRec root origin ignorePatterns: restrictToClass: = 
(
	local myBounds = undefined
	if 	(	0!=findItem allowedNodeAttrClasses (getattrClass root) or 
			0!=findItem allowedNodeClasses (classof root) ) and
			not (StringContainsAnyOfPatterns root.name ignorePatterns) then
	(
		myBounds = nodeGetBoundingBox root origin
	)
	
	for c in root.children do
	(
		if (restrictToClass == unsupplied) or
			((GetAttrClass c) == restrictToClass) then
		(
			local childBounds = RsGetHierarchyBBRec c origin ignorePatterns:ignorePatterns
			if undefined!=childBounds then
			(
				if undefined==myBounds then
					myBounds = childBounds
				else
					RsExpandBB myBounds childBounds
			)
		)
	)
	return myBounds
)

-- the bb creation is world space by default as the attributes are saved that way
fn RsGetHierarchyBB root spaceOfObj: ignorePatterns: restrictToClass: = 
(
	if unsupplied!=spaceOfObj then
		return RsGetHierarchyBBRec root spaceOfObj.transform ignorePatterns:ignorePatterns restrictToClass:restrictToClass
	else
		return RsGetHierarchyBBRec root (matrix3 1) ignorePatterns:ignorePatterns restrictToClass:restrictToClass
)

fn drawHierarchyBBox root spaceOfObj: ignorePatterns: = 
(
	local bb = RsGetHierarchyBB root spaceOfObj:spaceOfObj ignorePatterns:ignorePatterns
	print bb
	local len = bb[2].x - bb[1].x
	local wid = bb[2].y - bb[1].y
	local hei = bb[2].z - bb[1].z
	local mid = [len/2, wid/2, 0]
	if unsupplied!=spaceOfObj then
	(
		mid = mid + spaceOfObj.pos
	)
	box width:len length:wid height:hei pos:(bb[1] + mid)
)

-- AnimatedBB attribute bounds are saved out in world space.
fn drawAttrBBox root space:#world = 
(
	
	local bbstring = getattr root idxObjAnimBB
	if "0"==bbstring then
		return false
	local animbb = filterString bbstring " "
	for k=1 to animBB.count do 
		animBB[k] = animBB[k] as float
	local bb = #([animbb[1],animbb[2],animbb[3]],[animbb[4],animbb[5],animbb[6]])
	print bb
	local len = bb[2].x - bb[1].x
	local wid = bb[2].y - bb[1].y
	local hei = bb[2].z - bb[1].z
	local mid = [len/2, wid/2, 0]
	if space==#local then
	(
		mid = mid + root.pos
	)
	local newBox = box width:len length:wid height:hei pos:(bb[1] + mid)
	selectmore newBox
)

fn drawAttrBBoxes root space:#world = 
(
	drawAttrBBox root space:space
	
	for c in root.children do
		drawAttrBBoxes c space:space
)

-- the IDE bounds are local by default
fn RsReadIinIDEBB root space:#local = 
(
	while undefined!=root.parent do root = root.parent
	if undefined==RsMapIdeFilename or undefiend==root then
		return false
	
	local f = openFile RsMapIdeFilename mode:"r"
	if undefined==f then
		return false
	
	while undefined != (linestring =  readLine f) do
	(
		local lineTokens = filterString linestring ", "
		if lineTokens[1]==root.name then
		(
			local boundFloats = for i=7 to 12 collect (lineTokens[i] as float)
			local bb = #((point3 boundFloats[1] boundFloats[2] boundFloats[3]), (point3 boundFloats[4] boundFloats[5] boundFloats[6]))
			print bb
			local len = bb[2].x - bb[1].x
			local wid = bb[2].y - bb[1].y
			local hei = bb[2].z - bb[1].z
			local mid = [len/2, wid/2, 0]
			if space==#local then
			(
				mid = mid + root.pos
			)
			local newBox = box width:len length:wid height:hei pos:(bb[1] + mid)
			select newBox
			exit
		)
		if eof f then
		(
			messagebox "Object not found in IDE."
			exit
		)
	)
	close f
	return true
)