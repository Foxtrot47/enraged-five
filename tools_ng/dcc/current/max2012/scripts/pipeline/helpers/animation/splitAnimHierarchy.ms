-- split animation hierarchies up into smaller ones
-- 
-- Author: Gunnar Droege
-- 

fn CountCHildrenRec obj = 
(
	local kidCount = obj.children.count
	for c in obj.children do 
		kidCount += CountCHildrenRec c
	return kidCount
)

fn childrenCountCompare v1 v2 =
(
	if not isValidNode v1 or not isvalidNode v2 then
		return 0
	
	local d = (CountCHildrenRec v1)-(CountCHildrenRec v2)
	case of
	(
		(d < 0.): -1
		(d > 0.): 1
		default: 0
	)
)

fn isValidRoot obj = 
(
	if  obj.parent == undefined or
		obj.parent.children.count == 1
	then 
		return true
	
	return false
)

-- Entry point
fn splitHierarchy objs maxNumObjs:128 = 
(
	if classof objs != ObjectSet then
		objs = #(objs)
	objs = objs as array
	for objI=objs.count to 1 by -1 do
		if not isValidRoot objs[objI] then
			deleteItem objs objI

	for obj in objs do 
		for o in obj.children do 
		(
			local thisChildNum = CountCHildrenRec o
			if (thisChildNum+1)>maxNumObjs then
			(
				messagebox (o.name+"'s sub hierarchy too large ("+thisChildNum as string+") for given max node amount: "+maxNumObjs as string)
				return false
			)
		)
	local sortedObjs = #()
	for obj in objs do 
	(
		join sortedObjs (for o in obj.children collect o)
	)
	qsort sortedObjs childrenCountCompare
	local obj = objs[1]
	local tempParent = dummy pos:obj.pos name:"tempParent"
	
-- 	for delOI = objs.count to 1 by -1 do
-- 	(
-- 		local delO = objs[delOI]
-- 		if delO!=obj then
-- 		(
-- 			delete delO
-- 		)
-- 	)
	
	for o in sortedObjs do
		o.parent = tempParent
	
	local currentParent = undefined
	local newParentIndex = 1
	local safety = 100
	local newParents = #()
	while sortedObjs.count>0 and safety>0 do
	(
		lastParent = currentParent
		currentParent = objs[1]
		if currentParent == undefined then
		(
			currentParent = copy lastParent
			selectmore currentParent
			newParentIndex += 1
		)
		else
			deleteItem objs 1
		
		local currobjectcount = 0
		for i = sortedObjs.count to 1 by -1 
			while currobjectcount<maxNumObjs 
			where (currobjectcount + (CountCHildrenRec sortedObjs[i])<maxNumObjs)
		do 
		(
			sortedObjs[i].parent = currentParent
			currobjectcount = (CountCHildrenRec currentParent)
			deleteItem sortedObjs i
		)
		safety -= 1
		append newParents currentParent
	)
	delete tempParent
	delete objs
)