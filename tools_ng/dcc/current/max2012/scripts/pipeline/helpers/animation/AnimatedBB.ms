-- Animation Helpers
-- Author: Gunnar Droege
-- Date: 21/7/11

filein "pipeline/export/maps/globals.ms"
filein "pipeline/helpers/animation/hierarchyBB.ms"

global gAnimObjects = #()
global idxObjAnimBB = getattrIndex "Gta Object" "AnimatedBB"
global gAnimCheckFirstFrame = true
global gAnimCheckErrorOccured = false

fn RsUpdateAnimatedBB obj animBB = 
(
	-- world space node BB
--	local zeroTrans = at time animationRange.start obj.transform
	local childBounds = #(obj.pos, obj.pos)
	if obj.numfaces>0 then
		childBounds = nodeGetBoundingBox obj (matrix3 1)
	else
		gRsUlog.LogError ("Degenerate object with o faces:"+obj.name) context:obj
	local rootbone = rexGetSkinRootBone obj
	if undefined!=rootbone then
	(
		childBounds = RsGetHierarchyBB rootbone
	)
	
	if gAnimCheckFirstFrame then
	(
		animBB = childBounds
		gRsUlog.LogMessage ("Initial anim bounds on "+obj.name+":"+animBB as string) context:obj
	)
	else
	(
		RsExpandBB animBB childBounds
	)
)

fn frameEvalCallback = 
(
	try
	(
		for ao in gAnimObjects do
		(
			local bbstring = getattr ao idxObjAnimBB
			local animbb = filterString bbstring " "
			if animBB.count<6 then
			(
				gRsUlog.LogMessage ("Uninitialised anim bounds on "+ao.name+". Resetting to 9999 9999 9999 -9999 -9999 -9999.") context:ao
				animBB = #(9999,9999,9999,-9999,-9999,-9999)
			)
			for k=1 to animBB.count do 
				animBB[k] = animBB[k] as float
			-- pack to BB format
			animBB = #([animBB[1],animBB[2],animBB[3]],[animBB[4],animBB[5],animBB[6]])
			RsUpdateAnimatedBB ao animBB
			-- unpack to string
			bbstring = RsStringArray (for a in animBB collect RsStringArray a " ") token:" "
			setattr ao idxObjAnimBB bbstring
		)
		if sliderTime>=animationRange.end-1 then
			stopAnimation()
		
		if not gRsUlog.Validate() then
		(
			gAnimCheckErrorOccured = true
			stopAnimation()
		)
		
		if gAnimCheckFirstFrame then
			gAnimCheckFirstFrame = false
	)
	catch
	(
		gRsUlog.LogError (getCurrentException())
		stopAnimation()
		unRegisterTimeCallback frameEvalCallback
	)
)

fn private__RsAnimBoundEvalProcess = 
(
	sliderTime = 0
	realTimePlayback = true
	timeConfiguration.playbackLoop = false
	viewport.SetRenderLevel #wireFrame
	completeRedraw()
	gAnimCheckFirstFrame = true
	
	registerTimeCallback frameEvalCallback
	select gAnimObjects
	playAnimation #selOnly
	unRegisterTimeCallback frameEvalCallback
)

fn collectCHildren root kids = 
(
	append kids root
	for c in root.children do 
		collectCHildren c kids
)

fn RsEvaluateAnimationBounds map = 
(
	
	global myobjs = #()
	for o in map.objects do
		collectCHildren o myobjs
	progressStart "collect animated objects"
	gAnimObjects = #()
	::gAnimCheckErrorOccured = false
	

	for i = 1 to myobjs.count while (RsMapProgress i myobjs.count) do 
	(
		exportItem = myobjs[i]
		
		model = exportItem
		local theSkin = model
		outname = model.name
		rootbone = rexGetSkinRootBone model
		
		if rootbone != undefined then 
		(
			model = rootbone
		)
		
		if ("Gta Object"==getattrclass theSkin) then
		(		
			local objIsAnimated = RsIsObjectAnimated theSkin needsAttribSet:false
			if not objIsAnimated then
				continue
		)
		else
		(
			continue
		)
		
		--animationRange = (interval model.pos.controller.keys[1].time model.pos.controller.keys[model.pos.controller.keys.count].time)
		local modelRange = RsGetAnimRange model
		if (modelRange.end - modelRange.start)==0 then
		(
			gRsUlog.LogWarning ("Object "+model.name+" is tagged with \"Has Anim\", but doesn't have animation on it.") context:model
			continue
		)
		else
		(
			newRange = animationRange
			if modelRange.end>newRange.end then
			(
				newRange.end = modelRange.end
			)
			else if modelRange.start<newRange.start then
			(
				newRange.start = modelRange.start
			)
			animationRange = newRange
			
			-- append to the items to check
			append gAnimObjects theSkin
		)
	)		
	
	progressEnd()
	gRsUlog.LogMessage ("Checking animated objects for their bounds:\n"+gAnimObjects as string)
	if gAnimObjects.count>0 then
		private__RsAnimBoundEvalProcess()
	
	return (not ::gAnimCheckErrorOccured)
)