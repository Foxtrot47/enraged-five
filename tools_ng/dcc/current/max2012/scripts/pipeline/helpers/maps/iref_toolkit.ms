-- 
-- File:: pipeline/helpers/maps/iref_toolkit.ms
-- Description:: IRef Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 16 March 2010
--
-----------------------------------------------------------------------------

rollout IRefScriptRoll "Script Helpers"
(
	struct IXR (ref, IXR)
	
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Iref_Toolkit" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	button btnConvert "Convert Xrefs to Irefs" pos:[13,20] width:125 height:27
	button btnRenameIXRs "Rename Irefs" pos:[146,20] width:125 height:27
	button btnAssign "Multi Assign" pos:[14,52] width:125 height:27
	button btnFix "Fix Iref Materials" pos:[146,52] width:125 height:27
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	fn QSortFunction value1 value2 = (
			if value1.ref.name < value2.ref.name then
				return -1
			else if value2.ref.name < value1.ref.name then
				return 1
			else if value1.ref.name == value2.ref.name then
				return 0
			else 
				messagebox "An error has occured"	
		)
		
	fn ConvertXRefs selobj selset = (
		areErrors = 0
	
		for obj in selset do (
			kiddySet = obj.children
			
			if isRefObj obj then (
				
				if obj.objectName == selobj.name then (
					print "found"
					newobj = InternalRef()
					newobj.transform = obj.transform
					newobj.parent = obj.parent
					dontAddIplIdx = getattrindex "Gta Object" "Dont Add To IPL"
					setattr selobj dontAddIplIdx true

					if ixref_settarget newobj selobj == true then (
						append delList obj
					) else (

						areErrors = areErrors + 1
						delete newobj
					)
				)
			)
			ConvertXRefs selobj kiddySet 
		)

		if areErrors > 0 then (
			messagebox ("there were errors, " + (areErrors as string) + " object(s) not converted")
		)

	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnRenameIXRs pressed do (
		
		IXRArr = #()

		for selobj in selection do (
			if classof selobj != InternalRef then (
				continue
			)
			
			targetObj = ixref_gettarget selobj
			theIXR = IXR IXR:selobj ref:targetobj				
			
			append IXRArr theIXR
		)
		
		qsort IXRArr QSortFunction
		
		objCount = 1
		prevIXR
		for sortedIXR in IXRArr do (
			
			if prevIXR != undefined then (
			
				targetPrevObject = prevIXR.ref
				targetPrevObjectName = targetPrevObject.name
			)
			
			targetObj = sortedIXR.ref
			targetObjName = targetObj.name
			
			if targetObjName.count > 20 then
				targetObjName = substring targetObjName 1 20
				
			sortedIXR.IXR.name = targetObjName + "0" + (objCount as string)
			
			if targetPrevObjectName != targetObjName and targetPrevObjectName != undefined then 
				objCount = 1
			else 
				objCount = objCount + 1
				
			prevIXR = sortedIXR
		)
		
	)

	on btnConvert pressed do (
		for selobj in selection do (
			if classof selobj == XRefObject then (
				continue
			)		
			
			if getattrclass selobj != "Gta Object" then (
				continue
			)		
			
			ConvertXRefs selobj rootnode.children
			
			for delobj in delList do (
				print "delbj"
				print delobj
				delete delobj
			)
			delList = #()
			
		)
	)
	
	on btnAssign pressed do (
		if classof obj == Editable_mesh then (
			sel = selection
			
			for iref in sel do (
				if classof iref == InternalRef then (
					ixref_settarget iref obj
				)
			)
		)
		else (
			messagebox "Selected object is not an editable mesh. Bailing out."
		)
	)
	
	on btnFix pressed do (
		for obj in $objects do (
			if ( InternalRef != classof obj ) then
				continue
			ixref_setmaterial obj
		)
	)
)

try CloseRolloutFloater IrefToolkit catch()
IrefToolkit = newRolloutFloater "Iref Toolkit" 300 120 50 96
addRollout IRefScriptRoll IrefToolkit