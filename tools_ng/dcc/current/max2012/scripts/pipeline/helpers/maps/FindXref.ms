--
-- File:: pipeline/helpers/maps/FindXref.ms
-- Description:: Find Xrefs
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com>
-- Date:: 22/9/2004
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Global
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout RsFindRefRoll "Find Refs" 
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Ref_Toolkit#Find_XRefs" align:#right offset:[0,-2] color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	edittext textFindName "Find Ref by source-name:" across:2 width:((RsFindRefRoll.width * 0.5) - 34) offset:[-4,-17] labelOnTop:true
	multilistbox lstFound "Found:" width:((RsFindRefRoll.width * 0.5) + 4) height:6 offset:[-16,-16]
	button butFind "Find" width:50 pos:(textFindName.pos + [textFindName.width - 50, textFindName.height + 8])

	local foundNames = #()
	local foundItems = #()

	
	fn doSearch = 
	(
		foundNames = #()
		foundItems = #()

		for obj in objects do 
		(
			case of 
			(
				(isRefObj obj):
				(		
					if matchPattern obj.objectname pattern:textFindName.text do 
					(				
						append foundNames obj.name
						append foundItems obj
					)
				)
				(isInternalRef obj):
				(
					local irefSrc = getIRefSource obj
					if (irefSrc != undefined) do 
					(
						if matchPattern irefSrc.name pattern:textFindName.text do 
						(				
							append foundNames obj.name
							append foundItems obj
						)
					)
				)
			)
		)
		
		lstFound.items = foundNames
		
		if (foundNames.count != 0) do 
		(
			lstFound.selection = #{1..foundNames.count}
		)
		
		clearSelection()
		select foundItems
		
		if (foundItems.count != 0) do 
		(
			max zoomext sel
		)
	)
	
	on butFind pressed do 
	(
		doSearch()
	)
	
	on textFindName entered value do 
	(
		doSearch()
	)
	
	on lstFound selectionEnd do 
	(
		clearSelection()
		select \
		(
			for item in lstFound.selection 
				where (isValidNode foundItems[item]) 
				collect foundItems[item]
		)
		max zoomext sel
	)
	
	-- Save rolled-up state:
	on RsFindRefRoll rolledUp down do 
	(
		RsSettingWrite "RsFindRefRoll" "rollup" (not down)
	)
)
