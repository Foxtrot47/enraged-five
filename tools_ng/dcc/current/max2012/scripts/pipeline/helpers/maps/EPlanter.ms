--
-- File:: pipeline/helpers/maps/EPlanter.ms
-- Description:: East Planter tool: Plants objects onto surface of another in Perspective view
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- 15/3/2010
-- by Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Converted from macroscript
--
-- v1.0
-- v1.01 added check to prevent double plant on first pick
-- 		 changed starting height to 0.0
-----------------------------------------------------------------------------
-- v1.0
-- v1.01 added check to prevent double plant on first pick
-- 		 changed starting height to 0.0

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global Eplanter_float
global numofObjects
	
	
-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
	rollout Eplanter_roll "Procedural Planter"
	(
		local Ebase_object
		local EPlant_object
		local Eplanting
		local moosepos
	
		hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Placement_Toolkit#Procedural_Planter" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
		group "setup"
		(
			spinner dfloat_val "floating height" range:[-10.00, 10.00, 0.00] type:#float align:#center
			pickButton pick_decobj "Pick Decal object" across:2
			pickButton pick_baseobj "Pick Base object" 
		)
		checkbox active_z "Z-rotations"
		group "Z-rotations"
		(
			checkbox randz "randomize Z-axis" enabled:false
			radiobuttons zaxis_rot labels:#("0", "90", "180", "270") align:#left enabled:false

			
		)		
		radiobuttons zaxis_align labels:#("align to face", "don't align") state: 1 align:#left
				
		checkbutton go_plant "Start" checked:false width:160
		 	
	
		on pick_decobj picked obj do 
		(
			if obj != undefined do
			(
				EPlant_object = obj
				pick_decobj.text = obj.name
			)	
		)
		
		on pick_baseobj picked obj do 
		(
			if obj != undefined do
			(
				Ebase_object = obj
				pick_baseobj.text = obj.name
			)		
		)
	
		fn drawLineQuick pointA pointB =

		(
		
		ss = SplineShape pos:pointA
		
		addNewSpline ss
		
		addKnot ss 1 #corner #line PointA
		
		addKnot ss 1 #corner #line PointB
		
		updateShape ss
		
		ss
		
		)


		fn getViewDirectionRay =
		(
			local coordSysTM = Inverse(getViewTM())
			local viewDir = -coordSysTM.row3
			local viewPt = coordSysTM.row4
			return ray viewPt viewDir
		)
		
				
		fn plantdaobject =
		(
			
			
			local ray deathray = mapScreenToWorldRay moosepos
			
			
	
			
			local px = ((deathray.dir.x * 2000)+deathray.pos.x)
			local py = ((deathray.dir.y * 2000)+deathray.pos.y)
			local pz = ((deathray.dir.z * 2000)+deathray.pos.z)
			print px
			print py
			print pz
			-- line debugger
			-- drawLineQuick deathray.pos [px,py,pz]
			
						
			local ray dahitray = IntersectRay Ebase_object deathray
			local dahitray2 = IntersectRayEx Ebase_object deathray
			print " dahitray: "
						
			if dahitray != undefined then
			(
				
				--newobj = copy EPlant_object
				--newobj.pos = dahitray.pos
			)
			
			if dahitray2 != undefined then
			(
				print " dahitray2: "
				print dahitray2[1].pos
				print dahitray2[1].dir
				
				newobj = copy EPlant_object
				newobj.pos = dahitray2[1].pos
				
				--next problem - getting the face normal right
				--newobj.dir = dahitray.dir
				dafaceid = dahitray2[2]
				--faceNormal = in coordsys Ebase_object (getFaceNormal Ebase_object dafaceid )
				faceNormal = in coordsys world (getFaceNormal Ebase_object dafaceid )
				
				--check face normal align wanted
				local zface = zaxis_align.state
				if zface == 1 then
				(
					newobj.dir = faceNormal
				)
				
				daobjdiffx =(dfloat_val.value * faceNormal.x)
				daobjdiffy = (dfloat_val.value  * faceNormal.y)
				daobjdiffz = (dfloat_val.value  * faceNormal.z)
				
				--print daobjdiffx
				--print daobjdiffy
				--print daobjdiffz
				
				move newobj  [daobjdiffx,daobjdiffy, daobjdiffz]
				
				--
				-- z-rotation stuff
				--
				--check if randomize z-axis checked, if so adjust z-axis
				print "rotation check"
				if active_z.checked == true then
				(
					if randz.checked == true then
					(
						rndZrot = eulerangles 0 0 (random 0 360)
	 					in coordsys newobj rotate newobj rndZrot					
					)
					else
					(
						if zaxis_rot.state == 1 then
						(
							--newobj.rotation.z_rotation = 0.0
							print "rotated"
						)
						else if zaxis_rot.state == 2 then 
							(
								rndZrot = eulerangles 0 0 90
	 							in coordsys world rotate newobj rndZrot
								print "rotated"
							)
							else if zaxis_rot.state == 3 then
								(
									rndZrot = eulerangles 0 0 180
	 								in coordsys world rotate newobj rndZrot
									print "rotated"
								)
								else if zaxis_rot.state == 4 then
									(
										rndZrot = eulerangles 0 0 270
	 									in coordsys world rotate newobj rndZrot
										print "rotated"
									)
						)
					
				
				)

				--print " dahitray2-1: "
				--print dahitray2[1].pos
				
			)
		
		
		)
		
		
		
		tool clickyplanter
		(
			on mousePoint clickno do
			(
				if Eplanting do
				(
					--plant object, test objects defined
					if EPlant_object != undefined then
						if Ebase_object != undefined then
							(
									--plant object()
									moosepos = viewPoint
									numofObjects = numofObjects + 1
									if numofObjects>1 then
									(
										plantdaobject()
										forceCompleteRedraw doDisabled:true
									)
								)
						)
				)
				on stop do
				(
					Eplanting = false
					go_plant.state = false
				)
			)
			
		on active_z changed state do
		(
			if active_z.checked == true then 
			(
				if zaxis_align.state == 1 then 
				( 	
					zaxis_rot.enabled = false
				) 
				else 
				(
					zaxis_rot.enabled = true
				)
				
				randz.enabled = true				
			)
			else 
			(
				zaxis_rot.enabled = false
				randz.enabled = false
			)
		
		)

		
		on randz changed state do
		(
			if randz.checked == true then 
			(
				zaxis_rot.enabled = false
			)
			else 
			(
				if zaxis_align.state == 1 then
				(
					zaxis_rot.enabled = false
				)
				else zaxis_rot.enabled = true
				--zaxis_align.state = 2
			)
		
		)
		
		on zaxis_align changed state do
		(
		
			if zaxis_align.state == 1 then 
			(
				zaxis_rot.enabled = false
			)
			else if zaxis_align.state == 2 then 
			(
				if active_z.checked == true then
				(
					zaxis_rot.enabled = true
				)
			)
			else zaxis_rot.enabled = false
				
			
		
		)

		
		on go_plant changed state do
		(
			if go_plant.state == true then
			(
				if ePlant_Object != undefined then
				(
					if eBase_Object != undefined then
					(
						Eplanting = true
						numOfObjects = 0
						startTool clickyplanter
					)
					else 
					(
						Eplanting = false
						go_plant.state = false
					)
				)
				else 
				(
					Eplanting = false
					go_plant.state = false
				)
			)
			else 
			(
				Eplanting = false
				go_plant.state = false
			)
		)
	)
