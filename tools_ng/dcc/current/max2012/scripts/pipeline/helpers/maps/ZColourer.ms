--
-- File:: pipeline/helpers/maps/ZColourer.ms
-- Description:: Blends the specified colour with the vertex colours of the specified model
-- 					  in relation to their z distance.
--
-- Author:: Greg Smith (original)
-- Date:: 14 July 2004
-----------------------------------------------------------------------------
-- HISTORY
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com> (combining with new Radiosity and Lighting tool)
-- Date:: 10 February 2010  
--
-----------------------------------------------------------------------------

global changeNightColours = false

fn GtaGetColourFace mesh index = (
	if changeNightColours == false then (
		return getVCFace mesh index
	)
	
	return getIVFace mesh index
)

fn GtaSetColourFace mesh index face = (
	if changeNightColours == false then (
		return setVCFace mesh index face
	)
	
	return setIVFace mesh index face
)

fn GtaSetNumColourVerts mesh num = (
	if changeNightColours == false then (
		setNumCPVVerts mesh num
		buildVCFaces mesh
		return 0
	)
	
	setNumIVCVerts mesh num
	buildVCFaces mesh
)

fn GtaGetNumColourVerts mesh = (
	if changeNightColours == false then (
		return getNumCPVVerts mesh
	)
	
	return getNumIVCVerts mesh
)

fn GtaGetColourVert mesh index = (
	if changeNightColours == false then (
		return getVertColor mesh index
	)
	
	colVal = getIllumVertColor mesh index
	
	return (color colVal[1] colVal[2] colVal[3])
)

fn GtaSetColourVert mesh index vert = (
	if changeNightColours == false then (
		return setVertColor mesh index vert
	)
	
	setVal = [vert.r,vert.g,vert.b]
	
	return setIllumVertColor mesh index setVal
)

	
fn GoZColourButton fadeColour type topZ bottomZ = (

	if type == 2 then (
		changeNightColours = true
	) else (
		changeNightColours = false
	)
	
	fDistanceZ = topZ - bottomZ
			
	for obj in selection do (
		if classof obj == Editable_Mesh then (
			
			vertCFaces = #()
			vertColours = #()
			
			numFaces = getnumfaces obj
			numCPVVerts = GtaGetNumColourVerts obj.mesh
			
			-- make a unique list of each of vertex colour
			-- faces
			
			for i = 1 to numFaces do (
			
				posList = #()
				colList = #()
				
				curFace = getface obj i

				append posList obj.vertices[curFace[1]].pos
				append posList obj.vertices[curFace[2]].pos
				append posList obj.vertices[curFace[3]].pos

				if numCPVVerts > 0 then (
					curVCFace = GtaGetColourFace obj.mesh i
					
					append colList (GtaGetColourVert obj.mesh curVCFace[1])
					append colList (GtaGetColourVert obj.mesh curVCFace[2])
					append colList (GtaGetColourVert obj.mesh curVCFace[3])
				) else (
					append colList (color 255 255 255)
					append colList (color 255 255 255)
					append colList (color 255 255 255)
				)
					
				for i = 1 to 3 do (
					
					if posList[i].z < topZ then (
						if posList[i].z < bottomZ then (
							colList[i] = fadeColour
						) else (

							ratio = (topZ - posList[i].z) / fDistanceZ
								
							colDiff = fadeColour.r - colList[i].r
							colList[i].r = colList[i].r + (ratio * colDiff)
							colDiff = fadeColour.g - colList[i].g
							colList[i].g = colList[i].g + (ratio * colDiff)
							colDiff = fadeColour.b - colList[i].b
							colList[i].b = colList[i].b + (ratio * colDiff)
						)
					)
				)
					
				iFaceIdx = i - 1
				curVCFace = [(iFaceIdx * 3) + 1,(iFaceIdx * 3) + 2,(iFaceIdx * 3) + 3]
				
				append vertColours colList[1]
				append vertColours colList[2]
				append vertColours colList[3]
					
				append vertCFaces curVCFace
			)
			
			GtaSetNumColourVerts obj.mesh vertColours.count
			
			for i = 1 to numFaces do (
				GtaSetColourFace obj.mesh i vertCFaces[i]
			)
			
			for i = 1 to vertColours.count do (
				GtaSetColourVert obj.mesh i vertColours[i]
			)
		)
	)
)
