--
-- File:: pipeline/helpers/maps/CreateSelSets.ms
-- Description::  Will create selection sets for a number of different types of objects
--
-- 23/1/2004
-- by Greg Smith <greg.smith@rockstarnorth.com>
--
-- 08/12/2010
-- rewritten by Neal Corbett <neal.corbett@rockstarleeds.com>
--
-----------------------------------------------------------------------------

fn RsCreateSelectionSets = 
(
	local StartTime = TimeStamp()
	pushPrompt "Collecting object-lists..."

	global RSnewSelSetNames = #()
	global RSnewSelSetObjs = #()
	global RSnewSelSetsChanged = #{}
	
	-- Function to add details for creating a new selection-set if object-count is non-zero:
	fn newSelSet setName objs = 
	(
		if (setName != undefined) and (objs.count != 0) do 
		(
			append RSnewSelSetNames setName
			append RSnewSelSetObjs objs
			
			local existingSelSet = selectionsets[setName]			
			
			local setChanged = false
			
			if (existingSelSet == undefined) or (existingSelSet.count != objs.count) then (setChanged = true) else 
			(
				for n = 1 to objs.count while not setChanged do 
				(
					if (objs[n] != existingSelSet[n]) do (setChanged = true)
				)
			)
			
			-- Only change selectionSet if it's changed, as this operation is slow
			local addSetNum = RSnewSelSetNames.count
			RSnewSelSetsChanged[addSetNum] = setChanged
		)	
	)
	
	-- Put scene's objects into lists all in one go, for easy sorting later:
	local RsAttrClasses = #()		-- R* attribute-classes used in scene
	local RsAttrClassObjs = #()		-- Objects used by above classes
	local RsObjClasses = #()		-- Node-classes used in scene
	local RsObjClassObjs = #()		-- Objects used by above classes
	
	local RsCollisionObjs = #()			-- Collision-objects
	local RsUserProps = #()		-- User Properties used in scene
	local RsUserPropObjs = #()		-- Objects using the above properties
	local RsUserPropObjVals = #()	-- User Property values used by above objects

	-- Build object/class lists:
	for obj in objects do 
	(
		-- Get object attribute-classes:
		local objClass = getAttrClass obj
		local findNum = findItem RsAttrClasses objClass
		local isCollision = (objClass == "Gta Collision")
		
		if (findNum == 0) do 
		(
			append RsAttrClasses objClass
			append RsAttrClassObjs #()
			findNum = RsAttrClasses.count
		)
		append RsAttrClassObjs[findNum] obj
		
		-- Add to collision-list:
		if isCollision do 
		(
			append RsCollisionObjs obj
			
			-- Get object User-Defined Properties:
			-- (currently only done for Collision objects)
			local objUserProperties = filterString (getUserPropBuffer obj) "\n"
			for propLine in objUserProperties do 
			(
				local equalsAt = findString propLine "="
				
				if (equalsAt != undefined) do 
				(
					local propName = toLower (trimLeft (trimRight (subString propLine 1 (equalsAt - 1))))
					local propVal = trimLeft (trimRight (subString propLine (equalsAt + 1) -1))
					
					local findNum = findItem RsUserProps propName
						
					if (findNum == 0) do 
					(
						append RsUserProps propName
						append RsUserPropObjs #()
						append RsUserPropObjVals #()
						findNum = RsUserProps.count
					)
					
					append RsUserPropObjs[findNum] obj
					append RsUserPropObjVals[findNum] propVal
				)			
			)
		)
		
		-- Get object node-classes:
		objClass = classOf obj
		findNum = findItem RsObjClasses objClass
		
		if (findNum == 0) do 
		(
			append RsObjClasses objClass
			append RsObjClassObjs #()
			findNum = RsObjClasses.count
		)
		append RsObjClassObjs[findNum] obj
	)
	
	-- Function to retrieve object-lists by class:
	fn getClassObjs classList classObjList className = 
	(
		local findNum = findItem classList className
		if (findNum == 0) then #() else classObjList[findNum]
	)
	
	fn filterByAttrib objs objType attribName = 
	(
		local idxAttrib = getAttrIndex objType attribName
		for obj in objs where (GetAttr obj idxAttrib) collect obj
	)
	
	-- Function to retrieve object-lists by boolean user-property:
	fn getBoolUserPropObjs propList propObjsList propObjValsList propName allObjs val default:true = 
	(
		local excludeObjs = #()
		local includeObjs = #()
		
		local findNum = findItem propList propName
		
		if (findNum != 0) do 
		(
			local objList = propObjsList[findNum]
			local valList = propObjValsList[findNum]
			
			for n = 1 to objList.count do 
			(
				local objVal = (matchPattern valList[n] pattern:"true") or (matchPattern valList[n] pattern:"on")
				
				if val and objVal then 
				(
					append includeObjs objList[n]
				)
				else 
				(
					append excludeObjs objList[n]
				)
			)
		)
		
		local getByDefault = (val and default)
		
		local objList = for obj in allObjs collect 
		(
			case of 
			(
				(getByDefault and (findItem excludeObjs obj == 0)):obj
				(not getByDefault and (findItem includeObjs obj != 0)):obj
				default:dontCollect
			)
		)
		
		return objList
	)
	
	local setObjs
	
	newSelSet "R* Occlusion" 		(getClassObjs RsObjClasses RsObjClassObjs Box)
	newSelSet "R* CoverPoints" 	(getClassObjs RsObjClasses RsObjClassObjs Gta_Cover)
	newSelSet "R* Attractors" 	(getClassObjs RsObjClasses RsObjClassObjs Gta_Attractor)
	newSelSet "R* Portals" 			(getClassObjs RsObjClasses RsObjClassObjs GtaMloPortal)
	newSelSet "R* MiloRooms" 	(getClassObjs RsObjClasses RsObjClassObjs GtaMloRoom)
	newSelSet "R* MiloMeshes" 	(getClassObjs RsObjClasses RsObjClassObjs Gta_MILOTri)
	newSelSet "R* Xrefs" 			(getClassObjs RsObjClasses RsObjClassObjs XRefObject)
	
	-- Make non-ref Geometry selection-set --
	if (RSrefFuncs != undefined) do 
	(
		local NonRefGeomObjs = for obj in geometry where not (isRefObj obj) collect obj
		newSelSet "R* non-ref geometry" NonRefGeomObjs
	)
	
	local RSrefObjs = #()
	
	-- Make RSref selection-sets --
	if (RSrefFuncs != undefined) do 
	(
		local RSrefObjs = for obj in geometry where (isRSref obj includeDelegates:true) collect obj
		newSelSet "R* RSrefs" RSrefObjs
		
		newSelSet "R* RSrefs:ContainerLods" (for obj in RSrefObjs where (isKindOf obj RsContainerLodRef) collect obj)
		
		local definedRefs = for obj in RSrefObjs where (obj.refDef != undefined) collect obj
		
		newSelSet "R* RSrefs:Dynamic" (for obj in definedRefs where (obj.refDef.isDynamic) collect obj)
		newSelSet "R* RSrefs:Fragment" (for obj in definedRefs where (obj.refDef.isFragment) collect obj)
		newSelSet "R* RSrefs:LOD" (for obj in definedRefs where (obj.refDef.isLOD) collect obj)
		newSelSet "R* RSrefs:HasLights" (for obj in definedRefs where (obj.refDef.hasLights) collect obj)
		newSelSet "R* RSrefs:HasScenario" (for obj in definedRefs where (obj.refDef.hasScenario) collect obj)
		newSelSet "R* RSrefs:Milo" (for obj in definedRefs where (obj.refDef.objType == #milo) collect obj)
		newSelSet "R* RSrefs:StatedAnim" (for obj in definedRefs where (obj.refDef.objType == #anim) collect obj)
		
		-- Make sure extra data is loaded for Rsrefs:
		local RsRefDefs = for obj in definedRefs collect obj.refDef
		makeUniqueArray RsRefDefs
		
		RsRefFuncs.loadTxds RsRefDefs
		RsRefFuncs.loadIplGroups RsRefDefs
	)
	
	-- Make InternalRefs selection-set --
	local Irefs
	(
		Irefs = (getClassObjs RsObjClasses RsObjClassObjs InternalRef)
		join Irefs (getClassObjs RsObjClasses RsObjClassObjs ::RsInternalRef)
	
		newSelSet "R* InternalRefs" Irefs
	)
	
	-- Make InternalRef-parents selection-set --
	(
		local irefParents = #()
		
		local parentObj
		for obj in Irefs do 
		(
			parentObj = getIRefSource obj
			
			if (parentObj != undefined) do 
			(
				appendIfUnique irefParents parentObj
			)
		)
	
		newSelSet "R* InternalRef Parents" irefParents
	)
	
	-- Make ShadowCastingLights selection set --
	(
		local statObjShadIdx = getattrindex "Gta Light" "Cast Static Object Shadow"
		local dynObjShadIdx = getattrindex "Gta Light" "Cast Dynamic Object Shadow"
		setObjs = for obj in (getClassObjs RsAttrClasses RsAttrClassObjs "Gta Light") where ((getattr obj statObjShadIdx) or (getattr obj dynObjShadIdx)) collect obj

		statObjShadIdx = getattrindex "Gta LightPhoto" "Cast Static Object Shadow"
		dynObjShadIdx = getattrindex "Gta LightPhoto" "Cast Dynamic Object Shadow"
		join setObjs (for obj in (getClassObjs RsAttrClasses RsAttrClassObjs "Gta LightPhoto") where ((getattr obj statObjShadIdx) or (getattr obj dynObjShadIdx)) collect obj)
		
		newSelSet "R* shadowCasting Lights" setObjs
	)
	
	--= level1 objects: objects connected directly to rootnode or a container =--
	local level1objs = for obj in objects where (not isKindOf obj container) and ((obj.parent == undefined) or (isKindOf obj.parent container)) collect obj
	
	newSelSet "R* entryExit" (for obj in level1objs where (isKindOf obj Gta_EntryExit2) or (isKindOf obj entryexit_plugin_def) collect obj)
		
	-- Make gtaObjects selection-sets --
	(
		local gtaObjects = for obj in level1Objs where (getAttrClass obj == "Gta Object") collect obj
		newSelSet "R* GTA Objects" gtaObjects
		
		newSelSet "R* Time Objects" 			(filterByAttrib gtaObjects "Gta Object" "Is time object")
		newSelSet "R* Fragment Proxies"		(filterByAttrib gtaObjects "Gta Object" "Is FragProxy")
		newSelSet "R* Animation Proxies"		(filterByAttrib gtaObjects "Gta Object" "Is AnimProxy")
		newSelSet "R* Animated Objects" 		(filterByAttrib gtaObjects "Gta Object" "Has Anim")
		newSelSet "R* UV Animated Objects" 	(filterByAttrib gtaObjects "Gta Object" "Has UvAnim")
		newSelSet "R* Don't Export" 				(filterByAttrib gtaObjects "Gta Object" "Dont Export")
		newSelSet "R* Don't Add To IPL" 		(filterByAttrib gtaObjects "Gta Object" "Dont Add To IPL")
		newSelSet "R* Don't Add To IDE" 		(filterByAttrib gtaObjects "Gta Object" "Dont Add To IDE")
		newSelSet "R* Don't Apply Radiosity" 	(filterByAttrib gtaObjects "Gta Object" "Dont Apply Radiosity")
		newSelSet "R* Draw Last" 				(filterByAttrib gtaObjects "Gta Object" "Draw last")
		newSelSet "R* Low Priority" 				(filterByAttrib gtaObjects "Gta Object" "Low Priority")
		newSelSet "R* Is Dynamic" 				(filterByAttrib gtaObjects "Gta Object" "Is Dynamic")
		newSelSet "R* Is Breakable" 				(filterByAttrib gtaObjects "Gta Object" "Breakable")
		
		-- Selection-set for gtaObjects rotated on X/Y axes --
		(
			local angles
			setObjs = for obj in gtaObjects where 
			(
				angles = quattoeuler obj.rotation
				(abs (angles.x) > 0.001) or (abs (angles.y) > 0.001)
			) 
			collect obj
		
			newSelSet "R* Rotated X/Y" setObjs
		)
		
		-- Selection-set for gtaObjects with no collision --
		(
			local noCollision
			
			setObjs = for obj in gtaObjects collect 
			(
				noCollision = true
				
				for childobj in obj.children while noCollision do 
				(
					if (getattrclass childobj == "Gta Collision") do 
					(
						noCollision = false
					)
				)
				
				if noCollision then obj else dontCollect
			)
			
			newSelSet "R* No Collision" setObjs
		)
	)
	
	-- Create LOD-type selection-sets --
	(
		local NonLods = #()
		local LodNotLinked = #()
		local LodChildren = #()
		local LodParents = #()
		local LodParentParents = #()

		local LodParent, LodChildren, superLod
		
		for obj in gtaObjects do 
		(
			LodParent = RsSceneLink.getparent LinkType_LOD obj
			LodChildren = getLODChildren obj
			
			if LodChildren.count == 0 then 			
			(				
				if ((RsGetObjLodDistance obj) > 300.0) then 
				(
					if not isRefObj obj do 
					(					
						append LodNotLinked obj
					)
				) 
				else 
				(
					if (LodParent == undefined) then 
					(
						append NonLods obj
					) 
					else 
					(
						append LodChildren obj
					)
				)
			) 
			else 
			(
				superLod = false

				if parent == undefined do 
				(				
					for childObj in LodChildren while not superLod do 
					(
						ccobjs = getLODChildren childObj

						if ccobjs.count > 0 do 
						(
							superlod = true	
						)
					)
				)
			
				if superLod then 
				(					
					append LODParentParents obj
				) 
				else 
				(					
					append LodParents obj
				)
			)
		)
		
		newSelSet "R* LOD: Non-LODs" NonLods
		newSelSet "R* LOD Not Linked" LodNotLinked
		newSelSet "R* LOD Children" LodChildren
		newSelSet "R* LOD Parents" LodParents
		newSelSet "R* LOD Parent-Parents" LodParentParents
	)
	
	newSelSet "R* Collision" RsCollisionObjs
	
	local colMeshes = (for obj in RsCollisionObjs where (isKindOf obj Col_Mesh) collect obj)
	local colBoxes = (for obj in RsCollisionObjs where (isKindOf obj Col_Box) collect obj)
	local colSpheres = (for obj in RsCollisionObjs where (isKindOf obj Col_Sphere) collect obj)
	local colCyls = (for obj in RsCollisionObjs where (isKindOf obj Col_Cylinder) collect obj)
	local colCaps = (for obj in RsCollisionObjs where (isKindOf obj Col_Capsule) collect obj)
	
	newSelSet "R* Collision:Spheres" 	colSpheres
	newSelSet "R* Collision:Boxes"		colBoxes
	newSelSet "R* Collision:Meshes"		colMeshes
	newSelSet "R* Collision:Cylinders"	colCyls
	newSelSet "R* Collision:Capsules"	colCaps
	
	local zeroSizeCols = #()
	local minSize = RsMinCollSize
	join zeroSizeCols (for obj in colBoxes where (obj.width < minSize) or (obj.length < minSize) or (obj.height < minSize) collect obj)
	join zeroSizeCols (for obj in colSpheres where (obj.radius < minSize) collect obj)
	join zeroSizeCols (for obj in colCyls where (obj.radius < minSize) or (obj.length < minSize) collect obj)
	join zeroSizeCols (for obj in colCaps where (obj.radius < minSize) or (obj.length < minSize) collect obj)

	newSelSet "R* Collision:ZERO-SIZED" zeroSizeCols

	local moverCols = (getBoolUserPropObjs RsUserProps RsUserPropObjs RsUserPropObjVals "mover" RsCollisionObjs true)
	local weaponCols = (getBoolUserPropObjs RsUserProps RsUserPropObjs RsUserPropObjVals "weapons" RsCollisionObjs true)
	
	local moverWeaponCols = #()
	join moverWeaponCols moverCols
	join moverWeaponCols weaponCols
	moverWeaponCols = makeUniqueArray moverWeaponCols
	
	newSelSet "R* Collision:Type:Mover"		moverCols
	newSelSet "R* Collision:Type:Weapons"	weaponCols
	newSelSet "R* Collision:Type:WeaponMover"	moverWeaponCols
	newSelSet "R* Collision:Type:Camera"		(getBoolUserPropObjs RsUserProps RsUserPropObjs RsUserPropObjVals "camera" RsCollisionObjs true)
	newSelSet "R* Collision:Type:River"			(getBoolUserPropObjs RsUserProps RsUserPropObjs RsUserPropObjVals "river" RsCollisionObjs true default:false)

	newSelSet "R* ShadowMeshes" 	(for obj in helpers where (isKindOf obj Shad_Mesh) collect obj)
	
	--= level2 objects: selection-sets for children of level1 objects =--
	(
		local level2objs = #()
		for obj in level1objs do (join level2objs obj.children)
		newSelSet "R* SpriteLights" 		(for obj in level2objs where (superClassOf obj == light) collect obj)
	)
	
	-- Make selection-set for each Collision Group -- 
	local objLists = #()
	local groupNames = RsGetUniqueNames "Collision Group" objListsOut:objLists
	for n = 1 to groupNames.count do 
	(
		newSelSet ("R* CollGroup: " + groupNames[n]) objLists[n]
	)
	
	-- Make selection-set for each IPL Group -- 
	local objLists = #()
	local groupNames = RsGetUniqueNames "IPL Group" objListsOut:objLists includeRefs:true
	
	for n = 1 to groupNames.count do 
	(
		newSelSet ("R* IPLgroup: " + groupNames[n]) objLists[n]
	)

	-- Make selection-set for each TXD -- 
	local objLists = #()
	local txdsList = #()
	RsGetTxdList rootnode.children txdsList objLists includeRefs:true
	
	for n = 1 to txdsList.count do 
	(
		newSelSet ("R* TXD: " + txdsList[n]) objLists[n]
	)
	
	-- Create selection-set for objects marked as Isolated by Workbench, if csv file is found:
	(
		local isoFilename = RsConfigGetAssetsDir() + "maps/Reports/Isolated Reference Report.csv"
		local readFile = openFile isoFilename
			
		if (readFile != undefined) do 
		(
			local isoObjAreas = #()
			local isoObjNames = #()
			
			skipToNextLine readFile
			
			while not eof readFile do 
			(
				local lineItems = filterString (readLine readFile) ", \t"
				
				local objName = toLower lineItems[1]
				local objArea = toLower lineItems[2]
				
				local areaIdx = findItem isoObjAreas objArea
				if (areaIdx == 0) do 
				(
					append isoObjAreas objArea
					append isoObjNames #()
					areaIdx = isoObjAreas.count
				)
				
				append isoObjNames[areaIdx] objName
			)
			
			close readFile
			
			local isoObjs = #()
			
			for map in RsMapGetMapContainers() where (map.name != undefined) do 
			(
				local areaIdx = findItem isoObjAreas (toLower map.name)

				if (areaIdx != 0) do 
				(
					local areaObjNames = isoObjNames[areaIdx]
					
					for obj in map.objects do 
					(
						if (findItem areaObjNames (toLower obj.name)) != 0 do 
						(
							append isoObjs obj
						)
					)
				)
			)
			
			newSelSet "R* Isolated Refs" isoObjs
		)
	)
	
	popPrompt()
	
	-------------------------------
	-- Update selection-sets: --
	-------------------------------

	-- Commit selection-set updates:
	local updateNum = 0
	for setIdx in RSnewSelSetsChanged do 
	(
		pushPrompt ("Updating R* selection sets (" + (updateNum += 1) as string + "/" + RSnewSelSetsChanged.numberSet as string + ")")
		
		local setName = RSnewSelSetNames[setIdx]
		local objs = RSnewSelSetObjs[setIdx]
		
		local existingSelSet = selectionsets[setName]			
		
		local setUnchanged = true
		
		if (existingSelSet == undefined) or (existingSelSet.count != objs.count) then (setUnchanged = false) else 
		(
			for n = 1 to objs.count while setUnchanged do 
			(
				if (objs[n] != existingSelSet[n]) do (setUnchanged = false)
			)
		)
		
		-- Only change selectionSet if it's changed, as this operation is slow
		if not setUnchanged do 
		(
			selectionsets[setName] = objs
		)
		
		popPrompt()
	)
	
	
	-- Remove GTA/RS_ selection-sets that now have no members or are obsolete:
	pushPrompt "Removing empty R* selection sets"
	local setName
	for n = selectionSets.count to 1 by -1 where 
	(
		setName = getNamedSelSetName n
		(
			((findString setName "R* ") == 1) or
			(matchpattern setName pattern:"gta*")
		) and ((findItem RSnewSelSetNames setName) == 0)
	) do 
	(
		deleteItem selectionSets n
	)
	popPrompt()
	
	-- Remove globals, stay tidy:
	globalVars.remove "RSnewSelSetNames"
	globalVars.remove "RSnewSelSetObjs"
	globalVars.remove "RSnewSelSetsChanged"

	format "Selection-set creation took % seconds\n" ((TimeStamp() - StartTime) / 1000.0)

	
	OK
)

RsCreateSelectionSets()


