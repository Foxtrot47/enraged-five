-- 
-- File:: pipeline/helpers/maps/xref_toolkit.ms
-- Description:: XRef Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 16 March 2010
--
-- Author:: Adam Munson <adam.munson@rockstarnorth.com>
-- Updated:: 7 May 2010
--
-- Author:: Neal D Corbett <neal.corbett@rockstarleeds.com>
-- Updated:: 10 September 2010
-----------------------------------------------------------------------------

global RsRefToolkit
try (closeRolloutFloater RsRefToolkit) catch()

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/rb_script_generator.ms"
filein "pipeline/ui/ChangeXref.ms"
filein "pipeline/helpers/maps/FindXref.ms"
filein "pipeline/export/maps/scenexml.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------


-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsRefToolsRoll "Ref-object Helpers"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	local btnWidth = 177
	local btnHeight = 26
	local extractedRPFFiles = #()
	local tempExtractDirectory = RsConfigGetMapStreamDir() + "extract/"
	
	group "Xrefs:"
	(
		checkbutton btnRepoint 			"Repoint selected Xrefs" 	width:btnWidth height:20 across:2	tooltip:"Set selected Xrefs-objectNames to match their instance-object names"
		checkbutton btnRename 			"Rename all Xrefs" 			width:btnWidth height:20 				tooltip:"Rename Xref instance-objects to same as their ObjectNames"
		checkbutton btnFix 				"Fix Xrefs"  						width:btnWidth height:20 offset:[0,-2] across:2
		checkbutton btnReset 			"Reset Pivots for Xrefs"		width:btnWidth height:20 offset:[0,-2] 
	)
	group "RSrefs:"
	(
		checkbutton btnRSRefBrowser 	"RSref Browser" 				width:btnWidth height:btnHeight across:2	tooltip:"Open RSref Browser window\nAuto-navigates to selected object, if there is one"
		button btnRSrefSettings			"RSref System Settings" 	width:btnWidth height:btnHeight				tooltip:"Show RSref System Settings window"
		checkbutton btnRSrefInfo		"RSref Scene Info"  			width:btnWidth height:btnHeight offset:[0,-2]				tooltip:"View scene's RSref stats"
		
		checkButton btnConvertToRSrefByName "Convert to RSrefs by name" 	width:btnWidth height:btnHeight across:2	tooltip:"Convert all mesh/poly-objects named \"*_RSref*\" to RSrefObjects"
		checkbutton btnConvertToRSref	"Convert Xrefs to RSrefs" 	width:btnWidth height:btnHeight 				tooltip:"Convert all Xrefs into RSrefs, making new containers for propgroups"
		
		checkButton btnSourceXforms 	"Move to Source Transforms" width:btnWidth height:btnHeight offset:[0,-2] across:2 tooltip:"Set selected props to use their source-object's transforms"
		checkButton btnReplaceWithSource "Replace with Merged Sources" width:btnWidth height:btnHeight offset:[0,-2] tooltip:"Replace selected RsRefObjects with merged copies of source-objects"
		
		checkbutton btnP4update 		"Update RSrefs from Perforce"	width:btnWidth height:btnHeight across:2	tooltip:"Get latest versions of RSref Maxfiles and metafiles from Perforce"
		checkbutton btnImportMap		"Import RSrefs from map"  	width:btnWidth height:btnHeight				tooltip:"Import objects from an exported map's XML metafile"
		
		checkbutton btnRefreshRSrefs	"Refresh RSref data"		 	width:(btnWidth + 2) offset:[1,0] height:btnHeight across:2	tooltip:"Update RSref data from changed sourcefiles"
		checkbutton btnReloadRSrefs 	"Reload all RSref data" 		width:(btnWidth + 1) offset:[-1,0] height:btnHeight 				tooltip:"Clear all RSref caches, then load it all from scratch"
		
		checkbutton btnSelOldObjs 		"Select obsolete objects"	width:(btnWidth + 2) offset:[1,0] height:btnHeight across:2	tooltip:("Select all RSrefs with objectnames listed in " + RsRefData.SwapsCfgFile)
		checkbutton btnSwapOldObjs 	"Swap/delete obsolete objs"	width:(btnWidth + 1) offset:[-1,0] height:btnHeight 			tooltip:("Swap or obsolete RSrefs for new versions, or delete them (as defined by " + RsRefData.SwapsCfgFile + ")")
	)
	group "InternalRefs:"
	(
		checkbutton btnConvertIrefs 	"Upgrade old-style InternalRefs"	width:btnWidth height:btnHeight across:2	tooltip:"Converts selected old-style InternalRef objects into new RsInternalRefs"
		button btnIrefToolkit				"Iref Toolkit (old)" 			width:btnWidth height:btnHeight 						tooltip:"Open the Iref Toolkit, for use with old-style InternalRefs"
	)
	group "All ref-types:"
	(
		checkbutton btnFindUnres		"Select Unresolved Refs"	width:btnWidth height:btnHeight across:2
		button btnPropRender				"Prop Renderer"				width:btnWidth height:btnHeight
		checkButton btnFindOldObjMaps		"Find maps with obsolete objs"	width:btnWidth height:btnHeight 		tooltip:"Displays list of metafiles containing obsolete object-instances"
	)	
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnRepoint changed value do 
	(
		if querybox "Repoint xrefs to match their names?" do
		(
			local XrefList = for obj in selection where (isKindOf obj XRefObject) collect obj
			
			progressStart "Repointing Xrefs:"
			local currentItem = 0
			local newName
			for obj in XRefList while progressUpdate(100.0 * (currentItem += 1) / XRefList.count) do 
			(
				obj.objectName = obj.name
				updateXRef obj

				newName = uniquename (obj.objectName + "_")
				format "renaming % to %\n" obj.name newName
				obj.name = newName 
			)
			progressEnd()
		)
		btnRepoint.checked = false
	)
	
	on btnRename changed value do 
	(
		if querybox "Rename xrefs to match their references?" do 
		(
			local XrefList = for obj in objects where (isKindOf obj XRefObject) collect obj
			
			progressStart "Renaming Xrefs:"
			local currentItem = 0
			local srcName
			for obj in XRefList while progressUpdate(100.0 * (currentItem += 1) / XRefList.count) do 
			(
				srcName = obj.objectName
				format "renaming \"%\" to \"%\"\n" obj.name srcName
				obj.name = srcName
			)
			progressEnd()
		)
		btnRename.checked = false
	)
	
	on btnFix changed value do 
	(		
		local numRec = objXRefMgr.recordCount
		local totalCount = 0
		local currItem = 0

		for i=1 to numRec do
		(
			local rec = objXRefMgr.GetRecord(i)
			local items=#()
			local numItem = rec.GetItems #XRefAllType &items 	
		
			for j=1 to numItem do
			(
				if (items[j].unresolved) then totalCount = totalCount + 1
			)
		)

		if (querybox ("going to fix " + (totalCount as string) + " xrefs. continue?")) do 
		(
			progressStart "fixing xrefs"
			local tempName = "toto"
			
			local notCancelled = true
			local rec, items, numItem, originalName
			for i=1 to numRec while notCancelled do
			(		
				rec = objXRefMgr.GetRecord(i)
				items=#()
				numItem = rec.GetItems #XRefAllType &items 
				
				for j=1 to numItem while (notCancelled = (progressUpdate (100.0 * (currItem += 1)/totalCount))) do
				(
					if (items[j].unresolved) then 
					(
						-- Run garbageCollect every so often
						if ((mod currItem 50) == 0) do (gc())
					
						originalName = items[j].srcItemName
						objXRefMgr.SetXRefItemSrcName &items[j] tempname
						objXRefMgr.SetXRefItemSrcName &items[j] originalName 
					)
				)
			)
			
			progressEnd()
		)
		
		btnFix.checked = false
	)

	on btnFindUnres changed value do 
	(
		clearSelection()
		local objs = for obj in $objects where 
		(
			((isKindOf obj XRefObject) and obj.unresolved) or 
			((isRSref obj) and ((obj.refDef == undefined) or obj.refDef.badRef))
		)
		collect obj
		select objs
		btnFindUnres.checked = false
	)
		
	on btnReset changed value do 
	(
		progressStart "Updating XRefs"
		progressUpdate(0.0)
		
		local XRefList = for selobj in $selection where (isKindOf selobj XRefObject) collect selobj
		local XRefDeleteList = #()
		
		local currentItem = 0		
		for obj in XRefList while progressUpdate(100.0 * (currentItem += 1) / XRefList.count) do 
		(
			-- Replaces XRef using current XRef attributes and transform, 
			-- but updating the pivot if it's different in the prop source file
			select obj
			copyattrs()

			newobj = xrefs.addnewxrefobject obj.filename obj.objectname dupMtlNameAction:#useXRefed

			if (newobj != undefined) do 
			(
				append XRefDeleteList obj		
				newobj.name = obj.name		
				newobj.position = (obj.objectoffsetpos - newobj.objectoffsetpos)
				newobj.rotation = obj.rotation + (obj.objectoffsetrot - newobj.objectoffsetrot)
				newobj.position += obj.position
				newobj.wirecolor = obj.wirecolor
				newobj.parent = obj.parent

				if (getattrclass obj == "Gta Object") and (getattrclass newobj == "Gta Object") do 
				(
					select newobj
					pasteattrs()	
				)
			)	
			unhide newobj
		)
		delete XRefDeleteList
		progressend()
		
		btnReset.checked = false
	)

	on btnRSRefBrowser	changed value do 
	(
		RSrefFuncs.createBrowser()
		btnRSRefBrowser.checked = false
	)
	
	on btnConvertToRSref changed value do 
	(
		RSrefFuncs.replaceXrefs()
		btnConvertToRSref.checked = false
	)
	
	on btnSourceXforms changed value do 
	(
		fn quitThis = 
		(
			btnSourceXforms.checked = False
			return False
		)
		
		local success = RsRefFuncs.databaseActive toolName:"Move to Source Xforms" debugFuncSrc:"btnSourceXforms"
		if not success do (return quitThis())
		
		local objs = for obj in (getCurrentSelection()) where (isRsRef obj) and (obj.refDef != undefined) collect obj
		
		if (objs.count == 0) do (return quitThis())
			
		local refDefs = makeUniqueArray (for obj in objs collect obj.refDef)
		RsRefFuncs.loadSourceXforms refDefs
		
		undo "Use Source Transforms" on 
		(
			for obj in objs do 
			(
				obj.transform = obj.refDef.sourceXform
			)
			select objs
		)
		
		local msg = stringStream ""
		format "% props have been set to use their source-model's transforms." objs.count to:msg
		messageBox (msg as string) title:"Use Source Transforms"
		
		quitThis()
	)
	
	on btnReplaceWithSource changed value do 
	(
		local success = RsRefFuncs.databaseActive toolName:"Replace Refs with Sources" debugFuncSrc:"btnReplaceWithSource"
		
		if not success do 
		(
			btnReplaceWithSource.checked = false
			return False
		)
		
		local objs = for obj in selection where (isRsRef obj) and (obj.refDef != undefined) collect obj
		
		if (objs.count == 0) do 
		(
			btnReplaceWithSource.checked = false
			return False
		)
		
		-- Make sure node-name lists has been loaded for these refs:
		local objs = for obj in selection where (isRsRef obj) and (obj.refDef != undefined) collect obj
		local newObjs = #()
--clearListener()
--rsrefdata.refs.nodeMatchData = undefined
		RsRefFuncs.getNodeMatchData (for obj in objs collect obj.refDef)
		
		-- Merging kills undo, so we may as well turn it off now:
		clearUndoBuffer()  
		undo off 
		(
			progressStart "Replacing refs with sources..."
			
			local objCount = objs.count
			for n = 1 to objCount while progressUpdate (100.0 * n / objCount) collect 
			(
				local obj = objs[n]
				local refDef = obj.refDef
				local mergeFilename = RefDef.GetMaxFilename()
				
				local nodeMatchData = refDef.nodeMatchData
				local refNodeNames = nodeMatchData.names
				local refMatchData = nodeMatchData.matchData
				
				local objRefName = refNodeNames[1]
				
				local preMergeObjs = (objects as array)
				local mergedObjs = #()
				
				try 
				(
					mergeMAXFile mergeFilename refNodeNames #mergeDups #alwaysReparent #renameMtlDups #noRedraw quiet:True missingExtFilesAction:#logmsg missingDLLsAction:#logmsg
					mergedObjs = (for obj in objects where (appendIfUnique preMergeObjs obj) collect obj)
				)
				catch ()
--print "refMatchData:"
--print refMatchData
				
--print "mergedObjs:"
				-- Generate node-match data for merged nodes:
				local mergedObjData = for mergedObj in mergedObjs collect 
				(
					toLower (mergedObj.name + "_" + (getAttrClass mergedObj) as string + "_" + (getAttrGuid mergedObj) as string)
				)
				
				local deleteObjs = #()

				local mainObjNum = findItem mergedObjData refMatchData[1]
--print mainObjNum
--print (mergedObjData as string)
--print (refMatchData as string)
				if (mainObjNum == 0) then 
				(
					join deleteObjs mergedObjs
				)
				else 
				(
					local mainObj = mergedObjs[mainObjNum]
					
					-- Work out which objects we intended to merge, and discard any that just had matching names:
					local keepObjs = #(mainObj)
					for n = 1 to mergedObjData.count where (n != mainObjNum) do 
					(
						local mergedObj = mergedObjs[n]
						local objNum = findItem refMatchData mergedObjData[n]
						
						if (objNum == 0) then 
						(
--format "NOT FOUND: %, %\n" mergedObj.name mergedObjData[n]
							append deleteObjs mergedObj
						)
						else 
						(
--format "FOUND: %, %\n" mergedObj.name mergedObjData[n]
							append keepObjs mergedObj
						)
					)
					join newObjs keepObjs
					
					-- Move objects to replace RsRefObject:
					local objXform = (inverse mainObj.objectTransform) * obj.transform
					
					for thisObj in keepObjs where (thisObj.parent == undefined) do 
					(
						thisObj.transform *= objXform
					)
					
					append deleteObjs obj
				)
				
				if (deleteObjs.count != 0) do 
				(
					delete deleteObjs
				)
			)
			
			progressEnd()
		)

		select newObjs

		btnReplaceWithSource.checked = false
	)
	
	on btnConvertToRSrefByName changed value do 
	(
		RSrefFuncs.convertByObjName()
		btnConvertToRSrefByName.checked = false
	)
	
	on btnAddToCont changed value do 
	(
		RSrefFuncs.addSelRefsToContainer()
		btnAddToCont.checked = false
	)
	
	on btnRefreshRSrefs changed value do 
	(
		RSrefFuncs.refreshRSrefData()
		btnRefreshRSrefs.checked = false
	)
	
	on btnReloadRSrefs changed value do 
	(
		RSrefFuncs.reloadAllRSrefData()		
		btnReloadRSrefs.checked = false
	)
	
	on btnImportMap changed value do 
	(
		fileIn "pipeline/util/RSrefs_import.ms"
		btnImportMap.checked = false
	)
	
	on btnRSrefSettings pressed do 
	(
		RSrefSettingFuncs.showRSrefSettings autoOpen:false atMouse:true
	)
	
	on btnRSrefInfo changed value do 
	(
		fileIn "pipeline/util/RSrefs_sceneInfo.ms"
		btnRSrefInfo.checked = false
	)
	
	on btnPropRender pressed do 
	(
		fileIn (::RsConfigGetWildWestDir() + "script/3dsMax/Props/propRenderer.ms")
	)
	
	on btnP4update changed value do 
	(
		RSrefFuncs.P4update()
		btnP4update.checked = false
	)
	
	on btnSelOldObjs changed value do 
	(
		RSrefFuncs.selectObsoleteObjects()
		btnSelOldObjs.checked = false
	)
	
	on btnSwapOldObjs changed value do 
	(
		RSrefFuncs.swapObsoleteObjects()
		btnSwapOldObjs.checked = false
	)
	
	on btnFindOldObjMaps changed value do 
	(
		fileIn "pipeline/util/findObsoleteObjMaps.ms"
		btnFindOldObjMaps.checked = false
	)
	
	on btnConvertIrefs changed state do 
	(
		local oldObjs = for obj in selection where (isKindOf obj InternalRef) collect obj

		if (oldObjs.count == 0) then 
		(
			messageBox "No old-style InternalRef objects were selected, nothing to convert!" title:"No old IRefs to convert"
		)
		else 
		(
			undo "upgrade InternalRefs" on 
			(
				local newObjs = #()
				
				for oldObj in oldObjs do 
				(
					local objCont = RsGetObjContainer oldObj
					copyAttrs oldObj
					
					local newObj = RsInternalRef transform:oldObj.transform
					append newObjs newObj
					pasteAttrs newObj
					
					-- Add new object to same container:
					if (objCont != undefined) do 
					(
						objCont.addNodesToContent newObj
					)
					
					newObj.setSourceObj (getIRefSource oldObj)
					
					-- Transfer hierarchy links:
					newObj.parent = oldObj.parent
					setGroupMember newObj (isGroupMember oldObj)
					for child in oldObj.children do (child.parent = newObj)
					
					-- Transfer LOD links:
					RsSceneLink.SetParent LinkType_LOD newObj (RsSceneLink.GetParent LinkType_LOD oldObj)
					for LODchild in (getLODChildren oldObj) do (RsSceneLink.SetParent LinkType_LOD LODchild newObj)
					
					-- Add to same layer:
					oldObj.layer.addnode newObj
					
					delete oldObj
				)
				
				select newObjs
				
				messageBox ("Converted " + newObjs.count as string + " InternalRefs") title:"Iref Conversion Complete"
			)
		)
		
		btnConvertIrefs.state = false
	)
	on btnIrefToolkit pressed do 
	(
		filein "pipeline/helpers/maps/iref_toolkit.ms"
	)
	
	-- Save rolled-up state:
	on RsRefToolsRoll rolledUp down do 
	(
		RsSettingWrite "RsRefToolsRoll" "rollup" (not down)
	)
)


rollout RsRefToolkitBannerRoll ""
(
	dotNetControl RsBannerPanel "Panel" pos:[6,3] height:32 width:(RsRefToolkitBannerRoll.width - 6)
	local bannerStruct = makeRsBanner dn_Panel:RsBannerPanel wiki:"Ref_Toolkit" filename:(getThisScriptFilename())
	
	-- Refresh banner-icons when floater's rollouts are adjusted:
	on RsBannerPanel Paint Sender Args do 
	(
		for n = 0 to (RsBannerPanel.Controls.Count - 1) do 
		(
			RsBannerPanel.Controls.Item[n].Invalidate()
		)
	)
	
	--------------------------------------------------------------
	-- Rollout open/close
	--------------------------------------------------------------
	on RsRefToolkitBannerRoll open do
	(
		-- Initialise tech-art banner:
		bannerStruct.setup()
	)
)


-- Create floater:
(
	RsRefToolkit = newRolloutFloater "Ref Toolkit" 400 800 50 96
	
	addRollout RsRefToolkitBannerRoll RSrefToolkit border:False
	
	for thisRoll in #(RsRefToolsRoll, RsChangeRefsRoll, RsFindRefRoll) do 
	(
		local defaultRolledUp = (isProperty thisRoll #defaultRolledUp)
		addRollout thisRoll RSrefToolkit rolledup:(RsSettingsReadBoolean thisRoll.name "rollup" defaultRolledUp)
	)
)
