-- array related function 
-- @author Gunnar Droege

fn RsArrayJoin thearray token:"," property: = 
(
	local str = ""
	for item in thearray do
	(
		if ""!=str then
			append str token
		if unsupplied!=property and hasProperty item property then
			append str ((getProperty item property) as string)
		else
			append str (item as string)
	)
	return str
)