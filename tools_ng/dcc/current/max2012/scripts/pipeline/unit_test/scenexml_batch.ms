--
-- File:: scenexml_batch.ms
-- Description:: Batch file export for SceneXml files.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 30 January 2009
--

--
-- Uses
--
include "pipeline/export/maps/scenexml.ms"
include "rockstar/export/settings.ms"

--
-- Implementation
--
RsSceneXmlBatchExportDir = ( RsConfigGetProjRootDir() + "\\scenexml" )
RsSceneXmlBatchExportFilename = RsSceneXmlBatchExportDir + "\\" + RsRemoveExtension(maxFileName) + ".xml"

DisableSceneReDraw()

RsMakeSurePathExists( RsSceneXmlBatchExportDir )
if ( 0 == getFileSize( RsSceneXmlBatchExportFilename ) ) then
	RsSceneXmlExport filename:RsSceneXmlBatchExportFilename

quitMAX #noprompt

-- scenexml_batch.ms
