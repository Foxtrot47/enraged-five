import os
import copy

#TechArt startup process
if os.environ.has_key('MOTIONBUILDER_PYTHON_TECHART_STARTUP'):
	execfile(os.environ['MOTIONBUILDER_PYTHON_TECHART_STARTUP'], copy.copy(__builtins__))