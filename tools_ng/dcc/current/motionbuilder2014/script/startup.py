import os
from pyfbsdk import *


"""
We need to keep compatabilty between projects so adding a check to set the correct ini file

"""

#Get tools root
toolsRoot = os.environ['RS_TOOLSROOT']

#Sync dependencies
'''
We now call a batch file which runs our dependecny sycning ruby script meaing we can update the ruby script indeependt of the sync'd label tools.
The batch file will sync first to the ruby script before running it.
As this batch file needs to go into the tools label, we will only call it if it exists.
'''
cmd = "{0}\\techart\\dcc\motionbuilder2014\\python\\startup\\scripts\\SyncDependencies.bat".format( toolsRoot )
if not os.path.isfile(cmd): 
	cmd = "{0}\\ironlib\\prompt.bat {0}\\bin\\ironruby\\bin\\ir64.exe {0}\\techart\\dcc\motionbuilder2014\\python\\startup\\scripts\\SyncDependencies.rb".format( toolsRoot )

os.system( cmd )

#Start TechArt startup process
startupScript = '{0}\\techart\\dcc\\motionbuilder2014\\python\\startup\\startup.py'.format( toolsRoot )
FBApplication().ExecuteScript( startupScript )