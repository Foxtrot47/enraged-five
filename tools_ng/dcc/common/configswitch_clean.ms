-- 
-- File:: configswitch_clean.ms
-- Description:: Project configuration switch script.
--
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 1 September 2008
--
-- This is for switching between the content based tools sets and old locked
-- down ones.  It relies on functionality that exists in for lockeddown projects.  
--

--Maintain backward compatibility with the previous installer and ensure subsequents runs of the 
--this script work by re-installing the current tool chain, as dictated by RS_TOOLSROOT.
RsNewToolsRoot = (systemTools.getEnvVariable "RS_NEW_TOOLSROOT")
if ( RsNewToolsRoot != undefined ) then 
	RsToolsBase = ( systemTools.getEnvVariable "RS_NEW_TOOLSROOT" ) + "\\dcc"
else
	RsToolsBase = ( systemTools.getEnvVariable "RS_TOOLSROOT" ) + "\\dcc"

fn RsConfigMakeSafeSlashes pathin = (

	pathout = ""

	if classof pathin == String then (
	
		for i = 1 to pathin.count do (

			c = pathin[i]

			if c == "\\" then (

				pathout = pathout + "/"
			) else (

				pathout = pathout + c
			)
		)
		
		-- Second pass to remove duplicate '/' characters.		               
		pathout = substituteString pathout "//" "/"
	)
	
	pathout
)

fn RsConfigMakeBackSlashes pathin = (

	pathout = ""

	if classof pathin == String then (
		
		for i = 1 to pathin.count do (

			c = pathin[i]

			if c == "/" then (

				pathout = pathout + "\\"
			) else (

				pathout = pathout + c
			)
		)
	)
	
	pathout
)


fn RsConfigMakeSurePathExists filePath = (

	filePath = RsConfigMakeSafeSlashes filePath
	filePathBuild = ""
	filePathElems = filterstring filePath "/"
	
	for filePathElem in filePathElems do (
	
		filePathBuild = filePathBuild + filePathElem + "/"
	
		if findstring filePathElem ":" == undefined and findstring filePathElem "." == undefined then (
		
			makedir (RsConfigMakeBackSlashes filePathBuild)
		)
	)
)

fn RsConfigRemovePath path = (

	local temp = filterstring path "\\"
	filename = temp[temp.count]
	temp = filterstring filename "/"
	temp[temp.count]
)

fn RsFindFilesRecursiveCommon filelist searchdir = (
	
	join filelist (getfiles (searchdir + "*.*"))
	
	dirlist = GetDirectories (searchdir +"*")
	for dir in dirlist do (
		RsFindFilesRecursiveCommon filelist dir
	)
	
)

--
-- name: RsForceMenuRebuild
-- desc: Trigger a menu rebuild on next 3dsmax restart.
--
-- This is used to ensure we re-build the menus between project
-- switches.
--
fn RsForceMenuRebuild = (
	if RsSettingWrite != undefined then RsSettingWrite "RsSettings" "RebuildMenus" true
)

--
-- name: RsClearMenuBuild
-- desc: Clear a menu rebuild flag.
--
-- This is used after we have switched project.
--
fn RsClearMenuRebuild = (
	RsSettingWrite "RsSettings" "RebuildMenus" false
)

--
-- name: RsGetMenuRebuild
-- desc: Return true if we need to force rebuild of menus.
--
fn RsGetMenuRebuild = (
	( RsSettingsReadBoolean "RsSettings" "RebuildMenus" false )
)


fn RsCopyExistingPlugCfg full_tool_path = (
	plugcfgdir = getdir #maxData + "plugcfg/"
	cfgfiles = #()
	RsFindFilesRecursiveCommon cfgfiles plugcfgdir
	newplugcfgpath = full_tool_path + "/plugcfg/"
	
	for file in cfgfiles do (
		filetokens = filterstring file "\\"
		plugcfgdirtokens = filterstring plugcfgdir "\\"
		subpath = ""
		for i = plugcfgdirtokens.count + 1 to (filetokens.count) do (
			subpath = subpath + "/" + filetokens[i] 
		)
		
		destfile = newplugcfgpath + subpath 
		if (not DoesFileExist destfile) do 
		(			
			filename = RsConfigRemovePath file
			RsConfigMakeSurePathExists destfile
			copyFile file destfile
		)
		
	)
)

fn RsUpdateScriptPath full_tool_path = 
(
	setdir #plugcfg (full_tool_path + "/plugcfg/")
	setdir #scripts (full_tool_path + "/scripts/")
	setdir #startupScripts (full_tool_path + "/scripts/startup/")
	setdir #usermacros (full_tool_path + "/ui/macroscripts/")
	
	local iconDirWas = getDir #usericons
	local iconDirNew = (full_tool_path + "/ui/usericons/")
	setdir #usericons iconDirNew
	
	-- If userIcons directory has changed, copy across any files in the old directory:
	if iconDirNew != iconDirWas do 
	(
		for filename in (getFiles (iconDirWas + "/*.*")) do 
		(
			local copyPath = iconDirNew + (filenameFromPath filename)
			if not doesFileExist copyPath do 
			(
				copyFile filename copyPath
			)
		)
	)
)

fn RsUpdatePluginPath full_tool_path isX64 versionSubdir = (
	data_dir = getdir #maxData
	
	plugin_ini_filename = undefined
	-- Clearly this is supposed to be verison independent.  Ive hacked this in for now, whyd they ahve
	-- to go and change the bloody ini file name
	if versionSubdir == "max2009" then plugin_ini_filename = "plugin.ini"
	else plugin_ini_filename =  "Plugin.UserSettings.ini"
	
	plugin_ini = openfile(data_dir + "/" + plugin_ini_filename)
	
	write_ini = createfile (data_dir + "/temp.ini") 
	
	print plugin_ini
	
	if plugin_ini == undefined then (
			messagebox "Couln't find a plugin initialisation file.  Contact tools"
			return false
	)
	
	curr_line = readline plugin_ini
	
	while eof plugin_ini == false do (
		if curr_line == "[Directories]"	then (
			print "DIRECTORIES"
			format (curr_line + "\n") to:write_ini
			
			curr_line = readline plugin_ini
-- Find first line without the KEEP_ME token and add all lines before that as they are
			local useCurrLine = true
			local alreadyAddedDirs = #()
			while undefined!=(findString curr_line "KEEP_ME") do
			(
				local theDir = (filterstring curr_line "=")[2]
				if 0==(findItem alreadyAddedDirs theDir) then
				(
					format (curr_line+"\n") to:write_ini
					append alreadyAddedDirs theDir
				)
				else
					print ("already added custom directory "+theDir)
				curr_line = readline plugin_ini
				if curr_line[1] == "[" then 
				(
					useCurrLine = false
					exit
				)
			)
-- Create new string to add
			local label = "RsPluginPath"
--			if useCurrLine then
--				label = (filterstring curr_line "=")[1]
			if isX64 == true then 
				new_plugin_path = label + "=" + (full_tool_path + "/plugins/x64/\n")
			else 
				new_plugin_path = label + "=" + (full_tool_path + "/plugins/\n")
			
			-- Convert pluginpath to all single '/' characters, then back to '\\' characters.
			-- This allows the Plugin Manager to show the status of found plugins.
			new_plugin_path = ( RsConfigMakeSafeSlashes new_plugin_path )
			new_plugin_path = ( RsConfigMakeBackSlashes new_plugin_path )
			format new_plugin_path to:write_ini
-- add remaining paths as they are if they contain the magic letters
			while curr_line[1] != "[" do (
				if(findString curr_line "KEEP_ME" != undefined) then
				(
					local theDir = (filterstring curr_line "=")[2]
					if 0==(findItem alreadyAddedDirs theDir) then
					(
						format (curr_line+"\n") to:write_ini
						append alreadyAddedDirs theDir
					)
					else
						print ("already added custom directory "+theDir)
				)
				curr_line = readline plugin_ini
			)
		)
		
		format (curr_line + "\n") to:write_ini
		if eof plugin_ini == false then curr_line = readline plugin_ini
	)
	
	close plugin_ini
	close write_ini
	deletefile (data_dir + "/" + plugin_ini_filename)  
	renamefile (data_dir + "/temp.ini") (data_dir + "/" + plugin_ini_filename)   
	
	true
)

fn RsUpdateMaxSystemPaths curr_proj versionSubdir:"max2009" isX64:false toolset:"current" = (
	
	full_tool_path = RsToolsBase +  "/" + toolset + "/"	
	full_tool_path = full_tool_path + versionSubdir + "/"
	
	RsCopyExistingPlugCfg full_tool_path
	RsUpdateScriptPath full_tool_path
	res = RsUpdatePluginPath full_tool_path isX64 versionSubdir
	
	if res == true then 
	(
		RsForceMenuRebuild()
		messagebox "You'll need to restart max since gta/e1/e2 and jimmy have branched toolsets."
	)
)

