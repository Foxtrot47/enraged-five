'''
Simple callback to log syncing and loading of a file in perforce

Author: Mark H-B <mark.harrison-ball@rockstargames.com>

Description:
    Have no clue what i wrote, was drunk
    guess we want to initalize a callback system for pre loading and after loading
'''

from pyfbsdk import *
import os
import RS
import RS.Perforce as p4
import time
#import RS.Core.DBconnection as DB

app = FBApplication()


'''Classes'''

class FbxInfo():
    def __init__(self):
        self.filename = None
        self.revision = 0
        self.timeSync = 0
        self.timeLoad = 0
        self.size = 0
        
'''GLOBALS'''
TSTART = 0        
FBXINFO = None

'''Methods'''
def sync(filename):
    TSTART = time.clock()
    fileState = p4.Sync(filename)
    print fileState.HeadRevision
    TEND = time.clock()
    total = (TEND-TSTART)
    return total

def getFileSize(filename):
    kb_size = (os.path.getsize(filename) / 1024)
    return kb_size




'''EVENTS'''
def CallbackProfileOpen(eventsource, event):
    global FBXINFO
    FBXINFO = FbxInfo()
    TSTART = time.clock()
     
    
def CallbackProfileOpenCompleted(eventsource, event):
    global TSTART
    global FBXINFO
    TEND = time.clock()
    
    
    if eventsource.FBXFileName != "":
        FBXINFO.size = getFileSize(app.FBXFileName)
        FBXINFO.timeLoad = (TEND-TSTART) 
        
        print FBXINFO.timeLoad
        print FBXINFO.size
    
    
    
    

app.OnFileOpen.Add(CallbackProfileOpen)   
app.OnFileOpenCompleted.Add(CallbackProfileOpenCompleted)
    
    


