import os
from pyfbsdk import *
from xml.dom import minidom

class FrameData():
    CameraName = None
    Translation = None
    Rotation = None
    FOV = None

class ParseOldCamXml():
    def __init__(self, xmlPath):
        self.xmlPath = xmlPath
        self.frameDataList = []
        
        self.main()
        
    def convertStringListToList(self, stringList):
        filteredString = ''.join(i for i in stringList if  i not in "[]'")
        resultList = [float(filteredString.split(',')[0]), float(filteredString.split(',')[1]), float(filteredString.split(',')[2])]
        return resultList
    
    def main(self):
        xmlFile = minidom.parse(self.xmlPath)
        doc = minidom.Document()
        camDataNode = xmlFile.documentElement
        counter = 0
        for frameNode in camDataNode.childNodes:
            if str(frameNode.localName).startswith('frame'):
                
                currentFrame = FrameData()
                
                for infoNode in frameNode.childNodes:
                   
                    if str(infoNode.localName) == 'cameraName':
                        cameraName = infoNode.firstChild.nodeValue
                        currentFrame.CameraName = cameraName
                    
                    if str(infoNode.localName) == 'translation':
                        transString = infoNode.firstChild.nodeValue
                        translation = self.convertStringListToList(transString)
                        currentFrame.Translation = translation
                    
                    if str(infoNode.localName) == 'rotation':
                        rotateString = infoNode.firstChild.nodeValue
                        rotation = self.convertStringListToList(rotateString)
                        currentFrame.Rotation = rotation
                    
                    if str(infoNode.localName) == 'fov':
                        fov = float(infoNode.firstChild.nodeValue)
                        currentFrame.FOV = fov
                
                self.frameDataList.append(currentFrame)
        
class CreateCamData():
    def __init__(self):
        self.frameDataList = []
        self.main()
        
        #self.createXmlFromFrameDataList()
        
    def filterValues(self, valueList):
        resultsList = []
        for eachValue in valueList:
            newValue = float("{:.6f}".format(float(eachValue)))
            resultsList.append(newValue)
        return resultsList
    
    def addElementsToXml(self, xmlDoc, xmlNode, elementName, elementValue):
        newElementName = xmlDoc.createElement(elementName)
        newElementValue = xmlDoc.createTextNode(elementValue)
        newElementName.appendChild(newElementValue)
        xmlNode.appendChild(newElementName)
    
    def createXmlFromFrameDataList(self, xmlPath):
        doc = minidom.Document()
        camDataNode = doc.createElement('camData')
        doc.appendChild(camDataNode)
        
        for i in range(len(self.frameDataList)):
            frameTag = 'frame' + str(i)
            frameNode = doc.createElement(frameTag)
            camDataNode.appendChild(frameNode)
            
            self.addElementsToXml(doc, frameNode, 'cameraName', self.frameDataList[i].CameraName)
            self.addElementsToXml(doc, frameNode, 'translation', str(self.frameDataList[i].Translation))
            self.addElementsToXml(doc, frameNode, 'rotation', str(self.frameDataList[i].Rotation))
            self.addElementsToXml(doc, frameNode, 'fov', str(self.frameDataList[i].FOV))
            
        #CLEANUP OUTPUT
        pretty_print = lambda data: '\n'.join([line for line in minidom.parseString(data).toprettyxml(indent=' '*2).split('\n') if line.strip()])
        docPretty = pretty_print(doc.toxml())
        
        #OUTPUT FILE
        xmlFile = open(xmlPath, "w")
        xmlFile.write(docPretty)
        xmlFile.close()
            
    def main(self):
        #BASIC SETUP
        lTime = FBTime()
        lPlayerControl = FBPlayerControl()
        lsys = FBSystem()
        lscene = lsys.Scene
        lsys.Scene.Renderer.UseCameraSwitcher = True
        lCameraSwitcher = FBCameraSwitcher()
        
        #CREATE TRIMMED CAM LIST
        cameraList = []
        for camera in lscene.Cameras:
            cameraList.append(camera)
        cameraList = cameraList[6:]
        plottedCams = []
        
        #DETECT ENDFRAME
        endFrame = FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetTimeString()
        if endFrame.endswith('*'):
            endFrame = endFrame[:-1]
        endFrame = int(endFrame)
        
        
        #MASTER LOOP
        for i in range(endFrame+1):
            lTime.SetFrame( i )
            lPlayerControl.Goto( lTime )
            #FBSystem().Scene.Evaluate()
            lCurCameraIndex = lCameraSwitcher.CurrentCameraIndex
            lCurCamera = cameraList[lCurCameraIndex]
            
            #PLOT DOWN CAM LAYERS
            if lCurCamera.Name not in plottedCams:
                lSystem = FBSystem()
                lSystem.CurrentTake.SetCurrentLayer(0)
                lSystem.CurrentTake.GetLayer(0).SelectLayer(True, True)
                lCurCamera.Translation.SetFocus(True)
                lCurCamera.Rotation.SetFocus(True)
                lCurCamera.PropertyList.Find('Field Of View').SetFocus(True)
                lSystem.CurrentTake.MergeLayers(FBAnimationLayerMergeOptions.kFBAnimLayerMerge_AllLayers_SelectedProperties, True, FBMergeLayerMode.kFBMergeLayerModeAdditive)
                plottedCams.append(lCurCamera.Name)
            
            #GET TRANSLATION
            tNode = lCurCamera.Translation.GetAnimationNode()
            try:
                xValue = (tNode.Nodes[0]).FCurve.Evaluate(lTime) #/ 100
            except AttributeError:
                lCurCamera.Translation.Key()
                tNode = lCurCamera.Translation.GetAnimationNode()
                xValue = (tNode.Nodes[0]).FCurve.Evaluate(lTime) #/ 100
            yValue = (tNode.Nodes[1]).FCurve.Evaluate(lTime) #/ 100 # Also minus up front #Also swap this for node 2 instead of 1
            zValue = (tNode.Nodes[2]).FCurve.Evaluate(lTime) #/ 100
            tList =  [ xValue, yValue, zValue ]
            
            #GET ROTATION
            rNode = lCurCamera.Rotation.GetAnimationNode()
            try:
                xValue = (rNode.Nodes[0]).FCurve.Evaluate(lTime) #/ 100
            except AttributeError:
                lCurCamera.Translation.Key()
                rNode = lCurCamera.Rotation.GetAnimationNode()
                xValue = (rNode.Nodes[0]).FCurve.Evaluate(lTime) #/ 100
            yValue = (rNode.Nodes[1]).FCurve.Evaluate(lTime) #/ 100 # Also minus up front #Also swap this for node 2 instead of 1
            zValue = (rNode.Nodes[2]).FCurve.Evaluate(lTime) #/ 100
            rList =  [ xValue, yValue, zValue ]
            """
            #GET ROTATION
            rNode = lCurCamera.Rotation.Data
            lVector4d = FBVector4d()
            FBRotationToQuaternion(lVector4d, rNode, FBRotationOrder.kFBYZX)
            lVector3d = FBVector3d()
            FBQuaternionToRotation(lVector3d, lVector4d, FBRotationOrder.kFBXYZ)
            rList = [lVector3d[0], lVector3d[1], lVector3d[2]-90]
            """
            #GET FOV VALUE
            fNode = lCurCamera.PropertyList.Find('Field Of View').Data
            fList = [ fNode ]
            
            #FILTER VALUES
            tList = self.filterValues(tList)
            rList = self.filterValues(rList)
            fList = self.filterValues(fList)
            
            #CREATE FRAME DATA OBJ
            currentFrame = FrameData()
            currentFrame.CameraName = lCurCamera.Name
            currentFrame.Translation = tList
            currentFrame.Rotation = rList
            currentFrame.FOV = fList[0]
            
            self.frameDataList.append(currentFrame)
            
class CompareCamData():
    def __init__(self, oldDataList, newDataList):
        self.oldDataList = oldDataList
        self.newDataList = newDataList
        self.alteredFrameList = []
        
        self.main()
        
    def checkFrameForIndexError(self, frameIndex):
        try:
            newCameraName = self.newDataList[frameIndex].CameraName
        except IndexError:
            return "ERROR"
        try:
            oldCameraName = self.oldDataList[frameIndex].CameraName
        except IndexError:
            return "ERROR"
        return "SUCCESS"
        
    def main(self):
        for i in range(len(self.newDataList)):
            
            #CHECK FOR INDEX ERROR - If endrange has changed we add any new ranges to the altered frame list
            indexStatus = self.checkFrameForIndexError(i)
            if indexStatus == "ERROR":
                self.alteredFrameList.append(i)
                continue
            
            newCameraName = self.newDataList[i].CameraName
            newTranslation = self.newDataList[i].Translation
            newRotation = self.newDataList[i].Rotation
            newFov = self.newDataList[i].FOV
            
            oldCameraName = self.oldDataList[i].CameraName
            oldTranslation = self.oldDataList[i].Translation
            oldRotation = self.oldDataList[i].Rotation
            oldFov = self.oldDataList[i].FOV
            
            if (newCameraName != oldCameraName) or (newTranslation != oldTranslation) or (newRotation != oldRotation) or (newFov != oldFov):
                self.alteredFrameList.append(i)
"""
#DEBUG TESTING
ouputXmlPath = "X:\\BRT3_INT_P1_T04.xml"
#_ParseXmlObj_ = ParseOldCamXml(ouputXmlPath)
#print _ParseXmlObj_.frameDataList[1430].Rotation
_CamDataObj_ = CreateCamData()
_CamDataObj_.createXmlFromFrameDataList(ouputXmlPath)
"""