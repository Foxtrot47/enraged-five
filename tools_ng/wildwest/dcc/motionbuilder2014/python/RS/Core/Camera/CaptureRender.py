import os, stat
import shutil
import subprocess
import RS.Config
from xml.dom import minidom
from pyfbsdk import *

class QueueCaptureRender():
    def __init__(self):
        self.queueFolder = "N:\\RSGNYC\\Production\\CaptureRenders\\fbxQueue\\"
        self.camDataFolder = "N:\\RSGNYC\\Production\\CaptureRenders\\camData\\"
        
        self.userEmail = 'None'
        self.fbxPath = 'None'
        self.fbxName = 'None'
        self.xmlPath = 'None'
        self.endFrame = 'None'
        self.projectName = 'None'
        self.captureType = 'None'
        
        self.mainStatus = self.main()
        
    def validateUser(self):
        validUsers = ['blake.buck@rockstargames.com', 'mark.harrison-ball@rockstargames.com', 'joe.rubino@rockstargames.com',
                        'gethin.aldous@rockstargames.com', 'luke.howard@rockstargames.com', 'forrest.karbowski@rockstargames.com',
                        'perry.katz@rockstargames.com', 'jason.barnes@rockstargames.com', 'rod.edge@rockstargames.com']
        self.userEmail = RS.Config.User.Email.lower()
        if self.userEmail in validUsers:
            return "VALID"
        else:
            return "INVALID"
            
    def validateFbx(self):
        #DETECT FBX FILENAME LENGTH
        self.fbxPath = FBApplication().FBXFileName
        if (len(self.fbxPath) < 10):
            FBMessageBox( "Capture Render", "Invalid Fbx - filename is less than 10 characters.", "OK" )
            return "ERROR"
        fbxFileName = os.path.split(self.fbxPath)[-1].upper()
        if (len(fbxFileName) < 10):
            FBMessageBox( "Capture Render", "Invalid Fbx - filename is less than 10 characters.", "OK" )
            return "ERROR"
        
        #DETECT STANDARD TYPE NAMES    
        standardSceneTypeList = ['INT', 'EXT', 'MCS', 'RSC']
        detectedType = 'None'
        for eachType in standardSceneTypeList:
            if eachType in fbxFileName:
                detectedType = eachType
                break
        if detectedType == 'None':
            FBMessageBox( "Capture Render", "Invalid Fbx - this fbx does not have a valid type (INT,EXT,MCS,RSC).", "OK" )
            return "ERROR"
        
        #DETECT !!SCENE ROOT
        if '!!SCENE' not in self.fbxPath.upper():
            FBMessageBox( "Capture Render", "Invalid Fbx - this fbx is not located in the '!!scenes' root folder.", "OK" )
            return "ERROR"
        
        #DETECT USER HAS SAVED CHANGES
        if FBMessageBox( "Capture Render", "Have you saved the FBX with your latest cameras?", "Yes", "No" ) != 1:
            FBMessageBox( "Capture Render", "Please save the FBX first then try again.", "OK" )
            return "ERROR"
        
        self.fbxName = fbxFileName[:-4]
        print 'FBX Validated!'
        return "SUCCESS"
        
    def detectExtraFbxInfo(self):
        #DETECT ENDFRAME
        self.endFrame = FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetTimeString()
        if self.endFrame.endswith('*'):
            self.endFrame = self.endFrame[:-1]
            
        #DETECT PROJECT
        pathParts = self.fbxPath.split("\\")
        self.projectName = pathParts[1].upper()
            
    def createNetworkFolders(self):
        pathParts = (self.queueFolder.split("\\"))[1:-1]
        currentPath = "N:\\"
        for eachFolder in pathParts:
            currentPath = currentPath + eachFolder + "\\"
            if not os.path.exists(currentPath):
                os.mkdirs(currentPath)
    
    def checkEndrange(self):
        #DETECT PREVIOUS ENDFRAME
        """
        captureOutputFolder = "N:\\RSGNYC\\Production\\CaptureRenders\\" + self.projectName + "\\" + self.fbxName + "\\"
        if os.path.exists(captureOutputFolder):
            oldFrameCount = 0
            for eachFile in os.listdir(captureOutputFolder):
                if not eachFile.lower().endswith('.jpg'):
                    continue
                else:
                    oldFrameCount = oldFrameCount + 1
        """
        oldXmlPath = "N:\\RSGNYC\\Production\\CaptureRenders\\" + self.projectName + "\\" + self.fbxName + "\\" + self.fbxName + ".xml"
        if os.path.exists(oldXmlPath):
            xmlFile = minidom.parse(oldXmlPath)
            oldEndFrame = "None"
            for node in xmlFile.getElementsByTagName("endFrame"):
                oldEndFrame = int(node.firstChild.nodeValue)
                
            #IF OUR LAST CAPTURE ENDFRAME WAS DIFFERENT WE ASK THEN FIX
            if (oldEndFrame != "None") and (oldEndFrame != int(self.endFrame)):
                if FBMessageBox( "Capture Render", "Altered endframe detected. Do you want to use the old endrange (" + str(oldEndFrame) + ") or the new endrange (" + self.endFrame + ")?\n\n\tWarning: Changing endrange will break batch import.", "Keep Old Endrange", "Use New Endrange" ) == 1:
                    self.endFrame = str(oldEndFrame)
                    fixedTimeSpan = FBTimeSpan(FBTime(0,0,0,0),FBTime(0,0,0,int(self.endFrame)))
                    FBSystem().CurrentTake.LocalTimeSpan = fixedTimeSpan
    
    def detectCaptureType(self):
        oldCamDataPath = self.camDataFolder + self.projectName + "\\" + self.fbxName + ".xml"
        print oldCamDataPath
        if not os.path.exists(oldCamDataPath):
            return "FULL"
        if FBMessageBox( "Capture Render", "Previous capture detected. Do you want to capture all frames, or only updated frames?", "FULL CAPTURE", "FAST CAPTURE" ) == 1:
            return "FULL"
        else:
            return "FAST"
        
    def deletePreviousXml(self):
        self.xmlPath = self.queueFolder + self.fbxName + ".xml"
        if os.path.exists(self.xmlPath):
            os.unlink(self.xmlPath)
    
    def copyFbxToDestFolder(self):
        destPath = self.queueFolder + self.fbxName + ".FBX"
        #DETECT PREVIOUS - And overwrite automatically
        if os.path.exists(destPath):
            fileAtt = os.stat(destPath)[0]
            if (not fileAtt & stat.S_IWRITE):
                os.chmod(destPath, stat.S_IWRITE)
            os.unlink(destPath)
        #COPY FILE
        shutil.copy(self.fbxPath, destPath)
        #MAKE FILE WRITEABLE - In case they haven't checked out the file
        fileAtt = os.stat(destPath)[0]
        if (not fileAtt & stat.S_IWRITE):
            os.chmod(destPath, stat.S_IWRITE)
    
    def addElementsToXml(self, xmlDoc, xmlNode, elementName, elementValue):
        newElementName = xmlDoc.createElement(elementName)
        newElementValue = xmlDoc.createTextNode(elementValue)
        newElementName.appendChild(newElementValue)
        xmlNode.appendChild(newElementName)
    
    def writeInfoXml(self):
        # Create New Xml Document and Set Master Node
        doc = minidom.Document()
        fbxInfo = doc.createElement('fbxInfo')
        doc.appendChild(fbxInfo)
        
        # Lists containing ordered tag names and values
        tagNameList = [ "projectName", "fbxName", "workspaceFbxPath", "endFrame", "captureType", "userEmail"]
        tagValueList = [ self.projectName, self.fbxName, self.fbxPath, self.endFrame, self.captureType, self.userEmail]
        
        # Write each key and value using the addElementsToXml function
        for i in range(len(tagNameList)):
            self.addElementsToXml(doc, fbxInfo, tagNameList[i], tagValueList[i])
        
        # Format final output to look nice
        docPretty = doc.toprettyxml(indent="    ", encoding="utf-8")
        
        # Open our file, write the output, and save
        xmlFile = open(self.xmlPath, "w")
        xmlFile.write(docPretty)
        xmlFile.close()
        
    def main(self):
        #DETECT VALID USER / EMAIL
        userStatus = self.validateUser()
        if userStatus != "VALID":
            FBMessageBox( "Capture Render", "Invalid user. Only NY camera team members are supported at this time.", "OK" )
            return "ERROR"
        
        #DETECT VALID FBX
        fbxStatus = self.validateFbx()
        if fbxStatus != "SUCCESS":
            return "ERROR"
            
        #DETECT EXTRA FBX INFO
        self.detectExtraFbxInfo()
        
        #CREATE NETWORK FOLDERS
        self.createNetworkFolders()
        
        #CHECK PREVIOUS CAPTURE AND ASK TO MATCH END FRAME
        self.checkEndrange()
        
        #DETECT FULL OR FAST CAPTURE
        self.captureType = self.detectCaptureType()
        
        #DETECT/DELETE PREVIOUS XML FILE
        self.deletePreviousXml()
        
        #COPY OVER FBX
        self.copyFbxToDestFolder()
        
        #WRITE OUT INFO XML FILE
        self.writeInfoXml()
            
        FBMessageBox( "Capture Render", "Your capture has been queued, and you will recieve an email when finished.", "OK" )
        return "SUCCESS"

def Run():
    _queueObj_ = QueueCaptureRender()
    if _queueObj_.mainStatus == "ERROR":
        print 'WARNING: Process stopped or errored before queuing FBX sucessfully.'
    else:
        print 'FBX queued successfully!'
#Run()