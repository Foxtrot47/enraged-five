import os
import time

from pyfbsdk import *

import RS.Config
import RS.Core.Server
import RS.Core.Camera.camKeyHelper as helper
import RS.Core.Camera.CameraLocker as CameraLocker

def submitTempXml():
    
    #Only Continue if TempFile Exists
    tempFilePath = 'X:/camKeyTemp.txt'
    if os.path.exists(tempFilePath):
        
        #Create Server Connection
        connection = RS.Core.Server.Connection("CamKeySaver")
        #RS.Core.Server.visible= True
        
        #Test Connection Is Working
        try:
            connection.Run("print 'testing connection'")
        except:
            #If server isn't working, close all servers, wait, and retry
            RS.Core.Server.CloseAll()
            time.sleep(2)
            connection = RS.Core.Server.Connection("CamKeySaver")
        
        #Run camKeyHelper Commands
        connection.Sync(helper)
        commandText = "import camKeyHelper as helper\nhelper.PerforceManager()"
        connection.Thread(commandText)

class CreateTempXml():
    def __init__(self, expCamMode=False):
        self.expCamMode = expCamMode
        self.tempFilePath = "x:/camKeyTemp.txt"
        
        self.main()
        
    def main(self):
        
        #Unlock Cams
        _camLockObj_ = CameraLocker.LockManager()
        _camLockObj_.keyAllKeylessCams() #Still needed in expCamMode, since some properties may not be keyed
        
        #Lock Cams
        _camLockObj_.setAllCamLocks()
        
        #Check for Previous Temp File
        if self.expCamMode == False and os.path.exists(self.tempFilePath):
            messageText = "WARNING: Your previously saved camera key data failed to submit to P4.\n\n\tDo you submit the old data now, or delete it?"
            submitOrDelete = FBMessageBox( "Camera Key Saver", messageText, "Submit Old Data", "Delete Old Data" )
            if submitOrDelete == 1:
                #Submit Old Data
                commandText = "import camKeyHelper as helper\nhelper.PerforceManager()"
                submitTempXml()
                messageText = "Your previous cam data has been queued. The current FBX key data HAS NOT been submitted.\n\n\tLock and save your cameras again to submit the current data."
                FBMessageBox( "Camera Key Saver", messageText, "Okay" )
            elif submitOrDelete == 2:
                #Delete Old Data
                os.unlink(self.tempFilePath)
                messageText = "The previous cam data was deleted. The current FBX key data HAS NOT been submitted.\n\n\tLock and save your cameras again to submit the current data."
                FBMessageBox( "Camera Key Saver", messageText, "Okay" )
        
        else:
            #Validate FBX
            fbxPath = FBApplication().FBXFileName.replace("\\", "/")
            if (self.expCamMode == True) or ('animation/cutscene/!!scenes/' in fbxPath.lower()) or ('anim/source_fbx/' in fbxPath.lower()):
            
                #Basic Variable and Property Setup
                camPropertiesList = ['Translation (Lcl)', 'Rotation (Lcl)', 'Field Of View', 'Lens', 'FOCUS (cm)',
                            'CoC', 'CoC Override', 'CoC Night', 'CoC Night Override', 'Near Outer Override', 'Near Inner Override',
                            'Far Inner Override', 'Far Outer Override', 'Motion_Blur', 'Shallow_DOF', 'Simple_DOF']
                textBlock = []
                textBlockAppend = textBlock.append
                textBlockExtend = textBlock.extend
                textBlockAppend('<?xml version="1.0" ?>\n')
                
                #Seup FBX Details and Camera Details
                textBlockAppend('<cameraDetails>\n\t<fbxInfo>\n')
                textBlockAppend("".join(['\t\t<fbxPath>', fbxPath, '</fbxPath>\n']))
                userEmail = RS.Config.User.Email.lower()
                textBlockAppend("".join(['\t\t<userEmail>', userEmail, '</userEmail>\n']))
                createdTime = str(time.time())
                textBlockAppend("".join(['\t\t<createdTime>', createdTime, '</createdTime>\n']))
                textBlockAppend('\t</fbxInfo>\n\t<switcherKeys>\n')
                
                #Create Basic MB Objects and Cameralist
                lSystem = FBSystem()
                lScene = lSystem.Scene
                cameraList = []
                for camera in lScene.Cameras:
                    cameraList.append(camera)
                cameraList = cameraList[6:]
                
                #Select Base Layer
                lSystem.CurrentTake.SetCurrentLayer(0)
                layerCount = lSystem.CurrentTake.GetLayerCount()
                
                # GET CAM INDEXES FROM SWITCHER
                switcherCamIndexes = []
                lCameraSwitcher = FBCameraSwitcher()
                lSwitcherKeys = lCameraSwitcher.PropertyList.Find('Camera Index').GetAnimationNode().FCurve.Keys
                keyTagNames = ['Time', 'Value', 'Tension', 'Continuity', 'Bias', 'TangentFlag',
                                'LeftBezierTangent', 'LeftDerivative', 'LeftTangentWeight',
                                'RightBezierTangent', 'RightDerivative', 'RightTangentWeight']
                currentKey = ['?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?']
                
                #LOOP THROUGH CAM SWITCHER
                for i in range(len(lSwitcherKeys)):
                    eachKey = lSwitcherKeys[i]
                    
                    #Add switcher index to indexList
                    currentSwitcherIndex = int(eachKey.Value)
                    if currentSwitcherIndex not in switcherCamIndexes:
                        switcherCamIndexes.append(currentSwitcherIndex)
                    
                    #Filter out * frame times
                    keyTime = eachKey.Time.GetTimeString()
                    if keyTime.endswith("*"):
                        keyTime = keyTime[:-1]
                        
                    #Create Basic Key Data
                    keyData = [keyTime, str((cameraList[int(eachKey.Value)]).Name)]
                    keyDataAppend = keyData.append
                    keyDataExtend = keyData.extend
                    
                    #Check Previous Key for Update
                    tagLineList = []
                    for k in range(len(keyData)):
                        if keyData[k] != currentKey[k]:
                            if self.expCamMode==True and k==1:
                                tagLineList.extend(['\t\t\t<', keyTagNames[k], '>ExportCamera</', keyTagNames[k], '>\n'])
                            else:
                                tagLineList.extend(['\t\t\t<', keyTagNames[k], '>', keyData[k], '</', keyTagNames[k], '>\n'])
                    if tagLineList.count('>\n') > 1:
                        textBlockAppend('\t\t<keyItem>\n')
                        textBlockExtend(tagLineList)
                        textBlockAppend('\t\t</keyItem>\n')
                        
                    #Update Current Key
                    currentKey = keyData
                    
                textBlockAppend('\t</switcherKeys>\n')
                
                #Reset SwitcherCamIndexes for ExpCamMode - to just the export cam index
                if self.expCamMode == True:
                    switcherCamIndexes = [len(cameraList)-1]
                
                #LOOP THROUGH SWITCHER CAMS
                for switcherIndex in switcherCamIndexes:
                    eachCam = cameraList[switcherIndex]
                    #print eachCam.Name
                    
                    #Skip invalid cam names
                    if '3lateral' in eachCam.Name.lower():
                        continue
                    
                    textBlockAppend('\t<camItem>\n')
                    camNameList = ['\t\t<camName>', eachCam.Name, '</camName>\n']
                    camNameLine = "".join(camNameList)
                    textBlockAppend(camNameLine)
                    
                    for eachProp in camPropertiesList:
                        #print eachProp
                        currentCamProp = eachCam.PropertyList.Find(eachProp)
                        
                        #Filter out Missing Properties
                        if not currentCamProp:
                            continue
                            
                        #Fix Broken Cams
                        if not currentCamProp.IsAnimated():
                            currentCamProp.SetAnimated(True)
                        currentAnimNode = currentCamProp.GetAnimationNode()
                        
                        #Standard Properties
                        if (eachProp != 'Translation (Lcl)') and (eachProp != 'Rotation (Lcl)'):
                            propObjExists = False
                            for i in range(layerCount):
                                lSystem.CurrentTake.SetCurrentLayer(i)
                                if currentAnimNode.FCurve.Keys:
                                    if not propObjExists:
                                        textBlockAppend('\t\t<propItem>\n')
                                        propNameList = ['\t\t\t<propName>', eachProp, '</propName>\n']
                                        propNameLine = "".join(propNameList)
                                        textBlockAppend(propNameLine)
                                        propObjExists = True
                                    
                                    #print lSystem.CurrentTake.GetLayer(i).Name
                                    textBlockAppend('\t\t\t<layerItem>\n')
                                    layerNameList = ['\t\t\t\t<layerName>', lSystem.CurrentTake.GetLayer(i).Name, '</layerName>\n']
                                    layerNameLine = "".join(layerNameList)
                                    textBlockAppend(layerNameLine)
                                    
                                    layerKeys = currentAnimNode.FCurve.Keys
                                    currentKey = ['?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?']
                                    
                                    seenTangentBreak = False
                                    for keyIndex in range(len(layerKeys)):
                                        eachKey = layerKeys[keyIndex]
                                            
                                        #Check TangentBreak
                                        if eachKey.TangentBreak == True:
                                            seenTangentBreak = True
                                            
                                        #Create Active Key Data
                                        keyData = [str(eachKey.Time.GetSecondDouble()), str(eachKey.Value), str(eachKey.Tension), str(eachKey.Continuity), str(eachKey.Bias),
                                                    "{}{}{}{}{}".format(eachKey.Interpolation.numerator, eachKey.TangentBreak.numerator, eachKey.TangentClampMode.numerator, eachKey.TangentConstantMode.numerator, eachKey.TangentMode.numerator),
                                                    str(eachKey.LeftBezierTangent), str(eachKey.LeftDerivative), str(eachKey.LeftTangentWeight), str(eachKey.RightBezierTangent), str(eachKey.RightDerivative), str(eachKey.RightTangentWeight)]
                                        if self.expCamMode == True:
                                            keyData[0] = eachKey.Time.GetTimeString().replace("*","") #expCamMode requires frames, not seconds
                                        
                                        #Create Next Key Data
                                        if keyIndex != len(layerKeys)-1:
                                            nextKey = layerKeys[keyIndex+1]
                                            nextKeyData = [str(nextKey.Time.GetSecondDouble()), str(nextKey.Value), str(nextKey.Tension), str(nextKey.Continuity), str(nextKey.Bias),
                                                            "{}{}{}{}{}".format(nextKey.Interpolation.numerator, nextKey.TangentBreak.numerator, nextKey.TangentClampMode.numerator, nextKey.TangentConstantMode.numerator, nextKey.TangentMode.numerator),
                                                            str(nextKey.LeftBezierTangent), str(nextKey.LeftDerivative), str(nextKey.LeftTangentWeight), str(nextKey.RightBezierTangent), str(nextKey.RightDerivative), str(nextKey.RightTangentWeight)]
                                            if self.expCamMode == True:
                                                nextKeyData[0] = nextKey.Time.GetTimeString().replace("*","") #expCamMode requires frames, not seconds
                                            
                                        #Check Previous Key for Update
                                        tagLineList = []
                                        for k in range(len(keyData)):
                                            #If TangentBreak is False - we skip the last 6 values
                                            if seenTangentBreak == False and k >= 6:
                                                continue
                                            #Check the Key Differs - from previous or next key
                                            differsFromPrevious = keyData[k] != currentKey[k]
                                            if len(layerKeys) == 1:
                                                differsFromNext = False
                                            else:
                                                differsFromNext = keyData[k] != nextKeyData[k]
                                            if differsFromPrevious == True or differsFromNext == True or self.expCamMode == True:
                                                tagLineList.extend(['\t\t\t\t\t<', keyTagNames[k], '>', keyData[k], '</', keyTagNames[k], '>\n'])
                                        if tagLineList.count('>\n') > 1:
                                            textBlockAppend('\t\t\t\t<keyItem>\n')
                                            textBlockExtend(tagLineList)
                                            textBlockAppend('\t\t\t\t</keyItem>\n')
                                        
                                        #Update Current Key
                                        currentKey = keyData
                                        
                                    textBlockAppend('\t\t\t</layerItem>\n')
                                    
                            if propObjExists:
                                textBlockAppend('\t\t</propItem>\n')
                
                        #Translation and Rotation Properties
                        else:
                            childNodes = currentAnimNode.Nodes
                            for g in range(len(childNodes)):
                                currentChild = childNodes[g]
                                propObjExists = False
                                for i in range(layerCount):
                                    lSystem.CurrentTake.SetCurrentLayer(i)
                                    keyCount = len(currentChild.FCurve.Keys)
                                    if keyCount > 0:
                                        if not propObjExists:
                                            textBlockAppend('\t\t<propItem>\n')
                                            propName = "".join([eachProp, 'XYZ'[g]])
                                            propNameList = ['\t\t\t<propName>', propName, '</propName>\n']
                                            propNameLine = "".join(propNameList)
                                            textBlockAppend(propNameLine)
                                            propObjExists = True
                                        
                                        #print lSystem.CurrentTake.GetLayer(i).Name
                                        textBlockAppend('\t\t\t<layerItem>\n')
                                        layerNameList = ['\t\t\t\t<layerName>', lSystem.CurrentTake.GetLayer(i).Name, '</layerName>\n']
                                        layerNameLine = "".join(layerNameList)
                                        textBlockAppend(layerNameLine)
                                        
                                        childKeys = childNodes[g].FCurve.Keys
                                        currentKey = ['?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?']
                                        
                                        seenTangentBreak = False
                                        for keyIndex in range(len(childKeys)):
                                            eachKey = childKeys[keyIndex]
                                                
                                            #Check TangentBreak
                                            if eachKey.TangentBreak == True:
                                                seenTangentBreak = True
                                                
                                            #Create Active Key Data
                                            keyData = [str(eachKey.Time.GetSecondDouble()), str(eachKey.Value), str(eachKey.Tension), str(eachKey.Continuity), str(eachKey.Bias),
                                                        "{}{}{}{}{}".format(eachKey.Interpolation.numerator, eachKey.TangentBreak.numerator, eachKey.TangentClampMode.numerator, eachKey.TangentConstantMode.numerator, eachKey.TangentMode.numerator),
                                                        str(eachKey.LeftBezierTangent), str(eachKey.LeftDerivative), str(eachKey.LeftTangentWeight), str(eachKey.RightBezierTangent), str(eachKey.RightDerivative), str(eachKey.RightTangentWeight)]
                                            if self.expCamMode == True:
                                                keyData[0] = eachKey.Time.GetTimeString().replace("*","") #expCamMode requires frames, not seconds
                                            
                                            #Create Next Key Data
                                            if keyIndex != len(childKeys)-1:
                                                nextKey = childKeys[keyIndex+1]
                                                nextKeyData = [str(nextKey.Time.GetSecondDouble()), str(nextKey.Value), str(nextKey.Tension), str(nextKey.Continuity), str(nextKey.Bias),
                                                                "{}{}{}{}{}".format(nextKey.Interpolation.numerator, nextKey.TangentBreak.numerator, nextKey.TangentClampMode.numerator, nextKey.TangentConstantMode.numerator, nextKey.TangentMode.numerator),
                                                                str(nextKey.LeftBezierTangent), str(nextKey.LeftDerivative), str(nextKey.LeftTangentWeight), str(nextKey.RightBezierTangent), str(nextKey.RightDerivative), str(nextKey.RightTangentWeight)]
                                                if self.expCamMode == True:
                                                    nextKeyData[0] = nextKey.Time.GetTimeString().replace("*","") #expCamMode requires frames, not seconds
                                                
                                            #Check Previous Key for Update
                                            tagLineList = []
                                            for k in range(len(keyData)):
                                                #If TangentBreak is False - we skip the last 6 values
                                                if seenTangentBreak == False and k >= 6:
                                                    continue
                                                #Check the Key Differs - from previous or next key
                                                differsFromPrevious = keyData[k] != currentKey[k]
                                                if len(childKeys) == 1:
                                                    differsFromNext = False
                                                else:
                                                    differsFromNext = keyData[k] != nextKeyData[k]
                                                if differsFromPrevious == True or differsFromNext == True or self.expCamMode == True:
                                                    tagLineList.extend(['\t\t\t\t\t<', keyTagNames[k], '>', keyData[k], '</', keyTagNames[k], '>\n'])
                                            if tagLineList.count('>\n') > 1:
                                                textBlockAppend('\t\t\t\t<keyItem>\n')
                                                textBlockExtend(tagLineList)
                                                textBlockAppend('\t\t\t\t</keyItem>\n')
                                            
                                            #Update Current Key
                                            currentKey = keyData
                                        
                                        textBlockAppend('\t\t\t</layerItem>\n')
                                
                                if propObjExists:
                                    textBlockAppend('\t\t</propItem>\n')
                
                    #Close camItem Tag
                    textBlockAppend('\t</camItem>\n')
                
                #Close camDetails Tag
                textBlockAppend('</cameraDetails>')
                finalText = "".join(textBlock)
                
                #Select Base Layer
                lSystem.CurrentTake.SetCurrentLayer(0)
                
                #Write Data To Temp File
                tempFile = open(self.tempFilePath, "w")
                tempFile.write(finalText)
                tempFile.close()

def Run():
    startTime = time.time()
    _camKeyObj_ = CreateTempXml()
    print str(time.time() - startTime)
#Run()