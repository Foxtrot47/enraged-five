import os
import time
from pyfbsdk import *
from xml.etree import ElementTree as ET

import RS.Perforce as P4
import RS.Core.Camera.Lib as CameraLib
import RS.Core.Camera.camKeyHelper as CamKeyHelper
import RS.Core.Camera.CameraLocker as CameraLocker
import RS.Config

def loadXmlIntoCurrentFbx(keyXmlPath=None, syncFromP4=False):
    
    #Get Xml Name (or use current FBX name if none provided)
    if keyXmlPath == None:
        fbxPath = FBApplication().FBXFileName.replace("\\", "/")
        if len(fbxPath) <= 10:
            return "Invalid FBX."
        keyXmlPath = CamKeyHelper.detectXmlPathFromFbxPath(fbxPath)
        syncFromP4 = True
    
    if syncFromP4 == True:
        #Sync Lastest Xml From P4
        P4.Sync( keyXmlPath, force=True )
    
    #Convert Perforce Path to Local (and check that it synced)
    localXmlPath = CamKeyHelper.convertPerforcePathToLocal(keyXmlPath)
    if not os.path.exists(localXmlPath):
        return "Cam data xml not found, or couldn't be synced from P4."
    
    #Run LoadCamData Object
    _LoadCamsObj_ = LoadCamDataFromXml(localXmlPath)
    return "SUCCESS"

class LoadCamDataFromXml():
    def __init__(self, localXmlPath):
        self.localXmlPath = localXmlPath
        self.expCamMode = False
        
        self.main()
        
    def main(self):
        
        #Read Data From Xml
        tree = ET.parse(self.localXmlPath)
        rootNode = tree.getroot()
        switcherNode = list(rootNode)[1]
        
        #Detect ExpCamMode - Only if the export cam is the only key in the switcher
        if list(switcherNode)[0].find("Value").text == "ExportCamera":
            self.expCamMode = True
            
        #Unlock Cameras
        _camLockObj_ = CameraLocker.LockManager()
        if _camLockObj_.lockedCount != 0:
            _camLockObj_.setAllCamLocks(False)
        
        #Create Switcher Node and Cam Lib Tools
        mbSwitcherNode = FBCameraSwitcher().PropertyList.Find('Camera Index').GetAnimationNode()
        _camFunObj_ = CameraLib._camFunctions()
        _camUpdateObj_ = CameraLib._camUpdate()
        _camUpdateObj_.bSilent = True
        
        #Delete Old Switcher Keys
        FBSystem().CurrentTake.SetCurrentLayer(0)
        if len(mbSwitcherNode.FCurve.Keys) > 0:
            maxKey = len(mbSwitcherNode.FCurve.Keys) - 1
            mbSwitcherNode.FCurve.KeyDeleteByIndexRange(0,maxKey)
        
        #Delete Old Cams - unless we're in expCamMode
        if self.expCamMode == False:
            _camUpdateObj_.rs_RemoveNonSwitcherCameras()
        
        #Create Layer List
        mbLayerList = []
        mbLayerCount = FBSystem().CurrentTake.GetLayerCount()
        for i in range(mbLayerCount):
            mbLayerName = FBSystem().CurrentTake.GetLayer(i).Name
            mbLayerList.append(mbLayerName)
        
        #MAIN CAM LOOP
        for eachDetailsChild in list(rootNode):
            if eachDetailsChild.tag != "camItem":
                continue
            camItemNode = eachDetailsChild
            camName = camItemNode.find("camName").text
            #print camName
            
            #Create Each Camera
            _camFunObj_._CreateBaseCamera(specificName=camName)
            mbCamObj = FBFindModelByLabelName(camName)
            
            #PROPERTY LOOP
            for eachCamItemChild in list(camItemNode):
                if eachCamItemChild.tag != "propItem":
                    continue
                propItemNode = eachCamItemChild
                propName = propItemNode.find("propName").text
                #print propName
                
                #Get Each Property's FCurve
                if ("Translation (Lcl)" in propName) or ("Rotation (Lcl)" in propName):
                    axisName = propName[-1:]
                    if axisName == "X":
                        axisNumber = 0
                    elif axisName == "Y":
                        axisNumber = 1
                    else:
                        axisNumber = 2
                    propName = propName[:-1]
                    mbTRParentNode = mbCamObj.PropertyList.Find(propName).GetAnimationNode()
                    childNodes = mbTRParentNode.Nodes
                    currentCamProp = childNodes[axisNumber]
                else:
                    currentCamProp = mbCamObj.PropertyList.Find(propName).GetAnimationNode()
                
                #LAYER LOOP
                for layerNode in list(propItemNode)[1:]:
                    layerName = list(layerNode)[0].text
                    #print layerName
                    
                    #Create New Layer - If it doesn't already exist in the FBX
                    if layerName not in mbLayerList:
                        FBSystem().CurrentTake.CreateNewLayer()
                        newLayer = FBSystem().CurrentTake.GetLayer(len(mbLayerList))
                        newLayer.Name = layerName
                        mbLayerList.append(newLayer.Name)
                        
                    activeLayer = FBSystem().CurrentTake.GetLayerByName(layerName)
                    layerIndex = activeLayer.GetLayerIndex()
                    FBSystem().CurrentTake.SetCurrentLayer(layerIndex)
                    #activeLayer.LayerMode = FBLayerMode.kFBLayerModeAdditive#Override#Additive
                    #activeLayer.SelectLayer(True, True)
                
                    #KEY LOOP - Main
                    keyDict = {}
                    keyNumber = 0
                    seenTangentBreak = False
                    for keyItemNode in list(layerNode)[1:]:
                        
                        #Search Node for Keys
                        for eachKeyValue in keyItemNode:
                            tagName = eachKeyValue.tag
                            tagData = eachKeyValue.text
                            keyDict[tagName] = tagData
                            #print tagName + ': ' + tagData
                        
                        #Get Key Time - and check if time is a frame or a float, since we swapped
                        if "." in keyDict["Time"]:
                            keyTime = FBTime()
                            keyTime.SetSecondDouble(float(keyDict["Time"]))
                        else:
                            keyTime = FBTime(0,0,0,int(keyDict["Time"]))
                        
                        #Get Key Value
                        keyValue = float(keyDict["Value"])
                        
                        #Create Each Key
                        currentCamProp.KeyAdd(keyTime, keyValue)
                        
                        #Get MB Key Object
                        mbKey = currentCamProp.FCurve.Keys[keyNumber]
                        
                        #Set TCB Values
                        mbKey.Tension = float(keyDict["Tension"])
                        mbKey.Continuity = float(keyDict["Continuity"])
                        mbKey.Bias = float(keyDict["Bias"])
                        
                        #Set Tangent Values
                        mbKey.Interpolation = mbKey.Interpolation.values[int(keyDict["TangentFlag"][:1])]
                        mbKey.TangentBreak = bool(keyDict["TangentFlag"][1:2])
                        mbKey.TangentMode = mbKey.TangentMode.values[int(keyDict["TangentFlag"][-1:])]
                        mbKey.TangentConstantMode = mbKey.TangentConstantMode.values[int(keyDict["TangentFlag"][3:4])]
                        mbKey.TangentClampMode = mbKey.TangentClampMode.values[int(keyDict["TangentFlag"][2:3])]
                            
                        #Detect TangentBreak
                        if mbKey.TangentBreak == True:
                            seenTangentBreak = True
                        
                        #Advance KeyCounter
                        keyNumber = keyNumber + 1
                    
                    #KEY LOOP - LR Tangents Only
                    if seenTangentBreak == True:
                        keyDict = {}
                        keyNumber = 0
                        for keyItemNode in list(layerNode)[1:]:
                        
                            #Search Node for Keys
                            for eachKeyValue in keyItemNode:
                                tagName = eachKeyValue.tag
                                tagData = eachKeyValue.text
                                keyDict[tagName] = tagData
                                
                            #Get MB Key Object
                            mbKey = currentCamProp.FCurve.Keys[keyNumber]
                            
                            #Detect TangentBreak
                            if mbKey.TangentBreak == True:
                                
                                #Set LR TangentValues
                                mbKey.LeftBezierTangent = float(keyDict["LeftBezierTangent"])
                                mbKey.LeftDerivative = float(keyDict["LeftDerivative"])
                                mbKey.LeftTangentWeight = float(keyDict["LeftTangentWeight"])
                                mbKey.RightBezierTangent = float(keyDict["RightBezierTangent"])
                                mbKey.RightDerivative = float(keyDict["RightDerivative"])
                                mbKey.RightTangentWeight = float(keyDict["RightTangentWeight"])
                                
                            #Advance KeyCounter
                            keyNumber = keyNumber + 1
                        #break
                    #break
                #break
            #break
        
        #Create New Switcher - using cameraNameList for Indexes
        cameraNameList = []
        for camera in FBSystem().Scene.Cameras:
            cameraNameList.append(camera.Name)
        cameraNameList = cameraNameList[7:]
        
        #SWITCHER LOOP
        FBSystem().CurrentTake.SetCurrentLayer(0)
        for eachItem in list(switcherNode):
            timeNode = eachItem.find("Time")
            valueNode = eachItem.find("Value")
            keyTime = int(timeNode.text)
            keyValue = float(cameraNameList.index(valueNode.text)) + 1.0
            
            #Create Key
            mbSwitcherNode.KeyAdd(FBTime(0,0,0,keyTime), keyValue)
            
            #Set Tangent Options
            currentKey = mbSwitcherNode.FCurve.Keys[-1]
            currentKey.Interpolation = currentKey.Interpolation.values[0]
            currentKey.TangentMode = currentKey.TangentMode.values[0]
            #currentKey.TangentBreak = False
            #currentKey.TangentClampMode = currentKey.TangentClampMode.values[0]
            #currentKey.TangentConstantMode = currentKey.TangentConstantMode.values[0]
        
def Run():
    startTime = time.time()
    
    #Simple Message Box Interface - a placeholder until I write something proper in qt.
    messageText = "\tDo you want to restore camera data from the most recent xml backup?\t\n\n\tWARNING: This will delete the current camera data."
    restoreCamsChoice = FBMessageBox( "Cam Data Xml Loader", messageText, "Delete Cams & Restore", "Custom Load", "Cancel" )
    output = None
    if restoreCamsChoice == 1:
        output = loadXmlIntoCurrentFbx()
        
    elif restoreCamsChoice == 2:

        lFp = FBFilePopup()
        lFp.Caption = "FBFilePopup example: Select a file"
        lFp.Style = FBFilePopupStyle.kFBFilePopupOpen
        lFp.Filter = "*.xml"
        lFp.Path = "{0}\\etc\\CameraKeyData".format(RS.Config.Tool.Path.TechArt)
        lRes = lFp.Execute()
        if (lRes):
            output = loadXmlIntoCurrentFbx(lFp.FullFilename)            
           
    if output == "SUCCESS":
        successText = "The cam data was successfully restored."
        FBMessageBox( "Cam Data Loader", successText, "Okay")
    elif output != None:
        errorText = 'Error: ' + output
        FBMessageBox( "Cam Data Loader", errorText, "Okay")

            
    #print 'Process Time: ' + str(time.time() - startTime)
#Run()