from pyfbsdk import *

class LockManager():
    def __init__(self):
        self.cameraList = []
        self.switcherNameList = []
        self.lockedCamPresent = False
        
        self.createCamAndSwitcherLists()
        self.detectLockedCams()
        
        #self.lockSwitcherCams()
        #self.unlockSwitcherCams()
        #self.keyAllKeylessCams()
        
    def getUserLayer(self):
        """ Gets the user's selected layer. """
        lSystem = FBSystem()
        layerCount = lSystem.CurrentTake.GetLayerCount()
        selectedLayers = []
        for i in range(layerCount):
            layerSelected = lSystem.CurrentTake.GetLayer(i).Selected
            if layerSelected == True:
                selectedLayers.append(i)
        userLayer = max(selectedLayers)
        return userLayer
        
    def createCamAndSwitcherLists(self):
        """ Creates a list of MB camera objects
        and an ordered list of switcher cam names. """
        #GET LAYER THEN SELECT BASE LAYER
        userLayer = self.getUserLayer()
        lSystem = FBSystem()
        lSystem.CurrentTake.SetCurrentLayer(0)
        lSystem.CurrentTake.GetLayer(0).SelectLayer(True, True)
        
        # MB SETUP
        lScene = lSystem.Scene
        exportCamExists = False
        
        # CREATE CAMERA LIST
        for camera in lScene.Cameras:
            self.cameraList.append(camera)
            if camera.Name == "ExportCamera":
                exportCamExists = True
        self.cameraList = self.cameraList[6:]
        
        # GET CAM INDEXES FROM SWITCHER
        switcherCamIndexes = []
        lCameraSwitcher = FBCameraSwitcher()
        lSwitcherKeys = lCameraSwitcher.PropertyList.Find('Camera Index').GetAnimationNode().FCurve.Keys
        for eachKey in lSwitcherKeys:
            switcherCamIndexes.append(int(eachKey.Value))
        
        # CREATE SWITCHER NAME LIST
        for i in range(len(switcherCamIndexes)):
            currentCamName = self.cameraList[switcherCamIndexes[i]].Name
            self.switcherNameList.append(currentCamName)
            
        # ADD EXPORT CAM IF IT EXISTS
        if exportCamExists == True:
            self.switcherNameList.append("ExportCamera")
            
        #RESET TO USER SELECTED LAYER
        lSystem.CurrentTake.GetLayer(0).SelectLayer(False, False)
        lSystem.CurrentTake.SetCurrentLayer(userLayer)
        
    def detectLockedCams(self):
        """ Detects if any cameras in our
        switcherNameList have locked values.  """
        
        #CHECK CAMS FOR LOCK STATUS
        for camera in self.cameraList:
            if camera.Name in self.switcherNameList:
                if (camera.Translation.IsLocked() == True) or (camera.Rotation.IsLocked() == True) or (camera.PropertyList.Find('Field of View').IsLocked() == True):
                    self.lockedCamPresent = True
        
        #CHECK SWITCHER FOR LOCK STATUS
        lCameraSwitcher = FBCameraSwitcher()
        lCameraIndexProp = lCameraSwitcher.PropertyList.Find("Camera Index")
        if (lCameraIndexProp.IsLocked() == True):
            self.lockedCamPresent = True
    
    def lockSwitcherCams(self):
        """ Locks switcher and all cameras. """
        
        #LOCK ALL CAMERAS
        for camera in self.cameraList:
            if camera.Name in self.switcherNameList:
                camera.Translation.SetLocked( True )
                camera.Rotation.SetLocked( True )
                camera.PropertyList.Find('Field Of View').SetLocked( True )
                
        #LOCK SWITCHER
        lCameraSwitcher = FBCameraSwitcher()
        lCameraIndexProp = lCameraSwitcher.PropertyList.Find("Camera Index")
        lCameraIndexProp.SetLocked( True )
                
    def unlockSwitcherCams(self):
        """ Unlocks switcher and all cameras. """
        
        #UNLOCK ALL CAMERAS
        for camera in self.cameraList:
            if camera.Name in self.switcherNameList:
                camera.Translation.SetLocked( False )
                camera.Rotation.SetLocked( False )
                camera.PropertyList.Find('Field Of View').SetLocked( False )
        
        #UNLOCK SWITCHER
        lCameraSwitcher = FBCameraSwitcher()
        lCameraIndexProp = lCameraSwitcher.PropertyList.Find("Camera Index")
        lCameraIndexProp.SetLocked( False )
        
    
    def validateNode(self, camera, nodeType):
        """ A poorly written system used to detect cameras
        with empty animation nodes that will error later on. """
        if nodeType == 1:
            try:
                result = camera.Translation.GetAnimationNode().Nodes
            except AttributeError:
                return False
            return True    
        if nodeType == 2:
            try:
                result = camera.Rotation.GetAnimationNode().Nodes
            except AttributeError:
                return False
            return True   
        if nodeType == 3:
            try:
                result = camera.PropertyList.Find('Field Of View').GetAnimationNode()
            except AttributeError:
                return False
            try:
                result = result.KeyCount
            except AttributeError:
                return False
            return True
    
    def keyAllKeylessCams(self):
        """ Keys any 'keyless' switcher camera
        found with a default key at frame 0. """
        
        keyTime = FBTime()
        keyTime.SetFrame( 0 )
        
        for camera in self.cameraList:
            if camera.Name in self.switcherNameList:
                
                #VALIDATE & CHECK TRANSLATION
                foundTranslationNode = self.validateNode(camera, 1)
                if foundTranslationNode == False:
                    camera.Translation.KeyAt(keyTime)
                else:
                    for eachNode in camera.Translation.GetAnimationNode().Nodes:
                        if eachNode.KeyCount == 0:
                            camera.Translation.KeyAt(keyTime)
                            break
                
                #VALIDATE & CHECK ROTATION
                foundRotationNode = self.validateNode(camera, 2)
                if foundRotationNode == False:
                    camera.Rotation.KeyAt(keyTime)
                else:
                    for eachNode in camera.Rotation.GetAnimationNode().Nodes:
                        if eachNode.KeyCount == 0:
                            camera.Rotation.KeyAt(keyTime)
                            break
                
                #VALIDATE & CHECK FOV
                foundFOVnode = self.validateNode(camera, 3)
                if foundFOVnode == False:
                    camera.PropertyList.Find('Field of View').KeyAt(keyTime)
                else:
                    fNode = camera.PropertyList.Find('Field Of View').GetAnimationNode()
                    if fNode.KeyCount == 0:
                        camera.PropertyList.Find('Field of View').KeyAt(keyTime)
                        
def Run():
    """ If triggered via the RS menu, we lookup
    and unlock any switcher or export cams found. """
    _camLockObj_ = LockManager()
    if _camLockObj_.lockedCamPresent == True:
        _camLockObj_.unlockSwitcherCams()
    FBMessageBox( "CAMS UNLOCKED", "Cameras now unlocked.", "Okay")
    
#Run()