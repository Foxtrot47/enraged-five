expressionsUsePresetPath = (theProjectRoot +"art/peds/Resources/Expressions/")
	format ("expressionsUsePresetPath set to "+expressionsUsePresetPath +"\n")

markerPresetPath = (theProjectRoot + "art/peds/Resources/Markers/")
	format ("markerPresetPath set to "+markerPresetPath +"\n")
	
boneTagMappingXmlFile = rsMakeSafeSlashes(toolsRoot + "/etc/content/animconfig/bone_tags.xml")
	format ("boneTagMappingXmlFile  set to "+boneTagMappingXmlFile +"\n")

noneSkelNodePrefixes = #( --list of all prefixes for nodes like IK and PH nodes. Therse will get stored on the markers and then duplicated across to the skeleton.
	"IK_",
	"PH_",
	"OH_"
	)
	format ("noneSkelNodePrefixes set to "+(noneSkelNodePrefixes as string)+"\n")