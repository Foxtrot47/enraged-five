/***************************************************\
-- Title: 3DS Max Key Breakdown
-- Author: Jay Grenier, 2009
-- Version: 1
-- Adjusts selected key to favor previous or next key.
\***************************************************/

fn jgBreakItDownToolbox =
	(

	--If rollout(ui window) exists, delete it and create a new one.
	if (breakdownUI != undefined)
	then (destroydialog breakdownUI)

	--Create rollout/ui window
	rollout breakdownUI "jgBreakItDown" width:200 height:95
	(

		button prevBtn "Breakdown Previous" pos:[5, 5] width:190 height:40
		button nextBtn "Breakdown Next" pos:[5, 50] width:190 height:40

		fn breakDown breakdown =
		(

			--Find the # of selected controllers, give them an array
			cArray = selection as array	
			
			--Run function on each selected controller.
			for i = 1 to cArray.count do
			(	
				--Find all keys, give them an array
				try (
					xArray = cArray[i].controller[1][1][1].keys
					yArray = cArray[i].controller[1][2][1].keys
				)
				catch (
					print "The selected controller is not a Rockstar Joystick!"
					break
					exit
				)
					
				--Create empty arrays for selected keys
				arrayKY = #()
				arrayKX = #()
				
				--Variable for Selected Key
				selectedKeyIndex = ""
				selectedKeyAxis = ""
				selectedKeyValue = ""
				
				--Breakdown Var (passed in as argument)
				--breakdown = ""
				
				--Put selected X keys into array
				for j = 1 to xArray.count do
				(
					if xArray[j].selected then (
						
						append arrayKX xArray[j]
						selectedKeyIndex = j
						selectedKeyAxis = "X"
						selectedKeyValue = xArray[j].value
					
					)
				)

				--Put selected Y keys into array	
				for k = 1 to yArray.count do
				(
					if yArray[k].selected then (
						
						append arrayKY yArray[k]
						selectedKeyIndex = k
						selectedKeyAxis = "Y"	
						selectedKeyValue = yArray[k].value
					
					)
				)
				
				--Check if any keys are selected, alert if not.
				if (arrayKX[1] == undefined AND arrayKY[1] == undefined) then
				(
					print "ERROR: No key selected!"
					exit
				)
				
				--Check if more than 1 key is selected, alert if so.	
				if (arrayKX.count > 1 OR arrayKY.count > 1) then
				(
					print "ERROR: Select one key only and try again."
					exit
				)		
				
				--Check if selected key is first or last
				if (selectedKeyIndex == 0) then (
					
					print "Key is first key!  Cannot breakdown..."
					exit			
				)
				
				if (selectedKeyAxis == "X") then (
				
					--Check if key is last key on X curve
					if (selectedKeyIndex == xArray.count) then (
						
						print "Key is last key! Cannot breakdown..."
						exit
						
					)
					
					-- Get Value of Previous Key
					previousKeyValue = xArray[selectedKeyIndex-1].value
					
					-- Get Value of Next Key
					nextKeyValue = xArray[selectedKeyIndex+1].value
					
				
					--Compute Value of Breakdown Previous
					if (previousKeyValue < selectedKeyValue) then (
					
						newPrevValue = ((selectedKeyValue - previousKeyValue)/2)
									
					)
					if (previousKeyValue > selectedKeyValue) then (
					
						newPrevValue = ((previousKeyValue + selectedKeyValue)/2)
						
					)			
					
					--Compute Value of Breakdown Next
					if (nextKeyValue < selectedKeyValue) then (
					
						newNextValue = ((selectedKeyValue + nextKeyValue)/2)
									
					)
					if (nextKeyValue > selectedKeyValue) then (
					
						newNextValue = ((nextKeyValue + selectedKeyValue)/2)
						
					)				
					
				
					if (breakdown == "previous") then (
						xArray[selectedKeyIndex].value = newPrevValue
					)
					
					if (breakdown == "next") then (
						xArray[selectedKeyIndex].value = newNextValue				
					)
					
				)

				if (selectedKeyAxis == "Y") then (
				
					--Check if key is last key on X curve
					if (selectedKeyIndex == yArray.count) then (
						
						print "Key is last key! Cannot breakdown..."
						exit
						
					)
					
					-- Get Value of Previous Key
					previousKeyValue = yArray[selectedKeyIndex-1].value
					
					-- Get Value of Next Key
					nextKeyValue = yArray[selectedKeyIndex+1].value
					
				
					--Compute Value of Breakdown Previous
					if (previousKeyValue < selectedKeyValue) then (
					
						newPrevValue = ((selectedKeyValue + previousKeyValue)/2)
									
					)
					if (previousKeyValue > selectedKeyValue) then (
					
						newPrevValue = ((previousKeyValue + selectedKeyValue)/2)
						
					)			
					
					--Compute Value of Breakdown Next
					if (nextKeyValue < selectedKeyValue) then (
					
						newNextValue = ((selectedKeyValue + nextKeyValue)/2)
									
					)
					if (nextKeyValue > selectedKeyValue) then (
					
						newNextValue = ((nextKeyValue + selectedKeyValue)/2)
						
					)				
					
				
					if (breakdown == "previous") then (
						if (previousKeyValue != yArray[selectedKeyIndex].value) then (yArray[selectedKeyIndex].value = newPrevValue)
					)
					
					if (breakdown == "next") then (
						if (nextKeyValue != yArray[selectedKeyIndex].value) then (yArray[selectedKeyIndex].value = newNextValue)	
					)
					
				)

				--Eat Pie.
				
			)-- end if
			
		) -- end fn
		
		-- BUTTON CALLS
		on prevBtn pressed do
		(
			breakDown("previous");
		)
		
		on nextBtn pressed do
		(
			breakDown("next");
		)
		
	)createDialog breakdownUI
)
		
		
		
		
		
		
		
		
		
		