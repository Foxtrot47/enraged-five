fn spreadKeys = 
	(
		-- Title: Spread Keys
		-- Version: 1.0
		-- Author: Jay Grenier, 2008
		-- Tooltip:
		-- This script will take two keys right next to each
		-- other and move them each one frame apart to
		-- assist in removing pops.

		sel = getCurrentSelection()
				
		if(sel.count > 0) then
		(				
			--Find the # of selected controllers, give them an array
			cArray = selection as array	
		
			try
			(
		
				--Run function on each selected controller.
				for i = 1 to cArray.count do
				(	
					--Find all keys, give them an array
					xArray = cArray[i].controller[1][1][1].keys
					yArray = cArray[i].controller[1][2][1].keys
					
					--Create empty arrays for selected keys
					arrayKY = #()
					arrayKX = #()
					
					--Put selected X keys into array
					for i = 1 to xArray.count do
					(
						if xArray[i].selected then append arrayKX xArray[i]
					)
			
					--Put selected Y keys into array	
					for i = 1 to yArray.count do
					(
						if yArray[i].selected then append arrayKY yArray[i]
					)
					
					--Check if any keys are selected, alert if not.
					if (arrayKX[1] == undefined AND arrayKY[1] == undefined) then
					(
						print "No keys selected!"
						exit
					)
				
					--Check if more than 2 keys are selected, alert if so.	
					if (arrayKX.count > 2 OR arrayKY.count > 2) then
					(
						print "Please select only two keys."
						exit
					)
					
					--Find times of 2 selected keys, first create empty arrays
					timesArrayX = #()
					timesArrayY = #()	
			
					/* Note, this script assumes lower value will be indexed first */
			
					--Add two times to X array
					for i = 1 to arrayKX.count do
					(
						append timesArrayX ArrayKX[i].time
					)
					
					--Add two times to Y array			
					for i = 1 to arrayKY.count do
					(
						append timesArrayY ArrayKY[i].time
					)
					
					--Find and append X times
					if(arrayKX.count > 0) then (xLow = (timesArrayX[1] as integer / ticksperframe) - 1)
					if(arrayKX.count > 0) then (xHi = (timesArrayX[2] as integer / ticksperframe) + 1)
					if(arrayKY.count > 0) then (yLow = (timesArrayY[1] as integer / ticksperframe) - 1) 
					if(arrayKY.count > 0) then (yHi = (timesArrayY[2] as integer / ticksperframe) + 1)
					
					--Add new keys on X, if X is selected
					if(xLow != undefined) then
					(
						addNewKey cArray[i].controller[1][1][1] xLow
						addNewKey cArray[i].controller[1][1][1] xHi
					)
					
					--Add new keys on Y, if Y is selected
					if(yLow != undefined) then
					(
						addNewKey cArray[i].controller[1][2][1] yLow
						addNewKey cArray[i].controller[1][2][1] yHi
					)
			
					--Delete old keys on X if needed
					deleteKeys cArray[i].controller[1][1][1] #selection
					deleteKeys cArray[i].controller[1][2][1] #selection	
			
				)
			)
			catch (print "Please select two keys and try again.") -- error message
		)
		else
		(
			print "Nothing selected.."
		)
	)