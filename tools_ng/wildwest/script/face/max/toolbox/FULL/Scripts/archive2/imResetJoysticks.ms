fn resetJoysticks = (
		
	try	(
		undo on
		(
			sel = getCurrentSelection()
			
			if(sel.count > 0) then
			(
				for i = 1 to sel.count do
				(
					--print sel[i].name
					if(sel[i].name != "con_nose_c" AND sel[i].name != "con_mouth_c") then (
						in coordsys parent sel[i].pos = [0,0,0]
					)
				)
			)
			if(sel.count == 0) then
			(
				print "Nothing selected.."
			)
		)
	)
	catch()
)