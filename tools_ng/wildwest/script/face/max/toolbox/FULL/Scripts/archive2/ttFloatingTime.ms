fn floatingTime =
	(
	
		if (theTimeRollout != undefined)
		then (destroydialog theTimeRollout)
		
		rollout theTimeRollout "Floating Slider Time" width:258 height:25
		(
			spinner theStartFrame "Start: " pos:[22,4] width:60 range:[-100,50000,1] type:#integer
			edittext theTimeInput "Go To:" pos:[89,4] width:80
			spinner theEndFrame "End: " pos:[193,4] width:60 range:[-100,50000,1] type:#integer
			
			
			
			on theTimeRollout open do
			(
				theTimeInput.text = ((sliderTime as integer) / ticksperframe) as string
				theStartFrame.value = (animationRange.start as integer / ticksperframe)
				theEndFrame.value = (animationRange.end as integer / ticksperframe)
			)
			on theTimeInput entered x do
			(
				slidertime = x as integer
			)
			on theStartFrame changed x do
			(
				if (x < animationrange.end)
				then
				(
				animationRange = interval x animationrange.end
				)
				else 
				(
					theStartFrame.value = (animationRange.end as integer / ticksperframe) - 1
					animationRange = interval theStartFrame.value animationrange.end
				)
			)
			on theEndFrame changed x do
			(
				if (x > animationrange.start)
				then
				(
				animationRange = interval animationrange.start x
				)
				else 
				(
					theEndFrame.value = (animationRange.start as integer / ticksperframe) + 1
					animationRange = interval animationrange.start theEndFrame.value
				)
			)
			
		)createDialog theTimeRollout style:#(#style_toolwindow, #style_sysmenu)
	
	)