fn setToZero = 
	(
	
		-- Title: Set To Zero
		-- Version: 1.0 
		-- Author: Jay Grenier, 2008 
		-- Tooltip: 
		-- Sets a selected keyframe(s) to 0.0

		sel = getCurrentSelection()
				
		if(sel.count > 0) then
		(
			--Find the # of selected controllers, give them an array
			cArray = selection as array	
		
			try
			(
		
				--Run function on each selected controller.
				for i = 1 to cArray.count do
				(	
					--Find all keys, give them an array
					xArray = cArray[i].controller[1][1][1].keys
					yArray = cArray[i].controller[1][2][1].keys
					
					--Create empty arrays for selected keys
					arrayKY = #()
					arrayKX = #()
					
					--Put selected X keys into array
					for i = 1 to xArray.count do
					(
						if xArray[i].selected then append arrayKX xArray[i]
					)
			
					--Put selected Y keys into array	
					for i = 1 to yArray.count do
					(
						if yArray[i].selected then append arrayKY yArray[i]
					)
					
					--Check if any keys are selected, alert if not.
					if (arrayKX[1] == undefined AND arrayKY[1] == undefined) then
					(
						print "No keys selected!"
						exit
					)
				  
					--Set value of all X keys to zero
					for i = 1 to arrayKX.count do
					(
						arrayKX.value = 0
					)

					--Set value of all Y keys to zero
					for i = 1 to arrayKY.count do
					(
						arrayKY.value = 0
					)
			
				)
			)
			catch (print "Error, please try again.") -- error messag
		)
		else
		(
			print "Nothing selected.."
		)
	)
