fn randomizeValues =
   (
		-- Title: Randomize Values
		-- Version: 1.0
		-- Author: Jay Grenier, 2008
		-- Tooltip:
		-- Take a group of selected keys and randomize their values
		
		/* DETECT AND GET SELECTED KEYS ON X & Y POS **************/
		
		try
		(
			
			if ($.controller[1][1][1] == undefined OR $.controller[1][2][1] == undefined) then
			(
				arrayX = $.controller[1][1].keys
				arrayY = $.controller[1][2].keys
			)
			else
			(
				arrayX = $.controller[1][1][1].keys
				arrayY = $.controller[1][2][1].keys		
			)
			
			
			arrayKY = #() -- arrayKY holds selected Y keys
			arrayKX = #() -- arrayKX holds selected X keys

			
			for i = 1 to arrayY.count do
			(
				t = arrayY[i].time
				v = arrayY[i].value
				if arrayY[i].selected then append arrayKY arrayY[i]
			)

			for i = 1 to arrayX.count do
			(
				if arrayX[i].selected then append arrayKX arrayX[i]
			)

			if(arrayKY.count == 0 AND arrayKX.count == 0) then
			(
				print "No keys selected.."
			)
			else
			(	
				/* FIND HIGH AND LOW  X POSITION ****************************/

				hi = 0
				low = 0
				
				for i = 1 to arrayKX.count do
				(
					
					--Check if value is higher or lower, if so, overwrite
					if (arrayKX[i].value > hi) then (hi = arrayKX[i].value)
					if (arrayKX[i].value < low) then (low = arrayKX[i].value)
				
				)
				
				/* APPEND RANDOM VALUES TO KEYS **************************/
				
				for i = 1 to arrayKX.count do
				(
					--Get average position of high and low values
					a = (hi + low)/2
						
					--Subtract percentage from high, low, and average
					p = 0.0275
					hi2 = hi - (hi*p)
					low2 = low + (low*p)
					rP = random -p p
					a2 = a + rP
					
					--Get random values for high and low
					rH = random a hi2
					rL = random low2 a
					
					if (arrayKX[i].value != a AND arrayKX[i].value != hi AND arrayKX[i].value > a) then (arrayKX[i].value = rH)
					if (arrayKX[i].value != a AND arrayKX[i].value != low AND arrayKX[i].value < a) then (arrayKX[i].value = rL)

				)
				
			
				/* FIND HIGH AND LOW Y POSITION ****************************/
				
				hi = 0
				low = 0
				
				for i = 1 to arrayKY.count do
				(
					
					--Check if value is higher or lower, if so, overwrite
					if (arrayKY[i].value > hi) then (hi = arrayKY[i].value)
					if (arrayKY[i].value < low) then (low = arrayKY[i].value)
				
				)
				
				/* APPEND RANDOM VALUES TO KEYS **************************/
				
				for i = 1 to arrayKY.count do
				(
					--Get average position of high and low values
					a = (hi + low)/2
						
					--Subtract percentage from high, low
					p = 0.0275
					hi2 = hi - (hi*p)
					low2 = low + (low*p)
					rP = random -p p
					a2 = a + rP
					
					--Get random values for high and low
					rH = random a hi2
					rL = random low2 a
					
					if (arrayKY[i].value != a AND arrayKY[i].value != hi AND arrayKY[i].value > a) then (arrayKY[i].value = rH)
					if (arrayKY[i].value != a AND arrayKY[i].value != low AND arrayKY[i].value < a) then (arrayKY[i].value = rL)
				)
			)
		)
		catch(print "Nothing selected..")
		
	)
