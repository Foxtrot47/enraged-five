function gta5_makePreview = (
	
	viewportbuttonMgr.EnableButtons = false

	tempHiddenCheck = false
	-- If the face circles are showing, hide them.
	if $upperLip_R_CTRL.ishidden == false do (
		tempHiddenCheck = true
		gta5_showHideFaceCirc()
	)
	
	-- Hide the Main GUI
	for i in $faceControls_Off...* do (
		i.ishidden = true
	)
	
	max preview
	
	
	-- Show the Main GUI
	for i in $faceControls_Off...* do (
		i.ishidden = false
	)
	viewportbuttonMgr.EnableButtons = true
	--If the face circles were hid earlier, reveal them
	if tempHiddenCheck do (
		gta5_showHideFaceCirc()
	)	
)

function gta5_makePreview_side = (
	
	viewportbuttonMgr.EnableButtons = false
	
	tempParent = undefined
	select $head_00???
	selectmore $teef_00???
	
	
	if selection.count == 2 do (
		if selection[2].parent != undefined then tempParent = selection[2].parent
		if selection[1].parent != undefined then tempParent = selection[1].parent
		
		if tempParent == undefined then (
			delete $headteef_geo*
			tempParent = dummy pos:[0,0,0]
			tempParent.name = "headteef_geo"
		)
		
		for i in selection do i.parent = tempParent
		newParent_TEMP = reference selection[1].parent
		newParent_TEMP.name = "instanced_geo"
		instancedGeo1_TEMP = reference selection[1]
		instancedGeo2_TEMP = reference selection[2]
		instancedGeo1_TEMP.parent = newParent_TEMP
		instancedGeo2_TEMP.parent = newParent_TEMP
		select newParent_TEMP
		move $ [0.183374,-0.04,0]
		currentMatrix = $.transform
		preRotate currentMatrix (eulertoquat (eulerAngles 0 0 -45))
		$.transform = currentMatrix
		deletekeys newParent_TEMP
	)
	
	tempHiddenCheck = false
	-- If the face circles are showing, hide them.
	if $upperLip_R_CTRL.ishidden == false do (
		tempHiddenCheck = true
		gta5_showHideFaceCirc()
	)
		
	-- Setup Display settings
	hideByCategory.geometry = false
	hideByCategory.shapes = true
	hideByCategory.lights = true
	hideByCategory.helpers = true
	hideByCategory.spacewarps = true
	hideByCategory.particles = true
	hideByCategory.bones = true
	hideByCategory.shapes = true
	hideByCategory.cameras = true
	
	max preview
	
	viewportbuttonMgr.EnableButtons = true
	delete newParent_TEMP
	delete instancedGeo1_TEMP
	delete instancedGeo2_TEMP
	delete $instanced_geo*
	delete $head_???_??*
	delete $teef_???_??*
	
	--If the face circles were hid earlier, reveal them
	if tempHiddenCheck do (
		gta5_showHideFaceCirc()
	)
	
	-- Setup Display settings
	hideByCategory.geometry = false
	hideByCategory.shapes = true
	hideByCategory.lights = true
	hideByCategory.helpers = true
	hideByCategory.spacewarps = true
	hideByCategory.particles = true
	hideByCategory.bones = true
	hideByCategory.shapes = true
	hideByCategory.cameras = true
	
)