
	
	global mainPoseArray = #() -- store all poses, objs, and attributes
	global diff = #() -- pose difference for blend
	global tempCurrentPose = #()  -- initial pose for blend
	global nextPosePosition = (mainPoseArray.count) + 1 -- how many pose we have currently
	global currentSelPose = 0 -- current selection in pose list
	global totalPoses = "Pose Count: " + (mainPoseArray.count as string)
	global needSave = false -- dirty scene flag
	global savePath = ""
	global loadPath = ""

	struct pose
	(
		name,
		obj = #()
	)

	struct obj 
	(
		name, 
		pos,
		rot = #(),
		scale,
		customAtt = #()
	)

	struct customAttribute 
	(
		name,
		location,
		value
	)


	-- get obj and their attributes
	function createPoseFn poseListBox poseName =
	(
		-- create new pose with name
		mainPoseArray[nextPosePosition] = pose poseName

		-- for each obj, get pos/rot/scale attributes
		n = 1 -- object iteration
		for i in selection do
		(
			currentObj = obj i.name
			currentObj.pos = in coordsys parent i.pos  
			currentObj.rot = in coordsys parent ((i.rotation) as eulerAngles)
			currentObj.scale = in coordsys parent i.scale

			mainPoseArray[nextPosePosition].obj[n] = currentObj

			n += 1 -- next obj
		)

		poseListBox.items = append poseListBox.items mainPoseArray[nextPosePosition].name -- update the pose list
		poseListBox.selection = currentSelPose = nextPosePosition -- select the last pose in pose list
		poseListBox.text = totalPoses = "Pose Count: " + (mainPoseArray.count as string) -- update pose count
		nextPosePosition += 1 -- update the mainPoseArray cursor so you can add next pose
		needSave = true
	)

	function recapturePoseFn poseListBox =
	(
		mainPoseArray[poseListBox.selection].obj = #()

		-- for each obj, get pos/rot/scale attributes
		n = 1 -- object iteration
		for i in selection do
		(
			currentObj = obj i.name
			currentObj.pos = in coordsys parent i.pos  
			currentObj.rot = in coordsys parent ((i.rotation) as eulerAngles)
			currentObj.scale = in coordsys parent i.scale

			mainPoseArray[poseListBox.selection].obj[n] = currentObj

			n += 1 -- next obj
		)

		needSave = true
	)

	function deletePoseFn poseListBox = 
	(	
		deleteItem mainPoseArray currentSelPose -- delete main array based on currently select pose
		poseListBox.items = deleteItem poseListBox.items currentSelPose -- update pose list
		poseListBox.text = totalPoses = "Pose Count: " + (mainPoseArray.count as string)
		-- if there are no more pose, then don't select anything, else select first pose
		if (poseListBox.items.count != 0)
		then (poseListBox.selection = currentSelPose = 1)
		else (poseListBox.selection = currentSelPose = 0)
		nextPosePosition -= 1 -- set the mainPoseArray to the right place on next create
		needSave = true
	)

	function renamePoseFn poseListBox poseName=
	(
		mainPoseArray[currentSelPose].name = poseName -- rename pose in main array
		-- update pose list
		poseListBox.items[currentSelPose] = poseName
		poseListBox.items = poseListBox.items
		needSave = true
	)

	-- create snapshot of current state and find the difference between select pose, so you can blend
	function blendPoseFn1 poseListBox  = 
	(
		-- create duplicate of obj list for current pose
		tempCurrentPose = deepcopy mainPoseArray[poseListBox.selection].obj

		-- for each obj, get current state, and find the difference from currently selected pose
		for i = 1 to tempCurrentPose.count do
		(
			diff[i] = obj "diff" rot: (eulerAngles 0 0 0)
			objectName = "$" + tempCurrentPose[i].name 
			tempCurrentPose[i].pos = execute ("in coordsys parent " + objectName + ".pos")
			tempCurrentPose[i].rot = execute ("in coordsys parent " + objectName + ".rotation as eulerAngles")
			tempCurrentPose[i].scale = execute ("in coordsys parent " + objectName + ".scale")
			diff[i].pos = (mainPoseArray[poseListBox.selection].obj[i].pos - tempCurrentPose[i].pos)
			-- have to do it separately for XYZ since (eulerAngles X Y Z) - (eulerAngles X Y Z) don't work! STUPID
			diff[i].rot.x = (mainPoseArray[poseListBox.selection].obj[i].rot.x - tempCurrentPose[i].rot.x)
			diff[i].rot.y = (mainPoseArray[poseListBox.selection].obj[i].rot.y - tempCurrentPose[i].rot.y)
			diff[i].rot.z = (mainPoseArray[poseListBox.selection].obj[i].rot.z - tempCurrentPose[i].rot.z)
			diff[i].scale = (mainPoseArray[poseListBox.selection].obj[i].scale - tempCurrentPose[i].scale)
		)
	)

	-- pose blending
	function blendPoseFn2 percentValue =
	(
		-- for every obj, slide to the right place based on slider
		for i = 1 to tempCurrentPose.count do
		(
			objectName = "$" + tempCurrentPose[i].name 
			newPos = tempCurrentPose[i].pos + ((percentValue/100.0) * diff[i].pos)
			newRot = (eulerAngles 0 0 0)
			newRot.x = tempCurrentPose[i].rot.x + ((percentValue/100.0) * diff[i].rot.x)
			newRot.y = tempCurrentPose[i].rot.y + ((percentValue/100.0) * diff[i].rot.y)
			newRot.z = tempCurrentPose[i].rot.z + ((percentValue/100.0) * diff[i].rot.z)
			newScale = tempCurrentPose[i].scale + ((percentValue/100.0) * diff[i].scale)
			theObject = execute objectName
			--in coordsys parent theObject.scale = newScale
			in coordsys parent theObject.rotation = eulertoquat(newRot) -- rotation has to be converted back to quat in order to work
			in coordsys parent theObject.pos = newPos
		)
	)

	function selPoseCtrlsFn poseListBox = 
	(
		selectionArray = #()
		for i = 1 to mainPoseArray[poseListBox.selection].obj.count do
		(
			selectionArray[i] = execute ("$" + mainPoseArray[poseListBox.selection].obj[i].name)
		)
		select selectionArray
	)

	function saveFileFn savePath =
	(
		ofstream = createFile savePath -- create file
		if (ofstream != undefined) -- if file can be created/opened
		then 
		(
			print mainPoseArray.count to: ofstream -- total poses
			for currentPose = 1 to mainPoseArray.count do
			(
				print mainPoseArray[currentPose].name to: ofstream --for each pose get the name
				--for each obj, save attributes
				for currentPoseObj = 1 to mainPoseArray[currentPose].obj.count do
				(
					print mainPoseArray[currentPose].obj[currentPoseObj].name to: ofstream
					print mainPoseArray[currentPose].obj[currentPoseObj].pos to: ofstream
					print mainPoseArray[currentPose].obj[currentPoseObj].rot to: ofstream
					print mainPoseArray[currentPose].obj[currentPoseObj].scale to: ofstream
					print "~" to: ofstream -- end of each obj
				)
				print "~~~" to: ofstream -- end of each pose
			)
			needSave = false
		)
		else (messageBox "Can't Open/Create File for Saving!")

		close ofstream
	)

	-- fail safe save in case ppl fuck up the file editable file
	function saveFileFSFn savePath = 
	(
		fileName = getFileNameFile savePath -- grab filename from path
		if ((getDirectories "c://P2PBAK").count == 0) -- if the folder don't exist then create
		then 
		(
			makeDir "c://P2PBAK" 
		)
		savePath = "c://P2PBAK//" + fileName + " DO_NOT_EDIT.bak" -- the save path

		if ((getfiles savePath).count == 0) -- if file exist then open, else create
		then 
		(
			ofstream = createFile savePath -- create file
			if (ofstream != undefined) -- if file can be created/opened
			then 
			(
				print mainPoseArray to: ofstream
				needSave = false
			)
			else (messageBox "Can't Open/Create File for Saving!")
			close ofstream
		)
		else 
		(
			ofstream = openFile savePath mode: "w" -- open file
			if (ofstream != undefined) -- if file can be created/opened
			then 
			(
				print mainPoseArray to: ofstream
				needSave = false
			)
			else (messageBox "Can't Open/Create File for Saving!")
			close ofstream
		)
	)

	function loadFileFn loadPath poseListBox =
	(
		-- open filestream
		ifstream = openFile loadPath mode: "r"
		if (ifstream != undefined)
		then
		(
			mainPoseArray = #()
			numOfPose = readValue ifstream -- read # of pose from file
			-- for every pose, initialize objects and attributes
			for i = 1 to numOfPose do
			(
				mainPoseArray[i] = pose (readValue ifstream) -- first pose name
				buffer = ""
				n = 1  -- object number in pose
				do 
				(
					do
					(
						mainPoseArray[i].obj[n] = obj (readValue ifstream)
						mainPoseArray[i].obj[n].pos = readValue ifstream
						mainPoseArray[i].obj[n].rot = readValue ifstream
						mainPoseArray[i].obj[n].scale = readValue ifstream
						buffer = readValue ifstream -- is it the end of obj? or custom attributes coming?
					) while (buffer != "~") -- exit at end of obj attributes
					n = n + 1 -- next obj attributes
					previousFilePos = filePos ifstream -- store file position here
					buffer = readValue ifstream -- test, is it the end of pose?
					if (buffer != "~~~") -- if not then back up and continue with next obj
					then (seek ifstream previousFilePos) 
				) while (buffer !="~~~") -- if it exits this loop, then the pose ends there, grab the next pose
			)	

			nextPosePosition = (mainPoseArray.count) + 1 -- set the mainPose array cursor
			-- gather all the imported pose names
			importedPoseNames = #() 
			for m = 1 to mainPoseArray.count do
			(
				importedPoseNames[m] = mainPoseArray[m].name
			)

			-- update pose list and select first pose
			poseListBox.items = importedPoseNames
			poseListBox.text = totalPoses = "Pose Count: " + (mainPoseArray.count as string)
			poseListBox.selection = currentSelPose = 1
		)

		close ifstream
	)

	function loadFileFSFn loadPath poseListBox = 
	(
		ifstream = openFile loadPath mode: "r"
		if (ifstream != undefined) -- if filestream open successfully
		then
		(
			n = 1 -- first pose
			do
			(
				mainPoseArray[n] = readExpr ifstream
				n = n + 1
			)while (eof ifstream != true)

			nextPosePosition = (mainPoseArray.count) + 1 -- set mainArray Cursor for next pose

			-- gather all imported pose name and update the pose list
			importedPoseNames = #()
			for m = 1 to mainPoseArray.count do
			(
				importedPoseNames[m] = mainPoseArray[m].name
			)

			poseListBox.items = importedPoseNames
			poseListBox.text = totalPoses = "Pose Count: " + (mainPoseArray.count as string)
			poseListBox.selection = currentSelPose = 1 -- select first pose
		)

		close ifstream
	)
	
fn pose2pose_init = (
	-- ========== UI START ===================
	if (P2P != undefined )
	then DestroyDialog P2P
	(
		rollout P2P "Pose2Pose" 
		(  
			group "Pose Sets"
			(
				button createPose "Create Pose" width: 140 height: 50 tooltip: "Create a new Pose"
				button recapturePose "Re-Capture Pose" width: 140 tooltip: "Select your ctrl objects and hit 'Re-Capture Pose' to capture the pose, keeping the same name."
				button renamePose "Rename Pose" width: 140
				button selPoseCtrls "Select Pose Controls" width: 140 tooltip: "Select all the the ctrl objects that contributed the pose." 
				--dotnetcontrol f1 "System.Windows.Forms.ListView" text: "Push Me" height: 200 checkboxes:true gridlines: true
				listbox poseList "Pose Count: 0" items: #() height: 30
				button deletePose "Delete Pose" width: 140
				slider poseSlider "Percent: 0" type: #integer ticks: 10
			)
			group "File I/O"
			(
				button exportFile "Save Pose File" width: 140
				button importFile "Load Pose File" width: 140
			)
			group "About"
			(
				label lbl1 "P2P Version 1.0 RC2"
				label lbl2 "Tom Tran 2008"
			)

			--CREATE POSE BUTTON--
			on createPose pressed do
			(
				if (selection.count != 0)  -- if theres a pose in the list
				then
				(
					-- create diaglog to get pose name
					rollout pNameDiag "New Pose Name"
					(
						edittext pName "New Pose Name: "

						on pName entered poseName do
						(
							if (poseName != "") -- if text entered, create new pose
							then
							(
								createPoseFn poseList poseName
								DestroyDialog pNameDiag
							)
						)
					)
					createDialog pNameDiag width: 200
				)
				else 
				(
					messageBox "No Objects Selected!"
				)
			)

			on recapturePose pressed do
			(
				if (mainPoseArray.count != 0 AND currentSelPose != 0)
				then (recapturePoseFn poseList)
			)

			--DELETE POSE BUTTON--
			on deletePose pressed do
			(
				if (currentSelPose != 0 AND mainPoseArray.count != 0)
				then
				(
					answer = queryBox "Are you sure you want to delete the pose?  This operation is NOT UNDOABLE."
					if (answer == true)
					then (deletePoseFn poseList)
				)
			)

			--RENAME POSE BUTTON--
			on renamePose pressed do
			(
				if (currentSelPose != 0 AND mainPoseArray.count != 0)
				then
				(
					-- create diaglog to get pose name
					rollout pNameDiag "New Pose Name"
					(
						edittext pName "New Pose Name: "

						on pName entered poseName do
						(
							if (poseName != "") -- if text entered, create new pose
							then
							(
								renamePoseFn poseList poseName
								DestroyDialog pNameDiag
							)
						)
					)
					createDialog pNameDiag width: 200
				)
			)

			--Select Pose Ctrls
			on selPoseCtrls pressed do
			(
				if (mainPoseArray.count != 0 AND currentSelPose != 0)
				then (selPoseCtrlsFn poseList)
			)

			--update global selected pose variable 
			on poseList selected x do 
			(
				currentSelPose = x
			)

			--go straight to pose on double click
			on poseList doubleClicked x do
			(
				blendPoseFn1 poseList
				blendPoseFn2 100
			)

			--create current snapshot
			on poseSlider buttondown do 
			(
				if (mainPoseArray.count != 0 AND currentSelPose != 0)
				then (blendPoseFn1 poseList)
			)

			--do pose blend 
			on poseSlider changed val do 
			(
				if (mainPoseArray.count != 0 AND currentSelPose != 0)
				then
				(
					poseSlider.text = "Percent: " + (val as string)
					blendPoseFn2 val
				)
			)

			--reset slider on mouse up
			on poseSlider buttonup do 
			(
				poseSlider.value = 0
				poseSlider.text = "Percent: 0"
			)

			on exportFile pressed do
			(
				if (needSave == true AND mainPoseArray.count != 0) -- if there IS something to save
				then
				(
					-- get save file path
						savePath = getSaveFileName caption: "Save File As" types: "Pose2Pose(*.p2p)|*.p2p|Text(*.txt)|*.txt"
						if (savePath != undefined)
						then
						( 
								saveFileFn savePath
								saveFileFSFn savePath -- secret save
						)	
				)
			)

			on importFile pressed do
			(
				-- before import, need to save? -dirty scene?
				if (needSave == true AND mainPoseArray.count != 0)
				then
				(
					answer = yesNoCancelBox "The Pose List Has Changed. Save?" caption: "Pose 2 Pose"
					-- YES, then save
					if (answer == #yes) 
					then 
					(
						-- get save file path
						savePath = getSaveFileName caption: "Save File As" types: "Pose2Pose(*.p2p)|*.p2p|Text(*.txt)|*.txt|Backup(*.bak)|*.bak"
						if (savePath != undefined)
						then
						( 
							saveFileFn savePath
							saveFileFSFn savePath -- secrete save
						)	
					)
					else
					(
						-- NO, then just load file
						if (answer == #no)
						then
						(
							-- get load file path
							loadPath = getOpenFileName caption: "Open File" types: "Pose2Pose(*.p2p)|*.p2p|Text(*.txt)|*.txt|Backup(*.bak)|*.bak"
							if (loadPath != undefined)
							then
							( 
								if ((getFileNameType loadPath) != ".bak")
								then (loadFileFn loadPath poseList)
								else (loadFileFSFn loadPath poseList)
							)
						)
					)
				)
				else -- nothing needs to be saved, so just load
				(
					-- get load file path
					loadPath = getOpenFileName caption: "Open File" types: "Pose2Pose(*.p2p)|*.p2p|Text(*.txt)|*.txt|Backup(*.bak)|*.bak"
					if (loadPath != undefined)
					then
					( 
						if ((getFileNameType loadPath) != ".bak")
						then (loadFileFn loadPath poseList)
						else (loadFileFSFn loadPath poseList)
					)
				)
			)

			on P2P close do 
			(
				if (needSave == true AND mainPoseArray.count != 0)
				then
				(
					answer = queryBox "The Pose List Has Changed. Save?" caption: "Pose 2 Pose"
					if (answer == true) 
					then 
					(
						-- get save file path
						savePath = getSaveFileName caption: "Save File As" types: "Pose2Pose(*.p2p)|*.p2p|Text(*.txt)|*.txt"
						if (savePath != undefined)
						then
						( 
							saveFileFn savePath
							--saveFileFSFn savePath
						)	
					)
				)
			)
			/*on P2P open do 
			(
				f1.gridLines = true
				f1.View = (dotnetclass "System.Windows.Forms.View").Details
				f1.Columns.add "Pose" 95
				f1.Columns.add "T/F" 37
			)
			*/
		)
		createDialog P2P
	)

)