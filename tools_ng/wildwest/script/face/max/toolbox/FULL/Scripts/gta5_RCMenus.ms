/* scene setup rollout help */
rcMenu rcSS
(
		menuItem ssHelp "Sets up Image Plane, Wav File, Frame Range when you start a scene."
)
rcMenu rcCE
(
		menuItem ceHelp "Opens curve editor and adjusts filters."
)
rcMenu rcFT
(
		menuItem ftHelp "Floating Time is a moveable GUI for adjusting frame range and current frame."
)
rcMenu rcTR
(
		menuItem ftHelp "Time Ranger is a moveable GUI for saving specific sets of frame ranges."
)


/* IMA rollout help */
rcMenu rcIMA1
(
		menuItem ima1Help "Export an IMA for A and B characters. (Rockstar)"
)
rcMenu rcIMA2
(
		menuItem ima2Help "Export an IMA for C and Bones characters. (Rockstar) (Must select bones)"
)
rcMenu rcIMA3
(
		menuItem ima3Help "Export an IMA for Old (original GTA4) characters. (Rockstar)"
)


/* im tools rollout help */
rcMenu rcRE
(
		menuItem reHelp "Opens the IM Retargeter if you have it installed."
)
rcMenu rcCS
(
		menuItem csHelp "Opens the IM Character XML Setup if you have it installed."
)
rcMenu rcRT
(
		menuItem rtHelp "Opens the IM Rigging Tools if you have them installed."
)

/* anim tools rollout help */
rcMenu rcP2P
(
		menuItem rcP2P "Brings up the Pose2Pose pose library."
)
rcMenu rcRC
(
		menuItem rcHelp "Resets *selected* controllers to their default position. (Viewport)"
)
rcMenu rcSK
(
		menuItem skHelp "Smooths out a group of *selected* keyframes. (Curve Editor)"
)
rcMenu rcCA
(
		menuItem caHelp "Converts two or more *selected* keys into one at their average. (Curve Editor)"
)
rcMenu rcSTZ
(
		menuItem stzHelp "Sets *selected* keys to 0. (Curve Editor)"
)
rcMenu rcRK
(
		menuItem rkHelp "Randomizes a group of *selected* keys to produce variation. (Curve Editor)"
)
rcMenu rcSKS
(
		menuItem sksHelp "Moves two *selected* keys a frame apart in each direction. (Curve Editor)"
)
rcMenu rcBID
(
		menuItem bidHelp "Break It Down Rockstar will allow you to select some keys and set them to favor the previous or next key."
)

/* Prepare Settings Rollout Help */


rcMenu rcPS
(
		menuItem psHelp "Arrange 3DS Max into standard viewport layout.  If objects appear black, press ALT+W twice."
)
rcMenu rcTF
(
		menuItem tfHelp "Open trackview Curve Editor, then click to automatically set up standard animation filters"
)
rcMenu rcFMP
(
		menuItem fmpHelp "Click to fix movieplane after Faceware creates it"
)
rcMenu rcDCR
(
		menuItem dcrHelp "Delete Rotation Animation Keys from All Controls"
)
rcMenu rcCL
(
		menuItem clHelp "Deletes limit keys, and locks limits to prevent them from being keyed"
)
rcMenu rcES
(
		menuItem csHelp "Use when done with animation"
)
rcMenu rcMPV
(
		menuItem mpvHelp "Prepares viewports for playblast.  '2 Heads' creates a temporary side head that gets deleted afterward."
)
rcMenu rcFBXEX
(
		menuItem fbxexHelp "Will select and export .fbx facial file for use with motionbuilder"
)
rcMenu rcBMT
(
		menuItem rcBMTHelp "Blink and Tongue Tools"
)
rcMenu rcOPM
(
		menuItem rcOPMHelp "Takes a fresh rig file from rigging team, and runs setup scripts for OPENME creation"
)
rcMenu rcAG
(
		menuItem rcAGHelp "Adds or removes the alternate GUI, preserving animation.  Can be slow."
)
rcMenu rcSHFC
(
		menuItem rcSHFCHelp "Shows/Hides the face circles."
)
rcMenu rc3LD
(
		menuItem rc3LDHelp "Deletes Facial GUIs for 3Lateral Head Integration"
)
rcMenu rc3LD2
(
		menuItem rc3LD2Help "Deletes Unused body assets to prepare head rig for animation"
)
rcMenu rcSHT
(
		menuItem rcSHTHelp "Show/Hide selection toggle"
)
rcMenu rcTF
(
		menuItem rcTFHelp "Brings up Tongue Animation Tool"
)
rcMenu rcFREEZE
(
		menuItem rcFREEZEHelp "Freeze/Unfreeze selection toggle"
)
rcMenu rcSSETS
(
		menuItem rcSSETSHelp "Deletes all selection sets, and creates new selection sets for Mouth, Eyes and Brows"
)
rcMenu rcZC
(
		menuItem rcZCHelp "Zeroes out all selected controls"
)
rcMenu rcDCS
(
		menuItem rcDCSHelp "Deletes ALL keys from ALL selected objects."
)
rcMenu rcC2to1
(
		menuItem rcC2to1Help "**WIP** Converts all Standard Paradise Face GUI's 2-way Wire Params to 1-way."
)
rcMenu rcC1to2
(
		menuItem rcC1to2Help "**WIP** Converts all Standard Paradise Face GUI's 1-way Wire Params to 2-way."
)
rcMenu rcDG
(
		menuItem rcDGHelp "**WIP** Disconnect Side GUI.  To be incorporated into export script."
)
rcMenu rcSSh
(
		menuItem rcSShHelp "Sets up shading display settings for all objects in selection."
)
rcMenu rcSLay
(
		menuItem rcSLayHelp "Organize all objects into layers."
)
rcMenu rcFAL
(
		menuItem rcFALHelp "Organize all objects into layers."
)
rcMenu rcEFBX
(
		menuItem rcEFBX "Export animation into [filename].FBX, in the same folder, to be used in mobu.  Do not save the scene afterward."
)