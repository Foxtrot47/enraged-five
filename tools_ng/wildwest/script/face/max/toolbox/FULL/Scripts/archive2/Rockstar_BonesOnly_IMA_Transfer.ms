fn runIMA_Bones =
(


	--If rollout(ui window) exists, delete it and create a new one.
	if (imaUI != undefined)
	then (destroydialog imaUI)

	--Create rollout/ui window
	rollout imaUI "IMA UI" width:100 height:95
	(

		button saveIMA "SAVE IMA" pos:[5, 5] width:90 height:40
		button getIMA "LOAD IMA" pos:[5, 50] width:90 height:40


		--FUNCTIONS ******************************************************


		fn AddKeyForController curController curLine = 
		(
			parts = filterString curLine "<>,"

			myVal = parts[3] as float
			myTime = parts[4] as float

			newKey = addnewKey curController myTime
			newKey.value = myVal
		)

		fn ExtractDataForController controllerLine in_file=
		(
			partsOfString = filterString controllerLine "<>"

			if partsOfString.count != 3 then return 0

			curObj = getNodeByname partsOfString[2]

			if(isKindOf curObj BoneGeometry) then
			(
				keysPosX = curObj.controller[1].controller[1].keys
				keysPosY = curObj.controller[1].controller[2].keys
				keysPosZ = curObj.controller[1].controller[3].keys

				keysRotX = curObj.controller[2].controller[1].keys
				keysRotY = curObj.controller[2].controller[2].keys
				keysRotZ = curObj.controller[2].controller[3].keys

				deleteKeys keysPosX
				deleteKeys keysPosY
				deleteKeys keysPosZ 
				deleteKeys keysRotX 
				deleteKeys keysRotY 
				deleteKeys keysRotZ 

				while not eof in_file do
				(
					curline = readline in_file

					if findString curline "<Controller>" != undefined then
					(
						--we have found the end for this controller
						exit
					)

					if findString curline "<XPos>" != undefined then
					(
						AddKeyForController keysPosX curLine
					)

					if findString curline "<YPos>" != undefined then
					(
						AddKeyForController keysPosY curLine
					)

					if findString curline "<ZPos>" != undefined then
					(
						AddKeyForController keysPosZ curLine
					)

					if findString curline "<XRot>" != undefined then
					(
						AddKeyForController keysRotX curLine
					)

					if findString curline "<YRot>" != undefined then
					(
						AddKeyForController keysRotY curLine
					)

					if findString curline "<ZRot>" != undefined then
					(
						AddKeyForController keysRotZ curLine
					)
				)
			)
			else
			(
				xKeys = curObj.pos.controller[1].controller[1].keys
				yKeys = curObj.pos.controller[2].controller[1].keys

				deleteKeys xKeys
				deleteKeys yKeys

				while not eof in_file do
				(
					curline = readline in_file

					if findString curline "<Controller>" != undefined then
					(
						--we have found the end for this controller
						exit
					)

					if findString curline "<X>" != undefined then
					(
						AddKeyForController xKeys curLine
					)

					if findString curline "<Y>" != undefined then
					(
						AddKeyForController yKeys curLine
					)
				)
			)
		)

		fn ReadControllerValuesFromFile = 
		(
			filename = getOpenFileName caption:"Animation Script File" types:"Script Files (*.ima)|*.ima"

			if filename == undefined then return 0

			in_file = openFile filename

			while not eof in_file do
			(
				curLine = readline in_file

				if findString curline "<Controller>" != undefined then
				(
					print curline 

					ExtractDataForController curline in_file
				)

			)

			close in_file
		)

		fn WriteControllerValuesToFile writingFile conArray =
		(	
			for i = 1 to conArray.count do
			(
				cur = conArray [i]

				if(isKindOf cur BoneGeometry) then
				(
					keysPosX = cur.controller[1].controller[1].keys
					keysPosY = cur.controller[1].controller[2].keys
					keysPosZ = cur.controller[1].controller[3].keys

					keysRotX = cur.controller[2].controller[1].keys
					keysRotY = cur.controller[2].controller[2].keys
					keysRotZ = cur.controller[2].controller[3].keys


					format ("<Controller>" + cur.name + "</Controller>\n") to:writingFile


					--Write out the X Pos Keys
					for j = 1 to keysPosX.count do
					(
						myVal = keysPosX[j].value
						myTime = keysPosX[j].time

						format ("\t<XPos>" + (myVal as string) + "," + (myTime as string)+ "</XPos>\n") to:writingFile
					)

					--Write out the Y Pos Keys
					for j = 1 to keysPosY .count do
					(
						myVal = keysPosY[j].value
						myTime = keysPosY[j].time

						format ("\t<YPos>" + (myVal as string) + "," + (myTime as string)+ "</YPos>\n") to:writingFile
					)

					--Write out the Z Pos Keys
					for j = 1 to keysPosZ.count do
					(
						myVal = keysPosZ[j].value
						myTime = keysPosZ[j].time

						format ("\t<ZPos>" + (myVal as string) + "," + (myTime as string)+ "</ZPos>\n") to:writingFile
					)

					--Write out the X Rot Keys
					for j = 1 to keysRotX.count do
					(
						myVal = keysRotX[j].value
						myTime = keysRotX[j].time

						format ("\t<XRot>" + (myVal as string) + "," + (myTime as string)+ "</XRot>\n") to:writingFile
					)

					--Write out the Y Rot Keys
					for j = 1 to keysRotY.count do
					(
						myVal = keysRotY[j].value
						myTime = keysRotY[j].time

						format ("\t<YRot>" + (myVal as string) + "," + (myTime as string)+ "</YRot>\n") to:writingFile
					)

					--Write out the Z Rot Keys
					for j = 1 to keysRotZ.count do
					(
						myVal = keysRotZ[j].value
						myTime = keysRotZ[j].time

						format ("\t<ZRot>" + (myVal as string) + "," + (myTime as string)+ "</ZRot>\n") to:writingFile
					)

					format ("<Controller>" + cur.name + "</Controller>\n") to:writingFile
				)
				else
				(
					keysX = cur.pos.controller[1].controller[1].keys
					keysY = cur.pos.controller[2].controller[1].keys

					format ("<Controller>" + cur.name + "</Controller>\n") to:writingFile


					--Write out the X Keys
					for j = 1 to keysX.count do
					(
						myVal = keysX[j].value
						myTime = keysX[j].time

						format ("\t<X>" + (myVal as string) + "," + (myTime as string)+ "</X>\n") to:writingFile
					)

					--Write out the Y Keys
					for j = 1 to keysY.count do
					(
						myVal = keysY[j].value
						myTime = keysY[j].time

						format ("\t<Y>" + (myVal as string) + "," + (myTime as string)+ "</Y>\n") to:writingFile
					)

					format ("<Controller>" + cur.name + "</Controller>\n") to:writingFile
				)
			)
		)

		fn SelectAndWriteOutput = 
		(
			--select the joystick controllers

			sel = getCurrentSelection()

			filename = getSaveFileName caption:"Animation Script File" types:"Script Files (*.ima)|*.ima"

			if filename == undefined then return 0

			output_file = createFile filename

			WriteControllerValuesToFile output_file sel

			close output_file
		)

			-- BUTTON CALLS
		on saveIMA pressed do
		(
				selectandwriteoutput();
		)

		on getIMA pressed do
		(
				readcontrollervaluesfromfile();		
		)





	)createDialog imaUI
)