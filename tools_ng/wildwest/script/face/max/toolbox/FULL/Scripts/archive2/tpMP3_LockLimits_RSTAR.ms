/*********************************************
tpMP3_LockLimits_v002

	Makes limits "unkeyable"
	So when animator presses "key all" button (K),
	no keys are placed on limits, leaving
	less clutter in Graph editor,
	and fewer keys overall.
	
	Tested Compatible Characters:
		MaxPayne (A-Rig)
		Giovanna (B-Rig)
		...(please check)
		
	Revision History
		v001 - Hello World! - Thanh P
		v002 - wrapped in fn tpMP3_LockLimits

**********************************************/

fn tpMP3_LockLimits = 
	(
	-- Initiation
	tempStartTime = animationRange.start
	animationRange = interval 0f animationRange.end
	sliderTime = 0
	progressTracker = 0
	aRig = false

	-- Define/Create progress bar
	rollout progressRollout "Progress" (
		progressBar progressLimitLock
	)
	destroyDialog progressRollout
	createDialog progressRollout

	-- These objects will have pos X & pos Y limits made "unkeyable"
	controllerSet1 = #(
		$con_browRaise_l, 
		$con_browRaise_cr, 
		$con_upperLipRaise_c, 
		$con_lowerLipDepress_c, 
		$con_upperLipRaise_l, 
		$con_browRaise_cl, 
		$con_lowerLipDepress_l, 
		$con_upperLipRaise_r, 
		$con_lowerLipDepress_r, 
		$con_mouthInner_c, 
		$con_eyeBall_r, 
		$con_eyeBall_l, 
		$con_lipStretch_l, 
		$con_jaw_c, 
		$con_browRaise_r, 
		$con_cheek_l, 
		$con_cheek_r, 
		$con_lipStretch_r,
		$IM_EyeSquintInnerL_circle, 
		$IM_EyeSquintInnerR_circle, 
		$IM_OpenShutL_Circle, 
		$IM_OpenShutR_Circle, 
		$IM_CheekSquintR_circle, 
		$IM_CheekSquintL_circle, 
		$IM_lipRollUR_circle, 
		$IM_lipRollUC_circle, 
		$IM_lipRollUL_circle, 
		$IM_lipRollDL_circle, 
		$IM_lipRollDC_circle, 
		$IM_lipRollDR_circle, 
		$IM_PuckerR_circle, 
		$IM_PuckerL_circle, 
		$IM_pressR_circle, 
		$IM_pressL_circle, 
		$IM_Oh_circle, 
		$IM_OpenSmileR_circle, 
		$IM_OpenSmileL_circle, 
		$IM_LipsCloseOuterR_circle, 
		$IM_LipsCloseOuterL_circle, 
		$IM_SuckPuffL_circle, 
		$IM_SuckPuffR_circle, 
		$IM_FrownSmileL_circle, 
		$IM_FrownSmileR_circle, 
		$IM_LowerCH_circle, 
		$IM_UpperCH_circle, 
		$IM_Tongue_Out_circle, 
		$IM_Funnel_UR_circle, 
		$IM_Funnel_UL_circle, 
		$IM_Funnel_DR_circle, 
		$IM_Funnel_DL_circle, 
		$IM_MouthSuck_UR_circle, 
		$IM_MouthSuck_UL_circle, 
		$IM_Tongue_WNP_circle, 
		$IM_BrowSqueezeL_circle, 
		$IM_DilateContract_circle, 
		$IM_SneerL_circle, 
		$IM_SneerR_circle, 
		$IM_TongueLRU_circle, 
		$IM_MouthSuck_DR_circle, 
		$IM_MouthSuck_DL_circle, 
		$IM_BrowSqueezeR_circle
	)

	-- These objects will have ONLY pos Y limits made "unkeyable"
	controllerSet2 = #(
		$con_chinRaise_c
	)

	-- The rest... these objects will have keys cleaned (if openMe file).
	controllerSet3 = #(
		$con_nose_c,
		$con_mouth_c,
		$IM_AltGui_text
	)

	-- These objects do NOT exist on B-Rig
	controllerSet4 = #(
		$IM_NasoFurrowR_circle, 
		$IM_NasoFurrowL_circle
	)

	-- Number of controllers for progress Tracking
	progressTrackerCount = controllerSet1.count+controllerSet2.count+controllerSet3.count+controllerSet4.count

	-- Make limits unkeyable in X_Position and Y_Position
	for i in controllerSet1 do (
		
		-- sets keys so that limits appear
		if (i != undefined) do (
			select i
			max set key all position
			
			-- delete keys from newly created limits
			deleteKeys i.pos.controller.X_Position.controller[2] #allKeys
			deleteKeys i.pos.controller.Y_Position.controller[2] #allKeys
			
			-- make limits unkeyable
			i.pos.controller.X_Position.controller.upper_limit.controller.keyable = false
			i.pos.controller.X_Position.controller.upper_smoothing.controller.keyable = false
			i.pos.controller.X_Position.controller.lower_limit.controller.keyable = false
			i.pos.controller.X_Position.controller.lower_smoothing.controller.keyable = false
			i.pos.controller.Y_Position.controller.upper_limit.controller.keyable = false
			i.pos.controller.Y_Position.controller.upper_smoothing.controller.keyable = false
			i.pos.controller.Y_Position.controller.lower_limit.controller.keyable = false
			i.pos.controller.Y_Position.controller.lower_smoothing.controller.keyable = false
			
			clearselection()
			
			-- update progress
			progressRollout.progressLimitLock.value = progressTracker*100/(controllerSet1.count+controllerSet2.count+controllerSet3.count)
			progressTracker+=1
		)
	)

	-- 
	if (queryBox "Is this an A-Rig?" beep:false) do (
		aRig = true
		for i in controllerSet4 do (
			if ( i != undefined) do(
				select i
				max set key all position
				deleteKeys i.pos.controller.X_Position.controller[2] #allKeys
				deleteKeys i.pos.controller.Y_Position.controller[2] #allKeys
				i.pos.controller.X_Position.controller.upper_limit.controller.keyable = false
				i.pos.controller.X_Position.controller.upper_smoothing.controller.keyable = false
				i.pos.controller.X_Position.controller.lower_limit.controller.keyable = false
				i.pos.controller.X_Position.controller.lower_smoothing.controller.keyable = false
				i.pos.controller.Y_Position.controller.upper_limit.controller.keyable = false
				i.pos.controller.Y_Position.controller.upper_smoothing.controller.keyable = false
				i.pos.controller.Y_Position.controller.lower_limit.controller.keyable = false
				i.pos.controller.Y_Position.controller.lower_smoothing.controller.keyable = false
				clearselection()
				progressRollout.progressLimitLock.value = progressTracker*100/(progressTrackerCount)
				progressTracker+=1
			)
		)
	)

	-- Make limits unkeyable in just Y_Position (similar to above)
	for i in controllerSet2 do (
		if (i != undefined) do (
			select i
			max set key all position
			deleteKeys i.pos.controller.Y_Position.controller[2] #allKeys
			i.pos.controller.Y_Position.controller.upper_limit.controller.keyable = false
			i.pos.controller.Y_Position.controller.upper_smoothing.controller.keyable = false
			i.pos.controller.Y_Position.controller.lower_limit.controller.keyable = false
			i.pos.controller.Y_Position.controller.lower_smoothing.controller.keyable = false
			clearselection()
			progressRollout.progressLimitLock.value = progressTracker*100/(progressTrackerCount)
			progressTracker+=1
		)
	)

	-- Reset progress bar.
	progressTracker = 0
	progressRollout.progressLimitLock.value = 0

	-- Asks user if it is the openMe.
	-- Yes - Clean up all keys. No - Leave keys where they are.
	if (queryBox "Is this the openMe file? \nIf YES, All animation will be deleted." beep:false) do (
		if (queryBox "Are you SURE this is the openMe file??? \nALL ANIMATION WILL BE DELETED!" beep:false) do (
			sliderTime = 0
			for i in controllerSet1 do (
				deleteKeys i #allKeys
				progressRollout.progressLimitLock.value = (progressTracker+controllerSet1.count)*100/(progressTrackerCount)
				progressTracker+=1
			)
			for i in controllerSet2 do (
				deleteKeys i #allKeys
				progressRollout.progressLimitLock.value = (progressTracker+controllerSet1.count)*100/(progressTrackerCount)
				progressTracker+=1
			)
			for i in controllerSet3 do (
				deleteKeys i #allKeys
				progressRollout.progressLimitLock.value = (progressTracker+controllerSet1.count)*100/(progressTrackerCount)
				progressTracker+=1
			)
			-- If this is an A-Rig from queryBox Before, cleanup A-Rig stuff
			if (aRig == true) do (
				for i in controllerSet4 do (
					deleteKeys i #allKeys
					progressRollout.progressLimitLock.value = (progressTracker+controllerSet1.count)*100/(progressTrackerCount)
					progressTracker+=1
				)
			)
		)
	)

	animationRange = interval tempStartTime animationRange.end
	destroyDialog progressRollout
)