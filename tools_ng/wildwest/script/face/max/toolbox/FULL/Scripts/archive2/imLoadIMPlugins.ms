fn openRetargeter =
 (
  utilArray = utilityplugin.classes
  checkPlugin = findItem utilArray Image_Metrics_Retargeting
  if(checkPlugin == 0) then (print "Image Metrics Retargeter not found...")
  if(checkPlugin != 0) then (UtilityPanel.OpenUtility Image_Metrics_Retargeting)
 )