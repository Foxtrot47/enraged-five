/* SET AUTOKEY MODE ****************************************************************/
	if(animButtonState == false)then(animButtonState = true)

fn loadFloaterConfiguration =
(
	-- LOAD IS BROKEN
	
	/*
	--open file in read mode
	openedFile = openFile fileNameQ mode:"r"
	
	--if file opened, load configuration
	if (openedFile != undefined) then
	(	
		while (not eof openedFile) do
		(
			append floatersArray (readValue openedFile)
		)
	)
	*/
)

fn saveFloaterConfiguration =
(
	--SAVE IS BROKEN
	
	/*
	--open file in write/read mode (delete contents first)
	openedFile = openFile fileNameQ mode:"w+"

	--if file opened, save configuration
	if (openedFile != undefined) then
	(
		format "%," jgAnimToolboxFloater.size[2] to:openedFile
		format "%," jgAnimToolboxFloater.pos[1] to:openedFile
		format "%," jgAnimToolboxFloater.pos[2] to:openedFile			
		for i = 1 to jgAnimToolBoxFloater.rollouts.count do
		(
			format "%," jgAnimToolBoxFloater.rollouts[i].open to:openedFile
		)
	)
	close openedFile
	*/
)

fn updateToolBoxCoords =
(
	--updates coords drop down in toolbox on tool or spacemode change, via callback
	x = getRefCoordSys()
	y = x as string
	zDDCoords = findItem coordsArray coordsString
	roMaxTools.ddCoords.selection = z
)

fn getToolboxCoords =
(
	x = getRefCoordSys()
	if(x == #hybrid)then(coordsString = "View")
	if(x == #screen)then(coordsString = "Screen")
	if(x == #world)then(coordsString = "World")
	if(x == #parent)then(coordsString = "Parent")
	if(x == #local)then(coordsString = "Local")
	if(x == #gimbal)then(coordsString = "Gimbal")	
	if(x == #grid)then(coordsString = "Grid")	
)
