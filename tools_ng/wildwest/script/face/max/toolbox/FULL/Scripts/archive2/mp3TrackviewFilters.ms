function mp3TrackviewFilters = (
	view = trackviews.currentTrackView
	if view != undefined then (
		trackviews.current.clearFilter #all
		trackviews.current.setFilter #hierarchy #objects #spacewarpBindings #transforms #position #curveX #curveY #modifiedObjects #baseObjects #materialsMaps #materialParameters #animatedTracks #selectedObjects #positionX #positionY #positionZ #positionW #rotationX #rotationY #rotationZ #scaleX #scaleY #scaleZ #red #green #blue #alpha #keyableTracks
	)
	else
		print "The Trackview needs to be opened for this button to work."
)