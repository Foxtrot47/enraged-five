/*----------------------------------------------------------- *\
| -- Title: Connect Alternate MP3 Gui
| -- Author: Jay Grenier, Jonah Goodman 2010
| -- Contact: jay.grenier@image-metrics.com
| -- Version: 0.01
| -- Last Update: May 17, 2010                                 
\*----------------------------------------------------------- */


-- The official wrapper for the alternate gui
fn connectAltGUI =
(
	
	-- Merge GUI Elements
	guiPath = "X:/payne/tools/wildwest/script/max/rockstar_sandiego/cutscene/face/OpenMeCreation/scripts/MP3_alternateGUI.max"
	mergeMAXFile(guiPath)
	print "Alternate GUI Merged."

	$IM_AltGui_text.pos = $con_jaw_c.pos + [0.15,0,0.27]
	deletekeys $IM_AltGui_text.controller[1].keys

	
	-- Remove Limit Controller from Cust_Attr
	objAttr = #(
		$con_eyeBall_r,
		$con_eyeBall_l,
		$con_nose_c,
		$con_mouth_c
	)
	
	for i in objAttr do
	(
		n = 1
		while (i.modifiers[#attribute_holder].IM_ATTRS_A[n] != undefined) do
		(
			i.modifiers[#attribute_holder].IM_ATTRS_A[n].controller = bezier_float()
			n+=1 
		)
	)
	-- end
	
	
	rollout roProgBar "Adding Alternate GUI" width:300
	(
		-- Progress Bar
		label lbl1 "Adding GUI and transferring animation."
		label lbl2 "Your computer may hang, please be VERY patient..."
		progressbar altGUI_prog color:white
	)
	
	createDialog roProgBar
	
	-- ALT GUI Y-Axis Joystick controllers (Y-direction)
	altGUI_source_Y = #(
		$IM_OpenShutR_Circle,
		$IM_OpenShutL_Circle,
		$IM_EyeSquintInnerR_circle,
		$IM_EyeSquintInnerL_circle,
		$IM_CheekSquintR_circle,			--5
		$IM_CheekSquintL_circle,
		$IM_BrowSqueezeR_circle,
		$IM_BrowSqueezeL_circle,
		$IM_DilateContract_circle,
		$IM_SneerL_circle,					--10
		$IM_SneerR_circle,
		$IM_FrownSmileR_circle,
		$IM_FrownSmileL_circle,
		$IM_OpenSmileR_circle,
		$IM_OpenSmileL_circle,				--15
		$IM_SuckPuffR_circle,
		$IM_SuckPuffL_circle,
		$IM_LipsCloseOuterR_circle,
		$IM_LipsCloseOuterL_circle,
		$IM_pressR_circle,					--20
		$IM_pressL_circle,
		$IM_Funnel_UL_circle,
		$IM_Funnel_UR_circle,
		$IM_Funnel_DL_circle,
		$IM_Funnel_DR_circle,				--25
		$IM_MouthSuck_UL_circle,
		$IM_MouthSuck_UR_circle,
		$IM_MouthSuck_DL_circle,
		$IM_MouthSuck_DR_circle,
		$IM_NasoFurrowR_circle,			--30
		$IM_NasoFurrowL_circle,
		$IM_lipRollUL_circle,
		$IM_lipRollUC_circle,
		$IM_lipRollUR_circle,
		$IM_lipRollDL_circle,				--35
		$IM_lipRollDC_circle,
		$IM_lipRollDR_circle,
		$IM_PuckerR_circle,
		$IM_PuckerL_circle,
		$IM_Oh_circle,							--40
		$IM_LowerCH_circle,
		$IM_UpperCH_circle,
		$IM_Tongue_Out_circle,
		$IM_TongueLRU_circle,
		$IM_Tongue_WNP_circle				--45
	)

	-- String Version
	altGUI_source_Y_string = #(
		"$IM_OpenShutR_Circle.controller[1][2][1].value",
		"$IM_OpenShutL_Circle.controller[1][2][1].value",
		"$IM_EyeSquintInnerR_circle.controller[1][2][1].value",
		"$IM_EyeSquintInnerL_circle.controller[1][2][1].value",
		"$IM_CheekSquintR_circle.controller[1][2][1].value",			--5
		"$IM_CheekSquintL_circle.controller[1][2][1].value",
		"$IM_BrowSqueezeR_circle.controller[1][2][1].value",
		"$IM_BrowSqueezeL_circle.controller[1][2][1].value",
		"$IM_DilateContract_circle.controller[1][2][1].value",
		"$IM_SneerL_circle.controller[1][2][1].value",					--10
		"$IM_SneerR_circle.controller[1][2][1].value",
		"$IM_FrownSmileR_circle.controller[1][2][1].value",
		"$IM_FrownSmileL_circle.controller[1][2][1].value",
		"$IM_OpenSmileR_circle.controller[1][2][1].value",
		"$IM_OpenSmileL_circle.controller[1][2][1].value",				--15
		"$IM_SuckPuffR_circle.controller[1][2][1].value",
		"$IM_SuckPuffL_circle.controller[1][2][1].value",
		"$IM_LipsCloseOuterR_circle.controller[1][2][1].value",
		"$IM_LipsCloseOuterL_circle.controller[1][2][1].value",
		"$IM_pressR_circle.controller[1][2][1].value",					--20
		"$IM_pressL_circle.controller[1][2][1].value",
		"$IM_Funnel_UL_circle.controller[1][2][1].value",
		"$IM_Funnel_UR_circle.controller[1][2][1].value",
		"$IM_Funnel_DL_circle.controller[1][2][1].value",
		"$IM_Funnel_DR_circle.controller[1][2][1].value",				--25
		"$IM_MouthSuck_UL_circle.controller[1][2][1].value",
		"$IM_MouthSuck_UR_circle.controller[1][2][1].value",
		"$IM_MouthSuck_DL_circle.controller[1][2][1].value",
		"$IM_MouthSuck_DR_circle.controller[1][2][1].value",
		"$IM_NasoFurrowR_circle.controller[1][2][1].value",			--30
		"$IM_NasoFurrowL_circle.controller[1][2][1].value",
		"$IM_lipRollUL_circle.controller[1][2][1].value",
		"$IM_lipRollUC_circle.controller[1][2][1].value",
		"$IM_lipRollUR_circle.controller[1][2][1].value",
		"$IM_lipRollDL_circle.controller[1][2][1].value",				--35
		"$IM_lipRollDC_circle.controller[1][2][1].value",
		"$IM_lipRollDR_circle.controller[1][2][1].value",
		"$IM_PuckerR_circle.controller[1][2][1].value",
		"$IM_PuckerL_circle.controller[1][2][1].value",
		"$IM_Oh_circle.controller[1][2][1].value",							--40
		"$IM_LowerCH_circle.controller[1][2][1].value",
		"$IM_UpperCH_circle.controller[1][2][1].value",
		"$IM_Tongue_Out_circle.controller[1][2][1].value",
		"$IM_TongueLRU_circle.controller[1][2][1].value",
		"$IM_Tongue_WNP_circle.controller[1][2][1].value"				--45
	)


	-- Attributes on controllers that the alt gui will wire to.  A-Rig, Y-Direction
	altGUI_dest_A_Y = #(
		$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[2],
		$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[2],
		$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[3],			--5
		$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[3],
		$con_browRaise_cr.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_browRaise_cl.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[2],					--10
		$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[3],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[2],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[4],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[3],				--15
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[23],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[22],
		$con_lipStretch_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_lipStretch_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[21],				--20
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[20],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[10],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[11],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[12],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[13],				--25
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[16],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[17],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[18],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[19],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[6],				--30
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[5],
		$con_upperLipRaise_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_upperLipRaise_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_upperLipRaise_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_lowerLipDepress_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],	--35
		$con_lowerLipDepress_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_lowerLipDepress_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[8],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[7],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[9],				--40
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[15],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[14],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[25],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[24],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[26]				--45
	)


	-- Attributes on controllers that the alt gui will wire to. B-Rig, Y-Direction
	altGUI_dest_B_Y = #(
		$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[2],
		$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[2],
		$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[3],			--5
		$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[3],
		$con_browRaise_cr.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_browRaise_cl.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[2],					--10
		$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[3],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[2],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[4],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[3],				--15
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[21],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[20],
		$con_lipStretch_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_lipStretch_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[19],				--20
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[18],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[8],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[9],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[10],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[11],				--25
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[14],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[15],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[16],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[17],
		undefined,																					--30
		undefined,
		$con_upperLipRaise_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_upperLipRaise_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_upperLipRaise_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_lowerLipDepress_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],	--35
		$con_lowerLipDepress_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_lowerLipDepress_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[6],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[5],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[7],				--40
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[13],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[12],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[23],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[22],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[24]				--45
	)

	-- ALT GUI X-Axis Joystick controllers (X-direction)
	altGUI_source_X = #(
		$IM_TongueLRU_circle,
		$IM_Tongue_WNP_circle
	)

	-- ALT GUI X-Axis Joystick controllers (X-direction) String version
	altGUI_source_X_string = #(
		"$IM_TongueLRU_circle.controller[1][1][1].value",
		"$IM_Tongue_WNP_circle.controller[1][1][1].value"
	)

	-- Attributes on controllers that the alt gui will wire to. A-Rigs, X-Direction
	altGUI_dest_A_X = #(
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[27],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[28]
	)

	-- Attributes on controllers that the alt gui will wire to. B-Rigs, X-Direction
	altGUI_dest_B_X = #(
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[25],
		undefined
	)
	
	if $con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[28] != undefined then (
		
		for i = 1 to altGUI_source_Y.count do (
			if altGUI_dest_A_Y[i] != undefined do (
				
				/*** GET DATA FROM Control ***/		
				-- Get All Keyframes On This Control
				allKeyframes = #()
				allKeyframes = altGUI_dest_A_Y[i].keys
				allValues = #()
				-- Get Key Values Into Array
				if allKeyframes.count != 0 do (
					for j = 1 to allKeyframes.count do (
						append allValues altGUI_dest_A_Y[i].keys[j].value
					)
				)
				
				paramWire.connect altGUI_source_Y[i].controller[1][2][1] altGUI_dest_A_Y[i] altGUI_source_Y_string[i]
				
				
				/*** ADD DATA TO ORIGINAL CONTROL ***/
				-- Set All Keys
				if allKeyframes.count != 0 do (
					for j = 1 to allKeyframes.count do (
						addNewKey altGUI_source_Y[i].controller[1][2][1] allKeyframes[j].time
						altGUI_source_Y[i].controller[1][2][1].keys[j].value = allValues[j]
					)
				)
				
				-- Update Progress Bar
				roProgBar.altGUI_prog.value = 100.*i/altGUI_source_Y.count
				
				
			)
		)
		
		for i = 1 to altGUI_source_X.count do (
			if altGUI_dest_A_X[i] != undefined do (
				
				/*** GET DATA FROM Control ***/		
				-- Get All Keyframes On This Control
				allKeyframes = #()
				allKeyframes = altGUI_dest_A_X[i].keys
				allValues = #()
				-- Get Key Values Into Array
				if allKeyframes.count != 0 do (
					for j = 1 to allKeyframes.count do (
						append allValues altGUI_dest_A_X[i].keys[j].value
					)
				)
				
				paramWire.connect altGUI_source_X[i].controller[1][1][1] altGUI_dest_A_X[i] altGUI_source_X_string[i]
				
				/*** ADD DATA TO ORIGINAL CONTROL ***/
				-- Set All Keys
				if allKeyframes.count != 0 do (
					for j = 1 to allKeyframes.count do (
						addNewKey altGUI_source_X[i].controller[1][1][1] allKeyframes[j].time
						altGUI_source_X[i].controller[1][1][1].keys[j].value = allValues[j]
					)
				)
				
			)
		)

		
		print "Alternate GUI A-Rig Connected."
	)
	
	else (
		
		for i = 1 to altGUI_source_Y.count do (
			if altGUI_dest_B_Y[i] != undefined do (
				
				/*** GET DATA FROM Control ***/		
				-- Get All Keyframes On This Control
				allKeyframes = #()
				allKeyframes = altGUI_dest_B_Y[i].keys
				allValues = #()
				-- Get Key Values Into Array
				if allKeyframes.count != 0 do (
					for j = 1 to allKeyframes.count do (
						append allValues altGUI_dest_B_Y[i].keys[j].value
					)
				)
				
				paramWire.connect altGUI_source_Y[i].controller[1][2][1] altGUI_dest_B_Y[i] altGUI_source_Y_string[i]
				
				/*** ADD DATA TO ORIGINAL CONTROL ***/
				-- Set All Keys
				if allKeyframes.count != 0 do (
					for j = 1 to allKeyframes.count do (
						addNewKey altGUI_source_Y[i].controller[1][2][1] allKeyframes[j].time
						altGUI_source_Y[i].controller[1][2][1].keys[j].value = allValues[j]
					)
				)
				
				-- Update Progress Bar
				roProgBar.altGUI_prog.value = 100.*i/altGUI_source_Y.count
				
			)
		)
		
		for i = 1 to altGUI_source_X.count do (
			if altGUI_dest_B_X[i] != undefined do (
				
				
				/*** GET DATA FROM Control ***/		
				-- Get All Keyframes On This Control
				allKeyframes = #()
				allKeyframes = altGUI_dest_B_X[i].keys
				allValues = #()
				-- Get Key Values Into Array
				if allKeyframes.count != 0 do (
					for j = 1 to allKeyframes.count do (
						append allValues altGUI_dest_B_X[i].keys[j].value
					)
				)
				
				paramWire.connect altGUI_source_X[i].controller[1][1][1] altGUI_dest_B_X[i] altGUI_source_X_string[i]
				
				/*** ADD DATA TO ORIGINAL CONTROL ***/
				-- Set All Keys
				if allKeyframes.count != 0 do (
					for j = 1 to allKeyframes.count do (
						addNewKey altGUI_source_X[i].controller[1][1][1] allKeyframes[j].time
						altGUI_source_X[i].controller[1][1][1].keys[j].value = allValues[j]
					)
				)
				
			)
		)
		
		print "Alternate GUI B-Rig Connected."
	)
	
	destroyDialog roProgBar
	
	

	move $IM_AltGui_text [0.0590744,0,-0.0328026]
	
	deletekeys $IM_AltGui_text.controller[1].keys

	max set key ikparams filter
	max set key scale filter
	 
	IM_Controls = #(
		$IM_EyeSquintInnerL_circle,
		$IM_EyeSquintInnerR_circle,
		$IM_OpenShutL_Circle,
		$IM_OpenShutR_Circle,
		$IM_CheekSquintR_circle,
		$IM_CheekSquintL_circle,
		$IM_lipRollUR_circle,
		$IM_lipRollUC_circle,
		$IM_lipRollUL_circle,
		$IM_lipRollDL_circle,
		$IM_lipRollDC_circle,
		$IM_lipRollDR_circle,
		$IM_PuckerR_circle,
		$IM_PuckerL_circle,
		$IM_pressR_circle,
		$IM_pressL_circle,
		$IM_Oh_circle,
		$IM_OpenSmileR_circle,
		$IM_OpenSmileL_circle,
		$IM_LipsCloseOuterR_circle,
		$IM_LipsCloseOuterL_circle,
		$IM_SuckPuffL_circle,
		$IM_SuckPuffR_circle,
		$IM_FrownSmileL_circle,
		$IM_FrownSmileR_circle,
		$IM_LowerCH_circle,
		$IM_UpperCH_circle,
		$IM_Tongue_Out_circle,
		$IM_Funnel_UR_circle,
		$IM_Funnel_UL_circle,
		$IM_Funnel_DR_circle,
		$IM_Funnel_DL_circle,
		$IM_MouthSuck_UR_circle,
		$IM_MouthSuck_UL_circle,
		$IM_Tongue_WNP_circle,
		$IM_BrowSqueezeL_circle,
		$IM_DilateContract_circle,
		$IM_SneerL_circle,
		$IM_SneerR_circle,
		$IM_NasoFurrowR_circle,
		$IM_NasoFurrowL_circle,
		$IM_TongueLRU_circle,
		$IM_MouthSuck_DR_circle,
		$IM_MouthSuck_DL_circle,
		$IM_BrowSqueezeR_circle
	)

	for i in IM_Controls do
	(
		try (
			setTransformLockFlags i #{3..9}
			i.controller[1][3].controller = bezier_float()
			i.controller[1][3].controller.keyable = false
			i.controller[2][1].controller.keyable = false
			i.controller[2][2].controller.keyable = false
			i.controller[2][3].controller.keyable = false
			i.controller[3].controller.keyable = false
		) catch()

		IM_Controls_2 = #(
			$con_eye_l,
			$con_eye_r
		)

		for i in IM_Controls_2 do
		(
			try(
				i.isfrozen = true	
			) catch()
		)
	)
)

fn disconnectAltGUI =
(
	
	rollout roProgBar "Disconnecting Alternate GUI" width:300
	(
		-- Progress Bar
		label lbl1 "Disconnecting GUI and transferring animation."
		label lbl2 "Your computer may hang, please be VERY patient..."
		progressbar altGUI_prog color:white
	)
	
	createDialog roProgBar
	
	/*** MASTER LIST OF CONTROLS ***/
	altGUI_source = #(
		$IM_OpenShutR_Circle.controller[1][2][1],
		$IM_OpenShutL_Circle.controller[1][2][1],
		$IM_EyeSquintInnerR_circle.controller[1][2][1],
		$IM_EyeSquintInnerL_circle.controller[1][2][1],
		$IM_CheekSquintR_circle.controller[1][2][1],			--5
		$IM_CheekSquintL_circle.controller[1][2][1],
		$IM_BrowSqueezeR_circle.controller[1][2][1],
		$IM_BrowSqueezeL_circle.controller[1][2][1],
		$IM_DilateContract_circle.controller[1][2][1],
		$IM_SneerL_circle.controller[1][2][1],					--10
		$IM_SneerR_circle.controller[1][2][1],
		$IM_FrownSmileR_circle.controller[1][2][1],
		$IM_FrownSmileL_circle.controller[1][2][1],
		$IM_OpenSmileR_circle.controller[1][2][1],
		$IM_OpenSmileL_circle.controller[1][2][1],			--15
		$IM_SuckPuffR_circle.controller[1][2][1],
		$IM_SuckPuffL_circle.controller[1][2][1],
		$IM_LipsCloseOuterR_circle.controller[1][2][1],
		$IM_LipsCloseOuterL_circle.controller[1][2][1],
		$IM_pressR_circle.controller[1][2][1],					--20
		$IM_pressL_circle.controller[1][2][1],
		$IM_Funnel_UL_circle.controller[1][2][1],
		$IM_Funnel_UR_circle.controller[1][2][1],
		$IM_Funnel_DL_circle.controller[1][2][1],
		$IM_Funnel_DR_circle.controller[1][2][1],				--25
		$IM_MouthSuck_UL_circle.controller[1][2][1],
		$IM_MouthSuck_UR_circle.controller[1][2][1],
		$IM_MouthSuck_DL_circle.controller[1][2][1],
		$IM_MouthSuck_DR_circle.controller[1][2][1],
		if($IM_NasoFurrowR_circle != undefined) then $IM_NasoFurrowR_circle.controller[1][2][1] else ( "NULL" ), --30
		if($IM_NasoFurrowL_circle != undefined) then $IM_NasoFurrowL_circle.controller[1][2][1] else ( "NULL" ),
		$IM_lipRollUL_circle.controller[1][2][1],
		$IM_lipRollUC_circle.controller[1][2][1],
		$IM_lipRollUR_circle.controller[1][2][1],
		$IM_lipRollDL_circle.controller[1][2][1],				--35
		$IM_lipRollDC_circle.controller[1][2][1],
		$IM_lipRollDR_circle.controller[1][2][1],
		$IM_PuckerR_circle.controller[1][2][1],
		$IM_PuckerL_circle.controller[1][2][1],
		$IM_Oh_circle.controller[1][2][1],							--40
		$IM_LowerCH_circle.controller[1][2][1],
		$IM_UpperCH_circle.controller[1][2][1],
		$IM_Tongue_Out_circle.controller[1][2][1],
		$IM_TongueLRU_circle.controller[1][2][1],
		$IM_Tongue_WNP_circle.controller[1][2][1],			--45
		$IM_TongueLRU_circle.controller[1][1][1],
		$IM_Tongue_WNP_circle.controller[1][1][1]
	)
	
	-- Attributes on controllers that the alt gui will wire to.  A-Rig, Y-Direction
	altGUI_dest_A = #(
		$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[2],
		$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[2],
		$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[3],			--5
		$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[3],
		$con_browRaise_cr.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_browRaise_cl.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[2],					--10
		$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[3],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[2],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[4],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[3],				--15
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[23],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[22],
		$con_lipStretch_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_lipStretch_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[21],				--20
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[20],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[10],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[11],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[12],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[13],				--25
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[16],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[17],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[18],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[19],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[6],				--30
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[5],
		$con_upperLipRaise_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_upperLipRaise_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_upperLipRaise_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_lowerLipDepress_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],	--35
		$con_lowerLipDepress_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_lowerLipDepress_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[8],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[7],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[9],				--40
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[15],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[14],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[25],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[24],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[26],				--45
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[27],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[28]
	)

	-- Attributes on controllers that the alt gui will wire to. B-Rig, Y-Direction
	altGUI_dest_B = #(
		$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[2],
		$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[2],
		$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[3],			--5
		$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[3],
		$con_browRaise_cr.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_browRaise_cl.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[2],					--10
		$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[3],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[2],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[4],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[3],				--15
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[21],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[20],
		$con_lipStretch_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_lipStretch_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[19],				--20
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[18],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[8],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[9],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[10],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[11],				--25
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[14],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[15],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[16],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[17],
		undefined,																					--30
		undefined,
		$con_upperLipRaise_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_upperLipRaise_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_upperLipRaise_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_lowerLipDepress_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1],	--35
		$con_lowerLipDepress_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_lowerLipDepress_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[6],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[5],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[7],				--40
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[13],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[12],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[23],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[22],
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[24],				--45
		$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[25],
		undefined
	)
	
	if $con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[28] != undefined then
	(
		temp_dest = altGUI_dest_A
		print "disconnecting A-Rig"
	)
	else
	(
		temp_dest = altGUI_dest_B
		print "disconnecting B-Rig"
	)
	
	for a = 1 to altGUI_source.count do (	
		
		
		-- Checks
		if(altGUI_source[a] == "NULL") then continue
		if(temp_dest[a] == undefined) then continue
		
		-- Update Progress Bar
		roProgBar.altGUI_prog.value = 100.*a/altGUI_source.count
		
		/*** GET DATA FROM JOYSTICK ***/		
			-- Get All Keyframes On This Control	
			allKeyframes = #()
			allKeyframes = altGUI_source[a].keys

			-- All Values (One For Each Keyframe)
			allValues = #()
			
			-- Get Key Values Into Array
			for i = 1 to allKeyframes.count do (
				append allValues altGUI_source[a].keys[i].value
			)			

		/*** BREAK CONNECTION ***/
			-- Disconnect Wire Param
			--print ("Disconnecting: " + temp_dest[a] as string)
			paramWire.disconnect temp_dest[a]

		/*** ADD DATA TO ORIGINAL CONTROL ***/
			-- Set All Keys
			for i = 1 to allKeyframes.count do (
				addNewKey temp_dest[a].controller allKeyframes[i].time
				temp_dest[a].controller.keys[i].value = allValues[i]
			)
			
	) -- End Ctrl Loop
	
	/*** REMOVE ALTERNATE GUI FROM SCENE ***/
	-- Select All IM_* Elements And Delete
	select $IM_*_circle
	delete $
	select $IM_*_text
	delete $
	select $IM_*_rect
	delete $

	destroyDialog roProgBar
	
	print "Finished disconnecting Alt GUI."
)

fn prepareAltGUI = 	
(
	if $IM_AltGui_text != undefined then (
		disconnectAltGUI()
	)
	else (
		
		-- Remove Limit Controller from Cust_Attr
		objAttr = #(
			$con_eyeBall_r,
			$con_eyeBall_l,
			$con_nose_c,
			$con_mouth_c
		)
		
		for i in objAttr do
		(
			n = 1
			while (i.modifiers[#attribute_holder].IM_ATTRS_A[n] != undefined) do
			(
				i.modifiers[#attribute_holder].IM_ATTRS_A[n].controller = bezier_float()
				n+=1 
			)
		)
		-- end
		
		connectAltGUI()
		
		max set key ikparams filter
		max set key scale filter
		
		IM_Controls = #(
			$IM_EyeSquintInnerL_circle,
			$IM_EyeSquintInnerR_circle,
			$IM_OpenShutL_Circle,
			$IM_OpenShutR_Circle,
			$IM_CheekSquintR_circle,
			$IM_CheekSquintL_circle,
			$IM_lipRollUR_circle,
			$IM_lipRollUC_circle,
			$IM_lipRollUL_circle,
			$IM_lipRollDL_circle,
			$IM_lipRollDC_circle,
			$IM_lipRollDR_circle,
			$IM_PuckerR_circle,
			$IM_PuckerL_circle,
			$IM_pressR_circle,
			$IM_pressL_circle,
			$IM_Oh_circle,
			$IM_OpenSmileR_circle,
			$IM_OpenSmileL_circle,
			$IM_LipsCloseOuterR_circle,
			$IM_LipsCloseOuterL_circle,
			$IM_SuckPuffL_circle,
			$IM_SuckPuffR_circle,
			$IM_FrownSmileL_circle,
			$IM_FrownSmileR_circle,
			$IM_LowerCH_circle,
			$IM_UpperCH_circle,
			$IM_Tongue_Out_circle,
			$IM_Funnel_UR_circle,
			$IM_Funnel_UL_circle,
			$IM_Funnel_DR_circle,
			$IM_Funnel_DL_circle,
			$IM_MouthSuck_UR_circle,
			$IM_MouthSuck_UL_circle,
			$IM_Tongue_WNP_circle,
			$IM_BrowSqueezeL_circle,
			$IM_DilateContract_circle,
			$IM_SneerL_circle,
			$IM_SneerR_circle,
			$IM_NasoFurrowR_circle,
			$IM_NasoFurrowL_circle,
			$IM_TongueLRU_circle,
			$IM_MouthSuck_DR_circle,
			$IM_MouthSuck_DL_circle,
			$IM_BrowSqueezeR_circle
		)
		
		for i in IM_Controls do
		(
			try (
				setTransformLockFlags i #{3..9}
				i.controller[1][3].controller = bezier_float()
				i.controller[1][3].controller.keyable = false
				i.controller[2][1].controller.keyable = false
				i.controller[2][2].controller.keyable = false
				i.controller[2][3].controller.keyable = false
				i.controller[3].controller.keyable = false
			) catch()

			IM_Controls_2 = #(
				$con_eye_l,
				$con_eye_r
			)

			for i in IM_Controls_2 do
			(
				try(
					i.isfrozen = true	
				) catch()
			)
			
		)
	)
)