fn runSceneSetup =
	(
		-- Title: Image Metrics 3DS Max Scene Setup
		-- Version: 1.1
		-- Author: Jay Grenier, 2009 
		-- Tooltip: 
		-- Sets up a Max scene file to be ready for an animator to begin working with Image Metrics' workflow.
		-- Creates image plane with layer, sets fps, frame length, imports wav/audio.
			
		--If rollout(ui window) exists, delete it and create a new one.
		if (maxSceneSetupRollout != undefined)
		then (destroydialog maxSceneSetupRollout)

		--Create rollout/ui window
		rollout maxSceneSetupRollout "Image Metrics 3DS Max Scene Setup" width:800 height:230
		(
			label titleLabel "-- Image Metrics 3DS Max Scene Creator --" pos:[300, 6] width:800 height:20
			
			edittext jpgInput "JPG Sequence (1st Frame): " pos:[8,28] width:700 height:18
			button jpgGet "Browse" pos:[715, 26] width:65 height:22
			
			edittext wavInput "WAV (Audio)                        " pos:[8,54] width:700 height:18
			button wavGet "Browse" pos:[715, 54] width:65 height:22				
			
			label framerateLabel "Frame Rate (FPS): " pos:[8, 82]
			radiobuttons sceneFR labels:#("24 (Film)   ", "25 (PAL)   ", "30 (NTSC)") default:3 pos:[142,82]
			
			edittext startFrameInput "Scene Starting Frame:         " pos:[8,111] width:160 height:18
							
			button submit "Create and Setup Scene" width:200 height:40 pos:[320, 120]
			
			label note1 "Note: Please open the character rig file before using this script." pos:[272, 185]
			label note2 "-- Jay Grenier 2009 --" pos:[370, 210]
		
		
		--set default starting frame to 0
		on maxSceneSetupRollout open do
		(
			startFrameInput.text = 0 as string
		)

		on jpgGet pressed do
		(
			--when button pressed, browse for JPG file
			global jpgPath = getOpenFileName types:"JPG (*.jpg)|*.jpg|JPEG (*.jpeg)"
			
			--if nothing is selected, do nothing
			if jpgPath != undefined then
			(
				jpgInput.text = jpgPath
			)
		)

		on wavGet pressed do
		(
			--when button pressed, browse for WAV file
			global wavPath = getOpenFileName types:"WAV (*.wav)|*.wav"
			
			--if nothing is selected, do nothing
			if wavPath != undefined then
			(
				wavInput.text = wavPath
			)				
			
		)
		
		on submit pressed do
		(
			
			if(jpgPath != undefined) then
				(
				
				/* -- BEGIN MAIN FUNCTIONS -- */
			
				
				if(wavPath != undefined) then
				(
					--import and add wav file
					wavsound.filename = wavPath
					animationRange = interval wavSound.start wavSound.end
				)				
		
				--define fps based radio buttons
				if(sceneFR.state == 1)then(fps = 24)
				if(sceneFR.state == 2)then(fps = 25)
				if(sceneFR.state == 3)then(fps = 30)			
				--set scene frame rate
				frameRate = fps
				
				
				--get aspect ratio of JPG file
				jpgImage = openBitMap jpgpath
				jpgWidth = jpgImage.width
				jpgHeight = jpgImage.height
				jpgAspect = float jpgWidth/jpgHeight
		
		
				--set imageplane width
				planeWidth = 0.165
				planeLength = float planeWidth/jpgAspect
			
					
				--create Plane
				myImagePlane = Plane width:planeWidth length:planeLength pos:[0,0,0]
				rotate myImagePlane (angleaxis 90 [1,0,0])
				move myImagePlane [-0.200228,0.000000,1.691464]		
				
					
				--define JPG paths/filenames
				jpgDir = getFilenamePath jpgpath
				jpgFile = getFilenameFile jpgPath
				jpgType = getFilenameType jpgPath
					
	
				--define IFL filename (to be created)
				iflName = jpgDir + jpgFile + ".ifl"
			
				--if old IFL exists, rename it
				renameFile iflName (jpgDir + jpgFile + "_old.ifl")		
		
				--create IFL with list of files from JPG directory
				 tnFiles= getfiles (jpgDir + "*.jpg")
				 FileStrm = createfile iflName
				 for i in tnFiles do
					format "%\n" i to:FileStrm
				close FileStrm
					
					
				--define material slot variable
				s = 20
					
					
				--create material
				meditMaterials[s].diffuseMap = Bitmaptexture fileName:iflName showInViewport:true
					
					
				--assign material to myImagePlane
				myImagePlane.material = meditMaterials[s]
					
					
				--turn on showInViewport
				meditMaterials[s].showInViewport = true
							 
				 
				--Set Frame Range
				sf = startFrameInput.text as integer
				ef = tnFiles.count + sf								
				animationRange = interval sf ef
				
				
				--create layer for image plane
				varImagePlaneLayer = layermanager.newLayerfromName "imagePlaneLayer"
				varImagePlaneLayer.addNode myImagePlane
				
				
				--close rollout/ui window
				destroydialog maxSceneSetupRollout
			)
		)
		
		)createDialog maxSceneSetupRollout 
	)
