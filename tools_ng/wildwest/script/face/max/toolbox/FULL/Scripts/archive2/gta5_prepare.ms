function gta5_prepare = (
	-- Prep Scene
	if $FacialAttrGUI != undefined then (
		$FacialAttrGUI.pos = ($faceControls_OFF.pos) + [.2,0,.0675]
		deletekeys $FacialAttrGUI
	)	
	-- Set up Viewport Layout
	viewport.resetAllViews()
	viewport.setLayout #layout_1
	viewport.setLayout #layout_2v
	
	-- Camera Name
	camera_name = "gta5_HeadCam"

	-- Create Camera
	viewport.activeViewport = 1
	if $gta5_HeadCam != undefined do delete $gta5_Headcam
	newCamera = freeCamera()
	newCamera.name = camera_name
	newCamera.clipManually = true
	newCamera.nearclip = 0
	rotate newCamera (eulerangles 90 0 0)
	headJointPosX = ($FACIAL_facialRoot.pos.x)
	headJointPosY = ($FACIAL_facialRoot.pos.y - .6)
	headJointPosZ = ($FACIAL_facialRoot.pos.z - 0.025)
	newCamera.pos = [headJointPosX,headJointPosY,headJointPosZ]
	viewport.setCamera newCamera
	viewport.SetTransparencyLevel 2
	actionMan.executeAction 0 "272"
	setTransformLockFlags newCamera #all
	deletekeys newCamera

	-- Active Viewport 2
	viewport.activeViewport = 2
	viewport.setType #view_front
	if ($FacialAttrGUI != undefined) then (select #($FacialAttrGUI,$faceControls_FRAME))
	--if ($IM_AltGui_text != undefined) then (select #($con_jaw_c,$con_browRaise_l,$IM_AltGui_text))
	actionMan.executeAction 0 "311"
	actionMan.executeAction 0 "311"
	actionMan.executeAction 0 "272"
	clearSelection()

	-- Setup Display settings
	hideByCategory.geometry = false
	hideByCategory.shapes = true
	hideByCategory.lights = true
	hideByCategory.helpers = true
	hideByCategory.spacewarps = true
	hideByCategory.particles = true
	hideByCategory.bones = true
	hideByCategory.shapes = true
	hideByCategory.cameras = true

	-- Turn on Auto Key
	animButtonState = true
		
	-- Positions FacialAttrGUI
	--$FacialAttrGUI.pos = [0.420079,0.00154519,1.65797]
)