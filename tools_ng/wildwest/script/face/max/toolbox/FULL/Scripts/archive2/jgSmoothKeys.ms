fn smoothKeys = 
	(
		/****************************************************************************************/
		-- Title: Smooth Keys
		-- Version: 1.0
		-- Author: Jay Grenier, 2009 
		-- Tooltip: 
		-- Runs a 'blur' effect on selected keys, to smooth out jitters in animation or mocap data.
		/****************************************************************************************/
		
		sel = getCurrentSelection()
				
		if(sel.count > 0) then
		(							
			--Find the # of selected controllers, give them an array
			cArray = selection as array
			
			--Create empty arrays for selected keys
			global arrayKY = #()
			global arrayKX = #()
			
			--Run action on each selected controller.
			for i = 1 to cArray.count do
			(	
				--Find all keys, give them an array
				xArray = cArray[i].controller[1][1][1].keys
				yArray = cArray[i].controller[1][2][1].keys

				--Put selected X keys into array
				for i = 1 to xArray.count do
				(
					if xArray[i].selected then append arrayKX xArray[i]
				)
											
				--Put selected Y keys into array	
				for i = 1 to yArray.count do
				(
					if yArray[i].selected then append arrayKY yArray[i]
				)
			)
			
			if(arrayKY.count == 0 AND arrayKX.count == 0) then
			(
				print "No keys selected.."
			)
			else
			(	
				/* arrayKY and arrayKX now hold all the selected keys, move forward.. *******/
							
				--Y KEYS
				try(for i = 2 to arrayKY.count do
				(
					
					if(arrayKY.count == 3)then
					(
						i = 1
						prevKey = arrayKY[i].value
						currentKey = arrayKY[i+1].value
						nextKey = arrayKY[i+2].value
						arrayKY[i+1].value = ((prevKey + nextKey + currentKey) /3)
						exit
					)	
					if(i == arrayKY.count)then(exit)
					prevKey = arrayKY[i-1].value
					currentKey = arrayKY[i].value
					nextKey = arrayKY[i+1].value
					
					--Get/Set average value for currentKey
					arrayKY[i].value = ((prevKey + nextKey + currentKey) /3)
				))catch()
				
				--X KEYS
				try(for i = 2 to arrayKX.count do
				(
					if(arrayKX.count == 3)then
					(
						i = 1
						prevKey = arrayKX[i].value
						currentKey = arrayKX[i+1].value
						nextKey = arrayKX[i+2].value
						arrayKX[i+1].value = ((prevKey + nextKey + currentKey) /3)
						exit
					)	
					if(i == arrayKX.count)then(exit)
					prevKey = arrayKX[i-1].value
					currentKey = arrayKX[i].value
					nextKey = arrayKX[i+1].value
					
					--Get/Set average value for currentKey
					arrayKX[i].value = ((prevKey + nextKey + currentKey) /3)
				))catch()
			)
		)
		else
		(
			print "Nothing selected.."
		)
	) --end Smooth Keys fn
