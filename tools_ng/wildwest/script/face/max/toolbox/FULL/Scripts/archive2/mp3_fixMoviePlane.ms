
/****************************************************************************************/
-- Title: fix Movie Plane (MAX PAYNE 3)
-- Version: 1.1
-- Author: Thanh Phan, 2011
-- Tooltip:
--
-- Resizes, scales, rotates and repositions the movieplane to comply with max payne 3.
-- Creates a layer and moves the movieplane onto that new layer.
--
/****************************************************************************************/

fn MP3_fixMoviePlane =
(
	-- MoviePlane attributes (MAX PAYNE 3):
	IM_MoviePlane = $FacewareMoviePlane*
	if IM_MoviePlane.count == 1 then (
		select IM_MoviePlane
		temp_Ratio = $.length/$.width
		-- the 4 possible values are: .666  .75   1.333 1.5
		-- 480x720, 480x640, 640x480, 720x480
		clearSelection()
		
		if temp_Ratio <= .7 then (
			IM_MoviePlane.width = 0.267
			IM_MoviePlane.length = 0.2
			IM_MoviePlane.pos = [-0.23614,0.132293,1.62368]
		)
		else if temp_Ratio > .7 and temp_Ratio < 1.0 then (
			IM_MoviePlane.width = 0.2
			IM_MoviePlane.length = 0.267
			IM_MoviePlane.pos = [-0.23614,0.132293,1.62368]
		)
		else if temp_Ratio >= 1.0 and temp_Ratio < 1.35 then (
			IM_MoviePlane.width = 0.2
			IM_MoviePlane.length = 0.377
			IM_MoviePlane.pos = [-0.23614,0.132293,1.62368]
		)
		else if temp_Ratio >= 1.35 then (
			IM_MoviePlane.width = 0.377
			IM_MoviePlane.length = 0.2
			IM_MoviePlane.pos = [-0.204712,0.132293,1.62368]
		)
		
		
		
		IM_MoviePlane.scale = [1,1,1]
		IM_MoviePlane.rotation.x_rotation = 90
		IM_MoviePlane.rotation.y_rotation = 0
		IM_MoviePlane.rotation.z_rotation = 0
		IM_MoviePlane_LayerName = "MoviePlane_Layer"
		
		-- Deletes animation keys from MoviePlane
		deleteKeys IM_MoviePlane

		-- If a layer for the movieplane does not exist, create it.
		if ( LayerManager.getLayerFromName IM_MoviePlane_LayerName == undefined ) then (
			layer1 = LayerManager.newLayerFromName IM_MoviePlane_LayerName
		)
		
		-- Select and place movieplanes into the movieplane layer.
		select IM_MoviePlane
		for obj in selection do (
			layer1 = LayerManager.getLayerFromName IM_MoviePlane_LayerName
			layer1.addNode obj
		)
	)
	else (
		if IM_MoviePlane.count == 0 then (
			messagebox "There is no compatible movieplane.\n\nPlease use this button to fix the movieplane outputted from the retargeter only.\nCompatible movieplanes are named: $FacewareMoviePlane*"
		)
		else (
			messagebox "There are too many movieplanes in the scene."
		)
	)
)---------------------- End MP3_fixMoviePlane