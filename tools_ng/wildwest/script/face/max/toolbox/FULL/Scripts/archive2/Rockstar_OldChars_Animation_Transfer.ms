fn runIMA_Old =
	(

	--If rollout(ui window) exists, delete it and create a new one.
	if (imaUI != undefined)
	then (destroydialog imaUI)

	--Create rollout/ui window
	rollout imaUI "IMA UI" width:100 height:95
	(

		button saveIMA "SAVE IMA" pos:[5, 5] width:90 height:40
		button getIMA "LOAD IMA" pos:[5, 50] width:90 height:40


		--FUNCTIONS ******************************************************

		fn AddKeyForController curController curLine = 
		(
			parts = filterString curLine "<>,"

			myVal = parts[3] as float
			myTime = parts[4] as float

			newKey = addnewKey curController myTime
			newKey.value = myVal
		)

		fn ExtractDataForController controllerLine in_file=
		(
			partsOfString = filterString controllerLine "<>"

			if partsOfString.count != 3 then return 0

			curObj = getNodeByname partsOfString[2]

			xKeys = curObj.pos.controller[1].controller[1].keys
			yKeys = curObj.pos.controller[2].controller[1].keys

			deleteKeys xKeys
			deleteKeys yKeys

			while not eof in_file do
			(
				curline = readline in_file

				if findString curline "<Controller>" != undefined then
				(
					--we have found the end for this controller
					exit
				)

				if findString curline "<X>" != undefined then
				(
					AddKeyForController xKeys curLine
				)

				if findString curline "<Y>" != undefined then
				(
					AddKeyForController yKeys curLine
				)
			)
		)

		fn ReadControllerValuesFromFile = 
		(
			filename = getOpenFileName caption:"Animation Script File" types:"Script Files (*.ima)|*.ima"

			if filename == undefined then return 0

			in_file = openFile filename

			while not eof in_file do
			(
				curLine = readline in_file

				if findString curline "<Controller>" != undefined then
				(
					print curline 

					ExtractDataForController curline in_file
				)

			)

			close in_file
		)

		fn WriteControllerValuesToFile writingFile conArray =
		(	
			for i = 1 to conArray.count do
			(
				cur = conArray [i]

				keysX = cur.pos.controller[1].controller[1].keys
				keysY = cur.pos.controller[2].controller[1].keys

				format ("<Controller>" + cur.name + "</Controller>\n") to:writingFile


				--Write out the X Keys
				for j = 1 to keysX.count do
				(
					myVal = keysX[j].value
					myTime = keysX[j].time

					format ("\t<X>" + (myVal as string) + "," + (myTime as string)+ "</X>\n") to:writingFile
				)

				--Write out the Y Keys
				for j = 1 to keysY.count do
				(
					myVal = keysY[j].value
					myTime = keysY[j].time

					format ("\t<Y>" + (myVal as string) + "," + (myTime as string)+ "</Y>\n") to:writingFile
				)

				format ("<Controller>$" + cur.name + "</Controller>\n") to:writingFile

			)
		)

		fn SelectAndWriteOutput = 
		(
			--select the joystick controllers

			sel = getCurrentSelection()

			filename = getSaveFileName caption:"Animation Script File" types:"Script Files (*.ima)|*.ima"

			if filename == undefined then return 0

			--we don't actually write the data out top this file we create a sub-directory of the same name
			--and put the data there.
			--This is because Max doesn't like running the large scripts that are produced for large scenes


			output_file = createFile filename

			--format ("undo off\n(\nwith redraw off\n(\nwith animate on\n(\n") to:output_file

			WriteControllerValuesToFile output_file sel

			--format (")\n)\n)\n") to:output_file

			close output_file

		)

		-- BUTTON CALLS
		on saveIMA pressed do
		(
				selectandwriteoutput();
		)

		on getIMA pressed do
		(
				readcontrollervaluesfromfile();		
		)





	)createDialog imaUI
)