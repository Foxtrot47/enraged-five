fn runIMA =
	(

	--If rollout(ui window) exists, delete it and create a new one.
	if (imaUI != undefined)
	then (destroydialog imaUI)

	--Create rollout/ui window
	rollout imaUI "IMA UI" width:100 height:95
	(

		button saveIMA "SAVE IMA" pos:[5, 5] width:90 height:40
		button getIMA "LOAD IMA" pos:[5, 50] width:90 height:40


		--FUNCTIONS ******************************************************

		fn AddKeyForController curController curLine = 
		(
			parts = filterString curLine "<>,"

			myVal = parts[3] as float
			myTime = parts[4] as float

			newKey = addnewKey curController myTime
			newKey.value = myVal
		)

		fn ExtractDataForController controllerLine in_file=
		(
			partsOfString = filterString controllerLine "<>"

			if partsOfString.count != 3 then return 0

			curObj = getNodeByname partsOfString[2]

			xKeys = curObj.pos.controller[1].controller[1].keys
			yKeys = curObj.pos.controller[2].controller[1].keys

			deleteKeys xKeys
			deleteKeys yKeys

			while not eof in_file do
			(
				curline = readline in_file

				if findString curline "<Controller>" != undefined then
				(
					--we have found the end for this controller
					exit
				)

				if findString curline "<X>" != undefined then
				(
					AddKeyForController xKeys curLine
				)

				if findString curline "<Y>" != undefined then
				(
					AddKeyForController yKeys curLine
				)
			)
		)

		fn ReadControllerValuesFromFile = 
		(
			filename = getOpenFileName caption:"Animation Script File" types:"Script Files (*.ima)|*.ima"

			if filename == undefined then return 0

			in_file = openFile filename

			while not eof in_file do
			(
				curLine = readline in_file

				if findString curline "<Controller>" != undefined then
				(
					print curline 

					ExtractDataForController curline in_file
				)

			)

			close in_file
		)

		fn WriteControllerValuesToFile writingFile conArray =
		(	
			for i = 1 to conArray.count do
			(
				cur = conArray [i]

				keysX = cur.pos.controller[1].controller[1].keys
				keysY = cur.pos.controller[2].controller[1].keys

				format ("<Controller>" + cur.name + "</Controller>\n") to:writingFile


				--Write out the X Keys
				for j = 1 to keysX.count do
				(
					myVal = keysX[j].value
					myTime = keysX[j].time

					format ("\t<X>" + (myVal as string) + "," + (myTime as string)+ "</X>\n") to:writingFile
				)

				--Write out the Y Keys
				for j = 1 to keysY.count do
				(
					myVal = keysY[j].value
					myTime = keysY[j].time

					format ("\t<Y>" + (myVal as string) + "," + (myTime as string)+ "</Y>\n") to:writingFile
				)

				format ("<Controller>$" + cur.name + "</Controller>\n") to:writingFile

			)
		)

		fn SelectAndWriteOutput = 
		(
			--select the joystick controllers

			sel = getCurrentSelection()

			filename = getSaveFileName caption:"Animation Script File" types:"Script Files (*.ima)|*.ima"

			if filename == undefined then return 0

			--we don't actually write the data out top this file we create a sub-directory of the same name
			--and put the data there.
			--This is because Max doesn't like running the large scripts that are produced for large scenes


			output_file = createFile filename

			--format ("undo off\n(\nwith redraw off\n(\nwith animate on\n(\n") to:output_file

			WriteControllerValuesToFile output_file sel

			--format (")\n)\n)\n") to:output_file

			close output_file

		)

		-- BUTTON CALLS
		on saveIMA pressed do
		(
			if($Sticky_Left_Circle == undefined) then (
				try (select #($Upper_Lid_Outer_Squint_Circle, $Lower_Lid_Outer_Squint_Circle, $Cheeks_Circle, $Right_Brow_Circle, $Brows_Mid_Circle, $Left_Brow_Circle, $Eyes_Circle, $R_Upper_Eyelid_Circle, $R_Lower_Eyelid_Circle, $Blink_Right_Circle, $Blink_Left_Circle, $L_Upper_Eyelid_Circle, $L_Lower_Eyelid_Circle, $R_Mouth_Corner_Circle, $U_Lip_Right_Circle, $U_Lip_Mid_Circle, $U_Lip_Left_Circle, $L_Mouth_Corner_Circle, $L_Lip_Left_Circle, $L_Lip_Mid_Circle, $L_Lip_Right_Circle, $U_Lip_Roll_Circle, $L_Lip_Roll_Circle, $Mouth_LR_Circle, $Lower_L_Teeth_Revealer_Circle, $Lower_R_Teeth_Revealer_Circle, $Lower_Lip_Circle, $Upper_Lip_Circle, $Tongue_Circle, $Tongue_Roll_Circle, $Tongue_Out_Circle, $Tongue_Pallete_Circle, $L_Blow_Suck_Circle, $R_Blow_Suck_Circle, $R_Mouth_Width_Circle, $L_Mouth_Width_Circle, $L_Smile_Frown_Circle, $R_Smile_Frown_Circle, $OH_Circle, $CH_Circle, $Normal_FV_Circle, $Strong_FV_Circle, $MBP_Circle, $Extreme_Narrow_Circle, $Jaw_Circle, $Jaw_In_Out_Circle))catch()
			)
			else (
				try(select #($Upper_Lid_Outer_Squint_Circle, $Lower_Lid_Outer_Squint_Circle, $Cheeks_Circle, $Brows_Mid_Circle, $Right_Brow_Circle, $Left_Brow_Circle, $Nostrils_Flare_Circle, $Sneer_Circle, $Nose_Stretch_Circle, $Blink_R_Circle, $Eyes_Circle, $Blink_L_Circle, $L_Upper_Eyelid_Circle, $Lower_L_Eyelid_Circle, $R_Upper_Eyelid_Circle, $Lower_R_Eyelid_Circle, $U_Lip_Right_Circle, $L_Lip_Right_Circle, $L_Lip_Mid_Circle, $U_Lip_Mid_Circle, $U_Lip_Left_Circle, $L_Lip_Left_Circle, $L_Mouth_Corner_Circle, $R_Mouth_Corner_Circle, $U_Lip_Roll_Circle, $U_Lip_Thin_Thick_Circle, $Tongue_Out_Circle, $Tongue_Roll_Circle, $Tongue_Circle, $Tongue_Pallete_Circle, $Lower_Right_Teeth_Revealer_Circle, $L_Lip_Roll_Circle, $Mouth_LR_Circle, $L_Lip_Thin_Thick_Circle, $Lower_Left_Teeth_Revealer_Circle, $Lower_Lip_Circle, $Upper_Lip_Circle, $OH_Circle, $CH_Circle, $Normal_FV_Circle, $Strong_FV_Circle, $MBP_Circle, $Chin_Wrinkle_Circle, $Extreme_Narrow_Circle, $Smile_Deepender_Circle, $L_Blow_Suck_Circle, $L_Mouth_Width_Circle, $L_Smile_Frown_Circle, $Sticky_Left_Circle, $R_Blow_Suck_Circle, $R_Mouth_Width_Circle, $R_Smile_Frown_Circle, $Sticky_Right_Circle, $Jaw_In_Out_Circle, $Jaw_Circle, $Jaw_Tighten_Circle, $Neck_Strain_Circle))catch()
			)

			if($ != undefined) then (			
				selectandwriteoutput();
				print "IMA File Successfully Exported"
			)

			if($ == undefined) then (
				print "ERROR: NOTHING EXPORTED!"
			)	

			clearSelection()

		)

		on getIMA pressed do
		(
			if($Sticky_Left_Circle == undefined) then (
				try (select #($Upper_Lid_Outer_Squint_Circle, $Lower_Lid_Outer_Squint_Circle, $Cheeks_Circle, $Right_Brow_Circle, $Brows_Mid_Circle, $Left_Brow_Circle, $Eyes_Circle, $R_Upper_Eyelid_Circle, $R_Lower_Eyelid_Circle, $Blink_Right_Circle, $Blink_Left_Circle, $L_Upper_Eyelid_Circle, $L_Lower_Eyelid_Circle, $R_Mouth_Corner_Circle, $U_Lip_Right_Circle, $U_Lip_Mid_Circle, $U_Lip_Left_Circle, $L_Mouth_Corner_Circle, $L_Lip_Left_Circle, $L_Lip_Mid_Circle, $L_Lip_Right_Circle, $U_Lip_Roll_Circle, $L_Lip_Roll_Circle, $Mouth_LR_Circle, $Lower_L_Teeth_Revealer_Circle, $Lower_R_Teeth_Revealer_Circle, $Lower_Lip_Circle, $Upper_Lip_Circle, $Tongue_Circle, $Tongue_Roll_Circle, $Tongue_Out_Circle, $Tongue_Pallete_Circle, $L_Blow_Suck_Circle, $R_Blow_Suck_Circle, $R_Mouth_Width_Circle, $L_Mouth_Width_Circle, $L_Smile_Frown_Circle, $R_Smile_Frown_Circle, $OH_Circle, $CH_Circle, $Normal_FV_Circle, $Strong_FV_Circle, $MBP_Circle, $Extreme_Narrow_Circle, $Jaw_Circle, $Jaw_In_Out_Circle))catch()
			)
			else (
				try (select #($Upper_Lid_Outer_Squint_Circle, $Lower_Lid_Outer_Squint_Circle, $Cheeks_Circle, $Brows_Mid_Circle, $Right_Brow_Circle, $Left_Brow_Circle, $Nostrils_Flare_Circle, $Sneer_Circle, $Nose_Stretch_Circle, $Blink_R_Circle, $Eyes_Circle, $Blink_L_Circle, $L_Upper_Eyelid_Circle, $Lower_L_Eyelid_Circle, $R_Upper_Eyelid_Circle, $Lower_R_Eyelid_Circle, $U_Lip_Right_Circle, $L_Lip_Right_Circle, $L_Lip_Mid_Circle, $U_Lip_Mid_Circle, $U_Lip_Left_Circle, $L_Lip_Left_Circle, $L_Mouth_Corner_Circle, $R_Mouth_Corner_Circle, $U_Lip_Roll_Circle, $U_Lip_Thin_Thick_Circle, $Tongue_Out_Circle, $Tongue_Roll_Circle, $Tongue_Circle, $Tongue_Pallete_Circle, $Lower_Right_Teeth_Revealer_Circle, $L_Lip_Roll_Circle, $Mouth_LR_Circle, $L_Lip_Thin_Thick_Circle, $Lower_Left_Teeth_Revealer_Circle, $Lower_Lip_Circle, $Upper_Lip_Circle, $OH_Circle, $CH_Circle, $Normal_FV_Circle, $Strong_FV_Circle, $MBP_Circle, $Chin_Wrinkle_Circle, $Extreme_Narrow_Circle, $Smile_Deepender_Circle, $L_Blow_Suck_Circle, $L_Mouth_Width_Circle, $L_Smile_Frown_Circle, $Sticky_Left_Circle, $R_Blow_Suck_Circle, $R_Mouth_Width_Circle, $R_Smile_Frown_Circle, $Sticky_Right_Circle, $Jaw_In_Out_Circle, $Jaw_Circle, $Jaw_Tighten_Circle, $Neck_Strain_Circle))catch()
			)

			if($ != undefined) then (	
				readcontrollervaluesfromfile();
				print "IMA File Successfully Imported"
			)

			if($ == undefined) then (
				print "ERROR: NOTHING IMPORTED!"
			)	

			clearSelection()

		)





	)createDialog imaUI
)