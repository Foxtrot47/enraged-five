
rollout IM_MP3_instantTongueGUI "MP3 Instant Tongue" width: 229 height:99 (
	group "Instant Tongue" (
		edittext easeInEt "Ease In" text:"3" fieldWidth:44 labelOnTop:true pos:[10,26]
		edittext holdEt "Hold" text:"1" fieldWidth:44 labelOnTop:true  pos:[65,26]
		edittext easeOutEt "Ease Out" text:"3" fieldWidth:44 labelOnTop:true  pos:[120,26]
		edittext tongueValueEt "Value" text:".4" fieldWidth:44 labelOnTop:true pos:[175,26]
		button instantTongueBtn "Flick Tongue" toolTip:"Make Instant Blink Using GUI Settings" width:208 pos:[10,68]
		--button easeInBtn "Ease In" toopTip:"Make an ease in from current frame" width:75 pos:[10,94]
		--button easeOutBtn "Ease Out" toopTip:"Make an ease out from current frame" width:74 pos:[90,94]
	)
	
	-- GUI FUNCTIONS
	fn IM_MP3_instantTongueDo = (
		-- Set Blink Control Vars (can have multiple)
		tongue_ctrls = #($tongueMove_CTRL.controller[1][2][2].controller)
		
		-- Data Vars
		tongue_value = tongueValueEt.text as float
		ease_in = easeInEt.text as integer
		ease_out = easeOutEt.text as integer
		hold = holdEt.text as integer
		
		-- Keyframe Vars
		frame = currentTime
		start_frame = frame - ease_in
		tongue_frame = frame
		end_tongue_frame = (frame + (hold - 1))
		end_frame = (frame + (hold - 1)) + ease_out
		
		-- Set Autokey
		autoKeyState = animButtonState
		animButtonState = true 
			
		-- Make Keyframes
		for i = 1 to tongue_ctrls.count do (
			
			-- Delete Current Keys
			for j = start_frame to end_frame do (
				
				key_index = getKeyIndex tongue_ctrls[i] j
				if(key_index != 0) then deleteKey tongue_ctrls[i] key_index
				
			)
			
			-- Start Frame
			at time start_frame tongue_ctrls[i].value = 0
			addNewKey tongue_ctrls[i] start_frame
			
			-- Blink Frame
			at time tongue_frame tongue_ctrls[i].value = tongue_value
			addNewKey tongue_ctrls[i] tongue_frame
			
			-- Hold Frame
			at time end_tongue_frame tongue_ctrls[i].value = tongue_value
			addNewKey tongue_ctrls[i] end_tongue_frame
			
			-- End Frame
			at time end_frame tongue_ctrls[i].value = 0
			addNewKey tongue_ctrls[i] end_frame
			
		)
		
		-- Set Autokey to what it was
		animButtonState = autoKeyState
		
	)
	
	on instantTongueBtn pressed do (
		
		with undo on (
			IM_MP3_instantTongueDo()
		)
		
	)
)
function gta5_tongueFlicker = (
	if (IM_MP3_instantTongueGUI != undefined) then (destroyDialog IM_MP3_instantTongueGUI)
	global IM_MP3_instantTongueGUI
	createDialog IM_MP3_instantTongueGUI
)