/*----------------------------------------------------------- *\
| -- Title: MP3 Animation Exit Script
| -- Author: Jay Grenier, Jonah Goodman 2010
| -- Contact: jay.grenier@image-metrics.com
| -- Version: 0.9
| -- Last Update: June 24, 2010                                 
\*----------------------------------------------------------- */

rollout IM_MP3_animationExitScript "Animation Transfer" width:300 (

	-- Start Button
	button startAnimTransferBtn "Process Animation And Remove Alternate GUI" width:240 

	radiobuttons shot_type labels:#("Headstart Shot     ", "Pro Shot")

	-- Progress Bar
	progressbar animTransfer_prog color:blue

	-- Go!
	on startAnimTransferBtn pressed do (

		-- Save File
		max file save
		
		/*** MASTER LIST OF CONTROLS ***/
		allSource = #(
			$IM_OpenShutR_Circle.controller[1][2], -- 0
			$IM_OpenShutL_Circle.controller[1][2], -- 1
			
			$IM_EyeSquintInnerR_circle.controller[1][2], -- 2
			$IM_EyeSquintInnerL_circle.controller[1][2], -- 3
			
			$IM_SneerR_circle.controller[1][2], -- 4
			$IM_SneerL_circle.controller[1][2], -- 5
			
			$IM_DilateContract_circle.controller[1][2], -- 6
			
			$IM_FrownSmileR_circle.controller[1][2], -- 7
			$IM_FrownSmileL_circle.controller[1][2], -- 8
			
			$IM_OpenSmileR_circle.controller[1][2], -- 9
			$IM_OpenSmileL_circle.controller[1][2], -- 10

			$IM_Tongue_Out_circle.controller[1][2], -- ( 11) 
			$IM_TongueLRU_circle.controller[1][2], -- (12)
				
			$IM_LipsCloseOuterR_circle.controller[1][2], -- 13
			$IM_LipsCloseOuterL_circle.controller[1][2], -- 14

			$IM_SuckPuffR_circle.controller[1][2], -- 15
			$IM_SuckPuffL_circle.controller[1][2], -- 16


			$IM_CheekSquintR_circle.controller[1][2], -- 17
			$IM_CheekSquintL_circle.controller[1][2], -- 18

			$IM_BrowSqueezeR_circle.controller[1][2], -- 19
			$IM_BrowSqueezeL_circle.controller[1][2], -- 20

			$IM_Funnel_DL_circle.controller[1][2],  -- 21
			$IM_Funnel_DR_circle.controller[1][2], -- 22


			$IM_UpperCH_circle.controller[1][2], -- 23
			$IM_LowerCH_circle.controller[1][2],  -- 24

			$IM_MouthSuck_DL_circle.controller[1][2], -- 25
			$IM_MouthSuck_DR_circle.controller[1][2], -- 26

			$IM_pressL_circle.controller[1][2], -- 27
			$IM_pressR_circle.controller[1][2], -- 28


			$IM_lipRollUL_circle.controller[1][2], -- 29
			$IM_lipRollUC_circle.controller[1][2], -- 30
			$IM_lipRollUR_circle.controller[1][2], -- 31
			$IM_lipRollDL_circle.controller[1][2], -- 32
			$IM_lipRollDC_circle.controller[1][2],  -- 33
			$IM_lipRollDR_circle.controller[1][2],  -- 34

			$IM_Funnel_UL_circle.controller[1][2], -- 35
			
			$IM_Oh_circle.controller[1][2], -- 36			
			
			$IM_Funnel_UR_circle.controller[1][2], -- 37

			$IM_MouthSuck_UR_circle.controller[1][2], -- 38
			$IM_MouthSuck_UL_circle.controller[1][2], -- 39

			$IM_Tongue_WNP_circle.controller[1][2],  -- 40

			$IM_TongueLRU_circle.controller[1][1],  -- 41

			$IM_PuckerL_circle.controller[1][2],  -- 42
			$IM_PuckerR_circle.controller[1][2]  -- 43			
			
	)

		allDest = #(
			$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1], -- 0
			$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1], -- 1
			
			$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[2], -- 2
			$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[2], -- 3
			
			$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[3], -- 4
			$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[2], -- 5
			
			$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1], -- 6
			
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[2], -- 7
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1], -- 8
			
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[4], -- 9
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[3], -- 10
			
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[23], -- 11
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[22], -- 12
			
			$con_lipStretch_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1], -- 13
			$con_lipStretch_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1], -- 14
			
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[21], -- 15
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[20], -- 16

			$con_eyeBall_r.modifiers[#Attribute_Holder].IM_ATTRS_A[3], -- 17
			$con_eyeBall_l.modifiers[#Attribute_Holder].IM_ATTRS_A[3], -- 18

			$con_browRaise_cr.modifiers[#Attribute_Holder].IM_ATTRS_A[1], -- 19
			$con_browRaise_cl.modifiers[#Attribute_Holder].IM_ATTRS_A[1], -- 20

			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[10], -- 21
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[11], -- 22
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[12], -- 23
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[13], -- 24

			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[16], -- 25
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[17], -- 26
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[18], -- 27
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[19], -- 28

			$con_upperLipRaise_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1], -- 31
			$con_upperLipRaise_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1], -- 32
			$con_upperLipRaise_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1], -- 33
			$con_lowerLipDepress_l.modifiers[#Attribute_Holder].IM_ATTRS_A[1], -- 34
			$con_lowerLipDepress_c.modifiers[#Attribute_Holder].IM_ATTRS_A[1], -- 35
			$con_lowerLipDepress_r.modifiers[#Attribute_Holder].IM_ATTRS_A[1], -- 36

			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[8], -- 37
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[7], -- 38

			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[9], -- 39

			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[15], -- 40
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[14], -- 41

			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[24], -- 43

			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[25], -- 44

			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[5], -- 45
			$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A[6] -- 46

		)
		
		for a = 1 to allSource.count do (	
			
			print (exprformaxobject allSource[a])
			print allDest[a]
			print ""
			
			-- Checks
			if(allSource[a] == "NULL") then continue
			if(allDest[a] == undefined) then continue
			
			-- Update Progress Bar
			animTransfer_prog.value = 100.*a/allSource.count
			
			/*** GET DATA FROM JOYSTICK ***/		
				-- Get All Keyframes On This Control	
				allKeyframes = #()
				allKeyframes = allSource[a].keys

				-- All Values (One For Each Keyframe)
				allValues = #()
				
				-- Get Key Values Into Array
				for i = 1 to allKeyframes.count do (
					append allValues allSource[a].keys[i].value
				)			

			/*** BREAK CONNECTION ***/
				-- Disconnect Wire Param
				--print ("Disconnecting: " + (a-1) as string + " - " + allDest[a] as string)
				paramWire.disconnect allDest[a]

			/*** ADD DATA TO ORIGINAL CONTROL ***/
				-- Set All Keys
				for i = 1 to allKeyframes.count do (
					addNewKey allDest[a].controller[1].controller allKeyframes[i].time
					allDest[a].controller[1].controller.keys[i].value = abs(allValues[i])
				)
				
		) -- End Ctrl Loop
		
		/*** REMOVE ALTERNATE GUI FROM SCENE ***/
			-- Select All IM_* Elements And Delete

		select $IM_*
		delete $ 
		select $instancedGeo
		delete $
		
		print "Finished transferring animation."
		destroyDialog IM_MP3_animationExitScript
			
		-- Save New File
		fileName = maxFilePath + maxFileName
		fileParts = filterString fileName "."
		shotType = shot_type.state
		if(shotType == 1) then ( newFileName = (fileParts[1]+"_HS.max"))
		if(shotType == 2) then ( newFileName = (fileParts[1]+"_Pro.max"))

		saveMaxFile newFileName
			
	)
	
)

if (IM_MP3_animationExitScript != undefined)
then (destroyDialog IM_MP3_animationExitScript)

createDialog IM_MP3_animationExitScript