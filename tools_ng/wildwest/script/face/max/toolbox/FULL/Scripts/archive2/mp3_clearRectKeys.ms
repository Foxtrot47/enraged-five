fn mp3_clearRectKeys = (

	-- Clears keys from the rectangle joystick borders.
	clearselection()
	select $IM_*_rect
	if selection.count != 0 do (
		deletekeys selection
	)
	clearselection()
	
)