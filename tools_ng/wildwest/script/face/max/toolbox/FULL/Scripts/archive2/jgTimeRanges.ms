fn timeRanges = 
(
		-- Title: JG Power Frame Ranger
		-- Version: 0.1 alpha
		-- Author: Jay Grenier, 2009 
		-- Tooltip: 
		-- This tool will allow you to place markers on certain frames allowing you to jump back and forth to sections.

		/* GLOBALS  ********************************************/
		global rangeArray = #()
		global rangeStringArray = #()
		global makeArray = #()

		/* PRE-DRAW CHECKS ***********************************/
		--If rollout(ui window) exists, delete it and create a new one.
			if (jgPFRrollout != undefined)
			then (destroydialog jgPFRrollout)

			fn loadRanges =
			(
				--open file in read mode
				openedFile = openFile fileNameQ mode:"r"
				
				--if file opened, load configuration
				if (openedFile != undefined) then
				(	
					while (not eof openedFile) do
					(
						append floatersArray (readValue openedFile)
					)
				)
			)

			fn saveRanges =
			(
				--open file in write/read mode (delete contents first)
				openedFile = openFile fileNameQ mode:"w+"

				--if file opened, save configuration
				if (openedFile != undefined) then
				(
					format "%," jgAnimToolboxFloater.size[2] to:openedFile
					format "%," jgAnimToolboxFloater.pos[1] to:openedFile
					format "%," jgAnimToolboxFloater.pos[2] to:openedFile			
					for i = 1 to jgAnimToolBoxFloater.rollouts.count do
					(
						format "%," jgAnimToolBoxFloater.rollouts[i].open to:openedFile
					)
				)
				close openedFile
			)

			rollout jgPFRrollout "jgPFR" width:119 height:179
			(		
				group "Add Range:"
				(
					--add frame button
					edittext etRange1 width:46 height:16 pos:[6,22]
					label lbl1 "to" pos:[55, 24]
					edittext etRange2 width:46 height:16 pos:[64, 22]
				
					button btnAdd "Add Range" pos:[25, 44]		
				)
				
				
				--drop down menu to show markers
				
				group "Go To Range:"
				(
					dropdownlist ddGoTo items:rangeStringArray width:94 height:18
				)

				group "Delete Range:"
				(
					--list for deletion
					dropdownlist ddDelete items:rangeStringArray width:94 height:18
				)
				
				/* START GUI FUNCTIONS **********************************/
						
				on btnAdd pressed do
				(
					--Convert to integers for checks, etc.
					a1 = etRange1.text as integer
					a2 = etRange2.text as integer
					
					--Check if both fields are filled out.
					if(etRange1.text == "" OR etRange2.text == "" OR etRange1.text == undefined OR etRange2.text == undefined OR a1 == undefined OR a2 == undefined)then(print "Please enter two numbers.")
					else
					(				
						--Check to make sure second time isn't lower than the first, if it is then swap them.
						if(a2 < a1)then
						(
							swap a1 a2
						)
						
						--Check if both values are the same, if so add one to second.
						if(a1 == a2)then
						(
							a2 += 1
						)
						
						--Check if they entered a string instead of an integer, if it's okay then proceed else throw error.
						if(a1 != undefined AND a2 != undefined)then(
						
							--Convert back to string for dropdown.
							a1string = a1 as string
							a2string = a2 as string
							makeString = a1string + "-" + a2string
							
							--make int for frame reference
							makeArray = #(a1 as integer,a2 as integer)
							
							--append both arrays
							append rangeStringArray makeString
							append rangeArray makeArray
									
							--loop and update drop downs
							for i = 1 to rangeArray.count do
							(
								ddGoTo.items = rangeStringArray
								ddDelete.items = rangeStringArray
								ddGoto.selection = i
								ddDelete.selection = 1					
							)
						)
						else
						(
							--Throws this error if they don't enter an integer or float.
							print "Please enter numbers only."
						)
					)
				)
				
				on ddGoTo selected x do
				(
					animationRange = interval rangeArray[x][1] rangeArray[x][2]
				)
				
				on ddDelete selected x do
				(
					--delete selected items from arrays
					deleteItem rangeStringArray x
					deleteItem rangeArray x
					
					--loop and update dropdowns
					for i = 1 to rangeArray.count do
					(
						ddGoTo.items = rangeStringArray
						ddDelete.items = rangeStringArray
						ddGoto.selection = 1
						ddDelete.selection = 1
					)
					
					if(rangeArray.count == 0) then
					(
						ddGoTo.items = rangeStringArray
						ddDelete.items = rangeStringArray
					)
				)
					
			)createDialog jgPFRrollout  style:#(#style_titlebar, #style_border, #style_sysmenu, #style_minimizebox) pos:[600,400] --create rollover ui
)
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		