function displayMP3 = (
	hideByCategory.geometry = false;
	hideByCategory.shapes = false;
	hideByCategory.lights = true;
	hideByCategory.cameras = true;
	hideByCategory.helpers = true;
	hideByCategory.spacewarps = true;
	hideByCategory.particles = true;
	hideByCategory.bones = true;
	animButtonState = true;
)