fn IM_MP3_instantTongue = (

	-- Delete Window If It Exists Already
	if (IM_MP3_instantTongueGUI != undefined)	then (destroyDialog IM_MP3_instantTongueGUI)
	
	-- Define Rollout And Functions
	global IM_MP3_instantTongueGUI
	rollout IM_MP3_instantTongueGUI "MP3 Instant Tongue" width:174 height:99 (

		-- GUI CONTROLS
		group "Instant Tongue" (
			edittext easeInEt "Ease In" text:"3" fieldWidth:31 labelOnTop:true pos:[10,26]
			edittext holdEt "Hold" text:"2" fieldWidth:31 labelOnTop:true  pos:[52,26]
			edittext easeOutEt "Ease Out" text:"3" fieldWidth:31 labelOnTop:true  pos:[93,26]
			edittext tongueValue_num "Value" text:".25" fieldWidth:31 labelOnTop:true pos:[134,26]
			button instantTongueBtn "Make Tongue" toolTip:"Make Instant Tongue Using GUI Settings" width:154 pos:[10,68]
			--button easeInBtn "Ease In" toopTip:"Make an ease in from current frame" width:75 pos:[10,94]
			--button easeOutBtn "Ease Out" toopTip:"Make an ease out from current frame" width:74 pos:[90,94]
		)	
		
		-- GUI FUNCTIONS
		fn IM_MP3_instantTongueDo = (

			-- Set Tongue Control Vars
			Tongue_ctrls = #($IM_TongueLRU_circle.controller[1][2][1].controller,$IM_Tongue_WNP_circle.controller[1][2][1].controller)
			
			-- Data Vars
			Tongue_value = tongueValue_num.text as float
			ease_in = easeInEt.text as integer
			ease_out = easeOutEt.text as integer
			hold = holdEt.text as integer
			
			-- Keyframe Vars
			frame = currentTime
			start_frame = frame - ease_in
			Tongue_frame = frame
			end_Tongue_frame = (frame + (hold - 1))
			end_frame = (frame + (hold - 1)) + ease_out

			-- Set Autokey
			autoKeyState = animButtonState
			animButtonState = true 
				
			-- Make Keyframes
			for i = 1 to Tongue_ctrls.count do (

				-- Delete Current Keys
				for j = start_frame to end_frame do (
					
					key_index = getKeyIndex Tongue_ctrls[i] j
					if(key_index != 0) then deleteKey Tongue_ctrls[i] key_index
					
				)
				
				-- Start Frame
				at time start_frame Tongue_ctrls[i].value = 0
				addNewKey Tongue_ctrls[i] start_frame

				-- Tongue Frame
				at time Tongue_frame Tongue_ctrls[i].value = Tongue_value
				addNewKey Tongue_ctrls[i] Tongue_frame

				-- Hold Frame
				at time end_Tongue_frame Tongue_ctrls[i].value = Tongue_value
				addNewKey Tongue_ctrls[i] end_Tongue_frame

				-- End Frame
				at time end_frame Tongue_ctrls[i].value = 0
				addNewKey Tongue_ctrls[i] end_frame
				
			)
			
			-- Set Autokey to what it was
			animButtonState = autoKeyState
			
		)		
		--
		fn IM_MP3_easeInTongueDo = (	
			
			-- Set Tongue Control Vars
			Tongue_ctrls = #($IM_TongueLRU_circle.controller[1][2][1].controller,$IM_Tongue_WNP_circle.controller[1][2][1].controller)
			
			-- Data Vars
			ease_in = easeInEt.text as integer

			-- Keyframe Vars
			frame = currentTime
			ease_in_frame = frame - ease_in
			
			-- Set Autokey
			autoKeyState = animButtonState
			animButtonState = true 
			
			-- Make Keyframes
			for i = 1 to Tongue_ctrls.count do (

				-- Delete Current Keys
				for j = ease_in_frame to (frame-1) do (
					
					key_index = getKeyIndex Tongue_ctrls[i] j
					if(key_index != 0) then deleteKey Tongue_ctrls[i] key_index
					
				)
				
				-- Ease In Frame
				at time ease_in_frame Tongue_ctrls[i].value = 0
				addNewKey Tongue_ctrls[i] ease_in_frame

			)
			
			-- Set Autokey to what it was
			animButtonState = autoKeyState
			
		)
		--
		fn IM_MP3_easeOutTongueDo = (	
			
			-- Set Tongue Control Vars
			Tongue_ctrls = #($IM_TongueLRU_circle.controller[1][2][1].controller,$IM_Tongue_WNP_circle.controller[1][2][1].controller)
			
			-- Data Vars
			ease_out = easeOutEt.text as integer

			-- Keyframe Vars
			frame = currentTime
			ease_out_frame = frame + ease_out
			
			-- Set Autokey
			autoKeyState = animButtonState
			animButtonState = true 
			
			-- Make Keyframes
			for i = 1 to Tongue_ctrls.count do (

				-- Delete Current Keys
				for j = frame+1 to (frame + ease_out_frame) do (
					
					key_index = getKeyIndex Tongue_ctrls[i] j
					if(key_index != 0) then deleteKey Tongue_ctrls[i] key_index
					
				)
				
				
				
				-- Ease In Frame
				at time ease_out_frame Tongue_ctrls[i].value = 0
				addNewKey Tongue_ctrls[i] ease_out_frame

			)
			
			-- Set Autokey to what it was
			animButtonState = autoKeyState
			
		)
		
		-- GUI ACTIONS
		on easeInBtn pressed do (
			
			with undo on (
				IM_MP3_easeInTongueDo()
			)
			
		)
		--
		on easeOutBtn pressed do (
			
			with undo on (
				IM_MP3_easeOutTongueDo()
			)
			
		)
		--
		on instantTongueBtn pressed do (
			
			with undo on (
				IM_MP3_instantTongueDo()
			)
			
		)	

		
	)	
	
	createDialog IM_MP3_instantTongueGUI
	
)