fn floatingTime =
(

	if (theTimeRollout != undefined)
	then (destroydialog theTimeRollout)
	
	rollout theTimeRollout "Floating Slider Time" width:258 height:25
	(
		spinner theStartFrame "Start: " pos:[22,4] width:60 range:[-100,50000,1] type:#integer
		edittext theTimeInput "Go To:" pos:[89,4] width:80
		spinner theEndFrame "End: " pos:[193,4] width:60 range:[-100,50000,1] type:#integer
		
		
		
		on theTimeRollout open do
		(
			theTimeInput.text = ((sliderTime as integer) / ticksperframe) as string
			theStartFrame.value = (animationRange.start as integer / ticksperframe)
			theEndFrame.value = (animationRange.end as integer / ticksperframe)
		)
		on theTimeInput entered x do
		(
			slidertime = x as integer
		)
		on theStartFrame changed x do
		(
			if (x < animationrange.end)
			then
			(
			animationRange = interval x animationrange.end
			)
			else 
			(
				theStartFrame.value = (animationRange.end as integer / ticksperframe) - 1
				animationRange = interval theStartFrame.value animationrange.end
			)
		)
		on theEndFrame changed x do
		(
			if (x > animationrange.start)
			then
			(
			animationRange = interval animationrange.start x
			)
			else 
			(
				theEndFrame.value = (animationRange.start as integer / ticksperframe) + 1
				animationRange = interval animationrange.start theEndFrame.value
			)
		)
		
	)createDialog theTimeRollout style:#(#style_toolwindow, #style_sysmenu)

)

fn timeRanges = 
(
	-- Title: JG Power Frame Ranger
	-- Version: 0.1 alpha
	-- Author: Jay Grenier, 2009 
	-- Tooltip: 
	-- This tool will allow you to place markers on certain frames allowing you to jump back and forth to sections.

	/* GLOBALS  ********************************************/
	global rangeArray = #()
	global rangeStringArray = #()
	global makeArray = #()

	/* PRE-DRAW CHECKS ***********************************/
	--If rollout(ui window) exists, delete it and create a new one.
	if (jgPFRrollout != undefined)
	then (destroydialog jgPFRrollout)

	fn loadRanges =
	(
		--open file in read mode
		openedFile = openFile fileNameQ mode:"r"
		
		--if file opened, load configuration
		if (openedFile != undefined) then
		(	
			while (not eof openedFile) do
			(
				append floatersArray (readValue openedFile)
			)
		)
	)

	fn saveRanges =
	(
		--open file in write/read mode (delete contents first)
		openedFile = openFile fileNameQ mode:"w+"

		--if file opened, save configuration
		if (openedFile != undefined) then
		(
			format "%," jgAnimToolboxFloater.size[2] to:openedFile
			format "%," jgAnimToolboxFloater.pos[1] to:openedFile
			format "%," jgAnimToolboxFloater.pos[2] to:openedFile			
			for i = 1 to jgAnimToolBoxFloater.rollouts.count do
			(
				format "%," jgAnimToolBoxFloater.rollouts[i].open to:openedFile
			)
		)
		close openedFile
	)

	rollout jgPFRrollout "jgPFR" width:119 height:179
	(		
		group "Add Range:"
		(
			--add frame button
			edittext etRange1 width:46 height:16 pos:[6,22]
			label lbl1 "to" pos:[55, 24]
			edittext etRange2 width:46 height:16 pos:[64, 22]
		
			button btnAdd "Add Range" pos:[25, 44]		
		)
		
		
		--drop down menu to show markers
		
		group "Go To Range:"
		(
			dropdownlist ddGoTo items:rangeStringArray width:94 height:18
		)

		group "Delete Range:"
		(
			--list for deletion
			dropdownlist ddDelete items:rangeStringArray width:94 height:18
		)
		
		/* START GUI FUNCTIONS **********************************/
				
		on btnAdd pressed do
		(
			--Convert to integers for checks, etc.
			a1 = etRange1.text as integer
			a2 = etRange2.text as integer
			
			--Check if both fields are filled out.
			if(etRange1.text == "" OR etRange2.text == "" OR etRange1.text == undefined OR etRange2.text == undefined OR a1 == undefined OR a2 == undefined)then(print "Please enter two numbers.")
			else
			(				
				--Check to make sure second time isn't lower than the first, if it is then swap them.
				if(a2 < a1)then
				(
					swap a1 a2
				)
				
				--Check if both values are the same, if so add one to second.
				if(a1 == a2)then
				(
					a2 += 1
				)
				
				--Check if they entered a string instead of an integer, if it's okay then proceed else throw error.
				if(a1 != undefined AND a2 != undefined)then(
				
					--Convert back to string for dropdown.
					a1string = a1 as string
					a2string = a2 as string
					makeString = a1string + "-" + a2string
					
					--make int for frame reference
					makeArray = #(a1 as integer,a2 as integer)
					
					--append both arrays
					append rangeStringArray makeString
					append rangeArray makeArray
							
					--loop and update drop downs
					for i = 1 to rangeArray.count do
					(
						ddGoTo.items = rangeStringArray
						ddDelete.items = rangeStringArray
						ddGoto.selection = i
						ddDelete.selection = 1					
					)
				)
				else
				(
					--Throws this error if they don't enter an integer or float.
					print "Please enter numbers only."
				)
			)
		)
		
		on ddGoTo selected x do
		(
			animationRange = interval rangeArray[x][1] rangeArray[x][2]
		)
		
		on ddDelete selected x do
		(
			--delete selected items from arrays
			deleteItem rangeStringArray x
			deleteItem rangeArray x
			
			--loop and update dropdowns
			for i = 1 to rangeArray.count do
			(
				ddGoTo.items = rangeStringArray
				ddDelete.items = rangeStringArray
				ddGoto.selection = 1
				ddDelete.selection = 1
			)
			
			if(rangeArray.count == 0) then
			(
				ddGoTo.items = rangeStringArray
				ddDelete.items = rangeStringArray
			)
		)
			
	)
	createDialog jgPFRrollout  style:#(#style_titlebar, #style_border, #style_sysmenu, #style_minimizebox) pos:[600,400] --create rollover ui
)

function mp3TrackviewFilters = (
	view = trackviews.currentTrackView
	if view != undefined then (
		trackviews.current.clearFilter #all
		trackviews.current.setFilter #hierarchy #objects #spacewarpBindings #transforms #position #rotation #curveX #curveY #curveZ #modifiedObjects #baseObjects #materialsMaps #materialParameters #animatedTracks #selectedObjects #positionX #positionY #positionZ #positionW #rotationX #rotationY #rotationZ #scaleX #scaleY #scaleZ #red #green #blue #alpha #keyableTracks
	)
	else
		print "The Trackview needs to be opened for this button to work."
)

rollout IM_MP3_instantTongueGUI "MP3 Instant Tongue" width: 229 height:99 (
	group "Instant Tongue" (
		edittext easeInEt "Ease In" text:"3" fieldWidth:44 labelOnTop:true pos:[10,26]
		edittext holdEt "Hold" text:"1" fieldWidth:44 labelOnTop:true  pos:[65,26]
		edittext easeOutEt "Ease Out" text:"3" fieldWidth:44 labelOnTop:true  pos:[120,26]
		edittext tongueValueEt "Value" text:".4" fieldWidth:44 labelOnTop:true pos:[175,26]
		button instantTongueBtn "Flick Tongue" toolTip:"Make Instant Blink Using GUI Settings" width:208 pos:[10,68]
		--button easeInBtn "Ease In" toopTip:"Make an ease in from current frame" width:75 pos:[10,94]
		--button easeOutBtn "Ease Out" toopTip:"Make an ease out from current frame" width:74 pos:[90,94]
	)
	
	
	-- GUI FUNCTIONS
	fn IM_MP3_instantTongueDo = (
		-- Set Blink Control Vars (can have multiple)
		--tongue_ctrls = #($tongueMove_CTRL.controller[1][2][2].controller)
		tongue_ctrls = #()
	
		tongueGroupY = #(
			$tongueInOut_CTRL,
			$tongueMove_CTRL,
			$tongueRoll_CTRL
		)
		
		tongueGroupX = #(
			$CIRC_narrowWide,
			$CIRC_press,
			$CIRC_rollIn
		)
		
		for each in selection do (
			for i = 1 to tongueGroupY.count do (
				if each == tongueGroupY[i] then (
					append tongue_ctrls each.controller[1][2][2].controller
				)
			)
			for i = 1 to tongueGroupX.count do (
				if each == tongueGroupX[i] then (
					append tongue_ctrls each.controller[1][2][1].controller
				)
			)
		)
		
		-- Data Vars
		tongue_value = tongueValueEt.text as float
		ease_in = easeInEt.text as integer
		ease_out = easeOutEt.text as integer
		hold = holdEt.text as integer
		
		-- Keyframe Vars
		frame = currentTime
		start_frame = frame - ease_in
		tongue_frame = frame
		end_tongue_frame = (frame + (hold - 1))
		end_frame = (frame + (hold - 1)) + ease_out
		
		-- Set Autokey
		autoKeyState = animButtonState
		animButtonState = true 
			
		-- Make Keyframes
		for i = 1 to tongue_ctrls.count do (
			
			-- Delete Current Keys
			for j = start_frame to end_frame do (
				
				key_index = getKeyIndex tongue_ctrls[i] j
				if(key_index != 0) then deleteKey tongue_ctrls[i] key_index
				
			)
			
			-- Start Frame
			at time start_frame tongue_ctrls[i].value = 0
			addNewKey tongue_ctrls[i] start_frame
			
			-- Blink Frame
			at time tongue_frame tongue_ctrls[i].value = tongue_value
			addNewKey tongue_ctrls[i] tongue_frame
			
			-- Hold Frame
			at time end_tongue_frame tongue_ctrls[i].value = tongue_value
			addNewKey tongue_ctrls[i] end_tongue_frame
			
			-- End Frame
			at time end_frame tongue_ctrls[i].value = 0
			addNewKey tongue_ctrls[i] end_frame
			
		)
		
		-- Set Autokey to what it was
		animButtonState = autoKeyState
		
	)
	
	on instantTongueBtn pressed do (
		
		with undo on (
			IM_MP3_instantTongueDo()
		)
		
	)
)

function gta5_tongueFlicker = (
	if (IM_MP3_instantTongueGUI != undefined) then (destroyDialog IM_MP3_instantTongueGUI)
	global IM_MP3_instantTongueGUI
	createDialog IM_MP3_instantTongueGUI
)

function gta5_makePreview = (
	
	viewportbuttonMgr.EnableButtons = false

	tempHiddenCheck = false
	-- If the face circles are showing, hide them.
	if $upperLip_R_CTRL.ishidden == false do (
		tempHiddenCheck = true
		gta5_showHideFaceCirc()
	)
	
	-- Hide the Main GUI
	for i in $faceControls_Off...* do (
		i.ishidden = true
	)
	
	max preview
	
	
	-- Show the Main GUI
	for i in $faceControls_Off...* do (
		i.ishidden = false
	)
	viewportbuttonMgr.EnableButtons = true
	--If the face circles were hid earlier, reveal them
	if tempHiddenCheck do (
		gta5_showHideFaceCirc()
	)	
)

function gta5_makePreview_side = (
	
	viewportbuttonMgr.EnableButtons = false
	
	tempParent = undefined
	select $head_00???
	selectmore $teef_00???
	
	
	if selection.count == 2 do (
		if selection[2].parent != undefined then tempParent = selection[2].parent
		if selection[1].parent != undefined then tempParent = selection[1].parent
		
		if tempParent == undefined then (
			delete $headteef_geo*
			tempParent = dummy pos:[0,0,0]
			tempParent.name = "headteef_geo"
		)
		
		for i in selection do i.parent = tempParent
		newParent_TEMP = reference selection[1].parent
		newParent_TEMP.name = "instanced_geo"
		instancedGeo1_TEMP = reference selection[1]
		instancedGeo2_TEMP = reference selection[2]
		instancedGeo1_TEMP.parent = newParent_TEMP
		instancedGeo2_TEMP.parent = newParent_TEMP
		for i in #(instancedGeo1_TEMP, instancedGeo2_TEMP) do (
			i.showFrozenInGray = false
			i.showVertexColors = false
			i.displayByLayer = false
		)
		select newParent_TEMP
		move $ [0.183374,-0.04,0]
		currentMatrix = $.transform
		preRotate currentMatrix (eulertoquat (eulerAngles 0 0 -45))
		$.transform = currentMatrix
		deletekeys newParent_TEMP
	)
	
	tempHiddenCheck = false
	-- If the face circles are showing, hide them.
	if $upperLip_R_CTRL.ishidden == false do (
		tempHiddenCheck = true
		gta5_showHideFaceCirc()
	)
		
	-- Setup Display settings
	hideByCategory.geometry = false
	hideByCategory.shapes = true
	hideByCategory.lights = true
	hideByCategory.helpers = true
	hideByCategory.spacewarps = true
	hideByCategory.particles = true
	hideByCategory.bones = true
	hideByCategory.shapes = true
	hideByCategory.cameras = true
	
	max preview
	
	viewportbuttonMgr.EnableButtons = true
	delete newParent_TEMP
	delete instancedGeo1_TEMP
	delete instancedGeo2_TEMP
	delete $instanced_geo*
	delete $head_???_??*
	delete $teef_???_??*
	
	--If the face circles were hid earlier, reveal them
	if tempHiddenCheck do (
		gta5_showHideFaceCirc()
	)
	
	-- Setup Display settings
	hideByCategory.geometry = false
	hideByCategory.shapes = true
	hideByCategory.lights = true
	hideByCategory.helpers = true
	hideByCategory.spacewarps = true
	hideByCategory.particles = true
	hideByCategory.bones = true
	hideByCategory.shapes = true
	hideByCategory.cameras = true
	
)



fn MP3_fixMoviePlane =
(
	
/****************************************************************************************/
-- Title: fix Movie Plane (MAX PAYNE 3)
-- Version: 1.1
-- Author: Thanh Phan, 2011
-- Tooltip:
--
-- Resizes, scales, rotates and repositions the movieplane to comply with max payne 3.
-- Creates a layer and moves the movieplane onto that new layer.
--
/****************************************************************************************/
	
	
	
	-- MoviePlane attributes (MAX PAYNE 3):
	IM_MoviePlane = $FacewareMoviePlane*
	if IM_MoviePlane.count == 1 then (
		select IM_MoviePlane
		temp_Ratio = $.length/$.width
		-- the 4 possible values are: .666  .75   1.333 1.5
		-- 480x720, 480x640, 640x480, 720x480
		clearSelection()
		
		if temp_Ratio <= .7 then (
			IM_MoviePlane.width = 0.267
			IM_MoviePlane.length = 0.2
			IM_MoviePlane.pos = [-0.23614,0.132293,1.62368]
		)
		else if temp_Ratio > .7 and temp_Ratio < 1.0 then (
			IM_MoviePlane.width = 0.2
			IM_MoviePlane.length = 0.267
			IM_MoviePlane.pos = [-0.23614,0.132293,1.62368]
		)
		else if temp_Ratio >= 1.0 and temp_Ratio < 1.35 then (
			IM_MoviePlane.width = 0.2
			IM_MoviePlane.length = 0.377
			IM_MoviePlane.pos = [-0.23614,0.132293,1.62368]
		)
		else if temp_Ratio >= 1.35 then (
			IM_MoviePlane.width = 0.377
			IM_MoviePlane.length = 0.2
			IM_MoviePlane.pos = [-0.204712,0.132293,1.62368]
		)
		
		
		
		IM_MoviePlane.scale = [1,1,1]
		IM_MoviePlane.rotation.x_rotation = 90
		IM_MoviePlane.rotation.y_rotation = 0
		IM_MoviePlane.rotation.z_rotation = 0
		IM_MoviePlane_LayerName = "MoviePlane_Layer"
		
		-- Deletes animation keys from MoviePlane
		deleteKeys IM_MoviePlane

		-- If a layer for the movieplane does not exist, create it.
		if ( LayerManager.getLayerFromName IM_MoviePlane_LayerName == undefined ) then (
			layer1 = LayerManager.newLayerFromName IM_MoviePlane_LayerName
		)
		
		-- Select and place movieplanes into the movieplane layer.
		select IM_MoviePlane
		for obj in selection do (
			layer1 = LayerManager.getLayerFromName IM_MoviePlane_LayerName
			layer1.addNode obj
		)
	)
	else (
		if IM_MoviePlane.count == 0 then (
			messagebox "There is no compatible movieplane.\n\nPlease use this button to fix the movieplane outputted from the retargeter only.\nCompatible movieplanes are named: $FacewareMoviePlane*"
		)
		else (
			messagebox "There are too many movieplanes in the scene."
		)
	)
)---------------------- End MP3_fixMoviePlane

function gta5_showHideFaceCirc = (

	face_Circles = #(
		$upperLip_R_CTRL,
		$upperLip_C_CTRL,
		$upperLip_L_CTRL,
		$lowerLip_R_CTRL,
		$lowerLip_C_CTRL,
		$lowerLip_L_CTRL,
		$lipCorner_R_CTRL,
		$lipCorner_L_CTRL,
		$eyelidUpperOuter_R_CTRL,
		$eyelidUpperInner_R_CTRL,
		$eyelidLowerOuter_R_CTRL,
		$eyelidLowerInner_R_CTRL,
		$eyelidUpperOuter_L_CTRL,
		$eyelidUpperInner_L_CTRL,
		$eyelidLowerOuter_L_CTRL,
		$eyelidLowerInner_L_CTRL
	)

	if face_Circles[1].ishidden == true then (
		for i in face_Circles do (
			if i != undefined do (
				i.ishidden = false
			)
		)
	)
	else (
		for i in face_Circles do (
			if i != undefined do (
				i.ishidden = true
			)
		)
	)
	
)


function gta5_createCamera camera_name ForS = (

	if (execute("$" + camera_name )) != undefined do ( delete( execute("$" + camera_name )))
	
	-- Create Camera
	newCamera = freeCamera()
	newCamera.name = camera_name
	newCamera.clipManually = true
	newCamera.nearclip = 0
	if ForS == 1 then (
		rotate newCamera (eulerangles 90 0 0)
		headJointPosX = ($FACIAL_facialRoot.pos.x)
		headJointPosY = ($FACIAL_facialRoot.pos.y - .65)
		headJointPosZ = ($FACIAL_facialRoot.pos.z - 0.025)
		newCamera.pos = [headJointPosX,headJointPosY,headJointPosZ]
	)
	else (
		rotate newCamera (eulerangles 90 0 0)
		headJointPosX = ($FACIAL_facialRoot.pos.x - .4)
		headJointPosY = ($FACIAL_facialRoot.pos.y - .4)
		headJointPosZ = ($FACIAL_facialRoot.pos.z - 0.025)
		rotate newCamera (eulerangles 0 0 -45)
		newCamera.pos = [headJointPosX,headJointPosY,headJointPosZ]
	)
	viewport.setCamera newCamera
	viewport.SetTransparencyLevel 2
	if viewport.isWire() then ( actionMan.executeAction 0 "272" )
	setTransformLockFlags newCamera #all
	deletekeys newCamera
	if $LIGHTRIG_MASTER != undefined then (
		newCamera.parent = $LIGHTRIG_MASTER
	)
	else (
		dummyObj = dummy()
		dummyObj.name = "LIGHTRIG_MASTER"
		newCamera.parent = dummyObj
	)
	
	-- Gets layer or adds new one (Components)
	if LayerManager.getLayerFromName "Components" == undefined then (
		layer1 = LayerManager.newLayerFromName "Components"
	)
	else (
		layer1 = LayerManager.getLayerFromName "Components"
	)
	
	-- Adds camera to layer
	layer1.addNode newCamera

)



function gta5_prepare HorV = (
	
	-- Prep Scene
	if $FacialAttrGUI != undefined then (
		$FacialAttrGUI.pos = ($faceControls_OFF.pos) + [.2,0,.0675]
		deletekeys $FacialAttrGUI
	)
	
	-- Set up Viewport Layout
	--viewport.resetAllViews()
	if HorV == 1 then (
		viewport.setLayout #layout_2v
	)
	else if HorV == 2 then (
		viewport.setLayout#layout_2h
	)
	else if HorV == 3 then (
		viewport.setLayout#layout_3hb
	)
	else (
		viewport.setLayout #layout_2v
	)
	
	
	-- Delete all existing headCams
	delete $gta5_headCam*
	
	
	-- Create Camera
	viewport.activeViewport = 1
	gta5_createCamera "gta5_headCam_Front" 1
	
	
	if HorV == 3 then (
		-- Active Viewport 2
		viewport.activeViewport = 2
		viewport.setType #view_persp_user
		if ($FacialAttrGUI != undefined) then (select #($FacialAttrGUI,$faceControls_FRAME))
		actionMan.executeAction 0 "311"
		actionMan.executeAction 0 "311"
		if viewport.isWire() then ( actionMan.executeAction 0 "272" )
		clearSelection()
		gta5_createCamera "gta5_headCam_Side" 2
			
		-- Active Viewport 3
		viewport.activeViewport = 3
		viewport.setType #view_front
		if ($FacialAttrGUI != undefined) then (select #($FacialAttrGUI,$faceControls_FRAME))
		actionMan.executeAction 0 "311"
		actionMan.executeAction 0 "311"
		if viewport.isWire() then ( actionMan.executeAction 0 "272" )
		clearSelection()
	)
	else (
		-- Active Viewport 2
		viewport.activeViewport = 2
		viewport.setType #view_front
		if ($FacialAttrGUI != undefined) then (select #($FacialAttrGUI,$faceControls_FRAME))
		actionMan.executeAction 0 "311"
		actionMan.executeAction 0 "311"
		if viewport.isWire() then ( actionMan.executeAction 0 "272" )
		clearSelection()
	)
		
		
	-- Setup Display settings
	hideByCategory.geometry = false
	hideByCategory.shapes = true
	hideByCategory.lights = true
	hideByCategory.helpers = true
	hideByCategory.spacewarps = true
	hideByCategory.particles = true
	hideByCategory.bones = true
	hideByCategory.shapes = true
	hideByCategory.cameras = true

	-- Turn on Auto Key
	animButtonState = true
		
	-- Positions FacialAttrGUI
	--$FacialAttrGUI.pos = [0.420079,0.00154519,1.65797]
	
)

fn reLimitKeys theCtrl lessThan theValue = (
	if theCtrl.keys.count != 0 then (
		for i = 1 to theCtrl.keys.count do (
			if lessThan then (
				if theCtrl.keys[i].value < theValue then (
					theCtrl.keys[i].value = theValue
				)
			)
			else (
				if theCtrl.keys[i].value > theValue then (
					theCtrl.keys[i].value = theValue
				)
			)
		)
	)
)

fn fixAnimLimits = (
	-- Controllers that go from 0 to 1
	ctrlList1 = #(
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[2].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[3].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[7].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[9].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[10].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[11].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[12].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[13].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[14].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[15].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[16].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[17].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[18].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[19].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[20].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[21].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[22].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[23].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[24].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[26].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[27].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[28].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[29].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[30].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[31].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[32].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[34].controller[1].controller,
		$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A[37].controller[1].controller,		
		$tongueInOut_CTRL.modifiers[#Attribute_Holder].tongueInOut_CTRL_A[1].controller[1].controller,
		$tongueRoll_CTRL.modifiers[#Attribute_Holder].tongueRoll_CTRL_A[1].controller[1].controller,
		$eye_C_CTRL.modifiers[#Attribute_Holder].eye_C_CTRL_A[1].controller[1].controller,
		$eye_C_CTRL.modifiers[#Attribute_Holder].eye_C_CTRL_A[2].controller[1].controller,
		$eye_C_CTRL.modifiers[#Attribute_Holder].eye_C_CTRL_A[5].controller[1].controller,
		$eye_C_CTRL.modifiers[#Attribute_Holder].eye_C_CTRL_A[8].controller[1].controller,
		$eye_C_CTRL.modifiers[#Attribute_Holder].eye_C_CTRL_A[9].controller[1].controller,
		$eye_C_CTRL.modifiers[#Attribute_Holder].eye_C_CTRL_A[11].controller[1].controller,
		$cheek_L_CTRL.controller[1][2][2][1].controller,
		$cheek_R_CTRL.controller[1][2][2][1].controller
	)
	
	-- Controllers that go from -1 to 0
	ctrlList2 = #(
		$jaw_CTRL.controller[1][2][2][1].controller,
		$innerBrow_R_CTRL.controller[1][2][1][1].controller,
		$innerBrow_L_CTRL.controller[1][2][1][1].controller
	)
	
	-- Controllers that go from -1 to 1
	ctrlList3 = #(
	)
	
	-- reLimitKeys [theController] [less than?] [the Value]
	for i = 1 to ctrlList1.count do (
		reLimitKeys ctrlList1[i] true 0
		reLimitKeys ctrlList1[i] false 1
	)
	for i = 1 to ctrlList2.count do (
		reLimitKeys ctrlList2[i] true -1
		reLimitKeys ctrlList2[i] false 0
	)
	for i = 1 to ctrlList3.count do (
		reLimitKeys ctrlList3[i] true -1
		reLimitKeys ctrlList3[i] false 1
	)
	
	messageBox "Done!"
	
)
