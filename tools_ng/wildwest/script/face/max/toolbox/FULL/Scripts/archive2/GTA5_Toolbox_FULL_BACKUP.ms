/****************************************************************************************/
-- Title: Image Metrics 3DS Max Animation Tools
-- Version: 1.1
-- Author: Jay Grenier, 2009 
-- Tooltip: 
-- Contains cleanup and setup tools useful to animators using the Image Metrics workflow.
/****************************************************************************************/


/* RUN SCENE SETUP SCRIPTS **********************************************************/
	fileIn ("Scripts/jgSceneSetup.ms")
	fileIn ("Scripts/jgFilterSetup.ms")
	fileIn ("Scripts/ttFloatingTime.ms")
	fileIn ("Scripts/jgTimeRanges.ms")
	fileIn ("Scripts/jgLoadImPlugin.ms")

/* RUN ANIM TOOL SCRIPTS ************************************************************/
	fileIn ("Scripts/jgBreakItDown_RockstarJoysticks.ms")
	fileIn ("Scripts/imResetJoysticks.ms")
	fileIn ("Scripts/jgSmoothKeys.ms")
	fileIn ("Scripts/jgConvertApex.ms")
	fileIn ("Scripts/jgSetToZero.ms")
	fileIn ("Scripts/jgRandomizeKeys.ms")
	fileIn ("Scripts/jgSpreadKeys.ms")
	fileIn ("Scripts/displayMP3.ms")
	fileIn ("Scripts/controlSelectionSetsMP3.ms")
	fileIn ("Scripts/mp3ViewportLayout.ms")
	fileIn ("Scripts/mp3TrackviewFilters.ms")
	fileIn ("Scripts/mp3_fixMoviePlane.ms")
	fileIn ("Scripts/mp3_clearRectKeys.ms")
	fileIn ("Scripts/mp3_delControllerRots.ms")
	fileIn ("Scripts/tpMP3_LockLimits_RSTAR.ms")
	fileIn ("Scripts/IM_MP3_animationExitScript.ms")
	fileIn ("Scripts/Pose2Pose_RC2_noScale.ms")
	fileIn ("Scripts/ttFBXExport.ms")
	fileIn ("Scripts/IM_MP3_blinkMaker.ms")
	--fileIn ("X:/payne/tools/wildwest/script/max/rockstar_sandiego/cutscene/face/OpenMeCreation/scripts/IM_MP3_buildOPENME_RSTAR_BRIG.ms")
	fileIn ("Scripts/tpAltGuiToggle.ms")


/* RUN GENERAL FUNCTIONS SCRIPTS ****************************************************/
	fileIn ("Scripts/jgGlobals.ms")
	fileIn ("Scripts/jgToolBoxFunctions.ms")
	fileIn ("Scripts/jgRCMenus.ms")
	fileIn ("Scripts/jgBuildToolboxUI.ms")
	fileIn ("Scripts/jgToolboxCallbacks.ms")
