function gta5_showHideFaceCirc = (

	face_Circles = #(
		$upperLip_R_CTRL,
		$upperLip_C_CTRL,
		$upperLip_L_CTRL,
		$lowerLip_R_CTRL,
		$lowerLip_C_CTRL,
		$lowerLip_L_CTRL,
		$lipCorner_R_CTRL,
		$lipCorner_L_CTRL,
		$eyelidUpperOuter_R_CTRL,
		$eyelidUpperInner_R_CTRL,
		$eyelidLowerOuter_R_CTRL,
		$eyelidLowerInner_R_CTRL,
		$eyelidUpperOuter_L_CTRL,
		$eyelidUpperInner_L_CTRL,
		$eyelidLowerOuter_L_CTRL,
		$eyelidLowerInner_L_CTRL
	)

	if face_Circles[1].ishidden == true then (
		for i in face_Circles do (
			if i != undefined do (
				i.ishidden = false
			)
		)
	)
	else (
		for i in face_Circles do (
			if i != undefined do (
				i.ishidden = true
			)
		)
	)
	
)