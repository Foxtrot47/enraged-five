fn IM_MP3_instantBlink = (

	-- Delete Window If It Exists Already
	if (IM_MP3_instantBlinkGUI != undefined)	then (destroyDialog IM_MP3_instantBlinkGUI)
	
	-- Define Rollout And Functions
	global IM_MP3_instantBlinkGUI
	rollout IM_MP3_instantBlinkGUI "MP3 Instant Blink" width:174 height:99 (

		-- GUI CONTROLS
		group "Instant Blink" (
			edittext easeInEt "Ease In" text:"3" fieldWidth:44 labelOnTop:true pos:[10,26]
			edittext holdEt "Hold" text:"2" fieldWidth:44 labelOnTop:true  pos:[65,26]
			edittext easeOutEt "Ease Out" text:"3" fieldWidth:44 labelOnTop:true  pos:[120,26]
			button instantBlinkBtn "Make Blink" toolTip:"Make Instant Blink Using GUI Settings" width:154 pos:[10,68]
			--button easeInBtn "Ease In" toopTip:"Make an ease in from current frame" width:75 pos:[10,94]
			--button easeOutBtn "Ease Out" toopTip:"Make an ease out from current frame" width:74 pos:[90,94]
		)	
		
		-- GUI FUNCTIONS
		fn IM_MP3_instantBlinkDo = (

			-- Set Blink Control Vars
			blink_ctrls = #($IM_OpenShutL_Circle.controller[1][2][1].controller,$IM_OpenShutR_Circle.controller[1][2][1].controller)
			blink_value = 0.5
			
			-- Data Vars
			ease_in = easeInEt.text as integer
			ease_out = easeOutEt.text as integer
			hold = holdEt.text as integer
			
			-- Keyframe Vars
			frame = currentTime
			start_frame = frame - ease_in
			blink_frame = frame
			end_blink_frame = (frame + (hold - 1))
			end_frame = (frame + (hold - 1)) + ease_out

			-- Set Autokey
			autoKeyState = animButtonState
			animButtonState = true 
				
			-- Make Keyframes
			for i = 1 to blink_ctrls.count do (

				-- Delete Current Keys
				for j = start_frame to end_frame do (
					
					key_index = getKeyIndex blink_ctrls[i] j
					if(key_index != 0) then deleteKey blink_ctrls[i] key_index
					
				)
				
				-- Start Frame
				at time start_frame blink_ctrls[i].value = 0
				addNewKey blink_ctrls[i] start_frame

				-- Blink Frame
				at time blink_frame blink_ctrls[i].value = blink_value
				addNewKey blink_ctrls[i] blink_frame

				-- Hold Frame
				at time end_blink_frame blink_ctrls[i].value = blink_value
				addNewKey blink_ctrls[i] end_blink_frame

				-- End Frame
				at time end_frame blink_ctrls[i].value = 0
				addNewKey blink_ctrls[i] end_frame
				
			)
			
			-- Set Autokey to what it was
			animButtonState = autoKeyState
			
		)		
		--
		fn IM_MP3_easeInBlinkDo = (	
			
			-- Set Blink Control Vars
			blink_ctrls = #($IM_OpenShutL_Circle.controller[1][2][1].controller,$IM_OpenShutR_Circle.controller[1][2][1].controller)
			
			-- Data Vars
			ease_in = easeInEt.text as integer

			-- Keyframe Vars
			frame = currentTime
			ease_in_frame = frame - ease_in
			
			-- Set Autokey
			autoKeyState = animButtonState
			animButtonState = true 
			
			-- Make Keyframes
			for i = 1 to blink_ctrls.count do (

				-- Delete Current Keys
				for j = ease_in_frame to (frame-1) do (
					
					key_index = getKeyIndex blink_ctrls[i] j
					if(key_index != 0) then deleteKey blink_ctrls[i] key_index
					
				)
				
				-- Ease In Frame
				at time ease_in_frame blink_ctrls[i].value = 0
				addNewKey blink_ctrls[i] ease_in_frame

			)
			
			-- Set Autokey to what it was
			animButtonState = autoKeyState
			
		)
		--
		fn IM_MP3_easeOutBlinkDo = (	
			
			-- Set Blink Control Vars
			blink_ctrls = #($IM_OpenShutL_Circle.controller[1][2][1].controller,$IM_OpenShutR_Circle.controller[1][2][1].controller)
			
			-- Data Vars
			ease_out = easeOutEt.text as integer

			-- Keyframe Vars
			frame = currentTime
			ease_out_frame = frame + ease_out
			
			-- Set Autokey
			autoKeyState = animButtonState
			animButtonState = true 
			
			-- Make Keyframes
			for i = 1 to blink_ctrls.count do (

				-- Delete Current Keys
				for j = frame+1 to (frame + ease_out_frame) do (
					
					key_index = getKeyIndex blink_ctrls[i] j
					if(key_index != 0) then deleteKey blink_ctrls[i] key_index
					
				)
				
				
				
				-- Ease In Frame
				at time ease_out_frame blink_ctrls[i].value = 0
				addNewKey blink_ctrls[i] ease_out_frame

			)
			
			-- Set Autokey to what it was
			animButtonState = autoKeyState
			
		)
		
		-- GUI ACTIONS
		on easeInBtn pressed do (
			
			with undo on (
				IM_MP3_easeInBlinkDo()
			)
			
		)
		--
		on easeOutBtn pressed do (
			
			with undo on (
				IM_MP3_easeOutBlinkDo()
			)
			
		)
		--
		on instantBlinkBtn pressed do (
			
			with undo on (
				IM_MP3_instantBlinkDo()
			)
			
		)	

		
	)	
	
	createDialog IM_MP3_instantBlinkGUI
	
)

fn IM_MP3_blinkFixer = (

	-- Delete Window If It Exists Already
	if (IM_MP3_blinkFixerGUI != undefined)	then (destroyDialog IM_MP3_blinkFixerGUI)	
	
	-- Define Rollout And Functions
	global IM_MP3_blinkFixerGUI
	rollout IM_MP3_blinkFixerGUI "MP3 Blink Fixer" width:190 height:240 (
	
		-- GUI CONTROLS
		group "Blink Fixer" (
			edittext minBlinkValueEt "Min. Blink Value" text:"0.4" fieldWidth:80 labelOnTop:true  pos:[10,26]
			edittext startFrameEt "Start Frame" text:"0" fieldWidth:80 labelOnTop:true pos:[10,70]
			edittext endFrameEt "End Frame" text:"3000" fieldWidth:80 labelOnTop:true  pos:[100,70]

			edittext easeInEt "Ease In" text:"3" fieldWidth:54 labelOnTop:true pos:[10,115]
			edittext holdEt "Hold" text:"2" fieldWidth:54 labelOnTop:true  pos:[68,115]
			edittext easeOutEt "Ease Out" text:"3" fieldWidth:54 labelOnTop:true  pos:[126,115]			
			
			button fixBlinksBtn "Fix Blinks" toolTip:"Fix Blinks Within Specified Range" width:170 pos:[10,156]

		)			

		button instantBlinkBtn "Instant Blink Maker" toolTip:"Make Single Instant Blinks" width:180 pos:[5,190]
		button instantTongueBtn "Instant Tongue Flicker" tooltip:"Make Single Instant Tongue Flick" width: 180 pos:[5,214]
		
		-- GUI FUNCTIONS
		fn IM_MP3_blinkFixerDo = (
			
			-- Query AutoKey State
			akState = animButtonState
			
			-- Turn On Autokey
			animButtonState = true
			
			-- Set Blink Control Vars
			blink_ctrls = #($IM_OpenShutL_Circle.controller[1][2][1].controller,$IM_OpenShutR_Circle.controller[1][2][1].controller)
			blink_value = 0.5
			
			-- Get Data
			min_blink_value = minBlinkValueEt.text as float
			start_frame = startFrameEt.text as integer
			end_frame = endFrameEt.text as integer
			ease_in = easeInEt.text as integer
			hold = holdEt.text as integer
			ease_out = easeOutEt.text as integer
			
			-- Get Blink Length
			forwardBlinkLength = hold + ease_out
			
			-- Loop Blink Ctrls
			for i = 1 to blink_ctrls.count do (
				
				-- Go Through Keys And Find Frames With Blink Keys
				blinkFrames = #()
				for j = 0 to 10000 do (
					key_index = getKeyIndex blink_ctrls[i] j
					if(key_index != 0) then (
						append blinkFrames j
					)	
				)
						
				-- Get Keys To Blink On
				keysAboveThreshold = #()
				for k = 1 to blinkFrames.count do (
					
					key_value = at time blinkFrames[k] blink_ctrls[i].value
					
					-- Check Key Value
					if(key_value >= min_blink_value) then (
						
						key_index = getKeyIndex blink_ctrls[i] blinkFrames[k]
						if(key_index != 0) then (
							if(findItem keysAboveThreshold blinkFrames[k] == 0) then append keysAboveThreshold blinkFrames[k]
						)	
						
					)
				
				)
					
				absoluteBlinkFrames = #()
				
			
				for k = 1 to keysAboveThreshold.count do (
					
					key_value = at time keysAboveThreshold[k] blink_ctrls[i].value
					
					-- Get Next Key Value
					if((keysAboveThreshold[k] + hold)  != undefined) then (
					
						-- Get Blink Range For This Key, Check For Higher Value
						range = keysAboveThreshold[k] + (keysAboveThreshold[k] + hold)
						range_int = ((hold + ease_out) - 1)
					
						highestValueInRange = key_value
					
						bumpedIt = false
						
						if(keysAboveThreshold[k+range_int] != undefined) then (
							
							for j = keysAboveThreshold[k] to range do (
								
								-- Value
								this_value = at time j blink_ctrls[i].value
								
								if(this_value > highestValueInRange) then (
									
									
									if(findItem absoluteBlinkFrames j == 0) then (
										
										bumpedIt = true
										
										append absoluteBlinkFrames j
										
										break
										
									)
									
								)
								
							)
							
						)
						
					)
					
					if(bumpedIt == false) then (
						
						if(findItem absoluteBlinkFrames keysAboveThreshold[k]  == 0) then (
							
							--print keysAboveThreshold[k]
							append absoluteBlinkFrames keysAboveThreshold[k] 
							
						)
						
					)
					
				)
				
				-- Sort Array
				sort absoluteBlinkFrames
				
				/*
					
				-- Check Key Value
				if(key_value >= min_blink_value) then (
						
					-- Check Next Frame
					next_index = k+1
					next_key_value = at time blinkFrames[next_index] blink_ctrls[i].value
					while(next_key_value > key_value) do (
						
						keysBumped += 1
						
						next_key_value = at time blinkFrames[next_index] blink_ctrls[i].value
						next_index += 1
						key_value = next_key_value
						
					)							
					
					keysBumped = forwardBlinkLength
					
					keyFrame = getKeyTime blink_ctrls[i] (next_index-1)
					if(findItem blinkOnFrames keyFrame == 0) then (
						
						append blinkOnFrames keyFrame
						
					)
					
					print keyFrame
					
				)
				
				*/
				
				-- Make Blinks
				print ""
				print "On to the real blinks"
				print ""
				for f = 1 to absoluteBlinkFrames.count do (
					
					
					if(absoluteBlinkFrames[f] >= start_frame AND absoluteBlinkFrames[f] <= end_frame) then (
						
						
						-- Find Frames
						blink_start_frame = absoluteBlinkFrames[f] - ease_in
						blink_frame = absoluteBlinkFrames[f]
						blink_end_blink_frame = (absoluteBlinkFrames[f] + (hold - 1))
						blink_end_frame = (absoluteBlinkFrames[f] + (hold - 1)) + ease_out
						
						-- Get Blink Values
						blink_start_frame_value = at time blink_start_frame blink_ctrls[i].value
						blink_end_frame_value = at time blink_end_frame blink_ctrls[i].value
						this_blink_value = blink_start_frame_value
							
						-- Delete Current Keys
						for k = blink_start_frame to blink_end_frame do (
							
							this_key_index = getKeyIndex blink_ctrls[i] k
							if(this_key_index != 0) then deleteKey blink_ctrls[i] this_key_index
							
						)
						
						-- Start Frame
						at time blink_start_frame blink_ctrls[i].value = blink_start_frame_value
						addNewKey blink_ctrls[i] blink_start_frame		
						
						-- Blink Frame
						at time blink_frame blink_ctrls[i].value = blink_value
						addNewKey blink_ctrls[i] blink_frame
						
						-- Hold Frame
						at time blink_end_blink_frame blink_ctrls[i].value = blink_value
						addNewKey blink_ctrls[i] blink_end_blink_frame
						
						-- End Frame
						at time blink_end_frame blink_ctrls[i].value = blink_end_frame_value
						addNewKey blink_ctrls[i] blink_end_frame							
						
					)
						
				)
				
	
			--print absoluteBlinkFrames
				
			)			
			
			-- Query AutoKey State
			animButtonState = akState
			
		)

		-- GUI ACTIONS
		on fixBlinksBtn pressed do (
			
			with undo on (
				IM_MP3_blinkFixerDo()
			)
			
		)

		on instantBlinkBtn pressed do (
			
			with undo on (
				IM_MP3_instantBlink()
			)
			
		)

		on instantTongueBtn pressed do (
			
			with undo on (
				IM_MP3_instantTongue()
			)
			
		)
		
	)

	createDialog IM_MP3_blinkFixerGUI

)

fn IM_MP3_instantTongue = (

	-- Delete Window If It Exists Already
	if (IM_MP3_instantTongueGUI != undefined)	then (destroyDialog IM_MP3_instantTongueGUI)
	
	-- Define Rollout And Functions
	global IM_MP3_instantTongueGUI
	rollout IM_MP3_instantTongueGUI "MP3 Instant Tongue" width:174 height:99 (

		-- GUI CONTROLS
		group "Instant Tongue" (
			edittext easeInEt "Ease In" text:"3" fieldWidth:31 labelOnTop:true pos:[10,26]
			edittext holdEt "Hold" text:"2" fieldWidth:31 labelOnTop:true  pos:[52,26]
			edittext easeOutEt "Ease Out" text:"3" fieldWidth:31 labelOnTop:true  pos:[93,26]
			edittext tongueValue_num "Value" text:".25" fieldWidth:31 labelOnTop:true pos:[134,26]
			button instantTongueBtn "Flick Tongue" toolTip:"Make Instant Tongue Using GUI Settings" width:154 pos:[10,68]
			--button easeInBtn "Ease In" toopTip:"Make an ease in from current frame" width:75 pos:[10,94]
			--button easeOutBtn "Ease Out" toopTip:"Make an ease out from current frame" width:74 pos:[90,94]
		)	
		
		-- GUI FUNCTIONS
		fn IM_MP3_instantTongueDo = (

			-- Set Tongue Control Vars
			Tongue_ctrls = #($IM_TongueLRU_circle.controller[1][2][1].controller,$IM_Tongue_WNP_circle.controller[1][2][1].controller)
			
			-- Data Vars
			Tongue_value = tongueValue_num.text as float
			ease_in = easeInEt.text as integer
			ease_out = easeOutEt.text as integer
			hold = holdEt.text as integer
			
			-- Keyframe Vars
			frame = currentTime
			start_frame = frame - ease_in
			Tongue_frame = frame
			end_Tongue_frame = (frame + (hold - 1))
			end_frame = (frame + (hold - 1)) + ease_out

			-- Set Autokey
			autoKeyState = animButtonState
			animButtonState = true 
				
			-- Make Keyframes
			for i = 1 to Tongue_ctrls.count do (

				-- Delete Current Keys
				for j = start_frame to end_frame do (
					
					key_index = getKeyIndex Tongue_ctrls[i] j
					if(key_index != 0) then deleteKey Tongue_ctrls[i] key_index
					
				)
				
				-- Start Frame
				at time start_frame Tongue_ctrls[i].value = 0
				addNewKey Tongue_ctrls[i] start_frame

				-- Tongue Frame
				at time Tongue_frame Tongue_ctrls[i].value = Tongue_value
				addNewKey Tongue_ctrls[i] Tongue_frame

				-- Hold Frame
				at time end_Tongue_frame Tongue_ctrls[i].value = Tongue_value
				addNewKey Tongue_ctrls[i] end_Tongue_frame

				-- End Frame
				at time end_frame Tongue_ctrls[i].value = 0
				addNewKey Tongue_ctrls[i] end_frame
				
			)
			
			-- Set Autokey to what it was
			animButtonState = autoKeyState
			
		)		
		--
		fn IM_MP3_easeInTongueDo = (	
			
			-- Set Tongue Control Vars
			Tongue_ctrls = #($IM_TongueLRU_circle.controller[1][2][1].controller,$IM_Tongue_WNP_circle.controller[1][2][1].controller)
			
			-- Data Vars
			ease_in = easeInEt.text as integer

			-- Keyframe Vars
			frame = currentTime
			ease_in_frame = frame - ease_in
			
			-- Set Autokey
			autoKeyState = animButtonState
			animButtonState = true 
			
			-- Make Keyframes
			for i = 1 to Tongue_ctrls.count do (

				-- Delete Current Keys
				for j = ease_in_frame to (frame-1) do (
					
					key_index = getKeyIndex Tongue_ctrls[i] j
					if(key_index != 0) then deleteKey Tongue_ctrls[i] key_index
					
				)
				
				-- Ease In Frame
				at time ease_in_frame Tongue_ctrls[i].value = 0
				addNewKey Tongue_ctrls[i] ease_in_frame

			)
			
			-- Set Autokey to what it was
			animButtonState = autoKeyState
			
		)
		--
		fn IM_MP3_easeOutTongueDo = (	
			
			-- Set Tongue Control Vars
			Tongue_ctrls = #($IM_TongueLRU_circle.controller[1][2][1].controller,$IM_Tongue_WNP_circle.controller[1][2][1].controller)
			
			-- Data Vars
			ease_out = easeOutEt.text as integer

			-- Keyframe Vars
			frame = currentTime
			ease_out_frame = frame + ease_out
			
			-- Set Autokey
			autoKeyState = animButtonState
			animButtonState = true 
			
			-- Make Keyframes
			for i = 1 to Tongue_ctrls.count do (

				-- Delete Current Keys
				for j = frame+1 to (frame + ease_out_frame) do (
					
					key_index = getKeyIndex Tongue_ctrls[i] j
					if(key_index != 0) then deleteKey Tongue_ctrls[i] key_index
					
				)
				
				
				
				-- Ease In Frame
				at time ease_out_frame Tongue_ctrls[i].value = 0
				addNewKey Tongue_ctrls[i] ease_out_frame

			)
			
			-- Set Autokey to what it was
			animButtonState = autoKeyState
			
		)
		
		-- GUI ACTIONS
		on easeInBtn pressed do (
			
			with undo on (
				IM_MP3_easeInTongueDo()
			)
			
		)
		--
		on easeOutBtn pressed do (
			
			with undo on (
				IM_MP3_easeOutTongueDo()
			)
			
		)
		--
		on instantTongueBtn pressed do (
			
			with undo on (
				IM_MP3_instantTongueDo()
			)
			
		)	

		
	)	
	
	createDialog IM_MP3_instantTongueGUI	
)