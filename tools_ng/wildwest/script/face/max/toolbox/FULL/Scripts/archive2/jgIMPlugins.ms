fn openRetargeter =
	(
		utilArray = utilityplugin.classes
		checkPlugin = findItem utilArray Image_Metrics_Retargeting
		if(checkPlugin == 0) then (print "Image Metrics Retargeter not found...")
		if(checkPlugin != 0) then (UtilityPanel.OpenUtility Image_Metrics_Retargeting)
	)
	

fn openCharSetup =
	(
		utilArray = utilityplugin.classes
		checkPlugin = findItem utilArray IM_CharacterSetup
		if(checkPlugin == 0) then (print "Image Metrics Character Setup not found...")
		if(checkPlugin != 0) then (UtilityPanel.OpenUtility IM_CharacterSetup)
	)
	
-- Note:	
-- Rigging Tools come with their own function installed,
-- if the function doesn't exist the button will alert the user.