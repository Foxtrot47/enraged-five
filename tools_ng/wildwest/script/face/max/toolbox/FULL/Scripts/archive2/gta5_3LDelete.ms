function gta5_3LDelete = (
	clearselection()
	select $FacialAttrGUI...*
	selectmore $faceControls_OFF...*
	selectmore $facialRoot_C_OFF...*
	selectmore $FACIAL_facialRoot...*
	delete selection
)