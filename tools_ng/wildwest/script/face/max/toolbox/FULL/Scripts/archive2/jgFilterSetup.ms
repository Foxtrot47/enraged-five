fn setupFilters = 
	(
		tv = "Curve Editor" as string
		trackViews.open tv
		trackViews.clearFilter tv #all
		trackViews.setFilter tv #selectedObjects #transforms #position #curveY #curveX #hierarchy #visibleObjects #sound #objects
		
		/* TURNED OFF DUE TO MAX 9 NOT ALLOWING GLOBAL TRACKS TO BE HIDDEN, CAUSES TOO MUCH FUSS IN THIS FILTERS WHEN YOU OPEN EVERYTHING */
		--trackviews.current.expandTracks()
	)
	
-- Note:
-- This will change as we experiment with user preference.