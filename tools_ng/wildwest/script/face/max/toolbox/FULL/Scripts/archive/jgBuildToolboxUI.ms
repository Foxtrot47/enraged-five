fileIn "S:/Animation/Max/MaxToolbox/IM/Scripts/displayMP3.ms"
fileIn "S:/Animation/Max/MaxToolbox/IM/Scripts/controlSelectionSetsMP3.ms"
fileIn "S:/Animation/Max/MaxToolbox/IM/Scripts/mp3ViewportLayout.ms"
fileIn "S:/Animation/Max/MaxToolbox/IM/Scripts/mp3TrackviewFilters.ms"
fileIn "S:/Animation/Max/MaxToolbox/IM/Scripts/mp3_fixMoviePlane.ms"
fileIn "S:/Animation/Max/MaxToolbox/IM/Scripts/mp3_clearRectKeys.ms"


--If rollout(ui window) exists, delete it and create a new one.
if (jgAnimToolboxFloater != undefined)
then (closeRolloutFloater jgAnimToolboxFloater)

		rollout roScnSetup "Scene Setup"
		(
				button btnSetupScene "Scene Setup GUI" width:107 pos:[3, 6]
				button btnFloatTime "Floating Time GUI" width:107 pos:[3, 31]
				button btnTimeRanges "Time Ranger GUI" width:107 pos:[3, 56]
							button btnSetupSceneQ "?" width:11 pos:[112, 6]
							button btnFloatTimeQ "?" width:11 pos:[112, 31]
							button btnTimeRangesQ "?" width:11 pos:[112, 56]

				--SETUP BUTTON
				on btnSetupScene pressed do
				(
					runSceneSetup()
				)

						on btnSetupSceneQ pressed do
						(
								PopupMenu rcSS align:#align_vcenterleft
						)
						
				--FLOATING TIME BUTTON
				on btnFloatTime pressed do
				(
					floatingTime()
				)

						on btnFloatTimeQ pressed do
						(
								PopupMenu rcFT align:#align_vcenterleft
						)
	
				--TIME RANGES BUTTON
				on btnTimeRanges pressed do
				(
					timeRanges()
				)

						on btnTimeRangesQ pressed do
						(
								PopupMenu rcTR align:#align_vcenterleft
						)
						
						
		)
			
		rollout roRetargeting "IM Tools"
		(
				button btnRetargeter "Retargeter" width:107 pos:[3, 6]
							button btnRetargeterQ "?" width:11 pos:[112, 6]

			
				--RETARGTER BUTTON
				on btnRetargeter pressed do
				(
					openRetargeter()
				)
				
						on btnRetargeterQ pressed do
						(
								PopupMenu rcRE align:#align_vcenterleft
						)									
						
		)
		
		rollout roAnimTools "Animation Tools"
		(
				button btnResetJoysticks "Reset Controller" width:107 pos:[3, 6]
				button btnSmoothKeys "Smooth Keys" width:107 pos:[3, 31]
				button btnConvertApex "Convert Apex" width:107 pos:[3, 56]
				button btnSetZero "Set To Zero" width:107 pos:[3, 81]
				button btnRandKeys "Randomize" width:107 pos:[3, 106]
				button btnSpreadKeys "Spread Keys" width:107 pos:[3, 131]
				button btnBreakitdown "Break It Down" width:107 pos:[3, 156]
				button btnP2P "Pose To Pose" width:107 pos:[3, 181]
				button btnBlinkMaker "Blink Maker (BETA)" width:120 pos:[3, 207]
				button btnExitScript "*** Anim Finished ***" width:120 pos:[3, 233]
							button btnResetJoysticksQ "?" width:11 pos:[112, 6]
							button btnSmoothKeysQ "?" width:11 pos:[112, 31]
							button btnConvertApexQ "?" width:11 pos:[112, 56]
							button btnSetZeroQ "?" width:11 pos:[112, 81]
							button btnRandKeysQ "?" width:11 pos:[112, 106]
							button btnSpreadKeysQ "?" width:11 pos:[112, 131]
							button btnBreakitdownQ "?" width:11 pos:[112, 156]
							button btnP2PQ "?" width:11 pos:[112, 181]

				--ANIMATION EXIT SCRIPT
				on btnExitScript pressed do
				(
					fileIn ("S:/Animation/Max/MaxToolbox/IM/Scripts/IM_MP3_animationExitScript.ms")
				)	
				
				--ANIMATION EXIT SCRIPT
				on btnBlinkMaker pressed do
				(
					fileIn ("S:/Animation/Max/MaxToolbox/IM/Scripts/IM_MP3_blinkMaker.ms")
				)					
			
				--RESET BUTTON
				on btnResetJoysticks pressed do
				(
					resetJoysticks()
				)

						on btnResetJoysticksQ pressed do
						(
								PopupMenu rcRC align:#align_vcenterleft
						)
						
				--SMOOTH BUTTON
				on btnSmoothKeys pressed do
				(
					smoothKeys()
				)
	
						on btnSmoothKeysQ pressed do
						(
								PopupMenu rcSK align:#align_vcenterleft
						)
						
				--CONVERT BUTTON
				on btnConvertApex pressed do
				(
					convertApex()
				)
				
						on btnConvertApexQ pressed do
						(
								PopupMenu rcCA align:#align_vcenterleft
						)
						
				--SET TO ZERO BUTTON
				on btnSetZero pressed do
				(
					setToZero()
				)
				
						on btnSetZeroQ pressed do
						(
								PopupMenu rcSTZ align:#align_vcenterleft
						)
						
				--RANDOM KEYS BUTTON
				on btnRandKeys pressed do
				(
					randomizeValues()
				)
				
						on btnRandKeysQ pressed do
						(
								PopupMenu rcRK align:#align_vcenterleft
						)
						
				--SPREAD KEYS BUTTON
				on btnSpreadKeys pressed do
				(
					spreadKeys()
				)				
				
						on btnSpreadKeysQ pressed do
						(
								PopupMenu rcSKS align:#align_vcenterleft
						)

				--BREAK IT DOWN BUTTON
				on btnBreakitdown pressed do
				(
					jgBreakItDownToolbox()
				)				
				
						on btnBreakitdownQ pressed do
						(
								PopupMenu rcBID align:#align_vcenterleft
						)

				--POSE 2 POSE BUTTON
				on btnP2P pressed do
				(
					fileIn ("Y:\\Maxscripts\\Toolbox\\Scripts\\Pose2Pose_RC2_noScale.ms")
				)				
				
						on btnP2PQ pressed do
						(
								PopupMenu rcP2P align:#align_vcenterleft
						)
						
						
		)
		
		rollout roMaxTools "Max Tools"
		(
				--Timeline Control Buttons
				button btnGoToStart "|<"width:18 height:18 pos:[10,6]		
				button btnPreviousFrame "<<" width:18 height:18 pos:[32,6]
				button btnStop "[-]" width:18 height:18 pos:[54,6] visible:false			
				button btnPlay "(>" width:18 height:18 pos:[54,6] visible:true
				button btnNextFrame ">>" width:18 height:18 pos:[76,6]
				button btnGoToEnd ">|" width:18 height:18 pos:[98,6]

				button btnAutoKey1 "Toggle Autokey"  width:120 pos:[3,30] visible:false
				button btnAutoKey2 "Toggle Autokey" width:120 pos:[3,30]  visible:true
			
				button btnPreview "Make Preview" width:120 pos:[3,55]

				button btnLayerManager "Layers" width:58 pos:[3, 80]
				button btnKeyMode "KeyMode" width:59 pos:[64, 80]

				dropdownlist ddCoords items:coordsArray width:69 height:18 pos:[30, 105]
			
				----------------------------------------
				--TIMELINE CONTROL BUTTONS
					
				--go to start
				on btnGoToStart pressed do
				(
					sliderTime = animationRange.start
				)

				--previous frame
				on btnPreviousFrame pressed do
				(
					sliderTime = sliderTime - 1
				)

				--stop
				on btnStop pressed do
				(
					btnStop.visible = false
					btnPlay.visible = true
					max time play
				)

				--play
				on btnPlay pressed do
				(
					btnPlay.visible = false
					btnStop.visible = true
					max time play
				)

				--next frame
				on btnNextFrame pressed do
				(
					sliderTime = sliderTime + 1
				)

				--go to end
				on btnGoToEnd pressed do
				(
					sliderTime = animationRange.end
				)
				
				--END TIMELINE CONTROLS
				-----------------------------------
				
				--AUTOKEY BUTTON ON
				on btnAutoKey1 pressed do
				(
					btnAutoKey1.visible = false
					btnAutoKey2.visible = true
					animButtonState = true
				)

				--AUTOKEY BUTTON OFF
				on btnAutoKey2 pressed do
				(
				btnAutoKey1.visible = true
				btnAutoKey2.visible = false
				animButtonState = false
				)			

				--PREVIEW BUTTON
				on btnPreview pressed do
				(
					viewportbuttonMgr.EnableButtons = false
  					max preview
  					viewportbuttonMgr.EnableButtons = true
				)
				
				--LAYERS BUTTON
				on btnLayerManager pressed do
				(
						if(layermanager.isDialogOpen() == true) then(layermanager.closeDialog())
						else(macros.run "Layers" "LayerManager")
				)

				--KEY MODE BUTTON
				on btnKeyMode pressed do
				(
						max key mode
				)
				
				on ddCoords selected x do
				(
					coordTypes = #(#view, #screen, #world, #parent, #local, #gimbal, #grid)
					toolMode.coordsys coordTypes[x]
				)
			
		)
		rollout roMP3Render "MP3 Render"
		(
			--Render
			button btnMP3Render "One Button Render"
			on btnMP3Render pressed do (
				fileIn "S:/Maxscripts/Dailies_MP3.ms"
			)		
		)
		rollout roMisc "Misc"
		(
			button btnFBXEX "Export FBX" width:110
			on btnFBXEX pressed do 
			(
				fileIn "S:\\Animation\\Max\\MaxToolbox\\IM\\Scripts\\ttFBXExport.ms"
			)	
		)
		rollout roMP3PrepScene "MP3 Prepare Scene"
		(
			--Render
			button btnMP3Render "Prepare Settings"
			on btnMP3Render pressed do (

				displayMP3()
				mp3ViewportLayout()
				controlSelectionSetsMP3()
				mp3_clearRectKeys()
				
			)
			button btnMP3TrackviewFilters "Trackview Filters"
			on btnMP3TrackviewFilters pressed do (
				mp3TrackviewFilters()
			)
			button btnMP3_fixMoviePlane "Fix MoviePlane" tooltip: "Use after importing video via retargeter to position the movieplane."
			on btnMP3_fixMoviePlane pressed do (
				MP3_fixMoviePlane()
			)
				
		)
	loadFloaterConfiguration()
		
	if(floatersArray[2] != undefined) then
	(
		try(closeRolloutFloater jgAnimToolboxFloater)catch()
		jgAnimToolboxFloater = newRolloutFloater "Toolbox" 140 floatersArray[1] floatersArray[2] floatersArray[3]
	)
	else
	(
		try(closeRolloutFloater jgAnimToolboxFloater)catch()
		jgAnimToolboxFloater = newRolloutFloater "Toolbox" 140 835
	)
		
	addRollout roScnSetup jgAnimToolboxFloater rolledUp:false
	addRollout roRetargeting jgAnimToolboxFloater rolledUp:false
	addRollout roAnimTools jgAnimToolboxFloater rolledUp:false
	addRollout roMaxTools jgAnimToolboxFloater rolledUp:false
	addRollout roMP3Render jgAnimToolboxFloater rolledup:false
	addRollout roMP3PrepScene jgAnimToolboxFloater rolledup:false
	addRollout roMisc jgAnimToolboxFloater rolledUp:false
	
	i = 4
	j = (i + 4)
	k = 1

	for i = 4 to j do
	(
		if(floatersArray[6] != undefined) then	(jgAnimToolBoxFloater.rollouts[k].open = floatersArray[i])
		k += 1
	)