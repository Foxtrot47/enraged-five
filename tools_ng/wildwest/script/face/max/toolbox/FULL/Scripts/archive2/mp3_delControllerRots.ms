/*

MP3_delControllerRots
This script zeroes, removes and prevents all rotation keys on R*'s Facial GUI.
Written by Thanh Phan 2011

*/


fn MP3_delControllerRots = (
	
	-- List of controllers
	controllerArray = #(
		$con_browRaise_l,
		$con_browRaise_r,
		$con_browRaise_cl,
		$con_browRaise_cr,
		$con_cheek_l,
		$con_cheek_r,
		$con_upperLipRaise_r,
		$con_upperLipRaise_c,
		$con_upperLipRaise_l,
		$con_lipStretch_r,
		$con_lipStretch_l,
		$con_lowerLipDepress_r,
		$con_lowerLipDepress_c,
		$con_lowerLipDepress_l,
		$con_mouthInner_c,
		$con_mouth_c,
		$con_chinRaise_c,
		$con_jaw_c
	)
	
	
	-- List of controllers
	controllerArray2 = #(
		$con_nose_c,
		$con_eye_r,
		$con_eye_l,
		$con_eyeBall_l,
		$con_eyeBall_r
	)
	
	for ctrls in controllerArray do (
		if ctrls != undefined do (
			-- Zeroes rotations
			ctrls.rotation.controller.X_Rotation.controller.value = 0
			ctrls.rotation.controller.Y_Rotation.controller.value = 0
			ctrls.rotation.controller.Z_Rotation.controller.value = 0
			
			-- Removes keys
			deletekeys ctrls.rotation.controller.X_Rotation.controller
			deletekeys ctrls.rotation.controller.Y_Rotation.controller
			deletekeys ctrls.rotation.controller.Z_Rotation.controller
			
			-- Prevents keys in rotation
			ctrls.rotation.controller.X_Rotation.controller.keyable = false
			ctrls.rotation.controller.Y_Rotation.controller.keyable = false
			ctrls.rotation.controller.Z_Rotation.controller.keyable = false
		)
	)
	
)