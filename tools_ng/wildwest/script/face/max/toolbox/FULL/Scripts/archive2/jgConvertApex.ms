fn convertApex = 
	(
	
		-- Title: Convert Apex
		-- Version: 1.1
		-- Author: Jay Grenier, 2008
		-- Tooltip: Convert a plateau of keys into a single key with the average value and time.
		
		sel = getCurrentSelection()
				
		if(sel.count > 0) then
		(		
			/* Find # of selected controllers ***************/
			cArray = selection as array	
		
			for i = 1 to cArray.count do
			(
				
				/* Define initial array(s) **********/
				xArray = cArray[i].controller[1][1][1].keys -- all X keys on selected controller
				yArray = cArray[i].controller[1][2][1].keys -- all Y keys on selected controller
				
				arrayKY = #() -- arrayKY holds selected Y keys
				arrayKX = #() -- arrayKX holds selected X keys
			
				--Get selected keys x
				for i = 1 to xArray.count do
				(
					if xArray[i].selected then append arrayKX xArray[i]
				)
			
				--Get selected keys y
				for i = 1 to yArray.count do
				(
					if yArray[i].selected then append arrayKY yArray[i]
				)
				
				if(arrayKY.count == 0 AND arrayKX.count == 0) then
				(
					print "No keys selected.."
				)
				else
				(				
					if(arrayKY[2].time == (arrayKY[1].time + 1)) then (exit)
					
					if(arrayKX.count == 1)then (deleteItem arrayKX 1)
					if(arrayKY.count == 1)then (deleteItem arrayKY 1)
					
					/* Get values and time of two selected keys ****/
					
					valuesArrayX = #()
					valuesArrayY = #()
					timeArrayX = #()
					timeArrayY = #()
				
					for i = 1 to arrayKX.count do
					(
						append valuesArrayX ArrayKX[i].value
						append timeArrayX ArrayKX[i].time
					)
					
					for i = 1 to arrayKY.count do
					(
						append valuesArrayY ArrayKY[i].value
						append timeArrayY ArrayKY[i].time
					)
					
					/* Get average value of two keys *****/
					if(valuesArrayX[1] != undefined) then (averageX = (valuesArrayX[1] + valuesArrayX[2])/2)
					if(valuesArrayY[1] != undefined) then (averageY = (valuesArrayY[1] + valuesArrayY[2])/2)
					
					/* Get average time of two keys *****/
					if(timeArrayX[1] != undefined) then (averageTimeX = (timeArrayX[1] + timeArrayX[2])/2)
					if(timeArrayY[1] != undefined) then (averageTimeY = (timeArrayY[1] + timeArrayY[2])/2)
				
					/* If average time is not an integer, round up *****/
					if(averageTimeX != undefined) then (newTimeX = ceil(averageTimeX as integer / ticksperframe))
					if(averageTimeY != undefined) then (newTimeY = ceil(averageTimeY as integer / ticksperframe))
					
					/* Add key at average value */
					if(newTimeX != undefined) then (addNewKey cArray[i].controller[1][1][1] newTimeX)
					if(newTimeY != undefined) then (addNewKey cArray[i].controller[1][2][1] newTimeY)
					
					/* Delete selected keys */
					deleteKeys cArray[i].controller[1][1][1] #selection
					deleteKeys cArray[i].controller[1][2][1] #selection
				)
			)
		)
		else
		(
			print "Nothing selected.."
		)
	)