--If rollout(ui window) exists, delete it and create a new one.
if (gta5_animToolboxFloater != undefined)
then (closeRolloutFloater gta5_animToolboxFloater)


rollout roParadise ""
(
	button btnFloatTime "Floating Time GUI" width:107 pos:[3, 6]
	button btnFloatTimeQ "?" width:11 pos:[112, 6]
	button btnTimeRanges "Time Ranger GUI" width:107 pos:[3, 31]
	button btnTimeRangesQ "?" width:11 pos:[112, 31]
	button btnP2P "Pose To Pose" width:107 pos:[3, 56]
	button btnP2PQ "?" width:11 pos:[112, 56]
	button btnTF "Tongue Flicker" width:107 pos:[3, 81]
	button btnTFQ "?" width:11 pos:[112, 81]
			
	--FLOATING TIME BUTTON
	on btnFloatTime pressed do (
		floatingTime()
	)
	on btnFloatTimeQ pressed do (
			PopupMenu rcFT align:#align_vcenterleft
	)
			
	--TIME RANGES BUTTON
	on btnTimeRanges pressed do (
		timeRanges()
	)
	on btnTimeRangesQ pressed do (
			PopupMenu rcTR align:#align_vcenterleft
	)
		
	--POSE 2 POSE BUTTON
	on btnP2P pressed do (
		pose2pose_init()
	)				
	on btnP2PQ pressed do (
			PopupMenu rcP2P align:#align_vcenterleft
	)
		
	--TONGUE FLICKER BUTTON
	on btnTF pressed do (
		gta5_tongueFlicker()
	)				
	on btnTFQ pressed do (
			PopupMenu rcTF align:#align_vcenterleft
	)
			
)
	
rollout roParadise2 ""
(
	label lbl1 " Layout:" width:120 height:17 pos:[3,4]
	button btnMP3_prepareSettingsV "2V" width:30 height:25 pos:[13, 21]
	button btnMP3_prepareSettingsH "2H" width:30 height:25 pos:[46, 21]
	button btnMP3_prepareSettings3 "3H" width:30 height:25 pos:[79, 21]
	button btnMP3_prepareSettingsQ "?" width:11 height:25 pos:[112, 21]
	button btnMP3_trackviewFilters "Trackview Filters" width:107 pos:[3, 50]
	button btnMP3_trackviewFiltersQ "?" width:11 pos:[112, 50]
	button btnMP3_fixMoviePlane "Fix MoviePlane" tooltip: "Use after importing video via retargeter to position the movieplane." width:107 pos:[3, 75]
	button btnMP3_fixMoviePlaneQ "?" width:11 pos:[112, 75]
			
	--PREPARE SETTINGS BUTTON
	on btnMP3_prepareSettingsV pressed do (
		gta5_prepare 1
	)
	on btnMP3_prepareSettingsH pressed do (
		gta5_prepare 2
	)
	on btnMP3_prepareSettings3 pressed do (
		gta5_prepare 3
	)
	on btnMP3_prepareSettingsQ pressed do (
		PopupMenu rcPS align:#align_vcenterleft
	)
	
	--GRAPH EDITOR FILTERS BUTTON
	on btnMP3_trackviewFilters pressed do (
		mp3TrackviewFilters()
	)
	on btnMP3_trackviewFiltersQ pressed do (
		PopupMenu rcTF align:#align_vcenterleft
	)
		
	--FIX MOVIEPLANE BUTTON
	on btnMP3_fixMoviePlane pressed do (
		MP3_fixMoviePlane()
	)
	on btnMP3_fixMoviePlaneQ pressed do (
		PopupMenu rcFMP align:#align_vcenterleft
	)
)
	
rollout roParadise3 ""
(
	button btnGTA5_showHideFaceCirc "Show/Hide Face Ctrls" width:107 pos:[3, 6]
	button btnGTA5_showHideFaceCircQ "?" width:11 pos:[112, 6]
		
	button btnGTA5_zeroCtrls "Zero Selected Ctrls" width:107 pos:[3, 31]
	button btnGTA5_zeroCtrlsQ "?" width:11 pos:[112, 31]
		
	button btnGTA5_delKeysSelected "Del Keys on Selected" width: 107 pos:[3,56]
	button btnGTA5_delKeysSelectedQ "?" width:11 pos:[112, 56]
	
	button btnGTA5_fixAnimLimits "Fix Anim Limits" width: 107 pos:[3,81]
	button btnGTA5_fixAnimLimitsQ "?" width: 11 pos:[112,81]
	
	label lbl2 " Make Preview:" width:120 height:17 pos:[3,110]
	button btnMakePreview "1 Head" width:47 height:25 pos:[13,127]
	button btnMakePreviewLG "2 Heads" width:47 height:25 pos:[63,127]
	button btnMakePreviewQ "?" width:11 height:25 pos:[112, 127]
		
	--SHOW/HIDE FACE CIRCLES BUTTON
	on btnGTA5_showHideFaceCirc pressed do (
		gta5_showHideFaceCirc()
	)
	on btnGTA5_showHideFaceCircQ pressed do (
		PopupMenu rcSHFC align:#align_vcenterleft
	)
		
	--SHOW/HIDE FACE CIRCLES BUTTON
	on btnGTA5_zeroCtrls pressed do (
		gta5_zeroCtrls()
	)
	on btnGTA5_zeroCtrlsQ pressed do (
		PopupMenu rcZC align:#align_vcenterleft
	)
		
	--SHOW/HIDE FACE CIRCLES BUTTON
	on btnGTA5_delKeysSelected pressed do (
		gta5_delKeys()
	)
	on btnGTA5_delKeysSelectedQ pressed do (
		PopupMenu rcDCS align:#align_vcenterleft
	)
		
	--FIX ANIM LIMITS BUTTON
	on btnGTA5_fixAnimLimits pressed do (
		fixAnimLimits()
	)
	on btnGTA5_fixAnimLimitsQ pressed do (
		PopupMenu rcFAL align:#align_vcenterleft
	)
		
	--PREVIEW BUTTONS
	on btnMakePreview pressed do
	(
		gta5_makePreview()
	)
	on btnMakePreviewLG pressed do
	(
		gta5_makePreview_side()
	)
	on btnMakePreviewQ pressed do (
		PopupMenu rcMPV align:#align_vcenterleft
	)
	
)

rollout roParadise5 ""
(
	
	button btnGTA5_exportFBX "Export FBX" width:107 pos:[3, 6]
	button btnGTA5_exportFBXQ "?" width:11 pos:[112, 6]
	on btnGTA5_exportFBX pressed do (
		importFiles()
		doTheConvertTo1Way()

	-- The list of controllers to be copied from
		copyFromList = #(
			$jaw_CTRL.controller[1][2][1][1].controller,
			$jaw_CTRL.controller[1][2][2][1].controller,
			$mouth_CTRL.controller[1][2][1][1].controller,
			$mouth_CTRL.controller[1][2][2][1].controller,
			$tongueRoll_CTRL.controller[1][2][1][1].controller,
			$tongueRoll_CTRL.controller[1][2][2][1].controller,
			$tongueMove_CTRL.controller[1][2][1][1].controller,
			$tongueMove_CTRL.controller[1][2][2][1].controller,
			$tongueInOut_CTRL.controller[1][2][2][1].controller,
			$nose_L_CTRL.controller[1][2][1][1].controller,
			$nose_L_CTRL.controller[1][2][2][1].controller,
			$nose_R_CTRL.controller[1][2][1][1].controller,
			$nose_R_CTRL.controller[1][2][2][1].controller,
			$cheek_L_CTRL.controller[1][2][2][1].controller,
			$cheek_R_CTRL.controller[1][2][2][1].controller,
			$eye_C_CTRL.controller[1][2][1][1].controller,
			$eye_C_CTRL.controller[1][2][2][1].controller,
			$eye_L_CTRL.controller[1][2][1][1].controller,
			$eye_L_CTRL.controller[1][2][2][1].controller,
			$eye_R_CTRL.controller[1][2][1][1].controller,
			$eye_R_CTRL.controller[1][2][2][1].controller,
			$innerBrow_L_CTRL.controller[1][2][1][1].controller,
			$innerBrow_L_CTRL.controller[1][2][2][1].controller,
			$innerBrow_R_CTRL.controller[1][2][1][1].controller,
			$innerBrow_R_CTRL.controller[1][2][2][1].controller,
			$outerBrow_L_CTRL.controller[1][2][2][1].controller,
			$outerBrow_R_CTRL.controller[1][2][2][1].controller, -- END faceControls_OFF
			$CIRC_press.controller[1][2][1][1].controller, -- BEGIN facialAttrGUI
			$CIRC_rollIn.controller[1][2][1][1].controller,
			$CIRC_narrowWide.controller[1][2][1][1].controller,
			$CIRC_clench.controller[1][2][1][1].controller,
			$CIRC_backFwd.controller[1][2][1][1].controller,
			$CIRC_nasolabialFurrowL.controller[1][2][1][1].controller,
			$CIRC_nasolabialFurrowR.controller[1][2][1][1].controller,
			$CIRC_smileR.controller[1][2][1][1].controller,
			$CIRC_smileL.controller[1][2][1][1].controller,
			$CIRC_openSmileR.controller[1][2][1][1].controller,
			$CIRC_openSmileL.controller[1][2][1][1].controller,
			$CIRC_frownR.controller[1][2][1][1].controller,
			$CIRC_frownL.controller[1][2][1][1].controller,
			$CIRC_scream.controller[1][2][1][1].controller,
			$CIRC_lipsNarrowWideR.controller[1][2][1][1].controller,
			$CIRC_lipsNarrowWideL.controller[1][2][1][1].controller,
			$CIRC_lipsStretchOpenR.controller[1][2][1][1].controller,
			$CIRC_lipsStretchOpenL.controller[1][2][1][1].controller,
			$CIRC_chinWrinkle.controller[1][2][1][1].controller,
			$CIRC_chinRaiseUpper.controller[1][2][1][1].controller,
			$CIRC_chinRaiseLower.controller[1][2][1][1].controller,
			$CIRC_closeOuterR.controller[1][2][1][1].controller,
			$CIRC_closeOuterL.controller[1][2][1][1].controller,
			$CIRC_puckerR.controller[1][2][1][1].controller,
			$CIRC_puckerL.controller[1][2][1][1].controller,
			$CIRC_oh.controller[1][2][1][1].controller,
			$CIRC_funnelUR.controller[1][2][1][1].controller,
			$CIRC_funnelDR.controller[1][2][1][1].controller,
			$CIRC_mouthSuckUR.controller[1][2][1][1].controller,
			$CIRC_mouthSuckDR.controller[1][2][1][1].controller,
			$CIRC_pressR.controller[1][2][1][1].controller,
			$CIRC_pressL.controller[1][2][1][1].controller,
			$CIRC_dimpleR.controller[1][2][1][1].controller,
			$CIRC_dimpleL.controller[1][2][1][1].controller,
			$CIRC_suckPuffR.controller[1][2][1][1].controller,
			$CIRC_suckPuffL.controller[1][2][1][1].controller,
			$CIRC_lipBite.controller[1][2][1][1].controller,
			$CIRC_funnelUL.controller[1][2][1][1].controller,
			$CIRC_funnelDL.controller[1][2][1][1].controller,
			$CIRC_mouthSuckUL.controller[1][2][1][1].controller,
			$CIRC_mouthSuckDL.controller[1][2][1][1].controller,
			$CIRC_blinkL.controller[1][2][1][1].controller,
			$CIRC_squeezeR.controller[1][2][1][1].controller,
			$CIRC_squeezeL.controller[1][2][1][1].controller,
			$CIRC_blinkR.controller[1][2][1][1].controller,
			$CIRC_openCloseDR.controller[1][2][1][1].controller,
			$CIRC_squintInnerUR.controller[1][2][1][1].controller,
			$CIRC_squintInnerDR.controller[1][2][1][1].controller,
			$CIRC_openCloseUR.controller[1][2][1][1].controller,
			$CIRC_openCloseDL.controller[1][2][1][1].controller,
			$CIRC_squintInnerUL.controller[1][2][1][1].controller,
			$CIRC_squintInnerDL.controller[1][2][1][1].controller,
			$CIRC_openCloseUL.controller[1][2][1][1].controller,
			$CIRC_thinThick_B.controller[1][2][1][1].controller,
			$CIRC_stickyLips_E.controller[1][2][1][1].controller,
			$CIRC_thinThick_F.controller[1][2][1][1].controller,
			$CIRC_thinThick_H.controller[1][2][1][1].controller,
			$CIRC_thinThick_C.controller[1][2][1][1].controller,
			$CIRC_thinThick_D.controller[1][2][1][1].controller,
			$CIRC_thinThick_G.controller[1][2][1][1].controller,
			$CIRC_stickyLips_A.controller[1][2][1][1].controller, -- END FacialAttrGUI
			$lipCorner_L_CTRL.controller[1][2][1].controller, -- BEGIN facialRoot_C_OFF
			$lipCorner_L_CTRL.controller[1][2][2].controller,
			$lipCorner_L_CTRL.controller[1][2][3].controller,
			$lipCorner_L_CTRL.controller[2][2][1].controller,
			$lipCorner_L_CTRL.controller[2][2][2].controller,
			$lipCorner_L_CTRL.controller[2][2][3].controller,
			$lipCorner_R_CTRL.controller[1][2][1].controller,
			$lipCorner_R_CTRL.controller[1][2][2].controller,
			$lipCorner_R_CTRL.controller[1][2][3].controller,
			$lipCorner_R_CTRL.controller[2][2][1].controller,
			$lipCorner_R_CTRL.controller[2][2][2].controller,
			$lipCorner_R_CTRL.controller[2][2][3].controller,
			$upperLip_R_CTRL.controller[1][2][1].controller,
			$upperLip_R_CTRL.controller[1][2][2].controller,
			$upperLip_R_CTRL.controller[1][2][3].controller,
			$upperLip_R_CTRL.controller[2][2][1].controller,
			$upperLip_R_CTRL.controller[2][2][2].controller,
			$upperLip_R_CTRL.controller[2][2][3].controller,
			$upperLip_C_CTRL.controller[1][2][1].controller,
			$upperLip_C_CTRL.controller[1][2][2].controller,
			$upperLip_C_CTRL.controller[1][2][3].controller,
			$upperLip_C_CTRL.controller[2][2][1].controller,
			$upperLip_C_CTRL.controller[2][2][2].controller,
			$upperLip_C_CTRL.controller[2][2][3].controller,
			$upperLip_L_CTRL.controller[1][2][1].controller,
			$upperLip_L_CTRL.controller[1][2][2].controller,
			$upperLip_L_CTRL.controller[1][2][3].controller,
			$upperLip_L_CTRL.controller[2][2][1].controller,
			$upperLip_L_CTRL.controller[2][2][2].controller,
			$upperLip_L_CTRL.controller[2][2][3].controller,
			$lowerLip_L_CTRL.controller[1][2][1].controller,
			$lowerLip_L_CTRL.controller[1][2][2].controller,
			$lowerLip_L_CTRL.controller[1][2][3].controller,
			$lowerLip_L_CTRL.controller[2][2][1].controller,
			$lowerLip_L_CTRL.controller[2][2][2].controller,
			$lowerLip_L_CTRL.controller[2][2][3].controller,
			$lowerLip_C_CTRL.controller[1][2][1].controller,
			$lowerLip_C_CTRL.controller[1][2][2].controller,
			$lowerLip_C_CTRL.controller[1][2][3].controller,
			$lowerLip_C_CTRL.controller[2][2][1].controller,
			$lowerLip_C_CTRL.controller[2][2][2].controller,
			$lowerLip_C_CTRL.controller[2][2][3].controller,
			$lowerLip_R_CTRL.controller[1][2][1].controller,
			$lowerLip_R_CTRL.controller[1][2][2].controller,
			$lowerLip_R_CTRL.controller[1][2][3].controller,
			$lowerLip_R_CTRL.controller[2][2][1].controller,
			$lowerLip_R_CTRL.controller[2][2][2].controller,
			$lowerLip_R_CTRL.controller[2][2][3].controller,
			$eyelidUpperOuter_R_CTRL.controller[1][2][1].controller,
			$eyelidUpperOuter_R_CTRL.controller[1][2][2].controller,
			$eyelidUpperOuter_R_CTRL.controller[1][2][3].controller,
			$eyelidUpperOuter_R_CTRL.controller[2][2][1].controller,
			$eyelidUpperOuter_R_CTRL.controller[2][2][2].controller,
			$eyelidUpperOuter_R_CTRL.controller[2][2][3].controller,
			$eyelidUpperInner_R_CTRL.controller[1][2][1].controller,
			$eyelidUpperInner_R_CTRL.controller[1][2][2].controller,
			$eyelidUpperInner_R_CTRL.controller[1][2][3].controller,
			$eyelidUpperInner_R_CTRL.controller[2][2][1].controller,
			$eyelidUpperInner_R_CTRL.controller[2][2][2].controller,
			$eyelidUpperInner_R_CTRL.controller[2][2][3].controller,
			$eyelidUpperInner_L_CTRL.controller[1][2][1].controller,
			$eyelidUpperInner_L_CTRL.controller[1][2][2].controller,
			$eyelidUpperInner_L_CTRL.controller[1][2][3].controller,
			$eyelidUpperInner_L_CTRL.controller[2][2][1].controller,
			$eyelidUpperInner_L_CTRL.controller[2][2][2].controller,
			$eyelidUpperInner_L_CTRL.controller[2][2][3].controller,
			$eyelidUpperOuter_L_CTRL.controller[1][2][1].controller,
			$eyelidUpperOuter_L_CTRL.controller[1][2][2].controller,
			$eyelidUpperOuter_L_CTRL.controller[1][2][3].controller,
			$eyelidUpperOuter_L_CTRL.controller[2][2][1].controller,
			$eyelidUpperOuter_L_CTRL.controller[2][2][2].controller,
			$eyelidUpperOuter_L_CTRL.controller[2][2][3].controller,
			$eyelidLowerOuter_R_CTRL.controller[1][2][1].controller,
			$eyelidLowerOuter_R_CTRL.controller[1][2][2].controller,
			$eyelidLowerOuter_R_CTRL.controller[1][2][3].controller,
			$eyelidLowerOuter_R_CTRL.controller[2][2][1].controller,
			$eyelidLowerOuter_R_CTRL.controller[2][2][2].controller,
			$eyelidLowerOuter_R_CTRL.controller[2][2][3].controller,
			$eyelidLowerInner_R_CTRL.controller[1][2][1].controller,
			$eyelidLowerInner_R_CTRL.controller[1][2][2].controller,
			$eyelidLowerInner_R_CTRL.controller[1][2][3].controller,
			$eyelidLowerInner_R_CTRL.controller[2][2][1].controller,
			$eyelidLowerInner_R_CTRL.controller[2][2][2].controller,
			$eyelidLowerInner_R_CTRL.controller[2][2][3].controller,
			$eyelidLowerInner_L_CTRL.controller[1][2][1].controller,
			$eyelidLowerInner_L_CTRL.controller[1][2][2].controller,
			$eyelidLowerInner_L_CTRL.controller[1][2][3].controller,
			$eyelidLowerInner_L_CTRL.controller[2][2][1].controller,
			$eyelidLowerInner_L_CTRL.controller[2][2][2].controller,
			$eyelidLowerInner_L_CTRL.controller[2][2][3].controller,
			$eyelidLowerOuter_L_CTRL.controller[1][2][1].controller,
			$eyelidLowerOuter_L_CTRL.controller[1][2][2].controller,
			$eyelidLowerOuter_L_CTRL.controller[1][2][3].controller,
			$eyelidLowerOuter_L_CTRL.controller[2][2][1].controller,
			$eyelidLowerOuter_L_CTRL.controller[2][2][2].controller,
			$eyelidLowerOuter_L_CTRL.controller[2][2][3].controller -- END facialRoot_C_OFF
		)
		-- The list of controllers to be copied to
		copyToList = #(
			($facialTransfer*jaw_CTRL)[1].controller[1][1].controller,
			($facialTransfer*jaw_CTRL)[1].controller[1][2].controller,
			($facialTransfer*mouth_CTRL)[1].controller[1][1].controller,
			($facialTransfer*mouth_CTRL)[1].controller[1][2].controller,
			($facialTransfer*tongueRoll_CTRL)[1].controller[1][1].controller,
			($facialTransfer*tongueRoll_CTRL)[1].controller[1][2].controller,
			($facialTransfer*tongueMove_CTRL)[1].controller[1][1].controller,
			($facialTransfer*tongueMove_CTRL)[1].controller[1][2].controller,
			($facialTransfer*tongueInOut_CTRL)[1].controller[1][2].controller,
			($facialTransfer*nose_L_CTRL)[1].controller[1][1].controller,
			($facialTransfer*nose_L_CTRL)[1].controller[1][2].controller,
			($facialTransfer*nose_R_CTRL)[1].controller[1][1].controller,
			($facialTransfer*nose_R_CTRL)[1].controller[1][2].controller,
			($facialTransfer*cheek_L_CTRL)[1].controller[1][2].controller,
			($facialTransfer*cheek_R_CTRL)[1].controller[1][2].controller,
			($facialTransfer*eye_C_CTRL)[1].controller[1][1].controller,
			($facialTransfer*eye_C_CTRL)[1].controller[1][2].controller,
			($facialTransfer*eye_L_CTRL)[1].controller[1][1].controller,
			($facialTransfer*eye_L_CTRL)[1].controller[1][2].controller,
			($facialTransfer*eye_R_CTRL)[1].controller[1][1].controller,
			($facialTransfer*eye_R_CTRL)[1].controller[1][2].controller,
			($facialTransfer*innerBrow_L_CTRL)[1].controller[1][1].controller,
			($facialTransfer*innerBrow_L_CTRL)[1].controller[1][2].controller,
			($facialTransfer*innerBrow_R_CTRL)[1].controller[1][1].controller,
			($facialTransfer*innerBrow_R_CTRL)[1].controller[1][2].controller,
			($facialTransfer*outerBrow_L_CTRL)[1].controller[1][2].controller,
			($facialTransfer*outerBrow_R_CTRL)[1].controller[1][2].controller, -- END faceControls_OFF
			($facialTransfer*CIRC_press)[1].controller[1][1].controller, -- BEGIN facialAttrGUI
			($facialTransfer*CIRC_rollIn)[1].controller[1][1].controller,
			($facialTransfer*CIRC_narrowWide)[1].controller[1][1].controller,
			($facialTransfer*CIRC_clench)[1].controller[1][1].controller,
			($facialTransfer*CIRC_backFwd)[1].controller[1][1].controller,
			($facialTransfer*CIRC_nasolabialFurrowL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_nasolabialFurrowR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_smileR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_smileL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_openSmileR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_openSmileL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_frownR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_frownL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_scream)[1].controller[1][1].controller,
			($facialTransfer*CIRC_lipsNarrowWideR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_lipsNarrowWideL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_lipsStretchOpenR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_lipsStretchOpenL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_chinWrinkle)[1].controller[1][1].controller,
			($facialTransfer*CIRC_chinRaiseUpper)[1].controller[1][1].controller,
			($facialTransfer*CIRC_chinRaiseLower)[1].controller[1][1].controller,
			($facialTransfer*CIRC_closeOuterR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_closeOuterL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_puckerR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_puckerL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_oh)[1].controller[1][1].controller,
			($facialTransfer*CIRC_funnelUR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_funnelDR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_mouthSuckUR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_mouthSuckDR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_pressR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_pressL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_dimpleR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_dimpleL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_suckPuffR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_suckPuffL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_lipBite)[1].controller[1][1].controller,
			($facialTransfer*CIRC_funnelUL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_funnelDL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_mouthSuckUL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_mouthSuckDL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_blinkL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_squeezeR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_squeezeL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_blinkR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_openCloseDR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_squintInnerUR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_squintInnerDR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_openCloseUR)[1].controller[1][1].controller,
			($facialTransfer*CIRC_openCloseDL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_squintInnerUL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_squintInnerDL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_openCloseUL)[1].controller[1][1].controller,
			($facialTransfer*CIRC_thinThick_B)[1].controller[1][1].controller,
			($facialTransfer*CIRC_stickyLips_E)[1].controller[1][1].controller,
			($facialTransfer*CIRC_thinThick_F)[1].controller[1][1].controller,
			($facialTransfer*CIRC_thinThick_H)[1].controller[1][1].controller,
			($facialTransfer*CIRC_thinThick_C)[1].controller[1][1].controller,
			($facialTransfer*CIRC_thinThick_D)[1].controller[1][1].controller,
			($facialTransfer*CIRC_thinThick_G)[1].controller[1][1].controller,
			($facialTransfer*CIRC_stickyLips_A)[1].controller[1][1].controller, -- END FacialAttrGUI
			($FacialTransfer*lipCorner_L_CTRL)[1].controller[1][1].controller, -- BEGIN facialRoot_C_OFF
			($FacialTransfer*lipCorner_L_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*lipCorner_L_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*lipCorner_L_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*lipCorner_L_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*lipCorner_L_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*lipCorner_R_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*lipCorner_R_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*lipCorner_R_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*lipCorner_R_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*lipCorner_R_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*lipCorner_R_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*upperLip_R_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*upperLip_R_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*upperLip_R_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*upperLip_R_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*upperLip_R_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*upperLip_R_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*upperLip_C_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*upperLip_C_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*upperLip_C_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*upperLip_C_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*upperLip_C_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*upperLip_C_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*upperLip_L_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*upperLip_L_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*upperLip_L_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*upperLip_L_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*upperLip_L_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*upperLip_L_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*lowerLip_L_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*lowerLip_L_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*lowerLip_L_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*lowerLip_L_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*lowerLip_L_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*lowerLip_L_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*lowerLip_C_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*lowerLip_C_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*lowerLip_C_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*lowerLip_C_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*lowerLip_C_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*lowerLip_C_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*lowerLip_R_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*lowerLip_R_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*lowerLip_R_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*lowerLip_R_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*lowerLip_R_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*lowerLip_R_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*eyelidUpperOuter_R_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*eyelidUpperOuter_R_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*eyelidUpperOuter_R_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*eyelidUpperOuter_R_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*eyelidUpperOuter_R_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*eyelidUpperOuter_R_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*eyelidUpperInner_R_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*eyelidUpperInner_R_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*eyelidUpperInner_R_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*eyelidUpperInner_R_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*eyelidUpperInner_R_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*eyelidUpperInner_R_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*eyelidUpperInner_L_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*eyelidUpperInner_L_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*eyelidUpperInner_L_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*eyelidUpperInner_L_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*eyelidUpperInner_L_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*eyelidUpperInner_L_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*eyelidUpperOuter_L_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*eyelidUpperOuter_L_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*eyelidUpperOuter_L_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*eyelidUpperOuter_L_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*eyelidUpperOuter_L_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*eyelidUpperOuter_L_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*eyelidLowerOuter_R_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*eyelidLowerOuter_R_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*eyelidLowerOuter_R_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*eyelidLowerOuter_R_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*eyelidLowerOuter_R_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*eyelidLowerOuter_R_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*eyelidLowerInner_R_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*eyelidLowerInner_R_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*eyelidLowerInner_R_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*eyelidLowerInner_R_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*eyelidLowerInner_R_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*eyelidLowerInner_R_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*eyelidLowerInner_L_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*eyelidLowerInner_L_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*eyelidLowerInner_L_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*eyelidLowerInner_L_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*eyelidLowerInner_L_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*eyelidLowerInner_L_CTRL)[1].controller[2][3].controller,
			($FacialTransfer*eyelidLowerOuter_L_CTRL)[1].controller[1][1].controller,
			($FacialTransfer*eyelidLowerOuter_L_CTRL)[1].controller[1][2].controller,
			($FacialTransfer*eyelidLowerOuter_L_CTRL)[1].controller[1][3].controller,
			($FacialTransfer*eyelidLowerOuter_L_CTRL)[1].controller[2][1].controller,
			($FacialTransfer*eyelidLowerOuter_L_CTRL)[1].controller[2][2].controller,
			($FacialTransfer*eyelidLowerOuter_L_CTRL)[1].controller[2][3].controller -- END facialRoot_C_OFF
		)
		
		doTheCopy copyFromList copyToList
		doTheExport()
	)
	on btnGTA5_exportFBXQ pressed do (
		PopupMenu rcEFBX align:#align_vcenterleft
	)
)

rollout roAmbient "Ambient Tools"
(
	button btnAmb_fixMovieplane "Fix Movieplane" width:107 pos:[3, 6]
	button btnAmb_fixMovieplaneQ "?" width:11 pos:[112, 6]
	button btnAmb_setupViewport "Setup Viewports" width:107 pos:[3, 31]
	button btnAmb_setupViewportQ "?" width:11 pos:[112, 31]
	button btnAmb_setupGUI "Setup GUI" width:107 pos:[3, 56]
	button btnAmb_setupGUIQ "?" width:11 pos:[112, 56]
	button btnAmb_exportFBX "Export AMBIENT FBX" width:107 pos:[3, 81]
	button btnAmb_exportFBXQ "?" width:11 pos:[112, 81]
	
	on btnAmb_fixMovieplane pressed do (
		amb_fixMovieplane()
	)
	on btnAmb_setupGUI pressed do (
		amb_setupGUI()
	)
	on btnAmb_exportFBX pressed do (
		amb_exportFBX()
	)
	on btnAmb_setupViewport pressed do (
		amb_setupViewport()
	)
	
)

rollout roParadise4 "Rigging Tools"
(
	button btnGTA5_3LDelete "3L Delete GUIs" width:107 pos:[3, 6]
	button btnGTA5_3LDeleteQ "?" width:11 pos:[112, 6]
	button btnGTA5_3LDelete2 "3L Delete Assets" width:107 pos:[3, 31]
	button btnGTA5_3LDelete2Q "?" width:11 pos:[112, 31]
	button btnGTA5_showHideToggle "Show/Hide Toggle" width:107 pos:[3, 56]
	button btnGTA5_showHideToggleQ "?" width:11 pos:[112, 56]
	button btnGTA5_freezeToggle "Freeze/Melt Toggle" width:107 pos:[3, 81]
	button btnGTA5_freezeToggleQ "?" width:11 pos:[112, 81]
	button btnGTA5_selectionSets "Create Selection Sets" width:107 pos:[3, 106]
	button btnGTA5_selectionSetsQ "?" width:11 pos:[112, 106]
	button btnGTA5_convert2to1way "Convert 2 to 1 Way" width:107 pos:[3, 131]
	button btnGTA5_convert2to1wayQ "?" width:11 pos:[112, 131]
	button btnGTA5_convert1to2way "Convert 1 to 2 Way" width:107 pos:[3, 156]
	button btnGTA5_convert1to2wayQ "?" width:11 pos:[112, 156]
	button btnGTA5_wireDisconnect "Disconnect GUI" width:107 pos:[3, 181]
	button btnGTA5_wireDisconnectQ "?" width:11 pos:[112, 181]
	button btnGTA5_gta5_setupShading "Setup Shading" width:107 pos:[3, 206]
	button btnGTA5_gta5_setupShadingQ "?" width:11 pos:[112, 206]
	button btnGTA5_gta5_setupLayers "Setup Layers" width:107 pos:[3, 231]
	button btnGTA5_gta5_setupLayersQ "?" width:11 pos:[112, 231]
	button btnGTA5_gta5_addNamespace "Add Namespace" width:107 pos:[3, 256]
	button btnGTA5_gta5_addNamespaceQ "?" width:11 pos:[112, 256]
	button btnGTA5_gta5_fixUIPos "Del Template Objects" width:107 pos:[3, 281]
	button btnGTA5_gta5_fixUIPosQ "?" width:11 pos:[112, 281]
	button btnGTA5_gta5_cleanAmbient "Prep Ambient" width:107 pos:[3, 306]
	button btnGTA5_gta5_cleanAmbientQ "?" width:11 pos:[112, 306]
	
	on btnGTA5_gta5_cleanAmbient pressed do
	(
		print "Preparing character"
		pedMeshes = showSkinningAndBones()
		sortAndSwitch pedMeshes
		writeSelectionSets()
		cleanAmbient()
	)
		
	--DELETE GUI BUTTON
	on btnGTA5_3LDelete pressed do (
		gta5_3LDelete()
	)
	on btnGTA5_3LDeleteQ pressed do (
		PopupMenu rc3LD align:#align_vcenterleft
	)
		
	--DELETE UNUSED ASSETS BUTTON
	on btnGTA5_3LDelete2 pressed do (
		gta5_3LDelete2()
	)
	on btnGTA5_3LDelete2Q pressed do (
		PopupMenu rc3LD2 align:#align_vcenterleft
	)
		
	--SHOW/HIDE SELECTION TOGGLE BUTTON
	on btnGTA5_showHideToggle pressed do (
		gta5_showHide()
	)
	on btnGTA5_showHideToggleQ pressed do (
		PopupMenu rcSHT align:#align_vcenterleft
	)
		
	--SHOW/HIDE SELECTION TOGGLE BUTTON
	on btnGTA5_freezeToggle pressed do (
		gta5_freeze()
	)
	on btnGTA5_freezeToggleQ pressed do (
		PopupMenu rcFREEZE align:#align_vcenterleft
	)
		
	--CREATE SELECTION SETS BUTTON
	on btnGTA5_selectionSets pressed do (
		gta5_selectionSets()
	)
	on btnGTA5_selectionSetsQ pressed do (
		PopupMenu rcSSETS align:#align_vcenterleft
	)
		
	--CONVERT 2WAY TO 1WAY BUTTON
	on btnGTA5_convert2to1way pressed do (
		gta5_convert2WayTo1Way()
	)
	on btnGTA5_convert2to1wayQ pressed do (
		PopupMenu rcC2to1 align:#align_vcenterleft
	)
		
	--CONVERT 1WAY TO 2WAY BUTTON (detect)
	on btnGTA5_convert1to2way pressed do (
		gta5_wireConvert()
	)
	on btnGTA5_convert1to2wayQ pressed do (
		PopupMenu rcC1to2 align:#align_vcenterleft
	)
		
	--DISCONNECT GUI BUTTON
	on btnGTA5_wireDisconnect pressed do (
		gta5_wireDisconnect()
	)
	on btnGTA5_wireDisconnectQ pressed do (
		PopupMenu rcDG align:#align_vcenterleft
	)
		
	--SETUP SHADING BUTTON
	on btnGTA5_gta5_setupShading pressed do (
		gta5_setupShading()
	)
	on btnGTA5_gta5_setupShadingQ pressed do (
		PopupMenu rcSSh align:#align_vcenterleft
	)
		
	-- SETUP LAYERS BUTTON
	on btnGTA5_gta5_setupLayers pressed do (
		gta5_setupLayers()
	)
	on btnGTA5_gta5_setupLayersQ pressed do (
		PopupMenu rcSLay align:#align_vcenterleft
	)
	
	on btnGTA5_gta5_addNamespace pressed do (
		amb_addNamespace()
	)
	on btnGTA5_gta5_setupLayersQ pressed do (
		print "not implemented"
	)
	
	on btnGTA5_gta5_fixUIPos pressed do (
		amb_fixUIPos()
	)
	on btnGTA5_gta5_fixUIPosQ pressed do (
		print "not implemented"
	)
	
	
)

gta5_animToolboxFloater = newRolloutFloater "Toolbox" 140 655

addRollout roParadise gta5_animToolboxFloater rolledUp:false
addRollout roParadise2 gta5_animToolboxFloater rolledUp:false
addRollout roParadise3 gta5_animToolboxFloater rolledUp:false
addRollout roParadise5 gta5_animToolboxFloater rolledUp:false
addRollout roAmbient gta5_animToolboxFloater rolledUp:false
addRollout roParadise4 gta5_animToolboxFloater rolledUp:true
