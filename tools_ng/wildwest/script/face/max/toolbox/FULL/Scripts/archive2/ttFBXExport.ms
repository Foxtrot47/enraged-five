fn exportFBX_init = (
	select (($con* as array) + ($IM_* as array))
	FbxExporterSetParam "Animation" true
	FbxExporterSetParam "EmbedTextures" false
	FbxExporterSetParam "Preserveinstances" false
	FbxExporterSetParam "ConvertUnit" "cm"
	FbxExporterSetParam "UpAxis" "Y"
	FbxExporterSetParam "FileVersion" "FBX201000"
	FbxExporterSetParam "ASCII" false
	exportFile (substituteString (maxfilepath+maxfilename) ".max" ".fbx") #noPrompt selectedOnly:true
)