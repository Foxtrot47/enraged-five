/****************************************************************************************/
-- Title: 3DS Max Facial Animation Tools
-- Version: 1.5
-- Author: Jay Grenier, Thanh Phan 2009-2012
-- Tooltip: 
-- Contains cleanup and setup tools useful to facial animators.
/****************************************************************************************/


/* RUN SCRIPTS **********************************************************/
	--fileIn ("Scripts/jgSceneSetup.ms")
	--fileIn ("Scripts/jgFilterSetup.ms")
	--fileIn ("Scripts/jgLoadImPlugin.ms")
	--fileIn ("Scripts/jgSmoothKeys.ms")
	--fileIn ("Scripts/jgConvertApex.ms")
	--fileIn ("Scripts/jgSetToZero.ms")
	--fileIn ("Scripts/jgRandomizeKeys.ms")
	--fileIn ("Scripts/jgSpreadKeys.ms")
	--fileIn ("Scripts/jgBreakItDown_RockstarJoysticks.ms")
	--fileIn ("Scripts/imResetJoysticks.ms")
	--fileIn ("Scripts/ttFBXExport.ms")
	--fileIn ("Scripts/IM_MP3_animationExitScript.ms")
	--fileIn ("X:/payne/tools/wildwest/script/max/rockstar_sandiego/cutscene/face/OpenMeCreation/scripts/IM_MP3_buildOPENME_RSTAR_BRIG.ms")
	--fileIn ("Scripts/tpAltGuiToggle.ms")
	--fileIn ("Scripts/displayMP3.ms")
	--fileIn ("Scripts/mp3ViewportLayout.ms")
	--fileIn ("Scripts/controlSelectionSetsMP3.ms")
	--fileIn ("Scripts/mp3_clearRectKeys.ms")
	--fileIn ("Scripts/mp3_delControllerRots.ms")
	--fileIn ("Scripts/tpMP3_LockLimits_RSTAR.ms")
	

	/* SET AUTOKEY MODE ****************************************************************/
	if(animButtonState == false)then(animButtonState = true)
	
	
	
	
	fileIn ("Scripts/gta5_animTools.ms")
	fileIn ("Scripts/gta5_riggingTools.ms")
	fileIn ("Scripts/Pose2Pose_RC2_noScale.ms")
	fileIn ("Scripts/gta5_RCMenus.ms")
	
	fileIn ("Scripts/gta5_buildToolboxUI.ms")