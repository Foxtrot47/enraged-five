-- Example Wildwest Script
-- Rick Stirling
-- Rockstar North
-- This script is an example of how the 2011 revision of the Wildwest should work

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/config_files/Wildwest_header.ms") -- This is fast


-- Use some functions from common_functions.ms
print (uppercase "rick")
buildPointHelper "RicksHelper"  [0,0,1] 2 (color 255 0 0)

