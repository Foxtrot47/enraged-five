filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

try( destroyDialog RsRefTintRandom )catch()
rollout RsRefTintRandom "RandomTint"
(
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:RsRefTintRandom.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Prop Random Tint" filename:(getThisScriptFilename())
	
	group ""
	(
		spinner spnRandom "Range:" range:[0,7,7] type:#integer width:40 offset:[-16, 0]
		button btnDo "Randomise Tint" width:180
	)
	
	on RsRefTintRandom open do banner.setup()
	--on rsBannerPanel open do banner.setup()
	
	on btnDo pressed do
	(
		--the attr
		tintAttrIdx = getAttrIndex "Gta Object" "Tint"
		
		range = spnRandom.value
		--randomisee
		for obj in $selection where classOf obj == RSrefObject do
		(
			seed (random 0 9999999)
			tint = random 0 range
			setAttr obj tintAttrIdx tint
		)
	)
)

createDialog RsRefTintRandom 200 120