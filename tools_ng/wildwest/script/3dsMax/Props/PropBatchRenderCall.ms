-- This script is called for each maxfile listed in a batch prop-render session:
filein (RsConfigGetWildwestDir() + "script/3dsmax/Props/PropRenderUtils.ms")
(
	local propRenderUtilInst = RsPropRenderUtils()
	propRenderUtilInst.RenderObjs (geometry as array) ::g_propRendererSettingsObject
)