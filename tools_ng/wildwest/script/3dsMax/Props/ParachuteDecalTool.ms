filein "pipeline/export/AP3Asset.ms"
filein "pipeline/export/AP3Invoke.ms"

struct DNStyles
(
	textClr = (colorMan.getColor #windowText) * 255,
	windowClr = (colorMan.getColor #window) * 255,
	backgroundClr = (colorMan.getColor #background) * 255,
	dnColour = dotNetClass "System.Drawing.Color",
	backColor = (dotNetClass "System.Drawing.Color").FromArgb (backgroundClr[1] + 10) (backgroundClr[2] + 10) (backgroundClr[3] + 10),
	foreColor = (dotNetClass "System.Drawing.Color").FromArgb textClr[1] textClr[2] textClr[3],
	dnColourExecute = dnColour.FromArgb 51 181 229,
	dnColourSuccess = dnColour.FromArgb 153 204 0,
	dnColourError = dnColour.FromArgb 255 68 68,
	dnColourDormant = dnColour.FromArgb (backgroundClr[1] - 10) (backgroundClr[2] - 10) (backgroundClr[3] - 10),
	
	stdButtonHeight = 24,
	
	fn setExecuteStyle ctrl =
	(
		ctrl.FlatAppearance.BorderColor = dnColourExecute
		windows.processPostedMessages()
	),
	
	fn setSuccessStyle ctrl =
	(
		ctrl.FlatAppearance.BorderColor = dnColourSuccess
		windows.processPostedMessages()
	),
	
	fn setErrorStyle ctrl =
	(
		ctrl.FlatAppearance.BorderColor = dnColourError
		windows.processPostedMessages()
	),
	
	fn setDormantStyle ctrl =
	(
		ctrl.FlatAppearance.BorderColor = dnColourDormant
		windows.processPostedMessages()
	),
	
	fn initButtonStyle ctrl =
	(
		ctrl.FlatStyle = (dotNetObject "Button").FlatStyle.Flat
		ctrl.FlatAppearance.BorderSize = 2
		ctrl.FlatAppearance.BorderColor = dnColourDormant
		ctrl.backColor = backColor
		ctrl.foreColor = foreColor
		windows.processPostedMessages()
	)
	
)

dnStyle = DNStyles()

struct ParachuteTool
(
	
	outputItd = RsConfigGetAssetsDir() + "export/textures/parachute_decals.itd.zip",
	
	textureExports = RsConfigGetAssetsDir() + "textures/props/",
	metadatapath = RsConfigGetAssetsDir() + "metadata/textures/miscs/parachute_decals/",
	
-- 	textures = #(@"X:\gta5\art\ng\textures\Props\lev_des\skinned\EAGLE_PARACHUTE_DEC.tga",
-- 						@"X:\gta5\art\ng\textures\Props\lev_des\skinned\EAGLEHEAD_PARACHUTE_DEC.tga",
-- 						@"X:\gta5\art\ng\textures\Props\lev_des\skinned\FLAG_PARACHUTE_DEC.tga"),
	textureIni = (RsConfigGetWildWestDir() + "etc/config/Props/Parachute_Decals.ini"),
	textures = getIniSetting textureIni "paths",
						
	
	fn process =
	(
		gRsPerforce.sync #(outputItd)
		local changeListNum = gRsPerforce.createChangelist "Parachute_decals.itd.zip update"
		gRsPerforce.add_or_edit #(outputItd)
		gRsPerforce.addToChangelist changeListNum outputItd
		
		--create an asset file
		asset = AP3AssetFile()
		asset.Initialize()
							
		local textureArchive = asset.AddZipArchive outputItd
							
							
		--convert the textures
		for tex in textures do
		(
			--local sourcePath = @"X:\gta5\art\ng\textures\Props\lev_des\skinned\EAGLE_PARACHUTE_DEC.tga"
			local barename = (RsRemovePathAndExtension tex)
			local targetPath = textureExports + barename + ".tif"
			rexExportRawTextureAsTiff tex targetPath
			
			local tcloutputfile = textureExports + barename + ".tcl"
			local tcsLocation = metadatapath + barename + ".tcs"
			
			RsGenerateTemplateReferenceFile tcsLocation @"${RS_ASSETS}/metadata/textures/templates/weapons/Diffuse" noop:false sourceTextures:#(tex) exportTexture:targetPath useSCC:false
			
			local tcsPathintcl = (substituteString metadatapath (RsConfigGetAssetsDir()) "$(assets)/") + barename
			RsGenerateTemplateReferenceFile tcloutputfile tcsPathintcl
			
			asset.AddFile textureArchive targetPath
			asset.AddFile textureArchive tcloutputfile
		)
		
		--RsGenerateTemplateReferenceFile (tcsreference + ".tcs") @"${RS_ASSETS}/metadata/textures/templates/peds/Streamed/MP_NormalMap" noop:false sourceTextures:#(sourceTexturePath) exportTexture:f useSCC:false
		
		
		/*
		local overlayArchive = overlayAssetFile.AddZipArchive outputDir
		
		local itdZipName = basePath + ".itd.zip"
		local textureArchive = asset.AddZipArchive itdZipName
		
		asset.AddFile textureArchive tcloutputfile
		asset.AddFile overlayArchive itdZipName --might not need this outer layer
		*/
		
		local itdBuilt = asset.BuildAssetFiles()
		
		
		--build the platform data
		AP3Invoke.convert #(outputItd)  debug:true
		
		
		gRsPerforce.submitOrDeleteChangelist changeListNum
		
		
		return itdBuilt
	),
	
	
	on create do
	(
		gRsPerforce.sync #(textureIni)
	)
	
)

--/////////////////////////////////////////
--	UI
--/////////////////////////////////////////
try(destroyDialog ParachuteToolUI)catch()
rollout ParachuteToolUI "Parachute Decal Tool" width:450 height:250
(
	--/////////////////////////////////////////
	--	VARIABLES
	--/////////////////////////////////////////
	local toolStruct = ParachuteTool()
	
	--/////////////////////////////////////////
	--	CONTROLS
	--/////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:ParachuteToolUI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"ParachuteTool" versionNum:0.2 versionName:"Cloud Face" filename:(getThisScriptFilename())
		
	timer ctrlState interval:5000 active:false
		
	listbox lstDecalPaths items:toolStruct.textures
	
	dotnetcontrol btnAdd "Button" text:"Add" width:(ParachuteToolUI.width * 0.5 - 5) height:dnStyle.stdButtonHeight offset:[-10, 0] across:2
	dotnetcontrol btnRemove "Button" text:"Remove" width:(ParachuteToolUI.width * 0.5 - 5) height:dnStyle.stdButtonHeight offset:[0, 0]
		
	dotnetcontrol btnProcess "Button" text:"Process" width:(ParachuteToolUI.width - 10) height:dnStyle.stdButtonHeight offset:[-10, 0]
	
	
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////
	fn init = 
	(
		dnStyle.initButtonStyle btnAdd
		dnStyle.initButtonStyle btnRemove
		dnStyle.initButtonStyle btnProcess
	)
	
	--/////////////////////////////////////////
	--	EVENTS
	--/////////////////////////////////////////
	on btnAdd click do
	(
		print "eh"
		local texPath = getOpenFileName caption:"Decal Path" types:"Content (*.*)|*.*"
		if (texPath == undefined) then
		(
			return false
		)
		else
		(
			texPath = (RsMakeSafeSlashes texPath)
			if (findItem toolStruct.textures texPath == 0) then
			(
				append toolStruct.textures texPath
				gRsPerforce.add_or_edit #(toolStruct.textureIni)
				setIniSetting toolStruct.textureIni "paths" texPath ""
				lstDecalPaths.items = toolStruct.textures
			)
		)
	)
	
	on btnRemove click do
	(
		item = lstDecalPaths.selected
		gRsPerforce.add_or_edit #(toolStruct.textureIni)
		delINISetting toolStruct.textureIni "paths" item
		idxRemove = finditem toolStruct.textures item
		if (idxRemove != 0) then
		(
			deleteItem toolStruct.textures idxRemove
			lstDecalPaths.items = toolStruct.textures
		)
	)
	
	on btnProcess click do
	(
		dnStyle.setExecuteStyle btnProcess
		local success = toolStruct.process()
		ctrlState.active = true
		if success do dnStyle.setSuccessStyle btnProcess
	)
	
	on ctrlState tick do
	(
		dnStyle.setDormantStyle btnProcess
		ctrlState.active = false
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on ParachuteToolUI open do
	(
		banner.setup()
		init()
	)
	
	
)
createDialog ParachuteToolUI
