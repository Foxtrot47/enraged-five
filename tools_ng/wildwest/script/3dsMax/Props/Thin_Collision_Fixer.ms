-- ****************************************************************************************
-- Thin collision fixer
-- Rick Stirling
-- Rockstar North 
-- July 2012
-- A tool to detect thin collision on objects and fix it automatically
--Updated October 2012 by Stewar Wright.  UI added.
-- ****************************************************************************************
--Fucntions
-- ****************************************************************************************
fn has_thin_collision collisionObj collisionThresholdSize = 
(	
	thinCollision = false
	if collisionObj.width < collisionThresholdSize do thinCollision = true
	if collisionObj.height < collisionThresholdSize do thinCollision = true
	if collisionObj.length < collisionThresholdSize do thinCollision = true
	thinCollision
)--end has_thin_collision

fn fix_thin_collision collisionObj collisionThresholdSize = 
(	
	if collisionObj.width < collisionThresholdSize do collisionObj.width = collisionThresholdSize
	if collisionObj.height < collisionThresholdSize do collisionObj.height = collisionThresholdSize
	if collisionObj.length < collisionThresholdSize do collisionObj.length = collisionThresholdSize
)--end fix_thin_collision
-- ****************************************************************************************
--UI
-- ****************************************************************************************
	if ((thinCollFix != undefined) and (thinCollFix.isDisplayed)) do
	(destroyDialog thinCollFix)
-- ****************************************************************************************
rollout thinCollFix "Thin Collision Fixer"
(
	dotNetControl rsBannerPanel "Panel" pos:[0,0] width:thinCollFix.Width height:32
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"thinCollFix" filename:(getThisScriptFilename())

	spinner spnCollThreshold "Threshold" width:100 height:22 type:#float range:[0,100,0.02] scale:0.01 align:#left
	button btnFixThin "Fix Thin Collision" width:125 height:35 tooltip:"Fix thin collision on selected."

	on ThinCollFix open do banner.setup()

	on btnFixThin pressed do
	(
		for collObj in selection do
		(
			if (classof collObj == Col_Box) do fix_thin_collision collObj spnCollThreshold.value -- if this is a collision object we'll check its collision
		)
		redrawViews()--redraw to show the changes
	)
)
-- ****************************************************************************************
createDialog thinCollFix width:135