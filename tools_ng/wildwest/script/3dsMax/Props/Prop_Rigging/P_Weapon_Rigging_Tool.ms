-- This script builds a custom bone rig from a set of helpers
-- Rockstar North
-- Rick Stirling April 2009

--  Updated October 2009: Builds bones based on feedback from props team
--  Updated May 2010: Loads data from a different location

-- June 11th 2010: Attachable accesory support.

-- Switches off bone mode so that the bones can translate
-- Use XML Lookup

-- ****************************************************************************************
-- ****************************************************************************************
-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

-- Load the common maxscript functions 
global zaxis = [0,-1,0]

global WeaponXMLPath=theWildWest + "etc/config/props/WeaponDOFs.xml"

global weapon_mover_mesh = theProjectWeaponsFolder + "Weapon_Rigging/Weapon_Mover_Mesh.max"

-- ***************************************************************************
-- Arrays are used to hold the markers.
-- This will become XML at some point.
weapon_bonelist =#()

spacercount = 0
rndspace =0

-- ****************************************************************************************
-- Set up some variables for the bones
-- Colours, just some useful defaults

global colourRoot = (color 255 128 0) 
global colourMarker = (color 5 5 135) 

global colourBoneEdit = (color 255 35 35) 
global colourBoneLocked = (color 10 10 10) 

-- useful default widths
defaultbonewidth = 0.05
defaultfinsize = 0.002
thinbone = 0.005
nofin = 0
smalldummy = 0.02
defaultTwist = 90


-- ****************************************************************************************
-- ****************************************************************************************
-- ****************************************************************************************
fn findWeaponDOFSettings xmlPath findBone=
--xmlPath = the path of the xml file we are going to read
--findBone = the bone we're looking for some values for
(
	xmlWeaponNames = #()--an array to hold our xml data
	xmlStream = xmlStreamHandler xmlFile:xmlPath --set up xmlStreamHandler struct
	xmlStream.open()--open xml file
	nodes=xmlStream.root.SelectNodes("weaponDOF")--collect all the lines with "weaponDOF"
	
	for thisNode=0 to (nodes.count - 1) do--for all the nodes we found go and collect some data
	(
		xmlName = nodes.itemOf[thisNode].getAttribute "name"--collect the name value
		if (upperCase xmlName) == (upperCase findBone) do--if we find a match for the bone
			(
				xmlExRot =  nodes.itemOf[thisNode].getAttribute "exportRotate"--collect the exportRotate value
				xmlExTran = nodes.itemOf[thisNode].getAttribute "exportTrans"--collect the exportTrans value
				xmlWeaponNames = #(xmlExRot, xmlExTran)--add the data to an array
			)
	)

	if xmlWeaponNames.count == 0 do
	(
		print (findBone + " not found!")--do a check to make sure we didn't lose anything
		xmlWeaponNames = #("false", "false")--we'll store some null values so we can cope with bones that don't exist
		print ("Temp transform and rotate values set for bone " + (findBone))
	)
	xmlStream.close()--close the xml file
	xmlWeaponNames--pass out the xml data
)--end findWeaponDOFSettings

-- ****************************************************************************************
-- Function to create a bone, parent it, and make it a bone/non-bone
fn createWeaponBone bonename bonestart boneend currentparent bonemode= (
	foundWeaponDOFSettings = findWeaponDOFSettings WeaponXMLPath bonename --search the weapon xml file for default values
	
	transformtag = "tag = " + (bonename as string)--add the bonetag
	transformtag = transformtag +"\r\n"+ "exportTrans = " + foundWeaponDOFSettings[2]--add the export rotate value
	transformtag = transformtag +"\r\n"+ "exportRotate = " + foundWeaponDOFSettings[1]--add the export trans value
	
	-- Create the main gun bones.
	buildNewBoneRA bonename "" bonestart boneend 0.01 0.01 colourBoneEdit currentparent defaultTwist -1
	$.boneEnable=bonemode

	setUserPropbuffer $ transformtag
)

-- Align the root bone to the hand helper
fn realignroot = (
	try (
		-- Does the alignment helper exist?
		select $PH_R_Hand
	)
	
	catch (
		messagebox "Could not find PH_R_Hand helper for alignment"
	)
	
	try (
		-- Does the root buffer bone exist?
		thegrip = getnodebyname "Gun_GripR"
		select thegrip
		
		thegrip.parent = undefined
		thegrip.transform = $PH_R_Hand.transform
		thegrip.parent = $Gun_Root
	)
	
	catch (
		messagebox "Could not find the Gun_Root/Gun_Grip to align"
	)
)

-- ****************************************************************************************
fn buildWeaponRigFM =
(
	-- Can we auto align the root by using the mesh object position.
	-- Store the position of the root marker as 
	meshroot = undefined
	rootpos = $Marker_Root.pos
	
	if $ == undefined or (superclassof $ != GeometryClass) then (
		messagebox "No Weapon selected. Please select a weapon mesh."
		return 0 
	)
	
	else (
		meshroot = $
		try (rootpos = $.pos) catch()
		
		-- Delete existing rig
		deleteExistingRig()
		
		weapon_bonelist =#()
		currentparent=undefined
		
		
		-- Create the root bone
		-- This is the container for the rig
		bonename = "Gun_Root"
		thebone = buildPointHelper bonename rootpos 0.04 colourRoot
		
		foundWeaponDOFSettings = findWeaponDOFSettings WeaponXMLPath bonename --search the weapon xml file for default values
	
		transformtag = "tag = " + (bonename as string)--add the bonetag
		transformtag = transformtag +"\r\n"+ "exportTrans = " + foundWeaponDOFSettings[2]--add the export rotate value
		transformtag = transformtag +"\r\n"+ "exportRotate = " + foundWeaponDOFSettings[1]--add the export trans value

		setUserPropBuffer (getnodebyname bonename) transformtag

		-- auto align?
		if meshroot != undefined then thebone.transform = meshroot.transform
			
		-- This is the new parent bone for the main bone
		currentparent = (getnodebyname bonename)
		append weapon_bonelist thebone

		-- Create the Right grip bone
		-- This is the attachpoint for the weapon to the hand
		bonename = "Gun_GripR"
		thebone = buildPointHelper bonename $Marker_Root.pos 0.02 colourRoot

		foundWeaponDOFSettings = findWeaponDOFSettings WeaponXMLPath bonename --search the weapon xml file for default values
	
		transformtag = "tag = " + (bonename as string)--add the bonetag
		transformtag = transformtag +"\r\n"+ "exportTrans = " + foundWeaponDOFSettings[2]--add the export rotate value
		transformtag = transformtag +"\r\n"+ "exportRotate = " + foundWeaponDOFSettings[1]--add the export trans value
		
		setUserPropBuffer (getnodebyname bonename) transformtag
		thebone.Box = on

		-- Try to automatically align the right grip bone to the hand.
		realignRoot()	

		thebone.parent = currentparent -- the grip is a child of the root.

		-- This is the new parent bone for the main bone
		currentparent = thebone
		append weapon_bonelist thebone

		-- Create the Left grip bone if required
		-- This is the attachpoint for the weapon to the hand
		-- only build this is there is a left hand in the scene
		if($PH_L_Hand != undefined) then
		(
			bonename = "Gun_GripL"
			thebone = buildPointHelper bonename $PH_L_Hand.pos 0.02 colourRoot
			
			foundWeaponDOFSettings = findWeaponDOFSettings WeaponXMLPath bonename --search the weapon xml file for default values
	
			transformtag = "tag = " + (bonename as string)--add the bonetag
			transformtag = transformtag +"\r\n"+ "exportTrans = " + foundWeaponDOFSettings[2]--add the export rotate value
			transformtag = transformtag +"\r\n"+ "exportRotate = " + foundWeaponDOFSettings[1]--add the export trans value
			
			setUserPropBuffer (getnodebyname bonename) transformtag
			thebone.Box = on
			thebone.transform = $PH_L_Hand.transform
			thebone.parent = currentparent.parent -- the grip is a child of the root.

			append weapon_bonelist thebone
		)

		-- Create the Main bone
		-- This is the bone that carries most of the skinning for the rig
		bonename = "Gun_Main_Bone"
		thebone = buildPointHelper bonename $Gun_Root.pos 0.06 colourRoot
		thebone.Box = on
		
		foundWeaponDOFSettings = findWeaponDOFSettings WeaponXMLPath bonename --search the weapon xml file for default values
	
		transformtag = "tag = " + (bonename as string)--add the bonetag
		transformtag = transformtag +"\r\n"+ "exportTrans = " + foundWeaponDOFSettings[2]--add the export rotate value
		transformtag = transformtag +"\r\n"+ "exportRotate = " + foundWeaponDOFSettings[1]--add the export trans value
		
		setUserPropBuffer (getnodebyname bonename) transformtag
		append weapon_bonelist thebone

		-- This is the new parent bone for the rest of the rig
		-- The artists will then need to manually set up the rest of the hierarchy
		thebone.parent = currentparent
		currentparent = thebone

		-- parent the Butt bone
		$NM_Butt_Marker.parent = currentparent
		append weapon_bonelist $NM_Butt_Marker

		-- Build the rest of the rig
		-- First, find a list of all the markers that end in _S
		-- We will be using these to generate the rig
		weaponmarkerlist =#()
		for gunmarker in helpers do (
			thename = gunmarker.name
			prefix = substring thename 1 6
			suffix = substring thename (thename.count-1) 2 
			if ((prefix == "Marker") and (suffix == "_S")) then append weaponmarkerlist gunmarker
		)
		
		for wb = 1 to weaponmarkerlist.count do (
			thename = weaponmarkerlist[wb].name
			nosuffix = substring thename 1 (thename.count-2)  
			corename = substring nosuffix 8 255
			bonename = "Gun_" + corename
			
			bonestart = (getnodebyname ("Marker_" + corename + "_S")).pos
			boneend = (getnodebyname ("Marker_" + corename + "_E")).pos
			createWeaponBone bonename bonestart boneend currentparent false	
			(getnodebyname bonename).parent = currentparent

			append weapon_bonelist $
		)
		
		-- Create and place the rootdummy
		thedummy = buildDummyHelper "Dummy01" [0,0,0] 0.01
		thedummy.transform.position = $Gun_Root.transform.position

		-- Create and place the mover
		themover = mergemaxfile weapon_mover_mesh 
		$Mover.position = $Gun_Root.transform.position
		
		$Mover.parent = thedummy
		$Gun_Root.parent = $Mover
		
		-- set the mesh pivot correctly.
		AlignPivotTo meshroot thedummy

		-- reset transforms
		ResetXForm meshroot
		maxOps.CollapseNode meshroot off
	)
)

-- ****************************************************************************************
-- Build a single bone from the selected marker(s)
fn buildweaponBoneFM theHelper =
(
	markername= theHelper.name
	checkMarker = lowercase (substring markername 1 6)
	
	if checkMarker == "marker" then
	(
		-- Get marker name
		mlength = markername.count
		bonetype= (lowercase (substring markername 8 ((mlength - 2) - 7)))
		bonename= "gun_" + bonetype
		
		bonestart = (getnodebyname ("Marker_" + bonetype + "_S")).pos
		boneend = (getnodebyname ("Marker_" + bonetype + "_E")).pos
		
		currentparent = undefined	
		try (currentparent =$'Gun_Main_Bone') catch ()

		createWeaponBone bonename bonestart boneend currentparent true	
		(getnodebyname bonename).parent = currentparent	
	)
	else (messagebox "Does not appear to be a weapon marker")
)--end buildweaponBoneFM

-- ****************************************************************************************
fn BuildAttachmentDummy theAAP =
(
	if (substring theAAP.name 1 3) != "AAP"  then 
	(
		messagebox "Please select a valid AAP helper"
	)
	else
	(
		clearselection()
		theattdummy = buildDummyHelper "AttDummy01" [0,0,0] 0.0075
		theattdummy.transform = theAAP.transform
		theAAP.parent = theattdummy
		theattdummy.name = "Dummy_" + theAAP.name
	)
)--end BuildAttachmentDummy

-- ****************************************************************************************
fn AlignAttachmentAAP =
(
	-- Needs two objects
	sel = getcurrentselection()
	if sel.count != 2 then (
		messagebox "Need to select 2 objects: mesh and AAP helper"
	)
	else (
		themesh = undefined
		theaap = undefined
		
		for i = 1 to 2 do (
			obj = sel[i]
			if superclassof obj == GeometryClass then themesh = obj
			if superclassof obj == helper then theaap = obj
		)	
		
		AlignPivotTo themesh theaap
		
		-- reset transforms
		ResetXForm themesh
		maxOps.CollapseNode themesh off
		
		clearSelection()
	)
)--end AlignAttachmentAAP

-- ****************************************************************************************
fn buildAttachmentRigFM =
(
	-- Find a root bone
	theattachmentroot = undefined
	try (
		select $Gun_Main_Bone
		theattachmentroot = $
	) catch()
	
	-- This is the new parent bone for the main bone
	currentparent = theattachmentroot

	-- Build the rest of the rig
	-- First, find a list of all the markers that end in _S
	-- We will be using these to generate the rig
	weaponmarkerlist =#()
	for gunmarker in helpers do
	(
		thename = gunmarker.name
		prefix = substring thename 1 8
		suffix = substring thename (thename.count-1) 2 
		if ((prefix == "APMarker") and (suffix == "_S")) then append weaponmarkerlist gunmarker
	)
	
	for wb = 1 to weaponmarkerlist.count do
	(
		thename = weaponmarkerlist[wb].name
		nosuffix = substring thename 1 (thename.count-2)  
		corename = substring nosuffix 10 255
		bonename = "Gun_" + corename
		
		bonestart = (getnodebyname ("APMarker_" + corename + "_S")).pos
		boneend = (getnodebyname ("APMarker_" + corename + "_E")).pos
		createWeaponBone bonename bonestart boneend currentparent false	
		(getnodebyname bonename).parent = currentparent
	)
)--buildAttachmentRigFM

-- ****************************************************************************************
fn cloneSelectedAttach theAAP = (
	
	if (substring theAAP.name 1 3) != "AAP"  then
	(
		messagebox "Please select a valid AAP helper"
	)
	else
	(
		shortapname = substring theAAP.name 1 3

		-- Delete existing helper if it already exists
		wapname = "W" + (substring theAAP.name 2 255)
		for wap in helpers do
		(
			if wap.name == wapname then delete wap
		)

		-- Create the new WAP
		apclone = copy ap
		apclone.name = wapname

		-- Try to parent it to the gun main bone
		try (apclone.parent = $Gun_Main_Bone) catch()
	)
)--end CloneSelectedAttach

-- ****************************************************************************************
fn hideShowWMarkers hideState =
(
	try (
		selectByWildcard "Marker_"
		selectedMarkers = getCurrentSelection()
		selectByWildcard "MAIN_BONE_Marker"
		addThis = getCurrentSelection()
		join selectedMarkers addThis

		if hideState == true then (hide selectedMarkers) else (unhide selectedMarkers)
	)
	catch()
)--end hideShowWMarkers

-- ****************************************************************************************
-- Takes a true/false setting for boxmode 
-- Should become a common function perhaps?
fn boxModeToggle boxState =
(
	try
	(
		selectByWildCard "Gun_"
		selectedBones = getCurrentSelection()
		selectedBones.boxmode = boxState
	)catch ()
)--end boxModeToggle

-- ****************************************************************************************
--bonestate = true or false value
--toggles bone mode and changes bone colour to identify bone mode state
fn boneEditToggle boneState =
(
	boneColour = (color 255 35 35) 
	
	case boneState of
	(
		true:
			boneColour = (color 255 35 35) 
		false:
			boneColour = (color 10 10 10) 
	)

	try
	(
		selectByWildCard "Gun_"
		selectedBones = getCurrentSelection()
		clearSelection()
		BoneEditModeToggle selectedBones boneState boneColour
	)catch ()
)--end boneEditToggle

-- ****************************************************************************************
fn addSkinToWeapon meshToSkin =
(
	modPanel.addModToSelection (Skin ()) ui:on
	meshToSkin.modifiers[#Skin].bone_Limit = 4
	meshToSkin.modifiers[#Skin].showNoEnvelopes = on
			
	for wbone = 1 to weapon_bonelist.count do
	(
		print wbone
		try (skinOps.addBone meshToSkin.modifiers[#Skin] weapon_bonelist[wbone] 1) catch ()
	)	

	-- Set the Attributes:
	indexIsDynamic = GetAttrIndex "Gta Object" "Is Dynamic"
	DynamicGet = GetAttr meshToSkin indexIsDynamic
	DynamicGet = SetAttr meshToSkin indexIsDynamic true
)--end addSkinToWeapon

-- ****************************************************************************************
-- Delete existing rig
fn deleteExistingRig =
(
	deleteThisRig = #()

	selectByWildCard "Gun_"
	gunBones = getCurrentSelection()
	join deleteThisRig gunBones
	selectByWildCard "Dummy01"
	dummyBones = getCurrentSelection()
	join deleteThisRig dummyBones
	selectByWildCard "mover"
	moverBones = getCurrentSelection()
	join deleteThisRig moverBones
	try (delete deleteThisRig) catch()
)--end deleteExistingRig

-- ****************************************************************************************
-- If the bones are in the skin modifier, keep them: if not, delete them
fn deleteUnusedBones thisMesh =
--thisMesh=the skinned mesh
(
	if isMeshSkinned thisMesh == true then
	(
		skinnedBones = #()

		theSkin = thisMesh.modifiers[#skin]
		
		for skinBone=1 to skinOps.GetNumberBones theSkin do
		(
			realBone = getBoneFromSkin theSkin skinBone --find a real bone in the scene
			appendIfUnique skinnedBones realBone
		)
		
		selectByWildCard "Gun_"
		allBones = getCurrentSelection()
		
		unusedBones = differenceInArray allBones skinnedBones 
		
		try (delete unusedBones)catch()
	)
	else
	(
		messagebox  "You need to select a skinned weapon"
	)
)--end deleteUnusedBones

(
	global weaponRigger
	try (cui.UnRegisterDialogBar weaponRigger)catch()
	try(destroyDialog weaponRigger)catch()
	local LastSubRollout = 1

	-- ****************************************************************************************
	rollout markerTools "Weapon Setup Tools" width:190 height:385
	(
		group "Sync Files"
		(
		button btnSyncFiles "Sync Files from Perforce" width:170 height:30 tooltip:"Sync required files from perforce"
		)
		
		radiobuttons radWeaponType "Weapon Grip Type:" labels:#("None","Pistol", "Rifle") default:1
		button btnMergeMarkers "Merge Weapon Markers" width:180 height:30 offset:[0,5] tooltip:"Merge weapon markers"

		button btnBuildRig "Build Rig from Markers" width:180 height:30 tooltip:"Build Rig from Markers - select the weapon mesh first"
		button btnbuildSelectedBone "Build Selected Bone" width:180 height:30 tooltip:"Build Selected Bone - select any marker"
		
		group "Bone and Marker Toggless"
		(
		checkbutton btnBoxModeToggle "Box Mode" width:80 height:30 across:2 offset:[0,5] checked:off tooltip:"Toggle Bone Box Mode"
		checkbutton btnHideShowMarkers "Hide Markers" width:80 height:30 offset:[0,5] checked:off tooltip:"Show\Hide Merged Markers"
		checkbutton btnBoneEditToggle "Bone Edit" width:80 height:30 checked:off tooltip:"Toggle Bone Edit Mode"
		)
	-- ****************************************************************************************
		on markerTools open do
		(
			weaponRigger.height = 415
		)
		
		on btnSyncFiles pressed do
		(
			WWP4vSync weaponRiggingFiles
		)

		on btnMergeMarkers pressed do
		(
			case (markertools.radWeaponType.state) of
			(
				1:
					print "No Weapon Grip Mesh Merged"
				2:
					mergemaxfile weapon_pistol_grip_mesh #select
				3:
					mergemaxfile weapon_rifle_grip_mesh #select
			)
			mergemaxfile weapon_markers #select
		)
		
		on btnBuildRig pressed do buildWeaponRigFM()
		
		on btnBuildSelectedBone pressed do
		(
			selectedMesh = getCurrentSelection()
			
			if (selectedMesh.count == 1) and (classof selectedMesh[1] ==Point) then
			(
				buildWeaponBoneFM selectedMesh[1]
			)
			else
			(
				messagebox "You need to select a SINGLE weapon bone marker"
			)
		)
		
		on btnHideShowMarkers changed state do hideShowWMarkers markerTools.btnHideShowMarkers.state
		on btnBoxModeToggle changed state do boxModeToggle markerTools.btnBoxModeToggle.state
		on btnBoneEditToggle changed state do boneEditToggle markerTools.btnBoneEditToggle.state
	)--end markerTools

	-- ****************************************************************************************
	rollout attachmentTools "Attachment Tools" width:190 height:315
	(
		group "Sync Files"
		(
		button btnSyncFiles "Sync Files from Perforce" width:170 height:30 tooltip:"Sync required files from perforce"
		)
		
		button btnMergeAttMarkers "Merge Attachment Markers" width:180 height:30 tooltip:"Merge Attachment Markers"
		button btnBuildAttachmentDummy "Build Attachment Dummy" width:180 height:30 tooltip:"Build Attachment Dummy for selected AAP"
		button btnAlignAttachmentAAP "Align Attachment to AAP" width:180 height:30 tooltip:"Align attachment pivot to dummy object."
		button btnBuildAttachmentRig "Build Attachment Rig from Markers" width:180 height:30 tooltip:"Build Attachment Rig from Markers"
		button btnCloneSelectedAttach "Copy Selected AAP > WAP" width:180 height:30  tooltip:"Merge Weapon Markers"

		on attachmentTools open do
		(
			weaponRigger.height = 345
		)

		on btnSyncFiles pressed do
		(
			WWP4vSync weaponRiggingFiles
		)

		on btnMergeAttMarkers pressed do mergemaxfile weapon_att_markers #select
		
		on btnBuildAttachmentDummy pressed do
		(
			selectedMesh = getCurrentSelection()
			if selectedMesh.count == 1 then
			(
				BuildAttachmentDummy selectedMesh[1]
			)
			else
			(
				messageBox "Please select a valid AAP helper"
			)
		)
		
		on btnBuildAttachmentRig pressed do buildAttachmentRigFM()
		on btnAlignAttachmentAAP pressed do AlignAttachmentAAP ()
		
		on btnCloneSelectedAttach pressed do 
		(
			selectedMesh = getCurrentSelection()
			if selectedMesh.count == 1 then
			(
				cloneSelectedAttach selectedMesh[1]
			)
			else
			(
				messageBox "Please select a valid AAP helper"
			)
		)
	)--end attachmentTools

	-- ****************************************************************************************
	rollout skinningTools "Skinning Tools" width:190 height:215
	(
		button btnSkinmainbody "Add Skin" width:180 height:30 tooltip:"Add skin to selected object."
		button btnDeleteExistingRig "Delete Existing Rig" width:180 height:30 tooltip:"Merge Weapon Markers"
		button btnDeleteUnusedBones "Delete Unused Bones" width:180 height:30 tooltip:"Merge Weapon Markers"

		on skinningTools open do
		(
			weaponRigger.height = 215
		)
		
		on btnSkinMainBody pressed do
		(
			aSelectedMesh = getCurrentSelection()
			
			if ((aSelectedMesh.count == 1) and (superclassof aSelectedMesh[1] == GeometryClass)) then
			(		
				addSkinToWeapon aSelectedMesh[1]
			)
			else
			(
				messageBox "Please Select The Mesh To Skin"
			)
		)
		
		on btnDeleteUnusedBones pressed do
		(
			aSelectedMesh = getCurrentSelection()
			
			if aSelectedMesh.count == 1 then
			(
				deleteUnusedBones aSelectedMesh[1]
			)
			else
			(
				messageBox "Please Select 1 Mesh"
			)
		)
		
		on btnDeleteExistingRig pressed do deleteExistingRig()
	)--end skinningTools

-------------------------------------------------------------------------------------------------------------------------
	weaponRigger_Rollouts = #(#("Weapon Setup",#(markerTools)), #("Attachments",#(attachmentTools)), #("Skinning",#(skinningTools)))       
-------------------------------------------------------------------------------------------------------------------------
	rollout weaponRigger "Weapon Rigging Tool"
	(
		dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:weaponRigger.width
		local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Animated_Weapons" filename:(getThisScriptFilename())
		
		dotNetControl dn_tabs "TabControl" height:20 width:210 align:#center offset:[0,-3]
		subRollout theSubRollout width:200 height:485 align:#center
		
		on dn_tabs Selected itm do
		(
			if LastSubRollout != (itm.TabPageIndex+1) do --do not update if the same tab clicked twice
			(
				for subroll in weaponRigger_Rollouts[LastSubRollout][2] do
					removeSubRollout theSubRollout subroll
				for subroll in weaponRigger_Rollouts[LastSubRollout = itm.TabPageIndex+1][2] do     
					addSubRollout theSubRollout subroll
			) 
		)--end tabs clicked
-------------------------------------------------------------------------------------------------------------------------
		on weaponRigger open do
		(
			for aTab in weaponRigger_Rollouts do
				(dn_tabs.TabPages.add aTab[1])
			for subroll in weaponRigger_Rollouts[1][2] do
				addSubRollout theSubRollout subroll
			banner.setup()
			--WWP4vSync weaponRiggingFiles
		)
	)
)
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
createDialog weaponRigger 210 400