--Bone DOF Tagger Script
--
--A tool for editing DOF tags (translation, rotation and scale) on bones.
--
--Stewart Wright, Rockstar North, 01/08/12
--
-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

-- ****************************************************************************************
--a function to check the UDP for our DOF tags
fn checkUDP geoComponent =
--geoComponent=the mesh we're check the UDP of
(
	--reset our UDP values
	transUDP = false
	rotUDP = true
	scaleUDP = false
	
	currentUDP = getUserPropbuffer geoComponent --read the udp
	filteredUDP = filterString currentUDP "\r\n" --filter the udp for line breaks
	
	for UDPVal=1 to filteredUDP.count do --for each line we found in the udp
	(
		case of --if we find a match we'll set the udp state to true
		(
			(upperCase (substring filteredUDP[UDPVal] 1 16) == "EXPORTTRANS=TRUE"): transUDP = true
			(upperCase (substring filteredUDP[UDPVal] 1 15) == "EXPORTROT=FALSE"): rotUDP = false
			(upperCase (substring filteredUDP[UDPVal] 1 16) == "EXPORTSCALE=TRUE"): scaleUDP = true
		)
	)
	passOutTags = #(transUDP, rotUDP, scaleUDP)--pass out the udp states
)--end checkUDP

fn writeUDP geoComponent DOFStates =
--geoComponent=the mesh we're check the UDP of
--DOFStates=an array containing the 3 DOF states wwe're going to write
(
	keepUDP = #()
	currentUDP = getUserPropbuffer	geoComponent
	filteredUDP = filterString currentUDP "\r\n"

	for UDPVal = 1 to filteredUDP.count do
	(
		shortUDP = upperCase (substring filteredUDP[UDPVal] 1 6)

		if (shortUDP != "EXPORT") do append keepUDP filteredUDP[UDPVal]
	)
	
	if DOFStates[1] == true do
	(
		tempUDP = "exportTrans=true"
		append keepUDP tempUDP
	)
	if DOFStates[2] == false do
	(
		tempUDP = "exportRot=false"
		append keepUDP tempUDP
	)
	if DOFStates[3] == true do
	(
		tempUDP = "exportScale=true"
		append keepUDP tempUDP
	)

	tag = ""

	if keepUDP.count == 0 then
	(	
		setUserPropbuffer geoComponent tag
	)
	else
	(
		for k = 1 to keepUDP.count do
		(
			tag = tag + keepUDP[k] + "\r\n"
			setUserPropbuffer geoComponent tag
		)
	)
)--end writeUDP

-- ****************************************************************************************
	if boneDOFTagger != undefined do destroyDialog boneDOFTagger
-- ****************************************************************************************
(
	local iEntryIndex = 0
	local selectedThings = #()

	textCol = dotNetObject "System.Windows.Forms.DataGridViewTextBoxColumn"
	checkBoxCol = dotNetObject "System.Windows.Forms.DataGridViewCheckBoxColumn" 

	-- ****************************************************************************************
	fn initDotnetView dotnetFileView =
	(
		dotnetFileView.AllowUserToAddRows = off
		dotnetFileView.AutoSize = on
		dotnetFileView.AutoSizeColumnsMode = dotnetFileView.AutoSizeColumnsMode.Fill
		dotnetFileView.ColumnHeadersDefaultCellStyle.Alignment = dotnetFileView.ColumnHeadersDefaultCellStyle.Alignment.MiddleCenter
		dotnetFileView.ShowEditingIcon = dotnetFileView.RowHeadersVisible = off

		boneText = textCol.clone()
		boneText.HeaderText = "Bone"
		dotnetFileView.columns.add (boneText)

		transChk =checkBoxCol.clone()
		transChk.HeaderText = "Translation"
		dotnetFileView.columns.add (transChk)

		rotChk =checkBoxCol.clone()
		rotChk.HeaderText = "Rotation"
		dotnetFileView.columns.add (rotChk)

		scaleChk =checkBoxCol.clone()
		scaleChk.HeaderText = "Scale"
		dotnetFileView.columns.add (scaleChk)
	)

	-- ****************************************************************************************
	-- ****************************************************************************************
	rollout boneDOFTagger "Bone DOF Tagger"
	(
		dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:boneDOFTagger.width
		local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Bone_DOF_Tagger" filename:(getThisScriptFilename())

		button btnRefresh "Refresh" width:50 height:20 pos:[7,44] tooltip:"Refresh list with selected meshes."
		button btnCommit "Commit Edits" width:80 height:20 pos:[67,44] tooltip:"Commit edits to listed bones."
		
		dotNetControl  dotnetFileView "DataGridView" width:330 height:90 pos:[7, 75]

		-- ****************************************************************************************
		fn updateGridView selectedBones =
		--selectedBones=an array of objects
		(
			dotnetFileView.RowCount = selectedBones.count --refresh the datagrid with a row per bone

			--some UI stuff
			resizeFactor = 25 --this is the maximum size the UI will change to
			if selectedBones.count < resizeFactor do (resizeFactor = selectedBones.count) --if we don't exceed the resizefactor we'll resize to that value
			dotnetFileView.height = (25 + (resizeFactor * 22)) --resize the datagrid
			boneDOFTagger.height = (105 + (resizeFactor * 22)) --resize the window
			--end UI stuff

			for isBone=1 to selectedBones.count do
			(
				dotnetFileView.Rows.item[isBone - 1].cells.item[0].value = selectedBones[isBone].name--fill the bone column with the bone name
				dotnetFileView.Rows.item[isBone - 1].cells.item[0].readonly = true --set the column to readonly

				checkStates = checkUDP selectedBones[isBone]--run the checkUDP function to find out the dof values.  Returns 3 boolean states. I'll use this to toggle the checkboxes.

				dotnetFileView.currentCell = dotnetFileView.Rows.item[0].cells.item[0]--select a cell so we can enable the edits
				dotnetFileView.beginedit false--enable the editing of the checkboxes
				dotnetFileView.Rows.item[isBone - 1].cells.item[1].value = checkStates[1]--set the translation column
				dotnetFileView.Rows.item[isBone - 1].cells.item[2].value = checkStates[2]--set the rotation column
				dotnetFileView.Rows.item[isBone - 1].cells.item[3].value = checkStates[3]--set the scale column
				dotnetFileView.endedit()--'commit' the checkbox edits
			)
		)--end updateGridView

		-- ****************************************************************************************
		on boneDOFTagger open do
		(
			banner.setup()
			initDotnetView boneDOFTagger.dotnetFileView
			
			if selection != undefined do
			(
				selectedThings = getCurrentSelection()
				updateGridView selectedThings
			)
		)--end boneDOFTagger open

		-- ****************************************************************************************
		on btnRefresh pressed do
		(
			if selection != undefined do
			(
				selectedThings = getCurrentSelection()
				updateGridView selectedThings
			)
		)--end on btnRefresh pressed
		
		on btnCommit pressed do
		(
			if dotnetFileView.RowCount !=0 do
			(
				for sel=1 to selectedThings.count do
				(
					readStates = #(false, false, false)
					readStates[1] = dotnetFileView.Rows.item[sel - 1].cells.item[1].EditedFormattedValue--read translation checkbox
					readStates[2] = dotnetFileView.Rows.item[sel - 1].cells.item[2].EditedFormattedValue--read rotation checkbox
					readStates[3] = dotnetFileView.Rows.item[sel - 1].cells.item[3].EditedFormattedValue--read scale checkbox
					
					writeUDP selectedThings[sel] readStates--write the dofs to the udp
				)
			)
		)
		on boneDOFTagger close do ()
	)--end boneDOFTagger rollout
	createDialog boneDOFTagger 345 170
)