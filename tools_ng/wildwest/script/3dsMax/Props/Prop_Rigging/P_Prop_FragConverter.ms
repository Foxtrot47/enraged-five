filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_Rigging.ms")

rollout RsBoneFragConvertRoll "Frag Converter"
(
	button btnConvertToSkinned "Convert Frag to Skinned Frag" width:210 height:30
	button btnExplodeSelectedFrag "Explode Selected Frag" width:210 height:30 offset:[-1,0]
--	button btnUnexplodeSelectedFrag "Restore Selected Frag" width:210 height:30
	button btnConvertSkinnedToFrag "Convert Skinned Frag to Frag" width:210 height:30
	
	-------------------------------------------------------------------------------------------------------------------------

	-- Function to detect frags based on name:
	fn isObjFragName obj = 
	(
		matchPattern obj.name pattern:("*_frag_")
	)


	-- Returns frag-root for selected object:
	fn getSelFragRoot = 
	(
		local selObjs = getCurrentSelection()
		
		if (selObjs.count == 0) do 
		(
			messageBox "You don't have any objects selected" title:"Invalid Selection"
			return False
		)

		-- Find selected-object's root:
		local rootobj = WWfindRootObject selObjs[1]
		
		--Is this a frag? (It'll have a _FRAG_ suffix)
		local isfrag = isObjFragName rootobj
		
		-- If it wasn't a frag, was it a proxy?
		if (not isfrag) do 
		(
			if (isValidNode rootobj) do 
			(
				rootobj = RsGetGeomNodeByName (rootobj.name + "_frag_")
			)
		)
		
		-- Only continue if we've found a valid frag mesh...
		if not (isValidNode rootobj) do  
		(
			messagebox "Could not find frag root object" title:"Invalid Selection"
			rootobj = undefined
		)
		
		return rootObj
	)

	
	-------------------------------------------------------------------------------------------------------------------------
	-- Create a single mesh from a group of meshes and skin them using a supplied rig
	-------------------------------------------------------------------------------------------------------------------------
	--meshesToAdd = a selection of meshes we are going to skin
	--skinBones = the array of bones we will be skinning to
	--unskinnedBones = an array of bones we need in the modifier but don't skin to
	--meshName = what to name the mesh.  if undefined we will inherit the name from the 1st mesh
	fn combineAndSkin meshesToAdd skinBones unskinnedBones meshName:"" =
	(
		local masterMesh = meshesToAdd[1]
		if (masterMesh == undefined) do return undefined
		
		unhide masterMesh
		
		if (meshName != "") do 
		(
			masterMesh.name = meshName
		)
		
		-- Reset radiosity models, to avoid getting the "The Radiosity Solution has been Invalidated" message:
		if (isKindOf SceneRadiosity.Radiosity Radiosity) do 
		(
			SceneRadiosity.Radiosity.Reset True False
		)
		
		-- Combine meshes into the master mesh:
		local meshVertCounts = #()
		
		pushPrompt ("Combining meshes: " + masterMesh.name)
		for thisMesh in meshesToAdd do 
		(
			convertToPoly thisMesh
			append meshVertCounts thisMesh.numVerts

			-- Attach objects to master-mesh:
			if (thisMesh != masterMesh) do
			(
				masterMesh.EditablePoly.attach thisMesh masterMesh
				--meshOp.attach masterMesh thisMesh attachMat:#MatToID
			)
		)
		--convertToPoly masterMesh
		popPrompt()
		--end combine section

		-- Skinning section:
		pushPrompt ("Skinning mesh: " + masterMesh.name)
		-- Now do the skinning. Add a modifier to the fragmesh
		local skinMod = Skin()
		skinMod.bone_limit = 1 -- we are using solid weighting
		skinMod.rigid_vertices = True
		addModifier masterMesh skinMod
		
		if (getCommandPanelTaskMode() != #modify) do (setCommandPanelTaskMode #modify)
		modPanel.setCurrentObject skinMod ui:True

		-- Add the bones
		pushPrompt "Adding bones to Skin"
		for objIndex = 1 to skinBones.count do
		(
			local theDeformer = skinBones[objIndex]
			skinOps.AddBone skinMod theDeformer 1 
			
			local cCount = skinOps.getNumberCrossSections skinMod objIndex
			for i = 1 to cCount do 
			(
				skinOps.SetInnerRadius skinMod objIndex i 0.01
				skinOps.SetOuterRadius skinMod objIndex i 0.01
			)
		)
		popPrompt()
		
		classOf masterMesh -- a hack to refresh the stack

		-- Now skin the mesh verts 100% to the bones. I know the indices that match each bone
		pushPrompt "Assigning verts to bones"
		local vertRunningTotal = 0
		for theBoneIndex = 1 to skinBones.count do 
		(	
			local elementVerts = meshVertCounts[theBoneIndex]

			for theVertLoop = 1 to elementVerts do
			(
				-- The index of the vert is a based on the loop, plus the number of verts currently in the object
				local theVertIndex = (theVertLoop + vertRunningTotal)
				
				skinOps.setVertexWeights skinMod theVertIndex theBoneIndex 1
			)
			
			vertRunningTotal += elementVerts
		) -- end skinning loop
		popPrompt()
		
		-- Add extra bones to modifier:
		pushPrompt "Adding unskinned bones to Skin"
		if (unskinnedBones.count != 0) do
		(
			for objIndex = 1 to unskinnedBones[1].count do
			(
				theDeformer =unskinnedBones[1][objIndex]
				skinOps.AddBone skinMod theDeformer 1 
				
				local cCount = skinOps.getNumberCrossSections skinMod objIndex
				for i = 1 to cCount do 
				(
					skinOps.SetInnerRadius skinMod objIndex i 0.01
					skinOps.SetOuterRadius skinMod objIndex i 0.01
				)
			)
		)
		popPrompt()
		--end skinning section

		masterMesh.wirecolor = color 100 0 100
		setAttribute masterMesh "Is Fragment" True
		
		popPrompt()
		
		return masterMesh--pass out the mesh
	)--end combineAndSkin
	
	
	-------------------------------------------------------------------------------------------------------------------------
	--unskin a mesh and create new meshes based on the skinned skeleton
	-------------------------------------------------------------------------------------------------------------------------
	--skinnedMesh = the mesh we are going to break apart
	fn unskinAndBreak skinnedMesh skinMod =
	(
		pushPrompt ("Unskinning and breaking up mesh: " + skinnedMesh.name)
		
		if (getCommandPanelTaskMode() != #modify) do (setCommandPanelTaskMode #modify)
		modPanel.setCurrentObject skinMod ui:True

		skinnedMesh.name = "SKINNED_" + skinnedMesh.name --we'll rename the mesh we are using so there aren't any conflicts later
		
		bonesInSkin = #()
		vertArray = #()

		for bs=1 to skinops.getNumberBones skinMod do
		(
			realBone = getBoneFromSkin skinMod bs
			if realBone != undefined do (appendIfUnique bonesInSkin realBone)
		)
		
		local vertCount = skinOps.getNumberVertices skinMod

		for thisBone in bonesInSkin do
		(
			local boneVerts = #{}
			
			for vertNum = 1 to vertcount do
			(
				local vertBoneCount = skinOps.getVertexWeightCount skinMod vertNum

				for vertBoneIdx = 1 to vertBoneCount do
				(
					local boneID = skinOps.getVertexWeightBoneId skinMod vertNum vertBoneIdx
					
					if (thisBone.name == skinOps.GetBoneName skinMod boneID 0) do 
					(
						boneVerts[vertNum] = True
					)
				)
			)
			
			if (boneVerts.numberSet !=0) do 
			(
				append vertArray (dataPair bone:thisBone verts:boneVerts)
			)
		)
		
		local createdMeshes = #()
		local skinnedBones = #()
		
		--for each bone in our array we will select the verts skinned to it and detach them as a mesh using the bones name
		local detachedVerts = #{}
		for boneSelect = 1 to vertArray.count do
		(
			local thisFragObj

			if (boneSelect == 1) then 
			(
				-- Don't detach verts from original mesh:
				thisFragObj = skinnedMesh
				convertToPoly thisFragObj
			)
			else 
			(
				-- Detach verts, and name the new mesh:
				local vertNums = vertArray[boneSelect].verts
				polyop.detachVerts skinnedMesh vertNums delete:False asNode:True name:"temp_mesh_name"
				local thisFragObj = getNodeByName "temp_mesh_name"
				
				detachedVerts += vertNums
			)
			
			local meshBone = vertArray[boneSelect].bone
			local boneName = vertArray[boneSelect].bone.name--the new mesh name base
			
			thisFragObj.name = boneName
			thisFragObj.pivot = meshBone.pivot
			thisFragObj.wirecolor = (RsGetRandomColour())

			append createdMeshes thisFragObj
			append skinnedBones meshBone
		)
		
		-- Delete detached verts from root-mesh:
		polyop.deleteVerts skinnedMesh detachedVerts

		local parentBones= for obj in skinnedBones collect (dataPair obj:obj objParent:obj.parent)
		
		-- Parent meshes to new frag-root:
		for meshNum = 1 to parentBones.count do
		(
			local theBoneParent = parentBones[meshNum].objParent
			
			if (theBoneParent != undefined) do
			(
				local theMesh = createdMeshes[meshNum]
				local boneParentNum = findItem skinnedBones theBoneParent

				if (boneParentNum != 0) do 
				(
					theMesh.parent = createdMeshes[boneParentNum]
				)
			)
		)
		
		local isFirstObj = True
		for thisObj in createdMeshes where (getAttrClass thisObj == "Gta Object") do
		(
			if isFirstObj then 
			(
				-- Copy attributes from root-object:
				CopyAttrs thisObj
			)
			else 
			(
				-- Paste attributes onto children:
				PasteAttrs thisObj
			)
			
			-- Set first object as frag-root, but not its children:
			setAttribute thisObj "Is Fragment" isFirstObj
			
			isFirstObj = False
		)
		
		popPrompt()
		
		return createdMeshes--pass out the new meshes
	)--end unskinAndBreak
	
	
	-------------------------------------------------------------------------------------------------------------------------
	-- Function to create a skeleton based on an existing hierarchy.  we 'mimic' the hierarchy of the passed in mesh
	--	meshArray = a bunch of stuff we'll pass in and create a skel for
	--	boneSize = the size of the PointHelper.  boneColour = the colour of the PointHelper (passed in as an array #(r,g,b).  used in the buildPointHelper function
	-------------------------------------------------------------------------------------------------------------------------
	fn createSkeletonFromMesh meshArray boneSize boneColour =
	(
	--find out the parent of each mesh and store it
		meshesOnly =#() -- the meshes
		
		-- Split the selection by meshes and non meshes, and store this plus the parent object
		for obj in meshArray do
		(	
			if superClassOf obj == GeometryClass do append meshesOnly #(obj, obj.parent)
		)
	--
	-- Create a root structure
		boneList = #()
		newBoneList = #()
		
		-- create a bone for each mesh
		for objIndex = 1 to meshesOnly.count do 
		(	
			theMesh = meshesOnly[objIndex][1]
			theParent = meshesOnly[objIndex][2]
			theMeshName = theMesh.name

			buildPointHelper "temp_bone_name" theMesh.pos boneSize (color bonecolour[1] bonecolour[2] bonecolour[3])
			theBone = getnodebyname "temp_bone_name"--we give the bone a unique name so we can select it safely
			theBone.name = theMeshName--now we have a node and can rename the bone
			setUserPropbuffer thebone "exportTrans = true"
			append boneList #(theBone, theParent)
			append newBoneList theBone
		)
		
		--link the bones based on the original mesh hierarchy
		for deformerIndex = 1 to boneList.count do 
		(
			if boneList[deformerIndex][2] != undefined do (boneList[deformerIndex][1].parent = boneList[deformerIndex][2])
		)
	--
		newBoneList--return the list of bones we made
	)--end createSkeletonFromMesh


	-- Reparents children of objects in "fromArray" to objects with matching names in "toArray"
	fn transferChildren fromArray toArray =
	(
		local hasChildren = for obj in fromArray where (obj.children.count != 0) collect (dataPair name:(toLower obj.name) children:(join #() obj.children))

		--create an array of object names so we can check them against our name prefix
		local toArrayNames = for obj in toArray collect (toLower obj.name)

		--for every object with children
		for fromItem in hasChildren do
		(
			local toObjNum = findItem toArrayNames (toLower fromItem.name)

			--for all the children
			for childObj in fromItem.children do 
			(
				if ((findItem fromArray childObj) == 0) do -- a check to make sure we don't carry over our from parent meshes (ie skeleton)
				(
					childObj.parent = toArray[toObjNum] -- link the child to new parent
				)
			)
		)
	)


	-- This function will take a standard frag and convert it to a skinned frag
	fn ConvertSelectionToSkinnedFrag = 
	(
		local rootObj = getSelFragRoot()
		if (rootObj == undefined) do return False

		-- Abort if is is already a Skinned Frag:
		if (RsGetSkinModifier rootObj != undefined) do  
		(
			messagebox "This object already has a Skin modifier!" title:"Convert Failed"
			return False
		)
		
		-- Need to grab all the bones and the particles too - basically any helpers attached to the meshes
		local objectList = RsGetFlatHierarchy rootObj

		local meshList = for obj in objectList 
			where ((superClassOf obj == GeometryClass) and ((UDPsearch obj "isdamage = true") == 0)) 
			collect obj

		local fragMeshbones = createSkeletonFromMesh meshList 0.5 #(255,255,0)

		for obj in fragMeshbones do
		(
			setUserPropbuffer obj "exportTrans = true"
		)

		transferChildren meshList fragMeshbones -- Transfer the helpers (and anything else that is linked) over to new bones

-- 		local rootSetup = buildRigRootSystem rootobj--create some root objects
		local rootSetup = RSTA_buildPropRoot rootObj false false
		
		
		fragMeshbones[1].parent = rootSetup[1][2]--link skeleton to root objects
		
		local combinedMesh = combineAndSkin meshList fragMeshbones rootSetup

		-- Set the Attributes:
		::RsBoneCreateRoll.addUsefulAttributes combinedMesh
		
		select rootObj
		
		return rootObj
	)


	-- This function will take a skinned frag and convert it to a standard multi-object frag
	fn ConvertSkinnedSelectionToFrag =
	(
		local rootObj = getSelFragRoot()
		if (rootObj == undefined) do return False

		local skinMod = RsGetSkinModifier rootObj
		if (skinMod == undefined) do  
		(
			messagebox "Object doesn't use Skin modifier!" title:"Convert Failed"
			return False
		)

		if (getCommandPanelTaskMode() != #modify) do (setCommandPanelTaskMode #modify)
		modPanel.setCurrentObject skinMod ui:True
		
		--work out the frag skeleton
		local aRealBone = getBoneFromSkin skinMod 1		--get first bone from the skin modifier
		local rootSkelObj = WWfindRootObject aRealBone	--work out the root bone
		
		local tempSkelList=#()
		for bs=1 to (skinops.getNumberBones skinMod) do
		(
			local realBone = getBoneFromSkin skinMod bs
			if (realBone != undefined) do (appendIfUnique tempSkelList realBone)
		)

		sliderTime = 0f--set the timeline to frame 0
		
		local brokenMeshes = unskinAndBreak rootobj skinMod --break the mesh in to frag parts
		
		transferChildren tempSkelList brokenMeshes --transfer the hierarchy

		--Delete the old hierarchy parts:
		delete (RsGetFlatHierarchy rootSkelObj)
		
		local retVal = brokenMeshes
		select retVal[1]
		
		return retVal
	)


	-- Explodes Skinned Frag's bone-objects out from midpoint, in frame 10:
	fn ExplodeSelectedFrag =
	(
		local rootObj = getSelFragRoot()
		if (rootObj == undefined) do return False
		
		local skinMod = RsGetSkinModifier rootObj
		if (skinMod == undefined) do 
		(
			messagebox "Object doesn't use Skin modifier!" title:"Explode Failed"
			return False
		)
		
		-- Grab some bone data from the skin modifier:
		sliderTime = 0f
		if (getCommandPanelTaskMode() != #modify) do (setCommandPanelTaskMode #modify)
		modPanel.setCurrentObject skinMod ui:True
		totalSkinModBones = skinOps.getNumberBones skinMod
		
		-- Collect the bones in the skin modifier
		local boneList = for i = 1 to totalSkinModBones collect 
		(	
			local boneName = (skinOps.getBoneName skinMod i 1)
			
			-- Use helper, if multiple nodes were found with this name:
			local namedBones = getNodeByName boneName all:True
			if (namedBones.count > 1) do 
			(
				namedBones = for obj in namedBones where (isKindOf obj Helper) collect obj
			)
			
			local thisBone = namedBones[1]
			
			if ((thisBone == undefined) or (thisBone == rootObj)) then dontCollect else (dataPair obj:thisBone pos:thisBone.pos)
		)
		
		-- set a keyframe on all these bones at frame 0
		sliderTime = 0f
		for item in boneList do 
		(
			with animate on
			(
				addNewKey item.obj[3] 0
			)
		)	

		-- Now move the bones relative to the root.
		local rootPos = rootObj.pos
		
		-- set a keyframe on all these bones at frame 10
		sliderTime = 10f
		for item in boneList where (thisBone != rootObj) do 
		(
			-- calculate how far this bone should move
			-- make it the distance between the position and the central point x2
			local thisBone = item.obj
			local bonePos = item.pos
			local offset = (bonePos - rootPos) * 2
			
			with animate on
			(
				thisBone.pos = (bonePos + offset)
				addNewKey thisBone[3] 10
			)
		)	

		return OK
	)


	fn UnexplodeSelectedFrag =
	(
		sliderTime = 0f
	)
	-------------------------------------------------------------------------------------------------------------------------
	
	on btnConvertToSkinned pressed do 
	(
		undo "Convert Fragments To Skinned" on 
		(
			ConvertSelectionToSkinnedFrag()
		)
	)
	
	on btnExplodeSelectedFrag pressed do 
	(
		undo "Explode Frag" on 
		(
			ExplodeSelectedFrag()
			completeRedraw()
		)
	)

	on btnConvertSkinnedToFrag pressed do 
	(
		undo "Convert Skinned To Fragments" on
		(
			ConvertSkinnedSelectionToFrag()
		)
	)

	-- Save rolled-up state:
	on RsBoneFragConvertRoll rolledUp down do 
	(
		RsSettingWrite "RsBoneFragConvertRoll" "rollup" (not down)
	)
)
