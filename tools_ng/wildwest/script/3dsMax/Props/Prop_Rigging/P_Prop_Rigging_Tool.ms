filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

theProjectConfig = RsConfigGetProjBinConfigDir()

filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/mrSkeleton_2_ui.ms")

-- 		-- Uses the bone name as the bopnename
-- 		-- Set up translation on all selected bones is required
-- 		-- Rockstar North
-- 		-- Rick Stirling February 2010
-- 		-- Stewart Wright 15/06/10 Edit.  Added checkbox for bonetags and checks on existing UDP tags.
-- 		-- Stewart Wright 08/09/10 Edit.  Added checkboxes for useful attributes

-- 		-- Update --Oct 2012 Allan Hayburn
-- 		-- Added transfer skin props rollout. Allows the transfer of skinned prop and info to another object.

-- 		global RsPropRiggingTools
-- 		try (closeRolloutFloater RsPropRiggingTools) catch()

-- 		-- This line loads the custom header
-- 		filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

-- 		global isDynamicState = true
-- 		global keepBoneRotState = true

-- 		global idxIsDynamic = GetAttrIndex "Gta Object" "Is Dynamic"
-- 		global idxKeepBoneRotations = GetAttrIndex "Gta Object" "Keep Bone Rotations"
-- 		global idxAuthoredOrients = GetAttrIndex "Gta Object" "Authored Orients"

-- 		-------------------------------------------------------------------------------------------------------------------------

-- 		-- Script for "RsBoneFragConvertRoll"
-- 		filein (RsConfigGetWildWestDir() + "script/3dsMax/Props/Prop_Rigging/P_Prop_FragConverter.ms")

-- 		-------------------------------------------------------------------------------------------------------------------------

-- 		rollout RsPropRigToolBannerRoll ""
-- 		(
-- 			dotNetControl RsBannerPanel "Panel" pos:[6,3] height:32 width:(RsPropRigToolBannerRoll.width - 6)
-- 			local bannerStruct = makeRsBanner dn_Panel:RsBannerPanel wiki:"Prop_Bone_Tagger" filename:(getThisScriptFilename())
-- 			
-- 			-- Refresh banner-icons when floater's rollouts are adjusted:
-- 			on RsBannerPanel Paint Sender Args do 
-- 			(
-- 				for n = 0 to (RsBannerPanel.Controls.Count - 1) do 
-- 				(
-- 					RsBannerPanel.Controls.Item[n].Invalidate()
-- 				)
-- 			)
-- 			
-- 			--------------------------------------------------------------
-- 			-- Rollout open/close
-- 			--------------------------------------------------------------
-- 			on RsPropRigToolBannerRoll open do
-- 			(
-- 				-- Initialise tech-art banner:
-- 				bannerStruct.setup()
-- 			)
-- 		)

-- 		-------------------------------------------------------------------------------------------------------------------------
-- 		rollout RsBoneCreateRoll "Bone Creation"
-- 		(
-- 			button btnBuildRigRootSystem "Build rig root system" width:210 height:30
-- 			button btnBonesOnVerts "Create deformers from selected verts" width:210 height:30
-- 			button btnTagBones "Enable Translation on selected bones" width:210 height:30
-- 			button btnautoRig "Autorig Selected mesh and bones" width:210 height:30
-- 			button btnSkinBrush "Skin Brush Tool" width:210 height:30
-- 			button btnOpenSchematicView "Open Schematic View" width:210 height:30
-- 			
-- 			checkbox chkIsDynamic "Is Dynamic" checked:isDynamicState across:2 align:#right offset:[-29,0]
-- 			checkbox chkKeepBoneRot "Keep Bone Rotations" checked:keepBoneRotState align:#left offset:[-20,0]
-- 			
-- 			button btnAddUsefulAttributes "Add Selected Attributes" width:210 height:30

-- 			-------------------------------------------------------------------------------------------------------------------------
-- 			
-- 			fn addUsefulAttributes obj = 
-- 			(
-- 				if ((superClassOf obj) == GeometryClass) do 
-- 				(	
-- 					-- Set the Attributes:
-- 					SetAttr obj idxIsDynamic isDynamicState
-- 					SetAttr obj idxKeepBoneRotations keepBoneRotState
-- 					SetAttr obj idxAuthoredOrients keepBoneRotState
-- 				)
-- 			)


-- 			fn addTranslationTag objs:(selection as array) = 
-- 			(
-- 				for theBone in objs do 
-- 				(	
-- 					setUserPropbuffer thebone "exportTrans = true"
-- 				)
-- 			)


-- 			-- Take a vert selection and create a point helper at each vert
-- 			-- Automatically add translation to these deformer nodes
-- 			fn createDeformerAtVert = 
-- 			(
-- 				local selObjs = for obj in (getCurrentSelection()) where (isProperty obj #mesh) collect obj
-- 				
-- 				if (selObjs.count != 1) do 
-- 				(
-- 					messagebox "Please select a valid mesh for rigging" title:"Invalid Selection"
-- 					return #()
-- 				)

-- 				local selObj = selObjs[1]
-- 				local objectname = selObj.name	
-- 				local outList = #()
-- 				
-- 				vertList = case (classof selObj) of 
-- 				(
-- 					Editable_Poly:(( selObj.GetSelection #Vertex ) as array)
-- 					Editable_mesh:(( getVertSelection selObj ) as array)
-- 					Default:#()
-- 				)
-- 				
-- 				if (vertList.count < 1) do 
-- 				(
-- 					MessageBox "No verts selected"
-- 					return false
-- 				)
-- 				
-- 				local outObjs
-- 				
-- 				-- For each vert, create a point helper
-- 				for v = 1 to vertList.count do 
-- 				(
-- 					local thevert = vertList[v]
-- 					local vertPos = selObj.vertices[thevert].pos
-- 					
-- 					local deformername = objectname + "_Bone_" + (v as string)
-- 					local deformerObj = buildPointHelper deformername vertPos 0.25 (color 255 255 0)
-- 					addTranslationTag objs:#(deformerObj)
-- 					
-- 					-- try to parent it
-- 					local pbone = RsGetHelperNodeByName ("Main_" + objectName)
-- 					if (isValidNode pbone) do 
-- 					(
-- 						deformerObj.parent = pbone
-- 					)
-- 					
-- 					append outList deformerObj
-- 				)

-- 				return outList
-- 			)
-- 			
-- 			
-- 			fn AlignPropRoot = 
-- 			(
-- 				-- Needs two objects
-- 				sel = getcurrentselection()
-- 				if sel.count != 2 then (
-- 					messagebox "Need to select 2 objects: A mesh and alignment helper"
-- 				)
-- 				else (
-- 					themesh = undefined
-- 					theaap = undefined
-- 					
-- 					for i = 1 to 2 do (
-- 						obj = sel[i]
-- 						if superclassof obj == GeometryClass then themesh = obj
-- 						if superclassof obj == helper then theaap = obj
-- 					)	
-- 					
-- 					AlignPivotTo themesh theaap
-- 					
-- 					-- reset transforms
-- 					ResetXForm themesh
-- 					maxOps.CollapseNode themesh off
-- 					
-- 					clearSelection()
-- 					
-- 					select themesh
-- 				)
-- 			)
-- 			
-- 			
-- 			fn collectRiggingObjects = 
-- 			(
-- 				local objectList = getCurrentSelection()
-- 				local riggingMesh = #()
-- 				local bonelist =#()
-- 				local geocount = 0
-- 				
-- 				local riggingList = #() -- this will be returned
-- 				
-- 				-- collect all the data
-- 				for obj in objectList do
-- 				(
-- 					--Find the bones, all point helpers in this function
-- 					if (isKindOf obj Point) do 
-- 					(
-- 						append bonelist obj
-- 					)	
-- 					
-- 					-- Find the mesh
-- 					if superclassof obj == GeometryClass then 
-- 					(	
-- 						-- Discard the mover mesh - if n
-- 						if (substring obj.name 1 6) != "Mover_" then 
-- 						(
-- 							geocount += 1
-- 							
-- 							-- Is it a LOD?
-- 							local flod = findString obj.name "LOD"
-- 							
-- 							if (flod == undefined) or (queryBox "A selected mesh seems to be a LOD - add it?" beep:false) do 
-- 							(
-- 								append riggingMesh obj
-- 							)
-- 						)
-- 					)
-- 				)		

-- 				append riggingList riggingMesh[1] -- populate array with first mesh found.
-- 				append riggingList bonelist -- add the bones
-- 				
-- 				-- Check that the meshes are valid
-- 				if (riggingMesh.count < 1) then 
-- 				(
-- 					messageBox "No meshes found in selection." title:"Invalid Selection"
-- 					riggingList = undefined
-- 				)
-- 				
-- 				if (riggingMesh.count > 1) then 
-- 				(
-- 					messageBox "More than 1 geometry in selection, please include only one." title:"Invalid Selection"
-- 					riggingList = undefined
-- 				)
-- 				
-- 				-- check that bone lists is valid too!
-- 				local bonenames = #()
-- 				local uniquebonenames = #()
-- 				for thebone in bonelist do append bonenames thebone.name
-- 				for thebone in bonenames do appendIfUnique uniquebonenames thebone
-- 					
-- 				if (bonenames.count != uniquebonenames.count) then
-- 				(
-- 					messageBox "Duplicate bone names found - please check the names of the bones (I've selected the bones for you.)"
-- 					select bonelist
-- 					riggingList = undefined
-- 				)
-- 				
-- 				return riggingList
-- 			)


-- 			fn autorigmesh = 
-- 			(
-- 				boneInfSize = 0.2 -- an initial size for the skin bone radius in metres
-- 				
-- 				-- Process the selection and get the mesh and bones.
-- 				riggingdata = collectRiggingObjects()
-- 				
-- 				if riggingdata == undefined then (print "Stopped the rig building")
-- 					
-- 				else
-- 				(
-- 					meshtoskin =riggingdata[1]
-- 					bonelist = riggingdata[2]
-- 					
-- 					select meshtoskin
-- 					
-- 					local skinMod = Skin()
-- 					skinMod.bone_limit = 4
-- 					addModifier meshtoskin skinMod
-- 					
-- 					-- Make sure we're on the modify panel because we have to be to use the skinOps struct to add bones and edit them
-- 					if (getCommandPanelTaskMode() != #modify) do (setCommandPanelTaskMode #modify)
-- 					modPanel.setCurrentObject skinMod ui:True
-- 					
-- 					-- Loop through all the bones adding and setting them up
-- 					for boneIndex = 1 to bonelist.count do 
-- 					(
-- 						-- If i is less than the number of bones we can add a bone without updating the mesh
-- 						-- When the last bone is added, we also update the mesh
-- 						if boneIndex < bonelist.count then
-- 						(
-- 							skinOps.AddBone skinMod bonelist[boneIndex] 0
-- 						)
-- 						
-- 						else
-- 						(
-- 							skinOps.AddBone skinMod bonelist[boneIndex] 1
-- 						)
-- 						
-- 						-- set the radius of these bones in the skin mod
-- 						cCount = skinOps.getNumberCrossSections skinMod boneIndex
-- 						for i = 1 to cCount do 
-- 						(
-- 							skinOps.SetInnerRadius skinMod boneIndex i boneInfSize
-- 							skinOps.SetOuterRadius skinMod boneIndex i boneInfSize
-- 						)
-- 					)
-- 					

-- 				)
-- 			)
-- 			
-- 			-------------------------------------------------------------------------------------------------------------------------
-- 			
-- 			on chkIsDynamic changed theState do
-- 			(
-- 				isDynamicState = theState
-- 			)

-- 			on chkKeepBoneRot changed theState do
-- 			(
-- 				keepBoneRotState = theState
-- 			)

-- 				
-- 			on btnBonesOnVerts pressed do createDeformerAtVert()
-- 				
-- 			on btnBuildRigRootSystem pressed do 
-- 			(	
-- 				if selection[1] == undefined then
-- 				(
-- 					messagebox "Please select a valid mesh for rigging"
-- 				)
-- 				else
-- 				(
-- 					theRigobject  = selection[1]
-- 					format ("This tool uses a deprecated function. Please speak to Matt Rennie in tech art")
-- 					messagebox ("This tool uses a deprecated function. Please speak to Matt Rennie in tech art") beep:true title:"THIS TOOL NO LONGER WORKS!"
-- 					--BuildRigRootSystem theRigobject
-- 				)
-- 			)
-- 			
-- 			on btnAlignPropRoot pressed do AlignPropRoot()

-- 			on btnTagBones pressed do addTranslationTag()

-- 			on btnautoRig pressed do autorigmesh()

-- 			on btnAddUsefulAttributes pressed do 
-- 			(
-- 				addUsefulAttributes selection[1]
-- 			)
-- 				
-- 			on btnSkinBrush pressed do
-- 			(
-- 				filein ped_skinbrush_script 
-- 			)
-- 				
-- 			on btnOpenSchematicView pressed do 
-- 			(
-- 				schematicView.Open "Schematic View 1"
-- 				actionMan.executeAction 5 "950"  -- Schematic View: Pan to Selected
-- 			)

-- 			-- Save rolled-up state:
-- 			on RsBoneCreateRoll rolledUp down do 
-- 			(
-- 				RsSettingWrite "RsBoneCreateRoll" "rollup" (not down)
-- 			)
-- 		)


-- 		-------------------------------------------------------------------------------------------------------------------------
-- 		rollout RsBoneConstraintsRoll "Bone Constraints"
-- 		(
-- 			local defaultRolledUp -- Flags rollout for being rolled-up by default

-- 			button btnShowBoneConstraints "Show Selected Bone Contraints" width:210 height:30 offset:[1,0]
-- 			button btnHideBoneConstraints "Hide Selected Bone Contraints" width:210 height:30

-- 			
-- 			spinner spnTxmin "Translation Constraint X Minimum:" range:[-1,0,-0.25] type:#float fieldwidth:40 offset:[-18,0]
-- 			spinner spnTxmax "Translation Constraint X Maximum:" range:[0,1,0.25] type:#float fieldwidth:40 offset:[-18,0]
-- 			spinner spnTymin "Translation Constraint Y Minimum:" range:[-1,0,-0.25] type:#float fieldwidth:40 offset:[-18,0]
-- 			spinner spnTymax "Translation Constraint Y Maximum:" range:[0,1,0.25] type:#float fieldwidth:40 offset:[-18,0]
-- 			spinner spnTzmin "Translation Constraint Z Minimum:" range:[-1,0,-0.25] type:#float fieldwidth:40 offset:[-18,0]
-- 			spinner spnTzmax "Translation Constraint Z Maximum:" range:[0,1,0.25] type:#float fieldwidth:40 offset:[-18,0]
-- 			
-- 			button btncreateTConstraints "Add Translation constraints to selected" width:210 height:30
-- 			
-- 			spinner spnRxmin "Rotation Constraint X Minimum:" range:[-180,0,-90] type:#float fieldwidth:40 offset:[-18,0]
-- 			spinner spnRxmax "Rotation Constraint X Maximum:" range:[0,180,90] type:#float fieldwidth:40 offset:[-18,0]
-- 			spinner spnRymin "Rotation Constraint Y Minimum:" range:[-180,0,-90] type:#float fieldwidth:40 offset:[-18,0]
-- 			spinner spnRymax "Rotation Constraint Y Maximum:" range:[0,180,90] type:#float fieldwidth:40 offset:[-18,0]
-- 			spinner spnRzmin "Rotation Constraint Z Minimum:" range:[-180,0,-90] type:#float fieldwidth:40 offset:[-18,0]
-- 			spinner spnRzmax "Rotation Constraint Z Maximum:" range:[0,180,90] type:#float fieldwidth:40 offset:[-18,0]
-- 			
-- 			button btncreateRConstraints "Add Rotation constraints to selected" width:210 height:30	
-- 			
-- 			-------------------------------------------------------------------------------------------------------------------------
-- 			
-- 			fn addConstraints Xmin Xmax Ymin Ymax Zmin Zmax constraintType= 
-- 			(
-- 				local deformers = for obj in (getCurrentSelection()) where (isKindOf obj Helper) collect obj
-- 				
-- 				if (deformers.count == 0) do 
-- 				(
-- 					messagebox "Please select some deformers for rigging" title:"Invalid Selection"
-- 					return False
-- 				)

-- 				for theDeformer in deformers do
-- 				(	
-- 					local theDeformer = deformers[d]
-- 					
-- 					if (isKindOf theDeformer) Helper do 
-- 					(
-- 						local createFunc = case constraintType of 
-- 						(
-- 							"R":(createRSRotationConstraint)
-- 							"T":(createRSTranslationConstraint)
-- 							default:undefined
-- 						)
-- 						
-- 						if (createFunc != undefined) do 
-- 						(
-- 							createFunc theDeformer "X" Xmin Xmax ""
-- 							createFunc theDeformer "Y" Ymin Ymax ""
-- 							createFunc theDeformer "Z" Zmin Zmax ""
-- 						)
-- 					)	
-- 				)
-- 			)

-- 				
-- 			fn showHideConstraints showHideBool =
-- 			(
-- 				objectlist = getCurrentSelection()
-- 				boneList =#()
-- 				
-- 				
-- 				for obj in objectlist do
-- 				(
-- 					if classof obj == Point then
-- 					(
-- 						append bonelist obj
-- 					)	
-- 				)		
-- 				
-- 				
-- 				for obj in boneList do
-- 				(
-- 					kidlist = obj.children 
-- 					for kid in kidlist do
-- 					(
-- 						if ((classof kid == RsRotationConstraint) or (classof kid == RsTranslationConstraint)) then
-- 						(
-- 							if showHideBool  == false then hide kid
-- 							if showHideBool  == true then unhide kid
-- 						)	
-- 					)	
-- 					
-- 					redrawViews()
-- 				)
-- 			)
-- 			
-- 			-------------------------------------------------------------------------------------------------------------------------
-- 			on btnShowBoneConstraints pressed do showHideConstraints true
-- 			on btnHideBoneConstraints pressed do showHideConstraints false

-- 			on btncreateTConstraints pressed do addConstraints spnTxmin.value spnTxmax.value spnTymin.value spnTymax.value spnTzmin.value spnTzmax.value "T"
-- 			on btncreateRConstraints pressed do addConstraints spnRxmin.value spnRxmax.value spnRymin.value spnRymax.value spnRzmin.value spnRzmax.value "R"

-- 			-- Save rolled-up state:
-- 			on RsBoneConstraintsRoll rolledUp down do 
-- 			(
-- 				RsSettingWrite "RsBoneConstraintsRoll" "rollup" (not down)
-- 			)
-- 		)


-- 		-------------------------------------------------------------------------------------------------------------------------
-- 		rollout RsBoneXmlRoll "XML Generation"
-- 		(
-- 			local defaultRolledUp -- Flags rollout for being rolled-up by default
-- 			
-- 			spinner spnObjectMass "Object Mass:" range:[0,1000,10] type:#float fieldwidth:40 offset:[-18,0]
-- 			spinner spnBoneStrength "Average Bone Strength:" range:[0,1,0.1] type:#float fieldwidth:40 offset:[-18,0]
-- 			spinner spnBoneRadius "Average Bone Radius:" range:[0,3,0.5] type:#float fieldwidth:40 offset:[-18,0]
-- 			spinner spnDamageThreshold "Damage Threshold:" range:[0,10,1.5] type:#float fieldwidth:40 offset:[-18,0]
-- 			
-- 			button btngenerateXML "Generate XML for selected object" width:210 height:30 offset:[1,0]
-- 			
-- 			-------------------------------------------------------------------------------------------------------------------------
-- 			
-- 			fn generatexml ObjectMass BoneStrength BoneRadius DamThresh=
-- 			(
-- 				local xmldata = ::RsBoneCreateRoll.collectRiggingObjects()
-- 				if (xmldata == undefined) do (return False)
-- 				
-- 				objectname =xmldata[1].name

-- 				-- Header
-- 				local xmlstring = stringStream ""
-- 				format "<Item>\n" to:xmlstring
-- 				format "\t<objectName>%</objectName>\n" objectname to:xmlstring
-- 				format "\t<objectStrength value=\"%\" />\n" (ObjectMass as string) to:xmlstring
-- 				format "\t\t<DeformableBones>\n" to:xmlstring

-- 				-- Bone section
-- 				for boneindex = 1 to xmldata[2].count do
-- 				(
-- 					thebonename = xmldata[2][boneindex].name
-- 					excludename = ((filterstring thebonename "_")[1])
-- 					
-- 					if ((excludename != "Main") and (excludename != "Root")) then
-- 					(
-- 						-- This is a valid bone for tagging
-- 						format "\t\t\t<Item>\n" to:xmlstring
-- 						format "\t\t\t\t<boneName>%</boneName>\n" thebonename to:xmlstring
-- 						
-- 						format "\t\t\t\t<strength value=\"%\" />\n" (BoneStrength as string) to:xmlstring
-- 						format "\t\t\t\t<radius value=\"%\" />\n" (BoneRadius as string) to:xmlstring
-- 						format "\t\t\t\t<damageVelThreshold value=\"%\" />\n" (DamThresh as string) to:xmlstring
-- 						
-- 						format "\t\t\t</Item>\n" to:xmlstring
-- 					)
-- 				)	

-- 				-- Tail
-- 				format "\t\t</DeformableBones>\n" to:xmlstring
-- 				format "</Item>\n" to:xmlstring
-- 				
-- 				RScreateTextWindow text:(xmlstring as string) title:"XML string" wordWrap:false width:600 height:600 position:[10,10] readonly:true
-- 				
-- 			)

-- 			-------------------------------------------------------------------------------------------------------------------------
-- 			 
-- 			on btngenerateXML pressed do generatexml spnObjectMass.value  spnBoneStrength.value  spnBoneRadius.value spnDamageThreshold .value

-- 			-- Save rolled-up state:
-- 			on RsBoneXmlRoll rolledUp down do 
-- 			(
-- 				RsSettingWrite "RsBoneXmlRoll" "rollup" (not down)
-- 			)
-- 		)


-- 		-------------------------------------------------------------------------------------------------------------------------
-- 		rollout RsBoneDebugRoll "BoneDebug"
-- 		(
-- 			local defaultRolledUp -- Flags rollout for being rolled-up by default
-- 			
-- 			button btngetboneHash "Get Bone hash for selected bone" width:210 height:30
-- 			
-- 			-------------------------------------------------------------------------------------------------------------------------
-- 			
-- 			fn generateHashInfo = 
-- 			(
-- 				local thebonedata = ::RsBoneCreateRoll.collectRiggingObjects()
-- 				if (thebonedata == undefined) do (return False)
-- 				
-- 				local outstring = ""
-- 				
-- 				local bonenamearray= for boneindex = 1 to thebonedata[2].count collect (thebonedata[2][boneindex].name)
-- 				
-- 				sort bonenamearray
-- 				
-- 				for boneindex = 1 to  bonenamearray.count do
-- 				(
-- 					thebonename = bonenamearray[boneindex]
-- 					bonehash = (athash16u thebonename) as string
-- 					outstring = outstring + "Bone: " + thebonename + " hashed name is: " + bonehash + "\n"
-- 				)	
-- 				
-- 				RScreateTextWindow text:outstring title:"Bone debug" wordWrap:false width:300 height:300 position:[10,10] readonly:true
-- 			)
-- 			
-- 			-------------------------------------------------------------------------------------------------------------------------
-- 			on btngetboneHash pressed do generateHashInfo()

-- 			-- Save rolled-up state:
-- 			on RsBoneDebugRoll rolledUp down do 
-- 			(
-- 				RsSettingWrite "RsBoneDebugRoll" "rollup" (not down)
-- 			)
-- 		)


-- 		-------------------------------------------------------------------------------------------------------------------------

-- 		struct RsSkinInfo -- struct to store the target data
-- 		(
-- 			theObject = undefined,
-- 			theObjectCenter = undefined,
-- 			theObjectClass = undefined,
-- 			infBones = #(),	
-- 			
-- 			fn getInfBones =  -- finds the bones used in the objects skin modifier
-- 			(
-- 				if (isValidNode theObject) do 
-- 				(
-- 					local skinMod = RsGetSkinModifier theObject
-- 					if (theSkin != undefined) then
-- 					(
-- 						if (getCommandPanelTaskMode() != #modify) do (setCommandPanelTaskMode #modify)
-- 						modPanel.setCurrentObject skinMod ui:True
-- 						
-- 						numBones = skinOps.GetNumberBones skinMod
-- 						for i in 1 to numBones do
-- 						(
-- 							local boneName = (skinOps.GetBoneName skinMod i 1)
-- 				
-- 							-- Use helper if multiple nodes were found with this name:
-- 							local namedBones = getNodeByName boneName all:True
-- 							if (namedBones.count > 1) do 
-- 							(
-- 								namedBones = for obj in namedBones where (isKindOf obj Helper) collect obj
-- 							)
-- 				
-- 							local thisBone = namedBones[1]

-- 							append infBones thisBone
-- 						)
-- 					)
-- 				)
-- 			),
-- 			
-- 			fn getObjectInfo theObj = -- populates the structs data
-- 			(
-- 				theObject = theObj
-- 				theObjectCenter = theObj.center
-- 				theObjectClass = classof theObj
-- 				if theObjectClass == Editable_mesh then theObjectClass = TriMeshGeometry
-- 				getInfBones()
-- 			)
-- 		)

-- 		-------------------------------------------------------------------------------------------------------------------------
-- 		rollout RsTransferSkinPropRoll "Update Skinned Props"
-- 		(		
-- 			-- Locals & Functions
-- 			local sourceObj, targetObj
-- 			
-- 			local skinInfo = RsSkinInfo()
-- 				
-- 			fn sourceFilter obj = (isKindOf obj.baseObject Editable_Poly) or (isKindOf obj.baseObject Editable_mesh)
-- 			fn targetFilter obj =((isKindOf obj.baseObject Editable_Poly) or (isKindOf obj.baseObject Editable_mesh)) and (obj.modifiers[#skin] != undefined)
-- 			
-- 			-- UI
-- 			label getTargetLbl "Skinned Object:" across:2 align:#right offset:[-48,3]
-- 			pickbutton getTargetObjBtn "[pick object]" filter:targetFilter autoDisplay:true width:170 offset:[-20,0]
-- 			
-- 			label getSourceLbl "New Prop:" across:2 align:#right offset:[-48,3]
-- 			checkbutton getSourceObjBtn "[pick object]" width:170 offset:[-20,0]
-- 			
-- 			local loadErrors = #("Selection not a valid mesh","Selection count must be 1","Select a Skinned object before loading the Prop")
-- 			fn gotLoadError theNum = -- returns the error messages for object loading
-- 			(
-- 				msg = loadErrors[theNum]
-- 				messagebox msg title:"Error"
-- 				getSourceObjBtn.checked = false
-- 				getSourceObjBtn.text = "[pick object]"
-- 				return false
-- 			)
-- 			
-- 			fn loadSource = -- loads the new prop object
-- 			(
-- 				if targetObj != undefined then
-- 				(
-- 					clearSelection() 
-- 					thecaption = "Open Max File" 
-- 					thefilename = (RsConfigGetProjRootDir()+"art/") 
-- 					theFiletypes = "Max File(*.max)|*.max|All|*.*|" 
-- 					thehistoryCategory = "RsArtPresets"
-- 					
-- 					filename = getOpenFileName caption:thecaption filename:thefilename types:theFiletypes historyCategory:thehistoryCategory	
-- 					if filename != undefined then	
-- 					(	
-- 						mergeMAXFile filename #select #prompt #mergeDups #useMergedMtlDups #promptReparent 
-- 						if $selection.count == 1 then -- has to be single object
-- 						(
-- 							obj = $selection[1]
-- 							if obj != undefined and sourceFilter obj== true then  -- is a valid edit poly or edit mesh
-- 							(
-- 								getSourceObjBtn.text = obj.name
-- 								getSourceObjBtn.checked = false
-- 								sourceObj = obj	
-- 								return true
-- 							) else (	
-- 								delete $selection					
-- 								checkError =  (gotLoadError 1)
-- 								if checkError == false then return false
-- 							)
-- 						) else (
-- 							delete $selection
-- 							checkError =  (gotLoadError 2)
-- 							if checkError == false then return false
-- 						)
-- 					) else (
-- 						getSourceObjBtn.checked = false
-- 						getSourceObjBtn.text = "[pick object]"
-- 						return false
-- 					)
-- 				) else (
-- 					checkError =  (gotLoadError 3)
-- 					if checkError == false then return false
-- 				)
-- 			)
-- 			
-- 			fn replaceTargetWithSource obj = 
-- 			(
-- 				-- Extract the skindata
-- 				skinUtils.ExtractSkinData targetObj
-- 				theSkinData = getnodebyname ("SkinData_" + targetObj.name)
-- 				
-- 				-- set up the new prop to match the skinned obj
-- 				obj.center = skinInfo.theObjectCenter 
-- 				
-- 				if (getCommandPanelTaskMode() != #modify) do (setCommandPanelTaskMode #modify)
-- 				
-- 				select obj
-- 				maxOps.CollapseNodeTo obj 1 off
-- 				convertTo obj skinInfo.theObjectClass
-- 				
-- 				-- Add the skin and bones and import skin data
-- 				modPanel.addModToSelection (Skin ()) ui:on
-- 				theSkin = obj.modifiers[1]
-- 				for i in 1 to skinInfo.infBones.count do
-- 				(
-- 					if i == skinInfo.infBones.count then updateInteger = 1 else updateInteger = 0
-- 					skinOps.addbone theSkin skinInfo.infBones[i] updateInteger
-- 				)
-- 				selectmore theSkinData
-- 				skinUtils.ImportSkinDataNoDialog true false false false  false 1.0 0		
-- 				delete theSkinData
-- 				
-- 				-- replace the skinned prop with the new prop
-- 				newInstance = instanceReplace targetObj obj
-- 				InstanceMgr.MakeObjectsUnique $ #individual 
-- 				delete obj	
-- 				
-- 				-- clean up
-- 				skinInfo = RsSkinInfo()
-- 				sourceObj = undefined
-- 				getSourceObjBtn.text = "[pick object]"
-- 				targetObj = undefined
-- 				getTargetObjBtn.text = "[pick object]"
-- 			)

-- 			
-- 			-- Events	
-- 			on getSourceObjBtn changed val do 
-- 			(
-- 				try
-- 				(
-- 					gotSource = loadSource()
-- 					if gotSource == true then
-- 					(
-- 						replaceTargetWithSource sourceObj
-- 					)
-- 				) catch (print "There was an error")
-- 			)
-- 			
-- 			on getTargetObjBtn picked obj do 
-- 			(
-- 				targetObj = obj
-- 				getTargetObjBtn.text = obj.name
-- 				skinInfo.getObjectInfo obj
-- 			)	
-- 			
-- 			on getSourceObjBtn rightclick do 
-- 			(
-- 				sourceObj = undefined
-- 				getSourceObjBtn.text = "[pick object]"
-- 			)
-- 			
-- 			on getTargetObjBtn rightclick do 
-- 			(
-- 				targetObj = undefined
-- 				getTargetObjBtn.text = "[pick object]"
-- 			)
-- 			
-- 			-- Save rolled-up state:
-- 			on RsTransferSkinPropRoll rolledUp down do 
-- 			(
-- 				RsSettingWrite "RsTransferSkinPropRoll" "rollup" (not down)
-- 			)
-- 		)

-- 		-- Create floater:
-- 		(
-- 			RsPropRiggingTools = newRolloutFloater "Prop Rigging Tools" 286 630
-- 			
-- 			addRollout RsPropRigToolBannerRoll RsPropRiggingTools border:False
-- 			
-- 			for thisRoll in #(RsBoneCreateRoll, RsBoneConstraintsRoll, RsBoneXmlRoll, RsBoneFragConvertRoll, RsTransferSkinPropRoll, RsBoneDebugRoll) do 
-- 			(
-- 				local defaultRolledUp = (isProperty thisRoll #defaultRolledUp)
-- 				addRollout thisRoll RsPropRiggingTools rolledup:(RsSettingsReadBoolean thisRoll.name "rollup" defaultRolledUp)
-- 			)
-- 		)