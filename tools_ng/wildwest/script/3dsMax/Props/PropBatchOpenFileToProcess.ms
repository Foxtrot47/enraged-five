-- This script is called to open each maxfile in a list, as part a batch prop-processing session:
global gRsBatchJobCurrentMaxFile

if gRsBatchJobCurrentMaxFile != undefined then
(
	if (matchpattern gRsBatchJobCurrentMaxFile pattern:"*.maxc" ) then
	(
		try
		(
			print ("-->" + gRsBatchJobCurrentMaxFile as string )
		
			openFileResult = loadMaxFile gRsBatchJobCurrentMaxFile useFileUnit:true quiet:true 
			print ("Open File: " + openFileResult as string) 
		)
		catch
		(
			print ("Error could not open file: " + gRsBatchJobCurrentMaxFile)
			
			format "Exception caught while opening the props:\n%\n" (getCurrentException())
		)
	)
	else if (matchpattern gRsBatchJobCurrentMaxFile pattern:"*.max" ) then
	(
		try
		(
			print ("-->" + gRsBatchJobCurrentMaxFile as string )
		
			openFileResult = loadMaxFile gRsBatchJobCurrentMaxFile useFileUnit:true quiet:true 
			print ("Open File: " + openFileResult as string) 
		)
		catch
		(
			print ("Error could not open file: " + gRsBatchJobCurrentMaxFile)
			
			format "Exception caught while opening the props:\n%\n" (getCurrentException())
		)
	)
	else
	(
		print "Could not open file."
	)
)