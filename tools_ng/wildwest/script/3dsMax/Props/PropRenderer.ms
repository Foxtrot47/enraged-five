--
-- File:: proprenderer.ms
-- Description:: Utility to render props for use in browser.
--
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Author:: Neal D Corbett <neal.corbett@rockstarleeds.com>

-- Required for building fragment-proxys:
filein "pipeline/export/maps/mapsetup.ms"
filein (RsConfigGetWildwestDir() + "script/3dsmax/Props/PropRenderUtils.ms")
filein (RsConfigGetWildwestDir() + "script/3dsmax/Props/PropBatchRendererUtils.ms")
-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------
try (destroyDialog RsRenderPropsRoll) catch ()

global g_propRendererSettingsObject
g_propRendererSettingsObject = RsPropRenderSettings()
rollout RsRenderPropsRoll "Prop Renderer" width:320
(
	local propRenderUtilObj = RsPropRenderUtils()
	
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:RsRenderPropsRoll.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Prop_Renderer" versionNum:2.20 versionName:"Special Truck" filename:(getThisScriptFilename())

	local btnWidth = 140
	local btnHeight = 30
	
	button btnRenderSelected "Render Selected" enabled:true width:btnWidth height:btnHeight across:2
	button btnRenderAll "Render All" enabled:true width:btnWidth height:btnHeight
	
	button btnSetSceneBits "Turn on Default Lighting" tooltip:"Set scene to use Default Lighting, which is often better for viewport-rendering props"
	
	group "Options:"
	(	
		checkbox chkRenderStandard "Render default image" across:2 checked:true
		label lblCamDir "Camera Direction:" align:#right offset:[0,-9]
		checkbox chkRenderScaleObj "Render image with scale-object" checked:true offset:[0,-4]
		
		checkbox chkSyncTextures "Sync to latest textures" checked:true --offset:[0,-4]
		checkbox chkRebuildFrags "Rebuild Fragment Proxies" checked:true offset:[0,-4]
		
		spinner spnJpegQuality "Jpeg Quality:" range:[0,100,70] type:#integer align:#right width:56 offset:[0,-6]
		spinner spnMinCamZoom "Min zoom-size:" range:[0,1,0.4] type:#float align:#right width:58

		edittext edtRenderOutPath "Output Path:" labelOnTop:true text:(RsMakeBackSlashes RSrefData.thumbsDir) offset:[0,-20]
		checkbox chkPerforce "Perforce: Add/edit files" offset:[0,0] checked:true align:#left
	)
	
	group "Batch Render"
	(
		edittext edtBatchDirPath "Source Folder:" labelOnTop:true text:(RsMakeBackSlashes (RsProjectGetArtDir() + "/models/props/")) width:(RsRenderPropsRoll.width - 50) offset:[0,-4] across:2
		button browseBatchDirPath "..." tooltip:"Select new Batch Render source-path" align:#right width:21 height:21 offset:[0,12]
		button btnBatchRender "Batch Render" enabled:true width:btnWidth height:btnHeight
	)
	
	local camBtnWidth = 32
	local camBtnHeight = 22
	checkButton chkBtnCamDirSE "SE \\" 		width:camBtnWidth height:camBtnHeight pos:(lblCamDir.pos + [8,18])
	checkButton chkBtnCamDirSW "/ SW" 	width:camBtnWidth height:camBtnHeight pos:(chkBtnCamDirSE.pos + [camBtnWidth,0])
	checkButton chkBtnCamDirNE "NE /" 		width:camBtnWidth height:camBtnHeight pos:(chkBtnCamDirSE.pos + [0,camBtnHeight])
	checkButton chkBtnCamDirNW "\\ NW" 	width:camBtnWidth height:camBtnHeight pos:(chkBtnCamDirSE.pos + [camBtnWidth,camBtnHeight]) checked:true
	
	local camBtns = #(chkBtnCamDirSE, chkBtnCamDirSW, chkBtnCamDirNE, chkBtnCamDirNW)
	
	---------------------------------------------------------------------------
	-- Functions
	---------------------------------------------------------------------------
	
	fn camBtnClicked btnNum = 
	(
		camBtns.checked = false
		camBtns[btnNum].checked = true
	)
	
	on chkBtnCamDirSE changed clicked do (camBtnClicked 1)
	on chkBtnCamDirSW changed clicked do (camBtnClicked 2)
	on chkBtnCamDirNE changed clicked do (camBtnClicked 3)
	on chkBtnCamDirNW changed clicked do (camBtnClicked 4)
	

	---------------------------------------------------------------------------
	-- Events
	---------------------------------------------------------------------------
	
	fn updateRenderSettings=
	(
		local renderAngle = case of 
		(
			(chkBtnCamDirSE.checked):(eulerAngles 0 0 90)
			(chkBtnCamDirSW.checked):(eulerAngles 0 0 0)
			(chkBtnCamDirNE.checked):(eulerAngles 0 0 180)
			(chkBtnCamDirNW.checked):(eulerAngles 0 0 -90) -- Default direction
		)			
		
		g_propRendererSettingsObject.buildFragsBool = chkRebuildFrags.checked
		g_propRendererSettingsObject.p4checkoutBool = chkPerforce.checked
		g_propRendererSettingsObject.syncTexturesBool = chkSyncTextures.checked
		g_propRendererSettingsObject.renderStandardBool = chkRenderStandard.checked
		g_propRendererSettingsObject.renderScaleObjectBool = chkRenderScaleObj.checked
		g_propRendererSettingsObject.renderAngle = renderAngle
		g_propRendererSettingsObject.renderOutputPath = edtRenderOutPath.text
		g_propRendererSettingsObject.jpgQualitySetting = spnJpegQuality.value
		g_propRendererSettingsObject.minCamZoom = spnMinCamZoom.value
		
		/*
		local structMembers = getPropNames propRenderSetting
		for i in structMembers do
		(
			print (i as string + " " + (getProperty propRenderSetting i) as string)
		)
		*/
	)
	
	on browseBatchDirPath pressed do 
	(
		--edtBatchDirPath "Source Folder:" labelOnTop:true text:(RsMakeBackSlashes (RsProjectGetArtDir() + "/models/props/")) width:(RsRenderPropsRoll.width - 50) offset:[0,-4] across:2
		local newFolder = getSavePath caption:"Change Batch Render source-folder:" initialDir:(edtBatchDirPath.Text)
		
		if (newFolder != undefined) do 
		(
			edtBatchDirPath.Text = (newFolder + "\\")
		)
	)
	
	on btnBatchRender pressed do
	(
		local propBatchUtilInst = propBatchRenderUtils()
		
		updateRenderSettings() -- updated the settings to match any UI changes...
		local tempDir = edtBatchDirPath.text
		propBatchUtilInst.startBatchRenderFromDirectory tempDir "*.max"
	)
	
	on btnRenderSelected pressed do 
	(
		updateRenderSettings()
		propRenderUtilObj.RenderObjs (selection as array) g_propRendererSettingsObject
	)
	
	on btnRenderAll pressed do 
	(
		updateRenderSettings()
		
		--propRenderUtilObj.RenderObjs (geometry as array) chkRebuildFrags.checked chkPerforce.checked chkSyncTextures.checked edtRenderOutPath.text chkRenderStandard.checked chkRenderScaleObj.checked renderAngle
		propRenderUtilObj.RenderObjs (geometry as array) g_propRendererSettingsObject
	)
	
	on btnSetSceneBits pressed do 
	(
		-- Turn on default lighting:
		actionMan.executeAction -844228238 "7"
	)
	
	on RsRenderPropsRoll open do 
	(
		banner.setup()
		updateRenderSettings()
	)
)
createDialog RsRenderPropsRoll