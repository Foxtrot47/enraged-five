-- Setup default materials for the Props team
-- Stewart Wright
-- January 2010

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

-- Set up some common variables
maxMats=24
baseSub=10

whatSlot = #()
propShader = "default.sps"


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- build a new multisub
fn create_multisub_material =
(
	matSlotName = "Default_" + ((whatSlot) as string)
	meditMaterials[whatSlot] = (multimaterial numsubs:baseSub)
	meditMaterials[whatSlot].name = matSlotName
	for sub = 1 to baseSub do
	(
		subSlotName = "Mat_" + ((whatSlot) as string) + " Sub_0" + ((sub) as string)
		-- Set all the sub materials to be Rage shaders
		meditMaterials[whatSlot].material[sub] = Rage_Shader ()
		-- And set them to the correct default shader for props
		RstSetShaderName meditMaterials[whatSlot].materiallist[sub] propShader
		-- Now rename them to something
		-- This line names the sub-material
		meditMaterials[whatSlot].material[sub].name = subSlotName
		-- This line names the 'name' column
		meditMaterials[whatSlot].names[sub] = "Default"
	)
)


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- add 1 new sub material to the selected multisub
fn plusOne =
(
	howMany = meditMaterials[whatSlot].material.count
	-- store the original material in an array
	saveMe = meditMaterials[whatSlot]
	
	-- add 1 value to the array, this updates the submaterial
	saveMe.count = saveMe.count + 1
	
	-- copy the last 2nd last material to the new (empty) slot
	meditMaterials[whatSlot][howMany+1] = copy meditMaterials[whatSlot][(howMany)] 
	-- and rename it to something
	newSubName = "Mat_" + ((whatSlot) as string) + " Sub_0" + ((howMany+1) as string)
	meditMaterials[whatSlot].material[(howMany +1)].name = newSubName
	meditMaterials[whatSlot].names[(howMany +1)] = "Default"
)

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
fn justTheOne =
(
		whatSlot = activeMeditSlot
)


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--create rollout
rollout PropMatTools "Prop Material Setup Tools"

(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Prop_Default_Shaders" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)
	

	
	label desc1 "Material Tools:"
	label desc2 "Setup Default Materials"

	--create buttons
	button btnSetupAll "Setup All Materials" width:160 height:40 toolTip:"Setup default shaders in all material slots"
	button btnResetSel "Reset Selected Material" width:160 height:40 toolTip:"Reset selected material to a default setup"
	button btnAddSub "Add to Multi/Sub Material" width:160 height:40 toolTip:"Add an extra Multi/Sub material to selected material"

	on btnSetupAll pressed do
	(
		for matCount = 1 to maxMats do
		(
			whatSlot = matCount
			create_multisub_material ()
		)
		
	)
	
	on btnResetSel pressed do
	(
		justTheOne ()
		create_multisub_material 	()
	)
	
		on btnAddSub pressed do
	(
		justTheOne ()
		plusOne ()
	)

)--end of rollout "Prop Material Setup Tools"
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
createDialog  PropMatTools width:180