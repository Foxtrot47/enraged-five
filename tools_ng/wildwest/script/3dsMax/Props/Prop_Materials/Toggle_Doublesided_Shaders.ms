-- Script to turn double sided shaders on/off on selected objects 
-- Rick Stirling
-- Rockstar North
-- July 2012


-- Written to quickly turn off the double sided shaders on vegetation for tree vegatation







-- Get the current material IDs 
fn get_material_ids geoObj =
(

	-- Parse the geo to get the polycount and shader data
	-- Create a virtual copy of the mesh as a trimesh for better stats
	tempdatamesh = snapshotAsMesh geoObj as TriMesh
	
	-- Polycount
	thepolycount = getnumfaces tempdatamesh
	
	
	-- Some arrays for holding the material IDs
	objMaterials = #()
	uobjMaterials = #()
	
	materialClass = classof geoObj.material
	if (materialClass == Multimaterial) then
	(
		for facecount = 1 to thepolycount do
		(
			face = getFace tempdatamesh facecount 
			matid = getFaceMatId tempdatamesh facecount 
			append objMaterials matid
			appendifunique uobjMaterials matid
		)	
	)
	
	else 
	(
		append objMaterials "1"
		append uobjMaterials "1"
	)	
		
	uobjMaterials -- Return this 
	
)	




-- Set the sidedness on the selected object
-- Takes an object and a true/false for the shader variable 
fn set_doubleside_shader geoObj togglemode =
(
	matList = get_material_ids geoObj
	
	objMaterial = geoObj.material
	
	-- Deal with multisubs first, these are the most common
	if (classof objMaterial == Multimaterial) then 
	(
		for matIndex = 1 to matList.count do 
		(
			theSubMatID = matList[matIndex]
			thematerial = objMaterial[theSubMatID]
				
			try(RstSetIsTwoSided thematerial togglemode)catch()
		)	
	)
	
	else
	(
		thematerial = objMaterial
		try(RstSetIsTwoSided thematerial togglemode)catch()
	)


	
)


-- UI here



-- HACKS for testing

for obj in selection do 
(	
	print obj.name
	geoObj = obj

	set_doubleside_shader geoObj false
)



