-- ****************************************************************************************
--TXD and Collision Group Attribute tagger
--Stewart Wright - RockstarNorth - 15/10/12
--A script to tag mesh TXD and Collision Group mesh attributes with the root mesh's name
-- ****************************************************************************************
--Functions
-- ****************************************************************************************
-- collects all children recursively. arguments:
-- found on http://forums.cgsociety.org/showpost.php?p=4427477&postcount=2
fn collectChildren obj &allChildren includeParent:false includeHidden:false =
(
	-- check if object should be collected
	if (includeHidden or not obj.isHiddenInVpt) and includeParent and finditem allChildren obj == 0 then
	
	append allChildren obj

	-- collect current object's children
	for c in obj.children do collectChildren c &allChildren includeParent:true includeHidden:includeHidden
) -- end collectChildren
-- ****************************************************************************************
--tag a root object and its children with the root object name
--meshesToTag = an array of meshes we will update the TXD and Collision Group attributes on
fn tagTXDandCollision meshesToTag =
(
	rootMeshes = #()--an array for the root meshes
	for obj in selectedMeshes do--for each mesh
	(
		rootObj = WWfindRootObject obj--find the root mesh
		appendIfUnique rootMeshes rootObj--add the root mesh to an array
	)

	taggedMeshes = #()
	for obj in rootMeshes do--for each mesh
	(
		allChildren = #()--an array for the root and the children
		rootAndChildMeshes = #()--an array for root and child meshes only
		
		objName = obj.name--get the mesh name
		collectChildren obj &allChildren includeParent:true includeHidden:true --collect all the skeleton heirarchy
		for justMeshes in allChildren do--we only want meshes
		(
			if superClassOf justMeshes == GeometryClass do appendIfUnique rootAndChildMeshes justMeshes
		)
		
		for toTag in rootAndChildMeshes do
		(
			setAttribute toTag "TXD" objName--add the mesh name to the TXD field
			setAttribute toTag "Collision Group" objName--add the mesh name to the Collision Group field
			appendIfUnique taggedMeshes toTag
		)
	)
	taggedMeshes--pass out the meshes we tagged
)--end tagTXDandCollision
-- ****************************************************************************************
--UI
-- ****************************************************************************************
	if ((TXDCollGroupTagger != undefined) and (TXDCollGroupTagger.isDisplayed)) do
	(destroyDialog TXDCollGroupTagger)
-- ****************************************************************************************
rollout TXDCollGroupTagger "Update TXD and Collision Group Attributes"
(
	dotNetControl rsBannerPanel "Panel" pos:[0,0] width:TXDCollGroupTagger.Width height:32
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"TXDCollGroupTagger" filename:(getThisScriptFilename())

	button btnTagThese "Update Attributes" width:125 height:35 tooltip:"Update selected TXD and Collision Group attributes."

	on TXDCollGroupTagger open do banner.setup()

	on btnTagThese pressed do
	(
		selectedMeshes = #()--an array for the meshes we'll be editing
		selectedThings = getCurrentSelection()--everything selected

		if selectedThings.count != 0 then
		(
			for justMeshes in selectedThings do--we only want meshes
			(
				if superClassOf justMeshes == GeometryClass do appendIfUnique selectedMeshes justMeshes
			)

			updatedMeshes = tagTXDandCollision selectedMeshes--run the function on this array
			
		--start messagebox stuff
			messageMeshText = ""
			for obj in updatedMeshes do
			(
				messageMeshText = messageMeshText + obj.name + "\r\n"
			)
			messageBoxText = "The TXD and Collision Group attributes have been update on:\r\n" + messageMeshText
			messageBox messageBoxText
		--end messagebox stuff
		)
	)
)
-- ****************************************************************************************
createDialog TXDCollGroupTagger width:135