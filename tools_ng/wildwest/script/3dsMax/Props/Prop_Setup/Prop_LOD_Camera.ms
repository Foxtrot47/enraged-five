-- ****************************************************************************************
/* ****************************************************************************************
Prop LOD Camera
Stewart Wright - RockstarNorth
25/06/12

A script for simulating lod changes.
Script takes a defined group of lods and presents them in a way that simulates the lod transition
-- ****************************************************************************************
-- ****************************************************************************************/
-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

-- ****************************************************************************************
--common variables for camera script
camPos = ""
camFOV = 45
camNC = 1
camFC = 1000
camNR = 0
camFR = 1000
camMPass = off
camPos = undefined
camSel = off
autoToggle = false
LODCam = undefined
standardCamDistance = -4.5
--end common variables
LODInstances = #()
LODInfoArray = #()
LODDistances = #()
-- ****************************************************************************************
-- ****************************************************************************************
fn camBuild xyzPoints =
--xyzPoints = the x,y and z points --we'll use the y for the dolly axis and the 
(
	try
	(
		delete $LOD_Camera
		print "old camera deleted, creating new camera"
	)catch()

	print "creating camera"
	
	camPos = [xyzPoints[1], standardCamDistance, xyzPoints[3]]
	
	--create a camera using the predefined variables and name it
 	LODCam = Targetcamera fov:camFOV nearclip:camNC farclip:camFC nearrange:camNR farrange:camFR mpassEnabled:off mpassRenderPerPass:off pos:camPos isSelected:camSel target:(Targetobject transform:(matrix3 [1,0,0] [0,1,0] [0,0,1] xyzPoints))
	LODCam.name = "LOD_Camera"
	
	viewport.setCamera LODCam --set the view to the camera
	--viewport.SetRenderLevel #smoothhighlights --set the viewport to smooth + highlights
)--end camRefresh
-- ****************************************************************************************
-- ****************************************************************************************
-- ****************************************************************************************
	if ((propLODDisplay != undefined) and (propLODDisplay.isDisplayed)) do
	(destroyDialog propLODDisplay)
-- ****************************************************************************************
rollout propLODDisplay "LOD Camera Sim for Props"
(
	-- ****************************************************************************************
	--functions to create the listview
	fn initListView lv =
	(
		lv.gridLines = true
		--The following controls the display of details. We use defaults:
		lv.View = (dotNetClass "System.Windows.Forms.View").Details
		lv.fullRowSelect = true
		lv.Columns.add "Mesh Name" 180 --add column with name and optional width
		lv.Columns.add "LOD Dist" 55 --add column with name and optional width
	)--end initListView

	fn fillInSpreadSheet lv =
	(
		lv.items.clear()
		theRange = #() --array to collect the list items
		for o=1 to LODInfoArray.count do
		(
			--First we create a ListViewItem objectwith the object's name:
			li = dotNetObject "System.Windows.Forms.ListViewItem" LODInfoArray[o][1].name
			--Then we add all the sub-itemswith the desired string values:
			sub_li = li.SubItems.add (LODInfoArray[o][2] as string)
			append theRange li--we add the list item to the array
		)
		lv.Items.AddRange theRange--when done, we populate the ListView
	)--end fillInSpreadSheet
	--end listview functions
	-- ****************************************************************************************

	-- ****************************************************************************************
	--function to clean up what the script does.  deletes instanced meshes and unhides hidden ones.
	fn tidyUp =
	(
		try
		(
			for delInstance=1 to LODInstances.count do
			(
				try(delete LODInstances[delInstance])catch()
			)
		)catch(print "Failed to delete instanced meshes")
		
		try
		(
			for unhideLODs=1 to LODInfoArray.count do
			(
				unhide LODInfoArray[unhideLODs][1]
			)
		)catch(print "Failed to unhide LOD meshes")
		
		try (delete LODCam)catch(print "Failed to delete LOD camera")
	)--end tidyUp

	-- ****************************************************************************************
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:propLODDisplay.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"CS_Prop_LOD_Camera" filename:(getThisScriptFilename())

	button btnSelMeshes "Define LOD Meshes" width:200 height:60 tooltip:"Select the LOD meshes you want to view"
	dotNetControl lv_objects "System.Windows.Forms.ListView" width:240 height:130 align:#center
	button btnClearMeshes "Clear List" width:200 height:20 tooltip:"Select the LOD meshes you want to view"

	group "Camera"
	(
		button btnCreateCamera "Create LOD Camera" width:200 height:30 tooltip:"Create a LOD camera"
		slider camDist "Camera Distance:" orient:#horizontal ticks:10 range:[0,1,0]
		checkbox chkAutoToggleMeshes "Automatically Toggle Between LOD Levels" checked:autoToggle
	)
	-- ****************************************************************************************
	
	on propLODDisplay open do 
	(
		initListView lv_objects
		banner.setup()
		
		tidyUp()

		if selection.count != 0 do
		(
			if queryBox "There are meshes selected.\r\nDo you want to define these as the LOD meshes?" beep:false do
			(
				selectedLODMeshes = getCurrentSelection()
				max hide inv
				clearSelection()
				
				
				LODInstances = #()
				LODInfoArray = #()
				LODDistances = #()
				
				for LODMesh=1 to selectedLODMeshes.count do --get the name and lod information from the selected meshes
				(
					LODDistance = readAttribute selectedLODMeshes[LODMesh] "LOD distance" ""
					meshAndDistance = #(selectedLODMeshes[LODMesh], LODDistance)
					append LODInfoArray meshAndDistance
				)

				qsort LODInfoArray sortArraybySubIndex subIndex:2 --sort the array by the lod distance
				fillInSpreadSheet lv_objects --populate the mesh list

				for instanceMesh=1 to LODInfoArray.count do --we will clone the lod meshes so the source ones are left in position
				(
					maxOps.cloneNodes LODInfoArray[instanceMesh][1] cloneType:#instance newNodes:&nnl
					nnl.name = "inst_" + LODInfoArray[instanceMesh][1].name
					append LODInstances nnl
					hide LODInfoArray[instanceMesh][1]
				)
				
				for ld=1 to LODInfoArray.count do
				(
					append LODDistances LODInfoArray[ld][2]
				)

				for alignInstance=2 to LODInstances.count do --align the meshes to the high level lod
				(
					LODInstances[alignInstance][1].position = LODInstances[1][1].position
				)
				clearSelection()
				max hide inv
				unhide LODInstances[1]

				lv_objects.Items.Item[0].selected = true
				lv_objects.focus()

				maxCamDistance = (LODInfoArray[LODInfoArray.count][2] + 100)
				startCamDistance = LODInfoArray[1][2]
				
				propLODDisplay.camDist.range = [standardCamDistance, maxCamDistance, startCamDistance]
			)
		)
	)
	
	on btnSelMeshes pressed do
	(
		tidyUp()

		if selection.count != 0 then
		(
			selectedLODMeshes = getCurrentSelection()
			max hide inv
			clearSelection()
			
			
			LODInstances = #()
			LODInfoArray = #()
			LODDistances = #()
			
			for LODMesh=1 to selectedLODMeshes.count do --get the name and lod information from the selected meshes
			(
				LODDistance = readAttribute selectedLODMeshes[LODMesh] "LOD distance" ""
				meshAndDistance = #(selectedLODMeshes[LODMesh], LODDistance)
				append LODInfoArray meshAndDistance
			)

			qsort LODInfoArray sortArraybySubIndex subIndex:2 --sort the array by the lod distance
			fillInSpreadSheet lv_objects --populate the mesh list

			for instanceMesh=1 to LODInfoArray.count do --we will clone the lod meshes so the source ones are left in position
			(
				maxOps.cloneNodes LODInfoArray[instanceMesh][1] cloneType:#instance newNodes:&nnl
				nnl.name = "inst_" + LODInfoArray[instanceMesh][1].name
				append LODInstances nnl
				hide LODInfoArray[instanceMesh][1]
			)
			
			for ld=1 to LODInfoArray.count do
			(
				append LODDistances LODInfoArray[ld][2]
			)

			for alignInstance=2 to LODInstances.count do --align the meshes to the high level lod
			(
				LODInstances[alignInstance][1].position = LODInstances[1][1].position
			)
			clearSelection()
			max hide inv
			unhide LODInstances[1]

			lv_objects.Items.Item[0].selected = true
			lv_objects.focus()

			maxCamDistance = (LODInfoArray[LODInfoArray.count][2] + 100)
			startCamDistance = LODInfoArray[1][2]
			
			propLODDisplay.camDist.range = [standardCamDistance, maxCamDistance, startCamDistance]
		)
		else
		(
			messageBox "Nothing Selected"
		)
	)--end btnSelMeshes

	on lv_objects ItemSelectionChanged c do
	(
		local selectedNodeMesh = c.itemindex + 1
		clearSelection()
		max hide inv
		unhide LODInstances[selectedNodeMesh]
	)
	
	on btnClearMeshes pressed do
	(
		tidyUp()
		LODInstances = #()
		LODInfoArray = #()
		LODDistances = #()
		fillInSpreadSheet lv_objects --populate the mesh list
	)	
	
	on btnCreateCamera pressed do
	(
		if LODInfoArray.count != 0 do
		(
			camPos = LODInfoArray[1][1].position
			camBuild camPos
		)
	)		
	
	on camDist changed val do 
	(
		if propLODDisplay.chkAutoToggleMeshes.checked == true do --if the toggle is true then we'll Automatically hide and unhide the levels of detail
		(
			for aLODMesh=1 to LODDistances.count do
			(
				if val > LODDistances[aLODMesh] do
				(
					clearSelection()
					max hide inv
					unhide LODInstances[aLODMesh] 
				)
			)
		)
		if LODCam != undefined do
		(
			LODCam.pos = [camPos[1], val , camPos[3]] --move the camera on the y axis based on the slider value
		)
	)
	
	--shutdown
	on propLODDisplay close do
	(
		tidyUp()
	)
)
-- ****************************************************************************************
createDialog propLODDisplay width:250