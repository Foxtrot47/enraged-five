filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

struct propParticleDataStruct
(
	sceneName,
	propName,
	fxName,
	parentFX,
	
	uid,
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn genUID =
	(
		local dataStr = (sceneName + propName + fxName)
		
		uid = getHashValue dataStr 1
	)
)

struct propParticleScannerStruct
(
	processPaths = #((RsProjectGetAssetsDir() + "/export/levels/gta5/props"),
						(RsProjectGetAssetsDir() + "/export/levels/gta5/_citye"),
						(RsProjectGetAssetsDir() + "/export/levels/gta5/_cityw"),
						(RsProjectGetAssetsDir() + "/export/levels/gta5/_hills"),
						(RsProjectGetAssetsDir() + "/export/levels/gta5/_prologue"),
						(RsProjectGetAssetsDir() + "/export/levels/gta5/_interiors"),
						(RsProjectGetAssetsDir() + "/export/levels/gta5/destruction")),
	
	processXmlList = #(),
	--processedZipRoot = (RsProjectGetAssetsDir() + "/processed/levels/gta5/props"),
	extractPath = (systemTools.getEnvVariable "TEMP") + "\propParticleScanner",
	tempITYPpath = (systemTools.getEnvVariable "TEMP") + "\propITYP",
	entityFXPath = (RsConfigGetBuildDir() + "common/data/effects/entityfx.dat"),
	entityFXIdx = #(),
	entityFXList = #(),
	
	propParticleUIDList = #(),
	propParticleDataList = #(),
	
	UIHandle = undefined,
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn processReporter msg =
	(
		if UIHandle != undefined do
		(
			UIHandle.edtProcess.text = msg
			windows.processPostedMessages()
		)
	),
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn extractITYP =
	(
		gRsPerforce.sync (RsProjectGetAssetsDir() + "/processed/levels/gta5/")
		local propArchives = #()
		for item in processedZipPaths do join propArchives (getFilesRecursive item "*.zip")
		
		if not isDirectoryWriteable extractPath then makeDir extractPath
		if not isDirectoryWriteable tempITYPpath then makeDir tempITYPpath
			
		local zipFile = undefined
		
		for item in propArchives do
		(
			try
			(
				zipFile = (dotNetClass "Ionic.Zip.ZipFile").Read item
				zipFile.TempFileFolder = extractPath
				extractAction = dotNetClass "Ionic.Zip.ExtractExistingFileAction"
				
				for i=0 to (zipFile.count - 1) where (matchPattern zipFile.item[i].filename pattern:"*.ityp") do
				(
					zipFile.item[i].extract tempITYPpath extractAction.OverwriteSilently
				)
				
				zipFile.dispose()
			)
			catch
			(
				format "failed to extract: % \n" item
				zipFile.dispose()
			)
		)
	),
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn parseEntityFX =
	(
		local fh = openFile entityFXPath mode:"r"
		
		while not eof fh do
		(
			local thisLine = readLine fh
			
			if not (thisLine[1] == "#") do
			(
				local lineBits = filterString thisLine "\t"
				if lineBits.count > 1 do
				(
					append entityFXIdx lineBits[1]
					append entityFXList (DataPair name:lineBits[1] asset:lineBits[2])
				)
			)
		)
	),
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn scanITYP =
	(
		--get the 
		local itypFiles = getFiles (tempITYPpath + "/*.ityp")
		--break()
		
		for ityp in itypFiles do
		(
			local xmlStream = XmlStreamHandler xmlFile:ityp
			xmlStream.open()
			
			local particleEffectNodes = xmlStream.root.SelectNodes ".//Item[@type = \"CExtensionDefParticleEffect\"]"
		
			xmlStream.close()
			
			local particleEffectNodesIt = particleEffectNodes.GetEnumerator()
			while particleEffectNodesIt.MoveNext() do
			(
				local node = particleEffectNodesIt.current
				
				local dataNode = propParticleDataStruct()
				
				dataNode.sceneName = getFilenameFile ityp
				local nameNode = node.SelectSingleNode ".//name"
				dataNode.propName = nameNode.innerText
				local fxNameNode = node.SelectSingleNode ".//fxName"
				dataNode.fxName = fxNameNode.innerText
				
				dataNode.genUID()
				
				if (findItem propParticleUIDList dataNode.uid) == 0 then
				(
					append propParticleUIDList dataNode.uid
					append propParticleDataList dataNode
				)
			)
		)
	),
	
	--/////////////////////////////////////////
	-- sync and collate all the scene xml files to scan
	--/////////////////////////////////////////
	fn harvestSceneXmlFiles =
	(
		gRsPerforce.sync (RsProjectGetAssetsDir() + "/export/levels/gta5/....xml")
		for item in processPaths do join processXmlList (getFilesRecursive item "*.xml")
		--print processXmlList
	),
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn scanXml = 
	(
		escapeenable = true
		local xmlPath = (RsConfigGetAssetsDir() + "export/levels/gta5/props/roadside/v_rubbish.xml")
		
		UIHandle.dnPrgBar.value = 0
		local count = 0
		for xmlPath in processXmlList do
		(
			processReporter ("Scanning:" + xmlPath)
			local xmlDoc = xmlStreamHandler xmlFile:xmlPath
			xmlDoc.open()
			
			local particleNodes = xmlDoc.root.SelectNodes ".//object[@class = \"rsn_particle\"]"
			--print particleNodes.count
			
			local particleNodesIt = particleNodes.GetEnumerator()
			
			while particleNodesIt.MoveNext() do
			(
				local thisNode = particleNodesIt.current
				
				local effectName = (thisNode.SelectSingleNode ".//attribute[@name = \"Name\"]/@value").value
				--print effectName
				
				--get the parent object
				local parentNode = thisNode.parentNode
				local particleParentObject = undefined
				if parentNode.name == "objects" then
				(
					particleParentObject = undefined
				)
				else
				(
					while parentNode.name != "objects" do
					(
						particleParentObject = parentNode
						parentNode = parentNode.parentNode
					)
				)
				
				if particleParentObject != undefined then
				(
					local particleParentObjectName = particleParentObject.GetAttribute "name"
					local particleParentObjectClass = particleParentObject.GetAttribute "class"
					
					--depending on the class type, massage the name
					if particleParentObjectClass == "dummy" then
					(
						particleParentObjectName = substituteString particleParentObjectName "Dummy_" ""
						particleParentObjectName = substituteString particleParentObjectName "_frag_" ""
					)
					
					--store accumulated data
					local dataNode = propParticleDataStruct()
					dataNode.sceneName = getFilenameFile xmlPath
					dataNode.propName = particleParentObjectName
					dataNode.fxName = effectName
					dataNode.genUID()
					
					if (findItem propParticleUIDList dataNode.uid) == 0 then
					(
						append propParticleUIDList dataNode.uid
						append propParticleDataList dataNode
					)
				)
				else
				(
					format "not found object parent for effect:% in:% \n" effectName xmlPath
				)
			)

			xmlDoc.close()
			
			count += 1
			UIHandle.dnPrgBar.value = (count * (100.0 / processXmlList.count))
			windows.processPostedMessages()
		)
		
		UIHandle.dnPrgBar.value = 0
	),
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn assetForEffect =
	(
		for item in propParticleDataList do
		(
			local idx = findItem entityFXIdx (toUpper item.fxName)
			if idx != 0 do
			(
				item.parentFX = entityFXList[idx].asset
			)
		)
	),
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn scan = 
	(
		--extract the ityp from processed prop zips
		--processReporter "Extracting ITYP Files..."
		--extractITYP()
		
		processReporter "Syncing xml files..."
		harvestSceneXmlFiles()
		--
		processReporter "Parsing entityFX.dat..."
		parseEntityFX()
		
		--scan the architypes for CExtensionDefParticleEffect
		--processReporter "Scanning ITYP Files..."
		--scanITYP()
		
		--scan the scene xml files
		processReporter "Scanning XML Files..."
		scanXml()
		
		--match up the types found with entityfx.dat
		processReporter "Matching results..."
		assetForEffect()
		
		--for item in propParticleDataList do print item
		processReporter "Done."
	)
)


--propParticleScanner.scan()


--/////////////////////////////////////////
--	UI
--/////////////////////////////////////////
try(destroyDialog PropParticleReporterUI)catch()
rollout PropParticleReporterUI "Prop Particle Reporter" width:600 height:570
(
	--/////////////////////////////////////////
	--	VARIABLES
	--/////////////////////////////////////////
	local propParticleScanner = propParticleScannerStruct()
	
	------------------------------------------------------------------------------------
	-- DotNet values: 
	------------------------------------------------------------------------------------
	local headers = #("Scene", "Prop", "FX Name", "Asset")
	
	local textFont, dingFont, textFontBold
	
	local DNknownColour = dotNetClass "system.Drawing.KnownColor"
	local DNcolour = dotNetClass "System.Drawing.Color"
	
	local selColour = DNcolour.fromKnownColor DNknownColour.MenuHighlight
	
	local textCol = (colorMan.getColor #windowText) * 255
	local windowCol = (colorMan.getColor #window) * 255
	local notLoadedCol = if (windowCol[1] < 128) then (windowCol * 1.5) else (windowCol * 0.85)
	
	local textColour = DNcolour.FromArgb textCol[1] textCol[2] textCol[3]
	local backColour = DNcolour.FromArgb windowCol[1] windowCol[2] windowCol[3]
	local altBackColour = DNcolour.FromArgb (windowCol[1]-10) (windowCol[2]-10) (windowCol[3]-10)
	local inSceneBackColour = DNcolour.FromArgb windowCol[1] windowCol[2] (windowCol[3])
	
	--/////////////////////////////////////////
	--	CONTROLS
	--/////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:PropParticleReporterUI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"CHANGEME" filename:(getThisScriptFilename())
	
	button btnReport "Report" width:(PropParticleReporterUI.width - 20)
	dotNetControl lstPropList "System.Windows.Forms.DataGridView" height:425 align:#center
	button btnDumpCSV "Dump To CSV" width:(PropParticleReporterUI.width - 20)
	edittext edtProcess "Process Message" readOnly:true
	dotNetControl dnPrgBar "ProgressBar"
		
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////
	fn init =
	(
		lstPropList.SelectionMode = lstPropList.SelectionMode.FullRowSelect
		lstPropList.AllowUserToAddRows = false
		lstPropList.AllowUserToDeleteRows = false
		lstPropList.AllowUserToOrderColumns = true
		lstPropList.AllowUserToResizeRows = false
		lstPropList.AllowUserToResizeColumns = false
		lstPropList.AllowDrop = false
		lstPropList.MultiSelect = true
		lstPropList.ReadOnly = true

		lstPropList.dock = lstPropList.dock.fill
		lstPropList.DefaultCellStyle.backColor = backColour
		lstPropList.DefaultCellStyle.foreColor = textColour
		lstPropList.AlternatingRowsDefaultCellStyle.BackColor = altBackColour
		
		textFont = lstPropList.font
		dingFont = dotNetObject "System.Drawing.Font" "Webdings" textFont.size
		textFontBold = dotnetobject "system.drawing.font" textFont (dotnetclass "system.drawing.fontstyle").bold

		lstPropList.EnableHeadersVisualStyles = false
		lstPropList.ColumnHeadersDefaultCellStyle.backColor = backColour
		lstPropList.ColumnHeadersDefaultCellStyle.foreColor = textColour
		lstPropList.ColumnHeadersDefaultCellStyle.font = textFontBold
		lstPropList.ColumnHeadersHeight = 34
		lstPropList.RowHeadersVisible = false
		--lstPropList.AutoSizeColumnsMode = (dotNetClass "System.Windows.Forms.DataGridViewAutoSizeColumnsMode").AllCellsExceptHeader
		
		--startSize = [1290, 500]
		
		local colNum
		
		for item in headers do 
		(
			colNum = lstPropList.Columns.add item item
			lstPropList.Columns.item[colNum].minimumWidth = 75
			lstPropList.Columns.item[colNum].autoSizeMode = (dotNetClass "System.Windows.Forms.DataGridViewAutoSizeColumnMode").AllCellsExceptHeader
		)
		
		local firstCol = lstPropList.Columns.item[0]
		--local lastCol = lstPropList.Columns.item[colNum]
		
		firstCol.width = 160
		--lastCol.AutoSizeMode = lastCol.AutoSizeMode.AllCellsExceptHeader
		
		windows.processPostedMessages()
		
		propParticleScanner.UIHandle = PropParticleReporterUI
	)
	
	fn updateList = 
	(
		lstPropList.Rows.Clear()
		
		-- Populate listview
		
		-- 
		for item in propParticleScanner.propParticleDataList do
		(
			local rowData = #(item.sceneName, item.PropName, item.fxName, item.parentFX)
			lstPropList.Rows.add rowData
		)
		
	)
	
	--/////////////////////////////////////////
	--	EVENTS
	--/////////////////////////////////////////
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btnReport pressed do
	(
		propParticleScanner.scan()
		updateList()
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btnDumpCSV pressed do
	(
		if lstPropList.Rows.count == 0 then return false
			
		local csvPath = getSavePath "Output CSV Folder" initialDir:"X:/gta5/Docs/art/VFX"
		if csvPath == undefined then
		(
			messagebox "Bad csv path" title:"Bad Path"
			return false
		)
		csvPath = csvPath + "/PropEffectsReport.csv"
		
		if (doesFileExist csvPath) and (getFileAttribute csvPath #readOnly) then deleteFile csvPath
		
		local csvFile = openFile csvPath mode:"wt"
		for item in propParticleScanner.propParticleDataList do
		(
			format "%,%,%,%\n" item.sceneName item.PropName item.fxName item.parentFX item.parentFX to:csvFile
		)
		
		close csvFile
		
		messageBox "Done"
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on PropParticleReporterUI open do
	(
		banner.setup()
		init()
		--update()
	)
)
createDialog PropParticleReporterUI style:#(#style_titlebar, #style_border, #style_sysmenu, #style_minimizebox)
