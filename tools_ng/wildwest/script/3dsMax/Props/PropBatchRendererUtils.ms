
filein "pipeline/util/batchFramework.ms"
filein (RsConfigGetWildwestDir() + "script/3dsmax/_common_functions/FN_common.ms")
--filein (RsConfigGetWildwestDir() + "script/3dsmax/Props/PropRenderUtils.ms")

struct propBatchRenderUtils
(
	batchJobInst = undefined,
	batchFrameworkInst = RsBatchFramework(),

	fn setupBatchJob filesToProcess preJobMs preFileMs jobMs postFileMs postJobMs =
	(
		local newBatchJob = RsBatchJob()
		newBatchJob.name = "temp"
		newBatchJob.preJobMaxscript = preJobMs
		newBatchJob.preTaskMaxscript = preFileMs
		newBatchJob.jobMaxscript = jobMs
		newBatchJob.postTaskMaxscript = postFileMs
		newBatchJob.postJobMaxscript = postJobMs
		
		
		
		if jobMs != undefined then
		(
		
			for filePath in filesToProcess do
			(
				
				filePath = RsMakeSafeSlashes filePath
				newBatchJob.addMaxFile filePath
				
			)
			
		)
		else
		(
			print "Warning: No script to run, so no max files added."
		)
		newBatchJob
	),

	fn setupJobFromDirectory parentDir fileType preJobMs preFileMs jobMs postFileMs postJobMs =
	(
		local filesToProcess = RsFindFilesRecursive parentDir #(fileType)
		
		--print filesToProcess.count
		if (filesToProcess.count > 0) then
		(
			local Plural = if (filesToProcess.count == 1) then "" else "s"
			local Msg = stringStream ""
			format "This process will render props for % Maxfile%.\n\nThis can be a slow process that can't be cancelled. \n\nDo you want to continue?" filesToProcess.count Plural to:Msg
			
			if (queryBox (Msg as String) Title:"Query: Do Batch Render?") do 
			(			
				local batchJobInst = setupBatchJob filesToProcess preJobMs preFileMs jobMs postFileMs postJobMs
				batchJobInst
			)
		)
		else
		(
			format "Error: no files to process!\n"
			batchJobInst = undefined
		)
	),

	fn startBatchRenderFromDirectory startDirectory fileType =
	(
		--startDirectory = "X:/rdr3/art/Models/Props/industrial"

		--fileType = "*.max"
		
		preJobMs = undefined
		preFileMs = (RsConfigGetWildwestDir() + "script/3dsMax/Props/PropBatchOpenFileToProcess.ms")
		jobMs = (RsConfigGetWildwestDir() + "script/3dsMax/Props/PropBatchRenderCall.ms")
		postFileMs = undefined
		postJobMs = undefined
		 
		batchJobInst = setupJobFromDirectory startDirectory fileType preJobMs preFileMs jobMs postFileMs postJobMs

		if batchJobInst != undefined then
		(
			batchFrameworkInst.addJob batchJobInst
			batchFrameworkInst.saveJobsQueue (startDirectory + "\\batchJob.xml")
			--batchFrameworkInst.importJobsQueue "c:\\test\\batchJob.xml"
			
			-- Stop Perforce-check callbacks during batch process:
			RsAutomatedExport = True
			
			-- Set off the batch-render job:
			batchJobInst.run()

			-- Reactivate global callbacks:
			RsAutomatedExport = False
		)
	)
)
	