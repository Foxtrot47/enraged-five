Filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_userSettings.ms")

try (destroyDialog  MeshDicerRoll) catch()
rollout MeshDicerRoll "Mesh Dicer" width:150
(	
	-- LOCALS -------------------------------------------------	
	local settings = rsta_userSettings app:"MeshDicer"
	
	-- CONTROLS ---------------------------------------------
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:MeshDicerRoll.Width
	local banner = makeRsBanner dn_Panel:rsBannerPanel versionNum:1.00  wiki:"" filename:(getThisScriptFilename())

	group "Divisions"
	(
		spinner spn_x "X : " range:[1,100,2]
		spinner spn_y "Y : " range:[1,100,2]
		spinner spn_z "Z : " range:[1,100,2]
	)
	checkbox cbx_keep "Keep Original Mesh" 		
	button btn_dice "Dice Selection" width:140	
	
	-- FUNTIONS ----------------------------------------------
	fn divideObjectAtTrans obj trans type = 
	(
		local obj_mesh = if (type == 2) then copy obj else obj
		addmodifier obj_mesh (SliceModifier Slice_Type:type Slice_Plane:trans)
		return (converttomesh obj_mesh)
	)
	---------------------------------------------------------------------------
	fn SplitMeshAtTrans obj trans = return #((divideObjectAtTrans obj trans 2),(divideObjectAtTrans obj trans 3))
	---------------------------------------------------------------------------
	fn SplitAllObjects objArray transArray =
	(
		local AllObjects = #()
		for obj in objArray do
		(
			local currentMesh = obj
			for tm in transArray do 
			(
				local mesh_ab = SplitMeshAtTrans currentMesh tm	
				for i=1 to 2 do appendIfUnique AllObjects mesh_ab[i]
				currentMesh = mesh_ab[2]		
			)
		)
		return AllObjects
	)
	---------------------------------------------------------------------------
	mapped fn gridSpliceMesh obj x_s y_s z_s keep:false = 
	(	
		max create mode	
		if (keep) do obj = copy obj	
		resetxform obj
		converttomesh obj		
		local bb = #(obj.min - obj.pos, obj.max - obj.pos)	
		local tA = #()		
		tA[1] = for i=1 to x_s-1 collect (prerotateY (transMatrix ([bb[1].x + ((bb[2].x - bb[1].x)/x_s * i) , 0, 0])) 90)
		tA[2] = for i=1 to y_s-1 collect (prerotateX (transMatrix ([0, bb[1].y + ((bb[2].y - bb[1].y)/y_s * i), 0])) -90)
		tA[3] = for i=1 to z_s-1 collect (prerotateZ (transMatrix ([0, 0, bb[1].z + ((bb[2].z - bb[1].z)/z_s * i)])) -90)
			
		local allObjects = #(obj) 
		for t in tA do allObjects = SplitAllObjects allObjects t	
		delete (for i in allObjects where (((rsmeshpolyop i).getNumFaces i) == 0) collect i)
	)
	---------------------------------------------------------------------------
	fn do_dice = 
	(
		undo "Dice Mesh" on 
		(
			gridSpliceMesh (getCurrentSelection()) spn_x.value spn_y.value spn_z.value keep:cbx_keep.checked
		)
	)
	
	-- EVENTS ---------------------------------------------------------
	on MeshDicerRoll open do 
	(
		banner.setup()
		settings.dialog_windowPos MeshDicerRoll #get
	)
	on MeshDicerRoll close do settings.dialog_windowPos MeshDicerRoll #set
	on btn_dice pressed do do_dice()
)
createDialog MeshDicerRoll style:#(#style_titlebar, #style_border, #style_sysmenu,#style_toolwindow)
