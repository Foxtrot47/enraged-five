-- 
-- File:: pipeline/helpers/maps/modelling.ms
-- Description:: Modelling Toolkit
--
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 16 March 2010
--
-----------------------------------------------------------------------------

global RsModelToolkit
try CloseRolloutFloater RsModelToolkit catch()

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein (RsConfigGetWildWestDir() + "script/3dsMax/Modelling/SelFacesByAngle_Funcs.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/Modelling/VertexSnap_Funcs.ms")
--filein ("x:\wildwest\script\3dsMax\Modelling\VertexSnap_Funcs.ms")
filein (RsConfigGetWildWestDir() + "script\3dsMax\_common_functions\FN_Geometry.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/Modelling/gridhelper_UI.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/Modelling/extrude_UI.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/Modelling/divideModel_UI.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/Modelling/alignModel_UI.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/Modelling/lodModelling_UI.ms")
filein (RsConfigGetWildWestDir() + "/script/3dsMax/Modelling/RSTA_SmoothBridge_ModellingToolkit_UI.ms")



--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
---------------------------------STRUCT
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


struct Strct_VertSnap
	(
		MyObject,
		VertPosArr = #(),
		
		
		-- Get vert initial pos (before snaping)
		fn RSTA_Get_Ini_Snap =
		(
			VertPosArr = #()
			For meshi = 1 to MyObject.count do 
			(
				TempArr = #()
				Component = MyObject[meshi]
				
				if (classof Component  == Editable_Poly) then
				(
				MyVertsCount =  polyop.getNumVerts Component
				)
				else
				(
				MyVertsCount = Component.numverts
				)
				--loop through each verts and append pos in VertPosArr
				for verti = 1 to MyVertsCount do
				(	
				if (classof Component  == Editable_Poly) then
				(
					VertPos = polyop.getVert Component verti 
				)
				else
				(
					VertPos = getVert Component verti 
				)
					append TempArr VertPos 
				)
				append VertPosArr TempArr 
			)
		),
		
		-- Load back vert initial pos
		fn RSTA_Load_Ini_snap =
		(			
			For meshi = 1 to MyObject.count do 
			(
				Component = MyObject[meshi]
				for verti = 1 to VertPosArr[meshi].count do
				(	
					if (classof Component  == Editable_Poly) then
					(	
					polyop.setVert Component verti VertPosArr[meshi][verti]
					)
					else
					(
						setVert Component verti VertPosArr[meshi][verti]
					)
				)
			)
		)
		
		
	)
	 Strct_VertSnap =  Strct_VertSnap()





--Connect closest pair of verts together in a face selection
struct Strct_ConnectClosest
	(
		MyObject,
		NewFaces,	
	
		-- Connect the closests verts
		fn RSTA_ConnectClosestVert =
		(
			--get the face selection and pass it in NewFaces
			NewFaces = polyop.getFaceSelection MyObject		
			NewFaces = NewFaces as array
			
			--get number of faces before conect
			FaceBeConec = polyop.getNumFaces MyObject
			
			
			subobjectLevel = 0
			--add this geo Update on an extra Edit-PolyModifier so it is easily undoable			
			
			modPanel.addModToSelection (Edit_Poly ()) ui:on
			suspendEditing which:#modify
			MyObject.modifiers[#Edit_Poly].name = "ConnectCloseVert"
			resumeEditing which:#modify
			--loop through new faces
			For TheFacei = 1 to NewFaces.count do
			(
				VertCoorArr = #()
				TheFace = NewFaces[TheFacei]
				
				--get all verts per new faces
				MyVerts = polyop.getVertsUsingFace MyObject TheFace 
				
				MyVerts = MyVerts as array
				
				-- get all vert positions
				For Verti = 1 to MyVerts.count do
				(
					MyVert = MyVerts[Verti]
					MyVertCoord = polyop.getVert MyObject MyVert					
					append VertCoorArr MyVertCoord
				)
	
				
				--get average
				Averaar = #()
				For posi = 1 to VertCoorArr.count do
				(
					Coord = VertCoorArr[posi]
					Avalue = (Coord[1]+Coord[2]+Coord[3])/3
					append Averaar Avalue
				)		
	
				
				SortedVertCoorArr = deepcopy  Averaar
				--sort verts vectors
				sort SortedVertCoorArr				
			
							
				--Who is who now ?  == Reconstruct the vertex id array in order of positions
				SortedVertArr = #()
				For Verti = 1 to MyVerts.count do
				(
					MyVert = MyVerts[Verti]
					MyVertCoord = polyop.getVert MyObject MyVert		
					Avalue = (MyVertCoord[1]+MyVertCoord[2]+MyVertCoord[3])/3
					Nb = findItem  SortedVertCoorArr Avalue
					SortedVertArr[Nb] = MyVert
				)	
				
				
 				--loop to conect the verts two by two
				For Verti = 2 to SortedVertArr.count by 2 do
				(
					Temp =#()
					TheVert1 =  SortedVertArr[(Verti-1)]
					TheVert2 =	SortedVertArr[Verti]
					append Temp TheVert1
					append Temp TheVert2
					BitTemp = Temp as bitarray
			
					try
					(
						subobjectLevel = 1				
						MyObject.modifiers[#ConnectCloseVert].SetSelection #Vertex #{}					
						MyObject.modifiers[#ConnectCloseVert].Select #Vertex BitTemp
						MyObject.modifiers[#ConnectCloseVert].ButtonOp #ConnectVertices
					)
					catch()						
				)
			)
		
		)
		
	)
	Strct_ConnectClosest = Strct_ConnectClosest()







-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- BANNER
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout RsModelToolkitBannerRoll ""
(
	dotNetControl RsBannerPanel "Panel" pos:[6,3] height:32 width:(RsModelToolkitBannerRoll.width - 6)
	local bannerStruct = makeRsBanner dn_Panel:RsBannerPanel VersionNum:1.10 VersionName:"Aquatic Leg" wiki:"Modelling_Toolkit"
	
	-- Refresh banner-icons when floater's rollouts are adjusted:
	on RsBannerPanel Paint Sender Args do 
	(
		for n = 0 to (RsBannerPanel.Controls.Count - 1) do 
		(
			RsBannerPanel.Controls.Item[n].Invalidate()
		)
	)
	
	--------------------------------------------------------------
	-- Rollout open/close
	--------------------------------------------------------------
	on RsModelToolkitBannerRoll open do
	(
		-- Initialise tech-art banner:
		bannerStruct.setup()
		
	)
)

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--------------------------------- HELPERS
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rollout RsModelToolHelpersRoll "Modelling Helpers"
(
	
	
	
	
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Modelling_Toolkit#Modelling_Helpers" 
		align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	groupBox grpVertex "Vertex Snap" pos:[7,20] width:130 height:120
	spinner spnSnapVal "Tolerance: " pos:[37,38] width:97 height:16 range:[0,100,1]
	pickbutton btnDoSnap "Snap To" pos:[21,62] width:102 height:22 filter:RnFilterSnapPick toolTip:"pick the object to snap to."
	button btnDoSnapSel "Snap Selected" pos:[21,88] width:102 height:22 toolTip:"Snap together selected objects by average."
	button btn_RevertSnap "Revert Snap" pos:[21,114] width:102 height:22 toolTip:"Revert your snap"
	
	groupBox grpFaces "Select Faces by Angle" pos:[143,20] width:130 height:95
	spinner spnAngle "Angle: " pos:[157,38] width:111 height:16 range:[0,100,0]
	button btnGreater "Select Greater" pos:[159,62] width:102 height:22
	button btnLess "Select Less" pos:[159,88] width:102 height:22
	
	local btnWidth = 190
	local btnHeight = 25
	
	button btnFindDupFaces "Find and Select Duplicate Faces" width:btnWidth height:btnHeight offset:[0, 30]
		toolTip:"Find and select duplicate faces on selected object."
	button btnSetBestPivot "Set Best Pivot for BBox" width:btnWidth height:btnHeight offset:[0, -3]
	button btnConvertToPoly "RS ConvertToPoly" width:btnWidth height:btnHeight offset:[0,-3]
		tooltip:"Converts selected objects to Editable Poly.\n\nThis version properly converts vertex-colours when converting Editable Meshes."
	button btnDecalCutter "Draw Decal Outline" width:btnWidth height:btnHeight offset:[0,-3]
		tooltip:"Creates new decal-mesh, taken from outline drawn over selected meshes"
		
	Button btn_ConnectClostV "Connect Closest Verts"  width:btnWidth height:btnHeight offset:[0,-3] toolTip:"Connect closest pair of verts together in a face selection"	
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	
	
	--------------------------------------------------------------
	-- Connect Closest Verts
	--------------------------------------------------------------
	on btn_ConnectClostV pressed do
		(
			Strct_ConnectClosest.MyObject = selection[1]
			
			Strct_ConnectClosest.RSTA_ConnectClosestVert()
		)
	
	
	--------------------------------------------------------------
	-- Vertex Snap
	--------------------------------------------------------------
	on btnDoSnap picked snapto do
	(	
		undo off()
		
		Strct_VertSnap.MyObject = selection
		Strct_VertSnap.RSTA_Get_Ini_Snap()
		
		
		snapset = #()
		cursel = #()
		cursel = cursel + selection	
			
		if snapto != undefined and (finditem cursel snapto == 0) then (
		
			append snapset snapto
			snapset = snapset + selection
		
			SnapVertsByTol (snapset as array) spnSnapVal.value ByAverage:false
			redrawViews()
		)
			undo on()
	)
	
	on btnDoSnapSel pressed do
	(
		Strct_VertSnap.MyObject = selection
		Strct_VertSnap.RSTA_Get_Ini_Snap()
	
		
		
		undo off()
		snapset = #()
		snapset = snapset + selection
	
		SnapVertsByTol (snapset as array) spnSnapVal.value ByAverage:true	
		redrawViews()
		undo on()
	)
	
	
	on btn_RevertSnap pressed do
	(	
		undo off()
		Strct_VertSnap.RSTA_Load_Ini_snap()
		redrawViews()
		undo on()
	)
	
	--------------------------------------------------------------
	-- Find Duplicate Faces
	--------------------------------------------------------------
	on btnFindDupFaces pressed do
	(
		local SelObjs = (GetCurrentSelection())
		
		if (SelObjs.Count == 1) and (isEditPolyMesh SelObjs[1]) do 
		(
			local Obj = SelObjs[1]
			
			-- Make sure we're on Modify tab:
			if (GetCommandPanelTaskMode() != #Modify) do 
			(
				SetCommandPanelTaskMode #Modify
			)
			
			undo "Select Duplicate Faces" on 
			(
				local ModName = "Select Duplicates"
				
				-- Add Edit_Poly modifier to non-poly objects:
				local PolyMod = undefined
				if (not isKindOf Obj Editable_Poly) do 
				(
					-- Does object already have this modifier on top of stack?
					if (Obj.Modifiers.Count != 0) and (Obj.Modifiers[1].Name == ModName) then 
					(
						PolyMod = Obj.Modifiers[1]
						PolyMod.Name = "Find Duplicates..."
						ModPanel.SetCurrentObject PolyMod
					)
					else 
					(					
						PolyMod = Edit_Poly Name:"Find Duplicates..."
						AddModifier Obj PolyMod
					)
				)
				
				--Get all the duplicate faces in the selected mesh
				local DupFaces = RsGeom_GetFaceList_Duplicated Obj IgnoreOnce:True
				
				--select one of the elements
				subobjectLevel = 4
				if (DupFaces.NumberSet > 0) then
				(
					if (PolyMod != undefined) then 
					(
						PolyMod.Name = "Select Duplicates"
						PolyMod.SetSelection #Face DupFaces
						
						-- Makes sure name-change shows:
						ModPanel.SetCurrentObject PolyMod
					)
					else 
					(
						SetFaceSelection Obj DupFaces
					)
				)
				else 
				(
					if (PolyMod != undefined) then 
					(
						DeleteModifier Obj PolyMod
					)
					
					messageBox "No duplicate faces were found on object" Title:"No duplicates found"
				)
			)
		)
	)	
	
	
	
	--------------------------------------------------------------
	-- Select Faces By Angle
	--------------------------------------------------------------
	on btnGreater pressed do (
		selectGreater spnAngle.value
	)
	
	on btnLess pressed do (
		selectLess spnAngle.value
	)
	
	--------------------------------------------------------------
	-- Set Best Pivot for BBox
	--------------------------------------------------------------
	on btnSetBestPivot pressed do (
		filein "pipeline/util/SetPivotForBBox.ms"
	)
	
	on btnConvertToPoly pressed do 
	(
		RSconvertToPoly $ showProgress:true
	)
	
	on btnDecalCutter pressed do 
	(
		undo "decal cutter" on 
		(
			filein "pipeline/helpers/models/decalCutter.ms"
		)
	)
	
	on RsModelToolHelpersRoll rolledUp down do 
	(
		RsSettingWrite "RsModelToolHelpersRoll" "rollup" (not down)
	)
)

(
	--////////////////////////////////////////////////////////////
	-- Create rollout floater:
	--////////////////////////////////////////////////////////////
	RsModelToolkit = newRolloutFloater "Modelling Toolkit" 300 700 50 96

	addRollout RsModelToolkitBannerRoll RsModelToolkit border:False

	for thisRoll in #(RsModelToolHelpersRoll, RsModelToolAlignRoll, RsModelToolDivideRoll, RsLodModellingRoll, RsModelToolGridRoll, RsModelToolExtrudeRoll, RSRollSmoothBridgeSubrollout) do 
	(
		addRollout thisRoll RsModelToolkit rolledup:(RsSettingsReadBoolean thisRoll.name "rollup" false)
	)
)
