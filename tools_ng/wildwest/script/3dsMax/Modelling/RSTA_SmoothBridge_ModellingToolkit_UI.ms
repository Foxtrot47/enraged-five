--SmoothBridge UI for ModellingToolkit Subrollout Menu.
--Bridge between two selected edges with mesh-geometry using a bezier curve algorithm.
--Notes: the main floating rollout is in RSTA_SmoothBridge_UI.ms

FileIn (RsConfigGetWildWestDir() + "script/3dsMax/Modelling/RSTA_SmoothBridge.ms")

rollout RSRollSmoothBridgeSubrollout "Smooth Bridge" Width:240 
(
	--UI
	Button btnBridge "Bridge Selected Edges" width:(RSRollSmoothBridgeSubrollout.width-4) tooltip:"Select the Edges To Bridge"
	Spinner spnSegments "Segments:" range:[0,1000,10] type:#integer  align:#left across:2 tooltip:"Number of Segmants"
	Spinner spnExtrusionUnitScale "Scale:" range:[-5.0,5.0,1.0] type:#float align:#right tooltip:"Scale of Extrusion"
	Spinner spnRelaxMidPoints "Relax:" range:[0,1,0] type:#float align:#left tooltip:"Relax MidPoints"
	Checkbox chkConformToStartEndEdges "Conform To Shape" checked:false tooltip:"Conform to the start and end edges of the selection"
	Checkbox chkFlipNormals  "Flip Normals" checked:false tooltip:"Flip Normals on Resulting Mesh"
	Checkbox chkModifyEdgeSelection  "Match Verts On Selected Object" checked:false tooltip:"Match the bridge verts on the selected object"  
	Spinner spnEdgeMatchPrecision "EdgeMatch Precision:" range:[0.001,0.1,0.001] type:#float align:#left tooltip:"Precision value used to match selected edges, (Default = 0.001)"
	Checkbox chkUseMinimumEdgesToMatch "Use Minimum Edges To Match" checked:true tooltip:"Use Minimum Edges To Match"
	on btnBridge pressed do
	(
		gRsSmoothBridge.BridgeEdges Selection[1] ExtrusionUnitScale:spnExtrusionUnitScale.value \
																		Segments:spnSegments.value \
																		relaxMidPointsValue:spnRelaxMidPoints.value \
																		showAllEdges:false \
																		edgeMatchPrecision:spnEdgeMatchPrecision.value \
																		useMinimumEdgesToMatch:chkUseMinimumEdgesToMatch.checked \
																		conformToStartEndEdges:chkConformToStartEndEdges.checked \
																		flipNormals:chkFlipNormals.checked \
																		modifyEdgeSelection:chkModifyEdgeSelection.checked
	)
)