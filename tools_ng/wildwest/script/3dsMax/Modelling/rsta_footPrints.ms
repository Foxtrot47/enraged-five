filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
rsta_loadCommonFunction #("FN_RSTA_userSettings")

if (FootPrints != undefined) do FootPrints.dispose()
struct FootPrints
(
	---------------------------------------------------------------------------------------
	-- LOCALS 
	---------------------------------------------------------------------------------------
	MainWindow, 
	vm,
	
	Settings = rsta_userSettings app:"FootPrints",	
	settings_array = #("Vis_Tab_FootPrints", "Vis_Tab_Projection", "StrideLength.Min", "StrideLength.Max", "StrideWidth.Min", "StrideWidth.Max", \
					"PrintWidth.Min", "PrintWidth.Max", "PrintLength.Min", "PrintLength.Max","SplinePos.Min", "SplinePos.Max", "Random_PrintZ.Min",\
					"Random_PrintZ.Max", "FeetCountIdx", "BackFootOffset", "OffsetFromGround", "PrintDimentions.Min", \
					"PrintDimentions.Max","PrintTaper.Min","PrintTaper.Max"),
	
	SourceSpline,
	SplineLength,
	TargetGround, 
	
	final_mesh,
	
	---------------------------------------------------------------------------------------
	-- FUNCTIONS
	---------------------------------------------------------------------------------------
	fn endFn = MainWindow.hasError = false,
	---------------------------------------------------------------------------------------
	fn SetSpline =
	(
		if (selection.count != 0) AND (classof selection[1] == line) then 
		(
			SourceSpline = selection[1]
			vm.IsPathSet = true
			vm.PathBtnString = "Path : \"" + SourceSpline.name + "\""
			
		)
		else messageBox "Please Select A Spline."		
		endFn()		
	),
	---------------------------------------------------------------------------------------
	fn setGround = 
	(
		if (selection.count != 0) AND (superclassof selection[1] == GeometryClass) then 
		(
			TargetGround = selection[1]
			vm.IsGroundSet = true
			vm.GroundBtnString = "Path : \"" + TargetGround.name + "\""			
		)
		else messageBox "Please Select A Mesh."		
		endFn()	
	),
	---------------------------------------------------------------------------------------
	fn getPointAndDirection fraction =
	(	
		return datapair pos:(interpCurve3D sourceSpline 1 fraction) dir:(tangentCurve3D sourceSpline 1 fraction)
	),
	---------------------------------------------------------------------------------------
	fn makePrint posAndDir isLeft back:false =
	(
		local fP_width = random vm.printWidth.Min vm.printWidth.Max
		local fP_length = random vm.printLength.Min vm.printLength.Max
		local footPrint = Plane length:fP_length width:fP_width pos:posAndDir.pos lengthsegs:vm.PrintDivisions.Min widthsegs:vm.PrintDivisions.Max
		
		-- TAPER 
		local taperMod = Taper ()
		addModifier footPrint taperMod
		taperMod.amount = random vm.PrintTaper.Min vm.PrintTaper.max
		taperMod.primaryaxis = 1
		
		-- FLIP 
		if (isLeft) do 
		(
			local mirrorMod = mirror ()
			addModifier footPrint mirrorMod
			mirrorMod.mirror_axis = 0
			mirrorMod.offset = 0
			mirrorMod.copy = false
		)
		
		footPrint.dir = posAndDir.dir
		footPrint.wirecolor = color 255 0 0
		if (back) do footPrint.wirecolor = color 0 255 0
					
		-- PUT BACK FLAT
		footPrint.rotation.x_rotation -= 90		

		-- RANDOMISE ROTATION IN Z
		footPrint.rotation.z_rotation += random (vm.Random_PrintZ.Min) (vm.Random_PrintZ.Max)
		
		local footOffset = if (isLeft) then [(random vm.StrideWidth.Min vm.StrideWidth.Max)/2,0,0] else [-((random vm.StrideWidth.Min vm.StrideWidth.Max)/2),0,0]
		footPrint.transform = preTranslate footPrint.transform footOffset
		
		convertToPoly footPrint
		
		-- APPEND TO MESH 
		if (final_mesh == undefined) then final_mesh = footPrint
		else (RsMeshPolyOp final_mesh).attach final_mesh footPrint	
	),
	---------------------------------------------------------------------------------------
	fn makeSetOFPrint pathFraction isLeft = 
	(
		local currentPoint_A = getPointAndDirection pathFraction
		makePrint currentPoint_A isLeft	
		
		-- ADD 2ND PAIR, FOR ANIMALS
		if (vm.FeetCountIdx == 1) do
		(			
			local backPathFraction =  pathFraction - (vm.BackFootOffset/splineLength)
			if (backPathFraction > 0) OR (backPathFraction < 1) do
			(
				local currentPoint_B = getPointAndDirection backPathFraction
				makePrint currentPoint_B (not isLeft) back:true
			)				
		)
	),
	---------------------------------------------------------------------------------------
	fn MakeFootPrint = 
	(
		max create mode
		final_mesh = undefined
		SplineLength = (getSegLengths SourceSpline 1)[(getSegLengths SourceSpline 1).count]
		local currentpathFraction = vm.SplinePos.Min/100	
		local SplineEnd  = vm.SplinePos.Max/100
		local isLeftFoot = true
		
		while (currentpathFraction < SplineEnd) do 
		(			
			makeSetOFPrint currentpathFraction isLeftFoot	
			-- WORK OUT THE NEXT STRIDE LENGTH
	
			local nextStride = random vm.StrideLength.Min vm.StrideLength.Max
			currentpathFraction += nextStride/splineLength				
			isLeftFoot = not(isLeftFoot)
		)
		endFn()				
	),
	---------------------------------------------------------------------------------------
	fn do_projectMesh obj = 
	(
		local tempMesh = snapshot TargetGround
		for idx = 1 to obj.numVerts do 
		(		
			local vertPos = (rsmeshpolyop obj).getVert obj idx			
			local vect = [0,0,-1]
			local hitdata = intersectRayEx tempMesh (ray (vertPos - (vect * 1000)) vect)

			if (hitdata != undefined) do 
			(	
				local vertPos = hitdata[1].pos					
				vertPos = vertPos + (vect * -vm.OffsetFromGround)				
				(rsmeshpolyop obj).setVert obj idx vertPos
			)					
		)
		delete tempMesh
	),
	---------------------------------------------------------------------------------------
	fn do_projectLine obj = 
	(
		local tempMesh = snapshot TargetGround
		for s = 1 to  (numSplines obj) do
		(
			for k = 1 to (numKnots obj s) do
			(
				local knt_pos = getKnotPoint obj s k	
				
				local vect = [0,0,-1]
				local hitdata = intersectRayEx tempMesh  (ray (knt_pos - (vect * 1000)) vect)
				
				if (hitdata != undefined) do 
				(											
					knt_pos = hitdata[1].pos			
					knt_pos + (hitdata[1].dir * -vm.OffsetFromGround)									
					setKnotPoint obj s k knt_pos
				)
			)
		)						
		updateshape obj	
		delete tempMesh
	),
	---------------------------------------------------------------------------------------
	fn ProjectToGround = 
	(
		for obj in (getCurrentSelection()) where (obj != TargetGround) do 
		(
			case (classof obj) of 
			(
				line : do_projectLine obj 
				Editable_Poly : do_projectMesh obj				
				Editable_Mesh : do_projectMesh obj
			)
		)
		endFn()	
	),
	---------------------------------------------------------------------------------------
	-- STRUCT FUNCTIONS 
	---------------------------------------------------------------------------------------
	fn dispose = 
	(
		if (MainWindow != undefined) do 
		(
			-- SETTINGS 
			for attr in settings_array do settings.getDnProp attr vm attr 
			
			-- WINDOW POS 
			settings.wpf_windowPos mainWindow #set
			
			-- CLOSE AND DISPOSE THE WINDOW
			MainWindow.Close()     
			MainWindow = undefined
		)
		execute ("FootPrints = undefined")
	),
	-------------------------------------------------------------------
	on create do 
	(
		-- LOAD ASSEMBLY
		--dotnet.loadAssembly  @"X:\wildwest\src\Library\RSG.TechArt\RSG.TechArt.FootPrints\RSG.TechArt.FootPrints\bin\Debug\RSG.TechArt.FootPrints.dll"
		dotNet.loadAssembly (RsConfigGetToolsDir() + @"techart\dcc\3dsMax\RSG.TechArt.FootPrints.dll")
		-- MAKE MAIN WINDOW INSTANCE 
		MainWindow = dotNetObject "RSG.TechArt.FootPrints.MainWindow"             
		dn_window.setup MainWindow 
		MainWindow.Tag = dotnetmxsvalue this
		-- PARENTS THE WINDOW UNDER MAX, STOPS IT FROM DROPPING BEHIND
		MainWindow.ChangeOwner (DotNetObject "System.IntPtr" (Windows.GetMAXHWND()))			
		
		-- SHOW THE WINDOW
		MainWindow.show()
		vm = MainWindow.dataContext
			
		-- WINDOW POS
		settings.wpf_windowPos mainWindow #get		
			
		MainWindow.readXML (RsConfigGetToolsDir() + @"wildwest\etc\config\general\FootprintsSettings.xml")			
			
		-- SETTINGS 
		for attr in settings_array do settings.setDnProp attr vm attr
	)
)
FootPrints = FootPrints()



