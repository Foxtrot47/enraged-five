FileIn (RsConfigGetWildWestDir() + "script/3dsMax/Modelling/RSTA_SmoothBridge.ms")

try(destroyDialog RSRollSmoothBridge) catch()
rollout RSRollSmoothBridge "Smooth Bridge" Width:240 
(
	--The RS Banner
	DotNetControl rsBannerPanel "Panel" width:RSRollSmoothBridge.width align:#center height:35 pos:[0,0]
	local bannerStruct = makeRsBanner dn_Panel:rsBannerPanel VersionNum:1.0 VersionName:"Smooth Monkey"
	--UI
	Button btnBridge "Bridge Selected Edges" width:(RSRollSmoothBridge.width-4) tooltip:"Select the Edges To Bridge"
	Spinner spnSegments "Segments:" range:[0,1000,10] type:#integer  align:#left across:2 tooltip:"Number of Segmants"
	Spinner spnExtrusionUnitScale "Scale:" range:[-5.0,5.0,1.0] type:#float align:#right tooltip:"Scale of Extrusion"
	Spinner spnRelaxMidPoints "Relax:" range:[0,1,0] type:#float align:#left tooltip:"Relax MidPoints"
	Checkbox chkConformToStartEndEdges "Conform To Shape" checked:false tooltip:"Conform to the start and end edges of the selection"
	Checkbox chkFlipNormals  "Flip Normals" checked:false tooltip:"Flip Normals on Resulting Mesh"
	Checkbox chkModifyEdgeSelection  "Match Verts On Selected Object" checked:false tooltip:"Match the bridge verts on the selected object"  
	Spinner spnEdgeMatchPrecision "EdgeMatch Precision:" range:[0.001,0.1,0.001] type:#float align:#left tooltip:"Precision value used to match selected edges, (Default = 0.001)"
	Checkbox chkUseMinimumEdgesToMatch "Use Minimum Edges To Match" checked:true tooltip:"Use Minimum Edges To Match"
	on btnBridge pressed do
	(
		gRsSmoothBridge.BridgeEdges Selection[1] ExtrusionUnitScale:spnExtrusionUnitScale.value \
																		Segments:spnSegments.value \
																		relaxMidPointsValue:spnRelaxMidPoints.value \
																		showAllEdges:false \
																		edgeMatchPrecision:spnEdgeMatchPrecision.value \
																		useMinimumEdgesToMatch:chkUseMinimumEdgesToMatch.checked \
																		conformToStartEndEdges:chkConformToStartEndEdges.checked \
																		flipNormals:chkFlipNormals.checked \
																		modifyEdgeSelection:chkModifyEdgeSelection.checked
	)
	
	on RSRollSmoothBridge open do 
	(
		bannerStruct.Setup()
	)
)	
CreateDialog RSRollSmoothBridge