--
-- File:: SelFacesByAngle.ms
-- Description:: Select faces according to their normal angles.
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com>
-- Date:: 20/5/2004
--
-----------------------------------------------------------------------------
	
	fn getFaceAngle faceNormal = (
						
			floorVec = copy faceNormal
			
			isNegative = false
			
			if floorVec[3] < 0.0f then (
				isNegative = true
			)
			
			floorVec[3] = 0.0f
			floorVec = floorVec / (length floorVec)

			cosAngle = dot floorVec faceNormal					
			angle = acos cosAngle
			
			if isNegative == true then (
				angle = -angle
			)
			
			return angle
	)
	
	fn selectGreater angleSpin = (
	
		if selection.count < 1 then (
			return 0
		)
		
		if classof selection[1] != Editable_Mesh then (
			messagebox "The selected object has to be an editable mesh."
			return 0
		)
	
		select selection[1]
		subobjectLevel = 3
	
		selectlist = #()
		objmesh = selection[1]
		
		for i = 1 to objmesh.numfaces do (
			objface = getface objmesh i
			
			vertA = objmesh.verts[objface[1]].pos
			vertB = objmesh.verts[objface[2]].pos
			vertC = objmesh.verts[objface[3]].pos
			
			vecA = vertB - vertA
			vecA = vecA / (length vecA)
			vecB = vertB - vertC
			vecB = vecB / (length vecB)
						
			faceNormal = cross vecB vecA
			faceNormal = faceNormal / (length faceNormal)
			
			angle = getFaceAngle faceNormal
			
			if angle > angleSpin then (
				append selectlist i
			)
		)
		
		objmesh.selectedfaces = selectlist
	)
	
	fn selectLess angleSpin = (
	
		if selection.count < 1 then (
			return 0
		)
		
		if classof selection[1] != Editable_Mesh then (
			messagebox "The selected object has to be an editable mesh."
			return 0
		)
	
		select selection[1]
		subobjectLevel = 3
	
		selectlist = #()
		objmesh = selection[1]
		
		for i = 1 to objmesh.numfaces do (
			objface = getface objmesh i
			
			vertA = objmesh.verts[objface[1]].pos
			vertB = objmesh.verts[objface[2]].pos
			vertC = objmesh.verts[objface[3]].pos
			
			vecA = vertB - vertA
			vecA = vecA / (length vecA)
			vecB = vertB - vertC
			vecB = vecB / (length vecB)
						
			faceNormal = cross vecB vecA
			faceNormal = faceNormal / (length faceNormal)
			
			angle = getFaceAngle faceNormal
			
			if angle < angleSpin then (
				append selectlist i
			)
		)
		
		objmesh.selectedfaces = selectlist
	)