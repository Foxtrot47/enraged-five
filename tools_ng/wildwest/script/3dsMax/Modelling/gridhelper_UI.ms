--
-- File:: rockstar/helpers/gridhelper.ms
-- Description:: Grid Helper: Sneaky little floater to make it easier to move a grid around as you are working
--
-- Date:: 2/2/2005
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- 17/3/2010
-- by Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Updated to work with modelling toolkit
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsModelToolGridRoll "Grid Helper"
(
	local GridObject

	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	checkbutton chkActive "Start" align:#left across:2
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Modelling_Toolkit#Grid_Helper" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255) align:#right

	spinner spnLength "Length" range:[0,1000,(RsSettingsReadInteger "grid" "length" 50)] enabled:false across:2
	spinner spnWidth "Width" range:[0,1000,(RsSettingsReadInteger "grid" "width" 50)] enabled:false
	spinner spnSpacing "Spacing" range:[0,100,(RsSettingsReadInteger "grid" "spacing" 10)] enabled:false across:2
	spinner spnRotation "Z Rotation" range:[-360,360,(RsSettingsReadInteger "grid" "rotation" 0)] enabled:false
	button btnUpdate "Update" enabled:false

	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	-------------------------------------------------------------
	-- always use this method to get the grid
	-- just in case the user has deleted it
	--------------------------------------------------------------
	fn GetGrid = (
		if GridObject != undefined and GridObject.isdeleted == false then (
			return GridObject
		)
		
		GridObject = Grid()
		select GridObject
		max activate grid object
		
		return GridObject
	)

	--------------------------------------------------------------
	-- updates the grid with the current parameters
	--------------------------------------------------------------
	fn UpdateGrid = (
		GridObj = GetGrid()
		GridObj.rotation = (eulerangles 0 0 spnRotation.value) as Quat
		GridObj.length = spnLength.value
		GridObj.width = spnWidth.value
		GridObj.grid = spnSpacing.value
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	--------------------------------------------------------------
	-- on activate button changed
	--------------------------------------------------------------
	on chkActive changed state do (
		if state == on then (
			-- If we're starting to work with the grid, turn everything on and create the grid
			chkActive.text = "Stop"
			spnLength.enabled = true
			spnWidth.enabled = true
			spnSpacing.enabled = true
			spnRotation.enabled = true
			btnUpdate.enabled = true
			
			UpdateGrid()
		)
		else (
		-- If we're done working with the grid, turn everything off and destroy the grid
			chkActive.text = "Start"
			spnLength.enabled = false
			spnWidth.enabled = false
			spnSpacing.enabled = false
			spnRotation.enabled = false
			btnUpdate.enabled = false
			
			if GridObject != undefined and GridObject.isdeleted == false then (
				local Angles = GridObject .rotation as EulerAngles
			
				RsSettingWrite "grid" "length" GridObject.length
				RsSettingWrite "grid" "width" GridObject.width
				RsSettingWrite "grid" "spacing" GridObject.grid
				RsSettingWrite "grid" "rotation" Angles.z
			
				max activate home grid
			
				delete GridObject
			)
		)
	)

	--------------------------------------------------------------
	-- on update button pressed
	--------------------------------------------------------------
	on btnUpdate pressed do (
		UpdateGrid()
	)
	
	on RsModelToolGridRoll rolledUp down do 
	(
		RsSettingWrite "RsModelToolGridRoll" "rollup" (not down)
	)
)