/*
Simplygon Toolkit
Author: Jason Hayes <jason.hayes@rockstarsandiego.com>
*/

filein ( RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms" )

filein ( RsConfigGetWildwestDir() + "script/3dsmax/simplygon/simplygon.ms" )

filein ( RsConfigGetWildwestDir() + "script/3dsmax/simplygon/simplygon_collision_ui.ms" )
filein ( RsConfigGetWildwestDir() + "script/3dsmax/simplygon/simplygon_vertex_colors_ui.ms" )
filein ( RsConfigGetWildwestDir() + "script/3dsmax/simplygon/simplygon_lod_ui.ms" )
filein ( RsConfigGetWildwestDir() + "script/3dsmax/simplygon/simplygon_utilities_ui.ms" )


global RsSimplygonToolkitRollout

try ( destroyDialog RsSimplygonToolkitRollout ) catch ()


rollout RsSimplygonToolkitRollout "Rockstar - Simplygon Toolkit"
(
	-- Used to create a new tab page.
	struct TabPage (
		name		= undefined,
		toolRollout	= undefined
	)
	
	-- Store all of the registered tab pages.
	local tabPages = #()
	
	-- Tip cycling..
	
	-- TODO: should query map channel.
	local tipMessages = #( "Vertex color channel 8 is reserved for Simplygon.",
						   "Vertex colors in channel 8 are used for weighting Simplygon decimation.",
						   "Contact a Technical Artist to get new Simplygon settings added.",
						   "Red vertex colors in channel 8 will not be removed by Simplygon.",
						   "Green vertex colors in channel 8 will be removed last by Simplygon.",
						   "Black vertex colors in channel 8 will be removed first by Simplygon.",
						   "This toolkit will sync latest Simplygon settings files when opened." )
						   
	local currentTipIdx = random 1 tipMessages.count
	local tipMessage = tipMessages[ currentTipIdx ]
	local tipInterval = 20
	
	timer tipTimer interval:( tipInterval * 1000 ) active:true
						   
	dotNetControl rsBannerPanel "Panel" pos:[ 0, 0 ] height:32 width:RsSimplygonToolkitRollout.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Simplygon Toolkit" filename:( getThisScriptFilename() )
						   
	group "General"
	(
		dropDownList ddlSimplygonSettings "Simplygon Settings"
		checkbox cbLockOpenEdges "Lock Open Edges" across:2 checked:true tooltip:"Any vertices on open edges will not get removed by Simplygon."
		checkbox cbLockPaintedVertices "Lock Painted Red Vertices" checked:true tooltip:"Any vertices painted red in the Simplygon vertex channel will not get removed by Simplygon."
		checkbox cbUseCustomLodSettings "Use Custom LOD Settings" align:#left
	)
	
	button btnRunSimplygon "Run Simplygon on Selection" height:32 width:390
	
	dotNetControl tabStrip "System.Windows.Forms.TabControl" width:( RsSimplygonToolkitRollout.width - 10 ) height:25 align:#left offset:[ -9, 0 ]
	subRollout theSubRollout width:( RsSimplygonToolkitRollout.width - 10 ) offset:[ -8, -4 ] height:( RsSimplygonToolkitRollout.height - 260 )
	
	group "Tip"
	(
		label lblTip tipMessage align:#left
	)
	
	-- Functions
	
	fn registerTabPages = (
		tabPages = #()
		
		local lodTab = TabPage name:"Custom LOD" toolRollout:RsSimplygonLodSettingsRollout
		local vertexColorsTab = TabPage name:"Vertex Colors" toolRollout:RsSimplygonVertexColorsRollout
		local collisionTab = TabPage name:"Collision" toolRollout:RsSimplygonCollisionRollout
		local utilitiesTab = TabPage name:"Utilities" toolRollout:RsSimplygonUtilitiesRollout
		
		append tabPages lodTab
		append tabPages vertexColorsTab
		append tabPages collisionTab
		append tabPages utilitiesTab
		
		for tab in tabPages do (
			tabStrip.tabPages.add tab.name
		)
	)
	
	fn setTabPage tabIdx = (
		for roll in theSubRollout.rollouts do (
			removeSubRollout theSubRollout roll
		)
		
		local tabPage = tabPages[ tabIdx ]
		
		if tabPage != undefined then (
			addSubRollout theSubRollout tabPage.toolRollout rolledUp:false border:false
		)
	)
	
	fn populateSimplygonSettings = (
		local simplygonFilenames = getFiles ( RsConfigGetToolsDir() + "etc/config/simplygon/*.ini" )
		local simplygonSettings = #()
		
		for simplygonFilename in simplygonFilenames do (
			append simplygonSettings ( getFilenameFile simplygonFilename )
		)
		
		sort simplygonSettings
		ddlSimplygonSettings.items = simplygonSettings
		
		if simplygonSettings.count > 0 then (
			ddlSimplygonSettings.selection = 1
		)
	)
	
	fn getSelectedSimplygonSettings = (
		local result = undefined
		local simplygonSettingsName = ddlSimplygonSettings.selected
		
		result = RsConfigGetToolsDir() + "etc/config/simplygon/" + simplygonSettingsName + ".ini"
		
		if not ( doesFileExist result ) then (
			messageBox ( "Could not find the Simplygon settings file! Was expecting it here (" + result + ")." ) title:"Rockstar"
			result = undefined
		)
		
		result
	)
	
	fn getLatestSimplygonFiles = (
		files = #( ( RsConfigGetToolsDir() + "etc/config/simplygon/..." ), 
					( RsConfigGetToolsDir() + "dcc/current/max2012/ui/usericons/rs_simplygonToolkit_a.bmp" ),
					( RsConfigGetToolsDir() + "dcc/current/max2012/ui/usericons/rs_simplygonToolkit_i.bmp" ) )
					
		gRsPerforce.sync( files )
	)
	
	
	-- Events
	
	on tipTimer tick do (
		local nextTipIdx = random 1 tipMessages.count
		
		if currentTipIdx == nextTipIdx then (
			if nextTipIdx == tipMessages.count then (
				nextTipIdx = 1
			) else (
				nextTipIdx += 1
			)
		)
		
		lblTip.text = tipMessages[ nextTipIdx ]
		currentTipIdx = nextTipIdx
	)
	
	on tabStrip Click do
	(
		local tabIdx = tabStrip.SelectedIndex + 1
		setTabPage tabIdx
	)
	
	on btnRunSimplygon pressed do (
		local objs = selection as array
		
		if objs.count > 0 then (
			local simplygonSettings = getSelectedSimplygonSettings()
			
			if simplygonSettings != undefined then (
				local oldSelection = objs
				clearSelection()
				
				for obj in objs do (
					select obj
					
					-- Save out the current custom lod settings to disk, and then tell Simplygon to use those instead.
					if cbUseCustomLodSettings.checked then (
						local tempCustomLodSettings = ( RsConfigGetToolsDir() + "etc/config/simplygon/tempCustomLodSettings.ini" )
						gRsSimplygonCore.settings.save tempCustomLodSettings
						simplygonSettings = tempCustomLodSettings
					)
					
					gRsSimplygonCore.loadSettings simplygonSettings
					
					local lockedVertices = #()
					
					if cbLockOpenEdges.checked == true then (
						local vertices = gRsSimplygonUtils.getOpenEdges obj
						
						if vertices != undefined then (
							join lockedVertices vertices
						)
					)
					
					if cbLockPaintedVertices.checked == true then (
						local vertices = gRsSimplygonUtils.getLockedVertsByVertColor obj
						
						if vertices != undefined then (
							join lockedVertices vertices
						)
					)
					
					if lockedVertices.count > 0 then (
						gRsSimplygonCore.setLockSelectedVertices true
						
						if ( classof obj ) == Editable_Mesh then (
							convertTo obj Editable_Poly
						)
						
						polyop.setVertSelection obj lockedVertices
						
					) else (
						gRsSimplygonCore.setLockSelectedVertices false
					)
					
					local processedObjs = gRsSimplygonCore.process()
				)
				
				select oldSelection
			)
		) else (
			messageBox "You must select at least one object to process!" title:"Rockstar"
		)
	)
	
	on RsSimplygonToolkitRollout open do (
		getLatestSimplygonFiles()
		banner.setup()
		registerTabPages()
		populateSimplygonSettings()
		setTabPage 1
	)
)


if ( gRsSimplygonCore.isSdkInstalled() ) then (
	createDialog RsSimplygonToolkitRollout width:400 height:700
	
) else (
	local pluginPath = ( RsConfigGetPluginDir() ) + ( gRsSimplygonCore.getPluginFilename() )
			
	if not ( doesFileExist pluginPath ) then (
		messageBox ( "Could not find the Simplygon plugin named:\n\n\t" + ( gRsSimplygonCore.getPluginFilename() ) + "\n\nWas expecting it here:\n\n\t" + ( RsConfigGetPluginDir() ) + "\n\nCannot open the toolkit until this is fixed." ) title:"Rockstar"
	) else (
		messageBox "The Simplygon SDK is not installed!  It must be installed before continuing.\n\nPlease contact a Technical Artist." title:"Rockstar"
	)
)
