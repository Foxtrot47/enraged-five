/*
Simplygon Toolkit - LOD
Author: Jason Hayes <jason.hayes@rockstarsandiego.com>
*/

filein ( RsConfigGetWildwestDir() + "script/3dsmax/simplygon/simplygon.ms" )

global RsSimplygonToolkitRollout

rollout RsSimplygonLodSettingsRollout "Custom LOD Settings"
(
	local previousLodCount = 1
	local lodTabPages = #()
	
	button btnSaveCustomSettings "Save Custom Settings" width:172 across:2
	button btnLoadCustomSettings "Load Custom Settings" width:172 
	
	dropDownList ddlLodSwitchQuality "LOD Switch Quality" items:#( "High", "Normal", "Low", "Poor" ) width:100
	
	spinner spNumLods "Number of LODs:" type:#integer range:[ 1, 10, 1 ] width:70 align:#left across:2
	checkbox cbCascadedLodChain "Cascaded LOD Chain"
	
	dotNetControl tabStrip "System.Windows.Forms.TabControl" width:( RsSimplygonLodSettingsRollout.width - 10 ) height:25 align:#left offset:[ -9, 0 ]
	subRollout theSubRollout width:( RsSimplygonLodSettingsRollout.width - 10 ) offset:[ -8, -4 ] height:( RsSimplygonLodSettingsRollout.height - 1388 )
	
	
	fn buildCollectionRollout collections rolloutStr lodIdx = (
		/*
		Constructs all of the controls and event handlers for a rollout.
		*/
		
		for collection in collections do (
			local sectionName = collection.sectionName

			local sectionNamesRaw = filterString sectionName "[/]"
			local groupName = sectionNamesRaw[ sectionNamesRaw.count ]
			
			-- Start group
			format "group \"%\" (\n" collection.displayName to:rolloutStr
			
			local sectionPropNames = collection.getPropertyNames()
			sort sectionPropNames
			
			for sectionPropName in sectionPropNames do (
				local data = collection.getProperty sectionPropName
				
				if data.visible then (
				
					-- Create unique control name.
					local controlName = StringStream ""
					
					for i = 4 to sectionNamesRaw.count do (
						format "%" sectionNamesRaw[ i ] to:controlName 
					)
					format "%" sectionPropName to:controlName
					controlName = controlName  as string
					
					-- Create control and event handler.
					if ( classof data.value ) == BooleanClass then (
						
						-- Control
						format "checkbox cb% \"%\" align:#left checked:% tooltip:\"%\" \n" controlName data.displayName data.value data.tooltip to:rolloutStr
						
						-- Event Hander
						format "on cb% changed state do (\n" controlName to:rolloutStr
						format "\tgRsSimplygonCore.settings.setProperty % \"%\" \"%\" state \n" lodIdx sectionName sectionPropName to:rolloutStr
						format ")\n" to:rolloutStr
						
					) else if ( classof data.value ) == String and ( classof data.options ) == Array then (
						
						local selectionIdx = findItem data.options data.value
						
						-- Control
						format "dropDownList ddl% \"%:\" align:#left items:% width:120 selection:% tooltip:\"%\" \n" controlName data.displayName data.options selectionIdx data.tooltip to:rolloutStr
						
						-- Event Handler
						format "on ddl% selected val do ( \n" controlName to:rolloutStr
						format "\tlocal val = ddl%.selected \n" controlName to:rolloutStr
						format "\tgRsSimplygonCore.settings.setProperty % \"%\" \"%\" val \n" lodIdx sectionName sectionPropName to:rolloutStr
						format ")\n" to:rolloutStr
						
					) else if ( classof data.value ) == float or ( classof data.value ) == integer and data.options != undefined then (
						
						-- Control
						data.options.z = data.value
						format "spinner sp% \"%:\" align:#left type:#% width:80 range:% tooltip:\"%\" \n" controlName data.displayName ( classof data.value ) data.options data.tooltip to:rolloutStr
						
						-- Event Handler
						format "on sp% changed val do ( \n" controlName to:rolloutStr
						format "\tgRsSimplygonCore.settings.setProperty % \"%\" \"%\" val \n" lodIdx sectionName sectionPropName to:rolloutStr
						format ")\n" to:rolloutStr
					)
				)
			)
			
			-- End group
			format ")\n" to:rolloutStr
		)
	)
	
	fn buildCollectionRolloutOnOpenEventHandler collections rolloutStr lodIdx = (
		/*
		For use within the rollout open event handler, it sets all of the controls for the rollout to their current values.
		*/
		
		for collection in collections do (
			local sectionName = collection.sectionName
			local sectionPropNames = collection.getPropertyNames()
			sort sectionPropNames
			
			for sectionPropName in sectionPropNames do (
				local data = collection.getProperty sectionPropName
				
				if data.visible then (
					-- Create unique control name.
					local sectionNamesRaw = filterString sectionName "[/]"
					local controlName = StringStream ""
					
					for i = 4 to sectionNamesRaw.count do (
						format "%" sectionNamesRaw[ i ] to:controlName 
					)
					format "%" sectionPropName to:controlName
					controlName = controlName  as string
					
					-- Create control and event handler.
					if ( classof data.value ) == BooleanClass then (
						format "cb%.checked = ( gRsSimplygonCore.settings.getProperty % \"%\" \"%\" ).value \n" controlName lodIdx sectionName sectionPropName to:rolloutStr
						
					) else if ( classof data.value ) == String and ( classof data.options ) == Array then (
						format "local propData = gRsSimplygonCore.settings.getProperty % \"%\" \"%\" \n" lodIdx sectionName sectionPropName to:rolloutStr
						format "local selectionIdx = findItem propData.options propData.value \n" to:rolloutStr
						format "ddl%.selection = selectionIdx \n" controlName to:rolloutStr
						
					) else if ( classof data.value ) == float or ( classof data.value ) == integer and ( classof data.options ) == Point3 then (
						format "local propData = gRsSimplygonCore.settings.getProperty % \"%\" \"%\" \n" lodIdx sectionName sectionPropName to:rolloutStr
						format "sp%.value = propData.value \n" controlName to:rolloutStr
					)
				)
			)
		)
	)
	
	fn buildLodLevelRollouts = (
		/*
		Constructs rollouts for each lod level dynamically.
		*/
		
		lodTabPages = #()
		
		fn sortCollections a b = (
			local result = 0
			
			if a.order > b.order then (
				result = 1
			) else if a.order < b.order then (
				result = -1
			)
			
			result
		)
		
		local mainSubRo = RsSimplygonToolkitRollout.theSubRollout
		
		for roll in mainSubRo.rollouts do (
			if not roll == RsSimplygonLodSettingsRollout then (
				removeSubRollout mainSubRo roll
			)
		)
		
		local lodCount = gRsSimplygonCore.settings.getLodCount()
		previousLodCount = lodCount
		
		tabstrip.tabpages.Clear()
		
		for lodIdx = 1 to lodCount do (
			tabStrip.tabPages.Add ( "LOD " + lodIdx as string )
				
			local lodCollection = gRsSimplygonCore.settings.getLod lodIdx
			local enum = lodCollection.GetEnumerator()
			
			local collections = #()
			local advancedCollections = #()
			
			while ( enum.MoveNext() ) do (
				local collection = enum.Value.value
				
				if collection.advanced then (
					append advancedCollections collection
					
				) else (
					append collections collection
				)
			)
			
			qsort collections sortCollections
			qsort advancedCollections sortCollections
			
			
			-- Build general rollout
			local rolloutStr = StringStream ""
			format "rollout RsSimplygonLOD%Rollout \"LOD %\" (\n" lodIdx lodIdx to:rolloutStr
			
			buildCollectionRollout collections rolloutStr lodIdx
			
			-- Need to setup the open event handler so that the controls get the current settings.
			format "on RsSimplygonLOD%Rollout open do ( \n" lodIdx to:rolloutStr
			buildCollectionRolloutOnOpenEventHandler collections rolloutStr lodIdx
			format ")\n" to:rolloutStr
			
			-- End rollout
			format ")\n" to:rolloutStr
			
			
			-- Build advanced rollout
			local advancedRolloutStr = StringStream ""
			format "rollout RsSimplygonAdvancedLOD%Rollout \"Advanced\" (\n" lodIdx to:advancedRolloutStr
			
			buildCollectionRollout advancedCollections advancedRolloutStr lodIdx
			
			-- Need to setup the open event handler so that the controls get the current settings.
			format "on RsSimplygonAdvancedLOD%Rollout open do ( \n" lodIdx to:advancedRolloutStr
			buildCollectionRolloutOnOpenEventHandler advancedCollections advancedRolloutStr lodIdx
			format ")\n" to:advancedRolloutStr
			
			-- End rollout
			format ")\n" to:advancedRolloutStr
			
			
			struct LodRollouts
			(
				general = undefined,
				advanced = undefined
			)
			
			local lodRollout = LodRollouts general:( execute ( rolloutStr as string ) ) advanced:( execute ( advancedRolloutStr as string ) )

			append lodTabPages lodRollout

			addSubRollout theSubRollout lodRollout.general rolledUp:false border:false
			addSubRollout theSubRollout lodRollout.advanced rolledUp:true border:false
		)
	)
	
	fn setTabPage lodIdx = (
		for roll in theSubRollout.rollouts do (
			removeSubRollout theSubRollout roll
		)
		
		local lodRollouts = lodTabPages[ lodIdx ]
		
		if lodRollouts != undefined then (
			addSubRollout theSubRollout lodRollouts.general rolledUp:false border:false
			addSubRollout theSubRollout lodRollouts.advanced rolledUp:true border:false
		)
	)
	
	fn updateNumTabPages = (
		local val = spNumLods.value
		
		if val != previousLodCount then (
			local delta = val - previousLodCount
			
			if delta > 0 then (
				for i = 1 to delta do (
					gRsSimplygonCore.settings.addLodLevel()
				)
					
			) else if delta < 0 then (
				delta = abs delta
				
				for i = 1 to delta do (
					gRsSimplygonCore.settings.removeLodLevel()
				)
			)
			
			previousLodCount = val
			buildLodLevelRollouts()
			setTabPage val
		)
	)
	
	-- Event Handlers
	
	on btnLoadCustomSettings pressed do (
		local result = getOpenFilename filename:( RsConfigGetToolsDir() + "etc/config/simplygon/" ) types:"Simplygon Settings (*.ini)|*.ini"
		
		if result != undefined then (
			gRsSimplygonCore.settings.load result
			buildLodLevelRollouts()
			setTabPage 1
		)
	)
	
	on btnSaveCustomSettings pressed do (
		local result = getSaveFilename filename:( RsConfigGetToolsDir() + "etc/config/simplygon/" ) types:"Simplygon Settings (*.ini)|*.ini"
		
		if result != undefined then (
			gRsSimplygonCore.settings.save result
			RsSimplygonToolkitRollout.populateSimplygonSettings()
		)
	)
	
	on cbCascadedLodChain changed state do (
		gRsSimplygonCore.settings.cascadedLodChain = state
	)
	
	on ddlLodSwitchQuality selected item do (
		gRsSimplygonCore.settings.lodSwitchQuality = ddlLodSwitchQuality.selected
	)
	
	on tabStrip click do (		
		setTabPage ( tabStrip.selectedIndex + 1 )
	)
	
	on spNumLods changed val do (
		updateNumTabPages()
	)
	
	on RsSimplygonLodSettingsRollout open do (
		cbCascadedLodChain.checked = gRsSimplygonCore.settings.cascadedLodChain
		ddlLodSwitchQuality.selection = ( findItem ddlLodSwitchQuality.items gRsSimplygonCore.settings.lodSwitchQuality )
		spNumLods.value = gRsSimplygonCore.settings.getLodCount()
		
		previousLodCount = gRsSimplygonCore.settings.getLodCount()
		
		buildLodLevelRollouts()
		
		setTabPage 1
	)
)