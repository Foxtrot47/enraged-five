/*
Simplygon Toolkit - Utilities
Author: Jason Hayes <jason.hayes@rockstarsandiego.com>
*/

filein ( RsConfigGetWildwestDir() + "script/3dsmax/simplygon/simplygon.ms" )

rollout RsSimplygonUtilitiesRollout "Utilities"
(
	group "Deselect Face Islands"
	(
		button btnDeselectIslands "Deselect" across:2 width:100 height:24 align:#left
		spinner spDeselectIslandsNumTriangles "Less Than or Equal to Num Triangles:" type:#integer range:[ 2, 99999, 6 ] width:140 offset:[ 120, 4 ]
	)
	
	group "Detach Face Selection"
	(
		button btnDetachFaceSelection "Detach" width:100 height:24 align:#left across:2 tooltip:"Detaches the current poly selection, renames it with the supplied suffix and changes the color."
		edittext etDetachFaceSelectionSuffix "Suffix:" text:"_w" width:100 offset:[ -56, 4 ]
	)
	
	
	-- Event Handlers
	
	on btnDetachFaceSelection pressed do (
		local objs = selection as array
		
		if objs.count == 1 then (
			gRsSimplygonUtils.detachAndRenameFaceSelection objs[ 1 ] suffix:etDetachFaceSelectionSuffix.text
		) else (
			messageBox "Please select only one object!" title:"Rockstar"
		)
	)
	
	on btnDeselectIslands pressed do (
		local objs = selection as array
		
		if objs.count == 1 then (
			gRsSimplygonUtils.deselectIslands objs[ 1 ] numTriangles:spDeselectIslandsNumTriangles.value
		) else (
			messageBox "Please select only one object!" title:"Rockstar"
		)
	)
)