/*
Simplygon Toolkit - Collision
Author: Jason Hayes <jason.hayes@rockstarsandiego.com>
*/

filein ( RsConfigGetWildwestDir() + "script/3dsmax/simplygon/simplygon.ms" )
filein "pipeline/util/collision_funcs.ms"

--global gRsSimplygonCore


rollout RsSimplygonCollisionRollout "Collision Tools"
(
	local btnWidth = 88
	local collMaterialNames = for collMat in gRsSimplygonCore.xmlSettings.getCollisionMaterials() collect collMat.name
	
	group "Collision Material Tools"
	(
		multiListBox rbCollisionPresets "Collision Materials" items:( sort collMaterialNames ) align:#left height:7
		
		checkbutton btnColorBlack "Black" width:btnWidth across:4 highlightColor:( color 0 0 0 ) border:false
		checkbutton btnColorLightBlue "Light Blue" width:btnWidth highlightColor:( Color 0 255 255 ) border:false
		checkbutton btnColorBlue "Blue" width:btnWidth highLightColor:( Color 0 0 255 ) border:false
		checkbutton btnColorGreen "Green" width:btnWidth highLightColor:( Color 0 255 0 ) border:false
		
		button btnSelectVertices "Select Vertices" height:32 width:360 align:#right offset:[ 4, 0 ] tooltip:"Selects vertices on the selected collision object based on the collision material types and selected vertex colors."
		button btnColorizeMapChannel "Color Simplygon Map Channel" height:32 width:360 align:#right offset:[ 4, 0 ]
		button btnPushVertices "Push Vertices" height:32 width:360 align:#right offset:[ 4, 0 ] tooltip:"Pushes vertices by a certain amount."
		checkbox cbIgnoreRedVertsOnPush "Ignore Red Vertices on Push" align:#left
	)
	
	
	-- Functions
	
	fn getCollisionMaterialData = (
		local data = #()
		
		struct CollMatData
		(
			type			= undefined,
			pushAmount		= undefined,
			vertexColors	= undefined,
			verticesToPush	= #()
		)
		
		-- Get selected presets.
		for i in rbCollisionPresets.selection do (
			local presetName = rbCollisionPresets.items[ i ]
			
			local vertColors = #()
					
			if btnColorBlack.checked do append vertColors [ 0, 0, 0 ]
			if btnColorLightBlue.checked do append vertColors [ 0, 1, 1 ]
			if btnColorBlue.checked do append vertColors [ 0, 0, 1 ]
			if btnColorGreen.checked do append vertColors [ 0, 1, 0 ]
				
			local matData = CollMatData type:presetName vertexColors:vertColors
			
			for collMat in gRsSimplygonCore.xmlSettings.getCollisionMaterials() do (
				if collMat.name == presetName then (
					matData.type = collMat.name
					matData.pushAmount = collMat.pushAmount
				)
			)
			
			append data matData
		)
		
		data
	)
	
	
	-- Events
	
	on btnSelectVertices pressed do (
		local objs = selection as array
		
		if objs.count != 2 then (
			messageBox "You must have only two objects selected!  Please select a collision mesh and its high-definition drawable mesh." title:"Rockstar"
			
		) else (
			local objA = objs[ 1 ]
			local objB = objs[ 2 ]
			
			if ( classof objA ) != Col_Mesh and ( classof objB ) != Col_Mesh then (
				messageBox "You must have a collision mesh and its high-definition drawable mesh selected!" title:"Rockstar"
				
			) else (
				local hdMesh = objs[ 1 ]
				local collMesh = objs[ 2 ]
				
				if ( classof objs[ 1 ] ) == Col_Mesh then (
					collMesh = objs[ 1 ]
					hdMesh = objs[ 2 ]
				)
				
				if ( classof hdMesh ) != Editable_Mesh and ( classof hdMesh != Editable_Poly ) then (
					messageBox "You must have an Editable_Mesh or Editable_Poly and a Collision Mesh selected!" title:"Rockstar"
					
				) else (
					local matData = getCollisionMaterialData()
					gRsSimplygonUtils.operateOnFilteredCollisionVertices hdMesh collMesh matData true false false
				)
			)
		)
	)
	
	on btnColorizeMapChannel pressed do (
		local objs = selection as array
		
		if objs.count != 2 then (
			messageBox "You must have only two objects selected!  Please select a collision mesh and its high-definition drawable mesh." title:"Rockstar"
			
		) else (
			local objA = objs[ 1 ]
			local objB = objs[ 2 ]
			
			if ( classof objA ) != Col_Mesh and ( classof objB ) != Col_Mesh then (
				messageBox "You must have a collision mesh and its high-definition drawable mesh selected!" title:"Rockstar"
				
			) else (
				local hdMesh = objs[ 1 ]
				local collMesh = objs[ 2 ]
				
				if ( classof objs[ 1 ] ) == Col_Mesh then (
					collMesh = objs[ 1 ]
					hdMesh = objs[ 2 ]
				)
				
				if ( classof hdMesh ) != Editable_Mesh and ( classof hdMesh != Editable_Poly ) then (
					messageBox "You must have an Editable_Mesh or Editable_Poly and a Collision Mesh selected!" title:"Rockstar"
					
				) else (
					local matData = getCollisionMaterialData()
					gRsSimplygonUtils.operateOnFilteredCollisionVertices hdMesh collMesh matData false false true
				)
			)
		)
	)
	
	on btnPushVertices pressed do (
		local colorSelected = false
		
		if btnColorBlack.checked or btnColorLightBlue.checked or btnColorBlue.checked or btnColorGreen.checked then (
			colorSelected = true
		)
			
		if not colorSelected then (
			messageBox "Please select at least one color to push!" title:"Rockstar"
			return false
		)
		
		local objs = selection as array
		
		if objs.count != 2 then (
			messageBox "You must have only two objects selected!  Please select a collision mesh and its high-definition drawable mesh." title:"Rockstar"
			
		) else (
			local objA = objs[ 1 ]
			local objB = objs[ 2 ]
			
			if ( classof objA ) != Col_Mesh and ( classof objB ) != Col_Mesh then (
				messageBox "You must have a collision mesh and its high-definition drawable mesh selected!" title:"Rockstar"
				
			) else (
				local hdMesh = objs[ 1 ]
				local collMesh = objs[ 2 ]
				
				if ( classof objs[ 1 ] ) == Col_Mesh then (
					collMesh = objs[ 1 ]
					hdMesh = objs[ 2 ]
				)
				
				if ( classof hdMesh ) != Editable_Mesh and ( classof hdMesh != Editable_Poly ) then (
					messageBox "You must have an Editable_Mesh or Editable_Poly and a Collision Mesh selected!" title:"Rockstar"
					
				) else (
					local matData = getCollisionMaterialData()
					gRsSimplygonUtils.operateOnFilteredCollisionVertices hdMesh collMesh matData false true false ignoreRedVertsOnPush:cbIgnoreRedVertsOnPush.checked
				)
			)
		)
	)
	
	on RsSimplygonCollisionRollout open do (
		rbCollisionPresets.selection = 1
	)
)