/*
Simplygon Toolkit - Vertex Colors
Author: Jason Hayes <jason.hayes@rockstarsandiego.com>
*/

filein ( RsConfigGetWildwestDir() + "script/3dsmax/simplygon/simplygon.ms" )

rollout RsSimplygonVertexColorsRollout "Vertex Color Tools"
(
	local updateMaterialsListCallback = undefined
	
	local paintBucketIcon		= #( "rs_simplygonToolkit_i.bmp", "rs_simplygonToolkit_a.bmp", 8, 1, 1, 2, 2, true )
	local selectLockedVertsIcon	= #( "rs_simplygonToolkit_i.bmp", "rs_simplygonToolkit_a.bmp", 8, 3, 3, 4, 4, true )
	local displayMapChannelIcon	= #( "rs_simplygonToolkit_i.bmp", "rs_simplygonToolkit_a.bmp", 8, 5, 5, 6, 6, true )
	local projectChannelIcon	= #( "rs_simplygonToolkit_i.bmp", "rs_simplygonToolkit_a.bmp", 8, 7, 7, 8, 8, true )
	
	local floodFillColors = #( "Black: First to go", "Green: Last to go", "Red: Not removed at all"  )
	
	button btnSelectPaintedLockedVerts "" images:selectLockedVertsIcon height:34 width:34 across:3 align:#left offset:[ -6, 0 ] border:false tooltip:"Select vertices that will be locked by Simplygon."
	button btnDisplayMapChannel "" images:displayMapChannelIcon height:34 width:34 offset:[ -120, 0 ] border:false tooltip:"Display the Simplygon vertex color channel, which uses map channel 8."
	button btnProjectChannels "" images:projectChannelIcon height:34 width:34 offset:[ -190, 0 ] border:false tooltip:"Project vertex colors from one map channel to another."
	
	group "Flood Fill"
	(
		button btnFloodFill "Fill" images:paintBucketIcon width:48 height:48 across:2 align:#left tooltip:"Flood all or selected vertices on a mesh in the Simplygon map channel."
		radiobuttons rbFloodFill "" labels:floodFillColors offset:[ -140, 2 ]
		
		radiobuttons cbFillType labels:#( "Use Vertex Selection", "Use Selected Material Id's" ) columns:1 align:#left across:2
		checkbox cbIgnoreRedVerts "Ignore Red Vertices on Fill"
		
		multilistbox lbMaterials "Material Id's" enabled:false height:6
	)
	
	-- Events
	
	on btnProjectChannels pressed do (
		filein ( RsConfigGetWildwestDir() + "script/3dsmax/General_tools/VertexChannelTools/VertexChannelProjector.ms" )
	)
	
	on cbFillType changed state do (
		if state == 2 then (
			lbMaterials.enabled = True
		) else (
			lbMaterials.enabled = False
		)
	)
	
	on btnFloodFill pressed do (
		local objs = selection as array
		
		if objs.count > 0 then (
			local vertColor = undefined
			
			case rbFloodFill.state of (
				1 : vertColor = [ 0, 0, 0 ]
				2 : vertColor = [ 0, 255, 0 ]
				3 : vertColor = [ 255, 0, 0 ]
			)
			
			if vertColor != undefined then (
				undo "Flood fill Simplygon vertex colors" on (
					local floodFillVertexSelection = false
					local floodFillMaterials = false
					local materialIdList = undefined
					
					case cbFillType.state of
					(
						1: floodFillVertexSelection = true
						
						2: (
							floodFillMaterials = true
							
							materialIdList = #()
							
							for itemId = 1 to lbMaterials.items.count do (
								if ( findItem lbMaterials.selection itemId ) != 0 then (
									append materialIdList ( lbMaterials.items[ itemId ] as integer )
								)
							)
						)
					)
					
					for obj in objs do (
						gRsSimplygonUtils.floodFillVertsUsingColor obj vertColor vertSelectionOnly:floodFillVertexSelection ignoreRedVerts:cbIgnoreRedVerts.checked materialIds:materialIdList
					)
				)
				
				redrawViews()
				
			) else (
				messageBox "An unknown error occurred while determining a vertex color to flood fill.  Please contact a Technical Artist." title:"Rockstar"
			)
			
		) else (
			messageBox "Please select at least one mesh object!" title:"Rockstar"
		)
	)
	
	on btnDisplayMapChannel pressed do (
		local objs = selection as array
		
		if objs.count > 0 then (
			for obj in objs do (
				gRsSimplygonUtils.displayMapChannel obj
			)
		) else (
			messageBox "You must select at least one mesh object!" title:"Rockstar"
		)
	)
	
	on btnSelectPaintedLockedVerts pressed do (
		local objs = selection as array
		
		if objs.count == 1 then (
			local obj = objs[ 1 ]
			local verts = gRsSimplygonUtils.getLockedVertsByVertColor obj
			
			if ( classof obj ) == Editable_Mesh then (
				setVertSelection obj verts
				
			) else if ( classof obj ) == Editable_Poly then (
				polyop.setVertSelection obj verts
				
			) else if ( classof obj ) == Col_Mesh then (
				print ( "Cannot set vertex selection on the collision object (" + obj.name + ")!" )
			)
			
			subObjectLevel = 1
			redrawViews()

		) else (
			messageBox "You must select only one object!" title:"Rockstar"
		)
	)
	
	fn getMaterials obj &materials = (
		if obj != undefined then (
			if classof obj == Editable_Mesh then (
				local numFaces = meshop.getNumFaces obj
				
				for faceId = 1 to numFaces do (
					appendIfUnique materials ( ( getFaceMatId obj faceId ) as string )
				)
				
			) else if classof obj == Editable_Poly then (
				local numFaces = polyop.getNumFaces obj
				
				for faceId = 1 to numFaces do (
					appendIfUnique materials ( ( polyop.getFaceMatId obj faceId ) as string )
				)
			)
		)
	)
	
	fn updateMaterialsList ev nd = (
		lbMaterials.items = #()
		
		local objs = selection as array
		
		if objs.count > 0 then (
			local materials = #()
			
			for obj in objs do (
				getMaterials obj &materials
			)
			
			sort materials
			
			lbMaterials.items = materials
		)
	)
	
	on RsSimplygonVertexColorsRollout open do (
		updateMaterialsListCallback = NodeEventCallback selectionChanged:updateMaterialsList
		updateMaterialsList undefined undefined
	)
	
	on RsSimplygonVertexColorsRollout close do (
		updateMaterialsListCallback = undefined
	)
)