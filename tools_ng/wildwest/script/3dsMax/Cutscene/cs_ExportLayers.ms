fn printAllLayerContentInTxt txtFile =
(
	local printArr = #()
	for i = 0 to LayerManager.count - 1 do
	(
		layer = (LayerManager.getLayer i).layerAsRefTarg
		nodes = refs.dependentNodes layer
		if (nCnt = nodes.count) != 0 do
		(
			append printArr ("ENTITY SET START>> " + layer.name + " <<ENTITY SET END"+ "(" + nCnt as string + " objects) :")
			for n = 1 to nCnt do append printArr ("<<" + layer.name + ">>" + nodes[n].name)
		)
	)
	if printArr.count != 0 do ((dotnetclass "System.IO.File").WriteAllLines txtFile printArr ; free printArr)
)

maxFile = (getFilenameFile maxfilename)
setAssetRoot = (theProjectRoot+"art/animation/resources/sets/Max_Files/")
exportName = (setAssetRoot + maxFile + "_EntitySets.txt")
textFile = getSaveFileName fileName:exportName caption:"What would the text file to be called?" types:"txt File (*.TXT)|*.txt|All Files (*.*)|*.*|"

printAllLayerContentInTxt textFile