rootDir = (theProjectRoot+"art/animation/resources/sets/objectCategories/")
exists = doesFileExist rootDir
if exists != true then
(
	rootDir = (theProjectRoot + "art/dev/animation/resources/sets/objectCategories/")
)

setCsvPath = getOpenFileName fileName:rootDir caption:"Category CSV File" types:"CSV|*.csv"

if setCsvPath  != undefined then
(
	csvFile = openFile setCsvPath
	if (csvFile != undefined) then
		(
		skiptonextline csvFile
			(
			while not eof csvFile do
				(
					csvLine = readline csvFile
					stringArray = filterString csvLine ","
					if(stringArray.count == 3) then -- Check if groups wer found on that line
					(
						maxItem = stringArray[1]
						category = stringArray[2]
						cap = substring category 1 1 -- get the first letter of the sting
						cap = toUpper cap -- capitilse it
						capitalised = cap + subString category 2 category.count -- add it back to the rest of the string
						allObjects = objects as array
		
						matchedObjects = for obj in allObjects where (MatchPattern obj.name Pattern:("*"+maxItem+"*")== true) collect obj
						for obj in matchedObjects do
						(
							objectName = obj.name
							currentPrefix = ((filterString obj.name  "_")[1])
							newName = (capitalised + subString objectName (currentPrefix.count+1) objectName.count )
							obj.name = newName
						)
					)
				)
			)
		messageBox "Set Categories loaded from the .csv file. Check over the scene, some objects will likely need categorisation." title:"Categories configured"
		)
	)

else
	(
	messageBox "Open Failed" title:"Error!"
	)
free setCsvPath