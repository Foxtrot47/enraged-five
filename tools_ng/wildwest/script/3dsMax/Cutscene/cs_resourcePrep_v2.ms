-- filein "X:/wildwest/script/3dsMax/CutScene/cs_resourcePrep_v2.ms"

escapeEnable = true
-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

theProjectConfig = RsConfigGetProjBinConfigDir()

toolsRoot = RsConfigGetToolsRootDir()

filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_Perforce.ms")

filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_Rigging.ms")

filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_UI.ms")

	filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_Perforce.ms")
	filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/mrSkeleton_2_functions.ms")
	filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/expressionExport/mrExpression_SpringHack.ms")
	filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/expressionExport/mrExpression_CutPasteController.ms")
	filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/expressionExport/mrExpression_Export.ms")
-- 	filein (theWildwest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/expressionExport/rsta_removeRsExprSolver.ms")
	
	if rsMakeSafeSlashes((toLower(theProjectRoot))) == "x:/rdr3/" then
	(
		filein (theProjectRoot + "tools/dcc/current/max2015/scripts/pipeline/export/expressions.ms")
	)
	else
	(
-- 		filein (theProjectRoot + "tools/dcc/current/max2012/scripts/pipeline/export/expressions.ms")
	)

fbXAssetRoot = (theProjectRoot+"art/animation/resources/")

pedMetaFile = (theProjectRoot+"/assets/export/data/peds.pso.meta")
metaPedMetaFile = (theProjectRoot+"/assets/export/data/metapeds.pso.meta")
pedTypeList  = (theProjectRoot + "/tools/etc/config/characters/ped_type_list.xml")

skelSetFolder = (theProjectRoot + "assets/metadata/characters/skeleton/skeletonsets/")

filein (theWildWest + "script/3dsMax/Cutscene/cs_ResourcePrep_Functions_v2.ms")

rsta_loadCommonFunction #("FN_RSTA_userSettings")


CsResourcePrep_UIHeight = 400
CsResourcePrep_UIWidth = 200
verNo = 2.0
-- global udpData = undefined

--//////////////////////////////////////////////////////////////////////////////////////////////
--			UI
--//////////////////////////////////////////////////////////////////////////////////////////////
if CsResourcePrep_UI != undefined then destroyDialog CsResourcePrep_UI
	
rollout CsResourcePrep_UI ("Cs Resource Prep"+(verNo as string)) --this is the main rollout that contains all the tabs and sub rollouts.
(
	--//////////////////////////////////////////////////////////////////////////////
	-- 	VARIABLES
	--//////////////////////////////////////////////////////////////////////////////
-- 	local tabRollouts = #(::sub_Characters, ::sub_Props, ::sub_Vehicles, ::sub_Map)
	local tabRollouts = #(::sub_Characters, ::sub_Props, ::sub_Vehicles)
	
	--//////////////////////////////////////////////////////////////////////////////
	-- 	CONTROLS
	--//////////////////////////////////////////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:CsResourcePrep_UI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"MrSkeletonv2" filename:(getThisScriptFilename())
		
	dotNetControl dnTabs "system.windows.forms.tabControl" width:CsResourcePrep_UI.width height:25 offset:[-12, 0]
-- 	dotNetControl dnTabs "system.windows.forms.tabControl" width:CsResourcePrep_UI.width height:40 offset:[-12, 0]
	
	subRollout theSubRollout width:(CsResourcePrep_UI.width - 2) height:(CsResourcePrep_UI.Height - (dnTabs.height + rsBannerPanel.height))offset:[-12, 0]--pos:[-1, dnTabs.pos.y + 50]
		
	--//////////////////////////////////////////////////////////////////////////////
	-- 	FUNCTIONS
	--//////////////////////////////////////////////////////////////////////////////
	fn RSTA_getUiPosFile = 
	(
		--THIS HAS TO BE DECLARED FROM  WITHIN THIS SCRIPT 
		
		uiPosFile = ((getThisScriptFilename()))
	-- 	format (uiPosFile+"\n")

		filePath = filterString uiPosFile "."
			
		uiPosFile = (filePath[1]+"_UIPOSFILE.TXT")
			
	-- 	format ("final "+uiPosFile+"\n")
			
		return uiPosFile
	)		
		
		
	fn SetPage index = 
	(
		if index > tabRollouts.count do messageBox "invalid tab"
		
		for roll in theSubRollout.rollouts do
		(
			removeRollout roll
		)

		AddSubRollout theSubRollout tabRollouts[index] rolledup:false border:true
		
		if index == 1 do --ie markers
		(
-- 			AddSubRollout theSubRollout markerSubRolls[1] rolledup:true border:false
-- 			AddSubRollout theSubRollout markerSubRolls[2] rolledup:true border:false
		)

	)
	
	--//////////////////////////////////////////////////////////////////////////////
	-- 	EVENTS
	--//////////////////////////////////////////////////////////////////////////////
	on dnTabs Click do
	(
-- 		if stretchEnabled == false then
-- 		(
			local tabNum = dnTabs.SelectedIndex + 1
			local tabName = tabRollouts[tabNum].name
			
			SetPage tabNum
-- 		)
-- 		else
-- 		(
-- 			messagebox "Please deactivate Stretchy Mode before switching tabs" beep:true
-- 		)
	)
	
	on CsResourcePrep_UI open do
	(
		banner.setup()
		
		dnTabs.multiline = true --this lets us have multiple lines of tabs
		
		for tab in tabRollouts do dnTabs.tabPages.Add tab.title
		SetPage 1
		
		--now we need to find a previous saved position and apply that if found
		uiPosFile = RSTA_getUiPosFile() --get the dialogPos file
		dialogPos = RSTA_DialogPosLoad uiPosFile --get the position form the dialog pos file
		format ("Attempting to set dialog pos to "+(dialogPos as string)+"\n")
		if dialogPos != undefined do
		(
			SetDialogPos CsResourcePrep_UI dialogPos
		)
	)
	
	on CsResourcePrep_UI close do 
	(
		gc light:true		
		if boneDebugTextBoxRollout_UI != undefined then destroyDialog boneDebugTextBoxRollout_UI
	)	
	
	on CsResourcePrep_UI moved position do 
	(
		uiPosFile = RSTA_getUiPosFile() --get the dialogPos file
		RSTA_DialogPosRec CsResourcePrep_UI uiPosFile
		
		if boneDebugTextBoxRollout_UI != undefined do 
		(
			boneDebugPositioner CsResourcePrep_UI boneDebugTextBoxRollout_UI
		)
	)
)

rollout sub_Characters "Characters"
(
	--//////////////////////////////////////////////////////////////////////////////
	-- 	VARIABLES
	--//////////////////////////////////////////////////////////////////////////////	
	
	local chk_ExportCsVersionStateVal = false
	
	local dropDownItems = #("Male", "Female", "Animal", "None")
	
	checkBox chk_ExportCsVersion "IG Version" pos:[10,10] tooltiup:"Export a none scaled up IG ped along with the scaled CS Ped." checked:chk_ExportCsVersionStateVal 
	checkBox chk_StripMaterials "Strip Mats" pos:[100,(chk_ExportCsVersion.pos[2])] tooltip:"Strip materials from fbx." checked:false
	checkBox chk_ForceSync "Force Sync of source" pos:[10,30] tooltip:"Force sync of latest max file." checked:true
	
	checkBox chk_OldStyle "Old Style Expressions" pos:[10,50] tooltip:"Export as old style rsnxml formatted expressions." checked:false
	
	dropdownlist dd_pedSex items:dropDownItems width:170 pos:[(chk_OldStyle.pos[1]), (chk_OldStyle.pos[2] + 20)]
	
	button btn_PrepareAndExportCharacters "Prepare & Export" width:180 height:30 tooltip:"Configure scene for motionbuidler and export to fbx."

	button btn_PrepareCharacter "Prepare Character" width:180 height:30 tooltip:"Prepare Character."
	
	button btn_PrepareExpressions "Prepare Expressions" width:180 height:30 tooltip:"Prepare Expressions"

	button btn_NukeMaterials "Nuke Materials" width:180 height:30 tooltip:"Nuke Materials"
	
	button btn_RestoreMaterials "Restore Materials" width:180 height:30 tooltip:"Nuke Materials"
	
	button btn_removeFacial "TEMP Remove Facial Expressions" width:180 height:30 tooltip:"TEMPORARY! Remove all expressions on facial nodes. Useful when needing to pass to max 2012."

	--//////////////////////////////////////////////////////////////////////////////
	-- 	FUNCTIONS
	--//////////////////////////////////////////////////////////////////////////////
	
	fn enableButtons enableState = 
	(
		if enableState == 1 then
		(
		)
		else
		(	
		)		
	)
	
	--//////////////////////////////////////////////////////////////////////////////
	-- 	EVENTS
	--//////////////////////////////////////////////////////////////////////////////	
	
	
	on sub_Characters open do
	(		
		currentFileName = maxFilePath + maxFileName

		if (substring maxFileName 2 3) == "_M_" then
		(
			print "This scene is a male"
			
			dd_pedSex.selection = 1
		)
		else
		(
			if (substring maxFileName 2 3) == "_F_" then
			(
				print "This scene is a female"
				dd_pedSex.selection = 2
			)
			else
			(
				print "Could not define sex of scene so opening gui"
				dd_pedSex.selection = 4
			)
		)
	)

	
	on chk_ExportCsVersion changed thestate do 
	(
		chk_ExportCsVersionStateVal = theState
	)
	
	on btn_PrepareAndExportCharacters pressed do 
	(	
		udpData = undefined
			--for safeties sake remove the initial selection set so we can make a new one

			charSex = dropDownItems[dd_pedSex.selection]
		
			local tmpPedDummyNodeName = undefined
		
			format ("charSex: "+(charSex as string)+"\n")
		
			if dropDownItems[dd_pedSex.selection] != "None" then
			(
				CS_fbxFileName = undefined
				
					selCount = getNumNamedSelSets()
					SelName = "*expressions"
					currItems = #()
					
					for i = selCount to 1 by -1 do 
					(
						--format ("SelName: "+selName+"\n")
						setName = (getNamedSelSetName i)
						--format ("setName: "+(setName as string)+"\n")
						if (toLower(selName)) == (toLower(setName)) then
						(
							--format ("removing "+setName+"\n")				
							--now delete that selection set
							deleteItem selectionSets setName
						)
					)	
					
					local thisMaxPath = (maxfilepath + maxfilename)
					
					if (substring (toLower(maxfilename)) 1 3) == "ig_" then
					(
						chk_ExportCsVersionStateVal = false
						messagebox ("You are exporting from a IG ped so we can skip secondary export scaling pass.")
						format ("You are exporting from a IG ped so we can skip secondary export scaling pass.\n")
					)
					
					local isValidFile = true
					
					if chk_ForceSync.state == true do 
					(				
						try (
							local fileRev = RSTA_getToolRevision thisMaxPath undefined
					
							if fileRev[1] != fileRev[2] do 
							(
								format ("Updating from revision "+(fileRev[1] as string)+" to "+(fileRev[2] as string)+" of "+thisMaxPath+"\n")
				 				--format ("Getting latest version of "+thisMaxPath+"\n")
								gRsPerforce.sync thisMaxPath silent:true 
									
								loadMaxFile thisMaxPath useFileUnit:true quiet:true
								format ("Finished updating "+thisMaxPath+"\n")
							)
						)catch(
							format("Failed to sync "+(thisMaxPath as string)+" from perforce.\n")
							isValidFile = false
							)
					)
					
					--***THESE FUCKING LOD CAMERA ARE A MASSIVE PAIN IN THE FUCKING ARSE
					camshit = #()
					for obj in objects do 
					(
						if classof obj == Targetcamera do 
						(						
							format ("Deleting LOD_Camera...\n")
							appendifUnique camshit obj
						)

						if classof obj == Targetobject do
						(
							format ("Deleting Target001...\n")
							appendifUnique camshit obj
						)
					)						
					
					delete camshit
-- 					--*** FUCKING THINGS.					
					
					rsta_removeRsExprControllers()
					rsta_removeSceneRootObjects()
					
					local igStart = undefined
					local igEnd = undefined
					local csStart = undefined
					local csEnd = undefined
					
					if isValidFile == true then
					(
						undoText = "csPrepareAndExportCharacter"		
						csStart = timestamp()
						
						continueExport = false
						
						udpData = undefined
						
						undo undoText on
						(	
							CS_fbxFileName = getSaveFileName fileName:fbXAssetRoot caption:"FBX File" types:"FBX File (*.FBX)|*.fbx|All Files (*.*)|*.*|"

							exportStyle = 1
-- 							udpData = undefined
							local structData = PrepAndExportCharacter CS_fbxFileName chk_StripMaterials.state pedMetaFile charSex chk_OldStyle.state exportStyle metaPedMetaFile udpData
							
							pedDummyNode = structData.pedDummyNode
							udpData = structData.udpData
							if pedDummyNode != undefined do
							(
								tmpPedDummyNodeName = pedDummyNode.name		
								continueExport = true
								format ("CS Ped Export process complete...\n\n\n")
							)
						)
						
						if continueExport == true then
						(
							thisDidUndoOk = true
							
							try(
							format ("Restoring Scene State\n")
							max create mode		
							max undo 					
							)catch(thisDidUndoOk = false)

							csEnd = timestamp()
							
							if thisDidUndoOk == true then
							(
								local doUndo = false
								--now do the cs ped.
								if chk_ExportCsVersion.state == true do 
								(
									format ("Trying a ig export...\n")
									
									undoText = "igPrepareAndExportCharacter"		

									igStart = timestamp()
									undo undoText on
									(			
										rsta_removeSceneRootObjects()
										
										doUndo = true							
										format "Exporting IG version...\n"		
								
										--first off check if the peds.pso.meta is latest
										fstats = ( gRsPerforce.getFileStats pedMetaFile )[ 1 ]
										haveRev = fstats.Item[ "haveRev" ] as integer
											format ("haveRev: "+(haveRev as string)+"\n")
										headRev = fstats.Item[ "headRev" ] as integer
											format ("headRev: "+(headRev as string)+"\n")				
										
										local rootNodes = #()
										for obj in objects do 
										(
											if obj.parent == undefined do 
											(
												append rootNodes obj
											)
										)
														
										local originalPedName = pedDummyNode.name							
										
										lowerCaseName = (toLower(substring pedDummyNode.name 1 3))

										local fileName = filenameFromPath CS_fbxFileName
										local filePath = (toLower(getFilenamePath CS_fbxFileName))
											
										if lowerCaseName == "cs_" then --swap cs_ part of name to IG_
										(
											local noneSpecificName = (substring pedDummyNode.name 4 -1)
											pedDummyNode.name = ("CS_"+noneSpecificName) --for safety
											local origName = pedDummyNode.name
											
											pedDummyNode.name = substituteString pedDummyNode.name "CS_" "IG_"
											format ("Changing CS Node name to "+(pedDummyNode.name)+" from "+(origName)+"\n")
											isCsPed = true
											
											fileName = substituteString fileName "CS_" "IG_"
										)

										

										local isIngame = findstring (toLower(filePath)) "cutscene"

										local csFilePath = filePath
										if isIngame != undefined do 
										(
											csFilePath = substituteString filePath "cutscene" "ingame"
-- 											splitPath = filterString csFilePath "/"
-- 												
-- 											for i = 1 to splitPath.count do 
-- 											(
-- 												if splitPath[i] == "cs" do
-- 												(
-- 													csFilePath = (substituteString filePath "cs" "")
-- 												)
-- 											)
											
										)		
										
										format ("csFilePath: "+csFilePath+"\n")
										format ("filename: "+filename+"\n")
										

-- 										local cs_fbxFileName = (csFilePath+"/"+fileName+".FBX")
										local cs_fbxFileName = (csFilePath+fileName)
										
										format ("cs_fbxFileName: "+cs_fbxFileName+"\n")
	-- 									if lowerCaseName != "cs_" then 
	-- 									(
	-- 										if lowerCaseName != "ig_" then 
	-- 										(
	-- 											format ("This ped is not IG or CS. We will export a cs version to the cs folder but keep existing name.\n")
	-- 											
	-- 											cs_fbxFileName = (csFilePath+"/"+pedDummyNode.name+".FBX")
	-- 										)
	-- 									)
										exportStyle  = 0							

	-- 									format ("IG FILENAME: "+(fileName)+"\n")
										
										PrepAndExportCharacter cs_fbxFileName chk_StripMaterials.state pedMetaFile charSex chk_OldStyle.state exportStyle metaPedMetaFile udpData
										
										pedDummyNode.name = originalPedName
										
										format ("IG Ped Export process complete.\n")
									)
									
									if doUndo == true do 
									(
										format ("Restoring Scene State\n")
										max create mode		
										max undo 					
									)
									igEnd = timestamp()
								)
								format ("Export complete.\n")
								format ("Processing CS Export took "+(((csEnd - csStart) / 1000.0) as string) +" seconds\n" )
								if chk_ExportCsVersion.state == true do 
								(
									format ("Processing IG Export took "+(((igend - igstart) / 1000.0) as string)+" seconds\n" )
								)
								
								messagebox "Complete. Please check the listener for any warnings." beep:true
							)
							else
							(
								--ok it didnt undo properly, so we will just re-open the file and start again.
								skinnedMeshes = #()
								for obj in objects do 
								(
									for m = 1 to obj.modifiers.count do 
									(
										if classof obj.modifiers[m] == Skin() do
										(
											appendIfUnique skinnedMeshes obj
										)
									)									
								)
								
								for geo in skinnedMeshes do
								(
									local skinFile = "c:\\skins\tmp.vml"
									rsta_savePerComponentSkinning geo skinSaveFileName:skinFile
									geo.modifiers[1].enabled = true
									collapseStack geo
									rsta_importSkinWeightsFromXml skinFile geo								
									geo.modifiers[1].enabled = true
								)
								
								selCount = getNumNamedSelSets()
								SelName = "*expressions"
								currItems = #()
								
								for i = selCount to 1 by -1 do 
								(
									setName = (getNamedSelSetName i)
									if (toLower(selName)) == (toLower(setName)) then
									(
										deleteItem selectionSets setName
									)
								)								
								
								
								format ("Scene restore failed. Reopening source file.\n")
								local sceneName = maxFilePath + maxFileName
								
								loadMaxFile sceneName quiet:true
								
								undoText = "csPrepareAndExportCharacter"		
								csStart = timestamp()
								undo undoText on
								(			
									doUndo = true							
									format "Exporting IG version...\n"		
							
									--first off check if the peds.pso.meta is latest
									fstats = ( gRsPerforce.getFileStats pedMetaFile )[ 1 ]
									haveRev = fstats.Item[ "haveRev" ] as integer
										format ("haveRev: "+(haveRev as string)+"\n")
									headRev = fstats.Item[ "headRev" ] as integer
										format ("headRev: "+(headRev as string)+"\n")				
									
									local rootNodes = #()
									for obj in objects do 
									(
										if obj.parent == undefined do 
										(
											append rootNodes obj
										)
									)
													
									pedDummyNode = getNodeByName tmpPedDummyNodeName 
									local originalPedName = pedDummyNode.name							
									
									lowerCaseName = (toLower(substring pedDummyNode.name 1 3))

									if lowerCaseName == "cs_" then 
									(
										local noneSpecificName = (substring pedDummyNode.name 4 -1)
										pedDummyNode.name = ("CS_"+noneSpecificName) --for safety
										format ("Changing CS Node name\n")

										pedDummyNode.name = substituteString pedDummyNode.name "CS_" "IG_"
										isCsPed = true
									)
									local filePath = (getFilenamePath CS_fbxFileName)
									
									local isIngame = findstring (toLower(filePath)) "cutscene"

									local csFilePath = filePath
									if isIngame != undefined do 
									(
										csFilePath = rsMakeSafeSlashes(substituteString filePath "cutscene" "ingame")
										
										splitPath = filterString csFilePath "/"
											
										for i = 1 to splitPath.count do 
										(
											if splitPath[i] == "cs" do
											(
												csFilePath = (substituteString filePath "cs" "")
											)
										)
										
									)		
									
									local cs_fbxFileName = (csFilePath+"/"+pedDummyNode.name+".FBX")
									
									if lowerCaseName != "cs_" then 
									(
										if lowerCaseName != "ig_" then 
										(
											format ("This ped is not IG or CS. We will export a cs version to the cs folder but keep existing name.\n")
											fName = filenameFromPath cs_fbxFileName
											
											cs_fbxFileName = (csFilePath+"/"+fName+".FBX")
										)
									)

									exportStyle = 0
-- 									PrepAndExportCharacter cs_fbxFileName chk_StripMaterials.state pedMetaFile charSex chk_OldStyle.state exportStyle pedMetaFile udpData
									PrepAndExportCharacter CS_fbxFileName chk_StripMaterials.state pedMetaFile charSex chk_OldStyle.state exportStyle metaPedMetaFile udpData
									pedDummyNode.name = originalPedName
									
									format ("IG Ped Export process complete.\n")
								)
								
								if doUndo == true do 
								(
									format ("Restoring Scene State\n")
									max create mode		
									max undo 					
								)
								csEnd = timestamp()							
									
								format ("Export complete.\n")
															
								format ("Processing IG Export took "+(((igend - igstart) / 1000.0) as string)+" seconds\n" )
								if chk_ExportCsVersion.state == true do 
								(
									format ("Processing CS Export took "+(((csEnd - csStart) / 1000.0) as string) +" seconds\n" )
								)			
								
								
								messagebox "Complete. Please check the listener for any warnings." beep:true								
							)
						)
						else
						(
							format ("Asset setup failed.\n")
						)
					)
					else
					(
						format ("This max file is not checked into perforce. We cannot continue.\n")
						messagebox ("This max file is not checked into perforce. We cannot continue.\n") beep:true title:"Export Failure."
					)
-- 				)
			)
			else
			(
				format ("We cannot auto derive the skeleton type from the file name. Please select from the dropdown and try again.\n")
				messagebox ("We cannot auto derive the skeleton type from the file name. Please select from the dropdown and try again.\n") beep:true 
			)
	)

	on btn_PrepareCharacter pressed do 
	(
		udpData = undefined
		PrepareCharacter()		
	)

	on btn_PrepareExpressions pressed do 
	(	
-- 		PrepareExpressions()
		
		local pedPath = (maxFilePath)
		
		IG_fbxFileName = getSaveFileName fileName:fbXAssetRoot caption:"What would the fbx be called?" types:"FBX File (*.FBX)|*.fbx|All Files (*.*)|*.*|"
		
		local pedFileName = (getFilenameFile IG_fbxFileName)
		local pedNameAndPath = (pedPath + pedFileName)	
		
		max select all
		local thisSel = selection as array
		
		local exprNodesToUse = #()
		for obj in thisSel do 
		(
			if (substring obj.name 1 7) != "FACIAL_" do
			(
				if findstring (toLower(obj.name)) "muscle" == undefined do 
				(
					isSpring = rsta_findSpring obj 
					
					if isSpring.count == 0 do
					(
						appendIfUnique exprNodesToUse obj
					)
				)
			)
		)		
		
		PrepareExpressions pedNameAndPath exprNodesToUse
		
		format ("Prepare Expressions complete.\n")
	)

	on btn_NukeMaterials pressed do 
	(	
		rsta_NukeMaterials false true
	)	
	
	on btn_RestoreMaterials pressed do 
	(
		restoreMaterials rageMaterialsArray
	)
	
	on btn_removeFacial pressed do 
	(
-- 		objNodes = objects as array
		
-- 		rsta_removeUfcStleControllers objNodes 
		rsta_removeRsExprControllers()
	)
)

rollout sub_Props "Props"
(
	--//////////////////////////////////////////////////////////////////////////////
	-- 	VARIABLES
	--//////////////////////////////////////////////////////////////////////////////	
			
-- 	checkBox chk_StripMaterials "Strip MAterials" checked:true pos:[10,10] tooltip:"Strip materials from fbx." checked:false
	
	button btn_PrepareAndExportProp "Prepare & Export Prop" width:180 height:30 tooltip:"Configure scene for motionbuilder and export to fbx."

	button btn_PrepareProp "Prepare Prop" width:180 height:30 tooltip:"Prepare Prop."
	
	button btn_NukeMaterials "Nuke Materials" width:180 height:30 tooltip:"Nuke Materials"
	
	button btn_RestoreMaterials "Restore Materials" width:180 height:30 tooltip:"Nuke Materials"
	

	--//////////////////////////////////////////////////////////////////////////////
	-- 	FUNCTIONS
	--//////////////////////////////////////////////////////////////////////////////
	
	fn enableButtons enableState = 
	(
		if enableState == 1 then
		(
		)
		else
		(	
		)		
	)
	
	--//////////////////////////////////////////////////////////////////////////////
	-- 	EVENTS
	--//////////////////////////////////////////////////////////////////////////////	
	
	
	on sub_Props open do
	(
	)

	on btn_PrepareAndExportProp pressed do 
	(
		prepareAndExportProp()		
	)

	on btn_PrepareProp pressed do 
	(
		fbxFileName = getSaveFileName caption:"FBX File" types:"FBX File (*.FBX)|*.fbx|All Files (*.*)|*.*|" fileName:(theProjectRoot+"art/animation/*.fbx")
		if fbxFileName != undefined then
		(
			skipMatStrip = false

			initPos = PrepareProp false skipMatStrip fbxFileName
		)
	)

	on btn_NukeMaterials pressed do 
	(	
		rsta_NukeMaterials false true
	)	
	
	on btn_RestoreMaterials pressed do 
	(
		restoreMaterials rageMaterialsArray
	)
)

rollout sub_Vehicles "Vehicles"
(
	--//////////////////////////////////////////////////////////////////////////////
	-- 	VARIABLES
	--//////////////////////////////////////////////////////////////////////////////	
-- 	checkBox chk_StripMaterials "Strip MAterials" checked:true pos:[10,10] tooltip:"Strip materials from fbx." checked:false		
		
	button btn_PrepareAndExportVehicles "Prepare & Export Vehicles" width:180 height:30 tooltip:"Configure scene for motionbuidler and export to fbx."

	button btn_PrepareVehicles "Prepare Vehicles" width:180 height:30 tooltip:"Prepare Character."
	
	button btn_NukeMaterials "Nuke Materials" width:180 height:30 tooltip:"Nuke Materials"
	
	button btn_RestoreMaterials "Restore Materials" width:180 height:30 tooltip:"Restore Materials"
	

	--//////////////////////////////////////////////////////////////////////////////
	-- 	FUNCTIONS
	--//////////////////////////////////////////////////////////////////////////////
	
	fn enableButtons enableState = 
	(
		if enableState == 1 then
		(
		)
		else
		(	
		)		
	)
	
	--//////////////////////////////////////////////////////////////////////////////
	-- 	EVENTS
	--//////////////////////////////////////////////////////////////////////////////	
	
	
	on sub_Vehicles open do
	(
	)

	on btn_PrepareAndExportVehicles pressed do 
	(
		PrepAndExportVehicle()		
	)

	on btn_PrepareVehicles pressed do 
	(
		PrepareVehicle()		
	)

	on btn_NukeMaterials pressed do 
	(	
		rsta_NukeMaterials false true
	)	
	
	on btn_RestoreMaterials pressed do 
	(
		restoreMaterials rageMaterialsArray
	)
)

rollout sub_Map "Map"
(
	--//////////////////////////////////////////////////////////////////////////////
	-- 	VARIABLES
	--//////////////////////////////////////////////////////////////////////////////	
	checkBox chk_InteriorAsset "interior Asset" checked:false pos:[10,30] 		
	checkBox chk_StripMaterials "Strip MAterials" checked:true pos:[10,10] tooltip:"Strip materials from fbx." checked:true
		
	button btn_ImportMapAssets "Import Map Assets" width:180 height:30 tooltip:"Import Map Assets."

	button btn_PrepareAndExportMap "Prepare & Export Map" width:180 height:30 tooltip:""
	
	button btn_PrepareMap "Prepare Map" width:180 height:30 tooltip:""

	button btn_NukeMaterials "Nuke Materials" width:180 height:30 tooltip:"Nuke Materials"
	
	button btn_RestoreMaterials "Restore Materials" width:180 height:30 tooltip:"Nuke Materials"
	
	button btn_ExportFBX "Export FBX" width:180 height:30 tooltip:""
	

	--//////////////////////////////////////////////////////////////////////////////
	-- 	FUNCTIONS
	--//////////////////////////////////////////////////////////////////////////////
	
	fn enableButtons enableState = 
	(
		if enableState == 1 then
		(
		)
		else
		(	
		)		
	)
	
	--//////////////////////////////////////////////////////////////////////////////
	-- 	EVENTS
	--//////////////////////////////////////////////////////////////////////////////	
	
	
	on sub_Map open do
	(
	)

	
	on btn_ImportMapAssets pressed do 
	(
		ImportMapAssets chk_InteriorAsset.state		
	)

	on btn_PrepareAndExportMap pressed do 
	(	
		EnvSetupPrepMapAndExport chk_StripMaterials.state
	)

	on btn_PrepareMap pressed do 
	(
		CSMapExport()		
	)

	on btn_NukeMaterials pressed do 
	(	
		rsta_NukeMaterials false true
	)	
	
	on btn_RestoreMaterials pressed do 
	(
		restoreMaterials rageMaterialsArray
	)

	on btn_ExportFBX pressed do 
	(
		rsta_prepForFBXExport()
	)	
)
createDialog CsResourcePrep_UI width:CsResourcePrep_UIWidth height:CsResourcePrep_UIHeight style:#(#style_titlebar, #style_border, #style_sysmenu)
