filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())


Fn materialfix = 
(	 
	for i = 1 to 24 do 
	(

		howmany = meditMaterials[i].numsubs
		print ("Total number of subs = "+(howmany as string))
		
		for subid = 1 to howmany do
		(
			dtex = undefined
			print ("Testing subID:"+(subId as string)+" on Material slot "+(i as string))

				if classof meditMaterials[i] == Multimaterial then --is it a multisub material
				(
					if (classof meditMaterials[i].materialList[subId] as string) == "Rage_Shader" do --is it a rage shader
					(
						print "Testing rage Material"
						print ("meditMaterials["+(i as string)+"].materialList["+(subId as string)+"].no_name" )
-- 						dtex = meditMaterials[i].materialList[subId].no_name.fileName
						
						--ok need to test if we have a valid 'no_name' - this basically crashes if the slot has no texture
						
						shaderVarCount = rstGetVariableCount meditMaterials[i].materialList[subId]

						for sV = 1 to shaderVarCount do
						(
							slotName = RstGetVariableName meditMaterials[i].materialList[subId] sV

							if slotName == "Diffuse Texture" do
							(
								print "Found diff"
								if (RstGetVariable meditMaterials[i].materialList[subId] sV ) != "" do
								(
									print ("Diff txture = " +(RstGetVariable meditMaterials[i].materialList[subId] sV ) )
									dtex = meditMaterials[i].materialList[subId].no_name.fileName
								)
							)
						)						
					)
				)
				else
				(
					if (classof meditmaterials[i] as string) == "Rage_Shader" do --ok none multimat rage
					(
						if meditMaterials[i].no_name != undefined do
						(
							dtex = meditMaterials[i].no_name.fileName
						)							
					)
				)


				if (classof meditMaterials[i]) == Multimaterial then --is it a multisub material
				(
						if (classof meditMaterials[i].materialList[subId] )== Standardmaterial do -- is it a standard material
						(
						
							print ("trying meditMaterials["+(i as string)+"].materialList["+(subId as string)+"].diffuseMap.bitmap")

							if meditmaterials[i].materialList[subId].diffusemap != undefined do
							(
								if (classof meditmaterials[i].materialList[subId].diffusemap ) == "Bitmaptexture" do
								(
									dtex = meditmaterials[i].materialList[subId].diffusemap.bitmap
								)
							)
						)

				)
				else
				(
					if (classof meditmaterials[i] ) == Standardmaterial do --ok none multimat standard
					(
						if meditmaterials[i].diffusemap != undefined do
						(
							if (classof meditmaterials[i].diffusemap as string) == "Bitmaptexture" do
							(
								print ("Looking for: "+(meditmaterials[i].diffusemap.filename as string))
								foundTexture = doesFileExist meditmaterials[1].diffusemap.filename
								if foundTexture == true then
								(
									dtex = meditmaterials[i].diffusemap.bitmap
								)
								else
								(
									dtex = ""
								)
							)
						)
					)
				)

					-- try to set them by overiding the origanal rage shader
				if dtex != undefined do
				(
					try 
					(
						print ("SETTING: meditMaterials["+(i as string)+"].material["+(subid as string)+"] TO BE "+(dtex as string))
						meditMaterials[i].material[subid] = Standardmaterial () 	
-- 						meditmaterials[i].materialList[subid].diffusemap = Bitmaptexture fileName:dtex
-- 						meditmaterials[i].materialList[subid].specularLevelMap = undefined
-- 						meditmaterials[i].materialList[subid].bumpMap = undefined
						meditmaterials[i].material[subid].diffusemap = Bitmaptexture fileName:(dtex as string)
						meditmaterials[i].material[subid].specularLevelMap = undefined
						meditmaterials[i].material[subid].bumpMap = undefined

						
					) catch()
				)

			) -- close the texture change loop
			
		MMClean.fixall prompt:false			-- Clean out unused slots

		actionMan.executeAction 0 "63508"  -- Views: Standard Display with Maps
	
	)-- close the loop for everything materials!!!
	
	print "Material fix complete!"
	messagebox "Material fix complete!" beep:true
) -- close materialfix


materialfix()