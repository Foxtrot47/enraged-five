--tool to create buttons on an object 
--user provides a bug number via a gui then a button gets created on an object which opens that bug via bug*

--July 2011
--Matt Rennie & Kat

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BugStarButtons = undefined

fn generateBugStarButton obj bugNumber =
(
	rollStr1 = ("BugStarButtons = attributes bugStarLinks"+"\r\n"+"("+"\r\n\t"+"parameters main rollout:bugTrack"+"\r\n\t"+"("+"\r\n\t"+")"+"\r\n")
	rollStr2 = ("\t"+"rollout bugTrack "+"\""+"Bug*"+"\""+"\r\n\t"+"(")
	rollStr3 = ("\r\n\t\t"+"button "+"btn_"+(bugNumber as string)+" "+"\""+"Bug# "+(bugNumber as string)+"\""+"\r\n\r\n")
	rollStr4 =("\t\t"+"on btn_"+(bugNumber as string)+" pressed do"+"\r\n\t\t"+"(")
	rollStr5 = ("\r\n\t\t\t"+"print "+"\""+"url:bugstar:"+(bugNumber as string)+"\""+"\r\n\t\t\t"+"ShellLaunch "+"\""+"url:bugstar:"+(bugNumber as string)+"\""+"\""+"\""+"\r\n\t\t"+")"+"\r\n\t"+")"+"\r\n"+")")
		
	finalStr = (rollStr1+rollStr2+rollStr3+rollStr4+rollStr5)
		clearListener()
		print finalStr
	
	execute finalStr
	
	if obj.modifiers[#'Attribute holder'] == undefined do
	(
		addModifier obj (EmptyModifier ())
	)
	max modify mode
	custAttributes.add obj.modifiers[#'Attribute holder'] BugStarButtons
)


if ((bugstarSetupGUI != undefined) and (bugstarSetupGUI.isDisplayed)) do
	(destroyDialog bugstarSetupGUI)

rollout bugstarSetupGUI "Cutscene Reference"
(
	editText edtTxt "Bug Number:" text:"" width:150 pos:[13, 15] 
	editText edtTxt2 "Cutscene Name:" text:"" width:180 pos:[13, 45] 
	editText edtTxtx "X:" text:"" width:60 pos:[13, 80] 
	editText edtTxty "Y:" text:"" width:60 pos:[80, 80] 
	editText edtTxtz "Z:" text:"" width:60  pos:[147, 80] 
-- 	editText edtTxtRot "R:" text:"" width:60  pos:[214, 70] 
	button btnAddBug "Add Bug" width:110 pos:[53, 120] 
		
	on btnAddBug pressed do
	(
		if edtTxt.text != "" then
		(
				bugNumber = edtTxt.text

				lXTrns = edtTxtx.text as float
				lYTrns = edtTxty.text as float
				lZTrns = edtTxtz.text as float

				lCutsceneStage = plane pos:[lXTrns, lYTrns, lZTrns]
				lCutsceneStage.name = edtTxt2.text
				lCutsceneStage.length = 14.817
				lCutsceneStage.width = 9.721
				lCutsceneStage.wirecolor = color 228 214 153
				lCutsceneStage.isSelected = on
					
				lCutsceneText = text pos:[lXTrns, lYTrns, lZTrns]
				lCutsceneText.name = edtTxt2.text
				lCutsceneText.text = edtTxt2.text
				lCutsceneText.wirecolor = color 255 255 255
				lCutsceneText.size = 1
				--lCutsceneText.rotation = (quat 0 0 -0.707107 0.707107)
				
				lCutsceneText.parent = lCutsceneStage
				
				edtTxt.text = ""
				edtTxt2.text = ""
				edtTxtx.text = ""
				edtTxty.text = ""
				edtTxtz.text = ""
				
				generateBugStarButton lCutsceneStage bugNumber			
		)
		else
		(
			messageBox "Please enter a valid bug number!" beep:true
		)
	)
)

CreateDialog bugstarSetupGUI width:220 height: 150 pos:[1200, 100] 