-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

theProjectConfig = RsConfigGetProjBinConfigDir()

toolsRoot = RsConfigGetToolsRootDir()

filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_Perforce.ms")

filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_Rigging.ms")

filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_UI.ms")

filein (theWildWest + "script/3dsMax/Cutscene/cs_ResourcePrep_Functions.ms")

rsta_loadCommonFunction #("FN_RSTA_userSettings")

if (cs_ResourcePrepStruct != undefined) do cs_ResourcePrepStruct.dispose()
struct cs_ResourcePrepStruct
(
	-- LOCALS ------------------------------------------------
	MainWindow, 
	VM,
	Settings = rsta_userSettings app:"cs_ResourcePrep",	
	settingsArray = #("SelectedTabIndex"),
	
--  	dllFile = "X:/wildwest/src/Library/RSG.TechArt/RSG.TechArt.cs_ResourcePrep/cs_ResourcePrep/cs_ResourcePrep/bin/Release/RSG.TechArt.cs_ResourcePrep.dll",
	dllFile = rsMakeSafeSlashes (theProjectRoot+"tools_ng/techart/dcc/3dsMax/RSG.TechArt.cs_ResourcePrep.dll"),
	rageMaterialsArray = undefined,
	
	-- FUNCTIONS ---------------------------------------------
	fn dispose = 
	(
		if (MainWindow != undefined) do 
		(
			try(
			-- WINDOW POS 
			settings.wpf_windowPos mainWindow #set
			-- SETTINGS 
			for attr in settingsArray do Settings.setValue attr (getProperty MainWindow.dataContext attr)			
			
			-- CLOSE AND DISPOSE THE WINDOW
			
				MainWindow.Close()    
			MainWindow = undefined)catch(MainWindow = undefined)
		)
		execute ("cs_ResourcePrepStruct = undefined")
	),
	-------------------------------------------------------------------
	fn init =
	(
		-- LOAD ASSEMBLY
		dotnet.loadAssembly dllFile
		-- MAKE MAIN WINDOW INSTANCE 
		MainWindow = dotNetObject "RSG.TechArt.cs_ResourcePrep.MainWindow" -- IE. "RSG.TechArt.MassCalculator.MainWindow"             
		dn_window.setup MainWindow 
		MainWindow.Tag = dotnetmxsvalue this
		-- PARENTS THE WINDOW UNDER MAX, STOPS IT FROM DROPPING BEHIND
		MainWindow.ChangeOwner (DotNetObject "System.IntPtr" (Windows.GetMAXHWND()))			
		-- SHOW THE WINDOW
		MainWindow.show()
			
		-- WINDOW POS
		settings.wpf_windowPos mainWindow #get
		VM = MainWindow.dataContext
		
		-- SETTINGS 
		for attr in settingsArray do 
		(
			local val = (Settings.getValue attr)
			if (val != undefined) do setProperty MainWindow.dataContext attr val
		)				
			
		--cs_ResourcePrepStruct.vm.selectedTabIndex = 0 --this is how i could if i wanted to set the tab via maxscript
			
		-- SET PREFS
		--settings.get
	),
	-- EVENTS -----------------------------------------------------
	on create do init()            
)
cs_ResourcePrepStruct = cs_ResourcePrepStruct()