--script to find any nodes in the scene which contain a float expression
-- Oct 2011
--Matt rennie

------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------

objWithExprArray = #()

controllersToInclude = #(
	"Float_Expression",
	"Controller:Float_Expression" ,
	"SubAnim:RsSpring",
	"SubAnim:LookAt_Constraint"
	)

------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn sclXYZ obj = 
(
	for cont in controllersToInclude do
	(
		if (obj.scale.controller as string) == "SubAnim:RsSpring" then
		(
			appendIfUnique objWithExprArray obj
		)
		else
		(		
			if (classof obj.Scale.controller.X_Scale.controller as string) == cont do
			(
				--print (cont+" found on X Scale of "+obj.name)
				appendIfUnique objWithExprArray obj
			)
			if (classof obj.Scale.controller.Y_Scale.controller as string) == cont do
			(
				--print (cont+" found on Y Scale of "+obj.name)
				appendIfUnique objWithExprArray obj
			)	
			if (classof obj.Scale.controller.Z_Scale.controller as string) == cont do
			(
				--print (cont+" found on Z Scale of "+obj.name)
				appendIfUnique objWithExprArray obj
			)		
		)
	)
)

fn sclList obj = 
(
	for i = 2 to obj.scale.controller.count do --skip 1st controller as thats the frozen one
	(
		for cont in controllersToInclude do
		(
			if (obj.scale.controller[i] as string) == "SubAnim:RsSpring" then
			(
				appendIfUnique objWithExprArray obj
			)
			else
			(			
				if (classof obj.scale.controller[i].X_scale.controller as string) == cont do
				(
					--print (cont+" found on X Scale of "+obj.name)
					appendIfUnique objWithExprArray obj
				)
				if (classof obj.scale.controller[i].Y_scale.controller as string) == cont do
				(
					--print (cont+" found on Y Scale of "+obj.name)
					appendIfUnique objWithExprArray obj
				)
				if (classof obj.scale.controller[i].Z_scale.controller as string) == cont do
				(
					--print (cont+" found on Z Scale of "+obj.name)
					appendIfUnique objWithExprArray obj
				)		
			)
		)
	)		
)

fn rotEulerXYZ obj = 
(
	for cont in controllersToInclude do
	(
		if (obj.rotation.controller as string) == "SubAnim:RsSpring" then
		(
			appendIfUnique objWithExprArray obj
		)
		else
		(
			if (obj.rotation.controller[i] as string) == "SubAnim:LookAt_Constraint" then
			(
				appendIfUnique objWithExprArray obj
			)
			else			
			(
				if (classof obj.Rotation.controller.X_Rotation.controller as string) == cont do
				(
					--print (cont+" found on X Rotation of "+obj.name)
					appendIfUnique objWithExprArray obj
				)
				if (classof obj.Rotation.controller.Y_Rotation.controller as string) == cont do
				(
					--print (cont+" found on Y Rotation of "+obj.name)
					appendIfUnique objWithExprArray obj
				)	
				if (classof obj.Rotation.controller.Z_Rotation.controller as string) == cont do
				(
					--print (cont+" found on Z Rotation of "+obj.name)
					appendIfUnique objWithExprArray obj
				)	
			)
		)
	)
)

fn rotList obj = 
(
	for i = 2 to obj.rotation.controller.count do --skip 1st controller as thats the frozen one
	(
		for cont in controllersToInclude do
		(
			if (obj.rotation.controller[i] as string) == "SubAnim:RsSpring" then
			(
				appendIfUnique objWithExprArray obj
			)
			else
			(
				if (obj.rotation.controller[i] as string) == "SubAnim:LookAt_Constraint" then
				(
					appendIfUnique objWithExprArray obj
				)
				else
				(
					if (classof obj.rotation.controller[i].X_rotation.controller as string) == cont do
					(
						--print (cont+" found on X Rotation of "+obj.name)
						appendIfUnique objWithExprArray obj
					)
					if (classof obj.rotation.controller[i].Y_rotation.controller as string) == cont do
					(
						--print (cont+" found on Y Rotation of "+obj.name)
						appendIfUnique objWithExprArray obj
					)
					if (classof obj.rotation.controller[i].Z_rotation.controller as string) == cont do
					(
						--print (cont+" found on Z Rotation of "+obj.name)
						appendIfUnique objWithExprArray obj
					)		
				)
			)
		)
	)		
)

fn posXYZ obj = 
(
	for cont in controllersToInclude do
	(
		if (obj.position.controller as string) == "SubAnim:RsSpring" then
		(
			appendIfUnique objWithExprArray obj
		)
		else
		(		
			if (classof obj.position.controller.X_Position.controller as string) == cont do
			(
				--print (cont+" found on X Position of "+obj.name)
				appendIfUnique objWithExprArray obj
			)
			if (classof obj.position.controller.Y_Position.controller as string) == cont do
			(
				--print (cont+" found on Y Position of "+obj.name)
				appendIfUnique objWithExprArray obj
			)	
			if (classof obj.position.controller.Z_Position.controller as string) == cont do
			(
				--print (cont+" found on Z Position of "+obj.name)
				appendIfUnique objWithExprArray obj
			)
		)
	)
)

fn posList obj = 
(
	for i = 2 to obj.position.controller.count do --skip 1st controller as thats the frozen one
	(
		for cont in controllersToInclude do
		(
			if (obj.position.controller[i] as string) == "SubAnim:RsSpring" then
			(
				appendIfUnique objWithExprArray obj
			)
			else
			(
				if (classof obj.position.controller[i].X_Position.controller as string) == cont do
				(
					--print (cont+" found on X Position of "+obj.name)
					appendIfUnique objWithExprArray obj
				)
				if (classof obj.position.controller[i].Y_Position.controller as string) == cont do
				(
					--print (cont+" found on Y Position of "+obj.name)
					appendIfUnique objWithExprArray obj
				)
				if (classof obj.position.controller[i].Z_Position.controller as string) == cont do
				(
					--print (cont+" found on Z Position of "+obj.name)
					appendIfUnique objWithExprArray obj
				)		
			)
		)
	)	
)

fn queryFloatExpression obj = 
(
	--print ("Testing "+obj.name)
	if (classof obj.position.controller as string) == "position_list" do
	(
		--print ("Position List found on "+obj.name)
		posList obj
	)
	if (classof obj.position.controller as string) == "Position_XYZ" do
	(
		--print ("Position XYZ found on "+obj.name)
		posXYZ obj
	)
	---------
	if (classof obj.rotation.controller as string) == "rotation_list" do
	(
		--print ("Rotation List found on "+obj.name)
		rotList obj
	)
	if (classof obj.position.controller as string) == "Euler_XYZ" do
	(
		--print ("Euler XYZ found on "+obj.name)
		rotEulerXYZ obj
	)
	--------------
	if (classof obj.scale.controller as string) == "scale_list" do
	(
		--print ("Scale List found on "+obj.name)
		sclList obj
	)
	if (classof obj.scale.controller as string) == "Controller:ScaleXYZ" do
	(
		--print ("Scale XYZ found on "+obj.name)
		sclXYZ obj
	)
	
)

fn findExpressionObjects = 
(
	for obj in objects do
	(
		if classof obj != Targetcamera do 
		(
			if classof obj != IES_Sky do 
			(
				if (substring obj.name 1 6) != "FACIAL" do
				(
					if (substring obj.name 1 2) != "FB" do 
					(
						queryFloatExpression obj
					)
				)
			)
		)
	)

	if objWithExprArray.count != 0 do
	(
		for i = 1 to objWithExprArray.count do
		(
			--print objWithExprArray[i].name
		)
	)

	for o in objWithExprArray do
	(
		unhide o
	)
	select objWithExprArray
)

findExpressionObjects()