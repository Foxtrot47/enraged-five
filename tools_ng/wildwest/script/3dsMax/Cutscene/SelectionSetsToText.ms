-- SelectionSetsToText
-- A script write a files selection sets out to a text file.
-- Each selection set is created in an array.  The array is constructed as follows:
-- #(selction set x of y, selection set name, selection set contents (as an array))
--
-- Text file is created in the same folder as the max scene.
--
-- Stewart Wright
-- 29/2/12  Created
-- ****************************************************************************************

selectionSetFile = ""

-------------------------------------------------------------------------------------------------------------------------
--function for writing out the selection sets to a text file
fn writeSelectionSets =
(
	everything = #()
	
	outputFolder = maxfilepath  --output folder is the same as the max file
	outputFile = outputFolder + "SelectionSets.txt"  --save the file as "SelectionSets.txt"
	selectionSetFile = createFile outputFile  --now we make the text file using the above settings
	
	--fileHeadText = ("Created on " + localtime)
	--print fileHeadText to: selectionSetFile --write the system time to the text file

	for ss = 1 to selectionsets.count do
	(
		singleSelSet = #(infoText = ("Selection Set " + ss as string + " of " + selectionsets.count as string)) --some header information
		selSetName = SelectionSets[ss].name --the name of the selection set
		append singleSelSet selSetName
		matchSS = for obj in SelectionSets[ss] collect obj.name --turn sel.set into array
		append singleSelSet matchSS
		
		append everything singleSelSet
	)
	
	print everything to: selectionSetFile
	
	close selectionSetFile  --housekeeping.  this closes the txt file
)--end writeSelectionSets

-------------------------------------------------------------------------------------------------------------------------
rollout OutputSelect "Save Selection Sets"
(
	button btnWriteThem "Write Selection Sets" width:160 height:50
	button btnOpenPath "Open File Path" width:160 height:50
	
	on btnWriteThem pressed do
	(
		writeSelectionSets()
	)
	
	on btnOpenPath pressed do
	(
		try (shellLaunch maxfilepath "") catch (messagebox "Couldn't work out max file path?")
	)
)

-------------------------------------------------------------------------------------------------------------------------
try(closeRolloutFloater theSelectionSetsFloater)catch() -- Destroy the UI if it exists 
theSelectionSetsFloater = newRolloutFloater "Selection Sets Output" 200 150
addRollout OutputSelect theSelectionSetsFloater rolledUp:false