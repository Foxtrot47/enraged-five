--renameSelectedObjects.ms
--Spet2010
--Matt Rennie
-- script to rename selected objects to a name supplied by the user
-- if (renameSelectionFloater != undefined) do (destroyDialog renameSelectionFloater)

-- global renameSelectionFloater = undefined
global renameSelection
global textVal = ""

fn renameSelected nameToRenameTo =
(
	myArray = selection as array
	for i = 1 to myArray.count do
	(
		print ("renaming "+myArray[i].name+" to "+nameToRenameTo+"_"+(i as string))
		myArray[i].name = nameToRenameTo+"_"+(i as string) + "_" + myArray[i].name
	)
	print "Renaming complete."

)

fn renameUI = 
(
	try(if renameSelectionFloater != undefined then closeRolloutFloater renameSelectionFloater)catch()
	
	textVal = ""
	rollout renameSelection ""
	(
		edittext renameText "Rename to:" fieldWidth:200 labelOnTop:true width:260 height:25
		button btnRename "Rename" width:170 height:25
		button btnGround "Ground" width:170 height:25
		button btnDressing "Dressing" width:170 height:25
		button btnProp "Prop" width:170 height:25
		button btnWall "Wall" width:170 height:25
		button btnCeiling "Ceiling" width:170 height:25
		button btnWD "Wall Dressing" width:170 height:25
		
		on renameText entered txt do
		(
			if txt != "" do
			(
				textVal = txt
			)
		)
		
		on btnRename pressed do
		(
			if textVal != "" then 
			(
				renameSelected textVal
			)
			else
			(
				print "No text value specified"
			)
		)
		
		on btnGround pressed do 
		(
			renameSelected "Ground"
		)
		
		on btnDressing pressed do 
		(
			renameSelected "Dressing"
		)
		
		on btnProp pressed do 
		(
			renameSelected "Prop"
		)
		
		on btnWall pressed do 
		(
			renameSelected "Wall"
		)
		
		on btnCeiling pressed do 
		(
			renameSelected "Ceiling"
		)
		
		on btnWD pressed do 
		(
			renameSelected "Wall_Dressing"
		)
	)

	renameSelectionFloater = newRolloutFloater "Rename Selection" 300 290
	
-- 	renameSelectionFloater = createDialog "Rename Selection" 300 290
	
	addRollout renameSelection renameSelectionFloater

 )

 renameUI()