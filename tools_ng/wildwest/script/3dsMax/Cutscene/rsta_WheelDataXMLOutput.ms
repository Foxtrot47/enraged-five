-- Kathryn Adams
-- 29 Sept 2014
-- Output Wheel Details into an XML
-- url:bugstar:2027574

filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_xml.ms")


------------------------------------------------------------------------------------
-- write out wheel data to an xml
------------------------------------------------------------------------------------
fn RSTA_WheelDataOutput multiWheelArray =
(
	-- xml setup: instance of the struct
	local xml_wheelData = rsta_xml_io()
	
	-- create root
	xml_wheelData.new "WheelDataFile"
	
	-- create sub-roots
	maxPath = maxfilepath + maxfilename
	local vehiclePathNode = rsta_xml.makeNode "VehiclePath" xml_wheelData.root innerText:maxPath
	local dataList = rsta_xml.makeNode "WheelDataList" xml_wheelData.root 
	
	-- iterate through the array of structs and create xml nodes with the wheel data
	for i in multiWheelArray do
	(
		local item = rsta_xml.makeNode "Item" dataList 			
		rsta_xml.makeNode "BoneName" item innerText:i.boneObject
		rsta_xml.makeNode "MeshName" item innerText:i.meshObject
		rsta_xml.makeNode "CollisionName" item innerText:i.collisionObject
		rsta_xml.makeNode "CollisionRadius" item attr:"value" value:( i.collisionRadius as string )
	)
	
	-- get vehicle name
	count =  ( ( maxfilename.count ) - 12 )
	vehicleName =  ( substring maxfilename 1 count )
	
	--- save xml
	xml_wheelData.xmlFile = "x:/VehicleWheelData_" + vehicleName + ".xml"
	xml_wheelData.save saveWarning:false
)


------------------------------------------------------------------------------------
-- get wheel details and input into a struct.  add each struct to an array.
------------------------------------------------------------------------------------
fn RSTA_GenerateWheelInfo =
(
	clearlistener()

	-- array container for each struct (one per wheel)
	local wheelStructArray = #()
	
	-- similar to a class in python
	struct RSTA_WheelStructParams ( boneObject, meshObject, collisionObject, collisionRadius )
	
	-- get all objects in the file
	local allObjects = objects as array
	
	--- iterate through objects by index
	for i = 1 to allObjects.count do
	(
		
		obj = allObjects[i]
		
		local wheelBone = "None"
		local wheelMesh ="None"
		local wheelCollision = "None"
		local wheelRadius = "None"		
		
		
		-- find wheel bone
		if classof obj == Dummy do
		(
			if tolower( substring obj.name 1 6 ) == "wheel_" do
			(
				wheelBone = obj 
				
				if wheelBone != undefined do
					
					(
						-- find the wheel mesh related to the bone
						for child in wheelBone.Children do
						(
							local wheelBoneName = wheelBone.Name
							
							local foundIndex = findstring wheelBoneName "_"
							
							if foundIndex != undefined do 
							(
								local wheelTypeName = tolower( substring wheelBoneName foundIndex -1 )
								
								local wheelTypeLength = wheelTypeName.count
								
								local childNameCount = child.name.count
								
								local childSuffix = tolower( substring child.name ( childNameCount - wheelTypeLength + 1 ) -1 )
								
								if childSuffix == wheelTypeName do
								(
									wheelMesh = child.Name
								)
							)
						)
						
						-- find the wheel collision related to the bone
						for child in wheelBone.Children do
						(
							if classof child == Collision_Disc do
							(
								wheelCollision = child.Name
								wheelRadius = child.Radius
							)
						)
						
						-- build wheel struct
						RSTA_WheelStruct = ( RSTA_WheelStructParams boneObject:wheelBone.Name meshObject:wheelMesh collisionObject:wheelCollision collisionRadius:wheelRadius )
							
						-- append struct to multi-dimensional array
						appendifunique wheelStructArray RSTA_WheelStruct
					)
				)
			)	
		)	
	return wheelStructArray
)

wheelData = RSTA_GenerateWheelInfo()	
RSTA_WheelDataOutput wheelData
