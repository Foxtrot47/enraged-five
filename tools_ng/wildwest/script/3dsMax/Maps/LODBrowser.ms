if (not gRsIsOutsource) do ( fileIn (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms") )

if( LODBrowserSystem == Undefined ) then
(
::LODBrowserSystem = CSharp.CompileToMemory #( RsConfigGetWildWestDir() + "script/3dsMax/Maps/LODBrowser.cs" )
)

try
( 
	LODBrowser.ToolWindow.Close()
) catch()

struct LODBrowser
(
	--Contains the core logic of the lod browser
	Core,
	
	--The tool window
	ToolWindow,
	
	--UI mark up file
	UIMarkupFilePath = RsMakeBackSlashes( RsConfigGetWildWestDir() + "/script/3dsMax/UI/LODBrowser.xaml" ),
	
	/*------------------------------------------------------------------------------
	Merges all the passed LODObjects into the scene.
	*/------------------------------------------------------------------------------
	fn MergeLODObjects InputLODObjects =
	(
		for LODObject in InputLODObjects do
		(
			PreMergeCount = Objects.Count
			
			MergeMAXFile LODObject.SourceFilePath #(LODObject.SourceObjectName) #noRedraw #skipDups #renameMtlDups #neverReparent Quiet:True
			
			--Get the objects that we merged in
							
			MergedObjects = #()
			CurrentObjectIndex = Objects.Count
			while( CurrentObjectIndex > PreMergeCount ) do
			(
				CurrentObject = Objects[CurrentObjectIndex]
				Append MergedObjects CurrentObject
				CurrentObjectIndex = CurrentObjectIndex - 1
			)

			--Check if max merged any objects
			if( MergedObjects.Count == 0 ) then
			(
				continue()
			)
			
			--Check if max merged too many objects and try and clean up
			if( MergedObjects.Count > 1 ) then
			(
				for MergedObject in MergedObjects do
				(
					if( MergedObject.Name != LODObject.SourceObjectName ) then
					(
						Delete MergedObject
					)
				)
			)
			
			--If we still don't have one object then delete everything and continue
			if( MergedObjects.Count != 1 ) then
			(
				for MergedObject in MergedObjects do
				(
					if( IsValidNode( MergedObject ) ) then
					(
						Delete MergedObject
					)
				)
				
				continue()
			)
			
			--If we have got this far we must have one object
			SourceMatrix = LODObject.SourceObjectTransform
			ObjectMatrix = Matrix3 1 
			ObjectMatrix.Row1 = [ SourceMatrix.A.X, SourceMatrix.A.Y, SourceMatrix.A.Z ]
			ObjectMatrix.Row2 = [ SourceMatrix.B.X, SourceMatrix.B.Y, SourceMatrix.B.Z ]
			ObjectMatrix.Row3 = [ SourceMatrix.C.X, SourceMatrix.C.Y, SourceMatrix.C.Z ]
			ObjectMatrix.Row4 = [ SourceMatrix.D.X, SourceMatrix.D.Y, SourceMatrix.D.Z ]
			
			SourceObject = Objects[Objects.Count]
			SourceObject.Transform = ObjectMatrix
			Unhide SourceObject
		)
		
		return "FINISHED"
	),
	
	/*------------------------------------------------------------------------------
	Intializes the tool
	*/------------------------------------------------------------------------------
	on Create do
	(
		--Create an instance of the 'Painter' class passing in the relevant file paths
		this.Core = DotNetObject "LODBrowserSystem.LODBrowser"
		
		--Setup our tool window and open it
		this.ToolWindow = RSToolWindow Name:"LOD Browser" UISource:UIMarkupFilePath
		
		--Bind to our view
		this.Core.BindView this.ToolWindow.Content
		
		--Open the window
		this.ToolWindow.Open()
	)
)
LODBrowser = LODBrowser()

/*------------------------------------------------------------------------------
Action when the user requests to merge in selected LODObject
*/------------------------------------------------------------------------------
fn LODBrowser_OnRequestMergeLODObjects s e =
(
	print "Merging objects"	
	LODBrowser.MergeLODObjects e.TargetLODObjects
)
DotNet.AddEventHandler LODBrowser.Core "RequestMergeLODObjects" LODBrowser_OnRequestMergeLODObjects
