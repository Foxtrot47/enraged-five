--/////////////////////////////////////////
--	UI
--/////////////////////////////////////////
try(destroyDialog MiniMapSlicerUI)catch()
rollout MiniMapSlicerUI "MiniMap Slicer" width:200 height:270
(
	--/////////////////////////////////////////
	--	VARIABLES
	--/////////////////////////////////////////
	local mapBounds = #([-4500, -4500], [4900, 8000])
	local tileOrigin = [-4500, 8000]	--top left. | +x-->right  | -y-->down
	--local tileSizeX = 192.13
	local superTilesX = 8
	local tileSizeX = 9400 / superTilesX
	local maxTilesX = 48
	local defaultTilesX = 3
	
	--local tileSizeY = 213.48
	local superTilesY = 9
	local tileSizeY = 12500 / superTilesY
	local maxTilesY = 56
	local defaultTilesY = 3
	
	local mapTilesCount = undefined
	local meshVertPos = #()

	--/////////////////////////////////////////
	--	CONTROLS
	--/////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:MiniMapSlicerUI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"CHANGEME" filename:(getThisScriptFilename())

	group "Map SuperTile:"
	(
		spinner spnMapSuperTileX "Tile X:" range:[0, (superTilesX - 1), 0 ] type:#integer align:#left width:60
		spinner spnMapSuperTileY "Tile Y:" range:[0, (superTilesY - 1), 0 ] type:#integer align:#left across:2
		checkbutton chkMapToggle "All Tiles" offset:[0, -22] width:60 height:40
		button btnBackMapSlice "Background SuperTile" width:(MiniMapSlicerUI.width - 20)
		button btnForeMapSlice "ForeGround SuperTile" width:(MiniMapSlicerUI.width - 20)
	)
	
	group "Super Tile Slices:"
	(
		spinner spnSuperTileX "Tiles X:" range:[0, maxTilesX, defaultTilesX ] type:#integer align:#left
		spinner spnSuperTileY "Tiles Y:" range:[0, maxTilesX, defaultTilesX ] type:#integer align:#left
		button btnSlice "Slice SuperTile" width:(MiniMapSlicerUI.width - 20)
	)
	
	progressBar prgBar color:orange
	
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn getMapTilesCount =
	(
		local tileCountX = ((mapBounds[2].x - mapBounds[1].x) / tileSizeX) as Integer
		local tileCountY = ((mapBounds[2].y - mapBounds[1].y) / tileSizeY) as Integer
		
		mapTilesCount = DataPair x:tileCountX y:tileCountY
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn popTileTest XTile YTile = 
	(
		local tileLeft = tileOrigin.x + (XTile * tileSizeX)
		local tileRight = tileLeft + tileSizeX
		local tileUp = tileOrigin.y - (YTile * tileSizeY)
		local tileDown = tileUp - tileSizeY
		
		for mv in meshVertPos do
		(
			if (mv.x > tileLeft) and (mv.x < tileRight) and (mv.y < tileUp) and (mv.y > tileDown) do return true
		)
		
		return false
		
	)
	
	--/////////////////////////////////////////
	--the slicer
	--/////////////////////////////////////////
	fn addSlice obj posX posY orientation =
	(
		
		undo off
		(
			local objTM = obj.objectTransform
			
			local sliceMod = SliceModifier()
			addModifier obj sliceMod
			local modTM = getModContextTM obj sliceMod
			
			sliceMod.Slice_Type = 2 --remove top
		
			case orientation of
			(
				#left:sliceMod.Slice_Plane.Rotation = (quat 0 -0.707107 0 0.707107)
				#right:sliceMod.Slice_Plane.Rotation = (quat 0 0.707107 0 0.707107)
				#up:sliceMod.Slice_Plane.Rotation = (quat -0.707107 0 0 0.707107)
				#down:sliceMod.Slice_Plane.Rotation = (quat 0.707107 0 0 0.707107)
			)
			
			sliceMod.Slice_Plane.Position = [posX, posY, 0] * modTM * (inverse objTM)
		)
		
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn sliceSuperTile obj XTile YTile nameRoot:"" =
	(
		local UISuperTileX = spnSuperTileX.value
		local UISuperTileY = spnSuperTileY.value
		
		local superTilePosX = tileOrigin.x + (XTile * tileSizeX)
		local superTilePosY = tileOrigin.y - (YTile * tileSizeY)
		
		local tileX = tileSizeX / (UISuperTileX as Float)
		local tileY = tileSizeY / (UISuperTileY as Float)
		
		--superTilePosX = 0
		--superTilePosY = 0
		
		undo off
		(
			for x=0 to (UISuperTileX - 1) do
			(
				for y=0 to (UISuperTileY - 1) do
				(
					local posX = superTilePosX + (x * tileX)
					local posY = superTilePosY - (y * tileY)
					local newPivot = [posX, posY, 0]
						
					--create box
					local tileBox = Box width:tileX length:tileY height:50
					tileBox.pos = [(posX + (0.5 * tileX)), (posY - (0.5 * tileY)), -25]
					
					--do the bool
					tileBox * obj
					
					if (tileBox.numFaces == 0) then
					(
						delete tileBox
					)
					else
					(
						meshop.deleteIsoVerts tileBox
						
						--set pivot
						tileBox.pivot = newPivot
						ResetXForm tileBox
						
						--collapse 
						collapseStack tileBox
						
						--name it
						local nameX = (x as Integer) as String
						local nameY = (y as Integer) as String
						tileBox.name = (nameRoot + "_Tile_" + nameX + "_" + nameY)
					)
				)
			)
		)
	)
	
	--/////////////////////////////////////////
	-- Chop a super tile out of the map
	--/////////////////////////////////////////
	fn getSuperTile obj XTile YTile name:"" =
	(
		--left
		local posX = mapBounds[1].x + (XTile * tileSizeX)
		local posY = mapBounds[2].y - (YTile * tileSizeY)
		local newPivot = [posX, posY, 0]
		
		undo off
		(
			--create box
			local tileBox = Box width:tileSizeX length:tileSizeY height:50
			tileBox.pos = [(posX + (0.5 * tileSizeX)), (posY - (0.5 * tileSizeY)), -25]
			
			--do the bool
			tileBox * obj
				
			if (tileBox.numFaces == 0) then
			(
				delete tileBox
			)
			else
			(
				meshop.deleteIsoVerts tileBox
				
				--set pivot
				tileBox.pivot = newPivot
				ResetXForm tileBox
				
				--collapse 
				collapseStack tileBox
				
				--rename
				tileBox.name = ("SuperTile_" + name + "_" + (XTile as String) + "_" + (YTile as String))
			)
		)
		
	)
	
	--/////////////////////////////////////////
	--	EVENTS
	--/////////////////////////////////////////
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on chkMapToggle changed active do
	(
		if active then
		(
			spnMapSuperTileX.enabled = false
			spnMapSuperTileY.enabled = false
		)
		else
		(
			spnMapSuperTileX.enabled = true
			spnMapSuperTileY.enabled = true
		)
	)
	
	--/////////////////////////////////////////
	-- background name superTile
	--/////////////////////////////////////////
	on btnBackMapSlice pressed do
	(
		if (selection.count == 0) do
		(
			messageBox "Nothing selected" title:"No Selection"
			return ok
		)
		
		coordsys #world
		meshVertPos = for v in selection[1].mesh.verts collect meshop.getvert selection[1] v.index node:selection[1]
		
		if chkMapToggle.state == true then
		(
			escapeenable = true
			prgBar.value = 1
			for x=0 to (superTilesX - 1) do
			(
				for y=0 to (superTilesY - 1) do
				(
					getSuperTile selection[1] x y name:"Back"

					
					prgBar.value = (100.0 * ((x * superTilesX) + y) / (superTilesX * superTilesY) as Float )
					format "x:% y:% \n" (x as String) (y as String)
				)
			)
			prgBar.value = 0
		)
		else
		(
			getSuperTile selection[1] spnMapSuperTileX.value spnMapSuperTileY.value name:"Back"
		)
	)
	
	--/////////////////////////////////////////
	-- foreground name superTile
	--/////////////////////////////////////////
	on btnForeMapSlice pressed do
	(
		if (selection.count == 0) do
		(
			messageBox "Nothing selected" title:"No Selection"
			return ok
		)
		
		coordsys #world
		meshVertPos = for v in selection[1].mesh.verts collect meshop.getvert selection[1] v.index node:selection[1]
		
		if chkMapToggle.state == true then
		(
			escapeenable = true
			prgBar.value = 1
			for x=0 to (superTilesX - 1) do
			(
				for y=0 to (superTilesY - 1) do
				(
					getSuperTile selection[1] x y name:"Fore"
					
					prgBar.value = (100.0 * ((x * superTilesX) + y) / (superTilesX * superTilesY) as Float )
					format "x:% y:% \n" (x as String) (y as String)
				)
			)
			prgBar.value = 0
		)
		else
		(
			getSuperTile selection[1] spnMapSuperTileX.value spnMapSuperTileY.value name:"Fore"
		)
		
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btnSlice pressed do
	(
		if (selection.count == 0) do
		(
			messageBox "Nothing selected" title:"No Selection"
			return ok
		)
	
		for tile in selection do
		(
			--work out its tile index
			local indX = (abs(tileOrigin.x - tile.pos.x) / tileSizeX) as Integer
			local indY = (abs(tileOrigin.y - tile.pos.y) / tileSizeY) as Integer
			
			sliceSuperTile tile indX indY nameRoot:tile.name
		)
	)
	
	
	--/////////////////////////////////////////
	-- init
	--/////////////////////////////////////////
	on MiniMapSlicerUI open do
	(
		banner.setup()
	)
)
createDialog MiniMapSlicerUI
