filein (RsConfigGetWildWestDir() + "script/3dsmax/maps/LODBaker_Core.ms")

rollout LODBakeMachine "Bake Machine"
(
	edittext edtMachineName
	button btnConfirm "Confirm"
	
	on btnConfirm pressed do
	(
		local theMachine = edtMachineName.text
		append ::LODBakerBatchUI.machines theMachine
		append ::LODBakerBatchUI.jobs (DataPair machine:theMachine jobs:#())
		::LODBakerBatchUI.lbMachines.items = ::LODBakerBatchUI.machines
		destroyDialog LODBakeMachine
	)
	
)

rollout LODBakerBatchUI "Batch Manager"
(
	--//////////////////////////////////////////////////////
	--	Vars
	--//////////////////////////////////////////////////////
	local machines = #()
	local jobs = #()
	local scenePool = #()
	local assignedJobs = #()
	
	
	--//////////////////////////////////////////////////////
	--	UI
	--//////////////////////////////////////////////////////
	label lblMachines "Machines" align:#left across:3
	label lblJobs "Jobs" offset:[-170, 0]
	label lblPool "Scene Pool" offset:[-75, 0]
	
	multilistbox lbMachines items:machines width:100 height:20 across:5
	multilistbox mlbJobs items:jobs width:200 height:20 offset:[-10, 0]
	button btnInPool "-->" offset:[55, 60]
	button btnOutPool "<--" offset:[-60, 140]
	multilistbox mlbScenePool items:scenePool width:200 height:20 offset:[-80, 0]
	
	button btnAddMachine "Add" across:3 align:#left
	button btnRemoveMachine "Remove" offset:[-220, 0]
	
	button btnShareJobs "Share Jobs" offset:[-70, 0]
	
	--button btnAddScene "Add" offset:[35, 0]
	--button btnRemoveScene "Remove" offset:[-55, 0]
	
	button btnSaveIni "Save Config" width:550
	
	--//////////////////////////////////////////////////////
	--	Functions
	--//////////////////////////////////////////////////////
	
	--//////////////////////////////////////////////////////
	--Events dear boy
	--//////////////////////////////////////////////////////
	on LODBakerBatchUI open do
	(
		--sync the ini file
		if gRsPerforce.connected() == false then gRsPerforce.Connect()
		gRsPerforce.run "sync" #(LODBaker.iniPath)
		
		--read the ini file
		local config = getIniSetting LODBaker.iniPath
		
		--populate ui arrays with whats there
		machines = for machine in (getIniSetting LODBaker.iniPath "MACHINEPOOL") collect getIniSetting LODBaker.iniPath "MACHINEPOOL" machine
		jobs = for machine in machines collect (dataPair machine:machine jobs:(getIniSetting LODBaker.iniPath machine))
		
		--update scenePool with whats in p4
		scenePool = for f in (getDepotFiles (RsConfigGetArtDir() + "Models/Handy objects/LODBakes") wildcard:"*.max" walk:true)
						where f.action != "delete" collect f.depotFile
		
		--populate UI fields
		lbMachines.items = machines
		mlbScenePool.items = for f in scenePool collect getFileNameFile f
		
/*		
		if lbMachines.selection.count > 0 then
		(
			local selectedMachine = lbMachines.items[(lbMachines.selection as Array)[1]]
			if selectedMachine != undefined then
			(
				mlbJobs.items = (for item in jobs where item.machine == selectedMachine collect item.jobs)[1]
			)
		)
*/		
		lbMachines.selection = 1
	)
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	fn lbMachines_Selected arg =
	(
		local machineSelection = lbMachines.selection as Array
		if machineSelection.count == 1 then
		(
			local selectedMachine = lbMachines.items[machineSelection[1]]
			--format "selectedMachine: % \n" selectedMachine
			local machineJobs = (for item in jobs where item.machine == selectedMachine collect item.jobs)[1]
			--format "machineJobs: % \n" machineJobs
			if machineJobs != undefined then
			(
				mlbJobs.items = for f in machineJobs collect getFileNameFile f
			)
		)
		else
		(
			mlbJobs.items = #()
		)
	)
	
	on lbMachines selected arg do lbMachines_Selected arg 
		
	--//////////////////////////////////////////////////////
	-- remove jobs from machine
	--//////////////////////////////////////////////////////
	on btnInPool pressed do
	(
		--remove them from the machine jobs list
		local machineSelection = lbMachines.selection as Array
		local machine = lbMachines.items[machineSelection[1]]
		local machineJobsIdx = (for i=1 to jobs.count where machine == jobs[i].machine collect i)[1]
		local machineJobs = jobs[machineJobsIdx].jobs
		
		--get assigned jobs selected
		local selectedJobs = for it in mlbJobs.selection collect machineJobs[it]
		format "selectedJobs: % \n" selectedJobs
		
		--remove selected jobs from machine jobs
		--local cleanedJobs = #()
		for sj in selectedJobs do
		(
			local foundIdx = findItem machineJobs sj
			if foundIdx != 0 then deleteItem machineJobs foundIdx
				
			--remove from assigned jobs
			local foundIdx = findItem assignedJobs sj
			if foundIdx != 0 then deleteItem assignedJobs foundIdx
		)
		format "machineJobs: % \n" machineJobs
		jobs[machineJobsIdx].jobs = machineJobs
	)
	
	--//////////////////////////////////////////////////////
	-- assign jobs to a machine
	--//////////////////////////////////////////////////////
	on btnOutPool pressed do
	(
		--get the selected scenes
		local scenePoolSelectionIdx = mlbScenePool.selection as Array
		local scenePoolSelection = for i in scenePoolSelectionIdx collect scenePool[i]
			
		--assign them to the jobs array for the machine
		local machineSelection = lbMachines.selection as Array
		local machine = lbMachines.items[machineSelection[1]]
		local machineJobsIdx = (for i=1 to jobs.count where machine == jobs[i].machine collect i)[1]
		local machineJobs = jobs[machineJobsIdx].jobs
		
		--check if tis already assigned
		local cleanScenePoolSelection = #()
		for scn=1 to scenePoolSelection.count do
		(
			if findItem assignedJobs scenePoolSelection[scn] == 0 then
			(
				append cleanScenePoolSelection scenePoolSelection[scn]
			)				
		)
			
		join machineJobs cleanScenePoolSelection
		jobs[machineJobsIdx].jobs = machineJobs
		
		--add to assigned Jobs
		join assignedJobs cleanScenePoolSelection
		
		mlbJobs.items = for f in machineJobs collect getFileNameFile f
		
	)
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	on btnAddMachine pressed do
	(
		createDialog LODBakeMachine
	)
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	on btnRemoveMachine pressed do
	(
		if queryBox "Are you sure you want to delete this machine and any of its jobs?" then
		(
			local Idx = findItem machines lbMachines.selected
			if Idx != 0 then
			(
				deleteItem machines Idx
				local jobsClone = deepcopy jobs
				for i=1 to jobs.count where jobs[i].machine == lbMachines.selected do deleteItem jobsClone i
				jobs = jobsClone
				lbMachines.items = machines
			)
			lbMachines.selection = 1
		)
	)
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	on btnShareJobs pressed do
	(
		--check we have multi selections for machines and for scenepool items
		if lbMachines.selection.count < 2 or mlbScenePool.selection.count < 2 then
		(
			messagebox "You need multiple selections for machines and scenepool to share them out" title:"Error"
			return false
		)
		
		--how many scene to dish out?
		local theScenes = for item in (mlbScenePool.selection as Array) collect scenePool[item]
		local theMachines = for item in (lbMachines.selection as Array) collect lbMachines.items[item]
		
		local sceneIdx = theScenes.count
		--format "sceneIdx: % \n" sceneIdx
		local machineIdx = 1
		while sceneIdx != 0 do
		(
			--assign the scene to the machine
			local theMachine = theMachines[machineIdx]
			local machineJobsIdx = (for i=1 to jobs.count where theMachine == jobs[i].machine collect i)[1]
			local machineJobs = jobs[machineJobsIdx].jobs
			
			--append machineJobs theScenes[sceneIdx]
			append jobs[machineJobsIdx].jobs theScenes[sceneIdx]
			
			--inc machineIdx
			if machineIdx != theMachines.count then machineIdx +=1 else machineIdx = 1
			--dec sceneIdx
			sceneIdx -= 1
		)
	)
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	on btnSaveIni pressed do
	(
		for theMachine in machines do
		(
			--delete the current jobs
			delIniSetting LODBaker.iniPath theMachine
			
			--get the jobs
			local theMachineJobs = (for item in jobs where item.machine == theMachine collect item.jobs)[1]
			for job in theMachineJobs do
			(
				local formattedJob = (filterString (substituteString job "//depot/gta5/art/Models/Handy objects/LodBakes/" "") ".")[1]
				setIniSetting LODBaker.iniPath theMachine formattedJob "1024 1024"
			)
			
			--add any new machines to the machine pool
			local theINIMachines = getIniSetting LODBaker.iniPath "MACHINEPOOL"
			--format "theINIMachines: % \n" theINIMachines
			if machines.count != theINIMachines.count then
			(
				for idx = (theINIMachines.count + 1) to machines.count do
				(
					setIniSetting LODBaker.iniPath "MACHINEPOOL" ("machine"+(idx as String)) machines[idx]
				)
			)
		)
	)
)

try(DestroyDialog LODBakerUI)catch()
rollout LODBakerUI "LOD Baker"
(
	--//////////////////////////////////////////////////////
	--	Vars
	--//////////////////////////////////////////////////////
	local __DEBUG__ = true
	
	--local defaultBackGroundColour = color 255 0 255
	
	--//////////////////////////////////////////////////////
	--	UI
	--//////////////////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:LODBakerUI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"LODBaker" versionNum:1.01 versionName:"Frightful Heir" filename:(getThisScriptFilename())
	
	group "Settings:"
	(
		label lblTxScale "Scene Texture Scale:" across:2 align:#left offset:[0, 3]
		dropdownlist ddlTxScale items:#("100%", "75%", "50%", "25%", "12.5%") tooltip:"Texture Scale"
		
		label lblBakeRes "Bake Size X: " across:4 offset:[0, 3]
		dropdownlist ddlMapX items:LODBaker.res tooltip:"Map Width" offset:[5, 0]
		label lblBakeY "Y:" offset:[0, 3]
		dropdownlist ddlMapY items:LODBaker.res tooltip:"Map Height" offset:[-20, 0]
		
		colorpicker cpSetBackgroundColour "Background Colour: " color:LODBaker.defaultBackGroundColour title:"Choose New BackGround Colour"
		
		label lblPath "Path:" across:3 align:#left width:40
		edittext edtPath width:160 offset:[-50, 0] tooltip:"Map path"
		button btnBrowse "..." align:#right tooltip:"Browse path"
		
	)
	
	group ""
	(
		button btnBake "Bake!" width:230 tooltip:"Bake the map"
		button btnShowLast "Show last bake" width:230 tooltip:"Show map set in the path field"
		button btnBatchSetup "Batch Setup" width:230 tooltip:"Setup multiple bakes" 
	)
	
	
	--//////////////////////////////////////////////////////
	--Events dear boy
	--//////////////////////////////////////////////////////
	on LODBakerUI open do
	(
		banner.setup()
		LODBaker.mapRes_X = LODBaker.res[ddlMapX.selection] as Float
		LODBaker.mapRes_Y = LODBaker.res[ddlMapY.selection] as Float
		
		if gRsIsOutSource do btnBatchSetup.enabled = false
	)
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	on LODBakerUI close do
	(
		try(DestroyDialog LODBakerBatchUI)catch()
	)
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	fn ddlMapResChanged arg dim =
	(
		case dim of
		(
			"x":
			( 
				LODBaker.mapRes_X = LODBaker.res[arg] as Float
				ddlMapY.selection = arg
				LODBaker.mapRes_Y = LODBaker.res[arg] as Float
			)
			"y":LODBaker.mapRes_Y = LODBaker.res[arg] as Float
		)
	)
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	on ddlMapX selected arg do ddlMapResChanged arg "x"
	on ddlMapY selected arg do ddlMapResChanged arg "y"
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	fn ddlTxScaleChanged arg =
	(
		case (arg as String) of
		(
			"1":LODBaker.txScale = "To100pcScene"
			"2":LODBaker.txScale = "To75pcScene"
			"3":LODBaker.txScale = "To50pcScene"
			"4":LODBaker.txScale = "To25pcScene"
			"5":LODBaker.txScale = "To12pcScene"
		)
	)
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	on ddlTxScale selected arg do ddlTxScaleChanged arg
		
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	on btnBrowse pressed do
	(
		local chosenPath = getSaveFileName caption:"LODMap save location" filename:LODBaker.bakePath types:"BMP(*.bmp)|*.bmp"
		if chosenPath != undefined then
		(
			chosenPath = RsMakeSafeSlashes chosenPath
			LODBaker.bakePath = chosenPath
			edtPath.text = chosenPath
		)
	)
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	on btnBake pressed do
	(
		LODBaker.createBakeLog()
		LODBaker.doTheBake mode:"manual"
		closeLog LODBaker.processLog
	)
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	on btnBatchSetup pressed do
	(
		if gRsIsOutSource do return false
			
		try(DestroyDialog LODBakerBatchUI)catch()
		createDialog LODBakerBatchUI width:600 height:350
	)
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	on btnShowLast pressed do
	(
		last = openbitmap LODBaker.bakePath
		display last
	)
	
	--//////////////////////////////////////////////////////
	-- Set the new backgroundcolour for the
	-- LOD bake
	--//////////////////////////////////////////////////////
	on cpSetBackgroundColour changed col do
	(
		LODBaker.defaultBackGroundColour = col
	)
)

createDialog LODBakerUI width:250 height:265
