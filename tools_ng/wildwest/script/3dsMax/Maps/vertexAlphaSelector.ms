--Modified version for R* by Neil Gregory
--original http://scriptspot.com/3ds-max/scripts/vertex-alpha-selector

filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

global My_alpha_test()
global My_alpha_select_verts()

if List_win_alpha != undefined then
( 
	callbacks.removeScripts id:#alpha_test_1
	destroydialog List_win_alpha
)

--==============================================================================
rollout List_win_alpha "Vertex Alpha Selector" width:150 height:350
(
	hyperlink help "help" address:"https://devstar.rockstargames.com/wiki/index.php/Vertex_Alpha_Selector" align:#right
	group "Display"
	(
		Checkbox btnAlpha "A" checked:false    tooltip:"display vertex Alpha" across:3
		Checkbox btnColour "C" checked:false     tooltip:"display vertex Color"
		Checkbox btnNormal "none"  checked:false tooltip:"display normal" 
	)
	
	group "Selection"
	(
		multiListBox lbx_1  across:2
		button btnSelect "Select" width:42 height:80  toolTip:"Select vertices"
		colorpicker cpPick "pick" color:[255, 255, 255]
	)
	
	group "Adjust"
	(
		spinner spn_1 "Value: "  fieldWidth:50 range:[0.0,100.0,0.0] type:#float scale:1.0 align:#left

		--button btn5 "integer" pos:[76,120] width:42 height:32 toolTip:"Integer alpha, 12.2563 --> 12.0"
		
		dropdownList dd_1 "object : " width:90 height:15 items:#("")
	)

--##########################################################################################
	on List_win_alpha open do
	(	
		callbacks.removeScripts id:#alpha_test_1
		callbacks.addScript #selectionSetChanged "My_alpha_test()" id:#alpha_test_1
		try (select selection)catch()
	)

	on List_win_alpha close do
	( 	
		callbacks.removeScripts id:#alpha_test_1	
	)
	
	on btnSelect pressed  do
	(  
		/*
		if classOf selection[1] != Editable_poly then
		(
			q = queryBox "Needs to be Editable_Poly, convert?" title:"Object Type"
			if q == true then
			(
				addModifier selection[1] (Edit_Poly())
				collapseStack selection[1]
			)
			else if q == false then
			(
				return false
			)
		)
		*/
		subobjectLevel = 1
		setwaitcursor()
		--set the value spinner to the current selection if one item selected
		aSel = lbx_1.selection  as array
		items = lbx_1.items as array
		
		if aSel.count == 1 then
		(
			spn_1.value = items[aSel[1]] as float
		)
		
		--do the do
		My_alpha_select_verts lbx_1.selection 
	)

	on btnAlpha changed checked  do 
	(   
		btnAlpha.checked = btnColour.checked = btnNormal.checked = false
	    btnAlpha.checked = true
		selection.vertexColorType = 2
		selection.showVertexColors=on
		selection.vertexColorsShaded=false
		completeredraw() 	
	)
	on btnColour changed checked do 
	( 
		btnAlpha.checked = btnColour.checked = btnNormal.checked = false
		btn3.checked = true
	    selection.vertexColorType = 0
		selection.showVertexColors=on
		selection.vertexColorsShaded=false
		completeredraw()
	)
	on btnNormal changed checked do 
	( 
		btnAlpha.checked = btnColour.checked = btnNormal.checked = false
		btn4.checked = true
	    selection.vertexColorType = 0
		selection.showVertexColors=off
		selection.vertexColorsShaded=false
		completeredraw()
	)
	
	on btn5 pressed  do
	(
		for nn=1 to selection.count do
		(
			for tt=1 to selection[nn].verts.count do
			( alpha_integer=(polyOp.getMapVert selection[nn] -2 tt )[1] 
			  alpha_integer=(alpha_integer * 100.0) as integer
			  alpha_integer=alpha_integer/100.0
			  polyOp.setMapVert selection[nn] -2 tt [alpha_integer,alpha_integer,alpha_integer]
			)  
		)	
		select selection
	--	messagebox "alpha_integer complete!!"
	)
	
	--ColorPicker Change Event
	on cpPick changed arg do
	(
		--take first component of arg as alpha only
		--find the closest match in the list
		
		picked = arg.r as Integer
		items = lbx_1.items as array
		
		idx = findItem items picked
		
		/*
		if idx != 0 then
		(
			lbx_1.selection = #{idx}
		)
		*/
		--find closest match
		
		minDiff = 9999
		idx = 99
		for i=1 to items.count do
		(
			ItemInt = (items[i] as float * 2.55) as Integer
			
			--Find the diff and check against closest
			diff = abs(ItemInt - picked)
			if diff < minDiff then
			(
				minDiff = diff
				idx = i
			)
		)
		--Now we have the closest
		lbx_1.selection  = #{idx}
		--set the value to the selection val
		spn_1.value = items[idx] as float
		
	)
	
--***************************************************************************************	
    --apply new values
	on spn_1 entered  do 
	(
		setwaitcursor()
		new_alpha_value = spn_1.value * 0.01
		new_alpha_value = [new_alpha_value, new_alpha_value, new_alpha_value]
		for nn in selection do
		(	
			--go through mapverts checking aginst value apply new cvalue to matches
			case (classOf nn) of
			(
				Editable_Poly:
				(
					polyOp_getMapVert = polyOp.getMapVert
					currentAlpha = for i = 1 to (polyop.getNumMapVerts nn -2) collect (( polyOp_getMapVert nn -2 i)[1] * 100.0)
					listValuesCount = lbx_1.selection.count
					listSelection =  lbx_1.selection as array
					
					for lv = 1 to listSelection.count do
					(	
						listValue = lbx_1.items[listSelection[lv]] as Float
						for v = 1 to currentAlpha.count do
						(
							diff = abs (currentAlpha[v] - listValue)
							if diff < 0.3921 then
							(
								--format "listValue: % currentAlpha: % diff: %\n " listValue currentAlpha[v] diff
								polyOp.setMapVert nn -2 v new_alpha_value
							)
						)
					)
					--polyOp.setVertColor nn -2 (nn.selectedVerts) new_alpha_value  
				)
				
				Editable_Mesh:
				(
					meshOp_getMapVert = meshOp.getMapVert
					currentAlpha = for i = 1 to (meshop.getNumMapVerts nn -2) collect (( meshOp_getMapVert nn -2 i)[1] * 100.0)
					listValuesCount = lbx_1.selection.count
					listSelection =  lbx_1.selection as array
					
					for lv = 1 to listSelection.count do
					(	
						listValue = lbx_1.items[listSelection[lv]] as Float
						for v = 1 to currentAlpha.count do
						(
							diff = abs (currentAlpha[v] - listValue)
							if diff < 0.3921 then		-- 100 / 255 = 0.3921
							(
								--format "listValue: % currentAlpha: % diff: %\n " listValue currentAlpha[v] diff
								meshOp.setMapVert nn -2 v new_alpha_value
							)
						)
					)
				)
			)
		)
		--Clear list selection
		lbx_1.selection = #()
		
		My_alpha_test()
		
		--re-select verts of new value
		idx = findItem lbx_1.items (spn_1.value as String)
		lbx_1.selection = idx
		ba = #{}
		if idx != 0 then append ba idx
		My_alpha_select_verts ba
		
		for ii in selection do update ii
		
	) -- on
--***************************************************************************************
	on dd_1 selected i do
	(
		try (
				select selection[i]
			)
		catch()
	)
)
--########################################################################
--#####																######
--#####																######
--########################################################################

fn My_alpha_select_verts item_number =        
(
	setwaitcursor()
	max modify mode
	if selection.count == 0 then
	(
		List_win_alpha.lbx_1.items =#()
		List_win_alpha.dd_1.items  =#(" ") 
		List_win_alpha.dd_1.caption=""
	)
	else
	(
		
		current_alpha_array = #() 
		item_content = #()
		--convertToPoly selection
		for g = 1 to item_number.count do
		(
			if item_number[g] == true then ( append item_content (List_win_alpha.lbx_1.items[g] as float) )  )
			--subobjectLevel = 1
			-------------------------------------------------
			for nn = 1 to selection.count do
			(
				current_alpha_array = #()
				all_vertexs = 0
				
				case (classOf selection[nn].baseobject ) of
				(
					Editable_Poly:
					(
						polyOp.setMapSupport selection[nn] -2 true
						all_vertexs=polyop.getNumMapVerts selection[nn] -2
					)
					
					Editable_mesh:
					(
						meshOp.setMapSupport selection[nn] -2 true
						all_vertexs=meshOp.getNumMapVerts selection[nn] -2
					)
				)
				
				must_select_verts = #{}
				must_select_verts[all_vertexs] = false
				xx = #{}	--the selected verts array
				
				for h =1 to item_content.count do
				(
					vv= ( item_content[h] ) * 2.55
					case (classOf selection[nn].baseobject ) of
					(
						Editable_Poly:
						(
							xx+=polyop.getVertsByColor selection[nn] (color vv vv vv) 0.01 0.01 0.01 channel:-2
						)
						
						Editable_mesh:
						(
							--selectMapVerts = #()
							--each map face
							--test mapverts against colour
							--pass test then find geo vert
							--append to array
							numMapFaces = meshOp.getNumMapFaces selection[nn] -2
							for mf = 1 to numMapFaces do
							(
								mv = meshOp.getMapFace selection[nn] -2 mf
								mv = #(mv.x as Integer, mv.y as Integer, mv.z as Integer)
								--check colour
								for v = 1 to mv.count do
								(
									col = (meshOp.getMapVert selection[nn] -2 mv[v])[1]
									diff = abs (col - (0.01 * item_content[h]))
									--if diff < 0.25 then	format "v: % col: % item: % diff: % \n" v col (0.01 * item_content[h]) diff
									if diff < 0.003921 then
									(
										--find its geo vert counterpart
										--v is the mapvert index
										face = getFace selection[nn] mf
										face = #(face.x as Integer, face.y as Integer, face.z as Integer)
										append xx face[v]
									)
								)
							)
							--xx+=meshop.getVertsByColor selection[nn] (color vv vv vv) 0.01 0.01 0.01 channel:-2
						)
					)
					
				)
				select selection[nn].verts[xx]
			)--for nn
		
		--modPanel.addModToSelection (Edit_Mesh ()) ui:on
		--subobjectLevel = 1
	)--else 
	completeRedraw()		
)-- fn

--########################################################################################
--#####																				######
--#####																				######
--########################################################################################
fn My_alpha_test =
(
		
	alpha_array=#()
	uniqueAlphaValues = #()
	--print $selection[1].name
	sel = $selection
	--check for no selection
	if sel.count == 0 then
	( 	
		List_win_alpha.lbx_1.items	  =#()
		List_win_alpha.dd_1.items  =#(" ") 
		List_win_alpha.dd_1.caption=""
	)
	else
	( 
		--Add the object names to the ui dropdown
		obj_name = #()
		for i =1 to $selection.count do
		(
			obj_name[i] = selection[i].name
		)
		List_win_alpha.dd_1.items = obj_name
		List_win_alpha.dd_1.caption = ($selection.count as string)+" obj"
		
		current_alpha_array=#()
		--diffent_alpha=#()
		
		for nn in sel do
		(
			current_alpha_array=#()
			case (classOf nn.baseobject) of
			(
				Editable_Poly:
				( 
					polyOp.setMapSupport nn -2 true
					numMapVerts = polyop.getNumMapVerts nn -2
					polyOp_getMapVert = polyOp.getMapVert
					current_alpha_array = for i = 1 to numMapVerts collect (( polyOp_getMapVert nn -2 i)[1] * 100.0)	--get the alpha as a percentag
					sort current_alpha_array
				)

				Editable_mesh:
				(
					meshOp.setMapSupport nn -2 true
					all_vertexs=meshop.getNumMapVerts nn -2
					for i = 1 to  all_vertexs do
					(
						current_alpha_array[i] = ( meshOp.getMapVert nn -2 i)[1] * 100.0
					)
					sort current_alpha_array
				)
			
				default:
				(
					--all_vertexs=0
					current_alpha_array=#()
				)
			)--end case
			
			for a in current_alpha_array do appendIfUnique uniqueAlphaValues (a as String)
			--sort uniqueAlphaValues
			
			--alpha_array += sss
		)--for	
		
		List_win_alpha.lbx_1.items = uniqueAlphaValues
		
		
		--My_alpha_select_verts( List_win_alpha.lbx_1.selection )
	)--else 
		
)-- fn
--########################################################################
--#####		start													######
--########################################################################
	
options.printAllElements = true
pos_a=[200,150]
createdialog List_win_alpha pos:pos_a  fgcolor:(color 250 250 0) \
style:#(#style_titlebar, #style_border, #style_sysmenu,#style_minimizebox) \
escapeEnable:true
try (select selection)catch()
	
   

