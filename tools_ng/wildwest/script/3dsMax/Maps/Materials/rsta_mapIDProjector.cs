﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace rsta.mapIDProjecor
{
	//========================================================================================================================================
	//
	// CLASS - 
	//
	//========================================================================================================================================

    class mapIdProjector
	{
		public List<mapIDColor> colorList = new List<mapIDColor>();
		public projectorMesh pMesh = new projectorMesh();
		public Color match_colour = new Color();
		public int tolerance;

		public Bitmap image;
		public String image_file;

		//----------------------------------------------------------------------------------------------------------------
		// 
		//----------------------------------------------------------------------------------------------------------------
        public void set_image(string image_file_name)
		{
			image_file =  image_file_name;
            image = new Bitmap(image_file_name);
        }

        //----------------------------------------------------------------------------------------------------------------
        // 
        //----------------------------------------------------------------------------------------------------------------
        public void add_color(int id, int R, int G, int B)
        {
            mapIDColor mID = new mapIDColor() { ID = id, color = Color.FromArgb(R, G, B) };
            colorList.Add(mID);
        }

        public void set_match_colour_tol (int R, int G, int B, int Tol)
        {
            match_colour = Color.FromArgb(R, G, B);
            tolerance = Tol;
        }

        //----------------------------------------------------------------------------------------------------------------
        // 
        //----------------------------------------------------------------------------------------------------------------
        public void clear_colors()
        {
            colorList.Clear();
        }

        //----------------------------------------------------------------------------------------------------------------
        // 
        //----------------------------------------------------------------------------------------------------------------
        private double color_distance(Color a, Color b)
        {
            float deltaX = a.R - b.R;
            float deltaY = a.G - b.G;
            float deltaZ = a.B - b.B;

            return (float)Math.Sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);
        }

        //----------------------------------------------------------------------------------------------------------------
        // 
        //----------------------------------------------------------------------------------------------------------------
        public double get_map_id(double x, double y)
        {            
            double2D offset_p = pMesh.get_offset_point(new double2D(x, y));            

            double pixel_unit_x = image.Width / pMesh.dimentions.x;
            double pixel_unit_y = image.Height / pMesh.dimentions.y;

            int pixel_x = (int)(offset_p.x * pixel_unit_x);
            if (pixel_x > image.Width-1) pixel_x = image.Width-1;
            if (pixel_x < 0) pixel_x = 0;

            int pixel_y = (int)((pMesh.dimentions.y-offset_p.y) * pixel_unit_y);
            if (pixel_y > image.Height-1) pixel_y = image.Height-1;
            if (pixel_y < 1) pixel_y = 1;

            Color pixelColor = image.GetPixel(pixel_x, pixel_y);

            double minDist = 999;
            int mapID = 0;

            foreach (mapIDColor mIDc in colorList)
            {
                double thisDist = color_distance(pixelColor, mIDc.color);
                if (thisDist < minDist)
                {
                    minDist = thisDist;
                    mapID = mIDc.ID;
                }
            }

            return mapID;
        }

        //----------------------------------------------------------------------------------------------------------------
        // 
        //----------------------------------------------------------------------------------------------------------------
        public bool get_colourMatch(double x, double y)
        {
            x = x*image.Width-1;
            y = image.Height - (y * image.Height - 1);
			
			// CLAMPS
			if (x < 0) x = 0;
			if (y < 0) y = 0;
			if (x > image.Width-1) x = image.Width-1;
			if (y > image.Height-1) y = image.Height-1;

            Color pixelColor = image.GetPixel((int)x, (int)y);
            double thisDist = color_distance(pixelColor, match_colour);

            if (thisDist < tolerance) return true;
            return false;
        }
    }


    //========================================================================================================================================
    //
    // CLASS - 
    //
    //========================================================================================================================================

    class mapIDColor
    {
        public int ID { get; set; }
        public Color color { get; set; }
    }

    //========================================================================================================================================
    //
    // CLASS - 
    //
    //========================================================================================================================================

    class projectorMesh
    {
        public double2D dimentions { get; set; }
        public double rotation { get; set; }
        public double2D offset { get; set; }
        public double2D pivot { get; set; }

        //----------------------------------------------------------------------------------------------------------------
        // 
        //----------------------------------------------------------------------------------------------------------------
        private double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        //----------------------------------------------------------------------------------------------------------------
        // 
        //----------------------------------------------------------------------------------------------------------------
        public double2D get_offset_point(double2D query_world_space_point)
        {
            double2D point_to_new_grid = query_world_space_point - pivot;
            // ROTATE AROUND PIVOT
            double2D target = new double2D(0,0);
            target.x = ((point_to_new_grid.x * Math.Cos(DegreeToRadian(rotation))) - (point_to_new_grid.y * Math.Sin(DegreeToRadian(rotation))));
            target.y = ((point_to_new_grid.y * Math.Cos(DegreeToRadian(rotation))) + (point_to_new_grid.x * Math.Sin(DegreeToRadian(rotation))));
            // ADD OFFSET BACK IN
            target -= offset;

            return target;
        }        
    }

    //========================================================================================================================================
    //
    // CLASS - 
    //
    //========================================================================================================================================

    class double2D 
    {
        public double x;
        public double y;

        // Constructor. 
        public double2D(double x, double y)  
        {
            this.x = x;
            this.y = y;
        }

        // ADD
        public static double2D operator +(double2D c1, double2D c2)
        {
            return new double2D(c1.x + c2.x, c1.y + c2.y);
        }

        // MINUS
        public static double2D operator -(double2D c1, double2D c2)
        {
            return new double2D(c1.x - c2.x, c1.y - c2.y);
        }

        // TIMES
        public static double2D operator *(double2D c1, double2D c2)
        {
            return new double2D(c1.x * c2.x, c1.y * c2.y);
        }

        public void set(double x_in, double y_in)
        {
            x = x_in;
            y = y_in;
        }
    }
}


