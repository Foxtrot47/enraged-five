FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_userSettings.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_xml.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_UV.ms")

struct rm_presets
(
	name,
	c1_u,
	c1_v,
	c2_u,
	c2_v
)

try (destroyDialog  RoadMapperWin) catch()
rollout RoadMapperWin "Road Mapper" width:200
(
	------------------------------------------------------------------
	-- LOCALS
	------------------------------------------------------------------
	local Settings = rsta_userSettings app:"RoadMapper"
	local presets = #()
	local targetBB 

	------------------------------------------------------------------
	-- FUNTIONS
	------------------------------------------------------------------
	fn getLengthFromEdge = 
	(
		local totalLength = 0
		if (selection.count != 0) then 
		(
			local theMesh = selection[1]
			if (classof_array #(editable_poly, editable_mesh) theMesh) do 
			(			
				local edges = if (classof theMesh == editable_mesh) then (getEdgeSelection theMesh) else (polyop.getEdgeSelection theMesh)				
				for theEdge in edges do 
				(
					local verts = ((rsmeshpolyop theMesh).getVertsUsingEdge theMesh theEdge) as array
					totalLength += distance ((rsmeshpolyop theMesh).getvert theMesh verts[1]) ((rsmeshpolyop theMesh).getvert theMesh verts[2])
				)
			)					
		)
		return totalLength
	)
	---------------------------------------------------------------------------
	fn getPresets =
	(
		local xmlPath = RsConfigGetWildWestDir() + @"\etc\config\maps\RoadMapper.xml"
		if (doesFileExist xmlPath) do
		(
			local xml_io = rsta_xml_io xmlFile:xmlPath	
			
			for preset_xm in (rsta_xml.getChildren xml_io.root name:"Presets/Preset") do 
			(
				local p = rm_presets()
				p.name = preset_xm.getAttribute "name"
				for item_xm in (rsta_xml.getChildren preset_xm name:"Item") do 
				(
					local propName = (item_xm.getattribute "name")
					if (hasProperty p propName) do setProperty p propName (item_xm.getattribute "value")
				)	
				
				append presets p
			)
		)
	)
	---------------------------------------------------------------------------
	fn setSpinnerCheckbox spn cbx v =
	(
		if (v == "!") then 
		(
			cbx .checked = true
			spn.enabled = false
			spn.value = 1
		)
		else 
		(
			cbx .checked = false
			spn.enabled = true
			spn.value = v as float 
		)
	)
	---------------------------------------------------------------------------
	fn setPresets arg = 
	(
		local p = presets[arg]
		local w = RoadMapperWin
		
		setSpinnerCheckbox w.spn_c1_u w.cbx_scaleToFit_u1 p.c1_u
		setSpinnerCheckbox w.spn_c1_v w.cbx_scaleToFit_v1 p.c1_v			
		setSpinnerCheckbox w.spn_c2_u w.cbx_scaleToFit_u2 p.c2_u
		setSpinnerCheckbox w.spn_c2_v w.cbx_scaleToFit_v2 p.c2_v			
	)
	---------------------------------------------------------------------------
	fn getWidthHeight bbox =
	(
		local w = (bbox[2] - bbox[1]) as Float
		local h =  (bbox[4] - bbox[3]) as Float	
		return dataPair w:w h:h
	)
	---------------------------------------------------------------------------
	fn isObjectValid = 
	(
		if (RoadMapperWin.spn_roadLength.value != 0) then 
		(	
			if (selection.count != 0) then 
			(
				local obj = selection[1]
				if (obj.modifiers[1] != undefined AND (classof obj.modifiers[1] == unwrap_uvw)) then 	
				(	
					return true
				)
				else 
				(
					local mod_uv = unwrap_uvw()
					modPanel.addModToSelection mod_uv
					subobjectLevel = 3
					return true
				)
			)			
			else messagebox "Please select a object." title:"Road Mapper..."
		)
		else messagebox "Please set a length first (the top spinner on the UI)." title:"Road Mapper..."
		return false
	)
	---------------------------------------------------------------------------
	fn setTargetPolys arg = 
	(
		if (isObjectValid()) do 
		(
			local obj = selection[1]
			local mod_uv = obj.modifiers[1]
			
			if not ((mod_uv.getSelectedFaces()).isempty) then
			(			
				targetBB = rsta_uv.get_selBoundingBox mod_uv
				if (arg == 1) do RoadMapperWin.btn_apply.enabled = true
				if (arg == 2) do RoadMapperWin.btn_apply2.enabled = true
			)
			else  messagebox "No UV Faces Selected." title:"Road Mapper..."
		)	
	)
	---------------------------------------------------------------------------
	fn doMapping =
	(
		local win = RoadMapperWin
		if (isObjectValid()) do 
		(	
			local obj = selection[1]			
			local mod_uv = obj.modifiers[1]
			subobjectLevel = 3
			
			if not ((mod_uv.getSelectedFaces()).isempty) then
			(
				local wh = getWidthHeight targetBB
				local cPr = win.spn_roadLength.value/wh.h			

				mod_uv.name = "Diffuse UVs"				
				
				-- SCALE
				if (win.cbx_scaleToFit_u1.checked) then mod_uv.scaleSelectedCenter (1/wh.w) 1		
				else mod_uv.scaleSelectedCenter (cPr/win.spn_c1_u.value) 1
				
				if (win.cbx_scaleToFit_v1.checked) then mod_uv.scaleSelectedCenter (1/wh.h) 2
				else mod_uv.scaleSelectedCenter (cPr/win.spn_c1_v.value) 2		

				-- SNAP TO CORNER
				bbox =  rsta_uv.get_selBoundingBox mod_uv		
				mod_uv.unwrap2.snapPivot 2 -- BOTTOM LEFT
				mod_uv.unwrap2.moveSelected [-bbox[1],-bbox[3],0]	
				
				-- OFFSET 
				--local offsetValue = if (win.cbx_random_vOffset.checked) then (random 0.0 1.0) else win.spn_vOffset.value
				--mod_uv.unwrap2.moveSelected [0,offsetValue,0]	
			
				targetBB = undefined
				win.btn_apply.enabled = false	
			)
			else  messagebox "No UV Faces Selected." title:"Road Mapper..."
		)
	)
	---------------------------------------------------------------------------
	fn doMask = 
	(
		if (isObjectValid()) do 
		(
			local win = RoadMapperWin
			obj = selection[1]
			local mod_uv = unwrap_uvw()
			addModifier obj mod_uv
			mod_uv.name = "Mask UVs"	
			mod_uv.setMapChannel 2	
			
			subobjectLevel = 3
			mod_uv.getFaceSelectionFromStack() 	

			-----------------------------------------------------------------------------------
			if (win.cbx_scaleToFit_u2.checked) then 
			(
				-- SCALE
				mod_uv.scaleSelectedCenter (1/(targetBB[2] - targetBB[1])) 1	
				-- MOVE BACK
				local bb2 = rsta_uv.get_selBoundingBox mod_uv
				mod_uv.moveSelected [-bb2[1],0,0]
			)
			else 
			(
				-- SCALE
				local scaleFactor = win.spn_c1_u.value/win.spn_c2_u.value
				mod_uv.scaleSelectedCenter scaleFactor 1

				-- MOVE BACK
				local bb2 = rsta_uv.get_selBoundingBox mod_uv
				mod_uv.moveSelected [-((bb2[1] - targetBB[1]) + (targetBB[1] - (targetBB[1] * scaleFactor))),0,0]	
			)
				
			-----------------------------------------------------------------------------------
			if (win.cbx_scaleToFit_v2.checked) then 
			(
				-- SCALE
				mod_uv.scaleSelectedCenter (1/(targetBB[4] - targetBB[3])) 2
				-- MOVE BACK
				local bb2 = rsta_uv.get_selBoundingBox mod_uv
				mod_uv.moveSelected [0,-bb2[3],0]
			)
			else
			(
				-- SCALE
				local scaleFactor = win.spn_c1_v.value/win.spn_c2_v.value	
				mod_uv.scaleSelectedCenter scaleFactor 2
				-- MOVE BACK
				local bb2 = rsta_uv.get_selBoundingBox mod_uv
				mod_uv.moveSelected [0,-((bb2[3] - targetBB[3]) + (targetBB[3] - (targetBB[3] * scaleFactor))),0]				
			)	
			
			-- UPDATE
			max create mode
			max modify mode
		)
	)
	-----------------------------------------------------------------
	fn scaleEdge arg = 
	(
		if (selection.count != 0) do 
		(
			local obj = selection[1]
			if (obj.modifiers[1] != undefined AND (classof obj.modifiers[1] == unwrap_uvw)) do 
			(
				local uvMod = obj.modifiers[1] 

				if not ((uvMod.getSelectedEdges()).isempty) do 
				(
					uvMod.setTVSubObjectMode  2

					local uvEdge = ((uvMod.getSelectedEdges()) as array)[1]
					local verDist 
					local uvDist 
					
					-- GET UV DIST 
					uvMod.edgeToVertSelect() 					
					uvMod.setTVSubObjectMode  1

					local UvVerts = (uvMod.getSelectedVertices()) as array	
					if (UvVerts.count > 1) do 
					(
						uvDist = rsta_uv.get_vertDistance uvMod UvVerts[1] UvVerts[2] 
					)
					
					-- GET VERT DIST
					local verts = (uvMod.getSelectedGeomVerts()) as array	
					if (verts.count > 1) do 
					(
						verDist = distance ((rsmeshpolyop obj).getvert obj verts[1]) ((rsmeshpolyop obj).getvert obj verts[2])
					)
			
					-- SCALE
					if (verDist != undefined AND uvDist != undefined) then
					(
						uvMod.selectElement()					
						uvScale = (verDist/uvDist)/100					
						uvMod.scaleSelectedCenter uvScale arg
					)
					else break()
					
					uvMod.setTVSubObjectMode  2					
					uvMod.selectEdges #{uvEdge}		
				)				
			)
		)
	)
	------------------------------------------------------------------
	-- CONTROLS
	------------------------------------------------------------------
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:RoadMapperWin.Width	
	local banner = makeRsBanner dn_Panel:rsBannerPanel versionNum:1.01  wiki:"" filename:(getThisScriptFilename())
	---------------------------------------------------------------------------
	
	group "Scale Shell From UV Edge"
	(
		button btn_scaleEdgeU "Scale U" width:90 across:2 align:#left offset:[-5,0]
		button btn_scaleEdgeV "Scale V" width:90 across:2 align:#right offset:[5,0]
	)
		
	spinner spn_roadLength "Length :" fieldwidth:45 across:2 align:#left offset:[-5,3] range:[0,10000,0]	
	button btn_getLength "Get From Edge" align:#right offset:[8,0]	
		
	label lb_1 "Preset :" across:2 align:#left offset:[-5,3]
	dropdownlist ddl_preset  align:#right width:145 offset:[8,0]	
		
	group "Channel 1 (Diffuse)"
	(
		spinner spn_c1_u "U Scale :" fieldwidth:45 align:#left across:2 offset:[0,0]
		checkbox cbx_scaleToFit_u1 "Scale 0-1" align:#right
		
		spinner spn_c1_v "V Scale :" fieldwidth:45 align:#left across:2 offset:[0,0]
		checkbox cbx_scaleToFit_v1 "Scale 0-1" align:#right	
	
		--spinner spn_vOffset "vOffset :" fieldwidth:45 align:#left across:2 offset:[-2,0]
		--checkbox cbx_random_vOffset "Random" align:#right offset:[-5,0]		
			
		button btn_setTarget "Set Target UV Faces" align:#left width:180 offset:[-4,0]		
		button btn_apply "Apply Road Mapping To Faces" align:#left width:180 offset:[-4,0] enabled:false	
	)	
	
	group "Channel 2 (Mask)"
	(
		spinner spn_c2_u "U Scale :" fieldwidth:45 align:#left across:2 offset:[0,0]
		checkbox cbx_scaleToFit_u2 "Scale 0-1" align:#right
		
		spinner spn_c2_v "V Scale :" fieldwidth:45 align:#left across:2 offset:[0,0]
		checkbox cbx_scaleToFit_v2 "Scale 0-1" align:#right	
	
		--checkbox cbx_c2_snapV "Snap V" align:#right
		button btn_setTarget2 "Set Target UV Faces" align:#left width:180 offset:[-4,0]	
		button btn_apply2 "Apply Road Mask To UV Faces" align:#left width:180 offset:[-4,0] enabled:false	
	)
	
	------------------------------------------------------------------
	-- EVENTS
	------------------------------------------------------------------	
	on cbx_scaleToFit_u1 changed arg do spn_c1_u.enabled = not arg
	on cbx_scaleToFit_v1 changed arg do spn_c1_v.enabled = not arg
	on cbx_scaleToFit_u2 changed arg do spn_c2_u.enabled = not arg
	on cbx_scaleToFit_v2 changed arg do spn_c2_v.enabled = not arg		
	---------------------------------------------------------------------------
	on RoadMapperWin open do 
	(
		settings.dialog_windowPos RoadMapperWin #get
		banner.setup()
		
		getPresets()
		ddl_preset.items = for p in presets collect p.name
		setPresets 1
	)
	---------------------------------------------------------------------------
	on RoadMapperWin close do settings.dialog_windowPos RoadMapperWin #set
	---------------------------------------------------------------------------
	on btn_scaleEdgeU pressed do scaleEdge 1	
	---------------------------------------------------------------------------
	on btn_scaleEdgeV pressed do scaleEdge 2	
	---------------------------------------------------------------------------
	on btn_getLength pressed do spn_roadLength.value = getLengthFromEdge()
	---------------------------------------------------------------------------
	on ddl_preset selected arg do setPresets arg		
	---------------------------------------------------------------------------
	on btn_setTarget pressed do setTargetPolys 1	
	---------------------------------------------------------------------------
	on btn_setTarget2 pressed do setTargetPolys 2
	---------------------------------------------------------------------------
	on btn_apply pressed do doMapping()
	--------------------------------------------------------------------------- 
	on btn_apply2 pressed do doMask()
)
createDialog RoadMapperWin style:#(#style_titlebar, #style_border, #style_sysmenu,#style_toolwindow)