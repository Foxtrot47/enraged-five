/*

	POM toolkit

*/

struct DNStyles
(
	textClr = (colorMan.getColor #windowText) * 255,
	windowClr = (colorMan.getColor #window) * 255,
	backgroundClr = (colorMan.getColor #background) * 255,
	dnColour = dotNetClass "System.Drawing.Color",
	backColor = (dotNetClass "System.Drawing.Color").FromArgb (backgroundClr[1] + 10) (backgroundClr[2] + 10) (backgroundClr[3] + 10),
	foreColor = (dotNetClass "System.Drawing.Color").FromArgb textClr[1] textClr[2] textClr[3],
	dnColourExecute = dnColour.FromArgb 51 181 229,
	dnColourSuccess = dnColour.FromArgb 153 204 0,
	dnColourError = dnColour.FromArgb 255 68 68,
	dnColourDormant = dnColour.FromArgb (backgroundClr[1] - 10) (backgroundClr[2] - 10) (backgroundClr[3] - 10),
	
	stdButtonHeight = 24,
	
	fn setExecuteStyle ctrl =
	(
		ctrl.FlatAppearance.BorderColor = dnColourExecute
		windows.processPostedMessages()
	),
	
	fn setSuccessStyle ctrl =
	(
		ctrl.FlatAppearance.BorderColor = dnColourSuccess
		windows.processPostedMessages()
	),
	
	fn setErrorStyle ctrl =
	(
		ctrl.FlatAppearance.BorderColor = dnColourError
		windows.processPostedMessages()
	),
	
	fn setDormantStyle ctrl =
	(
		ctrl.FlatAppearance.BorderColor = dnColourDormant
		windows.processPostedMessages()
	),
	
	fn initButtonStyle ctrl =
	(
		ctrl.FlatStyle = (dotNetObject "Button").FlatStyle.Flat
		ctrl.FlatAppearance.BorderSize = 2
		ctrl.FlatAppearance.BorderColor = dnColourDormant
		ctrl.backColor = backColor
		ctrl.foreColor = foreColor
		windows.processPostedMessages()
	)
	
)

dnStyle = DNStyles()


filein (RsConfigGetWildWestDir() + "script/3dsmax/_config_files/wildwest_header.ms")
filein (RsConfigGetWildWestDir() + "script/3dsmax/maps/materials/POMcurvatureMasker.ms")
filein (RsConfigGetWildWestDir() + "script/3dsmax/maps/materials/pxmremoveunderip.ms")
filein (RsConfigGetWildWestDir() + "script/3dsmax/maps/materials/pxmfadeunderdecals.ms")
filein (RsConfigGetWildWestDir() + "script/3dsmax/maps/materials/pxmfadeunderfur.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/Maps/PxmUpgradeTool.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/Maps/PxmRevert.ms")


struct POMToolsStruct
(
	fn selectObjectsWithPxmShaders =
	(
		local POMSelection = #()
		
		for obj in selection where (isKindOf obj Editable_Poly) or (isKindOf obj Editable_Mesh) do
		(
			local ObjMats = #()
			local MatFaces = #()
			RsGetMaterialsOnObjFaces obj materials:ObjMats faceLists:MatFaces
			local FaceCount = obj.faces.count
			-- No need to mask joins if object only uses one material...
			if (ObjMats.Count > 1) do 
			(					
				-- Make blank bitarrays for recording POM/non-POM faces:
				local PomFaces = #{}
						
				PomFaces.Count = FaceCount
				
				-- Find out which materials have a POM shader, and take note of their faces:
				for n = 1 to ObjMats.Count do 
				(
					local ThisMat = ObjMats[n]
					local ThisMatFaces = MatFaces[n]
					
					local isPOM = False
					
					-- Is this a POM shader?
					if (isKindOf ThisMat Rage_Shader) do 
					(
						local ShaderName = RstGetShaderName ThisMat
						isPOM = (matchPattern ShaderName pattern:"*pxm*")
					)
					
					-- Add material face-list to relevant bitarray:
					if isPOM then 
					(
						PomFaces += ThisMatFaces
					)
				)
				
				if (PomFaces.numberSet != 0) then
				(
					print obj.name
					obj.selectedFaces = PomFaces
					append POMSelection obj
				)
			)
			
		)
		
		clearSelection()
		for item in POMSelection do
		(
			selectmore item
		)
		
		true
	)
)

--/////////////////////////////////////////
--	UI
--/////////////////////////////////////////
rollout selectPOMMeshesUI "Select POM Meshes" width:400
(
	timer ctrlState interval:5000 active:false
	dotnetcontrol btnSelectPOMMeshes "Button" text:"Select POM Meshes" width:(selectPOMMeshesUI.width - 10) height:dnStyle.stdButtonHeight offset:[-10, 0]
	
	
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////

	/***
		Setup button look
	***/
	fn init =
	(
		btnSelectPOMMeshes.FlatStyle = (dotNetObject "Button").FlatStyle.Flat
		btnSelectPOMMeshes.FlatAppearance.BorderSize = 2
		btnSelectPOMMeshes.FlatAppearance.BorderColor = dnStyle.dnColourDormant
		btnSelectPOMMeshes.backColor = dnStyle.backColor
		btnSelectPOMMeshes.foreColor = dnStyle.foreColor
		
	)
	
	on btnSelectPOMMeshes click do
	(
		dnStyle.setExecuteStyle btnSelectPOMMeshes
		local success = ::POMToolkitUI.POMTools.selectObjectsWithPxmShaders()
		ctrlState.active = true
		if success do dnStyle.setSuccessStyle btnSelectPOMMeshes
	)
	
	on ctrlState tick do
	(
		dnStyle.setDormantStyle btnSelectPOMMeshes
		ctrlState.active = false
	)
	
	on selectPOMMeshesUI open do
	(
		init()
	)
)

try(destroyDialog POMToolkitUI)catch()
rollout POMToolkitUI "POM Toolkit" width:400 height:600
(
	--/////////////////////////////////////////
	--	VARIABLES
	--/////////////////////////////////////////
	local subRolls = #(RsPOMCurvatureMaskerUI, POMUnderIP_UI, PxmFadeUnderDecalsUI, PxmFadeUnderFurUI, PXMUpgradeUI, PxmRevertUI, selectPOMMeshesUI)
	local subRollHeight = 120
	
	local POMTools = POMToolsStruct()
	
	--/////////////////////////////////////////
	--	CONTROLS
	--/////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:400
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"POM Toolkit" versionName:"Ultra Floss" versionNum:1.1 filename:(getThisScriptFilename())
	
	subRollout subRoll height:subRollHeight width:(POMToolkitUI.width - 10) offset:[-7, 0]
	
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////	
	
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on POMToolkitUI open do
	(
		banner.setup()
		
		--clean any orphaned progress rollouts
		local progRoll = windows.getChildHWND 0 "Progress..." parent:#max
		if (progRoll != undefined) do UIAccessor.CloseDialog progRoll[1]

		for roll in subRolls do
		(
			addSubRollout subRoll roll
			subRollHeight += roll.height
		)
		
		subRoll.height = subRollHeight
		POMToolkitUI.height = (rsBannerPanel.height + subRoll.height + 10)
		
	)
)
createDialog POMToolkitUI style:#(#style_border, #style_titlebar, #style_sysmenu, #style_minimizebox)
