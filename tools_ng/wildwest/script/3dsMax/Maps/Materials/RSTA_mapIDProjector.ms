------------------------------------------------------------------------------
-- FILE IN 
------------------------------------------------------------------------------
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_xml.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_image.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_userSettings.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Selection_Funcs.ms")
-----------------------------------------------------------------------------
-- RIGHT CLICK MENU
-----------------------------------------------------------------------------
rcmenu RSTA_mapIDProjector_RCM
(
	menuItem mi_makeProjection "Make New Projector (Covers Selected Mesh)"	
	separator sp_0
	menuItem mi_setProjector "Set Selected Mesh To Projector"
	menuItem mi_setProjectorTexture "Set New Projector Texture"
	separator sp_1
	menuItem mi_autoPickColours "Auto Pick Colours"
	separator sp_2
	menuItem mi_makeDebugMat "Apply Debug Material To Selection" 
	-- EVENTS 
	on mi_setProjector picked do ::RSTA_mapIDProjector.setSourceObject()
	on mi_makeProjection picked do ::RSTA_mapIDProjector.make_projector()
	on mi_setProjectorTexture picked do ::RSTA_mapIDProjector.setProjectorTexture()
	on mi_autoPickColours picked do ::RSTA_mapIDProjector.auto_extract_colours()
	on mi_makeDebugMat picked do ::RSTA_mapIDProjector.make_debugMat()
)
------------------------------------------------------------------------------
-- ROLLOUT 
------------------------------------------------------------------------------
try (destroyDialog  RSTA_mapIDProjector) catch()
rollout RSTA_mapIDProjector "MapID Projector/Render" width:420
(
	------------------------------------------------------------------------------------------------------------
	-- LOCALS 
	------------------------------------------------------------------------------------------------------------
	local Settings = rsta_userSettings app:"mapIDProjector"
	local DNcolour = dotNetClass "System.Drawing.Color"
	local colour_maxBack = DNcolour.FromArgb ((colorMan.getColor #background)[1]*255) ((colorMan.getColor #background)[2]*255) ((colorMan.getColor #background)[3]*255)
	local colour_maxText =  DNcolour.FromArgb ((colorMan.getColor #text)[1]*255) ((colorMan.getColor #text)[2]*255) ((colorMan.getColor #text)[3]*255)
	local colour_maxShadow =  DNcolour.FromArgb ((colorMan.getColor #shadow)[1]*255) ((colorMan.getColor #shadow)[2]*255) ((colorMan.getColor #shadow)[3]*255)
	local mapIdCount = 0
	local mapIdHolder
	local cs_mapIDProjector
	local projector = undefined
	local colourCount = 30
	local colourDistance = 16

	-----------------------------------------------------------------------------------------------------------
	-- MATHS
	------------------------------------------------------------------------------------------------------------
	fn math_biggest a b arg =
	(	
		for i=1 to 3 do 
		(
			if (arg == #min) then if a[i]<b[i] do b[i] = a[i]
			if (arg == #max) then if a[i]>b[i] do b[i] = a[i]
		)
		return b
	)
	------------------------------------------------------------------------------------------------------------
	fn mathMin a b = return ((a+b)-(abs(a-b)))/2
	------------------------------------------------------------------------------------------------------------
	fn mathMax a b = return ((a+b)+(abs(a-b)))/2		
	------------------------------------------------------------------------------------------------------------
	-- DOT NET FUNCTIONS 
	------------------------------------------------------------------------------------------------------------		
	fn disable_accelerators = enableAccelerators = false
	------------------------------------------------------------------------------------------------------------
	fn add_button text sX sY lX lY cmd:undefined fc:colour_maxText bc:undefined bdc:colour_maxShadow =
	(
		btn =  dotNetObject "System.Windows.Forms.Button"	
		btn.size = dotNetObject "System.Drawing.Size" sX sY
		btn.text = text
		btn.FlatAppearance.BorderColor = bdc
		btn.ForeColor = fc
		if (bc !=undefined) do btn.Backcolor = bc
		btn.flatStyle = btn.flatStyle.flat
		btn.location = dotNetObject "System.Drawing.Point" lX lY 
		if (cmd!=undefined) do dotnet.addEventHandler btn "Click" cmd	
		return btn
	)
	------------------------------------------------------------------------------------------------------------
	fn add_mapIdControl idColour:undefined =
	(		
		IDpan = dotNetObject "System.Windows.Forms.Panel" 
		IDpan.size = dotNetObject "System.Drawing.Size"  100 25
		IDpan.location = dotNetObject "System.Drawing.Point" 0 ((28*mapIdCount))	
		
			-- COLOUR BUTTON 
			if (idColour == undefined) then idColour = colour_maxBack
			else idColour = DNcolour.FromArgb idColour.r idColour.g idColour.b
			IDpan.controls.add (add_button "" 20 20 0 0	bc:idColour cmd:RSTA_mapIDProjector.pickColour)		
			-- MAP ID			
			mapID =  dotNetObject "System.Windows.Forms.NumericUpDown"	
			mapID.size = dotNetObject "System.Drawing.Size"  50 20
			mapID.location = dotNetObject "System.Drawing.Point" 25 0	
			mapID.BorderStyle = mapID.BorderStyle.FixedSingle
			mapID.Value = mapIdCount + 1
			-- SET KEEP FOCUS 
			IDpan.controls.add mapID
			dotnet.addEventHandler mapID "gotFocus" RSTA_mapIDProjector.disable_accelerators			
			-- X BUTTON	
			IDpan.controls.add (add_button "x" 20 20 80 0	bc:colour_maxBack cmd:RSTA_mapIDProjector.removeMapId)			

		-- ADD TO HOLDER	
		mapIdHolder.controls.add IDpan
		-- ADD TO COUNT
		mapIdCount +=1
	)
	------------------------------------------------------------------------------------------------------------
	fn removeMapId s e = 
	(
		s.parent.Dispose()
		mapIdCount -=1
		for i=0 to mapIdHolder.controls.count-1 do
		(
			p = mapIdHolder.controls.Item[i]
			p.location = dotNetObject "System.Drawing.Point" 0 (28*i)	
		)
	)
	------------------------------------------------------------------------------------------------------------
	fn clearMapIDs =
	(
		if (mapIdHolder.controls.count !=0) do for i=mapIdHolder.controls.count-1 to 0 by -1 do	mapIdHolder.controls.Item[i].Dispose()
		mapIdCount = 0
	)
	-----------------------------------------------------------------------------------------------------------
	-- FILE IO 
	-----------------------------------------------------------------------------------------------------------
	fn file_save = 
	(
		fName = getSaveFileName caption:"Save Map IDs" initialDir:(getDir #maxroot) types:"XML(*.xml)"
		if (fName != undefined) do
		(
			if (subString fName (fName.count-3) (fName.count)) != ".xml" then fName = fName + ".xml"
			xml = RSTA_xml_IO()
			xml.new "mapID_preset"
			for i=0 to mapIdHolder.controls.count-1 do
			(
				p = mapIdHolder.controls.Item[i]				
				node_id = rsta_xml.makeNode "mapID" xml.xDoc.documentElement 
				node_id.SetAttribute "idx" (p.controls.Item[1].Value as string)
				node_id.SetAttribute "r" (p.controls.Item[0].Backcolor.r  as string)
				node_id.SetAttribute "g" (p.controls.Item[0].Backcolor.g  as string)
				node_id.SetAttribute "b" (p.controls.Item[0].Backcolor.b  as string)				
			)
			xml.saveAs fName
		)
	)
	----------------------------------------------------------------------------------------------------------
	fn file_load fName: noWarning:false =
	(
		if (fName == unsupplied) do fName = getOpenFileName caption:"Load Map IDs" initialDir:(getDir #maxroot) types:"XML(*.xml)"	
		if (fName != undefined) AND (doesFileExist fName ) do
		(
			if (noWarning) OR (queryBox ("Loading new Colours, you will lose any unsaved mapIDs.\n\nDo you want to continue?") title:"mapID Projector") do 
			(
				clearMapIDs()
				xml = RSTA_xml_IO xmlFile:fName
				xml.load()
				for mapID in (rsta_xml.getChildren xml.root name:"mapID") do
				(
					add_mapIdControl idColour: (color ((mapID.GetAttribute "r") as float)  ((mapID.GetAttribute "g") as float) ((mapID.GetAttribute "b") as float)) 
				)
			)
		)			
	)
	
	------------------------------------------------------------------------------------------------------------
	-- FUNCTIONS
	------------------------------------------------------------------------------------------------------------	
	fn getTextureOffSelected =
	(
		if (classof selection[1].material ==Standardmaterial) then
		(
			if (selected[1].material.diffuseMap.bitmap != undefined) then
			(
				-- RESIZE THE TEXTURE
				local tempBMP = bitmap 256 256	
				RSTA_mapIDProjector.the_bmp.bitmap = (copy selected[1].material.diffuseMap.bitmap tempBMP)	
			)
		)
		else messageBox "Selected object dosn't have a standard shader applied"	 title:"mapID Projector"
	)
	-----------------------------------------------------------------------------------------------------------
	fn auto_extract_colours =
	(
		if (RSTA_mapIDProjector.the_bmp.bitmap != undefined) then
		(
			clearMapIDs()
			rankedC = RSTA_image_rankedColours source_size:256 colourDistance:colourDistance source_bmp:RSTA_mapIDProjector.the_bmp.bitmap
			for c in (rankedC.get_topColours colourCount) do add_mapIdControl idColour:c
		)
	)
	-----------------------------------------------------------------------------------------------------------
	fn pickColour s e =
	(
		rCol = colorPickerDlg (color 100 200 300) "Pick A Colour:" alpha:true pos:mouse.screenpos
		if (rCol != undefined) do	s.Backcolor = DNcolour.FromArgb rCol.r rCol.g rCol.b
	)
	-----------------------------------------------------------------------------------------------------------
	fn setSourceObject obj:undefined=
	(	
		if obj == undefined do obj = selection[1]	
		if (obj != undefined) do
		(
			if (obj.name != "mapId_projectorMesh") do
			(
				if (queryBox "Rename to \"mapId_projectorMesh\"?\n") do obj.name = "mapId_projectorMesh"
			)
			projector = obj
			if (classof projector.material == Standardmaterial) then
			(
				if (projector.material.diffuseMap.fileName != undefined) then 
				(
					cs_mapIDProjector.set_image projector.material.diffuseMap.fileName
					-- RESIZE THE TEXTURE
					local tempBMP = bitmap 256 256	
					RSTA_mapIDProjector.the_bmp.bitmap = (copy projector.material.diffuseMap.bitmap tempBMP)		
					if not(RSTA_mapIDProjector.cbx_autoLoadXml.Checked) do 
					(
						if (queryBox "Do you want to auto-extract the colours?" Title:"MapID Projector") do auto_extract_colours()
					)
				)
				else messageBox "This doesn't have a diffuse bitmap applied to it" title:"mapID Projector"
			)
			else messageBox "This doesn't have a standard material applied to it" title:"mapID Projector"
		)	
	)
	-----------------------------------------------------------------------------------------------------------
	fn setProjectorTexture =
	(
		local fName = getOpenFileName caption:"Projection Texture..."
		if (fName != undefined) do 	projector.mat.diffuseMap = Bitmaptexture fileName:fName	
		
		cs_mapIDProjector.set_image projector.material.diffuseMap.fileName
		-- RESIZE THE TEXTURE
		local tempBMP = bitmap 256 256	
		RSTA_mapIDProjector.the_bmp.bitmap = (copy projector.material.diffuseMap.bitmap tempBMP)			
	)
	-----------------------------------------------------------------------------------------------------------
	fn make_projector =
	(
		if (selection.count != 0) then
		(
			local oldSource = getnodebyname "mapId_projectorMesh"
			if (oldSource != undefined) do delete oldSource
			------------------------------------------------------------------------------------------
			bbMin = selection[1].min
			bbMax = selection[1].max
			
			for obj in selection do 
			(
				bbMin = math_biggest bbMin obj.min #min
				bbMax = math_biggest bbMax obj.max #max
			)
			
			center = [((bbMax.x + bbMin.x)/2),((bbMax.y + bbMin.y)/2), (((bbMax.z + bbMin.z)/2) + abs(bbMax.z - bbMin.z))]	
			p = Plane length:(bbMax.y - bbMin.y) width:(bbMax.x - bbMin.x) pos:center lengthsegs:1 widthsegs:1 
			p.name = "mapId_projectorMesh"
			--rotate p (angleaxis 0 [1,0,0])
			convertToMesh p
			p.mat = Standardmaterial()
			p.mat.ShowInViewport = true
			
			if (cs_mapIDProjector.image_file == undefined) then			
			(			
				local fName = getOpenFileName caption:"Projection Texture..."
				if (fName != undefined) do 	p.mat.diffuseMap = Bitmaptexture fileName:fName		
			)
			else p.mat.diffuseMap = Bitmaptexture fileName:cs_mapIDProjector.image_file				

			setSourceObject obj:p
		)
		else messageBox "Nothing Selected!" title:"mapID Projector"
	)
	----------------------------------------------------------------------------------------------------------
	fn make_debugMat selfIllum: maskID: =
	( 
		if (selection.count != 0) then
		(
			isMask = false
			if maskID != unsupplied do isMask = true
			----------------------------------------------------------------
			debugMat = Multimaterial()
			debugMat.numsubs = mapIdHolder.controls.count-1 
			matCount = 1
			
			for i=0 to mapIdHolder.controls.count-1 do		
			(
				local id = mapIdHolder.controls.Item[i].controls.Item[1].Value	
				local c = mapIdHolder.controls.Item[i].controls.Item[0].BackColor
				
				tempMat  = Standardmaterial()
				
				-- COLOUR 
				tempMat.Diffuse = color c.r c.g c.b
				if (isMask) do 
				(
					tempMat.Diffuse = color 0 0 0 
					if id == maskID then tempMat.Diffuse = color 255 255 255
				)

				-- self illum for render
				if (selfIllum == true) do tempMat.selfIllumAmount = 100				
				debugMat.materialList[matCount] = tempMat		
				debugMat.materialIDList[matCount] = id			
				matCount +=1
			)			
			for t in selection do t.mat = debugMat		
		)
		else messageBox "Nothing selected to apply debug material to." title:"mapID Projector"
	)
	----------------------------------------------------------------------------------------------------------
	fn Set_vertBorderSelection obj = 
	(
		local copyObj = convertToPoly (copy obj)

		local MapBorderEdges = #{}
		
		for MatId in (RsGetModelFaceIds copyObj) do 
		(
			copyObj.EditablePoly.SelectByMaterial MatId
			copyObj.EditablePoly.ConvertSelectionToBorder #Face #Edge
			MapBorderEdges += (GetEdgeSelection copyObj)
		)
		
		-- SET TO VERTS		
		polyop.setEdgeSelection copyObj (MapBorderEdges)	
		copyObj.EditablePoly.ConvertSelection #Edge #Vertex
		local mapBorderVerts = GetVertSelection copyObj

		
		-- REMOVE EDGES
		copyObj.EditablePoly.SetSelection #edge (polyOp.getOpenEdges copyObj)
		copyObj.EditablePoly.ConvertSelection #Edge #Vertex
		local geoBorderVerts = GetVertSelection copyObj
		
		-- MINUS BORDERS
		if not(RSTA_mapIDProjector.cbx_vertex_excludeBorders.state) do geoBorderVerts = #{}
		polyop.setVertSelection copyObj (mapBorderVerts-geoBorderVerts)	

		-- COPY TO ORGINAL
		if (classof obj == Editable_mesh) do setVertSelection obj (polyop.getVertSelection copyObj)
		if (classof obj == Editable_poly) do polyop.setVertSelection obj (polyop.getVertSelection copyObj)

		delete copyObj	
	)
	------------------------------------------------------------------------------------------------------------
	-- USER INTERFACE 
	------------------------------------------------------------------------------------------------------------
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:RSTA_mapIDProjector.Width	
	local banner = makeRsBanner dn_Panel:rsBannerPanel versionNum:1.71 versionName:"Tasteless Bear" wiki:"mapID_projector" filename:(getThisScriptFilename())

	group "MapID Projector"
	(
		bitmap the_bmp width:256 height:256 offset:[-2,5] align:#left		
			
		button btn_menu "Menu" height:30 width:40  align:#left offset:[-2,0]
		button btn_project "Project IDs To Selection" width:211 offset:[43,-35] height:30 align:#left

		dotNetControl dn_panel "System.Windows.Forms.Panel" height:300 width:135 offset:[266,-304]	
		checkbox cbx_autoLoadXml "Auto Load XML : " across:3 offset:[0,3] checked:false
		edittext edt_xmlFilePath width:260  offset:[-30,2] enabled:false
		button btn_pickXmlFilePath "..."  offset:[55,0]
		progressbar pbar height:10 width:400 offset:[-2,2] color:(color 246 186 0)
	)	
	
	group "Map ID Border Vertex Blend"
	(
		checkbox cbx_vertex_1 "M1" across:7 
		checkbox cbx_vertex_2 "M2" checked:true offset:[-20,0]
		checkbox cbx_vertex_3 "M3" offset:[-40,0]
		checkbox cbx_vertex_4 "M4" checked:true offset:[-60,0]
		
		checkbox cbx_vertexAlpha "Alpha" checked:true offset:[-70,0]
		
		checkbox cbx_vertex_excludeBorders "Exclude Borders" checked:true offset:[-70,0]
		button btn_vertexBlend "Blend Verts" width:80 offset:[-15,-5]

	)
	
	group "Auto Pick Colours"
	(
		Spinner spn_colourCount "Count : " type:#integer width:100 align:#left across:3 offset:[2,3]
		Spinner spn_colourDistance "Tolerance : " type:#integer width:100 align:#left offset:[-15,3]
		button btn_autoPickColour "Auto Pick" width:150 align:#right offset:[2,0]
	)
	
	group "Render To Texture"
	(
		button btn_render "Render Selected Objects To Texture" width:250 across: 2 offset:[25,0]
		spinner spn_render_width "Width (pixels) : " range:[256, 8192, 1024] type:#integer offset:[40,3] width:100
		radiobuttons rdb_render_uvSpace labels:#("Top Down", "UV Space") columns:2 across:3 offset:[7,-1]
		checkbox cbx_render_mask "Render Mask From Map ID :" offset:[51,0]
		spinner spn_render_map "" width:58 offset:[8,0] type:#integer range:[1,99,1] enabled:false 
	)
	
	group "Select Polys From Texture (Uses UV Space)"
	(
		button  btn_texture "None" width:150  offset:[25,0] across:4
		colorpicker clp_sft_colour "" width:24 offset:[54,0] 
		spinner spn_threshold "Tol :" range:[0,255,10] type:#integer width:80 offset:[-40,2] 
		button btn_select_from_texture "Select From Texture" width:130 offset:[-22,0] 
	)	

		
	------------------------------------------------------------------------------------------------------------
	-- EVENTS 
	------------------------------------------------------------------------------------------------------------	
	on btn_menu pressed do popUpMenu RSTA_mapIDProjector_RCM
	-------------------------------------------------------------------------------------------------------------
	on btn_project pressed do 
	(
		if (projector != undefined) then
		(
			if (selection.count != 0 ) then
			(
				-- SET IMAGE
				r_angle = filterString ((quatToEuler projector.rotation) as string) " ()"
				r_angle = (r_angle[r_angle.count]) as float
				
				localBounds = nodeGetBoundingBox projector projector.transform 	
				bounds = localBounds[2]-localBounds[1] 	
				bounds = bounds * projector.scale
				
				minBounds = [(mathMin localBounds[1].x localBounds[2].x), (mathMin localBounds[1].y localBounds[2].y), (mathMin localBounds[1].z localBounds[2].z)]	
				minBounds *=  projector.scale	
				
				cs_mapIDProjector.pMesh.dimentions = dotNetObject "rsta.mapIDProjecor.double2D" bounds.x bounds.y
				cs_mapIDProjector.pMesh.pivot = dotNetObject "rsta.mapIDProjecor.double2D" projector.pos.x projector.pos.y
				cs_mapIDProjector.pMesh.offset = dotNetObject "rsta.mapIDProjecor.double2D" minBounds.x minBounds.y

				cs_mapIDProjector.pMesh.rotation = r_angle
				
				-- SET COLOURS			
				if (mapIdHolder.controls.count !=0) then
				(				
					for i=0 to mapIdHolder.controls.count-1 do		
					(
						local id = mapIdHolder.controls.Item[i].controls.Item[1].Value	
						local c = mapIdHolder.controls.Item[i].controls.Item[0].BackColor
						cs_mapIDProjector.add_color id c.r c.g c.b
					)	
					
					local pb_percent = 0.0
					local pb_inc = 100.0/selection.count
					-- LOOP OBJECTS
					for tObj in (selection as array) do
					(
						if classOf_Array #(Editable_Poly, Editable_mesh) tObj do
						(
							convertToMesh tObj
							for faceIdx=1 to (tObj.numfaces) do
							(	
								local polyCenter = meshop.getFaceCenter tObj faceIdx		
								setFaceMatID tObj faceIdx (cs_mapIDProjector.get_map_id polyCenter.x polyCenter.y)
							)
							pb_percent += pb_inc
							RSTA_mapIDProjector.pbar.value = pb_percent as integer	
						)					
					)
				)
				else   messagebox "No mapID/Colors Set!" title:"mapID Projector"
			)
			else  messagebox "Nothing Selected!" title:"mapID Projector"			
		)
		else messagebox "No Projector Mesh Set!" title:"mapID Projector"
		
		RSTA_mapIDProjector.pbar.value = 0		
		redrawViews()
		
	)
	------------------------------------------------------------------------------------------------------------
	on cbx_autoLoadXml changed arg do 
	(		
		edt_xmlFilePath.enabled = arg
		if (arg) do file_load fName:edt_xmlFilePath.text noWarning:true
	)
	------------------------------------------------------------------------------------------------------------
	on btn_pickXmlFilePath pressed do 
	(
		local file = (getOpenFileName types:"XML(*.xml)|*.xml")
		if (file != undefined) do 
		(
			edt_xmlFilePath.enabled = true
			cbx_autoLoadXml.checked = true
			edt_xmlFilePath.text = file
			file_load fName:file noWarning:true
		)
	)
	------------------------------------------------------------------------------------------------------------
	on btn_vertexBlend pressed do 
	(		
		local mapArray = #(	datapair checkbox:cbx_vertex_1 colour:(color 0 0 0),
							datapair checkbox:cbx_vertex_2 colour:(color 0 0 255),
							datapair checkbox:cbx_vertex_3 colour:(color 0 255 0),
							datapair checkbox:cbx_vertex_4 colour:(color 0 255 255))
			
		for obj in selection where classof_array #(Editable_Poly,Editable_mesh) obj do 
		(	
			if (classof obj == Editable_mesh) do 
			(
				local msg = obj.name + "  is a mesh object, this is going to take forever to process. A poly object is much quicker, do you want to convert this to a poly obj?"
				if (queryBox msg title:"mapID Projector") do convertToPoly obj
			)
			-- SET ALL BLACK 
			(RsMeshPolyOp obj).setVertColor obj 9 #{1..((RsMeshPolyOp obj).getNumVerts obj)} mapArray[1].colour

			-- GET EDGES
			Set_vertBorderSelection obj

			-- DO ALPHA 
			if (cbx_vertexAlpha.state) do
			(
				local currentVerts = if (classof obj == editable_poly) then (polyOp.getVertSelection obj) else (getVertSelection obj)
				-- SET BLACK 
				(RsMeshPolyOp obj).setVertColor obj -2 #{1..((RsMeshPolyOp obj).getNumVerts obj)} (color 0 0 0)
				-- SET WHITE
				(RsMeshPolyOp obj).setVertColor obj -2 currentVerts (color 255 255 255)
			)

			local percent = 0
			for i in mapArray do if (i.checkbox.state) do percent += 1
			percent = 100/percent
			
			for colour in mapArray do 
			(
				if (colour.checkbox.state) do 
				(
					local verts = if (classof obj == editable_poly) then (polyOp.getVertSelection obj) else (getVertSelection obj)
					(RsMeshPolyOp obj).setVertColor obj 9 verts colour.colour
					gRsSelectionTools.RandomlyDeselectVerts obj percent
				)
			)

			subobjectLevel = 0
		)		
	)
	------------------------------------------------------------------------------------------------------------
	on btn_texture pressed do
	(
		local fName = getOpenFileName caption:"Projection Texture..."
		if (fName != undefined) do 	
		(
			local fName_bits = FilterString fName "\\/"
			btn_texture.text = fName_bits[fName_bits.count]
			cs_mapIDProjector.set_image fName
			
			-- RESIZE THE TEXTURE
			local tempBMP = bitmap 256 256	
			RSTA_mapIDProjector.the_bmp.bitmap = (copy (openBitMap fName) tempBMP)	
		)
	)
	------------------------------------------------------------------------------------------------------------
	on btn_select_from_texture pressed do 
	(
		if (selection.count != 0) then
		(
			if (btn_texture.text != "None") then
			(
				-- SET COLOUR AND TOLERANCE
				cs_mapIDProjector.set_match_colour_tol  clp_sft_colour.color.r clp_sft_colour.color.g clp_sft_colour.color.b spn_threshold.value				
				
				for obj in selection where classof_array #(Editable_Poly,Editable_mesh) obj do
				(				
					if classof obj == Editable_Poly do convertToMesh obj
					
					local face_array = #()					
					for faceIdx = 1 to (meshop.getNumFaces obj) do 
					(		
						local polyCenter = meshop.getFaceCenter obj faceIdx	
						local bary = meshop.getBaryCoords obj faceIdx polyCenter						
						-- GET THE TEXTURE FACE
						local textureFace = getTVFace obj faceIdx	
						-- GET THE UVW VERTS OF THE FACE
						local tVert_1 = getTVert obj textureFace.x
						local tVert_2 = getTVert obj textureFace.y
						local tVert_3 = getTVert obj textureFace.z
						-- CALCULATE THE TEXTURE VERTICES AT POINT OF INTERSECTION FROM  THE BARYCENTRIC COORDINATES
						local tv = tVert_1*bary.x + tVert_2*bary.y + tVert_3 * bary.z	
						if (cs_mapIDProjector.get_colourMatch (mod tv.x 1) (mod tv.y 1)) do append face_array faceIdx
					)
					-- SET SELECTION
					setFaceSelection obj face_array
				)
			)
			else messageBox "No Texture Set!"
		)
		else messageBox "Nothing Selected!"
	)
	------------------------------------------------------------------------------------------------------------
	on btn_render pressed do
	(
		local objs = selection
		if objs.count != 0 then 
		(
			if (mapIdHolder.controls.count != 0) then
			(
				local size = spn_render_width.value
				local unhideProjector = false
				try 
				(
					hide projector
					unhideProjector = not(projector.ishidden)
				) 
				catch()	
				
				-- SET UP MATERIAL --------------------------------------
				local shader_array = #()
				for obj in objs do append shader_array obj.mat	
				-- CHECK FOR MASK?
				if (cbx_render_mask.state) then	make_debugMat selfIllum:true maskID:spn_render_map.value			
				else make_debugMat selfIllum:true	

				-- RENDER TOP DOWN 
                if rdb_render_uvSpace.state == 1	then 
				(				
					-- WORK OUT BOUNDS AND CAM POS
					local target_bb_min = objs[1].pivot
					local target_bb_max = objs[1].pivot
						
					for obj in objs do 
					(
						target_bb_min = math_biggest target_bb_min obj.min #min 
						target_bb_max = math_biggest target_bb_max obj.max #max
					)	
					
					local target_size = target_bb_max - target_bb_min
					local target_center = target_size/2 + target_bb_min
					local target_biggest = mathMax target_size.x target_size.y
					
					-- SET UP CAMERA ----------------------------------------------
					cam = freecamera()
					cam.fov = 90
					cam.orthoProjection = true
					cam.baseObject.targetDistance = (target_biggest/2)
					cam.pos = target_center + [0,0,100]					
					-- RENDER --------------------------------------------------------
					render 	camera:cam \
							outputSize:[size,size] \
							antiAliasing:false				
					---------------------------------------
					delete cam
				)
				-- RENDER IN UV SPACE -----------------------------------
				else 
				(
					for obj in objs do
					(		
						obj.iNodeBakeProperties.removeAllBakeElements() 
						
						be1 = diffusemap() 
						be1.outputSzX = be1.outputSzY = size 
						be1.fileType = (getDir #image + "\\" + obj.name + "_mapIDs.tga")
						be1.fileName = filenameFromPath be1.fileType
						be1.filterOn = be1.shadowsOn = be1.lightingOn = false 					
						be1.enabled = true
						
						obj.INodeBakeProperties.addBakeElement be1 
						obj.INodeBakeProperties.bakeEnabled = true 
						obj.INodeBakeProperties.bakeChannel = 1 
						obj.INodeBakeProperties.nDilations = 1 
						
						select obj 
						render rendertype:#bakeSelected vfb:off progressBar:true outputSize:[size,size]
					)
					
					shellLaunch "explorer.exe" (getDir #image) 
				)
				
				-- CLEAN UP -----------------------------------
				if (unhideProjector) do unhide projector
				-- REASSIGN MATERIALS 
				for i=1 to objs.count do objs[i].mat = shader_array[i]
					
				
			)
			else messageBox "No MapID Colours Set"
		)
		else messagebox "Nothing Selected!"
	)
	-----------------------------------------------------------------------------------------------------------
	on cbx_render_mask changed arg do
	(
		spn_render_map.enabled = arg
	)	
	-----------------------------------------------------------------------------------------------------------
	on spn_colourDistance changed arg do colourDistance = arg
	-----------------------------------------------------------------------------------------------------------
	on spn_colourCount changed arg do colourCount = arg
	-----------------------------------------------------------------------------------------------------------
	on btn_autoPickColour pressed do auto_extract_colours()
	------------------------------------------------------------------------------------------------------------	
	on RSTA_mapIDProjector open do
	(
		settings.dialog_windowPos RSTA_mapIDProjector #get
		-- BANNER SET UP
		banner.setup()
		-- DISABLE GAMMA
		IDisplayGamma.colorCorrectionMode = #none
		-- SET THE BACK GROUND	
		dn_panel.BackColor = colour_maxBack

		-- SET UP THE DOTNET 
		dn_panel.controls.add (add_button "Add New MapID" 130 21 0 0 cmd:RSTA_mapIDProjector.add_mapIdControl)			
			mapIdHolder = dotNetObject "System.Windows.Forms.Panel"
			mapIdHolder.size = dotNetObject "System.Drawing.Size"  130 233
			mapIdHolder.location = dotNetObject "System.Drawing.Point" 0 35	
			mapIdHolder.AutoScroll = true
		dn_panel.controls.add mapIdHolder	

		dn_panel.controls.add (add_button "Load" 60 21 0 278 cmd:RSTA_mapIDProjector.file_load)	
		dn_panel.controls.add (add_button "Save" 60 21 70 278 cmd:RSTA_mapIDProjector.file_save)	

		-- SET UP PROJECTOR
		CSharp.CompileToMemory #(RsConfigGetWildWestDir() + @"script\3dsMax\Maps\Materials\rsta_mapIDProjector.cs")
		cs_mapIDProjector = dotNetObject "rsta.mapIDProjecor.mapIdProjector"	

		-- SET VALUES
		spn_colourDistance.value = colourDistance
		spn_colourCount.value = colourCount

		--if (getnodebyname "mapId_projectorMesh") != undefined do setSourceObject obj:(getnodebyname "mapId_projectorMesh")

		-- USER SETTINGS 
		if ((settings.getValue "AutoLoadXmlPathEnable") != undefined) do cbx_autoLoadXml.checked = (settings.getValue "AutoLoadXmlPathEnable") 
		if ((settings.getValue "AutoLoadXmlPath") != undefined) do edt_xmlFilePath.text = (settings.getValue "AutoLoadXmlPath") 
		
		if ((settings.getValue "VertexBlend_vtx1") != undefined) do cbx_vertex_1.checked = (settings.getValue "VertexBlend_vtx1") 
		if ((settings.getValue "VertexBlend_vtx2") != undefined) do cbx_vertex_2.checked = (settings.getValue "VertexBlend_vtx2") 
		if ((settings.getValue "VertexBlend_vtx3") != undefined) do cbx_vertex_3.checked = (settings.getValue "VertexBlend_vtx3") 
		if ((settings.getValue "VertexBlend_vtx4") != undefined) do cbx_vertex_4.checked = (settings.getValue "VertexBlend_vtx4") 
		if ((settings.getValue "VertexBlend_vtxAlpha") != undefined) do cbx_vertexAlpha.checked = (settings.getValue "VertexBlend_vtxAlpha") 
		if ((settings.getValue "VertexBlend_exludeBorders") != undefined) do cbx_vertex_excludeBorders.checked = (settings.getValue "VertexBlend_exludeBorders") 

		if ((settings.getValue "AutoPick_ColourCount") != undefined) do spn_colourCount.value = (settings.getValue "AutoPick_ColourCount") 
		if ((settings.getValue "AutoPick_ColourDistance") != undefined) do spn_colourDistance.value = (settings.getValue "AutoPick_ColourDistance") 
		
		if ((settings.getValue "RenderToTexture_RenderWidth") != undefined) do spn_render_width.value = (settings.getValue "RenderToTexture_RenderWidth") 
		if ((settings.getValue "RenderToTexture_RenderMap") != undefined) do cbx_render_mask.checked = (settings.getValue "RenderToTexture_RenderMap") 
		if ((settings.getValue "RenderToTexture_RenderMapId") != undefined) do spn_render_map.value = (settings.getValue "RenderToTexture_RenderMapId") 
		if ((settings.getValue "RenderToTexture_UvSpace") != undefined) do rdb_render_uvSpace.state = (settings.getValue "RenderToTexture_UvSpace") 

		if ((settings.getValue "SelectPolyFromTexture_Threshold") != undefined) do spn_threshold.value = (settings.getValue "SelectPolyFromTexture_Threshold") 
		
		-- AUTOLOAD 		
		if (cbx_autoLoadXml.checked) do file_load fName:edt_xmlFilePath.text noWarning:true
		
		edt_xmlFilePath.enabled  = cbx_autoLoadXml.checked			
	)
	----------------------------------------------------------------------------------------------------------------------------
	on RSTA_mapIDProjector close do 
	(
		settings.dialog_windowPos RSTA_mapIDProjector #set
		-- USER SETTINGS 
		settings.setValue "AutoLoadXmlPathEnable" cbx_autoLoadXml.checked
		settings.setValue "AutoLoadXmlPath" edt_xmlFilePath.text
		
		settings.setValue "VertexBlend_vtx1" cbx_vertex_1.checked
		settings.setValue "VertexBlend_vtx2" cbx_vertex_2.checked
		settings.setValue "VertexBlend_vtx3" cbx_vertex_3.checked
		settings.setValue "VertexBlend_vtx4" cbx_vertex_4.checked
		settings.setValue "VertexBlend_vtxAlpha" cbx_vertexAlpha.checked
		settings.setValue "VertexBlend_exludeBorders" cbx_vertex_excludeBorders.checked
		
		settings.setValue "AutoPick_ColourCount" spn_colourCount.value
		settings.setValue "AutoPick_ColourDistance" spn_colourDistance.value
		
		settings.setValue "RenderToTexture_RenderWidth" spn_render_width.value
		settings.setValue "RenderToTexture_RenderMap" cbx_render_mask.checked
		settings.setValue "RenderToTexture_RenderMapId" spn_render_map.value
		settings.setValue "RenderToTexture_UvSpace" rdb_render_uvSpace.state 	
		
		settings.setValue "SelectPolyFromTexture_Threshold" spn_threshold.value
	)
)
createDialog RSTA_mapIDProjector style:#(#style_titlebar, #style_border, #style_sysmenu,#style_toolwindow)
