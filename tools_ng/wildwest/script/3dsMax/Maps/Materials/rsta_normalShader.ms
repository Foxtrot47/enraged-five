filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_userSettings.ms")

global gRsNormalShaderObject , gRsNormalShader

-- GLOBAL NORMAL SHADER OBJECT ARRAY
if (gRsNormalShaderObject == undefined) do 
(
	gRsNormalShaderObject = #()
)

-- GLOABL NORMAL SHADER 
if (gRsNormalShader == undefined) do 
(
	local fxFile = RsConfigGetToolsDir() + "techart/dcc/Shaders/rs_normal_only.fx"
	gRsPerForce.sync #(fxFile)
	if (doesFileExist fxFile) then 	gRsNormalShader = DirectX_9_Shader effectfile:fxFile
	else messageBox "Can't Find \"rs_normal_only.fx\", Please contact Tech Art."
)

try (destroyDialog  ViewNormalsWin) catch()
rollout ViewNormalsWin "Normals Only" width:100
(	
	-- LOCALS
	local Settings = rsta_userSettings app:"ViewNormals"	
	
	-- FUNTIONS	
	fn resortMaterials = 
	(
		for dp in gRsNormalShaderObject do dp.obj.mat = dp.mat 
		gRsNormalShaderObject = #()
	)
	
	-- CONTROLS	
	checkbutton btn_toggle "Toggle Sel" width:90
	
	-- EVENTS	
	on ViewNormalsWin open do settings.dialog_windowPos ViewNormalsWin #get
	on ViewNormalsWin close do 
	(
		settings.dialog_windowPos ViewNormalsWin #set
		resortMaterials()	
	)
	------------------------------------------------------------------	
	on btn_toggle changed state do
	(
		if (state) then 
		(
			undo "Normal Only Shader" on
			(
				resortMaterials()
				for obj in (getCurrentSelection()) where (classof_array #(editable_poly, editable_mesh) obj) do 
				(
					append gRsNormalShaderObject (datapair obj:obj mat:obj.mat)
					obj.mat = gRsNormalShader
				)
			)
		)
		else resortMaterials()			
	)
)
createDialog ViewNormalsWin style:#(#style_titlebar, #style_border, #style_sysmenu,#style_toolwindow)