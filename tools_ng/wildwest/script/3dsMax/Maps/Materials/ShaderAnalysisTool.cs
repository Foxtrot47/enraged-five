﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ShaderAnalysis
{
    class TextureMap
    {
        public string Path { get; set; }
        public float Usage { get; set; }
        public string UsageString
        {
            get
            {
                return String.Format("{0:0.0}% Used", this.Usage);
            }
        }
    }

    class MaterialDescription
    {
        public string Name { get; set; }
        public int Handle { get; set; }     //animation handle of the material NOT THE OBJECT HANDLE
        public int[] FaceList { get; set; }
        public int Percentage { get; set; } //percentage of faces on object using this texture
        public string ShaderType { get; set; }
        public TextureMap[] TextureMapList { get; set; }
        public int ObjectHandle { get; set; }     //handle of object
        public string FaceCountString
        {
            get
            {
                string outputString = "\tUsed on " + this.FaceList.Count().ToString() + " faces (";
                outputString += this.Percentage.ToString() + "% of faces)";
                return outputString;
            }
        }
        public Color Tint { get; set; }
    }

    class ShaderAnalysisTool
    {
        FrameworkElement BindedElement;
        public int testValue = 42;

        //RoGeo XAniML's explanation of this stolen chunk of code
        //Setups up the passed window for use with this Painter instance. Sets up the window data 
        //context to this instance of Painter. Also uses attached events to monitor for clicks and 
        //changes on the UI. These events are then processed and forwarded on in a more consumable 
        //way to MXS via named events on core.
        public void BindElement(FrameworkElement InputTargetElement)
        {
            this.BindedElement = InputTargetElement;
            this.BindedElement.DataContext = this;
            this.BindedElement.AddHandler(Button.ClickEvent, new RoutedEventHandler(this.Button_ClickEvent), true);
        }

        public void Button_ClickEvent(Object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource is Button)
            {
                var TargetButton = e.OriginalSource as Button;

                switch (TargetButton.Name)
                {
                    case "Select_And_Zoom":
                        {
                            //MessageBox.Show(TargetButton.DataContext.ToString());
                            if (TargetButton.DataContext is MaterialDescription)
                            {
                                var TargetMaterialDescription = TargetButton.DataContext as MaterialDescription;
                                RaiseSelectAndZoom(TargetMaterialDescription );
                            }
                            break;
                        }
                    case "Tint_Shaders":
                        {
                            //MessageBox.Show(TargetButton.DataContext.ToString());
                            if (TargetButton.DataContext is ObjectData)
                            {
                                var TargetObjectData = TargetButton.DataContext as ObjectData;
                                RaiseTintShaders(TargetObjectData);
                            }
                        }
                        break;
                }
            }
        }

        //Event to select polygons and to zoom in on them in the max window
        public event EventHandler<MaterialDescriptionEventArgs> SelectAndZoom; //= delegate { };
        public event EventHandler<ObjectDataEventArgs> TintShaders;

        void RaiseSelectAndZoom(MaterialDescription TargetMaterialDescription)
        {
            SelectAndZoom(this, new MaterialDescriptionEventArgs(TargetMaterialDescription));
        }

        void RaiseTintShaders(ObjectData TargetObjectData)
        {
            TintShaders(this, new ObjectDataEventArgs(TargetObjectData));
        }
    }

    class MaterialDescriptionEventArgs : EventArgs
    {
        public MaterialDescription Data;

        public MaterialDescriptionEventArgs(MaterialDescription InputData)
        {
            this.Data = InputData;
        }
    }

    class ObjectDataEventArgs : EventArgs
    {
        public ObjectData Data;

        public ObjectDataEventArgs(ObjectData InputData)
        {
            this.Data = InputData;
        }
    }

    class ObjectData
    {
        public string Name { get; set; }
        public int Handle { get; set; }
        public int FaceCount { get; set; }
        public string[] TestStringArray { get; set; }   //temporary array to help understand the binding process
        public string FaceCountString
        {
            get
            {
                string outputString = "Face count: " + this.FaceCount.ToString();
                return outputString;
            }
        }

        public ObservableCollection<MaterialDescription> ShaderList { get; set; }

        public ObjectData()
        {
            ShaderList = new ObservableCollection<MaterialDescription>();
        }

        /// <summary>
        /// Works out the percentage of an object's faces that a material covers
        /// </summary>
        /// <param name="item">the MaterialDescription object</param>
        public void CalculatePercentage(MaterialDescription item)
        {
            float value = (float)item.FaceList.Count() / (float)this.FaceCount * 100;
            item.Percentage = Convert.ToInt32(value);
        }

    }

    class BindingList
    {
        public ObservableCollection<ObjectData> ObjectDataList { get; set; }

        public BindingList()
        {
            ObjectDataList = new ObservableCollection<ObjectData>();
        }
    }

    class ShaderAnalysisTest
    {
        public List<MaterialDescription> MaterialList = new List<MaterialDescription>();

        public void InitTestValues()
        {
            MaterialDescription matDesc = new MaterialDescription();
            matDesc.Handle = 44;
            matDesc.FaceList = new int[] { 1, 4, 7, 8, 88 };

            this.MaterialList.Add(matDesc);

            matDesc = new MaterialDescription();
            matDesc.Handle = 22;
            matDesc.FaceList = new int[] { 31, 54, 85 };
            this.MaterialList.Add(matDesc);
        }

        public void PrintTestValues()
        {
            foreach (MaterialDescription item in this.MaterialList)
            {
                string FaceString = item.FaceList[0].ToString();

                if (item.FaceList.Count() > 1)
                {
                    for (int i = 1; i < item.FaceList.Count(); i++)
                        FaceString += (", " + item.FaceList[i].ToString());
                }

                Console.WriteLine("Handle: {0}\nFaces: {1}\n", item.Handle, FaceString);
            }
        }
    }
}
