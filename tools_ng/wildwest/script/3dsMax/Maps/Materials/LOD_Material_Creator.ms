-- LOD shader cretor
-- Requesteed by Simon Little
-- Rick Stirling, November 2011

--A utility that takes the material on a selected object and creates a new material but adds a _lod postfix onto the names

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")



-- Clone the shader and rename it.
fn duplicateMattoMat_LOD =
(
	-- Get the first object in the selection
	theSelection = getcurrentselection()
	if (theSelection.count > 0) do
	(
		selObj = theSelection[1]
		
		-- grab the material from this object
		grabbedMat = selObj.material
		grabbedname = (filterstring grabbedMat.name " ")[1]
		
		
		-- Duplicate this and put to the Material Editor
		-- This material is NOT applied to anything, it is not the original.
		duplicateMat = copy grabbedMat
		duplicateMat.name = grabbedname + "_LOD"
		meditMaterials[24] = duplicateMat
	)		
)	




-- Function to copy and rename a texture
fn cloneLodTexture textureToClone  =
(
	print "in clone lod textures"
	
	-- construct a new filename
	lodsuffix = "_LOD"
	newFilename = (getfilenamefile textureToClone ) + lodsuffix + (getFilenameType textureToClone )
	newfilepath = (getfilenamepath textureToClone) + lodsuffix
	newTexture =  (newfilepath + "\\"+ newFilename)
	newTexture = RSmakesafeslashes  (newfilepath + "\\"+ newFilename)
	
	
	try 
	(
		makedir newfilepath
		print "made new folder"
	) 
	catch (messagebox ("Failed to make folder" + newfilepath))
		

	try
	(
		print "copying texture now!"
		copyFile textureToClone newTexture
	)
	catch (messagebox ("Failed to copy texture " +newFilename + " to " + newfilepath))		
		
	newTexture -- return this
)	




-- Copy all the textures and rename them to be lods
fn findLODTextures textureTypeArray =
(
	-- Assumes that slot 24 is set up with the cloned shader
	local lodMatIndex = 24
	
	-- Loop through this and create the copies
	for subMatIndex = 1 to meditMaterials[lodMatIndex].count do
	(
		thematerial = meditMaterials[lodMatIndex].materialList[subMatIndex]

		-- Now that I know the shader, get the required textures
		-- This depends on the input to this functions
		for textypeindex = 1 to textureTypeArray.count do
		(
			
			-- textureTypeArray is a multidimensional array
			-- item 1 is the texture type, item two is the flag for processing it
			textype = textureTypeArray[textypeindex][1]
			processflag =  textureTypeArray[textypeindex][2]
				
			if processflag == true then
			(
				-- Need to know the shadertype, needs to be Rage
				try 
				(
					RageSlotNames =#()
					RageSlotTypes =#()
					
					RageShaderName = RstGetShaderName thematerial 
					RageShaderSlotCount = RstGetVariableCount thematerial 
					
					-- Find out the shader and get an indexed array of the shader variables
					for ss = 1 to RageShaderSlotCount do
					(
						append RageSlotNames (lowercase (RstGetVariableName thematerial ss))
					)
					
					-- Find the correct shader variable index for this map type
					shaderslot = finditem RageSlotNames textype

					-- Read the shader to get that map
					textureToClone = RstGetVariable thematerial shaderslot
					
					-- Call the function to clone and rename this texture, return the new one
					newLodTexture = cloneLodTexture textureToClone 
					
					-- repoint the new texture
					RstSetVariable thematerial shaderslot newLodTexture 
				)	
				catch()	
			) -- end process this texture
			
		)	
	)	-- end submat loop
)	-- end fucntion





-------------------------------------------------------------------------------------------------------------------------
rollout lodMats "LOD Material Creator" width:220 height:200
(
	
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/LOD_Material_Creator" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	button btnCloneSelectedMat "Clone Selected to slot 24" width:170 height:25

	checkbox chkLODTexDiffuse "Diffuse" checked:true across:3
	checkbox chkLODTexNormal "Normal" checked:false
	checkbox chkLODTexSpecular "Specular" checked:false
	
	button btnCreateLODTextures "Create texture copies" width:170 height:25

	
	on btnCloneSelectedMat pressed do duplicateMattoMat_LOD() 
	
	on btnCreateLODTextures pressed do 
	(
		-- This array need to match the input to the function

		 textureTypeArray =#(
							#("diffuse texture",	lodMats.chkLODTexDiffuse.state), 
							#("bump texture", 	lodMats.chkLODTexNormal.state), 
							#("specular texture",	lodMats.chkLODTexSpecular.state)	
							)	
							
		findLODTextures textureTypeArray
	)
	
	
	on lodMats open do ()
	on slodMats close do()

)



createDialog lodMats
