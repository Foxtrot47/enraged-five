-- Select alpha/decal faces
-- Requested by Aaron Garbut for quickly finding polys with decal or alpha shaders on an object
-- Rick Stirling, Rockstar North, Octiber 2011.



filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

-- load a list of shaders with alpha in them
-- multidimensioanl array, the shader, the default checkbox value, the runtime value
mapAlphaShaderList = #(
	#("alpha",true,true),
	#("decal",true,true),
	#("decal_amb_only",false,false),
	#("decal_dirt",false,false),
	#("decal_glue",false,false),
	#("decal_normal_only",false,false),
	#("decal_spec_only",false,false),
	#("decal_tnt",false,false),
	#("normal_alpha",false,false),
	#("normal_decal",false,false),
	#("normal_decal_tnt",false,false),
	#("normal_reflect_alpha",false,false),
	#("normal_reflect_decal",false,false),
	#("normal_reflect_screendooralpha",false,false),
	#("normal_screendooralpha",false,false),
	#("normal_spec_alpha",false,false),
	#("normal_spec_decal",false,false),
	#("normal_spec_decal_tnt",false,false),
	#("normal_spec_reflect_alpha",false,false),
	#("normal_spec_reflect_decal",false,false),
	#("normal_spec_reflect_emmisive_alpha",false,false),
	#("normal_spec_reflect_emmisivenight_alpha",false,false),
	#("normal_spec_screendooralpha",false,false),
	#("reflect_alpha",false,false),
	#("reflect_decal",false,false),
	#("spec_alpha",false,false),
	#("spec_decal",false,false),
	#("spec_reflect_alpha",false,false),
	#("spec_reflect_decal",false,false),
	#("spec_screendooralpha",false,false),
	#("water_decal",false,false)
	)
	


	
	
-- prettify a shadername to a UI string,
fn UIformatshader underbarstring =
(	
	shadername = replaceunderbar underbarstring	
	shadername[1] = uppercase shadername[1]
	shadername
)


-- Update the UI based on the Select All checkbox
fn updateChoices togglestate = (
	
	for shaderindex = 1 to mapAlphaShaderList.count do
	(
		if togglestate == true then 
		(
			execute ("alphaselection.chk_shader_" + mapAlphaShaderList[shaderindex][1] + ".state = true")
			mapAlphaShaderList[shaderindex][3] = true
		)
		
		if togglestate == false then 
		(
			execute ("alphaselection.chk_shader_" + mapAlphaShaderList[shaderindex][1] + ".state = " + (mapAlphaShaderList[shaderindex][2] as string))
			mapAlphaShaderList[shaderindex][3] = mapAlphaShaderList[shaderindex][2]
		)
	)	
)	




-- This function will select polygons that use a chosen shader
fn findpolygons = (
	-- Parse the array of shaders and look for the select ones
	chosenArray = #()

	for shaderindex = 1 to mapAlphaShaderList.count do
	(
		if mapAlphaShaderList[shaderindex][3] == true then append chosenArray shaderindex
	)
	
	-- chosenArray now holds the index of the shaders we are looking for.
	
	
	-- With an object selected
	foundShaderIndex=#()
	theSelection = getcurrentselection()
	if (theSelection.count == 1) do
	(
		selObj = theSelection[1]
		-- Get the shader that is applied to the object
		if superclassof selObj == GeometryClass then
		(	
			appliedMaterial = selObj.material
			
			matcount = 1
			try (matcount = appliedmaterial.count)  catch()
	
			if matcount > 1 then 
			(	
				--loop through the materials and get the shader names
				for submat = 1 to matcount do
				(
					theMat = appliedmaterial[submat]
					
					-- Add a catch for standard materials
					RageShaderType = undefined
					try (
						RageShaderType =  RstGetShaderName theMat 
						RageShaderType = (filterstring RageShaderType ".")[1]
					)catch()
					
					RageShaderType = RageShaderType  as string
									
					-- Now look to see if that shader is in our list of ones we want to find.
					-- We have a list of indices to search for
					for shaderindex = 1 to chosenArray.count do
					(
						searchIndex = chosenArray[shaderindex]
						if mapAlphaShaderList[searchindex][1] == RageShaderType then append foundShaderIndex submat
					)
				) --end subloop
			)-- end material list loop
		) -- end is valid geo
	) -- end selection	
	
	-- foundShaderIndex now holds a list of materials IDs
	-- select them on the model
	max modify mode
	subobjectLevel = 4


	newFaceSel = #() 
	for f = 1 to selObj.numfaces do
	(	
		for mat = 1 to foundShaderIndex.count do
		(	
			if classof selObj == Editable_mesh then 
			(	
				if getFaceMatID selObj f == foundShaderIndex[mat] do append newFaceSel f
			)	
			
			if classof selObj == Editable_Poly then 
			(	
				if polyop.getFaceMatID selObj f == foundShaderIndex[mat] do append newFaceSel f
			)	
			
		)
	)
	
	setFaceSelection selObj newFaceSel

	
)	

-- Function to construct the UI from the 
fn build_alphaSelection_rollout = (
	s = stringStream ""
	-- Open the rollout with a name
	format "rollout alphaselection \"Alpha Poly Selector\" (\n" to:s
	format "hyperlink lnkHelp \"Help?\" address:\"https://devstar.rockstargames.com/wiki/index.php/Alpha_Poly_Selector\" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255) \n" skinBrushSliders to:s

	
	-- Add the checkboxes from the shaderlist
	for i = 1 to mapAlphaShaderList.count do (
		shadername = UIformatshader mapAlphaShaderList[i][1]
		-- On the first line I want to use the across parameter for the button layout
		format "checkbox chk_shader_% \"%\" width:180 checked:%  \n" mapAlphaShaderList[i][1] shadername mapAlphaShaderList[i][2] to:s
		format "on chk_shader_% changed thestate do mapAlphaShaderList[%][3] = chk_shader_%.checked \n" mapAlphaShaderList[i][1] i mapAlphaShaderList[i][1] to:s
	)
	
	format "checkbox chk_shader_all \"*Select All/Reset to Defaults*\" width:180  \n" to:s
	format "on chk_shader_all changed thestate do (updateChoices thestate) \n"  to:s
		
	format "button btn_SelectPolygons \"Select Polygons\" width:140 height:30  \n"  to:s
	format "on btn_SelectPolygons pressed do findpolygons() \n" to:s

	-- Close the rollout
	format ")\n" to:s
	execute (s as string)
)





-- Create floater 
build_alphaSelection_rollout()

createDialog alphaselection width:280

