filein "pipeline/util/texturemetatag_utils.ms"


Struct GTXDTCS
(
	name,
	tcsPath,
	template,
	templateString,
	sourceTexture,
	
	XMLRoot = undefined,
	ExpClass = dotNetClass "System.Xml.XPath.XPathExpression",
	
	
	fn validTcs =
	(
		if tcsPath == undefined then return false
		if doesFileExist tcsPath then return true else return false
		
		--load up the XML if it dont exist
		if XMLRoot == undefined then getXML()
		
	),
	
	fn getXML =
	(
		if (not validTcs()) then return false
		
		local dataStream = dotNetObject "System.IO.StreamReader" tcsPath
		local XML = dotNetObject "System.Xml.XmlDocument"
		XML.load dataStream
		XMLroot = XML.documentElement
		
		dataStream.close() 
		dataStream.dispose() 
		dataStream = undefined 
		
		gc light:true
	),
	
	fn getName =
	(
		if (not validTcs()) then return false
	
		name = getFilenameFile tcsPath
	),
	
	
	fn getSourceTex =
	(
		if (not validTcs()) then return false

		--load up the XML if it dont exist
		if XMLRoot == undefined then getXML()
			
		nodeList = XMLRoot.selectNodes ".//*/sourceTextures/Item/pathname"
		if nodeList.count != 0 then
		(
			sourceTexture = nodeList.item[0].InnerText
		)
		else
		(
			sourceTexture = ""
		)
			
	),
	
	
	fn getTemplate =
	(
		if (not validTcs()) then return false
		
		--load up the XML if it dont exist
		if XMLRoot == undefined then getXML()
			
		nodeList = XMLRoot.selectNodes ".//parent"
		templateString = nodeList.item[0].InnerText
		
		templateStringBits = filterString templateString "/"
		if templateStringBits.count == 0 then --we got no template set
		(
			template = "Default"
		)
		else
		(
			template = templateStringBits[templateStringBits.count]	
		)
		
	)
)

--/////////////////////////////////////////
--	UI
--/////////////////////////////////////////
rollout RsTexPreview "Texture Preview" width:260 height:260
(
	bitmap bmTexPreview width:256 height:256
)

try(destroyDialog GTXDTaggerUI)catch()
rollout GTXDTaggerUI "GTXD Texture Tagger" width:600 height:800
(
	--/////////////////////////////////////////
	--	VARIABLES
	--/////////////////////////////////////////
	local xmlTemplateFile = (RsConfigGetEtcDir() + "texture/maptemplates.xml")
	local TexTuneMapList = #()
	local GTXD_tcsSyncPath = (RsConfigGetAssetsDir() + "metadata/textures/maps/gtxd/...")
	local GTXD_tcsPath = (RsConfigGetAssetsDir() + "metadata/textures/maps/gtxd/*.tcs")
	local maps_tcsRoot = (RsConfigGetAssetsDir() + "metadata/textures/maps/")
	local TCSList = #()
	local changedRowIdx = #()
	
	-- DOTNET VALUES --
	
	local textFont, dingFont, textFontBold, textFontItalic
	
	local DNknownColour = dotNetClass "system.Drawing.KnownColor"
	local DNcolour = dotNetClass "System.Drawing.Color"
	local DNeditedColour = DNcolour.fromARGB 170 64 0
	local DNparentTxdColour = DNcolour.fromARGB 32 32 32
	
	local selColour = DNcolour.fromKnownColor DNknownColour.MenuHighlight
	
	local textCol = (colorMan.getColor #windowText) * 255
	local windowCol = (colorMan.getColor #window) * 255
	local notLoadedCol = if (windowCol[1] < 128) then (windowCol * 1.5) else (windowCol * 0.85)
	
	local textColour = DNcolour.FromArgb textCol[1] textCol[2] textCol[3]
	local backColour = DNcolour.FromArgb windowCol[1] windowCol[2] windowCol[3]
	
	local ALLSTOP = False
	
	--/////////////////////////////////////////
	--	CONTROLS
	--/////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:GTXDTaggerUI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"CHANGEME" versionNum:1.01 versionName:"Yesterdays Flan" filename:(getThisScriptFilename())
	
	--dotNetControl
	group "Refresh List"
	(
		button btnRefresh "Refresh" width:(GTXDTaggerUI.width - 20)
		progressBar prgRefreshList color:(color 0 127 255)
	)
	
	group "Multiple Selection"
	(
		button btnApplySelected "Apply to selection" across:2
		dropdownlist ddlTextureType offset:[0, 0]
		--checkbox chkUnique "Unique" offset:[70, 3]
	)
	
	group "Make Changes"
	(
		button btnMakeChanges "Update Changed TCS" width:300
	)
	
	group "Selection Filter"
	(
		label lblSearchName "Name: " width:100 align:#left across:3
		edittext edtSearchName offset:[-130,0]
		button btnSearchName "Find" width:200 offset:[-100,0]
		
		label lblSearchType "Type: " width:100 align:#left across:3
		edittext edtSearchType offset:[-130,0]
		button btnSearchType "Find" width:200 offset:[-100,0]
	)
	
	dotNetControl texListCtrl "DataGridView" width:10 height:10 visible:true
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn setTexListSize size:[GTXDTaggerUI.width, GTXDTaggerUI.height] = 
	(
		local bufferVal = texListCtrl.pos.x
		texListCtrl.width = GTXDTaggerUI.width - texListCtrl.pos.x - bufferVal
		texListCtrl.height = GTXDTaggerUI.height - texListCtrl.pos.y - bufferVal
		
		--lnkHelp.pos.x = texListCtrl.width - 20
	)
	
	--//////////////////////////////////////////////////////////////////////////////////
	--Update the list of textures in the UI
	--//////////////////////////////////////////////////////////////////////////////////
	fn refreshTexmapList = 
	(
		texListCtrl.rows.clear()
		
		-- sync the GTXD tcs files
		gRsPerforce.sync #(GTXD_tcsSyncPath)
		
		-- iterate them and store them
		local tcsFiles = getFiles GTXD_tcsPath
		local count = 1
		prgRefreshList.value = 0
		for tcsPath in tcsFiles while (not ALLSTOP) do
		(
			--The data object to store the info
			local tcsData = GTXDTCS()
			tcsData.tcsPath = tcsPath
			
			-- get the name
			tcsData.getName()
			-- get the current template type
			tcsData.getTemplate()
			-- get the source texture for preview
			tcsData.getSourceTex()
			
			append TCSList tcsData
			
			local templateUIName = RsResolveTemplateToTypeName tcsData.templateString templatesXmlFilename:xmlTemplateFile
			
			--Now build the visual list for the UI
			local rowData = #(count, tcsData.name, templateUIName)
			
			local newRowNum = texListCtrl.rows.add rowData
			
			-- Store lookup idx into TCSList
			texListCtrl.rows.item[newRowNum].cells.item[0].tag = count
			
			prgRefreshList.value = (100.0 * (count as Float / tcsFiles.count as Float))
			
			count += 1
				
			windows.processPostedMessages()
		)
		
		if (not ALLSTOP) then
		(
			prgRefreshList.value = 0
			texListCtrl.ClearSelection()
		)
		
		gc light:true
	)
	
	--/////////////////////////////////////////--/////////////////////////////////////////--/////////////////////////////////////////--/////////////////////////////////////////
	-- Update the background colour of the trow when an editbale column has been changed
	--/////////////////////////////////////////--/////////////////////////////////////////--/////////////////////////////////////////--/////////////////////////////////////////
	fn setEditedRow rowIdx =
	(
		texListCtrl.rows.item[rowIdx].DefaultCellStyle.backcolor = DNeditedColour
	)
	
	--/////////////////////////////////////////--/////////////////////////////////////////
	-- Set the row to the defualt backColour
	--/////////////////////////////////////////--/////////////////////////////////////////
	fn setUpdatedRow rowIdx =
	(
		texListCtrl.rows.item[rowIdx].DefaultCellStyle.backcolor = backColour
	)
	
	--/////////////////////////////////////////
	--Add the row index to 
	--the list of rows to update
	--when the btnMakeChanges is pressed
	--/////////////////////////////////////////
	fn addRowToChangedRowIdx rowIdx =
	(
		if findItem changedRowIdx rowIdx == 0 then append changedRowIdx rowIdx
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn mirrorToMaps tcsFileName =
	(
		--get the maps/ location
		local tcsPath = maps_tcsRoot + (getFilenameFile tcsFileName) + ".tcs"
		
		--check it out
		gRsPerforce.edit tcsPath
		
		--copy it over
		local success = deleteFile tcsPath
		if not success then
		(
			format "Problem deleting % \n" tcsPath
			return false
		)
		
		success = copyFile tcsFileName tcsPath
		if not success then
		(
			format "Problem copying % to % \n" tcsFileName tcsPath
			return false
		)
		
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn WriteOutTCSFile row = 
	(
		local texmapIdx = texListCtrl.rows.item[row].cells.item[0].tag
		
		local tcsFilename = TCSList[texmapIdx].tcsPath

		--is it checked out
		--get the template name from the column row
		local templateName = (texListCtrl.Item 2 row).EditedFormattedValue
		local selectedTemplate = (RsResolveTypeNameToTemplate templateName templatesXmlFilename:xmlTemplateFile)
--		print ("templateIdx:"+templateIdx as string+" tmpl:"+tmpl as string+", selectedTemplate:"+selectedTemplate as string)
		
		--modify the tcs template if it is different or create it if it doesnt exist
		if ( RsFileExist tcsFilename ) then
		(
			gRsPerforce.add_or_edit #(tcsFilename) silent:false
			
			-- Open up the tcs file to find out what's set currently
			xmlDoc = XMlDocument()
			xmlDoc.init()
			xmlDoc.load ( tcsFilename )
			xmlRoot = xmlDoc.document.DocumentElement

			if xmlRoot != undefined then 
			(
				parentnode = RsGetXmlElement xmlRoot "parent"
				
				-- If not already set to this template then open up the tcs file and edit it, leaving in
				-- default changelist
				if ( parentnode == undefined ) then
				(
					parentnode = xmlDoc.createelement "parent" appendTo:xmlRoot
				)
				if ( parentnode.InnerText != selectedTemplate ) do
				(
					parentnode.InnerText = selectedTemplate	
					xmlDoc.save ( tcsFilename )
				)
			)
			else
			(
				gRsUlog.LogError ("failed to create XML document root for tcs "+tcsFilename)
				gRsUlog.Validate()
			)
			
			--copy the change back into maps
			mirrorToMaps tcsFileName
		)
		else
		(					
			print "file not existent - create new."
			sourceTex = RsMakeSafeSlashes ( toLower ( texmaplist[texmapIdx].filename ) )
			RsGenerateTemplateReferenceFile tcsFilename selectedTemplate sourceTextures:#(sourceTex)
			
			--copy the change back into maps
			mirrorToMaps tcsFileName
		)
	)

	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn init =
	(
		texListCtrl.RowHeadersVisible = false
		texListCtrl.AllowUserToAddRows = false
		texListCtrl.AllowUserToDeleteRows = false
		texListCtrl.AllowUserToOrderColumns = false
		texListCtrl.AllowUserToResizeRows = false
		texListCtrl.AllowUserToResizeColumns = false
		texListCtrl.AllowDrop = false
		texListCtrl.MultiSelect = true

		texListCtrl.dock = texListCtrl.dock.fill
		texListCtrl.DefaultCellStyle.backColor = backColour
		texListCtrl.DefaultCellStyle.foreColor = textColour

		textFont = texListCtrl.font
		dingFont = dotNetObject "System.Drawing.Font" "Webdings" textFont.size
		textFontBold = dotnetobject "system.drawing.font" textFont (dotnetclass "system.drawing.fontstyle").bold
		textFontItalic = dotnetobject "system.drawing.font" textFont (dotnetclass "system.drawing.fontstyle").italic

		texListCtrl.columnCount = 2
		texListCtrl.EnableHeadersVisualStyles = false
		texListCtrl.ColumnHeadersDefaultCellStyle.backColor = backColour
		texListCtrl.ColumnHeadersDefaultCellStyle.foreColor = textColour
		texListCtrl.ColumnHeadersDefaultCellStyle.font = textFontBold
		
		-- Set up index-number column:
		local idxCol = texListCtrl.columns.item[0]
		idxCol.HeaderText = "ID"
		idxCol.ReadOnly = true
		idxCol.width = 34
		
		-- Set up texmap-name column
		local texmapNameCol = texListCtrl.columns.item[1]
		texmapNameCol.ReadOnly = true
		texmapNameCol.AutoSizeMode = texmapNameCol.AutoSizeMode.AllCells
		texmapNameCol.HeaderText = "Texture Name"

		-- Set up texmap-type combobox colum:
		local texmapTypeCol = dotNetObject "System.Windows.Forms.DataGridViewComboBoxColumn"
		texmapTypeCol.HeaderText = "Texture Type"
		--texmapTypeCol.width = 200
		texmapTypeCol.AutoSizeMode = texmapNameCol.AutoSizeMode.AllCells
		texmapTypeCol.DefaultCellStyle.backColor = backColour
		texmapTypeCol.SortMode = (dotNetClass "DataGridViewColumnSortMode").Automatic

		texListCtrl.columns.add texmapTypeCol

		--Populate the texture type drop down combo with the avaialble types
		RsLoadTexTuneConfig TexTuneMapList templatesXmlFilename:xmlTemplateFile
		templateNames = for texTuneMap in TexTuneMapList collect texTuneMap.desc
		texmapTypeCol.CellTemplate.items.addRange templateNames
		texmapTypeCol.FlatStyle = texmapTypeCol.FlatStyle.Flat
		
		ddlTextureType.items = templateNames
		
		--local mnu = dotNetObject "ContextMenuStrip"
		--local texItem = dotNetObject "ToolStripMenuItem" "Find objs with same texture";
		--dotNet.AddEventHandler texItem "Click" menuClickEvntHandler
		--mnu.Items.Add texItem
--		local mapItem = dotNetObject "ToolStripMenuItem" "Find objs with same texmap";
--		dotNet.AddEventHandler mapItem "Click" menuClickEvntHandler
--		mnu.Items.Add mapItem
		--texListCtrl.ContextMenuStrip = mnu
		
		setTexListSize()
		refreshTexmapList()
		if (not ALLSTOP) then
		(
			texListCtrl.AutoResizeColumns()
		)
	)
	
	--/////////////////////////////////////////////////////
	-- Position the preview bitmap window 
	--/////////////////////////////////////////////////////
	fn RsTexPreviewPosition =
	(
		if RsTexPreview.open and RsTexPreview.isDisplayed then
		(
			--is the parent rollout minimized?
			if GTXDTaggerUI.placement == #minimized then try(DestroyDialog RsTexPreview)catch()
			local GTXDTaggerUIPos = GetDialogPos GTXDTaggerUI
			SetDialogPos RsTexPreview [(GTXDTaggerUIPos.x + GTXDTaggerUI.width + 10), GTXDTaggerUIPos.y]
		)
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn isCombobox ctrl = 
	(
		(ctrl != undefined) and ((ctrl.gettype()).name == "DataGridViewComboBoxEditingControl")
	)
	
	--/////////////////////////////////////////
	--	EVENTS
	--/////////////////////////////////////////
	
	--/////////////////////////////////////////--/////////////////////////////////////////--/////////////////////////////////////////--/////////////////////////////////////////
	-- Update the preview bitmap when the cell is entered either by mouse or keypress
	--/////////////////////////////////////////--/////////////////////////////////////////--/////////////////////////////////////////--/////////////////////////////////////////
	on texListCtrl CellEnter ev arg do
	(
		--if we have anything selected
		if texListCtrl.selectedcells.count == 0 then return false
			
		--display and update the preview image window
		if RsTexPreview == undefined or (not RsTexPreview.open) then
		(
			createDialog RsTexPreview
			RsTexPreviewPosition()
		)
		else	--update the texture displayed
		(
			local img = bitmap RsTexPreview.bmTexPreview.width RsTexPreview.bmTexPreview.height
			
			local texmapIdx = texListCtrl.rows.item[arg.RowIndex].cells.item[0].tag
			local texMap = TCSList[texmapIdx]
			
			if (doesFileExist texMap.sourceTexture) then 
			(
				renderMap (bitmapTexture bitmap:(openBitmap texMap.sourceTexture)) filter:true into:img
			)
			
			RsTexPreview.bmTexPreview.bitmap = img
		)
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on texListCtrl CellClick ev arg do
	(
		if (arg.ColumnIndex == 2) and (arg.RowIndex > 0) do
		(
			ev.BeginEdit off
			if isCombobox ev.EditingControl do 
			(
				ev.EditingControl.DroppedDown = on
			)
		)
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on texListCtrl CellMouseUp ev arg do
	(
		if arg.RowIndex < 0 then return false
		
		if (arg.ColumnIndex == 2) then
		(
			
			
			--set the row colour to changed
			setEditedRow arg.RowIndex
			
			--add to list to enact changes
			addRowToChangedRowIdx arg.RowIndex
		)
	)
	
	local dropDownCtrl
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn onDropDownClosed ev arg =
	(		
		local texmapIdx = texmapListCtrl.rows.item[ev.EditingControlRowIndex].cells.item[0].tag
		
		local templateIdx = ev.SelectedIndex + 1

		--set the row colour to changed
		setEditedRow ev.EditingControlRowIndex
		
		--add to list to enact changes
		addRowToChangedRowIdx ev.EditingControlRowIndex
	)

	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on texmapListCtrl EditingControlShowing ev arg do
	(
		if (isCombobox arg.control) and (arg.control.tag == undefined) do
		(
			dropDownCtrl = arg.control
			dotNet.removeAllEventHandlers dropDownCtrl
			dotnet.addEventHandler dropDownCtrl "DropDownClosed" onDropDownClosed
		)
	)
	
	--/////////////////////////////////////////
	--Update the TCS files marked for change
	--/////////////////////////////////////////
	on btnMakeChanges pressed do
	(
		--print changedRowIdx
		local progMax = changedRowIdx.count
		local itemCount = 1
		progressStart "Update TCS Files:"
		for item in changedRowIdx do
		(
			WriteOutTCSFile item
			itemCount += 1
			setUpdatedRow item
			progressUpdate (100.0 * (itemCount / progMax))
		)
		
		progressEnd()
		
		changedRowIdx = #()
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btnRefresh pressed do
	(
		refreshTexmapList()
	)
	
	--/////////////////////////////////////////
	-- Apply the selected template to the currently chosen
	--texture list selection
	--/////////////////////////////////////////
	on btnApplySelected pressed do
	(
		local selectedCells = texListCtrl.SelectedCells 
		local selectedRows = for c=0 to (selectedCells.count - 1) collect selectedCells.item[c].rowIndex
		selectedRows = makeUniqueArray selectedRows
		
		for theRow in selectedRows do
		(		
			templateName = ddlTextureType.selected
			(texListCtrl.Item 2 theRow).value = templateName
			
			--update the row edited colour status
			setEditedRow theRow
			
			--add to list to enact changes
			addRowToChangedRowIdx theRow
		)
	)
	
	--/////////////////////////////////////////
	-- Search rows matching search string
	--/////////////////////////////////////////
	on btnSearchName pressed do
	(
		texListCtrl.ClearSelection()
		local searchStr = edtSearchName.text
		local rowNames = for i=0 to (texListCtrl.rows.count - 1) collect (DataPair name:texListCtrl.rows.item[i].cells.item[1].value id:i)
		
		local idxMatches = for item in rowNames where (MatchPattern item.name pattern:("*"+searchStr+"*") ignoreCase:true) collect item.id
			
		for id in idxMatches do
		(
			texListCtrl.rows.item[id].Selected = true
		)
		
	)
	
	--/////////////////////////////////////////
	-- Search rows matching type search string
	--/////////////////////////////////////////
	on btnSearchType pressed do
	(
		texListCtrl.ClearSelection()
		local searchStr = edtSearchType.text
		local rowTypes = for i=0 to (texListCtrl.rows.count - 1) collect (DataPair name:((texListCtrl.Item 2 i).EditedFormattedValue) id:i)
		print rowTypes
		local idxMatches = for item in rowTypes where (MatchPattern item.name pattern:("*"+searchStr+"*") ignoreCase:true) collect item.id
			
		for id in idxMatches do
		(
			texListCtrl.rows.item[id].Selected = true
		)
		
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on GTXDTaggerUI open do
	(
		banner.setup()
		RS_CustomDataGrid forceRecompile:true
		init()
	)
	
	on GTXDTaggerUI close do
	(
		ALLSTOP = True
		if ::RsTexPreview.open then try(DestroyDialog RsTexPreview) catch()
	)
	
)

createDialog GTXDTaggerUI style:#(#style_titlebar, #style_resizing, #style_sysmenu, #style_minimizebox, #style_maximizebox)
