------------------------------------------------------
-- MATERIAL PRESETS
-- MATT HARRAD [24/04/12]
-- ROCKSTAR NORTH
-- SAVES/LOADS THE SETTINGS OF A RAGE SHADER
------------------------------------------------------
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
try (destroyDialog  rs_materialPresets) catch()
-----------------------------------------------
rollout rs_materialPresets "Material Presets" width:200
(
	------------------------------------------------------------------------------------------
	-- LOCALS 
	------------------------------------------------------------------------------------------
	local iniPath = (theWildWest + @"script\3dsMax\_config_files\materialPresets\")		
	local specularSettings = dotNetObject "System.Collections.Hashtable"
	local specularFresnelVarName = "Specular Fresnel"
	local specularFalloffVarName = "Specular Falloff"
	
	------------------------------------------------------------------------------------------
	-- FUNCTIONS 
	------------------------------------------------------------------------------------------
	fn getSpecularSettings = 
	(
		local surfaceTypes = #()
		local specSettingsDat = openfile ( ( RsConfigGetWildWestDir() + "etc/config/general/spec_value_settings.dat" ) )
		
		while not ( eof specSettingsDat ) do 
		(
			local currentLine = readline specSettingsDat
			
			if currentLine[ 1 ] != "#" and currentLine != "" then 
			(
				local data = filterstring currentLine " \t\r\n"
				local surfaceType = data[ 1 ]
				local specFresnelMin = data[ 2 ] as float
				local specFresnelMax = data[ 3 ] as float
				local specFalloffMin = data[ 4 ] as float
				local specFalloffMax = data[ 5 ] as float
				
				-- Fixup surface type name to remove underscores and capitalize words.
				local splitNames = filterString surfaceType "_"
				local surfaceType = StringStream ""
				
				for splitName in splitNames do 
				(
					splitName = replace splitName 1 1 ( toUpper splitName[ 1 ] )
					format "% " splitName to:surfaceType
				)
				
				surfaceType = trimRight surfaceType " "
				
				specularSettings.Add surfaceType #( specFresnelMin, specFresnelMax, specFalloffMin, specFalloffMax )
				append surfaceTypes surfaceType
			)
		)
		
		sort surfaceTypes
		
		surfaceTypes
	)
	-----------------------------------------------------------
	fn assignSpecularSettings surfaceType = 
	(
		if specularSettings.contains( surfaceType ) then 
		(
			local surfaceSpecSettings = specularSettings.Item[ surfaceType ]
			local specFresnelMin = surfaceSpecSettings[ 1 ]
			local specFresnelMax = surfaceSpecSettings[ 2 ]
			local specFalloffMin = surfaceSpecSettings[ 3 ]
			local specFalloffMax = surfaceSpecSettings[ 4 ]
		
			mat = medit.GetCurMtl()
			
			if (classof mat == Rage_Shader) then 
			(
				local numVars = RstGetVariableCount mat
	
				for varIdx = 1 to numVars do 
				(
					local varName = RstGetVariableName mat varIdx

					if varName == specularFresnelVarName do RstSetVariable mat varIdx ( ( specFresnelMin + specFresnelMax ) / 2.0 )			
					if varName == specularFalloffVarName do  RstSetVariable mat varIdx ( ( specFalloffMin + specFalloffMax ) / 2.0 )					
					if (rs_materialPresets.chb_SetSpec.checked) do if varName == "Specular Intensity" do  RstSetVariable mat varIdx 1					
					if (rs_materialPresets.chb_SetBump.checked) do if varName == "Bumpiness" do  RstSetVariable mat varIdx 1					
				)
			)
		)
	)
	-----------------------------------------------------------
	fn get_presetList =
	(
		returnArray = #()
		presetFiles = getFiles (iniPath + "*.ini")		
		for i in presetFiles do
		(
			tempArray = filterString i @"\ /" 
			justName = tempArray[tempArray.count]
			justName = substring justName 1 (justName.count-4)
			append returnArray justName			
		)
		return returnArray
	)
	---------------------------------------------------------
	fn  load_matPreset = 
	(
		mat = medit.GetCurMtl()
		if (classof mat== Rage_Shader) then
		(
			presetName =  rs_materialPresets.ddl_presets.selected
			iniFile = iniPath +  presetName + ".ini"
			settingsArray = (getINISetting iniFile "settings")
			-- SET SHADER TYPE ----------------------
			if (rs_materialPresets.cbx_sType.checked) do
			(
				RstSetShaderName mat (getINISetting iniFile "settings" settingsArray[1])
			)
			-- SET SHADER VARS ----------------------
			for s in settingsArray do
			(
				for i = 1 to (RstGetVariableCount mat) where (RstGetVariableName mat i) == s do
				(	
					type = RstGetVariableType mat i
					ini_set = getINISetting iniFile "settings" s
					
					if (type == "texmap") then
					(
						if (rs_materialPresets.cbx_Textures.checked) do RstSetVariable mat i ini_set 
					)
					else
					(
						if (rs_materialPresets.cbx_settings.checked) do
						(
							if (type  == "float") do ini_set = ini_set as float
							if (type  == "vector3") do 
							(
								values = filterString ini_set  "[ , ]"
								temp_point = [0,0,0]
								temp_point[1]= values[1] as float
								temp_point[2]= values[2] as float
								temp_point[3]= values[3] as float
								ini_set = temp_point
							)
							if (type  == "vector4") do 
							(
								values = filterString ini_set  "[ , ]"
								temp_point = [0,0,0,0]
								temp_point[1]= values[1] as float
								temp_point[2]= values[2] as float
								temp_point[3]= values[3] as float
								temp_point[4]= values[4] as float
								ini_set = temp_point
							)						
							RstSetVariable mat i ini_set
						)
					)
				)
			)
		)
	)
	--------------------------------------------------------
	fn save_matPreset = 
	(
		presetName = rs_materialPresets.edt_name.text
		if (presetName!= "") do
		(
			safeToSave = true			
			iniFile = (iniPath + presetName + ".ini")
			if (doesFileExist iniFile) do
			(
				if not(queryBox "Preset already exists, Do you want to continue?" title:"Warning!") do safeToSave = false
			)
			if (safeToSave) do
			(
				mat = medit.GetCurMtl()
				if (classof mat== Rage_Shader) then
				(
					setINISetting iniFile "settings" "shaderName" (RstGetShaderName mat)	
					-----------------------------------------------------------
					for varN = 1 to (RstGetVariableCount mat) do
					(				
						varName = RstGetVariableName mat varN
						varVal = (RstGetVariable mat varN) as string
						setINISetting iniFile "settings" varName varVal						
					)	
				)
				rs_materialPresets.ddl_presets.items = (get_presetList())
			)
			rs_materialPresets.edt_name.text = "" -- RESET NAME FEILD
		)
	)
	------------------------------------------------------------------------------------------
	-- UI 
	------------------------------------------------------------------------------------------
	dotNetControl RsBannerPanel "Panel" pos:[0,0] height:32 width:rs_materialPresets.width
	local banner = makeRsBanner dn_Panel:RsBannerPanel versionNum:1.2 versionName:"Crabby Bag" wiki:"MaterialPresets" filename:(getThisScriptFilename())
	
	group "Make New Preset"
	(
		editText edt_name "Name : " width: 175 offset:[0,0]
		button btn_save "Save Selected Rage Material"  width:180
	)	
	group "Replace With Preset"
	(
		dropdownlist ddl_presets width: 175  items:(get_presetList())
		button btn_load "Set values On Shader" width:180
	)
	group "Options"
	(
		checkbox cbx_sType "Replace Shader Type" checked:true
		checkbox cbx_Textures "Replace Textures Paths" checked:true 
		checkbox cbx_settings "Replace Shader Values" checked:true 
	)
	group "Specular"
	(
		dropdownlist ddl_specSettings "" items:( getSpecularSettings())
		checkbox chb_SetSpec "Spec To 1" across:2 checked:true
		checkbox chb_SetBump "Bump To 1" checked:true
		button btn_assignSpecSettings "Set Specular on Shader" width:175		
	)

	------------------------------------------------------------------------------------------
	-- EVENTS 	
	------------------------------------------------------------------------------------------
	on btn_save pressed do save_matPreset()
	-----------------------------------------------------------
	on btn_load pressed do load_matPreset()
	-----------------------------------------------------------
	on rs_materialPresets open do 
	(
		rs_dialogPosition "get" rs_materialPresets	
		banner.setup()
	)
	-----------------------------------------------------------
	on rs_materialPresets close do rs_dialogPosition "set" rs_materialPresets
	-----------------------------------------------------------	
	on btn_assignSpecSettings pressed do 
	(
		local surfaceType = ddl_specSettings.items[ddl_specSettings.selection]
		assignSpecularSettings surfaceType
	)
)
createDialog rs_materialPresets style:#(#style_border,#style_toolwindow,#style_sysmenu)