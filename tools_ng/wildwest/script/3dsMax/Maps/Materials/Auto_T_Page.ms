-- AutoTPager  - Tool to create Tpages from textures in max

-- Rick Stirling
-- Rockstar North
-- March 2012



-- Load the usual WW headers
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

planeZheight = 250
planeVerticalOffset = 1
planeGridOffset = 20
planerelativesize = 1/100 as float
masterplanearray = #()




GlobalTextures = #()
ParentTextures = #()

flatParentTextureList = #()
flatGlobalTextureList = #()


objectTXDlist = undefined -- this holds the list of the objects in the scene, plus the TXD applied to them 
texturesByPTXD = #()
uniquetexturesByPTXD =#() 



struct RageMaterialCollection  
(
	matDiffMap = undefined,
	matSpecMap = undefined,
	matNormalMap = undefined,
	matAlphaMap = undefined,
	matEmmissiveMap = undefined,
	matEnvMap = undefined

)	





-- Function to grab all the TXDs on a group of objects
-- Usually this will be called with all the scene objects
fn WW_gatherTXDS objectselection = (
	allTXDTextures = #()
	local txdList = #()
	local txdObjList = #()
	
	RsGetTxdList objectselection txdList txdObjList

	txdlist -- return this
)






-- Function to grab all the textures in the TXDs provided
-- Used in conjunction with the WW_gatherTXDS function
fn WW_gatherTXDTextures txdlist = (
	allTXDTextures = #()

	texmaplist = #()
	maxsizelist = #()
	isbumplist = #()
	usagelist= #()
	for tx in txdList do
	(
		RsGetTexMapsByTxdNameNoStrip texmaplist maxsizelist isbumplist usagelist tx
		join allTXDTextures texmaplist
	)
		
	allTXDTextures -- return this
)





fn GrabSceneInfoLocalTXDs = 
(
	localTXDS =#()
	objectselection = #()
	-- Grab the txds from all the objects in the scene 

	for gobj in geometry do append objectselection gobj

	for obj in objectselection do 
	(
		try (attrIndex = GetAttrIndex (getAttrClass obj) "TXD") catch (attrIndex = "undefined")
		try (TXDattr = lowercase (GetAttr obj attrIndex)) catch (TXDattr = "undefined")
		append localTXDS #(lowercase(obj.name), TXDattr)
	)

	localTXDS -- return this.
)







fn GetSceneXMLTXDS sceneXMLfile = 
(
	-- Enable Xpath
	ExpClass = dotNetClass "System.Xml.XPath.XPathExpression"
	
	-- Get the scenexml data 
	stream = dotNetObject "System.IO.StreamReader" sceneXMLfile
	xmldoc = dotNetObject "System.Xml.XmlDocument"
	xmldoc.load stream
	xmlroot = xmldoc.documentElement
		
	
	-- Find the TXD list from the scene XML
	searchstring = "../scene/stats/txds/txd/@name"
	nodelist = xmlroot.Selectnodes (searchstring)

	sceneXMLTXDList = #()
	
	for nlindex = 0 to (nodelist.count-1)  do
	(
		thenode = nodelist.item[nlindex]
		xmlname = thenode.name
		xmlvalue = thenode.value
				
		appendIfUnique sceneXMLTXDList thenode.value
	)

	sceneXMLTXDList -- return the TXD list
)




-- Takes an array of TXDS and searches the PXTD files for them 
-- If any are found it builds a reverse tree for Parent TXDS
fn GetParentTXDStructure scenexmlTXDS = 
(
	foundTexturelist = #()
	
	-- Enable Xpath
	ExpClass = dotNetClass "System.Xml.XPath.XPathExpression"
	
	-- Get the xml data for parent TXDs
	parentTXDxml = 	(RsConfigGetAssetsDir core:True + "maps/ParentTxds.xml") 

	stream = dotNetObject "System.IO.StreamReader" parentTXDxml
	xmldoc = dotNetObject "System.Xml.XmlDocument"
	xmldoc.load stream
	xmlroot = xmldoc.documentElement
		
	
	-- loop through the array of passed in TXDS
	
	for suppliedTXDIndex = 1 to scenexmlTXDS.count do 
	(
		theTXD = scenexmlTXDS[suppliedTXDIndex]
		
		-- Find anything that uses that name in the XMl file 
		searchstring = "//@name"
		nodelist = xmlroot.Selectnodes (searchstring)
		nodelist.count --for quick debug to see that I'm getting data
		
		
		for nlindex = 0 to (nodelist.count-1)  do
		(
	
			thenode = nodelist.item[nlindex]
			xmlvalue = thenode.value
			
			-- If the supplied TXD is in the parent list, then build a backwards hierarchy list 
			if xmlvalue == theTXD then 
			(
				thenodeElement = thenode.OwnerElement 
				
				-- get the texture list for this TXD 
				txdname = thenodeElement.GetAttribute "name"
				txcount = (thenodeElement.GetAttribute "textureCount") as integer
				if txcount > 0 then
				(
					elementchildren = thenodeElement.childnodes
					elementchildren.count
					
					-- There are source textures to collect
					for i = 0 to (txcount - 1) do 
					(
						thetexture = elementchildren.itemof[i].getattribute "filename"						
						append foundTexturelist #("LT", txdname, thetexture )
					)	
				)
				
				else append foundTexturelist #("LT", txdname, "" )
				
				-- Thats the textures for the TXD found. Now we need all the textures that live in any of the parent txds 
				-- Get the  parent structure 
				-- Each time we need to jump up 2 levels to find the next containers 
				thenodeElementBlockGroup = thenodeElement.parentnode.parentnode -- probably a globalDictionary container block
				
				
				-- Does this global holder dictionary have any child textures?
				-- These are for the the block shared TXDS
				if thenodeElementBlockGroup.name == "globalDictionary" then 
				(
					blockGroupName = thenodeElementBlockGroup.GetAttribute "name"
					txcount = (thenodeElementBlockGroup.GetAttribute "textureCount") as integer
					if txcount > 0 then
					(
						elementchildren = thenodeElementBlockGroup.childnodes
						
						-- There are source textures to collect
						for i = 0 to (elementchildren.count - 1) do 
						(
							-- We only need the ones that are tagged as globalTextures
							if (elementchildren.itemof[i].name) == "globalTextures" then
							(
								textureStub = elementchildren.itemof[i].childnodes
								for tindex = 0 to (textureStub.count - 1) do 
								(
									thetexture = textureStub.itemof[tindex].getattribute "filename"	
									append foundTexturelist #("GP", areaName, thetexture )
								)
								
							)
						)
					)	
					
					else append foundTexturelist #("GP", blockGroupName, "")
				)	


				try (
					thenodeElementBlocks = thenodeElementBlockGroup.parentnode.parentnode -- probably a globalDictionary container block
					if thenodeElementBlocks.name == "globalDictionary" then 
					(
						blockName = thenodeElementBlocks.GetAttribute "name"
						txcount = (thenodeElementBlocks.GetAttribute "textureCount") as integer
						if txcount > 0 then
						(
							-- There are source textures to collect
						)
						
						else append foundTexturelist #("BL", blockName, "")
					)	
				) catch()
					
				
				
				try (
					thenodeElementAreaGroup = thenodeElementBlocks.parentnode.parentnode -- probably a globalDictionary container block
					if thenodeElementAreaGroup.name == "globalDictionary" then 
					(
						areaName = thenodeElementAreaGroup.GetAttribute "name"
						txcount = (thenodeElementAreaGroup.GetAttribute "textureCount") as integer
						if txcount > 0 then
						(
							elementchildren = thenodeElementAreaGroup.childnodes
							
							-- There are source textures to collect
							for i = 0 to (elementchildren.count - 1) do 
							(
								-- We only need the ones that are tagged as globalTextures
								if (elementchildren.itemof[i].name) == "globalTextures" then
								(
									textureStub = elementchildren.itemof[i].childnodes
									for tindex = 0 to (textureStub.count - 1) do 
									(
										thetexture = textureStub.itemof[tindex].getattribute "filename"	
										append foundTexturelist #("AR", areaName, thetexture )
									)
									
								)
							)
						)
						
						else append foundTexturelist #("AR", areaName, "")
						
					)	
				) catch()
				
				
				try (
					thenodeElementGlobalParent = thenodeElementAreaGroup.parentnode.parentnode -- probably a globalDictionary container block
					if thenodeElementGlobalParent.name == "globalDictionary" then 
					(
						globalName = thenodeElementGlobalParent.GetAttribute "name"
						txcount = (thenodeElementGlobalParent.GetAttribute "textureCount") as integer
						
						if txcount > 0 then
						(
							elementchildren = thenodeElementGlobalParent.childnodes
							
							-- There are source textures to collect
							for i = 0 to (elementchildren.count - 1) do 
							(
								-- We only need the ones that are tagged as globalTextures
								if (elementchildren.itemof[i].name) == "globalTextures" then
								(
									textureStub = elementchildren.itemof[i].childnodes
									for tindex = 0 to (textureStub.count - 1) do 
									(
										thetexture = textureStub.itemof[tindex].getattribute "filename"	
										append foundTexturelist #("GL", globalName, thetexture )
									)
									
								)
							)
						)
						
						else append foundTexturelist #("GL", globalName, "")
					)					
				)catch()
				
				
				
			)	
			
					
		) -- end search for the txd in the parenttxd file 

	) -- end looping of supplied txds 
	


	foundTexturelist -- return this list of textures split by txd

	
)
	




-- This function will take an array of textures in txds and remove duplicates from the child dictionaries
-- The array data is texture level, TXD name, then the texture name
fn sortUniqueTexturesByPTXD texturesByPTXD =
(
	
	-- Firstly split the data into subarrays
	localtxd = #()
	grouptxd = #()
	blocktxd = #()
	areatxd = #()
	globaltxd = #()
	
	cleanedlocaltxd = #()
	cleanedgrouptxd = #()
	cleanedblocktxd = #()
	cleanedareatxd = #()
	cleanedglobaltxd = #()
	
	
	for txindex = 1 to texturesByPTXD.count do 
	(
		texdata = texturesByPTXD[txindex]
		
		textureLevel = texdata[1]
		
		case textureLevel of 
		(
			"LT": append localtxd texdata
			"GP": append grouptxd texdata
			"BL": append blocktxd texdata
			"AR": append areatxd texdata
			"GL": append globaltxd texdata
			
		)
	)	
	
	--Now discard any textures in the arrays if they occur in any parent array.
	cleanedlocaltxd = #()
	duplicatelocaltxd = #()
	cleanedgrouptxd = #()
	cleanedblocktxd = #()
	cleanedareatxd = #()
	duplicateareatxd = #()
	cleanedglobaltxd = globaltxd -- We never remove from the global

	

	-- Compare the area to its parent global
	childarray = areatxd
	parentarray = cleanedglobaltxd
	
	for txindex = 1 to childarray.count do 
	(
		texdata  = childarray[txindex]
		searchtex = childarray[txindex][3]
		
		foundmatch = false
		for matchedIndex = 1 to parentarray.count do 
		(
			ptex = parentarray[matchedIndex][3]
			if  ptex == searchtex then foundmatch = true
		)	
		
		if foundmatch == false then (append cleanedareatxd texdata) else (append duplicateareatxd texdata)
	)	
		
	

	-- Now the local compared to all its parents 
	childarray = localtxd
	parentarray = cleanedglobaltxd + areatxd
	
	for txindex = 1 to childarray.count do 
	(
		texdata  = childarray[txindex]
		searchtex = childarray[txindex][3]
		
		foundmatch = false
		for matchedIndex = 1 to parentarray.count do 
		(
			ptex = parentarray[matchedIndex][3]
			if  ptex == searchtex then foundmatch = true
		)	
		
		if foundmatch == false then (append cleanedlocaltxd texdata) else (append duplicatelocaltxd texdata)
	)	
		
	
	returndata = #(cleanedlocaltxd, cleanedareatxd, cleanedglobaltxd) -- return these arrays
	
	
)





fn collectObjectMaterials mapMeshObject =
(
	-- Parse the geo to get the polycount and shader data
	-- Create a virtual copy of the mesh as a trimesh for better stats
	tempdatamesh = snapshotAsMesh mapMeshObject as TriMesh
	thepolycount = getnumfaces tempdatamesh
	uobjMaterials = #()
	
	-- Collect sub ids of materials used on this object
	meshMat = mapMeshObject.material
	materialClass = classof meshMat
	
	if (materialClass == Multimaterial) then
	(
		for facecount = 1 to thepolycount do
		(
			face = getFace tempdatamesh facecount 
			matid = getFaceMatId tempdatamesh facecount 
			appendifunique uobjMaterials matid
		)	
	)
	
	else 
	(
		-- nothing
	)	
	
	uobjMaterials = sort uobjMaterials

	
	myMatArray = #()
	
	
	-- Now get all the maps for the applied materials
	if (uobjMaterials.count > 0) then for submatindex = 1 to uobjMaterials.count do 
	(
		
		matref = uobjMaterials[submatindex]
		
		theMaterial = meshMat.materialList[matref]

		
		if ((classof theMaterial) == Rage_Shader) then
		(	

			submat = RageMaterialCollection matDiffMap: undefined
			
			-- collect the material information
			theMaterial = meshMat.materialList[matref]
			
			-- Arrays for storing data
			RageSlotNames =#()
			RageSlotTypes =#()
			
			-- Need to know the shadertype, this dictates the slot we need
			RageShaderName = RstGetShaderName theMaterial
			--submat.matShaderType = RageShaderName
			RageShaderSlotCount = RstGetVariableCount theMaterial
			
			-- Populate the arrays
			for ss = 1 to RageShaderSlotCount do
			(
				append RageSlotNames (lowercase (RstGetVariableName theMaterial ss))
				append RageSlotTypes (RstGetVariableType theMaterial ss)
			)

			-- Now find the texture maps from that shader 
			for texMapList = 1 to RageSlotTypes.count do
			(
				if RageSlotTypes[texMapList] == "texmap" then
				(
					theMapType = RageSlotNames[texMapList]
					theTexture =  lowercase (RstGetVariable theMaterial texMapList)

					-- Populate the struct with the maps
					case theMapType of
					(
						"diffuse texture": submat.matDiffMap = theTexture 
						"specular texture": submat.matSpecMap  = theTexture 
						"bump texture": submat.matNormalMap = theTexture 
						"environment texture" : submat.matEnvMap = theTexture 
					)	
				)	-- end found texture map loop 
			) -- end shader variable loop
			
			
			appendifunique myMatArray submat
		)	-- end looping through all the materials in the object.

				

		
		else 
		(
			smi = (uobjMaterials[submatindex] as string)
			messagebox ("I couldn't find a valid Rage shader for material index " + smi + " on object " + mapMeshObject.name)
			return undefined
		)
		
	)	
	
	myMatArray -- return this  data	
)



fn getimagesize inputimage = 
(
	
	bmp_w = 256
	bmp_h = 256
	
	try
	(
		loadedimage = openbitmap inputimage
		bmp_w = loadedimage.width
		bmp_h = loadedimage.height 
		close loadedimage
	) 
		
	catch( 
		messageBox ("Couldn't get image size for " + inputimage as string + " - you may not have this texture locally") 
	)

	
	imagesize = #(bmp_w,bmp_h) -- return the size
)	



-- Create Image planes
-- Given a struct containing textures, create a layers of planes
fn createLayeredImagePlane textureData planestartpos planeName =
(
	
	-- Grab all the images
	-- Some might be undefined, that's fine
	dmfile = textureData.matDiffMap
	nmfile = textureData.matNormalMap
	smfile = textureData.matSpecMap
	
	shadermaps = #()
	append shadermaps dmfile 
	append shadermaps nmfile 
	append shadermaps smfile 
	
	imageplanelist = #()
	
	for planeLayer = 1 to shadermaps.count do 
	(
		if shadermaps[planeLayer] != undefined then 
		(	
			theImageSize = (getimagesize shadermaps[planeLayer]) 
			thePlanePos = planestartpos + ([0,0,-planeVerticalOffset]*planeLayer)
			ipw = theImageSize[2] * planerelativesize
			iph = theImageSize[1] * planerelativesize
			theImagePlane = Plane length:ipw width:iph pos:thePlanePos isSelected:on lengthsegs: 1 widthsegs: 1
			theImagePlane.name = "imPla_" + (planeLayer as string)
			
			addModifier theImagePlane (Materialmodifier materialid:planeLayer)
			convertto theImagePlane editable_poly
			
			-- apply the texturemap to the new plane 
			new_material = Standardmaterial ()
			new_material.diffuseMap = Bitmaptexture fileName: shadermaps[planeLayer]
			new_material.showInViewport = true
			
			theImagePlane.material = new_material
			theImagePlane.showVertexColors = on

			
			append imageplanelist theImagePlane
		)	
	)	
	
	-- join all the planes in the plane list 
	for attachObject in 2 to imageplanelist.count do
	(
			polyop.attach imageplanelist[1] imageplanelist[attachObject]				
	)

	imageplanelist[1].name = planeName
	
	imageplanelist[1] -- return this
)




fn createMaterialPlanes planeShaderArray planestartpos  = 
(
	
	thePlaneArray = #()
	-- loop through the materials and punt them off to functions
	
	-- I need to lay these out in a grid, give myself some breathing space too 
	numshaders = planeShaderArray.count
	planegridsize = ((sqrt numshaders)+1) as integer 
	
	rowPos = 1
	columnPos = 1
	columnChk = planegridsize 
	
	for gatheredIndex = 1 to numshaders do 
	(	
		textureData = planeShaderArray[gatheredIndex]
		
		-- What is the position of this plane?
		if gatheredIndex > columnChk then
		(
			columnChk = columnChk + planegridsize
			rowPos = 1
			columnPos = columnPos + 1
		)	
		
		planeXoffset = rowPos * planeGridOffset
		planeYoffset = columnPos * planeGridOffset
		theplanepos = planestartpos + [planeXoffset,planeYoffset,0]
		
		
		-- Create the plane
		PlaneName = "imPla_" + (gatheredIndex as string)
		theNewPlane = createLayeredImagePlane textureData theplanepos planename
		
		append thePlaneArray theNewPlane
		
		rowPos = rowPos + 1
	)
	
	thePlaneArray -- return this

)




fn cleanupPlanes = 
(
	for obj in masterplanearray do delete obj 
	masterplanearray = #()
	textureArray = #()
)	






fn sortPlanesbysize sortplanearray =
(

	storagearray =#()
	for p = 1 to sortplanearray.count do 
	(	
		theplane = getNodeByName sortplanearray[p]

		planesize = nodeLocalBoundingBox theplane
		planex = (planesize[2][1] - planesize[1][1])
		planey = (planesize[2][2] - planesize[1][2]) 
		
		if planex < 0 then planex = planex * -1
		if planey < 0 then planey = planey * -1
			
		largestside = planey
		if planex > planey then largestside = planex 
		
		surfacearea = planex * planey
		
		append storagearray #(surfacearea, sortplanearray[p])
	)	
	

	qsort storagearray sortArraybySubIndex subIndex:1	
	
	storagearray -- return the array sorted by surface area
)



-- Take an array of planes and sort them.
-- Use the material ids to build an inidex like a bit array
fn sortPlanesByLayers sortplanearray =
(
	
	initialPlanePos = sortplanearray[1].pos
	planeNames =#()

	-- Rename the planes based on the number of material layers that they have
	for p = 1 to sortplanearray.count do 
	(	
		theplane = sortplanearray[p]
				
		tempdatamesh = snapshotAsMesh theplane as TriMesh
		thepolycount = getnumfaces tempdatamesh

		
		-- Collect sub ids of materials used on this object
		matidlist = #()
		for facecount = 1 to thepolycount do
		(
			face = getFace tempdatamesh facecount 
			matid = getFaceMatId tempdatamesh facecount 
			appendifunique matidlist matid
		)	
		
		layerbitarray = 0
		for x = 1 to matidlist.count do
		(
			layerbitarray = layerbitarray + matidlist[x]
		)
		
		theplane.name = ((layerbitarray as string) + "_" + theplane.name )
		append planeNames theplane.name
	)
	
	planeNames = sort planeNames 
	
	
	
	-- Now reorder the planes based on materaisl and the size
	
	-- THIS IS A MESS!
	-- REALLY, ITS A SHAMBLES
	
	-- But I'm getting hacked off, so brute force is in and elegance can go on holiday 
	pa1 = #()
	pa2 = #()
	pa3 = #()
	pa4 = #()
	pa5 = #()
	pa6 = #()
	
	-- now split this array in arrays based on the first Number
	for theplane in planeNames do 
	(	
		firstdigit = substring theplane 1 1
		case firstdigit of
		(
			"1": append pa1 theplane
			"2": append pa2 theplane
			"3": append pa3 theplane
			"4": append pa4 theplane
			"5": append pa5 theplane
			"6": append pa6 theplane
		)	
	)
	
	-- Sort each of these by size
	pa1 = sortPlanesbysize pa1
	pa2 = sortPlanesbysize pa2
	pa3 = sortPlanesbysize pa3
	pa4 = sortPlanesbysize pa4
	pa5 = sortPlanesbysize pa5
	pa6 = sortPlanesbysize pa6
	

	
	-- Consrtuct a final sorted Array
	sortedplaneArray = #()
	for p = 1 to pa1.count do append sortedplaneArray pa1[p][2]
	for p = 1 to pa2.count do append sortedplaneArray pa2[p][2]
	for p = 1 to pa3.count do append sortedplaneArray pa3[p][2]
	for p = 1 to pa4.count do append sortedplaneArray pa4[p][2]
	for p = 1 to pa5.count do append sortedplaneArray pa5[p][2]
	for p = 1 to pa6.count do append sortedplaneArray pa6[p][2]
		
	
	-- get the size of the largest texture 
	
	largestTextureArea = 0
	try (if pa1[pa1.count][1] > largestTextureArea then largestTextureArea = pa1[pa1.count][1])catch()
	try (if pa2[pa2.count][1] > largestTextureArea then largestTextureArea = pa2[pa2.count][1])catch()
	try (if pa3[pa3.count][1] > largestTextureArea then largestTextureArea = pa3[pa3.count][1])catch()
	try (if pa4[pa4.count][1] > largestTextureArea then largestTextureArea = pa4[pa4.count][1])catch()
	try (if pa5[pa5.count][1] > largestTextureArea then largestTextureArea = pa5[pa5.count][1])catch()
	try (if pa6[pa6.count][1] > largestTextureArea then largestTextureArea = pa6[pa6.count][1])catch()
		
	
	-- Ok, reposition the planes
	-- the default size is 512x512, if it's bigger then we need to make all the dimensions larger
	
	
	planegridsize = ((sqrt sortedplaneArray.count)+1) as integer 
	rowPos = 1
	columnPos = 1
	columnChk = planegridsize 
	
	newplanepos =#()
	
	for theplaneindex = 1 to sortedplaneArray.count do 
	(	
		theplane = sortedplaneArray[theplaneindex]
		
		-- What is the position of this plane?
		if theplaneindex > columnChk then
		(
			columnChk = columnChk + planegridsize
			rowPos = 1
			columnPos = columnPos + 1
		)	
		
		planeXoffset = rowPos * ((sqrt largestTextureArea)+1) as integer 
		planeYoffset = columnPos * ((sqrt largestTextureArea)+1) as integer 
		theplanepos = initialPlanePos + [planeXoffset,planeYoffset,0]
		append newplanepos theplanepos
		
		rowPos = rowPos + 1
	)	
	
	

	
	for p = 1 to sortplanearray.count do 
	(
		theplane = getnodebyname planeNames[p]
		theplane.pos = newplanepos[p]
		theplane.name = substring theplane.name 3 255
	)
	
)	
	






fn removeDuplicateMStruct textureArray = 
(
	cleanarray = #()
	dtmarray=#(undefined)
	ntmarray=#(undefined)
	stmarray=#(undefined)
	
	for ms = 1 to textureArray.count do 
	(	
		-- collect the textures from that mstruct
		dtm = textureArray[ms].matDiffMap
		ntm = textureArray[ms].matNormalMap
		stm = textureArray[ms].matSpecMap
		
		-- check to see if those texutres have already been found
		foundmatch = false
		for dmc = 1 to dtmarray.count do 
		(
			previousdtm = dtmarray[dmc]
			if previousdtm == dtm then
			(	
				foundmatch = true
			)
		)	

		if foundmatch == false then
		(
			-- The texture didn't previously exist, so record it as a new textures 
			append dtmarray dtm
			append cleanarray textureArray[ms]
		)	
		
		
		if foundmatch == true then
		(
			-- The texture did previously exist, but the normal map may not have.

		)		
		
	)
	
	cleanarray -- return this
	
)	







-- With a list of textures in a TXD, sort the textures on the model into packed and nonpacked
fn matchTXDStoMstructs textureArray selectedTXDs = 
(
	txdmatchlist =#()
	nontxdlist = #()
	
	for ms = 1 to textureArray.count do 
	(	
		-- collect the textures from that mstruct
		dtm = RSMakeSafeSlashes (textureArray[ms].matDiffMap)
		ntm = RSMakeSafeSlashes (textureArray[ms].matNormalMap)
		stm = RSMakeSafeSlashes (textureArray[ms].matSpecMap)
		
		txdmatch = false
		
		for theindex = 1 to selectedTXDs.count do 
		(
			txdtexture = lowercase selectedTXDs[theindex]
			if txdtexture == dtm then txdmatch = true
			if txdtexture == ntm then txdmatch = true
			if txdtexture == stm then txdmatch = true
			
		)	
		
		if txdmatch == true then append txdmatchlist textureArray[ms] else append nontxdlist textureArray[ms]
		
	)	
	
	splitList = #(txdmatchlist, nontxdlist) --return this
	
)	







-- With a list of textures in a TXD, sort the textures on the model into local and parent
fn discardPTXDtextures textureArray = 
(
	gTXDmatchList =#()
	pTXDmatchList =#()
	localTXDList = #()
	
	for ms = 1 to textureArray.count do 
	(	
		-- collect the textures from that mstruct
		dtm = RSMakeSafeSlashes (textureArray[ms].matDiffMap)
		ntm = RSMakeSafeSlashes (textureArray[ms].matNormalMap)
		stm = RSMakeSafeSlashes (textureArray[ms].matSpecMap)
		
		txdmatch = false
		
		
		-- I have gathered all the PTXD textures when the tool starts up.
		
		-- I put these here to get their names	
		--	parentTXDarray 
		--	GlobalTextures 
		--	ParentTextures 
		
		-- Look for any global TXDS first
		gTextEntryName = undefined
		for theindex = 1 to GlobalTextures.count do 
		(
			gTextEntry = GlobalTextures[theindex]
			gTextEntryName = gTextEntry[1]
			
			for gtx = 1 to gTextEntry[2].count do 
			(
				txdtexture = RSMakeSafeSlashes (lowercase gTextEntry[2][gtx])
				if ((txdtexture == dtm) or (txdtexture == ntm) or (txdtexture == stm)) then 
				(	
					txdmatch = true
				)	
			)
		)	
		
		if txdmatch == true then append gTXDmatchList #(textureArray[ms], gTextEntryName, "")
		txdmatch = false
		
		
		-- Now the parent TXDS
		pTextEntryName = undefined
		for theindex = 1 to ParentTextures.count do 
		(
			pTextEntry = ParentTextures[theindex]
			pTextEntryName = pTextEntry[1]
			
			for ptx = 1 to pTextEntry[2].count do 
			(
				txdtexture = RSMakeSafeSlashes (lowercase pTextEntry[2][ptx])
				if ((txdtexture == dtm) or (txdtexture == ntm) or (txdtexture == stm)) then 
				(	
					txdmatch = true
				)	
			)
		)			
		
		
		-- Finished looking at this texture, if it has not already been added to a GTXD or PTXD list, I'll keep it 
		if txdmatch == true then append pTXDmatchList #(textureArray[ms], pTextEntryName, "")
			
		if txdmatch == false then append localTXDList textureArray[ms]
		
	)	
	
	
	
	splitList = #(txdmatchlist, nontxdlist) --return this
	
)	







-- Taking the textures from the objects, sort those into ones that are in other txds
-- The other txds were gathered by the initialisation function 
fn sortTexturesByTXD textureArray =
(

	-- the data is stored in uniquetexturesByPTXD 
	-- this is the local TXDs, the area TXDS and the global TXDs
	
	gTXDmatchList =#()
	aTXDmatchList =#()
	pTXDmatchList =#()
	localTXDList = #()
	
	
	-- Extract just the texture names from the data arrays
	localtxds = uniquetexturesByPTXD[1] 
	areatxds = uniquetexturesByPTXD[2]
	globaltxds = uniquetexturesByPTXD[3]
		

	
	-- Now look at the textures in the texture arrays and see if they were in the texture dictionaries
	for ms = 1 to textureArray.count do 
	(	
		match = false
		
		-- collect the textures from that mstruct
		dtm = RSMakeSafeSlashes (textureArray[ms].matDiffMap)
		ntm = RSMakeSafeSlashes (textureArray[ms].matNormalMap)
		stm = RSMakeSafeSlashes (textureArray[ms].matSpecMap)
		
		-- Search for these textures in the arrays 
		for x = 1 to globaltxds.count do 
		(
			if dtm == globaltxds[x][3] do (appendIfUnique gTXDmatchList textureArray[ms]; match=true)
			if ntm == globaltxds[x][3] do (appendIfUnique gTXDmatchList textureArray[ms]; match=true)
			if stm == globaltxds[x][3] do (appendIfUnique gTXDmatchList textureArray[ms]; match=true)				
		)	

		for x = 1 to areatxds.count do 
		(
			if dtm == areatxds[x][3] do (appendIfUnique aTXDmatchList textureArray[ms]; match=true)
			if ntm == areatxds[x][3] do (appendIfUnique aTXDmatchList textureArray[ms]; match=true)
			if stm == areatxds[x][3] do (appendIfUnique aTXDmatchList textureArray[ms]; match=true)					
		)			

		for x = 1 to localtxds.count do 
		(
			if dtm == localtxds[x][3] do (appendIfUnique pTXDmatchList textureArray[ms]; match=true)
			if ntm == localtxds[x][3] do (appendIfUnique pTXDmatchList textureArray[ms]; match=true)
			if stm == localtxds[x][3] do (appendIfUnique pTXDmatchList textureArray[ms]; match=true)					
		)	
		
		if match == false then (appendIfUnique localTXDList textureArray[ms])
		
	)	


	splitList = #(gTXDmatchList, aTXDmatchList, pTXDmatchList, localTXDList)
	
)	






fn nameplanearray textpos captiontext =
(
	thecaption = Text text:captiontext  size:5 kerning:0 leading:0 pos:textpos  isSelected:off
	thecaption.render_displayRenderMesh = true
	thecaption.render_thickness = 0.25
	
	thecaption.name = captiontext
	
	thecaption 
)






fn getaverageoffsetpos objectarray offsetpos =
(
	temppos = [0,0,0]
	for obj in objectarray do temppos = temppos + obj.pos
	temppos = temppos / objectarray.count
	temppos = temppos + offsetpos
)





fn createThePlanes objectselection = (

	planestartpos = objectselection[1].pos -- this'll come from the UI ?
	planestartpos.x = planestartpos.x as integer
	planestartpos.y = planestartpos.y as integer
	planestartpos.z = planeZheight as integer

	textureArray = #()

	-- collect the shaders
	for theobjindex = 1 to objectselection.count do (
		objmatarray = collectObjectMaterials objectselection[theobjindex]
	 	for mstruct in objmatarray do 
	 	(		
	 		append textureArray mstruct
	 	)	
	)


	textureArray = removeDuplicateMStruct textureArray
	
	
	splitbyTXDs = sortTexturesByTXD textureArray

	
	if textureArray != undefined then 
	(
		planeShaderArray = splitbyTXDS[4]
		if planeShaderArray.count > 0 then 
		(	
			ltxdarray = createMaterialPlanes planeShaderArray planestartpos
			textpos = getaverageoffsetpos ltxdarray [0,0,10]
			plparent = nameplanearray textpos "LOCAL"
			sortPlanesByLayers ltxdarray
			for p in ltxdarray do p.parent = plparent
			append masterplanearray ltxdarray
		)	
		
		
		planeShaderArray = splitbyTXDS[3]
		if planeShaderArray.count > 0 then 
		(	
			ptxdarray = createMaterialPlanes planeShaderArray (planestartpos + [(4*textureArray.count),0,0])
			textpos = getaverageoffsetpos ptxdarray [0,0,10]
			plparent = nameplanearray textpos "PARENT"
			sortPlanesByLayers ptxdarray 
			for p in ptxdarray do p.parent = plparent
			append masterplanearray ptxdarray
		)	
		
		
		planeShaderArray = splitbyTXDS[2]
		if planeShaderArray.count > 0 then 
		(	
			atxdarray = createMaterialPlanes planeShaderArray (planestartpos + [(4*textureArray.count),(4*textureArray.count),0])
			textpos = getaverageoffsetpos atxdarray [0,0,10]
			plparent = nameplanearray textpos "AREA"
			sortPlanesByLayers atxdarray 
			for p in atxdarray do p.parent = plparent
			append masterplanearray atxdarray
		)
		
		
		planeShaderArray = splitbyTXDS[1]
		if planeShaderArray.count > 0 then 
		(	
			gtxdarray = createMaterialPlanes planeShaderArray (planestartpos + [(4*textureArray.count),(4*textureArray.count),0])
			textpos = getaverageoffsetpos gtxdarray [0,0,10]
			plparent = nameplanearray textpos "GLOBAL"
			sortPlanesByLayers gtxdarray 
			for p in gtxdarray do p.parent = plparent
			append masterplanearray gtxdarray	
			
		)
		
		
	)	

	
	
)





fn initialisedata =
(
	
	-- What is the container and the scemeXML?
	RsProjectContentReload()
	maps = RsMapGetMapContainers()
	mapname =  lowercase(maps[1].name)
	mapZipNode = ( RsProjectContentFind mapname type:"mapzip" )
	

	RsMapSceneXmlFilename = ""
	for c in mapZipNode.children do
	(
		if ( undefined != ( findString c.filename ".xml" ) ) then RsMapSceneXmlFilename = mapZipNode.path + "/" + c.filename
	)

	-- I need to clean RsMapSceneXmlFilename to remove double //
	sceneXMLfile = RSMakeSafeSlashes RsMapSceneXmlFilename
	
	
	-- Sync that SceneXML file 
	try
	(
		depotPath = gRsPerforce.local2depot RsMapSceneXmlFilename
		gRsPerforce.p4.run "sync" #(depotPath) silent:true
	)
	catch(print "Couldnt sync the sceneXML file")

	
	-- Get the TXDS from the scene XML 
	scenexmlTXDS = GetSceneXMLTXDS sceneXMLfile
	
	-- Find out which of these TXDs are the Parent TXD listview and build a hierarchy structure 
	texturesByPTXD  = GetParentTXDStructure scenexmlTXDS
	
	-- Organise this data 
	uniquetexturesByPTXD = sortUniqueTexturesByPTXD texturesByPTXD
	
)	




fn DumpSelectedTextures theoutputfoldername =
(

	fileOutputPath = "c:/temp/"+ theoutputfoldername + "/"
	makeDir (fileOutputPath)
	
	
	plobjectselection = getCurrentSelection()
	if plobjectselection.count > 1 then 
	(	
		myMatArray=#()
		for pobj in plobjectselection do 
		(
			pobjname = pobj.name
			
			if (lowercase (substring pobjname 1 6) == "impla_" ) then
			(
				-- Parse the geo to get the polycount and shader data
				-- Create a virtual copy of the mesh as a trimesh for better stats
				tempdatamesh = snapshotAsMesh pobj as TriMesh
				thepolycount = getnumfaces tempdatamesh
				uobjMaterials = #()
				
				for facecount = 1 to thepolycount do
				(
					face = getFace tempdatamesh facecount 
					matid = getFaceMatId tempdatamesh facecount 
					appendifunique uobjMaterials matid
				)	
				
				uobjMaterials = sort uobjMaterials
				
				meshMat = pobj.material
				
				if classOf meshMat == Multimaterial then
				(
					for submatindex = 1 to uobjMaterials.count do 
					(
						theMaterial = meshMat.materialList[submatindex]
						themap = theMaterial.diffuseMap.filename
						appendifunique myMatArray themap
					)	
				)	
				
				if classOf meshMat == Standardmaterial then
				(
					theMaterial = meshMat
					themap = theMaterial.diffuseMap.filename
					appendifunique myMatArray themap
				)
				
			)	-- end looping through all the valid plane object
							

			else 
			(
				messageBox (pobjname + " does not seem to be an image plane, skipping")
			)
			
			-- Dump the textures now
			for f in myMatArray do
			(
				copyFile f (fileOutputPath + "/"+ getFilenameFile f + getFilenameType f)
			)	
			
			
		)	--end looping through all the supplied objects	
			
	) -- there were some objects selected	
	
	
	else
	(
		messageBox "Please select image planes to dump the textures from."
	)
	
)	


try(DestroyDialog autoTpager)catch()


rollout autoTpager "Auto Tpager" width:200 height:300
(
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:autoTpager.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"autoTpager" filename:(getThisScriptFilename())

	button btnSelectTXDbyMesh "Choose TXD of selected object" width:180 height:30
	dropdownlist ddlLTXDSelection  "Scene TXDs" width:180 height:40 items:#() --pos:[25,20]
	
	
	button btnSelectbyTXD "Select objects from TXD" width:180 height:30
	button btnbuildTexturePlanes "Build Texture Planes" width:180 height:30
	button btnCleanupPlanes "Delete the texture planes" width:180 height:30
	
	EditText edtDumpSelectedTexturesFolder text:"outputfoldername" width:180 height:20
	button btnDumpSelectedTextures "Dump Selected Textures" width:180 height:30
	
	on autoTpager open do 
	(
		banner.setup()
		objectTXDlist = GrabSceneInfoLocalTXDs()
	
		--populate the TXD list 
		objbytxd = #()
		
		for t = 1 to objectTXDlist.count do 
		(
			thetxd = lowercase (objectTXDlist[t][2])
			appendIfUnique objbytxd thetxd
		)
	
		autoTpager.ddlLTXDSelection.items = objbytxd
		clearSelection()
		
		-- Now get the parentTXD split 
		initialisedata()
		
	)
	


	on btnSelectbyTXD pressed do 
	(
		meshselection = #()
		if undefined == autoTpager.ddlLTXDSelection.selected then
		(
			messagebox "No TXD selected"
			return false
		)	
		
		else
		(
			-- get the selected txd from the UI
			thechosentxd = autoTpager.ddlLTXDSelection.selected
			
			-- now get all the objects that use that txd 
			for meshindex = 1 to objectTXDlist.count do 
			(
				themeshobject = objectTXDlist[meshindex][1]
				themeshtxd = objectTXDlist[meshindex][2]
				
				if themeshtxd == thechosentxd then append meshselection (getNodeByName themeshobject)
			)	
		)
		
		-- Now meshselection is a list of objects that use the selected TXD
		clearSelection()
		for obj in meshselection do selectmore obj
		
	)	
	
	
	
	on btnbuildTexturePlanes pressed do 
	(
		-- Check that objects are selected
		objectselection = getCurrentSelection()
		if objectselection.count > 0 then 
		(	
			createThePlanes objectselection
		)	
		
		else 
		(
			messageBox "No objects selected - please select objects by TXD"
		)
	)	
	
	
	
	on btnSelectTXDbyMesh pressed do 
	(
		cobjectselection = getCurrentSelection()
		if cobjectselection.count == 1 then 
		(	
			selobj = cobjectselection[1]
			attrIndex = GetAttrIndex (getAttrClass selobj) "TXD"
			TXDattr = lowercase (GetAttr selobj attrIndex)
			
			--try to set the UI DDL to match the TXD on the object
			
			try
			(
				autoTpager.ddlLTXDSelection.selected = TXDattr
			)

			catch 
			(
				messageBox "Unable to find the selected objects TXD in the UI. Hmm. You should never get this message."
			)	
		)	
		
		else
		(
			messageBox "Please select a single object."
		)
		
	)	
	
	on btnCleanupPlanes pressed do cleanupPlanes()
	
	
	on btnDumpSelectedTextures pressed do DumpSelectedTextures autoTpager.edtDumpSelectedTexturesFolder.text
)


createDialog autoTpager


