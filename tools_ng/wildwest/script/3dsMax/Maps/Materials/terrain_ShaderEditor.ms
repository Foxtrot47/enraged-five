try (gRsTerrainUberEditor.ToolForm.Close()) catch ()
Callbacks.RemoveScripts id:#RsTerrainShaderEditor
	
global gRsTerrainUberEditor

filein (RsConfigGetWildwestDir() + "Script/3dsMax/Maps/Materials/terrain_helperFunctions.ms")
RSTA_LoadCommonFunction #("fn_RSTA_ThumbsMgr.ms")

struct RsTerrainUberEditor 
(
	rsBannerPanel = DotNetObject "Panel",
	bannerStruct = MakeRsBanner dn_Panel:rsBannerPanel versionNum:1.21 versionName:"Wobbly Fidget",
	
	-- Currently-shown material-descriptors:
	MatInfoList = #(),
	
	-- Tool's form and defined controls (so functions can quickly find them)
	toolForm, editPanel, browsePanel, texmapPanels = #(),
	texMapRCmenu,
	
	formSize = [800, 670],
	editPanelWidth = 300,
	
	-- Thumbnail-manager:
	ToolName = #TerrainUberEdit,
	Thumbs = (RSTA_ThumbsMgr.GetThumbsStruct ToolName),
	
	dnTooltip = (dotNetObject "ToolTip"),
	dnColour = (dotNetClass "System.Drawing.Color"),
	SizeType_AutoSize = (dotNetClass "SizeType").AutoSize,
	SizeType_Absolute = (dotNetClass "SizeType").Absolute,
	SizeType_Percent = (dotNetClass "SizeType").Percent,
	
	dnContextMenuStrip = DotNetClass "ContextMenuStrip",
	dnToolStripMenuItem = DotNetClass "ToolStripMenuItem",
	dnToolStripSeparator = DotNetClass "ToolStripSeparator",
	
	dnLabel = DotNetClass "Label",
	dnGroupBox = DotNetClass "GroupBox",
	dnFlowLayoutPanel =  DotNetClass "FlowLayoutPanel",
	dnTableLayoutPanel =  DotNetClass "TableLayoutPanel",
	dnNumericUpDown = DotNetClass "NumericUpDown",
	dnButton = DotNetClass "Button",
	dnCheckBox = DotNetClass "CheckBox",
	
	-- Common padding/margin sizes
	padding0 = (DotNetObject "Padding" 0 0 0 0),
	spnMargin = (DotNetObject "Padding" 2 0 2 2),
	outClrPanelMargin = (DotNetObject "Padding" 0 0 2 0),
	outClrPanelPadding = (DotNetObject "Padding" 2 2 2 2),
	clrPanelPadding = (DotNetObject "Padding" 3 3 3 3),	
	
	HighlightColour = dnColour.Orange,
	FormClr, WindowClr, TextClr,
	
	WM_SETREDRAW = 0xB,
	
	-- Show materials as expanded by default?
	initiallyShowExpanded = False,
	--Show object materials only checked (false displays all the shaders in a multi-sub , true will just display shaders applied to the selected mesh)
	justObjMatIdsChecked = true,
	
	------------------------------------------------------------------------------
	-- IsBtnChecked:
	--	Returns 'True' if a button has been set to 'Flat' style:
	------------------------------------------------------------------------------
	fn IsBtnChecked thisBtn = 
	(
		return (thisBtn.flatStyle == thisBtn.flatStyle.flat)
	),
	
	------------------------------------------------------------------------------
	-- SetBtnChecked:
	--	Sets button style to make it look checked or not
	------------------------------------------------------------------------------
	fn SetBtnChecked thisBtn checked = 
	(
		if checked then 
		(
			-- Don't allow button to autosize in this mode:
			local btnWidth = thisBtn.width
			local btnHeight = thisBtn.height
			thisBtn.autoSize = False
			thisBtn.width = btnWidth
			thisBtn.Height = btnHeight
			
			-- These colours are only shown if button is set to 'Flat' style, 
			thisBtn.backColor = dnColour.orange
			thisBtn.foreColor = dnColour.black
			
			thisBtn.flatStyle = thisBtn.flatStyle.flat
		)
		else 
		(
			thisBtn.flatStyle = thisBtn.flatStyle.system
			thisBtn.autoSize = True
		)

		thisBtn.Refresh()
	),
	
	-- Get list descriptors for Terrain_Uber materials on selected objects:
	fn GetSelObjTerrainMatsInfo = 
	(
		-- Collect rage-materials applied to selected objects:
		--	(includes materials that aren't applied to specific faces)
		local SelMats = #()
		local SelMatInfos = #()
		for Obj in GetCurrentSelection() do 
		(
			local ObjMatInfos = (gRsMaterials.GetObjMatData obj justObjMatIds:justObjMatIdsChecked flatList:True)
			
			-- Collect unique materials:
			for MatInfo in ObjMatInfos where (appendIfUnique SelMats MatInfo.Mat) do 
			(
				append SelMatInfos MatInfo
			)
		)
		
		-- Order by MatId
		fn SortByMatId v1 v2 = (v1.matId - v2.matId)
		Qsort selMatInfos SortByMatId
		
		-- Collect terrain-material descriptors; filter out invalid descriptors:
		local TerrMatDefs = for MatInfo in SelMatInfos collect (RsTerrainMatInfo TerrainMat:MatInfo.Mat MatId:MatInfo.MatId)
		local OutList = for Item in TerrMatDefs where (item.typeDef != Undefined) collect Item
		
		return OutList
	),
	
	-- Function for adding Max-like dotnet group-boxes:
	fn AddGrpBox AddToPanel title:"" size:1 panelType:dnFlowLayoutPanel = 
	(
		local GrpBox = DotNetObject dnGroupBox
		GrpBox.FlatStyle = GrpBox.FlatStyle.System
		GrpBox.Text = Title
		GrpBox.Dock = GrpBox.Dock.Fill
		GrpBox.AutoSize = True
		
		-- Add subpanel:
		local SubPanel = DotNetObject panelType
		SubPanel.Dock = SubPanel.Dock.Fill
		SubPanel.AutoSize = True
		GrpBox.Controls.Add SubPanel
		
		-- Add groupbox-panel to parent:
		AddToPanel.Controls.Add GrpBox
		
		return SubPanel
	),
	
	-- Function triggered by single-spinner change-events:
	fn ev_SpnCtrl_Changed Sender Args = 
	(
		local TheTool = ::gRsTerrainUberEditor
		
		-- Get variable-name from spinner:
		local ctrlName = Sender.Name
		
		-- Workaround for Max incorrectly treating control's Decimal value as an Integer:
		local NewVal = (GetProperty Sender #Value asDotNetObject:True) -- Get Decimal value
		NewVal = NewVal.ToSingle NewVal -- Convert Decimal to float
		
		-- Loop through panels with selected texmaps:
		for ThisMatInfo in TheTool.MatInfoList do 
		(
			local SelTexNums = ThisMatInfo.Selected
			
			-- Only process material if material has a selected texmap:
			if (selTexNums.numberSet != 0) do 
			(
				-- Add digit to float-spinner name, to get the corresponding variable-name
				local varName = ctrlName
				local varIdx = FindItem thisMatInfo.varNames varName
				
				-- Try looking for variable with a number instead
				if (varIdx == 0) do 
				(
					local numVarName = ctrlName + " " + (varIdx as String)
					varIdx = FindItem thisMatInfo.varNames (ctrlName + " 1")
				)
				
				-- Only process material if shader includes requested Variable Name:
				if (varIdx != 0) do 
				(
					local thisMat = thisMatInfo.terrainMat
					local varType = thisMatInfo.varTypes[varIdx]
					
					if (varType == "float") then 
					(
						-- Set float-value(s) directly on material
						for texNum in selTexNums do 
						(
							local varIdx = FindItem thisMatInfo.varNames (ctrlName + " " + (texNum as String))
							local varNum = thisMatInfo.varNums[varIdx]
							RstSetVariable thisMat varNum newVal
						)
					)
					else if (varType == "vector4") then 
					(
						local varNum = thisMatInfo.varNums[varIdx]
						
						-- Get material's current Vector4 value
						local combinedVal = RstGetVariable thisMat varNum
						
						-- Edit indices for selected texmaps
						for texNum in selTexNums do 
							combinedVal[texNum] = newVal
						
						-- Push Vector4 back to material
						RstSetVariable thisMat varNum combinedVal
					)
					
					RstRefreshMtl thisMat
				)
				
				-- Update values-list:
				ThisMatInfo.UpdateVals()
			)
		)
		
		-- Update controls to show updated values:
		TheTool.UpdateShowSelection()
	),
	
	-- Function triggered by multi-spinner change-events:
	fn ev_MultiSpnCtrl_Changed Sender Args = 
	(
		local TheTool = ::gRsTerrainUberEditor
		
		-- Get variable-name from spinner (this will be missing the texmap-number)
		local VarName = Sender.Name
		
		-- Find this spinner's panel-index:
		--	 (this is already 1-based, as first control will be label)
		local SpnNum = (Sender.Parent.Controls.IndexOf Sender)
		
		-- Workaround for Max incorrectly treating control's Decimal value as an Integer:
		local NewVal = (GetProperty Sender #Value asDotNetObject:True) -- Get Decimal value
		NewVal = NewVal.ToSingle NewVal -- Convert Decimal to float
		
		-- Loop through panels with selected texmaps:
		for ThisMatInfo in TheTool.MatInfoList do 
		(
			local SelTexNums = ThisMatInfo.Selected
			
			-- Only process material if material has a selected texmap:
			if (SelTexNums.NumberSet != 0) do 
			(
				-- Get index for first variable with this name:
				local VarIdx = FindItem ThisMatInfo.VarNames (VarName + " 1")
				
				-- Only process material if shader includes requested Variable Name:
				if (VarIdx != 0) do 
				(
					-- Get the variable-index before the first one:
					local VarNumZero = (ThisMatInfo.VarNums[VarIdx] - 1)
					
					local ThisMat = ThisMatInfo.TerrainMat
					
					-- Edit indices for selected texmaps:
					for TexNum in SelTexNums do 
					(
						local VarNum = (VarNumZero + TexNum)
						
						-- Get and edit variable's current Vector4 value:
						local CombinedVal = RstGetVariable ThisMat VarNum
						CombinedVal[SpnNum] = NewVal
						
						-- Push Vector4 back to material:
						RstSetVariable ThisMat VarNum CombinedVal
					)
					
					-- Refresh material:
					RstRefreshMtl ThisMat
				)
				
				-- Update values-list:
				ThisMatInfo.UpdateVals()
			)
		)
		
		-- Update controls to show updated values:
		TheTool.UpdateShowSelection()
	),
	
	-- Update texmap selection-highlights:
	fn UpdateShowSelection SuspendBrowseLayout:True = 
	(
		local AllVarNames = #()
		local AllVarVals = #()
		local MultiSpnVar = #{}
		
		if SuspendBrowseLayout do 
		(
			SetWaitCursor()
			
			Windows.Sendmessage browsePanel.handle WM_SETREDRAW 0 0
			browsePanel.SuspendLayout()
		)
		
		-- Update selection-highlights, and collect details:
		for MatNum = 1 to MatInfoList.Count do 
		(
			local MatInfo = MatInfoList[MatNum]
			local TexPanelList = TexmapPanels[MatNum]
			
			local MatVarNames = MatInfo.VarNames
			local MatVarTypes = MatInfo.VarTypes
			local MatVarVals = MatInfo.VarVals
			local SelTexNums = MatInfo.Selected
			
			local UvChansIdx = (FindItem MatVarNames "UV Set")
			local UvChanStrs = if (UvChansIdx == 0) then 
			(
				for n = 1 to 4 collect ""
			)
			else 
			(
				local Chans = MatVarVals[UvChansIdx]
				Chans = for n = 1 to 4 collect (Chans[n] as Integer)
				for Chan in Chans collect ("UV Set: " + (Chan as String))
			)
			
			-- Set button-panel colours to indicate if texmap is selected:
			for TexNum = 1 to TexPanelList.Count do 
			(
				local isSelTex = SelTexNums[TexNum]
				
				local TexBox = TexPanelList[TexNum]
				TexBox.BackColor = if isSelTex then HighlightColour else ToolForm.BackColor
				TexBox.Refresh()
				
				-- Update texmap's channel-text label:
				local TexClrPanel = TexBox.Controls.Item[0]
				local ChanTxt = TexClrPanel.Controls.Item[1]
				ChanTxt.Text = UvChanStrs[TexNum]
				
				-- Collect data for updating EditPanel's controls:
				if isSelTex do 
				(
					-- Process vector4 values and vars that end in numbers
					for varIdx = 1 to matVarNames.count where (matVarTypes[varIdx] != "texmap") do 
					(
						local thisVarType = matVarTypes[varIdx]
						local isVector4 = (thisVarType == "vector4")
						local isFloat = (thisVarType == "float")
						
						-- Get variable-name
						local varName = matVarNames[varIdx]
						local varNameNum = Undefined
						
						local lastChar = varName[varName.count]
						if (TrimRight lastChar "0123456789" == "") do 
						(
							-- Get number-value:
							varNameNum = (lastChar as Number)
							
							-- Strip number:
							varName = (SubString VarName 1 (VarName.Count - 2))
						)
						
						-- Only pay attention to numbered variables that match this texmap-number:
						if (varNameNum == texNum) or (isVector4 and (varNameNum == Undefined)) do 
						(
							-- Find material's variable-name on combined name-list:
							local allVarIdx = FindItem allVarNames varName
							
							-- Add new variable-name to list:
							if (allVarIdx == 0) do 
							(
								Append allVarNames varName
								allVarIdx = allVarNames.count
								
								-- Keep track of variables that have multiple spinners
								multiSpnVar[allVarIdx] = (isVector4 and (varNameNum != Undefined))
							)
							
							-- Get texmap's variable-value, if combined-list value isn't indeterminate:
							local currValArray = allVarVals[allVarIdx]
							
							-- Don't bother processing if we've already found that this value is indeterminate:
							if (currValArray != unsupplied) do 
							(
								local texValArray
								
								if (not isVector4) then 
								(
									-- This is just a single-value per-texmap variable
									texValArray = #(matVarVals[varIdx])
								)
								else if (varNameNum == Undefined) then 
								(
									-- Get value for texmap-index from tuple-variable:
									-- 	(converted to array)
									texValArray = #(matVarVals[varIdx][texNum])
								)
								else 
								(
									-- This whole tuple-variable is for a specific texmap-index
									texValArray = MatVarVals[VarIdx]
									
									-- Convert to array:
									texValArray = for n = 1 to 4 collect texValArray[n]
								)
								
								-- Test for differences if we've already seen this variable before:
								if (currValArray != undefined) do 
								(
									local IndetCount = 0
									for n = 1 to texValArray.Count do 
									(
										local currVal = currValArray[n]
										
										if (currVal == Unsupplied) then 
										(
											indetCount += 1
										)
										else 
										(
											-- Find difference between texmap's value and previously-found value:
											local diff = Abs (texValArray[n] - currVal)
											
											-- Mark as indeterminate if multiple values have been found for this variable-name:
											if (diff > 0.01) do 
											(
												texValArray[n] = unsupplied
												indetCount += 1
											)
										)
										
										-- If all values in array are indeterminate, set whole array as such:
										if (indetCount == texValArray.count) do 
										(
											texValArray = Unsupplied
										)
									)
								)
								
								-- Collect value/indetermination:
								allVarVals[AllVarIdx] = texValArray
							)
						)
					)
				)
			)
		)
		
		-- Update EditPanel (i.e. panel to the right)
		(
			Windows.Sendmessage EditPanel.Handle WM_SETREDRAW 0 0
			EditPanel.SuspendLayout()
			EditPanel.Controls.Clear()
			
			for varNum = 1 to allVarNames.count do 
			(
				local varName = allVarNames[varNum]
				local varVal = allVarVals[varNum]
				
				-- Pull single items out of arrays
				if (IsKindOf varVal Array) and (varVal.count == 1) do 
					varVal = varVal[1]
				
				local isIndeterminate = (varVal == Unsupplied)
				
				local LblCtrl = (dotNetObject dnLabel)
				LblCtrl.AutoSize = True
				LblCtrl.Text = (VarName + ":")
				
				if MultiSpnVar[VarNum] then 
				(
					-- Add sub-panel to hold row of spinners:
					local SpnPanel = (DotNetObject dnFlowLayoutPanel)
					SpnPanel.FlowDirection = SpnPanel.FlowDirection.LeftToRight
					SpnPanel.AutoSize = True
					SpnPanel.Margin = padding0
					SpnPanel.Padding = padding0
					
					EditPanel.Controls.Add SpnPanel
					EditPanel.SetFlowBreak SpnPanel True
					
					-- Add label:
					LblCtrl.Margin = padding0
					SpnPanel.Controls.Add LblCtrl
					SpnPanel.SetFlowBreak LblCtrl True
					
					for n = 1 to 4 do 
					(
						local SpnCtrl = (DotNetObject dnNumericUpDown)
						SpnCtrl.Margin = spnMargin
						SpnCtrl.Name = VarName
						SpnCtrl.Width = 55
						SpnCtrl.BackColor = WindowClr
						SpnCtrl.ForeColor = TextClr
						SpnCtrl.DecimalPlaces = 2
						SpnCtrl.Maximum= 9999
						SpnCtrl.Minimum = -1
						SpnCtrl.Increment = 0.1
						SpnCtrl.ReadOnly = False
						
						-- Set value, or set as indeterminate:
						if (isIndeterminate) or (not isKindOf VarVal[n] Number) then 
						(
							SpnCtrl.Text = ""
						)
						else 
						(
							SpnCtrl.Value = VarVal[n]
						)
						
						SpnPanel.Controls.Add SpnCtrl
						DotNet.SetLifetimeControl SpnCtrl #Dotnet
						DotNet.AddEventHandler SpnCtrl "ValueChanged" ev_MultiSpnCtrl_Changed
					)
				)
				else 
				(
					-- Add single spinner control:
					local SpnCtrl = (DotNetObject dnNumericUpDown)
					SpnCtrl.Name = VarName
					SpnCtrl.Width = 80
					SpnCtrl.BackColor = WindowClr
					SpnCtrl.ForeColor = TextClr
					SpnCtrl.DecimalPlaces = 2
					SpnCtrl.Maximum= 9999
					SpnCtrl.Minimum = -1
					SpnCtrl.Increment = 0.1
					SpnCtrl.ReadOnly = False
					
					-- Set value, or set as indeterminate:
					if (isIndeterminate) then 
					(
						SpnCtrl.Text = ""
					)
					else 
					(
						SpnCtrl.Value = (Float VarVal)
					)
					
					EditPanel.Controls.Add SpnCtrl
					DotNet.SetLifetimeControl SpnCtrl #Dotnet
					DotNet.AddEventHandler SpnCtrl "ValueChanged" ev_SpnCtrl_Changed
					
					-- Note that controls are being placed right-to-left, to align them to the right:
					EditPanel.Controls.Add LblCtrl
					EditPanel.SetFlowBreak LblCtrl True
				)
			)
			
			EditPanel.ResumeLayout()
			Windows.Sendmessage EditPanel.Handle WM_SETREDRAW 1 0
			EditPanel.Refresh()
		)
		
		if SuspendBrowseLayout do 
		(
			SetArrowCursor()
			BrowsePanel.ResumeLayout()
			Windows.Sendmessage BrowsePanel.Handle WM_SETREDRAW 1 0
			BrowsePanel.Refresh()
		)
	),
	
	-- Set all texmaps as unselected:
	fn ClearTexSel = 
	(
		local TheTool = ::gRsTerrainUberEditor
		
		for ThisMatInfo in TheTool.MatInfoList do 
		(
			ThisMatInfo.Selected.Count = 0
		)
	),
	
	-- Texmap-button paint-event:
	fn ev_TexmapBtn_Paint Sender Args = 
	(
		-- Request thumbnail for button if this hasn't been set yet:
		if (Sender.ImageIndex == -1) do 
		(
			local TheTool = ::gRsTerrainUberEditor
			local BtnTagVal = Sender.Tag.Value
			
			local MatInfo = TheTool.MatInfoList[BtnTagVal.MatNum]
			local TexPath = MatInfo.GetTexPath BtnTagVal.TexNum
			
			-- Queue control for thumbnail-update:
			TheTool.Thumbs.SetThumb Sender TexPath
		)
	),
	
	fn SetupExpanderBtn thisBtn expanded panelSize = 
	(
		local dirStr = (if expanded then "<" else ">")
		
		local btnText = dirStr
		
		local matId = thisBtn.tag
		if (matId != -1) do 
		(
			Append btnText (" " + (matId as String) + " " + dirStr)
		)
		
		thisBtn.text = btnText
		thisBtn.width = if expanded then 44 else panelSize
		thisBtn.height = if expanded then panelSize else 14
	),
	
	-- Expand/Contract buttons clicked
	fn ev_ExpanderBtn_clicked sender args = 
	(
		local theTool = ::gRsTerrainUberEditor
		
		local clrPanels = args
		if (not IsKindOf clrPanels Array) do 
		(
			-- Get per-texmap panels:
			local parentPanel = sender.parent
			clrPanels = for n = 1 to (parentPanel.controls.count - 1) collect parentPanel.controls.item[n]
		)
		
		-- Toggle panel-visibility:
		local setVisible = (not clrPanels[1].visible)
		clrPanels.visible = setVisible
		
		theTool.SetupExpanderBtn sender setVisible clrPanels[1].height
	),
	fn ExpandCollapseAll mode = 
	(
		local expand = (mode == #Expand)
		
		Windows.Sendmessage BrowsePanel.Handle WM_SETREDRAW 0 0
		this.browsePanel.SuspendLayout()
		for clrPanels in this.texmapPanels do 
		(
			-- Get first item in panel
			local texPanel = clrPanels[1]
			
			-- Trigger expand-button if visibility toggle is required
			if (texPanel.visible != expand) do 
			(
				local expanderBtn = texPanel.parent.controls.Item 0
				this.ev_ExpanderBtn_clicked expanderBtn clrPanels
			)
		)
		Windows.Sendmessage BrowsePanel.Handle WM_SETREDRAW 1 0
		this.browsePanel.ResumeLayout()
		
		return OK
	),
	fn ev_ExpandAllBtn_clicked sender args = 
	(
		local theTool = ::gRsTerrainUberEditor
		theTool.ExpandCollapseAll #Expand
	),
	fn ev_CollapseAllBtn_clicked sender args = 
	(
		local theTool = ::gRsTerrainUberEditor
		theTool.ExpandCollapseAll #Contract
	),
	fn ev_showMultiMatCheckBox_changed sender args = 
	(
		local theTool = ::gRsTerrainUberEditor
		theTool.justObjMatIdsChecked = not theTool.justObjMatIdsChecked		
		theTool.UpdateBrowsePanel()
	),
	
	-- Texmap-button clicked:
	fn ev_TexmapBtn_Clicked Sender Args = 
	(
		-- Was this a rightclick?
		local RightClick = Args.Button.Equals Args.Button.Right
		
		local TheTool = ::gRsTerrainUberEditor
		local BtnTagVal = Sender.Tag.Value
			
		local MatInfo = TheTool.MatInfoList[BtnTagVal.MatNum]
		local TexNum = BtnTagVal.TexNum
		
		local ClickMatSelected = MatInfo.Selected
		
		case of 
		(
			(keyboard.controlPressed):
			(
				-- Toggle selection:
				ClickMatSelected[TexNum] = not ClickMatSelected[TexNum]
				
				-- Toggle full line if shift is also held down:
				if (keyboard.shiftPressed) do 
				(
					MatInfo.Selected = if ClickMatSelected[TexNum] then #{1..MatInfo.TexCount} else #{}
				)
			)
			(keyboard.shiftPressed):
			(
				-- Select all material's texmaps:
				TheTool.ClearTexSel()
				MatInfo.Selected = #{1..MatInfo.TexCount}
			)
			Default:
			(
				-- Clear current selection and select item (unless we're rightclicking an already-selected item)
				if not (RightClick and ClickMatSelected[TexNum]) do 
				(				
					TheTool.ClearTexSel()
					ClickMatSelected[TexNum] = True
				)
			)
		)
		
		-- Update selection-highlights:
		TheTool.UpdateShowSelection()
	),
	
	-- Texmap-selector menuitem selected:
	fn ev_SelTexMenuItem_Clicked Sender Args = 
	(
		local TheTool = ::gRsTerrainUberEditor
		
		-- Get requested select-mode:
		local SelectByPath = False
		local SelectByIdx = False
		local SelectAll = False
		case Sender.Name of 
		(
			"ByPath":(SelectByPath = True)
			"ByIdx":(SelectByIdx = True)
			"All":(SelectAll = True)
		)
		
		-- Get rightclicked control:
		local SelCtrl = Sender.Owner.SourceControl
		
		-- Get details for rightclicked texmap:
		local TagValue = SelCtrl.Tag.Value
		local MatInfo = TheTool.MatInfoList[TagValue.MatNum]
		local TexNum = TagValue.TexNum
		local FindTexPath = MatInfo.GetTexPath TexNum
		
		-- Clear selection, unless Ctrl is pressed:
		if (not keyboard.controlPressed) do 
		(
			TheTool.ClearTexSel()
		)
		
		-- Loop through all panels, searching for relevant texmaps:
		for ThisMatInfo in TheTool.MatInfoList do 
		(
			-- Get list of terrain-texmaps:
			local ThisMatDiffPaths = ThisMatInfo.DiffuseTexPaths
			
			case of 
			(
				SelectAll:
				(
					ThisMatInfo.Selected = #{1..ThisMatDiffPaths.Count}
				)
				SelectByIdx:
				(
					ThisMatInfo.Selected[TexNum] = True
				)
				SelectByPath:
				(
					for n = 1 to ThisMatDiffPaths.Count do 
					(					
						if (pathConfig.pathsResolveEquivalent ThisMatDiffPaths[n] FindTexPath) do 
						(
							ThisMatInfo.Selected[n] = True
						)
					)
				)
			)
			
			-- Set texmap-idx as selected if it's the same as the rightclicked button's path:
			for n = 1 to ThisMatDiffPaths.Count do 
			(
				if (pathConfig.pathsResolveEquivalent ThisMatDiffPaths[n] FindTexPath) do 
				(
					ThisMatInfo.Selected[n] = True
				)
			)
		)
		
		-- Update selection-highlights:
		TheTool.UpdateShowSelection()
	),
	
	-- 'Copy path' menuitem selected:
	fn ev_CopyPathMenuItem_Clicked Sender Args = 
	(
		-- Get rightclicked control:
		local SelCtrl = Sender.Owner.SourceControl
		
		-- Get button's associated texturepath:
		local BtnTagVal = SelCtrl.Tag.Value
		
		local TheTool = ::gRsTerrainUberEditor
		local MatInfo = TheTool.MatInfoList[BtnTagVal.MatNum]
		local TexPath = MatInfo.GetTexPath BtnTagVal.TexNum
		
		-- Send text to clipboard:
		SetClipboardText TexPath
	),
	
	-----------------------------------------------------------------------------------------
	-- CreateTexmapRCMenu:
	--	Generates a rightclick-menu to be applied to texmap-id buttons:
	-----------------------------------------------------------------------------------------
	fn CreateTexmapRCMenu = 
	(
		local NewMenu = DotNetObject dnContextMenuStrip
		
		for Item in 
		#(
			(DataPair Name:"ByPath" Text:"Select texmaps with this filename"), 
			(DataPair Name:"ByIdx" Text:"Select texmaps with this lookup-colour"), 
			(DataPair Name:"All" Text:"Select all texmaps")
		) do 
		(
			local CmdItem = DotNetObject dnToolStripMenuItem Item.Text
			CmdItem.Name = Item.Name
			NewMenu.Items.Add CmdItem
			DotNet.SetLifetimeControl CmdItem #Dotnet
			DotNet.AddEventHandler CmdItem "Click" ev_SelTexMenuItem_Clicked
		)
		
		newMenu.items.Add (DotNetObject dnToolStripSeparator)
		
		(
			local CmdItem = DotNetObject dnToolStripMenuItem "Copy path to clipboard"			
			NewMenu.Items.Add CmdItem
			DotNet.SetLifetimeControl CmdItem #Dotnet
			DotNet.AddEventHandler CmdItem "Click" ev_CopyPathMenuItem_Clicked
		)
		
		return NewMenu
	),
	
	-- Update controls to match currently-selected objects:
	fn UpdateBrowsePanel = 
	(
		PushPrompt "Updating controls..."
		
		-- Clear rows, reset scroll-size:
		TexmapPanels.Count = 0
		BrowsePanel.Controls.Clear()
		BrowsePanel.AutoScroll = False
		BrowsePanel.AutoScroll = True
		
		-- Stop redraws while tweaking layout:
		Windows.Sendmessage BrowsePanel.Handle WM_SETREDRAW 0 0
		BrowsePanel.SuspendLayout()
		SetWaitCursor()
		
		-- Get terrain-material descriptors for selected objects:
		this.matInfoList.count = 0
		this.matInfoList = this.GetSelObjTerrainMatsInfo()
		
		if (matInfoList.count == 0) do 
		(
			EditPanel.Controls.Clear()
			BrowsePanel.ResumeLayout()
			Windows.Sendmessage BrowsePanel.Handle WM_SETREDRAW 1 0
			BrowsePanel.Refresh()
			SetArrowCursor()
			PopPrompt()
			return False
		)
		
		-- Get shadertype info from first material-descriptor:
		local shaderTypeDef = this.matInfoList[1].typeDef
		
		-- Get shadertype's lookup-colours:
		local lookupClrs = shaderTypeDef.lookupColours
		
		-- We want rows for each material, plus a row at the top/bottom
		browsePanel.rowCount = (matInfoList.Count + 2)
		
		-- Add panel for Expand/Collapse buttons
		(
			local btnWidth = 103
			
			local expandPanel = DotNetObject dnTableLayoutPanel
			expandPanel.autoSize = True
			expandPanel.columnCount = 3
			expandPanel.dock = expandPanel.dock.fill
			browsePanel.controls.Add expandPanel
			
			local collapseAllBtn = DotNetObject dnButton
			collapseAllBtn.autoSize = False
			collapseAllBtn.width = btnWidth
			collapseAllBtn.text = "< Collapse All <"
			collapseAllBtn.flatStyle = collapseAllBtn.flatStyle.system
			collapseAllBtn.dock = collapseAllBtn.dock.left
			expandPanel.controls.Add collapseAllBtn 0 0
			DotNet.AddEventHandler collapseAllBtn "Click" ev_collapseAllBtn_clicked
			DotNet.SetLifetimeControl collapseAllBtn #DotNet
			
			--Checkbox 
			local checkBoxCentre = (expandPanel.width / 7) 
			local showMultiMatCheckBox = DotNetObject dnCheckBox
			showMultiMatCheckBox.autoSize = False
			showMultiMatCheckBox.width = 160			
			showMultiMatCheckBox.text = "Show used materials only"
			showMultiMatCheckBox.flatStyle = showMultiMatCheckBox.flatStyle.system
			showMultiMatCheckBox.Margin = (dotNetObject "Padding" checkBoxCentre 5 0 0)
			showMultiMatCheckBox.Checked = this.justObjMatIdsChecked
			expandPanel.controls.Add showMultiMatCheckBox 1 0
			dnTooltip.SetToolTip showMultiMatCheckBox "Show materials used on the mesh,\n or the objects multi materials"
			DotNet.AddEventHandler showMultiMatCheckBox "CheckedChanged" ev_showMultiMatCheckBox_changed
			DotNet.SetLifetimeControl showMultiMatCheckBox #DotNet
			
			local expandAllBtn = DotNetObject dnButton
			expandAllBtn.autoSize = False
			expandAllBtn.width = btnWidth
			expandAllBtn.text = "> Expand All >"
			expandAllBtn.flatStyle = expandAllBtn.flatStyle.system
			expandAllBtn.dock = expandAllBtn.dock.left
			expandPanel.controls.Add expandAllBtn 2 0
			DotNet.AddEventHandler expandAllBtn "Click" ev_ExpandAllBtn_clicked
			DotNet.SetLifetimeControl expandAllBtn #DotNet
		)
		
		local btnWidth = thumbs.thumbSize
		
		-- Build list of panels for listed terrain-materials:
		this.texmapPanels.count = 0
		for matNum = 1 to This.MatInfoList.Count collect 
		(
			local thisMatInfo = this.matInfoList[MatNum]
			
			-- Add groupbox-panel to hold texmap-thumbnails:
			local matPanel = AddGrpBox BrowsePanel Title:(thisMatInfo.GetNameString())
			browsePanel.RowStyles.Add (dotNetObject "RowStyle" SizeType_AutoSize)
			
			-- Add expander-button:
			local expanderBtn = DotNetObject dnButton		
			expanderBtn.tag = thisMatInfo.matId
			expanderBtn.flatStyle = expanderBtn.flatStyle.system
			matPanel.controls.Add expanderBtn
			DotNet.AddEventHandler expanderBtn "Click" ev_ExpanderBtn_clicked
			DotNet.SetLifetimeControl expanderBtn #DotNet
			
			local texPaths = thisMatInfo.diffuseTexPaths
			
			-- Add per-texmap panels - background-colour is changed if texmap is selected:
			local matTexPanels = #()
			for ClrNum = 1 to TexPaths.Count do 
			(
				-- Add sub-panel to hold colour's controls:
				local OutClrPanel = DotNetObject dnFlowLayoutPanel				
				OutClrPanel.visible = this.initiallyShowExpanded
				OutClrPanel.AutoSize = True
				OutClrPanel.Margin = outClrPanelMargin
				OutClrPanel.Padding = outClrPanelPadding
				
				-- Tag panel with material-info and texmap-index:
				OutClrPanel.Tag = DotNetMxsValue (dataPair MatNum:MatNum TexNum:ClrNum)
				MatPanel.Controls.Add OutClrPanel
				
				local TexPath = TexPaths[ClrNum]
				
				-- Get colour from list, and make colour for overlaid text:
				local LookupClr = LookupClrs[ClrNum]
				
				-- Add Texture-button:
				(
					local ClrPanel
					
					-- Add lookup-colour outline:
					(
						ClrPanel = DotNetObject dnFlowLayoutPanel
						--ClrPanel.AutoSize = True
						ClrPanel.Margin = padding0
						ClrPanel.Padding = clrPanelPadding
						ClrPanel.Width = (BtnWidth + 6)
						
						-- Set button-background to lookup-colour:
						ClrPanel.BackColor = dnColour.FromArgb LookupClr.r LookupClr.g LookupClr.b
						
						OutClrPanel.Controls.Add ClrPanel
					)
					
					-- Add texmap-button:
					(
						local TexBtn = DotNetObject dnButton
						TexBtn.FlatStyle = TexBtn.FlatStyle.Flat
						TexBtn.Margin = padding0
						TexBtn.Tag = OutClrPanel.Tag
						
						-- Add rightclick-menu:
						if (TexMapRCmenu != undefined) do 
						(
							TexBtn.ContextMenuStrip = TexMapRCmenu
						)
						
						TexBtn.Width = BtnWidth
						TexBtn.Height = TexBtn.Width
						TexBtn.BackColor = ClrPanel.BackColor
						
						dotNet.addEventHandler TexBtn "MouseUp" ev_TexmapBtn_Clicked
						dotNet.addEventHandler TexBtn "Paint" ev_TexmapBtn_Paint
						ClrPanel.SetFlowBreak TexBtn True
						ClrPanel.Controls.Add TexBtn
						dotNet.setLifetimeControl TexBtn #dotnet
						
						-- Show texture/tooltip on texture-button:
						if (TexPath != undefined) and (TexPath != "") do 
						(
							dnTooltip.SetToolTip TexBtn TexPath
						)
					)
					
					-- Add uv-channel label:
					(
						local TxtClr = (white - LookupClr)
						local ChanTxt = DotNetObject dnLabel
						ChanTxt.Text = "UV Set:"
						ChanTxt.Width = BtnWidth
						ChanTxt.ForeColor = dnColour.FromArgb TxtClr.r TxtClr.g TxtClr.b
						ClrPanel.Controls.Add ChanTxt
					)
				)
				
				-- Collect highlighter-panel:
				Append matTexPanels outClrPanel
			)
			
			-- Setup previously-added expander-button based on size of texmap-buttons:
			if (matTexPanels.count > 0) do 
			(
				SetupExpanderBtn expanderBtn this.initiallyShowExpanded (matTexPanels[1].height)
			)
			
			Append texmapPanels matTexPanels
		)
		
		-- Update edit-controls:
		UpdateShowSelection SuspendBrowseLayout:False
		
		BrowsePanel.ResumeLayout()
		Windows.Sendmessage BrowsePanel.Handle WM_SETREDRAW 1 0
		BrowsePanel.Refresh()
		SetArrowCursor()
		
		PopPrompt()
	
		return Ok
	),
	
	fn ev_BtnUpdate_Clicked sender args = 
	(
		local theTool = ::gRsTerrainUberEditor
		theTool.UpdateBrowsePanel()
	),
	
	fn ev_BtnAutoUpdate_Clicked sender args = 
	(
		local theTool = ::gRsTerrainUberEditor
		
		-- Get toggled value
		local doAutoUpdate = (not theTool.IsBtnChecked sender)
		
		-- Show button as toggled
		theTool.SetBtnChecked sender doAutoUpdate
		
		if doAutoUpdate then 
		(
			theTool.UpdateBrowsePanel()
			theTool.AddCallback()
		)
		else 
		(
			theTool.RemoveCallback()
		)
		
		return OK
	),
	
	fn ev_BtnSyncTexmaps_Clicked Sender Args = 
	(
		local TexPaths = #()
		
		local TheTool = ::gRsTerrainUberEditor
		local ShownMats = for ThisTerrMatInfo in TheTool.MatInfoList collect ThisTerrMatInfo.TerrainMat
			
		-- Get list of all texmap-paths used by shown terrain-materials:
		for ThisMat in ShownMats do 
		(
			local MatInfo = RsMaterialItem.DescribeMat ThisMat GetTexMaps:True
			for TexInfo in MatInfo.Texmaps do 
			(
				join TexPaths TexInfo.Filepaths
			)
		)
		TexPaths = (MakeUniqueArray TexPaths)
		
		if (TexPaths.Count == 0) do return False
		
		-- Sync filenames: (thumbnails will update automatically)
		SetWaitCursor()
		gRsPerforce.Sync syncList callerName:"Terrain_ShaderEditor"
		SetArrowCursor()
		
		-- Refresh material:
		for ThisMat in ShownMats do 
		(
			RstRefreshMtl ThisMat
		)
		
		TheTool.UpdateBrowsePanel()
	),
	
	fn ToolClose = 
	(
		-- Remove selection-change callback:
		gRsTerrainUberEditor.RemoveCallback()
		
		-- Deregister tool from thumbnails-system
		if (gRSTA_ThumbsMgr != undefined) do 
		(
			gRSTA_ThumbsMgr.DeregisterTool ToolName
		)
		
		Free this.MatInfoList
		
		-- Dispose of struct's dotnetobjects:
		for PropName in (getPropNames This) do 
		(
			local ThisVal = (getProperty This PropName)
			if (isKindOf ThisVal DotNetObject) and (isProperty ThisVal #Dispose) do 
			(
				ThisVal.Dispose()
			)
		)
		
		gRsTerrainUberEditor = undefined
	),
	fn ev_ToolForm_Close Sender Args = 
	(
		gRsTerrainUberEditor.ToolClose()
	),
	
	fn InitForm = 
	(
		PushPrompt "Initialising Terrain Shader Editor..."
		
		ToolForm = DotNetObject "MaxCustomControls.MaxForm"
		ToolForm.Text = "Terrain Shader Editor"
		ToolForm.Size = DotNetObject "System.Drawing.Size" FormSize.X FormSize.Y
		ToolForm.MinimumSize = (DotNetObject "System.Drawing.Size" FormSize.X 0)
		
		-- Get dotnet colours for current Max colourscheme:
		FormClr = (ColorMan.GetColor #background) * 255
		FormClr = (dnColour.FromArgb FormClr[1] FormClr[2] FormClr[3])
		WindowClr = (ColorMan.GetColor #window) * 255
		WindowClr = (dnColour.FromArgb WindowClr[1] WindowClr[2] WindowClr[3])
		TextClr = (ColorMan.GetColor #windowText) * 255; 
		TextClr = (dnColour.FromArgb TextClr[1] TextClr[2] TextClr[3])
		
		ToolForm.BackColor = FormClr
		
		-- Show the tool-form, with redraw deactivated:
		(
			ToolForm.ShowModeless()
			ToolForm.Refresh()
			Windows.Sendmessage ToolForm.Handle WM_SETREDRAW 0 0
		)
		
		DotNet.addEventHandler ToolForm "Closing" ev_ToolForm_Close
		
		-- Struct for generating lists of controls:
		struct CtrlDef (name, text, tooltip="", tagVal, func, width, flowBreak=False)
		
		-- Main layout-table:
		(
			local MainTable = dotNetObject dnTableLayoutPanel
			ToolForm.Controls.Add MainTable
			MainTable.Dock = MainTable.Dock.Fill
			MainTable.CellBorderStyle = MainTable.CellBorderStyle.None
			
			-- Add banner:
			(
				-- Set fixed row-height:
				MainTable.RowStyles.Add (dotNetObject "RowStyle" SizeType_AutoSize)
				RsBannerPanel.Margin = padding0
				RsBannerPanel.Dock = RsBannerPanel.Dock.Fill
				
				MainTable.Controls.Add RsBannerPanel
			)
			
			-- Add thumbnails/editor split:
			(
				local SplitterCtrl = dotNetObject "SplitContainer"
				MainTable.Controls.Add SplitterCtrl
				
				SplitterCtrl.Dock = MainTable.Dock.Fill
				SplitterCtrl.Margin = padding0
				SplitterCtrl.Padding = (dotNetObject "Padding" 0 0 0 4)
				SplitterCtrl.BorderStyle = (dotNetClass "BorderStyle").FixedSingle
				
				SplitterCtrl.SplitterDistance = (formSize.x - editPanelWidth)
				
				-- Add to left panel:
				(
					this.browsePanel = DotNetObject dnTableLayoutPanel
					splitterCtrl.panel1.controls.Add browsePanel
					
					browsePanel.dock = browsePanel.dock.fill
					browsePanel.autoScroll = True
					browsePanel.margin = padding0
					browsePanel.padding = padding0
				)
				
				-- Add to right panel:
				(
					local RightTable = dotNetObject dnTableLayoutPanel
					SplitterCtrl.Panel2.Controls.Add RightTable
					
					RightTable.Dock = RightTable.Dock.Fill
					RightTable.Margin = padding0
					RightTable.Padding = padding0
					
					-- Add refresh-button groupbox:
					(
						local btnPanel = AddGrpBox rightTable
						--btnPanel.flowDirection = btnPanel.flowDirection.rightToLeft
						
						for BtnDef in 
						#(
							CtrlDef name:#btnAutoUpdate text:"Auto-Update" func:ev_BtnAutoUpdate_Clicked \ 
								tooltip:"Update tool whenever object selection changes",
							CtrlDef name:#btnUpdate text:"Update" func:ev_BtnUpdate_Clicked \ 
								tooltip:"Update tool to show selected objects' materials",
							CtrlDef name:#btnSyncTexmaps text:"Sync Texmaps" func:ev_BtnSyncTexmaps_Clicked \ 
								tooltip:"Sync files for listed materials"
						) 
						do 
						(
							local NewBtn = DotNetObject dnButton
							NewBtn.Name = BtnDef.Name
							NewBtn.Text = BtnDef.Text
							NewBtn.FlatStyle = NewBtn.FlatStyle.System
							NewBtn.AutoSize = True
							NewBtn.Margin = (dotNetObject "Padding" 3 0 3 3)
							
							BtnPanel.Controls.Add NewBtn
							DotNet.SetLifetimeControl NewBtn #Dotnet
							DotNet.AddEventHandler NewBtn "Click" BtnDef.Func
							
							btnPanel.SetFlowBreak newBtn btnDef.flowBreak
							
							if (btnDef.tooltip != "") do 
								dnTooltip.SetToolTip newBtn btnDef.tooltip
						)
					)
					
					-- Add edit-controls groupbox:
					(
						EditPanel = AddGrpBox RightTable Title:"Per-Texmap Values:"
						EditPanel.AutoScroll = True
						EditPanel.FlowDirection = EditPanel.FlowDirection.RightToLeft
					)
				)
				
			)
		)
		
		-- Set up a rightclick-menu:
		TexMapRCmenu = CreateTexmapRCMenu()
		
		-- Set up banner:
		BannerStruct.Setup()
		
		-- Update the tool-form:
		(
			Windows.Sendmessage ToolForm.Handle WM_SETREDRAW 1 0
			ToolForm.Refresh()
		)
		
		PopPrompt()
	),
	
	-- Add/Remove selection-callback
	fn RemoveCallback = 
	(
		Callbacks.RemoveScripts id:#RsTerrainShaderEditor
	),
	fn AddCallback = 
	(
		this.RemoveCallback()
		CallBacks.Addscript #SelectionSetChanged "::gRsTerrainUberEditor.UpdateBrowsePanel()" id:#RsTerrainShaderEditor
	),
	
	fn Create = 
	(
		InitForm()
		
		-- Set up initial materials:
		UpdateBrowsePanel()
	)
)

gRsTerrainUberEditor = RsTerrainUberEditor()
gRsTerrainUberEditor.Create()
