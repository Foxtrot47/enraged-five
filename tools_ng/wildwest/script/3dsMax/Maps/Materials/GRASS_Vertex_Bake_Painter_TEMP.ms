-- Vertex_Bake_Painter
-- A script to bake Skylight based ambient occlusion to models
-- Rick Stirling 2008/2009

-- 12 June 2009, added prebake to vertex illumination channel to make it dark. Set the illumination bake to 60%
-- 27th August 2009: Rick Stirling. Renamed the tool and started adding CPV Grass editing
-- 2nd September 2009: Rick Stirling. Bugfixes, added vertex alpha baking.
-- OCTOBER 09 - notes
-- Removed all game choice for now. Will add in for GTA5 or Max Payne 3/Bully if requested

-- December 2009:  Moved to Wild West. Added some extra features for bakes props/map objects. Renamed to Vertex_Bake_Painter
-- Jan 2009, each object gets a standard material during the bake.

--August 2010: David Evans. Bugfixes, moved the h,v, and rate modifiers to channels 20, 21, and 22 and added a button to condense them
--down to channel 11.

-- Novemeber 2010: Rick Stirling. Rewrote the construction of the Grass channels.  
-- Killed off the autorate and Grass cavity. UI updates
-- Luke Openshaw rewrote the channel condensing at this stage.

--December 2010: Stewart Wright.
--Added preserve checkboxes for prebake.
--Added emesh version of the condense Grass channel function
--Added check to collapse mesh to 1st vertexpaint modifier

-- January 2011: Rick Stirling
--- MAJOR rewrite. Moved functions to another include to make them easier to use. 
--- Split lighting and baking into two different data files
--- Added Artype variable plus local environment variables
--- Added contrast types


-- July 2011, Rick Stirling
-- Moved to Wildwest 2.
-- ***************************************************************************************************************************


-- ***************************************************************************************************************************
-- Common script headers, setup paths and load common functions
-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")


filein (pathToCommonFunctions + "FN_VertexPaint.ms")


cpvChannelList=#()
global cpvRed = 30
global cpvGreen = 31
global cpvBlue = 32
global cpvBake = 12

-- Load the vertex and light settings required for the tool
vbakesettings = pathToConfigFiles + "general/max_vertex_bake_settings.dat"
Skylightsettings = pathToConfigFiles + "general/max_skylight_settings.dat"

try (filein SkylightSettings) 
catch (messagebox ("Couldn't find project specific light settings file. \r\nWas expecting " + SkylightSettings))

try (filein vbakesettings) 
catch (messagebox ("Couldn't find project specific light settings file. \r\nWas expecting " + vbakesettings ))

-- Load common functions	
filein "pipeline/util/radiosity.ms"
	
-- Default the art type to peds
arttype = "Ped_"
ddatitem = 1


case arttype of
(
	"Ped_" :ddatitem = 1
	"Map_" :ddatitem = 2
	"Veh_" :ddatitem = 3
)


fn getInstallerArtType = (
	-- Get the art type using the data from the installer
	UIUserType = RsUserGetFriendlyName()	
	ShortUserType = substring UIUserType 1 3
)


	
-- ****************************************************************************************
-- This array is the channels 
-- Vertex channel id, flat colour for that channel, UI checkbox status
fn populateChannelData = (
	cpvChannelList = #(
			#(TheColourChannel, 	(readvalue ((artType + "BakeColour") as stringstream)), 	false),
			#(TheGrassChannel,  	(readvalue ((artType + "GrassColour") as stringstream)), 	false),
			#(TheGrassChannelR, 	(readvalue ((artType + "GrassColourR") as stringstream)), 	false),
			#(TheGrassChannelG, 	(readvalue ((artType + "GrassColourG") as stringstream)), 	false),
			#(TheGrassChannelB, 	(readvalue ((artType + "GrassColourB") as stringstream)), 	false),
			#(TheEnvEffChannel, 	(readvalue ((artType + "EnvEffColour") as stringstream)), 	false),
			#(TheFatChannel, 		(readvalue ((artType + "FatColour") as stringstream)), 		false),
			#(TheAlphaChannel, 	(readvalue ((artType + "AlphaColour") as stringstream)),	 false),
			#(TheRimChannel, 		(readvalue ((artType + "RimColour") as stringstream)), 	false)
	)
)	
populateChannelData()



-- ****************************************************************************************
fn CondenseGRASSChannels =
(
	if $ == undefined  then (messagebox "Nothing selected")
	
	else
	(
		objSelectedArray = getCurrentSelection()
		objCount = objSelectedArray.count

		clearselection()		

		for i = 1 to objCount do
		(
			obj = objSelectedArray[i]

			if obj.modifiers["Grass Condensed"] == undefined then (AddVpaintChannel obj cpvBake "grass Condensed" 0 0 0)
			
 			if obj.modifiers["Grass XYZScale"] == undefined or classof obj.modifiers["Grass XYZScale"] != vertexPaint Then
 				(messagebox ("Unable to find the Grass XYZScale channel for the object : " + (obj.name as string)) title:"XYZScale Channel!")

 			else
 			(
 				if obj.modifiers["Grass ZScale"] == undefined or classof obj.modifiers["Grass ZScale"] != vertexPaint Then
					(messagebox ("Unable to find the Grass Vertical channel for the object : " + (obj.name as string)) title:"ZScale Channel!")
 				else
 				(
 					if obj.modifiers["Grass Density"] == undefined or classof obj.modifiers["Grass Density"] != vertexPaint Then
						(messagebox ("Unable to find the Grass Rate channel for the object : " + (obj.name as string)) title:"Grass Density Channel!")

 					else
 					(
						collapseToClasses = #(VertexPaint)
						for i = obj.modifiers.count to 1 by -1 where findItem collapseToClasses (classof obj.modifiers[i]) != 0 do
						(
							maxOps.CollapseNodeTo obj i off
						)

						modPanel.setCurrentObject obj.baseObject

						-- get number of verts in the object  
						if classof obj == Editable_Poly then
						(
							nv = polyop.getnummapverts obj cpvRed
							polyop.setNumMapVerts obj cpvBake nv

							for v = 1 to nv do
							(	
								-- Set the Grass to black
								polyop.setmapvert obj cpvBake v (color 0 0 0)
							)

							for v = 1 to nv do
							(	
								r = (polyop.getmapvert $ cpvRed v).x 
								g = (polyop.getmapvert $ cpvGreen v).y
								b = (polyop.getmapvert $ cpvBlue v).z 
								
								CondensedColour = color r g b

								polyop.setmapvert obj cpvBake v CondensedColour 
							)	-- End of loop setting the correct condensed colour
						) -- end of epoly object

						if classof obj == Editable_Mesh then
						(
							nv = meshop.getnummapverts obj cpvRed
							meshop.setNumMapVerts obj cpvBake nv

							for v = 1 to nv do
							(	
								-- Set the Grass to black
								meshop.setmapvert obj cpvBake v (color 0 0 0)
							)

							for v = 1 to nv  do
							(	
								r = (meshop.getmapvert $ cpvRed v).x 
								g = (meshop.getmapvert $ cpvGreen v).y
								b = (meshop.getmapvert $ cpvBlue v).z 

								CondensedColour = color r g b
									
								meshop.setmapvert obj cpvBake v CondensedColour 
							)	-- End of loop setting the correct condensed colour
						)
					)
				) -- end has a rate
			) -- end has a vertical

			AddVpaintChannel obj cpvBake "Grass Condensed" 0 0 0
			clearSelection()
			select obj

		) -- end has horizontal
	) -- end valid object selection
) -- end CondenseGrassChannels


-- ****************************************************************************************
fn CreateGrassModifiers =
(
	fn copyBakeToTempCPV obj = 
	(
		--flush black
		if classof obj.baseobject == Editable_Poly then
		(
			nv = polyop.getnummapverts obj cpvBake
			
			polyop.setMapSupport obj cpvRed true
			polyop.setnummapverts obj cpvRed nv keep:false
			polyop.setMapSupport obj cpvGreen true
			polyop.setnummapverts obj cpvGreen nv keep:false
			polyop.setMapSupport obj cpvBlue true
			polyop.setnummapverts obj cpvBlue nv keep:false
			for v = 1 to nv do
			(	
				-- Set the Grass to black
				polyop.setmapvert obj cpvRed v (color 0 0 0)
				polyop.setmapvert obj cpvGreen v (color 0 0 0)
				polyop.setmapvert obj cpvBlue v (color 0 0 0)
				--break()
			)
			
			col = for v = 1 to nv collect polyop.getmapvert obj cpvBake v
				
			for v = 1 to nv do
			(
				polyop.setmapvert obj cpvRed v [col[v].x, 0, 0]
				polyop.setmapvert obj cpvGreen v [0, col[v].y, 0]
				polyop.setmapvert obj cpvBlue v [0, 0, col[v].z]
			)
			
			
		)
		
		if classof obj.baseobject == Editable_mesh then
		(
			nv = meshop.getnummapverts obj cpvBake
			meshop.setMapSupport obj cpvRed true
			meshop.setMapSupport obj cpvGreen true
			meshop.setMapSupport obj cpvBlue true
			for v = 1 to nv do
			(	
				-- Set the Grass to black
				meshop.setmapvert obj cpvRed v (color 0 0 0)
				meshop.setmapvert obj cpvGreen v (color 0 0 0)
				meshop.setmapvert obj cpvBlue v (color 0 0 0)
			)
			
			col = for v = 1 to nv collect meshop.getmapvert obj cpvBake v
			for v = 1 to nv do
			(
				meshop.setmapvert obj cpvRed v (color col[v].x 0 0)
				meshop.setmapvert obj cpvGreen v (color 0 col[v].y 0)
				meshop.setmapvert obj cpvBlue v (color 0 0 col[v].z)
			)
		)
		
		--paste channel component
		--channelInfo.PasteSubChannel obj 3 tgtChannel srcComp
		
	)
	
	if $ == undefined  then
	(
		messagebox "Nothing selected"
	)
	else
	(
		-- Don't make an array with a single object, that is just silly!
		objSelectedArray = getCurrentSelection()
		objCount = objSelectedArray.count
		clearselection()
		
		
		for i = 1 to objCount do (
			obj = objSelectedArray[i]
			--Check for existing channel data on the cpvBake channel
			cpvBakeExists = false
			if classof obj == Editable_Poly and polyop.getMapSupport obj cpvBake then cpvBakeExists = true
			if classof obj == Editable_Mesh and meshop.getMapSupport obj cpvBake then cpvBakeExists = true
			
			--dupe baked colour to temp cpv channels
			if cpvBakeExists then copyBakeToTempCPV obj
				
			--if obj.modifiers["Grass Condensed"] == undefined then (AddVpaintChannel obj cpvBake "Grass Condensed" 0 0 0)
 			if obj.modifiers["Grass XYZScale"] == undefined then
			(
				AddVpaintChannel obj cpvRed "Grass XYZScale" 0 0 0
			)
			if obj.modifiers["Grass Density"] == undefined then
			(
				AddVpaintChannel obj cpvBlue"Grass Density" 0 0 0
			)
			if obj.modifiers["Grass ZScale"] == undefined then
			(
				AddVpaintChannel obj cpvGreen "Grass ZScale" 0 0 0
			)
			
			
		)
		select objSelectedArray[1]
	)
) --end CreateGrassModifiers




-- ****************************************************************************************
if theVertPaintToolsFloater != undefined do
(
	try (closeRolloutFloater theVertPaintToolsFloater) catch()
)

-- ****************************************************************************************

if GrassBaker != undefined then destroyDialog GrassBaker
-- ****************************************************************************************
rollout GrassBaker "Grass Baker"
(	
	button btnAddGrass "Add Grass Layers" width:290 height:30
	button btnGrassHor "(R) XYZ Scale" width:70 height:30 across:4 align:#left
	button btnGrassVert "(G) Z Scale" width:70 height:30 align:#left
	button btnGrassRate "(B) Density" width:70 height:30 align:#left
	button btnGrassBLACK "Black" width:70 height:30 align:#left
	
	button btnCompressGrassChannels "Condense grass channels for exporting" width:290 height:30	
	
	on btnAddGrass pressed do CreateGrassModifiers()
	
	on btnGrassHor pressed do
	(
		if $ == undefined  then (messagebox "Nothing selected" )
		else
		(
			if $.modifiers[#Grass_XYZScale] != undefined Then
			(
				vpt = VertexPaintTool()
				vpt.paintColor = windHorizontalColour
			
				--objSelectedArray = getCurrentSelection()
				--clearselection()
				--select objSelectedArray[1]
				modPanel.setCurrentObject $.modifiers[#Grass_XYZScale]
				thePainterInterface.maxSize =readvalue ((artType + "paintbrushsize") as stringstream)
				vpt.curPaintMode = 1
			)
			else (	Messagebox "Unable to select the Grass_XYZScale modifier, it hasn't been created yet!" title:"Grass_XYZScale!")
		)
	)
	
	on btnGrassVert pressed do
	(
		if $ == undefined  then (messagebox "Nothing selected")
		else
		(
			if $.modifiers[#Grass_ZScale] != undefined Then
			(
				vpt = VertexPaintTool()
				vpt.paintColor = windRateColour	
				if artType == "Map_" then vpt.paintColor = inversewindVerticalColour


				--objSelectedArray = getCurrentSelection()
				--clearselection()
				--select objSelectedArray[1]
				modPanel.setCurrentObject $.modifiers[#Grass_ZScale]
				thePainterInterface.maxSize =readvalue ((artType + "paintbrushsize") as stringstream)
				vpt.curPaintMode = 1
			)
			else (Messagebox "Unable to select the Grass_ZScale modifier, it hasn't been created yet!" title:"Grass_ZScale Modifier!")
		)
	)
	
	on btnGrassRate pressed do
	(
		if $ == undefined  then (messagebox "Nothing selected")
		else
		(
			if $.modifiers[#Grass_Density] != undefined Then
			(
				vpt = VertexPaintTool()
				vpt.paintColor = windVerticalColour 	

				--objSelectedArray = getCurrentSelection()
				--clearselection()
				--select objSelectedArray[1]
				modPanel.setCurrentObject $.modifiers[#Grass_Density]
				thePainterInterface.maxSize =readvalue ((artType + "paintbrushsize") as stringstream)
				vpt.curPaintMode = 1
			)
			else (Messagebox "Unable to select the Grass_Density modifier, it hasn't been created yet!" title:"Grass_Density Modifier!")
		)
	)

	on btnGrassBLACK pressed do
	(
		if $ == undefined  then (messagebox "Nothing selected")
		else
		(
			if classof (modPanel.getCurrentObject()) == VertexPaint do
			(
				currentmodifer = modPanel.getCurrentObject()
				currentmodifername = currentmodifer.name
				vpt = VertexPaintTool()
				vpt.paintColor = (color 0 0 0)

				objSelectedArray = getCurrentSelection()
				clearselection()
				select objSelectedArray[1]
				modPanel.setCurrentObject  $.modifiers[currentmodifername ]
				thePainterInterface.maxSize = readvalue ((artType + "paintbrushsize") as stringstream)

				vpt.curPaintMode = 1
			)
		)
	)
	
	on btnCompressGrassChannels pressed do CondenseGRASSChannels()
)


-- ****************************************************************************************
--theVertPaintToolsFloater = newRolloutFloater "Vertex Paint Tools" 240 200

--addRollout colourbaker theVertPaintToolsFloater
createDialog GrassBaker width:310 height:120 style:#(#style_titlebar, #style_minimizebox, #style_sysmenu)
--addRollout GrassBaker theVertPaintToolsFloater