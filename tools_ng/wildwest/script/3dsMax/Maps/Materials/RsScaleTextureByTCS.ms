--filein "pipeline/util/texturemetatag-util.ms"
filein "rockstar/util/material.ms"


struct RsScaleMatsByTCS_STRUCT
(
	--a lookup for texture template downsample values
	templateDSLookup = #(),
	scaleDefault = 0.5,
	scaleFactor = scaleDefault,		--starts at 0.5 to account for the max export downsample
	processList = #(),

	fn RsExtractDownSampleFromTemplate tcpPath =
	(
		xmlDoc = XMlDocument()
		
		xmlDoc.init()
		xmlDoc.load tcpPath

		local retVal = undefined
		xmlRoot = xmlDoc.document.DocumentElement
		if xmlRoot != undefined then 
		(
			refnode = RsGetXmlElement xmlRoot "imageDownsampleScale"
			if refnode != undefined then 
			(
				return refnode.getAttribute "value"
			)
		)
		return retVal
	),
	
	fn buildDSLookup = 
	(
		local templates = RsConfigGetAssetsDir() + "/metadata/textures/templates/maps/*.tcp"
		
		for t in getFiles templates do
		(
			downSampleVal = RsExtractDownSampleFromTemplate t
			if downSampleVal == undefined then downSampleVal = 0
				
			append templateDSLookup (DataPair template:(getFilenameFile t) downSample:(downSampleVal as Integer))
		)
		
		--format "templateDSLookup: % \n" templateDSLookup
	),

	fn processMatScaling =
	(
		progressStart "Scaling"
		local matNum = 0
		for item in processList do
		(
			RstSetTextureScaleValue item.mat item.scaleFactor
			matNum += 1
			progressUpdate (100.0 * matNum / processList.count)
		)
		
		progressEnd()
	),

	fn RsScaleTexturesByTCS =
	(
		--local processList = #()
		macros.run "RS Utils" "SwitchSceneDXMats"
		
		--only work if we have to
		if processList.count > 0 then processMatScaling()
		
		--buildLookup
		if templateDSLookup.count == 0 then buildDSLookup()
		
		local matNum = 0
		
		--get all scene Rage Materials
		local rageMats = getClassInstances Rage_Shader
		
		progressStart "Analyse Materials.."
		for mat in rageMats do --get the diffuse texture
		(
			scaleFactor = scaleDefault
			
			--local mapName = RsGetTexMapNameFromRsShader mat 1
			local diff = for var=1 to RstGetVariableCount mat \
									where 	RstGetVariableType mat var == "texmap" and \
											matchpattern (RstGetVariableName mat var) pattern:"*diffuse*"
									collect var
			
			if diff.count == 0 then continue
				
			mapName = RstGetVariableName mat diff[1]
			--print mapName
			
			if RsIsDiffuseMap mapName then
			(
				local tex = RstGetVariable mat diff[1]
			
				local filePath = tex
				
				--now find the tcs file for the texture
				local tcsPath = RsConfigGetAssetsDir() + "/metadata/textures/maps/" + (getFilenameFile filePath) + ".tcs"
				tcsPath = tolower tcsPath
				
				if (not doesFileExist tcsPath) then
				(
					depotPath = gRsPerforce.local2depot tcsPath
					gRsPerforce.sync #(depotPath)
				)
				
				if (not doesFileExist tcsPath) then print "Cant find that tcs in maps"
				
				--get the template from it and read the downsample value
				if doesFileExist tcsPath then
				(
					
					pathBits = filterString (RsMakeSafeSlashes (RsExtractTemplateFromFile tcsPath)) "/"
					parentTemplate = undefined
					
					if pathBits.count != 0 then
					(
						parentTemplate = pathBits[pathBits.count]
					)
					else --no template so just the default tcp downsample
					(
						print "No parent template"
						parentTemplate = "Default"
					)
						
					--get the downsample value from the parent
					local templateDownSample = (for item in templateDSLookup where item.template == parentTemplate collect item.downSample)[1]
					
					if templateDownSample > 1 then
					(
						for d = 2 to templateDownSample do scaleFactor *= 0.5
					)
					
					--format "RstSetTextureScaleValue % %\n" mat scaleFactor
					append processList (DataPair mat:mat scaleFactor:scaleFactor)
					
					progressUpdate (100.0 * matNum / rageMats.count)
					matNum += 1
				)
			)
		)
		progressEnd()
		
		processMatScaling()
		
	)
)

-- RsScaleMatsByTCS = RsScaleMatsByTCS_STRUCT()
-- RsScaleMatsByTCS.buildDSLookup()
-- RsScaleMatsByTCS.RsScaleTexturesByTCS()

--/////////////////////////////////////////
--	UI
--/////////////////////////////////////////
try(destroyDialog ScaleTextureByTCSUI)catch()
rollout ScaleTextureByTCSUI "ScaleTextureByTCS" width:200 height:100
(
	--/////////////////////////////////////////
	--	VARIABLES
	--/////////////////////////////////////////
	local RsScaleMatsByTCS = undefined
	
	--/////////////////////////////////////////
	--	CONTROLS
	--/////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:200
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"ScaleTextureByTCS" filename:(getThisScriptFilename())
	
	button btnScaleTextures "Scale Textures"
	
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////
	
	
	--/////////////////////////////////////////
	--	EVENTS
	--/////////////////////////////////////////
	on btnScaleTextures pressed do RsScaleMatsByTCS.RsScaleTexturesByTCS()
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on ScaleTextureByTCSUI open do
	(
		banner.setup()
		RsScaleMatsByTCS = RsScaleMatsByTCS_STRUCT()
		RsScaleMatsByTCS.buildDSLookup()
	)
	
	
)
createDialog ScaleTextureByTCSUI
