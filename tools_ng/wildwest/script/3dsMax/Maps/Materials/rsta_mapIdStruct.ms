FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_xml.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Selection_Funcs.ms")
struct mapIDProjector
(
	------------------------------------------------------------------------------------------------------------
	-- LOCALS 
	------------------------------------------------------------------------------------------------------------
	cs_mapIDProjector,
	prog, 
	xmlFile = (RsConfigGetWildWestDir() + @"etc\config\general\AutoMapIDColours.xml"),
	------------------------------------------------------------------------------------------------------------
	-- FUNCITONS 
	------------------------------------------------------------------------------------------------------------	
	fn mathMin a b = 
	(
		return ((a+b)-(abs(a-b)))/2
	),
	------------------------------------------------------------------------------------------------------------
	fn project obj texture =
	(
		if (obj != undefined) AND (doesFileExist xmlFile) do 
		(
			-- PROGRESS BAR
			prog = RSProgressWindow Title:"MapID Projection : "		
			prog.start()
			
			-- SET UP PROJECTOR
			CSharp.CompileToMemory #(RsConfigGetWildWestDir() + @"script\3dsMax\Maps\Materials\rsta_mapIDProjector.cs")
			cs_mapIDProjector = dotNetobject "rsta.mapIDProjecor.mapIdProjector"

			cs_mapIDProjector.set_image texture
			
			-- SET BOUNDS 
			local LocalBounds = nodeGetBoundingBox obj obj.transform 	
			local bounds = LocalBounds[2]-LocalBounds[1] 	
			bounds *= obj.scale
			
			minBounds = [(mathMin localBounds[1].x localBounds[2].x), (mathMin localBounds[1].y localBounds[2].y), (mathMin localBounds[1].z localBounds[2].z)]	
			minBounds *=  obj.scale	
			
			cs_mapIDProjector.pMesh.dimentions = dotNetobject "rsta.mapIDProjecor.double2D" bounds.x bounds.y
			cs_mapIDProjector.pMesh.pivot = dotNetobject "rsta.mapIDProjecor.double2D" obj.pos.x obj.pos.y
			cs_mapIDProjector.pMesh.offset = dotNetobject "rsta.mapIDProjecor.double2D" minBounds.x minBounds.y

			cs_mapIDProjector.pMesh.rotation = 0
			
			-- ADD COLOURS 
			xml = RSTA_xml_IO xmlFile:xmlFile
			xml.load()
			for mapID in (rsta_xml.getChildren xml.root name:"mapID") do
			(
				cs_mapIDProjector.add_color (mapID.GetAttribute["idx"] as integer) (mapID.GetAttribute "r" as float) (mapID.GetAttribute "g" as float) (mapID.GetAttribute "b" as float)
			)
			
			-- PROJECT 			
			convertToMesh obj
			local faceCount = obj.numfaces
			prog.StepProgressValue = 100.0/faceCount
			for faceIdx = 1 to faceCount do
			(	
				prog.PostProgressStep()
				local polyCenter = meshop.getFaceCenter obj faceIdx		
				setFaceMatID obj faceIdx (cs_mapIDProjector.get_map_id polyCenter.x polyCenter.y)				
			)
			prog.End()
		)
	),
	------------------------------------------------------------------------------------------------------------
	fn Set_vertBorderSelection obj = 
	(
		local copyObj = convertToPoly (copy obj)

		local MapBorderEdges = #{}
		
		for MatId in (RsGetModelFaceIds copyObj) do 
		(
			copyObj.EditablePoly.SelectByMaterial MatId
			copyObj.EditablePoly.ConvertSelectionToBorder #Face #Edge
			MapBorderEdges += (GetEdgeSelection copyObj)
		)
		
		-- SET TO VERTS		
		polyop.setEdgeSelection copyObj (MapBorderEdges)	
		copyObj.EditablePoly.ConvertSelection #Edge #Vertex
		local mapBorderVerts = GetVertSelection copyObj

		
		-- REMOVE EDGES
		copyObj.EditablePoly.SetSelection #edge (polyOp.getOpenEdges copyObj)
		copyObj.EditablePoly.ConvertSelection #Edge #Vertex
		local geoBorderVerts = GetVertSelection copyObj
		
		-- MINUS BORDERS
		if (false) do geoBorderVerts = #{}
		polyop.setVertSelection copyObj (mapBorderVerts-geoBorderVerts)	

		-- COPY TO ORGINAL
		if (classof obj == Editable_mesh) do setVertSelection obj (polyop.getVertSelection copyObj)
		if (classof obj == Editable_poly) do polyop.setVertSelection obj (polyop.getVertSelection copyObj)

		delete copyObj	
	),
	------------------------------------------------------------------------------------------------------------
	fn blendVerts obj ChannelIndex =
	(	
		-- PROGRESS BAR
		prog = RSProgressWindow Title:"MapID Vertex Blend : "		
		prog.start()
		
		local mapArray = #((color 0 0 0),(color 0 0 255),(color 0 255 0),(color 0 255 255))
			
		if (classof_array #(Editable_Poly,Editable_mesh) obj) do 
		(
			select obj

			(RsMeshPolyOp obj).setVertColor obj 9 #{1..((RsMeshPolyOp obj).getNumVerts obj)} mapArray[1]
			Set_vertBorderSelection obj					
			
			-- DO ALPHA 
			if (true) do
			(
				local currentVerts = if (classof obj == editable_poly) then (polyOp.getVertSelection obj) else (getVertSelection obj)
				-- SET BLACK 
				(RsMeshPolyOp obj).setVertColor obj -2 #{1..((RsMeshPolyOp obj).getNumVerts obj)} (color 0 0 0)
				-- SET WHITE
				(RsMeshPolyOp obj).setVertColor obj -2 currentVerts (color 255 255 255)
			)	
			
			local percent = 100/ ChannelIndex.count	
			prog.StepProgressValue = percent		
			for idx in ChannelIndex do 
			(
				prog.PostProgressStep()
				local verts = if (classof obj == editable_poly) then (polyOp.getVertSelection obj) else (getVertSelection obj)
				(RsMeshPolyOp obj).setVertColor obj 9 verts mapArray[idx]
				gRsSelectionTools.RandomlyDeselectVerts obj percent
			)
			
			subobjectLevel = 0
		)
		prog.End()
	)
)