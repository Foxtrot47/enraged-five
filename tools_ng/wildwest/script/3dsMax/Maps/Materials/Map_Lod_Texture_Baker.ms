-- Map LOD Texture baker
-- Tool to back out layers of textures and tints
-- Artist can create composite in Photoshop
-- Rick Stirling, Rockstar North
-- Initial version :January 2012


-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
--RsCollectToolUsageData (getThisScriptFilename())




bakeFilePath = "C:\\temp\\MaxLodGrabs\\"
vpclist = #(0,13)





-- Takes an array of objects and hides their decal faces
fn hideDecalGeo selset =
(	
	for selobjindex = 1 to selset.count do 
	(
		obj = selset[selobjindex]
		
		-- Loop though the multimaterial and find ids that have a decal shader 
		decalIds = multiMaterialHasDecals obj
		
		if decalIDs != undefined then
		(
			-- hide those faces
			for decalindex = 1 to decalIds.count do
			(
				matid = decalIds[decalindex]
				
				if classOf obj == Editable_Poly then 
				(	
					obj.selectbymaterial matid --Select by material id
					obj.EditablePoly.Hide #Face
				)	
				
				-- Editable meshes are a bit of a pain when selecting faces by material Identity
				-- need to loop through them all and check to see if it's valid.
				if classOf obj == Editable_mesh then 
				(
					newFaceSel = #() 
					for facenum = 1 to obj.numfaces do
					(	
						if getFaceMatID obj facenum == matid do append newFaceSel facenum
					)	
					setFaceSelection obj newFaceSel --set the selection when the loop is done
					
					meshOps.hide obj -- hide these faces
				) -- end emesh stuff 
			)
			
		) -- end mat id loop
	) -- end object loop
) -- end function




fn unhideDecalGeo selset =
(
	for obj in selset do 
	(

		if classOf obj == Editable_Poly then 
		(	
			obj.EditablePoly.unhideAll #Face
		)	
		
			
		if classOf obj == Editable_mesh then 
		(
			select obj
			subobjectLevel = 3
			meshOps.unhideAll obj
			subobjectLevel = 0
		)
	)
)	





-- Set up camera from framed selection
fn setupGrabCamera camview = 
(
	viewport.setType camview
	actionMan.executeAction 0 "331" -- frame selected
)	





-- Write out the textures
fn bakeGrabbedImage bakeFilePath bakeFileName =
(	
	clearSelection()
	actionMan.executeAction 0 "549"
	-- Make sure path exists
	try (makeDir (bakeFilePath)) catch()	
	completeRedraw ()
	grabbedImg = gw.getViewportDib()
	grabbedImg.filename =bakeFilePath + bakeFileName + ".bmp"
	save grabbedImg
	close grabbedImg
)





-- Bake textures
-- Takes bool for decal yes/no
fn bakeTextures bakeFilePath bakeFileName selset baketype =
(
	
	displayColor.shaded = baketype -- #material or #object

	-- If we are baking textured models, we need to bake twice, once with decals on and once with decals off
	if baketype == #material then
	(
		-- With decals on 
		bakeFileName = bakeFileName  + "_Textures_and_Decals"
		bakeGrabbedImage bakeFilePath bakeFileName 
		
		-- With decals off
		hideDecalGeo selset
		bakeFileName = bakeFileName  + "_Textures_No_Decals"
		bakeGrabbedImage bakeFilePath bakeFileName 
		unhideDecalGeo selset
	)	
	

	-- If we are baking textured models, we need to bake twice, once with decals on and once with decals off
	if baketype == #object then
	(
		selset.showvertexcolors = true
		selset.vertexColorsShaded = false
		
		-- Loop through the vertex paint channels that we want to bake
		for vpcindex = 1 to vpclist.count do 
		(
			print vpcindex 
			print vpclist[vpcindex]
			-- Set all the objects to have that channel applied
			for obj in selset do
			(
				obj.vertexColorType = 5
				obj.vertexColorMapChannel = vpclist[vpcindex]	
			)
			-- Now do the screengrabs
			-- With decals on 
			outbakeFileName = bakeFileName  + "_Colours_and_Decals_" + (vpclist[vpcindex] as string)
			bakeGrabbedImage bakeFilePath outbakeFileName 
		
			-- With decals off
			hideDecalGeo selset
			outbakeFileName = bakeFileName  + "_Textures_No_Decals" + (vpclist[vpcindex] as string)
			bakeGrabbedImage bakeFilePath outbakeFileName 
			unhideDecalGeo selset
		)	
	) -- end object type bakes		
	
)	









-- UI Here
-------------------------------------------------------------------------------------------------------------------------
rollout MapLodTextures "Map_LOD_Texture_Baker" width:200 height:280
(
	local theSelection = undefined
	local hideselset = undefined
	
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Map_LOD_Texture_Baker" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	edittext edtOutputName "" width:170 height:25 readonly:true
	
	button btnSetCurrentSelection "Set Current Selection" width:170 height:25
	button btnClearNonGeoCurrentSelection "Remove Non-geo from Selection" width:170 height:25
	button btnSetupTopGrabCamera "Set Top Camera For Selected" width:170 height:25
	button btnBakeDiffuseTextures "Bake Textured Geo" width:170 height:25
	button btnBakeVPCTextures "Bake Vertex Coloured Geo" width:170 height:25
	button btnOpenWinExplorer "Open Baked Texture Folder" width:170 height:25
	
	-- Create a selection
	on btnSetCurrentSelection pressed do 
	(
		theSelection = getcurrentselection()
		
		-- If  this is a valid selection lets store it and get a temp name
		if (theSelection.count > 0) then
		(
			selObj = theSelection[1]
			MapLodTextures.edtOutputName.text = selObj.name
			
			Clearselection()
		)	

		else 
		(
			messagebox "Please select some valid objects to create a selection set for"
		)
		
	) -- End set selection


	-- Bake Diffuse textures
	on btnBakeDiffuseTextures pressed do
	(
		bakeFileName = MapLodTextures.edtOutputName.text -- This will be over written
		selset = theSelection
		baketype = #material
		
		bakeTextures bakeFilePath bakeFileName selset baketype
	)	

	
	on btnBakeVPCTextures pressed do
	(
		bakeFileName = MapLodTextures.edtOutputName.text -- This will be over written
		selset = theSelection
		baketype = #object

		bakeTextures bakeFilePath bakeFileName selset baketype
	)	
	
	on btnClearNonGeoCurrentSelection pressed do
	(
		hideselset = #()
		keepselset = #()
		
		for obj in maplodtextures.theSelection do
		(
			if superclassof obj ==helper then append hideselset obj
			if superclassof obj ==light then append hideselset obj
			if superclassof obj == GeometryClass then append keepselset obj
		)	
		
		hide hideselset
		completeRedraw ()
		
		maplodtextures.theSelection = keepselset
	)	

	on btnsetupTopGrabCamera pressed do setupGrabCamera #view_top 
	
	on btnOpenWinExplorer pressed do
	(
		shellLaunch "explorer.exe" bakeFilePath	
	)	

	-- Bake Decal textures

	-- Bake AO

	-- Bake tints

	-- Uberguess button


	
)



createDialog MapLodTextures 


