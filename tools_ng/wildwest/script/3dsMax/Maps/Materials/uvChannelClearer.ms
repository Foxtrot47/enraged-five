
--	R* vertex-channel clearer tool
-- 	Removes the unselected vertex-channels for selected mesh/collision objects
--	(channels named in fn_RSTA_vertexColours.ms are preserved by default)

--	06/06/2011 - Neal D Corbett, Rockstar Leeds

try (destroyDialog RsUVchannelClearer) catch ()
callbacks.removeScripts id:#RsUVchanClearer

RSTA_LoadCommonFunction #("fn_RSTA_vertexColours.ms")

rollout RsUVchannelClearer "Mapping/Vertex Channel Clearer" width:340
(
	dotNetControl RsBannerPanel "Panel" pos:[0,0] height:32 width:RsUVchannelClearer.width
	local bannerStruct = makeRsBanner dn_Panel:RsBannerPanel VersionNum:1.13 VersionName:"Opposite Cars"
	
	local meshChanLookup, collChanLookup
	
	label lblChooseType "Clear channels on selected:" align:#left across:2
	radioButtons chooseType labels:#("Geometry", "Collision") align:#left offset:[-20,0]
	
	multiListBox lstChannelsUsed "Channels used:" height:12
	
	checkbutton btnClear "Clear selected channels" width:233
	
	fn meshTest obj = 
	(
		isEditPolyMesh obj
	)
	
	fn collTest obj = 
	(
		(getattrclass obj == "Gta Collision")
	)
	
	fn CollRemoveClrChans obj RemoveChans:#() = 
	(
		col2Mesh obj
		RsRemoveClrChans obj RemoveChans:RemoveChans
		mesh2Col obj
	)
	
	fn updateList = 
	(
		callbacks.removeScripts id:#RsUVchanClearer
		
		local objTest, chanList, chanLookup
		
		case chooseType.state of 
		(
			1:
			(
				objTest = meshTest
				chanList = RsNamedMeshVertChannels
				chanLookup = meshChanLookup
			)
			2:
			(
				objTest = collTest
				chanList = RsNamedCollVertChannels
				chanLookup = collChanLookup
			)
		)
		
		local chansUsed = #()
		for obj in selection where (objTest obj) do 
		(
			join ChansUsed (RsGetActiveChannels Obj)
		)
		ChansUsed = (MakeUniqueArray ChansUsed)
		sort chansUsed
		
		local newItems = #()
		local newSel = #{}
		
		local itemNum = 0
		for chanNum in chansUsed do 
		(
			itemNum += 1
			
			local chanIdx = findItem chanLookup chanNum
			local knownChan = (chanIdx != 0)
			
			local itemName = stringStream ""
			format "%: %" chanNum (if knownChan then chanList[chanIdx].name else "[unknown]") to:itemName
			
			append newItems (itemName as string)
			newSel[itemNum] = not knownChan
		)
		
		lstChannelsUsed.items = newItems
		lstChannelsUsed.selection = (newSel as bitarray)
		
		callbacks.addScript #selectionSetChanged "RsUVchannelClearer.updateList()" id:#RsUVchanClearer
	)
	
	on chooseType changed state do 
	(
		updateList()
	)
	
	on btnClear changed state do 
	(
		SetWaitCursor()
		local objSel = (GetCurrentSelection())
			
		if (objSel.count == 0) do
		(
			btnClear.state = false
			return False
		)
		
		callbacks.removeScripts id:#RsUVchanClearer
		
		undo off
		(
			local objTest, clearFunc
			
			case chooseType.state of 
			(
				1:
				(
					objTest = meshTest
					clearFunc = RsRemoveClrChans
				)
				2:
				(
					objTest = collTest
					clearFunc = CollRemoveClrChans
				)
			)
			
			-- Get selected channel-numbers from item-strings:
			local RemoveChans = for itemNum = lstChannelsUsed.selection collect 
			(
				((filterString lstChannelsUsed.items[itemNum] ":")[1] as integer)
			)
			
			ClearSelection()
			
			for obj in objSel where (objTest obj) do 
			(
				ClearFunc Obj RemoveChans:RemoveChans
			)
			
			select objSel
		)
		
		updateList()
		btnClear.state = false
		SetArrowCursor()
	)
	
	on RsUVchannelClearer open do 
	(
		-- Initialise tech-art banner:
		bannerStruct.setup()
		
		meshChanLookup = for vChan in RsNamedMeshVertChannels collect vChan.id
		collChanLookup = for vChan in RsNamedCollVertChannels collect vChan.id
		
		updateList()
	)
	
	on RsUVchannelClearer close do 
	(
		callbacks.removeScripts id:#RsUVchanClearer
	)
)

createDialog RsUVchannelClearer
