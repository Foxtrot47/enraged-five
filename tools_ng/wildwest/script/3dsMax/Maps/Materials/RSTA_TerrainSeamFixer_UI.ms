try (DestroyDialog RstaTerrainSeamFixer) catch ()

RSTA_LoadCommonFunction #("fn_RSTA_vertexColours.ms")
Filein (RsConfigGetWildwestDir() + "Script/3dsMax/Maps/Materials/RSTA_TerrainSeamFixer.ms")

rollout RstaTerrainSeamFixer "Terrain Seam Fixer" width:640
(
	DotNetControl RsBannerPanel "Panel" pos:[0,0] height:32 width:RstaTerrainSeamFixer.width
	local banner = MakeRsBanner dn_Panel:RsBannerPanel versionNum:1.18 versionName:"Weird Flux"
	
	DotNetControl lstSeams "RsCustomDataGridView" height:300
	local listColumnTitles = #("Fix", "Object", "Problem Description", "Vertex\nCount", "Channel", "Sub\nChannel")
	local listColumnSizes = #(32, 132, 250, 58, 58, 58)
	local seamItems = #()
	
	Button btnFindSeams "Find Seams" align:#Left across:6
	Button btnFixSel "Fix Shown Problems" enabled:False pos:(btnFindSeams.pos + [73,0]) tooltip:"Fix texture-seams, where possible."
	Button btnFindFixAll "Quick Find/Fix" pos:(btnFixSel.pos + [114,0]) tooltip:"Find and Fix all texture-seams found, where possible."
	
	Label lblFindFixSelAll "Fix Verts:" pos:(btnFindFixAll.pos + [93,3])
	RadioButtons rdoFindFixSelAll "" labels:#("Selected", "All") default:1 pos:(lblFindFixSelAll.pos + [50,-4])
	
	Button btnShowBlends "Show Common Blends" align:#Right tooltip:"Show common-blends info for selected object."
	
	CheckBox chkAssumeTexLookupCantBlend "Assume texture-lookup borders can't blend" align:#Left offset:[0,-8] \
		checked:(gRsTerrainSeamFinder.assumeTexLookupCantBlend) \ 
		tooltip:"We have no way of knowing if a material-border vert with texture-lookup shows only common-blend textures.\n\n
(i.e. black in channel 5)\n\n
With this enabled, these verts will be flagged for common-blend fixes.
"
	
	-- Output 'seamItems' array to list-control:
	fn ShowSeamsInList = 
	(
		lstSeams.rows.Clear()
		
		if (seamItems.count == 0) do 
		(
			lstSeams.rows.Add #(True, "", "No seams found!")
		)
		
		for seamNum = 1 to seamItems.count do 
		(
			local seamItem = seamItems[seamNum]
			
			-- Create value-array, to be added to grid:
			-- 	(Show as checked by default)
			local addList = #(True)
			Append addList (seamItem.obj.name)
			
			local seamText = case seamItem.name of 
			(
				#TintMask:"Verts with Vertex Tint (i.e. non-0% tint-mask)"
				#LookupMask:"Lookup Mask seams"
				#CurveMask:"Curvature Mask seams"
				#DisplaceMask:"Displacement Mask seams"
				#WetnessSeam:"Terrain Wetness seams"
				#WeightSeam:"Terrain Lookup seams"
				#BlendSeam:"Uncommon-blend seams between materials"
				#BadTwoLayer:"2-layer verts with more than 2 blends"
				#BadBlendSum:"Blend-values don't sum to 1.0"
				Default:"UNKNOWN SEAM NAME"
			)
			Append addList seamText
			
			Append addList (seamItem.verts.NumberSet)
			
			local chan = case seamItem.chan of 
			(
				(-2):"Alpha"
				(-1):"Illum"
				(0):"VC"
				Default:(seamItem.chan)
			)
			Append addList chan
			
			local subchan = case seamItem.subChan of 
			(
				1:"Red"
				2:"Green"
				3:"Blue"
				Default:"N/A"
			)
			Append addList subchan
			
			-- Add row:
			local listIdx = lstSeams.rows.Add addList
			
			-- Add index corresponding to 'seamItems' array:
			lstSeams.rows.Item[listIdx].tag = seamNum
		)
		
		lstSeams.ClearSelection()
		btnFixSel.enabled = (seamItems.count != 0)
		
		return OK
	)
	
	fn DisposeItems = 
	(
		for item in seamItems do item.Dispose()
		seamItems.count = 0
	)
	
	fn InitControls = 
	(
		local listPadding = 5
		lstSeams.pos.x = listPadding
		lstSeams.width = RstaTerrainSeamFixer.width - (2 * listPadding)
		lstSeams.dock = lstSeams.dock.fill
		
		lstSeams.selectionMode = lstSeams.selectionMode.FullRowSelect
		lstSeams.allowUserToAddRows = False
		lstSeams.allowUserToDeleteRows = False
		lstSeams.allowUserToOrderColumns = True
		lstSeams.allowUserToResizeRows = False
		lstSeams.allowUserToResizeColumns = True
		lstSeams.allowDrop = False
		lstSeams.multiSelect = False
		
		-- Set up control's colours/fonts:
		local dnKnownColour = (DotNetClass "System.Drawing.KnownColor")
		local dnColour = (DotNetClass "System.Drawing.Color")
		local selColour = dnColour.FromKnownColor dnKnownColour.menuHighlight
		
		local textCol = (ColorMan.GetColor #windowText) * 255
		local windowCol = (ColorMan.GetColor #window) * 255
		local notLoadedCol = if (windowCol[1] < 128) then (windowCol * 1.5) else (windowCol * 0.85)
		
		local textColour = DNcolour.FromArgb textCol[1] textCol[2] textCol[3]
		local backColour = DNcolour.FromArgb windowCol[1] windowCol[2] windowCol[3]
		
		textFont = lstSeams.font
		textFontBold = (Dotnetobject "System.Drawing.Font" textFont (DotnetClass "System.Drawing.Fontstyle").bold)
		
		lstSeams.DefaultCellStyle.backColor = backColour
		lstSeams.DefaultCellStyle.foreColor = textColour
		
		lstSeams.EnableHeadersVisualStyles = False
		lstSeams.ColumnHeadersDefaultCellStyle.backColor = backColour
		lstSeams.ColumnHeadersDefaultCellStyle.foreColor = textColour
		lstSeams.ColumnHeadersDefaultCellStyle.font = textFontBold
		lstSeams.ColumnHeadersHeight = 34
		
		-- Add checkbox-column:
		for n = 1 to listColumnTitles.count do 
		(
			local newCol = Undefined
			
			if (n == 1) then 
			(
				-- Add checkbox-column:
				newCol = (DotNetObject "DataGridViewCheckBoxColumn")
				newCol.SortMode = newCol.sortMode.Automatic
			)
			else 
			(
				-- Add text-columns:
				newCol = (DotNetObject "DataGridViewTextBoxColumn")
				newCol.readOnly = True
			)
			
			local headerText = listColumnTitles[n]
			newCol.width = listColumnSizes[n]
			if (n == listColumnTitles.count) do 
			(
				newCol.AutoSizeMode = newCol.autoSizeMode.AllCells
			)
			newCol.headerText = headerText
			lstSeams.columns.Add newCol
		)
		
		return OK
	)
	
	fn FindSeams = 
	(
		DisposeItems()
		
		local selObjs = (GetCurrentSelection())
		
		for objNum = 1 to selObjs.count do 
		(
			-- Show currently-processing object in list:
			lstSeams.rows.Clear()
			for n = 1 to selObjs.count do 
			(
				local objName = selObjs[n].name
				lstSeams.rows.Add #((n < objNum), objName, (if (n == objNum) then "Finding Seams..." else ""))
			)
			Windows.ProcessPostedMessages()
			
			-- Find seams for object:
			local obj = selObjs[objNum]
			local objSeams = (gRsTerrainSeamFinder.FindProblems obj)
			if (objSeams != Undefined) do 
			(
				Join seamItems objSeams.items
			)
		)
		
		-- Show collected seam-data in list:
		ShowSeamsInList()
		
		return OK
	)
	
	-- Calculate and show list of selected object's Common Blends:
	fn ShowCommonBlends obj: = 
	(
		if (obj == Unsupplied) do 
		(
			local selObjs = (GetCurrentSelection())
			case (selObjs.count) of 
			(
				0:
				(
					MessageBox "Please select a Terrain object." title:"Error: Empty selection"
					return OK
				)
				1:
				(
					obj = selObjs[1]
				)
				Default:
				(
					MessageBox "Please select only one object." title:"Error: Multiple selection"
					return OK
				)
			)
		)
		
		-- Get Terrain Material descriptors for object:
		local terrMatInfos = (gRsTerrainHelpers.GetObjTerrainFaceData obj)
		
		case (terrMatInfos.count) of 
		(
			0:
			(
				MessageBox "Object has no Terrain materials:\n\nPlease select a Terrain object." title:"Error: Invalid materials"
				return OK
			)
			1:
			(
				MessageBox "Object only has one Terrain material.\n\nNo comparisons can be made." title:"Warning: Can't compare materials"
				return OK
			)
		)
		
		local GetBlendCompareInfo = (gRsTerrainSeamFinder.GetBlendCompareInfo)
		
		local listTitles = #("MatId A", "MatId B", "Texmap Idx", "Channel A", "Channel B", "Texmap A", "Texmap B", "Is Common")
		local listItems = #()
		
		local blendsCount = 0
		
		-- Calculate common-blend info for all object's terrain-materials:
		for matIdxA = 1 to (terrMatInfos.Count - 1) do 
		(
			-- Get info for first comparison-material:
			local terrInfoA = terrMatInfos[matIdxA]
			
			for matIdxB = (matIdxA + 1) to terrMatInfos.count do 
			(
				local terrInfoB = terrMatInfos[matIdxB]
				
				-- Get comparison-info for these materials:
				local compareInfo = (GetBlendCompareInfo terrInfoA terrInfoB)
				local matIds = compareInfo.matIds
				local texIdxInfos = compareInfo.texmaps
				
				for texIdx = 1 to texIdxInfos.count do 
				(
					local texIdxInfo = texIdxInfos[texIdx]
					
					local texUVchanA = texIdxInfo.chans[1]
					local texUVchanB = texIdxInfo.chans[2]
					local diffPathA = texIdxInfo.paths[1]
					local diffPathB = texIdxInfo.paths[2]
					
					local matchingChans = (texUVchanA == texUVchanB)
					local matchingPaths = (PathConfig.PathsResolveEquivalent diffPathA diffPathB)
					local isCommon = (matchingChans and matchingPaths)
					
					if isCommon do 
					(
						blendsCount += 1
					)
					
					local rowItems = #()
					Append rowItems matIds[1]
					Append rowItems matIds[2]
					Append rowItems texIdx
					Append rowItems texUVchanA
					Append rowItems texUVchanB
					Append rowItems (FilenameFromPath diffPathA)
					Append rowItems (FilenameFromPath diffPathB)
					Append rowItems isCommon
					
					Append listItems rowItems
				)
			)
		)
		
		local msgStr = (blendsCount as String) + " possible common blends were found for object: " + (obj.name)
		
		try (::RsTerrainBlendsInfo.Close()) catch ()
		global RsTerrainBlendsInfo = (RsQueryBoxMultiBtn msgStr title:"Seam Finder: Common Blends" timeout:-1 width:800 labels:#("OK") listItems:listItems listTitles:listTitles modal:False)
		
		return OK
	)
	
	on btnFindSeams pressed do 
	(
		FindSeams()
	)
	
	fn DoFixSeams fixObjs fixSeamNames:#() = 
	(
		if (fixObjs.count == 0) do 
		(
			return OK
		)
		
		local justSelVerts = (rdoFindFixSelAll.state == 1)
		
		if justSelVerts and ((SubobjectLevel == Undefined) or (SubobjectLevel == 0)) do
		(
			local msg = "'Selected' fix-mode requires a subobject selection."
			MessageBox msg title:"Error: Not in Subobject Selection mode"
			return OK
		)
		
		lstSeams.rows.Clear()
		
		undo "Fix Terrain Seams" on 
		(
			for n = 1 to fixObjs.count do 
			(
				local fixObj = fixObjs[n]
				local objFixNames = fixSeamNames[n]
				if (objFixNames == Undefined) do 
				(
					objFixNames = Unsupplied
				)
				
				-- Show current status in list:
				local rowNum = lstSeams.rows.Add #(False, fixObj.name, "Fixing Verts...")
				lstSeams.ClearSelection()
				Windows.ProcessPostedMessages()
				
				-- Fix those seams:
				gRsTerrainSeamFinder.FixObjProblems fixObj fixNames:objFixNames justSelVerts:justSelVerts
				
				-- Clear text:
				local rowCells = lstSeams.rows.item[rowNum].cells
				rowCells.item[0].value = True
				rowCells.item[2].value = ""
				Windows.ProcessPostedMessages()
			)
		)
		
		-- Update view:
		lstSeams.rows.Clear()
		lstSeams.rows.Add #(True, "", "Fix completed!")
		lstSeams.rows.Add #(True, "", "Run 'Find Seams' to see if any remain.")
		lstSeams.ClearSelection()
		
		btnFixSel.enabled = False
	)
	
	on btnFixSel pressed do 
	(
		-- Make sure all checkbox-changes have been committed:
		lstSeams.CommitEdit (DotNetClass "DataGridViewDataErrorContexts").Commit
		
		-- Get seams corresponding to checked rows:
		local fixSeams = #()
		local rows = lstSeams.rows
		for rowIdx = 0 to (rows.count - 1) do 
		(
			local thisRow = rows.Item[rowIdx]
			local checked = thisRow.cells.Item[0].value
			
			if checked do 
			(
				local seamNum = thisRow.tag
				Append fixSeams seamItems[seamNum]
			)
		)
		seamItems.count = 0
		
		if (fixSeams.count == 0) do return OK
		
		-- Collect seam-fix names per object:
		local fixObjs = #()
		local fixSeamNames = #()
		for item in fixSeams do 
		(
			local objNum = (FindItem fixObjs item.obj)
			if (objNum == 0) do 
			(
				Append fixObjs item.obj
				Append fixSeamNames #()
				objNum = fixObjs.count
			)
			Append fixSeamNames[objNum] item.name
		)
		fixSeams.count = 0
		
		DoFixSeams fixObjs fixSeamNames:fixSeamNames
		
		return OK
	)
	
	on btnFindFixAll pressed do 
	(
		DoFixSeams (GetCurrentSelection())
	)
	
	on btnShowBlends pressed do 
	(
		ShowCommonBlends()
	)
	
	on chkAssumeTexLookupCantBlend changed state do 
	(
		gRsTerrainSeamFinder.assumeTexLookupCantBlend = state
	)
	
	-- Called when list is clicked/rightclicked:
	on lstSeams cellMouseUp sender arg do 
	(
		local rowIdx = arg.rowIndex
		if (rowIdx < 0) do return False
		
		local clickRow = sender.rows.Item[rowIdx]
		local rightClick = arg.button.equals arg.button.right
		
		if rightClick and (seamItems.count != 0) do 
		(
			--if right-clicked cell is unselected, replace selection with it
			if not clickRow.Selected do 
			(
				sender.ClearSelection()
				clickRow.selected = True
			)
			
			-- Rightclick menu:
			rcmenu rcMenu_lstSeams
			(
				MenuItem itmSelVerts "Select Verts"
				Separator itmSep
				MenuItem itmShowChanOn "Show Channel in Viewport"
				MenuItem itmShowChanOff "Show Textured in Viewport"
				
				-- Get rightclicked seam-item:
				fn GetSelSeamItem = 
				(
					local lstSeams = lstSeams = RstaTerrainSeamFixer.lstSeams
					local selRows = lstSeams.selectedRows
					
					local selSeamItems = #()
					for selIdx = 0 to (selRows.count - 1) do 
					(
						local seamNum = selRows.Item[selIdx].tag
						local seamItem = RstaTerrainSeamFixer.seamItems[seamNum]
						Append selSeamItems seamItem
					)
					
					-- We only allow single-row selection for rightclick:
					local selSeam = selSeamItems[1]
					
					return selSeam
				)
				
				on itmSelVerts picked do 
				(
					local seamItem = GetSelSeamItem()
					if (seamItem == Undefined) do return False
					
					local seamObj = seamItem.obj
					
					-- Select object:
					local selObjs = GetCurrentSelection()
					if (selObjs.count != 1) or (selObjs[1] != seamObj) do 
					(
						Select seamObj
					)
					
					-- Show Modify tab:
					if (GetCommandPanelTaskMode() != #Modify) do 
					(
						SetCommandPanelTaskMode #Modify
					)
					
					-- Set Vertex subobject-mode:
					if (SubObjectLevel != 1) do 
					(
						SubObjectLevel = 1
					)
					
					-- Set vertex selection:
					(
						local selVertsFunc = case (ClassOf seamObj) of 
						(
							Editable_Poly:(PolyOp.SetVertSelection)
							Editable_Mesh:(SetVertSelection)
						)
						selVertsFunc seamObj seamItem.verts
					)
					
					return OK
				)
				
				on itmShowChanOn picked do 
				(
					local seamItem = GetSelSeamItem()
					if (seamItem == Undefined) do return False
					
					RsShowObjVertChannel seamItem.chan
				)
				
				on itmShowChanOff picked do 
				(
					RsShowObjVertChannel -3
				)
			)
			PopUpMenu rcMenu_lstSeams
		)
	)
	
	on RstaTerrainSeamFixer open do 
	(
		banner.Setup()
		InitControls()
		lstSeams.rows.Add #(True, "", "Click 'Find Seams' to continue")
		lstSeams.ClearSelection()
	)
	
	on RstaTerrainSeamFixer close do 
	(
		DisposeItems()
	)
)

-- Make sure "RsCustomDataGridView" is defined:
RS_CustomDataGrid()
CreateDialog RstaTerrainSeamFixer