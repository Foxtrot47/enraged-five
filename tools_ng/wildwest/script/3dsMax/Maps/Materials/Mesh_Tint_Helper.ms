-- Mesh tinting helper

-- Rick Stirling, Rockstar North
-- November 2011


-- Tool to help artists with setup of mesh tints.
-- No real design, just constructed as I go.



filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
filein (pathToCommonFunctions + "FN_VertexPaint.ms")

-- Load the vertex and light settings required for the tool
vbakesettings = pathToConfigFiles + "general/max_vertex_bake_settings.dat"

try (filein vbakesettings) 
catch (messagebox ("Couldn't find project specific light settings file. \r\nWas expecting " + vbakesettings ))

	

fn createTintVPM = 
(
	theSelection = getcurrentselection()
	if (theSelection.count > 0) do
	(
		selObj = theSelection[1]
		
		if (classof selObj == Editable_mesh) or (classof selObj == Editable_poly) then
		(	
			addModifier selObj (vertexpaint()) 
			selObj.modifiers[#VertexPaint].mapchannel = theTintChannel
			selObj.modifiers[#VertexPaint].name = "Tint Channel"
			clearSelection()
			select selObj
				
			vpt = VertexPaintTool()
			vpt.paintColor = (color 255 255 255)
			thePainterInterface.maxSize = 1.0
			vpt.curPaintMode = 1
		)
	)	
)



fn floodObjectWhite = 
(
	theSelection = getcurrentselection()
	if (theSelection.count > 0) do
	(
		for objectIndex = 1 to theSelection.count do 
		(	
			selObj = theSelection[objectIndex]
			modPanel.setCurrentObject selObj.baseObject
			
			if (classof selObj == Editable_mesh) or (classof selObj == Editable_poly)  then
			(	
				selObj.showVertexColors = on
					
				subobjectLevel = 1 
				nv = SelObj.numverts

				for vertIndex = 1 to nv do 
				(
					if classof SelObj == Editable_Poly then  (polyop.setvertcolor selObj theTintChannel vertIndex (color 255 255 255))
					if classof selObj == Editable_Mesh then (meshop.setvertcolor selObj theTintChannel vertIndex (color 255 255 255))	
				)
				subobjectLevel = 0
			)
		)	
	)	
)



-------------------------------------------------------------------------------------------------------------------------
rollout meshtinttools "Mesh Tinting Helper" width:220 height:200
(
	
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/mesh_tint_tools" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)


	button btnfloodObjectWhite "Flood Selected Objects with White Tint" width:200 height:25
	button btncreateTintVPM "Create Tint Vertex Paint Modifier" width:200 height:25
	


	
	
	on btncreateTintVPM pressed do createTintVPM()
	on btnfloodObjectWhite pressed do floodObjectWhite()
		

)



createDialog meshtinttools 
