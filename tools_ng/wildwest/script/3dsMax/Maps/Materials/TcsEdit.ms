
try (DestroyDialog tcsEditRollout) catch()

filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

global tcsEditRollout, gTcsEdit

struct tcsEdit
(
	bmpPathArray = #(),
	bmpFileArray = #(),
	templateArray = #(),
	regex = dotnetclass "System.Text.RegularExpressions.Regex",
	xmlTemplateFile = (RsConfigGetEtcDir() + "texture/templates.xml"),
	TexTuneMapList = #(),
	
	--Use filenames to get template names
	fn getTemplateList = 
	(
		tcpArray =#()
		files = getFiles (RsConfigGetAssetsDir() + "metadata/textures/templates/maps/*.tcp")
		for i in files do
		(
			sArray = filterString i "\\"
			tcp = sArray[sArray.count]
			append tcpArray (toLower(substring tcp 1 (tcp.count-4)))
		)
		return tcpArray
	),	

	fn bmpListFromSelObj =	
	(
		bmpPaths = #()
		for obj in selection do
		(
			if classof obj.material == MultiMaterial then
			(	
				for rageShader in obj.material.materialList do 
				(		
					if classOf rageShader == Rage_Shader then
					(
						bmpCount = RstGetVariableCount rageShader
						if bmpCount != undefined and bmpCount >= 1 do 
						(
							for bmpNum = 1 to bmpCount where (RstGetVariableType rageShader bmpNum == "texmap") do 
							(
								bmpPath = RstGetVariable rageShader bmpNum
								if bmpPath != "" then appendIfUnique bmpPaths bmpPath
							)
						)
					)
				)
			)
		)
		return bmpPaths
	),
	
	fn makeFileNameList bmpPathList =
	(
		bmpNames = #()
		for i in bmpPathList do
		(
			iStringArray = filterString i "\\"
			if iStringArray.count != 0 then
			(
				append bmpNames (iStringArray[iStringArray.count])
			)
		)
		return bmpNames
	),
	
	fn getTcsPath bmpName =
	(
		tcsPath =toLower(RsConfigGetAssetsDir() + @"metadata/textures/maps/" + (substring bmpName 1 (bmpName.count-3)) + "tcs")
		tcsPath = substituteString tcsPath "/" "\\"
		if doesFileExist tcsPath then
		(
			return tcsPath 
		)
		else
		(
			--set the tcs path text to missing
			return "**MISSING**"
		)
		
	),
	
	fn getTcsTemplateType =
	(
		currentFilePath =  RsMakeSafeSlashes tcsEditRollout.edt_tcsPath.text
		
		inP4 = gRsPerforce.exists #(currentFilePath)
		existsLocal = doesfileExist currentFilePath
		if existsLocal != true and inP4.count != 0 then	--sync it
		(
			gRsPerforce.p4.run "sync" #(currentFilePath)
			--return 0
		)
		
		if existsLocal != true then return false
			
		xmlStream = XmlStreamHandler xmlFile:currentFilePath
		xmlStream.open()
		local nodelist = xmlStream.root.SelectNodes("parent")
		tcpPath = nodelist.ItemOf[0].InnerText
		xmlStream.close()
		
		pathBits = filterString tcpPath "/"
		if pathBits.count != 0 then
		(
			tcpFile = pathBits[pathBits.count]
			tcpName = toLower (filterString tcpFile ".")[1]
			--print tcpName
			return (findItem templateArray tcpName)
		)
		
		/*
		currentFile = openFile currentFilePath
		if currentFile != undefined then
		(			
			tcsAsString = ""			
			while not eof currentFile do
			(
				append tcsAsString  ((readLine currentFile)+"\n")
			)
			
			pStart = findString tcsAsString "<parent>" + 8 -- 8 FOR THE WORD PARTENT
			pEnd = findString tcsAsString "</parent>"
			pLine = substring tcsAsString pStart (pEnd-PStart)
			pLineStringArray = filterString pLine "/"
			
			template = pLineStringArray[pLineStringArray.count]		

			close currentFile			
			return (findItem templateArray template)
		)
		*/
		
	),
	
	fn modifyTCS =
	(
		filePath =  RsMakeSafeSlashes tcsEditRollout.edt_tcsPath.text
		
		--check it out
		inP4 = gRsPerforce.exists #(filePath)
		existsLocal = doesfileExist filePath
		if inP4.count != 0 then --check it out
		(
			gRsPerforce.checkedOutForEdit filePath
		)
		
		--modify its contents
		if existsLocal == true then
		(
			xmlStream = XmlStreamHandler xmlFile:filePath
			xmlStream.open()
			local nodelist = xmlStream.root.SelectNodes("parent")
			tcpPath = nodelist.ItemOf[0].InnerText
			tcp = (regex.matches tcpPath "\w+$").item[0].value
			xmlStream.close()
		)
		
		--save
		
	),
	
	
	fn saveToTcs =
	(
		filePath =  tcsEditRollout.edt_tcsPath.text
		newFilePath = tcsEditRollout.edt_tcsPath.text + "_TEMP"
		
		theOldFile = openFile filePath
		if theOldFile != undefined then
		(			
			fileAsString = ""
			
			while not eof theOldFile do
			(
				theVal = readLine theOldFile 
				append fileAsString theVal
				append fileAsString "\n"
			)
			
			pStart = findString fileAsString "<parent>" + 8 -- 8 FOR THE WORD PARTENT
			pEnd = findString fileAsString "</parent>"
			pLine = substring fileAsString pStart (pEnd-PStart)
			pLineStringArray = filterString pLine "/"
			Template = pLineStringArray[pLineStringArray.count]
			
			pFirstBit = substring fileAsString 1 (pStart-1)
			pLastBit = substring fileAsString pEnd fileAsString.count
			
			newString= pFirstBit
			
			for i = 1 to (pLineStringArray.count-1) do
			(
				newString = (newString + pLineStringArray[i] + "/")
			)
			newString =  newString + tcsEditRollout.lbx_template.selected
			newString = newString + pLastBit 
			
			newFilePath = tcsEditRollout.edt_tcsPath.text + "_TEMP"
			theNewFile = createFile newFilePath
			format newString to:theNewFile
			
			close theNewFile
			close theOldFile
			
			deleteFile filePath
			if (renameFile newFilePath filePath) != true then
			(
				print "WARNING: Did not save to .tcs"
			)
			else
			(
				messagebox "OK" title:"success"
			)
		)
	),
	
	fn isFileReadOnly filePath =
	(
		try
		( 
			readOnly = getFileAttribute (filePath) #readOnly
			if readOnly then
			(
				tcsEditRollout.btn_save.enabled = false
			)
			else
			(
				tcsEditRollout.btn_save.enabled = true
			)
			
		)
		catch --fail safe
		(
			tcsEditRollout.btn_save.enabled = false
		)
		
	),
	
	fn bmpListSelected =
	(
		this.bmpListRefresh()
		
		if bmpPathArray.count > 0 do
		(
			bmpInt = tcsEditRollout.lbx_bmpList.selection
			if bmpInt != 0 then
			(
				tcsEditRollout.edt_bmpPath.text = bmpPathArray[bmpInt]
				tcsEditRollout.edt_tcsPath.text = getTcsPath bmpFileArray[bmpInt]
				sel = getTcsTemplateType()
				if sel == undefined then
				(
					--set null selection in the list
					tcsEditRollout.lbx_template.selection = 0
				)
				else
				(
					tcsEditRollout.lbx_template.selection = getTcsTemplateType()
				)
				
				isFileReadOnly tcsEditRollout.edt_tcsPath.text
			)
		)
	),
	
	fn bmpListRefresh = 
	(
		if selection.count > 0 do
		(
			bmpPathArray = bmpListFromSelObj()
			bmpFileArray = makeFileNameList bmpPathArray
			tcsEditRollout.lbx_bmpList.items = bmpFileArray
		)
	),
	
	on create do
	(
		templateArray = getTemplateList()
		xmlStreamHandler()
		RsLoadTexTuneConfig TexTuneMapList templatesXmlFilename:xmlTemplateFile
		templateNames = for texTuneMap in TexTuneMapList collect texTuneMap.desc
	)
)
gTcsEdit = tcsEdit()

rollout tcsEditRollout "TCS Editor" width:380
(
	----Banner
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:tcsEditRollout.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel versionNum:1.10 versionName:"Deadpan Bubble" wiki:"tcsedit" filename:(getThisScriptFilename())
	----/Banner
	
	----Main Controls
	editText edt_bmpPath "BMP Path:" across:2 width:320 readonly:true offset:[-5, 2]
	button btn_bmpShow "Show" width:42 height:22 align:#right offset:[5, 0] tooltip:"show in explorer"
	editText edt_tcsPath "TCS Path:" width:320 across:2 readonly:true offset:[-5, 2]
	button btn_tcsShow "Show" width:42 height:22 align:#right offset:[5, 0] tooltip:"show in explorer"
	
	--button btn_refresh "Reload Sel Object Bmp's" pos:[5,60] width:200
	listBox lbx_bmpList "Object Bitmaps:" width:200 across:2 offset:[-5, 0]
	listBox lbx_template "TCS Templates:" width:162 align:#right offset:[5, -2]
	--button btn_p4 "P4" width:50 across:2 align:#left offset:[-5, -15]
	button btn_save "SAVE CHANGE" width:365 enabled:true align:#left offset:[-5, 0]

	----/Main Controls
	
	on tcsEditRollout open do
	(
		banner.setup()
		lbx_template.items = gTcsEdit.templateArray
		--gTcsEdit.bmpListRefresh()
		gTcsEdit.bmpListSelected()
		
		--make sure connected to p4
		if gRsPerforce.connected() != true then gRsPerforce.connect()
		
		callbacks.addscript #selectionSetChanged "gTcsEdit.bmpListSelected()" id:#tcsEdit
	)
	
	on tcsEditRollout close do
	(
		callbacks.removeScripts id:#tcsEdit
	)
	
	on btn_refresh pressed do
	(
		gTcsEdit.bmpListRefresh()
		gTcsEdit.bmpListSelected()
	)
	
	on lbx_bmpList selected i do
	(
		gTcsEdit.bmpListSelected()
	)
	
	on  btn_bmpShow pressed do ShellLaunch "explorer.exe" ("/e,/select,\"" + edt_bmpPath.text + "\"")
	on btn_tcsShow pressed do	ShellLaunch "explorer.exe" ("/e,/select,\"" + edt_tcsPath.text + "\"")
	
	on btn_p4 pressed do
	(
		gRsPerforce.edit edt_tcsPath.text
		gTcsEdit.isFileReadOnly edt_tcsPath.text
	)
	on btn_save pressed do gTcsEdit.saveToTcs()
)

createDialog tcsEditRollout lockWidth:true lockHeight:true style:#(#style_titlebar, #style_sysmenu, #style_minimizebox)
--select callback to update lists

