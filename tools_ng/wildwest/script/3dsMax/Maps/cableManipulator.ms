function genCablePoints startNode endNode count =
(
	--print "."
	outPoints = #()
	for i=1 to count do
	(
		append outPoints (startNode.pos + ((endNode.pos - startNode.pos) / count) * i)
	)
	--print outPoints
	outPoints
)

plugin simpleManipulator CableProxy
    name:"Cable Proxy"
	classID:#(0x1860182, 0x79724a54)
    category:"RS Helpers"
(
    -- Create the green and red colors for the gizmo
    local greenColor =  colorMan.getColor #manipulatorsActive
    local redColor =    colorMan.getColor #manipulatorsSelected
	local offsetLength = 0.0
	local editing = false
	local parentDummy
	local textPos = [0, 0, 0]
	local tooltip = ""
	--local gizmoTM = (matrix3 1)

	parameters pblock rollout:params
	(
		startNode type:#node
		endNode type:#node
		points type:#point3Tab tabSizeVariable:true
		pntCount type:#integer default:10
		slack type:#float default:0.03
		nameTag type:#string default:"Cable"
		uid type:#string default:"{00000000-0000-0000-0000-000000000000}"
	)
	
	rollout params "Cable Proxy"
	(
		spinner spnSlack "Slack" default:0.03 range:[0.001, 3.0, 0.03] scale:0.001 type:#float
		spinner spnPntCount "Count" default:10 range:[3,30,10] type:#integer

		on spnPntCount changed val do
		(
			--cablePoints = genCablePoints this.startNode this.endNode val
			--meshObj = undefined
			this.pntCount = val
			this.updateGizmos 1f &toolTip
		)
		
		on spnSlack changed val do
		(
			catenaryCurve.slack = val
			this.slack = val
			this.updateGizmos 1f &toolTip
		)
		
		on params open do
		(
			spnSlack.value = slack
		)
	)
	
    -- This manipulator manipulates any node with a "radius" property
    on canManipulate target return false

	tool create
  	(
  		local startNode, endNode, parentDummy
		
		on mousePoint click do
		(
  		   case click of
  		   (
				1:
				(
					startNode = Point pos:worldPoint wirecolor:green constantscreensize:true name:"CableStart"
				)
				3:
				(
					endNode = Point pos:worldPoint wirecolor:red constantscreensize:true name:"CableEnd"
				)
				4:
				(
					try
					(
						this.startNode = startNode
						this.endNode = endNode
						
						local theNode = (refs.dependentNodes this)[1]
						setTransformLockFlags theNode #all
						
						local dummyPos = startNode.pos + (0.5 * (endNode.pos - startNode.pos))
						parentDummy = Dummy pos:dummyPos wirecolor:yellow boxsize:[1,1,1] name:"CableParent"
						startNode.parent = parentDummy
						endNode.parent = parentDummy
						--theNode.parent = parentDummy
							
						theNode.points = genCablePoints startNode endNode 10
							
						theNode.uid = genGuid()
							
						editing = true
						#stop
					)
					catch
					(
						#stop
					)
					
					#stop
						
				)
				5: #stop
  		   )
	   )
  		
  	)
	
    -- Create the manipulator gizmo.
    -- This is called initially and whenever the manipulator target changes
    on updateGizmos do
    (
        --local modTM = getModContextTM node target
		--format "."
		this.clearGizmos()
  		
		if editing or (startNode != undefined) and (endNode != undefined) do
		(
			points = #()
			catenaryCurve.slack = this.slack
			catenaryCurve.simulateHangingWire startNode.pos endNode.pos pntCount
			points = catenaryCurve.points
			
			if (parentDummy != undefined) do
			(
				textPos = parentDummy.pos
			)
			textPos = startNode.pos + (0.5*(endNode.pos - startNode.pos))
		
		
			--this.addGizmoMarker #asterisk [0,0,0] flags greenColor redColor
			this.addGizmoText this.nameTag textPos 0 greenColor redColor
			
			local giz = manip.makeGizmoShape()
			--giz.transform gizmoTM
			
			giz.startNewLine()
			giz.addPoint startNode.pos
			--this.addGizmoMarker #asterisk startNode.pos 1 greenColor redColor
			for pnt in points do
			(
				giz.addPoint pnt
				--this.addGizmoMarker #asterisk pnt flags greenColor redColor
			)
			
			this.addGizmoShape giz 0 greenColor redColor
		)
		
        return true
    )

  
	
	on MouseDown m which do
	(
		offsetLength = 0.0
	)
	
	
)
