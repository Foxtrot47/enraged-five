
--/////////////////////////////////////////
--	UI
--/////////////////////////////////////////
try(destroyDialog SwapSpecForDiff_SUI)catch()
rollout SwapSpecForDiff_SUI "Swap Spec for Diff_S" width:400 height:200
(
	--/////////////////////////////////////////
	--	VARIABLES
	--/////////////////////////////////////////
	local feedback = undefined
	
	--/////////////////////////////////////////
	--	CONTROLS
	--/////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:400
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"CHANGEME" filename:(getThisScriptFilename())
	
	button btnProcessScene "Process Scene"
	progressBar prgProgress
	
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////
		
	/***
	For the given material determine its diffuse texture path and use that to construct its specular path
	***/
	fn doSpecDiffSwap mat =
	(
		--get the diffuse texture
		local diffTex = undefined
		for i = 1 to (RstGetVariableCount mat) where ((RstGetVariableType mat i) == "texmap") and (RstGetVariableName mat i) == "Diffuse Texture" do
		(
			diffTex = RstGetVariable mat i
		)
		
		if (diffTex != undefined) do
		(
			--build the spec version of the name
			local bareName = getFilenameFile diffTex
			local newSpecPath = (getFilenamePath diffTex) + bareName + "_s.bmp"
			
			--check the file exists
			if (not (doesFileExist newSpecPath)) then
			(
				newSpecPath = (getFilenamePath diffTex) + bareName + "_spec.bmp"
			)
			
			if (not (doesFileExist newSpecPath)) then
			(
				newSpecPath = (getFilenamePath diffTex) + bareName + "spec.bmp"
			)
			
			if (not (doesFileExist newSpecPath)) then
			(
				newSpecPath = (getFilenamePath diffTex) + bareName + "_Spec.bmp"
			)
			
			
			--look for the specular texture slots
			for i = 1 to (RstGetVariableCount mat) do
			(
				if ((RstGetVariableType mat i) == "texmap") and (RstGetVariableName mat i) == "Specular Texture" then
				(
					local specTex = RstGetVariableName mat i
					if (specTex != newSpecPath) do
					(
						if (doesFileExist newSpecPath) then
						(
							RstSetVariable mat i newSpecPath
						)
						else
						(
							format "cant find a spec file for %\n" diffTex to:feedback
						)
					)
				)
			)
		)
	)

	/***
	Process scene object and determine material dependencies for swapping the spec texture for
	***/
	fn swapSpecForDiff_S =
	(
		--get all the drawable object
		local counter = 0
		local gtaObjects = for obj in objects where (GetAttrClass obj == "Gta Object") collect obj
		for obj in gtaObjects where (GetAttrClass obj == "Gta Object") do
		(
			--get material assigned
			objMat = obj.material
			
			if (classOf objMat == Multimaterial) then
			(
				local matIds = RsMatGetMapIdsUsedOnObject obj #()
				
				for id in matIds where (id != -1) do
				(
					local mat = objMat.materialList[id]
					if (classOf mat == Rage_Shader) do
					(
						doSpecDiffSwap mat
					)
				)
			)
			else if (classOf objMat == Rage_Shader) then
			(
				local mat = obj.material
				doSpecDiffSwap mat
			)

			--get the diffuse texture

			--does it have a composite specular
				--if it does then switch for a bitmap
				--assign the _s version of the diffuse
			
			counter += 1
			prgProgress.value = (100.0 * (counter / gtaObjects.count as Float))
		)
		
		prgProgress.value = 0
	)
	
	--/////////////////////////////////////////
	--	EVENTS
	--/////////////////////////////////////////
	on btnProcessScene pressed do
	(
		prgProgress.value = 0
		feedback = newScript()
		swapSpecForDiff_S()
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on SwapSpecForDiff_SUI open do
	(
		banner.setup()
	)
)

createDialog SwapSpecForDiff_SUI width:200 height:100 pos:[730, 300]
