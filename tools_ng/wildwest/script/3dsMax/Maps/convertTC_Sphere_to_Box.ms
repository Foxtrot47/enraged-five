--GtaTimeCycleSphere -> GtaTimeCycle converter
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")


fn convertTC_Sphere_To_Box =
(
	local sphereTCs = for obj in objects where classOf obj == GtaTimeCycleSphere collect obj
		
	--attr indices
	local idxPercentage	= GetAttrIndex "Gta TimeCycle" "Percentage" 
	local idxRange		= GetAttrIndex "Gta TimeCycle" "Range"
	local idxStartHour	= GetAttrIndex "Gta TimeCycle" "StartHour"
	local idxEndHour	= GetAttrIndex "Gta TimeCycle" "EndHour"
	local idxIDString	= GetAttrIndex "Gta TimeCycle" "IDString"
	
	local idx_TC_Sphere_Percentage	= GetAttrIndex "Gta TC Sphere" "Percentage"
	local idx_TC_Sphere_Range		= GetAttrIndex "Gta TC Sphere" "Range"
	local idx_TC_Sphere_StartHour	= GetAttrIndex "Gta TC Sphere" "StartHour"
	local idx_TC_Sphere_EndHour		= GetAttrIndex "Gta TC Sphere" "EndHour"
	local idx_TC_Sphere_Name		= GetAttrIndex "Gta TC Sphere" "Name"
		
	for tc in sphereTCs do
	(
		--get attr values
		local percentage	= GetAttr tc idx_TC_Sphere_Percentage
		local range			= GetAttr tc idx_TC_Sphere_Range
		local startHour		= GetAttr tc idx_TC_Sphere_StartHour
		local endHour		= GetAttr tc idx_TC_Sphere_EndHour
		local IDString		= GetAttr tc idx_TC_Sphere_Name
		
		--create new TC mod
		--origin is different for box than sphere so drop it by sphere TC radius
		local boxTC = GtaTimeCycle pos:[tc.pos.x, tc.pos.y, (tc.pos.z - tc.usrRadius)]
		
		--converting to radius, so double it
		boxTC.boxgizmo.length = 2.0 * tc.usrRadius
		boxTC.boxgizmo.width = 2.0 * tc.usrRadius
		boxTC.boxGizmo.height = 2.0 * tc.usrRadius
		
		SetAttr boxTC idxPercentage	percentage
		SetAttr boxTC idxRange		range
		SetAttr boxTC idxStartHour	startHour
		SetAttr boxTC idxEndHour	endHour
		SetAttr boxTC idxIDString	IDString
		
		
	)
	
)

convertTC_Sphere_To_Box()