filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
rsta_loadCommonFunction #("FN_RSTA_userSettings", "FN_RSTA_rageMat") 

if (lod_material_swapper != undefined) do lod_material_swapper.dispose()
struct lod_material_swapper
(	
	-------------------------------------------------------------------------------------------------------------------------
	-- LOCALS 
	-------------------------------------------------------------------------------------------------------------------------
	LODShaderMapPath = (RsConfigGetToolsDir() + "etc/config/maps/gta5/LODShaderMap.ini"),
	MainWindow,
	vm,
	Settings = rsta_userSettings app:"lod_shader_swapper",	
	selectedMaterials = #(),
	selectedMaterialsObjects = #(),
	
	-------------------------------------------------------------------------------------------------------------------------
	-- FUNCTIONS 
	-------------------------------------------------------------------------------------------------------------------------	
	-- NORMAL PROCESS --------------------------------------------------------------------------------------
	fn do_normals mat = 
	(
		case (toLower (getFilenameFile (RstGetShaderName mat))) of
		(
			"terrain_cb_4lyr_cm_tnt":	--swap the normal for a tiny proxy
			(
				local tinyFlatNormal = RsProjectGetArtDir() + "/textures/_Core Textures/TinyFlatNormal_n.bmp"
				gRsperforce.sync #(tinyFlatNormal)				
				if (doesFileExist tinyFlatNormal) do rsta_rageMat.setTexMapByPattern mat "Bump*" tinyFlatNormal 						
			)
		)		
	),
	
	-- GET LOD SHADER NAME ------------------------------------------------------------------------------
	fn get_LOD_shaderName HDName =
	(
		local LODName = HDName + "_lod"	
		
		-- IF NO LOD SHADER THEN CHECK THIS LIST....
		if not (RsShaderExists LODName) do 
		(			
			case of
			(
				-- SPECIAL CASE
				(HDName == "terrain_cb_4lyr_2tex"):  LODName = "terrain_cb_4lyr_lod"			
				
				-- SWAP ALL DECAL-SHADERS TO DECAL 
				(matchPattern HDName pattern:"*decal*"):  LODName = "decal"				
		
				--PXM SHADERS 
				(matchPattern HDName pattern:"*_pxm*"):
				(
					--FIND THE LOD SHADER THE HDNAME MAPS TO
					LODName = getIniSetting LODShaderMapPath "lod_shader_map" HDName
					if (LODName == "") do LODName = "default"				
				)
				
				-- SPECULAR SHADERS
				(matchPattern HDName pattern:"*spec*"): LODName = "spec"
							
				-- SWAP ALL OTHER SHADERS TO DEFAULT:
				Default: LODName = "default"				
			)
		)		
		return LODName
	),
	
	-- PROCESS THE OBJECTS -------------------------------------------------------------------------------------------
	fn processObjects =
	(
		for matIdx=1 to selectedMaterials.count do 
		(	
			-- MAKE A COPY TO EDIT
			local matCopy = copy selectedMaterials[matIdx] 
			
			-- GET MATCHING SHADERS WITH MAT IDX
			for selShadIdx=0 to vm.SelectedShaders.count-1 do
			(					
				local item = vm.SelectedShaders.Item[selShadIdx]
				if (item.MatIndex == matIdx) do
				(
					-- GET THE COPY VERSION OF THIS MAT 
					local matIDidx = (finditem matCopy.materialIDList item.MatID)
					if (matIDidx != 0) then 
					(
						local mat = matCopy.materialList[matIDidx]						

						-- SHADER SWAP
						if (item.shader) do RstSetShaderName mat item.LODShader									
						
						-- NORMAL SWITCH
						if (item.Normals) do do_normals mat	
						
						-- FILENAME 
						if (item.fileNames) do RsSetMaterialTexturesToLod mat SLOD:(if (vm.LodType == 1) then true else false)		
					)
					else format ("Mat ID missmatch in " + item.MaterialName + ".\n")					
				)
			)
			-- ASSIGN NEW SHADER TO OBJECTS
			for obj in selectedMaterialsObjects[matIdx] do obj.mat = matCopy		
		)	
		this.dispose()
	),	
		
	-- ADD MATERIAL TO DATA GRID ------------------------------------------------------------------------------
	fn processMat matIdx mat matID: =
	(
		if (isKindOf mat Rage_Shader) do
		(
			if (matID == unsupplied) do matID = -1
			local HDName = toLower (getFilenameFile(RstGetShaderName mat))
			local LODName = get_LOD_shaderName HDName
			MainWindow.add_shader mat.name matID matIdx HDName LODName
		)
	),
	
	-- GET THE DATA FROM THE OBJECTS FOR THE GRID -----------------------------------------------
	fn get_dataGridData sel =
	(		
		-- COLLECT ALL THE SHADERS 
		for obj in sel do
		(
			local idx = findItem selectedMaterials obj.mat			
			if (idx == 0) do 
			(
				append selectedMaterials obj.mat
				append selectedMaterialsObjects #()
				idx = selectedMaterials.count
			)
			append selectedMaterialsObjects[idx] obj	
		)		
		
		-- LOOP THE SHADERS AND ADD TO DATAGRID 
		for matIdx=1 to selectedMaterials.count do
		(
			local mat = selectedMaterials[matIdx]
			if (isKindOf mat MultiMaterial) then 
			(
				for mapID=1 to mat.materialList.count do processMat matIdx mat.materialList[mapID] matID:mat.materialIdList[mapID]						
			)
			else processMat matIdx mat				
		)
	),
	
	-------------------------------------------------------------------------------------------------------------------------
	-- WINDOW FUNCTIONS 
	-------------------------------------------------------------------------------------------------------------------------
	-- DISPOSE OF WINDOW AND STRUCT -----------------------------------------------------------
	fn dispose = 
	(
		if (MainWindow != undefined) do 
		(
			-- WINDOW POS 
			settings.wpf_windowPos mainWindow #set
			MainWindow.Close()     
			MainWindow = undefined
		)
		execute ("lod_material_swapper = undefined")
	),
	
	-- LOAD UI -------------------------------------------------------------------------------------------------------------
	fn init =
	(	
		gRsPerforce.sync #(LODShaderMapPath)		
		local sel = for obj in selection where classof_array #(Editable_Poly, Editable_Mesh) obj collect obj
		
		if sel.count != 0 then
		(	
			-- DOT NET STUFF 
			dotnet.loadAssembly (RsConfigGetToolsDir() + @"techart/dcc/3dsMax/RSG.TechArt.LodShaderSwap.dll")
			MainWindow = dotNetObject "RSG.TechArt.LODShaderSwap.MainWindow"			
			dn_window.setup MainWindow 
			MainWindow.Tag = dotnetmxsvalue this			
			
			-- GET DATA 
			get_dataGridData sel						
			-- WINDOW POS 
			settings.wpf_windowPos mainWindow #get			
			-- SHOW UI 	
			MainWindow.show()				
			-- SET VM
			vm = MainWindow.vm

			MainWindow.title = "LOD Shader Swapper : v2.0"
		)
		else messageBox "Nothing Selected!"		
	),
	-------------------------------------------------------------------------------------------------------------------------
	-- EVENTS 
	-------------------------------------------------------------------------------------------------------------------------
	on create do init() 
)
-- RUN ---------------------------------------------------------
lod_material_swapper = lod_material_swapper()
