------------------------------------------------------------------------------
-- FILE IN 
------------------------------------------------------------------------------
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
------------------------------------------------------------------------------
-- ROLLOUT 
------------------------------------------------------------------------------
try (destroyDialog  RSTA_snapSlpineToMesh_roll) catch()
rollout RSTA_snapSlpineToMesh_roll "Conform To Surface" width:200
(
	--------------------------------------------------------------
	-- LOCAL 
	--------------------------------------------------------------
	local obj_A
	local obj_B
	
	--------------------------------------------------------------
	-- CONTROLS 
	--------------------------------------------------------------
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:RSTA_snapSlpineToMesh_roll.Width	
	local banner = makeRsBanner dn_Panel:rsBannerPanel versionNum:1.1 versionName:"" wiki:"snap_spline_to_mesh" filename:(getThisScriptFilename())
	--------------------------------------------------------
	group "Spline To Poly/Mesh"
	(
		button btn_setSplineA "> Set Spline A<" width:180
		button btn_setSplineB "> Set Obj B<" width:180
		spinner spn_spline_offset "Offset" offset:[3,0]
		button btn_project "Snap A To B" width:180
	)
	group "Poly/Mesh To Poly/Mesh"
	(
		button btn_setMeshA "> Set Obj A <" width:180
		button btn_setMeshB "> Set Obj B <" width:180
		spinner spn_mesh_offset "Offset" offset:[3,0]
		button btn_Conform_mesh_mesh "Snap Obj A To Obj B" width:180
		checkbox cbx_snapToVert "Snap To Nearest Vert" 
	)
	
	--------------------------------------------------------------
	-- FUNCTIONS 
	--------------------------------------------------------------
	fn isValid type obj =
	(
		case type of 
		(
			#poly: return (classof_array #(Editable_Poly, Editable_mesh) obj) 			
			#spline: return (classof_array #(line) obj)
		)
		return false
	)
	--------------------------------------------------------
	fn set_obj objIdx type =
	(
		if selection.count != 0 then 
		(
			local obj = selection[1]
			
			case type of 
			(
				#spline:
				(
					if objIdx == #a AND (isValid #spline obj) do
					(
						obj_A = obj 
						btn_setSplineA.text = obj_A.name
					)
					if objIdx == #b AND (isValid #poly obj) do
					(
						obj_B = obj 	
						btn_setSplineB.text = obj_B.name
					)	
				)	
				
				#poly:
				(						
					if objIdx == #a AND (isValid #poly obj) do
					(
						obj_A = obj 
						btn_setMeshA.text = obj_A.name
					)
					if objIdx == #b AND (isValid #poly obj) do
					(
						obj_B = obj 	
						btn_setMeshB.text = obj_B.name
					)							
				)
			)
		)
		else messagebox "Nothing Selected!"
	)	
	--------------------------------------------------------
	fn fire_ray startPos vect obj = 
	(			
		local theRay = ray startPos vect	
		local hitData = intersectRayEx obj theRay
		if (hitdata != undefined) do return hitdata[1].pos
		return startPos
	)
	--------------------------------------------------------
	fn copyB = 
	(
		local obj_B_temp =  copy obj_B
		convertToMesh obj_B_temp
		return obj_B_temp
	)
	--------------------------------------------------------
	fn conform_spline_mesh = 
	(
		if (obj_A != undefined) AND (obj_B != undefined) then
		(	
			undo on 
			(			
				local obj_B_temp = copyB()
				
				for s = 1 to  (numSplines obj_A) do
				(
					for k = 1 to (numKnots obj_A s) do
					(
						local knt_pos = getKnotPoint obj_A s k	
						local hitPos = fire_ray knt_pos [0,0,-1] obj_B_temp
						-- OFFSET
						hitPos += [0,0,spn_spline_offset.value]
						
						setKnotPoint obj_A s k hitPos
					)
				)	
				
				updateshape obj_A				
				delete obj_B_temp
			)
		)
		else messageBox "Please set Spline A and Poly B first."
	)
	--------------------------------------------------------
	fn conform_mesh_mesh =
	(
		if (obj_A != undefined) AND (obj_B != undefined) then
		(
			undo on 
			(			
				local obj_B_temp = copyB()
				for idx = 1 to ((RsMeshPolyOp obj_A).getNumVerts obj_A) do 
				(
					local vtx_pos = (RsMeshPolyOp obj_A).getVert obj_A idx 
					local hitPos = fire_ray vtx_pos [0,0,-1] obj_B_temp
					
					if (cbx_snapToVert.state) do 
					(
						local dist
						local clostestPointIdx
						for idx2 = 1 to ((RsMeshPolyOp obj_B).getNumVerts obj_B) do 
						(
							local vtx2_pos = (RsMeshPolyOp obj_B).getVert obj_B idx2 
							local dist_to_vert = distance vtx2_pos hitPos
							if (dist == undefined OR dist_to_vert < dist) do 
							(
								dist = dist_to_vert
								clostestPointIdx = idx2
							)
						)	
						
						hitPos = (RsMeshPolyOp obj_B).getVert obj_B clostestPointIdx 						
					)	

					-- OFFSET
					hitPos += [0,0,spn_mesh_offset.value]
					
					(RsMeshPolyOp obj_A).setVert obj_A idx hitPos
				)
				
				delete obj_B_temp
			)
		)
		else messageBox "Please set Mesh/Poly A and Mesh/Poly B first."
	)
	
	--------------------------------------------------------------
	-- EVENTS	
	--------------------------------------------------------------
	on btn_setSplineA pressed do set_obj #a #spline
	--------------------------------------------------------
	on btn_setSplineB pressed do set_obj #b #spline
	--------------------------------------------------------
	on btn_project pressed do conform_spline_mesh()	
	--------------------------------------------------------
	on btn_setMeshA pressed do set_obj #a #poly
	--------------------------------------------------------
	on btn_setMeshB pressed do set_obj #b #poly
	--------------------------------------------------------
	on btn_Conform_mesh_mesh pressed do conform_mesh_mesh()
	--------------------------------------------------------
	on RSTA_snapSlpineToMesh_roll open do
	(
		rs_dialogPosition "get" RSTA_snapSlpineToMesh_roll 
		banner.setup()
	)	
	--------------------------------------------------------	
	on RSTA_snapSlpineToMesh_roll close do rs_dialogPosition "set" RSTA_snapSlpineToMesh_roll 
)
createDialog RSTA_snapSlpineToMesh_roll style:#(#style_titlebar, #style_border, #style_sysmenu,#style_toolwindow)




