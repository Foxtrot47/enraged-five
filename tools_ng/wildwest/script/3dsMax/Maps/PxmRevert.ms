--/////////////////////////////////////////
--	UI
--/////////////////////////////////////////
--try(destroyDialog PxmRevertUI)catch()
rollout PxmRevertUI "Revert POM Materials" width:400 height:200
(
	--/////////////////////////////////////////
	--	VARIABLES
	--/////////////////////////////////////////
	local pxmMapPath = ( RsConfigGetWildWestDir() + "script/3dsMax/maps/PxmMap.ini")
	local shaderPXM = getINISetting pxmMapPath "pxmmap"
	local shaderOriginal = for item in shaderPXM collect getINISetting pxmMapPath "pxmmap" item
	local mapChannel = 4
	
	--/////////////////////////////////////////
	--	CONTROLS
	--/////////////////////////////////////////
	--dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:400
	--local banner = makeRsBanner dn_Panel:rsBannerPanel versionNum:0.01 versionName:"Futuristic Earth" wiki:"CHANGEME" filename:(getThisScriptFilename())
	timer ctrlState interval:5000 active:false
	dotnetControl btnRevert "Button" text:"Revert POMs" width:(PxmRevertUI.width - 10) height:dnStyle.stdButtonHeight offset:[-10, 0]
	--button btnRevert "Revert POMs" tooltip:"Revert Selected Faces POM Materials"
		
	progressBar prgProcess
	
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////
	
	/***
		Revert POM shaders to nonPOM shaders using mapping from shaderPXM->shaderOriginal
	***/
	fn revertFaces =
	(
		local obj = selection[1]
		if (classof obj != Editable_Mesh) and (classof obj != Editable_Poly) then
		(
			messagebox "Please pick a mesh object" title:"bad Selection"
			return false
		)
		
		--convert the object and face selection if starting from an editable mesh
		if (classOf obj == Editable_Mesh) then
		(
			convertToPoly obj
			collapseStack obj
		)
		
		local faces = obj.selectedFaces as BitArray
		if faces.count == 0 then
		(
			messagebox "No Faces selected" title:"Bad Selection"
			return false
		)
		
		local objMat = obj.material
		local faceIDs = #()
		local getFaceMatIDFunc = undefined
		local setFaceMatIDFunc = undefined
		if (classof obj == Editable_Mesh) then
		(
			getFaceMatIDFunc = getFaceMatID
			setFaceMatIDFunc = setFaceMatID
		)
		else if (classof obj == Editable_Poly) then
		(
			getFaceMatIDFunc = polyop.getFaceMatID
			setFaceMatIDFunc = polyop.setFaceMatID
		)
		
		
		--Process the faces and find the shader on each face
		local matIDFaceList = #()
		for face in faces do
		(
			local matId = getFaceMatIDFunc obj face
			appendIfUnique faceIDs matId
			if matIDFaceList[matId] == undefined then
			(
				matIDFaceList[matId] = #(face)
			)
			else
			(
				append matIDFaceList[matId] face
			)
		)
		--format "faceIDs: % \n" faceIDs
		
		--for each face get the shader on the material
		for idx in faceIDs do
		(
			local mat = obj.material.materialList[idx]
			
			--check if there is a non POM version already by name
			if (matchPattern obj.material.names[idx] pattern:"nonPOM:*") == false then
			(
				local nonPOMIdx = findItem obj.material.names ("nonPOM:"+(idx as String))
				if (nonPOMIdx != 0) then
				(
					for face in matIDFaceList[idx] do
					(
						setFaceMatIDFunc obj face nonPOMIdx
					)
				)
				else
				(
					--dupe the material and add it back to the end of the multisub
					obj.material.materialList[(obj.material.materialList.count + 1)] = copy mat
					local newMat = obj.material.materialList[obj.material.materialList.count]
					local newMatIdx = obj.material.materialList.count
					
					local theShader = RstGetShaderName newMat
					local rawShaderName = (filterString theShader ".")[1]
					local theNewShader = undefined
					
					if (rawShaderName != undefined) then
					(
						local idxMatch = findItem shaderPXM rawShaderName
						if (idxMatch != 0) then	--we got a hot one
						(
							theNewShader = shaderOriginal[idxMatch]
							RstSetShaderName newMat theNewShader
						)
					)
					
					--set the name
					obj.material.names[newMatIdx] = ("nonPOM:"+(idx as String))
					
					--set the new faces mat id
					for face in matIDFaceList[idx] do
					(
						setFaceMatIDFunc obj face newMatIdx
					)
				)
			)
			--print theShader
		)
		
		--ok, now we need the border verts to blend off the pxm strength
		local faceBorderEdges = RSPoly_GetFaceList_BorderEdgeList obj faces
		local borderEdgeVerts = polyop.getVertsUsingEdge obj faceBorderEdges
		
		--get the mapverts from borderEdgeVerts
		prgProcess.value = 0
		local counter = 0
		for vtx in borderEdgeVerts do
		(
			local mapVerts = RsGeom_MapVertsUsingVert obj mapChannel vtx
			
			--set the mapverts to white
			for mv in mapVerts do
			(
				polyop.setMapVert obj mapChannel mv [1.0, 1.0, 1.0]
			)
			
			prgProcess.value = (100.0 * (counter / (borderEdgeVerts.numberSet as float)))
			counter += 1
			windows.processPostedMessages()
		)
		
		prgProcess.value = 0
		CompleteRedraw()
		
		return true
		
	)
	
	
	
	--/////////////////////////////////////////
	--	EVENTS
	--/////////////////////////////////////////
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btnRevert click do
	(
		dnStyle.setExecuteStyle btnRevert
		local success = revertFaces()
		ctrlState.active = true
		if success then dnStyle.setSuccessStyle btnRevert else dnStyle.setErrorStyle btnRevert
	)
	
	--/////////////////////////////////////////
	--	Switch control state on timer trigger
	--/////////////////////////////////////////
	on ctrlState tick do
	(
		dnStyle.setDormantStyle btnRevert
		ctrlState.active = false
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on PxmRevertUI open do
	(
		--banner.setup()
		dnStyle.initButtonStyle btnRevert
	)
	
	
)
--createDialog PxmRevertUI width:400 height:100
