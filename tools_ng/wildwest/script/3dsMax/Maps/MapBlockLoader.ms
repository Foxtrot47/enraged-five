--Map Container Loader


filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

try( destroyDialog MapContainerLoader_UI)catch()
rollout MapContainerLoader_UI "Map Container Loader"
(
	local regex = dotnetclass "System.Text.RegularExpressions.Regex"
	local re_IgnoreCase = (dotnetclass "System.Text.RegularExpressions.RegexOptions").IgnoreCase
	local textures = #()
	local listTextures = #()
	local feedback = undefined
	local blocks = #()
	local blockAreas = #()
	local areaKey = undefined
	local mapContainers = RsConfigGetProjRootDir()+"art/Models"
	local containerDataListFile = RsConfigGetToolsDir() + "etc/config/generic/ContainerLoaderData.xml"
	local theTreeNodes = #()
	local loadList = #()
	
	local missionContainerPath = RsConfigGetArtDir() + "Models/Handy objects/MissonCoordsMapGuide.maxc"
	local cutsceneContainerPath = RsConfigGetArtDir() + "Models/Handy objects/CutsceneCoordsMapGuide.maxc"
	
	----------------------------------------------------------------------------------
	--		CONTROLS
	----------------------------------------------------------------------------------
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:MapContainerLoader_UI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Map Container Loader" filename:(getThisScriptFilename())
	
	group "Blocks"
	(
		checkbox chk_PlusSubTree "Tick SubTree" checked:true across:2
		checkbox chk_ClearSelection "Clear All" checked:false
		dotNetControl tvContainerList "System.Windows.Forms.TreeView" height:200 align:#center
	)
	
	--if gRsIsOutsource == false then
	
	group "Special Containers"
	(
		checkbox chk_MissionContainer "Mission Map Coords Container"
		checkbox chk_CutsceneContainer "Cutscene Coords Container"
	)
	
	
	group "Load"
	(
		button btn_LoadContainers "Load Container(s)" across:3 align:#left
		checkBox chk_LoadPropFiles "+PropFiles" offset:[20,3]
		checkBox chk_chkP4 "Get Latest P4" offset:[05,3] checked:true
		listbox lst_Process readOnly:true
		progressbar prg_Process color:green
	)
	
	------------------------------------------------------------------------------------
	-- DotNet values: 
	------------------------------------------------------------------------------------
	local textFont, dingFont, textFontBold
	local DNcolour = dotNetClass "System.Drawing.Color"
	
	local textCol = (colorMan.getColor #windowText) * 255
	local windowCol = (colorMan.getColor #window) * 255
	local notLoadedCol = if (windowCol[1] < 128) then (windowCol * 1.5) else (windowCol * 0.85)
	
	local textColour = DNcolour.FromArgb textCol[1] textCol[2] textCol[3]
	local backColour = DNcolour.FromArgb 128.0 128.0 128.0
	local altBackColour = DNcolour.FromArgb (windowCol[1]-10) (windowCol[2]-10) (windowCol[3]-10)
	
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn arrangeCtrls size:[MapContainerLoader_UI.width, MapContainerLoader_UI.height] = 
	(
		tvContainerList.width = size.x - 15
		tvContainerList.height = size.y - 330
	)
	
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn recurseXml ele theTree =
	(
		if ele != undefined then 
		(
			for i = 0 to ele.childNodes.count-1 do
			(
				theObject = ele.ChildNodes.itemOf[i]
				if theObject.name == "item" then
				(
					theObjectName = (theObject.GetAttributeNode "name").value					
					theChildTreeNode = theTree.add theObjectName
					theChildTreeNode.name = theObjectName
					if theObject.attributes.getnameditem "type" != undefined then 
					(
						theObjectType = (theObject.GetAttributeNode "type").value
						if theObjectType == "group" then 
						(
							theChildTreeNode.forecolor = (dotNetClass "System.Drawing.Color").fromARGB 0.0 0.0 0.0
							theChildTreeNode.tag = "group"
							
							-- change font style for "group"
							fontFam = dotNetObject "system.drawing.fontfamily" "tahoma" --this makes the font object  
							fontStyle = dotNetClass "system.drawing.fontStyle" --this creates a list from which style can be pulled  
							
							myFont = dotNetObject "system.drawing.font" fontFam 8 fontStyle.underline  
							--the node font can now be overwritten by this line  
							theChildTreeNode.nodeFont = myFont  
						
						) else theChildTreeNode.tag = "file"
					)
					recurseXml ele.ChildNodes.itemOf[i] theChildTreeNode.nodes
				)
			)
		)
	)
	
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn initXML =
	(
		xmlDoc = dotnetobject "System.XML.XMLDocument"
		xmlDoc.load containerDataListFile
		theTree = MapContainerLoader_UI.tvContainerList
		thetree.nodes.clear()
		theEle = xmlDoc.DocumentElement 
		
		-- recurse XML document into treeview nodes
		recurseXML theEle theTree.nodes 
		
		startSize = [MapContainerLoader_UI.width - 10, MapContainerLoader_UI.height - 40]
		arrangeCtrls size:startSize
		tvContainerList.checkBoxes = true
		tvContainerList.sorted = true
		
		--disable special containers if its outsource
		if gRsIsOutsource then
		(
			chk_MissionContainer.enabled = false
			chk_CutsceneContainer.enabled = false
		)
	)
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn notifyProcess inText =
	(
		--get the text array
		theText = lst_Process.items
		
		--append the new item
		append theText inText
		
		--stuff back into the control
		lst_Process.items = theText
		
		--select last item
		lst_Process.selection = theText.count
		windows.processPostedMessages()
	)
		
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn parseTV theNode theKey = 
	(
		theCount = theNode.nodes.count
		for i in 0 to (theCount - 1) do
		(
			theName = theNode.nodes.item[i].text
			if theName != theKey then
			(
				parseTV theNode.nodes.item[i] theKey
			)
			else
			(
				theNode.nodes.item[i].checked = true
			)
		)
	)
	
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn clearTheSelection = 
	(
		theNode = tvContainerList.nodes
			for n = 0 to (theNode.count - 1) do
			(
				theNode.item[n].checked = false
			)

	)
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn checkAllChildNodes treeNode state subTree:true =
	(
		theNode = treeNode.nodes
		if subTree then
		(
			for n = 0 to (theNode.count - 1) do
			(
				--print theNode.item[n].name
				theNode.item[n].checked = state
			)
		)
	)
	
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn collectChecked treeNode = 
	(
		theNode = treeNode.nodes
		for n = 0 to (theNode.count - 1) do
		(
			--print theNode.item[n].name
			if theNode.item[n].checked == true then
			(
					if theNode.item[n].tag != "group" then
					(
						alreadyOpen = getnodebyname theNode.item[n].name
						if alreadyOpen == undefined then
						(
							appendIfUnique loadList theNode.item[n].name
						)
					)
			)
			
			if theNode.item[n].nodes.count > 0 then
			(
				collectChecked theNode.item[n]
			)
		)		
	)
	
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn loadPropFiles = 
	(
		return false
		
		depotFiles = for f in files collect f.depotfile
					
		for item in depotFiles where matchPattern item pattern:(blockName + "_props.maxc)") != false do
		(
			notifyProcess ("Load road prop file:" + item)
			gRsPerforce.p4.run "sync" #(item)
			whereLocal = (gRsPerforce.p4.run "where" #(item)).item[0]
			localPath = whereLocal.fields.item "path"
			propContainer = Containers.CreateInheritedContainer localPath
		)
		
	)
	
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn getFilesRecursive root pattern =
	(
	dir_array = GetDirectories (root+"/*")
	for d in dir_array do
	  join dir_array (GetDirectories (d+"/*"))
	my_files = #()
	for f in dir_array do
	  join my_files (getFiles (f + pattern))
	my_files
	)
	
	-------------------------------
	-- Events dear boy
	-------------------------------
	on tvContainerList AfterCheck s e do
	(
		if chk_PlusSubTree.checked then
		(
			checkAllChildNodes e.node e.node.checked
		)
		else
		(
			checkAllChildNodes e.node e.node.checked subTree:false
		)
	)
	
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	on chk_clearSelection changed val do
	(
		clearTheSelection()
		chk_clearSelection.checked = false
	)
	
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	on btn_loadContainers pressed do
	(
		loadList = #()
		collectChecked tvContainerList
		format "Load: % \n" loadList
		--clear notify
		lst_Process.items=#()
		
		if chk_chkP4.checked then 
		(
			--load each container and process the relevant contents
			files = getDepotFiles mapContainers wildcard:"*.maxc" walk:true

			depotFiles = for f in files collect f.depotfile
		) else (
			files = getFilesRecursive mapContainers "*.maxc" 
		)
		
		--reset progressBar
		prg_Process.value = 0
		maxProgress = loadList.count
		counter = 0
		
		
		--Load Blocks
		notifyProcess "Loading Blocks..."
		for blockName in loadList do
		(
			if chk_chkP4.checked then -- use latest in Perforce
			(
				--find the container path
				containerPath = (for item in files where (getFileNameFile item.depotFile) == blockName collect item.depotFile)[1]
			) else (
				--find the container path
				containerPath = (for item in files where (getFileNameFile item) == blockName collect item)[1]
			)
			
			if containerPath != undefined then
			(					
				if chk_chkP4.checked then -- use latest in Perforce
				(
					--sync the file and on we go
						gRsPerforce.p4.run "sync" #(containerPath)
						
					--get local path
					whereLocal = (gRsPerforce.p4.run "where" #(containerPath)).item[0]
					adjBlockLocalPath = whereLocal.fields.item "path"

					--check we did get the file locally
					if doesFileExist adjBlockLocalPath == false then continue		

					--load the container
					notifyProcess ("Loading: Perforce - " + blockName)
					theContainer = Containers.CreateInheritedContainer adjBlockLocalPath					
				) else (
					--load the container
					notifyProcess ("Loading: Local - " + blockName)
					theContainer = Containers.CreateInheritedContainer containerPath	
				)

			) 
			
			--Load block Prop container
			if chk_LoadPropFiles.checked then
			(
				if chk_chkP4.checked then -- use latest in Perforce
				(
					--notifyProcess ("*" + blockName + "_props.maxc")
					for item in depotFiles where matchPattern item pattern:("*" + blockName + "_props.maxc") == true do
					(
						notifyProcess ("\tLoad: Perforce - prop file")
						gRsPerforce.p4.run "sync" #(item)
						whereLocal = (gRsPerforce.p4.run "where" #(item)).item[0]
						localPath = whereLocal.fields.item "path"
						propContainer = Containers.CreateInheritedContainer localPath
					)
				) else (
					--notifyProcess ("*" + blockName + "_props.maxc")
					for item in files where matchPattern item pattern:("*" + blockName + "_props.maxc") == true do
					(
						notifyProcess ("\tLoad: Local - prop file")							
						propContainer = Containers.CreateInheritedContainer item
					)
				)
			)
			--update Progress
			counter += 1
			prg_Process.value = 100.0 * counter / maxProgress
						
		)
		prg_Process.value = 0
		
		--Load specials
		if chk_MissionContainer.checked then --sync and load
		(
			notifyProcess "Loading Mission Container.."
			gRsPerforce.sync #(missionContainerPath)
			Containers.CreateInheritedContainer missionContainerPath
		)
		
		if chk_CutsceneContainer.checked then --sync and load
		(
			notifyProcess "Loading Cutscene Container.."
			gRsPerforce.sync #(cutsceneContainerPath)
			Containers.CreateInheritedContainer cutsceneContainerPath
		)
		
		max tool zoomextents	
		notifyProcess "Done."
	)
	
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	on MapContainerLoader_UI open do
	(
		if gRsPerforce.connected() == false then gRsPerforce.connect()
		banner.setup()
		gRsPerforce.p4.run "sync" #(containerDataListFile)
		initXML()
	)
)

createDialog MapContainerLoader_UI width:300 height:570 style:#(#style_titlebar, #style_border, #style_sysmenu, #style_minimizebox)
