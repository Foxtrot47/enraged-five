
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

dotnet.loadAssembly  (RsConfigGetToolsDir() + "techart/dcc/3dsMax/RSG.TechArt.IPExtractor.dll")

--/////////////////////////////////////////
--	UI
--/////////////////////////////////////////
try(destroyDialog IPExtractorUI)catch()
rollout IPExtractorUI "IP Extractor" width:400 height:210
(
	--/////////////////////////////////////////
	--	VARIABLES
	--/////////////////////////////////////////
	local selectedImap
	local imapPaths = #()
	local imapPathsStripped = #()
	local imap = dotnetobject "IPExtractor.IPExtractor"
	--local iptype = "prop"
	
	--/////////////////////////////////////////
	--	CONTROLS
	--/////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:IPExtractorUI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"CHANGEME" versionNum:0.3 versionName:"Advisory Circle" filename:(getThisScriptFilename())
	
	listbox lstImaps height:10
	button btnExtract "Extract Selected" width:(IPExtractorUI.width - 30)
		
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////
	fn createPlaneData pos size =
	(
		v1 = [(pos.x - size), (pos.y + size), pos.z]
		v2 = [(pos.x - size), (pos.y - size), pos.z]
		v3 = [(pos.x + size), (pos.y - size), pos.z]
		v4 = [(pos.x + size), (pos.y + size), pos.z]
		
		#(v1, v2, v3, v4)
		
	)
	
	
	fn makeBoundsOffsetPositions positions boundsMin boundsMax &meshVertData &meshFaceData =
	(
		for x=0 to positions.count-1 do
		(
			local xPos = boundsMin[1] + (positions.Item[x][1] * ((boundsMax[1] - boundsMin[1]) / 65536))
			local yPos = boundsMin[2] + (positions.Item[x][2] * ((boundsMax[2] - boundsMin[2]) / 65536))
			local zPos = boundsMin[3] + (positions.Item[x][3] * ((boundsMax[3] - boundsMin[3]) / 65536))
			
			--d = point pos:[xPos, yPos, zPos] constantscreensize:off size:0.5
			--d.parent = b
			
			--create mesh
			local verts = createPlaneData [xPos, yPos, zPos] 0.25
			
			join meshVertData verts
			inPt = meshVertData.count - 3
			local face1 = [inPt, inPt+1, inPt+2]
			append meshFaceData face1
			local face2 = [inPt, inPt+2, inPt+3]
			append meshFaceData face2
			
		)
		
		ok
	)
	
	
	fn makeAbsolutePositions positions &meshVertData &meshFaceData =
	(
		for x=0 to positions.count-1 do
		(
			local xPos = positions.Item[x][1]
			local yPos = positions.Item[x][2]
			local zPos = positions.Item[x][3]
			
			--d = point pos:[xPos, yPos, zPos] constantscreensize:off size:0.5
			--d.parent = b
			
			--create mesh
			local verts = createPlaneData [xPos, yPos, zPos] 0.25
			
			join meshVertData verts
			inPt = meshVertData.count - 3
			local face1 = [inPt, inPt+1, inPt+2]
			append meshFaceData face1
			local face2 = [inPt, inPt+2, inPt+3]
			append meshFaceData face2
			
		)
	)


	fn createIPVis imapPath =
	(
		local data = undefined
		try
		(
			data = imap.ExtractData imapPath
		)
		catch
		(
			print "No Elements in source file"
			messageBox "This imap has no IP" title:"Empty IMAP"
			return false
		)
		
		local iptype = imap.IPType
		
		local objectList = #()
		for i=0 to (data.Count-1) do
		(
			local boundsMin = data.Item[i].GetBounds "min"
			local boundsMax = data.Item[i].GetBounds "max"
			
			--make bounds vis
			local boundsPos = [(boundsMin[1] + (0.5 * (boundsMax[1] - boundsMin[1]))), (boundsMin[2] + (0.5 * (boundsMax[2] - boundsMin[2]))),  (boundsMin[3] + (0.5 * (boundsMax[3] - boundsMin[3])))]
			--y
			local boundsLength = boundsMax[2] - boundsMin[2]
			--x
			local boundsWidth = boundsMax[1] - boundsMin[1]
			--z
			local boundsHeight = boundsMax[3] - boundsMin[3]
				
			
			--get colour from definition lookup
			local colourHexValue = imap.colourLookup.item[data.Item[i].archetype]
			colourHexValue = substituteString colourHexValue "#" "0x00"
			local procColour = RsHexARGBToColor colourHexValue
			
			local bname = (getFilenameFile imapPath) + "_" + data.Item[i].archetype
			local b = Box name:bname pos:boundsPos length:boundsLength width:boundsWidth height:boundsHeight wirecolor:procColour
			b.pos.z -= (boundsHeight / 2.0)
			b.boxmode = on
			append objectList b
			
			local meshVertData = #()
			local meshFaceData = #()
			
			local positions = undefined
			case iptype of
			(
				"GrassInstanceList":
				(
					positions = data.Item[i].GetPositions()
					makeBoundsOffsetPositions positions boundsMin boundsMax meshVertData meshFaceData
				)
					
				"PropInstanceList":
				(
					positions = data.Item[i].GetPropPositions()
					makeAbsolutePositions positions meshVertData meshFaceData
				)
			)
			
						
			local proxyMesh = mesh vertices:meshVertData faces:meshFaceData
			CenterPivot proxyMesh
			
			--set mesh colour based on def lookup
			meshop.setMapSupport proxyMesh 0 true
			for i=1 to (getNumCPVVerts proxyMesh) do
			(
				meshop.setMapVert proxyMesh 0 i procColour
			)
			
			proxymesh.showVertexColors = on
			
			--parent mesh to the bounds box
			proxyMesh.parent = b
		)
		
		select objectList
		max zoomext sel all
	)
	
	fn discoverLocalImaps =
	(
		local searchPath = (RsMakeSafeSlashes gRsCoreProject.Cache) + "/instance_placement/" + gRsCoreBranch.name
		imaps = getFilesRecursive searchPath "*.imap"
	)
	
	--/////////////////////////////////////////
	--	EVENTS
	--/////////////////////////////////////////
	
	on btnExtract pressed do
	(
		createIPVis selectedImap
	)
	
	on lstImaps selected val do
	(
		selectedImap = imapPaths[val]
		--print selectedImap
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on IPExtractorUI open do
	(
		banner.setup()
		
		imapPaths = discoverLocalImaps()
		imapPathsStripped = for item in imapPaths collect getFilenameFile item
		lstImaps.items = imapPathsStripped
		
		local imapConfigPath = RsConfigGetToolsDir() + "etc/processors/RSG.Pipeline.InstancePlacementProcessor.xml"
		imap.BuildDefinitionColorLookup imapConfigPath
	)
	
	
)
createDialog IPExtractorUI
