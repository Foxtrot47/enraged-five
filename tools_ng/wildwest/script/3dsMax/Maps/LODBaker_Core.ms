if (not gRsIsOutsource) do ( filein (RsConfigGetWildWestDir() + "script/3dsmax/_config_files/wildwest_header.ms") )
filein ("pipeline/util/rect.ms")

struct LODBakerStruct
(
	LODSceneRoot = (RsConfigGetArtDir() + "Models/Handy objects/LODBakes/"),
	iniPath = LODSceneRoot + "LODBakerBatch.ini",
	processLog = LODSceneRoot + "Process.log",
	mapRes_X = 0,
	mapRes_Y = 0,
	res = #("64", "128", "256", "512", "1024", "2048"),
	txScale = "To100pcScene",
	meshop_getMapVert = meshop.getMapVert,
	meshop_getMapVertsUsingMapFace = meshop.getMapVertsUsingMapFace,
	meshop_getVertsUsingFace = meshop.getVertsUsingFace,
	meshop_getVert = meshop.getVert,
	bucketCount = 4,
	bucketSize = 1.0 / bucketCount as Float,
	bucketCentres = for b = 1 to bucketCount collect b * bucketSize - 0.5 * bucketSize,
	bucketPadding = 0.05,
	buckets = for u = 1 to bucketCount collect #(),
	bucketList = #(#(1, 1),#(1, 2), #(1, 3), #(1, 4),
						#(2, 1), #(2, 2), #(2, 3), #(2, 4),
						#(3, 1), #(3, 2), #(3, 3), #(3, 4),
						#(4, 1), #(4, 2), #(4, 3), #(4, 4)),
	camTarget = undefined,
	visibilityBuckets = #(),
	vpX = gw.getWinSizeX(),
	vpY = gw.getWinSizeY(),
	bakePath = "c:/temp/LODMapXXX.bmp",
	defaultBackGroundColour = color 80 83 61,
	-- DEBUG defaultBackGroundColour = color 255 0 255,
	aDecalMeshes = #(),
	mapFaceLookup = #(),
	networkBakePath = "N:\\RSGEDI\\Temp\\LODBakes",
	timingLog = pathConfig.appendPath networkBakePath "timer.log",
	allMapFaces = undefined,
	
	--//////////////////////////////////////////////////////
	--Functions
	--//////////////////////////////////////////////////////
	fn createBakeLog =
	(
		if (doesfileexist processLog) then
		(
			flushlog processLog
			deleteFile processLog
		)
		else
		(
			try
			(
				openlog processLog
			)
			catch
			(
				print "Couldnt create process.log"
			)
		)
	),
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	fn detachDecalFaces obj =
	(
		--clear aDecalMeshes
		aDecalMeshes = #()
		
		--For storing leached attrs
		local attrs = undefined
		
		if (classOf obj) != RSrefObject and obj.isHidden != true then
		(
			--identify any decal shaders in the mesh, split them off into a seperate objects
			local decalIDs = multiMaterialHasDecals obj
			
			local decalFacesIDs = #()
			
			if decalIDs != undefined then
			(
				--check decalIDs count, if its the same as the object face count then the whole object is made of decals so exit early
				--if decalIDs.count == obj.faces.count then return false
				--leach the host attrs
				--attrs = leachGtaAttrs obj
				
				--split decal meshes from host
				for id in decalIDs do
				(
					--local idFaces = #()
					if (classOf obj) == Editable_mesh then
					(
						idFaces = for f in obj.faces where (getFaceMatID obj f.index) == id collect f.index
						join decalFacesIDs idFaces
						
					)
					else if (classOf obj) == Editable_Poly then
					(
						idFaces = for f in obj.faces where (polyOp.getFaceMatID obj f.index) == id collect f.index
						join decalFacesIDs idFaces
					)
				)
				
				--is there any decal shaded faces on the mesh? exit  early if not
				if decalFacesIDs.count == 0 then return false
					
				--dupe the object and delete the non decal faces
				local decalMesh = copy obj
				collapseStack decalMesh
				--break()
				--objFaces = obj.faces as 
				local nonDecalFaces = (obj.faces as BitArray) - (decalFacesIDs as BitArray)
				--decalFaces = -decalFaces
	
				if (classOf decalMesh) == Editable_mesh then
				(
					meshop.deleteFaces decalMesh nonDecalFaces
					--delete the decal faces from the original
					meshop.deleteFaces obj (decalFacesIDs as BitArray)
				)
				else if (classOf decalMesh) == Editable_Poly then
				(
					polyop.deleteFaces decalMesh nonDecalFaces
					--delete the decal faces from the original
					polyop.deleteFaces obj (decalFacesIDs as BitArray)
				)
				
				--add to an array that contains host and decalMesh so we can find them again later to re-attach to the correct host
				append aDecalMeshes (DataPair host:obj.handle decal:decalMesh.handle)
				
				--Re-apply the Gta Attrs we leached from the host
				--for i = 1 to attrs.count do SetAttr decalMesh i attrs[i]
				
			)
		)
	),
	
	--//////////////////////////////////////////////////////
	--barycentric
	-- p = v1 + U * (v2-v1) + v * (v3-v1)
	--//////////////////////////////////////////////////////
	fn PointInTriangle pt A:[0, 0, 0] B:[1, 0, 0] C:[0, 1 ,0] =
	(
		/*
		T den = 1 / ((b.y - c.y) * (a.x - c.x) + (c.x - b.x) * (a.y - c.y));

			lambda.x = ((b.y - c.y) * (vec.x - c.x) + (c.x - b.x) * (vec.y - c.y)) * den;
			lambda.y = ((c.y - a.y) * (vec.x - c.x) + (a.x - c.x) * (vec.y - c.y)) * den;
			lambda.z = 1 - lambda.x - lambda.y;

		*/

		-- Compute barycentric coordinates
		ptYcY = pt.y - c.y
		ptXcX = pt.x - c.x
		bycy = b.y - c.y
		axcx = a.x - c.x
		cxbx = c.x - b.x
		
		invDenom = 1.0 / (bycy * axcx + cxbx * (a.y - c.y))
		
		u = (bycy * ptXcX + cxbx * ptYcY) * invDenom
		v = ((c.y - a.y) * ptXcX + axcx * ptYcY) * invDenom

		-- Check if point is in triangle
		--return (u >= 0) && (v >= 0) && (u + v < 1)
		baryPt = [u, v, 0]
		
	),
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	fn getNearestBucket pt =
	(
		--U
		proxU = 1.0
		UIdx = 0
		for i = 1 to bucketCount do
		(
			dist = abs (pt.x  - bucketCentres[i])
			if dist < proxU then
			(
				proxU = dist
				UIdx = i
			)
		)
		
		--V
		proxV = 1.0
		VIdx = 0
		for i = 1 to bucketCount do
		(
			dist = abs (pt.y  - bucketCentres[i])
			if dist < proxV then
			(
				proxV = dist
				VIdx = i
			)
		)
		
		#(UIdx, VIdx)
	),
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	fn genBucketList =
	(
		bucketList = #()
		for x = 1 to bucketCount do
		(
			for y = 1 to bucketCount do
			(
				append bucketList #(x, y)
			)
		)
	),
	
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	fn preProcess obj =
	(
		buckets = for u = 1 to bucketCount collect #()
		mapFaceLookup = #()
		local bucketCentreDist = bucketCentres[2]-bucketCentres[1]
		local bucketDiagonal = sqrt (bucketCentreDist*bucketCentreDist + bucketCentreDist*bucketCentreDist)
		local bucketRange = bucketDiagonal + (0.05 * bucketDiagonal)
		local bucketHalfWidth = 0.5 * bucketSize
		
		--Check for map support
		if (meshop.getMapSupport obj 1) == false then
		(
			messagebox "No UV for the SLOD, Have a look" title:"Error"
			return false
		)
		
		--------------------------------------------------------------
		--iterate map faces and dump into buckets
		--------------------------------------------------------------
		--allMapFaces = for idx=1 to (meshop.getNumMapFaces obj 1) collect meshop.getMapFace obj 1 idx
		for idx=1 to (meshop.getNumMapFaces obj 1) do
		(
			--get map verts for face
			local mfPoints = meshop.getMapFace obj 1 idx
			append mapFaceLookup mfPoints
			
			--get avg pos, thats the centre
			local centrePos = [0, 0, 0]
			for i=1 to 3 do
			(
				local mvPos = meshop.getMapVert obj 1 mfPoints[i]
				centrePos += [mvPos.x, mvPos.y, 0]
			)
			centrePos /= 3
			
			--test the distance to each of the tripoints, largest is the circle radius
			local radius = 0
			for i=1 to 3 do
			(
				local mvPos = meshop.getMapVert obj 1 mfPoints[i]
				dist = distance centrePos [mvPos.x, mvPos.y, 0]
				if dist > radius then radius = dist
			)
			radius *= 2.0
			
			
			--alternate method???
			--mapface can be in multiple buckets
			
			--if distance form centrePos to bucket centre is less than bucketcentre to bucket corner + 0.25 * that distance
			for u = 1 to bucketCount do
			(
				for v = 1 to bucketCount do
				(
					bucketHit = false
					if distance [centrePos.x, centrePos.y, 0] [bucketCentres[u], bucketCentres[v], 0] < bucketRange  then
					(
						--make sure theres an array there to append to
						if buckets[u][v] == undefined then
						(
							buckets[u][v] = #()
						)
						
						--check the points to see if any within the bucket bounds
						for i=1 to 3 do
						(
							mvPos = meshop_getMapVert obj 1 mfPoints[i]
							
							if mvPos.x > (bucketCentres[u]-bucketHalfWidth)	and 
							mvPos.x < (bucketCentres[u]+bucketHalfWidth) and
							mvPos.y > (bucketCentres[v]-bucketHalfWidth) and
							mvPos.y < (bucketCentres[v]+bucketHalfWidth) then 
							(
								bucketHit = true	--its inside the bucket
							)
						)
					)
					if bucketHit then append buckets[u][v] #(idx, (DataPair centre:centrePos radius:radius))
				)
			)
			
			
			--append datapair to the uvTriLoci array
			--append uvTriLoci (DataPair centre:centrePos radius:radius)
			--append uvTriLoci[bucketU][bucketV] #(idx, (DataPair centre:centrePos radius:radius))
		)
	),
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	fn dnFlipBitmap theBmp =
	(
		setclipboardBitmap theBmp 
		clipboard = dotnetclass "Clipboard"
		b = clipboard.GetImage()
		b.RotateFlip (dotnetclass "System.Drawing.RotateFlipType").RotateNoneFlipY
		clipboard.SetImage b
		getclipboardBitmap() 
	),
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	fn tidyUp obj LODMap start progressMax =
	(
		progressEnd()
		--revert viewport size
		gw.setPos 0 0 vpX vpY
		
		obj.isHidden = false
		delete camTarget
		--delete theCamera
		--flip the bitmap
		flipLODMap = dnFlipBitmap LODMap
		
		--save it
		flipLODMap.filename = bakePath
		success = save flipLODMap
		format "BakePath: %\n" bakePath
		format "Exists? %\n" (doesFileExist bakePath)
		if not success then messagebox "LOD Map Image didnt save :(" title:"SaveError"
		
		--now copy to T: for safe keeping and review
		local mapName = filenameFromPath bakePath
		local mapNameNoExt = getfilenamefile mapName
		local mapNameBits = filterString mapNameNoExt "_"
		local blockPath = ""
		local containerPath = ""
		
		--if its a SLOD bake then [3] should be SLOD
		if not gRsIsOutSource do
		(
			if mapNameBits[3] == "SLOD" then --make dirs as appropriate for this bake
			(
				blockPath = pathConfig.appendPath networkBakePath mapNameBits[1]
				containerPath = pathConfig.appendPath blockPath (mapNameBits[1] + "_" + mapNameBits[2])
				
				if doesFileExist containerPath == false then --create the dir
				(
					success = makeDir containerPath all:true
					if not success then format "Couldnt create dir: %\n" containerPath
				)
			)
			
			if containerPath != "" then
			(
				local tempStorePath = (pathConfig.appendPath containerPath mapName)
				if doesFileExist tempStorePath == true then
				(
					success = deleteFile tempStorePath
					if not success then format "Couldnt delete: %\n" tempStorePath
					success = copyFile bakePath tempStorePath
					if not success then format "Couldnt Copy: src=% dest=%\n" bakePath tempStorePath
				)
				else
				(
					success = copyFile bakePath tempStorePath
					if not success then format "Couldnt Copy: src=% dest=%\n" bakePath tempStorePath
				)
			)
		)
		
		freeSceneBitmaps()
		completeRedraw()
		display flipLODMap
		
		--frame all
		max zoomext sel all
		
		--shwo command panel
		cui.commandPanelOpen = true
		
		end = timeStamp()
		duration = ((end-start) / 1000.0)
		format "Bake Time: % perpixel: %\n" duration (duration / progressMax)
		
		--try to write to the timing file
		try
		(
			if doesFileExist timingLog and (not gRsIsOutSource) then
			(
				local timingLogHND = openFile timingLog mode:"a"
				format "%, %, %, %, % \n" maxFileName (mapRes_X as String + ":" + mapRes_Y as String) sysInfo.computername duration localtime to:timingLogHND
				close timingLogHND
			)
		)
		catch(if (not gRsIsOutSource) do format "Failed to write to timing file.\n")
	),
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	fn syncSceneTex =
	(
		sceneBitmaps = getClassInstances Bitmaptexture
		sceneBitmapPaths = for bm in sceneBitmaps collect bm.filename
		gRsPerforce.sync sceneBitmapPaths silent:true
	),
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	fn doTheBake mode:"manual" =
	(
		if mode == "manual" then
		(
			--Check we have a bakepath set
			if ::LODBakerUI.edtPath.text == "" then
			(
				messageBox "No bake path set, please set one to continue." caption:"Error"
				return false
			)
			
			if doesFileExist bakePath == true then
			(
				query = queryBox "LOD Map already exists\nDo you want to overwrite it?"
				if query == false then return false
			)
			
			--make sure we have full size textures
			macros.run "RS Utils" txScale
		)
		else
		(
			--attempt to sync scene textures
			syncSceneTex()
			
			--make sure we have full size textures
			--hard set to 50% for url:bugstar:1031124
			macros.run "RS Utils" "To50pcScene"
		)
		
		--hide command panel
		cui.commandPanelOpen = false
		
		format "Baking...\n"
		local start = timeStamp()
		
		--the LOD to bake
		local sel = undefined
		if mode == "manual" then
		(
			sel = (getCurrentSelection())[1]
		)
		else	--auto
		(
			sel = ($*SLODBASE_nocrunch*)[1]
		)
		
		if not (isValidNode sel) then
		(
			messagebox "You need to select your SLOD" title:"Error"
			--if mode == "Auto" then bakeLog
			return false
		)
		
		local obj = snapshotasmesh sel
		
		--create bitmap to render into
		local LODMap = bitmap mapRes_X mapRes_Y color:defaultBackGroundColour
		
		--create camera
		camTarget = TargetObject()
		local theCamera = TargetCamera fov:0.001 target:camTarget
		viewport.setType #view_camera
		viewport.setCamera theCamera
		viewport.SetRenderLevel #smoothhighlights
		viewport.SetTransparencyLevel 1
		gw.setRndLimits #(#flat, #backcull, #texture, #zBuffer)
		actionMan.executeAction 0 "63509"  -- Views: Hardware Display with Maps
		if viewport.GetShowEdgeFaces() == true then actionMan.executeAction 0 "557" --turn off edges
		displayColor.shaded = #material
		displayColor.wireframe = #material
		IDisplayGamma.colorCorrectionMode = #none
		
		macros.run "RS Utils" "SwitchSceneDXMats"
		
		completeRedraw()
		
		--unhide all if auto mode
		if mode == "Auto" then
		(
			for o in $objects do
			(
				o.isHidden = false
				o.wirecolor = defaultBackGroundColour
				o.showVertexColors = off
			)
		)
		
		--hide SLOD
		sel.isHidden = true
		
		----
		--set the viewport
		----
		gw.setPos 100 100 2 2
			
		--local mapFaceLookup = #()
		
		--preprocess the mesh
		format "Start PreProcess\n"
		
		
		--Get geometry source
		GeometrySourceList = for GeoObj in Geometry where GeoObj.isHidden != true collect GeoObj
		
		--Set vertex colors to white
		ResetVCMod = UVWMap MapType:0 Length:0.0 Width:0.0 UTile:2.0 VTile:2.0 WTile:2.0 Channel:1
		for GeometrySource in GeometrySourceList do
		(
			AddModifier GeometrySource ResetVCMod
		)
		
		genBucketList()
		--objectBuckets()
		local success = preProcess obj
		if success == false then return false
		
		--max select all
		--max hide selection
		--format "stage1 time: % \n" ((timeStamp() - start) / 1000.0)
		
		local mapXStep = 1.0 / mapRes_X
		local mapXOffset = 0.5 * mapXStep
			
		local mapYStep = 1.0 / mapRes_Y
		local mapYOffset = 0.5 * mapYStep
			
		local progressMax = mapRes_X * mapRes_Y
		
		local lastBucket = [1, 1]
		--for m in visibilityBuckets[1][1] do unhide m
		progressStart "Rendering.."
		progressUpdate 1	--just get it visible
		local start = timeStamp()
		with undo off
		(
			local bucketPxWidth = (mapRes_X / bucketCount) as Integer
			local bucketPxHeight = (mapRes_Y / bucketCount) as Integer
			local pixelCount = bucketPxWidth * bucketPxHeight
			
			for thisBucket in bucketList do
			(
				--check visibility
				/*
				if (thisBucket[1] != lastBucket.x) or (thisBucket[2] != lastBucket.y) then
				(
					--format "visibility: % % % %\n" v u bucketIdx[1] bucketIdx[2]
					with redraw off
					(
						for m in visibilityBuckets[lastBucket.x][lastBucket.y] do hide m
						for m in visibilityBuckets[thisBucket[1]][thisBucket[2]] do unhide m
					)
					lastBucket = [thisBucket[1], thisBucket[2]]
				)
				*/
				
				--bucketMapFaces = uvTriLoci[bucketIdx[1]][bucketIdx[2]]
				local bucketMapFaces = buckets[thisBucket[1]][thisBucket[2]]
				--bucketMapFaces = allMapFaces
				--if bucketMapFaces == undefined then continue
				
				local startPxV = (thisBucket[1] - 1) * bucketPxWidth
				local endPxV = startPxV + (bucketPxWidth - 1)
				
				local startPxU = (thisBucket[2] - 1) * bucketPxHeight
				local endPxU = startPxU + (bucketPxHeight - 1)
				
				--format "startPxV: % \n" startPxV
				--format "endPxV: % \n" endPxV
				--format "startPxU: % \n" startPxU
				--format "endPxU: % \n" endPxU
				
				for v=startPxV to endPxV do
				(
					for u=startPxU to endPxU do
					(
						--format "v: % u: % \n" ((v*mapXStep) + mapXOffset) ((u*mapYStep) + mapYOffset)
						--uvSamplePos
						local uvSamplePos = [(v*mapXStep) + mapXOffset, (u*mapYStep) + mapYOffset, 0]
						
						--look for nearest map faces
						local closeMapFaces = #()
						--closeMapFaces = for t=1 to bucketMapFaces.count collect bucketMapFaces[t][1]
						if bucketMapFaces == undefined then continue
						for t=1 to bucketMapFaces.count do
						(
							if (distance uvSamplePos bucketMapFaces[t][2].centre) < bucketMapFaces[t][2].radius then append closeMapFaces bucketMapFaces[t][1]
						)
						
				
						--bail if no hits
						--format "u: % v: %\n" u v
						--if closeMapFaces.count == 0 then continue
						
						--format "closeMapFaces count: % \n" closeMapFaces.count
						if closeMapFaces.count > 0 then
						(
							if (amax closeMapFaces) > (meshop.getNumMapFaces obj 1) then break()
							--now see which map face we are inside and get the barycentric coords inside it
							local sampleMapFace = 0
							local sampleMapFaceCoord = [0, 0, 0]
							for mf=1 to closeMapFaces.count do
							(
								--test if uvSamplePos is inside the mapface
								local mapVertsIdx = mapFaceLookup[closeMapFaces[mf]]
								local mapVertPos = for i=1 to 3 collect meshop_getMapVert obj 1 mapVertsIdx[i]
								
								--test by getting barycentric coords
								local baryPt = PointInTriangle uvSamplePos A:mapVertPos[1] B:mapVertPos[2] C:mapVertPos[3]
								--format "baryPt: % \n" baryPt as string
								if baryPt.x >= 0.0 and baryPt.y >= 0.0 and (baryPt.x + baryPt.y) < 1.0 then
								(
									sampleMapFace = closeMapFaces[mf]
									sampleMapFaceCoord = baryPt
								)
							)
							
							if sampleMapFace == 0 or sampleMapFaceCoord == [0, 0, 0] then
							(
								--print "no sample"
								continue
							)
							
							--find the corresponding geo face world space position
							local facePts = getFace obj sampleMapFace
							local facePtsPos = for v=1 to 3 collect meshop_getVert obj facePts[v]
							local wsPos = facePtsPos[3] + ((sampleMapFaceCoord.x * (facePtsPos[1]-facePtsPos[3])) + (sampleMapFaceCoord.y * (facePtsPos[2]-facePtsPos[3])))
							--Point pos:wsPos
								
							--get the face normal
							local faceNormal = getFaceNormal obj sampleMapFace
							
							--reverse the normal
							--faceNormal *= -1
							
							--set the camera pointing down the normal and a suitable distance away
							camTarget.pos = wsPos + (-50.0 * faceNormal)
							theCamera.pos = wsPos + (100.0 * faceNormal)
								
							--take a snap
							completeRedraw()
							local snap = gw.getViewportDib()
							
							--paste it into the LODMap
							pasteBitmap snap LODMap [0, 0] [v, u] type:#paste
							free snap
							
							if getProgressCancel() == true then tidyUp sel LODMap start progressMax
							
						)
					)
				)
				pixelCount += bucketPxWidth * bucketPxHeight
				--format "pixelCount: % \n" pixelCount
				progressUpdate  (100.0 * (pixelCount / progressMax))
			)
		)
		
		--Revert vertex colors changes
		for GeometrySource in GeometrySourceList do
		(
			DeleteModifier GeometrySource ResetVCMod
		)
		
		tidyUp sel LODMap start progressMax
	)
)

LODBaker = LODBakerStruct()