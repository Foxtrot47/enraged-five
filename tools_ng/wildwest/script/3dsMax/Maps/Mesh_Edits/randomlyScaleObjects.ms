-- Scale randomiser tools


-- Rick Stirling
-- Rockstar North
-- July 2011


-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")


-- Set up a default scale tolerance
scaleTolerance = [0.2,0.2,0.2] -- defafult scale for x, y and z
validScaleObjectTypes = #(Editable_Poly,Editable_mesh, RSrefObject)


fn resetScaledTransforms obj= (
	resetxform obj
	maxOps.CollapseNode obj off
)



-- We only want to scale certain types of object, this function returns tru
fn isValidToScale obj = (
	
	isValid = false
	for meshTypeIndex = 1 to validScaleObjectTypes.count do 
	(
		if classof obj == validScaleObjectTypes[meshTypeIndex] then isValid = true
	)
	
	isValid -- return this
)



fn randomScaleObjects objectlist scaletolerance uniformBool= (
	for obj in objectlist do 
	(
		if (isValidToScale obj) then
		(
			if classof obj == RSrefObject then isRSref = true else isRSref = false

			-- Get a random scale value for each axis 
			randomScaleTolerancex =	(random (scaleTolerance[1]*-1) scaleTolerance[1])
			randomScaleTolerancey =	(random (scaleTolerance[2]*-1) scaleTolerance[2])
			randomScaleTolerancez =	(random (scaleTolerance[3]*-1) scaleTolerance[3])
			
			randomScaleTolerance = [randomScaleTolerancex,randomScaleTolerancey,randomScaleTolerancez]
			
			
			-- Uniform  scales and RSref objects need to be handled a little differently
			-- Uniform uses the random X scale for all 3 axis, use the already set value
			if uniformBool == true then randomScaleTolerance = [randomScaleTolerance[1],randomScaleTolerance[1],randomScaleTolerance[1]]
			
			-- RSref must have a matching x and y, but z can be different. Use the previous line to handle uniformity in all 3 axis
			-- If all 3 axis are not uniform overwrite the y with the x value and leave z alone.
			if isRSref == true then randomScaleTolerance = [randomScaleTolerance[1],randomScaleTolerance[1],randomScaleTolerance[3]]

			randomScaleModified = obj.scale + randomScaleTolerance

			--print (randomScaleModified as string)
			
			scale obj randomScaleModified
				
			if isRSref == false then resetScaledTransforms obj
			
		)
	)
	
)



-- UI
rollout randomscale "Object Random Scale" width:190 height:150
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Random_Scale_Objects" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	
	spinner spnScaleAmountX "X -/+ % Range:" range:[0,50,(scaleTolerance[1] * 100)] type:#integer width: 70
	spinner spnScaleAmountY "Y -/+ % Range:" range:[0,50,(scaleTolerance[2] * 100)] type:#integer width: 70
	spinner spnScaleAmountZ "Z -/+ % Range:" range:[0,50,(scaleTolerance[3] * 100)] type:#integer width: 70
	
	checkbox chkUniformScale "Uniform scale using X value" checked:true

	button btnScaleSelected "Scale Valid Selected Meshes" width:160 height:30 tooltip:"Scale Selected Objects"


	on btnScaleSelected pressed do 
	(	
		objectlist = getcurrentSelection()
		
		scaleFromUIX = ((randomscale.spnScaleAmountX.value)/100 as float)
		scaleFromUIY = ((randomscale.spnScaleAmountY.value)/100 as float)
		scaleFromUIZ = ((randomscale.spnScaleAmountZ.value)/100 as float)
		
		scalefromUI = [scaleFromUIX,scaleFromUIY,scaleFromUIZ]
		
		randomScaleObjects objectlist scalefromUI randomscale.chkUniformScale.state
	)
)


createDialog  randomscale