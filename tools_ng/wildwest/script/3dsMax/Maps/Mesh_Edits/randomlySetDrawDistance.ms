-- draw distance randomiser tool

-- Rick Stirling
-- Rockstar North
-- August 2011


-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")


-- Set up a default scale tolerance
defaultLodDistance = 250
defaultDistanceTolerance = 0.15 -- 15% 
validScaleObjectTypes = #(Editable_Poly,Editable_mesh, RSrefObject)




-- We only want to scale certain types of object, this function returns tru
fn isValidToScale obj = (
	
	isValid = false
	for meshTypeIndex = 1 to validScaleObjectTypes.count do 
	(
		if classof obj == validScaleObjectTypes[meshTypeIndex] then isValid = true
	)
	
	isValid -- return this
)



fn randomSetDistance objectlist baseDistance distanceTolerance useScale = (
	for obj in objectlist do 
	(
		if (isValidToScale obj) then
		(

			varRange = baseDistance * distanceTolerance
			
			-- Get maximum deviation
			scaleModSeed = obj.scale.x 
		
			
			Rscalemod = (random 1.0 (scalemodSeed * 5)) 
			if scaleModSeed < 1.0 then Rscalemod  = Rscalemod  * -1
			if useScale == true then scaleMod = scaleModSeed else scaleMod = 0
			
			randomDistance = (random (baseDistance - varRange) (baseDistance + varRange)) +scaleMod

			-- convert to 2 decimal places
			randomDistance= ((dotNetClass "System.Math").round (dotNetObject "System.Double" randomDistance) 2) as float
			
			indexLodDistance = GetAttrIndex (getAttrClass obj) "LOD distance"
			AttrSet = SetAttr obj indexLodDistance  randomDistance
				
		)
	)
	
)



-- UI
rollout randomdistance "Object Random LOD Distance" width:190 height:150
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Randomise_Lod_distance" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	spinner spnLodDistanceBase "Base Lod Distance:" range:[0,500,defaultLodDistance] type:#integer fieldwidth: 40 width: 100
	spinner spnLodChange "% Variance Range:" range:[0,50,(defaultDistanceTolerance*100)] type:#integer fieldwidth: 40  width: 100

	
	checkbox chkUseScale "Include Object Scale" checked:false

	button btnRandomSetDistance "Set LOD distance on selected" width:160 height:30 tooltip:"Set LOD distance on selected"


	on btnRandomSetDistance  pressed do 
	(	
		objectlist = getcurrentSelection()
		perVar = (spnLodChange.value/100 as float)

		randomSetDistance  objectlist spnLodDistanceBase.value perVar chkUseScale.state
	)
)


createDialog   randomdistance