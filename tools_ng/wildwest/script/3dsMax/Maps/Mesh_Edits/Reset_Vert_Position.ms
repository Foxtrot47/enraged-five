-- Vertex postion reset tool
-- Rick Stirling, Rockstar north
-- December 2011


-- This tool will store the vertex positions of a mesh and reset them after other work
-- Initially created to preserve mesh edges for soft selecting meshes

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())


storedVertPositions =#()	
	
fn storeVertexPositions selObj  = 
(

	storedVertPositions =#()
	
	select selObj
	max modify mode
	modPanel.setCurrentObject selection[1].baseObject
	
	-- detect Edge or Border selection
	local vertSelection = undefined
	
	if ( subobjectlevel == 1 ) then
	(	
		vertSelection = true
	)
	
	if ( subobjectlevel == 2 or subobjectlevel == 3 ) then
	(	
		vertSelection = false
	)

	
	edgeList = #()
	vertList = #()
	
	if ( vertSelection == false ) then 
	(
		edgeList = polyop.getEdgeSelection selection[1]	
		vertList = (polyop.getVertsUsingEdge selection[1] edgeList) as array
	)
	else
	(
		vertList = (selection[1].GetSelection #Vertex ) as array
	)
	
	
	if (vertList.count < 1) do 
	(
		MessageBox "Nothing selected - you need to select either edges or verts"
		return false
	)
	
	-- Store these vert positions
	SelectedVertData =#()
	for v = 1 to vertList.count do
	(
		VertData =#()
		append VertData vertList[v]
		append VertData (polyop.getvert $ vertList[v])
		append SelectedVertData VertData
	)	
	
	-- Append this vert data to the main store
	for vd = 1 to SelectedVertData.count do
	(
		append storedVertPositions  SelectedVertData[vd]
	)	
	
)	




fn RestoreVertexPositions selObj  = 
(

	select selObj
	
	max modify mode
	modPanel.setCurrentObject selection[1].baseObject
	
	 subobjectlevel == 1 
	

	-- Loop through the stored verts
	for svd = 1 to storedVertPositions.count do
	(
		-- Read the stored data
		theVertnumber = storedVertPositions[svd][1]
		theVertPosition = storedVertPositions[svd][2]
		
		-- Set the vert back to the original position
		polyop.setvert $  theVertnumber theVertPosition 
	)	
	
)	








-------------------------------------------------------------------------------------------------------------------------
rollout vprt "Vertex postion reset tool" width:200 height:100
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Reset_Vert_Positions" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)


	button btnStoreVertPositions "Store Vert Positions" width:170 height:25
	button btnRestoreVertPositions "Restore Vert Positions" width:170 height:25
	--button btnResetStore "Skin brush" width:170 height:25
	
	on btnStoreVertPositions pressed do
	(
		theSelection = getCurrentSelection() 
		selObj = theSelection[1]
		storeVertexPositions selObj
	)

	on btnRestoreVertPositions pressed do
	(
		theSelection = getCurrentSelection() 
		selObj = theSelection[1]
		restoreVertexPositions selObj
	)
)



createDialog vprt






