Filein "pipeline/helpers/terrain/terrain_funcs.ms"

containerList = #(	
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_island_ne_placement.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_island_nw_placement.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_island_se_placement.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_island_sw_placement.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_airstrip_props.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_beach_props.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_dock_props.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_island_placement.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_island_props.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_mansion_props.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Terrain\mph4_Terrain_props_01.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Terrain\mph4_Terrain_props_02.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Terrain\mph4_Terrain_props_03.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Terrain\mph4_Terrain_props_04.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Terrain\mph4_Terrain_props_05.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Terrain\mph4_Terrain_props_06.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Terrain\mph4_Terrain_01.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Terrain\mph4_Terrain_02.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Terrain\mph4_Terrain_03.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Terrain\mph4_Terrain_04.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Terrain\mph4_Terrain_05.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Terrain\mph4_Terrain_06.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_airstrip.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_beach.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_dock.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_Island.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_mansion.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_mansion_B.maxc",
@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_wtowers.maxc")

-- containerList = #(@"X:\gta5_dlc\mpPacks\mpHeist4\Art\ng\Models\mph4_Island\mph4_dock.maxc")

gRsPerforce.Sync containerList



with undo off
(
	local containerNodes = #()
	setwaitcursor()
	local counter = 1.0
	progressStart "Processing..."
	for item in containerList while (progressUpdate (100 * (counter / containerList.count))) do
	(
		replaceprompt ("Extracting from " + item)
		try
		(
			local cn = containers.CreateInheritedContainer item
			append containerNodes cn
			
			local sucess = false
			try
			(
				success = cn.MergeSource()
			)
			catch
			(
				format "Couldnt merge % \n" item.name
			)
			
			--detach all objets from container
			cn.GetContentNodes false &nodes
			for node in nodes do
			(
				cn.RemoveNodeFromContent node true
			)
			
			ClearSelection()
			
			unhide objects
			max hide inv
			local terrainMeshes = for obj in Geometry where (GetAttrClass obj == "Gta Object") and (not IsRsref obj) and (not RsIsLOD obj) and (RsIsTerrainMesh obj) collect obj
			local ipProps = for obj in Geometry where (GetAttrClass obj == "Gta Object") and (IsRsref obj) collect obj
			
			--filter out only 	
			local vegMatches = #("*_bush_*", "*_palm_*", "*_plant_*", "*_tree_*", "*_weed_*")
			local vegProps = #()
			for item in ipProps do
			(
				local gotMatch = false
				for match in vegMatches do
				(
					if(matchPattern item.name pattern:match) do
					(
						gotMatch = true
						append vegProps item
					)
				)
			)
			
			--convert rsrefs to meshes
			for item in vegProps do
			(
				convertToMesh item
			)
			
			local allNodes = join terrainMeshes vegProps
			
			for item in allNodes do item.isHidden = false
			saveNodes allNodes ("X:/gta5_dlc/mpPacks/mpHeist4/Art/working/" + cn.name + "_TerrainMeshes.max")
		)
		catch
		(
			format "problem inheriting % \n" item 
		)
		
		resetMaxFile #noPrompt
	)
	
	progressEnd()
	setarrowcursor()
	

)