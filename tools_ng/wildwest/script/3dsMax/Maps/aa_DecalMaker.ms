-- Decal Maker
-- Rockstar Leeds
-- 06/05/2011
-- by Abhishek Agrawal

/* Functionality */
-- Clones selected polys and makes a decal with the alpha borders. 
-- Also resets vert color/alpha info so you dont get any weird behaviour. 
-- UVmaps @ 6x6, relaxes the UV's
-- Push @ 0.01 and Smoothing Group 1
-- Works on multiple editable poly objects with selected polys
-- Uniue name for every decal made... _Decal ##

/* Revision 1 (08/09/2011) */
-- Randomised Wire Color
-- Adds the new decal into current layer

/* Revision 2 (13/11/2013) */
--Kevin Ala-Pantti
-- Wrapped in a rollout
-- Added ability to adjust push amount
-- Added ability to choose between planar mapping and view aligned mapping
-- Added spinners for choosing map channel, mapping height and width

try(destroyDialog RsTa_DecalMaker)catch()

/*
	aligns uvmap gizmo to viewport
	the gizmo comes rotated 180, so flip u and v to get it right side up again
*/
fn RsTa_viewAlign obj=
(
	select obj
	
	local viewTM = viewport.getTM()
	local uvmod = obj.modifiers[#UVW_Map]
	uvmod.gizmo.rotation = viewTM.rotation
	uvmod.uflip = true
	uvmod.vflip = true
)

/*
	makes a new decal from the currently selected faces of an editable poly
*/
fn RsTa_makeDecal mappingType pushAmt mapChannel mapHeight mapWidth=
(
	selset = getcurrentselection()
	for n = 1 to selset.count do
	(
		if (classof selset[n] == editable_Poly) then
		(
			thisobj = selset[n]
			if (thisobj.selectedfaces.count != 0) do
			(
				polyop.detachFaces thisobj thisobj.selectedfaces delete:false asnode:True name:"DingDong"
				
				thisdecal = $DingDong
				thisdecal.name = uniqueName "_Decal"
				centerPivot thisdecal

				-- All this to set vert alpha and color properties... 
				edge_selection = #{}
				num_edges = polyop.getNumEdges thisdecal
				for f = 1 to num_edges do
				   edge_selection[f] = true

				polyop.setEdgeSelection thisdecal edge_selection

				-- Select all verts for applying vertex color and alpha values
				polyop.setVertSelection thisdecal thisdecal.verts

				thisdecal.SetVertexColor (color 255 255 255) #VertexColor
				thisdecal.SetVertexColor white #Alpha

				-- Select border edges (needs all edges to be selected to work)
				thisdecal.Selectborder()
				border_selection = polyop.getEdgeSelection thisdecal
				vert_selection = polyop.getVertsUsingEdge thisdecal border_selection 
				polyop.setVertSelection thisdecal vert_selection
				thisdecal.SetVertexColor black #Alpha

				uvmod = UVWMap()
				uvmod.mapChannel = mapChannel
				uvmod.length = mapHeight
				uvmod.width = mapWidth
				addModifier thisdecal uvmod
				if (mappingType == 2) then
				(
					RsTa_viewAlign thisdecal
				)

				unwrapmod = UVWUnwrap()
				unwrapmod.height = mapHeight
				unwrapmod.width = mapWidth
				addModifier thisdecal unwrapmod

				modPanel.setCurrentObject unwrapmod
				unwrapmod.relax
				unwrapmod.setRelaxAmount 1
				unwrapmod.relax2()		

				smoothmod = Smooth()
				smoothmod.smoothingBits = 1
				addModifier thisdecal smoothmod

				p = push()
				p.push_value = pushAmt
				addModifier thisdecal p

				thisdecal.wirecolor = random (color 0 0 0) (color 255 255 255)

				-- Add decal to the currently selected layer
				inputLayer = LayerManager.current 
				inputLayer.addnode thisdecal
			)
		)
		else
		(
			messageBox (selset[n].name + " is not an Editable_Poly or you aren't in subobject mode.")
		)
	)
)

-- **************************************************************
/*
	MAIN ROLLOUT
*/
rollout RsTa_DecalMaker "Decal Maker" width:275
(
	dotNetControl RsBannerPanel "Panel" pos:[3,3] height:32 width:(RsTa_DecalMaker.width-6)
	local bannerStruct = makeRsBanner dn_Panel:rsBannerPanel wiki:"Decal_Maker" doOutline:false filename:(getThisScriptFilename())

	radioButtons rbtnMapType "" labels:#("Planar Mapping", "View Align") align:#center columns:2
	spinner spPushAmt "Push Amount" scale:.001 range:[0, 1, .01] fieldWidth:50 across:2
	spinner spChannel "Map Channel" range:[1, 99, 1] fieldWidth:50 type:#integer
	spinner spHeight "Map Height" range:[0, 100, 6] fieldWidth:50 type:#integer across:2
	spinner spWidth "Map Width" range:[0, 100, 6] fieldWidth:50 type:#integer
	button btnMakeDecals "Make Decals From Selected Faces" width:(RsTa_DecalMaker.width-6) height:30 align:#center

	-- Refresh banner-icons when floater's rollouts are adjusted:
	on RsBannerPanel Paint Sender Args do 
	(
		for n = 0 to (RsBannerPanel.Controls.Count - 1) do 
		(
			RsBannerPanel.Controls.Item[n].Invalidate()
		)
	)
	
	--------------------------------------------------------------
	-- Rollout open/close
	--------------------------------------------------------------
	on RsTa_DecalMaker open do
	(
		-- Initialise tech-art banner:
		bannerStruct.setup()
	)
	
	on btnMakeDecals pressed do
	(
		local mappingType = rbtnMapType.state
		local pushAmt = spPushAmt.value
		
		RsTa_makeDecal mappingType pushAmt spChannel.value spHeight.value spWidth.value
	)
)

createDialog RsTa_DecalMaker