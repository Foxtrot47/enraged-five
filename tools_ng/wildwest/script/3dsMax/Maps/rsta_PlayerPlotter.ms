struct sPlayerPosPlotter
(
	ORIGIN = [3676, -6174],
	image,
	imageRootPath = (RsConfigGetCacheDir()),
	imageFilename = RsConfigGetCacheDir() + "PlayerPlotter.png",
	DEBUGTEXTPATH = @"X:\gta5\build\dev_ng\temp_debug.txt",
	
	fn _CreateImage =
	(
		image = Bitmap 2048 2048
		
		local t = (dotnetclass "System.DateTime").Now.ToString("yyyyMMddTHHmmss")
		image.filename = imageFilename
	),
	
	fn GetDebugPlayerPos =
	(
		local playerPos = #()
		if (DoesFileExist DEBUGTEXTPATH) then
		(
			local fh = openFile DEBUGTEXTPATH mode:"r"
			
			while not eof fh do
			(
				local txt = readline fh
				if(matchPattern txt pattern:"Player Position =*") do
				(					
					local pieces = filterstring txt "<<>>"
					local coords = filterString pieces[2] ","
					append playerPos [(coords[1] as float), (coords[2] as float)]
				)
			)
		)
		
		--return the last position saved in the text file as that should be latest
		return playerPos[playerPos.count]
	),
	
	fn PlotImagePosition pos =
	(
		_CreateImage()
		
		--check bounds
		if (pos.x < ORIGIN.x) or (pos.x > ORIGIN.x + 2048) or (pos.y < ORIGIN.y) or (pos.y > ORIGIN.y + 2048) then
		(
			return false
		)
		
		local offset = pos - ORIGIN
		local pixelPos = [offset.x, (2047 - offset.y)]
		
		setpixels image pixelPos #((color 255 255 255 255))
		
		return true
	),
	
	fn SavePlot =
	(
		if (image != undefined) then
		(
			try
			(
				save image
			)
			catch
			(
				return false
			)
			
			return true
		)
		else
		(
			return false
		)
	)
)

try(DestroyDialog RSTA_PlayerPosPlotterUI) catch()
rollout RSTA_PlayerPosPlotterUI "Player Plotter" width:300
(
	local plotter = sPlayerPosPlotter()
	
	button btnPlotPosition "Plot Position" tooltip:"Run game and hit 'c' to export player position" width:280
	button btnDisplayPlot "Display" width:280 enabled:false
	button btnOpenFolder "Show Folder" width:280
	
	on btnPlotPosition pressed do
	(
		local playerPos = plotter.GetDebugPlayerPos()
		if(playerPos != undefined) then
		(
			local success = plotter.PlotImagePosition playerPos
			if not success then
			(
				messagebox "Position out of bounds"
				return false
			)
			else
			(
				if not (plotter.SavePlot()) then
				(
					messagebox "Problem saving image"
					return false
				)
				else
				(
					btnDisplayPlot.enabled = true
				)
			)
		)
		else
		(
			messagebox "Did you run the game yet and hit 'c' to export the player position?" title:"Missing Player Data"
		)
	)
	
	on btnDisplayPlot pressed do
	(
		
			--get the latest playerplotter image
			local plotterFiles = for item in (getFiles (RsConfigGetCacheDir() + "*.png")) where (matchpattern (getfilenamefile item) pattern:"PlayerPlotter_*") collect item
			if(plotterFiles.count > 0) then
			(
				local latest = (dotnetobject "System.IO.FileInfo" plotterFiles[1]).LastAccessTime
				local latestFile = plotterFiles[1]
				if(plotterFiles.count > 1) then
				(
					for i=2 to plotterFiles.Count do
					(
						local thisDateStamp = (dotnetObject "System.IO.FileInfo" plotterFiles[i]).LastAccessTime
						if ((thisDateStamp.CompareTo latest) > 0) then
						(
							latest = thisDateStamp
							latestFile = plotterFiles[i]
						)
					)
				)
				print latestFile
				local bm = openBitmap latestFile
				display bm
			)
		
	)
	
	on btnOpenFolder pressed do
	(
		shellLaunch "explorer.exe" (substitutestring (RsConfigGetCacheDir()) "/" "\\")
	)
)

CreateDialog RSTA_PlayerPosPlotterUI

