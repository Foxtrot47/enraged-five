--
-- File:: pipeline/ui/containers.ms
-- Description:: Container helper rollout utility.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 15 January 2009
--

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
try (destroyDialog RsContainersRoll) catch ()

filein "pipeline/export/maps/maptest.ms"

rollout RsContainersRoll "Container Toolkit" width:220
(
	dotNetControl RsBannerPanel "Panel" height:32 width:(RsContainersRoll.Width) pos:[0,0]
	local Banner = makeRsBanner versionNum:1.11 versionName:"Fulsome Paint" dn_Panel:rsBannerPanel wiki:"Containers#Containers"

	local btnWidthA = 140
	local btnWidthB = 92
	
	group "Container Utilities"
	(
		checkButton btnCreateFromFiles "Inherit Multiple Containers" width:btnWidthA
			tooltip:"Select Container files to be loaded"
		checkButton btnReInherit "Re-inherit selection" width:btnWidthA offset:[0,-3]
			tooltip:"Re-inherit files for selected Container helpers"
		checkButton btnDeleteLocks "Delete lock-files" width:btnWidthA offset:[-1,-3]
			tooltip:"Delete .lock files for selected Container helpers"
		button btnCreateAltConts "Alt-Container Creator" width:btnWidthA offset:[-1,-3]
			tooltip:"Creates new alternative containers based on selected containers - a new suffix will be added to the base map-names"
	)
	
	group "Perforce Commands: (for selected)"
	(
		checkButton btnGetLatest "Get latest" width:btnWidthA offset:[-1,0]
			tooltip:"Get latest MAXC files from Perforce for selected Container helpers"
		checkButton btnCheckout "Check out files" width:btnWidthA offset:[0,-3]
			tooltip:"Check out files from Perforce for selected Container helpers"
		checkButton btnCheckin "Check in files" width:btnWidthA offset:[0,-3]
			tooltip:"Check files into Perforce for selected Container helpers"
	)
	
	group "Scene Utilities"
	(
		button btnCheckForDupeObjects "Find Duplicate Objects"
	)

	group "Container Commands"
	(	
		edittext edtContainerName "Name:"
		button btnCreateFromSel "Create from Selection" width:btnWidthA height:16
			tooltip:"Create a new container from the selected objects"
		button btnCreateFromSelUnique "Create from Selection / Unique Materials" height:16
			tooltip:"Create a new container from the selected objects and create unique materials"
		button btnOpenSel		"Open Selection" 		width:btnWidthB height:16 offset:[2,-3]	across:2
			tooltip:"Sets selected containers to \"Open\", if available"
		button btnCloseSel		"Close Selection" 		width:btnWidthB height:16 offset:[-2,-3]
			tooltip:"Sets selected containers to \"Closed\", if available"
		button btnLoadSel		"Load Selection" 		width:btnWidthB height:16 offset:[1,-3]	across:2
			tooltip:"Runs \"Load\" command for selected containers"
		button btnUnloadSel	"Unload Selection"	width:btnWidthB height:16 offset:[-2,-3]
			tooltip:"Runs \"Unload\" command for selected containers"
		button btnSaveSel		"Save Selection" 		width:btnWidthB height:16 offset:[2,-3]	across:2
			tooltip:"Runs \"Save\" command for selected containers"
		button btnReloadSel	"Reload Selection" 	width:btnWidthB height:16 offset:[-2,-3]
			tooltip:"Runs \"Reload\" command for selected containers"

		label orUseLbl "...or use Max's \"Containers\" toolbar:\n     Rightclick main toolbar for option\n            (these will work better)" align:#left height:44 offset:[4,0]
	)
	
	---------------------------------------------------------------------------
	-- Functions
	---------------------------------------------------------------------------

	--
	-- Rename material
	--
	fn RenameMaterial containername materialcopy material = (
		materialcopy.name = (containername + "_" + material.name)
	)

	--
	-- Build container using a selection of objects, copy their materials and uniquely name them based on the container name
	--
	fn BuildContainer containername = (

		if containername == "" then (
			messagebox "Enter a container name."
			return false
		)

		if $selection.count > 0 then(
			for node in $selection do (
				if node.material != undefined then (
					if classof node.material == Multimaterial then (
						mat = copy node.material
						RenameMaterial containername mat node.material
						for index=1 to mat.numsubs do (
							if mat[index] != undefined then (
								RenameMaterial containername mat[index] node.material[index]
							)
						)
						node.material = mat
					)
					else (
						mat = copy node.material
						RenameMaterial containername mat node.material
						node.material = mat
					)
				)
			)

			c = containers.createcontainer($selection)
			c.name = containername
		)
	)

	---------------------------------------------------------------------------
	-- Event Handlers
	---------------------------------------------------------------------------

	--
	-- event: btnCreateFromSel pressed
	-- desc: Create a container for the currently selected objects.
	--
	on btnCreateFromSel pressed do
	(
		cont = ( Containers.CreateContainer selection )
		if ( undefined != cont ) then
			select cont
	)

	on btnCreateFromSelUnique pressed do
	(
		BuildContainer edtContainerName.text
	)
	
	--
	-- event: btnCreateFromFiles pressed
	-- desc: Create an inherited containers from MAXC files.
	--
	on btnCreateFromFiles changed state do 
	(
		RsContFuncs.inheritBrowsedFiles()
		
		btnCreateFromFiles.checked = false
	)
	
	-- Replaces selected container-helpers with new ones created from their definition-files
	on btnReInherit changed state do 
	(
		local newConts = RsContFuncs.reInherit selection
		selectMore newConts
		
		btnReInherit.checked = false
	)
	
	on btnDeleteLocks changed state do 
	(
		RsContFuncs.deleteLocks selection
		
		btnDeleteLocks.checked = false
	)
	
	on btnGetLatest changed state do 
	(
		gRsP4Container.getLatestFor $
		btnGetLatest.checked = false
	)
					
	on btnCheckout changed state do 
	(
		gRsP4Container.checkoutFor $
		btnCheckout.checked = false
	)
					
	on btnCheckin changed state do 
	(
		gRsP4Container.checkinFor $
		btnCheckin.checked = false
	)
	
	--
	-- event: btnOpenSel pressed
	-- desc: Open selected containers.
	--
	on btnOpenSel pressed do
	(
		for o in selection do
		(
			if ( not Container == classof o ) then
				continue
			if ( o.unloaded ) then
				o.LoadContainer()
			if ( not o.IsOpen() ) then
				o.setOpen true
		)	
	)
	
	--
	-- event: btnCloseSel pressed
	-- desc: Close selected containers.
	--
	on btnCloseSel pressed do
	(
		for o in selection do
		(
			if ( not Container == classof o ) then
				continue
			if ( o.IsOpen() ) then
				o.setOpen false
		)	
	)	
	
	--
	-- event: btnCreateFromSel pressed
	-- desc: 
	--
	on btnLoadSel pressed do
	(
		for o in selection do
		(
			if ( not Container == classof o ) then
				continue
			if ( o.unloaded ) then
				o.LoadContainer()
		)
	)
	
	--
	-- event: btnCreateFromSel pressed
	-- desc: 
	--
	on btnUnloadSel pressed do
	(
		for o in selection do
		(
			if ( not Container == classof o ) then
				continue
			if ( not o.unloaded ) then
				o.UnloadContainer()
		)
	)
	
	--
	-- event: btnCreateFromSel pressed
	-- desc: Create a container for the currently selected objects.
	--
	on btnSaveSel pressed do
	(
		for o in selection do
		(
			if ( not Container == classof o ) then
				continue
			o.SaveContainer false
		)
	)
	
	--
	-- event: btnCreateFromSel pressed
	-- desc: Create a container for the currently selected objects.
	--
	on btnReloadSel pressed do
	(
		for o in selection do
		(
			if ( not Container == classof o ) then
				continue
			o.ReloadContainer()
		)
	)
	
	on btnCheckForDupeObjects pressed do
	(
		gRsULog.Init "Geom repeats" appendToFile:false
		RsMapExportCancelled = false
		RsMapCheckGeometryRepeats $objects
		gRsULog.Validate()
	)
	
	on btnCreateAltConts pressed do 
	(
		filein (::RsConfigGetWildWestDir() + "script/3dsMax/Maps/Containers/CreateAltConts.ms")
	)
	
	on RsContainersRoll open do 
	(
		Banner.Setup()
	)
)

createDialog RsContainersRoll
