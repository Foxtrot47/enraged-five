-----------------------------------------------------------------------------------------------
-- Create Alternative Containers tool:
--	Tool to create new containers, with names/helpers based on existing ones
--	By Neal D Corbett (R* Leeds) 11/06/2014
-----------------------------------------------------------------------------------------------

try (destroyDialog RsCreateAltContsTool) catch ()
Callbacks.RemoveScripts id:#RsCreateAltConts

-- Tool to create new containers based on selected containers:
rollout RsCreateAltContsTool "Alt-Container Creator" Width:380
(
	dotNetControl RsBannerPanel "Panel" height:32 width:(RsCreateAltContsTool.Width) pos:[0,0]
	local Banner = makeRsBanner versionNum:1.02 versionName:"Inexpensive Twist" dn_Panel:rsBannerPanel wiki:"Alt-Container Creator"
	
	EditText TxtRemSuffix "Remove Suffixes:" Text:"_;_props;_placement;_vegetation" FieldWidth:200 Align:#Right Offset:[3,0]
		Tooltip:"Semicolon-separated list of suffixes that will be removed to create base-names for new containers"
	EditText TxtSuffix "Add Suffix:" Text:"_example" FieldWidth:200 Align:#Right Offset:[3,0]
		Tooltip:"This suffix will be added to original containers' name (with suffixes removed) when naming new containers"
	
	ListBox LstSelConts "" ReadOnly:True Width:364 Align:#Center
	Button BtnCreateConts "Create from Selected Containers" Height:30
	
	-- Generate create-list, and update controls, to match selection:
	fn UpdateList Suffix:TxtSuffix.Text = 
	(
		TxtSuffix.Text = (TrimRight TxtSuffix.Text)
		
		if (TxtSuffix.Text == "") do 
		(
			BtnCreateConts.Enabled = False
			LstSelConts.Items = #("[No suffix supplied]")
			return #()
		)
		
		SetWaitCursor()
		
		-- Containers to be examined:
		local SelConts = (for Obj in GetCurrentSelection() where (isKindOf Obj Container) collect Obj)
		
		-- Suffix-patterns to be removed:
		local RemSuffixes = for Item in (FilterString TxtRemSuffix.Text ";") collect ("*" + Item)
		
		struct NewContData (OrigCont, NewName, NewPath, ListText, Valid = False)
		
		local BaseNames = #()
		
		local CreateContsList = for OrigCont in SelConts collect 
		(
			local ContData = NewContData OrigCont:OrigCont
			
			local ListText = (StringStream "")
			format "'%' " OrigCont.Name To:ListText
			
			-- Remove suffix from container-name, if found:
			local BaseName = (OrigCont.Name)
			local Suffixed = False
			for ThisSuffix in RemSuffixes while (not Suffixed) do 
			(
				Suffixed = (MatchPattern BaseName Pattern:ThisSuffix)
				
				if Suffixed do 
				(
					BaseName = SubString BaseName 1 (BaseName.Count - ThisSuffix.Count + 1)
				)
			)
			
			-- Generate new container-name:
			ContData.NewName = (BaseName + Suffix)
			
			-- Generate new container's filename:
			local OrigFilename = (RsContFuncs.GetContFilename OrigCont)
			ContData.NewPath = (RsMakeSafeSlashes ((GetFilenamePath OrigFilename) + ContData.NewName + ".maxc"))

			format " => '%' " ContData.NewName To:ListText
			
			-- Don't allow container-create if object or filename already exists:
			case of 
			(
				(not AppendIfUnique BaseNames BaseName):
				(
					format "[IGNORED: Already listed]" To:ListText
				)
				(IsValidNode (GetNodeByName ContData.NewName)):
				(
					format "[IGNORED: Exists in scene]" To:ListText
				)
				(DoesFileExist ContData.NewPath):
				(
					format "[IGNORED: File exists]" To:ListText
				)
				Default:
				(
					--format "[CAN CREATE]" To:ListText
					ContData.Valid = True
				)
			)
			
			ContData.ListText = (ListText as String)
			
			ContData
		)
		
		if (CreateContsList.Count == 0) then 
		(
			BtnCreateConts.Enabled = False
			LstSelConts.Items = #("[No containers selected]")
		)
		else 
		(
			BtnCreateConts.Enabled = True
			LstSelConts.Items = for Item in CreateContsList collect Item.ListText
		)
		
		SetArrowCursor()
		
		return CreateContsList
	)
	
	fn CreateAltConts Scale:0.75 Offset:[0,0,45] SetEditable:True = 
	(
		local AddFiles = #()
		
		for Item in (UpdateList()) where Item.Valid do 
		(
			-- Container queries user if saving a new file; create a dummy file to get around this:
			Close (CreateFile Item.NewPath)

			-- Create new container, and set it up 
			local NewContObj = Container Name:Item.NewName \
				DisplayLabel:true \
				Size:(Scale * Item.OrigCont.Size) \ -- Scale new node, compared to the original node
				Wirecolor:(White - Item.OrigCont.Wirecolor) \ -- Invert the original node's colour, to make it different
				Pos:(Item.OrigCont.Pos + Offset)\ -- Offset new node from original node
				LocalDefinitionFilename:Item.NewPath
			NewContObj.SetAccessType #OnlyEditInPlace
			
			append AddFiles Item.NewPath
		)
		
		if (AddFiles.Count == 0) then 
		(
			MessageBox "No new containers were generated" Title:""
		)
		else 
		(
			if (QueryBox ((AddFiles.Count as string) + " containers generated.\nAdd new files to Perforce?") Title:"Query: Add to Perforce?") do 
			(
				-- Add new container-files to perforce:
				gRsPerforce.addToChangelistByName ("New containers: '" + TxtSuffix.Text + "'") AddFiles
				
				MessageBox "Done!" Title:""
			)
		)
		
		UpdateList()
	)
	
	on TxtPrefix Entered Val do 
	(
		UpdateList()
	)
	on TxtSuffix Entered Val do 
	(
		UpdateList()
	)
	
	on BtnCreateConts pressed do 
	(
		CreateAltConts()
	)
	
	on RsCreateAltContsTool Open do 
	(
		Banner.Setup()
		UpdateList()
		Callbacks.Addscript #SelectionSetChanged "RsCreateAltContsTool.UpdateList()" id:#RsCreateAltConts
	)
	
	on RsCreateAltContsTool Close do 
	(
		Callbacks.RemoveScripts id:#RsCreateAltConts
	)
)

CreateDialog RsCreateAltContsTool