
try (DestroyDialog RsMemResZonesRoll) catch ()

filein (RsConfigGetWildwestDir() + "Script/3dsMax/Maps/MemResidentZones.ms")

rollout RsMemResZonesRoll "Memory Resident Zones Toolkit" width:300
(
	dotNetControl RsBannerPanel "Panel" Pos:[0,0] Height:32 width:RsMemResZonesRoll.Width
	local BannerStruct = MakeRsBanner dn_Panel:RsBannerPanel VersionNum:1.03 VersionName:"Memorable Flute"
	
	button BtnGenAreaZones "Update Area-Zone Metafiles" Tooltip:"Reads game's prop-positions from map-scenexmls, and regenerates Memory Resident Model lists for each map-area" Width:290 Height:24
		
	spinner SpnPropLimit "Maximum Model-Count per Area-Zone:" FieldWidth:40 Range:[1,1000,gRsMemResidentLists.MaxPropCount] Type:#Integer Tooltip:"A prop won't be made resident in an area if it has fewer instances than this"
		
	button BtnCreateZoneBoxes "Create Boxes from Zones" Tooltip:"Load Memory Resident metafiles, and creates box-objects showing their bounding-boxes" Height:24 Align:#Left Offset:[-8,0] across:2
	radiobuttons RdoZoneBoxes "" labels:#("Areas", "Maps", "All") columns:3  default:3 Offset:[14,5] \
		tooltip:"Create boxes from metafiles for AreaZones, Map zones, or for All zones"
	
	radiobuttons rdoUseDlc "Project" labels:#("Core", "DLC") default:(if RsIsDLCProj() then 2 else 1) enabled:(RsIsDLCProj()) align:#Left across:2 \
		tooltip:"Load/Save core-project data, or for active DLC project"
	
	button btnDeleteBoxes "Delete all Boxes" tooltip:"Delete all Box nodes from the scene" align:#Right offset:[0,4]
	
	fn IsDLC = 
	(
		rdoUseDlc.state == 2
	)
	
	on BtnGenAreaZones pressed do 
	(
		gRsMemResidentLists.MaxPropCount = SpnPropLimit.Value
		gRsMemResidentLists.UpdateAreaZones()
		
		-- Update ComboLodder tool if it's open:
		if (::RsLodCombinerTool != undefined) and (RsLodCombinerTool.Open) do 
		(
			RsLodCombinerTool.RefreshTreeView()
		)
	)
	
	on BtnCreateZoneBoxes pressed do 
	(
		local metafilePath = gRsMemResidentLists.GetMetafilePath dlc:(IsDLC())
		local filePattern = (metafilePath + "*.meta")
		
		-- Sync latest metafiles:
		gRsPerforce.SyncChanged #(filePattern) clobberAsk:False silent:True
		
		local loadPaths = (GetFiles filePattern)
		local areaPattern = (gRsMemResidentLists.areaFilePrefix + "*")
		
		case RdoZoneBoxes.State of 
		(
			1:
			(
				-- AreaZones:
				LoadPaths = for ThisPath in LoadPaths where (MatchPattern (GetFilenameFile ThisPath) Pattern:AreaPattern) collect ThisPath
			)
			2:
			(
				-- Non-AreaZones:
				LoadPaths = for ThisPath in LoadPaths where (not MatchPattern (GetFilenameFile ThisPath) Pattern:AreaPattern) collect ThisPath
			)
		)
		
		if (loadPaths.count == 0) do 
		(
			local pathStr = (RsWordWrap metafilePath 20 delimiters:#("/"))
			print pathStr
				
				
			local msg = "No matching zone-files were found in:\n\n"
				
			local strWidth = 60
			for n = 1 to metafilePath.count by strWidth do 
				Append msg ((Substring metafilePath n strWidth) + "\n")
			
			MessageBox msg title:"Warning: No files found"
			
			return OK
		)
		
		-- Import metafiles, create boxes for each
		undo "create Zone boxes" on 
		(
			local ZoneBoxes = for Filename in LoadPaths collect 
			(
				local AreaZone = (RsMemRes_Zone.Import Filename Models:False)
				AreaZone.CreateBoxObj()
			)
			
			Select ZoneBoxes
		)
		
		return OK
	)
	
	on btnDeleteBoxes pressed do undo "delete Boxes" on 
	(
		local boxObjs = for obj in Geometry where IsKindOf obj Box collect obj
		Delete boxObjs
	)
	
	on RsMemResZonesRoll open do 
	(
		-- Initialise tech-art banner:
		BannerStruct.Setup()
	)
)

createDialog RsMemResZonesRoll