try(DestroyDialog SunkenOcclusionBoxUI)catch()

rollout SunkenOcclusionBoxUI "Create Sunken Occlusion Box"
(
	--UI
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:SunkenOcclusionBoxUI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"SunkenOcclusionCreator" filename:(getThisScriptFilename())
	
	group ""
	(
		spinner spnNegZExtent "Negative Z: " range:[1, 100, 10]
		button btnCreate "Create" width:210
	)
	
	--FUNctions
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	fn convertToOcclusion o =
	(			
		if ( Box == classof o ) then
		(
			occlBox = RsOcclusionBox()
			
			occlBox.Box2.Length	= o.Length
			occlBox.Box2.Width = o.Width
			occlBox.Box2.Height	= o.Height
			occlBox.Box2.LengthSegments = o.LengthSegments
			occlBox.Box2.WidthSegments = o.WidthSegments
			occlBox.Box2.HeightSegments = o.HeightSegments
			occlBox.Box2.GenerateUVs = false
			occlBox.Box2.Front = true
			occlBox.Box2.Top = true
			occlBox.Box2.Left = true
			occlBox.Box2.Back = true
			occlBox.Box2.Bottom = true
			occlBox.Box2.Right = true
			occlBox.transform = o.transform
			occlBox.name = o.name
			
			occlBox
		)
		else
		(
			format "Couldn't copy parameters from %:% to %:% as they aren't both the required type (box2Geometry and RsPopZone or \
					RsMapZone)\n" box2Obj (classof box2Obj) newZoneObj (classof newZoneObj)
		)
	)
	
	--//////////////////////////////////////////////////////
	--Events dear boy
	--//////////////////////////////////////////////////////
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	on SunkenOcclusionBoxUI open do
	(
		banner.setup()
	)
	
	--//////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////
	on btnCreate pressed do
	(
		local sel = for obj in $selection where superClassOf obj == GeometryClass collect obj
		if sel.count == 0  then return false
		local zPad = spnNegZExtent.value
		local geo = RsCreateCollisionGeometry objs:sel collClass:#box padding:[0.0, 0.0, zPad] createAsGeometry:true \
		createPerElement:false longCyl:true rotZ:true rotYZ:false wideRamp:false longRamp:false inaccuracy:0.05 minSize:RsMinCollSize showProgress:true
		
		--rename
		geo.name = sel[1].name + "_Occlusion"
		
		geo_bbox = nodegetBoundingBox geo[1] geo[1].transform
		geo_bboxZMag = abs(geo_bbox[2].z - geo_bbox[1].z)
		
		sel_bbox = nodegetBoundingBox sel[1] sel[1].transform
		sel_bboxZMag = abs(sel_bbox[2].z - sel_bbox[1].z)
		move geo [0, 0, -geo_bboxZMag + sel_bboxZMag]
		
		--create occlusion box
		occBox = convertToOcclusion geo[1]
		centerPivot occBox
		
		--delete collisionbox
		delete geo[1]
	)
)

createDialog SunkenOcclusionBoxUI width:230