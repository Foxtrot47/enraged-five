-- Collision mesh reporter
-- Rick Stirling, Rockstar North
-- November 2011

-- Quick and dirty tool to report the details of mesh collision
-- The first version has ZERO error checking.


filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")


colldata = #()

for collObj in selection do append colldata #(collObj.name,(GetCollPolyCount collobj))

qsort colldata sortArraybySubIndex subIndex:2	

debug = newScript()
for entry in colldata do
(
	format "Name: %, Tricount: %\n" entry[1] entry[2]	to:debug
)