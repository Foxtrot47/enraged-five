-- Vert density reporter
-- tool to report the vert density of objects by LOD distance

-- Rick Stirling, Rockstar North July 2012



filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")




struct geoData 
(
	geoObject = undefined,
	geoName = undefined,
	geoPos = undefined,
	geoBBox = undefined,
	geoVolume = undefined,
	geoParent = undefined,
	geoLodDistance = undefined,
	geoShaderCount = undefined,
	geoVertCount = undefined,
	geoTriCount = undefined
)	


-- Get the object 


-- Get the bounding volume 


-- Get the vert count 
fn get_geo_breakdown theGeo = 
(
	-- Create a struct and gather some data on the object 
	-- Object, name and position
	geoCollect = geoData geoObject: theGeo
	geoCollect.geoName = theGeo.name
	geoCollect.geoPos = theGeo.pos
	
	
	-- Size
	bboxsize = theGeo.max - theGeo.min
	geoCollect.geoBBox = bboxsize
	
	
	geoCollect.geoVolume = ((bboxsize.x as integer) * (bboxsize.y as integer) * (bboxsize.z as integer)) 
	
	-- Parse the geo to get the polycount and shader data
	-- Create a virtual copy of the mesh as a trimesh for better stats
	tempdatamesh = snapshotAsMesh theGeo as TriMesh
	
	
	
	-- Vert Count
	theVertCount = getnumverts tempdatamesh
	geoCollect.geoVertCount = theVertCount
	
	-- Polycount
	thePolyCount = getnumfaces tempdatamesh
	geoCollect.geoTriCount = thePolyCount
	
	
	-- shaders
	objMaterials = #()
	uobjMaterials = #()
	
	materialClass = classof theGeo.material
	if (materialClass == Multimaterial) then
	(
		for facecount = 1 to thePolyCount do
		(
			face = getFace tempdatamesh facecount 
			matid = getFaceMatId tempdatamesh facecount 
			append objMaterials matid
			appendifunique uobjMaterials matid
		)	
	)
	
	else 
	(
		append objMaterials "1"
		append uobjMaterials "1"
	)	
	
	shadercount = uobjMaterials.count
	geoCollect.geoShaderCount = shadercount
	
	
	
	-- Attributes
	lodDistance = readAttribute theGeo "LOD distance" false
	geoCollect.geoLodDistance = lodDistance
	
	
	geoCollect -- return the collected data
)
	
	
-- Get the cluster size







-- Function to create a series of volumetric boxes on an input object
-- Create boxes based on the size on the object and the boundingBoxSize variable
fn create_bounding_boxes theGeoObject boundingBoxSize boxpadding  = 
(

	-- We need to know some details about the bounding boxes
	bbmin =  theGeoObject.min
	bbmax =  theGeoObject.max 

	bboxsize = bbmax - bbmin
	
	
	-- Figure out how many boxes we need to create and get the starting position
	startx = bbmin.x
	starty = bbmin.y
	startz = bbmin.z 
	
	finishx =  bbmax.x
	finishy =  bbmax.y
	finishz =  bbmax.z

	bbcountx = ceil (((  finishx) - ( startx)) / boundingBoxSize) + boxpadding
	bbcounty = ceil (((  finishy) - ( starty)) / boundingBoxSize) + boxpadding
	bbcountz = ceil (((  finishz) - ( startz)) / boundingBoxSize) + boxpadding
	
	
	-- Create a 3d position array for all the boxes that need to be created
	bboxPosArray = #()
	for x = 1 to bbcountx do 
	(
		for y = 1 to bbcounty do 
		(
			for z = 1 to bbcountz do 
			(
				boxXpos = (bbmin.x + ((x - 1) * boundingBoxSize) +  boundingBoxSize * 0.5) 
				boxYpos = (bbmin.y + ((y - 1) * boundingBoxSize) +  boundingBoxSize * 0.5) 
				boxZpos = (bbmin.z + ((z - 1) * boundingBoxSize) +  0)  -- Boxes get create with pivot at base
				append bboxPosArray [boxXpos,boxYpos,boxZpos]
			)
		)
	)	

	
	-- Create the boxes 
	-- This can be slow, so turn off as much stuff as possible
	clusterArray = #()
	startTime = timeStamp()
	DisableSceneRedraw()
	with undo off for boxindex = 1 to bboxPosArray.count do
	(				
			tempBox = Box lengthsegs:1 widthsegs:1 heightsegs:1 length:boundingBoxSize width:boundingBoxSize height:boundingBoxSize mapcoords:on pos:(bboxPosArray[boxindex]) isSelected:off
			tempBox.name = "ClusterBox_" + (boxindex as string)
			tempBox.pivot = tempBox.center
			append clusterArray tempBox
	)	
	EnableSceneRedraw()
	CompleteRedraw()
	endTime = timeStamp()
	format "Box Creation time :% seconds\n" ((endTime-startTime )/ 1000)
	
	
	-- Create a dummy object for the boxes
	theClusterDummy = Dummy pos:(theGeoObject.Pos) isSelected:off
	theClusterDummy.name = "CLUSTERBOXES_PARENT_" +  theGeoObject.name
	
	for bbobj in clusterArray do bbobj.parent = theClusterDummy
	
	theClusterDummy -- return name of the parent object
	
)	





-- Function that takes an object and a bunch of bounding boxes and kills any that don't intersect the geo
-- The bounding boxes are discovered as the children of a parent object
fn cull_bounding_boxes theGeoObject bboxarrayParent cullmode = 
(
	externalBoxes=#()
	internalBoxes=#()
	intersectingBoxes=#()

	numboxes =  bboxarrayParent.children.count
	
	startTime = timeStamp()

	--DisableSceneRedraw()
	with undo off for boxIndex = 1 to numboxes do  
	(
		obj = bboxarrayParent.children[boxIndex]
		--print obj.name
		
		intersectTestBox = snapshotAsMesh obj as TriMesh
		intersectTestBaseObj = snapshotAsMesh theGeoObject as TriMesh
		
		intersectTestBoxVerts = intersectTestBox.numVerts
		nVertsAfter = (intersectTestBox-intersectTestBaseObj).numverts
		
		if nVertsAfter == 8 then
		(
			append externalBoxes obj
			--print "External"
		)	
		
		if nVertsAfter > 8 then 
		(
			append intersectingBoxes obj
			--print "Intersect"
		)	
		
		if nVertsAfter == 0  then 
		(
			append internalBoxes obj
			--print "Internal"
		)	
		
	)
	--EnableSceneRedraw()
	
	endTime = timeStamp()
	format "Intersection test time:% seconds\n"  ((endTime - startTime) / 1000.0)


	
		for obj in externalBoxes do obj.wirecolor = color 255 0 0 
		for obj in internalBoxes do obj.wirecolor = color 0 0 255 
		for obj in intersectingBoxes do obj.wirecolor = color 0 255 0
	
	
	if cullmode == true then 
	(
		for obj in externalBoxes do delete obj
		for obj in internalBoxes do delete obj
	)
	
	intersectingBoxes -- Return list of intersecting boxes 
	
)	




fn find_closest_verts theGeoStruct vertdistance = 
(
	theobject = theGeoStruct.geoObject
		
	theVertCount = theGeoStruct.geoVertCount
	thePolyCount = theGeoStruct.geoTriCount
	
	tempdatamesh = snapshotAsMesh theobject as TriMesh
	
	
	-- Build a list of all the verts
	vertlist = #()
	for vertIndex = 1 to theVertCount do 
	(
		theVertPos = getVert tempdatamesh vertIndex 		
		append vertlist theVertPos 
	)	

	
	-- Now search for verts that are close together
	closeverts = #()
	
	for sourceVertIndex = 1 to vertlist.count do 
	(
		foundclosevert= false
		closestDist = 0
		
		theSourceVertPos = vertlist[sourceVertIndex]
		
		
		
		-- Loop through all the verts and check the distance to all the other verts
		for searchVertIndex = sourceVertIndex to vertlist.count do 
		(	
			theSearchVertPos = vertlist[searchVertIndex]
			closestDist = distance theSourceVertPos theSearchVertPos
			
			-- Is this distance under the threshold?
			if closestDist < vertdistance then
			(			
				-- Only mark this as a match if the vert numbers are different!
				if sourceVertIndex != searchVertIndex then appendifunique closeverts sourceVertIndex
				foundclosevert= true
			)
			
			if foundclosevert == true then continue
		)
		
		if foundclosevert == true then continue
	)	
	
	
	
)












try( destroyDialog meshDensityTool )catch()
rollout meshDensityTool "Mesh Density Tool"
(
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:meshDensityTool.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Collision Size Tool" filename:(getThisScriptFilename())

	on meshDensityTool open do banner.setup()

	button btnAnalyseSelected  "Analyse Selected Geo" width:160 height:30 tooltip:"Analyse data on selected geo"

	--spinner spnKBthreshold  "Object KB Threshold" range:[0,48,16] type:#integer width:150 fieldwidth:40 tooltip:"kb threshold for new groups"
	


	
	on btnAnalyseSelected pressed do 
	(	
		meshCollection = #()
		
		-- Make sure the mesh is valid
		for selmeshobj in selection do 
		(	
			if superClassOf selmeshobj == GeometryClass then 
			(	
				collectedData = get_geo_breakdown selmeshobj
				append meshCollection collectedData
			)	
		)	
		
		
		
		debugWindow = newScript()
		for meshEntry in meshCollection do
		(
			format "Name: %, LOD Distance: %, Shader Count: %  \n" meshEntry.geoName  meshEntry.geoLodDistance meshEntry.geoShaderCount to:debugWindow
			format "Volume (m3): %, Vert Count: %, Poly Count: % \n"  meshEntry.geoVolume meshEntry.geoVertCount meshEntry.geoTriCount  to:debugWindow
			format "Vert Density: %, Poly Density: % \n\n"   (formattedprint  ( meshEntry.geoVertCount/meshEntry.geoVolume as float) format:"1.3f") (formattedprint  ( meshEntry.geoTriCount/meshEntry.geoVolume as float)  format:"1.3f")  to:debugWindow
			
			
		)
	)	


	
)

createDialog meshDensityTool 180 300




-- WIP debug stuff
vertBucketSize = 2.0


vertdistance = 1.0



		theGeoObject = $test_building
		 theGeoObject = $simple_test
		 bboxsizearray = #(10,5,1)
		bboxarrayParent = create_bounding_boxes theGeoObject bboxsizearray[1] 0

     	intersectingBoxArray = cull_bounding_boxes theGeoObject bboxarrayParent true --cull mode set to true


-- OK, I've got an initial list of bounding boxes that intersect the geo 
-- do a second pass, creating smaller boxes inside those objects 


newboxarray = #()

for x = 1 to intersectingBoxArray.count do 
(
	theGeoObject = intersectingBoxArray[1]
	boundingBoxSize = bboxsizearray[2]
	boxpadding = 0
	
	bboxarrayParent = create_bounding_boxes theGeoObject boundingBoxSize boxpadding
	for bb in bboxarrayParent.children do append newboxarray bb
	
 	




