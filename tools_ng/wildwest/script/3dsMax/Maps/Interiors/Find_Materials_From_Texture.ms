------------------------------------------------------
-- FIND_MATERIAL_FROM_TEXTURE
-- MATT HARRAD [29/02/12]
-- ROCKSTAR NORTH
-- FINDS ALL THE MATERIALS USING A GIVEN TEXTURE KEYWORD
------------------------------------------------------
try (destroyDialog RsFindTexture) catch()
rollout RsFindTexture "Find Texture" width:200
(
	-- LOCALS  -----------------------------
	local shaderList =#()
	local shaderNameList =#()
	-----------------------------------------------------
	fn findShader obj findTextureString shaderList =
	(	
		if (findTextureString != "") then
		(
			if (classof obj.material) == Multimaterial then
			(
				for listID = 1 to (obj.material.materiallist.count) do
				(
					subMat = obj.material.materiallist[listID] 
					if (classof submat) == rage_shader then
					(
						for varCount = 1 to (RstGetVariableCount subMat) do
						(
							if (RstGetVariableType subMat varCount) == "texmap" then
							(
								texmapString = (RstGetVariable subMat varCount)
								--format "%\n" texmapString
								if (matchPattern texmapString pattern:("*" + findTextureString + "*") matchcase:false) then 
								(
									format "% [%] : %\n" obj.name  listID texmapString
									appendifunique shaderList subMat
								)
								
								-- TEST IF ALPHA 
								if (RstGetIsAlphaShader subMat) do
								(
									local alphaString = (RstGetAlphaTextureName subMat varCount)
									if (alphaString != undefined) do 
									(
										if (matchPattern alphaString pattern:("*" + findTextureString + "*") matchcase:false) then 
										(
											format "% [%] (ALPHA): %\n" obj.name listID alphaString
											appendifunique shaderList subMat
										)
									)
								)
								
							)
						)
					)
				)
			)
		)
	)
	
	-- USER INTERFACE  -----------------------------
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:RsFindTexture.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel filename:(getThisScriptFilename()) wiki:""
	
	group "Texture Search:"
	(
		edittext edt_textureName "" width:180 offset:[-5,0]
		button btn_search "Search" width:180
	)		
	group "Results:"
	(
		label lb_text "Clicking Below Sets Shader to Slot 1"
		listbox lbx_shaderList "" height:20
	)
	-- EVENTS  -----------------------------
	on lbx_shaderList selected nameIndex do
	(
		medit.PutMtlToMtlEditor shaderList[nameIndex] 1
	)
	
	on btn_search pressed do
	(
		shaderList =#()
		shaderNameList =#()
		
		for i in geometry do
		(
			findShader  i edt_textureName.text shaderList
		)
		
		for i in shaderList do append shaderNameList i.name
		
		lbx_shaderList.items = shaderNameList
	)
		on RsFindTexture open do 
	(
		banner.setup() 
	)
)
createDialog RsFindTexture style:#(#style_border,#style_toolwindow,#style_sysmenu) 
	