------------------------------------------------------------------------------------
-- SET_ROOM_ID
-- MATT HARRAD [14/10/11]
-- ROCKSTAR NORTH
-- CONVERTS MESH TO COLLISION (VISEVERSA), ALLOWS YOU TO SET THE ROOM IDS ON MESHES
------------------------------------------------------------------------------------
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

------------------------------------------------------------------------------------
-- STRUCTS
------------------------------------------------------------------------------------
struct setRoomId
(
	currentRoom,
	--------------------------------------------------------------------------------
	-- GET MILO ROOMS
	-- RETURNS A STRING ARRAY WITH THE NAMES OF THE MLO ROOMS IN THE SCENE
	--------------------------------------------------------------------------------
	 fn getMiloRoomList =
	(
		miloRooms = #("")
		
		for h in helpers do
		(			
			v_objectType = classOf h
			if (v_objectType as string)  == "GtaMloRoom" then 	append miloRooms h.name
		)
		
		return miloRooms
	),
	--------------------------------------------------------------------------------
	-- FILTER FOR PICKING MLO ROOM 
	--------------------------------------------------------------------------------
	fn miloRoomFilter obj = (

		(classof obj == GtaMloRoom)
	),
	------------------------------------------------------------------------------
	-- SET ROOM ID ON POLYS
	-- THIS IS FROM THE OLD BOUNDROOM.MS, JUST MADE INTO A FUNCTION
	-- BY GREG SMITH <GREG.SMITH@ROCKSTARNORTH.COM>
	------------------------------------------------------------------------------
	fn setRoomIdOnPoly = 
	(
		-- CHECK WE CAN DO THE SET

		selObj = selection[1]		
		facesellist = getfaceselection selObj		
		if facesellist.count < 1 then 
		(		
			messagebox "no faces selected"
			return 0
		)

		-- CHECK THAT WE ARE USING A MULTIMATERIAL	
		if classof selObj.material != MultiMaterial then 
		(		
			newmat = MultiMaterial()					
			setMat = undefined			
			if classof selObj.material == RexBoundMtl then 
			(			
				setMat = selObj.material
			) 
			else 
			(			
				setMat = RexBoundMtl()
			)
					
			newmat.materiallist.count = 1
			newmat.materiallist[1] = selObj.material			
			numfaces = getnumfaces selobj
			
			for i = 1 to numfaces do 
			(			
				setfacematid selobj i 1
			)
			
			selObj.material = newmat
		)
			
		-- GO THROUGH EVERY SELECTED FACE WORKING OUT WHAT MATERIALS ARE USED
		
		usedMatIDs = #()
		newMatID = #()
		
		for selFace in facesellist do 
		(		
			matID = getfacematid selobj selFace			
			if matID != undefined then 
			(
				idxFound = finditem usedMatIDs matID
				if idxFound == 0 then 
				(
					append usedMatIDs matID					
					matidx = finditem selobj.material.materialIDList matID					
					newmat = undefined					
					if classof selobj.material.materialList[matidx] == RexBoundMtl then 
					(					
						newmat = copy selobj.material.materialList[matidx]
					) 
					else 
					(						
						newmat = RexBoundMtl()
					)					
					RexSetRoomNode newmat currentRoom					
					foundItem = 0					
					for i = 1 to selobj.material.materialList.count do 
					(					
						checkmat = selobj.material.materialList[i]						
						if RexIsCollisionEqual checkmat newmat then 
						(						
							foundItem = i
							exit
						)
					)
					
					if foundItem != 0 then 
					(					
						newID = selobj.material.materialIDList[foundItem]
					) 
					else 
					(					
						selobj.material.materialList.count = selobj.material.materialList.count + 1
						selobj.material.materialList[selobj.material.materialList.count] = newmat
						
						newID = selobj.material.materialIDList[selobj.material.materialList.count]
					)					
					append newMatID newID
					setfacematid selobj selFace newID
				) 
				else 
				(				
					setfacematid selobj selFace newMatID[idxFound]
				)
			)			
		)		
	)
)

--------------------------------------------------------------------------------
-- UI 
--------------------------------------------------------------------------------
try (destroyDialog mh_setIdWin) catch()
rollout mh_setIdWin "Set Room IDs..." width:250 
(
	local sri = setRoomId()
	local miloList = sri.getMiloRoomList()
	-----------------------------------------------
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:mh_setIdWin.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"setRoomIDs" filename:(getThisScriptFilename())
	on mh_setIdWin open do 
	(
		banner.setup() 
		rs_dialogPosition "get" mh_setIdWin
	)
	-----------------------------------------------
	-- BUTTONS ETC
	-----------------------------------------------
	button btnMesh2Col "Mesh To Collision" width:117  offset:[-60,0] tooltip:"Convert Selected Collision Mesh's to Editable Mesh's"
	button btnCol2Mesh "Collision To Mesh" width:117  offset:[62,-26] tooltip:"Convert Selected Editable Meshs  Collision Mesh's"
	-------------------------------------
	Label lbl_room "Room MLO: " width:60 pos:[10,70] 
	dropdownlist dd_roomList items: miloList width:120  pos:[70,67]  
	button btn_pickRoom "Pick" width:50  pos:[195,67] tooltip:"Pick a MLO Room from scene"
	
	button btn_Face "Set To Poly Face's" width:117 pos:[5,95] enabled:false
	button btn_Obj "Set On Sel Object's" width:117  pos:[128,95] enabled:false
	
	group "Show/Hide"
	(	
	checkbox cbx_hideGeo "Geometry" checked:(hideByCategory.geometry) across:2
	checkbox cbx_hideHelpers "Helpers" checked:(hideByCategory.helpers)
	)
	-----------------------------------------------		
	-- EVENT HANDLERS
	-----------------------------------------------
	on dd_roomList selected i do 
	(
		if (getnodebyname dd_roomList.items[i]) == undefined then
		(
			mh_setIdWin.btn_Face.enabled = false
			mh_setIdWin.btn_Obj.enabled = false
		)
		else
		(	
			sri.currentRoom = (getnodebyname dd_roomList.items[i])
			mh_setIdWin.btn_Face.enabled = true
			mh_setIdWin.btn_Obj.enabled = true
		)
	)	
	-----------------------------------------------
	-- BUTTON: PICK
	-----------------------------------------------	
	on btn_pickRoom pressed do 
	(	
		gotObj = pickobject message:"pick a milo room" filter:sri.miloRoomFilter
		
		if gotObj != undefined then 
		(
			
			sri.currentRoom = gotObj
			print  ("currentRoom has been set to [" + sri.currentRoom.name as string + "]")
			itemCount = 1
				
			for i in (mh_setIdWin.dd_roomList.items) do 
			(
				if gotObj.name == i do	
				(
					mh_setIdWin.dd_roomList.selection = itemCount
					mh_setIdWin.btn_Face.enabled = true
					mh_setIdWin.btn_Obj.enabled = true
				)
				itemCount = itemCount + 1
			)
		)
	)	
	-----------------------------------------------
	-- BUTTON: M
	-----------------------------------------------
	on btnCol2Mesh pressed do 
	(
		colMeshes = selection as array
		for colMesh in colMeshes do 
		(
			if classof colMesh == Col_mesh then col2mesh colMesh
		)
		clearSelection()
		select colMeshes
	)	
	-----------------------------------------------
	-- BUTTON: C
	-----------------------------------------------	
	on btnMesh2Col pressed do 
	(
		meshObjs = selection as array
		for meshobj in meshObjs do 
		(
			if classOf meshobj == Editable_Poly then convertToMesh meshobj
			if classOf meshobj == Editable_Mesh then mesh2col meshObj
		)
		clearSelection()
		select meshObjs
	)	
	-----------------------------------------------
	-- BUTTON: SET TO FACE
	-----------------------------------------------	
	on btn_Face pressed do 
	(
		meshObjs = selection as array
		for meshobj in meshObjs do
		(
			if classOf meshobj == Editable_Mesh then sri.setRoomIdOnPoly()
			else messagebox "Only works on Editable Mesh Objects!"
		)
	)
	-----------------------------------------------
	-- BUTTON: SET  ON OBJECTS
	-----------------------------------------------	
	on btn_Obj pressed do 
	(
		isGeoHiden = hideByCategory.geometry
		if (isGeoHiden) do hideByCategory.geometry = false -- NEEDS TO SEE THE GEO
		temp_sel = selection as array
		for i in temp_sel do 
		(
			if classOf i == Editable_Poly then convertToMesh i -- THIS IS JUST IN CASE THERES ANY EDIT POLYS 	
			if classOf i == Col_Mesh then col2mesh i -- CONVERT TO MESH IF COLLISION
			clearSelection()			
			if classOf i == Editable_Mesh then 
			(	
				select i				
				subobjectLevel = 4
				max select all				
				sri.setRoomIdOnPoly()
				subobjectLevel = 0
				mesh2col i
			)
		)
		
		clearSelection()
		select temp_sel
		if (isGeoHiden) do hideByCategory.geometry = true -- HIDE THE GEO IF IT WAS HIDDEN BEFORE
	)	
	-----------------------------------------------
	-- SHOW HIDE GEO, HELPERS
	-----------------------------------------------	
	on cbx_hideGeo changed state do hideByCategory.geometry = state
	on cbx_hidehelpers changed state do hideByCategory.helpers = state
		
	on mh_setIdWin close do rs_dialogPosition "set" mh_setIdWin		
)
CreateDialog mh_setIdWin style:#(#style_border,#style_toolwindow,#style_sysmenu) 

