------------------------------------------------------------------------------------
-- ENTITY SET MANAGER
-- MATT HARRAD
-- 19/04/2013
------------------------------------------------------------------------------------
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

try(destroyDialog entity_set_manager_roll)catch()
rollout entity_set_manager_roll "Entity Set Manager" width:300
(
	--** VARIBLES ********************************************
	local ilTv = dotNetObject "System.Windows.Forms.ImageList"
	
	--** CONTROLS ********************************************
	dotNetControl rsBannerPanel "System.Windows.Forms.Panel"	pos:[0,0] height:32 width:entity_set_manager_roll.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel  wiki:"entity_set_manager" mail:"mattharrad@rockstarnorth.com" filename:(getThisScriptFilename())
	dotNetControl tv "system.windows.forms.treeView" height:400 width:(entity_set_manager_roll.width-10) pos:[5,38]
	
	--** FUNCTIONS ********************************************
	fn menu_picked switch =
	(
		selectedNode = entity_set_manager_roll.tv.SelectedNode
		matchingIntGroup = undefined
		if (selectedNode != undefined) do
		(			
			if (selectedNode.tag == "intGroup") then -- FIND THE GROUP 
			(		
				for intGroup in helpers where (classof intGroup) == InteriorGroupHelper do
				(	
					if intGroup.RS_ObjectGroup.objectGroupName == selectedNode.text do 
					(
						matchingIntGroup = intGroup
						exit
					)
				)
			)
			else
			(
				for intGroup in helpers where (classof intGroup) == InteriorGroupHelper do
				(	
					if intGroup.RS_ObjectGroup.objectGroupName == selectedNode.parent.text do
					(
						matchingIntGroup = intGroup
						exit
					)
				)
			)
		)
		
		case switch of 
		(
			#selectNode : select matchingIntGroup
			#selectobjs : 
			(		
				clearSelection()
				for i in matchingIntGroup.interiorGroupObjects do selectMore i
			)
			
			#add :
			(
				for i in selection do append matchingIntGroup.interiorGroupObjects i
				entity_set_manager_roll.populateTreeView tv
			)
			
			#show: for i in matchingIntGroup.interiorGroupObjects do unhide i
			#hide: for i in matchingIntGroup.interiorGroupObjects do hide i
			#showAll: 
			(
				for intG in  (for intGroup in helpers where (classof intGroup) == InteriorGroupHelper collect intGroup) do
				(
					for i in intG.interiorGroupObjects do unhide i
				)
			)
			#hideAll: 
			(
				for intG in  (for intGroup in helpers where (classof intGroup) == InteriorGroupHelper collect intGroup) do
				(
					for i in intG.interiorGroupObjects do hide i
				)
			)			
			#isolate: 
			(
				for intGroup in helpers where (classof intGroup) == InteriorGroupHelper do
				(	
					if intGroup.RS_ObjectGroup.objectGroupName == selectedNode.text then
					(
						for i in intGroup.interiorGroupObjects do unhide i
					)
					else
					(
						for i in intGroup.interiorGroupObjects do hide i
					)
				)
			)
			
			#refresh : entity_set_manager_roll.populateTreeView tv		
			
			#selectObj : select (getnodebyname (selectedNode.text))
			#removeObj : 
			(
				--print matchingIntGroup.interiorGroupObjects
				index = 0
				for i = 1 to (matchingIntGroup.interiorGroupObjects.count) do
				(
					if (matchingIntGroup.interiorGroupObjects[i].name) == selectedNode.text do index = i
				)
				if index !=0 do
				(
					deleteItem matchingIntGroup.interiorGroupObjects index
					entity_set_manager_roll.populateTreeView tv
				)
			)
			#removeSel :
			(				
				for obj in selection do 
				(
					for i in matchingIntGroup.interiorGroupObjects where i == obj do 
					(
						deleteItem matchingIntGroup.interiorGroupObjects (finditem matchingIntGroup.interiorGroupObjects obj)
					)
				)
				
				entity_set_manager_roll.populateTreeView tv
			)
			#newGroup : 
			(
				newGroup = InteriorGroupHelper()
				entity_set_manager_roll.populateTreeView tv	
				select newGroup
			)
			#findGroupFromObject:
			(
				if selection.count !=0 do
				(
					obj = selection[1]
					itsGroup = undefined
					for intGroup in helpers where (classof intGroup) == InteriorGroupHelper do
					(	
						objectList = intGroup.interiorGroupObjects
						if (findItem objectList obj) != 0 do 
						(
							itsGroup = intGroup
							exit
						)						
					)
					if itsGroup != undefined do
					(
						tempTv = entity_set_manager_roll.tv
 						for i=0 to (tempTv.Nodes.count-1) do
 						(
 							if (tempTv.Nodes.item[i].Text) == itsGroup.RS_ObjectGroup.objectGroupName do
 							(
								tempTv.SelectedNode = tempTv.Nodes.item[i]
								tempTv.focus()
 							)
 						)
					)
				)
			)
		)
	)
	----------------------------------------------------------------------
	fn makeMenu menuType=
	(
		case menuType of
		(
			0 :
			(
				rcMenu intGroupsMenu 
				(	
					menuItem mi_newGroup "Make New Group"
					menuItem mi_findGroupFromObject "Find Group From Object"
					separator sep1										
					menuItem mi_show_all "Show All Groups"
					menuItem mi_hide_all "Hide All Groups"
					separator sep2
					menuItem mi_refresh "Refresh Tree"
					
					-- EVENTS ------------
					on mi_newGroup picked do entity_set_manager_roll.menu_picked #newGroup
					on mi_findGroupFromObject picked do entity_set_manager_roll.menu_picked #findGroupFromObject
					on mi_show_all picked do entity_set_manager_roll.menu_picked #showAll
					on mi_hide_all picked do entity_set_manager_roll.menu_picked #hideAll
					on mi_refresh picked do entity_set_manager_roll.menu_picked #refresh
				)
				popUpMenu intGroupsMenu 
			)
			1 :
			(
				rcMenu intGroupsMenu 
				(	
					menuItem mi_selectNode "Select Group Node"
					menuItem mi_selectobjs "Select Group Objects"
					separator sep1
					menuItem mi_add "Add Selected To Group"
					menuItem mi_remove "Removed Selected From Group"
					separator sep2
					menuItem mi_show "Show Group"
					menuItem mi_hide "Hide Group"
					menuItem mi_isolate "Show This And Hide Others"
					menuItem mi_show_all "Show All Groups"
					menuItem mi_hide_all "Hide All Groups"
					separator sep3
					menuItem mi_refresh "Refresh Tree"

					-- EVENTS -------------------
		 			on mi_selectNode picked do entity_set_manager_roll.menu_picked #selectNode
					on mi_selectobjs picked do entity_set_manager_roll.menu_picked #selectobjs
						
					on mi_add picked do entity_set_manager_roll.menu_picked #add
					on mi_remove picked do entity_set_manager_roll.menu_picked #removeSel
						
					on mi_show picked do entity_set_manager_roll.menu_picked #show
					on mi_hide picked do entity_set_manager_roll.menu_picked #hide
					on mi_isolate picked do entity_set_manager_roll.menu_picked #isolate
					on mi_show_all picked do entity_set_manager_roll.menu_picked #showAll
					on mi_hide_all picked do entity_set_manager_roll.menu_picked #hideAll
						
					on mi_refresh picked do entity_set_manager_roll.menu_picked #refresh
				)			
				popUpMenu intGroupsMenu 
			)
			2 :
			(
				rcMenu intGroupsMenu 
				(	
					menuItem mi_selectObj "Select Object"
					separator sep1
					menuItem mi_removetObj "Remove From Group"

					
					-- EVENTS -------------------
					on mi_selectObj picked do entity_set_manager_roll.menu_picked #selectObj
					on mi_removetObj picked do entity_set_manager_roll.menu_picked #removeObj

				)
				popUpMenu intGroupsMenu 
			)
		)		
			
	)
	------------------------------------------------------------------------
	fn addNewTreeNode itemName parentNode tag imageNum =
	(
		objNode=(dotNetObject "System.Windows.Forms.TreeNode" itemName)
		objNode.imageIndex = objNode.selectedImageIndex= imageNum
		objNode.tag = tag
		parentNode.nodes.add objNode
		--------------------------------------------------------------------
		return objNode
	)
	------------------------------------------------------------------------
	fn group_error intGroup =
	(
		if intGroup.parent != undefined then return 0
		else 
		(
			format "Entity Set Error: % has no parent.\n" intGroup.name
			return 1
		)
	)	
	-----------------------------------------------------------------------
	fn icon_error obj =
	(
		if obj == undefined then return 1 -- INVALID OBJECT
		else
		(
			if obj.parent == undefined then return 1
			else
			(
			case (classof obj) of 
				(
					RSrefObject :  return 3
					default: return 2
				)
			)
		)
	)
	------------------------------------------------------------------------
	fn initTreeView tv =
	(	
		iconDir = theWildWest + "script/3dsMax/UI/entity_set_manager/" 
		iconFileNames = #("icon_milo.bmp", "icon_error.bmp", "icon_mesh.bmp", "icon_rsRef.bmp")
		for i in iconFileNames do
		(
			img = dotNetClass "System.Drawing.Image" --create an image
			ilTv.images.add (img.fromFile (iconDir + i))
		)	
		tv.imageList = ilTv 
	)
	------------------------------------------------------------------------
	fn sort_intGroups in_array =
	(
		name_array = for i in in_array collect i.name
		sort name_array
		out_array = #()
		
		for i in name_array do
		(
			for intG in in_array where intG.name == i do append out_array intG
		)
		
		return out_array		
	)
	------------------------------------------------------------------------
	fn populateTreeView theTv=
	(
		theTv.Nodes.Clear() -- RESET IT
		intGroups = for intGroup in helpers where (classof intGroup) == InteriorGroupHelper collect intGroup
		intGroups  = sort_intGroups intGroups
		print (sort_intGroups intGroups)
		for intGroup in intGroups do
		(		
			objectList = intGroup.interiorGroupObjects
			groupName =  intGroup.RS_ObjectGroup.objectGroupName			
			parentNode = addNewTreeNode groupName theTv "intGroup" (group_error intGroup)			
			for obj in objectList do 
			(
				if obj == undefined then 
				(
					addNewTreeNode "missing object!" parentNode "object" 1
					parentNode.Expand() -- HAS ERROR
				)
				else
				(
					icon_id = (icon_error obj)
					addNewTreeNode obj.name parentNode "object" icon_id
					if icon_id == 1 do parentNode.Expand() -- HAS ERROR
				)
			)
			--parentNode.Expand()
		)
	)
	-- ** EVENTS **********************************
	on tv MouseDown arg do 
	(	
		hitNode = tv.GetNodeAt (dotNetObject "System.Drawing.Point" arg.x arg.y)
		tv.selectedNode = hitNode
		if (hitNode != undefined) then
		(
			if (arg.Button == arg.Button.Right) then -- RIGHT CLICK
			(
				if (hitNode.tag == "intGroup") do entity_set_manager_roll.makeMenu 1
				if (hitNode.tag == "object") do entity_set_manager_roll.makeMenu 2
			)
			else -- LEFT CLICK
			(
				if (hitNode.tag == "intGroup") do
				(				
					if (keyboard.shiftPressed) do entity_set_manager_roll.menu_picked #show
					if (keyboard.controlPressed) do entity_set_manager_roll.menu_picked #hide
				)
			)
		)
		else 
		(
			if (arg.Button == arg.Button.Right) then -- RIGHT CLICK
			(
				entity_set_manager_roll.makeMenu 0
			)
		)
	)
	---------------------------------------------------------------------
	on entity_set_manager_roll open do
	(
		banner.setup()
		rs_dialogPosition "get" entity_set_manager_roll
		populateTreeView tv
		initTreeView tv	
	)
	---------------------------------------------------------------------
	on entity_set_manager_roll close do rs_dialogPosition "set" entity_set_manager_roll
)
createDialog entity_set_manager_roll style:#(#style_border,#style_toolwindow,#style_sysmenu)