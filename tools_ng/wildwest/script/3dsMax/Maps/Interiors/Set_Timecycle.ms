--------------------------------------------------------------------
-- SET_TIMECYLES.MS
-- MATT HARRAD [09/12/11]
-- ROCKSTAR NORTH
-- ALLOWS YOU TO SET THE TIME CYCLES ON ALL YOUR ROOMS AT ONCE 
-- USING A DROP DOWN LIST GENERATED FROM THE TIMECYCLE XML
--------------------------------------------------------------------
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

----------------------------------------------------------------------
setTimeCycleUI -- DECLAIRE UI NAME
struct setTimeCycleHandler
(	
	xmlFile = theProjectRoot + @"build\dev\common\data\timecycle\timecycle_mods_3.xml",
	miloRooms,	
	dropDownItems,
	------------------------------------------------------------
	fn getTimeCycleNames xmlFile =
	(
		timeCycleNamesArray = #() 
		if (gRSperforce.connected()) do gRSperforce.sync xmlFile
		------------------------------------------------------
		xmlStream = xmlStreamHandler xmlFile:xmlFile
		xmlStream.open()	
		nodes = xmlStream.root.Selectnodes("/timecycle_modifier_data/modifier")
		for i = 0 to nodes.count - 1 do append timeCycleNamesArray (nodes.itemof[i].getAttribute "name")
		xmlStream.close()
		------------------------------------------------------	
		return timeCycleNamesArray
	),
	------------------------------------------------------------
	fn getMiloRoomList =
	(
		miloRooms = #()	
		for h in helpers where ((classOf h)  == GtaMloRoom) do append miloRooms h.name
		return miloRooms
	),
	------------------------------------------------------------
	fn timeCycleAttr miloRoom secondary:false tcIndex:0 =
	(	
		miloRoom = getnodebyname miloRoom
		tc_text = "Timecycle"	
		if (secondary) do tc_text = "Secondary Timecycle"
		indexAttr = GetAttrIndex (getAttrClass miloRoom) tc_text
		-----------------------------------------------------------------------
		if tcIndex == 0 then
		(
			attrGet = GetAttr miloRoom indexAttr
			return attrGet
		)
		else
		(
			ddl = execute ("setTimeCycleUI.ddl_" + miloRoom.name)
			timeCycle = ddl.items[tcIndex]
			SetAttr miloRoom indexAttr timeCycle 
		)
	),
	------------------------------------------------------------
	fn openUI =
	(	
		miloRooms = getMiloRoomList()		
		dropDownItems = getTimeCycleNames xmlFile
		append dropDownItems "-------------"
		--------------------------------------
		try(destroyDialog setTimeCycleUI)catch()
		--------------------------------------
		c = "rollout setTimeCycleUI \"Set Room TimeCycles\" width:430\n(\n"
		c += "dotNetControl rsBannerPanel \"System.Windows.Forms.Panel\" pos:[0,0] height:32 width:setTimeCycleUI.width\n"
		c += ("local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:\"setRoomTCs\" filename:\"" + (getThisScriptFilename()) + "\"\n")
		c += "on setTimeCycleUI open do\n(\n banner.setup()\nrs_dialogPosition \"get\" setTimeCycleUI\n)\n"
		c += "on setTimeCycleUI close do\n(\nrs_dialogPosition \"set\" setTimeCycleUI\n)\n"
		for i in miloRooms do 
		(
			offset = 3
			if i != miloRooms[1] then offset = 0
			c += "button btn_" + i + " \"" + i + "\" width:130 offset:[-8," +offset as string+ "] align:#left across:3\n"
			c += "on btn_" + i  + " pressed do select (getnodebyname \"" + i + "\")\n"
			c += "dropdownlist ddl_" + i + " \"\" width:140 offset:[-7," + offset as string + "]\n"
			c += "on ddl_" + i + " selected arg do (stc.timeCycleAttr \"" + i + "\" tcIndex:arg)\n"
			c += "dropdownlist ddl_" + i + "_stc \"\" width:140 offset:[8," + offset as string + "] align:#right\n"
			c += "on ddl_" + i + "_stc selected arg do (stc.timeCycleAttr \"" + i + "\" secondary:true tcIndex:arg)\n"
		)
		c +=")\n"
		--------------------------------------
		execute  c
		createDialog setTimeCycleUI style:#(#style_border,#style_toolwindow,#style_sysmenu)
				
		for i in miloRooms do
		(
			ddl = execute ("setTimeCycleUI.ddl_" + i)
			ddl.items = dropDownItems 
			index = findItem dropDownItems (timeCycleAttr i)
			if index != 0 then ddl.selection = index
			else ddl.selection = dropDownItems.count
			
			ddl = execute ("setTimeCycleUI.ddl_" + i + "_stc")
			ddl.items = dropDownItems 
			index = findItem dropDownItems (timeCycleAttr i secondary:true)
			if index != 0 then ddl.selection = index
			else ddl.selection = dropDownItems.count
		)
	)
)
stc = setTimeCycleHandler()
stc.openUI()	