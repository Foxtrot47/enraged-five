------------------------------------------------------------------------------
-- TIMECYCLE_USAGE_REPORT
-- MATT HARRAD [11/01/12]
-- ROCKSTAR NORTH 
-- GETS INFORMATION ON THE INTERIORS TIME CYCLES AND DISPLAYS THEM IN A TABLE
------------------------------------------------------------------------------

-- FILEIN --------------------------------------
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

-- STRUCTS -------------------------------------
struct miloRoomTc 
(
	miloName=undefined,
	fileName=undefined,
	roomName=undefined,
	timeCycle=undefined
)

-- ROLLOUT -------------------------------------
try(destroyDialog timeCycleReport)catch()
rollout timeCycleReport "Timecycle Report" width:400
(	
	-- LOCALS ------------------------------------
	local tcr_roomArray = #()
 	local ColorClass = DotNetClass "System.Drawing.Color"
	local DataGridFont = dotNetObject "System.Drawing.Font" "Verdana" 7 ((dotNetClass "System.Drawing.FontStyle").regular)
 	local HeaderFont = dotNetObject "System.Drawing.Font" "Verdana" 7 ((dotNetClass "System.Drawing.FontStyle").regular)
 	local DataGridSizeMode= (dotnetclass "system.windows.forms.DatagridviewAutoSizeColumnMode")
	local dgvSortMode = DotNetClass "System.Windows.Forms.DataGridViewColumnSortMode"
 	local ResizeCol = dotnetclass "system.windows.forms.DataGridViewTriState"
	local ContAlign =  dotnetclass "System.Drawing.ContentAlignment"
 	local CellBG = ColorClass.lightgray
 	Local CellFG = ColorClass.black
	Local headerColour = ColorClass.FromArgb 60 60 60
	
	-- FUNCTIONS ---------------------------------
	fn getTimeCycles =
	(
		intSceneFiles = getFiles (theProjectAssetRoot + "export/Levels/gta5/Interiors/*.xml")
		---------------------------------------------------------------
		try (destroyDialog  newXmlWait) catch()
		rollout newXmlWait "Reading XMLs" width:210
		(
			label lab_Name ""
			progressbar progBar color:red width:200 height:20 offset:[-8,0]
			label lab_count "0 of 0" 		
		)
		createDialog newXmlWait style:#(#style_border,#style_toolwindow,#style_sysmenu)
		---------------------------------------------------------------
		if (gRSperforce.connected()) do for i in intSceneFiles do gRSperforce.sync i
		clearlistener()
		-------------------------------------------------
		count = 1
		for xmlFile in intSceneFiles do --while count < 5 do
		(
			tempArray = filterString xmlFile @"\ /" 
			niceName = tempArray[tempArray.count]
			niceName = substring niceName 1 (niceName.count-4)
			---------------------------------------------------------			
			newXmlWait.lab_Name.text = ("Reading: " + niceName)
			newXmlWait.progBar.value=100.*count /intSceneFiles.count
			newXmlWait.lab_count.text = (count as string+ " of " + intSceneFiles.count as string)
			----------------------------------------------------------
			xmlStream = xmlStreamHandler xmlFile:xmlFile
			xmlStream.open()
			------------------------------------------------------
			root = xmlStream.root
			nodes = root.Selectnodes("/scene")
			sceneNode = nodes.itemof[0]
			fileName = sceneNode.getAttribute "filename"
			------------------------------------------------------	
			gtaMilo = root.Selectnodes("objects/object[@class = \"gta_milo\"]")
			miloName = ""
			try (miloName = gtaMilo.itemOf[0].getAttribute "name")catch()
			------------------------------------------------------		
			miloRooms = root.Selectnodes("objects/object[@class = \"gta_milo\"]/children/object[@class=\"gtamloroom\"]")
			for i = 0 to miloRooms.count - 1 do
			(
				miloRoom = miloRooms.itemof[i]
				roomName = miloRoom.getAttribute "name"
				------------------------------------------------------				
				attributeNodes = miloRoom.selectNodes("attributes/attribute[@name=\"Timecycle\"]")
				timeCycle = ""
				try (timeCycle = attributeNodes.itemOf[0].getAttribute "value")catch()
				------------------------------------------------------	
				tempRoom = miloRoomTc miloName:miloName fileName:fileName roomName:roomName timeCycle:timeCycle
				append tcr_roomArray tempRoom
			)	
			------------------------------------------------------		
			xmlStream.close()
			count += 1
		)
		try (destroyDialog  newXmlWait) catch()		
	)
	-------------------------------------------------------------
	fn InitColumnProperties col coltext readonly colwidth fcolor bcolor autosize resizable divider:false=
	(
		Col.resizable = (dotnetclass "system.windows.forms.DataGridViewTriState").false
		Col.headerText = coltext
		Col.readOnly = readonly
		Col.width = colwidth
		--Col.Defaultcellstyle.selectionbackcolor = bcolor
		Col.Defaultcellstyle.backcolor = bcolor			
		Col.Defaultcellstyle.forecolor =  fcolor
		Col.autosizemode = autosize
		Col.resizable = resizable
		--Col.Defaultcellstyle.font = DataGridFont
		Col.SortMode = dgvSortMode.Automatic
		show Col.SortMode
		if divider then Col.dividerwidth = 2
		return Col
	)
	-------------------------------------------------------------
	fn addData data =
	(
		for i in tcr_roomArray do 
		(			
			data.rows.add	#(i.miloName, i.roomName, i.timecycle)
			if i.timecycle == "" then 
			(
				for i = 0 to 2 do (data.item i (data.rows.count-1)).style.backcolor = ColorClass.red
			)
		)
	)
	-------------------------------------------------------------
	fn InitDataGrid data =
	(
			
		data.columns.clear()		
		
		local dgvImageCol = dotNetObject "System.Windows.Forms.DataGridViewTextBoxColumn"
		InitColumnProperties dgvImageCol "Interior" true 120 CellFG CellBG DataGridSizeMode.none ResizeCol.false
		local dgvAttrNameCol = dotNetObject "System.Windows.Forms.DataGridViewTextBoxColumn"
		InitColumnProperties dgvAttrNameCol "Room" true 120 CellFG CellBG DataGridSizeMode.fill ResizeCol.false divider:true			
		local dgvAttrValueCol = dotNetObject "System.Windows.Forms.DataGridViewTextBoxColumn"
		InitColumnProperties  dgvAttrValueCol "Timecycle" true 120 CellFG CellBG DataGridSizeMode.none ResizeCol.false 
		
		data.columns.addrange #(dgvImageCol,dgvAttrNameCol,dgvAttrValueCol)

		CellStyle = dotnetobject "system.Windows.Forms.DataGridViewCellStyle"
		Cellstyle.font = HeaderFont			
		
		data.RowTemplate.Height = 18 -- SET HEIGHT BEFORE DATA IS ENTERED	
		data.multiselect = true
		data.BackgroundColor = CellBG
		data.Visible = true
		data.RowHeadersVisible = false	
		data.ColumnHeadersVisible = true			
		data.ColumnHeadersHeight = 16
		data.ColumnHeadersDefaultCellStyle = CellStyle
		data.cellborderstyle= (dotnetclass "System.Windows.Forms.DataGridViewCellBorderStyle").none
		data.AllowUserToAddRows = false
		data.AllowUserToDeleteRows = false
		data.AllowUserToOrderColumns = true
		data.AllowUserToResizeColumns = true
		data.AllowUserToResizeRows = false
		data.ColumnHeadersHeightSizeMode = (dotnetclass "System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode").DisableResizing    
		data.EnableHeadersVisualStyles = true
		data.ColumnHeadersBorderStyle = (dotnetclass "System.Windows.Forms.DataGridViewHeaderBorderStyle").none			
		data.selectionmode = data.selectionmode.fullrowselect		
	)
	-------------------------------------------------------------
	fn applyColours data =
	(
-- 		rowCount = 1
-- 		for row=0 to (data.rows.count-1) do
-- 		(	
-- 			if (data.rows.item[row].visible) then
-- 			(
-- 				modNumber = mod rowCount 2
-- 				if (modNumber == 1.0) then rowColour = ColorClass.Gainsboro
-- 				else rowColour = ColorClass.Snow
-- 				-------------------------------------------
-- 				for i = 0 to 2 do (data.item i row).style.backcolor = rowColour	
-- 				-------------------------------------------					
-- 				rowCount += 1
-- 			)		
-- 		)
	)
	-------------------------------------------------------------
	fn printToListener data =
	(
		format "============================\n   TimeCycle Report\n============================\n"
		for i=0 to (data.rows.count-1) do
		(	
			if (data.rows.item[i].visible) do
			(
				miloName = (data.rows.item[i].Cells.item[0].value)
				roomName = (data.rows.item[i].Cells.item[1].value)
				timeCycle = (data.rows.item[i].Cells.item[2].value)
				
				format "%.% [%]\n" miloName roomName timeCycle
			)
		)
	)
	--------------------------------------------------------
	-- USER INTERFACE
	--------------------------------------------------------
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:timeCycleReport.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"TimecyleUsageReport" filename:(getThisScriptFilename())
	button btn_print "Print to Listener" offset:[-145,0] width:100	
	edittext edt_filter "Filter Timecycles:" fieldWidth:130 offset:[100,-25] 
	label lab_info "[ ? = none ]" offset:[165,-20]	
	dotNetControl data "System.Windows.Forms.DataGridView" pos:[5,65] width:390 height:495
	-- EVENTS ----------------------------------------------	
	on timeCycleReport open do
	(
		banner.setup() 
		InitDataGrid data
		getTimeCycles()		
		addData data
		applyColours data
	)
	-------------------------------------------------------------
	on btn_print pressed do printToListener data
	-------------------------------------------------------------
	on edt_filter changed arg do 
	(		
		if arg == "" then 
		(
			for i=0 to (data.rows.count-1) do data.rows.item[i].visible = true
		)
		else
		(
			searchResults = #()
			if arg == "?" then
			(
				for i=0 to (data.rows.count-1) do
				(	
					cellValue = (data.rows.item[i].Cells.item[2].value)
					if (cellValue == "") then data.rows.item[i].visible = true
					else data.rows.item[i].visible = false
				)
			)
			else
			(
				for room in tcr_roomArray do
				(
					if matchPattern room.timecycle pattern:("*" + arg + "*") then 
					(
						timecycleName = tolower (room.timecycle as string)
						appendIfUnique searchResults timecycleName
					)
				)		
				for i=0 to (data.rows.count-1) do
				(	
					cellValue = tolower(data.rows.item[i].Cells.item[2].value)
					if (findItem searchResults cellValue) != 0 then data.rows.item[i].visible = true
					else data.rows.item[i].visible = false
				)
			)
		)
		applyColours data
	)
)
createdialog timeCycleReport style:#(#style_border,#style_toolwindow,#style_sysmenu)