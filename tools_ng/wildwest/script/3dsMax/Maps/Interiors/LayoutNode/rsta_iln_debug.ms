fn iln_make_debug =
(
	local offset = 0
	local startOffset = [20,-5,1]
	xml_io_debug = rsta_xml_io xmlFile:@"X:\gta5_dlc\mpPacks\mpApartment\assets\interiors\layout\int_mp_h_04.xml"
	
	local propFiles = #("int_mp_h_accessories.xml", 
						"int_mp_h_beds.xml",
						"int_mp_h_dining.xml",
						"int_mp_h_lighting.xml",
						"int_mp_h_seating.xml",
						"int_mp_h_storage.xml",
						"int_mp_h_tables.xml",
						"int_mp_h_Kitchens.xml")
						
	local lnList_xn = xml_io_debug.root.selectSingleNode "Milo/MloRoom/LayoutNodeList"
	lnList_xn.RemoveAll()
	
	for pFile in propFiles do
	(
		ln_xn = rsta_xml.makeNode "LayoutNode" lnList_xn attr:"guid" value:(genguid())
		rsta_xml.makeNode "Name" ln_xn innerText:pFile
			
		local pos = startOffset + [0,offset*4,0]
			
		rsta_xml.makeNode_point3 "Translation" pos ln_xn 
		rsta_xml.makeNode_point3 "Rotation" [0,0,0] ln_xn 	
		rsta_xml.makeNode_bool "Purchasable" true ln_xn
		rsta_xml.makeNode_bool "AllowEmpty" false ln_xn
		rsta_xml.makeNode_bool "AllowGrandchildren" false ln_xn
			
		offset += 1
			
			
		local groupList_xn = rsta_xml.makeNode "GroupList" ln_xn 			
			
		local xml_io = rsta_xml_io xmlFile:(@"X:\gta5_dlc\mpPacks\mpApartment\assets\export\levels\gta5\props\high\" + pFile)
		for prop_xn in (rsta_xml.getChildren xml_io.root name:"objects/object[@superclass=\"geomobject\"]") do
		(		
			local dontExport_xn = prop_xn.selectSingleNode "attributes/attribute[@name=\"Dont Export\"]"
			if (dontExport_xn == undefined) do
			(
				local propName = (prop_xn.getAttribute "name")
				local group_xn = rsta_xml.makeNode "Group" groupList_xn attr:"guid" value:(genguid())
				group_xn.setAttribute "name" propName 
				local rsRef_xn = rsta_xml.makeNode "RsRef" group_xn attr:"name" value:propName
				
				rsta_xml.makeNode_point3 "Translation" [0,0,0] rsRef_xn 
				rsta_xml.makeNode_point3 "Rotation" [0,0,0] rsRef_xn 
					
				format "%\n" propName
			)
		)
	)
	
	xml_io_debug.save()
)


fn iln_remake_from_xml =
(
	delete (for h in helpers where classof h == RsInteriorLayoutNode collect h)
	
	xml_io = rsta_xml_io xmlFile:@"X:\gta5_dlc\mpPacks\mpApartment\assets\interiors\layout\int_mp_h_04.xml"
	for milo_xn in (rsta_xml.getchildren xml_io.root name:"Milo") do
	(
		local miloName = milo_xn.getattribute "name"
		local milo_node = getNodeByName miloName
		if (milo_node != undefined) do
		(
			for mloRoom_xn in (rsta_xml.getchildren milo_xn name:"MloRoom") do
			(
				local mloRoomName = mloRoom_xn.getattribute "name"
				local mloRoom_node = getNodeByName mloRoomName
				if (mloRoom_node != undefined) do
				(
					for ln_xn in (rsta_xml.getchildren mloRoom_xn name:"LayoutNodeList/LayoutNode") do
					(
						local ln_node = RsInteriorLayoutNode()
						ln_node.name = (ln_xn.selectSingleNode "Name").InnerText
						ln_node.guid = ln_xn.getAttribute "guid"
						ln_node.pos = rsta_xml.get_point3 (ln_xn.selectSingleNode "Translation")
						local r_p3 = rsta_xml.get_point3 (ln_xn.selectSingleNode "Rotation") 
						ln_node.rotation = eulerToQuat (eulerAngles r_p3.x r_p3.y r_p3.z)
						ln_node.purchasable = rsta_xml.get_bool (ln_xn.selectSingleNode "Purchasable")
						ln_node.allowEmpty = rsta_xml.get_bool (ln_xn.selectSingleNode "AllowEmpty")
						ln_node.allowGrandchildren = rsta_xml.get_bool (ln_xn.selectSingleNode "AllowGrandchildren")
						
						ln_node.parent = mloRoom_node				
					)
				)
			)
		)
	)
)

--iln_remake_from_xml()

/*
for h in helpers where classof h == RsInteriorLayoutNode do
(
	h.wirecolor = color 88 177 27
	for c in h.children do c.wirecolor = color 176 26 26
)

select (for h in helpers where classof h == RsInteriorLayoutNode collect h)

$[1].pos = $[2].pivot
*/