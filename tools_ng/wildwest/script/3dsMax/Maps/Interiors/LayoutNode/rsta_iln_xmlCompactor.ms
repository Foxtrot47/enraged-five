--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- XML COMPACTOR
--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct rsta_iln_xmlCompactor
(
	files_components,
	files_interiors,
	files_dynamicLists,
	layoutFolder = @"X:\gta5_dlc\mpPacks\mpApartment\assets\interiors\layout\",
	filePathXml = @"X:\gta5_dlc\mpPacks\mpApartment\assets\interiors\layout\propLayoutFiles.xml",
	current_is_purchasable = true,
	current_progress_total = 0.0,
	percent_amount = 100.0,
	---------------------------------------------------------------------------
	-- GET FILE PATHS
	---------------------------------------------------------------------------	
	fn get_fileLists =
	(
		local xml_io = RSTA_xml_IO xmlFile:filePathXml	
		xml_io.load()
		files_interiors = for file in (rsta_xml.getChildren xml_io.root name:"interior/file") collect file.innerText
		files_components = for file in (rsta_xml.getChildren xml_io.root name:"components/file") collect file.innerText
		files_dynamicLists = for file in (rsta_xml.getChildren xml_io.root name:"dynamicLists/file") collect file.innerText
	),
	---------------------------------------------------------------------------
	-- GET HASH OF STRING
	---------------------------------------------------------------------------
	fn get_hashCode input = 
	(
		return (((dotNetObject "System.String" (input as string)).GetHashCode()) as string)	
	),
	---------------------------------------------------------------------------
	-- IMPORT RSREF FROM COMPONENTS 
	---------------------------------------------------------------------------
	fn import_rsRef rsRef_xn =
	(	
		for component_file in files_components do
		(
			local rsRef_xml_io = RSTA_xml_IO xmlFile:component_file 
			rsRef_xml_io.load()
			for rsRef_info_xn in (rsta_xml.getChildren rsRef_xml_io.root name:"File/RsRef" attr:"name" value:(rsRef_xn.getAttribute "name")) do return rsRef_info_xn
		)	
		return undefined
	),
	---------------------------------------------------------------------------
	-- IMPORT DYNAMIC LISTS 
	---------------------------------------------------------------------------
	fn import_dynamicList dynamicList_xn =
	(
		for dl_file in files_dynamicLists do
		(					
			local dynamicList_xml_io = RSTA_xml_IO xmlFile:dl_file 
			dynamicList_xml_io.load()
			for dynamicList_xn in (rsta_xml.getChildren dynamicList_xml_io.root name:"DynamicList" attr:"guid" value:(dynamicList_xn.getAttribute "guid")) do return dynamicList_xn
		)
		messageBox "import_dynamicList Failed"
		return undefined
	),
	---------------------------------------------------------------------------
	-- COMPACT GROUP 
	---------------------------------------------------------------------------
	fn compact_group group_node compact_parent allowChildren =
	(
		local c_group_xn = rsta_xml.makeNode "Item" compact_parent attr:"type" value:"CGroup" 
		-- SET NAME 
		rsta_xml.makeNode "Name" c_group_xn innerText:(group_node.getAttribute "name")
		-- SET ID
		rsta_xml.makeNode "Id" c_group_xn attr:"value" value:(get_hashCode (group_node.getAttribute "guid"))
		local c_rsRefList_xn = rsta_xml.makeNode "RefList" c_group_xn
		-- BRING IN RSREFS			
		for rsRef_xn in  rsta_xml.getChildren group_node name:"RsRef" do this.compact_rsRef rsRef_xn c_rsRefList_xn allowChildren
	),
	---------------------------------------------------------------------------
	-- COMPACT RSREF 
	---------------------------------------------------------------------------
	fn compact_rsRef rsRef_node compact_parent allowChildren = 
	(
		-- GET COMPONENT INFO -------------------------------------------		
		component_rsRef_xn = import_rsRef rsRef_node
		if (component_rsRef_xn != undefined) then
		(
			local c_rsRef_xn = rsta_xml.makeNode "Item" compact_parent attr:"type" value:"CRsRef" 
			rsta_xml.makeNode "Name" c_rsRef_xn innerText:(rsRef_node.getAttribute "name")
			-- TRANSLATION / ROTATION 
			for type in #("Translation", "Rotation") do
			(			
				local temp_xn = rsRef_node.SelectSingleNode(type) 
				if (temp_xn != undefined) then rsta_xml.makeNode_point3 type (rsta_xml.get_point3 temp_xn) c_rsRef_xn
				else rsta_xml.makeNode_point3 type [0,0,0] c_rsRef_xn
			)
			-- SHOP INFO ------------------------------------------------		
			local shopData_xn = rsta_xml.makeNode "ShopData" c_rsRef_xn
			if (current_is_purchasable) do
			(
				local component_shopData = component_rsRef_xn.SelectSingleNode("ShopData")
				if (component_shopData != undefined) do
				(				
					rsta_xml.makeNode "Name" shopData_xn innerText:((component_shopData.SelectSingleNode("Name")).innerText)
					rsta_xml.makeNode "Price" shopData_xn innerText:((component_shopData.SelectSingleNode("Price")).innerText)
					rsta_xml.makeNode "Description" shopData_xn innerText:((component_shopData.SelectSingleNode("Description")).innerText)
				)
			)
			-- LAYOUTNODES ------------------------------------------------
			local c_layoutNodeList = rsta_xml.makeNode "LayoutNodeList" c_rsRef_xn
			if (allowChildren) do
			(
				for layoutNode_xn in rsta_xml.getChildren component_rsRef_xn name:"LayoutNodeList/LayoutNode" do
				(
					this.compact_layoutNode layoutNode_xn c_layoutNodeList 
				)
			)
		)
		else 
		(
			format "WARNING! % not in componet list\n" (rsRef_node.getAttribute "name")
		)
	),	
	---------------------------------------------------------------------------
	-- IS THE RSREF BIGGER THAN THE LAYOUTNODE?
	---------------------------------------------------------------------------
	fn is_valid_size rsRef_xn layoutBounds =
	(
		
		local is_valid = true
		local component_rsRef_xn = import_rsRef rsRef_xn
		
		if (component_rsRef_xn == undefined) do 
		(
			format "% - RsRef not found in XML, skipping.\n" (rsRef_xn.getAttribute "name")
			return false
		)		

		local rsRef_bounds_min = rsta_xml.get_point3 (component_rsRef_xn.selectSingleNode("Bounds/Min"))
		local rsRef_bounds_max = rsta_xml.get_point3 (component_rsRef_xn.selectSingleNode("Bounds/Max"))			
		-- TEST SIZES	
		if rsRef_bounds_min.x < layoutBounds.min.x do is_valid = false
		if rsRef_bounds_min.y < layoutBounds.min.y do is_valid = false	
		if rsRef_bounds_min.z < layoutBounds.min.z do is_valid = false				
		if rsRef_bounds_max.x > layoutBounds.max.x do is_valid = false
		if rsRef_bounds_max.y > layoutBounds.max.y do is_valid = false
		if rsRef_bounds_max.z > layoutBounds.max.z do is_valid = false

		return is_valid
	),
	---------------------------------------------------------------------------
	-- COMPACT DYNAMIC LIST
	---------------------------------------------------------------------------
	fn compact_dynamicList dListPointer_xn compact_parent layoutBounds allowChildren =
	(
		local dynamicList_xn = (import_dynamicList dListPointer_xn)
		if (dynamicList_xn != undefined) then
		(			
			for rsRef_xn in rsta_xml.getChildren (import_dynamicList dynamicList_xn) name:"RsRef" do
			(
				if (is_valid_size rsRef_xn layoutBounds) then
				(
					-- MAKE GROUP
					local c_group_xn = rsta_xml.makeNode "Item" compact_parent attr:"type" value:"CGroup" 
					-- SET NAME 
					rsta_xml.makeNode "Name" c_group_xn innerText:(rsRef_xn.getAttribute "name")
					-- SET ID
					rsta_xml.makeNode "Id" c_group_xn attr:"value" value:(get_hashCode ((compact_parent.getAttribute "id") + (rsRef_xn.getAttribute "name")))
					-- MAKE REFLIST
					local c_rsRefList_xn = rsta_xml.makeNode "RefList" c_group_xn				
					compact_rsRef rsRef_xn c_rsRefList_xn allowChildren
				)
				else format "Too big : %\n" (rsRef_xn.getAttribute "name")
			)	
		)
		else messageBox "DList Not Found."
	),
	---------------------------------------------------------------------------
	-- COMPACT LAYOUT NODE 
	---------------------------------------------------------------------------
	fn compact_layoutNode layout_node compact_parent =
	(		
		-- GET BOUNDS 
		local layoutBounds = dataPair min:(rsta_xml.get_point3 (layout_node.selectSingleNode("Bounds/Min"))) max:(rsta_xml.get_point3 (layout_node.selectSingleNode("Bounds/Max")))
		-- MAKE LAYOUT NODE		
		local c_layoutNode_xn = rsta_xml.makeNode "Item" compact_parent attr:"type" value:"CLayoutNode"  
		rsta_xml.makeNode "Name" c_layoutNode_xn innerText:((layout_node.selectSingleNode("Name")).InnerText)
		-- TRANSLATION /ROATION
		rsta_xml.makeNode_point3 "Translation" (rsta_xml.get_point3 (layout_node.selectSingleNode("Translation"))) c_layoutNode_xn	
		rsta_xml.makeNode_point3 "Rotation" (rsta_xml.get_point3 (layout_node.selectSingleNode("Rotation"))) c_layoutNode_xn	
		-- PURCHASABLE
		rsta_xml.makeNode_bool "Purchasable" (rsta_xml.get_bool (layout_node.SelectSingleNode("Purchasable"))) c_layoutNode_xn	
		current_is_purchasable = (rsta_xml.get_bool (layout_node.SelectSingleNode("Purchasable")))
		-- ALLOW GRANDCHILDREN
		local allow_grandchildren = rsta_xml.get_bool (layout_node.SelectSingleNode("AllowGrandchildren"))	
		-- GROUP LIST 
		local c_groupsList_xn = rsta_xml.makeNode "GroupList" c_layoutNode_xn		
		-- BRING IN DYNAMIC LISTS
		for dynamicList_xn in rsta_xml.getChildren layout_node name:"GroupList/DynamicList" do compact_dynamicList dynamicList_xn c_groupsList_xn layoutBounds allow_grandchildren
		-- BRING IN GROUPS
		for group_xn in rsta_xml.getChildren layout_node name:"GroupList/Group" do compact_group group_xn c_groupsList_xn allow_grandchildren
	),
	---------------------------------------------------------------------------
	-- COMPACT MILO XML NODE
	---------------------------------------------------------------------------
	fn compact_milo milo_xn int_file =
	(
		-- MAKE COMPACT IO
		local compact_xml_io = RSTA_xml_IO()	
		-- MAKE ROOT
		compact_xml_io.new "CMiloInterior"
		-- ADD NAME, FILE AND ROOM LIST
		local fileName = FilterString int_file "\\/."
		rsta_xml.makeNode "Name" compact_xml_io.root innerText:(milo_xn.getAttribute "name")
		rsta_xml.makeNode "File" compact_xml_io.root innerText:fileName[fileName.count-1]
		local c_roomList_xn = rsta_xml.makeNode "RoomList" compact_xml_io.root
		-- LOOP
		local miloRooms_xnArray = rsta_xml.getChildren milo_xn name:"MloRoom"
		
		percent_amount = percent_amount/miloRooms_xnArray.count
		format "1. percent_amount : %\n" percent_amount
		
		for miloRoom_xn in miloRooms_xnArray do
		(
			
			-- ADD COMPACTED ROOM
			local c_miloRoom_xn = rsta_xml.makeNode "Item" c_roomList_xn attr:"type" value:"CMiloRoom"
			-- ADD NAME AND LAYOUT NODE LIST 
			rsta_xml.makeNode "Name" c_miloRoom_xn innerText:(miloRoom_xn.getAttribute "name")
			-- LAYOUT NODES
			local c_layoutNodeList = rsta_xml.makeNode "LayoutNodeList" c_miloRoom_xn
			
			local layoutNodes_xnArray = rsta_xml.getChildren miloRoom_xn name:"LayoutNodeList/LayoutNode"
			percent_amount = percent_amount/layoutNodes_xnArray.count
			format "1. percent_amount : %\n" percent_amount
			
			for layoutNode_xn in layoutNodes_xnArray do
			(
				compact_layoutNode layoutNode_xn c_layoutNodeList 
				this.set_progressBar()
			)
		)
		-- SAVE ------------------------------------------------
		--local xml_saveAs_name = layoutFolder + @"compacted\" + (milo_xn.getAttribute "name") + ".xml"
		local xml_saveAs_name = @"X:\gta5_dlc\mpPacks\mpApartment\build\dev\common\data\" + (milo_xn.getAttribute "name") + ".xml"		
		makeDir (layoutFolder + @"compacted\")
		compact_xml_io.saveAs xml_saveAs_name saveWarning:false
	),
	---------------------------------------------------------------------------
	-- MAKE UI
	---------------------------------------------------------------------------
	fn make_UI =
	(
		try (destroyDialog  iln_compactor_roll) catch()
		rollout iln_compactor_roll "Interior Layout Node" width:300
		(
			label lb1 "Compiling Game Data..." width:290
			progressbar pb width:290 offset:[-8,0]
		)
		createDialog iln_compactor_roll style:#(#style_titlebar, #style_border, #style_sysmenu,#style_toolwindow)
	),
	
	---------------------------------------------------------------------------
	-- COMPACT ALL INTERIORS
	---------------------------------------------------------------------------
	fn compact_all JustThisFile:false =
	(
		make_UI()
		if not(JustThisFile) do percent_amount = percent_amount/files_interiors.count
		
		for int_file in files_interiors do
		(
			interior_xml_io = RSTA_xml_IO xmlFile:int_file
			interior_xml_io.load()
			
			if (not(JustThisFile) OR (JustThisFile AND getFilenameFile int_file == getFilenameFile maxfilename)) do
			(			
				local milos_xn_array = rsta_xml.getChildren interior_xml_io.root name:"Milo"
				if not(JustThisFile) do percent_amount = percent_amount/milos_xn_array.count
				
				format "1. percent_amount : %\n" percent_amount
				
				for milo_xn in milos_xn_array do 
				(
					compact_milo milo_xn int_file
				)
			)
		)
		if (iln_compactor_roll != undefined) do iln_compactor_roll.lb1.text = "Compacted Compleate!"	
	),
	---------------------------------------------------------------------------
	-- UPDATE PROGRESSBAR
	---------------------------------------------------------------------------
	fn set_progressBar = 
	(
		current_progress_total += percent_amount 
		if (iln_compactor_roll != undefined) do
		(
			iln_compactor_roll.pb.value = (current_progress_total as integer)
		)
	),
	---------------------------------------------------------------------------
	-- ON CREATE
	---------------------------------------------------------------------------
	on create do
	(
		get_fileLists()
	)
)
