filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_XML.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_UI.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_conversion.ms") -- MOVE THIS TO COMMON
-- STRUCTS
filein (RsConfigGetWildWestDir() + "script/3dsMax/Maps/Interiors/LayoutNode/rsta_iln_xmlCompactor.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/Maps/Interiors/LayoutNode/rsta_iln_functions.ms")

-----------------------------------------------------------------------------------------------
-- MAIN ROLLOUT 
------------------------------------------------------------------------------------------------
try(destroyDialog iln_manager_ui)catch()
rollout iln_manager_ui "Interior Layout Node Manager" width:350
(
	-------------------------------------------------------------------------------
	-- LOCALS
	-------------------------------------------------------------------------------
	--local xmlCompactor = rsta_iln_xmlCompactor()
	-------------------------------------------------------------------------------
	-- CONTROLS
	-------------------------------------------------------------------------------
	-- BANNER -------------------------------------------------------------
	dotNetControl rsBannerPanel "System.Windows.Forms.Panel" height:32 pos:[0,0]  width:iln_manager_ui.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel versionNum:1.2 versionName:"Lopsided Cat" wiki:"Interior_Layout_ToolKit" mail:"mattharrad@rockstarnorth.com" filename:(getThisScriptFilename())
	-------------------------------------------------------------------------------
	button btn_menu "Menu" width:40 across:3 offset:[-39,0]
	button btn_dLists "Dynamic List Mode" width:140  offset:[-46,0]
	button btn_layoutNode "Layout Node Mode" width:140 offset:[-10,0] 
	dotNetControl dotnet_treeview "system.windows.forms.treeView" height:400 width:(iln_manager_ui.width-10) offset:[-7,5]	
	-------------------------------------------------------------------------------
	-- FUNCTIONS 
	-------------------------------------------------------------------------------	
	fn mainMenuMaker =
	(
		rcMenu iln_mainMenu 
		(		
			submenu "File"
			(
				menuItem mi_export "Save Current Layouts"	
				separator sep3				
				menuItem mi_showLayoutFiles "Show Layout Files"
			)
			submenu "Layout Node"
			(
				menuItem mi_newNode "New Layout Node"
				separator sep1
				menuITem mi_boundsSnapTop "Snap Bounds To Top"
				menuITem mi_boundsSnapMiddle "Snap Bounds To Middle"
				menuITem mi_boundsSnapBottom "Snap Bounds To Bottom"
			)
			submenu "Compile Game Data"
			(
				menuItem mi_compress_this "Just This File"
				menuItem mi_compress_all "All Files"
				separator sep2
				menuItem mi_showCompressedFiles "Show Game Data Files" 
			)
			-- EVENTS 
			on mi_export picked do iln_fn.export.export()
			on mi_showLayoutFiles picked do iln_fn.general.showFiles #layout
			--			
			on mi_newNode picked do iln_fn.general.newLayoutNode()
			on mi_boundsSnapTop picked do iln_fn.general.node_snap_bounds #snapTop
			on mi_boundsSnapMiddle picked do iln_fn.general.node_snap_bounds #snapMiddle
			on mi_boundsSnapBottom picked do iln_fn.general.node_snap_bounds #snapBottom
			--			
			on mi_compress_this picked do 
			(
				for file in iln_files.interior where (getFilenameFile file == getFilenameFile maxfilename) do
				(
					ShellLaunch (RsConfigGetWildWestDir() + "script/standalone/LayoutNodeExporter/LayoutNodeExporter.exe") file
				)
			)
			on mi_compress_all picked do
			(
				for file in iln_files.interior do
				(
					format "Compiling : %\n" file
					ShellLaunch (RsConfigGetWildWestDir() + "script/standalone/LayoutNodeExporter/LayoutNodeExporter.exe") file
				)
			)
			on mi_showCompressedFiles picked do iln_fn.general.showFiles #compactor				
		)
		popUpMenu iln_mainMenu 
	)
	-------------------------------------------------------------------------------
	-- EVENTS 
	-------------------------------------------------------------------------------
	on btn_menu pressed do mainMenuMaker()
	-------------------------------------------------------------------------------
	on btn_dLists pressed do iln_fn.tree.populate_tree #dynamicList
	-------------------------------------------------------------------
	on btn_layoutNode pressed do iln_fn.tree.populate_tree #layoutNode
	------------------------------------------------------------------------------
	on dotnet_treeview MouseDown sender arg do 
	(	
		hitNode = dotnet_treeview.GetNodeAt (dotNetObject "System.Drawing.Point" arg.x arg.y)
		dotnet_treeview.selectedNode = hitNode
		if (arg.Button == arg.Button.Right) do ::iln_fn.tree.makeMenu (iln_fn.tree.get_node_type hitNode)
	)
	-------------------------------------------------------------------------------
	on iln_manager_ui open do
	(	
		rs_dialogPosition "get" iln_manager_ui
		banner.setup()
		iln_fn.tree.treeview = iln_fn.tree.treeMan.treeview = dotnet_treeview
		iln_fn.tree.treeMan.init_icons()
		
		-- SET SELECTION MODE
		if (selection.count != 0 AND classof selection[1] == RsInteriorLayoutNode) then
		(
			iln_fn.tree.populate_tree #layoutNode
		)
		else 
		(
			iln_fn.tree.populate_tree #dynamicList
		)		
		
		-- CALLBACKS
		callbacks.addScript #filePostOpen "iln_fn.io.set_up_layout()" id:#iln_fileRefresh
	)	
	-------------------------------------------------------------------------------
	on iln_manager_ui close do
	(
		if (iln_fn.hasChanged) do messageBox "Warning, You've not saved this!"
		rs_dialogPosition "set" iln_manager_ui
		callbacks.removeScripts id:#iln_fileRefresh
	)
	-------------------------------------------------------------------------------
	on iln_manager_ui resized newSize do
	(
		iln_manager_ui.width = 350
		iln_manager_ui.dotnet_treeview.height = iln_manager_ui.height-74
	)
)
createDialog iln_manager_ui style:#(#style_border,#style_toolwindow,#style_sysmenu,#style_resizing)
