filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
filein (RsConfigGetWildWestDir() + "script\3dsMax\_common_functions\FN_RSTA_UI.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/fn_rsta_xml.ms")
-----------------------------------------------
try (destroyDialog  openIntFile) catch()
-----------------------------------------------
rollout openIntFile "Open Interior File..." width:300 height:600
(	
	local maxFile = ""
	local xmlFile = thewildwest + @"script\3dsMax\_config_files\general\interiorFileNames.xml"	
	-- USER INTERFACE -----------------------
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:openIntFile.Width	
	local banner = makeRsBanner dn_Panel:rsBannerPanel versionNum:2.00 versionName:"Brash Sweater" wiki:"Open_From_Milo_List" filename:(getThisScriptFilename())
	radiobuttons search_type labels:#("Search By Milo", "Search By File")		
	dotNetControl tv "system.windows.forms.treeView" height:(openIntFile.Height-60) width:openIntFile.Width	 pos:[0,60]	
	local tvMan = RSTA_ui_treeviewManager()
		
	-- FUNCTIONS -----------------------------------
	fn sort_milo_by_name v1 v2 = stricmp (toLower (v1.getAttribute "name")) (toLower (v2.getAttribute "name"))
	-----------------------------------------------------------	
	fn makeMenu node =
	(
		if node.tag == "File" OR node.tag == "Milo" do
		(			
			if node.tag == "File" then maxFile = node.text
			else
			(
				if search_type.state == 1 then maxFile = node.Nodes.Item[0].text
				else maxFile = node.parent.text
			)
				
			-- MAKE THE MENU
			rcMenu openIntFile_menu 
			(
				menuItem mi_open_max "Open Max File"
				separator sep1
				menuItem mi_show_max "Show Max File In Explorer"
				separator sep2
				menuItem mi_show_export "Show Export Files In Explorer"
				separator sep3
				menuItem mi_show_pc "Show PC Files In Explorer"
				menuItem mi_show_xbone "Show xBone Files In Explorer"
				menuItem mi_show_ps4 "Show PS4 Files In Explorer"
				separator sep4
				menuItem mi_update "Update Data"
				
				-- EVENTS ------------
				on mi_open_max picked do openIntFile.menu_picked #openMax
				on mi_show_max picked do openIntFile.menu_picked #showMax
				on mi_show_export picked do openIntFile.menu_picked #showExport
				on mi_show_pc picked do openIntFile.menu_picked #showPC
				on mi_show_xbone picked do openIntFile.menu_picked #showXbone
				on mi_show_ps4 picked do openIntFile.menu_picked #showPs4
				on mi_update picked do openIntFile.update_xml()
			)
			popUpMenu openIntFile_menu 
		)
	)
	----------------------------------------------------------
	fn backSlashify input =
	(
		return substituteString input "/" "\\"
	)
	----------------------------------------------------------
	fn shell_launch path =
	(
		shellLaunch "explorer.exe" ("/e,/select, " + (backSlashify path) + "\"")
	)
	----------------------------------------------------------
	fn getFileName type = 
	(
		return substituteString maxFile ".max" ("." + type)
	)
	----------------------------------------------------------
	fn menu_picked cmd =
	(
		case cmd of
		(
			#openMax : 
			(
				if (CheckForSave()) do loadMaxFile (backSlashify (RsConfigGetArtDir() + "models\\" + (getFileName "max")))
				try (destroyDialog  openIntFile) catch()
			)
			#showMax : shell_launch (RsConfigGetArtDir() + "models\\" + (getFileName "max"))
			#showExport : shell_launch (RsConfigGetExportDir() + "levels\\gta5\\" + (getFileName "xml"))
			#showPC : shell_launch (RsConfigGetBuildDir() + "x64\\levels\\gta5\\" + (getFileName "rpf"))	
			#showXbone : shell_launch (RsConfigGetBuildDir() + "xboxone\\levels\\gta5\\" + (getFileName "rpf"))	
			#showPs4 : shell_launch (RsConfigGetBuildDir() + "ps4\\levels\\gta5\\" + (getFileName "rpf"))				
		)
	)
	-----------------------------------------------------------
	fn populateTv =
	(
		tvMan.clear()
		local xml_io = rsta_xml_io xmlFile:xmlFile
		if (search_type.state == 2) then -- BY FILE
		(
			for folder_xn in rsta_xml.getChildren xml_io.root do
			(
				folder_tvn = tvMan.addNode (folder_xn.getAttribute "path") tv tag:"Folder" img:0				
				for file_xn in rsta_xml.getChildren folder_xn do
				(					
					file_tvn = tvMan.addNode (file_xn.getAttribute "path") folder_tvn tag:"File" img:1					
					for milo_xn in rsta_xml.getChildren file_xn do tvMan.addNode (milo_xn.getAttribute "name") file_tvn tag:"Milo" img:2							
				)
				folder_tvn.Expand()
			)						
		)
		else -- BY MILO
		(
			miloList = #()
			for milo_xn in rsta_xml.getChildren xml_io.root name:"Folder/File/Milo" do append miloList milo_xn				
			qsort miloList sort_milo_by_name			
			for milo_xn in miloList do
			(
				milo_tvn = tvMan.addNode (milo_xn.getAttribute "name") tv tag:"Milo" img:2	
				tvMan.addNode (milo_xn.parentNode.getAttribute "path") milo_tvn tag:"File" img:1	
			)
		)	
		-- SCROLL TO TOP
		tv.Nodes.Item[0].EnsureVisible()
	)
	------------------------------------------------------
	fn update_xml =
	(
		local xml_cache = @"T:\SceneXmlCache\GTAV_Cache.xml"
		local milo_xml = thewildwest + @"script\3dsMax\_config_files\general\interiorFileNames.xml"
		gRSperforce.sync xml_cache 
		gRSperforce.edit milo_xml
		
		local xml_io_cache = rsta_xml_io xmlfile:xml_cache
			
		xml_io_c = rsta_xml_io()
		xml_io_c.new "Folders"
		
		for milo_xn in (rsta_xml.getChildren xml_io_cache.root name:"File[@type=\"interior\"]/Milo") do
		(
			file_xn = milo_xn.parentNode
			filePath = file_xn.getAttribute "path"
			filePath_a = FilterString filePath "\\"
			folderPath = filePath_a[1]
			for i=2 to filePath_a.count-1 do folderPath = folderPath + "\\"+ filePath_a[i]
			folderPath = substituteString folderPath @"X:\gta5\assets_ng\export\levels\gta5\" @""			
			-- MAKE FOLDER NODE
			folder_xn = rsta_xml.makeNode "Folder" xml_io_c.root attr:"path" value:folderPath unique:true		
			filePath = substituteString filePath @"X:\gta5\assets_ng\export\levels\gta5\" @""
			filePath = substituteString filePath @".xml" @".max"
			-- MAKE FILE NODE
			file_xn = rsta_xml.makeNode "File" folder_xn attr:"path" value:filePath unique:true				
			-- MAKE MILO NODE
			c_milo_xn = rsta_xml.makeNode "Milo" file_xn attr:"name" value:(milo_xn.getAttribute "name") unique:true				
		)
		xml_io_c.saveAs milo_xml	
		
	)
	
	-- EVENTS --------------------------------------
	on openIntFile open do 
	(
		banner.setup() 
		
		tvMan.treeview = tv	
		tvMan.iconFiles = #(@"x:/gta5/tools_ng/wildwest/script/3dsMax/UI/icons_treeview/icon_folderWindows.bmp",
							@"x:/gta5/tools_ng/wildwest/script/3dsMax/UI/icons_treeview/icon_list.bmp",
							@"x:/gta5/tools_ng/wildwest/script/3dsMax/UI/icons_treeview/icon_pyramid_blue.bmp")
		tvMan.init_icons()
		
		if (gRSperforce.connected()) do gRSperforce.sync xmlFile -- GRAB THE LATEST LIST
		rs_dialogPosition "get" openIntFile -- SET POS			
		populateTv()
	)
	-----------------------------------------------------------
	on search_type changed arg do populateTv()
	-----------------------------------------------------------
	on tv MouseDown arg do 
	(	
		hitNode = tv.GetNodeAt (dotNetObject "System.Drawing.Point" arg.x arg.y)
		tv.selectedNode = hitNode
		if (hitNode != undefined) then if (arg.Button == arg.Button.Right) do makeMenu hitNode	
	)

)
createDialog openIntFile style:#(#style_border,#style_toolwindow,#style_sysmenu)

