------------------------------------------------------
-- SET_NAMES_ON_MULIMAT
-- MATT HARRAD [29/02/12]
-- ROCKSTAR NORTH
-- SETS THE BLANK NAMES IN THE MULTIMATERIAL FROM THE SHADERS DIFFUSE TEXTURE
------------------------------------------------------

-- FILE IN-----------------------------
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

-------------------------------------------------------------------------
try (destroyDialog setNamesOnMultiMap) catch()
rollout setNamesOnMultiMap  "Set Names On Multimaterial's" width:200
(
	-- FUNCTIONS -----------------------------
	fn setNames =
	(
		mat = medit.GetCurMtl()
		if (classof mat) == Multimaterial then
		(
			for listID = 1 to mat.materiallist.count do
			(	
				rename = true
				if (setNamesOnMultiMap.cbx_allNames.checked) AND (mat.names[listID] != "") then rename = false

				-- RENAME -----------------------------
				if (rename) then 
				(
					subMat = mat.materiallist[listID] 
					for i = 1 to (RstGetVariableCount subMat) do
					(
						if (RstGetVariableName subMat i) == "Diffuse Texture" then
						(
							pathArray = filterString (RstGetVariable subMat i) "\\"
							fileName = pathArray[pathArray.count] 
							fileName = substring fileName 1 (fileName.count-4)  -- remove filetype
							nameArray = filterstring fileName "_"
							
							if (nameArray[nameArray.count]).count == 1 then textureName = nameArray[nameArray.count-1]
							else textureName = nameArray[nameArray.count]						
					
							textureName =  (substring textureName 1 setNamesOnMultiMap.spn_borderAmount.value)
							mat.names[listID] = textureName
						)
					)
				)
			)
		)	
		else format "% is not a Multimaterial!\n" mat.name 
	)
	
	-- USER INTERFACE -----------------------------
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:setNamesOnMultiMap.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"setNamesOnMultiMap" filename:(getThisScriptFilename())
	
	group "Settings"
	(
		checkbox cbx_allNames "Keep Current (Fill in Blanks)" checked:true
		spinner spn_borderAmount "Name Cutoff Length: " range:[0,20,8] type:#integer width:150 offset:[0,2] align:#left 
	)
	button btn_setNames "Set Names On Selected" width:190
	
	-- EVENTS -----------------------------
	on btn_setNames pressed do setNames()
	
	-- ON OPEN -----------------------------
	on setNamesOnMultiMap open do 
	(
		banner.setup() 
		rs_dialogPosition "get" setNamesOnMultiMap
	)
	-- ON CLOSE -----------------------------
	on setNamesOnMultiMap close do
	(
		rs_dialogPosition "set" setNamesOnMultiMap	
	)
)
CreateDialog setNamesOnMultiMap style:#(#style_border,#style_toolwindow,#style_sysmenu) 

