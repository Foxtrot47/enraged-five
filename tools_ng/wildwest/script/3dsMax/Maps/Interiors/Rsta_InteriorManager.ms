filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
rsta_loadCommonFunction #("FN_RSTA_userSettings")

if (InteriorToolkit != undefined) do InteriorToolkit.dispose()
struct InteriorToolkit
(
	-- DEBUG ------------------------------------------------
	fn debug_fn name = if (false) do format "FN %\n" name,
	-- LOCALS ------------------------------------------------
	MainWindow, 
	vm,
	Settings = rsta_userSettings app:"InteriorManger",	
	Milos, 
	SelectedMilo,
	miloRooms,
	-- FUNCTIONS ---------------------------------------------
	fn initMilo = 
	(
		debug_fn "initMilo"
		Milos = for h in helpers where classof h == Gta_MILO collect h
		for m in Milos do vm.Milos.add m.name
	),
	-------------------------------------------------------------------
	fn initMiloRooms =
	(
		debug_fn "initMiloRooms"
		vm.MiloRooms.Clear()
		vm.AddDefaults()
		if (vm.Milos.Count != 0) do
		(
			SelectedMilo = getNodeByName vm.Milos.Item[0]
			if (SelectedMilo != undefined) do 
			(		
				miloRooms = rsGetMloRooms Milo:SelectedMilo
				for miloRoom in (miloRooms) do vm.MiloRooms.Add miloRoom.name 
			)
			vm.SelectedMiloRoomIdx = 0
		)
	),
	-------------------------------------------------------------------
	fn isType_bool obj type =
	(
		debug_fn "isType_bool"
		case type of
		(
			#all :return true
			#mesh: return (classof_array #(Editable_Poly, Editable_Mesh) obj)		
			#RsRef: return (classof_array #(RSrefObject) obj)	
			#Collision: return if (GetAttrClass obj) == "Gta Collision" then true else false
			#alllights: return (superclassof obj == Light)
			#maxLights : return (superclassof obj == Light) AND NOT (classof_array #(RageLight) obj)
			#RageLights : return (classof_array #(RageLight) obj)
			#AllHelpers : return ((superclassof obj == Helper) OR  (classof obj == Gta_MILOTri)) AND NOT ((GetAttrClass obj) == "Gta Collision")
			#milo : return (classof obj == Gta_MILO)
			#MiloPreview : return (classof obj == Gta_MILOTri)
			#MiloRoom : return  (classof obj == GtaMloRoom)
			#portals : return (classof obj == GtaMloPortal)
			#particles : return (classof obj == RsRageParticleHelper)
			#Audio : return (classof obj == GtaAudioEmit)
			#TcMods : return (classof_array #(GtaTimeCycle, GtaTimeCycleSphere) obj)
			#IntGroup : return (classof obj == InteriorGroupHelper)
			#SpawnPoint : return (classof obj == GtaSpawnPoint) 
			#DoNotExport : 
			(
				local doNotExport = readAttribute obj "Dont Export" undefined
				if (doNotExport == undefined) do return false
				return doNotExport
			)
		)
	),
	-------------------------------------------------------------------
	fn addChildrenArray parent =
	(
		debug_fn "addChildrenArray"
		local childArray = for i in parent.children collect i
		local NewArray =  childArray
		append NewArray parent 
		
		--local getChildren = true
		--if (vm.MiloRooms.Item[vm.SelectedMiloRoomIdx] == "Room 0") do getChildren = false
		
		for i in childArray do 
		(
			if NOT (classof_array # (GtaMloRoom) i) do for j in i.children do append NewArray j	
		)				
		return NewArray
	),
	-------------------------------------------------------------------
	fn DoCommand btnType MenuType =
	(		
		debug_fn "DoCommand"
		format "% % \n" btnType MenuType 

		local MiloRoom = vm.MiloRooms.Item[vm.SelectedMiloRoomIdx]		
		
		if (btnType == #AddRemove) then 
		(
			local parent
			case miloRoom of
			(
				"All" : parent = undefined
				"Room 0": parent = SelectedMilo			
				"World": parent = undefined
				Default: parent = getNodeByName MiloRoom
				
			)
			
			case MenuType of 
			(
				#add : for i in selection do i.parent = parent					
				#remove : for i in selection do i.parent = undefined		
				#SetDoNotExport : for i in selection do setAttribute i "Dont Export" true					
			)
		)
		else
		(
			local objArray
			case miloRoom of
			(
				"All" : objArray = $*
				"Room 0":  objArray = addChildrenArray SelectedMilo				
				"World": objArray =  for i in $* where i.parent == undefined collect i 
				Default: objArray = addChildrenArray (getNodeByName MiloRoom)		
				
			)
			
			local filteredArray = for child in objArray where (isType_bool child MenuType) collect child
			
			case btnType of
			(
				#select: select filteredArray						
				#show: unhide filteredArray		
				#hide:   hide filteredArray	
				#isolate: 
				(				
					hide $*
					unhide filteredArray					
				)			
			)
		)
	),
	-------------------------------------------------------------------
	fn MiloChanged = 
	(
		debug_fn "MiloChanged"
		initMiloRooms()
	),
	-------------------------------------------------------------------
	fn fileOpen = 
	(
		debug_fn "fileOpen"
		initMilo()
		initMiloRooms()
	),
	-------------------------------------------------------------------
	fn dispose = 
	(
		debug_fn "Dispose"
		if (MainWindow != undefined) do 
		(
			-- WINDOW POS 
			settings.wpf_windowPos mainWindow #set
			-- CLOSE AND DISPOSE THE WINDOW
			if (hasProperty MainWindow "Close") do MainWindow.Close()     
			MainWindow = undefined
		)
		execute ("InteriorToolkit = undefined")
		-- KILL CALLBACK 
		callbacks.removeScripts #filePostOpen id:#intToolKit
	),
	-------------------------------------------------------------------
	fn init =
	(
		debug_fn "init"
		-- LOAD ASSEMBLY
		dotnet.loadAssembly  (RsConfigGetToolsDir() + @"techart/dcc/3dsMax/RSG.TechArt.InteriorManager.dll")
		-- MAKE MAIN WINDOW INSTANCE 
		MainWindow = dotNetObject "RSG.TechArt.InteriorManager.MainWindow"        
		dn_window.setup MainWindow 
		MainWindow.Tag = dotnetmxsvalue this
		-- PARENTS THE WINDOW UNDER MAX, STOPS IT FROM DROPPING BEHIND
		MainWindow.ChangeOwner (DotNetObject "System.IntPtr" (Windows.GetMAXHWND()))		
		vm = MainWindow.DataContext	
			
		-- SHOW THE WINDOW
		MainWindow.show()
		-- WINDOW POS
		settings.wpf_windowPos mainWindow #get
			
		initMilo()
		initMiloRooms()	

		MainWindow.title = 	"Interior Manager - v1.0"		

		-- ADD CALL BACK 
		callbacks.addScript #filePostOpen "InteriorToolkit.fileOpen()" id:#intToolKit
	),
	-- EVENTS -----------------------------------------------------
	on create do init()            
)
InteriorToolkit = InteriorToolkit()