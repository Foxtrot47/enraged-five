------------------------------------------------------
-- RSREF_USAGE_UTILS
-- MATT HARRAD [13/11/11]
-- ROCKSTAR NORTH
-- A BUNCH OF DIFFERENT WAYS OF SEARCHING FOR WHERE RSREFS ARE CURRENTLY BEING USED

-- 19.07.2011 HARRAD 
-- Reworked the UI to be more compact as it was too confusing.
-- Auto update on the XML.
-- Added a report window instead of using the listener.
------------------------------------------------------
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

------------------------------------------------------
rsRefReport -- WINDOW
rsr_fn -- FUNCTIONS
------------------------------------------------------
-- STRUCTS
------------------------------------------------------
struct xmlRsRefInfo 
(
	objectName=undefined, fileName=undefined, miloName=undefined
)
struct xmlSceneInfo
(
	miloName=undefined, fileName=undefined, user=undefined
)
------------------------------------------------------
-- FUNCTIONS
------------------------------------------------------
struct rsRefReport_functions
(
	intSceneFiles = undefined,
	compactXmlPath = (theWildWest + "script/max/Rockstar_North/Int_RsRef.xml"),
	reportWindow = undefined,
	dash = "----------------------------------------------------------",
	--------------------------------------------
	-- FUNCTIONS
	--------------------------------------------
	fn get_niceName xmlFile =
	(
		shortName = ""
		tempArray = filterString xmlFile @"\ /" 
		niceName = tempArray[tempArray.count]
		niceName = substring niceName 1 (niceName.count-4)
		return niceName
	),
	-----------------------------------------
	fn getIntSceneInfo xmlFile =
	(
		rsRefArray = #() -- TO RETURN
		------------------------------------------------------
		xmlStream = xmlStreamHandler xmlFile:xmlFile
		xmlStream.open()	
		------------------------------------------------------	
		-- LOAD THE SCENE INFO
		------------------------------------------------------	
		sceneInfo = xmlSceneInfo() 
		------------------------------------------------------
		nodes = xmlStream.root.Selectnodes("/scene")
		sceneNode = nodes.itemof[0]		
		sceneInfo.filename = ""			
		try(sceneInfo.filename = (sceneNode.getAttribute "filename")) catch()
		sceneInfo.user = ""			
		try(sceneInfo.user = (sceneNode.getAttribute "user")) catch()
		------------------------------------------------------				
		nodes = xmlStream.root.Selectnodes("objects/object[@class = \"gta_milo\"]")	
		miloNode = nodes.itemof[0]
		sceneInfo.miloName = ""
		try(sceneInfo.miloName = (miloNode.getAttribute "name")) catch()
		------------------------------------------------------
		append rsRefArray sceneInfo		
		------------------------------------------------------
		-- LOAD THE RSREFS
		------------------------------------------------------	
		nodes = xmlStream.root.Selectnodes("objects/object[@class = \"gta_milo\"]/children/object[@class = \"gtamloroom\"]/children/object/paramblocks/*")		
		for i = 0 to nodes.count - 1 do
		(
			rsRefInfo = xmlRsRefInfo()
			------------------------------------------------------
			params = nodes.itemof[i]
			objNode = params.selectNodes("parameter[@name = \"objectName\"]")
			rsRefInfo.objectName = ""			
			try(rsRefInfo.objectName = (objNode.itemOf[0].getAttribute "value")) catch()
			------------------------------------------------------
			fileNode = params.selectNodes("parameter[@name = \"filename\"]")
			rsRefInfo.fileName = ""			
			try(rsRefInfo.fileName = (fileNode.itemOf[0].getAttribute "value")) catch()
			------------------------------------------------------
			append rsRefArray rsRefInfo
		)
		------------------------------------------------------		
		xmlStream.close()
		------------------------------------------------------
		return rsRefArray
	),
	fn makeCompactXml =
	(	
		uniDate = getUniversalTime() 
		theNewFile = createFile compactXmlPath
		------------------------------------------------------
		format "<?xml version=\"1.0\"?>\n" to:theNewFile
		format "<interiors date =\"%/%/%\">\n" uniDate[4] uniDate[2] uniDate[1] to:theNewFile
		count = 1
		for xmlFile in intSceneFiles do
		(
			rsRefReport.progBar.value=100.*count /intSceneFiles.count
			------------------------------------------------------
			sceneInfo = getIntSceneInfo xmlFile
			format "\t<scene miloName=\"%\" fileName=\"%\" user=\"%\">\n" sceneInfo[1].miloName sceneInfo[1].fileName sceneInfo[1].user to:theNewFile
			for i=2 to sceneInfo.count do
			(
				format "\t\t<rsRef objectName=\"%\" fileName=\"%\"/>\n" sceneInfo[i].objectName sceneInfo[i].filename to:theNewFile
			)				
			format "\t</scene>\n" to:theNewFile

			count += 1
		)
		format "</interiors>\n" to:theNewFile
		close theNewFile
		format "rsRef_Report: XML done!\n"
		rsRefReport.progBar.value = 0
	),
	-----------------------------------------
	fn propCount_list =
	(
		start = timeStamp()
		--------------------------------
		rsRefReport.lbx_objects.items =#() -- CLEAR THE OBJECT LIST 
		countValue = rsRefReport.spr_count.value
		sceneObjects = #()
		if (rsRefReport.cbx_count_useSel.state) then
		(
			for g in (selection as array) where (classof g == Editable_Poly OR classof g == Editable_Mesh) do append sceneObjects g.name
		)
		else
		(
			for g in geometry where (classof g == Editable_Poly OR classof g == Editable_Mesh) do append sceneObjects g.name	
		)
		
		-----------------------------------------
		xmlStream = xmlStreamHandler xmlFile:compactXmlPath
		xmlStream.open()
		------------------------------------------	
		rsRefNodes = xmlStream.root.selectNodes("/interiors/scene/rsRef")
		
		count = 1
		for qProp in sceneObjects do
		(
			rsRefReport.progBar.value=100.*count/sceneObjects.count			
			
			pCount = 0				
			
			for i = 0 to rsRefNodes.count - 1 do
			(
				rsRefNode = rsRefNodes.itemof[i]
				rsRefAttrValue= (rsRefNode.Attributes).ItemOf("objectName")											
				isMatch = matchPattern rsRefAttrValue.value pattern:( "*" +  qProp + "*") ignoreCase:true					
				if (isMatch) then pCount += 1
			)
			
			format "% [%]\n" qProp pCount
			count += 1
			
			if pCount == countValue then
			(						
				objectList = rsRefReport.lbx_objects.items
				append objectList qProp
				rsRefReport.lbx_objects.items =objectList  
			)
		)	
		-------------------------------------------
		xmlStream.close()
		-------------------------------------------
		end = timeStamp()
	--	format "-------------------------------------------\nCounting took % seconds\n" ((end - start) / 1000.0)
		rsRefReport.progBar.value = 0
	),
	-------------------------------------------
	fn query_list rsRefList =
	(
		start = timeStamp()
		--------------------------------
		xmlStream = xmlStreamHandler xmlFile:compactXmlPath
		xmlStream.open()
		------------------------------------------	
		rsRef_NodeList = xmlStream.root.selectNodes("/interiors/scene/rsRef")
		--------------------------------
		progBar_count = 1
		for rsRef_name in rsRefList do
		(
			isMatch_array = #()
			rsRefReport.progBar.value=100.*progBar_count/rsRefList.Count			
			rsr_fn.report_add (" RsRef: \""+rsRef_name+"\"\r\n")
			
			for i = 0 to rsRef_NodeList.Count - 1 do
			(				
				rsRefNode = rsRef_NodeList.itemof[i]
				rsRefAttrValue= (rsRefNode.Attributes).ItemOf("objectName")											
				isMatch = matchPattern rsRefAttrValue.value pattern:( "*" +  rsRef_name + "*") ignoreCase:true					
				if (isMatch) then 
				(
					parentNodeList = rsRefNode.selectNodes("..")
					parent = parentNodeList.itemof[0]
					parentMiloName = ((parent.Attributes).ItemOf("miloName")).value
					parentFileName =  ((parent.Attributes).ItemOf("fileName")).value					
					
					temp_info = xmlRsRefInfo miloName:parentMiloName fileName:parentFileName
					append isMatch_array temp_info
				)				
			)	
			---------------------------------------
			-- REPORT
			---------------------------------------
			uniqueMiloNames = #()	
			for xml_info in isMatch_array do appendifunique uniqueMiloNames xml_info.miloName			
			miloName_count = 0
			if (isMatch_array.count == 0) then rsr_fn.report_add " + Invalid name or its not used in any interior\r\n" 
			else
			(
				for miloName in uniqueMiloNames do
				(
					fileName
					for xml_info in isMatch_array do if (xml_info.miloName == miloName) do 
					(
						miloName_count += 1
						fileName = xml_info.fileName
					)
					--format "\t+ % (%) [%]\n" miloName (get_niceName fileName) miloName_count
					rsr_fn.report_add ("  + "+miloName+" ("+(get_niceName fileName)+"), Count: "+miloName_count as string+"\r\n" )
					miloName_count = 0
				)	
			)
			rsr_fn.report_add (dash+"\r\n")
			
			progBar_count += 1
		)	
		-------------------------------------------
		xmlStream.close()
		-------------------------------------------
		end = timeStamp()
		--format "% seconds\n" ((end - start) / 1000.0)
		rsRefReport.progBar.value=0
	),	
	------------------------------------------	
	fn queryScene sceneXml= 
	(
		rsRefs =#()
		if (sceneXml == "prologueint") then maxScene = (theProjectRoot + "art/models/_prologue/" + sceneXml + ".max")
		else maxScene = (theProjectRoot + "art/models/interiors/" + sceneXml + ".max")
		rsr_fn.report_add "" clear:true
		rsr_fn.report_add (dash + "\r\n Test by file\r\n"+ dash +"\r\n")
		rsr_fn.report_add (" File: "+maxScene+"\r\n" )
		------------------------------------------	
		xmlStream = xmlStreamHandler xmlFile:compactXmlPath
		xmlStream.open()
		------------------------------------------	
		sceneNodes = xmlStream.root.Selectnodes("/interiors/scene[@fileName=\"" + maxScene + "\"]") -- GET SCENE
		if (sceneNodes.count == 0) then
		(
			rsr_fn.report_add  " >> Error: Can't Find Scene?\r\n"
		)
		else
		(
			sceneNode = sceneNodes.itemof[0]
			nodeAtts = sceneNode.Attributes		
			user = nodeAtts.ItemOf("user")
			miloName =  nodeAtts.ItemOf("miloName")	
			------------------------------------------	
			rsr_fn.report_add  (" Scene: "+(get_niceName maxScene)+"("+miloName.value+")\r\n Last exported by: "+user.value+"\r\n"+dash+"\r\n") 
			------------------------------------------	
			rsRefNodes = sceneNode.selectNodes("rsRef")
			for i = 0 to rsRefNodes.count - 1 do
			(	
				rsRef = xmlRsRefInfo()
				------------------------------------------	
				rsRefNode = rsRefNodes.itemof[i]	
				paraAtts = rsRefNode.Attributes
				attValue= paraAtts.ItemOf("objectName")
				rsRef.objectName = attValue.value
				attValue= paraAtts.ItemOf("fileName")
				rsRef.fileName = attValue.value
				------------------------------------------	
				append rsRefs rsRef
			)
			------------------------------------------
			rsRefsUniqueNames = #()
			rsRefsUnique =#()
			for i in rsRefs do
			(
				if (findItem rsRefsUniqueNames i.objectName) == 0 do 
				(
					append rsRefsUniqueNames i.objectName
					append rsRefsUnique i
				)
			)
			------------------------------------------
			if rsRefs.count == 0 do rsr_fn.report_add " + There doesn't appear to be any rsRefs in this file!\r\n"
			------------------------------------------	
			for rsUni in rsRefsUnique do
			(
				if ( rsUni.fileName == "") then
				(
					rsr_fn.report_add ("\n **Warning, "+rsUni.objectName+" doesn't have a fileName!**\r\n\r\n") 
				)
				else
				(
					count = 0
					for rsRef in rsRefs do if rsUni.objectName == rsRef.objectName then count = count + 1
					rsr_fn.report_add (" + RsRef: "+ rsUni.objectName +" ("+(get_niceName rsUni.fileName)+"), Count:"+count as string+"\r\n") 
				)
			)
		)
		------------------------------------------
		xmlStream.close()
	),
	------------------------------------------	
	fn xmlDateUpdate =				
	(
		isXmlThere = doesFileExist rsr_fn.compactXmlPath
		if isXmlThere == false then
		(
			--rsRefReport.lbl_xmlDate.text  = "Warning: No XML File!"
			--rsRefReport.btn_makeXml.text = "Generate XML"
			rsr_fn.update_xml()
		)
		else
		(
			lastMod = filterString (getFileModDate rsr_fn.compactXmlPath) " "
			rsRefReport.lbl_xmlDate.text = ("Last Updated: " + lastMod[1])
		)
	),
	------------------------------------------	
	fn _init =
	(
		intSceneFiles = getFiles (theProjectAssetRoot + "export/Levels/gta5/Interiors/*.xml")
		append intSceneFiles (theProjectAssetRoot + "export/Levels/gta5/_prologue/prologueint.xml")
	),
	------------------------------------------
	fn update_xml =
	(
		setWaitCursor()
		if (gRSperforce.connected()) do
		(
			for i in rsr_fn.intSceneFiles do gRSperforce.sync i
		)
		rsr_fn.makeCompactXml()
		rsr_fn.xmlDateUpdate()		
		setArrowCursor()
	),
	------------------------------------------	
	fn report_add theString clear:false =
	(
		if ( rsr_fn.reportWindow != undefined) do
		(
			if clear then rsr_fn.reportWindow.dn_reportTextbox.text = ""
			else 	rsr_fn.reportWindow.dn_reportTextbox.text += theString
		)
	),
	------------------------------------------	
	-- UI
	------------------------------------------	
	fn makeUI =
	(
		try (destroyDialog rsRefReport) catch()
		rollout rsRefReport "rsRef Report" width:200
		(
			dotNetControl rsBannerPanel "Panel"	height:32 pos:[0,0] width:rsRefReport.width
			local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"RsRef_Report" filename:(getThisScriptFilename())
			
			group "Update XML"
			(
				label lbl_xmlDate "Last Updated:" 
				button btn_makeXml "Update XML File" width:175 
			)
			group "Per Object Report"
			(
				radiobuttons rdb_selName labels:#("Selection", "Name")
				editText edt_name "Name:" enabled:false
				button btn_querySel "Print Report" width:175
			)
			group "Per Interior Report"
			(
				dropdownlist ddl_xmlFiles "" width:175 
				button btn_queryScene "Print Report" width:175
			)
-- 			group "Selection"
-- 			(
-- 				label lb_1 "Print a report on the usage of the" align:#left
-- 				label lb_2 "selected rsRef in the interior files." align:#left offset:[0,-5]
-- 				button btn_querySel "Test Selection" width:175
-- 			)
-- 			group "Name"
-- 			(		
-- 				label lb_3 "Print a report on the usage of a rsRef " align:#left
-- 				label lb_4 "in the interior files from its name." align:#left offset:[0,-5]
-- 				editText edt_name "Name:" 
-- 				button btn_queryName "Test Name"width:175
-- 			)
-- 			
-- 			group "Test By Count (Current File)"
-- 			(
-- 				checkbox cbx_count_useSel "Only Use Selected Objects" checked:true
-- 				spinner spr_count "Count To List:" range:[0,1000,0] type:#integer 
-- 				button btn_queryCount "Test By Count" width:175
-- 				listbox lbx_objects "" height:15
-- 			)

			progressbar progBar color:red width:190 height:20 offset:[-8,0]
			------------------------------------------------------	
			-- EVENTS
			------------------------------------------------------	
			on btn_makeXml pressed do 
			(
				rsr_fn.update_xml()
			)
			-----------------------------------------------------
			on rdb_selName changed arg do
			(
				case arg of
				(
					1:
					(
						edt_name.enabled = false
						edt_name.text = ""
					)
					2:
					(
						edt_name.enabled = true
					)
				)

			)
			------------------------------------------------------	
			on btn_querySel pressed do
			(
				rsr_fn.makeReportUi()
				case (rdb_selName.state) of 
				(
					1: -- TEST BY SELECTION
					(
						rsRefList = #()
						for i in selection as array do 
						(
							if classOf i == Editable_Mesh or classOf i == Editable_Poly do append rsRefList i.name	
							if classOf i == RSrefObject do append rsRefList  i.objectName
						)	
						rsr_fn.report_add "" clear:true
						rsr_fn.report_add (rsr_fn.dash+"\r\n Test by selection\r\n"+rsr_fn.dash+"\r\n")	
					)
					2: -- TEST BY NAME
					(
						rsRefList = #(rsRefReport.edt_name.text)
						rsr_fn.report_add "" clear:true
						rsr_fn.report_add (rsr_fn.dash+"\r\n Test by name\r\n"+rsr_fn.dash+"\r\n")
					)
				)	
				------------------------------------			
				rsr_fn.query_list rsRefList		
				progBar.value = 0				
			)			
			------------------------------------------------------	
			on btn_queryScene pressed do 
			(
				rsr_fn.makeReportUi()
				rsr_fn.queryScene rsRefReport.ddl_xmlFiles.selected
			)
			------------------------------------------------------
			on lbx_objects selected arg do 
			(
				select (getnodebyname (lbx_objects.items[arg]))
			)
			------------------------------------------------------
			on btn_queryCount pressed do
			(
				rsr_fn.propCount_list()				
			)
			------------------------------------------------------	
			on rsRefReport open do
			(
				rs_dialogPosition "get" rsRefReport		
				banner.setup() 
				rsr_fn._init()
				rsr_fn.xmlDateUpdate()
				------------------------------------------------------	
				xmlList = #()
				for i in rsr_fn.intSceneFiles do append xmlList (rsr_fn.get_niceName i)
				ddl_xmlFiles.items = xmlList
				clearlistener()
			)		
			on rsRefReport close do
			(
				try destroyDialog rsr_fn.reportWindow catch()
				rs_dialogPosition "set" rsRefReport		
			)
		)
	),	
	fn makeReportUi =
	(
		if rsr_fn.reportWindow == undefined do
		(
			rsr_fn.reportWindow = rollout reportWindow "RsRef Report" width:450 height:500
			(
				dotNetControl dn_reportTextbox "System.Windows.Forms.Textbox" width:440 height:490 align:#center
				
				on reportWindow open do
				(
					rs_dialogPosition "get" reportWindow	
					
					dn_reportTextbox.ReadOnly = true
					dn_reportTextbox.Font = dotNetObject "System.Drawing.Font" "Courier New" 9 ((dotNetClass "System.Drawing.FontStyle").Regular)
					dn_reportTextbox.BorderStyle = (dotNetClass "System.Windows.Forms.BorderStyle").FixedSingle
					dn_reportTextbox.BackColor = (dotNetClass "System.Drawing.Color").fromARGB 255 255 255
					dn_reportTextbox.ForeColor = (dotNetClass "System.Drawing.Color").fromARGB 0 0 0
					dn_reportTextbox.MultiLine = true
					dn_reportTextbox.WordWrap = true
					dn_reportTextbox.ScrollBars = (dotNetClass "System.Windows.Forms.ScrollBars").Vertical
					dn_reportTextbox.Text = ""			
				)
				on reportWindow close do 
				(
					rsr_fn.reportWindow = undefined
					rs_dialogPosition "set" reportWindow	
				)
			)
			createDialog reportWindow style:#(#style_border,#style_toolwindow,#style_sysmenu)
		)
	)
)
------------------------------------------------------
rsr_fn = rsRefReport_functions()
rsr_fn.makeUI()
createDialog rsRefReport style:#(#style_border,#style_toolwindow,#style_sysmenu)
