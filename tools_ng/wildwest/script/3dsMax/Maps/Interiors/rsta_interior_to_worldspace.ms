---------------------------------------------------------------------------
-- FILE IN 
---------------------------------------------------------------------------
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_XML.ms")
---------------------------------------------------------------------------
-- ROLLOUT
---------------------------------------------------------------------------
try (destroyDialog  intWS_roll) catch()
rollout intWS_roll "Interior To World Space" width:200
(
	-- LOCALS --------------------------------------------------------
	local milo_xml = (RsConfigGetWildWestDir() + "etc/config/maps/MiloInstances.xml")	
	local xml_io = rsta_xml_io xmlFile:milo_xml
	local instance_array = #()
	local current_instance = undefined
	local current_offsets = undefined
	-- BANNER --------------------------------------------------------
	dotNetControl rsBannerPanel "System.Windows.Forms.Panel" height:32 pos:[0,0]  width:intWS_roll.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel versionNum:1.1 versionName:"Shy Ham" wiki:"InteriorToWorldSpace" filename:(getThisScriptFilename())
	---------------------------------------------------------------------------
	-- CONTROLS 
	---------------------------------------------------------------------------
	group "Source "
	(
		label lb_01 "Milo Name:" across:2 offset:[-18,3]
		dropdownlist ddl_milo "" width:120 offset:[-30,0] toolTip:"Milo to offset from"
		label lb_02 "Instance:" across:2 offset:[-14,3]
		dropdownlist ddl_instance "" width:120 offset:[-30,0] toolTip:"Instance of milo in game"
	)
	group "Selection In World Space "
	(
		edittext edt_x "" width:60 across:3 offset:[-5,0] readOnly:True toolTip:"X value of selected object in World Space"
		edittext edt_y "" width:60 offset:[-2,0] readOnly:True toolTip:"Y value of selected object in World Space"
		edittext edt_z "" width:60 offset:[2,0] readOnly:True toolTip:"Z value of selected object in World Space"
		button btn_copy "Copy" width:88 across:2 offset:[-3,0] toolTip:"Copy XYZ value to clipboard"
		button btn_warp "Warp" width:88 offset:[5,0] toolTip:"Warp to XYZ value in game (if connected)"
	)
	group "Milo In World Space "
	(
		button btn_makeMilo "Create Milo Preview" width:180
	)
	---------------------------------------------------------------------------
	-- FUNCTIONS
	---------------------------------------------------------------------------
	fn makeXml =
	(		
		local cache_xml = @"T:\SceneXmlCache\GTAV_Cache.xml"		
		local xml_io_cache = rsta_xml_io xmlFile:cache_xml
		local xml_io_milos = rsta_xml_io()
		xml_io_milos.new "Milos"
		
		for rsRef_xn in rsta_xml.getChildren xml_io_cache.root name:"File/Container/RsRefList/RsRef" do
		(
			local rsname = rsRef_xn.getAttribute "name"
			local cName = rsRef_xn.ParentNode.ParentNode.getAttribute "name"
			if (matchPattern rsname pattern:"*_milo_" ignoreCase:true) do
			(
				rsname = substituteString (toLower rsname) "_milo_" ""			
				local milo_xn = rsta_xml.makeNode "Milo" xml_io_milos.root attr:"name" value:rsname unique:true
				
				for instance_xn in rsta_xml.getChildren rsRef_xn name:"Instance" do
				(
					local i_xn = rsta_xml.makeNode "Instance" milo_xn attr:"container" value:cName
					-- POSITION
					pos_xn = rsta_xml.makeNode "Position" i_xn 
					pos_xn.SetAttribute "x" (instance_xn.getAttribute "pX")
					pos_xn.SetAttribute "y" (instance_xn.getAttribute "pY")
					pos_xn.SetAttribute "z" (instance_xn.getAttribute "pZ")
					-- ROTATION
					local q = (quat ((instance_xn.getAttribute "rX") as float) 	((instance_xn.getAttribute "rY") as float) 	((instance_xn.getAttribute "rZ") as float) 	((instance_xn.getAttribute "rW") as float))
					rsta_xml.makeNode_quat "Rotation" q i_xn 					
				)			
			)		
		)		
		xml_io_milos.saveAs milo_xml saveWarning:false
	)
	---------------------------------------------------------------------------
	fn isValid =
	(
		if (selection.count == 0) do return false	
		if (current_instance == undefined) do return false
		if not(classof_array #(Editable_Poly, Editable_Mesh, RSrefObject) selection[1]) do return false
		return true
	)
	---------------------------------------------------------------------------
	fn set_milo_dropdown =
	(
		-- SET FROM MILO
		ddl_milo.items = for h in helpers where classof h == Gta_MILO collect h.name
		
		-- SET FROM RSREF
		if ddl_milo.items.count == 0 do 
		(
			local all_milos = #()
			all_milos = for milo_xn in (rsta_xml.GetChildren xml_io.root name:"Milo") collect (milo_xn.getAttribute "name")
			sort all_milos
			ddl_milo.items = all_milos
		)
	)
	---------------------------------------------------------------------------
	fn get_offset  =
	(
		local obj = selection[1]
		milo_node = getnodebyname ddl_milo.selected	
		if (milo_node != undefined) do
		(		
			-- MILO TRANSLATION WS
			rsMilo_tm = transMatrix current_offsets.pos
			-- MILO ROTATION WS
			rsMilo_rm = current_offsets.rot as matrix3
			-- LOCAL PROP OFFSET
			propOffset = obj.pos - milo_node.pos 
			propOffset_tm = transMatrix propOffset
			-- ADD THEM ALL LUP
			total_tm = propOffset_tm * rsMilo_rm * rsMilo_tm 		

			return total_tm.translationpart
		)
		return [0,0,0]
	)
	---------------------------------------------------------------------------
	fn set_WS_values =
	(
		if (isValid()) do
		(
			offset_xyz = get_offset()			

			edt_x.text = offset_xyz[1] as string 
			edt_y.text = offset_xyz[2] as string 
			edt_z.text = offset_xyz[3] as string 
		)
	)
	---------------------------------------------------------------------------
	fn set_current_instance =
	(
		if (ddl_instance.items.count != 0) then 
		(
			current_instance = instance_array[ddl_instance.selection].node
			local pos = rsta_xml.get_point3 (current_instance.selectSingleNode "Position")
			local rot = rsta_xml.get_quat (current_instance.selectSingleNode "Rotation")
			
			current_offsets = dataPair pos:pos rot:rot
			set_WS_values()
		)
		else current_instance = undefined
	)
	---------------------------------------------------------------------------
	fn set_instance_dropdown =
	(
		instance_array = #()
		local selectedMiloName = ddl_milo.selected
		if (selectedMiloName != undefined) do
		(
			local milo_xn = xml_io.root.selectSingleNode ("Milo[@name=\"" + (toLower selectedMiloName) + "\"]")		
			
			if (milo_xn != undefined) then
			(
				for inst_xn in rsta_xml.getChildren milo_xn name:"Instance" do
				(
					append instance_array (dataPair name:(inst_xn.getAttribute "container") node:inst_xn)
				)
				
				ddl_instance.items = (for i in instance_array collect i.name)	
				set_current_instance()
			)	
		)		
	)
	---------------------------------------------------------------------------
	fn get_formated_XYZ = return (edt_x.text + " " + edt_y.text  + " " + edt_z.text)	
	---------------------------------------------------------------------------
	fn make_milo_preview =
	(
		local rsRef = RSrefObject ObjectName:(ddl_milo.selected + "_milo_") 
		rsRef.rotation = current_offsets.rot
		rsRef.pos =current_offsets.pos
		select rsRef
		max zoomext sel all
	)
	---------------------------------------------------------------------------
	
	-- EVENTS 
	---------------------------------------------------------------------------
	on btn_copy pressed do setclipboardText (get_formated_XYZ())
	---------------------------------------------------------------------------
	on btn_warp pressed do 
	(
		if RemoteConnection.IsConnected() do 
		(
			RemoteConnection.WriteStringWidget "Debug/Warp Player x y z h vx vy vz" (get_formated_XYZ())
		)		
	)
	---------------------------------------------------------------------------
	on ddl_milo selected arg do set_instance_dropdown()
	---------------------------------------------------------------------------
	on ddl_instance selected arg do set_current_instance()	
	---------------------------------------------------------------------------
	on btn_makeMilo pressed do make_milo_preview()
	---------------------------------------------------------------------------
	on intWS_roll open do
	(	
		rs_dialogPosition "get" intWS_roll
		banner.setup()
		-- SET UP 
		set_milo_dropdown()
		set_instance_dropdown()
		set_WS_values()
		-- CALLBACKS 
		callbacks.addScript #selectionSetChanged "intWS_roll.set_WS_values()" id:#intWS_callBacks
		-- RAG CONNECT  
		RemoteConnection.Connect()
	)
	---------------------------------------------------------------------------
	on intWS_roll close do
	(
		rs_dialogPosition "set" intWS_roll
		callbacks.removeScripts id:#intWS_callBacks
		RemoteConnection.Disconnect()
	)
)
createDialog intWS_roll style:#(#style_titlebar, #style_border, #style_sysmenu,#style_toolwindow)