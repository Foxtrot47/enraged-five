
(
		
	
	rollout TreeHumper_roll "Tree Humper"
	(
		--variables
		global selset
		global facSet
		global CenVertSet
		
		--ui
		spinner Outer_Hump "Outer Push" range:[0, 100, 0.25]
		spinner Inner_Hump "Inner Push" range:[0, 100, 0.75]
		spinner Rand_Move "Rand Rad" range:[0, 100, 3.0]
		spinner Rand_Slide "slide cut" range:[0, 100, 0] type: #integer
		button Hump_it "Hump it"
		
		fn HumpTheMesh theObject rMove rIPush rOPush rSlide =
		(
			theObject.tesselateBy = 1
			polyOp.tessellateByFace theObject facSet
			--refresh face selection as new faces made
			facSet = polyOp.getfaceSelection theObject
			format "facSet:%\n" facSet
	
			CenVertSet = polyOp.getVertsUsedOnlyByFaces theObject facSet

			format "CENVERTSET:%\n" CenVertSet
			
			vertOffsetList = #()
			
			for n = 1 to (CenVertSet as array).count do
			(
				
				--randomize vert position
				RandRad = random 0.0 rMove
				format " RandRad:%\n" RandRad

				randRot = random 1.0 359.0
				format " RandRot:%\n" RandRot

				xMov = (cos randRot) * RandRad
				yMov = (sin randRot) * RandRad
				format "xmov, ymov % %\n" xMov yMov
				
				tempP3 = [xMov, yMov, rIPush]
				
				append vertOffsetList tempP3
				
				
							
			)
			
			polyOp.moveVert theObject CenVertSet vertOffsetList
			
			polyOp.setVertSelection theObject CenVertSet
			
			crossEdges = polyOp.getEdgesUsingVert theObject CenVertSet
			
			polyOp.setEdgeSelection theObject crossEdges
			
			subobjectlevel = 2
			
			theObject.connectEdgeSlide = rSlide
			
			theObject.EditablePoly.ConnectEdges ()
			
			newEdges = polyOp.getEdgeSelection theObject
			
			newEdgeVerts = polyOp.getVertsUsingEdge theObject newEdges
			
			rOPush = rOPush - ( rIPush/2 )
			polyOp.moveVert theObject newEdgeVerts [0,0,rOPush]
			
			subobjectlevel = 1
			
			polyOp.setVertSelection theObject CenVertSet
			
			
			
			
		)
		
		on Hump_it pressed do
		(
			gc()
			selset = getCurrentSelection()
			
			for n = 1 to selset.count do
			(
				if classOf selSet[n] == editable_Poly then
				(
					facSet = polyOp.getfaceSelection selSet[n]
					
					format " faces:%\n" facSet
					
					HumpTheMesh selSet[n] Rand_Move.value Inner_Hump.value Outer_Hump.value Rand_Slide.value
				)
				
			)
			
				
			
		)
		

	
	)

	if TreeHumper_float != undefined then closerolloutfloater TreeHumper_float
	TreeHumper_float = newrolloutfloater "Tree Humper" 130 140 100 100
	addrollout TreeHumper_roll TreeHumper_float

) 