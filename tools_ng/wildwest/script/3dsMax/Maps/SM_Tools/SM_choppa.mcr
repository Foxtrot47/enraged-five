--This tool is obsolete and the functionality added to the modelling toolkit
--This script now informs the user and redirects them there, automatically launching the modelling toolkit
(
	messagebox "This functionality is now accessed from the modelling toolkit\nLook for chop into blocks under the Divide Model section." Title:"Redirect"
	filein (RsConfigGetWildWestDir() + "/script/3dsmax/Modelling/modellingToolkit.ms")

)