--SM_Path_Deform
--
-- Stuart Macdonald
--
-- v1.0 11/03/10
--
-- v1.1 10/11/10
-- Added spacing function for placement
-- Fixed placement fo first object in array
--
-- to do
-- 
macroScript SM_Path_Deform category:"SM_Tools"
(	
	
	global thisFloat

	rollout Roll_Path_Deform "Path Deform"
	(
		--Globals
		global pickCheck = 0
		global thePolyObj
		global theSplineObj
		global objLength
		global splLength
		
		fn FilterPoly obj = classof obj==Editable_Poly
		fn FilterXRef obj = classof obj==XrefObject
		--fn FilterSpline obj = classof obj==Line

		hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Path_Deform" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

		
		
		--UI
		group "setup"
		(
			pickbutton pb_pickPoly "Pick object" message:"Pick" filter:FilterPoly autoDisplay:true
			checkbox ch_useCopy "use copy" enabled:true checked:false
			pickbutton pb_pickSpline "Pick path" message:"Pick" autoDisplay:true
		)
		checkbox ch_TrimObj  "trim Object" enabled:true checked:false
		group "array"
		(
			checkbox ch_tileObj "Array Obj" enabled:true checked:false
			--label lb_spacing "Spacing" align: #left
			--spinner sp_tileSpace "" range:[0.0,100.0, 0.0] align: #left fieldwidth: 40 type: #float
			checkbox ch_disableDeform "No Deform" enabled:true checked:false
			checkbox ch_useXref "use Xref" enabled:false checked:false
			pickbutton pb_pickXRef "Pick Xref" message:"Pick" filter:FilterXref autoDisplay:true enabled:false
			checkbox ch_useXrefXAxis "Use X-axis" enabled:false checked:false
			label lb_spacing "Spacing"
			spinner sp_objoffset "" align:#right width:60 range:[0.0,99.99,0.0] type:#float
		)
		
		button go_deform "GO" width:60 height:40  enabled:false
		progressBar prog_Deform value:0
		--Functions
		fn goDeformObject objOffset =
		(
			trimVertList = #()
			
			startPoint = thePolyObj.pos
			
			numVerts = polyOp.getNumVerts thePolyObj
			
			-- below moved to go event
			--get spline length
			--segLengths = getSegLengths theSplineObj 1
			--format "\n segLengths: %" segLengths
			--splLength = segLengths[segLengths.count]
			--format "\n splLength: %" splLength
			
			for v = 1 to numVerts do
			(
				
				vertPos = polyOp.getVert thePolyObj v
				
				vOffset = vertPos - startPoint
				--format "\n vOffset  %" vOffset
				
				--get proportion along obj length
				--vlengthPos = ((startPoint.y - vertPos.y)/objLength) as float
				vlength = abs ( startPoint.y - vertPos.y ) + objOffset
				
				vlengthPos = (startPoint.y - vertPos.y) + objOffset
				--vlengthPos = (vlengthPos/objLength) as float
				vlengthPos = abs (vlengthPos/splLength) as float
				
				--format "\n objlength  %" objLength
				--format "\n vlength pos  %" vlengthPos
				if vlengthPos <= 1 then 
				( 
					spPos = interpCurve3D theSplineObj 1 vlengthPos
					spTangent = tangentCurve3D theSplineObj 1 vlengthPos
					--format "\n position %" spPos
					--format "\n tangent % = %" v spTangent
				) else
				(
					spPos = interpCurve3D theSplineObj 1 1.0
					spTangent = tangentCurve3D theSplineObj 1 1.0
					spPos = spPos + (spTangent * abs (vLength - splLength) )
					
				)
				
				perpVector = cross spTangent [0,0,1]
				perpVector = normalize [perpVector.x, perpVector.y, 0]
				
				nvPos = spPos + (vOffset.x * perpVector)
				--nvPos.x = spPos.x + (vOffset.x * spTangent.x)
				--nvPos.y = spPos.y + (vOffset.x * spTangent.y)
				nvPos.z = spPos.z + vOffset.z
				
				polyOp.setvert thePolyObj v nvPos
				
				if vlengthPos > 1 then 
				(
						append trimVertList v
					
				)
				
				prog_Deform.value = 100 * v/numVerts
				
			)
				if ch_trimObj.checked == true then
				(
					polyOp.deleteVerts thePolyObj (trimVertList as bitarray )
					thePolyObj.deleteIsoVerts()
					if polyOp.getNumFaces == 0 then delete thePolyObj
				)
			
		)
			
		fn goArrayObject objOffset =
		(
				trimVertList = #()
				
				startPoint = thePolyObj.pos

					--vertPos = polyOp.getVert thePolyObj v
					
					offsetRatio = abs (objOffset/splLength) as float
					
					--vlength = abs ( startPoint.y - vertPos.y ) + objOffset
					
					--vlengthPos = (startPoint.y - vertPos.y) + objOffset
					
					--vlengthPos = abs (vlengthPos/splLength) as float
					
					
					if offsetRatio <= 1 then 
					( 
						spPos = interpCurve3D theSplineObj 1 offsetRatio
						spTangent = tangentCurve3D theSplineObj 1 offsetRatio
					
					) else
					(
						spPos = interpCurve3D theSplineObj 1 1.0
						spTangent = tangentCurve3D theSplineObj 1 1.0
						spPos = spPos + (spTangent * abs (objOffset - splLength) )
						
					)
					
					perpVector = cross spTangent [0,0,1]
					perpVector = normalize [perpVector.x, perpVector.y, 0]
					
					thePolyObj.pos = spPos
					
					--create transform matrix for object
					theMatrix = matrix3 perpVector spTangent [0,0,1] spPos
					
					thePolyObj.transform = theMatrix
					
					
					
	
					--prog_Deform.value = 100 * v/numVerts

		)
		
		fn getPolyObjLength =
		(
		
			polyBB = nodeLocalBoundingBox thePolyObj
			
			return (polyBB[1].y - polyBB[2].y)
			
		)
		
		--Events
		on pb_pickPoly picked obj do
		(
			
			thePolyObj = obj

			pickCheck += 1
			if pickCheck > 1 then(
				go_deform.enabled = true
				pickCheck = 2 )
		)
		
		on pb_pickSpline picked obj do
		(
			if Classof obj ==Line then (
				theSplineObj = obj
				pickCheck += 1
			) else
				if classof obj == SplineShape then
				(					
					theSplineObj = obj
					pickCheck += 1
				) else
					if classof obj == Editable_Spline then
					(					
						theSplineObj = obj
						pickCheck += 1
					)
			
			
			
			
			if pickCheck > 1 then(
				go_deform.enabled = true
				pickCheck = 2 )
		) 
		
		on ch_disableDeform changed state do
		(
			
				if ch_disableDeform.checked == true then
				(
						ch_useXref.enabled = true
						pb_pickXRef.enabled = true
						ch_useXrefXAxis.enabled = true
					
				) else (
						ch_useXref.enabled = false
						pb_pickXRef.enabled = false
						ch_useXrefXAxis.enabled = false
					
				)	
					
			
		)
		
		on go_deform pressed do
		(
			
			
			if ch_tileObj.checked != true then
			(
				-- check duplicate ref mesh not required
				if ch_useCopy.checked == true then thePolyObj = copy pb_pickPoly.object
				
				--move object to spline start loc
				startLoc = interpCurve3d theSplineObj 1 0.0
				thePolyObj.pos = startLoc
				objLength = getPolyObjLength()
				
				--get spline length
				segLengths = getSegLengths theSplineObj 1
				--format "\n segLengths: %" segLengths
				splLength = segLengths[segLengths.count]
				--format "\n splLength: %" splLength
				
				goDeformObject 0
			) 
			else
			(
				thePolyObj = copy pb_pickPoly.object
				--move object to spline start loc
				startLoc = interpCurve3d theSplineObj 1 0.0
				thePolyObj.pos = startLoc
				objLength = getPolyObjLength()
				
				--get spline length
				segLengths = getSegLengths theSplineObj 1
				--format "\n segLengths: %" segLengths
				splLength = segLengths[segLengths.count]
				--format "\n splLength: %" splLength
				--format "\n objLength: %" objLength
				objLength = objLength + sp_objoffset.value
				--format "\n objLengths + offset: %" objLength
				
				numSteps = (abs(splLength/objLength)) + 1
						
				
				
				for n = 1 to numSteps do
				(
					
					objStep = (n-1) * objLength
					
					if ch_disableDeform.checked == false then
					(
						
						goDeformObject objStep
						
					) else 
					(
						if ch_useXref.enabled == true then
						(
								
						)
						goArrayObject objStep
					)
					
					thePolyObj = copy pb_pickPoly.object
					
					
					
					
				)
				
				delete thePolyObj
			)
				
			
		)
		
	
	
	)
	
	-- rollout and floater generation
	if thisFloat != undefined then closerolloutfloater thisFloat
	thisFloat = newrolloutfloater "Path Deform" 100 408 100 100
	addrollout Roll_Path_Deform thisFloat
)
