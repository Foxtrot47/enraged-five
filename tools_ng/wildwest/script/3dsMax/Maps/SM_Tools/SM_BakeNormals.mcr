--optimized version reworked to use voxels to speed up hit test




	global Bake_Normals_float
	global noHit = [0,0,0]

	rollout Bake_Normals_roll "SM_Bake Normals"
	(

	 
	 
	 pickbutton pick "Pick Reference Object" width:190
	 pickbutton pickTarget "Select Target" enabled:false width:190
	 button goBake "Go Bake" enabled:false width:180 height:48
	 
	
	 
	 
	 label label "Current Vert:"
	 --progressbar progress_vert offset:[8,0] color:(color 0 100 240) 
	  
	 
	--Functions
	fn voxelHit tarObj srcObj v =
	(
			--uses voxel grid accelleration to find best face normal
			
			
			rm = RayMeshGridintersect()
			rm.Initialize 1000
			rm.addnode srcObj
			rm.buildGrid()
		
			tarCen = srcObj.pos
		

			vPos = polyOp.getVert tarObj v
			vVect = normalize ( vPos - tarCen )
			
			hitCount = rm.intersectRay vPos vVect true
		
			format "\n hitCount %" hitCount
		
			if hitCount > 0 then
			(
					
				bestHit = rm.getClosestHit()
					
				newNormal = rm.getHitNormal bestHit
					
				return newNormal
					
			) else return noHit
				
			
		
	)

	
	on pick picked refmesh do (
		
			 pick.text = "Ref - " + refmesh.name
			 pickTarget.enabled = true
			 
			 global refObject = pick.object
			 
	)


	on pickTarget picked tarMesh do (
		
		global targetObj = tarMesh
		pickTarget.text = "Target - " + targetObj.name
		
		global targetVertList = (polyOp.getVertSelection tarMesh) as array
		format "\n selected vert list: %" targetVertList
		print targetVertList.count
		print tarMesh
				
		goBake.enabled = true
		
	)

	on goBake pressed do
	(	 
		
		gc()
		
		label.caption = "Current Vert:"
		
		
    	--disableSceneRedraw()

		max modify mode

		addmodifier targetObj (editnormals())
		
		
		--try(modPanel.setCurrentObject targetObj.edit_normals) catch (addmodifier targetObj (editnormals()))

		targetMod = targetObj.edit_normals.EditNormalsMod
		
		---------------------------------------------------
		print targetVertList.count
		For x=1 to targetVertList.count do 
		(    	    
			
			targetMod.Unify ()
			targetMod.MakeExplicit()
			vert_array = #{targetVertList[x]}
			normal_array = #{}
			targetObj.Edit_Normals.ConvertVertexSelection &vert_array &normal_array
			normal_result = normal_array as array
			
			normalResult = -(voxelHit targetObj refObject x)
			
			if normalResult != noHit then (
				
				try (
						targetMod.Setnormal normal_result[1] normalResult
						targetMod.SetNormalExplicit normal_result[1] true
						format "\n normal applied % \n" normal_result[1]
					) catch ()
				)
			label.caption = "Current Vert:" + x as string + "/" + targetObj.numverts as string
		 
		)


		--subobjectlevel = 0
		
		--double check make explicit is on, then turn any subobject selection off in mesh
		vert_array = targetVertList as bitarray
		normal_array = #{}
		targetObj.Edit_Normals.ConvertVertexSelection &vert_array &normal_array
		targetMod.SetSelection normal_array
		targetMod.MakeExplicit()
		modPanel.setCurrentObject targetObj.baseObject
		subobjectlevel = 0
	 	modPanel.setCurrentObject targetObj.edit_normals

		--label.caption = "Done"
	
	
       --enableSceneRedraw() 
	   
	   gc()
	-- end try
	


	) -- end button

	) -- end rollout

	

	if Bake_Normals_float != undefined then closerolloutfloater Bake_Normals_float
	Bake_Normals_float = newrolloutfloater "SM_Bake_Normals" 200 160 100 100
	addrollout Bake_Normals_roll Bake_Normals_float






