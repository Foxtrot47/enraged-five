-- 26/11/08 Quick script to bung objects at knots on a spline



(
		
	global SM_SplinePlanter_float	

	rollout SM_SplinePlanter_roll "Spline Planter"
	(
		--variables
		
		
		pickButton pick_spline "Pick Spline" width:160
		button goPlant "go"
		
		on pick_spline picked obj do
		(
			
			if obj != undefined do
			(
				global sp_object = obj
				pick_spline.text = obj.name
			)	
			
		)
		
		on goPlant pressed do
		(
		
print "go"			
			selset = getcurrentSelection()
			nSelset = selset.count
			
			
			if sp_object != undefined then
			(

				if superclassof sp_object == Shape then
				(
					nKnots = numKnots sp_object 1
					for n = 1 to nKnots do
					(
						Kpos = getKnotPoint sp_object 1 n
						
						for z=1 to nSelset do
						(
							
								obj = copy selset[z]
							
								obj.pos = Kpos
							
						)
						
						
					)
					
					
				)

			)			
		)

	
	)

	if SM_SplinePlanter_float != undefined then closerolloutfloater SM_SplinePlanter_float
	SM_SplinePlanter_float = newrolloutfloater "Spline Planter" 110 90 100 100
	addrollout SM_SplinePlanter_roll SM_SplinePlanter_float

) 