
(
	
	fileIn "SMLedge.ms"
	/*
	global MakeLedge_Float

	rollout Roll_MakeLedge"title"
	(
	
		-- ////////////////////////////////////////
		-- UI
		-- ////////////////////////////////////////
		
		button chk_start "Generate Route"
		
		
		
		-- ///////////////////////////////////////
		-- Methods
		-- ///////////////////////////////////////
		tool findEdge
		(
			on mousePoint clickno do
			(
				if snapmode.hit then
				(
					
						format "\nobject:%" snapmode.node
						format "\nsnap pos:%" snapmode.refpoint
						format "\nsnap node relative pos:%" snapmode.hitpoint
						objHit = snapmode.node
						snapPos = snapmode.hitpoint
						
					
					
						b = box()
						b.length = b.height = b.width = 1
						in coordsys objHit b.pos = snapPos

					
				)
				
				
			)



		)
		
		fn findTopFace obj thisEdge =
		(
			
			faceList = (polyOp.getEdgeFaces obj thisEdge) as array
			
			useThisFace = faceList[1]
			faceNorm = polyOp.getFaceNormal obj faceList[1]
			
			
			
			if faceList.count > 1 then
			(
					faceNorm2 = polyOp.getFaceNormal obj faceList[2]
					
					if faceNorm2.z < faceNorm.z then
					(
						faceNorm = faceNorm2
						useThisFace = faceList[2]	
					)
					
			)
			
			return useThisFace
			
		)
		
		-- ///////////////////////////////////////
		-- Events
		-- ///////////////////////////////////////
		
		on chk_start pressed do
		(
			-- set up snap
			-- Start Mousetool
			--starttool findEdge
			
			selset = getcurrentselection()
			
			for n = 1 to selset.count do
			(
				
				if classof selset[n] == editable_poly then
				(
					
					edgeList = (polyOp.getEdgeSelection selset[n]) as array
					
					if edgeList != undefined then
					(
						
							for p = 1 to edgeList.count do
							(
								
								edgeVerts = polyOp.getEdgeVerts selset[n] edgeList[p]
								
								v1 = polyOp.getVert selset[n] edgeVerts[1]
								v2 = polyOp.getVert selset[n] edgeVerts[2]
								
								ledgeWidth = length (v1 - v2)
								edgeVector = normalize (v1 - v2)
								
								topFace = findTopFace selset[n] edgeList[p]
								
								faceCen = polyOp.getFaceCenter selset[n] topFace
								edgeCen = [(v1.x +v2.x)/2, (v1.y +v2.y)/2, (v1.z +v2.z)/2]
								
								ledgeLength = ( length ( faceCen - edgeCen ))*2
									
								faceNorm = in coordsys world (polyOp.getFaceNormal selset[n] topFace)
								--polyOp.setFaceSelection selset[n] topFacet
								--faceRot = (matrixFromNormal faceNorm ) as eulerAngles
								
									
								
									
								l = ledge() 
								-- blah
									
									-- blah
						
								--in coordsys world l.dir = faceNorm
									
								--make up rot matrix
								worldUpVector = [0,0,1]
								upVector = normalize ( cross edgeVector faceNorm )
								thematrix = matrix3 edgeVector upVector faceNorm [0,0,0]
								l.transform = thematrix
							
								l.pos = edgeCen 
								l.grid.length = ledgeLength 
								l.grid.width = ledgeWidth
								
							)
						
					)
					
				)
				
			)
			
		)
	
	
	)
	
	-- rollout and floater generation
	if MakeLedge_Float != undefined then closerolloutfloater MakeLedge_Float
	MakeLedge_Float = newrolloutfloater "MakeLedge" 100 100 100 100
	addrollout Roll_MakeLedge MakeLedge_Float
	
	*/

)
