
(

	selset = getcurrentselection()
	
	
	for n = 1 to selset.count do
	(
		
		if classof selset[n] == editable_Poly then
		(
		
			obj = selset[n]
				
			f = #{}
			
			f = polyop.getFaceSelection obj
			
			killfaces = -f
			
			emobj = copy obj
			
			emobj.name = uniqueName "DECAL_"
			
			polyOp.deleteFaces emobj killfaces delIsoVerts:true
			
			p = push()
			
			p.push_value = 0.01
			
			addModifier emobj p
			
			convertToPoly emobj
			
			selectMore emobj
			
			deselect selset
			
		)
			
	)	
		
)
