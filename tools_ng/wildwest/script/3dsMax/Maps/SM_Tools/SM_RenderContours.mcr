
(
	global RenderContoursFloat
	global RenderPath = "C:\Documents and Settings\stuartm\My Documents\My Pictures"
	--set target folder
	-- set render file names
	--set step height
	--set number of steps


	--move plane up

	
	rollout RollRenderContours "Render Contours"
	(
	
		--UI
		colorpicker ContourColour "Plane Colour:" color:[0,0,255]
		edittext LayerFilename "filename:" text:"contour"
		button ChoosePath "Pick Folder"
		spinner ContourStartHeight "start height" range:[-1000, 1000, 0] type:#integer
		spinner ContourStepHeight "step height" range:[1, 100, 20] type:#integer
		spinner ContourStepTotal "step total" range:[1, 100, 10] type:#integer
		
		button GoRender "Render Contours"
		
		progressbar pb_contour color:red
		
		
		on ChoosePath pressed do
		(
			
				RenderPath = getSavePath caption:"Save to" initialDir:RenderPath
		
		)
		
		on GoRender pressed do
		(
			--getselection to calc position and size of plane needed
			--SelSet = getcurrentselection()
			SelSet = selection
			selchk = getcurrentselection()
			format "\n selection:%" selchk
			
			if selchk.count > 0 then
			(
				ContourCen = SelSet.center
				CMax = SelSet.max
				CMin = SelSet.min

		
				--create contour plane
				ContourPlane = plane name:"CONTOUR"
				ContourPlane.pos = ContourCen
				ContourPlane.width = CMax.x - CMin.x
				ContourPlane.Length = CMax.y - CMin.y
				
				--set up contour plane material
				ContourPlane.material = standard diffuse:ContourColour.color selfIllumAmount:100
				
				--Render out contour levels
				SetContourHeight = ContourStartHeight.value
				
				for n = 1 to ContourStepTotal.value do
				(
					--move plane to position
					ContourPlane.pos.z = SetContourHeight
					
					--render to file section
					--append height in metres to name
					CFilename = LayerFilename.text + "_" + (SetContourHeight as string) +"m"
					
					--update progress bar
					pb_contour.value = 100*(n/ContourStepTotal.value)
					
					--render image
					render outputFile:(RenderPath + "\\" + CFilename + ".bmp") vfb:off
					--format "\n IMAGE SENT TO:%" (RenderPath  + CFilename + ".bmp")
					
					--move plane to next height
					SetContourHeight = SetContourHeight + ContourStepHeight.value
					
					
						
				)

				--remove plane object
				delete ContourPlane
				
			) else messagebox "no bounding objects selected!"
			
			
		)
		
	)
	
	
	-- rollout and floater generation
	if RenderContoursFloat != undefined then closerolloutfloater RenderContoursFloat
	RenderContoursFloat = newrolloutfloater "Render Contours" 200 220 100 100
	addrollout RollRenderContours RenderContoursFloat

) 