
(
		
	global SM_BakeVertAlpha_float	

	rollout SM_BakeVert_roll "Vert Baker"
	(
		--variables
		global Blayers = #()

		
		--interface
		button loadpsd "load psd"
		spinner startChan "1st Chan" range:[5,99,8] type:#integer
		button gobake "go"
		
		--bitmap bpreview1 bitmap:(bitmap 256 256 color:white)
		--bitmap bpreview2 bitmap:(bitmap 256 256 color:white)
		--bitmap bpreview3 bitmap:(bitmap 256 256 color:white)
		--bitmap bpreview4 bitmap:(bitmap 256 256 color:white)
		
		--functions
		fn loadPSDfile psdfile =
		(
			global psdlayers = bitmapLayerManager.getLayerCount psdfile
	
			if psdlayers > 0 and psdlayers < 5 then
			(
				for n = 1 to psdlayers do
				(
					Blayers[n] = bitmapLayerManager.LoadLayer psdfile (n-1) true
					
					--format "bitmap % loaded, % x % /n" n Blayers[n].width Blayers[n].height
				)
			)
			else
			(
				messagebox "layer count not 1-4"
			) 

			
		)
		
		fn BakePSDtoMesh =
		(
		 --Setup dimensions, then call load bitmap, add vert alpha layer, then bakelayer( copies colours to verts )
			
			format "------------BakePSDtoMEsh--------------"
			
			--measure mesh and bitmap dimensions
			bitmapX = Blayers[1].width
			bitmapY = Blayers[1].height
			
			--format "bitmap x:%" bitmapX
			--format "bitmap y:% \n" bitmapY 
			
			-- get mesh sizes
			meshX = $.max.x - $.min.x
			meshY = $.max.y - $.min.y
			
			--cell size for vert colouring
			cellx = meshX/bitmapX
			celly = meshY/bitmapY
			
			--format "cell x:%" cellx
			--format "cell y:%" celly
			
			-----------------------------------------
			------ main loop for loading colours ----
			-----------------------------------------
			
	
				nv = $.numverts
								
				offsetx = cellx/2
				offsety = celly/2
				
				-- set up number of vertex channels for VA
				-- just set up for 4 layer atm
				--polyOp.setNumVDataChannels $ 8 keep:false					
				
						
				for v = 1 to nv do
				(
				
					-- get vert position	
					vpos = in coordsys local polyOp.getVert $ v
 					vpos.x = vpos.x + (meshX/2)
					vpos.y = vpos.y + (meshY/2)
					--format "\n **** \nvpos: % \n" vpos

					-- calculate position of pixel to read
					pixposx = abs (vpos.x/cellx) as integer
					pixposy = abs (vpos.y/celly) as integer
					
					--format "pixel pos x:%  y:% \n" pixposx pixposy
					
					pixposy = bitmapY - pixposy
					
					if pixposx >= bitmapX then pixposx = bitmapX-1
					if pixposy >= bitmapY then pixposy = bitmapY-1
					
					
					-- run through bitmaps and assign pixel colour to the vert
					-- accross the 4 layers of VA
					for n = 1 to psdlayers do
					(
					
						bvertcolor = getPixels Blayers[n] [pixposx, pixposy] 1
						--format "pix colour: %" bvertcolor[1]
						
						polyOp.setVertColor $ (startChan.value+n-1) v bvertcolor[1]
						
						
					)
				
				)
				
				
		)
		
		
		--events
		
		on loadpsd pressed do
		(
			global psdfilename = getOpenFilename "load psd"
			print psdfilename
			--print "call loadPSD"
			loadPSDfile psdfilename
			
			--bpreview1.bitmap = blayers[1]
			--bpreview2.bitmap = blayers[2]
			--bpreview3.bitmap = blayers[3]
			--bpreview4.bitmap = blayers[4]

		)
		
		on gobake pressed do
		(
			
				if classof $ == editable_poly then
				(
					--print "call loadPSD"
					--loadPSDfile psdfilename
					--moved to load psd event
					
					--print "call bakePSD"
					BakePSDtoMesh()
					
					
				)

 
		)
	
	)

	if SM_BakeVertAlpha_float != undefined then closerolloutfloater SM_BakeVertAlpha_float
	SM_BakeVertAlpha_float = newrolloutfloater "Bake PSD to Vert Colour" 110 110 100 100
	addrollout SM_BakeVert_roll SM_BakeVertAlpha_float

)
