-- Auto LOD mapper tool
-- Triggers a LOD map bake using the prepped scene files.

--logging...
--bakepath location
--baked map name
--time to bake

--core functions
filein (RsConfigGetWildWestDir() + "script/3dsmax/maps/LODBaker_Core.ms")

--LODSceneRoot = "x:/gta5/art/ng/Models/Handy objects/LODBakes/"
--iniPath = LODSceneRoot + "LODBakerBatch.ini"
--processLog = LODSceneRoot + "Process.log"


if gRsPerforce.connected() == false then gRsPerforce.Connect()
gRsPerforce.run "sync" #(LODBaker.iniPath)


fn doNextLODBake theScene =
(
	--reset max
	resetMaxFile #noPrompt
	
	try
	(
		--update log
	
		--set LODBaker_Core struct settings to what they need to be
		rawParams = getINISetting LODBaker.iniPath sysInfo.computername theScene
		params = filterString rawParams " "
		LODBaker.mapRes_X = params[1] as Float
		LODBaker.mapRes_Y = params[2] as Float
		theMaxScene = theScene
		LODBaker.bakePath = LODBaker.LODSceneRoot + theScene + ".bmp"
		
		--load the scene
		loadMaxFile (LODBaker.LODSceneRoot + theScene + ".max") quiet:true missingExtFilesAction:#logmsg 
		
		--do bake
		LODBaker.doTheBake mode:"Auto"
			--update log
		
		--create completed
		finishLog = createFile (LODBaker.LODSceneRoot + theScene + "_Completed.txt" )
		--openFile 
		
		--copy file to t:
	)
	catch
	(
		closeLog processLog
	)
	
)

fn LODBakeProcessor =
(
	--create bake log
	if (doesfileexist LODBaker.processLog) then
	(
		flushlog LODBaker.processLog
		deleteFile LODBaker.processLog
	)
	else
	(
		openlog LODBaker.processLog
	)
	
	--first try to sync the files
	local depotBakeFiles = getDepotFiles (RsConfigGetArtDir() + "Models/Handy objects/LODBakes") walk:true
	local bakeFiles = for f in depotBakeFiles collect f.depotFile
	gRsPerforce.sync bakeFiles
	
	
	--load next bake scene
	sceneNames = getINISetting LODBaker.iniPath sysInfo.computername
	for theScene in sceneNames do
	(
		doNextLODBake theScene
	)
	
	closeLog LODBaker.processLog
	
)
