-- This file should be included in all wildwest scripts to give us access to the most useful functions and config files
-- by running:

--filein (RsConfigGetWildWestDir() + "script/3dsmax/_config_files/wildwest_header.ms")


-- Load the official tools config file
filein "rockstar/export/settings_funcs.ms"

-- Load the xml parser
filein ("pipeline/util/xml.ms")


-- Figure out the project
theProject = RSConfigGetProjectName core:true
theProjectRoot = RsConfigGetProjRootDir  core:true
theWildWest = RsConfigGetWildWestDir  core:true
theProjectConfig = RsConfigGetProjBinConfigDir  core:true
theProjectToolsRoot = RsConfigGetToolsDir  core:true
theProjectAssetRoot = RsConfigGetAssetsDir  core:true

pathToCommonFunctions =  theWildWest + "script/3dsmax/_common_functions/"
pathToConfigFiles =  theWildWest +"script/3dsmax/_config_files/" 

-- Load the project-specific config files here.
(
	configFile = case of
	(
		(matchPattern theproject pattern:"*gta5*"): ("project_config_gta5.ms")
		(theproject == "rdr3"): ("project_config_bob.ms")
		Default:(undefined)
	)
	
	if (configFile == undefined) then 
	(
		format "WARNING: UNDEFINED TECHART PROJECT"
	)
	else 
	(
		fileIn (pathToConfigFiles + configFile)
	)
)

-- MAPPED FUNCTION FOR LOADING AN ARRAY OF FILES IN THE COMMON FOLDER
mapped fn rsta_loadCommonFunction file = 
(
	-- ADD THE FILE TYPE IF MISSING AND PATH 
	if (getFilenameType file == "") do file = file + ".ms"
	file = pathToCommonFunctions + file
	
	-- FILE IN
	if (doesFileExist file) then filein file
	else messageBox (file + "\nFile not found, Please Email TechArt.") title:"Filein Error!"
)


-- Load common functions
rsta_loadCommonFunction \
#(
	"FN_Common",
	"FN_Constructors",
	"FN_Rigging",
	"Fn_RSTA_Characters",
	"FN_Materials",
	"FN_Math",
	"FN_SpatialHash",
	"FN_Geometry",
	"FN_CSharp",
	"FN_Tool",
	"FN_Animation",
	"FN_Image",
	"FN_Objects",
	"FN_FreeFormDeformers",
	"FN_RSTA_UI",
	"fn_rsta_dependency"
)