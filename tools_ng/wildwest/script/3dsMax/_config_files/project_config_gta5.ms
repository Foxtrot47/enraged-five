-- GTA5 Config file

textureTypes = #("*.tga","*.png","*.bmp","*.tif") 



-- ==========================================
-- =====                Ped Section                =====
-- ==========================================




ped_standardnameprefix = "Standard 00"
ped_shadernameprefix = "Rage00" 		-- What's the base name for the shader?
ped_shadernamevalue = 0 			-- Whats the base number for shader numbering?

ped_fullComponentNamesByID =#("Head", "Upper", "Lower", "Accessory", "Task", "Hands", "Feet", "Hair 0", "Hair 1", "Hair 2", "Pal-Dec", "Pal-Dec", "Pal-Dec","Pal-Dec","Teeth","Jbib","Beard")
ped_textureprefix = #("head","uppr", "lowr", "accs", "task", "hand", "feet","hair","hair","hair","decl","decl","decl","decl", "teef","jbib","berd")
ped_componenttypes = #("HEAD","UPPR","LOWR","ACCS","TASK","HAND","FEET","HAIR","DECL","TEEF","JBIB","BERD")
ped_componenttypeSLOT = #(1,2,3,4,5,6,7,8,12,15,16,17)
ped_textureDefaultVariation = "_a"
ped_propList = #(#("P_HE","P_EY","P_EA","P_MO"),#("P_LH"),#("P_RH"),#("P_LW", "P_LF"),#("P_RW", "P_RF"),#("P_HI")) -- all prop name types
ped_propAlignAndLinkBones = #("Skel_Head","SKEL_L_Hand","SKEL_R_Hand","RB_L_ForeArmRoll","RB_R_ForeArmRoll","SKEL_Pelvis") -- bones props are parented too
ped_propPrefix = "p_"-- define the prop prefix that we will use when selecting props
	
-- Define some things that we will refer to through the prop functions inside fn_rigging.ms

--heirachy of objects
ped_vExclusionOrder = #("HEAD", "BERD", "HAIR", "UPPR", "LOWR", "HAND", "FEET", "TEEF", "ACCS", "TASK", "DECL", "JBIB", "P_HE", "P_EY", "P_EA", "P_MO", "P_LH", "P_RH", "P_LW", "P_RW", "P_HI", "P_LF", "P_RF")

--Standard bone, LOD parent.  Use this array to override any parent bones. Eg, we want the all fingers to be skinned to the index finger to allow basic animation shapes.
ped_stdVSkelToLODSkel =
#(#("SKEL_ROOT", "mover"),
#("SKEL_Pelvis", "SKEL_ROOT"),
#("RB_L_ThighRoll", "SKEL_L_Thigh"),
#("SKEL_Spine_Root", "SKEL_ROOT"),
#("SKEL_L_Thigh", "SKEL_Pelvis"),
#("SKEL_L_Calf", "SKEL_L_Thigh"),
#("SKEL_L_Foot", "SKEL_L_Calf"),
#("SKEL_L_Toe0", "SKEL_L_Foot"),
#("RB_R_ThighRoll", "SKEL_R_Thigh"),
#("SKEL_R_Thigh", "SKEL_Pelvis"),
#("SKEL_R_Calf", "SKEL_R_Thigh"),
#("SKEL_R_Foot", "SKEL_R_Calf"),
#("SKEL_R_Toe0", "SKEL_R_Foot"),
#("SKEL_Spine0", "SKEL_Spine_Root"),
#("SKEL_Spine1", "SKEL_Spine0"),
#("SKEL_Spine2", "SKEL_Spine1"),
#("SKEL_Spine3", "SKEL_Spine2"),
#("SKEL_Neck_1", "SKEL_Spine3"),
#("RB_Neck_1", "SKEL_Neck_1"),
#("SKEL_Head", "SKEL_Neck_1"),
#("SKEL_L_Clavicle", "SKEL_Spine3"),
#("RB_L_ArmRoll", "SKEL_L_UpperArm"),
#("SKEL_L_UpperArm", "SKEL_L_Clavicle"),
#("SKEL_L_Forearm", "SKEL_L_UpperArm"),
#("RB_L_ForeArmRoll", "SKEL_L_Forearm"),
#("SKEL_L_Hand", "SKEL_L_Forearm"),
#("SKEL_R_Clavicle", "SKEL_Spine3"),
#("RB_R_ArmRoll", "SKEL_R_UpperArm"),
#("SKEL_R_UpperArm", "SKEL_R_Clavicle"),
#("SKEL_R_Forearm", "SKEL_R_UpperArm"),
#("RB_R_ForeArmRoll", "SKEL_R_Forearm"),
#("SKEL_R_Hand", "SKEL_R_Forearm"),
#("SKEL_L_Finger00", "SKEL_L_Hand"),
#("SKEL_L_Finger01", "SKEL_L_Hand"),
#("SKEL_L_Finger02", "SKEL_L_Hand"),
#("SKEL_L_Finger10", "SKEL_L_Hand"),
#("SKEL_L_Finger11", "SKEL_L_Finger20"),
#("SKEL_L_Finger12", "SKEL_L_Finger20"),
#("SKEL_L_Finger20", "SKEL_L_Hand"),
#("SKEL_L_Finger21", "SKEL_L_Finger20"),
#("SKEL_L_Finger22", "SKEL_L_Finger20"),
#("SKEL_L_Finger30", "SKEL_L_Finger20"),
#("SKEL_L_Finger31", "SKEL_L_Finger20"),
#("SKEL_L_Finger32", "SKEL_L_Finger20"),
#("SKEL_L_Finger40", "SKEL_L_Finger20"),
#("SKEL_L_Finger41", "SKEL_L_Finger20"),
#("SKEL_L_Finger42", "SKEL_L_Finger20"),
#("SKEL_R_Finger00", "SKEL_R_Hand"),
#("SKEL_R_Finger01", "SKEL_R_Hand"),
#("SKEL_R_Finger02", "SKEL_R_Hand"),
#("SKEL_R_Finger10", "SKEL_R_Finger20"),
#("SKEL_R_Finger11", "SKEL_R_Finger20"),
#("SKEL_R_Finger12", "SKEL_R_Finger20"),
#("SKEL_R_Finger20", "SKEL_R_Hand"),
#("SKEL_R_Finger21", "SKEL_R_Finger20"),
#("SKEL_R_Finger22", "SKEL_R_Finger20"),
#("SKEL_R_Finger30", "SKEL_R_Finger20"),
#("SKEL_R_Finger31", "SKEL_R_Finger20"),
#("SKEL_R_Finger32", "SKEL_R_Finger20"),
#("SKEL_R_Finger40", "SKEL_R_Finger20"),
#("SKEL_R_Finger41", "SKEL_R_Finger20"),
#("SKEL_R_Finger42", "SKEL_R_Finger20"),
#("PH_L_Hand", "SKEL_L_Hand"),
#("PH_R_Hand", "SKEL_R_Hand"))

--LOD skeleton bone list
ped_vLODBones = 
#("SKEL_ROOT",
"SKEL_Pelvis",
"SKEL_Spine_Root",
"SKEL_L_Thigh",
"SKEL_L_Calf",
"SKEL_L_Foot",
"SKEL_R_Thigh",
"SKEL_R_Calf",
"SKEL_R_Foot",
"SKEL_Spine0",
"SKEL_Spine1",
"SKEL_Spine2",
"SKEL_Spine3",
"SKEL_Neck_1",
"SKEL_Head",
"SKEL_L_Clavicle",
"SKEL_L_UpperArm",
"SKEL_L_Forearm",
"SKEL_L_Hand",
"SKEL_R_Clavicle",
"SKEL_R_UpperArm",
"SKEL_R_Forearm",
"SKEL_R_Hand",
"SKEL_L_Finger20",
"SKEL_R_Finger20",
"MH_Hair_Crown",
"MH_Hair_Scale")


-- Ped variables
ped_standardMaterialSlotsToUse=3
ped_totalSubIDsToInitialise=ped_fullComponentNamesByID.count
ped_defaultmaxshaderslot =1
ped_defaultRageshaderslot =13
ped_defaultPlayerRageshaderslot =7


ped_modelNameLength = 18 -- number of characters in a peds name (exlcuding props, including prefixes)



--Ped Node variables
ped_skeleton_prefix = "SKEL_"
ped_rigcontrol_prefix = "CTRL_"
ped_rootbone = $'SKEL_ROOT'
ped_FaceRoot = $'Facial_UI'

SkeletonVersionTag = "SkeletonVersionTag"

ambient_facial_joySticks = #($CTRL_R_Brow, $CTRL_LowerLip, $CTRL_R_Blink, $CTRL_L_Blink, $CTRL_LookAT_Activator, $CTRL_L_Cheek, $CTRL_R_Cheek, $CTRL_UpperLip_Curl, $CTRL_LowerLip_Curl, $CTRL_C_Brow, $CTRL_Jaw, $CTRL_L_Brow, $CTRL_R_Eye, $CTRL_L_Eye, $CTRL_R_Mouth, $CTRL_Mouth, $CTRL_L_Mouth, $CTRL_UpperLip, $Facial_UI)




-- Ped Paths
theProjectPedFolder = theProjectRoot + "art/peds/"
theProjectPedTexturesFolder = ((rsconfigmakesafeslashes maxfilepath) + "textures/highres" + "/")
defaultBasePedTextures = (theProjectPedFolder + "Library/Base_Textures/TemplateTextures/")
theProjectPedRiggingToolsFolder = (theWildWest + "script/3dsmax/Characters/Rigging/")
theProjectPedSkeletonFolder = theProjectPedFolder + "skeletons/"
theProjectPedSkinningFolder = theProjectPedSkeletonFolder + "Skinning_Helpers/"
animfolder = (theProjectPedSkeletonFolder  +"Animations/")
pedSkinData = "c:/skins/"


-- Ped Scripts
ped_reset_bindpose_script = (theProjectPedRiggingToolsFolder+ "resetBindPose.ms")		
ped_animation_library_script = (theProjectPedRiggingToolsFolder + "Anim_Library_Interface.ms")
ped_skinbrush_script = (theProjectPedRiggingToolsFolder + "Skinbrush.ms")
ped_pose_reset_script = (theProjectPedRiggingToolsFolder + "Rigging_Data/fn_Skeleton_Resets.ms")


-- Ped Files
animmappingfile =  (animfolder + "SKEL_MALE_TRANSFORM_MAPPING.xmm")
faceanimmappingfile =  (animfolder+ "FACEJOYSTICK_MAPPING.xmm")
pedShaderSettingsFile = (pathToConfigFiles + "characters/max_shader_settings.dat")

male_skinwrap = (theProjectPedSkinningFolder + "Male_SkinWrap.max" )
female_skinwrap = (theProjectPedSkinningFolder + "Female_SkinWrap.max" )
female_skinwrap_heels = (theProjectPedSkinningFolder + "Female_SkinWrap_Heels.max" )

-- Tool Presets
skinBrushRadius = #(0.005,0.02,0.05,0.1,0.2,0.4)
facialSkinBrushRadius = #(0.001,0.005,0.01,0.15,0.05,0.1)
skinBrushStrength = #(0.01, 0.05, 0.1, 0.25,0.5,1.0)
skinBrushSliders = [0.001,2.0,0.1]

--Ped Sync Arrays
pedSkeletonFiles = #(theProjectPedSkeletonFolder + "Male Skeleton.max", theProjectPedSkeletonFolder + "Female Skeleton.max", theProjectPedSkeletonFolder + "Female Skeleton Heels.max")
pedGuideFiles = #(theProjectPedSkeletonFolder + "Male Guides.max", theProjectPedSkeletonFolder + "Female Guides.max")
pedSkinwrapFiles = #(male_skinwrap, female_skinwrap, female_skinwrap_heels)
pedRenderFiles = #(theProjectPedFolder + "Renders/...")
pedAnimFiles = #(theProjectPedSkeletonFolder + "Animations/...")
pedSharedTex = #(theProjectPedFolder + "Ped_Shared_Textures/PedSh_Generic/...")

-- ==========================================
-- =====                Map Section                =====
-- ==========================================

--map paths
mapShaderPresetPath = theProjectRoot + "art/textures/_PresetFiles/"

--map sync arrays
mapShaderPresetFiles = #(mapShaderPresetPath +"...")

-- ==========================================
-- =====              Prop Section                =====
-- ==========================================

-- Prop Paths
theProjectWeaponsFolder = theProjectRoot + "art/Models/Props/Weapons/"
theProjectWeaponRigsFolder = theProjectRoot + "art/Models/Props/Weapons/Weapon_Rigging/"


-- Prop Files
weapon_markers = theProjectWeaponRigsFolder + "Weapon_Markers.max" 
weapon_att_markers = theProjectWeaponRigsFolder + "Weapon_Attachment_Markers.max" 
weapon_pistol_grip_mesh = theProjectWeaponRigsFolder + "Weapon_Pistol_Grip_Meshes.max"
weapon_rifle_grip_mesh = theProjectWeaponRigsFolder + "Weapon_Rifle_Grip_Meshes.max"
weapon_mover_mesh = theProjectWeaponRigsFolder + "Weapon_Mover_Mesh.max"

--Prop Sync Arrays
weaponRiggingFiles = #(weapon_markers, weapon_att_markers, weapon_pistol_grip_mesh, weapon_rifle_grip_mesh, weapon_mover_mesh)


-- ==========================================
-- =====             Vehicle Section             =====
-- ==========================================