--	quick script to fire the content tree wizard from inside max.

theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

theProjectConfig = RsConfigGetProjBinConfigDir()

toolsRoot = RsConfigGetToolsRootDir()

wizardPath = (toolsRoot +"/bin/ContentTreeWizard/ContentTreeWizard.exe")

format ("wp: "+wizardPath+"\n")

-- dosCommand "X:/gta5/tools_ng/bin/ContentTreeWizard/ContentTreeWizard.exe"
dosCOmmand wizardPath