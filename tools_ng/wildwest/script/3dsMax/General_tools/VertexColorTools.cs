﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.ComponentModel;


namespace VertexColorTools
{
    class ChannelData
    {
        public SolidColorBrush StatusColor { get; set; }
        public String Text { get; set; }
        public String Status { get; set; }
        public int ID { get; set; }
    }

    class ChannelDataEventArgs : EventArgs
    {
        public ChannelData Data;

        public ChannelDataEventArgs(ChannelData InputData)
        {
            this.Data = InputData;
        }
    }

    class VertexColorData: INotifyPropertyChanged
    {
        //PROPERTIES--------------------------------------------------

        public SolidColorBrush SwatchColor { get; set; }
        public ObservableCollection<string> ActionList { get; set; }

        Int16 _ActionIndex;
        public Int16 ActionIndex
        {
            get
            {
                return _ActionIndex;
            }
            set
            {
                _ActionIndex = value;
                this.RaisePropertyChanged("ActionIndex");
            }
        }

        public ObservableCollection<String> FullChannelList { get; set; }
        public ObservableCollection<ChannelData> ChannelDataList { get; set; }

        string _ToolHeader;
        public string ToolHeader
        {
            get
            {
                return _ToolHeader;
            }
            set
            {
                _ToolHeader = value;
                this.RaisePropertyChanged("ToolHeader");
            }
        }

        string _ToolPath;
        public string ToolPath
        {
            get
            {
                return _ToolPath;
            }
            set
            {
                _ToolPath = value;
                this.RaisePropertyChanged("ToolPath");
            }
        }

        Int32 _ChannelIndex;
        public Int32 ChannelIndex
        {
            get
            {
                return _ChannelIndex;
            }
            set
            {
                _ChannelIndex = value;
                this.RaisePropertyChanged("ChannelIndex");
            }
        }

        Int32 _ChanManIndex;
        public Int32 ChanManIndex
        {
            get
            {
                return _ChanManIndex;
            }
            set
            {
                _ChanManIndex = value;
                this.RaisePropertyChanged("ChanManIndex");
            }
        }

        Boolean _ShowVertexColors;
        public Boolean ShowVertexColors
        {
            get
            {
                return _ShowVertexColors;
            }
            set
            {
                _ShowVertexColors = value;
                this.RaisePropertyChanged("ShowVertexColors");
            }
        }

        Boolean _ShowTextures;
        public Boolean ShowTextures
        {
            get
            {
                return _ShowTextures;
            }
            set
            {
                _ShowTextures = value;
                this.RaisePropertyChanged("ShowTextures");
            }
        }

        Boolean _DisplayObject;
        public Boolean DisplayObject
        {
            get
            {
                return _DisplayObject;
            }
            set
            {
                _DisplayObject = value;
                this.RaisePropertyChanged("DisplayObject");
            }
        }

        Boolean _DisplayMaterial;
        public Boolean DisplayMaterial
        {
            get
            {
                return _DisplayMaterial;
            }
            set
            {
                _DisplayMaterial = value;
                this.RaisePropertyChanged("DisplayMaterial");
            }
        }

        Boolean _ShowCollisionChannels;
        public Boolean ShowCollisionChannels
        {
            get
            {
                return _ShowCollisionChannels;
            }
            set
            {
                _ShowCollisionChannels = value;
                this.RaisePropertyChanged("ShowCollisionChannels");
            }
        }

        Boolean _ShowGeometryChannels;
        public Boolean ShowGeometryChannels
        {
            get
            {
                return _ShowGeometryChannels;
            }
            set
            {
                _ShowGeometryChannels = value;
                this.RaisePropertyChanged("ShowGeometryChannels");
            }
        }

        String _InfoText;
        public String InfoText
        {
            get
            {
                return _InfoText;
            }
            set
            {
                _InfoText = value;
                this.RaisePropertyChanged("InfoText");
            }
        }
        private Dictionary<int, string> ChannelDict = new Dictionary<int, string>();

        String _RangeField;
        public String RangeField
        {
            get
            {
                return _RangeField;
            }
            set
            {
                int Num;
                bool isInt = int.TryParse(value, out Num);
                if (isInt)
                {
                    if (Num < 0)
                        _RangeField = "0";
                    else if (Num > 255)
                        _RangeField = "255";
                    else
                        _RangeField = value;
                }
            }
        }

        String _ChannelField;
        public String ChannelField
        {
            get
            {
                return _ChannelField;
            }
            set
            {
                int Num;
                bool isInt = int.TryParse(value, out Num);
                if (isInt)
                {
                    if (Num < -2)
                        _ChannelField = "-2";
                    else if (Num > 31)
                        _ChannelField = "31";
                    else
                        _ChannelField = value;
                }
            }
        }

        public VertexColorData()
        {
            //Hard-Coded Channel Dictionary
            //Ref: https://devstar.rockstargames.com/wiki/index.php/Vertex_Channels
            ChannelDict.Add(-2, "-2:\tAlpha");
            ChannelDict.Add(-1, "-1:\tIllumination");
            ChannelDict.Add(0, "0:\tVertex Color");
            ChannelDict.Add(1, "1:\tTexture UVs");
            ChannelDict.Add(2, "2:\tMask UVs");
            ChannelDict.Add(8, "8:\tSimplygon");
            ChannelDict.Add(9, "9:\tTerrain Shader");
            ChannelDict.Add(11, "11:\tMicromovement");
            ChannelDict.Add(13, "13:\tMesh-tint");
            ChannelDict.Add(14, "14:\tEmissive/Ped Environmental");
            ChannelDict.Add(15, "15:\tPed Fatness");
            ChannelDict.Add(20, "20:\tTemporary Working Color");
            ChannelDict.Add(21, "21:\tTemporary Working Color");
            ChannelDict.Add(22, "22:\tTemporary Working Color");
            ChannelDict.Add(23, "23:\tCollision-Surface Data");
            ChannelDict.Add(24, "24:\tCollision-Surface Data");
            ChannelDict.Add(25, "25:\tCollision-Surface Data");
            ChannelDict.Add(26, "26:\tTemporary Working Color");
            ChannelDict.Add(30, "30:\tAO Bake Overlay");
            ChannelDict.Add(31, "31:\tProcedural Painter Data");

            FullChannelList = new ObservableCollection<String>();

            this.InitChannelList();

            //Hard-Coded Actions for the Action List
            ActionList = new ObservableCollection<String>();
            ActionList.Add("Set Color");
            ActionList.Add("Colorise (Set Hue/Saturation)");
            ActionList.Add("Set Hue");
            ActionList.Add("Set Saturation");
            ActionList.Add("Set Luminosity");
            ActionList.Add("Multiply");
            ActionList.Add("Screen");
            ActionList.Add("Invert");

            //Default Values
            InfoText = "Ready";
            SwatchColor = new SolidColorBrush(Color.FromRgb(255, 0, 160));
            ChannelDataList = new ObservableCollection<ChannelData>();
            RangeField = "10";
            ChannelField = "0";
            ChanManIndex = 0;
            ChannelIndex = 0;
            ToolHeader = "Select a tool from above";
            ShowCollisionChannels = false;
            ShowGeometryChannels = true;
        }

        //FUNCTIONS----------------------------------------------------

        FrameworkElement BindedElement;

        public void BindElement(FrameworkElement InputTargetElement)
        {
            this.BindedElement = InputTargetElement;
            this.BindedElement.DataContext = this;
            this.BindedElement.AddHandler(TextBlock.MouseDownEvent, new RoutedEventHandler(this.TextBlock_MouseDownEvent), true);
        }

        public void TextBlock_MouseDownEvent(Object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource is TextBlock)
            {
                var TargetTextBlock = e.OriginalSource as TextBlock;

                if (TargetTextBlock.Name == "ChannelItem" && TargetTextBlock.DataContext is ChannelData)
                {
                    var TargetChannelData = TargetTextBlock.DataContext as ChannelData;
                    RaiseSelectChannel(TargetChannelData);
                }
            }
        }

        public event EventHandler<ChannelDataEventArgs> SelectChannel;  // delegate

        void RaiseSelectChannel(ChannelData TargetChannelData)
        {
            SelectChannel(this, new ChannelDataEventArgs(TargetChannelData));
        }

        /// <summary>
        /// Set up FullChannelList with the map channels
        /// </summary>
        public void InitChannelList()
        {
            FullChannelList.Clear();

            for (int i = -2; i < 32; i++)
            {
                String NewValue;
                if (ChannelDict.ContainsKey(i))
                {
                    NewValue = ChannelDict[i];
                }
                else
                {
                    NewValue = (i.ToString() + ":\t[Unused]");
                }

                FullChannelList.Add(NewValue);
            }
        }

        /// <summary>
        /// Clear FullChannelList and replaces list with a single message
        /// </summary>
        /// <param name="Message">message to replace the contents of the list</param>
        public void InitChannelListWithMessage(String Message)
        {
            FullChannelList.Clear();
            FullChannelList.Add(Message);
        }

        public string GetActionMode()
        {
            string Mode = "set";

            switch (this.ActionIndex)
            {
                case 1:
                    Mode = "colourise";
                    break;
                case 2:
                    Mode = "hue";
                    break;
                case 3:
                    Mode = "saturation";
                    break;
                case 4:
                    Mode = "luminosity";
                    break;
                case 5:
                    Mode = "multiply";
                    break;
                case 6:
                    Mode = "screen";
                    break;
                case 7:
                    Mode = "invert";
                    break;
            }

            return Mode;
        }

        public void UpdateChannelDataListWithMessage(string Message)
        {
            this.ChannelDataList.Clear();
            ChannelData NewItem = new ChannelData();
            NewItem.Text = Message;
            NewItem.ID = -3;
            NewItem.StatusColor = new SolidColorBrush(Color.FromRgb(255, 0, 80));
            NewItem.Status = "void";
            this.ChannelDataList.Add(NewItem);
        }

        public void UpdateChannelDataList(Int32[] ChannelList, String[] StatusList)
        {
            this.ChannelDataList.Clear();

            for (int i = 0; i < ChannelList.Count(); i++)
            {
                String NewEntry;
                SolidColorBrush NewStatusColor;

                ChannelData NewItem = new ChannelData();

                if (this.ChannelDict.ContainsKey(ChannelList[i]))
                {
                    NewEntry = this.ChannelDict[ChannelList[i]];
                }
                else
                {
                    NewEntry = (ChannelList[i].ToString() + ":\t[Unused]");
                }

                //code to set the StatusColor value according to the StatusList

                switch (StatusList[i])
                {
                    case "used":
                        NewStatusColor = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                        break;
                    case "active":
                        NewStatusColor = new SolidColorBrush(Color.FromRgb(255, 216, 0));
                        break;
                    default:
                        NewStatusColor = new SolidColorBrush(Color.FromRgb(216, 216, 216));
                        break;
                }

                NewItem.Text = NewEntry;
                NewItem.ID = ChannelList[i];
                NewItem.StatusColor = NewStatusColor;
                NewItem.Status = StatusList[i];
                this.ChannelDataList.Add(NewItem);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void RaisePropertyChanged(string PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }

    }
}