/*=====================================================================
	Tool to remove redundant max container lock files
	left when max crashes without closing a container

	Neil Gregory
	11/2011
=====================================================================*/

try(DestroyDialog LockFileNuker)catch()
rollout LockFileNuker "Lock File Nuker"
(
	local dirPath = (RsConfigGetArtDir() + "models")
	
	edittext txtPath enabled:false text:dirPath
	button btnRootDir "Choose Root Folder"
	button btnNuke "Nuke Files"
	
	fn getFilesRecursive root pattern =
	(
		dir_array = GetDirectories (root+"/*")
		append dir_array root
		for d in dir_array do
		  join dir_array (GetDirectories (d+"/*"))
		my_files = #()
		for f in dir_array do
		  join my_files (getFiles (f + pattern))
		my_files
	)
	
	on btnRootDir pressed do
	(
	  dirPath = getSavePath caption:"pick folder" initialDir:dirPath
	  txtPath.text = dirPath
	 --format "\n dir: %" theDir
	  
	)
	
	on btnNuke pressed do
	(
		local lockFiles = getFilesRecursive dirPath "*.maxc.lock"
		if lockFiles.count > 0 then
		(
			for f in lockFiles do 
			(
				if getFileAttribute f #readOnly == true then setFileAttribute f #readOnly false
				deleteFile f
			)
			messagebox "OK" title:"Complete"
		)
		else
		(
			messagebox "No Lock Files to delete" title:"Complete"
		)
		
	)
	
)

createDialog LockFileNuker 