filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
filein "pipeline/helpers/materials/texturemetatag.ms"

rollout quickTCSEdit "Quick TCS Edit"
(
	local event_subObjSelChange
	local templateExclusions = #("pedtemplates")
	local templates = for t in (getfiles (RsConfigGetEtcDir() + "texture/*.xml") ) where
						(
							findItem templateExclusions (getfilenamefile t) == 0
						)
						collect getfilenamefile t
						
	----Banner
	dotNetControl rsBannerPanel "Panel" height:32 pos:[0,0] width:quickTCSEdit.Width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"quicktcsedit" filename:(getThisScriptFilename())
	
	dropdownlist ddlTemplateType "Template Types:" items:templates
	
	fn getFaceMaterial ev nd = 
	(
		if subObjectLevel != 4 then return false
			
		theMesh = GetAnimByHandle nd[1]
		theMaterial = theMesh.material
		if theMaterial == undefined then return false
			
		faces = (getFaceSelection theMesh) as array
		if faces.count != 0 then
		(
			--get material
			if classOf theMaterial == MultiMaterial then
			(
				idx = if classOf theMesh == Editable_poly then
				(
					polyop.getFaceMatID theMesh faces[1]
				)
				else
				(
					getFaceMatId theMesh faces[1]
				)
				
				theMaterial = theMaterial.materialList[idx]
			)
			
			meditMaterials[24] = theMaterial
			medit.SetActiveMtlSlot 24 false
			
			--refresh
			RsTexMetaRoll.refreshTexmapList()
		)
	)
	
	fn templateSelector arg = 
	(
		choice = ddlTemplateType.items[arg]
		
		--kill the texturemetatag and restart with the new template set
		destroyDialog RsTexMetaRoll
		textureFolder = quickTCSEdit.getTextureFolderFromTemplate choice
		RsCreateTextureTagTool xmlTemplateFile:(RsConfigGetEtcDir() + "texture/"+choice+".xml") textureFolder:textureFolder
		RsTexMetaRoll.ddl_selType.selection = 2
		RsTexMetaRoll.refreshTexmapList()
	)
	
	fn getTextureFolderFromTemplate template =
	(
		local thisScene = getFilenameFile(maxFileName)
		case template of
		(
			"componentpedtemplates":
			(
				if thisScene != undefined then return ("componentpeds/" + thisScene) else return "componentpeds"
			)
			"MapQuickTemplates": return "maps"
			"maptemplates": return "maps"
			"pedtemplates": return "componentpeds"
			"streamedpedtemplates":
			(
				if thisScene != undefined then return ("streamedpeds/" + thisScene) else return "streamedpeds"
			)
			"vehicletemplates": return "vehicles"
			"weapontemplates": return "weapons"
			default: return "globals"
		)
	)
	
	on ddlTemplateType selected arg do templateSelector arg
	
	on quickTCSEdit open do
	(
		banner.setup()
		
		textureFolder = getTextureFolderFromTemplate templates[1]
		
		RsCreateTextureTagTool xmlTemplateFile:(RsConfigGetEtcDir() + "texture/"+templates[1]+".xml") tcsEdit:true textureFolder:textureFolder
		--set in material slot mode
		RsTexMetaRoll.ddl_selType.selection = 2
		RsTexMetaRoll.refreshTexmapList()
		--get the selected face material and stuff it in slot 24 and make it active.
		
		event_subObjSelChange = NodeEventCallback mouseup:true subobjectSelectionChanged:getFaceMaterial
	)
	
	on quickTCSEdit close do
	(
		event_subObjSelChange = undefined
		gc light:true
		
		--close the texturemetatag tool
		destroyDialog RsTexMetaRoll
	)
	
)

try(destroyDialog quickTCSEdit) catch()
createDialog quickTCSEdit width:200 height:100 pos:[744, 300] style:#(#style_titlebar, #style_sysmenu, #style_minimizebox, #style_maximizebox)