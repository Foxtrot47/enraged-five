--TPageBuilderUI.ms
--2013 Andy Davis
--Rockstar London
--UI for the TextureBuilder tool
-----------------------------------------
--August 2013 Updates:
--support for .dds files added
--support for multimaterials added
--October 2013 Updates
--project generic pathing added
--December 2013
--new developement carried out in wildwest

	
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")					--wildwest header

global TPageUI

-----------------------------------------------------------
--DEV MODE ONLY!!!
--run this alias only if developing in wildwest
--comment out prior to distribution to projects
-- fn RsConfigGetWildWestDir =
-- (
-- 	"X:/wildwest/"
-- )
------------------------------------------------------------

filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/RSL_dotNetUIOps.ms")		--RS_dotNetPreset structure
CSharp.CompileToMemory #(::RsConfigGetWildwestDir() + "Script/3dsMax/General_tools/TPage.cs")
filein (RsConfigGetWildWestDir() + "script/3dsMax/General_Tools/TPageBuilder.ms")						--TPage wrapper and functionality	
RsCollectToolUsageData (GetThisScriptFilename())

	
struct TPageUIStruct
(	
	fn SafePath inPath slash:"back" =
	(
		local dirs = filterString inPath "/\\"
		local safe = ""
		local slashCharacter = "/"
		local numTokens = dirs.count
		
		if (slash == "back") then slashCharacter = "\\"
		
		for i = 1 to (numTokens - 1) do
		(
			append safe (dirs[i] + slashCharacter)
		)
		
		append safe dirs[numTokens]
		safe
	),
	
	Form,
	ToolTip,
	InfoPanel,
	ApplyButton,
	GenerateButton,
	PathButton,
	NameField,
	Thumbnail,
	ToleranceField,
	FixCheckBox,
	IniFilePath = this.SafePath (RsConfigGetWildWestDir() + @"script\3dsMax\_config_files\General_tools.ini"),
	DefaultTPagePath = this.SafePath (RsConfigGetArtDir() + @"Textures\LOD"),
	LogoPath = this.SafePath (RsConfigGetWildWestDir() + @"script\3dsMax\UI\texturePageLogo.bmp"),
	TPageOps,
		
	------------------------------------------------------------------------------------------------------------------------------------------
	--GENERAL FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn SelectionUpdate =
	(
		local infoString = "Select geometry to create a texture page."
		local tnPath = TpageUI.TPageOps.ThumbnailPath
		
		--clear thumbnail
		TPageUI.Thumbnail.Image = (dotNetClass "System.Drawing.Image").FromFile TPageUI.LogoPath
		if (doesFileExist tnPath) then
		(
			local tempFilePath = (getFilenamePath tnPath + getFilenameFile tnPath + ".tmp")
			
			gc()
			deleteFile tnPath
			deleteFile tempFilePath
		)
		
		if selection.count > 0 then
		(
			infoString = "Selection: " + selection[1].name
			TPageUI.GenerateButton.Enabled = true
			
			if selection.count > 1 then
			(
				
				for i = 2 to selection.count do
					append infoString (", " + selection[i].name)
			)
		)
		else
		(
			infoString = "Select geometry to create a texture page."
			TPageUI.GenerateButton.Enabled = false
				TPageUI.ApplyButton.Enabled = false
		)
		
		TPageUI.InfoPanel.Text = infoString
	),
	
	fn GenerateTexturePage s e =
	(
		TPageUI.ValidateFilename()
		TPageUI.TPageOps.TPagePath = TPageUI.PathButton.Text + TPageUI.NameField.Text
		
		rollout temprollout "Texture Page Builder"
		(
			label myLabel "Generating Texture Page" 
		)
		
		theNewFloater = createDialog temprollout 200 30
		
		local success = TPageUI.TPageOps.ProcessTPage (selection as array)
		
		destroyDialog temprollout
		
		if (success) then
		(
			TPageUI.TPageOps.ShowTextures true
			TPageUI.GenerateButton.Enabled = false
			TPageUI.TPageOps.GenerateThumbnail()
			TPageUI.Thumbnail.Image = (dotNetClass "System.Drawing.Image").FromFile TPageUI.TPageOps.ThumbnailPath
			
			local resolution = TPageUI.TPageOps.TPageBuilder.resolution
			
			TPageUI.InfoPanel.Text = "Texture Page resolution: " + resolution.Width as string + " x " + resolution.Height as string
			TPageUI.InfoPanel.Text += "\nNon-normalized UV's found on " + TPageUI.TPageOps.NonNormalisedObjs.count as string + (if TPageUI.TPageOps.NonNormalisedObjs.count == 1 then " object." else " objects.")
			TPageUI.ApplyButton.Enabled = true
		)
		
		else
		(
			if (TPageUI.TPageOps.TPageBuilder.TextureDefinitionList.Count == 1) then
			(
				TPageUI.InfoPanel.Text = "Only one valid texture found."
			)
			else
				TPageUI.InfoPanel.Text = "TPage failed to build."
			
			TPageUI.Thumbnail.Image = (dotNetClass "System.Drawing.Image").FromFile TPageUI.LogoPath
		)
		
		TPageUI.PrintNonNormalisedObjectData()
	),
	
	fn PrintNonNormalisedObjectData =
	(
		if (TPageUI.TPageOps.NonNormalisedObjs.count > 0) then
		(
			local objString = TPageUI.TPageOps.NonNormalisedObjs[1].name
			if TPageUI.TPageOps.NonNormalisedObjs.count > 1 then
			(
				for i = 2 to TPageUI.TPageOps.NonNormalisedObjs.count do
					objString += ", " + TPageUI.TPageOps.NonNormalisedObjs[i].name
			)
			TPageUI.InfoPanel.Text += "\nOmitted non-normalised UVs found on: " + objString
		)
	),
	
	fn ApplyTexturePage s e =
	(
		TPageUI.TPageOps.ApplyTPageShader()
		TPageUI.ApplyButton.Enabled = false
		TPageUI.TPageOps.ShowTextures true
	),
	
	fn ValidateFilename =
	(
		local filename = TPageUI.NameField.Text
		
		filename = this.ReplaceCharacters filename "\\/" "_"
		
		if (getFilenameType filename != toLower ".bmp") then
		(
			filename = this.ReplaceCharacters filename "." "_"
			filename += ".bmp"
		)
		
		TPageUI.NameField.Text = filename
	),
	
	fn ReplaceCharacters instring pattern sub =
	(
		local tokens = filterstring instring pattern
		local outstring = ""
		if tokens.count > 0 then outstring = tokens[1]
		
		if (tokens.count > 1) then
		(
			for i = 2 to tokens.count do
			(
				outstring += sub + tokens[i]
			)
		)
		return outstring
	),
	
	fn BrowseForDirectory =
	(
		local dirPath = TPageUI.PathButton.Text
		local newPath
		
		if (not doesFileExist dirPath) then dirPath = @"x:\"
		newPath = getSavePath caption:"Select the directory for the Texture Page" initialDir:dirPath
		
		if (newPath != undefined) then
		(
			if (newPath[newPath.count] != @"\") then newPath += "\\"
			TPageUI.PathButton.Text = newPath
			TPageUI.ToolTip.SetToolTip TPageUI.PathButton TPageUI.PathButton.Text
		)
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE FUNCTIONS
	-----------------------------------------------------------------f-------------------------------------------------------------------------
	
	fn SaveIniFile =
	(
		setINISetting TPageUI.IniFilePath "TPageUI" "WinLocX" (TPageUI.Form.Location.x as string)
		setINISetting TPageUI.IniFilePath "TPageUI" "WinLocY" (TPageUI.Form.Location.y as string)
		setINISetting TPageUI.IniFilePath "TPageUI" "WinWidth" (TPageUI.Form.Width as string)
		setINISetting TPageUI.IniFilePath "TPageUI" "WinHeight" (TPageUI.Form.Height as string)
		setINISetting TPageUI.IniFilePath "TPageUI" "TPagePath" TPageUI.PathButton.Text
		callbacks.removeScripts #selectionSetChanged id:#TPageCallbacks
	),
	
	fn LoadIniFile =
	(
		--default values
		local WinLocX = 100
		local WinLocY = 100
		local WinWidth = 200
		local WinHeight = 200
		local TPagePath = this.SafePath (RsConfigGetArtDir() + "Textures/LOD")
		
		try
		(
			WinLocX = getINISetting TPageUI.IniFilePath "TPageUI" "WinLocX" as integer
			WinLocY = getINISetting TPageUI.IniFilePath "TPageUI" "WinLocY" as integer
			WinWidth = getINISetting TPageUI.IniFilePath "TPageUI" "WinWidth" as integer
			WinHeight = getINISetting TPageUI.IniFilePath "TPageUI" "WinHeight" as integer
			TPagePath = getINISetting TPageUI.IniFilePath "TPageUI" "TPagePath"
		)
		
		catch()
		
		TPageUI.Form.Location = dotNetObject "system.drawing.point" WinLocX WinLocY
		TPageUI.Form.Size = dotNetObject "System.Drawing.Size" WinWidth WinHeight
		
		callbacks.addScript #selectionSetChanged "TPageUI.SelectionUpdate()" id:#TPageCallbacks
		
		--Texture Page Path handling
		if (TPagePath.count == 0) then TPagePath = this.SafePath (RsConfigGetArtDir() + "Textures/LOD")
		if (TPagePath[TPagePath.count] != @"\") then TPagePath += "\\"
		if (doesFileExist TPagePath) then TPageUI.PathButton.Text = TPagePath
		else TPageUI.PathButton.Text = this.DefaultTPagePath
		TPageUI.TPageOps.TPagePath = (TPageUI.PathButton.Text + TPageUI.NameField.Text)
		ToolTip.SetToolTip this.PathButton PathButton.Text		
		TPageUI.SelectionUpdate()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn CreateUI =
	(
		-- form setup		
		Form = dotNetObject "maxCustomControls.maxForm"
		Form.Text = "Texture Page Builder"
		Form.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").manual
		Form.Location = dotNetObject "system.drawing.point" 0 80
		Form.MaximumSize = dotNetObject "System.Drawing.Size" 276 1200
		Form.MinimumSize = dotNetObject "System.Drawing.Size" 276	525
		Form.FormBorderStyle = RS_dotNetPreset.FB_Sizable
		dotNet.AddEventHandler Form "Load" LoadIniFile
		dotNet.AddEventHandler Form "Closing" SaveIniFile
		
		--content
		ToolTip = dotnetobject "ToolTip"
		Table = dotNetObject "TableLayoutPanel"
		Table.Dock = RS_dotNetPreset.DS_Fill
		Table.RowCount = 9
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 40)		--R* banner
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)		--directory
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)		--filename
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30) 		--tolerance
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 260)	--texture preview
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "percent" 100)		--info
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)		--generate texture
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)		--apply texture
		Table.ColumnCount = 2
		Table.ColumnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 50)
		Table.ColumnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		Table.Margin = RS_dotNetPreset.Padding_None
		Form.Controls.Add Table
		
		RSBannerPanel	= dotNetObject "System.Windows.Forms.Panel"
		RSBannerPanel.borderstyle	= RS_dotNetClass.borderStyleClass.FixedSingle
		RSBannerPanel.dock = RS_dotNetPreset.DS.Fill
		
		local banner = makeRsBanner dn_Panel:RSBannerPanel width:395 studio:"london" mail:"andy.davis@rockstarlondon.com" wiki:"Texture_Page_Builder"
		banner.setup()
		Table.Controls.Add RSBannerPanel 0 0
		Table.SetColumnSpan RSBannerPanel 2
		
		local textColor = RS_dotNetPreset.ARGB 246 186 0
		
		PathLabel = RS_dotNetUI.InitLabel "Directory" RS_dotNetPreset.TA.MiddleLeft
		Table.Controls.Add PathLabel 0 1		
		
		PathButton = RS_dotNetUI.InitButton this.DefaultTPagePath
		PathButton.TextAlign = RS_dotNetPreset.TA.MiddleLeft
		PathButton.FlatStyle = RS_dotNetPreset.FS_Flat
		PathButton.Margin = dotNetObject "System.Windows.Forms.Padding" 2
		PathButton.ForeColor = textColor
		dotnet.AddEventHandler PathButton "Click" BrowseForDirectory
		Table.Controls.Add PathButton 1 1
		
		NameLabel = RS_dotNetUI.InitLabel "Filename" RS_dotNetPreset.TA.MiddleLeft
		Table.Controls.Add NameLabel 0 2
		
		NameField = dotNetObject "TextBox"
		NameField.TextAlign = (dotNetClass "System.Windows.Forms.HorizontalAlignment").Left
		NameField.Text = getFilenameFile maxFileName + "_TPage.bmp"
		NameField.Dock = RS_dotNetPreset.DS_Fill
		NameField.Margin = dotNetObject "System.Windows.Forms.Padding" 2
		Table.Controls.Add NameField 1 2
		
		ToleranceTable = dotNetObject "TableLayoutPanel"
		ToleranceTable.Dock = RS_dotNetPreset.DS_Fill
		ToleranceTable.ColumnCount = 3
		ToleranceTable.ColumnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 50)
		ToleranceTable.ColumnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 100)
		ToleranceTable.ColumnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		Table.Controls.Add ToleranceTable 0 3
		Table.SetColumnSpan ToleranceTable 2
		
		ToleranceLabel = RS_dotNetUI.InitLabel "Tolerance" RS_dotNetPreset.TA.MiddleLeft
		ToleranceTable.Controls.Add ToleranceLabel 0 0
		ToleranceField = dotNetObject "NumericUpDown"
		ToleranceField.TextAlign = (dotNetClass "System.Windows.Forms.HorizontalAlignment").Left
		ToleranceField.Value = 0.01
		ToleranceField.Minimum = 0
		ToleranceField.Maximum = 0.2
		ToleranceField.Increment = 0.01
		ToleranceField.DecimalPlaces = 3
		ToleranceField.Dock = RS_dotNetPreset.DS_Fill
		ToleranceField.Margin = dotNetObject "System.Windows.Forms.Padding" 2
		ToleranceTable.Controls.Add ToleranceField 1 0
		
		FixCheckBox = RS_DotNetUI.InitCheckBox "Fix Border UV's"
		ToleranceTable.Controls.Add FixCheckBox 2 0
		
		Thumbnail = dotNetObject "PictureBox"
		Thumbnail.dock = DS_Fill
		Thumbnail.Margin = dotNetObject "System.Windows.Forms.Padding" 2
		Thumbnail.Image = (dotNetClass "System.Drawing.Image").FromFile LogoPath
		Thumbnail.Size = dotNetObject "System.Drawing.Size" 256 256
		Table.Controls.Add Thumbnail 0 4
		Table.SetColumnSpan Thumbnail 2
		
		InfoPanel = RS_dotNetUI.InitLabel "Select geometry to preview the texture page." RS_dotNetPreset.TA.MiddleLeft
		Table.Controls.Add InfoPanel 0 5
		Table.SetColumnSpan InfoPanel 2

		GenerateButton = RS_dotNetUI.InitButton "Generate Texture Page"
		dotNet.AddEventHandler GenerateButton "Click" GenerateTexturePage
		GenerateButton.Margin = dotNetObject "System.Windows.Forms.Padding" 2
		Table.Controls.Add GenerateButton 0 6
		Table.SetColumnSpan GenerateButton 2
		
		ApplyButton = RS_dotNetUI.InitButton "Apply Texture Page"
		dotNet.AddEventHandler ApplyButton "Click" ApplyTexturePage
		ApplyButton.Margin = dotNetObject "System.Windows.Forms.Padding" 2
		ApplyButton.Enabled = false
		Table.Controls.Add ApplyButton 0 7
		Table.SetColumnSpan ApplyButton 2
		
		--check for existence of LOD texture folder before starting tool
		if (doesFileExist this.DefaultTPagePath) then
		(
			--draw form
			Form.ShowModeless()
			Form
		)		
		else messagebox ("\"" + DefaultTPagePath + "\"" + " not found") title:"Texture Page Builder"
	),
	
	on create do
	(
		TPageOps = TPageStruct()
	)
)

--------------------------------------------------------------------------------------------------------------------------
ClearListener()
TPageBuilder = dotNetObject "TPage.TPageBuilder"

if TPageUI != undefined then
(
	TPageUI.Form.Close()
)

TPageUI = TPageUIStruct()
TPageUI.CreateUI()
