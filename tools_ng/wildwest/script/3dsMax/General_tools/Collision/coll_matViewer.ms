try (destroyDialog RScollisionMaterialsRoll) catch ()

rollout RScollisionMaterialsRoll "Collision Materials" width:300 height:300
(
	local thumbSize = 100
	local minRollSize = [230,60]
	
	local matDefList
	
	local browseView_rowHeight = 70
	
	local showListMode = true
	local showProgress = false
	
	------------------------------------------------------------------------------------
	-- DotNet values: 
	------------------------------------------------------------------------------------
	local textFont, textFontBold, wingDingFont, webDingFont, gridPen
	
	local DNknownColour = dotNetClass "system.Drawing.KnownColor"
	local DNcolour = dotNetClass "System.Drawing.Color"
	
	local selColour = DNcolour.fromKnownColor DNknownColour.MenuHighlight
	
	local textCol = (colorMan.getColor #windowText) * 255
	local windowCol = (colorMan.getColor #window) * 255
	local notLoadedCol = if (windowCol[1] < 128) then (windowCol * 1.5) else (windowCol * 0.85)
	
	local textColour = DNcolour.FromArgb textCol[1] textCol[2] textCol[3]
	
	local backColour = DNcolour.FromArgb windowCol[1] windowCol[2] windowCol[3]
	local notLoadedColour =DNcolour.FromArgb notLoadedCol[1] notLoadedCol[2] notLoadedCol[3]

	local textBrush = dotNetObject "System.Drawing.SolidBrush" textColour
	local whiteBrush = dotNetObject "System.Drawing.SolidBrush" DNcolour.white
	local disabledBrush = dotNetObject "System.Drawing.SolidBrush" DNcolour.gray
	local selectBrush = dotNetObject "System.Drawing.SolidBrush" selColour
	local notLoadedBrush = dotNetObject "System.Drawing.SolidBrush" notLoadedColour
	----------------------------- End of DotNet values -----------------------------
	
	------------------------------------------------------------------------------------
	-- CONTROLS:
	------------------------------------------------------------------------------------
	-- Buttons to change between material-list view-types
	local viewBtnSize = 20
	checkButton listViewBtn "List" tooltip:"Change to detail-list view" checked:showListMode width:viewBtnSize height:viewBtnSize images:#("$ui/icons/Maintoolbar_16i.bmp", undefined, 103, 19, 19, 19, 19, true) visible:true pos:[4,4] across:3
	checkButton browseViewBtn "Browse" tooltip:"Change to material-browse view" checked:(not showListMode) width:viewBtnSize height:viewBtnSize images:#("$ui/icons/Maintoolbar_16i.bmp", undefined, 103, 76, 76, 76, 76, true) pos:(listViewBtn.pos + [viewBtnSize,0]) visible:true
	button refreshListBtn "Refresh" tooltip:"Refresh material-list" width:viewBtnSize height:viewBtnSize images:#("$ui/icons/Maintoolbar_16i.bmp", undefined, 103, 23, 23, 23, 23, true) pos:(browseViewBtn.pos + [viewBtnSize,0]) visible:true

	dotNetControl matListCtrl "RsCustomDataGridView" width:0 height:0 visible:true offset:[-9,-1]
	---------------------------------- End of controls -------------------------------
	
	------------------------------------------------------------------------------------
	-- setListMode: Set up controls for list/browse modes:
	------------------------------------------------------------------------------------
	fn setListMode listMode = 
	(
		showListMode = listMode
		
		listViewBtn.checked = showListMode
		browseViewBtn.checked = not showListMode
		
		-- Remove existing items:
		matListCtrl.rows.clear()
		
		if showListMode then 
		(
			local columnNames = RsCollMatDef.listNames()
			matListCtrl.columnCount = columnNames.count
			for n = 0 to (columnNames.count - 1) do 
			(
				matListCtrl.columns.item[n].name = columnNames[n + 1]
			)
			
			matListCtrl.AutoSizeRowsMode = (dotNetClass "System.Windows.Forms.DataGridViewAutoSizeRowsMode").AllCells
			matListCtrl.RowTemplate.height = 20
			matListCtrl.DefaultCellStyle.WrapMode = (dotNetClass "System.Windows.Forms.DataGridViewTriState").True

			matListCtrl.columns.item[0].AutoSizeMode = (dotNetClass "System.Windows.Forms.DataGridViewAutoSizeColumnMode").None
			
			matListCtrl.ColumnHeadersVisible = true
		)
		else 
		(
			matListCtrl.columnCount = 1
			matListCtrl.columnHeadersVisible = false
			matListCtrl.RowTemplate.height = browseView_rowHeight
			
			matListCtrl.AutoSizeRowsMode = (dotNetClass "System.Windows.Forms.DataGridViewAutoSizeRowsMode").None
		)
		
		matListCtrl.columns.item[matListCtrl.columnCount - 1].AutoSizeMode = (dotNetClass "System.Windows.Forms.DataGridViewAutoSizeColumnMode").Fill
		
		-- Add the items back in:
		if (matDefList == undefined) do 
		(
			matDefList = RsGetCollMatDefsFromColMeshes showProgress:showProgress
		)
		
		local nonDataResizeCols = #{}
		
		for item in matDefList do 
		(
			local rowData = item.listVals()
			
			local rowTextData = #()
			local rowTagData = #()
			
			for n = 1 to rowData.count do 
			(
				local columnVal = rowData[n]
				local itemText
				local itemTag
				
				case classOf columnVal of 
				(
					color:
					(
						-- Colours will sort by hue:
						itemText = columnVal.hue as string
						itemTag = DNcolour.FromArgb columnVal.r columnVal.g columnVal.b
						nonDataResizeCols[n] = true
					)
					array:
					(
						local textStream = stringStream ""
						for n = 1 to columnVal.count do 
						(
							format "%" (toUpper (columnVal[n] as string)) to:textStream
							
							if n != (columnVal.count) do 
							(
								format ", " to:textStream
							)
						)
						itemText = textStream as string
						
						itemTag = dotNetMXSValue columnVal
					)
					booleanClass:
					(
						itemText = if columnVal then "1" else "0"
						itemTag = columnVal
					)
					default:
					(
						itemText = columnVal as string
						itemTag = columnVal
					)
				)
				
				append rowTextData itemText
				append rowTagData itemTag
			)
			
			local newRowNum = matListCtrl.rows.add rowTextData
			local newRow = matListCtrl.rows.item[newRowNum]
			
			-- Set row-tag to material-def struct:
			newRow.tag = dotNetMXSValue item
			
			-- Set up cell-tags:
			if showListMode do 
			(
				for n = 1 to rowData.count do 
				(
					newRow.cells.item[n - 1].tag = rowTagData[n]
				)
			)
		)
		
		-- Resize columns/rows:
		if showListMode do 
		(
			local allCellResizer = (dotNetClass "System.Windows.Forms.DataGridViewAutoSizeColumnMode").AllCells
			local justHeadResizer = (dotNetClass "System.Windows.Forms.DataGridViewAutoSizeColumnMode").ColumnHeader

			for colNum = 0 to (matListCtrl.columns.count - 1) do 
			(
				local resizeMode = if nonDataResizeCols[colNum +1] then justHeadResizer else allCellResizer
				
				matListCtrl.AutoResizeColumn colNum resizeMode
			)
		)
		
		matListCtrl.ClearSelection()
	)
	
	------------------------------------------------------------------------------------
	-- initMatList: Initialise matListCtrl:
	------------------------------------------------------------------------------------
	fn initMatList = 
	(
		matListCtrl.ReadOnly = true
		matListCtrl.AllowUserToAddRows = false
		matListCtrl.AllowUserToDeleteRows = false
		matListCtrl.AllowUserToOrderColumns = true
		matListCtrl.AllowUserToResizeRows = false
		matListCtrl.AllowDrop = false
		matListCtrl.MultiSelect = false
		
		matListCtrl.dock = matListCtrl.dock.fill
		matListCtrl.DefaultCellStyle.backColor = backColour
		matListCtrl.DefaultCellStyle.foreColor = textColour
		
		textFont = matListCtrl.font
		wingDingFont = dotNetObject "System.Drawing.Font" "Wingdings" 16
		webDingFont = dotNetObject "System.Drawing.Font" "Webdings" 16
		textFontBold = dotNetObject "system.drawing.font" textFont (dotnetclass "system.drawing.fontstyle").bold
		gridPen = dotNetObject "System.Drawing.Pen" matListCtrl.gridColor 1
		
		matListCtrl.EnableHeadersVisualStyles = false
		matListCtrl.ColumnHeadersDefaultCellStyle.backColor = backColour
		matListCtrl.ColumnHeadersDefaultCellStyle.foreColor = textColour
		matListCtrl.ColumnHeadersDefaultCellStyle.font = textFontBold
	)
	
	------------------------------------------------------------------------------------
	-- Move controls around to fit window:
	------------------------------------------------------------------------------------
	fn arrangeControls = 
	(
		local rollSize = [RScollisionMaterialsRoll.width, RScollisionMaterialsRoll.height]
		matListCtrl.width = rollSize.x - matListCtrl.pos.x - 4
		matListCtrl.height = rollSize.y - matListCtrl.pos.y - 4
	)
	
	-- Draw cell-item:
	on matListCtrl CellPainting arg do
	(
		if (arg.RowIndex != -1) and (arg.ColumnIndex != -1) do 
		(
			local cellSelected = dotnet.CompareEnums arg.state arg.state.selected
			local backBrush = if cellSelected then selectBrush else (dotNetObject "System.Drawing.SolidBrush" arg.cellStyle.backColor)

			if showListMode then 
			(
				local cellTag = matListCtrl.rows.item[arg.RowIndex].cells.item[arg.ColumnIndex].tag
				
				case of 
				(
					-- Draw ticks/crosses for booleans:
					(isKindOf cellTag BooleanClass):
					(
						arg.handled = true
						
						arg.graphics.FillRectangle backBrush arg.CellBounds
						
						local cellChar = if cellTag then (bit.intAsChar 0xFC) else (bit.intAsChar 0xFB)
						
						arg.graphics.drawString cellChar wingDingFont textBrush (arg.CellBounds.Left + 2) (arg.CellBounds.Top)
					)					
					-- Draw colour-box cell:
					((isKindOf cellTag dotNetObject) and ((cellTag.GetType()).ToString() == "System.Drawing.Color")):
					(
						arg.handled = true
						
						arg.graphics.FillRectangle backBrush arg.CellBounds
						
						local innerRect = dotNetObject "System.Drawing.Rectangle" (arg.CellBounds.Left + 2) (arg.CellBounds.Top + 2) (arg.CellBounds.Width - 6) (arg.CellBounds.Height - 6)
						arg.graphics.FillRectangle (dotNetObject "System.Drawing.SolidBrush" cellTag) innerRect
						arg.graphics.DrawRectangle gridPen innerRect
					)
				)
			)
			else 
			(
				arg.handled = true
				
				local itemLocation = arg.CellBounds.location
				local itemTag = matListCtrl.rows.item[arg.RowIndex].tag.value
				
				arg.graphics.FillRectangle backBrush arg.CellBounds
				
				local writePos = [itemLocation.x + 6, itemLocation.y + 6]
				
				arg.graphics.drawString itemTag.type textFontBold textBrush writePos.x writePos.y
				writePos += [0, 14]
				
				if (itemTag.procType != "") do 
				(
					local colour = matListCtrl.rows.item[arg.RowIndex].tag.value.procTint
					local drawColour = DNcolour.FromArgb colour.r colour.g colour.b
					local colourRect = dotNetObject "System.Drawing.Rectangle" writePos.x writePos.y 16 16
					
					arg.graphics.FillRectangle (dotNetObject "System.Drawing.SolidBrush" drawColour) colourRect
					arg.graphics.DrawRectangle gridPen colourRect
					
					arg.graphics.drawString itemTag.procType textFont textBrush (writePos.x + 18) (writePos.y + 1)
					
					writePos += [0, 20]
				)
				
				-- Draw ped-density:
				writePos += [0, -2]
				local pedDensityColour = if (itemTag.pedDensity == 0) then notLoadedBrush else textBrush
				arg.graphics.drawString (bit.intAsChar 0x80) webDingFont pedDensityColour (writePos.x - 8) writePos.y
				arg.graphics.drawString ("x" + (itemTag.pedDensity as string)) textFontBold pedDensityColour (writePos.x + 10) (writePos.y + 10)
			)
			
			-- Draw grid-lines if cell is user-painted:
			if arg.handled do 
			(
				-- Draw the grid lines (only the right and bottom lines, DataGridView takes care of the others).
				arg.Graphics.DrawLine gridPen arg.CellBounds.Left (arg.CellBounds.Bottom - 1) (arg.CellBounds.Right - 1) (arg.CellBounds.Bottom - 1)
				arg.Graphics.DrawLine gridPen (arg.CellBounds.Right - 1) arg.CellBounds.Top (arg.CellBounds.Right - 1) arg.CellBounds.Bottom
			)
		)
	)
	
	on listViewBtn changed val do 
	(
		setListMode val
	)
	
	on browseViewBtn changed val do 
	(
		setListMode (not val)
	)
	
	on refreshListBtn pressed do 
	(
		matDefList = undefined
		setListMode showListMode
	)

	on RScollisionMaterialsRoll resized newSize do
	(
		-- Don't allow window to be smaller than minRollSize
		if (newSize.x < minRollSize.x) do (RScollisionMaterialsRoll.width = minRollSize.x)
		if (newSize.y < minRollSize.y) do (RScollisionMaterialsRoll.height = minRollSize.y)
	
		-- Rearrange the controls to fit the new window-size
		arrangeControls()
	)
	
	on RScollisionMaterialsRoll open do 
	(
		initMatList()
		arrangeControls()
		setListMode showListMode

		RScollisionMaterialsRoll.width = (matListCtrl.preferredSize.width + matListCtrl.pos.x + 46) - (if ((matListCtrl.preferredSize.height - 25) > RScollisionMaterialsRoll.height) then 7 else 24)
	)
)

-- Creates an Collision Materials Tool window:
fn RsCreateCollMatsRoll = 
(
	try (destroyDialog ::RScollisionMaterialsRoll) catch ()
	
	-- Compile the custom dataGrid control, if required:
	RS_CustomDataGrid()
	
	createDialog RScollisionMaterialsRoll style:#(#style_titlebar, #style_resizing, #style_sysmenu, #style_minimizebox, #style_maximizebox)
)

RsCreateCollMatsRoll()
