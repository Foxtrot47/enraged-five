------------------------------------------------------------------------------------
-- Defines attribute-list for common collision attributes editor rollout:
------------------------------------------------------------------------------------

global RsCollEditAttrRollDef = 
(
	RsCreateObjEditRollDef propClassName:"Gta Collision" rollType:#attr includeAttrs:
	#(
		"Stairs", "Non Climable", "See Through", "No Decal"
	) rollName:"Collision Attributes"
)

global idxIsDynamic = (GetAttrIndex "Gta Object" "Is Dynamic")

------------------------------------------------------------------------------------
-- Structure used to get/edit Collision Type flags
-- (stored in Collision-objects' User Defined Properties text)
-- Accessed via global "gRsCollTypes"
------------------------------------------------------------------------------------
global gRsCollTypes, RsCollAddCallback

if (RsCollAddCallback != undefined) do 
(
	RsCollAddCallback = undefined; gc light:true
)

struct RsCollTypesDef 
(
	name, colour=Green, flags=#{}
)

struct RsCollTypes 
(
	-- Array used to show collision-types in editor combobox:
	typesList = 
	#(
		RsCollTypesDef name:"Mover|Weapons|Horse|Vehicle|Cover" colour:(red + blue),
		RsCollTypesDef name:"Mover|Weapons|Horse|Cover (Not Vehicle)" colour:(orange),
		RsCollTypesDef name:"Mover|Horse|Vehicle|Cover" colour:(yellow),
		RsCollTypesDef name:"Weapons" colour:(blue + green),
		RsCollTypesDef name:"Weapons|Deep Material Surface",
		RsCollTypesDef name:"Horse|Vehicle",
		RsCollTypesDef name:"Horse",
		RsCollTypesDef name:"Vehicle",
		RsCollTypesDef name:"Cover" colour:(color 255 165 177),
		RsCollTypesDef name:"River",
		RsCollTypesDef name:"Foliage",
		RsCollTypesDef name:"Stair Slope",
		RsCollTypesDef name:"Material",
		RsCollTypesDef name:"[None] (Dynamic Only)"
	),
	
	-------------------------------------------------------
	-- Saved to ini-file, loaded on init:
	-------------------------------------------------------
	-- Automatically set collision-colours on create/load:
	autoColourSet = False,
	
	-------------------------------------------------------
	-- Generated on init from typesList array:
	-------------------------------------------------------
	-- All flag-names defined by typesList:
	flagNames = #(),
	
	-- Flag-nums that exclude all other flags:
	singleFlagNums = #{},
	
	-- Extra flag-nums required by given flags, if not specified:
	requiredFlagNums = #(),
	
	-- List of flag-combinations each flag is part of:
	usedFlagCombos = #(),
	
	-- Specific flag-numbers:
	moverNum, weaponsNum,
	
	-- Flags allowed for non-mover weapon-collision:
	weaponNonMoverFlags = #{},
	
	moverListNum, weaponsListNum, moverWeaponsListNum,
	-------------------------------------------------------
	
	--/////////////////////////////////////////
	-- Collision-add event-function (for auto-setting collision-colours)
	--/////////////////////////////////////////
	fn collAddEvent ev handles = 
	(
		-- Clear callback if it shouldn't be active:
		if not gRsCollTypes.autoColourSet do 
		(
			RsCollAddCallback = undefined
			gc light:true
			return false
		)
		
		-- See if any collision-objects have been added:
		local collObjs = for handle in handles collect 
		(
			local obj = getAnimByHandle handle
			
			-- Only pay attention to collision-objects that aren't in closed containers:
			if (isKindOf obj MAXRootNode) or (getAttrClass obj != "Gta Collision") then dontCollect else 
			(
				local closedCont = false
				local parentNode = obj
				
				while (not closedCont) and (parentNode != undefined) do 
				(
					parentNode = parentNode.parent
					closedCont = (isKindOf parentNode MAXRootNode)
				)
				
				if closedCont then dontCollect else obj
			)
		)
		
		-- Update colours for collision-objects:
		if (collObjs.count != 0) do 
		(
			undo off 
			(
				gRsCollTypes.setCollColours collObjs
			)
		)
	),
	
	--/////////////////////////////////////////
	-- Get safely-formatted flag-names from given flag-string:
	--/////////////////////////////////////////
	fn getFlagNamesFromString inString = 
	(
		-- Remove any bracketed comments:
		local stringStart = (filterString inString "[]()" splitEmptyTokens:true)[1]
		
		if (stringStart == undefined) do 
		(
			return #()
		)
		
		-- Split pipe-separated string up into tokens:
		for typeToken in (filterString stringStart "|") collect 
		(
			local typeToken = toLower (RsRemoveSpaces (trimLeft (trimRight (typeToken))))
			
			if (typeToken == "") then dontCollect else typeToken
		)
	),
	
	--/////////////////////////////////////////
	-- Get flag-numbers represented by a given string:
	--/////////////////////////////////////////
	fn getFlagNumsFromString inString = 
	(
		local outVal = #{}
		local stringFlagNames = getFlagNamesFromString inString
		
		for flagName in stringFlagNames do 
		(
			local flagNum = findItem flagNames flagName

			if (flagNum != 0) do 
			(
				outVal[flagNum] = True
			)
		)
		
		return outVal
	),
	
	--/////////////////////////////////////////
	-- Set/get auto-colour-set flag:
	--/////////////////////////////////////////
	fn setAutoColourSet state = 
	(
		if (state == #load) then 
		(
			-- Get pre-set state:
			autoColourSet = RsSettingsReadBoolean "rsCollEdit" "autoColourSet" False
		)
		else 
		(
			-- Set new state:
			autoColourSet = state
			RsSettingWrite "rsCollEdit" "autoColourSet" state
		)
		
		local collObjs = for obj in helpers where (getAttrClass obj == "Gta Collision") collect obj
		
		-- Set collision-colours for scene if activating:
		if autoColourSet then 
		(
			gRsCollTypes.setCollColours collObjs
			
			RsCollAddCallback = NodeEventCallback added:collAddEvent
		)
		else 			
		(
			if (RsCollAddCallback != undefined) do 
			(
				RsCollAddCallback = undefined
				gc light:true
			)
			
			if (state != #load) and (querybox "Reset collision-colours?" title:"Deactivating auto-colouring") do 
			(
				gRsCollTypes.setCollColours collObjs reset:True
			)
		)
	),
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn init = 
	(
		-- Load auto-colouring flag, and set up callback if required:
		setAutoColourSet #load
		
		flagNames = #()
		local listItemFlagNames = #()
		
		-- Collect list of flag-names used by flag-options list:
		for item in typesList do 
		(
			local itemFlagNames = getFlagNamesFromString item.name
			
			join flagNames itemFlagNames
			append listItemFlagNames itemFlagNames
		)
		flagNames = makeUniqueArray flagNames
		
		-- Set special values used to parse flag-rules:
		for flagNum = 1 to flagNames.count do 
		(
			local flagName = flagNames[flagNum]
			local requireds  = #{}
			
			case of 
			(
				-- "Anything with foliage, stairs or river set should clear all other types" (bug:905566)
				-- Material should also clear other types
				((findItem #("foliage", "stair_slope", "river", "material") flagName) != 0):
				(
					singleFlagNums[flagNum] = True
				)
				-- "Anything with weapon, but not mover should be set to weapon" (bug:905566)
				(flagName == "weapons"):
				(
					weaponsNum = flagNum
				)
				-- "Anything with mover set should have horse, vehicle and cover set" (bug:905566)
				-- Amended as required for bug:1155354 which required mover without vehicle
				(flagName == "mover"):
				(
					moverNum = flagNum
					
					for reqName in #("horse", "cover") do 
					(
						local reqNum = findItem flagNames reqName
						
						if (reqNum != 0) do 
						(
							requireds[reqNum] = True
						)
					)
				)
			)
			
			append requiredFlagNums requireds
		)
		
		-- Set appropriate flag-bitarrays for each options-list item:
		for itemNum = 1 to typesList.count do 
		(
			local itemFlagNames = listItemFlagNames[itemNum]
			local listItem = typesList[itemNum]
			
			for flagName in itemFlagNames do 
			(
				local nameNum = findItem flagNames flagName
				listItem.flags[nameNum] = True
			)
			
			local isMover = listItem.flags[moverNum]
			local isWeapons = listItem.flags[weaponsNum]
			
			-- Make note of acceptable flags for non-mover weapon-flag options:
			if isWeapons and not isMover do 
			(				
				weaponNonMoverFlags += listItem.flags
			)
			
			-- Set default option-nums for mover/weapon/mover&weapon:
			case of 
			(
				(isWeapons and isMover):
				(
					if (moverWeaponsListNum == undefined) do 
					(
						moverWeaponsListNum = itemNum
					)
				)
				isMover:
				(
					if (moverListNum == undefined) do 
					(
						moverListNum = itemNum
					)
				)
				isWeapons:
				(
					if (weaponsListNum == undefined) do 
					(
						weaponsListNum = itemNum
					)
				)
			)
		)
		
		-- Make note of which flags are seen with which:
		for flagNum = 1 to flagNames.count do 
		(
			local flagFriends = #{}
			flagFriends.count = flagNames.count
			
			for item in typesList do 
			(
				if item.flags[flagNum] do 
				(
					flagFriends += item.flags
				)
			)
			
			append usedFlagCombos flagFriends
		)
		
		return OK
	),
	
	-- Is obj linked to a Dynamic parent?
	fn hasDynamicParent obj = 
	(
		if (isValidNode obj) and (isValidNode obj.parent) and (GetAttrClass obj.parent == "Gta Object") then (getAttr obj.parent idxIsDynamic) else false
	),
	
	--/////////////////////////////////////////
	-- Apply rules to given flags:
	--/////////////////////////////////////////
	fn processFlags flagsIn obj: justFillGaps:False = 
	(
		local defaultFlags = if (hasDynamicParent obj) then (typesList[typesList.count].flags) else (typesList[1].flags)
		
		-- Replace Undefined flags with the default boolean:
		local newFlagsIn = #{}
		newFlagsIn.count = flagNames.count
		for n = 1 to flagNames.count do 
		(
			newFlagsIn[n] = if (flagsIn[n] == undefined) then defaultFlags[n] else flagsIn[n]
		)
		flagsIn = newFlagsIn
		
		if justFillGaps do return flagsIn
		
		local flagsOut = #{}
		
		case of 
		(
			-- "Anything with foliage, stairs or river set should clear all other types" (bug:905566)
			((flagsIn * singleFlagNums).numberSet != 0):
			(
				-- Set flagsOut to first single-flag found, if more than one is set...
				for singleFlag in (singleFlagNums * flagsIn) while (flagsOut.numberSet == 0) do 
				(
					flagsOut[singleFlag] = True
				)
			)
			-- "Anything with weapon, but not mover should be set to weapon" (bug:905566)
			(flagsIn[weaponsNum] and not flagsIn[moverNum]):
			(
				flagsOut = (flagsIn * weaponNonMoverFlags)
			)
			Default:
			(
				flagsOut += flagsIn
			)
		)
		
		-- "Anything with mover set should have horse, vehicle and cover set" (bug:905566)
		for flagNum in flagsOut do 
		(
			flagsOut += requiredFlagNums[flagNum]
		)
		
		return flagsOut
	),
	
	--/////////////////////////////////////////
	-- Get indices of known type-flags used by obj:
	--/////////////////////////////////////////
	fn getObjFlagNums obj processRules:True &hasFlags: = 
	(
		local objFlags = #()
		
		local checkHasFlags = (isKindOf hasFlags BooleanClass)
		
		for n = 1 to flagNames.count do 
		(
			local getVal = getUserProp obj flagNames[n]
			
			if (isKindOf getVal BooleanClass) do 
			(
--if getVal do (print flagNames[n])
				objFlags[n] = getVal
				
				if checkHasFlags do 
				(
					hasFlags = True
				)
			)
		)

		-- Apply rules to flags:
		if processRules then 
		(
			objFlags = (processFlags objFlags obj:obj)
		)

		return objFlags
	),
	
	--/////////////////////////////////////////
	-- Returns index of list-option with matching flags:
	--/////////////////////////////////////////
	fn getListNumByFlags findFlags = 
	(
--print findFlags
		local findNumSet = findFlags.numberSet
		
		-- If no flags are set, use default option:
		if (findNumSet == 0) do 
		(
--print "None FLAGS"
			return typesList.count
		)

		-- Find list-option with exactly-matching flags:
		local listIdx = 0
		for n = 1 to typesList.count while (listIdx == 0) do 
		(
			local listFlags = typesList[n].flags
			local listNumSet = listFlags.numberSet
			
			if (listNumSet == findNumSet) and ((listFlags * findFlags).numberSet == findNumSet) do 
			(
--format "% * % = % (%: %)\n" typesList[n].flags findFlags (typesList[n].flags * findFlags) n typesList[n].name
				listIdx = n
			)
		)
		
		return listIdx
	),
	
	--/////////////////////////////////////////
	-- Return a flag-display string, for use when flags do not match any TypesList option:
	--/////////////////////////////////////////
	fn getFlagString flags = 
	(
		local addNames = for flagNum in flags collect flagNames[flagNum]
		
		local outVal = stringStream ""
		
		for n = 1 to addNames.count do 
		(
			format "%" addNames[n] to:outVal
			
			if (n != addNames.count) do 
			(
				format "|" to:outVal
			)
		)
		
		return (outVal as string)
	),
	
	--/////////////////////////////////////////
	-- Apply flags from input flag-string onto listed objs:
	--/////////////////////////////////////////
	fn setObjCollTypeFlags collObjs setFlagsString = 
	(
		-- Flags to be set to True:
		-- (may include flags that aren't mentioned in flagNames array)
		local setFlagNames = (getFlagNamesFromString setFlagsString)
		
		-- Get nums for known flags:
		local flagSetNums = #{}
		flagSetNums.count = flagNames.count
		for setFlagName in setFlagNames do 
		(
			local flagNum = findItem flagNames setFlagName
			if (flagNum != 0) do 
			(
				flagSetNums[flagNum] = True
			)
		)
		
		
		-- Invert bitarray to find flags we're setting to False:
		local unsetFlagNames = for flagNum = -flagSetNums collect 
		(
			flagNames[flagNum]
		)
		
		-- Make list of all flag-names that we're going to be outputting:
		local allFlagNames = #()
		join allFlagNames setFlagNames
		join allFlagNames unsetFlagNames
		
		for obj in collObjs do 
		(
			local objUserPropLines = filterstring (getUserPropBuffer obj) "\r\n"
			
			local newBuffer = stringStream ""
			
			-- Filter existing flag-names out of buffer-string:
			for lineString in objUserPropLines do 
			(
				local lineName = toLower (trimLeft (trimRight (filterString lineString "=")[1]))
				
				-- Keep line if we don't recognise the key-name - otherwise ignore it:
				if ((findItem allFlagNames lineName) == 0) do 
				(
					format "%\r\n" lineString to:newBuffer
				)
			)
			
			-- Output all flags to buffer-string:
			for item in #(dataPair names:setFlagNames val:True, dataPair names:unsetFlagNames val:False) do 
			(
				local names = item.names
				local val = item.val
				
				for valName in names do 
				(
					format "% = %\r\n" valName val to:newBuffer
				)
			)
			
			-- Save edited buffer-string to object's User Properties:
			setUserPropBuffer obj (newBuffer as string)
		)
		
		-- Update collision-colours, if option is active:
		if gRsCollTypes.autoColourSet do 
		(
			gRsCollTypes.setCollColours collObjs
		)
		
		OK
	),
	
	--/////////////////////////////////////////
	-- Ensure that objs have valid Collision Type flags set:
	--/////////////////////////////////////////
	fn findBadFlagObjs objs preFiltered:False fix:False ignoreEmpties:True = 
	(
		local foundObjs = #()
		
		local fixFlagBits = #()
		local fixFlagBitStrings = #()
		local fixFlagObjs = #()
		
		for obj in objs where preFiltered or (getAttrClass obj == "Gta Collision") do 
		(
			-- Get flags explicitly set on object:
			local hasFlags = False
			local objFlags = getObjFlagNums obj processRules:false hasFlags:&hasFlags

			-- Don't worry about objects with empty flags, those are set to defaults by exporter:
			if (not ignoreEmpties) or (hasFlags) do 
			(
				-- Set defaults for non-specified flags, convert to bitarray:
				local gotFlags = gRsCollTypes.processFlags objFlags obj:obj justFillGaps:True
				
				-- Apply rules to flags:
				local ruledFlags = gRsCollTypes.processFlags objFlags obj:obj

				-- Apply corrected flags if required:
				if (not hasFlags) or (not ((gotFlags.numberSet == ruledFlags.numberSet) and (gotFlags - ruledFlags).isEmpty)) do 
				(
					if fix do 
					(
						local bitString = (ruledFlags as string)
						local flagsNum = findItem fixFlagBitStrings bitString
						
						if (flagsNum == 0) do 
						(
							append fixFlagBits ruledFlags
							append fixFlagBitStrings bitString
							append fixFlagObjs #()
							flagsNum = fixFlagBits.count
						)
						
						append fixFlagObjs[flagsNum] obj
					)
						
					append foundObjs obj
				)
			)
		)
		
		-- Apply fixes to objects, per flag-combo:
		if fix do 
		(
			for n = 1 to fixFlagBits.count do 
			(
				local flagString = getFlagString fixFlagBits[n]
				setObjCollTypeFlags fixFlagObjs[n] flagString
			)
		)
		
		return foundObjs
	),
	
	--/////////////////////////////////////////
	-- Set volume-colours by collision-type flags:
	--/////////////////////////////////////////
	fn setCollColours collObjs reset:false = 
	(
		undo off 
		(
			for obj in collObjs do 
			(
				if reset then 
				(
					if (isKindOf obj Col_Mesh) then
					(
						-- Restore wirecolor:
						obj.wirecolor = CollisionVolumes.GetNodeColour obj 
						CollisionVolumes.SetNodeColour obj [0,0,0]
					)
					else
					(
						CollisionVolumes.SetUseWireframeColour obj false
					)
				)
				else 
				(
					if (isKindOf obj Col_Mesh) and (CollisionVolumes.GetNodeColour obj == black) do 
					(
						-- Store wirecolor:
						CollisionVolumes.SetNodeColour obj obj.wirecolor
					)
					CollisionVolumes.SetUseWireframeColour obj true
					
					local objFlags = gRsCollTypes.getObjFlagNums obj
					local listIdx = gRsCollTypes.getListNumByFlags objFlags
					local newColour = if (listIdx == 0) then (yellow) else (gRsCollTypes.typesList[listIdx].colour)
					
					if (obj.wirecolor != newColour) do 
					(
						obj.wirecolor = newColour
					)
				)
			)
		)
	),
	
	--/////////////////////////////////////////
	-- These functions return true if obj has particular flags:
	--/////////////////////////////////////////
	
	-- True if obj matches both mover/weapons flags:
	fn hasMoverWeaponsCombo obj mover:True weapons:True = 
	(
		local objFlags = (getObjFlagNums obj)
		
		local moverFlag = objFlags[moverNum]
		local weaponFlag = objFlags[weaponsNum]
		
		return ((mover == moverFlag) and (weapons == weaponFlag))
	),
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn hasWeaponsFlag obj = 
	(
		local objFlags = (getObjFlagNums obj)
		return objFlags[weaponsNum]
	)
)

-- Set up gRsCollTypes struct-instance, and initialise RsCollAddCallback (if used)
gRsCollTypes = RsCollTypes()
gRsCollTypes.init()
