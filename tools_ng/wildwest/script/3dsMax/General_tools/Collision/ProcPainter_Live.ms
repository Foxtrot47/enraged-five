global gRsCollPaintToolLive = undefined

fileIn (::RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Collision/ProcPainter_Funcs.ms")

------------------------------------------------------------------------------------
-- Structures: 
------------------------------------------------------------------------------------
-- Structure representing a single procedural type change
struct RsProcTypeChange
(
	positions = #([0,0,0]),
	
	procType = 0,
	density = 3,
	scaleZ = 8,
	scale3 = 3,
	colour = [1,1,1,1],
	
	hashVal,
	fn setHashVal = 
	(
		hashVal = getHashValue #(procType, density, scaleZ, scale3, colour[1], colour[2], colour[3], colour[4]) 1
	)
)

struct RsCollPaintToolLive 
(
	isConnected = false,
	connectionTimer = undefined, --Timer used to periodically verify that a connection has occurred.
	timerInterval = 5000, --Tick every 5,000 milliseconds 

	------------------------------------------------------------------------------------
	-- RAG Widgets: 
	------------------------------------------------------------------------------------
	
	SearchSphereRadiusWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Search Options/Sphere Radius",
	SearchShowPolysWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Search Options/Show Plant Polys",
	EditColor0WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground Color at vertex#0",
	EditColor1WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground Color at vertex#1",
	EditColor2WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground Color at vertex#2",
	EditTypeWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Type",
	EditDensity0WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground Density weight at vertex#0 (maps to [1, 0.6, 0.3, 0])",
	EditDensity1WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground Density weight at vertex#1 (maps to [1, 0.6, 0.3, 0])",
	EditDensity2WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground Density weight at vertex#2 (maps to [1, 0.6, 0.3, 0])",
	EditScaleZ0WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground ScaleZ weight at vertex#0 (maps to [1, 0.6, 0.3, 0])",
	EditScaleZ1WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground ScaleZ weight at vertex#1 (maps to [1, 0.6, 0.3, 0])",
	EditScaleZ2WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground ScaleZ weight at vertex#2 (maps to [1, 0.6, 0.3, 0])",
	EditScaleXYZ0WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground ScaleXYZ weight at vertex#0 (maps to <-1; 1>)",
	EditScaleXYZ1WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground ScaleXYZ weight at vertex#1 (maps to <-1; 1>)",
	EditScaleXYZ2WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground ScaleXYZ weight at vertex#2 (maps to <-1; 1>)",
	EditSaveBatchWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Save changes to batch",
	EditReadFromFileWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Read Input From File",
	MaxUIReadingFromFileWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Max UI/Reading input from file",
	MaxUISavingBatchWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Max UI/Saving changes to batch",

	------------------------------------------------------------------------------------
	-- Constants:
	------------------------------------------------------------------------------------
	searchRadius = 0.1,
	batchTriLimit = 500,
	batchFileName = "x:/procdata.bin",
	groundTintChannel = 11,
	scaleDensityChannel = 12,
	
	-- Set by UpdateLiveData:
	hasScaleDensityChannel,
	hasGroundTintChannel,

	------------------------------------------------------------------------------------
	-- Functions: 
	------------------------------------------------------------------------------------
	-- Tick called every 5 seconds to enure the remote connection is still valid
	fn OnTick s e =
	(
		if (RemoteConnection.IsConnected() == false and gRsCollPaintToolLive.isConnected == true) then
		(
			--If connection has faltered, then notify the UI.
			print ("Lost connection to the game!")
			::RsCollisionMaterialPaintRoll.btnLiveLink.checked = false
			gRsCollPaintToolLive.OnDisconnect()
		)
	),

	-- Called when the user attempts to connect to the RAG proxy
	fn OnConnect =
	(
		pushPrompt "Attempting to connect to RAG proxy..."
		
		-- Call into DLL to establish connection.	
		connectionResult = RemoteConnection.Connect()
		
		if connectionResult == false then
		(
			format "Unable to establish connection to the game!\n"
			
			::RsCollisionMaterialPaintRoll.btnLiveLink.checked = false
		)
		else
		(
			--Create a periodic timer to ping the connection status in the assembly.
			connectionTimer = dotNetObject "System.Windows.Forms.Timer"
			dotnet.addEventHandler connectionTimer "tick" OnTick
			connectionTimer.Interval = timerInterval
			connectionTimer.Start()
			
			isConnected = true
		)
		
		popPrompt()
	),

	-- Called when the user disconnects from the RAG proxy
	fn OnDisconnect =
	(	
		pushPrompt "Disconnecting from RAG proxy..."
		
		if connectionTimer != undefined then
		(
			--Stop the ongoing timer.
			connectionTimer.Stop()
		)
		
		RemoteConnection.Disconnect()			
		isConnected = false
		
		popPrompt()
	),

	-- Creates a proc type change from a mesh
	fn CreateProcTypeChange obj objMesh faceIdx faceMat =
	(
		-- Create the change object
		local change = RsProcTypeChange()
		
		-- Face position
		change.positions[1] = meshOp.getFaceCenter objMesh faceIdx node:obj
		
		-- Procedural type
		if (isKindOf faceMat RexBoundMtl) do 
		(
			matName = RexGetProceduralName faceMat
			selNum = findItem RsProceduralTypeData.names matName
			
			-- Check whether the procedural type was present
			if (selNum != 0) then
			(
				grassItem = selNum - RsProceduralTypeData.numProcObjs
				change.procType = (128 + grassItem - 1)
				
				-- Density/Scale values
				if hasScaleDensityChannel do 
				(
					local faceVerts = meshOp.getMapFace objMesh scaleDensityChannel faceIdx
					local val = meshOp.getMapVert objMesh scaleDensityChannel faceVerts[1]
					
					change.scaleZ = int(floor((val.x * 3) + 0.5))
					change.scale3 = int(floor((val.y * 15) + 0.5))
					change.density = int(floor((val.z * 3) + 0.5))
				)

				-- Ground Tint
				if hasGroundTintChannel do 
				(
					local faceVerts = meshOp.getMapFace objMesh groundTintChannel faceIdx
					local val = meshOp.getMapVert objMesh groundTintChannel faceVerts[1]
					
					change.colour = (val as point4)
					change.colour.w = 1.0
				)
			)
		)
		
		-- Set searchable hash-value:
		change.setHashVal()
		
		return change
	),

	-- Creates a file containing the list of positions
	fn CreateBatchedPositionFile positions =
	(
		local file = fopen batchFileName "wb"
		writeLong file (positions.count*3)

		for position in positions do
		(
			writeFloat file position.x
			writeFloat file position.y
			writeFloat file position.z
		)
		fclose file
	),

	-- Send a list of batched changes to the game
	fn SendBatchedChangesToGame batchedChanges =
	(
		local keepGoing = true
		
		-- Process all the batches
		--progressStart "Sending collision-data to game"
		
		-- Filter batches by proc type:
		local useChanges = for batch in batchedChanges where (batch.procType >= 128) collect batch
		
		-- Make sure the plant polys aren't shown
		RemoteConnection.WriteBoolWidget SearchShowPolysWidgetName false
		
		for batchNum = 1 to useChanges.count while keepGoing do 
		(
			local batch = useChanges[batchNum]
			--print "Processing Batch"
			--print batch
			
			-- Create the file containing the list of positions
			CreateBatchedPositionFile batch.positions
			
			-- Set up the search radius
			RemoteConnection.WriteFloatWidget SearchSphereRadiusWidgetName searchRadius
			
			-- Tell the game to load the tri's from the file
			RemoteConnection.SendCommand("widget \"" + EditReadFromFileWidgetName + "\"")
			
			-- Wait for the tris to be selected in game before continuing
			local waitingForSelection = true
			local attempts = 0
			local maxAttempts = 25
			
			while (attempts < maxAttempts) and (waitingForSelection == true) do
			(
				attempts += 1

				waitingForSelection = (RemoteConnection.ReadBoolWidget MaxUIReadingFromFileWidgetName)
				RemoteConnection.SendSyncCommand()
				sleep 0.01
				
				-- Offer to cancel if this has been attempted more than a set number of times:
				if (attempts >= maxAttempts) do 
				(
					keepGoing = queryBox "No response from RAG!\n\nPlease ensure that RAG is set to show:\n__Live Edit__ -> PlantsMgr Data Edit\n\nContinue trying to send?" title:"Warning: Send Error"
					
					if keepGoing then 
					(
						attempts = 0
					)
					else 
					(
						waitingForSelection = false
					)
				)
			)
			
			if (keepGoing == true) then
			(
				-- Update the various RAG widgets with their new values
				RemoteConnection.WriteIntWidget EditTypeWidgetName batch.procType

				if hasScaleDensityChannel do 
				(
					RemoteConnection.WriteIntWidget EditScaleZ0WidgetName batch.scaleZ
					RemoteConnection.WriteIntWidget EditScaleZ1WidgetName batch.scaleZ
					RemoteConnection.WriteIntWidget EditScaleZ2WidgetName batch.scaleZ
					
					RemoteConnection.WriteIntWidget EditScaleXYZ0WidgetName batch.scale3
					RemoteConnection.WriteIntWidget EditScaleXYZ1WidgetName batch.scale3
					RemoteConnection.WriteIntWidget EditScaleXYZ2WidgetName batch.scale3
					
					RemoteConnection.WriteIntWidget EditDensity0WidgetName batch.density
					RemoteConnection.WriteIntWidget EditDensity1WidgetName batch.density
					RemoteConnection.WriteIntWidget EditDensity2WidgetName batch.density
				)
				
				if hasGroundTintChannel do 
				(
					RemoteConnection.WriteVector4Widget EditColor0WidgetName batch.colour
					RemoteConnection.WriteVector4Widget EditColor1WidgetName batch.colour
					RemoteConnection.WriteVector4Widget EditColor2WidgetName batch.colour
				)
				
				-- Save the changes
				RemoteConnection.SendCommand("widget \"" + EditSaveBatchWidgetName + "\"")
				RemoteConnection.SendSyncCommand()
				
				-- Wait for the values to have finished saving before continuing
				local waitingForSave = true
				attempts = 0
				maxAttempts = 10
				
				while (attempts < maxAttempts) and (waitingForSave == true) do
				(
					attempts += 1

					waitingForSave = (RemoteConnection.ReadBoolWidget MaxUISavingBatchWidgetName)
					RemoteConnection.SendSyncCommand()
					sleep 0.01
					--print "..."
					
					-- Offer to cancel if this has been attempted more than a set number of times:
					if (attempts >= maxAttempts) do 
					(
						keepGoing = queryBox "No response from RAG!\n\nPlease ensure that RAG is set to show:\n__Live Edit__ -> PlantsMgr Data Edit\n\nContinue trying to send?" title:"Warning: Send Error"
						
						if keepGoing then 
						(
							attempts = 0
						)
						else 
						(
							waitingForSave = false
						)
					)
				)
			)
			
			if keepGoing do 
			(
				keepGoing = progressUpdate (100.0 * batchNum / useChanges.count)
			)
		)
		
		--progressEnd()
			
		return keepGoing
	),

	-- Goes through all faces in the collision object and attempts to update their data in game via rag
	fn UpdateLiveData colObj = 
	(
		local updateString = "Updating data for $" + colObj.Name
		
		pushPrompt updateString		
		format "%\n" updateString
		
		--gRsUlog.init "Procedural Type Live Update" appendToFile:false
		
		-- We should be getting a collision mesh object through
		if (isKindOf colObj col_mesh) then
		(
			local objMesh = getColMesh colObj
			local faceCount = getNumFaces objMesh
			
			-- Get the list of materials
			local getMatFaces = #()
			local objMaterials = RsGetMaterialsOnObjFaces colObj faceLists:getMatFaces
			
			hasScaleDensityChannel = (meshOp.getMapSupport objMesh scaleDensityChannel)
			hasGroundTintChannel = (meshOp.getMapSupport objMesh groundTintChannel)
			
			-- Make list of materials per face:
			local matsPerFace = #()
			matsPerFace.count = faceCount
			for matNum = 1 to objMaterials.count do 
			(
				local mat = objMaterials[matNum]
				
				for faceNum = getMatFaces[matNum] do 
				(
					matsPerFace[faceNum] = mat
				)
			)
			
			-- Array containing the changes we are after
			local batchHashes = #()
			local batchedChanges = #()
			
			-- Keep track of any faces that didn't have plant procedural types applied
			local nonPlantFaces = #{}
			nonPlantFaces.count = faceCount
			
			-- Generate the list of batched changes
			for faceIdx = 1 to faceCount do 
			(
				-- create an object representing the change
				local change = CreateProcTypeChange colObj objMesh faceIdx matsPerFace[faceIdx]
				--print change
				
				-- Attempt to add the item to an existing batch (note that batches are limited to 500 tri's)
				local findBatchNum = findItem batchHashes change.hashVal
				
				if (findBatchNum == 0) then 
				(
					append batchHashes change.hashVal
					append batchedChanges change
				)
				else 
				(
					append batchedChanges[findBatchNum].positions change.positions[1]
					
					-- Clear this search-hash from list when tri-limit is found, to force creation of new entry for next matching face-data:
					if (batchedChanges[findBatchNum].positions.count == batchTriLimit) do 
					(
						batchHashes[findBatchNum] = undefined
					)
				)
				
				if (change.procType < 128) do 
				(
					nonPlantFaces[faceIdx] = true
				)
			)
			
			-- Now we have a list of all the batched changes, send them to the game
			local success = SendBatchedChangesToGame batchedChanges
			
			if success do 
			(
				local endMessage = stringStream ""
				format "Collision-data game-transfer completed!\n(% batches sent)" batchedChanges.count to:endMessage
				
				if (nonPlantFaces.numberSet > 0) do 
				(
					format "\n\n% of % faces didn't have a Plant procedural-type applied, and won't have been updated." nonPlantFaces.numberSet faceCount to:endMessage
				)
				
				messageBox (endMessage as string) title:"Live-Link transfer completed"
			)
			
			--gRsUlog.Validate()
		)
		else
		(
			format "Error! UpdateLiveData() Expected a collision mesh\n"
		)
		
		popPrompt()
	)
)

gRsCollPaintToolLive = RsCollPaintToolLive()
--gRsCollPaintToolLive.OnConnect()
--gRsCollPaintToolLive.UpdateLiveData $
