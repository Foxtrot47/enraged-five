using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;

namespace ProceduralSystem
{
    public class NodeCollection : INotifyPropertyChanged
    {
        ///-----------------------------------------------------------------------------------------------
        #region FIELDS AND PROPERTIES
        ///-----------------------------------------------------------------------------------------------

        //Owning painter tool
        public Painter Owner { get; private set; }

        //Defines whether we have changes to this object data that need saving
        public Boolean _UnsavedDataChanges = false;
        public Boolean UnsavedDataChanges
        {
            get { return _UnsavedDataChanges; }
            set
            {
                _UnsavedDataChanges = value;
                OnPropertyChanged("UnsavedDataChanges");
            }
        }

        //Stores the associated NodeFaceData (which contains the procedural levels) against FaceIndex keys.
        public Dictionary<Int32, NodeFaceData> NodeFaceDataStore { get; private set; }

        //List of NodeDatas associated with the collection
        public ObservableCollection<NodeData> NodeDataList { get; private set; }

        //Exposed for iteration in MXS
        public NodeData[] NodeDataArray
        {
            get { return NodeDataList.ToArray(); }
        }

        #endregion

        ///-----------------------------------------------------------------------------------------------
        #region CONSTRUCTORS
        ///-----------------------------------------------------------------------------------------------

        public NodeCollection(Painter InputOwner)
        {
            this.Owner = InputOwner;
            this.NodeFaceDataStore = new Dictionary<Int32, NodeFaceData>();
            this.NodeDataList = new ObservableCollection<NodeData>();
        }

        #endregion

        ///-----------------------------------------------------------------------------------------------
        #region METHODS
        ///-----------------------------------------------------------------------------------------------

        //Creates a new NodeData from the passed information
        public NodeData CreateNode(Int32 NodeHandle, String NodeName, Int32 FaceCount)
        {
            var NewNodeData = new NodeData(NodeHandle, NodeName, FaceCount, this);
            NodeDataList.Add(NewNodeData);
            return NewNodeData;
        }

        //Clears the collection of all data
        public void Clear()
        {
            NodeFaceDataStore.Clear();
            NodeDataList.Clear();

            UnsavedDataChanges = false;
        }

        //Gets the set procedural level on the passed face index. 
        //Returns null if a procedural is not set.
        public Procedural.Level GetProceduralLevel(Int32 InputFaceIndex)
        {
            //Attempt to get the procedural level for the passed face
            Procedural.Level AppliedLevel = null;
            if (NodeFaceDataStore.ContainsKey(InputFaceIndex))
            {
                AppliedLevel = NodeFaceDataStore[InputFaceIndex].ProceduralLevel;
            }

            return AppliedLevel;
        }

        //Sets the procedural level on the passed face indexes
        public void SetProceduralLevel(Int32[] InputFaceIndexes, Procedural.Level InputProceduralLevel)
        {
            //Iterated through each of the passed faces and setting them
            foreach (var FaceIndex in InputFaceIndexes)
            {
                NodeFaceDataStore[FaceIndex].ProceduralLevel = InputProceduralLevel;
            }

            //Update our applied face counts to reflect changes
            Owner.UpdateAppliedCounts();

            //Then we update our applied set
            Owner.UpdateAppliedSet();
        }

        //Gets all the procedural levels set on the given faces
        public Procedural.Level[] GetProceduralLevelList(Int32[] InputFaceIndexes)
        {
            var ProceduralLevelMatches = from FaceIndex in InputFaceIndexes
                                         select NodeFaceDataStore[FaceIndex].ProceduralLevel;

            return ProceduralLevelMatches.Distinct().ToArray();
        }

        //Gets all the procedurals set on the given faces
        public Procedural[] GetProceduralList(Int32[] InputFaceIndexes)
        {
            var ProceduralMatches = from FaceIndex in InputFaceIndexes
                                         select NodeFaceDataStore[FaceIndex].ProceduralLevel.ParentProcedural;

            return ProceduralMatches.Distinct().ToArray();
        }

        //Gets all the face indexes that use the passed procedural level.
        //Search is retricted to the faces in the passed range.
        public Int32[] GetFaceListByProceduralLevel(Procedural.Level InputProceduralLevel, Int32[] InputRangeFaceIndexes)
        {
            var FaceIndexMatches = from FaceIndex in InputRangeFaceIndexes
                                   where
                                       NodeFaceDataStore[FaceIndex].ProceduralLevel == InputProceduralLevel
                                   select FaceIndex;

            return FaceIndexMatches.ToArray();
        }

        //Gets all the face indexes that use a procedural level that is a child of the procedural.
        //Search is retricted to the faces in the passed range.
        public Int32[] GetFaceListByProcedural(Procedural InputProcedural, Int32[] InputRangeFaceIndexes)
        {
            var FaceIndexMatches = from FaceIndex in InputRangeFaceIndexes
                                   where
                                       NodeFaceDataStore[FaceIndex].ProceduralLevel.ParentProcedural == InputProcedural
                                   select FaceIndex;

            return FaceIndexMatches.ToArray();
        }

        //Adds the passed NodeFaceData to the next slot of our NodeFaceDataStore
        internal void AppendNodeFaceData(NodeFaceData InputNodeFaceData)
        {
            NodeFaceDataStore[NodeFaceDataStore.Count + 1] = InputNodeFaceData;
        }

        public void ClearCollisionTargetFaces()
        {
            foreach (var SubNodeFaceData in NodeFaceDataStore.Values)
            {
                SubNodeFaceData.CollisionTargetFaceList.Clear();
            }
        }

        //Gets a live face update batch from the input face indexes. Used for Live updating
        public LiveUpdateFaceBatch[] GetLiveUpdateFaceBatches(Int32[] InputRangeFaceIndexes)
        {
            //Create our return CollisionFaceBatch list
            List<LiveUpdateFaceBatch> LiveUpdateFaceBatchList = new List<LiveUpdateFaceBatch>();

            //For each face in the range provided
            foreach (Int32 Index in InputRangeFaceIndexes)
            {
                //Get the corrolating NodeFaceData
                var TargetNodeFaceData = NodeFaceDataStore[Index];
                
                //Check the node face data has some collisions
                if (TargetNodeFaceData.CollisionTargetFaceList.Count == 0) { continue; }

                //Get the best batch 
                var TargetBatch = (from Batch in LiveUpdateFaceBatchList
                                    where Batch.TargetProceduralLevel == TargetNodeFaceData.ProceduralLevel && Batch.NotFull
                                    select Batch).FirstOrDefault();

                //If no batches are suitable make a new one
                if (TargetBatch == null)
                {
                    TargetBatch = new LiveUpdateFaceBatch(TargetNodeFaceData.ProceduralLevel);
                    LiveUpdateFaceBatchList.Add(TargetBatch);
                }

                //Add the collision face to this batch
                TargetBatch.AddNodeFaceData(TargetNodeFaceData);
            }

            //Return the collision batch list as an array for use in mxs
            return LiveUpdateFaceBatchList.ToArray();
        }

        //Gets the node face data that corrosponds the passed handle and face index
        public NodeFaceData GetNodeFaceData(Int32 NodeHandle, Int32 FaceIndex)
        {
            var MatchingNodeDatas = from ND in NodeDataList where ND.Handle == NodeHandle select ND;

            if (MatchingNodeDatas.Count() == 0)
            { return null; }
            else
            { return (MatchingNodeDatas.First().NodeFaceDataArray[FaceIndex-1]); }
        }

        //Gets the node face data that corrosponds the passed handle and face index from the input face indexes.
        public NodeFaceData GetNodeFaceData( Int32 NodeHandle, Int32 FaceIndex, Int32[] InputRangeFaceIndexes )
        {
            var MatchingNodeDatas = from RangeFaceIndex in InputRangeFaceIndexes where NodeFaceDataStore[RangeFaceIndex].ParentNode.Handle == NodeHandle && NodeFaceDataStore[RangeFaceIndex].FaceIndex == FaceIndex select NodeFaceDataStore[RangeFaceIndex];

            if (MatchingNodeDatas.Count() == 0)
            { return null; }
            else
            { return MatchingNodeDatas.First(); }
        }

        #endregion

        ///-----------------------------------------------------------------------------------------------
        #region EVENTS
        ///-----------------------------------------------------------------------------------------------

        //Event to signal a property has changed. Used for data binding.
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        void OnPropertyChanged(String PropertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }

        #endregion
    }

    public class NodeData
    {
        List<NodeFaceData> _NodeFaceDataList;
        public NodeFaceData[] NodeFaceDataArray
        {
            get { return _NodeFaceDataList.ToArray(); }
        }
        public NodeCollection Parent { get; private set; }
        public Int32 Handle { get; private set; }
        public String Name { get; private set; }

        public NodeData(Int32 InputHandle, String InputName, Int32 InputFaceCount, NodeCollection InputParent)
        {
            this.Handle = InputHandle;
            this.Name = InputName;
            this.Parent = InputParent;
            this._NodeFaceDataList = new List<NodeFaceData>(InputFaceCount);
        }

        public void CreateFaceData(Int32 InputFaceIndex, Procedural.Level InputProceduralLevel)
        {
            var NewNodeFaceData = new NodeFaceData(InputFaceIndex, InputProceduralLevel, this);

            _NodeFaceDataList.Add(NewNodeFaceData);
            Parent.AppendNodeFaceData(NewNodeFaceData);
        }
    }

    public class NodeFaceData
    {
        Procedural.Level _ProceduralLevel;

        public Int32 FaceIndex { get; private set; }
        public NodeData ParentNode { get; private set; }
        public List<CollisionTargetFace> CollisionTargetFaceList { get; private set; }
        public CollisionTargetFace[] CollisionTargetFaceArray
        {
            get { return CollisionTargetFaceList.ToArray(); }
        }
        public Boolean DataAltered { get; set; }

        public Procedural.Level ProceduralLevel
        {
            get { return _ProceduralLevel; }
            set
            {
                if (_ProceduralLevel != null)
                {
                    _ProceduralLevel.AppliedFaceCount -= 1;
                }

                _ProceduralLevel = value;
                _ProceduralLevel.AppliedFaceCount += 1;
                DataAltered = true;
            }
        }

        public void AddCollisionTargetFace(Int32 InputNodeHandle, Int32 InputFaceIndex, Single InputFaceCenterX, Single InputFaceCenterY, Single InputFaceCenterZ)
        {
            CollisionTargetFaceList.Add(new CollisionTargetFace(InputNodeHandle, InputFaceIndex, InputFaceCenterX, InputFaceCenterY, InputFaceCenterZ));
        }

        public NodeFaceData(Int32 InputFaceIndex, Procedural.Level InputProceduralLevel, NodeData InputParentNode)
        {
            this.ParentNode = InputParentNode;
            this.FaceIndex = InputFaceIndex;
            this.ProceduralLevel = InputProceduralLevel;
            this.DataAltered = false;
            this.CollisionTargetFaceList = new List<CollisionTargetFace>();
        }
    }

    //Stores information about a face on a collision target
    public class CollisionTargetFace
    {
        public Int32 NodeHandle { get; private set; }
        public Int32 FaceIndex { get; private set; }
        public Single FaceCenterX { get; private set; }
        public Single FaceCenterY { get; private set; }
        public Single FaceCenterZ { get; private set; }

        public CollisionTargetFace(Int32 InputNodeHandle, Int32 InputFaceIndex, Single InputFaceCenterX, Single InputFaceCenterY, Single InputFaceCenterZ)
        {
            this.NodeHandle = InputNodeHandle;
            this.FaceIndex = InputFaceIndex;
            this.FaceCenterX = InputFaceCenterX;
            this.FaceCenterY = InputFaceCenterY;
            this.FaceCenterZ = InputFaceCenterZ;
        }
    }

    public class LiveUpdateFaceBatch
    {
        public static Int32 BatchFaceLimit = 80;

        List<NodeFaceData> _NodeFaceDataList = new List<NodeFaceData>();
        public NodeFaceData[] NodeFaceDataArray { get { return _NodeFaceDataList.ToArray(); } }

        public Procedural.Level TargetProceduralLevel { get; private set; }

        public Boolean NotFull
        {
            get 
            {
                var CollisionTargetFaces = from NFD in _NodeFaceDataList
                                           from CTF in NFD.CollisionTargetFaceList
                                           select CTF;
                return ( CollisionTargetFaces.Count() < BatchFaceLimit ); 
            }
        }

        public void AddNodeFaceData(NodeFaceData InputNodeFaceData)
        {
            _NodeFaceDataList.Add(InputNodeFaceData);
        }

        public LiveUpdateFaceBatch(Procedural.Level InputProceduralLevel)
        {
            this.TargetProceduralLevel = InputProceduralLevel;
        }
    }
}