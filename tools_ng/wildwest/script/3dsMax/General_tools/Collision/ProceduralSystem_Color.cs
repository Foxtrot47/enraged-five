using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.ComponentModel;

namespace ProceduralSystem
{
    public class Color : INotifyPropertyChanged, IEquatable<Color>
    {
        Double _H = 0;
        public Double H
        {
            get { return _H; }
            set
            {
                FromHSV(value, _S, _V);
                OnPropertyChanged("HexCode");
            }
        }
        Double _S = 0.0;
        public Double S
        {
            get { return _S; }
            set
            {
                FromHSV(_H, value, _V);
                OnPropertyChanged("HexCode");
            }
        }
        Double _V = 0.0;
        public Double V
        {
            get { return _V; }
            set
            {
                FromHSV(_H, _S, value);
                OnPropertyChanged("HexCode");
            }
        }

        //We expose this property mainly for data binding
        public String HexCode
        {
            get
            {
                var LocalColor = System.Drawing.Color.FromArgb(R, G, B);
                return System.Drawing.ColorTranslator.ToHtml(LocalColor);
            }
            set
            {
                var LocalColor = System.Drawing.ColorTranslator.FromHtml(value);
                FromRGB(LocalColor.R, LocalColor.G, LocalColor.B);
                OnPropertyChanged("HexCode");
            }
        }

        public Byte R { get; private set; }
        public Byte G { get; private set; }
        public Byte B { get; private set; }

        public Double ScR
        {
            get
            {
                return R / 255d;
            }
        }
        public Double ScG
        {
            get
            {
                return G / 255d;
            }
        }
        public Double ScB
        {
            get
            {
                return B / 255d;
            }
        }

        public void FromRGB(Byte InputR, Byte InputG, Byte InputB)
        {
            var LocalColor = System.Drawing.Color.FromArgb(InputR, InputG, InputB);

            int max = Math.Max(LocalColor.R, Math.Max(LocalColor.G, LocalColor.B));
            int min = Math.Min(LocalColor.R, Math.Min(LocalColor.G, LocalColor.B));

            var NewH = LocalColor.GetHue();
            var NewS = (max == 0) ? 0 : 1d - (1d * min / max);
            var NewV = max / 255d;

            FromHSV(NewH, NewS, NewV);

            OnPropertyChanged("HexCode");
        }

        public void FromScRGB(Double InputR, Double InputG, Double InputB)
        {
            if (InputR < 0) { InputR = 0; }
            if (InputR > 1) { InputR = 1; }
            if (InputG < 0) { InputG = 0; }
            if (InputG > 1) { InputG = 1; }
            if (InputB < 0) { InputB = 0; }
            if (InputB > 1) { InputB = 1; }

            Byte LocalR = Convert.ToByte(InputR * 255);
            Byte LocalG = Convert.ToByte(InputG * 255);
            Byte LocalB = Convert.ToByte(InputB * 255);

            this.FromRGB(LocalR, LocalG, LocalB);
        }

        public void FromHSV(double InputH, double InputS, double InputV)
        {
            //Sanitise values
            if (InputH > 360) { InputH = 360; }
            if (InputH < 0) { InputH = 0; }
            if (InputS > 1) { InputS = 1; }
            if (InputS < 0) { InputS = 0; }
            if (InputV > 1) { InputV = 1; }
            if (InputV < 0) { InputV = 0; }

            _H = InputH;
            _S = InputS;
            _V = InputV;


            int hi = Convert.ToInt32(Math.Floor(InputH / 60)) % 6;
            double f = InputH / 60 - Math.Floor(InputH / 60);

            InputV = InputV * 255;
            Byte v = Convert.ToByte(InputV);
            Byte p = Convert.ToByte(InputV * (1 - InputS));
            Byte q = Convert.ToByte(InputV * (1 - f * InputS));
            Byte t = Convert.ToByte(InputV * (1 - (1 - f) * InputS));

            switch (hi)
            {
                case 0: { R = v; G = t; B = p; break; }
                case 1: { R = q; G = v; B = p; break; }
                case 2: { R = p; G = v; B = t; break; }
                case 3: { R = p; G = q; B = v; break; }
                case 4: { R = t; G = p; B = v; break; }
                default: { R = v; G = p; B = q; break; }
            }

            OnPropertyChanged("HexCode");
        }

        public Color()
        {
            _H = 0.0;
            _S = 0.0;
            _V = 0.0;

            R = 0;
            G = 0;
            B = 0;
        }

        public Color(Byte InputR, Byte InputG, Byte InputB)
        {
            FromRGB(InputR, InputG, InputB);
        }

        public Color(Int32 InputR, Int32 InputG, Int32 InputB)
        {
            FromRGB((Byte)InputR, (Byte)InputG, (Byte)InputB);
        }

        public Color(Double InputH, Double InputS, Double InputV)
        {
            FromHSV(InputH, InputS, InputV);
        }

        public Boolean Equals(Color InputColor)
        {
            if (this == InputColor) { return true; }

            if (this.R == InputColor.R &&
                this.B == InputColor.B &&
                this.G == InputColor.G)
            { return true; }
            else
            { return false; }
        }

        public override int GetHashCode()
        {
            return (this.R + this.G + this.B);
        }

        public override bool Equals(object obj)
        {
            if (obj is Color)
            {
                return (this.Equals(obj as Color));
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return String.Format("[{0:000}][{1:000}][{2:000}]", this.R, this.G, this.B);
        }
        ///-----------------------------------------------------------------------------------------------
        #region EVENTS
        ///-----------------------------------------------------------------------------------------------

        //Event to signal that a property has changed. Used for data binding.
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        void OnPropertyChanged(String PropertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }

        #endregion
        ///-----------------------------------------------------------------------------------------------


    }
}
