using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;

namespace ProceduralSystem
{
    public class Set : INotifyPropertyChanged
    {
        [XmlAttribute]
        public String Name { get; set; }
        [XmlElement("ProceduralHue")]
        public ObservableCollection<Int32> ProceduralLinkList { get; set; }
        [XmlIgnore]
        public ObservableCollection<Procedural> ProceduralList { get; set; }
        [XmlIgnore]
        public Procedural[] ProceduralArray
        {
            get
            { return ProceduralList.ToArray(); }
        }
        [XmlIgnore]
        private SetData _Parent;
        [XmlIgnore]
        public SetData Parent
        {
            get
            {
                return _Parent;
            }
            set
            {
                _Parent = value;
                InitializeProceduralList();
            }
        }
        
        public Set()
        {
            this.Name = String.Empty;
            this.ProceduralLinkList = new ObservableCollection<Int32>();
            this.ProceduralList = new ObservableCollection<Procedural>();
        }

        public override string ToString()
        {
            return String.Format("{0} [{1} Procedurals]", this.Name, this.ProceduralList.Count );
        }

        //This method gets called when the parent of the set changes. This method will fetch 
        //and fillout the ProceduralList with the Procedurals that corrospond to the Handles 
        //in ProceduralLinkList. This is done via the parent property. It will also add a 
        //handler to monitor future updates to the ProceduralList and propogate those changes 
        //to the ProceduralLinkList.
        void InitializeProceduralList()
        {
            if (Parent != null)
            {
                ProceduralList.Clear();

                foreach (var LinkHue in ProceduralLinkList)
                {
                    var TargetProcedural = Parent.GetProcedural(LinkHue);
                    if (TargetProcedural != null)
                    {
                        ProceduralList.Add(TargetProcedural);
                        TargetProcedural.PropertyChanged += Procedural_PropertyChanged;

                    }
                    else
                    {
                        throw new Exception("Failed to link to a procedural by the given hue.");
                    }
                }

                ProceduralList.CollectionChanged += ProceduralList_CollectionChanged;
            }
        }

        //Tracks changes made to procedurals
        void Procedural_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //If it was the color hue we will rebuild the ProceduralLinkList
            if (e.PropertyName == "ColorHue")
            {
                ProceduralLinkList.Clear();
                foreach (var SubProcedural in ProceduralList)
                {
                    ProceduralLinkList.Add( SubProcedural.ColorHue );
                }
            }
        }

        [XmlIgnore]
        public Boolean HasProcedurals
        {
            get
            {
                return ( ProceduralList.Count > 0 );
            }
        }

        //Pushes changes made to ProceduralList to the ProceduralLinkList. This means that any changes made to the 
        //ProceduralList made via binding will be reflected in the ProceduralLinkList.
        void ProceduralList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (Procedural RemovedProcedural in e.OldItems)
                {
                    ProceduralLinkList.Remove(RemovedProcedural.ColorHue);
                }
            }

            if (e.NewItems != null)
            {
                foreach (Procedural AddedProcedural in e.NewItems)
                {
                    ProceduralLinkList.Add(AddedProcedural.ColorHue);
                }
            }

            OnPropertyChanged("HasProcedurals");
        }


        //Event to signal that a property has changed. Used for data binding.
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        void OnPropertyChanged(String PropertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }

    }
}