
try (destroydialog RsCollEditRoll) catch ()
callbacks.removescripts id:#RsCollEditRollCallbacks

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsCollEditRoll "Collision Edit" width:349
(
	label lblSetCollType "Get/set settings for selected/created collision:" offset:[-6, 0] height:14 align:#left across:2
	
	hyperlink lnkHelp "Help?" offset:[6,0] align:#right address:"https://devstar.rockstargames.com/wiki/index.php/Collision_Toolkit#Collision_Edit" color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	group "Collision Type:"
	(
		label lblCollTypes "XXX" offset:[0,-6]
		listBox lstCollTypes "" offset:[0,-2] height:5
	)

	local favBtnWidth = 163
	local favBtnHeight = 16
	group "Surface-Type:"
	(
		label lblSurfType "Category:\n\nName:" width:50 height:40 across:2 align:#left
		dropDownList cboCategories "" align:#right width:200 offset:[3,-2]
		dropDownList cboCollision "" align:#right width:200 pos:(cboCategories.pos + [0, 26])
		
		label lblFavSurfs "Favourites: [right-click to change]" align:#left
		button btnFavSurfA "" width:favBtnWidth height:favBtnHeight offset:[-3,-3] across:2
		button btnFavSurfB "" width:favBtnWidth height:favBtnHeight offset:[3,-3]
		button btnFavSurfC "" width:favBtnWidth height:favBtnHeight offset:[-3,-3]across:2
		button btnFavSurfD "" width:favBtnWidth height:favBtnHeight offset:[3,-3]
		button btnFavSurfE "" width:favBtnWidth height:favBtnHeight offset:[-3,-3] across:2
		button btnFavSurfF "" width:favBtnWidth height:favBtnHeight offset:[3,-3]
		button btnFavSurfG "" width:favBtnWidth height:favBtnHeight offset:[-3,-3] across:2
		button btnFavSurfH "" width:favBtnWidth height:favBtnHeight offset:[3,-3]
	)
	
	local favSurfCtrls = #(btnFavSurfA, btnFavSurfB, btnFavSurfC, btnFavSurfD, btnFavSurfE, btnFavSurfF, btnFavSurfG, btnFavSurfH)

	button btnReset "^    Reset To Defaults    ^" width:lstCollTypes.width height:24 offset:[-1,0] tooltip:"Resets Collision/Surface Type controls to default settings.\n\nAlso resets settings on selected collision."
	
	--------------------------------------------------------------
	-- COLLISION-TYPE CHECKBOX FUNCTIONS:
	--------------------------------------------------------------
	
	-- Set up controls to show the selected collision-objects' collision-type booleans, or default if none:
	fn showSelObjCollTypes = 
	(
		local indetColType = False
		local collObjs = for obj in selection where (getAttrClass obj == "Gta Collision") collect obj

		-- Find flags used on each object, stopping if multiple values are found:
		local flagsVal
		for obj in collObjs while (not indetColType) do 
		(
			local objFlags = gRsCollTypes.getObjFlagNums obj
			
			if (flagsVal == undefined) or ((flagsVal.numberSet == objFlags.numberSet) and ((flagsVal - objFlags).isEmpty)) then 
			(
				flagsVal = objFlags
			)
			else 
			(
				indetColType = True
			)
		)
		
		case of 
		(
			(flagsVal == undefined):
			(
				lstCollTypes.selection = 1
				lblCollTypes.text = gRsCollTypes.typesList[1].name
--format "SETTING TO DEFAULT\n"
			)
			(indetColType):
			(
				-- If multiple flag-combinations are selected:
				lstCollTypes.selection = 0
				lblCollTypes.text = "[multiple combinations]"
			)
			Default:
			(
				local listNum = gRsCollTypes.getListNumByFlags flagsVal

				if (listNum != 0) then 
				(
					-- Set combobox list to item if found:
					lstCollTypes.selection = listNum
					lblCollTypes.text = gRsCollTypes.typesList[listNum].name
--format "SETTING TO: %\n" listNum
--print gRsCollTypes.typesList[listNum].name
				)
				else 
				(
					-- If flag-combination was unknown, show it in text-box:
					local setText = gRsCollTypes.getFlagString flagsVal
--format "SETTING FLAG-TEXT: %\n" setText
					lstCollTypes.selection = 0
					lblCollTypes.text = setText
				)
			)
		)
		
		OK
	)
	
	-- Apply Collision Type options to selected collision-objects:
	fn setSelObjCollTypes collObjs listNum:0 = 
	(
		-- Don't set values if collision-type selection is indeterminate:
		if (lstCollTypes.selection != 0) do 
		(
			local flagString = lstCollTypes.selected
			local doApply = True

			-- Using the flagString, check through the collObjs and see if any are not valid for this flag combo:
			if (matchPattern flagString pattern:"*Dynamic Only*") do 
			(
				local idxIsDynamic = GetAttrIndex "Gta Object" "Is Dynamic"
				doApply = (collObjs.count != 0)

				for obj in collObjs while doApply do
				(
					-- Is the type of this object valid for this collision combo?
					doApply = ((getAttrClass obj.parent == "Gta Object") and (getAttr obj.parent idxIsDynamic))
				)
				
				if not doApply do 
				(
					messageBox ("\"" + flagString + "\" can only be applied to dynamic props.\n\nCollision unchanged.") title:"Flags Mismatch" 
				)
			)
			
			if doApply then 
			(
				gRsCollTypes.setObjCollTypeFlags collObjs flagString
			)
			else 
			(
				-- Reset controls to show current object-flags (or default if no collision is selected)
				showSelObjCollTypes()
			)
		)
	)
	
	fn collTypeChanged listNum:0 = 
	(
		local collObjs = for obj in selection where (getAttrClass obj == "Gta Collision") collect obj
		setSelObjCollTypes collObjs listNum:listNum
		
		listNum = lstCollTypes.selection
		
		if (listNum != 0) do 
		(
			lblCollTypes.text = gRsCollTypes.typesList[listNum].name
		)
	)
	on lstCollTypes selected num do 
	(
		collTypeChanged listNum:num
	)
	
	on btnFindBadFlags pressed do 
	(
		local selObjs = gRsCollTypes.findBadFlagObjs objects fix:False
		clearSelection()
		select selObjs
		
		if (selObjs.count != 0) do 
		(
			local msg = stringStream ""
			format "% objects have inappropriate Collision Type flags\n(stored in User Defined Properties)\n\nThey may have conflicting flags, or be missing some required by set flags." selObjs.count to:msg

			messageBox (msg as string) title:"Found Bad Collision Type Flags"
		)
	)
	on btnFixBadFlags pressed do 
	(
		local selObjs = gRsCollTypes.findBadFlagObjs objects fix:True
		
		clearSelection()
		select selObjs
		
		if (selObjs.count != 0) do 
		(
			local msg = stringStream ""
			format "Applied fixes to % objects with inappropriate Collision Type flag-data." selObjs.count to:msg

			messageBox (msg as string) title:"Fixed Bad Collision Type Flags"
		)
	)

	--------------------------------------------------------------
	-- SURFACE-TYPE FUNCTIONS:
	--------------------------------------------------------------
	-- Load collsion-surfaces:
	local collMatEntries = RsCollMatDef.loadCollisionEntries()
	
	struct RsCollCategory (name, entries = #())
	local RsCollCategories = #()
	local RsCollItems = #()
	
	-- Set up the Type Category dropdown list:
	fn setSurfTypeCats = 
	(
		RsCollCategories = #()
		
		local allMatCat = RsCollCategory name:"[ALL]"
		local catNameList = #()
		local matNameList = #()
		
		for item in collMatEntries do 
		(
			local findNum = findItem catNameList item.category
			
			if (findNum == 0) do 
			(
				append catNameList item.category
				append RsCollCategories (RsCollCategory name:item.category)
				findNum = RsCollCategories.count
			)
			
			append RsCollCategories[findNum].entries item.name
			append allMatCat.entries item.name
		)
		
		sort catNameList
		insertItem allMatCat RsCollCategories 1
		insertItem allMatCat.name catNameList 1
		
		cboCategories.items = catNameList
		cboCategories.selection = 1
	)
	
	-- Get surface-type used on selected collision-objects (if only one is used)
	fn getSelObjSurfTypes = 
	(
		local valCollTypes = #()
		
		for obj in selection where (getattrclass obj == "Gta Collision") do 
		(
			appendIfUnique valCollTypes (toUpper (getattr obj RsIdxCollType))
		)
		
		return valCollTypes
	)
	
	fn sortDefaultToTop v1 v2 = 
	(
		case of 
		(
			(matchPattern v1 pattern:"default"):-1
			(matchPattern v2 pattern:"default"):1
			Default:(striCmp v1 v2)
		)
	)

	-- Set the Surface-Type name-list based on the selected category-name:
	fn showCatSurfTypes arg = 
	(
		RsCollItems = #()				
		
		if arg != 0 then (	
			idxCat = 0
			
			for i = 1 to RsCollCategories.count do 
			(
				if RsCollCategories[i].name == cboCategories.items[arg] do 
				(
					idxCat = i
				)
			)
		
			RsCollItems = RsCollItems + RsCollCategories[idxCat].entries
		)
	
		qsort RsCollItems sortDefaultToTop

		cboCollision.items = RsCollItems
	)

	-- Select the surface-type name, based on current collision-selection:
	fn showSurfTypeSelection valCollTypes:(getSelObjSurfTypes()) = 
	(
		if (valCollTypes.count == 1) then 
		(
			local valCollType = valCollTypes[1]
			showCatSurfTypes cboCategories.selection

			local idxFound = finditem cboCollision.items valCollType
			
			if (idxFound == 0) then 
			(
				cboCategories.selection = 1
				showCatSurfTypes 1
				idxFound = finditem cboCollision.items valCollType
			)
			
			cboCollision.selection = idxFound
		)
		else 
		(
			-- Set default category:
			if (cboCategories.selection != 1) do 
			(
				cboCategories.selection = 1
				showCatSurfTypes 1
			)
			
			if (valCollTypes.count == 0) then 
			(
				-- Set to DEFAULT if object-selection uses no surface-types:
				cboCollision.selection = 1
			)
			else 
			(
				-- Set indeterminate surface-type if object-selection uses multiple surface-types:
				cboCollision.selection = 0
			)
		)
	)

	-- Set selected collision-objects to use the selected surface-type:
	fn SetSurfType collObjs collName:cboCollision.selected = 
	(
		if (cboCollision.selection != 0) do 
		(
			for obj in collObjs do 
			(
				setattr obj RsIdxCollType collName
			)
		)
	)
	
	-- Set controls to defaults:
	fn setDefaultCollCtrls doReset:False = 
	(
		local collObjs
		if doReset do 
		(
			collObjs = for obj in selection where (getAttrClass obj == "Gta Collision") collect obj
		)
		
		-- Set controls to defaults:
		lstCollTypes.selection = 1
		lblCollTypes.text = gRsCollTypes.typesList[1].name
		
		cboCollision.selection = 1
		
		if doReset do 
		(
			cboCategories.selection = 1

			setSelObjCollTypes collObjs
			SetSurfType collObjs
		)
	)
	
	-- Set surface and collision types on objects, from current control settings:
	fn SetCollAndSurfType objs = 
	(
		local collObjs = for obj in objs where (isValidNode obj) and (getattrclass obj == "Gta Collision") collect obj
		
		if (collObjs.count != 0) do 
		(
			SetSurfType collObjs
			SetSelObjCollTypes collObjs
			
			local setNoDecal = ((::RsCollCreateRoll != undefined) and (::RsCollCreateRoll.open) and (::RsCollCreateRoll.chkSetNoDecal.checked))
			
			-- Apply "No Decal" to cylinders/capsules/spheres, or all collisions if option is ticked
			for obj in collObjs where setNoDecal or ((isKindOf obj Col_Cylinder) or (isKindOf obj Col_Capsule) or (isKindOf obj Col_Sphere)) do 
			(
				setAttr obj RsIdxNoDecal True
			)
		)
	)
	
	-- Updates rollout controls based on selected objects, called by selection-change callback:
	-- (only actually updates controls if any collision-objects are selected)
	fn showFromObjSelection = 
	(
		local hasCollSel = False
		for obj in selection where (getattrclass obj == "Gta Collision") while (hasCollSel != True) do 
		(
			hasCollSel = True
		)
		
		if hasCollSel then
		(
			-- Update controls to show current selection's values:
			setDefaultCollCtrls()
			showSelObjCollTypes()
			showSurfTypeSelection()
		)
		else
		(
			-- Set to default type-flags option if no collision was selected and option is currently indeterminate from previous selection
			
			if (lstCollTypes.selection == 0) do
			(
				lstCollTypes.selection = 1
				lblCollTypes.text = gRsCollTypes.typesList[1].name
			)
			
		)
	)
	
	--////////////////////////////////////////////////////////////
	-- EVENTS
	--////////////////////////////////////////////////////////////
	
	on btnReset pressed do 
	(
		setDefaultCollCtrls doReset:True
	)
	
	--------------------------------------------------------------
	-- Surface-Type dropdowns:
	--------------------------------------------------------------	
	on cboCategories selected arg do 
	(
		showCatSurfTypes arg
		
		cboCollision.selection = finditem cboCollision.items (getSelObjSurfTypes())[1]
	)
	
	on cboCollision selected arg do 
	(
		local collObjs = for obj in selection where (getattrclass obj == "Gta Collision") collect obj
		SetSurfType collObjs
	)
	
	-- Load data for favourite-buttons:
	fn favSurfsLoad = 
	(
		for num = 1 to favSurfCtrls.count do 
		(
			local ctrl = favSurfCtrls[num]
			local numString = formattedPrint num format:"02i"
			ctrl.text = RsSettingRead "rsCollEdit" ("favSurface_" + numString) "DEFAULT"
		)
	)
	-- Set surface-type to string shown in numbered button:
	fn favSurfSet num = 
	(
		local ctrl = favSurfCtrls[num]
		local collType = ctrl.text
		
		local collObjs = for obj in selection where (getAttrClass obj == "Gta Collision") collect obj
		
		if (collObjs.count != 0) do 
		(
			SetSurfType collObjs collName:collType
		)
		
		showSurfTypeSelection valCollTypes:#(collType)
	)
	-- Change favourite-button's text to current surface-type:
	fn favSurfChange num = 
	(
		local getSurf = cboCollision.selected
		
		local ctrl = favSurfCtrls[num]
		ctrl.text = getSurf
		
		local numString = formattedPrint num format:"02i"
		RsSettingWrite "rsCollEdit" ("favSurface_" + numString) getSurf
	)
	on btnFavSurfA pressed do (favSurfSet 1)
	on btnFavSurfB pressed do (favSurfSet 2)
	on btnFavSurfC pressed do (favSurfSet 3)
	on btnFavSurfD pressed do (favSurfSet 4)
	on btnFavSurfE pressed do (favSurfSet 5)
	on btnFavSurfF pressed do (favSurfSet 6)
	on btnFavSurfG pressed do (favSurfSet 7)
	on btnFavSurfH pressed do (favSurfSet 8)
	on btnFavSurfA rightClick do (favSurfChange 1)
	on btnFavSurfB rightClick do (favSurfChange 2)
	on btnFavSurfC rightClick do (favSurfChange 3)
	on btnFavSurfD rightClick do (favSurfChange 4)
	on btnFavSurfE rightClick do (favSurfChange 5)
	on btnFavSurfF rightClick do (favSurfChange 6)
	on btnFavSurfG rightClick do (favSurfChange 7)
	on btnFavSurfH rightClick do (favSurfChange 8)
	
	--------------------------------------------------------------
	-- Rollout open/close
	--------------------------------------------------------------
	on RsCollEditRoll open do 
	(
		RsCollConvertSourceMats = undefined
		
		-- Set up Collision Type options-list:
		lstCollTypes.items = for item in gRsCollTypes.typesList collect item.name

 		setSurfTypeCats()
		
		setDefaultCollCtrls()
		showFromObjSelection()
		
		showCatSurfTypes 1
		
		favSurfsLoad()
		
		callbacks.addscript #selectionSetChanged "RsCollEditRoll.showFromObjSelection()" id:#RsCollEditRollCallbacks
	)
	
	on RsCollEditRoll close do 
	(
		callbacks.removescripts id:#RsCollEditRollCallbacks
	)
	
	on RsCollEditRoll rolledUp down do 
	(
		RsSettingWrite "RsCollEditRoll" "rollup" (not down)
	)
)

--createDialog RsCollEditRoll
