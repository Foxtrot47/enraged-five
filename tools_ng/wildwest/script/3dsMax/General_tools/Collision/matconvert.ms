--
-- File:: matconvert.ms
-- Desc:: material converter
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: Pre-GTA4, updated 5 December 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global RsCollConvertSourceMats = #()
global RsCollConvertTargetMats = #()

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsLoadConvertFile
-- desc: Load <common>/data/materials/mtl_convert.txt
--
fn RsLoadConvertFile filename = 
(
	-- Make sure we have latest version:
	gRsPerforce.sync filename
	
	RsCollConvertSourceMats = #()
	RsCollConvertTargetMats = #()

	datfile = openfile filename
	
	if datfile == undefined then return false
	
	while eof datfile == false do (
	
		datline = readline datfile
		
		if datline.count > 0 and datline[1] != "#" then (
		
			lineTokens = filterstring datline " \t"
			
			if ( 2 == lineTokens.count ) then
			(
				lineTokens[1] = toUpper lineTokens[1]
				lineTokens[2] = toUpper lineTokens[2]
				
--				format "Map % ==> %\n" lineTokens[1] lineTokens[2]
				
				append RsCollConvertSourceMats lineTokens[1]
				append RsCollConvertTargetMats lineTokens[2]
			)
		)
	)
	
	close datfile
	
	( RsCollConvertSourceMats.count == RsCollConvertTargetMats.count )
)

--
-- name: RsConvertCollisionOnMaterial
-- desc:
--
fn RsConvertCollisionOnMaterial mat = 
(
	if (isKindOf mat RexBoundMtl) do 
	(
		-- Convert obsolete collision-types, or set to uppercase if it isn't already:
		local matSurfType = RexGetCollisionName mat
		local useSurfType = (toUpper matSurfType)
		local idFound = finditem RsCollConvertSourceMats useSurfType 
		
		if (idFound != 0) do 
		(
			useSurfType = RsCollConvertTargetMats[idFound]
		)
		if (useSurfType != matSurfType) do 
		(
			format "%: converting material from % to %\n" mat.name matSurfType useSurfType
			RexSetCollisionName mat useSurfType
		)
	)
)

--
-- name: RsConvertCollisionOnObjectCollision
-- desc:
--
fn RsConvertCollisionOnObjectCollision childobj = 
(
	if (getattrclass childobj != "Gta Collision") do return False
	
	-- Convert obsolete collision-types attributes, or set to uppercase if it isn't already:
	local idxSurfaceType = getattrindex "Gta Collision" "Coll Type"
	local valSurfaceType = (getattr childobj idxSurfaceType)
	local useSurfType = (toUpper valSurfaceType)
	local idFound = finditem RsCollConvertSourceMats useSurfType 
	
	if (idFound != 0) do 
	(
		useSurfType = RsCollConvertSourceMats[idFound]
	)
	if (useSurfType != valSurfaceType) do 
	(
		format "%: converting attribute from % to %\n" childobj.name valSurfaceType useSurfType
		setattr childobj idxSurfaceType useSurfType 
	)
	
	-- Convert obsolete collision-material types:
	if (isKindOf childObj Col_Mesh) do 
	(	
		case (classOf childobj.material) of 
		(
			Multimaterial:
			(
				for childmat in childobj.material.materiallist do 
				(
					if isKindOf childmat RexBoundMtl do 
					(
						RsConvertCollisionOnMaterial childmat		
					)
				)
			)
			RexBoundMtl:
			(
				RsConvertCollisionOnMaterial childobj.material
			)
			Default:
			(
				local boundmat = RexBoundMtl()
				RexSetCollisionName boundmat RsCollConvertTargetMats[idFound]
				childobj.material = Multimaterial()
				childobj.material.materiallist[1] = boundmat
				
				savetrans = childobj.transform
				col2mesh childobj

				numFaces = getnumfaces childobj

				for i = 1 to numFaces do 
				(
					SetfaceMatID childobj i 1
				)
				
				mesh2col childobj
				childobj.transform = savetrans
			) 
		)	
	)
)

--
-- name: RsConvertCollisionOnObject
-- desc:
--
fn RsConvertCollisionOnObject obj = 
(
	if getattrclass obj == "Gta Object" then 
	(
		for childobj in obj.children do 
		(
			RsConvertCollisionOnObjectCollision childobj
		)
	) 
	else 
	(
		RsConvertCollisionOnObjectCollision obj
	)
)
