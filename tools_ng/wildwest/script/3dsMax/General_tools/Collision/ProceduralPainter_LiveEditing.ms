--Big list of widgets used in live editing. Bit clunky but the way the system works.
LiveEditProcTypeWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Type",
LiveEditReadInputFileWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Read input from file",

LiveEditDensity0WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground Density weight at vertex#0 (maps to [1, 0.6, 0.3, 0])",
LiveEditDensity1WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground Density weight at vertex#1 (maps to [1, 0.6, 0.3, 0])",
LiveEditDensity2WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground Density weight at vertex#2 (maps to [1, 0.6, 0.3, 0])",

LiveEditScaleZ0WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground ScaleZ weight at vertex#0 (maps to [1, 0.6, 0.3, 0])",
LiveEditScaleZ1WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground ScaleZ weight at vertex#1 (maps to [1, 0.6, 0.3, 0])",
LiveEditScaleZ2WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground ScaleZ weight at vertex#2 (maps to [1, 0.6, 0.3, 0])",

LiveEditScaleXYZ0WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground ScaleXYZ weight at vertex#0 (maps to <0; 1>)",
LiveEditScaleXYZ1WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground ScaleXYZ weight at vertex#1 (maps to <0; 1>)",
LiveEditScaleXYZ2WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground ScaleXYZ weight at vertex#2 (maps to <0; 1>)",

LiveEditGroundTint0WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground Color at vertex#0",
LiveEditGroundTint1WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground Color at vertex#1",
LiveEditGroundTint2WidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Ground Color at vertex#2",

LiveEditWriteSelectionWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Write selection to file",
LiveEditSphereRadiusWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Search Options/Sphere Radius",
LiveEditSphereCenterWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Search Options/Sphere Center",
LiveEditSearchWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Search Options/Search",
LiveEditAllowPickingWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Search Options/Allow Picking",
LiveEditShowPlantPolysWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Search Options/Show Plant Polys",
LiveEditAllCollisionSelectableWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Search Options/All Collision Selectable",
LiveEditCommitChangesWidgetName = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Save changes to batch",
LiveEditClearFaces = "_LiveEdit_/PlantsMgr Data Edit/Search Options/Clear Selected Entries",
LiveEditReadingInputFile = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Max UI/Reading input from file",
LiveEditSavingChanges = "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Max UI/Saving changes to batch",

CameraMtxPositionXWidgetName = "Camera/Free camera/Position X",
CameraMtxPositionYWidgetName = "Camera/Free camera/Position Y",
CameraMtxPositionZWidgetName = "Camera/Free camera/Position Z",


/*------------------------------------------------------------------------------ 	
Reads in LocTri data exported from the game.
*/------------------------------------------------------------------------------
fn ReadExportedLocTriList =
(
	--The binary file the LocTri data is written to
	ExportedLocTriDataFilepath = "X:\\procdataOut.bin"
	
	if not (RsFileExists ExportedLocTriDataFilepath) then
	(
		MessageBox ( ExportedLocTriDataFilepath + " has not been written out from the game!" )
		return Undefined
	)
	
	-- File layout:
	------------------------------------------
	-- header: 1 int (4 bytes)
	-- 9 floats (36 bytes): 3xXYZ positions of verts
	-- 9 bytes: 3xRGB ground color data
	-- 3 bytes: 3xScaleXYZ [0-15]
	-- 3 bytes: 3xScaleZ [0-4]
	-- 3 bytes: 3xDensity [0-4]
	-- 2 bytes: control footer data (=0xBEEF).
	
	--Load the file and parse the header which will contain the number of LocTris we are dealing with.
	ExportedLocTriDataFile = FOpen ExportedLocTriDataFilepath "rb"
	NumberOfExportedLocTris = ReadLong ExportedLocTriDataFile
	
	--Iterate through each of the LocTris
	ReturnExportedLocTriList = #()
	for i = 1 to NumberOfExportedLocTris do
	(
		CurrentExportedLocTri = ExportedLocTri()
		
		--Get vert positions
		for Vert = 1 to 3 do
		(
			VertX = ReadFloat ExportedLocTriDataFile
			VertY = ReadFloat ExportedLocTriDataFile
			VertZ = ReadFloat ExportedLocTriDataFile
			
			Vert = [ VertX, VertY, VertZ ]
			Append CurrentExportedLocTri.VertList Vert
		)
		
		--Derive face center (used for matching faces)
		CurrentExportedLocTri.Center = ( CurrentExportedLocTri.VertList[1] + CurrentExportedLocTri.VertList[2] + CurrentExportedLocTri.VertList[3] ) /3
		
		--Figure out the area
		CurrentExportedLocTri.Area = TriangleArea #(CurrentExportedLocTri.VertList[1], CurrentExportedLocTri.VertList[2], CurrentExportedLocTri.VertList[3] )
			
		--Get ground tint colors
		for GroundTint = 1 to 3 do
		(
			--Oddly the numbers come through as minus the remainder of the 256 range
			GroundTintR = 256 + ( ReadByte ExportedLocTriDataFile )
			GroundTintG = 256 + ( ReadByte ExportedLocTriDataFile )
			GroundTintB = 256 + ( ReadByte ExportedLocTriDataFile )
			
			GroundTint = [ GroundTintR, GroundTintG, GroundTintB ]
			Append CurrentExportedLocTri.GroundTintList GroundTint
		)
		
		--Get the ground tint average. Data is exported per face max side so setting 
		--individual vert colors is not representative of what will get exported
		CurrentExportedLocTri.GroundTint = ( CurrentExportedLocTri.GroundTintList[1] + CurrentExportedLocTri.GroundTintList[2] + CurrentExportedLocTri.GroundTintList[3] ) /3
		
		--Get scale and density values. Take the second value (they will likely all be the 
		--same - since this data now is set per face and not blended in game)
		ScaleXYZ_1 = ReadByte ExportedLocTriDataFile
		ScaleXYZ_2 = ReadByte ExportedLocTriDataFile
		ScaleXYZ_3 = ReadByte ExportedLocTriDataFile
		CurrentExportedLocTri.ScaleXYZLevel = ScaleXYZ_2
		
		ScaleZ_1 = ReadByte ExportedLocTriDataFile
		ScaleZ_2 = ReadByte ExportedLocTriDataFile
		ScaleZ_3 = ReadByte ExportedLocTriDataFile
		CurrentExportedLocTri.ScaleZLevel = ScaleZ_2
		
		Density_1 = ReadByte ExportedLocTriDataFile
		Density_2 = ReadByte ExportedLocTriDataFile
		Density_3 = ReadByte ExportedLocTriDataFile
		CurrentExportedLocTri.DensityLevel = Density_2
		
		--Read the footer to bring us to the correct location for the next LocTri
		Append ReturnExportedLocTriList CurrentExportedLocTri
		Footer = ReadShort ExportedLocTriDataFile
	)

	--Close file and return data
	FClose ExportedLocTriDataFile
	return ReturnExportedLocTriList
),

/*------------------------------------------------------------------------------ 	
Creates a point helper for each LocTri face center. Useful for checking the data is valid.
*/------------------------------------------------------------------------------
fn DebugExportedLocTriList =
(
	--Read in the exported face data
	ExportedLocTriList = this.ReadExportedLocTriList()
	if( ExportedLocTriList == Undefined or ExportedLocTriList.Count == 0 ) then
	(
		return Undefined
	)
	
	--for each of the LocTris
	for SubExportedLocTri in ExportedLocTriList do
	(
		TestPoint = Point Size:0.1
		TestPoint.Position = SubExportedLocTri.Center
		
		--print (TriangleArea #(SubExportedLocTri.VertList[1], SubExportedLocTri.VertList[2], SubExportedLocTri.VertList[3]))
	)
),

/*------------------------------------------------------------------------------
Trys to connect to the game and if not then throws a message up and returns False
*/------------------------------------------------------------------------------
fn LiveConnected =
(
	if RemoteConnection.Connect() then return True else
	(
		MessageBox "Connection to the game could not be established." Title:"Live Edit Connection"
		return False
	)
),

/*------------------------------------------------------------------------------
Triggers an export of selected faces from within game. Then it imports this data 
and injects it directly into matching collision target faces.
*/------------------------------------------------------------------------------
fn LiveImport =
(
	--Check we are connected
	if not LiveConnected() then return False
	
	--Make the game output the current selection
	RemoteConnection.WriteBoolWidget this.LiveEditWriteSelectionWidgetName True
	RemoteConnection.SendSyncCommand()
	
	--Read in the exported face data return if we have nothing
	ExportedLocTriList = this.ReadExportedLocTriList()
	if( ExportedLocTriList == Undefined or ExportedLocTriList.Count == 0 ) then return ()

	--First ensure that our collision target spatial hash set is upto date
	this.UpdateCollisionTargetSpatialHashSet()
	
	--for each of the LocTris
	for SubExportedLocTri in ExportedLocTriList do
	(
		--Get closest entry (closest target collision face). Use a very low threshold so the match should be practically exact. 
		--Threshold can't be 0 because of floating point inprecision
		FaceCollisionTargetSpatialEntry = this.CollisionTargetSpatialHashSet.GetClosestEntry SubExportedLocTri.Center Threshold:0.005
		
		--If we got a target collision face match
		if( FaceCollisionTargetSpatialEntry != Undefined ) then
		(
			--Extract the target and face index
			CollisionTarget = FaceCollisionTargetSpatialEntry.Value.Node
			FaceIndex = FaceCollisionTargetSpatialEntry.Value.FaceIndex
			
			--Convert to EditMesh
			Col2Mesh CollisionTarget
			
			--Derive and set the ground tint map value. Note we must divide by 255.0 to get FP number in range 0.0-1.0
			GroundTintMapValue = SubExportedLocTri.GroundTint / 255.0
			RSMesh_SetFace_MapValue CollisionTarget FaceIndex this.CollisionGroundTintChannel GroundTintMapValue
			
			--Setup our override channel
			if not( MeshOp.GetMapSupport CollisionTarget this.CollisionGroundTintOverrideChannel ) then ( RSMesh_ResetMapChannel CollisionTarget this.CollisionGroundTintOverrideChannel [0,0,0] )
			RSMesh_SetFace_MapValue CollisionTarget FaceIndex this.CollisionGroundTintOverrideChannel GroundTintMapValue
			
			--Derive and set the density/scale map value.
			--ScaleZ, ScaleXYZ, Density
			DensityScaleMapValue = [ SubExportedLocTri.ScaleZLevel / 3.0, SubExportedLocTri.ScaleXYZLevel / 15.0,  SubExportedLocTri.DensityLevel / 3.0 ]
			
			--Back to ColMesh
			Mesh2Col CollisionTarget
		)
	)
),

/*------------------------------------------------------------------------------
Exports data from max to the game
*/------------------------------------------------------------------------------
fn LiveExport =
(
	
),

LiveMapEditingService,
/*------------------------------------------------------------------------------
Initialises the game camera ready for syncing
*/------------------------------------------------------------------------------
fn LiveSyncGameCameraIntialise =
(
	--Check we are connected
	if not LiveConnected() then return False
	
	RSPushPrompt "Setting up camera..."
	
	--Create the camera widgets
	RemoteConnection.WriteBoolWidget "Camera/Create camera widgets" true
	RemoteConnection.SendSyncCommand()
	
	--Override streaming focus
	RemoteConnection.WriteBoolWidget "Camera/Free camera/Override streaming focus" true
	RemoteConnection.SendSyncCommand()
	
	--Setup our LiveMapEditingService (for changing the camera)
	IPAddress = RemoteConnection.GetGameIP()
	this.LiveMapEditingService = DotNetObject "RSG.RESTServices.Game.Map.LiveMapEditingService" IPAddress
	
	--Overide FOV
	RemoteConnection.WriteBoolWidget "Camera/Frame propagator/Override FOV" True
	RemoteConnection.SendSyncCommand()
	
	--Sync up FOV
	viewport.SetFOV 69.5
	RemoteConnection.WriteFloatWidget "Camera/Frame propagator/Overridden FOV" 45
	RemoteConnection.SendSyncCommand()
	
	--Setup safe frame and aspect ratio (16:9)
	RenderWidth = 16
	RenderHeight = 9
	DisplaySafeFrames = True
	
	RSPopPrompt()
	
	return True
),

/*------------------------------------------------------------------------------
Syncs the game camera to the viewport camera
*/------------------------------------------------------------------------------
LiveSyncGameCameraTimeout = 1000,
LiveSyncPreviousViewTM = Matrix3 1,
fn LiveSyncGameCamera =
(	
	--Record time so we can check the update happened in a timely fashion
	StartStamp = TimeStamp()
	if Viewport.IsPerspView() then
	(	
		CurrentViewTM = GetViewTM()
		
		if not( RsCompareMatrix CurrentViewTM this.LiveSyncPreviousViewTM ) then
		(
			--Get view matrix (from Live Map Editing tool)
			inverseViewTM = Inverse( CurrentViewTM  )
			cameraPosition = inverseViewTM[4]
			cameraFrontVector = -inverseViewTM[3]
			cameraUpVector = inverseViewTM[2]
			freeCamAdjust = dotnetobject "RSG.RESTServices.Game.Map.FreeCamAdjust" \
			cameraPosition.x cameraPosition.y cameraPosition.z \
			cameraFrontVector.x cameraFrontVector.y cameraFrontVector.z \
			cameraUpVector.x cameraUpVector.y cameraUpVector.z		
			
			--Push updates through REST service 
			this.LiveMapEditingService.AddFreeCamAdjust freeCamAdjust
			this.LiveMapEditingService.SendUpdates()
		)
		
		this.LiveSyncPreviousViewTM = CurrentViewTM
	)
	EndStamp = TimeStamp()
	
	--Check if the update took long
	if( EndStamp - StartStamp > this.LiveSyncGameCameraTimeout ) then
	(
		--If it did well stop sending camera changes
		print "Camera sync took too long - turning off"
		this.Painter.CameraSyncState = off
	)
),

/*------------------------------------------------------------------------------
Starts a viewport callback which constantly updates the game camera to the viewport
*/------------------------------------------------------------------------------
fn LiveSyncCameraOn =
(
	print "Starting camera syncing"

	--Setup camera and push an intial update
	if( this.LiveSyncGameCameraIntialise() ) then
	(
		RegisterRedrawViewsCallback this.LiveSyncGameCamera
	)
	else ( this.Painter.CameraSyncState = off )
),

/*------------------------------------------------------------------------------
Stop the viewport callback which constantly updates the game camera to the viewport (see above)
*/------------------------------------------------------------------------------
fn LiveSyncCameraOff =
(
	print "Stopping camera syncing"

	UnRegisterRedrawViewsCallback this.LiveSyncGameCamera
),

/*------------------------------------------------------------------------------
Used during painting. Updates either the selected faces or all faces within range of the camera. 
*/------------------------------------------------------------------------------
fn LiveUpdateCollectData =
(
	--Check we are connected
	if not LiveConnected() then return False
	
	--Update our hash sets
	this.UpdateCollisionTargetSpatialHashSet()
	this.UpdateProceduralSourceSpatialHashSet()
	
	--Clear any old collected data
	this.Painter.NodeCollection.ClearCollisionTargetFaces()
	
	--Set up selection option and show polys
	RemoteConnection.WriteBoolWidget this.LiveEditAllowPickingWidgetName True
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteBoolWidget this.LiveEditShowPlantPolysWidgetName True
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteBoolWidget this.LiveEditAllCollisionSelectableWidgetName True
	RemoteConnection.SendSyncCommand()
	
	--Get the cameras current positions
	CameraPosX = RemoteConnection.ReadFloatWidget this.CameraMtxPositionXWidgetName
	CameraPosY = RemoteConnection.ReadFloatWidget this.CameraMtxPositionYWidgetName
	CameraPosZ = RemoteConnection.ReadFloatWidget this.CameraMtxPositionZWidgetName
	CameraPos = [ CameraPosX, CameraPosY, CameraPosZ ]
	RemoteConnection.SendSyncCommand()
	
	--Setup and trigger the search sphere ( makes a selection around the game camera- 50m )
	RemoteConnection.WriteFloatWidget this.LiveEditSphereRadiusWidgetName 50.00
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteVector3Widget LiveEditSphereCenterWidgetName CameraPos
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteBoolWidget LiveEditSearchWidgetName True
	RemoteConnection.SendSyncCommand()
	
	--Write out the selection
	RemoteConnection.WriteBoolWidget this.LiveEditWriteSelectionWidgetName True
	RemoteConnection.SendSyncCommand()
	
	--Sleep to make sure the file gets written out and we can continue
	Sleep 0.2
	
	--Read in the exported LocTri data and check we got valid data
	ExportedLocTriList = this.ReadExportedLocTriList()
	if( ExportedLocTriList == Undefined or ExportedLocTriList.Count == 0 ) then
	(
		MessageBox "Failed to update faces. Ensure that you have the game loaded and the camera is within range of the faces that you are trying to live update." Title:"Live Update Report"
		return Undefined
	)

	--for each of the LocTris
	for SubExportedLocTri in ExportedLocTriList do
	(
		--If the area of the triangle is really big then ignore it as there is a bug in RAG atm
		if( SubExportedLocTri.Area > 60 ) then ( continue )
		
		--Check this is a collision face from one of our collision targets. We use a very low threshold 
		--so the match should be practically exact. Threshold can't be 0 because of floating point inprecision
		--FaceCollisionTargetSpatialEntry = this.CollisionTargetSpatialHashSet.GetClosestEntry SubExportedLocTri.Center Threshold:0.005
		
		--If it's not a valid collision face just move onto the next LocTri
		--if( FaceCollisionTargetSpatialEntry == Undefined ) then ( continue )

		--Resolve procedural source face which this collision face would use during transfer
		--The code below closely matches the transfer logic
		FaceProceduralSourceSpatialEntry = this.ProceduralSourceSpatialHashSet.GetClosestEntry SubExportedLocTri.Center
		
		--If we got a procedural source face
		if( FaceProceduralSourceSpatialEntry != Undefined ) then
		(
			--Try and get a corrosponding node face data from within our working object.
			--Basically we are checking that the procedural source face is within the current set we are editing
			NodeFaceData = this.Painter.NodeCollection.GetNodeFaceData FaceProceduralSourceSpatialEntry.Value.Node.Handle FaceProceduralSourceSpatialEntry.Value.FaceIndex
			
			--If we have got this far then we have a face with the working object
			if( NodeFaceData != Undefined ) then
			(
				--Int32 InputNodeHandle, Int32 InputFaceIndex, Single InputFaceCenterX, Single InputFaceCenterY, Single InputFaceCenterZ
				NodeFaceData.AddCollisionTargetFace 0 0 SubExportedLocTri.Center.X SubExportedLocTri.Center.Y SubExportedLocTri.Center.Z
			)
		)
	) 
	
	--Clear faces and hide polys
	RemoteConnection.WriteBoolWidget "_LiveEdit_/PlantsMgr Data Edit/Search Options/Clear Selected Entries" True
	RemoteConnection.WriteBoolWidget "_LiveEdit_/PlantsMgr Data Edit/Search Options/Show Plant Polys" False

),

/*------------------------------------------------------------------------------
Updates the selected faces in game. Previous version
*/------------------------------------------------------------------------------
fn LiveUpdateFaces =
(
	--Check we are connected
	if not LiveConnected() then return False
	this.LiveUpdateCollectData()
	
	--Set the search sphere
	RemoteConnection.WriteFloatWidget this.LiveEditSphereRadiusWidgetName 0.05
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteBoolWidget this.LiveEditAllCollisionSelectableWidgetName True
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteBoolWidget this.LiveEditShowPlantPolysWidgetName True
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteBoolWidget this.LiveEditAllowPickingWidgetName True
	RemoteConnection.SendSyncCommand()
			
	--Get current face list as selection
	FaceList = ( RSPoly_GetFaceList_Selection this.WorkingObject as Array )
	
	--Get collision batch list from face selection
	LiveUpdateFaceBatchList = this.Painter.NodeCollection.GetLiveUpdateFaceBatches FaceList
	
	--Create our top progress window
	LiveUpdateFaceBatchProgress = RSProgressWindow Title:"Sending live updates to faces..." StartStep:0 EndStep:LiveUpdateFaceBatchList.Count
	LiveUpdateFaceBatchProgress.Start()
	
	--Loop through each batch
	for LiveUpdateFaceBatch in LiveUpdateFaceBatchList do
	(
		--Push progress update
		LiveUpdateFaceBatchProgress.PostProgressStep()
				
		--Create our batch position list
		BatchPositionList = #()
		for NodeFaceData in LiveUpdateFaceBatch.NodeFaceDataArray do
		(
			for CollisionTargetFace in NodeFaceData.CollisionTargetFaceArray do
			(
				CollisionTargetFacePosition = [ CollisionTargetFace.FaceCenterX, CollisionTargetFace.FaceCenterY, CollisionTargetFace.FaceCenterZ ]
				Append BatchPositionList CollisionTargetFacePosition
			)
		)
		
		--First clear the current faces
		RemoteConnection.WriteBoolWidget this.LiveEditClearFaces True
		RemoteConnection.SendSyncCommand()
		
		gRsCollPaintToolLive.CreateBatchedPositionFile BatchPositionList
		
		RemoteConnection.WriteBoolWidget this.LiveEditReadInputFileWidgetName True
		RemoteConnection.SendSyncCommand()
		while( RemoteConnection.ReadBoolWidget this.LiveEditReadingInputFile ) do ( Sleep 0.05 )
		
		--Set procedural specifics
		if( RemoteConnection.IsConnected() ) then
		(
			--Get target procedural level
			TargetProceduralLevel = LiveUpdateFaceBatch.TargetProceduralLevel
			
			--Get procedural index
			ProceduralIndex = ProceduralPainter.Painter.Metadata.GetProceduralIndex TargetProceduralLevel.TargetTag
			
			--If we have a valid procedural index and that the procedural index is plant based then send it over
			if( ProceduralIndex != -1 and ProceduralPainter.Painter.Metadata.HasPlantData ProceduralIndex ) then
			(
				RemoteConnection.WriteIntWidget "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Type" ProceduralIndex
				RemoteConnection.SendSyncCommand()
				
				RemoteConnection.WriteIntWidget this.LiveEditScaleZ0WidgetName TargetProceduralLevel.ScaleZLevel
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteIntWidget this.LiveEditScaleZ1WidgetName TargetProceduralLevel.ScaleZLevel
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteIntWidget this.LiveEditScaleZ2WidgetName TargetProceduralLevel.ScaleZLevel
				RemoteConnection.SendSyncCommand()
				
				RemoteConnection.WriteIntWidget this.LiveEditScaleXYZ0WidgetName TargetProceduralLevel.ScaleXYZLevel
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteIntWidget this.LiveEditScaleXYZ1WidgetName TargetProceduralLevel.ScaleXYZLevel
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteIntWidget this.LiveEditScaleXYZ2WidgetName TargetProceduralLevel.ScaleXYZLevel
				RemoteConnection.SendSyncCommand()
				
				RemoteConnection.WriteIntWidget this.LiveEditDensity0WidgetName TargetProceduralLevel.DensityLevel
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteIntWidget this.LiveEditDensity1WidgetName TargetProceduralLevel.DensityLevel
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteIntWidget this.LiveEditDensity2WidgetName TargetProceduralLevel.DensityLevel
				RemoteConnection.SendSyncCommand()
				
				RemoteConnection.WriteVector4Widget this.LiveEditGroundTint0WidgetName White
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteVector4Widget this.LiveEditGroundTint1WidgetName White
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteVector4Widget this.LiveEditGroundTint2WidgetName White
				RemoteConnection.SendSyncCommand()
				
				--Commit changes to the batch
				RemoteConnection.WriteBoolWidget this.LiveEditCommitChangesWidgetName True
				RemoteConnection.SendSyncCommand()
				while( RemoteConnection.ReadBoolWidget this.LiveEditSavingChanges ) do ( Sleep 0.05 )
			)
		)
	)

	--End progress
	LiveUpdateFaceBatchProgress.End()

	RemoteConnection.WriteBoolWidget this.LiveEditClearFaces  True
	RemoteConnection.SendSyncCommand()
	
	RemoteConnection.WriteBoolWidget this.LiveEditShowPlantPolysWidgetName False
	RemoteConnection.SendSyncCommand()

),


fn LiveUpdateFaces_Old =
(
	--Check we are connected
	if not LiveConnected() then return False
	
	--Set the search sphere
	RemoteConnection.WriteFloatWidget this.LiveEditSphereRadiusWidgetName 0.75
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteBoolWidget this.LiveEditAllCollisionSelectableWidgetName True
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteBoolWidget this.LiveEditShowPlantPolysWidgetName True
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteBoolWidget this.LiveEditAllowPickingWidgetName True
	RemoteConnection.SendSyncCommand()
			
	--Get current face list as selection
	FaceList = ( RSPoly_GetFaceList_Selection this.WorkingObject as Array )
	
	--Get collision batch list from face selection
	LiveUpdateFaceBatchList = this.Painter.NodeCollection.GetLiveUpdateFaceBatches FaceList
	
	--Create our top progress window
	LiveUpdateFaceBatchProgress = RSProgressWindow Title:"Sending live updates to faces..." StartStep:0 EndStep:LiveUpdateFaceBatchList.Count
	LiveUpdateFaceBatchProgress.Start()
	
	--Loop through each batch
	for LiveUpdateFaceBatch in LiveUpdateFaceBatchList do
	(
		--Push progress update
		LiveUpdateFaceBatchProgress.PostProgressStep()
		
		--Create our batch position list
		BatchPositionList = #()
		for FaceIndex in LiveUpdateFaceBatch.FaceIndexArray do
		(
			CollisionFacePosition = PolyOp_GetFaceCenter this.WorkingObject FaceIndex
			Append BatchPositionList CollisionFacePosition
		)

		--First clear the current faces
		RemoteConnection.WriteBoolWidget this.LiveEditClearFaces True
		RemoteConnection.SendSyncCommand()
		
		gRsCollPaintToolLive.CreateBatchedPositionFile BatchPositionList
		
		RemoteConnection.WriteBoolWidget this.LiveEditReadInputFileWidgetName True
		RemoteConnection.SendSyncCommand()
		
		--Set procedural specifics
		if( RemoteConnection.IsConnected() ) then
		(
			--Get target procedural level
			TargetProceduralLevel = LiveUpdateFaceBatch.TargetProceduralLevel
			
			--Get procedural index
			ProceduralIndex = ProceduralPainter.Painter.Metadata.GetProceduralIndex TargetProceduralLevel.TargetTag
			
			--If we have a valid procedural index and that the procedural index is plant based then send it over
			if( ProceduralIndex != -1 and ProceduralPainter.Painter.Metadata.HasPlantData ProceduralIndex ) then
			(
				RemoteConnection.WriteIntWidget "_LiveEdit_/PlantsMgr Data Edit/Edit Options/Type" ProceduralIndex
				RemoteConnection.SendSyncCommand()
				
				RemoteConnection.WriteIntWidget this.LiveEditScaleZ0WidgetName TargetProceduralLevel.ScaleZLevel
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteIntWidget this.LiveEditScaleZ1WidgetName TargetProceduralLevel.ScaleZLevel
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteIntWidget this.LiveEditScaleZ2WidgetName TargetProceduralLevel.ScaleZLevel
				RemoteConnection.SendSyncCommand()
				
				RemoteConnection.WriteIntWidget this.LiveEditScaleXYZ0WidgetName TargetProceduralLevel.ScaleXYZLevel
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteIntWidget this.LiveEditScaleXYZ1WidgetName TargetProceduralLevel.ScaleXYZLevel
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteIntWidget this.LiveEditScaleXYZ2WidgetName TargetProceduralLevel.ScaleXYZLevel
				RemoteConnection.SendSyncCommand()
				
				RemoteConnection.WriteIntWidget this.LiveEditDensity0WidgetName TargetProceduralLevel.DensityLevel
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteIntWidget this.LiveEditDensity1WidgetName TargetProceduralLevel.DensityLevel
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteIntWidget this.LiveEditDensity2WidgetName TargetProceduralLevel.DensityLevel
				RemoteConnection.SendSyncCommand()
				
				RemoteConnection.WriteVector4Widget this.LiveEditGroundTint0WidgetName White
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteVector4Widget this.LiveEditGroundTint1WidgetName White
				RemoteConnection.SendSyncCommand()
				RemoteConnection.WriteVector4Widget this.LiveEditGroundTint2WidgetName White
				RemoteConnection.SendSyncCommand()
				
				--Commit changes to the batch
				RemoteConnection.WriteBoolWidget this.LiveEditCommitChangesWidgetName True
				RemoteConnection.SendSyncCommand()
			)
		)
	)

	--End progress
	LiveUpdateFaceBatchProgress.End()

	RemoteConnection.WriteBoolWidget this.LiveEditClearFaces  True
	RemoteConnection.SendSyncCommand()
	
	RemoteConnection.WriteBoolWidget this.LiveEditShowPlantPolysWidgetName False
	RemoteConnection.SendSyncCommand()

),

/*------------------------------------------------------------------------------
Updates a single face with all the information contained within the passed procedural level.
Sources the ground tint color from the master ground tint image.
*/------------------------------------------------------------------------------
fn LiveUpdateSingleFace_New InputFaceCenter InputProceduralLevel =
(
	gRsCollPaintToolLive.CreateBatchedPositionFile #(InputFaceCenter)
	
	ProceduralIndex = ProceduralPainter.Painter.Metadata.GetProceduralIndex InputProceduralLevel.TargetTag

	RemoteConnection.WriteBoolWidget this.LiveEditClearFaces True
	RemoteConnection.SendSyncCommand()
	
	RemoteConnection.WriteBoolWidget this.LiveEditReadInputFileWidgetName True
	RemoteConnection.SendSyncCommand()
	
	RemoteConnection.WriteIntWidget this.LiveEditProcTypeWidgetName ProceduralIndex
	RemoteConnection.SendSyncCommand()
	
	RemoteConnection.WriteIntWidget this.LiveEditScaleZ0WidgetName InputProceduralLevel.ScaleZLevel
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteIntWidget this.LiveEditScaleZ1WidgetName InputProceduralLevel.ScaleZLevel
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteIntWidget this.LiveEditScaleZ2WidgetName InputProceduralLevel.ScaleZLevel
	RemoteConnection.SendSyncCommand()
	
	RemoteConnection.WriteIntWidget this.LiveEditScaleXYZ0WidgetName InputProceduralLevel.ScaleXYZLevel
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteIntWidget this.LiveEditScaleXYZ1WidgetName InputProceduralLevel.ScaleXYZLevel
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteIntWidget this.LiveEditScaleXYZ2WidgetName InputProceduralLevel.ScaleXYZLevel
	RemoteConnection.SendSyncCommand()
	RemoteConnection.SendSyncCommand()
	
	RemoteConnection.WriteIntWidget this.LiveEditDensity0WidgetName InputProceduralLevel.DensityLevel
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteIntWidget this.LiveEditDensity1WidgetName InputProceduralLevel.DensityLevel
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteIntWidget this.LiveEditDensity2WidgetName InputProceduralLevel.DensityLevel
	RemoteConnection.SendSyncCommand()
	RemoteConnection.SendSyncCommand()
	
	GroundTintColor = this.GroundTintMapImage.GetPixel InputFaceCenter
	RemoteConnection.WriteVector4Widget this.LiveEditGroundTint0WidgetName GroundTintColor
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteVector4Widget this.LiveEditGroundTint1WidgetName GroundTintColor
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteVector4Widget this.LiveEditGroundTint2WidgetName GroundTintColor
	RemoteConnection.SendSyncCommand()
	
	RemoteConnection.WriteBoolWidget this.LiveEditCommitChangesWidgetName True
	RemoteConnection.SendSyncCommand()	
),
			
/*------------------------------------------------------------------------------
Used during painting. Updates either the selected faces or all faces within range of the camera. 
*/------------------------------------------------------------------------------
fn LiveUpdateFaces_New RestrictToSelection:True =
(
	--Check we are connected
	if not LiveConnected() then return False
	
	--Set up selection option and show polys
	RemoteConnection.WriteBoolWidget this.LiveEditAllowPickingWidgetName True
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteBoolWidget this.LiveEditShowPlantPolysWidgetName True
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteBoolWidget this.LiveEditAllCollisionSelectableWidgetName True
	RemoteConnection.SendSyncCommand()
	
	--Get the cameras current positions
	CameraPosX = RemoteConnection.ReadFloatWidget this.CameraMtxPositionXWidgetName
	CameraPosY = RemoteConnection.ReadFloatWidget this.CameraMtxPositionYWidgetName
	CameraPosZ = RemoteConnection.ReadFloatWidget this.CameraMtxPositionZWidgetName
	CameraPos = [ CameraPosX, CameraPosY, CameraPosZ ]
	RemoteConnection.SendSyncCommand()
	
	--Setup and trigger the search sphere ( makes a selection around the game camera- 50m )
	RemoteConnection.WriteFloatWidget this.LiveEditSphereRadiusWidgetName 50.00
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteVector3Widget LiveEditSphereCenterWidgetName CameraPos
	RemoteConnection.SendSyncCommand()
	RemoteConnection.WriteBoolWidget LiveEditSearchWidgetName True
	RemoteConnection.SendSyncCommand()
	
	--Write out the selection
	RemoteConnection.WriteBoolWidget this.LiveEditWriteSelectionWidgetName True
	RemoteConnection.SendSyncCommand()
	
	--Sleep to make sure the file gets written out and we can continue
	Sleep 0.2
	
	--Read in the exported LocTri data and check we got valid data
	ExportedLocTriList = this.ReadExportedLocTriList()
	if( ExportedLocTriList == Undefined or ExportedLocTriList.Count == 0 ) then
	(
		MessageBox "Failed to update faces. Ensure that you have the game loaded and the camera is within range of the faces that you are trying to live update." Title:"Live Update Report"
		return Undefined
	)
	
	--Get working object selected faces
	WorkingObjectSelectedFaceList = ( RSPoly_GetFaceList_Selection this.WorkingObject as Array )

	--Set the search sphere much lower so that we are matching faces more precisely
	RemoteConnection.WriteFloatWidget this.LiveEditSphereRadiusWidgetName 0.05
	
	--Load ground tint image
	this.GroundTintMapImage.Load()
	
	--for each of the LocTris
	for SubExportedLocTri in ExportedLocTriList do
	(
		--Check this is a collision face from one of our collision targets. We use a very low threshold 
		--so the match should be practically exact. Threshold can't be 0 because of floating point inprecision
		FaceCollisionTargetSpatialEntry = this.CollisionTargetSpatialHashSet.GetClosestEntry SubExportedLocTri.Center Threshold:0.005
		
		--If it's not a valid collision face just move onto the next LocTri
		if( FaceCollisionTargetSpatialEntry == Undefined ) then ( continue() )

		--Resolve procedural source face which this collision face would use during transfer
		--The code below closely matches the transfer logic
		FaceProceduralSourceSpatialEntry = this.ProceduralSourceSpatialHashSet.GetClosestEntry SubExportedLocTri.Center
		
		--If we got a procedural source face
		if( FaceProceduralSourceSpatialEntry != Undefined ) then
		(
			--Try and get a corrosponding node face data from within our working object.
			--Basically we are checking that the procedural source face is within the current set we are editing
			NodeFaceData
			
			--If we are restricting the search to the current selection on the working object then use the appropriate overlaod
			if( RestrictToSelection ) then
			NodeFaceData = this.Painter.NodeCollection.GetNodeFaceData FaceProceduralSourceSpatialEntry.Value.Node.Handle FaceProceduralSourceSpatialEntry.Value.FaceIndex WorkingObjectSelectedFaceList
			else
			NodeFaceData = this.Painter.NodeCollection.GetNodeFaceData FaceProceduralSourceSpatialEntry.Value.Node.Handle FaceProceduralSourceSpatialEntry.Value.FaceIndex
			
			--If we have got this far then we have a face with the working object
			if( NodeFaceData != Undefined ) then
			(
				this.LiveUpdateSingleFace SubExportedLocTri.Center NodeFaceData.ProceduralLevel
			)
		)
	) 
	
	--Clear faces and hide polys
	RemoteConnection.WriteBoolWidget "_LiveEdit_/PlantsMgr Data Edit/Search Options/Clear Selected Entries" True
	RemoteConnection.WriteBoolWidget "_LiveEdit_/PlantsMgr Data Edit/Search Options/Show Plant Polys" False

),

/*------------------------------------------------------------------------------
Prepares for data needed to live edit. Should only be called if we have a working object.
*/------------------------------------------------------------------------------
CachedLiveEditData = False,
fn CacheLiveEditData =
(
	--Update our spatial hash set - allows on the fly live editing
	this.UpdateProceduralSourceSpatialHashSet()
	this.ProceduralSourceSpatialHashSet.SetCellSearchBreadth 1

	--Get list of procedural sources
	CollisionTargetList = this.GetCollisionTargetList()
	
	--Create our top progress window
	CollisionTargetProgress = RSProgressWindow Title:"Gathering live edit data..." StartStep:0 EndStep:CollisionTargetList.Count
	CollisionTargetProgress.Start()
			
	for CollisionTarget in CollisionTargetList do
	(
		--Push progress update
		CollisionTargetProgress.PostProgressStep()
		
		--Convert the collision mesh to editable mesh
		Col2Mesh CollisionTarget
		FaceCount = GetNumFaces CollisionTarget
		
		--Start a sub progress
		FaceIndexProgress = CollisionTargetProgress.StartSubProgress 0 FaceCount SubTitle:CollisionTarget.Name
		FaceIndexProgress.Start()
		
		--For each face of the ProceduralSource
		for FaceIndex = 1 to FaceCount do
		(
			--Push progress update
			FaceIndexProgress.PostProgressStep()
			
			--Get the face centre
			FaceCenter = MeshOp_GetFaceCenter CollisionTarget FaceIndex
			
			--Resolve procedural source face which this collision face would use during transfer
			--The code below closely matches the transfer logic
			FaceProceduralSourceSpatialEntry = this.ProceduralSourceSpatialHashSet.GetClosestEntry FaceCenter
			if( FaceProceduralSourceSpatialEntry != Undefined ) then
			(
								
				--Try and get a corrosponding node face data from within our working object.
				--Basically we are checking that the procedural source face is within the current set we are editing
				NodeFaceData = this.Painter.NodeCollection.GetNodeFaceData FaceProceduralSourceSpatialEntry.Value.Node.Handle FaceProceduralSourceSpatialEntry.Value.FaceIndex
				
				--If we have got this far then we have a matching face within the working object
				if( NodeFaceData != Undefined ) then
				(
					--Int32 InputNodeHandle, Int32 InputFaceIndex, Single InputFaceCenterX, Single InputFaceCenterY, Single InputFaceCenterZ
					NodeFaceData.AddCollisionTargetFace CollisionTarget.Handle FaceIndex FaceCenter.X FaceCenter.Y FaceCenter.Z
				)
			)
		)
		
		--End progress
		FaceIndexProgress.End()

		--Back to collision
		Mesh2Col CollisionTarget
	)
	
	CollisionTargetProgress.End()
),