--
-- File:: rockstar/helpers/secondsurface_painter.ms
-- Description:: Rockstar Second Surface Painter
--							Second surface painting utility
--
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 26/1/2009
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Global
-----------------------------------------------------------------------------
	global CollSurfaceObj 
	
-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

	fn ConvertAndApplyModifier paintType = 
	(
		if (undefined==CollSurfaceObj or (classof CollSurfaceObj)!=Editable_mesh) then
		(
			messagebox "Object not converted to Mesh yet. Initialize in beforehand."
			return false
		)
		max modify mode
		col2mesh CollSurfaceObj
		CollSurfaceObj.vertexColorType = #map_channel
		
		p = VertexPaint()

		if paintType==#SecondSurface then
		(
			CollSurfaceObj.vertexColorMapChannel = 10
			p.mapchannel = 10
			addmodifier CollSurfaceObj p
			vPainttool = VertexPaintTool()
			vPainttool.mapDisplayChannel = 10
			vPainttool.curPaintMode = 1
		)
		else if paintType==#GroundPaint then
		(
			CollSurfaceObj.vertexColorMapChannel = 11
			p.mapchannel = 11
			addmodifier CollSurfaceObj p
			vPainttool = VertexPaintTool()
			vPainttool.mapDisplayChannel = 11
			vPainttool.curPaintMode = 2
		)
		else
		(
			messagebox "unknown paint type"
			return false
		)

		vPainttool.paintColor = color 0.0 0.0 0.0 
				
		aaa = vPainttool.keepToolboxOpen	
		vPainttool.keepToolboxOpen = aaa
	)
