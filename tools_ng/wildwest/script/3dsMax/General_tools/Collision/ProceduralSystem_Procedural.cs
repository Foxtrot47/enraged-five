using System;
using System.Xml.Serialization;
using System.Xml;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace ProceduralSystem
{
    public partial class Procedural : INotifyPropertyChanged
    {

        #region STATIC
        ///-----------------------------------------------------------------------------------------------

        //Contains a list of valid hues for displaying in the viewport.
        static List<Color> _ValidColors = new List<Color>();

        //Maximum number of levels
        static Int32 MaximumLevels = 16;

        //Static constructor that populates our valid colors
        static Procedural()
        {
            for (Double iHue = 0; iHue < 360; iHue += 10)
            {
                _ValidColors.Add(new Color(iHue, 0.65, 1.0));
            }
        }

        //The Hue handle of the procedural.
        public static readonly Int32 UnsetColorHue = -1;

        //These values indicate the range for the value part of the HSV level display colors.
        //We don't go all the way 0.0 because this is black and this is the color for nothing. 
        static Double ColorValueMin = 0.45;
        static Double ColorValueMax = 1.0;

        //The saturation 
        static Double ColorSaturation = 1.0;

        ///-----------------------------------------------------------------------------------------------
        #endregion

        #region FIELDS AND PROPERTIES
        ///-----------------------------------------------------------------------------------------------

        //Valid display colors
        [XmlIgnore]
        public List<Color> ValidColors
        {
            get { return _ValidColors; }
        }

        //Indicates whether we are within the level limit. 
        //We bind to this to stop additional values getting added
        [XmlIgnore]
        public Boolean WithinMaximumLevelLimit
        {
            get
            {
                if (LevelList.Count == MaximumLevels)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        //Display name of the procedural
        [XmlAttribute]
        public String Name { get; set; }

        //The procedural meta tag
        String _TargetTag = "";
        [XmlAttribute]
        public String TargetTag
        {
            get
            {
                return _TargetTag;
            }
            set
            {
                foreach (var SubLevel in this.LevelList)
                {
                    if (String.Compare(SubLevel.TargetTag, _TargetTag, true) == 0)
                    {
                        SubLevel.TargetTag = value;
                    }
                }
                _TargetTag = value;
            }
        }

        //List of levels
        [XmlElement("Level")]
        public ObservableCollection<Level> LevelList { get; set; }

        //Array of levels - exposes level list to MXS
        [XmlIgnore]
        public Level[] LevelArray
        {
            get
            {
                return LevelList.ToArray();
            }
        }

        //Parent set data 
        [XmlIgnore]
        public SetData Parent { get; set; }

        //The color hue value of the procedural. This is very important as it 
        //acts as our unique handle to the procedural. As in there can only be one procedural per SetData
        //with a given hue value
        Int32 _ColorHue = UnsetColorHue;
        [XmlAttribute]
        public Int32 ColorHue
        {
            get { return _ColorHue; }
            set
            {
                //First we'll make sure the value being passed is within the 0-360 range
                Int32 SanitisedValue = value;
                if (SanitisedValue > 360)
                {
                    SanitisedValue = 360;
                }
                if (SanitisedValue < 0)
                {
                    SanitisedValue = 0;
                }

                //If the parent isn't null then we verify the value with it.
                //Otherwise we go ahead and assign.
                if (Parent != null)
                {
                    _ColorHue = Parent.GetClosestColorHueAvailable(SanitisedValue);
                }
                else
                {
                    _ColorHue = SanitisedValue;
                }

                RecalculateColorValues();

                OnPropertyChanged("ColorHue");
                OnPropertyChanged("ColorHexCode");
            }
        }

        //The color hex code of the procedural
        [XmlIgnore]
        public String ColorHexCode
        {
            get
            {
                Color TempColor = new Color();
                TempColor.FromHSV(ColorHue, 1.0, 1.0);
                return (TempColor.HexCode);
            }
        }

        //The display opacity 
        Byte _DisplayOpacity;
        [XmlIgnore]
        public Byte DisplayOpacity
        {
            get { return _DisplayOpacity; }
            set
            {
                _DisplayOpacity = value;
                OnPropertyChanged("DisplayOpacity");
            }
        }

        //Thumbnail path
        String _ThumbnailImageFilePath = "";
        [XmlAttribute("Thumbnail")]
        public String ThumbnailImageFilePath
        {
            get { return _ThumbnailImageFilePath; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    _ThumbnailImageFilePath = @"X:\gta5\tools\wildwest\script\3dsMax\UI\ThumbnailDefault.jpg";
                }
                else
                {
                    _ThumbnailImageFilePath = value;
                }

                OnPropertyChanged("ThumbnailImageFilePath");
            }
        }

        //Desired spacing between each instance of the procedural
        [XmlElement]
        public Single Spacing { get; set; }

        //Description of the procedural
        [XmlElement]
        public String Description { get; set; }

        //Indicates whether the procedural should set 'ProcCullImmune' when transfering
        [XmlElement]
        public Boolean CullImmune { get; set; }

        //Returns the base level of the procedural (last level)
        public Procedural.Level BaseLevel
        {
            get { return LevelList.LastOrDefault(); }
        }

        //Returns the top level of the procedural (first level)
        public Procedural.Level TopLevel
        {
            get { return LevelList.FirstOrDefault(); }
        }

        //Returns the next level in the chain after the one passed
        public Procedural.Level GetNextLevel(Procedural.Level InputLevel)
        {
            var InputLevelIndex = this.LevelList.IndexOf(InputLevel);

            if (InputLevelIndex > 0)
            {
                var NextLevelIndex = InputLevelIndex - 1;
                return this.LevelList[NextLevelIndex];
            }

            return null;
        }

        //Returns the previous level in the chain after the one passed
        public Procedural.Level GetPreviousLevel(Procedural.Level InputLevel)
        {
            var InputLevelIndex = this.LevelList.IndexOf(InputLevel);

            if (InputLevelIndex >= 0)
            {
                var PreviousLevelIndex = InputLevelIndex + 1;
                if (this.LevelList.Count - 1 < PreviousLevelIndex)
                {
                    return this.LevelList[PreviousLevelIndex];
                }
            }

            return null;
        }

        ///-----------------------------------------------------------------------------------------------
        #endregion

        #region CONSTRUCTORS
        ///-----------------------------------------------------------------------------------------------

        //Default constructor
        public Procedural()
        {
            Name = String.Empty;
            LevelList = new ObservableCollection<Level>();
            TargetTag = "PROC_SCALE_TEST";
            DisplayOpacity = 80;

            //Register with the collection changed event
            LevelList.CollectionChanged += LevelList_CollectionChanged;
        }

        ///-----------------------------------------------------------------------------------------------
        #endregion

        #region METHODS
        ///-----------------------------------------------------------------------------------------------

        //Monitors changes to the procedural level list. Any new levels added get their parent setup to point to this procedural.
        //Also we recalculate level colors passing in our display hues.
        void LevelList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Procedural.Level AddedProceduralLevel in e.NewItems)
                {
                    AddedProceduralLevel.ParentProcedural = this;

                    if (String.IsNullOrEmpty(AddedProceduralLevel.TargetTag))
                    {
                        AddedProceduralLevel.TargetTag = this.TargetTag;
                    }

                    AddedProceduralLevel.Name = this.ProcessLevelName(AddedProceduralLevel);
                }
            }

            //Now a level has been added (or removed) we should rebalance the color display levels to reflect the new number of levels.
            RecalculateColorValues();

            //Post a property changed event as we have reached our maximum number of levels
            OnPropertyChanged("WithinMaximumLevelLimit");
        }

        //Recalculates all the value (V) components of the procedural level colors. 
        public void RecalculateColorValues()
        {
            //Flesh out each of the colour levels
            Double CurrentColorValue = ColorValueMax;
            Double ColorValueStep = (ColorValueMax - ColorValueMin) / LevelList.Count;
            foreach (var SubLevel in LevelList)
            {
                SubLevel.Color.FromHSV(ColorHue, ColorSaturation, CurrentColorValue);
                CurrentColorValue -= ColorValueStep;
            }
        }

        //Gets the closest level based on the passed value
        public Procedural.Level GetClosestLevelByColorValue(Double InputColorValue)
        {
            Procedural.Level ClosestLevel = LevelList.FirstOrDefault();
            Double ClosestDistance = 1.0;

            //For each level test to see how close we are 
            foreach (var CurrentLevel in LevelList)
            {
                Double CurrentDistance = Math.Abs(CurrentLevel.Color.V - InputColorValue);
                if (CurrentDistance < ClosestDistance)
                {
                    ClosestDistance = CurrentDistance;
                    ClosestLevel = CurrentLevel;
                }
            }

            return ClosestLevel;
        }

        //Checks the passed level name. If it's unique it returns unedited else it adds a _xx to make it unique
        public String ProcessLevelName(Procedural.Level InputProceduralLevel)
        {
            var ReturnLevelName = InputProceduralLevel.Name;
            var ProceduralLevelMatch = (from ProcLevel in this.LevelList
                                        where ProcLevel != InputProceduralLevel &&
                                        ProcLevel.Name == ReturnLevelName
                                        select ProcLevel).FirstOrDefault();

            var CurrentIteration = 0;
            while (ProceduralLevelMatch != null)
            {
                ReturnLevelName = String.Format("{0}_{1}", InputProceduralLevel.Name, CurrentIteration);
                ProceduralLevelMatch = (from ProcLevel in this.LevelList 
                                        where ProcLevel != InputProceduralLevel && 
                                        ProcLevel.Name == ReturnLevelName 
                                        select ProcLevel).FirstOrDefault();
                CurrentIteration++;
            }

            return ReturnLevelName;
        }

        ///-----------------------------------------------------------------------------------------------
        #endregion

        #region EVENTS
        ///-----------------------------------------------------------------------------------------------

        // Declare the PropertyChanged event
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        // NotifyPropertyChanged will raise the PropertyChanged event passing the
        // source property that is being updated.
        void OnPropertyChanged(String PropertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }

        ///-----------------------------------------------------------------------------------------------
        #endregion

    }
}