struct MapImage
(
	--Percentage that the world origin sits in map images (allows us to use smaller images without everything becoming broken)
	ImageOriginPercentX = 0.4426,
	ImageOriginPercentY = 0.6747,

	--Scale of the image when 1 pixel = 1 metre
	ImageScaleDatumX = 9150.0,
	ImageScaleDatumY = 13050.0,
	
	--MapTintImage data
	ImageFilePath,
	ImageBitmap,
	ImageOriginX,
	ImageOriginY,
	ImageScalarX,
	ImageScalarY,
	
	/*------------------------------------------------------------------------------
	Gets the passed point's closest pixel on the image
	*/------------------------------------------------------------------------------
	fn GetPixel InputPoint =
	(
		TargetPixel = this.TranslateToImageSpace InputPoint

		return ( GetPixels this.ImageBitmap TargetPixel 1 )[1]
	),

	fn SetAreaPixels InputPointList InputColor =
	(
		TranslatedPointList = for InputPoint in InputPointList collect this.TranslateToImageSpace( InputPoint )
		
		RSImage_SetColor this.ImageBitmap TranslatedPointList InputColor
	),
	
	fn TranslateToImageSpace InputPoint =
	(
		return [ InputPoint.X * this.ImageScalarX + this.ImageOriginX, ( -(InputPoint.Y * this.ImageScalarY) + this.ImageOriginY ) ]
	),
	
	/*------------------------------------------------------------------------------
	Loads an image ready to read some pixels
	*/------------------------------------------------------------------------------
	fn Load =
	(
		--Check if the image is already loaded
		if( this.ImageBitmap != Undefined ) then ( return True )
		
		--Load the image
		RSPushPrompt "Loading image..."
		this.ImageBitmap = OpenBitMap this.ImageFilePath
		RSPopPrompt()
		
		--Check we have loaded the image successfully		
		if( this.ImageBitmap == undefined ) then
		(
			return False
		)
		else
		(
			this.ImageScalarX = this.ImageBitmap.Width / this.ImageScaleDatumX
			this.ImageScalarY = this.ImageBitmap.Height / this.ImageScaleDatumY
			
			this.ImageOriginX = RoundToIncrement( this.ImageBitmap.Width * this.ImageOriginPercentX )
			this.ImageOriginY = RoundToIncrement( this.ImageBitmap.Height * this.ImageOriginPercentY )
			
			return True
		)
	),
	
	/*------------------------------------------------------------------------------
	Saves changes the map image. Should be called after load.
	*/------------------------------------------------------------------------------
	fn Save =
	(
		RSPushPrompt "Saving image..."
		--We need to make a copy so we can save
		this.ImageBitmap = Copy this.ImageBitmap
		this.ImageBitmap.Filename = this.ImageFilePath
		::Save this.ImageBitmap
		RSPopPrompt()
	),

	/*------------------------------------------------------------------------------
	Disposes of the image resources
	*/------------------------------------------------------------------------------
	fn Dispose =
	(
		if( this.ImageBitmap != Undefined ) then
		(
			Close this.ImageBitmap
			Free this.ImageBitmap
			this.ImageBitmap = Undefined
		)
	),
	
	/*---------------------------------------------------------------------------
		Initialise struct values
	*/---------------------------------------------------------------------------
	on create do
	(
		local projectName = gRsConfig.Project.Name
		
		case projectName of
		(
			"gta5":
			(
				this.ImageOriginPercentX = 0.4426
				this.ImageOriginPercentY = 0.6747

				--Scale of the image when 1 pixel = 1 metre
				this.ImageScaleDatumX = 9150.0
				this.ImageScaleDatumY = 12450.0
			)
			
			"gta5_liberty":
			(
				this.ImageOriginPercentX = 0.25
				this.ImageOriginPercentY = 0.25

				--Scale of the image when 1 pixel = 1 metre
				this.ImageScaleDatumX = 16384.0
				this.ImageScaleDatumY = 16384.0
			)
		)
	)
)