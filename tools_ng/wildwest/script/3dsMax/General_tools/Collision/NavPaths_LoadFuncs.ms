--  
-- Navigation-Node/Link load-methods
-- 	Neal D Corbett (R* Leeds) 16/2014
-- 
--	Functions to load AI-nodes data from paths.xml, and find object-faces within range 
--	of their links, that should be tagged with Road collision-flags
-- 

struct RsPathLoad
(
	-- Sizes of road-lanes:
	LaneWidth = 5.4, LaneWidthNarrow = 4.0,
	
	-- Verts will be matched to paths if their distance to path's plane is less than this:
	VerticalMatchDist = 2.0,
	
	-- Default node/link-values:
	DefSpeed = GetAttrDefault "VehicleNode" (GetAttrIndex "VehicleNode" "Speed"),
	DefLanesIn = GetAttrDefault "VehicleLink" (GetAttrIndex "VehicleLink" "Lanes In"),
	DefLanesOut = GetAttrDefault "VehicleLink" (GetAttrIndex "VehicleLink" "Lanes Out"),
	DefWidth = GetAttrDefault "VehicleLink" (GetAttrIndex "VehicleLink" "Width"),
	DefNarrowroad = GetAttrDefault "VehicleLink" (GetAttrIndex "VehicleLink" "Narrowroad"),
	
	-- Returns path to path-nodes file:
	fn GetPathsFilename = 
	(
		(RsConfigGetCommonDir() + "/data/levels/" + RsConfigGetProjectName()  + "/paths.xml")
	),
	
	-- Sync to latest paths-file:
	fn SyncPathsFile = 
	(
		gRsPerforce.SyncWithRetry (GetPathsFilename())
	),
	
	-- Returns list of path-links.
	--	If 'Bounds' is supplied, the load will be limited to links within range of that X/Y box. 
	--	Bounds:(DataPair Min:[x,y,z] Max:[x,y,z])
	fn LoadPaths Bounds: BoundsPadding:20 = 
	(
		PushPrompt "NavPaths: Loading 'Paths.xml'..."
		local Filename = GetPathsFilename()
		
		-- Open paths.xml:
		local PathsXml = undefined
		try 
		(
			gc light:true
			(dotnetclass "system.gc").collect()
			
			local LoadOptions = (dotnetclass "RSG.SceneXml.LoadOptions").Objects
			local ct = RsContentTree.ContentTreeHelper.ContentTree
			local cth = RsContentTree.ContentTreeHelper		
			PathsXml = (dotnetobject "RSG.SceneXml.Scene" ct cth Filename LoadOptions False)
		)
		catch ()
		
		if (PathsXml == undefined) do 
		(
			PopPrompt()
			return #()
		)
		
		local Nodes = #()
		local Links = #()
		
		local NodesInBox = #{}
		local NodeGuids = #()
		
		ReplacePrompt "NavPaths: Parsing Nodes..."
		
		local BoundsMin, BoundsMax
		local LimitBounds = (Bounds != unsupplied)
		if LimitBounds do 
		(
			BoundsMin = (Bounds.Min - BoundsPadding)
			BoundsMax = (Bounds.Max + BoundsPadding)
		)
		
		--local TimeStart = TimeStamp()
		struct NavLinkDef (Nodes, Width, Speed)
		local MaxPathWidth = LaneWidth
		
		for ObjElem in PathsXml.Objects do 
		(
			-- Get node-position:
			local ObjXform = ObjElem.NodeTransform.D
			local ObjPos = [ObjXform.X, ObjXform.Y, ObjXform.Z]
			
			-- Is link/node inside padded bounding-box?
			local AllowUse = True
			if LimitBounds do 
			(
				-- Check on x/y:
				for n = 1 to 2 while AllowUse do 
				(
					AllowUse = (ObjPos[n] >= BoundsMin[n]) and (ObjPos[n] <= BoundsMax[n])
				)
			)
			
			if AllowUse do 
			(
				case ObjElem.Class of 
				(
					"vehiclenode":
					(
						local NodeGuid = ObjElem.Guid.ToString()
						local NodeSpeed = (ObjElem.GetAttribute "Speed" DefSpeed)
						append Nodes (DataPair Pos:ObjPos Speed:NodeSpeed)
						append NodeGuids (NodeGuid)
					)
					"vehiclelink":
					(
						local LinkNarrow = (ObjElem.GetAttribute "Narrowroad" DefNarrowroad)
						local LinkLaneSize = if LinkNarrow then LaneWidthNarrow else LaneWidth
						local LinkWidth = LinkLaneSize
						
						-- If 'Width' is -1 or 0, it'll be single-lane, otherwise the value will be size of the central reservation:
						local LinkWidthNum = (ObjElem.GetAttribute "Width" DefWidth)
						
						if (LinkWidthNum > 0) do 
						(
							local LinkLanesIn = (ObjElem.GetAttribute "Lanes In" DefLanesIn)
							local LinkLanesOut = (ObjElem.GetAttribute "Lanes Out" DefLanesOut)
							
							LinkWidth = LinkWidthNum + (LinkLaneSize * (LinkLanesIn + LinkLanesOut))
							
							if (LinkWidth > MaxPathWidth) do 
							(
								MaxPathWidth = LinkWidth
							)
						)
						
						-- Get link's node-guids:
						local LinkNodeGuids = (for NodeGuid in ObjElem.GenericReferenceGuids collect (NodeGuid.ToString()))
						append Links (NavLinkDef Nodes:LinkNodeGuids Width:LinkWidth)
					)
				)
			)
		)
		--format "Loading NavNodes: Time taken: %s\n" ((TimeStamp() - TimeStart) / 1000.0)
		--local TimeStart = TimeStamp()
		ReplacePrompt "NavPaths: Linking Nodes..."
		
		-- Bring in bounds-padding to MaxPathWidth:
		if LimitBounds do 
		(
			BoundsMin = (Bounds.Min - MaxPathWidth)
			BoundsMax = (Bounds.Max + MaxPathWidth)
		)
		
		for ThisLink in Links do 
		(
			-- Convert links' node-guids to point to matching node-data:
			ThisLink.Nodes = for NodeGuid in ThisLink.Nodes collect 
			(
				local NodeNum = (FindItem NodeGuids NodeGuid)
				if (NodeNum == 0) then DontCollect else Nodes[NodeNum]
			)
			
			-- Make sure at least one node is within (MaxRadius) of boundingbox:
			if (ThisLink.Nodes.Count == 2) do 
			(
				-- Get minimum node-speed for link: (0-3)
				ThisLink.Speed = Amin (for ThisNode in ThisLink.Nodes collect ThisNode.Speed)
				
				-- Replace nodes with their positions:
				ThisLink.Nodes = (for ThisNode in ThisLink.Nodes collect ThisNode.Pos)
				
				-- Do link bounds-testing:
				if LimitBounds do 
				(
					-- Is one of this link's nodes within the current bounds?
					local AllowUse = False
					for NodePos in ThisLink.Nodes while (not AllowUse) do 
					(
						AllowUse = True
						for n = 1 to 2 while AllowUse do 
						(
							AllowUse = (NodePos[n] >= BoundsMin[n]) and (NodePos[n] <= BoundsMax[n])
						)
					)
					
					-- Clear nodes if neither was within bounds:
					if (not AllowUse) do (ThisLink.Nodes.Count = 0)
				)
			)
		)
		--format "Linking Nav Nodes: Time taken: %s\n" ((TimeStamp() - TimeStart) / 1000.0)
		
		-- Filter out incomplete links:
		Links = for ThisLink in Links where (ThisLink.Nodes.Count == 2) collect ThisLink
		
		PopPrompt()
		
		return Links
	),
	
	-- Returns list of pathlinks covering a given object:
	fn GetObjPathLinks Obj = 
	(
		LoadPaths Bounds:(DataPair Min:Obj.Min Max:Obj.Max)
	),
	
	-- Creates spline-object showing a given set of PathLinks:
	fn CreatePathLines PathLinks Pos:[0,0,0] = 
	(
		local SplineObj = SplineShape Pos:Pos Name:(UniqueName "AiPathsSpline") WireColor:Yellow
		
		for PathNum = 1 to PathLinks.Count do 
		(
			local ThisPath = PathLinks[PathNum]
			AddNewSpline SplineObj
			AddKnot SplineObj PathNum #Corner #Line ThisPath.Nodes[1]
			AddKnot SplineObj PathNum #Corner #Line ThisPath.Nodes[2]
		)
		UpdateShape SplineObj
		
		return SplineObj
	),
	
	-- Returns lists of object-faces, one list for each of the three road-node speeds:
	fn GetObjRoadFaces Obj PathLinks: = 
	(
		if (PathLinks == unsupplied) do 
		(
			PathLinks = (This.GetObjPathLinks Obj)
		)
		
		-- Return empty lists if no path-links were found:
		if (PathLinks.Count == 0) do 
		(
			return (for n = 1 to 3 collect #{})
		)
		
		local ObjOp = RsMeshPolyOp Obj
		local ObjGetVert = ObjOp.GetVert
		
		local MaxRadius = 4
		
		local VertCount = (GetNumVerts Obj)
		local VertPositions = for n = 1 to VertCount collect (ObjGetVert Obj n)
		local VertPositionsXY = for Pos in VertPositions collect [Pos.X, Pos.Y, 0]
		
		local SpeedVerts = for n = 1 to 3 collect #{}
		for Verts in SpeedVerts do (Verts.Count = VertCount)
		
		local TimeStart = TimeStamp()
		
		PushPrompt "NavPaths: Finding Verts..."
		for ThisPath in PathLinks do 
		(
			-- Road-size:
			local Radius = (ThisPath.Width / 2)
			
			-- Speed can be (0-3) but we want it as (1-3)...
			local SpeedNum = (ThisPath.Speed + 1)
			if (SpeedNum > 3) do (SpeedNum = 3)
			
			local PosA = ThisPath.Nodes[1]
			local PosB = ThisPath.Nodes[2]
			
			local PathWidth = ThisPath.Width
			local PathXYLength = Length [PosB.X - PosA.X, PosB.Y - PosA.Y]
			
			-- Found verts will be added to this list:
			local VertsList = SpeedVerts[SpeedNum]
			
			-- Values for finding distance to line:
			local LineVec = (PosB - PosA)
			local NormLineVec = (Normalize LineVec)
			
			local PosAXY = [PosA.X, PosA.Y, 0]
			local PosBXY = [PosB.X, PosB.Y, 0]
			local LineVecXY = (PosBXY - PosAXY)
			
			-- Get line's bounding-circle details:
			local LineCirclePos = (PosAXY + (LineVecXY / 2))
			local LineCircleRadius = Radius + ((Length LineVecXY) / 2)
			
			-- Build values for calculating whether or not a points lie within this path's top-down box:
			local NormLineXY = (Normalize LineVecXY)
			local NormPerpLineXY = [-NormLineXY.Y, NormLineXY.X, 0]
			local RadiusVec = (Radius * NormPerpLineXY)
			local RectCorner = (PosAXY - RadiusVec)
			
			-- Get 3d path-rectangle's up-normal:
			local PathUpNorm = (Cross NormLineVec NormPerpLineXY)
			local DotUpNorm = (Dot PathUpNorm PathUpNorm)
			
			-- Test verts that haven't been flagged for this path-speed yet:
			for VertNum in (-VertsList) do 
			(
				local TestPosXY = VertPositionsXY[VertNum]
				
				-- Quick initial test - is this vert within line's bounding-sphere?
				if ((Distance TestPosXY LineCirclePos) < LineCircleRadius) do 
				(
					-- Is this vert within one of line's nodes' XY circles?
					local XYmatch = case of 
					(
						((Distance TestPosXY PosAXY) <= Radius):True
						((Distance TestPosXY PosBXY) <= Radius):True
						Default:False
					)
					
					-- Is vert within path's XY rectangle?
					if (not XYmatch) do 
					(
						-- Get vert-position relative to rectangle-corner:
						local MovedPos = (TestPosXY - RectCorner)
						
						-- Is position within path's X/Y rectangle?
						local DotProd = (Dot MovedPos NormLineXY)
						if (DotProd >= 0) and (DotProd <= PathXYLength) do 
						(
							DotProd = (Dot MovedPos NormPerpLineXY)
							
							if (DotProd >= 0) and (DotProd <= PathWidth) do 
							(
								XYmatch = True
							)
						)
					)
					
					-- If this was a horizontal match, make sure the path isn't too far away from this vert:
					if XYmatch do 
					(
						-- Get full 3D vert-position:
						local TestPos = VertPositions[VertNum]
						
						-- Get vert's distance to path-rectangle plane:
						local sN = -(Dot  PathUpNorm (TestPos - PosA))
						local sB = (sN / DotUpNorm)
						local Dist = Length (sb * PathUpNorm)
						
						-- Allow vert to match if it is within a certain distance of path-plane:
						if ((Abs Dist) < VerticalMatchDist) do 
						(
							VertsList[VertNum] = True
						)
					)
				)
			)
		)
		
		--format "NavPath Verts: Time taken: %s\n" ((TimeStamp() - TimeStart) / 1000.0)
		
		Free VertPositions
		Free VertPositionsXY 
		
		ReplacePrompt "NavPaths: Finding Faces"
		
		-- Find face-lists for each node-speed:
		local SpeedNames = #("Trail", "Path", "Road")		
		local FaceLists = for n = 1 to SpeedVerts.Count collect 
		(
			local Verts = SpeedVerts[n]
			--format "%:\t % verts\n" SpeedNames[n] Verts.NumberSet
			(ObjOp.GetFacesUsingVert Obj Verts)
		)
		
		PopPrompt()
		
		return FaceLists
	),
	
	-- Apply appropriate Road flags to an (editable mesh/poly) object's collision-materials, 
	--	copying materials and reassigning matids as necessary.
	fn ApplyRoadFlagsToCollision Obj PathLinks: = 
	(
		-- Get collision-material flag-indices for road-flags, in ascending road-size/speed order:
		local RoadFlagNames = #("Road - Path", "Road - Trail", "Road - Road")
		local CollFlagNames = RexGetCollisionFlagNames()
		local RoadFlagNums = for FlagName in RoadFlagNames collect (FindItem CollFlagNames FlagName)
		
		-- Don't attempt to apply road-flags if they aren't defined for this project:
		--	(i.e. if all FindItem values were zero)
		if ((amax RoadFlagNums) == 0) do return OK
		
		-- Get Road face-lists, one for each of the three road-speeds we have collision-flags for:
		local RoadFaceLists = This.GetObjRoadFaces Obj PathLinks:PathLinks
		
		-- Do any of this objects faces require road-flagging?
		local HasRoads = False
		for FaceList in RoadFaceLists while (not HasRoads) do 
		(
			HasRoads = (FaceList.NumberSet != 0)
		)
		
		-- Do nothing if no roady faces were found:
		if (not HasRoads) do return False
		
		PushPrompt "NavPaths: Flagging Road-faces..."
		
		-- Get list of all flag-combinations that we want to test:
		local FlagsCount = RoadFaceLists.Count
		local MaxFlagVal = (2^FlagsCount - 1)
		local FlagBitArrs = for FlagsCombo = 1 to MaxFlagVal collect 
		(
			local BitArr = #{}
			BitArr.Count = FlagsCount
			for n = 1 to FlagsCount where (Bit.Get FlagsCombo n) do 
			(
				BitArr[n] = True
			)
			BitArr
		)
		
		-- Sort flag-bitarrays, in descending order of number of flags set:
		Qsort FlagBitArrs (fn SortByCount v1 v2 = (v2.NumberSet - v1.NumberSet))
		
		-- Collect lists of faces for each possible flag-combo:
		--	Each individual face will only be collected once.  Unflagged faces are not collected.
		local FlagComboFaceLists = for BitArr in FlagBitArrs collect 
		(
			-- Get list of faces that use all flag-bits represented by 'FlagsCombo'
			local ThisComboFaces = #{}
			local FirstBit = True
			for n in BitArr do 
			(		
				if FirstBit then 
				(
					ThisComboFaces = RoadFaceLists[n]
					FirstBit = False
				)
				else 
				(
					ThisComboFaces = (ThisComboFaces * RoadFaceLists[n])
				)
			)
			
			-- Don't allow other flag-combos to use the faces that were found for this combo:
			if (ThisComboFaces.NumberSet != 0) do 
			(
				for n = 1 to FlagsCount do 
				(
					RoadFaceLists[n] -= ThisComboFaces
				)
			)
			--format "%: %\n" BitArr ThisComboFaces
			
			ThisComboFaces
		)
		
		-- Get list of descriptors for object's current collision-materials:
		local Mat = Obj.Material
		local MatInfos = (gRsMaterials.GetObjMatData Obj Mat:Mat GetFaces:True FlatList:True)
		MatInfos = for MatInfo in MatInfos where MatInfo.isCollisionMat collect MatInfo
		
		-- Get current highest matid:
		local MaxMatId = Amax (for MatInfo in MatInfos collect MatInfo.MatId)
		
		-- Convert non-multimats to multimaterial:
		if (MaxMatId == -1) do 
		(
			Mat = MultiMaterial MaterialList:#(MatInfos[1].Mat) MaterialIdList:#(1)
			MatInfos.MatId = 1
			MaxMatId = 1
			Obj.Material = Mat
		)
		
		-- Get appropriate function for setting face-matids:
		local ObjSetFaceMatIds = (RsSetFaceMatIdsFunc Obj)
		
		-- Process each of object's collision-material decriptors:
		for MatInfo in MatInfos do 
		(
			-- Get list of material's faces that should have road-flags:
			local MatHasRoadFaces = False
			local MatFaces = MatInfo.Faces
			local MatRoadFaces = for RoadFaceList in FlagComboFaceLists collect 
			(
				local RoadFaces = (RoadFaceList * MatFaces)
				if (RoadFaces.NumberSet != 0) do 
				(
					MatHasRoadFaces = True
				)
				RoadFaces
			)
			
			-- Process material if any faces need to be road-flagged:
			if MatHasRoadFaces do 
			(
				-- Get object-material's Road collision-flags as road-speed bitarray:
				local FlagsVal = (RexGetCollisionFlags MatInfo.Mat)
				
				-- Convert FlagsVal to speed-flags bitarray:
				local MatFlagsBitArr = #{}
				for RoadFlagIdx = 1 to RoadFlagNums.Count do 
				(
					if (Bit.Get FlagsVal RoadFlagNums[RoadFlagIdx]) do 
					(
						MatFlagsBitArr[RoadFlagIdx] = True
					)
				)
				
				-- Make new collision-materials for faces that don't already have the required flags:
				-- 	(flags will only ever be added - this should not wipe any existing flags)
				for RoadFlagsIdx = 1 to MaxFlagVal where (MatRoadFaces[RoadFlagsIdx].NumberSet != 0) do 
				(
					-- Which flags, if any, need to be added to this material?
					local FlagsNeeded = (FlagBitArrs[RoadFlagsIdx] - MatFlagsBitArr)
					
					if (FlagsNeeded.NumberSet != 0) do 
					(
						-- Add required flags to material's flags-integer:
						local NewFlagsVal = (Copy FlagsVal)
						for RoadFlagIdx = FlagsNeeded do 
						(
							NewFlagsVal = (Bit.Set NewFlagsVal RoadFlagNums[RoadFlagIdx] True)
						)
						
						-- Make copy of original material, with updated flags:
						local NewMat = (Copy MatInfo.Mat)
						NewMat.Name = MatInfo.Mat.Name
						RexSetCollisionFlags NewMat NewFlagsVal
						
						-- Add new material to multimat's lists:
						append Mat.MaterialIdList (MaxMatId += 1)
						Mat.MaterialList[Mat.MaterialIdList.Count] = NewMat
						
						-- Set relevant faces to use this new matid:
						ObjSetFaceMatIds Obj MatRoadFaces[RoadFlagsIdx] MaxMatId
					)
				)
			)
		)
		
		PopPrompt()
		
		return OK
	)
)
global gRsNavPathLoad = RsPathLoad()

-- Test-functions:
IF FALSE DO 
(
	clearlistener()
	for obj in getcurrentselection() do 
	(
		--local PathLinks = gRsNavPathLoad.GetObjPathLinks Obj
		--gRsNavPathLoad.CreatePathLines PathLinks Pos:Obj.Pos
		local FaceLists = gRsNavPathLoad.GetObjRoadFaces Obj --PathLinks:PathLinks
		
		-- Select road-faces:
		local AllFacesList = #{}
		for ThisList in FaceLists do (AllFacesList += ThisList)
		SetFaceSelection Obj AllFacesList
		--gRsNavPathLoad.ApplyRoadFlagsToCollision Obj --PathLinks:PathLinks
	)
)