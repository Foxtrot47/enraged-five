-----------------------------------------------------------------------------
-- Collision Convert tool
-----------------------------------------------------------------------------
-- Allows user to convert between various collision/geometry classes
-----------------------------------------------------------------------------

--try (destroyDialog RsCollisionConverterTool) catch ()
callbacks.removeScripts id:#RsCollConvertTool

filein (RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Collision/Collision_Convert.ms")

rollout RsCollisionConverterTool "Collision Converter" width:350
(
	local convertedDataList, convertedObjNodes
	local btnWidth = 200
	local btnWidthSmall = 151
	
	group "Temporary Conversion:"
	(
		groupBox grpTempConvert "Convert to Geometry:" width:(RsCollisionConverterTool.width - 18) height:40 offset:[-4,-5]
		button btnConvertCollToGeom "Collision -> Geometry" width:btnWidth offset:[-1,-30] tooltip:"Convert selected collision-objects to geometry.\nCollision-attributes are stored for later reconversion."

		groupBox grpTempRevert "Revert to Collision:" width:grpTempConvert.width height:74 offset:[-4,2]
		label lblConvertBackStatus "xxx" width:180 height:27 offset:[-1, 10 - grpTempRevert.height]
		button btnRevertSelGeomToColl "Revert Selected" width:(btnWidthSmall + 5) align:#right offset:[-3,0] across:2 tooltip:"Convert selected temp-converted geometry-objects back to their original collision-types."
		button btnRevertAllGeomToColl "Revert All" width:(btnWidthSmall + 5) align:#left offset:[3,0] tooltip:"Convert all temp-converted geometry-objects back to their original collision-types."
		
		label lblDummyA "" height:0
	)
	
	group "Permanent Conversion:"
	(
		groupBox grpEquivs "To Equivalent Classes:" width:grpTempConvert.width height:40 offset:[-4,-5]
		button btnConvToGeo "Geometry" width:(btnWidthSmall + 5) align:#right offset:[-3,-30] across:2 tooltip:"Convert selected collision-objects to geometry"
		button btnConvToCol "Collision" width:(btnWidthSmall + 5) align:#left offset:[3,-30] tooltip:"Convert selected geometry-objects to collision"
	
		groupBox grpGeom "To Geometry:" width:((grpTempConvert.width / 2) - 3) height:135 offset:[-4,2]
		button btnConvToGeoBox "Box" width:btnWidthSmall pos:(grpGeom.pos + [5,16]) tooltip:"Convert selected geometry/collision objects to Box primitives"
		button btnConvToGeoSph "Sphere" width:btnWidthSmall pos:(btnConvToGeoBox.pos + [0,23]) tooltip:"Convert selected geometry/collision objects to Sphere primitives"
		button btnConvToGeoCyl "Cylinder" width:btnWidthSmall pos:(btnConvToGeoSph.pos + [0,23]) tooltip:"Convert selected geometry/collision objects to Cylinder primitives"
		button btnConvToGeoCap "Capsule" width:btnWidthSmall pos:(btnConvToGeoCyl.pos + [0,23]) tooltip:"Convert selected geometry/collision objects to Capsule primitives"
		button btnConvToGeoMsh "Mesh" width:btnWidthSmall pos:(btnConvToGeoCap.pos + [0,23]) tooltip:"Convert selected geometry/collision objects to mesh geometry"
		
		groupBox grpColl "To Collision:" width:(grpGeom.width) height:(grpGeom.height) pos:(grpGeom.pos + [grpGeom.width + 7, 0])
		button btnConvToColBox "Box" width:btnWidthSmall pos:(grpColl.pos + [5,16]) tooltip:"Convert selected geometry/collision objects to Collision Boxes\n(Converted collision retains its attributes)"
		button btnConvToColSph "Sphere" width:btnWidthSmall pos:(btnConvToColBox.pos + [0,23]) tooltip:"Convert selected geometry/collision objects to Collision Spheres\n(Converted collision retains its attributes)"
		button btnConvToColCyl "Cylinder" width:btnWidthSmall pos:(btnConvToColSph.pos + [0,23]) tooltip:"Convert selected geometry/collision objects to Collision Cylinders\n(Converted collision retains its attributes)"
		button btnConvToColCap "Capsule" width:btnWidthSmall pos:(btnConvToColCyl.pos + [0,23]) tooltip:"Convert selected geometry/collision objects to Collision Capsules\n(Converted collision retains its attributes)"
		button btnConvToCollMsh "Mesh" width:btnWidthSmall pos:(btnConvToColCap.pos + [0,23]) tooltip:"Convert selected geometry/collision objects to Collision Meshes\n(Converted collision retains its attributes)"
		
		label lblDummyB "" height:0
	)
	
	label lblHelpBack "          " offset:[0,-20] align:#right
	hyperlink lnkHelp "Help?" pos:(lblHelpBack.pos + [3,0]) address:"https://devstar.rockstargames.com/wiki/index.php/CollisionConvert" color:Blue hoverColor:Blue visitedColor:Blue
	
	-- Update rollout's status-label:
	fn updateStatus = 
	(
		local txt = stringStream ""
		
		local convCount = 0
		local selConvCount = 0
		
		-- Collect converted-object counts:
		for item in convertedDataList do 
		(
			local obj = item.obj
			
			if (isValidNode obj) and (isKindOf obj GeometryClass) do 
			(
				convCount += 1
				
				if obj.isSelected do 
				(
					selConvCount += 1
				)
			)
		)

		format "Temp-converted objects: %\n" convCount to:txt
		format "(% selected)" selConvCount to:txt
		
		lblConvertBackStatus.text = (txt as string)
	)
	
	-- Load converted-objects data from datanodes in scene:
	fn loadFromDataNodes = 
	(
		if RsCollConvertDebug do 
			(format "RsCollisionConverterTool.loadFromDataNodes()")
		
		convertedDataList = RsCollConvert.loadFromDataNodes()
		convertedObjNodes = for item in convertedDataList collect item.obj
		
		updateStatus()
	)
	
	-- Save rollout's converted-objects data to datanodes in scene:
	fn saveToDataNodes = 
	(
		pushPrompt "Storing reversion-data"
		if RsCollConvertDebug do 
			(format "RsCollisionConverterTool.saveToDataNodes()\n")
		
		local retval = RsCollConvert.saveToDataNodes convertedDataList
		popPrompt()
		
		return retval
	)
	
	fn setupCallbacks = 
	(
		callbacks.removeScripts id:#RsCollConvertTool
		callbacks.addScript #filePreOpenProcess "RsCollisionConverterTool.saveToDataNodes()" id:#RsCollConvertTool
		callbacks.addScript #filePostOpen "RsCollisionConverterTool.loadFromDataNodes()" id:#RsCollConvertTool
		callbacks.addScript #filePostMerge "RsCollisionConverterTool.loadFromDataNodes()" id:#RsCollConvertTool
		
		callbacks.addScript #sceneUndo "RsCollisionConverterTool.loadFromDataNodes()" id:#RsCollConvertTool
		callbacks.addScript #sceneRedo "RsCollisionConverterTool.loadFromDataNodes()" id:#RsCollConvertTool
		
		callbacks.addScript #selectionSetChanged "RsCollisionConverterTool.updateStatus()" id:#RsCollConvertTool
	)
	
	-- Revert selected/all temp-converted objects back to being collision:
	fn revertGeomToColl objs:(selection as array) all:False = 
	(
		local convObjNums = #{}
		convObjNums.count = convertedDataList.count
		
		for n = 1 to convertedDataList.count do 
		(
			local item = convertedDataList[n]
			
			if (isValidNode item.obj) and (all) or (findItem objs item.obj != 0) do 
			(
				convObjNums[n] = True
			)
		)
		
		if (convObjNums.numberSet != 0) do 
		(
			callbacks.removeScripts id:#RsCollConvertTool
			gc()
			
			with redraw off with undo off 
			(
				local timeStart = timeStamp()
				
				local convItems = for objNum = convObjNums collect convertedDataList[objNum]
				
				local selObjNums = #{}
				for n = 1 to convItems.count do 
				(
					selObjNums[n] = (isValidNode convItems[n].obj) and (convItems[n].obj.isSelected)
				)

				progressStart "Converting Geom to Collision..."
				
				local notCancelled = True
				local delObjs = #()
				local newObjs = for n = 1 to convItems.count while (notCancelled and (notCancelled = progressUpdate (100.0 * n / convItems.count))) collect 
				(
					local convData = convItems[n]
					local newObj = RsCollConvert.convertTo convData.obj convData.objClass restoreConvData:convData delObjs:delObjs
					notCancelled = (newObj != False)

					if notCancelled then newObj else dontCollect
				)
				progressEnd()
				
				if (delObjs.count != 0) do (delete delObjs)
				
				(format "Time taken to revert geom to coll: %s\n" ((timestamp() - timeStart) / 1000.0))
				
				-- If selected objects have been converted, select the new versions:
				local selObjs = for n = selObjNums where (isValidNode newObjs[n]) collect newObjs[n]

				pushPrompt "Selecting new objects"
				selectMore selObjs
				popPrompt()
				
				-- Filter reverted items out of converted-things list:
				convertedDataList = for item in convertedDataList where (isValidNode item.obj) and (isKindOf item.obj GeometryClass) collect item
				convertedObjNodes = for item in convertedDataList collect item.obj
				
				-- Update data-nodes:
				saveToDataNodes()
				
				-- Clear any undo...
				gc()
			)
			
			forceCompleteRedraw()
			updateStatus()
			setupCallbacks()
		)

		return OK
	)
	
	fn convertSelToClass toClass toCollision:False = 
	(
		-- Revert any selected temp-geom objects if converting to collision:
		if toCollision do 
		(
			revertGeomToColl objs:(selection as array) all:False
		)
		
		local useFunc = (isKindOf toClass MAXScriptFunction)
		local classFunc = if useFunc then toClass else undefined
		
		-- Build list of objects and classes to convert them to:
		--print ((selection as array) as string)
		local convertList = for obj in selection collect 
		(
			print obj
			
			if useFunc do 
			(
				toClass = classFunc (classOf obj)
			)
			
			if (toClass != undefined) and (not isKindOf obj toClass) then (dataPair obj:obj toClass:toClass) else dontCollect
		)
		
		if (convertList.count != 0) do 
		(
			undo "convert" on 
			(
				with redraw off 
				(					
					callbacks.removeScripts id:#RsCollConvertTool
					
					progressStart "Converting objects..."
					local timeStart = timeStamp()
					
					local selObjNums = #{}
					for n = 1 to convertList.count do 
					(
						selObjNums[n] = (isValidNode convertList[n].obj) and (convertList[n].obj.isSelected)
					)
					
					local cancelled = False
					local delObjs = #()
					local newObjs = for n = 1 to convertList.count while (progressUpdate (100.0 * n / convertList.count)) collect 
					(
						local newObj = RsCollConvert.convertTo convertList[n].obj convertList[n].toClass delObjs:delObjs
						cancelled = (newObj == False)
						
						if cancelled then dontCollect else newObj
					)
					
					if (delObjs.count != 0) do (delete delObjs)
					
					(format "Time taken to convert: %s\n" ((timestamp() - timeStart) / 1000.0))
					
					-- If old objects were selected, select the new versions:
					local selObjs = for n = selObjNums where (isValidNode newObjs[n]) collect newObjs[n]
					selectMore selObjs
										
					progressEnd()
					
					forceCompleteRedraw()
					updateStatus()
					setupCallbacks()
				)
			)
		)
	)
	
	on btnConvToGeo pressed do (convertSelToClass RsCollConvert.geomSwapClass)
	on btnConvToCol pressed do (convertSelToClass RsCollConvert.collSwapClass toCollision:True)
	on btnConvToGeoBox pressed do (convertSelToClass Box)
	on btnConvToGeoSph pressed do (convertSelToClass Sphere)
	on btnConvToGeoCyl pressed do (convertSelToClass Cylinder)
	on btnConvToGeoCap pressed do (convertSelToClass Capsule)
	on btnConvToGeoMsh pressed do (convertSelToClass Editable_Mesh)
	on btnConvToColBox pressed do (convertSelToClass Col_Box toCollision:True)
	on btnConvToColSph pressed do (convertSelToClass Col_Sphere toCollision:True)
	on btnConvToColCyl pressed do (convertSelToClass Col_Cylinder toCollision:True)
	on btnConvToColCap pressed do (convertSelToClass Col_Capsule toCollision:True)
	on btnConvToCollMsh pressed do (convertSelToClass Col_Mesh toCollision:True)
	
	on btnConvertCollToGeom pressed do 
	(
		with redraw off with undo off
		(
			callbacks.removeScripts id:#RsCollConvertTool
			
			local convSelList = for obj in (selection as array) collect 
			(
				local convClass = RsCollConvert.geomSwapClass (classOf obj)
				if (convClass == undefined) then dontCollect else (dataPair obj:obj convClass:convClass)
			)
			
			local newConvDataList = #()
			local newObjs = #()
			
			if (convSelList.count != 0) do 
			(
				local timeStart = timeStamp()
				
				progressStart "Converting Collision to Geom..."
				gc()
				local delObjs = #()
				
				for n = 1 to convSelList.count while (progressUpdate (100.0 * n / convSelList.count)) collect 
				(
					local obj = convSelList[n].obj
					local convClass = convSelList[n].convClass

					local newConvData = RsCollConvert.convertTo obj convClass returnConvData:True deleteOld:True delObjs:delObjs
					
					if (newConvData != undefined) and (isValidNode newConvData.obj) then 
					(
						append newConvDataList newConvData
						append newObjs newConvData.obj						
					)
					else dontCollect
				)
				
				if (delObjs.count != 0) do (delete delObjs)
				
				progressEnd()
				
				(format "Time taken to convert coll to geom: %s\n" ((timestamp() - timeStart) / 1000.0))
			)
			
			if (newConvDataList.count != 0) do 
			(
				-- Set converted objects as "Dont Export", if converted class supports it
				for obj in newObjs do 
				(
					local objClass = getAttrClass obj
					
					if (objClass != undefined) do 
					(
						local idxDontExport = getAttrIndex objClass "Dont Export"
						
						if (idxDontExport != undefined) do 
						(
							setattr obj idxDontExport True
						)
					)
				)
				
				selectMore newObjs
				
				-- Add new converted-objs data to convertedDataList:
				join convertedDataList newConvDataList
				join convertedObjNodes newObjs
				
				-- Update data stored on datanodes:
				RsCollConvert.saveToDataNodes convertedDataList
				
				forceCompleteRedraw()
				gc()
				
				updateStatus()
				
			)
			
			setupCallbacks()
		)
		
		OK
	)
	on btnRevertSelGeomToColl pressed do 
	(
		revertGeomToColl all:False
	)
	on btnRevertAllGeomToColl pressed do 
	(
		revertGeomToColl all:True
	)
	
	on RsCollisionConverterTool open do 
	(
		-- Move "Help?" up to the top of rollout:
		lblHelpBack.pos.y = lnkHelp.pos.y = 6
		
		loadFromDataNodes()
		setupCallbacks()
	)
	
	on RsCollisionConverterTool close do 
	(
		callbacks.removeScripts id:#RsCollConvertTool
	)
	
	on RsCollisionConverterTool rolledUp down do 
	(
		RsSettingWrite "RsCollisionConverterTool" "rollup" (not down)
	)
)

--createDialog RsCollisionConverterTool
