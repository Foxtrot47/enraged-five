-----------------------------------------------------------------------------
-- Collision Convert structure
-----------------------------------------------------------------------------
-- Struct contains functions for converting between different 
-- collision/geometry classes, and for storing attributes for 
-- collision that has been temporarily converted to geometry
-----------------------------------------------------------------------------

global RsCollConvertDebug = False

struct RsCollConvert  
(
	obj, -- Object described
	transform, -- Object's transform (moved/rotated to common point for each object-type)
	size, -- Radius Width/Length/Height/Etc.  Left undefined if obj is non-primitive
	
	cylinderType = False, -- If True, converter won't need to run alignForCylinder, when converting to Cylinder types
	
	-- The following values will be stored to xml on file-save:
	objName, objClass, primType,
	attrsUsed = #{}, attrVals = #(), userProps = "", objColour, collUseClr = False, -- Used to store collision-object attributes
	
	-- Store this struct-instance's object-data to xml:
	fn saveObjToXml xmlWriter objsList:#() = 
	(
		if (isValidNode obj) do 
		(
			xmlWriter.WriteStartElement "collData"
			
			-- Add node to object-list, and store its index:
			append objsList obj
			xmlWriter.WriteAttributeString "objNum" (objsList.count as string)
			
			xmlWriter.WriteAttributeString "objName" (objName as string)
			xmlWriter.WriteAttributeString "objClass" (objClass as string)
			xmlWriter.WriteAttributeString "primType" (primType as string)
			xmlWriter.WriteAttributeString "objColour" (objColour as string)
			xmlWriter.WriteAttributeString "collUseClr" (collUseClr as string)
			xmlWriter.WriteAttributeString "userProps" (userProps as string)

			-- Write attributes out:
			for attrNum in attrsUsed do 
			(			
				xmlWriter.WriteStartElement "attr"
				xmlWriter.WriteAttributeString "idx" (attrNum as string)
				xmlWriter.WriteAttributeString "value" (attrVals[attrNum] as string)
				xmlWriter.WriteAttributeString "class" ((classOf attrVals[attrNum]) as string)
				xmlWriter.WriteEndElement()
			)
			
			xmlWriter.WriteEndElement()
		)
			
		return OK
	),
	
	-- Returns an object-data struct reconstituted from xml:
	fn loadObjFromXml xmlReader objsList:#() = 
	(
		local objNum = xmlReader.Item["objNum"]

		if (objNum == undefined) do return dontCollect
		
		-- Retrieve indexed object from list:
		local getObj = objsList[objNum as integer]

		-- Cancel object-import if object wasn't found...
		if (not isValidNode getObj) do return dontCollect
		
		-- If obj was found, generate new struct:
		local restoredData = RsCollConvert obj:getObj
		
		restoredData.objName = xmlReader.Item["objName"]
		restoredData.objClass = execute xmlReader.Item["objClass"]
		restoredData.primType = xmlReader.Item["primType"] as name
		restoredData.objColour = execute xmlReader.Item["objColour"]
		restoredData.collUseClr = xmlReader.Item["collUseClr"] as BooleanClass
		restoredData.userProps = xmlReader.Item["userProps"]
		
		-- Reconstitute attributes-list, if it has one:
		if (not xmlReader.IsEmptyElement) do 
		(
			xmlReader.Read()
			local isSameElement = True
			while (xmlReader.NodeType != xmlReader.NodeType.EndElement) do 
			(
				if (xmlReader.NodeType == xmlReader.NodeType.Element) do 
				(
					case xmlReader.name of 
					(
						"attr":
						(
							local attrNum = xmlReader.Item["idx"]

							if (attrNum != undefined) do 
							(
								attrNum = attrNum as integer
								
								local attrVal = xmlReader.Item["value"]
								local attrClass = xmlReader.Item["class"] as name

								-- Convert string to correct class: 
								attrVal = case attrClass of 
								(
									#string:(attrVal)
									#Integer:(attrVal as Integer)
									#Float:(attrVal as Float)
									#BooleanClass:(attrVal as BooleanClass)
									Default:(execute attrVal)
								)
								
								restoredData.attrsUsed[attrNum] = True
								restoredData.attrVals[attrNum] = attrVal
							)
						)
					)
				)
				
				-- Read next element:
				xmlReader.Read()
			)
		)
		
		return restoredData
	),
	
	-- Replaces data in scene's collision-convert data-node paramblocks with data from objDataList:
	fn saveToDataNodes objDataList = 
	(
		if RsCollConvertDebug do 
			(format "  RsCollConvert.saveToDataNodes (% items)\n" objDataList.count)
		
		-- Don't store data for invalid/collision objects - we only store data for objects that have been converted to geometry:
		local storeItems = for item in objDataList where (isValidNode item.obj) and (GetAttrClass item.obj != "Gta Collision") collect item
		
		local objConts = #()
		local objDataPerCont = #()
		
		-- Seperate list up per container:
		for item in storeItems do 
		(
			local objCont = Containers.IsInContainer item.obj
			
			local contNum = findItem objConts objCont
			if (contNum == 0) do 
			(
				append objConts objCont
				append objDataPerCont #()
				contNum = objConts.count
			)
			append objDataPerCont[contNum] item
		)
		
		-- Match existing data-nodes up with their container-lists:
		local contDataNodes = #()
		local delNodes = #()
		for dataNode in helpers where (isKindOf dataNode RsGenericDataNode) and (dataNode.dataClass == "CollisionConvert") do 
		(
			local dataNodeCont = Containers.IsInContainer dataNode
			local contNum = findItem objConts dataNodeCont
			
			if (contNum == 0) or (contDataNodes[contNum] != undefined) then 
			(
				-- If dataNode isn't required, or a container has more than one, mark it for deletion:
				append delNodes dataNode
			)
			else 
			(
				-- Otherwise, collect this node:
				contDataNodes[contNum] = dataNode
			)
		)
		
		-- Delete unwanted datanodes:
		if (delNodes.count != 0) do 
		(
			delete delNodes
		)
		
		-- Exit function if nothing needs to be stored:
		if (objConts.count == 0) do return false
		
		-- Add new data-nodes for containers that haven't got one:
		for contNum = 1 to objConts.count where (contDataNodes[contNum] == undefined) do 
		(
			local newDataNode = RsGenericDataNode dataClass:"CollisionConvert" name:(uniqueName "_CollisionConvert_") isHidden:true isFrozen:true
			contDataNodes[contNum] = newDataNode
			
			-- Add datanode to default layer:
			(LayerManager.GetLayer 0).addNode newDataNode
			
			-- Add data-node to specified container, if it's not storing for the null container (ie for non-containerised objects)
			local contNode = objConts[contNum]
			if (contNode != undefined) do 
			(
				newDataNode.pos = copy contNode.pos
				contNode.addNodeToContent newDataNode
				
				newDataNode.name += contNode.name
			)
			
			-- Move datanode down a bit
			newDataNode.pos -= [0,0,-10]
		)
		
		pushPrompt ("Storing Collision Converter data")
		
		-- Save conversion-data to each container's data-node:
		for contNum = 1 to objConts.count do 
		(
			local dataList = objDataPerCont[contNum]
			local dataNode = contDataNodes[contNum]
			
			-- This list will be used to collect object-nodes:
			local newObjsList = #()
			
			-- Create xml-writer, which we'll use to generate an xml-string:
			local stringBuilder = dotNetObject "System.Text.StringBuilder"
			local stringWriter = dotNetObject "System.IO.StringWriter" stringBuilder
			local xmlWriter = dotNetObject "System.Xml.XmlTextWriter" stringWriter
			xmlWriter.Formatting = xmlWriter.Formatting.Indented
			xmlWriter.Namespaces = False
			
			-- Create root-node:
			xmlWriter.WriteStartElement "root"
			
			-- Store map's slod-groups:
			for item in dataList do 
			(
				item.saveObjToXml xmlWriter objsList:newObjsList
			)
			
			-- Close root-node:
			xmlWriter.WriteEndElement()
			
			-- Finalise xml-writing:
			xmlWriter.close()
			stringWriter.close()
			
			-- Store data to datanode's paramblock:
			dataNode.setNodeList newObjsList
			dataNode.dataString = stringBuilder.ToString()
		)
		
		popPrompt()
		
		return contDataNodes
	),
	
	fn loadFromDataNode dataNode = 
	(
		if RsCollConvertDebug do 
			(format "  RsCollConvert.loadFromDataNode %\n" (dataNode as string))
		
		local dataList = #()
		
		if (not isValidNode dataNode) or (dataNode.dataString == undefined) or (dataNode.dataString == "") do 
		(
			return #()
		)
		
		local dataList = #()
		
		-- Read xml-data from groupsXML string:
		local textReader = dotNetObject "System.IO.StringReader" dataNode.dataString
		local xmlReader = dotNetObject "System.Xml.XmlTextReader" textReader
		
		local parentName = maxfilename
		if (dataNode.parent != undefined) do 
		(
			parentName = dataNode.parent.name
		)
		
		pushPrompt ("Parsing saved Collision Converter data: " + parentName)
		
		-- Reconstitute group-data from xml:
		xmlReader.Read()
local timeStart = timeStamp()
		if (xmlReader.NodeType == xmlReader.NodeType.Element) do 
		(
			-- Read first element:
			xmlReader.Read()
			
			local objsList = dataNode.getNodeList()
			
			while (xmlReader.NodeType != xmlReader.NodeType.EndElement) do 
			(
				case xmlReader.name of 
				(
					"collData":
					(
						local newItem = RsCollConvert.loadObjFromXml xmlReader objsList:(objsList)
						
						if (isKindOf newItem RsCollConvert) do 
						(
							append dataList newItem
						)
					)
				)
				
				-- Read next element:
				xmlReader.Read()
			)
		)
		
		xmlReader.close()
		textReader.close()
(format "Time taken to revert geom to coll: %s\n" ((timestamp() - timeStart) / 1000.0))
		popPrompt()
		
		return dataList
	),
	
	-- Load combined data-list from all CollisionConvert datanodes in scene:
	fn loadFromDataNodes = 
	(
		if RsCollConvertDebug do 
			(format "  RsCollConvert.loadFromDataNodes()\n")
		
		local dataList = #()
		
		for dataNode in helpers where (isKindOf dataNode RsGenericDataNode) and (dataNode.dataClass == "CollisionConvert")  do 
		(
			local nodeDataList = RsCollConvert.loadFromDataNode dataNode
			join dataList nodeDataList
		)
		
		return dataList
	),
	
	fn alignForCylinder longCyl:True = 
	(
		local cylLength, cylRadius, cylAxis, radAxis
		for n = 1 to 3 do 
		(
			if (cylLength == undefined) or (longCyl and (size[n] > cylLength)) or (not longCyl and (size[n] < cylLength)) do 
			(
				cylAxis = n
				cylLength = size[n]

				cylRadius = 0				
				for m = 1 to 3 where (m != n) do 
				(
					if size[m] > cylRadius do 
					(
						cylRadius = size[m]
						radAxis = m
					)
				)
			)
		)

		local cylDir = [0,0,0]
		cylDir[4 - cylAxis] = 1
		local rotMatrix = matrixFromNormal cylDir
		preRotate transform rotMatrix

		size = [cylLength, cylRadius, cylRadius]
		
		return This
	),
	
	-- Extract pertinent data from obj and save to current struct:
	fn getObjAttrs getColAttrs:True = 
	(
		--if RsCollConvertDebug do (format "  RsCollConvert.getObjAttrs (%)\n" getColAttrs)

		-- Store collision-attributes:
		if (GetAttrClass obj != "Gta Collision") then 
		(
			objColour = obj.wireColor
		)
		else 
		(
			collUseClr = CollisionVolumes.GetUseWireframeColour obj
			objColour = CollisionVolumes.GetNodeColour obj
			
			if getColAttrs do 
			(
				-- Store "User Properties" buffer:
				userProps = GetUserPropBuffer obj
				
				local attrCount = (GetNumAttr "Gta Collision")
				attrsUsed.count = attrCount
				attrVals.count = attrCount
				
				-- Store values for each non-default attribute:
				for attrIdx = 1 to attrCount do
				(
					local defVal = GetAttrDefault "Gta Collision" attrIdx
					local objVal = GetAttr obj attrIdx
					
					-- Only store non-default values:
					if (defVal != objVal) do 
					(
						attrsUsed[attrIdx] = True
						attrVals[attrIdx] = objVal
					)
				)
			)
		)
		
		-- Make sure object-colour uses nice clamped integers:
		objColour = objColour as point3
		for n = 1 to 3 do 
		(
			case of 
			(
				(objColour[n] < 0):(objColour[n] = 0)
				(objColour[n] > 255):(objColour[n] = 255)
			)
		)
		objColour = objColour as color

		OK
	),
	
	-- Get description-data for a given object:
	fn init inObj getColAttrs:True = 
	(
		if RsCollConvertDebug do (format "  RsCollConvert.init getColAttrs:% (%)\n" getColAttrs(inObj.name))
		
		obj = inObj
		objName = (obj.name)
		objClass = (classOf obj)
		transform = (copy obj.transform)
		
		-- Store relevant attributes:
		getObjAttrs getColAttrs:getColAttrs
		
		case (classOf obj) of 
		(
			Col_Box:
			(
				primType = #Box
				size = [obj.width, obj.length, obj.height]
			)
			Box:
			(
				primType = #Box
				size = 0.5 * [obj.length, obj.width, obj.height]
				preTranslate transform [0, 0, size.z]	
			)
			Col_Cylinder:
			(
				primType = #Cylinder
				cylinderType = True
				size = [obj.length * 0.5, obj.radius, obj.radius]
			)
			Cylinder:
			(
				primType = #Cylinder
				cylinderType = True
				size = [obj.height * 0.5, obj.radius, obj.radius]
				preRotateX transform 90
				preTranslate transform [0, (0.5 * obj.height), 0]
			)
			Capsule:
			(
				primType = #Capsule
				cylinderType = True
				size = [obj.height * 0.5, obj.radius, obj.radius]
				
				local capOffset = 0
				if (obj.heightType == 1) do 
				(
					capOffset = (obj.radius)
					size.x += capOffset
				)
				
				preRotateX transform 90
				preTranslate transform [0, (0.5 * obj.height) + capOffset, 0]
			)
			Col_Capsule:
			(
				primType = #Capsule
				cylinderType = True
				size = [obj.length * 0.5, 0, 0]
				size += obj.radius
			)
			Sphere:
			(
				primType = #Sphere
				size = [0,0,0]
				size += obj.radius
			)
			Col_Sphere:
			(
				primType = #Sphere
				size = [0,0,0]
				size += obj.radius
			)
			Default:
			(
				retVal = undefined
			)
		)
		
		OK
	),
	
	-- Returns a primitive-description struct for a particular object:
	fn getData obj getColAttrs:True = 
	(
		local retVal = RsCollConvert()
		retVal.init obj getColAttrs:getColAttrs
	
		return retVal
	),
	
	-- Which collision-class should a given geometry-primitive class be converted to?
	fn collSwapClass inClass = 
	(
		case inClass of 
		(
			Box:Col_Box
			Cylinder:Col_Cylinder
			Capsule:Col_Capsule
			Sphere:Col_Sphere
			Editable_Poly:Col_Mesh
			Editable_Mesh:Col_Mesh
			PolyMeshObject:Col_Mesh
			Default:undefined
		)
	),
	
	-- Which geometry-class should a given collision-primitive class be converted to?
	fn geomSwapClass inClass = 
	(
		case inClass of 
		(
			Col_Box:Box
			Col_Cylinder:Cylinder
			Col_Capsule:Capsule
			Col_Sphere:Sphere
			Col_Mesh:Editable_Mesh
			Default:undefined
		)
	),
	
	-- Returns True if inClass is a compatible primitive-class:
	fn isPrimClass inClass = 
	(
		(findItem #(Box, Col_Box,Cylinder, Col_Cylinder, Capsule, Col_Capsule, Sphere, Col_Sphere) inClass) != 0
	),
	
	-- Convert an object (geometry or collision) to a given class:
	fn convertTo obj convClass deleteOld:True restoreConvData: returnConvData:False delObjs: tempConvert:False = 
	(
		if RsCollConvertDebug do 
			(format "  RsCollConvert.convertTo (% to %)\n" (obj.name) (convClass as string))
		
		if (subObjectLevel != 0) do (subObjectLevel = 0)
		
		-- Pass back undefined if converting to same class:
		if (isKindOf obj convClass) do return undefined
		
		-- True if converting between collision-types:
		local colToCol = (isKindOf obj Helper) and (isKindOf convClass Helper)
		
		-- Store object-data: 
		-- (only bother getting collision-attributes if storing them, or if converting between collision-types)
		local convData = RsCollConvert.getData obj getColAttrs:(returnConvData or colToCol)
		local fromPrim = (convData.primType != undefined)
		local toPrim = (RsCollConvert.isPrimClass convClass)

		-- Collision-meshes need to be converted to editable-meshes for any conversions:
		local geomObj = obj
		local objName = obj.name
		
		if (isKindOf obj Col_Mesh) do 
		(
			-- Make copy if geometry isn't being replaced:
			if (not deleteOld) do 
			(
				geomObj = copy obj
			)
			
			col2mesh geomObj
			
			if RsCollConvertDebug do 
				(format "Converted ColMesh to mesh\n")
		)
		
		-- Return undefined if object isn't compatible with conversion
		-- (non primitive, or non-meshy)
		if (not fromPrim) and (not isProperty geomObj #mesh) do 
		(
			if RsCollConvertDebug do 
				(format "Not converting incompatible object: %\n" geomObj.name)

			return undefined
		)
		
		-- Convert prims to mesh if required:
		if (fromPrim) and not (toPrim) do 
		(
			undo off 
			(
				-- Convert collision-prims to geometry:
				if (isKindOf obj Helper) do 
				(
					local geomPrimClass = RsCollConvert.geomSwapClass convData.objClass
					geomObj = RsCollConvert.convertTo geomObj geomPrimClass deleteOld:(geomObj != obj) tempConvert:True
					
					if RsCollConvertDebug do 
						(format "Converted collision-prim to geom: %\n" geomObj.name)
				)
				
				-- Collapse geom-primitive to mesh:
				convertToPoly geomObj
				
				if RsCollConvertDebug do 
					(format "Primitive converted to mesh: %\n" geomObj.name)
			)
		)

		-- Convert generic convData to different object-classes:
		local newObj
		
		-- If converting to non-primitive, set up geomObj:
		if (not toPrim) do 
		(
			-- Make geomObj a copy, if it is currently original obj:
			if (not deleteOld) and (geomObj == obj) do 
			(
				if RsCollConvertDebug do 
					(format "Making object-copy: %\n" geomObj.name)
				
				geomObj = copy obj
			)
			
			newObj = geomObj
		)
		
		-- Create new primitive-object:
		if (isKindOf convClass Helper) then 
		(
			case convClass of 
			(
				Col_Mesh:
				(
					mesh2col geomObj
					newObj = geomObj
				)
				Col_Box:
				(
					if fromPrim then 
					(
						newObj = col_box transform:convData.transform width:convData.size.x length:convData.size.y height:convData.size.z
					)
					else 
					(
						newObj = (RsCreateCollisionGeometry objs:#(geomObj) collClass:#box rotXYZ:True createAsGeometry:False)[1]
					)
				)
				Col_Cylinder:
				(
					if fromPrim then 
					(
						if (not convData.cylinderType) do (convData.alignForCylinder())
						newObj = Col_Cylinder transform:convData.Transform length:(convData.Size.X * 2) radius:convData.Size.Y
					)
					else 
					(
						newObj = (RsCreateCollisionGeometry objs:#(geomObj) collClass:#cylinder createAsGeometry:False)[1]
					)
				)
				Col_Capsule:
				(
					if fromPrim then 
					(
						if (not convData.cylinderType) do (convData.alignForCylinder())
						newObj = Col_Capsule transform:convData.Transform length:((convData.Size.X - convData.Size.Y) * 2) radius:convData.Size.Y
					)
					else 
					(
						newObj = (RsCreateCollisionGeometry objs:#(geomObj) collClass:#capsule createAsGeometry:False)[1]
					)
				)			
				Col_Sphere:
				(
					if fromPrim then 
					(
						if (not convData.cylinderType) do (convData.alignForCylinder())
						newObj = col_sphere transform:convData.Transform radius:convData.Size.Y
					)
					else 
					(
						-- Convert via box, to get inset sphere:
						local tempObj = (RsCreateCollisionGeometry objs:#(geomObj) collClass:#box createAsGeometry:True)[1]
						newObj = RsCollConvert.convertTo tempObj Sphere deleteOld:True tempConvert:True
					)
				)
			)
		)
		else 
		(
			case convClass of 
			(
				Box:
				(
					if fromPrim then 
					(
						preTranslate convData.transform [0, 0, -convData.size.z]
						local boxSize = 2* [convData.size.y, convData.size.x, convData.size.z]
						newObj = box transform:convData.transform width:boxSize.x length:boxSize.y height:boxSize.z
					)
					else 
					(
						newObj = (RsCreateCollisionGeometry objs:#(geomObj) collClass:#box rotXYZ:True createAsGeometry:True)[1]
					)
				)
				Cylinder:
				(
					if fromPrim then 
					(
						if (not convData.cylinderType) do (convData.alignForCylinder())
						preRotateX convData.transform 90
						preTranslate convData.transform [0, 0, -convData.Size.X]
						newObj = Cylinder transform:convData.transform height:(convData.Size.X * 2) radius:convData.Size.Y
					)
					else 
					(
						newObj = (RsCreateCollisionGeometry objs:#(geomObj) collClass:#cylinder createAsGeometry:True)[1]
					)
				)
				Capsule:
				(
					if fromPrim then 
					(
						if (not convData.cylinderType) do (convData.alignForCylinder())
						preRotateX convData.transform 90
						preTranslate convData.transform [0, 0, -convData.Size.X]
						newObj = Capsule transform:convData.Transform height:(convData.Size.X * 2) radius:convData.Size.Y heightType:0
					)
					else 
					(
						newObj = (RsCreateCollisionGeometry objs:#(geomObj) collClass:#capsule createAsGeometry:True)[1]
					)
				)			
				Sphere:
				(
					if fromPrim then 
					(
						if (not convData.cylinderType) do (convData.alignForCylinder())
						newObj = sphere transform:convData.Transform radius:convData.Size.Y
					)
					else 
					(
						-- Convert via box, to get inset sphere:
						local tempObj = (RsCreateCollisionGeometry objs:#(geomObj) collClass:#box createAsGeometry:True)[1]
						newObj = RsCollConvert.convertTo tempObj Sphere deleteOld:True tempConvert:True
					)
				)
			)
		)
		
		-- Remove temp geomObj:
		undo off 
		(
			if (geomObj != obj) and (geomObj != newObj) do 
			(
				delete geomObj
				if RsCollConvertDebug do (format "Removed temp geom-object\n")
			)
		)

		-- Return False if newObj is undefined - this should be due to cancelling geometry-finder function:
		if (newObj == undefined) do (return False)
		
		-- Rename new object:
		newObj.name = case of 
		(
			tempConvert:
			(
				uniqueName (objName + "_TEMPCONVERT")
			)
			(restoreConvData == unsupplied):
			(
				-- Add suffix if converting from original:
				uniqueName (objName + "_CONVERTED")
			)
			Default:
			(
				-- Restore original name:
				restoreConvData.objName
			)
		)

		if (newObj != obj) do 
		(
			-- Put new object in same layer/container as original:
			obj.Layer.addNode newObj
			
			-- Add to same container:
			local objCont = Containers.IsInContainer obj
			if ((Containers.IsInContainer newObj) != objCont) do 
			(
				objCont.addNodeToContent newObj
			)
			
			-- Give it the same parent:
			newObj.parent = obj.parent
		)
		
		-- Reapply stored attributes, if supplied:
		if (GetAttrClass newObj != "Gta Collision") then 
		(
			-- Converting to geometry...
			if (convData.objColour != undefined) do 
			(
				newObj.wireColor = convData.objColour
			)
			
			-- Create collision-material on temp-converted geometry, to show colours:
			if returnConvData and (GetAttrClass obj == "Gta Collision") do 
			(
				local collName = getAttr obj RsIdxCollType
				local collMat = RexBoundMtl name:("TempMat_" + obj.name)
				RexSetCollisionName collMat collName
				newObj.material = collMat
			)
		)
		else 
		(
			-- Apply collision-specific attributes:
			
			if (convData.objColour != undefined) do 
			(
				newObj.wireColor = convData.objColour
				CollisionVolumes.SetNodeColour newObj convData.objColour
				CollisionVolumes.SetUseWireframeColour newObj convData.collUseClr
			)
			
			-- Apply original-object's data if converting back to collision:
			local applyData = if (restoreConvData != unsupplied) then restoreConvData else convData
			
			if (applyData.attrsUsed != undefined) do 
			(
				-- Replace new object's "User Properties" buffer:
				setUserPropBuffer newObj applyData.userProps

				-- Restore values for each non-default attribute:
				for attrIdx = applyData.attrsUsed do
				(
					setAttr newObj attrIdx applyData.attrVals[attrIdx]
				)
			)
			
			-- Transfer collision-name from material to new object, if found:
			if (isKindOf obj.material RexBoundMtl) do 
			(
				local collName = RexGetCollisionName obj.material
				setAttr newObj RsIdxCollType collName
			)
		)
		
		-- Delete original object:
		if deleteOld and (newObj != obj) and (not isDeleted obj) do 
		(
			-- If delObjs is used, external function will delete objects in a oner:
			if (isKindOf delObjs Array) then 
			(
				append delObjs obj
			)
			else 
			(
				delete obj
			)
		)
		
		if returnConvData then 
		(
			convData.obj = newObj
			return convData
		)
		else 
		(
			return newObj
		)
	)
)
