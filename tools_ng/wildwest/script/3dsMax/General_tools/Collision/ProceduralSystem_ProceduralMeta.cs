using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;

namespace ProceduralSystem
{
    [XmlRoot("CProceduralInfo")]
    public class Metadata
    {
        [XmlArray("procObjInfos")]
        [XmlArrayItem("Item")]
        public List<ProceduralObject> ProceduralObjectList = new List<ProceduralObject>();
        public ProceduralObject[] ProceduralObjectArray
        {
            get
            {
                return ProceduralObjectList.ToArray();
            }
        }

        [XmlArray("plantInfos")]
        [XmlArrayItem("Item")]
        public List<ProceduralPlant> ProceduralPlantList = new List<ProceduralPlant>();
        public ProceduralPlant[] ProceduralPlantArray
        {
            get
            {
                return ProceduralPlantList.ToArray();
            }
        }

        [XmlArray("procTagTable")]
        [XmlArrayItem("Item")]
        public List<ProceduralTagIndex> ProceduralTagIndexList = new List<ProceduralTagIndex>();
        public ProceduralTagIndex[] ProceduralTagIndexArray
        {
            get
            {
                return ProceduralTagIndexList.ToArray();
            }
        }

        //Collects all the procedural tags (both plants and objects) exposed as an array for use in MXS
        public List<String> ProceduralTags { get; set; }
        public String[] ProceduralTagArray
        {
            get
            {
                return ProceduralTags.ToArray();
            }
        }

        public Metadata()
        {
            ProceduralTags = new List<String>();
        }

        public static Metadata Read(String InputFilePath)
        {
            XmlSerializer Serializer = new XmlSerializer(typeof(Metadata));
            var WriterSettings = new XmlWriterSettings { Indent = true };

            Metadata ReturnMetadataInstance; 
            using (var Reader = XmlReader.Create(InputFilePath))
            {
                ReturnMetadataInstance = (Metadata)Serializer.Deserialize(Reader);
            }

            ReturnMetadataInstance.ProcessProceduralTags();
            return ReturnMetadataInstance;
        }

        //Adds all the procedural tags in uppercase format
        void ProcessProceduralTags()
        {
            //foreach (var ProcObject in ProceduralObjectList)
            //{
            //    ProceduralTags.Add(ProcObject.Tag);
            //}

            //foreach (var ProcPlant in ProceduralPlantList)
            //{
            //    ProceduralTags.Add(ProcPlant.Tag);
            //}

            foreach (var ProcTag in ProceduralTagIndexList)
            {
                if (!String.IsNullOrEmpty(ProcTag.Name) && ProcTag.Name != "null" )
                {
                ProceduralTags.Add(ProcTag.Name);
                }
            }

            ProceduralTags = ProceduralTags.Distinct().ToList();
            ProceduralTags.Sort();
        }

        public Int32 GetProceduralIndex(String InputProceduralTag)
        {
            foreach (var SubProceduralTagIndex in ProceduralTagIndexList)
            {
                if( SubProceduralTagIndex.Name == InputProceduralTag )
                {
                    return ProceduralTagIndexList.IndexOf(SubProceduralTagIndex);
                }
            }

            return -1;
        }

        public Boolean HasPlantData ( Int32 InputProceduralIndex )
        {
            if (InputProceduralIndex < ProceduralTagIndexList.Count)
            {
                if ( String.IsNullOrEmpty(ProceduralTagIndexList[InputProceduralIndex].ProceduralPlantTag) )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        public class ProceduralPlant
        {
            [XmlElement("Tag")]
            public String Tag { get; set; }
        }

        public class ProceduralObject
        {
            [XmlElement("Tag")]
            public String Tag { get; set; }
        }

        public class ProceduralTagIndex
        {
            [XmlElement("name")]
            public String Name { get; set; }
            [XmlElement("procObjTag")]
            public String ProceduralObjectTag { get; set; }
            [XmlElement("plantTag")]
            public String ProceduralPlantTag { get; set; }
        }
    }
}
