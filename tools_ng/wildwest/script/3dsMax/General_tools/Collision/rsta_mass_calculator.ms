filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
rsta_loadCommonFunction #("FN_RSTA_userSettings")

if (MassCalculator != undefined) do MassCalculator.dispose()
struct MassCalculator
(
	-- LOCALS ------------------------------------------------
	MainWindow, 
	Settings = rsta_userSettings app:"MassCalculator",	
	CurrentObject,
	CurrentCollisionObjs = #(),
	datFile = RsConfigGetBuildDir() + "common\data\materials\materials.dat",
	density_array = #(),
	---------------------------------------------------------------------
	-- FUNCTIONS 
	---------------------------------------------------------------------
	fn read_dat_file attr_idx = 
	(
		ss = openFile datFile
		colMat_array = #()
		
		while not(eof ss) do 
		(
			local line = readLine ss
			if (line[1] != "#") do
			(
				local colMat = FilterString line " \t"
				if (colMat.count == 23) do 
				(
					append colMat_array (datapair name:colMat[1] denstity:(colMat[attr_idx] as float))
				)			
			)
		)
		close ss
		return colMat_array
	),
	---------------------------------------------------------------------
	fn get_object = 
	(
		if classof_array #(Editable_Poly, Editable_Mesh, PolyMeshObject) selection[1] do
		(
			-- IS IT A FRAG? 
			local is_frag = false
			if (MatchPattern selection[1].name pattern:"*_frag_") do is_frag = true
			if ((getNodeByName (selection[1].name +"_frag_")) != undefined) do  is_frag = true
			if (is_frag) do 
			(
				messageBox "This is frag object, please use the frag debuger to tune this."
				return false
			)
			
			-- GET UP OBJECTS
			MainWindow.vm.CollObjects.Clear()
			CurrentObject = selection[1]
			CurrentCollisionObjs = for child_col in CurrentObject.children where GetAttrClass child_col == "Gta Collision" collect child_col
			return true
		)
		return false
	),
	---------------------------------------------------------------------
	fn get_type col_obj =
	(
		case (classof col_obj) of
		(
			Col_Box : return "Bx"
			Col_Mesh : return "Me"
			Col_Cylinder : return "Cy"
			Col_Capsule : return "Ca"
			Col_Sphere : return "Sp"
			default : return "?"
		)
	),
	---------------------------------------------------------------------
	fn isRexBound col_obj =
	(
		if (classof col_obj == Col_Mesh) then 	
		(
			if (classof col_obj.mat == RexBoundMtl) do return true								
			if (classof col_obj.mat == Multimaterial) do 
			(
				if (classof col_obj.mat.materialList[1] == RexBoundMtl) do return true	
		
			)
		)
		return false
	),
	---------------------------------------------------------------------
	fn get_surface_type col_obj =
	(
		if (classof col_obj == Col_Mesh) then 	
		(
			if (classof col_obj.mat == RexBoundMtl) do
			(
				RexGetCollisionName col_obj.mat
			)
			if (classof col_obj.mat == Multimaterial) do 
			(
				return (RexGetCollisionName col_obj.mat.materialList[1])
			)
		)
		return getattr col_obj RsIdxCollType
	),
	---------------------------------------------------------------------
	fn get_surface_idx surface_type= 
	(
		for i=1 to (density_array.count) do
		(
			if (density_array[i].Name == surface_type) do return (i-1)
		)	
		return 0		
	),
	---------------------------------------------------------------------
	fn get_density surfaceType = 
	(
		for i in density_array where i.name == toUpper(surfaceType) do return i.denstity
		return 0
	),
	---------------------------------------------------------------------
	fn getVol_sphere r = 
	(
		return (pi*(r*r*r))*1.33333
	),
	---------------------------------------------------------------------
	fn getVol_cylinder r h =
	(
		return pi*(r*r)*h
	),
	---------------------------------------------------------------------
	fn getVol_box w h d =
	(
		return w*h*d
	),
	---------------------------------------------------------------------
	fn getVol_mesh obj =
	(
		col2mesh obj
		local volume	= 0.0
		local objMesh	= snapshotAsMesh obj
		for f = 1 to objMesh.numFaces do
		(
			v = getFace objMesh f
			v1 = getVert objMesh v.x
			v2 = getVert objMesh v.y
			v3 = getVert objMesh v.z
			volume += ((v2.y-v1.y)*(v3.z-v1.z)-(v2.z-v1.z)*(v3.y-v1.y))*(v1.x+v2.x+v3.x)
		)
		delete objMesh
		mesh2col obj
		return volume / 6.0
	),
	---------------------------------------------------------------------
	fn get_volume col_obj = 
	(
		case (classof col_obj) of
		(
			Col_Box : return ((getVol_box col_obj.height col_obj.width col_obj.length) * 8)
			Col_Cylinder : return (getVol_cylinder col_obj.radius col_obj.length)
			Col_Sphere : return (getVol_sphere col_obj.radius)
			Col_Capsule : return ((getVol_sphere col_obj.radius) + (getVol_cylinder col_obj.radius col_obj.length))
			Col_Mesh : return ((getVol_mesh col_obj))	
		)
	),
	---------------------------------------------------------------------
	fn get_mass col_obj surface_type = 
	(
		local denstity = get_density surface_type
		local volume = get_volume col_obj 
		return (volume * denstity)		
	),
	---------------------------------------------------------------------
	fn load_col_objects =
	(
		if (get_object()) do
		(			
			for col_obj in CurrentCollisionObjs do
			(
				local surface_type = get_surface_type col_obj
				local surface_idx = get_surface_idx surface_type
				local mass = get_mass col_obj surface_type
				-- IS IT REXBOUND?
				if (isRexBound col_obj) do surface_idx = -1				
				MainWindow.vm.add_colObj (get_type col_obj) col_obj.name surface_idx mass
			)			
		)
	),
	---------------------------------------------------------------------
	fn set_selection = 
	(
		Local sel_name = MainWindow.vm.CollObjects.Item[MainWindow.vm.SelectedIdx]
		try (select (getNodeByName sel_name.Name))catch()
		redrawViews()
	),
	---------------------------------------------------------------------
	fn SurfaceChange obj_name surface_idx =
	(
		format "% %\n" obj_name surface_idx
		local col_obj = getNodeByName obj_name
		if (col_obj != undefined) do
		(		
			setattr col_obj RsIdxCollType density_array[surface_idx + 1].Name
			select CurrentObject 
			load_col_objects()
		)
	),
	---------------------------------------------------------------------
	---BASE 
	---------------------------------------------------------------------
	fn CmdMan arg =
	(
		case arg of 
		(
			#GetMass : load_col_objects()
			#SelectionChanged : set_selection()
		)
	),
	---------------------------------------------------------------------
	fn dispose = 
	(
		if (MainWindow != undefined) do 
		(
			-- WINDOW POS 
			settings.wpf_windowPos mainWindow #set
				
			MainWindow.Close()     
			MainWindow = undefined
		)
		execute ("MassCalculator = undefined")
	),
	-------------------------------------------------------------------
	fn init =
	(
		-- LOAD ASSEMBLY
		dotnet.loadAssembly (RsConfigGetToolsDir() + "techart/dcc/3dsMax/RSG.TechArt.MassCalculator.dll")
		MainWindow = dotNetObject "RSG.TechArt.MassCalculator.MainWindow"              
		dn_window.setup MainWindow 
		MainWindow.Tag = dotnetmxsvalue this
		MainWindow.ChangeOwner (DotNetObject "System.IntPtr" (Windows.GetMAXHWND()))
			
		-- WINDOW POS
		settings.wpf_windowPos mainWindow #get					
		MainWindow.show()
		
		-- GET DENSITY LIST 
		density_array = read_dat_file 9
		-- ADD TO UI			
		for i in density_array do MainWindow.vm.SurfaceTypes.Add i.Name
			
		-- RUN ON SELECTION
		load_col_objects()
	),
	-- EVENTS -----------------------------------------------------
	on create do init()            
)
MassCalculator = MassCalculator()