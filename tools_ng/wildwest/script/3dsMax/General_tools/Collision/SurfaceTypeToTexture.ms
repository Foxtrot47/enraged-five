FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_xml.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_userSettings.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Collision/Collision_FromTexture.ms")

struct st_colorSet 
(
	name,
	color,
	surfaceTypes = #()
)

struct st_outputData
(
	path, 
	name, 
	suffix, 
	ext
)

try (destroyDialog  SurfaceColorsWindow) catch()
rollout SurfaceColorsWindow "Surface Type To Texture" width:200
(
	------------------------------------------------------------------
	-- LOCALS
	------------------------------------------------------------------
	local Settings = rsta_userSettings app:"SurfaceTypeToTexture"
	local xmlPath 
	local xmlLoaded = false
	local xml_io = rsta_xml_io()
	local colourSets = #()
	local hasChanged = false
	local missingColourSets = #()	
	local outPutData = #()
	
	------------------------------------------------------------------
	-- FUNCTIONS 
	------------------------------------------------------------------
	fn sortColourSets v1 v2 =
	(
		return stricmp v1.name v2.name
	)
	------------------------------------------------------------------
	fn getOutputPaths =
	(
		outPutData = #((datapair menuname:"Max Image" output:(datapair folder:(getDir #image + "\\") suffix:"" )))
			
		local pathXML = (RsConfigGetWildWestDir() + @"etc\config\maps\SurfaceTypeToTexture.xml")
		if (doesFileExist pathXML) do 
		(
			local pathXml_io = rsta_xml_io xmlFile:pathXML
			for output_xn in (rsta_xml.getChildren pathXml_io.root name:"OutputPaths/Output") do 
			(
				local dp = datapair menuname:(output_xn.getAttribute "menuName") output:(datapair folder:(output_xn.getAttribute "path") suffix:(output_xn.getAttribute "nameSuffix"))
				append outPutData dp
			)
		)
	)
	------------------------------------------------------------------
	fn getFolderPathAndName obj = 
	(
		local oput = outPutData[SurfaceColorsWindow.ddl_outputFolder.selection]
		local folderPath = substituteString oput.output.folder "%ProjRootDir%" (RsConfigGetProjRootDir())
		
		local nameArray = FilterString obj.name "_"	
		local containerName = if (nameArray.count >= 2) then (nameArray[1] + "_" + nameArray[2]) else obj.name
		folderPath = RsMakeSafeSlashes (substituteString folderPath "%TileName%"  containerName)		
		
		makedir folderPath
			
		o = st_outputData()
		o.path = folderPath
		o.name = containerName
		o.suffix = oput.output.suffix
		o.ext = ".tif"		
			
		return o 
	)
	------------------------------------------------------------------
	fn mergeImage image_array outpath = 
	(	
		local imageClass = dotNetClass "System.Drawing.Image"

		-- IMAGE SOURCE
		local imageSourceArray = #()
		for i in image_array do if (doesFileExist i) do append imageSourceArray (imageClass.FromFile i)
		
		-- IMAGE DIMENTIONS 
		local w = imageSourceArray[1].Width
		local h = imageSourceArray[1].Height

		-- MERGE IMAGE	
		local target = dotNetObject "System.Drawing.Bitmap" w h (dotNetClass "System.Drawing.Imaging.PixelFormat").Format32bppArgb
		local graphics = (dotNetClass "System.Drawing.Graphics").FromImage(target);
		
		for i in imageSourceArray do graphics.DrawImage i 0 0

		-- DISPOSE
		for i in imageSourceArray do i.Dispose()
	
		target.Save outpath (dotNetClass "System.Drawing.Imaging.ImageFormat").tiff
		target.Dispose()
		graphics.Dispose()
	)
	------------------------------------------------------------------
	fn validSetting settingName = 
	(
		return  (settings.getValue settingName != undefined)
	)
	------------------------------------------------------------------
	fn getCurrentColourSet = 
	(
		local value = SurfaceColorsWindow.ddl_colourSets.selection
		if value == 0 do 
		(
			value = 1
			SurfaceColorsWindow.ddl_colourSets.selection = 1
		)
		return colourSets[value]
	)
	---------------------------------------------------------------------
	fn set_pickerColour =
	(
		if (ColourSets.count != 0) do SurfaceColorsWindow.clp_colour.color = (getCurrentColourSet()).color
	)
	--------------------------------------------------------------------- 
	fn set_currentSetSurfaceTypes = 
	(
		if (ColourSets.count != 0) then SurfaceColorsWindow.lbx_surfaceTypes.items = for i in (getCurrentColourSet()).surfaceTypes collect i
		else SurfaceColorsWindow.lbx_surfaceTypes.items = #()
	)
	---------------------------------------------------------------------
	fn set_ColourSetDropDown =
	(
		SurfaceColorsWindow.ddl_colourSets.items = for i in colourSets collect i.name
		set_pickerColour()
		set_currentSetSurfaceTypes()
	)
	---------------------------------------------------------------------
	fn string_color input arg = 
	(
		if arg == #get do
		(
			local fs = filterstring input " "
			return (color (fs[1] as float) (fs[2] as float) (fs[3] as float))
		)
		if arg == #set do return (input.r as string + " " + input.g as string + " " + input.b as string )
	)
	---------------------------------------------------------------------
	fn set_surfaceTypesLeft =
	(
		local allSurfaceTypes = for i in (RsCollMatDef.loadCollisionEntries()) where not (MatchPattern i.name pattern:"UNUSED_*") collect i.name
		sort allSurfaceTypes
		local usedSurfaceTypes = #()
		for i in colourSets do for j in i.surfaceTypes do append usedSurfaceTypes j
			
		SurfaceColorsWindow.ddl_SurfaceTypes.items = for i in allSurfaceTypes where ((finditem usedSurfaceTypes i) == 0) collect i
	)
	---------------------------------------------------------------------
	fn updateUI = 
	(
		qsort ColourSets sortColourSets
		set_ColourSetDropDown()
		set_pickerColour()
		set_currentSetSurfaceTypes()
		set_surfaceTypesLeft()
		if (xmlPath != undefined) do SurfaceColorsWindow.edt_xmlFile.text = getFilenameFile xmlPath
	)
	---------------------------------------------------------------------
	fn saveChanges file:xmlPath = 
	(
		gRsPerforce.edit #(file)
		
		xml_io = rsta_xml_io()
		xml_io.new "SurfaceTypeColorSet"
		
		for cs in colourSets do 
		(
			local cs_xn = rsta_xml.makeNode "ColorSet" xml_io.root attr:"name" value:cs.name		
			cs_xn.setAttribute "color" (string_color cs.color #set)		
			
			for st in cs.surfaceTypes do rsta_xml.makeNode "SurfaceType" cs_xn attr:"name" value:st				
		)
		
		xml_io.saveas file
		hasChanged = false
	)
	------------------------------------------------------------------
	fn loadXML = 
	(
		colourSets =#()		
		gRsPerforce.sync #(xmlPath)	
		
		if (doesFileExist xmlPath) then
		(
			xml_io.xmlFile = xmlPath		
			xml_io.load()		
			
			-- GET COLOR SETS 
			for cs in (rsta_xml.getChildren xml_io.root name:"ColorSet") do 
			(
				cSet = st_colorSet name:(cs.getAttribute "name") color:(string_color (cs.getAttribute "color") #get)					
				for st in (rsta_xml.getChildren cs name:"SurfaceType") do  append cSet.surfaceTypes (st.getattribute "name")				
				append colourSets cSet
			)
			
			set_ColourSetDropDown()							
			xmlLoaded = true
			updateUI()
		)
		else xmlPath = undefined	
	)
	------------------------------------------------------------------
	fn getColour texType = 
	(
		for cs in colourSets do if ((findItem cs.surfaceTypes texType) != 0) do return cs.color		
		appendifunique missingColourSets texType
		return  color 0 0 0 
	)
	------------------------------------------------------------------
	fn math_biggest a b arg =
	(	
		for i=1 to 3 do 
		(
			if (arg == #min) then if a[i]<b[i] do b[i] = a[i]
			if (arg == #max) then if a[i]>b[i] do b[i] = a[i]
		)
		return b
	)
	------------------------------------------------------------------
	fn mathMax a b = return ((a+b)+(abs(a-b)))/2
	------------------------------------------------------------------
	fn renderSelection =
	(
		local start = timeStamp()
		if (selection.count != 0) do
		(			
			local size = SurfaceColorsWindow.spn_renderSize.value
			local tempMeshs = #()
			missingColourSets = #()	
			
			-- SET UP SHADERS
			for obj in (getcurrentselection()) where (classof_array #(editable_mesh, editable_poly) obj) do 
			(				
				local tempMesh = copy obj
				convertToPoly tempMesh 
				local rexData = gRsTexToColl.GetObjColMatFaces tempMesh
				
				local debugMat = Multimaterial()
				debugMat.numsubs = rexData.ColMatNames.count
				
				for i=1 to debugMat.numsubs do 
				(		
					local tempMat  = debugMat.materialList[i] = Standardmaterial()
					tempMat.Diffuse = getColour (rexData.ColMatNames[i])
					tempMat.selfIllumAmount = 100							
				)
				
				tempMesh.mat = debugMat
				
				-- APPLY MAP IDS
				for i=1 to rexData.ColMatFaces.count do polyop.SetFaceMatId tempMesh rexData.ColMatFaces[i] i			
				
				append tempMeshs tempMesh
			)	

			local oPuts = getFolderPathAndName tempMeshs[1]		
			
			-- RENDER TOP DOWN 
			if (SurfaceColorsWindow.ddl_renderType.selection == 1) then
			(
				local currentVis = #()
				for g in geometry do append currentVis (datapair obj:g vis:g.ishidden)				
				hide $*
				unhide tempMeshs
				
				-- WORK OUT BOUNDS AND CAM POS				
				local target_bb_min = tempMeshs[1].center
				local target_bb_max = tempMeshs[1].center
					
				for obj in tempMeshs do 
				(
					target_bb_min = math_biggest target_bb_min obj.min #min 
					target_bb_max = math_biggest target_bb_max obj.max #max
				)	
				
				local target_size = target_bb_max - target_bb_min
				local target_center = target_size/2 + target_bb_min
				local target_biggest = mathMax target_size.x target_size.y
				
				-- SET UP CAMERA ----------------------------------------------
				cam = freecamera()
				cam.fov = 90
				cam.orthoProjection = true
				cam.baseObject.targetDistance = (target_biggest/2)
				cam.pos = target_center + [0,0,100]		
				
				-- RENDER --------------------------------------------------------
				render 	camera:cam vfb:false outputSize:[size,size] antiAliasing:false outputfile:(oPuts.path + oPuts.name + oPuts.suffix + oPuts.ext)				
				---------------------------------------
				delete cam
				
				for i in currentVis do i.obj.isHidden = i.vis
			)
			-- UV RENDER
			else 
			(	
				local outputFiles = #()						
		
				for obj in tempMeshs do 
				(
					obj.iNodeBakeProperties.removeAllBakeElements() 
					
					be1 = diffusemap() 
					be1.outputSzX = be1.outputSzY = size 
					be1.fileType = oPuts.path + obj.name + oPuts.suffix + ".png"
					be1.fileName = filenameFromPath be1.fileType
					be1.filterOn = be1.shadowsOn = be1.lightingOn = false 					
					be1.enabled = true
					
					append outputFiles be1.fileType
					
					obj.INodeBakeProperties.addBakeElement be1 
					obj.INodeBakeProperties.bakeEnabled = true 
					obj.INodeBakeProperties.bakeChannel = 1 
					obj.INodeBakeProperties.nDilations = 1 
				
					select obj 					
			
					render rendertype:#bakeSelected vfb:false progressBar:true outputSize:[size,size] antiAliasing:false 
				)
				
				if (SurfaceColorsWindow.cbx_mergeToOneTexture.checked) do 
				(					
					local outputName = oPuts.path + oPuts.name + oPuts.suffix + oPuts.ext	
					
					gRsPerforce.edit #(outputName)					
					
					if not ((doesFileExist outputName) AND (getFileAttribute outputName #readOnly)) then 
					(
						mergeImage outputFiles outputName
						-- ADD TO P4 IF NEW
						if (SurfaceColorsWindow.ddl_outputFolder.selection != 1) do gRsPerforce.add_or_edit #(outputName)
						
						-- DELETE OLD 
						for i in outputFiles do deleteFile i					
					)
					else messageBox "Looks like the file is locked, maybe someone else has it locked out in p4?" Title:"Hold up!"
				)	
			)
			
			shellLaunch "explorer.exe" (substituteString oPuts.path "/" "\\")
			
			for i in tempMeshs do delete i
				
			if (missingColourSets.count != 0) do 
			(
				local srt = "Missing colour sets on ... \n"
				for i in missingColourSets do srt += i + "\n"
				messageBox srt Title:"WARNING!"
			)
		)
		format "Surface Type To Texture Took : %\n" ((timeStamp() - start)/1000.0)
	)
	------------------------------------------------------------------
	-- CONTROLS 
	------------------------------------------------------------------
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:SurfaceColorsWindow.Width	
	local banner = makeRsBanner dn_Panel:rsBannerPanel versionNum:1.15 versionName:"Ultra Worm" wiki:"" filename:(getThisScriptFilename())
	
	group "XML File"
	(
		edittext edt_xmlFile "" width:180  offset:[-5,0] readOnly:true
		button btn_newXML "New" width:56 align:#left across:3 
		button btn_loadXML "Load" width:56 align:#center 
		button btn_saveXML "Save" width:56 align:#right  
	)
	
	group "Color Set"
	(
		colorpicker clp_colour "" width:24 align:#left across:2 offset:[-5,0] 
		dropdownlist ddl_colourSets "" width:150 align:#right
		
		edittext edt_colourSetName "" width:90 align:#left offset:[-5,0] across:4
		button btn_RenameColourSet "R" width:24 align:#right offset:[30,0] tooltip:"Rename selected colour set."
		button btn_addColourSet "+" width:24 align:#right offset:[15,0] tooltip:"Add new colour set."
		button btn_removeColourSet "-" width:24 align:#right tooltip:"Remove selected colour set."
	)
	
	group "Surface Types"
	(
		listbox lbx_surfaceTypes "" width:180 align:#left  offset:[-3,0] 
		
		dropdownlist ddl_SurfaceTypes "" width:120 align:#left across:3  offset:[-3,0] height:25	
		button btn_addSurface "+" width:24 align:#right   offset:[30,0] tooltip:"Add selected surface type to colour set."
		button btn_removeSurface "-" width:24 align:#right  tooltip:"Remove selected surface type to colour set."
	)	
	
	group "Render To Texture" 
	(
		dropdownlist ddl_renderType "" items:#("Top Down", "UV Space") width:80 align:#left across:2  offset:[-2,0]
		spinner spn_renderSize "Size : " fieldwidth:50 range:[32,4096,1024] align:#right  offset:[0,3] type:#integer
		checkbox cbx_mergeToOneTexture "Merge To One Texture"
		
		label lb_1 "Output Folder : " across:2 align:#left  offset:[0,3]
		dropdownlist ddl_outputFolder width:100 align:#right  offset:[2,0]
		button btn_render ">> Render Selected <<"  width:180 align:#left  offset:[-2,0]
	)
	
	------------------------------------------------------------------
	-- EVENTS 
	------------------------------------------------------------------
	on SurfaceColorsWindow open do 
	(
		settings.dialog_windowPos SurfaceColorsWindow #get
		banner.setup()	

		if (validSetting "XmlPath") do xmlPath = settings.getValue "XmlPath"	
		if (validSetting "RenderType") do SurfaceColorsWindow.ddl_renderType.selection = settings.getValue "RenderType"	
		if (validSetting "RenderSize") do SurfaceColorsWindow.spn_renderSize.value = settings.getValue "RenderSize"	
		if (validSetting "MergeTexture") do SurfaceColorsWindow.cbx_mergeToOneTexture.checked = settings.getValue "MergeTexture"
		if (validSetting "OutputFolder") do SurfaceColorsWindow.ddl_outputFolder.selection = settings.getValue "OutputFolder" 

		
		if (xmlPath != undefined ) do loadXML()
		getOutputPaths()
		ddl_outputFolder.items = for i in outPutData collect i.menuName		
	)
	------------------------------------------------------------------
	on SurfaceColorsWindow close do 
	(
		if (hasChanged) do saveChanges()
		settings.dialog_windowPos SurfaceColorsWindow #set
		
		if (xmlPath != undefined) do settings.setValue "XmlPath" xmlPath 
		settings.setValue "RenderType" SurfaceColorsWindow.ddl_renderType.selection
		settings.setValue "RenderSize" SurfaceColorsWindow.spn_renderSize.value
		settings.setValue "MergeTexture" SurfaceColorsWindow.cbx_mergeToOneTexture.checked
		settings.setValue "OutputFolder" SurfaceColorsWindow.ddl_outputFolder.selection
	)
	------------------------------------------------------------------
	on btn_newXML pressed do 
	(		
		local file = (getSaveFileName caption:"Select Xml File..." types:".xml|*.xml")
		if (file != undefined) do 
		(
			if (queryBox "Do you want to copy the current settings?" title:"Question?") then
			(
				saveChanges file:file
				xmlPath = file
				loadXML()
			)
			else 
			(
				xml_io = rsta_xml_io()
				xml_io.new "SurfaceTypeColorSet"			
				xml_io.saveas file
		
				xmlPath = file
				loadXML()
			)
		)	
	)
	------------------------------------------------------------------
	on btn_loadXML pressed do 
	(
		xmlPath = getOpenFileName caption:"Select Xml File..." types:".xml|*.xml"
		if (xmlPath != undefined) do 
		(		
			loadXML()
		)
	)
	------------------------------------------------------------------
	on ddl_colourSets selected arg do 
	(
		if (xmlLoaded) do updateUI()
	)
	------------------------------------------------------------------
	on clp_colour changed arg do 
	(
		if (xmlLoaded) do 
		(
			(getCurrentColourSet()).color = arg
			hasChanged = true
		)
	)
	------------------------------------------------------------------
	on btn_RenameColourSet pressed do 
	(
		if (xmlLoaded) do 
		(
			if (SurfaceColorsWindow.edt_colourSetName.text != "") do 
			(
				(getCurrentColourSet()).name = SurfaceColorsWindow.edt_colourSetName.text
				SurfaceColorsWindow.edt_colourSetName.text = ""
				updateUI()
				hasChanged = true
			)
		)		
	)
	------------------------------------------------------------------
	on btn_addColourSet pressed do 
	(
		if (xmlLoaded) AND (SurfaceColorsWindow.edt_colourSetName.text != "") do 
		(
			cSet = st_colorSet name:SurfaceColorsWindow.edt_colourSetName.text color:(color 0 0 0)	
			append colourSets cSet
			SurfaceColorsWindow.edt_colourSetName.text = ""
			updateUI()
			hasChanged = true
		)
	)
	------------------------------------------------------------------
	on btn_removeColourSet pressed do 
	(
		if (xmlLoaded) AND (queryBox ("Are you sure you want to delete \"" + (getCurrentColourSet()).name + "\" ?")) do 
		(
			deleteItem colourSets SurfaceColorsWindow.ddl_colourSets.selection
			set_ColourSetDropDown()
			hasChanged = true
		)
	)
	------------------------------------------------------------------
	on btn_saveXML pressed do 
	(
		if (xmlLoaded) do saveChanges()
	)
	------------------------------------------------------------------
	on btn_addSurface pressed do 
	(
		if (xmlLoaded) do 
		(
			local surfaceType = SurfaceColorsWindow.ddl_SurfaceTypes.selected
			append (getCurrentColourSet()).surfaceTypes surfaceType
			updateUI()
			hasChanged = true
		)
	)
	------------------------------------------------------------------
	on btn_removeSurface pressed do 
	(
		if (xmlLoaded) do 
		(
			local value = SurfaceColorsWindow.lbx_surfaceTypes.selection 
			if (value != 0) do 	deleteItem (getCurrentColourSet()).surfaceTypes value
			updateUI()
			hasChanged = true
		)
	)
	------------------------------------------------------------------
	on btn_render pressed do 
	(
		if (xmlLoaded) do renderSelection()
	)	
)
createDialog SurfaceColorsWindow style:#(#style_titlebar, #style_border, #style_sysmenu,#style_toolwindow)
