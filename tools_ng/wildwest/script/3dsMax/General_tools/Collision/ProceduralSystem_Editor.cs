using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;
using System.IO;
using System.Windows.Data;
using System.Windows.Markup;

namespace ProceduralSystem
{
    //The core editor class
    public class Editor : INotifyPropertyChanged
    {
        //The root element the tool is binded to.
        public FrameworkElement BindedElement { get; set; }

        //Set data - sets and procedurals
        SetData _SetData;
        public SetData SetData
        {
            get { return _SetData; }
            set
            {
                _SetData = value;
                OnPropertyChanged("SetData");

                SelectedProcedural = _SetData.ProceduralList.First();
                SelectedSet = _SetData.SetList.FirstOrDefault();
            }
        }

        //The currently selected procedural
        Procedural _SelectedProcedural;
        public Procedural SelectedProcedural
        {
            get { return _SelectedProcedural; }
            set
            {
                _SelectedProcedural = value;
                OnPropertyChanged("SelectedProcedural");
            }
        }

        //The currently selected set
        Set _SelectedSet;
        public Set SelectedSet
        {
            get { return _SelectedSet; }
            set
            {
                _SelectedSet = value;
                OnPropertyChanged("SelectedSet");
                UpdateValidProceduralList();
            }
        }

        //Procedurals that are valid to add for the current set
        public ObservableCollection<Procedural> ValidProceduralList { get; private set; }

        //Metadata information
        public Metadata Metadata { get; set; }

        //Default path for opening set data files.
        public String DefaultPath { get; private set; }

        public Editor(String InputSetDataFilePath, String InputMetadataFilePath)
        {
            this.SetData = SetData.Read(InputSetDataFilePath);
            this.Metadata = Metadata.Read(InputMetadataFilePath);

            this.DefaultPath = Path.GetDirectoryName( InputSetDataFilePath );
        }

        public void BindElement(FrameworkElement InputTargetElement)
        {
            //Bind our window to this instance of Editor
            BindedElement = InputTargetElement;
            BindedElement.DataContext = this;

            //Attached event handlers
            BindedElement.AddHandler(Button.ClickEvent, new RoutedEventHandler(this.ProcessEvent));
        }

        void ProcessEvent(Object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource is Button)
            {
                var TargetButton = e.OriginalSource as Button;

                if (TargetButton.Name == "LoadSetFileButton")
                {
                    OpenFileDialog OpenDialog = new OpenFileDialog();
                    OpenDialog.InitialDirectory = this.DefaultPath;
                    OpenDialog.Filter = "Procedural set files|*.xml";
                    OpenDialog.ShowDialog();
                    

                    if (File.Exists(OpenDialog.FileName))
                    {
                        try
                        {
                            //Read in the set data
                            SetData = SetData.Read(OpenDialog.FileName);
                        }
                        catch { }
                    }
                }

                if (TargetButton.Name == "SaveSetFileButton")
                {
                    SaveFileDialog SaveDialog = new SaveFileDialog();
                    SaveDialog.InitialDirectory = this.DefaultPath;
                    SaveDialog.Filter = "Procedural set files|*.xml";
                    SaveDialog.ShowDialog();

                    if (SaveDialog.FileName != String.Empty)
                    {
                        SetData.Write(SaveDialog.FileName, SetData);
                    }
                }

                if (TargetButton.Name == "AddProceduralLevelButton")
                {
                    if (SelectedProcedural != null)
                    {
                        SelectedProcedural.LevelList.Add(new Procedural.Level());
                    }
                }

                if (TargetButton.Name == "NewProceduralButton")
                {
                    var NewProcedural = new Procedural();
                    NewProcedural.LevelList.Add(new Procedural.Level());
                    SetData.ProceduralList.Add(NewProcedural);
                    SelectedProcedural = NewProcedural;
                }

                if (TargetButton.Name == "DeleteProceduralButton")
                {
                    SetData.ProceduralList.Remove(SelectedProcedural);
                    SelectedProcedural = SetData.ProceduralList.FirstOrDefault();
                }

                if (TargetButton.Name == "NewSetButton")
                {
                    var NewSet = new Set();
                    SetData.SetList.Add(NewSet);
                    SelectedSet = NewSet;
                }

                if (TargetButton.Name == "AddProceduralToSetButton")
                {
                    var ProceduralToAdd = TargetButton.DataContext as Procedural;

                    if (ProceduralToAdd != null && SelectedSet != null)
                    {
                        SelectedSet.ProceduralList.Add(ProceduralToAdd);
                    }

                    ValidProceduralList.Remove(ProceduralToAdd);
                }

                if (TargetButton.Name == "RemoveProceduralButton")
                {
                    var ProceduralToRemove = TargetButton.DataContext as Procedural;

                    if (ProceduralToRemove != null && SelectedSet != null)
                    {
                        SelectedSet.ProceduralList.Remove(ProceduralToRemove);
                    }
                    ValidProceduralList.Add(ProceduralToRemove);
                }

                if (TargetButton.Name == "Remove_Level_Button")
                {
                    var LevelToRemove = TargetButton.DataContext as Procedural.Level;

                    if (LevelToRemove != null)
                    {
                        SelectedProcedural.LevelList.Remove(LevelToRemove);
                    }
                }

                if (TargetButton.Name == "Select_Thumbnail_Button")
                {
                    var TargetProcedural = TargetButton.DataContext as Procedural;

                    if (TargetProcedural != null)
                    {
                        OpenFileDialog OpenDialog = new OpenFileDialog();
                        if (File.Exists(SelectedProcedural.ThumbnailImageFilePath))
                        {
                            OpenDialog.InitialDirectory = Path.GetFullPath(SelectedProcedural.ThumbnailImageFilePath);
                        }
                        OpenDialog.Filter = "Procedural thumbnail images|*.jpg";
                        OpenDialog.ShowDialog();

                        if (File.Exists(OpenDialog.FileName))
                        {
                            try
                            {
                                TargetProcedural.ThumbnailImageFilePath = OpenDialog.FileName;
                            }
                            catch { }
                        }

                    }
                }
            }
        }

        //This method adds valid procedurals to the procedurals to add combo box
        //Method gets called when a.) we switch sets b.) add a procedural
        void UpdateValidProceduralList()
        {
            if (this.ValidProceduralList == null)
            {
                this.ValidProceduralList = new ObservableCollection<Procedural>();
            }

            ValidProceduralList.Clear();

            if (SelectedSet != null)
            {
                

                //create a list of all the procedurals excluding ones that are already assigned
                var ProceduralMatches = from P in SetData.ProceduralList
                                        where !SelectedSet.ProceduralList.Contains(P)
                                        select P;
                foreach (var MatchedProcedural in ProceduralMatches)
                {
                    ValidProceduralList.Add(MatchedProcedural);
                }
            }
        }

        // Declare the PropertyChanged event
        public event PropertyChangedEventHandler PropertyChanged;

        // NotifyPropertyChanged will raise the PropertyChanged event passing the
        // source property that is being updated.
        public void OnPropertyChanged(String PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
    }
}