using System;
using System.Xml.Serialization;
using System.Xml;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;

namespace ProceduralSystem
{
    public partial class Procedural
    {
        public class Level : INotifyPropertyChanged
        {
            public readonly static Int32 UnsetHandleValue = -1;

            #region FIELDS AND PROPERTIES
            ///-----------------------------------------------------------------------------------------------

            //Name of the level e.g. 'Full', 'Thin' etc...
            String _Name = "";
            [XmlAttribute]
            public String Name 
            {
                get { return _Name; }
                set
                {
                    this._Name = value;
                    if( this.ParentProcedural != null )
                    {
                        this._Name = this.ParentProcedural.ProcessLevelName(this);
                    }
                }
            }

            //Valid values 0 - 3
            [XmlAttribute]
            public Byte DensityLevel { get; set; }

            //Valid values 0 - 15
            [XmlAttribute]
            public Byte ScaleXYZLevel { get; set; }

            //Valid values 0 - 3
            [XmlAttribute]
            public Byte ScaleZLevel { get; set; }

            //Density of the level. Returns value in the range 0.0 - 1.0
            [XmlIgnore]
            public Double Density
            {
                get { return DensityLevel * 0.333; }
            }

            //ScaleXYZ of the level. Returns value in the range 0.0 - 1.0
            [XmlIgnore]
            public Double ScaleXYZ
            {
                get { return ScaleXYZLevel * 0.066; }
            }

            //ScaleZ of the level. Returns value in the range 0.0 - 1.0
            [XmlIgnore]
            public Double ScaleZ
            {
                get { return ScaleZLevel * 0.333; }
            }

            //Ground tint Hex code. Indirectly sets GroundTint. Exposed for writing back and forth to the .xml file.
            [XmlAttribute("GroundTint")]
            public String GroundTintHexCode
            {
                get
                { return GroundTint.HexCode; }
                set
                { GroundTint.HexCode = value; }
            }

            //The ground tint color
            [XmlIgnore]
            public Color GroundTint { get; set; }

            //The parent procedural of the level
            [XmlIgnore]
            public Procedural ParentProcedural { get; set; }

            //The procedural meta tag
            String _TargetTag = "";
            [XmlAttribute]
            public String TargetTag
            {
                get {return _TargetTag;}
                set
                {
                    _TargetTag = value;
                    OnPropertyChanged("TargetTag");
                }
            }

            //Number of faces the procedural level is applied to
            [XmlIgnore]
            public Int32 AppliedFaceCount { get; set; }

            //Indicates if the procedural has been applied
            [XmlIgnore]
            public Boolean Applied
            {
                get { return AppliedFaceCount == 0 ? false : true; }
            }

            //The actual color of the level based on the HueHandle from the parent procedural and it's position in the level list. 
            //Gets assign to by the parent procedural when the level gets added to it's level list.
            [XmlIgnore]
            public Color Color { get; set; }

            [XmlIgnore]
            public Procedural.Level NextLevel 
            {
                get { return ParentProcedural.GetNextLevel(this); }
            }

            [XmlIgnore]
            public Procedural.Level PreviousLevel
            {
                get { return ParentProcedural.GetPreviousLevel(this); }
            }

            [XmlAttribute]
            public Single BlendDistance { get; set; }

            #endregion

            #region METHODS
            ///-----------------------------------------------------------------------------------------------
            
            //Resets the applied face count
            void ResetAppliedFaceCount()
            {
                AppliedFaceCount = 0;
            }

            //Pushes an update on the applied attributes 
            public void PushAppliedFaceCountPropertyUpdate()
            {
                OnPropertyChanged("AppliedFaceCount");
                OnPropertyChanged("Applied");
            }

            ///-----------------------------------------------------------------------------------------------
            #endregion

            #region CONSTRUCTORS
            ///-----------------------------------------------------------------------------------------------

            //Intialize the level with default values for all properties
            public Level()
            {
                this.Name = "CHANGE_ME";
                this.DensityLevel = 3;
                this.ScaleXYZLevel = 7;
                this.ScaleZLevel = 3;
                this.GroundTint = new Color(255, 204, 0);
                this.Color = new Color(0, 0, 0);
            }

            #endregion

            public override string ToString()
            {
                return String.Format("{0}_{1}", this.ParentProcedural.Name, this.Name);
            }

            #region EVENTS
            ///-----------------------------------------------------------------------------------------------

            //Event to signal that a property has changed. Used for data binding.
            public event PropertyChangedEventHandler PropertyChanged = delegate { };
            void OnPropertyChanged(String PropertyName)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }

            #endregion

        }
    }
}