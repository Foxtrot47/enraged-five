filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
rsta_loadCommonFunction #("FN_RSTA_userSettings", "FN_RSTA_rageMat")

if (TetraTools != undefined) do TetraTools.dispose()
struct TetraTools
(
	-- LOCALS ------------------------------------------------
	MainWindow, 
	vm,
	sourceMesh, 	 
	importMesh,
	Settings = rsta_userSettings app:"TetraTool",	
	tetraArray = #(),
	collList = RexGetCollisionList(),
	collMat,
	
	-- =====================================================================
	-- TOOL FUNCTIONS 
	-- =====================================================================
	fn ExportSelected = 
	(
		if (selection.count != 0) do 
		(
			local obj =  selection[1]
			local temp = copy obj			
			
			case (classOf temp) of
			(
				Col_Mesh: col2mesh temp
				Editable_Poly: convertToMesh temp
			)			
			
			temp.name = "TetraMesh"
			select temp 
			exportFile "x:/tetra_export.obj" #noPrompt selectedOnly:true 
			delete temp
			select obj
			
			this.setSourceMesh obj:obj
		)
	),	
	-------------------------------------------------------------------
	fn OpenHoudini = 
	(
		local file = RsConfigGetWildWestDir() + @"dcc\houdini\Tetrahedron\obj_tetra_obj.hip"
		gRsPerforce.sync #(file)
		if (doesFileExist file) then 
		(
			ShellLaunch file ""
		)
		else messageBox ("Error, " + file + " doesn't exist. Please Contact Matt Harrad") title:"Tetra Tools"
	),
	-------------------------------------------------------------------
	fn ImportHoudiniMesh = 
	(
		local file = "x:/tetra_import.obj" 
		if (doesFileExist file) then 
		(
			importFile file #noPrompt 
			
			local obj = getNodeByName "tetra_import"
			if (obj != undefined) do this.setImportMesh obj:obj
			
		)
		else messageBox ("Error, " + file + " doesn't exist. Have you exported it from Houdini yet?") title:"Tetra Tools"
	),
	-------------------------------------------------------------------
	fn add_rexControls = 
	(	
		if (tetraArray.count != 0) AND (collMat == undefined) do collMat = tetraArray[1].mat
		if (collMat != undefined) do 
		(
			if (classof collMat == Multimaterial) then 
			(			
				for i=1 to collMat.materialList.count do 
				(
					local mat = collMat.materialList[i]		
					local RexID = (findItem collList (RexGetCollisionName mat))-1
					local c = color 0 0 0  -- mat.rbmGetCollisionColour()

					vm.addMaterial collMat.materialIDList[i] collMat.names[i] RexID #(c.r,c.g,c.b)
				)
			)		
		)
	),
	-------------------------------------------------------------------
	fn add_tetra_to_array t =
	(
		if (vm.RexMaterials.count == 0) do add_rexControls()
		if (classof_array #(Editable_mesh, Editable_poly) t) AND (t.mesh.numfaces == 4) do 
		(
			converttopoly t
			append tetraArray t
			vm.TetraArrayCount = tetraArray.count as string + " Tetra"
		)
	), 
	-------------------------------------------------------------------
	fn TetraArray_Add =
	(
		for i in selection do add_tetra_to_array i		
	),
	-------------------------------------------------------------------
	fn TetraArray_Reset =
	(
		tetraArray = #()
		vm.TetraArrayCount = "No Tetra."
		vm.RexMaterials.clear()
		collMat =  undefined
	),
	-------------------------------------------------------------------
	fn rexMat_edit cmd value matID = 
	(
		case cmd of 
		(
			#type: RexSetCollisionName collMat.materialList[matID+1] collList[value+1]			
			#name: collMat.names[matID+1] =TetraTools.vm.RexMaterials.Item[matID].Name			
		)
	),
	-------------------------------------------------------------------
	fn AddRexMat =
	(
		if (collMat != undefined AND classof collMat == Multimaterial) do  
		(			
			local newRex = RexBoundMtl()
			collMat.MaterialList[collMat.MaterialList.count + 1] = newRex			
			vm.addMaterial collMat.materialIDList[collMat.MaterialList.count] collMat.names[collMat.MaterialList.count] -1 #(0,0,0)
		)
	),	
	-------------------------------------------------------------------
	fn rexMat_cmd cmd matID =
	(
		local rexMat = vm.RexMaterials.Item[matID]
		case cmd of 
		(
			#colour:
			(
				local int_c = color (rexMat.SwatchColor.Color.R) (rexMat.SwatchColor.Color.G) (rexMat.SwatchColor.Color.B)
				local c = colorPickerDlg int_c "Pick Colour...." 
			
				rexMat.SwatchColor.Color.R = c.r
				rexMat.SwatchColor.Color.G = c.g
				rexMat.SwatchColor.Color.B = c.b
				
				--collMat.materialList[rexMat.MatIdx].rbmSetCollisionColour c
			)
			#set:
			(
				for obj in selection where ((finditem tetraArray obj) != 0) do 
				(
					obj.mat = collMat
					for f=1 to obj.numfaces do
					(
						polyop.setFaceMatID obj f rexMat.MatIdx
					)
				)
			)
			#sel:
			(
				local objArray = #()
				for obj in tetraArray do 
				(
					if ((getFaceMatID obj.mesh 1) == rexMat.MatIdx) do append objArray  obj
				)
				select objArray
			)
			#show:
			(
				local objArray = #()
				for obj in tetraArray do 
				(
					if ((getFaceMatID obj.mesh 1) == rexMat.MatIdx) do append objArray obj
				)
				unhide objArray
			)
			#hide:
			(
				local objArray = #()
				for obj in tetraArray do 
				(
					if ((getFaceMatID obj.mesh 1) == rexMat.MatIdx) do append objArray  obj
				)
				hide objArray
			)
			#iso:
			(
				local showA = #()
				local hideA = #()
				for obj in tetraArray do 
				(
					if ((getFaceMatID obj.mesh 1) == rexMat.MatIdx) then append showA obj
					else append hideA obj
				)
				unhide showA
				hide hideA
			)
		)
	),
	-------------------------------------------------------------------
	fn TetraArray_ShowAll = 
	(
		unhide tetraArray
	),
	-------------------------------------------------------------------
	fn breakIntoTetra obj = 
	(
		tetraArray = #()
		obj = copy obj
		convertToPoly obj
		polycount = obj.numFaces
		count = 1
		
		while polycount > 4 do 
		(	
			faces = polyop.getElementsUsingFace obj 1
			polyop.setFaceSelection obj faces
			polyop.detachFaces obj faces asNode:true name:("tetra_" + count as string)	
			add_tetra_to_array (getNodeByName ("tetra_" + count as string))
			
			polycount = obj.numFaces	
			count += 1
		)
		
		obj.name = ("tetra_" + count as string)	
		append tetraArray (getNodeByName ("tetra_" + count as string))	
	),
	-------------------------------------------------------------------
	fn setSourceMesh obj: = 
	(
		if (obj == unsupplied) AND (selection.count != 0) do obj = selection[1]
		if (obj != undefined) then
		(
			mesh2col obj
			sourceMesh = obj
			vm.ColTet_sourceMesh = sourceMesh.name
			collMat  = sourceMesh.mat
		)
	),
	-------------------------------------------------------------------
	fn setImportMesh obj: = 
	(
		if (obj == unsupplied) AND (selection.count != 0) do obj = selection[1]
		if (obj != undefined) AND (classof_array #(Editable_mesh, Editable_poly) obj) then
		(
			importMesh = obj
			vm.ColTet_importMesh = importMesh.name
		)	
	),
	-------------------------------------------------------------------
	fn processHoudiniMesh = 
	(
		max create mode
		if (sourceMesh != undefined) AND (importMesh != undefined) then 
		(
			breakIntoTetra importMesh
			-- RESET IMPORT MESH
			delete importMesh
			importMesh = undefined
			vm.ColTet_importMesh = ""
			
			for t in tetraArray do 
			(
				t.pivot = sourceMesh.pivot
				t.mat = collMat
			)		
		)
		else 
		(
			messagebox "Please set source and import mesh first."
		)
	),	
	
	-- =====================================================================
	--  IMPORT FUNCTIONS 
	-- =====================================================================	
	fn make_tetra posArray matID =
	(
		local previewMesh = Hedra family:0 scaleq:100 scalep:100 scaler:100 radius:1 q:0 p:1
		converttomesh previewMesh	
		for i in #(1,2,3,4) do setvert previewMesh i posArray[i]
		previewMesh.pivot = previewMesh.center
		previewMesh.mat = collMat
		previewMesh.wirecolor = color 0 0 0 
		
		for f=1 to previewMesh.numfaces do setFaceMatID previewMesh f matID		
		
		add_tetra_to_array previewMesh
	),
	--------------------------------------------------------------------------------
	fn importNodeEle = 
	(
		local fileName = getOpenFileName caption:"Export File Name..." types:"Node File (*.node)|*.node|Element File (*.ele)|*.ele"
		if (fileName != undefined) do 
		(
			local nodeFile = (getFilenamePath fileName) + (getFilenameFile fileName) + ".node"
			local eleFile =  (getFilenamePath fileName) + (getFilenameFile fileName) + ".ele"
			
			if (doesFileExist nodeFile) AND (doesFileExist eleFile)  do 
			(			
				TetraArray_Reset()				
				
				local pointsArray = #()
				
				-- NODE FILE 	
				local fstream = openFile nodeFile 
				lineCount = 1
				while not eof fstream do 
				(
					theLine = readline fstream
					if (lineCount != 1) AND (theLine[1] != "#") do 
					(
						local bits = filterString theLine " \t"	
						append pointsArray [bits[2] as float,bits[3] as float,bits[4] as float]		
					)
					lineCount += 1
				)	
				close fstream
						
				-- ELE FILE	
				local tetraToMake = #()
				local rexMatIds = #()
				
				local fstream = openFile eleFile 
				lineCount = 1
				while not eof fstream do 
				(
					theLine = readline fstream
					if (lineCount != 1) AND (theLine[1] != "#") do 
					(
						local bits = filterString theLine " \t"
						local posArray = #()
						
						for i=2 to bits.count-1 do 
						(
							vertIdx = bits[i] as integer
							vertPos = pointsArray[vertIdx+1]
							append posArray vertPos
						)
						appendIfUnique rexMatIds bits[bits.count]
						append tetraToMake (datapair pos:posArray matId:(finditem rexMatIds bits[bits.count]))
						
					)
					lineCount += 1
				)	
				close fstream	
				
				-- MAKE MULTI MAP
				collMat = Multimaterial()
				collMat.numsubs  = rexMatIds.count
				
				for i=1 to rexMatIds.count do 
				(
					collMat.materialList[i] = RexBoundMtl()
					RexSetCollisionName collMat.materialList[i] (RexGetCollisionList())[rexMatIds[i] as integer]
				)
								
				for t in tetraToMake do make_tetra t.pos t.matId
			)
			
		)
	),
	
	-- =====================================================================
	--  EXPORT FUNCTIONS 
	-- =====================================================================
	fn wline str file = 
	(
		format "%\n" str to:file	
	),
	--------------------------------------------------------------------------------
	fn makeFile fileName type =
	(
		local filePath = getFilenamePath fileName
		makeDir filePath
		return (createfile ((getFilenameFile fileName) + "." + type))		
	),	
	--------------------------------------------------------------------------------
	fn getPointIdx pnt &PointDataArray =
	(
		local idx = findItem PointDataArray pnt
		if (idx == 0) do 
		(
			append PointDataArray pnt
			return PointDataArray.count
		)
		return idx
	),
	--------------------------------------------------------------------------------
	fn getRexId mapID obj =
	(
		local matIdx = findItem obj.mat.materialIDList mapID
		local faceMat = obj.mat.materialList[matIdx]
		return (findItem (RexGetCollisionList()) (RexGetCollisionName faceMat))
	),
	--------------------------------------------------------------------------------
	fn exportNodeEle = 
	(
		if (tetraArray.count != 0) do 
		(
			local fileName = getSaveFileName caption:"Export File Name..." types:".node|.node|.ele|.ele"
	 
			if (fileName != undefined) do 
			(
				local PointDataArray = #()
				local TetraDataArray = #()

				for t in tetraArray do 
				(
					convertToMesh t					

					-- GET THE VERT POS
					local p1 = getvert t 1
					local p2 = getvert t 2
					local p3 = getvert t 3
					local p4 = getvert t 4
					
					-- ORDER THE POINTS 
					local faceCenter = (p1 + p2 + p3)/3
					local v_normal = cross (p2-p1) (p3-p1)			
					local dot_n_p4 = dot v_normal (p4 - faceCenter)
						
					if (dot_n_p4 > 0) do 
					(
						pX = p4; p4 = p3; p3 = p2; p2 = p1; p1 = pX		
					)						
					
					-- REMIOVE THE OFFSET AND FIND THE INDEX
					p1 = (getPointIdx (p1 - t.pos) PointDataArray) - 1
					p2 = (getPointIdx (p2 - t.pos) PointDataArray) - 1
					p3 = (getPointIdx (p3 - t.pos) PointDataArray) - 1
					p4 = (getPointIdx (p4 - t.pos) PointDataArray) - 1
					
					-- ADD TO THE TETRA DATA ARRAY				
					append TetraDataArray #(p1,p2,p3,p4, (getRexId (getFaceMatID t 1) t))						
				)
				
				-- WRITE NODE FILE 
				
				nodeFile = makeFile fileName "node"	
				wline (PointDataArray.count as string + "\t\t3\t\t1") nodeFile
				
				for c=1 to PointDataArray.count do
				(
					local p = PointDataArray[c]
					local str = (c-1) as string + "\t\t"
					for i=1 to 3 do str += (formattedPrint p[i] format:"#.7f") + "\t\t"
					wline (str + "0") nodeFile
				)
				
				wline ("# " + localTime) nodeFile

				close nodeFile	

				
				-- WRITE ELE FILE 
				eleFile = makeFile fileName "ele"	
				wline (TetraDataArray.count as string + "\t\t4\t\t1") eleFile

				for c=1 to TetraDataArray.count do
				(
					local t = TetraDataArray[c]
					local str = (c-1) as string + "\t\t"
					for i=1 to 5 do str += (t[i] as string) + "\t\t"
					wline (str) eleFile
				)
				
				wline ("# " + localTime) eleFile

				close eleFile
				
				messageBox "Export Compleate!"
			)
		)
	),	
	
	-- =====================================================================
	-- STRUCT FUNCTIONS 
	-- =====================================================================
	fn dispose = 
	(
		if (MainWindow != undefined) do 
		(
			-- WINDOW POS 
			settings.wpf_windowPos mainWindow #set
			-- CLOSE AND DISPOSE THE WINDOW
			MainWindow.Close()     
			MainWindow = undefined
		)
		execute ("TetraTools = undefined")
	),
	-------------------------------------------------------------------
	fn init =
	(
		-- LOAD ASSEMBLY		
		dotnet.loadAssembly  (RsConfigGetToolsDir() + @"techart\dcc\3dsMax\RSG.TechArt.TetraTools.dll")
		-- MAKE MAIN WINDOW INSTANCE 
		MainWindow = dotNetObject "RSG.TechArt.TetraTools.MainWindow"             
		dn_window.setup MainWindow 
		MainWindow.Tag = dotnetmxsvalue this
		-- PARENTS THE WINDOW UNDER MAX, STOPS IT FROM DROPPING BEHIND
		MainWindow.ChangeOwner (DotNetObject "System.IntPtr" (Windows.GetMAXHWND()))			
		-- WINDOW POS
		settings.wpf_windowPos mainWindow #get
		-- SHOW THE WINDOW
		MainWindow.show()
		vm = MainWindow.datacontext
		MainWindow.Title = "Tetra Tools - v1.01"
			
		for i in collList do vm.addRexType i

	),
	-- EVENTS -----------------------------------------------------
	on create do init()            
)
TetraTools = TetraTools()
