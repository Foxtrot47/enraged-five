FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

--FileIn the types for the procedural painter and editor
FileIn "ProceduralBase.ms"

try( ProceduralEditor.ToolWindow.Close() ) catch()

struct ProceduralEditor
(
	Editor,
	ToolWindow,
		
	fn CreateTestScene =
	(
		GroundPlane = Plane Width:1000 Length:1000 Name:"GROUND_PLANTHINE"
		ConvertToPoly GroundPlane
		GroundPlaneCollision = Copy GroundPlane
		GroundPlaneCollision.Parent = GroundPlane
		GroundPlaneCollision.Name = "GROUND_PLANE_COL"
		Mesh2Col GroundPlaneCollision
		
		ProceduralIterator = 0
		
		--Go through each procedural
		for Procedural in Editor.SetData.ProceduralArray do
		(
			ProceduralText = Text Text:Procedural.Name Size:0.17 Steps:0
			ProceduralText.Position = [ProceduralIterator * 5, -1.75, 0.25]
			AddModifier ProceduralText (Extrude Amount:0.01)
			ConvertToPoly ProceduralText
			ProceduralText.Name = UniqueName "PROC_LABEL"
			
			ProceduralLevelIterator = 0
			
			--Go through each level and create our plane
			for ProceduralLevel in Procedural.LevelArray do
			(
				ProceduralLevelPlane = Plane Width:3 WidthSegs:1 Length:3 LengthSegs:1
				ProceduralLevelPlane.Name = UniqueName "PROC_LEVEL"
				ProceduralLevelPlane.Position = [ProceduralIterator * 5, ProceduralLevelIterator * 3, 0.25]
				ConvertToPoly ProceduralLevelPlane
				ProceduralLevelPlaneCollision = Copy ProceduralLevelPlane
				ProceduralLevelPlaneCollision.Parent = ProceduralLevelPlane
				ProceduralLevelPlaneCollision.Name = UniqueName "PROC_COL"
				Mesh2Col ProceduralLevelPlaneCollision
				
				--Setup our map values from the procedural level
				HandleMapValue = [ProceduralLevel.ParentProcedural.HandleA, ProceduralLevel.ParentProcedural.HandleB, ProceduralLevel.Handle]
				
				--Set channel map support
				PolyOp.SetMapSupport ProceduralLevelPlane 31 True
				
				--Set the target faces to the handle map value
				RSPoly_SetFace_MapValue ProceduralLevelPlane 1 31 HandleMapValue
				
				ProceduralLevelIterator = ProceduralLevelIterator + 1
			)
			
			ProceduralIterator = ProceduralIterator + 1
		)
		
	),
	
	--The paths to the relevant metadata and UI markup for the tool
	UIMarkupFilePath = RsMakeBackSlashes( RsConfigGetWildWestDir() + "/script/3dsMax/UI/ProceduralEditor.xaml" ),
	SetDataFilePath = RsMakeBackSlashes( RsConfigGetAssetsDir() + "/maps/procPaint/ProceduralSets.xml" ),
	MetadataFilePath = RsMakeBackSlashes( RsConfigGetCommonDir() + "/data/materials/procedural.meta" ),
	ThumbnailDirectoryPath = RsMakeBackSlashes( RsConfigGetAssetsDir() + "/maps/procPaint/Thumbnails/..." ),
	
	on Create do
	(
		--Sync to the latest on the relevant files
 		if( gRsPerforce.connect() ) then
 		(
			RSPushPrompt "Syncing assets..."
			SyncSuccess = gRsPerforce.Sync #( 	this.SetDataFilePath, 
												this.MetadataFilePath,
												this.ThumbnailDirectoryPath 	)

			if not ( SyncSuccess ) then ( MessageBox "Unable to sync correctly to some of the required assets. You may have them set to be writable locally and/or there maybe problems with your P4 setup. The tool may not function correctly without the latest on those assets. Continue at your own risk..." )
			RSPopPrompt()
 		)
		
		--Create an instance of the 'Editor' class passing in the relevant file paths
		this.Editor = DotNetObject "ProceduralSystem.Editor" SetDataFilePath MetadataFilePath
		
		--Setup our tool window and open it
		this.ToolWindow = RSToolWindow Name:"Procedural Editor" UISource:UIMarkupFilePath

		--Bind our tool window content to the editor instance
		--This will link them together.
		this.Editor.BindElement this.ToolWindow.Content
		
		--Open are window
		this.ToolWindow.Open()
	)
)

ProceduralEditor = ProceduralEditor()