using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using System.Collections.Generic;

namespace ProceduralSystem
{
    /// <summary>
    /// Manages a set of CollisionCombination objects. 
    /// </summary>
    public class CollisionCombinationSet
    {
        Dictionary<Int32, CollisionCombination> _CollisionCombinationDictionary = new Dictionary<Int32, CollisionCombination>();
        public CollisionCombination[] CollisionCombinationArray
        {
            get { return _CollisionCombinationDictionary.Values.ToArray(); }
        }

        public FaceInfo[] FaceInfoArray
        {
            get { return (from ColCombination in _CollisionCombinationDictionary.Values from F in ColCombination.FaceInfoList select F).ToArray(); }
        }

        public void AddFace(Int32 CombinationHash, Int32 InputFaceIndex, Color InputGroundTint)
        {
            if (!_CollisionCombinationDictionary.ContainsKey(CombinationHash))
            {
                this._CollisionCombinationDictionary.Add(CombinationHash, new CollisionCombination(CombinationHash));
            }

            _CollisionCombinationDictionary[CombinationHash].AddFace(InputFaceIndex, InputGroundTint);
        }

        public Int32 BaseCombinationCount()
        {
            return _CollisionCombinationDictionary.Values.Count;
        }

        public Int32 FullCombinationCount()
        {
            Int32 ReturnCount = 0;
            foreach (var ColCombination in _CollisionCombinationDictionary.Values)
            {
                ReturnCount += ColCombination.GetUniqueGroundTintList().Count;
            }
            return ReturnCount;
        }

        public void CondenseGroundTints(Int32 InputThreshold)
        {
            foreach (var ColCombination in _CollisionCombinationDictionary.Values)
            {
                ColCombination.CondenseGroundTints(InputThreshold);
            }
        }
    }

    public class CollisionCombination
    {
        readonly Int32 _CombinationHash;
        public Int32 CombinationHash { get { return _CombinationHash; } }

        public List<FaceInfo> FaceInfoList { get; private set; }

        public CollisionCombination(Int32 InputCombinationHash)
        {
            this._CombinationHash = InputCombinationHash;
            this.FaceInfoList = new List<FaceInfo>();
        }

        internal void AddFace(Int32 InputFaceIndex, Color InputGroundTint)
        {
            FaceInfoList.Add(new FaceInfo { FaceGroundTint = InputGroundTint, FaceIndex = InputFaceIndex });
        }

        public List<Color> GetUniqueGroundTintList()
        {
            return (from F in FaceInfoList select F.FaceGroundTint).Distinct().ToList();
        }

        public void CondenseGroundTints(Int32 InputThreshold)
        {
            var UniqueGroundTintList = this.GetUniqueGroundTintList();

            var MasterColorListSet = new List<List<Color>>();

            for (Int32 InnerIndex = 0; InnerIndex < UniqueGroundTintList.Count - 1; InnerIndex++)
            {
                for (Int32 OuterIndex = InnerIndex + 1; OuterIndex < UniqueGroundTintList.Count; OuterIndex++)
                {
                    var Difference = 0;

                    var ColorA = UniqueGroundTintList[InnerIndex];
                    var ColorB = UniqueGroundTintList[OuterIndex];

                    Difference += Math.Abs(ColorA.R - ColorB.R);
                    if (Difference > InputThreshold) { continue; }

                    Difference += Math.Abs(ColorA.G - ColorB.G);
                    if (Difference > InputThreshold) { continue; }

                    Difference += Math.Abs(ColorA.B - ColorB.B);
                    if (Difference > InputThreshold) { continue; }


                    var MatchingColorLists = (from ColorList in MasterColorListSet where ColorList.Contains(ColorA) || ColorList.Contains(ColorB) select ColorList).ToList();
                    List<Color> MatchedColorList;
                    if (MatchingColorLists.Count() == 1)
                    {
                        MatchedColorList = MatchingColorLists.First();
                    }
                    else
                    {
                        MatchedColorList = new List<Color>();
                        MasterColorListSet.Add(MatchedColorList);
                        foreach (var MatchingColorList in MatchingColorLists)
                        {
                            MatchedColorList.AddRange(MatchingColorList);
                            MasterColorListSet.Remove(MatchingColorList);
                        } 
                    }

                    if (!MatchedColorList.Contains(ColorA)) { MatchedColorList.Add(ColorA); }
                    if (!MatchedColorList.Contains(ColorB)) { MatchedColorList.Add(ColorB); }
                }
            }

            foreach (var ColorList in MasterColorListSet)
            {
                Int32 ColorCount = 0;
                Int64 SumR = 0;
                Int64 SumG = 0;
                Int64 SumB = 0;

                foreach (var SubColor in ColorList)
                {
                    //Get the amount that color is used so we weight evenly
                    var SubColorCount = (from F in FaceInfoList where F.FaceGroundTint == SubColor select F).Count();

                    if (SubColorCount == 0)
                    {
                        var MatchingColorList = ( from CL in MasterColorListSet where CL.Contains( SubColor ) select CL ).ToList();
                    }

                    ColorCount += SubColorCount;
                    SumR += (SubColor.R * SubColorCount);
                    SumG += (SubColor.G * SubColorCount);
                    SumB += (SubColor.B * SubColorCount);
                }

                var AveragedColor = new Color(
                    (Byte)(SumR / ColorCount),
                    (Byte)(SumG / ColorCount),
                    (Byte)(SumB / ColorCount));


                var TargetFaceInfoList = from F in FaceInfoList where ColorList.Contains(F.FaceGroundTint) select F;
                foreach (var SubFaceInfo in TargetFaceInfoList)
                {
                    SubFaceInfo.FaceGroundTint = AveragedColor;
                }
            }
        }
    }

    public class FaceInfo
    {
        public Color FaceGroundTint = new Color(0, 0, 0);
        public Int32 FaceIndex = -1;
    }
}
