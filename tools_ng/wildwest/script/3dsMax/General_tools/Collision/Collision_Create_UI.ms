
-----------------------------------------------------------------------------
-- Collision Create Rollout
-----------------------------------------------------------------------------
rollout RsCollCreateRoll "Collision Create"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	local btnWidth = 130
	local btnSmWidth = 90
	
	group "Collision Mesh:"
	(
		button btnCreateColMesh "Collision Setter" offset:[-4, -3] width:btnSmWidth align:#left across:2 tooltip:"Open the Collision Set tool"
		button btnPaintColMesh "Collision Paint" pos:(btnCreateColMesh.pos + [btnSmWidth + 4, 0]) width:btnSmWidth tooltip:"Open the Collision Paint tool"
	)

	group "Create Box(es):" 
	(	
		button btnCreateColBox "Create Box" width:btnSmWidth offset:[-3, 6] align:#left height:46 across:3 tooltip:"Create aligned collision-box around selected subobjects"
		radiobuttons chooseBoxFit "Box Rotation:" labels:#("Axis-aligned","Z-Rotated", "YZ-Rotated", "XYZ-Rotated") align:#left offset:[10,-8]
		radiobuttons chooseBoxLocks "Ramp type:" labels:#("Any","Wide Ramp", "Long Ramp") align:#right offset:[0,-8] enabled:false
	)
	
	group "Create Cylinder/Capsule(s):" 
	(
		button btnCreateColCylinder "Create Cylinder" width:btnSmWidth offset:[-3,0] across:3 align:#left tooltip:"Create collision-cylinder around selected subobjects"
		button btnCreateColCapsule "Create Capsule" width:btnSmWidth pos:(btnCreateColCylinder.pos + [btnSmWidth + 4,0]) align:#left tooltip:"Create aligned collision-capsule around selected subobjects"
		
		radiobuttons chooseCylDir "Cyl/Caps length:" labelOnTop:true labels:#("Long", "Short") align:#right offset:[0,-6]
	)

	group "Create Sphere(s):"
	(
		button btnCreateColSphere "Create Sphere" width:btnSmWidth offset:[-3, 0] align:#left across:2 tooltip:"Create collision-sphere around selected subobjects"
		spinner spnSphereInacc "Fit inaccuracy:" range:[0.01,1.0,0.05] scale:0.01 align:#right offset:[0,4] width:100
	)
	
	group "General Options:"
	(
		checkbox chkCreatePerElement "Create objects per element" offset:[-2,-2] tooltip:"Subobjects on seperate face-elements will be given separate collision-objects" across:2
		checkbox chkCreateAsGeom "Create as geometry" offset:[10,-2] tooltip:"Create geometry primitives instead of collision objects"
		checkbox chkSetNoDecal "Set \"No Decal\" flag on create" offset:[-2,0] tooltip:"\"No Decal\" flag is set on new collision-primitives"

		spinner spnCollXPadding "Padding: X/Rad:" range:[-10.0,10.0,0.0] scale:0.01 align:#left offset:[-1,2] across:3
		spinner spnCollYPadding "Y/Len:" range:[-10.0,10.0,0.0] scale:0.01 pos:(spnCollXPadding.pos + [18,0])
		spinner spnCollZPadding "Z:" range:[-10.0,10.0,0.0] scale:0.01 pos:(spnCollYPadding.pos + [18,0])
	)
	
	label spacerLblA "" offset:[0,-12]
	
	label lblHelpBack "          " offset:[0,-20] align:#right
	hyperlink lnkHelp "Help?" pos:(lblHelpBack.pos + [3,0]) address:"https://devstar.rockstargames.com/wiki/index.php/Collision_Toolkit#Collision_Create" color:Blue hoverColor:Blue visitedColor:Blue
	
	--------------------------------------------------------------	
	-- Open the Collision Setter tool
	--------------------------------------------------------------	
	on btnCreateColMesh pressed do 
	(
		filein (::RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Collision/collset.ms")
	)

	--------------------------------------------------------------	
	-- Open the Collision Painter tool
	--------------------------------------------------------------	
	on btnPaintColMesh pressed do 
	(
		fileIn (::RsConfigGetWildWestDir() + "script/3dsMax/General_tools/Collision/ProcPainter_UI.ms")
	)
	
	on chooseBoxFit changed val do 
	(
		chooseBoxLocks.enabled = (val == 3)
	)
	
	--------------------------------------------------------------	
	-- Create collision-boxes for selection
	--------------------------------------------------------------	
	on btnCreateColBox pressed do 
	(
		undo "create fitted box" on 
		(
			local rotZ = (chooseBoxFit.state == 2)
			local rotYZ = (chooseBoxFit.state == 3)
			local rotXYZ = (chooseBoxFit.state == 4)
			
			local newObjs = RsCreateCollisionGeometry collClass:#box padding:[spnCollXPadding.value,spnCollYPadding.value,spnCollZPadding.value] \
				createAsGeometry:chkCreateAsGeom.checked createPerElement:chkCreatePerElement.checked \ 
				rotZ:rotZ rotYZ:rotYZ rotXYZ:rotXYZ \
				wideRamp:(rotYZ and (chooseBoxLocks.state == 2)) longRamp:(rotYZ and (chooseBoxLocks.state == 3))
		)
	)
	
	--------------------------------------------------------------	
	-- Create collision-spheres for selection
	--------------------------------------------------------------	
	on btnCreateColSphere pressed do 
	(
		undo "create fitted sphere" on 
		(
			local newObjs = RsCreateCollisionGeometry collClass:#sphere padding:[spnCollXPadding.value,0,0] \ 
				createAsGeometry:chkCreateAsGeom.checked createPerElement:chkCreatePerElement.checked \ 
				inaccuracy:spnSphereInacc.value
		)
	)
	
	--------------------------------------------------------------	
	-- Create collision-cylinders for selection
	--------------------------------------------------------------	
	on btnCreateColCylinder pressed do 
	(
		undo "create fitted cylinder" on 
		(
			local newObjs = RsCreateCollisionGeometry collClass:#cylinder padding:[spnCollXPadding.value,spnCollYPadding.value,0] \
				createAsGeometry:chkCreateAsGeom.checked createPerElement:chkCreatePerElement.checked \
				longCyl:(chooseCylDir.state == 1)
		)
	)

	--------------------------------------------------------------	
	-- Create collision-capsules for selection
	--------------------------------------------------------------	
	on btnCreateColCapsule pressed do 
	(
		undo "create fitted capsule" on 
		(
			local newObjs = RsCreateCollisionGeometry collClass:#capsule padding:[spnCollXPadding.value,spnCollYPadding.value,0] \
				createAsGeometry:chkCreateAsGeom.checked createPerElement:chkCreatePerElement.checked \ 
				longCyl:(chooseCylDir.state == 1)
		)
	)
	
	-- Don't allow "Set No Decal" checkbox to be active at the same time as "Create as geometry"
	on chkCreateAsGeom changed state do 
	(
		chkSetNoDecal.enabled = not state
		if state do (chkSetNoDecal.checked = False)
	)
	
	--------------------------------------------------------------
	-- Rollout open/close
	--------------------------------------------------------------
	on RsCollCreateRoll open do
	(
		-- Move "Help?" up to the top of rollout:
		lblHelpBack.pos.y = lnkHelp.pos.y = 6
	)
	
	on RsCollCreateRoll rolledUp down do 
	(
		RsSettingWrite "RsCollCreateRoll" "rollup" (not down)
	)
	
	--------------------------------------------------------------
	-- Floater move/resize events happen to first rollout:
	--------------------------------------------------------------
	on RsCollCreateRoll moved newPos do ()
	
	on RsCollCreateRoll resized newSize do 
	(
		RsSettingWrite "RsCollCreateRoll" "height" newSize.y
	)
)
