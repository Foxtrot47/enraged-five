using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using System.Collections.Generic;

namespace ProceduralSystem
{
    public class SetData
    {

        #region FIELDS AND PROPERTIES
        //-----------------------------------------------------------------------------------------------

        [XmlIgnore]
        readonly public Procedural BlankProcedural;
        [XmlIgnore]
        readonly public Procedural.Level BlankProceduralLevel;

        [XmlArray("ProceduralDefinitions")]
        public ObservableCollection<Procedural> ProceduralList { get; set; }

        [XmlIgnore]
        public Procedural[] ProceduralArray
        {
            get
            {
                return (new List<Procedural>(ProceduralList).ToArray());
            }
        }

        [XmlIgnore]
        public ObservableCollection<Procedural> ErroneousProceduralList { get; set; }

        [XmlArray("SetDefinitions")]
        public ObservableCollection<Set> SetList { get; set; }

        [XmlAttribute]
        public Guid Version { get; set; }

        //-----------------------------------------------------------------------------------------------
        #endregion

        #region CONSTRUCTORS
        //-----------------------------------------------------------------------------------------------

        public SetData()
        {
            this.Version = Guid.NewGuid();

            this.ProceduralList = new ObservableCollection<Procedural>();
            this.ProceduralList.CollectionChanged += ProceduralList_CollectionChanged;

            this.SetList = new ObservableCollection<Set>();
            this.SetList.CollectionChanged += SetList_CollectionChanged;

            this.ErroneousProceduralList = new ObservableCollection<Procedural>();

            //Setup our blank procedural
            BlankProcedural = new Procedural
            {
                Name = "Blank",
                TargetTag = "_EMPTY_DO_NOT_USE_",
                DisplayOpacity = 0
            };
            BlankProceduralLevel = new Procedural.Level { Name = "None", GroundTint = new Color(128, 128, 128) };
            BlankProcedural.LevelList.Add(BlankProceduralLevel);

            //Set the level color to mid grey
            BlankProceduralLevel.Color.FromRGB(128, 128, 128);
        }

        //-----------------------------------------------------------------------------------------------
        #endregion

        #region METHODS
        //-----------------------------------------------------------------------------------------------

        //Triggered when a new set is added to the SetFile. Links the new Set to 
        //this SetFile and intializes it. Including assigning a new and unique name. 
        public void SetList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Set AddedSet in e.NewItems)
                {
                    AddedSet.Parent = this;
                    if (AddedSet.Name == String.Empty)
                    {
                        AddedSet.Name = GetNextSetName();
                    }
                }
            }
        }

        //Triggered when a new procedural is added to the SetFile. Links the new Procedural to this SetFile
        //Assigns the procedural a new and unique name. Also assigns a unique handle. 
        public void ProceduralList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Procedural AddedProcedural in e.NewItems)
                {
                    AddedProcedural.Parent = this;
                    if (AddedProcedural.Name == String.Empty)
                    {
                        AddedProcedural.Name = GetNextProceduralName();
                    }

                    if (AddedProcedural.ColorHue == Procedural.UnsetColorHue)
                    {
                        AddedProcedural.ColorHue = GetNextProceduralColorHue();
                    }
                }
            }
        }

        //Returns a unique and valid name for use with a new Set
        String GetNextSetName()
        {
            //register a increment variable
            Int32 DefaultCount = 0;

            //go through each of the sets currently registered
            foreach (var SubSet in SetList)
            {
                //check to see if that set is using the default name
                if (Regex.IsMatch(SubSet.Name, @"^New Set [0-9]*\z", RegexOptions.IgnoreCase))
                {
                    //if it is then increment the number of default names detected
                    DefaultCount += 1;
                }
            }

            //return default name postfixed with the number of defaults found
            return String.Format(@"New Set {0:00}", DefaultCount);
        }

        //Returns a unique and valid name for use with a new Procedural
        String GetNextProceduralName()
        {
            //register an increment variable
            Int32 DefaultCount = 0;

            //go through each of the procedurals currently registered
            foreach (var SubProcedural in ProceduralList)
            {
                //check to see if that procedural is using the default name
                if (Regex.IsMatch(SubProcedural.Name, @"^New Procedural [0-9]*\z", RegexOptions.IgnoreCase))
                {
                    //if it is then increment the number of default names detected
                    DefaultCount += 1;
                }
            }

            //return default name postfixed with the number of defaults found
            return String.Format(@"New Procedural {0:00}", DefaultCount);
        }

        //Returns a valid handle value (0-360) which is not in use by another procedural
        Int32 GetNextProceduralColorHue()
        {
            Int32 ReturnColorHue = GetClosestColorHueAvailable(ProceduralColorHueGenerator.Next(0, 360));
            return ReturnColorHue;
        }

        //Returns the closest color hue that is available for use
        public Int32 GetClosestColorHueAvailable(Int32 InputColorHue)
        {
            Int32 CurrentSpreadValue = 0;
            while (CurrentSpreadValue <= 360)
            {
                //Outer color hue
                var OuterColorHue = InputColorHue - CurrentSpreadValue;
                if (OuterColorHue >= 0)
                {
                    var ProceduralMatches = from Proc in ProceduralList where Proc.ColorHue == OuterColorHue select Proc;
                    if (ProceduralMatches.Count() == 0)
                    {
                        return OuterColorHue;
                    }
                }

                //Inner color hue
                var InnerColorHue = InputColorHue + CurrentSpreadValue;
                if (InnerColorHue <= 360)
                {
                    var ProceduralMatches = from Proc in ProceduralList where Proc.ColorHue == InnerColorHue select Proc;
                    if (ProceduralMatches.Count() == 0)
                    {
                        return InnerColorHue;
                    }
                }

                CurrentSpreadValue += 1;
            }

            //If we have got this far we have used all our hue values up
            throw new SystemException("Failed to assign a unique hue to a procedural. This means that we have more than 360 procedurals in the set data. Which is too many.");
        }

        //Returns the procedural that matches the passed Hue
        public Procedural GetProcedural(Int32 ProceduralColorHue)
        {
            var ProceduralMatches = from Proc in ProceduralList
                                    where Proc.ColorHue == ProceduralColorHue
                                    select Proc;

            if (ProceduralMatches.Count() > 0)
            {
                return ProceduralMatches.First();
            }
            else
            {
                return null;
            }
        }

        //Gets the best procedural level from the passed UVW (ScRGB) values. Returns blank if no appropriate procedural exists.
        public Procedural.Level GetProceduralLevel(Double InputU, Double InputV, Double InputW)
        {
            var TargetColor = new Color();
            TargetColor.FromScRGB(InputU, InputV, InputW);

            //If we doesn't have any color saturation or color value then we can't logically get a Hue so
            //Return our BlankProceduralLevel
            if (TargetColor.S <= 0.0 || TargetColor.V <= 0.0)
            {
                return BlankProceduralLevel;
            }

            var ProceduralLevelMatches = from Proc in ProceduralList
                                         where Proc.ColorHue == Math.Round(TargetColor.H)
                                         select Proc.GetClosestLevelByColorValue(TargetColor.V);

            //If we found a match then return it. Otherwise return our blank procedural.
            if (ProceduralLevelMatches.Count() == 1 && ProceduralLevelMatches.First() != null)
            {
                return ProceduralLevelMatches.First();
            }
            else
            {
                return BlankProceduralLevel;
            }
        }

        //-----------------------------------------------------------------------------------------------
        #endregion

        //Generates a random hue for newly created procedurals (and assocaited levels)
        static Random ProceduralColorHueGenerator = new Random();

        //Writes out the passed SetFile to an .xml file
        public static void Write(String OutputFilePath, SetData InputSetFile)
        {
            XmlSerializer Serializer = new XmlSerializer(typeof(SetData));
            var WriterSettings = new XmlWriterSettings { Indent = true };

            using (var Writer = XmlWriter.Create(OutputFilePath, WriterSettings))
            {
                //Assign a new GUID for this save
                InputSetFile.Version = Guid.NewGuid();
                Serializer.Serialize(Writer, InputSetFile);
            }
        }

        //Reads in and creates a new SetFile from an .xml file
        public static SetData Read(String InputFilePath)
        {
            XmlSerializer Serializer = new XmlSerializer(typeof(SetData));
            var WriterSettings = new XmlWriterSettings { Indent = true };

            using (var Reader = XmlReader.Create(InputFilePath))
            {
                return (SetData)Serializer.Deserialize(Reader);
            }
        }
    }

    //Basic struct which makes passing face data between MXS and .net less verbose
    public class FaceData
    {
        public String ProceduralTag = "";
        public Double ScaleXYZ = 0.0;
        public Double ScaleZ = 0.0;
        public Double Density = 0.0;
        public Double GroundTintScR = 0.0;
        public Double GroundTintScG = 0.0;
        public Double GroundTintScB = 0.0;

        public FaceData() { }

        public override string ToString()
        {
            return String.Format("[{0}][ScaleXYZ - {1}][ScaleZ - {2}][Density - {3}][GroundTint - {4},{5},{6}]", ProceduralTag, ScaleXYZ, ScaleZ, Density, GroundTintScR, GroundTintScG, GroundTintScB);
        }
    }
}
