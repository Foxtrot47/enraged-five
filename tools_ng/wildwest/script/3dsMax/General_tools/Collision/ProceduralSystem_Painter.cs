using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;

namespace ProceduralSystem
{
    /// <summary>
    /// Encapulates and brings together many of the classes from the ProceduralPaint namespace 
    /// into a cohisive unit. An instance of this class can be bound to the UI xaml window and 
    /// all bindings will resolve from there.
    /// </summary>
    public class Painter : INotifyPropertyChanged
    {
        ///-----------------------------------------------------------------------------------------------
        #region FIELDS AND PROPERTIES
        ///-----------------------------------------------------------------------------------------------

        readonly String HelpWikiURL = @"https://devstar.rockstargames.com/wiki/index.php/Procedural_Painter";
        readonly String HelpMailURL = @"mailto:ross.george@rockstarlondon.com?subject=Procedural Painter Query...";

        public Display Display { get; private set; }
        public Render Render { get; private set; }
        public SetData SetData { get; private set; }
        public NodeCollection NodeCollection { get; private set; }
        public Metadata Metadata { get; private set; }
        public FrameworkElement BindedElement { get; private set; }
        public List<Object> SetList { get; private set; }
        public Set AppliedSet { get; private set; }
        public ObservableCollection<TransferCollisionTarget> TransferList { get; private set; }
        public ICollectionView TransferListView { get; private set; }
        public TransferCollisionTarget[] TransferArray
        {
            get
            {
                return TransferList.ToArray();
            }
        }
        public Boolean CanTransfer
        {
            get { return (TransferList.Count != 0); }
        }
        Boolean _CameraSyncState = false;
        public Boolean CameraSyncState
        {
            get
            {
                return _CameraSyncState;
            }
            set
            {
                _CameraSyncState = value;
                OnCameraSyncStateAltered();
                OnPropertyChanged("CameraSyncState");
            }
        }
        public Boolean AutoTransferAfterCommit { get; set; }
        public Boolean TransferGroundTint { get; set; }
        public Boolean UseGroundTintOverride { get; set; }

        Set _SelectedSet;
        public Set SelectedSet
        {
            get { return _SelectedSet; }
            set
            {
                _SelectedSet = value;
                OnPropertyChanged("SelectedSet");
            }
        }

        //Description of what action can be taken. Ties to CurrentlyEditing. Binded into the UI. 
        String _CurrentEditAction = "Start Editing";
        public String CurrentEditAction
        {
            get { return _CurrentEditAction; }
            set
            {
                _CurrentEditAction = value;
                OnPropertyChanged("CurrentEditAction");
            }
        }

        //Indicates whether we are currently editing an object
        Boolean _CurrentlyEditing = false;
        public Boolean CurrentlyEditing
        {
            get { return _CurrentlyEditing; }
            set
            {
                _CurrentlyEditing = value;
                if (value == true)
                {
                    CurrentEditAction = "Stop Editing";
                }
                else
                {
                    CurrentEditAction = "Start Editing";
                }
                OnPropertyChanged("CurrentlyEditing");
            }
        }

        #endregion
        ///-----------------------------------------------------------------------------------------------

        ///-----------------------------------------------------------------------------------------------
        #region CONSTRUCTORS
        ///-----------------------------------------------------------------------------------------------

        public Painter(String InputSetDataFilePath, String InputMetadataFilePath)
        {
            this.SetData = SetData.Read(InputSetDataFilePath);
            this.Metadata = Metadata.Read(InputMetadataFilePath);
            this.NodeCollection = new NodeCollection(this);
            this.Display = new Display();
            this.Render = new Render();
            this.TransferList = new ObservableCollection<TransferCollisionTarget>();
            this.TransferListView = CollectionViewSource.GetDefaultView(TransferList);
            this.TransferListView.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Descending));
            this.TransferGroundTint = true;
            this.AutoTransferAfterCommit = true;
            this.UseGroundTintOverride = false;

            //Monitor the display mode if it changes we raise the display mode changed event
            this.Display.PropertyChanged +=
                delegate(object sender, PropertyChangedEventArgs e)
                {
                    if (e.PropertyName == "CurrentMode")
                    {
                        OnDisplayModeChanged();
                    }
                };

            //Monitor the render mode if it changes we raise the render mode changed event
            this.Render.PropertyChanged +=
                delegate(object sender, PropertyChangedEventArgs e)
                {
                    if (e.PropertyName == "CurrentMode")
                    {
                        OnRenderModeChanged();
                    }
                };

            //Monitors the display color and opacity of all procedurals and raise RequestedRedrawProcedural event
            foreach (var Proc in SetData.ProceduralList)
            {
                Proc.PropertyChanged +=
                    delegate(object sender, PropertyChangedEventArgs e)
                    {
                        if (e.PropertyName == "DisplayOpacity")
                        {
                            var TargetProcedural = sender as Procedural;
                            OnRequestedRedrawProcedural(TargetProcedural);
                        }
                    };
            }

            //Set up our procedural list
            AppliedSet = new Set { Name = "Applied Procedurals" };
            SelectedSet = AppliedSet;

            SetList = new List<Object>();
            SetList.Add(AppliedSet);
            SetList.Add(new Separator());
            SetList.AddRange(SetData.SetList);
        }

        #endregion
        ///-----------------------------------------------------------------------------------------------

        ///-----------------------------------------------------------------------------------------------
        #region METHODS
        ///-----------------------------------------------------------------------------------------------

        //Setups up the passed window for use with this Painter instance. Sets up the window data 
        //context to this instance of Painter. Also uses attached events to monitor for clicks and 
        //changes on the UI. These events are then processed and forwarded on in a more consumable 
        //way to MXS via named events on core.
        public void BindElement(FrameworkElement InputTargetElement)
        {
            BindedElement = InputTargetElement;
            BindedElement.DataContext = this;

            BindedElement.AddHandler(Button.ClickEvent, new RoutedEventHandler(this.Button_ClickEvent));
            BindedElement.AddHandler(Grid.MouseEnterEvent, new RoutedEventHandler(this.Grid_MouseEnterEvent), true);
            BindedElement.AddHandler(Grid.MouseLeaveEvent, new RoutedEventHandler(this.Grid_MouseLeaveEvent), true);
			//BindedElement.AddHandler(Label.MouseUpEvent, new MouseButtonEventHandler(this.Label_MouseUpEvent), true);
        }

        //Attached event handlers.
        void Grid_MouseEnterEvent(Object sender, RoutedEventArgs e)
        {
            var TargetGrid = e.OriginalSource as Grid;

            switch (TargetGrid.Name)
            {
                case "Procedural_Level_Grid":
                    {
                        var TargetProceduralLevel = TargetGrid.DataContext as Procedural.Level;
                        OnRequestedStartHighlightProceduralLevel(TargetProceduralLevel);
                        break;
                    }
            }
        }

        void Grid_MouseLeaveEvent(Object sender, RoutedEventArgs e)
        {
            var TargetGrid = e.OriginalSource as Grid;

            switch (TargetGrid.Name)
            {
                case "Procedural_Level_Grid":
                    {
                        var TargetProceduralLevel = TargetGrid.DataContext as Procedural.Level;
                        OnRequestedStopHighlightProceduralLevel(TargetProceduralLevel);
                        break;
                    }
            }
        }
		
		void Label_MouseUpEvent(Object sender, MouseButtonEventArgs e)
		{	
			MessageBox.Show(sender.ToString());
			MessageBox.Show(e.OriginalSource.ToString());
            MessageBox.Show(e.Source.ToString());

            //var TargetLabel = e.OriginalSource as Label;
            //MessageBox.Show(TargetLabel.ToString());
			
			//For some reason we need to check that the original source is a Label when as far as I understood about attached events we should only ever get a Label. Bit weird.
			if( e.OriginalSource is Label)
			{
	            var TargetLabel = e.OriginalSource as Label;
				//Also check la
				//if( !String.IsNullOrEmpty( TargetLabel.Name ) )
				//{
					switch (TargetLabel.Name)
		            {
		                case "Collision_Mesh_Label":
		                    {
                                MessageBox.Show("bong");
                                var TransferCollisionTarget = TargetLabel.DataContext as TransferCollisionTarget;
								
								//Do something with collision target
                                //MessageBox.Show(TransferCollisionTarget.ToString());
								//OnRequestedSelectListItem(TargetLabel.Text);
                                //OnRequestedSelectVisualTargets();
		                        //OnRequestedStopHighlightProceduralLevel(TargetProceduralLevel);
		                        break;
		                    }
		            }
				//}
			}
		}
				

        void Button_ClickEvent(Object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource is Button)
            {
                var TargetButton = e.OriginalSource as Button;

                switch (TargetButton.Name)
                {
                    case "Banner_Mail_Button":
                        {
                            System.Diagnostics.Process.Start( this.HelpMailURL);
                            break;
                        }
                    case "Banner_Wiki_Button":
                        {
                            System.Diagnostics.Process.Start( this.HelpWikiURL );
                            break;
                        }
                    case "ProceduralLevel_Apply_Selection_Button":
                        {
                            var TargetProceduralLevel = TargetButton.DataContext as Procedural.Level;
                            OnApplyProceduralToSelection(TargetProceduralLevel);
                            break;
                        }
                    case "ProceduralLevel_Clear_Selection_Button":
                        {
                            OnApplyProceduralToSelection(SetData.BlankProceduralLevel);
                            break;
                        }
                    case "ProceduralLevel_Select_Button":
                        {
                            var TargetProceduralLevel = TargetButton.DataContext as Procedural.Level;
                            OnRequestedSelectFacesUsedByLevel(TargetProceduralLevel);
                            break;
                        }
                    case "Commit_Data_Button":
                        {
                            OnRequestedCommitData();
                            break;
                        }
                    case "Procedural_Remove_Button":
                        {
                            var TargetProcedural = TargetButton.DataContext as Procedural;
                            OnRequestedClearProcedural(TargetProcedural);
                            break;
                        }
                    case "Procedural_Select_Button":
                        {
                            var TargetProcedural = TargetButton.DataContext as Procedural;
                            OnRequestedSelectFacesUsedByProcedural(TargetProcedural);
                            break;
                        }
                    case "Import_Data_Button":
                        {
                            OnRequestedImportData();
                            break;
                        }
                    case "Export_Data_Button":
                        {
                            OnRequestedExportData();
                            break;
                        }
                    case "Transfer_Data_Button":
                        {
                            OnRequestedTransferData();
                            break;
                        }
                    case "Transfer_ProcMesh_Data_Button":
                        {
                            OnRequestedTransferDataProcMesh();
                            break;
                        }
                    case "Add_Collision_Button":
                        {
                            OnRequestedAddCollision();
                            break;
                        }
                    case "Remove_Collision_Button":
                        {
                            OnRequestedRemoveCollision();
                            break;
                        }
                    case "Update_Transfer_List_Button":
                        {
                            OnRequestedUpdateTransferList();
                            break;
                        }
                    case "Live_Update_Faces_Button":
                        {
                            OnRequestedLiveFaceUpdate();
                            break;
                        }
                    case "Procedural_Auto_Blend_Levels_Button":
                        {
                            OnRequestedAutoBlendFaceProceduralLevels();
                            break;
                        }
                    case "Procedural_Process_Spacing_Button":
                        {
                            OnRequestedProcessFaceSpacing();
                            break;
                        }
                    case "Import_Live_Button":
                        {
                            OnRequestedImportLiveData();
                            break;
                        }
					case "Select_Visual_Target_Button":
                        {
                            OnRequestedSelectVisualTargets();
                            break;
                        }
                    case "SelectNone_Visual_Target_Button":
                        {
                            OnRequestedSelectNoneVisualTargets();
                            break;
                        }
                    case "Focus_Visual_Target_Button":
                        {
                            OnRequestedFocusSelectedVisualTargets();
                            break;
                        }
                }
            }

            if (e.OriginalSource is ToggleButton)
            {
                var TargetToggleButton = e.OriginalSource as ToggleButton;

                //Push current state through the system again incase the operation cancels.
                CurrentlyEditing = CurrentlyEditing;

                switch (TargetToggleButton.Name)
                {
                    case "Edit_Action_Button":
                        {
                            //If we are currently editing then send a stop event else send a start event
                            if (CurrentlyEditing)
                            {
                                OnRequestedStopEdit();
                            }
                            else
                            {
                                OnRequestedStartEdit();
                            }
                            break;
                        }
                }
            }
        }

        //Updates the applied set with information from our NodeCollection
        public void UpdateAppliedSet()
        {
            var NewAppliedProceduralList = (from N in NodeCollection.NodeFaceDataStore select N.Value.ProceduralLevel.ParentProcedural).Distinct();

            //Remove any ones that have since been removed
            foreach (var OldAppliedProcedural in AppliedSet.ProceduralList.ToList())
            {
                if (!NewAppliedProceduralList.Contains(OldAppliedProcedural))
                {
                    AppliedSet.ProceduralList.Remove(OldAppliedProcedural);
                }
            }

            //Add any new ones
            foreach (var NewAppliedProcedural in NewAppliedProceduralList)
            {
                if (!AppliedSet.ProceduralList.Contains(NewAppliedProcedural))
                {
                    AppliedSet.ProceduralList.Add(NewAppliedProcedural);
                }
            }

            //If we have the blank procedural in the list then remove it.
            if (AppliedSet.ProceduralList.Contains(SetData.BlankProcedural))
            {
                AppliedSet.ProceduralList.Remove(SetData.BlankProcedural);
            }
        }

        //Iterating through each of the applied procedurals and update their applied procedural face counts
        //We call this after a batch of face updates.
        public void UpdateAppliedCounts()
        {
            foreach (var SubProcedural in SetData.ProceduralList)
            {
                foreach (var SubProceduralLevel in SubProcedural.LevelList)
                {
                    SubProceduralLevel.PushAppliedFaceCountPropertyUpdate();
                }
            }
        }

        //Clear applied counts
        public void ResetAppliedCounts()
        {
            foreach (var SubProcedural in SetData.ProceduralList)
            {
                foreach (var SubProceduralLevel in SubProcedural.LevelList)
                {
                    SubProceduralLevel.AppliedFaceCount = 0;
                    SubProceduralLevel.PushAppliedFaceCountPropertyUpdate();
                }
            }
        }

        //Resets the currently selected material
        public void ResetSelectedSet()
        {
            SelectedSet = AppliedSet;
        }

        public void TransferListClear()
        {
            TransferList.Clear();
        }

        public void TransferListAddCollision(Int32 CollisionMeshNodeHandle, String CollisionMeshName, Boolean CollisionMeshNeedsUpdating)
        {
            var CollisionMatches = (from CollisionMesh in TransferList where CollisionMesh.NodeHandle == CollisionMeshNodeHandle select CollisionMesh).ToArray();
            if (CollisionMatches.Count() > 0)
            {
                foreach (var CollisionMatch in CollisionMatches)
                {
                    CollisionMatch.Name = CollisionMeshName;
                    CollisionMatch.NeedsUpdating = CollisionMeshNeedsUpdating;
                }
            }
            else
            {
                TransferList.Add(new TransferCollisionTarget { NodeHandle = CollisionMeshNodeHandle, Name = CollisionMeshName, NeedsUpdating = CollisionMeshNeedsUpdating, SelectedForTransfer = CollisionMeshNeedsUpdating });
            }

            OnPropertyChanged("CanTransfer");
        }

        public void TransferListRemoveCollision(Int32 CollisionMeshNodeHandle)
        {
            var CollisionMatches = (from CollisionMesh in TransferList where CollisionMesh.NodeHandle == CollisionMeshNodeHandle select CollisionMesh).ToArray();
            if (CollisionMatches.Count() > 0)
            {
                foreach (var CollisionMatch in CollisionMatches)
                {
                    TransferList.Remove(CollisionMatch);
                }
            }

            OnPropertyChanged("CanTransfer");
        }
		
		/*
		public void TransferListSelectListItem()
		{
			return true;
		}
		*/

        #endregion
        ///-----------------------------------------------------------------------------------------------

        ///-----------------------------------------------------------------------------------------------
        #region EVENTS
        ///-----------------------------------------------------------------------------------------------

        //Event to signal that a property has changed. Used for data binding.
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        void OnPropertyChanged(String PropertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }

        //Event to signal the user has requested to apply a procedural level on selected polys
        public event EventHandler<ProceduralLevelActionEventArgs> RequestedApplyLevelToSelection = delegate { };
        void OnApplyProceduralToSelection(Procedural.Level InputTargetProceduralLevel)
        {
            RequestedApplyLevelToSelection(this, new ProceduralLevelActionEventArgs(InputTargetProceduralLevel));
        }

        //Event to signal the user has requested to apply a procedural level across all polys
        public event EventHandler<ProceduralLevelActionEventArgs> RequestedApplyLevelToAll = delegate { };
        void OnRequestedApplyLevelToAll(Procedural.Level InputTargetProceduralLevel)
        {
            RequestedApplyLevelToAll(this, new ProceduralLevelActionEventArgs(InputTargetProceduralLevel));
        }

        //Event to signal the user has requested to select the polys used by a procedural level
        public event EventHandler<ProceduralLevelActionEventArgs> RequestedSelectFacesUsedByLevel = delegate { };
        void OnRequestedSelectFacesUsedByLevel(Procedural.Level InputTargetProceduralLevel)
        {
            RequestedSelectFacesUsedByLevel(this, new ProceduralLevelActionEventArgs(InputTargetProceduralLevel));
        }

        //Event to signal the user has requested to start editing
        public event EventHandler RequestedStartEdit = delegate { };
        void OnRequestedStartEdit()
        {
            RequestedStartEdit(this, new EventArgs());
        }

        //Event to signal the user has requested to stop editing
        public event EventHandler RequestedStopEdit = delegate { };
        void OnRequestedStopEdit()
        {
            RequestedStopEdit(this, new EventArgs());
        }

        //Event to signal the user has requested to commit unsaved data
        public event EventHandler RequestedCommitData = delegate { };
        void OnRequestedCommitData()
        {
            RequestedCommitData(this, new EventArgs());
        }

        //Event to signal a change in display mode
        public event EventHandler DisplayModeChanged = delegate { };
        void OnDisplayModeChanged()
        {
            DisplayModeChanged(this, new EventArgs());
        }

        //Event to signal a change in render mode
        public event EventHandler RenderModeChanged = delegate { };
        void OnRenderModeChanged()
        {
            RenderModeChanged(this, new EventArgs());
        }

        //Event to signal the user has requested to do automated blending between procedural on the faces selected
        public event EventHandler RequestedAutoBlendFaceProceduralLevels = delegate { };
        void OnRequestedAutoBlendFaceProceduralLevels()
        {
            RequestedAutoBlendFaceProceduralLevels(this, new EventArgs());
        }

        //Event to signal the user has requested to process the spacing of the selected faces
        public event EventHandler RequestedProcessFaceSpacing = delegate { };
        void OnRequestedProcessFaceSpacing()
        {
            RequestedProcessFaceSpacing(this, new EventArgs());
        }

        //Event to signal the user has requested to select the polys used by a procedural
        public event EventHandler<ProceduralActionEventArgs> RequestedSelectFacesUsedByProcedural = delegate { };
        void OnRequestedSelectFacesUsedByProcedural(Procedural InputTargetProcedural)
        {
            RequestedSelectFacesUsedByProcedural(this, new ProceduralActionEventArgs(InputTargetProcedural));
        }

        //Event to signal the user has requested to clear a procedural
        public event EventHandler<ProceduralActionEventArgs> RequestedClearProcedural = delegate { };
        void OnRequestedClearProcedural(Procedural InputTargetProcedural)
        {
            RequestedClearProcedural(this, new ProceduralActionEventArgs(InputTargetProcedural));
        }

        //Event to signal that a redraw is needed on the faces the given procedural is applied to.
        public event EventHandler<ProceduralActionEventArgs> RequestedRedrawProcedural = delegate { };
        void OnRequestedRedrawProcedural(Procedural InputTargetProcedural)
        {
            RequestedRedrawProcedural(this, new ProceduralActionEventArgs(InputTargetProcedural));
        }

        //Event to signal that the user wants to attempt to import data from the previous tool.
        public event EventHandler RequestedImportData = delegate { };
        void OnRequestedImportData()
        {
            RequestedImportData(this, new EventArgs());
        }

        //Event to signal that the user wants to export data to the previous tool.
        public event EventHandler RequestedExportData = delegate { };
        void OnRequestedExportData()
        {
            RequestedExportData(this, new EventArgs());
        }

        //Event to signal that the user wants to transfer data to the selected collision objects.
        public event EventHandler RequestedTransferData = delegate { };
        void OnRequestedTransferData()
        {
            RequestedTransferData(this, new EventArgs());
        }

        //Event to signal that the user wants to transfer data to the selected collision objects.
        public event EventHandler RequestedTransferDataProcMesh = delegate { };
        void OnRequestedTransferDataProcMesh()
        {
            RequestedTransferDataProcMesh(this, new EventArgs());
        }

        //Event to signal that the user wants to add the selected collision mesh to the transfer list
        public event EventHandler RequestedAddCollision = delegate { };
        void OnRequestedAddCollision()
        {
            RequestedAddCollision(this, new EventArgs());
        }

        //Event to signal that the user wants to update the transfer list
        public event EventHandler RequestedUpdateTransferList = delegate { };
        void OnRequestedUpdateTransferList()
        {
            RequestedUpdateTransferList(this, new EventArgs());
        }

        //Event to signal that the user wants to live update update the currently selected face
        public event EventHandler RequestedLiveFaceUpdate = delegate { };
        void OnRequestedLiveFaceUpdate()
        {
            RequestedLiveFaceUpdate(this, new EventArgs());
        }

        //Event to signal that the user wants to remove the selected collision meshes from the transfer list
        public event EventHandler RequestedRemoveCollision = delegate { };
        void OnRequestedRemoveCollision()
        {
            RequestedRemoveCollision(this, new EventArgs());
        }

        //Event to signal that the user wants to select all collision targets
        public event EventHandler RequestedSelectAllCollision = delegate { };
        void OnRequestedSelectAllCollision()
        {
            RequestedSelectAllCollision(this, new EventArgs());
        }

        //Event to signal that the tool wants to start visually highlighting a given procedural level
        public event EventHandler<ProceduralLevelActionEventArgs> RequestedStartHighlightProceduralLevel = delegate { };
        void OnRequestedStartHighlightProceduralLevel(Procedural.Level InputProceduralLevel)
        {
            RequestedStartHighlightProceduralLevel(this, new ProceduralLevelActionEventArgs(InputProceduralLevel));
        }

        //Event to signal that the tool wants to stop visually highlighting a given procedural level
        public event EventHandler<ProceduralLevelActionEventArgs> RequestedStopHighlightProceduralLevel = delegate { };
        void OnRequestedStopHighlightProceduralLevel(Procedural.Level InputProceduralLevel)
        {
            RequestedStopHighlightProceduralLevel(this, new ProceduralLevelActionEventArgs(InputProceduralLevel));
        }

        //Event to signal that the tool either wants to start/stop the live syncing the game camera 
        public event EventHandler CameraSyncStateAltered = delegate { };
        void OnCameraSyncStateAltered()
        {
            CameraSyncStateAltered(this, new EventArgs() );
        }

        //Event to signal that the user wants to import live data from the game
        public event EventHandler RequestedImportLiveData = delegate { };
        void OnRequestedImportLiveData()
        {
            RequestedImportLiveData(this, new EventArgs());
        }
		
		//Event for when the user selects a list item in the visual meshes list
		
		public event EventHandler<VisualTargetActionEventArgs> RequestedSelectVisualTargets = delegate { };
		void OnRequestedSelectVisualTargets()
		{
			//MessageBox.Show("Selecting visual targets...");
			var visualTargetList = new List<TransferCollisionTarget>();
			foreach(var visualTarget in this.TransferList )
			{
				if( visualTarget.IsSelected )
					visualTargetList.Add(visualTarget);
			}
			RequestedSelectVisualTargets(this, new VisualTargetActionEventArgs( visualTargetList ) );
		}


        public event EventHandler<VisualTargetActionEventArgs> RequestedSelectNoneVisualTargets = delegate { };
        void OnRequestedSelectNoneVisualTargets()
        {
            //MessageBox.Show("Selecting visual targets...");
            var visualTargetList = new List<TransferCollisionTarget>();
            RequestedSelectNoneVisualTargets(this, new VisualTargetActionEventArgs(visualTargetList));
        }

        //select single list item
        public event EventHandler<VisualTargetActionEventArgs> RequestedFocusSelectedVisualTargets = delegate { };
        void OnRequestedFocusSelectedVisualTargets()
        {
            //MessageBox.Show("Focus_Visual_Target_Button");
            var visualTargetList = new List<TransferCollisionTarget>();
            foreach (var visualTarget in this.TransferList)
            {
                if (visualTarget.IsSelected)
                    visualTargetList.Add(visualTarget);
            }
            RequestedFocusSelectedVisualTargets(this, new VisualTargetActionEventArgs(visualTargetList));
        }
		

        #endregion
        ///-----------------------------------------------------------------------------------------------
    }

	//Event args for events related to visual targets 
	public class VisualTargetActionEventArgs : EventArgs
    {
        public List<TransferCollisionTarget> VisualTargetList { get; private set; }

        public VisualTargetActionEventArgs( List<TransferCollisionTarget> VisualTargetList )
        {
            this.VisualTargetList = VisualTargetList;
        }
    }
	
    //Event args for events related to procedurals levels
    public class ProceduralLevelActionEventArgs : EventArgs
    {
        public Procedural.Level TargetProceduralLevel { get; private set; }

        public ProceduralLevelActionEventArgs(Procedural.Level InputProceduralLevel)
        {
            TargetProceduralLevel = InputProceduralLevel;
        }
    }

    //Event args for events related to procedurals
    public class ProceduralActionEventArgs : EventArgs
    {
        public Procedural TargetProcedural { get; private set; }

        public ProceduralActionEventArgs(Procedural InputProcedural)
        {
            TargetProcedural = InputProcedural;
        }
    }

    //Class that allows manages the switching between opaque and transparent
    public enum RenderMode { Opaque, Transparent }
    public class Render : INotifyPropertyChanged
    {
        public RenderMode CurrentMode { get; private set; }

        Boolean _Opaque;
        public Boolean Opaque
        {
            get { return _Opaque; }
            set { SetRenderMode(RenderMode.Opaque); }
        }
        Boolean _Transparent;
        public Boolean Transparent
        {
            get { return _Transparent; }
            set { SetRenderMode(RenderMode.Transparent); }
        }

        void SetRenderMode(RenderMode InputMode)
        {
            _Opaque = false;
            _Transparent = false;

            switch (InputMode)
            {
                case RenderMode.Opaque:
                    {
                        _Opaque = true;
                        break;
                    }
                case RenderMode.Transparent:
                    {
                        _Transparent = true;
                        break;
                    }
            }

            OnPropertyChanged("Opaque");
            OnPropertyChanged("Transparent");

            CurrentMode = InputMode;
            OnPropertyChanged("CurrentMode");
        }

        public Render()
        {
            //Set transparent as our starting state.
            SetRenderMode(RenderMode.Transparent);
        }

        // Declare the PropertyChanged event
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
    }

    //Class that allows manages the switching between the various display modes available
    public enum DisplayMode { CombinedResult, IsolateDensity, IsolateScaleXYZ, IsolateScaleZ, IsolateProcedural, IsolateGroundTint };
    public class Display : INotifyPropertyChanged
    {
        public DisplayMode CurrentMode { get; private set; }

        Boolean _CombinedResult;
        public Boolean CombinedResult
        {
            get { return _CombinedResult; }
            set { SetDisplayMode(DisplayMode.CombinedResult); }
        }
        Boolean _IsolateDensity;
        public Boolean IsolateDensity
        {
            get { return _IsolateDensity; }
            set { SetDisplayMode(DisplayMode.IsolateDensity); }
        }
        Boolean _IsolateScaleXYZ;
        public Boolean IsolateScaleXYZ
        {
            get { return _IsolateScaleXYZ; }
            set { SetDisplayMode(DisplayMode.IsolateScaleXYZ); }
        }
        Boolean _IsolateScaleZ;
        public Boolean IsolateScaleZ
        {
            get { return _IsolateScaleZ; }
            set { SetDisplayMode(DisplayMode.IsolateScaleZ); }
        }
        Boolean _IsolateGroundTint;
        public Boolean IsolateGroundTint
        {
            get { return _IsolateGroundTint; }
            set { SetDisplayMode(DisplayMode.IsolateGroundTint); }
        }

        void SetDisplayMode(DisplayMode InputMode)
        {
            _CombinedResult = false;
            _IsolateDensity = false;
            _IsolateScaleXYZ = false;
            _IsolateScaleZ = false;
            _IsolateGroundTint = false;

            switch (InputMode)
            {
                case DisplayMode.CombinedResult:
                    {
                        _CombinedResult = true;
                        break;
                    }
                case DisplayMode.IsolateDensity:
                    {
                        _IsolateDensity = true;
                        break;
                    }
                case DisplayMode.IsolateScaleXYZ:
                    {
                        _IsolateScaleXYZ = true;
                        break;
                    }
                case DisplayMode.IsolateScaleZ:
                    {
                        _IsolateScaleZ = true;
                        break;
                    }
                case DisplayMode.IsolateGroundTint:
                    {
                        _IsolateGroundTint = true;
                        break;
                    }
            }

            OnPropertyChanged("IsolateGroundTint");
            OnPropertyChanged("IsolateScaleZ");
            OnPropertyChanged("IsolateScaleXYZ");
            OnPropertyChanged("IsolateDensity");
            OnPropertyChanged("CombinedResult");

            CurrentMode = InputMode;
            OnPropertyChanged("CurrentMode");
        }

        public Display()
        {
            //Set combined result as our starting state.
            SetDisplayMode(DisplayMode.CombinedResult);
        }

        // Declare the PropertyChanged event
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(String PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
    }

    public class TransferCollisionTarget
    {
        public String Name { get; set; }
        public Int32 NodeHandle { get; set; }
        public String ContainerName { get; set; }
        public Boolean NeedsUpdating { get; set; }
        public Boolean SelectedForTransfer { get; set; }
		public Boolean IsSelected { get; set; }
    }
}