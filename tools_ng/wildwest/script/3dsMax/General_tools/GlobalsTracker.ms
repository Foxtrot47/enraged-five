--Global Variable Remove


	struct GlobalsTrackerStruct
	(
		--///////////VARIABLES/////////////--
		
		MyGlobalSelection,
		OrderSwitch = False,

		--///////////FUNCTIONS/////////////--
		
		--Select the first item coresponding to the typed search
		fn  FN_FindMyGlobal MyGlobal  CurentListGlobalsAZ	=
		(				
			MyFoundArray = #()			
			ThePatern =  ((MyGlobal as string) + "*")			
			
			For GlobalName = 1 to CurentListGlobalsAZ.count do			
			(
				TheName = CurentListGlobalsAZ[GlobalName]				
				MyGlobal = matchPattern TheName  pattern: ThePatern ignoreCase:false
				
				--if you find it, call GlobalFind dialog and populate it with your new obtained array
				if MyGlobal == True then
				(					
					appendifunique  MyFoundArray  GlobalName							
				)else ()								
			)
			if MyFoundArray.count > 0 do
			(				
				--Select the first item of the array
				Sort MyFoundArray
				MyFirst = MyFoundArray[1]			
				GlobalsTracker.lst_GlobalList.selection = (MyFirst as integer)				
			)
		)
		
		
				
	)
	GlobalsTrkStruct = GlobalsTrackerStruct()


	Rollout GlobalsTracker "Globals Tracker"
	(
			--///////////VARIABLES/////////////--
		
		local MyGlobals = globalVars.gather()
		local MyGlobalsCount = (MyGlobals.count as string)
		
			--R* Banner
		dotNetControl rsBannerPanel "Panel" pos:[2,3] height:32 width:( GlobalsTracker.width - 5)	
		local banner = makeRsBanner dn_Panel:rsBannerPanel versionNum:0.01 versionName:"" wiki:"WIKI_NAME" filename:(getThisScriptFilename())

		
			--///////////BUTTONS/////////////--		
					
		
		Button btn_Reset "Reload" width:70 height:30 align:#right offset:[-65,0] toolTip: "Reset Bone List to initial state"
		Button btn_A_Z_Order "Sort"  width:70 height:30 align:#right offset:[10,-35] toolTip:"Sort Bones by alphabetical order"
		Button btn_Remove "Remove" width:110 height:30 align:#right offset:[-8,0] toolTip:"Sort Bones by alphabetical order"
		edittext GlobalsFinder "Find"  fieldWidth: 110 align:#right offset:[5,0] toolTip: "Remove Facial Bones from the ListBox"
		hyperLink GlobalsCountText  "Globals Running :"	color:[248,148,29] offset:[-5,0] 		
		hyperLink GlobalsCount  MyGlobalsCount	color:[248,148,29] offset:[80,-20] 	
		listbox lst_GlobalList width: 143   height: 20 offset:[-9,0] items: MyGlobals 	
		
		
				--///////////EVENTS/////////////--
		
			on GlobalsTracker open do
			(
				banner.setup()
				GlobalsCountText .enabled = false 
				GlobalsCount.enabled = false 		
			)
		
			
			on btn_Reset pressed do 
			(
				MyGlobals = globalVars.gather()
				lst_GlobalList.items = MyGlobals
				MyGlobalsCount = (MyGlobals.count as string)
				GlobalsCount.caption = MyGlobalsCount
				GlobalsTrkStruct.OrderSwitch = False	
			)			

			on btn_A_Z_Order pressed do
			(		
				--Sort ListBox Aray by alphabetical order
				CurentListGlobals = GlobalsTracker.lst_GlobalList.items					
				CurentListGlobalsAZ = sort CurentListGlobals			
				GlobalsTracker.lst_GlobalList.items = CurentListGlobalsAZ
				--keep a trace of the change
				GlobalsTrkStruct.OrderSwitch = True				
			)	
			
			on lst_GlobalList selected Myselection do 
			(
				print "hello"
				print Myselection
				print MyGlobals[Myselection]
				GlobalsTrkStruct.MyGlobalSelection = MyGlobals[Myselection] 	
			)
			
			
			on btn_Remove pressed do
			(				
				globalVars.remove GlobalsTrkStruct.MyGlobalSelection 
				
				--update the text count
				MyGlobals = globalVars.gather()
				MyGlobalsCount = (MyGlobals.count as string)
				GlobalsCount.caption = MyGlobalsCount
				
				--refresh listbox (keep the alphabetical sorting if it have been done)
				if GlobalsTrkStruct.OrderSwitch == True do
				(
					MyGlobals = globalVars.gather()
					sort MyGlobals
					lst_GlobalList.items = MyGlobals
				)	

				if GlobalsTrkStruct.OrderSwitch == False do
				(
					MyGlobals = globalVars.gather()					
					lst_GlobalList.items = MyGlobals
				)				
			)
			
			on GlobalsTracker resized TheSize do 
			(										
				GlobalsTrackerWidth = 150
				MyNewGlobalsTrackerWidth = TheSize[1]
				MyWidthOffset = (MyNewGlobalsTrackerWidth / GlobalsTrackerWidth)					
				GlobalsTracker.lst_GlobalList.width = (MyWidthOffset * GlobalsTrackerWidth )
				
				GlobalsTrackerHeight = 425
				MyNewGlobalsTrackerHeight = TheSize[2]
				MyHeightOffset = (MyNewGlobalsTrackerHeight / GlobalsTrackerHeight)								
				GlobalsTracker.lst_GlobalList.height = (GlobalsTrackerWidth * (MyHeightOffset*(2.75)-1) )

		)	




			on GlobalsFinder entered MyGlobal do 
			(	
				--Sort ListBox Aray by alphabetical order
				CurentListGlobals = GlobalsTracker.lst_GlobalList.items					
				CurentListGlobalsAZ = sort CurentListGlobals			
				GlobalsTracker.lst_GlobalList.items = CurentListGlobalsAZ	
				GlobalsTrkStruct.FN_FindMyGlobal MyGlobal CurentListGlobalsAZ
				GlobalsTrkStruct.OrderSwitch = True						
			)			
		
		
	)
	createdialog GlobalsTracker  150 425  style:#(#style_titlebar, #style_border, #style_sysmenu, #style_resizing)	