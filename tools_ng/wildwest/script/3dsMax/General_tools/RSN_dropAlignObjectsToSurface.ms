filein  (RsConfigGetWildWestDir()+"/script/3dsMax/_common_functions/FN_common.ms" )

fn RSN_g_filter o = superclassof o == Geometryclass

fn RSN_dropAlignObjectsToSurface =
(
	target_mesh = pickObject message:"Pick Target Surface:" filter:RSN_g_filter

	if isValidNode target_mesh then
	(
		undo "MoveToSurface" on
		(
			for i in selection do
			(
				int_point = find_Z_intersection target_mesh i	--find_Z_intersection() in FN_Common.ms
				--format "\n %" int_point
				
				if int_point != undefined then
				(
					i.pos = int_point.pos
					i.dir = int_point.dir
				)

			)--end i loop
		)--end undo
	)--end if
)

RSN_dropAlignObjectsToSurface()