global WebBrowser

struct simpleBrowser
(
	maxTemp = GetDir #temp,
	browser = dotNetObject "System.Windows.Forms.WebBrowser",
	form = dotNetObject "maxCustomControls.maxForm",
	URLtextBox = dotNetObject "System.Windows.Forms.TextBox",
	backButton,
	forwardButton,
	searchTextBox,
	--url = "http://download.autodesk.com/us/3dsmax/2012help/index.html",
	url = "http://docs.autodesk.com/f3DSMAX/14/ENU/MAXScript%20Help%202012/",
	
	--config
	browserFavourites = #( "http://download.autodesk.com/us/3dsmax/2012help/index.html",
						"http://docs.autodesk.com/3DSMAX/14/ENU/MAXScript%20Help%202012/",
						"https://devstar.rockstargames.com/wiki/index.php/Main_Page"),
						
	fn URLChanged s e = 
	(
		if e.KeyChar == "\r" then
		(
			uriCheck = dotnetclass "System.Net.WebRequest"
			 
			try
			(
				valid = uriCheck.create s.text
			)
			catch()
			if valid != undefined then
			(
				WebBrowser.url = s.text
				WebBrowser.browser.url = dotNetObject "System.Uri" WebBrowser.url
			)
		)
		
	),
	
	fn addFavourite s e =
	(
		--save the curent url to favourites
		
	
	),
	
	fn recallFavourite s e =
	(
		--get the combobox selection index
		idx = s.SelectedIndex
		WebBrowser.url = s.Items.Item[idx]
		WebBrowser.URLtextBox.text = WebBrowser.url
		WebBrowser.browser.url = dotNetObject "System.Uri" WebBrowser.url
	),
	
	fn browserGoBack s e =
	(
		WebBrowser.browser.GoBack()
	),
	
	fn browserGoForward s e =
	(
		WebBrowser.browser.GoForward()
	),
	
	fn searchEntered s e =
	(
		if e.KeyChar == "\r" then
		(
			query = s.text
			WebBrowser.url = "http://www.google.co.uk/search?q="+s.text
			WebBrowser.browser.url = dotNetObject "System.Uri" WebBrowser.url
		)
	),
	
	--WebBrowser shutdown event
	fn formShutDown s e = 
	(
		print "WebBrowser:shutdown"
		gameLauncher = undefined
		gc light:true
	),
	
	
	
	fn createUI = 
	(
		--Set the parent of the form to be Max. 
		--Get the max handle pointer.
		maxHandlePointer=(Windows.GetMAXHWND())

		--Convert the HWND handle of Max to a dotNet system pointer
		sysPointer = DotNetObject "System.IntPtr" maxHandlePointer

		--Create a dotNet wrapper containing the maxHWND
		maxHwnd = DotNetObject "MaxCustomControls.Win32HandleWrapper" sysPointer
		
		dock = dotNetClass "System.Windows.Forms.DockStyle"
		columnStyle1 = dotNetObject "System.Windows.Forms.ColumnStyle"
		columnStyle2 = dotNetObject "System.Windows.Forms.ColumnStyle"
		columnStyle3 = dotNetObject "System.Windows.Forms.ColumnStyle"
		columnStyle4 = dotNetObject "System.Windows.Forms.ColumnStyle"
		columnStyle5 = dotNetObject "System.Windows.Forms.ColumnStyle"
		rowStyle1 = dotNetObject "System.Windows.Forms.RowStyle"
		rowStyle2 = dotNetObject "System.Windows.Forms.RowStyle"
		sizeType = dotNetClass "System.Windows.Forms.SizeType"
		--size = dotNetClass "System.Drawing.Size"
		--location = dotNetClass "System.Drawing.Point"
		
		--form = dotNetObject "System.Windows.Forms.Form"
		--form = dotNetObject "maxCustomControls.maxForm"
		form.size = dotNetObject "System.Drawing.Size" 1200 1000
		
		panel = dotNetObject "System.Windows.Forms.Panel"
		--panel.suspendLayout()
		
		tableLayoutPanel = dotNetObject "System.Windows.Forms.TableLayoutPanel"
		--size = tableLayoutPanel.size
		--size.width = 600
		--size.height = 66
		tableLayoutPanel.size =  dotNetObject "System.Drawing.Size" 600 66
		
		--URLtextBox = dotNetObject "System.Windows.Forms.TextBox"
		URLtextBox.text = url
		URLtextBox.dock = dock.Fill
		URLtextBox.size = dotNetObject "System.Drawing.Size" 20 400
		URLtextBox.location = dotNetObject "System.Drawing.Point" 3 3
		URLtextBox.tabindex = 0
		dotNet.addEventHandler URLtextBox "KeyPress" URLChanged
		
		addFavouriteButton = dotNetObject "System.Windows.Forms.Button"
		addFavouriteButton.text = "+"
		dotNet.addEventHandler addFavouriteButton "Click" addFavourite
		
		backButton = dotNetObject "System.Windows.Forms.Button"
		backButton.text = "<"
		dotNet.addEventHandler backButton "Click" browserGoBack
		
		forwardButton = dotNetObject "System.Windows.Forms.Button"
		forwardButton.text = ">"
		dotNet.addEventHandler forwardButton "Click" browserGoForward
		
		favouritesComboBox = dotNetObject "System.Windows.Forms.ComboBox"
		favouritesComboBox.size = dotNetObject "System.Drawing.Size" 400 20
		
		--Add favourites
		favouritesComboBox.Items.AddRange browserFavourites
		dotNet.addEventHandler favouritesComboBox "SelectedIndexChanged" recallFavourite
		
		searchTextBox = dotNetObject "System.Windows.Forms.TextBox"
		searchTextBox.text = "Search"
		dotNet.addEventHandler searchTextBox "KeyPress" searchEntered
			
		--browser = dotNetObject "System.Windows.Forms.WebBrowser"
		browser.dock = dock.Fill
		browser.url = dotNetObject "System.Uri" url
		--browser.location = dotNetObject "System.Drawing.Point" 80 180
		browser.autoSize = true
		
        --tableLayoutPanel1 
		tableLayoutPanel.ColumnCount = 5
		--sizeType.value__ = 70
		columnStyle1.sizeType = sizeType.Percent
		columnStyle1.width = 70
		tableLayoutPanel.ColumnStyles.Add(columnStyle1)
		columnStyle2.sizeType = sizeType.Absolute
		columnStyle2.width = 36
		tableLayoutPanel.ColumnStyles.Add(columnStyle2) --36
		columnStyle3.sizeType = sizeType.Absolute
		columnStyle3.width = 36
		tableLayoutPanel.ColumnStyles.Add(columnStyle3)	--36
		columnStyle4.sizeType = sizeType.Absolute
		columnStyle4.width = 36
		tableLayoutPanel.ColumnStyles.Add(columnStyle4) --36
		columnStyle5.sizeType = sizeType.Percent
		columnStyle5.width = 30
		tableLayoutPanel.ColumnStyles.Add(columnStyle5) --30
		
		tableLayoutPanel.Controls.Add URLtextBox 0 0
		tableLayoutPanel.Controls.Add addFavouriteButton 1 0
		tableLayoutPanel.Controls.Add backButton 2 0
		tableLayoutPanel.Controls.Add favouritesComboBox 0 1
		tableLayoutPanel.Controls.Add forwardButton 3 0
		tableLayoutPanel.Controls.Add searchTextBox 4 0
		
		--tableLayoutPanel.Location = new System.Drawing.Point(0, 0)
		--tableLayoutPanel.Name = "tableLayoutPanel1"
		tableLayoutPanel.RowCount = 2
		rowStyle1.sizeType = sizeType.Percent
		rowStyle1.height = 50
		tableLayoutPanel.RowStyles.Add(rowStyle1)
		rowStyle2.sizeType = sizeType.Percent
		rowStyle2.height = 50
		tableLayoutPanel.RowStyles.Add(rowStyle2)
		--tableLayoutPanel.Size = new System.Drawing.Size(682, 66)
		tableLayoutPanel.TabIndex = 0
			
		tableLayoutPanel.dock = dock.Top
		tableLayoutPanel.resumelayout false
		--tableLayoutPanel.controls.add(browser)
		
		--panel1
		panel.Controls.Add(browser)
		panel.Controls.Add(tableLayoutPanel)
		panel.Dock = dock.Fill
		
		panel.TabIndex = 0
		panel.resumeLayout()
		
		
		--form.Opacity=0.01
		form.controls.add(panel)
		form.resumeLayout false
		form.show(maxHwnd)
		dotNet.addEventHandler form "FormClosed" formShutDown
	)
	
	
)

if WebBrowser != undefined then
(
	try
	(
		WebBrowser.form.close()
	)
	catch()
)

WebBrowser = simpleBrowser()
WebBrowser.createUI()