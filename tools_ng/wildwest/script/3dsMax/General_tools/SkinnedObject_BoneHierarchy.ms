FileIn "SkinnedObject_Bone.ms"
FileIn "SkinModifier.ms"

/*---------------------------------------------------
Stores all the information about how an object is skinned. Including the bones and their weights.
*/---------------------------------------------------
struct SkinnedObject_BoneHierarchy
(
	BoneArray = #(),
		
	/*---------------------------------------------------
	Copys the bone. Returns the copy.
	*/---------------------------------------------------
	fn CopyBone InputBone InputVertIDOffset =
	(
		NewBone = InputBone.Clone VertIDOffset:InputVertIDOffset 
		Append BoneArray NewBone
		return NewBone
	),
	
	/*---------------------------------------------------
	Takes a geometry object as input and creates a bone hierarchy with only one bone which is weighted to affect all of the passed geometry object. Returns a clone of the mesh which the bone hierarchy has been mapped to.
	*/---------------------------------------------------
	fn CreateFromAnimatedGeometry InputGeometryNode =
	(	
		--Copy the mesh so we can start going through each of the verts. This is also the mesh we will return.
		InputGeometryClone = RSMesh_Snapshot InputGeometryNode
		
		--Create a new bone which uses the InputGeometryNode as it's source
		NewBone = SkinnedObject_Bone()
		NewBone.CreateFromNode( InputGeometryNode )
				
		--Create weight table which has every vert set to 1.0 (full effect)
		VertexCount = (GetPolygonCount InputGeometryClone)[2]
		WeightTable = #()
		for VertID = 1 to VertexCount do Append WeightTable 1.0
		
		--Map the weight table over to the bone
		NewBone.MapWeightTable WeightTable
		
		--Add the bone to our bone hierarchy
		Append BoneArray NewBone
		
		--Return the cloned mesh
		return InputGeometryClone
	),
	
	/*---------------------------------------------------
	Takes a geometry object which has skinning as input and creates a bone hierarchy from it. Returns a clone of the mesh which the bone hierarchy has been mapped to.
	*/---------------------------------------------------
	fn CreateFromSkinnedGeometry InputGeometryNode =
	(
		--Copy the mesh so we can start going through each of the verts. This is also the mesh we will return.
		InputGeometryClone = RSMesh_Snapshot InputGeometryNode
		
		--Get the skin modifer from the input object
		InputSkinModifier = SkinModifier()
		InputSkinModifier.ExtractSkinModifier( InputGeometryNode )
		
		InputSkinModifier.StartEdit()
		
		--Go through each bone cloning it and copying the weights across
		for BoneID = 1 to InputSkinModifier.BoneCount() do
		(
			--Get the actual bone node 
			BoneNode = InputSkinModifier.GetBoneNode BoneID
			
			--Create a new bone from it
			NewBone = SkinnedObject_Bone()
			NewBone.CreateFromNode BoneNode
			
			--Map the weights across
			WeightTable = InputSkinModifier.GetWeightTable BoneID
			NewBone.MapWeightTable WeightTable
			
			--If the bone has some weights then add it to the heirarchy
			if( NewBone.VertexWeightTable.Count > 0 ) then
			(
				--Add the bone to our bone hierarchy
				Append BoneArray NewBone
			)
			else
			(
				--If no weight information was found then dispose of it
				NewBone.Destroy()
			)
		)
	 
		InputSkinModifier.EndEdit()
		
		--Return a copy of the mesh
		return InputGeometryClone
	),

	/*---------------------------------------------------
	Removes the bones from the hierarchy
	*/---------------------------------------------------
	fn RemoveBones InputBoneArray =
	(
		for SubBone in InputBoneArray do This.RemoveBone SubBone
	),
	
	/*---------------------------------------------------
	Removes the bone from the hierarchy
	*/---------------------------------------------------
	fn RemoveBone InputBone =
	(
		InputBone.Destroy()
		InputBoneID = FindItem BoneArray InputBone
		DeleteItem BoneArray InputBoneID
	),
	
	/*---------------------------------------------------
	Processes each of the bone 
	*/---------------------------------------------------
	fn ReorderBoneVertIDs VertIDRemapTable =
	(
		for SubBone in This.BoneArray do
		(
			SubBone.ReorderVertIDs VertIDRemapTable
		)
	),
	
	/*---------------------------------------------------
	Destroys each of the bones in the hierarchy and related data ready for GC
	*/---------------------------------------------------
	fn Destroy =
	(
		for SubBone in This.BoneArray do
		(
			SubBone.Destroy()
		)
		
		This.BoneArray = #()
	)
)