fn activeMaterialToggle =
(
	if RSN_ActiveMaterialActive == false or RSN_ActiveMaterialActive == undefined then
	(
		callbacks.addScript #selectionSetChanged "if (isKindOf $ GeometryClass) == true and $.material != undefined then (meditMaterials[1]=$.material; medit.SetActiveMtlSlot 1 true)" id:#matUpdate
		global RSN_ActiveMaterialActive = true
	)
	else if RSN_ActiveMaterialActive == true then
	(
		callbacks.removeScripts id:#matUpdate
		global RSN_ActiveMaterialActive = false
	)
)

activeMaterialToggle()