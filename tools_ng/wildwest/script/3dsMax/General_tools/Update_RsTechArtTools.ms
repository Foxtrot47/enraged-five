(
	--connect to perforce if we need to
	if gRsPerforce.connected() == false then gRsPerforce.connect()
	
	--get the latest wildwest tools and the max menus
	gRsPerforce.sync #((RsConfigGetToolsDir() + "wildwest/..."))
)