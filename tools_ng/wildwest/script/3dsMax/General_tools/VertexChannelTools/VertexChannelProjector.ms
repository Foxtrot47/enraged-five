filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/fn_vertexpaint.ms")

--/////////////////////////////////////////
--	UI
--/////////////////////////////////////////
try( DestroyDialog VertexChannelProjectorUI )catch()
rollout VertexChannelProjectorUI "Vertex Channel Projector" width:250 height:205
(
	--/////////////////////////////////////////
	--	VARIABLES
	--/////////////////////////////////////////
	local sourceObj = undefined
	local targetObj = undefined
	
	--/////////////////////////////////////////
	--	CONTROLS
	--/////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:250
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Vertex Channel Projector" filename:(getThisScriptFilename())
		
	group "Source Object"
	(
		label lblSourceObject "Source:" across:3 align:#left
		edittext edtSourceObject readOnly:true text:"undefined" width:150 offset:[-40, 0]
		button btnSourceObject "Pick" align:#right
		label lblSourceChannel "Channel: " across:2 align:#left
		spinner spnSourceChannel range:[-2, 99, 0] type:#integer align:#left offset:[-70, 0]
	)
	
	group "Target Object"
	(
		label lblTargetObject "Target:" across:3 align:#left
		edittext edtTargetObject readOnly:true text:"undefined" width:150 offset:[-40, 0]
		button btnTargetObject "Pick" align:#right
		label lblTargetChannel "Channel: " across:2 align:#left
		spinner spnTargetChannel range:[-2, 99, 0] type:#integer align:#left offset:[-70, 0]
	)
	
	button btnProject "GO" width:240
	
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////
	--fn discriminator obj = GetAttrClass obj == "Gta Object"
		
	fn objectPicker mode:#source =
	(
-- 		local message = ""
-- 		case mode of
-- 		(
-- 			#source:message = "Pick a Source object"
-- 			#target:message = "Pick a Target object"
-- 		)
-- 		
		local userSelection = selectByName()
		if userSelection == undefined then return false
			
		local obj = userSelection[1]
		if obj != undefined then
		(
			case mode of
			(
				#source:
				(
					sourceObj = obj
					edtSourceObject.text = sourceObj.name
				)
				#target:
				(
					targetObj = obj
					edtTargetObject.text = targetObj.name
				)
			)
		)
	)
	
	
	--/////////////////////////////////////////
	--	EVENTS
	--/////////////////////////////////////////
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btnSourceObject pressed do
	(
		objectPicker mode:#source
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btnTargetObject pressed do
	(
		objectPicker mode:#target
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btnProject pressed do
	(
		if sourceObj == undefined or targetObj == undefined then
		(
			messageBox "Not all objects necessary are selected\nCheck your selection." title:"Error"
			return false
		)
		
		--check for stacks. doesnt work with stacks currently
		if sourceObj.modifiers.count != 0 or targetObj.modifiers.count != 0 then
		(
			messageBox "One of the objects has modifiers in its stack\nThe tool doesnt currently support modifier stacks, sorry." title:"Invalid Selection"
			return false
		)
		
		--check the source map channel exists
		local sourceMapSupport = false
		case (classOf sourceObj) of
		(
			Editable_mesh:sourceMapSupport = meshop.getMapSupport sourceObj spnSourceChannel.value
			Editable_Poly:sourceMapSupport = polyop.getMapSupport sourceObj spnSourceChannel.value
			PolyMeshObject:sourceMapSupport = polyop.getMapSupport sourceObj spnSourceChannel.value
		)
		
		if (not sourceMapSupport) then
		(
			messageBox "Map Channel doesnt exist on source object" title:"Error"
			return false
		)
		
		--do the doo
		RsProjectVertexMap sourceObj targetObj SRCchannel:spnSourceChannel.value TGTchannel:spnTargetChannel.value
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on VertexChannelProjectorUI open do
	(
		banner.setup()
	)
)

CreateDialog VertexChannelProjectorUI style:#(#style_titlebar, #style_sysmenu, #style_minimizebox)

