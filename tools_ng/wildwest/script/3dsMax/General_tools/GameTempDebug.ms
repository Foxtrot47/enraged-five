filein ( RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms" )

global RsTempDebugRollout
global gRsTempDebug

try ( destroyDialog RsTempDebugRollout ) catch ()

struct RsTempDebugInfo
(
	playerPos 	= Point3 0 0 0,
	menuWarp 	= Point3 0 0 0,
	cameraPos 	= Point3 0 0 0,
	cameraFov 	= 0.0
)

struct RsTempDebug
(
	debugFile = RsConfigGetProjRootDir() + "build/dev/temp_debug.txt",
	debugInfos = #(),
	
	-- String id's
	strPlayerPos		= "Player Position",
	strActualZCoords	= "Actual Z coords",
	strDebugMenuWarp	= "Debug Menu Warp",
	strCameraPos		= "Camera Position",
	strCameraFov		= "Camera FOV",
	
	fn load = (
		if ( doesFileExist this.debugFile ) then (
			this.debugInfos = #()
			
			local fstream = openFile this.debugFile mode:"r"
			
			local debugInfo = undefined
			
			while ( not eof fstream ) do (
				local currentLine = readLine fstream
				currentLine = trimRight currentLine " \n\r"
				currentLine = trimLeft currentLine " \t"
				
				if currentLine != "" then (
					local rawData = filterString currentLine "="
					local id = trimRight rawData[ 1 ] " "
					
					if id == this.strPlayerPos then (
						
						-- Add the previous debug info object before we create a new one.
						if debugInfo != undefined then (
							append this.debugInfos debugInfo
						)
						
						debugInfo = RsTempDebugInfo()
						
						local posStart = findString rawData[ 2 ] "<<"
						local posEnd = findString rawData[ 2 ] ">>"
						
						local rawPosStr = substring rawData[ 2 ] ( posStart + 3 ) ( posEnd - 6 )
						local rawPosData = filterString rawPosStr ", "
						
						debugInfo.playerPos.x = rawPosData[ 1 ] as float
						debugInfo.playerPos.y = rawPosData[ 2 ] as float
						debugInfo.playerPos.z = rawPosData[ 3 ] as float
					)
				)
			)
			
			-- Add the last one.
			append this.debugInfos debugInfo
			
			close fstream
			
		) else (
			local msg = "Could not find the file (" + this.debugFile + ").  To generate the file, press the 'C' key while the game is running."
			messageBox msg title:"Rockstar"
		)
	),
	
	fn clearDebugFile = (
		if ( doesFileExist this.debugFile ) then (
			local fstream = createFile this.debugFile
			close fstream
			
			this.debugInfos = #()
		)
	),
	
	fn createObjectAtPlayerPos createObj:true createPath:false = (
		local debugPath = undefined
		local parentObj = undefined
		
		if createPath == true then (
			debugPath = SplineShape()
			debugPath.name = uniqueName "DebugPath"
			debugPath.wirecolor = Color 0 0 255
			
			addNewSpline debugPath
		)
		
		for idx = 1 to this.debugInfos.count do (
			local debugInfo = this.debugInfos[ idx ]
			
			if debugInfo != undefined do (
				
				
				if createObj == true then (
					local debugName = uniqueName "DebugPlayerPos"
					local debugObj = Box name:debugName pos:debugInfo.playerPos
					debugObj.length = 0.4
					debugObj.width = 1.0
					debugObj.height = 1.8
					
					if idx == 1 then (
						parentObj = debugObj
						debugObj.wirecolor = Color 0 255 0
						
					) else if idx == this.debugInfos.count and idx != 1 then (
						debugObj.wirecolor = Color 255 0 0
						debugObj.parent = parentObj
						
					) else (
						debugObj.wirecolor = Color 0 255 255
						debugObj.parent = parentObj
					)
				)
				
				if debugPath != undefined do (
					addKnot debugPath 1 #corner #line debugInfo.playerPos
				)
			)
		)
		
		if debugPath != undefined do (
			try ( updateShape debugPath ) catch ()
			
			if parentObj != undefined do (
				debugPath.parent = parentObj
			)
		)
		
		forceCompleteRedraw()
	)
)

gRsTempDebug = RsTempDebug()
	

rollout RsTempDebugRollout "Game Temp Debug"
(
	dotNetControl rsBannerPanel "Panel" pos:[ 0, 0 ] height:32 width:RsTempDebugRollout.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Game Temp Debug" filename:( getThisScriptFilename() )
	
	group "Create"
	(
		checkbox cbCreatePath "Path" checked:true
		checkbox cbCreatePlayerPos "Player Position" checked:true
	)
	
	group "Temp Debug File"
	(
		edittext etDebugFile "" labelOnTop:true width:172 across:2
		button btnBrowseDebugFile "..." offset:[ 42, -2 ] width:20
		button btnReloadDebugFile "Reload Debug File" across:2
		button btnClearDebugFile "Clear Debug File" align:#right offset:[ 3, 0 ]
	)
	
	button btnCreate "Create" width:210 height:32
	checkbutton cbLiveUpdate "Live Update" width:210 height:32
	
	timer liveUpdateTimer interval:1000 active:false
	
	fn zoomToDebugObjs = (
		local objs = for obj in $Debug* collect obj
			
		local objsMinX = #()
		local objsMinY = #()
		local objsMinZ = #()
		
		local objsMaxX = #()
		local objsMaxY = #()
		local objsMaxZ = #()
		
		for obj in objs do (
			append objsMinX obj.min.x
			append objsMinY obj.min.y
			append objsMinZ obj.min.z
			
			append objsMaxX obj.max.x
			append objsMaxY obj.max.y
			append objsMaxZ obj.max.z
		)
		
		local bboxMin = Point3 ( amin objsMinX ) ( amin objsMinY ) ( amin objsMinZ )
		local bboxMax = Point3 ( amax objsMaxX ) ( amax objsMaxY ) ( amax objsMaxZ )
		
		viewport.ZoomToBounds true bboxMin bboxMax
	)
	
	on liveUpdateTimer tick do (
		delete $DebugPath*
		delete $DebugPlayerPos*
		
		gRsTempDebug.load()
		gRsTempDebug.createObjectAtPlayerPos createObj:cbCreatePlayerPos.checked createPath:cbCreatePath.checked
		
		zoomToDebugObjs()
	)
	
	on cbLiveUpdate changed state do (
		if state == true then (
			liveUpdateTimer.active = true
			
		) else (
			liveUpdateTimer.active = false
		)
	)
	
	on btnCreate pressed do (
		gRsTempDebug.createObjectAtPlayerPos createObj:cbCreatePlayerPos.checked createPath:cbCreatePath.checked
	)
	
	on btnReloadDebugFile pressed do (
		gRsTempDebug.load()
	)
	
	on btnBrowseDebugFile pressed do (
		local result = getOpenFilename filename:( RsConfigGetProjRootDir() + "build/dev/temp_debug.txt" ) types:"Text(*.txt)|*.txt"
		
		if result != undefined do (
			etDebugFile.text = result
			gRsTempDebug.debugFile = result
			gRsTempDebug.load()
		)
	)
	
	on btnClearDebugFile pressed do (
		if queryBox ( "This will clear all data in the following file!\n\n" + gRsTempDebug.debugFile + "\n\nAre you sure?" ) title:"Rockstar" beep:yes then (
			gRsTempDebug.clearDebugFile()
		)
	)
	
	on RsTempDebugRollout open do (
		banner.setup()
		
		etDebugFile.text = gRsTempDebug.debugFile
		gRsTempDebug.load()
	)
)

createDialog RsTempDebugRollout width:220 height:252
