--VertexColorTools.ms
--2013 Andy Davis
--Rockstar London
--Collection of Vertex color Tools

RSTA_LoadCommonFunction #("fn_RSTA_vertexColours.ms")

--UI Xaml Markup	
VertexColorToolXAML = @"X:\gta5\tools\wildwest\script\3dsMax\UI\VertexColorToolsWindow.xaml"

--Custom dotNet Class
CSharp.CompileToMemory #(::RsConfigGetWildwestDir() + @"script\3dsMax\General_tools\VertexColorTools.cs")

if VertexColorTool.UI != undefined then VertexColorTool.UI.Form.Close()

struct VertexColorTool
(
	VertexColorData,
	RefreshButton,
	ColorSwatch,
	ChannelComboBox,
	ChannelManagerListBox,
	PickColorFromMeshButton,
	GetColorFromSelectedButton,
	AddMapChannelButton,
	VertexPaintButton,
	ApplyActionButton,
	SelectByColorButton,
	DisplayObjectRB,
	DisplayMaterialRB,
	ShowVertexColorsCB,
	ShowTextureCB,
	ChannelManagerButton,
	VertexChannelProjectorButton,
	AddChannelButton,
	DeleteChannelButton,
	ActiveObjectList,
	ActiveChannel,		--vertex color channel on the current selected object
	ChannelList = #(),
	StatusList = #(),		--list of strings "used", "unused" or "active" to describe channels in the channel list
	UI,
	
	-------------------------------------------------------------------------------------------------------------------------------------------
	--EVENT HANDLERS
	-------------------------------------------------------------------------------------------------------------------------------------------
	
	fn ColorSwatchPressed =
	(
		local OldColor = this.VertexColorData.SwatchColor.Color 
		local NewColor = colorPickerDlg (color OldColor.R OldColor.G OldColor.B) "Vertex Color" alpha:false
		
		if (NewColor != undefined) then
		(
			this.SetColorSwatch NewColor
		)
	),
	
	fn ChannelComboBoxChanged =
	(
		if (this.ActiveObjectList.count > 0) and (this.VertexColorData.ChannelIndex > -1) then
		(
			ActiveObjectList.vertexColorMapChannel = this.VertexColorData.ChannelIndex - 2
			this.GetChannels()
		)
	),
	
	fn RefreshSelection =
	(
		this.CheckDisplayMode()
		this.GetChannels()
	),
	
	fn PickColorFromMeshButtonPressed =
	(
		this.PickColorFromMesh()
	),
	
	fn GetColorFromSelectedButtonPressed =
	(		
		this.SetColorSwatch (RsPickColourFromSel chan:this.ActiveChannel)
	),
	
	fn AddMapChannelButtonPressed =
	(
		messagebox "add map channel"
	),
	
	fn AddVertexPaintModifierButtonPressed =
	(
		local VPMod = VertexPaint mapChannel: this.VertexColorData.ChannelIndex
		
		if SubObjectLevel == 0 then
		(
			AddModifier this.ActiveObjectList VPMod
			select this.ActiveObjectList
		)
		else
		(
			ModPanel.AddModToSelection VPMod
		)
	),
	
	fn ApplyActionButtonPressed =
	(
		local action = this.VertexColorData.GetActionMode() as name
		local SwatchColor = this.GetSwatchColor()
		local ColorValue = [SwatchColor.r, SwatchColor.g, SwatchColor.b]
		local objClass = classOf this.ActiveObjectList[1]
		
		if (objClass != Editable_Mesh and objClass != Editable_Poly and objClass != PolyMeshObject) then
		(
			if (this.ActiveObjectList[1].modifiers.count == 0) then
			(
				addModifier this.ActiveObjectList[1] (Edit_Mesh())
			)
		)
		
		RsApplyVertColourChange chan:this.ActiveChannel action:action inputVal:ColorValue
	),
	
	fn SelectByColorButtonPressed =
	(
		this.SelectUsingColor()
	),
	
	fn OnShowTextureChanged =
	(
		if (this.VertexColorData.ShowTextures) then displayColor.shaded = #material
		else displayColor.shaded = #object
	),
	
	fn OnShowVertexColorChanged =
	(
		this.ActiveObjectList.showVertexColors = this.VertexColorData.ShowVertexColors
	),
	
	fn SetChannel NewChannel =
	(
		if (NewChannel > -3) then
		(
			this.VertexColorData.ChannelIndex = NewChannel + 2
		)
	),
	
	fn AddChannelButtonPressed =
	(
		local NewChannel = this.VertexColorData.ChannelField as integer
		
		for item in this.ActiveObjectList do
		(
			if (classOf item == Editable_Mesh) then
				meshop.setMapSupport item NewChannel true
				
			else
				polyop.setMapSupport item NewChannel true
		)
		
		this.SetChannel NewChannel
		this.GetChannels()
	),
	
	fn DeleteChannelButtonPressed =
	(
		local ActiveChannel = undefined
		
		local numItems = this.VertexColorData.ChannelDataList.count
		for i = 1 to numItems where ActiveChannel == undefined do
		(
			if (this.VertexColorData.ChannelDataList.Item[i - 1].Status == "active") then
			(
				ActiveChannel = this.VertexColorData.ChannelDataList.Item[i - 1].ID
			)
		)
		
		if (ActiveChannel != undefined) then
		(			
			if (queryBox ("Are you sure you want to delete channel " + ActiveChannel as string + "?") title:"Channel Manager") then
			(
				for item in this.ActiveObjectList do
				(
					if (classOf item == Editable_Mesh) then
					(
						meshop.setMapSupport item ActiveChannel false
					)
					else
					(
						polyop.setMapSupport item ActiveChannel false
					)
				)
				
				this.SetChannel 0
				this.GetChannels()
			)
		)
	),
	
	-------------------------------------------------------------------------------------------------------------------------------------------
	--PROCESSING FUNCTIONS
	-------------------------------------------------------------------------------------------------------------------------------------------

	--get the current object's channel list and send to tool
	fn GetChannels =
	(		
		this.ActiveObjectList = for item in selection where (this.IsMeshOrPoly item) collect item
			
		if this.ActiveObjectList.count == 0 then
		(
			this.VertexColorData.InitChannelListWithMessage "No channels detected"
			this.VertexColorData.UpdateChannelDataListWithMessage "No channels detected"
			this.VertexColorData.InfoText = "No valid objects detected"
			this.VertexColorData.ChanManIndex = 0
			this.VertexColorData.ShowVertexColors = false
			this.VertexColorData.ChannelIndex = 0
			
			--buttons
			this.ApplyActionButton.IsEnabled = false
			this.PickColorFromMeshButton.IsEnabled = false
			this.GetColorFromSelectedButton.IsEnabled = false
			this.SelectByColorButton.IsEnabled = false
			this.AddChannelButton.IsEnabled = false
			this.DeleteChannelButton.IsEnabled = false
			this.VertexPaintButton.IsEnabled = false
		)
		else
		(
			if this.ChannelComboBox.Items.Count == 1 then
			(
				this.VertexColorData.InitChannelList()
			)
			
			this.ApplyActionButton.IsEnabled = true
			this.PickColorFromMeshButton.IsEnabled = true
			this.GetColorFromSelectedButton.IsEnabled = true
			this.SelectByColorButton.IsEnabled = true
			this.AddChannelButton.IsEnabled = true
			this.DeleteChannelButton.IsEnabled = true
			this.VertexPaintButton.IsEnabled = true
		)
		
		if this.ActiveObjectList.count == 1 then
		(
			this.ActiveChannel = this.ActiveObjectList[1].vertexColorMapChannel
			this.VertexColorData.ChannelIndex = this.ActiveChannel + 2
			this.VertexColorData.InfoText = ("Object: " + this.ActiveObjectList[1].Name)
			this.ActiveObjectList[1].VertexColorType = #map_channel
			this.ChannelList = #()
			this.ChannelList = RSGeom_GetMapChannels this.ActiveObjectList[1]
			
			if this.ChannelList.count > 0 then
			(
				this.StatusList = #()
				
				for item in this.ChannelList do
					append this.StatusList "used"
				
				ActiveIndex = findItem this.ChannelList this.ActiveChannel			
				this.VertexColorData.UpdateChannelDataList this.ChannelList this.StatusList
				
				if (ActiveIndex != 0) then
					this.StatusList[ActiveIndex] = "active"
					
				this.VertexColorData.UpdateChannelDataList this.ChannelList this.StatusList
				
				if (ActiveIndex != 0) then
					this.VertexColorData.ChanManIndex = ActiveIndex - 1
			)
			else
			(
				this.VertexColorData.UpdateChannelDataListWithMessage "No channels detected"
			)
			
			if (this.ActiveObjectList[1].showVertexColors) then
				this.VertexColorData.ShowVertexColors = true
			else
				this.VertexColorData.ShowVertexColors = false
			
			this.ApplyActionButton.IsEnabled = true
		)
		
		if this.ActiveObjectList.count > 1 then
		(
			local ShowVC = false
			
			local CurrentMapChannels = this.GetCurrentMapChannels()
			
			if CurrentMapChannels.count > 1 then
			(
				this.VertexColorData.ChannelIndex = -1
			)
			
			if CurrentMapChannels.count == 1 then
			(
				this.VertexColorData.ChannelIndex = CurrentMapChannels[1] + 2
			)
			
			this.VertexColorData.UpdateChannelDataListWithMessage "Multiple objects detected"
			this.VertexColorData.InfoText = "Multiple objects detected"
			this.VertexColorData.ChanManIndex = 0
			
			for item in selection where superClassOf item == GeometryClass do
				if (item.ShowVertexColors) then ShowVC = true
			
			this.VertexColorData.ShowVertexColors = ShowVC
			this.ApplyActionButton.IsEnabled = false
		)
	),
	
	fn GetCurrentMapChannels =
	(
		local MapChannels = #()
		
		for item in this.ActiveObjectList do
		(
			appendIfUnique MapChannels item.vertexColorMapChannel
		)
		
		return MapChannels
	),
	
	fn PickColorFromMesh =
	(
		Tool RsFaceClrPickerTool 
		(
			local chan, obj, objOp, rayMesh
			
			on start do 
			(
				obj = $
				
				if not (isProperty obj #mesh) do return #stop
				
				chan = this.ActiveChannel				
				objOp = RsMeshPolyOp obj
				
				-- Abort if object isn't compatible, or doesn't support requested map-channel:
				if (objOp == undefined) or not (objOp.getMapSupport obj chan) do return #stop
				
				replacePrompt "Pick Face"
				
				-- Set up RayMesh for object:
				rayMesh = RayMeshGridIntersect()
				rayMesh.Initialize 10
				rayMesh.addNode obj
				rayMesh.BuildGrid()
			)
			
			on mousePoint clickNum do 
			(
				-- Cast ray at object:
				local mouseRay = (mapScreenToWorldRay mouse.pos)
				local hits = rayMesh.intersectRay mouseRay.pos mouseRay.dir False
				
				local hitIdx = rayMesh.getClosestHit() -- Get index of closest ray-hit:
				local hitFace = rayMesh.getHitFace hitIdx
				
				if (hitFace == 0) do return False
				
				local colours = #()
				
				case objOp of 
				(
					meshOp:
					(
						local mapVerts = meshOp.getMapFace obj chan hitFace
						colours = for n = 1 to 3 collect (255.0 * (meshOp.getMapVert obj chan mapVerts[n]))
					)
					polyOp:
					(					
						local triToPolyList = RsMakeTriToPolyList obj
						
						local mapVerts = polyOp.getMapFace obj chan triToPolyList[hitFace]
						colours = for vert in mapVerts collect (255.0 * (polyOp.getMapVert obj chan vert))
					)
				)
				
				-- Convert values to clamped integered colours:
				colours = for val in colours collect 
				(
					for n = 1 to 3 do 
					(
						case of 
						(
							(val[n] > 255):(val[n] = 255)
							(val[n] < 0):(val[n] = 0)
							Default:(val[n] = val[n] as integer)
						)
					)
					(val as color)
				)
				colours = makeUniqueArray colours
				
				local getClr = if (colours.count == 1) then colours[1] else (RsChooseFromColours colours)
				this.SetColorSwatch getClr
			)
		)
		
		startTool RsFaceClrPickerTool
	),
	
	fn IsMeshOrPoly InObject =
	(
		IsIt = false
		
		if (classOf InObject == Editable_Mesh) then IsIt = true
		if (classOf InObject == Editable_Poly) then IsIt = true
		if (classOf InObject == PolyMeshObject) then IsIt = true
		
		return IsIt
	),
	
	fn SelectUsingColor =
	(
		local findClr = this.GetSwatchColor()
		local matchDist = VertexColorTool.VertexColorData.RangeField as integer
		local obj = $
		
		if (not isEditPolyMesh obj) do return false
		obj = obj.baseObject

		local subObjType = RsGetSubObjLevelName()
		if (subObjType != #face) do (subObjType = #vertex)
		
		setCommandPanelTaskMode #modify
		modPanel.setCurrentObject obj ui:True
		
		-- Get colour/subobj lists:
		local chan = this.ActiveChannel
		local objClrs = #()
		local clrVerts = #()
		local clrFaces = #()
		RsGetColourSubobjLists obj chan:chan colours:objClrs verts:clrVerts faces:clrFaces
		
		-- Find matching colours: (convert to CIE-L*ab for better matching)
		local findClrVal = (RsXYZtoCIELAB (RsRGBtoXYZ findClr))
		local searchClrVals = for clr in objClrs collect (RsXYZtoCIELAB (RsRGBtoXYZ clr))		
		local searchSubObjs = if (subObjType == #face) then clrFaces else clrVerts
		local matchedSubObjs = #{}
		
		-- Find subobjects with matching colours:
		for clrIdx = 1 to objClrs.count do 
		(
			if (distance findClrVal searchClrVals[clrIdx]) <= matchDist do 
			(
				matchedSubObjs += searchSubObjs[clrIdx]
			)
		)
		
		-- Set subobject selection
		case subObjType of 
		(
			#vertex:
			(
				subObjectLevel = 1
				case (classOf obj) of 
				(
					Editable_Poly:(polyop.setVertSelection obj matchedSubObjs)
					Editable_Mesh:(setVertSelection obj matchedSubObjs)
				)
			)
			#face:
			(
				case (classOf obj) of 
				(
					Editable_Poly:(polyop.setFaceSelection obj matchedSubObjs)
					Editable_Mesh:(setFaceSelection obj matchedSubObjs)
				)
			)
		)
		completeRedraw()
		
		return matchedSubObjs
	),

	fn GetSwatchColor =
	(
		local dotnetColor = VertexColorTool.VertexColorData.SwatchColor.Color
		return (color dotnetColor.R dotnetColor.G dotnetColor.B)
	),
	
	fn SetColorSwatch InColor =
	(
		local WPFColor = dotNetClass "System.Windows.Media.Color"	--dotnet preset
		this.VertexColorData.SwatchColor.Color = WPFColor.FromRGB InColor.R InColor.G InColor.B
	),
	
	-------------------------------------------------------------------------------------------------------------------------------------------
	--OTHER STUFF
	-------------------------------------------------------------------------------------------------------------------------------------------
	
	fn InitStuff =
	(
		--bind to class
		this.VertexColorData = dotNetObject "VertexColorTools.VertexColorData"
		this.UI.Content.DataContext = this.VertexColorData
		
		--chanman list items
		this.VertexColorData.BindElement this.UI.content
		
		--EVENT WIRING (part 1)
		this.ColorSwatch = VertexColorTool.UI.Content.FindName("ColorPicker")
		this.ChannelComboBox = VertexColorTool.UI.Content.FindName("ChannelComboBox")
		this.PickColorFromMeshButton = VertexColorTool.UI.Content.FindName("PickColorFromMesh")
		this.GetColorFromSelectedButton = VertexColorTool.UI.Content.FindName("GetColorFromSelected")
		this.AddMapChannelButton = VertexColorTool.UI.Content.FindName("AddMapChannel")
		this.VertexPaintButton = VertexColorTool.UI.Content.FindName("VertexPaint")
		this.ApplyActionButton = VertexColorTool.UI.Content.FindName("ApplyAction")
		this.SelectByColorButton = VertexColorTool.UI.Content.FindName("SelectByColor")
		this.ShowVertexColorsCB = VertexColorTool.UI.Content.FindName("CB_ShowVC")
		this.ShowTextureCB = VertexColorTool.UI.Content.FindName("CB_ShowTexture")
		this.ChannelManagerListBox = VertexColorTool.UI.Content.FindName("ChanMan")
		this.AddChannelButton = VertexColorTool.UI.Content.FindName("AddChannel")
		this.DeleteChannelButton = VertexColorTool.UI.Content.FindName("DeleteChannel")
		
		this.RefreshSelection()
		this.VertexColorData.ActionIndex = 0		
	),
	
	fn CheckDisplayMode =
	(		
		if (displayColor.shaded == #material) then this.VertexColorData.ShowTextures = true
		else this.VertexColorData.ShowTextures = false
	)
)

--Setup our tool window
VertexColorTool = VertexColorTool()
VertexColorTool.UI = RSToolWindow Name:"Vertex Color Tools" UISource:VertexColorToolXAML
VertexColorTool.InitStuff()

--CALLBACK SCRIPTS

fn VertexColorTool_SelectionChanged = (	VertexColorTool.RefreshSelection() )
fn VertexColorTool_OnClose = ( callbacks.removeScripts #selectionSetChanged id:#VertexColorToolsCallbacks )
fn VertexColorTool_OnOpen = ( callbacks.addScript #selectionSetChanged "VertexColorTool_SelectionChanged()" id:#VertexColorToolsCallbacks )
dotnet.AddEventHandler VertexColorTool.UI.Form "Closing" VertexColorTool_OnClose
dotnet.AddEventHandler VertexColorTool.UI.Form "Load" VertexColorTool_OnOpen

--EVENT WIRING (part 2)

fn VertexColorTool_OnColorSwatchPressed = ( VertexColorTool.ColorSwatchPressed() )
dotnet.AddEventHandler VertexColorTool.ColorSwatch "Click" VertexColorTool_OnColorSwatchPressed

fn VertexColorTool_OnChannelComboBoxChanged = ( VertexColorTool.ChannelComboBoxChanged() )
dotNet.AddEventHandler VertexColorTool.ChannelComboBox "SelectionChanged" VertexColorTool_OnChannelComboBoxChanged

fn VertexColorTool_OnPickColorFromMeshPressed = ( VertexColorTool.PickColorFromMeshButtonPressed() )
dotnet.AddEventHandler VertexColorTool.PickColorFromMeshButton "Click" VertexColorTool_OnPickColorFromMeshPressed

fn VertexColorTool_OnGetColorFromSelectedPressed = ( VertexColorTool.GetColorFromSelectedButtonPressed() )
dotnet.AddEventHandler VertexColorTool.GetColorFromSelectedButton "Click" VertexColorTool_OnGetColorFromSelectedPressed

fn VertexColorTool_OnVertexPaintPressed = ( VertexColorTool.AddVertexPaintModifierButtonPressed() )
dotnet.AddEventHandler VertexColorTool.VertexPaintButton "Click" VertexColorTool_OnVertexPaintPressed

fn VertexColorTool_OnApplyActionPressed = ( VertexColorTool.ApplyActionButtonPressed() )
dotnet.AddEventHandler VertexColorTool.ApplyActionButton "Click" VertexColorTool_OnApplyActionPressed

fn VertexColorTool_OnSelectByColorPressed = ( VertexColorTool.SelectByColorButtonPressed() )
dotnet.AddEventHandler VertexColorTool.SelectByColorButton "Click" VertexColorTool_OnSelectByColorPressed

fn VertexColorTool_OnShowTextureChanged = ( VertexColorTool.OnShowTextureChanged() )
dotnet.AddEventHandler VertexColorTool.ShowTextureCB "Checked" VertexColorTool_OnShowTextureChanged
dotnet.AddEventHandler VertexColorTool.ShowTextureCB "Unchecked" VertexColorTool_OnShowTextureChanged

fn VertexColorTool_OnShowVertexColorsChanged = ( VertexColorTool.OnShowVertexColorChanged() )
dotnet.AddEventHandler VertexColorTool.ShowVertexColorsCB "Checked" VertexColorTool_OnShowVertexColorsChanged
dotnet.AddEventHandler VertexColorTool.ShowVertexColorsCB "Unchecked" VertexColorTool_OnShowVertexColorsChanged

fn VertexColorTool_SelectChannelAction s e = ( VertexColorTool.SetChannel e.Data.ID )
dotnet.AddEventHandler VertexColorTool.VertexColorData "SelectChannel" VertexColorTool_SelectChannelAction

fn VertexColorTool_AddChannelPressed = ( VertexColorTool.AddChannelButtonPressed() )
dotnet.AddEventHandler VertexColorTool.AddChannelButton "Click" VertexColorTool_AddChannelPressed
	
fn VertexColorTool_DeleteChannelPressed = ( VertexColorTool.DeleteChannelButtonPressed() )
dotnet.AddEventHandler VertexColorTool.DeleteChannelButton "Click" VertexColorTool_DeleteChannelPressed

clearListener()
VertexColorTool.UI.Open()