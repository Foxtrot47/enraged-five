--Reflection Map Tool
--Stewart Wright
--23/07/10

-- Moved to new wildwest May 2011 by Rick Stirling

-------------------------------------------------------------------------------------------------------------------------
--A script for loading and selection reflection maps
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")


filein "pipeline\\util\\ragematerial.ms"

-- Where to get the reflection maps from
reflMapFolder = (theProjectRoot + "assets/reflections/")

saveMe = ""
reflMapSel = "None"
-------------------------------------------------------------------------------------------------------------------------
fn loadReflMap =
(
	if reflMapSel != "None" then --load the map
	(
		reflMapPath = reflMapFolder + reflMapSel
		environmentMap = Bitmaptexture fileName:reflMapPath
	)
	else --clear the map
	(
		environmentMap = undefined
	)
)--end loadReflMap
-------------------------------------------------------------------------------------------------------------------------
rollout reflMapTool "Reflection Map Tool"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Reflection_Map_Loader" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	dropdownlist ddlReflMap "Select Reflection Map" items:#()
	--button btnLoadMap "Load Reflection Map"  width:140 height:30 tooltip:"Load selected reflection map"
	checkbox chkMapToggle "Use Map" checked:useEnvironmentMap
-------------------------------------------------------------------------------------------------------------------------
	-- Populate the list of reflections
    on reflMapTool open do (
		--lets save the current enviroment map, I might be able to use it...
		saveMe = environmentMap
		
		-- get list of relfection map files
 		mapList=#("Select Reflection Map")
 		mapList=#()
-- 		mapFiles = reflMapFolder  + "*.bmp"	
-- 		theMaps = getfiles mapFiles
		
		themaps = returnImageFiles reflMapFolder
		
		-- add some checks to see if the reflection maps exist
		if theMaps.count != 0 then
		(
			for m in theMaps do
			(
				mapName = m
				print mapName
				append mapList (uppercase(filenameFromPath m))	
			)
			append mapList "None"
		)
		else --if the reflection folder is empty
		(
			mText = "No reflection maps found in " + reflMapFolder + "\r\nHave you fetched latest?"
			messagebox mText
			destroyDialog reflMapTool --lets close the script because we can't do anything without the maps
		)
		
		--populate the file list
		ddlReflMap.items = mapList
	)
-------------------------------------------------------------------------------------------------------------------------
	--monitor ddlReflMap field
	on ddlReflMap selected sel do
		(
			reflMapSel = ddlReflMap.selected
			print reflMapSel
			loadReflMap()
			RstUpdateRageShaders()
			reflMapTool.chkMapToggle.state = true
		)
-------------------------------------------------------------------------------------------------------------------------
	--monitor chkMapToggle checkbox
	on chkMapToggle changed theState do
		(
		useEnvironmentMap = theState
		RstUpdateRageShaders()
		)
)
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
createDialog reflMapTool