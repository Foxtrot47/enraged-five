struct hardSoftEdge (
	--
	--Get smoothing group integer as bitarray.
	--Not used in this script, but can be useful for testing.
	--
	fn getBitsAsArray bits = (
		arr = #{}
		for i = 1 to 32 do (
			arr[i] = bit.get bits i;
		)
		return arr;
	),
	

		
	--
	--Get shared smoothing groups between two faces.
	--
	fn getSharedSmGroups obj faces = (
		local smGroups = #();
		for f in faces do (
			append smGroups (polyOp.getFaceSmoothGroup obj f);
		)
		local sharedSmGroups = (bit.and smGroups[1] smGroups[2]);
		return sharedSmGroups;
	),
	
	
	
	
	--
	--Get adjacent faces.
	--
	fn getAdjacentFaces obj face = (
		local vertList = polyOp.getVertsUsingFace obj face;
		local adjacentFaces = polyOp.getFacesUsingVert obj vertList;
		deleteItem adjacentFaces face;
		return adjacentFaces;
	),
	
	
	
	
	--
	--Removes any smoothing groups that are not shared between the given
	--face or any of the adjacent faces.
	--
	fn removeRedundantGroups obj face = (
		local adjacentFaces = hardSoftEdge.getAdjacentFaces obj face;
		local nonRedundantGroups = 0;
		for a in adjacentFaces do (
			nonRedundantGroups = (bit.or nonRedundantGroups (hardSoftEdge.getSharedSmGroups obj #{face, a}));
		)
		polyOp.setFaceSmoothGroup obj face nonRedundantGroups;
	),
	
	
	
	
	--
	--SET SOFT EDGE
	--
	fn setSoftEdge obj faces = (
		--Get shared smoothing groups.
		local sharedSmGroups = (hardSoftEdge.getSharedSmGroups obj faces);

		--If there are no shared smoothing groups, continue. Otherwise, edge is already soft.
		if sharedSmGroups == 0 do (
			--Get adjacent faces.
			local adjacentFaces = #();
			for f in faces do (
				append adjacentFaces (hardSoftEdge.getAdjacentFaces obj f);
			)
			adjacentFaces[1] = adjacentFaces[1] - faces;
			adjacentFaces[2] = adjacentFaces[2] - faces;

			
			--Determine impossible smoothing groups.
			local impSmGrps = 0;
			local c = 1;
			for f in faces do (
				--Get current face smoothing group.
				local smGrpCurFace = (polyOp.getFaceSmoothGroup obj f);

				--Get adjacent non-shared smoothing groups and add them to impSmGrps.
				for a in adjacentFaces[c] do (
					local smGrpAdjFace = (polyOp.getFaceSmoothGroup obj a);
					local nonShared = (bit.and smGrpAdjFace (bit.not smGrpCurFace));
					impSmGrps = (bit.or impSmGrps nonShared);
				)
				c += 1;
			)
		
			--Check for first available smoothing group.
			for i = 1 to 32 do (
				if (bit.get impSmGrps i) == false do (
					--Add smoothing group.
					polyOp.setFaceSmoothGroup obj faces (2 ^ (i - 1)) add:true;
					exit;
				)
			)
			
			for f in faces do (
				hardSoftEdge.removeRedundantGroups obj f;
			)
		)
		
	),
	
	
	
	
	
	
	
	
	--
	--SET HARD EDGE
	--
	fn setHardEdge obj faces = (
		--Get shared smoothing groups.
		local sharedSmGroups = (hardSoftEdge.getSharedSmGroups obj faces);

		
		--If there are shared smoothing groups, continue. Otherwise, edge is already hard.
		if sharedSmGroups != 0 do (
/*			--Get adjacent faces.
			local adjacentFaces = #();
			for f in faces do (
				append adjacentFaces (hardSoftEdge.getAdjacentFaces obj f);
			)
			adjacentFaces[1] = adjacentFaces[1] - faces;
			adjacentFaces[2] = adjacentFaces[2] - faces;
			

			--Check if shared smoothing groups are not shared with adjacent faces.
			local sharedSmGroupsAdjFaces = #()
			local c = 1;
			for f in faces do (
				--Get current face smoothing group.
				local sharedSmGrps = 0;
				local smGrpCurFace = (polyOp.getFaceSmoothGroup $ f);
								
				--Get adjacent shared smoothing groups and add them to impSmGrps.
				for a in adjacentFaces[c] do (
					local smGrpAdjFace = (polyOp.getFaceSmoothGroup $ a);
					sharedSmGrps += (bit.xor sharedSmGrps (bit.and smGrpAdjFace smGrpCurFace));
				)
				append sharedSmGroupsAdjFaces sharedSmGrps;
				c += 1;
			)
			
				
			local conflictingSmGroups = (bit.and sharedSmGroupsAdjFaces[1] sharedSmGroupsAdjFaces[2]);
*/			
			
			--Detect which edges will need resmoothing.
			local resmoothEdges = #();
			for f in faces do (
				--Get edges using face.
				local faceEdges = (polyOp.getEdgesUsingFace obj f);
			
				for e in faceEdges do (
					--Get faces using edge.
					local edgeFaces = (polyOp.getFacesUsingEdge obj e);
					--Only use non-open edges.
					if edgeFaces.numberSet > 1 do (
						
						for ef in edgeFaces do (
							--Only look at faces that are not the faces to smooth initially.
							if faces[ef] == false do (
								--Get face smoothing group.
								local efSmGroups = (polyOp.getFaceSmoothGroup obj ef);
								--Compare with smoothing groups that will be removed. 
								--If there are shared smoothing groups between these faces, the edge 
								--will need resmoothing.
								if (bit.and efSmGroups sharedSmGroups) != 0 do (
									append resmoothEdges edgeFaces;
								)
							)
						)
						
					)
				)
			)

			--Remove shared smoothing groups.
			for f in faces do (
				polyOp.setFaceSmoothGroup obj f (bit.xor (polyOp.getFaceSmoothGroup obj f) sharedSmGroups);				
			)
			
			--Resmooth surrounding edges.
			for e in resmoothEdges do (
				hardSoftEdge.setSoftEdge obj e;
			)
			
			for f in faces do (
				hardSoftEdge.removeRedundantGroups obj f;
			)
		)
	),
	
	
	
	
	
	
	--
	--SET EDGE
	--
	fn setEdge mode = (
		local obj = $;
		local objClass = (classOf obj);
		
		if (objClass == Editable_Poly) then (
			--Get edge selection.
			local selEdges = (polyOp.getEdgeSelection obj);

			print selEdges.numberSet;
			print (polyOp.getNumEdges obj)
			
			if selEdges.numberSet == (polyOp.getNumEdges obj) then (
				--All edges selected, so assign one smoothing group to all faces.
				if mode == "soft" then (
					polyOp.setFaceSmoothGroup obj #{1..(polyOp.getNumFaces obj)} 1;
				) else if mode == "hard" do (
					polyOp.setFaceSmoothGroup obj #{1..(polyOp.getNumFaces obj)} 0;
				)
			) else (
				for e in selEdges do (
					--Get faces used by selected edges.
					local edgeFaces = (polyOp.getFacesUsingEdge obj e);
					--If not an open edge, continue.
					if edgeFaces.numberSet > 1 then (
						if mode == "soft" then (
							--Soft edge.
							hardSoftEdge.setSoftEdge obj edgeFaces;
						) else if mode == "hard" do (
							--Hard edge.
							hardSoftEdge.setHardEdge obj edgeFaces;
						)
					) else (
						print ("Set " + mode + " edge warning: Open edge!");
					)
				)
			)
		) else if (objClass == PolyMeshObject) then (
			MessageBox "Sorry, Edit Poly modifiers are not supported." title:"Error";
		) else if (objClass == Editable_Mesh) then (
			MessageBox "Editable Mesh is not supported.\nUse Editable Poly instead, it's better." title:"Error";
		) else (
			print ("Set " + mode + " edge error: No Editable Poly selected!");
		)
		
	)

)