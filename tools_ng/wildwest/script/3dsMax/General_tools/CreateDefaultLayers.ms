filein (RsConfigGetWildWestDir() + "script/3dsmax/_config_files/wildwest_header.ms")

struct DefaultLayersStruct
(
	--//////////////////////////////////////////////////////////////////////////////////////
	--PRIVATE
	--//////////////////////////////////////////////////////////////////////////////////////
	private
	projectLayerDef = RsConfigGetEtcDir() + "config/generic/DefaultLayers.xml",
	layersByType = #(),
	
	
	--//////////////////////////////////////////////////////////////////////////////////////
	fn parseLayerDefs =
	(
		--parse xml for layer types and names
		local xmlStream = xmlStreamHandler xmlFile:projectLayerDef
		
		xmlStream.open()
		
		--get me layertypes
		local root = xmlStream.root
		local types = root.selectNodes "Type"
		
		--get the layersByTypes
		for t = 0 to types.count - 1 do
		(
			local thisType = types.itemOf t
			local thisTypeName = thisType.GetAttribute "name"
			local layerNodes = thisType.selectNodes "./Layer[@name]"
			local layerNames = for n = 0 to layerNodes.count - 1 collect (layerNodes.itemOf n).getAttribute "name"
			
			append layersByType (DataPair type:thisTypeName layers:layerNames)
		)
		
		xmlStream.close()
	),
	
	--//////////////////////////////////////////////////////////////////////////////////////
	--PUBLIC
	--//////////////////////////////////////////////////////////////////////////////////////
	public
	
	--//////////////////////////////////////////////////////////////////////////////////////
	fn getProjectLayerDef =
	(
		projectLayerDef
	),
	
	--//////////////////////////////////////////////////////////////////////////////////////
	fn parseLayers =
	(
		parseLayerDefs()
	),
	
	--//////////////////////////////////////////////////////////////////////////////////////
	fn getLayerNames type = 
	(
		(for item in layersByType where item.type == type collect item.layers)[1]
	),
	
	--//////////////////////////////////////////////////////////////////////////////////////
	fn haveLayers =
	(
		return layersByType.count > 0
	),
	
	--//////////////////////////////////////////////////////////////////////////////////////
	fn getlayersByType =
	(
		for l in layersByType collect l.type
	),
	
	--//////////////////////////////////////////////////////////////////////////////////////
	fn createLayers type prefix:"" =
	(
		local layerNames = getLayerNames type
		for name in layerNames do
		(
			if prefix != "" and name != "DONT_EXPORT" then name = prefix+"_"+name else ""
			local newLayer = LayerManager.newLayerFromName name
		)
		
		--open the layer manager to show what we wrought
		macros.run "Layers" "LayerManager"
	)
	
)

try(destroyDialog CreateDefaultLayers)catch()
rollout CreateDefaultLayers "Create Default Layers" width:200
(
	--//////////////////////////////////////////////////////////////////////////////////////
	--	VARS
	--//////////////////////////////////////////////////////////////////////////////////////
	local defLayers = DefaultLayersStruct()
	local rolloutWidth = 200	--CreateDefaultLayers.width
	--//////////////////////////////////////////////////////////////////////////////////////
	--	UI
	--//////////////////////////////////////////////////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:rolloutWidth
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Create Default Layers" filename:(getThisScriptFilename())
	label lblType "Type: " across:2 align:#left 
	dropdownlist ddlMapType tooltip:"Content type"
	label lblContainerPrefix "Container: " across:2 align:#left tooltip:"Container Prefix"
	dropdownlist ddlContainerPrefix
	label lblPrefix "Prefix: " across:2 align:#left
	edittext edtPrefix width:135 offset:[-50, 0] tooltip:"User defined prefix"
	button btnCreateLayers "Create Layers" width:(rolloutWidth-10)
	
	--//////////////////////////////////////////////////////////////////////////////////////
	--	FUNCTIONS
	--//////////////////////////////////////////////////////////////////////////////////////
	
	--//////////////////////////////////////////////////////////////////////////////////////
	--	EVENTS
	--//////////////////////////////////////////////////////////////////////////////////////
	
	--//////////////////////////////////////////////////////////////////////////////////////
	on btnCreatelayers pressed do
	(
		if not defLayers.haveLayers() then return false
		if ddlMaptype.selected == undefined or ddlMaptype.selected == "" then return false
		
		local prefix = if ddlContainerPrefix.selected != "None" then ddlContainerPrefix.selected else edtPrefix.text
		--get the dropdown selection and create the layers for the type
		defLayers.createLayers ddlMaptype.selected prefix:prefix 
		
	)
	
	--//////////////////////////////////////////////////////////////////////////////////////
	on CreateDefaultLayers open do
	(
		banner.setup()
		
		if gRsPerforce.connected() == false then gRsPerforce.connect()
		gRsPerforce.sync (defLayers.getProjectLayerDef()) silent:true
		
		--populate the dropdown with the content types available
		defLayers.parseLayers()
		
		--finished parsing xml, now show the results in the dropdown
		ddlMapType.items = defLayers.getLayersByType()
			
		--populate the container dropdown prefix list
		local sceneContainers = for o in $objects where classOf o == Container collect o.name
		append sceneContainers "None"
		if sceneContainers.count == 0 then
		(
			ddlContainerPrefix.items = #("None")
		)
		else
		(
			ddlContainerPrefix.items = sceneContainers
		)
	)
)

createDialog CreateDefaultLayers