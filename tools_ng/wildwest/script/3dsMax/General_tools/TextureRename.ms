filein "pipeline/helpers/textures/RsTextureToolkit.ms"

global textureToolkit = RsTextureToolkitStruct()
-- This line loads the custom header
--filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

struct textureRenameUI 
(
	DisplayMode = #NewNames,
	SelObjs = False, HasDirtyFlags = False,
	
		fn refreshObjectList theObjectList textureObjArray:#() = 
		(
			local objNames = #()
			
			if textureObjArray.count > 0 then (
				
				for textureObj in textureObjArray do 
					if textureObj != undefined then (
						
						for obj in textureObj.objs do (
							appendifUnique objNames obj.name
						)
				)
						
				theObjectList.items = objNames
				theObjectList.selection = 0
				
			) else (
			
					theObjectList.items = #()
					theObjectList.selection = 0
			)
		),
		
		fn filterVal val theEditText = (
			
			if (val.count - 1) > 0 then (				
				if substring val val.count val.count != "\\" and 
				(substring val val.count val.count != "\\" or substring val (val.count-1) (val.count-1) != "\\" )					
				then theEditText.text = val+"\\"
			)
		),

		fn makePrefixList thePrefixList = 
		(
			local textureListTextures = textureToolkit.makeDisplayArray #NewNames displayOnly:false
			local PrefixList = #()
			if thePrefixList != undefined then  PrefixList = thePrefixList.items 
				
			for tex in textureListTextures do
			(
				
				if (filterstring tex "_").count > 1 then appendIfUnique PrefixList (filterstring tex "_")[1]
				
			)
			
			if thePrefixList != undefined then thePrefixList.items = PrefixList
			if thePrefixList != undefined then thePrefixList.selection = 0
		),
		
		-- Update 'TextureList' control to current UI/selection state:
		fn RefreshTexturesList TextureListCtrl Mode: Reselect:True = 
		(
			if (Mode == unsupplied) do 
			(
				Mode = This.DisplayMode
			)
			
			-- 'DisplayTexturesArray' stores items for previously-shown texmaps:
			local TexArray = TextureToolKit.DisplayTexturesArray
			
			-- Get texmap-items for list's selected items:
			local isMultiSel = (isKindOf TextureListCtrl.Selection BitArray)
			local CurrentSel = if isMultiSel then 
			(
				for n in TextureListCtrl.Selection collect TexArray[n]
			)
			else 
			(
				local SelNum = TextureListCtrl.Selection
				if (SelNum == 0) then undefined else TexArray[SelNum]
			)
			
			-- Regenerate list with updated settings:
			local NewList = TextureToolkit.MakeDisplayArray Mode displayonly:true SelObjs:SelObjs
			TextureListCtrl.Items = NewList
			
			-- Do any of the displayed filenames has 'doRename' flag set?
			HasDirtyFlags = TextureToolkit.DisplayingDirtyFiles
			
			-- Re-select item(s) from previous version of list (or clear selection)
			(
				if isMultiSel then 
				(
					local NewSel = #{}
					
					if Reselect do 
					(
						for TexItem in CurrentSel do 
						(
							local FindNum = FindItem TexArray TexItem
							
							if (FindNum != 0) do 
							(
								NewSel[FindNum] = True
							)
						)
					)
					
					TextureListCtrl.Selection = NewSel
				)
				else 
				(
					if Reselect do 
					(
						local SelNum = 0
						
						if Reselect and (CurrentSel != undefined) do 
						(
							SelNum = FindItem TexArray CurrentSel
						)
						
						TextureListCtrl.Selection = SelNum
					)
				)
			)
			
			return OK
		),
		
		-- Toggles path/path-display:
		fn ToggleDisplayMode TextureListCtrl = 
		(
			This.DisplayMode = (if (This.DisplayMode == #NewNames) then #NewPaths else #NewNames)
			RefreshTexturesList TextureListCtrl
		),
		
		fn updateUI theTextureList theObjectList thePrefixList theDisplayVal clearlog:false validate:true errors:false updateList:false errorList:false = (

			if clearlog do 
			(
				gRsTextureToolkitULog.clearLogDirectory()
			)
			
			textureToolkit.refreshNoMissingTextureErrors = (not errors)
			textureToolkit.MultipleFiles = true
			textureToolkit.doErrors = errorList
			
			if updateList do 
			(
				textureToolkit.update()						
				textureToolkit.texturesArray = textureToolkit.getTexturesList()
			)
			
			RefreshTexturesList theTextureList Reselect:False
			
			refreshObjectList theObjectList			
			makePrefixList thePrefixList
			
			if ( gRsTextureToolkitULog.HasErrors() ) and validate and errors do 
			(
				gRsTextureToolkitULog.validate()
			)	
		)
)

global texRenUI = textureRenameUI()

(try destroydialog TextureRenameTools_Rollout catch ())
-------------------------------------------------------------------------------------------------------------------------	

-------------------------------------------------------------------------------------------------------------------------
(
	
	global TextureRenameTools_Rollout
	try (cui.UnRegisterDialogBar TextureRenameTools_Rollout)catch()
	try(destroyDialog TextureRenameTools_Rollout)catch()
	local LastSubRollout = 1
	
	local theVal = 0
	local displayVal = 0
	
	local TextureDir = (RsConfigGetArtDir()+"textures/")

-------------------------------------------------------------------------------------------------------------------------
	rollout MultipleFilesRename_Rollout "Multiple Files Rename" 
	(
		checkbox cbFilterSelected "Filter Selected Objects Only" checked:(TexRenUI.SelObjs) align:#left across:2		
		button refreshList "Refresh List" width:100 pos:[672,17] align:#right
		multilistbox textureList "Files: (Right Click to Toggle Path)" width:760 height:18  pos:[13,27]
		button  btn_selAll "Select All" width:80 pos:[13,295]
		button  btn_selNone "Select None" width:80 pos:[103,295]
		button btn_resetSelected "RESET SELECTED TEXTURES" width:190  pos:[193,295]
		
		multilistbox objectList "Objects Using Texture" height:12 pos:[585,286] width:186

		
		groupbox grp_prefixes1 "Change Prefix" width:370 height:50 pos:[13,320]	
		
		--label lbl_repText "New Prefix:"	pos:[(grp_prefixes1.pos.x+11),(grp_prefixes1.pos.y+15)] 	
 		editText edtNewName "" width:160 labelontop:true pos:[(grp_prefixes1.pos.x+11),(grp_prefixes1.pos.y+20)] 
		button btn_addPref "Add Prefix" pos:[(grp_prefixes1.pos.x+180),(grp_prefixes1.pos.y+18)] width:78
		button btn_assignPref "Change Prefix" pos:[(grp_prefixes1.pos.x+275),(grp_prefixes1.pos.y+18)] width:78
		
		
		
		groupbox	grp_prefixes "Prefix Filter" width:180 height:176 pos:[395,289]	
		
 		label lbl_remPre "Remove Prefixes:" align:#left pos:[(grp_prefixes.pos.x+10),(grp_prefixes.pos.y+15)]
 		listbox lst_filter ""   width:75 items:#() pos:[(grp_prefixes.pos.x+10),(grp_prefixes.pos.y+30)] align:#left height:10		
		edittext txt_filter "" align:#left  width:75 height:19  pos:[(grp_prefixes.pos.x+95),(grp_prefixes.pos.y+30)]		
 		button btn_add "Add"  width:73  pos:[(grp_prefixes.pos.x+98),(grp_prefixes.pos.y+55)]
 		button btn_remo "Remove"  width:73  pos:[(grp_prefixes.pos.x+98),(grp_prefixes.pos.y+80)]

		groupbox	grp_path "Path" width:370 height:90 pos:[13,375]	

		editText edtNewPath "New File Path:" width:330  pos:[(grp_path.pos.x+10),(grp_path.pos.y+15)] labelontop:true	--readonly:true		
		button btnSetPath "..." pos:[(grp_path.pos.x+343),(grp_path.pos.y+32)] width:20 height:20		
		button btnassignPath "Assign Path to Selected"  pos:[(grp_path.pos.x+203),(grp_path.pos.y+57)] width:160 height:20
		
		groupbox grp_name "Name" width:765 height:155 pos:[12,465]
		
		editText txt_TextureNameOld "Old Name:" labelontop:true readonly:true pos:[(grp_name.pos.x+10),(grp_name.pos.y+15)] width:745
		editText txt_TextureNameNew "New Name:" labelontop:true pos:[(grp_name.pos.x+10),(grp_name.pos.y+55)] width:745
		button btn_ChooseNewTexture "Choose New Texture"  pos:[(grp_name.pos.x+1755),(grp_name.pos.y+71)] width:140
		button btn_RenameFiles "Rename Files"  width:120 pos:[(grp_name.pos.x+637),(grp_name.pos.y+105)] height:40
		
		groupbox grp_Options "Options" width:625 height:45 pos:[22,565]
		checkbox chk_removeold "Remove Old File when Renaming (Dont Check if unsure)"  checked:false  pos:[(grp_Options.pos.x+10),(grp_Options.pos.y+18)] 
		checkbox chk_backupOld "Shelve a Backup of the Old Texture"  checked:true  pos:[(grp_Options.pos.x+320),(grp_Options.pos.y+18)] 
		checkbox chk_repathOnly "Repath Only"  checked:false  pos:[(grp_Options.pos.x+535),(grp_Options.pos.y+18)] 
		
		fn clearInfo = (
			
			edtNewPath.text = ""
			lst_filter.items = #()
			txt_filter.text = ""
			edtNewName.text = ""
			txt_TextureNameOld.text = ""
			txt_TextureNameNew.text = ""
		)
		
		fn openAFile  = (
	 
			local filename = getOpenFileName \
			caption:("Open Texture ")\
			filename:(textureDir) 
			
			filename
		)
		
		on cbFilterSelected changed state do 
		(
			TexRenUI.SelObjs = (not TexRenUI.SelObjs)
			TexRenUI.RefreshTexturesList TextureList
		)
		
		on chk_repathOnly changed val do (
			
			if val then btn_RenameFiles.text = "Repath Files" else btn_RenameFiles.text = "Rename Files"
			if val then txt_TextureNameOld.caption = "Original:" else txt_TextureNameOld.caption = "Old Name:"
			if val then txt_TextureNameNew.caption = "New:" else txt_TextureNameNew.caption = "New Name:"
			if val then txt_TextureNameNew.width = 600 else txt_TextureNameNew.width = 745
			if val then btn_ChooseNewTexture.pos = [(grp_name.pos.x+615),(grp_name.pos.y+71)] else btn_ChooseNewTexture.pos = [(grp_name.pos.x+1755),(grp_name.pos.y+71)]
		)
		
		on btn_ChooseNewTexture pressed do (
			
			local NewTexture = openAFile()
			
			local selectedItems = textureList.selection
			
			if (selectedItems.NumberSet == 1) do 
			(
				for item in selectedItems do 
				(
					local oldNameArray = textureToolkit.makeDisplayArray #OldNames
					local textureObj = textureToolkit.getTextureObj oldNameArray[item]	
					if textureObj != undefined then (
		
						textureObj.newfilename = NewTexture
						textureObj.doRenameFile = true
					)
					
					TexRenUI.RefreshTexturesList TextureList
					txt_TextureNameNew.text = NewTexture
				)
				
			)
			
		)
		
		on edtNewPath changed val do 
		(
			texRenUI.filterVal val edtNewPath
		)
		on edtNewPath entered val do 
		(
			texRenUI.filterVal val edtNewPath
		)
		
		on btn_selAll pressed do 
		(			
			if (TextureList.Items.Count != 0) do 
			(
				Texturelist.Selection = #{1..TextureList.Items.Count}
			)
		)
		
		on btn_selNone pressed do (
			
			texturelist.selection = #{}
			
		)
				
		on refreshList Pressed do 
		(
			local reallyReset = True
			
			if TexRenUI.hasDirtyFlags do 
			(
				reallyReset = querybox "Refreshing the list will reset any changes you have made. Are you sure?" title:"R* Query"
			)
			
			if reallyReset do 
			(
				texRenUI.updateUI textureList objectList lst_filter DisplayMode errors:false updateList:true
			)
		)	
		
		on btn_resetSelected pressed do (	
			
			local allTextures= textureToolkit.makeDisplayArray #OldNames displayOnly:false			
			local selectedArray = for item in textureList.selection collect allTextures[item]
			
			for item in selectedArray do (	
				
				local textureObj = textureToolkit.getTextureObj item	
 				if textureObj != undefined and textureObj.doRenameFile == true  then (
					
					textureObj.doRenameFile = false
					textureObj.newFilename = textureObj.filename
				)

			)
			
			TexRenUI.RefreshTexturesList TextureList
			texRenUI.updateUI textureList objectList lst_filter DisplayMode errors:false clearUIinfo:false			
		)
		
		on btn_RenameFiles pressed do (

			local allTextures = textureToolkit.makeDisplayArray #OldNames

			local selectedArray = #()
			gRsTextureToolkitULog.clearLogDirectory()
			for item in allTextures do 
			(
				local textureObj = textureToolkit.getTextureObj item
				if textureObj != undefined and textureObj.doRenameFile == true then (
					textureObj.removeOldFile = chk_removeold.state
					append selectedArray (getfilenamefile textureObj.filename + getfilenametype  textureObj.filename)
				)		
			)

			TexRenUI.RefreshTexturesList TextureList
			local usePerforce  = true
			local repathOnly = false
			if chk_repathOnly.state do 
			(
				usePerforce = false
				repathOnly = true				
			)
			textureToolkit.renameAllTextures usePerforce repathOnly (textureToolkit.genChangeList()) texturesList:selectedArray backUp:chk_backupOld.state
			
			texRenUI.updateUI textureList objectList lst_filter DisplayMode errors:true updateList:true
		)
		
		on objectList selected item do (
			clearSelection()
			
			local sel = objectList.selection as array
			local objNames = #()
			
			for idx in sel do (
				append objNames objectList.items[ idx ]
			)
			
			for objName in objNames do (
				local obj = getNodeByName objName
			
				if ( isValidNode obj ) then (
					selectMore obj
				)
			)
			
			completeRedraw()
		)	
		
		on btnSetPath pressed do
		(
			thePath = getSavepath initialDir:textureDir
			if thePath != undefined then 
			(				
				edtNewPath.text =  (thePath+"\\")				
				local allTextures = textureToolkit.makeDisplayArray #OldNames displayOnly:false
				local selectedNums = textureList.selection as Array
				local selectedArray = for num in selectedNums collect allTextures[num]

				textureToolkit.changePathTexOBj selectedArray newPath:edtNewPath.text
				TexRenUI.RefreshTexturesList TextureList
			)
			textureList.selection = 0
			texRenUI.refreshObjectList objectList
		)
		
 		on btnassignPath pressed do
 		(
			thePath = edtNewPath.text
			if thePath != undefined and thePath !="" then 
			(	
				local allTextures = textureToolkit.makeDisplayArray #OldNames displayOnly:false
		
				local selectedNums = textureList.selection as Array
				local selectedArray = for num in selectedNums collect allTextures[num]
					
				textureToolkit.changePathTexOBj selectedArray newPath:edtNewPath.text
				
				-- Show updated paths in list:
				DisplayMode = #NewPaths
				TexRenUI.RefreshTexturesList TextureList
				
				textureList.selection = 0
				texRenUI.refreshObjectList objectList
				texRenUI.makePrefixList lst_filter
			)

		)
	
		on textureList selectionEnd  do (

			local selectedItems = textureList.selection
			local textureObjArray = #()
			
			for item in selectedItems do 
			(
				local oldNameArray = textureToolkit.makeDisplayArray #OldNames
				local textureObj = textureToolkit.getTextureObj oldNameArray[item]	
				
				if (textureObj != undefined) do 
				(
					append textureObjArray textureObj
				)
			)
			
			if (selectedItems.NumberSet == 0) then 
			(
				txt_TextureNameOld.text	= ""
				txt_TextureNameNew.text	= ""
			)
			else 
			(
				local singleTexObj = textureObjArray[1]
				
				if (selectedItems.NumberSet == 1) and (singleTexObj != undefined) then 
				(
					txt_TextureNameOld.text	= singleTexObj.filename
					txt_TextureNameNew.text	= singleTexObj.newFilename 				
				) 
				else 
				(
					txt_TextureNameOld.text = "[Multiple Selection]"
					txt_TextureNameNew.text = "[Multiple Selection]"
				)
			)
			
			if (textureObjArray.count > 0)  do 
			(
				texRenUI.refreshObjectList objectList textureObjArray:textureObjArray
			)
		)
		
		-- Toggle full/partial path-display:
		on textureList rightclick do 
		(							
			TexRenUI.ToggleDisplayMode TextureList
		)
		
		-- Change-event for 'New' name-field:
		on txt_TextureNameNew changed val do 
		(
			local selectedItems = textureList.selection
			
			if (selectedItems.NumberSet == 1) and ((val != undefined) or (val != "")) then 
			(
				for item in selectedItems do 
				(
					local oldNameArray = textureToolkit.makeDisplayArray #OldNames
					local textureObj = textureToolkit.getTextureObj oldNameArray[item]	
					
					if (textureObj != undefined) do 
					(
						textureObj.newfilename = val
						textureObj.doRenameFile = true
					)
					
					TexRenUI.RefreshTexturesList TextureList
				)
				
			)		
			
		)
		
		on lst_filter selected val do (
			if val != 0 then  (
				valText =  lst_filter.items[val]
				txt_filter.text = lst_filter.items[val]
		
				local selectList = #()
				local allTextures = textureToolkit.makeDisplayArray #NewNames displayOnly:false	
				for tex in 1 to allTextures.count do (
					
					if substring allTextures[tex] 1 valText.count == valText then append selectList tex
					
				)
				
				textureList.selection =  (selectList as bitarray)
				lst_filter.selection = 0		
			)
		)
			
		on btn_remo pressed do (
		
			theFilters = lst_filter.items
			if txt_filter.text != "" then local RemoveText = finditem theFilters txt_filter.text 
			if RemoveText != 0 and RemoveText != undefined then deleteItem theFilters RemoveText
			lst_filter.items = theFilters 
			txt_filter.text = ""
			lst_filter.selection = 0
		)
		
		on btn_add pressed do
		(
			theFilters = lst_filter.items
			if txt_filter.text != "" then appendifUnique theFilters txt_filter.text 
			lst_filter.items = theFilters 
			txt_filter.text = ""
			lst_filter.selection = 0
		)
			
		on btn_addPref pressed do
		(
			thePrefix = edtNewName.text
			if thePrefix != undefined and thePrefix !="" then 
			(	
				
				local allTextures= textureToolkit.makeDisplayArray #OldNames displayOnly:false			
				local selectedArray = for item in textureList.selection collect allTextures[item]
					

				for item in selectedArray do (	
					
					local textureObj = textureToolkit.getTextureObj item	
					if textureObj != undefined then (
						
						textureObj.doRenameFile = true
						textureObj.newFilename =(getfilenamepath textureObj.filename + (thePrefix +getfilenamefile  textureObj.filename) + getfilenametype textureObj.filename)
					)
				
					TexRenUI.RefreshTexturesList TextureList
					
					texRenUI.updateUI textureList objectList lst_filter DisplayMode errors:false clearUIinfo:false	
					
					texRenUI.makePrefixList lst_filter
				)

			)
		)		
		
		on btn_assignPref pressed do
		(
			thePrefix = edtNewName.text
			if thePrefix != undefined and thePrefix !="" then 
			(	
				local allTextures = textureToolkit.makeDisplayArray #OldNames displayOnly:false		
				local selectedNums = textureList.selection as Array
				local selectedArray = for num in selectedNums collect allTextures[num]	
					
				textureToolkit.removePrefixTexOBj selectedArray thePrefixs:lst_filter.items newPrefix:thePrefix
				
				TexRenUI.RefreshTexturesList TextureList Mode:#NewNames
				theVal = 0
				displayVal = 0
			
				textureList.selection = 0
				texRenUI.refreshObjectList objectList
			)
			
			texRenUI.makePrefixList lst_filter
			
		)		
		
		on MultipleFilesRename_Rollout open do (
			texRenUI.updateUI textureList objectList lst_filter DisplayMode errors:false updateList:true errorList:false
			clearInfo()
			texRenUI.makePrefixList lst_filter
		)
	)
-------------------------------------------------------------------------------------------------------------------------
	rollout errorFilesRename_Rollout "Error Files Rename" 
	(
		checkbox cbFilterSelected "Filter Selected Objects Only" Checked:(TexRenUI.SelObjs) align:#left pos:[10,10]
		listbox textureList "Files:" width:385 height:15  pos:[10,37]
		label lbl_click "(Right Click to Toggle Path)" pos:[40,37]
			
		button btn_ReplaceFile "Replace Selected" align:#right  pos:[298,260] width:100
			
		multilistbox objectList "Objects Using Texture" height:15 pos:[402,37] width:370
		button refreshList "Refresh" align:#right width:100 pos:[673,260] 
		
	
		
		fn refreshObjectList textureObjArray:#() = (
			
			local objNames = #()
			
			if textureObjArray.count > 0 then (
				
				for textureObj in textureObjArray do 
					if textureObj != undefined then (
						
						for obj in textureObj.objs do (
							appendifUnique objNames obj.name
						)
				)
						
				objectList.items = objNames
				objectList.selection = 0
				
			) else (
			
					objectList.items = #()
					objectList.selection = 0
			)
		)
		
		fn updateAllErrorScreens = (
			
			gRsTextureToolkitULog.clearLogDirectory()
			textureToolkit.MultipleFiles = true
			textureToolkit.doErrors = true
			textureToolkit.update()		
			
			TextureToolkit.TexturesArray = TextureToolkit.GetTexturesList()
			TexRenUI.RefreshTexturesList TextureList Reselect:False
			
			refreshObjectList()				
		)
		
		fn refreshTexturesList = 
		(
			TexRenUI.RefreshTexturesList TextureList
		)
		
		on cbFilterSelected changed state do 
		(
			TexRenUI.SelObjs = (not TexRenUI.SelObjs)
			TexRenUI.RefreshTexturesList TextureList
		)
		
		on btn_ReplaceFile pressed do 
		(			
			if (textureList.selection == 0) do return False 
			
			local item = textureList.selection
			local oldNameArray = textureToolkit.makeDisplayArray #OldNames
			local textureObj = textureToolkit.getTextureObj oldNameArray[item]	
			
			if (textureObj == undefined) do return False
			
			local filename = getOpenFileName \
					caption:"Replace Texture" \
					filename:(textureDir ) \
					types:"All File(*.*)|*.*" \
					historyCategory:textureDir
				
			if (filename == undefined) do return False
			
			textureObj.newFileName = filename
			textureObj.existsLocally = true
			textureObj.doRenameFile = true
			textureObj.updateAllMaterials textureObj.newFileName
			
			if ((grsperforce.exists filename).NumberSet == 0) do  
			(
				local newChangeList = textureToolkit.genChangeList msg:"Repathed Textures"
				grsperforce.add_or_edit #(filename) switches:#( "-c", newChangelist as string )  --exclusive:true silent:false queueAdd:true switches:#( "-c", newChangelist as string ) 
			)
			
			updateAllErrorScreens()
		)
		
		on objectList selected item do (
			clearSelection()
			
			local sel = objectList.selection as array
			local objNames = #()
			
			for idx in sel do (
				append objNames objectList.items[ idx ]
			)
			
			for objName in objNames do (
				local obj = getNodeByName objName
			
				if ( isValidNode obj ) then (
					selectMore obj
				)
			)
			
			completeRedraw()
		)			

		on textureList selected item  do (		
			
			if item != 0 then (
			
				local oldNameArray = textureToolkit.makeDisplayArray #OldNames
				local textureObj = textureToolkit.getTextureObj oldNameArray[item]	
				textureObjArray = #(textureObj)
				if textureObj != undefined   then refreshObjectList textureObjArray:textureObjArray
			)
		)
		
		-- Toggle full/partial path-display:
		on textureList rightclick do 
		(							
			TexRenUI.ToggleDisplayMode TextureList
		)

		on refreshList Pressed do (			
			updateAllErrorScreens()			
		)
		
		on errorFilesRename_Rollout open do (			
			updateAllErrorScreens()
		)
		
	)
-------------------------------------------------------------------------------------------------------------------------
	rollout RsTextureRenameRollout "Texture Rename" 
	(
		local textureToolkit = undefined
		local selectionCallback = undefined	
		local isDirty = False
			
		checkbox cbFilterSelected "Filter Selected Objects Only" Checked:TexRenUI.SelObjs align:#left across:2
		button refreshList "Refresh" align:#right	
			
		listbox textureList "Textures" height:20 across:2
		multilistbox objectList "Objects Using Texture" height:20
		
		edittext etCurrentFilename "Current Filename:" readOnly:true labelOnTop:true
		edittext etNewFilename "New Base Filename:" labelOnTop:true
		
		spinner spRemoveFirstNumChars "Remove First Characters:" type:#integer range:[ 0, 9999, 0 ] width:80 offset:[ 0, 20 ] align:#left across:4
		spinner spRemoveLastNumChars "Remove Last Characters:" type:#integer range:[ 0, 9999, 0 ] width:80 offset:[ 100, 20 ]
		
		edittext etPrefix "Prefix:" labelOnTop:true width:100
		edittext etSuffix "Suffix:" labelOnTop:true width:100 offset:[ -50, 0 ]
		
		edittext etResult "Result:" labelOnTop:true readOnly:true offset:[ 0, 10 ]
		
		checkbox cbUsePerforce "Use Perforce" checked:true across:3 align:#right offset:[ 260, 8 ] tooltip:"When checked, will perform a Perforce rename.  Otherwise, the rename operation will perform on the local files only."
		button btnRenameSelected "Rename Selected" width:120 height:30 offset:[ 200, 0 ]
		button btnRenameAll "Rename All" width:120 height:30 offset:[ 68, 0 ]
		
		
		-- Functions
		
		fn getSelectedTexture = (
			local result = undefined
			
			if textureList.selection != 0 then (
				local textureName = textureList.selected
				
				-- Strip * from name if it exists.
				if ( substring textureName ( textureName.count - 1 ) textureName.count ) == " *" then (
					textureName = substring textureName 1 ( textureName.count - 2 )
				)
				
				result = textureToolkit.getTexture textureName
			)
			
			result
		)
		
		fn dirtySelectedTexture = (
			local textureObj = getSelectedTexture()
				
			if textureObj != undefined then (
				if textureObj.doRenameFile == false then (
					local selectedIdx = textureList.selection
					local textureName = textureList.items[ selectedIdx ] + " *"
					local textureListItems = textureList.items
					
					textureListItems[ selectedIdx ] = textureName
					
					textureList.items = textureListItems
					textureList.selection = selectedIdx
				)
				
				textureObj.doRenameFile = true
			)
		)
		
		fn dirty state = 
		(
			isDirty = State
			RsTextureRenameRollout.Title = "Texture Rename"
			
			if isDirty do 
			(
				RsTextureRenameRollout.title += " *"
			)
		)
		
		fn updateResult = (
			local textureObj = getSelectedTexture()
			
			if textureObj != undefined then (
				etResult.text = textureObj.getRenamedFile()
				
			) else (
				etResult.text = ""
			)
		)
		
		fn refreshRenameControls = (
			local textureObj = getSelectedTexture()
			
			if textureObj != undefined then (
				if textureObj.newFilename == undefined then (
					etNewFilename.text = textureObj.filename
					
				) else (
					etNewFilename.text = textureObj.newFilename
				)
				
				etCurrentFilename.text = textureObj.filename
				spRemoveFirstNumChars.value = textureObj.removeFirstNumChars
				spRemoveLastNumChars.value = textureObj.removeLastNumChars
				etPrefix.text = textureObj.prefix
				etSuffix.text = textureObj.suffix
				
			) else (
				etNewFilename.text = ""
				etCurrentFilename.text = ""
				spRemoveFirstNumChars.value = 0
				spRemoveLastNumChars.value = 0
				etPrefix.text = ""
				etSuffix.text = ""
			)
		)
		
		fn refreshObjectList = (
			local textureObj = getSelectedTexture()
			
			if textureObj != undefined then (
				local objNames = #()
				
				for obj in textureObj.objs do (
					append objNames obj.name
				)
				
				objectList.items = objNames
				objectList.selection = 0
				
			) else (
				objectList.items = #()
				objectList.selection = 0
			)
		)
		
		fn refreshTexturesList Reselect:True = 
		(
			TexRenUI.RefreshTexturesList TextureList Reselect:Reselect
			Dirty TexRenUI.hasDirtyFlags
		)
		
		fn onSelectionCallback ev obj = (
			if TexRenUI.SelObjs then 
			(
				refreshTexturesList()
				refreshRenameControls()
				updateResult()
			)
		)
		
		fn registerCallbacks = (
			selectionCallback = NodeEventCallback selectionChanged:onSelectionCallback
		)
		
		fn unregisterCallbacks = (
			selectionCallback = undefined
			gc light:true
		)
		
		
		-- Events
		
		on refreshList Pressed do (
			
			textureToolkit = RsTextureToolkitStruct()
			textureToolkit.update()		

			RefreshTexturesList Reselect:False
			
			if ( gRsTextureToolkitULog.HasErrors() ) then (
				--gRsTextureToolkitULog.validate()
			)	
			
		)
		

		on cbFilterSelected changed state do 
		(
			TexRenUI.SelObjs = (not TexRenUI.SelObjs)
			refreshTexturesList()
			
			refreshRenameControls()
			refreshObjectList()
			updateResult()
		)
		
		on etPrefix changed txt do (
			local textureObj = getSelectedTexture()
			
			if textureObj != undefined do (
				textureObj.prefix = etPrefix.text
				dirty true
				dirtySelectedTexture()
				
				updateResult()
			)
		)
		
		on etSuffix changed txt do (
			local textureObj = getSelectedTexture()
			
			if textureObj != undefined do (
				textureObj.suffix = etSuffix.text
				dirty true
				dirtySelectedTexture()
				
				updateResult()
			)
		)
		
		on spRemoveFirstNumChars changed val do (
			local textureObj = getSelectedTexture()
			
			if textureObj != undefined do (
				textureObj.removeFirstNumChars = val
				dirty true
				dirtySelectedTexture()
				
				updateResult()
			)
		)
		
		on spRemoveLastNumChars changed val do (
			local textureObj = getSelectedTexture()
			
			if textureObj != undefined do (
				textureObj.removeLastNumChars = val
				dirty true
				dirtySelectedTexture()
				
				updateResult()
			)
		)
		
		on etNewFilename changed txt do (
			local textureObj = getSelectedTexture()
			
			if etNewFilename.text != "" then (
				textureObj.newFilename = etNewFilename.text
				dirty true
				dirtySelectedTexture()
				
				updateResult()
			)
		)
		
		on textureList rightClick item do (
			
			local textureObj = getSelectedTexture()
			
			if textureObj != undefined then (
				textureObj.clearRenameSettings()
				refreshTexturesList()
				refreshRenameControls()
				updateResult()
			)
		)
		
		on textureList selected item do (
			local textureObj = getSelectedTexture()
			
			if textureObj != undefined then (
				refreshRenameControls()
				refreshObjectList()
				updateResult()
			)
		)
		
		on objectList selected item do (
			clearSelection()
			
			local sel = objectList.selection as array
			local objNames = #()
			
			for idx in sel do (
				append objNames objectList.items[ idx ]
			)
			
			for objName in objNames do (
				local obj = getNodeByName objName
			
				if ( isValidNode obj ) then (
					selectMore obj
				)
			)
			
			completeRedraw()
		)
		
		on btnRenameAll pressed do (
			if textureToolkit.p4changeList == undefined then textureToolkit.genChangeList()
			local newChangeList =  textureToolkit.p4changeList
			if ( textureToolkit.renameAllTextures cbUsePerforce.checked false newChangeList) == true then (
				dirty false
				refreshTexturesList()
			)
			
			if ( gRsTextureToolkitULog.HasWarnings() ) or ( gRsTextureToolkitULog.HasErrors() ) then (
				gRsTextureToolkitULog.showULogFile()
			)
		)
		
		on btnRenameSelected pressed do (
			local textureObj = getSelectedTexture()
			
			if textureObj != undefined then (
				local justTextureName = toLower ( filenameFromPath textureObj.filename )
				
				if textureToolkit.p4changeList == undefined then textureToolkit.genChangeList()
				local newChangeList =  textureToolkit.p4changeList
				
				if ( textureToolkit.renameTexture justTextureName cbUsePerforce.checked false newchangelist) == true then (
					
					local texturesList = textureToolkit.getTexturesList()
					local dirtyTextures = #()
					
					for textureName in texturesList do (
						local textureObj = textureToolkit.getTexture textureName
						
						if textureObj.doRenameFile == true then (
							append dirtyTextures true
						)
					)
					
					if dirtyTextures.count == 0 then (
						dirty false
						
					) else (
						dirty true
					)
				)
				
				refreshTexturesList()
				
				if ( gRsTextureToolkitULog.HasWarnings() ) or ( gRsTextureToolkitULog.HasErrors() ) then (
					gRsTextureToolkitULog.showULogFile()
				)
			)
		)
		
		on RsTextureRenameRollout open do (
			
			
			textureToolkit = RsTextureToolkitStruct()
			textureToolkit.MultipleFiles = false
			textureToolkit.doErrors = false
			textureToolkit.update()
			
			registerCallbacks()
			
			RefreshTexturesList Reselect:False
			
			if ( gRsTextureToolkitULog.HasErrors() ) then (
				gRsTextureToolkitULog.validate()
			)
		)
		
		on RsTextureRenameRollout close do 
		(
			unregisterCallbacks()
			
			if isDirty do 
			(
				local msg = "You have uncommitted renames on the following textures:\n\n"
				
				local texturesList = textureToolkit.getTexturesList()
				
				for textureName in texturesList do (
					local textureObj = textureToolkit.getTexture textureName
					
					if textureObj != undefined do (
						if textureObj.doRenameFile then (
							msg += ( filenameFromPath textureObj.filename ) + "\n"
						)
					)
				)
				
				msg += "\nWould you like to rename these files now?"
				
				if queryBox msg title:"Texture Rename" beep:true do 
				(
					print "renaming textures..."			
					
					if textureToolkit.p4changeList == undefined then textureToolkit.genChangeList()
					
					local newChangeList =  textureToolkit.p4changeList
					if ( textureToolkit.renameAllTextures cbUsePerforce.checked newChangeList) == true then (
						dirty false
						refreshTexturesList()
					)
					
					if ( gRsTextureToolkitULog.HasWarnings() ) or ( gRsTextureToolkitULog.HasErrors() ) then (
						gRsTextureToolkitULog.showULogFile()
					)
					
				)
			)
		)
	)
-------------------------------------------------------------------------------------------------------------------------
	global  TextureRenameTools_Rollout_Array = #(
	  	#("Multiple Files Rename",#(MultipleFilesRename_Rollout),730),
		#("Error Files Rename",#(errorFilesRename_Rollout),375),	
		#("Texture Rename",#(RsTextureRenameRollout),615)
	)   
-------------------------------------------------------------------------------------------------------------------------
	rollout TextureRenameTools_Rollout "Texture Tools"
	(
		dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:TextureRenameTools_Rollout.width
		local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Texture Rename and Repath" versionNum:1.10 versionName:"Acoustic Stone"
		dotNetControl dn_tabs "System.Windows.Forms.TabControl" height:20 width:800 align:#left
		subRollout theSubRollout width:800 height:740 align:#center offset:[0,0]
	--	subRollout theSubRollout1 width:800 height:750 align:#center offset:[0,0]
			
		on dn_tabs Selected itm do
		(
			if LastSubRollout != (itm.TabPageIndex+1) do --do not update if the same tab clicked twice
			(
				for subroll in TextureRenameTools_Rollout_Array[LastSubRollout][2] do
					removeSubRollout theSubRollout subroll
				for subroll in TextureRenameTools_Rollout_Array[LastSubRollout = itm.TabPageIndex+1][2] do	(								
					addSubRollout theSubRollout subroll
		--			addSubRollout theSubRollout1 subroll
				)
				TextureRenameTools_Rollout.height = TextureRenameTools_Rollout_Array[LastSubRollout = itm.TabPageIndex+1][3] 				
			) 
		)--end tabs clicked
-------------------------------------------------------------------------------------------------------------------------
		on TextureRenameTools_Rollout open do
		(
			for aTab in TextureRenameTools_Rollout_Array do
				(dn_tabs.TabPages.add aTab[1])
			for subroll in TextureRenameTools_Rollout_Array[1][2] do (
				addSubRollout theSubRollout subroll
			
			)
		--	addSubRollout theSubRollout1 TextureRenameTools_Rollout_Array[2][2][1]
			banner.setup()
		)
	)
)
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
 
createDialog TextureRenameTools_Rollout 800 (TextureRenameTools_Rollout_Array[1][3]) style:#(#style_resizing,#style_titlebar, #style_toolwindow, #style_sysmenu)
