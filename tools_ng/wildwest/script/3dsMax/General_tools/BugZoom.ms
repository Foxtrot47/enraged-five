filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
filein "pipeline/util/RAG_funcs.ms"

try( DestroyDialog BugZoom ) catch()
rollout BugZoom "BugZoom" width:200 height:170
(
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--		VARIABLES
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	local regex = dotnetclass "System.Text.RegularExpressions.Regex"
	
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--		CONTROLS
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	----Banner
	dotNetControl rsBannerPanel "Panel" height:32 width:(BugZoom.Width) pos:[0,0]
	local banner = makeRsBanner versionNum:1.12 versionName:"Needy Balls" dn_Panel:rsBannerPanel wiki:"bugzoom"
	
	editText edt1 "Coordinates:" Text:"0,0,0" offset:[4,-2] labelontop:True fieldWidth:103 align:#right
	button btn1 "Zoom View:" width:70 height:30 align:#left offset:[-2,-33] across:2 Tooltip:"Zoom viewport to Coordinates"
	spinner spnDist "Zoom Distance:" range:[0.1,20,0.1] fieldWidth:60 align:#right offset:[4,1] Tooltip:"Tweak bugzoom distance"
	
	button btnGameToMax "Game -> Viewport" tooltip:"Sync Viewport Camera to Game Camera" width:(BugZoom.Width - 20) height:21 offset:[0,6]
	button btnMaxToGame "Viewport -> Game" tooltip:"Sync Game Camera to Viewport Camera" width:(BugZoom.Width - 20) height:21
	
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--	FUNCTIONS
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	--//////////////////////////////////////////////////////////////////////////
	-- 
	--//////////////////////////////////////////////////////////////////////////
	fn setViewportCamera InputPosition InputRotation =
	(		
 		TempCamera = FreeCamera Name:"__TEMP__" FOV:65

		TempCamera.Position = InputPosition
 		Rotate TempCamera (angleaxis InputRotation.X [1,0,0])
 		Rotate TempCamera (angleaxis InputRotation.Z [0,0,1])
	
 		viewport.setCamera TempCamera
 		RedrawViews()
		Delete TempCamera
	)
	
	--//////////////////////////////////////////////////////////////////////////
	-- place the viewport camera at the coords specified
	-- or at the selected bugnode
	--//////////////////////////////////////////////////////////////////////////
	fn zoomCamera Coords:[0,0,0] =
	(
		-- Convert coordinates-string to point3:
		if (isKindOf Coords String) do 
		(
			-- Split string up:
			local Matches = Regex.Matches Coords "-?[0-9]+(\.?[0-9]+)?"
			
			-- Convert number-strings to floats:
			if (Matches.Count == 3) do 
			(
				Coords = [0,0,0]
				
				for n = 1 to 3 do 
				(
					Coords[n] = (Matches.Item[n - 1].Value as Number)
				)
			)
		)
		
		if (not isKindOf Coords Point3) do return False
		
		format "Coords: % \n" Coords 
		
		local viewCam = viewport.getcamera()
		
		if viewCam != undefined then
		(
			viewCam.pos = Coords
		)
		else
		(
			-- Zoom extents to temporary object:
			undo off 
			(
				local TempObj = Sphere radius:(spnDist.Value) smooth:on segs:4 chop:0 slice:off sliceFrom:0 sliceTo:0 mapcoords:on recenter:off pos:Coords isSelected:on
				max tool zoomextents
				delete TempObj
			)
		)
		
		RsRagFuncs.setPlayerPos Coords
	)
	
	--//////////////////////////////////////////////////////////////////////////
	-- Called when a bugnode is selected
	--//////////////////////////////////////////////////////////////////////////
	fn update =
	(
		local SelObj = (GetCurrentSelection())[1]
		
		if (isKindOf SelObj RsBugNode) then
		(
			local bugPos = SelObj.Pos
			
			--move it to the bug position
			setViewportCamera bugPos
			
			edt1.text = (bugPos as String)
			
			zoomCamera coords:bugPos
		)
	)
	
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--		EVENTS
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	--//////////////////////////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////////////////////////
	on BugZoom open do
	(
		banner.setup()
		callbacks.addScript #selectionSetChanged "BugZoom.update()" id:#bugzoom
	)
	
	--//////////////////////////////////////////////////////////////////////////
	-- remove selection callback
	--//////////////////////////////////////////////////////////////////////////
	on BugZoom close do
	(
		callbacks.removeScripts id:#bugzoom
	)
	
	--//////////////////////////////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////////////////////////////
	on edt1 Entered NewText do 
	(
		zoomCamera coords:NewText
	)
	on btn1 pressed do 
	(
		zoomCamera coords:edt1.Text
	)
	
	--//////////////////////////////////////////////////////////////////////////
	-- Get max camera pos from current game(debug) camera
	--//////////////////////////////////////////////////////////////////////////
	on btnGameToMax pressed do
	(
		local gameCamPos = RsRagFuncs.getGameDebugCamPos()
		local gameCamRot = RsRagFuncs.getGameDebugCamRot()
		
		--Check if the value we have been returned is actually origin - this means the user is not in the debug camera
		if( gameCamPos == [0,0,0] ) then return()
		
		setViewportCamera gameCamPos gameCamRot
	)
	
	on btnMaxToGame pressed do
	(
		if ( viewport.getType() == #view_persp_user ) then
		(	
			TempCamera = getActiveCamera() 
			if( TempCamera == Undefined ) then
			(
				macros.run "Lights and Cameras" "Camera_CreateFromView"
				TempCamera = Objects[Objects.Count-1]
			)
			else
			(
				TempCamera = Copy TempCamera
			)
			
			TempCamera.Type = #Free
			
			local gameCamPos = TempCamera.Position
			local gameCamRot = QuatToEuler TempCamera.Rotation Order:6
			gameCamRot = eulerAngles -gameCamRot.Z -gameCamRot.Y -gameCamRot.X

			RsRagFuncs.setGameDebugCamPos gameCamPos
			RsRagFuncs.setGameDebugCamRot gameCamRot
			
			delete TempCamera
		)
		else
		(
			MessageBox "This will only work if you are in perspective mode."
		)
	) 
)
createdialog BugZoom
