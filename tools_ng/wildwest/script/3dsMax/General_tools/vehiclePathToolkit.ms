try (destroyDialog RsVehiclePathToolkitRollout) catch()

struct RsVehiclePathToolkitStruct
(
	
	
	fn hideShowObjectsByAttribute attributeIndexList hideShowBool =
	(
		
		local vehicleNodes= for obj in helpers where classof obj == VehicleNode collect obj
		for vNode in vehicleNodes do
		(
			local attributeBools = for selAttrs in attributeIndexList where getAttr vNode selAttrs == True collect obj
				
			--local attrIndex = GetAttrIndex "VehicleNode" "Highway"
			if attributeBools.count > 0 then
			(
				if hideShowBool == 0 then
				(
					hide vNode
				)
				else
				(
					unHide vNode
				)
			)
		)
	),

	fn getAttributeIndecies attributeList = 
	(
		print attributeList
		local attrIndecies = #()
		for attribute in attributeList do
		(
			
			attrIndex = getAttrIndex "VehicleNode" attribute
			--print (attribute + " " + attrIndex)
			append attrIndecies attrIndex
		)
		
		print attrIndecies
		attrIndecies
	)
)

RsVehiclePathToolkit = RsVehiclePathToolkitStruct()

rollout RsVehiclePathToolkitRollout "vehiclePathToolkit"
(
	local densityValues = #("0", "1","2","3","4","5","6","7","8","9","10","11","12","13","14","15")
	group ""
	(
		button setDensityBtn "Set Density on Selected" width:120 align:#left 
		dropdownlist densityDropdown "Density Setting" items:densityValues width:40 align:#right offset:[0,-35]
		button isolateDensityButton "Isolate By Density" width:100 offset:[0,0] align:#left
		button showAllButton "Show All" width:50 offset:[0, -26] align:#right
		label densityOutputLabel "" style_sunkenedge:true height: 20 width:180
	)
	
	group "Hide/Show"
	(
		checkbox highwayChk "Highway" 
		checkbox waterChk "Water" 
		checkbox tunnelChk "Tunnel" 
		checkbox offRoadChk "Off Road"
	
		button hideButton "Hide Checked" width:80 align:#left 
		button hideVLinkButton "Hide Vehicle Links" width:95 align:#right offset:[3,-26]
		button showButton "Show Checked" width:80 align:#left 
		button showVLinkButton "Show Vehicle Links" width:95 align:#right offset:[3,-26]
		
		

	)
	
	on showAllButton pressed do
	(
		local vehicleNodes= for obj in helpers where classof obj == VehicleNode collect obj
		for vNode in vehicleNodes do
		(
			unhide vNode
		)
		
	)
	
	on hideVLinkButton pressed do
	(
		local vLinks = for vLink in helpers where classof vLink == VehicleLink collect vLink
		for vLink in vLinks do
		(
			hide vLink
		)
	)
	
	on showVLinkButton pressed do
	(
		local vLinks = for vLink in helpers where classof vLink == VehicleLink collect vLink
		for vLink in vLinks do
		(
			unhide vLink
		)
	)
	on isolateDensityButton pressed do
	(
		local attrIndex = (RsVehiclePathToolkit.getAttributeIndecies #("Density"))
		local vehicleNodes= for obj in helpers where classof obj == VehicleNode collect obj
		local hideCount = 0
		for vNode in vehicleNodes do
		(
			if (getAttr vNode attrIndex[1]) == (densityDropdown.selected as integer) then
			(
				unhide vNode
				hideCount += 1
			)
			else
			(
				--print vNode
				hide vNode
				
			)
		)
		densityOutputLabel.text = ("Isolated " + (hideCount as string) + " objects.")
	)
	
	on hideButton pressed do
	(
		
		local checkBoxes = #(highwayChk, waterChk, tunnelChk, offRoadChk)
		local attributeList = #()
		
		local boxesCheckedOn =  for chkBox in checkBoxes where chkBox.checked == True collect chkBox
		for checkedOn in boxesCheckedOn do
		(
			
			append attributeList checkedOn.caption
		)
		print attributeList
		
		local attributeIndexList = RsVehiclePathToolkit.getAttributeIndecies attributeList
	
		
		RsVehiclePathToolkit.hideShowObjectsByAttribute attributeIndexList 0
		
	)
	
	on showButton pressed do
	(
		local checkBoxes = #(highwayChk, waterChk, tunnelChk, offRoadChk)
		local attributeList = #()
		
		local boxesCheckedOn =  for chkBox in checkBoxes where chkBox.checked == True collect chkBox
		for checkedOn in boxesCheckedOn do
		(
			append attributeList checkedOn.caption
		)
		local attributeIndexList = RsVehiclePathToolkit.getAttributeIndecies attributeList
		RsVehiclePathToolkit.hideShowObjectsByAttribute attributeIndexList 1
	)

	on setDensityBtn pressed do
	(
		
		local selectedObject = (getCurrentSelection() as array)
		local count = 0
		for selObj in selectedObject do
		(
			if classOf selObj == VehicleNode then
			(
				local attrIndex = GetAttrIndex "VehicleNode" "Density" 
				if selObj.isHidden == False then
				(
					setAttr selObj attrIndex (densityDropdown.selected as integer)
					count += 1
				)
			)
		)
		densityOutputLabel.text = ("Set Density to: " + densityDropdown.selected + " on " + (count as string) + " objects.")
	)
)
createDialog RsVehiclePathToolkitRollout 200 300
	