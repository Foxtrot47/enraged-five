struct TextureReporterHeaders
(
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	--TextureData
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	TextureData_headers = #("DDS Name",
							"DDS Path",
							"RPF Name",
							"Format",
							"Alpha",
							"TCS",
							"TCP",
							"Shader Usage",
							"Shaders",
							"UsedBy",
							"Bumpiness",
							"Specular",
							"Source Path",
							"Missing",
							"Flat",
							"Asset Mismatch",
							"Check-in",
							"User"),

	TextureData_columnRemap = #(
								(DataPair field:#dds_name header:"DDS Name"),
								(DataPair field:#dds_uri header:"DDS Path"),
								(DataPair field:#dds_rpfName header:"RPF Name"),
								(DataPair field:#texFormat header:"Format"),
								(DataPair field:#texAlpha header:"Alpha"),
								(DataPair field:#tcs header:"TCS"),
								(DataPair field:#tcp header:"TCP"),
								(DataPair field:#shaderUsage header:"Shader Usage"),
								(DataPair field:#shaders header:"Shaders"),
								(DataPair field:#UseName header:"UsedBy"),
								(DataPair field:#bumpiness header:"Bumpiness"),
								(DataPair field:#specularVal header:"Specular"),
								(DataPair field:#source_uri header:"Source Path"),
								(DataPair field:#src_missing header:"Missing"),
								(DataPair field:#flat header:"Flat"),
								(DataPair field:#cacheMismatch header:"Asset Mismatch"),
								(DataPair field:#lastCheckin header:"Check-in"),
								(DataPair field:#lastUser header:"User")
							),
							
	TextureData_rawColumnIndex = 	#(	
										#uid,						--line number
										#dds_uri,
										#dds_rpfName,
										#dds_name,
										#texFormat,
										#texAlpha,
										0,						--CRC
										#tcs,
										#tcp,
										#shaderUsage,
										#shaders,
										#UseName,
										#bumpiness,
										#specularVal,
										#source_uri,
										#src_missing,
										#flat,
										#cacheMismatch,
										#lastCheckin,
										#lastUser
									),
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	--DrawableData
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------							
	DrawableData_columnRemap = #(
									(DataPair field:#dds_name header:"DDS Name"),
									(DataPair field:#dds_uri header:"DDS Path"),
									(DataPair field:#dds_rpfName header:"RPF Name"),
									(DataPair field:#idr_uri header:"Drawable Path"),
									(DataPair field:#idr_rpfName header:"Drawable RPF"),
									(DataPair field:#shaderName header:"ShaderName"),
									(DataPair field:#shaderCount header:"Shader Count"),
									(DataPair field:#shaderUsage header:"Shader Usage"),
									(DataPair field:#intensity header:"Intensity"),
									(DataPair field:#scaleWarn header:"Scale Warn")
								),
							
	DrawableData_rawColumnIndex =	#(
										#uid,
										#dds_uri,
										#dds_rpfName,
										#dds_name,
										#idr_uri,
										#idr_rpfName,
										#shaderName,
										#shaderCount,
										#shaderUsage,
										#intensity,
										#scaleWarn
									),
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	--ShaderData
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------								
	ShaderData_columnRemap =	#(
									(DataPair field:#idr_uri header:"Drawable Path"),
									(DataPair field:#rpfName header:"RPF Name"),
									(DataPair field:#shaderName header:"ShaderName"),
									(DataPair field:#shaderCount header:"Shader Count")
								),
								
	ShaderData_rawColumnIndex =	#(
									#uid,
									#idr_uri,
									#rpfName,
									#shaderName,
									#shaderCount
								),
	
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	--GameData
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------								
	GameData_headers = #(	"Asset Path",
							"RPF Name",
							"Error",
							"Other Path"),
							
	GameData_columnRemap =	#(
								(DataPair field:#asset_uri header:"Asset Path"),
								(DataPair field:#asset_rpfName header:"RPF Name"),
								(DataPair field:#errorStr header:"Error"),
								(DataPair field:#other_uri header:"Other Path")
							),
	
	GameData_rawColumnIndex =	#(
									#uid,
									#asset_uri,
									#asset_rpfName,
									#errorStr,
									#other_uri
								),
								
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	--SceneData
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	SceneData_columnRemap = #(
								(DataPair field:#data1 header:"Data1"),
								(DataPair field:#data2 header:"Data2"),
								(DataPair field:#data3 header:"Data3"),
								(DataPair field:#data4 header:"Data4"),
								(DataPair field:#data5 header:"Data5"),
								(DataPair field:#data6 header:"Data6"),
								(DataPair field:#data7 header:"Data7"),
								(DataPair field:#data8 header:"Data8")								
							),
							
	SceneData_rawColumnIndex =	#(	
									#data1,
									#data2,
									#data3,
									#data4,
									#data5,
									#data6,
									#data7,
									#data8
									
								),
	
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	--TXD HeirarchyData
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------							
	HierarchyData_columnRemap = #(
									(DataPair field:#dds_uri header:"DDS Path"),
									(DataPair field:#asset_rpfName header:"RPF Name"),
									(DataPair field:#dds_name header:"DDS Name"),
									(DataPair field:#parent_txd header:"Parent TXD"),
									(DataPair field:#scoreup header:"Score Up"),
									(DataPair field:#scoreup_details header:"Score Up Details"),
									(DataPair field:#scoredown header:"Score Down"),
									(DataPair field:#scoredown_details header:"Score Down Details"),
									(DataPair field:#onlychild_path header:"Only Child Path"),
									(DataPair field:#error header:"Error")
								),
							
	HierarchyData_rawColumnIndex = #(
										#uid,
										#dds_uri,
										#asset_rpfName,
										#dds_name,
										#parent_txd,
										#scoreup,
										#scoreup_details,
										#scoredown,
										#scoredown_details,
										#onlychild_path,
										#error
									)
)

--Create
textureReporterHeaders = TextureReporterHeaders()

struct TextureData
(
	rawString,
	uid,
	dds_uri,
	dds_name,
	dds_rpfName,
	texFormat,
	texAlpha,
	shaderUsage,
	shaders,
	useName =#(),
	bumpiness,
	specularVal,
	source_uri,
	src_missing,
	tcsPath,
	tcs,
	tcpPath,
	tcp,
	flat,
	cacheMismatch,
	lastCheckin,
	lastUser,
	
	extraData = #()
	
)

--The error reports ending in *.drawable.csv come from the analysis_drawables.csv file
struct DrawableData
(
	rawString,
	uid,
    dds_uri,
    dds_rpfName,
	dds_name,
    idr_uri,
    idr_rpfName,
    shaderName,
    shaderCount,
    shaderUsage,
    intensity,
    scaleWarn
)

--The error reports ending in *.shader.csv come from the analysis_shaders.csv file
struct ShaderData
(
	rawString,
	uid,
	idr_uri,
    rpfName,
    shaderName,
    shaderCount
)


--The error reports ending in *.game.csv come from the texture_usage.log file (which is generated during the Streaming Iterator phase),
struct GameData
(
	rawString,
	uid,
    asset_uri,
    asset_rpfName,
    errorStr,
	other_uri
)

struct SceneData
(
	rawString,
	uid = 0,
	data1,
	data2,
	data3,
	data4,
	data5,
	data6,
	data7,
	data8
)

struct HierarchyData
(
	rawString,
	uid,
	dds_uri,
	asset_rpfName,
	dds_name,
	parent_txd,
	scoreup,
	scoreup_details,
	scoredown,
	scoredown_details,
	onlychild_path,
	error
)

