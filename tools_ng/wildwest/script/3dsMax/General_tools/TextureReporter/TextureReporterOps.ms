struct RecordItemStruct
(
	asset,
	reportType,
	reportRecord
)

struct SceneDiagnosisStruct
(
	reportsRoot 	= (RsConfigGetLocalWildWestAppData() + "/Analysis/analysis/all"),
	
	--@@@@@@@@@@@@@@@
	--TextureIssues
	--@@@@@@@@@@@@@@@
	templateBadLink_Path 				= pathConfig.appendPath reportsRoot "ErrorReport_TemplateBadLink.textures.csv",
	templateDefault_Path 				= pathConfig.appendPath reportsRoot "ErrorReport_TemplateDefault.textures.csv",
	templateInconsistent_Path			= pathConfig.appendPath reportsRoot "ErrorReport_TemplateInconsistent.textures.csv",
	MultipleUses_Path					= pathConfig.appendPath reportsRoot "ErrorReport_MultipleUses.textures.csv",
	NotUsed_Path						= pathConfig.appendPath reportsRoot "ErrorReport_NotUsed.textures.csv",
	CompressedNotProcessed_Path			= pathConfig.appendPath reportsRoot "ErrorReport_CompressedNotProcessed.textures.csv",
	Compressed_IncompleteMipChain_Path	= pathConfig.appendPath reportsRoot "ErrorReport_Compressed_IncompleteMipChain.textures.csv",
	UncompressedSkipProcessing_Path		= pathConfig.appendPath reportsRoot "ErrorReport_UncompressedSkipProcessing.textures.csv",
	Ineffective_Path					= pathConfig.appendPath reportsRoot "ErrorReport_Ineffective.textures.csv",
	Flat_Path							= pathConfig.appendPath reportsRoot "ErrorReport_Flat.textures.csv",
	HasUnusedAlpha_Path					= pathConfig.appendPath reportsRoot "ErrorReport_HasUnusedAlpha.textures.csv",
	HDTexAppliedToTerrainShader_Path	= pathConfig.appendPath reportsRoot "ErrorReport_HDTexAppliedToTerrainShader.textures.csv",
	HDTexAppliedToLargeDrawable_Path	= pathConfig.appendPath reportsRoot "ErrorReport_HDTexAppliedToLargeDrawable.textures.csv",
	
	--@@@@@@@@@@@@@@@
	--Shader Issues
	--@@@@@@@@@@@@@@@
	ShouldNotUseBump_Path 				= pathConfig.appendPath reportsRoot "ErrorReport_ShouldNotUseBump.textureusage.csv",
	ShouldNotUseSpec_Path 				= pathConfig.appendPath reportsRoot "ErrorReport_ShouldNotUseSpec.textureusage.csv",
	UnexpectedSpecMask_Path				= pathConfig.appendPath reportsRoot "ErrorReport_UnexpectedSpecMask.textureusage.csv",
	UsingRareShader_Path				= pathConfig.appendPath reportsRoot "ErrorReport_UsingRareShader.textureusage.csv",
	UsingNonCableTexture_Path			= pathConfig.appendPath reportsRoot "ErrorReport_UsingNonCableTexture.textureusage.csv",
	UsingDeprecatedShader_Wire_Path		= pathConfig.appendPath reportsRoot "ErrorReport_UsingDeprecatedShader_Wire.shaders.csv",
	UsingDeprecatedShader_Water_Path	= pathConfig.appendPath reportsRoot "ErrorReport_UsingDeprecatedShader_Water.shaders.csv",
	UsingDeprecatedShader_Other_Path	= pathConfig.appendPath reportsRoot "ErrorReport_UsingDeprecatedShader_Other.shaders.csv",
	
	
	textureIssues	= #(),
	shaderIssues	= #(),
	drawableIssues	= #(),
	
	sceneTextures	= #(),
	sceneTXD		= #(),
	sceneShaders	= #(),
	drawables		= #(),
	
	dumpLogPath = "c:/temp/",
	
	--//////////////////////////////////////////////////
	-- 	UTILITY FUNCS
	--/////////////////////////////////////////////////
	
	--//////////////////////////////////////////////////////////////////////////////////
	-- debug dump all the texture issue reports collected 
	--from the csv to a log file for further investigation or distribution
	--//////////////////////////////////////////////////////////////////////////////////
	fn dumpTextureIssueRawReportsToLog =
	(
		if textureIssues.count == 0 then return false
		
		--logname
		local logName = (dumpLogPath + (getfilenamefile maxFileName) + "_TextureIssues.log") 
		local fH = undefined
		
		if doesFileExist logName != true then
		(
			fH = createFile logName
		)
		else	--deletethe old one and create anew
		(
			deleteFile logName
			fH = createFile logName
		)
		
		--now dump the log
		for item in textureIssues do format "%\n" item.reportRecord[1] to:fH
			
		close fH
	),
	
	--//////////////////////////////////////////////////////////////////////////////////
	-- Same as a bove but for the shader issues
	--//////////////////////////////////////////////////////////////////////////////////
	fn dumpShaderIssueRawReportsToLog =
	(
		if shaderIssues.count == 0 then return false
			
		--logname
		local logName = (dumpLogPath + (getfilenamefile maxFileName) + "_ShaderIssues.log") 
		local fH = undefined
		
		if doesFileExist logName != true then
		(
			fH = createFile logName
		)
		else	--deletethe old one and create anew
		(
			deleteFile logName
			fH = createFile logName
		)
		
		--now dump the log
		for item in shaderIssues do format "%\n" item.reportRecord[1] to:fH
			
		close fH
	),
	
	--//////////////////////////////////////////////////////////////////////////////////
	-- Create report record
	--//////////////////////////////////////////////////////////////////////////////////
	fn createReportRecord item &dict reportType =
	(
		--create a new recordItemStruct instance
		local recordItem = RecordItemStruct()
		recordItem.asset = item
		recordItem.reportType = reportType
		key = tolower (getFileNameFile item.filename)
		dict.TryGetValue key &recordVal
		--turn recordVal into an max array
		recordItem.reportRecord = recordVal.toArray()
		
		recordItem
	),
	
	--//////////////////////////////////////////////////////////////////////////////////
	-- 
	--//////////////////////////////////////////////////////////////////////////////////
	fn checkReportPath thePath =
	(
		if doesFileExist thePath == false then
		(
			format "Report Path not found: % \n" thePath
			return false
		)
		else return true
	),
	
	--//////////////////////////////////////////////////////////////////////////////////
	-- Check the scene textures for the textures report type
	--//////////////////////////////////////////////////////////////////////////////////
	fn textureIssueDiagnostic reportType reportPath columnIndex:3 =
	(
		if checkReportPath reportPath == false then return false
		
		local the_Dict = CSV2Dict.process reportPath columnIndex
		
		--search for textures keys
		for tex in sceneTextures do
		(
			local textureName = tolower (getFileNameFile tex.filename)
			if the_Dict.containsKey textureName == true then --add the values to the textureMatches list
			(
				--just append the whole record for later processing as a recordItem instance
				local theRecord = createReportRecord tex &the_Dict reportType
				append textureIssues theRecord
			)
		)
	),
	
	--//////////////////////////////////////////////////////////////////////////////////
	-- Check the scene textures for the textureUsage report type
	--//////////////////////////////////////////////////////////////////////////////////
	fn shaderIssueDiagnostic reportType reportPath columnIndex:3 =
	(
		if checkReportPath reportPath == false then return false
		
		--create dictionary key:texturename value:reportcontents
		local theDict = CSV2Dict.process reportPath columnIndex
		
		--local problemShaders = #()
		--iterate scene textures and match against any report issues
		for tex in sceneTextures do
		(
			local textureName = tolower (getFileNameFile tex.filename)
			if theDict.ContainsKey textureName == true then
			(
				--Create a record object
				local theRecord = createReportRecord tex &theDict reportType
				
				--now find the shaders of the right type using this texture in the scene
				local shaderType = theRecord.reportRecord[1][7]
				
				--go through the scene shaders and match against shaderType
				local shadersOfType = for shd in sceneShaders where getFileNameFile(RstGetShaderName shd) == shaderType collect shd
					
				--then look for any that use textureName
				for shd in shadersOfType do
				(
					local shdBitmaps = for bm in (refs.dependsOn shd) where classOf bm == Bitmaptexture collect bm
					--for bm in shdBitmaps do format "textureName: % bm.filename: % match: % \n" textureName (getFileNameFile bm.filename) (matchPattern (getFileNameFile bm.filename) pattern:textureName ignoreCase:true)
					
					for bm in shdBitmaps do
					(
						if matchPattern (getFileNameFile bm.filename) pattern:textureName ignoreCase:true == true then
						(
							theRecord.asset = shd
							append shaderIssues theRecord
						)
					)
				)
			)
		)
		--format "problemShaders: % \n" shaderIssues
	),
	
	--/////////////////////////////////////////////////
	--	DIAGNOSIS FUNCTIONS
	--/////////////////////////////////////////////////
	
	--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	--TextureIssues
	--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	
	--///////////////////////////////////////////////////////////////////////
	-- ErrorReport_TemplateBadLink.textures
	-- report column D: DDS texture name == index:3
	--///////////////////////////////////////////////////////////////////////
	fn templateBadLink = 
	(
		textureIssueDiagnostic "TemplateBadLink" templateBadLink_Path columnIndex:3
	),
	
	--///////////////////////////////////////////////////////////////////////
	--TemplateDefault.textures 
	--///////////////////////////////////////////////////////////////////////
	fn templateDefault =
	(
		textureIssueDiagnostic "TemplateDefault" templateDefault_Path columnIndex:3
	),
	
	--///////////////////////////////////////////////////////////////////////
	--TemplateInconsistent.textures 
	--///////////////////////////////////////////////////////////////////////
	fn templateInconsistent =
	(
		textureIssueDiagnostic "TemplateInconsistent" templateDefault_Path columnIndex:3
	),
	
	--///////////////////////////////////////////////////////////////////////
	-- MultipleUses.textures
	--///////////////////////////////////////////////////////////////////////
	fn multipleUses =
	(
		textureIssueDiagnostic "MultipleUses" MultipleUses_Path columnIndex:3
	),
	
	--///////////////////////////////////////////////////////////////////////
	--NotUsed.textures
	--///////////////////////////////////////////////////////////////////////
	fn notUsed = 
	(
		textureIssueDiagnostic "NotUsed" NotUsed_Path columnIndex:3
	),
	
	--///////////////////////////////////////////////////////////////////////
	--CompressedNotProcessed.textures 
	--///////////////////////////////////////////////////////////////////////
	fn compressedNotProcessed = 
	(
		textureIssueDiagnostic "CompressedNotProcessed" CompressedNotProcessed_Path columnIndex:3
	),
	
	--///////////////////////////////////////////////////////////////////////
	--Compressed_IncompleteMipChain.textures 
	--///////////////////////////////////////////////////////////////////////
	fn compressed_IncompleteMipChain =
	(
		textureIssueDiagnostic "Compressed_IncompleteMipChain" Compressed_IncompleteMipChain_Path columnIndex:3
	),
	
	--///////////////////////////////////////////////////////////////////////
	--UncompressedSkipProcessing.textures
	--///////////////////////////////////////////////////////////////////////
	fn uncompressedSkipProcessing = 
	(
		textureIssueDiagnostic "UncompressedSkipProcessing" UncompressedSkipProcessing_Path columnIndex:3
	),
	
	--///////////////////////////////////////////////////////////////////////
	--Ineffective.textures
	--///////////////////////////////////////////////////////////////////////
	fn ineffective =
	(
		textureIssueDiagnostic "Ineffective" Ineffective_Path columnIndex:3
	),
	
	--///////////////////////////////////////////////////////////////////////
	--Flat.textures 
	--///////////////////////////////////////////////////////////////////////
	fn flat =
	(
		textureIssueDiagnostic "Flat" Flat_Path columnIndex:3
	),
	
	--///////////////////////////////////////////////////////////////////////
	--HasUnusedAlpha.textures 
	--///////////////////////////////////////////////////////////////////////
	fn hasUnusedAlpha =
	(
		textureIssueDiagnostic "HasUnusedAlpha" HasUnusedAlpha_Path columnIndex:3
	),
	
	--///////////////////////////////////////////////////////////////////////
	--HDTexAppliedToTerrainShader.textures
	--///////////////////////////////////////////////////////////////////////
	fn hdTexAppliedToTerrainShader =
	(
		textureIssueDiagnostic "HDTexAppliedToTerrainShader" HDTexAppliedToTerrainShader_Path columnIndex:3
	),
	
	--///////////////////////////////////////////////////////////////////////
	--HDTexAppliedToLargeDrawable.textures 
	--///////////////////////////////////////////////////////////////////////
	fn hdTexAppliedToLargeDrawable = 
	(
		textureIssueDiagnostic "HDTexAppliedToLargeDrawable" HDTexAppliedToLargeDrawable_Path columnIndex:3
	),
	
	
	
	--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	--Shader Issues
	--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	-- ErrorReport_ShouldNotUseBump.textureusage
	-- report column D: DDS texture name == columnIndex:3
	--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn shouldNotUseBump =
	(
		shaderIssueDiagnostic "ShouldNotUseBump" ShouldNotUseBump_Path columnIndex:3
	),
	
	--////////////////////////////////////////////////////////////////////////////////////////////////////
	--ShouldNotUseSpec.textureusage
	-- report column D: DDS texture name == columnIndex:3
	--////////////////////////////////////////////////////////////////////////////////////////////////////
	fn shouldNotUseSpec =
	(
		shaderIssueDiagnostic "ShouldNotUseSpec" ShouldNotUseSpec_Path columnIndex:3
	),
	
	--////////////////////////////////////////////////////////////////////////////////////////////////////
	-- UnexpectedSpecMask.textureusage
	-- report column D: DDS texture name == columnIndex:3
	--////////////////////////////////////////////////////////////////////////////////////////////////////
	fn unexpectedSpecMask =
	(
		shaderIssueDiagnostic "UnexpectedSpecMask" UnexpectedSpecMask_Path columnIndex:3
	),
	
	--////////////////////////////////////////////////////////////////////////////////////////////////////
	-- UsingRareShader.textureusage
	-- report column D: DDS texture name == columnIndex:3
	--////////////////////////////////////////////////////////////////////////////////////////////////////
	fn usingRareShader =
	(
		shaderIssueDiagnostic "UsingRareShader" UsingRareShader_Path columnIndex:3
	),
	
	--////////////////////////////////////////////////////////////////////////////////////////////////////
	--UsingNonCableTexture.textureusage
	-- report column D: DDS texture name == columnIndex:3
	--////////////////////////////////////////////////////////////////////////////////////////////////////
	fn usingNonCableTexture =
	(
		shaderIssueDiagnostic "UsingNonCableTexture" UsingNonCableTexture_Path columnIndex:3
	),
	
	--//////////////////////////////////////////////////
	--Constructor
	--//////////////////////////////////////////////////
	on create do
	(
		--get scene textures
		sceneTextures = for bm in getClassInstances BitmapTexture collect bm
		/*
		local sceneBitmaps = for bm in getClassInstances BitmapTexture collect bm
		
		--now strip out duplicate textures
		tempDict = #()
		for bm in sceneBitmaps do
		(
			bmFileName = (getfilenamefile bm.filename)
			if findItem tempDict bmFileName == 0 then
			(
				append sceneTextures bm
				append tempDict bmFileName
			)
		)
		*/
		--scenetextures = tempDict.toArray()
/*
		sceneTextures = for i = 1 to AssetManager.GetNumAssets() where (AssetManager.GetAssetByIndex i).getType() == #bitmap \
								collect
								(
									Afile = AssetManager.GetAssetByIndex i
									Afile.getfilename()
								)
*/						
		--shaders
		sceneShaders = for rs in getclassinstances Rage_Shader collect rs
		--refs.dependentNodes
			
		--TXDs
		local TXD_idx = getAttrIndex "Gta Object" "TXD"
		sceneTXD = for obj in $objects where getAttrClass obj  == "Gta Object" collect (DataPair object:obj TXD:(getAttr obj TXD_idx))
		
		--Drawables
		drawables = for obj in $objects where getAttrClass obj  == "Gta Object" collect obj.name
		
	)
)

struct TextureReporterOps
(
	--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--			STRUCT MEMBERS
	--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	regex = dotnetclass "System.Text.RegularExpressions.Regex",
	artRoot = RsConfigGetArtDir(),
	sceneDiagnosis,
	
	--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--			OPERATIONS
	--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	--_____________________________________________________________________________________
	-- Fix unused alpha
	--_____________________________________________________________________________________
	fn fix_UnusedAlpha =
	(
		return false
		
		
	),
	
	--_____________________________________________________________________________________
	-- Update tcs with a  new template
	--_____________________________________________________________________________________
	fn updateTCS tcsFilename newTemplate =
	(
		--prepare new template
		case newTemplate of
		(
			"TintPalette":
			(
				newTemplate = "${RS_ASSETS}/metadata/textures/templates/maps_other/TintPalette"
			)
		
			default:
			(			
				currentTCS = RsExtractTemplateFromFile tcsFilename
				parts = filterString currentTCS "/"
				tcsConstructor = stringStream ""
				for i = 1 to (parts.count - 1) do format "%/" parts[i] to:tcsConstructor
				format "%" newTemplate to:tcsConstructor
				newTemplate = tcsConstructor as string
				--format "tcs: %\n" newTemplate
			)--end default
		)--end case
		
		-- Open up the tcs file to find out what's set currently
		xmlDoc = XMlDocument()
		xmlDoc.init()
		xmlDoc.load ( tcsFilename )
		xmlRoot = xmlDoc.document.DocumentElement

		if xmlRoot != undefined then
		(
			parentnode = RsGetXmlElement xmlRoot "parent"
			
			if ( parentnode != undefined ) then
			(
				-- If not already set to this template then open up the tcs file and edit it, leaving in
				-- default changelist
				if ( parentnode.InnerText != newTemplate ) do
				(
					gRsPerforce.add_or_edit #(tcsFilename) silent:false
					parentnode.InnerText = newTemplate
					xmlDoc.save ( tcsFilename )
				)
			)
		)
	),
	
	--____________________________________________________________________________________________________________________________________________
	-- Diagnose scene
	--
	-- Take assets in the current scene(meshes, textures, shaders) and try to match them up with items in the analysis reports
	--Then return this to the user in the form of a table
	--____________________________________________________________________________________________________________________________________________
	
	--//////////////////////////////////////////////////////////////////////////////////
	-- 
	--//////////////////////////////////////////////////////////////////////////////////
	fn consumeReport &theDict =
	(
		fn buildLookup &theDict index =
		(
			theDict = CSV2Dict.process reportCSVParser.drawablesCSV_Path index
		)
		
		theThread = dotNetobject "CSharpUtilities.SynchronizingBackgroundWorker"
		dotNet.addEventHandler theThread "DoWork" buildLookup
 	
		theThread.RunWorkerAsync()
	),
	
	--//////////////////////////////////////////////////////////////////////////////////
	-- 
	--//////////////////////////////////////////////////////////////////////////////////
	fn advanceProgressBar progressMax &counter =
	(
		progressUpdate ( 100.0 * (counter / progressMax))
		counter += 1
	),
	
	--//////////////////////////////////////////////////////////////////////////////////
	-- 
	--//////////////////////////////////////////////////////////////////////////////////
	fn diagnoseScene =
	(
		local progressMax = 18.0	--number of reports to process
		local counter = 0
		progressStart "Analysising Scene: "
		progressUpdate 1
		--create a new sceneDiagnosis instance
		sceneDiagnosis = SceneDiagnosisStruct()
		
		--reports to check
		
		--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
		--Textures
		--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
		
		--TemplateBadLink.textures
		sceneDiagnosis.templateBadLink()
		advanceProgressBar progressMax &counter 
			
		--TemplateDefault.textures 
		sceneDiagnosis.templateDefault()
		advanceProgressBar progressMax &counter
			
		--TemplateInconsistent.textures
		sceneDiagnosis.templateInconsistent()
		advanceProgressBar progressMax &counter
			
		--MultipleUses.textures
		sceneDiagnosis.multipleUses()
		advanceProgressBar progressMax &counter
			
		--NotUsed.textures
		sceneDiagnosis.notUsed()
		advanceProgressBar progressMax &counter
			
		--CompressedNotProcessed.textures
		sceneDiagnosis.compressedNotProcessed()
		advanceProgressBar progressMax &counter
			
		--Compressed_IncompleteMipChain.textures 
		sceneDiagnosis.compressed_IncompleteMipChain()
		advanceProgressBar progressMax &counter
			
		--UncompressedSkipProcessing.textures
		sceneDiagnosis.uncompressedSkipProcessing()
		advanceProgressBar progressMax &counter
			
		--Ineffective.textures
		sceneDiagnosis.ineffective()
		advanceProgressBar progressMax &counter
			
		--Flat.textures 
		sceneDiagnosis.flat()
		advanceProgressBar progressMax &counter
			
		--HasUnusedAlpha.textures 
		sceneDiagnosis.hasUnusedAlpha()
		advanceProgressBar progressMax &counter
			
		--HDTexAppliedToTerrainShader.textures
		sceneDiagnosis.hdTexAppliedToTerrainShader()
		advanceProgressBar progressMax &counter
			
		--HDTexAppliedToLargeDrawable.textures
		sceneDiagnosis.hdTexAppliedToLargeDrawable()
		advanceProgressBar progressMax &counter
			
		--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
		-- Shaders
		--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
		
		--ShouldNotUseBump.textureusage 
		--This one is a texture usage report but needs to identify the shaders that the texture is used in
		sceneDiagnosis.shouldNotUseBump()
		advanceProgressBar progressMax &counter
		
		--ShouldNotUseSpec.textureusage 
		sceneDiagnosis.shouldNotUseSpec()
		advanceProgressBar progressMax &counter
		
		--UnexpectedSpecMask.textureusage
		sceneDiagnosis.unexpectedSpecMask()
		advanceProgressBar progressMax &counter
		
		--UsingRareShader.textureusage
		sceneDiagnosis.usingRareShader()
		advanceProgressBar progressMax &counter
		
		--UsingNonCableTexture.textureUsage
		sceneDiagnosis.usingNonCableTexture()
		advanceProgressBar progressMax &counter
			
		
		--update progress
		progressEnd()
		--/////////////////////////////////////////////
		-- DEBUG Display the results
		/*
		--temp
		format "--------------TEXTURE ISSUES-----------------\n"
		for te in sceneDiagnosis.textureIssues do format "Texture: % ReportType: % \n" te.asset te.reportType
		
		format "--------------SHADER ISSUES-------------------\n"
		for se in sceneDiagnosis.shaderIssues do format "Shader: % ReportType: % \n" se.asset se.reportType
		*/
	),
	
	--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--			UTILITIES
	--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	--//////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////
	fn getShaderUsage textureDataObj =
	(
		if textureDataObj.shaders == undefined then return false
			
		shaderUsageRE = regex.matches textureDataObj.shaders "\w+"
		shaderUsage = for i = 0 to (shaderUsageRE.count - 1) collect shaderUsageRE.item[i].value
	),
	
	--//////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////
	fn getArtRootArcPath inPath =
	(
		localArcPath = (regex.matches inPath "(?<=\.\.\\\\\.\.\\\\export\\\\levels\\\\gta5\\\\)\w.+").item[0].value
		localArcPath = RsMakeSafeSlashes (pathConfig.removePathLeaf localArcPath)
		artRootArcPath = pathConfig.appendPath (pathConfig.appendPath artRoot "models") localArcPath
	),
	
	--//////////////////////////////////////////////////
	--
	--//////////////////////////////////////////////////
	fn getArchiveType inPath = 
	(
		(regex.matches inPath "((\.itd)|(\.idr))+").item[0].value
	),
	
	--_____________________________________________________________________________________
	-- Display the material using the passed in texture and shader
	--_____________________________________________________________________________________
	fn showNodeShadersUsingTexture theNode theTexture shaderUsage =
	(
		--format "theNode: % theTexture: % shaderUsage: % \n" theNode theTexture shaderUsage
		
		--find the shader with the texture assigned
		theMaterial = #()
		found = #()
		
		--Material Editor
		MatEditor.Open() 
		meditMaterials[1] = theNode.material
		medit.SetActiveMtlSlot 1 false
		
		appliedMats = getAppliedMaterials theNode
		
		if classOf theNode.material != MultiMaterial then
		(
			--format "\tMaterial: % \n" theNode.material.name
			rageShaderName = if classOf theNode.material == Rage_Shader then RstGetShaderName theNode.material
			--format "\t\tRageShaderName: % \n" rageShaderName
			
			if rageShaderName == shaderUsage then --check the texture is assigned to one of the params
			(
				--Material Editor
				meditMaterials[1] = theNode.material
			)
		)
		else --Multimaterial
		(
			for m = 1 to appliedMats.count do
			(
				theNodeMaterial = theNode.material[appliedMats[m]]
				rageShaderName = if classOf theNodeMaterial == Rage_Shader then RstGetShaderName theNodeMaterial
				--print rageShaderName
				if rageShaderName == undefined then continue
				
				for shaderName in shaderUsage do
				(
					if matchPattern rageShaderName pattern:(shaderName + "*.sps") != false then --check the texture is assigned to one of the params
					(
						--format "MATCH! rageShaderName: % usage: % \n" rageShaderName (shaderName + ".sps")
						for i = 1 to (RstGetVariableCount theNodeMaterial) do
						(
							if (RstGetVariableType theNodeMaterial i) == "texmap" then
							(
								shaderTex = toLower (getFilenameFile (RstGetVariable theNodeMaterial i))
								if (regex.matches theTexture shaderTex).count != 0 then
								(
									--format "match var: % pattern: % \n" (RstGetVariable theNodeMaterial i) ("*"+theTexture+"*")
									append found theNodeMaterial
								)
							)
						)
					)
				)
				/*
				params = for i = 1 to (RstGetVariableCount obj.material[m]) collect RstGetVariableName obj.material[m] i
				for p = 1 to params do
				(
					
				)
				*/
			)
			
			--unique materials
			found = makeUniqueArray found
			
			if found.count > 24 then messagebox "More than 24 material match this usage" title:"Warning"
				
			if found.count != 0 then 
			(
				--format "found: % \n" found
				--Material Editor
				MatEditor.Open()
				--flush
				stdMat = StandardMaterial()
				for i = 1 to 24 do setMeditMaterial i stdMat
				meditMaterials[1] = theNode.material
				medit.SetActiveMtlSlot 1 true
				--select correct mutlisub mat
				for f = 1 to found.count do	meditMaterials[f] = found[f]
			)
			else
			(
				messagebox "Couldnt find any matching shaders" title:"Unknown"
			)
		)
	),
	
	--//////////////////////////////////////////////////
	--Constructor
	--//////////////////////////////////////////////////
	on create do
	(
		regex = dotnetclass "System.Text.RegularExpressions.Regex"
	)
)

TROps = TextureReporterOps()