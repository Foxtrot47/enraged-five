
			/*  just overwrite it
			--check if the file has already been extracted
			extractedReportExists = for f in (getFiles (extractPath+"/*.csv") where 
									(
										csvPath = pathConfig.stripPathToLeaf f
										matchPattern csvPath pattern:validReport != false
									) return true
			
			--it has? ok check if the one in the archive is newer
			if extractedReportExists then
			
			--it is, so extract it to the temp dir
			*/
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

global reportCSVParser
struct ReportCSVParserStruct
(
	--sync latest map_objects.csv file
	texture_csv_zip = @"X:\gta5\tools\wildwest\data\analysis.zip",
	extractPath = pathConfig.appendPath (RsConfigGetLocalWildWestAppData()) "Analysis",
	extractAction = (dotNetClass "Ionic.Zip.ExtractExistingFileAction").OverwriteSilently,
	--map_objects = "c:/temp/map_objects_test.csv",
	
	assetTypes = #(),
	reports = #(),
	reportDict = #(),
	reportRoot = "analysis",
	reportClass,
	
	csvReader = undefined,
	--setup
	--containerProps = dotNetObject "RSG.MaxUtils.MaxDictionary",
	reportDataList = #(),
	ddsLookupDrawables = dotNetObject "RSG.MaxUtils.MaxDictionary",
	drawablesCSV_Path = (extractPath + "/" + reportRoot + "/analysis.textureusage.csv"),
	
	columnRemap = #(),
					
	rawColumnIndex = #(),
						
	assetRoot 	= (RsConfigGetAssetsDir() + "export/levels/gta5"),
	--tcsRoot 	= RsConfigGetTCSRoot(),
	tcsRoot 	= (RsConfigGetAssetsDir() + "/metadata/textures"),
	tcpRoot		= RsConfigGetTCPRoot(),
						
	--FUNCTIONS
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	-- 
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn scanZip zip =
	(
		reader = (dotNetClass "Ionic.Zip.ZipFile").Read zip
		try
		(
			readerIt = reader.EntriesSorted.GetEnumerator()
			readerES = reader.EntriesSorted
			newAsset = undefined
			while readerIt.moveNext() != false do
			(
				--get the asset types from directory names
				isDir = readerIt.current.isDirectory
				
				if isDir == true then
				(
					--format "dir: % \n" readerIt.current.filename
					--newAsset = DataPair type:readerIt.current.filename reports:#()
					append assetTypes readerIt.current.filename
				)
				else
				(
					--format "file: % \n" readerIt.current.filename
					append reports readerIt.current.filename
				)
				
				--append assetTypes readerIt.current.filename
				analysisPath = pathConfig.appendPath extractPath readerIt.current.filename
				--print analysisPath
				
				--check archive is newer before extracting over the top
				lastModified = readerIt.current.lastModified
				if doesFileExist analysisPath == true and isDir == false then
				(
					lastModifiedLocalFile = (dotNetClass "System.IO.File").GetCreationTime analysisPath
					relativeTime = lastModified.compareTo lastModifiedLocalFile
					
					if relativeTime > 0 then	--its newer so extract it
					(
						readerIt.current.extract extractPath extractAction
					)
				)
				
				--if it doesnt exist locally then extract it
				if doesFileExist analysisPath != true then
				(
					--print "EXTRACTING"
					try
					(
						readerIt.current.extract extractPath extractAction
					)
					catch (format "Error extracting % \n" readerIt.current.filename)
				)
			)
		)
		catch
		(
			reader.dispose()
		)
		
		reader.dispose()
	),
	
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	-- 
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn formattedReports = 
	(
		for ass in assetTypes do
		(
			--extract asset type
			--format "ass: %\n" ass
			bits = filterString ass "/"
			assetType = bits[2]
			--format "assetType: % \n" assetType
			if assetType != undefined then
			(
				assetReports = for rep in reports where findString rep ass != undefined collect pathConfig.stripPathToLeaf rep
					
				--add to dict
				 append reportDict #(assetType, assetReports)
			)
		)
	),
	
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	-- 
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn extractReport theReport =
	(
		--validate report name
		validReport = (for rep in reports where matchPattern rep pattern:theReport != false collect rep)[1]
		print validReport
		if validReport != undefined then
		(
			print "extracting"
			extractor = (dotNetClass "Ionic.Zip.ZipFile").Read texture_csv_zip
			extractor.ExtractSelectedEntries validReport extractAction
		)
		--return the path to it
	),
	
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	-- -process each line of the csv and create TextureData objects from it
	-- place each one in the reportDataList master list
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn csvRowProcessor row =
	(
		--split by comma
		--bits = filterString row ","
		bits = (dotNetObject "System.String" row).split ","
		reportDataRow = undefined
		
		case reportClass of
		(
			"textures":
			(
				reportDataRow = TextureData()
				reportDataRow.rawString = row
				props = getPropNames reportDataRow
				
				--process the row
				for property in props do
				(
					field = findItem rawColumnIndex property
					if field != 0 then setProperty reportDataRow property bits[field]
				)
				
				--do any formatting for list display here
				for property in props do
				(
					case property of
					(
						#dds_uri:		--correct uri
						(
							original = trimLeft (getProperty reportDataRow #dds_uri)
							--replace unzipped for export
							intersector = findString original "unzipped"
							concise = replace original intersector 8 "export"
							concise = pathConfig.convertPathToRelativeTo concise tcsRoot
							--set the value
							setProperty reportDataRow #dds_uri concise
						)
						
						#tcs:			--remove path root
						(
							original = trimLeft (getProperty reportDataRow #tcs)
							concise = pathConfig.convertPathToRelativeTo original tcsRoot
							--set the value
							setProperty reportDataRow #tcs concise
							setProperty reportDataRow #tcsPath original
						)
						
						#tcp:			--remove path root
						(
							original = trimLeft (getProperty reportDataRow #tcp)
							concise = pathConfig.convertPathToRelativeTo original tcpRoot
							--set the value
							setProperty reportDataRow #tcp concise
							setProperty reportDataRow #tcpPath original
						)
						
						#source_uri:	--remove xml formatting
						(
							--original
						)
						
						default:
						(
							if getProperty reportDataRow property == undefined then setProperty reportDataRow property ""
						)
					)
				)
			)--end texture
			
			"textureusage":
			(
				reportDataRow = DrawableData()
				reportDataRow.rawString = row
				props = getPropNames reportDataRow
				
				--process the row
				for property in props do
				(
					field = findItem rawColumnIndex property
					if field != 0 then setProperty reportDataRow property bits[field]
				)
			)
			
			"shaders":
			(
				columnRemap = textureReporterHeaders.ShaderData_columnRemap
				rawColumnIndex = textureReporterHeaders.ShaderData_rawColumnIndex
				
				reportDataRow = ShaderData()
				reportDataRow.rawString = row
				props = getPropNames reportDataRow
				
				--process the row
				for property in props do
				(
					field = findItem rawColumnIndex property
					if field != 0 then setProperty reportDataRow property bits[field]
				)
			)
			
			"game":
			(
				columnRemap = textureReporterHeaders.GameData_columnRemap
				rawColumnIndex = textureReporterHeaders.GameData_rawColumnIndex
				
				reportDataRow = GameData()
				reportDataRow.rawString = row
				props = getPropNames reportDataRow
				
				--process the row
				for property in props do
				(
					field = findItem rawColumnIndex property
					if field != 0 then setProperty reportDataRow property bits[field]
				)
			)
			
			"scene":
			(
				columnRemap = textureReporterHeaders.SceneData_columnRemap
				rawColumnIndex = textureReporterHeaders.SceneData_rawColumnIndex
				
				reportDataRow = SceneData()
				reportDataRow.rawString = row
				props = getPropNames reportDataRow
				
				--process the row
				
				for property in props do
				(
					field = findItem rawColumnIndex property
					if field != 0 then setProperty reportDataRow property bits[field]
				)
				
			)
			
			"hierarchy":
			(
				columnRemap = textureReporterHeaders.HierarchyData_columnRemap
				rawColumnIndex = textureReporterHeaders.HierarchyData_rawColumnIndex
				
				reportDataRow = HierarchyData()
				reportDataRow.rawString = row
				props = getPropNames reportDataRow
				
				--process the row
				
				for property in props do
				(
					field = findItem rawColumnIndex property
					if field != 0 then setProperty reportDataRow property bits[field]
				)
			)
			
		)--end case
		
		--append to reportDataList
		append reportDataList reportDataRow
	),
	
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	-- 
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn createCSV2Dict = 
	(
		source = "
		using System;
		using System.Collections;
		using System.Collections.Generic;

		public class CSV2Dict
		{
			public Dictionary<string, ArrayList> Process(string theCSVPath, int index)
			{
				if (System.IO.File.Exists(theCSVPath) == false)
				{
					return new Dictionary<string, ArrayList>();
				}
				
				var stream = new System.IO.StreamReader(theCSVPath);
				
				var dict = new Dictionary<string, ArrayList>();
				while (stream.EndOfStream != true)
				{
					string theLine = stream.ReadLine();
					//split it up
					var bits = theLine.Split(new char[] {','}, StringSplitOptions.None);
					//the key is bits[index]
					string key = bits[index].TrimStart(new char[] {' '});

					if (dict.ContainsKey(key) == true)    //replace the value with the orignal array plus the new entry
					{
						var tempArray = dict[key];
						tempArray.Add(bits);

						dict.Remove(key);
						dict.Add(key, tempArray);

					}
					else  //its a new key
					{
						var tempArray = new ArrayList();
						tempArray.Add(bits);
						dict.Add(key, tempArray);
					}
				}
				return dict;
			}
		}
		"
		csharpProvider = dotnetobject "Microsoft.CSharp.CSharpCodeProvider"
		compilerParams = dotnetobject "System.CodeDom.Compiler.CompilerParameters"

		compilerParams.ReferencedAssemblies.AddRange #("System.dll")

		compilerParams.GenerateInMemory = on
		compilerResults = csharpProvider.CompileAssemblyFromSource compilerParams #(source)
		
		assembly = compilerResults.CompiledAssembly
		CSV2Dict = assembly.CreateInstance "CSV2Dict"
		
	),
	
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	-- 
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	fn drawablesProcessor =
	(
		global CSV2Dict = createCSV2Dict()
		
		if doesFileExist drawablesCSV_Path != true then
		(
			messageBox "Cant find drawables analysis" title:"Error"
			return false
		)
		
		fn buildLookup =
		(
			reportCSVParser.ddsLookupDrawables = CSV2Dict.process reportCSVParser.drawablesCSV_Path 2
		)
		
		theThread = dotNetobject "CSharpUtilities.SynchronizingBackgroundWorker"
		dotNet.addEventHandler theThread "DoWork" buildLookup
 	
		theThread.RunWorkerAsync()
		--buildLookup()
		--ddsLookupDrawables = CSV2Dict.process drawablesCSV_Path
		print "DONE"
		
	),
	
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	-- 
	--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	on create do
	(
		gRsPerforce.connect()
		--check if we have head rev before syncing
		fstat = (gRsPerforce.getFileStats texture_csv_zip)[1]
		if fstat.item "haveRev" != fstat.item "headRev" then gRsPerforce.p4.run "sync" #(texture_csv_zip)
		
		scanZip texture_csv_zip
		formattedReports()
	)
	
	
)

--create texture parser
reportCSVParser = ReportCSVParserStruct()