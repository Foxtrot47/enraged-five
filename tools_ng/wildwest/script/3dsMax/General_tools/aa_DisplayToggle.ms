-- Display Toggle
-- Rockstar Leeds
-- 06/05/2011
-- by Abhishek Agrawal

-- Toggles Display types between vert color, alpha and various map channels. 

/* --------------------------------------------------------------------------------------------------
	ROLLOUT DEFINITIONS		
*/ --------------------------------------------------------------------------------------------------
global dispFloat

rollout displayToggle_rollout "Display Toggle"
(
	-- dropdownlist ddl_someList items:#("Object Color", "Material Color", "Vertex Color","Vertex Alpha","Map Channel 9", "Map Channel 10") --this is the dropdownlist
	button btn_select0 "  Object Color  " 
	button btn_select1 " Material Color " 
	button btn_select2 "  Vertex Color  " --these buttons will set the list 
	button btn_select3 "  Vertex Alpha  " --by calling the custom function that
	button btn_select4 "Map Channel  9 " --replaces the on selected() handler
	button btn_select5 "Map Channel 10" 

	groupBox gbPmap "" pos:[5,156] width:92 height:7
	checkbutton chk_ShadeSelected "Shade Selected" tooltip:"Display only Selected as Shaded"

	--this is the custom function that performs the operations normally done
	--inside the on selected() event handler:
	fn ddl_someList_Selected itm = --we pass the argument passed to the handler
	(
		displayColor.shaded = #object 

		Case itm of
		(
			0: (
					-- Switch back to Object Shaded View
					displayColor.shaded = #object
					for i in selection do 
						i.showVertexColors = off
				)
			1: (
					-- Switch back to Material Shaded View
					displayColor.shaded = #material
					for i in selection do 
						i.showVertexColors = off
				)
			2: (
					-- Vertex Channel Display for Vertex Color (Lighting)
					for i in selection do 
					(
						i.showVertexColors = on
						i.vertexColorType = 0
					)
				)
			3: (
					-- Vertex Channel Display for Vertex Alpha (Decals)
					for i in selection do 
					(
						i.showVertexColors = on
						i.vertexColorType = 2
					)
				)
			4: (
					-- Vertex Channel Display for Map Channel Color = "9" (Terrain Paint)
					for i in selection do 
					(
						i.showVertexColors = on
						i.vertexColorType = 5
						i.vertexColorMapChannel = 9
					)
				)
			5: (
					-- Vertex Channel Display for Map Channel Color = "10" (Collision)
					for i in selection do 
					(
						i.showVertexColors = on
						i.vertexColorType = 5
						i.vertexColorMapChannel = 10
					)
				)
		)
	)

		/*
		viewport.getType()
		viewport.isWire()
		
		viewport.GetRenderLevel()
			Returns the viewport render level. 
			Can be one of the following: #smoothhighlights, #wireFrame, #smooth, #facethighlights, #facet, #flat, #litwireframe, #Box

		viewport.SetRenderLevel <render_level>
			Sets the current viewport's render level to the specified mode. 
			Valid values are #smoothhighlights, #wireFrame, #smooth, #facethighlights, #facet, #flat, #litwireframe, #Box

	--now we can call this custom function from the event hander itself
	--on ddl_someList selected itm do ddl_someList_Selected itm
	*/
	
	--Toggle Max command to shade selected objects only. 
	on chk_ShadeSelected changed state do 
		max shade selected

	--and from any other event handlers that want to affect the dropdownlist
	on btn_select0 pressed do ddl_someList_Selected 0
	on btn_select1 pressed do ddl_someList_Selected 1
	on btn_select2 pressed do ddl_someList_Selected 2
	on btn_select3 pressed do ddl_someList_Selected 3
	on btn_select4 pressed do ddl_someList_Selected 4
	on btn_select5 pressed do ddl_someList_Selected 5
)

if dispFloat != undefined then closerolloutfloater dispFloat
dispFloat = newrolloutfloater "Display Toggle" 115 225 100 100
addrollout displayToggle_rollout dispFloat 

	
