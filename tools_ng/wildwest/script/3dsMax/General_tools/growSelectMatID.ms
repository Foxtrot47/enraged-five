--
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())
	
escapeEnable = true

struct GrowMatID
(
	aSel = #(),
	aBorder = #(),
	aChecked = #(),
	
	obj = getCurrentSelection(),
	
	fn growSelection = 
	(
		--get face selection
		obj = obj[1]
		
		--check selection
		--if classOf obj != Editable_Poly then return
		
		--get face seelction
		sel = polyop.getFaceSelection obj
		
		sel = sel as array
		append aChecked sel[1]
		
		--??what if you have faces with various matID selected at first??
		--first add this selection to the final aSel array and get the matID for testing with
		selMatID = polyop.getFaceMatID obj sel[1]
		aSel = for item in sel where polyop.getFaceMatID obj item == selMatID collect item
		--append aSel sel[1]
		
		--Find the faces connected to this face
		faceEdges = #()
		for item in aSel do
		(
			edges = polyop.getFaceEdges obj item
			join faceEdges edges
		)
		
		--faces from those faceEdges
		connFaces = polyop.getFacesUsingEdge obj faceEdges
		--set the first border faces
		aBorder = connFaces as array
		
		aMatch = #()
		do
		(
			--fvFaces = #()
			aNewBorder = #()
			for f in aBorder do
			(
				--test this face for a match
				if polyop.getFaceMatID obj f == selMatID then
				(
					--this face is the right matID so add it to the match array
					append aMatch f
				
					--Find the faces connected to this face
					borderFaceEdges = polyop.getFaceEdges obj f
					edgeFaces = polyop.getFacesUsingEdge obj borderFaceEdges
					
					appendIfUnique aChecked f
					--test if they've already been checked - or remove the already aSel values from this new array
					aTesting = (edgeFaces - aChecked as BitArray ) as array
					--the tested ones to checked 
					join aChecked aTesting
					
					--check which if any are the same matID
					aBorderMatch = for item in aTesting where polyop.getFaceMatID obj item == selMatID collect item
					
					--Add the new mtahcing matID faces to the match array
					join aMatch aBorderMatch
					
					--add matching matID faces to new border
					join aNewBorder aBorderMatch
				)
			)
			--strip out duplicate array entries
			aNewBorder = makeUniqueArray aNewBorder
			--aMatch = makeUniqueArray aMatch
			--ones that are the same add to aSel
			join aSel aMatch
			
			--the new border to check is the one we found
			aBorder = aNewBorder
		)
		while aNewBorder.count > 0
		
		--selectFaces on the model
		obj.selectedFaces = aSel
		
	)
	
)

--instance GrowMatID
growByMatID = GrowMatID()
--do your duty
growByMatID.growSelection()
