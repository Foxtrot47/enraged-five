
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/General_tools/sdHardSoftEdges.ms")

RsCollectToolUsageData (getThisScriptFilename())

fn edgeToolPanelRolloutUI=
(
	rollout edgeToolPanelRollout "Edge Tools"
	(
		
		dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:252	-- .NET FORM TO HOLD THE IMAGES
		local banner = makeRsBanner dn_Panel:rsBannerPanel width:290 studio:"sandiego" mail:"kyle.hansen@rockstarsandiego.com" wiki:"Edge Tool Panel" filename:(getThisScriptFilename())-- INSTANCE THE STRUCT
				
		button btnHardenEdge "Harden Edge" width:225 tooltip:"Sets the edge normals to be hard. Does not work on Editable_Meshes."
		button btnSoftenEdge "Soften Edge" width:225 tooltip:"Matches smoothing groups between faces to soften edges.  Does not work on Editable_Meshes."
		button btnTriangulate "Triangulate Object" width:225 tooltip:"Triangulates Poly/Mesh objects, on mesh objects collapses stack!"
		
		on edgeToolPanelRollout open do
		(
			banner.setup() -- add the banner
		)		
			
		on btnHardenEdge pressed do
		(
			
			hardSoftEdge.setEdge "hard";
		)
		
		on btnSoftenEdge pressed do
		(
			hardSoftEdge.setEdge "soft";
		)
		
		on btnTriangulate pressed do
		(
			local sel = (selection as array)
			if sel.count > 0 then
			(
				for objectIndex=1 to sel.count do
				(
					triObject = sel[objectIndex]
					if classof triObject == editable_poly then
					(
						subobjectLevel = 1
						meshVerts = polyop.getnumverts triObject
						polyop.setVertSelection triObject #{1..meshVerts}
						triObject.EditablePoly.ConnectVertices ()
						subobjectLevel = 0
					)
					else if classof triObject == editable_mesh then
					(
						
						modPanel.addModToSelection (Turn_to_Mesh ()) ui:on
						triObject.modifiers[#Turn_to_Mesh].useInvisibleEdges = off
						maxOps.CollapseNode triObject off
					)
				)
			)
		)
		
	)

	createDialog  edgeToolPanelRollout 250 120
	edgeToolPanelRollout
)

if newEdgeUI != undefined then 
(
	DestroyDialog newEdgeUI
)

newEdgeUI = edgeToolPanelRolloutUI()