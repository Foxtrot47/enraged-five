filein  (RsConfigGetWildWestDir()+"/script/3dsMax/_common_functions/FN_common.ms" )

fn RSN_resetTransforms = 
(
	obj = $selection as array
	if obj.count == 0 then
	(
		messagebox "Nothing selected" title:"Error"
		return false
	)
	obj = obj[1]
	
	--create a stack from the heirarchy and then process it
	hStack = heirarchyWalk obj
	
	--Now we have a stack pop them off reseting as we go
	while hStack.count != 0 do
	(
		obj = hStack[hStack.count]
		oClass = classOf(obj)
		
		if oClass == Editable_mesh or oClass == Editable_Poly or oClass == Col_Mesh then
		(
			--note heirarchy
			parent = obj.parent
			children = (readvalue (("$"+obj.name+"/*") as stringstream)) as Array
			
			--detach
			obj.parent = undefined
			for child in children do	child.parent = undefined
						
			--reset the xform
			resetXform obj
			collapseStack obj
			
			--restore heirarchy
			obj.parent = parent
			for child in children do child.parent = obj
				
		)
		
		--pop
		deleteItem hStack hStack.count

	)
	
	messagebox "Complete" title:"Reset Transforms"
	
)

RSN_resetTransforms()