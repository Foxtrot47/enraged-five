--=============================================================--
--  AUTH: Allan Hayburn  - Rockstar San Diego:  Allan.Hayburn@rockstarsandiego.com
--  1.0 - 08-10-2012
--  Sets object shadow attributes
--=============================================================--

filein (RsConfigGetWildWestDir() + "script/3dsmax/_config_files/wildwest_header.ms")

--RsCollectToolUsageData (getThisScriptFilename())

--globals
global grsPerforce = rsPerforce()	
global rsPerforceHistoryTool = undefined

try ( destroyDialog rsPerforceHistoryToolRollout ) catch ()

struct rsPerforceHistoryToolStruct 
(
	
	theObjectName = undefined,
	theObjectFileName = undefined,
	theObjectFileRefdName = undefined,
	theFileHeadRev = undefined,
	theFileHaveRev = undefined,
	theFileClient = undefined,
	theFileDepot = undefined,
	theRevFiles = undefined,
	theTempFile = (pathConfig.GetDir #temp + "\\tempfile.max"),
	
	fn populateData theObject = 
	(
		HiddenDOSCommand "del tempfile.max /f" startpath:(pathConfig.GetDir #temp)
		local theObjP4Struct = grsPerforce.record2Struct (grsPerforce.getfilestats theObject.filename)[1]
		theObjectName = theObject.name
		theObjectFileName = (getfilenamefile theObject.filename)
		theObjectFileRefdName = rsPerforceHistoryToolStruct.filterRefNodeName theObject
		theFileHeadRev = theObjP4Struct.headrev
		theFileHaveRev = theObjP4Struct.haverev
		theFileClient = theObjP4Struct.clientFile
		theFileDepot = theObjP4Struct.depotFile
		local theRevisionNums = (theFileHaveRev as integer)
		theRevFiles = #()
		for i in theRevisionNums to 1 by -1 do -- add 0's to headrev number to make ui cleaner
		(
			local revNum = ""
			if (i as string).count < 2 then revNum = ("0" +(i as string)) else revNum = (i as string)
			if revNum.count < 3 then revNum = ("0" +revNum)
			append theRevFiles revNum
		)
	),
	----------------------------------------------------------------------------------------------
	-- fn to filter directory string for length and tab to next line if string is too long
	----------------------------------------------------------------------------------------------
	fn filterDirectoryStructure theDir = 
	(
		local theDirArray = filterstring theDir "/\\"
		local theDirString = stringstream ""
		local theCount = 0
		if theDirArray[1] == "depot" then theDirArray[1] = "//depot"
		for i in 1 to theDirArray.count do
		(
			theCount = theCount + theDirArray[i].count
			if theCount > 20 then  -- if string is longer than 20 then add a new line
			(
					format "\n" to:theDirString
					theCount  = 0
			)
			format (theDirArray[i]+"/") to:theDirString

		)
		theDirString as string
	),
	----------------------------------------------------------------------------------------------
	-- fn to sync an objects file to a temp file
	----------------------------------------------------------------------------------------------
	fn syncData theRevision theDepotFile = 
	(
			theTemporaryFile = (pathConfig.GetDir #temp + "\\tempfile.max")
			HiddenDOSCommand "del tempfile.max /f" startpath:(pathConfig.GetDir #temp)			
			grsPerforce.p4.run "print" #("-o",  theTemporaryFile, (theDepotFile+"#"+theRevision))
	),	
	----------------------------------------------------------------------------------------------
	-- fn to remove the rsRefobject suffix from a rsrefobject's name
	----------------------------------------------------------------------------------------------
	fn filterRefNodeName thenode =
	(
		local filteredString = ""
		local filteredNames = filterstring theNode.name "_"
		for i in 1 to filteredNames.count - 1 do
		(
			append filteredString filteredNames[i]
			if i != (filteredNames.count - 1) then append filteredString "_"
		)
		filteredString
	)
)

rsPerforceHistoryTool = rsPerforceHistoryToolStruct()

rollout rsPerforceHistoryToolRollout "Perforce History Tool"
(	
	-- 	local defines
	local theSyncRev = undefined

	
	-- used for laying out the ui
	local RlltStrtX = 20
	local RlltStrtY = 70
	
	--functions	
	fn RSrefObj_filt o = (classof o == RSrefObject)
	fn populateUI = 
	(
		rsPerforceHistoryToolRollout.edtObjName.text = rsPerforceHistoryTool.theobjectname
		rsPerforceHistoryToolRollout.edtRefObjName.text = rsPerforceHistoryTool.theObjectFileRefdName
		rsPerforceHistoryToolRollout.edtFileName.text = rsPerforceHistoryTool.theObjectFileName
		rsPerforceHistoryToolRollout.edtHeadRevNum.text = rsPerforceHistoryTool.theFileHeadRev
		rsPerforceHistoryToolRollout.edtClientFile.text = rsPerforceHistoryTool.filterDirectoryStructure rsPerforceHistoryTool.theFileClient
		rsPerforceHistoryToolRollout.edtDepotFile.text = rsPerforceHistoryTool.filterDirectoryStructure rsPerforceHistoryTool.theFiledepot
		rsPerforceHistoryToolRollout.lstRevisionNum.items = rsPerforceHistoryTool.theRevFiles
	)
	fn errorUI = 
	(
		rsPerforceHistoryToolRollout.edtObjName.text = "Error: No Valid Selection"
		rsPerforceHistoryToolRollout.edtRefObjName.text =  "Error: No Valid Selection"
		rsPerforceHistoryToolRollout.edtFileName.text =  "Error: No Valid Selection"
		rsPerforceHistoryToolRollout.edtHeadRevNum.text =  "Error"
		rsPerforceHistoryToolRollout.edtClientFile.text =  "Error: No Valid Selection"
		rsPerforceHistoryToolRollout.edtDepotFile.text =  "Error: No Valid Selection"
	)
	--Controls		
	
	-- Rockstar Banner
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:rsPerforceHistoryToolRollout.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Perforce_History_Tools" filename:(getThisScriptFilename())
		
	--groupbox grpHeader width:200 height:33 pos:[0, 0]
	
	label lblObjName "Object Name:" pos:[RlltStrtX,RlltStrtY]
	edittext edtObjName "" pos:[RlltStrtX-3,lblObjName.pos.y+20] width:164 readOnly:true
	
	label lblRefObjName "Referenced Object Name:" pos:[RlltStrtX,edtObjName.pos.y+25]
	edittext edtRefObjName "" pos:[RlltStrtX-3,lblRefObjName.pos.y+20] width:164 readOnly:true
	
	button btnSelectObj  "Select Object" pos:[RlltStrtX,edtRefObjName.pos.y+25] width:161 
	
	label lblFileName "File Name:" pos:[RlltStrtX,btnSelectObj.pos.y+27]
	edittext edtFileName "" pos:[RlltStrtX-3,lblFileName.pos.y+20] width:164 readOnly:true
	
 	label lblHeadRevNum "Head Revision #:" pos:[RlltStrtX,edtFileName.pos.y+30]
	edittext edtHeadRevNum "" pos:[lblHeadRevNum.pos.x+90,lblHeadRevNum.pos.y] width:71 readOnly:true

	label lblClientFile "Client File:" pos:[RlltStrtX,edtHeadRevNum.pos.y+20]
	edittext edtClientFile "" pos:[RlltStrtX-3,lblClientFile.pos.y+20] width:164 readOnly:true height:80

	label lblDepotFile "Depot File:" pos:[RlltStrtX,edtClientFile.pos.y+edtClientFile.height+10]
	edittext edtDepotFile "" pos:[RlltStrtX-3,lblDepotFile.pos.y+20] width:164 readOnly:true height:80
	
	groupbox grpp1 "Perforce Data" pos:[RlltStrtX-10,RlltStrtY-25] height:435 width:180
	
	label lblRevNum "Temp file using Revision:" pos:[RlltStrtX,edtDepotFile.pos.y+edtDepotFile.height+50]
	edittext edtRevisionNum "" pos:[lblRevNum.pos.x+120,lblRevNum.pos.y] width:35 readOnly:true text:"N/A"
	button btnSync "Sync Revision" pos:[RlltStrtX,edtRevisionNum.pos.y+25] width:105
	dropdownlist lstRevisionNum "" items:#() height:30 width:45 pos:[btnSync.pos.x+111,btnSync.pos.y]

	button btnMergeObjSel "Merge Selected"  pos:[RlltStrtX,lstRevisionNum.pos.y+30] width:161
	button btnMergeObjPick "Pick Merge Objects"  pos:[RlltStrtX,btnMergeObjSel.pos.y+30] width:161
	
	groupbox grpp2 "Sync and Merge" pos:[RlltStrtX-10,edtDepotFile.pos.y+edtDepotFile.height+30] height:140 width:180
	
	on rsPerforceHistoryToolRollout open do
	(		
		banner.setup() -- add the banner
		if $selection.count == 1 then 
		(
			local theSelectedObj = $selection[1]
			if classof theSelectedObj == rsRefObject then
			(
				if theSelectedObj.filename != "" then 
				(
					rsPerforceHistoryTool.populateData theSelectedObj
					populateUI()
				) else (
					ErrorUI()
				)
				
			) else (
				ErrorUI()
			)
		)
		
	)
	
	on btnSelectObj pressed do -- if an object is selected it will use that object otherwise it will create a pickobject
	(
		local thePickedObject = pickObject rubberband:[0,0,0] filter:RSrefObj_filt
		if thePickedObject != undefined and thePickedObject !=  #escape and (classof thePickedObject) ==  RSrefObject then
		(
			select thePickedObject
			if thePickedObject.filename != "" then 
			(
				rsPerforceHistoryTool.populateData thePickedObject
				populateUI()
			) else (
				 ErrorUI()
			)
		)
	)

	on btnSync pressed do
	(		
		if rsPerforceHistoryToolRollout.lstRevisionNum.selected != undefined then
		(
			theSyncRev =  rsPerforceHistoryToolRollout.lstRevisionNum.selected 
			rsPerforceHistoryTool.syncData theSyncRev rsPerforceHistoryTool.theFileDepot
			rsPerforceHistoryToolRollout.edtRevisionNum.text = theSyncRev
			
		) else (
			messagebox "Error Syncing check object" title:"Error"
		)
	)
	
	on btnMergeObjPick pressed do -- brings up ui to choose the objects to merge in
	(
		if doesfileexist rsPerforceHistoryTool.theTempFile == true  then
		(
			local selectedObj =  $selection[1]
			clearSelection() 
			mergeMAXFile rsPerforceHistoryTool.theTempFile  #prompt #select 
			if $selection.count != 0 then (
				max tool zoomextents	
					
			) else if selectedObj != undefined then (
				select selectedObj
			)
			
		) else (
			messagebox "No merge file defined. Sync to a valid revision.\nIf the revision no longer exists in perforce, you should contact your IT Dept." title:"Error"
		)
	)
		
	on btnMergeObjSel pressed do -- brings in the selected object ref
	(
		if doesfileexist rsPerforceHistoryTool.theTempFile == true  then
		(
			if $selection[1] != undefined  and $selection[1].name == rsPerforceHistoryToolRollout.edtObjName.text then
				(
				--	try
					(
						select (getnodebyname rsPerforceHistoryToolRollout.edtObjName.text)
						local selectedObj =  $selection[1]
						local theMergeNode = rsPerforceHistoryTool.filterRefNodeName selectedObj
						mergeMAXFile rsPerforceHistoryTool.theTempFile #(theMergeNode)  #select 
						if $selection.count != 0 then
						(
							local mergedObj = $selection[1]
							mergedObj.pos = selectedObj.max + [1,1,1]
							selectMore selectedObj
							macros.run "Tools" "Isolate_Selection"
							clearselection()
						)else messagebox "Object not found in revision" title:"ERROR"
						)--catch(print "Merge Error")
					)
				
		) else (
			messagebox "No merge file defined. Sync to a valid revision.\nIf the revision no longer exists in perforce, you should contact your IT Dept." title:"Error"
		)
	)
	
)

createDialog rsPerforceHistoryToolRollout width:200 height:650 style:#( #style_titlebar, #style_toolwindow, #style_sysmenu )
