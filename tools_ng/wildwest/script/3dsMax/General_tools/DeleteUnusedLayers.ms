--Delete unused layers

fn deleteUnusedLayers =
(
   DeletedLayerCount = 0
   local deflayer = layermanager.getlayer 0
   deflayer.current = true
   for i = Layermanager.count-1 to 1 by-1 do
   (
		layer = layermanager.getLayer i
		local thislayername = layer.name
		layer.nodes &theNodes
		deleted = LayerManager.deleteLayerbyname thislayername
		if deleted then DeletedLayerCount += 1
   )

   if not DeletedLayerCount == 0 then Messagebox ("Number of layers removed - " + DeletedLayerCount as string) title:"Layer Manager"
)

deleteUnusedLayers()
