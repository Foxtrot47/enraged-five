filein "pipeline/helpers/models/BoundingBox.ms"
filein "pipeline/util/LodViewManipTool.ms"


struct DNStyles
(
	textClr = (colorMan.getColor #windowText) * 255,
	windowClr = (colorMan.getColor #window) * 255,
	backgroundClr = (colorMan.getColor #background) * 255,
	dnColour = dotNetClass "System.Drawing.Color",
	backColor = (dotNetClass "System.Drawing.Color").FromArgb (backgroundClr[1] + 10) (backgroundClr[2] + 10) (backgroundClr[3] + 10),
	foreColor = (dotNetClass "System.Drawing.Color").FromArgb textClr[1] textClr[2] textClr[3],
	dnColourExecute = dnColour.FromArgb 51 181 229,
	dnColourSuccess = dnColour.FromArgb 153 204 0,
	dnColourError = dnColour.FromArgb 255 68 68,
	dnColourDormant = dnColour.FromArgb (backgroundClr[1] - 10) (backgroundClr[2] - 10) (backgroundClr[3] - 10),
	
	stdButtonHeight = 24,
	
	fn setExecuteStyle ctrl =
	(
		ctrl.FlatAppearance.BorderColor = dnColourExecute
		windows.processPostedMessages()
	),
	
	fn setSuccessStyle ctrl =
	(
		ctrl.FlatAppearance.BorderColor = dnColourSuccess
		windows.processPostedMessages()
	),
	
	fn setErrorStyle ctrl =
	(
		ctrl.FlatAppearance.BorderColor = dnColourError
		windows.processPostedMessages()
	),
	
	fn setDormantStyle ctrl =
	(
		ctrl.FlatAppearance.BorderColor = dnColourDormant
		windows.processPostedMessages()
	),
	
	fn initButtonStyle ctrl =
	(
		ctrl.FlatStyle = (dotNetObject "Button").FlatStyle.Flat
		ctrl.FlatAppearance.BorderSize = 2
		ctrl.FlatAppearance.BorderColor = dnColourDormant
		ctrl.backColor = backColor
		ctrl.foreColor = foreColor
		windows.processPostedMessages()
	)
	
)

dnStyle = DNStyles()


--/////////////////////////////////////////
--	UI
--/////////////////////////////////////////
rollout ExternalPivotItemsUI "External Pivot items"
(
	label lblNames "External Pivot Objects"
	listbox lstObjects height:20
	
	fn selectObject arg =
	(
		local theObj = getNodeByName lstObjects.items[arg]
		select theObj
		max zoomext sel all
	)
	
	on lstObjects selected arg do
	(
		selectObject arg
	)
)

try(DestroyDialog HDTextureDistanceUI)catch()
rollout HDTextureDistanceUI "HD Texture Distance" width:285 height:310
(
	--/////////////////////////////////////////
	--	VARIABLES
	--/////////////////////////////////////////
	local HDTexturesDistanceAttr = GetAttrIndex "Gta Object" "HD Textures Distance"
	local toolRunning = false
	
	-- Used to define the render bits:
	struct renderItem (clr, class = #line, points = #(), extra)
	local renderList = #()
	
	--/////////////////////////////////////////
	--	CONTROLS
	--/////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:(HDTextureDistanceUI.width)
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"HD Texture Distance Tool" versionName:"Gong Farmer" versionNum:1.05 filename:(getThisScriptFilename())
	
	group "Distance Settings"
	(
		checkbox chkAutoHDDistance "Proportional" across:2 checked:true
		spinner spnPercentLODDist "LOD Distance % : " range:[0, 100, 50] type:#Integer
		spinner spnHDDistance "Explicit Distance :" width:110 align:#right range:[1, 3000, 200] type:#Integer enabled:(not chkAutoHDDistance.state)
	)
	
	dotnetcontrol btnSet "Button" text:"Set Selection" width:(HDTextureDistanceUI.width - 20) height:24
	
	group "Visual Helper"
	(
		checkButton btnSphereVis "Show HD Distance" width:(HDTextureDistanceUI.width - 20)
		--button btnShowLODDist "Show LOD Toolkit"
	)
	
	subrollout subLODVis "LOD Visualisation" width:(HDTextureDistanceUI.width - 10)  offset:[-5, 0]
	
	timer ctrlState interval:5000 active:false
	
	
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////
	
	
	--/////////////////////////////////////////
	--	EVENTS
	--/////////////////////////////////////////
		
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on chkAutoHDDistance changed state do
	(
		if (state == true) then
		(
			spnHDDistance.enabled = false
			spnPercentLODDist.enabled = true
		)
		else
		(
			spnHDDistance.enabled = true
			spnPercentLODDist.enabled = false
		)
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btnSet click do
	(
		dnStyle.setExecuteStyle btnSet
		
		local externalPivotObjects = #()	-- a list of objects with pivots outside the bounding box of the object
		
		for obj in selection where (GetAttrClass obj == "Gta Object") do
		(
			local bbox = nodeGetBoundingBox obj (matrix3 1)
			in coordsys #world local objPivot = obj.pivot
			
			--check if the object pivot is outside the bounding volume
			local exludeObject = false
			if (RsBBoxContains bbox objPivot) == false then
			(
				append externalPivotObjects obj
				exludeObject = true
			)
			
			if exludeObject == false then
			(
				if (chkAutoHDDistance.checked == true) then
				(
					--get object bounds and lod distance work out hd dist from that
					local LODDistance = GetAttr obj (GetAttrIndex "Gta Object" "LOD distance")
					
					local bboxDiff = bbox[2] - bbox[1]
					--local maxDim = amax #((abs bboxDiff.x), (abs bboxDiff.y), (abs bboxDiff.z))
					local bbAxes = sort #((abs bboxDiff.x), (abs bboxDiff.y), (abs bboxDiff.z))
					local maxDim = sqrt((bbAxes[3]*bbAxes[3]) + (bbAxes[2]*bbAxes[2]))
					
					--find the pivot offset from the bounds centre
					local boundsMidPt = bbox[1] + (bboxDiff / 2.0)
					local pivToBBDist = distance objPivot boundsMidPt
					
					local bbRadius = (maxDim / 2.0) + pivToBBDist
					local newDistance = LODDistance * (spnPercentLODDist.value / 100.0)
					--format "bbRadius: % \n" bbRadius
					
					--if the new distance is inside the boudning sphere then set the new distacent o the boundingsphere + 10
					if newDistance < bbRadius then newDistance = bbRadius + 10
					
										
					SetAttr obj HDTexturesDistanceAttr newDistance
				)
				else
				(
					SetAttr obj HDTexturesDistanceAttr spnHDDistance.value
				)
			)
			
			exludeObject = false
		)
		
		--got any external pivot objects? then show em in a list so they can have their distances set by hand
		if externalPivotObjects.count != 0 then
		(
			CreateDialog ExternalPivotItemsUI width:200 height:300
			ExternalPivotItemsUI.lstObjects.items = (for i in externalPivotObjects collect i.name)
		)
		
		HDTextureDistanceUI.updateHDDistVis()
		
		ctrlState.active = true
		dnStyle.setSuccessStyle btnSet
	)
	
	
	--/////////////////////////////////////////
	--	Switch control state on timer trigger
	--/////////////////////////////////////////
	on ctrlState tick do
	(
		dnStyle.setDormantStyle btnSet
		ctrlState.active = false
	)
	
	--/////////////////////////////////////////////////////////////////////////////////////
	fn getCirclePoints pos radius segs = 
	(
		local slice = 360.0 / segs
		
		for n = 0 to (segs - 1) collect 
		(
			local ang = slice * n
			[pos.x + (radius * (cos ang)), pos.y + (radius * (sin ang)), pos.z]
		)
	)
	
	--/////////////////////////////////////////////////////////////////////////////////////
	fn renderHDDistVis =
	(
		-- Only continue drawing if editor-tool is still open:
		if not toolRunning do 
		(
			unregisterRedrawViewsCallback renderHDDistVis
			return OK
		)
		
		gw.setTransform (Matrix3 1)
		
		local lineClr, textClr
		for item in renderList do 
		(
			local points = for pos in item.points collect (gw.wtransPoint pos)
			
			case item.class of 
			(
				#line:
				(
					gw.setColor #line item.clr
					gw.wPolyline points item.extra
				)
				#marker:
				(
					gw.Marker points[1] item.extra color:item.clr
				)
				#text:
				(
					gw.setColor #text item.clr
					gw.text points[1] item.extra
				)
			)
		)
	)
	
	--/////////////////////////////////////////////////////////////////////////////////////
	fn updateHDDistVis updateObjList:true doRedraw:true = 
	(
		-- Clear the current render-list:
        renderList.count = 0
		
		if not btnSphereVis.checked do 
		(
			deleteAllChangeHandlers id:#HDDistTool
			return OK
		)
			
		unregisterRedrawViewsCallback renderHDDistVis
		
		if updateObjList then
		(
			showList = #()
			
			join showList (for obj in selection where (getAttrClass obj == "Gta Object") and (not obj.isHidden) collect obj)
			
			
			
			deleteAllChangeHandlers id:#HDDistTool
			when showList deleted id:#HDDistTool do (updateHDDistVis updateObjList:false doRedraw:false)
			when transform showList changes id:#HDDistTool do (updateHDDistVis updateObjList:false doRedraw:false)
			when parameters showList changes id:#HDDistTool do (updateHDDistVis updateObjList:false doRedraw:false)
			
		)
		
		for item in showList do
		(
			local thisHDDist = getAttr item HDTexturesDistanceAttr
			
			local backXform, rotXformA, rotXformB
			
			backXform = transMatrix -item.pivot
			
			rotXformA = rotateXMatrix 90
			rotXformA[4] = item.pivot
			
			rotXformB = rotateZMatrix 90
			rotXformB[4] = item.pivot
			
			local thisClr = red
			local circlePoints = getCirclePoints [0,0,0] thisHDDist 28
			append renderList (renderItem clr:thisClr class:#line points:(for pos in circlePoints collect (pos * rotXformA)) extra:true)
			append renderList (renderItem clr:thisClr class:#line points:(for pos in circlePoints collect (pos * rotXformB)) extra:true)
					
		)
		
		registerRedrawViewsCallback renderHDDistVis
		
		if doRedraw do 
		(
			completeRedraw()
		)
		
		return OK
			
	)
	
	
	--/////////////////////////////////////////////////////////////////////////////////////
	fn removeCallbacks = 
	(
		renderList.count = 0
		deleteAllChangeHandlers id:#HDDistTool
		callbacks.removeScripts id:#HDDistTool
		unregisterRedrawViewsCallback renderHDDistVis
	)
	
	--/////////////////////////////////////////////////////////////////////////////////////
	fn setupCallbacks = 
	(
		callbacks.addScript #selectionSetChanged	"HDTextureDistanceUI.updateHDDistVis()"	id:#HDDistTool
		callbacks.addScript #filePostOpenProcess		"HDTextureDistanceUI.updateHDDistVis()"	id:#HDDistTool
		callbacks.addScript #filePostSaveProcess		"HDTextureDistanceUI.updateHDDistVis()"	id:#HDDistTool
		callbacks.addScript #systemPostReset			"HDTextureDistanceUI.updateHDDistVis()"	id:#HDDistTool
		callbacks.addScript #systemPostNew			"HDTextureDistanceUI.updateHDDistVis()"	id:#HDDistTool
		registerRedrawViewsCallback renderHDDistVis
	)
	
	
	on btnSphereVis changed state do 
	(
		updateHDDistVis() 
		if not state do (completeRedraw())
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on HDTextureDistanceUI open do
	(
		banner.setup()
		dnStyle.initButtonStyle btnSet
		AddSubRollout subLODVis RsLodViewToolRoll
		subLODVis.height = (RsLodViewToolRoll.height + 30)
		setupCallbacks()
		toolRunning = true
		updateHDDistVis()
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on HDTextureDistanceUI close do
	(
		toolRunning = false
		removeCallbacks()
		try(DestroyDialog ExternalPivotItemsUI)catch()
		completeRedraw()
	)
	
)

CreateDialog HDTextureDistanceUI style:#(#style_titlebar, #style_resizing, #style_sysmenu, #style_minimizebox, #style_maximizebox)
