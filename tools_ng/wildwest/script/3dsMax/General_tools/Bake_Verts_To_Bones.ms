-- Vert to bone animation converter
-- To rip vertex transforms and project onto bones.
-- Rick Stirling
-- Rockstar North
-- November 2011

-- January 2012, added some error checking



-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_Rigging.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/mrSkeleton_2_functions.ms")
-- filein ("x:\wildwest\script\3dsMax\_config_files\Wildwest_header.ms")
-- filein ("x:\wildwest\script\3dsMax\_common_functions\FN_RSTA_Rigging.ms")
-- filein ("x:\wildwest\script\3dsMax\Characters\Rigging\mrSkeleton_v2\mrSkeleton_2_functions.ms")

--<= not used anymore (?)
rootSystemBuilt = #()	





	struct Strct_BakeVertsToBones
	(
		MyObject,
		MyRollout,
		RootBones,
		AnimatedBones = #(),
		AnimOptimSteps = 10,
		PosKeysToKeep = #(),

		
	
			--get all anim from selection (dummy) and childs
			fn RSTA_GetControllers =
			(
				
				--First navigate through the Root wich will contain all the animated bones and get them
				MoverHlp =  MyObject.children
				select MoverHlp						
				RootHlp =  selection[1].children
				select RootHlp
				AnimatedBones = selection[1].children

				--reselect the Dummy. Tidy.
				select MyObject
				
			),
		
			
			
			
			
			--loop through each of the controllers and optimize the key count
			fn RSTA_OptimizeKeys =
			(	
				--Progress Bar count
				ProgCount = 0
				
				For ctrli = 1 to AnimatedBones.count do
				(
					KeysToKeep =#()
					PosKeysToKeep =#()
					ctrl =  AnimatedBones[ctrli]
					select ctrl 				
					
									
					--How Many Keys do we have here ?
					--ctrlKeyCnt = (ctrl.position.controller[1].keys.count)						
					ctrlKeyCnt = (ctrl.position.controller.keys.count)		
					
					--How much do we want to optimize ?	
					if ctrlKeyCnt > 0 do
					(
						--OptimRatio = ctrlKeyCnt/(ctrlKeyCnt-(ctrlKeyCnt*(AnimOptimSteps/100)))
						
						OptimRatio = ctrlKeyCnt/AnimOptimSteps
							
						intervalKey = ctrlKeyCnt/OptimRatio	
			
						--Keep keys only each "OptimRatio". Kill all others.
						--How many intervales ?
						KeyNb = ctrlKeyCnt/intervalKey	
						--Which keys do we keep ?
						append KeysToKeep 0
						TheInterval = intervalKey
						for intervali = 1 to KeyNb do
						(						
							Thekey = TheInterval
							append PosKeysToKeep TheInterval
							TheInterval += intervalKey
						)
						appendifunique PosKeysToKeep ctrlKeyCnt
					
						--Delete keys between those intervales
						--For some reasons max refuse to delete pos.rot.scale keys properly in one single loop, have to separate them. 
						
						--Position Keys loop
						FirstKill = 1
						For intervali = 1 to PosKeysToKeep.count do
						(
							EndInterval = PosKeysToKeep[intervali]									
							KeyInterval = EndInterval - 1							
							TheFirst = floor FirstKill	as integer								
							TheLast = floor KeyInterval as integer						
							selectKeys ctrl.pos.controller (interval TheFirst TheLast)
							deselectKeys ctrl.pos.controller (interval  (ctrlKeyCnt-2 as integer) (ctrlKeyCnt+2 as integer) )						
							deleteKeys ctrl.pos.controller #selection-- 						
						
							FirstKill = EndInterval+1
						)						
						--Rotation Keys loop
						FirstKill = 1
						For intervali = 1 to PosKeysToKeep.count do
						(
							EndInterval = PosKeysToKeep[intervali]									
							KeyInterval = EndInterval - 1							
							TheFirst = floor FirstKill	as integer							
							TheLast = floor KeyInterval as integer							
							--rotation
							selectKeys ctrl.rotation.controller (interval TheFirst TheLast)
							deselectKeys ctrl.rotation.controller (interval  (ctrlKeyCnt-2 as integer) (ctrlKeyCnt+2 as integer) )						
							deleteKeys ctrl.rotation.controller #selection
						
							FirstKill = EndInterval+1
						)						
						--Scale Keys loop
						FirstKill = 1
						For intervali = 1 to PosKeysToKeep.count do
						(
							EndInterval = PosKeysToKeep[intervali]									
							KeyInterval = EndInterval - 1								
							TheFirst = floor FirstKill	as integer								
							TheLast = floor KeyInterval as integer							
							--scale
							selectKeys ctrl.scale.controller (interval TheFirst TheLast)
							deselectKeys ctrl.scale.controller (interval  (ctrlKeyCnt-2 as integer) (ctrlKeyCnt+2 as integer) )						
							deleteKeys ctrl.scale.controller #selection
						
							FirstKill = EndInterval+1
						)
						
						
					)
					max select none	
						--Update Progress Bar
					ProgCount += 1
					MyRollout.Bake_Verts_To_Bones.pgr_OptimAnimProg.value = 100.*ProgCount/AnimatedBones.count
					
				)


				--Once done re-ini the progress bar, ready for the next.
				MyRollout.Bake_Verts_To_Bones.pgr_OptimAnimProg.value = 0		
				--re select the dummy
					select MyObject
			)
			
			
			

		
		
	)
	Strct_BakeVertsToBones = Strct_BakeVertsToBones()



-- Create a clone of the base object with zero modifiers
fn CreateCleanMesh meshToClean = 
(	
	for objectmodifiers = 1 to (meshToClean.modifiers.count) do deleteModifier meshToClean 1
	meshToClean -- return the mesh
)


fn bakeModifiersToVerts bakeObj AnimStart AnimEnd = 
(
	try
	(
		bmVertData = #()

		-- Stick an edit Mesh modifier on the stack because max is shit at verts in epoly mode. Meh.
		addmodifier  bakeObj  (Edit_mesh())
		max modify mode
		subObjectLevel = 1
		numVerts = bakeObj.Vertices.count
			
		-- Data Collection 	
		-- Loop through the timebar and grab the vert position at that frame
		for timebar = AnimStart to AnimEnd do
		(
			local curVerts = #()
			local curVertsPos = #()
			local frameVertData =#()
			at time timebar
			(
				for vertIndex = 1 to numVerts do append frameVertData (bakeObj.vertices[vertIndex].pos)
			)
			append bmVertData frameVertData
		)

	
		-- Delete all the modifiers from the stack
		-- This gives a base object
		bakeobj = CreateCleanMesh bakeobj
		
		
		-- get a base pose.
		BasePoseVertData =#()
		at time AnimStart
		(
				for vertIndex = 1 to numVerts do append BasePoseVertData (bakeObj.vertices[vertIndex].pos)
		)


		-- Data Baking
		with animate on
		(
			-- loop through the timerange and set the verts
			for timebar = AnimStart to AnimEnd do
			(
				at time timebar
					(	
					for vertIndex = 1 to numVerts do
					(
						polyop.setVert bakeObj vertIndex bmVertData[timebar + 1][vertIndex]
					)
				)
			)
		)
		
		
		-- Write base pose to frame -1f, just to be sure.
		sliderTime = -1f
		with animate on
		(
			at time -1
			(	
				for vertIndex = 1 to numVerts do
				(
					polyop.setVert bakeObj vertIndex BasePoseVertData[vertIndex]
				)
			)
		)
	)

	
	catch
	(
		messagebox "Couldn't bake the proxy mesh"
	)
)


-- For translation only
-- Take in an array of starting postitions to create the bones
fn createBoneVerts selObj BasePoseVertData = 
(
	bonelist = #()
	
	for theboneIndex = 1 to BasePoseVertData.count do
	(
		buildPos = BasePoseVertData[theboneIndex]
		boneName = selObj.name+"_B" + theboneIndex as string
		thebone =  buildPointHelper boneName buildPos 0.1 (color 255 128 0)
		tag = "exportTrans = true"
		setUserPropbuffer thebone tag
		
		append bonelist thebone
	)	
	return bonelist -- return the list of created bones.
)


-- Fucntion to create a proxy mesh to bake down
fn createProxyMesh selObj proxytype =
(
	proxyObj  = undefined
	
	-- Do we create a new mesh or a clone of the original?
	-- A copy is easy
	if proxytype == 1 then
	(
		-- Clone the original object
		proxyObj = copy selObj
		proxyObj.name = "PROXY_BAKE_" + selobj.name
	)	

	-- A new mesh is harder
	-- I need to copy the modifiers too
	if proxytype == 2 then
	(
		-- Create the proxy box
		boundingBox = nodeGetBoundingBox selObj selObj.transform
		boundingBoxSize = (boundingBox[2]-boundingBox[1])
		in coordsys selObj (
			proxyObj = box width:boundingBoxSize[1] length:boundingBoxSize[2] height:boundingBoxSize[3]
			CenterPivot proxyObj
			proxyObj.transform = selObj.transform
			proxyObj.pivot = selObj.pivot
			proxyObj.pos = selObj.center
			proxyObj.lengthsegs = 3
			proxyObj.widthsegs = 3
			proxyObj.heightsegs = 3
			ConvertTo proxyObj Editable_Poly
			proxyObj.name = "PROXY_BAKE_" + selobj.name
		)
		
		-- Copy the modifiers
		picked_mods = #()
		for i = 1 to selObj.modifiers.count do (append picked_mods selObj.modifiers[i].name)
		
		for i = picked_mods.count to 1 by -1 do --Loop for modifiers
		(
			temp_mod = selObj.modifiers[picked_mods[i]]
			addmodifier proxyObj temp_mod
		)

	)		
	select proxyobj
	proxyobj -- Return object node
)	


-- We'll take the transforms from the verts in the proxyobj
-- these transforms get applied to bones.
fn animateBonesusingVerts proxyobj bonelist animstart AnimEnd = 
(
	with animate on
	(
		-- loop through the Timerange and set the pone position to match the verts
		for timebar = -1 to AnimEnd do
		(
			at time timebar
			(	
				for arrayIndex = 1 to bonelist.count do
				(
					
					baseVertPos=(getVert proxyobj.mesh arrayIndex) * proxyobj.objectTransform 
					basevertexNormal = getNormal proxyobj.mesh arrayIndex	
					
					-- Pick a random edge connected to this vert
					allConnectedEdges = (polyop.getEdgesUsingVert proxyobj arrayIndex) as array
					firstConnectedEdge = allConnectedEdges[1]

					-- Stick the known vert into the vert list for that edge
					-- This is one I want to ignore
					connectedVertlist = #(arrayIndex)

					-- Find all verts on that edge, append if unique to connectedVertlist 
					allConnectedVerts = (polyop.getVertsUsingEdge proxyobj firstConnectedEdge) as array
					for v = 1 to allConnectedVerts.count do appendifunique connectedVertlist allConnectedVerts[v]
						
					-- the last vert in the new list is the connected vert, thants the one we want
					-- This will probably not be perpendicular, but it will give me a vector
					secondvert = connectedVertlist[connectedVertlist.count]
					secondvertpos = (getVert proxyobj.mesh secondvert)* proxyobj.objectTransform
					secondVector =  secondvertpos - baseVertPos
										
					-- I now have 2 vectors, these describe a plane, and I can get the cross product of that to get third vector and create another place
					-- Then I can cross product that plane to get the 3rd 
					row1=basevertexNormal
					row2=normalize (cross row1 secondVector)
					row3=normalize (cross row1 row2)
					finalTransform = matrix3 row1 row2 row3 baseVertPos

					thebone = bonelist[arrayIndex]
					thebone.transform = finalTransform
				)	-- end bone loop
			) 
		) -- end time loop
	)	-- end animation on
)


fn applySkinToMesh skinmesh bonelist = 
(
	sliderTime = -1f
	local skinMod = Skin()
	skinMod.bone_limit = 4
	addModifier skinmesh skinMod
	
	-- Make sure we're on the modify panel because we have to be to use the skinOps struct to add bones and edit them
	SetCommandPanelTaskMode #Modify
	select skinmesh

	-- Add the main and root bones.

	
	--coment out, this array dont seem to be used anymore, replaced by  Strct_BakeVertsToBones.RootBones 
-- 	mainbone = rootSystemBuilt[1][2]
-- 	rootbone = rootSystemBuilt[1][1]
	
	mainbone = Strct_BakeVertsToBones.RootBones 
	rootbone = Strct_BakeVertsToBones.RootBones 
	
	skinOps.AddBone skinMod rootbone 0
	skinOps.AddBone skinMod mainbone 0
	
	cCount = skinOps.getNumberCrossSections skinMod 1

	for i = 1 to cCount do 
	(
		skinOps.SetInnerRadius skinMod 1 i 0.0
		skinOps.SetOuterRadius skinMod 1 i 0.0
	)

	for i = 1 to cCount do 
	(
		skinOps.SetInnerRadius skinMod 2 i 0.0
		skinOps.SetOuterRadius skinMod 2 i 0.0
	)

	-- Loop through all the bones adding and setting them up
	for boneIndex = 1 to bonelist.count do 
	(
		-- If i is less than the number of bones we can add a bone without updating the FFDobject.
		-- When the last bone is added, we also update the FFDobject
		if boneIndex < bonelist.count then
		(
			skinOps.AddBone skinMod bonelist[boneIndex] 0
		)
		else
		(
			skinOps.AddBone skinMod bonelist[boneIndex] 1
		)
		
		cCount = skinOps.getNumberCrossSections skinMod (boneIndex+2)
		for i = 1 to cCount do 
		(
			skinOps.SetInnerRadius skinMod (boneIndex+2) i 0.1
			skinOps.SetOuterRadius skinMod (boneIndex+2) i 0.1
		)
	)
)		

try(DestroyDialog Bake_Verts_To_Bones)catch()

-------------------------------------------------------------------------------------------------------------------------
rollout Bake_Verts_To_Bones "Bake Verts To Bones" width:220 height:380
(
	
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:Bake_Verts_To_Bones.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Bake_Verts_To_Bones" filename:(getThisScriptFilename())

	group "Set Source Mesh"
	(	
	edittext edtSourceName "Source" width:170 height:20 readonly:true
	button btnSelectSourceMesh "Set Selection as Source Mesh" width:170 height:25 
	

	)
	group "Auto-Rig Optimization"
	(
	radiobuttons rdoProxyType labels:#("Proxy Mesh Copy", "Proxy FFD Box")
	button btnCreateProxyMesh "Create Proxy Mesh" width:170 height:25  toolTip:"Creat mesh to be optimized"	
	button btnSelectProxyMesh "Set Selection as Proxy Mesh" width:170 height:25
	
	edittext edtProxyname "Proxy" width:170 height:20 readonly:true
	
	spinner spnAnimStart "Start Frame:" range:[0,10000,0] type:#integer
	spinner spnAnimEnd "End Frame:" range:[0,10000,0] type:#integer
	
	button btnBakeProxyMesh "Bake Motion to Proxy Mesh" width:170 height:25
	)	
	group "Auto-Rigging"
	(			
	button btnSkinCloneMesh "Skin/Animate Clone of Source" width:170 height:25
	)
		
	group "Optimize animation"
	(	
	spinner spn_OptimSteps "Optim Steps"  range:[2,100,10 ] type:#integer  toolTip:"Set new interval of keys. ie 10 = 1 key every 10 frames."		
	button btnOptimAnim "Optimise Anim" width:170 height:25	 toolTip:"Optimize animation by removing key frames "		
	progressbar pgr_OptimAnimProg  color:green offset:[0,-2] -- a green progress bar		
	)
	group "Skining"
	(
	button btnSkinBrush "Skin Brush Tool" width:170 height:25	toolTip:"Bring the Skin Brush tool"	
	button btnAlwaysDeform "Fix Bust Skin" width:170 height:25 toolTip:"Trigger Always Deform"	
	)
	
	

	
	on Bake_Verts_To_Bones open do
	(
		
		banner.setup()
		
		Bake_Verts_To_Bones.spnAnimEnd.value = animationRange.end

		try 
		(
			theSelection = getcurrentselection()
			-- If yes, create a proxy mesh based on the UI choice.
			if (theSelection.count > 0) then
			(
				selObj = theSelection[1]
				
				-- Check that this is an editable poly object
				-- I should also have at least one thing in the stack
				numMods = selObj.modifiers.count
				
				if ((classof selObj == PolyMeshObject) and (numMods > 0)) then
				(
					edtSourcename.text = selObj.name
				)
				
				else 
				(
					--The object selected as a proxy doesn't seem to be an Editable Poly object, or has no modifiers."
				)
			)	 
		) catch()
		
		
		Strct_BakeVertsToBones.MyRollout = Bake_Verts_To_Bones

	)	
	
	
	-- This will take the users chosen mesh and set it to be the proxy
	on btnSelectSourceMesh pressed do
	(
		-- Valid selection?
		theSelection = getcurrentselection()
		
		-- If yes, create a proxy mesh based on the UI choice.
		if (theSelection.count > 0) then
		(
			selObj = theSelection[1]
			
			-- Check that this is an editable poly object
			-- I should also have at least one thing in the stack
			numMods = selObj.modifiers.count
			
			if ((classof selObj == PolyMeshObject) and (numMods > 0)) then
			(
				edtSourcename.text = selObj.name
			)
			
			else 
			(
				messagebox "The object selected as a proxy doesn't seem to be an Editable Poly object, or has no modifiers."
			)
		)
	) -- End proxy mesh creation
	
	
	on btnCreateProxyMesh pressed do 
	(
		-- Valid selection?
		theSelection = getcurrentselection()
		
		-- If yes, create a proxy mesh based on the UI choice.
		if (theSelection.count > 0) then
		(
			selObj = theSelection[1]
			
			-- Check that this is an editable poly object
			-- At a later date this could be made mesh type agnostic
			-- The proxy doesn't care.
			if classof selObj == PolyMeshObject then
			(
				proxyType = Bake_Verts_To_Bones.rdoProxyType.state
				proxyobj = CreateProxyMesh selObj proxyType
				
				edtSourcename.text = selObj.name
				edtProxyname.text = proxyobj.name
			)
			
			else 
			(
				messagebox "Selected Object doesn't seem to be an Editable Poly object. This is required for querying the vertex transforms."
			)
		)	

		else 
		(
			messagebox "Please select a valid mesh to create a proxy for."
		)
	) -- End proxy mesh creation


	-- This will take the users chosen mesh and set it to be the proxy
	on btnSelectProxyMesh pressed do
	(
		-- Valid selection?
		theSelection = getcurrentselection()
		
		-- If yes, create a proxy mesh based on the UI choice.
		if (theSelection.count > 0) then
		(
			selObj = theSelection[1]
			
			-- Check that this is an editable poly object
			-- I should also have at least one thing in the stack
			numMods = selObj.modifiers.count
			
			if ((classof selObj == PolyMeshObject) and (numMods > 0)) then
			(
				edtProxyname.text = selObj.name
			)
			
			else 
			(
				messagebox "The object selected as a proxy doesn't seem to be an Editable Poly object, or has no modifiers."
			)
		)	
	) -- End proxy mesh creation

	
	on btnBakeProxyMesh pressed do
	(
		AnimStart = Bake_Verts_To_Bones.spnAnimStart.value
	 	AnimEnd = Bake_Verts_To_Bones.spnAnimEnd.value	
		

		
		try 
		(
			proxyobj = getnodebyname Bake_Verts_To_Bones.edtProxyname.text
			
			-- check vert count on the proxy mesh
			-- needs to be under a certain threshold (127 for GTA5)
			numVerts = proxyobj.Vertices.count
			if numverts < 125 then 
			(
				bakeModifiersToVerts proxyobj AnimStart AnimEnd
			)	
			
			else 
			(
				messagebox "There are more than 125 verts in the proxy object. 127 is the limit for bones, and the object requires a a root bone and main bone."
			)
		) 
		
		catch
		(
			messagebox "Couldn't find the proxy mesh"
		)
	) -- end bakedown of proxy mesh

	
	on btnSkinCloneMesh pressed do
	(
		-- Go to the base pose of the proxe mesh and create the deformer bones
		
		selobj = undefined
		proxyobj  =undefined

		local bonelist = #()
		try 
		(
			selobj = getnodebyname Bake_Verts_To_Bones.edtSourceName.text
			proxyobj = getnodebyname Bake_Verts_To_Bones.edtProxyName.text
			
			-- Get the rest pose vert positions
			-- These will be used to create the positions of the bones.
			BasePoseVertData =#()
			at time -1
			(
				for vertIndex = 1 to proxyobj.numverts do append BasePoseVertData (proxyobj.vertices[vertIndex].pos)
			)

			-- Create the bones and store their node details
			bonelist = createBoneVerts selObj BasePoseVertData
		) 
		catch
		(
			messagebox "Failed to create bones on the verts"
		)

		skinMesh = copy selobj
		skinMesh.name = selobj.name + "_Skin"
		skinMesh = CreateCleanMesh skinmesh

		-- Need to build the root section of the skeleton now
		RSTA_buildPropRoot skinMesh false false
		

		
		movObj = getNodeByName ("Mover_"+skinMesh.name)			
		mainBone = movObj.children[1]
		Strct_BakeVertsToBones.RootBones = movObj.children[1]
		-- Parent these bones to the main bone
		for boneindex = 1 to bonelist.count do 
		(	
-- 			 bonelist[boneindex].parent = mainbone = rootSystemBuilt[1][2]
			bonelist[boneindex].parent = mainbone
		)	

		-- Skin the mesh with the animating bones
		applySkinToMesh skinmesh bonelist
		
		-- Animate the bones
		-- At every frame, set the transform of the bonelist bones to match the vert transform.
		AnimStart = Bake_Verts_To_Bones.spnAnimStart.value
	 	AnimEnd = Bake_Verts_To_Bones.spnAnimEnd.value	
		animateBonesusingVerts proxyobj bonelist AnimStart AnimEnd
	)	

	
	on btnSkinBrush pressed do
	(
		filein ped_skinbrush_script 
	)

	
	-- try to fix bust skinning
	on btnAlwaysDeform pressed do
	(
		-- Valid selection?
		theSelection = getcurrentselection()
		
		-- If yes, create a proxy mesh based on the UI choice.
		if (theSelection.count > 0) then
		(
			selObj = theSelection[1]
			
			
			foundskin = false	
				
			for m = 1 to selObj.modifiers.count do 
			(
				if classof selObj.modifiers[m] == skin then foundskin = true
			)
			
			if (foundskin == true) then
			(
				-- Toogle the always deform 
				sliderTime = 0f
				max modify mode
				selObj.modifiers[#Skin].always_deform=off
				selObj.modifiers[#Skin].always_deform=on
			)
			
			else 
			(
				messagebox "The object selected doesn't seem to have a Skin modifier on it."
			)
		)	
	)	
	
	
	
	on btnOptimAnim pressed do
	(
		
		try (
		--Get controllers
		Strct_BakeVertsToBones.MyObject	= selection[1]
		Strct_BakeVertsToBones.RSTA_GetControllers()
		
		--How much do we want to optimise ?
		Strct_BakeVertsToBones.AnimOptimSteps = spn_OptimSteps.value
		--Do it
		Strct_BakeVertsToBones.RSTA_OptimizeKeys()
		)catch (messagebox "Please select the main dummy")
	)
	
	
	
)
createDialog Bake_Verts_To_Bones 210 575