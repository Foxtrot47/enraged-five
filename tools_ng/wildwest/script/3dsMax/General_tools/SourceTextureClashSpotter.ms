filein (RsConfigGetWildWestDir() + "script/3dsmax/_config_files/wildwest_header.ms")

filein (RsConfigGetToolsRootDir() + "/dcc/current/max2012/scripts/pipeline/helpers/materials/texture_test.ms")

struct TextureInfoStruct
(
	name,
	collisions = #()
)

struct SourceTextureClashStruct
(
	sourceTextures = #(),
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn CheckForTextureFilenameCollisions texturePathnames =
	(
		filenameLocationHashtable = dotnetobject "System.Collections.Hashtable"
		
		for texturePathname in texturePathnames do
		(
			textureFilename = getFilenameFile texturePathname
			
			if( filenameLocationHashtable.ContainsKey(textureFilename) ) then
			(
				oldCollection = filenameLocationHashtable.item[textureFilename]
				filenameLocationHashtable.Remove textureFilename
				filenameLocationHashtable.Add textureFilename (append oldCollection texturePathname)
			)
			else
			(
				filenameLocationHashtable.Add textureFilename #(texturePathname)
			)
		)
		
		local enumerator = filenameLocationHashtable.GetEnumerator()
		while enumerator.MoveNext() do
		(
			if( enumerator.value.count > 1 ) do
			(
				local texInfo = TextureInfoStruct()
				texInfo.name = enumerator.key
				for texPathname in enumerator.value do
				(
					append texInfo.collisions texPathname
				)
				
				append sourceTextures texInfo
			)
		)
	),
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn clearSourceTextures =
	(
		sourceTextures = #()
	),
	
	--/////////////////////////////////////////
	-- return the list of collisions for a given texture name
	--/////////////////////////////////////////
	fn getCollisions texName =
	(
		local collisions = undefined
		for item in sourceTextures where item.name == texName do collisions = item.collisions
		
		--retval
		collisions
	),
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn fixMaterials texturePath =
	(
		if texturePath == undefined then return false
			
		local sceneTextures = getClassInstances bitmaptexture
		if sceneTextures == undefined then return false
			
		for tex in sceneTextures where (getfilenamefile tex.filename) == (getfilenamefile texturePath) do
		(
			tex.filename = texturePath
		)
	)
)

--/////////////////////////////////////////
--	UI
--/////////////////////////////////////////
try(destroyDialog SourceTextureClashUI)catch()
rollout SourceTextureClashUI "Source Texture Clash Reporter" width:630 height:460
(
	--/////////////////////////////////////////
	--	VARIABLES
	--/////////////////////////////////////////
	local texClashFuncs = SourceTextureClashStruct()
	local collisionPaths = #()
	
	--/////////////////////////////////////////
	--	CONTROLS
	--/////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:SourceTextureClashUI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"CHANGEME" filename:(getThisScriptFilename())
	
	group ""
	(
		button btnSelected "Selected" across:2 width:(((SourceTextureClashUI.width - 20) / 2) - 10) offset:[0, -5]
		button btnAll "All" width:(((SourceTextureClashUI.width - 20) / 2) - 10) offset:[0, -5]
		--button btnUlogGrab "From Ulog" width:(((SourceTextureClashUI.width - 20) / 3) - 10) offset:[0, -5]
	)
	
	group ""
	(
		listbox lstNames height:15 width:400 across:2
		bitmap bmPreview width:200 height:200 offset:[55, 0]
		listbox lstPaths width:605 height:5 offset:[0, 0]
		button btnFix "Fix Selected" width:lstPaths.width
	)
	
	group ""
	(
		edittext edtProcessMsg "Messages:" readonly:true enabled:false offset:[0, -5]
	)
	
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////
	fn processObjects objSet = 
	(
		mats = #()
		
		for obj in objSet do
		(
			if (RsTexTest_ObjWillHaveMaterialsExported obj) do
			(
				appendIfUnique mats obj.material
			)
		)
		
		local sourcePathNames = RSTexTest_GetUniqueSourceTexturePathnames mats
		
		texClashFuncs.CheckForTextureFilenameCollisions sourcePathNames
		
		lstNames.items = for item in texClashFuncs.sourceTextures collect item.name
	)
	
	--/////////////////////////////////////////
	--	EVENTS
	--/////////////////////////////////////////
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btnSelected pressed do
	(
		lstNames.items = #()
		texClashFuncs.clearSourceTextures()
		
		processObjects selection
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btnAll pressed do
	(
		lstNames.items = #()
		texClashFuncs.clearSourceTextures()
		
		processObjects objects
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	/*
	on btnUlogGrab pressed do
	(
		lstNames.items = #()
		texClashFuncs.clearSourceTextures()
		
		
	)
	*/
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn nameSelected arg =
	(
		--print lstNames.selected
		collisionPaths = texClashFuncs.getCollisions lstNames.selected
		if collisionPaths != undefined do
		lstPaths.items = collisionPaths
		
		if not (doesFileExist collisionPaths[1]) then
		(
			gRsPerforce.sync collisionPaths[1]
			bmPreview.filename = collisionPaths[1]
		)
		else
		(
			bmPreview.filename = collisionPaths[1]
		)
	)
	
	on lstNames selected arg do
	(
		nameSelected arg
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn pathSelected arg =
	(
		if not (doesFileExist collisionPaths[arg]) then
		(
			gRsPerforce.sync collisionPaths[arg]
			bmPreview.filename = collisionPaths[arg]
		)
		else
		(
			bmPreview.filename = collisionPaths[arg]
		)
	)
	
	on lstPaths selected arg do
	(
		pathSelected arg
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btnFix pressed do
	(
		if lstPaths.selected == undefined then return false
			
		texClashFuncs.fixMaterials lstPaths.selected
		
		lstNames.items = #()
		texClashFuncs.clearSourceTextures()
		processObjects objects
		lstNames.selection = 1
		bmPreview.filename = collisionPaths[1]
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on SourceTextureClashUI open do
	(
		banner.setup()
		
	)
	
	
)
createDialog SourceTextureClashUI style:#(#style_titlebar, #style_resizing, #style_sysmenu, #style_minimizebox, #style_maximizebox)

