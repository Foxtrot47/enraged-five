--******************************************************************************************************
-- Created: 		10-09-2012
-- Last Updated:	10-09-2012
-- Version:			1.0
--
-- Author :  Kostadin Kotev / miau_u@yahoo.com /    http://miauumaxscript.blogspot.com/
-- Version:  3ds max 9
--
-- Discription: Rotate or Shake active viewport around X, Y, Z axis or any combination of them.
--	
-- Usage: RUN IT
--
-- Special thanks to Denis Trofimov for providing a list with all "actionMan " in 3ds Max:
--	http://forums.cgsociety.org/showpost.php?p=7100836&postcount=1
--
--******************************************************************************************************
 -- Added to R* Tools Oct 2012 Allan Hayburn

filein (RsConfigGetWildWestDir() + "script/3dsmax/_config_files/wildwest_header.ms")

(
	global rol_ViewportRotator
	try(destroyDialog rol_ViewportRotator)catch()
	rollout rol_ViewportRotator "Viewport Turntable"
	(
		-- Rockstar Banner
		dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:rol_ViewportRotator.width
		local banner = makeRsBanner dn_Panel:rsBannerPanel 
		
		local rotDir = 1
		checkButton chkbtn_RotX "X" pos:[5,35] width:40
		checkButton chkbtn_RotY "Y" pos:[49,35] width:40
		checkButton chkbtn_RotZ "Z" pos:[93,35] width:40
		checkbox chkbox_slowRot "Slow" pos:[5,65]
		spinner spn_speed "Speed" range:[1,100,50]  pos:[55,65] fieldwidth:35 
		checkButton chkbtn_back "<<" pos:[5,85] width:60 height:25
		checkButton chkbtn_forward ">>" pos:[73,85] width:60 height:25
		checkbutton chkbtn_shake "Shake" pos:[5,115]
		spinner spn_shakeTolerance "sec:" pos:[66,118] range:[1,60,1] fieldwidth:35
		timer timer_setSpeed "Timer" interval:100 active:false
		
		fn RotateViewport =  
		(
			with undo off
			(
				if rotDir == -1 then
				(
					if chkbtn_RotX.state == true do
					(
						if chkbox_slowRot.state == true then
							(actionMan.executeAction 0 "356")
						else
							(actionMan.executeAction 0 "350")
					)
					if chkbtn_RotY.state == true do
					(
						if chkbox_slowRot.state == true then
							(actionMan.executeAction 0 "357")
						else
							(actionMan.executeAction 0 "351")
					)
					if chkbtn_RotZ.state == true do
					(
						if chkbox_slowRot.state == true then
							(actionMan.executeAction 0 "358")
						else
							(actionMan.executeAction 0 "352")
					)				
				)
				else
				(	
					if chkbtn_RotX.state == true do
					(
						if chkbox_slowRot.state == true then
							(actionMan.executeAction 0 "359")
						else
							(actionMan.executeAction 0 "353")
					)
					if chkbtn_RotY.state == true do
					(
						if chkbox_slowRot.state == true then
							(actionMan.executeAction 0 "360")
						else
							(actionMan.executeAction 0 "354")
					)
					if chkbtn_RotZ.state == true do
					(
						if chkbox_slowRot.state == true then
							(actionMan.executeAction 0 "361")
						else
							(actionMan.executeAction 0 "355")
					)
				)
			)
		)
		
		on rol_ViewportRotator open do
		(
			banner.setup()
			timer_setSpeed.interval = 100 - spn_speed.value
		)
		
		on timer_setSpeed tick do
		(
			if chkbtn_shake.state == true then
			(
				if (mod (timer_setSpeed.ticks) (spn_shakeTolerance.value*10)) == 0.0 then
				(
					rotDir *= -1
					RotateViewport()
				)
				else	
					RotateViewport()
			)
			else
				RotateViewport()
		)
		
		on spn_speed changed val do
		(
			timer_setSpeed.interval = 100 - val
		)
		
		on chkbtn_RotX rightclick do
		(
			chkbtn_RotX.state = true
			chkbtn_RotY.state = false
			chkbtn_RotZ.state = false
		)
		on chkbtn_RotY rightclick do
		(
			chkbtn_RotY.state = true
			chkbtn_RotX.state = false
			chkbtn_RotZ.state = false
		)
		on chkbtn_RotZ rightclick do
		(
			chkbtn_RotZ.state = true
			chkbtn_RotX.state = false
			chkbtn_RotY.state = false
		)
		
		
		on chkbtn_back changed theState do
		(
			if theState then
			(
				chkbtn_forward.state = false
				rotDir = -1
				timer_setSpeed.active = true
			)
			else
			(
				timer_setSpeed.active = false
			)
		)
		
		on chkbtn_forward changed theState do
		(
			if theState then
			(
				chkbtn_back.state = false
				rotDir = 1
				timer_setSpeed.active = true
			)
			else
			(
				timer_setSpeed.active = false
			)
		)	
		
		on chkbtn_shake changed theState do
		(
			timer_setSpeed.active = theState
		)		
	)
	createdialog rol_ViewportRotator width:138  style:#(#style_resizing,#style_titlebar, #style_toolwindow, #style_sysmenu)

)