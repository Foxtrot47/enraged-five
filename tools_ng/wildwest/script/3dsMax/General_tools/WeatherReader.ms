--
-- WeatherReader
-- Loads WeatherCycle data from xml, and shows it in Excel
--
-- Neal.Corbett@rockstarLeeds.com
-- 05 July 2013
--

(
	local InitData = 
	#(
		dataPair Name:"EXTRASUNNY" Colour:(color 237 205 61),
		dataPair Name:"CLEAR" Colour:(color 204 203 170),
		dataPair Name:"SMOG" Colour:(color 209 217 217),
		dataPair Name:"OVERCAST" Colour:(color 225 255 223),
		dataPair Name:"RAIN" Colour:(color 179 181 183),
		dataPair Name:"THUNDER" Colour:(color 83 96 112),
		dataPair Name:"CLEARING" Colour:(color 227 233 243),
		dataPair Name:"CLOUDS" Colour:(color 123 89 71),
		dataPair Name:"FOGGY" Colour:(color 249 232 220)
	)
	
	local WeatherPath = (RsProjectGetCommonDir() + "/data/levels/" + RsProjectGetName()  + "/weather.xml")
	
	local MetaManager = RsGetMetaDataManager()
	MetaManager.LoadWithMetaFile WeatherPath
	
	local WeatherCycles = MetaManager.FindFirstStructureNamed "WeatherCycles"
	
	local CycleNames = for Item in InitData collect Item.Name
	local CycleColours = for Item in InitData collect Item.Colour
	
	local CycleItems = for Idx = 0 to (WeatherCycles.Length - 1) collect 
	(
		local CycleNode = WeatherCycles.Item Idx
		
		local CycleName = toUpper (CycleNode.Item "CycleName").Value
		local TimeMult = (CycleNode.Item "TimeMult").Value
		
		dataPair Name:CycleName Times:TimeMult
	)
	
	-- Export to Excel:
	(
		-- Excel needs a colour's RGB to be reversed because [CLASSIFIED].
		fn ExcelColour clr = (color clr.B clr.G clr.R)
		
		-- Create a new Excel document:
		local Excel = CreateOLEObject "Excel.Application"
		Excel.application.Workbooks.Add
		Excel.ActiveSheet.Name = "Weather Cycles"
		
		local LineNum = 0
		local AlternLine = False
		
		for Item in CycleItems do 
		(
			local CycleName = Item.Name
			local CycleTimes = Item.Times
			
			local ClrNum = findItem CycleNames CycleName
			local BackColour = if (ClrNum == 0) then Red else InitData[ClrNum].Colour
			
			local CellRange = StringStream ""
			format "A%:A%" (LineNum + 1) (LineNum + CycleTimes) to:CellRange
			
			local Cells = Excel.Application.Range(CellRange as string)
			
			Cells.value = CycleName
			if AlternLine then 
			(
				Cells.Font.Italic = True
			) 
			else 			
			(
				Cells.Font.Bold = True
			)
			Cells.Interior.Color = (ExcelColour BackColour)
			
			LineNum += CycleTimes
			AlternLine = (not AlternLine)
		)
		
		-- Resize the columns to fit:
		(excel.application.Range("A1:B1")).entireColumn.AutoFit()
		
		-- Show Excel, then give it focus:
		(
			Excel.visible = True
			
			SC_MINIMIZE = 0xF020
			SC_RESTORE = 0xF120
			WM_SYSCOMMAND = 0x112
			windows.SendMessage Excel.Hwnd WM_SYSCOMMAND SC_MINIMIZE 0
			windows.SendMessage Excel.Hwnd WM_SYSCOMMAND SC_RESTORE 0
			
			releaseOLEObject Excel
		)
	)
	
	OK
)
