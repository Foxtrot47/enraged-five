escapeEnable = true

try (DestroyDialog RSTA_MatIDProjectorUI)catch()
rollout RSTA_MatIDProjectorUI "MatID Projector" width:300
(
	label lblRx "Receiver: " align:#left across:2
	pickbutton pckRx "Pick Reciever" autoDisplay:true width:(0.75 * RSTA_MatIDProjectorUI.width-20) offset:[-40,0]
	label lblTx "Donor: " align:#left across:2
	pickbutton pckTx "Pick Donor" autoDisplay:true width:(0.75 * RSTA_MatIDProjectorUI.width-20) offset:[-40,0]
	checkbox chkByNormal "Project By Normals" across:2
	spinner spnDonorPush "Donor Cage Push:" type:#float range:[0.0, 1.0, 0.0]
	button btnProject "Project!" width:(RSTA_MatIDProjectorUI.width-10)
	
	
	fn Project rx tx byNormal:false =
	(		
		local rm = RayMeshGridIntersect()
		rm.Initialize 30 --init. the voxel grid size to 10x10x10
		rm.addNode tx --add the sphere to the grid
		rm.buildGrid()
		
		local upVec = [0,0,1]
		
		ProgressStart "Projecting mat IDs..."
		for face=1 to rx.faces.count do
		(
			local facePos = meshop.getFaceCenter rx face
			if byNormal do upVec = getfaceNormal rx face
			
			local hits = rm.intersectRay facePos upVec true
			
			if hits>0 then
			(
				local theIndex = rm.getClosestHit () 
				local theFace = rm.getHitFace theIndex
				
				local matId = getfacematid tx theFace
				
				setFacematID rx face matId
			)
			
			progressUpdate (100.0 * (face as float / rx.faces.count))
		)
		
		progressEnd()
	)
	
	--on pckRx picked do
		
	on btnProject pressed do
	(
		local validSelection = false
		
		if (pckRx.object != undefined) and (pckTx.object != undefined) do validSelection = true
		if (isValidNode pckRx.object) and (isValidNode pckTx.object) do validSelection = true
		if (pckRx.object == pckTx.object) then validSelection = false
		
		
		if not validSelection do
		(
			messageBox "Not a valid picked selection, check it" title:"Bad Selection"
			return false
		)
		
		if (spnDonorPush.value > 0.0) do
		(
			if (pckTx.object.modifiers[#Push] == undefined) or (classof pckTx.object.modifiers[pckTx.object.modifiers.count] != Push) then
			(
				addModifier pckTx.object (Push Push_Value:spnDonorPush.value)
			)
			else
			(
				pckTx.object.modifiers[#Push].Push_Value = spnDonorPush.value
			)
		)
		
		with undo off
		(
			Project pckRx.object pckTx.object byNormal:(chkByNormal.checked)
		)
	)
	
)

createDialog RSTA_MatIDProjectorUI