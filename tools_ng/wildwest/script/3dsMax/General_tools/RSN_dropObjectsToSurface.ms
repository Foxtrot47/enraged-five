filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())


fn RSN_g_filter o = superclassof o == Geometryclass

fn RSN_dropObjectsToSurface = 
(
	target_mesh = pickObject message:"Pick Target Surface:" filter:RSN_g_filter
	if isValidNode target_mesh then
	(
		undo "MoveToSurface" on
		(
			for i in selection do
			(
				int_point = find_Z_intersection target_mesh i	--find_Z_intersection() in FN_Common.ms
				if int_point != undefined then i.pos = int_point.pos

			)--end i loop
		)--end undo
	)--end if
)--end execute

RSN_dropObjectsToSurface()