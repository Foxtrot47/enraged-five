--matchMarkers.ms
--Tool to match markers to stretchy skeleton then rebuild a clean game ready version
--Feb 2011
--Matt Rennie


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

global currMaxScene = undefined
global theNewFloater = undefined
global rootTrans = undefined

 filein (theWildWest + "script/max/Rockstar_North/character/Rigging_tools/C_Build_Ped_Rig_from_Markers.ms")

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

markersArray = #(
"Marker_Root",
"Marker_Pelvis",
"Marker_Spine0",
"Marker_Spine1",
"Marker_Spine2",
"Marker_Spine3",	
"Marker_Neck1",
"Marker_Head",
"Marker_HeadNub",	
	
	
"Marker_L_Thigh",
"Marker_L_Calf",
"Marker_L_Foot",
"Marker_L_Toe0",
"Marker_L_Toe0Nub",

	
"Marker_R_Thigh",	
"Marker_R_Calf",
"Marker_R_Foot",
"Marker_R_Toe0",
"Marker_R_Toe0Nub",

"Marker_L_Clavicle",
"Marker_L_UpperArm",
"Marker_L_Forearm",
"Marker_L_Hand",
"Marker_L_Finger00",
"Marker_L_Finger01",
"Marker_L_Finger02",
"Marker_L_Finger0Nub",
"Marker_L_Finger10",
"Marker_L_Finger11",
"Marker_L_Finger12",
"Marker_L_Finger1Nub",
"Marker_L_Finger20",
"Marker_L_Finger21",
"Marker_L_Finger22",
"Marker_L_Finger2Nub",
"Marker_L_Finger30",
"Marker_L_Finger31",
"Marker_L_Finger32",
"Marker_L_Finger3Nub",
"Marker_L_Finger40",
"Marker_L_Finger41",
"Marker_L_Finger42",
"Marker_L_Finger4Nub",

"Marker_R_Clavicle",
"Marker_R_UpperArm",
"Marker_R_Forearm",
"Marker_R_Hand",
"Marker_R_Finger10",
"Marker_R_Finger11",
"Marker_R_Finger12",
"Marker_R_Finger1Nub",
"Marker_R_Finger20",
"Marker_R_Finger21",
"Marker_R_Finger22",
"Marker_R_Finger2Nub",
"Marker_R_Finger30",
"Marker_R_Finger31",
"Marker_R_Finger32",
"Marker_R_Finger3Nub",
"Marker_R_Finger40",
"Marker_R_Finger41",
"Marker_R_Finger42",
"Marker_R_Finger4Nub",
"Marker_R_Finger00",
"Marker_R_Finger0Nub",
"Marker_R_Finger02",
"Marker_R_Finger01",

"Marker_L_Finger_CurlRoot",
"Marker_R_Finger_CurlRoot",
"Marker_L_thumbhelper1",
"Marker_L_thumbhelper2",
"Marker_R_thumbhelper2",
"Marker_R_thumbhelper1",
"Marker_L_Finger_CurlEnd",
"Marker_R_Finger_CurlEnd"

)

skelArray = #(
"SKEL_ROOT", 
"SKEL_Pelvis", 
"SKEL_L_Thigh", 
"SKEL_L_Calf", 
"SKEL_L_Foot", 
"SKEL_L_Toe0", 
"SKEL_R_Thigh", 
"SKEL_R_Calf", 
"SKEL_R_Foot", 
"SKEL_R_Toe0", 
"SKEL_Spine_Root", 
"SKEL_Spine0", 
"SKEL_Spine1", 
"SKEL_Spine2", 
"SKEL_Spine3", 
"SKEL_L_Clavicle", 
"SKEL_L_UpperArm", 
"SKEL_L_Forearm", 
"SKEL_L_Hand", 
"SKEL_L_Finger00", 
"SKEL_L_Finger01", 
"SKEL_L_Finger02", 
"SKEL_L_Finger10", 
"SKEL_L_Finger11", 
"SKEL_L_Finger12", 
"SKEL_L_Finger20", 
"SKEL_L_Finger21", 
"SKEL_L_Finger22", 
"SKEL_L_Finger30", 
"SKEL_L_Finger31", 
"SKEL_L_Finger32", 
"SKEL_L_Finger40", 
"SKEL_L_Finger41", 
"SKEL_L_Finger42", 
"SKEL_R_Clavicle", 
"SKEL_R_UpperArm", 
"SKEL_R_Forearm", 
"SKEL_R_Hand", 
"SKEL_R_Finger00", 
"SKEL_R_Finger01", 
"SKEL_R_Finger02", 
"SKEL_R_Finger10", 
"SKEL_R_Finger11", 
"SKEL_R_Finger12", 
"SKEL_R_Finger20", 
"SKEL_R_Finger21", 
"SKEL_R_Finger22", 
"SKEL_R_Finger30", 
"SKEL_R_Finger31", 
"SKEL_R_Finger32", 
"SKEL_R_Finger40", 
"SKEL_R_Finger41", 
"SKEL_R_Finger42", 
"SKEL_Neck_1", 
"SKEL_Head"
)

skinBonesArray = #(
"SKEL_ROOT",
"SKEL_Pelvis",
"SKEL_L_Thigh",
"SKEL_L_Calf",
"SKEL_L_Foot",
"SKEL_L_Toe0",
"SKEL_R_Thigh",
"SKEL_R_Calf",
"SKEL_R_Foot",
"SKEL_R_Toe0",
"RB_L_ThighRoll",
"RB_R_ThighRoll",
"SKEL_Spine0",
"SKEL_Spine1",
"SKEL_Spine2",
"SKEL_Spine3",
"SKEL_L_Clavicle",
"SKEL_L_UpperArm",
"SKEL_L_Forearm",
"SKEL_L_Hand",
"SKEL_L_Finger00",
"SKEL_L_Finger01",
"SKEL_L_Finger02",
"SKEL_L_Finger10",
"SKEL_L_Finger11",
"SKEL_L_Finger12",
"SKEL_L_Finger20",
"SKEL_L_Finger21",
"SKEL_L_Finger22",
"SKEL_L_Finger30",
"SKEL_L_Finger31",
"SKEL_L_Finger32",
"SKEL_L_Finger40",
"SKEL_L_Finger41",
"SKEL_L_Finger42",
"RB_L_ForeArmRoll",
"RB_L_ArmRoll",
"SKEL_R_Clavicle",
"SKEL_R_UpperArm",
"SKEL_R_Forearm",
"SKEL_R_Hand",
"SKEL_R_Finger00",
"SKEL_R_Finger01",
"SKEL_R_Finger02",
"SKEL_R_Finger10",
"SKEL_R_Finger11",
"SKEL_R_Finger12",
"SKEL_R_Finger20",
"SKEL_R_Finger21",
"SKEL_R_Finger22",
"SKEL_R_Finger30",
"SKEL_R_Finger31",
"SKEL_R_Finger32",
"SKEL_R_Finger40",
"SKEL_R_Finger41",
"SKEL_R_Finger42",
"RB_R_ForeArmRoll",
"RB_R_ArmRoll",
"SKEL_Neck_1",
"SKEL_Head",
"RB_Neck_1",
"SKEL_Spine_Root",
"PH_R_Hand",
"PH_L_Hand"
)

spotMarkersArray = #()

closeRolloutFloater theNewFloater
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn LSW_EnvelopeCallbackFunction =
(
	WindowHandle = DialogMonitorOPS.GetWindowHandle()
	theDialogName = UIAccessor.GetWindowText WindowHandle

	if theDialogName != undefined and matchpattern theDialogName pattern:"*Load Envelopes*" do
		UIAccessor.PressButtonByName WindowHandle "Match by Name"	
		
	if theDialogName != undefined and matchpattern theDialogName pattern:"*Load Envelopes*" do
		UIAccessor.PressButtonByName WindowHandle "OK"

	true
)



fn renameMarkers =
(
	clearListener()
	output_name = "c:\skins\animations\originalScene.MJR" 
		output_file = createfile output_name		
		theSceneName = (maxFilePath + maxFileName)

		format ((theSceneName as string)+"\n") to:output_file
		
	close output_file
-- 	
-- 	currMaxFile = (maxFilePath + maxFileName)	
	
	spotMarkersArray = #()
		
	for obj in markersArray do
	(
		currMarker = (getNodeByName obj)
		if currMarker != undefined do
		(
			print ("renaming "+currMarker.name+" to "+"SPOT_"+obj)
			currMarker.name = ("SPOT_"+obj)
			appendIfUnique spotMarkersArray currMarker
		)
	)
	mergeMaxFile "X:\\gta5\\art\\peds\\Skeletons\\Male Markers.max" #deleteOldDups #alwaysReparent
	
-- 	matchMarkers()
)


fn prepSkinModifiers geo = 
(
	print ("Starting skin population on "+geo.name)
		select geo
		if geo.modifiers[#Skin] == undefined then
		(
			modPanel.addModToSelection (Skin ()) ui:on
		)
		
		geo.modifiers[#Skin].bone_Limit = 4
		
		for i = 1 to skinBonesArray.count do
		(
			select geo
			SetCommandPanelTaskMode mode:#modify
			boneToAdd = getNodeByName skinBonesArray[i]

			if boneToAdd != undefined do
			(
				boneToAdd = (getNodeByName skinBonesArray[i])
				if boneToAdd != undefined do
				(
					print ("adding "+skinBonesArray[i]+" to skin on "+geo.name)
					skinOps.addbone geo.modifiers[#Skin] boneToAdd 0
				)
			)
		)
		
		
		input_name = "c:\\skins\\animations\\charSex.MJR"
		f = openfile input_name
		inputData = #() -- define as array
		while not eof f do
		(
			append inputData (filterstring (readLine f) "\n")
		)
		close f

		stringName = ( inputData[1] as string)
		
		charSex = stringName
		
		skinToUse = undefined
		
		if geo.name == "head_000_R" then
		(
			if charSex == "M" then
			(
				skinToUse = "X:\\gta5\\art\\peds\\Story_Characters\\CS_Male_Proxy\\skinData\\maleHardWeights.env"
				print "Using male skin"
			)
			else
			(
				skinToUse = "X:\\gta5\\art\\peds\\Story_Characters\\CS_Female_Proxy\\skinData\\femaleHardWeights.env"
				print "Using female Skin"
			)
		)
		else
		(
			if geo.name == "uppr_000_U" then --left hand
			(
				skinToUse = "X:\\gta5\\art\\peds\\Story_Characters\\CS_Male_Proxy\\skinData\\Uppr_000_U.env"
				print "using left hand skin"
			)
			else
			(
				skinToUse = "X:\\gta5\\art\\peds\\Story_Characters\\CS_Male_Proxy\\skinData\\Lowr_000_U.env"
				print "Using right hand skin"
			)
		)
		
		
				DialogMonitorOPS.RegisterNotification LSW_EnvelopeCallbackFunction ID:#ANoon_Envelopes
		DialogMonitorOPS.Enabled = true
		
		completeRedraw()
		skinOPs.loadEnvelope geo.modifiers[#Skin] skinToUse
		
		DialogMonitorOPS.Enabled = false
		DialogMonitorOPS.UnRegisterNotification ID:#ANoon_Envelopes
		
)

fn matchMarkers = 
(
	for obj in markersArray do
	(
		currMarker = (getnodeByName obj)
		if currMarker != undefined do
		(
			if currMarker.name == $Marker_Pelvis then
			(
				currSpotMarker = getNodeByName ("Spot_"+currMarker.name+"001")
			)
			else
			(
				currSpotMarker = getNodeByName ("Spot_"+currMarker.name)
			)
			if currSpotMarker != undefined do
			(
				currMarker.transform = currSpotMarker.transform
			)
			print ("Matched "+obj+" with "+"Spot_"+currMarker.name)
		)
	)
-- 	removeSpotMarkers()
-- 	
-- 	createCleanRig()
-- 	
-- 	setRigToXYZ()
-- 	
-- 	saveAnimPose()
-- 	
-- 	openProxyScene()
	
-- 	prepProxyMesh()	
)


fn removeSpotMarkers =
(
	clearSelection()
	for i in spotMarkersArray do
	(
		if i != undefined do
		(
			selectMore i
			print ("Preparing to delete "+i.name)
		)
	)
	
-- 	if selection.count != 0 do
-- 	(
	delete selection
-- 	)
	
	clearSelection() 
	for obj in objects do
	(
		if (substring obj.name 1 5) == "SKEL_" then
		(
			selectMore obj
			print ("Preparing to remove stretchy bone "+obj.name)
		)
		else
		(
			if (substring obj.name 1 3) == "RB_" then
			(
				selectMore obj
				print ("Preparing to remove stretchy bone "+obj.name)
			)
		)
	)

-- 	if selection.count != 0 do
-- 	(
-- 		delete selection
-- 	)
	
	delete Selection
)


fn createCleanRig stage =
(
	filein (theWildWest + "script/max/Rockstar_North/character/Rigging_tools/C_Build_Ped_Rig_from_Markers.ms")
	
	buildRigFM()
	
	if stage == 2 do --builds roll bones which we dont need to do first time round
	(
		buildSecondarySystem()
	)
	
	closeRolloutFloater theNewFloater

)


fn mergeProxyMesh = --this will 
(
	input_name = "c:\skins\animations\originalScene.MJR" 
	f = openfile input_name
	inputData = #() -- define as array
	while not eof f do
	(
		append inputData (filterstring (readLine f) "\n")
	)
	close f

	stringName = ( inputData[1] as string)
	currMaxSceneLength = stringName.count
	currMaxScene = (substring stringName 4 (currMaxSceneLength - 5)) --mega hacky workaround

-- 	messagebox ("TRYING TO LOAD: "+currMaxScene)
	print currMaxScene
		
	--need to check now if we're doing a giant run
	if runningAsGiantSpot == true then		
	(
		loadmaxfile "c:/skins/giantPrep.max" quiet:true
	)
	else
	(
		loadMaxFile currMaxScene quiet:true
	)
	
	if useSceneMesh != true then
	(
		mergeMaxFile "c:\skins\animations\deformedMesh.max" quiet:true
	)
	else
	(
		mergeMaxFile "c:\skins\giantSceneMesh.max" quiet:true
	)

	renameMarkers()
	matchMarkers()

	
	removeSpotMarkers() --removes stretchySkel
	createCleanRig 2 --this will re-create a cleanRig 
		
	clearSelection()
	for obj in objects do
	(
		if (substring obj.name 1 6 ) == "Marker" do
		(
			selectMore obj
		)
	)
	
	if selection.count != 0 do
	(
		delete selection
	)	

		prepSkinModifiers $head_000_r
	
	LHand = getNodeByName "Uppr_000_U"
	if LHand != undefined do
	(
		prepSkinModifiers LHand
	)
	RHand = getNodeByName "Lowr_000_U"
	if RHand != undefined do
	(
		prepSkinModifiers RHand
	)	
	
	if runningAsGiantSpot == undefined do
	(
		messageBox "Process Complete."
	)
)

fn loadAnimPose = 
(
	clearSelection()
	for i = 1 to skelArray.count do
	(
		currBone = getNodeByName skelArray[i]
		if currBone != undefined do
		(
			selectMore currBone
		)
	)
	
	print ("SKEL_R_Toe0 orig rot = "+($SKEL_R_Toe0.rotation as string))
	dirArray = getDirectories ("C:/skins/animations/")
	if dirArray.count != 1 do
	(
		makeDir "C:/skins/animations/"
	)
	selectedanim = "C:/skins/animations/SpotAnimPose.xaf"
	LoadSaveAnimation.loadAnimation selectedanim selection 
	
	input_name = "c:/skins/animations/rootTrans.MJR" 
	f = openfile input_name
	inputData = #() -- define as array
	while not eof f do
	(
		append inputData (filterstring (readLine f) "\n")
	)
	close f

	stringName = ( inputData[1] as string)
	currMaxSceneLength = stringName.count
	currMaxScene = (substring stringName 4 (currMaxSceneLength - 5)) --mega hacky workaround

	rootTrans = currMaxScene
		execStr = ("$Skel_Root.transform = "+rootTrans)
	execute execStr
	
	if runningAsGiantSpot != undefined do --this variable only gets passed in via the markerToGiantMatch tool
	(		
		in coordsys local $SKEL_R_Toe0.rotation.controller.X_Rotation = 180	
		in coordsys local $SKEL_L_Toe0.rotation.controller.X_Rotation = 180
	)
		
	print ("SKEL_R_Toe0 POST rot = "+($SKEL_R_Toe0.rotation as string))
		--clean arrays
		inputData = undefined
)


fn prepProxyMesh = --this will save out a collapsed mesh which has been pulled around by loading pose onto a soft skinned skin
(
	
	loadAnimPose()
	
	collapseStack $head_000_R 
	select $head_000_R
	modPanel.addModToSelection (XForm ()) ui:on
	collapseStack $head_000_R 
	
	if runningAsGiantSpot != undefined do
	(
		in coordsys local $SKEL_R_Toe0.rotation.controller.X_Rotation = -180	
		in coordsys local $SKEL_L_Toe0.rotation.controller.X_Rotation = -180
	)
		
	nodesToSave = #($head_000_R)
	--this will save out a collapsed mesh which has been pulled around by loading pose onto a soft skinned skin
	saveNodes nodesToSave "c:\skins\animations\deformedMesh.max" quiet:true
		
		
-- 	print ("currMaxScene = "+currMaxScene)
	mergeProxyMesh()
)

fn openProxyScene = 
(
	
	if ((chooseSexGUI  != undefined) and (chooseSexGUI .isDisplayed)) do
		(destroyDialog chooseSexGUI )

	rollout chooseSexGUI  "Rigging Tools"
	(
		button btnMale "Male" width:210 height:80
		button btnFemale "Female" width:210 height:80

		on btnMale pressed do
		(
			proxyFile = "X:\gta5\art\peds\Story_Characters\CS_Male_Proxy\CS_Male_Proxy_SoftSkin_NoFrozen.max"

			loadMaxFile proxyFile quiet:true

			output_name = "c:\\skins\\animations\\charSex.MJR" 
			output_file = createfile output_name		

			format (("M")+"\n") to:output_file
				
			close output_file			
	
			destroyDialog chooseSexGUI
			
			prepProxyMesh()
		)

		on btnFemale pressed do
		(
			proxyFile = "X:\gta5\art\peds\Story_Characters\CS_Female_Proxy\CS_Female_Proxy_SoftSkin_NoFrozen.max"
			
			loadMaxFile proxyFile quiet:true

			output_name = "c:\\skins\\animations\\charSex.MJR" 
			output_file = createfile output_name		

			format (("F")+"\n") to:output_file
				
			close output_file			
			
			destroyDialog chooseSexGUI
			prepProxyMesh()
		)
	)

	CreateDialog chooseSexGUI  width:300 height:200 pos:[600, 600] 
)



fn saveAnimPose = 
(
	clearSelection()
	for i = 1 to skelArray.count do
	(
		currBone = getNodeByName skelArray[i]
		if currBone != undefined do
		(
			selectMore currBone
		)
	)
	
	dirArray = getDirectories ("C:\skins\animations\"")
	if dirArray.count != 1 do
	(
		makeDir "C:\skins\animations\""
	)
	
	maxIsShit = #()
	maxIsReallyShit = #()
	selectedanim = "C:\skins\animations\SpotAnimPose.xaf"
	LoadSaveAnimation.saveAnimation selectedanim selection maxIsShit maxIsReallyShit
	
	rootTrans = $SKEL_ROOT.transform

	output_name = "c:\\skins\\animations\\rootTrans.MJR" 
	output_file = createfile output_name		
	-- 	theSceneName = (maxFilePath + maxFileName)

	format ((rootTrans as string)+"\n") to:output_file
		
	close output_file
)

fn setRigToXYZ = 
(
	
-- 	messagebox "setting to xyz"
	for b = 1 to skelArray.count do
	(
		obj = (getNodeByName skelArray[b])
		if obj != undefined then
		(
			obj.pos.controller = Position_XYZ ()
			obj.rotation.controller = Euler_XYZ ()
			max set key keys
		)
		else
		(
			print ("Couldnt find "+skelArray[b]+" so couldnt key.")
		)
		print ("Controllers unFrozen on "+obj.name+" // "+skelArray[b])
		
	)
)







renameMarkers()
matchMarkers()

	removeSpotMarkers()
	
	createCleanRig 1
	
	setRigToXYZ()
	
	saveAnimPose()

-- messageBox ("NEW sexcheckState = "+(sexCheckState as string))

if sexCheckState == undefined then
(
	openProxyScene()
)
else
(
	--if we;re using a raw mesh then we need to skip the section here for loading weights dont we?
-- 	if useSceneMesh != true then
-- 	(
		if sexCheckState == true then
		(
			proxyFile = "X:\gta5\art\peds\Story_Characters\CS_Male_Proxy\CS_Male_Proxy_SoftSkin_NoFrozen.max"

			loadMaxFile proxyFile quiet:true

			output_name = "c:\\skins\\animations\\charSex.MJR" 
			output_file = createfile output_name		

			format (("M")+"\n") to:output_file
				
			close output_file			

	-- 		destroyDialog chooseSexGUI
			
			prepProxyMesh()
		)
		else
		(
			proxyFile = "X:\gta5\art\peds\Story_Characters\CS_Female_Proxy\CS_Female_Proxy_SoftSkin_NoFrozen.max"
			
			loadMaxFile proxyFile quiet:true

			output_name = "c:\\skins\\animations\\charSex.MJR" 
			output_file = createfile output_name		

			format (("F")+"\n") to:output_file
				
			close output_file			
			
	-- 		destroyDialog chooseSexGUI
			prepProxyMesh()
		)
-- 	)
-- 	else
-- 	(
		--proxyFile = "c:\skins\animations\deformedMesh.max" 
-- 		proxyFile = "X:\gta5\art\peds\Story_Characters\CS_Male_Proxy\CS_Male_Proxy_SoftSkin_NoFrozen.max"
-- 		loadMaxFile proxyFile quiet:true

-- 		output_name = "c:\\skins\\animations\\charSex.MJR" 
-- 		output_file = createfile output_name		

-- 		format (("M")+"\n") to:output_file
-- 			
-- 		close output_file	
-- 			
-- 		prepProxyMesh()
-- 	)
)