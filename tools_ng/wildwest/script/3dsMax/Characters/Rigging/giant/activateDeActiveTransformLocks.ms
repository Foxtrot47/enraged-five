--script which activate joint limits on skeleton so they cannot accidentally be twisted along illegal axes

-----------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------

skelJoints = #(
	#($SKEL_ROOT, #{}),
	#($SKEL_Pelvis, #{}),
	#($SKEL_R_Thigh, #{}),
	#($SKEL_R_Calf, #{4,5}),
	#($SKEL_R_Foot, #{}),
	#($SKEL_R_Toe0, #{4,5}),
	#($SKEL_L_Thigh, #{}),
	#($SKEL_L_Calf, #{4,5}),
	#($SKEL_L_Foot, #{}),
	#($SKEL_L_Toe0, #{4,5}),
	#($SKEL_Spine_Root, #{}),
	#($SKEL_Spine0, #{}),
	#($SKEL_Spine1, #{}),
	#($SKEL_Spine2, #{}),
	#($SKEL_Spine3, #{}),
	#($SKEL_L_Clavicle, #{}),
	#($SKEL_L_UpperArm, #{}),
	#($SKEL_L_Forearm, #{4,5}),
	#($SKEL_L_Hand, #{}),
	#($SKEL_L_Finger00, #{}),
	#($SKEL_L_Finger01, #{}),
	#($SKEL_L_Finger02, #{}),
	#($SKEL_L_Finger10, #{4}),
	#($SKEL_L_Finger11, #{4,5}),
	#($SKEL_L_Finger12, #{4,5}),
	#($SKEL_L_Finger20, #{4}),
	#($SKEL_L_Finger21, #{4,5}),
	#($SKEL_L_Finger22, #{4,5}),
	#($SKEL_L_Finger30, #{4}),
	#($SKEL_L_Finger31, #{4,5}),
	#($SKEL_L_Finger32, #{4,5}),
	#($SKEL_L_Finger40, #{4}),
	#($SKEL_L_Finger41, #{4,5}),
	#($SKEL_L_Finger42, #{4,5}),
	#($SKEL_R_Clavicle, #{}),
	#($SKEL_R_UpperArm, #{}),
	#($SKEL_R_Forearm, #{4,5}),
	#($SKEL_R_Hand, #{}),
	#($SKEL_R_Finger00, #{}),
	#($SKEL_R_Finger01, #{}),
	#($SKEL_R_Finger02, #{}),
	#($SKEL_R_Finger10, #{4}),
	#($SKEL_R_Finger11, #{4,5}),
	#($SKEL_R_Finger12, #{4,5}),
	#($SKEL_R_Finger20, #{4}),
	#($SKEL_R_Finger21, #{4,5}),
	#($SKEL_R_Finger22, #{4,5}),
	#($SKEL_R_Finger30, #{4}),
	#($SKEL_R_Finger31, #{4,5}),
	#($SKEL_R_Finger32, #{4,5}),
	#($SKEL_R_Finger40, #{4}),
	#($SKEL_R_Finger41, #{4,5}),
	#($SKEL_R_Finger42, #{4,5}),
	#($SKEL_Neck_1, #{}),
	#($SKEL_Head, #{})
	)



-----------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------

fn activateTransformLocks =
(
	for i = 1 to skelJoints.count do
	(
		geo = skelJoints[i][1]
		
		if geo != undefined do
		(
			print ("geo = "+geo.name)
			setTransformLockFlags geo skelJoints[i][2]
		)
	)
)

fn deactivateTransformLocks = 
(
	for i = 1 to skelJoints.count do
	(
		geo = skelJoints[i][1]
		if geo != undefined do
		(
			print ("geo = "+geo.name)
			setTransformLockFlags geo #{}
		)
	)	
)

