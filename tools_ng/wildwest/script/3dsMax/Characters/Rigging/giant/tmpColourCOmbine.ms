-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Includes\\FN_Rigging.ms")	

global vertBitArray = #(
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#(),
	#()
	)

geoBoneColours = #(
#("geo_Pelvis", --[1 ] array of geo pieces
"geo_Spine0", --[2]
"geo_Spine1", --3
"geo_Spine2", --4
"geo_Spine3", --5
"geo_Head", --6
"geo_L_Thigh", --7
"geo_L_Calf", --8 
"geo_L_Toe0", --9
"geo_R_Thigh", --10
"geo_R_Calf", --11
"geo_R_Toe0", --12
"geo_L_Clavicle", --13
"geo_L_UpperArm", --14
"geo_L_Forearm", --15
"geo_L_Hand", --16
"geo_L_Finger00", --17
"geo_L_Finger01", --18
"geo_L_Finger02", --19
"geo_L_Finger10", --20
"geo_L_Finger11", --21
"geo_L_Finger12", --22
"geo_L_Finger20", --23
"geo_L_Finger21", --24
"geo_L_Finger22", --25
"geo_L_Finger30", --26
"geo_L_Finger31", --27
"geo_L_Finger32", --28
"geo_L_Finger40", --29
"geo_L_Finger41", --30
"geo_L_Finger42", --31
"geo_R_Clavicle", --32
"geo_R_UpperArm", --33
"geo_R_Forearm", --34
"geo_R_Hand", --35
"geo_R_Finger00", --36
"geo_R_Finger01", --37
"geo_R_Finger02", --38
"geo_R_Finger10", --39
"geo_R_Finger11", --40
"geo_R_Finger12", --41
"geo_R_Finger20", --42
"geo_R_Finger21", --43
"geo_R_Finger22", --44
"geo_R_Finger30", --45
"geo_R_Finger31", --46
"geo_R_Finger32", --47
"geo_R_Finger40", --48
"geo_R_Finger41", --49
"geo_R_Finger42"), --50
#( -- [2] COLOURS USED FOR SPECIFIC PIECES
(color 0 0 0), --1
(color 10 0 0), --2
(color 20 0 0), --3
(color 30 0 0), --4
(color 40 0 0), --5
(color 50 0 0 ), --6
(color 60 0 0 ), --7
(color 70 0 0 ), --8
(color 80 0 0 ), --9
(color 90 0 0 ), --10
(color 100 0 0 ), --11
(color 110 0 0 ), --12
(color 120 0 0 ), --13
(color 130 0 0 ), --14
(color 140 0 0 ), --15
(color 150 0 0 ), --16
(color 160 0 0 ), --17
(color 170 0 0 ), --18
(color 180 0 0 ), --19
(color 190 0 0 ), --20
(color 200 0 0 ), --21
(color 210 0 0 ), --22
(color 220 0 0 ), --23
(color 230 0 0 ), --24
(color 240 0 0 ), --25
(color 250 0 0 ), --26
(color 0 10 0 ), --27
(color 0 20 0 ), --28
(color 0 30 0 ), --29
(color 0 40 0 ), --30
(color 0 50 0 ), --31
(color 0 60 0 ), --32
(color 0 70 0 ), --33
(color 0 80 0 ), --34
(color 0 90 0 ), --35
(color 0 100 0 ), --36
(color 0 110 0 ), --37
(color 0 120 0 ), --38
(color 0 130 0 ), --39
(color 0 140 0 ), --40
(color 0 150 0 ), --41
(color 0 160 0 ), --42
(color 0 170 0 ), --43
(color 0 180 0 ), --44
(color 0 190 0 ), --45
(color 0 200 0 ), --46
(color 0 210 0 ), --47
(color 0 220 0 ), --48
(color 0 230 0 ), --49
(color 0 240 0 ) --50
),
-- #( --[3]  this element gets dynamically populated and will contain arrays of verts for this specific segment
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#(),
-- 	#()
-- ),
#( -- [4] array ofjoints
	"SKEL_Pelvis",
	"SKEL_Spine0",
	"SKEL_Spine1",
	"SKEL_Spine2",
	"SKEL_Spine3",
	"SKEL_Head",
	"SKEL_L_Thigh",
	"SKEL_L_Calf",
	"SKEL_L_Toe0",
	"SKEL_R_Thigh",
	"SKEL_R_Calf",
	"SKEL_R_Toe0",
	"SKEL_L_Clavicle",
	"SKEL_L_UpperArm",
	"SKEL_L_Forearm",
	"SKEL_L_Hand",
	"SKEL_L_Finger00",
	"SKEL_L_Finger01",
	"SKEL_L_Finger02",
	"SKEL_L_Finger10",
	"SKEL_L_Finger11",
	"SKEL_L_Finger12",
	"SKEL_L_Finger20",
	"SKEL_L_Finger21",
	"SKEL_L_Finger22",
	"SKEL_L_Finger30",
	"SKEL_L_Finger31",
	"SKEL_L_Finger32",
	"SKEL_L_Finger40",
	"SKEL_L_Finger41",
	"SKEL_L_Finger42",
	"SKEL_R_Clavicle",
	"SKEL_R_UpperArm",
	"SKEL_R_Forearm",
	"SKEL_R_Hand",
	"SKEL_R_Finger00",
	"SKEL_R_Finger01",
	"SKEL_R_Finger02",
	"SKEL_R_Finger10",
	"SKEL_R_Finger11",
	"SKEL_R_Finger12",
	"SKEL_R_Finger20",
	"SKEL_R_Finger21",
	"SKEL_R_Finger22",
	"SKEL_R_Finger30",
	"SKEL_R_Finger31",
	"SKEL_R_Finger32",
	"SKEL_R_Finger40",
	"SKEL_R_Finger41",
	"SKEL_R_Finger42"),
#(--[5] dynamix array of bone indeces inside the skin mod
	) 
)


fn colourAndCombine = 
(
	allGeoObjects = #()
	totalvertCount = 0
	for i = 1 to geoBoneColours[1].count do
	(
		obj = getNodeByName geoBoneColours[1][i]
		if obj != undefined do
		(
			select obj
			if obj.modifiers[1] != undefined do (deleteModifier obj obj.modifiers[1])--just a bit of housecleaning
			max modify mode
			nv = obj.numverts
-- 			totalVertCount  = (totalVertCount + nv)
			for v = 1 to nv do 
			(
				obj.EditablePoly.SetSelection #Vertex #{v}
				
				VertColour = geoBoneColours[2][i]
				polyop.setvertcolor obj 1 v VertColour
			)
-- 			print ("colours on "+obj.name+" set to "+(geoBoneColours[2][i] as string))
			appendIfUnique allGeoObjects obj
		)
	)
	print ("Precombine totalVertCount = "+totalVertCount as string)
	
	--now combine the pieces together
	masterObj = allGeoObjects [1]
	for i = allGeoObjects .count to 2 by -1 do
	(
-- 		print ("Attaching "+allGeoObjects [i].name+" to "+masterObj.name)
		masterObj.attach allGeoObjects [i] masterObj
		select masterObj
		modPanel.addModToSelection (Edit_Poly ()) ui:on

		collapseStack masterObj
	)
	
	print ("Postcombine vert count = "+(masterObj.numverts as string))
		
-- 	--now query the vert colour and use that to figure out which bone the vert should be skinned to
-- 	print "Starting colour lookup"
-- 	--*** SHOULD I LOOP THRU THE VERTS AND DO A ARRAY OF VERTS PER BONE TO CALL FROM SKIN?
	nv = masterobj.numverts
	print ("no = "+(nv as string))

	for v = 1 to nv do
-- 	for v = 1 to (nv / 4) do
	(
		max modify mode
		masterObj.EditablePoly.setSelection #Vertex #{v}
		vertCol = masterObj.EditablePoly.getVertexColor 1
				--print ("VertColour for vert "+(v as string)+" = "+(vertCol as string))
-- 		found = false
		for col = 1 to geoBoneColours[2].count do --while (found != true) do
		(
			found = false
			print "--------------------------------------------------------"
			vCol = (vertCol as string)
			arrayCol = (geoBoneColours[2][col] as string)
			print ("VertColour for vert "+(v as string)+" = "+(vCol))
			print ("Testing against "+arrayCol)			
				if vCol == arrayCol do
				(
					print ("appending vert "+(v as string)+" to vertBitArray["+(col as string)+"] because its color was "+vCol+".")
	-- 					appendifunique vertBitArray[col] v
					--currVal = vertBitArray[col] as bitarray
					vertBitArray[col] = vertBitArray[col] as bitarray
					appendIfUnique vertBitArray[col] v
-- 					vertBitArray[col] = currVal
-- 					currVal = undefined
					found = true --quick little hack to prevent looping once we've foudn a match
	-- 					appendifunique vertBitArray[col] v
				)
				if found == true then exit --ok break out of the loop we've done this vert ** does this need to be here or higher up?
		)


	)

	--now add a skin mod and all the bones
	select masterObj
	max modify mode
	modPanel.addModToSelection (Skin ()) ui:on
	masterObj.modifiers[#Skin].bone_Limit = 4
		
	--now we need to add the bones
	for i = 2 to vertBitArray.count do
	(
		updateInt = 0
		thisBone = getNodeByName geoBoneColours[3][i]
		if i != vertBitArray.count then --this allows us to only do a full redraw once all bones are added
		(
			updateInt = 0
		)
		else
		(
			updateInt = 1
		)
		skinOps.addBone masterObj.modifiers[#Skin] thisBone updateInt 
	)
	
-- 	--now we need to build an array of bone indexes to match with the bone names
	for i = 1 to (skinOps.getNumberBones masterObj.modifiers[#Skin]) do
	(
		for bn = 1 to vertBitArray.count do
		(
			boneName = skinOps.getBoneName masterObj.modifiers[#Skin] i 1
			if boneName == vertBitArray[bn] do
			(
				geoBoneColours[4][i] = i
			)
		)
	) 
-- 		
--  	--we can now use the indexes in the array just done to now skin the vert arrays to the right bone
	for i = 1 to geoBoneColours[4].count do
	(
		myVertArray = vertBitArray[i]
		for v = 1 to myVertArray.count do
		(
			myBoneInt = geoBoneColours[4][i]
			skinOps.SetVertexWeights masterObj.modifiers[#Skin] myVertArray[v] myBoneInt
		)
	)

	print "=============================================="
	print vertBitArray
	print "--------------------------------------------------------------"
	-- 			--skinOps.selectVertices masterObj.modifiers[#Skin] vertArray
	messagebox "DONE!" beep:true
)

startE = timeStamp()
clearListener()
colourAndCombine()
endE = timestamp()
format "Processing SEGMESHY took % mins\n" (((endE - startE) / 1000.0) / 60)