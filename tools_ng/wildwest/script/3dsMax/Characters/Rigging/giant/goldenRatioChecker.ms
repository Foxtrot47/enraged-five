clearListener()

arrayOfNodes1 = #(
	#($HAND_SKEL_R_Finger00, $HAND_SKEL_R_Finger01, $HAND_SKEL_R_Finger02),
	#($HAND_SKEL_R_Finger10, $HAND_SKEL_R_Finger11, $HAND_SKEL_R_Finger12),
	#($HAND_SKEL_R_Finger20, $HAND_SKEL_R_Finger21, $HAND_SKEL_R_Finger22),
	#($HAND_SKEL_R_Finger30, $HAND_SKEL_R_Finger31, $HAND_SKEL_R_Finger32),
	#($HAND_SKEL_R_Finger40, $HAND_SKEL_R_Finger41, $HAND_SKEL_R_Finger42)
	)
	
arrayOfNodes2 = #(
	#($Marker_R_Finger00, $Marker_R_Finger01, $Marker_R_Finger02),
	#($Marker_R_Finger10, $Marker_R_Finger11, $Marker_R_Finger12),
	#($Marker_R_Finger20, $Marker_R_Finger21, $Marker_R_Finger22),
	#($Marker_R_Finger30, $Marker_R_Finger31, $Marker_R_Finger32),
	#($Marker_R_Finger40, $Marker_R_Finger41, $Marker_R_Finger42)
	)	

gtaNodes = #(
	$HAND_ROOT_RHand, $HAND_SKEL_R_Finger20
	)
	
giantNodes = #(
	$Marker_L_Hand, $Marker_R_Finger20
	)
	
	
	
	
	
fn goldenratio arrayOfNodes1 arrayOfNodes2 = 
(
	goldenValue = 0.618	
	for c = 1 to arrayOfNodes1.count do
	(
		--the 1's are the gta fingers
			root1 = arrayOfNodes1[c][1]
			mid1 = arrayOfNodes1[c][2]
			end1 = arrayOfNodes1[c][3]
			firstFinger1 = distance root1 mid1
			secondFinger1 = distance mid1 end1
			diff1 = (secondFinger1 - (firstFinger1 * goldenValue)) --gives the distance over the golden ration so the biggest diff is the furthest from the ratio
		--the 2's are the giant fingers		
			root2 = arrayOfNodes2[c][1]
			mid2 = arrayOfNodes2[c][2]
			end2 = arrayOfNodes2[c][3]
			firstFinger2 = distance root2 mid2
			secondFinger2 = distance mid2 end2
			diff2 = (secondFinger2 - (firstFinger2 * goldenValue))		

				
			--now do a little hack to make the signs the same so we can compare which is the largest difference
			if diff1 < 0 do
			(
				diff1 = diff1 * -1
			)
			if diff2 < 0 do
			(
				diff2 = diff2 * -1
			)
				
			if diff1 < diff2 then
			(
				print "=================================================="
				print ("GTAV "+arrayOfNodes1[c][1].name+" is closer to the golden rule")
				print ("GTAV diff = "+"\t"+(diff1 as string))
				print ("Giant diff = "+"\t"+(diff2 as string))
			)
			else
			(
				print "=================================================="				
				print ("giant "+arrayOfNodes2[c][1].name+" is closer to the golden rule")
				print ("Giant diff = "+"\t"+(diff2 as string))
				print ("GTAV diff = "+"\t"+(diff1 as string))
			)
	)
	--now check hand values
	root1 = gtaNodes[1]
	knuckle1 = gtaNodes[2]
	fing1 = arrayOfNodes1[3][2]
	firstDist = distance root1 knuckle1
	secDist = distance knuckle1 fing1
	handDiff1 = (secDist - (firstDist * goldenValue))
	
	root2 = giantNodes[1]
	knuckle2 = giantNodes[2]
	fing2 = arrayOfNodes2[3][2]
	firstDist2 = distance root2 knuckle2
	secDist2 = distance knuckle2 fing2
	handDiff2 = (secDist2 - (firstDist2 * goldenValue))	
		
			if handDiff1 < 0 do
			(
				handDiff1 = handDiff1 * -1
			)
			if handDiff2 < 0 do
			(
				handDiff2 = handDiff2 * -1
			)
				
			if handDiff1 < handDiff2 then
			(
				print "=================================================="
				print ("GTAV hand is closer to the golden rule")
				print ("GTAV diff = "+"\t"+(handDiff1 as string))
				print ("Giant diff = "+"\t"+(handDiff2 as string))
			)
			else
			(
				print "=================================================="				
				print ("giant hand is closer to the golden rule")
				print ("Giant diff = "+"\t"+(handDiff2 as string))
				print ("GTAV diff = "+"\t"+(handDiff1 as string))
			)	
)

clearListener()
goldenratio arrayOfNodes1 arrayOfNodes2 