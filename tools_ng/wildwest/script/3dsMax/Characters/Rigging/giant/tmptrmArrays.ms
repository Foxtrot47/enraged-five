#(
#("splitVol_SKEL_L_Toe0", "splitVol_SKEL_L_Foot"),
#("splitVol_SKEL_L_Foot", "splitVol_SKEL_L_Calf"),
#("splitVol_SKEL_L_Calf", "splitVol_SKEL_L_Thigh"),
#("splitVol_SKEL_L_Thigh", "TRIMMERBOX"),

#("splitVol_SKEL_R_Toe0", "splitVol_SKEL_R_Foot"),
#("splitVol_SKEL_R_Foot", "splitVol_SKEL_R_Calf"),
#("splitVol_SKEL_R_Calf", "splitVol_SKEL_R_Thigh"),
#("splitVol_SKEL_R_Thigh", "TRIMMERBOX"),

#("splitVol_SKEL_Pelvis",  "splitVol_SKEL_R_Thigh", "splitVol_SKEL_L_Thigh"),

#("splitVol_SKEL_L_Finger02", "splitVol_SKEL_L_Finger01"),
#("splitVol_SKEL_L_Finger01", "splitVol_SKEL_L_Finger00"),
#("splitVol_SKEL_L_Finger00"),

#("splitVol_SKEL_L_Finger12", "splitVol_SKEL_L_Finger11"),
#("splitVol_SKEL_L_Finger11", "splitVol_SKEL_L_Finger10"),
#("splitVol_SKEL_L_Finger10"),

#("splitVol_SKEL_L_Finger22", "splitVol_SKEL_L_Finger21"),
#("splitVol_SKEL_L_Finger21", "splitVol_SKEL_L_Finger20"),
#("splitVol_SKEL_L_Finger20"),

#("splitVol_SKEL_L_Finger32", "splitVol_SKEL_L_Finger31"),
#("splitVol_SKEL_L_Finger31", "splitVol_SKEL_L_Finger30"),
#("splitVol_SKEL_L_Finger30"),

#("splitVol_SKEL_L_Finger42", "splitVol_SKEL_L_Finger41"),
#("splitVol_SKEL_L_Finger41", "splitVol_SKEL_L_Finger40"),
#("splitVol_SKEL_L_Finger40"),

#("splitVol_SKEL_R_Finger02", "splitVol_SKEL_R_Finger01"),
#("splitVol_SKEL_R_Finger01", "splitVol_SKEL_R_Finger00"),
#("splitVol_SKEL_R_Finger00"),

#("splitVol_SKEL_R_Finger12", "splitVol_SKEL_R_Finger11"),
#("splitVol_SKEL_R_Finger11", "splitVol_SKEL_R_Finger10"),
#("splitVol_SKEL_R_Finger10"),

#("splitVol_SKEL_R_Finger22", "splitVol_SKEL_R_Finger21"),
#("splitVol_SKEL_R_Finger21", "splitVol_SKEL_R_Finger20"),
#("splitVol_SKEL_R_Finger20"),

#("splitVol_SKEL_R_Finger32", "splitVol_SKEL_R_Finger31"),
#("splitVol_SKEL_R_Finger31", "splitVol_SKEL_R_Finger30"),
#("splitVol_SKEL_R_Finger30"),

#("splitVol_SKEL_R_Finger42", "splitVol_SKEL_R_Finger41"),
#("splitVol_SKEL_R_Finger41", "splitVol_SKEL_R_Finger40"),
#("splitVol_SKEL_R_Finger40"),


#("splitVol_SKEL_L_Hand", "splitVol_SKEL_L_Forearm"),
#("splitVol_SKEL_L_Forearm", "splitVol_SKEL_L_UpperArm"),
#("splitVol_SKEL_L_UpperArm", "splitVol_SKEL_L_Clavicle", "splitVol_SKEL_Spine3"),
#("splitVol_SKEL_L_Clavicle", "splitVol_SKEL_Spine3"),

#("splitVol_SKEL_R_Hand", "splitVol_SKEL_R_Forearm"),
#("splitVol_SKEL_R_Forearm", "splitVol_SKEL_R_UpperArm"),
#("splitVol_SKEL_R_UpperArm", "splitVol_SKEL_R_Clavicle", "splitVol_SKEL_Spine3"),
#("splitVol_SKEL_R_Clavicle", "splitVol_SKEL_Spine3"),

#("splitVol_SKEL_Head"),

#("splitVol_SKEL_Spine3", "splitVol_SKEL_Spine3"),
#("splitVol_SKEL_Spine2", "splitVol_SKEL_Spine1"),
#("splitVol_SKEL_Spine1", "splitVol_SKEL_Spine0"),
#("splitVol_SKEL_Spine0", "splitVol_SKEL_Pelvis")
)