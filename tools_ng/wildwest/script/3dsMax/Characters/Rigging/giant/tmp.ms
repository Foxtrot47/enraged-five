volObjects = #( --we do an intersection on first element and subtraction on others
	#("splitVol_SKEL_R_Toe0"), 
	#("splitVol_SKEL_R_Foot","splitVol_SKEL_R_Toe0", "splitVol_SKEL_R_Calf"), 
	#("splitVol_SKEL_R_Calf", "splitVol_SKEL_R_Foot", "splitVol_SKEL_R_Thigh"), 
	#("splitVol_SKEL_R_Thigh","splitVol_SKEL_R_Calf", "splitVol_SKEL_Pelvis" "trimVol_L_ThighBox"), 
	#("splitVol_SKEL_L_Toe0"), 
	#("splitVol_SKEL_L_Foot","splitVol_SKEL_L_Toe0", "splitVol_SKEL_L_Calf"), 
	#("splitVol_SKEL_L_Calf", "splitVol_SKEL_L_Foot", "splitVol_SKEL_L_Thigh"), 
	#("splitVol_SKEL_L_Thigh","splitVol_SKEL_L_Calf", "splitVol_SKEL_Pelvis" "trimVol_R_ThighBox"), 
	#("splitVol_SKEL_Pelvis", "splitVol_SKEL_L_Thigh", "splitVol_SKEL_R_Thigh", "splitVol_SKEL_Spine0"), 
	#("splitVol_SKEL_L_Finger02","splitVol_SKEL_L_Finger01"), 
	#("splitVol_SKEL_L_Finger01","splitVol_SKEL_L_Finger01","splitVol_SKEL_L_Finger00"), 
	#("splitVol_SKEL_L_Finger00","splitVol_SKEL_L_Finger01", "splitVol_SKEL_L_Hand"), 
	#("splitVol_SKEL_L_Finger12","splitVol_SKEL_L_Finger11"), 
	#("splitVol_SKEL_L_Finger11","splitVol_SKEL_L_Finger12","splitVol_SKEL_L_Finger10"), 
	#("splitVol_SKEL_L_Finger10","splitVol_SKEL_L_Finger11", "splitVol_SKEL_L_Hand"), 
	#("splitVol_SKEL_L_Finger22","splitVol_SKEL_L_Finger21"), 
	#("splitVol_SKEL_L_Finger21","splitVol_SKEL_L_Finger22","splitVol_SKEL_L_Finger20"), 
	#("splitVol_SKEL_L_Finger20","splitVol_SKEL_L_Finger21", "splitVol_SKEL_L_Hand"), 
	#("splitVol_SKEL_L_Finger32","splitVol_SKEL_L_Finger31"), 
	#("splitVol_SKEL_L_Finger31","splitVol_SKEL_L_Finger32","splitVol_SKEL_L_Finger30"), 
	#("splitVol_SKEL_L_Finger30","splitVol_SKEL_L_Finger31", "splitVol_SKEL_L_Hand"), 
	#("splitVol_SKEL_L_Finger42","splitVol_SKEL_L_Finger41"), 
	#("splitVol_SKEL_L_Finger41","splitVol_SKEL_L_Finger42","splitVol_SKEL_L_Finger40"), 
	#("splitVol_SKEL_L_Finger40","splitVol_SKEL_L_Finger41", "splitVol_SKEL_L_Hand"), 
	#("splitVol_SKEL_L_Hand","splitVol_SKEL_L_Forearm"), 
	#("splitVol_SKEL_L_Forearm","splitVol_SKEL_L_Hand","splitVol_SKEL_L_UpperArm"), 
	#("splitVol_SKEL_L_UpperArm","splitVol_SKEL_L_Forearm"), 
	#("splitVol_SKEL_L_Clavicle","splitVol_SKEL_L_UpperArm"), 
	#("splitVol_SKEL_R_Finger02","splitVol_SKEL_R_Finger01"), 
	#("splitVol_SKEL_R_Finger01","splitVol_SKEL_R_Finger01","splitVol_SKEL_R_Finger00"), 
	#("splitVol_SKEL_R_Finger00","splitVol_SKEL_R_Finger01", "splitVol_SKEL_R_Hand"), 
	#("splitVol_SKEL_R_Finger12","splitVol_SKEL_R_Finger11"), 
	#("splitVol_SKEL_R_Finger11","splitVol_SKEL_R_Finger12","splitVol_SKEL_R_Finger10"), 
	#("splitVol_SKEL_R_Finger10","splitVol_SKEL_R_Finger11", "splitVol_SKEL_R_Hand"), 
	#("splitVol_SKEL_R_Finger22","splitVol_SKEL_R_Finger21"), 
	#("splitVol_SKEL_R_Finger21","splitVol_SKEL_R_Finger22","splitVol_SKEL_R_Finger20"), 
	#("splitVol_SKEL_R_Finger20","splitVol_SKEL_R_Finger21", "splitVol_SKEL_R_Hand"), 
	#("splitVol_SKEL_R_Finger32","splitVol_SKEL_R_Finger31"), 
	#("splitVol_SKEL_R_Finger31","splitVol_SKEL_R_Finger32","splitVol_SKEL_R_Finger30"), 
	#("splitVol_SKEL_R_Finger30","splitVol_SKEL_R_Finger31", "splitVol_SKEL_R_Hand"), 
	#("splitVol_SKEL_R_Finger42","splitVol_SKEL_R_Finger41"), 
	#("splitVol_SKEL_R_Finger41","splitVol_SKEL_R_Finger42","splitVol_SKEL_R_Finger40"), 
	#("splitVol_SKEL_R_Finger40","splitVol_SKEL_R_Finger41", "splitVol_SKEL_R_Hand"), 
	#("splitVol_SKEL_R_Hand","splitVol_SKEL_R_Forearm"), 
	#("splitVol_SKEL_R_Forearm","splitVol_SKEL_R_Hand","splitVol_SKEL_R_UpperArm"), 
	#("splitVol_SKEL_R_UpperArm","splitVol_SKEL_R_Forearm"), 
	#("splitVol_SKEL_R_Clavicle","splitVol_SKEL_R_UpperArm"), 
	#("splitVol_SKEL_Head"), 
	#("splitVol_SKEL_Spine3","splitVol_SKEL_Spine2", "trimVol_R_ArmBox","trimVol_L_ArmBox"), 
	#("splitVol_SKEL_Spine2","splitVol_SKEL_Spine3", "splitVol_SKEL_Spine1"), 
	#("splitVol_SKEL_Spine1","splitVol_SKEL_Spine2", "splitVol_SKEL_Spine0"), 
	#("splitVol_SKEL_Spine0", "splitVol_SKEL_Spine1", "splitVol_SKEL_Pelvis")
	)
	
for ob = 1 to volObjects.count do
(
	for tr = 1 to volObjects[ob].count do
	(
		if volObjects[ob][tr] == "trimVol_L_ArmBox" then
		(
			nudger = -0.025
			
			trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:2 width:1 height:2 mapcoords:on --transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
			trimmer.name = volObjects[ob][tr]
			
			arm = $SKEL_R_UpperArm
			aPos = arm.position[1]
			bPos = trimmer.position[1]

			NPos = (trimmer.width /2)

			-- bocks.position.controller.x_position = ( aPos)
			neg = undefined
			if Apos < 0 do
			(
				Apos = apos * -1
				neg = true
			)
			if nPos < 0 do
			(
				nPos = nPos * -1
			)
			newP = (apos + nPos)

			if neg == true then
			(
				trimmer.position.controller.x_position  = ((newP * -1) + nudger)
			)
			else
			(
				trimmer.position.controller.x_position = (newP + nudger)
			)
		)
		else
		(
			if volObjects[ob][tr] == "trimVol_L_ArmBox" then
			(
				nudger = 0.025
				
				trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:2 width:1 height:2 mapcoords:on --transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
				trimmer.name = volObjects[ob][tr]
				
				arm = $SKEL_L_UpperArm
				aPos = arm.position[1]
				bPos = trimmer.position[1]

				NPos = (trimmer.width /2)

				-- bocks.position.controller.x_position = ( aPos)
				neg = undefined
				if Apos < 0 do
				(
					Apos = apos * -1
					neg = true
				)
				if nPos < 0 do
				(
					nPos = nPos * -1
				)
				newP = (apos + nPos)

				if neg == true then
				(
					trimmer.position.controller.x_position  = ((newP * -1) + nudger)
				)
				else
				(
					trimmer.position.controller.x_position = (newP + nudger)
				)
			)
			else
			(
			--ok so first get the first item to be used as a trimmer and dupliocate it
-- 				trimmer = getNodeByName ("MASTERTRIM_"+volObjects[ob][tr])
			)
		)
	)	
)