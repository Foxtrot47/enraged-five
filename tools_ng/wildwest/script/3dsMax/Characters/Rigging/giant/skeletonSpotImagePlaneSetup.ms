-- script to setup image planes for use when spotting skeletons
-- Jan 2011
-- Matt Rennie

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------





--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--we will open a gui asking user to pick front or side image
--then query image sizes and create a plane matching those sizes

--thsese paths can be removed once we have an image picker setup.
-- need to build an image picker that picks front and side images in a specific ordere
-- tmpImageNameFront = "X:\\gta5\\art\\animation\\resources\\mocap\\data\\NYC\\Shoot#1\\Character_and_Prop_spots\\NedLu_Michael_Front.jpg"
-- tmpImageNameSide = "X:\\gta5\\art\\animation\\resources\\mocap\\data\\NYC\\Shoot#1\\Character_and_Prop_spots\\NedLu_Michael_Side.jpg"

global frontPlane = undefined
global sideplane = undefined

fn createOffsetExpression imagePlane offsetAttr = 
(
	print "Starting to create expressions..."
	imagePlane.pos.controller.Y_Position.controller = Float_Expression ()
	newfcStr = imagePlane.pos.controller.Y_Position.controller--the path to the controller itself
	
	SVN = ("Offset_Front")
	-- 	("$"+joystick.name+".modifiers[#Attribute_Holder]."+bName+"."+attrNamePos+".controller") 
	driverStr = $imagePlaneRootNode.modifiers[#Attribute_Holder].Adjustments.Front_Offset.controller --path to controller of driver
	
	newfcStr.AddScalarTarget SVN driverStr --add scalar pointing to the translation of the joystick object
	newfcStr.SetExpression	(SVN+" * 10") --set the expression here
	
	-- now do the side one
	imagePlane = $Side_Plane
	
	imagePlane.pos.controller.X_Position.controller = Float_Expression ()
	newfcStr = imagePlane.pos.controller.X_Position.controller--the path to the controller itself
	
	SVN = ("Offset_Side")
	-- 	("$"+joystick.name+".modifiers[#Attribute_Holder]."+bName+"."+attrNamePos+".controller") 
	driverStr = $imagePlaneRootNode.modifiers[#Attribute_Holder].Adjustments.Side_Offset.controller --path to controller of driver
	
	newfcStr.AddScalarTarget SVN driverStr --add scalar pointing to the translation of the joystick object
	newfcStr.SetExpression	(SVN+" * 10") --set the expression here
	
)

fn createHeightExpression imagePlane = 
(
	print "Starting to create height expressions..."
	
	imagePlane.length.controller = bezier_float()
	imagePlane.pos.controller.Z_Position.controller = Float_Expression ()
	newfcStr = imagePlane.pos.controller.Z_Position.controller--the path to the controller itself
	
	SVN = ("Height")
	-- 	("$"+joystick.name+".modifiers[#Attribute_Holder]."+bName+"."+attrNamePos+".controller") 
	driverStr = imagePlane.Length.controller --path to controller of driver
	
	newfcStr.AddScalarTarget SVN driverStr --add scalar pointing to the translation of the joystick object
	newfcStr.SetExpression	(" 0 + ("+SVN+" / 2)") --set the expression here
	
)

fn addSizeAdjusters imagePlane = --this will add custom attributes to the imagePLaneRootNode to allow the user to slide them into and out of a view to help clipping
(
-- 	select $imagePlaneRootNode
	print "Starting to addSizeAdjusters..."
	
	currentJoystick = imagePlane
	rootNodeObj = $imagePlaneRootNode

	if rootNodeObj != undefined do 
	(
		if rootNodeObj .modifiers[#Attribute_Holder] == undefined do
		(
			select rootNodeObj 
			modPanel.addModToSelection (EmptyModifier ()) ui:on
		)

		facialMultipliersCA = (rootNodeObj.name +"CA")

		boneName = (imagePlane.name+"_Dims")


			uiNameFront = ("Height") --name as seen inside the code and track view	
			nameFront = "Height" --name as seen in the ui

			uiNameSide = ("Width") --name as seen inside the code and track view	
			nameSide = "Width" --name as seen in the ui

			posVal = "10000"
			negval = "-10000"		

-- 		if imagePlane.name != "HeightBox" then
-- 		(
			rolloutCreation = (facialMultipliersCA +" = attributes "+boneName +"\n" +"(" +"\n"+"\t" +"parameters main rollout:params"+"\n" +"\t"+"(" +"\n" +"\t" +"\t" +uiNameFront +" type:#float ui:" +uiNameFront +" default:" +"0.0" +"\n" +"\t" +"\t" +uiNameSide +" type:#float ui:" +uiNameSide +" default:" +"0.0" +"\n" +"\t"+")" +"\n" +"\t"  +"rollout params "+" \"" +boneName+" \"" +"\n" +"\t" +"(" +"\n" +"\t"+"\t"  +"spinner "+uiNameFront +" \"" +nameFront+"\"" +" "+"fieldwidth:60 range:[-10000,10000,1] type:#float" +"\n" +"\t"+"\t"  +"spinner "+uiNameSide+" \"" +nameSide+"\"" +" "+"fieldwidth:60 range:[-10000,10000,1] type:#float" +"\n" +"\t" +")" +"\n" +")"+"\n" +"\n" +"\t" +"custAttributes.add $" +rootNodeObj.name+".modifiers[#'Attribute holder'] "+facialMultipliersCA)
-- 		)
-- 		else
-- 		(
-- 			rolloutCreation = (facialMultipliersCA +" = attributes "+boneName +"\n"+"(" +"\n"+"\t" +"parameters main rollout:params"+"\n" +"\t"+"(" +"\n"+"\t" +"\t" +uiNameFront +" type:#float ui:" +uiNameFront +" default:" +"0.0" +"\n" +"\t"+")" +"\n" +"\t"  +"rollout params "+" \"" +boneName+" \"" +"\n" +"\t" +"(" +"\n" +"\t"+"\t"  +"spinner "+uiNameFront +" \"" +nameFront+"\"" +" "+"fieldwidth:60 range:[-10000,10000,1] type:#float" +"\n" +"\t" +")" +"\n" +")"+"\n" +"\n" +"\t" +"custAttributes.add $" +rootNodeObj.name+".modifiers[#'Attribute holder'] "+facialMultipliersCA)
-- 		)
		execute rolloutCreation		

		--now add bezier floats controllers to these attributes

		cont1 = ("$"+rootNodeObj.name+".modifiers[#Attribute_Holder]."+boneName+"."+uiNameFront+".controller = bezier_float ()")
			execute cont1

		cont2 = ("$"+rootNodeObj.name+".modifiers[#Attribute_Holder]."+boneName+"."+uiNameSide+".controller = bezier_float ()")
			execute cont2
			
	)
)

fn addPlaneAdjusters = --this will add custom attributes to the imagePLaneRootNode to allow the user to slide them into and out of a view to help clipping
(
-- 	select $imagePlaneRootNode
	print "Starting to addPlaneAdjusters..."
	
	currentJoystick = $imagePlaneRootNode

	if currentJoystick != undefined do 
	(
		if currentJoystick.modifiers[#Attribute_Holder] == undefined do
		(
			select currentJoystick
			modPanel.addModToSelection (EmptyModifier ()) ui:on
		)

		facialMultipliersCA = (currentJoystick.name +"CA")

		boneName = ("Adjustments")


			uiNameFront = ("Front_Offset") --name as seen inside the code and track view	
			nameFront = "Front_Offset" --name as seen in the ui

			uiNameSide = ("Side_Offset") --name as seen inside the code and track view	
			nameSide = "Side_Offset" --name as seen in the ui

			posVal = "1000"
			negval = "-1000"		
				
		rolloutCreation = (facialMultipliersCA +" = attributes "+boneName +"\n" +"(" +"\n" +"\t" +"parameters main rollout:params"+"\n" +"\t"+"(" +"\n" +"\t" +"\t" +uiNameFront +" type:#float ui:" +uiNameFront +" default:" +"0.0" +"\n" +"\t" +"\t" +uiNameSide +" type:#float ui:" +uiNameSide +" default:" +"0.0" +"\n" +"\t"+")" +"\n" +"\t"  +"rollout params "+" \"" +boneName+" \"" +"\n" +"\t" +"(" +"\n" +"\t"+"\t"  +"spinner "+uiNameFront +" \"" +nameFront+"\"" +" "+"fieldwidth:50 range:[-1000,1000,1] type:#float" +"\n" +"\t"+"\t"  +"spinner "+uiNameSide+" \"" +nameSide+"\"" +" "+"fieldwidth:50 range:[-1000,1000,1] type:#float" +"\n" +"\t" +")" +"\n" +")"+"\n" +"\n" +"\t" +"custAttributes.add $" +currentJoystick.name+".modifiers[#'Attribute holder'] "+facialMultipliersCA)
			execute rolloutCreation		

		--now add bezier floats controllers to these attributes

		$imagePlaneRootNode.modifiers[#Attribute_Holder].Adjustments.Front_Offset.controller = bezier_float()
		$imagePlaneRootNode.modifiers[#Attribute_Holder].Adjustments.Side_Offset.controller = bezier_float()
-- 		cont1 = ("currentJoystick.modifiers[#Attribute_Holder]."+boneName+"."+uiNameFront+".controller = bezier_float ()")
-- 			execute cont1
-- 		cont2 = ("currentJoystick.modifiers[#Attribute_Holder]."+boneName+"."+uiNameSide+".controller = bezier_float ()")
-- 			execute cont2
			
		--- ok now the attributes have been created we need to hook these attributes up to the position of the image planes
		print "Custom attrs added now hooking up..."
		createOffsetExpression $Front_Plane "Front"
	)
)

fn createImagePlane PlaneName ImageName = --pass in name of imagePlane (such as "Front_Plane") as string, and path to specific image
(

-- 	print ("using "+ImageName)
	
	imagePlane = Plane length:1 width:1 pos:[0,0,0] isSelected:on
	imagePlane.lengthsegs = 1
	imagePlane.widthsegs = 1
	
	if PlaneName == "Front_Plane" then
	(
		i = 1
		imagePlaneRotation = (quat 0.707107 0 0 0.707107)
		print "Doing Front"
		frontPlane = imagePlane
	)
	else
	(
		i = 2
		imagePlaneRotation = (quat -0.5 0.5 0.5 -0.5)
		print "Doing Side"
		sidePlane = imagePlane
	)	

	meditMaterials[i] = Standardmaterial ()	
	imagePlane.material = meditMaterials[i] --showInViewport:true
	meditMaterials[i].twoSided = true
	meditMaterials[i].showInViewport = true
	meditMaterials[i].selfIllumAmount = 100
	meditMaterials[i].name = (PlaneName+"_MAT")
	image = BitmapTexture()
	image.filename = ImageName
	meditMaterials[i].diffuseMap = image
	
	imagePlane.name = PlaneName
	
	--now we need to query the bitmap we just set and find its width and height and set the length and height on the image plane geo to match
	
-- 	imagePlane.width = (image.bitmap.width / 1)
-- 	imagePlane.length = (image.bitmap.height / 1)
	
	initialWidth = (image.bitmap.width as float)
	initialHeight = (image.bitmap.height as float)
	
	print ("Initial Width = "+initialWidth as string)
	print ("Initial Height = "+initialHeight as string)
	
	myWidth = ((image.bitmap.width as float) / 100)
	myheight = ((image.bitmap.height as float) / 100) 
	
	print ("myWidth = "+myWidth as string)
	print ("myHeight = "+myHeight as string)

	print ("myWidth / 10 = "+((myWidth /10 )as float) as string)
	print ("myHeight / 10 = "+((myHeight / 10) as float )as string)

	imagePlane.width = ((myWidth/ 10) as float)
	imagePlane.length = ((myHeight / 10) as float)
	
	in coordsys world imagePlane.rotation = imagePlaneRotation

	--rather than adjusting pivot we need to drive vert position by 0 + (height / 2)
	--this will then mean when done via an expression that the plane will stay on the ground plane
	createHeightExpression imagePlane
	
	addSizeAdjusters imagePlane
	
	imagePlane.parent = $imagePlaneRootNode
)

fn skelSpotPrep = 
(
	rootObj = getNodeByName "imagePlaneRootNode"
	if rootObj == undefined do
	(
		imagePlaneRootNode = Point pos:[0,0,0] isSelected:on
		imagePlaneRootNode.size = 0.5
		imagePlaneRootNode.Box = on
		imagePlaneRootNode.cross = off	
		imagePlaneRootNode.name = "imagePlaneRootNode"
	)
	
	rootObj = getNodeByName "imagePlaneRootNode"
	
	tmpImageNameFront = undefined
	tmpImageNameSide = undefined
	
	if tmpImageNameFront == undefined do
	(
		tmpImageNameFront = getOpenFileName caption:"Pick Front Image" types:"jPeg (*.jpg)|*.jpg|All Files (*.*)|*.*|"
	)
	
	if tmpImageNameSide == undefined do
	(
		tmpImageNameSide = getOpenFileName caption:"Pick Side Image" types:"jPeg (*.jpg)|*.jpg|All Files (*.*)|*.*|"
	)	
	
	if tmpImageNameFront != undefined then
	(
		if tmpImageNameSide != undefined then
		(
			createImagePLane "Front_Plane" tmpImageNameFront
			createImagePLane "Side_Plane" tmpImageNameSide
			
			--now create a box we can fit to at suggestion of rick
			
-- 			charBoxLength = (sidePlane.width / 2)
-- 			charBoxWidth = (frontPlane.width / 2)
-- 			charBoxHeight = frontPlane.length

			charBoxLength = 0.4
			charBoxWidth = 0.4
			charBoxHeight = 1.8
			
			charBox = Box lengthsegs:1 widthsegs:1 heightsegs:1 length:charBoxLength width:charBoxWidth height:charBoxHeight mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0,0,0]) isSelected:on
			charBox.name = "HeightBox"
			in coordsys world charBox.position = [0,0,0]
			in coordsys world charBox.rotation = (quat 0 0 0 1)
			
-- 			charBox.parent = $imagePlaneRootNode

			meditMaterials[3] = Standardmaterial ()	
			charBox.material = meditMaterials[3] --showInViewport:true
			meditMaterials[3].showInViewport = true
			meditMaterials[3].wire = on
			meditMaterials[3].name = (charBox.name+"_MAT")
			
			addSizeAdjusters charBox
				
			addPlaneAdjusters()
			
			paramWire.connect2way $Front_Plane.baseObject[#Length] $imagePlaneRootNode.modifiers[#Attribute_Holder].Front_Plane_Dims[#height] "height" "Length"
			paramWire.connect2way $Front_Plane.baseObject[#Width] $imagePlaneRootNode.modifiers[#Attribute_Holder].Front_Plane_Dims[#width] "width" "Width"
			
			paramWire.connect2way $Side_Plane.baseObject[#Length] $imagePlaneRootNode.modifiers[#Attribute_Holder].Side_Plane_Dims[#height] "height" "Length"
			paramWire.connect2way $Side_Plane.baseObject[#Width] $imagePlaneRootNode.modifiers[#Attribute_Holder].Side_Plane_Dims[#width] "width" "Width"			
			
			paramWire.connect2way $HeightBox.baseObject[#Height] $imagePlaneRootNode.modifiers[#Attribute_Holder].HeightBox_Dims[#height] "height" "Height"
			paramWire.connect2way $HeightBox.baseObject[#Width] $imagePlaneRootNode.modifiers[#Attribute_Holder].HeightBox_Dims[#width] "width" "Width"
		)
		else
		(
			messageBox ("WARNING! Please pick a side image.")
		)
	)
	else
	(
		messageBox ("WARNING! Please pick a front image.")
	)
)

skelSpotPrep()