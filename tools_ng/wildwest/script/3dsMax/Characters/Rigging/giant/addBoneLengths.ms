markerArray = #(
	"MARKER_CONTAINER", 
	"Marker_Pelvis001", 
	"Marker_R_Thigh", 
	"Marker_R_Calf", 
	"Marker_R_Foot", 
	"Marker_R_Toe0", 
	"Marker_R_Toe0Nub", 
	"Marker_L_Thigh", 
	"Marker_L_Calf", 
	"Marker_L_Foot", 
	"Marker_L_Toe0", 
	"Marker_L_Toe0Nub", 
	"Marker_Spine0", 
	"Marker_Spine1", 
	"Marker_Spine2", 
	"Marker_Spine3", 
	"Marker_Neck1", 
	"Marker_Head", 
	"Marker_HeadNub", 
	"Marker_R_Clavicle", 
	"Marker_R_UpperArm", 
	"Marker_R_Forearm", 
	"Marker_R_Hand", 
	"Marker_R_Finger00", 
	"Marker_R_Finger01", 
	"Marker_R_Finger02", 
	"Marker_R_Finger0Nub", 
	"Marker_R_Finger40", 
	"Marker_R_Finger41", 
	"Marker_R_Finger42", 
	"Marker_R_Finger4Nub", 
	"Marker_R_Finger30", 
	"Marker_R_Finger31", 
	"Marker_R_Finger32", 
	"Marker_R_Finger3Nub", 
	"Marker_R_Finger20", 
	"Marker_R_Finger21", 
	"Marker_R_Finger22", 
	"Marker_R_Finger2Nub", 
	"Marker_R_Finger10", 
	"Marker_R_Finger11", 
	"Marker_R_Finger12", 
	"Marker_R_Finger1Nub", 
	"Marker_R_HandEND", 
	"Marker_L_Clavicle", 
	"Marker_L_UpperArm", 
	"Marker_L_Forearm", 
	"Marker_L_Hand", 
	"Marker_L_Finger10", 
	"Marker_L_Finger11", 
	"Marker_L_Finger12", 
	"Marker_L_Finger1Nub", 
	"Marker_L_Finger20", 
	"Marker_L_Finger21", 
	"Marker_L_Finger22", 
	"Marker_L_Finger2Nub", 
	"Marker_L_Finger00", 
	"Marker_L_Finger01", 
	"Marker_L_Finger02", 
	"Marker_L_Finger0Nub", 
	"Marker_L_Finger30", 
	"Marker_L_Finger31", 
	"Marker_L_Finger32", 
	"Marker_L_Finger3Nub", 
	"Marker_L_Finger40", 
	"Marker_L_Finger41", 
	"Marker_L_Finger42", 
	"Marker_L_Finger4Nub", 
	"Marker_L_HandEND", 
	"Marker_PelvisEND"
	)
	
markerArrayParents = #(
"Null",
"Null",
"Null", 
"Marker_R_Thigh", 
"Marker_R_Calf", 
"Marker_R_Foot", 
"Marker_R_Toe0", 
"Null", 
"Marker_L_Thigh", 
"Marker_L_Calf", 
"Marker_L_Foot", 
"Marker_L_Toe0", 
"Null", 
"Marker_Spine0", 
"Marker_Spine1", 
"Marker_Spine2", 
"Marker_Spine3", 
"Marker_Neck1", 
"Marker_Head", 
"Marker_Spine3", 
"Marker_R_Clavicle", 
"Marker_R_UpperArm", 
"Marker_R_Forearm", 
"Marker_R_Hand", 
"Marker_R_Finger00", 
"Marker_R_Finger01", 
"Marker_R_Finger02", 
"Marker_R_Hand", 
"Marker_R_Finger40", 
"Marker_R_Finger41", 
"Marker_R_Finger42", 
"Marker_R_Hand", 
"Marker_R_Finger30", 
"Marker_R_Finger31", 
"Marker_R_Finger32", 
"Marker_R_Hand", 
"Marker_R_Finger20", 
"Marker_R_Finger21", 
"Marker_R_Finger22", 
"Marker_R_Hand", 
"Marker_R_Finger10", 
"Marker_R_Finger11", 
"Marker_R_Finger12", 
"Null", 
"Marker_Spine3", 
"Marker_L_Clavicle", 
"Marker_L_UpperArm", 
"Marker_L_Forearm", 
"Marker_L_Hand", 
"Marker_L_Finger10", 
"Marker_L_Finger11", 
"Marker_L_Finger12", 
"Marker_L_Hand", 
"Marker_L_Finger20", 
"Marker_L_Finger21", 
"Marker_L_Finger22", 
"Marker_L_Hand", 
"Marker_L_Finger00", 
"Marker_L_Finger01", 
"Marker_L_Finger02", 
"Marker_L_Hand", 
"Marker_L_Finger30", 
"Marker_L_Finger31", 
"Marker_L_Finger32", 
"Marker_L_Hand", 
"Marker_L_Finger40", 
"Marker_L_Finger41", 
"Marker_L_Finger42", 
"Null", 
"Null"
	
)

fn createLengthExpression currObj TapeObj currChild = 
(
-- 	print "Starting to create expressions..."

	childName = (substring currChild.name 8 200 )
	fcStr = ("$"+currObj.name+".modifiers[#Attribute_Holder]."+childName+".uiBoneLength.controller") --the path to the controller itself
		newfcStr = execute fcStr
	
	
	SVN = ("Length")

	drvStr = ("$"+TapeObj.name+".length.controller") --path to controller of driver
		driverStr = execute drvStr
	
	newfcStr.AddScalarTarget SVN driverStr --add scalar pointing to the translation of the joystick object
	newfcStr.SetExpression	(SVN) --set the expression here
)

fn addCustomAttributes obj currObj = --this will add custom attributes to the imagePLaneRootNode to allow the user to slide them into and out of a view to help clipping
(
-- 	select $imagePlaneRootNode
	
	currentJoystick = obj
	
	print ("Starting to add attributes to "+currentJoystick.name)
	
	select currentJoystick

	if currentJoystick != undefined do 
	(
		if currentJoystick.modifiers[#Attribute_Holder] == undefined do
		(
			select currentJoystick
			modPanel.addModToSelection (EmptyModifier ()) ui:on
		)

		facialMultipliersCA = (currentJoystick.name +"CA")

-- 		rollName = ("Adjustments")
		objNameStr = (substring currObj.name 8 200)
		rollName = (objNameStr)
				
			uiName = ("uiBoneLength") --name as seen inside the code and track view	
			displayName = ("BoneLength") --name as seen in the ui

			posVal = "1000"
			negval = "-1000"		
				
		rolloutCreation = (facialMultipliersCA +" = attributes "+rollName +"\n"+"(" +"\n"+"\t" +"parameters main rollout:params"+"\n"+"\t"+"(" +"\n" +"\t" +"\t" +uiName +" type:#float ui:" +uiName +" default:" +"0.0" +"\n" +"\t"+")" +"\n" +"\t"  +"rollout params "+" \"" +rollName+" \"" +"\n" +"\t" +"(" +"\n" +"\t"+"\t"  +"spinner "+uiName +" \"" +displayName+"\"" +" "+"fieldwidth:40 range:[-100,100,1] type:#float" +"\n" +"\t" +")" +"\n" +")"+"\n" +"\n" +"\t" +"custAttributes.add $" +currentJoystick.name+".modifiers[#'Attribute holder'] "+facialMultipliersCA)
			execute rolloutCreation		

		--now add bezier floats controllers to these attributes
-- 		cont1 = ("$"+currentJoystick.name+".modifiers[#Attribute_Holder]."+rollName+"."+uiName+".controller = bezier_float()")
		cont1 = ("$"+currentJoystick.name+".modifiers[#Attribute_Holder]."+rollName+"."+uiName+".controller = Float_Expression ()")
			execute cont1
			
		--- ok now the attributes have been created we need to hook these attributes up to the position of the image planes
		print ("Custom attrs added to "+currentjoystick.name)
	)
)

fn addBoneLengths = 
(
	for obj = 1 to markerArray.Count do	
	(
		if markerArrayParents[obj] != "Null" do
		(
			
			currPar = getNodeByName markerArrayParents[obj]
			currObj = getNodeByName markerArray[obj]
			
			pP = currPar.position
			cP = currObj.position
			
			thisTape = Tape length:100 pos:pP isSelected:on target:(Targetobject transform:(matrix3 [1,0,0] [0,1,0] [0,0,1] cP))
				thisTape.name = (currPar.name+"_Tape")
				thisTarget = thisTape.target
				thisTarget.name = (currPar.name+"_TapeTarget")
				thisTape.parent = currPar
				thisTarget.parent = currObj
				
			thisTape.length.controller = bezier_float ()
				
			addCustomAttributes currPar currObj
				
			--now we need to get the length of this to drive the attribute we added.
			createLengthExpression currPar thisTape currObj
		)
		
	)
)

addBoneLengths()