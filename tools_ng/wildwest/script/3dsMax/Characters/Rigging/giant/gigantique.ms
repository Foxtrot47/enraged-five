-- giantFloater.ms
--Matt Rennie
--Nov 2010

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()


		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/giant/activateDeActiveTransformLocks.ms")	

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if ((giantFloaterGUI != undefined) and (giantFloaterGUI.isDisplayed)) do
	(destroyDialog giantFloaterGUI)

rollout giantFloaterGUI "Gigantique"
(
	button btnAlignMesh "Align Mesh Vertically" width:160 height:40
	button btnRunAll "Giant Prep" width:160 height:40
-- 	button btnRunMarkerToGiant "Marker -> Giant" width:110
-- 	button btnBoolMesh "Segment" width:110
-- 	button btnCombineGeo "Combine & Skin" width:110

	button btnGenSkel "Generate Skeleton" width:160 height:40
	
	button btnActivateLocks "Enable Transform Locks" width:160 height:40
	button btnDeActivateLocks "Disable Transform Locks" width:160 height:40
	
	on btnAlignMesh pressed do
	(
		gotRoot = getNodeByName "root"
		gotmesh = getNodeByName "root_mesh"
		
		if gotRoot != undefined then
		(
			if gotMesh != undefined then 
			(
				bb = nodeGetBoundingBox $root_mesh $root.transform

				rp1 = $root.position[1]
				rp2 = $root.position[2]
				rp3 = ((bb[1][2] / 100) * -1)
				$root.position = [rp1,rp2,rp3]
			)
			else
			(
				messagebox ("Warning! Couldnt find 'root_mesh' geometry.") beep:true
			)
		)
		else
		(
			messagebox ("Warning! Couldnt find 'root' bone.") beep:true
		)
	)
	
	on btnRunAll pressed do
	(
		selA = selection as array
		if selA.count != 1 then
		(
			messagebox "Please pick your raw mesh and try again" beep:true
		)
		else
		(
			currObj = selection
			if classof $ == Editable_Poly then
			(
				if $.numFaces <= 14000 then
				(
				currObj.name = "RAWMESH"

				startE = timeStamp()
				clearListener()
				
				--first we need to merge in the stretchy skel shit
				mergeMaxFile (theProjectRoot+"art/peds/Skeletons/giantConstrainedSkel_Male.max") #deleteOldDups #useSceneMtlDups 
				mergeMaxFile (theProjectRoot+"art/peds/Story_Characters/CS_Male_Proxy/CS_Male_Hands.max") #deleteOldDups #useSceneMtlDups 
				
				filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/Rigging_tools/giant/markerToGiantMatch.ms")	
				filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/Rigging_tools/giant/boolMeshes.ms")	
				
				endE = timestamp()
				format "Processing took % mins\n" (((endE - startE) / 1000.0) / 60)
				messagebox "Processing complete. BOOYAH!" beep:true
				)
				else
				(
					messageBox ("WARNING! "+$.name+" has "+($.numfaces as string)+" faces. Should be less than 14000.") beep:true
				)
			)
			else
			(
				messagebox "Selection is not an Editable Poly." beep:true
			)
		)
	)
	
	on btnGenSkel pressed do
	(
			startE = timeStamp()
				clearListener()
			--first we need to merge in the stretchy skel shit
				mergeMaxFile (theProjectRoot+"art/peds/Skeletons/giantConstrainedSkel_Male.max") #deleteOldDups #useSceneMtlDups 
				mergeMaxFile (theProjectRoot+"art/peds/Story_Characters/CS_Male_Proxy/CS_Male_Hands.max") #deleteOldDups #useSceneMtlDups 
			
			filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/giant/markerToGiantMatch.ms")	

		--now we need to lock transform flags so that buildpose bindpose process doesnt introduce twists
-- 		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/giant/activateDeActiveTransformLocks.ms")	
		activateTransformLocks()
		
		--now sort out finger alignments
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/giant/fingerAlign.ms")	
		
	)	
	
	on btnActivateLocks pressed do
	(
-- 		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/giant/activateDeActiveTransformLocks.ms")	
		activateTransformLocks()		
	)

	on btnDeActivateLocks pressed do
	(
-- 		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/giant/activateDeActiveTransformLocks.ms")	
		deactivateTransformLocks()		
	)
)

CreateDialog giantFloaterGUI width:175 pos:[1400, 100] 