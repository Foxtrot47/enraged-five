--take count of verts in initial piece then put that in a variable.
--attach second piece then get total vert count and subtract the initial count from that thereby getting all the new vert numbers.


geoPieces = #(
"geo_Pelvis",
"geo_R_Toe0",
"geo_R_Foot",
"geo_R_Calf",
"geo_R_Thigh",
"geo_L_Toe0",
"geo_L_Foot",
"geo_L_Calf",
"geo_L_Thigh",
"geo_L_Finger02",
"geo_L_Finger01",
"geo_L_Finger00",
"geo_L_Finger12",
"geo_L_Finger11",
"geo_L_Finger10",
"geo_L_Finger22",
"geo_L_Finger21",
"geo_L_Finger20",
"geo_L_Finger32",
"geo_L_Finger31",
"geo_L_Finger30",
"geo_L_Finger42",
"geo_L_Finger41",
"geo_L_Finger40",
"geo_L_Hand",
"geo_L_Forearm",
"geo_L_UpperArm",
"geo_L_Clavicle",
"geo_R_Finger02",
"geo_R_Finger01",
"geo_R_Finger00",
"geo_R_Finger12",
"geo_R_Finger11",
"geo_R_Finger10",
"geo_R_Finger22",
"geo_R_Finger21",
"geo_R_Finger20",
"geo_R_Finger32",
"geo_R_Finger31",
"geo_R_Finger30",
"geo_R_Finger42",
"geo_R_Finger41",
"geo_R_Finger40",
"geo_R_Hand",
"geo_R_Forearm",
"geo_R_UpperArm",
"geo_R_Clavicle",
"geo_Head",
"geo_Neck_1",
"geo_Spine3",
"geo_Spine2",
"geo_Spine1",
"geo_Spine0"
)

SKELJoints = #()
	
SkinModifierIndexes = #() --this gets dynamically populated and will be indexes of the bones in the order of the SKELJoints array (used during the skinning pass)
	
vertArrays = #()

masterObj = undefined
	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn LSW_EnvelopeCallbackFunction =
(
	WindowHandle = DialogMonitorOPS.GetWindowHandle()
	theDialogName = UIAccessor.GetWindowText WindowHandle

	if theDialogName != undefined and matchpattern theDialogName pattern:"*Load Envelopes*" do
		UIAccessor.PressButtonByName WindowHandle "Match by Name"	
		
	if theDialogName != undefined and matchpattern theDialogName pattern:"*Load Envelopes*" do
		UIAccessor.PressButtonByName WindowHandle "OK"

	true
)
	
fn skinCombinedMesh masterObj = 
(

		--now add a skin mod and all the bones
	select masterObj
	max modify mode
	modPanel.addModToSelection (Skin ()) ui:on
	masterObj.modifiers[#Skin].bone_Limit = 4
		
	--now we need to add the bones
	for i = 1 to SKELJoints.count do
	(
		updateInt = 0
		thisBone = getNodeByName skelJoints[i]
		if i != SKELJoints.count then --this allows us to only do a full redraw once all bones are added
		(
			updateInt = 0
		)
		else
		(
			updateInt = 1
		)
		skinOps.addBone masterObj.modifiers[#Skin] thisBone updateInt 
	)
	
	--now we need to build an array of bone indexes to match with the bone names
	for i = 1 to (skinOps.getNumberBones masterObj.modifiers[#Skin]) do
	(
		for bn = 1 to skelJoints.count do
		(
			boneName = skinOps.getBoneName masterObj.modifiers[#Skin] i 1
			if boneName == skelJoints[bn] do
			(
				SkinModifierIndexes[i] = i
			)
		)
	) 
	
	format ">Before>%\n" (skinops.getNumberVertices masterObj.modifiers[#Skin])
	completeRedraw() --this is to force the skin modifier which is retarded to update.
	format ">After>%\n" (skinops.getNumberVertices masterObj.modifiers[#Skin])
	
	--now we need to use the vert array to pick the correct vert and convert to an element then skin all of those verts
	for i = 1 to vertArrays.count do
	(
		myVertArray = vertArrays[i] --need to ensure that the bone index array we use matches this vert index array
		for v = 1 to myVertArray.count do
		(
			myBoneInt = SkinModifierIndexes[i]
			skinOps.SetVertexWeights masterObj.modifiers[#Skin] myVertArray[v] myBoneInt 1.0
		)
	)
	
	rootBonez = #(
		"SKEL_ROOT",
		"SKEL_Spine_Root"
		)
		
	for tb = 1 to rootBonez.count do
	(
		btoadd = getnodebyname rootbonez[tb]
		if btoadd != undefined do
		(
			skinOps.addBone masterObj.modifiers[#Skin] btoadd 1
			btoadd = undefined
		)
	)
	
	if shouldIskinHands == true do --ok we need to skin the hands now
	(
		handObjs = #(
			"Uppr_000_U", --left
			"Lowr_000_U" --right
			)
			
		for i = 1 to handObjs.count do
		(
			handGeo = getNodeByName handObjs[i]
			
			--now add a skin mod and all the bones
			select handGeo
			max modify mode
			modPanel.addModToSelection (Skin ()) ui:on
			handGeo.modifiers[#Skin].bone_Limit = 4
				
			--now we need to add the bones
			for i = 1 to SKELJoints.count do
			(
				updateInt = 0
				thisBone = getNodeByName skelJoints[i]
				if i != SKELJoints.count then --this allows us to only do a full redraw once all bones are added
				(
					updateInt = 0
				)
				else
				(
					updateInt = 1
				)
				skinOps.addBone handGeo.modifiers[#Skin] thisBone updateInt 
			)
		
			completeRedraw() --this is to force the skin modifier which is retarded to update.
			
				for tb = 1 to rootBonez.count do
				(
					btoadd = getnodebyname rootbonez[tb]
					if btoadd != undefined do
					(
						skinOps.addBone handGeo.modifiers[#Skin] btoadd 1
						btoadd = undefined
					)
				)
			
			--now we need to test which hand it is and then load the appropriate skin envelope
			skinFile = (theProjectRoot +"art/peds/Skeletons/giantData/"+handObjs[i]+".env")
			
			if skinFile == undefined then 
			(
				messageBox ("Couldn't find "+skinFile)
			)
			else
			(
				-- Load the skin weights back on.
				-- Need to use completeRedraw() or the weights wont load when in a loop. Madness.
				-- Fix was from here: http://tech-artists.org/forum/showthread.php?p=4034
				-- Use a callback to press the buttons
				-- I found this here: http://forums.cgsociety.org/showthread.php?t=485300
				DialogMonitorOPS.RegisterNotification LSW_EnvelopeCallbackFunction ID:#ANoon_Envelopes
				DialogMonitorOPS.Enabled = true
				
				completeRedraw()
				skinOps.loadEnvelope handGeo.modifiers[#Skin] skinFile
				
				DialogMonitorOPS.Enabled = false
				DialogMonitorOPS.UnRegisterNotification ID:#ANoon_Envelopes
			)
		)
	)
)	

fn combineGeo = 
(
	--now build an array of the joints off the geo names
	for obj = 1 to geoPieces.count do
	(
		jointName = "SKEL"+(substring geoPieces[obj] 4 50)
		append SKELJoints jointName
	)
	
	clearListener()
	masterObj = getNodeByName geoPieces[1]
	if masterObj == undefined do (Messagebox "couldnt find geoPieces[1] which was "+geoPieces[1])
	newVertCount = masterObj.numverts
	
	--first we need to add all the verts in initial vert count to first vertarray
	vertArrays[1] = #()
	
	for i = 1 to newVertCount do
	(
		appendIfUnique vertArrays[1] i
	)
	
		
	for i = 2 to geoPieces.count do
	(
		currObj = getNodeByName geoPieces[i]
		--now attach currObj to masterObj
		masterObj.attach currObj masterObj
	
		--so we need to track the original vertex count
		startVerts = newVertCount
			print ("startVerts = "+(startVerts as string))
		--now get total vert count of object and subtract initialVertCount from it to get the number of first vert from newly added element
		newVertCount = masterObj.numverts
			print ("newVertCoutn = "+(newVertCount as string))
		vertArrays[i] = #()		
		for v = (startVerts + 1) to newVertCount do
		(
			appendIfUnique vertArrays[i] v
		)
		print "============="
	)

	print vertArrays

	--now by runnign the following line we should be able to pick the first vert of that element
-- 	masterObj.EditablePoly.SetSelection #Vertex #{vertArrays[2][1]}
	skinCombinedMesh masterObj
	

)

combineGeo()