filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()


fn runMarkerToGiant = 
(
	filein (theWildWest+ "script/max/Rockstar_North/character/Rigging_tools/giant/markerToGiantMatch.ms")	
)

fn mergeStretchy = 
(
	mergeMaxFile (theProjectRoot+"/art/peds/Skeletons/giantConstrainedSkel_Male.max")
		
	mergeMaxFile (theProjectRoot+"/art/peds/Story_Characters/CS_Male_Proxy/CS_Male_Hands.max")
	
	runMarkerToGiant()
)

mergeStretchy ()