-- Load the common maxscript functions 
--include "rockstar/export/settings.ms" -- This is SLOW! 
filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()


-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_bone_tagger.ms")
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_Rigging.ms")
filein (theProjectRoot + "tools/dcc/current/max2011/scripts/pipeline/util/xml.ms")

global shouldIskinHands = false
global vIndex = undefined

splitVolObjArray = #( --we do an intersection on first element and subtraction on others
	#("splitVol_SKEL_R_Toe0"), 
	#("splitVol_SKEL_R_Foot","splitVol_SKEL_R_Toe0", "splitVol_SKEL_R_Calf"), 
	#("splitVol_SKEL_R_Calf", "splitVol_SKEL_R_Thigh"), 
	#("splitVol_SKEL_R_Thigh","splitVol_SKEL_R_Calf", "splitVol_SKEL_Pelvis", "trimVol_L_ThighBox"), 
	#("splitVol_SKEL_L_Toe0"), 
	#("splitVol_SKEL_L_Foot","splitVol_SKEL_L_Toe0", "splitVol_SKEL_L_Calf"), 
	#("splitVol_SKEL_L_Calf", "splitVol_SKEL_L_Thigh"), 
	#("splitVol_SKEL_L_Thigh","splitVol_SKEL_L_Calf", "splitVol_SKEL_Pelvis", "trimVol_R_ThighBox"), 
	#("splitVol_SKEL_Pelvis", "splitVol_SKEL_Spine0", "trimVol_Pelvis_Bot"), 
	#("splitVol_SKEL_L_Finger02"),
	#("splitVol_SKEL_L_Finger01","splitVol_SKEL_L_Finger02"),
	#("splitVol_SKEL_L_Finger00","splitVol_SKEL_L_Finger01"),
	#("splitVol_SKEL_L_Finger12"),
	#("splitVol_SKEL_L_Finger11","splitVol_SKEL_L_Finger12"),
	#("splitVol_SKEL_L_Finger10","splitVol_SKEL_L_Finger11"),
	#("splitVol_SKEL_L_Finger22"),
	#("splitVol_SKEL_L_Finger21","splitVol_SKEL_L_Finger22"),
	#("splitVol_SKEL_L_Finger20","splitVol_SKEL_L_Finger21"),
	#("splitVol_SKEL_L_Finger32"),
	#("splitVol_SKEL_L_Finger31","splitVol_SKEL_L_Finger32"),
	#("splitVol_SKEL_L_Finger30","splitVol_SKEL_L_Finger31"),
	#("splitVol_SKEL_L_Finger42"),
	#("splitVol_SKEL_L_Finger41","splitVol_SKEL_L_Finger42"),
	#("splitVol_SKEL_L_Finger40","splitVol_SKEL_L_Finger41"),
	#("splitVol_SKEL_L_Hand"),--"splitVol_SKEL_L_Forearm"), 
	#("splitVol_SKEL_L_Forearm","splitVol_SKEL_L_Hand","splitVol_SKEL_L_UpperArm"), 
	#("splitVol_SKEL_L_UpperArm","splitVol_SKEL_L_Forearm", "splitVol_SKEL_L_Clavicle", "trimBox_L_Shoulder"), 
	#("splitVol_SKEL_L_Clavicle","splitVol_SKEL_L_UpperArm", "trimBox_Clavs"), 
	#("splitVol_SKEL_R_Finger02"),
	#("splitVol_SKEL_R_Finger01","splitVol_SKEL_R_Finger02"),
	#("splitVol_SKEL_R_Finger00","splitVol_SKEL_R_Finger01"),
	#("splitVol_SKEL_R_Finger12"),
	#("splitVol_SKEL_R_Finger11","splitVol_SKEL_R_Finger12"),
	#("splitVol_SKEL_R_Finger10","splitVol_SKEL_R_Finger11"),
	#("splitVol_SKEL_R_Finger22"),
	#("splitVol_SKEL_R_Finger21","splitVol_SKEL_R_Finger22"),
	#("splitVol_SKEL_R_Finger20","splitVol_SKEL_R_Finger21"),
	#("splitVol_SKEL_R_Finger32"),
	#("splitVol_SKEL_R_Finger31","splitVol_SKEL_R_Finger32"),
	#("splitVol_SKEL_R_Finger30","splitVol_SKEL_R_Finger31"),
	#("splitVol_SKEL_R_Finger42"),
	#("splitVol_SKEL_R_Finger41","splitVol_SKEL_R_Finger42"),
	#("splitVol_SKEL_R_Finger40","splitVol_SKEL_R_Finger41"),
	#("splitVol_SKEL_R_Hand"),
	#("splitVol_SKEL_R_Forearm","splitVol_SKEL_R_Hand","splitVol_SKEL_R_UpperArm"), 
	#("splitVol_SKEL_R_UpperArm","splitVol_SKEL_R_Forearm", "splitVol_SKEL_R_Clavicle", "trimBox_R_Shoulder"), 
	#("splitVol_SKEL_R_Clavicle","splitVol_SKEL_R_UpperArm", "trimBox_Clavs"), 
	#("splitVol_SKEL_Head"), 
	#("splitVol_SKEL_Neck_1"), 
	#("splitVol_SKEL_Spine3","trimVol_R_ArmBox","trimVol_L_ArmBox", "trimBox_L_Shoulder_ForSpine3", "trimBox_R_Shoulder_ForSpine3"), 
	#("splitVol_SKEL_Spine2","splitVol_SKEL_Spine3"), 
	#("splitVol_SKEL_Spine1","splitVol_SKEL_Spine2", "splitVol_SKEL_Spine0"), 
	#("splitVol_SKEL_Spine0", "splitVol_SKEL_Spine1", "splitVol_SKEL_Pelvis")
)

-- splitVolObjArray = #( --we do an intersection on first element and subtraction on others
-- 	#("splitVol_SKEL_R_Foot","splitVol_SKEL_R_Calf")
-- )
	
	rigBoneListStruct = #()

struct rigBoneStruct (rb_bone, rb_Radius, rb_Length, rb_Trans)

-- global originalMasterMesh = undefined

fn cleanVols = --cleans out all the volume shit
(
	obsToDel = #()
	
	for obj in objects do
	(
		if ((substring obj.name 1 9) == "splitVol_") or ((substring obj.name 1 7) == "trimBox") or ((substring obj.name 1 7) == "trimVol") do
		(
			appendIfUnique obsToDel obj
		)
	)
	
	delete obsToDel

)

fn removeIsolatedVerts myMesh = 
(
	select myMesh
-- 	modPanel.addModToSelection (Edit_Mesh ()) ui:on
				addmodifier myMesh (Edit_Mesh ())
	collapsestack myMesh
	isoVertArray = meshop.getIsoverts myMesh
	meshop.DeleteVerts myMesh isoVertArray
)

fn boolMesh masterObj volArr vIndex =
(
	--volObj passedin as an array of volumes. First one used as a intersection boolean and the rest as subtractions
	--first off duplicate the masterObject
	volO = getNodeByName volArr[1]
	newName = ("geo_"+(substring volO.Name 15 30))
	maxOps.cloneNodes masterObj cloneType:#copy actualNodeList:&c newNodes:&nnl
	obToBool = nnl[1]
	obToBool.name = newName
	
-- 	--disable the command panel for speed purposes
-- 	cui.commandPanelOpen = false

	cleanVolObj = undefined

		
	for vo = 1 to volArr.count do --we need to loop thru the array of vols and cut the obToBool with them
	(
		--now duplicate the volObject as this will disappear uring booleaning	
-- 		newName = ("CUTTER_"+(substring volArr[vo] 15 30))
		newName = ("CUTTER_"+volArr[vo])
		newVolO = getNodeByName volArr[vo]
			if newVolO == undefined do
			(
				print ("FUUUUCK. Couldn't find "+volArr[vo])
			)
		maxOps.cloneNodes newVolO cloneType:#copy actualNodeList:&c newNodes:&nnl
		cutterObj = nnl[1]
		cutterObj.name = newName		

			-- BEFORE WE DO THE INITIAL TRIM HERE, DUPLICATE THE CUTTERoBJ AND ADD A TINY PUSH TO IT
			--WE WILL AFTER THE BOOLEAN USE THIS AS A VL SELECT AND DELETE ANY NONE SELECTED VERTS
			--THIS IS TO TRY AND RESOLVE THE ODD DIRTY BITS WHICH GET LEFT
-- 				currS = selection as array
-- 				currO = getNodeByName currS[1].name
			
-- 				cleanVolName = ("geo_"+(substring volO.Name 15 30))
-- 				maxOps.cloneNodes cutterObj cloneType:#copy actualNodeList:&c newNodes:&nnl
-- 				cleanVolObj = nnl[1]
-- 				cleanVolObj.name = cleanVolName
-- 				select cleanVolObj
-- 				modPanel.addModToSelection (Push ()) ui:on
-- 				cleanVolObj.modifiers[#Push].Push_Value = 0.02
-- 				collapseStack cleanVolObj
			
-- 			select currO
			
			
		if vo == 1 then --do intersection boolean
		(			
			print ("Trimming "+obToBool.name+" with "+cutterObj.name)
					
				cleanVolName = ("geo_"+(substring volO.Name 15 30))
				maxOps.cloneNodes cutterObj cloneType:#copy actualNodeList:&c newNodes:&nnl
				cleanVolObj = nnl[1]
				cleanVolObj.name = cleanVolName
				select cleanVolObj
				modPanel.addModToSelection (Push ()) ui:on
				cleanVolObj.modifiers[#Push].Push_Value = 0.02
-- 				maxOps.CollapseNodeTo $ 1 off
				collapseStack cleanVolObj			
			
			boolObj.createBooleanObject obToBool cutterObj 4 5
			boolObj.setBoolOp obToBool 2
			convertTo obToBool PolyMeshObject	
			select obToBool
			removeIsolatedVerts obToBool
				
-- 			modPanel.addModToSelection (Cap_Holes ()) ui:on
-- 			addmodifier obToBool (Cap_Holes ())
-- 			obToBool.modifiers[#Cap_Holes].Make_All_New_Edges_Visible = 1			
-- 			convertTo obToBool PolyMeshObject				
-- 			collapseStack obToBool		
-- 					
					
			
		)
		else --do a subtraction boolean
		(
			print ("Trimming "+obToBool.name+" with "+cutterObj.name)
			boolObj.createBooleanObject obToBool cutterObj 4 5
			boolObj.setBoolOp obToBool 3
			convertTo obToBool PolyMeshObject	
			select obToBool
			removeIsolatedVerts obToBool
			
-- 			modPanel.addModToSelection (Cap_Holes ()) ui:on
-- 			addmodifier obToBool (Cap_Holes ())			
-- 			obToBool.modifiers[#Cap_Holes].Make_All_New_Edges_Visible = 1			
-- 			convertTo obToBool PolyMeshObject	
-- 			collapseStack
			
		
		)
	)
	
			--now do a vol select using cleanVolObj and delete non selected verts
			select obToBool
		
			modPanel.addModToSelection (Vol__Select ()) ui:on
			obToBool.modifiers[#Vol__Select].level = 1
			obToBool.modifiers[#Vol__Select].Volume = 3
			obToBool.modifiers[#Vol__Select].node = cleanVolObj
			modPanel.addModToSelection (Edit_Poly ()) ui:on		
			obToBool.modifiers[#Vol__Select].invert = on
			subobjectLevel = 1
			obToBool.modifiers[#Edit_Poly].useStackSelection = on
			obToBool.modifiers[#Edit_Poly].ButtonOp #DeleteVertex
			collapseStack obToBool
			delete cleanVolObj	

			--now for ease of debugging set the mat id of this object to be the index number of the volume.
			faceCnt = obToBool.numFaces
			subobjectLevel = 4
			for f = 1 to  faceCnt do
			(
				obToBool.EditablePoly.SetSelection #Face #{f}
				obToBool.EditablePoly.setMaterialIndex vIndex 1			
			)
			
-- 	--enable the command panel for speed purposes
-- 	cui.commandPanelOpen = true
)

fn generateMiscVols = --this generates the extra cutters used which arent cylinders such as those used to trim edges of arms and trim opposing thighs
(
	for ob = 1 to splitVolObjArray.count do
	(
		for tr = 1 to splitVolObjArray[ob].count do
		(
			if splitVolObjArray[ob][tr] == "trimVol_R_ArmBox" then
			(
				print ("Generating "+splitVolObjArray[ob][tr])
				nudger = -0.025
				
				trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:2 width:1 height:2 mapcoords:on --transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
				trimmer.name = splitVolObjArray[ob][tr]
				
				arm = $SKEL_R_UpperArm
				aPos = arm.position[1]
				bPos = trimmer.position[1]

				NPos = (trimmer.width /2)

				-- bocks.position.controller.x_position = ( aPos)
				neg = undefined
				if Apos < 0 do
				(
					Apos = apos * -1
					neg = true
				)
				if nPos < 0 do
				(
					nPos = nPos * -1
				)
				newP = (apos + nPos)

				if neg == true then
				(
					trimmer.position.controller.x_position  = ((newP * -1) + nudger)
				)
				else
				(
					trimmer.position.controller.x_position = (newP + nudger)
				)
			)
			else
			(
				if splitVolObjArray[ob][tr] == "trimVol_L_ArmBox" then
				(
					print ("Generating "+splitVolObjArray[ob][tr])					
					nudger = 0.025
					
					trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:2 width:1 height:2 mapcoords:on --transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
					trimmer.name = splitVolObjArray[ob][tr]
					
					arm = $SKEL_L_UpperArm
					aPos = arm.position[1]
					bPos = trimmer.position[1]

					NPos = (trimmer.width /2)

					-- bocks.position.controller.x_position = ( aPos)
					neg = undefined
					if Apos < 0 do
					(
						Apos = apos * -1
						neg = true
					)
					if nPos < 0 do
					(
						nPos = nPos * -1
					)
					newP = (apos + nPos)

					if neg == true then
					(
						trimmer.position.controller.x_position  = ((newP * -1) + nudger)
					)
					else
					(
						trimmer.position.controller.x_position = (newP + nudger)
					)
				)
				else
				(
					if splitVolObjArray[ob][tr] == "trimVol_L_ThighBox" then
					(
						print ("Generating "+splitVolObjArray[ob][tr])					
						nudger = - 0.08
						
						
						trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:2 width:1 height:2 mapcoords:on --transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
						trimmer.name = splitVolObjArray[ob][tr]
						
						arm = $SKEL_L_Thigh
						aPos = arm.position[1]
						bPos = trimmer.position[1]

						NPos = (trimmer.width /2)

						-- bocks.position.controller.x_position = ( aPos)
						neg = undefined
						if Apos < 0 do
						(
							Apos = apos * -1
							neg = true
						)
						if nPos < 0 do
						(
							nPos = nPos * -1
						)
						newP = (apos + nPos)

						if neg == true then
						(
-- 								trimmer.position.controller.x_position  = ((newP * -1) + nudger )
							in coordsys world trimmer.position.controller.X_Position = (( 0 - (trimmer.width / 2)))
						)
						else
						(
-- 								trimmer.position.controller.x_position = (newP + (nudger * -1))
							in coordsys world trimmer.position.controller.X_Position = (( 0 - (trimmer.width / 2)) * -1)
						)
					)					
					else
					(
						if splitVolObjArray[ob][tr] == "trimVol_R_ThighBox" then
						(
							print ("Generating "+splitVolObjArray[ob][tr])					
-- 							nudger = 0.08
							nudger = 0.12
							
							trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:2 width:1 height:2 mapcoords:on --transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
							trimmer.name = splitVolObjArray[ob][tr]
							
							arm = $SKEL_R_Thigh
							aPos = arm.position[1]
							bPos = trimmer.position[1]

							NPos = (trimmer.width /2)

							-- bocks.position.controller.x_position = ( aPos)
							neg = undefined
							if Apos < 0 do
							(
								Apos = apos * -1
								neg = true
							)
							if nPos < 0 do
							(
								nPos = nPos * -1
							)
							newP = (apos + nPos)

							if neg == true then
							(
-- 								trimmer.position.controller.x_position  = ((newP * -1) + nudger )
								in coordsys world trimmer.position.controller.X_Position = (( 0 - (trimmer.width / 2)))
							)
							else
							(
-- 								trimmer.position.controller.x_position = (newP + (nudger * -1))
								in coordsys world trimmer.position.controller.X_Position = (( 0 - (trimmer.width / 2)) * -1)
							)
						)
						else
						(
							if splitVolObjArray[ob][tr] == "trimVol_Pelvis_Bot" then
							(
								trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:1 width:1 height:0.25 mapcoords:on --transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
								trimmer.name = splitVolObjArray[ob][tr]
								--we will now position the trimbox at the thigh then offset it down by a percentage fo the thigh length
								thighLength = distance $Skel_L_Thigh $Skel_L_Calf
								print ("thighLength = "+(thighlength as string))
								trimmer.parent = $SKEL_L_Thigh
								in coordsys parent trimmer.position = [0,0,0]
								trimmer.parent = undefined
								trimPos = in coordsys world trimmer.position
								--newZ = (trimPos[3]-((thighLength/100) * 30))
								newZ = (trimPos[3] - ((thighLength / 100) * 90))
								trimPosNew = [trimPos[1], trimPos[2], newZ]
								in coordsys world trimmer.position = trimPosNew
							)
							else
							(
								if splitVolObjArray[ob][tr] == "trimBox_R_Shoulder" then
								(
									trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:1 width:1 height:0.25 mapcoords:on --transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
									trimmer.position = $Skel_R_Upperarm.position
									trimmer.name = splitVolObjArray[ob][tr]
									in coordsys world trimmer.rotation.controller.Y_Rotation = 90
								)
								else
								(
									if splitVolObjArray[ob][tr] == "trimBox_L_Shoulder" then
									(
										trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:1 width:1 height:0.25 mapcoords:on --transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
										trimmer.position = $Skel_R_Upperarm.position
										trimmer.name = splitVolObjArray[ob][tr]
										in coordsys world trimmer.rotation.controller.Y_Rotation = -90
									)
									else
									(
										if splitVolObjArray[ob][tr] == "trimBox_R_Shoulder_ForSpine3" then
										(
											trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:1 width:1 height:0.25 mapcoords:on --transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
											trimmer.position = $Skel_R_Upperarm.position
											trimmer.name = splitVolObjArray[ob][tr]
											in coordsys world trimmer.rotation.controller.Y_Rotation = -90
										)
										else
										(
											if splitVolObjArray[ob][tr] == "trimBox_L_Shoulder_ForSpine3" then
											(
												trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:1 width:1 height:0.25 mapcoords:on --transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
												trimmer.position = $Skel_L_Upperarm.position
												trimmer.name = splitVolObjArray[ob][tr]
												in coordsys world trimmer.rotation.controller.Y_Rotation = 90
											)
											else
											(
												if splitVolObjArray[ob][tr] == "trimBox_Clavs" do
												(
													trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:1 width:1 height:0.25 mapcoords:on --transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
													trimmer.name = splitVolObjArray[ob][tr]
													--we will now position the trimbox at the thigh then offset it down by a percentage fo the thigh length
													thighLength = distance $Skel_Spine3 $Skel_Neck_1
													print ("thighLength = "+(thighlength as string))
													trimmer.parent = $SKEL_Spine3
													in coordsys parent trimmer.position = [0,0,0]
													trimmer.parent = undefined
													trimPos = in coordsys world trimmer.position
													newZ = (trimPos[3] + ((thighLength / 100) * 30))
													trimPosNew = [trimPos[1], trimPos[2], newZ]
													in coordsys world trimmer.position = trimPosNew
													in coordsys world trimmer.controller.rotation.X_rotation = 180
												)
											)
										)
									)
								)
							)
						)
					)
				)
			)
		)	
	)
)

fn createVols = 
(

-- 	splitVolObjArray = #()
	
	rigBoneListStruct = #( --array of all skeleton bones which we want to split mesh for
		( rigBoneStruct rb_bone:$SKEL_Pelvis  rb_Radius: 0.25 rb_Length:($SKEL_Pelvis.length * 2) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_Spine0  rb_Radius: 0.25 rb_Length:((distance $SKEL_Spine0 $SKEL_Spine1)) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_Spine1  rb_Radius: 0.25 rb_Length:($SKEL_Spine1.length * 1.4) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_Spine2  rb_Radius: 0.22 rb_Length:($SKEL_Spine2.length * 1.4) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_Spine3  rb_Radius: 0.3 rb_Length:($SKEL_Spine3.length * 0.8) rb_Trans:undefined ), 


-- 		( rigBoneStruct rb_bone:$SKEL_Head  rb_Radius: 0.15 rb_Length:($SKEL_Head.length * 2) rb_Trans:(matrix3 [-1,0.000145016,-0.000328264] [-5.81659e-005,-0.96816,-0.250334] [-0.000354256,-0.250334,0.96816] [-0.000112729,0.0139014,1.59917]) ),	
		( rigBoneStruct rb_bone:$SKEL_Head  rb_Radius: 0.19 rb_Length:($SKEL_Head.length * 2) rb_Trans:undefined),	
		( rigBoneStruct rb_bone:$SKEL_Neck_1 rb_Radius: 0.15 rb_Length:($SKEL_Neck_1.length * 0.45) rb_Trans:undefined),			
		
		( rigBoneStruct rb_bone:$SKEL_L_Thigh  rb_Radius: 0.18 rb_Length:$SKEL_L_Thigh.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Calf  rb_Radius: 0.15 rb_Length:$SKEL_L_Calf.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Foot  rb_Radius: 0.1 rb_Length:($SKEL_L_Foot.length * 1.5) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Toe0  rb_Radius: 0.1 rb_Length:0.25 rb_Trans:undefined ),
		
		( rigBoneStruct rb_bone:$SKEL_R_Thigh  rb_Radius: 0.18 rb_Length:$SKEL_R_Thigh.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Calf  rb_Radius: 0.15 rb_Length:$SKEL_R_Calf.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Foot  rb_Radius: 0.1 rb_Length:($SKEL_R_Foot.length * 1.5) rb_Trans:(matrix3 [0.999233,0.0391547,3.80804e-005] [-0.00134612,0.0333794,0.999442] [0.0391315,-0.998676,0.0334067] [-0.155,0.145386,0.0342977]) ), 
		( rigBoneStruct rb_bone:$SKEL_R_Toe0  rb_Radius: 0.1 rb_Length:0.25 rb_Trans:undefined ), 

		( rigBoneStruct rb_bone:$SKEL_L_Clavicle  rb_Radius: 0.15 rb_Length:$SKEL_L_Clavicle.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_UpperArm  rb_Radius: 0.1 rb_Length:$SKEL_L_UpperArm.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Forearm  rb_Radius: 0.1 rb_Length:($SKEL_L_Forearm.length * 1.1) rb_Trans:undefined ), 
		
		( rigBoneStruct rb_bone:$SKEL_L_Hand  rb_Radius: 0.04 rb_Length:($SKEL_L_Hand.length * 0.9) rb_Trans:undefined ), 
		
		( rigBoneStruct rb_bone:$SKEL_L_Finger00  rb_Radius: 0.018 rb_Length:($SKEL_L_Finger00.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger01  rb_Radius: 0.024 rb_Length:($SKEL_L_Finger01.length * 1.15) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger02  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger02.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger10  rb_Radius: 0.016 rb_Length:($SKEL_L_Finger10.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger11  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger11.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger12  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger12.length * 1.3) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger20  rb_Radius: 0.014 rb_Length:($SKEL_L_Finger20.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger21  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger21.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger22  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger22.length * 1.3) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger30  rb_Radius: 0.014 rb_Length:($SKEL_L_Finger30.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger31  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger31.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger32  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger32.length * 1.3) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger40  rb_Radius: 0.014 rb_Length:($SKEL_L_Finger40.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger41  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger41.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger42  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger42.length * 1.3) rb_Trans:undefined ), 

		( rigBoneStruct rb_bone:$SKEL_R_Clavicle  rb_Radius: 0.15 rb_Length:$SKEL_R_Clavicle.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_UpperArm  rb_Radius: 0.1 rb_Length:$SKEL_R_UpperArm.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Forearm  rb_Radius: 0.1 rb_Length:($SKEL_R_Forearm.length * 1.1) rb_Trans:undefined ), 
		
		( rigBoneStruct rb_bone:$SKEL_R_Hand  rb_Radius: 0.04 rb_Length:($SKEL_R_Hand.length * 0.9) rb_Trans:undefined ), 
		
		( rigBoneStruct rb_bone:$SKEL_R_Finger00  rb_Radius: 0.018 rb_Length:($SKEL_R_Finger00.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger01  rb_Radius: 0.024 rb_Length:($SKEL_R_Finger01.length * 1.15) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger02  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger02.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger10  rb_Radius: 0.016 rb_Length:($SKEL_R_Finger10.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger11  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger11.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger12  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger12.length * 1.3) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger20  rb_Radius: 0.014 rb_Length:($SKEL_R_Finger20.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger21  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger21.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger22  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger22.length * 1.3) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger30  rb_Radius: 0.014 rb_Length:($SKEL_R_Finger30.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger31  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger31.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger32  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger32.length * 1.3) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger40  rb_Radius: 0.014 rb_Length:($SKEL_R_Finger40.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger41  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger41.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger42  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger42.length * 1.3) rb_Trans:undefined )
	)
	
	--******************************************************************************************
	--NOW CREATE THE VOLUMES FOR DETACHING THE MESH INTO PIECES
	--******************************************************************************************
	for i = 1 to rigBoneListStruct.count do
	(
		volTrans = undefined
		
		currentObj = getNodeByName ("splitVol_"+rigBoneListStruct[i].rb_bone.name)
		if currentObj != undefined do
		(
			delete currentObj
		)
		
		--we need to make the feet vols as cubes so this is a bit of a hack
		if (rigBoneListStruct[i].rb_bone.name == "SKEL_L_Foot") then
		(
			splitObj = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:0.4 width:0.3 height:0.125 mapcoords:on pos:[0.141602,0.0458236,0] isSelected:on
			splitObj.name = ("splitVol_"+rigBoneListStruct[i].rb_bone.name)
			splitObj.parent = rigBoneListStruct[i].rb_bone	
		)
		else
		(
			if (rigBoneListStruct[i].rb_bone.name == "SKEL_R_Foot") then
			(
				splitObj = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:0.4 width:0.3 height:0.125 mapcoords:on pos:[-0.141602,0.0458236,0] isSelected:on
				splitObj.name = ("splitVol_"+rigBoneListStruct[i].rb_bone.name)
				splitObj.parent = rigBoneListStruct[i].rb_bone						
			)
			else
			(
				
				
				splitObj = Cylinder smooth:on heightsegs:8 capsegs:4 sides:12 height:rigBoneListStruct[i].rb_Length radius:rigBoneListStruct[i].rb_Radius mapcoords:on pos:[0,0,0] isSelected:on
				splitObj.name = ("splitVol_"+rigBoneListStruct[i].rb_bone.name)
				splitObj.parent = rigBoneListStruct[i].rb_bone
				
				
				--******************************************************************************************
				--NOW SORT OUT THE ALIGNMENT OF THE VOLUME
				--******************************************************************************************
				if rigBoneListStruct[i].rb_Trans == undefined then 
				(
					volTrans = rigBoneListStruct[i].rb_bone.transform
					splitObj.transform = ( in coordsys parent volTrans)
					in coordsys parent rotate splitObj (angleaxis 90 [0,1,0])
					if (substring splitObj.name 15 5) == "Spine" do
					(
						in coordsys world splitObj.rotation = (quat 3.65661e-007 0 -1 6.78003e-007)
					)
				)
				else --this allows us to offset the transforms from the bone.
				(
					volTrans = rigBoneListStruct[i].rb_Trans
					splitObj.transform = ( in coordsys parent volTrans)
				)
				if splitObj.name == "splitVol_SKEL_Head" do
				(
					splitObj.position = $RB_Neck_1.position
				)
				
-- 				append splitVolObjArray splitObj		
			)
		)
	)

	--hacky little offset for the neck
	in coordsys parent $splitVol_SKEL_Neck_1.position.controller.X_position = 0.01
	--now we need to make the custom trimmers
	generateMiscVols()
)

fn runBooleanProc = --this kicks it all off
(
	--first off create the volumes
	createVols()

	--need to do a segment on all the geo - hands and body
-- 	disableSceneRedraw()
-- 	--disable the command panel for speed purposes
-- 	cui.commandPanelOpen = false

	shouldIskinHands = true --flag set to control if we are skinning the hands or segmenting them
	
	obToOperateOn = #( --array of objects which will get segmented etc
		$RAWMESH
-- 		$Uppr_000_U,
-- 		$Lowr_000_U 
	)
	finalObNames = #( --array of names to rename the ifnal recombined segmented meshes to based off the index of obToOperateOn
		"head_000_r"
-- 		"Uppr_000_U",
-- 		"Lowr_000_U"
		)
	
	for m = 1 to obToOperateon.count do
-- 	for m = 3 to obToOperateon.count do	
	(
		disableSceneRedraw()
		--disable the command panel for speed purposes
		cui.commandPanelOpen = false
		
		--we need to teselate the hands so booleans work
		if m != 1 do
		(
			addmodifier obToOperateon[m] (TurboSmooth ())	
			obToOperateon[m].modifiers[#TurboSmooth].iterations = 2 --trying to put more polies in
			collapsestack obToOperateon[m] 
		)
		
		masterMesh = obToOperateon[m]

		("Booleaning a copy of "+masterMesh.name)
		for i = 1 to splitVolObjArray.count do
		(
			vIndex = i
			boolMesh masterMesh splitVolObjArray[i] vIndex
		)
		--ok now we can try and delete
		print ("Deleting "+masterMesh.name)
		delete masterMesh
		
		--now need to filein the combine function
		enableSceneRedraw()
		--enable the command panel for speed purposes
		cui.commandPanelOpen = true
		filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/Rigging_tools/giant/combineGeo.ms")	
		
		--now we can rename 
		--** geo_Pelvis will always be the name of the geo as thats the first piece of segmented geo ***
		$geo_Pelvis.name = finalObNames[m]
	)
	
	print "trimming complete...."
	print "CLEANING VOLS"
	cleanVols()


)

runBooleanProc()