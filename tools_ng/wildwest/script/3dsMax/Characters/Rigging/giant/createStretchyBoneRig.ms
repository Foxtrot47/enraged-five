--createStretchyBoneRig.ms
--Script to create markers to allow creation of a stretchy rig for skeleton spotting purposes.
--Jan 2011
--Matt Rennie
markerParentingArray = #( --first element is the children, second is the parent
	#( 
"MARKER_L_Finger0_NUB",
"MARKER_L_Finger02",
"MARKER_L_Finger01",
"MARKER_L_Finger00",
"MARKER_L_Finger1_NUB",
"MARKER_L_Finger12",
"MARKER_L_Finger11",
"MARKER_L_Finger10",
"MARKER_L_Finger2_NUB",
"MARKER_L_Finger22",
"MARKER_L_Finger21",
"MARKER_L_Finger20",
"MARKER_L_Finger3_NUB",
"MARKER_L_Finger32",
"MARKER_L_Finger31",
"MARKER_L_Finger30",
"MARKER_L_Finger4_NUB",
"MARKER_L_Finger42",
"MARKER_L_Finger41",
"MARKER_L_Finger40",
"MARKER_L_Hand",
"MARKER_L_Forearm",
"MARKER_L_UpperArm",
"MARKER_L_Clavicle",
"MARKER_Spine3",
"MARKER_Spine2",
"MARKER_Spine1",
"MARKER_Spine0",
"MARKER_Spine_Root",
"MARKER_R_Toe0_NUB",
"MARKER_R_Toe0",
"MARKER_R_Foot",
"MARKER_R_Calf",
"MARKER_R_Thigh",
"MARKER_L_Toe0_NUB",
"MARKER_L_Toe0",
"MARKER_L_Foot",
"MARKER_L_Calf",
"MARKER_L_Thigh",
"MARKER_Head_NUB",
"MARKER_Head",
"MARKER_Neck_1",
"MARKER_R_Finger4_NUB",
"MARKER_R_Finger42",
"MARKER_R_Finger41",
"MARKER_R_Finger40",
"MARKER_R_Finger3_NUB",
"MARKER_R_Finger32",
"MARKER_R_Finger31",
"MARKER_R_Finger30",
"MARKER_R_Finger2_NUB",
"MARKER_R_Finger22",
"MARKER_R_Finger21",
"MARKER_R_Finger20",
"MARKER_R_Finger1_NUB",
"MARKER_R_Finger12",
"MARKER_R_Finger11",
"MARKER_R_Finger10",
"MARKER_R_Finger0_NUB",
"MARKER_R_Finger02",
"MARKER_R_Finger01",
"MARKER_R_Finger00",
"MARKER_R_Hand",
"MARKER_R_Forearm",
"MARKER_R_UpperArm",
"MARKER_R_Clavicle"
	),
	#(
"MARKER_L_Finger02",
"MARKER_L_Finger01",
"MARKER_L_Finger00",
"MARKER_L_Hand",
"MARKER_L_Finger12",
"MARKER_L_Finger11",
"MARKER_L_Finger10",
"MARKER_L_Hand",
"MARKER_L_Finger12",
"MARKER_L_Finger11",
"MARKER_L_Finger10",
"MARKER_L_Hand",
"MARKER_L_Finger12",
"MARKER_L_Finger11",
"MARKER_L_Finger10",
"MARKER_L_Hand",
"MARKER_L_Finger12",
"MARKER_L_Finger11",
"MARKER_L_Finger10",
"MARKER_L_Hand",
"MARKER_L_Forearm",
"MARKER_L_UpperArm",
"MARKER_L_Clavicle",
"MARKER_Spine3",
"MARKER_Spine2",
"MARKER_Spine1",
"MARKER_Spine0",
"MARKER_Spine_Root",
"MARKER_Pelvis",
"MARKER_R_Toe0",
"MARKER_R_Foot",
"MARKER_R_Calf",
"MARKER_R_Thigh",
"MARKER_Pelvis",
"MARKER_L_Toe0",
"MARKER_L_Foot",
"MARKER_L_Calf",
"MARKER_L_Thigh",
"MARKER_Pelvis",
"MARKER_Head",
"MARKER_Neck_1",
"MARKER_Spine3",
"MARKER_R_Finger42",
"MARKER_R_Finger41",
"MARKER_R_Finger40",
"MARKER_R_Hand",
"MARKER_R_Finger32",
"MARKER_R_Finger31",
"MARKER_R_Finger30",
"MARKER_R_Hand",
"MARKER_R_Finger22",
"MARKER_R_Finger21",
"MARKER_R_Finger20",
"MARKER_R_Hand",
"MARKER_R_Finger12",
"MARKER_R_Finger11",
"MARKER_R_Finger10",
"MARKER_R_Hand",
"MARKER_R_Finger02",
"MARKER_R_Finger01",
"MARKER_R_Finger00",
"MARKER_R_Hand",
"MARKER_R_Forearm",
"MARKER_R_UpperArm",
"MARKER_R_Clavicle",
"MARKER_Spine3"
		)
	)

parentArray = #(
"SKEL_Pelvis",
"SKEL_L_Thigh",
"SKEL_L_Calf",
"SKEL_L_Foot",
"SKEL_L_Toe0",
"SKEL_Pelvis",
"SKEL_R_Thigh",
"SKEL_R_Calf",
"SKEL_R_Foot",
"SKEL_R_Toe0",
"SKEL_Spine_Root",
"SKEL_Spine0",
"SKEL_Spine1",
"SKEL_Spine2",
"SKEL_Spine3",
"SKEL_L_Clavicle",
"SKEL_L_UpperArm",
"SKEL_L_Forearm",
"SKEL_L_Hand",
"SKEL_L_Finger00",
"SKEL_L_Finger01",
"SKEL_L_Finger02",
"SKEL_L_Hand",
"SKEL_L_Finger10",
"SKEL_L_Finger11",
"SKEL_L_Finger12",
"SKEL_L_Hand",
"SKEL_L_Finger20",
"SKEL_L_Finger21",
"SKEL_L_Finger22",
"SKEL_L_Hand",
"SKEL_L_Finger30",
"SKEL_L_Finger31",
"SKEL_L_Finger32",
"SKEL_L_Hand",
"SKEL_L_Finger40",
"SKEL_L_Finger41",
"SKEL_L_Finger42",
"SKEL_Spine3",
"SKEL_R_Clavicle",
"SKEL_R_UpperArm",
"SKEL_R_Forearm",
"SKEL_R_Hand",
"SKEL_R_Finger00",
"SKEL_R_Finger01",
"SKEL_R_Finger02",
"SKEL_R_Hand",
"SKEL_R_Finger10",
"SKEL_R_Finger11",
"SKEL_R_Finger12",
"SKEL_R_Hand",
"SKEL_R_Finger20",
"SKEL_R_Finger21",
"SKEL_R_Finger22",
"SKEL_R_Hand",
"SKEL_R_Finger30",
"SKEL_R_Finger31",
"SKEL_R_Finger32",
"SKEL_R_Hand",
"SKEL_R_Finger40",
"SKEL_R_Finger41",
"SKEL_R_Finger42",
"SKEL_Spine3",
"SKEL_Head",
"SKEL_Neck_1"
)

childArray = #(
"SKEL_L_Thigh",
"SKEL_L_Calf",
"SKEL_L_Foot",
"SKEL_L_Toe0",
"SKEL_L_Toe0_NUB",
"SKEL_R_Thigh",
"SKEL_R_Calf",
"SKEL_R_Foot",
"SKEL_R_Toe0",
"SKEL_R_Toe0_NUB",
"SKEL_Spine0",
"SKEL_Spine1",
"SKEL_Spine2",
"SKEL_Spine3",
"SKEL_L_Clavicle",
"SKEL_L_UpperArm",
"SKEL_L_Forearm",
"SKEL_L_Hand",
"SKEL_L_Finger00",
"SKEL_L_Finger01",
"SKEL_L_Finger02",
"SKEL_L_Finger0_NUB",
"SKEL_L_Finger10",
"SKEL_L_Finger11",
"SKEL_L_Finger12",
"SKEL_L_Finger1_NUB",
"SKEL_L_Finger20",
"SKEL_L_Finger21",
"SKEL_L_Finger22",
"SKEL_L_Finger2_NUB",
"SKEL_L_Finger30",
"SKEL_L_Finger31",
"SKEL_L_Finger32",
"SKEL_L_Finger3_NUB",
"SKEL_L_Finger40",
"SKEL_L_Finger41",
"SKEL_L_Finger42",
"SKEL_L_Finger4_NUB",
"SKEL_R_Clavicle",
"SKEL_R_UpperArm",
"SKEL_R_Forearm",
"SKEL_R_Hand",
"SKEL_R_Finger00",
"SKEL_R_Finger01",
"SKEL_R_Finger02",
"SKEL_R_Finger0_NUB",
"SKEL_R_Finger10",
"SKEL_R_Finger11",
"SKEL_R_Finger12",
"SKEL_R_Finger1_NUB",
"SKEL_R_Finger20",
"SKEL_R_Finger21",
"SKEL_R_Finger22",
"SKEL_R_Finger2_NUB",
"SKEL_R_Finger30",
"SKEL_R_Finger31",
"SKEL_R_Finger32",
"SKEL_R_Finger3_NUB",
"SKEL_R_Finger40",
"SKEL_R_Finger41",
"SKEL_R_Finger42",
"SKEL_R_Finger4_NUB",
"SKEL_Neck_1",
"SKEL_Head_NUB",
"SKEL_Head"
)


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



fn createLengthExpression currObj TapeObj currChild = 
(
-- 	print "Starting to create expressions..."

	childName = (substring currChild.name 8 200 )
	fcStr = ("$"+currObj.name+".modifiers[#Attribute_Holder]."+childName+".uiBoneLength.controller") --the path to the controller itself
		newfcStr = execute fcStr
	
	
	SVN = ("Distance")

	drvStr = ("$"+TapeObj.name+".distance") --path to controller of driver
		driverStr = execute drvStr
	
-- 	newfcStr.AddScalarTarget SVN driverStr --add scalar pointing to the translation of the joystick object
-- 	newfcStr.SetExpression	(SVN) --set the expression here
)



fn addCustomAttributes obj currObj = --this will add custom attributes to the imagePLaneRootNode to allow the user to slide them into and out of a view to help clipping
(
-- 	select $imagePlaneRootNode
	
	currentJoystick = obj
	
	print ("Starting to add attributes to "+currentJoystick.name)
	
	select currentJoystick

	if currentJoystick != undefined do 
	(
		if currentJoystick.modifiers[#Attribute_Holder] == undefined do
		(
			select currentJoystick
			modPanel.addModToSelection (EmptyModifier ()) ui:on
		)

		facialMultipliersCA = (currentJoystick.name +"CA")

-- 		rollName = ("Adjustments")
		objNameStr = (substring currObj.name 8 200)
		rollName = (objNameStr)
				
			uiName = ("uiBoneLength") --name as seen inside the code and track view	
			displayName = ("BoneLength") --name as seen in the ui

			posVal = "1000"
			negval = "-1000"		
				
		rolloutCreation = (facialMultipliersCA +" = attributes "+rollName +"\n"+"(" +"\n"+"\t" +"parameters main rollout:params"+"\n"+"\t"+"(" +"\n" +"\t" +"\t" +uiName +" type:#float ui:" +uiName +" default:" +"0.0" +"\n" +"\t"+")" +"\n" +"\t"  +"rollout params "+" \"" +rollName+" \"" +"\n" +"\t" +"(" +"\n" +"\t"+"\t"  +"spinner "+uiName +" \"" +displayName+"\"" +" "+"fieldwidth:40 range:[-100,100,1] type:#float" +"\n" +"\t" +")" +"\n" +")"+"\n" +"\n" +"\t" +"custAttributes.add $" +currentJoystick.name+".modifiers[#'Attribute holder'] "+facialMultipliersCA)
			execute rolloutCreation		

		--now add bezier floats controllers to these attributes
-- 		cont1 = ("$"+currentJoystick.name+".modifiers[#Attribute_Holder]."+rollName+"."+uiName+".controller = bezier_float()")
		cont1 = ("$"+currentJoystick.name+".modifiers[#Attribute_Holder]."+rollName+"."+uiName+".controller = Bezier_Float()")
			execute cont1
			
		--- ok now the attributes have been created we need to hook these attributes up to the position of the image planes
		print ("Custom attrs added to "+currentjoystick.name)
	)
)


fn addStretchConstraints =
(
	for p = 1 to parentArray.count do
	(
		--first create an markerPoint object name Marker _ parentArray[p].nme and align to parent object
		print ("looking for " +parentArray[p])
		
		pObj = getNodeByName parentArray[p]
		pName = (substring pObj.name 6 100) --stips off the SKEL_ part of name		
		pPos = in coordsys parent pObj.position
		pRot = in coordsys parent pObj.Rotation			
		markerPointName = ("MARKER_"+pName)
		etmName = ("ETM_"+pName)
		markerPoint = getNodeByName markerPointName
		etm = getnodebyname etmName
		if markerPoint == undefined do
		(
			--
			markerPoint = Point pos:[0,0,0]isSelected:on size:0.1 box:on cross:off
-- 				in coordsys world markerPoint.position = pPos
	-- 			in coordsys world markerPoint.rotation = pRot

			markerPoint.Name = markerPointName
			markerPoint.parent = pObj
			markerPoint.rotation.controller 
			in coordsys parent markerPoint.position = [0,0,0]
			in coordsys parent markerPoint.rotation = (quat 0 0 0 1)
				print (markerPoint.name+" pos = "+((in coordsys world markerPoint.position) as string))
		)
		if etm == undefined do
		(
			etm = ExposeTm pos:[0,0,0]isSelected:on size:0.1
			etm.parent = markerPoint
			etm.transform = markerPoint.transform
			etm.Name = etmName
		)
		
			--now find out what the skel objects parent would be and find the corresponding marker and if it exists link it the same way
			pParent = pObj.parent
			if pParent != undefined do
			(
				pParName = pParent.name
				pParSubst = (substring pParName 6 100)
				markerParent = ("MARKER_"+pParSubst)
				markerParObj = getNodeByName markerParent
				if markerParObj != undefined do
				(
					print ("Linking "+markerPoint.name+" to "+markerParObj.name)
					markerPoint.parent = markerParObj
				)
			)

		--then create another markerPoint at the child and name appropriately
		cObj = getNodeByName childArray[p]			
		cName = (substring cObj.name 6 100) --stips off the SKEL_ part of name			
			
		childMarkerName = ("MARKER_"+cName)
		markerPoint2 = getNodeByName childMarkerName 
		
		--**CHECK TO SEE IF THE NODE ALREADY EXISTS AND IF SO DONT RE-CREATE**
		if markerPoint2 == undefined do
		(
			cPos = in coordsys world cObj.position
-- 			cRot = in coordsys world cObj.Rotation
			
			--markerPoint2 = ExposeTm pos:[0,0,0]isSelected:on size:0.1
			markerPoint2 = Point pos:[0,0,0]isSelected:on size:0.1 box:on
				in coordsys world markerPoint2.position = cPos
-- 				in coordsys world markerPoint2.rotation = cRot
			markerPoint2.Name = childMarkerName
			
			markerPoint2.parent = markerPoint
		)
		
		etm.exposeNode = markerPoint2
		etm.useParent = off
		etm.localReferenceNode = markerPoint --this needs to be the parent of the 

		markerPoint.transform = pObj.transform

-- 		in coordsys parent markerPoint.rotation = pRot
-- 		in coordsys parent markerPoint.position = pPos
		
		--then add a position constraint on bone and constrain to markerPoint object
-- 		pObj.pos.controller.Available.controller = Position_Constraint ()
-- 		pObj.pos.controller.Position_Constraint.appendTarget markerPoint 100
-- 		pObj.pos.controller.Position_Constraint.relative = on
-- 				
-- 		pObj.rotation.controller.Available.controller = LookAt_Constraint ()
-- 		pObj.rotation.controller[3].appendTarget markerPoint2 100 --relative:true
-- 		pObj.rotation.controller.LookAt_Constraint.controller.viewline_length_abs = off
-- 		pObj.rotation.controller.LookAt_Constraint.controller.relative = on
		
		--now add custom attributes ready for connecting to etm distance
		addCustomAttributes markerPoint markerPoint2
		
		--now add the expression connecting distance to custom attr
		--name of object expression added to, etmNode, child node
-- 		createLengthExpression markerPoint etm markerPoint2
		objNameStr = (substring markerPoint2.name 8 200)
		if markerPoint.modifiers[#Attribute_Holder] != undefined do
		(
			etmNodeParent = --
			print ("etm.distance = "+etm.distance as string)
			print ("markerPoint.name = "+markerPoint.name)
			print ("objNameStr = "+objNameStr )
			execStr = ("paramWire.connect $"+etm.name+".distance $"+markerPoint.name+".modifiers[#Attribute_Holder]."+objNameStr +".uiBoneLength.controller "+"\""+"Distance"+"\"") 
			print execsTR
-- 	 			omNom = execute execStr
		)

	)
	
	
-- 	for m = 1 to markerParentingArray.count do
-- 	(
-- 		marker = getnodebyname markerParentingArray[m]
-- 		etmName = ("ETM_"+(substring marker.name 8 200))
-- 		etm = getnodebyname etmName
-- 		etm.exposeNode = markerPoint2
-- 		etm.useParent = off
-- 		etm.localReferenceNode = markerPoint --this needs to be the parent of the 
-- 	)
)

addStretchConstraints()