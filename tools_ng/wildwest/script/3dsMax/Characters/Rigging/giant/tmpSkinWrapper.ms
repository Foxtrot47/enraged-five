filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()


filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Includes\\FN_Rigging.ms")	


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




fn combineIndiv splitMeshes = --this takes all of the split meshes and combines them into one
(
	dupMeshes = #()
	--first duplicate all of the pieces
	for i = 1 to splitMeshes.count do
	(
		origName = splitMeshes[i].name
		print ("origName = "+origName)
		newName = ("dup"+(substring origName 4 20))
		maxOps.cloneNodes splitMeshes[i] cloneType:#copy actualNodeList:&c newNodes:&nnl
		newNode = nnl[1]
		newNode.name = newName
		appendIfUnique dupMeshes newNode
		deletemodifier newNode 1	
	)
	
	print ("dupMeshes count = "+(dupMeshes.count as string)+". They are:")
	print dupMeshes
	masterObj = dupMeshes[1]
	for i = dupMeshes.count to 2 by -1 do
	(
		print ("Attaching "+dupMeshes[i].name+" to "+masterObj.name)
		masterObj.attach dupMeshes[i] masterObj
	)
	
	addModifier masterObj (Skin_Wrap ())
		
	wrapMod = masterObj.modifiers[1]
		
	print ("Beginning skinWrap - this can take a while!")
		
		wrapMod.meshList = splitMeshes
		iVal = finditem splitmeshes i
		progbar.prog.value = (100.*ival/splitmeshes.count)

		wrapMod.engine = 0
	print ("Skin Wrap Complete!")
	select masterObj
)
	

fn doMe =
(
	splitMeshes = selection as array
	
	combineIndiv splitMeshes 
)

doMe()