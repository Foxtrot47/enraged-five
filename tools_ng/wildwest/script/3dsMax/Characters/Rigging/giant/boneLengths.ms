boneArray = #(
"SKEL_R_Finger02",
"SKEL_R_Finger01",
"SKEL_R_Finger00",
"SKEL_R_Finger12",
"SKEL_R_Finger11",
"SKEL_R_Finger10",
"SKEL_R_Finger22",
"SKEL_R_Finger21",
"SKEL_R_Finger20",
"SKEL_R_Finger32",
"SKEL_R_Finger31",
"SKEL_R_Finger30",
"SKEL_R_Finger42",
"SKEL_R_Finger41",
"SKEL_R_Finger40",
"SKEL_L_Finger02",
"SKEL_L_Finger01",
"SKEL_L_Finger00",
"SKEL_L_Finger12",
"SKEL_L_Finger11",
"SKEL_L_Finger10",
"SKEL_L_Finger22",
"SKEL_L_Finger21",
"SKEL_L_Finger20",
"SKEL_L_Finger32",
"SKEL_L_Finger31",
"SKEL_L_Finger30",
"SKEL_L_Finger42",
"SKEL_L_Finger41",
"SKEL_L_Finger40",
"SKEL_L_Toe0",
"SKEL_L_Foot",
"SKEL_L_Calf",
"SKEL_L_Thigh",
"SKEL_R_Toe0",
"SKEL_R_Foot",
"SKEL_R_Calf",
"SKEL_R_Thigh",
"SKEL_R_Hand",
"SKEL_R_Forearm",	
"SKEL_R_UpperArm",
"SKEL_R_Clavicle",
"SKEL_L_Hand",
"SKEL_L_Forearm",	
"SKEL_L_UpperArm",
"SKEL_L_Clavicle",
"SKEL_Head",
"SKEL_Neck_1",
"SKEL_Spine3",
"SKEL_Spine2",
"SKEL_Spine1",
"SKEL_Spine0",
"SKEL_Spine_Root",
"SKEL_Pelvis"
-- "SKEL_ROOT"
)

parentArray = #(
"SKEL_Pelvis",
"SKEL_L_Thigh",
"SKEL_L_Calf",
"SKEL_L_Foot",
"SKEL_L_Toe0",
"SKEL_Pelvis",
"SKEL_R_Thigh",
"SKEL_R_Calf",
"SKEL_R_Foot",
"SKEL_R_Toe0",
"SKEL_Spine_Root",
"SKEL_Spine0",
"SKEL_Spine1",
"SKEL_Spine2",
"SKEL_Spine3",
"SKEL_L_Clavicle",
"SKEL_L_UpperArm",
"SKEL_L_Forearm",
"SKEL_L_Hand",
"SKEL_L_Finger00",
"SKEL_L_Finger01",
"SKEL_L_Finger02",
"SKEL_L_Hand",
"SKEL_L_Finger10",
"SKEL_L_Finger11",
"SKEL_L_Finger12",
"SKEL_L_Hand",
"SKEL_L_Finger20",
"SKEL_L_Finger21",
"SKEL_L_Finger22",
"SKEL_L_Hand",
"SKEL_L_Finger30",
"SKEL_L_Finger31",
"SKEL_L_Finger32",
"SKEL_L_Hand",
"SKEL_L_Finger40",
"SKEL_L_Finger41",
"SKEL_L_Finger42",
"SKEL_Spine3",
"SKEL_R_Clavicle",
"SKEL_R_UpperArm",
"SKEL_R_Forearm",
"SKEL_R_Hand",
"SKEL_R_Finger00",
"SKEL_R_Finger01",
"SKEL_R_Finger02",
"SKEL_R_Hand",
"SKEL_R_Finger10",
"SKEL_R_Finger11",
"SKEL_R_Finger12",
"SKEL_R_Hand",
"SKEL_R_Finger20",
"SKEL_R_Finger21",
"SKEL_R_Finger22",
"SKEL_R_Hand",
"SKEL_R_Finger30",
"SKEL_R_Finger31",
"SKEL_R_Finger32",
"SKEL_R_Hand",
"SKEL_R_Finger40",
"SKEL_R_Finger41",
"SKEL_R_Finger42",
"SKEL_Spine3",
"SKEL_Head",
"SKEL_Neck_1"
)

childArray = #(
"SKEL_L_Thigh",
"SKEL_L_Calf",
"SKEL_L_Foot",
"SKEL_L_Toe0",
"SKEL_L_Toe0_NUB",
"SKEL_R_Thigh",
"SKEL_R_Calf",
"SKEL_R_Foot",
"SKEL_R_Toe0",
"SKEL_R_Toe0_NUB",
"SKEL_Spine0",
"SKEL_Spine1",
"SKEL_Spine2",
"SKEL_Spine3",
"SKEL_L_Clavicle",
"SKEL_L_UpperArm",
"SKEL_L_Forearm",
"SKEL_L_Hand",
"SKEL_L_Finger00",
"SKEL_L_Finger01",
"SKEL_L_Finger02",
"SKEL_L_Finger0_NUB",
"SKEL_L_Finger10",
"SKEL_L_Finger11",
"SKEL_L_Finger12",
"SKEL_L_Finger1_NUB",
"SKEL_L_Finger20",
"SKEL_L_Finger21",
"SKEL_L_Finger22",
"SKEL_L_Finger2_NUB",
"SKEL_L_Finger30",
"SKEL_L_Finger31",
"SKEL_L_Finger32",
"SKEL_L_Finger3_NUB",
"SKEL_L_Finger40",
"SKEL_L_Finger41",
"SKEL_L_Finger42",
"SKEL_L_Finger4_NUB",
"SKEL_R_Clavicle",
"SKEL_R_UpperArm",
"SKEL_R_Forearm",
"SKEL_R_Hand",
"SKEL_R_Finger00",
"SKEL_R_Finger01",
"SKEL_R_Finger02",
"SKEL_R_Finger0_NUB",
"SKEL_R_Finger10",
"SKEL_R_Finger11",
"SKEL_R_Finger12",
"SKEL_R_Finger1_NUB",
"SKEL_R_Finger20",
"SKEL_R_Finger21",
"SKEL_R_Finger22",
"SKEL_R_Finger2_NUB",
"SKEL_R_Finger30",
"SKEL_R_Finger31",
"SKEL_R_Finger32",
"SKEL_R_Finger3_NUB",
"SKEL_R_Finger40",
"SKEL_R_Finger41",
"SKEL_R_Finger42",
"SKEL_R_Finger4_NUB",
"SKEL_Neck_1",
"SKEL_Head_NUB",
"SKEL_Head"
)

boneLengthList = #()
inputData = #()

fn unparentRig = 
(
	for i = 1 to parentArray.count do
	(
		thisBn = getNodeByName parentArray[i]
		select thisBn
		max unlink 
	)
)

fn reparentRig =
(
	for i = 1 to childArray.count do
	(
		thisBn = getNodeByName childArray[i]
		thisParent = getNodeByName parentArray[i]
		thisBn.parent = thisParent
	)
)

fn setBoneLengths = 
(
	unparentRig()
	
	if input_name == undefined do
	(
		input_name = getOpenFileName caption:"Bone Length file" types:"BoneLengths (*.len)|*.len|All Files (*.*)|*.*|"
	)

	if input_name != undefined then
	(
		f = openfile input_name
		inputData = #() -- define as array
		while not eof f do
		(
		append inputData (filterstring (readLine f) ",")
		)
		close f
	)
	
	for i in inputData do
	(
		execStr = i[1]
		execute execStr
	)
	
	reparentRig
)

fn queryBoneLength =
(
	output_name = getSaveFileName caption:"Bone Length file" types:"BoneLengths (*.len)|*.len|All Files (*.*)|*.*|"
	
	if output_name != undefined then 
	(		
		output_file = createfile output_name
				
-- 		for i = 1 to boneArray.count do
		for i = 1 to parentArray.count do
		(
			thisBone = getNodeByname parentArray[i]
			thisChild = getNodeByName childArray[i]
			if thisBone != undefined do
			(
				if thisChild != undefined do
				(
	-- 				thisBoneLength = (thisBone.length as float)
					thisBoneLength = ((distance thisBone thisChild) as float)
					existingPos = in coordsys parent thisBone.position
					eY = (existingPos[2] as float)
					eZ = (existingPos[3] as float)
					boneLengthStr = ("$"+thisBone.name+".length = "+(thisBoneLength as string))
						boneParentStr = ("in coordsys parent $"+thisBone.name+".position = ["+(thisBoneLength as string)+","+(eY as string)+","+(eZ as string)+"]")
					append boneLengthList boneLengthStr
					
					format ((boneLengthStr as string)+"\n") to:output_file
					format ((boneParentStr as string)+"\n") to:output_file
				)
			)
		)
		close output_file
	)
	print ("Bone Lengths saved")
)


if ((lengthToolsGUI != undefined) and (lengthToolsGUI.isDisplayed)) do
	(destroyDialog lengthToolsGUI)

rollout lengthToolsGUI "Spotting Tools"
(
	button btnSaveLengths "Save Lengths" width:110
	button btnSetLengths "Set Lengths" width:110
	
	

-- 	on btnImagePlanes pressed do
-- 	(
-- 		filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Rigging_tools\\giant\\skeletonSpotImagePlaneSetup.ms")	
-- 	)

-- 	on btnMirrorSel pressed do
-- 	(
-- 		filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Rigging_tools\\giant\\mirrorMarkers.ms")	
-- 	)
-- 	
	on btnSaveLengths pressed do
	(
		queryBoneLength()
	)

	on btnSetLengths pressed do
	(
		setBoneLengths()
	)	
)

CreateDialog lengthToolsGUI width:125 pos:[1450, 100] 