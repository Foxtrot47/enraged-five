--mirrorSkelMarker.ms
--tool to mirror positioning of constraint skeleton from left to right or right to left
--Matt Rennie
--Jan 2011


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
undo "Mirror Markers" on
(
	selArray = selection as array

	for obj = 1 to selArray.count do
	(
		currObjName = selArray[obj].name
		subsName = (substring currObjName 1 6)
		
		if subsName == "Marker" do --force to only work on markers
		(
			currPos = in coordsys world selArray[obj].position
			mirrorPos = [(currPos[1] * -1), currPos[2], currPos[3]]
			leftorRight = (substring currObjName 8 1)
			if leftOrRight == "R" then
			(
				side = "L"
			)
			else
			(
				side = "R"
			)
			mirrorNodeName = ((substring currObjName 1 7)+side+(substring currObjName 9 200))
			
				mirroredNode = getNodeByname mirrorNodeName 
				if mirroredNode != undefined do
				(
					print ("mirrring from "+currObjName+" to "+mirrorNodeName)
					in coordsys world mirroredNode.position = mirrorPos
				)
				
		)
	)
)