boneArray = #(

"SKEL_L_Finger42",
"SKEL_L_Finger41",
"SKEL_L_Finger40",	
"SKEL_L_Finger32",
"SKEL_L_Finger31",
"SKEL_L_Finger30",	
"SKEL_L_Finger22",
"SKEL_L_Finger21",
"SKEL_L_Finger20",	
"SKEL_L_Finger12",
"SKEL_L_Finger11",
"SKEL_L_Finger10",	
"SKEL_L_Finger02",
"SKEL_L_Finger01",
"SKEL_L_Finger00",	
"SKEL_L_Hand",	
"SKEL_L_Forearm",	
"SKEL_L_UpperArm",	
"SKEL_L_Clavicle",	
"SKEL_L_Toe0",	
"SKEL_L_Foot",
"SKEL_L_Calf",
"SKEL_L_Thigh",

"SKEL_Head",
"SKEL_Neck_1",
"SKEL_Spine3",
"SKEL_Spine2",
"SKEL_Spine1",
"SKEL_Spine0",
"SKEL_Spine_Root",
	
"SKEL_R_Finger42",
"SKEL_R_Finger41",
"SKEL_R_Finger40",	
"SKEL_R_Finger32",
"SKEL_R_Finger31",
"SKEL_R_Finger30",	
"SKEL_R_Finger22",
"SKEL_R_Finger21",
"SKEL_R_Finger20",	
"SKEL_R_Finger12",
"SKEL_R_Finger11",
"SKEL_R_Finger10",	
"SKEL_R_Finger02",
"SKEL_R_Finger01",
"SKEL_R_Finger00",	
"SKEL_R_Hand",	
"SKEL_R_Forearm",	
"SKEL_R_UpperArm",	
"SKEL_R_Clavicle",	
"SKEL_R_Toe0",	
"SKEL_R_Foot",
"SKEL_R_Calf",
"SKEL_R_Thigh"
)

fn storePose =
(
	output_name = getSaveFileName caption:"Pose file" types:"PoseFile (*.pos)|*.pos|All Files (*.*)|*.*|"
	
	if output_name != undefined then 
	(		
		output_file = createfile output_name
				
-- 		for i = 1 to boneArray.count do
		for i = 1 to boneArray.count do
		(
			thisBone = getNodeByname boneArray[i]
			if thisBone != undefined do
			(
				thisBoneTrans = ("$"+thisBone.name+".transform = "+(thisBone.transform as string))
					
					format ((thisBoneTrans as string)+"\n") to:output_file
			)
		)
		close output_file
	)
	print ("Pose file saved.")	
)

storePose()