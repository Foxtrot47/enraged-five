handBones = #(
	"HAND_SKEL_R_Finger00",
	"HAND_SKEL_R_Finger01",
	"HAND_SKEL_R_Finger02", 
	"HAND_SKEL_R_Finger0_NUB",
	"HAND_SKEL_R_Finger10",
	"HAND_SKEL_R_Finger11",
	"HAND_SKEL_R_Finger12",
	"HAND_SKEL_R_Finger1_NUB",
	"HAND_SKEL_R_Finger20",
	"HAND_SKEL_R_Finger21",
	"HAND_SKEL_R_Finger22",
	"HAND_SKEL_R_Finger2_NUB",
	"HAND_SKEL_R_Finger30",
	"HAND_SKEL_R_Finger31",
	"HAND_SKEL_R_Finger32",
	"HAND_SKEL_R_Finger3_NUB",
	"HAND_SKEL_R_Finger40",
	"HAND_SKEL_R_Finger41",
	"HAND_SKEL_R_Finger42",
	"HAND_SKEL_R_Finger4_NUB",
	"HAND_SKEL_L_Finger00",
	"HAND_SKEL_L_Finger01",
	"HAND_SKEL_L_Finger02",
	"HAND_SKEL_L_Finger0_NUB",
	"HAND_SKEL_L_Finger10",
	"HAND_SKEL_L_Finger11",
	"HAND_SKEL_L_Finger12",
	"HAND_SKEL_L_Finger1_NUB",
	"HAND_SKEL_L_Finger20",
	"HAND_SKEL_L_Finger21",
	"HAND_SKEL_L_Finger22",
	"HAND_SKEL_L_Finger2_NUB",
	"HAND_SKEL_L_Finger30",
	"HAND_SKEL_L_Finger31",
	"HAND_SKEL_L_Finger32",
	"HAND_SKEL_L_Finger3_NUB",
	"HAND_SKEL_L_Finger40",
	"HAND_SKEL_L_Finger41",
	"HAND_SKEL_L_Finger42",
	"HAND_SKEL_L_Finger4_NUB"
	)
	
nodesToDelete = #(
	"HAND_RB_L_ForeArmRoll",
	"HAND_SKEL_R_Forearm",
	"HAND_SKEL_R_Hand",
	"HAND_SKEL_R_Finger00",
	"HAND_SKEL_R_Finger01",
	"HAND_SKEL_R_Finger02",
	"HAND_SKEL_R_Finger0_NUB",
	"HAND_SKEL_R_Finger10",
	"HAND_SKEL_R_Finger11",
	"HAND_SKEL_R_Finger12",
	"HAND_SKEL_R_Finger1_NUB",
	"HAND_SKEL_R_Finger20",
	"HAND_SKEL_R_Finger21",
	"HAND_SKEL_R_Finger22",
	"HAND_SKEL_R_Finger2_NUB",
	"HAND_SKEL_R_Finger30",
	"HAND_SKEL_R_Finger31",
	"HAND_SKEL_R_Finger32",
	"HAND_SKEL_R_Finger3_NUB",
	"HAND_SKEL_R_Finger40",
	"HAND_SKEL_R_Finger41",
	"HAND_SKEL_R_Finger42",
	"HAND_SKEL_R_Finger4_NUB",
	"HAND_SKEL_L_Finger42",
	"HAND_RB_R_ForeArmRoll", 
	"HAND_SKEL_L_Finger41",
	"HAND_SKEL_L_Forearm",
	"HAND_SKEL_L_Hand",
	"HAND_SKEL_L_Finger00",
	"HAND_SKEL_L_Finger01",
	"HAND_SKEL_L_Finger02",
	"HAND_SKEL_L_Finger0_NUB",
	"HAND_SKEL_L_Finger10",
	"HAND_SKEL_L_Finger11",
	"HAND_SKEL_L_Finger12",
	"HAND_SKEL_L_Finger1_NUB",
	"HAND_SKEL_L_Finger20",
	"HAND_SKEL_L_Finger21",
	"HAND_SKEL_L_Finger22",
	"HAND_SKEL_L_Finger2_NUB",
	"HAND_SKEL_L_Finger30",
	"HAND_SKEL_L_Finger31",
	"HAND_SKEL_L_Finger32",
	"HAND_SKEL_L_Finger3_NUB",
	"HAND_SKEL_L_Finger40",
	"HAND_SKEL_L_Finger4_NUB",
	"HAND_PH_R_Hand",
	"HAND_ROOT_LHand",
	"HAND_PH_L_Hand",
	"HAND_ROOT_RHand"
	)
	
markerNodeArray = #(
	"Marker_R_Finger00",
	"Marker_R_Finger01",
	"Marker_R_Finger02",
	"Marker_R_Finger0Nub",
	"Marker_R_Finger10",
	"Marker_R_Finger11",
	"Marker_R_Finger12",
	"Marker_R_Finger1Nub",
	"Marker_R_Finger20",
	"Marker_R_Finger21",
	"Marker_R_Finger22",
	"Marker_R_Finger2Nub",
	"Marker_R_Finger30",
	"Marker_R_Finger31",
	"Marker_R_Finger32",
	"Marker_R_Finger3Nub",
	"Marker_R_Finger40",
	"Marker_R_Finger41",
	"Marker_R_Finger42",
	"Marker_R_Finger4Nub",
	"Marker_L_Finger00",
	"Marker_L_Finger01",
	"Marker_L_Finger02",
	"Marker_L_Finger0Nub",
	"Marker_L_Finger10",
	"Marker_L_Finger11",
	"Marker_L_Finger12",
	"Marker_L_Finger1Nub",
	"Marker_L_Finger20",
	"Marker_L_Finger21",
	"Marker_L_Finger22",
	"Marker_L_Finger2Nub",
	"Marker_L_Finger30",
	"Marker_L_Finger31",
	"Marker_L_Finger32",
	"Marker_L_Finger3Nub",
	"Marker_L_Finger40",
	"Marker_L_Finger41",
	"Marker_L_Finger42",
	"Marker_L_Finger4Nub"
	)
	
	
fn handSetup = 
(
	for b = 1 to handBones.count do
	(
		handBone = getNodeByName handBones[b]
		marker = getNodeByName markerNodeArray[b]
		if handBone != undefined then
		(
			if marker != undefined then
			(
				print ("Aligning "+marker.name+" with "+handBone.name)
				hP = in coordsys world handBone.position
				in coordsys world marker.position = hP
			)
			else
			(
				print ("Couldn't find "+markerNodeArray[b])
			)
		)
		else
		(
			print ("Couldn't find "+handBones[b])
		)
	)
	
	--now collapse the stack on hand bones
	
	collapseStack $Uppr_000_U
	collapseStack $Lowr_000_U

	--now remove hand shit
	for d = 1 to nodesToDelete.count do
	(
		clearSelection()
		toDel = getNodeByName nodesToDelete[d]
		if toDel != undefined do
		(
			print ("Deleting "+nodesToDelete[d])			
			select toDel
			delete selection
		)
	)
)

handSetup()