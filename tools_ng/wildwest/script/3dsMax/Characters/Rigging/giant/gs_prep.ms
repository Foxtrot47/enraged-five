--script to reweight existing characters so we can remove roll bones etc
--so we can prep as a GS character





---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
bonesToKeep = #( --array of all bones we want to keep
	"SKEL_ROOT",
	"SKEL_Pelvis",
	"SKEL_L_Thigh",
	"SKEL_L_Calf",
	"SKEL_L_Foot",
	"SKEL_L_Toe0",
	"SKEL_R_Thigh",
	"SKEL_R_Calf",
	"SKEL_R_Foot",
	"SKEL_R_Toe0",
	"SKEL_Spine_Root",
	"SKEL_Spine0",
	"SKEL_Spine1",
	"SKEL_Spine2",
	"SKEL_Spine3",
	"SKEL_L_Clavicle",
	"SKEL_L_UpperArm",
	"SKEL_L_Forearm",
	"SKEL_L_Hand",
	"SKEL_L_Finger00",
	"SKEL_L_Finger01",
	"SKEL_L_Finger02",
	"SKEL_L_Finger10",
	"SKEL_L_Finger11",
	"SKEL_L_Finger12",
	"SKEL_L_Finger20",
	"SKEL_L_Finger21",
	"SKEL_L_Finger22",
	"SKEL_L_Finger30",
	"SKEL_L_Finger31",
	"SKEL_L_Finger32",
	"SKEL_L_Finger40",
	"SKEL_L_Finger41",
	"SKEL_L_Finger42",
	"SKEL_R_Clavicle",
	"SKEL_R_UpperArm",
	"SKEL_R_Forearm",
	"SKEL_R_Hand",
	"SKEL_R_Finger00",
	"SKEL_R_Finger01",
	"SKEL_R_Finger02",
	"SKEL_R_Finger10",
	"SKEL_R_Finger11",
	"SKEL_R_Finger12",
	"SKEL_R_Finger20",
	"SKEL_R_Finger21",
	"SKEL_R_Finger22",
	"SKEL_R_Finger30",
	"SKEL_R_Finger31",
	"SKEL_R_Finger32",
	"SKEL_R_Finger40",
	"SKEL_R_Finger41",
	"SKEL_R_Finger42",
	"SKEL_Neck_1",
	"SKEL_Head",
	"mover"
)

nodesToKeep = #( --used by the cleanup process to help select skeleton nodes to keep
	"Dummy01",
"mover",
"SKEL_ROOT",
"SKEL_Pelvis",
"SKEL_R_Thigh",
"SKEL_R_Calf",
"SKEL_R_Foot",
"SKEL_R_Toe0",
"SKEL_L_Thigh",
"SKEL_L_Calf",
"SKEL_L_Foot",
"SKEL_L_Toe0",
-- "RB_L_ThighRoll",
-- "RB_R_ThighRoll",
"SKEL_Spine_Root",
"SKEL_Spine0",
"SKEL_Spine1",
"SKEL_Spine2",
"SKEL_Spine3",
"SKEL_L_Clavicle",
"SKEL_L_UpperArm",
"SKEL_L_Forearm",
"SKEL_L_Hand",
"SKEL_L_Finger00",
"SKEL_L_Finger01",
"SKEL_L_Finger02",
"SKEL_L_Finger10",
"SKEL_L_Finger11",
"SKEL_L_Finger12",
"SKEL_L_Finger20",
"SKEL_L_Finger21",
"SKEL_L_Finger22",
"SKEL_L_Finger30",
"SKEL_L_Finger31",
"SKEL_L_Finger32",
"SKEL_L_Finger40",
"SKEL_L_Finger41",
"SKEL_L_Finger42",
"PH_L_Hand",
-- "RB_L_ForeArmRoll",
-- "RB_L_ArmRoll",
"SKEL_R_Clavicle",
"SKEL_R_UpperArm",
"SKEL_R_Forearm",
"SKEL_R_Hand",
"SKEL_R_Finger00",
"SKEL_R_Finger01",
"SKEL_R_Finger02",
"SKEL_R_Finger10",
"SKEL_R_Finger11",
"SKEL_R_Finger12",
"SKEL_R_Finger20",
"SKEL_R_Finger21",
"SKEL_R_Finger22",
"SKEL_R_Finger30",
"SKEL_R_Finger31",
"SKEL_R_Finger32",
"SKEL_R_Finger40",
"SKEL_R_Finger41",
"SKEL_R_Finger42",
"PH_R_Hand",
-- "RB_R_ForeArmRoll",
-- "RB_R_ArmRoll",
"SKEL_Neck_1",
"SKEL_Head"--,
-- "RB_Neck_1"
)

geoPrefixes = #( --used by the cleanup process to help select geo by wildcard
	"Accs_",
	"Decl_",
	"Feet_",
	"Hair_",
	"Hand_",
	"Head_",
	"Lowr_",
	"P_eye",
	"Task_",
	"Uppr_",
	
	"accs_",
	"decl_",
	"feet_",
	"hair_",
	"hand_",
	"head_",
	"lowr_",
	"p_eye",
	"task_",
	"uppr_"
)


fromToBones = #(--this will be a dynamically generated 2d array containing the original bone (such as a roll bone) and what its weight will be moved to
 	#(), --this will contain the initial bone
 	#() --this will contain the bone to move weights to
	) 

	
skinModBones = #()
vertSkinValues = #()
boneNames = #()
debugPrintVal = true --toggle this true/false to enable/disable print statements
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	
fn removeBadBones = --this will remove all none core bones
(
	
	keepThis = #()
	--need to do something that will look for wildcards based off uppr_ and lowr_ etc etc
	geoNode = undefined
	

		geoNode = undefined
		for o in objects do
		(
			for g = 1 to geoPrefixes.count do
			(
				if (substring o.name 1 5) == geoPrefixes[g] do
				(
					debugprint ("substring = "+(substring o.name 1 5) +" geoPrefixes = "+geoPrefixes[g])
					appendifunique nodesToKeep o.name
					if geoNode == undefined do
					(
						geoNode = o
						appendIfUnique keepThis geoNode 
						debugprint ("adding geo "+geoNode.name+" to keepThis array")					
					)
				)
			)
		)

	
	if geoNode != undefined do
	(
		geoNodeParent = geoNode.parent
	)
	
	debugprint ("nodesToKeep.count = "+(nodesToKeep.count as string))
	for i = 1 to nodesToKeep.count do
	(
		
		thisNode = getNodeByName nodesToKeep[i]
		if thisNode != undefined do
		(
			appendIfUnique keepThis thisNode 
			debugprint ("adding "+thisNode.name+" to array keepThisArray")
		)
	)
-- 	
	allobjs = objects as array
-- 		found = 0
		for i = 1 to keepThis.count do
		(
			--just find items in allobjs which match keepThis then if match foudn remove from the array then at the end we delete whats in toTrash
			found = findItem allObjs keepThis[i]
			if found != 0 do
			(
				deleteItem allObjs found
				debugprint ("removing "+keepThis[i].name+" from allObjs")
			)
		)
		
		if geoNodeParent != undefined do
		(
			found = findItem allObjs geoNodeParent --this makes sure we keep the geoNode dummy
			if found != 0 do
			(
				deleteItem allObjs found
			)
		)
		debugprint "--------------------------------------------------------------------------------------------------------------------------------"
-- 		print "remainder = "
-- 		print allObjs
		
		--now delete the allobjs objects
		for o = 1 to allObjs.count do
		(
			debugprint ("Deleting "+allObjs[o].name)
		)
		delete allobjs	
	
		--this section will do a final check to see if there are any nodes which have no parents or children and delete them
		noParents = #()
		killThese = #()
		
		for o in objects do
		(
			if o.parent == undefined do
			(
				appendIfUnique noParents o
			)
		)
			
		for i = 1 to noParents.count do
		(
			if noParents[i].children.count == 0 do
			(
				appendIfUnique killThese noParents[i]
			)
		)			
		
		debugprint killThese
		delete killThese
		
	print "Skeleton node removal complete."	
-- 	messagebox "Skeleton node removal complete."
)

--update the skinning on the LOD mesh using our filtered vertSkinValues array
fn updateLODSkin theMesh =
(
	meshName = theMesh.name
	
	for vertNum = 1 to vertSkinValues.count do
	(
		tempBoneArray = #()
		tempWeightArray = #()
		
		for affectingBones=1 to vertSkinValues[vertNum].count do
		(
			boneID = findItem boneNames vertSkinValues[vertNum][affectingBones][2]
			append tempBoneArray boneID
			append tempWeightArray vertSkinValues[vertNum][affectingBones][3]
		)
		
		theVert = vertNum
		theSkin = theMesh.modifiers[#skin]
		boneArray = tempBoneArray
		weightArray = tempWeightArray
		
		--skinOps.unNormalizeVertex theskin theVert true
		skinOps.ReplaceVertexWeights theskin theVert boneArray weightArray
		--skinOps.unNormalizeVertex theskin theVert false
	)

-- 	print ("skinning updated on " + theMesh.name)
)--end updateLODSkin

--get the skinned verts for a mesh (theMesh) and add them to the vertSkinValues array
fn buildSkinDataArray theMesh =
(	
	debugprint ("Getting vertex skin weights from " + theMesh.name)
	vertSkinValues = #()
	
	vertCount = skinOps.getNumberVertices theMesh.modifiers[#skin]
	
	for x=1 to vertCount do
	(
		vertArray = #()
		BNumber = skinOps.getVertexWeightCount theMesh.modifiers[#skin] x
		
		for i = 1 to BNumber do
		(
			boneID = skinOps.getVertexWeightBoneId theMesh.modifiers[#skin] x i
			boneName = skinOps.GetBoneName theMesh.modifiers[#skin] boneID 0
			boneWeight = skinOps.getVertexWeight theMesh.modifiers[#skin] x i

			tempArray = #(boneID, boneName, boneWeight)
			append vertArray tempArray
		)
		append vertSkinValues vertArray
	)
-- 	print ("vertex skin weights grabbed from " + theMesh.name)
	
)--end vertSkin

--check the vert weights for the bone we want to remove (fromBone).  If found, we add the weight on the the parent bone (toBone).
fn replaceWeights fromBone toBone  =
(

	debugprint ("Moving "+(fromBone as string)+" to "+(toBone as string))
	toBoneId = findItem skinModBones (getNodeByName toBone)
	
-- 	createDialog progBar width:300 Height: 30
	-- loop through all the verts in the object
	for vertNoArray=1 to vertSkinValues.count do --for every vert do
	(
		iVal = findItem vertSkinValues vertNoArray
-- 		progBar.prog.value = (100.*iVal/vertSkinValues.count)
		-- Get the influence data for each vert
		currentVertData = vertSkinValues[vertNoArray]
		weightToMove = 0
		
		for vertBoneArray=1 to currentVertData.count do --for every weight do
		(
			if fromBone == currentVertData[vertBoneArray][2] do
			(
				addedWeight = false
				weightToMove = currentVertData[vertBoneArray][3]
			
				--lets check the other weights on this vert for a match with our parent bone
				for recheckVertBoneArray=1 to currentVertData.count do
				(
					if currentVertData[recheckVertBoneArray][2] == toBone do
					(
						-- If we have a match, then the destination bone already exists, create a composite weight
						weightToAdd = currentVertData[recheckVertBoneArray][3]
						weightToWrite = weightToAdd + weightToMove
						
						currentVertData[recheckVertBoneArray][3] = weightToWrite -- set parent to new weight
						currentVertData[vertBoneArray][3] = 0 -- clear child weight
						
						addedWeight = true  -- set the flag to say we've created an updated weight set.
					)--end add weight loop
				)
				
				-- what if we didn't find a good parent bone?
				-- we need to kill the dirty child and repalce it with the correct parent
				if addedWeight != true do
				(
					currentVertData[vertBoneArray][2] = toBone					
					addedWeight = true
				)--end new bone and weight loop
			)--end found toBone loop
		)
	)
	gc()
-- 	destroyDialog progBar
)--end replaceWeights




fn prepBoneNames geo =
(
	boneNames = #()
	
	selBoneCount = skinOps.GetNumberBones geo.modifiers[#Skin]

	--get the bone name order
	for sb=1 to selBoneCount do
	(
		singleBoneName = skinOps.GetBoneName  geo.modifiers[#Skin] sb 0
		append boneNames singleBoneName
	)
)

fn findBoneRecursive currentBone  =  -- Find a bone recursively if needed, iterate up the parent tree to find a bone which exists within the bonesToKeep array
(
	foundResult = findItem bonesToKeep currentBone.Name
	if foundResult != 0 then
	(
		-- found the bone in the bonesToKeep array so return it
		return currentBone
		debugprint (currentBone.Name+" doesnt need changing")
	)
	else
	(
		-- did not find the bone, so search for its parent
		return findBoneRecursive currentBone.parent
		debugprint ("found that")
	)
)


fn checkSkinBones = --find the bones in skin mod and see if they are toi be kept and if not record what their replacement will be
(
	--clearArray
	fromToBones = #(--this will be a dynamically generated 2d array containing the original bone (such as a roll bone) and what its weight will be moved to
 	#(), --this will contain the initial bone
 	#() --this will contain the bone to move weights to
	) 
	
	--endClearArrays
	
	
	print "starting checkSkinBones"
	for i = 1 to skinModBones.count do --now add data of original bone plus its final bone into a paired array
	(
		myBone = findBoneRecursive skinModBones[i] 
-- 		print myBone.name as string -- This is the upmost parent which is in the bonesToKeep array
		
-- 		print ("Appending "+sceneBones[i].name+" into fromToBones ["+(i as string)+"][1] array")
		append fromToBones[1] skinModBones[i].name --put in the original bone
		
-- 		print ("Appending "+myBone.name+" into fromToBones ["+(i as string)+"][2] array")
		
		if skinModBones[i].name == "RB_L_ThighRoll" then
		(
			append fromToBones[2] "SKEL_L_Thigh"
		)
		else
		(
			if skinModBones[i].name == "RB_R_ThighRoll" then
			(
				append fromToBones[2] "SKEL_R_Thigh"
			)
			else
			(
				append fromToBones[2] myBone.name --put in the bone we need to move the weight to
			)
		)
	)
	debugprint "fromToBones array built"
)

fn buildSkinModBoneList geo = --query all bones in the skin modifier and put them into a array so we can loop trhu them
(
	max modify mode
	skinModBones = #() --ensure its empty
				
	noOfBones = skinops.getNumberBones geo.modifiers[#Skin]
	
	for b = 1 to noOfBones do --build the array of bones in the skin modifier
	(
		boneName = skinOps.GetBoneName geo.modifiers[#Skin] b 1
		realBoneObj = getnodeByName boneName
		appendIfUnique skinModBones realBoneObj
	)
	print "buildSkinModBoneList done."
)

fn runReplaceWeight geo = --using the fromToBOnes array call Stews replaceWeight script
(
	print ("Running replaceweight on "+geo.name)
	tmpSel = selection as array

	
	-- Use selection.count here Matt
	-- selection[1] will also return the first item in a selection
	
	
	if tmpSel.count == 1 then
	(
		if geo.modifiers[#Skin] != undefined then
		(
			max modify mode
			modPanel.setCurrentObject geo.modifiers[#Skin]
			
			buildSkinDataArray geo --build an array of all the verts in the mesh
			buildSkinModBoneList geo --generate an array of all bones in the skin modifier
			checkSkinBones()
			
			
			debugprint ("fromToBones count = "+(fromToBones[1].count as string))
			for i = 1 to fromToBones[1].count do
			(
				-- compare the data in both arrays, if they do not match we need to flip.
				if fromToBones[1][i] != fromToBones[2][i] then
				(
									
					replaceWeights fromToBones[1][i] fromToBones[2][i] --create arrays of the new weighting data
					prepBoneNames geo 
					updateLODSkin geo --now we have the arrays setup do the actual skin weighting
				)
			)
			debugprint  ("Weight switching complete on "+geo.name+".")
		)
		else
		(
			messagebox ("WARNING! Selection "+geo.name+" has no skin modifier!") beep:true
		)
	)
	else
	(
		messagebox ("WARNING! Please run on just one mesh") beep:true
	)
)	




	


fn gs_prep = 
(
	clearListener()
-- 	skinnedMeshes = selection as array
	skinnedMeshes = #()
	for o in objects do
	(
		if o.modifiers[#Skin] != undefined do
		(
			appendIfunique skinnedMeshes o
		)
	)
	
-- 	if selection.count != 0 then
	if skinnedMeshes.count != 0 then
	(
		max modify mode
		for sm = 1 to skinnedMeshes.count do
		(
			fromToBones = #(--this will be a dynamically generated 2d array containing the original bone (such as a roll bone) and what its weight will be moved to
				#(), --this will contain the initial bone
				#() --this will contain the bone to move weights to
				) 

				
			skinModBones = #()
			vertSkinValues = #()
			boneNames = #()
			
			
			
			select skinnedMeshes[sm]
			if skinnedMeshes[sm].modifiers[#Skin] != undefined do
			(
				buildSkinModBoneList skinnedMeshes[sm]
				checkSkinBones()
				
				runReplaceWeight skinnedMeshes[sm]
			)
		)
		removeBadBones()
		messagebox ("GS Prep done.") beep:true
	)
	else
	(
		messagebox "Please run with skinned objects selected" beep:true
	)
)




---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


if ((gs_prepGUI != undefined) and (gs_prepGUI.isDisplayed)) do
	(destroyDialog gs_prepGUI)

rollout gs_prepGUI "Gigantique"
(
	button btnGS_Prep "GS Prep" width:110 height:40

	checkBox chkDebugPrnt "DebugPrint" checked:false tooltip:"Check to enable debug print output."

	on gs_prepGUI open do
	(
		chkDebugPrnt.state = false
		debugPrintVal = false
		print ("Debug printing defaulting to: "+(debugPrintVal as string))
	)
	
	on chkDebugPrnt changed theState do
	(
		if chkDebugPrnt.state == false then
		(
			debugPrintVal = false
			print "Debug printing disabled."
		)
		else
		(
			debugPrintVal = true
			print "Debug printing enabled."
		)
	)	
	on btnGS_Prep pressed do
	(
		gs_prep()
		clearSelection()
	)
)

CreateDialog gs_prepGUI width:125 pos:[1450, 100] 