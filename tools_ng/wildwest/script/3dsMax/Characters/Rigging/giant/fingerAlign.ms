--ensures that fingers are all aligned straight.

fingerArrays = #(
#("SKEL_L_Finger00","SKEL_L_Finger01","SKEL_L_Finger02"),
#("SKEL_L_Finger10","SKEL_L_Finger11","SKEL_L_Finger12"),
#("SKEL_L_Finger20","SKEL_L_Finger21","SKEL_L_Finger22"),
#("SKEL_L_Finger30","SKEL_L_Finger31","SKEL_L_Finger32"),
#("SKEL_L_Finger40","SKEL_L_Finger41","SKEL_L_Finger42"),
#("SKEL_R_Finger00","SKEL_R_Finger01","SKEL_R_Finger02"),
#("SKEL_R_Finger10","SKEL_R_Finger11","SKEL_R_Finger12"),
#("SKEL_R_Finger20","SKEL_R_Finger21","SKEL_R_Finger22"),
#("SKEL_R_Finger30","SKEL_R_Finger31","SKEL_R_Finger32"),
#("SKEL_R_Finger40","SKEL_R_Finger41","SKEL_R_Finger42")
	)
	
fn sortFingers = 
(
	for fa = 1 to fingerArrays.count do 
	(
		finger1 = getNodeByName fingerArrays[fa][1]
		finger2 = getNodeByName fingerArrays[fa][2]
		finger3 = getNodeByName fingerArrays[fa][3]
		
		print ("setting rot on "+finger2.name)
		in coordsys parent finger2.rotation.controller.Frozen_Rotation.X_Rotation  = 0
		in coordsys parent finger2.rotation.controller.Frozen_Rotation.Y_Rotation  = 0
		print ("setting rot on "+finger3.name)	
		in coordsys parent finger3.rotation.controller.Frozen_Rotation.X_Rotation = 0
		in coordsys parent finger3.rotation.controller.Frozen_Rotation.Y_Rotation = 0
	)
)

sortFingers()