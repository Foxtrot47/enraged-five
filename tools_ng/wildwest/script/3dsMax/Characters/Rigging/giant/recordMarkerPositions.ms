markArray = #(
$Marker_Pelvis, 
$Marker_PelvisEND,
$Marker_R_Thigh, 
$Marker_R_Calf, 
$Marker_R_Foot, 
$Marker_R_Toe0, 
$Marker_R_Toe0Nub, 
$Marker_L_Thigh, 
$Marker_L_Calf, 
$Marker_L_Foot, 
$Marker_L_Toe0, 
$Marker_L_Toe0Nub,
$Marker_Spine0, 
$Marker_Spine1, 
$Marker_Spine2, 
$Marker_Spine3, 
$Marker_Neck1, 
$Marker_Head, 
$Marker_HeadNub, 
$Marker_R_Clavicle, 
$Marker_R_UpperArm, 
$Marker_R_Forearm, 
$Marker_R_Hand, 
$Marker_R_HandEND, 
$Marker_R_Finger00, 
$Marker_R_Finger01, 
$Marker_R_Finger02, 
$Marker_R_Finger0Nub, 
$Marker_R_Finger40, 
$Marker_R_Finger41, 
$Marker_R_Finger42, 
$Marker_R_Finger4Nub, 
$Marker_R_Finger30, 
$Marker_R_Finger31, 
$Marker_R_Finger32, 
$Marker_R_Finger3Nub, 
$Marker_R_Finger20, 
$Marker_R_Finger21, 
$Marker_R_Finger22, 
$Marker_R_Finger2Nub, 
$Marker_R_Finger10, 
$Marker_R_Finger11, 
$Marker_R_Finger12, 
$Marker_R_Finger1Nub, 
$Marker_L_Clavicle, 
$Marker_L_UpperArm, 
$Marker_L_Forearm, 
$Marker_L_Hand, 
$Marker_L_HandEND, 
$Marker_L_Finger10, 
$Marker_L_Finger11, 
$Marker_L_Finger12, 
$Marker_L_Finger1Nub, 
$Marker_L_Finger20, 
$Marker_L_Finger21, 
$Marker_L_Finger22, 
$Marker_L_Finger2Nub, 
$Marker_L_Finger00, 
$Marker_L_Finger01, 
$Marker_L_Finger02, 
$Marker_L_Finger0Nub, 
$Marker_L_Finger30, 
$Marker_L_Finger31, 
$Marker_L_Finger32, 
$Marker_L_Finger3Nub, 
$Marker_L_Finger40, 
$Marker_L_Finger41, 
$Marker_L_Finger42, 
$Marker_L_Finger4Nub
)

fn genMarkerPositions = 
(
	clearListener()
	for i = 1 to markArray.count do
	(
		currMark = markArray[i]
		markPos = in coordsys world currMark.position
		print ("in coordsys world $"+currMark.name+".position = "+(markPos as string))
	)
)

genMarkerPositions()