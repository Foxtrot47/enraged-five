struct rigBoneStruct (rb_bone, rb_Radius, rb_Length, rb_Trans)

fn genVols = 
(
	for obj in objects do
	(
		if (substring obj.name 1 9) == "splitVol_" do
		(
			delete obj
		)
	)
	
	volObjects = #()
	
	rigBoneListStruct = #( --array of all skeleton bones which we want to split mesh for
	( rigBoneStruct rb_bone:$SKEL_Pelvis  rb_Radius: 0.25 rb_Length:($SKEL_Pelvis.length * 2) rb_Trans:undefined ), 
-- 	( rigBoneStruct rb_bone:$SKEL_Spine0  rb_Radius: 0.25 rb_Length:($SKEL_Spine0.length * 1.4) rb_Trans:(matrix3 [-1,-0.000168886,1.60497e-005] [0.000169019,-0.999968,0.00799049] [1.47042e-005,0.00799066,0.999969] [1.22934e-007,0.0171712,0.972031]) ), 
( rigBoneStruct rb_bone:$SKEL_Spine0  rb_Radius: 0.25 rb_Length:((distance $SKEL_Spine0 $SKEL_Spine1)) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_Spine1  rb_Radius: 0.25 rb_Length:($SKEL_Spine1.length * 1.4) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_Spine2  rb_Radius: 0.22 rb_Length:($SKEL_Spine2.length * 1.4) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_Spine3  rb_Radius: 0.24 rb_Length:($SKEL_Spine3.length * 0.8) rb_Trans:undefined ), 

-- 	( rigBoneStruct rb_bone:$SKEL_Neck_1  rb_Radius: 0.09 rb_Length:$SKEL_Neck_1.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_Head  rb_Radius: 0.15 rb_Length:($SKEL_Head.length * 2) rb_Trans:(matrix3 [-1,0.000145016,-0.000328264] [-5.81659e-005,-0.96816,-0.250334] [-0.000354256,-0.250334,0.96816] [-0.000112729,0.0139014,1.59917]) ),	
	
	( rigBoneStruct rb_bone:$SKEL_L_Thigh  rb_Radius: 0.14 rb_Length:$SKEL_L_Thigh.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Calf  rb_Radius: 0.12 rb_Length:$SKEL_L_Calf.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Foot  rb_Radius: 0.1 rb_Length:($SKEL_L_Foot.length * 1.5) rb_Trans:undefined ), --(matrix3 [0.999234,0.0391548,3.82012e-005] [-0.00134613,0.0333797,0.999442] [0.0391316,-0.998676,0.0334069] [0.155156,0.145386,0.0342977]) ), 
	( rigBoneStruct rb_bone:$SKEL_L_Toe0  rb_Radius: 0.1 rb_Length:0.25 rb_Trans:undefined ),
	
	( rigBoneStruct rb_bone:$SKEL_R_Thigh  rb_Radius: 0.14 rb_Length:$SKEL_R_Thigh.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Calf  rb_Radius: 0.12 rb_Length:$SKEL_R_Calf.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Foot  rb_Radius: 0.1 rb_Length:($SKEL_R_Foot.length * 1.5) rb_Trans:(matrix3 [0.999233,0.0391547,3.80804e-005] [-0.00134612,0.0333794,0.999442] [0.0391315,-0.998676,0.0334067] [-0.155,0.145386,0.0342977]) ), 
	( rigBoneStruct rb_bone:$SKEL_R_Toe0  rb_Radius: 0.1 rb_Length:0.25 rb_Trans:undefined ), 
	
-- 	( rigBoneStruct rb_bone:$SKEL_Spine_Root  rb_Radius: 1 rb_Length: .length  rb_Trans:undefined ), 

	( rigBoneStruct rb_bone:$SKEL_L_Clavicle  rb_Radius: 0.15 rb_Length:$SKEL_L_Clavicle.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_UpperArm  rb_Radius: 0.1 rb_Length:$SKEL_L_UpperArm.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Forearm  rb_Radius: 0.1 rb_Length:($SKEL_L_Forearm.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Hand  rb_Radius: 0.05 rb_Length:$SKEL_L_Hand.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger00  rb_Radius: 0.018 rb_Length:($SKEL_L_Finger00.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger01  rb_Radius: 0.018 rb_Length:($SKEL_L_Finger01.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger02  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger02.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger10  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger10.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger11  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger11.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger12  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger12.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger20  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger20.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger21  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger21.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger22  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger22.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger30  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger30.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger31  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger31.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger32  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger32.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger40  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger40.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger41  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger41.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger42  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger42.length * 1.1) rb_Trans:undefined ), 

	( rigBoneStruct rb_bone:$SKEL_R_Clavicle  rb_Radius: 0.15 rb_Length:$SKEL_R_Clavicle.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_UpperArm  rb_Radius: 0.1 rb_Length:$SKEL_R_UpperArm.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Forearm  rb_Radius: 0.1 rb_Length:($SKEL_R_Forearm.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Hand  rb_Radius: 0.05 rb_Length:$SKEL_R_Hand.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger00  rb_Radius: 0.018 rb_Length:($SKEL_R_Finger00.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger01  rb_Radius: 0.018 rb_Length:($SKEL_R_Finger01.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger02  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger02.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger10  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger10.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger11  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger11.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger12  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger12.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger20  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger20.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger21  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger21.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger22  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger22.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger30  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger30.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger31  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger31.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger32  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger32.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger40  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger40.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger41  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger41.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger42  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger42.length * 1.1) rb_Trans:undefined )
	)
	
	--******************************************************************************************
	--NOW CREATE THE VOLUMES FOR DETACHING THE MESH INTO PIECES
	--******************************************************************************************
	for i = 1 to rigBoneListStruct.count do
	(
		volTrans = undefined
		
		currentObj = getNodeByName ("splitVol_"+rigBoneListStruct[i].rb_bone.name)
		if currentObj != undefined do
		(
			delete currentObj
		)
		
		--we need to make the feet vols as cubes so this is a bit of a hack
		if (rigBoneListStruct[i].rb_bone.name == "SKEL_L_Foot") then
		(
			splitObj = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:0.4 width:0.3 height:0.125 mapcoords:on pos:[0.141602,0.0458236,0] isSelected:on
			splitObj.name = ("splitVol_"+rigBoneListStruct[i].rb_bone.name)
			splitObj.parent = rigBoneListStruct[i].rb_bone	
		)
		else
		(
			if (rigBoneListStruct[i].rb_bone.name == "SKEL_R_Foot") then
			(
				splitObj = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:0.4 width:0.3 height:0.125 mapcoords:on pos:[-0.141602,0.0458236,0] isSelected:on
				splitObj.name = ("splitVol_"+rigBoneListStruct[i].rb_bone.name)
				splitObj.parent = rigBoneListStruct[i].rb_bone						
			)
			else
			(
				
				
				splitObj = Cylinder smooth:on heightsegs:8 capsegs:4 sides:8 height:rigBoneListStruct[i].rb_Length radius:rigBoneListStruct[i].rb_Radius mapcoords:on pos:[0,0,0] isSelected:on
				splitObj.name = ("splitVol_"+rigBoneListStruct[i].rb_bone.name)
				splitObj.parent = rigBoneListStruct[i].rb_bone
				
				
				--******************************************************************************************
				--NOW SORT OUT THE ALIGNMENT OF THE VOLUME
				--******************************************************************************************
				if rigBoneListStruct[i].rb_Trans == undefined then 
				(
					volTrans = rigBoneListStruct[i].rb_bone.transform
					splitObj.transform = ( in coordsys parent volTrans)
					in coordsys parent rotate splitObj (angleaxis 90 [0,1,0])
					if (substring splitObj.name 15 5) == "Spine" do
					(
						in coordsys world splitObj.rotation = (quat 3.65661e-007 0 -1 6.78003e-007)
					)
				)
				else --this allows us to offset the transforms from the bone.
				(
					volTrans = rigBoneListStruct[i].rb_Trans
					splitObj.transform = ( in coordsys parent volTrans)
				)
				
				
		-- 		splitObj.scale = [1.05,1.05,1.05]
		-- 		convertTo splitObj PolyMeshObject
		-- 		
		-- 		--******************************************************************************************
		-- 		--NOW WE NEED TO TRIM TO VOLUMES SO WE DONT GET NASTY LOOKING SPLIT GEO PIECES.
		-- 		--******************************************************************************************
		-- 		
		-- 		--need to modify this so we have a set of multi dimensional arrays, the first eement being the piece that needs trimming and each other element is pieces to trim it with.
		-- 		
		-- 		if (substring splitObj.name 15 7) == "R_Thigh" then --this bit will trim the thigh volume so it doesnt pick the other leg
		-- 		(
		-- 			print ("Trimming "+splitObj.name)
		-- 			trimmerBox = Box lengthsegs:1 widthsegs:1 heightsegs:1 length:2 width:1 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
		-- 			boolObj.createBooleanObject splitObj trimmerBox 4 5
		-- 			boolObj.setBoolOp splitObj 3
					--convertTo splitObj PolyMeshObject
		-- 		)
		-- 		else
		-- 		(
		-- 			if (substring splitObj.name 15 7) == "L_Thigh" do
		-- 			(
		-- 				print ("Trimming "+splitObj.name)
		-- 				trimmerBox = Box lengthsegs:1 widthsegs:1 heightsegs:1 length:2 width:1 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [-0.49,1.06454,0.862417]) isSelected:on
		-- 				boolObj.createBooleanObject splitObj trimmerBox 4 5
		-- 				boolObj.setBoolOp splitObj 3
						--convertTo splitObj PolyMeshObject				
		-- 			)
		-- 		)
		-- 		append volObjects splitObj		
			)
		)
	)
)

genVols()