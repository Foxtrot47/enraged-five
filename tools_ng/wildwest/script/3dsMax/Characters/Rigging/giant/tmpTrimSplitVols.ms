trimArrays = #( --array of items to be trimmed followed by the items used to trim them
#("splitVol_SKEL_L_Toe0", "splitVol_SKEL_L_Foot"),
#("splitVol_SKEL_L_Foot", "splitVol_SKEL_L_Calf"),
#("splitVol_SKEL_L_Calf", "splitVol_SKEL_L_Thigh"),
#("splitVol_SKEL_L_Thigh", "TRIMMERBOX"),

#("splitVol_SKEL_R_Toe0", "splitVol_SKEL_R_Foot"),
#("splitVol_SKEL_R_Foot", "splitVol_SKEL_R_Calf"),
#("splitVol_SKEL_R_Calf", "splitVol_SKEL_R_Thigh"),
#("splitVol_SKEL_R_Thigh", "TRIMMERBOX"),

#("splitVol_SKEL_Pelvis",  "splitVol_SKEL_R_Thigh", "splitVol_SKEL_L_Thigh"),

#("splitVol_SKEL_L_Finger02", "splitVol_SKEL_L_Finger01"),
#("splitVol_SKEL_L_Finger01", "splitVol_SKEL_L_Finger00"),
#("splitVol_SKEL_L_Finger00"),

#("splitVol_SKEL_L_Finger12", "splitVol_SKEL_L_Finger11"),
#("splitVol_SKEL_L_Finger11", "splitVol_SKEL_L_Finger10"),
#("splitVol_SKEL_L_Finger10"),

#("splitVol_SKEL_L_Finger22", "splitVol_SKEL_L_Finger21"),
#("splitVol_SKEL_L_Finger21", "splitVol_SKEL_L_Finger20"),
#("splitVol_SKEL_L_Finger20"),

#("splitVol_SKEL_L_Finger32", "splitVol_SKEL_L_Finger31"),
#("splitVol_SKEL_L_Finger31", "splitVol_SKEL_L_Finger30"),
#("splitVol_SKEL_L_Finger30"),

#("splitVol_SKEL_L_Finger42", "splitVol_SKEL_L_Finger41"),
#("splitVol_SKEL_L_Finger41", "splitVol_SKEL_L_Finger40"),
#("splitVol_SKEL_L_Finger40"),

#("splitVol_SKEL_R_Finger02", "splitVol_SKEL_R_Finger01"),
#("splitVol_SKEL_R_Finger01", "splitVol_SKEL_R_Finger00"),
#("splitVol_SKEL_R_Finger00"),

#("splitVol_SKEL_R_Finger12", "splitVol_SKEL_R_Finger11"),
#("splitVol_SKEL_R_Finger11", "splitVol_SKEL_R_Finger10"),
#("splitVol_SKEL_R_Finger10"),

#("splitVol_SKEL_R_Finger22", "splitVol_SKEL_R_Finger21"),
#("splitVol_SKEL_R_Finger21", "splitVol_SKEL_R_Finger20"),
#("splitVol_SKEL_R_Finger20"),

#("splitVol_SKEL_R_Finger32", "splitVol_SKEL_R_Finger31"),
#("splitVol_SKEL_R_Finger31", "splitVol_SKEL_R_Finger30"),
#("splitVol_SKEL_R_Finger30"),

#("splitVol_SKEL_R_Finger42", "splitVol_SKEL_R_Finger41"),
#("splitVol_SKEL_R_Finger41", "splitVol_SKEL_R_Finger40"),
#("splitVol_SKEL_R_Finger40"),


#("splitVol_SKEL_L_Hand", "splitVol_SKEL_L_Forearm"),
#("splitVol_SKEL_L_Forearm", "splitVol_SKEL_L_UpperArm"),
#("splitVol_SKEL_L_UpperArm", "ARMTRIMBOX"),
#("splitVol_SKEL_L_Clavicle", "splitVol_SKEL_Spine3"),

#("splitVol_SKEL_R_Hand", "splitVol_SKEL_R_Forearm"),
#("splitVol_SKEL_R_Forearm", "splitVol_SKEL_R_UpperArm"),
#("splitVol_SKEL_R_UpperArm", "ARMTRIMBOX"),
#("splitVol_SKEL_R_Clavicle", "splitVol_SKEL_Spine3"),

#("splitVol_SKEL_Head"),

#("splitVol_SKEL_Spine3", "splitVol_SKEL_Spine3"),
#("splitVol_SKEL_Spine2", "splitVol_SKEL_Spine1"),
#("splitVol_SKEL_Spine1", "splitVol_SKEL_Spine0"),
#("splitVol_SKEL_Spine0", "splitVol_SKEL_Pelvis")
)

--***************************************************************************************************************************
--***************************************************************************************************************************
--DO I NEED TO DUPLICATE THE SPLIT VOLS SO WE ARE NOT THENMAKING NEW TRIM OBJECTS OFF NONE CLEAN MESHES?
--***************************************************************************************************************************
--***************************************************************************************************************************

dupSplitVol = #()

fn dupSplitVols = --we need to duplicate each splitVol object so we dont fuck up the clean ones. THESE then become to one we use for duplicating and the SplitVol one becomes the oen which get trimmed
(
	for o in objects do
	(
		clearSelection()
		select o
		if (substring o.name 1 8) == "splitVol" do
		(
			trimDupArr = selection as array
			
			origName = o.name

			newName = ("MASTERTRIM_"+origName)
			maxOps.cloneNodes trimDupArr cloneType:#copy actualNodeList:&c newNodes:&nnl
			trimmerObj = nnl[1]
			trimmerObj.name = newName
			appendIfUnique dupSplitVol trimmerObj 
			
			--now we need to convert the splitMesh into an epoly and triangulate it
			convertTo o PolyMeshObject
			select o
			subObjectLevel = 1
			max select all
			o.EditablePoly.ConnectVertices ()
			subobjectLevel = 0
		)
	)
)

FN trimSplitVols = 
(
-- 	clearListener()
	dupSplitVols()
	
	for tr = 1 to trimArrays.count do
	(
		for i = 2 to trimArrays[tr].count do
		(
			print (trimArrays[tr][1]+" needs to be trimmed by "+trimArrays[tr][i])

			clearSelection()
			
			if trimArrays[tr][i] == "TRIMMERBOX" then
			(
				if (substring trimArrays[tr][1] 15 1) == "L" then
				(
					trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:2 width:1 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [-0.49,1.06454,0.862417]) isSelected:on
					trimmer.name = ("MASTERTRIM_TB_"+trimArrays[tr][1])
					appendIfUnique dupSplitVol trimmer
					select trimmer
				)
				else
				(
					trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:2 width:1 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
					trimmer.name = ("MASTERTRIM_TB_"+trimArrays[tr][1])
					appendIfUnique dupSplitVol trimmer
					select trimmer
				)
			)
			else
			(
				if trimArrays[tr][i] == "ARMTRIMBOX" then
				(
					trimBoxName = ("MASTERTRIM_TB_"+trimArrays[tr][i])
-- 					alreadyExists = getnodebyname trimBoxName
-- 					if alreadyExists != undefined do
-- 					(
-- 						delete alreadyExists
-- 					)
					trimmer = Box lengthsegs:30 widthsegs:10 heightsegs:30 length:2 width:0.4 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [-0.0,1.06454,0.862417]) isSelected:on
					trimmer.name = trimBoxName 
					appendIfUnique dupSplitVol trimmer
					select trimmer
				)
				else
				(
				--ok so first get the first item to be used as a trimmer and dupliocate it
-- 				trimmer = getNodeByName trimArrays[tr][i]
					trimmer = getNodeByName ("MASTERTRIM_"+trimArrays[tr][i])
					appendIfUnique dupSplitVol trimmer
					select trimmer
				)
			)
			
			nodeToBeTrimmed = getNodeByName trimArrays[tr][1]
			
				--have to put trimmer into an array cos duplicate in max is dogshite

				
				trimDupArr = selection as array
				
				origName = trimArrays[tr][i]

				newName = ("trimmer_"+(substring trimmer.Name 19 20))
				maxOps.cloneNodes trimDupArr cloneType:#copy actualNodeList:&c newNodes:&nnl
				trimmerObj = nnl[1]
				trimmerObj.name = newName
				select trimmerObj
				--now to ensure we dont get clipping and nasty shit im gonna try increasing the radius
					if classof trimmerObj == Cylinder do
					(
						trimmerObj.radius = (trimmerObj.radius  * 2)

					)
					convertTo trimmerObj PolyMeshObject
					
					
					--ok now we have duplicated the volume (so we dont loose it when we do a bool) we need to do the boolean to trim the original object
					
-- 					print ("Trimming "+trimmerObj.name)
	-- 				trimmerBox = Box lengthsegs:1 widthsegs:1 heightsegs:1 length:2 width:1 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
					print ("Trimming "+nodeToBeTrimmed.name+" with "+trimmerObj.name)
					boolObj.createBooleanObject nodeToBeTrimmed trimmerObj 4 5
					boolObj.setBoolOp nodeToBeTrimmed 3
					convertTo nodeToBeTrimmed PolyMeshObject
		)
	)
	
	--now remove the masterTrim objects
	for obj = dupSplitVol.count to 1 by -1 do
	(
		if (substring dupSplitVol[obj].name 1 11) == "MASTERTRIM_" do
		(
			delete dupSplitVol[obj]
		)
	)
)

clearListener()
start = timeStamp()
trimSplitVols()
end = timeStamp()
format "Processing took % seconds\n" ((end - start) / 1000.0)

