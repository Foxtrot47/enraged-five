fn straightenLegs = 
(
	LStart = in coordsys parent $SKEL_L_Thigh.rotation.controller.Frozen_Rotation.x_rotation 
	
	print ("LStart = "+(LStart as string))
	
	LFinish = LStart * - 1
		
	print ("LFinish = "+(LFinish as string))
	in coordsys parent $SKEL_L_Thigh.rotation.controller.Zero_Euler_XYZ.x_rotation = LFinish
	
	RStart = in coordsys parent $SKEL_R_Thigh.rotation.controller.Frozen_Rotation.x_rotation 
	RFinish = RStart * -1
	in coordsys parent  $SKEL_R_Thigh.rotation.controller.Zero_Euler_XYZ.x_rotation = RFinish

)



fn straightenArms = 
(
	LStart = in coordsys parent $SKEL_L_Forearm.rotation.controller.Frozen_Rotation.z_rotation 
	
	print ("LStart = "+(LStart as string))
	
	LFinish = LStart * - 1
		
	print ("LFinish = "+(LFinish as string))
	in coordsys parent $SKEL_L_Forearm.rotation.controller.Zero_Euler_XYZ.z_rotation = LFinish
	
	RStart = in coordsys parent $SKEL_R_Forearm.rotation.controller.Frozen_Rotation.z_rotation 
	RFinish = RStart * -1
	in coordsys parent  $SKEL_R_Forearm.rotation.controller.Zero_Euler_XYZ.z_rotation = RFinish

)
	
if ((straightenGUI != undefined) and (straightenGUI.isDisplayed)) do
	(destroyDialog straightenGUI)

rollout straightenGUI ""
(
	button btnStraight "Straighten Shit" width:110 height:40
-- 	button btnRunMarkerToGiant "Marker -> Giant" width:110
-- 	button btnBoolMesh "Segment" width:110
-- 	button btnCombineGeo "Combine & Skin" width:110

	on btnStraight pressed do
	(
		straightenArms()
		straightenLegs()
		
	)
)

CreateDialog straightenGUI width:125 pos:[1450, 100] 