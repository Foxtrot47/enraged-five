-- Load the common maxscript functions 
--include "rockstar/export/settings.ms" -- This is SLOW! 
filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()


-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_bone_tagger.ms")
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_Rigging.ms")
filein (theProjectRoot + "tools/dcc/current/max2011/scripts/pipeline/util/xml.ms")


dupSplitVol = #()
volObjects = #()
rigBoneListStruct = #()

struct rigBoneStruct (rb_bone, rb_Radius, rb_Length, rb_Trans)

trimArrays = #( --array of items to be trimmed followed by the items used to trim them
#("splitVol_SKEL_L_Toe0", "splitVol_SKEL_L_Foot"),
#("splitVol_SKEL_L_Foot", "splitVol_SKEL_L_Calf"),
#("splitVol_SKEL_L_Calf", "splitVol_SKEL_L_Thigh"),
#("splitVol_SKEL_L_Thigh", "TRIMMERBOX"),

#("splitVol_SKEL_R_Toe0", "splitVol_SKEL_R_Foot"),
#("splitVol_SKEL_R_Foot", "splitVol_SKEL_R_Calf"),
#("splitVol_SKEL_R_Calf", "splitVol_SKEL_R_Thigh"),
#("splitVol_SKEL_R_Thigh", "TRIMMERBOX"),

#("splitVol_SKEL_Pelvis",  "splitVol_SKEL_R_Thigh", "splitVol_SKEL_L_Thigh"),

#("splitVol_SKEL_L_Finger02", "splitVol_SKEL_L_Finger01"),
#("splitVol_SKEL_L_Finger01", "splitVol_SKEL_L_Finger00"),
#("splitVol_SKEL_L_Finger00"),#("splitVol_SKEL_L_Finger12", "splitVol_SKEL_L_Finger11"),
#("splitVol_SKEL_L_Finger11", "splitVol_SKEL_L_Finger10"),
#("splitVol_SKEL_L_Finger10"),

#("splitVol_SKEL_L_Finger22", "splitVol_SKEL_L_Finger21"),
#("splitVol_SKEL_L_Finger21", "splitVol_SKEL_L_Finger20"),
#("splitVol_SKEL_L_Finger20"),

#("splitVol_SKEL_L_Finger32", "splitVol_SKEL_L_Finger31"),
#("splitVol_SKEL_L_Finger31", "splitVol_SKEL_L_Finger30"),
#("splitVol_SKEL_L_Finger30"),

#("splitVol_SKEL_L_Finger42", "splitVol_SKEL_L_Finger41"),
#("splitVol_SKEL_L_Finger41", "splitVol_SKEL_L_Finger40"),
#("splitVol_SKEL_L_Finger40"),

#("splitVol_SKEL_R_Finger02", "splitVol_SKEL_R_Finger01"),
#("splitVol_SKEL_R_Finger01", "splitVol_SKEL_R_Finger00"),
#("splitVol_SKEL_R_Finger00"),

#("splitVol_SKEL_R_Finger12", "splitVol_SKEL_R_Finger11"),
#("splitVol_SKEL_R_Finger11", "splitVol_SKEL_R_Finger10"),
#("splitVol_SKEL_R_Finger10"),

#("splitVol_SKEL_R_Finger22", "splitVol_SKEL_R_Finger21"),
#("splitVol_SKEL_R_Finger21", "splitVol_SKEL_R_Finger20"),
#("splitVol_SKEL_R_Finger20"),

#("splitVol_SKEL_R_Finger32", "splitVol_SKEL_R_Finger31"),
#("splitVol_SKEL_R_Finger31", "splitVol_SKEL_R_Finger30"),
#("splitVol_SKEL_R_Finger30"),

#("splitVol_SKEL_R_Finger42", "splitVol_SKEL_R_Finger41"),
#("splitVol_SKEL_R_Finger41", "splitVol_SKEL_R_Finger40"),
#("splitVol_SKEL_R_Finger40"),


#("splitVol_SKEL_L_Hand", "splitVol_SKEL_L_Forearm"),
#("splitVol_SKEL_L_Forearm", "splitVol_SKEL_L_UpperArm"),
#("splitVol_SKEL_L_UpperArm", "ARMTRIMBOX"),
#("splitVol_SKEL_L_Clavicle", "splitVol_SKEL_Spine3"),

#("splitVol_SKEL_R_Hand", "splitVol_SKEL_R_Forearm"),
#("splitVol_SKEL_R_Forearm", "splitVol_SKEL_R_UpperArm"),
#("splitVol_SKEL_R_UpperArm", "ARMTRIMBOX"),
#("splitVol_SKEL_R_Clavicle", "splitVol_SKEL_Spine3"),

#("splitVol_SKEL_Head"),

-- #("splitVol_SKEL_Spine3", "splitVol_SKEL_Spine3"),
#("splitVol_SKEL_Spine3", "splitVol_SKEL_Spine2", "SPINEBOX_R", "SPINEBOX_L"),
#("splitVol_SKEL_Spine2", "splitVol_SKEL_Spine1"),
#("splitVol_SKEL_Spine1", "splitVol_SKEL_Spine0"),
#("splitVol_SKEL_Spine0", "splitVol_SKEL_Pelvis")
)

fn dupSplitVols = --we need to duplicate each splitVol object so we dont fuck up the clean ones. THESE then become to one we use for duplicating and the SplitVol one becomes the oen which get trimmed
(
	for o in objects do
	(
		clearSelection()
		select o
		if (substring o.name 1 8) == "splitVol" do
		(
			trimDupArr = selection as array
			
			origName = o.name

			newName = ("MASTERTRIM_"+origName)
			maxOps.cloneNodes trimDupArr cloneType:#copy actualNodeList:&c newNodes:&nnl
			trimmerObj = nnl[1]
			trimmerObj.name = newName
			appendIfUnique dupSplitVol trimmerObj 
			
			--now we need to convert the splitMesh into an epoly and triangulate it
			convertTo o PolyMeshObject
			select o
			subObjectLevel = 1
			max select all
			o.EditablePoly.ConnectVertices ()
			subobjectLevel = 0
		)
	)
)


FN trimSplitVols = 
(
	clearListener()
-- start = timeStamp()
-- 	clearListener()
	dupSplitVols()
	
	for tr = 1 to trimArrays.count do
	(
		for i = 2 to trimArrays[tr].count do
		(
			print (trimArrays[tr][1]+" needs to be trimmed by "+trimArrays[tr][i])

			clearSelection()
			
			if trimArrays[tr][i] == "TRIMMERBOX" then
			(
				if (substring trimArrays[tr][1] 15 1) == "L" then
				(
					trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:2 width:1 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [-0.49,1.06454,0.862417]) isSelected:on
					trimmer.name = ("MASTERTRIM_TB_"+trimArrays[tr][1])
					appendIfUnique dupSplitVol trimmer
					select trimmer
				)
				else
				(
					trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:2 width:1 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
					trimmer.name = ("MASTERTRIM_TB_"+trimArrays[tr][1])
					appendIfUnique dupSplitVol trimmer
					select trimmer
				)
			)
			else
			(
				if trimArrays[tr][i] == "ARMTRIMBOX" then
				(
					trimBoxName = ("MASTERTRIM_TB_"+trimArrays[tr][i])
-- 					alreadyExists = getnodebyname trimBoxName
-- 					if alreadyExists != undefined do
-- 					(
-- 						delete alreadyExists
-- 					)
					trimmer = Box lengthsegs:30 widthsegs:10 heightsegs:30 length:2 width:0.4 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [-0.0,1.06454,0.862417]) isSelected:on
					trimmer.name = trimBoxName 
					appendIfUnique dupSplitVol trimmer
					select trimmer
				)
				else
				(
					
					if trimArrays[tr][i] == "SPINEBOX_R" then
					(
						nudger = -0.025
						
						trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:2 width:1 height:2 mapcoords:on --transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
						trimmer.name = ("MASTERTRIM_TB_"+trimArrays[tr][i])
						
						arm = $SKEL_R_UpperArm
						aPos = arm.position[1]
						bPos = trimmer.position[1]

						NPos = (trimmer.width /2)

						-- bocks.position.controller.x_position = ( aPos)
						neg = undefined
						if Apos < 0 do
						(
							Apos = apos * -1
							neg = true
						)
						if nPos < 0 do
						(
							nPos = nPos * -1
						)
						newP = (apos + nPos)

						if neg == true then
						(
							trimmer.position.controller.x_position  = ((newP * -1) + nudger)
						)
						else
						(
							trimmer.position.controller.x_position = (newP + nudger)
						)

						appendIfUnique dupSplitVol trimmer
						select trimmer
					)
					else
					(
						if trimArrays[tr][i] == "SPINEBOX_L" then
						(
							nudger = 0.025
							
							trimmer = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:2 width:1 height:2 mapcoords:on --transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
							trimmer.name = ("MASTERTRIM_TB_"+trimArrays[tr][i])
							
							arm = $SKEL_L_UpperArm
							aPos = arm.position[1]
							bPos = trimmer.position[1]

							NPos = (trimmer.width /2)

							-- bocks.position.controller.x_position = ( aPos)
							neg = undefined
							if Apos < 0 do
							(
								Apos = apos * -1
								neg = true
							)
							if nPos < 0 do
							(
								nPos = nPos * -1
							)
							newP = (apos + nPos)

							if neg == true then
							(
								trimmer.position.controller.x_position  = ((newP * -1) + nudger)
							)
							else
							(
								trimmer.position.controller.x_position = (newP + nudger)
							)
								appendIfUnique dupSplitVol trimmer
								select trimmer
						)
						else
						(
						--ok so first get the first item to be used as a trimmer and dupliocate it
		-- 				trimmer = getNodeByName trimArrays[tr][i]
							trimmer = getNodeByName ("MASTERTRIM_"+trimArrays[tr][i])
							appendIfUnique dupSplitVol trimmer
							select trimmer
						)
					)
				)
			)
			
			nodeToBeTrimmed = getNodeByName trimArrays[tr][1]
			
				--have to put trimmer into an array cos duplicate in max is dogshite

				
				trimDupArr = selection as array
				
				origName = trimArrays[tr][i]

				newName = ("trimmer_"+(substring trimmer.Name 19 20))
				maxOps.cloneNodes trimDupArr cloneType:#copy actualNodeList:&c newNodes:&nnl
				trimmerObj = nnl[1]
				trimmerObj.name = newName
				select trimmerObj
				--now to ensure we dont get clipping and nasty shit im gonna try increasing the radius
					if classof trimmerObj == Cylinder do
					(
						trimmerObj.radius = (trimmerObj.radius  * 2)

					)
-- 					convertTo trimmerObj PolyMeshObject
					
					
					--ok now we have duplicated the volume (so we dont loose it when we do a bool) we need to do the boolean to trim the original object
					
-- 					print ("Trimming "+trimmerObj.name)
	-- 				trimmerBox = Box lengthsegs:1 widthsegs:1 heightsegs:1 length:2 width:1 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
					print ("Trimming "+nodeToBeTrimmed.name+" with "+trimmerObj.name)
					boolObj.createBooleanObject nodeToBeTrimmed trimmerObj 4 5
					boolObj.setBoolOp nodeToBeTrimmed 3
					convertTo nodeToBeTrimmed PolyMeshObject
		)
	)
	
	--now remove the masterTrim objects
	for obj = dupSplitVol.count to 1 by -1 do
	(
		if (substring dupSplitVol[obj].name 1 11) == "MASTERTRIM_" do
		(
			delete dupSplitVol[obj]
		)
	)
-- 	end = timeStamp()
-- 	format "Processing took % seconds\n" ((end - start) / 1000.0)
)

fn createVols = 
(

	volObjects = #()
	
	rigBoneListStruct = #( --array of all skeleton bones which we want to split mesh for
		( rigBoneStruct rb_bone:$SKEL_Pelvis  rb_Radius: 0.25 rb_Length:($SKEL_Pelvis.length * 2) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_Spine0  rb_Radius: 0.25 rb_Length:((distance $SKEL_Spine0 $SKEL_Spine1)) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_Spine1  rb_Radius: 0.25 rb_Length:($SKEL_Spine1.length * 1.4) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_Spine2  rb_Radius: 0.22 rb_Length:($SKEL_Spine2.length * 1.4) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_Spine3  rb_Radius: 0.24 rb_Length:($SKEL_Spine3.length * 0.8) rb_Trans:undefined ), 


-- 		( rigBoneStruct rb_bone:$SKEL_Head  rb_Radius: 0.15 rb_Length:($SKEL_Head.length * 2) rb_Trans:(matrix3 [-1,0.000145016,-0.000328264] [-5.81659e-005,-0.96816,-0.250334] [-0.000354256,-0.250334,0.96816] [-0.000112729,0.0139014,1.59917]) ),	
		( rigBoneStruct rb_bone:$SKEL_Head  rb_Radius: 0.15 rb_Length:($SKEL_Head.length * 2) rb_Trans:undefined),	
		
		( rigBoneStruct rb_bone:$SKEL_L_Thigh  rb_Radius: 0.14 rb_Length:$SKEL_L_Thigh.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Calf  rb_Radius: 0.12 rb_Length:$SKEL_L_Calf.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Foot  rb_Radius: 0.1 rb_Length:($SKEL_L_Foot.length * 1.5) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Toe0  rb_Radius: 0.1 rb_Length:0.25 rb_Trans:undefined ),
		
		( rigBoneStruct rb_bone:$SKEL_R_Thigh  rb_Radius: 0.14 rb_Length:$SKEL_R_Thigh.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Calf  rb_Radius: 0.12 rb_Length:$SKEL_R_Calf.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Foot  rb_Radius: 0.1 rb_Length:($SKEL_R_Foot.length * 1.5) rb_Trans:(matrix3 [0.999233,0.0391547,3.80804e-005] [-0.00134612,0.0333794,0.999442] [0.0391315,-0.998676,0.0334067] [-0.155,0.145386,0.0342977]) ), 
		( rigBoneStruct rb_bone:$SKEL_R_Toe0  rb_Radius: 0.1 rb_Length:0.25 rb_Trans:undefined ), 

		( rigBoneStruct rb_bone:$SKEL_L_Clavicle  rb_Radius: 0.15 rb_Length:$SKEL_L_Clavicle.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_UpperArm  rb_Radius: 0.1 rb_Length:$SKEL_L_UpperArm.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Forearm  rb_Radius: 0.1 rb_Length:($SKEL_L_Forearm.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Hand  rb_Radius: 0.05 rb_Length:$SKEL_L_Hand.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger00  rb_Radius: 0.018 rb_Length:($SKEL_L_Finger00.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger01  rb_Radius: 0.018 rb_Length:($SKEL_L_Finger01.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger02  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger02.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger10  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger10.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger11  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger11.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger12  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger12.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger20  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger20.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger21  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger21.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger22  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger22.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger30  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger30.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger31  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger31.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger32  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger32.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger40  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger40.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger41  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger41.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_L_Finger42  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger42.length * 1.1) rb_Trans:undefined ), 

		( rigBoneStruct rb_bone:$SKEL_R_Clavicle  rb_Radius: 0.15 rb_Length:$SKEL_R_Clavicle.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_UpperArm  rb_Radius: 0.1 rb_Length:$SKEL_R_UpperArm.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Forearm  rb_Radius: 0.1 rb_Length:($SKEL_R_Forearm.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Hand  rb_Radius: 0.05 rb_Length:$SKEL_R_Hand.length  rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger00  rb_Radius: 0.018 rb_Length:($SKEL_R_Finger00.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger01  rb_Radius: 0.018 rb_Length:($SKEL_R_Finger01.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger02  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger02.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger10  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger10.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger11  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger11.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger12  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger12.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger20  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger20.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger21  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger21.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger22  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger22.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger30  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger30.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger31  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger31.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger32  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger32.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger40  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger40.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger41  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger41.length * 1.1) rb_Trans:undefined ), 
		( rigBoneStruct rb_bone:$SKEL_R_Finger42  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger42.length * 1.1) rb_Trans:undefined )
	)
	
	--******************************************************************************************
	--NOW CREATE THE VOLUMES FOR DETACHING THE MESH INTO PIECES
	--******************************************************************************************
	for i = 1 to rigBoneListStruct.count do
	(
		volTrans = undefined
		
		currentObj = getNodeByName ("splitVol_"+rigBoneListStruct[i].rb_bone.name)
		if currentObj != undefined do
		(
			delete currentObj
		)
		
		--we need to make the feet vols as cubes so this is a bit of a hack
		if (rigBoneListStruct[i].rb_bone.name == "SKEL_L_Foot") then
		(
			splitObj = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:0.4 width:0.3 height:0.125 mapcoords:on pos:[0.141602,0.0458236,0] isSelected:on
			splitObj.name = ("splitVol_"+rigBoneListStruct[i].rb_bone.name)
			splitObj.parent = rigBoneListStruct[i].rb_bone	
		)
		else
		(
			if (rigBoneListStruct[i].rb_bone.name == "SKEL_R_Foot") then
			(
				splitObj = Box lengthsegs:10 widthsegs:10 heightsegs:10 length:0.4 width:0.3 height:0.125 mapcoords:on pos:[-0.141602,0.0458236,0] isSelected:on
				splitObj.name = ("splitVol_"+rigBoneListStruct[i].rb_bone.name)
				splitObj.parent = rigBoneListStruct[i].rb_bone						
			)
			else
			(
				
				
				splitObj = Cylinder smooth:on heightsegs:8 capsegs:4 sides:12 height:rigBoneListStruct[i].rb_Length radius:rigBoneListStruct[i].rb_Radius mapcoords:on pos:[0,0,0] isSelected:on
				splitObj.name = ("splitVol_"+rigBoneListStruct[i].rb_bone.name)
				splitObj.parent = rigBoneListStruct[i].rb_bone
				
				
				--******************************************************************************************
				--NOW SORT OUT THE ALIGNMENT OF THE VOLUME
				--******************************************************************************************
				if rigBoneListStruct[i].rb_Trans == undefined then 
				(
					volTrans = rigBoneListStruct[i].rb_bone.transform
					splitObj.transform = ( in coordsys parent volTrans)
					in coordsys parent rotate splitObj (angleaxis 90 [0,1,0])
					if (substring splitObj.name 15 5) == "Spine" do
					(
						in coordsys world splitObj.rotation = (quat 3.65661e-007 0 -1 6.78003e-007)
					)
				)
				else --this allows us to offset the transforms from the bone.
				(
					volTrans = rigBoneListStruct[i].rb_Trans
					splitObj.transform = ( in coordsys parent volTrans)
				)
				if splitObj.name == "splitVol_SKEL_Head" do
				(
					splitObj.position = $RB_Neck_1.position
				)
				
				append volObjects splitObj		
			)
		)
	)

	
-- 	trimSplitVols()
)

fn splitMesh origMeshName skinOrNot = --this splits the mesh into chunks based off volumes
(
-- 	MASTEROBJ = getNodeByName origMeshName
-- 	
-- 	original
	createDialog progBar width:300 Height:30
	undo off
	(
--********************************************		
--********************************************		
		decimationAmt = 0 --TMP HACK
--********************************************
--********************************************		
		volPer = 0 --set to default at 0
		chunkMeshes = #()
		
-- 		disableSceneRedraw()
		
-- 		messagebox ("Number of vol objects = "+(volobjects.count as string))
-- 		for i = 1 to volObjects.count do
			for i = 1 to 10 do
		(
			if (substring volObjects[i].name 17 6) == "Finger" then
			(
				volper = decimationAmt + (decimationAmt * 0.1)
			)
			else
			(
				volPer = decimationAmt
			)
			
			
			originalMeshToSplit  = getNodeByName origMeshName
			print ("originalMeshToSplit = "+originalMeshToSplit.name)			
			
			--first clone the object to be split so we can clone it again for the next piece
			
			maxOps.cloneNodes originalMeshToSplit cloneType:#copy newNodes:&nnl
			meshToSplit = nnl[1]
			select meshToSplit 
-- 			messagebox ("Check selection - this is what will get cut")
			
			if geoNo != undefined then
			(
				meshToSplit.name = ("geo_"+geoNo+"_"+(subString volObjects[i].name 15 20))
			)
			else
			(
				meshToSplit.name = ("geo_"+(subString volObjects[i].name 15 20))
			)
			
			select volObjects[i]
-- 			messagebox ("Booleaning "+meshToSplit.name+" with selection - "+volObjects[i].name)
			trimeeName = meshToSplit.name
			trimmerName = volObjects[i].name
			boolObj.createBooleanObject meshToSplit volObjects[i] 4 5
			boolObj.setBoolOp meshToSplit 2
			convertTo meshToSplit PolyMeshObject
			
			if meshToSplit.numverts < 2 do 
				(
					MESSAGEBOX ("WARNING! Boolean on "+trimeename+" by "+trimmername+" seems to have failed.") beep:true
					exit()
				)

			print ("meshToSplit = "+meshToSplit.name)
			print ("i = "+(i as string))
			select meshToSplit

			if (decimationAmt != 0) do
			(
				messagebox "Decimation activated"
				if decimationAmt == 100 do --quick hack to ensure we never decimate by 100%
				(
					volper = (99)
				)
				modPanel.addModToSelection (ProOptimizer ()) ui:on
				meshToSplit.modifiers[#ProOptimizer].KeepUV = on
				meshToSplit.modifiers[#ProOptimizer].OptimizationMode = 1
				meshToSplit.modifiers[#ProOptimizer].Calculate = on
				meshToSplit.modifiers[#ProOptimizer].VertexPercent = volPer
				meshToSplit.modifiers[#ProOptimizer].Calculate = off --trying toforce a recalc as sometimes it becomes invalid
				meshToSplit.modifiers[#ProOptimizer].Calculate = on
			)

			modPanel.addModToSelection (Cap_Holes ()) ui:on
			$.modifiers[#Cap_Holes].Make_All_New_Edges_Visible = 1
			$.modifiers[#Cap_Holes].Smooth_With_Old_Faces = 0

			collapseStack meshToSplit

-- 			if skinOrNot == true then
-- 			(
-- 				print ("Skinning "+meshToSplit.name+" to "+(rigBoneListStruct[i].rb_Bone as string))
-- 				--skin it
-- 				modPanel.addModToSelection (Skin ()) ui:on
-- 				meshToSplit.modifiers[#Skin].bone_Limit = 1
-- 				skinOps.addBone meshToSplit.modifiers[#Skin] rigBoneListStruct[i].rb_bone 1
-- 			)
-- 			else
-- 			(
-- 				print ("Not skinning "+meshToSplit.name)
-- 	-- 				print ("Parenting "+meshToSplit.name+" to "+rigBoneListStruct[i].rb_bone.name)
-- 	-- 				meshToSplit.parent = rigBoneListStruct[i].rb_bone
-- 			)
			
			progBar.prog.value = (100.*i/volObjects.count)
			
			appendIfUnique chunkMeshes meshToSplit
		)
	)
		destroyDialog progBar
		
		if originalMeshToSplit != undefined do
		(
			delete originalMeshToSplit
		)


	
	--now do the bit that combines all splitted meshes into one and reskins
-- 	tmpSegMesh()
-- 	combineIndiv origMeshName chunkMeshes
	
	--test
-- 	print ("Attempting to delete volObjects array")
-- 	delete volObjects
	
)	

fn segmentMesh originalMeshToSplit= 
(
	for i = 1 to volObjects.count do
	(
		print "----------------------------------"
		print ("We need to trim with "+volObjects[i].name)
		print ("MasterObject = "+originalMeshToSplit.name)
		--first we make a copy of the master object which we will do the boolean on
			newName = ("BoolMe")
			maxOps.cloneNodes originalMeshToSplit cloneType:#copy actualNodeList:&c newNodes:&nnl
			boolMe = nnl[1]
		print ("Clone = "+nnl[1].name)
			boolMe.name = newName
		
		--now do the boolean
			finalName = ("geo_"+(substring volObjects[i].name 10 20))
-- 			boolObj.createBooleanObject boolMe volObjects[i] 4 5
-- 				boolObj.createBooleanObject boolMe volObjects[i] 1 1
-- 			boolObj.setBoolOp boolMe 2
				boolObj.createBooleanObject boolMe volObjects[i] 4 5
			boolObj.setBoolOp boolMe 3				
-- 			convertTo boolMe PolyMeshObject
			collapseStack boolMe 
				
			boolMe.name = finalName
				print ("Boolean done on "+boolMe.name)
	)
)

fn dupMeshes originalMeshToSplit  =
(
	for i = 1 to volObjects.count do --TMP HACK TO DO A DUP OF MAIN BODY MESH SO WE CAN TRIM BY HAND
	(
		print "------------------"
		print (volObjects[i].name)
		print ("Mesh to split = "+originalMeshToSplit.name)
		volName = (substring volObjetcs[i].name 10 20)
		newName = ("geo_"+volName)
					
		maxOps.cloneNodes originalMeshToSplit cloneType:#copy newNodes:&nnl
		meshToSplit = nnl[1]
-- 		select meshToSplit 
		meshToSplit.name = volName	
	)
)

-- global decimationAmt = 0
originalMeshToSplit = $RAWMESH
createVols()
-- segmentMesh originalMeshToSplit
-- splitMesh originalMeshToSplit.name false
-- dupMeshes originalMeshToSplit  