-- resetBindPose.ms
-- Rockstar North
-- Matt Rennie Nov 2010
-- Tool to reset skeletons back to bind pose
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

	if ((resetBindPoseGUI != undefined) and (resetBindPoseGUI.isDisplayed)) do
		(destroyDialog resetBindPoseGUI)


fn femaleTransforms = 
(
	if $SKEL_ROOT != undefined do ($SKEL_ROOT.transform = (matrix3 [-1,-1.50996e-007,0] [1.50996e-007,-1,0] [0,0,1] [0,-0.00577943,0.970781]))
	if $SKEL_Pelvis != undefined do (	$SKEL_Pelvis.transform = (matrix3 [-1.78814e-007,0,-1] [-5.00687e-007,-1,0] [-1,5.00687e-007,1.78814e-007] [0,-0.00577943,0.970781]))
	if $SKEL_L_Thigh != undefined do (	$SKEL_L_Thigh.transform = (matrix3 [0.0625566,0.0285136,-0.997634] [0.018218,-0.999458,-0.0274233] [-0.997875,-0.0164594,-0.0630421] [0.0948202,0.00156018,0.905115]))
	if $SKEL_L_Calf != undefined do ($SKEL_L_Calf.transform = (matrix3 [0.0615128,0.0809942,-0.994815] [0.0214825,-0.996579,-0.0798095] [-0.997875,-0.0164617,-0.0630423] [0.122284,0.0140784,0.467126]))
	if $SKEL_L_Foot != undefined do ($SKEL_L_Foot.transform = (matrix3 [0.0155809,-0.944532,-0.328048] [0.00541065,-0.328004,0.944661] [-0.999864,-0.0164936,0] [0.146895,0.0464835,0.0691084]))
	if $SKEL_L_Toe0 != undefined do ($SKEL_L_Toe0.transform = (matrix3 [0.0164935,-0.999858,0.00341801] [5.68256e-005,-0.00341733,-0.999995] [0.999864,0.0164935,3.7998e-007] [0.149072,-0.08548,0.0232757]))
	if $SKEL_L_Toe0_NUB != undefined do ($SKEL_L_Toe0_NUB.transform = (matrix3 [1,2.27243e-007,0] [-1.52271e-007,1,2.44379e-006] [0,-2.5034e-006,1] [0.150728,-0.161426,0.0235321]))
	if $SKEL_R_Thigh != undefined do ($SKEL_R_Thigh.transform = (matrix3 [-0.0625565,0.0285141,-0.997634] [-0.0182198,-0.999458,-0.0274238] [-0.997875,0.0164611,0.0630421] [-0.0948202,0.0015602,0.905115]))
	if $SKEL_R_Calf != undefined do ($SKEL_R_Calf.transform = (matrix3 [-0.0615127,0.0809947,-0.994814] [-0.0214821,-0.996578,-0.07981] [-0.997875,0.0164615,0.0630422] [-0.122284,0.0140786,0.467126]))
	if $SKEL_R_Foot != undefined do ($SKEL_R_Foot.transform = (matrix3 [-0.0155815,-0.944532,-0.328049] [-0.00541106,-0.328004,0.94466] [-0.999864,0.0164945,-1.49012e-007] [-0.146895,0.0464838,0.0691084]))
	if $SKEL_R_Toe0 != undefined do ($SKEL_R_Toe0.transform = (matrix3 [-0.0164943,-0.999858,0.00341715] [-5.58645e-005,-0.00341697,-0.999994] [0.999864,-0.0164944,5.81145e-007] [-0.149072,-0.0854796,0.0232757]))
	if $SKEL_R_Toe0_NUB != undefined do ($SKEL_R_Toe0_NUB.transform = (matrix3 [1,1.93715e-007,0] [-1.41561e-007,1,5.27501e-006] [0,-5.126e-006,1] [-0.150728,-0.161426,0.0235322]))
	if $SKEL_Spine_Root != undefined do ($SKEL_Spine_Root.transform = (matrix3 [-3.57628e-007,0,1] [1.50996e-007,-1,0] [1,1.50996e-007,3.57628e-007] [0,-0.00577943,0.970781]))
	if $SKEL_Spine0 != undefined do ($SKEL_Spine0.transform = (matrix3 [-3.69201e-007,0.0852755,0.996357] [9.54414e-007,-0.996357,0.0852755] [1,9.82421e-007,2.86468e-007] [0,0.0105974,0.992034]))
	if $SKEL_Spine1 != undefined do ($SKEL_Spine1.transform = (matrix3 [-5.62033e-007,-0.0508002,0.998709] [0,-0.998709,-0.0508003] [1,0,5.63442e-007] [0,0.0142315,1.0345]))
	if $SKEL_Spine2 != undefined do ($SKEL_Spine2.transform = (matrix3 [-6.52857e-007,0.015675,0.999877] [0,-0.999877,0.015675] [1,0,6.45407e-007] [0,0.0117116,1.08404]))
	if $SKEL_Spine3 != undefined do ($SKEL_Spine3.transform = (matrix3 [-7.41333e-007,-0.00338531,0.999994] [-1.71491e-005,-0.999994,-0.00338532] [1,-1.71516e-005,6.71484e-007] [-1.36257e-007,0.0136941,1.2105]))
	if $SKEL_L_Clavicle != undefined do ($SKEL_L_Clavicle.transform = (matrix3 [0.895332,0.325933,-0.30356] [0.308674,-0.945392,-0.104655] [-0.321094,-4.39584e-007,-0.947048] [0.0358861,-0.0293614,1.42212]))
	if $SKEL_L_UpperArm != undefined do ($SKEL_L_UpperArm.transform = (matrix3 [0.607956,0.0104617,-0.793902] [0.00636065,-0.999944,-0.00830587] [-0.793946,-4.54485e-007,-0.607989] [0.157633,0.0149589,1.38084]))
	if $SKEL_L_Forearm != undefined do ($SKEL_L_Forearm.transform = (matrix3 [0.547848,-0.0331106,-0.835923] [-0.0181496,-0.99945,0.0276929] [-0.836383,-4.84288e-007,-0.54815] [0.316765,0.0176973,1.17304]))
	if $SKEL_L_Hand != undefined do ($SKEL_L_Hand.transform = (matrix3 [0.547848,-0.0331106,-0.835923] [-0.0181496,-0.99945,0.0276929] [-0.836382,-4.02331e-007,-0.548149] [0.453393,0.00944,0.964567]))
	if $SKEL_L_Finger00 != undefined do ($SKEL_L_Finger00.transform = (matrix3 [0.260741,-0.465241,-0.845911] [-0.137043,-0.885181,0.444599] [-0.955635,-5.96046e-007,-0.294565] [0.456143,-0.00776872,0.958872]))
	if $SKEL_L_Finger01 != undefined do ($SKEL_L_Finger01.transform = (matrix3 [0.261242,-0.215715,-0.940861] [-0.0577118,-0.976452,0.207847] [-0.963552,-1.16392e-006,-0.267543] [0.469053,-0.030804,0.916989]))
	if $SKEL_L_Finger02 != undefined do ($SKEL_L_Finger02.transform = (matrix3 [0.256006,0.095211,-0.961975] [0.0244855,-0.995446,-0.0920147] [-0.966376,-2.01166e-006,-0.257172] [0.480318,-0.0401056,0.876419]))
	if $SKEL_L_Finger0_NUB != undefined do ($SKEL_L_Finger0_NUB.transform = (matrix3 [1,0,0] [0,1,0] [0,0,1] [0.487733,-0.0373482,0.848558]))
	if $SKEL_L_Finger10 != undefined do ($SKEL_L_Finger10.transform = (matrix3 [0.45428,-0.017677,-0.890683] [0.875894,-0.17362,0.450183] [-0.162599,-0.984654,-0.0633894] [0.51358,-0.0109874,0.905873]))
	if $SKEL_L_Finger11 != undefined do ($SKEL_L_Finger11.transform = (matrix3 [-0.00412123,0.00359794,-0.999985] [0.9848,-0.173628,-0.00468311] [-0.173642,-0.984805,-0.00282758] [0.535586,-0.0118437,0.862728]))
	if $SKEL_L_Finger12 != undefined do ($SKEL_L_Finger12.transform = (matrix3 [-0.387364,0.0264484,-0.921548] [0.906085,-0.173587,-0.385846] [-0.170173,-0.984463,0.0432769] [0.53549,-0.01176,0.839474]))
	if $SKEL_L_Finger1_NUB != undefined do ($SKEL_L_Finger1_NUB.transform = (matrix3 [1,0,0] [0,1,0] [0,1.49012e-007,1] [0.526448,-0.0111429,0.817962]))
	if $SKEL_L_Finger20 != undefined do ($SKEL_L_Finger20.transform = (matrix3 [0.31702,0.042504,-0.947466] [0.948323,3.7998e-007,0.317307] [0.0134871,-0.999096,-0.0403072] [0.51491,0.0118613,0.905808]))
	if $SKEL_L_Finger21 != undefined do ($SKEL_L_Finger21.transform = (matrix3 [-0.200338,-0.00513678,-0.979714] [0.979726,2.27243e-007,-0.200341] [0.00102926,-0.999987,0.00503271] [0.531631,0.0141032,0.855834]))
	if $SKEL_L_Finger22 != undefined do ($SKEL_L_Finger22.transform = (matrix3 [-0.559705,-0.062161,-0.826357] [0.827958,3.92087e-007,-0.56079] [0.0348595,-0.998066,0.0514667] [0.525626,0.0139491,0.826467]))
	if $SKEL_L_Finger2_NUB != undefined do ($SKEL_L_Finger2_NUB.transform = (matrix3 [1,-2.2538e-007,0] [2.17929e-007,1,0] [0,0,1] [0.511762,0.0124092,0.805999]))
	if $SKEL_L_Finger30 != undefined do ($SKEL_L_Finger30.transform = (matrix3 [0.299248,0.181312,-0.936791] [0.905841,0.254528,0.338625] [0.299838,-0.949916,-0.0880718] [0.505685,0.0307827,0.904964]))
	if $SKEL_L_Finger31 != undefined do ($SKEL_L_Finger31.transform = (matrix3 [-0.147887,0.00382757,-0.988997] [0.955451,0.258817,-0.141869] [0.255427,-0.965919,-0.0419326] [0.518202,0.0383666,0.865779]))
	if $SKEL_L_Finger32 != undefined do ($SKEL_L_Finger32.transform = (matrix3 [-0.525267,-0.186509,-0.830247] [0.79047,0.254278,-0.557225] [0.31504,-0.948977,0.0138665] [0.513497,0.0384884,0.834316]))
	if $SKEL_L_Finger3_NUB != undefined do ($SKEL_L_Finger3_NUB.transform = (matrix3 [1,2.16067e-007,-1.78814e-007] [-2.5332e-007,1,0] [0,0,1] [0.503234,0.034844,0.818094]))
	if $SKEL_L_Finger40 != undefined do ($SKEL_L_Finger40.transform = (matrix3 [0.369946,0.258385,-0.892399] [0.795401,0.408266,0.447946] [0.480081,-0.875531,-0.0544819] [0.492593,0.0462828,0.907498]))
	if $SKEL_L_Finger41 != undefined do ($SKEL_L_Finger41.transform = (matrix3 [-0.140201,-0.0307818,-0.989645] [0.895522,0.422419,-0.140006] [0.422355,-0.905878,-0.0316576] [0.507349,0.0565886,0.871905]))
	if $SKEL_L_Finger42 != undefined do ($SKEL_L_Finger42.transform = (matrix3 [-0.364801,-0.147577,-0.919316] [0.819402,0.417992,-0.392254] [0.442153,-0.896384,-0.0315594] [0.50435,0.0559301,0.850738]))
	if $SKEL_L_Finger4_NUB != undefined do ($SKEL_L_Finger4_NUB.transform = (matrix3 [1,1.3411e-007,-1.63913e-007] [-1.78814e-007,1,-1.19209e-007] [1.78814e-007,1.63913e-007,1] [0.497978,0.053352,0.83468]))
	if $SKEL_R_Clavicle != undefined do ($SKEL_R_Clavicle.transform = (matrix3 [-0.895332,0.325932,-0.30356] [-0.308674,-0.945393,-0.104655] [-0.321094,-3.12924e-007,0.947048] [-0.0358865,-0.0293614,1.42212]))
	if $SKEL_R_UpperArm != undefined do ($SKEL_R_UpperArm.transform = (matrix3 [-0.607955,0.0104613,-0.793902] [-0.00636074,-0.999945,-0.00830537] [-0.793946,-5.36442e-007,0.607989] [-0.157633,0.0149588,1.38084]))
	if $SKEL_R_Forearm != undefined do ($SKEL_R_Forearm.transform = (matrix3 [-0.547848,-0.0331105,-0.835923] [0.0181488,-0.999451,0.0276943] [-0.836382,-9.23872e-007,0.548149] [-0.316766,0.017697,1.17304]))
	if $SKEL_R_Hand != undefined do ($SKEL_R_Hand.transform = (matrix3 [-0.547848,-0.0331105,-0.835923] [0.0181485,-0.999451,0.0276945] [-0.836382,-5.66244e-007,0.548149] [-0.453393,0.00943956,0.964567]))
	if $SKEL_R_Finger00 != undefined do ($SKEL_R_Finger00.transform = (matrix3 [-0.260742,-0.465241,-0.84591] [0.137041,-0.885182,0.444601] [-0.955633,-2.89083e-006,0.294563] [-0.456143,-0.00776917,0.958872]))
	if $SKEL_R_Finger01 != undefined do	($SKEL_R_Finger01.transform = (matrix3 [-0.261243,-0.215713,-0.940861] [0.057708,-0.976454,0.207851] [-0.963549,-4.91738e-006,0.267542] [-0.469053,-0.0308043,0.91699]))
	if $SKEL_R_Finger02 != undefined do ($SKEL_R_Finger02.transform = (matrix3 [-0.256002,0.0952144,-0.961975] [-0.0244946,-0.995452,-0.0920097] [-0.96637,-1.01626e-005,0.257173] [-0.480318,-0.0401061,0.876419]))
	if $SKEL_R_Finger0_NUB != undefined do ($SKEL_R_Finger0_NUB.transform = (matrix3 [1,0,-2.38419e-007] [0,1,0] [2.38419e-007,0,1] [-0.487733,-0.0373482,0.848558]))
	if $SKEL_R_Finger10 != undefined do ($SKEL_R_Finger10.transform = (matrix3 [-0.45428,-0.0176775,-0.890684] [0.875895,0.17362,-0.450183] [0.1626,-0.984654,-0.0633893] [-0.51358,-0.0109879,0.905873]))
	if $SKEL_R_Finger11 != undefined do ($SKEL_R_Finger11.transform = (matrix3 [0.00412173,0.00359767,-0.999985] [0.984795,0.173656,0.00468389] [0.173671,-0.9848,-0.00282746] [-0.535586,-0.0118442,0.862728]))
	if $SKEL_R_Finger12 != undefined do ($SKEL_R_Finger12.transform = (matrix3 [0.387364,0.0264481,-0.921547] [0.906085,0.173588,0.385846] [0.170174,-0.984463,0.0432771] [-0.53549,-0.0117606,0.839474]))
	if $SKEL_R_Finger1_NUB != undefined do ($SKEL_R_Finger1_NUB.transform = (matrix3 [1,0,2.08616e-007] [0,1,-1.63913e-007] [0,2.08616e-007,1] [-0.526448,-0.0111429,0.817962]))
	if $SKEL_R_Finger20 != undefined do ($SKEL_R_Finger20.transform = (matrix3 [-0.317019,0.0425038,-0.947466] [0.948323,-5.36442e-007,-0.317306] [-0.0134872,-0.999096,-0.0403074] [-0.51491,0.0118608,0.905808]))
	if $SKEL_R_Finger21 != undefined do ($SKEL_R_Finger21.transform = (matrix3 [0.200338,-0.00513697,-0.979713] [0.979726,0,0.20034] [-0.00102902,-0.999987,0.00503269] [-0.531631,0.0141028,0.855834]))
	if $SKEL_R_Finger22 != undefined do ($SKEL_R_Finger22.transform = (matrix3 [0.559705,-0.0621607,-0.826357] [0.827958,5.36442e-007,0.560789] [-0.0348585,-0.998067,0.0514665] [-0.525626,0.0139487,0.826467]))
	if $SKEL_R_Finger2_NUB != undefined do ($SKEL_R_Finger2_NUB.transform = (matrix3 [1,0,1.49012e-007] [0,1,0] [-2.98023e-007,0,1] [-0.511762,0.0124093,0.805999]))
	if $SKEL_R_Finger30 != undefined do ($SKEL_R_Finger30.transform = (matrix3 [-0.299248,0.181311,-0.93679] [0.905841,-0.254529,-0.338624] [-0.299838,-0.949917,-0.0880714] [-0.505685,0.0307822,0.904964]))
	if $SKEL_R_Finger31 != undefined do ($SKEL_R_Finger31.transform = (matrix3 [0.147886,0.00382724,-0.988997] [0.955451,-0.258818,0.141869] [-0.255427,-0.965919,-0.0419325] [-0.518202,0.0383661,0.865779]))
	if $SKEL_R_Finger32 != undefined do ($SKEL_R_Finger32.transform = (matrix3 [0.525268,-0.186509,-0.830246] [0.79047,-0.254277,0.557225] [-0.31504,-0.948977,0.0138663] [-0.513497,0.038488,0.834316]))
	if $SKEL_R_Finger3_NUB != undefined do ($SKEL_R_Finger3_NUB.transform = (matrix3 [1,0,0] [0,1,2.98023e-007] [0,-2.83122e-007,1] [-0.503234,0.034844,0.818094]))
	if $SKEL_R_Finger40 != undefined do ($SKEL_R_Finger40.transform = (matrix3 [-0.369946,0.258385,-0.892399] [0.795401,-0.408266,-0.447944] [-0.48008,-0.875533,-0.0544823] [-0.492594,0.0462824,0.907498]))
	if $SKEL_R_Finger41 != undefined do ($SKEL_R_Finger41.transform = (matrix3 [0.140201,-0.0307817,-0.989644] [0.895522,-0.422418,0.140006] [-0.422353,-0.905879,-0.0316583] [-0.507349,0.0565882,0.871905]))
	if $SKEL_R_Finger42 != undefined do ($SKEL_R_Finger42.transform = (matrix3 [0.364802,-0.147576,-0.919316] [0.819402,-0.417991,0.392253] [-0.442152,-0.896384,-0.0315596] [-0.50435,0.0559296,0.850738]))
	if $SKEL_R_Finger4_NUB != undefined do ($SKEL_R_Finger4_NUB.transform = (matrix3 [1,0,1.25729e-007] [0,1,-1.49012e-007] [0,1.49012e-007,1] [-0.497978,0.053352,0.83468]))
	if $SKEL_Neck_1 != undefined do ($SKEL_Neck_1.transform = (matrix3 [-1.20699e-006,-0.228711,0.973494] [1.27906e-006,-0.973494,-0.228711] [1,9.46224e-007,1.53482e-006] [-1.97274e-007,0.0128711,1.45361]))
	if $SKEL_Head != undefined do ($SKEL_Head.transform = (matrix3 [-5.66244e-007,-0.00759969,0.999971] [1.26138e-005,-0.99997,-0.0075994] [1,1.2558e-005,6.10948e-007] [-3.44743e-007,-0.0150725,1.57255]))
	if $SKEL_Head_NUB!= undefined do ($SKEL_Head_NUB.transform = (matrix3 [1,-1.26893e-007,5.97909e-007] [1.31549e-007,0.999978,-0.00659546] [-6.59376e-007,0.00659544,0.999978] [0,-0.0164234,1.75031]))
)

fn maleTransforms = 
(
	if $SKEL_ROOT != undefined do ($SKEL_ROOT.transform = (matrix3 [-1,-1.50996e-007,-1.94785e-007] [1.50996e-007,-1,0] [-1.94785e-007,0,1] [0,0.00710078,0.981294]))
	if $SKEL_Pelvis != undefined do ($SKEL_Pelvis.transform = (matrix3 [-4.01261e-007,0,-1] [1.50996e-007,-1,0] [-1,-1.50996e-007,4.01261e-007] [0,0.00710078,0.981294]))
	if $SKEL_L_Thigh != undefined do ($SKEL_L_Thigh.transform = (matrix3 [0.0770737,0.0415614,-0.996159] [0.0421553,-0.998373,-0.0383922] [-0.996134,-0.0390344,-0.0787003] [0.0961179,0.00732644,0.907786]))
	if $SKEL_L_Calf != undefined do ($SKEL_L_Calf.transform = (matrix3 [0.0752391,0.0833694,-0.993674] [0.0453484,-0.995754,-0.0801102] [-0.996134,-0.0390342,-0.0787003] [0.12745,0.024222,0.502829]))
	if $SKEL_L_Foot != undefined do ($SKEL_L_Foot.transform = (matrix3 [0.0372206,-0.949864,-0.310441] [0.0121556,-0.310203,0.950592] [-0.999234,-0.0391555,1.75089e-007] [0.158521,0.0586511,0.0924718]))
	if $SKEL_L_Toe0 != undefined do ($SKEL_L_Toe0.transform = (matrix3 [0.0391253,-0.998473,0.0389946] [0.0015261,-0.0389648,-0.999239] [0.999234,0.0391557,-5.1409e-007] [0.164533,-0.0947744,0.0423283]))
	if $SKEL_L_Toe0_NUB!= undefined do ($SKEL_L_Toe0_NUB.transform = (matrix3 [1,2.48663e-007,0] [-1.8219e-007,1,4.84288e-007] [0,-5.17815e-007,1] [0.16731,-0.187147,0.0458317]))
	if $SKEL_R_Thigh != undefined do ($SKEL_R_Thigh.transform = (matrix3 [-0.0770747,0.0415614,-0.996159] [-0.0421542,-0.998373,-0.0383922] [-0.996134,0.0390332,0.0787013] [-0.0961181,0.00732648,0.907786]))
	if $SKEL_R_Calf != undefined do ($SKEL_R_Calf.transform = (matrix3 [-0.0752402,0.0833698,-0.993674] [-0.0453484,-0.995754,-0.0801106] [-0.996134,0.039034,0.0787014] [-0.12745,0.024222,0.502829]))
	if $SKEL_R_Foot!= undefined do ($SKEL_R_Foot.transform = (matrix3 [-0.0372218,-0.949864,-0.31044] [-0.0121554,-0.310202,0.950593] [-0.999233,0.0391563,3.12924e-007] [-0.158522,0.058651,0.0924719]))
	if $SKEL_R_Toe0 != undefined do ($SKEL_R_Toe0.transform = (matrix3 [-0.0391265,-0.998473,0.0389953] [-0.00152764,-0.0389655,-0.999239] [0.999233,-0.0391563,-7.15256e-007] [-0.164534,-0.0947745,0.0423284]))
	if $SKEL_R_Toe0_NUB != undefined do ($SKEL_R_Toe0_NUB.transform = (matrix3 [1,4.57396e-007,0] [-3.02622e-007,1,1.2219e-006] [0,-1.16229e-006,1] [-0.16731,-0.187147,0.0458317]))
	if $SKEL_Spine_Root!= undefined do ($SKEL_Spine_Root.transform = (matrix3 [-1.08885e-006,0,1] [1.50996e-007,-1,0] [1,1.50996e-007,1.08885e-006] [0,0.00710078,0.981294]))
	if $SKEL_Spine0!= undefined do ($SKEL_Spine0.transform = (matrix3 [-6.21443e-007,0.0951133,0.995466] [0,-0.995467,0.0951133] [1,0,6.14179e-007] [0,0.0171712,1]))
	if $SKEL_Spine1!= undefined do ($SKEL_Spine1.transform = (matrix3 [-6.14848e-007,0.00645121,0.999979] [-8.95823e-006,-0.999979,0.00645121] [1,-8.95407e-006,6.72627e-007] [0,0.025242,1.08447]))
	if $SKEL_Spine2 != undefined do ($SKEL_Spine2.transform = (matrix3 [-6.6217e-007,0.0541195,0.998535] [4.40181e-007,-0.998535,0.0541195] [1,4.75615e-007,6.4075e-007] [0,0.0257963,1.17039]))
	if $SKEL_Spine3!= undefined do ($SKEL_Spine3.transform = (matrix3 [-6.4075e-007,-0.0124379,0.999922] [-4.28513e-006,-0.999923,-0.0124379] [1,-4.29223e-006,5.7742e-007] [-2.61814e-007,0.0319062,1.28312]))
	if $SKEL_L_Clavicle != undefined do ($SKEL_L_Clavicle.transform = (matrix3 [0.921892,0.311585,-0.230284] [0.302296,-0.950219,-0.0755125] [-0.242348,5.66244e-007,-0.970189] [0.0319188,-0.00723723,1.50069]))
	if $SKEL_L_Clavicle != undefined do ($SKEL_L_UpperArm.transform = (matrix3 [0.544118,0.0239979,-0.838666] [0.0130617,-0.999713,-0.0201315] [-0.838907,-1.19209e-007,-0.544273] [0.199606,0.0494385,1.4588]))
	if $SKEL_L_Forearm != undefined do ($SKEL_L_Forearm.transform = (matrix3 [0.543452,-0.0549558,-0.837639] [-0.0299104,-0.998491,0.046104] [-0.838905,-4.24683e-007,-0.544273] [0.348787,0.056018,1.22886]))
	if $SKEL_L_Hand != undefined do ($SKEL_L_Hand.transform = (matrix3 [0.543452,-0.0549558,-0.837639] [-0.0299104,-0.998491,0.046104] [-0.838905,-4.54485e-007,-0.544273] [0.489648,0.0417736,1.01175]))
	if $SKEL_L_Finger00 != undefined do ($SKEL_L_Finger00.transform = (matrix3 [0.206189,-0.534491,-0.819638] [-0.130394,-0.845178,0.518344] [-0.969782,-4.17233e-007,-0.243955] [0.502507,0.0176223,0.990807]))
	if $SKEL_L_Finger01!= UNDEFINED do ($SKEL_L_Finger01.transform = (matrix3 [0.259001,-0.271417,-0.926958] [-0.0730406,-0.962469,0.261412] [-0.963104,0,-0.269099] [0.51412,-0.0124812,0.944643]))
	if $SKEL_L_Finger02!= undefined do ($SKEL_L_Finger02.transform = (matrix3 [0.288308,-0.0432526,-0.95656] [-0.0124836,-0.999079,0.0414229] [-0.957439,0,-0.288582] [0.52344,-0.022248,0.911287]))
	if $SKEL_L_Finger0_NUB != undefined do ($SKEL_L_Finger0_NUB.transform = (matrix3 [1,0,0] [0,1,0] [1.50874e-007,1.19209e-007,1] [0.533384,-0.0237404,0.878296]))
	if $SKEL_L_Finger10 != undefined do ($SKEL_L_Finger10.transform = (matrix3 [0.426536,-0.0803597,-0.900894] [0.884115,-0.173087,0.434031] [-0.19081,-0.981623,-0.00277992] [0.564466,0.00679795,0.935194]))
	if $SKEL_L_Finger11!= undefined do ($SKEL_L_Finger11.transform = (matrix3 [-0.0538138,-0.0564718,-0.996953] [0.983905,-0.173371,-0.0432886] [-0.170398,-0.983236,0.0648926] [0.584636,0.00299782,0.892591]))
	if $SKEL_L_Finger12 != undefined do ($SKEL_L_Finger12.transform = (matrix3 [-0.31912,-0.0292656,-0.947263] [0.934894,-0.173573,-0.30959] [-0.155359,-0.984386,0.0827507] [0.583339,0.00163648,0.868556]))
	if $SKEL_L_Finger1_NUB!= undefined do ($SKEL_L_Finger1_NUB.transform = (matrix3 [1,0,-1.49012e-007] [0,1,0] [0,0,1] [0.575537,0.000920334,0.845398]))
	if $SKEL_L_Finger20!= undefined do ($SKEL_L_Finger20.transform = (matrix3 [0.251935,-0.0190309,-0.967557] [0.967733,5.70901e-007,0.251981] [-0.00479491,-0.999819,0.0184169] [0.56636,0.0307995,0.935284]))
	if $SKEL_L_Finger21 != undefined do ($SKEL_L_Finger21.transform = (matrix3 [-0.15655,-0.0501062,-0.986398] [0.987639,9.72301e-007,-0.156746] [0.00785488,-0.998744,0.0494866] [0.579409,0.0298139,0.885167]))
	if $SKEL_L_Finger22 != undefined do ($SKEL_L_Finger22.transform = (matrix3 [-0.535203,-0.1083,-0.837752] [0.842709,5.91041e-007,-0.53837] [0.058306,-0.994118,0.0912652] [0.574269,0.0281683,0.852775]))
	if $SKEL_L_Finger2_NUB!= undefined do ($SKEL_L_Finger2_NUB.transform = (matrix3 [1,0,1.49012e-007] [0,1,-3.05474e-007] [-1.63913e-007,1.79745e-007,1] [0.559429,0.0251649,0.829545]))
	if $SKEL_L_Finger30 != undefined do ($SKEL_L_Finger30.transform = (matrix3 [0.303954,0.0752058,-0.949714] [0.914025,0.258087,0.31297] [0.268644,-0.963191,0.00970551] [0.555776,0.0537535,0.934313]))
	if $SKEL_L_Finger31!= undefined do ($SKEL_L_Finger31.transform = (matrix3 [-0.175927,-0.0607711,-0.982526] [0.948033,0.258342,-0.185729] [0.265113,-0.96414,0.0121637] [0.569329,0.0571067,0.891966]))
	if $SKEL_L_Finger32!= undefined do ($SKEL_L_Finger32.transform = (matrix3 [-0.568252,-0.1894,-0.800761] [0.759364,0.254135,-0.598984] [0.316949,-0.948442,-0.000588998] [0.563936,0.0552438,0.861848]))
	if $SKEL_L_Finger3_NUB != undefined do ($SKEL_L_Finger3_NUB.transform = (matrix3 [1,1.2666e-007,0] [-1.65775e-007,1,0] [0,0,1] [0.548622,0.050139,0.840267]))
	if $SKEL_L_Finger40!= undefined do ($SKEL_L_Finger40.transform = (matrix3 [0.434048,0.187925,-0.881071] [0.777909,0.415091,0.471762] [0.454377,-0.890161,0.0339788] [0.540245,0.0705118,0.930825]))
	if $SKEL_L_Finger41 != undefined do ($SKEL_L_Finger41.transform = (matrix3 [-0.162258,-0.169861,-0.972018] [0.88212,0.416479,-0.22003] [0.442198,-0.893136,0.0822608] [0.556835,0.0776944,0.89715]))
	if $SKEL_L_Finger42!= undefined do ($SKEL_L_Finger42.transform = (matrix3 [-0.332038,-0.27058,-0.903624] [0.811255,0.406854,-0.419924] [0.481268,-0.872499,0.0844188] [0.55312,0.0738052,0.874893]))
	if $SKEL_L_Finger4_NUB != undefined do ($SKEL_L_Finger4_NUB.transform = (matrix3 [1,0,-3.29688e-007] [0,1,0] [3.23169e-007,0,1] [0.546289,0.0682378,0.856302]))
	if $SKEL_R_Clavicle != undefined do ($SKEL_R_Clavicle.transform = (matrix3 [-0.921892,0.311584,-0.230285] [-0.302295,-0.950219,-0.0755132] [-0.24235,-9.23872e-007,0.970188] [-0.0319193,-0.00723726,1.50069]))
	if $SKEL_R_UpperArm != undefined do ($SKEL_R_UpperArm.transform = (matrix3 [-0.544117,0.0239972,-0.838666] [-0.0130612,-0.999713,-0.0201318] [-0.838908,6.4075e-007,0.544273] [-0.199607,0.0494382,1.4588]))
	if $SKEL_R_Forearm != undefined do ($SKEL_R_Forearm.transform = (matrix3 [-0.543451,-0.0549572,-0.83764] [0.029912,-0.99849,0.0461029] [-0.838907,4.32134e-007,0.544273] [-0.348787,0.0560176,1.22886]))
	if $SKEL_R_Hand != undefined do ($SKEL_R_Hand.transform = (matrix3 [-0.543451,-0.0549571,-0.837641] [0.029912,-0.99849,0.046103] [-0.838907,4.17233e-007,0.544273] [-0.489648,0.041773,1.01175]))
	if $SKEL_R_Finger00!= undefined do ($SKEL_R_Finger00.transform = (matrix3 [-0.206184,-0.534492,-0.819638] [0.130394,-0.845176,0.518342] [-0.969785,9.94653e-007,0.243956] [-0.502507,0.0176218,0.990806]))
	if $SKEL_R_Finger01 != undefined do ($SKEL_R_Finger01.transform = (matrix3 [-0.258998,-0.27142,-0.926958] [0.0730416,-0.962464,0.261408] [-0.963109,2.92063e-006,0.2691] [-0.51412,-0.0124817,0.944642]))
	if $SKEL_R_Finger02!= undefined do ($SKEL_R_Finger02.transform = (matrix3 [-0.288311,-0.0432566,-0.956559] [0.0124888,-0.99907,0.041417] [-0.95745,4.58956e-006,0.288579] [-0.52344,-0.0222486,0.911286]))
	if $SKEL_R_Finger0_NUB!= undefined do ($SKEL_R_Finger0_NUB.transform = (matrix3 [1,0,2.98023e-007] [0,1,-1.3411e-007] [-1.78814e-007,1.3411e-007,1] [-0.533384,-0.0237404,0.878296]))
	if $SKEL_R_Finger10!= undefined do ($SKEL_R_Finger10.transform = (matrix3 [-0.426535,-0.0803603,-0.900894] [0.884115,0.173088,-0.434031] [0.190812,-0.981623,-0.00278002] [-0.564465,0.00679731,0.935193]))
	if $SKEL_R_Finger11 != undefined do ($SKEL_R_Finger11.transform = (matrix3 [0.0538147,-0.0564717,-0.996953] [0.983905,0.173372,0.0432894] [0.170399,-0.983236,0.0648928] [-0.584636,0.00299719,0.89259]))
	if $SKEL_R_Finger12 != undefined do ($SKEL_R_Finger12.transform = (matrix3 [0.319121,-0.0292653,-0.947263] [0.934893,0.173574,0.309591] [0.15536,-0.984386,0.0827509] [-0.583338,0.0016357,0.868555]))
	if $SKEL_R_Finger1_NUB!= undefined do ($SKEL_R_Finger1_NUB.transform = (matrix3 [1,0,0] [-1.19209e-007,1,-4.76837e-007] [-1.19209e-007,4.47035e-007,1] [-0.575537,0.000920327,0.845398]))
	if $SKEL_R_Finger20 != undefined do ($SKEL_R_Finger20.transform = (matrix3 [-0.251934,-0.0190311,-0.967558] [0.967733,2.98023e-007,-0.25198] [0.00479593,-0.999819,0.0184169] [-0.56636,0.0307989,0.935283]))
	if $SKEL_R_Finger21 != undefined do ($SKEL_R_Finger21.transform = (matrix3 [0.156551,-0.0501061,-0.986398] [0.987639,-1.2517e-006,0.156747] [-0.007855,-0.998744,0.0494865] [-0.579409,0.0298132,0.885166]))
	if $SKEL_R_Finger22 != undefined do ($SKEL_R_Finger22.transform = (matrix3 [0.535204,-0.1083,-0.837752] [0.842708,-1.78814e-007,0.538371] [-0.0583053,-0.994118,0.091265] [-0.574268,0.0281677,0.852774]))
	if $SKEL_R_Finger2_NUB != undefined do ($SKEL_R_Finger2_NUB.transform = (matrix3 [1,0,0] [0,1,0] [0,0,1] [-0.559429,0.0251649,0.829545]))
	if $SKEL_R_Finger30!= undefined do ($SKEL_R_Finger30.transform = (matrix3 [-0.303953,0.0752061,-0.949714] [0.914026,-0.258086,-0.312969] [-0.268644,-0.96319,0.0097055] [-0.555776,0.0537529,0.934312]))
	if $SKEL_R_Finger31 != undefined do ($SKEL_R_Finger31.transform = (matrix3 [0.175928,-0.0607714,-0.982526] [0.948033,-0.25834,0.18573] [-0.265113,-0.964141,0.0121639] [-0.569329,0.0571063,0.891965]))
	if $SKEL_R_Finger32!= undefined do ($SKEL_R_Finger32.transform = (matrix3 [0.568253,-0.1894,-0.80076] [0.759363,-0.254134,0.598985] [-0.316948,-0.948443,-0.000589032] [-0.563936,0.0552433,0.861847]))
	if $SKEL_R_Finger3_NUB != undefined do ($SKEL_R_Finger3_NUB.transform = (matrix3 [1,1.75089e-007,0] [-1.49012e-007,1,0] [0,0,1] [-0.548622,0.0501391,0.840267]))
	if $SKEL_R_Finger40 != undefined do ($SKEL_R_Finger40.transform = (matrix3 [-0.434047,0.187925,-0.881072] [0.77791,-0.41509,-0.471761] [-0.454378,-0.89016,0.0339787] [-0.540245,0.0705112,0.930824]))
	if $SKEL_R_Finger41!= undefined do ($SKEL_R_Finger41.transform = (matrix3 [0.162259,-0.169861,-0.972018] [0.88212,-0.416477,0.220031] [-0.442198,-0.893137,0.0822602] [-0.556835,0.0776939,0.897149]))
	if $SKEL_R_Finger42 != undefined do ($SKEL_R_Finger42.transform = (matrix3 [0.33204,-0.27058,-0.903624] [0.811254,-0.406852,0.419926] [-0.481266,-0.8725,0.084418] [-0.55312,0.0738046,0.874892]))
	if $SKEL_R_Finger4_NUB != undefined do ($SKEL_R_Finger4_NUB.transform = (matrix3 [1,-1.52737e-007,0] [1.22935e-007,1,0] [1.63913e-007,0,1] [-0.546289,0.0682377,0.856302]))
	if $SKEL_Neck_1 != undefined do ($SKEL_Neck_1.transform = (matrix3 [-5.96046e-007,-0.229052,0.973414] [1.05047e-006,-0.973415,-0.229052] [0.999999,9.53674e-007,5.06639e-007] [-4.21036e-007,0.0288156,1.5316]))
	if $SKEL_Head != undefined do ($SKEL_Head.transform = (matrix3 [-1.09896e-006,0.00880543,0.999961] [2.50246e-005,-0.999963,0.00880498] [0.999999,2.51476e-005,6.74278e-007] [-3.69224e-007,0.00291588,1.64167]))
	if $SKEL_Head_NUB!= undefined do ($SKEL_Head_NUB.transform = (matrix3 [1,0,7.37607e-007] [0,0.999995,0.00307062] [-8.04663e-007,-0.00307063,0.999995] [-2.16144e-007,0.00455486,1.82779])	)
)


fn resetPicker = 
(
	
	if ((resetBindPoseGUI != undefined) and (resetBindPoseGUI.isDisplayed)) do
		(destroyDialog resetBindPoseGUI)

	rollout resetBindPoseGUI "Facial bone displayer"
	(
		button male "Reset Male Skeleton" width: 150
		button female "Reset Female Skeleton" width: 150
		button custom "Load bPos file" width: 150
		
		on male pressed do
		(
			maleTransforms()

	-- 		if ((resetBindPoseGUI != undefined) and (resetBindPoseGUI.isDisplayed)) do
	 		destroyDialog resetBindPoseGUI
		)
		
		on female pressed do
		(
			femaleTransforms()
	-- 		
	-- 		if ((resetBindPoseGUI != undefined) and (resetBindPoseGUI.isDisplayed)) do
	 		destroyDialog resetBindPoseGUI
		)
		
		on custom pressed do --this will run the same fn as the pastePose from the bindpose save load tool.
		(
			input_name = undefined
			
			if input_name == undefined do
			(
				input_name = getOpenFileName caption:"BindPose file" types:"BPosData (*.bPos)|*.bpos|All Files (*.*)|*.*|"
			)

			if input_name != undefined then
			(
				f = openfile input_name
				inputData = #() -- define as array
				while not eof f do
				(
					append inputData (filterstring (readLine f) "\n")
				)
				close f

				for i = 1 to inputData.count do
				(
					execute inputData[i][1]
				)
			)
			destroyDialog resetBindPoseGUI
		)
	)

	CreateDialog resetBindPoseGUI
)

currentFileName = maxFilePath + maxFileName

if (substring maxFileName 2 3) == "_M_" then
(
	print "This scene is a male"
	maleTransforms()
)
else
(
	if (substring maxFileName 2 3) == "_F_" then
	(
		print "This scene is a female"
		femaleTransforms()
	)
	else
	(
		print "Could not define sex of scene so opening gui"
		resetPicker()
	)
)


 
