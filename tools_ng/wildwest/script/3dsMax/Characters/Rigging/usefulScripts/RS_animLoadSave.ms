-- sela = selection as array

fn RS_saveAnim sela =
(
	output_name = getSaveFileName caption:"Save RS anim file" types:"RsAnimData (*.RsAnim)|*.RsAnim|All Files (*.*)|*.*|"
	
	if output_name != undefined then 
	(
		output_file = createfile output_name	
	
		animStart = (((animationRange.start as float) / ticksPerFrame) as integer)
		animEnd = (((animationRange.end as float) / ticksPerFrame) as integer)
		
		for frame = animStart to animEnd do 
		(
			sliderTime = frame
			timeVal = ("sliderTime = "+(frame as string)+"\n")
			format (timeVal) to:output_file
			for a in sela do 
			(
				posi = in coordsys parent a.position
				rot = in coordsys parent a.rotation
				scl = in coordsys parent a.scale
				
				rotVal = ("in coordsys parent $"+a.name+".rotation = "+(rot as string)+"\n")				
				posVal = ("in coordsys parent $"+a.name+".position = "+(posi as string)+"\n")
				sclVal = ("in coordsys parent $"+a.name+".scale = "+(scl as string)+"\n")
				sepVal = ("-----"+"\n")
				
				outputData = (rotVal+posVal+sclVal+sepVal)
				
				format ((outputData)) to:output_file
			)
		)
		
		close output_file
	)
)

fn RS_loadAnim = 
(
		
	if input_name == undefined do
	(
		input_name = getOpenFileName caption:"Load RS anim file" types:"RsAnimData (*.RsAnim)|*.RsAnim|All Files (*.*)|*.*|"
	)
	
	if input_name != undefined then
	(
		with redraw off 
		(
			max tool animmode
			set animate on
	-- 		f = openfile input_name
	-- 		inputData = #() -- define as array
	-- 		
	-- 		while not eof f do
	-- 		(
	-- 			append inputData (filterstring (readLine f) ",")
	-- 		)
	-- 		close f		
			filein input_name
			
			max tool animmode
			set animate off
		)
	)
	
)


-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- RS_saveAnim()
-- RS_loadAnim()

rollout RSanimSaveLoad "RS Anim Save/Load"
(
	button btnOutput "Save Anim" width:110
	button btnInput "Load Anim" width:110
	
	on btnOutput pressed do
	(
		sela = selection as array
		
		if selection.count == 0 then
		(
			messagebox ("Please select objects to save from") beep:true
		)
		else
		(
			RS_saveAnim sela
		)
	)

	on btnInput pressed do
	(
		input_name = undefined
		
		try (
			currentFrame  = slidertime
			with redraw off 
			RS_loadAnim()
			slidertime = currentframe
		)
		catch
		(
			messagebox ("WARNING! Anim load failed!") beep:true
		)
	)

)

CreateDialog RSanimSaveLoad  width:125 pos:[500, 500] 