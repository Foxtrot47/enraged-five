boobBones = #(
	"SPR_L_Breast",
	"SPR_R_Breast"
	)
	
fn boobSettings = 
(
	for boob in boobBones do
	(
		boobJoint = getNodeByName boob
		
		if (classof boobJoint.position.controller[3].controller as string) == "RsSpring" do 
		(
			cnt = boobJoint.position.controller[3].controller

			cnt.linkParams = false
			cnt.xLimitMin = -0.03
			cnt.yLimitMin = -0.03
			cnt.zLimitMin = -0.03
			cnt.xLimitMax = 0.03
			cnt.yLimitMax = 0.03
			cnt.zLimitMax = 0.06
			cnt.preserveXFormsOnFreeze = false
			cnt.gravityX = 0.0
			cnt.gravityY = 0.0 
			cnt.gravityZ = -9.81
			cnt.strengthX = 320.0
			cnt.strengthY = 320
			cnt.strengthZ =310
			cnt.dampenX = 8.5			
			cnt.dampenY = 8.5
			cnt.dampenZ = 8.5
			cnt.gravity_axisX = 0.0
			cnt.gravity_axisY = 0.0
			cnt.gravity_axisZ = -1.0
			
			format ("Breast Configured for "+boob+"\n")
		)
	)
)	

boobSettings()