filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

theProjectConfig = RsConfigGetProjBinConfigDir()

toolsRoot = RsConfigGetToolsRootDir()

-- filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Includes\\FN_Rigging.ms")	
filein (theWildWest + "script/3dsMax/_config_files/Wildwest_header.ms")


filein (theWildWest + "script/3dsMax/Characters/Rigging/ambientFacial/removeGuideHead.ms")


fn RSTA_deleteEmptyScalarTargets cont obj =
(
	theScalarCount = cont.NumScalars()
	
	for i = 5 to theScalarCount do
	(			
		thisScalarName = cont.GetScalarName i	
			
		thisScalarTgt = cont.GetScalarType thisScalarName
		
		if thisScalarTgt == #scalarConstant  do
		(
			format ("Removing "+thisScalarName+" on "+obj.name+" "+((exprForMaxObject cont) as string)"\n")
			cont.DeleteVariable thisScalarName
		)
	)	
)

fn RSTA_deleteInvalidExpression cont obj = 
(
	format ("Converting "+((exprForMaxObject cont) as string)+" to bezier_float()\n")
	cStr = ((exprForMaxObject cont) as string) 
	
	execute (cStr+ "= bezier_float()")
)

fn RSTA_findConstantScalar cont obj =
(
-- 	local cont = $MH_MulletRoot.rotation.controller[3].Z_rotation.controller
	isConstant = true
	
	scalarCount = cont.NumScalars()
	for i = 5 to scalarCount do
	(
		local thisScalarName = cont.GetScalarName i
		
		--format ("Scalar "+(i as string)+" == "+thisScalarName+"\n")
		
		try(thisScalarTarget = cont.GetScalarTarget i)catch()
		
		if thisScalarTarget == undefined then
		(
			format (thisScalarName+" is a constant on "+((exprForMaxObject cont) as string)+".\n")
		)
		else
		(
			--format ("ThisScalarTarget: "+(thisScalarTarget as string)+"\n")
			isConstant = false
		)
	)
-- 	return isConstant
	
	if isConstant == true do 
	(
		RSTA_deleteInvalidExpression cont obj 
	)
)

fn RSTA_findZeroExpressionString cont obj = 
(
	/**
	WILL CHECK THE EXPRESSION STRING IN cont AND SEE IF THESE ARE VALID STRINGS AND SET CONTROLLER TO BEZIER FLOAT IF NOT
	**/
	
	stringsToCheck = #( --if we get more invalid strings to test against we can add into this array
		"le-006",
		"1e-006",
		" 0",
		" 0\n"
		)
	
	exprStr = cont.getExpression()
	
	validZero = false
	
	while validZero == false do
	(
		for st = 1 to stringsToCheck.count do 
		(
			stc = stringsToCheck[st]
			if exprStr == stC then
			(
				if st == 3 then
				(
					format ("Please check expression on "+obj.name+" "+((exprForMaxObject cont )as string)+"\n")
				)
				else
				(
					if st == 4 then
					(
						format ("Please check expression on "+obj.name+" "+((exprForMaxObject cont )as string)+"\n")
					)
					else
					(
						format ("Replacing empty float expression on "+obj.name+" "+((exprForMaxObject cont )as string)+"\n")
						
						myCont = (exprForMaxObject cont) 	
						myContStr = ((myCont as string)+" = bezier_float()")		
						execute myContStr
						validZero = true
					)
				)
			)
			else
			(
				if st < 3 then
				(
					invString = findstring exprStr stc
					
					if invString != undefined do 
					(
						strLength = stc.count
						newStr = replace exprStr invString strLength "0.0"
						
						format ("Trying to change expression on "+obj.name+" "+((exprForMaxObject cont )as string)+"\n")
						format ("Changing "+exprStr+" to "+newStr+"\n")
						cont.setExpression newStr
						validZero = true
					)
				)
			)
			
			if st == stringsToCheck.count do 
			(
				validZero = true
			)
		)
	)
	
	return validZero 
)

fn RSTA_checkEmptyExpression = 
(
	lookAtContollers = #()
	--sela = selection as array
	sela = objects as array
	--loop through for scalar constants
	for a in sela do 
	(
-- 		if classof a != IES_Sky do 
		if (classof a != IES_Sky) and (classof a != Targetobject) and (classof a != Targetcamera) do
		(			
			if classOf a.position.controller == Position_XYZ then
			(
				for c = 1 to 3 do 
				(
					if classof a.position.controller[c].controller == Float_Expression do 
					(
						obj = a
						cont = a.position.controller[c].controller

						RSTA_findConstantScalar cont obj
					)
				)
			)
			else
			(
				if classOf a.position.controller == Position_List then						
				(
					for p = 1 to a.position.controller.count do 
					(
						if classof a.position.controller[p].controller == Position_XYZ do 
						(
							for c = 1 to 3 do 
							(
								if classof a.position.controller[p][c].controller == Float_Expression do 
								(
									obj = a
									cont = a.position.controller[p][c].controller

									RSTA_findConstantScalar cont obj
								)
							)
						)
					)
				)
			)	

			if classof a.rotation.controller == Euler_XYZ then
			(
				for c = 1 to 3 do 
				(
					if classof a.rotation.controller[c].controller == Float_Expression do 
					(
						obj = a
						cont = a.rotation.controller[c].controller

						RSTA_findConstantScalar cont obj
					)
					if classof a.rotation.controller[c].controller == LookAt_Constraint do 
					(
						appendIfUnique lookAtContollers a.name
					)					
				)				
			)
			else
			(
				if classof a.rotation.controller == Rotation_List do 
				(
					for p = 1 to a.rotation.controller.count do 
					(
						if classof a.rotation.controller[p].controller == Euler_XYZ do 
						(
							for c = 1 to 3 do 
							(
								if classof a.rotation.controller[p][c].controller == Float_Expression do 
								(
									obj = a
									cont = a.rotation.controller[p][c].controller

									RSTA_findConstantScalar cont obj
								)
								if classof a.rotation.controller[p][c].controller == LookAt_Constraint do 
								(
									appendIfUnique lookAtContollers a.name
								)
							)
						)
					)		
				)
			)
			
			if classOf a.scale.controller != bezier_scale do
			(
				--format ("Testing scale on "+(a as string)+"\n")
				--format ("classof "+a.name+".scale.controller : "+(classof a.scale.controller as string)+"\n")
				if classof a.scale.controller == scale_list then
				(
					--format ("Scale list found on "+a.name+"\n")
					for p = 1 to a.scale.controller.count do 
					(
						if classof a.scale.controller[p].controller == ScaleXYZ do
						(
							for c = 1 to 3 do 
							(
								if classof a.scale.controller[p][c].controller == Float_Expression do 
								(
									obj = a
									cont = a.scale.controller[p][c].controller

									RSTA_findConstantScalar cont obj
								)
							)
						)
					)		
				)
				else
				(					
					if classof a.scale.controller == ScaleXYZ do
					(
						--format ("ScaleXYZ found on "+a.name+"\n")
						for c = 1 to 3 do 
						(
							if classof a.scale.controller[c].controller == Float_Expression do 
							(
								obj = a
								cont = a.scale.controller[c].controller

								RSTA_findConstantScalar cont obj
							)
						)
					)					
				)
			)
		)
	)
	

-- 	--now loop through for expression strings
	for a in sela do 
	(
-- 		if classof a != IES_Sky do 
		if (classof a != IES_Sky) and (classof a != Targetobject) and (classof a != Targetcamera) do
		(
			if classOf a.position.controller == Position_XYZ then
			(
				for c = 1 to 3 do 
				(
					if classof a.position.controller[c].controller == Float_Expression do 
					(
						obj = a
						cont = a.position.controller[c].controller

						validZero = RSTA_findZeroExpressionString cont obj 
						
						if validZero == false do 
						(
							RSTA_deleteEmptyScalarTargets cont obj 
						)
					)
				)
			)
			else
			(
				if classOf a.position.controller == Position_List then						
				(
					for p = 1 to a.position.controller.count do 
					(
						if classof a.position.controller[p].controller == Position_XYZ do 
						(
							for c = 1 to 3 do 
							(
								if classof a.position.controller[p][c].controller == Float_Expression do 
								(
									obj = a
									cont = a.position.controller[p][c].controller

									validZero = RSTA_findZeroExpressionString cont obj 
									
									if validZero == false do 
									(
										RSTA_deleteEmptyScalarTargets cont obj 
									)
								)
							)
						)
					)
				)
			)	

			if classof a.rotation.controller == Euler_XYZ then
			(
				for c = 1 to 3 do 
				(
					if classof a.rotation.controller[c].controller == Float_Expression do 
					(
						obj = a
						cont = a.rotation.controller[c].controller

						validZero = RSTA_findZeroExpressionString cont obj 
									
						if validZero == false do 
						(
							RSTA_deleteEmptyScalarTargets cont obj 
						)
					)
				)				
			)
			else
			(
				if classof a.rotation.controller == Rotation_List do 
				(
					for p = 1 to a.rotation.controller.count do 
					(
						if classof a.rotation.controller[p].controller == Euler_XYZ do 
						(
							for c = 1 to 3 do 
							(
								if classof a.rotation.controller[p][c].controller == Float_Expression do 
								(
									obj = a
									cont = a.rotation.controller[p][c].controller

									validZero = RSTA_findZeroExpressionString cont obj 
									
									if validZero == false do 
									(
										RSTA_deleteEmptyScalarTargets cont obj 
									)
								)
							)
						)
					)		
				)
			)
			
-- 			if classOf a.scale.controller != bezier_scale do
-- 			(
-- 				for p = 1 to a.scale.controller.count do 
-- 				(
-- 					if classof a.scale.controller[p].controller == ScaleXYZ do
-- 					(
-- 						for c = 1 to 3 do 
-- 						(
-- 							if classof a.scale.controller[p][c].controller == Float_Expression do 
-- 							(
-- 								obj = a
-- 								cont = a.scale.controller[p][c].controller

-- 								validZero = RSTA_findZeroExpressionString cont obj 
-- 								
-- 								if validZero == false do 
-- 								(
-- 									RSTA_deleteEmptyScalarTargets cont obj 
-- 								)
-- 							)
-- 						)
-- 					)
-- 				)		
-- 			)
			
			if classOf a.scale.controller != bezier_scale do
			(
				--format ("Testing scale on "+(a as string)+"\n")
				
				if classof a.scale.controller == scale_list then
				(
					for p = 1 to a.scale.controller.count do 
					(
						if classof a.scale.controller[p].controller == ScaleXYZ do
						(
							for c = 1 to 3 do 
							(
								if classof a.scale.controller[p][c].controller == Float_Expression do 
								(
									obj = a
									cont = a.scale.controller[p][c].controller

									validZero = RSTA_findZeroExpressionString cont obj 
									
									if validZero == false do 
									(
										RSTA_deleteEmptyScalarTargets cont obj 
									)
								)
							)
						)
					)		
				)
				else
				(
					if classof a.scale.controller == ScaleXYZ do
					(
						for c = 1 to 3 do 
						(
							if classof a.scale.controller[c].controller == Float_Expression do 
							(
								obj = a
								cont = a.scale.controller[c].controller

								validZero = RSTA_findZeroExpressionString cont obj 
								
								if validZero == false do 
								(
									RSTA_deleteEmptyScalarTargets cont obj 
								)
							)
						)
					)					
				)
			)			
		)
	)
	
	if lookAtContollers.count > 0 do
	(
		format ("LookAt controllers found on:\n")
		for la in lookAtContollers do
		(
			format la.name+"\n"
		)
		--messagebox ((lookatControllers.count as string)+" lookAt controllers found in scene. These are not currently supported by the expression system in motionbuilder. Results will be broken.") beep:true title:"Exp export Warning!" 
	)
	
	format "Done.\n"
)



RSTA_checkEmptyExpression()