	
markerNames = #(
-- "Marker_Root",
-- "Marker_Pelvis",
-- "Marker_L_Thigh",
-- "Marker_L_Calf",
-- "Marker_L_Foot",
-- "Marker_R_Thigh",
-- "Marker_R_Calf",
-- "Marker_R_Foot",
-- "Marker_R_Toe0",
-- "Marker_R_Toe0Nub",
-- "Marker_Spine0",
-- "Marker_Spine1",
-- "Marker_Spine2",
-- "Marker_Spine3",
-- "Marker_L_Clavicle",
-- "Marker_L_UpperArm",
-- "Marker_L_Forearm",
-- "Marker_Neck1",
-- "Marker_Head",
-- "Marker_HeadNub",
-- "Marker_R_Clavicle",
-- "Marker_R_UpperArm",
-- "Marker_R_Forearm",
-- "Marker_L_Toe0",
-- "Marker_L_Toe0Nub",
-- "Marker_L_Hand",
-- "Marker_R_Hand",
-- "Marker_R_Finger10",
-- "Marker_R_Finger11",
-- "Marker_R_Finger12",
-- "Marker_R_Finger1Nub",
-- "Marker_R_Finger20",
-- "Marker_R_Finger21",
-- "Marker_R_Finger22",
-- "Marker_R_Finger2Nub",
-- "Marker_R_Finger30",
-- "Marker_R_Finger31",
-- "Marker_R_Finger32",
-- "Marker_R_Finger3Nub",
-- "Marker_R_Finger40",
-- "Marker_R_Finger41",
-- "Marker_R_Finger42",
-- "Marker_R_Finger4Nub",
-- "Marker_R_Finger00",
-- "Marker_R_Finger0Nub",
-- "Marker_R_Finger02",
-- "Marker_R_Finger01",
-- "Marker_L_Finger00",
-- "Marker_L_Finger01",
-- "Marker_L_Finger02",
-- "Marker_L_Finger0Nub",
-- "Marker_L_Finger10",
-- "Marker_L_Finger11",
-- "Marker_L_Finger12",
-- "Marker_L_Finger1Nub",
-- "Marker_L_Finger20",
-- "Marker_L_Finger21",
-- "Marker_L_Finger22",
-- "Marker_L_Finger2Nub",
-- "Marker_L_Finger30",
-- "Marker_L_Finger31",
-- "Marker_L_Finger32",
-- "Marker_L_Finger3Nub",
-- "Marker_L_Finger40",
-- "Marker_L_Finger41",
-- "Marker_L_Finger42",
-- "Marker_L_Finger4Nub",
-- "Marker_L_Finger_CurlRoot",
-- "Marker_R_Finger_CurlRoot",
-- "Marker_L_thumbhelper1",
-- "Marker_L_thumbhelper2",
-- "Marker_R_thumbhelper2",
-- "Marker_R_thumbhelper1",
-- "Marker_L_Finger_CurlEnd",
-- "Marker_R_Finger_CurlEnd"	
	)	

fn buildMarkerNames = 
(
	for o in objects do 
	(
		if (substring o.name 1 6) == "Marker" do 
		(
			appendIfUnique markerNames o.name
-- 			mkr = o
-- 			skelNode = getNodeByName ("SKEL"+(substring o.name 7 -1))
-- 			
-- 				if skelNode != undefined then
-- 				(
-- 					mkr.position = skelNode.position
-- 				)
-- 				else
-- 				(
-- 					print ("Couldn't find "+("SKEL"+(substring o.name 7 -1)))
-- 				)
		)
	)
)	
	
fn matchMarkerToSkel = 
(
	buildMarkerNames()
	
	for m = 1 to markerNames.count do
	(
		mkr = getNodeByName markerNames[m]
		
		for jnt in objects do 
		(
			if jnt.name == ("SKEL_"+(substring markerNames[m] 8 -1)) do 
			(
				print ("Aligning "+mkr.name+" to "+jnt.name)
				mkr.position = jnt.position
			)
		)
	)
	

	nubNames = #(
		#( --nubs
			"Marker_R_Toe0Nub",
			"Marker_HeadNub",
			"Marker_L_Toe0Nub",
			"Marker_R_Finger0Nub",			
			"Marker_R_Finger1Nub",
			"Marker_R_Finger2Nub",
			"Marker_R_Finger3Nub",
			"Marker_R_Finger4Nub",
			"Marker_L_Finger0Nub",
			"Marker_L_Finger1Nub",
			"Marker_L_Finger2Nub",
			"Marker_L_Finger3Nub",
			"Marker_L_Finger4Nub"			
		),
		#( --parents
			"Marker_R_Toe0",
			"Marker_Head",
			"Marker_L_Toe0",
			"Marker_R_Finger02",
			"Marker_R_Finger12",
			"Marker_R_Finger22",			
			"Marker_R_Finger32",			
			"Marker_R_Finger42",
			"Marker_L_Finger02",
			"Marker_L_Finger12",
			"Marker_L_Finger22",			
			"Marker_L_Finger32",			
			"Marker_L_Finger42"			
		)
-- 		#( --parents
-- 			"Marker_R_Foot",
-- 			"Marker_Neck1",
-- 			"Marker_L_Foot",
-- 			"Marker_R_Finger01",
-- 			"Marker_R_Finger11",
-- 			"Marker_R_Finger21",			
-- 			"Marker_R_Finger31",			
-- 			"Marker_R_Finger41",
-- 			"Marker_L_Finger01",
-- 			"Marker_L_Finger11",
-- 			"Marker_L_Finger21",			
-- 			"Marker_L_Finger31",			
-- 			"Marker_L_Finger41"			
-- 		)		
	)
	
	for m = 1 to markerNames.count do
	(
		mkr = getNodeByName markerNames[m]	
		
-- 		for i = 1 to nubNames[1].count do 
-- 		(
-- 			if mkr.name == nubNames[1][i] do 
-- 			(
-- 				node1 = getNodeByName nubnames[2][i]
-- 				node2 = getNodeByName nubnames[3][i]
-- 				dist = distance node1 node2
-- 				
-- 				mkr.transform = node1.transform
-- 				
-- 				mkr.position.controller.x_position = dist
-- 			)
-- 		)	
		
		for i = 1 to nubNames[1].count do 
		(
			mkr = getNodeByName nubNames[1][i]
			skelNode = getNodeByName ("SKEL_"+(substring nubNames[2][i] 8 -1))
				
			if skelNode != undefined then 
			(
				initPar = mkr.parent
				mkr.transform = skelNode.transform
				mkr.parent = skelNode
				in coordsys parent mkr.position.controller.X_Position = skelNode.length
				mkr.parent = initPar 
			)
			else
			(
				print ("Couldn't find "+("SKEL_"+(substring mkr.name 8 -1)))
			)
		)
	)
)	

matchMarkerToSkel()