-- selectedArray = #($SKEL_ROOT, $SKEL_Pelvis, $SKEL_L_Thigh, $SKEL_L_Calf, $SKEL_L_Foot, $SKEL_L_Toe0, $SKEL_L_Toe0_NUB, $SKEL_R_Thigh, $SKEL_R_Calf, $SKEL_R_Foot, $SKEL_R_Toe0, $SKEL_R_Toe0_NUB, $SKEL_Spine_Root, $SKEL_Spine0, $SKEL_Spine1, $SKEL_Spine2, $SKEL_Spine3, $SKEL_L_Clavicle, $SKEL_L_UpperArm, $SKEL_L_Forearm, $SKEL_L_Hand, $SKEL_L_Finger00, $SKEL_L_Finger01, $SKEL_L_Finger02, $SKEL_L_Finger0_NUB, $SKEL_L_Finger10, $SKEL_L_Finger11, $SKEL_L_Finger12, $SKEL_L_Finger1_NUB, $SKEL_L_Finger20, $SKEL_L_Finger21, $SKEL_L_Finger22, $SKEL_L_Finger2_NUB, $SKEL_L_Finger30, $SKEL_L_Finger31, $SKEL_L_Finger32, $SKEL_L_Finger3_NUB, $SKEL_L_Finger40, $SKEL_L_Finger41, $SKEL_L_Finger42, $SKEL_L_Finger4_NUB, $SKEL_R_Clavicle, $SKEL_R_UpperArm, $SKEL_R_Forearm, $SKEL_R_Hand, $SKEL_R_Finger00, $SKEL_R_Finger01, $SKEL_R_Finger02, $SKEL_R_Finger0_NUB, $SKEL_R_Finger10, $SKEL_R_Finger11, $SKEL_R_Finger12, $SKEL_R_Finger1_NUB, $SKEL_R_Finger20, $SKEL_R_Finger21, $SKEL_R_Finger22, $SKEL_R_Finger2_NUB, $SKEL_R_Finger30, $SKEL_R_Finger31, $SKEL_R_Finger32, $SKEL_R_Finger3_NUB, $SKEL_R_Finger40, $SKEL_R_Finger41, $SKEL_R_Finger42, $SKEL_R_Finger4_NUB, $SKEL_Neck_1, $SKEL_Head, $SKEL_Head_NUB)

selectedArray = #()
dummyNode = "Dummy01"

--got this from onmline very VERY fast hierarchy selector
--http://forums.cgsociety.org/archive/index.php/t-734159.html
fn getAllChildren obj &arr:#() =
(
	for c in obj.children do
	(
		append arr c
		append selectedArray c
		getAllChildren c arr:arr
	)
	arr
)


fn queryFloatExpression obj = 
(
	---------
	if (classof obj.rotation.controller as string) == "rotation_list" do
	(
		--debugPrint ("Rotation List found on "+obj.name)
		for i = 1 to obj.rotation.controller.count do
		(
			thisCnt = obj.rotation.controller
			thisName = thisCnt.getName i
			if thisName == "Keyframe XYZ" do
			(
				thisCnt.setName i "Zero Euler XYZ"
				print ("Renamed rotation controller "+(i as string)+" on "+obj.name+" from "+thisName+" to "+"Zero Euler XYZ")
			)
			
			if thisName == "Inital Pose" do
			(
				thisCnt.setName i "Frozen Rotation"
				print ("Renamed rotation controller "+(i as string)+" on "+obj.name+" from "+thisName+" to "+"Frozen Rotation")
			)			
		)
	)

)

fn initialiseMe = 
(
	currSel = selection as array
	dummyNode = getNodeByName dummyNode
	
	select dummyNode
	
	getAllChildren $ arr:(selection as array)
	
	for o = 1 to selectedArray.count do
	(
		queryFloatExpression selectedArray[o]
	)
	
	select currSel
)

initialiseMe()