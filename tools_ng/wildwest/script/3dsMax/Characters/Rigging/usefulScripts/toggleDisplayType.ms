--script to swap object to shaded/textured

currentDisp = 1


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



if ((animRangeGUI != undefined) and (animRangeGUI.isDisplayed)) do
	(destroyDialog animRangeGUI)

rollout animRangeGUI "Mat Rox"
(
	button btnSetDisp "Toggle"
	
	on btnSetDisp pressed do
	(
		if currentDisp == 1 then
		(
			displayColor.shaded = #object
			currentDisp = 0
		)
		else
		(
			displayColor.shaded = #material
			currentDisp = 1
		)
	)
	
)

CreateDialog animRangeGUI width:80 pos:[1450, 100] 