------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
try (destroyDialog mirroringRollout) catch ()

obSel = selection as array
subLvl = subObjectLevel
currentSelectedBone = undefined
-- if obSel.count == 1 then
-- (
-- 	if obSel[1].modifiers[#Skin] != undefined then
-- 	(
-- 		totalVerts = skinOps.GetNumberVertices obSel[1].modifiers[#Skin]
-- 		selVerts = #()
-- 		for i = 1 to totalVerts do
-- 		(
-- 			if (skinOps.IsVertexSelected obSel[1].modifiers[#Skin] i == 1) do
-- 			(
-- 				append selVerts i
-- 				print ("Appending vert "+(i as string))
-- 			)
-- 		)	
-- 	)
-- 	else
-- 	(
-- 		Messagebox ("Object has no skin modifer applied.") beep:true title:"WARNING!"
-- 	)
-- )
-- else
-- (
-- 	Messagebox ("Please run with only one object selected.") beep:true title:"WARNING!"
-- )

rollout mirroringRollout "Mirror Per-Vert" width:180 height:180
(
	button btnPreview "Prev" pos:[10,5] width:31 height:40 tooltip:"Preview the mirror"
	button btnUNPreview "Un-Prev" pos:[45,5] width:43 height:40 tooltip:"Un-Preview the mirror"
	button btnMirrorPasteSelection "Mirror Sel" pos:[91,5] width:71 height:40
	button btnCopy "Copy Indiv" pos:[20,70] width:71 height:40
	button btnPaste "Paste indiv" pos:[91,70] width:71 height:40
	button btnSelCopy	"Select Copy" pos:[20,120] width:71 height:30
	button btnSelPaste	"Select Pasted" pos:[91,120] width:71 height:30		
	progressBar pbProgress "ProgressBar" pos:[7,52] width:164 height:14
	
	spinner spnVerTol "Vtx Tol" pos:[20,160] width:71 range:[0,10,0.03] type:#float scale:0.005 tooltip:"Vertex matching tolerance"

	--------------------------------------------------------------------------------------------------------------------------------------------
	-- Values:
	--------------------------------------------------------------------------------------------------------------------------------------------
	local initVertSelection = #() --copy from verts

	local copiedWeightArray = #()
	
	local masterArray = #(
		#(), --mesh vert (array of all mesh verts
		#(), --copyFrom vert
		#() --vert matching tol
		)

	local boneList = 	#( --list of skin modifier indeces and the corresponding bone names
	#(),
	#()
	)

	local indivCopyVert = undefined --used for indiv copy vert
	local indivPastevert = undefined --used when pasting selected verts

	local skinMod = undefined 	
	local skinMesh = undefined 

	local tolDefault = 0.03
	
	local mirrorVol = undefined
	
	
	--------------------------------------------------------------------------------------------------------------------------------------------
	-- Functions:
	--------------------------------------------------------------------------------------------------------------------------------------------
	fn debugPrint printStr = 
	(
		if debugPrintVal == true do 
		(
			print (printStr)
		)
	)

	fn buildBoneList = --builds a list of paired arrays of bone ID's and their corresponding names
	(
		totalBones = skinOps.GetNumberBones skinMod 
		
		for b = 1 to totalBones do 
		(
			append boneList[1] b --appends bone index
			currentBonename = skinOps.getBoneName skinMod b 0
			append boneList[2] currentBonename
		)
		
	)

	fn findOppositeBone boneName = 
	(
		left = "_L_"
		right = "_R_"
		
		mirroredBoneName = undefined
		
		sideStart = findString boneName left
		if sideStart != undefined then --first we'll try and check if the original is left and replace _L_ with _R_
		(
			mirroredBoneName = replace boneName sideStart 3 right
				--debugPrint ("Opposite of "+boneName+": "+mirroredBoneName+"(was LEFT)" )
		)
		else
		( --ok we didnt find _L_ so we'll look for _R_
			sideStart = findString boneName right
			if sideStart != undefined then
			(
				mirroredBoneName = replace boneName sideStart 3 left
					--debugPrint ("Opposite of "+boneName+": "+mirroredBoneName+"(was RIGHT)" )
			)		
			else --ok we didnt find _L_ or _R_ so this joint must be a centreline one
			(
				mirroredBoneName = boneName
					--debugPrint ("Opposite of "+boneName+": "+mirroredBoneName+"(seems CENTRED)" )
			)
		)
		
		return mirroredBonename
	)

	fn reNomalize vertexId =
	(
		skinOps.unNormalizeVertex skinMod vertexID false
	)

	--if the vert has already been weighted tyhen the denormalizing will nacker up so we have to be a bit funky here!
	fn deNormalize vertexID = 
	(
		skinOps.unNormalizeVertex skinMod vertexID false
		skinOps.SetVertexWeights skinMod vertexID 1 1.0
		skinOps.unNormalizeVertex skinMod vertexID true
		skinOps.SetVertexWeights skinMod vertexID 1 0.0	
	)
		
	fn pasteVert copyFromVert pasteTovert = 
	(
		buildBoneList() --first off build the bbone list so we dont have to do the loop all the time
		
		--first we need to unnormalize verts
		deNormalize pasteTovert
		
		-- now we set all influences for every bone to zero
		totalBones = skinOps.GetNumberBones skinMod 
		
		for b = 1 to totalBones do
		(
			skinOps.SetVertexWeights skinMod pasteTovert b 0.0	
		)	
		
		--now we look at each influence on the copy from and find the corresponding opposite bone
		noOfInfluences = skinOps.GetVertexWeightCount skinMod copyFromvert
		for inf = 1 to noOfInfluences do 
		(
			--get the id of the current bone influence
			affectedBones = skinOps.GetVertexWeightBoneID skinMod copyFromvert inf
				--debugPrint ("affectedBones:"+(affectedBones  as string))
					
			--get the bone name of the current bone influence
			currentBonename = skinOps.getBoneName skinMod affectedBones 0
				--debugPrint ("currentBonename:"+(currentBonename  as string))	

					--get the influence of the current bone influence
			infForCurrBone = skinOps.getvertexWeight skinMod copyFromvert inf
				--debugPrint ("infForCurrBone: "+(infForCurrBone as string))
			
			--find the name of the mirrored version of the current bone influence
			mirrorBone = findOppositeBone currentBonename
				--debugPrint ("mirrorBone: "+(mirrorBone as string))

					--now we need to find the index of mirrorBone inside the skin modifier
			boneIndex = finditem boneList[2] mirrorBone
				--debugPrint ("boneIndex: "+(boneIndex as string))

					--now we take the vertex weight off the original joint and then paste that onto the mirrored one
			skinOps.SetVertexWeights skinMod pasteTovert boneIndex infForCurrBone
					
				--debugPrint "------------------------------"
		)
			
		--now we re normalize
		
		reNomalize pasteTovert 
	)
	
	fn initSkinMirror =
	(
		mirroringRollout.pbProgress.color = green
		modPanel.setCurrentObject skinMesh.modifiers[#Skin]
		if subObjectLevel != 1 do 
		(
			subobjectLevel = 1	
		)
		
		for i = 1 to masterArray[1].count do
		(

			if masterArray[3][i] != undefined do 
			(			
				copyFromVert = masterArray[2][i]
				pasteToVert = masterArray[1][i]
				pasteVert copyFromVert pasteToVert
				
				--need to update the progress bvar
				mirroringRollout.pbProgress.value = 100.*i/masterArray[3].count
			)
		)
		
		mirroringRollout.pbProgress.value = 100.0 --HACK!
		print ("Skin mirroring completed.")
	)	
	
	fn findMirroredVert =
	(
		mv = getNodeByName "perVertBall"
		
		if mv != undefined then
		(
			startTime = timestamp()
			print ("Starting mirror match")
			mirroringRollout.pbProgress.color = blue
			setCommandPanelTaskMode #create		

			undo off (
				
					select skinMesh 	

					addModifier skinmesh (Vol__Select ())
					skinMesh.modifiers[#Vol__Select].volume = 3
					skinMesh.modifiers[#Vol__Select].node = mirrorVol
					skinMesh.modifiers[#Vol__Select].level = 1	
						
				for v = 1 to initVertSelection.count do 
				(
					tol = tolDefault
					selVertNo = initVertSelection[v]
					
					selectedVertPos = skinMesh.vertices[selVertNo].pos
					mirroredPos = selectedVertPos 
					mirroredPos.x = (mirroredPos.x * -1)	
					
					--ok we'll build a vol select sphere at mirroredPos and find verts within that.
	-- 				mirrorVol = Sphere radius:tol smooth:on segs:32 chop:0 slice:off sliceFrom:0 sliceTo:0 mapcoords:on recenter:off transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [-0.882635,0,0.523521]) isSelected:off
	-- 				mirrorVol.name = "perVertBall"
					mirrorVol.position = mirroredPos
	-- 				mirrorVol.name = ("VolVert_"+(initVertSelection[v] as string))
					

	-- 				select skinMesh 	

	-- 				addModifier skinmesh (Vol__Select ())
	-- 				skinMesh.modifiers[#Vol__Select].volume = 3
	-- 				skinMesh.modifiers[#Vol__Select].node = mirrorVol
	-- 				skinMesh.modifiers[#Vol__Select].level = 1	
						
					--now we add a new edit poly so we can get the vetr selection
					selectedMirroredVertArray =#()
					selectedMirroredVertArray = skinMesh.mesh.selectedVerts as bitarray
					
					selVerts = #()
					for i in selectedMirroredVertArray do 
					(
						append selVerts i
					)			
						

					for i in selVerts do 
					(
						--ok so we need to add a match to the arrays 2nd index of the copy from vert
						--by using thisDist we can decide if the pivot of mirrorVol (ie copyFrom vert pos) is a closer match than a previous mirrorVol
						thisDist = distance skinMesh.vertices[i].pos mirrorVol.pos 
						if masterArray[3][i] == undefined then
						(
							masterArray[2][i] = selVertNo
							masterArray[3][i] = thisDist
						)
						else
						(
							if thisDist < masterArray[3][i] do
							(
								masterArray[2][i] = selVertNo
								masterArray[3][i] = thisDist
							)
						)
					)

					--now we can delete the vol select mod and the vol sphere

	-- 				deleteModifier skinMesh 1 --deletes vol select
	-- 				delete mirrorVol --deletes vol select mesh
					
					--repick the skin mesh and go into sub object sel
					modPanel.setCurrentObject skinMesh.modifiers[#Skin]
					if subObjectLevel != 1 do 
					(
						subobjectLevel = 1
					)
					
					mirroringRollout.pbProgress.value = 100.*v/initVertSelection.count
				)
				deleteModifier skinMesh 1 --deletes vol select
			)-- turn undo back on

		endTime = timestamp()
		print ("Mirror matching complete")
		format "Mirror matching took % seconds" ((endTime - startTime) / 1000)
		setCommandPanelTaskMode #modify
		)
		else
		(
			messagebox ("WARNING! perVertBall not found. Please adjust vtxTol to regenerate.")
		)
	)

	fn buildinitSelection = 
	(
		modPanel.setCurrentObject skinMesh.modifiers[#Skin]
		totalVerts = skinOps.GetNumberVertices skinMod
		
		initVertSelection = #()
		vertArray = #()
		
		for i = 1 to totalverts do 
		(
			append vertArray i
		)
		
		for i = 1 to vertArray.count do
		(
			if (skinOps.IsVertexSelected skinMod i == 1) do
			(
				append initVertSelection i
			)
		)	

		vertArray.count = 0	
		
		print ("initSelection initialised. Count:"+(initVertSelection.count as string))
	)

	fn buildMasterArray = --builds the master array of verts which we can go through to match verts with
	(
		skinMesh = $
		skinMod = skinMesh.modifiers[#Skin]
		
		if skinMod != undefined then 
		(
			setCommandPanelTaskMode #modify
			modPanel.setCurrentObject skinMesh.modifiers[#Skin]
			if subObjectLevel != 1 do 
			(
				subobjectLevel = 1
			)
			masterArray = #( --RESET TO DEFAULTS
				#(), --mesh vert (array of all mesh verts
				#(), --copyFrom vert
				#() --vert matching tol
				)
			
			for vert = 1 to skinMesh.vertices.count do
			(
				append masterArray[1] vert
				append masterArray[2] undefined --we first off set this to undefined
				append masterArray[3] undefined
			)
			print ("Total vert count:"+(skinMesh.vertices.count as string))
			print ("Master array initialised")
		)
		else
		(
			messageBox ("Please run with verts selected on a skinned mesh") beep:true
			return false
		)
	)
	
	--------------------------------------------------------------------------------------------------------------------------------------------
	-- Events:
	--------------------------------------------------------------------------------------------------------------------------------------------
	on mirroringRollout open do 
	(
		if $.modifiers[#Skin] != undefined do 
		(
			if currentSelectedBone == undefined do 
			(
				currentSelectedBone = skinOps.GetSelectedBone $.modifiers[#Skin]
			)
		)


		
		select obSel
		
		tolDefault = spnVerTol.value
		
		mirrorVol = getNodeByName "perVertBall"	
		if mirrorVol == undefined do 
		(
			mirrorVol = Sphere radius:tolDefault smooth:on segs:32 chop:0 slice:off sliceFrom:0 sliceTo:0 mapcoords:on recenter:off transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [-0.882635,0,0.523521]) isSelected:off
			mirrorVol.name = "perVertBall"		
			mirrorVol.position = [0,0,0.8] --default position at hip height
			mirrorVol.wirecolor = Green
		)
		
		select obSel
-- 		subObjectLevel = subLvl
-- 		
-- 		skinOps.SelectVertices obSel.modifiers[#Skin] selVerts
		
-- 		if selection.count == 1 do 
		--aa = selection as array

		if obSel.count == 1 do 
		(
			if $.modifiers[#Skin] != undefined do 
			(
				subObjectLevel = subLvl
				
				skinOps.SelectVertices obSel[1].modifiers[#Skin] selVerts				
				
				mirrorVol.center = $.center
			)
		)
		
		if currentSelectedBone != undefined do 
		(
			print ("Selecting from open "+(currentSelectedBone as string))
			skinOps.SelectBone obSel[1].modifiers[#Skin] currentSelectedBone
		)		
	)
	
	on mirroringRollout close do 
	(
		mirrorVol = getnodebyname "perVertBall"
		if mirrorVol != undefined do 
		(
			delete mirrorVol
			
		)
		gc()
	)
	
	on spnVerTol changed val do 
	(
		tolDefault = spnVerTol.value
		debugPrint ("tol changed to "+(tolDefault as string))
		mirrorVol.radius = tolDefault

		if selection.count == 1 do 
		(
			if $.modifiers[#Skin] != undefined do 
			(
				mirrorVol.center = $.center
			)
		)			
	)
	
		
	on btnPreview pressed do 
	(
		print ("tolDefault = "+(tolDefault as string))
		buildMasterArray()
		buildinitSelection()
		findMirroredVert()
		
		mirroredArray = #()
		--now we select all the mirrored verts
		for i = 1 to masterArray[1].count do 
		(
			if masterArray[3][i] != undefined do 
			(
				append mirroredArray masterArray[1][i]
			)
		)
		
		print ("mirroredArray.count:"+(mirroredArray.count as string))
		skinOps.selectvertices skinMod mirroredArray
		modPanel.setCurrentObject skinMesh.modifiers[#Skin]
		
		if currentSelectedBone != undefined do 
		(
			print ("Selecting from btn preview "+(currentSelectedBone as string))
			skinOps.SelectBone skinMesh.modifiers[#Skin] currentSelectedBone
		)

	
		if subObjectLevel != 1 do 
		(
			subobjectLevel = 1	
		)
	)
	
	on btnUnPreview pressed do 
	(
		if initVertSelection  != undefined then
		(
			skinOps.SelectVertices skinMod initVertSelection 
			if currentSelectedBone != undefined do 
			(
				print ("Selecting from btnUnPreview "+(currentSelectedBone as string))
				skinOps.SelectBone skinMesh.modifiers[#Skin] currentSelectedBone
			)			
		)
		else
		(
			messagebox ("Could not retrieve the inital preview selection.") beep:true
		)
	)
	
	on btnMirrorPasteSelection pressed do
	(
		gc()
		skinOps.CopyWeights  (modPanel.GetcurrentObject()) --copy the weights for the selected verts
		pbProgress.color = blue
		valid = buildMasterArray()
		
		if valid != false do 
		(
			buildInitSelection()
			findMirroredVert()

			initSkinMirror()
		)
		
		skinOps.PasteWeights  (modPanel.GetcurrentObject()) --paste back the copied verts in case the mirror had mirrored onto any of them
		if currentSelectedBone != undefined do 
		(
			print ("Selecting from mirrorPasteSelection "+(currentSelectedBone as string))
			skinOps.SelectBone skinMesh.modifiers[#Skin] currentSelectedBone
		)			
		gc()
	)

	on btnCopy pressed do
	(
		indivCopyVert = undefined --reset
		
		buildMasterArray()
		
		totalVerts = skinOps.GetNumberVertices skinMod
		vertArray = #()
		for i = 1 to totalverts do 
		(
			if (skinOps.IsVertexSelected skinMod i == 1) do
			(
				append vertArray i
			)
		)
		
		if vertArray.count == 1 then
		(
			for i = 1 to vertArray.count do
			(
				if (skinOps.IsVertexSelected skinMod vertArray[i] == 1) do
				(
					indivCopyVert = vertArray[i]
					Print ("indivCopyVert = "+(indivCopyVert as string))
				)
			)		
		)
		else
		(
			messagebox ("Copy Indiv only valid with 1 vert selected. Sel count:"+(vertArray.count as string)) beep:true
		)
		
		if currentSelectedBone != undefined do 
		(
			print ("Selecting from copy "+(currentSelectedBone as string))
			skinOps.SelectBone $.modifiers[#Skin] currentSelectedBone
		)		
	)

	on btnPaste pressed do
	(
		totalVerts = skinOps.GetNumberVertices skinMod
		
		selectedVerts = 0
		
		for i = 1 to totalVerts do
		(		
			if skinOps.IsVertexSelected skinMod i != 0 do
			(
				selectedverts = (selectedverts + 1)
				pasteVert indivCopyVert i 
				indivPastevert = i
			)
		)
		
		if selectedVerts == 0 do 
		(
			messagebox ("WARNING! It looks like you didnt pick anything to paste to!") beep:true
		)
		
		if currentSelectedBone != undefined do 
		(
			print ("Selecting "+(currentSelectedBone as string))
			skinOps.SelectBone skinMesh.modifiers[#Skin] currentSelectedBone
		)
		
		gc()
	)

	on btnSelCopy pressed do
	(	
		debugPrint ("attempting to pick "+(indivCopyVert as string))
		skinOps.SelectVertices skinMod indivCopyVert  
		if currentSelectedBone != undefined do 
		(
			print ("Selecting from select copy"+(currentSelectedBone as string))
			skinOps.SelectBone skinMesh.modifiers[#Skin] currentSelectedBone
		)				
	)

	on btnSelPaste pressed do
	(
		debugPrint ("attempting to pick "+(indivPasteVert as string))
		skinOps.SelectVertices skinMod indivPasteVert
		if currentSelectedBone != undefined do 
		(
			print ("Selecting from selpaste "+(currentSelectedBone as string))
			skinOps.SelectBone skinMesh.modifiers[#Skin] currentSelectedBone
		)					
	)
	
)

if obSel.count == 1 then
(
	
	if obSel[1].modifiers[#Skin] != undefined then
	(
		currentSelectedBone = skinOps.GetSelectedBone obSel[1].modifiers[#Skin]
		
		if currentSelectedBone != undefined do 
		(
			print ("Selecting from initialisation "+(currentSelectedBone as string))
			skinOps.SelectBone obSel[1].modifiers[#Skin] currentSelectedBone
		)		
		
		totalVerts = skinOps.GetNumberVertices obSel[1].modifiers[#Skin]
		selVerts = #()
		for i = 1 to totalVerts do
		(
			if (skinOps.IsVertexSelected obSel[1].modifiers[#Skin] i == 1) do
			(
				append selVerts i
				--print ("Appending vert "+(i as string))
			)
		)	
		
		createDialog mirroringRollout
	)
	else
	(
		Messagebox ("Object has no skin modifer applied.") beep:true title:"WARNING!"
	)
)
else
(
	Messagebox ("Please run with only one object selected.") beep:true title:"WARNING!"
)

-- createDialog mirroringRollout
	