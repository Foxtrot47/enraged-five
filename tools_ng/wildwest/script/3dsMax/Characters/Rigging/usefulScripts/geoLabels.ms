--script to read object labels off objects then save to a file so they can be reloaded if they get lost
--Nov 2011
--Matt Rennie

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

geoObjects = #()

objData = #(
	#() --obj name
	)

-- inputName = undefined
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	
fn addLabels = 
(
	geoObjects = #()

	objData = #(
		#() --obj name
		)	
	
	print ("Attempting to add labels")
	inputData = #() -- define as array
	
	if input_name == undefined do
	(
		input_name = getOpenFileName caption:"Geo Labels" types:"geoNames (*.geoNames)|*.geoNames|All Files (*.*)|*.*|"
	)

	if input_name != undefined then
	(
		f = openfile input_name
-- 		inputData = #() -- define as array
		while not eof f do
		(
			append inputData (filterstring (readLine f) ",")
		)
	  close f
	)
	
	print (inputData as string)
	if inputData.count > 0 do
	(
		for o in objects do
		(
			thisname = o.name
			thisNameLength = thisName.count
			
			for i = 1 to inputData.count do
			(
				if (substring inputData[i][1] 1 thisNameLength) == o.name do
				(
					print ("Changing "+o.name+" to "+inputData[i][1])
					o.name = inputData[i][1]
				)
			)
		)
	)
)

fn readLabels charRoot = 
(
	geoObjects = #()

	objData = #(
		#() --obj name
		)
	select charRoot
	if selection.count == 1 then
	(
		if (classof charRoot as string) == "Dummy" then
		(
			--ok find the children of the root so we can work on them
			for o in objects do
			(
				if o.parent == charRoot do
				(
					appendIfUnique geoObjects o
				)
			)
			
			for i = 1 to geoObjects.count do
			(
				print geoObjects[i].name
				append objData[1] geoObjects[i].name
			)

			--ok now we need to output the geoObjects data to a file
			
			
			output_name = getSaveFileName caption:"Geo Labels" types:"geoNames (*.geoNames)|*.geoNames|All Files (*.*)|*.*|"
	-- 		
			if output_name != undefined then 
			(
				output_file = createfile output_name		

				for i = 1 to objData[1].count do
				(
					format ((objData[1][i])+","+"\n") to:output_file
				)
				
				close output_file
				print ("Data saved to "+output_name)
			)
			
		)
		else
		(
			messagebox (charRoot.name+" is not a valid Dummy object")
		)
	)
	else
	(
		messagebox ("WARNING! Please only select the character root dummy")
	)
	
)

-- clearListener()

if ((geoLabelGUI != undefined) and (geoLabelGUI.isDisplayed)) do
	(destroyDialog geoLabelGUI)

rollout geoLabelGUI "Geo labeller"
(
	checkBox chkDebugPrnt "DebugPrint" checked:false tooltip:"Check to enable debug print output."
	
	button btnExport "Export" width:110 tooltip:"Export Geometry labels."
	button btnImport "Import" width:110 tooltip:"Re-create geo labels"

	on geoLabelGUI open do
	(
		chkDebugPrnt.state = false
		debugPrintVal = false
		print ("Debug printing defaulting to: "+(debugPrintVal as string))
	)
	
	on chkDebugPrnt changed theState do
	(
		if chkDebugPrnt.state == false then
		(
			debugPrintVal = false
			print "Debug printing disabled."
		)
		else
		(
			debugPrintVal = true
			print "Debug printing enabled."
		)
	)
		
	on btnExport pressed do
	(
		readLabels $
	)
	
	on btnImport pressed do
	(
		print "btn clicked"
		addLabels()
	)	
)

CreateDialog geoLabelGUI width:125 pos:[1450, 100] 