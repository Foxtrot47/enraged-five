idxIsClothCollision = ( GetAttrIndex "Gta Collision" "Is Cloth Collision" )

fn findRemoveBoneList obj =
(
	skinMod = obj.modifiers[#Skin]
	select obj
	max modify mode
	totalBones = skinOps.getNumberBones skinMod
	totalVerts = skinOps.getNumberVertices skinMod
	
	skinModBones = #()
	
	for i = 1 to totalBones do 
	(
		thisBone = skinOps.GetBoneName skinMod i 1
		append skinModBones thisBone
	)
	
	for b = skinModBones.count to 1 by -1 do 
	(
		thisBoneName = skinModBones[b]
		thisBoneNode = GetNodeByName thisBoneName
		keepThisBone = false
		
		if( IsValidNode thisBoneNode ) then
		(
			for child in thisBoneNode.children do
			(
				if( GetAttrClass child == "Gta Collision" ) then
				(
					if( GetAttr child idxIsClothCollision ) then
					(	
						keepThisBone = false
					)
				)
			)
		)

		foundVert = undefined 
		for v = 1 to totalVerts do --now we test each vert and see if this bones is weighted to it
		(
			if( keepThisBone == true ) then exit
			
			weightCount = skinOps.GetVertexWeightCount skinMod v --first get the no of influences on this vert
			
			for vB = 1 to weightCount do 
			(
				tB = skinOps.getVertexWeightBoneID skinMod v vB
				
				if tB == b do --this means we have found the match to the skinModBones bones we are on
				(
					vtWeight = skinOps.getVertexWeight skinMod v vB
					
					if vtWeight != undefined do 
					(
						if vtWeight >= 0.0 do 
						(
							keepThisBone = true
							continue
						)
					)
				)
			)
		)
		
		if keepThisBone == false then 
		(
			print ("Removing "+thisBoneName+" from "+obj.name)
			skinOps.SelectBone skinMod b
			skinOps.removebone skinMod			
		)
		else
		(
			print ("Keeping "+thisBoneName+" from "+obj.name)
		)	
	)

	skinOps.addBone skinMod $SKEL_ROOT 1
)

selA = selection as array

for a = 1 to sela.count do 
(
	if sela[a].modifiers[#Skin] != undefined do 
	(
		try (findRemoveBoneList sela[a]) catch (Print (sela[a].name+" has a prob!"))
		--findRemoveBoneList sela[a]
	)
)
messagebox ("Removal COMPLETE!") beep:true