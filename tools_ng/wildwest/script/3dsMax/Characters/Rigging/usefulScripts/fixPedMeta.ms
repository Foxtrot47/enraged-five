filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

-- nameTypes = #(
-- 	#("male_std", "expr_set_ambient_male"),
-- 	#("male_std_shorts", "expr_set_ambient_male_shorts"),
-- 	#("male_std_shortsLong", "expr_set_ambient_male_shortsLong"),
-- 	#("male_std_skirt", "expr_set_ambient_male_skirt"),
-- 	#("female_std", "expr_set_ambient_female"),
-- 	#("female_heels_std", "expr_set_ambient_female_heels"),
-- 	#("female_hiheels_std", "expr_set_ambient_female_hiheels"),
-- 	#("female_heels_skirt", "expr_set_ambient_female_heels_skirt"),
-- 	#("female_std_skirt", "expr_set_ambient_female_skirt")
-- 	)

nameTypes = #(
	#(".//ExpressionName[text() = \"male_std\"]", "expr_set_ambient_male"),
	#(".//ExpressionName[text() = \"male_std_shorts\"]", "expr_set_ambient_male_shorts"),
	#(".//ExpressionName[text() = \"male_std_shortsLong\"]", "expr_set_ambient_male_shortsLong"),
	#(".//ExpressionName[text() = \"male_std_skirt\"]", "expr_set_ambient_male_skirt"),
	#(".//ExpressionName[text() = \"female_std\"]", "expr_set_ambient_female"),
	#(".//ExpressionName[text() = \"female_heels_std\"]", "expr_set_ambient_female_heels"),
	#(".//ExpressionName[text() = \"female_hiheels_std\"]", "expr_set_ambient_female_hiheels"),
	#(".//ExpressionName[text() = \"female_heels_skirt\"]", "expr_set_ambient_female_heels_skirt"),
	#(".//ExpressionName[text() = \"female_std_skirt\"]", "expr_set_ambient_female_skirt")
	)

fn fixPedMeta searchStr repStr = 
(
	--Peds.meta does not exist in dev_ng location
	local pedMetaPath = "x:/gta5/build/dev/common/data/peds.meta"
	
	xmlStream = XmlStreamHandler xmlFile:pedMetaPath
	xmlStream.open()
	
-- 	local expressionNodes = xmlStream.root.SelectNodes ".//ExpressionName[text() = \"male_std\"]"
-- 	try(
-- 	exString = (".//ExpressionName[text() = "+"\\"+"\""+searchStr+"\\"+"\"]")
	
	local expressionNodes = xmlStream.root.Selectnodes searchStr
	
	local expressIt = expressionNodes.GetEnumerator()
	
-- 	count = 0
	
	while expressIt.MoveNext() do
	(
-- 		count = (count + 1)
		local thisNode = expressIt.current
		
		local parentNode = thisNode.parentNode
		
		local expressSetName = ( parentNode.GetElementsByTagName "ExpressionSetName").itemof [0]
		local expressDictName = (parentNode.GetElementsByTagName "ExpressionDictionaryName").itemof [0]
		
		if expressSetName == undefined do
		(

			expressSetName = thisNode.createElement("ExpressionSetName")
		)
		
-- 		print ("count: "+(count as string))
		
-- 		expressSetName.innerText = "something"
		expressSetName.innerText = repStr
		expressDictName.innerText = "null"
		thisNode.innerText = "null"
	)
	
	
	tempPath = "c:/pedoMetaTemp.meta"
	
	xmlStream.XML.save(tempPath)
	
	xmlStream.close()
	
	xmlStream = XmlStreamHandler xmlFile:tempPath
	xmlStream.open()
	xmlStream.XML.save(pedMetaPath)
	xmlStream.close()
	
	--now delete the tempPath
	
-- 	deleteFile tempPath
-- 	)
-- 	catch (xmlStream.close)
	
)

for i = 1 to nameTypes.count do 
(
	fixPedMeta nameTypes[i][1] nameTypes[i][2]
	
	if i == nameTypes.count do 
	(
		print "DONE!"
	)
)

-- fixPedMeta "male_std" "expr_set_ambient_male"