--scriupt to reparent thigh rolls to thighs 

filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-- filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Includes\\FN_Rigging.ms")	
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")


-- skinningInputOutputGUI = undefined
boneList = #()
--imported from skin data loader tool
--***********************************************************************************************************************************************************************************
fn LSW_EnvelopeCallbackFunction =
(
	WindowHandle = DialogMonitorOPS.GetWindowHandle()
	theDialogName = UIAccessor.GetWindowText WindowHandle

	if theDialogName != undefined and matchpattern theDialogName pattern:"*Load Envelopes*" do
		UIAccessor.PressButtonByName WindowHandle "Match by Name"	
		
	if theDialogName != undefined and matchpattern theDialogName pattern:"*Load Envelopes*" do
		UIAccessor.PressButtonByName WindowHandle "OK"

	true
)

fn outputBonesData geo output_Name = 
(
	max modify mode
-- 	Geo = selection

	skinMod = geo.modifiers[#Skin]
	totalSkinModBones = skinOps.getNumberBones skinMod

		if totalSkinModBones > 0 then
		(	
			if output_name != undefined then 
			(
				output_file = createfile output_name		

				for i = 1 to totalSkinModBones do
				(
					thebonename = skinOps.getBoneName skinMod  i 1
					append boneList theBoneName
					format ((theBoneName as string)+"\n") to:output_file
				)
				
				close output_file
					--edit output_name --opens the file in a maxscript window so it can be checked.
				
			)--end if
		)--end if
)



FN outputSkinningData geo =
(
	thisSkin = geo.modifiers[#Skin]
	if thisSkin != undefined do
	(
		thisSkinFolder = "c:\\skins\\"
		thisSkinSubFolder = (filterstring maxfilename ".")
		thisSkinSubFolder = (thisSkinSubFolder[1]+"\\")
		makeDir (thisSkinFolder+thisSkinSubFolder)
		max modify mode
		select geo
		skinFileName = (thisSkinFolder+thisSkinSubFolder+geo.name+".env")
		--now we'll try and delete the existing env file first
		skinFiles = getFiles (thisSkinFolder+thisSkinSubFolder+"*.env")
		for i = skinFiles.count to 1 by -1 do 
		(
			if skinFiles[i] == skinFileName do
			(
				--ok we delete the existing file
				deleteFile skinFiles[i]
			)
		)
		
		skinOps.saveEnvelope thisSkin skinFileName
-- 		skinOps.saveEnvelopeAsASCII thisSkin (thisSkinFolder+thisSkinSubFolder+geo.name+".env")
		output_name = ("c:/skins/"+thisSkinSubFolder+geo.name+".bon")
		outputBonesData geo output_name 
		print ("Bone data output for "+geo.name+". Saved in "+output_Name)
		print ("Skin data output for "+geo.name+". Saved in "+(thisSkinFolder+thisSkinSubFolder+geo.name+".env"))
	)
)


fn runOutput = 
(
	if selection.count == 0 then
	(
		messageBox ("Please pick your geometry.") beep:true
	)
	else
	(
		selA = selection as array
		for i in selA do
		(
			outputSkinningData i
		)		
	)
)

fn inputBonesData geo input_name = 
(
	if geo.modifiers[#Skin] != undefined do
	(
		skinMod = geo.modifiers[#Skin]

		boneList = #() --reinitialise to empty
		
		if input_name != undefined then
		(
			f = openfile input_name
			inputData = #() -- define as array
			
			while not eof f do
			(
			append inputData (filterstring (readLine f) ",")
			)
			close f

			for i in inputData do --read in all the bone name data to an array
			(
				currBonename = (i as string)
				currBoneLength = currBonename.count
				currBone = ((substring currBonename 4 (currBonelength - 5)))
					
					currentBone = getNodeByName currBone
					if currentBone != undefined then	
					(
						appendIfUnique boneList currentBone
					)
					else
					(
						print ("Couldn't find "+currBone+" in this scene.")
					)
			)
			
			select geo
			SetCommandPanelTaskMode mode:#modify			
			modPanel.setCurrentObject $.modifiers[#Skin]
			
			for i = 1 to boneList.count do --add the bones into the skin modifier
			(				
				boneToAdd = bonelist[i]
				
				boneUpdateInt = undefined
				if i != bonelist.count then
				(
					boneUpdateInt = 0
				)
				else
				(
					--ok we're at the count so we need to refresh
					boneUpdateInt = 1
				)
				
				if boneToAdd != undefined do
				(
					skinOps.addbone skinMod boneToAdd boneUpdateInt
				)
				
			)
		)
		print ("Loaded Bones:"+input_name+" on to "+geo.name)
	)
)

fn applySkinning geo envFile = 
(
	DialogMonitorOPS.RegisterNotification LSW_EnvelopeCallbackFunction ID:#ANoon_Envelopes
	DialogMonitorOPS.Enabled = true
	
	completeRedraw()
	skinOps.loadEnvelope geo.modifiers[#Skin] envFile
-- 	skinOps.loadEnvelopeAsASCII geo.modifiers[#Skin] envFile
	completeRedraw()

	DialogMonitorOPS.Enabled = false
	DialogMonitorOPS.UnRegisterNotification ID:#ANoon_Envelopes	
	
	
	print ("Loaded Skin: "+envFile+" onto "+geo.name)
)

fn inputSkinningData geo =
(
	thisSkinFolder = "c:\\skins"
	maxName = (filterString maxfileName ".")
	maxName = maxName[1]
	thisSkinSubFolder = (geo.name)
	
	slashChar = "\\"
	
	boneFileName = (thisSkinFolder+slashChar+maxName+slashChar+thisSkinSubFolder+".bon")
	skinFileName = (thisSkinFolder+slashChar+maxName+slashChar+thisSkinSubFolder+".env")
	
	bonFiles = getFiles (thisSkinFolder+slashChar+maxName+slashChar+"*.bon")
	skinFiles = getFiles (thisSkinFolder+slashChar+maxName+slashChar+"*.env")
	
	--first clear off existing mod
	if geo.modifiers[#Skin] != undefined do 
	(
		for i = geo.modifiers.count to 1 by -1 do
		(
			if classOf geo.modifiers[i] as string == "Skin" do 
			(
				deleteModifier geo geo.modifiers[i]
			)
		)
	)
	
	--now add a new skin modifier
	addModifier geo (Skin ())
	geo.modifiers[#Skin].bone_Limit = 4
	geo.modifiers[#Skin].showNoEnvelopes = on		
	
	--now we'll see if we can find the relevant bon file
	boneFile = undefined 
	for i = 1 to bonFiles.count do 
	(
		if bonFiles[i] == boneFileName do --ok inut the bone data
		(
			--print ("Found bone: "+boneFileName)
			boneFile = bonFiles[i]
			inputBonesData geo boneFile
		)
	)
	
	skinFile = undefined
	for i = 1 to skinFiles.count do --ok input the skin data
	(
		if skinFiles[i] == skinFileName do 
		(
			--print ("Found skin: "+skinFileName)
			skinFile = skinFiles[i]
			applySkinning geo skinFile
		)
	)	
)

--***********************************************************************************************************************************************************************************


fn reparentThighRollBones dummyObj = --run with the character dummy picked
(
	if classof dummyObj == Dummy then 
	(
		meshObjsA = #()
		meshObjs = #()
		
		--first off we'll build a valid list of mesh objects
		for o in dummyObj.children do 
		(
			if o.modifiers[#Skin] != undefined do 
			(
				objName = lowercase o.name
				
				appendIfUnique meshObjsA objname
			)
		)
		
		--now we'll do a filein of the skinning io tool to save weights out
		
		--we'll sort meshObjs alphabetically
-- 		for o in meshObjs do 
-- 		(
-- 			
-- 		)
-- 		
		sort meshObjsA --alphabetise the list so listener output is easier to trace
		
		for o in meshObjsA do 
		(
			obj = getNodeByName o
			append meshObjs obj
		)
		select meshObjs
-- 		filein (RsConfigGetWildWestDir() + "script\\3dsMax\\Characters\\Rigging\\SaveAndLoadFullSkinningData.ms")
-- 		filein (RsConfigGetWildWestDir() + "script\\3dsMax\\Characters\\Rigging\\SaveAndLoadFullSkinningData.ms")
		runOutput()
		
		--now we've got the skinning saved out we can do the reparenting
		
		sides = #(
			"R",
			"L"
			)
		
		for side in sides do 
		(
			Thigh = getNodeByName ("SKEL_"+side+"_Thigh")
			Rollbone = getNodeByName ("RB_"+side+"_ThighRoll")
			
			select Rollbone
			freezeTransform()
			Rollbone.parent = thigh
			freezeTransform()
			
			--now we need to build the expression
			
			expObj = rollbone
			expCont = (".rotation.controller.Zero_Euler_XYZ.controller."+"X"+"_Rotation.controller")
			scalarNames = #("ThighRotX")								
			scalarConts = #("$"+Thigh.name+".rotation.controller.Zero_Euler_XYZ.controller.X_Rotation.controller")			
			expString = (scalarNames[1]	+ " * -0.5")
			
			generateExpression expObj expCont scalarNames scalarConts expString --creates the main expression		
		)
		
		--now we need to reload the skinning
		
		for meshobj in meshObjs do 
		(
			inputSkinningData meshObj
		)
		
		select dummyObj
		
		if ((skinningInputOutputGUI != undefined) and (skinningInputOutputGUI.isDisplayed)) do
		(destroyDialog skinningInputOutputGUI)	
		
		
		format ("Thigh reparenting completed."+"\n")
		messagebox ("Thigh reparenting completed."+"\n")
	)	
	else
	(
		messagebox "Please pick the character Dummy and try again." beep:true title:"WARNING!"
	)
)


if selection.count == 1 then
(	
	reparentThighRollBones $
)
else
(
	messagebox "Please pick the character Dummy and try again." beep:true title:"WARNING!"
)