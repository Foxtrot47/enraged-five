------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()


exprJoints = #()
exprObjects = #()
found = undefined
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn findSkinnedExpObj geo = 
(
-- 	found = false
	skinModBones = #()
	if geo.modifiers[#Skin] != undefined do 
	(
-- 		while found != true do 
-- 		(
		
		--select the skin mod
		
		setCommandPanelTaskMode #modify

		val = geo.modifiers.count
		for modif = 1 to val do
		(
		  if (geo.modifiers[modif] as string) == "Skin:Skin" do 
		  (
			modPanel.setCurrentObject geo.modifiers[modif]
		  )
		)
		
		skinMod = geo.modifiers[#Skin]
		totalBones = skinOps.getNumberBones skinMod

		for i = 1 to totalBones do 
		(
			thisBone = skinOps.GetBoneName skinMod i 1
			append skinModBones thisBone
		)		
	

		for j = 1 to exprJoints.count do 
		(
			expJointName = exprJoints[j].name
			for b = 1 to skinModBones.count do 
			(
				thisBoneName = skinModBones[b]
				if thisBoneName == expJointName do 
				(
					appendIfUnique exprObjects geo
-- 						found = true
				)
			)
		)
-- 		)
	)
)

fn findExpressionsComponent =
(
	--first off build the array of all expression objects
	filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/findExpressionObjects.ms")

	selCount = getNumNamedSelSets()
	SelName = "*Expressions"
	currItems = #()
	
	for i = selCount to 1 by -1 do 
	(
		if getNamedSelSetName i == "*Expressions" then
		(
			SelName = "*Expressions"
			--now record what items already exist in that set
			setItems = getNamedSelSetItemCount i
			
			for a = 1 to setItems do
			(
				thisItem = getNamedSelSetItem i a 
				appendIfUnique exprJoints thisItem
			)
		)	
		
		if GetNamedSelSetName i == "*ExpressionObjects" do 
		(
				deleteItem selectionSets "*ExpressionObjects"
		)
	)

	selA = selection as array --put all the selected geo's into an array
	
	for geo in sela do 
	(
		findSkinnedExpObj geo
	)
	
	selectionSets["*ExpressionObjects"] = exprObjects
	select exprObjects
	print ("ExpressionObjects selection set generated.")
)

findExpressionsComponent()