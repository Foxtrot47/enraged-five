-- tool to auto cleanup geo and add skin modifier
--converts geo to epoly
-- this will weld coincident verts
-- reset xforms
-- add skin modifier
-- set skin to 4 wpv
-- turn off display of envelopes

-----------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------



toleranceValue = 0.0001

-----------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------

fn cleanupGeo geo = 
(
	--first convert geo to epoly
	addModifier geo (Edit_Poly ())
	collapseStack geo
		
	--now weld verts within a tolerance
	--first we select all verts	
	subObjectLevel = 1
	vSel = geo.verts as bitarray
	geo.weldThreshold = toleranceValue
	polyop.weldVertsByThreshold geo vSel
	subObjectLevel = 0

	--now reset xform
	addModifier geo (XForm ())	
	collapseStack geo	
	
	--now add skin modifier
	addModifier geo (Skin ())
	geo.modifiers[#Skin].bone_Limit = 4
	geo.modifiers[#Skin].showNoEnvelopes = on
		
	print ("Cleaned "+geo.name+". ")
)


fn prepCleanup selA = 
(
	if selA.count == 0 then
	(
		messagebox ("WARNING! Please select your meshes") beep:true
	)
	else
	(
		for i in selA do
		(
			if classOf i == PolyMeshObject then
			(
				cleanupGeo i
			)
			else
			(
				if classOf i == Editable_mesh then
				(
					cleanupGeo i
				)
				else
				(
					if classOf i == Editable_Poly then
					(
						cleanupGeo i
					)	
					else					
					(
						print ("Couldnt clean "+i.name)
						)
				)
			)
		)
	)
)

selA = selection as array
prepCleanup selA