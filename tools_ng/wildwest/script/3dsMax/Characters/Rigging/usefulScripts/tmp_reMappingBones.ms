arrayOfBones = #( --list of all bones in skin modifier this can eventually be queried via skin mod
	$A,
	$B,
	$C,
	$D,
	$E,
	$F,
	$G
	)
	
initialBonesToRemove = # --initial list of bones to remove
	(
		$F,
		$C,
		$E
	)

bonesToRemove = #() --dynamically generated array of all bones which need to be removed (if we were to remove bone C then its children will need removing also
	
boneMapping = #( --dynamically generated 2d array. id1 = initial bone, id2 = bone to remap to
	)

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
fn reNomalize vertexId =
(
	skinOps.unNormalizeVertex skinMod vertexID false
)

--if the vert has already been weighted tyhen the denormalizing will nacker up so we have to be a bit funky here!
fn deNormalize vertexID = 
(
	skinOps.unNormalizeVertex skinMod vertexID false
	skinOps.SetVertexWeights skinMod vertexID 1 1.0
	skinOps.unNormalizeVertex skinMod vertexID true
	skinOps.SetVertexWeights skinMod vertexID 1 0.0	
)

fn applyReMappedSkinning obj vertData = 
(
	skinMod = obj.modifiers[#Skin]
	
	vertCount = undefined
	
	if classof obj == Editable_mesh then
	(
		vertCount = meshop.getNumVerts obj.baseobject
	)
	else
	(
		vertCount = polyOp.getNumVerts obj.baseobject
	)
	
	--unnormalize weights

	for vertexID = 1 to vertCount do 
	(
		deNormalize vertexID
		
		weightCount = skinOps.GetVertexWeightCount skinMod vertexId
		--now loop via the weightCount
		for wt = 1 to weightCount do
		(
			for vtw = 1 to vertData.count do 
			(
				if vertData[vtw][1] == vertexId do 
				(
					boneId = vertData[vtw][2]
					weightVal =vertData[vtw][3]
					
					if weightVal != 0 do 
					(
						skinOps.SetVertexWeights skinMod vertexId 
					)
				)
			)
		)
	)
	
	--set all influences to 0
	

	
	--apply new skinning (additively not replacing)
	
	--renormalize weights
)

fn reMapSkinning obj = 
(
	skinMod = obj.modifiers[#Skin]
	
	vertCount = undefined
	
	if classof obj == Editable_mesh then
	(
		vertCount = meshop.getNumVerts obj.baseobject
	)
	else
	(
		vertCount = polyOp.getNumVerts obj.baseobject
	)
	
-- 	weightCount = skinOps.GetVertexWeightCount skinMod 
	
	--now loop through each vert in the mesh and query the weight
		vertData = #() --multi dimensional array of vertIndex, boneIndex, boneWeight
	
	for vert = 1 to vertCount do 
	(
		weightCount = skinOps.GetVertexWeightCount skinMod vert
		--now loop via the weightCount
		for wt = 1 to weightCount do
		(
			boneItem = skinOps.GetVertexWeightBoneID skinMod vert wt --gets the index of the bone wt
			boneWeight = skinOps.GetVertexWeight skinMod vert wt --gets the weight value applied to bone wt

			tmpArr = #(vert, boneItem, boneWeight)
			append vertData tmpArr
		)
	)
	
-- 	format ("DEBUG"+"\n")
-- 	for i = 1 to vertData.count do 
-- 	(
-- 		format ((vertData[i] as string)+"\n")
-- 	)
-- 	format ("/DEBUG"+"\n")
	
-- break()	
	--now we have a full list of verts & their weighting, we need to look through arrayOfBones and then also the bones to remove dynamic array and for any matches, 
	--we then need to find a match for that in the id 2 of vertData and remap that weight
	
	for vert = 1 to vertData.count do 
	(
		--now find the index of the current boneItem in index 2
		
		currentVertBoneIndex = vertData[vert][2]
		
		foundMatch = undefined
		
		--need to find the index in the main bone array that matches boneMapping[currentVertBoneIndex][2]
	
		
	
		foundmatch = findItem arrayOfBones boneMapping[currentVertBoneIndex][2]
	
		if foundMatch == undefined do ( break())
		
		--now we have found the node we need to rpelace with we will change the value in vertData[vert][2] to match
		
		if vertData[vert][2] != foundMatch do 
		(
			fromBone = arrayOfBones[vertData[vert][2]]
			toBone = arrayOfBones[foundMatch]
-- 			print ("For vert "+(currentVertBoneIndex as string)+" replacing "+(vertData[vert][2] as string)+" with "+(foundMatch as string))
			print ("For vert "+(currentVertBoneIndex as string)+" replacing "+(fromBone.name as string)+" with "+(toBone.name as string))
			vertData[vert][2] = foundMatch
		)
	)
	
	applyReMappedSkinning obj vertData
)

fn buildBonesToRemove bonesToLose = 
(
	--go through the initialBonesToRemove array and add all of their children
	
	for b in initialBonesToRemove do 
	(
		boneChildren = b.children
		
		appendIfUnique bonesToRemove b
		
		for c in boneChildren do 
		(
			appendIfUnique bonesToRemove c
		)
	)
	
	format ("Initial removal nodes:"+"\n")
	for i in initialBonesToRemove do 
	(
		format (i.name+"\n")
	)
	
	format ("Total removal nodes:"+"\n")
	for b in bonesToRemove do 
	(
		format (b.name+"\n")
	)
	format ("---------------"+"\n")
)
	
fn boneRemap = 
(
	--first off build the total list of all nodes we will need to remove
	buildBonesToRemove bonesToLose 
	
	for i in arrayOfBones do 
	(
		parBone = i.parent
		
		if parBone != undefined then
		(
			--ok now we have a parent bone we need to see if i is in bonesToRemove
			foundToRemove = finditem bonesToRemove i
			
			if foundToRemove != 0 then
			(
				--ok so we need to remove bone 'i'
				--now wee need to iterate up through its parents until we find a bone that is not to be removed
				
				isValid = false
				
				boneToUse = parBone
				
				while isValid == false do 
				(
					parValid = findItem bonesToRemove parBone
					
					if parValid != 0 then --ok this parent is one we need to remove so we need to go higher
					(
						parBone = parBone.parent
						isValid = false
					)
					else
					(
						--ok we can stick weights to this bone
						boneToUse = parBone
						isValid = true
					)
				)
				
				tmpArr = #(i, boneToUse)
				print ((i.name)+" mapped to "+(boneToUse.name)+" <-- CHANGE")
				append boneMapping tmpArr
			)
			else
			(
				tmpArr = #(i, i)
				print ((i.name)+" mapped to "+(i.name))
				append boneMapping tmpArr
			)
		)
		else
		(
			tmpArr = #(i, i)
			print ((i.name)+" mapped to "+(i.name))
			append boneMapping tmpArr
		)
	)
)

fn skinXfer obj = 
(
	skinMod = obj.modifiers[#Skin]

	--first off build a list of all the bones in the skin modifier
	totalSkinBones = skinOps.GetNumberBones skinMod

	boneNames = #()
	
	select obj
	
	max modify mode
	modPanel.setCurrentObject $.modifiers[#Skin]
	
	for i = 1 to totalSkinBones do
	(
		thisBone = skinops.GetBoneName skinMod i 1
		
		append boneNames thisBone
	)
	
	--now do the bone remapping
	
	boneRemap()

-- for f = 1 to boneMapping.count do 
-- (
-- 	print ((f as string)+": "+(boneMapping[f] as string))
-- )	
	
	--now we have the bone remapping done we need to transfer the skinning
	reMapSkinning obj
)

clearListener()
obj = $Plane001
skinXfer obj