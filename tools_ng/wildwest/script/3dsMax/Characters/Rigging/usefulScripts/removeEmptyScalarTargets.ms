fn isExpressionStillValid cont obj = 
(
	local theScalarCount = cont.numScalars()
	
	if theScalarCount == 4 then
	(
		format ("Converting "+((exprForMaxObject cont) as string)+" on "+obj.name+" to Bezier_Float\n")
-- 		cont = Bezier_Float()
		contType = exprForMaxObject cont
		execStr = (contType+" = Bezier_Float()")
		execute execStr
	)
)

fn deleteScalarTargets cont obj mode = 
(
	local theScalarCount = cont.numScalars()
	
	local scalarsOk = true
	
	if mode == 1 do 
	(
-- 		for i = 5 to theScalarCount do
		for i = theScalarCount to 5 by -1 do
		(			
			thisScalarName = cont.GetScalarName i	
				
			thisScalarTgt = cont.GetScalarType thisScalarName
			
			if thisScalarTgt == #scalarConstant  do
			(
				print ("Bad scalar found on "+thisScalarName+" on "+((exprFormaxObject cont) as string))
-- 				append badScalars cont
				scalarsOk = false
			)
		)
	)
	
	if mode == 2 do
	(
-- 		if queryBox "Empty scalar variables found in expressions..\r\nDo you want to remove them?" beep:false title:"Expression QA" do
-- 		(
-- 			for i = 5 to theScalarCount do
			for i = theScalarCount to 5 by -1 do
			(			
				thisScalarName = cont.GetScalarName i	
				
				thisScalarTgt = cont.GetScalarType thisScalarName
				
				if thisScalarTgt == #scalarConstant  do
				(
					print ("Removing bad scalar on "+thisScalarName+" on "+((exprFormaxObject cont) as string))
					cont.DeleteVariable thisScalarName
				)
			)			
-- 		)
	)
	
	if mode == 2 then
	(
		isExpressionStillValid cont obj 
	)
	
	return scalarsOk
)

fn testEmptyScalars mode = 
(
	
	--sela = selection as array
	sela = objects as array
	
	noBadScalars = true
	
		for a in sela do 
		(
			if classof a != IES_Sky do 
			(
				format ("Testing "+a.name+"\n")	
				if classOf a.position.controller == Position_XYZ then
				(
					for c = 1 to 3 do 
					(
						if classof a.position.controller[c].controller == Float_Expression do 
						(
							obj = a
							cont = a.position.controller[c].controller
							scalarsOk = deleteScalarTargets cont obj mode
							if scalarsOk == false do 
							(
								noBadScalars = false
							)
						)
					)
				)
				else
				(
					
					
					if classOf a.position.controller == Position_List then						
					(
						for p = 1 to a.position.controller.count do 
						(
							if classof a.position.controller[p].controller == Position_XYZ do 
							(
				-- 				for c = 1 to a.position.controller[p].count do 
								for c = 1 to 3 do 
								(
									if classof a.position.controller[p][c].controller == Float_Expression do 
									(
										obj = a
										cont = a.position.controller[p][c].controller
										scalarsOk = deleteScalarTargets cont obj mode
										if scalarsOk == false do 
										(
											noBadScalars = false
										)
									)
								)
							)
						)
					)
				)	

				if classof a.rotation.controller == Euler_XYZ then
				(
					for c = 1 to 3 do 
					(
						if classof a.rotation.controller[c].controller == Float_Expression do 
						(
							obj = a
							cont = a.rotation.controller[c].controller
							scalarsOk = deleteScalarTargets cont obj mode
							if scalarsOk == false do 
							(
								noBadScalars = false
							)
						)
					)				
				)
				else
				(
					if classof a.rotation.controller == Rotation_List do 
					(
						for p = 1 to a.rotation.controller.count do 
						(
				-- 			for c = 1 to a.rotation.controller[p].count do 
							if classof a.rotation.controller[p].controller == Euler_XYZ do 
							(
								for c = 1 to 3 do 
								(
									if classof a.rotation.controller[p][c].controller == Float_Expression do 
									(
										obj = a
										cont = a.rotation.controller[p][c].controller
										scalarsOk = deleteScalarTargets cont obj mode
										if scalarsOk == false do 
										(
											noBadScalars = false
										)										
									)
								)
							)
						)		
					)
				)
				
				if classOf a.scale.controller != bezier_scale do
				(
					if classof a.scale.controller == ScaleXYZ then
					(
		-- 				for c = 1 to a.scale.controller[p].count do 
						for c = 1 to 3 do 
						(
							if classof a.scale.controller[c].controller == Float_Expression do 
							(
								obj = a
								cont = a.scale.controller[p][c].controller
								scalarsOk = deleteScalarTargets cont obj mode
								if scalarsOk == false do 
								(
									noBadScalars = false
								)									
							)
						)
					)					
					else
					(
						for p = 1 to a.scale.controller.count do 
						(
							if classof a.scale.controller[p].controller == ScaleXYZ do
							(
				-- 				for c = 1 to a.scale.controller[p].count do 
								for c = 1 to 3 do 
								(
									if classof a.scale.controller[p][c].controller == Float_Expression do 
									(
										obj = a
										cont = a.scale.controller[p][c].controller
										scalarsOk = deleteScalarTargets cont obj mode
										if scalarsOk == false do 
										(
											noBadScalars = false
										)									
									)
								)
							)
						)	
					)						
				)
			)
		)
-- obj = $

-- cont = $.position.controller[2][1].controller

-- deleteScalarTargets cont obj
	return noBadScalars
)

fn RSTA_removeEmptyScalars mode =
(
	allOk = testEmptyScalars mode
	
	if allOk == false do
	(		
		if queryBox "Empty scalar variables found in expressions..\r\nDo you want to remove them?" beep:false title:"Expression QA" do
		(
			testEmptyScalars 2
		)
	)
)

--set mode to 1 to merely print and not correct

RSTA_removeEmptyScalars 1