--script to add exportTrans and exportRot and exportScale tags to selection

filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_Rigging.ms")

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



fn cleanTags obj = 
(
	badTags = #(
	("\r\n"+"exportTrans = true"),
	("\r\n"+"exportTrans = false"),
	("\r\n"+"exportTrans=true"),
	("\r\n"+"exportTrans=false"),
	("\r\n"+"exportRot = true"),
	("\r\n"+"exportRot = false"),
	("\r\n"+"exportRot=true"),
	("\r\n"+"exportRot=false"),
	("\r\n"+"exportScale = true"),
	("\r\n"+"exportScale = false"),
	("\r\n"+"exportScale=true"),
	("\r\n"+"exportScale=false"),
	("\r\n"+"relRot=true"),
	("\r\n"+"relRot = true")		
	)		
	
	replaceWithMe = ""
	
	for badTag in badTags do
	(
		currentTag = getUserPropBuffer obj
		
		foundTag = findstring currentTag badTag
		
		if foundTag != undefined do 
		(
			print ("replacing "+badTag)
			newUdp = substituteString currentTag badTag replaceWithMe
			
			setUserPropBuffer obj newUdp
		)
	)
)
	
fn tagAdd obj = 
(
	controllerTypes = #(
	"Controller:Float_Expression",
	"Controller:RsSpring",
	"Controller:LookAt_Constraint"
	)

	
	--first off we need to remove any bad udp tags
	cleanTags obj
	
	addTrans = false
	addRot = false
	addScale = false
	debugPrint ("Testing "+obj.name)
	
	if (classof obj.position.controller as string) == "position_list" then
	(
		debugPrint ("Position List found on "+obj.name)
		
		for i = 2 to obj.position.controller.count do 
		(
-- 			print ("cont no = "+(i as string))
			for y = 1 to 3 do 
			(
-- 				print ("y:"+(y as string))
				for ct in controllerTypes do 
				(
					if (obj.position.controller[i][y].controller as string) == ct do 
					(
						addTrans = true
					)
				)
			)
		)
	)
	else
	(
		if (classof obj.position.controller as string) == "Position_XYZ" do
		(
			debugPrint ("Position XYZ found on "+obj.name)
			
			for y = 1 to 3 do 
			(
				if (obj.position.controller[y].controller as string) == "Controller:Float_Expression" do 
				(
					addTrans = true
				)
			)			
		)
	)

	if (classof obj.rotation.controller as string) == "rotation_list" then
	(
		debugPrint ("Rotation List found on "+obj.name)
		
		for i = 2 to obj.rotation.controller.count do 
		(
			for y = 1 to 3 do 
			(
				for ct in controllerTypes do 
				(
					if (obj.rotation.controller[i][y].controller as string) == ct do 
					(
						addRot = true
					)
				)
			)
		)		
	)
	else
	(
		if (classof obj.rotation.controller as string) == "Euler_XYZ" then
		(
			debugPrint ("Euler XYZ found on "+obj.name)
			
			for y = 1 to 3 do 
			(
				if (obj.rotation.controller[y].controller as string) == "Controller:Float_Expression" do 
				(
					addRot = true
				)
			)			
		)
		else
		(
			if (obj.rotation.controller as string) == "Controller:LookAt_Constraint" then
			(
				addRot = true
			)
			else
			(
				if (obj.rotation.controller as string) == "Controller:RsSpring" then
				(
					addRot = true
				)
			)
		)
	)

	if (classof obj.scale.controller as string) == "scale_list" then
	(
		debugPrint ("Scale List found on "+obj.name)
		
		for i = 2 to obj.scale.controller.count do 
		(
			for y = 1 to 3 do 
			(
				for ct in controllerTypes do 
				(
					if (obj.scale.controller[i][y].controller as string) == ct do 
					(
						addScale = true
					)
				)
			)
		)		
	)
	else
	(
		if (classof obj.scale.controller as string) == "Controller:ScaleXYZ" do
		(
			debugPrint ("Scale XYZ found on "+obj.name)

			for y = 1 to 3 do 
			(
				if (obj.scale.controller[y].controller as string) == "Controller:Float_Expression" do 
				(
					addScale = true
				)
			)			
		)
	)
	
	print "Setting tags"
	
	--now add the tags in
	currentTag = getUserPropBuffer obj
		print ("currentTag = "+currentTag)
	--now we'll do some tidying up to remove unwanted carriage returns
	linez = filterstring currentTag "\r\n"
	
	cUdp = ""
	for i = 1 to linez.count do 
	(
		cUdp = (cUdp+linez[i]+"\r\n")
	)
	
	currentTag = cUdp
	
	trans = ("exportTrans = "+(addTrans as string))
		print ("trans= "+trans)
	rota = ("exportRot = "+(addRot as string))
		print ("rota = "+rota)
	rotb = ("relRot = true")
		
	scl = ("exportScale = "+(addScale as string))
		print ("scl = "+scl)
	newUdp = (currentTag + trans + "\r\n" + rota +"\r\n" + rotB +"\r\n" + scl)
	
	setUserPropBuffer obj newUdp
)

for obj in selection do 
(
	tagAdd obj
)