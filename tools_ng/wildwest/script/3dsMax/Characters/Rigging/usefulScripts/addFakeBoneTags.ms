tagToRemove = #(
	"tag ="
	)
	
fn addFakeTags selA = 
(
	for o in selA do 
	(
		thisUdp = getUserPropBuffer o
		
		for tag = 1 to tagToRemove.count do 
		(
			foundTag = findString thisUdp tagToRemove[tag]
			if (foundTag == undefined) then
			(
				--ok we haven't found that tag so need to remove it 
				endTag = ("\r\n"+"tag = BONETAG_"+o.name)
				newUdp = (thisUdp+endTag)
				
				setUserPropBuffer o newUdp
				print ("UDP modified on "+o.name)
			)
			else -- makes sure that all the "tag" doesnt have any capital letter as this is causing exporting errors when having cloth objects.
			(
				newUdp = substituteString thisUdp "Tag" "tag"
				setUserPropBuffer o newUdp
				print ("UDP modified on "+o.name)
			)
		)
	)
)
clearListener()

selA = selection as array
if selA.count != 0 then
(
	addFakeTags selA
	print"done tagging."
)
else
(
	messagebox "Please select objects to be tagged first." beep:true
)

