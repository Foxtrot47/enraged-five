--script to record bind pose
filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

theProjectConfig = RsConfigGetProjBinConfigDir()

toolsRoot = RsConfigGetToolsRootDir()

filein (theWildWest+"/script/3dsMax/Characters/Rigging/rsta_bindpose.ms")
-- filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_Rigging.ms")
filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_UI.ms")
-- global recPasteBindPosGUI = undefined

bindPoseArray = #()

input_name = undefined
output_name = undefined


fn RSTA_getUiPosFile = 
(
	--THIS HAS TO BE DECLARED FROM  WITHIN THIS SCRIPT 
	
	uiPosFile = ((getThisScriptFilename()))
-- 	format (uiPosFile+"\n")

	filePath = filterString uiPosFile "."
		
	uiPosFile = (filePath[1]+"_UIPOSFILE.TXT")
		
-- 	format ("final "+uiPosFile+"\n")
		
	return uiPosFile
)


fn recBindPose = 
(
-- 	selA = objects as array
	
	output_name = getSaveFileName caption:"BindPose file" types:"BPos Data (*.bPos)|*.bpos|All Files (*.*)|*.*|"
-- 		
	if output_name != undefined then 
	(
		local sela = #()
		
		for obj in objects do 
		(
			if (substring obj.name 1 4) == "SKEL" do
			(
				appendifunique sela obj
			)
		)
			
		if (doesFileExist output_name) == true do
		(	
			deleteFile output_name
		)

		local cnt = 0
		rootObj = undefined 
		
		for obj in sela do 
		(
			if obj.parent == undefined do 
			(
				rotoObj = obj
				cnt = cnt + 1
			)
		)
		
		if rootObj != undefined then
		(
			if cnt == 1 then
			(				
				rsta_bindpose.SavePose rootObj filepath:output_name quiet:false		
			)
			else
			(
				format ("Too many root objects found: "+(cnt as string)+" in total! Cannot save bind pose\n")
				messagebox ("Too many root objects found: "+(cnt as string)+" in total!\n") Title:"BIND POSE SAVING ERROR"
			)
		)
		else
		(
			messagebox "WARNING Couldn't find a root obj\n"
		)
	)
-- 	print bindPoseArray
)


fn pasteBindPose = 
(
	input_name  = undefined
	if input_name == undefined do
	(
		input_name = getOpenFileName caption:"BindPose file" types:"BPosData (*.bPos)|*.bpos|All Files (*.*)|*.*|"
	)

	if input_name != undefined then
	(
-- 		f = openfile input_name
-- 		inputData = #() -- define as array
-- 		while not eof f do
-- 		(
-- 			append inputData (filterstring (readLine f) "\n")
-- 		)
-- 		close f

-- 		for i = 1 to inputData.count do
-- 		(
-- 			execute inputData[i][1]
-- 		)
		rsta_bindpose.LoadPose input_name nameFilter:"*.*" zeroHeel:false
	)	
)



















------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if ((recPasteBindPosGUI != undefined) and (recPasteBindPosGUI.isDisplayed)) do
	(destroyDialog recPasteBindPosGUI)

rollout recPasteBindPosGUI "Bind Pose Tools"
(
	button btnRec "Record BindPose" width:110
	button btnPaste "LoadBindPose" width:110

	on recPasteBindPosGUI open do 
	(
		uiPosFile = RSTA_getUiPosFile() --get the dialogPos file
		dialogPos = RSTA_DialogPosLoad uiPosFile --get the position form the dialog pos file
		format ("Attempting to set dialog pos to "+(dialogPos as string)+"\n")
		if dialogPos != undefined do
		(
			SetDialogPos recPasteBindPosGUI dialogPos
		)		
	)
	
	on recPasteBindPosGUI moved position do 
	(
		uiPosFile = RSTA_getUiPosFile() --get the dialogPos file
		RSTA_DialogPosRec recPasteBindPosGUI uiPosFile
	)	
	
	on btnRec pressed do
	(
		clearListener()
		recBindPose()
	)

	on btnPaste pressed do
	(
		pasteBindPose()
	)

)

CreateDialog recPasteBindPosGUI width:125 pos:[100, 100] 
