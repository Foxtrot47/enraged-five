

fn RSTA_repointScalar exprObj controller scalarName joystick axisUsed = 
(
	local theScalarCount = controller.numScalars()
	
	for i = 5 to theScalarCount do
	(
		local thisScalarName = controller.GetScalarName i
		
		if thisScalarName == scalarName do
		(
			if axisUsed == "X_Position" then
			(
-- 				controller.SetScalarTarget i Target:joystick.position.controller[2].X_Position.controller
				controller.SetScalarTarget i joystick.position.controller[2].X_Position.controller
			)
			else
			(
				if axisUsed == "Y_Position" then
				(
-- 					controller.SetScalarTarget i Target:joystick.position.controller[2].Y_Position.controller
					controller.SetScalarTarget i joystick.position.controller[2].Y_Position.controller
				)
				else
				(
					if axisUsed == "Z_Position" then
					(
-- 						controller.SetScalarTarget i Target:joystick.position.controller[2].Z_Position.controller
						controller.SetScalarTarget i joystick.position.controller[2].Z_Position.controller
					)
				)
			)
		)
	)
)


fn RSTA_queryScalarTarget obj controller targetToTestForArray objWithExprArray = 
(
-- 		objWithExprArray = #(
-- 		#(), --object
-- 		#(), --full controller path
-- 		#(), --scalar Name
-- 	 	#() --joystick name
--		#() --axisOfScalar used	
-- 		)	
	
	
	tgtObjfoundTarget = false
	
	local theScalarCount = controller.numScalars()
	
	posStringArray = #(
		"X_Position",
		"Y_Position",
		"Z_Position"
		)
	
	for i = 5 to theScalarCount do
	(
		--get scalar names

		local thisScalarName = controller.GetScalarName i
		--format ("thisScalarName: "+thisScalarName+"\n")
		--appendIfUnique exprScalarNames thisScalarName
							
		--append scalarData[1] thisScalarName
		--get scalar targets
		local thisScalarTgt = undefined
		
		try ( --this will stop crashes for scalars which are broken/constants
		thisScalarTgt = controller.GetScalarTarget thisScalarName asController:true
			)catch(
				--Format (thisScalarName+" not mapped for "+fullCont+"\n")
-- 						messagebox (thisScalarName+" not mapped for "+fullCont) beep:true
			thisData = #(
				obj,
				thisScalarName,
				fullCont
				)

		)

		local thisScalarTgtCont = exprformaxobject thisScalarTgt explicitNames:true
		
		for tgt = 1 to targetToTestForArray.count do 
		(
			local target = targetToTestForArray[tgt]

			local nameStr = filterString (thisScalarTgtCont as string) "."
			local objName = nameStr[1]
			if objName[1] == "$" do
			(
				objName = substring objName 2 -1
			)
			
			format ("objName: "+objName+"\n")
			local tgtObj = getNodeByName objName
			
			if target == tgtObj.Name do
			(
				foundTarget = true
				format ("Found match for joystick "+tgtObj.name+" in "+(thisScalarTgtCont as string)+"\n")
				

				
				append objWithExprArray[1] obj.name
-- 				append objWithExprArray[2] (thisScalarTgtCont as string)
				append objWithExprArray[2] (exprFormaxObject controller explicitNames:true)
				format ("Set "+"objWithExprArray[2]"+" to "+((exprFormaxObject controller explicitNames:true) as string)+"\n")
				append objWithExprArray[3] thisScalarName
				append objWithExprArray[4] target
				
				--now we need to find if its driven by x_position etc
				
				posType = undefined
				for pos in posStringArray do
				(
					fnd = findString (thisScalarTgtCont as string) pos
					
					if fnd != undefined do 
					(
						posType = pos
					)
				)
				append objWithExprArray[5] posType
			)
		)
		
	)
)
	
fn RSTA_Find_sclXYZ obj targetToTestForArray objWithExprArray controllersToInclude = 
(
	for cont in controllersToInclude do
	(
		if (classof obj.Scale.controller.X_Scale.controller as string) == cont do
		(
			RSTA_queryScalarTarget obj obj.Scale.controller.X_Scale.controller targetToTestForArray objWithExprArray
		)
		if (classof obj.Scale.controller.Y_Scale.controller as string) == cont do
		(
			RSTA_queryScalarTarget obj obj.Scale.controller.Y_Scale.controller targetToTestForArray objWithExprArray
		)	
		if (classof obj.Scale.controller.Z_Scale.controller as string) == cont do
		(
			RSTA_queryScalarTarget obj obj.Scale.controller.Z_Scale.controller targetToTestForArray objWithExprArray
		)		
	)
)

fn RSTA_Find_sclList obj targetToTestForArray objWithExprArray controllersToInclude = 
(
	for i = 2 to obj.scale.controller.count do --skip 1st controller as thats the frozen one
	(
		for cont in controllersToInclude do
		(		
			if (classof obj.scale.controller[i].X_scale.controller as string) == cont do
			(
				RSTA_queryScalarTarget obj obj.scale.controller[i].X_scale.controller targetToTestForArray objWithExprArray
			)
			if (classof obj.scale.controller[i].Y_scale.controller as string) == cont do
			(
				RSTA_queryScalarTarget obj obj.scale.controller[i].Y_scale.controller targetToTestForArray objWithExprArray
			)
			if (classof obj.scale.controller[i].Z_scale.controller as string) == cont do
			(
				RSTA_queryScalarTarget obj obj.scale.controller[i].Z_scale.controller targetToTestForArray objWithExprArray
			)		
		)
	)		
)

fn RSTA_Find_rotEulerXYZ obj targetToTestForArray controllersToInclude = 
(
	for cont in controllersToInclude do
	(
		if (classof obj.Rotation.controller.X_Rotation.controller as string) == cont do
		(
			RSTA_queryScalarTarget obj obj.Rotation.controller.X_Rotation.controller targetToTestForArray objWithExprArray
		)
		if (classof obj.Rotation.controller.Y_Rotation.controller as string) == cont do
		(
			RSTA_queryScalarTarget obj obj.Rotation.controller.Y_Rotation.controller targetToTestForArray objWithExprArray
		)	
		if (classof obj.Rotation.controller.Z_Rotation.controller as string) == cont do
		(
			RSTA_queryScalarTarget obj obj.Rotation.controller.Z_Rotation.controller targetToTestForArray objWithExprArray
		)	
	)
)

fn RSTA_Find_rotList obj targetToTestForArray objWithExprArray controllersToInclude = 
(
	for i = 2 to obj.rotation.controller.count do --skip 1st controller as thats the frozen one
	(
		for cont in controllersToInclude do
		(
			if (classof obj.rotation.controller[i].X_rotation.controller as string) == cont do
			(
				RSTA_queryScalarTarget obj obj.rotation.controller[i].X_rotation.controller targetToTestForArray objWithExprArray
			)
			if (classof obj.rotation.controller[i].Y_rotation.controller as string) == cont do
			(
				RSTA_queryScalarTarget obj obj.rotation.controller[i].Y_rotation.controller targetToTestForArray objWithExprArray
			)
			if (classof obj.rotation.controller[i].Z_rotation.controller as string) == cont do
			(
				RSTA_queryScalarTarget obj obj.rotation.controller[i].Z_rotation.controller targetToTestForArray objWithExprArray
			)		
		)
	)		
)

fn RSTA_Find_posXYZ obj targetToTestForArray objWithExprArray controllersToInclude = 
(
	for cont in controllersToInclude do
	(		
		if (classof obj.position.controller.X_Position.controller as string) == cont do
		(
			RSTA_queryScalarTarget obj obj.position.controller.X_Position.controller targetToTestForArray objWithExprArray
		)
		if (classof obj.position.controller.Y_Position.controller as string) == cont do
		(
			RSTA_queryScalarTarget obj obj.position.controller.Y_Position.controller targetToTestForArray objWithExprArray
		)	
		if (classof obj.position.controller.Z_Position.controller as string) == cont do
		(
			RSTA_queryScalarTarget obj obj.position.controller.Z_Position.controller targetToTestForArray objWithExprArray
		)
	)
)

fn RSTA_Find_posList obj targetToTestForArray objWithExprArray controllersToInclude = 
(
	for i = 2 to obj.position.controller.count do --skip 1st controller as thats the frozen one
	(
		for cont in controllersToInclude do
		(
			if (classof obj.position.controller[i].X_Position.controller as string) == cont do
			(
				RSTA_queryScalarTarget obj obj.position.controller[i].X_Position.controller targetToTestForArray objWithExprArray
			)
			if (classof obj.position.controller[i].Y_Position.controller as string) == cont do
			(
				RSTA_queryScalarTarget obj obj.position.controller[i].Y_Position.controller targetToTestForArray objWithExprArray
			)
			if (classof obj.position.controller[i].Z_Position.controller as string) == cont do
			(
				RSTA_queryScalarTarget obj obj.position.controller[i].Z_Position.controller targetToTestForArray objWithExprArray
			)		
		)
	)	
)

fn RSTA__queryFloatExpression obj targetToTestForArray objWithExprArray = 
(
	local controllersToInclude = #(
	"Float_Expression",
	"Controller:Float_Expression" ,
	"SubAnim:RsSpring",
	"SubAnim:LookAt_Constraint"
	)
	
	--print ("Testing "+obj.name)
	if (classof obj.position.controller as string) == "position_list" do
	(
		--print ("Position List found on "+obj.name)
		RSTA_Find_posList obj targetToTestForArray objWithExprArray controllersToInclude
	)
	if (classof obj.position.controller as string) == "Position_XYZ" do
	(
		--print ("Position XYZ found on "+obj.name)
		RSTA_Find_posXYZ obj targetToTestForArray objWithExprArray controllersToInclude
	)
	---------
	if (classof obj.rotation.controller as string) == "rotation_list" do
	(
		--print ("Rotation List found on "+obj.name)
		RSTA_Find_rotList obj targetToTestForArray objWithExprArray controllersToInclude
	)
	if (classof obj.position.controller as string) == "Euler_XYZ" do
	(
		--print ("Euler XYZ found on "+obj.name)
		RSTA_Find_rotEulerXYZ obj targetToTestForArray objWithExprArray controllersToInclude
	)
	--------------
	if (classof obj.scale.controller as string) == "scale_list" do
	(
		--print ("Scale List found on "+obj.name)
		RSTA_Find_sclList obj targetToTestForArray objWithExprArray controllersToInclude
	)
	if (classof obj.scale.controller as string) == "Controller:ScaleXYZ" do
	(
		--print ("Scale XYZ found on "+obj.name)
		RSTA_Find_sclXYZ obj targetToTestForArray objWithExprArray controllersToInclude
	)
	
)

fn RSTA_queryExpessionsForTargets obj targetToTestForArray objWithExprArray = 
(
	if classof obj != Targetcamera do 
	(
		if classof obj != IES_Sky do 
		(
			RSTA__queryFloatExpression obj targetToTestForArray objWithExprArray
		)
	)
	return objWithExprArray
)

fn RSTA_addKnownObjectTagsToJoysticks objArray =
(
	local knownObjectFlags = #(
		#("CTRL_", "joystick"),
		#("TEXT_", "joystickText"),
		#("RECT_", "joystickSurround")
		)
	
	joystickArray = #(
		--ctrl
		--text
		--rect
		)
	
	for obj in objArray do 
	(
		if toLower(substring obj.name 1 5) == toLower(knownObjectFlags[1][1]) do
		(
			thisData = #()
			local type = knownObjectFlags[1][2]
			local exist = getUserProp obj type
			if exist == undefined do 
			(
				setUserProp obj "knownObjectType" type
			)
			
			append thisData obj
			textNode = getNodeByName (knownObjectFlags[2][1]+(substring obj.name 6 -1))
			append thisData textNode	
			rectNode = getNodeByName (knownObjectFlags[3][1]+(substring obj.name 6 -1))
			append thisData rectNode	
			
			if textNode != undefined then
			(
				type = knownObjectFlags[2][1]
				local exist = getUserProp obj type
				if exist == undefined do 
				(
					setUserProp obj "knownObjectType" type
				)				
			)
			
			if rectNode != undefined then
			(
				type = knownObjectFlags[3][1]
				local exist = getUserProp obj type
				if exist == undefined do 
				(
					setUserProp obj "knownObjectType" type
				)								
			)
			
			append joystickArray thisData
		)
	)
	format ("Known object flags added.\n")
	return joystickArray
)

fn RSTA_convertOldJoystickToNew obj = 
(
	--first off we will go through the whole scene and apply knownObjectType flags to all objects
	local objArray = objects as array
	local joystickArray = RSTA_addKnownObjectTagsToJoysticks objArray 
-- 	joystickArray = #(
-- 		--ctrl
-- 		--text
-- 		--rect
-- 		)	
	format ("joystickArray.count: "+(joystickArray.count as string) +"\n")
	--now we will ensure we have clean expressions
	
	RSTA_removeEmptyScalars 1	
	--we will use mrExpressions to save out all expressions from the scene then once they are made, we'll remake the joystick then reload the expressions
	--we cant use mr Expression to save out the expressions so we will have to find all of the expressions then find any scalar targets which point to them
		
	
	local joystickNames = #(
		#(), --name
		#(), --pos
		#(), -- (Square =1, Vertical=2, Horizontal=3)
		#()--scale
		)
	
	
		
	for obj = 1 to joystickArray.count do
	(
		append joystickNames[1] joystickArray[obj][1].name
		local jpos = (in coordsys world joystickArray[obj][3].position)
		append joystickNames[2] jpos
		--now we need to query the bounding box of the rect
		
		local bbox = nodeLocalBoundingBox joystickArray[obj][3]
		
-- 		local xDist = distance bBox[1][1] bBox[2][1]
		local xDist = abs (bBox[1][1] - bBox[2][1])
-- 		local yDist = distance bBox[1][2] bBox[2][2]
		local yDist = abs (bBox[1][2] - bBox[2][2])
-- 		local zDist = distance bBox[1][3] bBox[2][3]
		local zDist = abs (bBox[1][3] - bBox[2][3])
		
		local jsType = undefined
		if xDist > zDist then
		(
			--x bigger than y so horizontal
			jsType  = 3
		)
		else
		(
			if xDist == zDist then
			(
				--both the same so square
				jsType = 1
			)
			else
			(
				--y bigger than x so vertical
				jsType = 2
			)
		)
		
		append joystickNames[3] jsType
		append joystickNames[4] joystickArray[obj][3].scale
	)
	
	--now we have the names of all the joysticks we can est the expression objects and see if they point to any of these joysticks
	local objWithExprArray = #(
		#(), --object
		#(), --full controller path
		#(), --scalar Name
		#(), -- joystick name
		#() -- axis
		)	
		

		
	for obj in objects do 
	(
-- 		RSTA_queryExpessionsForTargets eObj joystickNames[1]
		RSTA_queryExpessionsForTargets obj joystickNames[1] objWithExprArray
	)
	
	
	for i = joystickArray.count to 1 by -1 do 
	(
		delete joystickArray[i][3]
		delete joystickArray[i][2]
		delete joystickArray[i][1]
		format ("Removing index "+(i as string)+"\n")
	)
	
	for obj = 1 to joystickNames[1].count do
	(
		--now we replace obj with a new one
		local jsn = joystickNames[1][obj]
		local jPos = joystickNames[2][obj]
		local jStyle = joystickNames[3][obj]
		local scaleit = joystickNames[4][obj]
		
		RSTA_createJoystickNEW jsn jstyle jpos scaleit

	)
	
	for i = 1 to objWithExprArray[1].count do 
	(
		local exprObj = getNodeByName objWithExprArray[1][i]
		local cont = execute objWithExprArray[2][i]
		local scalarName = objWithExprArray[3][i]
		local joystick = getNodeByName objWithExprArray[4][i]
		local axisUsed = objWithExprArray[5][i]
		
		RSTA_repointScalar exprObj cont scalarName joystick axisUsed
	)
)

-- clearListener()

-- onButtonPushedDo()


