renameArray = #(
	#("PH_L_Hand", "tag = BONETAG_L_PH_Hand", "exportTrans = true"),
	#("PH_R_Hand", "tag = BONETAG_R_PH_Hand", "exportTrans = true"),
	#("IK_L_Hand", "tag = IK_L_Hand", "exportTrans = true"),
	#("IK_R_Hand", "tag = IK_R_Hand", "exportTrans = true"),
	#("IK_L_Hand", "tag = IK_L_Hand", "exportTrans = true"),
	#("PH_L_Foot", "tag = BONETAG_L_PH_Foot", "exportTrans = true"),
	#("PH_R_Foot", "tag = BONETAG_R_PH_Foot", "exportTrans = true"),
	#("IK_R_Foot", "tag = IK_R_Foot", "exportTrans = true"),
	#("IK_L_Foot", "tag = IK_L_Foot", "exportTrans = true")
	)

fn renamePHIKtags = 
(
	for item = 1 to renameArray.count do 
	(
		obj = getNodeByName renameArray[item][1]
		
		if obj != undefined then
		(
			tag = (renameArray[item][2]+"\n"+renameArray[item][3]+"\n")
			setUserPropBuffer obj tag
			
			format ("Set "+obj.name+" to: "+"\n"+tag+"\n")
		)
		else
		(
			format ("Couldnt find object "+renameArray[item][1]+"\n")
		)
	)
	format ("Re-tagging Done!"+"\n")
)

renamePHIKtags()