(
	output_name = undefined
	sela = undefined

	saveSuccess = undefined
	
	tmpArr = #()

-- 	fn RS_saveAnim output_name =
	fn RS_saveAnim =
	(
		print "Running saveAnim"
		--output_name = getSaveFileName caption:"Save RS anim file" types:"RsAnimData (*.RsAnim)|*.RsAnim|All Files (*.*)|*.*|"
		
-- 		if output_name != undefined then 
-- 		(
-- 			output_file = createfile output_name	
		
			animStart = (((animationRange.start as float) / ticksPerFrame) as integer)
			animEnd = (((animationRange.end as float) / ticksPerFrame) as integer)
			
			for frame = animStart to animEnd do 
			(
				sliderTime = frame
				timeVal = ("sliderTime = "+(frame as string)+"\n")
-- 				format (timeVal) to:output_file
				
				append tmpArr timeVal
				
				for a in sela do 
				(
					posi = in coordsys parent a.position
					rot = in coordsys parent a.rotation
					scl = in coordsys parent a.scale
					
					sep = "'"
					
					rotVal = ("in coordsys parent $"+sep+a.name+sep+".rotation = "+(rot as string)+"\n")				
					posVal = ("in coordsys parent $"+sep+a.name+sep+".position = "+(posi as string)+"\n")
					sclVal = ("in coordsys parent $"+sep+a.name+sep+".scale = "+(scl as string)+"\n")
					sepVal = ("-----"+"\n")
					
					outputData = (rotVal+posVal+sclVal+sepVal)
					
-- 					format ((outputData)) to:output_file
					
					append tmpArr outputData
				)
			)
			
-- 			close output_file
			
			saveSuccess = true
			
			print "Saved"
-- 		)
	)

		fn fnSaveAnim =
		(
			sela = selection as array
			
			try (
				--with redraw off
				RS_saveAnim() --output_name --saves off the anim
			)
			catch(format "Anim save failed."+"\n")		

			print "fnSave completed."			
		)	
	
		fn fnCleanCtrl =
		(
			--with redraw off
			--(
				if saveSuccess == true then 
				(
					--now we need to remove any anim from sela objects & set their controllers to posXYZ and Euler XYZ
					sliderTime = animationRange.start
					
					for obj in sela do 
					(
						deleteKeys obj.controller 
						
						obj.position.controller = Position_XYZ ()
						obj.rotation.controller = Euler_XYZ ()
					)			
					format ("Controllers reset"+"\n")
				)
				else
				(
					print ("saveSuccess == "+(saveSuccess as string))
				)
			--)
		)
		
		fn fnLoadAnim =
		(
			--now we need to load the anim back on
			animButtonState = true
			try ( 	
				--with redraw off 
					
				for i in tmpArr do 
				(
					--format (i+"\n")
					execute i
				)
					
					format ("Animation loaded succesfully"+"\n")
					format ("Scene bake cleanup completed successfully!"+"\n")			
			)
			catch
			(
				format ("Animation loading failed."+"\n")
			)
			animButtonState = false
			
					sliderTime = animationRange.start 
-- 					--gc()
-- 					--format ("Scene bake cleanup completed successfully!"+"\n")			
		)		
	-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
btnText = ("Clean Post-bake "+"\n"+"list controllers")

	rollout RSanimSaveLoad "RS SceneBake Cleanup"
	(
		
-- 		button btnSaveAnim "Saved anim"
-- 		button btnCleanCtrl "Clean Ctrls"
-- 		button btnLoadAnim "Load anims"
	
		button btnCleanBake btnText height:40
		
		on btnSaveAnim pressed do 
		(
			fnSaveAnim()	
		)
		
		on btnCleanCtrl pressed do 
		(
			fnCleanCtrl()
		)
		
		on btnLoadAnim pressed do 
		(
			fnLoadAnim()
		)
		
		on btnCleanBake pressed do 
		(
			if selection.count == 0 then
			(
				messagebox ("Please select objects to save from") beep:true
			)
			else
			(
				fnSaveAnim()
				fnCleanCtrl()
				fnLoadAnim()				
			)	
		)
	)

	CreateDialog RSanimSaveLoad width:200 pos:[500, 500] 

)