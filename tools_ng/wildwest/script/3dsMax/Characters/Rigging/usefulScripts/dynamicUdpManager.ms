--script to check controller types on objects and disable transfrom udp's if no relevant controller found
--Jan 2013
--Matt Rennie

--this is mainly written to reduce dofs carried at runtime

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-- filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Includes\\FN_Rigging.ms")	
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
controllersToCheckPos = #(
	Position_XYZ,
	RsSpring
	)

controllersToCheckRot = #(
	Euler_XYZ,
	RsSpringRotationController,
	LookAt_Constraint
	)
	
controllersToCheckScale = #(
	ScaleXYZ
	)	
	
dummyNodeName = "Dummy01"	
	
tDof = #()
rDof = #()
sDof = #()	
	
	TrackUdpChanges = false --set to true to get extra debug info printed out
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	

fn parseUdp tType tVal obj = 
(
	--tType = exportTrans / exportRot / exportScale	
	--tVal = true / false
	--obj = the object to scan

	existingUdp = getUserPropBuffer obj
	newUdp = undefined
	--now we need to scan this udp string for tType
	
	findStr = findString existingUdp (tType+" = "+tVal)
	
	if findStr == undefined then
	(
		--look to see if the tType is set to the inverse
		inverseTVal = "true"
		if tVal == inverseTVal do (inverseTVal = "false")
		
		findOpposite = findString existingUdp (tType+" = "+inverseTVal)
		
		if findOpposite != undefined then --ok we found an inverse of what we want so we gotta remove it
		(
			--print ("Found an opposite to "+tVal+" for "+tType)
			
			newUdp = substituteString existingUdp (tType+" = "+inverseTVal) (tType+" = "+tVal)
			
			--this next section allows debugging 
			if TrackUdpChanges == true do 
			(
				if tVal == "true" do 
				(
					if tType == "exportTrans" do (appendIfUnique tDof obj)
					if tType == "exportRot" do (appendIfUnique rDof obj)
					if tType == "exportScale" do (appendIfUnique sDof obj)		
				)
			)
		)
		else --ok so there wasnt an opposite either so we just need to add
		(
			newUdp = (existingUdp+"\r\n"+(tType+" = "+tVal))
			
			--this next section allows debugging
			if TrackUdpChanges == true do 
			(
				if tVal == "true" do 
				(
					if tType == "exportTrans" do (appendIfUnique tDof obj)
					if tType == "exportRot" do (appendIfUnique rDof obj)
					if tType == "exportScale" do (appendIfUnique sDof obj)		
				)
			)
		)
	)
	else
	(
		--print ((tType+" = "+tVal)+" already exists so skipping")
		newUdp = existingUdp
	)
	
	setUserPropBuffer obj newUdp
)

fn findControllers obj = 
(
	--first off position
	posUdp = undefined
	for cnt in controllersToCheckPos do
	(
		controllers = getClassInstances cnt target:obj
		
		if cnt == Position_XYZ then --if we're a pos xyz we need to check for float expressions
		(
			for i = 1 to controllers.count do 
			(
				--HAD TO PUT A HACK HERE FOR EULERXYZ INSIDE A SPRING CONTROLLER.
				--LOOKS LIKE GUNNARS CODE FOR THE SPRINGS DOESNT WANT TO PLAY NICE
				
				targetString = (exprForMAXObject controllers[i])
				
				foundSpringInString = findString targetString "RsSpring"
				
				if foundSpringInString == undefined do 
				(				
				
					tgt = execute (exprForMAXObject controllers[i])
					
					subCnt = getClassInstances Float_Expression target:tgt
					
					--print ("subCnt = "+(subCnt as string))
					
					if subCnt.count > 0 do
					(
							posUdp = true
							--print ("posUdp = true from Float Expression")
					)
				)
			)
		)
		else
		(
			if cnt == RsSpring do 
			(
				if controllers.count > 0 do --if the array is greater than 0 then we have a rsSpring so its trans
				(
					posUdp = true
					--print ("posUdp = true from Spring")
				)
			)
		)
	)
	
	if posUdp == undefined do (posUdp = false)
	--print ("posUdp: "+(posUdp as string))	
	
	--now Rotation
	rotUdp = undefined
	for cnt in controllersToCheckRot do
	(
		controllers = getClassInstances cnt target:obj
		
		if cnt == Euler_XYZ then --if we're a pos xyz we need to check for float expressions
		(
			for i = 1 to controllers.count do 
			(
				--HAD TO PUT A HACK HERE FOR EULERXYZ INSIDE A SPRING CONTROLLER.
				--LOOKS LIKE GUNNARS CODE FOR THE SPRINGS DOESNT WANT TO PLAY NICE
				
				targetString = (exprForMAXObject controllers[i])
				
				foundSpringInString = findString targetString "RsSpring"
				
				if foundSpringInString == undefined do 
				(
					tgt = execute targetString
					
					subCnt = getClassInstances Float_Expression target:tgt
					
					--print ("subCnt = "+(subCnt as string))
					
					if subCnt.count > 0 do
					(
						rotUdp = true
						--print ("rotUdp = true from Float Expression")
					)
				)
			)
		)
		else
		(
			if cnt == RsSpringRotationController then
			(
				if controllers.count > 0 do --if the array is greater than 0 then we have a rsSpring so its trans
				(
					rotUdp = true
					--print ("rotUdp = true from Spring")
				)
			)
			else
			(
				if cnt == LookAt_Constraint do 
				(
					if controllers.count > 0 do --if the array is greater than 0 then we have a rsSpring so its trans
					(
						rotUdp = true
						--print ("rotUdp = true from Look at")	
					)				
				)
			)
		)
	)

	if rotUdp == undefined do (rotUdp = false)
	--print ("rotUdp: "+(rotUdp as string))
		
	--now scale
	scaleUdp = undefined
	for cnt in controllersToCheckScale do
	(
		controllers = getClassInstances cnt target:obj
		
		if cnt == ScaleXYZ then --if we're a pos xyz we need to check for float expressions
		(
			for i = 1 to controllers.count do 
			(
				tgt = execute (exprForMAXObject controllers[i])
				
				subCnt = getClassInstances Float_Expression target:tgt
				
				--print ("subCnt = "+(subCnt as string))
				
				if subCnt.count > 0 do
				(
						scaleUdp = true
						--print ("scaleUdp = true from Float Expression")
				)
			)
		)
		else
		(
			if cnt == RsSpring do 
			(
				if controllers.count > 0 do --if the array is greater than 0 then we have a rsSpring so its trans
				(
					scaleUdp = true
					--print ("scaleUdp = true from Spring")
				)
			)
		)
	)	
	
	if scaleUdp == undefined do (scaleUdp = false)
	--print ("scaleUdp: "+(scaleUdp as string))
		
	--now set the udps
	tType = "exportTrans"
	tVal = (posUdp as string)
	parseUdp tType tVal obj 
		
	tType = "exportRot"
	tVal = (rotUdp as string)
	parseUdp tType tVal obj 		
		
	tType = "exportScale"
	tVal = (scaleUdp as string)
	parseUdp tType tVal obj 		
)


fn autoSetUdp = 
(
	dummyNode = getNodeByName dummyNodeName
	
	if dummyNode != undefined then
	(
		select dummyNode
		
		skelArray = getAllChildren $ arr:(selection as array)

-- 		initProgBar()
		createDialog progBar width:400 Height:30
		for ob in skelArray do
		(
			if substring ob.name 1 4 != "SKEL" do 
			(
				if substring ob.name 1 9 != "Collision" do 
				(
					if substring ob.name 1 2 != "PH" do 
					(
						iVal = findItem skelArray ob
						progBar.prog.value = (100.*iVal/skelArray.count)
						findControllers ob
					)
				)
			)
		)
		
		destroyDialog progBar
		
		format ("UDP setting complete."+"\n")
		
		if TrackUdpChanges == true do 
		(
			print "***************** TRANS ****************** "
			for i in tDof do 
			(
				print i.name
			)
			print "***************** ROT ****************** "
			for i in rDof do 
			(
				print i.name
			)
			print "***************** SCALE ****************** "
			for i in sDof do 
			(
				print i.name
			)			
		)
	)
	else
	(
		print ("Could not find "+dummyNodeName) 
		messagebox ("Could not find "+dummyNodeName) beep:true
	)
)


autoSetUdp()


-- findControllers $SPR_L_Leg_Fireman_F