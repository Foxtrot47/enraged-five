--laspPoseCleanup.ms
--hackky little script to remove the lastPose udp tag which is mysteriously appearing

RsCollectToolUsageData (getThisScriptFilename())


tagToRemove = #(
	"lastPose = undefined",
	"lastPose=undefined"
	)
	
fn removeLastPoseTag = 
(
	for o in objects do 
	(
		thisUdp = getUserPropBuffer o
		
		for tag = 1 to tagToRemove.count do 
		(
			foundTag = findString thisUdp tagToRemove[tag]
			if foundTag != undefined do 
			(
				--ok we have found that tag so need to remove it 
				startStr = substring thisUdp 1 (foundTag - 1)
				postStart = ((foundTag ) + (tagToRemove[tag].count))
				endTag = substring thisUdp postStart 1000
				newUdp = (startStr+endTag)
				
				setUserPropBuffer o newUdp
				print ("UDP modified on "+o.name)
			)
		)
	)
)
clearListener()
removeLastPoseTag()

print"done"