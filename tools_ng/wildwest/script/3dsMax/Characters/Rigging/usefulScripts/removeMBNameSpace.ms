--tool to remove motionbuilder namespaces

sela = selection as array

for a in sela do 
(
	markerPoint = findstring a.name ":"
	
	if markerPoint != undefined do 
	(
		a.name = substring a.name (markerPoint + 1) -1
	)
)