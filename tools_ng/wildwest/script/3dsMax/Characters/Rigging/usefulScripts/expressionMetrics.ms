--tool to parse all expression style controllers in a scene
--and output metrics for them

--Matt Rennie
--Nov 2013


posExpressionControllers = #()
posSpringControllers = #()
rotExpressionControllers = #()
rotSpringControllers = #()
rotLookAtControllers = #()
scaleExpressionControllers = #()
scaleSpringControllers = #()

fn RSTA_posXYZ obj = 
(
	if classof obj.position.controller as string == RsSpring do
	(
		appendIfUnique posSpringControllers[1] obj
		arrIndex = finditem posSpringControllers[1] obj
		append posSpringControllers[2][arrIndex] obj
	)
	if classof obj.position.controller as string == Position_XYZ do
	(	
		appendIfUnique posExpressionControllers[1] obj		
		cont = Float_Expression
		if classof obj.position.controller.X_Position.controller == cont do
		(
			--now we need to do a finditem to find obj in the controller array
			arrIndex = finditem posExpressionControllers[1] obj
			append posExpressionControllers[2][arrIndex] obj
		)
		if classof obj.position.controller.Y_Position.controller == cont do
		(
			arrIndex = finditem posExpressionControllers[1] obj
			append posExpressionControllers[2][arrIndex] obj			
		)	
		if classof obj.position.controller.Z_Position.controller == cont do
		(
			arrIndex = finditem posExpressionControllers[1] obj
			append posExpressionControllers[2][arrIndex] obj
		)
	)
)

fn RSTA_posList obj = 
(
	expArr = #(obj,#())
	sprArr = #(obj,#())
	for i = 1 to obj.position.controller.count do 
	(
		if classof obj.position.controller[i].controller == RsSpring do
		(
-- 			appendIfUnique sprArr[1] obj
-- 			arrIndex = finditem posSpringControllers[1] obj
			append sprArr[2] obj
		)
		if classof obj.position.controller[i].controller == Position_XYZ do
		(
-- 			appendIfUnique posExpressionControllers[1] obj			
			cont = Float_Expression
			
			if classof obj.position.controller[i].X_Position.controller == cont do
			(
				append expArr[2] obj
			)
			if classof obj.position.controller[i].Y_Position.controller == cont do
			(
				append expArr[2] obj
			)
			if classof obj.position.controller[i].Z_Position.controller == cont do
			(
				append expArr[2] obj
			)		
		)
	)	
	
	if expArr[2].count > 0 do 
	(
		append posExpressionControllers expArr 
	)
	if sprArr[2].count > 0 do 
	(
		append posSpringControllers sprArr
	)
)

fn RSTA_rotEulerXYZ obj = 
(
	/**
	FUNCTION TO TEST IF obj HAS A STANDARD EULER XYZ
	ROTATION CONTROLLER AND THEN SEND THAT TO BE PARSED
	**/
	expArr = #(obj,#())
	sprArr = #(obj,#())
	laArr = #(obj,#())
		
	if classof obj.rotation.controller == RsSpringRotationController do
	(
		append sprArr[2] obj
	)
	
	if classof obj.rotation.controller == LookAt_Constraint do
	(
		append laArr[2] obj
	)	
	
	if classof obj.rotation.controller == Euler_XYZ do
	(
-- 		appendIfUnique rotExpressionControllers[1] obj
		cont = Float_Expression
		if classof obj.Rotation.controller.X_Rotation.controller == cont do
		(
			append expArr[2] obj
		)
		if classof obj.Rotation.controller.Y_Rotation.controller == cont do
		(
			append expArr[2] obj
		)	
		if classof obj.Rotation.controller.Z_Rotation.controller == cont do
		(
			append expArr[2] obj
		)	
	)
	
	if expArr[2].count > 0 do 
	(
		append rotExpressionControllers expArr 
	)
	if sprArr[2].count > 0 do 
	(
		append rotSpringControllers sprArr
	)
	if laArr[2].count > 0 do 
	(
		append rotLookAtControllers laArr
	)
)

fn RSTA_rotList obj = 
(
	/**
	FUNCTION TO TEST IF obj HAS A ROTATION LIST
	ROTATION CONTROLLER AND THEN SEND THAT TO BE PARSED
	**/	
	expArr = #(obj,#())
	sprArr = #(obj,#())
	laArr = #(obj,#())
	
	for i = 1 to obj.rotation.controller.count do --skip 1st controller as thats the frozen one
	(
		if classof obj.rotation.controller[i].controller == RsSpringRotationController do
		(
			appendSprArr[2] obj
		)

		if classof obj.rotation.controller[i].controller == LookAt_Constraint do
		(
			append laArr[2] obj
		)		
		
		if classof obj.rotation.controller[i].controller == Euler_XYZ do
		(
-- 			appendIfUnique rotExpressionControllers[1] obj
			cont = Float_Expression
			if classof obj.rotation.controller[i].X_rotation.controller == cont do
			(
				append expArr[2] obj
			)
			if classof obj.rotation.controller[i].Y_rotation.controller == cont do
			(
				append expArr[2] obj
			)
			if classof obj.rotation.controller[i].Z_rotation.controller == cont do
			(
				append expArr[2] obj
			)		
		)
	)	

	if expArr[2].count > 0 do 
	(
		append rotExpressionControllers expArr 
	)
	if sprArr[2].count > 0 do 
	(
		append rotSpringControllers sprArr
	)
	if laArr[2].count > 0 do 
	(
		append rotLookAtControllers laArr
	)
)

fn RSTA_sclXYZ obj = 
(
	/**
	FUNCTION TO TEST IF obj HAS A STANDARD SCALE XYZ
	CONTROLLER AND THEN SEND THAT TO BE PARSED
	**/
	expArr = #(obj,#())
	sprArr = #(obj,#())

		
	if classof obj.scale.controller == RsSpring do
	(
		append sprArr obj		
	)
	if classof obj.scale.controller == ScaleXYZ do
	(		
		cont = Float_Expression
		if classof obj.Scale.controller.X_Scale.controller == cont do
		(
			append expArr[2] obj
		)
		if classof obj.Scale.controller.Y_Scale.controller == cont do
		(
			append expArr[2] obj
		)	
		if classof obj.Scale.controller.Z_Scale.controller == cont do
		(
			append expArr[2] obj
		)		
	)
	
	if expArr[2].count > 0 do 
	(
		append scaleExpressionControllers expArr
	)
	if sprArr[2].count > 0 do 
	(
		append scaleSpringControllers sprArr
	)
)

fn RSTA_sclList obj = 
(
	/**
	FUNCTION TO TEST IF obj HAS A SCALE LIST
	CONTROLLER AND THEN SEND THAT TO BE PARSED
	**/
	expArr = #(obj,#())
	sprArr = #(obj,#())
		
	for i = 1 to obj.scale.controller.count do --skip 1st controller as thats the frozen one
	(

		if classof obj.scale.controller[i].controller == RsSpring do
		(
			append sprArr[2] obj
		)
		if classof obj.scale.controller[i].controller == ScaleXYZ do
		(	
			cont = Float_Expression
			if classof obj.scale.controller[i].X_scale.controller == cont do
			(
				append expArr[2] obj
			)
			if classof obj.scale.controller[i].Y_scale.controller == cont do
			(
				append expArr[2] obj

			)
			if classof obj.scale.controller[i].Z_scale.controller == cont do
			(
				append expArr[2] obj
			)		
		)
	)		
	
	if expArr[2].count > 0 do 
	(
		append scaleExpressionControllers expArr
	)
	if sprArr[2].count > 0 do 
	(
		append scaleSpringControllers sprArr
	)	
)

fn RSTA_queryControllerType obj = 
(
	--format ("Querying controllers on "+obj.name+"\n")
	
	if classof obj.position.controller == position_list then
	(
		RSTA_posList obj
	)
	else
	(
		if classof obj.position.controller == Position_XYZ do
		(
			RSTA_posXYZ obj
		)
	)
	
	if classof obj.rotation.controller == rotation_list then
	(
		--format ("Rotation List found on "+obj.name+"\n")
		RSTA_rotList obj
	)
	else
	(
		if classof obj.rotation.controller == Euler_XYZ do
		(
			--Format ("Euler XYZ found on "+obj.name+"\n")
			RSTA_rotEulerXYZ obj
		)
	)

	if classof obj.scale.controller == scale_list then
	(
		debugPrint ("Scale List found on "+obj.name)
		RSTA_sclList obj
	)
	else
	(
		if classof obj.scale.controller == ScaleXYZ do
		(
			debugPrint ("Scale XYZ found on "+obj.name)
			RSTA_sclXYZ obj
		)
	)
)

fn RSTA_expressionCostQuery = 
(
	for obj in objects do 
	(
		RSTA_queryControllerType obj 
	)
-- 	
-- 	RSTA_queryControllerType $MH_BreatheBone

	
	totalPosExpressionCount = 0
	totalPosSpringCount = 0
	totalRotExpressionCount = 0
	totalRotSpringCount = 0
	totalRotLookAtCount = 0
	totalScaleExpressionCount = 0
	totalScaleSpringCount = 0
	
	if posExpressionControllers[1] != undefined do 
	(
		for obj = 1 to posExpressionControllers.count do 
		(
			format (posExpressionControllers[obj][1].name+" has "+(posExpressionControllers[obj][2].count as string)+" position Expression controllers.\n")
			totalPosExpressionCount = totalPosExpressionCount + posExpressionControllers[obj][2].count
		)
	)

	if posSpringControllers[1] != undefined do 
	(
		for obj = 1 to posSpringControllers.count do 
		(
			format (posSpringControllers[obj][1].name+" has "+(posSpringControllers[obj][2].count as string)+" position Spring controllers.\n")
			totalPosSpringCount = totalPosSpringCount + posSpringControllers[obj][2].count 
		)	
	)
	
	-----
	if rotExpressionControllers[1] != undefined do 
	(
		for obj = 1 to rotExpressionControllers.count do 
		(
			format (rotExpressionControllers[obj][1].name+" has "+(rotExpressionControllers[obj][2].count as string)+" rotation Expression controllers.\n")
			totalRotExpressionCount = totalRotExpressionCount + rotExpressionControllers[obj][2].count
		)	
	)

	if rotSpringControllers[1] != undefined do 
	(
		for obj = 1 to rotSpringControllers.count do 
		(
			format (rotSpringControllers[obj][1].name+" has "+(rotSpringControllers[obj][2].count as string)+" rotation Spring controllers.\n")
			totalRotSpringCount = totalRotSpringCount + rotSpringControllers[obj][2].count 
		)		
	)
	
	if rotLookAtControllers[1] != undefined do 
	(
		for obj = 1 to rotLookAtControllers.count do 
		(
			format (rotLookAtControllers[obj][1].name+" has "+(rotLookAtControllers[obj][2].count as string)+" rotation LookAt controllers.\n")
			totalRotLookAtCount = totalRotLookAtCount + rotLookAtControllers[obj][2].count 
		)	
	)
	------	
	if scaleExpressionControllers[1] != undefined do 
	(
		for obj = 1 to scaleExpressionControllers.count do 
		(
			format (scaleExpressionControllers[obj][1].name+" has "+(scaleExpressionControllers[obj][2].count as string)+" scale Expression controllers.\n")
			totalScaleExpressionCount = totalScaleExpressionCount + scaleExpressionControllers[obj][2].count 
		)	
	)

	if scaleSpringControllers[1] != undefined do 
	(
		for obj = 1 to scaleSpringControllers.count do 
		(
			format (scaleSpringControllers[obj][1].name+" has "+(scaleSpringControllers[obj][2].count as string)+" scale Spring controllers.\n")
			totalScaleSpringCount = totalScaleSpringCount + scaleSpringControllers[obj][2].count 
		)		
	)
	
	format "------------------------------\n"
	thisName = maxfileName
	format (thisName+"\n")
	format ("total Position Expression Count = "+(totalPosExpressionCount as string)+"\n")
	format ("total Position Spring Count = "+(totalPosSpringCount as string)+"\n")
	format ("total Rotation Expression Count = "+(totalRotExpressionCount as string)+"\n")
	format ("total Rotation Spring Count = "+(totalRotSpringCount as string)+"\n")
	format ("total Rotation LookAt Count = "+(totalRotLookAtCount as string)+"\n")
	format ("total Scale Expression Count = "+(totalScaleExpressionCount as string)+"\n")
	format ("total Scale Spring Count = "+(totalScaleSpringCount as string)+"\n")

)

RSTA_expressionCostQuery()