--tool to strip out he massive number of carriage return lines in udp's
--this will hopefully help improve scvene responsiveness
--Matt Rennie
--April 2013

totalRemoved = 0

fn removeCrFromUDP obj = 
(
	tag = getUserPropBuffer obj
	
	tagCount = tag.count
	
	newTag = ""
	
	removeCount = 0
	
	for char = 1 to tagCount  do 	
	(
			nextChar = char + 1
			
			if tag[char] == "\n" then 
			(
				if tag[nextChar] == "\n" then
				(
					--ok we found two coincident carriage returns so need to strip then off
					--print "found double cr"
					removeCount = removeCount + 1
				)
				else
				(
					newTag = (newTag + tag[char])
				)
			)
			else
			(
					newTag = (newTag + tag[char])
			)
	)
	
	totalRemoved = totalRemoved + removeCount
	
	--format ("new udp: "+newTag+"\n")
	if removeCount != 0 do 
	(
		print ("removed "+(removeCount as string)+" carriage returns from "+obj.name)
	)
	setUserPropBuffer obj newTag
)

sela = selection as array

for obj in sela do 
(
	removeCrFromUDP obj
)

format ("Removed a total of "+(totalRemoved as string)+" carriage returns."+"\n")