inputArray = undefined

input_name = undefined
output_name = undefined

fn outputSelectionSets = 
(	
	outputName = undefined
	selCount = getNumNamedSelSets()

	currItems = #()
	
	output_name = getSaveFileName caption:"Selection Set file" types:"Sset Data (*.sSet)|*.sset|All Files (*.*)|*.*|"

	if output_name != undefined then 
	(
		
		if (doesFileExist output_name) == true do
		(	
			deleteFile output_name
		)
		output_file = createfile output_name		

		outputData = #()
		
		for i = 1 to selCount do 
		(
			selname = getNamedSelSetName i 
				--now record what items already exist in that set
				setItems = getNamedSelSetItemCount i
				
				currItems = #()
			
				for a = 1 to setItems do
				(
					thisItem = getNamedSelSetItem i a 
					arrStr = ("'"+selName+"'"+","+thisItem.name)
			
					format ((arrStr as string)+"\r") to:output_file
				)	
		)
		
		close output_file
	)
	else
	(
		messagebox ("WARNING! Please select an output file.") beep:true
	)
)

fn inputSelectionSets = 
(
	clearListener()
	inputData = #(
		#(),
		#()
		) -- define as array
	input_name  = undefined
		
	arrayCount = #()
	if input_name == undefined do
	(
		input_name = getOpenFileName caption:"Selection Set file" types:"Sset Data (*.sSet)|*.sset|All Files (*.*)|*.*|"
	)	

	if input_name != undefined then
	(
		f = openfile input_name

		while not eof f do
		(
			myLine = (filterstring (readLine f) "\n")
			filtered = filterString (myLine as string) ","
			
			myLineA = (substring filtered[1] 4 -1)
-- 			print ("myLineA: "+myLineA)
			myLineB = (substring filtered[2] 1 (filtered[2].count - 2))
-- 			print ("myLineB: "+myLineB)

			append inputData[1] myLineA
			append inputData[2] myLineB
				
			appendIfUnique arrayCount myLineA --this creates an array of all selection set names
		)
		close f

		inputDataArray = #()
		
		inputDataArray.count = arrayCount.count
		print ("set inputDataArray to "+(inputDataArray.count as string))
		
		for i = 1 to inputDataArray.count do 
		(
			inputDataArray[i] = #()
		)
			
		for i = 1 to inputData[1].count do
		(
			for ida = 1 to arrayCount.count do 
			(
-- 				print ("inputData[1]:"+(inputData[1] as string)+" arrayCount")
				if inputData[1][i] == arrayCount[ida] do -- match found for sel set
				(
					thisNode = getNodeByName inputData[2][i]
-- 					print ("thisNodeName:"+inputData[2][i]+" thisNode:"+(thisNode as string))
					if thisNode != undefined do
					(
						append inputDataArray[ida] thisNode --this will mean we can then get the selection set from arrayCount[ida] and then the nodes to go in that set from inputDataArray[ida]
					)
				)
			)				
		)
		
		print ("inputDataArray[1].count:"+(inputDataArray[1].count as string))
		
		for ida = 1 to arrayCount.count do 
		(
			selName = arrayCount[ida]
			--now we need to filter the selName to remove the ' marks
			selName = subString selName 2 -1 --strips first '
			selName = subString selName 1 (selName.count - 1)
			
			selNodes = inputDataArray[ida]
			
			--now we need to delete any existing sets with selName
			deleteItem selectionSets selName
			
			--now we can recreate the selection set
			selectionSets[selName] = selNodes
			print ("Created Selection Set: "+selName)
		)
		Print "DONE!"
	)		
)


if ((SelSetGui != undefined) and (SelSetGui.isDisplayed)) do
	(destroyDialog SelSetGui)

rollout SelSetGui "SelSets"
(
	button btnRead "Output Sets" width:110
	button btnPaste "Input Sets" width:110

		
	
	on btnRead pressed do
	(
		outputSelectionSets()
	)
	
	on btnPaste pressed do
	(
		inputSelectionSets()
	)
)

CreateDialog SelSetGui width:125 pos:[1450, 100] 