--mr skeleton v2 functions specific to weapon rig building.

filein (theWildwest+"/script/3dsMax/Characters/Rigging/mrSkeleton_v2/mrSkeleton_2_weaponDataFile.ms")

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn rsta_nameCheck nameStr = 
(
	/*
	THIS WILL QUERY nameStr AND SEE IF THE FINAL CHARACTERS ARE NUMBERS
	IF SO IT WILL RETURN A STRING WITH THOSE NUMERIC CHARACTERS TRIMMED OFF 
	USED FOR WHEN WE HAVE MULTIPLE PARTS WITH THE SAME STARTER NAME
	SUCH AS MAG_1 AND MAG_2
	*/
	
	local regExprStr = "\w+(?<!\d+)(?<!_)"
	
	local rgx = dotNetObject "System.Text.RegularExpressions.Regex" regExprStr
	
	local matches = rgx.matches nameStr
	
	local thisVal = nameStr
	
	if matches.count > 0 then
	(
		format ((matches.count as string)+" total matches found\n")
		
		for i = 0 to (matches.count - 1) do 
		(
			local thisMatch = matches.item[i]
			
			thisVal = thisMatch.Value 
			
			--format ("thisVal:"+(thisVal as string)+"\n")
		)
	)
	return thisVal
)

fn rsta_AlignAttachment obj = 
(
	--need to check that obj is either an AAP or a WAP
	
	isWap = findString obj.name "_WAP"
	
	if isWap != undefined then
	(
		--ok now we need to find the object thats its pair by doing a getnodebyname on its string using a substituteString 
		
		aapName = substituteString obj.name "_WAP" "_AAP"
		aapNode = getNodeByName aapName
		
		if aapNode != undefined then
		(
			obj.transform = aapNode.transform
		)
		else
		(
			format ("1) Could not align "+obj.name+" as AAP node "+aapName+" could not be found.\n")
			messagebox ("Could not align "+obj.name+" as AAP node "+aapName+" could not be found.\n") beep:true title:"WARNING!"
		)
		
	)
	else
	(
		--ok we're not a WAP
		isDummyAap = findString obj.name "Marker_Dummy_AAP" 
			
		if isDummyAap != undefined then
		(
			aapNode = obj.children[1]
			
			wapName = substituteString aapNode.name "_AAP" "_WAP"
			wapNode = getNodeByName wapName
		
			if wapNode != undefined then
			(		
				obj.transform = wapNode.transform
				aapNode.transform = wapNode.transform					
			)
			else
			(
				format ("3) Could not align "+obj.name+" as WAP node "+wapName+" could not be found.\n")
				messagebox ("Could not align "+obj.name+" as WAP node "+wapName+" could not be found.\n") beep:true title:"WARNING!"
			)
			
		)		
		else
		(
			isAap = findString obj.name "_AAP"
		
			if isAap != undefined then
			(
				wapName = substituteString obj.name "_AAP" "_WAP"
				wapNode = getNodeByName wapName
			
				if wapNode != undefined then
				(
					aapDummy = obj.parent
					aapDummy.transform = wapNode.transform
					obj.transform = wapNode.transform
				)
				else
				(
					format ("2) Could not align "+obj.name+" as WAP node "+wapName+" could not be found.\n")
					messagebox ("Could not align "+obj.name+" as WAP node "+wapName+" could not be found.\n") beep:true title:"WARNING!"
				)
			)
			else
			(
				format ("4) Cannot align "+obj.name+" as it is not a valid AAP or WAP node.\n")
				messagebox ("Cannot align "+obj.name+" as it is not a valid AAP or WAP node.\n") beep:true title:"WARNING!"
			)
		)
	)
)

fn rsta_LoadWeaponPart markerFileName = 
(
	local mkrName = (filterString (fileNameFromPath markerFileName) ".")
	
	partName = rsta_nameCheck mkrName[1]

	--now we need to see if there is a part called this already and if so ask if we wanna delete it or just not make anything
	format ("Looking for existing "+partname+"...\n")
	existingWap = RSTA_getNodeByNameWildcard ("Marker_WAP"+partName)
	
	
	if existingWap != undefined then
	(
		--theres a node claled this already
		deleteExisting = queryBox ("There is already a "+existingWap.name+" node in the scene.\nDo you want to delete and generate from your file?") title:"err...force" beep:true
		
		if deleteExisting == true do
		(
			markerRoot = getNodeByName partName
			wapNode = getNodeByName ("Marker_WAP"+partName)
			aapDummyNode = getNodeByName ("Marker_Dummy_AAP"+partName)
			aapNode = getNodeByName ("Marker_AAP"+partName)
			
			if markerRoot != undefined do (delete markerRoot)
			if wapNode != undefined do (delete wapNode)
			if aapDummyNode != undefined do (delete aapDummyNode)
			if aapNode != undefined do (delete aapNode)
			
			RSTA_buildMarkerFromMarkerFile markerFileName  

			--now we need to derive from the markerFileName the weapon part Name

			local mkrName = (filterString (fileNameFromPath markerFileName) ".")

			partName = rsta_nameCheck mkrName[1]

			markerRoot = getNodeByName partName

			if markerRoot != undefined then 
			(
				--do we need to find if there are matching parts in the scene so we can number them if necessary
				
				skelMainNode = GetNodeByName ("Marker_GUN_Main_Bone")
				
				wapNode = getNodeByName ("Marker_WAP"+partName)
				if wapNode.parent == markerRoot do
				(
					wapNode.parent = skelMainNode
				)
				aapNode = GetNodeByName ("Marker_Dummy_AAP"+partName)
				
				if aapNode.parent == markerRoot do 
				(
					aapNode.parent = undefined
				)
						
				delete markerRoot
			)
			else
			(
				format ("Couldn't find object called "+partName+"\n")
				messagebox ("Couldn't find object called "+partName+"\n") beep:true
			)			
		)		
		
	)
	else
	(
		RSTA_buildMarkerFromMarkerFile markerFileName  
		
		--now we need to derive from the markerFileName the weapon part Name
		
		local mkrName = (filterString (fileNameFromPath markerFileName) ".")
		
		partName = rsta_nameCheck mkrName[1]
		
		markerRoot = getNodeByName partName
		
		if markerRoot != undefined then 
		(
			--do we need to find if there are matching parts in the scene so we can number them if necessary
			
			skelMainNode = GetNodeByName ("Marker_GUN_Main_Bone")
			format ("skelMainNode set to "+skelMainNode.name+"\n")
			markerKids = markerRoot.children --as array
			
			childDummy = undefined 
			for k in markerKids do 
			(
				fnd = findstring k.name "Dummy"
				
				if fnd != undefined then
				(
					childDummy = k
					format ("FOUND "+k.name+"\n")
				)
			)
			
			format ("ChildDummy set to "+childDummy.name+"\n")
			aapNode = childDummy.children[1]
			format ("aapNode set to "+aapNode.name+"\n")
			
			wapName = substituteString aapNode.name "_AAP" "_WAP"
			
			wapNode = getNodeByName wapName
			
			wapNode.parent = skelMainNode
			childDummy.parent = undefined
					
			delete markerRoot
		)
		else
		(
			format ("Couldn't find object called "+partName+"\n")
			messagebox ("Couldn't find object called "+partName+"\n") beep:true
		)
	)
)

fn rsta_InitialiseWeaponGrip gripData = 
(
	fn diffArray a b =
	(
		retArray = #()
		for element in a do
		(
			idx = finditem b element
			if idx == 0 then
			append retArray element 
		)
		return retArray
	)	

	gripMeshes = gripData[1] --geometry for visual grip alignment
	gripMarkers = gripData[2] --mkr file
	gripAlignArray = gripData[3] --an array of pairs of nodes that align to each other such as PH_R_Hand aligns to Marker_SKEL_GripR
	
	initialNodes = objects as array

	mergemaxfile gripMeshes #select
	
	format ("Merged gripMesh from "+gripMeshes+"\n")
	
	for mkFile in gripMarkers do 
	(
		RSTA_buildMarkerFromMarkerFile mkFile
		format ("Loaded markers via "+mkFile+"\n")
	)
	
	
	postMergeNodes = objects as array
	
	newNodes = diffArray postMergeNodes initialNodes
	
	format "NewNodes==========================\n"
	for obj in newNodes do 
	(
		format (obj.name+"\n")
	)
	format "/NewNodes=========================\n"
	--now we can see from the newNodes array if we have left or right grips and align shit together appropriately
		
	for ind = 1 to gripAlignArray.count do 
	(
		alignToNode = undefined
		alignToNodeName = gripAlignArray[ind][1]
		nodeToAlign = undefined
		nodeToAlignName = gripAlignArray[ind][2]
		
		for i = 1 to newNodes.count do 
		(
			if newNodes[i].name == alignToNodeName do
			(
				alignToNode = newNodes[i]
			)
			if newNodes[i].name == nodeToAlignName do
			(
				nodeToAlign = newNodes[i]
			)			
		)
		
		if alignToNode != undefined then
		(
			if nodeToAlign != undefined then
			(
				format ("Aligning "+nodeToAlign.name+" to "+alignToNode.name+"\n")
				
				kidArray = #()
				if nodeToAlign.children != undefined do 
				(
					for i = nodeToAlign.children.count to 1 by -1 do 
					(
						append kidArray nodeToAlign.children[i]
						nodeToAlign.children[i].parent = undefined
					)
				)
				nodeToAlign.transform = alignToNode.transform
				
				for kid in kidArray do 
				(
					kid.parent = nodeToAlign
				)
			)
			else
			(
				format ("Couldn't find nodeToAlign node called "+nodeToAlignName+" cannot continue alignment.\n")
			)
		)
		else
		(
			format ("Couldn't find alignToNode node called "+alignToNodeName+" cannot continue alignment.\n")
		)
	)	
	
	
	
)

fn rsta_SaveWeaponPart obj = 
(
		local markerArray = #() --array of all chidlren of root Obj
		
		--first need to identify if this is an AAP or WAP node
		--then we can find any objects necessary for this
		--then we append them into the markerArray
		
		local isWapNode = matchPattern obj.name pattern:"*WAP*"
		
		local wapNode = undefined
		local aapNode = undefined
		local aapDummyNode = undefined
		
		if isWapNode == true then
		(
			--ok we need to find the corresponding AAP node
			wapNode = obj
			
			local aapName = (substituteString wapNode.name "WAP" "AAP")
			
			aapNode = getNodeByName aapName
			
			if aapNode == undefined then
			(
				format ("Failed to find an AAP node called "+aapName+"\n")
			)
			else
			(
				aapDummyNode = aapNode.parent
			)
		)
		else
		(
			local isAapNode = matchPattern obj.name pattern:"*AAP*"
			
			if isAapNode == true then
			(
				--now need to see if this is the dummy node
				
				local isDummyNode = matchPattern obj.name pattern:"*Dummy*"
				
				if isDummyNode == false then
				(
					aapNode = obj
					aapDummyNode = obj.parent
					--ok we need to find the corresponding WAP node
					local wapName = (substituteString aapNode.name "AAP" "WAP")
					wapNode = getNodeByName wapName
					
					if wapNode == undefined then 
					(
						format ("Failed to find an WAP node called "+wapName+"\n")
					)
				)
				else
				(
					aapDummyNode = obj
					aapNode = obj.children[1]
					--ok we need to find the corresponding WAP node

					local wapName = (substituteString aapNode.name "AAP" "WAP")
					wapNode = getNodeByName wapName
					
					if wapNode == undefined then 
					(
						format ("Failed to find an WAP node called "+wapName+"\n")
					)				
				)
			)
			else
			(
				format ("Could not derive if "+obj.name+" is a WAP or an AAP node. We cannot continue.\n")
				messagebox ("Could not derive if "+obj.name+" is a WAP or an AAP node. We cannot continue.\n") beep:true title:"Part Saving FAIL."
			)
		)

		--now we will make a MASTERDUMMY node to act as a parent for all the pieces
		
		local masterNode = point pos:[0,0,0]
		masterNode.name = "WAPAAP_NodeMaster"
				
		RSTA_buildMarkerAttributes masterNode
				
		local aapNodeName = undefined
		local wapNodeName = undefined
		local origWapParent = undefined
		local origAapDummyParent = undefined
		
		append markerArray masterNode
			
		if aapDummyNode != undefined do 
		(
			--first off ensure its got osme attrs
			if aapDummyNode.modifiers[#Attribute_Holder] == undefined do
			(
				RSTA_buildMarkerAttributes aapDummyNode
				
				aapDummyNode.modifiers[#Attribute_Holder].Custom_Attributes.AlignWithMarker = true
				aapDummyNode.modifiers[#Attribute_Holder].Custom_Attributes.LookAtMe = false
				aapDummyNode.modifiers[#Attribute_Holder].Custom_Attributes.AlignToOpposite = false
				aapDummyNode.modifiers[#Attribute_Holder].Custom_Attributes.FlipMe = false
				aapDummyNode.modifiers[#Attribute_Holder].Custom_Attributes.ForceYForward = false
				aapDummyNode.modifiers[#Attribute_Holder].Custom_Attributes.DontStraighten = true
				aapDummyNode.modifiers[#Attribute_Holder].Custom_Attributes.CreateAsPoint = false
				aapDummyNode.modifiers[#Attribute_Holder].Custom_Attributes.CreateAsDummy = true
				aapDummyNode.modifiers[#Attribute_Holder].Custom_Attributes.EnableTrans = false
				aapDummyNode.modifiers[#Attribute_Holder].Custom_Attributes.EnableRot = false
				aapDummyNode.modifiers[#Attribute_Holder].Custom_Attributes.EnableScale = false	
			)
			
			
			origAapDummyParent = aapDummyNode.parent
			
			aapDummyNode.parent = masterNode
			
			append markerArray aapDummyNode
			append markerArray aapNode
			
			aapIndex = findstring aapNode.name "AAP"
			aapNodeName = (substring aapNode.name (aapIndex + 3) -1)		
			
			--now we need to figure out if this node has a number towards the end 
		)
		
		if wapNode != undefined do 
		(
			origWapParent = wapNode.parent
			
			wapNode.parent = masterNode
			
			append markerArray wapNode
			
			wapIndex = findstring wapNode.name "WAP"
			wapNodeName = (substring wapNode.name (wapIndex + 3) -1)		
		)
		
		markerFileName = undefined
		
		if aapNodeName != undefined then
		(
			nodeType = rsta_nameCheck aapNodeName
			masterNode.name = (nodeType)
			markerFileName = (markerPresetFolder+masterNode.name+".mkr")
		)
		else
		(
			if wapNodeName != undefined then
			(
				nodeType = rsta_nameCheck wapNodeName
				
				masterNode.name = (nodeType)
				markerFileName = (markerPresetFolder+masterNode.name+".mkr")
			)
		)
					
		format ("Trying to save to "+markerFileName+"\n")
		format ((markerArray as string)+"\n")
		
		RSTA_saveMarkerFile markerFileName markerArray
		
		if aapDummyNode != undefined do 
		(
			aapDummyNode.parent = origAapDummyParent
		)
		if wapNode != undefined do 
		(
			wapNode.parent = origWapParent
		)
		
		delete masterNode
)

