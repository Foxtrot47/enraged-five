filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
rsta_loadCommonFunction #("FN_RSTA_userSettings")

theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

theProjectConfig = RsConfigGetProjBinConfigDir()

toolsRoot = RsConfigGetToolsRootDir()

filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_Perforce.ms")

configSuccess = #() --used to tell us if we can continue with running the script - we dont want to if the prefs file isnt present
--NEED TO ADD A P4 FETCH FOR THIS FILE
--***************************************************************************
mrSkelConfigFile = (theWildWest + "etc/config/rigging/mrSkeletonConfig.ms")

doesConfigExist = doesFileExist mrSkelConfigFile

if doesConfigExist == true then
(
	local versionNumber = undefined
	--now check what revision it is
	toolData = RSTA_getToolRevision mrSkelConfigFile versionNumber
	
	format ("toolData: "+(toolData as string)+"\n")
	
	if toolData[1] != toolData[2] then
	(
		--ok we're not on latest so we need to ask if we want to sync to latest
		syncthis = queryBox (mrSkelConfigFile+" is not Latest.\nGet latest?") title:"err...force" beep:true
		
		if syncThis == true do
		(
			format ("Trying to sync "+mrSkelConfigFile+"\n")
			gRsPerforce.sync mrSkelConfigFile silent:true force:true
		)
	)
	
)
else
(
	--ok we need to fetch it
	gRsPerforce.sync mrSkelConfigFile silent:true force:true
	format ("Synched "+mrSkelConfigFile)
	
	doesConfigExist = doesFileExist mrSkelConfigFile
	
	if doesConfigExist != true then
	(
		messagebox ("Please contact TechArt. Your Project Specific mr Skeleton Config File is missing.\n") beep:True
		format (("Please contact TechArt. Your Project Specific mr Skeleton Config File is missing.\n"))
		
		append configSuccess ("Please contact TechArt. Your Project Specific mr Skeleton Config File is missing.\n")
	)
)

filein mrSkelConfigFile

filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_Rigging.ms")

filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_UI.ms")

filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/mrSkeleton_2_functions.ms")

filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/mrSkeleton_2_weaponRigging_functions.ms")
	filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/mrSkeleton_2_weaponRigging_functions.ms") --HORRIBLE HACK. HAVE TO FILEIN TWICE TO TRY AND HACK AROUND A SCOPE ISSUE WITH FUNCTION ORDERS.

filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/mrSkeleton_2_propRigging_functions.ms")

filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/mrSkeleton_2_propRigging_subRollouts.ms")

filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/expressionExport/mrExpression_SpringHack.ms")

filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/expressionExport/mrExpression_CutPasteController.ms")

filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/expressionExport/mrExpression_Export.ms")

filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/expressionExport/mrExpression_Import.ms")

filein (theWildWest + "script/3dsMax/Characters/Rigging/mrSkeleton_v2/expressionExport/exprssionMetrics.ms")


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if (RSG_MrSkeleton2_Struct != undefined) do RSG_MrSkeleton2_Struct.dispose()
struct RSG_MrSkeleton2_Struct
(
	-- LOCALS ------------------------------------------------
	MainWindow, 
	Settings = rsta_userSettings app:"RSG_MrSkeleton2",	
	
	viewModel,
	
-- 	--WE WILL INITIALISE ALL THE CHECKBOX STATES HERE
-- 	cb_AmmoVal = false,
-- 	cb_BoltVal = false,
-- 	cb_BreachVal = false,
-- 	cb_ButtVal = false,
-- 	cb_CockVal = false,
-- 	cb_FeedVal = false,
-- 	cb_HammerVal = false,
-- 	cb_HandleVal = false,
-- 	cb_MuzzleVal = false,
-- 	cb_ProjectileVal = false,
-- 	cb_SafetyVal = false,
-- 	cb_SightsVal = false,
-- 	cb_TriggerVal = false,
-- 	cb_VFXEjectVal = false,
-- 	cb_AAPClipVal = false,
-- 	cb_AAPFlshVal = false,
-- 	cb_AAPGripVal = false,
-- 	cb_AAPLasrVal = false,
-- 	cb_AAPScopVal = false,
-- 	cb_AAPSeWpVal = false,
-- 	cb_AAPStckVal = false,
-- 	cb_AAPSuppVal = false,
-- 	cb_BulletVal = false,
-- 	cb_FLMuzzleVal = false,
-- 	cb_LaserVal = false,
-- 	cb_LsMuzzleVal = false,
-- 	cb_ProjectileAttVal = false,
-- 	cb_SeCockVal = false,
-- 	cb_SeMuzzleVal = false,
-- 	cb_SeTriggerVal = false,
-- 	cb_StockVal = false,
-- 	cb_VFX_SeEjectVal = false,
-- 	cb_SuMuzzleVal = false,
-- 	cb_VFX_SuEjectVal = false,
-- 	
-- 	--NOW INITIALISE THE RADIO BUTTON STATES
-- 	rad_None = false,
-- 	rad_Pistol = false,
-- 	rad_Rifle = false,
-- 	rad_Melee = false,
-- 	rad_Bow = false,
-- 	rad_Crossbow = false,
-- 	
-- 	rad_state = false,
	
	
	-- FUNCTIONS ---------------------------------------------
	fn dispose = 
	(
		if (MainWindow != undefined) do 
		(
			-- WINDOW POS 
			settings.wpf_windowPos mainWindow #set
			-- CLOSE AND DISPOSE THE WINDOW
			MainWindow.Close()     
			MainWindow = undefined
		)
		execute ("RSG_MrSkeleton2_Struct = undefined")
	),
	-------------------------------------------------------------------
	fn init =
	(
		-- LOAD ASSEMBLY 
		
		dotnet.loadAssembly  @"X:\wildwest\src\Library\RSG.TechArt\RSG.TechArt.rs_mrSkeleton2\RSG_MrSkeleton2\RSG_MrSkeleton2\bin\Debug\RSG_MrSkeleton2.dll"
		-- MAKE MAIN WINDOW INSTANCE 
		MainWindow = dotNetObject "RSG_MrSkeleton2.MainWindow"             
		dn_window.setup MainWindow 
		MainWindow.Tag = dotnetmxsvalue this
		-- PARENTS THE WINDOW UNDER MAX, STOPS IT FROM DROPPING BEHIND
		MainWindow.ChangeOwner (DotNetObject "System.IntPtr" (Windows.GetMAXHWND()))			
		-- WINDOW POS
		settings.wpf_windowPos mainWindow #get
		viewModel = MainWindow.DataContext
			
		-- SHOW THE WINDOW
        MainWindow.show()

	),
	
	fn buttonClickTest = 
	(
-- 		messagebox ("Not implemented yet!") beep:true
		
		format ("viewModel.miscCheckBoxes.cb_PreserveExistingExpressionsVal: "+(viewModel.miscCheckBoxes.cb_PreserveExistingExpressionsVal as string)+"\n")		

	),
	
	fn btn_LoadMarkerPreset = 
	(
		markerFileName = getOpenFileName filename:markerPresetPath caption:"mkr File" types:"Marker Data (*.mkr)|*.mkr|All Files (*.*)|*.*|"
		
		if markerFileName != undefined then
		(		
			--format ("After filein count "+(masterMarkerArray.count as string)+"\n")
			RSTA_buildMarkerFromMarkerFile markerFileName  
			
			format ("Markers loaded.\n")
		)
		else
		(
			format ("Cannot Load as no valid file name was picked."+"\n")
			messagebox ("Cannot Load as no valid file name was picked."+"\n") beep:true
		)
	),
	
	-- EVENTS -----------------------------------------------------
	on create do init()            
)
RSG_MrSkeleton2_Struct = RSG_MrSkeleton2_Struct()

