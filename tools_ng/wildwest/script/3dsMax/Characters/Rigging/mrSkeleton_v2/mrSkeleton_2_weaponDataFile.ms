--weapon data file

--for the presets we have a gripMesh, a marker file (contains markers for base nodes required by the selected archetype, and a array of nodes which align index 2 to 1

weaponMover = (theWildWest+"data/Rigging/mrSkeletonMergeNodes/weaponMover.max")
markerPresetFolder = (theWildWest+"data/Rigging/WeaponParts/")

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

weapon_rifle_grip_mesh = (theProjectRoot+"art/Models/Props/weapons/Weapon_Rigging/Preset_Rifle.max")

weapon_pistol_grip_mesh = (theProjectRoot+"art/Models/Props/weapons/Weapon_Rigging/Preset_Pistol.max")

weapon_bow_grip_mesh = (theProjectRoot+"art/Models/Props/Weapons/Weapon_Rigging/Weapon_Bow_Grip_Meshes.max")		
	
weapon_crossbow_grip_mesh = (theProjectRoot+"art/Models/Props/Weapons/Weapon_Rigging/Weapon_CrossBow_Grip_Meshes.max")	


PistolData = #(
	weapon_pistol_grip_mesh, --grip meshes
	#( --marker files
		(theWildwest+"/data/Rigging/WeaponParts/GripParts/Preset_Pistol.mkr")
	),
	#( --nodes to align to each other
-- 		#("Marker_SKEL_GripR", "PH_R_Hand")
		#("PH_R_Hand", "Marker_GUN_GripR")
		)
	)

RifleData = #(
	weapon_rifle_grip_mesh, --grip meshes
	#( --marker files
		(theWildwest+"/data/Rigging/WeaponParts/GripParts/Preset_Rifle.mkr")
		),
	#( --nodes to align to each other
		#("PH_R_Hand", "Marker_GUN_GripR"),
		#("PH_L_Hand", "Marker_GUN_GripL")
		)
	)

MeleeData = #()

BowData = #()

CrossBowData = #()

