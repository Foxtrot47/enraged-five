--mrSkeleton_2_propRigging_functions


fn RSTA_autoSkin meshtoskin dummyObj = 
(
	setAttribute meshtoskin "Authored Orients" true
	
	boneInfSize = 0.2 -- an initial size for the skin bone radius in metres
	
	local rootHierarchyArray = #(dummyObj) --array of all chidlren of root Obj
	
 	rootHierarchyArray = RSTA_getAllChildren dummyObj arr:rootHierarchyArray#()
	
	select meshtoskin
	
	local skinMod = Skin()
	skinMod.bone_limit = 4
	addModifier meshtoskin skinMod
	
	-- Make sure we're on the modify panel because we have to be to use the skinOps struct to add bones and edit them
	if (getCommandPanelTaskMode() != #modify) do (setCommandPanelTaskMode #modify)
	modPanel.setCurrentObject skinMod ui:True
	
	-- Loop through all the bones adding and setting them up
	boneNo = 0	
	--format ("rootHierarchyArray.count: "+(rootHierarchyArray.count as string)+"\n")
	for boneIndex = 1 to rootHierarchyArray.count do 
	(
		if (substring rootHierarchyArray[boneIndex].name 1 5 ) != "Dummy" then
		(
			if (substring rootHierarchyArray[boneIndex].name 1 5 ) != "Mover" then
			(
				-- If i is less than the number of bones we can add a bone without updating the mesh
				-- When the last bone is added, we also update the mesh
				if boneIndex < rootHierarchyArray.count then
				(
					skinOps.AddBone skinMod rootHierarchyArray[boneIndex] 0
					--format ("Added "+rootHierarchyArray[boneIndex].name+"\n")
					boneNo = boneNo + 1
				)
				
				else
				(
					skinOps.AddBone skinMod rootHierarchyArray[boneIndex] 1
					--format ("Added "+rootHierarchyArray[boneIndex].name+"\n")
					boneNo = boneNo + 1
				)
				
				-- set the radius of these bones in the skin mod
				cCount = skinOps.getNumberCrossSections skinMod boneNo
				for i = 1 to cCount do 
				(
					skinOps.SetInnerRadius skinMod boneNo i boneInfSize
					skinOps.SetOuterRadius skinMod boneNo i boneInfSize
				)
			)
		)
	)
)



fn RSTA_collectRiggingObjects = 
(
	local objectList = getCurrentSelection()
	local riggingMesh = #()
	local bonelist =#()
	local geocount = 0
	
	local riggingList = #() -- this will be returned
	
	-- collect all the data
	for obj in objectList do
	(
		--Find the bones, all point helpers in this function
		if (isKindOf obj Point) do 
		(
			append bonelist obj
		)	
		
		-- Find the mesh
		if superclassof obj == GeometryClass then 
		(	
			-- Discard the mover mesh - if n
			if (substring obj.name 1 6) != "Mover_" then 
			(
				geocount += 1
				
				-- Is it a LOD?
				local flod = findString obj.name "LOD"
				
				if (flod == undefined) or (queryBox "A selected mesh seems to be a LOD - add it?" beep:false) do 
				(
					append riggingMesh obj
				)
			)
		)
	)		

	append riggingList riggingMesh[1] -- populate array with first mesh found.
	append riggingList bonelist -- add the bones
	
	-- Check that the meshes are valid
	if (riggingMesh.count < 1) then 
	(
		messageBox "No meshes found in selection." title:"Invalid Selection"
		riggingList = undefined
	)
	
	if (riggingMesh.count > 1) then 
	(
		messageBox "More than 1 geometry in selection, please include only one." title:"Invalid Selection"
		riggingList = undefined
	)
	
	-- check that bone lists is valid too!
	local bonenames = #()
	local uniquebonenames = #()
	for thebone in bonelist do append bonenames thebone.name
	for thebone in bonenames do appendIfUnique uniquebonenames thebone
		
	if (bonenames.count != uniquebonenames.count) then
	(
		messageBox "Duplicate bone names found - please check the names of the bones (I've selected the bones for you.)"
		select bonelist
		riggingList = undefined
	)
	
	return riggingList
)

fn RSTA_createDeformerAtVert parentNode = 
(
	local selObjs = for obj in (getCurrentSelection()) where (isProperty obj #mesh) collect obj
	
	if (selObjs.count != 1) do 
	(
		messagebox "Please select ONE valid mesh for rigging" title:"Invalid Selection" beep:true
		return #()
	)

	local selObj = selObjs[1]
	local objectname = selObj.name	
	local outList = #()
	
	vertList = case (classof selObj) of 
	(
		Editable_Poly:(( selObj.GetSelection #Vertex ) as array)
		Editable_mesh:(( getVertSelection selObj ) as array)
		Default:#()
	)
	
	if (vertList.count < 1) do 
	(
		MessageBox "No verts selected"
		return false
	)
	
	local outObjs
	
	-- For each vert, create a point helper
	for v = 1 to vertList.count do 
	(
		local thevert = vertList[v]
		local vertPos = selObj.vertices[thevert].pos
		
		local deformername = objectname + "_Bone_" + (v as string)
		local deformerObj = RSTA_buildMarkerObj deformername vertPos
		
		RSTA_buildMarkerAttributes deformerObj 
		
-- 		addTranslationTag objs:#(deformerObj)
		
		-- try to parent it
		deformerObj.parent = parentNode
		
		append outList deformerObj
	)

	return outList
)


fn RSTA_addPropConstraints Xmin Xmax Ymin Ymax Zmin Zmax constraintType= 
(
	local deformers = for obj in (getCurrentSelection()) where (isKindOf obj Helper) collect obj
	
	if (deformers.count == 0) do 
	(
		messagebox "Please select some deformers for rigging" title:"Invalid Selection"
		return False
	)

	for theDeformer in deformers do
	(	
		--local theDeformer = deformers[d]
		
		if (isKindOf theDeformer Helper) do 
		(
			local createFunc = case constraintType of 
			(
				"R":(createRSRotationConstraint)
				"T":(createRSTranslationConstraint)
				default:undefined
			)
			
			if (createFunc != undefined) do 
			(
				createFunc theDeformer "X" Xmin Xmax ""
				createFunc theDeformer "Y" Ymin Ymax ""
				createFunc theDeformer "Z" Zmin Zmax ""
			)
		)	
	)
)

	
fn RSTA_showHideConstraints showHideBool =
(
	objectlist = getCurrentSelection()
	boneList =#()
	
	
	for obj in objectlist do
	(
		if classof obj == Point then
		(
			append bonelist obj
		)	
	)		
	
	
	for obj in boneList do
	(
		kidlist = obj.children 
		for kid in kidlist do
		(
			if ((classof kid == RsRotationConstraint) or (classof kid == RsTranslationConstraint)) then
			(
				if showHideBool  == false then hide kid
				if showHideBool  == true then unhide kid
			)	
		)	
		
		redrawViews()
	)
)

fn RSTA_generatePropxml ObjectMass BoneStrength BoneRadius DamThresh =
(
	format ("Triggered RSTA_generatePropxml\n")
	local xmldata = RSTA_collectRiggingObjects()
	if (xmldata == undefined) do 
	(
		format ("RSTA_collectRiggingObjects returned undefined\n")
		return False
	)
	
	objectname =xmldata[1].name

	-- Header
	local xmlstring = stringStream ""
	format "<Item>\n" to:xmlstring
	format "\t<objectName>%</objectName>\n" objectname to:xmlstring
	format "\t<objectStrength value=\"%\" />\n" (ObjectMass as string) to:xmlstring
	format "\t\t<DeformableBones>\n" to:xmlstring

	-- Bone section
	for boneindex = 1 to xmldata[2].count do
	(
		thebonename = xmldata[2][boneindex].name
		excludename = ((filterstring thebonename "_")[1])
		
		if ((excludename != "Main") and (excludename != "Root")) then
		(
			-- This is a valid bone for tagging
			format "\t\t\t<Item>\n" to:xmlstring
			format "\t\t\t\t<boneName>%</boneName>\n" thebonename to:xmlstring
			
			format "\t\t\t\t<strength value=\"%\" />\n" (BoneStrength as string) to:xmlstring
			format "\t\t\t\t<radius value=\"%\" />\n" (BoneRadius as string) to:xmlstring
			format "\t\t\t\t<damageVelThreshold value=\"%\" />\n" (DamThresh as string) to:xmlstring
			
			format "\t\t\t</Item>\n" to:xmlstring
		)
	)	

	-- Tail
	format "\t\t</DeformableBones>\n" to:xmlstring
	format "</Item>\n" to:xmlstring
	
	RScreateTextWindow text:(xmlstring as string) title:"XML string" wordWrap:false width:600 height:600 position:[10,10] readonly:true
	
)

fn RSTA_isObjFragName obj = 
(
	matchPattern obj.name pattern:("*_frag_")
)

fn RSTA_addUsefulPropAttributes obj = 
(
	local isDynamicState = true
	local keepBoneRotState = true		
	
	local idxIsDynamic = GetAttrIndex "Gta Object" "Is Dynamic"
	local idxKeepBoneRotations = GetAttrIndex "Gta Object" "Keep Bone Rotations"
	local idxAuthoredOrients = GetAttrIndex "Gta Object" "Authored Orients"		
	
	if ((superClassOf obj) == GeometryClass) do 
	(	
		-- Set the Attributes:
		SetAttr obj idxIsDynamic isDynamicState
		SetAttr obj idxKeepBoneRotations keepBoneRotState
		SetAttr obj idxAuthoredOrients keepBoneRotState
	)
)


fn RSTA_getSelFragRoot = 
(
	-- Returns frag-root for selected object:
	local selObjs = getCurrentSelection()
	
	if (selObjs.count == 0) do 
	(
		messageBox "You don't have any objects selected" title:"Invalid Selection"
		return False
	)

	-- Find selected-object's root:
	local rootobj = WWfindRootObject selObjs[1]
	
	--Is this a frag? (It'll have a _FRAG_ suffix)
	local isfrag = RSTA_isObjFragName rootobj
	
	-- If it wasn't a frag, was it a proxy?
	if (not isfrag) do 
	(
		if (isValidNode rootobj) do 
		(
			rootobj = RsGetGeomNodeByName (rootobj.name + "_frag_")
		)
	)
	
	-- Only continue if we've found a valid frag mesh...
	if not (isValidNode rootobj) do  
	(
		messagebox "Could not find frag root object" title:"Invalid Selection"
		rootobj = undefined
	)
	
	return rootObj
)

fn RSTA_combineAndSkinFrag meshesToAdd skinBones unskinnedBones meshName:"" =
(
-------------------------------------------------------------------------------------------------------------------------
-- Create a single mesh from a group of meshes and skin them using a supplied rig
-------------------------------------------------------------------------------------------------------------------------
--meshesToAdd = a selection of meshes we are going to skin
--skinBones = the array of bones we will be skinning to
--unskinnedBones = an array of bones we need in the modifier but don't skin to
--meshName = what to name the mesh.  if undefined we will inherit the name from the 1st mesh		
	local masterMesh = meshesToAdd[1]
	if (masterMesh == undefined) do return undefined
	
	unhide masterMesh
	
	if (meshName != "") do 
	(
		masterMesh.name = meshName
	)
	
	-- Reset radiosity models, to avoid getting the "The Radiosity Solution has been Invalidated" message:
	if (isKindOf SceneRadiosity.Radiosity Radiosity) do 
	(
		SceneRadiosity.Radiosity.Reset True False
	)
	
	-- Combine meshes into the master mesh:
	local meshVertCounts = #()
	
	pushPrompt ("Combining meshes: " + masterMesh.name)
	for thisMesh in meshesToAdd do 
	(
		convertToPoly thisMesh
		append meshVertCounts thisMesh.numVerts

		-- Attach objects to master-mesh:
		if (thisMesh != masterMesh) do
		(
			masterMesh.EditablePoly.attach thisMesh masterMesh
			--meshOp.attach masterMesh thisMesh attachMat:#MatToID
		)
	)
	--convertToPoly masterMesh
	popPrompt()
	--end combine section

	-- Skinning section:
	pushPrompt ("Skinning mesh: " + masterMesh.name)
	-- Now do the skinning. Add a modifier to the fragmesh
	local skinMod = Skin()
	skinMod.bone_limit = 1 -- we are using solid weighting
	skinMod.rigid_vertices = True
	addModifier masterMesh skinMod
	
	if (getCommandPanelTaskMode() != #modify) do (setCommandPanelTaskMode #modify)
	modPanel.setCurrentObject skinMod ui:True

	-- Add the bones
	pushPrompt "Adding bones to Skin"
	for objIndex = 1 to skinBones.count do
	(
		local theDeformer = skinBones[objIndex]
		skinOps.AddBone skinMod theDeformer 1 
		
		local cCount = skinOps.getNumberCrossSections skinMod objIndex
		for i = 1 to cCount do 
		(
-- 				WHY??
			skinOps.SetInnerRadius skinMod objIndex i 0.01
			skinOps.SetOuterRadius skinMod objIndex i 0.01
		)
	)
	popPrompt()
	
	classOf masterMesh -- a hack to refresh the stack

	-- Now skin the mesh verts 100% to the bones. I know the indices that match each bone
	pushPrompt "Assigning verts to bones"
	local vertRunningTotal = 0
	for theBoneIndex = 1 to skinBones.count do 
	(	
		local elementVerts = meshVertCounts[theBoneIndex]

		for theVertLoop = 1 to elementVerts do
		(
			-- The index of the vert is a based on the loop, plus the number of verts currently in the object
			local theVertIndex = (theVertLoop + vertRunningTotal)
			
			skinOps.setVertexWeights skinMod theVertIndex theBoneIndex 1
		)
		
		vertRunningTotal += elementVerts
	) -- end skinning loop
	popPrompt()
	
	-- Add extra bones to modifier:
	pushPrompt "Adding unskinned bones to Skin"
	if (unskinnedBones.count != 0) do
	(
		for objIndex = 1 to unskinnedBones[1].count do
		(
			theDeformer =unskinnedBones[1][objIndex]
			skinOps.AddBone skinMod theDeformer 1 
			
			local cCount = skinOps.getNumberCrossSections skinMod objIndex
			for i = 1 to cCount do 
			(
-- 					WHY????
				skinOps.SetInnerRadius skinMod objIndex i 0.01
				skinOps.SetOuterRadius skinMod objIndex i 0.01
			)
		)
	)
	popPrompt()
	--end skinning section

	masterMesh.wirecolor = color 100 0 100
	setAttribute masterMesh "Is Fragment" True
	
	popPrompt()
	
	return masterMesh--pass out the mesh
)--end RSTA_combineAndSkinFrag



fn RSTA_unskinAndBreakFrag skinnedMesh skinMod =
(
	-------------------------------------------------------------------------------------------------------------------------
	--unskin a mesh and create new meshes based on the skinned skeleton
	-------------------------------------------------------------------------------------------------------------------------
	--skinnedMesh = the mesh we are going to break apart		
	pushPrompt ("Unskinning and breaking up mesh: " + skinnedMesh.name)
	
	if (getCommandPanelTaskMode() != #modify) do (setCommandPanelTaskMode #modify)
	modPanel.setCurrentObject skinMod ui:True

	skinnedMesh.name = "SKINNED_" + skinnedMesh.name --we'll rename the mesh we are using so there aren't any conflicts later
	
	bonesInSkin = #()
	vertArray = #()

	for bs=1 to skinops.getNumberBones skinMod do
	(
		realBone = getBoneFromSkin skinMod bs
		if realBone != undefined do (appendIfUnique bonesInSkin realBone)
	)
	
	local vertCount = skinOps.getNumberVertices skinMod

	for thisBone in bonesInSkin do
	(
		local boneVerts = #{}
		
		for vertNum = 1 to vertcount do
		(
			local vertBoneCount = skinOps.getVertexWeightCount skinMod vertNum

			for vertBoneIdx = 1 to vertBoneCount do
			(
				local boneID = skinOps.getVertexWeightBoneId skinMod vertNum vertBoneIdx
				
				if (thisBone.name == skinOps.GetBoneName skinMod boneID 0) do 
				(
					boneVerts[vertNum] = True
				)
			)
		)
		
		if (boneVerts.numberSet !=0) do 
		(
			append vertArray (dataPair bone:thisBone verts:boneVerts)
		)
	)
	
	local createdMeshes = #()
	local skinnedBones = #()
	
	--for each bone in our array we will select the verts skinned to it and detach them as a mesh using the bones name
	local detachedVerts = #{}
	for boneSelect = 1 to vertArray.count do
	(
		local thisFragObj

		if (boneSelect == 1) then 
		(
			-- Don't detach verts from original mesh:
			thisFragObj = skinnedMesh
			convertToPoly thisFragObj
		)
		else 
		(
			-- Detach verts, and name the new mesh:
			local vertNums = vertArray[boneSelect].verts
			polyop.detachVerts skinnedMesh vertNums delete:False asNode:True name:"temp_mesh_name"
			local thisFragObj = getNodeByName "temp_mesh_name"
			
			detachedVerts += vertNums
		)
		
		local meshBone = vertArray[boneSelect].bone
		local boneName = vertArray[boneSelect].bone.name--the new mesh name base
		
		thisFragObj.name = boneName
		thisFragObj.pivot = meshBone.pivot
		thisFragObj.wirecolor = (RsGetRandomColour())

		append createdMeshes thisFragObj
		append skinnedBones meshBone
	)
	
	-- Delete detached verts from root-mesh:
	polyop.deleteVerts skinnedMesh detachedVerts

	local parentBones= for obj in skinnedBones collect (dataPair obj:obj objParent:obj.parent)
	
	-- Parent meshes to new frag-root:
	for meshNum = 1 to parentBones.count do
	(
		local theBoneParent = parentBones[meshNum].objParent
		
		if (theBoneParent != undefined) do
		(
			local theMesh = createdMeshes[meshNum]
			local boneParentNum = findItem skinnedBones theBoneParent

			if (boneParentNum != 0) do 
			(
				theMesh.parent = createdMeshes[boneParentNum]
			)
		)
	)
	
	local isFirstObj = True
	for thisObj in createdMeshes where (getAttrClass thisObj == "Gta Object") do
	(
		if isFirstObj then 
		(
			-- Copy attributes from root-object:
			CopyAttrs thisObj
		)
		else 
		(
			-- Paste attributes onto children:
			PasteAttrs thisObj
		)
		
		-- Set first object as frag-root, but not its children:
		setAttribute thisObj "Is Fragment" isFirstObj
		
		isFirstObj = False
	)
	
	popPrompt()
	
	return createdMeshes--pass out the new meshes
)--end RSTA_unskinAndBreakFrag



fn RSTA_createFragSkeletonFromMesh meshArray boneSize boneColour =
(
	-------------------------------------------------------------------------------------------------------------------------
	-- Function to create a skeleton based on an existing hierarchy.  we 'mimic' the hierarchy of the passed in mesh
	--	meshArray = a bunch of stuff we'll pass in and create a skel for
	--	boneSize = the size of the PointHelper.  boneColour = the colour of the PointHelper (passed in as an array #(r,g,b).  used in the buildPointHelper function
	-------------------------------------------------------------------------------------------------------------------------		
	
--find out the parent of each mesh and store it
	meshesOnly =#() -- the meshes
	
	-- Split the selection by meshes and non meshes, and store this plus the parent object
	for obj in meshArray do
	(	
		if superClassOf obj == GeometryClass do append meshesOnly #(obj, obj.parent)
	)
--
-- Create a root structure
	boneList = #()
	newBoneList = #()
	
	-- create a bone for each mesh
	for objIndex = 1 to meshesOnly.count do 
	(	
		theMesh = meshesOnly[objIndex][1]
		theParent = meshesOnly[objIndex][2]
		theMeshName = theMesh.name

-- 		buildPointHelper "temp_bone_name" theMesh.pos boneSize (color bonecolour[1] bonecolour[2] bonecolour[3])
-- 		theBone = getnodebyname "temp_bone_name"--we give the bone a unique name so we can select it safely
-- 		theBone.name = (theMeshName+"_bone")--now we have a node and can rename the bone
		
-- 		theBone = RSTA_buildMarkerObj ("Bone_"+theMeshName) theMesh.pos
		theBone = RSTA_buildMarkerObj (theMeshName) theMesh.pos
			theBone.wirecolor = (color bonecolour[1] bonecolour[2] bonecolour[3])
-- 				RSTA_buildMarkerAttributes theBone
		setUserPropbuffer thebone "exportTrans = true"
		append boneList #(theBone, theParent)
		append newBoneList theBone
	)
	
	--link the bones based on the original mesh hierarchy
	for deformerIndex = 1 to boneList.count do 
	(
		if boneList[deformerIndex][2] != undefined do (boneList[deformerIndex][1].parent = boneList[deformerIndex][2])
	)
--
	newBoneList--return the list of bones we made
)--end RSTA_createFragSkeletonFromMesh



fn RSTA_transferFragChildren fromArray toArray =
(
	initialNames = #(
		#(), --init name
		#(), --safe name
		#() -- the obj
		)
		
	for obj in toArray do 
	(
		append initialNames[1] obj.name
		--format ("InitialName: "+obj.name+"\n")
		if substring obj.name 1 5 == "Bone_" do 
		(
			obj.name = substring obj.name 6 -1
		)
		append initialNames[2] obj.name
		append initialNames[3] obj
		--format ("Safe name: "+obj.name+"\n")
	)
	-- Reparents children of objects in "fromArray" to objects with matching names in "toArray"
-- 	local hasChildren = for obj in fromArray where (obj.children.count != 0) collect (dataPair name:(toLower obj.name) children:(join #() obj.children))
	local hasChildren = for obj in fromArray where (obj.children.count != 0) collect (dataPair name:(obj) children:(join #() obj.children))
	--format ("hasChildren complete "+(hasChildren.count as string)+" entries.\n")
	--create an array of object names so we can check them against our name prefix
	local toArrayNames = for obj in toArray collect (toLower obj.name)
	--format ("toArrayNames complete "+(toArrayNames.count as string)+"\n")
		
		
	--format "==================================================================\n"
	for i = 1 to hasChildren.count do 
	(
		--format ((i as string)+": "+(hasChildren[i] as string)+"\n")
		thisData = hasChildren[i]

		fromItem = thisData.name
		--format ("fromItem: "+(fromItem as string)+"\n")
				
-- 		local toObjNum = findItem toArrayNames (toLower (substring fromItem.name 6 -1))
		
		local toObjNum = undefined
		
		for tn = 1 to toArrayNames.count do 
		(
			if (toLower (substring fromItem.name 6 -1 )) == toArrayNames[tn] then
			(
				toObjNum = tn
				--format ("Setting tn to "+(tn as string)+" because "+((toLower (substring fromItem.name 6 -1 ))+" == "+toArrayNames[tn] )+"\n")
			)
			else
			(
				--format ((toLower (substring fromItem.name 6 -1 ))+" != "+toArrayNames[tn]+"\n" )
				if (toLower fromItem.name ) == toArrayNames[tn] then
				(
					toObjNum = tn
					--format ("Setting tn to "+(tn as string)+" "+(toLower fromItem.name )+" == "+toArrayNames[tn]+"\n")
				)
				else
				(
					--format ((toLower fromItem.name )+" != "+toArrayNames[tn]+"\n")
				)
			)
		)
		
		
		--format ("toObjName:"+(toArrayNames[toObjNum] as string)+" num:"+(toObjNum as string)+"\n")
			for childObj in thisData.children do 
			(
				--format ("childObj: "+(childObj as string)+"\n")
				if ((findItem fromArray childObj) == 0) do -- a check to make sure we don't carry over our from parent meshes (ie skeleton)
				(
					childObj.parent = toArray[toObjNum] -- link the child to new parent
				)
			)
	)

	--format "==================================================================\n"		
	--for every object with children

	--format "fromItem complete\n"	
	for ind = 1 to initialNames[1].count do 
	(
		obj = initialNames[3][ind]
		--format ("Renamed name: "+obj.name)
		obj.name = initialNames[1][ind]
		--format ("Renamed back to "+obj.name)
	)
)



fn RSTA_ConvertSelectionToSkinnedFrag = 
(
	-- This function will take a standard frag and convert it to a skinned frag
	local rootObj = RSTA_getSelFragRoot()
	if (rootObj == undefined) do return False

	-- Abort if is is already a Skinned Frag:
	if (RSTA_GetSkinModifier rootObj != undefined) do  
	(
		messagebox "This object already has a Skin modifier!" title:"Convert Failed"
		return False
	)

	-- Need to grab all the bones and the particles too - basically any helpers attached to the meshes
	local objectList = #(rootObj)
-- 		local objectList = RsGetFlatHierarchy rootObj
	objectList = RSTA_getAllChildren rootObj arr:objectList

	local meshList = for obj in objectList 
		where ((superClassOf obj == GeometryClass) and ((UDPsearch obj "isdamage = true") == 0)) 
		collect obj

	local fragMeshbones = RSTA_createFragSkeletonFromMesh meshList 0.5 #(255,255,0)

	--format ("RSTA_ConvertSelectionToSkinnedFrag: frag skeleton generated.\n" )
		
	for obj in fragMeshbones do
	(
		format (obj.name+" is a fragmeshBone\n")
		setUserPropbuffer obj "exportTrans = true"
	)

	RSTA_transferFragChildren meshList fragMeshbones -- Transfer the helpers (and anything else that is linked) over to new bones
	--format ("RSTA_ConvertSelectionToSkinnedFrag: transferfragchildren complete...\n")
-- 		local rootSetup = buildRigRootSystem rootobj--create some root objects
	RSTA_buildPropRoot rootObj false true
	--format ("RSTA_ConvertSelectionToSkinnedFrag: propRoot built\n")
	
	
	local dummyNode = getNodeByName ("Dummy_"+rootObj.name+"_P_S")
		format ("Searched for "+("Dummy_"+rootObj.name)+" got "+(dummyNode as string)+"\n")		
		local dummyNodeNameCount = dummyNode.name.count
		dummyNode.name = (substring dummyNode.name 1 (dummyNodeNameCount - 4)) --have to change the name back to frag compatible stuff
	
	local moverNode = getNodeByName ("Mover_"+rootObj.name+"_P_S")
		format ("Searched for "+("Mover_"+rootObj.name)+" got "+(moverNode as string)+"\n")
		local moverNodeCount = moverNode.name.count
		moverNode.name = (substring moverNode.name 1 (moverNodeCount - 4)) --have to change the name back to frag compatible stuff
		--need to now set the txd shit on the mover
			
		local meshNameCount = meshList[1].name.count			
-- 		local txdName = substring meshList[1].name 1 (meshName.count - 6)
		local txdName = substring meshList[1].name 1 (meshNameCount - 6)
		setAttr moverNode (getattrindex "Gta Object" "TXD") txdName
	
	local root = getNodeByName ("Root_"+rootObj.name+"_P_S")
		format ("Searched for "+("Root_"+rootObj.name)+" got "+(root as string)+"\n")
		local rootCount = root.name.count
		root.name = (substring root.name 1 (rootCount - 4)) --have to change the name back to frag compatible stuff
			
	local mainNode = getNodeByName ("Main_"+rootObj.name+"_P_S")
		format ("Searched for "+("Main_"+rootObj.name)+" got "+(mainNode as string)+"\n")
		local mainNodeCount = mainNode.name.count
		mainNode.name = (substring mainNode.name 1 (mainNodeCount - 4)) --have to change the name back to frag compatible stuff
	
	fragMeshbones[1].parent = mainNode
	
	local rootSetup = #(
		#(root, mainNode),
		#(dummyNode, moverNode)
		)
	
-- 		fragMeshbones[1].parent = rootSetup[1][2]--link skeleton to root objects
		
	local combinedMesh = RSTA_combineAndSkinFrag meshList fragMeshbones rootSetup
	--format ("RSTA_ConvertSelectionToSkinnedFrag: comineskinfrag complete\n")
	-- Set the Attributes:
	RSTA_addUsefulPropAttributes combinedMesh
	--format ("RSTA_ConvertSelectionToSkinnedFrag: useful properties added.\n")
	select rootObj
	
	return rootObj
)



fn RSTA_ConvertSkinnedSelectionToFrag =
(
	-- This function will take a skinned frag and convert it to a standard multi-object frag
	local rootObj = RSTA_getSelFragRoot()
	
	format ("rootObj derived to be: "+(rootObj as string)+"\n")
	
	if (rootObj == undefined) do return False

	local skinMod = RSTA_GetSkinModifier rootObj
	if (skinMod == undefined) do  
	(
		messagebox "Object doesn't use Skin modifier!" title:"Convert Failed"
		return False
	)

	if (getCommandPanelTaskMode() != #modify) do (setCommandPanelTaskMode #modify)
	modPanel.setCurrentObject skinMod ui:True
	
	--work out the frag skeleton
	local aRealBone = getBoneFromSkin skinMod 1		--get first bone from the skin modifier
	format ("aRealBone: "+(aRealBone as string)+"\n")
	
	local rootSkelObj = WWfindRootObject aRealBone	--work out the root bone
	format ("rootSkelObj: "+(rootSkelObj as string)+"\n")
	
	local tempSkelList=#()
	for bs=1 to (skinops.getNumberBones skinMod) do
	(
		local realBone = getBoneFromSkin skinMod bs
		if (realBone != undefined) do (appendIfUnique tempSkelList realBone)
	)

	sliderTime = 0f--set the timeline to frame 0
	
	local brokenMeshes = RSTA_unskinAndBreakFrag rootobj skinMod --break the mesh in to frag parts
	format ("brokenMeshes: "+(brokenMeshes as string)+"\n")
	
	format ("tempskelList: "+(RSTA_transferFragChildren as string)+"\n")
	
	RSTA_transferFragChildren tempSkelList brokenMeshes --transfer the hierarchy
format ("RSTA_transferFragChildren completed\n" )
	
	--Delete the old hierarchy parts:
-- 		delete (RsGetFlatHierarchy rootSkelObj)
	obArr = #(rootSkelObj)
	obArr = RSTA_getAllChildren rootSkelObj arr:obArr
	delete obArr
	
	local retVal = brokenMeshes
	select retVal[1]
	
	return retVal
)



fn RSTA_ExplodeSelectedFrag =
(
	-- Explodes Skinned Frag's bone-objects out from midpoint, in frame 10:
	local rootObj = RSTA_getSelFragRoot()
	if (rootObj == undefined) do return False
	
	local skinMod = RSTA_GetSkinModifier rootObj
	if (skinMod == undefined) do 
	(
		messagebox "Object doesn't use Skin modifier!" title:"Explode Failed"
		return False
	)
	
	-- Grab some bone data from the skin modifier:
	sliderTime = 0f
	if (getCommandPanelTaskMode() != #modify) do (setCommandPanelTaskMode #modify)
	modPanel.setCurrentObject skinMod ui:True
	totalSkinModBones = skinOps.getNumberBones skinMod
	
	-- Collect the bones in the skin modifier
	local boneList = for i = 1 to totalSkinModBones collect 
	(	
		local boneName = (skinOps.getBoneName skinMod i 1)
		
		-- Use helper, if multiple nodes were found with this name:
		local namedBones = getNodeByName boneName all:True
		if (namedBones.count > 1) do 
		(
			namedBones = for obj in namedBones where (isKindOf obj Helper) collect obj
		)
		
		local thisBone = namedBones[1]
		
		if ((thisBone == undefined) or (thisBone == rootObj)) then dontCollect else (dataPair obj:thisBone pos:thisBone.pos)
	)
	
	-- set a keyframe on all these bones at frame 0
	sliderTime = 0f
	for item in boneList do 
	(
		with animate on
		(
			addNewKey item.obj[3] 0
		)
	)	

	-- Now move the bones relative to the root.
	local rootPos = rootObj.pos
	
	-- set a keyframe on all these bones at frame 10
	sliderTime = 10f
	for item in boneList where (thisBone != rootObj) do 
	(
		-- calculate how far this bone should move
		-- make it the distance between the position and the central point x2
		local thisBone = item.obj
		local bonePos = item.pos
		local offset = (bonePos - rootPos) * 2
		
		with animate on
		(
			thisBone.pos = (bonePos + offset)
			addNewKey thisBone[3] 10
		)
	)	

	return OK
)


fn RSTA_UnExplodeSelectedFrag =
(
	sliderTime = 0f
)


fn RSTA_gotLoadError theNum = -- returns the error messages for object loading
(
	local loadErrors = #("Selection not a valid mesh","Selection count must be 1","Select a Skinned object before loading the Prop")
	
	msg = loadErrors[theNum]
	messagebox msg title:"Error"
	getSourceObjBtn.checked = false
	getSourceObjBtn.text = "[pick object]"
	return false
)

fn RSTA_sourceFilter obj = 
(
	(isKindOf obj.baseObject Editable_Poly) or (isKindOf obj.baseObject Editable_mesh)
)	

fn RSTA_targetFilter obj =
(
	((isKindOf obj.baseObject Editable_Poly) or (isKindOf obj.baseObject Editable_mesh)) and (obj.modifiers[#skin] != undefined)
)	

fn RSTA_loadSourcePropTransferSkin targetObj = -- loads the new prop object
(
	if targetObj != undefined then
	(
		clearSelection() 
		thecaption = "Open Max File" 
		thefilename = (RsConfigGetProjRootDir()+"art/") 
		theFiletypes = "Max File(*.max)|*.max|All|*.*|" 
		thehistoryCategory = "RsArtPresets"
		
		filename = getOpenFileName caption:thecaption filename:thefilename types:theFiletypes historyCategory:thehistoryCategory	
		if filename != undefined then	
		(	
			mergeMAXFile filename #select #prompt #mergeDups #useMergedMtlDups #promptReparent 
			if $selection.count == 1 then -- has to be single object
			(
				obj = $selection[1]
				if obj != undefined and RSTA_sourceFilter obj== true then  -- is a valid edit poly or edit mesh
				(
-- 						getSourceObjBtn.text = obj.name
					::subRollPropTransferSkinPropRoll.getSourceObjBtn.text = obj.name
-- 						getSourceObjBtn.checked = false
					::subRollPropTransferSkinPropRoll.getSourceObjBtn.checked = false
					sourceObj = obj	
					return true
				)
				else 
				(	
					delete $selection					
					checkError =  (RSTA_gotLoadError 1)
					if checkError == false then return false
				)
			)
			else 
			(
				delete $selection
				checkError =  (RSTA_gotLoadError 2)
				if checkError == false then return false
			)
		)
		else 
		(
-- 				getSourceObjBtn.checked = false
			::subRollPropTransferSkinPropRoll.getSourceObjBtn.checked = false
-- 				getSourceObjBtn.text = "[pick object]"
-- 				getSourceObjBtn.text = "[pick New]"
			::subRollPropTransferSkinPropRoll.getSourceObjBtn.text = "[pick New]"
			return false
		)
	)
	else 
	(
		checkError =  (RSTA_gotLoadError 3)
		if checkError == false then return false
	)
)

fn RSTA_replaceTargetWithSourcePropTransferSkin obj targetObj skinInfo = 
(
	-- Extract the skindata
	skinUtils.ExtractSkinData targetObj
	theSkinData = getnodebyname ("SkinData_" + targetObj.name)
	
	-- set up the new prop to match the skinned obj
	obj.center = skinInfo.theObjectCenter 
	
	if (getCommandPanelTaskMode() != #modify) do (setCommandPanelTaskMode #modify)
	
	select obj
	maxOps.CollapseNodeTo obj 1 off
	convertTo obj skinInfo.theObjectClass
	
	-- Add the skin and bones and import skin data
	modPanel.addModToSelection (Skin ()) ui:on
	theSkin = obj.modifiers[1]
	for i in 1 to skinInfo.infBones.count do
	(
		if i == skinInfo.infBones.count then updateInteger = 1 else updateInteger = 0
		skinOps.addbone theSkin skinInfo.infBones[i] updateInteger
	)
	selectmore theSkinData
	skinUtils.ImportSkinDataNoDialog true false false false  false 1.0 0		
	delete theSkinData
	
	-- replace the skinned prop with the new prop
	newInstance = instanceReplace targetObj obj
	InstanceMgr.MakeObjectsUnique $ #individual 
	delete obj	
	
	-- clean up
	skinInfo = RsSkinInfo()
	sourceObj = undefined
-- 		getSourceObjBtn.text = "[pick object]"
	::subRollPropTransferSkinPropRoll.getSourceObjBtn.text = "[pick New]"
	targetObj = undefined
-- 		getTargetObjBtn.text = "[pick object]" 
	::subRollPropTransferSkinPropRoll.getTargetObjBtn.text = "[pick Skinned]"
)

