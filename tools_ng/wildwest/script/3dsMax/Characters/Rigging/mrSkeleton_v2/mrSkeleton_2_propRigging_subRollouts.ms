	
	
rollout subRollPropBoneConstraints "Bone Constraints"
(
-- 	local defaultRolledUp -- Flags rollout for being rolled-up by default

	button btnShowBoneConstraints "Show Selected" pos:[8, 5] width:85 height:30 offset:[1,0] tooltip:"Show Constraints for Selected Bone"
	button btnHideBoneConstraints "Hide Selected" pos:[100, 5] width:85 height:30 tooltip:"Hide Constraints for Selected Bone"
	
	
	spinner spnTxmin "Tra X Min:" range:[-1,0,-0.25] pos:[5, 40] type:#float fieldwidth:40 offset:[-18,0] tooltip:"Translation Constraint X Minimum Value"
	spinner spnTxmax "Max:" range:[0,1,0.25] pos:[110,40] type:#float fieldwidth:40 offset:[-18,0] tooltip:"Translation Constraint X Miaximum Value"
	spinner spnTymin "Tra Y Min:" range:[-1,0,-0.25] pos:[5,60] type:#float fieldwidth:40 offset:[-18,0] tooltip:"Translation Constraint Y Minimum Value"
	spinner spnTymax "Max:" range:[0,1,0.25] pos:[110,60] type:#float fieldwidth:40 offset:[-18,0] tooltip:"Translation Constraint Y Maximum Value"
	spinner spnTzmin "Tra Z Min:" range:[-1,0,-0.25] pos:[5,80]type:#float fieldwidth:40 offset:[-18,0] tooltip:"Translation Constraint Z Minimum Value"
	spinner spnTzmax "Max:" range:[0,1,0.25] pos:[110,80] type:#float fieldwidth:40 offset:[-18,0] tooltip:"Translation Constraint Z Maximum Value"
	
	button btncreateTConstraints "Add Trans Constraints" width:180 height:30 tooltip:"Add Translation Constraints to Selection"
	
	spinner spnRxmin "Rot X Min:" range:[-180,0,-90] pos:[5,135] type:#float fieldwidth:40 offset:[-18,0] tooltip:"Rotation Constraint X Minimum Value"
	spinner spnRxmax "Max:" range:[0,180,90] pos:[110, 135] type:#float fieldwidth:40 offset:[-18,0] tooltip:"Rotation Constraint X Maximum Value"
	spinner spnRymin "Rot Y Min:" range:[-180,0,-90] pos:[5,155] type:#float fieldwidth:40 offset:[-18,0] tooltip:"Rotation Constraint Y Minimum Value"
	spinner spnRymax "Max:" range:[0,180,90] pos:[110,155]type:#float fieldwidth:40 offset:[-18,0] tooltip:"Rotation Constraint Y Maximum Value"
	spinner spnRzmin "Rot Z Min:" range:[-180,0,-90] pos:[5,175] type:#float fieldwidth:40 offset:[-18,0] tooltip:"Rotation Constraint Z Minimum Value"
	spinner spnRzmax "Max:" range:[0,180,90] pos:[110,175] type:#float fieldwidth:40 offset:[-18,0] tooltip:"Rotation Constraint Z Maximum Value"
	
	button btncreateRConstraints "Add Rot Constraints" width:180 height:30 tooltip:"Add Rotation Constraints to Selection"
	
	-------------------------------------------------------------------------------------------------------------------------

	
	-------------------------------------------------------------------------------------------------------------------------
	on btnShowBoneConstraints pressed do 
	(
		RSTA_showHideConstraints true
	)
	
	on btnHideBoneConstraints pressed do 
	(
		RSTA_showHideConstraints false
	)

	on btncreateTConstraints pressed do 
	(
		currSel = selection as array
		RSTA_addPropConstraints spnTxmin.value spnTxmax.value spnTymin.value spnTymax.value spnTzmin.value spnTzmax.value "T"
		select currSel
	)
	
	on btncreateRConstraints pressed do 
	(
		currSel = selection as array
		RSTA_addPropConstraints spnRxmin.value spnRxmax.value spnRymin.value spnRymax.value spnRzmin.value spnRzmax.value "R"
		select currSel
	)
)

rollout subRollPropBoneXml "XML Generation"
(
		
	spinner spn_ObjectMass "Object Mass:" range:[0,1000,10] type:#float fieldwidth:40 offset:[-2,0]
	spinner spn_BoneStrength "Average Bone Strength:" range:[0,1,0.1] type:#float fieldwidth:40 offset:[-2,0]
	spinner spn_BoneRadius "Average Bone Radius:" range:[0,3,0.5] type:#float fieldwidth:40 offset:[-2,0]
	spinner spn_DamageThreshold "Damage Threshold:" range:[0,10,1.5] type:#float fieldwidth:40 offset:[-2,0]
	
	button btn_generateXML "Generate XML for selected object" width:180 height:30 offset:[1,0] tooltip:"Generate XML for selection"
	
	-------------------------------------------------------------------------------------------------------------------------
	

	-------------------------------------------------------------------------------------------------------------------------
	 
	on btn_generateXML pressed do 
	(
		format "Click\n"
		RSTA_generatePropxml spn_ObjectMass.value  spn_BoneStrength.value  spn_BoneRadius.value spn_DamageThreshold.value
	)
)

rollout subRollPropBoneFragConvert "Frag Converter"
(
	button btnConvertToSkinned "Convert Frag to Skinned Frag" width:180 height:30
	button btnRSTA_ExplodeSelectedFrag "Explode Selected Frag" width:180 height:30 offset:[-1,0]
--	button btnUnRSTA_ExplodeSelectedFrag "Restore Selected Frag" width:210 height:30
	button btnConvertSkinnedToFrag "Convert Skinned Frag to Frag" width:180 height:30
	
	-------------------------------------------------------------------------------------------------------------------------

	-- Function to detect frags based on name:

	


	-------------------------------------------------------------------------------------------------------------------------
	
	on btnConvertToSkinned pressed do 
	(
		undo "Convert Fragments To Skinned" on 
		(
			RSTA_ConvertSelectionToSkinnedFrag()
		)
	)
	
	on btnRSTA_ExplodeSelectedFrag pressed do 
	(
		undo "Explode Frag" on 
		(
			RSTA_ExplodeSelectedFrag()
			completeRedraw()
		)
	)

	on btnConvertSkinnedToFrag pressed do 
	(
		undo "Convert Skinned To Fragments" on
		(
			RSTA_ConvertSkinnedSelectionToFrag()
		)
	)

	-- Save rolled-up state:
	on RsBoneFragConvertRoll rolledUp down do 
	(
		RsSettingWrite "RsBoneFragConvertRoll" "rollup" (not down)
	)
)

rollout subRollPropTransferSkinPropRoll "Update Skinned Props"
(	
	struct RsSkinInfo -- struct to store the target data
	(
		theObject = undefined,
		theObjectCenter = undefined,
		theObjectClass = undefined,
		infBones = #(),	
		
		fn getInfBones =  -- finds the bones used in the objects skin modifier
		(
			if (isValidNode theObject) do 
			(
				local skinMod = RSTA_GetSkinModifier theObject
				if (skinMod != undefined) then
				(
					if (getCommandPanelTaskMode() != #modify) do (setCommandPanelTaskMode #modify)
					modPanel.setCurrentObject skinMod ui:True
					
					numBones = skinOps.GetNumberBones skinMod
					for i in 1 to numBones do
					(
						local boneName = (skinOps.GetBoneName skinMod i 1)
			
						-- Use helper if multiple nodes were found with this name:
						local namedBones = getNodeByName boneName all:True
						if (namedBones.count > 1) do 
						(
							namedBones = for obj in namedBones where (isKindOf obj Helper) collect obj
						)
			
						local thisBone = namedBones[1]
						append infBones thisBone
					)
				)
			)
		),
		
		fn getObjectInfo theObj = -- populates the structs data
		(
			theObject = theObj
			theObjectCenter = theObj.center
			theObjectClass = classof theObj
			if theObjectClass == Editable_mesh then theObjectClass = TriMeshGeometry
			getInfBones()
		)
	)	
	
	-- Locals & Functions
	local sourceObj, targetObj
	
	local skinInfo = RsSkinInfo()
		
	pickbutton getTargetObjBtn "[pick Skinned]" filter:RSTA_targetFilter autoDisplay:true width:180 tooltip:"Pick Skinned Object"

	checkbutton getSourceObjBtn "[pick New]" width:180 tooltip:"Pick New Object"
	
	
	local targetObj = undefined
	local sourceObj = undefined
	
	-- Events	
	on getSourceObjBtn changed val do 
	(
		gotSource = RSTA_loadSourcePropTransferSkin targetObj
		if gotSource == true then
		(
			sourceObj = getNodeByName getSourceObjBtn.text
				RSTA_replaceTargetWithSourcePropTransferSkin sourceObj targetObj skinInfo

		)
	)
	
	on getTargetObjBtn picked obj do 
	(
		targetObj = obj
		getTargetObjBtn.text = obj.name
		skinInfo.getObjectInfo obj
	)	
	
	on getSourceObjBtn rightclick do 
	(
		sourceObj = undefined
		getSourceObjBtn.text = "[pick object]"
	)
	
	on getTargetObjBtn rightclick do 
	(
		targetObj = undefined
		getTargetObjBtn.text = "[pick object]"
	)
)

rollout subRollPropBoneDebugRoll "BoneDebug" height:2
(
	local defaultRolledUp -- Flags rollout for being rolled-up by default
	
	button btngetboneHash "Get Bone hash for selected bone" width:180 height:30 offset:[-4,0]
	
	-------------------------------------------------------------------------------------------------------------------------
	
	-------------------------------------------------------------------------------------------------------------------------
	on btngetboneHash pressed do 
	(
		outstring = RSTA_generateHashInfoForSkinnedBones()
		
		RScreateTextWindow text:outstring title:"Bone debug" wordWrap:false width:380 height:300 position:[10,10] readonly:true
	)
)
