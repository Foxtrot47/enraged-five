
fn RSTA_formatTextBoxB tb tb2 =
(
	tb.multiline = true
	tb.wordwrap = true
	tb.acceptsreturn = true
	tb.scrollbars = (dotnetclass "System.Windows.Forms.ScrollBars").vertical	
	tb.scrollbars = (dotnetclass "System.Windows.Forms.ScrollBars").horizontal
	
	tb2.multiline = true
	tb2.wordwrap = true
	tb2.acceptsreturn = true
	tb2.scrollbars = (dotnetclass "System.Windows.Forms.ScrollBars").vertical	
	tb2.scrollbars = (dotnetclass "System.Windows.Forms.ScrollBars").horizontal		
	
	tb.text = ::subRollNodePick.driverStringViewer
	tb2.text = ::subRollNodePick.driverStringExpr
)	

fn RSTA_buildHydraulicRam driver receiver =
(
	--ok we need to make 2 joints one for driver and one for receiver
	--link the joints to their respective parent
	--freeze their transforms and then add lookats
	--then set thier length to be:  half the distance between the driver and receiver
	
	--driver will be the cylinder parts
	--receiver will be the piston part
	
	local zaxis = [0,-1,0]
	
	local driverBone = BoneSys.createBone [0,0,0] [1,0,0] zaxis
	
	driverBone.transform = driver.transform
	driverBone.parent = driver
	driverBone.name = (driver.name+"_HydraulicCylinder")
	
	local receiverBone = BoneSys.createBone [0,0,0] [1,0,0] zaxis
	
	receiverBone.transform = receiver.transform
	receiverBone.parent = receiver	
	receiverBone.name = (receiver.name+"_HydraulicPiston")
	
	dist = distance driver receiver
	
	driverBone.length = dist / 2
	driverBone.width = driverBone.length / 6
	receiverBone.length = dist / 2
	receiverBone.width = receiverBone.length / 6
	
	select driverBone
	selectMore receiverBone
	
	RSTA_FreezeTransform()
	
	RSTA_createAimConstraint driverBone receiver receiver
	
	RSTA_createAimConstraint receiverBone driver driver
)

fn RSTA_enableExprText = 
(	
	local drvName = undefined
	local drvType = undefined
	local drvTypeExpr = undefined
	local drvAxis = undefined
	local rcvName = undefined
	local rcvType = undefined
	local rcvTypeExpr = undefined
	local rcvAxis = undefined
	
	::subRollNodePick.driverType = ::subRollNodePick.radDriverType.state
-- 	::subRollNodePick.driverAxis = ::subRollNodePick.radDriverAxis.state
	
	::subRollNodePick.receiverType = ::subRollNodePick.radReceiverType.state
-- 	::subRollNodePick.receiverAxis = ::subRollNodePick.radReceiverAxis.state	
	
-- 	if ::subRollNodePick.driverType == 1 then
-- 	(
-- 		--trans
-- 		drvType = "Translate"
-- 		drvTypeExpr = "Translate"

-- 		
-- 		format ("drvType set to Translate")
-- 	)
-- 	else
-- 	(
-- 		if ::subRollNodePick.driverType == 2 then
-- 		( --rot
-- 			drvType = "Rotate"
-- 			drvTypeExpr = "Rotation"
-- 			format ("drvType set to Rotation")
-- 		)
-- 		else
-- 		(
-- 			drvType = "Scale"
-- 			drvTypeExpr = "Scale"
-- 			format ("drvType set to Scale")
-- 		)
-- 	)
-- 	if ::subRollNodePick.driverAxis == 1 then
-- 	(
-- 		--trans
-- 		drvAxis = "X"
-- 	)
-- 	else
-- 	(
-- 		if ::subRollNodePick.driverAxis == 2 then
-- 		( --rot
-- 			drvAxis = "Y"
-- 		)
-- 		else
-- 		(
-- 			drvAxis = "Z"
-- 		)
-- 	)	

-- 	if ::subRollNodePick.radReceiverType.state == 1 then
-- 	(
-- 		--trans
-- 		rcvType = "Translate"
-- 		rcvTypeExpr = "Translate"
-- 	)
-- 	else
-- 	(
-- 		if ::subRollNodePick.radReceiverType.state == 2 then
-- 		( --rot
-- 			rcvType = "Rotate"
-- 			rcvTypeExpr = "Rotation"
-- 		)
-- 		else
-- 		(
-- 			rcvType = "Scale"
-- 			rcvTypeExpr = "Scale"
-- 		)
-- 	)
-- 	if ::subRollNodePick.radReceiverAxis.state == 1 then
-- 	(
-- 		--trans
-- 		rcvAxis = "X"
-- 	)
-- 	else
-- 	(
-- 		if ::subRollNodePick.radReceiverAxis.state == 2 then
-- 		( --rot
-- 			rcvAxis = "Y"
-- 		)
-- 		else
-- 		(
-- 			rcvAxis = "Z"
-- 		)
-- 	)		
	
	if ::subRollNodePick.radDriverType.state == 1 do
	(
		drvType = "Translate"
		drvTypeExpr = "Translate"
		drvAxis = "X"
		
		format ("Picked drv trans X\n")
	)
	if ::subRollNodePick.radDriverType.state == 2 do
	(
		drvType = "Translate"
		drvTypeExpr = "Translate"
		drvAxis = "Y"
		
		format ("Picked drv trans Y\n")
	)
	if ::subRollNodePick.radDriverType.state == 3 do
	(
		drvType = "Translate"
		drvTypeExpr = "Translate"
		drvAxis = "Z"
		format ("Picked drv trans Z\n")
	)
	if ::subRollNodePick.radDriverType.state == 4 do
	(
		drvType = "Rotate"
		drvTypeExpr = "Rotation"
		drvAxis = "X"
		format ("Picked drv rot X\n")
	)
	if ::subRollNodePick.radDriverType.state == 5 do
	(
		drvType = "Rotate"
		drvTypeExpr = "Rotation"
		drvAxis = "Y"
		format ("Picked drv rot Y\n")
	)
	if ::subRollNodePick.radDriverType.state == 6 do
	(
		drvType = "Rotate"
		drvTypeExpr = "Rotation"
		drvAxis = "Z"
		format ("Picked drv rot Z\n")
	)
	if ::subRollNodePick.radDriverType.state == 7 do
	(
		drvType = "Scale"
		drvTypeExpr = "Scale"
		drvAxis = "X"
		format ("Picked drv Scale X\n")
	)
	if ::subRollNodePick.radDriverType.state == 8 do
	(
		drvType = "Scale"
		drvTypeExpr = "Scale"
		drvAxis = "Y"
		format ("Picked drv Scale Y\n")
	)
	if ::subRollNodePick.radDriverType.state == 9 do
	(
		drvType = "Scale"
		drvTypeExpr = "Scale"
		drvAxis = "Z"
		format ("Picked drv Scale Z\n")
	)	

	if ::subRollNodePick.radReceiverType.state == 1 do
	(
		rcvType = "Translate"
		rcvTypeExpr = "Translate"
		rcvAxis = "X"
		format ("Picked rcv trans X\n")
	)
	if ::subRollNodePick.radReceiverType.state == 2 do
	(
		rcvType = "Translate"
		rcvTypeExpr = "Translate"
		rcvAxis = "Y"
		format ("Picked rcv trans Y\n")
	)
	if ::subRollNodePick.radReceiverType.state == 3 do
	(
		rcvType = "Translate"
		rcvTypeExpr = "Translate"
		rcvAxis = "Z"
		format ("Picked rcv trans Z\n")
	)
	if ::subRollNodePick.radReceiverType.state == 4 do
	(
		rcvType = "Rotate"
		rcvTypeExpr = "Rotation"
		rcvAxis = "X"
		format ("Picked rcv Rot X\n")
	)
	if ::subRollNodePick.radReceiverType.state == 5 do
	(
		rcvType = "Rotate"
		rcvTypeExpr = "Rotation"
		rcvAxis = "Y"
		format ("Picked rcv Rot Y\n")
	)
	if ::subRollNodePick.radReceiverType.state == 6 do
	(
		rcvType = "Rotate"
		rcvTypeExpr = "Rotation"
		rcvAxis = "Z"
		format ("Picked rcv Rot Z\n")
	)
	if ::subRollNodePick.radReceiverType.state == 7 do
	(
		rcvType = "Scale"
		rcvTypeExpr = "Scale"
		rcvAxis = "X"
		format ("Picked rcv Scale X\n")
	)
	if ::subRollNodePick.radReceiverType.state == 8 do
	(
		rcvType = "Scale"
		rcvTypeExpr = "Scale"
		rcvAxis = "Y"
		format ("Picked rcv Scale Y\n")
	)
	if ::subRollNodePick.radReceiverType.state == 9 do
	(
		rcvType = "Scale"
		rcvTypeExpr = "Scale"
		rcvAxis = "Z"
		format ("Picked rcv Scale Z\n")
	)	
	
	::subRollNodePick.driverAxis = drvAxis	
	::subRollNodePick.receiverAxis = rcvAxis	
	
	
	if ::subRollNodePick.driver == undefined then
	(
		drvName = "NoDriverPicked"
	)
	else
	(
		if (isValidNode ::subRollNodePick.driver ) then
		(
			drvName = ::subRollNodePick.driver.name
		)
		else
		(
			format ("Driver was invalid...\n")
			::subRollNodePick.driver = undefined
			drvName = "NoDriverPicked"
			RSTA_enableExprText()
		)
	)	
	if ::subRollNodePick.receiver == undefined then
	(
		rcvName = "NoReceiverPicked"
	)
	else
	(
		if (isValidNode ::subRollNodePick.receiver) then
		(
			rcvName = ::subRollNodePick.receiver.name
		)
		else
		(
			::subRollNodePick.receiver = undefined
			rcvName = "NoReceiverPicked"
			RSTA_enableExprText()
		)
	)
	
	
	::subRollNodePick.driverTypeString = drvType	
	::subRollNodePick.driverTypeExpr = drvTypeExpr
	::subRollNodePick.driverAxisString = drvAxis
	
	::subRollNodePick.receiverTypeString = rcvType	
	::subRollNodePick.receiverTypeExpr = rcvTypeExpr
	::subRollNodePick.receiverAxisString = rcvAxis	
		
	::subRollNodePick.txtDriverName.text = drvName
	::subRollNodePick.txtReceiverName.text = rcvName
	
	::subRollNodePick.driverStringViewer = ("When you "+drvType+" "+drvName+" in the "+drvAxis+" axis ")
	::subRollNodePick.driverStringExpr = (drvName+"_"+drvAxis+"_"+drvTypeExpr)
	
	::subRollNodePick.receiverStringViewer = ("you will "+rcvType+" "+rcvName+" in the "+rcvAxis+" axis")
	::subRollNodePick.receiverStringExpr = (rcvName+"_"+rcvAxis+"_"+rcvTypeExpr)

	if ::subRollNodePick.driver != undefined then
	(
		if ::subRollNodePick.driverAxis != undefined then
		(
			if subRollNodePick.receiver != undefined then
			(
-- 				if subRollNodePick.receiverAxis != undefined do
-- 				(
					::subRollExpressionType.ddlExpType.enabled = true
					
					if ::subRollExpressionType.expSelection != undefined then
					(
						::subRollExpressionText.txtFriendlyExpressionText.enabled = true
						::subRollExpressionText.btnCreateExpression.enabled = true
						::subRollExpressionText.txtRawExpressionText.enabled = true
					)
					else
					(
						::subRollExpressionText.txtFriendlyExpressionText.enabled = true
						::subRollExpressionText.btnCreateExpression.enabled = true
						
						::subRollExpressionText.txtRawExpressionText.enabled = true
					)
-- 				)
			)
			else
			(
				format "subRollNodePick.receiver == undefined\n"
			)
		)
		else
		(
			format "subRollNodePick.driverAxis == undefined\n"
		)
	)
	else
	(
		format ("subRollNodePick.driver == undefined\n")
	)
	
	::subRollExpressionText.txtFriendlyExpressionText.text = (::subRollNodePick.driverStringViewer + ::subRollNodePick.receiverStringViewer )
	::subRollExpressionText.txtRawExpressionText.text = (::subRollNodePick.driverStringExpr)--	+" "+ ::subRollNodePick.receiverStringExpr	)
	
	--hack to try and get the expressiojn text shit working properly when changing radio buttons after expression type set
	
	::subRollExpressionType.RSTA_updateSpinnerValue ::subRollExpressionType.expSelection 
)

fn RSTA_syncPresetFolder expressionPresetPath = 
(
	filesToSync =  getFilesRecursive expressionPresetPath "*.*"
		
	WWP4vSync filesToSync
	
-- 	pathDirs = getDirectories
)

fn RSTA_SaveExpressionFile driverNode receiverNode = 
(
	global missingDriverObjects = #() --used to store missing objects required in the expressions
	global exprXmlFile = undefined -- used to store the name of the file the expressions will be saved to
	global expressionObjects = #() --used to store which objects are epression driven
	global ancilliaryLookAtNodes = #() --used to store look at targets / up vector nodes
	global caDriverObjects = #()
	global final_expressionData = #()--used to store all expression data
	global final_springData = #()--used to store all spring data
	global final_lookAtData = #() --used to store all lookAtData
	global final_customAttrData = #() --used to store all custom attribute info
	global controllersToZero = #() --used to store all expression controllers. We use this to zero their weights on export and then set back to 100%
	
	local exprSaveFile = undefined
	if exprSaveFile == undefined then
	(
		local exprSaveFile = getSaveFileName caption:"ExpressionXML File" types:"XML Data (*.xml)|*.xml|All Files (*.*)|*.*|" filename:expressionPresetPath
	)	
	
	local initialSelection = selection as array
	select receiverNode
	
	RSTA_SaveExpressions exprSaveFile driverNode receiverNode  
	
	RSTA_addEditFileInPerforce exprSaveFile
	
	select initialSelection
)

fn RSTA_LoadExpressionFile preserveExistingExpressionsVal = 
(
	global missingDriverObjects = #() --used to store missing objects required in the expressions
	global exprXmlFile = undefined -- used to store the name of the file the expressions will be saved to
	global expressionObjects = #() --used to store which objects are epression driven
	global ancilliaryLookAtNodes = #() --used to store look at targets / up vector nodes
	global caDriverObjects = #()
	global final_expressionData = #()--used to store all expression data
	global final_springData = #()--used to store all spring data
	global final_lookAtData = #() --used to store all lookAtData
	global final_customAttrData = #() --used to store all custom attribute info
	global controllersToZero = #() --used to store all expression controllers. We use this to zero their weights on export and then set back to 100%		

	local forceDeleteSprings = false
	local preserveExistingBonesVal = true
	local loadAsThreeLateral = false
	
	global forcePresetPath = expressionPresetPath
	
	RSTA_LoadExpressions expressionPresetPath forceDeleteSprings preserveExistingExpressionsVal preserveExistingBonesVal loadAsThreeLateral	
)

fn RSTA_JoystickCreateBox = 
(
	if textInput_UI != undefined then destroyDialog textInput_UI
	
	rollout textInput_UI "Node Name" --this is the main rollout that contains all the tabs and sub rollouts.
	(
		local jstylePick = undefined
		editText txtFriendlyExpressionText "Name:" text:"" pos:[5,10] width: 180 height:25
		-- style (Square =1, Vertical=2, Horizontal=3)
		dropDownList ddlJsType "Joystick Type" pos:[5,(txtFriendlyExpressionText.height+12)] width:180 height:25 items:#("Square", "Vertical", "Horizontal")
		
		button btn_AcceptValue "OK" pos:[5,(ddlJsType.pos[2] + 30)] width:85
		button btn_Cancel "Cancel" pos:[(btn_AcceptValue.pos[1]+75 + 20),btn_AcceptValue.pos[2]] width:85
		
		on textInput_UI open do 
		(
			jStyle = 1
		)
		
		on textInput_UI.txtFriendlyExpressionText changed txt do
		(
			textInputValue = txt
		)
		
		on ddlJsType selected i do
		(		
			jstylePick = ddlJsType.items[i]
			jstyle = i 
			format "You selected '%'!\n" ddlJsType.items[i]
		)			
		
		on btn_AcceptValue pressed do 
		(
			textInputValue = txtFriendlyExpressionText.text
			format ("textInputValue: "+(textInputValue as string)+"\n")
			
			jsn = textInputValue
			jstyle = ddlJsType.selection			
			jPos = [0,0,0]			
			scaleIt = [1,1,1]
			
			if jsn != undefined do 
			(		
				format ("driverOrReceiver: "+(driverOrReceiver as string)+"\n")
				
				if driverOrReceiver == 1 then
				(
					local drv = RSTA_createJoystick jsn jstyle jpos scaleit
					::subRollNodePick.driver = drv
				
-- 					::subRollNodePick.driverName = jsn
					--subRollNodePick.driverP1 = subRollNodePick.driver.Name
					--subRollNodePick.driverP2 = "undefined"
					
-- 					::subRollExpressionText.txtFriendlyExpressionText.text = (subRollNodePick.driverP1+"_"+subRollNodePick.driverP2)
					RSTA_enableExprText()
					subRollNodePick.txtDriverName.text = jsn
				)
				else
				(
					subRollNodePick.receiver = RSTA_createJoystick jsn jstyle jpos scaleit
				
					subRollNodePick.txtReceiverName.text = subRollNodePick.receiver.name					
				)
				
				driverOrReceiver = undefined
				destroyDialog textInput_UI
			)
		)			
		
		on btn_Cancel pressed do 
		(
			textInputValue = undefined
			format ("textInputValue: "+(textInputValue as string)+"\n")
			driverOrReceiver = undefined
			destroyDialog textInput_UI
		)
		
		on textInput_UI close do 
		(
			return textInputValue
		)
	)
	createDialog textInput_UI width:200 height:120 style:#(#style_titlebar, #style_border, #style_sysmenu)
)

fn RSTA_freezeSpecificControl receiverTransType receiver = 
(
	if toLower(receiverTransType) == toLower("Position.controller") do
	(
		format ("Froze position on "+receiver.name+"\n")
		-- freeze position
		receiver.position.controller = Bezier_Position() 			
		receiver.position.controller = position_list() 			
		receiver.position.controller.available.controller = Position_XYZ() 	

		/* "Localization on" */  
				
		receiver.position.controller.setname 1 "Frozen Position" 	
		receiver.position.controller.setname 2 "Zero Pos XYZ" 			
		
		/* "Localization off" */  
		
		receiver.position.controller.SetActive 2 		

		-- position to zero
		receiver.Position.controller[2].x_Position = 0
		receiver.Position.controller[2].y_Position = 0
		receiver.Position.controller[2].z_Position = 0
	)
	if toLower(receiverTransType) == toLower("Rotation.controller") do
	(
		format ("Froze Rotation on "+receiver.name+"\n")
		-- freeze rotation		
		receiver.rotation.controller = Euler_Xyz() 		
		receiver.rotation.controller = Rotation_list() 			
		receiver.rotation.controller.available.controller = Euler_xyz() 		
		
		/* "Localization on" */  
	
		receiver.rotation.controller.setname 1 "Frozen Rotation" 		
		receiver.rotation.controller.setname 2 "Zero Euler XYZ" 		
	
		/* "Localization off" */  
		
		receiver.rotation.controller.SetActive 2 	
	)	
	if toLower(receiverTransType) == toLower("Scale.controller") do
	(
		format ("Froze Scale on "+receiver.name+"\n")
		-- freeze scale
		receiver.scale.controller = Bezier_Scale() 			
		receiver.scale.controller = scale_list() 			
		receiver.scale.controller.available.controller = ScaleXYZ() 	
				
		receiver.scale.controller.setname 1 "Frozen Scale" 	
		receiver.scale.controller.setname 2 "Zero Scale XYZ" 			
		
		receiver.scale.controller.SetActive 2 		

		-- scale to zero
		receiver.scale.controller[2].x_Scale = 100
		receiver.scale.controller[2].y_Scale = 100
		receiver.scale.controller[2].z_Scale = 100
	)			
)

fn RSTA_addLookAt_ExpressionCreate driver receiver preserveContVal=
(
	print ("Tring to add lookat to "+receiver.name+" with "+driver.name+"as target")
	
	if preserveContVal != true then
	(
		format "Not preserving controllers\n"
		
		local receiverTransType  = "Rotation.controller"
		RSTA_freezeSpecificControl receiverTransType receiver
	)
	else
	(
		format "Preserving controllers\n"
	)		
	
	local newLookat = receiver.rotation.controller.Available.controller = LookAt_Constraint ()
	newLookAt.appendTarget driver 50	
)

fn RSTA_addExpression_ExpressionCreate driver driverAxis receiver receiverAxis expressionText preserveContVal =
(				
	local newfcStr = undefined
	local driverTrans = undefined
	local driverAxisValue = undefined
	local receiverTrans = undefined
	local receiverTransValue = undefined
	local SVN = undefined
	local fjString = undefined
	local fjStr = undefined
		
	if (substring driverAxis 3 5) == "Trans" then
	(
		driverTransType = "position.controller"
	)
	else
	(
		if (substring driverAxis 3 3) == "Rot" then
		(
			driverTransType = "rotation.controller"
		)
		else
		(
			driverTransType = "scale.controller"
		)
	)
	
	if (substring receiverAxis 3 3) == "Rot" then
	(
		receiverTransType = "rotation.controller"
	)
	else
	(
		if (substring receiverAxis 3 3) == "Tra" then
		(
			receiverTransType = "position.controller"
		)
		else
		(
			receiverTransType = "scale.controller"
		)
	)
	
	if preserveContVal != true then
	(
		format "Not preserving controllers\n"
		
		format ("receiverTransType: "+receiverTransType+"\n")
		RSTA_freezeSpecificControl receiverTransType receiver
-- 		if toLower(receiverTransType) == toLower("Position.controller") do
-- 		(
-- 			-- freeze position
-- 			receiver.position.controller = Bezier_Position() 			
-- 			receiver.position.controller = position_list() 			
-- 			receiver.position.controller.available.controller = Position_XYZ() 	

-- 			/* "Localization on" */  
-- 					
-- 			receiver.position.controller.setname 1 "Frozen Position" 	
-- 			receiver.position.controller.setname 2 "Zero Pos XYZ" 			
-- 			
-- 			/* "Localization off" */  
-- 			
-- 			receiver.position.controller.SetActive 2 		

-- 			-- position to zero
-- 			receiver.Position.controller[2].x_Position = 0
-- 			receiver.Position.controller[2].y_Position = 0
-- 			receiver.Position.controller[2].z_Position = 0
-- 		)
-- 		if toLower(receiverTransType) == toLower("Rotation.controller") do
-- 		(
-- 			-- freeze rotation		
-- 			receiver.rotation.controller = Euler_Xyz() 		
-- 			receiver.rotation.controller = Rotation_list() 			
-- 			receiver.rotation.controller.available.controller = Euler_xyz() 		
-- 			
-- 			/* "Localization on" */  
-- 		
-- 			receiver.rotation.controller.setname 1 "Frozen Rotation" 		
-- 			receiver.rotation.controller.setname 2 "Zero Euler XYZ" 		
-- 		
-- 			/* "Localization off" */  
-- 			
-- 			receiver.rotation.controller.SetActive 2 	
-- 		)	
-- 		if toLower(receiverTransType) == toLower("Scale.controller") do
-- 		(
-- 			-- freeze scale
-- 			receiver.scale.controller = Bezier_Scale() 			
-- 			receiver.scale.controller = scale_list() 			
-- 			receiver.scale.controller.available.controller = ScaleXYZ() 	
-- 					
-- 			receiver.scale.controller.setname 1 "Frozen Scale" 	
-- 			receiver.scale.controller.setname 2 "Zero Scale XYZ" 			
-- 			
-- 			receiver.scale.controller.SetActive 2 		

-- 			-- scale to zero
-- 			receiver.scale.controller[2].x_Scale = 100
-- 			receiver.scale.controller[2].y_Scale = 100
-- 			receiver.scale.controller[2].z_Scale = 100
-- 		)				
	)
	else
	(
		format "Preserving controllers\n"
	)				
	
	driverTransValue = (substring driverAxis 1 1)
	receiverTransValue = (substring receiverAxis 1 1)

	if driverTransType == "rotation.controller" do
	(
		if (driver.rotation.controller as string) != "Controller:Euler_XYZ" then
		(				
			controllerNumberdriver = driver.rotation.controller.count
		)
		else
		(
			controllerNumberdriver = "notList"
		)
	)
	if driverTransType == "position.controller" do
	(
		if (driver.position.controller as string) != "Controller:Position_XYZ" then
		(
			controllerNumberdriver = driver.position.controller.count				
		)
		else
		(
			controllerNumberdriver = "notList"
		)
	)
	if driverTransType == "scale.controller" do
	(
		if (driver.scale.controller as string) != "Controller:ScaleXYZ" then
		(
			controllerNumberdriver = driver.scale.controller.count				
		)
		else
		(
			controllerNumberdriver = "notList"
		)
	)
	
	if receiverTransType == "rotation.controller" do
	(
		if (receiver.rotation.controller as string) != "Controller:Euler_XYZ" then
		(	
			receiver.rotation.controller.Available.controller = Euler_XYZ () --add a new euler controller onto the object so we can add a float expression
			controllerNumberReceiver = receiver.rotation.controller.count
		)
		else
		(
			controllerNumberReceiver = "notList"
		)
	)
	if receiverTransType == "position.controller" do
	(
		if (receiver.position.controller as string) != "Controller:Position_XYZ" then
		(
			receiver.position.controller.Available.controller = Position_XYZ () --add a new pos controller onto the object so we can add a float expression
			controllerNumberReceiver = receiver.position.controller.count				
		)
		else
		(
			controllerNumberReceiver = "notList"
		)
	)
	if receiverTransType == "scale.controller" do
	(
		if (receiver.scale.controller as string) != "Controller:ScaleXYZ" then
		(
			receiver.scale.controller.Available.controller = ScaleXYZ () --add a new pos controller onto the object so we can add a float expression
			controllerNumberReceiver = receiver.scale.controller.count				
		)
		else
		(
			controllerNumberReceiver = "notList"
		)
	)

	if controllerNumberReceiver != "notList" then
	(
		fcStr = ("$"+receiver.name+"."+receiverTransType+"["+(controllerNumberReceiver as string)+"].controller."+receiverTransValue+"_"+receiverTransType+" = Float_Expression ()")
			execute fcStr
		fcStr = ("$"+receiver.name+"."+receiverTransType+"["+(controllerNumberReceiver as string)+"].controller."+receiverTransValue+"_"+receiverTransType)
		newfcStr = execute fcStr
	)
	else
	(
		fcStr = ("$"+receiver.name+"."+receiverTransType+"."+receiverTransValue+"_"+receiverTransType+" = Float_Expression ()")
			execute fcStr
		fcStr = ("$"+receiver.name+"."+receiverTransType+"."+receiverTransValue+"_"+receiverTransType)
		newfcStr = execute fcStr
	)
				
	if controllerNumberDriver != "notList" then
	(				
		driverString = ("$"+driver.name+"."+driverTransType+"["+(controllerNumberDriver as string)+"].controller."+driverTransValue+"_"+driverTransType)
			driverStr = execute driverString
	)
	else
	(
		driverString = ("$"+driver.name+"."+driverTransType+"."+driverTransValue+"_"+driverTransType)
			driverStr = execute driverString
	)

	SVN = (driver.name+ "_"+driverAxis)
	format ("SVN: "+SVN+"\n")
	
	newfcStr.AddScalarTarget SVN driverStr --add scalar pointing to the translation of the joystick object

	newfcStr.SetExpression (ExpressionText)

	format ("Expression Created on "+receiver.name+"'s "+receiverTransValue+" "+receiverTransType+" driven by "+driver.name+"'s "+driverTransValue+" "+driverTransType+"\n")
)

fn RSTA_populateSpringDataArray = 
(
	struct springData (holderObj, transType, strengthX, strengthY, strengthZ, dampenX, dampenY, dampenZ, /**linkParams,**/ xLimitMin, yLimitMin, zLimitMin, xLimitMax, yLimitMax, zLimitMax, preserveXFormsOnFreeze, gravityX, gravityY, gravityZ, gravity_axisX, gravity_axisY, gravity_axisZ)
	
	local thisData = springData()

	local attrVal = undefined
	
	attrVal = ::subRollExpressionType.spinSprStrength.value
	setProperty thisData #strengthX attrVal
	attrVal = ::subRollExpressionType.spinSprStrength.value
	setProperty thisData #strengthY attrVal
	attrVal = ::subRollExpressionType.spinSprStrength.value
	setProperty thisData #strengthZ attrVal
	attrVal = ::subRollExpressionType.spinSprDampening.value
	setProperty thisData #dampenX attrVal
	attrVal = ::subRollExpressionType.spinSprDampening.value
	setProperty thisData #dampenY attrVal
	attrVal = ::subRollExpressionType.spinSprDampening.value
	setProperty thisData #dampenZ attrVal
-- 	attrVal = thisChildNode.getAttribute "value"
-- 	setProperty thisData #linkParams attrVal

	attrVal = ::subRollExpressionType.spinSprMinX.value
	setProperty thisData #xLimitMin attrVal

	attrVal = ::subRollExpressionType.spinSprMinY.value
	setProperty thisData #yLimitMin attrVal

	attrVal = ::subRollExpressionType.spinSprMinZ.value
	setProperty thisData #zLimitMin attrVal
	attrVal = ::subRollExpressionType.spinSprMaxX.value
	setProperty thisData #xLimitMax attrVal
	attrVal = ::subRollExpressionType.spinSprMaxY.value
	setProperty thisData #yLimitMax attrVal
	attrVal = ::subRollExpressionType.spinSprMaxZ.value
	setProperty thisData #zLimitMax attrVal
	
	attrVal = ::subRollExpressionType.chk_KeepXFormsOnFreeze.state
	setProperty thisData #preserveXFormsOnFreeze attrVal

	attrVal = ::subRollExpressionType.spinSprGravX.value
	setProperty thisData #gravityX attrVal

	attrVal = ::subRollExpressionType.spinSprGravY.value
	setProperty thisData #gravityY attrVal
	attrVal = ::subRollExpressionType.spinSprGravZ.value
	setProperty thisData #gravityZ attrVal
	attrVal = ::subRollExpressionType.spinGravAxisX.value
	setProperty thisData #gravity_axisX attrVal
	attrVal = ::subRollExpressionType.spinGravAxisY.value
	setProperty thisData #gravity_axisY attrVal
	attrVal = ::subRollExpressionType.spinGravAxisZ.value
	setProperty thisData #gravity_axisZ attrVal

-- 	append springDataArray thisData		
	
	return thisData
)
