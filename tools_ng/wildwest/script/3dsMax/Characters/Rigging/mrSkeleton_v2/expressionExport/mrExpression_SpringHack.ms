/**
THIS IS A FILE OF HACK EXPRESSIONS TO GET AROUND SPRING CONTROLLER CRASHES UNTIL TOOLS FIX THE ISSUE
**/

fn RSTA_posXYZ_Spring obj = 
(
	if classof obj.position.controller as string == RsSpring do
	(
		obj.position.controller = Position_XYZ()
-- 		format ("Set "+(obj.position.controller as string)+" to Position_XYZ\n")
	)
)

fn RSTA_posList_Spring obj = 
(
-- 	for i = 2 to obj.position.controller.count do --skip 1st controller as thats the frozen one
	for i = 1 to obj.position.controller.count do 
	(
		if classof obj.position.controller[i].controller == RsSpring do
		(
			obj.position.controller[i].controller = Position_XYZ()
-- 			format ("Set "+(obj.position.controller[i].controller as string)+" to Position_XYZ\n")
		)
	)	
)

fn RSTA_rotEulerXYZ_Spring obj = 
(
	/**
	FUNCTION TO TEST IF obj HAS A STANDARD EULER XYZ
	ROTATION CONTROLLER AND THEN SEND THAT TO BE PARSED
	**/
	
	if classof obj.rotation.controller == RsSpringRotationController do
	(
		obj.rotation.controller = Euler_XYZ()
		format ("Set "+(obj.rotation.controller as string)+" to Euler_XYZ\n")
	)
)

fn RSTA_rotList_Spring obj = 
(
	/**
	FUNCTION TO TEST IF obj HAS A ROTATION LIST
	ROTATION CONTROLLER AND THEN SEND THAT TO BE PARSED
	**/	
	
	for i = 1 to obj.rotation.controller.count do --skip 1st controller as thats the frozen one
	(
		if classof obj.rotation.controller[i].controller == RsSpringRotationController do
		(
			obj.rotation.controller[i].controller = Euler_XYZ()
			format ("Set "+(obj.rotation.controller[i].controller as string)+" to Euler_XYZ\n")
		)	
	)		
)

fn RSTA_sclXYZ_Spring obj = 
(
	/**
	FUNCTION TO TEST IF obj HAS A STANDARD SCALE XYZ
	CONTROLLER AND THEN SEND THAT TO BE PARSED
	**/
	
	if classof obj.scale.controller == RsSpring do
	(
		obj.scale.controller = ScaleXYZ()
		format ("Set "+(obj.scale.controller as string)+" to ScaleXYZ\n")
	)
)

fn RSTA_sclList_Spring obj = 
(
	/**
	FUNCTION TO TEST IF obj HAS A SCALE LIST
	CONTROLLER AND THEN SEND THAT TO BE PARSED
	**/
	
	for i = 1 to obj.scale.controller.count do --skip 1st controller as thats the frozen one
	(

		if classof obj.scale.controller[i].controller == RsSpring do
		(
			obj.scale.controller[i].controller = ScaleXYZ()
			format ("Set "+(obj.scale.controller[i].controller as string)+" to ScaleXYZ\n")
		)
	)		
)

fn RSTA_queryControllerTypeForSprings obj = 
(
	----format ("Querying controllers on "+obj.name+"\n")
	
	if classof obj.position.controller == position_list then
	(
		RSTA_posList_Spring obj
	)
	else
	(
		if classof obj.position.controller == Position_XYZ do
		(
			RSTA_posXYZ_Spring obj
		)
	)
	
	if classof obj.rotation.controller == rotation_list then
	(
		----format ("Rotation List found on "+obj.name+"\n")
		RSTA_rotList_Spring obj
	)
	else
	(
		if classof obj.rotation.controller == Euler_XYZ do
		(
			----format ("Euler XYZ found on "+obj.name+"\n")
			RSTA_rotEulerXYZ_Spring obj
		)
	)

	if classof obj.scale.controller == scale_list then
	(
-- 		debugPrint ("Scale List found on "+obj.name)
		RSTA_sclList_Spring obj
	)
	else
	(
		if classof obj.scale.controller == ScaleXYZ do
		(
-- 			debugPrint ("Scale XYZ found on "+obj.name)
			RSTA_sclXYZ_Spring obj
		)
	)
)