/*
Freeze Transform MacroScript File



Author :   Frederick Ruff

Revision History:
	
	Dec 1 2000, created
	
	Aug 22 2003, Larry Minton
	
	12 dec 2003, Pierre-Felix Breton, 
		added product switcher: this macroscript file can be shared with all Discreet products
    08 mar 2006, Michael Zyracki
	    added support for Animation Layers, by disabling  Freezes over layers, and making sure zero
		keys are set on active layers.

 
This script adds tools for freezing a transform
This is done via adding a second controller to the controllers stack.
--***********************************************************************************************
-- MODIFY THIS AT YOUR OWN RISK
*/
macroScript FreezeTransform
enabledIn:#("max", "viz") --pfb: 2003.12.12 added product switch
	ButtonText:~FREEZETRANSFORM_BUTTONTEXT~  	
	Category:~FREEZETRANSFORM_CATEGORY~  	
	internalCategory:"Animation Tools"  	
	Tooltip:~FREEZETRANSFORM_TOOLTIP~  
( 	
	fn FreezeTransform = 	--freezes transforms. Copied from the max macroscript as this does not like being run from other scripts.
	( 		
		local Obj = Selection as array 	
		suspendEditing which:#motion
		for i = 1 to Obj.count do 		
		( 
			Try
			(	
				local CurObj = Obj[i] 	
	
				if classof CurObj.rotation.controller != Rotation_Layer do
				(

					-- freeze rotation		
					CurObj.rotation.controller = Euler_Xyz() 		
					CurObj.rotation.controller = Rotation_list() 			
					CurObj.rotation.controller.available.controller = Euler_xyz() 		
					
					/* "Localization on" */  
				
					CurObj.rotation.controller.setname 1 "Frozen Rotation" 		
					CurObj.rotation.controller.setname 2 "Zero Euler XYZ" 		
				
					/* "Localization off" */  
					
					CurObj.rotation.controller.SetActive 2 		
				)
				if classof CurObj.position.controller != Position_Layer do
				(

					-- freeze position
					CurObj.position.controller = Bezier_Position() 			
					CurObj.position.controller = position_list() 			
					CurObj.position.controller.available.controller = Position_XYZ() 	
		
					/* "Localization on" */  
							
					CurObj.position.controller.setname 1 "Frozen Position" 	
					CurObj.position.controller.setname 2 "Zero Pos XYZ" 			
					
					/* "Localization off" */  
					
					CurObj.position.controller.SetActive 2 		
		
					-- position to zero
					CurObj.Position.controller[2].x_Position = 0
					CurObj.Position.controller[2].y_Position = 0
					CurObj.Position.controller[2].z_Position = 0
				)
				if classof CurObj.scale.controller != Scale_Layer do
				(

					-- freeze scale
					CurObj.scale.controller = Bezier_Scale() 			
					CurObj.scale.controller = scale_list() 			
					CurObj.scale.controller.available.controller = ScaleXYZ() 	
		
					/* "Localization on" */  
							
					CurObj.scale.controller.setname 1 "Frozen Scale" 	
					CurObj.scale.controller.setname 2 "Zero Scale XYZ" 			
					
					/* "Localization off" */  
					
					CurObj.scale.controller.SetActive 2 		
		
					-- scale to zero
					CurObj.scale.controller[2].x_Scale = 100
					CurObj.scale.controller[2].y_Scale = 100
					CurObj.scale.controller[2].z_Scale = 100
				)				
				
			)	
			/* "Localization on" */  
					
			Catch( messagebox "A failure occurred while freezing an object's transform." title:"Freeze Transform")
					
			/* "Localization off" */  	
		)
		resumeEditing which:#motion
	)
	
	/* "Localization on" */  
	
	if querybox ~QUERYBOX_FREEZING_TRANSFORM_INVOLVES~ title:~FREEZE_TRANSFORMS_TITLE~ == true do 
	
	/* "Localization off" */
  
	FreezeTransform()

 )
 -- Set Transform to Zero
MacroScript TransformToZero
enabledIn:#("max", "viz") --pfb: 2003.12.12 added product switch
	ButtonText:~TRANSFORMTOZERO_BUTTONTEXT~ 
	Category:~TRANSFORMTOZERO_CATEGORY~ 
	internalCategory:"Animation Tools" 
	Tooltip:~TRANSFORMTOZERO_TOOLTIP~ 
(
	fn TransformToZero =
	(
	
		local Obj = Selection as array
		for i = 1 to Obj.count do
		(
			Try
			(
				local CurObj = Obj[i]
				if classof CurObj.Position.controller == Position_Layer then
				(
					local active = CurObj.position.controller.active
					CurObj.Position.controller[active].value = (Point3 0 0 0)
				)
				else
				(
					CurObj.Position.controller[2].x_Position = 0
					CurObj.Position.controller[2].y_Position = 0
					CurObj.Position.controller[2].z_Position = 0
				)

				if classof CurObj.rotation.controller == Rotation_Layer then
				(
					local active = CurObj.rotation.controller.active
				  	CurObj.rotation.controller[active].value = (quat 0 0 0 1)
				)
				else
				(
					CurObj.rotation.controller[2].x_rotation = 0
					CurObj.rotation.controller[2].y_rotation = 0
					CurObj.rotation.controller[2].z_rotation = 0
				)	
			)
			/* "Localization on" */  
			
			Catch( messagebox ~MSGBOX_ONE_OBJ_TRANSFORM_NEVER_FROZEN~ title:~MSGBOX_ONE_OBJ_TRANSFORM_NEVER_FROZEN_TITLE~)
			
			/* "Localization off" */  	
		)
		select Obj
	)

	TransformToZero()
)
 -- Freeze Rotation Only
 macroScript FreezeRotation
enabledIn:#("max", "viz") --pfb: 2003.12.12 added product switch
	ButtonText:~FREEZEROTATION_BUTTONTEXT~  	
	Category:~FREEZEROTATION_CATEGORY~  	
	internalCategory:"Animation Tools"  	
	Tooltip:~FREEZEROTATION_TOOLTIP~  
( 	
	fn FreezeRotation = 	
	( 		
		local Obj = Selection as array 		
		for i = 1 to Obj.count do 		
		( 	
			Try
			(		
				local CurObj = Obj[i] 	
				if classof CurObj.rotation.controller != Rotation_Layer do
				(
			
					CurObj.rotation.controller = Euler_Xyz() 		
					CurObj.rotation.controller = Rotation_list() 		
					
					/* "Localization on" */
		  
					CurObj.rotation.controller.setname 1 ~CUROBJ_INIT_INITIAL_POSE~ 		
					CurObj.rotation.controller.available.controller = Euler_xyz() 		
					CurObj.rotation.controller.setname 1 ~CUROBJ_ROTATION_INIT_INITIAL_POSE~ 		
					CurObj.rotation.controller.setname 2 ~CUROBJ_KEYFRAME_XYZ~ 		
					CurObj.rotation.controller.SetActive 2 		
				)
				
				/* "Localization off" */  
			)
			
			/* "Localization on" */  
			
			Catch( messagebox ~MSGBOX_FAILURE_OCCURED_WHILE_FREEZING_AN_OBJ_ROTATION~ title:~MSGBOX_FAILURE_OCCURED_WHILE_FREEZING_AN_OBJ_ROTATION_TITLE~)
			
			/* "Localization off" */  	
		) 		
		select Obj 
	) 
	
	FreezeRotation()
 )
 
 -- Freeze Position only
macroScript FreezePosition
enabledIn:#("max", "viz") --pfb: 2003.12.12 added product switch
	ButtonText:~FREEZEPOSITION_BUTTONTEXT~  	
	Category:~FREEZEPOSITION_CATEGORY~  	
	internalCategory:"Animation Tools"  	
	Tooltip:~FREEZEPOSITION_TOOLTIP~  
( 	
	fn FreezePosition = 	
	( 		
		local Obj = Selection as array 		
		for i = 1 to Obj.count do 		
		(
			Try
			(
				local CurObj = Obj[i] 	

				if classof CurObj.Position.controller !=  Position_Layer do
				(
	
					-- freeze position		
					CurObj.position.controller = Bezier_Position() 			
					CurObj.position.controller = position_list() 			
					
					/* "Localization on" */
		  
					CurObj.position.controller.setname 1 ~CUROBJ_POSITION_CONTROLLER_INITIAL_POSE~ 			
					CurObj.position.controller.available.controller = Position_XYZ() 						
					CurObj.position.controller.setname 2 ~CUROBJ_POSITION_CONTROLLER_KEYFRAME_XYZ~ 			
					CurObj.position.controller.SetActive 2 		
				
					/* "Localization off" */  	
					
					-- position to zero
					CurObj.Position.controller[2].x_Position = 0
					CurObj.Position.controller[2].y_Position = 0
					CurObj.Position.controller[2].z_Position = 0
				)
			)
			
			/* "Localization on" */  
			
			Catch( messagebox ~MSGBOX_FAILURE_OCCURED_WHILE_FREEZING_OBJ_POSITION~ title:~MSGBOX_FAILURE_OCCURED_WHILE_FREEZING_OBJ_POSITION_TITLE~)
			
			/* "Localization off" */  	
		) 		
		select Obj 	
	) 
	
	FreezePosition()
) 

--Set Rotation to Zero
MacroScript RotationToZero
enabledIn:#("max", "viz") --pfb: 2003.12.12 added product switch
	ButtonText:~ROTATIONTOZERO_BUTTONTEXT~ 
	Category:~ROTATIONTOZERO_CATEGORY~ 
	internalCategory:"Animation Tools" 
	Tooltip:~ROTATIONTOZERO_TOOLTIP~ 
(
	fn RotationToZero =
	(
		local Obj = Selection as array
		for i = 1 to Obj.count do
		(
			Try
			(
				local CurObj = Obj[i]
				if classof CurObj.rotation.controller == Rotation_Layer then
				(
					local active = CurObj.rotation.controller.active
				  	CurObj.rotation.controller[active].value = (quat 0 0 0 1)
				)
				else
				(
					CurObj.rotation.controller[2].x_rotation = 0
					CurObj.rotation.controller[2].y_rotation = 0
					CurObj.rotation.controller[2].z_rotation = 0
				)	
			)
			
			/* "Localization on" */  
	
			Catch( messagebox ~MSGBOX_ONE_OF_THE_OBJ_ROTATIONS_NEVER_FROZEN~ title:~MSGBOX_ONE_OF_THE_OBJ_ROTATIONS_NEVER_FROZEN_TITLE~)
		
			/* "Localization off" */  
		)
		select Obj
	)
	
	RotationToZero()
)

-- Position To Zero
MacroScript PositionToZero
enabledIn:#("max", "viz") --pfb: 2003.12.12 added product switch
	ButtonText:~POSITIONTOZERO_BUTTONTEXT~ 
	Category:~POSITIONTOZERO_CATEGORY~ 
	internalCategory:"Animation Tools" 
	Tooltip:~POSITIONTOZERO_TOOLTIP~ 
(
	fn PositionToZero =
	(
	
		local Obj = Selection as array
		for i = 1 to Obj.count do
		(
			Try
			(
				local CurObj = Obj[i]
				if classof CurObj.Position.controller == Position_Layer then
				(
					local active = CurObj.position.controller.active
					CurObj.Position.controller[active].value = (Point3 0 0 0)
				)
				else
				(
					CurObj.Position.controller[2].x_Position = 0
					CurObj.Position.controller[2].y_Position = 0
					CurObj.Position.controller[2].z_Position = 0
				)
			)
			/* "Localization on" */  
			
			Catch( messagebox ~MSGBOX_OBJ_POSITIONS_NEVER_FROZEN~ title:~MSGBOX_OBJ_POSITIONS_NEVER_FROZEN_TITLE~)
			
			/* "Localization off" */  	
		)
		select Obj
	)

	PositionToZero()
)



