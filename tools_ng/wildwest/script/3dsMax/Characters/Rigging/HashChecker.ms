

-- hashCheck.ms
-- Rockstar North
-- Matt Rennie Nov 2010
--Tool to display hashId of current bone
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

fn generateHash hashType =
(
	selArray = selection as array
	if selArray.count == 1 then
	(
		local val = undefined
-- 		local tagToCheck = getUserPropBuffer $.name
		local tagToCheck = $.name
		if hashType == 1 do
		(
			val = atHash16 tagToCheck 
-- 			print ($.name+" = hash "+(val as string))
-- 			messagebox ($.name+" = hash "+(val as string))
		)
		if hashType == 2 do
		(
			val = atHash16U tagToCheck 
-- 			print ($.name+" = hash "+(val as string))
-- 			messagebox ($.name+" = hash "+(val as string))
		)
		if hashType == 3 do
		(
			val = atStringHash tagToCheck 
-- 			print ($.name+" = hash "+(val as string))
-- 			messagebox ($.name+" = hash "+(val as string))
		)
			print ($.name+" = hash "+(val as string))
			msgTitle = undefined 	
				if hashType == 1 then
				(
					msgTitle = "Generated using atHash16"
				)
				else
				(
					if hashType == 2 then
					(
						msgTitle = "Generated using atHash16U"
					)
					else
					(
						msgTitle = "Generated using atStringHash"
					)
				)
			messagebox ($.name+" = hash "+(val as string)) title:msgTitle
	)
	else
	(
		messagebox "Warning! Please pick one node and try again" beep:true
	)
)



global hashType = undefined
global hashGenRoll


if ((hashGenRoll != undefined) and (hashGenRoll.isDisplayed)) do
	(destroyDialog hashGenRoll)

-------------------------------------------------------------------------------------------------------------------------	

rollout hashGenRoll "HaSh-ish" 
(
	
	checkBox chkHash1 "atHash16" checked:true 
	checkBox chkHash2 "atHash16U" checked:false 
	checkBox chkHash3 "atStringHash" checked:false 

	button btnGetHash "Get Hash" width:120 height:25
	
	on hashGenRoll open do
	(
		hashType = 1
		print "Hash check defaulting to atHash16."
	)
	
	on chkHash1 changed theState do
	(
		if chkHash1.state == true do
		(
			hashType  = 1
			chkHash2.state = false
			chkHash3.state = false
		)
	)

	on chkHash2 changed theState do
	(
		if chkHash2.state == true do
		(
			hashType  = 2
			chkHash1.state = false
			chkHash3.state = false
		)
	)
	
	on chkHash3 changed theState do
	(
		if chkHash3.state == true do
		(
			hashType  = 3
			chkHash1.state = false
			chkHash2.state = false
		)
	)		
	
	
	on btnGetHash pressed do
	(
		if chkHash1.state == true then
		(
			hashType = 1
			print "Checking hash using atHash16"
			generateHash hashType
		)
		else
		(
			if chkHash2.state == true then
			(
				hashType = 2
				print "Checking hash using atHash16U"
				generateHash hashType
			)
			else
			(
				if chkHash3.state ==true then
				(
					hashType = 3
					print "Checking hash using atStringHash"
					generateHash hashType
				)
				else
				(
					messagebox "WARNING! Couldn't figure hashtype" beep:true
				)
			)
		)
	)

)
	
	
CreateDialog hashGenRoll width:125 pos:[1450, 100] 