---- written by Nicolas Malovec 
-- Swap your weights from bones to bones.


	struct BoneSwapper
	(
		MyObject = selection[1],
		
		--Bone Swapper variables
		
		MySelectedFromBone =#(),
		MySelectedToBone =#(),
		
		MySelectedToBoneIndex,
		MySelectedFromBoneIndex,
		AllSwappVerts,
		AllSelectedSwappVerts,
		totalBonesAfter,
		NewBonesAdded=#(),
		
		--used to know wether or not the buttons from and to have already been pressed
		btn_FromTrigger1 = false,
		btn_FromTrigger2 = false,
		btn_FromTrigger3 = false,
		btn_FromTrigger4 = false,
		btn_FromTrigger5 = false,
		btn_FromTrigger6 = false,
		btn_FromTrigger7 = false,
		btn_FromTrigger8 = false,
		btn_FromTrigger9 = false,
		btn_FromTrigger10 = false,
		
		btn_ToTrigger1 = false,
		btn_ToTrigger2 = false,
		btn_ToTrigger3 = false,
		btn_ToTrigger4 = false,
		btn_ToTrigger5 = false,
		btn_ToTrigger6 = false,
		btn_ToTrigger7 = false,
		btn_ToTrigger8 = false,
		btn_ToTrigger9 = false,
		btn_ToTrigger10 = false,


		--////////////////////////////////////////////////////////////////////////////////////////////////////////////
		--FUNCTIONS//////////////////////////////////////////////////////////////////////////////////////////
		--////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		------ Get the index of my selected vertex in skin mode
		fn FN_GetVertSelect =
		(
			
			skinMod = modPanel.getCurrentObject()
			numVerts = skinOps.getNumberVertices skinMod
			vertsArr = #()
			for i = 1 to numVerts do
			(
				if (skinops.isVertexSelected skinMod i) == 1 then
				(
					append vertsArr i
				)
				
			)
			vertsArr
		),
		

		fn FN_BoneSwapper AllSwappVerts MySelectedFromBoneIndex MySelectedToBoneIndex = 
			(				
				-- get the number of verts skined in the whole selected mesh
	 			local AllSwappVerts = skinOps.GetNumberVertices MyObject.modifiers[#skin]	
				--For each of those vert find how many bones influences them
				For vert = 1 to AllSwappVerts do
				(		
					--Empty Arrays
					local BoneArray = #()
					local WeightArray = #()				
					-- Get number of bones per verts	
					local MyBonesPerVert = skinOps.GetVertexWeightCount MyObject.modifiers[#skin] vert 
		
					--Then in each bones found in each of those vert look for the ones with the same index of MySelectedFromBoneIndex
					for Mybone = 1 to MyBonesPerVert do
					(
					-- get bones index and weights 
					local BoneIndex =	skinOps.GetVertexWeightBoneID MyObject.modifiers[#skin] vert Mybone
		
					local BoneWeight = skinOps.GetVertexWeight MyObject.modifiers[#skin] vert Mybone

					--iif this bone index is  == to the stored  MySelectedFromBoneIndex	then swap
						if BoneIndex == MySelectedFromBoneIndex do
						(					
							BoneIndex = MySelectedToBoneIndex					
						)				
						Append BoneArray BoneIndex
						Append WeightArray BoneWeight				
					)
					--From max 2015 we need to set the FromBone to 0 or instead of switching max will average with the ToBone	
					skinOps.ReplaceVertexWeights MyObject.modifiers[#skin] vert MySelectedFromBoneIndex  0
					
					skinOps.ReplaceVertexWeights MyObject.modifiers[#skin] vert BoneArray WeightArray
					
					-- force the skin modifier to refresh (desactivate reactivate) so it don't weap out every weight after the first swap
					skinOps.unNormalizeVertex MyObject.modifiers[#Skin] vert true
					
					skinOps.unNormalizeVertex MyObject.modifiers[#Skin] vert false		
				)
			),

			-- Swap skinned bones on every selected verts in the mesh 
			fn FN_BoneSwapperSelected AllSelectedSwappVerts MySelectedFromBoneIndex MySelectedToBoneIndex = 
			(				
				
				--For each of those vert find how many bones influences them	
				For vert = 1 to AllSelectedSwappVerts.count do
				(					
					TheVert =  AllSelectedSwappVerts[vert]					
					--Empty Arrays
					local BoneArray = #()
					local WeightArray = #()				
					-- Get number of bones per verts	
					local MyBonesPerVert = skinOps.GetVertexWeightCount MyObject.modifiers[#skin] TheVert 
		
					--Then in each bones found in each of those vert look for the ones with the same index of MySelectedFromBoneIndex
					for Mybone = 1 to MyBonesPerVert do
					(
					-- get bones index and weights 
					local BoneIndex =	skinOps.GetVertexWeightBoneID MyObject.modifiers[#skin] TheVert Mybone
		
					local BoneWeight = skinOps.GetVertexWeight MyObject.modifiers[#skin] TheVert Mybone

					--iif this bone index is  == to the stored  MySelectedFromBoneIndex	then swap
						if BoneIndex == MySelectedFromBoneIndex do
						(					
							BoneIndex = MySelectedToBoneIndex					
						)				
						Append BoneArray BoneIndex
						Append WeightArray BoneWeight				
					)
					--From max 2015 we need to set the FromBone to 0 or instead of switching max will average with the ToBone	
					skinOps.ReplaceVertexWeights MyObject.modifiers[#skin] TheVert MySelectedFromBoneIndex  0
					
					skinOps.ReplaceVertexWeights MyObject.modifiers[#skin] TheVert BoneArray WeightArray
					
					-- force the skin modifier to refresh (desactivate reactivate) so it don't weap out every weight after the first swap
					skinOps.unNormalizeVertex MyObject.modifiers[#Skin] TheVert true
					
					skinOps.unNormalizeVertex MyObject.modifiers[#Skin] TheVert false		
				)		
			)
			
			
			

		
			
		
	)
	
	
	BoneSWPStruct = BoneSwapper()
	
	
	
	
	Rollout  MultiBoneSwapper "Multi Bone Swapper"
(

	
	Spinner spn_count "Swapp Count" range:[1,10,1]  fieldWidth:25 pos:[2,5] type: #integer
	Button btn_SwapSelection "Swapp Selected" 	width:80 height:30 align:#right offset:[-80,-25] toolTip: "Swap Bones on the selected verts only"
	Button btn_GlobalSwap "Global Swapp" 	width:80 height:30 align:#right offset:[1,-35] toolTip: "Swap Bones on the whole mesh"
	group "Targeting"
	(
	Button btn_From "From" 	width:30 height:30 align:#right offset:[-230,-4] toolTip: "Pick source selected bone"
	edittext EdT_From "" width:105 height:22 align:#right offset:[-128,-30]
	Button btn_To "To" 	width:30 height:30 align:#right offset:[-95,-31] toolTip: "Pick target selected bone"
	edittext EdT_To "" width:105 height:22 align:#right offset:[8,-31]
		
	Button btn_From2 "From" width:30 height:30 align:#right offset:[-230,0] toolTip: "Pick source selected bone"
	edittext EdT_From2 "" width:105 height:22 align:#right offset:[-128,-30]
	Button btn_To2 "To" 	width:30 height:30 align:#right offset:[-95,-31] toolTip: "Pick target selected bone"
	edittext EdT_To2 "" width:105 height:22 align:#right offset:[8,-31]
		
	Button btn_From3 "From" width:30 height:30 align:#right offset:[-230,0] toolTip: "Pick source selected bone"
	edittext EdT_From3 "" width:105 height:22 align:#right offset:[-128,-30]
	Button btn_To3 "To" 	width:30 height:30 align:#right offset:[-95,-31] toolTip: "Pick target selected bone"
	edittext EdT_To3 "" width:105 height:22 align:#right offset:[8,-31]
		
	Button btn_From4 "From" width:30 height:30 align:#right offset:[-230,0] toolTip: "Pick source selected bone"
	edittext EdT_From4 "" width:105 height:22 align:#right offset:[-128,-30]
	Button btn_To4 "To" 	width:30 height:30 align:#right offset:[-95,-31] toolTip: "Pick target selected bone"
	edittext EdT_To4 "" width:105 height:22 align:#right offset:[8,-31]
		
	Button btn_From5 "From" width:30 height:30 align:#right offset:[-230,0] toolTip: "Pick source selected bone"
	edittext EdT_From5 "" width:105 height:22 align:#right offset:[-128,-30]
	Button btn_To5 "To" 	width:30 height:30 align:#right offset:[-95,-31] toolTip: "Pick target selected bone"
	edittext EdT_To5 "" width:105 height:22 align:#right offset:[8,-31]
	
	Button btn_From6 "From" width:30 height:30 align:#right offset:[-230,0] toolTip: "Pick source selected bone"
	edittext EdT_From6 "" width:105 height:22 align:#right offset:[-128,-30]
	Button btn_To6 "To" 	width:30 height:30 align:#right offset:[-95,-31] toolTip: "Pick target selected bone"
	edittext EdT_To6 "" width:105 height:22 align:#right offset:[8,-31]
	
	Button btn_From7 "From" width:30 height:30 align:#right offset:[-230,0] toolTip: "Pick source selected bone"
	edittext EdT_From7 "" width:105 height:22 align:#right offset:[-128,-30]
	Button btn_To7 "To" 	width:30 height:30 align:#right offset:[-95,-31] toolTip: "Pick target selected bone"
	edittext EdT_To7 "" width:105 height:22 align:#right offset:[8,-31]
	
	Button btn_From8 "From" width:30 height:30 align:#right offset:[-230,0] toolTip: "Pick source selected bone"
	edittext EdT_From8 "" width:105 height:22 align:#right offset:[-128,-30]
	Button btn_To8 "To" 	width:30 height:30 align:#right offset:[-95,-31] toolTip: "Pick target selected bone"
	edittext EdT_To8 "" width:105 height:22 align:#right offset:[8,-31]
	
	Button btn_From9 "From" width:30 height:30 align:#right offset:[-230,0] toolTip: "Pick source selected bone"
	edittext EdT_From9 "" width:105 height:22 align:#right offset:[-128,-30]
	Button btn_To9 "To" 	width:30 height:30 align:#right offset:[-95,-31] toolTip: "Pick target selected bone"
	edittext EdT_To9 "" width:105 height:22 align:#right offset:[8,-31]
	
	Button btn_From10 "From" width:30 height:30 align:#right offset:[-230,0] toolTip: "Pick source selected bone"
	edittext EdT_From10 "" width:105 height:22 align:#right offset:[-128,-30]
	Button btn_To10 "To" 	width:30 height:30 align:#right offset:[-95,-31] toolTip: "Pick target selected bone"
	edittext EdT_To10 "" width:105 height:22 align:#right offset:[8,-31]
	)


	
	--Grow the height of the rollout to reveal the relevent slots
	on spn_count changed SwapCount do 
	(	
			if spn_count.value == 1 do MultiBoneSwapper.height = 85	
			if spn_count.value == 2 do MultiBoneSwapper.height = 120	
			if spn_count.value == 3 do MultiBoneSwapper.height = 150
			if spn_count.value == 4 do MultiBoneSwapper.height = 180
			if spn_count.value == 5 do MultiBoneSwapper.height = 210
			if spn_count.value == 6 do MultiBoneSwapper.height = 240
			if spn_count.value == 7 do MultiBoneSwapper.height = 270
			if spn_count.value == 8 do MultiBoneSwapper.height = 310
			if spn_count.value == 9 do MultiBoneSwapper.height = 340
			if spn_count.value == 10 do MultiBoneSwapper.height = 370	
	)
	

	
	
	
	On BoneSwapper Open do
	(
	EdT_From.readOnly = true
	EdT_To.readOnly = true	
	EdT_From.readOnly2 = true
	EdT_To.readOnly2 = true
	EdT_From.readOnly3 = true
	EdT_To.readOnly3 = true	
	EdT_From.readOnly4 = true
	EdT_To.readOnly4 = true	
	EdT_From.readOnly5 = true
	EdT_To.readOnly5 = true	
	EdT_From.readOnly6 = true
	EdT_To.readOnly6 = true
	EdT_From.readOnly7 = true
	EdT_To.readOnly7 = true	
	EdT_From.readOnly8 = true
	EdT_To.readOnly8 = true	
	EdT_From.readOnly9 = true
	EdT_To.readOnly9 = true		
	EdT_From.readOnly10 = true
	EdT_To.readOnly10 = true			
	)
	
	

	on btn_From pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_FromTrigger1 == False do
		(				
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedFromBone FromIndex		
			--Feed the TextBox
			MultiBoneSwapper.EdT_From.text = FromName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_FromTrigger1 = True
		)
		--if Trigger = true then the button have already been pressed so if pressed again change the ID in MySelectedFromBone for the corresponding item only 
		
		if BoneSWPStruct.btn_FromTrigger1 == True do
		(				
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			BoneSWPStruct.MySelectedFromBone[1] = FromIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_From.text = FromName
		)			
	)
	
	on btn_From2 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_FromTrigger2 == False do
		(	
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedFromBone FromIndex		
			--Feed the TextBox
			MultiBoneSwapper.EdT_From2.text = FromName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_FromTrigger2 = True			
		)
		if BoneSWPStruct.btn_FromTrigger2 == True do
		(	
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			BoneSWPStruct.MySelectedFromBone[2] = FromIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_From2.text = FromName
		)		
	)	
	
	on btn_From3 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_FromTrigger3 == False do
		(
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedFromBone FromIndex				
			--Feed the TextBox
			MultiBoneSwapper.EdT_From3.text = FromName	
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_FromTrigger3 = True
		)
		if BoneSWPStruct.btn_FromTrigger3 == True do
		(	
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			BoneSWPStruct.MySelectedFromBone[3] = FromIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_From3.text = FromName
		)		
	)
	
	on btn_From4 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_FromTrigger4 == False do
		(
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedFromBone FromIndex				
			--Feed the TextBox
			MultiBoneSwapper.EdT_From4.text = FromName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_FromTrigger4 = True			
		)
		if BoneSWPStruct.btn_FromTrigger4 == True do
		(	
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			BoneSWPStruct.MySelectedFromBone[4] = FromIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_From4.text = FromName
		)
	)
	
	on btn_From5 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_FromTrigger5 == False do
		(
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedFromBone FromIndex				
			--Feed the TextBox
			MultiBoneSwapper.EdT_From5.text = FromName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_FromTrigger5 = True
		)
		if BoneSWPStruct.btn_FromTrigger5 == True do
		(	
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			BoneSWPStruct.MySelectedFromBone[5] = FromIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_From5.text = FromName
		)
	)
	
	on btn_From6 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_FromTrigger6 == False do
		(
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedFromBone FromIndex				
			--Feed the TextBox
			MultiBoneSwapper.EdT_From6.text = FromName	
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_FromTrigger6 = True
		)
		if BoneSWPStruct.btn_FromTrigger6 == True do
		(	
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			BoneSWPStruct.MySelectedFromBone[6] = FromIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_From6.text = FromName
		)		
	)
	
	on btn_From7 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_FromTrigger7 == False do
		(
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedFromBone FromIndex				
			--Feed the TextBox
			MultiBoneSwapper.EdT_From7.text = FromName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_FromTrigger7 = True			
		)
		if BoneSWPStruct.btn_FromTrigger7 == True do
		(	
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			BoneSWPStruct.MySelectedFromBone[7] = FromIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_From7.text = FromName
		)		
	)
	
	
	on btn_From8 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_FromTrigger8 == False do
		(
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedFromBone FromIndex				
			--Feed the TextBox
			MultiBoneSwapper.EdT_From8.text = FromName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_FromTrigger8 = True
		)	
		
		if BoneSWPStruct.btn_FromTrigger8 == True do
		(	
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			BoneSWPStruct.MySelectedFromBone[8] = FromIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_From8.text = FromName
		)		
	)
	
	on btn_From9 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_FromTrigger9 == False do
		(
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedFromBone FromIndex				
			--Feed the TextBox
			MultiBoneSwapper.EdT_From9.text = FromName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_FromTrigger9 = True			
		)
		if BoneSWPStruct.btn_FromTrigger9 == True do
		(	
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			BoneSWPStruct.MySelectedFromBone[9] = FromIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_From9.text = FromName
		)			
	)
	
	on btn_From10 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_FromTrigger10 == False do
		(
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedFromBone FromIndex				
			--Feed the TextBox
			MultiBoneSwapper.EdT_From10.text = FromName	
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_FromTrigger10 = True
		)
		if BoneSWPStruct.btn_FromTrigger10 == True do
		(	
			-- First we get the index of our selected bone		
			FromIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			FromName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] FromIndex 1	
			BoneSWPStruct.MySelectedFromBone[10] = FromIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_From10.text = FromName
		)		
	)
	

	



	--Feed the to's buttons	
	on btn_To pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_ToTrigger1 == False do
		(			
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedToBone ToIndex		
			--Feed the TextBox
			MultiBoneSwapper.EdT_To.text = ToName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_ToTrigger1 = True
		)
		--if Trigger = true then the button have already been pressed so if pressed again change the ID in MySelectedFromBone for the corresponding item only 
		
		if BoneSWPStruct.btn_ToTrigger1 == True do
		(	
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			BoneSWPStruct.MySelectedToBone[1] = ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To.text = ToName
			
		)	
	
	)
	
	
	on btn_To2 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_ToTrigger2 == False do
		(
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedToBone ToIndex	
			--Feed the TextBox
			MultiBoneSwapper.EdT_To2.text = ToName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_ToTrigger2 = True

		)
		--if Trigger = true then the button have already been pressed so if pressed again change the ID in MySelectedFromBone for the corresponding item only 
		
		if BoneSWPStruct.btn_ToTrigger2 == True do
		(	
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			BoneSWPStruct.MySelectedToBone[2] = ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To2.text = ToName
		)			
	)
	
	
	on btn_To3 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_ToTrigger3 == False do
		(
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedToBone ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To3.text = ToName	
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_ToTrigger3 = True

		)
		
		if BoneSWPStruct.btn_ToTrigger3 == True do
		(	
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			BoneSWPStruct.MySelectedToBone[3] = ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To3.text = ToName
		)
	)
	
	on btn_To4 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_ToTrigger4 == False do
		(
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedToBone ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To4.text = ToName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_ToTrigger4 = True
			
		)	
		if BoneSWPStruct.btn_ToTrigger4 == True do
		(	
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			BoneSWPStruct.MySelectedToBone[4] = ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To4.text = ToName
		)		
	)
	
	on btn_To5 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_ToTrigger5 == False do
		(
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedToBone ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To5.text = ToName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_ToTrigger5 = True
			
		)
		if BoneSWPStruct.btn_ToTrigger5 == True do
		(	
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			BoneSWPStruct.MySelectedToBone[5] = ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To5.text = ToName
		)		
	)
	
	
	on btn_To6 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_ToTrigger6 == False do
		(
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedToBone ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To6.text = ToName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_ToTrigger6 = True

		)
		if BoneSWPStruct.btn_ToTrigger6 == True do
		(	
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			BoneSWPStruct.MySelectedToBone[6] = ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To6.text = ToName
		)		
	)
	
	
	on btn_To7 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_ToTrigger7 == False do
		(
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedToBone ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To7.text = ToName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_ToTrigger7 = True

		)
		if BoneSWPStruct.btn_ToTrigger7 == True do
		(	
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			BoneSWPStruct.MySelectedToBone[7] = ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To7.text = ToName
		)		
	)
	
	on btn_To8 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_ToTrigger8 == False do
		(
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedToBone ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To8.text = ToName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_ToTrigger8 = True

		)
		if BoneSWPStruct.btn_ToTrigger8 == True do
		(	
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			BoneSWPStructBoneSWPStruct.MySelectedToBone[8] = ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To8.text = ToName
		)		
	)
	
	on btn_To9 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_ToTrigger9 == False do
		(
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedToBone ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To9.text = ToName
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_ToTrigger9 = True
			
		)
		if BoneSWPStruct.btn_ToTrigger9 == True do
		(	
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			BoneSWPStruct.MySelectedToBone[9] = ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To9.text = ToName
		)			
	)
	
	
	on btn_To10 pressed do     
	(
		BoneSWPStruct.MyObject = selection[1]			
		if BoneSWPStruct.btn_ToTrigger10 == False do
		(
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			--Now append this array MySelectedFromBone that will contain every  FromIndex 
			append BoneSWPStruct.MySelectedToBone ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To10.text = ToName	
			--Trigger ON the button (see bellow)			
			BoneSWPStruct.btn_ToTrigger10 = True
			
		)
		if BoneSWPStruct.btn_ToTrigger10 == True do
		(	
			-- First we get the index of our selected bone		
			ToIndex = skinOps.GetSelectedBone BoneSWPStruct.MyObject.modifiers[#skin]		
			--Then the name
			ToName =	skinOps.GetBoneName BoneSWPStruct.MyObject.modifiers[#skin] ToIndex 1	
			BoneSWPStruct.MySelectedToBone[10] = ToIndex
			--Feed the TextBox
			MultiBoneSwapper.EdT_To10.text = ToName
		)
	)

	
	
	
	
	
	--Swap bone in the whol mesh, no selections needed
	on btn_GlobalSwap pressed do     
	(	
		-- get the number of verts skined in the whole selected mesh
		BoneSWPStruct.AllSwappVerts = skinOps.GetNumberVertices BoneSWPStruct.MyObject.modifiers[#skin]	
		For Loop = 1 to spn_count.value do 
		(			
			BoneSWPStruct.MySelectedFromBoneIndex = BoneSWPStruct.MySelectedFromBone [Loop]			
			BoneSWPStruct.MySelectedToBoneIndex =	BoneSWPStruct.MySelectedToBone [Loop]
			BoneSWPStruct.FN_BoneSwapper BoneSWPStruct.AllSwappVerts BoneSWPStruct.MySelectedFromBoneIndex BoneSWPStruct.MySelectedToBoneIndex 
		)			
	)


	
	--Swap bones in the vert selection only
	on btn_SwapSelection pressed do     
	(	
		
		-- get your selection of verts
		BoneSWPStruct.AllSelectedSwappVerts = #()
		BoneSWPStruct.AllSelectedSwappVerts = BoneSWPStruct.FN_GetVertSelect()
		For Loop = 1 to spn_count.value do 
		(							
			BoneSWPStruct.MySelectedFromBoneIndex = BoneSWPStruct.MySelectedFromBone [Loop]			
			BoneSWPStruct.MySelectedToBoneIndex =	BoneSWPStruct.MySelectedToBone [Loop]
			BoneSWPStruct.FN_BoneSwapperSelected BoneSWPStruct.AllSelectedSwappVerts BoneSWPStruct.MySelectedFromBoneIndex BoneSWPStruct.MySelectedToBoneIndex 
		)
	)	
	
	
	
	
)
createdialog MultiBoneSwapper width:280 height:85
	
	
	
	
	