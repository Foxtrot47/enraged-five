-- Set up body wrinkles.



-- Rick Stirling
-- Rockstar North
-- Sepember 2012

filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())
	
filein "rockstar/export/settings.ms" -- This is fast	


-- Bring in the wrinkle sliders

existingSliders = getNodeByName "WRINKLES_TEXT"

if existingSliders == undefined do 
(
	mergemaxfile (theProjectRoot+"art/peds//Ped_Parts/Sliders/Body_Wrinkle_Sliders.max") #deleteOldDups #useSceneMtlDups #alwaysReparent quiet:true
)
-- Bring in the definition file.
-- This is a mapping between bones and sliders
-- Hardcode it for now.

sliderHookupTable = #(
	#($WM_Lower_000,
		#(#("LCalfZ",$'SKEL_L_Calf'.rotation.controller.Zero_Euler_XYZ.controller.Z_Rotation.controller)),
		("max (0, (min (1, (LCalfZ * -1) * 1)))")
	),
	
	#($WM_Lower_001,
		#(#("RCalfZ",$'SKEL_R_Calf'.rotation.controller.Zero_Euler_XYZ.controller.Z_Rotation.controller)),
		("max (0, (min (1, (RCalfZ * -1) * 1)))")
	),
	
	#($WM_Lower_002,
		#(#("LThighZ",$'SKEL_L_Thigh'.rotation.controller.Zero_Euler_XYZ.controller.Z_Rotation.controller)),
		("max ((min (1 ,(radToDeg (LThighZ) / 50))),0)")
	),
	
	#($WM_Lower_003,
		#(#("RThighZ",$'SKEL_R_Thigh'.rotation.controller.Zero_Euler_XYZ.controller.Z_Rotation.controller)),
		("max ((min (1 ,(radToDeg (RThighZ) / 50))),0)")
	),
	
	#($WM_Lower_006,
		#(#("LThighZ",$'SKEL_L_Thigh'.rotation.controller.Zero_Euler_XYZ.controller.Z_Rotation.controller)),
		("max ((min (1 ,(radToDeg (LThighZ * -3) / 50))),0)")
	),
	
	#($WM_Lower_007,
		#(#("RThighZ",$'SKEL_R_Thigh'.rotation.controller.Zero_Euler_XYZ.controller.Z_Rotation.controller)),
		("max ((min (1 ,(radToDeg (RThighZ * -3) / 50))),0)")
	),
	
	
	#($WM_Upper_000,
		#(
			#("Spine1X",$'SKEL_Spine1'.rotation.controller.Zero_Euler_XYZ.controller.X_Rotation.controller),
			#("Spine2X",$'SKEL_Spine2'.rotation.controller.Zero_Euler_XYZ.controller.X_Rotation.controller),
			#("Spine3X",$'SKEL_Spine3'.rotation.controller.Zero_Euler_XYZ.controller.X_Rotation.controller)
			),
		("min (1, (max (0, ((Spine1X + Spine2X + Spine3X) * 3))))")
	),
	
	#($WM_Upper_001,
		#(
			#("Spine0Z",$'SKEL_Spine0'.rotation.controller.Zero_Euler_XYZ.controller.Z_Rotation.controller),
			#("Spine1Z",$'SKEL_Spine1'.rotation.controller.Zero_Euler_XYZ.controller.Z_Rotation.controller)

			),
		("if( 0 + (Spine0Z + Spine1Z ) > 0.25,min (1,(max (0, ((Spine0Z + Spine1Z ) * (0.8 + Spine1Z) ) -0.1 ))),0)") --added the 0+ after the if due to our exporter being thick as fuck and not understanding double brackets
	),


	
	#($WM_Upper_008,
		#(
			#("Spine1X",$'SKEL_Spine1'.rotation.controller.Zero_Euler_XYZ.controller.X_Rotation.controller),
			#("Spine2X",$'SKEL_Spine2'.rotation.controller.Zero_Euler_XYZ.controller.X_Rotation.controller),
			#("Spine3X",$'SKEL_Spine3'.rotation.controller.Zero_Euler_XYZ.controller.X_Rotation.controller)
			),
		("min (1, (max (0, (-(Spine1X + Spine2X + Spine3X)*3))))")
	)
		
	
		
	
	
)




-- Function to set the controllers up.
-- Takes a single data structure 
fn linkExpressionsToSliders sliderdata = 
(
	-- The first element is the slider object.
	-- It need to have a flaot expression added to its controller list
	-- This is always Zero Y transaltion for sliders, so I'll hardcode that for now
	theSliderObject = sliderdata[1]
	
	fc = undefined -- initialise a floatexpression holder
	fc = theSliderObject.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller = Float_Expression ()
	
	-- For each bone in the list, create a scalar name and assign that to the controller for the bone 
	driverList = sliderdata[2]
	for theDriverIndex = 1 to driverList.count do 
	(	
		SVName = driverList[theDriverIndex][1]
		theController = driverList[theDriverIndex][2]
		try (fc.AddScalarTarget SVName thecontroller)
		catch (messagebox "Failed to add the expression to the controller") 
	)
	
	-- Hookup the expression now
	local theExpression = sliderdata[3]
	fc.SetExpression theExpression
)	




-- Test
sliderdata = sliderHookupTable[1]


-- Full setup 
for s = 1 to sliderHookupTable.count do 
(
	sliderdata = sliderHookupTable[s]
	linkExpressionsToSliders sliderdata
)	

