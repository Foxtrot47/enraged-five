FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_xml.ms")

struct rsta_bindpose 
(	
	-- LOCALS ----------------------------------------------------------
	NodesToMove = #(),
	xml_io,	
	xml_allNodes,
	debug = false,

	---------------------------------------------------------------------------
	-- SAVE POSE 
	---------------------------------------------------------------------------
	fn processNode node xml_parent = 
	(
		local xn_node = rsta_xml.makeNode "Item" xml_parent
		xn_node.SetAttribute "name" node.name	
		xn_node.SetAttribute "type" ((classof node) as string)		

		-- TRANSFORMS
		local trans = in coordsys parent node.transform		
		xn_node.SetAttribute "row1" (trans.row1 as string)		
		xn_node.SetAttribute "row2" (trans.row2 as string)		
		xn_node.SetAttribute "row3" (trans.row3 as string)		
		xn_node.SetAttribute "row4" (trans.row4 as string)
		
		for c in node.children do processNode c xn_node
	),
	---------------------------------------------------------------------------
	fn get_AllChildren item &arr:#() =
	(
		for c in (item.children) do
		(
			append arr c
			get_AllChildren c arr:arr
		)
		return arr
	),
	---------------------------------------------------------------------------	
	fn SavePose rootObj filepath: quiet:false = 
	(
		if (filepath == unsupplied) do filepath = maxfilepath + ((getFilenameFile maxfilename)  + ".bpos")
		grsPerforce.sync #(filepath)
		grsPerforce.edit #(filepath)
		
		if (not doesFileExist filepath) OR (not getFileAttribute filepath #readOnly) do 
		(
			local xml = RSTA_xml_IO()
			xml.new "BindPose"		
			
			-- LOOP OVER THE NODES AND ITS CHILDREN
			processNode rootObj xml.root
			
			-- SAVE OUT XML
			xml.saveAs filepath saveWarning:quiet
			grsPerforce.add #(filepath)
			format "Bindpose : %\n" filepath
		)
	),	
	
	---------------------------------------------------------------------------
	-- XML FUNCTIONS 
	---------------------------------------------------------------------------
	fn stringToPoint3 s = 
	(
		local sA = filterstring s " ,[]"
		return (if sA.count != 3 then [0,0,0] else [sA[1] as float,sA[2] as float,sA[3] as float])
	),
	---------------------------------------------------------------------------
	fn get_transfromFromXmlNode xmlNode = 
	(
		local row1 = stringToPoint3 (xmlNode.getAttribute "row1")
		local row2 = stringToPoint3 (xmlNode.getAttribute "row2")
		local row3 = stringToPoint3 (xmlNode.getAttribute "row3")
		local row4 = stringToPoint3 (xmlNode.getAttribute "row4")	
		
		-- RETURN MATRIX 
		return (matrix3 row1 row2 row3 row4)
	),	
	---------------------------------------------------------------------------
	fn get_AllXmlChildren xn_parent &arr:#() =
	(
		for c in (rsta_xml.getChildren xn_parent) do
		(
			append arr c
			get_AllXmlChildren c arr:arr
		)
		return arr
	),
	
	---------------------------------------------------------------------------
	-- LOAD POSE 
	---------------------------------------------------------------------------
	fn setNodeTransforms node trans =
	(
		if (debug) do format "setNodeTransforms : %\n" node.name
		if (node != undefined AND trans != undefined) then
		(
			in coordsys parent node.transform = trans
			if (debug) do 
			(
				local p = point name:(node.name + "_point") size:0.5
				p.transform = node.transform
			)
		)
		else format "\t- Node or tranform missing.\n"
	),
	---------------------------------------------------------------------------
	fn getTransByName name = 
	(
		for xmlNode in xml_allNodes do 
		(
			local attr = xmlNode.getAttribute "name" 
			if attr == name do return (get_transfromFromXmlNode xmlNode)
		)
		return undefined
	),
	---------------------------------------------------------------------------
	fn LoadPose rootObj: filepath: nameFilter:"SKEL_*" excludeList:#() zeroHeel:true = 
	(
		if (rootObj != undefined) do 
		(
			if (filepath == unsupplied) do filepath = maxfilepath + ((getFilenameFile maxfilename)  + ".bpos")		
			grsperforce.sync #(filepath)
			if (doesFileExist filepath) do 
			(
				xml_io = RSTA_xml_IO xmlFile:filepath
				xml_allNodes = get_AllXmlChildren xml_io.root
				
				if (zeroHeel) do (getNodeByName "SKEL_ROOT").pos.controller.weight[3] = 0
				
				local allChildren = if (rootObj != unsupplied) then get_AllChildren rootObj else (objects as array)
				NodesToMove = for i in allChildren where findItem excludeList i.name == 0 collect i

				for n in NodesToMove where (MatchPattern n.name pattern:nameFilter) do setNodeTransforms n (getTransByName n.name)	
			)	
		)		
	),
	---------------------------------------------------------------------------
	fn LoadPoseFromName rootObj name nameFilter:"SKEL_*" excludeList:#() zeroHeel:true =
	(	
		local filepath = RsConfigGetArtDir() + @"peds\Ped_Models\" + name + "\\" + name + ".bpos"
		LoadPose rootObj filepath:filepath nameFilter:nameFilter excludeList:excludeList zeroHeel:zeroHeel
	)	
)
rsta_bindpose = rsta_bindpose()


