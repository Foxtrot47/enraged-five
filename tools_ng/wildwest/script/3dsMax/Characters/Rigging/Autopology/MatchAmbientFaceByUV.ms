fn getArrayOfMapVerts2VertsPOLY theObj theMapChannel =
	(
	numMapVerts = polyOp.getNumMapVerts theObj theMapChannel
	mapVerts2Verts = for mv=1 to numMapVerts collect #()
	numMapFaces = polyOp.getNumMapFaces theObj theMapChannel
	for f = 1 to numMapFaces do (
    	theMapFace = polyOp.getMapFace theObj theMapChannel f
		polyFace = polyOp.getFaceVerts theObj f
		for mv=1 to theMapFace.count do (
			mapVert = theMapFace[mv]
			if findItem mapVerts2Verts[mapVert] polyFace[mv] == 0 do append mapVerts2Verts[mapVert] polyFace[mv]
			)
		)
	mapVerts2Verts
	)

fn getArrayOfMapVerts2VertsMESH theObj theMapChannel =
	(
	numMapVerts = meshOp.getNumMapVerts theObj theMapChannel
	mapVerts2Verts = for mv=1 to numMapVerts collect #()
	numMapFaces = meshOp.getNumMapFaces theObj theMapChannel
	for f = 1 to numMapFaces do (
    	theMapFace = meshOp.getMapFace theObj theMapChannel f
		theMapFace = #(theMapFace.x as integer,theMapFace.y as integer,theMapFace.z as integer)
		meshFace = getFace theObj f
		meshFace = #(meshFace.x as integer,meshFace.y as integer,meshFace.z as integer)
		for mv=1 to theMapFace.count do (
			mapVert = theMapFace[mv]
			if findItem mapVerts2Verts[mapVert] meshFace[mv] == 0 do append mapVerts2Verts[mapVert] meshFace[mv]
			)
		)
	mapVerts2Verts
	)

fn getArrayOfMapVerts2Verts theObj theMapChannel =
	(
	array=false
	case classOf theObj of
		(
		Editable_mesh: array=getArrayOfMapVerts2VertsMESH theObj theMapChannel
		Editable_Poly: array=getArrayOfMapVerts2VertsPOLY theObj theMapChannel
		)
	array
	)

fn getArrayOfVerts2MapVerts mapVerts2Verts =
	(
	verts2MapVerts=#()
	for mv=1 to mapVerts2Verts.count do
		(
		currentVerts=mapVerts2Verts[mv]
		if currentVerts!=undefined do
			(
			for v=1 to currentVerts.count do
				(
			if verts2MapVerts[currentVerts[v]] == undefined
				then verts2MapVerts[currentVerts[v]]=#(mv)
				else append verts2MapVerts[currentVerts[v]] mv
				)
			)
		)
	verts2MapVerts
	)

-- clearListener()
if selection.count!=0 then
	(
	obj=selection[1]
	channel=1
	mapVerts2Verts=getArrayOfMapVerts2Verts obj channel -- create the array mapVerts ==> verts
	if mapVerts2Verts!=false do (
		verts2MapVerts=getArrayOfVerts2MapVerts mapVerts2Verts -- create the array verts ==> mapVerts
		format"%\n" obj.name
		format "\narray : mapVerts2Verts\n======================\n"
		for i=1 to mapVerts2Verts.count do
		(
		
			format "mapVert % => verts %\n" i mapVerts2Verts[i]
			for j = 1 to mapVerts2Verts[i].count do
			(

				format "%\n" ((polyop.getvert obj mapVerts2Verts[i][j]) as string)
			
			)

		)			
-- 		format "\narray : verts2MapVerts\n======================\n"
-- 		for i=1 to verts2MapVerts.count do format "vert % => mapVerts %\n" i verts2MapVerts[i]
		)
	)