-- This script bakes weights and writes them out
-- It can be used to rebuild a model, to combine two skinned models and to re-order the bones in the skin modifier

-- Rick Stirling 2007
-- Rewritten July 2009
-- Modified by M Rennie March 2010
-- added Skeleton Transfer functionality and skeleton version testing
-- New version is for Max bones and the agent pipeline
-- New version tries to adhere to the R*N Maxscript coding guidelines
-- Variables named more sensibly

-- Rick Stirling, May 2001. Moved to Wildwest 2 and updated accordingly.
-- * Changed skeleton checkboxes to radio, killed some script variables
-- * general tidying

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

-- This script loads the commands to reset te bind pose.
-- FIX!
-- filein ped_pose_reset_script

escapeEnabled = true

skelator =#()
skelatorbool = 0

currentModelName = (filterstring (maxfilename as string) ".")[1]

-- Timing information
startProcess = undefined
endProcess = undefined
startImportWeights = undefined
endImportWeights = undefined
global mySkinnedGeoArray = #()
boneList = #()
skelFile = undefined
input_name = undefined
ped_rootBoneName = undefined
ped_rootBone = undefined

facialXmlFile = undefined

objToKeep = #()

currentSkeletonNumber = 1.0 --THIS SHOULD EVENTUALLY BE PULLED FROM A GLOBAL LOCATION

-- Skeleton types

currentGeoNumber = undefined
geoCount = undefined
myPBar = undefined
btt = undefined
doMy_prog = undefined

global pedSkelParentArray = #(
	#(), --object itself
	#(), --parent obj
	#(), --obj name
	#(), --parent name
	#(), -- obj position in world space
	#() --obj rotation in world
	)
	
importedBonesArray = #()
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
	

fn LSW_EnvelopeCallbackFunction =
(
	WindowHandle = DialogMonitorOPS.GetWindowHandle()
	theDialogName = UIAccessor.GetWindowText WindowHandle

	if theDialogName != undefined and matchpattern theDialogName pattern:"*Load Envelopes*" do
		UIAccessor.PressButtonByName WindowHandle "Match by Name"	
		
	if theDialogName != undefined and matchpattern theDialogName pattern:"*Load Envelopes*" do
		UIAccessor.PressButtonByName WindowHandle "OK"

	true
)
	
fn correctUpperArmRoll = 	--big hack to rectify shoulder roll bones from old skeletons (which used ot be parented differently and have expressions on all 3 axes which the new skeletons dont)
(
	rollbones = #(
		"RB_R_ArmRoll",
		"RB_L_ArmRoll"
		)
	
	rollParents = #(
		"SKEL_R_UpperArm",
		"SKEL_L_UpperArm"
		)	
		
	for i = 1 to rollBones.count do
	(	
		rollBone = getNodeByName rollBones[i]
		rollParent = getNodeByName rollParents[i]
		if rollBone.parent != rollParent do
		(
			rollBone.parent = rollParent
			
			rollBone.transform = rollParent.transform
			
			for i = 1 to 3 do --remove any previous expressions from the import
			(
				cnt = 2 --ie the Zero_Euler_XYZ controller
				if ((classof rollBone.rotation.controller[cnt].controller[i]) as string) == "Float_Expression" do
				(
					rollBone.rotation.controller[cnt].controller[i] = bezier_float()
				)
			)
			
			--now we need to ensure the y and Z expression are correct (i.e have no scalars and are set to an expression of 0)
			
			for cnt = 1 to rollBone.rotation.controller.count do
			(
				if ((classof rollBone.rotation.controller[cnt].Y_rotation.controller) as string) == "Float_Expression" do
				(
					--first off we'll overwrite the controller to delete any scalars
					initCnt = ( "$"+rollBone.name+"."+"rotation.controller["+(cnt as string)+"].Y_Rotation.controller = bezier_float()") 
					execute initCnt
					initCnt = ( "$"+rollBone.name+"."+"rotation.controller["+(cnt as string)+"].Y_Rotation.controller = Float_Expression()") 
					execute initCnt
					
					fcStr = ( "$"+rollBone.name+"."+"rotation.controller["+(cnt as string)+"].Y_Rotation.controller") --rebuild the string so it can be used below	
					local newfcStr = execute fcStr
					
					newfcStr.SetExpression "0"
				)
				
				if (classOf rollBone.rotation.controller[cnt].Z_Rotation.controller as string) == "Float_Expression" do
				(
					--first off we'll overwrite the controller to delete any scalars
					initCnt = ( "$"+rollBone.name+"."+"rotation.controller["+(cnt as string)+"].Z_Rotation.controller = bezier_float()") 
					execute initCnt
					initCnt = ( "$"+rollBone.name+"."+"rotation.controller["+(cnt as string)+"].Z_Rotation.controller = Float_Expression()") 
					execute initCnt					
					
					fcStr = ( "$"+rollBone.name+"."+"rotation.controller["+(cnt as string)+"].Z_Rotation.controller") --rebuild the string so it can be used below	
					local newfcStr = execute fcStr
					
					newfcStr.SetExpression "0"
				)				
			)

		)
	)
)
	
-------------------------------------------------------------------------------------------------------------------------
fn preParentiser = --function to preprocess and store parenting
(
	objArray = objects as array
	
-- 	for obj in ob
	
	for obj = 1 to objArray.count do
	(
		validObj = undefined
		if (superclassof objArray[obj] ) != camera then
		(
			if (isKindOf objArray[obj] TargetObject) != true then
			(
-- 				print (objArray[obj].name+" is ok")
				validObj = true
			)
			else
			(
-- 				print ("Skipping preparentise on "+objArray[obj].name+" A")
				validObj = false
			)
		)
		else
		(
-- 			print ("Skipping preparentise on "+objArray[obj].name+" B")
			validObj = false
		)	
		
		if validObj == true then
		(
				--pedSkelParentArray[1][obj] = (objArray[obj]) --this creates the object itself in the 1st dimension of array		
				append pedSkelParentArray[1] objArray[obj]
				--pedSkelParentArray[3][obj] = (objArray[obj].name) 
				append pedSkelParentArray[3] (objArray[obj].name) 
				
				if objArray[obj].parent != undefined then
				(
					objParent = objArray[obj].parent
					objParentName = objParent.Name
				)
				else
				(
					objParent = undefined
					objParentName = undefined
				)

				--pedSkelParentArray[2][obj] = objParent
				append pedSkelParentArray[2] objParent
				--pedSkelParentArray[4][obj] = objParentName
				append pedSkelParentArray[4] objParentName
				--pedSkelParentArray[5][obj] = (in coordsys parent objArray[obj].position)
				append pedSkelParentArray[5] (in coordsys parent objArray[obj].position)
				--pedSkelParentArray[6][obj] = (in coordsys parent objArray[obj].rotation)
				append pedSkelParentArray[6] (in coordsys parent objArray[obj].rotation)
		)
		else
		(
			print ("Skipping preparentise on "+objArray[obj].name)
		)
-- 			)
-- 		)
	)
)

-------------------------------------------------------------------------------------------------------------------------
fn reParentiser = --this will reparent after a skeleton merge to ensure that any bones that werent merged get reparented to their merged equivalanet
(	
	--gonna try a huge hack here
	for obj = 1 to pedSkelParentArray[1].count do
	(
		thisNode = getnodebyname pedSkelParentArray[3][obj]
		
		if (substring thisNode.name 1 4) != "CTRL" do
		(
			thisNode.parent = undefined
		)
	)
	
	for obj = 1 to pedSkelParentArray[3].count do
	(
		thisNode = getnodebyname pedSkelParentArray[3][obj]
		if thisNode != undefined do
		(
			if pedSkelParentArray[4][obj] != undefined do
			(
				thisParent = getNodeByName pedSkelParentArray[4][obj]
				
				if (substring thisNode.name 1 4) != "CTRL" do
				(
					thisNode.parent = thisParent
				)
			)
		)
	)	
)

-------------------------------------------------------------------------------------------------------------------------
-- Function to query user meta-data on skel_root to see if it contains a correct version no.
-- Returns version or Undefined
fn queryPedSkelVersion = (
	
	currentSkelversion = undefined
	
	if ((ped_rootbone  != undefined) and (not isdeleted ped_rootbone))do
	(
		-- Read the User Defined Properties
		currentUDP = getUserPropbuffer ped_rootbone
		filterUDP = filterString currentUDP "\r\n"

		-- Look for skeleton version numbers
		for i = 1 to filterUDP.count do (
			tagTest = filterstring filterUDP[i] "="
			skeltest = substring tagtest[1] 1 18
				
			if skelTest== "SkeletonVersionTag" do (
				verInt = (tagtest[2] as float) --hacky way to strip off any leading whitespace shite!
				currentSkelversion = (verInt as string)
			)
		)
	)
	currentSkelversion -- return this
) -- End function

-------------------------------------------------------------------------------------------------------------------------
-- try to figure out the skeleton type based on the models name
-- set up some boolean states based on this
fn defineSkelType = 
(
	skelcheck =  maxFileName[3]
	returnval = 3
	
	case skelcheck of 
	(	
		"M": returnval = 1
		"F": returnval = 2
	)
	
	returnval -- send back an index for the UI
) -- end funtion

-------------------------------------------------------------------------------------------------------------------------
-- Put the model into the base pose
-- Uses functions based on predefined skeleton transforms
fn assumeSkinPose = 
(
	objSelectedArray = getCurrentSelection()
		
	sliderTime = 0f
	skeletonType = defineSkelType()
	case skeletonType of
	(
		1: maleTransforms()
		2: femaleTransforms()
	)	
	
	select objSelectedArray
)

-------------------------------------------------------------------------------------------------------------------------
-- Build an array of all the skinned geometry in the scene
fn isGeoSkinned =
(
-- 	subfolder = (filterstring (maxfilename as string) ".")[1] 
-- 	try (makedir pedSkinData) catch()--Try to makes a skin data folder if it doesn't already exist.

	for obj in geometry do 
	(
		if obj.modifiers[#Skin] != undefined do
		(
			appendIfUnique mySkinnedGeoArray obj 
			--print ("Appending "+obj.name+" to mySkinnedGeoArray")
		)
	)
)

-------------------------------------------------------------------------------------------------------------------------
--NEED TO ADD FUNCTIONALITY HERE TO ADD THIS OUTPUT DATA FILE TO PERFORCE
--OUTPUT SKELETON VERSION NUMBER TO DATA FILE
fn outputDataFile currentSkelversion = (
	if currentSkelversion != undefined do (
		dataFolder = maxFilePath
		charName = ( filterstring maxFileName ".")
		charName = charName[1]
		fName = (dataFolder+charName+".skelVer")
		output_file = createfile fName
		format (currentSkelversion+"\n") to:output_file
		close output_file
	)
) -- End function

-------------------------------------------------------------------------------------------------------------------------
-- Need to decide what skeleton we are going to use
fn defineSkelFolder skeltype = 
(
	guessSkelFolder = theProjectPedSkeletonFolder
	
	-- TEST IF CUSTOM WAS CHOSEN AND IF SO DO THE FOLLOWING, OTHERWISE USE THE APPROPRIATE DEFAULT.
	if skeltype > 3 then (
		guessFilename = (defaultSkeleton+".max")
		skelFile = getOpenFileName caption:"Choose your base skeleton" filename:(guessSkelFolder+guessFilename)
	)
	
	skelfile=""

	case skeltype of
	(	
		1: skelFile = (guessSkelFolder + "Male Skeleton.max")
		2: skelFile = (guessSkelFolder + "Female Skeleton.max")
		3: skelFile = (guessSkelFolder +  "Female Skeleton Heels.max")
	)
	
	print skelfile	

	-- Increment the progress bar
	myPBar.value = 5
) -- End function

-------------------------------------------------------------------------------------------------------------------------
-- Load the dummy and mover if required, reparent the root.
fn ReparentRig =(
	tomerge =theProjectPedSkeletonFolder + "Dummy and Mover.max"
	if $Dummy01 == undefined do mergemaxfile tomerge #deleteOldDups
-- 		print ("ped_rootbone = "+(ped_rootbone as string))
-- 	ped_rootbone.parent = $mover
	ped_rootBone = getNodeByName ped_rootBoneName
	ped_rootbone.parent = $mover
) -- end function

-------------------------------------------------------------------------------------------------------------------------
fn mergeReplacementSkel =
(
	currentSkelversion = queryPedSkelVersion()
	
	outputDataFile currentSkelversion -- create the .ver skeleton version file

	mergeMaxFile skelFile #deleteOldDups #useSceneMtlDups #alwaysReparent quiet:true
--**** BUILD AN ARRAY OF ALL OBJECTS IMPORTED TO COMPARE AND APPEND TO THE BONE FILE IMPORTED LIST LATER	
	
	append importedBonesArray $SKEL_Root
	for obj in importedBonesArray do
	(
		if obj.children != undefined do
		(
-- 			selectMore obj.Children
			for c = 1 to obj.children.count do
			(
			appendIfUnique importedBonesArray obj.children[c]
			)
		)
	)
	
	ReparentRig()
	myPBar.value = 100
-- 	messagebox "mergeReplacementSkel completed."
)

invalidObs = #()

fn findInvalidObjects invalidsFile =
(
-- 	--use the exprObjDataFile file to rebuild the name of the _Invalids.txt file
-- 	print exprObjDataFile
-- 	
-- 	--"X:\gta5\art\peds\Skeletons\expression_skeletons\ambient\male_std_EXPRObjs.hir"
-- 	newString = filterstring exprObjDataFile "."
-- 	
-- 	newstring = newString[1]
-- 	
-- 	newStringCount = newString.count
-- 	
-- 	newString = (substring newstring 1 (newStringCount - 8) )
-- 	
-- 	newString = (newString + "Invalids.txt")
	
	f = openfile invalidsFile

	while not eof f do
	(
		thisLine = (filterstring (readLine f) "\n")
		thisLine = thisLine[1]
-- 		thisLine = (substring thisLine 3 -1)
-- 		thisLineCount = thisLine.count
-- 		thisLine = (substring thisLine 1 (thisLineCount -1))
		append invalidObs thisLine 
	)
	close f
	
	missingObjects = #()
	for i in invalidObs do 
	(
		ob = getNodeByName i
		if ob == undefined do 
		(
			append missingObjects i
		)
	)
	
	continueVal = true
	
	if missingObjects.count != 0 do 
	(
		messageBox ("Warning we cannot import your expressions.")
		print ("The following objects are missing and cannot be regenerated by script."+"\n"+"\n\n"+"Please merge these in from the scene from which your expressions originated.")
		for i = 1 to missingObjects.count do 
		(
			print i
		)
		continueVal = false
	)
	return continueVal
)


fn queryExpressionObjects objToKeep = 
(
	
--now need to figure out how to remove objects which are not required for the expressions.
			
	--what we'll do is run the script which makes expression sets, then query 
	--the expression objects and then for each 
	--object in there we'll add it and its parents and their parent to a 'tokeep' array
	filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/findExpressionObjects.ms")
			
			
	
	selCount = getNumNamedSelSets()
	SelName = "*Expressions"
	exprItems = #()
	
-- 	for i = 1 to selCount do 
	for i = selCount to 1 by -1 do 
	(
		if getNamedSelSetName i == SelName then
		(
			--now record what items already exist in that set
			setItems = getNamedSelSetItemCount i
			
			for a = 1 to setItems do
			(
				thisItem = getNamedSelSetItem i a 
				appendIfUnique exprItems thisItem
			)
			
		)
	)
	
	--now we need to loop through the hierarchy and find all the parent nodes above the expression bones
	
	for ex = 1 to exprItems.count do 
	(
		expNode = exprItems[ex]
		
		appendIfUnique objToKeep expNode
		
		expNodePar = expNode.parent
		
		while expNodePar != undefined do 
		(
			appendIfUnique objToKeep  expNodePar
			
			expNodePar = expNodePar.parent
		)
	)
	
	
-- 	return objToKeep 
)


fn outputBonesData geo output_Name = 
(
	max modify mode
-- 	Geo = selection

	skinMod = geo.modifiers[#Skin]
	totalSkinModBones = skinOps.getNumberBones skinMod

		if totalSkinModBones > 0 then
		(	
			if output_name != undefined then 
			(
				output_file = createfile output_name		

				for i = 1 to totalSkinModBones do
				(
					thebonename = skinOps.getBoneName skinMod  i 1
					append boneList theBoneName
					format ((theBoneName as string)+"\n") to:output_file
				)
				
				close output_file
					--edit output_name --opens the file in a maxscript window so it can be checked.
				
			)--end if
		)--end if
)



FN outputSkinningData geo =
(
	thisSkin = geo.modifiers[#Skin]
	if thisSkin != undefined do
	(
		thisSkinFolder = "c:\\skins\\"
		thisSkinSubFolder = (filterstring maxfilename ".")
		thisSkinSubFolder = (thisSkinSubFolder[1]+"\\")
		makeDir (thisSkinFolder+thisSkinSubFolder)
		max modify mode
		select geo
		skinFileName = (thisSkinFolder+thisSkinSubFolder+geo.name+".env")
		--now we'll try and delete the existing env file first
		skinFiles = getFiles (thisSkinFolder+thisSkinSubFolder+"*.env")
		for i = skinFiles.count to 1 by -1 do 
		(
			if skinFiles[i] == skinFileName do
			(
				--ok we delete the existing file
				deleteFile skinFiles[i]
			)
		)
		
		skinOps.saveEnvelope thisSkin skinFileName
-- 		skinOps.saveEnvelopeAsASCII thisSkin (thisSkinFolder+thisSkinSubFolder+geo.name+".env")
		output_name = ("c:/skins/"+thisSkinSubFolder+geo.name+".bon")
		outputBonesData geo output_name 
		print ("Bone data output for "+geo.name+". Saved in "+output_Name)
		print ("Skin data output for "+geo.name+". Saved in "+(thisSkinFolder+thisSkinSubFolder+geo.name+".env"))
	)
)


fn runOutput = 
(
	if selection.count == 0 then
	(
		messageBox ("Please pick your geometry.") beep:true
	)
	else
	(
		selA = selection as array
		for i in selA do
		(
			outputSkinningData i
		)		
	)
)

fn inputBonesData geo input_name = 
(
	if geo.modifiers[#Skin] != undefined do
	(
		skinMod = geo.modifiers[#Skin]

		boneList = #() --reinitialise to empty
		
		if input_name != undefined then
		(
			f = openfile input_name
			inputData = #() -- define as array
			
			while not eof f do
			(
			append inputData (filterstring (readLine f) ",")
			)
			close f

			for i in inputData do --read in all the bone name data to an array
			(
				currBonename = (i as string)
				currBoneLength = currBonename.count
				currBone = ((substring currBonename 4 (currBonelength - 5)))
					
					currentBone = getNodeByName currBone
					if currentBone != undefined then	
					(
						appendIfUnique boneList currentBone
					)
					else
					(
						print ("Couldn't find "+currBone+" in this scene.")
					)
			)
			
			select geo
			SetCommandPanelTaskMode mode:#modify			
			modPanel.setCurrentObject $.modifiers[#Skin]
			
			for i = 1 to boneList.count do --add the bones into the skin modifier
			(				
				boneToAdd = bonelist[i]
				
				boneUpdateInt = undefined
				if i != bonelist.count then
				(
					boneUpdateInt = 0
				)
				else
				(
					--ok we're at the count so we need to refresh
					boneUpdateInt = 1
				)
				
				if boneToAdd != undefined do
				(
					skinOps.addbone skinMod boneToAdd boneUpdateInt
				)
				
			)
		)
-- 		print ("Loaded Bones:"+input_name+" on to "+geo.name)
	)
)

fn applySkinning geo envFile = 
(
	DialogMonitorOPS.RegisterNotification LSW_EnvelopeCallbackFunction ID:#ANoon_Envelopes
	DialogMonitorOPS.Enabled = true
	
	completeRedraw()
	skinOps.loadEnvelope geo.modifiers[#Skin] envFile
-- 	skinOps.loadEnvelopeAsASCII geo.modifiers[#Skin] envFile
	completeRedraw()

	DialogMonitorOPS.Enabled = false
	DialogMonitorOPS.UnRegisterNotification ID:#ANoon_Envelopes	
	
	
-- 	print ("Loaded Skin: "+envFile+" onto "+geo.name)
)

fn inputSkinningData geo =
(
	thisSkinFolder = "c:\\skins"
	maxName = (filterString maxfileName ".")
	maxName = maxName[1]
	thisSkinSubFolder = (geo.name)
	
	slashChar = "\\"
	
	boneFileName = (thisSkinFolder+slashChar+maxName+slashChar+thisSkinSubFolder+".bon")
	skinFileName = (thisSkinFolder+slashChar+maxName+slashChar+thisSkinSubFolder+".env")
	
	bonFiles = getFiles (thisSkinFolder+slashChar+maxName+slashChar+"*.bon")
	skinFiles = getFiles (thisSkinFolder+slashChar+maxName+slashChar+"*.env")
	
	--first clear off existing mod
	if geo.modifiers[#Skin] != undefined do 
	(
		for i = geo.modifiers.count to 1 by -1 do
		(
			if classOf geo.modifiers[i] as string == "Skin" do 
			(
				deleteModifier geo geo.modifiers[i]
			)
		)
	)
	
	--now add a new skin modifier
	addModifier geo (Skin ())
	geo.modifiers[#Skin].bone_Limit = 4
	geo.modifiers[#Skin].showNoEnvelopes = on		
	
	--now we'll see if we can find the relevant bon file
	boneFile = undefined 
	for i = 1 to bonFiles.count do 
	(
		if bonFiles[i] == boneFileName do --ok inut the bone data
		(
			--print ("Found bone: "+boneFileName)
			boneFile = bonFiles[i]
			inputBonesData geo boneFile
		)
	)
	
	skinFile = undefined
	for i = 1 to skinFiles.count do --ok input the skin data
	(
		if skinFiles[i] == skinFileName do 
		(
			--print ("Found skin: "+skinFileName)
			skinFile = skinFiles[i]
			applySkinning geo skinFile
		)
	)	
)



-------------------------------------------------------------------------------------------------------------------------
--THIS IS WHAT GETS CALLED BY THE BUTTON TO RUN THE SKELETON TRANSFER
-- It mainly just calls a nunch of other functions
-- fn runSkelTransfer Skeltype= (
fn runBodyTransfer Skeltype= (
	
	startProcess = timestamp()
	
	skinnedObjs = #()
	
	-- do preprocess to sort out parenting
	-- Rick says "I have no idea why Matt wrote this bit"
	parentArray = #(	#(),	#(), #(), #()) --empty the array
	preParentiser()
		
	myPBar.value = 0
	defineSkelFolder skeltype
-- 	isGeoSkinned()
	
-- 	for i = 1 to mySkinnedGeoArray.count do
-- 	(			
-- 			findSkinnedGeo mySkinnedGeoArray[i]
-- 	)
	
		clearSelection()
		for o in objects do 
		(
			if o.modifiers[#Skin] != undefined do 
			(
				selectMore o
				appendIfUnique skinnedObjs o
			)
		)
	runOutput()	--save skinning
	--now need to export the expressions

	--first we need to select everything
	max select all
--HERE	
-- 	filein (RsConfigGetWildWestDir() + "script/3dsMax/characters/rigging/ExpressionExporter/outputExpressionToXML.ms")
		
			hierArray = #()
			
			filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/ExpressionExporter/objectOutput.ms")	
			
			filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/ExpressionExporter/expressionOutputXML.ms")			
		
	--need to find the ped root
	moverNode = getNodeByName "mover"
		moverKids = moverNode.children
		ped_rootBoneName = moverKids[1].name
	
	--now freeze transforms of all nodes
--WHY AM I FREEZING EVERYTHING?		
	for obj = 1 to pedSkelParentArray[1].count do
	(
		select pedSkelParentArray[1][obj]
		if (substring pedSkelParentArray[1][obj].name 1 4) != "CTRL" do --may need to come up with somethign more sophisticated here
		(
			freezeTransform()
		)
	)
		
	mergeReplacementSkel()
	
	--now need to regen the expressions
	thisPath = maxFilePath
	thisMaxFile = (filterstring (maxFileName as string) ".")
		thisMaxFile = thisMaxFile[1]
	facialXmlFile = (thisPath+thisMaxFile+"_RSNXML.xml")		
	
	print ("using "+facialXMLFile+" as facial xml")

	global mjrBodyXferFile = facialXmlFile
	
driverXML = (maxFilePath+"driverXML.xml") 
cAttXML = (maxfilepath+"cAttXML.xml")
springXML = (maxfilepath+"springXML.xml")
lookATXML = (maxfilepath+"lookAtXML.xml")

	global exprObjDataFile	= (thisPath+thisMaxFile+"_EXPRObjs.hir")	

			--createdObjects = #() --used to record which objects get generated
			global objToKeep = #()
			global createdObjects = #()
			

			mjrBodyXferFile  = undefined
			facialXmlFile = undefined
			
			--now we'll figure out the facial file based off the obj file
			newString = filterstring exprObjDataFile "."
	
			newstring = newString[1]
	
			newStringCount = newString.count
	
			newString = (substring newstring 1 (newStringCount - 8) )
	
			facialXmlFile = (newString + "RSNXML.XML")
			
			invalidsFile = (newString + "Invalids.txt")
			--first off we need to use the _invalids.txt file to test if the scene has any of the objects contained within it. If it doesnt we need to stop execution and tell
			--the user they need to merge these in from which the scene which the expression originated			
			
			carryOn = findInvalidObjects invalidsFile
			
			print ("CarryOn = "+(carryon as string))
			
			if carryOn == true do 
			(
				filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/ExpressionExporter/objectInput.ms")	
					
				filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/ExpressionExporter/expressionInputXML.ms")	
				
				--filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/ExpressionExporter/trimHierarchy.ms")	
			
				queryExpressionObjects objToKeep
				
				for i = 1 to objToKeep.count do 
				(
					for o = createdObjects.count to 1 by -1 do 
					(
						if createdObjects[o] == objToKeep[i] do 
						(
							--these are the same so we can remove from created objects array then we'll delete the array at the end
							deleteItem createdObjects o
						)
					)
				)
	-- 			
				--now we've gone through all the objects we can remove what we dont need
				delete createdObjects
			)
			
			objToKeep.count = 0
			createdObjects.count = 0
			
			print "Expressions recreated!"
	
-- 	filein (RsConfigGetWildWestDir() + "script/3dsMax/characters/rigging/ExpressionExporter/inputExpressionFromXML.ms")		

	facialXmlFile = undefined
	
-- 	correctUpperArmRoll() -- doesnt seem required anymore
	
-- 	importAllBonesAndWeights()
			
	for obj in skinnedObjs do
	(
		select obj
		inputSkinningData obj
	)
		
	messagebox "Skeleton transfer complete."
	
	exprObjDataFile = undefined
)

-------------------------------------------------------------------------------------------------------------------------
fn findDupGeoNames mcbState fcbState ccbState = 
(
	matchedArray = #()
	objArray = objects as array
	print ("objArray count =  "+(objArray.count as string))
	
	for obj = 1 to objArray.count do
	(
		for thisObj = 1 to objArray.count do
		(
			if thisObj != obj do
			(
				if objArray[obj].name == objArray[thisObj].name do
				(
					print ("WARNING! "+objArray[obj].name +" clashes with "+objArray[thisObj].name )
					append matchedArray objArray[thisObj]
				)
			)
		)
	)

	if matchedArray.count == 0 then
	(
		print "OK we can run the rest. No clashes found."
		runBodyTransfer mcbState fcbState ccbState 
	)
	else
	(
		messageBox ("WARNING! Duplicate object names found. Please see the listener try again.") beep:true
	)
)

-------------------------------------------------------------------------------------------------------------------------
-- Get the model name
fn initialiseModel = (
	-- Delete collisions and contraints, they can be added again later.
	try (delete $const*)catch()
	try (delete $colli*)catch()

	-- get model name
	currentModelName = (filterstring (maxfilename as string) ".")[1]
)

-------------------------------------------------------------------------------------------------------------------------
fn SaveBodyParts= (
	--Save body
	savemaxfile (currentModelLocation + "body.max")
)

-------------------------------------------------------------------------------------------------------------------------
fn LoadBodyParts = (
	-- Merge Body
	tomerge = (maxfilepath + "body.max")
	try (mergemaxfile tomerge) catch (messagebox "Body.max file not found")
)

-------------------------------------------------------------------------------------------------------------------------
fn skinWrapSelected skeletonType SWFalloff SWThresh = (
	
	if selection[1] == undefined then (
		messagebox "Need to select meshes to Skinwrap"
		return 0
	)	
		
	meshesToSkinwrap = getcurrentselection()
	
	-- This function loads a skeleton and skinwraps the selected parts
	skinwrapfile = undefined
	case skeletonType of 
	(
		1: skinwrapfile = male_skinwrap 
		2: skinwrapfile = female_skinwrap 
		3: skinwrapfile = female_skinwrap_heels 
	)	
	
	-- The skinwrap file is valid then load it
	if skinwrapfile != undefined then (
		print "Found skeleton for skinwrap, merging it now"
		mergeMaxFile skinwrapfile #select #deleteOldDups #useSceneMtlDups #alwaysReparent quiet:true
		skinwrapdata = getcurrentselection()
		clearSelection()
		
		-- name the model
		$'Your Model Name'.name = currentModelName
		
		-- Skinwrap and skin each object
		meshcount = meshesToSkinwrap.count
		print ("User has selected " + (meshcount as string) + " meshes to process")
		for i = 1 to meshcount do (
			pedcomp = meshesToSkinwrap[i]
			select pedcomp 
			print ("About to skinwrap " + pedcomp.name)
			
			-- Reset the transform
			resetxform pedcomp 
			maxOps.CollapseNodeTo pedcomp  1 True
			
			-- Parent the rig
			pedcomp.parent = getnodebyname currentModelName
			
			-- At this point, skip the object if it's a prop 
			if pedcomp.name[2] != "_" then  (
				print (pedcomp.name + " is a component so will skinwrap it.")
				-- Skinwrap and convert to skin
				modPanel.addModToSelection (Skin_Wrap ()) ui:on
				
				pedcomp.modifiers[#Skin_Wrap].falloff = SWFalloff 
				pedcomp.modifiers[#Skin_Wrap].threshold  = SWThresh
				pedcomp.modifiers[#Skin_Wrap].meshList = #($SkinWrapBody,$SkinWrapHands)
				pedcomp.modifiers[#Skin_Wrap].weightAllVerts = on
					
				pedcomp.modifiers[#Skin_Wrap].meshDeformOps.convertToSkin off
				modPanel.setCurrentObject pedcomp.modifiers[#Skin]
				pedcomp.modifiers[#Skin].bone_Limit = 4
				maxOps.CollapseNodeTo pedcomp 2 true
			)
			
			else print "Skipping prop, does not require skin"
		)
	)	
)

-------------------------------------------------------------------------------------------------------------------------
fn autopropSetup =
(
	print "Sorting Props..."
	selectByWildcard ped_propPrefix
	if selection.count != 0 then
	(
		AlignProps ()
		unlinkProps ()
		LinkProps ()
		print "...Props Linked and Aligned"
	)
	else(print "...No Props in scene")
)
--end function Align and Links props	

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
	if ((bodyTransferTool != undefined) and (bodyTransferTool.isDisplayed)) do
	(destroyDialog bodyTransferTool)
-------------------------------------------------------------------------------------------------------------------------
 rollout bodyTransferTool "Skeleton update tool"
(
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:bodyTransferTool.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Character_Body_Transfer_Tool" filename:(getThisScriptFilename())
	
	edittext edtModel "" readonly:true
	
	progressbar doMy_prog color:red

	edittext skelver "Skeleton Version: "  readOnly:true  
	radiobuttons rad_skeletonType "" labels:#("Male", "Female", "F-Heels", "Custom") columns:4  offset: [0,10]
		
	button btnSkinWrap "Skinwrap Selected"  width:120 height:50 across:2 
	spinner spn_skinwrapFalloff "Falloff" range:[0,1,0.1] fieldwidth: 30 offset: [-10,5]
	spinner spn_skinwrapThreshold "Threshold" range:[0,1,0.1] fieldwidth: 30 offset: [-10,-30]

	button btnSaveSkinWeights "Save selected skin"  width:120 height:30 across:2 offset: [0,10]
	button btnSaveBodyParts "Save body"  width:120 height:30 offset: [0,10]

	button btnLoadSkinWeights  "Load selected skin"  width:120 height:30 across:2
	button btnLoadBodyParts "Load body" width:120 height:30

	button btnReparentRig "Reparent Rig"  width:120 height:30 across:2
	button btnUpdateSkeleton "Update Skeleton"  width:120 height:30
	
	button btnRefreshScript "Refresh This UI"  width:255 height:30
	
	button btnSkinBrush "Skin brush" width:120 height:30 across:2
	button btn_animlibrary "Animation Loader"  width:120 height:30 
	
	on bodyTransferTool open do -- When the UI opens, do some stuff
	(
		banner.setup()
		
		WWP4vSync pedSkinwrapFiles
		WWP4vSync pedSkeletonFiles
		
		try (makedir pedSkinData) catch()--Try to makes a skin data folder if it doesn't already exist.

		myPBar = doMy_prog
		pedname = initialiseModel()
		
		defineSkelType() 
		
		bodyTransferTool.edtModel.text = pedname as string
		currentSkelversion = queryPedSkelVersion()
		bodyTransferTool.skelVer.text = currentSkelversion as string
		bodyTransferTool.rad_skeletonType.state = defineSkelType() 
	)

	on btnSkinWrap pressed do
	(
		skinWrapSelected rad_skeletonType.state spn_skinwrapFalloff.value spn_skinwrapThreshold.value
		autopropSetup()
	)
	
	on btnSaveSkinWeights pressed do
	(
		sliderTime = 0f
		
		if selection.count == 0 then
		(
			messageBox ("Please pick your geometry.") beep:true
		)
		else
		(
			selA = selection as array
			for i=1 to selA.count do
			(
				outputSkinningData selA[i]
				myPBar.value = ((100* i)/selA .count)
			)
		)
		myPBar.value = 0
	)
		
	on btnLoadSkinWeights pressed do
	(
		sliderTime = 0f
		
		if selection.count == 0 then
		(
			messageBox ("Please pick your geometry.") beep:true
		)
		else
		(
			selA = selection as array
			for i=1 to selA.count do
			(
				inputSkinningData selA[i]
				myPBar.value = ((100* i)/selA .count)
			)
		)
		myPBar.value = 0
	)
	
	on btnSaveBodyParts pressed do SaveBodyParts()
	on btnLoadBodyParts pressed do LoadBodyParts()
	on btnReparentRig  pressed do ReparentRig()

	on btnUpdateSkeleton pressed do 
	(
		runBodyTransfer bodyTransferTool.rad_skeletonType.state
	)
		
	on btnRefreshScript pressed do
	(
		try (makedir pedSkinData) catch()--Try to makes a skin data folder if it doesn't already exist.

		myPBar = doMy_prog
		pedname = initialiseModel()
		
		defineSkelType() 
		
		bodyTransferTool.edtModel.text = pedname as string
		currentSkelversion = queryPedSkelVersion()
		bodyTransferTool.skelVer.text = currentSkelversion as string
		bodyTransferTool.rad_skeletonType.state = defineSkelType()
	)
	on btnSkinBrush pressed do(filein ped_skinbrush_script )
	on btn_animlibrary pressed do (filein ped_animation_library_script) 
)

createDialog bodyTransferTool width:290