-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()


--Load common functions	
-- filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
-- filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_bone_tagger.ms")
-- filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_Rigging.ms")
-- -- filein (theProjectRoot + "tools/dcc/current/max2010/scripts/pipeline/util/xml.ms")

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())

-- selectedAnim = (theProjectRoot+"art/peds/Skeletons/Animations/FaceFX_3L_AmbientRigPoses_Trevor.xaf")
filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_Rigging.ms")

filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_UI.ms")	
selectedAnim = undefined
	
-- 	messagebox "Reset pose file"
-- facePoseTextFile = (theProjectRoot+"art/peds/FaceFX/ambientFaceRigPosesTEST.txt")
facePoseTextFile = (theProjectRoot+"art/peds/FaceFX/ambientFaceRigPoses.txt")	

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

joystickArray = #( --array of all joysticks to track
	"mouth_CTRL",
	"jaw_CTRL",
	"tongueMove_CTRL",
	"tongueRoll_CTRL",
	"tongueInOut_CTRL",
	"outerBrow_R_CTRL",
	"innerBrow_L_CTRL",
	"outerBrow_L_CTRL",
	"innerBrow_R_CTRL",
	"eye_C_CTRL",
	"eye_L_CTRL",
	"eye_R_CTRL",
	"nose_L_CTRL",
	"nose_R_CTRL",
	"cheek_L_CTRL",
	"cheek_R_CTRL",

	"CIRC_press",
	"CIRC_rollIn",
	"CIRC_narrowWide",
	"CIRC_clench",
	"CIRC_backFwd",
	"CIRC_nasolabialFurrowL",
	"CIRC_nasolabialFurrowR",
	"CIRC_smileR",
	"CIRC_smileL",
	"CIRC_openSmileR",
	"CIRC_openSmileL",
	"CIRC_frownR",
	"CIRC_frownL",
	"CIRC_scream",
	"CIRC_lipsNarrowWideR",
	"CIRC_lipsNarrowWideL",
	"CIRC_lipsStretchOpenR",
	"CIRC_lipsStretchOpenL",
	"CIRC_chinWrinkle",
	"CIRC_chinRaiseUpper",
	"CIRC_chinRaiseLower",
	"CIRC_closeOuterR",
	"CIRC_closeOuterL",
	"CIRC_puckerR",
	"CIRC_puckerL",
	"CIRC_oh",
	"CIRC_funnelUR",
	"CIRC_funnelDR",
	"CIRC_mouthSuckUR",
	"CIRC_mouthSuckDR",
	"CIRC_pressR",
	"CIRC_pressL",
	"CIRC_dimpleR",
	"CIRC_dimpleL",
	"CIRC_suckPuffR",
	"CIRC_suckPuffL",
	"CIRC_lipBite",
	"CIRC_funnelUL",
	"CIRC_funnelDL",
	"CIRC_mouthSuckUL",
	"CIRC_mouthSuckDL",
	"CIRC_blinkL",
	"CIRC_squeezeR",
	"CIRC_squeezeL",
	"CIRC_blinkR",
	"CIRC_openCloseDR",
	"CIRC_squintInnerUR",
	"CIRC_squintInnerDR",
	"CIRC_openCloseUR",
	"CIRC_openCloseDL",
	"CIRC_squintInnerUL",
	"CIRC_squintInnerDL",
	"CIRC_openCloseUL",
	"CIRC_thinThick_C",
	"CIRC_thinThick_D",
	"CIRC_stickyLips_E",
	"CIRC_thinThick_B",
	"CIRC_thinThick_F",
	"CIRC_thinThick_H",
	"CIRC_thinThick_G",
	"CIRC_stickyLips_A",
	
	"lowerLip_L_CTRL",
	"lowerLip_C_CTRL",
	"lowerLip_R_CTRL",
	"lipCorner_R_CTRL",
	"upperLip_C_CTRL",
	"upperLip_L_CTRL",
	"lipCorner_L_CTRL",
	"upperLip_R_CTRL",
	"eyelidUpperOuter_L_CTRL",
	"eyelidUpperInner_L_CTRL",
	"eyelidLowerOuter_L_CTRL",
	"eyelidLowerInner_L_CTRL",
	"eyelidLowerOuter_R_CTRL",
	"eyelidLowerInner_R_CTRL",
	"eyelidUpperOuter_R_CTRL",
	"eyelidUpperInner_R_CTRL"	
	) 
	
neutralFrame = 0f
poseArray = #(--frame, --poseName, jsPos
-- 	#(10f, "W"),[0,1,0],

)


createdJoysticks = #()
	
neutralPoseData = #(
	#(), --js Name
	#() --neutral position
	)
	
posedPoseData = #(
	#(), --jsName
	#(), --posed position
	#(), --poseName
	#() --neutralPose
	)

neutralJoysticks = #()
neutralPositions = #()
	
posedJoysticks = #()
posedPositions = #()
	
blankJoysticks = #() --used to store which joysticks havent moved in the anim so we can give them a dummy expression so faceinit can override it
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	

fn cleanExistingControllers = --clean items in joystickArray of existing controllers
(
	for i in joystickArray do 
	(
		obj = getNodeByName i
		
		if obj != undefined do 
		(
			thisCnt = obj.position.controller
			contCount = thisCnt.count
			
			if contCount > 2 do 
			(
				for cont = contCount to 3 by -1 do 
				(
					thisCnt.delete cont
				)
			)
		)
	)
)	
	
fn convertToEditMesh obj = --convert the baserobject from edit poly to edit mesh due to the exporter being bobbins
(
	objModCount = obj.modifiers.count
	addModifier obj (Edit_Mesh ()) before:objModCount 
	maxOps.CollapseNodeTo obj (objModCount +1) off
)	
	
fn loadFaceFXAnim = 
(

	currsel = selection as array
	clearSelection()
	for js in joystickArray do 
	(
		thisJs = getNodeByName js
		selectMore thisJs
	)

		

			debugprint ("attempting to load facial "+selectedAnim)
			debugprint ("Mapping file facial = "+faceanimmappingfile )
		
		LoadSaveAnimation.loadAnimation selectedanim $ relative:false insertTime:0f --useMapFile:true mapFileName:faceanimmappingfile  
)	

fn buildDummyExpressions blankNodeArray = 
(
	bnArrayCount = blankNodeArray.count
	
-- 	for obj in blankNodeArray do 
	for obj = 1 to bnArrayCount do
	(
		expObj = getNodeByName blankNodeArray[obj]
		
		if expObj.name == "jaw_CTRL" do (break())
		
		--first off add the dummyController
		pContCount = expObj.position.controller.count
		
		foundExistingController = false
		
		for c = 1 to pContCount do 
		(
			cName = expObj.position.controller.getName c 
			
			if cName ==	"FFX_Dummy" do 
			(
				foundExistingController = true
			)
		)
		
		if foundExistingController == false then
		(
			expObj.position.controller.available.controller = Position_XYZ ()
			
			expObj.position.controller.setName 3 "FFX_Dummy"
		)
		
		scalarNames = #("open_pose")
		scalarConts = #("$open_pose.position.controller[2][2].controller")

-- 		expString = #("if (ScaleOffset < 0, 0,0)")
		expString = ("open_pose - open_pose")
		
		for i = 1 to 2 do 
		(
-- 			expCont = expObj.position.controller[3][i].controller
			
			expCont = (".position.controller[3]["+(i as string)+"].controller")
			
			generateExpression expObj expCont scalarNames scalarConts expString
		)
	)
)

fn buildPoseData  =
(
	loadFaceFXAnim()
	
	sliderTime = neutralFrame

	--now store the neutral positions
	for i = 1 to joystickArray.count do 
	(		
		joystick = getNodeByName joystickArray[i]
		neutralPose = in coordsys Parent joystick.position 
			
		append neutralPoseData[1] joystick.name
		append neutralPoseData[2] neutralPose

	)
	
	for p = 1 to poseArray.count do 
	(
		sliderTime = poseArray[p][1] as time
		poseName = poseArray[p][2] 

		--now for each joystick if the joystick position is different to the neutral pose we need to store the position
		
		for i = 1 to joystickArray.count do 
		(
			joystick = getNodeByName joystickArray[i]
			
			--print ("Joysitck = "+(joystick.name))
			
			neutralPose = undefined
			for js = 1 to neutralPoseData[1].count do 
			(
				if joystick.name == neutralPoseData[1][js] do 
				(
					--print (joystick.name+" matched "+neutralPoseData[1][js]+" setting NeutralPose to "+(neutralPoseData[2][js] as string))
					neutralPose = neutralPoseData[2][js]
				)
			)
			
			posedPos = in coordsys parent joystick.position
			if posedPos != neutralPose then
			(
				--print ((posedPos as string)+" != "+(neutralPose as string)+" for "+joystick.name+" on pose "+(poseName as string))
				append posedPoseData[1] joystick.name
				append posedPoseData[2] posedPos
				append posedPoseData[3] poseName
				append posedPoseData[4] neutralPose
			)
			else
			(
				--print ((posedPos as string)+" MATCHES "+(neutralPose as string)+" for "+joystick.name+" on pose "+(poseName as string)+" so skipping")
				
				--ok so we need to record this node so we can give it a blankj expression
				
-- 				append blankJoysticks joystick.name
			)
		)
	)
	
	--now we need to look at all the joysticks store in posedData[1]
	--and compare that to the total joysticks so we can see 
	--which ones need a dummy expression adding
	
	usedJs = #()
	
	for js = 1 to joystickArray.count do
	(
		masterJs = joystickArray[js]

		for p = 1 to posedPoseData[1].count do 
		(
			thisJs = posedPoseData[1][p]
			
			if thisJs == masterJs do 
			(
				appendIfUnique usedJs thisJs
			)
		)
	)
	
	--now we need to make a copy of joystickArray
	--and subtract all the usedJs nodes from it
	
	for js = 1 to joystickArray.count do --did this cos deepcopy was being weird!
	(
		append blankJoysticks joystickArray[js]
	)
	
	for i = blankJoysticks.count to 1 by -1 do 
	(
		masterJs = blankJoysticks[i]
		
		for js = 1 to usedJs.count do 
		(
			thisJs = usedJs[js]
			
			if thisJs == masterJs do 
			(
				print ("removing "+thisJs)
				deleteItem blankJoysticks i
			)
		)
	)
-- 	
-- 	print "BlankNodeArray:"
-- 	
-- 	for bn = 1 to blankJoysticks.count do (print blankJoysticks[bn])
-- 	
-- 	break()
	

	-- format ("------------------------------------------------------------------------------------------"+"\n")
	-- format ("------------------------------------------------------------------------------------------"+"\n")
	-- 	for i = 1 to posedPoseData.count do 
	-- 	(
	-- 		print "---------"
	-- 		print ("Joystick: "+(posedPoseData[1][i] as string))
	-- 		print ("posedPos: "+(posedPoseData[2][i] as string))
	-- 		print ("poseName: "+(posedPoseData[3][i] as string))
	-- 		print ("neutralPos: "+(posedPoseData[4][i] as string))
	-- 	)
	-- format ("------------------------------------------------------------------------------------------"+"\n")
	-- format ("------------------------------------------------------------------------------------------"+"\n")	
)

fn poseDrivingPose joyStickName neutralPos posedPos poseName = 
(
	joystick = getnodebyName joystickName

	xPosNeutral = neutralPos[1]
	yPosNeutral = neutralPos[2]	
	
	if poseName == "L_Blink" do
	(
		poseName = "CTRL_L_Blink"
	)
	if poseName == "R_Blink" do
	(
		poseName = "CTRL_R_Blink"
	)
	
	poseDriveJoystick = getNodeByName poseName
	
	if poseDriveJoystick == undefined do 
	(
		print ("Couldnt find pose drive joystick "+poseName)
		break()
	)

	xPosPosed = posedPos[1]
	yPosPosed = posedPos[2]		

	if xPosPosed != xPosNeutral do --ok js has moved so we need to build X expression
	(
		contCount = joyStick.Position.Controller.count
		joyStick.Position.Controller.Available.controller = Position_XYZ()
		
		joyStick.Position.Controller.setName (contCount + 1) poseName
		
		contCount = joyStick.Position.Controller.count --recount controllers
		
		expObj = joystick
		expCont = (".Position.controller["+(contCount as string)+"][1].controller")
		
		scalarNames = #(poseName)

		scalarConts = #("$"+poseDriveJoystick.name+".Position.Controller[2][2].controller")
		expString = ((xPosPosed as string)+" * "+scalarNames[1])
		
		generateExpression expObj expCont scalarNames scalarConts expString
	)
	
	if yPosPosed != yPosNeutral do -- ok we need to build Y Expression
	(
		
		contCount = joyStick.Position.Controller.count
		
		contExists = false
		for c = 1 to contCount do 
		(
			thisName = joyStick.Position.Controller.getName
			if thisName == poseName do 
			(
				contExists = true
			)
		)
		
		if contExists == false then
		(
			joyStick.Position.Controller.Available.controller = Position_XYZ()
			joyStick.Position.Controller.setName (contCount + 1) poseName
			
			contCount = joyStick.Position.Controller.count
		)
		
		expObj = joystick
		expCont = (".Position.controller["+(contCount as string)+"][2].controller")
		
		scalarNames = #(poseName)

		scalarConts = #("$"+poseDriveJoystick.name+".Position.Controller[2][2].controller")
		expString = ((yPosPosed as string)+" * "+scalarNames[1])
		
		generateExpression expObj expCont scalarNames scalarConts expString		
	)
)


fn create1DSlider sliderCount mySliderName sliderPos = 
(
	-- call the joystick creation function with the UI data
	jsPrefix = ""
	
	if mySliderName == "L_Blink" then
	(
		jsPrefix = "CTRL_"
	)
	else
	(
		if mySliderName == "R_Blink" then
		(
			jsPrefix = "CTRL_"
		)
	)
	jstemp = mySliderName as string
	jsname = replacespace jstemp
	jstyle = 2
	jpos = sliderPos -- the default position on screen. This can overridden later, perhaps by mouse click.

	
	
	
	-- Create a joystick slider and select it
		
	seljoy = createJoystick (jsPrefix+jsname) jstyle jpos 1  -- This can also be copied and pasted to a script to auto build a controller
	
	--now we need to tag shit
	thisJsNode = getNodeByName (jsPrefix+jsname)

	thisJsNodeParent = thisJsNode.parent --get the rectangle
	appendIfUnique createdJoysticks thisJsNodeParent --store the joystick so we can use it later	
	
	tagName = jsName

	setUserPropbuffer thisJsNode ("tag= "+(jsname)+"\r\n"+"translateTrackType=TRACK_VISEMES"+"\r\n"+"exportTrans = true")	
)

fn createFaceFXSliders = 
(
	--firstly make the 1d joysticks
	
	existingFaceFXNode = getNodeByName "FaceFX"

	layer = layermanager.getLayerFromName "FacialUI_Visuals" 

	if existingFaceFXNode != undefined do 
	(
		layer.addNode existingFaceFXNode 
	)
	
	if existingFaceFXNode == undefined do 
	(
		if ((progBar != undefined) and (progBar.isDisplayed)) do
		(destroyDialog progBar)
		CreateDialog progBar width:300 Height:30
		progBar.prog.color = [050,200,050] 

		--first off make sure we have the ui layers needed.
		layerNameArray = #(
			"FacialUI_Visuals",
			"FacialUI_Controls" 
			)
		
		for lay in layerNameArray do
		(
			--need to check if the layer "FacialUI_Visuals" exists
			layerExists = false

			lCount = layerManager.count 
			
			for lyr = 0 to lCount by -1 do
			(
				thisLayer = layerManager.getLayer lyr
				thisLayerName = thisLayer.name
				if thisLayerName == lay do 
				(
					layerExists = true
				)
			)
			
			if layerExists == false do 
			(
				LayerManager.newLayerFromName lay
			)
		)
		
		for sl = 1 to poseArray.count do
		(
			create1DSlider 1 poseArray[sl][2] poseArray[sl][3]
			
			layer = layermanager.getLayerFromName "FacialUI_Visuals" 
			rectJs = getNodeByName ("RECT_"+poseArray[sl][2])
			textJs = getNodeByName ("TEXT_"+poseArray[sl][2])
			if rectJs != undefined then
			(
				layer.Addnode rectJS
					--print ("Added "+rectJS.name+" to "+"FacialUI_Visuals")
				layer.Addnode textJS
					--print ("Added "+textJs.name+" to "+"FacialUI_Visuals")
				
				controlJs = getNodeByName poseArray[sl][2]
				layer = layermanager.getLayerFromName "FacialUI_Controls" 
				layer.addNode controlJS
					--print ("Added "+textJs.name+" to "+"FacialUI_Controls")		
			)
			else
			(
				print ("WARNING! "+"couldn't find "+("RECT_"+poseArray[sl][2]))
			)

			progBar.prog.value = (100.*sl/poseArray.count)
		)
		
		--now create the facefx text parent obj
		faceFXText = text size:0.25 kerning:0 leading:0 transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.215069,0,-0.0633061]) isSelected:on
		faceFXText.text = "FaceFX"
		faceFXText.render_displayRenderMesh = true
		faceFXText.thickness = 0.01
		faceFXText.sides = 3
		faceFXText.name = "FaceFX"	
		in coordsys world faceFXText.pos = [0.45,0,0.1]
		
		--now link all the ffx rectangles to the faceFX object

		for o in createdJoysticks do
		(
			o.parent = faceFXText
		)	
		
		faceFXText.scale = [0.3,0.3,0.3]
		in coordsys world faceFXText.pos = [0.6,0.0,1.8]
		
		layer = layermanager.getLayerFromName "FacialUI_Visuals" 
		layer.addNode faceFXText 		
		
		
	)
	
	(destroyDialog progBar)	
)



fn parsePoseFile input_name = 
(
	fileData = #(
		)
		
	inputData = #()	
	
	if input_name != undefined then
	(
		f = openfile input_name
		  while not eof f do
		  (
		   append inputData (filterstring (readLine f) "\n")
		  )
		  close f
	)
	print "File parsed."
	
	jPos = [-0.35,-0.1,0]	
	
	for i = 1 to inputData.count do 
	(
		tmpArr = #(1,2,3)
		
		jPos = [(jPos[1]+0.1),jpos[2],jPos[3]]
		--ok now we need to filterstring based on the " " and split into two arrays
		
		--#("open", 10, [0.1,0,0]),
		--pose name then frame
		
		flt = filterString inputData[i][1] " "
		
		if flt[1] == "CTRL_L_Blink" then
		(
			--ok temp strip off CTRL_
			flt[1] = (substring flt[1] 6 -1)
		)
		else
		(
			if flt[1] == "CTRL_R_Blink" then
			(
				--ok temp strip off CTRL_
				flt[1] = (substring flt[1] 6 -1)
			)			
		)
		
		tmpArr[1] = flt[2] --pose Name
		tmpArr[2] = flt[1] --pose Frame
		tmpArr[3] = jPos --joystick position
		
		append poseArray tmpArr
	)


)

fn runFacefxThreeLGen =
(
	if selectedAnim == undefined do
	(
		selectedAnim = getOpenFileName caption:"Pose Animation" types:"Pose Anima (*.xaf)|*.xaf|All Files (*.*)|*.*|"
	)
	
	if selectedAnim != undefined then
	(

		filein (RsConfigGetWildWestDir() + "/script/3dsMax/Characters/Rigging/3Lateral/removeFaceFx.ms")
		
		filein (RsConfigGetWildWestDir() + "/script/3dsMax/Characters/Rigging/3Lateral/renameOnFaceControlTrackTypes.ms")
		
		masterStart = timestamp()
		
		fileToSync = getFiles selectedAnim
		WWP4vSync fileToSync
		
	-- 	messagebox ("Uncomment sync")
		fileToSync = getFiles facePoseTextFile
		WWP4vSync fileToSync 

		cleanExistingControllers()
		
		parsePoseFile facePoseTextFile

		createFaceFXSliders()

		buildPoseData()
		
		print ("Pose data gen completed...")
		
		
		if ((progBar != undefined) and (progBar.isDisplayed)) do
		(destroyDialog progBar)
		CreateDialog progBar width:300 Height:30
		progBar.prog.color = [050,100,050] 		

		--print (posedPoseData[1].count as string)

		for js in joystickArray do --convert the base objects to edit mesh thanks to our awesome tool chain
		(
			thisJoyStick = getNodeByName js
			convertToEditMesh thisJoyStick
		)

		for i = 1 to posedPoseData[1].count do
		(
			joystickName = posedPoseData[1][i]
			
			posedPos = posedPoseData[2][i]
			
			poseName = posedPoseData[3][i]
			
			--print ("____"+poseName+"______")
			
			neutralPos = undefined
			--now we need to find the corresponding joysticks neutral pose
			

			
			for j = 1 to neutralPoseData[1].count do 
			(
				if neutralPoseData[1][j] == joystickName do
				(
					neutralPos = neutralPoseData[2][j]
				)
			)
			
	-- 		print ("Neutral pos: "+(neutralPos as string))
	-- 		print ("Posed pos: "+(posedPos as string))
		
			if neutralPos != posedPos do 
			(
				--print ("joyStickName  = "+(joyStickName as string))
				--print ("neutralPos = "+(neutralPos  as string))
				--print ("posedPos = "+(posedPos  as string))
				--print ("poseName = "+(poseName as string))

				poseDrivingPose joyStickName neutralPos posedPos poseName
			)
			
			progBar.prog.value = (100.*i/posedPoseData[1].count)
		)

		buildDummyExpressions blankJoysticks
		
		(destroyDialog progBar)
		
		format "Done!"
		
		masterEnd = timeStamp()
		
		ng = ((masterEnd - masterStart) / 1000.0)
		proctime =  ("Processing took "+(ng as string)+" seconds\n")
		
		--now get the joysticks tagged
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/3Lateral/ThreeLateral_tagCustAttrGui.ms")
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/3Lateral/fixFaceFXBlinkTags.ms")
		
		print proctime
		messagebox ("Done!"+"\n"+proctime) beep:true
	)
	else
	(
		print ("No anim selected. Stopping.")
	)
)

runFacefxThreeLGen()