custAttrMappingArray = #( --1 = name of attrGui object, 2 = name of object the attr is on, 3 = name of item in the expr/xml
#("CIRC_pressR", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_pressR" ),
#("CIRC_press", "tongueInOut_CTRL", "tongueInOut_CTRL_c_tongueInOut_CTRL_press" ),
#("CIRC_rollIn", "tongueRoll_CTRL", "tongueRoll_CTRL_c_tongueRoll_CTRL_rollIn" ),
#("CIRC_narrowWide", "tongueInOut_CTRL", "tongueInOut_CTRL_c_tongueInOut_CTRL_narrowWide" ),
#("CIRC_clench", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_clench" ),
#("CIRC_backFwd", "jaw_CTRL" , "jaw_CTRL_c_jaw_CTRL_backFwd" ),
#("CIRC_nasolabialFurrowL","mouth_CTRL" , "mouth_CTRL_c_mouth_CTRL_nasolabialFurrowL" ),
#("CIRC_nasolabialFurrowR", "mouth_CTRL" , "mouth_CTRL_c_mouth_CTRL_nasolabialFurrowR" ),
#("CIRC_smileR", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_smileR" ),
#("CIRC_smileL", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_smileL" ),
#("CIRC_openSmileR", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_openSmileR" ),
#("CIRC_openSmileL", "mouth_CTRL" , "mouth_CTRL_c_mouth_CTRL_openSmileL" ),
#("CIRC_frownR", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_frownR" ),
#("CIRC_frownL", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_frownL" ),
#("CIRC_scream", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_scream" ),
#("CIRC_lipsNarrowWideR", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_lipsNarrowWideR" ),
#("CIRC_lipsNarrowWideL", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_lipsNarrowWideL" ),
#("CIRC_lipsStretchOpenR", "mouth_CTRL" , "mouth_CTRL_c_mouth_CTRL_lipsStretchOpenR" ),
#("CIRC_lipsStretchOpenL", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_lipsStretchOpenL" ),
#("CIRC_chinWrinkle", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_chinWrinkle" ),
#("CIRC_chinRaiseUpper", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_chinRaiseUpper" ),
#("CIRC_chinRaiseLower", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_chinRaiseLower" ),
#("CIRC_closeOuterR", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_closeOuterR" ),
#("CIRC_closeOuterL", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_closeOuterL" ),
#("CIRC_puckerR", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_puckerR" ),
#("CIRC_puckerL", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_puckerL" ),
#("CIRC_oh", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_oh" ),
#("CIRC_funnelUR", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_funnelUR" ),
#("CIRC_funnelDR", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_funnelDR" ),
#("CIRC_mouthSuckUR","mouth_CTRL" , "mouth_CTRL_c_mouth_CTRL_mouthSuckUR" ),
#("CIRC_mouthSuckDR", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_mouthSuckDR" ),

#("CIRC_pressL", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_pressL" ),
#("CIRC_dimpleR", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_dimpleR" ),
#("CIRC_dimpleL", "mouth_CTRL" , "mouth_CTRL_c_mouth_CTRL_dimpleL" ),
#("CIRC_suckPuffR", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_suckPuffR" ),
#("CIRC_suckPuffL", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_suckPuffL" ),
#("CIRC_lipBite", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_lipBite" ),
#("CIRC_funnelUL","mouth_CTRL" , "mouth_CTRL_c_mouth_CTRL_funnelUL" ),
#("CIRC_funnelDL", "mouth_CTRL" , "mouth_CTRL_c_mouth_CTRL_funnelDL" ),
#("CIRC_mouthSuckUL", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_mouthSuckUL" ),
#("CIRC_mouthSuckDL", "mouth_CTRL", "mouth_CTRL_c_mouth_CTRL_mouthSuckDL" ),
#("CIRC_blinkL", "eye_C_CTRL" , "eye_C_CTRL_c_eye_C_CTRL_blinkL" ),
#("CIRC_squeezeR", "eye_C_CTRL", "eye_C_CTRL_c_eye_C_CTRL_squeezeR" ),
#("CIRC_squeezeL", "eye_C_CTRL" , "eye_C_CTRL_c_eye_C_CTRL_squeezeL" ),
#("CIRC_blinkR", "eye_C_CTRL", "eye_C_CTRL_c_eye_C_CTRL_blinkR" ),
#("CIRC_openCloseDR", "eye_C_CTRL", "eye_C_CTRL_c_eye_C_CTRL_openCloseDR" ),
#("CIRC_squintInnerUR", "eye_C_CTRL", "eye_C_CTRL_c_eye_C_CTRL_squintInnerUR" ),
#("CIRC_squintInnerDR", "eye_C_CTRL", "eye_C_CTRL_c_eye_C_CTRL_squintInnerDR" ),
#("CIRC_openCloseUR", "eye_C_CTRL" , "eye_C_CTRL_c_eye_C_CTRL_openCloseUR" ),
#("CIRC_openCloseDL", "eye_C_CTRL", "eye_C_CTRL_c_eye_C_CTRL_openCloseDL" ),
#("CIRC_squintInnerUL", "eye_C_CTRL", "eye_C_CTRL_c_eye_C_CTRL_squintInnerUL" ),
#("CIRC_squintInnerDL", "eye_C_CTRL", "eye_C_CTRL_c_eye_C_CTRL_squintInnerDL" ),
#("CIRC_openCloseUL", "eye_C_CTRL", "eye_C_CTRL_c_eye_C_CTRL_openCloseUL" ),



#("CIRC_stickyLips_A", "lipCorner_R_CTRL", "lipCorner_R_CTRL_c_lipCorner_R_CTRL_stickyLips" ),
#("CIRC_stickyLips_E", "lipCorner_L_CTRL", "lipCorner_L_CTRL_c_lipCorner_L_CTRL_stickyLips" ),

#("CIRC_thinThick_B", "upperLip_R_CTRL", "upperLip_R_CTRL_c_upperLip_R_CTRL_thinThick" ),
#("CIRC_thinThick_C", "upperLip_C_CTRL", "upperLip_C_CTRL_c_upperLip_C_CTRL_thinThick" ),
#("CIRC_thinThick_D", "upperLip_L_CTRL", "upperLip_L_CTRL_c_upperLip_L_CTRL_thinThick" ),
#("CIRC_thinThick_F", "lowerLip_L_CTRL", "lowerLip_L_CTRL_c_lowerLip_L_CTRL_thinThick" ),
#("CIRC_thinThick_G", "lowerLip_C_CTRL", "lowerLip_C_CTRL_c_lowerLip_C_CTRL_thinThick" ),
#("CIRC_thinThick_H", "lowerLip_R_CTRL", "lowerLip_R_CTRL_c_lowerLip_R_CTRL_thinThick" )
)

regJoysticks = #(
"outerBrow_L_CTRL",
"cheek_L_CTRL",
"mouth_CTRL",
"jaw_CTRL",
"tongueMove_CTRL",
"tongueRoll_CTRL",
"tongueInOut_CTRL",
"nose_R_CTRL",
"innerBrow_L_CTRL",
"cheek_R_CTRL",
"innerBrow_R_CTRL",
"eye_C_CTRL",
"eye_L_CTRL",
"eye_R_CTRL",
"nose_L_CTRL",
"outerBrow_R_CTRL"
)

fn createFaceInitSelectionSet = 
(
	selectionSetName = "*FaceInit"
	nodesToAdd = #()

	for obj in regJoysticks do 
	(
		i = getNodeByName obj
		
		if i != undefined do 
		(
			appendIfUnique nodesToAdd i
		)
	)
	
	for obj = 1 to custAttrMappingArray.count do
	(
		i = getNodeByName custAttrMappingArray[obj][1]
		
		if i != undefined do 
		(
			appendIfUnique nodesToAdd i
		)
	)
		
		expSet = selectionSets[selectionSetName]
			tmpObjs = #()
		
		if expSet != undefined then
		(		
			for ss = 1 to expSet.count do
			(
				append tmpObjs expSet[ss]
			)
		)
		else
		(
			expSet = #()
		)
		
		for ts = 1 to nodesToAdd.count do
		(
			appendIfUnique tmpObjs nodesToAdd[ts]
			debugPrint ("adding "+nodesToAdd[ts].name+" to "+selectionSetName+" selection set")
		)
		
		deleteItem selectionSets selectionSetName
		
		selectionSets[selectionSetName] = tmpObjs
	
)

fn tagAttrGui = 
(
	
	
	for attr = 1 to custAttrMappingArray.count do 
	(
		obj = getNodeByName custAttrMappingArray[attr][1]
		
		if obj != undefined do 
		(
			setUserPropBuffer obj ("tag = "+custAttrMappingArray[attr][3]+"\r\n"+"translateTrackType=TRACK_FACIAL_CONTROL")
			print ("Tagged "+obj.name)
		)
	)
	
	
)

fn tagRegJoysticks = 
(
	for joystick in regJoysticks do 
	(
		joy = getNodeByName joystick
		
		if joy != undefined do 
		(
			setUserPropBuffer joy ("translateTrackType=TRACK_FACIAL_TRANSLATION")
			print ("Tagged "+joy.name)
		)
	)
)



tagAttrGui()

tagRegJoysticks()

createFaceInitSelectionSet()