filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-- filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Includes\\FN_Rigging.ms")	
filein (theWildWest + "script/3dsMax/_config_files/Wildwest_header.ms")

filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_Rigging.ms")


fn addCutsceneBlushSlider = 
(
	bnPosX = 0.0
	bnPosY = 0.0
	bnPosZ = 0.0
	
	bName = "cutsceneBlush_OFF"
	jstyle = 2
	jpos = [bnPosX, bnPosY, bnPosZ]
	scaleit = 0.25
	createdNode = RSTA_createJoystick bName jstyle jpos scaleit	
	
	controlNodePar = getNodeByName "faceControls_OFF"
	
	if controlNodePar != undefined do 
	(
		createdNodePar = createdNode.parent
		createdNodePar.parent = controlNodePar
		
		in coordsys parent createdNodePar.position = [0.0,0.0,-4.5]
	)
	
	setUserProp createdNode "tag" createdNode.name --add the bone tag (id)
	
	--track 50, id 0, type 2
	--add the type which needs to be track 2
-- 	setuserProp createdNode 
	--add track which should be 50
	setUserProp createdNode "translateTrackType" "TRACK_FACIAL_TINTING"
	--enableTranslation dof
	setUserProp createdNode "exportTrans" "true"
	
	


)

addCutsceneBlushSlider()