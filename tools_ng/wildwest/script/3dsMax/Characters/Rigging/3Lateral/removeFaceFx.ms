jsArr = #(
"mouth_CTRL",
"jaw_CTRL",
"tongueMove_CTRL",
"tongueRoll_CTRL",
"tongueInOut_CTRL",
"CIRC_thinThick_G",
"innerBrow_L_CTRL",
"outerBrow_L_CTRL",
"innerBrow_R_CTRL",
"eye_C_CTRL",
"eye_L_CTRL",
"eye_R_CTRL",
"nose_L_CTRL",
"nose_R_CTRL",
"cheek_L_CTRL",
"cheek_R_CTRL",
"CIRC_press",
"CIRC_rollIn",
"CIRC_narrowWide",
"CIRC_clench",
"CIRC_backFwd",
"CIRC_nasolabialFurrowL",
"CIRC_nasolabialFurrowR",
"CIRC_smileR",
"CIRC_smileL",
"CIRC_openSmileR",
"CIRC_openSmileL",
"CIRC_frownR",
"CIRC_frownL",
"CIRC_scream",
"CIRC_lipsNarrowWideR",
"CIRC_lipsNarrowWideL",
"CIRC_lipsStretchOpenR",
"CIRC_lipsStretchOpenL",
"CIRC_chinWrinkle",
"CIRC_chinRaiseUpper",
"CIRC_chinRaiseLower",
"CIRC_closeOuterR",
"CIRC_closeOuterL",
"CIRC_puckerR",
"CIRC_puckerL",
"CIRC_oh",
"CIRC_funnelUR",
"CIRC_funnelDR",
"CIRC_mouthSuckUR",
"CIRC_mouthSuckDR",
"CIRC_pressR",
"CIRC_pressL",
"CIRC_dimpleR",
"CIRC_dimpleL",
"CIRC_suckPuffR",
"CIRC_suckPuffL",
"CIRC_lipBite",
"CIRC_funnelUL",
"CIRC_funnelDL",
"CIRC_mouthSuckUL",
"CIRC_mouthSuckDL",
"CIRC_blinkL",
"CIRC_squeezeR",
"CIRC_squeezeL",
"CIRC_blinkR",
"CIRC_openCloseDR",
"CIRC_squintInnerUR",
"CIRC_squintInnerDR",
"CIRC_openCloseUR",
"CIRC_openCloseDL",
"CIRC_squintInnerUL",
"CIRC_squintInnerDL",
"CIRC_openCloseUL",
"CIRC_thinThick_C",
"CIRC_thinThick_D",
"CIRC_stickyLips_E",
"CIRC_thinThick_B",
"CIRC_thinThick_F",
"CIRC_thinThick_H",
"CIRC_stickyLips_A",
"outerBrow_R_CTRL",

"lowerLip_L_CTRL",
"lowerLip_C_CTRL",
"lowerLip_R_CTRL",
"lipCorner_R_CTRL",
"upperLip_C_CTRL",
"upperLip_L_CTRL",
"lipCorner_L_CTRL",
"upperLip_R_CTRL",
"eyelidUpperOuter_L_CTRL",
"eyelidUpperInner_L_CTRL",
"eyelidLowerOuter_L_CTRL",
"eyelidLowerInner_L_CTRL",
"eyelidLowerOuter_R_CTRL",
"eyelidLowerInner_R_CTRL",
"eyelidUpperOuter_R_CTRL",
"eyelidUpperInner_R_CTRL"
	)

facefxNodes = #(
	"FaceFX",
	"RECT_IH",
	"TEXT_IH",
	"IH",
	"RECT_Squint",
	"TEXT_Squint",
	"Squint",
	"RECT_Brows_Down",
	"TEXT_Brows_Down",
	"Brows_Down",
	"RECT_Brow_Up_R",
	"TEXT_Brow_Up_R",
	"Brow_Up_R",
	"RECT_Brow_Up_L",
	"TEXT_Brow_Up_L",
	"Brow_Up_L",
	"RECT_R_Blink",
	"TEXT_R_Blink",
	"CTRL_R_Blink",
	"RECT_L_Blink",
	"TEXT_L_Blink",
	"CTRL_L_Blink",
	"RECT_tRoof_pose",
	"TEXT_tRoof_pose",
	"tRoof_pose",
	"RECT_tTeeth_pose",
	"TEXT_tTeeth_pose",
	"tTeeth_pose",
	"RECT_tBack_pose",
	"TEXT_tBack_pose",
	"tBack_pose",
	"RECT_wide_pose",
	"TEXT_wide_pose",
	"wide_pose",
	"RECT_FV",
	"TEXT_FV",
	"FV",
	"RECT_PBM",
	"TEXT_PBM",
	"PBM",
	"RECT_ShCh",
	"TEXT_ShCh",
	"ShCh",
	"RECT_W_pose",
	"TEXT_W_pose",
	"W_pose",
	"RECT_open_pose",
	"TEXT_open_pose",
	"open_pose"
)
	
sliderTime = 0f

for obj = faceFxNodes.count to 1 by -1 do 
(
	thisObj = getNodebyName faceFxNodes[obj]
	
	if thisObj != undefiend do 
	(
		delete thisObj
	)
)

for o in jsArr do 
(
	obj = getnodebyname o
	
	cCnt = obj.position.controller.count 
	
	if cCnt > 2 do 
	(
		for i = cCnt to 3 by -1 do 
		(
			obj.position.controller.delete i
		)
	)
)

print "Facefx removed"