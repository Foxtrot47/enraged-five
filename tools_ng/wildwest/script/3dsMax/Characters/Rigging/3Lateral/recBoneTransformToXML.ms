--script to record the rotations, translations and scales of every single bone in the selection
--June 2011
--Matt Rennie

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

dataForFile = #(	) --all queried data gets stored here

ddListArray = #(
	"-----",
	"World",
	"Parent"
	)
	
transformType = ddListArray[1]	
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn recTransforms transformType = 
(
	dataForFile = #()
	openStr = ("<?xml version="+"\""+"1.0"+"\""+" ?>"+"\r"+"<joints>")
	append dataForFile openStr
	
	selA = selection as array
	for i = 1 to (selA.count +1) do
	(
		if i < (selA.count + 1) then
		(
			if transformType == ddListArray[2] then
			(
				bNam = sela[i].name
				bPos = in coordsys world selA[i].position
				bPosX = bPos[1]
				bPosY = bPos[2]
				bPosZ = bPos[3]
				
				bRot = ((in coordsys world selA[i].rotation as eulerAngles) as string)
				bRotSplit = (filterString bRot " " )
				bRotX = bRotSplit[2]
				bRotY = bRotSplit[3]
				bRotZa = bRotSplit[4]
				bRotZSplit = (filterstring bRotZa[1] ")")
				bRotZ = bRotZSplit[1]
					
				bSca = in coordsys parent selA[i].scale
				bScaX = bSca[1]
				bScaY = bSca[2]
				bScaZ = bSca[3]
					
				newLineString = ("\r"+"\t"+"<joint name="+"\""+(bNam as string)+"\""+" rx="+"\""+(bRotX as string)+"\""+" ry="+"\""+(bRotY as string)+"\""+" rz="+"\""+(bRotZ as string)+"\""+" tx="+"\""+(bPosX as string)+"\""+" ty="+"\""+(bPosY as string)+"\""+" tz="+"\""+(bPosZ as string)+"\""+"/>")
					
				endStr = ""
				append dataForFile (newLineString+endStr)
			)
			else
			(
				bNam = sela[i].name
				bPos = in coordsys Parent selA[i].position
				bPosX = bPos[1]
				bPosY = bPos[2]
				bPosZ = bPos[3]
				
				bRot = ((in coordsys Parent selA[i].rotation as eulerAngles) as string)
				bRotSplit = (filterString bRot " " )
				bRotX = bRotSplit[2]
				bRotY = bRotSplit[3]
				bRotZa = bRotSplit[4]
				bRotZSplit = (filterstring bRotZa[1] ")")
				bRotZ = bRotZSplit[1]
					
				bSca = in coordsys Parent selA[i].scale
				bScaX = bSca[1]
				bScaY = bSca[2]
				bScaZ = bSca[3]
					
				newLineString = ("\r"+"\t"+"<joint name="+"\""+(bNam as string)+"\""+" rx="+"\""+(bRotX as string)+"\""+" ry="+"\""+(bRotY as string)+"\""+" rz="+"\""+(bRotZ as string)+"\""+" tx="+"\""+(bPosX as string)+"\""+" ty="+"\""+(bPosY as string)+"\""+" tz="+"\""+(bPosZ as string)+"\""+"/>")
					
				endStr = ""
				append dataForFile (newLineString+endStr)				
			)
		)
		else
		(
-- 			ok we can add the closing bits of xml here
			finalStr = ("\r"+"</joints>")
			append dataForFile finalStr
		)
	)
	--now output the data in the array to a file
	
	
		output_name = getSaveFileName caption:"Bone output xml" types:"BoneXML (*.xml)|*.xml|All Files (*.*)|*.*|"

		if output_name != undefined then 
		(
			output_file = createfile output_name		

			for i = 1 to dataForFile.count do
			(
				format ((dataForFile[i])) to:output_file
			)
			
			close output_file
				--edit output_name --opens the file in a maxscript window so it can be checked.
			
		)--end if	
		print "Saving Complete."
)



------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if ((queryBonesGUI != undefined) and (queryBonesGUI.isDisplayed)) do
	(destroyDialog queryBonesGUI)

rollout queryBonesGUI "Highclass Facial"
(
	dropDownList ddTransType "Transform Type" items:ddListArray
	button btnQuerySelection "Query Selected" width:110

	on queryBonesGUI open do
	(
		transformType = ddListArray[1]
		Print ("transformType defaulting to "+ddListArray[1])
	)
		
	on ddTransType selected i do
	(
		transformType = ddListArray[i]
		Print ("You picked "+transformType)		
	)
	
	on btnQuerySelection pressed do
	(
		if transformType != ddListArray[1] then
		(
			clearListener()
			if selection.count != 0 then
			(
				recTransforms transFormType
			)
			else
			(
				messagebox "Please select the nodes to query and try again." beep:true
			)
		)
		else
		(
			messagebox "Please select transform Type and try again." beep:true	
		)
	)
)

CreateDialog queryBonesGUI width:125 pos:[1450, 100] 