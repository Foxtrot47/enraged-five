-- sela = selection as array

-- for a in sela do 
-- (
-- origName = a.name
-- newName = (substring a.name 6 -1)
-- format ("#("+"\""+origName+"\""+","+"\""+newName+"\""+"),"+"\n")
-- )

nameMapArray = #(
#("CIRC_press","press"),
#("CIRC_rollIn","rollIn"),
#("CIRC_narrowWide","narrowWide"),
#("CIRC_clench","clench"),
#("CIRC_backFwd","backFwd"),
#("CIRC_nasolabialFurrowL","nasolabialFurrowL"),
#("CIRC_nasolabialFurrowR","nasolabialFurrowR"),
#("CIRC_smileR","smileR"),
#("CIRC_smileL","smileL"),
#("CIRC_openSmileR","openSmileR"),
#("CIRC_openSmileL","openSmileL"),
#("CIRC_frownR","frownR"),
#("CIRC_frownL","frownL"),
#("CIRC_scream","scream"),
#("CIRC_lipsNarrowWideR","lipsNarrowWideR"),
#("CIRC_lipsNarrowWideL","lipsNarrowWideL"),
#("CIRC_lipsStretchOpenR","lipsStretchOpenR"),
#("CIRC_lipsStretchOpenL","lipsStretchOpenL"),
#("CIRC_chinWrinkle","chinWrinkle"),
#("CIRC_chinRaiseUpper","chinRaiseUpper"),
#("CIRC_chinRaiseLower","chinRaiseLower"),
#("CIRC_closeOuterR","closeOuterR"),
#("CIRC_closeOuterL","closeOuterL"),
#("CIRC_puckerR","puckerR"),
#("CIRC_puckerL","puckerL"),
#("CIRC_oh","oh"),
#("CIRC_funnelUR","funnelUR"),
#("CIRC_funnelDR","funnelDR"),
#("CIRC_mouthSuckUR","mouthSuckUR"),
#("CIRC_mouthSuckDR","mouthSuckDR"),
#("CIRC_pressR","pressR"),
#("CIRC_pressL","pressL"),
#("CIRC_dimpleR","dimpleR"),
#("CIRC_dimpleL","dimpleL"),
#("CIRC_suckPuffR","suckPuffR"),
#("CIRC_suckPuffL","suckPuffL"),
#("CIRC_lipBite","lipBite"),
#("CIRC_funnelUL","funnelUL"),
#("CIRC_funnelDL","funnelDL"),
#("CIRC_mouthSuckUL","mouthSuckUL"),
#("CIRC_mouthSuckDL","mouthSuckDL"),
#("CIRC_blinkL","blinkL"),
#("CIRC_squeezeR","squeezeR"),
#("CIRC_squeezeL","squeezeL"),
#("CIRC_blinkR","blinkR"),
#("CIRC_openCloseDR","openCloseDR"),
#("CIRC_squintInnerUR","squintInnerUR"),
#("CIRC_squintInnerDR","squintInnerDR"),
#("CIRC_openCloseUR","openCloseUR"),
#("CIRC_openCloseDL","openCloseDL"),
#("CIRC_squintInnerUL","squintInnerUL"),
#("CIRC_squintInnerDL","squintInnerDL"),
#("CIRC_openCloseUL","openCloseUL"),
#("CIRC_thinThick_C","thinThick_C"),
#("CIRC_thinThick_D","thinThick_D"),
#("CIRC_stickyLips_E","stickyLips_E"),
#("CIRC_thinThick_B","thinThick_B"),
#("CIRC_thinThick_F","thinThick_F"),
#("CIRC_thinThick_H","thinThick_H"),
#("CIRC_thinThick_G","thinThick_G"),
#("CIRC_stickyLips_A","stickyLips_A")
)

fn remapAttrNames = 
(
	for nom = 1 to nameMapArray.count do 
	(
		circObj = getNodeByName nameMapArray[nom][1]
		newName = (nameMapArray[nom][2]+"_CA")
		circObj.name = newName
	)
)

remapAttrNames()