--rename tracktypes

trackJoysticks = #(
		"lowerLip_L_CTRL",
	"lowerLip_C_CTRL",
	"lowerLip_R_CTRL",
	"lipCorner_R_CTRL",
	"upperLip_C_CTRL",
	"upperLip_L_CTRL",
	"lipCorner_L_CTRL",
	"upperLip_R_CTRL",
	"eyelidUpperOuter_L_CTRL",
	"eyelidUpperInner_L_CTRL",
	"eyelidLowerOuter_L_CTRL",
	"eyelidLowerInner_L_CTRL",
	"eyelidLowerOuter_R_CTRL",
	"eyelidLowerInner_R_CTRL",
	"eyelidUpperOuter_R_CTRL",
	"eyelidUpperInner_R_CTRL"	
	)
	
fn renameTrackTypes =
(
	for js in trackJoysticks do 
	(
		obj = getNodeByName js
		
		existingUdp = getuserPropBuffer obj
		
		existingUdp = filterString existingUdp "\r\n"
		
		print (obj.name+" udp count: "+(existingUdp.count as string))
			
		newUdp = (existingUdp[1]+"\r\n"+"exportTrans=true"+"\r\n"+"translateTrackType=TRACK_FACIAL_TRANSLATION")
			
		setUserPropBuffer obj newUdp
	)
)	

renameTrackTypes()