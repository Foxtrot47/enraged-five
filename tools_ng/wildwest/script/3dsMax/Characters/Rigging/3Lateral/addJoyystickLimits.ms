--add3LateralJoystickLimits
--Matt Rennie
--Jan 2012
--script to re-apply limits to 3Lateral off face controls

----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------

threeLateraljsArray = #(
	--jsName, -x, +x, -y, +y 
	#("mouth_CTRL", -1, 1, -1, 1 ),
	#("jaw_CTRL", -1, 1, -1, 0 ),
	#("tongueMove_CTRL", -1, 1, -1, 1 ),
	#("tongueRoll_CTRL", -1, 1, -1, 1 ),
	#("tongueInOut_CTRL", 0, 0, -1, 1 ),
	#("outerBrow_R_CTRL", 0, 0, -1, 1 ),
	#("innerBrow_L_CTRL", -1, 0, -1, 1 ),
	#("outerBrow_L_CTRL", 0, 0, -1, 1 ),
	#("innerBrow_R_CTRL", -1, 0, -1, 1 ),
	#("eye_C_CTRL", -1, 1, -1, 1 ),
	#("eye_L_CTRL", -1, 1, -1, 1 ),
	#("eye_R_CTRL", -1, 1, -1, 1 ),
	#("nose_L_CTRL", -1, 1, -1, 1 ),
	#("nose_R_CTRL", -1, 1, -1, 1 ),
	#("cheek_L_CTRL", 0, 0, 0, 1 ),
	#("cheek_R_CTRL", 0, 0, 0, 1 )
	)

----------------------------------------------------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------------------------------
	
fn addFloatLimits = 
(
	for i = 1 to threeLateraljsArray.count do 
	(
		jsName = threeLateraljsArray[i][1]
		jsMinX = threeLateraljsArray[i][2]
		jsMaxX = threeLateraljsArray[i][3]
		jsMinY = threeLateraljsArray[i][4]
		jsMaxY = threeLateraljsArray[i][5]
		jsMaxZ = 0.0
		jsMinZ = 0.0
		
		jsObj = getNodeByName jsName
		if jsObj != undeifned do 
		(
			if (classof jsObj.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller) != float_limit do
			(
				jsObj.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller = float_limit ()	
				--print ("Float Limit added to X on"+jsName)
			)
			jsObj.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller.upper_limit = jsMaxX
			jsObj.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller.lower_limit = jsMinX

			if (classof jsObj.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller) != float_limit do
			(
				jsObj.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller = float_limit ()	
				--print ("Float Limit added to Y on"+jsName)
			)
			jsObj.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller.upper_limit = jsMaxY
			jsObj.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller.lower_limit = jsMinY		

			if (classof jsObj.pos.controller.Zero_Pos_XYZ.controller.Z_Position.controller) != float_limit do
			(
				jsObj.pos.controller.Zero_Pos_XYZ.controller.Z_Position.controller = float_limit ()	
				--print ("Float Limit added to Z on"+jsName)
			)
			jsObj.pos.controller.Zero_Pos_XYZ.controller.Z_Position.controller.upper_limit = jsMaxZ
			jsObj.pos.controller.Zero_Pos_XYZ.controller.Z_Position.controller.lower_limit = jsMinZ					
			
			--print ("Float Limit configured on "+jsName)
		)
	)
)

addFloatLimits()