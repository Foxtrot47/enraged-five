blinkCtrls = #(
	$CTRL_L_Blink,
	$CTRL_R_Blink
)

fn fixBlinkTags = 
(
	for o in blinkCtrls do 
	(
		tagName = ("tag = "+o.name)
		
		tagLine2 = "translateTrackType=TRACK_VISEMES"
		tagLine3 = "exportTrans = true"

		setUserPropBuffer o (tagName+"\r\n"+tagLine2+"\r\n"+tagLine3)
		
		format ("Tags reset on "+o.name+"\n")
	)
)

fixBlinkTags()