--script to connect alpha plane custom attribute gui to custom Attributes

--Matt Rennie

--Feb 2012

----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------

attrObjs = #( --objects with attrs used to connect to alpha attr gui. If id 2 = undefined this gets the attr from the definition if not undefined its cos this attr name is shared on multiple objects.
	#("eye_C_CTRL", undefined),
	#("mouth_CTRL", undefined),
	#("tongueInOut_CTRL", undefined),
	#("tongueRoll_CTRL", undefined),
	#("jaw_CTRL", undefined),
	#("lipCorner_L_CTRL", "CIRC_stickyLips_A"),
	#("upperLip_R_CTRL", "CIRC_thinThick_B" ),
	#("upperLip_C_CTRL", "CIRC_thinThick_C"),
	#("upperLip_L_CTRL", "CIRC_thinThick_D"),
	#("lipCorner_L_CTRL", "CIRC_stickyLips_E"),
	#("lowerLip_L_CTRL", "CIRC_thinThick_F"),
	#("lowerLip_C_CTRL", "CIRC_thinThick_G"),
	#("lowerLip_R_CTRL", "CIRC_thinThick_H")	
	)

errorArray = #() --used to store nodes which didnt work for print at end.	
	
	
----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------

	

fn connectAlphaGUIToAttrs = 
(

	
	print "------------------"
	
	for i = 1 to attrObjs.count do 
	(
		obj = getNodeByName attrObjs[i][1]
		
		if attrObjs[i][2] == undefined then 
		(
			
			
			attrDef = custAttributes.get obj.modifiers[#Attribute_Holder] 1
			
			attrNames = getPropNames attrDef
				
			for a = 1 to attrNames.count do 
			(
				print ("Current attr = "+(attrNames[a] as string))
				attName = (attrNames[a] as string)
				attLength = attName.count
				attributeName = (substring attName 1 (attLength - 3))
				guiObj = getNodeByName ("CIRC_"+attributeName)
				
				if guiObj != undefined then
				(
					--now we need to wire this attr to the gui object
					
					--so now we build the path to the Attribute
					
					attrStr = ("$"+obj.name+".modifiers[#Attribute_Holder]."+attrNames[a])

					thisAttr = execute attrStr
					
					if thisAttr != undefined then
					(
						
						
						fromStr = ("$"+obj.name+".modifiers[#Attribute_Holder]."+obj.name+"_A"+"[#"+attributeName+"_CA] ")
						toStr = ("$CIRC_"+attributeName+".pos.controller.Zero_Pos_XYZ.controller.X_Position.controller[#Limited_Controller__Bezier_Float]") 
						endStr = ("\""+"Limited_Controller__Bezier_Float"+"\""+" "+"\""+attributeName+"_CA"+"\"")
						
						--first we need to ensure that the from and to are bezier floats
						
						execute ("$"+obj.name+".modifiers[#Attribute_Holder]."+obj.name+"_A"+"[#"+attributeName+"_CA].controller = bezier_float ()")
						execute ("$CIRC_"+attributeName+".pos.controller.Zero_Pos_XYZ.controller.X_Position.controller[1].controller = bezier_float()")
						
						wireStr = ("paramWire.connect2way "+fromStr + toStr+endStr)
						print (wireStr)
						execute wireStr
					)
					else
					(
						print ("Attribute path generation failed for "+(attrObjs[i][1]+".modifiers[#Attribute_Holder]."+attrNames[a]))
					)
				)
				else
				(
					print ("Couldnt find "+("CIRC_"+attributeName))
				)
			)
		)
		else
		(
			guiObj = getNodeByName attrObjs[i][2]	
			guiObjNameLength = attrObjs[i][2].count
			startChar = 6
			trimmedLength = (guiObjNameLength - (1 + startChar))
			attributeName = (substring guiObj.name startChar trimmedLength)		
			
			fromStr = ("$"+obj.name+".modifiers[#Attribute_Holder]."+obj.name+"_A"+"[#"+attributeName+"_CA] ")
			toStr = ("$"+guiObj.name+".pos.controller.Zero_Pos_XYZ.controller.X_Position.controller[#Limited_Controller__Bezier_Float] "+"\""+"Limited_Controller__Bezier_Float"+"\""+" "+"\""+attributeName+"_CA"+"\"")
			
			--first we need to ensure that the from and to are bezier floats				
			execute ("$"+obj.name+".modifiers[#Attribute_Holder]."+obj.name+"_A"+"[#"+attributeName+"_CA].controller = bezier_float ()")
			execute ("$"+guiObj.name+".pos.controller.Zero_Pos_XYZ.controller.X_Position.controller[1].controller = bezier_float ()")
			
			wireStr = ("paramWire.connect2way "+fromStr+toStr)

			print (wireStr)
			execute wireStr				
		)
	)
	
	
	print "Attrs connected"
)



connectAlphaGUIToAttrs()

-- lipThicknessConnect()