--script to connect animated normal sliders to a component material

joystickArrayA = #(
	$'Head_WA-000', 
	$'Head_WA-001', 
	$'Head_WA-002', 
	$'Head_WA-003', 
	$'Head_WA-004', 
	$'Head_WA-005', 
	$'Head_WA-006', 
	$'Head_WA-007', 
	$'Head_WA-008', 
	$'Head_WA-009', 
	$'Head_WA-010', 
	$'Head_WA-011'
)
joystickArrayB = #(
	$'Head_WB-000', 
	$'Head_WB-001', 
	$'Head_WB-002', 
	$'Head_WB-003', 
	$'Head_WB-004', 
	$'Head_WB-005', 
	$'Head_WB-006', 
	$'Head_WB-007', 
	$'Head_WB-008', 
	$'Head_WB-009', 
	$'Head_WB-010', 
	$'Head_WB-011'
	)

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
	
fn generateExpressionIM expObj expCont scalarObjects scalarNames scalarConts expString=
(
	/*
	need to pass in:
		1) expObj =  name of object the expression is for
		2) expCont = controller the expression is on (as a string)
		3) scalarObjects = name of objects used as drivers as an array of string 
		4) scalarNames = name of scalars - do this as an array (of strings) so we cna loop through multiple scalars
		5) scalarConts = controller the scalars point to - do this as array (of strings) with an entry for each item in the scalarNames array
		6) expString = full expression string
	*/


	expString = ("("+expString+")")
	
	debugPrint "----------------------------------------------------------------------------"
	debugPrint "\r\n"
	debugPrint ("Creating expression on "+expObj+". Scalar of "+(scalarObjects as string))
	debugPrint ("Using expression:")
	debugPrint expString
	debugPrint "\r\n"
	debugPrint "----------------------------------------------------------------------------"
		
-- 	fcStr = ( "$"+expObj+expCont) --rebuild the string so it can be used below	
-- 		fcStr = (expObj+expCont)
-- 		local newfcStr = execute fcStr
		newfcStr = expCont
	debugPrint ("newfcStr = "+(fcStr as string))
	
	if scalarNames.count != 0 do
	(
		for i = 1 to scalarNames.count do --this should loop through the array of scalars and their controllers and create a scalar for each
		(
			
			SVN = scalarNames[i]
			debugPrint ("SVN = "+(SVN as string))
			
			scalContStr = (scalarConts[i] as string)
				debugPrint ("scalCont  = "+scalContStr)
				scalCont = execute scalContStr
				
			
			newfcStr.AddScalarTarget SVN scalCont --add scalar pointing to the controller of the scalar object
		)
	)	
	debugPrint ("setting expression to :\r\n"+expString)

		newfcStr.SetExpression expString
			
			debugPrint ("Expression created on "+expObj+" for "+expCont)
			debugPrint "```````````````````````````````````````````````````````"
	
)	
	
	
	
fn connectCompMat = 
(
-- 	meditMaterials[24] = $.material --copy the selected material to slot 24 so we can read it
-- 	local headMat = meditMaterials[1]
-- 	local headMat = meditmaterials[1].materialList[1]
	
	local headMat = undefined
	for i = 1 to 24 do
	(
		if meditMaterials[i].name == "Head_RAGE" do
		(
			headMat = meditMaterials[i].materialList[1]
		)
	)	
	
	if headMat != undefined then
	(
		local compMap = headMat.bumpMap.normal

		for maskLayerNum = 1 to 2 do 
		(
			local maskLayerMap = compMap.mask[maskLayerNum + 1]
			
			joystickArray = undefined
			
			if maskLayerNum == 1 then
			(
				joystickArray =  joystickArrayB
			)
			else
			(
				joystickArray =  joystickArrayA
			)
			
			for subMaskNum = 0 to 11 do 
			(
				/*
				need to pass in:
					1) expObj =  name of object the expression is for
					2) expCont = controller the expression is on (as a string)
					3) scalarObjects = name of objects used as drivers as an array of string 
					4) scalarNames = name of scalars - do this as an array (of strings) so we cna loop through multiple scalars
					5) scalarConts = controller the scalars point to - do this as array (of strings) with an entry for each item in the scalarNames array
					6) expString = full expression string
				*/
				expObj = (maskLayerMap as string)
					print ("expObj = "+expObj)
				expCont = ((exprForMAXObject maskLayerMap.opacity[subMaskNum + 1].controller) as string)
					print ("expCont = "+expCont )
				scalarObjects = #(joystickArray[subMaskNum + 1].name)
					print ("scalarObjects =  "+scalarObjects as string)
				scalarNames = #("'"+joystickArray[subMaskNum + 1].name+"'"+"_Ypos")
					print ("scalarNames = "+scalarNames as string)
				scalarConts = #((exprForMAXObject joystickArray[subMaskNum + 1].position.controller[2].Y_position.controller) as string)
					print ("scalarConts = "+scalarConts  as string)
						print "-------------------"
				
				expString = (scalarNames[1])
				generateExpressionIM expObj expCont scalarObjects scalarNames scalarConts expString
			)
		)
	)
	else
	(
		messagebox ("WARNING! Couldn't find a valid Head_RAGE shader in the material editor.") beep:true
	)
)

clearListener()
connectCompMat()