
-- wire_control_bone variables:
-- The controlling joystick circle, the control axis and then a normalising multiplier 
-- Then the bone to control and the axis to rotate it on, plus a rotation limit (use a minus to reverse direction)

rollout progBar "Progress..." --rollout for progress bar
(
	progressbar prog pos:[10,10] color:red
)

fn initProgBar = --fn to call initialision of a progress bar used throughout rigging tools
(
	if ((progBar != undefined) and (progBar.isDisplayed)) do
	(destroyDialog progBar)
	CreateDialog progBar width:300 Height:30
)


fn FreezeTransform = 	--freezes transforms. Copied from the max macroscript as this does not like being run from other scripts.
( 		
	local Obj = Selection as array 	
	suspendEditing which:#motion
	for i = 1 to Obj.count do 		
	( 
		Try
		(	
			local CurObj = Obj[i] 	

			if classof CurObj.rotation.controller != Rotation_Layer do
			(

				-- freeze rotation		
				CurObj.rotation.controller = Euler_Xyz() 		
				CurObj.rotation.controller = Rotation_list() 			
				CurObj.rotation.controller.available.controller = Euler_xyz() 		
				
				/* "Localization on" */  
			
				CurObj.rotation.controller.setname 1 "Frozen Rotation" 		
				CurObj.rotation.controller.setname 2 "Zero Euler XYZ" 		
			
				/* "Localization off" */  
				
				CurObj.rotation.controller.SetActive 2 		
			)
			if classof CurObj.position.controller != Position_Layer do
			(

				-- freeze position
				CurObj.position.controller = Bezier_Position() 			
				CurObj.position.controller = position_list() 			
				CurObj.position.controller.available.controller = Position_XYZ() 	
	
				/* "Localization on" */  
						
				CurObj.position.controller.setname 1 "Frozen Position" 	
				CurObj.position.controller.setname 2 "Zero Pos XYZ" 			
				
				/* "Localization off" */  
				
				CurObj.position.controller.SetActive 2 		
	
				-- position to zero
				CurObj.Position.controller[2].x_Position = 0
				CurObj.Position.controller[2].y_Position = 0
				CurObj.Position.controller[2].z_Position = 0
			)
			if classof CurObj.scale.controller != Scale_Layer do
			(

				-- freeze scale
				CurObj.scale.controller = Bezier_Scale() 			
				CurObj.scale.controller = scale_list() 			
				CurObj.scale.controller.available.controller = ScaleXYZ() 	
	
				/* "Localization on" */  
						
				CurObj.scale.controller.setname 1 "Frozen Scale" 	
				CurObj.scale.controller.setname 2 "Zero Scale XYZ" 			
				
				/* "Localization off" */  
				
				CurObj.scale.controller.SetActive 2 		
	
				-- scale to zero
				CurObj.scale.controller[2].x_Scale = 100
				CurObj.scale.controller[2].y_Scale = 100
				CurObj.scale.controller[2].z_Scale = 100
			)				
			
		)	
		/* "Localization on" */  
				
		Catch( messagebox ("A failure occurred while freezing "+CurObj.name+"'s transform." title:"Freeze Transform")
			print ("A failure occurred while freezing "+CurObj.name+"'s transform." )
			)
				
		/* "Localization off" */  	
	)
	resumeEditing which:#motion
)










-- ********************************************************************************************************************* 
-- *********************************************************************************************************************
-- Joysticks and controlling bones with them



fn joystickClamp obj cont limitVal axis mult=
(
	print ("obj = "+(obj as string))
	print ("cont = "+(cont as string))
	print ("limitVal = "+(limitVal as string))
				
			newfcStr = undefined
			SVN = undefined
				
---------------------------------------------------------------------------------------------	
			fcStr = ("$'"+obj.name+"'.pos.controller.Zero_Pos_XYZ.controller."+axis+"_Position.controller."+cont+".controller = Float_Expression ()")

				print ("_______SETTING "+fcStr+"_________________")
				execute fcStr
			fcStr =  ("$'"+obj.name+"'.pos.controller.Zero_Pos_XYZ.controller."+axis+"_Position.controller."+cont+".controller ")
			print ("fcStr = "+fcStr)
				newfcStr = execute fcStr
				showProperties newfcStr
				
			SVN = "LimitValue"
		
		if axis != "Z" then
		(
			
		--TURN THIS INTO AN IF EXPRESSION SO WE CAN CLAMP WIDTH BELOW A CERTAIN THRESHOLD.
			if (limitVal.value /2) == obj.radius then
			(
				newfcStr.SetExpression	("("+"0.0"+" /2)"+mult)--set the expression here
			)
			else
			(
				newfcStr.AddScalarTarget SVN limitVal --add scalar pointing to the translation of the joystick object
				newfcStr.SetExpression	("("+SVN+" /2)"+mult)--set the expression here
			)
		)
		else
		(
			newfcStr.SetExpression	("("+"0.0"+" /2)"+mult)--set the expression here
		)

			print "```````````````````````````````````````````````````````"

)

-- fn that builds a joystick - Rick Stirling
-- Takes the name, style (Square =1, Vertical=2, Horizontal=3), screen position and the eventual scale
--   createJoystick "TestJoy"  2 jpos 1
fn createJoystick jsn jstyle jpos scaleit =
(
	--QUICK hacky way to ensure we dont have selection locked
	testObj = Circle radius: 1 name: "testCir" wirecolor:(color 20 20 255) transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0,0,0]) 
	select testObj
	selArr = selection as array
	if selArr[1] == testObj then
	(
		
		-- Be sensible and make sure that there are no spaces in the joystick name
		--local jsn = replacespace jsn -- change any spaces to underbars
		
		-- Setup our default sizes
		-- These are the ones that work on our character scale.
		local jsl = conjsl = 2 as float		-- bounding box length
		local jsw = conjsw = 2 as float		-- bounding box width
		local jscor = 0.15 as float 				-- rounded corners
		local jscir = 0.2  as float				-- and the circle size
		local jscap = 0.4 as float				-- caption text size
		--local jstxsp = jsl - 0.3 as float		-- caption text offset
		local jstxsp = jsl - 3.4 as float		-- caption text offset

		
		-- The size of the bounding box will depend on the style chosen, so adjust variables now
		-- Vertical style stick
		if jstyle == 2 then 
		(
			jsw = 0.4 as float
			conjsw=0
		)
		
		-- Horizontal style stick	
		else if jstyle == 3 then 
		(
			jsl = 0.4 as float
			conjsl=0
			jstxsp = jsl + 0.5 as float		-- caption text offset
		)
		
		--lets define the substring values
		--if the jsn begins with ctrl_ then we'll ignore that part of the name
		tempJSN = substring jsn 1 5
		--tempJSN = uppercase tempJSN
		tempJSN = toUpper tempJSN
		if tempJSN == "CTRL_" then
		(
			ss1 = 6
-- 			ss2 = 18
			ss2 = 200
		)
		else
		(
			ss1 = 1
-- 			ss2 = 12
			ss2 = 200
		)
		
		-- Create bounding box for the joystick, keep it selected		 
		jsbox = Rectangle length:jsl width:jsw cornerRadius:jscor position:jpos name: ("RECT_"+( substring (jsn) ss1 ss2)) wirecolor:(color 250 230 100) transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0,0,0]) isselected:on
		jsBox.render_displayRenderMesh = true
		jsBox.thickness = 0.1
		jsBox.sides = 3
			
		-- Add the caption, parent it to the bounding box  
		jscaption = Text text: ( substring (jsn) ss1 ss2) size: jscap wirecolor:(color 20 20 255) name: ("TEXT_" +( substring (jsn) ss1 ss2)) transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0,0,0])
		jsCaption.render_displayRenderMesh = true
		jsCaption.thickness = 0.025
		jsCaption.sides = 3

		jscaption.parent = jsbox
		in coordsys parent jscaption.position = [0,jstxsp,0] 
	--COMMENTED OUT NEXT SECTION AS GOING TO USE LAYERS INSTEAD
		--	freeze jscaption -- I freeze it
		jscaption.showFrozenInGray = off
			
		
		-- Create the circle controller, parent it to the bounding box 
		jsCircle = Circle radius: (jscir) name: (jsn) wirecolor:(color 20 20 255) transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0,0,0]) 
		jsCircle.render_displayRenderMesh = true
		jsCircle.thickness = 0.1
		jsCircle.sides = 3

		jsCircle.parent = jsbox
		in coordsys parent jsCircle.position = [0,0,0]
		
			--NOW FREEZE THE CIRCLE
			select jsCircle
		
				FreezeTransform()
			
			-- I need to limit the controller circle to the bounding box edges. 
			-- Use float limits based on cgtalk ideas
			
			--****************************************************************************
			--need to modify this to use a clamp inside a float expression so we use no wires
			--****************************************************************************
			
			xfl=float_limit()
		-- 	jsCircle.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller = float_limit ()
			jsCircle.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller = xfl
				joystickClamp jsCircle ((#upper_limit) as string) jsbox.baseObject[#width] "X" " * 1"
				joystickClamp jsCircle ((#lower_limit) as string) jsbox.baseObject[#width] "X" " * -1"
				
			yfl=float_limit()
			jsCircle.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller = yfl
				joystickClamp jsCircle ((#upper_limit) as string) jsbox.baseObject[#length] "Y" " * 1"
				joystickClamp jsCircle ((#lower_limit) as string) jsbox.baseObject[#length] "Y" " * -1"
				
			zfl=float_limit()
			jsCircle.pos.controller.Zero_Pos_XYZ.controller.Z_Position.controller = zfl
				joystickClamp jsCircle ((#upper_limit) as string) jsbox.baseObject[#length] "Z" " * 1"
				joystickClamp jsCircle ((#lower_limit) as string) jsbox.baseObject[#length] "Z" " * -1"
				
			-- Box is too big for peds by default, scale down by a factor 20, also take in scaleit factor.
			defaultsize = 0.05
			scale jsbox ([defaultsize,defaultsize,defaultsize] * scaleit)
		-- 	
			jsbox


	)
	else
	(
		messageBox ("Please ensure you do NOT have selection locked.") beep:true
	)
	
	delete testObj
)



