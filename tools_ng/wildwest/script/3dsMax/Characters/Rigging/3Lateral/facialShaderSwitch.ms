-- facialShaderSwitch.ms
--Matt Rennie
--Jan 2012

--tool to switch shaders from rage to standard then import light rig and moving textures around

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if ((shaderSwitchGUI != undefined) and (shaderSwitchGUI.isDisplayed)) do
	(destroyDialog shaderSwitchGUI)


filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

LateralLightRig = (theProjectRoot+"art/peds/3LateralSetup/LightRig.max")
TexturePath = (theProjectRoot+"art/animation/cutscene/Facial/")
GUITexturePath = (theProjectRoot+"art\\animation\\cutscene\\Facial\\!!Characters\\!!GUI\\")
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


fn switchMaterials = --switches materials from rage to standard and tries to repath if possible
(
	
	currentCharName = (FilterString maxFileName ".")
	currentCharName = currentCharName[1]
	currentTexturePath = (maxFilePath+"textures\highres")
	
	newTexturePath = (TexturePath+currentCharName) 
	
	TexTypesToSave = #(
	"Diffuse Texture",
	"Bump Texture",
	"Specular Texture"
	)
	
	alphaShaderTypes = #( --list of rage alpha shader types to test for so we can save opacity maps
		"ped_alpha.sps",
		"ped_hair_cutout_alpha.sps",
		"ped_hair_dithered_alpha_expensive.sps",
		"ped_hair_sorted_alpha_expensive.sps",
		"ped_hair_spiked.sps",
		"ped_default_cutout.sps",
		"ped_decal.sps"
		)
	
	for matid = 1 to sceneMaterials.count do 
	(
-- 		print sceneMaterials[matid].name
		--now loop through the material if its a multimaterial and see if its rage
		if classof sceneMaterials[matID] == MultiMaterial do 
		(
			for realSubID = 1 to sceneMaterials[matid].materialList.count do 
			(
				print "----------------------"
				print ("matID:"+(matID as String)+"subID:"+(realSubID as String))
				if (classof sceneMaterials[matid].materialList[realSubID] as string) == "Rage_Shader" do 
				(
					--ok we need to store the textures then recreate this as a standard material 
					diffTex = undefined
					bumpTex = undefined
					specTex = undefined 	
					alphaTex = undefined
					
					isAlpha = false
					
					shaderType = RstGetShaderName sceneMaterials[matid].materialList[realSubID]
					
					for alph = 1 to alphaShaderTypes.count do 
					(
						if (shaderType as string) == alphaShaderTypes[alph] do 
						(
							isAlpha = true
						)
					)
					
					for x=1 to RstGetVariableCount sceneMaterials[matid].materialList[realSubID] do
					(
						
						tempArray = #()
						varType = RstGetVariableType sceneMaterials[matid].materialList[realSubID] x
						if varType == "texmap" then
						(
							varName = RstGetVariableName sceneMaterials[matid].materialList[realSubID] x
							append tempArray varName
							varValue = RstGetVariable sceneMaterials[matid].materialList[realSubID] x

-- 							print ("varName:"+varName)
-- 							print ("varValue:"+varValue)
							

							
							if varName == TexTypesToSave[1] do 
							(
								diffTex = varValue
								
								if isAlpha == true do 
								(
									alphaTex = varValue
								)
								
								--need to strip down to filename and then find a match in the new textures location
								
								texName = filterstring diffTex "\\"
								
								tCount = texName.count
								
								if TCount != 0 then 
								(
									tName = texName[tCount]
									thisFile = getFiles (newTexturePath+"\\"+tName)

									if thisFile.count != 0 then
									(
										diffTex = thisFile[1]
										if isAlpha == true do 
										(
											alphaTex = thisFile[1]
										)
									)
									else
									(
										print ("Couldn't find Diffuse "+tName+" in "+newTexturePath+" so leaving as is." )
									)
								)
								else
								(
									print ("Couldn't find Diffuse"+" so leaving as is." )
								)
							)
							if varName == TexTypesToSave[2] do 
							(
								bumpTex = varValue
								texName = filterstring bumpTex "\\"
								tCount = texName.count
								
								if TCount != 0 then 
								(
									tName = texName[tCount]
									thisFile = getFiles (newTexturePath+"\\"+tName)

									if thisFile.count != 0 then
									(
										bumpTex = thisFile[1]
									)
									else
									(
										print ("Couldn't find Normal "+tName+" in "+newTexturePath+" so leaving as is." )
									)								
								)
								else
								(
									print ("Couldn't find Normal "+" so leaving as is." )
								)
							)
							if varName == TexTypesToSave[3] do 
							(
								specTex = varValue
								
								texName = filterstring specTex "\\"
								tCount = texName.count
								
								if tCount != 0 then
								(
									tName = texName[tCount]
									thisFile = getFiles (newTexturePath+"\\"+tName)

									if thisFile.count != 0 then
									(
										specTex = thisFile[1]
									)
									else
									(
										print ("Couldn't find Spec "+tName+" in "+newTexturePath+" so leaving as is." )
									)
								)
								else
								(
									print ("Couldn't find Spec "+" so leaving as is." )
								)
							)
							
						)

					)
					--ok now we've stored the textures for this material we can now convert it to a standard and reload those textures
					sceneMaterials[matid].materialList[realSubID] = Standardmaterial ()
					
					if diffTex != undefined do 
					(
						sceneMaterials[matid].materialList[realSubID].diffuseMap = Bitmaptexture fileName:diffTex
					)
					
					if specTex != undefined do 
					(
						sceneMaterials[matid].materialList[realSubID].specularLevelMap = Bitmaptexture fileName:specTex
						sceneMaterials[matid].materialList[realSubID].glossinessMap = Bitmaptexture fileName:specTex
					)
					
					if bumpTex != undefined do 
					(
						sceneMaterials[matid].materialList[realSubID].bumpMap = Normal_Bump ()
						bN = sceneMaterials[matid].materialList[realSubID].bumpMap
						bN.normal_map = Bitmaptexture fileName:bumpTex
					)
					
					if alphaTex != undefined do 
					(
						sceneMaterials[matid].materialList[realSubID].opacityMap = Bitmaptexture fileName:alphaTex
					)
					showTextureMap sceneMaterials[matid].materialList[realSubID] on
				)
			)
		)		
		
	)
	print "Shader switch complete."
)

fn rePathAlphaGui = --repaths alpha gui textures
(
	for sM = 1 to sceneMaterials.count do 
	(
		if classOf sceneMaterials[sm] == MultiMaterial then
		(
			for matId = 1 to sceneMaterials[sM].count do 
			(
				if classOf sceneMaterials[sM].materialList[matId] == Standardmaterial do 
				(
					print ("Material #:"+(sm as string)+" id:"+(matID as string))
					--now we've found a standard we need to check diffuse and alpha for if they are in the initial guiPath
					--if they are we re-poath them
					
					diffFile = sceneMaterials[sM].materialList[matId].diffuseMap
					alphaFile = sceneMaterials[sM].materialList[matId].opacityMap
					
					--now we need to split the string of the filename so we can find the textures folder
					if diffFile != undefined do 
					(
						diffTex = sceneMaterials[sM].materialList[matId].diffuseMap.filename
						
						splitFname = (filterstring diffTex "\\") 
						splitCount = splitFname.count
						diffNameRoot = (splitFname[1])
						diffTexName = splitFname[splitCount]
						for f = 2 to (splitCount - 1) do
						(
							diffNameRoot = (diffNameRoot+"\\"+splitFname[f])
						)

						if diffNameRoot == "X:\gta5\art\peds\3LateralSetup" do 
						(
							sceneMaterials[sM].materialList[matId].diffuseMap = Bitmaptexture fileName:(GUITexturePath+diffTexName)
						)

					)					
					
					if alphaFile != undefined do 
					(
						
						alphaTex = sceneMaterials[sM].materialList[matId].opacityMap.filename						
						
						splitFname = (filterstring diffTex "\\") 
						splitCount = splitFname.count
						alpNameRoot = (splitFname[1])
						alpTexName = splitFname[splitCount]
						for f = 2 to (splitCount - 1) do
						(
							alpNameRoot = (alpNameRoot+"\\"+splitFname[f])
						)			
						
						
						if alpNameRoot == "X:\gta5\art\peds\3LateralSetup" do 
						(
							sceneMaterials[sM].materialList[matId].diffuseMap = Bitmaptexture fileName:(GUITexturePath+alpTexName)
						)			
					)
				)
			)
		)
		else
		(
			if classOf sceneMaterials[sM] == Standardmaterial do 
			(
				--now we've found a standard we need to check diffuse and alpha for if they are in the initial guiPath
				--if they are we re-poath them
				diffTex = undefined 
				alphaTex = undefined
				
				if sceneMaterials[sM].diffuseMap != undefined do 
				(
					diffTex = sceneMaterials[sM].diffuseMap.filename
				)
				
				if sceneMaterials[sM].opacityMap != undefined do 
				(
					alphaTex = sceneMaterials[sM].opacityMap.filename
				)
				
				if difftex != undefined do 
				(
					--now we need to split the string of the filename so we can find the textures folder
					
					splitFname = (filterstring diffTex "\\") 
					splitCount = splitFname.count
					diffNameRoot = (splitFname[1])
					diffTexName = splitFname[splitCount]
					for f = 2 to (splitCount - 1) do
					(
						diffNameRoot = (diffnameRoot+"\\"+splitFname[f])
					)
					
					splitFname = (filterstring diffTex "\\") 
					splitCount = splitFname.count
					alpNameRoot = (splitFname[1])
					alpTexName = splitFname[splitCount]
					for f = 2 to (splitCount - 1) do
					(
						alpNameRoot = (alpNameRoot+"\\"+splitFname[f])
					)			
					
					if diffNameRoot == "X:\gta5\art\peds\3LateralSetup" do 
					(
						sceneMaterials[sM].diffuseMap = Bitmaptexture fileName:(GUITexturePath+diffTexName)
					)
					
					if alpNameRoot == "X:\gta5\art\peds\3LateralSetup" do 
					(
						sceneMaterials[sM].diffuseMap = Bitmaptexture fileName:(GUITexturePath+alpTexName)
					)			
				)
			)	
		)
	)
)

fn moveTextures = --moves textures for this character to NewTexturePath
(
	currentCharName = (FilterString maxFileName ".")
	currentCharName = currentCharName[1]
	currentTexturePath = (maxFilePath+"textures\highres")
	
	newTexturePath = (TexturePath+"!!Characters/"+currentCharName+"_Txt") 
	makeDir newTexturePath all:true
	
	--first delete existing textures from new folder path
	files = getFiles (newTexturePath+"\\*.tga")
	
	for f in files do
	(
		deleteFile f
		print ("deleted "+f)
	)
	

	--now copy the files to the new location	
	files = getFiles (currentTexturePath+"\\*.tga")
	
	for f in files do 
	(
		currentPathLength = currentTexturePath.count
		newFileName = (substring f (currentPathLength+2) 60)
		
		copyFile f (newTexturePath+"\\"+newFileName)
		print ("copied "+f+" to "+(newTexturePath+"\\"+newFileName))
	)

	print "Textures moved to new location"
)

fn importLightRig = 
(
	mergeMaxFile LateralLightRig
	
	--now need to align the LightRig point helper to the skel head Joint_Angle_Deformer
	lightMaster = getNodeByName "LIGHTRIG_MASTER"
	headJoint = getNodeByName "SKEL_Head"
	
	lightMaster.transform = headJoint.transform
	
	hide lightMaster
)

fn disableVertexFlag = --unchecks the vertex colour object property
(
	for o in objects do 
	(
		if o.modifiers[#Skin] != undefined do 
		(
			o.showVertexColors = off
		)
	)
)


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

rollout shaderSwitchGUI "Shader Switch"
(
	
	button importLightBTN "Import Lights" width:160 height:30 tooltip:"Import light and camera rig to scene."
	button copyTexturesBTN "Copy Textures" width:160 height:30 tooltip:"Copy characters textures to outsource folder."
	button repathGUIBTN "GUI Textures" width:160 height:30 tooltip:"Re-path GUI textures to outsource folder."
	button toStdBTN "Convert to Standard" width:160 height:30 tooltip:"Convert all RAGE materials to standard 3DSMax versions"
	
	button doEveryThingBTN "Do it in a one-er!" width:160 height:50 tooltip:"Run the whole process."
	
	on importLightBTN pressed do 
	(
		importLightRig()
		disableVertexFlag()
	)

	on copyTexturesBTN pressed do 
	(
		moveTextures()
	)
	
	on repathGUIBTN pressed do 
	(
		rePathAlphaGui()
	)
	
	on toStdBTN pressed do 
	(
		switchMaterials()
	)
	
	on doEveryThingBTN pressed do 
	(
		importLightRig()
		moveTextures()
		rePathAlphaGui()		
		switchMaterials()
		messagebox "Complete!" beept:true
	)
	
)

CreateDialog shaderSwitchGUI width:175 pos:[1400, 100] 