fn RSTA_correctDoubleParenthesis expStr = 
(
	for i = (expStr.count - 1) to 1 by -1 do 
	(
		if substring expStr i 2 == "((" do
		(
			expStr = replace expStr i 2 "( 0 + ("
		)
	)
	
	for i = (expStr.count - 1) to 1 by -1 do 
	(
		if substring expStr i 2 == "))" do
		(
			expStr = replace expStr i 2 ") + 0)"
		)
	)	

	return expStr
)

fn removeDoubleParentheses = 
(	
	for obj in selection do 
	(
		if classof obj.position.controller == position_list do 
		(
			pC = obj.position.controller.count
			
			for contIndex = 2 to pc do
			(
				cont = obj.position.controller[contIndex]
				
				for axis = 1 to 3 do 
				(
					if ((classof cont[axis].controller) as string) == "Float_Expression" do 
					(
						local theExprStr = cont[axis].controller.getExpression()
						
						--now we need to check if its zero first of all and if so set it to a bexier
						
						if theExprStr == "0" then
						(
							cont[axis].controller = bezier_float()
						)
						else
						(
-- 							if substring theExprStr 1 2 == "((" then
-- 							(
-- 								exprLength = theExprStr.count
-- 								
-- 								if substring theExprStr (exprLength - 2) 2 == "))" do
-- 								(
-- 									exprStr = substring theExprStr 3 (exprLength - 4)
-- 									
-- 									cont[axis].controller.setExpression exprStr
-- 								)
-- 							)
							
							exprStr = RSTA_correctDoubleParenthesis theExprStr
							
							cont[axis].controller.setExpression exprStr
						)
					)
				)
			)
			
			----------------------------
			
			pC = obj.rotation.controller.count
			
			for contIndex = 2 to pc do
			(
				cont = obj.rotation.controller[contIndex]
				
				for axis = 1 to 3 do 
				(
					if ((classof cont[axis].controller) as string) == "Float_Expression" do 
					(
						local theExprStr = cont[axis].controller.getExpression()
						
						--now we need to check if its zero first of all and if so set it to a bexier
						
						if theExprStr == "0" then
						(
							cont[axis].controller = bezier_float()
						)
						else
						(
-- 							if substring theExprStr 1 2 == "((" then
-- 							(
-- 								exprLength = theExprStr.count
-- 								
-- 								if substring theExprStr (exprLength - 2) 2 == "))" do
-- 								(
-- 									exprStr = substring theExprStr 3 (exprLength - 4)
-- 									
-- 									cont[axis].controller.setExpression exprStr
-- 								)
-- 							)
							exprStr = RSTA_correctDoubleParenthesis theExprStr
							
							cont[axis].controller.setExpression exprStr							
						)
					)
				)
			)

	-----------

			if classof obj.scale.controller == scale_list do 
			(
				pC = obj.scale.controller.count
				
				for contIndex = 2 to pc do
				(
					cont = obj.scale.controller[contIndex]
					
					for axis = 1 to 3 do 
					(
						if ((classof cont[axis].controller) as string) == "Float_Expression" do 
						(
							local theExprStr = cont[axis].controller.getExpression()
							
							--now we need to check if its zero first of all and if so set it to a bexier
							
							if theExprStr == "0" then
							(
								cont[axis].controller = bezier_float()
							)
							else
							(
		-- 						if substring theExprStr 1 2 == "((" then
		-- 						(
		-- 							exprLength = theExprStr.count
		-- 							
		-- 							if substring theExprStr (exprLength - 2) 2 == "))" do
		-- 							(
		-- 								exprStr = substring theExprStr 3 (exprLength - 4)
		-- 								
		-- 								cont[axis].controller.setExpression exprStr
		-- 							)
		-- 						)
									exprStr = RSTA_correctDoubleParenthesis theExprStr
									
									cont[axis].controller.setExpression exprStr	
							)
						)
					)
				)
			)
		)

	)
	format "Done."
)

removeDoubleParentheses()