filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())

concatJoints = #( --joints which we do not want to concat scale
	"FACIAL_R_lipUpperAnalog",
	"FACIAL_R_lipLowerAnalog",
	"FACIAL_lipUpperAnalog",
	"FACIAL_L_lipLowerAnalog",
	"FACIAL_L_lipCornerAnalog",
	"FACIAL_L_lipUpperAnalog",
	"FACIAL_R_lipCornerAnalog",
	"FACIAL_lipLowerAnalog",
	
	"FACIAL_R_eyelidUpperInnerAnalog",
	"FACIAL_R_eyelidLowerInnerAnalog",
	"FACIAL_R_eyelidUpperOuterAnalog",
	--"FACIAL_R_eyelidLowerLowerAnalog",
	"FACIAL_R_eyelidLowerOuterAnalog",

	"FACIAL_L_eyelidUpperInnerAnalog",
	"FACIAL_L_eyelidLowerInnerAnalog",
	"FACIAL_L_eyelidUpperOuterAnalog",
	--"FACIAL_L_eyelidLowerLowerAnalog",
	"FACIAL_L_eyelidLowerOuterAnalog"
	)

fn replaceTags o = 
(
		if o.name != "FACIAL_facialRoot" do 
		(
			thisUdp = getUserPropBuffer o
			
			searchTag = "ag = "
			searchEnd = "\\n"
			repTag = ("")
			
			udpCount = filterString thisUdp "\n"
				
			if udpCount != undefined do 
			(	
				newUdp = thisUdp
				
				if udpCount == 1 then --if there wa sonly one line in the udp
				(
					newUdp = repTag
				)
				else
				(
					--ok we need to look through each filtered string element and check for tag and replace if we  find
					for cnt = 1 to udpCount.count do --for each line in the udp
					(
						curUdp = udpCount[cnt]
						startIndex = findString curUdp searchTag
						
						if startIndex != undefined do 
						(
							udpCount[cnt] = repTag
						)
					)
									
					newUdp = ""

					for cnt = 1 to udpCount.count do 
					(
						newUdp = (newUdp+(udpCount[cnt]+"\r\n"))
					)
				)
				setUserPropBuffer o newUdp
			)
		)
)

fn removeJoystickTags = 
(
	for o in objects do 
	(
		tObj = findString o.name "CTRL"
		
		if tObj != undefined do 
		(
			replaceTags o
		)
	)
)	

	
fn scaleFixUpScaleByScale expString = --adds in a -1 to all input values in scale driven by scale expressions due to game scale default being 0 but max being 1
(		
	strA = "> 3., 3.,"
	strARep =  "> (3.0 - 1), (3.0 - 1),"

	strB =  "< 0.333333, 0.333333,"
	strBRep = "< (0.333333 - 1), (0.333333 - 1),"

	strC = "< 1.,"
	strCRep = "< (1.0 - 1),"

	--first off we'll double check this hasnt already been done
	exist = findString expString strARep
	if exist == undefined do 
	(		
		startPoint = findString expString strA	
		if startPoint != undefined do 
		(
			expString = replace expString startPoint (strA.count) strARep
		)
	)		
	
	exist = findString expString strBRep
	if exist == undefined do 
	(			
		startPoint = findString expString strB
		if startPoint != undefined do 
		(		
			expString = replace expString startPoint (strB.count) strBRep	
		)
	)	
	
	exist = findString expString strCRep
	if exist == undefined do 
	(				
		startPoint = findString expString strC
		if startPoint != undefined do 
		(			
			expString = replace expString startPoint (strC.count) strCRep	
		)
	)
		
	return expString
)

fn queryExpression scaleControl obj = 
(
	expString = scaleControl.getExpression()
	
	--now we need to check the ScaleOffset target is present
	
	numC = scaleControl.NumScalars()
	
	foundScaleNode = false
	
	for c = 1 to numc do 
	(
		sName = scaleControl.GetScalarName c
		
		if sName == "ScaleOffset" do 
		(
			foundScaleNode = true
		)
	)
	
	if foundScaleNode == false do 
	(
		scalCont = $ScaleOffset.position.controller[2].Y_position.controller
		scaleControl.AddScalarTarget "ScaleOffset" scalCont		
	)
	
	foundScaleOffset = findString expString "ScaleOffset" 
	
	if foundScaleOffset == undefined do 
	(
		--now we need to add in toi the expression
		expString = replace expString 1 2 ("((ScaleOffset + 1)")
	)
	
	foundScaleOffset = findString expString "((ScaleOffset + 0)" 
	
	if foundScaleOffset != undefined do 
	(
		--now we need to add in toi the expression
		expString = replace expString 1 18 ("((ScaleOffset + 1)")
	)
	
	foundScaleOffset = findString expString "((ScaleOffset)"
	if foundScaleOffset != undefined do 
	(
		--now we need to add in toi the expression
		expString = replace expString 1 14 ("((ScaleOffset + 1)")
	)
	
	expString = scaleFixUpScaleByScale expString
	
	scaleControl.setExpression expString
	
)
	
fn fixScaleOffsets = 
(
	
	filein (theWildWest + "/script/3dsMax/Characters/Rigging/usefulScripts/removeEmptyScalarTargets.ms")
	
	filein (theWildWest + "/script/3dsMax/Characters/Rigging/3Lateral/fixDoublePlusScaleExpressions.ms")
	
	in coordsys parent $ScaleOffset.position.controller[2].Y_position = -1
		
	for o in concatJoints do 
	(
		obj = getNodeByName o
		
		format ("Working on "+obj.name+"\n")
		
		scaleCont = obj.scale.controller[3]
		
		scaleControl = scaleCont.X_Scale.controller
		
		queryExpression scaleControl obj
		
		scaleControl = scaleCont.Y_Scale.controller
		
		queryExpression scaleControl obj

		scaleControl = scaleCont.Z_Scale.controller
		
		queryExpression scaleControl obj	
	)
)	

fixScaleOffsets()

removeJoystickTags()