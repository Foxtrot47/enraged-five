objArray = #(
	"FACIAL_L_lipLowerAnalog",
"FACIAL_lipLowerAnalog",
"FACIAL_R_lipLowerAnalog",
"FACIAL_L_eyelidLowerOuterAnalog",
"FACIAL_L_eyelidLowerInnerAnalog",
"FACIAL_L_eyelidUpperOuterAnalog",
"FACIAL_L_eyelidUpperInnerAnalog",
"FACIAL_R_eyelidUpperOuterAnalog",
"FACIAL_R_eyelidUpperInnerAnalog",
"FACIAL_R_eyelidLowerOuterAnalog",
"FACIAL_R_eyelidLowerInnerAnalog",
"FACIAL_L_lipUpperAnalog",
"FACIAL_lipUpperAnalog",
"FACIAL_L_lipCornerAnalog",
"FACIAL_R_lipUpperAnalog",
"FACIAL_R_lipCornerAnalog"
	)

fn modifyExpr cont =
(
	expr = cont.getExpression()
	
	sVn = "ScaleOffset"
	
	scalCont = $ScaleOffset.position.controller[2].Y_Position.controller
	
	cont.AddScalarTarget SVN scalCont
	
	expr = (expr+" + (ScaleOffset + 1)")
	
	cont.setExpression expr
)

	
for o in objArray do 
(
	obj = getNodeByName o
	
	for i = 1 to 3 do 
	(
		cont = obj.Scale.Controller[3][i].controller
		
		modifyExpr cont 
	)
)	