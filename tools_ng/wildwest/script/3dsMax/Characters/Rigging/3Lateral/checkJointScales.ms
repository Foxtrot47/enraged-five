fn checkScales sela= 
(
	allOk = undefined 
	
	for i = 1 to sela.count do
	(
		thisScale = sela[i].scale
		
		xOk = undefined
		yOk = undefined
		zOk = undefined
		
		if thisScale.x <= 1.05 do
		(
			if thisScale.x >= 0.95 do
			(
				xOK = true
			)
		)
		
		if thisScale.y <= 1.05 do
		(
			if thisScale.y >= 0.95 do
			(
				yOK = true
			)
		)
		
		if thisScale.z <= 1.05 do
		(
			if thisScale.z >= 0.95 do
			(
				zOK = true
			)
		)
				
		if xOk == true then
		(
			if yOk == true then
			(
				if zOk == true then
				(
					allOK = true
				)
				else
				(
					print (sela[i].name+" z Scale = "+(thisScale.z as string))
					allOK = false
				)				
			)
			else
			(
				print (sela[i].name+" y Scale = "+(thisScale.y as string))
					allOK = false
			)
		)
		else
		(
			print (sela[i].name+" X Scale = "+(thisScale.x as string))
				allOK = false
		)
	)
	
	if allok == true do
	(
		print "Scales are ok across selection."
	)
)

sela = selection as array
if sela.count >= 1 do
(
	clearListener()
	checkScales sela
)