facialJoints = #(

	"FACIAL_facialRoot",
	"FACIAL_forehead",
	"FACIAL_jaw",
	"FACIAL_chin",
	"FACIAL_chinSkinBottom",
	"FACIAL_L_chinSkinBottom",
	"FACIAL_R_chinSkinBottom",
	"FACIAL_chinSkinMid",
	"FACIAL_chinSkinTop",
	"FACIAL_L_chinSide",
	"FACIAL_L_chinSkinMid",
	"FACIAL_L_chinSkinTop",
	"FACIAL_L_lipLowerSDK",
	"FACIAL_L_lipLowerAnalog",
	"FACIAL_L_lipLowerThicknessH",
	"FACIAL_L_lipLowerThicknessV",
	"FACIAL_L_underChin",
	"FACIAL_lipLowerSDK",
	"FACIAL_lipLowerAnalog",
	"FACIAL_lipLowerThicknessH",
	"FACIAL_lipLowerThicknessV",
	"FACIAL_R_chinSide",
	"FACIAL_R_chinSkinMid",
	"FACIAL_R_chinSkinTop",
	"FACIAL_R_lipLowerSDK",
	"FACIAL_R_lipLowerAnalog",
	"FACIAL_R_lipLowerThicknessH",
	"FACIAL_R_lipLowerThicknessV",
	"FACIAL_R_underChin",
	"FACIAL_tongueA",
	"FACIAL_L_tongueA",
	"FACIAL_R_tongueA",
	"FACIAL_tongueB",
	"FACIAL_L_tongueB",
	"FACIAL_R_tongueB",
	"FACIAL_tongueC",
	"FACIAL_L_tongueC",
	"FACIAL_R_tongueC",
	"FACIAL_tongueD",
	"FACIAL_L_tongueD",
	"FACIAL_R_tongueD",
	"FACIAL_tongueE",
	"FACIAL_L_tongueE",
	"FACIAL_R_tongueE",
	"FACIAL_underChin",
	"FACIAL_L_cheekInner",
	"FACIAL_L_cheekLower",
	"FACIAL_L_cheekLowerBulge1",
	"FACIAL_L_cheekLowerBulge2",
	"FACIAL_L_cheekOuter",
	"FACIAL_L_cheekOuterSkin",
	"FACIAL_L_ear",
	"FACIAL_L_earLower",
	"FACIAL_L_eyeball",
	"FACIAL_L_eyelidLower",
	"FACIAL_L_eyelidLowerInnerSDK",
	"FACIAL_L_eyelidLowerInnerAnalog",
	"FACIAL_L_eyelashLowerInner",
	"FACIAL_L_eyelidLowerOuterSDK",
	"FACIAL_L_eyelidLowerOuterAnalog",
	"FACIAL_L_eyelashLowerOuter",
	"FACIAL_L_eyelidUpper",
	"FACIAL_L_eyelidUpperInnerSDK",
	"FACIAL_L_eyelidUpperInnerAnalog",
	"FACIAL_L_eyelashUpperInner",
	"FACIAL_L_eyelidUpperOuterSDK",
	"FACIAL_L_eyelidUpperOuterAnalog",
	"FACIAL_L_eyelashUpperOuter",
	"FACIAL_L_eyesackLower",
	"FACIAL_L_eyesackUpperInnerBulge",
	"FACIAL_L_eyesackUpperInnerFurrow",
	"FACIAL_L_eyesackUpperOuterBulge",
	"FACIAL_L_eyesackUpperOuterFurrow",
	"FACIAL_L_foreheadInner",
	"FACIAL_L_foreheadInnerBulge",
	"FACIAL_L_foreheadOuter",
	"FACIAL_L_jawRecess",
	"FACIAL_L_lipCornerSDK",
	"FACIAL_L_lipCornerAnalog",
	"FACIAL_L_lipCornerThicknessLower",
	"FACIAL_L_lipCornerThicknessUpper",
	"FACIAL_L_lipUpperSDK",
	"FACIAL_L_lipUpperAnalog",
	"FACIAL_L_lipUpperThicknessH",
	"FACIAL_L_lipUpperThicknessV",
	"FACIAL_L_masseter",
	"FACIAL_L_nasolabialBulge",
	"FACIAL_L_nasolabialFurrow",
	"FACIAL_L_noseUpper",
	"FACIAL_L_temple",
	"FACIAL_lipUpperSDK",
	"FACIAL_lipUpperAnalog",
	"FACIAL_lipUpperThicknessH",
	"FACIAL_lipUpperThicknessV",
	"FACIAL_nose",
	"FACIAL_L_nostril",
	"FACIAL_L_nostrilThickness",
	"FACIAL_noseLower",
	"FACIAL_L_noseLowerThickness",
	"FACIAL_R_noseLowerThickness",
	"FACIAL_noseTip",
	"FACIAL_R_nostril",
	"FACIAL_R_nostrilThickness",
	"FACIAL_noseBridge",
	"FACIAL_noseUpper",
	"FACIAL_R_cheekInner",
	"FACIAL_R_cheekLower",
	"FACIAL_R_cheekLowerBulge1",
	"FACIAL_R_cheekLowerBulge2",
	"FACIAL_R_cheekOuter",
	"FACIAL_R_cheekOuterSkin",
	"FACIAL_R_ear",
	"FACIAL_R_earLower",
	"FACIAL_R_eyeball",
	"FACIAL_R_eyelidLower",
	"FACIAL_R_eyelidLowerInnerSDK",
	"FACIAL_R_eyelidLowerInnerAnalog",
	"FACIAL_R_eyelashLowerInner",
	"FACIAL_R_eyelidLowerOuterSDK",
	"FACIAL_R_eyelidLowerOuterAnalog",
	"FACIAL_R_eyelashLowerOuter",
	"FACIAL_R_eyelidUpper",
	"FACIAL_R_eyelidUpperInnerSDK",
	"FACIAL_R_eyelidUpperInnerAnalog",
	"FACIAL_R_eyelashUpperInner",
	"FACIAL_R_eyelidUpperOuterSDK",
	"FACIAL_R_eyelidUpperOuterAnalog",
	"FACIAL_R_eyelashUpperOuter",
	"FACIAL_R_eyesackLower",
	"FACIAL_R_eyesackUpperInnerBulge",
	"FACIAL_R_eyesackUpperInnerFurrow",
	"FACIAL_R_eyesackUpperOuterBulge",
	"FACIAL_R_eyesackUpperOuterFurrow",
	"FACIAL_R_foreheadInner",
	"FACIAL_R_foreheadInnerBulge",
	"FACIAL_R_foreheadOuter",
	"FACIAL_R_jawRecess",
	"FACIAL_R_lipCornerSDK",
	"FACIAL_R_lipCornerAnalog",
	"FACIAL_R_lipCornerThicknessLower",
	"FACIAL_R_lipCornerThicknessUpper",
	"FACIAL_R_lipUpperSDK",
	"FACIAL_R_lipUpperAnalog",
	"FACIAL_R_lipUpperThicknessH",
	"FACIAL_R_lipUpperThicknessV",
	"FACIAL_R_masseter",
	"FACIAL_R_nasolabialBulge",
	"FACIAL_R_nasolabialFurrow",
	"FACIAL_R_noseUpper",
	"FACIAL_R_temple",
	"FACIAL_skull",
	"FACIAL_foreheadUpper",
	"FACIAL_L_foreheadUpperInner",
	"FACIAL_L_foreheadUpperOuter",
	"FACIAL_R_foreheadUpperInner",
	"FACIAL_R_foreheadUpperOuter"

)

fn scaleOffsetTargetChange cont obj =
(
	--ok we need to modify the path for the scalarOffset nodes		
	
	expCont = exprForMAXObject cont
	
	contStr = (expCont as string)
	
-- 	contStr = (substring contStr 1 (contStr.count - 11))
	
	print ("contStr: "+contStr)
	
	checkFor = "Limited"
	
	foundLimit = findstring contStr checkFor
	
		
	theScalarCount = cont.NumScalars()
	
	for i = 5 to theScalarCount do
	(			
		thisScalarName = cont.GetScalarName i	
			
		if thisScalarName == "ScaleOffset" do 
		(
			thisScalarTgt = cont.GetScalarTarget thisScalarName asController:true
			print ("thisScalarTgt: "+(thisScalarTgt as string))
			
			realTgt = exprForMAXObject thisScalarTgt

			print ("realTgt: "+(realTgt as string))
			
			foundLimit = findstring realTgt checkFor
			
			if foundLimit != undefined do 
			(
				scalCont = (substring realTgt 1 (realTgt.count - 55))	
					
					print ("ScalCont: "+(scalCont as string))
					
				execStr = (contStr+".SetScalarTarget "+"\""+thisScalarName+"\""+" "+scalCont+".controller") --add scalar pointing to the controller of the scalar object
						
				print ("execStr: "+execStr)
				execute execStr
			)
		)
	)


)

fn plusPlusFixup cont bname = --take the supplied controller and do a replace on any strings of '+ +' with '+'
(
	nonConcatJoints = #( --joints which we do not want to concat scale
		"FACIAL_R_lipUpperAnalog",
		"FACIAL_R_lipLowerAnalog",
		"FACIAL_lipUpperAnalog",
		"FACIAL_L_lipLowerAnalog",
		"FACIAL_L_lipCornerAnalog",
		"FACIAL_L_lipUpperAnalog",
		"FACIAL_R_lipCornerAnalog",
		"FACIAL_lipLowerAnalog",
		
		"FACIAL_R_eyelidUpperInnerAnalog",
		"FACIAL_R_eyelidLowerInnerAnalog",
		"FACIAL_R_eyelidUpperOuterAnalog",
		"FACIAL_R_eyelidLowerLowerAnalog",

		"FACIAL_L_eyelidUpperInnerAnalog",
		"FACIAL_L_eyelidLowerInnerAnalog",
		"FACIAL_L_eyelidUpperOuterAnalog",
		"FACIAL_L_eyelidLowerLowerAnalog"

		)	
	expString = cont.getExpression()
		
	newExpString = substituteString expString "+ +" "+"
	
	newExpString = substituteString newExpString "+  +" "+"
	
	--now we need to change the scaleOffset to be (scaleOffset + 1)
	
	lookFor = "ScaleOffset + ("
	
	foundStr = findString newExpString lookFor
	
	if foundStr != undefined do 
	(
		--ok now we need to check if this joint is one of the nonescaleoffset joints 
		--if it is one of these then we dont add a 1
		
		scStr = "(ScaleOffset + 1) + ("
		for i in nonConcatJoints do 
		(
			if bName == i do 
			(
				scStr = "(ScaleOffset) + ("
			)
		)
		
		newExpString = substituteString newExpString lookFor scStr
	)
	
	cont.setExpression newExpString
	
	print ("Fixed scale on "+bName)	
)

for b in facialJoints do 
(
	o = getnodebyname b
	if (o.scale.controller[3] as string) == "SubAnim:ConcatScale" do
	(
		if (o.scale.controller[3].X_Scale.controller as string) == "Controller:Float_Expression" do 
		(
			cont = o.scale.controller[3].X_Scale.controller
			
			plusPlusFixup cont b
			
			scaleOffsetTargetChange cont o
		)
		
		if (o.scale.controller[3].Y_Scale.controller as string) == "Controller:Float_Expression" do 
		(
			cont = o.scale.controller[3].Y_Scale.controller
			
			plusPlusFixup cont b
			
			scaleOffsetTargetChange cont o
		)

		if (o.scale.controller[3].Z_Scale.controller as string) == "Controller:Float_Expression" do 
		(
			cont = o.scale.controller[3].Z_Scale.controller
			
			plusPlusFixup cont b
			
			scaleOffsetTargetChange cont o
		)

		
	)
)	