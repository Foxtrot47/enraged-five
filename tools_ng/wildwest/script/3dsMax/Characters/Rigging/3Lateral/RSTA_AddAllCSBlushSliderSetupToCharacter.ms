filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-- filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Includes\\FN_Rigging.ms")	
filein (theWildWest + "script/3dsMax/_config_files/Wildwest_header.ms")

filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_Rigging.ms")

fn RSTA_AddBlushSlider = 
(
	--first off create the blush slider joystick and the node which gets driven by it
	
	blushDrivenNodeName = "MH_BlushSlider" --41166 hash
	blushSliderName = "cutsceneBlush_OFF" --14882 hash
	blushExpr = "BlushSliderY * 0.1"
	
	blushDrivenNode  = getNodeByName blushDrivenNodeName
	
	if blushDrivenNode == undefined then
	(
		blushDrivenNode = point pos:[0,0,0] isSelected:off name:blushDrivenNodeName
		blushDrivenNode.parent = $SKEL_ROOT
		
		setUserProp blushDrivenNode "tag" blushDrivenNodeName
		setUserProp blushDrivenNode "exportTrans" "true"
		--no we need to add the blushDrivenNode to the head object
		headGeo = getNodeByName "head_000_r"
		
		if headGeo == undefined do
		(
			headGeo = getNodeByName "head_000_*"
		)
		
		runError = false
		
		if headGeo != undefined then 
		(
			skinMod = headGeo.modifiers[#Skin]
		
			if skinMode != undefined then
			(
				skinOps.addbone headGeo.modifiers[#Skin] blushDrivenNode boneUpdateInt
				
				--now we need to add a teeny bit of weight onto the bone
			)
			else
			(
				runError = true
			)
		)
		else
		(
			runError = true
		)		
		
		bnPosX = 0.0
		bnPosY = 0.0
		bnPosZ = 0.0
		
		bName = "cutsceneBlush_OFF"
		jstyle = 2
		jpos = [bnPosX, bnPosY, bnPosZ]
		scaleit = 0.25
		createdNode = RSTA_createJoystick bName jstyle jpos scaleit	
		
		controlNodePar = getNodeByName "faceControls_OFF"
		
		if controlNodePar != undefined do 
		(
			createdNodePar = createdNode.parent
			createdNodePar.parent = controlNodePar
			
			in coordsys parent createdNodePar.position = [0.0,0.0,-4.5]
		)
		
	-- 	setUserProp createdNode "tag" createdNode.name --add the bone tag (id)
		setUserProp createdNode "tag" "0" --add the bone tag (id)
		
		--track 50, id 0, type 2
		--add the type which needs to be track 2
	-- 	setuserProp createdNode 
		--add track which should be 50
		setUserProp createdNode "translateTrackType" "TRACK_FACIAL_TINTING"
		--enableTranslation dof
		setUserProp createdNode "exportTrans" "true"
		
		select blushDrivenNode
		selectMore createdNode
		
		RSTA_FreezeTransform()
		
		--now build the expression

		contCount = blushDrivenNode.position.controller.count
-- 		blushDrivenNode.position.controller.setName (2) drivingObjControllerName --if we dont set the name here then we end up with instancd controllers. Christ knows why!											

		blushDrivenNode.pos.controller.Available.controller = Position_XYZ ()
		blushDrivCont = blushDrivenNode.Position.Controller[3]
		blushDrivenNode.position.controller.setName (3) "Blush"
		expCont = blushDrivCont.Y_Position.controller = Float_Expression()

		driverScalarName = "BlushSliderY"
		driverController = createdNode.Position.Controller[2].Y_Position.Controller
		expCont.AddScalarTarget driverScalarName driverController
		expCont.SetExpression blushExpr
		
		--ok now we can actually add the blush expression updates
		exprStuff = #(
			#("Head_WB-011", "(min(1,  (max  ((((c_eye_C_CTRL_squintInnerDR)) * 2.24 / 2 ),0))  ))",#("c_eye_C_CTRL_squintInnerDR", "ScaleOffset"), #("$eye_C_CTRL.modifiers[#Attribute_Holder].eye_C_CTRL_A.squintInnerDR_CA.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WB-010", "(min(1,  (max  ((((c_eye_C_CTRL_squintInnerDL)) * 2.24 / 2),0))  ))",#("c_eye_C_CTRL_squintInnerDL", "ScaleOffset"), #("$eye_C_CTRL.modifiers[#Attribute_Holder].eye_C_CTRL_A.squintInnerDL_CA.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WB-009", "(min(1,  (max  ((((c_eye_C_CTRL_squeezeR)) * 1.44 / 2),0))  ))",#("c_eye_C_CTRL_squeezeR", "ScaleOffset"), #("$eye_C_CTRL.modifiers[#Attribute_Holder].eye_C_CTRL_A.squeezeR_CA.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WB-008", "(min(1,  (max  ((((c_eye_C_CTRL_squeezeL)) * 1.44 / 2),0))  ))",#("c_eye_C_CTRL_squeezeL", "ScaleOffset"), #("$eye_C_CTRL.modifiers[#Attribute_Holder].eye_C_CTRL_A.squeezeL_CA.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WB-007", "(min(1,  (max  ((((c_mouth_CTRL_scream)) * 2.4 / 2),0))  ))",#("c_mouth_CTRL_scream", "ScaleOffset"), #("$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.scream_CA.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WB-006", "(min(1,  (max  ((((c_mouth_CTRL_scream)) * 2.4 / 2),0))  ))",#("c_mouth_CTRL_scream", "ScaleOffset"), #("$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.scream_CA.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WB-005", "(min(1,  (max  ((((c_mouth_CTRL_lipsNarrowWideR * -1) + (c_mouth_CTRL_puckerR) + (c_mouth_CTRL_oh)) * 3.0 / 2),0))  ))",#("c_mouth_CTRL_lipsNarrowWideR", "c_mouth_CTRL_puckerR", "c_mouth_CTRL_oh", "ScaleOffset"), #("$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.lipsNarrowWideR_CA.controller", "$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.puckerR_CA.controller", "$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.oh_CA.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WB-004", "(min(1,  (max  ((((c_mouth_CTRL_lipsNarrowWideL * -1) + (c_mouth_CTRL_puckerL) + (c_mouth_CTRL_oh)) * 3.0 / 2),0))  ))",#("c_mouth_CTRL_lipsNarrowWideL", "c_mouth_CTRL_puckerL", "c_mouth_CTRL_oh", "ScaleOffset"), #("$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.lipsNarrowWideL_CA.controller", "$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.puckerL_CA.controller", "$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.oh_CA.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WB-003", "(min(1,  (max  ((((jaw_CTRL_ty * -1)) * 2.36 / 2),0))  ))",#("jaw_CTRL_ty", "ScaleOffset"), #("$jaw_CTRL.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WB-002", "(min(1,  (max  ((((jaw_CTRL_ty * -1)) * 2.36 / 2),0))  ))",#("jaw_CTRL_ty", "ScaleOffset"), #("$jaw_CTRL.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WB-001", "(min(1,  (max  ((((innerBrow_R_CTRL_ty * -1) + (innerBrow_R_CTRL_tx * -1)) * 0.96),0))  ))",#("innerBrow_R_CTRL_ty", "innerBrow_R_CTRL_tx", "ScaleOffset"), #("$innerBrow_R_CTRL.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller", "$innerBrow_R_CTRL.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WB-000", "(min(1,  (max  ((((innerBrow_L_CTRL_ty * -1) + (innerBrow_L_CTRL_tx * -1)) * 0.96),0))  ))",#("innerBrow_L_CTRL_ty", "innerBrow_L_CTRL_tx", "ScaleOffset"), #("$innerBrow_L_CTRL.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller", "$innerBrow_L_CTRL.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WA-011", "(min(1,  (max  ((((c_mouth_CTRL_smileR) + (cheek_R_CTRL_ty)) * 0.8 / 2),0))  ))",#("cheek_R_CTRL_ty", "c_mouth_CTRL_smileR", "ScaleOffset"), #("$cheek_R_CTRL.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller", "$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.smileR_CA.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WA-010", "(min(1,  (max  ((((c_mouth_CTRL_smileL) + (cheek_L_CTRL_ty)) * 0.8 / 2),0))  ))",#("cheek_L_CTRL_ty", "c_mouth_CTRL_smileL", "ScaleOffset"), #("$cheek_L_CTRL.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller", "$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.smileL_CA.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WA-009", "(min(1,  (max  ((((nose_R_CTRL_ty)) * 1.6 / 2),0))  ))",#("nose_R_CTRL_ty", "ScaleOffset"), #("$nose_R_CTRL.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WA-008", "(min(1,  (max  ((((nose_L_CTRL_ty)) * 1.6 / 2),0))  ))",#("nose_L_CTRL_ty", "ScaleOffset"), #("$nose_L_CTRL.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WA-006", "(min(1,  (max  ((((c_mouth_CTRL_smileR) + (mouth_CTRL_tx * -1)) * -2.36 / 2),0))  ))",#("mouth_CTRL_tx", "c_mouth_CTRL_smileR", "ScaleOffset"), #("$mouth_CTRL.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller", "$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.smileR_CA.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WA-007", "(min(1,  (max  ((((c_mouth_CTRL_smileL) + (mouth_CTRL_tx * -1)) * 2.36 / 2),0))  ))",#("mouth_CTRL_tx", "c_mouth_CTRL_smileL", "ScaleOffset"), #("$mouth_CTRL.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller", "$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.smileL_CA.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WA-005", "(min(1,  (max  ((((c_mouth_CTRL_chinRaiseLower) + (c_mouth_CTRL_chinWrinkle)) * 4.0 / 2),0))  ))",#("c_mouth_CTRL_chinRaiseLower", "c_mouth_CTRL_chinWrinkle", "ScaleOffset"), #("$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.chinRaiseLower_CA.controller", "$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.chinWrinkle_CA.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WA-004", "(min(1,  (max  ((((c_mouth_CTRL_chinRaiseLower) + (c_mouth_CTRL_chinWrinkle)) * 4.0 / 2),0))  ))",#("c_mouth_CTRL_chinRaiseLower", "c_mouth_CTRL_chinWrinkle", "ScaleOffset"), #("$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.chinRaiseLower_CA.controller", "$mouth_CTRL.modifiers[#Attribute_Holder].mouth_CTRL_A.chinWrinkle_CA.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WA-003", "(min(1,  (max  (((outerBrow_R_CTRL_ty) * 4.8 / 2),0))  ))",#("outerBrow_R_CTRL_ty", "ScaleOffset"), #("$outerBrow_R_CTRL.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WA-002", "(min(1,  (max  (((outerBrow_L_CTRL_ty) * 4.8 / 2),0))  ))",#("outerBrow_L_CTRL_ty", "ScaleOffset"), #("$outerBrow_L_CTRL.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WA-001", "(min(1,  (max  (((innerBrow_R_CTRL_ty) * 4.8 / 2),0))  ))",#("innerBrow_R_CTRL_ty", "ScaleOffset"), #("$innerBrow_R_CTRL.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller")),
			#("Head_WA-000", "(min(1,  (max  (((innerBrow_L_CTRL_ty) * 4.8 / 2),0))  ))",#("innerBrow_L_CTRL_ty", "ScaleOffset"), #("$innerBrow_L_CTRL.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller", "$ScaleOffset.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller"))	
			)
			
		for item = 1 to exprStuff.count do 
		(
			obj = getNodeByName exprStuff[item][1]
			
			expCont = obj.position.Controller[2].Y_Position.controller
			expStr = exprStuff[item][2]
			
			scalarNames = exprStuff[item][3]
			scalarTargets = exprStuff[item][4]
			
			for i = 1 to scalarNames.count do 
			(
				driverController  = execute scalarTargets[i]

				expCont.AddScalarTarget scalarNames[i] driverController				
			)
			
			expCont.SetExpression expStr 
			
			format ("Expression added to "+obj.name+"\n")
		)
		
		if runError == true do
		(
			messagebox ("Please add "+blushDrivenNodeName+" to the skin modfiier of your head geometry.\n")
			format ("Please add "+blushDrivenNodeName+" to the skin modfiier of your head geometry.\n")
		)
	)
	else
	(
		format ("We have found an existing "+blushDrivenNodeName+" node. Please ensure you do not have this or "+blushSliderName+". This suggests setup is already done.\n")
		messagebox ("We have found an existing "+blushDrivenNodeName+" node. Please ensure you do not have this or "+blushSliderName+". This suggests setup is already done.\n")
	)
)


RSTA_AddBlushSlider()