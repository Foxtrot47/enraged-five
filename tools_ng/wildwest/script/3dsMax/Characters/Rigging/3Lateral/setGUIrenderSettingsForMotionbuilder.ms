
sela = #($lowerLip_L_CTRL, $lowerLip_C_CTRL, $lowerLip_R_CTRL, $lipCorner_R_CTRL, $upperLip_C_CTRL, $upperLip_L_CTRL, $lipCorner_L_CTRL, $upperLip_R_CTRL, $eyelidUpperOuter_L_CTRL, $eyelidUpperInner_L_CTRL, $eyelidLowerOuter_L_CTRL, $eyelidLowerInner_L_CTRL, $eyelidLowerOuter_R_CTRL, $eyelidLowerInner_R_CTRL, $eyelidUpperOuter_R_CTRL, $eyelidUpperInner_R_CTRL)

-- sela = selection as array

for a = 1 to sela.count do
(
	sela[a].baseobject.render_displayRenderMesh = true
	sela[a].baseobject.thickness = 0.001
	sela[a].baseobject.sides = 3
)

sela = #($mouth_CTRL, $jaw_CTRL, $tongueMove_CTRL, $tongueRoll_CTRL, $tongueInOut_CTRL, $outerBrow_R_CTRL, $innerBrow_L_CTRL, $outerBrow_L_CTRL, $innerBrow_R_CTRL, $eye_C_CTRL, $eye_L_CTRL, $eye_R_CTRL, $nose_L_CTRL, $nose_R_CTRL, $cheek_L_CTRL, $cheek_R_CTRL)

for a = 1 to sela.count do
(
	sela[a].baseobject.render_displayRenderMesh = true
	sela[a].baseobject.thickness = 0.08
	sela[a].baseobject.sides = 3
)