fn RSTA_fixThreeLateralAlphAGuiShaders = 
(
-- 	headerNodes = #(
-- 		"Tongue",
-- 		"JAW",
-- 		"NOSE",
-- 		"MOUTH",
-- 		"Eyes",
-- 		"LIPS"	
-- 		)
-- 		


-- 	textNodes = #(
-- 		"RS_tongue_TEXT",
-- 		"RS_jaw_TEXT",
-- 		"RS_mouth_TEXT",
-- 		"RS_cheek_TEXT",
-- 		"RS_eyes_TEXT",
-- 		"RS_brows_TEXT",
-- 		"RS_nose_TEXT",	
-- 		"TEXT_press",
-- 		"TEXT_rollIn",
-- 		"TEXT_narrowWide",
-- 		"TEXT_clench",
-- 		"TEXT_backFwd",
-- 		"TEXT_nasolabialFurrowL",
-- 		"TEXT_nasolabialFurrowR",
-- 		"TEXT_smileR",
-- 		"TEXT_smileL",
-- 		"TEXT_openSmileR",
-- 		"TEXT_openSmileL",
-- 		"TEXT_frownR",
-- 		"TEXT_frownL",
-- 		"TEXT_scream",
-- 		"TEXT_lipsNarrowWideR",
-- 		"TEXT_lipsNarrowWideL",
-- 		"TEXT_lipsStretchOpenR",
-- 		"TEXT_lipsStretchOpenL",
-- 		"TEXT_chinWrinkle",
-- 		"TEXT_chinRaiseUpper",
-- 		"TEXT_chinRaiseLower",
-- 		"TEXT_closeOuterR",
-- 		"TEXT_closeOuterL",
-- 		"TEXT_puckerR",
-- 		"TEXT_puckerL",
-- 		"TEXT_oh",
-- 		"TEXT_funnelUR",
-- 		"TEXT_funnelDR",
-- 		"TEXT_mouthSuckUR",
-- 		"TEXT_mouthSuckDR",
-- 		"TEXT_pressR",
-- 		"TEXT_pressL",
-- 		"TEXT_dimpleR",
-- 		"TEXT_dimpleL",
-- 		"TEXT_suckPuffR",
-- 		"TEXT_suckPuffL",
-- 		"TEXT_lipBite",
-- 		"TEXT_funnelUL",
-- 		"TEXT_funnelDL",
-- 		"TEXT_mouthSuckUL",
-- 		"TEXT_mouthSuckDL",
-- 		"TEXT_blinkL",
-- 		"TEXT_squeezeR",
-- 		"TEXT_squeezeL",
-- 		"TEXT_blinkR",
-- 		"TEXT_openCloseDR",
-- 		"TEXT_squintInnerUR",
-- 		"TEXT_squintInnerDR",
-- 		"TEXT_openCloseUR",
-- 		"TEXT_openCloseDL",
-- 		"TEXT_squintInnerUL",
-- 		"TEXT_squintInnerDL",
-- 		"TEXT_openCloseUL",
-- 		
-- 		"tongue_TEXT",
-- 		"jaw_TEXT",
-- 		"mouth_TEXT",
-- 		"cheek_TEXT",
-- 		"eyes_TEXT",
-- 		"brows_TEXT",
-- 		"nose_TEXT"		
-- 		)

	textNodes = #(
		"Tongue",
		"JAW",
		"NOSE",
		"MOUTH",
		"Eyes",
		"LIPS"	
		)
		


	headerNodes = #(
		"TEXT_press",
		"TEXT_rollIn",
		"TEXT_narrowWide",
		"TEXT_clench",
		"TEXT_backFwd",
		"TEXT_nasolabialFurrowL",
		"TEXT_nasolabialFurrowR",
		"TEXT_smileR",
		"TEXT_smileL",
		"TEXT_openSmileR",
		"TEXT_openSmileL",
		"TEXT_frownR",
		"TEXT_frownL",
		"TEXT_scream",
		"TEXT_lipsNarrowWideR",
		"TEXT_lipsNarrowWideL",
		"TEXT_lipsStretchOpenR",
		"TEXT_lipsStretchOpenL",
		"TEXT_chinWrinkle",
		"TEXT_chinRaiseUpper",
		"TEXT_chinRaiseLower",
		"TEXT_closeOuterR",
		"TEXT_closeOuterL",
		"TEXT_puckerR",
		"TEXT_puckerL",
		"TEXT_oh",
		"TEXT_funnelUR",
		"TEXT_funnelDR",
		"TEXT_mouthSuckUR",
		"TEXT_mouthSuckDR",
		"TEXT_pressR",
		"TEXT_pressL",
		"TEXT_dimpleR",
		"TEXT_dimpleL",
		"TEXT_suckPuffR",
		"TEXT_suckPuffL",
		"TEXT_lipBite",
		"TEXT_funnelUL",
		"TEXT_funnelDL",
		"TEXT_mouthSuckUL",
		"TEXT_mouthSuckDL",
		"TEXT_blinkL",
		"TEXT_squeezeR",
		"TEXT_squeezeL",
		"TEXT_blinkR",
		"TEXT_openCloseDR",
		"TEXT_squintInnerUR",
		"TEXT_squintInnerDR",
		"TEXT_openCloseUR",
		"TEXT_openCloseDL",
		"TEXT_squintInnerUL",
		"TEXT_squintInnerDL",
		"TEXT_openCloseUL",
		
		"RS_tongue_TEXT",
		"RS_jaw_TEXT",
		"RS_mouth_TEXT",
		"RS_cheek_TEXT",
		"RS_eyes_TEXT",
		"RS_brows_TEXT",
		"RS_nose_TEXT",			
		
		"tongue_TEXT",
		"jaw_TEXT",
		"mouth_TEXT",
		"cheek_TEXT",
		"eyes_TEXT",
		"brows_TEXT",
		"nose_TEXT"		
		)
		
	newTextNodes = #(
	"TEXT_press",
	"TEXT_rollIn",
	"TEXT_narrowWide",
	"TEXT_clench",
	"TEXT_backFwd",
	"TEXT_nasolabialFurrowL",
	"TEXT_nasolabialFurrowR",
	"TEXT_smileR",
	"TEXT_smileL",
	"TEXT_openSmileR",
	"TEXT_openSmileL",
	"TEXT_frownR",
	"TEXT_frownL",
	"TEXT_scream",
	"TEXT_lipsNarrowWideR",
	"TEXT_lipsNarrowWideL",
	"TEXT_lipsStretchOpenR",
	"TEXT_lipsStretchOpenL",
	"TEXT_chinWrinkle",
	"TEXT_chinRaiseUpper",
	"TEXT_chinRaiseLower",
	"TEXT_closeOuterR",
	"TEXT_closeOuterL",
	"TEXT_puckerR",
	"TEXT_puckerL",
	"TEXT_oh",
	"TEXT_funnelUR",
	"TEXT_funnelDR",
	"TEXT_mouthSuckUR",
	"TEXT_mouthSuckDR",
	"TEXT_pressR",
	"TEXT_pressL",
	"TEXT_dimpleR",
	"TEXT_dimpleL",
	"TEXT_suckPuffR",
	"TEXT_suckPuffL",
	"TEXT_lipBite",
	"TEXT_funnelUL",
	"TEXT_funnelDL",
	"TEXT_mouthSuckUL",
	"TEXT_mouthSuckDL",
	"TEXT_blinkL",
	"TEXT_squeezeR",
	"TEXT_squeezeL",
	"TEXT_blinkR",
	"TEXT_openCloseDR",
	"TEXT_squintInnerUR",
	"TEXT_squintInnerDR",
	"TEXT_openCloseUR",
	"TEXT_openCloseDL",
	"TEXT_squintInnerUL",
	"TEXT_squintInnerDL",
	"TEXT_openCloseUL"
		)
		
	newHeaderNodes = #(
	"brows_TEXT",
	"eyes_TEXT",
	"cheek_TEXT",
	"nose_TEXT",
	"mouth_TEXT",
	"jaw_TEXT",
	"tongue_TEXT",
	"Eyes",
	"MOUTH",
	"LIPS",
	"NOSE",
	"JAW",
	"Tongue"
		)
		
	boxNodes = #(
		"FacialAttrGUI",
		"RS_tongueInOut_FRAME",
		"RS_tongueMove_FRAME",
		"RS_tongueRoll_FRAME",
		"RS_jaw_FRAME",
		"RS_mouth_FRAME",
		"RS_innerBrow_L_FRAME",
		"RS_outerBrow_L_FRAME",
		"RS_innerBrow_R_FRAME",
		"RS_eye_R_FRAME",
		"RS_cheek_R_FRAME",
		"RS_cheek_L_FRAME",
		"RS_eye_L_FRAME",
		"RS_eye_C_FRAME",
		"RS_nose_L_FRAME",
		"RS_outerBrow_R_FRAME",
		"RS_nose_R_FRAME",
		"RS_faceControls_FRAME",	
		"RECT_press",
		"RECT_rollIn",	
		"RECT_narrowWide",
		"RECT_clench",
		"RECT_backFwd",
		"RECT_nasolabialFurrowL",
		"RECT_nasolabialFurrowR",
		"RECT_smileR",
		"RECT_smileL",
		"RECT_openSmileR",
		"RECT_openSmileL",
		"RECT_frownR",
		"RECT_frownL",
		"RECT_scream",
		"RECT_lipsNarrowWideR",
		"RECT_lipsNarrowWideL",
		"RECT_lipsStretchOpenR",
		"RECT_lipsStretchOpenL",
		"RECT_chinWrinkle",
		"RECT_chinRaiseUpper",
		"RECT_chinRaiseLower",
		"RECT_closeOuterR",
		"RECT_closeOuterL",
		"RECT_puckerR",
		"RECT_puckerL",
		"RECT_oh",
		"RECT_funnelUR",
		"RECT_funnelDR",
		"RECT_mouthSuckUR",
		"RECT_mouthSuckDR",
		"RECT_pressR",
		"RECT_pressL",
		"RECT_dimpleR",
		"RECT_dimpleL",
		"RECT_suckPuffR",
		"RECT_suckPuffL",
		"RECT_lipBite",
		"RECT_funnelUL",
		"RECT_funnelDL",
		"RECT_funnelDL001",
		"RECT_mouthSuckDL",
		"RECT_blinkL",
		"RECT_squeezeR",
		"RECT_squeezeL",
		"RECT_lipsNarrowWideR001",
		"RECT_openCloseUR001",
		"RECT_squintInnerUR",
		"RECT_squintInnerDR",
		"RECT_openCloseUR",
		"RECT_openCloseDL",
		"RECT_squintInnerUL",
		"RECT_squintInnerDL",
		"RECT_openCloseUL",
		"RECT_thinThick_C",
		"RECT_thinThick_D",
		"RECT_stickyLips_E",
		"RECT_thinThick_B",
		"RECT_thinThick_F",
		"RECT_thinThick_H",
		"RECT_thinThick_G",
		"RECT_stickyLips_A",
		
		"tongueInOut_FRAME",
		"tongueMove_FRAME",
		"tongueRoll_FRAME",
		"jaw_FRAME",
		"mouth_FRAME",
		"innerBrow_L_FRAME",
		"outerBrow_L_FRAME",
		"innerBrow_R_FRAME",
		"eye_R_FRAME",
		"cheek_R_FRAME",
		"cheek_L_FRAME",
		"eye_L_FRAME",
		"eye_C_FRAME",
		"nose_L_FRAME",
		"outerBrow_R_FRAME",
		"nose_R_FRAME",
		"faceControls_FRAME"
		)

	circleNodes = #(
		"CIRC_press",
		"CIRC_rollIn",
		"CIRC_narrowWide",
		"CIRC_clench",
		"CIRC_backFwd",
		"CIRC_nasolabialFurrowL",
		"CIRC_nasolabialFurrowR",
		"CIRC_smileR",
		"CIRC_smileL",
		"CIRC_openSmileR",
		"CIRC_openSmileL",
		"CIRC_frownR",
		"CIRC_frownL",
		"CIRC_scream",
		"CIRC_lipsNarrowWideR",
		"CIRC_lipsNarrowWideL",
		"CIRC_lipsStretchOpenR",
		"CIRC_lipsStretchOpenL",
		"CIRC_chinWrinkle",
		"CIRC_chinRaiseUpper",
		"CIRC_chinRaiseLower",
		"CIRC_closeOuterR",
		"CIRC_closeOuterL",
		"CIRC_puckerR",
		"CIRC_puckerL",
		"CIRC_oh",
		"CIRC_funnelUR",
		"CIRC_funnelDR",
		"CIRC_mouthSuckUR",
		"CIRC_mouthSuckDR",
		"CIRC_pressR",
		"CIRC_pressL",
		"CIRC_dimpleR",
		"CIRC_dimpleL",
		"CIRC_suckPuffR",
		"CIRC_suckPuffL",
		"CIRC_lipBite",
		"CIRC_funnelUL",
		"CIRC_funnelDL",
		"CIRC_mouthSuckUL",
		"CIRC_mouthSuckDL",
		"CIRC_blinkL",
		"CIRC_squeezeR",
		"CIRC_squeezeL",
		"CIRC_blinkR",
		"CIRC_openCloseDR",
		"CIRC_squintInnerUR",
		"CIRC_squintInnerDR",
		"CIRC_openCloseUR",
		"CIRC_openCloseDL",
		"CIRC_squintInnerUL",
		"CIRC_squintInnerDL",
		"CIRC_openCloseUL",
		"CIRC_thinThick_C",
		"CIRC_thinThick_D",
		"CIRC_stickyLips_E",
		"CIRC_thinThick_B",
		"CIRC_thinThick_F",
		"CIRC_thinThick_H",
		"CIRC_thinThick_G",
		"CIRC_stickyLips_A",
		"RS_lipCorner_L_CTRL",
		"RS_upperLip_R_CTRL",	
		"RS_tongueMove_CTRL",
		"RS_eye_R_CTRL",
		"RS_tongueInOut_CTRL",	
		"RS_tongueRoll_CTRL",
		"RS_eyelidLowerInner_L_CTRL",
		"RS_eyelidLowerOuter_R_CTRL",
		"RS_eyelidUpperInner_L_CTRL",
		"RS_eyelidUpperOuter_L_CTRL",
		"RS_eyelidLowerInner_R_CTRL",
		"RS_eyelidLowerOuter_L_CTRL",	
		"RS_jaw_CTRL",
		"RS_mouth_CTRL",
		"RS_eyelidUpperOuter_R_CTRL",
		"RS_innerBrow_L_CTRL",
		"RS_outerBrow_L_CTRL",
		"RS_lowerLip_R_CTRL",
		"RS_lowerLip_C_CTRL",
		"RS_lowerLip_L_CTRL",
		"RS_innerBrow_R_CTRL",
		"RS_cheek_R_CTRL",
		"RS_cheek_L_CTRL",
		"RS_eye_C_CTRL",
		"RS_nose_L_CTRL",
		"RS_outerBrow_R_CTRL",
		"RS_eyelidUpperInner_R_CTRL",
		"RS_nose_R_CTRL",
		"RS_lipCorner_R_CTRL",
		"RS_upperLip_C_CTRL",
		"RS_upperLip_L_CTRL",

		"lipCorner_L_CTRL",
		"upperLip_R_CTRL",	
		"tongueMove_CTRL",
		"eye_R_CTRL",
		"tongueInOut_CTRL",	
		"tongueRoll_CTRL",
		"eyelidLowerInner_L_CTRL",
		"eyelidLowerOuter_R_CTRL",
		"eyelidUpperInner_L_CTRL",
		"eyelidUpperOuter_L_CTRL",
		"eyelidLowerInner_R_CTRL",
		"eyelidLowerOuter_L_CTRL",	
		"jaw_CTRL",
		"mouth_CTRL",
		"eyelidUpperOuter_R_CTRL",
		"innerBrow_L_CTRL",
		"outerBrow_L_CTRL",
		"lowerLip_R_CTRL",
		"lowerLip_C_CTRL",
		"lowerLip_L_CTRL",
		"innerBrow_R_CTRL",
		"cheek_R_CTRL",
		"cheek_L_CTRL",
		"eye_C_CTRL",
		"nose_L_CTRL",
		"outerBrow_R_CTRL",
		"eyelidUpperInner_R_CTRL",
		"nose_R_CTRL",
		"lipCorner_R_CTRL",
		"upperLip_C_CTRL",
		"upperLip_L_CTRL"
		
		)


	mergemaxFile "X:/gta5/art/peds/3LateralSetup/3LateralAlphaGuiMaterials.max" #useMergedMtlDups quiet:true

	headerShaderObj = getNodeByName "Sphere_HEADERS"
	headerMat = headerShaderObj.material	
	textShaderObj = getNodeByName "Sphere_JS_Text"
	textMat = textShaderObj.material		
	boxShaderObj = getNodeByName "Sphere_JSBox"
	boxMat = boxShaderObj.material
	circleShaderObj = getNodeByName "Sphere_JS_Circles"
	circleMat = circleShaderObj.material		
		
-- 	for iname in headerNodes do 
	for iname in newHeaderNodes do 	
	(
		obj = getNodeByName iName
		if obj != undefined do 
		(		
			obj.material = headerMat
		)
	)


-- 	for iname in textNodes do 
	for iname in newTextNodes do 
	(
		obj = getNodeByName iName
		if obj != undefined do 
		(
			obj.material = textMat
		)
	)


	for iname in boxNodes do 
	(
		obj = getNodeByName iName
		if obj != undefined do 
		(		
			obj.material = boxMat
		)
	)


	for iname in circleNodes do 
	(
		obj = getNodeByName iName
		if obj != undefined do 
		(		
			obj.material = circleMat
		)
	)

	delete headerShaderObj
	delete textShaderObj
	delete boxShaderObj
	delete circleShaderObj
	
	format ("GUI Shaders reset!\n")
	messagebox ("GUI Shaders reset!") title:"Woot Woot!" beep:true
)

RSTA_fixThreeLateralAlphAGuiShaders()