fn matchRightToLeft =
(
	local faceArray = #()
	local leftArray = #()
	local parentingArray = #()
	
	for obj in objects do 
	(
		if classof obj == Dummy do
		(
			if substring obj.name 1 7 == "FACIAL_" do
			(
				append faceArray obj
				
				if substring obj.name 7 3 == "_L_" do
				(
					append leftArray obj
				)
			)
		)
	)
	
	--now we need to record the parenting of the nodes
	
	for obj in faceArray do 
	(
		local tmpArr = #()
		
		append tmpArr obj
		
		if obj.parent != undefined then
		(
			append tmpArr obj.parent
		)
		else
		(
			append tmpArr undefined
		)
		
		append parentingArray tmpArr 
	)
	
	--then unparent
	
	for obj in faceArray do
	(
		obj.parent = undefined
	)
	
	for lNode in leftArray do 
	(
		rNode = getNodeByName ("FACIAL"+"_R_"+(substring lNode.name 10 -1))
			
		if rNode != undefined then
		(
			lPos = in coordsys world lNode.position
				
			in coordsys world rNode.position = [(lPos[1] * -1),lPos[2],lPos[3]]
			
			format ("Set position on "+rNode.name+"\n")
		)
		else
		(
			format ("Couldn't find a match for "+lNode.name)
		)
	)
	
	--nw we need to reparent
	
	for i = 1 to parentingArray.count do 
	(
		mainObj = parentingArray[i][1]
		parObj = parentingArray[i][2]
		
		if parObj != undefined do 
		(
			mainObj.parent = parObj
		)
	)
)

fn matchHelpersToBones = 
(
	mappingArray = #(
	#("FACIAL_L_lipCornerAnalog", "lipCorner_L_OFF"),
	#("FACIAL_L_lipUpperAnalog", "upperLip_L_OFF" ),
	#("FACIAL_lipUpperAnalog", "upperLip_C_OFF"),
	#("FACIAL_R_lipCornerAnalog", "lipCorner_R_OFF"),
	#("FACIAL_R_lipUpperAnalog", "upperLip_R_OFF" ),	
	#("FACIAL_L_lipLowerAnalog","lowerLip_L_OFF"),
	#("FACIAL_lipLowerAnalog", "lowerLip_C_OFF"),
	#("FACIAL_R_lipLowerAnalog","lowerLip_R_OFF")
	)

	for i = 1 to mappingArray.count do 
	(
		boneNode = getNodeByName mappingArray[i][1]
		helperNode = getNodeByName mappingArray[i][2]
		
		helperNode.position = boneNode.position
	)
	
)

fn recExisitngPose = 
(
	FaceArray = #()
	
	for obj in objects do 
	(
		if classof obj == Dummy do
		(
			if substring obj.name 1 7 == "FACIAL_" do
			(
				append faceArray obj
			)
		)
	)	
	
	for obj in FaceArray do 
	(
		objPos = in coordsys parent obj.position
		format ("\""+"in coordsys parent $"+obj.name+".position = "+(objPos as string)+"\""+"\n")
	)
)

-- recExisitngPose()
matchRightToLeft()
matchHelpersToBones()