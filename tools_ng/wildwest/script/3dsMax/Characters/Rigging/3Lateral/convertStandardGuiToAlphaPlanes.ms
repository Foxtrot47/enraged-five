--convert 3LateralGUI to Rockstar alpha plane version

--for every item in the 3 lateral gui shit align the rs version to them, 
--delete their geo, then attach the RS_ version

filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

----------------------------------------------
----------------------------------------------

standardGui = #( --list of all geo based gui nodes which will get replaced by the alphad plane ones
	"faceControls_FRAME", 
	"mouth_FRAME", 
	"mouth_CTRL", 
	"mouth_TEXT", 
	"jaw_FRAME", 
	"jaw_CTRL", 
	"jaw_TEXT", 
	"tongue_TEXT", 
	"tongueMove_FRAME", 
	"tongueMove_CTRL", 
	"tongueRoll_FRAME", 
	"tongueRoll_CTRL", 
	"tongueInOut_FRAME", 
	"tongueInOut_CTRL", 
	"outerBrow_R_FRAME", 
	"outerBrow_R_CTRL", 
	"innerBrow_L_FRAME", 
	"innerBrow_L_CTRL", 
	"outerBrow_L_FRAME", 
	"outerBrow_L_CTRL", 
	"brows_TEXT", 
	"innerBrow_R_FRAME", 
	"innerBrow_R_CTRL", 
	"eye_R_FRAME", 
	"eye_L_FRAME", 
	"eyes_TEXT", 
	"eye_C_FRAME", 
	"eye_C_CTRL", 
	"eye_L_CTRL", 
	"eye_R_CTRL", 
	"nose_L_FRAME", 
	"nose_L_CTRL", 
	"nose_R_FRAME", 
	"nose_R_CTRL", 
	"nose_TEXT", 
	"cheek_TEXT", 
	"cheek_L_FRAME", 
	"cheek_L_CTRL", 
	"cheek_R_FRAME", 
	"cheek_R_CTRL", 
	"lowerLip_L_CTRL", 
	"lowerLip_C_CTRL", 
	"lowerLip_R_CTRL", 
	"lipCorner_R_CTRL", 
	"upperLip_C_CTRL", 
	"upperLip_L_CTRL", 
	"lipCorner_L_CTRL", 
	"upperLip_R_CTRL", 
	"eyelidUpperOuter_L_CTRL", 
	"eyelidUpperInner_L_CTRL", 
	"eyelidLowerOuter_L_CTRL", 
	"eyelidLowerInner_L_CTRL", 
	"eyelidLowerOuter_R_CTRL", 
	"eyelidLowerInner_R_CTRL", 
	"eyelidUpperOuter_R_CTRL", 
	"eyelidUpperInner_R_CTRL"
)	
rsGUIFile = (theProjectRoot + "art/peds/3LateralSetup/RockStar_GTAV_GUI.max")


fn convertStandardGuiToAlphaGUI = 
(
	mergeMaxFile rsGUIFile quiet:true

-- 	disableSceneRedraw()
	max modify mode
	
	for sG = 1 to standardGui.count do 
	(
		stdGUI = getNodeByName standardGui[sG]
		alpGUI = getNodeByName ("RS_"+standardGui[sG])
		
		if alpGUI != undefined then
		(
			--do i need to parent the alpGUI to the stdGui.parent 1st?
			alpGui.parent = stdGui.parent

			in coordsys world alpGui.transform = in coordsys world stdGui.transform

			select stdGui
			
			--need to ensure we maintain existing attr holders
			
			if stdGui.modifiers.count > 0 then
			(
				modCount = stdGUI.modifiers.count
				val = (modCount - 1)
				if val < 2 then
				(
					modPanel.setCurrentObject stdGui.baseObject
					modPanel.addModToSelection (Edit_Poly ()) ui:on
					maxOps.CollapseNodeTo stdGui 2 off
				)
				else
				(
					modPanel.setCurrentObject stdGui.modifiers[val]
					modPanel.addModToSelection (Edit_Poly ()) ui:on
					maxOps.CollapseNodeTo stdGui (modCount - 1) off
				)
					
				modPanel.setCurrentObject stdGui.baseObject
			)
			else
			(
				convertTo stdGui PolyMeshObject
			)
			subobjectLevel = 4
			max select all
			
			max delete
			--now we can attach the alpha plane
			stdGui.EditablePoly.attach alpGUI stDGUI
			subobjectLevel = 0
		)
		else
		(
			print ("Couldn't find RS node RS_"+standardGui[sG])
		)
	)
	
	if ((yesNoGUI != undefined) and (yesNoGUI.isDisplayed)) do
	(destroyDialog yesNoGUI)
	
	rollout yesNoGUI "Connect Attrs?"
	(
		button btnYes "Yes" width:160 
		button btnNo "No" width:160 
		
		on btnYes pressed do 
		(
			filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/3Lateral/connectAlphaGuiToAttrs.ms")
-- 			connectAlphaGUIToAttrs()
			(destroyDialog yesNoGUI)
		)
		
		on btnNo pressed do 
		(
			--fack all
		)
	)	
		CreateDialog yesNoGUI width:175 pos:[800, 600] 
)

convertStandardGuiToAlphaGUI()





