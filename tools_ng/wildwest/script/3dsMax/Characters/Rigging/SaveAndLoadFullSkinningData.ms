-- filein "X:/wildwest/script/3dsMax/Characters/Rigging/SaveAndLoadFullSkinningData.ms"


--script to save and load both bone data and vertex weighting onto or from a selection of meshes.

filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

theProjectConfig = RsConfigGetProjBinConfigDir()
filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_Rigging.ms")

if theProject != "gta5" then
(
	messagebox ("This script is being run from the gtaV tools. Please run from "+theProject)
)
else
(
	if ((skinningInputOutputGUI != undefined) and (skinningInputOutputGUI.isDisplayed)) do
		(destroyDialog skinningInputOutputGUI)

	global skinningInputOutputGUI = undefined

	------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	-- if ((skinningInputOutputGUI != undefined) and (skinningInputOutputGUI.isDisplayed)) do
	-- 	(destroyDialog skinningInputOutputGUI)

	rollout skinningInputOutputGUI "Bone I/O"
	(
		button btnOutput "Save Skinning" width:110
		button btnInput "Load Skinning" width:110
		
		on btnOutput pressed do
		(
			if selection.count == 0 then
			(
				messageBox ("Please pick your geometry.") beep:true
			)
			else
			(
				selA = selection as array
				for i in selA do
				(
					rsta_savePerComponentSkinning i 
				)		
			)
		)

		on btnInput pressed do
		(
	-- 		runInput()
			sela = selection as array
			if sela.count >= 1 then
			(
				for a = 1 to sela.count do 
				(
					rsta_loadPerComponentSkinning sela[a]
				)
			)
			else
			(
				messagebox "Please select your meshes."
			)
			select sela
		)

	)

	CreateDialog skinningInputOutputGUI width:125 pos:[1450, 100] 
)