-- Skel animation loader.
-- Rick Stirling July 2009
--Modified by Matt Rennie March 2010
-- Rick Stirling April 2011 - copied to new wildwest and updated accordingly

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")


maxops.setDefaultTangentType #custom #custom


GLOBAL AnimationLoader --DECLARE THE ROLLOUT GLOBAL SO WE CAN CLOSE IT
GLOBAL addASkinPose 
global anim_loader_frameNumber = 0

ped_FaceRoot = undefined

try(destroyDialog AnimationLoader)catch() -- Destroy the UI if it exists 

lastFrame = 0
furthestKey = 0f --this is the variable used to find the furthest key along the timeline

joySticks = ambient_facial_joySticks

faceFXJoystickNames = #(
	"open_pose",
	"W_pose",
	"ShCh",
	"PBM",
	"FV",
	"wide_pose",
	"tBack_pose",
	"tTeeth_pose",
	"tRoof_pose",
	"Brow_Up_L",
	"Brow_Up_R",
	"Brows_Down",
	"Squint"
	)

fn addNewSkinPose = --set skin pose on hierarchy
(
	select ped_rootbone
	for p in selection do (
		if p.children != undefined do (
			selectmore p.children
		)
	)
	
	allBones = selection as array
	
	for boan in allBones do
	(
		boan.setSkinPose()
		print ("Skin Pose set on "+boan.name)
	)
	
)
-- Function to query user meta-data on skel_root to see if it contains a correct version no.
-- Returns version or Undefined
fn querySkelVersion = (
	currentSkelversion = undefined
	if ped_rootbone != undefined then
	(
		-- Read the User Defined Properties
		currentUDP = getUserPropbuffer ped_rootbone 
		filterUDP = filterString currentUDP "\r\n"

		-- Look for skeleton version numbers
		for i = 1 to filterUDP.count do (
			tagTest = filterstring filterUDP[i] "="
			skeltest = substring tagtest[1] 1 18
				
			if skelTest== SkeletonVersionTag do (
				verInt = (tagtest[2] as float) --hacky way to strip off any leading whitespace shite!
				currentSkelversion = (verInt as string)
			)
		)
		currentSkelversion -- return this
	)
	
	else (
		messageBox "WARNING! Couldn't find SKEL_Root"
	)
	
	
) -- End function



--get state of skinPoseMode for ped_rootbone
---toggle skinPoseMode of ped_rootbone
--set state of ped_rootbone children to match that of ped_rootbone
fn toggleFigMode =
(
	sPoseExists = ped_rootbone.skinPos
	
	if sPoseExists != [0,0,0] then
	(
		tF = ped_rootbone.skinPoseMode

		if tF == true then
			(
				ped_rootbone.skinPoseMode = false
			)
		else
			(
				ped_rootbone.skinPoseMode = true
			)

		select ped_rootbone
			for p in selection do (
				if p.children != undefined do (
					selectmore p.children
				)
			)
			
			allBones = selection as array
			
			for o in allBones do
			(
				if o.name != ped_rootbone.name do
				(
					if tF == true then
					(
						o.skinPoseMode = true
					)
					else
					(
						o.skinPoseMode = false
					)
				)
			)
	)
	else
	(
		createDialog addASkinPose 
	)
)




-- Put the model into the base pose
-- Involves a little trickery for older meshes
fn assumeSkinPose = 
(
	-- Figure out what version this skeleton is.
	currentSkelVersion = querySkelVersion()	
	
	-- If skeleton version is up to date just 'assume skin pose'.	
	if ((currentSkelVersion == currentSkeletonNumber) or (currentSkelVersion != undefined)) then (
		print "Skeleton is versioned so assuming skinPose."
		-- Build a list of all the bones in the object
		select ped_rootbone
		for p in selection do (
			p.assumeSkinPose
			if p.children != undefined do (
				selectmore p.children
			)
		)
	--DO WE WANT TO DO A WILDCARD SELECTION LIKE THIS OR DO SOME KIND OF ARRAY BUILD OF THE CHILDREN OF THE ped_rootbone?
	-- 	select $SKEL_* --DONT NEED THIS COS WE'VE ALREADY SELECTED EVERYTHING
	selectKeys $
	deletekeys $
	)
	
	--skeleton version is not the most up to date so we need to set frame to 0 before we do anything else to hackily get a skin pose
	else  (
		print "WARNING!!! Current Skeleton is not up to date. Doing hacky fig pose thingy."
		sliderTime = (0f)		
		)
	)





fn resetSkeleton = 
(
	assumeSkinPose()
)





fn getKeys skelType = --this finds the furthest key along the timeline on objects to allow setting of timeline range
(
	lastFrame = 0
	furthestKey = 0f --this is the variable used to find the furthest key along the timeline

	if skelType == 1 then --if skeltype is 1 then its a body skeleton
	(
		clearselection()
		
		selectByWildcard ped_skeleton_prefix
		allBones = selection as array
		
		for fb in allBones do
		(
			posCont = (fb.position.controller as string)
			rotCont = (fb.rotation.controller as string)
			
			if posCont == "Controller:Position_List" do
			(
				maxControllersPos = fb.position.controller.count 
				
				for c in maxControllersPos to 2 by -1 do
				(
					totalPKeys = fb.position.controller[c].keys
							
					if totalPKeys != -1 do
					(
						tPKCount = totalPKeys.count
						if tPKCount != 0 do
						(
							totalPString = (totalPKeys[tPKCount] as string)
							filtered = filterstring totalPString "@" --split the string by @ symbol
							filteredChunk = filtered.count
							filteredOutput = (filtered[filteredChunk] )
							fC = (filteredOutput.count - 2)
							filteredOutput = (substring filteredOutput 1 fC ) --should strip off the ")" from the end
							anim_loader_frameNumber = filteredOutput as integer
							
							if anim_loader_frameNumber >= lastframe do	(lastFrame = anim_loader_frameNumber)
						)
					)
				)
			)
		
			if posCont == "Controller:Position_XYZ" do
			(
				totalPKeys = fb.position.controller.keys
							
				if totalPKeys != -1 do
				(
					tPKCount = totalPKeys.count
					if tPKCount != 0 do
					(
						totalPString = (totalPKeys[tPKCount] as string)
						filtered = filterstring totalPString "@" --split the string by @ symbol
						filteredChunk = filtered.count
						filteredOutput = (filtered[filteredChunk] )
						fC = (filteredOutput.count - 2)
						filteredOutput = (substring filteredOutput 1 fC ) --should strip off the ")" from the end
						anim_loader_frameNumber = filteredOutput as integer
						
						if anim_loader_frameNumber >= lastframe do	(lastFrame = anim_loader_frameNumber)
					)
				)
	-- 			messageBox ("POS XYZ last key on "+fb.name+" = "+(anim_loader_frameNumber as string))
			)
		
			if rotCont == "Controller:Rotation_List" do
			(
				maxControllersRot = fb.rotation.controller.count 
				
				for c in maxControllersRot to 2 by -1 do
				(
					totalRKeys = fb.rotation.controller[c].keys				
					
					if totalRKeys != -1 do
					(
						tRKCount = totalRKeys.count
						if tRKCount != 0 do
						(
							totalRString = (totalRKeys[tRKCount] as string)
							filtered = filterstring totalRString "@" --split the string by @ symbol
							filteredChunk = filtered.count
							filteredOutput = (filtered[filteredChunk] )
							fC = (filteredOutput.count - 2)
							filteredOutput = (substring filteredOutput 1 fC ) --should strip off the ")" from the end
							anim_loader_frameNumber = filteredOutput as integer

							if anim_loader_frameNumber >= lastframe do	(lastFrame = anim_loader_frameNumber)	
						)
					)			
				)
	-- 			messageBox ("ROT list last key on "+fb.name+" = "+(anim_loader_frameNumber as string))
			)
			
			if rotCont == "Controller:Euler_XYZ" do
			(
				totalRKeys = fb.rotation.controller.keys
							
				if totalRKeys != -1 do
				(
					tRKCount = totalRKeys.count
					if tRKCount != 0 do
					(
						totalRString = (totalRKeys[tRKCount] as string)
						filtered = filterstring totalRString "@" --split the string by @ symbol
						filteredChunk = filtered.count
						filteredOutput = (filtered[filteredChunk] )
						fC = (filteredOutput.count - 2)
						filteredOutput = (substring filteredOutput 1 fC ) --should strip off the ")" from the end
						anim_loader_frameNumber = filteredOutput as integer

						if anim_loader_frameNumber >= lastframe do	(lastFrame = anim_loader_frameNumber)	
					)
				)
	-- 			messageBox ("Rot XYZ last key on "+fb.name+" = "+(anim_loader_frameNumber as string))
			)
		)

	print ("lastFrame = "+(lastFrame as string))
	furthestKey = (lastFrame as time)
		if furthestKey <= 0f do 
			(furthestKey = 0f +1f)
			
-- 		messageBox ("Setting final key to "+(furthestKey as string))
	animationRange = interval 0f furthestKey
			--now set frame to 0f
			sliderTime = 0f
	)
	else --if skeltype is not a 1 then it must be a facialskeleton
	(
-- 		messagebox "Setting key range for Joysticks"
		clearselection()	
		select $CTRL_*
		selectmore ped_FaceRoot
-- 		selectMore $CTRL_L_Mouth
		allBones = selection as array		
			--clearListener()
	
		for fb in allBones do --THIS IS A MEGA HACKY FIX
		(
			posCont = (fb.position.controller as string)
				print ("posCont on "+fb.name+" = "+posCont)
			rotCont = (fb.rotation.controller as string)
	-- 			print ("rotCont on "+fb.name+" = "+rotCont)
			
			if posCont == "Controller:Position_List" do
			(
	-- 			messageBox ("posCont = Pos list")
				maxControllersPos = fb.position.controller.count 
				
				kX = fb.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller.Limited_Controller__Bezier_Float.controller.keys
				kY = fb.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller.Limited_Controller__Bezier_Float.controller.keys
				kZ = fb.pos.controller.Zero_Pos_XYZ.controller.Z_Position.controller.Limited_Controller__Bezier_Float.controller.keys
				
				tPKCount = kX.count
					if tPKCount != 0 do
					(
						totalPKeys = kX
						--clearListener()
						totalPString = (totalPKeys[tPKCount] as string)
							print ("totalPString = "+totalPString)
						filtered = filterstring totalPString "f)" --split the string by @ symbol
							--print ("filtered = "+filtered)
	-- 					filtered2 = filterstring filtered "f)"
						filteredChunk = filtered.count
							print ("filteredChunk = "+(filteredChunk as string) )
						filteredOutput = (filtered[filteredChunk] )
							print ("filteredOutput = " +filteredOutput )
						if (substring filteredOutput 1 2) == "@" then
						(
							filteredOutput = (substring filteredOutput 2 30)
							fromCount = (filteredOutput.count - 3)
							print ("fromCount = "+(fromCount as string))
						)
						else
						(
							fromCount = (filteredOutput.count - 3)
							print ("fromCount = "+(fromCount as string))
						)
						totalCount = (filteredOutput.count)
							print ("totalCount = "+(totalCount as string))
						finalNumber = (substring filteredOutput fromCount 600)
								if (substring finalNumber 1 1) == "@" do
								(
									finalNumber = (substring finalNumber 2 30)
								)
								
						anim_loader_frameNumber = finalNumber as integer
							print ("anim_loader_frameNumber  = "+(anim_loader_frameNumber as string))
							
							print ("anim_loader_frameNumber = "+(anim_loader_frameNumber  as string))
							print ("lastFrame = "+(lastFrame as string))
								
							if anim_loader_frameNumber >= lastframe do	
								(
									print ("Key on "+fb.name+" Position_List greater than "+(lastframe as string))
										print ("**** Setting last frame to "+(anim_loader_frameNumber as string)+"from "+(lastframe as string))
									lastFrame = anim_loader_frameNumber
								)
					)
				tPKCount = kY.count
					if tPKCount != 0 do
					(
						totalPKeys = kY
						--clearListener()
						totalPString = (totalPKeys[tPKCount] as string)
							print ("totalPString = "+totalPString)
						filtered = filterstring totalPString "f)" --split the string by @ symbol
							--print ("filtered = "+filtered)
	-- 					filtered2 = filterstring filtered "f)"
						filteredChunk = filtered.count
							print ("filteredChunk = "+(filteredChunk as string) )
						filteredOutput = (filtered[filteredChunk] )
							print ("filteredOutput = " +filteredOutput )
						if (substring filteredOutput 1 2) == "@" then
						(
							filteredOutput = (substring filteredOutput 2 30)
							fromCount = (filteredOutput.count - 3)
							print ("fromCount = "+(fromCount as string))
						)
						else
						(
							fromCount = (filteredOutput.count - 3)
							print ("fromCount = "+(fromCount as string))
						)
						totalCount = (filteredOutput.count)
							print ("totalCount = "+(totalCount as string))
						finalNumber = (substring filteredOutput fromCount 600)
								if (substring finalNumber 1 1) == "@" do
								(
									finalNumber = (substring finalNumber 2 30)
								)
								
						anim_loader_frameNumber = finalNumber as integer
							print ("anim_loader_frameNumber  = "+(anim_loader_frameNumber as string))
							
							print ("anim_loader_frameNumber = "+(anim_loader_frameNumber  as string))
							print ("lastFrame = "+(lastFrame as string))
								
							if anim_loader_frameNumber >= lastframe do	
								(
									print ("Key on "+fb.name+" Position_List greater than "+(lastframe as string))
										print ("**** Setting last frame to "+(anim_loader_frameNumber as string)+"from "+(lastframe as string))
									lastFrame = anim_loader_frameNumber
								)
					)
				tPKCount = kZ.count
					if tPKCount != 0 do
					(
						totalPKeys = kZ
						--clearListener()
						totalPString = (totalPKeys[tPKCount] as string)
							print ("totalPString = "+totalPString)
						filtered = filterstring totalPString "f)" --split the string by @ symbol
							--print ("filtered = "+filtered)
	-- 					filtered2 = filterstring filtered "f)"
						filteredChunk = filtered.count
							print ("filteredChunk = "+(filteredChunk as string) )
						filteredOutput = (filtered[filteredChunk] )
							print ("filteredOutput = " +filteredOutput )
						if (substring filteredOutput 1 2) == "@" then
						(
							filteredOutput = (substring filteredOutput 2 30)
							fromCount = (filteredOutput.count - 3)
							print ("fromCount = "+(fromCount as string))
						)
						else
						(
							fromCount = (filteredOutput.count - 3)
							print ("fromCount = "+(fromCount as string))
						)
						totalCount = (filteredOutput.count)
							print ("totalCount = "+(totalCount as string))
						finalNumber = (substring filteredOutput fromCount 600)
								if (substring finalNumber 1 1) == "@" do
								(
									finalNumber = (substring finalNumber 2 30)
								)
								
						anim_loader_frameNumber = finalNumber as integer
							print ("anim_loader_frameNumber  = "+(anim_loader_frameNumber as string))
							
							print ("anim_loader_frameNumber = "+(anim_loader_frameNumber  as string))
							print ("lastFrame = "+(lastFrame as string))
								
							if anim_loader_frameNumber >= lastframe do	
								(
									print ("Key on "+fb.name+" Position_List greater than "+(lastframe as string))
										print ("**** Setting last frame to "+(anim_loader_frameNumber as string)+"from "+(lastframe as string))
									lastFrame = anim_loader_frameNumber
								)
					)				
			)
		)

		print ("lastFrame = "+(lastFrame as string))
		furthestKey = (lastFrame as time)
			if furthestKey <= 0f do 
				(furthestKey = 0f +1f)
				
	-- 		messageBox ("Setting final key to "+(furthestKey as string))
		animationRange = interval 0f furthestKey
			--now set frame to 0f
			sliderTime = 0f
	)



)

fn removeAllKeys skeltype = 
(
	if skelType == 1 then --if skeltype is 1 then its a body skeleton
	(
		clearselection()
		selectByWildcard ped_skeleton_prefix
		allBones = selection as array
	)
	else --if skeltype is not a 1 then it must be a facialskeleton
	(
		clearselection()	
		selectByWildcard 	ped_rigcontrol_prefix
		selectmore ped_FaceRoot
		allBones = selection as array		
	)
	
	for fb in allBones do
	(
		deleteKeys fb.controller 
	)
)



rollout progBar "Progress..."
(
	progressBar prog pos:[10,10] color:red
)



rollout AnimationLoader "Anims" width:250 height:215
(
	
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Max_anim_loader" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255) pos:[210,5]
	dropDownList animtestsfiles "Anim tests" width:200 height:40 items:#() pos:[25,20]
	button loadmyanim "Load Body Animation"  width:115 height:32 enabled:true toolTip:"loads selected body anim" pos:[5,70]
	button loadmyFaceAnim "Load Facial Animation"  width:115 height:32 enabled:true toolTip:"loads selected facial anim" pos:[130,70]
	button btnAnimLoader "Open .anim Loader" width:115 height:32 enabled:true toolTip:"Opens the GTA .anim loader" pos:[5,105]
	button btnSkinBrush "Skin brush" width:115 height:32 pos:[130,105]
	button btnKillKeys "Remove Animation" width:115 height:32 tooltip:"Remove all keys from the scene" pos:[5,140]
	button btnResetBindPose "Reset Bind Pose" width:115 height:32 pos:[130,140]

	button btnRecPasteBind "Rec/Paste Bind Pose" width:115 height:32 pos:[5, 175]
	button btnAnimRanges "Set Anim Ranges" width:115 height:32 pos:[130, 175]	
	
	-- Populate the list of animations
    on AnimationLoader open do 	(
		WWP4vSync pedAnimFiles
		-- get list of animation files files
		animlist=#()
		animfiles = animfolder  + "*.xaf"	

		thefiles = getfiles animfiles
		
		for f in thefiles do (
			animname = f
			print animname
			append animlist (uppercase(filenameFromPath f))	
		)
		
		--populate the file list
		animtestsfiles.items = animlist
	)

	on btnResetBindPose pressed do
	(
		messagebox "Add reset bindpose here"
		--filein ped_reset_bindpose_script	
	)
	
	on skinPoseToggle pressed do (
		toggleFigMode()
	)
	


	on loadmyanim pressed do (
		if undefined==animtestsfiles.selected then
			return false
		clearSelection()
		try (select ped_rootbone) catch()
		if $ == ped_rootbone then (
			-- Houston, we have lift off
			
			-- Initialise the model.
			
			skelObjects = objects as array
			clearSelection()
			for pB = 1 to skelObjects.count do
			(
				if substring skelObjects[pB].name 1 5 == ped_skeleton_prefix do
				(
					selectMore skelObjects[pB]
				)
			)
			
			
			selectedanim = animfolder + animtestsfiles.selected	

-- 			messageBox ("attempting to load "+selectedAnim)
				print ("attempting to load "+selectedAnim)
-- 			messageBox ("Mapping file = "+animmappingfile)
				print ("Mapping file = "+animmappingfile)
			
			--COMMENTED OUT THE USE MAPPING STUFF COS THIS SEEMS TO STOP THE ANIM FROM LOADING
-- 			LoadSaveAnimation.loadAnimation selectedanim $ useMapFile:true mapFileName:animmappingfile 
			LoadSaveAnimation.loadAnimation selectedanim selection useMapFile:true mapFileName:animmappingfile 
-- 						messageBox ("Loaded " +selectedAnim)
			getKeys 1
						
		)	

	)
	
	
	on btnAnimLoader pressed do
	(
		filein "pipeline//ui//animation_ui.ms"
	)
	
	on loadmyFaceAnim pressed do 
	(		
		ped_FaceRoot = getNodeByName "Ambient_UI"
		if ped_FaceRoot == undefined do 
		(
			ped_FaceRoot = getNodeByName "Facial_UI" --this is for the old rigs
		)
		
		clearSelection()
		try (select ped_FaceRoot) catch()
		if $ == ped_FaceRoot then (
		
			
			--NOW NEED TO DELETE EXISTING KEYS
			removeAllKeys 0 			

			joysticks = #() 
			
			for o in objects do 
			(
				if (substring o.name 1 5) == "CTRL_" do 
				(
					appendIfUnique joysticks o
				)
			)
			
			for obj in joySticks do
			(
				selectmore obj
			)
			
			for ffxJs in faceFXJoystickNames do
			(
				thisJs = getnodebyName ffxJs
				
				if thisJs != undefined do 
				(
					selectmore thisJs
				)
			)
			
			selectedanim = animfolder + animtestsfiles.selected	

				print ("attempting to load facial "+selectedAnim)
				print ("Mapping file facial = "+faceanimmappingfile )
			
			LoadSaveAnimation.loadAnimation selectedanim $ relative:false insertTime:0f --useMapFile:true mapFileName:faceanimmappingfile  
			getKeys 2
						
		)	

	)
	
	on btnSkinBrush pressed do
	(
		filein ped_skinbrush_script 
	)
	
	on btnKillKeys pressed do
	(
		objArray = objects as array
		createDialog progBar width:300 height:30
		
		for fb in objArray do
		(
			print ("deleting keys from "+fb.name)
			deleteKeys fb.controller 
			iVal = finditem objArray fb
			progBar.prog.value = (100.*iVal/objArray.count)
		)
		
		destroyDialog progBar
		--now set back to bind pose
-- 		messagebox "Add reset bindpose here"
		--filein (theWildWest + "script/max/Rockstar_North/character//Rigging_tools//c_resetBindPose.ms")		
		filein (theWildWest + "/script/3dsMax/Characters/Rigging/usefulScripts/resetBindPose.ms")
	)
	
	on btnAnimRanges pressed do
	(
		filein (theWildWest + "/script/3dsmax/characters/Rigging/setAnimRanges.ms")
	)

	on btnRecPasteBind pressed do
	(
		fileIn (theWildwest + "script/3dsMax/Characters/Rigging/usefulScripts/c_recPasteBindPose.ms")
	)	
	
)



rollOut addASkinPose "Add Skin Pose?"
(
	button btn1 "Add Skin Pose" tooltip:"Add a Skin Pose for this skeleton."
	button btn2 "Don't add a Skin Pose." tooltip:"Don't add a Skin Pose for this skeleton."
	
	on btn1 pressed do
	(
		addNewSkinPose ()
		destroyDialog addASkinPose
	)
	
	on btn2 pressed do
	(
		destroyDialog addASkinPose
	)
	
)

-- destroyDialog AnimationLoader
createDialog AnimationLoader
