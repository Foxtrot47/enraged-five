--script to find any nodes in the scene which contain a float expression
-- Oct 2011
--Matt rennie

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/characters/mb_expression_whitelist.dat")--our blacklist of expressions

------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------

objWithExprArray = #()
noSpringExpObjs = #()

controllersToInclude = #(
	"Float_Expression",
	"Controller:Float_Expression" ,
	"SubAnim:RsSpring",
	"SubAnim:LookAt_Constraint"
	)

------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
fn sclXYZ obj = 
(
	for cont in controllersToInclude do
	(
		if (obj.scale.controller as string) == "SubAnim:RsSpring" then
		(
			appendIfUnique objWithExprArray obj
		)
		else
		(		
			if (classof obj.Scale.controller.X_Scale.controller as string) == cont do
			(
				--print (cont+" found on X Scale of "+obj.name)
				appendIfUnique objWithExprArray obj
				appendIfUnique noSpringExpObjs obj
			)
			if (classof obj.Scale.controller.Y_Scale.controller as string) == cont do
			(
				--print (cont+" found on Y Scale of "+obj.name)
				appendIfUnique objWithExprArray obj
				appendIfUnique noSpringExpObjs obj
			)	
			if (classof obj.Scale.controller.Z_Scale.controller as string) == cont do
			(
				--print (cont+" found on Z Scale of "+obj.name)
				appendIfUnique objWithExprArray obj
				appendIfUnique noSpringExpObjs obj
			)		
		)
	)
)

fn sclList obj = 
(
	for i = 2 to obj.scale.controller.count do --skip 1st controller as thats the frozen one
	(
		for cont in controllersToInclude do
		(
			if (obj.scale.controller[i] as string) == "SubAnim:RsSpring" then
			(
				appendIfUnique objWithExprArray obj
			)
			else
			(			
				if (classof obj.scale.controller[i].X_scale.controller as string) == cont do
				(
					--print (cont+" found on X Scale of "+obj.name)
					appendIfUnique objWithExprArray obj
					appendIfUnique noSpringExpObjs obj
				)
				if (classof obj.scale.controller[i].Y_scale.controller as string) == cont do
				(
					--print (cont+" found on Y Scale of "+obj.name)
					appendIfUnique objWithExprArray obj
					appendIfUnique noSpringExpObjs obj
				)
				if (classof obj.scale.controller[i].Z_scale.controller as string) == cont do
				(
					--print (cont+" found on Z Scale of "+obj.name)
					appendIfUnique objWithExprArray obj
					appendIfUnique noSpringExpObjs obj
				)		
			)
		)
	)		
)

fn rotEulerXYZ obj = 
(
	for cont in controllersToInclude do
	(
		if (obj.rotation.controller as string) == "SubAnim:RsSpring" then
		(
			appendIfUnique objWithExprArray obj
		)
		else
		(
			if (obj.rotation.controller[i] as string) == "SubAnim:LookAt_Constraint" then
			(
				appendIfUnique objWithExprArray obj
				appendIfUnique noSpringExpObjs obj
			)
			else			
			(
				if (classof obj.Rotation.controller.X_Rotation.controller as string) == cont do
				(
					--print (cont+" found on X Rotation of "+obj.name)
					appendIfUnique objWithExprArray obj
					appendIfUnique noSpringExpObjs obj
				)
				if (classof obj.Rotation.controller.Y_Rotation.controller as string) == cont do
				(
					--print (cont+" found on Y Rotation of "+obj.name)
					appendIfUnique objWithExprArray obj
					appendIfUnique noSpringExpObjs obj
				)	
				if (classof obj.Rotation.controller.Z_Rotation.controller as string) == cont do
				(
					--print (cont+" found on Z Rotation of "+obj.name)
					appendIfUnique objWithExprArray obj
					appendIfUnique noSpringExpObjs obj
				)	
			)
		)
	)
)

fn rotList obj = 
(
	for i = 2 to obj.rotation.controller.count do --skip 1st controller as thats the frozen one
	(
		for cont in controllersToInclude do
		(
			if (obj.rotation.controller[i] as string) == "SubAnim:RsSpring" then
			(
				appendIfUnique objWithExprArray obj
			)
			else
			(
				if (obj.rotation.controller[i] as string) == "SubAnim:LookAt_Constraint" then
				(
					appendIfUnique objWithExprArray obj
					appendIfUnique noSpringExpObjs obj
				)
				else
				(
					if (classof obj.rotation.controller[i].X_rotation.controller as string) == cont do
					(
						--print (cont+" found on X Rotation of "+obj.name)
						appendIfUnique objWithExprArray obj
						appendIfUnique noSpringExpObjs obj
					)
					if (classof obj.rotation.controller[i].Y_rotation.controller as string) == cont do
					(
						--print (cont+" found on Y Rotation of "+obj.name)
						appendIfUnique objWithExprArray obj
						appendIfUnique noSpringExpObjs obj
					)
					if (classof obj.rotation.controller[i].Z_rotation.controller as string) == cont do
					(
						--print (cont+" found on Z Rotation of "+obj.name)
						appendIfUnique objWithExprArray obj
						appendIfUnique noSpringExpObjs obj
					)		
				)
			)
		)
	)		
)

fn posXYZ obj = 
(
	for cont in controllersToInclude do
	(
		if (obj.position.controller as string) == "SubAnim:RsSpring" then
		(
			appendIfUnique objWithExprArray obj
		)
		else
		(		
			if (classof obj.position.controller.X_Position.controller as string) == cont do
			(
				--print (cont+" found on X Position of "+obj.name)
				appendIfUnique objWithExprArray obj
				appendIfUnique noSpringExpObjs obj
			)
			if (classof obj.position.controller.Y_Position.controller as string) == cont do
			(
				--print (cont+" found on Y Position of "+obj.name)
				appendIfUnique objWithExprArray obj
				appendIfUnique noSpringExpObjs obj
			)	
			if (classof obj.position.controller.Z_Position.controller as string) == cont do
			(
				--print (cont+" found on Z Position of "+obj.name)
				appendIfUnique objWithExprArray obj
				appendIfUnique noSpringExpObjs obj
			)
		)
	)
)

fn posList obj = 
(
	for i = 2 to obj.position.controller.count do --skip 1st controller as thats the frozen one
	(
		for cont in controllersToInclude do
		(
			if (obj.position.controller[i] as string) == "SubAnim:RsSpring" then
			(
				appendIfUnique objWithExprArray obj
			)
			else
			(
				if (classof obj.position.controller[i].X_Position.controller as string) == cont do
				(
					--print (cont+" found on X Position of "+obj.name)
					appendIfUnique objWithExprArray obj
					appendIfUnique noSpringExpObjs obj
				)
				if (classof obj.position.controller[i].Y_Position.controller as string) == cont do
				(
					--print (cont+" found on Y Position of "+obj.name)
					appendIfUnique objWithExprArray obj
					appendIfUnique noSpringExpObjs obj
				)
				if (classof obj.position.controller[i].Z_Position.controller as string) == cont do
				(
					--print (cont+" found on Z Position of "+obj.name)
					appendIfUnique objWithExprArray obj
					appendIfUnique noSpringExpObjs obj
				)		
			)
		)
	)	
)

fn queryFloatExpression obj = 
(
	--print ("Testing "+obj.name)
	if (classof obj.position.controller as string) == "position_list" do
	(
		--print ("Position List found on "+obj.name)
		posList obj
	)
	if (classof obj.position.controller as string) == "Position_XYZ" do
	(
		--print ("Position XYZ found on "+obj.name)
		posXYZ obj
	)
	---------
	if (classof obj.rotation.controller as string) == "rotation_list" do
	(
		--print ("Rotation List found on "+obj.name)
		rotList obj
	)
	if (classof obj.position.controller as string) == "Euler_XYZ" do
	(
		--print ("Euler XYZ found on "+obj.name)
		rotEulerXYZ obj
	)
	--------------
	if (classof obj.scale.controller as string) == "scale_list" do
	(
		--print ("Scale List found on "+obj.name)
		sclList obj
	)
	if (classof obj.scale.controller as string) == "Controller:ScaleXYZ" do
	(
		--print ("Scale XYZ found on "+obj.name)
		sclXYZ obj
	)
	
)

fn findExpressionObjects = 
(
	for obj in objects do
	(
		if classof obj != Targetcamera do 
		(
			if classof obj != IES_Sky do 
			(
				queryFloatExpression obj
			)
		)
	)
-- 	clearListener()
	--now add these found objects to the *Expressions selection set
	
	selCount = getNumNamedSelSets()
	SelName = "*Expressions"
	currItems = #()
	
-- 	for i = 1 to selCount do 
	for i = selCount to 1 by -1 do 
	(
		if getNamedSelSetName i == "*Expressions" then
		(
			SelName = "*Expressions"
			--now record what items already exist in that set
			setItems = getNamedSelSetItemCount i
			
			for a = 1 to setItems do
			(
				thisItem = getNamedSelSetItem i a 
				appendIfUnique currItems thisItem
			)
			
			--now delete that selection set
			deleteItem selectionSets "*Expressions"
		)
		else
		(
			if getNamedSelSetName i == "*EXPRESSIONS" then
			(
				SelName = "*EXPRESSIONS"
				
				setItems = getNamedSelSetItemCount i
				
				for a = 1 to setItems do
				(
					thisItem = getNamedSelSetItem i a 
					appendIfUnique currItems thisItem
				)	
				deleteItem selectionSets "*EXPRESSIONS"
			)
		)
	)
	
	for i = 1 to currItems.count do 
	(
		appendIfUnique objWithExprArray currItems[i]
	)
	selectionSets["*Expressions"] = objWithExprArray

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	

	--- NOW CONFIGURE THE MOTIONBILDEREXPRESSION EXPORT SET
	--THIS BASICALLY STOPS SPRINGS FROM BEING SELECTED
	
	--now add these found objects to the *Expressions selection set
	
	selCount = getNumNamedSelSets()
	SelName = "*MBEXXPRESSION"
	currItems = #()
	
	for i = selCount to 1 by -1 do 
	(
		if getNamedSelSetName i == "*mbExpression" then
		(
			SelName = "*mbExpression"
			--now record what items already exist in that set
			setItems = getNamedSelSetItemCount i
			
			for a = 1 to setItems do
			(
				thisItem = getNamedSelSetItem i a 
				appendIfUnique currItems thisItem
			)
			
			--now delete that selection set
			deleteItem selectionSets "*mbExpression"
		)
		else
		(
			if getNamedSelSetName i == "*MBEXXPRESSION" then
			(
				SelName = "*MBEXXPRESSION"
				
				setItems = getNamedSelSetItemCount i
				
				for a = 1 to setItems do
				(
					thisItem = getNamedSelSetItem i a 
					appendIfUnique currItems thisItem
				)	
				deleteItem selectionSets "*MBEXXPRESSION"
			)
		)
	)
	
	for i = 1 to currItems.count do 
	(
		appendIfUnique noSpringExpObjs currItems[i]
	)
	
	selectionSets["*MBEXXPRESSION"] = noSpringExpObjs
	
	expressionsToExport = #()

	for i = 1 to noSpringExpObjs.count do
	(
		for m = 1 to mbExpressionWhiteList.count do
		(
			shortCount = mbExpressionWhiteList[m].count
			if subString noSpringExpObjs[i].name 1 shortCount == mbExpressionWhiteList[m] do
			(
				appendIfUnique expressionsToExport noSpringExpObjs[i]
			)
		)
	)

	selectionSets["*expressionsToExport"] = expressionsToExport
	
	return objWithExprArray
)

findExpressionObjects()