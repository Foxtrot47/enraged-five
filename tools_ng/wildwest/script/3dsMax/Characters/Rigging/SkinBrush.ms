-- Skel animation loader.
-- Rick Stirling July 2009
-- Modified by Matt Rennie March 2010
-- Rick Stirling April 2011. Transferred to Wildwest 2, UI is now dynamic

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

RsCollectToolUsageData (getThisScriptFilename())

skinbrush = undefined
brushRadius = skinBrushRadius
skinBrushType = false --standard skin brush
skinningBrushTitle = "Skinning Brush: Standard"

fn update_painter_size newSize = (
	thePainterInterface.maxSize = newSize
	thePainterInterface.minSize = newSize/4
	skinbrush.lbl_therad.text = (newSize as string)
)


fn update_painter_strength newStrength = (
	thePainterInterface.maxStr = newStrength
	thePainterInterface.minStr = newStrength/4
	skinbrush.lbl_thestr.text = (newStrength as string)
)

-- This function takes in a button index
fn Radius_ButtonPress buttonIndex = (
	
	-- Button has been pressed, look up the array to find the new radius
	brushRadiusMax = brushRadius[buttonIndex]
	
	update_painter_size brushRadiusMax 
	skinbrush.sld_brushradius.value = brushRadiusMax 
)



-- This function takes in a button index
fn strength_ButtonPress buttonIndex = (
	
	-- Button has been pressed, look up the array to find the new radius
	brushStrengthMax = skinBrushStrength[buttonIndex]
	
	update_painter_strength brushStrengthMax
	skinbrush.sld_brushstrength.value =brushStrengthMax
)






fn paint_toggle buttonstate = ( 
	if $==undefined then (
		messagebox "No object selected"
		skinbrush.btn_togglepaint.state = not skinbrush.btn_togglepaint.state
		)
	else (
		skinOps.paintWeightsButton $.skin	
	)
)

fn blend_toggle buttonstate = (
	if $==undefined then (
		messagebox "No object selected"
		skinbrush.btn_toggleblend.state = not skinbrush.btn_toggleblend.state
		)
	else (	
		if buttonstate == false then $.skin.paintBlendMode = false
		else $.skin.paintBlendMode = true
		thePainterInterface.pressureEnable =false
		thePainterInterface.pressureAffects = 1	
	)
)


fn build_brush_rollout = (
	s = stringStream ""

	-- Open the rollout with a name
	format "rollout skinbrush skinningBrushTitle (\n" to:s
	
	--banner setup
	format "dotNetControl rsBannerPanel \"Panel\" pos:[0,0] width:skinbrush.Width height:32 \n" to:s
	format "local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:\"Ped_SkinBrush\" filename:(getThisScriptFilename()) \n" to:s
	format "on skinbrush open do banner.setup() \n" to:s
	
	-- Add a slider for radius
	format "slider sld_brushradius \"Custom Radius:\" orient:#horizontal ticks:10 range:% \n" skinBrushSliders to:s
	format "on sld_brushradius changed val do update_painter_size val \n" to:s
	format "label lbl_therad \"%\" width:40 height:30 offset:[-25,-55] \n" thePainterInterface.maxSize to:s
	format "checkbox chk_facialSkin \"Facial Skinning Values\" checked:skinBrushType align:#right offset:[0, -35] \n" to:s
	format "on chk_facialSkin changed state do skinBrushRadiusValues state \n" to:s
	format "label lbl_sp1 \"\" width:1 height:10 \n" thePainterInterface.maxSize to:s

	-- Add the buttons from the skin radius array
	for i = 1 to brushRadius.count do (
		-- On the first line I want to use the across parameter for the button layout
		if i == 1 then (buttonacrossparam = ("across:" + brushRadius.count as string))  else buttonacrossparam =""
		format "button btn_size_% \"%\" width:40 height:30 % offset:[0,15] \n" i (brushRadius[i] as string) buttonacrossparam to:s
		format "on btn_size_% pressed do (Radius_ButtonPress %)\n" i i to:s
	)
	
	-- Add a slider for strength
	format "slider sld_brushstrength \"Custom Strength:\" orient:#horizontal ticks:10 range:% \n" skinBrushSliders to:s
    format "on sld_brushstrength changed val do update_painter_strength val \n" to:s
	
	format "label lbl_thestr \"%\" width:40 height:30 offset:[-15,-55] \n" thePainterInterface.maxStr to:s	
	format "label lbl_sp2 \"\" width:1 height:10 \n" thePainterInterface.maxSize to:s	

	-- Add the buttons from the skin strength array
	for i = 1 to skinBrushStrength.count do (
		-- On the first line I want to use the across parameter for the button layout
		if i == 1 then (buttonacrossparam = ("across:" + skinBrushStrength.count as string))  else buttonacrossparam =""
		format "button btn_strength_% \"%\" width:40 height:30 % \n" i (skinBrushStrength[i] as string) buttonacrossparam to:s
		format "on btn_strength_% pressed do (Strength_ButtonPress %)\n" i i to:s
	)

	format "checkbutton btn_togglepaint \"Skin Paint Mode\"  width:100 height:30 across:2 \n" to:s	
	format "on btn_togglepaint changed state do paint_toggle state  \n" to:s
	
	format "checkbutton btn_toggleblend \"Toggle Blend Mode\"  width:100 height:30 across:2 \n" to:s		
	format "on btn_toggleblend changed state do blend_toggle state  \n" to:s
	
	format "button btn_animlibrary \"Animation Loader\"  width:130 height:30\n" to:s		
	format "on btn_animlibrary pressed do (filein ped_animation_library_script)  \n" to:s

	format "on skinbrush open do (rs_dialogPosition \"get\" skinbrush \n banner.setup())\n " to:s
	format "on skinbrush close do (rs_dialogPosition \"set\" skinbrush) \n" to:s
	---------------------------------------------------------
	
	-- Close the rollout
	format ")\n" to:s
	execute (s as string)
)

fn skinBrushRadiusValues checkState =
(
	case of
	(
		(checkState == true):
		(
			destroyDialog skinbrush
			skinBrushType = checkState
			brushRadius = facialSkinBrushRadius
			skinningBrushTitle = "Skinning Brush: Facial"
			build_brush_rollout()
			createDialog skinbrush width:280
		)
		(checkState == false):
		(
			destroyDialog skinbrush
			skinBrushType = checkState
			brushRadius = skinBrushRadius
			skinningBrushTitle = "Skinning Brush: Standard"
			build_brush_rollout()
			createDialog skinbrush width:280
		)
	)
)


-- Create floater 
build_brush_rollout()

createDialog skinbrush width:280