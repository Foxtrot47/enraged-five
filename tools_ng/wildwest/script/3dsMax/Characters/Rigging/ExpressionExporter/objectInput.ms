createdObjects = #()

parentingArray = #( --this is used as a final pass to parent everything
	#(), --nodes to be parented
	#(), --parent nodes
	#(), --positionof node in parent space
	#() --rotation of node in parent space
	)

fn reParentNodes = 
(
	print ("running reparent...")
	nodesToFreeze = #()
	
	for i = 1 to parentingArray[1].count do 
	(
		obj = getNodeByName parentingArray[1][i]
		par = getNodeByName parentingArray[2][i]
		
		parPos = parentingArray[3][i]
		parRot = parentingArray[4][i]		
		
		obj.parent = par
		
		print ("Parented "+obj.name+" to "+par.name)
		
-- 		print ("in coordsys parent $"+obj.name+".Rotation = "+parRot)
		execute ("in coordsys parent $"+obj.name+".Rotation = "+parRot)		
-- 		print ("in coordsys parent $"+obj.name+".Position = "+parPos)
		execute ("in coordsys parent $"+obj.name+".Position = "+parPos)
		
		append nodesToFreeze obj
		
		if (isKindOf obj.baseobject BoneObj) == true do 
		(
			obj.boneEnable=true
		)
	)
	
	select nodesToFreeze
	
	freezeTransform()
)	
	
fn readDummyData dataStr = 
(
	hlpName = dataStr[2]
	
	exists = getNodeByName hlpName
	
	if exists == undefined do 
	(
		
	hlpColor = dataStr[3]
	hlpPosition = dataStr[4]
	hlpRotation = dataStr[5]
	hlpParent = dataStr[6]
	hlpBoxSize = dataStr[7]

	bb1 = dataStr[8]

	--now have to do some stripping of characters on last element
	
	finalStringSize = dataStr[9].count
	bb2 = (substring dataStr[9] 1 (finalStringSize - 2))

		--now we have the data we can create the Dummy object
		
		obj = Dummy pos:[0,0,0] isSelected:off
		obj.Name = hlpName
		
		execute ("$"+obj.name+".wirecolor = "+hlpColor)
		execute ("in coordsys parent $"+obj.name+".Position = "+hlpPosition)
		execute ("in coordsys parent $"+obj.name+".Rotation = "+hlpRotation)
		
		if hlpParent != "undefined" do 
		(
			parObj = getNodeByName hlpParent
			obj.parent = parObj
		)
		
		execute ("$"+obj.name+".boxsize = "+hlpBoxSize)
		
		nubDist = execute ("distance "+bb1+" "+bb2)	
		
		nn = nodeLocalBoundingBox obj

		newNubDist = (distance nn[1] nn[2]) 
		factor = (newNubDist / nubDist)
		
		obj.scale = obj.scale / factor
		
		print ("Created "+obj.name)	
		
		appendIfUnique createdObjects obj
		
			append parentingArray[1] dataStr[2] --node to be parented
			append parentingArray[2] dataStr[6] --parent
			append parentingArray[3] dataStr[4] --pos in parent
			append parentingArray[4] dataStr[5] --rot in parent

	)
)

fn readHelperData dataStr = 
(
	hlpName = dataStr[2]
	
	exists = getNodeByName hlpName
	
	if exists == undefined do 
	(	
		hlpColor = dataStr[3]
		hlpPosition= dataStr[4]
		hlpRotation = dataStr[5]
		hlpParent = dataStr[6]
			
		hlpSize = dataStr[7]
		hlpCenterMarker = dataStr[8]
		hlpAxisTripod = dataStr[9]
		hlpCross = dataStr[10]
		hlpBox = dataStr[11]
		hlpScreenSize = dataStr[12]
		hlpDrawOnTop = dataStr[13]

		finalStringSize = dataStr[13].count --need to strip off some unwanted characters from final string entries in the data file
		hlpDrawOnTop = (substring dataStr[13] 1 (finalStringSize - 2))			
		
		--now we have the data we can create the point
		obj = Point pos:[0,0,0] isSelected:off
		obj.Name = hlpName
		execute ("$"+obj.name+".wirecolor = "+hlpColor)
-- 		execute ("in coordsys world $"+obj.name+".transform = "+hlpTransform)
		execute ("in coordsys parent $"+obj.name+".position = "+hlpPosition)
		execute ("in coordsys parent $"+obj.name+".rotation = "+hlpRotation)	
		
		if hlpParent != "undefined" do 
		(
			parObj = getNodeByName hlpParent
			obj.parent = parObj
		)	
		
		execute ("$"+obj.name+".size = "+hlpSize)
		execute ("$"+obj.name+".centerMarker = "+hlpCenterMarker)
		execute ("$"+obj.name+".axistripod = "+hlpAxisTripod)
		execute ("$"+obj.name+".cross = "+hlpCross)
		execute ("$"+obj.name+".box = "+hlpBox)
		execute ("$"+obj.name+".constantscreensize = "+hlpScreenSize)
		execute ("$"+obj.name+".drawontop = "+hlpDrawOnTop)
		
		print ("Created "+obj.name)
		
		appendIfUnique createdObjects obj
		
			append parentingArray[1] dataStr[2] --node to be parented
			append parentingArray[2] dataStr[6] --parent
			append parentingArray[3] dataStr[4] --pos in parent
			append parentingArray[4] dataStr[5] --rot in parent

	)	
)

fn readBoneData dataStr = 
(
	boneName = dataStr[2]

	exists = getNodeByName boneName
	
	if exists == undefined do 
	(	
		boneColor = dataStr[3]
		bonePosition = dataStr[4]
		boneRotation = dataStr[5]
		boneParent = dataStr[6]
		
		boneWidth = dataStr[7]
		boneHeight = dataStr[8]
		boneTaper = dataStr[9]
		boneLength = dataStr[10]
		boneSideFins = dataStr[11]
		boneSideFinSize = dataStr[12]
		boneSidefinsstarttaper = dataStr[13]
		boneSidefinsendtaper = dataStr[14]
		boneFrontfin = dataStr[15]
		boneFrontfinsize = dataStr[16]
		boneFrontfinstarttaper= dataStr[17]
		boneFrontfinendtaper = dataStr[18]
		boneBackfin = dataStr[19]
		boneBackfinsize = dataStr[20]
		boneBackfinstarttaper = dataStr[21]
		boneBackfinendtaper = dataStr[22]
		boneGenmap = dataStr[23]

		finalStringSize = dataStr[23].count --need to strip off some unwanted characters from final string entries in the data file
		boneGenmap = (substring dataStr[23] 1 (finalStringSize - 2))			
		
		--first off we'll create a default bone
		startPos = [0,0,0]
		endPos = [0,1,0]
		zAxis = [0,1,0]
		obj = BoneSys.createBone startPos endPos zAxis		
		
		--temp turn off bone mode
		obj.boneEnable=false			
			
		obj.name = boneName
		execute ("$"+obj.name+".wireColor = "+boneColor)
-- 		execute ("in coordsys world $"+obj.name+".transform = "+boneTransform)
-- 		execute ("in coordsys parent $"+obj.name+".transform = "+boneTransform)	
		execute ("in coordsys parent $"+obj.name+".Position = "+bonePosition)		
		execute ("in coordsys parent $"+obj.name+".Rotation = "+boneRotation)		
			
		if boneParent != "undefined" do 
		(
			bonePar = getNodeByName boneParent
			obj.parent = bonePar
		)
		
		
		execute ("$"+obj.name+".width = "+boneWidth)
		execute ("$"+obj.name+".height = "+boneHeight) 
		execute ("$"+obj.name+".taper = "+boneTaper) 
		execute ("$"+obj.name+".length = "+boneLength) 
		execute ("$"+obj.name+".sidefins = "+boneSideFins) 
		execute ("$"+obj.name+".sidefinssize = "+boneSideFinSize) 
		execute ("$"+obj.name+".sidefinsstarttaper = "+boneSidefinsstarttaper) 
		execute ("$"+obj.name+".sidefinsendtaper = "+boneSidefinsendtaper) 
		execute ("$"+obj.name+".frontfin = "+boneFrontfin) 
		execute ("$"+obj.name+".frontfinsize = "+boneFrontfinsize) 
		execute ("$"+obj.name+".frontfinstarttaper = "+boneFrontfinstarttaper) 
		execute ("$"+obj.name+".frontfinendtaper = "+boneFrontfinendtaper) 
		execute ("$"+obj.name+".backfin = "+boneBackfin) 
		execute ("$"+obj.name+".backfinsize = "+boneBackfinsize) 
		execute ("$"+obj.name+".backfinstarttaper = "+boneBackfinstarttaper) 
		execute ("$"+obj.name+".backfinendtaper = "+boneBackfinendtaper) 
		execute ("$"+obj.name+".genmap = "+boneGenmap) 	
		
		appendIfUnique createdObjects obj
	
			append parentingArray[1] dataStr[2] --node to be parented
			append parentingArray[2] dataStr[6] --parent
			append parentingArray[3] dataStr[4] --pos in parent
			append parentingArray[4] dataStr[5] --rot in parent
		
		print ("Created "+obj.name)
	)		
)

fn readExprObjData exprObjDataFile = --read in data from the expression object file and recreate any nodes in there which do not exist in the scene.
(
-- 	exprObjDataFile = undefined 
-- 	if mjrExprObjDataFile != undefined do 
-- 	(
-- 		exprObjDataFile = mjrExprObjDataFile
-- 	)
-- 	
-- 	if exprObjDataFile == undefined do 
-- 	(
-- 		exprObjDataFile = getOpenFileName caption:"hir File" types:"hierarchy Data (*.hir)|*.hir|All Files (*.*)|*.*|"
-- 	)
	
	f = openfile exprObjDataFile

	while not eof f do
	(
		thisLine = (filterstring (readLine f) "�")
		
		dataStr = (filterstring (thisLine as string) "|")
		
		itemType = (substring dataStr[1] 4 -1)
		
		if itemType == "DUMMY"	do 
		(
			readDummyData dataStr 

			--need to populate the parenting via the functions instead so it only does it for created nodes
			
			--now we'll populate the parenting array
-- 			append parentingArray[1] dataStr[2]
-- 			append parentingArray[2] dataStr[5]
-- 			append parentingArray[3] dataStr[4]
		)
		
		if itemType == "HELPER"	do 
		(
			readHelperData dataStr 
			
			--now we'll populate the parenting array
-- 			append parentingArray[1] dataStr[2]
-- 			append parentingArray[2] dataStr[5]			
-- 			append parentingArray[3] dataStr[4]
		)

		if itemType == "BONE"	do 
		(
			readBoneData dataStr 
			
			--now we'll populate the parenting array
-- 			append parentingArray[1] dataStr[2]
-- 			append parentingArray[2] dataStr[5]
-- 			append parentingArray[3] dataStr[4]
		)		
	)
	close f
	
	--now we'll run the reparenting funtion
	
	reParentNodes()
)

-- clearListener()

-- exprObjDataFile = undefined

if exprObjDataFile != undefined then 
(
	readExprObjData exprObjDataFile 
)
else 
(
	messagebox ((exprObjDataFile as string)+" .hir file not found")
)