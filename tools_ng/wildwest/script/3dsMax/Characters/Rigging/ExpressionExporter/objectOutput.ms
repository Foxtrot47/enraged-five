hierArray = #() --used to store a hierarchical array of joints

allDataArray = #() --stores data to be output to file

-- helperObjectArray = #()
-- boneObjectArray = #()

-------------------------

fn outputObjDataToFile = 
(
	thisPath = maxFilePath
	thisMaxFile = (filterstring (maxFileName as string) ".")
	thisMaxFile = thisMaxFile[1]
	output_name = (thisPath+thisMaxFile+"_EXPRObjs.hir")	
	
	output_file = createfile output_name		
	
	for i = 1 to allDataArray.count do 
	(
		format (allDataArray[i]+"\n") to:output_file		
	)
	
	close output_file
	
	print ("Hierarchy data output to "+output_name)
)

fn dummyScaleFactor nubNode = --used to figure out how to ensure that dummy nodes get generated at the correct sizes
(
	--nubNode = $SKEL_R_Finger0_NUB
		
	nubPos = nubNode.pos
		
	newNub = Dummy pos:nubPos isSelected:on
	newNub.rotation = nubNode.rotation	

	newNub.boxsize = nubNode.boxsize

	bb = nodeLocalBoundingBox nubNode

	nn = nodeLocalBoundingBox newNub

	nubDist = (distance bb[1] bb[2]) 
	newNubDist = (distance nn[1] nn[2]) 

	-- print ("nubDist = "+(nubDist as string))
	-- print ("newNubDist = "+(newNubDist as string))

	factor = newNubDist / nubDist
		
	-- print ("factor = "+(factor as string))

	-- newNub.scale = (newNub.scale / factor)
	
	delete newNub
	
-- 	return factor

	return bb
)

fn storeHelperData obj = 
(
-- 	print (obj.name+" is a helper")		
	obType = "HELPER"
	hlpName = obj.name
	hlpColor = (obj.wirecolor as string)
-- 	hlpTransform = (in coordsys world obj.transform as string)
	hlpPosition = (in coordsys parent obj.position as string)
	hlpRotation = (in coordsys parent obj.rotation as string)
	hlpParent = "undefined"
	if obj.parent != undefined do (	hlpParent = (obj.parent.name as string))
	
	hlpSize = (obj.size as string)
	hlpCenterMarker = (obj.centermarker as string)
	hlpAxisTripod = (obj.axistripod as string)
	hlpCross = (obj.cross as string)
	hlpBox = (obj.box as string)
	hlpScreenSize = (obj.constantscreensize as string)
	hlpDrawOnTop = (obj.drawontop as string)
	
	sep = "|"
	end = "�"
	
-- 	OBJECTTYPE { OBJECTNAME { WIRECOLOR [ TRANSFORM [ PARENTNODE [ SIZE
		
	hlpData = (obType+sep+hlpName+sep+hlpColor+sep+hlpPosition+sep+hlpRotation+sep+hlpParent+sep+hlpSize+sep+hlpCenterMarker+sep+hlpAxisTripod+sep+hlpCross+sep+hlpBox+sep+hlpScreenSize+sep+hlpDrawOnTop+end)

	append allDataArray hlpData
)	

fn storeDummyData obj = 
(
-- 	print (obj.name+" is a Dummy" )	
	obType = "DUMMY"
	hlpName = obj.name
	hlpColor = (obj.wirecolor as string)
-- 	hlpTransform = (in coordsys world obj.transform as string)
	hlpPosition = (in coordsys parent obj.position as string)
	hlpRotation = (in coordsys parent obj.rotation as string)
	hlpParent = "undefined"
	if obj.parent != undefined do (hlpParent = (obj.parent.name as string))
	
-- 	--now figure out how we need to scale the dummy at recreation	
-- 	factor = dummyScaleFactor obj
-- 	factor = (factor as string)	
	bb = nodeLocalBoundingBox obj
		bb1 = (bb[1] as string)
		bb2 = (bb[2] as string)
		
	hlpBoxSize = (obj.boxsize as string)	
	sep = "|"
	end = "�"
	dumData = (obType+sep+hlpName+sep+hlpColor+sep+hlpPosition+sep+hlpRotation+sep+hlpParent+sep+hlpBoxSize+sep+bb1+sep+bb2+end)

	append allDataArray dumData	
)


fn storeBoneData obj = 
(
-- 	print (obj.name+" is a bone")
	
	obType = "BONE"
	boneName = obj.name
	boneColor = (obj.wirecolor as string)
-- 	boneTransform = (in coordsys world obj.transform as string)
	bonePosition = (in coordsys parent obj.position as string)
	boneRotation = (in coordsys parent obj.rotation as string)
	boneParent = "undefined"
	if obj.parent != undefined do (	boneParent = (obj.parent.name as string))
	
	boneWidth = (obj.width as string)
	boneHeight = (obj.height as string)
	boneTaper = (obj.taper as string)
	boneLength = (obj.length as string)
	boneSideFins = (obj.sidefins as string)
	boneSideFinSize = (obj.sidefinssize as string)
	boneSidefinsstarttaper = (obj.sidefinsstarttaper as string)
	boneSidefinsendtaper = (obj.sidefinsendtaper as string)
	boneFrontfin = (obj.frontfin as string)
	boneFrontfinsize = (obj.frontfinsize as string)
	boneFrontfinstarttaper= (obj.frontfinstarttaper as string)
	boneFrontfinendtaper = (obj.frontfinendtaper as string)
	boneBackfin = (obj.backfin as string)
	boneBackfinsize = (obj.backfinsize as string)
	boneBackfinstarttaper = (obj.backfinstarttaper as string)
	boneBackfinendtaper = (obj.backfinendtaper as string)
	boneGenmap = (obj.genmap as string)
	
	sep = "|"
	end = "�"
	
	boneDat = (obType+sep+boneName+sep+boneColor+sep+bonePosition+sep+boneRotation+sep+boneParent+sep+boneWidth+sep+boneHeight+sep+boneTaper+sep+boneLength+sep+boneSideFins+sep+boneSideFinSize+sep+boneSidefinsstarttaper+sep+boneSidefinsendtaper+sep+boneFrontfin+sep+boneFrontfinsize+sep+boneFrontfinstarttaper+sep+boneFrontfinendtaper+sep+boneBackfin+sep+boneBackfinsize+sep+boneBackfinstarttaper+sep+boneBackfinendtaper+sep+boneGenmap+end)
	
	append allDataArray boneDat
)

fn queryObjectType obj =
(
-- 	print ("Querying "+obj.name)
	
	if classof obj.baseobject == Point do 
	(
		storeHelperData obj
	)		
	
	if classof obj.baseobject == dummy do 
	(
		storeDummyData obj		
	)
	
	if (isKindOf obj.baseobject BoneObj) == true do 
	(
-- 		appendIfUnique boneObjectArray obj
		storeBoneData obj
	)
)

--got this from onmline very VERY fast hierarchy selector
--http://forums.cgsociety.org/archive/index.php/t-734159.html
fn getAllChildren obj &arr:#() =
(
	for c in obj.children do
	(
		append arr c
		append hierArray c
		getAllChildren c arr:arr
	)
	arr
)


fn outputHierarchyData = 
(
	currsel = selection as array
	dumNode = getNodeByName "Dummy01"
		
	if dumNode == undefined then
	(
		messagebox ("WARNING! Please Dummy01 is present and has children.") beep:true
	)
	else 
	(
		select dumNode
		getAllChildren $ arr:(selection as array)
		
		for obj in hierArray do 
		(
			queryObjectType obj
		)
		
-- 		for f in allDataArray do 
-- 		(
-- 			print f
-- 		)
		
		outputObjDataToFile() --now save the data to file
	)	
	
	select currSel
)
-- clearListener()

-- selArray = selection as array
outputHierarchyData()