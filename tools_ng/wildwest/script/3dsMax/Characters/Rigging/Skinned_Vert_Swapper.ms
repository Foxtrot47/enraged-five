-- skinned vert swapper UI

-- The code that does the skin fixing in in this file:
-- Work out the project so we don't have to use hardcoded filein paths etc
-- project = rsconfiggetprojectname()
-- filein ("X:/"+project+"/tools/dcc/current/max2009/scripts/character/includes/FN_common.ms")

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()




global bonelist =#()


-- Pastes weights on a vert from 1 bone to another
-- Originally written to convert Char root to Char Pelvis.
fn convertVert theVert bidfrom bidto =
(

	disableSceneredraw()
	
	-- Empty the arrays
	BoneArray = #()
	BoneNArray =#()
	WeightArray = #()
	
	theSkin = $.modifiers[#skin]
	BNumber = skinOps.getVertexWeightCount theSkin theVert
	BN = BNumber as string
	
	for i = 1 to BNumber do
	(
		boneID = skinOps.getVertexWeightBoneId theSkin theVert i
		boneWeight = skinOps.getVertexWeight theSkin theVert i

		if boneID == bidfrom then
		(
			boneID = bidto
		)
		
		append BoneArray boneID
		append WeightArray boneWeight
	)

	skinOps.ReplaceVertexWeights theskin theVert BoneArray WeightArray
		
	enableSceneredraw()
	redrawViews()
)




fn grabbones =(

	-- need to modify so that we switch to skin modifier automatically
	max modify mode
	modPanel.setCurrentObject $.modifiers[#Skin]
	
	-- build a list of all the in the selected skinned object
	objskin = $.modifiers[#skin]
	howmanybones = skinops.getnumberbones objskin
	
	for i = 1 to howmanybones do
	(
		thebonename = skinops.getbonename objskin i 1
		append bonelist thebonename
	)
	
	print bonelist

)



rollout blank "Skin switchers"
(
	button gbn "Regrab bone names" pos:[10,10] width:110 height:30
	button doit "Do it!" pos:[130,10] width:40 height:30
	progressBar pbProgress "ProgressBar" pos:[7,52] width:164 height:14
	
	dropdownlist frombonelist "From" items:#("")
	dropdownlist tobonelist "To" items:#("")

	on blank open do
	(
		if $.modifiers[#skin] != undefined then
		(
			grabbones()
			frombonelist.items = bonelist
			tobonelist.items = bonelist
			tobonelist.selection=2
		)
		else
		(
			destroyDialog theNewFloater
			messageBox "WARNING! Please ensure you pick an object with a skin modifier!" beep:true
		)
	)

	on gbn pressed do
	(
		grabbones()
		frombonelist.items = bonelist
		tobonelist.items = bonelist
		tobonelist.selection=2
	)
	
	
	on doit pressed do
	(
		-- perform the copy
		frombone = frombonelist.selection
		tobone =tobonelist.selection
		
		print frombone
		print tobone
		
		-- Get vert id  
		theSkin = selection[1].modifiers[#Skin]
		n = skinOps.getNumberVertices theSkin
		
		selVert=0
		
		for i = 1 to n do 
		(
			if (skinops.isVertexSelected theSkin i == 1) then 
			(
				selVert=i
				-- functions convertVert takes vert id, boneid to copy from, bone to copy to
				-- in this case 1=Char and 2=Pelvis
				convertVert selVert frombone tobone
				pbProgress.value = (100.*i/n)
			)
		)	

	)	
	
	
)


selectedObjects = selection as array
if selectedObjects.count != 0 then
(
	if $.modifiers[#skin] != undefined then
	(


	-- Create floater 
	theNewFloater = newRolloutFloater "Skin Switcher" 200 200

	-- Add the rollout section
	addRollout blank theNewFloater

	)
	else
	(
		messagebox "WARNING! Please pick an object with a Skin modifier."
	)
)
else
(
		messagebox "WARNING! Please pick an object."	
)