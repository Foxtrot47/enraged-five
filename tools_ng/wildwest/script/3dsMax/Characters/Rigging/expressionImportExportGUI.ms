global debugPrintVal = false


filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")


filein (RsConfigGetWildWestDir() + "/script/max/Rockstar_North/character/includes/FN_Rigging.ms")
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



if ((expressionExportImportGUI != undefined) and (expressionExportImportGUI.isDisplayed)) do
	(destroyDialog expressionExportImportGUI)

rollout expressionExportImportGUI "Expressions"
(
	checkBox chkDebugPrnt "DebugPrint" checked:false tooltip:"Check to enable debug print output."
	
	button btnExport "Export" width:110 tooltip:"Export expressions from selected."
	button btnImport "Import" width:110 tooltip:"Recreate expressions."

	on expressionExportImportGUI open do
	(
		chkDebugPrnt.state = false
		debugPrintVal = false
		print ("Debug printing defaulting to: "+(debugPrintVal as string))
	)
	
	on chkDebugPrnt changed theState do
	(
		if chkDebugPrnt.state == false then
		(
			debugPrintVal = false
			print "Debug printing disabled."
		)
		else
		(
			debugPrintVal = true
			print "Debug printing enabled."
		)
	)
		
	on btnExport pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/characters/rigging/ExpressionExporter/outputExpressionToXML.ms")
	)
	
	on btnImport pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/characters/rigging/ExpressionExporter/inputExpressionFromXML.ms")		
	)	
)

CreateDialog expressionExportImportGUI width:125 pos:[1450, 100] 