filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

theProjectConfig = RsConfigGetProjBinConfigDir()

toolsRoot = RsConfigGetToolsRootDir()

filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_debug.ms")

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

clearListener()

geoSkelArray = #(
-- 	#($Geo_SKEL_Pelvis, $SKEL_Pelvis),
-- 	#($Geo_SKEL_Spine0, $SKEL_Spine0),
-- 	#($Geo_SKEL_Spine1, $SKEL_Spine1),
-- 	#($Geo_SKEL_Spine2, $SKEL_Spine2),
-- 	#($Geo_SKEL_Spine3, $SKEL_Spine3),	
-- 	#($Geo_SKEL_Neck_1, $SKEL_Neck_1),
-- 	#($Geo_RB_Neck_1, $RB_Neck_1),
-- 	#($Geo_SKEL_Head, $Skel_Head),
-- 	
-- 	#($Geo_SKEL_L_Clavicle, $Skel_L_Clavicle),	
-- 	#($Geo_RB_L_ArmRoll, $RB_L_ArmRoll),
-- 	#($Geo_SKEL_L_UpperArm, $SKEL_L_UpperArm),	
-- 	#($Geo_SKEL_L_Forearm, $SKEL_L_Forearm),	
-- 	#($Geo_RB_L_ForeArmRoll, $RB_L_ForeArmRoll),
-- 	#($Geo_SKEL_L_Hand, $SKEL_L_Hand),
-- 	#($Geo_SKEL_L_Finger00, $SKEL_L_Finger00),
-- 	#($Geo_SKEL_L_Finger01, $SKEL_L_Finger01),
-- 	#($Geo_SKEL_L_Finger02, $SKEL_L_Finger02),
-- 	#($Geo_SKEL_L_Finger10, $SKEL_L_Finger10),
-- 	#($Geo_SKEL_L_Finger11, $SKEL_L_Finger11),
-- 	#($Geo_SKEL_L_Finger12, $SKEL_L_Finger12),
-- 	#($Geo_SKEL_L_Finger20, $SKEL_L_Finger20),
-- 	#($Geo_SKEL_L_Finger21, $SKEL_L_Finger21),
-- 	#($Geo_SKEL_L_Finger22, $SKEL_L_Finger22),
-- 	#($Geo_SKEL_L_Finger30, $SKEL_L_Finger30),
-- 	#($Geo_SKEL_L_Finger31, $SKEL_L_Finger31),
-- 	#($Geo_SKEL_L_Finger32, $SKEL_L_Finger32),
-- 	#($Geo_SKEL_L_Finger40, $SKEL_L_Finger40),
-- 	#($Geo_SKEL_L_Finger41, $SKEL_L_Finger41),
-- 	#($Geo_SKEL_L_Finger42, $SKEL_L_Finger42),

-- 	#($Geo_SKEL_R_Clavicle, $SKEL_R_Clavicle),
-- 	#($Geo_RB_R_ArmRoll, $RB_R_ArmRoll),
-- 	#($Geo_SKEL_R_UpperArm, $SKEL_R_UpperArm),	
-- 	#($Geo_SKEL_R_Forearm, $SKEL_R_Forearm),
-- 	#($Geo_RB_R_ForeArmRoll, $RB_R_ForeArmRoll),
-- 	#($Geo_SKEL_R_Hand, $SKEL_R_Hand),
-- 	#($Geo_SKEL_R_Finger00, $SKEL_R_Finger00),
-- 	#($Geo_SKEL_R_Finger01, $SKEL_R_Finger01),
-- 	#($Geo_SKEL_R_Finger02, $SKEL_R_Finger02),
-- 	#($Geo_SKEL_R_Finger10, $SKEL_R_Finger10),
-- 	#($Geo_SKEL_R_Finger11, $SKEL_R_Finger11),
-- 	#($Geo_SKEL_R_Finger12, $SKEL_R_Finger12),
-- 	#($Geo_SKEL_R_Finger20, $SKEL_R_Finger20),
-- 	#($Geo_SKEL_R_Finger21, $SKEL_R_Finger21),
-- 	#($Geo_SKEL_R_Finger22, $SKEL_R_Finger22),
-- 	#($Geo_SKEL_R_Finger30, $SKEL_R_Finger30),
-- 	#($Geo_SKEL_R_Finger31, $SKEL_R_Finger31),
-- 	#($Geo_SKEL_R_Finger32, $SKEL_R_Finger32),
-- 	#($Geo_SKEL_R_Finger40, $SKEL_R_Finger40),
-- 	#($Geo_SKEL_R_Finger41, $SKEL_R_Finger41),
-- 	#($Geo_SKEL_R_Finger42, $SKEL_R_Finger42),
	
-- 	#($Geo_RB_L_ThighRoll, $RB_L_ThighRoll),
-- 	#($Geo_SKEL_L_Thigh, $SKEL_L_Thigh),
-- 	#($Geo_SKEL_L_Calf, $Skel_L_Calf),
-- 	#($Geo_SKEL_L_Foot, $SKEL_L_Foot),
-- 	#($Geo_SKEL_L_Toe0, $SKEL_L_Toe0),

-- 	#($Geo_RB_R_ThighRoll, $RB_R_ThighRoll),
	#($Geo_SKEL_R_Thigh, $SKEL_R_Thigh),
-- 	#($Geo_SKEL_R_Calf, $SKEL_R_Calf),	
-- 	#($Geo_SKEL_R_Foot, $SKEL_R_Foot),	
	#($Geo_SKEL_R_Toe0, $SKEL_R_Toe0)


)

debugRay = rsta_debug_visual()

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn rsta_setVertsToPlanar meshObj vertArray = 
(
	--maybe get the average distance in the x direction from the bone the mesh is for
	--then iun bonespace set the verta x position to this
	
	local boneNode = getNodeByName (substring meshObj.name 5 -1)
	
	if boneNode != undefined then
	(
		--format ("boneNode: "+(boneNode.name)+"\n")
		local vertPosMax = undefined
		local vertPosMin = undefined
		
		local vPoint = getNodeByName "vPoint"
		if vPoint == undefined do 
		(
			vPoint = point name:"vPoint"
		)
		
-- 		vPminNode = undefined
-- 		vPMaxNode = undefined
		
		for v in vertArray do 
		(
			local vert = meshop.getVert meshObj v --node:<node=unsupplied>
			vPoint.position = vert
			
			local vPos = vert
			
			thisPoint = point pos:vert name:("vInit_"+(v as string)) size:0.05 wireColor:green
			
			thisPoint.parent = boneNode
			tpAll = (in coordsys parent thisPOint.position)
			tp = tpAll[1]
				
			if vertPosMax == undefined do 
			(
				vertPosMax = tp
				--vPmaxNode = thisPOint
			)
			if tp > vertPosMax do
			(
				vertPosMax = tp
				--vPmaxNode = thisPOint
			)
			
			if vertPosMin == undefined do
			(
				vertPosMin = tp
				--vPminNode = thisPOint
			)
			
			if tp < vertPosMin do 
			(
				vertPosMin = tp
				--vPminNode = thisPOint
			)
		)
-- 		
-- 		format ("Using "+vpmaxNode.name+" for max node\n")
-- 		format ("Using "+vpminNode.name+" for min node\n")
		
		--now what we need to do is find the diff between vertPosMax and vertPos Min
		--then get the midPoint between them

		vertPos = ((vertPosMax - vertPosMin) / 2 + vertPosMin)
		
		--format ("vertPos: "+(vertPos as string)+"\n")
		
		for v in vertArray do
		(			
			local pointNode = getNodeByName ("vInit_"+(v as string))
				
			--format ("init pos:"+(in coordsys boneNode pointNode.position as string)+"\n")
			local pointNode2 = point name:("vInit_"+(v as string)+"_B") size:0.05 wirecolor:red
				
			in coordsys boneNode pointNode2.position = [vertPos, pointNode.position[2],pointNode.position[3]]
			local vert = pointNode2.position
				
			--format ("vert: "+(vert as string)+"\n")
			
			meshop.setVert meshObj v vert
			
			delete pointNode
			delete pointNode2
		)
		
		update meshObj
	)
	else
	(
		format ("Couldn't find boneNode for "+meshObj.name+" with name "+(substring meshObj.name 5 -1)+"\n")
	)
)
fn rsta_volumeAdjustViaHit volMesh skinMesh vertsToTest missedVerts rm drawDebugData = 
(
-- 	for vNo = 1 to volMeshVertCount do
	
	local vertArray = #()
	local faceCentreArray = #()
	
	local skinMeshFaceCount = meshop.getNumFaces skinMesh
	
	for f = 1 to skinMeshFaceCount do 
	(
		local faceCentre = meshop.getFaceCenter skinMesh f
		
		append faceCentreArray #(f,faceCentre)
	)
	
	for vNo in vertsToTest do
	(
		local vert = meshop.getVert volMesh vNo
-- 		local vertNormal = -(getNormal volMesh vNo)	
-- 		
-- 		local theRay = ray vert vertNormal
		
		--MAYBE INSTEAD OF USING THE VERT NORMAL WE SHOULD FIND THE CLOSEST FACE CENTRE AND THEN USE THAT TO CREATE THE NORMAL VECTOR
		
		local closestFace = #(10000,undefined)
		
		for f = 1 to faceCentreArray.count do
		(
			local dist = distance vert faceCentreArray[f][2]
			if dist < closestFace[1] do
			(
				closestFace = #(dist,faceCentreArray[f][2])
			)
		)
-- 		
		local theRay = ray vert -(vert - closestFace[2])
-- 		
		local interR = intersectRay skinMesh theRay
-- 		if interR == undefined do 
-- 		(
-- 			--try and flip the normal
-- 			vertNormal = -vertNormal
-- 			theRay = ray vert vertNormal
-- 			interR = intersectRay skinMesh theRay
-- 		)
		
		--format ((vNo as string)+": "+(interR as string)+"\n")
		
		if interR != undefined then
		(
			--SHALL WE DO SOME KIND OF DISTANCE TEST OR SOMETHIGN HERE SO IT ONLY AFFECTS THE MESH VERT
			--IF ITS WITHIN A CERTAIN DISTANCE MAYBE? THIS WAY WE COULD GET AN UPPR TO MOVE CERTAIN BITS OF THE HAND
			--VOLUME PERHAPS.
			
			append vertArray #(vNo,interR.position )

		)
		else
		(		
			append missedVerts vNo
		)
		
		if drawDebugData == true do 
		(
			debugRay.add_ray theRay h:undefined
		)
-- 		update volMesh
	)
	
	--now we have all the hit data we can set the verts
	--dont set the verts whiler testing as this fucks the normals
	for vData in vertArray do 
	(
		meshop.setvert volMesh vData[1] vData[2]
	)
	
	update volMesh
		
	return missedVerts	
)

fn old_rsta_volumeAdjustViaHit volMesh skinMesh vertsToTest missedVerts rm = 
(
	local volMeshVertCount = meshop.getNumVerts volMesh

	for vNo = 1 to volMeshVertCount do
	(
-- 		local volVert = in coordsys world getVert volMesh vNo
		local volVert = getVert volMesh vNo
		local theHits = rm.closestFace volVert


		theNormal = -(getNormal volMesh vNo) --get the normal of the vertex, reverse direction
		theHitsCount = rm.intersectRay volVert theNormal true 		
		if theHitsCount > 0 then --if have hit anything...
		(
			theHitIndex = rm.getClosestHit () --get the index of the closest hit by the ray
			theFace = rm.getHitFace theHitIndex --get the face index corresponding to that indexed hit
-- 			append theFacesArray theFace --add to the face array to select the face...
			
			local hitPos = meshop.getFaceCenter skinmesh theFace
			point pos:hitPos name:("v_"+(vNo as string)) size:0.05
			setVert volMesh vNo hitPos
			update volMesh
				
			format ("updated "+(vNo as string)+"\n")
		)		
		
		
-- 		if theHits > 0 then
-- 		(		
-- 			local theHitIndex = rm.getClosestHit() --get the index of the closest hit by the ray		
-- 			
-- 			local bary = rm.getHitBary theHitIndex

-- 			local faceVerts = getFace skinMesh theHitIndex
-- 			local hitPos = meshop.getFaceCenter skinmesh theHitIndex
-- 			--local hitPos = (((in coordsys world getVert skinMesh faceVerts[1]) * bary.x) + ((in coordsys world getVert skinMesh faceVerts[2]) * bary.y) + ((in coordsys world getVert skinMesh faceVerts[3]) * bary.z))			
-- 			
-- 			m--eshop.setVert volMesh vNo (in coordsys world hitPos)
-- 			--setVert volMesh vNo (in coordsys world hitPos)
-- 			point pos:hitPos name:("v_"+(vNo as string)) size:0.05
-- 			format ("Attempted: setVert $"+volMesh.name+" "+(vNo as string)+" (in coordsys world "+(hitPos as string)+")"+"\n")
-- 			update volMesh
-- 		)
-- 		else
-- 		(
-- 			append missedVerts vNo
-- 		)
	)
	return missedVerts	
)

fn rsta_fitVolumeMeshToSkinMesh volMesh skinMesh drawDebugData = 
(
	--this will use intersect ray to cast a ray from the normal of each vert on volMesh and cast to find the nearest bary hit coord
	-- on skin mesh. Allowing us to then place the volMesh vert at that position so the volMesh fits the skinMesh
	
	local volMeshVertCount = meshop.getNumVerts volMesh
	
	--first we will invert the normals on the skinMesh
-- 	addModifier skinMesh (Edit_Mesh())
	convertToMesh skinMesh
-- 	meshop.flipNormals skinMesh #{1..skinMesh.numfaces}
	update skinMesh --update the mesh
	
	local missedVerts = #()
	
	local vertsToTest = #{1..volMeshVertCount}
	
	local rm = RayMeshGridIntersect ()	
	rm.ClearStats()
	rm.Initialize 10
	rm.addNode skinMesh
	rm.buildGrid()	
	
	missedVerts = rsta_volumeAdjustViaHit volMesh skinMesh vertsToTest missedVerts rm drawDebugData
	
	--rm.PrintStats()
	
	rm.free()

)

fn vtx_rsta_fitVolumeMeshToSkinMesh volMesh skinMesh = 
(
	--this will use intersect ray to cast a ray from the normal of each vert on volMesh and cast to find the nearest bary hit coord
	-- on skin mesh. Allowing us to then place the volMesh vert at that position so the volMesh fits the skinMesh
	
	local volMeshVertCount = meshop.getNumVerts volMesh
	
	local skinMeshVertCount = meshop.getNumVerts skinmesh
	
	
	for vNo = 1 to volMeshVertCount do
	(
		local dist = #(10000, undefined)
		
		local volVert = getVert volMesh vNo
		
		for sNo = 1 to skinMeshVertCount do
		(
			local skinVert = getVert skinmesh sNo
			local distVal = distance volVert skinVert 
			if distVal < dist[1] do 
			(
				dist[1] = distVal
				dist[2] = sNo
			)
		)
		
		--ok now we set the position of vNo to match the vertNo stored in dist[2]
		local skinVert = getVert skinmesh dist[2]
		setVert volMesh vNo skinVert
		update volMesh
	)
)

fn rsta_fitVolumeMeshToSkinMeshSetup skinMesh geoSkelArray drawDebugData =
(
	convertToMesh skinMesh
	for v = 1 to geoSkelArray.count do
	(
		volMesh = geoSkelArray[v][1]
-- 		volMesh = $GEO_Cylinder
		convertToMesh volMesh

-- 		rsta_fitVolumeMeshToSkinMesh volMesh skinMesh 	
		rsta_fitVolumeMeshToSkinMesh volMesh skinMesh drawDebugData
		
		--now see if we need to planarise the end verts
		if geoSkelArray[v][3] != undefined do
		(
			vertArray = geoSkelArray[v][3]
			rsta_setVertsToPlanar volMesh vertArray
		)
	)
	
)	


-- skinMesh = $SkinSphere

geoSkelArrayLower = #(
	#($CYL_RB_R_Thigh, undefined, undefined),
	#($CYL_SKEL_R_Thigh, undefined, undefined),
	#($CYL_SKEL_R_Calf, undefined, undefined),
	#($CYL_SKEL_R_Foot, undefined, undefined),
	#($CYL_SKEL_R_Toe, undefined, undefined),
	
	#($CYL_RB_L_Thigh, undefined, undefined),
	#($CYL_SKEL_L_Thigh, undefined, undefined),
	#($CYL_SKEL_L_Calf, undefined, undefined),
	#($CYL_SKEL_L_Foot, undefined, undefined),
	#($CYL_SKEL_L_Toe, undefined, undefined)	
	
-- 	#($CYL_RB_R_ArmRoll, undefined),
-- 	#($CYL_SKEL_R_UpperArm, undefined),
-- 	#($CYL_SKEL_R_ForeArm, undefined),
-- 	#($CYL_RB_R_ForeArmRoll, undefined)
	)

geoSkelArrayUpper = #(
	#($CYL_SKEL_Spine0, undefined, undefined),	
	#($CYL_SKEL_Spine1, undefined, undefined),	
	#($CYL_SKEL_Spine2, undefined, undefined),
	#($CYL_SKEL_Spine3, undefined, undefined),		

	#($CYL_SKEL_Neck_1, undefined, undefined),	
	#($CYL_RB_Neck_1, undefined, undefined),	
	#($CYL_SKEL_Head, undefined, undefined),	
	
	#($CYL_SKEL_R_Clavicle, undefined, undefined),
	#($CYL_RB_R_ArmRoll, undefined, undefined),
	#($CYL_SKEL_R_UpperArm, undefined, undefined),
	#($CYL_SKEL_R_ForeArm, undefined, undefined),
	#($CYL_RB_R_ForeArmRoll, undefined, #{16,15,14,20,13,21,18,17}),
	
	#($CYL_SKEL_L_Clavicle, undefined, undefined),
	#($CYL_RB_L_ArmRoll, undefined, undefined),
	#($CYL_SKEL_L_UpperArm, undefined, undefined),
	#($CYL_SKEL_L_ForeArm, undefined, undefined),
	#($CYL_RB_L_ForeArmRoll, undefined, #{16,15,14,20,13,21,18,17})
	

	)	
	
geoSkelArray = 	geoSkelArrayUpper
skinMesh = $uppr_000_u

-- geoSkelArray = 	geoSkelArrayLower
-- skinMesh = $lowr_000_u	
	
rsta_fitVolumeMeshToSkinMeshSetup skinMesh geoSkelArray false