--modified from a snippet at form http://forums.cgsociety.org/showthread.php?f=6&t=1214485&highlight=delta+mush

/*

--REAL DELTA MUSH TECHNIQUE

1 a) original mesh : BINDMESH
1) relax copy of BINDMESH: RELAXBINDMESH
2) for each vert in RELAXBINDMESH calculate a local rest coordinate system R based on the normal n and tangent t at the vert
3) Calculate a vector offset V for each vert in the coordsys R. I think this means between the RELAXBINDMESH and BINDMESH
4) Cache these vector offset
5) When mesh deforms during animation, Delta Mush smooths a copy of the points as it did for RELAXBINDMESH. Call this ANIMRELAXMESH
6) Have anone relaxed animated mesh ANIMMESH
6) For each vert in ANIMRELAXMESH a local current coordinate system C is made similar to R
7) The volume can now be restored by moving each point in the ANIMMESH to V in the coordinate system C


*/

plugin simpleMod DeltaMush
   name:"DeltaMush"
   classID:#(0x6904bde3, 0x17c79b7b)
   version:1
   (
	   --we need a bind mesh, bindMesh_anim relaxMesh and relaxMesh anim. These all start of dupes of bindMesh_Anim.
	   --bindMesh is just a base editPoly no other modifiers
	   --relaxMesh is a copy of bindMesh but with a relax modifier on, usually with relax of 1.0 and 3 iterations, then an editpoly mod above
	   --relaxMesh_anim is a copy of bindMesh but with a skin modifier with 1 influence per vert, then an edit poly, then a relax as on relaxMesh, then an edit poly on top
	   --bindMesh anim is bindMesh with a copy of the skin modifier form relaxMesh_anim then an edit poly mod then THIS modifier at the top
	   
		fn GetVertNormPoly PolyA Vert = --Function to get vertex normal based on faces normals
		(
			PolyArr = (polyop.getFacesUsingVert PolyA Vert) as array
			Norm = [0.0,0.0,0.0]
			For kkkk = 1 to PolyArr.count do
			(
				Norm = Norm  + ((polyop.getFaceNormal PolyA PolyArr[kkkk]))-- * (PolyA.transform))
			)
			return (normalize (Norm / (PolyArr.count * 1.0)))
		)
		 
		parameters main rollout:params
		(
			ObjA type:#node ui:Pck01 subAnim:false --relaxed mesh in "skin" pose
			ObjB type:#node ui:Pck02 subAnim:false --non relaxed mesh in "skin" pose
			ObjC type:#node ui:Pck03 subAnim:false --animated relaxed mesh
			
			--we apply this modifier to the none relaxed animated mesh
-- 			Offset type:#float ui:Offset default:0
		)
		rollout params "UVProjectWrapp"
		(
			Pickbutton Pck01 "Relaxed" autoDisplay:true width:80 height:30 tooltip:"Relaxed mesh in bind pose"
			Pickbutton Pck02 "None Relaxed" autoDisplay:true width:80 height:30 tooltip:"None Relaxed Mesh in bind pose"
			Pickbutton Pck03 "Animated" autoDisplay:true width:80 height:30 tooltip:"Animated relaxed mesh"
-- 			spinner Offset "Offset_Ray" type:#Float range:[-100000.0,900000.0,0]
		)
		on map i p do
		(
			if i != 0 and ObjA!=undefined  and ObjB!=undefined  and ObjC!=undefined then
			(
				PointA = polyop.getVert ObjA i --relaxed mesh in bind
				PointB = polyop.getVert ObjB i --none relaxed mesh in bind
				PointC = polyop.getVert ObjC i --animatd relaxed mesh
			  
				NormA = GetVertNormPoly ObjA i
				NormC = GetVertNormPoly ObjC i
				Sub = PointB - PointA
			  
				EdgesX = ((polyop.getEdgesUsingVert ObjC i) as array)
				NewSub = [0.0,0.0,0.0]
				For j = 1 to EdgesX.count do --for every edge create two matrixes. Get vertex pos in one Matrix. And transform it into second. Add them together.
				(
					EdgeVert = ((polyop.getVertsUsingEdge ObjC EdgesX[j])as array)
					NormA02 = normalize ((polyop.getVert ObjA EdgeVert[1]) - (polyop.getVert ObjA EdgeVert[2]))
					NormA03 = normalize (cross NormA02 NormA)
					NormA02 = normalize (cross NormA03 NormA)
					OldMatrix = inverse (matrix3 NormA  NormA02  NormA03  [0,0,0])
				  
					NormC02 = normalize ((polyop.getVert ObjC EdgeVert[1]) - (polyop.getVert ObjC EdgeVert[2]))
					NormC03 = normalize (cross NormC02 NormC)
					NormC02 = normalize (cross NormC03 NormC)
					NewMatrix = matrix3 NormC  NormC02 NormC03 [0,0,0]
					NewSub = NewSub + ((Normalize Sub) * OldMatrix * NewMatrix)
				)
				p = (Normalize NewSub) * (length Sub) + PointC
			)
			else(p)
		)
   )