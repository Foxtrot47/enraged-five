--mrFakeSSDSkinning

/**
you basically move each joint (also excluding movement of its children joints) to calculate how much this affects a vertex. You do that for each joint and vertex combination and apply the weighted normalized value over all joints? 
E.g. 
- joint1 moves vert1 by 2 units
- joint2 moves vert1 by 1 unit
Total movement is 3 units
- joint1 weight is 2/3
- joint2 weight is 1/3

*/
clearListener()
filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_Rigging.ms")

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

jointMapping = undefined





-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn rsta_ApplyDeltaSkinning geo skinDataArray = 
(
	--skinOps.SelectVertices $.modifiers[#Skin] #{1077}
	
	local meshVertCount = polyop.getNumVerts geo
	select geo
	max modify mode
	modPanel.setCurrentObject geo.modifiers[#Skin]
	local skinMod = geo.modifiers[#Skin]

	fn rsta_reNormalize vertexId skinMod =
	(
		skinOps.unNormalizeVertex skinMod vertexID false
	)

	--if the vert has already been weighted tyhen the denormalizing will nacker up so we have to be a bit funky here!
	fn rsta_deNormalize vertexID skinMod = 
	(
		skinOps.unNormalizeVertex skinMod vertexID false
		skinOps.SetVertexWeights skinMod vertexID 1 1.0
		skinOps.unNormalizeVertex skinMod vertexID true
		skinOps.SetVertexWeights skinMod vertexID 1 0.0	
	)	
	
	--first we'll rsta_deNormalize all the weights
	local meshVertCount = polyop.getNumVerts deltaMesh
	
	for vertexID = 1 to meshvertCount do
	(
		rsta_deNormalize vertexID skinMod
	)
	format "Verts denormalised...\n"
	
-- 		local skinnedBones  = #()
-- 		local totalSkinModBones = skinOps.getNumberBones skinMod

-- 		for b = 1 to totalSkinModBones do
-- 		(
-- 			thebonename = skinOps.getBoneName skinMod  b 1
-- 	-- 		realBone = getnodebyname theBoneName
-- 			append skinnedBones thebonename
-- 		)	
	
	for vertId = 1 to skinDataArray.count do 
	(
		thisData = skinDataArray[vertId]
		
		format ("vertId: "+(vertId as string)+" thisData: "+(thisData as string)+"\n")
		
		boneArray = #()
		weightArray = #()
		
		for v = 1 to thisData.count do 
		(
			append boneArray thisData[v][1]
			thebonename = skinOps.getBoneName skinMod thisData[v][1] 1
			
			format (thebonename+"\n")
			append weightArray thisData[v][2]
		)
		
		format ("Attempting to set "+(boneArray.count as string)+" weights.\n")
		
-- 		skinOps.SetVertexWeights skinMod vertId boneArray weightArray
		skinOps.ReplaceVertexWeights skinMod vertId boneArray weightArray
		
-- 			for v = 1 to thisData.count do 
-- 			(			
-- 	-- 			boneId = findItem skinnedBones thisData[v][1]
-- 				boneId = thisData[v][1]
-- 				weight = thisData[v][2]
-- 				
-- 				skinOps.SetVertexWeights skinMod vertId boneId weight
-- 				
-- 				format ("Vert:"+(vertId as string)+" boneId:"+(boneId as string)+" boneName:"+(thisData[v][1] as string)+" weight:"+(weight as string)+"\n")
-- 			)
		
	)
	
	for vertexID = 1 to meshvertCount do
	(
		rsta_reNormalize vertexID skinMod
	)

	format ("Ssd skinning applied.\n")
)


fn rsta_calculateVertDifferences restPose animPose = 
(
	local vertVector = restPose - animPose
	local vertDiff = length vertVector
		
	if abs(vertDiff) * 10 < 0.001 do
	(
		vertDiff = 0 as integer
	)	
	
	return vertDiff
)

fn rsta_calculateVertPositions deltaMesh frame meshVertCount jointToUse = 
(
	local vertSpaceArray = #()
		
	sliderTime = frame as time

	if classof deltaMesh.modifiers[1] != Edit_Poly do
	(
		addModifier deltaMesh (Edit_Poly())
		deltaMesh.modifiers[1].name = "DeltaMush_Edit_Poly"		
	)
	
	for vertNo = 1 to meshVertCount do 
	(	
		local vert = polyop.getVert deltaMesh vertNo --node:space
	
		append vertSpaceArray vert
	)	
	return vertSpaceArray
)

fn rsta_deltaMesh_PrepareTimeLine deltaMesh deltaSkinName skinnedBones =
(
	--first build an array of all the joints in the deltaMesh skin modifier
-- 	local skinnedBones = #()
	
	for s = 1 to deltaMesh.modifiers.count do 
	(
		skinMod = deltaMesh.modifiers[s]
		
		if classof skinMod == Skin do 
		(
			--first off unlink all the bones
			
			for bO = 1 to skinnedBones.count do 
			(
				boneObject = skinnedBones[bO]
				boneObject.parent = undefined
				
				format ("unlinked "+boneObject.name+"\n")
				boneObject.boneEnable = false
			)
			
			select deltaMesh
			max modify mode
			modPanel.setCurrentObject deltaMesh.modifiers[s]
			
			local totalSkinModBones = skinOps.getNumberBones skinMod

			--now we need to ensure the bones are all key less. Then once we are we will rom them
			animationRange = interval 0f (60000 as time) --set timeline to somethign stupidly big as a temp measure
			sliderTime = 0f
			
			removeAllKeys skinnedBones
			
			--now we need to save a bPos file so we can restore this pose at the end.			
			local boneFileName = ("c:/"+deltaSkinName+".bon")
			local skinFileName = ("c:/"+deltaSkinName+".env")			
			
			RSTA_outputSkinBoneData deltaMesh boneFileName
			skinOps.saveEnvelope skinMod skinFileName
			
			--now we need to create a rom for all of the objects in skinnedBones
			local bindPoses = #()
			local totalKeys = 0
			local maxtime = 0f
-- 			local rotAmount = 50				
			local posAmount = 100
			with redraw off ( --try to turn redraw off to speed shit up

				for thisBone in skinnedBones do 
				(
					local thisTran = in coordsys parent thisBone.transform
					
					append bindPoses thisTran
				)
				
				for b = 1 to skinnedBones.count do 
				(	
					slidertime = 0f
					thisBone = skinnedBones[b]
					with animate on (in coordsys parent thisBone.transform = bindPoses[b])
					
					--now ensure there is a key every frame we're gonna need
					for f = 1 to (skinnedBones.count) do 
					(
						slidertime = (f as time)
						with animate on (in coordsys parent thisBone.transform = bindPoses[b])
					)
				)
				
				maxtime = (skinnedBones.count as time)
				
				format ("total of "+(skinnedBones.count as string)+" bones\n")
				format ("All bones zero keyed."+"\n")
				
				animationRange = interval 0f (maxtime as time)
				
				for b = 1 to skinnedBones.count do 
				(	
					local thisBone = skinnedBones[b]

					sliderTime = b as time
					
					--with animate on (thisBone.Rotation.Z_rotation = thisBone.Rotation.Z_rotation + rotAmount)
										
					local initPos = thisBone.position
					with animate on (thisBone.position = ([initPos[1], initPos[2], (initPos[3]+ posAmount)]))
					
					format ("Set key for "+thisBone.name+" at frame "+(sliderTime as string)+"\n")
						
-- 					format ("Initial Pos = "+(initPos as string)+" new pos = "+(thisBone.position as string)+"\n")
				)
				
				sliderTime = 0f
			)

		)
	)
	
	format ("Timeline prepared...\n")
)

fn rsta_fakeSsdSkin deltaMesh deltaSkinName noOfInfluences = 
(
	local start = timeStamp()
	
	local meshVertCount = polyop.getNumVerts deltaMesh
	
	filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/deltaMushSkinning/joitnMappingArray.ms")	

	--first off we need to remove all keys
	sliderTime = 0f
	format ("Removing all keys\n")	
	local objArray = objects as array
	removeAllKeys objArray
	
	local skinnedBones = #()
		
	for i = 1 to jointMapping.count do 
	(
		append skinnedBones jointMapping[i][1]
	)	
			
	--now we need to query the rest positions of all the verts
	local frame = 0f
	
	local restPoseNode = getNodeByName "restPose"
	if restPoseNode == undefined do 
	(
		restPoseNode = point name:"restPose" pos:[0,0,0]
	)
	
	local jointToUse = restPoseNode			
	
	local vertsAtRestPose = #()
	for sBone = 1 to skinnedBones.count do 
	(
		jointToUse = skinnedBones[sBone]
		local theseRestVerts = rsta_calculateVertPositions deltaMesh frame meshVertCount jointToUse
		append vertsAtRestPose theseRestVerts 
	)
	
	format ("vertsAtRestPose[1] count:"+(vertsAtRestPose[1].count as string)+"\n")
	
	rsta_deltaMesh_PrepareTimeLine deltaMesh deltaSkinName skinnedBones
	
	
-- 	break()
	
	--now we need to query the vert positions for each pose
	
	vertsAtPose = #()
	
	for frame = 1 to skinnedBones.count do 
	(
		jointToUse = skinnedBones[frame]
		
		local thisFrameVertData = rsta_calculateVertPositions deltaMesh frame meshVertCount jointToUse
		
		append vertsAtPose thisFrameVertData
	)
	
	format ("vertsAtPose[1] count:"+(vertsAtPose[1].count as string)+"\n")
			
			
	local skinDataArray = #(
		--multi dimensional array 
		--vertNo, jointObject, jointIndex, distance
		
		--we will eventually need to sort it to find each entry for each vertNo
		)		
				
	for vertNo = 1 to vertsAtRestPose[1].count do 
	(
		for frame = 1 to skinnedBones.count do 
		(
			local jointToUse = skinnedBones[frame]
			
			local vertAtRest = vertsAtRestPose[frame][vertNo]
			
			local vertAtPose = vertsAtPose[frame][vertNo]
						
			if vertAtPose != vertAtRest then
			(
				thisSkinDataInfo = #(vertNo, jointToUse, frame)
				
				vertDistance = rsta_calculateVertDifferences vertAtRest vertAtPose

				if vertDistance > 0 then
				(
					appendIfUnique skinDataArray jointToUse
					
					append thisSkinDataInfo vertDistance
					append skinDataArray thisSkinDataInfo 
					
-- 					format ("\n"+skinnedBones[frame].name+"(frame "+(frame as string)+") vert:"+(vertNo as string)+"\n")
-- 					format ("vertAtRest             : "+(vertAtRest as string)+"\n")
-- 					format ("vertAtPose             : "+(vertAtPose as string)+"\n")					
					
-- 					format ((vertNo as string)+" affected by "+jointToUse.name+" at frame "+(frame as string)+"\n")
					
-- 					if vertNo == 865 do 
-- 					(
-- 						format ((vertNo as string)+" "+(thisSkinDataInfo as string)+"\n")
-- 					)
					
-- 					format ("\n")	
				)							
			)
		)
	)
							
-- 	format ("skinDataArray\n")
-- 	for sd = 1 to skinDataArray.count do 
-- 	(
-- 		format ((skinDataArray[sd] as string)+"\n")
-- 	)
		-- 	
	local modifiedSkinDataArray = #()
	
	for vertNo = 1 to meshVertCount do
	(
		local thisVertexData =#()
		for thisData in skinDataArray do 
		(
			if thisData[1] == vertNo do 
			(
				local newData = #()
				append newData thisData[3] --bone index
				append newData thisData[4] --distance
				
				append thisVertexData newData
			)
		)
		
		append modifiedSkinDataArray thisVertexData
	)
	
	for m = 1 to modifiedSkinDataArray.count do 
	(
		--format ("vert: "+(m as string)+" "+(modifiedSkinDataArray[m] as string)+"\n")
		
		--now we need to find the numbr of elements  inside modifiedSkinDataArray[m]
		--then usinng the maxInfluences variable find that number of biggest distances.
		--then we add them together and convert into usable skin weights
		--(the reason we dont just divide by 100 is we may have cases where we appear to get more than the
		--max allowable influences)
		
		--FOR NOW I'M NOT GOING TO DO THE SIZE TRIMMING
		
		local infTotal = 0
		
		for inf = 1 to modifiedSkinDataArray[m].count do 
		(
			infTotal = infTotal + modifiedSkinDataArray[m][inf][2]
		)
		
		for inf = 1 to modifiedSkinDataArray[m].count do 
		(
			local thisData = modifiedSkinDataArray[m][inf]
			local normalisedVal = thisData[2] / infTotal
			thisData[2] = normalisedVal 
		)
	
		format ("vert: "+(m as string)+" "+(modifiedSkinDataArray[m] as string)+"\n")
	)
		
	--now reparent the bones
	for i = 1 to jointMapping.count do 
	(
		jointMapping[i][1].parent = jointMapping[i][2]
	)		
	
	--now we can clone the deltaMesh and delete all the modifiers apart from skin
	--then we can use modifiedSkinDataArray to set the weights
	local deltaSkin = getNodeByName (deltaMesh.name+"_skinned")		
	if deltaSkin != undefined do 
	(
		delete deltaSkin
	)
	
	maxOps.cloneNodes deltaMesh cloneType:#copy actualNodeList:&c newNodes:&nnl
	deltaSkin = nnl[1]	
	deltaSkin.Name = (deltaMesh.name+"_skinned")		
	
-- 	for modifIndex = deltaSkin.modifiers.count to 1 by -1 do 
	for modifIndex = deltaSkin.modifiers.count to 1 by -1 do 
	(
		deletemodifier deltaSkin modifIndex
	)	
	
	addModifier deltaSkin (Skin ())
	deltaSkin.modifiers[#Skin].bone_Limit = 4

	local skinModifierBonesCount = skinOps.getNumberBones DeltaMesh.modifiers[#Skin]
	local skinModifierBones = #()
	local skinMod = deltaMesh.modifiers[#Skin]
	
	select deltaMesh
	max modify mode
	modPanel.setCurrentObject deltaMesh.modifiers[#Skin]	
		
	for b = 1 to skinModifierBonesCount do
	(
		thebonename = skinOps.getBoneName skinMod  b 1
		realBone = getnodebyname theBoneName
		append skinModifierBones realBone
	)	

	select deltaSkin
	max modify mode
	modPanel.setCurrentObject deltaSkin.modifiers[#Skin]	
	
	for i = 1 to skinModifierBones.count do
	(		
		format ("boneToAdd: "+(skinModifierBones[i] as string)+"\n")
		
		boneUpdateInt = undefined
		if i != skinModifierBones.count then
		(
			boneUpdateInt = 1
		)
		else
		(
			boneUpdateInt = 0
		)
		
		skinOps.addbone deltaSkin.modifiers[#Skin] skinModifierBones[i] boneUpdateInt
	)
	
	rsta_ApplyDeltaSkinning deltaSkin modifiedSkinDataArray
)


deltaMesh = $bindMesh_bindAnim   --this is the mesh with the delta mush on as we want to track how it is deforming
noOfInfluences = 4
rsta_fakeSsdSkin deltaMesh (deltaMesh.name+"_DeltaSkinMesh") noOfInfluences
