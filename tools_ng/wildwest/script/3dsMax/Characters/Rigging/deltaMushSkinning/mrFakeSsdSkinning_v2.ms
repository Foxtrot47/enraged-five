--mrFakeSSDSkinning

/**
you basically move each joint (also excluding movement of its children joints) to calculate how much this affects a vertex. You do that for each joint and vertex combination and apply the weighted normalized value over all joints? 
E.g. 
- joint1 moves vert1 by 2 units
- joint2 moves vert1 by 1 unit
Total movement is 3 units
- joint1 weight is 2/3
- joint2 weight is 1/3

*/
clearListener()
filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_Rigging.ms")

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

jointMapping = undefined





-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn rsta_calculateVertDifferences restPose animPose = 
(
	local vertVector = restPose - animPose
	local vertDiff = length vertVector
		
	if abs(vertDiff) * 10 < 0.001 do
	(
		vertDiff = 0 as integer
	)	
	
	return vertDiff
)

fn rsta_calculateVertPositions deltaMesh frame meshVertCount = 
(
	local vertSpaceArray = #()
		
	sliderTime = frame as time

	if classof deltaMesh.modifiers[1] != Edit_Poly do
	(
		addModifier deltaMesh (Edit_Poly())
		deltaMesh.modifiers[1].name = "DeltaMush_Edit_Poly"		
	)

	for vertNo = 1 to meshVertCount do 
	(	
		local vert = polyop.getVert deltaMesh vertNo --node:space
	
		append vertSpaceArray vert
	)	
	return vertSpaceArray
)

fn rsta_deltaMesh_PrepareTimeLine deltaMesh deltaSkinName =
(
	--first build an array of all the joints in the deltaMesh skin modifier
	local skinnedBones = #()
	
	for s = 1 to deltaMesh.modifiers.count do 
	(
		skinMod = deltaMesh.modifiers[s]
		
		if classof skinMod == Skin do 
		(
			select deltaMesh
			max modify mode
			modPanel.setCurrentObject deltaMesh.modifiers[s]
			
			local totalSkinModBones = skinOps.getNumberBones skinMod

			for b = 1 to totalSkinModBones do
			(
				thebonename = skinOps.getBoneName skinMod  b 1
				realBone = getnodebyname theBoneName
				append skinnedBones realBone
			)	

			--now we need to ensure the bones are all key less. Then once we are we will rom them
			animationRange = interval 0f (60000 as time) --set timeline to somethign stupidly big as a temp measure
			sliderTime = 0f
			
			removeAllKeys skinnedBones
			
			--now we need to save a bPos file so we can restore this pose at the end.			
			local boneFileName = ("c:/"+deltaSkinName+".bon")
			local skinFileName = ("c:/"+deltaSkinName+".env")			
			
			RSTA_outputSkinBoneData deltaMesh boneFileName
			skinOps.saveEnvelope skinMod skinFileName
			
			--now we need to create a rom for all of the objects in skinnedBones
			local bindPoses = #()
			local totalKeys = 0
			local maxtime = 0f
			local rotAmount = 50				
			with redraw off ( --try to turn redraw off to speed shit up

				for thisBone in skinnedBones do 
				(
					local thisTran = in coordsys parent thisBone.transform
					
					append bindPoses thisTran
				)
				
				for b = 1 to skinnedBones.count do 
				(	
					slidertime = 0f
					thisBone = skinnedBones[b]
					with animate on (in coordsys parent thisBone.transform = bindPoses[b])
					
					--now ensure there is a key every frame we're gonna need
					for f = 1 to (skinnedBones.count) do 
					(
						slidertime = (f as time)
						with animate on (in coordsys parent thisBone.transform = bindPoses[b])
					)
					slidertime = 0f --need to set this back to 0
				)
				
				maxtime = (skinnedBones.count as time)
				
				format ("total of "+(skinnedBones.count as string)+" bones\n")
				format ("All bones keyed."+"\n")
				
				
				
				animationRange = interval 0f (maxtime as time)
				
				for b = 1 to skinnedBones.count do 
				(	
					local thisBone = skinnedBones[b]

					sliderTime = sliderTime + 1f
					
					with animate on (thisBone.Rotation.Z_rotation = thisBone.Rotation.Z_rotation + rotAmount)
				)
				
				sliderTime = 0f
			)
		)
	)
	
	format ("Timeline prepared...\n")
)

fn rsta_fakeSsdSkin deltaMesh deltaSkinName noOfInfluences = 
(
	local start = timeStamp()
	
	local meshVertCount = polyop.getNumVerts deltaMesh
	
	rsta_deltaMesh_PrepareTimeLine deltaMesh deltaSkinName
		
	--now we need to query the rest positions of all the verts
	local frame = 0f
	
	local restPoseNode = getNodeByName "restPose"
	if restPoseNode == undefined do 
	(
		restPoseNode = point name:"restPose" pos:[0,0,0]
	)
	local jointToUse = restPoseNode	
	
	filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/deltaMushSkinning/joitnMappingArray.ms")	
			
	local vertsAtRestPose = rsta_calculateVertPositions deltaMesh frame meshVertCount
	
-- 	format ("restPose\n")
-- 	for v = 1 to vertsAtRestPose.count do 
-- 	(
-- 		format ((v as string)+": "+(vertsAtRestPose[v] as string)+"\n")
-- 	)
-- 	
-- 	format "---------------------------------------------------\n"
	local skinnedBones = #()
	
	for i = 1 to jointMapping.count do 
	(
		append skinnedBones jointMapping[i][1]
	)	
	
	--now we need to query the vert positions for each pose
	
	vertsAtPose = #()
	
	for frame = 1 to skinnedBones.count do 
	(
		jointToUse = skinnedBones[frame]
		
		local thisFrameVertData = rsta_calculateVertPositions deltaMesh frame meshVertCount
		
		append vertsAtPose thisFrameVertData
	)
	
-- 	for f = 1 to vertsAtPose.count do 
-- 	(
-- 		format ("f: "+(f as string)+" "+skinnedBones[f].name+"\n")
-- 		for v = 1 to vertsAtPose[f].count do 
-- 		(
-- 			format ((v as string)+": "+(vertsAtPose[f][v] as string)+"\n")
-- 		)
-- 	)
	
	--now using the childNode and parentNodes from the jointMapping, we can compare distances
	
	skinDataArray = #(
		--multi dimensional array 
		--vertNo, jointObject, jointIndex, distance
		
		--we will eventually need to sort it to find each entry for each vertNo
		)
	
-- 	for s = 1 to skinnedBones.count do 
-- 	(
-- 		format ((s as string)+": "+skinnedBones[s].name+"\n")
-- 	)
	
-- 	for frame = 1 to skinnedBones.count do
-- 	(	

	for vertNo = 1 to vertsAtRestPose.count do 
	(		
		for frame = 1 to skinnedBones.count do
		(			
			jointToUse = jointMapping[frame][1]
		
-- 		for vertNo = 1 to vertsAtRestPose.count do 
-- 		(		
			realVertNo = vertNo - 1
			vertAtRest = vertsAtRestPose[vertNo]
			
			jointParent = jointMapping[frame][2]
			jointChild = jointMapping[frame][3]

			--using the jointParent and jointChild we need to do a finditem in skinned bones to get those indexes so we can see what their values are at this frame
			--this should help us decide if this vert has moved relative to its parent and its child.
			--if its the same vs child and not the same vs parent then this joint has moved the vert
			
			jointParIndex = finditem skinnedBones jointParent
			if jointParent != undefined then
			(
				jointChiIndex = finditem skinnedBones jointChild
				
				if jointChiIndex != 0 then
				(
					vertAtPose = vertsAtPose[frame][vertNo]
					vertAtPoseParent = vertsAtPose[jointParIndex][vertNo]
					vertAtPoseChild = vertsAtPose[jointChiIndex][vertNo]
					
					format (skinnedBones[frame].name+" vert:"+(vertNo as string)+"\n")
					format ("vertAtRest      : "+(vertAtRest as string)+"\n")
					format ("vertAtPose      : "+(vertAtPose as string)+"\n")
					format ("vertAtPoseParent: "+(vertAtPoseParent as string)+"\n")
					format ("vertAtPoseChild : "+(vertAtPoseChild as string)+"\n")
					
					if vertAtPose != vertAtRest then
					(
						if vertAtPoseChild == vertAtRest then
						(
							if vertAtPoseParent != vertAtPose then 
							(
-- 								format "NEED TO TEST DISTANCES...\n"
								thisSkinDataInfo = #(vertNo, jointToUse, frame)
								
-- 								format ("vertAtRest: "+(vertAtRest as string)+"\n")
-- 								format ("vertAtPose: "+(vertAtPose as string)+"\n")
								vertDistance = rsta_calculateVertDifferences vertAtRest vertAtPose
								
-- 								format ("vertDistance: "+(vertDistance as string)+"\n")
								if vertDistance > 0 then
								(
									append thisSkinDataInfo vertDistance
									append skinDataArray thisSkinDataInfo 
									
									format ("F) "+(vertNo as string)+" affected by "+jointToUse.name+"\n")
								)
								else
								(
-- 									format ("E) "+skinnedBones[frame].name+" vert:"+(realvertNo as string)+" distance between rest "+(vertAtRest as string)+" & pose "+(vertAtPose as string)+" is too small\n")
								)
							)
							else
							(
-- 								format ("D) "+skinnedBones[frame].name+" vert:"+(realvertNo as string)+" vertAtPoseParent "+(vertAtPoseParent as string)+" == vertAtPose "+(vertAtPose as string)+"\n")	
							)
						)
						else
						(
-- 							format ("C) "+skinnedBones[frame].name+" vert:"+(realvertNo as string)+" vertAtPoseChild "+(vertAtPoseChild as string)+" != vertAtPose "+(vertAtPose as string)+"\n")	
						)
					)
					else
					(
-- 						format ("B) "+skinnedBones[frame].name+" vert:"+(realvertNo as string)+" vertAtPose "+(vertAtPose as string)+" == vertAtRest "+(vertAtRest as string)+"\n")
					)
				)
				else
				(
-- 					format ("A) Skipping looking for child for "+jointToUse.name+" as child "+jointChild.name+" not found as a skinned bone\n" )
				)
			)
			else
			(
-- 				format ("Skipping for "+jointToUse.name+" as parent is undefined\n")
			)
		)
	)
	
	format ("skinDataArray\n")
	for sd = 1 to skinDataArray.count do 
	(
		format ((skinDataArray[sd] as string)+"\n")
	)
-- 	
-- 	local modifiedSkinDataArray = #()
-- 	
-- 	for vertNo = 1 to meshVertCount do
-- 	(
-- 		local thisVertexData =#()
-- 		for thisData in skinDataArray do 
-- 		(
-- 			if thisData[1] == vertNo do 
-- 			(
-- 				local newData = #()
-- 				append newData thisData[3] --bone index
-- 				append newData thisData[4] --distance
-- 				
-- 				append thisVertexData newData
-- 			)
-- 		)
-- 		
-- 		append modifiedSkinDataArray thisVertexData
-- 	)
-- 	
-- 	for m = 1 to modifiedSkinDataArray.count do 
-- 	(
-- 		format ("vert: "+(m as string)+" "+(modifiedSkinDataArray[m] as string)+"\n")
-- 	)
)


deltaMesh = $bindMesh_bindAnim   --this is the mesh with the delta mush on as we want to track how it is deforming
noOfInfluences = 4
rsta_fakeSsdSkin deltaMesh (deltaMesh.name+"_DeltaSkinMesh") noOfInfluences
