-- UV/Texture based storage
-- Tool to save and load vertex data to a texture set

-- Rick Stirling
-- Rockstar North
-- August 2012



filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
--RsCollectToolUsageData (getThisScriptFilename())


-- Data structures
TBS_UVimageOutputSize = #(256,512,1024)
TBS_paddingSizeArray = #(16,10,4)

-- Dump textures to here, this is for debug only
TBS_skinTextureFolder = "c:/temp/tbs_textures/" 

-- Where will the data be loaded from?
TBS_InputDataFolder = (RsConfigGetWildWestDir() + "assets/texture_based_data/")


struct skinnedVertData
(
	vertMeshIndex = undefined,
	vertSkinArray = undefined,
	vertUVArray = #()
)


-- Function that collects all the map/UV verts from the mesh verts
-- Returns data as an array where each index corresponds to the mesh index
-- Save the UV coordinate too
fn TBS_meshVertToUVVert meshobj UVChannel =
(
	
	-- Work on a mesh version of the geometry
	tempMesh = snapshotasmesh meshobj
	numMeshFaces = tempMesh.numFaces
	

	local UVMappingArray = #()
	
	-- Run through the mesh and collect the UV data for each face 
	-- Each triangle will have 3 verts, so 3 values will be returned
	startTime = timeStamp()
	for faceIndex=1 to numMeshFaces do
 	(
 		theMeshFace = getFace tempMesh faceIndex
 		theTFace = getTVFace tempMesh faceIndex
		
		-- The Mesh face has 3 points, x, y, and z
		-- These correspond to the x, y and z of the tvert 
		-- Store the mapping plus the U, V and W of the Tvert
		local VertMapping = #(theMeshFace.x as integer, theTFace.x as integer, (getTVert tempMesh theTFace.x))
 		append UVMappingArray VertMapping
			
		local VertMapping = #(theMeshFace.y as integer, theTFace.y as integer, (getTVert tempMesh theTFace.y))
 		append UVMappingArray VertMapping

		local VertMapping = #(theMeshFace.z as integer, theTFace.z as integer, (getTVert tempMesh theTFace.z))
 		append UVMappingArray VertMapping		
 	)
	
	endTime = timeStamp()
	format "Collection mapping time :% seconds\n" ((endTime-startTime )/ 1000)
	
	
	-- The mapping array is very full, I need to make is as small as possible
	local parsedUVMappingArray = #(UVMappingArray[1])
	local mappingcount = UVMappingArray.count
	
	startTime = timeStamp()
	
	-- Loop through the collected data, collecting any unique vert/texture vert mappings
	-- This can get slow with large data sets - ~10 seconds for 3000 verts
	for UVMappingArrayIndex = 2 to mappingcount do
 	(
		-- Grab the mapped entry
		local theMappingEntry = UVMappingArray[UVMappingArrayIndex]

		-- Set a found flag to be false
		matchfound = false
		
		-- Check each in sequence 
		currentParsedCount = parsedUVMappingArray.count
		for checkIndex = 1 to currentParsedCount do 
		(
			local chkMappingEntry = parsedUVMappingArray[checkIndex]
			
			-- Have already stored that mesh vert?
			if theMappingEntry[1] == chkMappingEntry[1] then
			(
				-- Have we go that Texture vert too?
				if theMappingEntry[2] ==  chkMappingEntry[2] then 
				(
					matchfound = true
				)	
			)	
		)	
		
		-- If no match is found then we want to save this data
		if matchfound == false then append parsedUVMappingArray theMappingEntry
			
	) -- End parsing

	endTime = timeStamp()
	format "Search time :% seconds\n" ((endTime-startTime )/ 1000)
	
	
	-- Sort this parsed array to make things easier later
	qsort parsedUVMappingArray sortArraybySubIndex subIndex:1
	
	parsedUVMappingArray  -- return this mapping
)	




-- Collect the vert data 
-- Takes an object and returns a breakdown of the verts based on input criteria/flags
-- SEPT 29th : Need  to add an input array of the vertex colour channels we want to look at later 

fn TBS_CollectMeshData meshObject theObjectUVChannel collectSkinFlag collectVCFlag &theMeshBonelist =
(


	-- If the mesh is skinned, then save out the data.
	if collectSkinFlag  == true then 
	(
		
		
	-- ADD THIS BACK IN LATER	
	-- 	if isMeshSkinned skinnedObject == false then
	-- 	(	
	-- 		messagebox ("Could not find a skin modifier on " + skinnedObject.name)
	-- 	)
	-- 	
		
		
		-- Collect the skin data for all the verts on the object
		local theObjectSkinModifer = meshObject.modifiers[#skin]
		max modify mode -- I need to have the modifier panel open ???
		modPanel.setCurrentObject theObjectSkinModifer
		local numObjectVerts = skinOps.getNumberVertices theObjectSkinModifer 
		
		
		-- Having a bone list will be useful
		-- This will be used to create the textures later
		local skinnedObjectActiveBoneList = #()
		
		-- Loop through all the verts in index order and for each skinned bone store the skin modifier index, the bone name and the bone weights
		-- The skin modifier index is probably useless since that might change
		-- The bone name will be fine
		local skinnedVertListData = #()

		for vertIndex = 1 to numObjectVerts do 
		(
			-- Create a struct to hold this data 
			skinnedVertDataInstance = skinnedVertData vertMeshIndex:vertIndex
			
			-- How many bones is this vert skinned to?
			numberWeights = skinOps.getVertexWeightCount theObjectSkinModifer vertIndex 
			
			-- Create an array to hold the vertindex and then an array of weights
			vertSkinData = #()
			
			-- For each bone in the weight list, grab the weight data
			-- Get the name of the bone too, useful for debugging
			-- Also convert the weight to an RGB while here, we're in the loop anyway
			for weightIndex = 1 to numberWeights do
			(
				boneID = skinOps.getVertexWeightBoneId theObjectSkinModifer vertIndex weightIndex 
				boneName = skinOps.GetBoneName theObjectSkinModifer boneID 1
				boneWeight = skinOps.getVertexWeight theObjectSkinModifer vertIndex weightIndex

				-- Store this data
				append vertSkinData #(boneID, boneName, boneWeight)
				
				appendIfUnique skinnedObjectActiveBoneList boneName
			) -- End bone loop
			
			-- Store this vert weight data
			skinnedVertDataInstance.vertSkinArray  = vertSkinData
			
			append skinnedVertListData skinnedVertDataInstance
		) -- End vertex loop
		
		

		
		
		-- Now we need to map this skin data to the UV data
		-- This will be used so that for each mesh vert I know what UV vert to use 
		local meshTextureVertMapping = TBS_meshVertToUVVert meshObject theObjectUVChannel

		-- For each vert, find the corresponsing map vert and the UV coordinate
		-- skinnedVertListData contains a vert list, so skinnedVertListData[1] is vert 1, skinnedVertListData[4] is vert 4 etc.
		local theCount = skinnedVertListData.count
		for meshVertIndex = 1 to theCount do 
		(
			for textureVertIndex = 1 to meshTextureVertMapping.count do 
			(
				if meshTextureVertMapping[textureVertIndex][1] == meshVertIndex then 
				(
					append skinnedVertListData[meshVertIndex].vertUVArray meshTextureVertMapping[textureVertIndex]
				)	
			)	
		) -- end vertex loop
		
		
	)
	
	theMeshBonelist = skinnedObjectActiveBoneList  -- return this reference
	skinnedVertListData -- return the collected data
	
)	


-- Function that creates a skinweight texture for each bone in the skin modifier
fn TBS_writeSkinToTexture theMeshBonelist collectedMeshData skinTextureFolder saveDataName skinTextureSize paddingSizeArray = 
(
	-- Make sure the folder exists  
	outputTextureFolder = skinTextureFolder + saveDataName + "/"
	makeDir outputTextureFolder all:true
	
	-- Loop through the bones, there will be a texture per bone 
	for boneIndex = 1 to theMeshBonelist.count do 
	(
		-- Create a texture of the specified size and fill it with black 
		theTextureName = theBonelist[boneIndex] + ".tga"
		theSkinWeightImage = bitmap skinTextureSize skinTextureSize color:black
		theSkinWeightImage.filename = outputTextureFolder + theTextureName
		
		
		
		-- Now write the skin weights into this texture 
		-- This will be fuzzy, the artist will clean this up later 
		
		-- So, loop through all the verts and see if they use this bone 
		for theVertIndex = 1 to collectedMeshData.count do 
		(
			local theMeshVert = collectedMeshData[theVertIndex]
			
			-- How many skinned verts?
			for skinnedVerIndex = 1 to theMeshVert.vertSkinArray.count do 
			(
				-- Compare the bone name in the skin data to the bone we are writing the texture for 
				local theSkinnedBone = theMeshVert.vertSkinArray[skinnedVerIndex][2]
				if theSkinnedBone == theBonelist[boneIndex] then
				(
					-- The skin weight needs to be converted to RGB
					local boneWeight = theMeshVert.vertSkinArray[skinnedVerIndex][3]
					local textureRGB = boneWeight * 255
					
					-- Use a the padding array input parameter to add the fuzziness to the skin data blocks
					-- Create this as a new bitmap, then paste it into the bigger skin data image
					theSkinWeightBlockRGB = bitmap paddingSizeArray[1] paddingSizeArray[1] color:(color textureRGB 0 0)
					theSkinWeightBlockG = bitmap paddingSizeArray[2] paddingSizeArray[2] color:(color textureRGB textureRGB 0)
					theSkinWeightBlockB = bitmap paddingSizeArray[3] paddingSizeArray[3] color:(color textureRGB textureRGB textureRGB)
										
					g_offset = (paddingSizeArray[1] - paddingSizeArray[2])/2
					b_offset = g_offset + ((paddingSizeArray[2] - paddingSizeArray[3])/2)
					
					pasteBitmap theSkinWeightBlockG theSkinWeightBlockRGB (box2 0 0 paddingSizeArray[2] paddingSizeArray[2]) [g_offset, g_offset]
					pasteBitmap theSkinWeightBlockB theSkinWeightBlockRGB (box2 0 0 paddingSizeArray[3] paddingSizeArray[2]) [b_offset, b_offset]
					
					--display theSkinWeightBlockRGB
					
					-- There might be multiple texture co-ordinates for the mesh vert 
					for tvertIndex = 1 to theMeshVert.vertUVArray.count do 
					(
						local UVCoordinate = theMeshVert.vertUVArray[tvertIndex][3]
						local paddingOffset = (paddingSizeArray[1] - 1)/2
						local bitmapUVCoordinate =  [((UVCoordinate.x * skinTextureSize) - paddingOffset), ((skinTextureSize - (UVCoordinate.y * skinTextureSize)) - paddingOffset)] 
						pasteBitmap theSkinWeightBlockRGB theSkinWeightImage (box2 0 0 paddingSizeArray[1] paddingSizeArray[1]) bitmapUVCoordinate 
						
					) -- end tvert loop	
				) -- end found bone loop	
			) -- end skinned vert loop	
		) -- end mesh vert loop	
		
		-- Save the texture
		save theSkinWeightImage
	)	
	
	
	-- Let the user know that this has worked
	messagebox ("Skin texture data written to the folder " + outputTextureFolder)
	
)


-- Function to create a texture that excludes certain verts from being skinned
fn TBS_writeExclusionVertTexture skinTextureFolder skinTextureSize =
(

	
)	


-- Function that takes a skinned mesh and loads skin weights onto it 
fn TBS_loadSkinToTexture meshObject collectedMeshData skinWeightConfig = 
(
	if collectedMeshData == undefined then
	(
		messagebox "There are no saved skinweights of this mesh" title:"ERROR"
		return false
	)
	-- The incoming bone image names will not match the mesh bones in many cases 
	-- The bone mapping are stored in an XML file
	
	pushPrompt "Loading Skin Weights..."
	
	-- Need to load the config XML file 
	skinFolder = TBS_InputDataFolder + skinWeightConfig +"/"
	skinImageFolder = skinFolder + "image_files/"
	skinConfigFile = skinFolder + "skinning_mapping.xml"
	theXMLfile = skinConfigFile
	theXMLfile = RSMakeSafeSlashes theXMLfile 


	-- Initialise the XML file for processing using Xpath
	ExpClass = dotNetClass "System.Xml.XPath.XPathExpression"
	stream = dotNetObject "System.IO.StreamReader" theXMLfile
	xmldoc = dotNetObject "System.Xml.XmlDocument"
	xmldoc.load stream
	xmlroot = xmldoc.documentElement
	
	
	-- OK, find the data now
	searchstring = "//Item"
	nodelist = xmlroot.Selectnodes (searchstring)
	
	
	-- The XML contains the image files for this mapping, plus a list of the bone names it can be used with
	-- Grab this from the XML into an array for mapping later
	local boneImageMapping = #()
	for nlindex = 0 to (nodelist.count-1)  do
	(
		-- Firstly get the image name 
		thenode = nodelist.item[nlindex]
		innerSearchstring = "@imagename"
		innernodelist = thenode.Selectnodes (innerSearchstring)
		innernode = innernodelist.itemof[0]
		local theImageFile = innernode.value	-- print out the values	   
		
		-- Now get the bonelist 
		-- The children of this node are the bones 
		searchstring = ".//bonename"
		subnodelist = thenode.Selectnodes (searchstring)

		
		local imageboneList =#()
		for subNodeIndex = 0 to (subnodelist.count - 1) do 
		(
			theSubnode = subnodelist.item[subNodeIndex]
			xmlentry = theSubnode.innerxml
			append imageboneList xmlentry 
		)	
		
		--Collect the mapping data 
		
		local mappingData = #(theImageFile,  imageboneList)
		
		append boneImageMapping mappingData
	)  
	
	
	-- OK, what bones are in the skin modifier of the mesh?
	local theObjectSkinModifer = meshObject.modifiers[#skin]
	max modify mode -- I need to have the modifier panel open ???
	modPanel.setCurrentObject theObjectSkinModifer
	
	local meshBoneList = #()
	local numModBones = skinOps.GetNumberBones theObjectSkinModifer 
	for bindex = 1 to numModBones do
	(
		theBoneName = skinOps.GetBoneName theObjectSkinModifer bindex 1
		append meshBoneList #(bindex, theBoneName)
	)	
	
	
	-- Now loop through the images and match the bone names to the bone images
	-- Compare the incoming image bones with the mesh ones to find the match
	-- Update the incoming bones with the matched bone and it's index in the skin mod
			
	for imageNameIndex = 1 to boneImageMapping.count do 
	(
		-- Get the list of possible bones from the XML
		subBoneList = boneImageMapping[imageNameIndex][2]
		for imageBoneIndex = 1 to subBoneList.count do 
		(
			-- Find a matching bone from the skin mod list
			searchbonename = subBoneList[imageBoneIndex]
			for collectedBoneIndex = 1 to meshBoneList.count do
			(
				skinmodBoneName = meshBoneList[collectedBoneIndex][2]
				skinmodBoneIndex = meshBoneList[collectedBoneIndex][1]
				
				if searchbonename == skinmodBoneName then
				(
					append boneImageMapping[imageNameIndex] #(skinmodBoneName,skinmodBoneIndex )
				)	
			)	
		)	-- end image possible bone list
	) -- end image 

	
	-- Now there is a new array called boneImageMapping that lists the images and the bones they can be used with.
	-- Next, go through the model and for each vert find the UV, then the colour of the skin weights for each bones 
	-- Grab it all even the null weights, do the filtering later

	-- load all the images into an array for speed 
	local skinImageArray = #()
	local theImageSize = undefined
	for imageEntryIndex = 1 to boneImageMapping.count do 
	(

		theImageFileName = boneImageMapping[imageEntryIndex][1]
		theimageFilePath = TBS_InputDataFolder + skinWeightConfig + "/image_files/" + theImageFileName
		theimageFile = openBitmap theimageFilePath
		
		append skinImageArray #(theimageFile, theImageFileName)
		
		-- Grab the size of the first image
		-- Going to assume that the images are square and that all the images are same size
		-- Will that bite me in the ass later? Yes, I expect it will.
		if imageEntryIndex == 1 then 
		(
			theImageSize = theimageFile.width
		)	
	)	
	
	
	local collectedSkinData = #()
	local theCount = collectedMeshData.count
	local theSkinImagecount =  skinImageArray.count
	
	allstartTime = timeStamp()
	for collectedVert = 1 to theCount do 
	(
		theUVpos = collectedMeshData[collectedVert].vertUVArray[1][3]
		
		-- Store the vert number
		-- Store the UV just because it might be useful for debug 
		local collectedVertDataTemp = #()
		append collectedVertDataTemp collectedVert
		append collectedVertDataTemp theUVpos
		
		
		-- Array to store the pixel colours
		local imageEntryDataTemp = #()
		
		for imageEntryIndex = 1 to theSkinImagecount do 
		(
			theimageFile = skinImageArray[imageEntryIndex][1]
			
			-- Calculate the UV coordiante based on the image size
			-- Bitmaps are upsaide down, so invert the Y
			xyUV = [theUVpos.x * theImageSize, theImageSize - (theUVpos.y * theImageSize)]
			
			thePixelColour = getPixels theimageFile xyUV 1 
		
			append imageEntryDataTemp #(skinImageArray[imageEntryIndex][2],thePixelColour[1])
	
		)
		
		append collectedVertDataTemp imageEntryDataTemp 
		append collectedSkinData collectedVertDataTemp
	)	
	
	allendTime = timeStamp()
	format "Search time for all verts in all images was :%  seconds\n"  ((allendTime-allstartTime )/ 1000)

	
	-- Now that the colours have been collected these need to be converted to weights and normalised
	-- Grab the weight list from the images
	-- Store the image that it came from - this is the Bone
	-- I've already mapped the bone image to 
	-- The colour is the skin weight 
	local hasWeights = #()
	local theCount = collectedSkinData.count
	for vertIndex = 1 to theCount do 
	(
		local vertWeightList = #()
		
		-- Read each entry and find the bones 
		theBoneList = collectedSkinData[vertIndex][3]
		for theBoneEntry = 1 to theBoneList.count do 
		(
			
			theBoneImageName = theBoneList[theBoneEntry][1]
			theBonename = (filterstring theBoneImageName "." )[1]
			matchedbonename = boneImageMapping[theBoneEntry][3][1]
			matchedboneindex = boneImageMapping[theBoneEntry][3][2]
			thePixelColour = theBoneList[theBoneEntry][2]
			theRedvalue = ((thePixelColour as point3)[1]) 
			
			if theRedvalue > 0 then 
			(
				theSkinweight = theRedvalue/255
				append vertWeightList #(theBoneImageName,theBonename, matchedbonename, matchedboneindex,theSkinweight)
			)	
		)	
		
		append hasWeights #(vertIndex, vertWeightList )
	)	
	
	
	-- close the images
	for imageEntryIndex = 1 to theSkinImagecount do 
	(
		theimageFile = skinImageArray[imageEntryIndex][1]
		close theimageFile
		free theimageFile
	)
	
	
	-- Apply the weights
	for theWeightIndex = 1 to hasWeights.count do 
	(
		theWeightEntry = hasWeights[theWeightIndex]
		theImageBoneListArray = theWeightEntry[2]
		
		-- Are there bones to load?
		if (theImageBoneListArray.count > 0) then
		(	
			-- Collect the weight list
			local weightListBone = #()
			local weightListWeight = #()
			
			for boneEntryIndex = 1 to theImageBoneListArray.count do 
			(
				theBoneEntryList = theImageBoneListArray[boneEntryIndex]
				append weightListBone theBoneEntryList[4]
				append weightListWeight theBoneEntryList[5]
			)	
		
			-- Apply the weights 
			-- Turn off the normalisation
		
				skinOps.ReplaceVertexWeights theObjectSkinModifer theWeightIndex weightListBone weightListWeight
		)

		else
		(
			-- No weights to load, leave this vert alone.
			
		)	
			
	)
	
	popPrompt()
	print "Finished Loading Skin Weights."
	messagebox "Finished Loading Skin Weights."
	
)	

-- For every vertice skinned to an eyeball bone, it checks if the vertice is at the right or left side of the face and swap the skinning if necessary.
--This function uses "convertVert" taken from Skinned_Vert_Swapper script.
fn TBS_FixEyeballSkin collectedMeshData =
(
	local L_Eye_Bone=undefined
	local R_Eye_Bone=undefined
	
	-- Find bone IDs for eyeball pivots (this could be faster stopping as soon as the bones were found)
	for i=1 to collectedMeshData.count do
	(
		for j=1 to collectedMeshData[i].vertSkinArray.count do
		(
			if ((findstring collectedMeshData[i].vertSkinArray[j][2] "FB_L_Eye_00")!=undefined) then 
			(
				L_Eye_Bone=collectedMeshData[i].vertSkinArray[j][1]
			)
			if ((findstring collectedMeshData[i].vertSkinArray[j][2] "FB_R_Eye_00")!=undefined) then 
			(
				R_Eye_Bone=collectedMeshData[i].vertSkinArray[j][1]
			)
		)
	)
	theSkin = $.modifiers[#skin]
	
	--replace the skin weights when the vertice is weighted to the wrong bone.
	for i=1 to collectedMeshData.count do
	(
		theVert=collectedMeshData[i].vertMeshIndex
		for j=1 to collectedMeshData[i].vertSkinArray.count do
		(
			--print collectedMeshData[i].vertSkinArray[j]
			if (((findstring collectedMeshData[i].vertSkinArray[j][2] "FB_R_Eye_00")!=undefined)  and ($.verts[(collectedMeshData[i].vertMeshIndex)].position[1]<0) ) then
			(
				skinOps.ReplaceVertexWeights theSkin theVert R_Eye_Bone 1
			)
			if (((findstring collectedMeshData[i].vertSkinArray[j][2] "FB_L_Eye_00")!=undefined)  and ($.verts[(collectedMeshData[i].vertMeshIndex)].position[1]>0) ) then
			(
				skinOps.ReplaceVertexWeights theSkin theVert L_Eye_Bone 1
			)
		)
	)
)

--////////////////////////////////////////////////////
-- Main UI
--////////////////////////////////////////////////////
try(DestroyDialog TextureBasedVertexStorage)catch()
rollout TextureBasedVertexStorage "Texture Storage"
(
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:TextureBasedVertexStorage.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"TextureBasedVertexStorage" --filename:(getThisScriptFilename())
	
	dropdownlist ddlImageOutputSize "Image Output Size" width:100  tooltip:"Image Size for texture output" 
	spinner spnUVChannel "UV" range:[0,100,1] type:#integer width:50  tooltip:"Image Size for texture output" offset: [15,-25]

	button btnCollectVertData "Collect Vert Data" width:180 height:30 tooltip: "Collect Vert Data and map mesh verts to the UVs"
	
	button btnSaveSelectedSkinWeights "Save Selected Skin Weights" width:180 height:30 tooltip: "Save Selected Skin Weights"
	
	
	dropdownlist ddlSkinWeightConfig "Skin Weight Config" width:180  tooltip:"Skin Weight Config" 

	
	button btnLoadSelectedSkinWeights "Load Selected Skin Weights" width:180 height:30 tooltip: "Load Selected Skin Weights"

	button btnFixEyeballSkinning "Fix Skin Weights" width:180 height:30 tooltip: "Clean Up Skinning On The Eyeballs"
	
	local collectedMeshData = undefined
	local theMeshBonelist = undefined

	
	--////////////////////////////////////////////////////
	on TextureBasedVertexStorage open do
	(
		
		banner.setup()
		
		-- Setup the UI fields 
		ddlImageOutputSizeItems = #()
		for imageSizeItem in TBS_UVimageOutputSize do 
		(
			append ddlImageOutputSizeItems (imageSizeItem as string)
		)
		
		TextureBasedVertexStorage.ddlImageOutputSize.items = ddlImageOutputSizeItems
		TextureBasedVertexStorage.ddlImageOutputSize.selection = 2
		
		
		
		-- Get the folders within the TBS_InputDataFolder
		ddlSkinWeightConfigItems = #()
		dir_array = GetDirectories (TBS_InputDataFolder + "*")
		
		for theFolderIndex = 1 to dir_array.count do 
		(
			theFolder = dir_array[theFolderIndex]
			theSubFolder = substring thefolder (TBS_InputDataFolder.count) 255
			theSubfolder = substring  theSubFolder 2 (theSubFolder.count-2)
			
			append ddlSkinWeightConfigItems theSubfolder
			
		)	
		
		TextureBasedVertexStorage.ddlSkinWeightConfig.items = ddlSkinWeightConfigItems
	)
	
	
	on btnCollectVertData pressed do 
	(
		-- Is there a valid object selected?
		theSelection = getcurrentselection()

		if (theSelection.count == 1) then
		(
			theObject = theSelection[1]
			theObjectUVChannel = TextureBasedVertexStorage.spnUVChannel.value
			theMeshBonelist = undefined
			
			-- These will come from the UI later
			collectSkinFlag = true
			collectVCFlag = false
			
			collectedMeshData = TBS_CollectMeshData theObject theObjectUVChannel collectSkinFlag collectVCFlag &theMeshBonelist
			
		) 
		
		else
		(
			messagebox "Nothing selected - please select a single mesh"
		)
		
	)	
		
	
	
	on btnSaveSelectedSkinWeights pressed do 
	(
		-- Is there a valid object selected that has a skin modifier?
		theSelection = getcurrentselection()

		if (theSelection.count ==1) then
		(
			currentMeshSkinned = isMeshSkinned theSelection[1]
			
			-- Process a skinned mesh
			if currentMeshSkinned == true then 
			(
				skinnedObject =theSelection[1]
				skinnedObjectUVChannel = TextureBasedVertexStorage.spnUVChannel.value
				skinTextureSize = TBS_UVimageOutputSize[TextureBasedVertexStorage.ddlImageOutputSize.selection]
				
				saveDataName = skinnedObject.name
				
				skinTextureFolder = TBS_skinTextureFolder

				
				if (theBonelist == undefined) then 
				(
					messagebox ("No data collected for " + theSelection[1].name + " - please collect the vert data")
				)
				
				
				else
				(
					TBS_writeSkinToTexture theBonelist collectedMeshData skinTextureFolder saveDataName skinTextureSize TBS_paddingSizeArray
				)
			)
	
			else
			(
				messagebox (theSelection[theMeshIndex].name + " doesn't appear to be skinned.")
			)

		) 
		
		else
		(
			messagebox "Nothing/Multiple selected - please select a single skinned mesh"
		)
		
	)	
	



	on btnLoadSelectedSkinWeights pressed do 
	(
		-- Is there a valid object selected that has a skin modifier?
		theSelection = getcurrentselection()

		if (theSelection.count == 1) then
		(
	
			currentMeshSkinned = isMeshSkinned theSelection[1]
			
			-- Process a skinned mesh
			if currentMeshSkinned == true then 
			(
				meshObject =theSelection[1]

				if (theMeshBonelist == undefined) then 
				(
					messagebox ("No data collected for " + theSelection[1].name + " - please collect the vert data")
				)
				
				
				else
				(
					-- Need to pass in the current bone list from the object as well as what we want to do to the object 
					-- The collected mesh data maps the vert index to a UV 
					skinWeightConfig = TextureBasedVertexStorage.ddlSkinWeightConfig.selected
					TBS_loadSkinToTexture meshObject collectedMeshData skinWeightConfig
				)

			)
	
			else
			(
				messagebox (theSelection[1].name + " doesn't appear to be skinned - skipping this mesh.")
			)

		
		) 
		
		else
		(
			messagebox "Nothing/multiple selected - please select a single skinned mesh"
		)
		
	)
	
	on btnFixEyeballSkinning pressed do
	(
		-- Is there a valid object selected that has a skin modifier?
		theSelection = getcurrentselection()

		if (theSelection.count == 1) then
		(
	
			currentMeshSkinned = isMeshSkinned theSelection[1]
			
			-- Process a skinned mesh
			if currentMeshSkinned == true then 
			(
				meshObject =theSelection[1]

				if (theMeshBonelist == undefined) then 
				(
					messagebox ("No data collected for " + theSelection[1].name + " - please collect the vert data")
				)
				
				
				else
				(
					-- Need to pass in the current bone list from the object as well as what we want to do to the object 
					-- The collected mesh data maps the vert index to a UV 
					TBS_FixEyeballSkin collectedMeshData
				)

			)
	
			else
			(
				messagebox (theSelection[1].name + " doesn't appear to be skinned - skipping this mesh.")
			)

		
		) 
		
		else
		(
			messagebox "Nothing/multiple selected - please select a single skinned mesh"
		)
		
	)
	
	
)



createDialog TextureBasedVertexStorage width:220 height:270 pos:[200, 200] style:#(#style_titlebar, #style_border, #style_sysmenu, #style_minimizebox)










