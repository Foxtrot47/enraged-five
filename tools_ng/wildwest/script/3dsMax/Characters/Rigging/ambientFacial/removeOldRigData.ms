-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()


--Load common functions	
-- filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
-- filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_bone_tagger.ms")
-- filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_Rigging.ms")
-- -- filein (theProjectRoot + "tools/dcc/current/max2010/scripts/pipeline/util/xml.ms")

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

fn removeOldFacialRigData = 
(
	objToDelete = #()
-- 	objToDeleteNames = #(
-- 			"Ambient_UI",
-- 			"RECT_R_Blink",
-- 			"TEXT_R_Blink",
-- 			"CTRL_R_Blink",
-- 			"RECT_L_Blink",
-- 			"TEXT_L_Blink",
-- 			"CTRL_L_Blink",
-- 			"RECT_L_Cheek",
-- 			"TEXT_L_Cheek",
-- 			"CTRL_L_Cheek",
-- 			"RECT_R_Cheek",
-- 			"TEXT_R_Cheek",
-- 			"CTRL_R_Cheek",
-- 			"RECT_UpperLip_Curl",
-- 			"TEXT_UpperLip_Curl",
-- 			"CTRL_UpperLip_Curl",
-- 			"RECT_LowerLip_Curl",
-- 			"TEXT_LowerLip_Curl",
-- 			"CTRL_LowerLip_Curl",
-- 			"RECT_Tongue_In_Out",
-- 			"TEXT_Tongue_In_Out",
-- 			"CTRL_Tongue_In_Out",
-- 			"RECT_C_Brow",
-- 			"TEXT_C_Brow",
-- 			"CTRL_C_Brow",
-- 			"RECT_R_Brow",
-- 			"TEXT_R_Brow",
-- 			"CTRL_R_Brow",
-- 			"RECT_L_Brow",
-- 			"TEXT_L_Brow",
-- 			"CTRL_L_Brow",
-- 			"RECT_R_Eye",
-- 			"TEXT_R_Eye",
-- 			"CTRL_R_Eye",
-- 			"RECT_L_Eye",
-- 			"TEXT_L_Eye",
-- 			"CTRL_L_Eye",
-- 			"RECT_R_Mouth",
-- 			"TEXT_R_Mouth",
-- 			"CTRL_R_Mouth",
-- 			"RECT_Mouth",
-- 			"TEXT_Mouth",
-- 			"CTRL_Mouth",
-- 			"RECT_L_Mouth",
-- 			"TEXT_L_Mouth",
-- 			"CTRL_L_Mouth",
-- 			"RECT_UpperLip",
-- 			"TEXT_UpperLip",
-- 			"CTRL_UpperLip",
-- 			"RECT_LowerLip",
-- 			"TEXT_LowerLip",
-- 			"CTRL_LowerLip",
-- 			"RECT_Jaw",
-- 			"TEXT_Jaw",
-- 			"CTRL_Jaw",
-- 			"RECT_Tongue",
-- 			"TEXT_Tongue",
-- 			"CTRL_Tongue",
-- 			"RECT_MouthPinch",
-- 			"TEXT_MouthPinch",
-- 			"CTRL_MouthPinch",
-- 			"FaceFX",
-- 			"RECT_IH",
-- 			"TEXT_IH",
-- 			"IH",
-- 			"RECT_Squint",
-- 			"TEXT_Squint",
-- 			"Squint",
-- 			"RECT_Brows_Down",
-- 			"TEXT_Brows_Down",
-- 			"Brows_Down",
-- 			"RECT_Brow_Up_R",
-- 			"TEXT_Brow_Up_R",
-- 			"Brow_Up_R",
-- 			"RECT_Brow_Up_L",
-- 			"TEXT_Brow_Up_L",
-- 			"Brow_Up_L",
-- 			"RECT_tRoof_pose",
-- 			"TEXT_tRoof_pose",
-- 			"tRoof_pose",
-- 			"RECT_tTeeth_pose",
-- 			"TEXT_tTeeth_pose",
-- 			"tTeeth_pose",
-- 			"RECT_tBack_pose",
-- 			"TEXT_tBack_pose",
-- 			"tBack_pose",
-- 			"RECT_wide_pose",
-- 			"TEXT_wide_pose",
-- 			"wide_pose",
-- 			"RECT_FV",
-- 			"TEXT_FV",
-- 			"FV",
-- 			"RECT_PBM",
-- 			"TEXT_PBM",
-- 			"PBM",
-- 			"RECT_ShCh",
-- 			"TEXT_ShCh",
-- 			"ShCh",
-- 			"RECT_W_pose",
-- 			"TEXT_W_pose",
-- 			"W_pose",
-- 			"RECT_open_pose",
-- 			"TEXT_open_pose",
-- 			"open_pose"
-- 		)
	
-- 	objToDeleteNames = #()
	
	rootObs = #(
		"FaceFX",
		"Ambient_UI",
		"FacialAttrGUI",
		"faceControls_OFF",
		"facialRoot_C_OFF"
		)
	
	clearSelection()
		
	for n = 1 to rootObs.count do 
	(
		thisObj = getNodeByName rootObs[n]
		
		appendIfUnique objToDelete thisObj
		
		if thisObj != undefined do 
		(
			tmpArr = getAllChildren thisObj arr:(selection as array)
			
			for i in tmpArr do 
			(
				appendIfUnique objToDelete i
				
-- 				print ("Adding "+(i.name)+" for deletion")
			)
		)
	)
		
		
	for obj in objects do 
	(
		if substring obj.name 1 3 == "FB_" do 
		(
			appendIfUnique objToDelete obj
		)
	)
	
	for i = objToDelete.count to 1 by -1 do 
	(
		print ("deleting "+(objToDelete[i] as string))
		obj = objToDelete[i]
		
		if obj != undefined do 
		(
			delete obj
		)
	)
-- 	delete objToDelete
)

removeOldFacialRigData()