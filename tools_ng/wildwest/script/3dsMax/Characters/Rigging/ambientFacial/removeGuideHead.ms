

fn RSTA_cleanAmbientUiTemplateHead = 
(
	oldTemplateData = #(
	"TEMPLATE_Ambient_UI",
	"TEMPLATE_Dummy01",
	"TEMPLATE_FaceFX",
	"TEMPLATE_RECT_ScaleOffset",
	"TEMPLATE_TEMPLATE_MESH",
	"TEMPLATE_RECT_MouthPinch",
	"TEMPLATE_CTRL_MouthPinch",
	"TEMPLATE_TEXT_MouthPinch",
	"TEMPLATE_RECT_Tongue",
	"TEMPLATE_CTRL_Tongue",
	"TEMPLATE_TEXT_Tongue",
	"TEMPLATE_RECT_Jaw",
	"TEMPLATE_CTRL_Jaw",
	"TEMPLATE_TEXT_Jaw",
	"TEMPLATE_RECT_LowerLip",
	"TEMPLATE_CTRL_LowerLip",
	"TEMPLATE_TEXT_LowerLip",
	"TEMPLATE_RECT_UpperLip",
	"TEMPLATE_CTRL_UpperLip",
	"TEMPLATE_TEXT_UpperLip",
	"TEMPLATE_RECT_L_Mouth",
	"TEMPLATE_CTRL_L_Mouth",
	"TEMPLATE_TEXT_L_Mouth",
	"TEMPLATE_RECT_Mouth",
	"TEMPLATE_CTRL_Mouth",
	"TEMPLATE_TEXT_Mouth",
	"TEMPLATE_RECT_R_Mouth",
	"TEMPLATE_CTRL_R_Mouth",
	"TEMPLATE_TEXT_R_Mouth",
	"TEMPLATE_RECT_L_Eye",
	"TEMPLATE_CTRL_L_Eye",
	"TEMPLATE_TEXT_L_Eye",
	"TEMPLATE_RECT_R_Eye",
	"TEMPLATE_CTRL_R_Eye",
	"TEMPLATE_TEXT_R_Eye",
	"TEMPLATE_RECT_L_Brow",
	"TEMPLATE_CTRL_L_Brow",
	"TEMPLATE_TEXT_L_Brow",
	"TEMPLATE_RECT_R_Brow",
	"TEMPLATE_CTRL_R_Brow",
	"TEMPLATE_TEXT_R_Brow",
	"TEMPLATE_RECT_C_Brow",
	"TEMPLATE_CTRL_C_Brow",
	"TEMPLATE_TEXT_C_Brow",
	"TEMPLATE_RECT_Tongue_In_Out",
	"TEMPLATE_CTRL_Tongue_In_Out",
	"TEMPLATE_TEXT_Tongue_In_Out",
	"TEMPLATE_RECT_LowerLip_Curl",
	"TEMPLATE_CTRL_LowerLip_Curl",
	"TEMPLATE_TEXT_LowerLip_Curl",
	"TEMPLATE_RECT_UpperLip_Curl",
	"TEMPLATE_CTRL_UpperLip_Curl",
	"TEMPLATE_TEXT_UpperLip_Curl",
	"TEMPLATE_RECT_R_Cheek",
	"TEMPLATE_CTRL_R_Cheek",
	"TEMPLATE_TEXT_R_Cheek",
	"TEMPLATE_RECT_L_Cheek",
	"TEMPLATE_CTRL_L_Cheek",
	"TEMPLATE_TEXT_L_Cheek",
	"TEMPLATE_RECT_L_Blink",
	"TEMPLATE_CTRL_L_Blink",
	"TEMPLATE_TEXT_L_Blink",
	"TEMPLATE_RECT_R_Blink",
	"TEMPLATE_CTRL_R_Blink",
	"TEMPLATE_TEXT_R_Blink",
	"TEMPLATE_mover",
	"TEMPLATE_SKEL_ROOT",
	"TEMPLATE_SKEL_Spine_Root",
	"TEMPLATE_SKEL_Spine0",
	"TEMPLATE_SKEL_Spine1",
	"TEMPLATE_SKEL_Spine2",
	"TEMPLATE_SKEL_Spine3",
	"TEMPLATE_SKEL_Neck_1",
	"TEMPLATE_RB_Neck_1",
	"TEMPLATE_SKEL_Head",
	"TEMPLATE_SKEL_Head_NUB",
	"TEMPLATE_FACIAL_facialRoot",
	"TEMPLATE_FaceRoot_Head_000_R",
	"TEMPLATE_FB_Jaw_000",
	"TEMPLATE_FB_LowerLipRoot_000",
	"TEMPLATE_FB_Tongue_000",
	"TEMPLATE_FB_LowerLip_000",
	"TEMPLATE_FB_R_Lip_Bot_000",
	"TEMPLATE_FB_L_Lip_Bot_000",
	"TEMPLATE_FB_UpperLipRoot_000",
	"TEMPLATE_FB_UpperLip_000",
	"TEMPLATE_FB_R_Lip_Top_000",
	"TEMPLATE_FB_L_Lip_Top_000",
	"TEMPLATE_FB_Brow_Centre_000",
	"TEMPLATE_FB_R_Lip_Corner_000",
	"TEMPLATE_FB_R_Brow_Out_000",
	"TEMPLATE_FB_R_CheekBone_000",
	"TEMPLATE_FB_R_Eye_000",
	"TEMPLATE_FB_R_Lid_Upper_000",
	"TEMPLATE_FB_L_Lip_Corner_000",
	"TEMPLATE_FB_L_CheekBone_000",
	"TEMPLATE_FB_L_Eye_000",
	"TEMPLATE_FB_L_Lid_Upper_000",
	"TEMPLATE_FB_L_Brow_Out_000",
	"TEMPLATE_MH_Hair_Scale",
	"TEMPLATE_IK_Head",
	"TEMPLATE_IK_Root",
	"TEMPLATE_RECT_IH",
	"TEMPLATE_TEXT_IH",
	"TEMPLATE_IH",
	"TEMPLATE_RECT_Squint",
	"TEMPLATE_TEXT_Squint",
	"TEMPLATE_Squint",
	"TEMPLATE_RECT_Brows_Down",
	"TEMPLATE_TEXT_Brows_Down",
	"TEMPLATE_Brows_Down",
	"TEMPLATE_RECT_Brow_Up_R",
	"TEMPLATE_TEXT_Brow_Up_R",
	"TEMPLATE_Brow_Up_R",
	"TEMPLATE_RECT_Brow_Up_L",
	"TEMPLATE_TEXT_Brow_Up_L",
	"TEMPLATE_Brow_Up_L",
	"TEMPLATE_RECT_tRoof_pose",
	"TEMPLATE_TEXT_tRoof_pose",
	"TEMPLATE_tRoof_pose",
	"TEMPLATE_RECT_tTeeth_pose",
	"TEMPLATE_TEXT_tTeeth_pose",
	"TEMPLATE_tTeeth_pose",
	"TEMPLATE_RECT_tBack_pose",
	"TEMPLATE_TEXT_tBack_pose",
	"TEMPLATE_tBack_pose",
	"TEMPLATE_RECT_wide_pose",
	"TEMPLATE_TEXT_wide_pose",
	"TEMPLATE_wide_pose",
	"TEMPLATE_RECT_FV",
	"TEMPLATE_TEXT_FV",
	"TEMPLATE_FV",
	"TEMPLATE_RECT_PBM",
	"TEMPLATE_TEXT_PBM",
	"TEMPLATE_PBM",
	"TEMPLATE_RECT_ShCh",
	"TEMPLATE_TEXT_ShCh",
	"TEMPLATE_ShCh",
	"TEMPLATE_RECT_W_pose",
	"TEMPLATE_TEXT_W_pose",
	"TEMPLATE_W_pose",
	"TEMPLATE_RECT_open_pose",
	"TEMPLATE_TEXT_open_pose",
	"TEMPLATE_open_pose",
	"TEMPLATE_TEXT_ScaleOffset",
	"TEMPLATE_ScaleOffset",
	"TEMPLATE_TEMPLATE_HEAD"
	)	
	
	
	uiNames = #(
	"Ambient_UI",
	"RECT_R_Blink",
	"TEXT_R_Blink",
	"RECT_L_Blink",
	"TEXT_L_Blink",
	"RECT_L_Cheek",
	"TEXT_L_Cheek",
	"RECT_R_Cheek",
	"TEXT_R_Cheek",
	"RECT_UpperLip_Curl",
	"TEXT_UpperLip_Curl",
	"RECT_LowerLip_Curl",
	"TEXT_LowerLip_Curl",
	"RECT_Tongue_In_Out",
	"TEXT_Tongue_In_Out",
	"RECT_C_Brow",
	"TEXT_C_Brow",
	"RECT_R_Brow",
	"TEXT_R_Brow",
	"RECT_L_Brow",
	"TEXT_L_Brow",
	"RECT_R_Eye",
	"TEXT_R_Eye",
	"RECT_L_Eye",
	"TEXT_L_Eye",
	"RECT_R_Mouth",
	"TEXT_R_Mouth",
	"RECT_Mouth",
	"TEXT_Mouth",
	"RECT_L_Mouth",
	"TEXT_L_Mouth",
	"RECT_UpperLip",
	"TEXT_UpperLip",
	"RECT_LowerLip",
	"TEXT_LowerLip",
	"RECT_Jaw",
	"TEXT_Jaw",
	"RECT_Tongue",
	"TEXT_Tongue",
	"RECT_MouthPinch",
	"TEXT_MouthPinch"
		)

	joystickNames = #(
	"CTRL_R_Blink",
	"CTRL_L_Blink",
	"CTRL_L_Cheek",
	"CTRL_R_Cheek",
	"CTRL_UpperLip_Curl",
	"CTRL_LowerLip_Curl",
	"CTRL_Tongue_In_Out",
	"CTRL_C_Brow",
	"CTRL_R_Brow",
	"CTRL_L_Brow",
	"CTRL_R_Eye",
	"CTRL_L_Eye",
	"CTRL_R_Mouth",
	"CTRL_Mouth",
	"CTRL_L_Mouth",
	"CTRL_UpperLip",
	"CTRL_LowerLip",
	"CTRL_Jaw",
	"CTRL_Tongue",
	"CTRL_MouthPinch"
		)	
	
	tmpPoint = getNodeByName "tmpPoint"
	if tmpPoint == undefined do 
	(
		tmpPoint = point pos:[0,0,0] name:"tmpPoint"
	)	
	
	for uiItem in uiNames do 
	(
		obj = getNodeByName uiItem
		
		if obj != undefined do 
		(
			if (classof obj.position.controller as string) == "Position_Constraint" do 
			(
	-- 			obPos = in coordsys parent obj.position

				in coordsys world tmpPoint.transform = in coordsys world obj.transform
				obj.position.controller = Position_XYZ()
				in coordsys world obj.position = in coordsys world tmpPoint.position
				format ("Reset position controller on "+obj.name+"\n")
			)
		)
	)
	
	delete tmpPoint
	
	for objName in oldTemplateData do 
	(
		obj = getNodeByName objName
		if obj != undefined do 
		(
			format ("Deleted "+obj.name+"\n")
			delete obj
		)
	)	
	
	remainingTempObjs = #()
	
	for obj in objects do
	(
		if toLower(substring obj.name 1 9) == "template_" do
		(
			append remainingTempObjs obj
		)
	)
	
	delete remainingTempObjs
)

RSTA_cleanAmbientUiTemplateHead()