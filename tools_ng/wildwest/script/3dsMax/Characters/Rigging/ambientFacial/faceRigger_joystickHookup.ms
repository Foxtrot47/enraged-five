-- joystickHookup.ms
-- Rockstar North
--Part of the facial rigging tools
--script which creates the expressions connecting joysticks to their bones
--Matt Rennie Sept 2010
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

fn addBoneTandRTags = 
(
	for tfb = 1 to faceBoneList[2].count do
	(		
		--now check to see if the remaining controllers have anything in them and if not get rid of it
		maximumControllers = faceBoneList[2][tfb].pos.controller.count 
		totalControllers = (facialJoysticks.count +2)

		hasT = 0
		
		for d in totalControllers to 3 by -1 do 
		(
			
			bV = "Controller:Float_Expression" 
			
			if faceBoneList[2][tfb].pos.controller[d].X_position.controller != undefined do
			(
				nX = (faceBoneList[2][tfb].pos.controller[d].X_position.controller as string)
			)
			if faceBoneList[2][tfb].pos.controller[d].Y_position.controller != undefined do
			(
				nY = (faceBoneList[2][tfb].pos.controller[d].Y_position.controller as string)
			)
			if faceBoneList[2][tfb].pos.controller[d].Z_position.controller != undefined do
			(
				nZ = (faceBoneList[2][tfb].pos.controller[d].Z_position.controller as string)
			)
			
			if nX == bv do
			(
				hasTonX = 1
			)
			
			if nY == bV do
			(
				hasTonY = 1
			)
					
			if nZ == bV do
			(
				hasTonZ = 1
			)

			hasT = (hasTonX +hasTonY+ hasTonZ )
		)
		
		if hasT >= 0.1 then
		(
			--ok it has Trans
			--now need to add a udp tag to faceRoot
			existingUDP = getUserPropBuffer tfb
			setUserPropbuffer tfb ((existingUDP)+"\r\n"+"translateTrackType=TRACK_FACIAL_TRANSLATION"+"\r\n"+"translateTrackType=TRACK_FACIAL_ROTATION")
			debugPrint ("Adding T and R tag to "+tfb.name)
		)
		else
		(
			--OK IT HAS NO TRANS
			--now need to add a udp tag to faceRoot
			existingUDP = getUserPropBuffer tfb
			setUserPropbuffer tfb ((existingUDP)+"\r\n"+"translateTrackType=TRACK_FACIAL_ROTATION")
			debugPrint ("Adding only R tag to "+tfb.name)
		)
	)
)



fn populateSkinMod geo = 
(
	select geo
	setCommandPanelTaskMode #modify
	
	if ((progBar != undefined) and (progBar.isDisplayed)) do
	(destroyDialog progBar)
	CreateDialog progBar width:300 Height:30
	progBar.prog.color = [243,157,7] --purply pink
	
	for i = 1 to faceBoneList[2].count do 
	(
		boneUpdateInt = undefined
		if i != faceBoneList[2].count then
		(
			boneUpdateInt = 0
		)
		else
		(
			--ok we're at the count so we need to refresh
			boneUpdateInt = 1
		)
		
		debugPrint ("trying to add " +faceBoneList[2][i].name+" to skin on "+geo.name)

		modPanel.setCurrentObject geo.modifiers[#Skin] 
		
			skinOps.addBone geo.modifiers[#Skin] faceBoneList[2][i] boneUpdateInt
	
		progBar.prog.value = (100.*i/faceBoneList[2].count)
	)
		
	debugprint "****************************************************************************"	
	debugprint("\r\n"+"\t"+"\t"+"\t"+"create face completed.")
	debugPrint "****************************************************************************"
		if ((progBar != undefined) and (progBar.isDisplayed)) do
		(destroyDialog progBar)
)

fn addBonesToGeo geo = 
(	
	select geo
	if geo.modifiers[#Skin] != undefined then
	(
		geo.modifiers[#Skin].bone_Limit = 4
		geo.modifiers[#Skin].showNoEnvelopes = on
		populateSkinMod geo
	)
	else
	(
		-- add a skin modifier
		select geo
		modPanel.addModToSelection (Skin ()) ui:on
			geo.modifiers[#Skin].bone_Limit = 4
			geo.modifiers[#Skin].showNoEnvelopes = on
		clearSelection()
			populateSkinMod geo
			
		if headRoot == undefined do 
		(
			headRoot = getNodebyName ("FaceRoot_"+geo.name)
		)			
		
		modPanel.setCurrentObject geo.modifiers[#Skin] 
	
		skinOps.addBone geo.modifiers[#Skin] headRoot 1
	)
)

fn createFacialLayers=
(
	debugprint "========================================================================================"
	debugprint "                                                    LAYERIFYING                                                                                         "
	debugprint "========================================================================================"
	
	
	for i = 1 to lyrNames.count do
	(
		if layermanager.getLayerFromName lyrNames[i] == undefined then
		(
			debugPrint (lyrNames[i]+" layer not found. Attempting to create...")
			newLayer = Layermanager.newLayerFromname lyrNames[i]
		)
		else
		(
			debugPrint (lyrNames[i] +"layer already present")
		)
	)
	
	debugPrint "trying to add to the layers"
	layer = layermanager.getLayerFromName "FacialUI_Visuals" 
	layer.addNode $Ambient_UI
	
	progBar.prog.color = green
	
 	shpArray = (shapes as array) --this is used for progBar
	
	for obj in shapes do
	(
		obj.showFrozenInGray = off		

		if threeLatJSFrmaes != undefined do 
		(
			layer = layermanager.getLayerFromName "FacialUI_Visuals" 
			
			for o in threeLatJSFrmaes do 
			(
				obj = getNodeByName o
				layer.addNode obj
				obj.showFrozenInGray = false
			)				
		)
		
		if ( substring (obj.name) 1 4) == "RECT" then
		(
			layer = layermanager.getLayerFromName "FacialUI_Visuals" 
			layer.addNode obj
			obj.showFrozenInGray = false
		)
		else
		(
			if ( substring (obj.name) 1 4) == "TEXT"  then
			(
				layer = layermanager.getLayerFromName "FacialUI_Visuals" 
				layer.addNode obj				
				obj.showFrozenInGray = false
			)
			else
			(
				if ( substring (obj.name) 1 4) == "CTRL"  then
				(
					layer = layermanager.getLayerFromName "FacialUI_Controls" 
					--layer.addNode $Ambient_UI
					layer.addNode obj		
					obj.showFrozenInGray = false					
				)
				else
				(
				--debugPrint " "
				)
			)
		)
		iVal = findItem shpArray obj
	)
	
		fbLayerName = ("Facial_Bones_"+(substring (geo.name) 6 3))
		if layermanager.getLayerFromName fbLayerName == undefined do
		(
			debugPrint (fbLayerName +" layer not found. Attempting to create...")
			newLayer = Layermanager.newLayerFromname fbLayerName 
		)
		
		for rootObj in objects do
		(
			layer = layermanager.getLayerFromName fbLayerName 
			
			if (substring rootObj.name 1 8) == "FaceRoot" do
				(
					layer.addNode rootObj
					debugPrint (rootObj.name+" added to "+fbLayerName+" layer.")
				)
		)
		
	
		for obj =1 to faceBoneList[2].count do
		(	
			layer = layermanager.getLayerFromName fbLayerName 			
			layer.addNode faceBoneList[2][obj]
			debugPrint ("Adding "+faceBoneList[2][obj].name+" to "+fbLayerName +" layer")
			faceBoneList[2][obj].showFrozenInGray = false
			
			layer.isFrozen = true
			layer.showFrozenInGray = false
			
		)
		
		layer = layermanager.getLayerFromName lyrNames[2]
		layer.isFrozen = true
		layer.showFrozenInGray = false
		
		layer = layermanager.getLayerFromName lyrNames[1]
		layer.isFrozen = false
		layer.showFrozenInGray = false
		
		debugprint "========================================================================================"
		debugprint "                                    LAYERING DONE                                                  "
		debugprint "========================================================================================"		
	clearSelection()
)

fn concatExpression expObj expCont scalarObjects scalarNames scalarConts expString=
(
	format ("concatExpression on "+expObj+"."+expCont+"\n")
	
	
	--freeze transforms on the object to gain expressions
	
	expNode = getNodeByName expObj
	
	if expNode == undefined do
	(
		--ok try and strip off ' characters
		if substring expObj 1 1 == "'" do
		(
			nameLength = expObj.count
			expTempName = substring expObj 2 (nameLength - 2)
			expNode = getNodeByName expTempName
		)
	)
	
	if expNode == undefined then
	(
		messagebox ("WARNING! Couldn't find "+expObj)
	)
	else
	(
	
		select expNode
		--should i add a float expression node at this point?

		
		expObj = expNode.name
		
		/*
		need to pass in:
			1) expObj =  name of object the expression is for
			2) expCont = controller the expression is on (as a string)
			3) scalarObjects = name of objects used as drivers as an array of string 
			4) scalarNames = name of scalars - do this as an array (of strings) so we cna loop through multiple scalars
			5) scalarConts = controller the scalars point to - do this as array (of strings) with an entry for each item in the scalarNames array
			6) expString = full expression string
		*/	
			
		
-- 		expString = ("("+expString+")")
		
			
		fcStr = ( "$"+expObj+expCont+".controller") --rebuild the string so it can be used below	
	
			local newfcStr = execute fcStr
		debugPrint ("newfcStr = "+(fcStr as string))
		
		--now we'll add the scale offset shit
		scaleJs = getNodeByName "ScaleOffset"			
		scalJsStr = ("$"+scaleJs.name+".position.controller.Zero_Pos_XYZ."+"Y"+"_Position.controller" )
		scalJsStr = execute scalJsStr 
		SVN = "ScaleOffset"
		newFcStr.AddScalarTarget SVN scalJsStr			
					
			
		if scalarNames.count != 0 do
		(
			for i = 1 to scalarNames.count do --this should loop through the array of scalars and their controllers and create a scalar for each
			(
				debugprint ("Prepping expr for "+scalarNames[i]+" (index:"+(i as string)+")")
				SVN = scalarNames[i]
				debugPrint ("SVN = "+(SVN as string))
				
				scalContStr = (scalarConts[i] as string)
					debugprint "\r\n"
					debugPrint ("scalContStr  = "+(scalarConts[i] as string))
					scalCont = execute (scalContStr as string)
					
				newfcStr.AddScalarTarget SVN scalCont --add scalar pointing to the controller of the scalar object
					
				debugprint ("Expr prep for "+scalarNames[i]+" (index:"+(i as string)+") complete")
				debugprint ("-------------------------------------------------------------------------------------"+"\r\n")
			)
		)	
		debugPrint ("setting expression to :\r\n"+expString)

			newfcStr.SetExpression expString
				
				debugPrint ("Expression created on "+expObj+" for "+expCont)
				debugPrint "```````````````````````````````````````````````````````"
	)
)

fn removeOldScaleControls boneToConcat = 
(
	print ("removeOldScaleControls on "+boneToConcat.name)
	scaleContCount = boneToConcat.scale.controller.count
	
	for i = (scaleContCount - 1) to 3 by -1 do 
	(
		boneToConcat.scale.controller.delete i
	)
	
	--now delete the zero scale one so we get correctly normalised scales in game
	boneToConcat.scale.controller.delete 2
	
)

fn concatAmbientScale boneToConcat exprDataX exprDataY exprDataZ = 
(
	print ("concatAmbientScale scale on "+boneToConcat.name)
	
	if exprDataX == undefined do 
	(
		print "exprDataX == undefined"
		break()
	)
		
	if exprDataY == undefined do 
	(
		print "exprDataY == undefined"
		break()
	) 
		
	if exprDataZ == undefined do 
	(
		print "exprDataZ == undefined"
		break()
	)
	
	
	--now we'll get the count of controllers on this axis
	scaleContCount = boneToConcat.scale.controller.count
	
	boneToConcat.scale.controller.Available.controller = ScaleXYZ()
	
	boneToConcat.scale.controller.setName (scaleContCount + 1) "ConcatScale"	
	
	toArr = undefined
	for axis = 1 to 3 do 
	(
		toArr = undefined
		a = undefined
		if axis == 1 then 
		(
			toArr = exprDataX
			a = "X"
			--add a float expression controller
			boneToConcat.scale.controller[(scaleContCount + 1)][axis].controller = Float_Expression()
		)
		else
		(
			if axis == 2 then
			(
				toArr = exprDataY
				a = "Y"
				boneToConcat.scale.controller[(scaleContCount + 1)][axis].controller = Float_Expression()
			)
			else
			(
				toArr = exprDataZ
				a = "Z"
				boneToConcat.scale.controller[(scaleContCount + 1)][axis].controller = Float_Expression()
			)
		)
		
		print ("Concatting "+a+" axis...")
		
		--now we need to parse the data inside the toArr
		
		
		scalarNames = #()
		scalarConts = #()
		scalarObjects = #() --do i really need this?
		
	
	if toArr == undefined then
	(
		messagebox ("WARNING toArr == undefined") beep:true
		print ("WARNING toArr == undefined") 
		break()
	)
	else
	(
		if toArr[1] == undefined then 
		(
			messagebox ("WARNING! toArr[1] == undefined") beep:true
			print ("WARNING! toArr[1] == undefined") 
			break()
		)
		else
		(
			
			for i = 1 to toArr[1].count do 
			(
				scalarN = toArr[1][i]
				scalarT = toArr[2][i]
				
				for sn = 1 to scalarN.count do 
				(
					scalarName = scalarN[sn]
					append scalarNames scalarName
					append scalarObjects scalarName
				)
				
				for sn = 1 to scalarT.count do 
				(
					scalarTarget = scalarT[sn] 
					
					--print ("scalarTarget: "+(scalarTarget as string))
					append scalarConts scalarTarget
				)
				
				
			)
		)
	)
		
		--now we need to concat the expr strings together

-- 		expString = "(ScaleOffset + 0) "
		expString = "(if (ScaleOffset >= 0, ScaleOffset + 1, (if (ScaleOffset < 0,1,1)))) "
-- 		expString = "(ScaleOffset + 1) "
		
-- 		expString = "(if (ScaleOffset < -0.001, ScaleOffset + 1, ScaleOffset +1)) "
		
		for i = 1 to toArr[3].count do 
		(
			addStr = " "
			if expString != "" then (addStr = " + ")
			
			savedStr = toArr[3][i]
			
			expString = (expString+addStr+savedStr)
		)
		
		--now we need to hack the expression string to remove carriage returns and also double brackets as the exporter will crash max otherwise
		
		expString = substituteString expString "\n" ""

		expString = substituteString expString "((" "( 0 +("		
		

		
				/*
		need to pass in:
			1) expObj =  name of object the expression is for
			2) expCont = controller the expression is on (as a string)
			3) scalarObjects = name of objects used as drivers as an array of string 
			4) scalarNames = name of scalars - do this as an array (of strings) so we cna loop through multiple scalars
			5) scalarConts = controller the scalars point to - do this as array (of strings) with an entry for each item in the scalarNames array
			6) expString = full expression string
		*/	
	
		expObj = boneToConcat.name
		expCont = ".scale.controller["+((scaleContCount + 1) as string)+"]["+(axis as string)+"]"
		
		-- 		format ("expObj: "+( expObj as string)+"\n")
		-- 		format ("expCont: "+( expCont as string)+"\n")
		-- 		format ("scalarObjects: "+( scalarObjects as string)+"\n")
		-- 		format ("scalarNames: "+( scalarNames as string)+"\n")
		-- 		format ("scalarConts: "+( scalarConts as string)+"\n")
		 		format ("expString: "+( expString as string)+"\n")
		

			concatExpression expObj expCont scalarObjects scalarNames scalarConts expString
	)
	
	removeOldScaleControls boneToConcat

)	

fn preConcatAmbientScales boneToConcat = 
(
	print ("preConcatAmbientScales "+"on "+(boneToConcat.name) )
	--break()

	if boneToConcat.scale.controller.count > 2 do 
	(
		cntName = 	boneToConcat.scale.controller.getName 2
		
		if cntName == "Zero Scale XYZ" do 
		(
			boneToConcat.scale.controller.delete 2
		)
	)
	
	if boneToConcat.scale.controller.count > 3 do 
	(
	-- 	print ("PRECONCATTING "+boneToConcat.name)
		
		--re-decalre the variables here so we dont carry any old data
		exprDataX = #(
		#(), --scalarNames
		#(), --scalarTargets
		#() --expressionStrings
		)
		
		exprDataY = #(
		#(), --scalarNames
		#(), --scalarTargets
		#() --expressionStrings
		)

		exprDataZ = #(
		#(), --scalarNames
		#(), --scalarTargets
		#() --expressionStrings
		)
		
		for masterCnt = 1 to boneToConcat.scale.controller.count do 
		(
			for subCnt = 1 to 3 do --for the xyz
			(
				if boneToConcat.scale.controller[masterCnt][subCnt] != undefined do
				(	
					cntName = 	boneToConcat.scale.controller.getName masterCnt

					if (boneToConcat.scale.controller[masterCnt][subCnt].controller as string) == "Controller:Float_List" then
					(
						--check if its a paired controller
						
						for sub = 1 to boneToConcat.scale.controller[masterCnt][subCnt].controller.count do 
						(
							if (boneToConcat.scale.controller[masterCnt][subCnt][sub].controller as string) == "Controller:Float_Expression" do 
							(
	-- 							print ("Controller "+(cntName))
	-- 							print ("AXIS: "+(subCnt as string))								
								addToArr = false
								
								controller = boneToConcat.scale.controller[masterCnt][subCnt][sub].controller 
								
								toArr = undefined
								if subCnt == 1 then 
								(
									toArr = exprDataX
								)
								else
								(
									if subCnt == 2 then
									(
										toArr = exprDataY
									)
									else
									(
										toArr = exprDataZ
									)
								)
								
								--ok we need the scalar name
								
								--scalar target
															
									
								theScalarCount = controller.NumScalars()
								
								if theScalarCount != 4 do --need to remember there are always 4 for ticks, frames, secs and normalised-time
								(
									sclNames = #()
									sclTgts = #()

									for i = 5 to theScalarCount do
									(
										--get scalar names
										thisScalarNameStr = controller.GetScalarName i
										
										--now we need to ignore scale offsets
										if thisScalarNameStr != "scaleOffset" do 
										(
											append sclNames thisScalarNameStr
											
											--get scalar target
											
											thisScalarTgt = controller.GetScalarTarget thisScalarNameStr asController:true
												
											thisScalarTgtCont = exprformaxobject thisScalarTgt
												
											append sclTgts thisScalarTgtCont
											
											addToArr = true
											
										)
									)

									
									if addToArr == true do 
									(
										append toArr[1] sclNames
										append toArr[2] sclTgts
									)
								)
								
								if addToArr == true do 
								(
									--expression string
									exprStr = controller.getExpression()
									--print ("InitExpr: "+exprStr)
									--57 is the count of characters from the initial "(if (scaleOffset < 0,scaleOffset + 2,scaleOffset + 1)) + "
									
									strToStrip = "(if (scaleOffset < 0,scaleOffset + 2,scaleOffset + 1)) + "
									
									pat = matchPattern exprStr pattern:(strToStrip+"*")
									
									if pat == true do 
									(
										exprStr = (substring exprStr 57 -1)
									)
									
									--print ("FinalStr: "+exprStr)
									append toArr[3] exprStr							
								)							
							)
						)
					)
					else
					(
						if (boneToConcat.scale.controller[masterCnt][subCnt].controller as string) == "Controller:Float_Expression" do 
						(
							addToArr = false						
							
							controller = boneToConcat.scale.controller[masterCnt][subCnt].controller 
							
							toArr = undefined
							if subCnt == 1 then 
							(
								toArr = exprDataX
							)
							else
							(
								if subCnt == 2 then
								(
									toArr = exprDataY
								)
								else
								(
									toArr = exprDataZ
								)
							)
							
							--ok we need the scalar name
							
							--scalar target

								
								
							theScalarCount = controller.NumScalars()
							
							if theScalarCount != 4 do --need to remember there are always 4 for ticks, frames, secs and normalised-time
							(
								sclNames = #()
								sclTgts = #()
								for i = 5 to theScalarCount do
								(
									--get scalar names
									thisScalarNameStr = controller.GetScalarName i
									
									--now we need to ignore scale offsets
									if thisScalarNameStr != "scaleOffset" do 
									(
										append sclNames thisScalarNameStr
										
										--get scalar target
										
										thisScalarTgt = controller.GetScalarTarget thisScalarNameStr asController:true
											
										thisScalarTgtCont = exprformaxobject thisScalarTgt
											
										append sclTgts thisScalarTgtCont
										
										addToArr = true
										
									)
								)

								if addToArr == true do 
								(
									append toArr[1] sclNames
									append toArr[2] sclTgts
								)
							)
							
							if addToArr == true do 
							(
								--expression string
								exprStr = controller.getExpression()
								--print ("InitExpr: "+exprStr)
								--57 is the count of characters from the initial "(if (scaleOffset < 0,scaleOffset + 2,scaleOffset + 1)) + "
								
								strToStrip = "(if (scaleOffset < 0,scaleOffset + 2,scaleOffset + 1)) + "
								
								pat = matchPattern exprStr pattern:(strToStrip+"*")
								
								if pat == true do 
								(
									exprStr = (substring exprStr 57 -1)
								)
								
								--print ("FinalStr: "+exprStr)
								append toArr[3] exprStr							
							)
						)
					)
				)
			)
		)
		
		--now we've prepped the data run the actual concat
		concatAmbientScale boneToConcat exprDataX exprDataY exprDataZ
	)
)
	

fn removeZeroScales = --fn to remove the bezier and zero scale controllers as these add extra '1's to the output of the expressions
(
	print ("Initialising removeZeroScales...")
	
	for fb = 1 to faceBoneList[2].count do 
	(
		maximumControllers = faceBoneList[2][fb].scale.controller.count --get total number of controllers on the bone
		
		contName = undefined
		
-- 		for cnt = maximumControllers to 1 by -1 do --ignore the bezier and zero controller
		for cnt = maximumControllers to 3 by -1 do --ignore the bezier and zero controller
		(
			thisCont = faceBoneList[2][fb].scale.controller[cnt]
						
			keepThis = false
			
			if (classof thisCont.controller as string) == "bezier_scale" then
			(
				keepThis = false
			)
			else
			(
				if (classof thisCont.controller as string) == "ScaleXYZ" then
				(
					--print "Found ScaleXYZ"
					for ind = 1 to 3 do  --x y z 
					(
						if (thisCont[ind].controller as string) == "Controller:Float_Expression" do 
						(
							--print ((ind as string)+" == a float Expression")
							keepThis = true
						)
					)
				)
			)
			
			if keepThis != true do --ok no float expressions found so delete this controller
			(
				contName = faceBoneList[2][fb].scale.controller.getName cnt
-- 				print ("Deleting "+(contName as string))
				faceBoneList[2][fb].scale.controller.delete cnt
			)
		)
	)
	
	--now concat the scales
		if ((progBar != undefined) and (progBar.isDisplayed)) do
		(destroyDialog progBar)
		CreateDialog progBar width:300 Height:30
		progBar.prog.color = yellow 
	
	for obj in faceBoneList[2] do 
	(
		preConcatAmbientScales obj 
		
		iVal = finditem faceBoneList[2] obj
		progBar.prog.value = (100.*iVal/faceBoneList[2].count)
	)
	
	destroyDialog progBar
)

fn scaleCleanup = --look for any scale controllers which have no float expressions set and set them to a dummy scale setup to normalise the output
(
	print ("Initialising scaleCleanup")
	
	scaleJs = getNodeByName "ScaleOffset"
	
	if scaleJs != undefined do 
	(
		for fb = 1 to faceBoneList[2].count do 
		(
			maximumControllers = faceBoneList[2][fb].scale.controller.count --get total number of controllers on the bone
			
			for cnt = 3 to maximumControllers do --ignore the bezier and zero controller
			(
				foundExprCont = false
				
				thisCont = faceBoneList[2][fb].scale.controller[cnt]
				
				for ind = 1 to 3 do  --x y z 
				(
-- 						if (thisCont[ind].controller as string) == "Controller:Float_List" then
-- 						(
-- 							cont = thisCont[ind].controller
-- 							
-- 							print ("cont: "+(cont as string))
-- 							
-- 							cCount = cont.count
-- 	-- 						for fl = 1 to cCount do 
-- 	-- 						(
-- 								for c = 1 to 3 do 
-- 								(
-- 									print ("cont["+(c as string)+"].controller: "+(cont[c].controller as string))
-- 									if (cont[c].controller as string) != "Controller:Float_Expression" do 
-- 									(
-- 										cont[c].controller = Float_Expression()
-- 										
-- 										fcStr = cont[c].controller
-- 							
-- 										scalJsStr = ("$"+scaleJs.name+".position.controller.Zero_Pos_XYZ."+"Y"+"_Position.controller" )
-- 										scalJsStr = execute scalJsStr 
-- 										SVN = "scaleOffset"
-- 										fcStr.AddScalarTarget SVN scalJsStr
-- 								
-- 										--now we'll set the actual expression
-- 										
-- 				-- 						expString = ("(scaleOffset -1) + 1")
-- 				-- 						expString = ("(scaleOffset * -1)")
-- 										
-- 										expString = "if (scaleOffset < 0,scaleOffset + 2,scaleOffset + 1)"

-- 										fcStr.SetExpression expString
-- 	-- 									break()
-- 									)
-- 								)
-- 	-- 						)
-- 						)
					if (thisCont[ind].controller as string) == "Controller:Float_Expression" or (thisCont[ind].controller as string) == "Controller:Float_List" do ( foundExprCont = true)					
				)
				
				if foundExprCont == true do 
				(
					for ind = 1 to 3 do 
					(
						if (thisCont[ind].controller as string) != "Controller:Float_Expression" do
						(
							if (thisCont[ind].controller as string) != "Controller:Float_List" do
							(
-- 								print ("ScaleOfssettifying "+(thisCont[ind].controller as string))
								
								thisCont[ind].controller = Float_Expression()
								
								--now we'll add the scale offset shit
								
								fcStr = thisCont[ind].controller
								
								scalJsStr = ("$"+scaleJs.name+".position.controller.Zero_Pos_XYZ."+"Y"+"_Position.controller" )
								scalJsStr = execute scalJsStr 
								SVN = "scaleOffset"
								fcStr.AddScalarTarget SVN scalJsStr
						
								--now we'll set the actual expression
								
		-- 						expString = ("(scaleOffset -1) + 1")
		-- 						expString = ("(scaleOffset * -1)")
								
								expString = "if (scaleOffset < 0,scaleOffset + 2,scaleOffset + 1)"

								fcStr.SetExpression expString							
							)
						)
					)
				)

-- 					else
-- 					(
-- 						if (thisCont[ind].controller as string) != "Controller:Float_Expression" do 
-- 						(
-- 							thisCont[ind].controller = Float_Expression()
-- 							
-- 							--now we'll add the scale offset shit
-- 							
-- 							fcStr = thisCont[ind].controller
-- 							
-- 							scalJsStr = ("$"+scaleJs.name+".position.controller.Zero_Pos_XYZ."+"Y"+"_Position.controller" )
-- 							scalJsStr = execute scalJsStr 
-- 							SVN = "scaleOffset"
-- 							fcStr.AddScalarTarget SVN scalJsStr
-- 					
-- 							--now we'll set the actual expression
-- 							
-- 	-- 						expString = ("(scaleOffset -1) + 1")
-- 	-- 						expString = ("(scaleOffset * -1)")
-- 							
-- 							expString = "if (scaleOffset < 0,scaleOffset + 2,scaleOffset + 1)"

-- 							fcStr.SetExpression expString
-- 						)
-- 					)
-- 				)
			)
			
		)
	)
	
	--now remove any extraneous scale controllers
-- 	break()
	removeZeroScales()
)

fn cleanControllers = --clean facial controller lists to only contain controllers that are actually used.
(	
	print ("Initialising cleanControllers...")
	
	scaleCleanup()
	bonesWithTrans = #()
	bonesJustRot = #()
	--CLEAN THE ROTATION LISTS
	for fb = 1 to faceBoneList[2].count do
	(
		maximumControllers = faceBoneList[2][fb].rotation.controller.count --get total number of controllers on the bone
		totalControllers = (facialJoysticks.count +3)
		
		for c in maximumControllers to totalControllers by -1 do --loop from the max number of joysticks (plus the 2 base controls) through to the totalControllers value
		(
			faceBoneList[2][fb].rotation.controller.delete c
		)
		
		--now check to see if the remaining controllers have anything in them and if not get rid of it
		maximumControllers = faceBoneList[2][fb].rotation.controller.count 
		totalControllers = (facialJoysticks.count +2)

		for c = totalControllers to 3 by -1 do 
		(
			
			deleteControllerX = 0
			deleteControllerY = 0
			deleteControllerZ = 0
			
			bV = "Controller:Bezier_Float" 
			
			nX = (faceBoneList[2][fb].rotation.controller[c].X_Rotation.controller as string)
			nY = (faceBoneList[2][fb].rotation.controller[c].Y_Rotation.controller as string)
			nZ = (faceBoneList[2][fb].rotation.controller[c].Z_Rotation.controller as string)
			
				if nX != bv do
				(
					deleteControllerX = 1
				)

				if nY != bV do
				(
					deleteControllerY = 1
				)
				if nZ != bV do
				(
					deleteControllerZ = 1
				)

			
			deleteTotal = (deleteControllerX + deleteControllerY + deleteControllerZ)

				if deleteTotal <= -0.1 do
				(
					faceBoneList[2][fb].rotation.controller.delete c --delete the controller
				)
		)
	)
	
	--CLEAN THE POSITION LISTS
	for tfb = 1 to faceBoneList[2].count do
	(
		hasT = undefined
		maximumControllers = faceBoneList[2][tfb].pos.controller.count
		totalControllers = (facialJoysticks.count +3)
		
		for c in maximumControllers to totalControllers by -1 do
		(
			faceBoneList[2][tfb].pos.controller.delete c --this cleans off all the un-named controllers
		)
		
		--now check to see if the remaining controllers have anything in them and if not get rid of it
		maximumControllers = faceBoneList[2][tfb].pos.controller.count 
		totalControllers = (facialJoysticks.count +2)

		for d in totalControllers to 3 by -1 do 
		(
				deleteControllerX = 0
				deleteControllerY = 0
				deleteControllerZ = 0
				
				hasT = 0
				bV = "Controller:Bezier_Float" 
				
				nX = (faceBoneList[2][tfb].pos.controller[d].X_position.controller as string)
				nY = (faceBoneList[2][tfb].pos.controller[d].Y_position.controller as string)
				nZ = (faceBoneList[2][tfb].pos.controller[d].Z_position.controller as string)
				
				if nX != bv do
				(
					deleteControllerX = 1
				)
				
				if nY != bV do
				(
					deleteControllerY = 1
				)
						
				if nZ != bV do
				(
					deleteControllerZ = 1
				)

				deleteTotal = (deleteControllerX + deleteControllerY + deleteControllerZ)

					if deleteTotal <= -0.1 then
					(
						faceBoneList[2][tfb].pos.controller.delete d --delete the controller
					)
					else
					(
						hasT = 1
					)
					
				if faceBoneList[2][tfb].pos.controller[d].name == Position_XYZ do
				(
					faceBoneList[2][tfb].pos.controller.delete d
				)
				
				if hasT == 1 then
				(
					--ok it has Trans
					--now need to add a udp tag to faceRoot
					appendIfUnique bonesWithTrans faceBoneList[2][tfb]
				)
				else
				(
					--ok it has Trans
					--now need to add a udp tag to faceRoot
					appendIfUnique bonesJustRot faceBoneList[2][tfb]
				)
		)	
	)
	debugPrint "Controllers cleaned."

	for tagBone = 1 to bonesWithTrans.count do
	(
		existingUDP = getUserPropBuffer bonesWithTrans[tagBone]
		setUserPropbuffer bonesWithTrans[tagBone] ((existingUDP)+"\r\n"+transTrackVal)
		debugPrint ("Adding T and R tag to "+bonesWithTrans[tagBone].name)
	)
	
	for rotBone = 1 to bonesJustRot.count do
	(
		existingUDP = getUserPropBuffer bonesJustRot[rotBone]
		setUserPropbuffer bonesJustRot[rotBone] ((existingUDP)+"\r\n"+rotationTrackVal)
		debugPrint ("Adding only R tag to "+bonesJustRot[rotBone].name)
	)
)

fn joystickHookup = 
(
--***************************************************************	
	-- NEED TO PUT PROGRESS BAR HERE!
--***************************************************************	
	for i = 1 to facialJoysticks.count do
	(
		if (findString facialJoysticks[i].jsName "_Ear") == undefined then
		(
			--test to see if we are tongueA b or c
			aName = facialJoysticks[i].jsName
			aLength = aName.count
			a = (substring aName (aLength - 6) 6)
			
			if a != "Tongue" then
			(
				currentJoystick = getNodeByName facialJoysticks[i].jsName
				
		-- 		messagebox ("currentJoystick = "+(currentJoystick as string))
		-- 		messagebox ("Looked for name "+facialJoysticks[i].jsName)	
				if currentJoystick == undefined then
				(
					print ("couldnt find "+facialJoysticks[i].jsName)
					--break()
				)
				else
				(
					debugprint "============================================================================================="
					debugprint ("                                  STARTING EXPRESSION CREATION FOR "+currentJoystick.name)
					debugprint "============================================================================================="
					joystickArrayToUse = multiJS_list[i]
					
					debugPrint ("js array to use = "+(joystickArraytoUse as string))
					
					if ((progBar != undefined) and (progBar.isDisplayed)) do
					(destroyDialog progBar)
					CreateDialog progBar width:300 Height:30
					progBar.prog.color = [10,240,10] --greeny
					
					for index = 1 to joystickArraytoUse.count do --loop thru each bone in the joystick array to use
					(
						boneName = (substring (joystickArraytoUse[index].jsfb_faceBone) 4 25) --strip off the fb_prefix from the start of the bone name
					
						--genExpression currentJoystick facialMultipliersCA boneName index joystickArraytoUse geo 
						
						genExpression currentJoystick joystickArraytoUse[index] geo
						
						progBar.prog.value = (100.*index/joystickArrayToUse.count)
					)
				)
			)
			else --ok we're one of the custom tongue joints
			(
				joystick = getNodeByName facialJoysticks[i].jsName
-- 				side = substring joystick.name 6 1
				
				boneNumber = substring geo.name 6 3
				
				boneName = getNodeByName (("FB_"+(substring facialJoysticks[i].jsName 6 -1)+"_"+boneNumber))

				axes = #(
					"X",
					"Y",
					"Z"
					)

				tt = #(
					"Rotation",
					"Position"
					)
				
				for transType in tt do 
				(
-- 					transType = "Rotation"
						
					-- 	joystick = joystick object e.g. CTRL_UpperLip
					-- 	boneName = bone currently having expresion added to it
					-- 	axisUsed = axis of bone we are adding expression to
					-- 	joystickAxis	= axis of joystick which is driving this expression
					--	transType = is this joystick expression for translation or rotation
						curS = selection as array
						select joystick
						
-- 						if (substring joystick.name 8 3) != "Ear" do 
-- 						(
-- 							freezeTransform()
-- 						)
-- 						select curS
						
					for axisUsed in axes do 
					(
						joystickAxis = axisUsed
						createEarExpression joystick boneName.name axisUsed joystickAxis transType geo 
					)
				)
			)
		)
		else --ok we're an ear joystick so we have to do somethign custom
		(
			--we need to create a 1 to 1 relationship between the joystick and the ear joints rotations
			joystick = getNodeByName facialJoysticks[i].jsName
			side = substring joystick.name 6 1
			
			boneNumber = substring geo.name 6 3
			
			boneName = getNodeByName ("FB_"+side+"_Ear_"+boneNumber)

			axes = #(
				"X",
				"Y",
				"Z"
				)

			transType = "Rotation"
				
			-- 	joystick = joystick object e.g. CTRL_UpperLip
			-- 	boneName = bone currently having expresion added to it
			-- 	axisUsed = axis of bone we are adding expression to
			-- 	joystickAxis	= axis of joystick which is driving this expression
			--	transType = is this joystick expression for translation or rotation
				curS = selection as array
				select joystick
				
				if (substring joystick.name 8 3) != "Ear" do 
				(
					freezeTransform()
				)
				select curS
				
			for axisUsed in axes do 
			(
				joystickAxis = axisUsed
				createEarExpression joystick boneName.name axisUsed joystickAxis transType geo 
			)
			
		)
	)

		if ((progBar != undefined) and (progBar.isDisplayed)) do
		(destroyDialog progBar)
	debugPrint "Expressions complete!"
)