-- tool to auto generate the xaf file used by the ambient facefx rig generator
-- this has to be kept in parity with the text file.
-- Matt Rennie
-- Dec 2012

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

filein (theProjectRoot + "tools/wildwest/script/3dsMax/_common_functions/FN_Rigging.ms")

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

selectedAnim = (theProjectRoot+"art/peds/Skeletons/Animations/FaceFX_AmbientRigPoses.xaf")
facePoseTextFile = (theProjectRoot+"art/peds/FaceFX/ambientFaceRigPoses.txt")

faceFxGenerationScript = (theWildWest+"script/3dsMax/Characters/Rigging/ambientFacial/faceRigger_genFaceFXSliders.ms")	

poseArray= #(
-- 	"CTRL_L_Blink",
-- 	"CTRL_L_Blink"
	) --gets populated by parsing the facePoseTextFile  via parsePoseFile fn

ambientArray = #( --array of ambient joysticks
	"Tongue",
	"L_Blink",
	"L_Cheek",
	"LowerLip",
	"UpperLip_Curl",
	"LowerLip_Curl",
	"UpperLip",
	"L_Mouth",
	"Jaw",
	"Mouth",
	"R_Eye",
	"L_Eye",
	"R_Mouth",
	"C_Brow",
	"Tongue_In_Out",
	"R_Cheek",
	"R_Brow",
	"L_Brow",
	"R_Blink",
	"MouthPinch"
	)	
	
faceBoneList = #(#())	
	
geo = undefined	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn editp4file filenameString = 
(
	gRsPerforce.add_or_edit #( filenameString ) silent:true queueAdd:false
)	
	
fn parsePoseFile input_name = 
(
	fileData = #(
		)
		
	inputData = #()	
	
	if input_name != undefined then
	(
		f = openfile input_name
-- 		inputData = #() -- define as array
		  while not eof f do
		  (
		   append inputData (filterstring (readLine f) "\n")
		  )
		  close f
	)
	print "File parsed."

	
	print ("FD Count = "+(fileData.count as string))
	
	for i = 1 to inputData.count do 
	(
		tmpArr = #(1,2,3)
		
		--ok now we need to filterstring based on the " " and split into two arrays
		
		--#("open", 10, [0.1,0,0]),
		
		flt = filterString inputData[i][1] " "
		
		if flt[1] != "CTRL_L_Blink" do
		(
			if flt[1] != "CTRL_R_Blink" do
			(
				i1 = (((flt[2] as integer) * 0.01) as float)
				jsPos = ([i1,0,0])
				
				tmpArr[1] = flt[1] --name
				tmpArr[2] = (flt[2] as float) --frame
				tmpArr[3] = (jsPos as point3) --position
				
				append poseArray tmpArr
			)
		)
		
	)

-- 	return fileData
)


fn saveAnimFile = 
(
	parsePoseFile facePoseTextFile

	--now find the frame of the first & last element and set the tiem range to be between those.

	--startFrame = poseArray[1][2]
	startFrame = 0 -- had to set this to 0 as we dont store the 'rest' pose which is at frame zero
	endFrame = poseArray[poseArray.count][2]

	animationRange = interval startFrame endFrame

	slidertime = startFrame

	--now we need to select all the facial joysticks so we can save their animation out
	clearSelection()

	for js = 1 to ambientArray.count do 
	(
		obj = getNodeByName ("CTRL_"+ambientArray[js])
		if obj != undefined then
		(
			selectMore obj
		)
		else
		(
			format ("Warning! Coould not find "+ambientArray[js]+"\n")
		)
	)

	--now we need to check out the aniumation from p4

	editp4file selectedAnim

	editp4file facePoseTextFile
	--now we need to save the animation

	LoadSaveAnimation.saveAnimation selectedAnim selection #() #() --dunno why i need to pass last 2 commands as empty arrays!
	
	messagebox ("Animation saved to "+selectedAnim+". Please remember to submit.") beep:true title:"Submit to P4"
	format ("Animation saved to "+selectedAnim+". Please remember to submit."+"\n") 
)

fn clearFaceFXPoses boneNode cont countIndex = 
(
	cntCount = cont.count
	
-- 	if cntCount > countIndex do 
-- 	(
-- 		print ("boneNode : "+boneNode.name)
-- 		print ("cont: "+(cont as string))
-- 		print ("countIndex: "+(countIndex as string))		

		for cnt = cntCount to countIndex by -1 do 
		(
-- 			print ("cnt: "+(cnt as string))
			delThis = true
			for cName in ambientArray do 
			(
				if cont.getName cnt == cName do
				(
					--print ("WOOOOOOOOOOOOOO"+cont.getName cnt+" == "+cName )
					delThis = false
				)
			)
			
			if DelThis == true do 
			(
				print ("Deleting controller "+(cont.getName cnt)+" on "+boneNode.name)
				cont.delete cnt
			)
		)
-- 	)
)

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


if ((saveFacefxAnimGUI != undefined) and (saveFacefxAnimGUI.isDisplayed)) do
	(destroyDialog saveFacefxAnimGUI)

rollout saveFacefxAnimGUI "FaceFX Tools"
(	
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:saveFacefxAnimGUI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"saveFaceFXAnimGui" filename:(getThisScriptFilename())	
	
	button btnSaveAnim "Save face Animation" tooltip:"Save animation used for FaceFX Generation" width:132 --align:#left
	button btnRegenFacefx "Regenerate Face Sliders" tooltip:"Regenerate sliders driven by FaceFX" width:132 --align:#left

	on saveFacefxAnimGUI open do 
	(
		banner.setup()
	)		
		
	on btnSaveAnim pressed do
	(
		saveAnimFile()
	)
	
	on btnRegenFacefx  pressed do 
	(
		clearListener()
		meshesFound = #() --used to store mesh names so we dont redo for duplicate names i.e. on lods
		
		for o in objects do 
		(
			if (substring o.name 2 5) == "ead_0" do 
			(
				foundBefore = false
				for m in meshesFound do 
				(
					if m == (substring o.name 1 10) do 
					(
						foundBefore = true
					)
				)
				
				if foundBefore == false do 
				(
					geo = o
					append meshesFound (substring o.name 1 10)
					print (geo.name+" found")
					--now we need to build the faceBoneList
					--firstly get the mesh number off the geo mesh
					meshNameLength = geo.name.count
					meshNumber = (substring geo.name 6 3)
					
					faceBoneList = #(#())
					
					for b in objects do 
					(
						if ((substring b.name 1 2) == "FB") do
						(
							boneNameLength = b.name.count
							if (substring b.name (boneNameLength - 2) 3) == meshNumber do 
							(
								--ok now we need to append these bones to the fabeonlist array
								
								bNameLength = b.Name.count
								bName = substring b.Name 1 (bNameLength - 4)								
								append faceBoneList[1] bName
							)
						)
					)
					
					--now we need to go through all controllers in faceBoneList and remove any existing facefx poses
					
					for fb in faceBoneList[1] do 
					(
-- 						boneNode = getNodeByName ("FB_"+fb+"_"+meshNumber)
						boneNode = getNodeByName (fb+"_"+meshNumber)
						
						print ("Cleaning: "+boneNode.name)
						cont = boneNode.position.controller
						countIndex = 3
						clearFaceFXPoses boneNode cont countIndex
						
						cont = boneNode.rotation.controller
						countIndex = 3
						clearFaceFXPoses boneNode cont countIndex
						
						cont = boneNode.scale.controller
						countIndex = 3
						clearFaceFXPoses boneNode cont countIndex						
					)
					
-- 					break()
					
					filein faceFxGenerationScript 
				)
			)
		)
		sliderTime = 0f
	)
)

CreateDialog saveFacefxAnimGUI width:150 pos:[1400, 100] 