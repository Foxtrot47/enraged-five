--tool to generate arcGui's from an existing facial rig

filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-- filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Includes\\FN_Rigging.ms")	
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

defaultGui = (theProjectRoot +("/art/peds/Skeletons/arcGuiLibrary/male_standard.max"))

geo = undefined
	
	global regenArcGuis = undefined
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn addFileToP4 filenameString = 
(
	gRsPerforce.add #(fileNameString) silent:true
)

fn editp4file filenameString = 
(
	gRsPerforce.add_or_edit #( filenameString ) silent:true queueAdd:false
)

fn saveArcGuisLcl geo headRoot = 
(
	clearSelection()
	local arcGuiParentingArray = #(#(),#())
	
	headRoot = getNodebyName ("FaceRoot_"+geo.name)
	select headRoot
	
-- 	for a in shapes do
-- 	(
-- 		if (substring a.name 1 7 ) == "arcGUI_" do
-- 		(
-- 			appendIfUnique arcGuiParentingArray[1] a
-- 		)
-- 	)
	for a in objects do
	(
		if (substring a.name 1 7 ) == "arcGUI_" do
		(
			appendIfUnique arcGuiParentingArray[1] a
		)
		--now add in the vector nodes
		if (substring a.name 1 9 ) == "v_arcGUI_" do
		(
			appendIfUnique arcGuiParentingArray[1] a
		)		
	)		
	appendIfUnique arcGuiParentingArray[1] headRoot
		
	for aIndex = 1 to arcGuiParentingArray[1].count do --this is a way to store the current parent of all gui objects so we can reparent them after save
		(
			arcGuiParentingArray[2][aIndex] = arcGuiParentingArray[1][aIndex].parent
		)
 	max unlink

	if maxFileName != "" do
	(
		for obj in arcGuiParentingArray[1] do
		(
			selectMore obj
		)
		
		Print "==WARNING!!!== File is not saved so could not save guis."
		geoName = geo.name
		currentFileName = maxFileName
		maxStr = ".max"
		repMax = ""
			currentFileName = substituteString currentFileName maxStr repMax
		currentPath = maxFilePath
		arcGuiName = (currentPath+currentFileName+"_"+geoName+"_ArcGuis")
	
		deselect $SKEL_Head
		saveNodes selection arcGuiName quiet:true
		
		--now try to add to perforce
-- 		addFileToP4 (arcGuiName+".max")
		editp4file (arcGuiName+".max")
	)
	
	for aIndex = 1 to arcGuiParentingArray[1].count do
	(
		arcGuiParentingArray[1][aIndex].parent = arcGuiParentingArray[2][aIndex]
	)
)
	
fn generateGuis headNode = 
(
	mergeMaxFile defaultGui
	
	--now we need to get the facial root node and name it to match the headNode
	
	frNode = $FaceRoot_Lib
	
	headNodeNo = substring headNode.name 6 3
	
-- 	newFrName = replace frNode.name 15 3 headNodeNo
	
	newFrName  = ("FaceRoot_"+headNode.name)
	
	frNode.name = newFrName
	
	--now we need to find a corresponding match for all of the faceBones with the headNodeNo
	
	fbArray = #()
	
	for obj in objects do 
	(
		if substring obj.name 1 2 == "FB" do 
		(
			nameLength = obj.name.count
			
			boneNo = (substring obj.name (nameLength - 2) 3 )
-- 			print ("boneNo = "+boneNo)
-- 			break()
			if substring obj.name (nameLength - 2) 3 == headNodeNo do 
			(
				append fbArray obj 
			)
		)
	)
	
	--now we have all the bones we need to find the arcGui that goes with it
	
	for fb in fbArray do 
	(
		--ok so we need to strip off the number from the end
		fbName = substring fb.name 1 (fb.name.count - 4)
		
		guiObj = getNodeByName ("arcGUI_"+fbName)

		vectorNode = getNodeByName ("v_arcGUI_"+fbName+"_FwdVect")
		
		if guiObj != undefined then
		(
			if vectornode != undefined then 
			(
				guiObj.transform = fb.transform
				
				in coordsys local rotate guiObj (angleaxis 90.0 [0,0,1])
				
				guiObj.scale = [1,1,1]
				
				fbLength = fb.length
			
				vectorLength = distance guiObj.pos vectorNode.pos
				
				factor = fbLength / vectorLength 

				scaleValue = factor * 1--00	

				guiObj.scale = [scaleValue,scaleValue,scaleValue]
				
-- 				print ("Scale set on "+guiObj.name+" to "+(scaleValue as string))
			)
			else
			(
				print ("Warning couldn't find "+("v_arcGUI_"+fbName+"_FwdVect")+" to go with "+guiObj.name+" for "+fb.name)
			)
		)
		else
		(
			print ("Warning! Couldn't find "+("arcGUI_"+fbName)+" for "+fbName)
		)
	)
	
	saveArcGuisLcl geo frNode
	
	
	destroyDialog regenArcGuis
)	

-- headNode = $'head_000_r default'

-- generateGuis headNode




------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

rollout regenArcGuis "Bone I/O"
(
	button btnPickHead "Pick Head" width:110

	on btnPickHead pressed do
	(
		headNode = pickObject  message:"Pick head geo..." prompt:"Pick head geo" select:true 
-- 		runInput()
		sela = selection as array
		if sela.count >= 1 then
		(
			geo = headNode
			generateGuis headNode
		)
		else
		(
			messagebox "Please select your meshes."
		)
		select sela
	)

)


CreateDialog regenArcGuis width:125 pos:[1450, 100] 