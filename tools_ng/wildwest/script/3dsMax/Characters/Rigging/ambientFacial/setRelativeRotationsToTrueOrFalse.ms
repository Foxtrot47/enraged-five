--laspPoseCleanup.ms
--hackky little script to remove the lastPose udp tag which is mysteriously appearing

RsCollectToolUsageData (getThisScriptFilename())


tagToRemove = #(
-- 	"lastPose = undefined",
-- 	"lastPose=undefined"
	"relRot = true",
	"relRot = false"
	)
	
fn renameTag tagToReplace obj = 
(
	tagToChangeTo = undefined
	for tg in tagToRemove do 
	(
		if tg != tagToReplace do 
		(
			tagToChangeTo = tg
		)
	)
	
		thisUdp = getUserPropBuffer obj
		
			foundTag = findString thisUdp tagToReplace
			if foundTag != undefined do 
			(
				--ok we have found that tag so need to remove it 
-- 				startStr = substring thisUdp 1 (foundTag - 1)
-- 				postStart = ((foundTag ) + (tagToReplace.count))
-- 				endTag = substring thisUdp postStart 1000
				
				startPoint = findstring thisUdp tagToReplace
-- 				endPoint = startPoint + (tagToReplace.count)
				
				tagToReplaceLength = tagToReplace.count
				
				newString = replace thisUdp startPoint tagToReplaceLength tagToChangeTo
					
				print ("Old udp:"+"\n"+thisUdp)
				print ("new udp:"+"\n"+newString)
				
				setUserPropBuffer obj newString
				print ("UDP modified on "+obj.name)
			)


)

clearListener()

for obj in selection do 
(
	tagToReplace = "relRot = true"
	renameTag tagToReplace obj
)

print"done"


global swapRelGui = undefined

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- if ((skinningInputOutputGUI != undefined) and (skinningInputOutputGUI.isDisplayed)) do
-- 	(destroyDialog skinningInputOutputGUI)

rollout swapRelGui "Swap Relative Rotations"
(
	button btnTrue "Set to True" width:110
	button btnFalse "Set to False" width:110
	
	on btnTrue pressed do
	(
		for obj in selection do 
		(
			tagToReplace = tagToRemove[2]
			renameTag tagToReplace obj
			format ("Set to True"+"\n")
		)
	)

	on btnFalse pressed do
	(
		for obj in selection do 
		(
			tagToReplace = tagToRemove[1]
			renameTag tagToReplace obj
			format ("Set to False"+"\n")
		)
	)

)

CreateDialog swapRelGui width:125 pos:[1450, 100] 