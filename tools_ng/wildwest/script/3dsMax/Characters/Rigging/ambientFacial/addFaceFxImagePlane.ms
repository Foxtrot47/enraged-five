--quick tool to create an image plane for matching facefx poses

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()


--Load common functions	
-- filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
-- filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_bone_tagger.ms")
-- filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_Rigging.ms")
-- -- filein (theProjectRoot + "tools/dcc/current/max2010/scripts/pipeline/util/xml.ms")

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())
	
imagePlane = (theProjectRoot+"art/peds/FaceFX/facePosesImageSequence/faceFXimagePlane.max")

fn createImagePlane = 
(
	planeExists = getNodeByName "faceFXimagePlane"
	
	if planeExists == undefined do 
	(
		mergeMaxFile imagePlane
		
		format ("Merged "+imagePlane+"\n")
	)
)

createImagePlane()