--tool to merge in a template head which can be used to give the user a guide on how to skin other heads
--this will merge in skeleton geo and joysticks
--the joysticks will be constrained to the joysticks in the current scene so the current joysticks will drive the merged ones.

--Mar 2013
--Matt Rennie

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())

filein (theProjectRoot + "tools_ng/wildwest/script/3dsMax/_common_functions/FN_Rigging.ms")
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

guideHeadPath = (theProjectRoot + "art/peds/AmbientHead/SkinnedTemplateHead/SkinnedTemplateHead.max")

oldTemplateData = #(
	"TEMPLATE_Ambient_UI",
	"TEMPLATE_Dummy01",
	"TEMPLATE_FaceFX",
	"TEMPLATE_RECT_ScaleOffset",
	"TEMPLATE_TEMPLATE_MESH",
	"TEMPLATE_RECT_MouthPinch",
	"TEMPLATE_CTRL_MouthPinch",
	"TEMPLATE_TEXT_MouthPinch",
	"TEMPLATE_RECT_Tongue",
	"TEMPLATE_CTRL_Tongue",
	"TEMPLATE_TEXT_Tongue",
	"TEMPLATE_RECT_Jaw",
	"TEMPLATE_CTRL_Jaw",
	"TEMPLATE_TEXT_Jaw",
	"TEMPLATE_RECT_LowerLip",
	"TEMPLATE_CTRL_LowerLip",
	"TEMPLATE_TEXT_LowerLip",
	"TEMPLATE_RECT_UpperLip",
	"TEMPLATE_CTRL_UpperLip",
	"TEMPLATE_TEXT_UpperLip",
	"TEMPLATE_RECT_L_Mouth",
	"TEMPLATE_CTRL_L_Mouth",
	"TEMPLATE_TEXT_L_Mouth",
	"TEMPLATE_RECT_Mouth",
	"TEMPLATE_CTRL_Mouth",
	"TEMPLATE_TEXT_Mouth",
	"TEMPLATE_RECT_R_Mouth",
	"TEMPLATE_CTRL_R_Mouth",
	"TEMPLATE_TEXT_R_Mouth",
	"TEMPLATE_RECT_L_Eye",
	"TEMPLATE_CTRL_L_Eye",
	"TEMPLATE_TEXT_L_Eye",
	"TEMPLATE_RECT_R_Eye",
	"TEMPLATE_CTRL_R_Eye",
	"TEMPLATE_TEXT_R_Eye",
	"TEMPLATE_RECT_L_Brow",
	"TEMPLATE_CTRL_L_Brow",
	"TEMPLATE_TEXT_L_Brow",
	"TEMPLATE_RECT_R_Brow",
	"TEMPLATE_CTRL_R_Brow",
	"TEMPLATE_TEXT_R_Brow",
	"TEMPLATE_RECT_C_Brow",
	"TEMPLATE_CTRL_C_Brow",
	"TEMPLATE_TEXT_C_Brow",
	"TEMPLATE_RECT_Tongue_In_Out",
	"TEMPLATE_CTRL_Tongue_In_Out",
	"TEMPLATE_TEXT_Tongue_In_Out",
	"TEMPLATE_RECT_LowerLip_Curl",
	"TEMPLATE_CTRL_LowerLip_Curl",
	"TEMPLATE_TEXT_LowerLip_Curl",
	"TEMPLATE_RECT_UpperLip_Curl",
	"TEMPLATE_CTRL_UpperLip_Curl",
	"TEMPLATE_TEXT_UpperLip_Curl",
	"TEMPLATE_RECT_R_Cheek",
	"TEMPLATE_CTRL_R_Cheek",
	"TEMPLATE_TEXT_R_Cheek",
	"TEMPLATE_RECT_L_Cheek",
	"TEMPLATE_CTRL_L_Cheek",
	"TEMPLATE_TEXT_L_Cheek",
	"TEMPLATE_RECT_L_Blink",
	"TEMPLATE_CTRL_L_Blink",
	"TEMPLATE_TEXT_L_Blink",
	"TEMPLATE_RECT_R_Blink",
	"TEMPLATE_CTRL_R_Blink",
	"TEMPLATE_TEXT_R_Blink",
	"TEMPLATE_mover",
	"TEMPLATE_SKEL_ROOT",
	"TEMPLATE_SKEL_Spine_Root",
	"TEMPLATE_SKEL_Spine0",
	"TEMPLATE_SKEL_Spine1",
	"TEMPLATE_SKEL_Spine2",
	"TEMPLATE_SKEL_Spine3",
	"TEMPLATE_SKEL_Neck_1",
	"TEMPLATE_RB_Neck_1",
	"TEMPLATE_SKEL_Head",
	"TEMPLATE_SKEL_Head_NUB",
	"TEMPLATE_FACIAL_facialRoot",
	"TEMPLATE_FaceRoot_Head_000_R",
	"TEMPLATE_FB_Jaw_000",
	"TEMPLATE_FB_LowerLipRoot_000",
	"TEMPLATE_FB_Tongue_000",
	"TEMPLATE_FB_LowerLip_000",
	"TEMPLATE_FB_R_Lip_Bot_000",
	"TEMPLATE_FB_L_Lip_Bot_000",
	"TEMPLATE_FB_UpperLipRoot_000",
	"TEMPLATE_FB_UpperLip_000",
	"TEMPLATE_FB_R_Lip_Top_000",
	"TEMPLATE_FB_L_Lip_Top_000",
	"TEMPLATE_FB_Brow_Centre_000",
	"TEMPLATE_FB_R_Lip_Corner_000",
	"TEMPLATE_FB_R_Brow_Out_000",
	"TEMPLATE_FB_R_CheekBone_000",
	"TEMPLATE_FB_R_Eye_000",
	"TEMPLATE_FB_R_Lid_Upper_000",
	"TEMPLATE_FB_L_Lip_Corner_000",
	"TEMPLATE_FB_L_CheekBone_000",
	"TEMPLATE_FB_L_Eye_000",
	"TEMPLATE_FB_L_Lid_Upper_000",
	"TEMPLATE_FB_L_Brow_Out_000",
	"TEMPLATE_MH_Hair_Scale",
	"TEMPLATE_IK_Head",
	"TEMPLATE_IK_Root",
	"TEMPLATE_RECT_IH",
	"TEMPLATE_TEXT_IH",
	"TEMPLATE_IH",
	"TEMPLATE_RECT_Squint",
	"TEMPLATE_TEXT_Squint",
	"TEMPLATE_Squint",
	"TEMPLATE_RECT_Brows_Down",
	"TEMPLATE_TEXT_Brows_Down",
	"TEMPLATE_Brows_Down",
	"TEMPLATE_RECT_Brow_Up_R",
	"TEMPLATE_TEXT_Brow_Up_R",
	"TEMPLATE_Brow_Up_R",
	"TEMPLATE_RECT_Brow_Up_L",
	"TEMPLATE_TEXT_Brow_Up_L",
	"TEMPLATE_Brow_Up_L",
	"TEMPLATE_RECT_tRoof_pose",
	"TEMPLATE_TEXT_tRoof_pose",
	"TEMPLATE_tRoof_pose",
	"TEMPLATE_RECT_tTeeth_pose",
	"TEMPLATE_TEXT_tTeeth_pose",
	"TEMPLATE_tTeeth_pose",
	"TEMPLATE_RECT_tBack_pose",
	"TEMPLATE_TEXT_tBack_pose",
	"TEMPLATE_tBack_pose",
	"TEMPLATE_RECT_wide_pose",
	"TEMPLATE_TEXT_wide_pose",
	"TEMPLATE_wide_pose",
	"TEMPLATE_RECT_FV",
	"TEMPLATE_TEXT_FV",
	"TEMPLATE_FV",
	"TEMPLATE_RECT_PBM",
	"TEMPLATE_TEXT_PBM",
	"TEMPLATE_PBM",
	"TEMPLATE_RECT_ShCh",
	"TEMPLATE_TEXT_ShCh",
	"TEMPLATE_ShCh",
	"TEMPLATE_RECT_W_pose",
	"TEMPLATE_TEXT_W_pose",
	"TEMPLATE_W_pose",
	"TEMPLATE_RECT_open_pose",
	"TEMPLATE_TEXT_open_pose",
	"TEMPLATE_open_pose",
	"TEMPLATE_TEXT_ScaleOffset",
	"TEMPLATE_ScaleOffset",
	"TEMPLATE_TEMPLATE_HEAD"
	)	
	
	
uiNames = #(
"Ambient_UI",
"RECT_R_Blink",
"TEXT_R_Blink",
"RECT_L_Blink",
"TEXT_L_Blink",
"RECT_L_Cheek",
"TEXT_L_Cheek",
"RECT_R_Cheek",
"TEXT_R_Cheek",
"RECT_UpperLip_Curl",
"TEXT_UpperLip_Curl",
"RECT_LowerLip_Curl",
"TEXT_LowerLip_Curl",
"RECT_Tongue_In_Out",
"TEXT_Tongue_In_Out",
"RECT_C_Brow",
"TEXT_C_Brow",
"RECT_R_Brow",
"TEXT_R_Brow",
"RECT_L_Brow",
"TEXT_L_Brow",
"RECT_R_Eye",
"TEXT_R_Eye",
"RECT_L_Eye",
"TEXT_L_Eye",
"RECT_R_Mouth",
"TEXT_R_Mouth",
"RECT_Mouth",
"TEXT_Mouth",
"RECT_L_Mouth",
"TEXT_L_Mouth",
"RECT_UpperLip",
"TEXT_UpperLip",
"RECT_LowerLip",
"TEXT_LowerLip",
"RECT_Jaw",
"TEXT_Jaw",
"RECT_Tongue",
"TEXT_Tongue",
"RECT_MouthPinch",
"TEXT_MouthPinch"
	)

joystickNames = #(
"CTRL_R_Blink",
"CTRL_L_Blink",
"CTRL_L_Cheek",
"CTRL_R_Cheek",
"CTRL_UpperLip_Curl",
"CTRL_LowerLip_Curl",
"CTRL_Tongue_In_Out",
"CTRL_C_Brow",
"CTRL_R_Brow",
"CTRL_L_Brow",
"CTRL_R_Eye",
"CTRL_L_Eye",
"CTRL_R_Mouth",
"CTRL_Mouth",
"CTRL_L_Mouth",
"CTRL_UpperLip",
"CTRL_LowerLip",
"CTRL_Jaw",
"CTRL_Tongue",
"CTRL_MouthPinch"
	)	
	
errorItems = #() --used to store any items which we couldnt find for debug at the end	
	
debugPrintVal = true	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	
fn addUiNodesToLayer =	
(
	layer = layermanager.getLayerFromName "TemplateUI" 

	if layer == undefined do 
	(
		layer = layerManager.newLayerFromName "TemplateUI"
	)
	
	for ui in uiNames do 
	(
		obj = getNodeByName ("TEMPLATE_"+ui)
		if obj != undefined do 
		(
			layer.addNode obj
		)
	)
	
	for js in joystickNames do 
	(
		obj = getNodeByName ("TEMPLATE_"+js)
		if obj != undefined do 
		(
			layer.addNode obj
		)
	)	
	
	--now we need to set the layer to be frozen and hidden
	layer.ishidden  = true
	layer.isFrozen = true
	
	layer = layermanager.getLayerFromName "TemplateGEO" 

	if layer == undefined do 
	(
		layer = layerManager.newLayerFromName "TemplateGEO"
	)	
	
	obj = getnodebyName "TEMPLATE_TEMPLATE_HEAD"
	
	if obj != undefined do 
	(
		layer.addnode obj
	)
	
	for obj in objects do 
	(
		if substring obj.name 1 11 == "TEMPLATE_FB" do 
		(
			layer.addnode obj
		)
	)
	
	layer.ishidden  = false
	layer.isFrozen = true
)

fn generateUiExpression expObj expCont scalarObjects scalarNames scalarConts expString=
(
	/*
	need to pass in:
		1) expObj =  name of object the expression is for
		2) expCont = controller the expression is on (as a string)
		3) scalarObjects = name of objects used as drivers as an array of string 
		4) scalarNames = name of scalars - do this as an array (of strings) so we cna loop through multiple scalars
		5) scalarConts = controller the scalars point to - do this as array (of strings) with an entry for each item in the scalarNames array
		6) expString = full expression string
	*/

-- 	exController = "$"+expObj+expCont+" = float_Expression ()"
-- 	exCnt = execute exController

	expString = ("("+expString+")")
	
-- 	print "----------------------------------------------------------------------------"
-- 	print "\r\n"
-- 	print ("Creating expression on "+expObj+". Scalar of "+(scalarObjects as string))
-- 	print ("Using expression:")
-- 	print expString
-- 	print "\r\n"
-- 	print "----------------------------------------------------------------------------"
print "-"	
print ("Expression controller going on "+(expObj as string)+": "+(expCont as string))
print ("Driven by "+(scalarObjects[1] as string)+" controller: "+(scalarConts[1] as string))
print ("with a scalar name of "+(scalarNames[1])+" using expression "+(expString))
print "-------------------------------------------------------------------------------------"		
-- 	fcStr = ( "$"+expObj+expCont) --rebuild the string so it can be used below	
-- 		fcStr = (expObj+expCont)
-- 		local newfcStr = execute fcStr
		
	newExpCont = (expCont+" = Float_Expression()")	
print ("Trying to execute "+newExpCont )	
-- $TEMPLATE_CTRL_R_Blink.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller[1].controller = Float_Expression()	
	execute newExpCont
-- 	newfcStr = expCont
		newfcStr = execute newExpCont
	print ("newfcStr = "+(newfcStr as string))
-- 	
-- 		newfcStr = exCnt
		
	if scalarNames.count != 0 do
	(
		for i = 1 to scalarNames.count do --this should loop through the array of scalars and their controllers and create a scalar for each
		(
			
			SVN = scalarNames[i] --scalar variable name
			print ("SVN = "+(SVN as string))
			
			scalContStr = (scalarConts[i] as string)
				print ("scalCont  = "+scalContStr)
				scalCont = execute scalContStr
				
			
			newfcStr.AddScalarTarget SVN scalCont --add scalar pointing to the controller of the scalar object
				
		)
	)	
	print ("setting expression to :\r\n"+expString)

		newfcStr.SetExpression expString
			
			print ("Expression created on "+expObj+" for "+expCont)
			print "```````````````````````````````````````````````````````"
	
)	
		
fn constrainItems source target = 
(
	posCtrl = source.pos.controller = Position_Constraint ()

	posConstraintInterface = posCtrl.constraints

	posConstraintInterface.appendTarget target 100.0
)

fn expressionItems source target axis =
(
-- 	if axis == "X" then
-- 	(
-- 		target.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller.Limited_Controller__Bezier_Float.controller = Float_Expression ()
-- 		print ("added float expression to X on "+target.name)
-- 	)
-- 	else
-- 	(
-- 		target.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller.Limited_Controller__Bezier_Float.controller = Float_Expression ()
-- 		print ("added float expression to Y on "+target.name)
-- 	)

	scalarContsStr = ("$"+source.name+".pos.controller.Zero_Pos_XYZ.controller."+axis+"_Position.controller")
	/*
	need to pass in:
		1) expObj =  name of object the expression is for
		2) expCont = controller the expression is on (as a string)
		3) scalarObjects = name of objects used as drivers as an array of string 
		4) scalarNames = name of scalars - do this as an array (of strings) so we cna loop through multiple scalars
		5) scalarConts = controller the scalars point to - do this as array (of strings) with an entry for each item in the scalarNames array
		6) expString = full expression string
	*/
	
	expObj = target.name
-- 	expCont = execute expContStr
-- 	expCont = ("$"+target.name+".pos.controller.Zero_Pos_XYZ.controller."+axis+"_Position.controller.Limited_Controller__Bezier_Float.controller")
	expCont = ("$"+target.name+".pos.controller.Zero_Pos_XYZ.controller."+axis+"_Position.controller[1].controller")
	scalarObjects = #(source.name)
	scalarNames = #(source.name+"_"+axis)
-- 	scalarConts = #(scalarContsStr)
	scalarConts = #(scalarContsStr)
	expString = scalarNames[1]
	
	print ("expObj: "+(expObj  as string))
	print ("expCont : "+(expCont as string))
	print ("scalarObjects: "+(scalarObjects as string))
	print ("scalarNames: "+(scalarNames as string))
	print ("scalarConts: "+(scalarConts  as string))
	print ("expString: "+(expString as string))
-- break()	
	generateUiExpression expObj expCont scalarObjects scalarNames scalarConts expString
)


fn removeOldTemplateData = 
(
	for objName in oldTemplateData do 
	(
		obj = getNodeByName objName
		if obj != undefined do 
		(
			delete obj
		)
	)
)

fn mergeGuideHead = 
(
	--first off remove any old template head data
	removeOldTemplateData()
	
	--turn off the gta callbacks
	callbacks.removeScripts id:#RsGlobalCallbacks	
	
	WWP4vSync guideHeadPath
	
	mergeMaxFile guideHeadPath quiet:true

	--turn back on the gta callbacks
	filein (theProjectRoot+ "tools_ng/dcc/current/max2012/scripts/pipeline/util/global_callbacks.ms")
	
	--now we need to constrain the guide joysticks to the scene joysticks

	for js in joystickNames do 
	(
		thisJs = getNodeByName js
		
		guideJs = getNodeByName ("TEMPLATE_"+js)
		
		if thisJs != undefined then
		(
			if guideJs != undefined then
			(
				expressionItems thisJs guideJs "X"
				expressionItems thisJs guideJs "Y"
			)
			else
			(
				append errorItems ("TEMPLATE_"+js)
			)
		)
		else
		(
			append errorItems js
		)
		
	)
	
	for ui in uiNames do 
	(
		thisUI = getNodeByName ui
		
		guideUi = getnodebyName ("TEMPLATE_"+ui)
		
		if thisUI != undeifned then
		(
			if guideUI != undefined then
			(
				constrainItems thisUI guideUI
			)
			else
			(
				append errorItems ("TEMPLATE_"+ui)
			)
		)
		else
		(
			append errorItems ui
		)
	)
	
	addUiNodesToLayer()
	
	--now print out some error info if any was collected
	for err in errorItems do 
	(
		format (err+" not found."+"\n")
	)
	

)


mergeGuideHead()