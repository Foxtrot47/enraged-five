fn fixAmbient1DTracks =
(
	jsArr = #(
	$CTRL_L_Blink,
	$CTRL_L_Cheek,
	$CTRL_UpperLip_Curl,
	$CTRL_R_Cheek,
	$CTRL_R_Blink,
	$CTRL_MouthPinch,
	$CTRL_Tongue_In_Out,
	$CTRL_LowerLip_Curl	
		)

	for obj in jsArr do 
	(
		udp = ("tag = "+obj.name+"\r\n"+"translateTrackType=TRACK_VISEMES"+"\r\n"+"exportTrans = true"+"\r\n")
		
		setUserPropBuffer obj udp
		
		format ("Track Viseme set on "+obj.name+"\n")
	)
)

fixAmbient1DTracks()