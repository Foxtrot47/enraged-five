--tool to query animations and turn into struct

ambientArray = #( --array of all joysticks from the ambient facial setup
	"Tongue",
	"L_Blink",
	"LookAT_Activator",
	"L_Cheek",
	"LowerLip",
	"UpperLip_Curl",
	"LowerLip_Curl",
	"UpperLip",
	"L_Mouth",
	"Jaw",
	"Mouth",
	"R_Eye",
	"L_Eye",
	"R_Mouth",
	"C_Brow",
	"Tongue_In_Out",
	"R_Cheek",
	"R_Brow",
	"L_Brow",
	"R_Blink"
	)
	
poseArray= #( --array of all poses which are passed to the facefx software (these poses are initially derived from moving the ambient facial joysticks)
#("Neutral", 0),
#("open", 10),
#("W", 20),
#("ShCh", 30),
#("PBM", 40),
#("FV", 50),
#("wide", 60),
#("tBack", 70),
#("tTeeth", 80),
#("tRoof", 90),
#("Blink_Left", 100),
#("Blink_Right", 110),
#("Eyebrow_Raise_Left", 120),
#("Eyebrow_Raise_Right", 130),
#("Squint_Left", 140),
#("Squint_Right", 150),

-- #("Head_Pitch_Pos", 200),
-- #("Head_Yaw_Pos", 210),
-- #("Head_Roll_Pos", 220),
#("LeftEye_Pitch_Pos", 230),
#("LeftEye_Yaw_Pos", 240),
#("RightEye_Pitch_Pos", 250),
#("RightEye_Yaw_Pos", 260),

-- // Negative Rotations below
-- #("Head_Pitch_Neg", 270),
-- #("Head_Yaw_Neg", 280),
-- #("Head_Roll_Neg", 290),
#("LeftEye_Pitch_Neg", 300),
#("LeftEye_Yaw_Neg", 310),
#("RightEye_Pitch_Neg", 320),
#("RightEye_Yaw_Neg", 330),

#("AU1-Inner_Brow_Raiser_Left", 400),
#("AU1-Inner_Brow_Raiser_Right", 410),
#("AU2-Outer_Brow_Raiser_Left", 420),
#("AU2-Outer_Brow_Raiser_Right", 430),
#("AU4-Brow_Lowerer", 440),
#("AU10-Upper_Lip_Raiser", 450),
#("AU15-Lip_Corner_Depressor_Left", 460),
#("AU15-Lip_Corner_Depressor_Right", 470),
#("AU16-Lower_Lip_Depressor", 480),
#("AU20-Lip_Stretcher", 490),
#("AU23-Lip_Tightener", 500),
#("AU26-Jaw_Drop", 510),
#("AU29-Jaw_Sideways", 530),

#("Cheek_Up_Left", 600),
#("Cheek_Up_Right", 610),
#("Snarl_Left", 620),
#("Snarl_Right", 630)
)	

fn generate1dStruct = 
(
	for i = 1 to poseArray.count do --this looks through the PoseArray and figures out which ambient joysticks move to generate that pose
	(
		--first set frame to the 2nd element of this item ie frame number
		myFrame = posearray[i][2]
		poseName = poseArray[i][1]
		sliderTime = myFrame
		--now for each joystick query their x and y positions
		
		for js = 1 to ambientArray.count do
		(
			thisJoystick = getNodeByName ("CTRL_"+ambientArray[js])
			jsPos = in coordsys parent thisJoystick.position
			
			myX = jsPos[1]
			newX = 1 / myX
			jsX = myX * newX as integer
			
			myY = jsPos[2]
			newY = 1 / myY
			jsY = myY * newY as integer
			
			myStr = ("(driverParamStruct ffxSlider:"+"\""+"'"+poseName+"'" +"ambientJS:+"+"\""+"'"+ambientArray[js]+"'"+"\""+" multX:"+(jsX as string)+" multY:"+(jsY as string)+" ind:"+(i as string)+"),")
			print myStr
		)
	)
)

clearListener()


generate1dStruct()