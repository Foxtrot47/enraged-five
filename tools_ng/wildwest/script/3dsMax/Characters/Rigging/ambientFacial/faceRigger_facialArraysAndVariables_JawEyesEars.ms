-- messagebox ("ANIMALS!")
arcGuiLibraryPath = (theProjectRoot + "art/peds/Skeletons/arcGuiLibrary/")
	debugPrint ("arcGuiLibraryPath = "+arcGuiLibraryPath )
headBone = "SKEL_Head"
helpImagePath = (theProjectRoot + "/art/peds/Skeletons/Facial/images/")

rotDivider = "(0.5 /4)"
transDivider = "(0.005 /4)"

createFaceFX = undefined

--defaultAttrFileM = (RsConfigGetWildWestDir() + "script\\3dsMax\\Characters\\Rigging\\ambientFacial\\facialDefaultsM.atr") --male default attributes


--at present it uneccesary to have male and female atr files, so setting them both to use the female.
defaultAttrFileM = (RsConfigGetWildWestDir() + "script\\3dsMax\\Characters\\Rigging\\ambientFacial\\facialDefaults_JawEyesEars.atr") --female default attributes

defaultAttrFileF = (RsConfigGetWildWestDir() + "script\\3dsMax\\Characters\\Rigging\\ambientFacial\\facialDefaults_JawEyesEars.atr") --female default attributes


transTrackVal = ("exportTrans = true")
rotationTrackVal = ("exportTrans = false")

jointCreationArray = #( --index 1 = the node itself index2 == the node we want it to point at
	--if index 2 == "FORWARD then the tool will just point it directly forward of its pivot, if index2 == ""fwdVect" then it will point towards its custom forward vector helper

	#("arcGUI_FB_L_Lid_Upper" , "fwdVect"),
	#("arcGUI_FB_L_Eye" , "fwdVect"),
	#("arcGUI_FB_Jaw" , "fwdVect"),
	#("arcGUI_FB_TongueA" , "arcGUI_FB_TongueB"),
	#("arcGUI_FB_R_Lid_Upper" , "fwdVect"),
	#("arcGUI_FB_R_Eye" , "fwdVect"),
	#("arcGUI_FB_TongueB" , "arcGUI_FB_TongueC"),
	#("arcGUI_FB_TongueC" , "fwdVect"),
	#("arcGUI_FB_R_Ear" , "fwdVect"),
	#("arcGUI_FB_L_Ear" , "fwdVect")
	)

--this is the array of all arcGui names. This has to be declared seperately from the arcNameList as arcNameList is semi-dynamic and needs this to evaluate its 2nd dimnension. 
arcNameArray = #( --THIS SHOULD NEVER BE REFERRED TO IN SCRIPT. WE SHOULD ONLY REFERENCE THESE NAMES VIA arcNameList.

	"arcGUI_FB_L_Lid_Upper", --1
	"arcGUI_FB_L_Eye", --2
	"arcGUI_FB_Jaw", --3
	"arcGUI_FB_TongueA", --4
	"arcGUI_FB_R_Lid_Upper", --5
	"arcGUI_FB_R_Eye", --6
	"arcGUI_FB_TongueB", --7
	"arcGUI_FB_TongueC", --8
	"arcGUI_FB_R_Ear", --9
	"arcGUI_FB_L_Ear" --10	
	)

--array of all arcGui objects (last item is simply there as a label on final button)
arcNameList = #( -- first dimension is pulling items defined in arcNameArray, 2nd dimension is defining parenting, 3rd dimension is defualt gui positions
	#( 
	--DIMENSION 1
	arcNameArray[1], --1 arcGUI_FB_L_Lid_Upper
	arcNameArray[2], --2 arcGUI_FB_L_Eye
	arcNameArray[3], --3 arcGUI_FB_Jaw
	arcNameArray[4], --4 arcGUI_FB_TongueA
	arcNameArray[5], --5 arcGUI_FB_R_Lid_Upper
	arcNameArray[6], --6 arcGUI_FB_R_Eye
	arcNameArray[7], --7 arcGUI_FB_TongueB
	arcNameArray[8], --8 arcGUI_FB_TongueC
	arcNameArray[9], --9 arcGUI_FB_R_Ear
	arcNameArray[10] --10 arcGUI_FB_L_Ear	
	),
	#(	--array of parents for the nodes in the 1st dimension
	--DIMENSION 2
	headBone, --1 arcGUI_FB_L_Lid_Upper
	headBone, --2 arcGUI_FB_L_Eye
	headBone, --3 arcGUI_FB_Jaw
	arcNameArray[3], --4 arcGUI_FB_TongueA
	headBone, --5 arcGUI_FB_R_Lid_Upper
	headBone, --6 arcGUI_FB_R_Eye
	arcNameArray[4],--7 arcGUI_FB_TongueB
	arcNameArray[7], --8 arcGUI_FB_TongueC
	headBone, --9 arcGUI_FB_R_Ear
	headBone --10 arcGUI_FB_L_Ear	
	),
	#( --array of initial transform for MALE arcGuis		
	--DIMENSION 3
	),
	#( --dynamically created array of names for items in the listbox - if gui exists then we get a stripped version of its name, otherwise we get the name with warning characters on it.
		--DIMENSION 4
	),
	#(--array of all arcGui help images
		--DIMENSION 5
		(helpImagePath+(substring arcNameArray[1] 11 50)+".jpg"),--1 arcGUI_FB_L_Lid_Upper
		(helpImagePath+(substring arcNameArray[2] 11 50)+".jpg"),--2 arcGUI_FB_L_Eye
		(helpImagePath+(substring arcNameArray[3] 11 50)+".jpg"),--3 arcGUI_FB_Jaw
		(helpImagePath+(substring arcNameArray[4] 11 50)+".jpg"),--4 arcGUI_FB_TongueA
		undefined,--5 arcGUI_FB_R_Lid_Upper
		undefined,--6 arcGUI_FB_R_Eye
		(helpImagePath+(substring arcNameArray[7] 11 50)+".jpg"), --7 - arcGUI_FB_TongueB
		(helpImagePath+(substring arcNameArray[8] 11 50)+".jpg"), --8 - arcGUI_FB_TongueC
		(helpImagePath+(substring arcNameArray[9] 11 50)+".jpg"),--9 - arcGUI_FB_R_Ear
		(helpImagePath+(substring arcNameArray[10] 11 50)+".jpg") --10 - arcGUI_FB_L_Ear		
		),--,
		#(--dynamically created list of all arcGui objects
			--DIMENSION 6
		),
	#( --array of initial transforms for FEMALE arcGuis				
	--DIMENSION 7
	".transform = (matrix3 [0.542387,0.105729,0] [-0.105729,0.542387,0] [0,0,0.552596] [0.0219011,-0.276751,0.316263])",-- 1arcGUI_FB_L_Lid_Upper	
	".transform = (matrix3 [0.542387,0.105729,0] [-0.105729,0.542387,0] [0,0,0.552596] [0.0219011,-0.276751,0.316263])", -- 2arcGUI_FB_L_Eye
	".transform = (matrix3 [2.35681,0,0] [0,2.05477,1.15433] [0,-1.15433,2.05477] [0,-0.24678,0.282174])", -- 3 arcGUI_FB_Jaw
	".transform = (matrix3 [1.01907,0,0] [0,0.876317,0.52016] [0,-0.52016,0.876317] [0,-0.249519,0.290529])", --4 arcGui_FB_TongueA
	undefined, --"arcGUI_FB_R_Lid_Upper", --5
	undefined, --"arcGUI_FB_R_Eye", --6
	".transform = (matrix3 [0.930381,0,0] [0,0.811348,0.455326] [0,-0.455326,0.811348] [0,-0.26865,0.279178])", --7 tongue B
	".transform = (matrix3 [0.320329,0,0] [0,0.294091,0.126969] [0,-0.126969,0.294091] [0,-0.285105,0.269695])", --8 tongue C
	undefined, --9 R Ear
	".transform = (matrix3 [1.40827,0.804288,-0.506027] [-0.882809,0.772693,-1.22873] [-0.351555,1.2815,1.05846] [0.039193,-0.238385,0.364023])" --10 l ear	
	)
)
	
-- arcNameListCopy = deepcopy arcNameList
-- this is an array of all the 'right' guis. This needs to match with the 'left' list as they are paired together in order to mirror transforms	 
rightGuis = #( 
	arcNameList[1][5], --15 arcGUI_FB_R_Lid_Upper 
	arcNameList[1][6], --16 arcGUI_FB_R_Eye 
	arcNameList[1][9] --arcGUI_FB_R_Ear
	)	

--list of all arcguis for left side
leftGuis = #( 
	arcNameList[1][1], --3 arcGUI_FB_L_Lid_Upper 
	arcNameList[1][2], --4 arcGUI_FB_L_Eye 
	arcNameList[1][10] --arcGUI_FB_L_Ear
	)	
	
arcGuiParentingArray = #( --2d array used for physical objects in the scene and their parenting for arcGuis
		#(),
		#()
		)	

--this is the array of all faceBone names. This has to be declared seperately from the faceBoneList as faceBoneList is semi-dynamic 
--and needs this to evaluate its 2nd dimnension. 	
faceBoneNameList = #( --THIS SHOULD NEVER BE REFERRED TO IN SCRIPT. WE SHOULD ONLY REFERENCE THESE NAMES VIA faceBoneList.
"FB_L_Lid_Upper", --1
"FB_L_Eye", --2
"FB_Jaw", --3
"FB_TongueA", --4
"FB_R_Lid_Upper", --5
"FB_R_Eye", --6
"FB_TongueB", --7
"FB_TongueC", --8
"FB_R_Ear", --9
"FB_L_Ear" --10


	)
	
faceBoneList = #(
	#(--DIMENSION 1
	faceBoneNameList[1], --1  = FB_L_Lid_Upper
	faceBoneNameList[2], --2  = FB_L_Eye
	faceBoneNameList[3], --3  = FB_Jaw
	faceBoneNameList[4], --4  = FB_Tongue A
	faceBoneNameList[5], --5  = FB_R_Lid_Upper
	faceBoneNameList[6], --6  = FB_R_Eye
	faceBoneNameList[7], --7 = FB_TongueB
	faceBoneNameList[8], --8 = FB_TongueC
	faceBoneNameList[9], --9 = FB_R_Ear_Root
	faceBoneNameList[10] --10 = FB_L_Ear_Root
	),
	#(--dynamically populated array of actual bones we want to parent, generated via querying headRoot node and bones udp tag
		--DIMENSION 2
		),

	#(		--array of parents for the nodes in the 1st dimension. "headRootBone" is declared as a string as this is dependant on the number of them we have in the scene.
		--this array gets re-populated during faceRigCreation
		--DIMENSION 3
	"headRootBone", --1 arcGUI_FB_L_Lid_Upper
	"headRootBone", --2 arcGUI_FB_L_Eye
	"headRootBone", --3 arcGUI_FB_Jaw
	faceBoneNameList[3], --4 arcGUI_FB_TongueA ***************
	"headRootBone", --5 arcGUI_FB_R_Lid_Upper
	"headRootBone", --6 arcGUI_FB_R_Eye
	faceBoneNameList[4], --7 tongueB
	faceBoneNameList[7], --8 tongueC
	"headRootBone", --9 arcGUI_FB_R_Ear_Root
	"headRootBone" --10 arcGUI_FB_L_Ear_Root	
	)		
)

customTongueArray = #( --used for making custom tongue joysticks
	faceBoneNameList[4],
	faceBoneNameList[7],
	faceBoneNameList[8]
	)

-- jsfb_faceBone = bone driven by this joystick
--jsfb_Type - 1 = vert 2 = sq
--jsfb_Square - 1 = vert 2 = sq
-- jsfb_fbAxisTX = bone trsans axis driven by X trans of joystick
-- jsfb_fbAxisTY = bone trans axis driven by Y trans of joystick
-- jsfb_fbAxisTZ:undefined jsfb_fbAxisRX = bone rot axis driven by X trans of joystick
-- jsfb_fbAxisRY = bone rot axis driven by Y trans of joystick
struct jsfb_Settings (jsfb_faceBone, jsfb_Type, jsfb_fbAxisTX, jsfb_fbAxisTY, jsfb_fbAxisTZ, jsfb_fbAxisRX, jsfb_fbAxisRY,jsfb_fbAxisSX, jsfb_fbAxisSY, jsfb_Square)


multiJS_list = #( --multi dimensional array storing pointers to which face bones are used by which joystick. THESE ARE INDEXED TO MATCH THE FACIALJOYSTICKS STRUCT BELOW
-- **********************************************************************************************************************************************************************************************************************
-- **********************************************************************************************************************************************************************************************************************
-- I SHOULD TURN THIS INTO A STRUCT THEN I CAN DEFINE ON A JOYSTICK AND BONE BASIS IF WE WANT TO DO TRANS AND ROTATION.
-- **********************************************************************************************************************************************************************************************************************
-- **********************************************************************************************************************************************************************************************************************

	--this gets used by joystickMultipliers.ms
	--------	
	#( --array of stuff for CTRL_R_Blink
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][5] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:undefined 	jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:1) -- r_lid_Upper
		),
		
	#(--array of stuff for CTRL_L_Blink
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][1] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:undefined 	jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:1) --L_Lid_Upper
		),
-- 	#(--array of stuff for CTRL_Tongue_In_Out
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][4] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined				jsfb_fbAxisRX:undefined		jsfb_fbAxisRY:"Y" 	jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined		jsfb_Square:1) -- Tongue
-- 		),
	#(--array of stuff for CTRL_R_Eye
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][6] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 	jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- R_Eye
		),
	#(--array of stuff for CTRL_L_Eye
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][2] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 	jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- L_Eye ***
		),
	#(--array of stuff for CTRL_Jaw
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][3] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 	jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- Jaw		
		),
	#(--array of stuff for CTRL_TongueA
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][4] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined		jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:2), -- TongueA
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][7] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined		jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:2), -- TongueB
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][8] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined		jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:2) -- TongueC
		),
	#(--array of stuff for CTRL_L_Ear
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][10] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 	jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- L ear
		),
	#(--array of stuff for CTRL_R_Ear
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][9] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 	jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- R ear
		),
	#(--array of stuff for CTRL_TongueC
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][4] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined		jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:2), -- TongueA
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][7] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined		jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:2), -- TongueB
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][8] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined		jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:2) -- TongueC
		),
	#(--array of stuff for CTRL_TongueC
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][4] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined		jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:2), -- TongueA
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][7] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined		jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:2), -- TongueB
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][8] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined		jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:2) -- TongueC
		)		
)


----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------


-- jsName = name of joystick
-- jsShape -  1 = vertical 2 = square
--jsType - 1 = trans 2 = nonetrans
--jsPos = position of joysticks 'Rect' object
struct js_Params ( jsName, jsShape, jsType, jsPos )
	
facialJoysticks = #(--array of all joystic strcuts 
	(js_Params jsName:"CTRL_R_Blink" jsShape:1 jsType:1 jsPos:[-3.5,-4.5,0]),
 	(js_Params jsName:"CTRL_L_Blink" jsShape:1 jsType:1 jsPos:[3.5,-4.5,0]),
-- 	(js_Params jsName:"CTRL_Tongue_In_Out" jsShape:1 jsType:1 jsPos:[-5,-13.5,0]),
	(js_Params jsName:"CTRL_R_Eye" jsShape:2 jsType:2 jsPos:[-1.5,-4.5,0]),
	(js_Params jsName:"CTRL_L_Eye" jsShape:2 jsType:2 jsPos:[1.5,-4.5,0]),
	(js_Params jsName:"CTRL_Jaw" jsShape:2 jsType:2 jsPos:[0,-7.5,0]),
	(js_Params jsName:"CTRL_TongueA" jsShape:2 jsType:2 jsPos:[-3,-13.5,0]),
	(js_Params jsName:"CTRL_L_Ear" jsShape:2 jsType:2 jsPos:[-3,-13.5,0]),
	(js_Params jsName:"CTRL_R_Ear" jsShape:2 jsType:2 jsPos:[-3,-13.5,0]),
	(js_Params jsName:"CTRL_TongueB" jsShape:2 jsType:2 jsPos:[-3,-13.5,0]),
	(js_Params jsName:"CTRL_TongueC" jsShape:2 jsType:2 jsPos:[-3,-13.5,0])		
)

controlNames = #( --gets populated dynamically by the main user interface 'facial' window when the multipliers button is pressed.
	undefined,
	undefined,
-- 	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,	
	undefined
-- 	undefined,
-- 	undefined,
-- 	undefined,
-- 	undefined,
-- 	undefined,
-- 	undefined,
-- 	undefined,
-- 	undefined,
-- 	undefined,
-- 	undefined,
-- 	undefined,
-- 	undefined,	
-- 	undefined
)

lyrNames = #( --list of layers
		"FacialUI_Controls", 
		"FacialUI_Visuals"
		)	
		
	runCleanUpFN = true

nodesToKill = #( --joystick nodes to remove
"Tongue",
"Tongue_In_Out",
"LookAT_Activator"	
)	