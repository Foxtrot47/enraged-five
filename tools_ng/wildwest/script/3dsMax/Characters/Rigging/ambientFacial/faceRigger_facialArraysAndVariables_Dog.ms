-- messagebox ("ANIMALS!")
arcGuiLibraryPath = (theProjectRoot + "art/peds/Skeletons/arcGuiLibrary/")
	debugPrint ("arcGuiLibraryPath = "+arcGuiLibraryPath )
headBone = "SKEL_Head"
helpImagePath = (theProjectRoot + "/art/peds/Skeletons/Facial/images/")

rotDivider = "(0.5 /4)"
transDivider = "(0.005 /4)"

createFaceFX = undefined

--defaultAttrFileM = (RsConfigGetWildWestDir() + "script\\3dsMax\\Characters\\Rigging\\ambientFacial\\facialDefaultsM.atr") --male default attributes


--at present it uneccesary to have male and female atr files, so setting them both to use the female.
defaultAttrFileM = (RsConfigGetWildWestDir() + "script\\3dsMax\\Characters\\Rigging\\ambientFacial\\facialDefaultsDog.atr") --female default attributes

defaultAttrFileF = (RsConfigGetWildWestDir() + "script\\3dsMax\\Characters\\Rigging\\ambientFacial\\facialDefaultsDog.atr") --female default attributes


transTrackVal = ("exportTrans = true")
rotationTrackVal = ("exportTrans = false")

jointCreationArray = #( --index 1 = the node itself index2 == the node we want it to point at
	--if index 2 == "FORWARD then the tool will just point it directly forward of its pivot, if index2 == ""fwdVect" then it will point towards its custom forward vector helper
	#("arcGUI_FB_L_Brow_Out" ,"fwdVect" ),
	#("arcGUI_FB_Brow_Centre" , "fwdVect"),
	#("arcGUI_FB_L_Lid_Upper" , "fwdVect"),
	#("arcGUI_FB_L_Eye" , "fwdVect"),
	#("arcGUI_FB_L_CheekBone" , "fwdVect"),
	#("arcGUI_FB_UpperLipRoot" , "arcGUI_FB_UpperLip"), --**
	#("arcGUI_FB_UpperLip" ,"fwdVect" ), 
	#("arcGUI_FB_L_Lip_Corner" , "fwdVect"),
	#("arcGUI_FB_LowerLipRoot" , "arcGUI_FB_LowerLip"),
	#("arcGUI_FB_LowerLip" , "fwdVect" ),
	#("arcGUI_FB_Jaw" , "fwdVect"),
	#("arcGUI_FB_L_Lip_Top" , "fwdVect" ),
	#("arcGUI_FB_L_Lip_Bot" , "fwdVect" ),
-- 	#("arcGUI_FB_Tongue" , "fwdVect"),
	#("arcGUI_FB_TongueA" , "arcGUI_FB_TongueB"),
	#("arcGUI_FB_R_Lid_Upper" , "fwdVect"),
	#("arcGUI_FB_R_Eye" , "fwdVect"),
	#("arcGUI_FB_R_CheekBone" , "fwdVect"),
	#("arcGUI_FB_R_Brow_Out" , "fwdVect"),
	#("arcGUI_FB_R_Lip_Top" , "fwdVect" ),
	#("arcGUI_FB_R_Lip_Bot" , "fwdVect"),
	#("arcGUI_FB_R_Lip_Corner" , "fwdVect" ),
	#("arcGUI_FB_TongueB" , "arcGUI_FB_TongueC"),
	#("arcGUI_FB_TongueC" , "fwdVect"),
	#("arcGUI_FB_R_Ear" , "fwdVect"),
	#("arcGUI_FB_L_Ear" , "fwdVect")
	)

--this is the array of all arcGui names. This has to be declared seperately from the arcNameList as arcNameList is semi-dynamic and needs this to evaluate its 2nd dimnension. 
arcNameArray = #( --THIS SHOULD NEVER BE REFERRED TO IN SCRIPT. WE SHOULD ONLY REFERENCE THESE NAMES VIA arcNameList.
	"arcGUI_FB_L_Brow_Out", --1
	"arcGUI_FB_Brow_Centre", --2 ********
	"arcGUI_FB_L_Lid_Upper", --3
	"arcGUI_FB_L_Eye", --4
	"arcGUI_FB_L_CheekBone", --5
	"arcGUI_FB_UpperLipRoot", --6
	"arcGUI_FB_UpperLip", --7
	"arcGUI_FB_L_Lip_Corner", --8
	"arcGUI_FB_LowerLipRoot", --9
	"arcGUI_FB_LowerLip", --10
	"arcGUI_FB_Jaw", --11
	"arcGUI_FB_L_Lip_Top", --12
	"arcGUI_FB_L_Lip_Bot", --13
	"arcGUI_FB_TongueA", --14
	"arcGUI_FB_R_Lid_Upper", --15
	"arcGUI_FB_R_Eye", --16
	"arcGUI_FB_R_CheekBone", --17
	"arcGUI_FB_R_Brow_Out", --18
	"arcGUI_FB_R_Lip_Top", --19
	"arcGUI_FB_R_Lip_Bot", --20
	"arcGUI_FB_R_Lip_Corner", --21
	"arcGUI_FB_TongueB", --22
	"arcGUI_FB_TongueC", --23
	"arcGUI_FB_R_Ear", --24
	"arcGUI_FB_L_Ear" --25	
	)

--array of all arcGui objects (last item is simply there as a label on final button)
arcNameList = #( -- first dimension is pulling items defined in arcNameArray, 2nd dimension is defining parenting, 3rd dimension is defualt gui positions
	#( 
	--DIMENSION 1
	arcNameArray[1], --1 arcGUI_FB_L_Brow_Out
	arcNameArray[2], --2 arcGUI_FB_Brow_Centre
	arcNameArray[3], --3 arcGUI_FB_L_Lid_Upper
	arcNameArray[4], --4 arcGUI_FB_L_Eye
	arcNameArray[5], --5 arcGUI_FB_L_CheekBone
	arcNameArray[6],   --6 arcGUI_FB_UpperLipRoot
	arcNameArray[7], --7 arcGUI_FB_UpperLip
	arcNameArray[8], --8 arcGUI_FB_L_Lip_Corner
	arcNameArray[9], --9 arcGUI_FB_LowerLipRoot
	arcNameArray[10], --10 arcGUI_FB_LowerLip
	arcNameArray[11], --11 arcGUI_FB_Jaw
	arcNameArray[12], --12 arcGUI_FB_L_Lip_Top
	arcNameArray[13], --13 arcGUI_FB_L_Lip_Bot
	arcNameArray[14], --14 arcGUI_FB_Tongue
	arcNameArray[15], --15 arcGUI_FB_R_Lid_Upper
	arcNameArray[16], --16 arcGUI_FB_R_Eye
	arcNameArray[17], --17 arcGUI_FB_R_CheekBone
	arcNameArray[18], --18 arcGUI_FB_R_Brow_Out
	arcNameArray[19], --19 arcGUI_FB_R_Lip_Top
	arcNameArray[20], --20 arcGUI_FB_R_Lip_Bot
	arcNameArray[21], --21 arcGUI_FB_R_Lip_Corner
	arcNameArray[22], --22 arcGUI_FB_TongueB
	arcNameArray[23], --23 arcGUI_FB_TongueC
	arcNameArray[24], --24 arcGUI_FB_R_Ear
	arcNameArray[25] --25 arcGUI_FB_L_Ear	
	),
	#(	--array of parents for the nodes in the 1st dimension
	--DIMENSION 2
	headBone, --1 arcGUI_FB_L_Brow_Out
	headBone, --2 arcGUI_FB_Brow_Centre
	headBone, --3 arcGUI_FB_L_Lid_Upper
	headBone, --4 arcGUI_FB_L_Eye
	headBone, --5 arcGUI_FB_L_CheekBone
	headBone,   --6 arcGUI_FB_UpperLipRoot
	arcNameArray[6], --7 arcGUI_FB_UpperLip
	headBone, --8 arcGUI_FB_L_Lip_Corner
	arcNameArray[11], --9 arcGUI_FB_LowerLipRoot
	arcNameArray[9], --10 arcGUI_FB_LowerLip
	headBone, --11 arcGUI_FB_Jaw
	arcNameArray[7], --12 arcGUI_FB_L_Lip_Top
	arcNameArray[10], --13 arcGUI_FB_L_Lip_Bot
	arcNameArray[9], --14 arcGUI_FB_Tongue
	headBone, --15 arcGUI_FB_R_Lid_Upper
	headBone, --16 arcGUI_FB_R_Eye
	headBone, --17 arcGUI_FB_R_CheekBone
	headBone, --18 arcGUI_FB_R_Brow_Out
	arcNameArray[7], --19 arcGUI_FB_R_Lip_Top
	arcNameArray[10], --20 arcGUI_FB_R_Lip_Bot
	headBone, --21 arcGUI_FB_R_Lip_Corner
	arcNameArray[14],--24 arcGUI_FB_TongueB
	arcNameArray[22], --25 arcGUI_FB_TongueC
	headBone, --arcGUI_FB_R_Ear
	headBone --arcGUI_FB_L_Ear	
	),
	#( --array of initial transform for MALE arcGuis		
	--DIMENSION 3
	),
	#( --dynamically created array of names for items in the listbox - if gui exists then we get a stripped version of its name, otherwise we get the name with warning characters on it.
		--DIMENSION 4
	),
	#(--array of all arcGui help images
		--DIMENSION 5
		(helpImagePath+(substring arcNameArray[1] 11 50)+".jpg"),--1 arcGUI_FB_L_Brow_Out
		(helpImagePath+(substring arcNameArray[2] 11 50)+".jpg"),--2 arcGUI_FB_Brow_Centre
		(helpImagePath+(substring arcNameArray[3] 11 50)+".jpg"),--3 arcGUI_FB_L_Lid_Upper
		(helpImagePath+(substring arcNameArray[4] 11 50)+".jpg"),--4 arcGUI_FB_L_Eye
		(helpImagePath+(substring arcNameArray[5] 11 50)+".jpg"),--5 arcGUI_FB_L_CheekBone
		(helpImagePath+(substring arcNameArray[6] 11 50)+".jpg"),--6 arcGUI_FB_UpperLipRoot
		(helpImagePath+(substring arcNameArray[7] 11 50)+".jpg"),--7 arcGUI_FB_UpperLip
		(helpImagePath+(substring arcNameArray[8] 11 50)+".jpg"),--8 arcGUI_FB_L_Lip_Corner
		(helpImagePath+(substring arcNameArray[9] 11 50)+".jpg"),--9 arcGUI_FB_LowerLipRoot
		(helpImagePath+(substring arcNameArray[10] 11 50)+".jpg"),--10 arcGUI_FB_LowerLip
		(helpImagePath+(substring arcNameArray[11] 11 50)+".jpg"),--11 arcGUI_FB_Jaw
		(helpImagePath+(substring arcNameArray[12] 11 50)+".jpg"),--12 arcGUI_FB_L_Lip_Top
		(helpImagePath+(substring arcNameArray[13] 11 50)+".jpg"),--13 arcGUI_FB_L_Lip_Bot
		(helpImagePath+(substring arcNameArray[14] 11 50)+".jpg"),--14 arcGUI_FB_Tongue
		undefined,--15 arcGUI_FB_R_Lid_Upper
		undefined,--16 arcGUI_FB_R_Eye
		undefined,--17 arcGUI_FB_R_CheekBone
		undefined,--18 arcGUI_FB_R_Brow_Out
		undefined,--19 arcGUI_FB_R_Lip_Top
		undefined,--20 arcGUI_FB_R_Lip_Bot
		undefined, --21 arcGUI_FB_R_Lip_Corner
		(helpImagePath+(substring arcNameArray[22] 11 50)+".jpg"), --22 - arcGUI_FB_TongueB
		(helpImagePath+(substring arcNameArray[23] 11 50)+".jpg"), --23 - arcGUI_FB_TongueC
		(helpImagePath+(substring arcNameArray[24] 11 50)+".jpg"),--24 - arcGUI_FB_R_Ear
		(helpImagePath+(substring arcNameArray[25] 11 50)+".jpg") --2 - arcGUI_FB_L_Ear		
		),--,
		#(--dynamically created list of all arcGui objects
			--DIMENSION 6
		),
	#( --array of initial transforms for FEMALE arcGuis				
	--DIMENSION 7
".transform = (matrix3 [3.2518,1.89944,-1.3483] [-2.32518,2.7851,-1.68426] [0.138997,2.15298,3.36829] [0.00676383,-0.477951,0.794598])", --arcGUI_FB_L_Brow_Out
".transform = (matrix3 [4.75001,0,0] [0,4.11042,-2.38055] [0,2.38055,4.11042] [0,-0.46541,0.787801])", --arcGUI_FB_Brow_Centre
".transform = (matrix3 [1.04657,0.218077,0.0282374] [-0.219897,1.0379,0.134391] [0,-0.137325,1.06057] [0.0439428,-0.52109,0.812643])", --arcGUI_FB_L_Lid_Upper
".transform = (matrix3 [1.04657,0.218077,0.0282374] [-0.219897,1.0379,0.134391] [-1.49656e-007,-0.137325,1.06057] [0.0436416,-0.519602,0.812316])", --arcGUI_FB_L_Eye
".transform = (matrix3 [0.152104,1.247,0.112004] [-1.19067,0.109187,0.401333] [0.38711,-0.154139,1.19041] [0.00884598,-0.63439,0.773811])", --arcGUI_FB_L_CheekBone
".transform = (matrix3 [3.26245,1.55566e-006,1.08896e-005] [-1.46335e-006,3.26233,-0.0276375] [-1.09024e-005,0.0276375,3.26233] [2.66726e-007,-0.516524,0.744196])", --arcGUI_FB_UpperLipRoot
".transform = (matrix3 [0.75,0,0] [0,0.00454056,0.749986] [0,-0.749986,0.00454056] [0,-0.630908,0.736953])", --arcGUI_FB_UpperLip
".transform = (matrix3 [1.33722,-0.123649,0.120217] [0.171573,1.04887,-0.829671] [-0.0174322,0.838154,1.05599] [0.0622452,-0.498598,0.684617])", --arcGUI_FB_L_Lip_Corner
".transform = (matrix3 [2.68345,0,0] [0,2.68345,0] [0,0,2.68345] [0,-0.492406,0.688693])", --arcGUI_FB_LowerLipRoot
".transform = (matrix3 [0.5,0,0] [0,0.481605,-0.134376] [0,0.134376,0.481605] [0,-0.596636,0.684619])", --arcGUI_FB_LowerLip
".transform = (matrix3 [5.04746,0,0] [0,5.03231,0.390719] [0,-0.390719,5.03231] [0,-0.484574,0.686525])", --arcGUI_FB_Jaw
".transform = (matrix3 [-1.87293,1.60815,0.383842] [-1.50016,-1.8968,0.626922] [0.694982,0.239509,2.38768] [0.0177594,-0.625085,0.708507])", --arcGUI_FB_L_Lip_Top
".transform = (matrix3 [2.1792,-0.55983,-0.226304] [0.558696,2.19085,-0.03973] [0.229088,-0.0176249,2.24961] [0.0630646,-0.536511,0.694131])", --arcGUI_FB_L_Lip_Bot
".transform = (matrix3 [1.7181,0,0] [0,1.70574,0.205694] [0,-0.205694,1.70574] [0,-0.487573,0.698443])", --arcGUI_FB_TongueA
undefined,
undefined,
undefined,
undefined,
undefined,
undefined,
undefined,
".transform = (matrix3 [1.23014,0,0] [0,1.22623,-0.0979285] [0,0.0979285,1.22623] [0,-0.524509,0.694695])", --arcGUI_FB_TongueB
".transform = (matrix3 [1.55148,0,0] [0,1.51324,0.342336] [0,-0.342336,1.51324] [0,-0.550954,0.696559])", --arcGUI_FB_TongueC
undefined,
".transform = (matrix3 [0.529083,1.42792,-0.721416] [-0.136169,0.797282,1.47821] [1.594,-0.405847,0.365731] [0.0776867,-0.419005,0.849746])" --arcGUI_FB_L_Ear
-- 	undefined --"Gui's made"--26
	)
)
	
-- arcNameListCopy = deepcopy arcNameList
-- this is an array of all the 'right' guis. This needs to match with the 'left' list as they are paired together in order to mirror transforms	 
rightGuis = #( 
	arcNameList[1][15], --15 arcGUI_FB_R_Lid_Upper 
	arcNameList[1][16], --16 arcGUI_FB_R_Eye 
	arcNameList[1][17], --17 arcGUI_FB_R_CheekBone 
	arcNameList[1][18], --18 arcGUI_FB_R_Brow_Out 
	arcNameList[1][19], --19 arcGUI_FB_R_Lip_Top 
	arcNameList[1][20], --20 arcGUI_FB_R_Lip_Bot 
	arcNameList[1][21], --21 arcGUI_FB_R_Lip_Corner 
	arcNameList[1][24] --arcGUI_FB_R_Ear
	)	

--list of all arcguis for left side
leftGuis = #( 
	arcNameList[1][3], --3 arcGUI_FB_L_Lid_Upper 
	arcNameList[1][4], --4 arcGUI_FB_L_Eye 
	arcNameList[1][5], --5 arcGUI_FB_L_CheekBone 
	arcNameList[1][1], --1 arcGUI_FB_L_Brow_Out 
	arcNameList[1][12], --12 arcGUI_FB_L_Lip_Top 
	arcNameList[1][13], --13 arcGUI_FB_L_Lip_Bot 
	arcNameList[1][8], --8 arcGUI_FB_L_Lip_Corner
	arcNameList[1][25] --arcGUI_FB_L_Ear
	)	
	
arcGuiParentingArray = #( --2d array used for physical objects in the scene and their parenting for arcGuis
		#(),
		#()
		)	

--this is the array of all faceBone names. This has to be declared seperately from the faceBoneList as faceBoneList is semi-dynamic 
--and needs this to evaluate its 2nd dimnension. 	
faceBoneNameList = #( --THIS SHOULD NEVER BE REFERRED TO IN SCRIPT. WE SHOULD ONLY REFERENCE THESE NAMES VIA faceBoneList.
	"FB_L_Brow_Out", --1
"FB_Brow_Centre", --2
"FB_L_Lid_Upper", --3
"FB_L_Eye", --4
"FB_L_CheekBone", --5
"FB_UpperLipRoot", --6
"FB_UpperLip", --7
"FB_L_Lip_Corner", --8
"FB_LowerLipRoot", --9
"FB_LowerLip", --10
"FB_Jaw", --11
"FB_L_Lip_Top", --12
"FB_L_Lip_Bot", --13
"FB_TongueA", --14
"FB_R_Lid_Upper", --15
"FB_R_Eye", --16
"FB_R_CheekBone", --17
"FB_R_Brow_Out", --18
"FB_R_Lip_Top", --19
"FB_R_Lip_Bot", --20
"FB_R_Lip_Corner", --21
"FB_TongueB", --22
"FB_TongueC", --23
"FB_R_Ear", --24
"FB_L_Ear" --25
	)
	
faceBoneList = #(
	#(--DIMENSION 1
		faceBoneNameList[1], --1  = FB_L_Brow_Out
	faceBoneNameList[2], --2  = FB_Brow_Centre
	faceBoneNameList[3], --3  = FB_L_Lid_Upper
	faceBoneNameList[4], --4  = FB_L_Eye
	faceBoneNameList[5], --5  = FB_L_CheekBone
	faceBoneNameList[6],   --6  = FB_UpperLipRoot
	faceBoneNameList[7], --7  = FB_UpperLip
	faceBoneNameList[8], --8  = FB_L_Lip_Corner
	faceBoneNameList[9], --9  = FB_LowerLipRoot
	faceBoneNameList[10], --10  = FB_LowerLip
	faceBoneNameList[11], --11  = FB_Jaw
	faceBoneNameList[12], --12  = FB_L_Lip_Top
	faceBoneNameList[13], --13  = FB_L_Lip_Bot
	faceBoneNameList[14], --14  = FB_Tongue 
	faceBoneNameList[15], --15  = FB_R_Lid_Upper
	faceBoneNameList[16], --16  = FB_R_Eye
	faceBoneNameList[17], --17  = FB_R_CheekBone
	faceBoneNameList[18], --18  = FB_R_Brow_Out
	faceBoneNameList[19], --19  = FB_R_Lip_Top
	faceBoneNameList[20], --20  = FB_R_Lip_Bot
	faceBoneNameList[21], --21  = FB_R_Lip_Corner
	faceBoneNameList[22], --22 = FB_TongueB
	faceBoneNameList[23], --23 = FB_TongueC
	faceBoneNameList[24], --24 = FB_R_Ear_Root
	faceBoneNameList[25] --25 = FB_L_Ear_Root
	),
	#(--dynamically populated array of actual bones we want to parent, generated via querying headRoot node and bones udp tag
		--DIMENSION 2
		),

	#(		--array of parents for the nodes in the 1st dimension. "headRootBone" is declared as a string as this is dependant on the number of them we have in the scene.
		--this array gets re-populated during faceRigCreation
		--DIMENSION 3
		"headRootBone", --1 arcGUI_FB_L_Brow_Out
	"headRootBone", --2 arcGUI_FB_Brow_Centre
	"headRootBone", --3 arcGUI_FB_L_Lid_Upper
	"headRootBone", --4 arcGUI_FB_L_Eye
	"headRootBone", --5 arcGUI_FB_L_CheekBone
	"headRootBone",   --6 arcGUI_FB_UpperLipRoot
	faceBoneNameList[6], --7 arcGUI_FB_UpperLip *****************
	"headRootBone", --8 arcGUI_FB_L_Lip_Corner
	faceBoneNameList[11], --9 arcGUI_FB_LowerLipRoot ************
	faceBoneNameList[9], --10 arcGUI_FB_LowerLip ************
	"headRootBone", --11 arcGUI_FB_Jaw
	faceBoneNameList[7], --12 arcGUI_FB_L_Lip_Top ********
	faceBoneNameList[10], --13 arcGUI_FB_L_Lip_Bot ***************
	"headRootBone", --14 arcGUI_FB_Tongue ***************
	"headRootBone", --15 arcGUI_FB_R_Lid_Upper
	"headRootBone", --16 arcGUI_FB_R_Eye
	"headRootBone", --17 arcGUI_FB_R_CheekBone
	"headRootBone", --18 arcGUI_FB_R_Brow_Out
	faceBoneNameList[7], --19 arcGUI_FB_R_Lip_Top ************
	faceBoneNameList[10], --20 arcGUI_FB_R_Lip_Bot *************
	"headRootBone", --21 arcGUI_FB_R_Lip_Corner
	faceBoneNameList[14], --22 tongueB
	faceBoneNameList[22], --23 tongueC
	"headRootBone", --24 arcGUI_FB_R_Ear_Root
	"headRootBone" --25 arcGUI_FB_L_Ear_Root	
	)		
)

customTongueArray = #( --used for making custom tongue joysticks
	faceBoneNameList[14],
	faceBoneNameList[22],
	faceBoneNameList[23]
	)

-- jsfb_faceBone = bone driven by this joystick
--jsfb_Type - 1 = vert 2 = sq
--jsfb_Square - 1 = vert 2 = sq
-- jsfb_fbAxisTX = bone trsans axis driven by X trans of joystick
-- jsfb_fbAxisTY = bone trans axis driven by Y trans of joystick
-- jsfb_fbAxisTZ:undefined jsfb_fbAxisRX = bone rot axis driven by X trans of joystick
-- jsfb_fbAxisRY = bone rot axis driven by Y trans of joystick
struct jsfb_Settings (jsfb_faceBone, jsfb_Type, jsfb_fbAxisTX, jsfb_fbAxisTY, jsfb_fbAxisTZ, jsfb_fbAxisRX, jsfb_fbAxisRY,jsfb_fbAxisSX, jsfb_fbAxisSY, jsfb_Square)


multiJS_list = #( --multi dimensional array storing pointers to which face bones are used by which joystick. THESE ARE INDEXED TO MATCH THE FACIALJOYSTICKS STRUCT BELOW
-- **********************************************************************************************************************************************************************************************************************
-- **********************************************************************************************************************************************************************************************************************
-- I SHOULD TURN THIS INTO A STRUCT THEN I CAN DEFINE ON A JOYSTICK AND BONE BASIS IF WE WANT TO DO TRANS AND ROTATION.
-- **********************************************************************************************************************************************************************************************************************
-- **********************************************************************************************************************************************************************************************************************

	--this gets used by joystickMultipliers.ms
		#	( --array of bones used by the R_Blink joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][15] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:undefined 	jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:1), -- r_lid_Upper
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][17] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:undefined 	jsfb_fbAxisRY:"X"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:1) --r_cheekbone
		),
	#( --array of bones used by the L_Blink joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][3] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:undefined 	jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:1), --L_Lid_Upper
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][5] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:undefined 	jsfb_fbAxisRY:"X"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:1) --L_Cheekbone
		),
	#(), --array of bones used by lookat activator	
	#( --array of bones used by the L_Cheek joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][5] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:undefined		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:1), --l_cheekbone
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][8] jsfb_Type:2 	jsfb_fbAxisTX:undefined		jsfb_fbAxisTY:"X" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:undefined		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:1) --L_Lip_Corner
		),
	#( --array of bones used by the R_Cheek joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][17] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:undefined		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:1), -- R_Cheekbone
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][21] jsfb_Type:2 	jsfb_fbAxisTX:undefined		jsfb_fbAxisTY:"X" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:undefined		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:1) -- R_Lip_Corner
		),	
		#( --array of bones used by the UpperLip_Curl joystick
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][6] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:1), -- UpperlipRoot **
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][7] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:1), -- Upperlip ***
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][21] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:1), -- R_Lip_Corner ***
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][8] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:1), -- L_Lip_Corner ***
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][12] jsfb_Type:1 	jsfb_fbAxisTX:"Z"			 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:1), -- L_Lip_Top 
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][19] jsfb_Type:1 	jsfb_fbAxisTX:"Z"			 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:1) -- R_Lip_Top
		),
	#( --array of bones used by the LowerLip_Curl joystick
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][9] jsfb_Type:1 		jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:1), -- LowerLipRoot
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][10] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:1), -- Lowerlip
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][21] jsfb_Type:1 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:1), -- R_Lip_Corner
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][8] jsfb_Type:1 		jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:1), -- L_Lip_Corner
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][13] jsfb_Type:1 	jsfb_fbAxisTX:"Z" 			jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:1), -- L_Lip_Bot
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][20] jsfb_Type:1 	jsfb_fbAxisTX:"Z"			 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:1) -- R_Lip_Bot
		
		),

	#(  --array of bones used by the C_Brow joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][2] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- Brow_Centre
		),
	#( --array of bones used by the R_Brow joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][2] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- Brow_Centre
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][18] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- R_Brow_Out
		),
	#	( --array of bones used by the L_Brow joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][2] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- Brow_Centre
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][1] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- L_Brow_Out
		),	
	#( --array of bones used by the R_Eye joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][16] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- R_Eye
		),
	#( --array of bones used by the L_Eye joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][4] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- L_Eye ***
		),
	#( --array of bones used by the R_Mouth joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][21] jsfb_Type:2 	jsfb_fbAxisTX:"X" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- R_Lip_Corner
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][6] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- UpperLipRoot
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][7] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- Upperlip
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][19] jsfb_Type:2 	jsfb_fbAxisTX:"Z" 			jsfb_fbAxisTY:"X" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- R_Lip_Top
   		(jsfb_Settings jsfb_faceBone:faceBoneList[1][20] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- R_Lip_Bot
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][17] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined		jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) --R_Cheekbone
		),
	#( --array of bones used by the Mouth joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][6] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- UpperLipRoot
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][7] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- Upperlip
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][21] jsfb_Type:2 	jsfb_fbAxisTX:"X" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- R_Lip_Corner
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][8] jsfb_Type:2 	jsfb_fbAxisTX:"X" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- L_Lip_Corner
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][13] jsfb_Type:2 	jsfb_fbAxisTX:"X" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- L_Lip_Bot
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][20] jsfb_Type:2 	jsfb_fbAxisTX:"X" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- R_Lip_Bot 
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][12] jsfb_Type:2 	jsfb_fbAxisTX:"Z" 			jsfb_fbAxisTY:"X" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- L_Lip_Top
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][19] jsfb_Type:2 	jsfb_fbAxisTX:"Z" 			jsfb_fbAxisTY:"X" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- R_Lip_Top
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][17] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined		jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"X" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), --R_Cheekbone
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][5] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined		jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"X" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- L_Cheekbone
		),		
	#( --array of bones used by the L_Mouth joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][8] jsfb_Type:2 	jsfb_fbAxisTX:"X" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- L_Lip_Corner
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][6] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- UpperLipRoot
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][7] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- Upperlip
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][12] jsfb_Type:2 	jsfb_fbAxisTX:"Z" 			jsfb_fbAxisTY:"X" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- L_Lip_Top
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][13] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- L_Lip_Bot
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][5] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined		jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- L_Cheekbone
		),
	#( --array of bones used by the UpperLip joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][6] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- UpperLipRoot
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][7] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- Upperlip
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][21] jsfb_Type:2 	jsfb_fbAxisTX:"Z" 			jsfb_fbAxisTY:"X" 			jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- R_Lip_Corner
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][8] jsfb_Type:2 	jsfb_fbAxisTX:"Z" 			jsfb_fbAxisTY:"X" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- L_Lip_Corner
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][12] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- L_Lip_Top
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][19] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- R_Lip_Top
		),
	#( --array of bones used by the LowerLip joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][9] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- LowerLipRoot
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][10] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- LowerLip
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][21] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- R_Lip_Corner
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][8] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- L_Lip_Corner
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][13] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- L_Lip_Bot
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][20] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- R_Lip_Bot ***
		),
	#( --array of bones used by the Jaw joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][11] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- Jaw
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][6] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- UpperLipRoot
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][7] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- Upperlip
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][9] jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- LowerLipRoot
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][10] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"X" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- LowerLip
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][21] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"X" 			jsfb_fbAxisTZ:undefined 	jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- R_Lip_Corner
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][8] jsfb_Type:2 	jsfb_fbAxisTX:"Y" 			jsfb_fbAxisTY:"X" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- L_Lip_Corner
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][12] jsfb_Type:2 	jsfb_fbAxisTX:"X" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Y" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- L_Lip_Top
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][13] jsfb_Type:2 	jsfb_fbAxisTX:"X" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- L_Lip_Bot
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][19] jsfb_Type:2 	jsfb_fbAxisTX:"X" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Y" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2), -- R_Lip_Top
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][20] jsfb_Type:2 	jsfb_fbAxisTX:"X" 			jsfb_fbAxisTY:"Z" 			jsfb_fbAxisTZ:undefined		jsfb_fbAxisRX:"Z" 		jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- R_Lip_Bot ***
		),
	#( --array of bones used by the Tongue joystick
-- 		(jsfb_Settings jsfb_faceBone:faceBoneList[1][14] jsfb_Type:2 	jsfb_fbAxisTX:undefined	jsfb_fbAxisTY:undefined	jsfb_fbAxisTZ:undefined	jsfb_fbAxisRX:"Z" 	jsfb_fbAxisRY:"Y" 	jsfb_Square:2) -- Tongue
		),
	#(--array of stuff for L Ear js
		
		)	, 
	#(--array of stuff for R Ear js
		), 		
	#(--array of stuff for TongueB js
		), 
	#(--array of stuff for Tongue C js
		) 		
)


----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------


-- jsName = name of joystick
-- jsShape -  1 = vertical 2 = square
--jsType - 1 = trans 2 = nonetrans
--jsPos = position of joysticks 'Rect' object
struct js_Params ( jsName, jsShape, jsType, jsPos )
	
facialJoysticks = #(--array of all joystic strcuts 
	(js_Params jsName:"CTRL_R_Blink" jsShape:1 jsType:2 jsPos:[-5.0,-4.5,0]),
 	(js_Params jsName:"CTRL_L_Blink" jsShape:1 jsType:2 jsPos:[5.0,-4.5,0]),
	(js_Params jsName:"CTRL_LookAT_Activator" jsShape:1 jsType:2 jsPos:[0,-4.5,0]),
	(js_Params jsName:"CTRL_L_Cheek" jsShape:1 jsType:2 jsPos:[5,-7.5,0]),
	(js_Params jsName:"CTRL_R_Cheek" jsShape:1 jsType:2 jsPos:[-5,-7.5,0]),
	(js_Params jsName:"CTRL_UpperLip_Curl" jsShape:1 jsType:2 jsPos:[2.5,-7.5,0]),
	(js_Params jsName:"CTRL_LowerLip_Curl" jsShape:1 jsType:2 jsPos:[2.5,-13.5,0]),
	
	(js_Params jsName:"CTRL_C_Brow" jsShape:2 jsType:2 jsPos:[0,-1.7,0]),
	(js_Params jsName:"CTRL_R_Brow" jsShape:2 jsType:2 jsPos:[-3.0,-1.7,0]),
	(js_Params jsName:"CTRL_L_Brow" jsShape:2 jsType:2 jsPos:[3.0,-1.7,0]),
	(js_Params jsName:"CTRL_R_Eye" jsShape:2 jsType:2 jsPos:[-3,-4.5,0]),
	(js_Params jsName:"CTRL_L_Eye" jsShape:2 jsType:2 jsPos:[3,-4.5,0]),
	(js_Params jsName:"CTRL_R_Mouth" jsShape:2 jsType:2 jsPos:[-3,-10.5,0]),
	(js_Params jsName:"CTRL_Mouth" jsShape:2 jsType:2 jsPos:[0,-10.5,0]),
	(js_Params jsName:"CTRL_L_Mouth" jsShape:2 jsType:2 jsPos:[3,-10.5,0]),
	(js_Params jsName:"CTRL_UpperLip" jsShape:2 jsType:2 jsPos:[0,-7.5,0]),
	(js_Params jsName:"CTRL_LowerLip" jsShape:2 jsType:2 jsPos:[0,-13.5,0]),
	(js_Params jsName:"CTRL_Jaw" jsShape:2 jsType:2 jsPos:[0,-16.5,0]),
	(js_Params jsName:"CTRL_TongueA" jsShape:2 jsType:2 jsPos:[-3,-13.5,0]),
	(js_Params jsName:"CTRL_L_Ear" jsShape:2 jsType:2 jsPos:[-3,-13.5,0]),
	(js_Params jsName:"CTRL_R_Ear" jsShape:2 jsType:2 jsPos:[-3,-13.5,0]),
	(js_Params jsName:"CTRL_TongueB" jsShape:2 jsType:2 jsPos:[-3,-13.5,0]),
	(js_Params jsName:"CTRL_TongueC" jsShape:2 jsType:2 jsPos:[-3,-13.5,0])	
)

controlNames = #( --gets populated dynamically by the main user interface 'facial' window when the multipliers button is pressed.
	undefined,
	
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,
	undefined,	
	undefined
)

lyrNames = #( --list of layers
		"FacialUI_Controls", 
		"FacialUI_Visuals"
		)	
		
	runCleanUpFN = true

nodesToKill = #( --joystick nodes to remove
"Tongue",
"Tongue_In_Out",
"LookAT_Activator"	
)	