joystickNames = #(
"CTRL_R_Blink",
"CTRL_L_Blink",
"CTRL_L_Cheek",
"CTRL_R_Cheek",
"CTRL_UpperLip_Curl",
"CTRL_LowerLip_Curl",
"CTRL_Tongue_In_Out",
"CTRL_C_Brow",
"CTRL_R_Brow",
"CTRL_L_Brow",
"CTRL_R_Eye",
"CTRL_L_Eye",
"CTRL_R_Mouth",
"CTRL_Mouth",
"CTRL_L_Mouth",
"CTRL_UpperLip",
"CTRL_LowerLip",
"CTRL_Jaw",
"CTRL_Tongue",
"CTRL_MouthPinch"	
	)

storedAttrData = #(
#(), --joystick
#(), --rollout
#(), --attrname i.e. LowerLipRoot_YPos_R
#() --value
)

fn loadAttrData = 
(
	input_name = undefined
	
	if input_name == undefined do
	(
		input_name = getOpenFileName caption:"Facial attr file" types:"AttrData (*.atr)|*.atr|All Files (*.*)|*.*|"
	)
	
	if input_name != undefined then
	(
		filein input_name
		
		print ("Loaded "+input_name)
	)
)


fn outputAttrData storedAttrData = 
(
	output_name = getSaveFileName caption:"Facial attr file" types:"AttrData (*.atr)|*.atr|All Files (*.*)|*.*|"
		
	if output_name != undefined then 
	(
		output_file = createfile output_name		
		
		--now get the rrig revision number
		
		UINode = getNodeByName "Ambient_UI"
	
		revNo = "undefined"
		
		if UINode != undefined do 
		(
			revNoStr = getUserPropBuffer UINode
			
			revNo = substring revNoStr 31 -1
			
			foundCR = findString revNo "\n"
			
			if foundCR != undefined then
			(
				print ("CR found at index "+foundCR as string)
				
				revNo = substring revNo 1 (foundCR - 1)
			)			
		)
		
		rSt1 = ("UINode = getNodeByName "+"\""+"Ambient_UI"+"\""+"\n")			
		rSt2 = ("thisRevNo = "+"\""+"undefined"+"\""+"\n")
		rSt3 = ("if UINode != undefined do "+"\n")
		rSt4 = ("("+"\n")
		rSt5 = ("\t"+"revNoStr = getUserPropBuffer UINode"+"\n")
		rSt6 = ("\t"+"thisRevNo = substring revNoStr 31 -1"+"\n")
------------------------------------------------------------------------------------------------------------------------------------------------		
		rSt7 = ("\t"+"foundCR = findString thisRevNo "+"\""+"\\"+"n"+"\""+"\n")
		rSt8 = ("\t"+"if foundCR != undefined then"+"\n")
		rSt9 = ("\t"+"("+"\n")
		rSt10 = ("\t"+"\t"+"thisRevNo = substring thisRevNo 1 (foundCR - 1)"+"\n")
		rSt11 = ("\t"+")"+"\n")		
---------------------------------------------------------------------------------------------------------------------
		rSt12 = (")"+"\n")
		rSt13 = ("if "+revNo+" > (thisRevNo as integer) then"+"\n")
		rSt14 = ("("+"\n")
		rSt15 =("\t"+"print "+"\""+"this rig was generated prior to the rig the attributes were saved from. Regen Required."+"\""+"\n")
		rSt16 = ("\t"+"messagebox ("+"\""+"This rig was generated prior to the rig the attributes were saved from. Regen required."+"\""+") beep:true"+"\n")	
		rSt17 = (")"+"\n")
		rSt18 = ("else"+"\n")
		rSt19 = ("("+"\n")
			
		outPutStr = (rSt1+rsT2+rst3+rst4+rst5+rst6+rst7+rst8+rst9+rst10+rst11+rst12+rst13+rst14+rst15+rst16+rst17+rst18+rst19)		
		
		format outputStr to:output_file
		
		--format ("--RevNo: "+(revNo as string)) to:output_file
		
		for i = 1 to storedAttrData[1].count do
		(
-- 			if $CTRL_R_Blink != undefined do ($CTRL_R_Blink.modifiers[#Attribute_Holder].R_Lid_Upper.R_Lid_Upper_YPos_R = 5.9)
			
			objName = storedAttrData[1][i].name
			rollName = storedAttrData[2][i]
			attrName = storedAttrData[3][i]
			attrVal = storedAttrData[4][i]
			
			modif = "modifiers[#Attribute_Holder]"
			
			outputStr = ("\t"+"if $"+objName+" != undefined do ($"+objName+"."+modif+"."+rollName+"."+attrName+" = "+attrVal+")")
			
			format ((outputStr)+"\n") to:output_file
		)
		
		format (("\t"+"print "+"\""+"Attributes loading complete!"+"\"")+"\n") to:output_file
		format (")") to:output_file
		
		flush output_file --hack to try and release the file
		close output_file
		free output_file --hack to try and release the file

		
		print ("Data saved to "+(output_name as string))
			
		if ((suckTheCattOffGui != undefined) and (suckTheCattOffGui.isDisplayed)) do
		(destroyDialog suckTheCattOffGui)			
	)
)

fn getAttrDataFromPBlock objN = --objN = an object pointer. This function will search through the custom attribute on objN and store out attribute data
(
	
	
	obj = objN.modifiers[#Attribute_Holder]

	objectDefin = 	(custAttributes.getDefs obj)
	for i = 2 to objectDefin.count do --never do the first as thats the one for resetting guis		
	(
		objDef = objectDefin[i]

		pbArray = custAttributes.getPBlockDefs objdef

		for a = 1 to pbArray.count do 
		(
			itms = pbArray[a]

			keywordParams = itms[4]

			for y = 5 to itms.Count do
			(

				append storedAttrData[1] (objN) --pointer to object i.e. $Editable_Spline:CTRL_R_Blink @ [0.150000,0.002916,1.721670]
				append storedAttrData[2] objDef.name --Name off rollout i.e. R_Lid_Upper
				append storedAttrData[3] itms[y][1] --name of attribute inside the rollout i,e R_Lid_Upper_Ypos_R
				
				execStr = ("$"+objN.name+".modifiers[#Attribute_Holder]."+objDef.name+"."+itms[y][1])
				
				attrValue = execute execStr
				append storedAttrData[4] (attrValue as string)
				
			)
		)
	)
)


fn suckTheCattOff = 
(
	for js in joystickNames do 
	(
		objN = getNodeByName js

		getAttrDataFromPBlock objN
	)
	
	outputAttrData storedAttrData
)


-----------------------------------------------------------------------

if ((suckTheCattOffGui != undefined) and (suckTheCattOffGui.isDisplayed)) do
	(destroyDialog suckTheCattOffGui)

rollout suckTheCattOffGui "Suck The c.Att Off"
(
	button btnSaveAttr "Extract Data" width: 150 tooltip:"Extract Custom Attribute settings from Ambient Joysticks to a file"
	button btnLoadAttr "Insert Data" width:150 tooltip:"Apply previously stored Attribute Data from a file to Ambient Joysticks"

	on btnSaveAttr pressed do
	(
		suckTheCattOff()
	)

	on btnLoadAttr pressed do 
	(
		loadAttrData()
	)
)

CreateDialog suckTheCattOffGui --width:125 --pos:[1450, 100] 
