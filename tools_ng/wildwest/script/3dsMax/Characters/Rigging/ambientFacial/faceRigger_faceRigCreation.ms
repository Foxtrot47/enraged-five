-- faceRigCreation.ms
-- Matt Rennie
--May 2010

--=========================================================================================================================

--Script containing all functions required for facialBone and facial Joystick creation

--Version 0.1

--=========================================================================================================================


fn buildNewBoneMR bname btag source dest bonewidth bfinsize bcolour parentbone xrot alignto  = 
(	
	sourcePos = source.position
-- 	destPos = dest.position
	destPos = dest
	zAxis = undefined
	
	
	spineBackNode = undefined
	spineFrontNode = undefined 
	
	doTest = false
	
-- 	for i = 1 to spineBackArray.count do
-- 	(
-- 		if (bonePrefix+spineBackArray[i]) == bName do (spineBackNode = true)
-- 	)
-- 	for i = 1 to spineForwardArray.count do
-- 	(
-- 		if (bonePrefix+spineForwardArray[i]) == bName do (spineFrontNode = true)
-- 	)
	
	
-- 	if ((substring bName 5 3) != "_L_") then 
	if ((substring bName 3 3) != "_L_") then 
	(
-- 		if ((substring bName 5 3) != "_R_") then
		if ((substring bName 3 3) != "_R_") then
		(
			zAxis = [0,0,0]
-- 			zAxis = [0,0,1]
		)
		else
		(
			doTest = true	
		)
	)
	else
	(
		doTest = true		

	)

	if doTest == true do 
	(
		--WE'LL DO A TEST HERE ON THE AXES TO SEE IF A JOINT IS MORE VERTICAL AND IF SO WE'LL SET THE ZAXIS TO [0,-1,0]
			sPos = #()
			append sPos source.position[1]
			append sPos source.position[2]
			append sPos source.position[3]
			dPos = #()
			append dPos dest[1]
			append dPos dest[2]
			append dPos dest[3]		

			--now we'll make all values positive
			for s = 1 to sPos.count do 
			(
				if sPos[s] < 0 do (sPos[s] = sPos[s] * -1)
			)
			for d = 1 to dPos.count do 
			(
				if dPos[d] < 0 do (dPos[d] = dPos[d] * -1)
			)
			
			xDist = undefined 
			yDist = undefined 
			zDist = undefined 
			
			-- now we'll get the distances	
			if sPos[1] < dPos[1] then 
			(
				xDist = dPos[1] - sPos[1]
			)
			else
			(
				xDist = sPos[1] - dPos[1]
			)
			if sPos[2] < dPos[2] then 
			(
				yDist = dPos[2] - sPos[2]
			)
			else
			(
				yDist = sPos[2] - dPos[2]
			)
			if sPos[3] < dPos[3] then 
			(
				zDist = dPos[3] - sPos[3]
			)
			else
			(
				zDist = sPos[3] - dPos[3]
			)
			
			winString = undefined
			
			--if z wins we need to use [0,-1,0] as its vertical
			--if Y wins we need to use [0,0,-1]
			
			if xDist > yDist then --x bigger than y X WINS
			(
				if xDist > zDist then --x bigger than y and z X WINS
				(
					zAxis = [0,0,1]
					winString = (bName+" --x bigger than y and z - X WINS")
				)		
				else --x bigger than y but less than z Z WINS
				(
					zAxis = [0,-1,0]
					winString = (bName+" --x bigger than y but less than z - Z WINS")
				)
			) 
			else -- x less than y
			(
				if YDist >ZDist then --y bigger than z and X - Y WINS
				(
					zAxis = [0,0,1]
					winString = (bName+" --y bigger than z and X - Y WINS")
				)
				else --y bigger than x but less than z Z WINS
				(
					zAxis = [0,-1,0]
					winString = (bName+" --y bigger than x but less than z - Z WINS")
				)
			)
			
-- 			zAxis = [0,0,1] --temp hack
-- 		print winString
	)

	debugprint ("sourcePos = "+(sourcePos as string))
	debugprint ("destPos = "+(destPos as string))
	debugprint ("zAxis = "+(zAxis as string))
	
	newBone = BoneSys.createBone sourcePos destPos zAxis
	newBoneScale = newBone.scale as string
	if newBoneScale != "[1,1,1]" do
	(
		print ("***   WARNING! ***** "+"Invalid Scale: " + bname + " Scale " + newBone.scale as string)
		
		newbone.scale = [1,1,1]

		print ("*** Resetting to [1,1,1]. "+bName+".scale == "+(newBone.scale as string))		
		print "============================================================================"
	)
	
	--is the boneenable thing here necessary anymore?
	newBone.boneEnable = false
	
	newBone.height = newBone.length / 6
	newbone.width = newBone.length / 6
	
	newBone.taper = 90.0
	newBone.parent = undefined
	newBone.sidefins = on
	newBone.sidefinssize = newBone.length / 20
	bcolour = blue
	if (substring bname 4 1) == "L" do 
	(
		bcolour = green
	)
	if (substring bname 4 1) == "R" do 
	(
		bcolour = red
	)	
	newBone.wirecolor = bcolour
	newBone.name = bname
	
	select newbone
	newBone.boxmode = off
	
	
	--NOW MAKE THE BONE A BONE TYPE SO IT CAN BE FILTERED VIA THE BONE SELECTION TYPE
	newBone.boneEnable = true
	newBone.boneScaleType=#none
	
	newbone
)

fn deleteArcGuis = 
(
	local existingGuis = #()
		for arcGui in objects do 
		(
			--debugPrint ("arcGUI == " +(substring arcGui.name 1 7) )
			if (substring arcGui.name 1 7) == "arcGUI_" do
			(
				appendIfUnique existingGuis arcGui
			)
			
			if (substring arcGui.name 3 7) == "arcGUI_" do
			(
				appendIfUnique existingGuis arcGui
			)
		)
		
		for i = existingGuis.count to 1 by - 1 do --had to loop backwards thropugh the array otherwise it crashes!
		(
				debugPrint ("deleting "+existingGuis[i].name)
				delete existingGuis[i]
		)
		existingGuis = undefined
)

fn initialControllers = --ASSIGN NAMED CONTROLLERS TO THE ROTATION LIST
(
	for i = 1 to controlNames.count do
	(
		if controlNames[1] == undefined do
		(
			for i = 1 to controlNames.count do --this repopulates the controlnames array if joysticks are already present
			(
-- 				jsControl = (getNodeByName facialJoysticks[i].jsName)
				jsControl = (facialJoysticks[i].jsName)
				if jsControl != undefined then
				(
					controlNames[i] = jsControl
					debugPrint ("adding "+facialJoysticks[i].jsName+" to controlNames with index of "+(i as string))
				)
				else
				(
					Print ("Failed adding "+facialJoysticks[i].jsName+" to controlNames with index of "+(i as string)+" as this could not be found")	
				)
			)
		)
	)
	
	clearSelection()
	for fb in faceBoneList[2] do --freeze rotation and transforms of all facebones
	(
		selectmore fb
	)

	freezeTransform()
	clearSelection()
	debugprint "============================================================================================="
	debugprint "                                                            ADDING INITIAL CONTROLLERS                                                               "
	debugprint "============================================================================================="
	
	if ((progBar != undefined) and (progBar.isDisplayed)) do
		(destroyDialog progBar)
		CreateDialog progBar width:300 Height:30
		progBar.prog.color = [10,10,210] --blue
	
	for f = 1 to faceBoneList[2].count do
	(
		for i = 1 to controlNames.count do 
		(
			controllerName = (substring (controlNames[i] ) 6 100)
				faceBoneList[2][f].rotation.controller.Available.controller =  Euler_XYZ ()
				faceBoneList[2][f].rotation.controller.setName (i+2) controllerName
			-----------------------------------------------
				faceBoneList[2][f].pos.controller.Available.controller =  Position_XYZ ()
				faceBoneList[2][f].pos.controller.setName (i+2) controllerName
			-----------------------------------------------
				faceBoneList[2][f].scale.controller.Available.controller =  ScaleXYZ ()
				faceBoneList[2][f].scale.controller.setName (i+2) controllerName			
			debugPrint (controlNames[i]+" added to "+faceBoneList[2][f].name)
		)
		debugPrint ("-----Initial controllers added to "+faceBoneList[2][F].NAME+"-----")
			progBar.prog.value = (100.*f/faceBoneList[2].count)
	)
			if ((progBar != undefined) and (progBar.isDisplayed)) do
		(destroyDialog progBar)
	debugPrint ("ALL INITIAL CONTROLLERS ADDED.")
)

fn parentFaceBones headRoot  = 
(
	--try and build selection of facebones by comparing headroot and udp tag on bone - if they match we know this is a bone we are wanting.
	
	if headRoot == undefined do 
	(
		headRoot = getNodebyName ("FaceRoot_"+geo.name)
	)
	headRootStr = (substring headRoot.name 15 3) --get the number off the headRoot
	
	for obj in objects do --dynamically populate the 2nd element with the actual bone objects
	(
		if (substring obj.name 1 3) == "FB_" do
		(		
			existingUDP = getUserPropBuffer obj
			udpStr = (substring existingUDP 6 3)
			
			obName = obj.name
			obNameLength = obName.count
			obFrom = (obNameLength - 2) 
			nameNumber = (substring obj.name obFrom 3)

			if nameNumber == headRootStr then
			(
				debugPrint  ("udp on "+obj.name+" matches "+headRoot.name)
				for i = 1 to faceBoneList[1].count do
				(
					objName = obj.name 
					objNameLength = (objName.count - 4 )
					objName = (substring objName 1 objNameLength)

-- 					print ("\t"+"objName:"+objName+". faceBoneList[1]["+(i as string)+"]: "+faceBoneList[1][i])
					if objName == faceBoneList[1][i] do
					(
						debugPrint ("Adding "+obj.name+" to faceBoneList[2]["+(i as string)+"]")
						faceBoneList[2][i] = obj
					)
				)
			)
			else
			(
				Print  ("udp on "+obj.name+"which was "+nameNumber+" did not match "+headRootStr+" on "+headRoot.name)
			)		
		)
	)

	--NEED TO FIND A MUCH MORE ELEGANT DATA DRIVEN METHOD FOR THIS
	
		for i = 1 to faceBoneList[3].count do --repopulate the faceBoneList[3] dynamically with actual bone names rather than strings
		(
			if headRoot == undefined do 
			(
				headRoot = getNodebyName ("FaceRoot_"+geo.name)
			)
			headRootStr = (substring headRoot.name 15 3)
		
			if faceBoneList[3][i] == "headRootBone" then
			(
				faceBoneList[3][i] = headRoot
			)
			else
			(
				debugPrint ("str A = "+faceBoneList[3][i])
				debugPrint ("str B = "+"_")
				debugPrint ("str C = "+headRootStr)
				thisBone = (getNodeByName (faceBoneList[3][i]+"_"+headRootStr))
				faceBoneList[3][i] = thisBone
			)
		)
		
		for i = 1 to faceBoneList[3].count do --now loop through faceBoneList[3] and use it with faceBoneList[2] to parent nodes
		(
			if faceBoneList[2][i] != undefined do 
			(
				debugprint ("faceBoneList[2]["+(i as string)+"].name = "+faceBoneList[2][i].name)
				debugprint ("faceBoneList[3]["+(i as string)+"].name = "+faceBoneList[3][i].name)
				debugPrint ("parenting "+faceBoneList[2][i].name+" to "+faceBoneList[3][i].name)
				faceBoneList[2][i].parent = faceBoneList[3][i]
			)
		)
		
	--now set the display of all children of headroot and headroot itself to 'show links'
	headRoot.showLinks = true 
	for i in faceBoneList[2] do
	(
		if i != undefined do 
		(
			i.showLinks = true 
		)
	)
	
	skullBone = getNodeByName headBone
	headRoot.parent = skullBone
		
	deleteArcGuis()
)

fn createFacialBones geo= 
(
	headRoot = getNodebyName ("FaceRoot_"+geo.name)
	
	for obj in objects do --build the 6th dimension of the arcNameList array so we have a matched list of all existing gui objects
	(
		for i = 1 to arcNameList[1].count do
		(
			if obj.name == arcNameList[1][i] do
			(
				arcNameList[6][i] = obj
			)
		)
	)
	
	--need to do a test to see if all arcGuis are present before we generate face bones
	if arcNameList[6].count != arcNameList[1].count then
	(
		messageBox "Not all gui's have been made. Ensure the list is complete and that Mirroring has been performed." title:"WARNING!" beep:true
	)
	else
	(
		--now save the guis out
		saveArcGuisLcl geo headRoot
		
		for hlp in objects do --this is a test for if we have bones existing in scene for this current geo naming
		(
			if (substring hlp.name 1 3) == "FB_" do 
			(
				geoName = geo.name
				geoNumber = (substring geo.Name (geo.Name.count - 4) 3)
				for bName = 1 to faceBoneList[1].count do
				(
					if (substring hlp.name 1 faceBoneList[1][bName].count) == faceBoneList[1][bName] do
					(
						if (subString hlp.name (hlp.name.count - 3) 3 ) == geoNumber do
						(
							--ok appendThe shit now!
							faceBoneList[2][bName] = hlp
							print ("Appending "+(hlp.name)+" to faceBoneList[2]["+(bName as string)+"] (pre-exists)")
						)
					)
				)
			)
		)

		if faceBoneList[2][1] == undefined do
		(
			
			for arcName = 1 to arcNameList[1].count do
			(
					geoSuffix = (substring (geo.name) 6 3) --example head name will be 'head_000_r' so this substring should give us the '000'
					iName = (substring (arcNameList[1][arcName]) 8 30) 

					guiObj = getNodeByName arcNameList[1][arcName]
					
					if guiObj != undefined do
					(
						pHName =  (iName+"_"+geoSuffix)
-- 						pHName =  (iName+" "+geoSuffix)
						pH = (getNodeByName pHName)
						if pH == undefined then
						(
							bname = (pHname)

							source = guiObj
							
							dest = undefined --declare for scope reasons
							for aG = 1 to jointCreationArray.count do 
							(
								dest = undefined --declare for scope reasons
								if guiObj.name == jointCreationArray[ag][1] do 
								(
									if jointCreationArray[ag][2] == "fwdVect" then
									(											
										--ok we need to build the name of this guis fwdVect
										destNodeName = ("v_"+jointCreationArray[ag][1]+"_FwdVect")
										
										destNode = getNodeByName destNodeName 
										dest = destNode.position
										if destNode == undefined do 
										(
											messagebox ("WARNING! Couldn't find: "+destNodeName)
											print ("WARNING! Couldn't find: "+destNodeName)
											break()
										)
									)
									else
									(
										--ok we need to look for the gui node 
										destNode = getNodeByName jointCreationArray[ag][2]
										dest = destNode.position											
-- messagebox ("Deriving endPos from "+destNode.name) beep:true
										if destNode == undefined do 
										(
											messagebox ("WARNING! Couldn't find: "+jointCreationArray[ag][2])
											print ("WARNING! Couldn't find: "+jointCreationArray[ag][2])
											break()
										)											
									)
							
									bonewidth = 0.01
									bfinsize = 0.01
									bcolour = red
									parentbone = pointHelper --we will need to do this better
									xrot = 90
									alignto = -1
									
									if dest == undefined then 
									(
										print ("Dest undefined for gui "+guiObj.name)
										break()
									)
									else
									(
										newBone = buildNewBoneMR bname btag source dest bonewidth bfinsize bcolour parentbone xrot alignto
										newBone.parent = guiObj.parent
										
										setUserPropbuffer newBone (geo.name)
										existingUDP = getUserPropBuffer newBone
										btag = ((existingUDP)+"\r\n"+"tag = BONETAG_"+iName)
										setUserPropbuffer newBone ((existingUDP)+"\r\n"+"tag = BONETAG_"+iName)										
										
-- 										in coordsys parent newBone.transform = in coordsys parent guiObj.transform
										
-- 										if (substring guiObj.name 13 3 )!= "Ear" then
-- 										(
											in coordsys parent newBone.rotation = in coordsys parent guiObj.rotation
											in coordsys parent newBone.position = in coordsys parent guiObj.position
												
											in coordsys local newBone.rotation.Z_Rotation = (newBone.rotation.Z_Rotation - 90)
-- 										)
-- 										else
-- 										(
-- 											--messagebox ("Skipping "+guiObj.name)
-- 											newBone.transform = guiObj.transform
-- 											in coordsys local newBone.rotation.Z_Rotation = (newBone.rotation.Z_Rotation - 90)	
-- 											newBone.scale = [1,1,1]
-- 										)
										newBone.boneEnable = true --try to force bone mode on again										
									)
								)
							)									
						)
						else
						(
		-- 					debugPrint (pHName+" already exists so skipping creation")
						)
					)
			)
			parentFaceBones headRoot 
		)
	)	
	
	--break()
	initialControllers()
)