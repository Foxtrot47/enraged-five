filein (RsConfigGetToolsRootDir() + "\\dcc\\current\\max2012\\scripts\\pipeline\\util\\p4.ms")

--****************************************************************************************************************************************


fn parentGuis headRoot = 
(
)

fn loadArcGuisLib geo =
(		
	aG_Name = getOpenFileName caption:"Choose your arcGui file" filename:(arcGuiLibraryPath)

	if ag_name != undefined then
	(
		debugPrint ("IMPORTING EXISTING ARCGUI FILE: "+aG_Name)
		mergeMaxFile aG_Name #skipDups #alwaysReparent quiet:true

		headRoot = (getNodeByName "FaceRoot_Lib")
		geoNumber = (substring geo.Name (geo.Name.count - 4) 3)
		headRoot.name = ("FaceRoot_"+geo.name)

		parentGuis headRoot
		
		--now need to add a udp tag to faceRoot
		setUserPropbuffer headRoot ("tag = facialroot")		
	)
	else
	(
		messagebox "Warning! No valid arcGui file picked." beep:true
	)
)

fn loadArcGuisLcl geo =
(		
	currentFileName = maxFileName
	currentFileName = substituteString currentFileName ".max" ""
	currentPath = maxFilePath
	aG_Name = (currentPath+currentFileName+"_"+geo.Name+"_ArcGuis.max")
	
	fileIsThere = doesFileExist aG_Name
	if fileIsThere == true then
	(
		debugPrint ("IMPORTING EXISTING ARCGUI FILE: "+aG_Name)
		mergeMaxFile aG_Name #skipDups #alwaysReparent quiet:true
		
		headRoot = getNodebyName ("FaceRoot_"+geo.name)

		parentGuis headRoot
		
		--now need to add a udp tag to faceRoot
		setUserPropbuffer headRoot ("tag = facialroot")		
	)
	else
	(
		messageBox ("Could not find "+aG_Name+" gui loading aborted") beep:true
	)
)

fn addFileToP4 filenameString = 
(
	gRsPerforce.add #(fileNameString) silent:true
)

fn editp4file filenameString = 
(
	gRsPerforce.add_or_edit #( filenameString ) silent:true queueAdd:false
)

fn saveArcGuisLib geo headRoot = 
(
	clearSelection()
	local arcGuiParentingArray = #(#(),#())
	
	headRoot = getNodebyName ("FaceRoot_"+geo.name)
	select headRoot
		headRoot.name = "FaceRoot_Lib"
	
	for a in objects do
	(
		if (substring a.name 1 7 ) == "arcGUI_" do
		(
			appendIfUnique arcGuiParentingArray[1] a
		)
		--now add in the vector nodes
		if (substring a.name 1 9 ) == "v_arcGUI_" do
		(
			appendIfUnique arcGuiParentingArray[1] a
		)		
	)
	appendIfUnique arcGuiParentingArray[1] headRoot
		
	for aIndex = 1 to arcGuiParentingArray[1].count do --this is a way to store the current parent of all gui objects so we can reparent them after save
		(
			arcGuiParentingArray[2][aIndex] = arcGuiParentingArray[1][aIndex].parent
		)
 	max unlink

	if maxFileName != "" do
	(
		for obj in arcGuiParentingArray[1] do
		(
			selectMore obj
		)
		
		Print "==WARNING!!!== File is not saved so could not save guis."
		geoName = geo.name
		currentFileName = maxFileName
		maxStr = ".max"
		repMax = ""
			currentFileName = substituteString currentFileName maxStr repMax
		currentPath = arcGuiLibraryPath 

		arcGuiName = getSaveFileName caption:"Choose your save name" filename:(arcGuiLibraryPath)
	
		if arcGuiName != undefined then
		(
			deselect $SKEL_Head
			saveNodes selection arcGuiName quiet:true
			
			--now try to add to perforce
			--addFileToP4 (arcGuiName+".max")
			editp4file (arcGuiName+".max")
		)
		else
		(
			messagebox "Warning! No valid filename given for save so skipping." beep:true
		)
	)
	
	headRoot.name = ("FaceRoot_"+geo.name) --rename headRoot back
	
	for aIndex = 1 to arcGuiParentingArray[1].count do
	(
		arcGuiParentingArray[1][aIndex].parent = arcGuiParentingArray[2][aIndex]
	)
)

fn saveArcGuisLcl geo headRoot = 
(
	clearSelection()
	local arcGuiParentingArray = #(#(),#())
	
	headRoot = getNodebyName ("FaceRoot_"+geo.name)
	select headRoot
	
-- 	for a in shapes do
-- 	(
-- 		if (substring a.name 1 7 ) == "arcGUI_" do
-- 		(
-- 			appendIfUnique arcGuiParentingArray[1] a
-- 		)
-- 	)
	for a in objects do
	(
		if (substring a.name 1 7 ) == "arcGUI_" do
		(
			appendIfUnique arcGuiParentingArray[1] a
		)
		--now add in the vector nodes
		if (substring a.name 1 9 ) == "v_arcGUI_" do
		(
			appendIfUnique arcGuiParentingArray[1] a
		)		
	)		
	appendIfUnique arcGuiParentingArray[1] headRoot
		
	for aIndex = 1 to arcGuiParentingArray[1].count do --this is a way to store the current parent of all gui objects so we can reparent them after save
		(
			arcGuiParentingArray[2][aIndex] = arcGuiParentingArray[1][aIndex].parent
		)
 	max unlink

	if maxFileName != "" do
	(
		for obj in arcGuiParentingArray[1] do
		(
			selectMore obj
		)
				
		Print "==WARNING!!!== File is not saved so could not save guis."
		geoName = geo.name
		currentFileName = maxFileName
		maxStr = ".max"
		repMax = ""
			currentFileName = substituteString currentFileName maxStr repMax
		currentPath = maxFilePath
		arcGuiName = (currentPath+currentFileName+"_"+geoName+"_ArcGuis")
	
		deselect $SKEL_Head
-- 		saveNodes selection arcGuiName quiet:true
		
		--now try to add to perforce
-- 		addFileToP4 (arcGuiName+".max")
		
		editp4file (arcGuiName+".max")
		saveNodes selection arcGuiName quiet:true
	)
	
	for aIndex = 1 to arcGuiParentingArray[1].count do
	(
		arcGuiParentingArray[1][aIndex].parent = arcGuiParentingArray[2][aIndex]
	)
)

fn createAxesLines = --creates the cross at the centre of the arcGui
(
	knot_positions = #(
		[-0.0215,0,0],
		[0.0215,0,0]
		)

	-- first you have to create a shape
	sX = splineshape()

	-- it seems backwards, but next you have to add
	-- a spline segment to the shape
	idx = addNewSpline sX

	-- now you can start adding knots to the spline.
	-- this code assumes you already have an array
	-- of point3 values for your knot positions
	for k in knot_positions do addKnot sX idx #smooth #curve k

	-- now, after updating the shape, you should
	-- see your spline in the viewports!
	updateShape sX

	-- here's how you set the renderable and thickness properties
	sX.baseobject.renderable = false
	sX.thickness = 0.001
	
	knot_positions = #(
		[0,-0.0215,0],
		[0,0.0215,0]
		)

	-- first you have to create a shape
	sY = splineshape()

	-- it seems backwards, but next you have to add
	-- a spline segment to the shape
	idx = addNewSpline sY

	-- now you can start adding knots to the spline.
	-- this code assumes you already have an array
	-- of point3 values for your knot positions
	for k in knot_positions do addKnot sY idx #smooth #curve k

	-- now, after updating the shape, you should
	-- see your spline in the viewports!
	updateShape sY

	-- here's how you set the renderable and thickness properties
	sY.baseobject.renderable = false
	sY.thickness = 0.001
	
	knot_positions = #(
		[0,0,-0.0215],
		[0,0,0.0215]
		)

	-- first you have to create a shape
	sZ = splineshape()

	-- it seems backwards, but next you have to add
	-- a spline segment to the shape
	idx = addNewSpline sZ

	-- now you can start adding knots to the spline.
	-- this code assumes you already have an array
	-- of point3 values for your knot positions
	for k in knot_positions do addKnot sZ idx #smooth #curve k

	-- now, after updating the shape, you should
	-- see your spline in the viewports!
	updateShape sZ

	-- here's how you set the renderable and thickness properties
	sZ.baseobject.renderable = false
	sZ.thickness = 0.001	
	
	addAndWeld sX sY -1.0
		updateShape sX
	addAndWeld sX sZ -1.0
		updateShape sX		
	
	sX
)

fn bboxCentre obj = 
(
	worldPoint = point pos:[0,0,0] isSelected:off name:"worldPoint"
	nodeBB = nodeGetBoundingBox obj worldPoint.transform

	delete worldPoint
	cX = ((nodeBB[1][1] + nodeBB[2][1]) / 2)
	cY = ((nodeBB[1][2] + nodeBB[2][2]) / 2)
	cZ = ((nodeBB[1][3] + nodeBB[2][3]) / 2)

	centrePos = [cX,cY,cZ]
	
	return centrePos
)

fn arcGuiCreation guiName arcGuiIndex guiRad = --function to build an arcGui object with name of arcGui to be created
(

	headBoneObject = getNodeByName headBone
	headRoot = getNodebyName ("FaceRoot_"+geo.name)
	if headRoot == undefined do
	(
		debugPrint "headRoot is being generated..." 
		
		--ok now we need to find the bounding box of the head geo so we can find the centre point
		skelBone = $SKEL_Head
		centrePos = bboxCentre geo
		
		pX = centrePos.x
		pY = skelBone.position.Y
		pZ = skelBone.position.Z
		
		cPos = [pX,pY,pZ]
		
-- 		headRoot = Point pos:headBoneObject.pos
		headRoot = Point pos:cPos
		
		hName = geo.name
		headRoot.name = ("FaceRoot_"+hName)
		
		--now need to add a udp tag to faceRoot
		setUserPropbuffer headRoot ("tag = facialroot")		
	)
	
	headRoot.rotation = headBoneObject.rotation
	headRoot.position = headBoneObject.pos
	headRoot.size = 0.01
	headRoot.cross = on
	headRoot.axistripod = on
	headRoot.centermarker = on
	headRoot.Box = on
	headRoot.constantscreensize = off
	headRoot.drawontop = off
	headRoot.wirecolor = red
	headRoot.parent = headBoneObject
	
	debugPrint ("headRoot = "+headroot.name)
	
	doesExist = (getNodeByName guiName)
		if doesExist != undefined do
		(
			debugPrint (guiName+" already found so deleting...")
			delete doesExist
		)
	local arcGuiArray = #()
	local arcGuiParts = #() --local array of parts to make the arcGui
	local myCircle = undefined
	local myCircle2 = undefined
	local myCircle3 = undefined
	local LText = undefined
	local RText = undefined
	
		debugPrint "Starting to make gui ciircles..."
-- 	myCircle = Circle radius:0.0215 pos:[0,0,0]
	myCircle = Circle radius:guiRad pos:[0,0,0]	
		appendIfUnique arcGuiParts myCircle
	maxOps.cloneNodes myCircle cloneType:#copy newNodes:&myCircle2
		myCircle2 = myCircle2[1]
		appendIfUnique arcGuiParts myCircle2
	maxOps.cloneNodes myCircle cloneType:#copy newNodes:&myCircle3 
		myCircle3 = myCircle3[1]
		appendIfUnique arcGuiParts myCircle3
	myCircle2.rotation.controller.X_Rotation = 90
	myCircle3.rotation.controller.Y_Rotation = 90
		debugPrint "gui circles made"

	PointA = [guiRad,0.0015,0]
	PointB= [guiRad,-0.0015,0]

	debugPrint "starting to make gui crosses"
	LText = text size:0.01 kerning:0 leading:0 pos:[guiRad,1.1634e-005,0] 
		LText.Text = "Top"
-- 		LText.Text = ""
		CenterPivot LText
-- 		LText.rotation = (quat 0.5 0.5 0.5 0.5)
		LText.rotation = (quat 0 0 0 1)
-- 		LText.pos = [0.0214844,1.1634e-005,0] 	
		LText.pos = [0,0,guiRad]		
		appendIfUnique arcGuiParts LText
		
	RText = text size:0.01 kerning:0 leading:0 pos:[(guiRad * -1),1.1634e-005,0] 
		RText.Text = "Fwd"
-- 		RText.Text = ""
		CenterPivot RText
-- 		RText.rotation = (quat 0.5 -0.5 -0.5 0.5)
		RText.rotation = (quat 0.707107 0 0 0.707107)
-- 		RText.pos = [-0.021,1.1634e-005,0] 
		RText.pos = [0,(guiRad * -1),0]
	debugPrint "gui crosses made"
 	appendIfUnique arcGuiParts RText

	--NOW NEED TO CREATE UP VECTOR AND FORWARD VECTOR FOR A JOINT BUILT OFF THIS
		
		zPos = myCircle.radius
		
		findNode = getNodeByName ("v_"+guiName+"_UpVect")
		if findNode != undefined do (delete findNode)
		
		--upVect is generated for bone generation
		upVect = point pos:[0,0,zPos]
		upVect.size = 0.01
		upVect.name = ("v_"+guiName+"_UpVect")
		upVect.centermarker = off
		upVect.axistripod = off
		upVect.cross = on
		upVect.Box = off
		
		findNode = getNodeByName ("v_"+guiName+"_FwdVect")
		if findNode != undefined do (delete findNode)
		
		--fwdVect is used for bone generation
		fwdVect = point pos:[0,(zPos * -1),0]
		fwdVect.size = 0.01
		fwdVect.name = ("v_"+guiName+"_FwdVect")
		fwdVect.centermarker = off
		fwdVect.axistripod = off
		fwdVect.cross = on
		fwdVect.Box = off
		
-- 		--parNode is created to prevent gimbal on the gui
-- 		parNode = point pos:[0,0,0]
-- 		parNode.size = 0.2
-- 		parNode.name = ("p_"+guiName)
-- 		parNode.centermarker = off
-- 		parNode.axistripod = off
-- 		parNode.cross = on
-- 		parNode.Box = off		
		
		debugPrint "converting shapes to splines"
		for i in arcGuiParts do
		(
			convertTo i SplineShape
		)
		debugPrint "shapes converted to splines"
		
		debugPrint "combining gui shapes"
		addAndWeld myCircle myCircle3 -1.0
			updateShape myCircle
		addAndWeld myCircle myCircle2 -1.0
			updateShape myCircle		
		addAndWeld myCircle LText -1.0
			updateShape myCircle
		addAndWeld myCircle RText -1.0
			updateShape myCircle
		
		axisLine = createAxesLines()
		
		addAndWeld myCircle axisLine -1.0
			updateShape myCircle
				
		debugPrint "gui shapes combined"
	
		in coordsys parent myCircle.rotation = (quat 0 0 0 1)
		
		upVect.parent = myCircle
		fwdVect.parent = myCircle


		--now we will lock the transforms on the vector points so they cannot be moved
		setTransformLockFlags upVect #all
		setTransformLockFlags fwdVect #all
-- 		upVect.wirecolor = myCircle.wirecolor
-- 		fwdVect.wirecolor = myCircle.wirecolor
		hide upVect
		hide fwdVect
		
		select myCircle --this HAS to be selected in order to function correctly
		myCircle.thickness = 0.0
		
-- 		select myCircle 
-- 		freezeTransform()		
		
		myCircle.scale.controller = ScaleXYZ ()
		
		modPanel.addModToSelection (EmptyModifier ()) ui:on
			bname = "arcGui_Radius"
			
		rolloutCreation = ("arcGuiCA = attributes "+bname +"\n" +"(" +"\n" +"\t" +"parameters main rollout:params"+"\n" +"\t"+"(" +"\n" +"\t" +"\t" +"Radius" +" type:#float ui:" +"Radius"+" default:" +"1.0"+"\n" +"\t"+")" +"\n" +"\t"  +"rollout params "+" \"" +bname+" \"" +"\n" +"\t" +"(" +"\n" +"\t"+"\t"  +"spinner "+"Radius" +" \"" +"Radius" +"\"" +" "+"fieldwidth:40 range:[0,100,1] type:#float" +"\n" +"\t" +")" +"\n"+")"+"\n" +"\n" +"\t" +"custAttributes.add $" +$.name+".modifiers[#'Attribute holder'] "+"arcGuiCA")
			execute rolloutCreation				
			
		--now add a bezier float controller onto the newly created custom attribute		
		arcRad = ("$"+".modifiers[#Attribute_Holder]."+bname+"."+"Radius"+".controller = bezier_float ()")
			execute arcRad
			
		paramWire.connect2way myCircle.modifiers[#Attribute_Holder].arcGui_Radius[#radius] myCircle.scale.controller[#X_Scale] "X_Scale" "radius"		
		paramWire.connect2way myCircle.modifiers[#Attribute_Holder].arcGui_Radius[#radius] myCircle.scale.controller[#Y_Scale] "Y_Scale" "radius"		
		paramWire.connect2way myCircle.modifiers[#Attribute_Holder].arcGui_Radius[#radius] myCircle.scale.controller[#Z_Scale] "Z_Scale" "radius"		
			
		appendIfUnique arcGuiArray myCircle
		if guiName[11] == "R" then
		(
			myCircle.wirecolor = Red
		)
		else
		(
			if guiName[11] == "L" then
			(
				if guiName[12] == "_" do
				(
					myCircle.wirecolor = Green
				)
			)
			else
			(
				myCircle.wirecolor = Blue
			)
		)
		
		upVect.wirecolor = myCircle.wirecolor
		fwdVect.wirecolor = myCircle.wirecolor		
		myCircle.name = (guiName)
		findNumber = 1
		
		for i = 1 to arcNameList[1].count do
		(
			if guiName == arcNameList[1][i] do
			(
				findNumber = i
			)
		)

		sexArray = undefined
		
		if sex == "Male" then --this will allow us to pull default arcgui positions from the correct array
		(
			sexArray = 3
		)
		else
		(
			sexArray = 7
		)

		
		if arcNameList[sexArray][findNumber] != undefined do --sets default positions for arcGuis
		(
			debugPrint "starting to set default position based on character sex..."
			transformStr = ("in coordsys parent $" +(guiName)+arcNameList[sexArray][findNumber] )
			trans = execute transformStr
			debugPrint "set default position based on character sex."
		)

		--now to prevent gimble we have to link the gui to the parNode object
-- 		parNode.transform = myCircle.transform
-- 		myCircle.parent = parNode
-- 		hide parNode			

-- 		--now we need to run through the guiParentArray array so we can set limits correctly
-- 		
-- 		for ind = 1 to guiParentArray.count do 
-- 		(

-- 			if parNode.name == guiParentArray[ind][1] do
-- 			(
-- 				--messagebox ("parNode == "+guiParentArray[ind][1])
-- 				print ("Matched "+parNode.name)
-- 				nodeToLink = getNodeByName parNode.name
-- 				parentNode = undefined
-- 				if guiParentArray[ind][2] == "headRoot" then
-- 				(
-- 					parentNode = headRoot
-- 				)
-- 				else
-- 				(
-- 					parentNode = getNodeByName guiParentArray[ind][2]
-- 				)
-- 				
-- 				print ("nodeToLink = "+(nodeToLink as string))
-- 				print ("parentNode = "+(parentNode as string))
-- 				nodeToLink.parent = parentNode
-- 			)
-- 		)
		
		debugprint ("arcGuiIndex:"+(arcGuiIndex as string))
		arcNameList[6][arcGuiIndex] = $
		
		--try to reparent now
		--gonna only parent when we press the frigify button so the guis can be reoriented without fucking others up.
-- 		parentGuis headRoot
)


fn mirrorGui geo guiToMirror arcGuiIndex = --auto create the mirrored right hand guis and match their settings to those on the left. Also parent all nodes to match that required by final hierarchy.
(
	debugPrint ("mirroring "+guiToMirror)
	debugPrint ("geo name = "+geo.name)
	headBoneObject = getNodeByName headBone
	headRoot = getNodebyName ("FaceRoot_"+geo.name)
	if headRoot == undefined do
	(
		debugPrint "headRoot is being generated..." 
		
		--now we need to find the centre point...
		skelBone = $SKEL_Head
		centrePos = bboxCentre geo
		
		pX = centrePos.x
		pY = skelBone.position.Y
		pZ = skelBone.position.Z
		
		cPos = [pX,pY,pZ]
		
-- 		headRoot = Point pos:headBoneObject.pos
		headRoot = Point pos:cPos
		
		hName = geo.name
		headRoot.name = ("FaceRoot_"+hName)
		
		headRoot.rotation = headBoneObject.rotation
		headRoot.position = headBoneObject.pos
		headRoot.size = 0.01
		headRoot.cross = on
		headRoot.axistripod = on
		headRoot.centermarker = on
		headRoot.Box = on
		headRoot.constantscreensize = off
		headRoot.drawontop = off
		headRoot.wirecolor = red
		headRoot.parent = headBoneObject		
		
		--now need to add a udp tag to faceRoot
		setUserPropbuffer headRoot ("tag = facialroot")		
	)
	
-- 	headRoot.rotation = headBoneObject.rotation
-- 	headRoot.position = headBoneObject.pos
-- 	headRoot.size = 0.01
-- 	headRoot.cross = on
-- 	headRoot.axistripod = on
-- 	headRoot.centermarker = on
-- 	headRoot.Box = on
-- 	headRoot.constantscreensize = off
-- 	headRoot.drawontop = off
-- 	headRoot.wirecolor = red
-- 	headRoot.parent = headBoneObject

		
-- 		--now need to add a udp tag to faceRoot
-- 	setUserPropbuffer headRoot ("tag = facialroot")
		
-- 	MirrorPoint = Point pos:[0,0,0]
-- 	MirrorPoint.name = "MirrorPoint"
-- 	MirrorPoint.scale.controller = ScaleXYZ ()

	leftGui = getNodeByName guiToMirror
-- 	leftGui.parent = MirrorPoint

	guiIndex = undefined

	guiRad = 0.0215 --set default gui radius

-- 	MirrorPoint.scale.controller.X_Scale = -100 --temporarily flip them
	currentLayer = LayerManager.current

	for i = 1 to leftGuis.count do
	(
		if guiToMirror == leftGuis[i] do
		(
			--now we need to find the appropriate layer for the mirrored gui by finding the layer the leftgui is sitting on
			
			lyrGuiObj = getNodeByName leftGuis[i]
			for lyr = 0 to layerManager.count-1 do
			(
				ilayer = layerManager.getLayer lyr
				layerName = ilayer.name
				layer = ILayerManager.getLayerObject lyr
				layerNodes = refs.dependents layer
				
				for obj in layerNodes do 
				(
					if obj == lyrGuiObj then
					(
						--format ((obj.name as string)+" found on layer "+(layerName as string)+"\n")						
						layer.current = true
					)
				)
			)			
			
			rightGui = getNodeByName rightGuis[i]
			if rightGui != undefined do
			(
				print ("Deleting old "+rightGui.name)
				delete rightGui
			)
			
			rightGui = arcGuiCreation rightGuis[i] arcGuiIndex guiRad
			
			rightGui = getNodeByName rightGuis[i]
-- 			rightGui.transform = leftGui.transform
			rightGui.modifiers[#Attribute_Holder].arcGui_Radius.radius = leftGui.modifiers[#Attribute_Holder].arcGui_Radius.radius 

			rightGui.parent = headRoot
			
			leftGui.parent = headRoot			
			--now we will mirror the transforms
			
			origPos = leftGui.pos
			
			oX = (origPos[1] * -1)
			oY = origPos[2]
			oZ = origPos[3]
			newPos = [oX,oY,oZ]
			
			orX = leftGui.rotation.X_Rotation 
			orY = (leftGui.rotation.Y_Rotation * -1)
			orZ = (leftGui.rotation.Z_Rotation * -1)
			
			rightGui.pos = newPos
			rightGui.rotation.X_Rotation = orX 
			rightGui.rotation.Y_Rotation = orY
			rightGui.rotation.Z_Rotation = orZ			

		)
	)
)


fn genArcGuiFromSkel headRoot = --will generate arcguis from the currently selected heads skeleton
(

--***** WE NEED TO DYNAMICALLY GENERATE THE ARCGUIS BY FIGURING OUT THEIR RADIUS FROM THE LENGTH OF THE JOINT
--***** THEN WE SET IT'S TRANSLATION TO MATCH THE JOINT

	for f in objects do 
	(
		if (substring f.name 1 3) == "FB_" do 
		(
			--ok we found a face bone so we need to find it's length
			boneLength = f.length
			
			--need to strip off the _000 etc off the bone name
			jointNameLength  = f.name.count
			jointName = (substring f.name 1 (jointNameLength - 4))
			guiName = ("arcGUI_"+jointName)
			
			--now store the joint number so we can find the appropriate head root
			jNo = (substring f.name (jointNameLength - 3) 4)
			debugprint ("jNo = "+jNo)
			--as we dont know the name of the geo this node was for we will find the FB_Brow_Centre and get its parent
			browJoint = getNodeByName ("FB_Brow_Centre"+jNo)
			debugprint ("Brow joint = "+(browJoint as string))
			headRootObj = browJoint.parent
				
			--now we need to find the index of the gui by comparing guiName to the arcNameList
			arcGuiIndex = undefined 
			for i = 1 to arcNameList[1].count do 
			(
				if arcNameList[1][i] == guiName do 
				(
					arcGuiIndex = i
				)
			)
			
			--messagebox ("arcGuiIndex for "+guiName+" = "+(arcGuiIndex as string))
			newGui = arcGuiCreation guiName arcGuiIndex boneLength	
				
			--now we re-do the transform of the object to override what was set during gui creation
			newGui.transform = f.transform
			--now we need to flip the rotation -90
			--in coordsys local newGui.rotation.controller.Z_rotation = -90
			in coordsys local rotate newGui (angleaxis 90.0 [0,0,1])
			newGui.modifiers[#Attribute_Holder].arcGui_Radius.radius = 1
			newGui.parent = headRootObj
		)
	)
)
	