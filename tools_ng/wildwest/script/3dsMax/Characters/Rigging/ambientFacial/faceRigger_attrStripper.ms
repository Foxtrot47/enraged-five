-- attrStripper.ms
-- Rockstar North
--Part of the facial rigging tools
--script which saves out/loads in  custom attribute data from facial joysticks
--Matt Rennie Sept 2010
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

-- clearListener()
fn finaliseBoneNames = --this hacks the name back to the none numbered version
(
	debugPrint "Finalising bone Names"
-- 	for i =1 to  FaceBoneList[2].count do
-- 	(
-- 		debugPrint ("Renaming"+faceBoneList[2][i].name+" to "+faceBoneNameList[i])
-- 		faceBoneList[2][i].name = faceBoneNameList[i]
-- 	)
	debugprint "*********************************************************************************************"
	debugprint "		FACIAL RIGGING COMPLETE.!!"
)

fn setAttributes attrFileToUse = --load in custom attributes
(
	if attrFileToUse != undefined then
	(
		debugPrint ("Importing "+attrFileToUse)
		filein attrFileToUse
	)
	else
	(
		MessageBox "Warning couldn't find the specified file..."
	)
	finaliseBoneNames()
)
	
fn getAttributes = --save out attributes
(	
	boneFolder = maxFilePath
	maxName = filterstring maxFileName "."
	fName = (maxName[1]+".atr")
	output_name = (boneFolder + fName)
	debugPrint ("Output name = "+output_name)
	
	--delete the outputfile if exists
	if output_name != undefined do
	(
		deleteFile output_name
		debugPrint ("Deleted old "+output_name)
	)	
	
	if facialJoysticks.count == 0 then
	(
		for obj in objects do
		(
			if (substring obj.name 1 5) == "CTRL_" do
			(
				appendifunique facialJoysticks obj
			)
		)
	)
	
	if facialJoysticks.count != 0 then
	(
		output_file = createFile output_name	
		debugPrint ("Created new "+output_name +" file.")
		
		for i = 1 to facialJoysticks.count do
		(
			currObj = (getNodeByName facialJoysticks[i].jsName)
				
			if currObj != undefined do
			(
				if currObj.modifiers[#Attribute_Holder] != undefined do
				(
					debugPrint ("--"+currObj.name)
					format ("--"+currObj.name+"\n") to: output_file
					
					for index = 1 to 50 do --AS WE CANNOT QUERY THE NUMBER OF ATTRS ON A OBJECT JUST USED 50 AS AN ARBITRARY VALUE
					(
						im_Attr = custAttributes.get currObj.modifiers[#Attribute_Holder] index
						
						if im_Attr != undefined do
						(
								im_Array = getPropNames im_Attr

								rolloutStr = (filterstring (im_Attr as string) ":")
								rolloutStr = rolloutStr[2]
								
								for i = 1 to im_array.count do
								(
									currentAttr = (im_Array[i] as string)
									
									executeString = ("$"+currObj.name+".modifiers[#Attribute_Holder]."+rolloutStr+"."+currentAttr+".controller.value")
										attrValue = execute executeString
									
									attrName = ("if $"+currObj.name+" != undefined do ($"+currObj.name+".modifiers[#Attribute_Holder]."+rolloutStr+"."+currentAttr+" = ")
									debugPrint (attrName + (attrValue as string)) 
									format (attrName + (attrValue as string) +")"+"\n") to: output_file
								)
						)
					)
				)
			)
		)
		format ("--Attribute setting complete."+"\n") to: output_file		
		close output_file
		debugPrint ("Joystick custom Attribute data output to "+(output_file as string))		
	)
	else
	(
		messagebox "facialJoysticks array is empty!" beep:true
	)
)

