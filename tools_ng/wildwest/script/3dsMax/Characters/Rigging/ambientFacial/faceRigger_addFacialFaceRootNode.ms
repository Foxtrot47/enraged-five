--tool to add in a FACIAL_facialRoot node to pre generated ambient rigs

headName = "SKEL_Head"
rootName = "FACIAL_facialRoot"


headNode = getNodeByName headName
fRootNode = getNodeByName rootName

if fRootNode == undefined do 
(
	fRootNode = point name:rootName
	fRootNode.size = 0.01
	fRootNode.transform = headNode.transform
	fRootNode.parent = headNode
)

headKids = headNode.children

for a in headKids do 
(
	if substring a.name 1 8 == "FaceRoot" do 
	(
		a.parent = fRootNode
	)
)