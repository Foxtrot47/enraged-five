--script to hookup single axis sliders to drive facefx ambient head joysticks

-- Load the common maxscript functions 
--include "rockstar/export/settings.ms" -- This is SLOW! 
filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()


-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_bone_tagger.ms")
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_Rigging.ms")
filein (theProjectRoot + "tools/dcc/current/max2011/scripts/pipeline/util/xml.ms")


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



faceFXSliderArray = #(
	"Neutral",
	"Open"--,
-- 	"W",
-- 	"Sh-Ch",
-- 	"P-B-M",
-- 	"F-V",
-- 	"Oo",
-- 	"u",
-- 	"a",
-- 	"ee",
-- 	"d",
-- 	"th"
	)
	
ambientArray = #(
	"Tongue",
	"L_Blink",
	"LookAT_Activator",
	"L_Cheek",
	"LowerLip",
	"UpperLip_Curl",
	"LowerLip_Curl",
	"UpperLip",
	"L_Mouth",
	"Jaw",
	"Mouth",
	"R_Eye",
	"L_Eye",
	"R_Mouth",
	"C_Brow",
	"Tongue_In_Out",
	"R_Cheek",
	"R_Brow",
	"L_Brow",
	"R_Blink"
	)

struct driverParamStruct (ffxSlider, ambientJS, multX, multY, ind)
-- 	ffxSlider = name of face fx (1d) slider
-- 	ambientJS = name of the ambient head joystick (e.g. R_Brow or C_Brow)
-- 	multX = multiplier for the x movement of the ambient joystick for the current ffxslider
-- 	multY = multiplier for the y movement of the ambient joystick for the current ffxslider
-- 	ind = index of the controller for the list - neutral would be 1, open 2 etc etc

driverListStruct = #( --array of all skeleton bones which we want to split mesh for
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"Tongue" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"L_Blink" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"LookAt_Activator" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"L_Cheek" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"LowerLip" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"UpperLip_Curl" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"LowerLip_Curl" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"UpperLip" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"L_Mouth" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"Jaw" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"Mouth" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"R_Eye" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"L_Eye" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"R_Mouth" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"C_Brow" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"Tongue_In_Out" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"R_Cheek" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"R_Brow" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"L_Brow" multX:0 multY:0 ind:1),
	(driverParamStruct ffxSlider:"Neutral" ambientJS:"R_Blink" multX:0 multY:0 ind:1),
	----
	(driverParamStruct ffxSlider:"Open" ambientJS:"Tongue" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"L_Blink" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"LookAt_Activator" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"L_Cheek" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"LowerLip" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"UpperLip_Curl" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"LowerLip_Curl" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"UpperLip" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"L_Mouth" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"Jaw" multX:0 multY:-1 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"Mouth" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"R_Eye" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"L_Eye" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"R_Mouth" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"C_Brow" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"Tongue_In_Out" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"R_Cheek" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"R_Brow" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"L_Brow" multX:0 multY:0 ind:2),
	(driverParamStruct ffxSlider:"Open" ambientJS:"R_Blink" multX:0 multY:0 ind:2),	
	----
	(driverParamStruct ffxSlider:"W" ambientJS:"Tongue" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"L_Blink" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"LookAt_Activator" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"L_Cheek" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"LowerLip" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"UpperLip_Curl" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"LowerLip_Curl" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"UpperLip" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"L_Mouth" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"Jaw" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"Mouth" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"R_Eye" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"L_Eye" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"R_Mouth" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"C_Brow" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"Tongue_In_Out" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"R_Cheek" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"R_Brow" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"L_Brow" multX:0 multY:0 ind:3),
	(driverParamStruct ffxSlider:"W" ambientJS:"R_Blink" multX:0 multY:0 ind:3),	
	----
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"Tongue" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"L_Blink" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"LookAt_Activator" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"L_Cheek" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"LowerLip" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"UpperLip_Curl" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"LowerLip_Curl" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"UpperLip" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"L_Mouth" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"Jaw" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"Mouth" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"R_Eye" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"L_Eye" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"R_Mouth" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"C_Brow" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"Tongue_In_Out" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"R_Cheek" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"R_Brow" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"L_Brow" multX:0 multY:0 ind:4),
	(driverParamStruct ffxSlider:"Sh-Ch" ambientJS:"R_Blink" multX:0 multY:0 ind:4),	
	----	
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"Tongue" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"L_Blink" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"LookAt_Activator" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"L_Cheek" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"LowerLip" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"UpperLip_Curl" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"LowerLip_Curl" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"UpperLip" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"L_Mouth" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"Jaw" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"Mouth" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"R_Eye" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"L_Eye" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"R_Mouth" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"C_Brow" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"Tongue_In_Out" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"R_Cheek" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"R_Brow" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"L_Brow" multX:0 multY:0 ind:5),
	(driverParamStruct ffxSlider:"P-B-M" ambientJS:"R_Blink" multX:0 multY:0 ind:5),	
	----
	(driverParamStruct ffxSlider:"F-V" ambientJS:"Tongue" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"L_Blink" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"LookAt_Activator" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"L_Cheek" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"LowerLip" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"UpperLip_Curl" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"LowerLip_Curl" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"UpperLip" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"L_Mouth" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"Jaw" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"Mouth" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"R_Eye" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"L_Eye" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"R_Mouth" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"C_Brow" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"Tongue_In_Out" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"R_Cheek" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"R_Brow" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"L_Brow" multX:0 multY:0 ind:6),
	(driverParamStruct ffxSlider:"F-V" ambientJS:"R_Blink" multX:0 multY:0 ind:6),	
	----	
	(driverParamStruct ffxSlider:"Oo" ambientJS:"Tongue" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"L_Blink" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"LookAt_Activator" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"L_Cheek" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"LowerLip" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"UpperLip_Curl" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"LowerLip_Curl" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"UpperLip" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"L_Mouth" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"Jaw" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"Mouth" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"R_Eye" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"L_Eye" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"R_Mouth" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"C_Brow" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"Tongue_In_Out" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"R_Cheek" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"R_Brow" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"L_Brow" multX:0 multY:0 ind:7),
	(driverParamStruct ffxSlider:"Oo" ambientJS:"R_Blink" multX:0 multY:0 ind:7),	
	----	
	(driverParamStruct ffxSlider:"u" ambientJS:"Tongue" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"L_Blink" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"LookAt_Activator" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"L_Cheek" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"LowerLip" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"UpperLip_Curl" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"LowerLip_Curl" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"UpperLip" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"L_Mouth" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"Jaw" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"Mouth" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"R_Eye" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"L_Eye" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"R_Mouth" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"C_Brow" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"Tongue_In_Out" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"R_Cheek" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"R_Brow" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"L_Brow" multX:0 multY:0 ind:8),
	(driverParamStruct ffxSlider:"u" ambientJS:"R_Blink" multX:0 multY:0 ind:8),	
	----	
	(driverParamStruct ffxSlider:"a" ambientJS:"Tongue" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"L_Blink" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"LookAt_Activator" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"L_Cheek" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"LowerLip" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"UpperLip_Curl" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"LowerLip_Curl" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"UpperLip" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"L_Mouth" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"Jaw" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"Mouth" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"R_Eye" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"L_Eye" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"R_Mouth" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"C_Brow" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"Tongue_In_Out" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"R_Cheek" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"R_Brow" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"L_Brow" multX:0 multY:0 ind:9),
	(driverParamStruct ffxSlider:"a" ambientJS:"R_Blink" multX:0 multY:0 ind:9),	
	----		
	(driverParamStruct ffxSlider:"ee" ambientJS:"Tongue" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"L_Blink" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"LookAt_Activator" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"L_Cheek" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"LowerLip" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"UpperLip_Curl" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"LowerLip_Curl" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"UpperLip" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"L_Mouth" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"Jaw" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"Mouth" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"R_Eye" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"L_Eye" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"R_Mouth" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"C_Brow" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"Tongue_In_Out" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"R_Cheek" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"R_Brow" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"L_Brow" multX:0 multY:0 ind:10),
	(driverParamStruct ffxSlider:"ee" ambientJS:"R_Blink" multX:0 multY:0 ind:10),	
	----		
	(driverParamStruct ffxSlider:"d" ambientJS:"Tongue" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"L_Blink" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"LookAt_Activator" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"L_Cheek" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"LowerLip" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"UpperLip_Curl" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"LowerLip_Curl" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"UpperLip" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"L_Mouth" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"Jaw" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"Mouth" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"R_Eye" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"L_Eye" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"R_Mouth" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"C_Brow" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"Tongue_In_Out" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"R_Cheek" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"R_Brow" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"L_Brow" multX:0 multY:0 ind:11),
	(driverParamStruct ffxSlider:"d" ambientJS:"R_Blink" multX:0 multY:0 ind:11),	
	----		
	(driverParamStruct ffxSlider:"th" ambientJS:"Tongue" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"L_Blink" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"LookAt_Activator" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"L_Cheek" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"LowerLip" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"UpperLip_Curl" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"LowerLip_Curl" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"UpperLip" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"L_Mouth" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"Jaw" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"Mouth" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"R_Eye" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"L_Eye" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"R_Mouth" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"C_Brow" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"Tongue_In_Out" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"R_Cheek" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"R_Brow" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"L_Brow" multX:0 multY:0 ind:12),
	(driverParamStruct ffxSlider:"th" ambientJS:"R_Blink" multX:0 multY:0 ind:12)
	----		
)

fn connectViaStruct = 
(
	--facefx sliders must be created first...
	for str = 1 to driverListStruct.count do
-- 	for str = 2 to driverListStruct.count do	
	(
		clearListener()
		print ("Configuring for "+driverListStruct[str].ffxSlider)
		ambSlider = getNodeByName ("CTRL_"+driverListStruct[str].ambientJS)
		
		--first we add a controller into the x position for the ffxSlider IF its not just a vert joystick
		if ambSlider.position.controller.zero_pos_xyz.x_position.controller.upper_limit != 0 do --ok this means we have a slider which isnt just up and down
		(
			print (ambSlider.name+" is not limited in X")
		
			if (ambSlider.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller as string) == ("Controller:Float_Limit") do
			(
				if driverListStruct[str].ind == 1 do
				(
					ambSlider.pos.controller.Zero_Pos_XYZ.controller.x_position.controller.Limited_Controller__Bezier_Float.controller = float_List()
				)
				
				ffxJoystick = getNodeByName ("CTRL_"+driverListStruct[str].ffxSlider)
				
				ambSlider.pos.controller.Zero_Pos_XYZ.controller.x_position.controller.Limited_Controller__Float_List.controller.available.controller = float_Expression()

				--now name the controller
				print ("ind = "+(driverListStruct[str].ind as string))
				ambSlider.pos.controller.Zero_Pos_XYZ.controller.x_position.controller.Limited_Controller__Float_List.controller.setName (driverListStruct[str].ind + 1) driverListStruct[str].ffxSlider
				
				--now we need to add stuff to the expression controller we just created
				expObj = ambSlider
				expCont = (".pos.controller.Zero_Pos_XYZ.controller.x_position.controller.Limited_Controller__Float_List.controller["+((driverListStruct[str].ind + 1) as string)+"].controller")
				scalarNames = #("Driver")
				scalarConts = #(("$"+"'"+ffxJoystick.name+"'"+".position.controller.Zero_Pos_XYZ.controller.Y_position.controller"))
				expString = (scalarNames[1]+" * "+(driverListStruct[str].multX as string))

				print ("expObj = "+expObj.name)
				print ("expCont = "+expCont)
				print ("scalarNames = "+(scalarNames as string))
				print ("scalarConts = "+(scalarConts as string))
				print ("expString = "+expString)
					
				generateExpression expObj expCont scalarNames scalarConts expString
				print "==============================================="					
				--now name the controller
				ambSlider.pos.controller.Zero_Pos_XYZ.controller.x_position.controller.Limited_Controller__Float_List.controller.setName (driverListStruct[str].ind + 1) driverListStruct[str].ffxSlider
				print 	("X "+driverListStruct[str].ffxSlider+" done for "+driverListStruct[str].ambientJS)
			)
		)
-- 		
		if (ambSlider.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller as string) == ("Controller:Float_Limit") do
		(
			if driverListStruct[str].ind == 1 do
			(
				ambSlider.pos.controller.Zero_Pos_XYZ.controller.y_position.controller.Limited_Controller__Bezier_Float.controller = float_List()
			)
			
			ffxJoystick = getNodeByName ("CTRL_"+driverListStruct[str].ffxSlider)
			
			ambSlider.pos.controller.Zero_Pos_XYZ.controller.y_position.controller.Limited_Controller__Float_List.controller.available.controller = float_Expression()

					--now name the controller
			print ("ind = "+(driverListStruct[str].ind as string))
			ambSlider.pos.controller.Zero_Pos_XYZ.controller.y_position.controller.Limited_Controller__Float_List.controller.setName (driverListStruct[str].ind + 1) driverListStruct[str].ffxSlider
			
			--now we need to add stuff to the expression controller we just created
			expObj = ambSlider
			expCont = (".pos.controller.Zero_Pos_XYZ.controller.y_position.controller.Limited_Controller__Float_List.controller["+((driverListStruct[str].ind + 1) as string)+"].controller")
			scalarNames = #("Driver")
			scalarConts = #(("$"+"'"+ffxJoystick.name+"'"+".position.controller.Zero_Pos_XYZ.controller.Y_position.controller"))
			expString = (scalarNames[1]+" * "+(driverListStruct[str].multY as string))

			print ("expObj = "+expObj.name)
			print ("expCont = "+expCont)
			print ("scalarNames = "+(scalarNames as string))
			print ("scalarConts = "+(scalarConts as string))
			print ("expString = "+expString)

			generateExpression expObj expCont scalarNames scalarConts expString
			print "==============================================="				
			--now name the controller
			ambSlider.pos.controller.Zero_Pos_XYZ.controller.y_position.controller.Limited_Controller__Float_List.controller.setName (driverListStruct[str].ind + 1) driverListStruct[str].ffxSlider
			print 	("Y "+driverListStruct[str].ffxSlider+" done for "+driverListStruct[str].ambientJS)
		)
	)
)

fn connectSliders = 
(
		--now we need to add a controller to each axis on each of the faceFX sliders
		-- we first need to test if the facefx slider is one d or 2 d
		for amb = 1 to ambientArray.count do
		(
			ambSlider = getNodeByName ("CTRL_"+ambientArray[amb]) --this gets the actual ambient head joystick
		
			
			--first we add a controller into the x position for the ffxSlider IF its not just a vert joystick
			if ambSlider.position.controller.zero_pos_xyz.x_position.controller.upper_limit != 0 do --ok this means we have a slider which isnt just up and down
			(
				print (ambSlider.name+" is not limited in X")
			
				if (ambSlider.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller as string) == ("Controller:Float_Limit") do
				(
					if 	ambSlider.pos.controller.Zero_Pos_XYZ.controller.x_position.controller.Limited_Controller__Bezier_Float.controller != float_List() do
					(
						ambSlider.pos.controller.Zero_Pos_XYZ.controller.x_position.controller.Limited_Controller__Bezier_Float.controller = float_List()
					)

					--now we loop thru all the faceFX Sliders so we get a float on each ambient slider for each facefx(1d) slide

					for ffx = 1 to faceFXSliderArray.count do
					(	
-- 						print ("Looking for CTRL_"+faceFXSliderArray[ffx])
						ffxJoystick = getNodeByName ("CTRL_"+faceFXSliderArray[ffx])
						
						ambSlider.pos.controller.Zero_Pos_XYZ.controller.x_position.controller.Limited_Controller__Float_List.controller.available.controller = float_Expression()

						--now name the controller
						ambSlider.pos.controller.Zero_Pos_XYZ.controller.x_position.controller.Limited_Controller__Float_List.controller.setName (ffx + 1) faceFXSliderArray[ffx]
						
						--now we need to add stuff to the expression controller we just created
						expObj = ambSlider
						expCont = (".pos.controller.Zero_Pos_XYZ.controller.x_position.controller.Limited_Controller__Float_List.controller["+((ffx + 1) as string)+"].controller")
						scalarNames = #(faceFXSliderArray[ffx])
						scalarConts = #(("$"+ffxJoystick.name+".position.controller.Zero_Pos_XYZ.controller.Y_position.controller"))
						expString = (scalarNames[1]+" * 1")

						print ("expObj = "+expObj.name)
						print ("expCont = "+expCont)
						print ("scalarNames = "+(scalarNames as string))
						print ("scalarConts = "+(scalarConts as string))
						print ("expString = "+expString)

						generateExpression expObj expCont scalarNames scalarConts expString
						print "==============================================="							
							
						--now name the controller
						ambSlider.pos.controller.Zero_Pos_XYZ.controller.x_position.controller.Limited_Controller__Float_List.controller.setName (ffx + 1) faceFXSliderArray[ffx]							
					)
				)
			)

			if ambSlider.position.controller.zero_pos_xyz.y_position.controller.upper_limit != 0 do --ok this means we have a slider which isnt just up and down
			(
				print (ambSlider.name+" is not limited in Y")
			
				if (ambSlider.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller as string) == ("Controller:Float_Limit") do
				(
					if 	ambSlider.pos.controller.Zero_Pos_XYZ.controller.y_position.controller.Limited_Controller__Bezier_Float.controller != float_List() do
					(
						ambSlider.pos.controller.Zero_Pos_XYZ.controller.y_position.controller.Limited_Controller__Bezier_Float.controller = float_List()
					)

					--now we loop thru all the faceFX Sliders so we get a float on each ambient slider for each facefx(1d) slide
					for ffx = 1 to faceFXSliderArray.count do
					(	
-- 						print ("Looking for CTRL_"+faceFXSliderArray[ffx])
						ffxJoystick = getNodeByName ("CTRL_"+faceFXSliderArray[ffx])
						
						ambSlider.pos.controller.Zero_Pos_XYZ.controller.y_position.controller.Limited_Controller__Float_List.controller.available.controller = float_Expression()

						--now name the controller
						ambSlider.pos.controller.Zero_Pos_XYZ.controller.y_position.controller.Limited_Controller__Float_List.controller.setName (ffx + 1) faceFXSliderArray[ffx]
						
						--now we need to add stuff to the expression controller we just created
						expObj = ambSlider
						expCont = (".pos.controller.Zero_Pos_XYZ.controller.y_position.controller.Limited_Controller__Float_List.controller["+((ffx + 1) as string)+"].controller")
						scalarNames = #(faceFXSliderArray[ffx])
						scalarConts = #(("$"+ffxJoystick.name+".position.controller.Zero_Pos_XYZ.controller.Y_position.controller"))
						expString = (scalarNames[1]+" * 1")

						print ("expObj = "+expObj.name)
						print ("expCont = "+expCont)
						print ("scalarNames = "+(scalarNames as string))
						print ("scalarConts = "+(scalarConts as string))
						print ("expString = "+expString)
							print "==============================================="
						generateExpression expObj expCont scalarNames scalarConts expString
							
						--now name the controller
						ambSlider.pos.controller.Zero_Pos_XYZ.controller.x_position.controller.Limited_Controller__Float_List.controller.setName (ffx + 1) faceFXSliderArray[ffx]							
					)
				)
			)			
			
		)	
)

clearListener()
-- connectSliders()
connectViaStruct()