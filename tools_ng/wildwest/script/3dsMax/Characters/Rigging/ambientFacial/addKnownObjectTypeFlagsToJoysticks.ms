
filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

theProjectConfig = RsConfigGetProjBinConfigDir()

toolsRoot = RsConfigGetToolsRootDir()

filein (theWildWest + "script/3dsMax/_config_files/Wildwest_header.ms")

filein (theWildWest + "script/3dsMax/_common_functions/FN_RSTA_Rigging.ms")


-- fn RSTA_addKnownObjectTagsToJoysticks objArray =
-- (
-- 	knownObjectFlags = #(
-- 		#("CTRL_", "joystick"),
-- 		#("TEXT_", "joystickText"),
-- 		#("RECT_", "joystickSurround")
-- 		)
-- 		
-- 	for obj in objArray do 
-- 	(
-- 		for i = 1 to knownObjectFlags.count do 
-- 		(
-- 			type = knownObjectFlags[i][1]
-- 			typeLength = type.count
-- 			
-- 			if (substring obj.name 1 typeLength) == type then 
-- 			(
-- 				exist = getUserProp obj "knownObjectType"
-- 				
-- 				if exist == undefined do 
-- 				(
-- 					udp = getUserPropBuffer obj
-- 					udp = udp+"\r\n"
-- 					setUserPropBuffer obj udp
-- 					
-- 					setUserProp obj "knownObjectType" knownObjectFlags[i][2]
-- 					format ("Set "+obj.name+" to "+knownObjectFlags[i][2]+"\n")
-- 				)
-- 			)
-- 			--else
-- 			--(
-- 				--format ((substring obj.name 1 typeLength)+" != "+type+"\n" )
-- 			--)
-- 		)
-- 	)
-- 	
-- 	for obj in objArray do 
-- 	(
-- 		if substring obj.name 1 5 == "RECT_" do
-- 		(
-- 			for ch in obj.children do 
-- 			(
-- 				if substring ch.name 1 5 != "CTRL_" then
-- 				(
-- 					if substring ch.name 1 5 != "TEXT_" then
-- 					(
-- 						exist = getUserProp ch "knownObjectType"
-- 						
-- 						if exist == undefined then 
-- 						(
-- 							udp = getUserPropBuffer ch
-- 							udp = udp+"\r\n"
-- 							setUserPropBuffer ch udp
-- 							
-- 							setUserProp ch "knownObjectType" "joystick"
-- 							format ("Set "+ch.name+" to "+"joystick"+"\n")
-- 						)
-- 						else
-- 						(
-- 							format (ch.name+"already has knownObjectType set\n")
-- 						)
-- 					)
-- 					else
-- 					(
-- 						format (ch.name+" == CTRL_"+"\n")
-- 					)
-- 				)
-- 				else
-- 				(
-- 					format (ch.name+" == CTRL_"+"\n")
-- 				)
-- 			)
-- 		)
-- 	)
-- 	format ("Done\n")
-- )

objArray = selection as array
RSTA_addKnownObjectTagsToJoysticks objArray