-- messagebox ("ANIMALS!")
arcGuiLibraryPath = (theProjectRoot + "art/peds/Skeletons/arcGuiLibrary/")
	debugPrint ("arcGuiLibraryPath = "+arcGuiLibraryPath )
headBone = "SKEL_Head"
helpImagePath = (theProjectRoot + "/art/peds/Skeletons/Facial/images/")

rotDivider = "(0.5 /4)"
transDivider = "(0.005 /4)"

createFaceFX = false

-- flipExpressionJoysticks = #(
-- 	"FB_UpperLip_000"
-- 	)

--defaultAttrFileM = (RsConfigGetWildWestDir() + "script\\3dsMax\\Characters\\Rigging\\ambientFacial\\facialDefaultsM.atr") --male default attributes


--at present it uneccesary to have male and female atr files, so setting them both to use the female.
defaultAttrFileM = (RsConfigGetWildWestDir() + "script\\3dsMax\\Characters\\Rigging\\ambientFacial\\facialDefaultsJawOnly.atr") --female default attributes

defaultAttrFileF = (RsConfigGetWildWestDir() + "script\\3dsMax\\Characters\\Rigging\\ambientFacial\\facialDefaultsJawOnly.atr") --female default attributes


transTrackVal = ("exportTrans = true")
rotationTrackVal = ("exportTrans = false")

jointCreationArray = #( --index 1 = the node itself index2 == the node we want it to point at
	--if index 2 == "FORWARD then the tool will just point it directly forward of its pivot, if index2 == ""fwdVect" then it will point towards its custom forward vector helper
	#("arcGUI_FB_Jaw" ,"fwdVect" )
	)

--this is the array of all arcGui names. This has to be declared seperately from the arcNameList as arcNameList is semi-dynamic and needs this to evaluate its 2nd dimnension. 
arcNameArray = #( --THIS SHOULD NEVER BE REFERRED TO IN SCRIPT. WE SHOULD ONLY REFERENCE THESE NAMES VIA arcNameList.
	"arcGUI_FB_Jaw" --1
	)

--array of all arcGui objects (last item is simply there as a label on final button)
arcNameList = #( -- first dimension is pulling items defined in arcNameArray, 2nd dimension is defining parenting, 3rd dimension is defualt gui positions
	#( 
	--DIMENSION 1
	arcNameArray[1] --1 arcGUI_FB_Jaw
	),
	#(	--array of parents for the nodes in the 1st dimension
	--DIMENSION 2
	headBone --1 arcGUI_FB_Jaw
	),
	#( --array of initial transform for MALE arcGuis		
	--DIMENSION 3
	),
	#( --dynamically created array of names for items in the listbox - if gui exists then we get a stripped version of its name, otherwise we get the name with warning characters on it.
		--DIMENSION 4
	),
	#(--array of all arcGui help images
		--DIMENSION 5
		(helpImagePath+(substring arcNameArray[1] 11 50)+".jpg") --1 arcGUI_FB_Jaw

		),--,
		#(--dynamically created list of all arcGui objects
			--DIMENSION 6
		),
	#( --array of initial transforms for FEMALE arcGuis				
	--DIMENSION 7
	".transform = (matrix3 [12.4702,0,0] [0,8.59807,9.03218] [0,-9.03218,8.59807] [0,-1.21323,1.78914])" --arcGUI_FB_Jaw
	)
)
	
-- arcNameListCopy = deepcopy arcNameList
-- this is an array of all the 'right' guis. This needs to match with the 'left' list as they are paired together in order to mirror transforms	 
rightGuis = #( 
	)	

--list of all arcguis for left side
leftGuis = #( 
	)	
	
arcGuiParentingArray = #( --2d array used for physical objects in the scene and their parenting for arcGuis
		#(),
		#()
		)	

--this is the array of all faceBone names. This has to be declared seperately from the faceBoneList as faceBoneList is semi-dynamic 
--and needs this to evaluate its 2nd dimnension. 	
faceBoneNameList = #( --THIS SHOULD NEVER BE REFERRED TO IN SCRIPT. WE SHOULD ONLY REFERENCE THESE NAMES VIA faceBoneList.
"FB_Jaw" --1
	)
	
faceBoneList = #(
	#(--DIMENSION 1
	faceBoneNameList[1] --1  = FB_Jaw
	),
	#(--dynamically populated array of actual bones we want to parent, generated via querying headRoot node and bones udp tag
		--DIMENSION 2
		),

	#(		--array of parents for the nodes in the 1st dimension. "headRootBone" is declared as a string as this is dependant on the number of them we have in the scene.
		--this array gets re-populated during faceRigCreation
		--DIMENSION 3
	"headRootBone" --1 arcGUI_FB_Jaw

	)		
)

-- jsfb_faceBone = bone driven by this joystick
--jsfb_Type - 1 = vert 2 = sq
--jsfb_Square - 1 = vert 2 = sq
-- jsfb_fbAxisTX = bone trsans axis driven by X trans of joystick
-- jsfb_fbAxisTY = bone trans axis driven by Y trans of joystick
--  jsfb_fbAxisRX = bone rot axis driven by X trans of joystick
-- jsfb_fbAxisRY = bone rot axis driven by Y trans of joystick
struct jsfb_Settings (jsfb_faceBone, jsfb_Type, jsfb_fbAxisTX, jsfb_fbAxisTY, jsfb_fbAxisTZ, jsfb_fbAxisRX, jsfb_fbAxisRY,jsfb_fbAxisSX, jsfb_fbAxisSY,jsfb_Square)

multiJS_list = #( --multi dimensional array storing pointers to which face bones are used by which joystick. THESE ARE INDEXED TO MATCH THE FACIALJOYSTICKS STRUCT BELOW
-- **********************************************************************************************************************************************************************************************************************
-- **********************************************************************************************************************************************************************************************************************
-- I SHOULD TURN THIS INTO A STRUCT THEN I CAN DEFINE ON A JOYSTICK AND BONE BASIS IF WE WANT TO DO TRANS AND ROTATION.
-- **********************************************************************************************************************************************************************************************************************
-- **********************************************************************************************************************************************************************************************************************

	--this gets used by joystickMultipliers.ms
	#( --array of bones used by the Jaw joystick
		(jsfb_Settings jsfb_faceBone:faceBoneList[1][1] 	jsfb_Type:2 	jsfb_fbAxisTX:undefined 	jsfb_fbAxisTY:undefined 	 	jsfb_fbAxisRX:"Z" 	jsfb_fbAxisRY:"Y"    jsfb_fbAxisSX:undefined		jsfb_fbAxisSY:undefined 	jsfb_Square:2) -- Jaw

		)

)


----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------


-- jsName = name of joystick
-- jsShape -  1 = vertical 2 = square
--jsType - 1 = trans 2 = nonetrans
--jsPos = position of joysticks 'Rect' object
struct js_Params ( jsName, jsShape, jsType, jsPos )
	
facialJoysticks = #(--array of all joystic strcuts 
	(js_Params jsName:"CTRL_Jaw" jsShape:2 jsType:2 jsPos:[0,-16.5,0])
)

controlNames = #( --gets populated dynamically by the main user interface 'facial' window when the multipliers button is pressed.
	undefined	
)

lyrNames = #( --list of layers
		"FacialUI_Controls", 
		"FacialUI_Visuals"
		)	
		
runCleanUpFN = true

nodesToKill = #( --joystick nodes to remove
"Tongue",
"Tongue_In_Out",
"LookAT_Activator"	
)
