-- faceJoysticks.ms
-- Rockstar North
--Part of the facial rigging tools
--script which builds facial control joysticks
--Matt Rennie Sept 2010
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------


fn createFaceUI = 
(
	if faceType != "Human B-Rig" then
	(
		debugPrint ("Starting joystick generation")
		if $Ambient_UI == undefined do
		(
			debugPrint "Creating FacialUI..."
			textName = "Ambient_UI"
			FacialUI = text text:textName size:3.0 kerning:0
			FacialUI.name = textName
			rotate facialUI (angleaxis 90 [1,0,0])
			FacialUI.wireColor = Blue
				FacialUI.render_displayRenderMesh = true
				FacialUI.thickness = 0.3
				FacialUI.sides = 3
			
			headBoneObject = getNodeByName headBone
			FacialUI.pos = headBoneObject.pos + [0.25,0,0.17]
				addModifier FacialUI (EmptyModifier ()) 
								
			facialMultipliersCA = attributes FacialMultipliers
			(
				parameters main rollout:params
				(
					Zeroed type:#float ui:Zeroed default:0
				)
				
				rollout params "Facial Multipliers"
				(
					spinner Zeroed "Zero" type:#float
				)
			)

			custAttributes.add FacialUI.modifiers[#'Attribute holder'] facialMultipliersCA	

			visualsCA = attributes faceRigVisuals
			(
				parameters main rollout:rigVisuals
				(
					ShowLinks type:#Boolean ui:ShowLinks default:true
				)
				
				rollout rigVisuals "Facial Multipliers"
				(
					checkBox ShowLinks "Show Links" checked:true tooltip:"Togle facial skeleton links"
					
					on ShowLinks changed theState do			
					(
						if ShowLinks.state == true then
						(
							for hlpr in helpers do
							(
								if (substring (hlpr.name) 1 3) == "FB_" do
								(
									hlpr.showLinks = true
								)
							)
						)
						else
						(
							for hlpr in helpers do
							(
								if (substring (hlpr.name) 1 3) == "FB_" do
								(
									hlpr.showLinks = false
								)
							)
						)
					)
				)
			)
			
			custAttributes.add FacialUI.modifiers[#'Attribute holder'] visualsCA 	
			
			FacialUI.modifiers[#'Attribute_Holder'].Zeroed.controller = bezier_float ()
		)

		for joystick = 1 to facialJoysticks.count do
		(
			jpos = [0,0,0]
			currentJoystick = getNodeByName facialJoysticks[joystick].jsName

			jTag = ""
			
			if currentJoystick == undefined do
			(
				if (substring facialJoysticks[joystick].jsName 8 -1) == "Blink" do 
				(
					jTag = ("\r\n"+"translateTrackType=TRACK_VISEMES"+"\n")
				)

				debugPrint ("facialJoysticks["+(joystick as string)+"].jsName = "+(facialJoysticks[joystick].jsName))
				if facialJoysticks[joystick].jsShape == 1 then
				(
					if facialJoysticks[joystick] == "CTRL_R_Cheek" do 
					(
						messageBox "WHATDAFUCK!"
						print "WHATDAFUCK!"
						break()
					)
					debugPrint ("creating Vert joystick for "+facialJoysticks[joystick].jsName)				
					JoyStck = createJoystick facialJoysticks[joystick].jsName 2 jpos 1
					
					thisJsNode = getNodeByName facialJoysticks[joystick].jsName
					setUserPropbuffer thisJsNode ("tag= "+(thisJsNode.name)+jTag+"\r\n"+"exportTrans = true") --temp commented out track id				
					
					JoyStckRect = getNodeByName ("RECT_"+(substring facialJoysticks[joystick].jsName 6 50))					
					joystck = joystckrect
					JoyStck.scale = [1,1,1]
					
					JoyStck.parent = FacialUI
						
					debugPrint ("Parenting "+joystck.name+" to "+FacialUI.name)								
					in coordsys parent JoyStck.pos = facialJoysticks[joystick].jsPos
					debugPrint ("positioning "+Joystck.name+" to "+(facialJoysticks[joystick].jsPos as string))					
				) 
				else
				(
					debugPrint ("creating Square joystick for "+facialJoysticks[joystick].jsName)
					JoyStck = createJoystick facialJoysticks[joystick].jsName 1 jpos 1
					--now tag the joystick
					thisJsNode = getNodeByName facialJoysticks[joystick].jsName
	-- 				setUserPropbuffer thisJsNode ("tag= JS_"+(thisJsNode.name)+"\r\n"+"translateTrackType=TRACK_FACIAL_TRANSLATION"+"\r\n"+"exportTrans = true")
	-- 				setUserPropbuffer thisJsNode ("tag= JS_"+(thisJsNode.name)+"\r\n"+"exportTrans = true") --temp commented out track id
					setUserPropbuffer thisJsNode ("tag= "+(thisJsNode.name)+jTag+"\r\n"+"exportTrans = true") --temp commented out track id
					
					JoyStckRect = getNodeByName ("RECT_"+(substring facialJoysticks[joystick].jsName 6 50))					

					JoyStck = JoyStckRect
					JoyStck.parent = FacialUI
						debugPrint ("Parenting "+joystck.name+" to "+FacialUI.name)				
					JoyStck.scale = [1,1,1]
					in coordsys parent JoyStck.pos = facialJoysticks[joystick].jsPos
						debugPrint ("positioning "+Joystck.name+" to "+(facialJoysticks[joystick].jsPos as string))										
				)
			)
			
			if customTongueArray != undefined do 
			(
				if (substring facialJoysticks[joystick].jsName 1 11) == "CTRL_Tongue" do 
				(
					tongueRoot = getNodeByName "CTRL_Ear_Root" --we'll re-use the ear root node for custom tongue stuff
					
					if tongueRoot == undefined do 
					(
						tongueRoot = point pos:[0,0,0]
						tongueRoot.size = 0.1
						tongueRoot.transform = $SKEL_Head.transform
						tongueRoot.name = "CTRL_Ear_Root"
					)
					
					layer = layermanager.getLayerFromName "FacialUI_Visuals" 

					if layer != undefined do 
					(
						layer.addNode tongueRoot
					)
					
					jsCirc = getNodeByName facialJoysticks[joystick].jsName 
					jsRect = getNodeByName ("RECT"+(substring facialJoysticks[joystick].jsName 5 -1))
					jsText = getNodeByName ("TEXT"+(substring facialJoysticks[joystick].jsName 5 -1))
					
						delete jsRect
						delete jsText
						
					boneNo = substring geo.name 6 3
					tongueBone = getNodeByName (("FB_"+(substring facialJoysticks[joystick].jsName 6 -1)+"_"+boneNo))
						
					if tongueBone == undefined do 
					(
						
						print ("Couldn't find tonguebone "+(("FB_"+(substring facialJoysticks[joystick].jsName 6 -1)+"_"+boneNo)))
							messageBox ("Couldn't find tonguebone "+(("FB_"+(substring facialJoysticks[joystick].jsName 6 -1)+"_"+boneNo))) beep:true
								break()
					)

					currSel = selection as array
					clearSelection()
					select jsCirc
					freezeTransform()
					
					select currsel
					
					jsCirc.radius = 0.005
					jsCirc.thickness = 0.002
					
					--now we need to make a ear root node to zero control out for the expression
					jsCirParent = Point pos:[0,0,0] name:(jsCirc.name+"_ROOT") color:orange size:0.05 
					jsCirParent.centermarker = off
					jsCirParent.axistripod = off
					jsCirParent.Box = off
					jsCirParent.constantscreensize = off
					jsCirParent.drawontop = off
					jsCirParent.cross = on
					jsCirParent.transform = tongueBone.transform
					jsCirParent.parent = tongueRoot
					
					select jsCirParent
					freezeTransform()
					select jsCirc
					
					jsCirc.parent = jsCirParent
										
					jsCirc.rotation = tongueBone.rotation
					
					in coordsys parent jsCirc.position = [0,0.09,0]
						
					jsCirc.pivot = tongueBone.pivot
					--ok we'll now make a node with matching transforms to the jscirc so we can snap back to it
					
					sPos = in coordsys world jsCirc.position
					sRot = in coordsys world jsCirc.rotation

					freezeTransform()	
				)
			)
			
			if (substring facialJoysticks[joystick].jsName 8 -1) == "Ear" do 
			(
				earRoot = getNodeByName "CTRL_Ear_Root"
				
				if earRoot == undefined do 
				(
					earRoot = point pos:[0,0,0]
					earRoot.size = 0.1
					earRoot.transform = $SKEL_Head.transform
					earRoot.name = "CTRL_Ear_Root"
				)
				
				layer = layermanager.getLayerFromName "FacialUI_Visuals" 

				if layer != undefined do 
				(
					layer.addNode earRoot
				)


				
				jsCirc = getNodeByName facialJoysticks[joystick].jsName 
				jsRect = getNodeByName ("RECT"+(substring facialJoysticks[joystick].jsName 5 -1))
				jsText = getNodeByName ("TEXT"+(substring facialJoysticks[joystick].jsName 5 -1))
				
					delete jsRect
					delete jsText
					
				side = substring facialJoysticks[joystick].jsName 6 1
				
				boneNo = substring geo.name 6 3
				earBone = getNodeByName (("FB_"+side+"_Ear_"+boneNo) as string)
					
				if earBone == undefined then
				(
					messagebox ("Couldn't find "+("FB_"+side+"_Ear_"+boneNo))
						print ("Couldn't find "+("FB_"+side+"_Ear_"+boneNo))
							break()
				)
				else
				(
					currSel = selection as array
					clearSelection()
					select jsCirc
					freezeTransform()
					
					select currsel
					
					jsCirc.radius = 0.005
					jsCirc.thickness = 0.002
					
					--now we need to make a ear root node to zero control out for the expression
					jsCirParent = Point pos:[0,0,0] name:(jsCirc.name+"_ROOT") color:orange size:0.05 
					jsCirParent.centermarker = off
					jsCirParent.axistripod = off
					jsCirParent.Box = off
					jsCirParent.constantscreensize = off
					jsCirParent.drawontop = off
					jsCirParent.cross = on
					jsCirParent.transform = earBone.transform
					jsCirParent.parent = earRoot
					
					select jsCirParent
					freezeTransform()
					select jsCirc
					
					jsCirc.parent = jsCirParent
										
					jsCirc.rotation = earBone.rotation
					
					
					in coordsys parent jsCirc.position = [(earBone.length + (earBone.Length / 6)),0,0]
						
					jsCirc.pivot = earBone.pivot
					--ok we'll now make a node with matching transforms to the jscirc so we can snap back to it
					
					sPos = in coordsys world jsCirc.position
					sRot = in coordsys world jsCirc.rotation

					freezeTransform()
					
					
					--now need to add float limits to x y and z 
					
-- messagebox ("setting "+jsCirc.name+" pos to float limit") beep:true
					jsCirc.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller = float_limit ()
					jsCirc.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller = float_limit ()
					jsCirc.pos.controller.Zero_Pos_XYZ.controller.Z_Position.controller = float_limit ()
-- messagebox ("setting "+jsCirc.name+" X pos: "+(jsCirc.pos.controller.Zero_Pos_XYZ.controller.X_Position.controller as string)) beep:true					
	-- 				in coordsys world jsCirc.rotation = sRot
	-- 				in coordsys world jsCirc.position = sPos
				
	-- 				select jsCirc
	-- 				freezeTransform()
	-- 				select currsel
				)
			)
		)
		if FacialUI == undefined do
		(
			FacialUI = getNodeByName "Facial UI"
		)
		FacialUI.scale = [0.02,0.02,0.02]
		hasMult = 4
		
		earRoot = getNodeByName "CTRL_Ear_Root"
		
		if earRoot != undefined do 
		(
			earRoot.parent = $Ambient_UI
		)
	)
	else
	(
		--ok we need to merge in the 3lateral style gui and repopulate the variables via getNodeByName
		mergemaxFile ThreeLateralGuiFile #alwaysReparent #deleteOldDups #useSceneMtlDups quiet:true
		
		--now rename the 3L joysticks
		
		for i = 1 to ThreeLatJSNaming.count do 
		(
			jsNode = getNodeByName ThreeLatJSNaming[i][1]
			jsNode.name = ThreeLatJSNaming[i][2]
		)
	)
)