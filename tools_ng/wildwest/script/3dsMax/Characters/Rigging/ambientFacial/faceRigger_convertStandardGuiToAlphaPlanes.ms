--convert 3LateralGUI to Rockstar alpha plane version

--for every item in the 3 lateral gui shit align the rs version to them, 
--delete their geo, then attach the RS_ version

filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
clearListener()
----------------------------------------------
----------------------------------------------

guiNodeParentPairings = #( --id1 = std, id2 = alphad
	#("Ambient_UI", "RS_Facial_UI_TEXT"),
	#("FaceFX", "RS_FaceFX_TEXT")
	)

ambientGui = #(
#("Ambient_UI", "RS_Facial_UI_TEXT"),
#("FaceFX", "RS_FaceFX_TEXT"),
#("RECT_UpperLip_Curl","RS_UpperLip_Curl_FRAME"),
#("TEXT_UpperLip_Curl","RS_UpperLip_Curl_TEXT"),
#("CTRL_UpperLip_Curl","RS_UpperLip_Curl_CTRL"),
#("RECT_LowerLip_Curl","RS_LowerLip_Curl_FRAME"),
#("TEXT_LowerLip_Curl","RS_LowerLip_Curl_TEXT"),
#("CTRL_LowerLip_Curl","RS_LowerLip_Curl_CTRL"),
#("RECT_Tongue_In_Out","RS_Tongue_In_Out_FRAME"),
#("TEXT_Tongue_In_Out","RS_Tongue_In_Out_TEXT"),
#("CTRL_Tongue_In_Out","RS_Tongue_In_Out_CTRL"),
#("RECT_C_Brow","RS_C_Brow_FRAME"),
#("TEXT_C_Brow","RS_C_Brow_TEXT"),
#("CTRL_C_Brow","RS_C_Brow_CTRL"),
#("RECT_R_Brow","RS_R_Brow_FRAME"),
#("TEXT_R_Brow","RS_R_Brow_TEXT"),
#("CTRL_R_Brow","RS_R_Brow_CTRL"),
#("RECT_L_Brow","RS_L_Brow_FRAME"),
#("TEXT_L_Brow","RS_L_Brow_TEXT"),
#("CTRL_L_Brow","RS_L_Brow_CTRL"),
#("RECT_R_Eye","RS_R_Eye_FRAME"),
#("TEXT_R_Eye","RS_R_Eye_TEXT"),
#("CTRL_R_Eye","RS_R_Eye_CTRL"),
#("RECT_L_Eye","RS_L_Eye_FRAME"),
#("TEXT_L_Eye","RS_L_Eye_TEXT"),
#("CTRL_L_Eye","RS_L_Eye_CTRL"),
#("RECT_R_Mouth","RS_R_Mouth_FRAME"),
#("TEXT_R_Mouth","RS_R_Mouth_TEXT"),
#("CTRL_R_Mouth","RS_R_Mouth_CTRL"),
#("RECT_Mouth","RS_Mouth_FRAME"),
#("TEXT_Mouth","RS_Mouth_TEXT"),
#("CTRL_Mouth","RS_Mouth_CTRL"),
#("RECT_L_Mouth","RS_L_Mouth_FRAME"),
#("TEXT_L_Mouth","RS_L_Mouth_TEXT"),
#("CTRL_L_Mouth","RS_L_Mouth_CTRL"),
#("RECT_UpperLip","RS_UpperLip_FRAME"),
#("TEXT_UpperLip","RS_UpperLip_TEXT"),
#("CTRL_UpperLip","RS_UpperLip_CTRL"),
#("RECT_LowerLip","RS_LowerLip_FRAME"),
#("TEXT_LowerLip","RS_LowerLip_TEXT"),
#("CTRL_LowerLip","RS_LowerLip_CTRL"),
#("RECT_Jaw","RS_Jaw_FRAME"),
#("TEXT_Jaw","RS_Jaw_TEXT"),
#("CTRL_Jaw","RS_Jaw_CTRL"),
#("RECT_Tongue","RS_Tongue_FRAME"),
#("TEXT_Tongue","RS_Tongue_TEXT"),
#("CTRL_Tongue","RS_Tongue_CTRL"),

#("RECT_L_Blink","RS_Blink_Left_FRAME"),
#("TEXT_L_Blink","RS_Blink_Left_TEXT"),
#("CTRL_L_Blink","RS_Blink_Left_CTRL"),
#("RECT_R_Blink","RS_Blink_Right_FRAME"),
#("TEXT_R_Blink","RS_Blink_Right_TEXT"),
#("CTRL_R_Blink","RS_Blink_Right_CTRL"),


#("RECT_L_Cheek","RS_Cheek_Up_Left_FRAME"),
#("TEXT_L_Cheek","RS_Cheek_Up_Left_TEXT"),
#("CTRL_L_Cheek","RS_Cheek_Up_Left_CTRL"),
#("RECT_R_Cheek","RS_Cheek_Up_Right_FRAME"),
#("TEXT_R_Cheek","RS_Cheek_Up_Right_TEXT"),
#("CTRL_R_Cheek","RS_Cheek_Up_Right_CTRL"),



#("RECT_open","RS_open_FRAME"),
#("TEXT_open","RS_open_TEXT"),
#("CTRL_open","RS_open_CTRL"),
#("open","RS_open_CTRL"),
#("RECT_W","RS_W_FRAME"),
#("TEXT_W","RS_W_TEXT"),
#("CTRL_W","RS_W_CTRL"),
#("W","RS_W_CTRL"),
#("RECT_ShCh","RS_ShCh_FRAME"),
#("TEXT_ShCh","RS_ShCh_TEXT"),
#("CTRL_ShCh","RS_ShCh_CTRL"),
#("ShCh","RS_ShCh_CTRL"),
#("RECT_PBM","RS_PBM_FRAME"),
#("TEXT_PBM","RS_PBM_TEXT"),
#("CTRL_PBM","RS_PBM_CTRL"),
#("PBM","RS_PBM_CTRL"),
#("RECT_FV","RS_FV_FRAME"),
#("TEXT_FV","RS_FV_TEXT"),
#("CTRL_FV","RS_FV_CTRL"),
#("FV","RS_FV_CTRL"),
#("RECT_wide","RS_wide_FRAME"),
#("TEXT_wide","RS_wide_TEXT"),
#("CTRL_wide","RS_wide_CTRL"),
#("wide","RS_wide_CTRL"),
#("RECT_tBack","RS_tBack_FRAME"),
#("TEXT_tBack","RS_tBack_TEXT"),
#("CTRL_tBack","RS_tBack_CTRL"),
#("tBack","RS_tBack_CTRL"),
#("RECT_tTeeth","RS_tTeeth_FRAME"),
#("TEXT_tTeeth","RS_tTeeth_TEXT"),
#("CTRL_tTeeth","RS_tTeeth_CTRL"),
#("tTeeth","RS_tTeeth_CTRL"),
#("RECT_tRoof","RS_tRoof_FRAME"),
#("TEXT_tRoof","RS_tRoof_TEXT"),
#("CTRL_tRoof","RS_tRoof_CTRL"),
#("tRoof","RS_tRoof_CTRL"),
-- #("RECT_FFX_Eyebrow_Raise_Left","RS_Eyebrow_Raise_Left_FRAME"),
-- #("TEXT_FFX_Eyebrow_Raise_Left","RS_Eyebrow_Raise_Left_TEXT"),
-- #("FFX_CTRL_Eyebrow_Raise_Left","RS_Eyebrow_Raise_Left_CTRL"),
-- #("RECT_FFX_Eyebrow_Raise_Right","RS_Eyebrow_Raise_Right_FRAME"),
-- #("TEXT_FFX_Eyebrow_Raise_Right","RS_Eyebrow_Raise_Right_TEXT"),
-- #("FFX_CTRL_Eyebrow_Raise_Right","RS_Eyebrow_Raise_Right_CTRL"),
-- #("RECT_FFX_Snarl_Left","RS_Snarl_Left_FRAME"),
-- #("TEXT_FFX_Snarl_Left","RS_Snarl_Left_TEXT"),
-- #("FFX_CTRL_Snarl_Left","RS_Snarl_Left_CTRL"),
-- #("RECT_FFX_Snarl_Right","RS_Snarl_Right_FRAME"),
-- #("TEXT_FFX_Snarl_Right","RS_Snarl_Right_TEXT"),
-- #("FFX_CTRL_Snarl_Right","RS_Snarl_Right_CTRL")

#("RECT_AU2_Outer_Brow_Raiser_Right","RS_AU2_Outer_Brow_Raiser_Right_FRAME"),
#("CTRL_AU2_Outer_Brow_Raiser_Right","RS_AU2_Outer_Brow_Raiser_Right_CTRL"),
#("AU2_Outer_Brow_Raiser_Right","RS_AU2_Outer_Brow_Raiser_Right_CTRL"),
#("TEXT_AU2_Outer_Brow_Raiser_Right","RS_AU2_Outer_Brow_Raiser_Right_TEXT"),
#("RECT_AU23_Lip_Tightener","RS_AU23_Lip_Tightener_FRAME"),
#("CTRL_AU23_Lip_Tightener","RS_AU23_Lip_Tightener_CTRL"),
#("AU23_Lip_Tightener","RS_AU23_Lip_Tightener_CTRL"),
#("TEXT_AU23_Lip_Tightener","RS_AU23_Lip_Tightener_TEXT"),
#("RECT_AU10_Upper_Lip_Raiser","RS_AU10_Upper_Lip_Raiser_FRAME"),
#("CTRL_AU10_Upper_Lip_Raiser","RS_AU10_Upper_Lip_Raiser_CTRL"),
#("AU10_Upper_Lip_Raiser","RS_AU10_Upper_Lip_Raiser_CTRL"),
#("TEXT_AU10_Upper_Lip_Raiser","RS_AU10_Upper_Lip_Raiser_TEXT"),
#("RECT_AU15_Lip_Corner_Depressor_Left","RS_AU15_Lip_Corner_Depressor_Left_FRAME"),
#("CTRL_AU15_Lip_Corner_Depressor_Left","RS_AU15_Lip_Corner_Depressor_Left_CTRL"),
#("AU15_Lip_Corner_Depressor_Left","RS_AU15_Lip_Corner_Depressor_Left_CTRL"),
#("TEXT_AU15_Lip_Corner_Depressor_Left","RS_AU15_Lip_Corner_Depressor_Left_TEXT"),
#("RECT_AU26_Jaw_Drop","RS_AU26_Jaw_Drop_FRAME"),
#("CTRL_AU26_Jaw_Drop","RS_AU26_Jaw_Drop_CTRL"),
#("AU26_Jaw_Drop","RS_AU26_Jaw_Drop_CTRL"),
#("TEXT_AU26_Jaw_Drop","RS_AU26_Jaw_Drop_TEXT"),
#("RECT_AU20_Lip_Stretcher","RS_AU20_Lip_Stretcher_FRAME"),
#("CTRL_AU20_Lip_Stretcher","RS_AU20_Lip_Stretcher_CTRL"),
#("AU20_Lip_Stretcher","RS_AU20_Lip_Stretcher_CTRL"),
#("TEXT_AU20_Lip_Stretcher","RS_AU20_Lip_Stretcher_TEXT"),
#("RECT_AU15_Lip_Corner_Depressor_Right","RS_AU15_Lip_Corner_Depressor_Right_FRAME"),
#("CTRL_AU15_Lip_Corner_Depressor_Right","RS_AU15_Lip_Corner_Depressor_Right_CTRL"),
#("AU15_Lip_Corner_Depressor_Right","RS_AU15_Lip_Corner_Depressor_Right_CTRL"),
#("TEXT_AU15_Lip_Corner_Depressor_Right","RS_AU15_Lip_Corner_Depressor_Right_TEXT"),
#("RECT_AU16_Lower_Lip_Depressor","RS_AU16_Lower_Lip_Depressor_FRAME"),
#("CTRL_AU16_Lower_Lip_Depressor","RS_AU16_Lower_Lip_Depressor_CTRL"),
#("AU16_Lower_Lip_Depressor","RS_AU16_Lower_Lip_Depressor_CTRL"),
#("TEXT_AU16_Lower_Lip_Depressor","RS_AU16_Lower_Lip_Depressor_TEXT"),
#("RECT_AU4_Brow_Lowerer","RS_AU4_Brow_Lowerer_FRAME"),
#("CTRL_AU4_Brow_Lowerer","RS_AU4_Brow_Lowerer_CTRL"),
#("AU4_Brow_Lowerer","RS_AU4_Brow_Lowerer_CTRL"),
#("TEXT_AU4_Brow_Lowerer","RS_AU4_Brow_Lowerer_TEXT"),
#("RECT_AU2_Outer_Brow_Raiser_Left","RS_AU2_Outer_Brow_Raiser_Left_FRAME"),
#("CTRL_AU2_Outer_Brow_Raiser_Left","RS_AU2_Outer_Brow_Raiser_Left_CTRL"),
#("AU2_Outer_Brow_Raiser_Left","RS_AU2_Outer_Brow_Raiser_Left_CTRL"),
#("TEXT_AU2_Outer_Brow_Raiser_Left","RS_AU2_Outer_Brow_Raiser_Left_TEXT")
	
	)
	
rsGUIFile = (theProjectRoot + "art/peds/FaceFX/ambientFaceFXGUITwo.max")


fn convertStandardGuiToAlphaGUI = 
(
	mergeMaxFile rsGUIFile quiet:true

	--first we'll align the parent nodes
	stdPar = getNodeByName guiNodeParentPairings[1][1]
	alpPar = getNodeByName guiNodeParentPairings[1][2]
	
	if stdPar != undefined do 
	(
		if alpPar != undefined do
		(
			alpPar.transform = stdPar.transform	

			stdPar = getNodeByName guiNodeParentPairings[2][1]
			alpPar = getNodeByName guiNodeParentPairings[2][2]
			
			if stdPar != undefined do
			(
				if alpPar != undefined do 
				(
					alpPar.transform = stdPar.transform	
					
				-- 	disableSceneRedraw()
					max modify mode
				-- 	setCommandPanelTaskMode #create
					
					CreateDialog progBar width:300 Height:30
						progBar.prog.color = white
					for sG = 1 to ambientGui.count do 
					(
						
						stdGUI = getNodeByName ambientGui[sG][1]
						
-- 						if ambientGui[sG][1] == "RECT_Blink_Left"
				-- 		debugPrint ("Looking for "+ambientGui[sG][1])
						if stdGui != undefined do 
						(
					
							alpGui = getNodeByName ambientGui[Sg][2]
							alpName = ambientGui[Sg][2]
							
							if alpGUI != undefined do
							(

									alpGui.transForm = stdGui.transform

								select stdGui
								
								--need to ensure we maintain existing attr holders
								
								if stdGui.modifiers.count > 0 then
								(
									modCount = stdGUI.modifiers.count
									val = (modCount - 1)
									if val < 2 then
									(
										modPanel.setCurrentObject stdGui.baseObject
										modPanel.addModToSelection (Edit_Poly ()) ui:on
										maxOps.CollapseNodeTo stdGui 2 off
									)
									else
									(
										modPanel.setCurrentObject stdGui.modifiers[val]
										modPanel.addModToSelection (Edit_Poly ()) ui:on
										maxOps.CollapseNodeTo stdGui (modCount - 1) off
									)
										
									modPanel.setCurrentObject stdGui.baseObject
								)
								else
								(
									convertTo stdGui PolyMeshObject
								)
								subobjectLevel = 4
								max select all
								
								max delete
								--now we can attach the alpha plane
								stdGui.EditablePoly.attach alpGUI stDGUI
								subobjectLevel = 0
								
								debugdebugPrint ("Alpha'd "+stdGui.name)
							)
-- 							else
-- 							(
-- 								debugdebugPrint ("Couldn't find RS node "+alpName)
-- 								--break()
-- 							)
						)
-- 						else
-- 						(
-- 							debugPrint ("Couldn't find "+ ambientGui[sG][1])
-- 							--break()
-- 						)
						progBar.prog.value = (100.*sG/ambientGui.count)
					)
					DestroyDialog progBar
				)
-- 				else
-- 				(
-- 					debugPrint ("Couldn't find "+guiNodeParentPairings[2][2])
-- 				)
			)
-- 			else
-- 			(
-- 				debugPrint ("Couldn't find "+guiNodeParentPairings[2][1])
-- 			)
		)
-- 		else
-- 		(
-- 			debugPrint ("Couldn't find "+guiNodeParentPairings[1][2])
-- 		)
	)
-- 	else
-- 	(
-- 		debugPrint ("Couldn't find "+guiNodeParentPairings[1][1])
-- 	)
)


if classOf $Ambient_UI == SplineShape then
(
	start = timestamp()
	Print ("Converting GUI to alpha's")
	convertStandardGuiToAlphaGUI()
	end = timeStamp()
	format "Processing alphaGuiConversion took % seconds\n" ((end - start) / 1000.0)
)
else
(
	Print ("Skipping alpha conversion.")
)





