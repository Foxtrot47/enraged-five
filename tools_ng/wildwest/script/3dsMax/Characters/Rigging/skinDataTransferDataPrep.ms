-- tool to prepare template mesh data for skinDataTransferPrep.ms
-- dec 2012
-- Matt Rennie

filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

templateMeshFile = (theProjectRoot + "art/peds/AmbientHead/templateHead.max")
templateBoneFile = (theProjectRoot + "art/peds/AmbientHead/templateMesh.bon")
templateSkinFile = (theProjectRoot + "art/peds/AmbientHead/templateMesh.env")

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn LSW_EnvelopeCallbackFunction =
(
	WindowHandle = DialogMonitorOPS.GetWindowHandle()
	theDialogName = UIAccessor.GetWindowText WindowHandle

	if theDialogName != undefined and matchpattern theDialogName pattern:"*Load Envelopes*" do
		UIAccessor.PressButtonByName WindowHandle "Match by Name"	
		
	if theDialogName != undefined and matchpattern theDialogName pattern:"*Load Envelopes*" do
		UIAccessor.PressButtonByName WindowHandle "OK"

	true
)

fn applySkinning geo templateSkinFile = 
(
	DialogMonitorOPS.RegisterNotification LSW_EnvelopeCallbackFunction ID:#ANoon_Envelopes
	DialogMonitorOPS.Enabled = true
	
	completeRedraw()
	skinOps.loadEnvelope geo.modifiers[#Skin] templateSkinFile
-- 	skinOps.loadEnvelopeAsASCII geo.modifiers[#Skin] envFile
	completeRedraw()

	DialogMonitorOPS.Enabled = false
	DialogMonitorOPS.UnRegisterNotification ID:#ANoon_Envelopes	
	
	
	print ("Loaded Skin: "+templateSkinFile+" onto "+geo.name)
)

fn inputBones obj = 
(
	boneList = #()
	--*************************************************************************************
	-- NEED TO ADD A DISABLE UI HERE OTHERWISE IT TAKES AGES!
	--*************************************************************************************
	
	Geo = selection
	if Geo.count == 1 then
	(
		
		g = obj
		if g.modifiers[#Skin] == undefined then
		(
			messageBox "Please pick an object with a skin modifier." title:"gta5_boneinput"
		)
		else
		(
			skinMod = $.modifiers[#Skin]
			if input_name == undefined do
			(
				input_name = templateBoneFile
			)

			if input_name != undefined then
			(
			f = openfile input_name
			inputData = #() -- define as array
			  while not eof f do
			  (
			   append inputData (filterstring (readLine f) ",")
			  )
			  close f

				for i in inputData do
				(
					currBonename = (i as string)
					currBoneLength = currBonename.count
					currBone = (substring currBonename 4 (currBonelength - 5))
-- 						executeStr = ("if "+currBone+" != undefined do ("+"select "+currBone+")")
-- 						currentBone = execute executeStr
-- 						currentBone = execute currBone
						currentBone = getNodeByName currBone
-- 						if currentBone != undefined do
						if currentBone != undefined then	
						(
							appendIfUnique boneList currentBone
						)
						else
						(
							print ("Couldn't find "+currBone+" in this scene.")
						)
				)
-- 				print ("BoneList = " +(boneList as String))
				
					select g
					SetCommandPanelTaskMode mode:#modify
				
				for i = 1 to boneList.count do
				(

					boneToAdd = getNodeByName bonelist[i].name
					
					boneUpdateInt = undefined
					if i != bonelist.count then
					(
						boneUpdateInt = 1
					)
					else
					(
						boneUpdateInt = 0
					)
					
					if boneToAdd != undefined do
					(
						skinOps.addbone g.modifiers[#Skin] boneList[i] boneUpdateInt
					)
				)
			)
		)

	)
	else
	(
	messageBox "Please run with only 1 object selected." title:"gta5_boneinput"
	)
)

fn outputBones obj = 
(
	boneList = #()
	max modify mode
	Geo = selection
	if Geo.count == 1 then
	(
	skinMod = obj.modifiers[#Skin]
	totalSkinModBones = skinOps.getNumberBones skinMod

		if totalSkinModBones > 0 then
		(
			--output_name = getSaveFileName caption:"SpeedSheet File" types:"SpeedSheet (*.ssh)|*.ssh|All Files (*.*)|*.*|"

			output_name = templateBoneFile
	-- 		
			if output_name != undefined then 
			(
				output_file = createfile output_name		

				for i = 1 to totalSkinModBones do
				(
					thebonename = skinOps.getBoneName skinMod  i 1
					append boneList theBoneName
					format ((theBoneName as string)+"\n") to:output_file
				)
				
				close output_file
					--edit output_name --opens the file in a maxscript window so it can be checked.
				
			)--end if
		)--end if
	)
	else
	(
	messageBox "Please run with only 1 object selected." title:"gta5_boneOutput"
	)
)

fn editp4file filenameString = 
(
	gRsPerforce.add_or_edit #( filenameString ) silent:true queueAdd:false
)

fn skinDataTransferDataPrep obj = 
(
	--first off we'll check out the template data
	editp4file templateMeshFile 
	editp4file templateBoneFile 
	editp4file templateSkinFile
	
	initHeadName = obj.name
	initDummyName = obj.parent.name
	
	objNumber = substring obj.name 6 3
	objNumberInt = objNumber as integer
	
	if objNumberInt == undefined then 
	(
		messagebox ("Warning could not define head number. Cannot continue") beep:true
	)
	else
	(
		print ("objNumber: "+objNumber)
		--now we know the head number we need to find the bones matching that component
		boneArray = #()
		
		for o in objects do 
		(
			if substring o.name 1 2 == "FB" do 
			(
				nameLength = o.name.count
				if (substring o.name (nameLength - 2) -1) == objNumber do 
				(
					appendIfUnique boneArray o
				)
			)
		)
		
		--now we can rename the contents of boneArray to have TEMPLATE instead of objNumber
		
		for o in boneArray do 
		(
			thisName = o.name
			--print ("Init: "+thisName)
			nameLength = o.name.count
			thisName = replace thisName (nameLength - 2) 3 "TEMPLATE"
			--print ("Final: "+thisName)
			o.name = thisName
		)
		
		--now we need to rename the head mesh and its parent 
		obj.name = "templateMesh"
		obj.parent.name = "templateHead"
		
		objsToSave = #(
			obj,
			obj.parent
			)
		
		--now we need to save out a bone file, env file and mesh file
		outputBones obj
		skinMod = obj.modifiers[#Skin]
		skinOps.saveEnvelope skinMod templateSkinFile

		--now we need to delete the skin modifier and save out the mesh and dummy
		for modi = obj.modifiers.count to 1 by -1 do 
		(
			deleteModifier obj modi
		)
		
		saveNodes objsToSave templateMeshFile quiet:true
		
		--now we need to reload the skinning
		modPanel.addModToSelection (Skin ()) ui:on
		
		inputBones obj			
		
		applySkinning obj templateSkinFile	
			
		--now we need to rename the head mesh and its parent back again
		obj.name = initHeadName
		obj.parent.name = initDummyName
		
		-- now we rename the joints back to the pre TEMPLATE version
		
		for o in boneArray do 
		(
			thisName = o.name
			--print ("Init: "+thisName)
			nameLength = o.name.count
			thisName = replace thisName (nameLength - 7) 8 objNumber
			--print ("Final: "+thisName)
			o.name = thisName
		)	

		modPanel.setCurrentObject obj.baseObject	
		modPanel.setCurrentObject obj.modifiers[#Skin]		
	)
)

sela = selection as array

if sela.count == 1 then
(
	skinDataTransferDataPrep $
)
else
(
	messagebox ("Please run with your head mesh selected") beep:true
)