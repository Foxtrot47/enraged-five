-- rigTools.ms
-- a little floater gui to bring commonly used tools together in one place
--Matt Rennie
--Nov 2010

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()




-- filein (RsConfigGetProjRootDir() + "tools/dcc/current/max2012/scripts/pipeline/helpers/rigging/delUnusedBones.ms")

-- pedTypeFile = (RsConfigGetProjRootDir() + "tools/etc/config/characters/ped_type_list.xml")
pedTypeFile = (RsConfigGetToolsDir() + "etc/config/characters/ped_type_list.xml")
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn determineReducedSkelChar dummyObj = 
(
	charName = dummyObj.name
	
	splitSkelVal = false
		
	inputData = #()	
	
	if pedTypeFile != undefined then
	(
		f = openfile pedTypeFile
-- 		inputData = #() -- define as array
		  while not eof f do
		  (
		   append inputData (filterstring (readLine f) "\n")
		  )
		  close f
	)
	print "File parsed."

	for i in inputData do 
	(
		if findString (i as string) ("<file asset_name="+"\""+charName+"\"") != undefined do 
		(
			--ok we found an entry for this
			
			if findstring (i as string) ("splitSkel="+"\""+"true"+"\"") != undefined do 
			(
				splitSkelVal = true
			)
		)
	)
	
	
	return splitSkelVal
)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- if ((rigToolsGUI != undefined) and (rigToolsGUI.isDisplayed)) do
-- 	(destroyDialog rigToolsGUI)

runFromRigToolsPallette = true
rigToolsFileName  = undefined

-- if ((theRigGuiFloater != undefined) and (theRigGuiFloater.isDisplayed)) do
-- 	(destroyDialog theRigGuiFloater)

rollout skeletonRollout "Skel Tools"
(
	button btnBuildRig "Build Rig Markers" width:110 tooltip:"Build ped rig from markers"
	button btnGigantique "Gigantique" width:110 tooltip:"Prepare giant based skeleton from ROM data."
	button btnMrSkel "Mr Skeleton" width:110 tooltip:"Create skeletons from presets"
	button btnSkeletonTransfer "Skel Transfer" width:110 tooltip:"Skinwrapper and skeleton update tool"
	button btnAddIK "Add IK Bones" width:110 tooltip:"Add IK nodes to hands."
	
	on btnMrSkel pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/mrSkeleton/mrSkeleton.ms")
	)	
	
	on btnSkelSpot pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/Rigging_tools/skelSpot.ms")	
	)	
	
	on btnSkeletonTransfer pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/Body_Transfer_Tool.ms")
	)	

	on btnGigantique pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/giant/gigantique.ms")
	)	

	on btnBuildRig pressed do
	(
		
		filein (RsConfigGetWildWestDir() + "/script/3dsMax/Characters/Rigging/Build_Ped_Rig_from_Markers.ms")
	)	
	
	on btnAddIK pressed do
	(
		
		filein (RsConfigGetWildWestDir() + "/script/3dsMax/Characters/Rigging/addIKHandBones.ms")
	)	
)	

rollout facialToolsRollout "Facial Tools"
(
	button btn3LatFacial "3Lateral Facial" width:110 tooltip:"Outsource facial connection tool."
	button btnRemoveOldAmbientRigData "Remove old rig" width:110 tooltip:"Remove Ambient rig data."
	button btnAmbientFace "Ambient Facial" width:110 tooltip:"Generate FaceFX ambient facial rig."
	button btnSuckTheCattOff "Suck the Catt off" width:110 tooltip:"Save/Load custom attribute data for ambient rigs."
	button btnMergeTemplateHead "Template Head" width:110 tooltip:"Merge template head for skinning comparisons"
	button btnRemoveTemplateHead "Del Template Head" width:110 tooltip:"Remove template head"
	
	on btn3LatFacial pressed do
	(
		runFromRigToolsPallette = true
		rigToolsFileName = (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/3Lateral/threeLateralFacialConnect.ms")	
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/3Lateral/threeLateralFacialConnect.ms")	
	)	

	on btnRemoveOldAmbientRigData pressed do 
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/ambientFacial/removeOldRigData.ms")
	)
	
	on btnAmbientFace pressed do
	(
		runFromRigToolsPallette = true
		rigToolsFileName = (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/ambientFacial/faceRigger_facial_tools_ui.ms")
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/ambientFacial/faceRigger_facial_tools_ui.ms")
	)	
	
	on btnSuckTheCattOff pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/ambientFacial/suckTheCattOff.ms")
	)		
	
	on btnMergeTemplateHead pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/ambientFacial/mergeGuideHead.ms")
	)	

	on btnRemoveTemplateHead pressed do 
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/ambientFacial/deleteTemplateData.ms")
	)
)

rollout expressionToolsRollout "Expressions"
(
	button btnCreateExpressions "Create Expressions" width:110 tooltip:"Expression creation GUI"
	button btnExprInOut "Expr. Export" width:110 tooltip:"Export Expressions to xml."	
	button btnExpressionSet "Expr Set" width:110 tooltip:"Create *Expressions selection set."	
	button btnPerCompExprPrep "Prep per comp." width:110 tooltip:"Prep Per Component Expressions selection set."	
	
	on btnCreateExpressions pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/Rigging_tools/c_expressionCreator.ms")	
	)

	on btnExprInOut pressed do 
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/ExpressionExporter/expressionGUI.ms")
	)	
	
	on btnExpressionSet pressed do 
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/findExpressionObjects.ms")
		print ("Expression set Created!")
	)
	
	on btnPerCompExprPrep pressed do 
	(	
		--first we'll strip the skinning on all objects
		--filein (RsConfigGetProjRootDir() + "tools/dcc/current/max2012/scripts/pipeline/helpers/rigging/delUnusedBones.ms")
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/findExpressionObjects.ms")
		print ("Expression set Created!")
		
		reduceSkel = false
		
		if selection.count == 1 then
		(
			if classof $ == Dummy then
			(
				dummyObj = selection[1]
				redSkel = determineReducedSkelChar dummyObj
				
				objArray = #()
				
				clearSelection()
				
				renderMeshNames = #() --names of all
				clothMeshNodes =#()
				
				--first we need to find any sim meshes for cloth then once we have foudn them we know what the render meshes are
				--we can then use this to prevent these meshes from having their skin modifiers reduced.
				
-- 				for i in dummyObj.children do 
-- 					for i = dummyObj.children.count to 1 by -1 do 
-- 					(
-- 	-- 					if (substring i.name 1 8) == "CLOTHSIM" do 
-- 	-- 					(	
-- 	-- 						renderMeshName = (substring i.name 10 -1)
-- 	-- 						append renderMeshNames renderMeshName --store the name of the meshes that are render meshes for the clothsims for unparenting later
-- 	-- 						
-- 	-- 						append clothMeshNodes i
-- 	-- 					)				
-- 						
-- 						thisObj = dummyObj.children[i]
-- 						if (substring thisObj.name 1 8) == "CLOTHSIM" do 
-- 						(	
-- 							deselect thisObj
-- 						)									
-- 						
-- 					)
				
-- 				objArray = selection as array
				
				for i in renderMeshNames do --now find the cloth rendermeshes
				(
					print ("Looking for "+i)
					thisMesh = getnodeByName i
-- 					thisMesh.parent = undefined
					append clothMeshNodes thisMesh
				)
				
				for i in dummyObj.children do 
				(
					if substring i.name 1 8 != "CLOTHSIM" do 
					(
						appendIfUnique objArray i
					)
				)
				
				select objArray
				
				--now we need to ask if this is a reduce skel character - if yes we need to strip joints also
	-- 			if queryBox "Is this a reduced skeleton character?" beep:true do
				if redSkel == true do
				(

					--now we need to temporarily remove and cloth render meshes from the bone deletion phase
					
					local expSel = selection as array
					
					--now deselect any of the cloth mesh nodes we foudn above
					for obj in clothMeshNodes do 
					(
						if obj != undefined do
						(
							deselect obj
						)
					)
						
					format ("Character deemed to be reduced skeleton."+"\n")
					
					filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/usefulScripts/mjrSelDeleteUnusedBones.ms")
					
				)
							
				filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/usefulScripts/mjrSelDeleteUnusedBones.ms")
				
				select objArray
				

-- 				--now we'll create the per component expressions set	
				filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/usefulScripts/selectComponentExpressionObjects.ms")
				
				expSel= selectionSets["*ExpressionObjects"]

				select expSel
			)
			else
			(
				messagebox "Please select the character dummy" beep:true title:"Selection error"
			)
		)
		else
		(
			messagebox "Please select the character dummy" beep:true title:"Selection error"
		)
	)
		
)

rollout skinningRollout "Skinning Tools"
(
	button btnPrepGeo "Prep Meshes" width:110 tooltip:"Reset xforms, weld coincident verts & add skin mod to selection."
	button btnBindPose "Reset Bind Pose" width:110 tooltip:"reset current skeleton to bind pose."

	button btnSkinSwap "Skin Swap" width:110 tooltip:"Swap vertex weights from one joint to another"
	button btnBoneIO "Skinning I/O" width:110 tooltip:"Save/Load bone & skinning info."

	button btnMultRemove "Multi Remove" width:110 tooltip:"Remove multiple bones from selected objects Skin modifier."	
-- 	button btnSkinRemove "COmp Exp Prep" width:110 tooltip:"Prep skinning for per component export."	
	button btnSkinDataTransfer "Skin Data Transfer" width:110 tooltip:"Skin Data Transfer."	
	button btnVtxNorm "Vertex Normal Xfer" width:110 tooltip:"Copy vert normals form one mesh to another."	
	
	on btnPrepGeo pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/usefulScripts/prepMesh.ms")
	)
	
	on btnBindPose pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/Rigging_tools/c_resetBindPose.ms")	
	)

	on btnSkinSwap pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/Rigging_tools/Skinned_Vert_Swapper.ms")	
	)	
	
	on btnBoneIO pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/characters/Rigging/SaveAndLoadFullSkinningData.ms")	
-- 		filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/Rigging_tools/c_boneIO.ms")	
	)	
	
	on btnMultRemove pressed do 
	(
		skinOps.multiRemove $.modifiers[#Skin]
	)

-- 	on btnSkinRemove pressed do 
-- 	(
-- 		filein (RsConfigGetProjRootDir() + "tools/dcc/current/max2012/scripts/pipeline/helpers/rigging/delUnusedBones.ms")
-- 		RsSelDeleteUnusedBones()
-- 	)
	
	on btnSkinDataTransfer pressed do 
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/characters/Rigging/skinDataTransferPrep.ms")	
	)
	
	on btnVtxNorm pressed do 
	(
		filein (RsConfigGetWildWestDir() +"script/3dsMax/Characters/Setup/vertNormal_CopyPaste.ms")

	)
)

rollout miscRollout "Misc."
(
	button btnAnimLoader "Anim Loader" width:110 tooltip:"Animation loader"
	button btnGameLauncher "Game Launcher" width:110 tooltip:"Launch Game"
	button btnSpringToggle "RS spring Toggle" width:110 tooltip:"toggle R* spring"
	button btnMatToggle "Material Toggle" width:110	tooltip:"Toggle materials on/off"
	button btnVertexCol "Vtx Col Toggle" width:110	tooltip:"Deactivate vertex colours on selected mesh"
	button btnPedDataCheck "Data Check" width:110	tooltip:"Check shader / texture / mesh validity"
	button btnFrameStep "Frame Stepper" width:110	tooltip:"Tool to jump through an animation by user defined number of frames"
	
	on btnAnimLoader pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/Anim_Library_Interface.ms")
	)	
	
	on btnSpringToggle pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/usefulScripts/c_rsSpringToggle.ms")	
	)	
	
	on btnGameLauncher pressed do
	(
		filein (RsConfigGetWildWestDir() + "script/3dsMax/General_tools/gameLauncher.ms")
	)

	on btnMatToggle pressed do 
	(
		--fileIn (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/usefulScripts/toggleDisplayType.ms")
		
		if displayColor.shaded == #objects then 
		(
			displayColor.shaded = #material
		)
		else
		(
			displayColor.shaded = #object
		)
	)	
	
	on btnVertexCol pressed do 
	(
		sela = selection as array
		
		vtxState = undefined
		for a = 1 to sela.count do 
		(
			obj = sela[a]
			obj.wirecolor = color 86 86 86
			
			if vtxState == undefined do
			(
				if obj.showVertexColors == on then
				(
-- 					obj.showVertexColors = off
					vtxState = off
				)
				else
				(
-- 					obj.showVertexColors = on	
					vtxState = on
				)
			)
			
			obj.showVertexColors = vtxState
		)
	)
	
	on btnPedDataCheck pressed do 
	(
		filein (RsConfigGetWildWestDir() +"script/3dsMax/Characters/Setup/Ped_Data_Check.ms")
	)
	
	on btnFrameStep pressed do 
	(
		fileIn (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/usefulScripts/frameStepper.ms")
	)
)




-- CreateDialog rigToolsGUI width:125 pos:[1450, 100] 

global theRigGuiFloater = newRolloutFloater "RigTools" 160 920 10 160
addRollout skeletonRollout theRigGuiFloater
addRollout facialToolsRollout theRigGuiFloater
addRollout expressionToolsRollout theRigGuiFloater
addRollout skinningRollout theRigGuiFloater
addRollout miscRollout theRigGuiFloater