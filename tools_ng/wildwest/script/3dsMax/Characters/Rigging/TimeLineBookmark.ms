-- created by Nicolas Malovec


--First Try to destroy the dialog if already exist.
Try(destroydialog TLB)catch()

rollout TLB "Time Line Bookmark"

(
	--////////////////////////VARIABLES///////////////////////////////--
	Local MyKeys = #()	
	
	--////////////////////////BUTTONS///////////////////////////////--
	
	Button SaveBookmark "Save" width: 50 height: 20 offset:[-7,0] align:#left
	Button LoadBookmark "Load" width: 50 height: 20 offset:[48,-25] align:#left
	button AddBookmark"Add" width: 50 height: 20 offset:[-7,0] align:#left	
	button RemoveBookmark"Del" width: 50 height: 20 offset:[48,-25] align:#left	
	Button Previous "Prev" width: 27 height: 25 offset:[42,0] align:#left
	Button Next "Next" width: 27 height: 25 offset:[70,-30] align:#left
	listbox Bookmark_lb "Bookmarks" width: 115 height: 10 offset:[-12,-20] align:#left	 items:MyKeys


	
	--////////////////////////EVENT///////////////////////////////--
	
		on RemoveBookmark pressed do
		(
		ToRemove = Bookmark_lb.selection
		deleteitem Bookmark_lb.items ToRemove
		Bookmark_lb.items = Bookmark_lb.items						
		)
	
	
		--Store the bookmarks into a .ini file
		on SaveBookmark pressed do
		(
		-- Create the Saving Path
		SavedBook = getSaveFileName	caption:"BookMark" types:".ini"
		
		--Now Create the ini.file using the Saving Path			
		--Section_String = "Book" : arbritrary name. 			
		--key_String = "BookMark" : : arbritrary name.			
		--key value = Bookmark_lb.items = MyKeys
			
		setINISetting SavedBook "Book" "BookMark" (MyKeys as string) 
		)
		
		on LoadBookmark pressed do
		(
			-- Go for the Saving Path
		LoadedBook = getOpenFileName	caption:"BookMark" types:".ini"	
			
		BookMark = getINISetting LoadedBook "Book" "BookMark" 
			
		MyBookMark = execute BookMark
		print MyBookMark
			
		For Mark = 1 to MyBookMark.count do
		(	
		TheBookMark = MyBookMark[Mark]	
		appendifunique Bookmark_lb.items (TheBookMark as string) 
		)
		Bookmark_lb.items = Bookmark_lb.items	
		)
	
	    on Next pressed do
		(
		Curent = Bookmark_lb.selection 
		New = (Curent + 1)		
		Bookmark_lb.selection = New 			
		)
		
		on Previous pressed do
		(
		Curent = Bookmark_lb.selection 
		New = (Curent - 1)		
		Bookmark_lb.selection = New 
		-- if the first (0) item in the listbox is selected, looks how many items it contain and select the last one.
			if New == 0 then
			(
			New = MyKeys.count
			Bookmark_lb.selection = New				
			)
			else()			
		)
	
		on AddBookmark pressed do
		(				
			SetTime = sliderTime.frame		
			appendifunique Bookmark_lb.items ( SetTime as string )
			Bookmark_lb.items = Bookmark_lb.items
		)
		
		
		on TLB resized TheSize do
		(			
			Offset = 10 -- This Offset is to compensate the way height' ListBox is set-up, but lignes instead of pixels
			MyWidthOffset = ((115*TheSize[1])/90)			
			MyHeightOffset = (TheSize[2]-220)
			Bookmark_lb.height = (135+(10*MyHeightOffset)/Offset)
			Bookmark_lb.Width = MyWidthOffset*0.78
		)

		
		on Bookmark_lb selected BM do
		(	
         sliderTime =  (Bookmark_lb.selected as time)
		)
)
createDialog TLB width: 90 height: 220 style:#(#style_sysmenu, #style_titlebar, #style_resizing)




