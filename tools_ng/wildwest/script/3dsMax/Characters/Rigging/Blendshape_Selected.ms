-- Multi object blendshape tool
-- Takes a selection, orders it, then sets every object to have a blend with all others as targets
-- Rick Stirling
-- December 2012


-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())
	

-- Take a selection
meshSelection = getcurrentSelection()


-- get these node names and sort them
namedSelection = #()
for ms = 1 to meshSelection.count do append  namedSelection meshSelection[ms].name 
	
namedSelection = sort namedSelection


-- Loop through these nodes and apply a morph
-- Set the other objects to be the targets
for blendMeshIndex = 1 to namedSelection.count do
(
	-- Select the mesh and add the morph modifier
	theBlendMeshName = namedSelection[blendMeshIndex]
	theBlendMesh =  getNodeByName theBlendMeshName
	select theBlendMesh
	modPanel.addModToSelection (Morpher ()) ui:on
	
	with redraw off 
	-- Now add the targets	
	for targetMeshIndex = 1 to namedSelection.count do 
	(
		-- find the node in the select
		theTargetMeshName = namedSelection[targetMeshIndex]
		theTargetMesh =  getNodeByName theTargetMeshName

		-- Retarget the morph to the new mesh?
		WM3_MC_BuildFromNode theBlendMesh.morpher targetMeshIndex theTargetMesh 
		WM3_MC_Rebuild theBlendMesh.morpher targetMeshIndex 
	)	-- end add the target meshes
	
)	






