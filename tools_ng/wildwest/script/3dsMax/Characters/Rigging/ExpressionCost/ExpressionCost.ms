myNode = getNodeByName "FACIAL_L_lipLowerSDK"

for i = 1 to myNode.Position.controller.count do
(
	
	myController = myNode.Position.controller[i]
	if myController.name != "Frozen Position" and myController.name != "Zero Pos XYZ" do
	(
		xPosController = myController.x_Position.controller
		yPosController = myController.y_Position.controller
		zPosController = myController.z_Position.controller
		
		if (xPosController as string) == "Controller:Float_Expression" do
		(
			
			for i = 1 to xPosController.NumScalars() do
			(
				
			
				scalarName = xPosController.GetScalarName i
				
				if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
				(

					scalarTarget = exprformaxobject (xPosController.GetScalarTarget scalarName asController:True) explicitNames:true
					formattedScalar = filterString scalarTarget "."
					objectName = (substring formattedScalar[1] 2 -1)
					
					geo = getNodeByName objectName
					
					print geo
					
					if geo == undefined do
					(
						print (objectName + " doesn't exist")
						myController.x_Position.controller = bezier_float()
						
					)
					
				)				
				
			)
			
		)
		
		if (yPosController as string) == "Controller:Float_Expression" do
		(
			
			for i = 1 to yPosController.NumScalars() do
			(
				
				scalarName = yPosController.GetScalarName i
				if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
				(
					scalarTarget = exprformaxobject (yPosController.GetScalarTarget scalarName asController:True) explicitNames:true
					formattedScalar = filterString scalarTarget "."
					objectName = (substring formattedScalar[1] 2 -1)
					
					geo = getNodeByName objectName
					
					print geo
					
					if geo == undefined do
					(
						print (objectName + " doesn't exist")
						myController.y_Position.controller = bezier_float()
						
					)
					
				)				
				
			)
			
		)
		
		if (zPosController as string) == "Controller:Float_Expression" do
		(
			
			for i = 1 to zPosController.NumScalars() do
			(
			
				scalarName = zPosController.GetScalarName i
				
				if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
				(
					scalarTarget = exprformaxobject (zPosController.GetScalarTarget scalarName asController:True) explicitNames:true
					formattedScalar = filterString scalarTarget "."
					objectName = (substring formattedScalar[1] 2 -1)
					
					geo = getNodeByName objectName
					
					print geo
					
					if geo == undefined do
					(
						
						print (objectName + " doesn't exist")
						myController.z_Position.controller = bezier_float()
						
					)
					
					
				)				
				
			)
			
		)
		
	)

)