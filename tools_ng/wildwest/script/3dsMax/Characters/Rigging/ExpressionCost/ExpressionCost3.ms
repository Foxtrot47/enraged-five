bonePosition 	= #()
	
mouthCustomAttributes =   #("lipsNarrowWideR_CA", 	
							"frownR_CA", 		
							"funnelDR_CA", 	
							"clench_CA",
							"lipsNarrowWideL_CA", 
							"scream_CA", 		
							"chinRaiseUpper_CA", 
							"smileL_CA", 	
							"smileR_CA", 	
							"mouthSuckUL_CA", 	
							"mouthSuckUR_CA", 	
							"puckerL_CA", 		
							"puckerR_CA", 
							"oh_CA", 	
							"funnelUL_CA", 	
							"funnelUR_CA", 	
							"pressL_CA", 		
							"pressR_CA", 	
							"lipBite_CA", 		
							"closeOuterR_CA", 	
							"lipsStretchOpenR_CA",
							"chinRaiseLower_CA", 	
							"dimpleR_CA", 		
							"mouthSuckDR_CA", 		
							"chinWrinkle_CA", 	
							"closeOuterL_CA", 	
							"dimpleL_CA", 		
							"frownL_CA", 		
							"lipsStretchOpenL_CA", 
							"funnelDL_CA", 	
							"mouthSuckDL_CA", 		
							"nasolabialFurrowL_CA", 
							"suckPuffL_CA", 	
							"suckPuffR_CA", 	
							"nasolabialFurrowR_CA")
	
fn recurseHierarchyPositions Root =
(
	append bonePosition Root
	if Root.children.count > 0 do
	(
		for i = 1 to Root.children.count do
		(
			child = Root.children[i]
			recurseHierarchyPositions child
		)
	)
	
	return bonePosition
)

faceRoot = getNodeByName "FACIAL_facialRoot"
recurseHierarchyPositions faceRoot

for k = 1 to bonePosition.count do
(
	
	print bonePosition[k]

	for i = 1 to bonePosition[k].Position.controller.count do
	(
		
		myController = bonePosition[k].Position.controller[i]
		if myController.name != "Frozen Position" and myController.name != "Zero Pos XYZ" do
		(
			xPosController = myController.x_Position.controller
			yPosController = myController.y_Position.controller
			zPosController = myController.z_Position.controller
			
			if (xPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to xPosController.NumScalars() do
				(
					
				
					scalarName = xPosController.GetScalarName i
					
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(

						geo = undefined
						scalarTarget = undefined
						
						try (scalarTarget = exprformaxobject (xPosController.GetScalarTarget scalarName asController:True) explicitNames:true)
						catch()
						
						if scalarTarget != undefined do 
						(
							formattedScalar = filterString scalarTarget "."
							objectName = (substring formattedScalar[1] 2 -1)
							
							geo = getNodeByName objectName
							
							if matchPattern scalarTarget pattern:"*_CA*" do
							(
								scalarTargetController = filterString scalarTarget "$"

								for i = 1 to mouthCustomAttributes.count do
								(
									if (matchpattern scalarTargetController[1] pattern:("*" + mouthCustomAttributes[i] + "*")) do
									(
										var = ("." + mouthCustomAttributes[i]) 
										index = findString scalarTargetController[1] var
										if index != undefined do
										(
											customIndex = replace scalarTargetController[1] index var.count ("[" + (i as string) + "]")
											scalarTargetController[1] = customIndex
										)
									)

								)
								myString = ("format ($" + scalarTargetController[1] + " as string)")
								BezierOrWire = execute ("$" + scalarTargetController[1])
								if (BezierOrWire as string) == "Controller:Bezier_Float" do
								(
									geo = undefined
								)							
							)

						)
						
						if geo == undefined do
						(
							print ("doesn't exist")
							myController.x_Position.controller = bezier_float()
							
						)
						
					)				
					
				)
				
			)
			
			if (yPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to yPosController.NumScalars() do
				(
					
					scalarName = yPosController.GetScalarName i
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(

						geo = undefined
						scalarTarget = undefined
						
						try (scalarTarget = exprformaxobject (yPosController.GetScalarTarget scalarName asController:True) explicitNames:true)
						catch()
						
						if scalarTarget != undefined do 
						(
							formattedScalar = filterString scalarTarget "."
							objectName = (substring formattedScalar[1] 2 -1)
							
							geo = getNodeByName objectName
							
-- 							print geo
						)
							
						if geo == undefined do
						(
							print ("doesn't exist")
							myController.y_Position.controller = bezier_float()
							
						)
						
						
						
					)				
					
				)
				
			)
			
			if (zPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to zPosController.NumScalars() do
				(
				
					scalarName = zPosController.GetScalarName i
					
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(
						geo = undefined
						scalarTarget = undefined
						
						try (scalarTarget = exprformaxobject (zPosController.GetScalarTarget scalarName asController:True) explicitNames:true)
						catch()
						
						if scalarTarget != undefined do 
						(
							formattedScalar = filterString scalarTarget "."
							objectName = (substring formattedScalar[1] 2 -1)
							
							geo = getNodeByName objectName
							
-- 							print geo
						)
						
						if geo == undefined do
						(
							
							print ("doesn't exist")
							myController.z_Position.controller = bezier_float()
							
						)
						
						
					)				
					
				)
				
			)
			
		)

	)
	
	for i = 1 to bonePosition[k].Rotation.controller.count do
	(
		
		myController = bonePosition[k].Rotation.controller[i]
		if myController.name != "Frozen Rotation" and myController.name != "Zero Euler XYZ" do
		(
			xPosController = myController.x_Rotation.controller
			yPosController = myController.y_Rotation.controller
			zPosController = myController.z_Rotation.controller
			
			if (xPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to xPosController.NumScalars() do
				(
					
				
					scalarName = xPosController.GetScalarName i
					
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(

						geo = undefined
						scalarTarget = undefined
						
						try (scalarTarget = exprformaxobject (xPosController.GetScalarTarget scalarName asController:True) explicitNames:true)
						catch()
						
						if scalarTarget != undefined do 
						(
							formattedScalar = filterString scalarTarget "."
							objectName = (substring formattedScalar[1] 2 -1)
							
							geo = getNodeByName objectName
							
							print geo
						)
						
						if geo == undefined do
						(
							print ("doesn't exist")
							myController.x_Rotation.controller = bezier_float()
							
						)
						
					)				
					
				)
				
			)
			
			if (yPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to yPosController.NumScalars() do
				(
					
					scalarName = yPosController.GetScalarName i
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(
						geo = undefined
						scalarTarget = undefined
						
						try (scalarTarget = exprformaxobject (yPosController.GetScalarTarget scalarName asController:True) explicitNames:true)
						catch()
						
						if scalarTarget != undefined do 
						(
							formattedScalar = filterString scalarTarget "."
							objectName = (substring formattedScalar[1] 2 -1)
							
							geo = getNodeByName objectName
							
-- 							print geo
						)
						
						if geo == undefined do
						(
							print ("doesn't exist")
							myController.y_Rotation.controller = bezier_float()
							
						)
						
					)				
					
				)
				
			)
			
			if (zPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to zPosController.NumScalars() do
				(
				
					scalarName = zPosController.GetScalarName i
					
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(
						geo = undefined
						scalarTarget = undefined
						
						try (scalarTarget = exprformaxobject (zPosController.GetScalarTarget scalarName asController:True) explicitNames:true)
						catch()
						
						if scalarTarget != undefined do 
						(
							formattedScalar = filterString scalarTarget "."
							objectName = (substring formattedScalar[1] 2 -1)
							
							geo = getNodeByName objectName
							
-- 							print geo
						)
						
						if geo == undefined do
						(
							
							print ("doesn't exist")
							myController.z_Rotation.controller = bezier_float()
							
						)
						
						
					)				
					
				)
				
			)
			
		)

	)
	
	for i = 1 to bonePosition[k].Scale.controller.count do
	(
		
		myController = bonePosition[k].Scale.controller[i]
		if myController.name != "Frozen Scale" and myController.name != "Zero Scale XYZ" do
		(
			xPosController = myController.x_Scale.controller
			yPosController = myController.y_Scale.controller
			zPosController = myController.z_Scale.controller
			
			if (xPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to xPosController.NumScalars() do
				(
					
				
					scalarName = xPosController.GetScalarName i
					
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(

						geo = undefined
						scalarTarget = undefined
						
						try (scalarTarget = exprformaxobject (xPosController.GetScalarTarget scalarName asController:True) explicitNames:true)
						catch()
						
						if scalarTarget != undefined do 
						(
							formattedScalar = filterString scalarTarget "."
							objectName = (substring formattedScalar[1] 2 -1)
							
							geo = getNodeByName objectName
							
-- 							print geo
						)
						
						if geo == undefined do
						(
							print ("doesn't exist")
							myController.x_Scale.controller = bezier_float()
							
						)
						
					)				
					
				)
				
			)
			
			if (yPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to yPosController.NumScalars() do
				(
					
					scalarName = yPosController.GetScalarName i
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(
						geo = undefined
						scalarTarget = undefined
						
						try (scalarTarget = exprformaxobject (yPosController.GetScalarTarget scalarName asController:True) explicitNames:true)
						catch()
						
						if scalarTarget != undefined do 
						(
							formattedScalar = filterString scalarTarget "."
							objectName = (substring formattedScalar[1] 2 -1)
							
							geo = getNodeByName objectName
							
-- 							print geo
						)
						
						if geo == undefined do
						(
							print ("doesn't exist")
							myController.y_Scale.controller = bezier_float()
							
						)
						
					)				
					
				)
				
			)
			
			if (zPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to zPosController.NumScalars() do
				(
				
					scalarName = zPosController.GetScalarName i
					
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(
						geo = undefined
						scalarTarget = undefined
						
						try (scalarTarget = exprformaxobject (zPosController.GetScalarTarget scalarName asController:True) explicitNames:true)
						catch()
						
						if scalarTarget != undefined do 
						(
							formattedScalar = filterString scalarTarget "."
							objectName = (substring formattedScalar[1] 2 -1)
							
							geo = getNodeByName objectName
							
-- 							print geo
						)
							
						if geo == undefined do
						(
							
							print ("doesn't exist")
							myController.z_Scale.controller = bezier_float()
							
						)
						
						
					)				
					
				)
				
			)
			
		)

	)

)