myNode = getNodeByName "FACIAL_L_nasolabialFurrow"

bonePosition 	= #()
	
fn recurseHierarchyPositions Root =
(
	append bonePosition Root
	if Root.children.count > 0 do
	(
		for i = 1 to Root.children.count do
		(
			child = Root.children[i]
			recurseHierarchyPositions child
		)
	)
	
	return bonePosition
)

fn removeExpressionMissingScalarTargets myNodeAndTransform =
(

	for i = 1 to myNodeAndTransform.count do
	(
		
		myController = myNodeAndTransform[i]
		print myController
		if i > 3 do
		(
			xPosController = myController[1].controller
			yPosController = myController[2].controller
			zPosController = myController[3].controller
			
			if (xPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to xPosController.NumScalars() do
				(
					
				
					scalarName = xPosController.GetScalarName i
					
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(

						scalarTarget = exprformaxobject (xPosController.GetScalarTarget scalarName asController:True) explicitNames:true
						formattedScalar = filterString scalarTarget "."
						objectName = (substring formattedScalar[1] 2 -1)
						
						geo = getNodeByName objectName
						
						print geo
						
						if geo == undefined do
						(
							print (objectName + " doesn't exist")
-- 							xPosController.track = bezier_float()
							
						)
						
					)				
					
				)
				
			)
			
			if (yPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to yPosController.NumScalars() do
				(
					
					scalarName = yPosController.GetScalarName i
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(
						scalarTarget = exprformaxobject (yPosController.GetScalarTarget scalarName asController:True) explicitNames:true
						formattedScalar = filterString scalarTarget "."
						objectName = (substring formattedScalar[1] 2 -1)
						
						geo = getNodeByName objectName
						
						print geo
						
						if geo == undefined do
						(
							print (objectName + " doesn't exist")
-- 							yPosController.track = bezier_float()
							
						)
						
					)				
					
				)
				
			)
			
			if (zPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to zPosController.NumScalars() do
				(
				
					scalarName = zPosController.GetScalarName i
					
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(
						scalarTarget = exprformaxobject (zPosController.GetScalarTarget scalarName asController:True) explicitNames:true
						formattedScalar = filterString scalarTarget "."
						objectName = (substring formattedScalar[1] 2 -1)
						
						geo = getNodeByName objectName
						
						print geo
						
						if geo == undefined do
						(
							
							print (objectName + " doesn't exist")
-- 							zPosController.track = bezier_float()
							
						)
						
						
					)				
					
				)
				
			)
			
		)

	)

)

faceRoot = getNodeByName "FACIAL_facialRoot"
recurseHierarchyPositions faceRoot
for i = 1 to bonePosition.count do
(
	print bonePosition[i].name
	removeExpressionMissingScalarTargets bonePosition[i].Position.controller
	removeExpressionMissingScalarTargets bonePosition[i].Rotation.controller
	removeExpressionMissingScalarTargets bonePosition[i].Scale.controller
	print "\n"
	
)