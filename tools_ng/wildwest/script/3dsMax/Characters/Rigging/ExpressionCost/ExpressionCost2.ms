bonePosition 	= #()
	
fn recurseHierarchyPositions Root =
(
	append bonePosition Root
	if Root.children.count > 0 do
	(
		for i = 1 to Root.children.count do
		(
			child = Root.children[i]
			recurseHierarchyPositions child
		)
	)
	
	return bonePosition
)

faceRoot = getNodeByName "FACIAL_facialRoot"
recurseHierarchyPositions faceRoot

for k = 1 to bonePosition.count do
(

	for i = 1 to bonePosition[k].Position.controller.count do
	(
		
		myController = bonePosition[k].Position.controller[i]
		if myController.name != "Frozen Position" and myController.name != "Zero Pos XYZ" do
		(
			xPosController = myController.x_Position.controller
			yPosController = myController.y_Position.controller
			zPosController = myController.z_Position.controller
			
			if (xPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to xPosController.NumScalars() do
				(
					
				
					scalarName = xPosController.GetScalarName i
					
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(

						scalarTarget = exprformaxobject (xPosController.GetScalarTarget scalarName asController:True) explicitNames:true
						formattedScalar = filterString scalarTarget "."
						objectName = (substring formattedScalar[1] 2 -1)
						
						geo = getNodeByName objectName
						
						print geo
						
						if geo == undefined do
						(
							print (objectName + " doesn't exist")
							myController.x_Position.controller = bezier_float()
							
						)
						
					)				
					
				)
				
			)
			
			if (yPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to yPosController.NumScalars() do
				(
					
					scalarName = yPosController.GetScalarName i
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(
						scalarTarget = exprformaxobject (yPosController.GetScalarTarget scalarName asController:True) explicitNames:true
						formattedScalar = filterString scalarTarget "."
						objectName = (substring formattedScalar[1] 2 -1)
						
						geo = getNodeByName objectName
						
						print geo
						
						if geo == undefined do
						(
							print (objectName + " doesn't exist")
							myController.y_Position.controller = bezier_float()
							
						)
						
					)				
					
				)
				
			)
			
			if (zPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to zPosController.NumScalars() do
				(
				
					scalarName = zPosController.GetScalarName i
					
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(
						scalarTarget = exprformaxobject (zPosController.GetScalarTarget scalarName asController:True) explicitNames:true
						formattedScalar = filterString scalarTarget "."
						objectName = (substring formattedScalar[1] 2 -1)
						
						geo = getNodeByName objectName
						
						print geo
						
						if geo == undefined do
						(
							
							print (objectName + " doesn't exist")
							myController.z_Position.controller = bezier_float()
							
						)
						
						
					)				
					
				)
				
			)
			
		)

	)
	
	for i = 1 to bonePosition[k].Rotation.controller.count do
	(
		
		myController = bonePosition[k].Rotation.controller[i]
		if myController.name != "Frozen Rotation" and myController.name != "Zero Euler XYZ" do
		(
			xPosController = myController.x_Rotation.controller
			yPosController = myController.y_Rotation.controller
			zPosController = myController.z_Rotation.controller
			
			if (xPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to xPosController.NumScalars() do
				(
					
				
					scalarName = xPosController.GetScalarName i
					
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(

						scalarTarget = exprformaxobject (xPosController.GetScalarTarget scalarName asController:True) explicitNames:true
						formattedScalar = filterString scalarTarget "."
						objectName = (substring formattedScalar[1] 2 -1)
						
						geo = getNodeByName objectName
						
						print geo
						
						if geo == undefined do
						(
							print (objectName + " doesn't exist")
							myController.x_Rotation.controller = bezier_float()
							
						)
						
					)				
					
				)
				
			)
			
			if (yPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to yPosController.NumScalars() do
				(
					
					scalarName = yPosController.GetScalarName i
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(
						scalarTarget = exprformaxobject (yPosController.GetScalarTarget scalarName asController:True) explicitNames:true
						formattedScalar = filterString scalarTarget "."
						objectName = (substring formattedScalar[1] 2 -1)
						
						geo = getNodeByName objectName
						
						print geo
						
						if geo == undefined do
						(
							print (objectName + " doesn't exist")
							myController.y_Rotation.controller = bezier_float()
							
						)
						
					)				
					
				)
				
			)
			
			if (zPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to zPosController.NumScalars() do
				(
				
					scalarName = zPosController.GetScalarName i
					
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(
						scalarTarget = exprformaxobject (zPosController.GetScalarTarget scalarName asController:True) explicitNames:true
						formattedScalar = filterString scalarTarget "."
						objectName = (substring formattedScalar[1] 2 -1)
						
						geo = getNodeByName objectName
						
						print geo
						
						if geo == undefined do
						(
							
							print (objectName + " doesn't exist")
							myController.z_Rotation.controller = bezier_float()
							
						)
						
						
					)				
					
				)
				
			)
			
		)

	)
	
	for i = 1 to bonePosition[k].Scale.controller.count do
	(
		
		myController = bonePosition[k].Scale.controller[i]
		if myController.name != "Frozen Scale" and myController.name != "Zero Scale XYZ" do
		(
			xPosController = myController.x_Scale.controller
			yPosController = myController.y_Scale.controller
			zPosController = myController.z_Scale.controller
			
			if (xPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to xPosController.NumScalars() do
				(
					
				
					scalarName = xPosController.GetScalarName i
					
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(

						scalarTarget = exprformaxobject (xPosController.GetScalarTarget scalarName asController:True) explicitNames:true
						formattedScalar = filterString scalarTarget "."
						objectName = (substring formattedScalar[1] 2 -1)
						
						geo = getNodeByName objectName
						
						print geo
						
						if geo == undefined do
						(
							print (objectName + " doesn't exist")
							myController.x_Scale.controller = bezier_float()
							
						)
						
					)				
					
				)
				
			)
			
			if (yPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to yPosController.NumScalars() do
				(
					
					scalarName = yPosController.GetScalarName i
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(
						scalarTarget = exprformaxobject (yPosController.GetScalarTarget scalarName asController:True) explicitNames:true
						formattedScalar = filterString scalarTarget "."
						objectName = (substring formattedScalar[1] 2 -1)
						
						geo = getNodeByName objectName
						
						print geo
						
						if geo == undefined do
						(
							print (objectName + " doesn't exist")
							myController.y_Scale.controller = bezier_float()
							
						)
						
					)				
					
				)
				
			)
			
			if (zPosController as string) == "Controller:Float_Expression" do
			(
				
				for i = 1 to zPosController.NumScalars() do
				(
				
					scalarName = zPosController.GetScalarName i
					
					if scalarName != "F" and scalarName != "NT" and scalarName != "S" and scalarName != "T" do
					(
						scalarTarget = exprformaxobject (zPosController.GetScalarTarget scalarName asController:True) explicitNames:true
						formattedScalar = filterString scalarTarget "."
						objectName = (substring formattedScalar[1] 2 -1)
						
						geo = getNodeByName objectName
						
						print geo
						
						if geo == undefined do
						(
							
							print (objectName + " doesn't exist")
							myController.z_Scale.controller = bezier_float()
							
						)
						
						
					)				
					
				)
				
			)
			
		)

	)

)