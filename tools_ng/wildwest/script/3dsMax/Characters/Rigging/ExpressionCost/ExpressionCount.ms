bonePosition 	= #()
	
fn recurseHierarchyPositions Root =
(
	append bonePosition Root
	if Root.children.count > 0 do
	(
		for i = 1 to Root.children.count do
		(
			child = Root.children[i]
			recurseHierarchyPositions child
		)
	)
	
	return bonePosition
)

faceRoot = getNodeByName "FACIAL_facialRoot"
recurseHierarchyPositions faceRoot

posExpressionCount = 0
rotExpressionCount = 0
scaleExpressionCount = 0

for k = 1 to bonePosition.count do
(
	for i = 1 to bonePosition[k].Position.controller.count do
	(
		
		myController = bonePosition[k].Position.controller[i]
		if myController.name != "Frozen Position" and myController.name != "Zero Pos XYZ" do
		(
			xPosController = myController.x_Position.controller
			yPosController = myController.y_Position.controller
			zPosController = myController.z_Position.controller
			
			if (xPosController as string) == "Controller:Float_Expression" do
			(
				posExpressionCount += 1
			)
			
			if (yPosController as string) == "Controller:Float_Expression" do
			(
				posExpressionCount += 1
			)
			
			if (zPosController as string) == "Controller:Float_Expression" do
			(
				posExpressionCount += 1
			)
		)
	)
	
	for i = 1 to bonePosition[k].Rotation.controller.count do
	(
		
		myController = bonePosition[k].Rotation.controller[i]
		if myController.name != "Frozen Rotataion" and myController.name != "Zero Euler XYZ" do
		(
			xPosController = myController.x_Rotation.controller
			yPosController = myController.y_Rotation.controller
			zPosController = myController.z_Rotation.controller
			
			if (xPosController as string) == "Controller:Float_Expression" do
			(
				rotExpressionCount += 1
			)
			
			if (yPosController as string) == "Controller:Float_Expression" do
			(
				rotExpressionCount += 1
			)
			
			if (zPosController as string) == "Controller:Float_Expression" do
			(
				rotExpressionCount += 1
			)
		)
	)
	
	for i = 1 to bonePosition[k].Scale.controller.count do
	(
		
		myController = bonePosition[k].Scale.controller[i]
		if myController.name != "Frozen Scale" and myController.name != "Zero Scale XYZ" do
		(
			xPosController = myController.x_Scale.controller
			yPosController = myController.y_Scale.controller
			zPosController = myController.z_Scale.controller
			
			if (xPosController as string) == "Controller:Float_Expression" do
			(
				scaleExpressionCount += 1
			)
			
			if (yPosController as string) == "Controller:Float_Expression" do
			(
				scaleExpressionCount += 1
			)
			
			if (zPosController as string) == "Controller:Float_Expression" do
			(
				scaleExpressionCount += 1
			)
		)
	)
)

print ("POS EXPRESSION COUNT: " + (posExpressionCount as string))
print ("ROT EXPRESSION COUNT: " + (rotExpressionCount as string))
print ("SCALE EXPRESSION COUNT: " + (scaleExpressionCount as string))