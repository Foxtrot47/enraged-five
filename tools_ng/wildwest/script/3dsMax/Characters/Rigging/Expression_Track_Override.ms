--Expression Track Override Script
--Stewart Wright
--22/07/10
-------------------------------------------------------------------------------------------------------------------------
--We need to be able to tag certain bones and sliders with some specific data in the UDP.
--Most tracks are correct via the exporter, but sometimes we need overrides - like telling sliders that they are used to control animated normal maps.
--This script helps add the override information.
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

-- August 2011, Rick Stirling. Converted to WW2 and tidied up.


-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")



-------------------------------------------------------------------------------------------------------------------------
selOverRideTrans = "None"
overRideTrans = ""

selOverRideRot = "None"
overRideRot = ""

selOverRideScale = "None"
overRideScale = ""

-------------------------------------------------------------------------------------------------------------------------
-- tidy up the user defined properties by sorting it alphabetically
fn reorderTags tagMe = 
(
	currentUDP = getUserPropbuffer tagMe
	filterUDP = filterString currentUDP "\r\n"
	alphUDP = sort filterUDP

	tag = ""	
	for t = 1 to alphUDP.count do
		(
			tag = tag + alphUDP[t] + "\r\n"
			setUserPropbuffer tagMe tag
		)
)-- end reorderTags

-------------------------------------------------------------------------------------------------------------------------
--write the tags while preserving any existing UDP information
fn writeUDP obj selOverRide orTag =
(
	tempTag = uppercase orTag
	shortTag = substring tempTag 1 6
	
	for tagSel in obj do
		(
		keepUDP = #()
		currentUDP = getUserPropbuffer tagSel
		filterUDP = filterString currentUDP "\r\n"
		
		for x = 1 to filterUDP.count do
			(
			tempUDP = uppercase filterUDP[x]
			shortUDP = substring tempUDP 1 6
			if shortUDP != shortTag then
			append keepUDP filterUDP[x]
			)
		
		if selOverRide == "None" then
		(
			if keepUDP.count == 0 then
				(
				setUserPropbuffer tagSel ""
				)
			else
				(				
				for k = 1 to keepUDP.count do
					(
					orTag = keepUDP[k] + "\r\n"
					)
				setUserPropbuffer tagSel orTag --write the tags
				reorderTags tagSel --tidy up the tags
				)
		)
		else
		(
			if keepUDP.count == 0 then
				(
				setUserPropbuffer tagSel orTag
				)
			else
				(
				for k = 1 to keepUDP.count do
					(
					orTag = keepUDP[k] + "\r\n"	+orTag
					)
				setUserPropbuffer tagSel orTag --write the tags
				reorderTags tagSel --tidy up the tags
				)
		)
	)
)--end writeUDP

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
rollout trackOverRide "Expression Track Override"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Expression_System_Tool#Tracktype_Override" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	dropdownlist ddlOverRideTrans "Translation:" items:#("None", "TRACK_BONE_TRANSLATION", "TRACK_FACIAL_CONTROL", "TRACK_FACIAL_TRANSLATION", "TRACK_GENERIC_CONTROL", "TRACK_GENERIC_TRANSLATION", "TRACK_ANIMATED_NORMAL_MAPS") selection:1
	dropdownlist ddlOverRideRot "Rotation:" items:#("None", "TRACK_BONE_ROTATION", "TRACK_FACIAL_CONTROL", "TRACK_FACIAL_ROTATION", "TRACK_GENERIC_CONTROL", "TRACK_GENERIC_ROTATION", "TRACK_ANIMATED_NORMAL_MAPS") selection:1
	dropdownlist ddlOverRideScale "Scale:" items:#("None", "TRACK_BONE_SCALE", "TRACK_FACIAL_CONTROL", "TRACK_FACIAL_SCALE") selection:1
	
	button btnTagEm "Tag 'Em"  width:140 height:30 tooltip:"Tag selected"

-------------------------------------------------------------------------------------------------------------------------
	--monitor ddlOverRideTrans field
	on ddlOverRideTrans selected sel do
		(
			selOverRideTrans = trackOverRide.ddlOverRideTrans.selected
			print selOverRideTrans
			overRideTrans = "translateTrackType=" + selOverRideTrans
		)
-------------------------------------------------------------------------------------------------------------------------
	--monitor ddlOverRideRot field
	on ddlOverRideRot selected sel do
		(
			selOverRideRot = trackOverRide.ddlOverRideRot.selected
			print selOverRideRot
			overRideRot = "rotateTrackType=" + selOverRideRot
		)
		
		--monitor ddlOverRideScale field
	on ddlOverRideScale selected sel do
		(
			selOverRideScale = trackOverRide.ddlOverRideScale.selected
			print selOverRideScale
			overRideScale = "scaleTrackType=" + selOverRideScale
		)	
-------------------------------------------------------------------------------------------------------------------------
	on btnTagEm pressed do
		(
			if selection.count != 0 then
			(
				selObj = getCurrentSelection()
				for s=1 to selObj.count do
				(
					writeUDP selObj[s] selOverRideRot overRideRot
					writeUDP selObj[s] selOverRideTrans overRideTrans
					writeUDP selObj[s] selOverRideScale overRideScale
				)
			)
			else
			(
				messagebox "Nothing Selected"
			)
		)
	
)--end trackOverRide
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
createDialog trackOverRide