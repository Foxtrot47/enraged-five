-- 	jointArray = #(
-- 		"SKEL_ROOT",
-- 		"SKEL_Spine_Root",
-- 		"SKEL_Spine0",
-- 		"SKEL_Spine1",
-- 		"SKEL_Neck_1",
-- 		"SKEL_Head",
-- 	-- 	"SKEL_Head_NUB",
-- 		"SKEL_R_Clavicle",
-- 		"SKEL_R_UpperArm",
-- 		"SKEL_R_Forearm",
-- 	-- 	"SKEL_R_Forearm_NUB",
-- 		"SKEL_L_Clavicle",
-- 		"SKEL_L_UpperArm",
-- 		"SKEL_L_Forearm",
-- 	-- 	"SKEL_L_Forearm_NUB",
-- 		"SKEL_PelvisRoot",
-- 		"SKEL_Pelvis1",
-- 		"SKEL_Pelvis",
-- 		"SKEL_Tail_01",
-- 		"SKEL_Tail_02",
-- 		"SKEL_Tail_03",
-- 		"SKEL_Tail_04",
-- 	-- 	"SKEL_Tail_04_NUB",
-- 		"SKEL_R_Thigh",
-- 		"SKEL_R_Calf",
-- 		"SKEL_R_Foot",
-- 	-- 	"SKEL_R_Foot_NUB",
-- 		"SKEL_L_Thigh",
-- 		"SKEL_L_Calf",
-- 		"SKEL_L_Foot"
-- 	-- 	"SKEL_L_Foot_NUB"
-- 	)

-- fn buildBoneList = 
-- (
-- 	for o in objects do
-- 	(
-- 		if (substring o.name 1 5) == "SKEL_" do 
-- 		(
-- 			nameLength = o.name.count
-- 			if (substring o.name (nameLength -3) 4) != "_NUB" do
-- 			(
-- 				appendIfUnique jointArray o.name
-- 			)			
-- 		)
-- 	)
-- )

fn ninetyRot = 
(
	jointArray = #()
	for o in objects do
	(
		if (substring o.name 1 5) == "SKEL_" do 
		(
			nameLength = o.name.count
			if (substring o.name (nameLength - 3) 4) != "_NUB" do
			(
				appendIfUnique jointArray o.name
			)			
		)
	)
	
	animationRange = interval 0 (jointArray.count * 55)
	--------------------------------------------------------------------------------------------------------------------------------------------
	
	frameA = -50
	frameB = -45
	frameC = -40
	
	frameD = -35
	frameE = -30
	frameF = -25
	frameG = -20
	frameH = -15
	frameI = -10
	frameJ = -5
	frameK = 0
	
	for a = 1 to jointArray.count do
	(
		thisJoint = getNodeByName jointArray[a]
		frameA = frameA + 55
		frameB = frameB + 55
		frameC = frameC + 55
		frameD = frameD + 55
		frameE = frameE + 55
		frameF = frameF + 55
		frameG = frameG + 55
		frameH = frameH + 55
		frameI = frameI + 55
		frameJ = frameJ + 55
		frameK = frameK + 55
		--now we need to do a 90 degree rotation on X for frameA etc etc

		sliderTime = 0 --get the rotations in the bind pose
		initX = in coordsys parent thisJoint.rotation.controller.X_Rotation
		initY = in coordsys parent thisJoint.rotation.controller.Y_Rotation
		initZ = in coordsys parent thisJoint.rotation.controller.Z_Rotation
		set animate on
		sliderTime = (frameA - 5) --this sets us a 'bind' pose so everything is zeroed out
		in coordsys parent thisJoint.rotation.controller.X_Rotation = initX
		in coordsys parent thisJoint.rotation.controller.Y_Rotation = initY		
		in coordsys parent thisJoint.rotation.controller.Z_Rotation = initZ
		sliderTime = frameA		
		in coordsys parent thisJoint.rotation.controller.X_Rotation = (thisJoint.rotation.controller.X_Rotation + 90)
		in coordsys parent thisJoint.rotation.controller.Y_Rotation = initY
		in coordsys parent thisJoint.rotation.controller.Z_Rotation = initZ

		sliderTime = frameB
		in coordsys parent thisJoint.rotation.controller.X_Rotation = initX
		in coordsys parent thisJoint.rotation.controller.Y_Rotation = initY		
		in coordsys parent thisJoint.rotation.controller.Z_Rotation = initZ		
		
		sliderTime = frameC
		in coordsys parent thisJoint.rotation.controller.X_Rotation = (thisJoint.rotation.controller.X_Rotation - 90)
		in coordsys parent thisJoint.rotation.controller.Y_Rotation = initY
		in coordsys parent thisJoint.rotation.controller.Z_Rotation = initZ
		
		sliderTime = frameD		
		in coordsys parent thisJoint.rotation.controller.X_Rotation = initX
		in coordsys parent thisJoint.rotation.controller.Y_Rotation = initY		
		in coordsys parent thisJoint.rotation.controller.Z_Rotation = initZ
			
		sliderTime = frameE
		in coordsys parent thisJoint.rotation.controller.X_Rotation = initX
		in coordsys parent thisJoint.rotation.controller.Y_Rotation = (thisJoint.rotation.controller.Y_Rotation + 90)		
		in coordsys parent thisJoint.rotation.controller.Z_Rotation = initZ

		sliderTime = frameF
		in coordsys parent thisJoint.rotation.controller.X_Rotation = initX
		in coordsys parent thisJoint.rotation.controller.Y_Rotation = initY		
		in coordsys parent thisJoint.rotation.controller.Z_Rotation = initZ		
		
		sliderTime = frameG
		in coordsys parent thisJoint.rotation.controller.X_Rotation = initX
		in coordsys parent thisJoint.rotation.controller.Y_Rotation = (thisJoint.rotation.controller.Y_Rotation - 90)
		in coordsys parent thisJoint.rotation.controller.Z_Rotation = initZ

		sliderTime = frameH
		in coordsys parent thisJoint.rotation.controller.X_Rotation = initX
		in coordsys parent thisJoint.rotation.controller.Y_Rotation = initY		
		in coordsys parent thisJoint.rotation.controller.Z_Rotation = initZ
		
		sliderTime = frameI
		in coordsys parent thisJoint.rotation.controller.X_Rotation = initX
		in coordsys parent thisJoint.rotation.controller.Y_Rotation = initY
		in coordsys parent thisJoint.rotation.controller.Z_Rotation = (thisJoint.rotation.controller.Z_Rotation + 90)		
	
		sliderTime = frameJ
		in coordsys parent thisJoint.rotation.controller.X_Rotation = initX
		in coordsys parent thisJoint.rotation.controller.Y_Rotation = initY		
		in coordsys parent thisJoint.rotation.controller.Z_Rotation = initZ
			
		sliderTime = frameK
		in coordsys parent thisJoint.rotation.controller.X_Rotation = initX
		in coordsys parent thisJoint.rotation.controller.Y_Rotation = initY
		in coordsys parent thisJoint.rotation.controller.Z_Rotation = (thisJoint.rotation.controller.Z_Rotation - 90)
		
		sliderTime = (frameK + 5) --set everything back to the bind
		in coordsys parent thisJoint.rotation.controller.X_Rotation = initX
		in coordsys parent thisJoint.rotation.controller.Y_Rotation = initY		
		in coordsys parent thisJoint.rotation.controller.Z_Rotation = initZ
		set animate off
	)
	
	--animationRange = interval 0 frameK
)

ninetyRot()