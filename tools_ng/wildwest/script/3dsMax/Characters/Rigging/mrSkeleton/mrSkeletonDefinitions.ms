--dat file for configuration of mRSkeleton dropdown skeleton selection

--N.B. ENSURE YOU HAVE THE LATEST IN /ART/PEDS/SKELETONS/MRSKELETON/
--n.b marker_spine_root has to be in the exact same place as the marker_pelvis



ddListArray = #(
	"------", --1
	"Human", --2
	"Quad - Dog Large", --3
	"Quad - Dog Small", --4
	"Quad - Horse", --5
	"Quad - Deer", -- 6
	"Quad - Cow", --7
	"Quad - Boar", --8
	"Quad - Coyote", --9
	"Quad - Mt Lion", --10
	"Quad - Pig", --11
	"Lo Quad - Rat", --12
	"Bird - Seagull", --13
	"Bird - Pigeon", --14
	"Bird - Hen", --15
	"Bird - Crow", --16
	"Bird - ChickenHawk", --17
	"Bird - Cormorant", --18
	"Aqua - Fish", --19
	"Aqua - Shark" --20
	)
	

fn doMkrFilesExist fileStr = 
(
	gotIt = doesFileExist fileStr
	
	if gotIt == true then
	(
-- 		skelMarkerDefintion = doMkrFilesExist --marker file
		skelMarkerDefintion = fileStr --marker file
	)
	else
	(
		messageBox ("WARNING! "+fileStr+" does not exist. Please sync from Perforce.")
	)
)

fn clearSkelTypes = 
(
	legArray =   #(
	)
	
	spineForwardArray = #(
		)
	
	spineBackArray = #(
		)	
	
	planarMarkerArray = #( --multi dimensional array of marker nodes which need to be planarised.
			)	
	
	rotationOffsetOffNodes = #( --array of nodes which have several children and need to be handled differently so that they align along X rather than a random child
		)	

	connectedMarkerNodes = #( --array of coincident marker nodes which get callbacked so movement of one updates the other
		)
		
	transTaggedBones = #( --array of bones which need to have export trans tag added to them
		)
	
		
	feetNodes = #( --used for defining which nodes to realign after creation. First element is joint itself then 2nd is node to use as alignement vector to generate offset from
		)
		
	orderedSkeletonArray = #( --used during the skeleton reorientation process
	)	
		
		wingNodes = #( --bird specific 'wing' nodes as we need to align them to the top node
		)
		
	CTRLRIGPelvisArray = #( --used during ctrl rig generation to reparent pelvis area
	)	
		

	WingArray = #(
	)	
	
	customLegOrient = #() --tells the build bone function to inherit the zAxis from its parent
	
	--************************************************
		
	masterMarkerArray = #() --this gets defined during skeleton defnition			
	SkelArr = #()
	markArr = #()	
		
	doesMarkerExist = undefined	
		
	customPlanarMarkerArray = #()	
)

fn bipedSkelType = 
(
)	
	
fn quadrupedSkelType = 
(
-- 	--array of which joints the PH nodes are attached to
-- 	phParents = #( --derived from rottweiler so may need adjusting?
-- 		#("SKEL_L_Finger00", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
-- 		#("SKEL_R_Finger00", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
-- 		#("SKEL_L_Toe0", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
-- 		#("SKEL_R_Toe0", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
-- 		)
-- 		
	
	
	legArray =   #(
	"_L_Thigh",
	"_L_Calf",
	"_L_Foot",
	"_L_Toe0",
	"_L_Toe1",
	"_L_Toe1_NUB",
	"_R_Thigh",
	"_R_Calf",
	"_R_Foot",
	"_R_Toe0",
	"_R_Toe1",
	"_R_Toe1_NUB",
	"_L_Clavicle",
	"_L_UpperArm",
	"_L_Forearm",
	"_L_Hand",
	"_L_Finger00",
	"_L_Finger01",
	"_L_Finger01_NUB",
	"_R_Clavicle",
	"_R_UpperArm",
	"_R_Forearm",
	"_R_Hand",
	"_R_Finger00",
	"_R_Finger01",
	"_R_Finger01_NUB"
	)
	
	spineForwardArray = #(
		"_Spine_Root",
-- 		"_Spine0",
		"_Spine1",
		"_Spine2",
		"_Spine3",
		"_Neck_1",
		"_Neck_2",
		"_Head",
		"_Head_NUB"
		)
	
	spineBackArray = #(
		"_PelvisRoot",
		"_Pelvis1",
		"_Pelvis",
		"_Tail_01",
		"_Tail_02",
		"_Tail_03",
		"_Tail_04",
		"_Tail_05",
		"_Tail_05_NUB"
		)
		
	--if skelType != ddListArray[7] then --we dont want to planarise the cows legs as it messes up joint placement ([7] is the cow skel type)
		
	if skelType != "Quad - Cow" then --we dont want to planarise the cows legs as it messes up joint placement ([7] is the cow skel type)	
	(
	planarMarkerArray = #( --multi dimensional array of marker nodes which need to be planarised.

-- 		#("Marker_L_Thigh", "Marker_L_Toe1_NUB-END", "Marker_L_Toe1_NUB", "Marker_L_Calf"),
-- 		#("Marker_L_Thigh", "Marker_L_Toe1_NUB-END", "Marker_L_Toe1_NUB", "Marker_L_Foot"),
-- 		#("Marker_L_Thigh", "Marker_L_Toe1_NUB-END", "Marker_L_Toe1_NUB", "Marker_L_Toe0"),
-- 		#("Marker_L_Thigh", "Marker_L_Toe1_NUB-END", "Marker_L_Toe1_NUB", "Marker_L_Toe1"),

-- 		#("Marker_R_Thigh", "Marker_R_Toe1_NUB-END", "Marker_R_Toe1_NUB", "Marker_R_Calf"),
-- 		#("Marker_R_Thigh", "Marker_R_Toe1_NUB-END", "Marker_R_Toe1_NUB", "Marker_R_Foot"),
-- 		#("Marker_R_Thigh", "Marker_R_Toe1_NUB-END", "Marker_R_Toe1_NUB", "Marker_R_Toe0"),
-- 		#("Marker_R_Thigh", "Marker_R_Toe1_NUB-END", "Marker_R_Toe1_NUB", "Marker_R_Toe1"),			

-- 		#("Marker_L_UpperArm", "Marker_L_Finger01_NUB-END", "Marker_L_Finger01_NUB", "Marker_L_Finger01"),
-- 		#("Marker_L_UpperArm", "Marker_L_Finger01_NUB-END", "Marker_L_Finger01_NUB", "Marker_L_Forearm"),
-- 		#("Marker_L_UpperArm", "Marker_L_Finger01_NUB-END", "Marker_L_Finger01_NUB", "Marker_L_Hand"),
-- 		#("Marker_L_UpperArm", "Marker_L_Finger01_NUB-END", "Marker_L_Finger01_NUB", "Marker_L_Finger00"),
-- 			#("Marker_L_UpperArm", "Marker_L_Forearm", "Marker_L_Hand", "Marker_L_Finger00"),

-- 		#("Marker_R_UpperArm", "Marker_R_Finger01_NUB-END", "Marker_R_Finger01_NUB", "Marker_R_Finger01"),
-- 		#("Marker_R_UpperArm", "Marker_R_Finger01_NUB-END", "Marker_R_Finger01_NUB", "Marker_R_Forearm"),
-- 		#("Marker_R_UpperArm", "Marker_R_Finger01_NUB-END", "Marker_R_Finger01_NUB", "Marker_R_Hand"),
-- 		#("Marker_R_UpperArm", "Marker_R_Finger01_NUB-END", "Marker_R_Finger01_NUB", "Marker_R_Finger00"),
-- 			#("Marker_R_UpperArm", "Marker_R_Forearm", "Marker_R_Hand", "Marker_R_Finger00")
		)
	)
	else
	(
		planarMarkerArray = #()
	)
	rotationOffsetOffNodes = #( --array of nodes which have several children and need to be handled differently so that they align along X rather than a random child
		"Stretchy_Spine1",
		"Stretchy_Pelvis",
		"Stretchy_Facial_root"
		)		
		
	connectedMarkerNodes = #( --array of coincident marker nodes which get callbacked so movement of one updates the other
-- 		#("Marker_ROOT", "Marker_Spine_Root"),
-- 		#("Marker_Pelvis1", "Marker_Spine0")--,
-- 		#("Marker_PelvisRoot", "Marker_Spine0")--, --added in with new pelvisRoot
-- 			#("Marker_Spine1", "Marker_Spine2")
		)
		
	transTaggedBones = #( --array of bones which need to have export trans tag added to them
		"SKEL_L_Forearm",
		"SKEL_R_Forearm",
		"SKEL_L_Hand",
		"SKEL_R_Hand",
		"SKEL_ROOT",
		"SKEL_Spine_Root",
		"SKEL_Spine0",
		"SKEL_PelvisRoot",
		"SKEL_L_Thigh",
		"SKEL_R_Thigh",
		"SKEL_L_UpperArm",
		"SKEL_R_UpperArm",
		"SKEL_L_Finger00",
		"SKEL_R_Finger00",
		"SKEL_Pelvis",
		"SKEL_Tail_01",
		"SKEL_SADDLE"
		)

		
	feetNodes = #( --used for defining which nodes to realign after creation. First element is joint itself then 2nd is node to use as alignement vector to generate offset from
		#("_R_Finger00", "_R_Hand"),
		#("_L_Finger00", "_L_Hand"),
		#("_R_Finger01", "_R_Hand"),
		#("_L_Finger01", "_L_Hand"),
		#("_R_Toe0", "_R_Foot"),
		#("_L_Toe0", "_L_Foot"),
		#("_R_Toe1", "_R_Foot"),
		#("_L_Toe1", "_L_Foot")
		)
		
	orderedSkeletonArray = #( --used during the skeleton reorientation process
		"_ROOT",
		"_PelvisRoot",
		"_Pelvis1",
		"_Pelvis",
		"_Tail_01",
		"_Tail_02",
		"_Tail_03",
		"_Tail_04",
		"_Tail_05",
		"_Tail_05_NUB",
		"_L_Thigh",
		"_L_Calf",
		"_L_Foot",
		"_L_Toe0",
		"_L_Toe1",
		"_L_Toe1_NUB",
		"_R_Thigh",
		"_R_Calf",
		"_R_Foot",
		"_R_Toe0",
		"_R_Toe1",
		"_R_Toe1_NUB",
		"_Spine_Root",
		"_Spine0",
		"_Spine1",
		"_Spine2",
		"_Spine3",
		"_Neck_1",
		"_Neck_2",
		"_Head",
		"_Head_NUB",
		"_L_Clavicle",
		"_L_UpperArm",
		"_L_Forearm",
		"_L_Hand",
		"_L_Finger00",
		"_L_Finger01",
		"_L_Finger01_NUB",
		"_R_Clavicle",
		"_R_UpperArm",
		"_R_Forearm",
		"_R_Hand",
		"_R_Finger00",
		"_R_Finger01",
		"_R_Finger01_NUB"
		
	)	
	
-- 	realignTheseJoints = #()
	
	CTRLRIGPelvisArray = #( --used during ctrl rig generation to reparent pelvis area
 			
		-- Name of new parent Node followed by array of all nodes that become its children. Array ordering important. Item wanted as highest in hierarchy goes last
		
		#(#("CTRLRIG_PelvisRoot"), #("CTRLRIG_Spine_Root")),
			
		#(#("CTRLRIG_Pelvis1"), #("CTRLRIG_Spine0","CTRLRIG_PelvisRoot")),	
			
		#(#("CTRLRIG_Pelvis"), #("CTRLRIG_Pelvis1","CTRLRIG_ROOT" )),	
		
		#(#("CTRLRIG_Pelvis_AUX"), #( "CTRLRIG_L_Thigh", "CTRLRIG_R_Thigh", "CTRLRIG_Tail_01", "CTRLRIG_Pelvis"))
			
	)	

	WingArray = #( --has to be declared to clear previous versions if a bird was done prior to this etc
	)		
	
)

fn lowQuadrupedSkelType = 
(
	planarMarkerArray = #( --multi dimensional array of marker nodes which need to be planarised.

		)

	legArray =   #(
	"_L_Thigh",
	"_L_Calf",
	"_L_Foot",
	"_L_Foot_NUB",
	"_R_Thigh",
	"_R_Calf",
	"_R_Foot",
	"_R_Foot_NUB",
	"_L_Clavicle",
	"_L_UpperArm",
	"_L_Forearm",
	"_L_Forearm_NUB",
	"_R_Clavicle",
	"_R_UpperArm",
	"_R_Forearm",
	"_R_Forearm_NUB"
	)
	
	spineForwardArray = #(
		"_Spine_Root",
		"_Spine0",
		"_Spine1",
		"_Neck_1",
		"_Head",
		"_Head_NUB"
		)
	
	spineBackArray = #(
		"_PelvisRoot",
		"_Pelvis1",
		"_Pelvis",
		"_Tail_01",
		"_Tail_02",
		"_Tail_03",
		"_Tail_04",
		"_Tail_04_NUB"
		)
		
		
	rotationOffsetOffNodes = #( --array of nodes which have several children and need to be handled differently so that they align along X rather than a random child
-- 		"Stretchy_Spine1",
-- 		"Stretchy_Pelvis",
-- 		"Stretchy_Facial_root"
		)		
		
	connectedMarkerNodes = #( --array of coincident marker nodes which get callbacked so movement of one updates the other
		#("Marker_ROOT", "Marker_Spine_Root"),
-- 		#("Marker_Pelvis1", "Marker_Spine0")--,
		#("Marker_PelvisRoot", "Marker_Pelvis1")--, --added in with new pelvisRoot
-- 			#("Marker_Spine1", "Marker_Spine2")
		)
		
	transTaggedBones = #( --array of bones which need to have export trans tag added to them
		"SKEL_L_Forearm",
		"SKEL_R_Forearm",
		"SKEL_L_UpperArm",
		"SKEL_R_UpperArm"
		)
	
		
	feetNodes = #( --used for defining which nodes to realign after creation. First element is joint itself then 2nd is node to use as alignement vector to generate offset from
		#("_R_Forearm_NUB", "_R_Forearm"),
		#("_L_Forearm_NUB", "_L_Forearm"),
		#("_R_Forearm", "_R_UpperArm"),
		#("_L_Forearm", "_L_UpperArm")
		)
		
	orderedSkeletonArray = #( --used during the skeleton reorientation process
		"_ROOT",
		"_PelvisRoot",
		"_Pelvis1",
		"_Pelvis",
		"_Tail_01",
		"_Tail_02",
		"_Tail_03",
		"_Tail_04",
-- 		"_Tail_m_03",
		"_Tail_04_NUB",
		"_L_Thigh",
		"_L_Calf",
		"_L_Foot",
-- 		"_L_Toe0",
-- 		"_L_Toe1",
		"_L_Foot_NUB",
		"_R_Thigh",
		"_R_Calf",
		"_R_Foot",
-- 		"_R_Toe0",
-- 		"_R_Toe1",
		"_R_Foot_NUB",
		"_Spine_Root",
		"_Spine0",
		"_Spine1",
-- 		"_Spine2",
-- 		"_Spine3",
		"_Neck_1",
-- 		"_Neck_2",
		"_Head",
		"_Head_NUB",
		"_L_Clavicle",
		"_L_UpperArm",
		"_L_Forearm",
-- 		"_L_Hand",
-- 		"_L_Finger00",
-- 		"_L_Finger01",
		"_L_Forearm_NUB",
		"_R_Clavicle",
		"_R_UpperArm",
		"_R_Forearm",
-- 		"_R_Hand",
-- 		"_R_Finger00",
-- 		"_R_Finger01",
		"_R_Forearm_NUB"
		
	)	
		
	CTRLRIGPelvisArray = #( --used during ctrl rig generation to reparent pelvis area
 			
		-- Name of new parent Node followed by array of all nodes that become its children. Array ordering important. Item wanted as highest in hierarchy goes last

-- 		#(#("CTRLRIG_ROOT"), #("CTRLRIG_Pelvis_AUX") ),
-- 		#(#("CTRLRIG_Pelvis_AUX"), #("CTRLRIG_Pelvis", "CTRLRIG_Spine_Root"))
-- 		#(#("CTRLRIG_Pelvis_AUX"), #( "CTRLRIG_L_Thigh", "CTRLRIG_R_Thigh", "CTRLRIG_Tail_01", "CTRLRIG_Pelvis"))
			
-- 		#(#("CTRLRIG_PelvisRoot"), #("CTRLRIG_Spine_Root")),
-- 		#(#("CTRLRIG_Pelvis1"), #("CTRLRIG_Spine0","CTRLRIG_PelvisRoot")),	
-- 		#(#("CTRLRIG_Pelvis"), #("CTRLRIG_Pelvis1","CTRLRIG_ROOT" )),	
-- 		#(#("CTRLRIG_Pelvis_AUX"), #( "CTRLRIG_L_Thigh", "CTRLRIG_R_Thigh", "CTRLRIG_Tail_01", "CTRLRIG_Pelvis"))
			
		#(#("CTRLRIG_PelvisRoot"), #("CTRLRIG_Spine_Root")),
			
		#(#("CTRLRIG_Pelvis1"), #("CTRLRIG_PelvisRoot")),	
			
		#(#("CTRLRIG_Pelvis"), #("CTRLRIG_Spine_Root", "CTRLRIG_ROOT", "CTRLRIG_Pelvis1")),	
		
		#(#("CTRLRIG_Pelvis_AUX"), #( "CTRLRIG_L_Thigh", "CTRLRIG_R_Thigh", "CTRLRIG_Tail_01", "CTRLRIG_Pelvis"))		
	)	
	
)

fn birdSkelType = 
(
	legArray =   #(
	"_L_Thigh",
	"_L_Calf",
	"_L_Foot",
	"_L_Toe0",
	"_L_Toe0_NUB",
	"_R_Thigh",
	"_R_Calf",
	"_R_Foot",
	"_R_Toe0",
	"_R_Toe0_NUB",
	"_L_Clavicle",
	"_R_Clavicle"
	)
	
	spineForwardArray = #(
		"_Spine_Root",
		"_Spine0",
		"_Spine1",
		"_Spine2",
		"_Spine3",
		"_Neck_1",
		"_Head",
		"_Head_NUB"
		)
	
	spineBackArray = #(
		"_PelvisRoot",
		"_Pelvis1",
		"_Pelvis",
		"_Tail_01",
		"_Tail_02",
		"_Tail_02_NUB"
		)	
	
	planarMarkerArray = #( --multi dimensional array of marker nodes which need to be planarised.

			#("Marker_L_Thigh", "Marker_L_Toe0_NUB-END", "Marker_L_Toe0_NUB", "Marker_L_Calf"),
			#("Marker_L_Thigh", "Marker_L_Toe0_NUB-END", "Marker_L_Toe0_NUB", "Marker_L_Foot"),
			#("Marker_L_Thigh", "Marker_L_Toe0_NUB-END", "Marker_L_Toe0_NUB", "Marker_L_Toe0"),
			#("Marker_L_Thigh", "Marker_L_Toe0_NUB-END", "Marker_L_Toe0_NUB", "Marker_L_Toe0"),

			#("Marker_R_Thigh", "Marker_R_Toe0_NUB-END", "Marker_R_Toe0_NUB", "Marker_R_Calf"),
			#("Marker_R_Thigh", "Marker_R_Toe0_NUB-END", "Marker_R_Toe0_NUB", "Marker_R_Foot"),
			#("Marker_R_Thigh", "Marker_R_Toe0_NUB-END", "Marker_R_Toe0_NUB", "Marker_R_Toe0"),
			#("Marker_R_Thigh", "Marker_R_Toe0_NUB-END", "Marker_R_Toe0_NUB", "Marker_R_Toe0"),--,			
			
------------------------------------------------------------------------------------------------------

-- 			#("Marker_L_Clavicle","Marker_L_UpperArm", "Marker_L_Finger00_NUB", "Marker_L_Forearm"),
-- 			#("Marker_L_Clavicle","Marker_L_UpperArm", "Marker_L_Finger00_NUB", "Marker_L_Hand"),			
-- 			#("Marker_L_Clavicle","Marker_L_UpperArm", "Marker_L_Finger00_NUB", "Marker_L_Finger00"),
			
-- 			#("Marker_R_Clavicle","Marker_R_UpperArm", "Marker_R_Finger00_NUB", "Marker_R_Forearm"),
-- 			#("Marker_R_Clavicle","Marker_R_UpperArm", "Marker_R_Finger00_NUB", "Marker_R_Hand"),			
-- 			#("Marker_R_Clavicle","Marker_R_UpperArm", "Marker_R_Finger00_NUB", "Marker_R_Finger00")

-- 			#("Marker_L_UpperArm", "Marker_L_Finger00_NUB", "Marker_L_Forearm", "Marker_L_Clavicle"),
			#("Marker_L_UpperArm", "Marker_L_Finger00_NUB", "Marker_L_Forearm", "Marker_L_Hand"),
			#("Marker_L_UpperArm", "Marker_L_Finger00_NUB", "Marker_L_Forearm", "Marker_L_Finger00"),		

-- 			#("Marker_R_UpperArm", "Marker_R_Finger00_NUB", "Marker_R_Forearm", "Marker_R_UpperArm"),
			#("Marker_R_UpperArm", "Marker_R_Finger00_NUB", "Marker_R_Forearm", "Marker_R_Hand"),
			#("Marker_R_UpperArm", "Marker_R_Finger00_NUB", "Marker_R_Forearm", "Marker_R_Finger00")
			)	
	
	rotationOffsetOffNodes = #( --array of nodes which have several children and need to be handled differently so that they align along X rather than a random child
		"Stretchy_Spine1",
		"Stretchy_Pelvis",
		"SKEL_Spine3",
		"Stretchy_Facial_root"
		)	

	connectedMarkerNodes = #( --array of coincident marker nodes which get callbacked so movement of one updates the other
		#("Marker_ROOT", "Marker_Spine_Root"),
-- 		#("Marker_Pelvis1", "Marker_Spine0")--,
		#("Marker_Pelvis", "Marker_ROOT")--, --added in with new pelvisRoot
-- 			#("Marker_Spine1", "Marker_Spine2")
		)
		
	transTaggedBones = #( --array of bones which need to have export trans tag added to them
		"SKEL_L_Forearm",
		"SKEL_R_Forearm",
		"SKEL_L_Hand",
		"SKEL_R_Hand"
		)
	
		
	feetNodes = #( --used for defining which nodes to realign after creation. First element is joint itself then 2nd is node to use as alignement vector to generate offset from
		#("_R_Finger00", "_R_Hand"),
		#("_L_Finger00", "_L_Hand"),
-- 		#("_R_Finger01", "_R_Hand"),
-- 		#("_L_Finger01", "_L_Hand"),
		#("_R_Toe0", "_R_Foot"),
		#("_L_Toe0", "_L_Foot"),
		#("_R_Calf", "_R_Thigh"),
		#("_L_Calf", "_L_Thigh")
-- 		#("_R_Toe1", "_R_Foot"),
-- 		#("_L_Toe1", "_L_Foot")
		)
		
	orderedSkeletonArray = #( --used during the skeleton reorientation process
		"_ROOT",
		"_Pelvis",
		"_Tail_01",
		"_Tail_02",
		"_L_Thigh",
		"_L_Calf",
		"_L_Foot",
		"_L_Toe0",
		"_L_Toe0_NUB",
		"_R_Thigh",
		"_R_Calf",
		"_R_Foot",
		"_R_Toe0",
		"_R_Toe0_NUB",
		"_Spine_Root",
		"_Spine0",
		"_Spine1",
		"_Spine2",
		"_Spine3",
		"_Neck_1",
		"_Head",
		"_Head_NUB",
		"_L_Clavicle",
		"_L_UpperArm",
		"_L_Forearm",
		"_L_Hand",
		"_L_Finger00",
		"_L_Finger00_NUB",
		"_R_Clavicle",
		"_R_UpperArm",
		"_R_Forearm",
		"_R_Hand",
		"_R_Finger00",
		"_R_Finger00_NUB"		
	)	
		
		wingNodes = #( --bird specific 'wing' nodes as we need to align them to the top node
		"_Clavicle", 
		"_UpperArm",
		"_Forearm",
		"_Hand",
		"_Finger00"
		)
		
	CTRLRIGPelvisArray = #( --used during ctrl rig generation to reparent pelvis area
 			
		-- Name of new parent Node followed by array of all nodes that become its children. Array ordering important. Item wanted as highest in hierarchy goes last
			
		#(#("CTRLRIG_Pelvis"), #("CTRLRIG_ROOT", "CTRLRIG_Spine_Root" )),	
		
		#(#("CTRLRIG_Pelvis_AUX"), #( "CTRLRIG_L_Thigh", "CTRLRIG_R_Thigh", "CTRLRIG_Tail_01", "CTRLRIG_Pelvis"))
			
	)	
		

	WingArray = #(
	#("_R_Clavicle","_L_Clavicle"),
	#("_R_UpperArm", "_L_UpperArm"),
	#("_R_Forearm", "_L_Forearm"),
	#("_R_Hand", "_L_Hand"),
	#("_R_Finger00", "_L_Finger00"),
	#("_R_Finger00_NUB","_L_Finger00_NUB")
	)	
	
	customLegOrient = #( --tells the build bone function to inherit the zAxis from its parent
		"_L_Calf",
		"_R_Calf"
		)
	
)

fn fishSkelType = 
(
	planarMarkerArray = #( --multi dimensional array of marker nodes which need to be planarised.

			)	

	legArray =   #(
	"_L_Thigh",
	"_L_Calf",
	"_L_Foot",
	"_L_Toe0",
	"_L_Toe1",
	"_L_Toe1_NUB",
	"_R_Thigh",
	"_R_Calf",
	"_R_Foot",
	"_R_Toe0",
	"_R_Toe1",
	"_R_Toe1_NUB",
	"_L_Clavicle",
	"_L_UpperArm",
	"_L_Forearm",
	"_L_Hand",
	"_L_Finger00",
	"_L_Finger01",
	"_L_Finger01_NUB",
	"_R_Clavicle",
	"_R_UpperArm",
	"_R_Forearm",
	"_R_Hand",
	"_R_Finger00",
	"_R_Finger01",
	"_R_Finger01_NUB"
	)
	
	spineForwardArray = #(
		"_Spine_Root",
-- 		"_Spine0",
		"_Spine1",
		"_Spine2",
		"_Spine3",
		"_Neck_1",
		"_Neck_2",
		"_Head",
		"_Head_NUB"
		)
	
	spineBackArray = #(
		"_PelvisRoot",
		"_Pelvis1",
		"_Pelvis",
		"_Tail_01",
		"_Tail_02",
		"_Tail_03",
		"_Tail_04",
		"_Tail_05",
		"_Tail_05_NUB"
		)
					
	rotationOffsetOffNodes = #( --array of nodes which have several children and need to be handled differently so that they align along X rather than a random child
		"Stretchy_Spine1",
		"Stretchy_Pelvis",
		"Stretchy_Facial_root"
		)	

	connectedMarkerNodes = #( --array of coincident marker nodes which get callbacked so movement of one updates the other
		#("Marker_ROOT", "Marker_Spine_Root"),
-- 		#("Marker_Pelvis1", "Marker_Spine0")--,
		#("Marker_Pelvis", "Marker_ROOT")--, 
-- 			#("Marker_Spine1", "Marker_Spine2")
		)
		
	transTaggedBones = #( --array of bones which need to have export trans tag added to them
-- 		"SKEL_L_Forearm",
-- 		"SKEL_R_Forearm",
-- 		"SKEL_L_Hand",
-- 		"SKEL_R_Hand"
		)
	
-- 		--orientNodes is called during stretchy skel generation		
-- 		orientNodes = #( -- this is used for coincident joints which do not have an ideal lookat as their child is in the same position as them. So here we do name of the joint and 2nd element is their new lookat target
-- 	-- 			#("Stretchy_Spine1", "Marker_Spine3"),
-- 	-- 		#("Stretchy_ROOT", "Marker_Pelvis1")
-- 			#("Stretchy_ROOT", "Marker_Pelvis")
-- 			)
		
	feetNodes = #( --used for defining which nodes to realign after creation. First element is joint itself then 2nd is node to use as alignement vector to generate offset from
-- 		#("_R_Finger00", "_R_Hand"),
-- 		#("_L_Finger00", "_L_Hand"),
-- 		#("_R_Finger01", "_R_Hand"),
-- 		#("_L_Finger01", "_L_Hand"),
-- 		#("_R_Toe0", "_R_Foot"),
-- 		#("_L_Toe0", "_L_Foot")--,
-- 		#("_R_Toe1", "_R_Foot"),
-- 		#("_L_Toe1", "_L_Foot")
		)
		
	orderedSkeletonArray = #( --used during the skeleton reorientation process
		"_ROOT",
		"_Pelvis",
		"_Tail1",
		"_Tail2",
		"_Tail3",
		"_Tail3_NUB",
		"_L_Thigh",
		"_L_Thigh_NUB",
		"_R_Thigh",
		"_R_Thigh_NUB",
		"_Spine_Root",
		"_Spine0",
		"_Spine1",
		"_Spine2",
		"_Spine2_NUB",
		"_L_Clavicle",
		"_L_Clavicle_NUB",
		"_R_Clavicle",
		"_R_Clavicle_NUB"		
	)	
		
		wingNodes = #( --bird specific 'wing' nodes as we need to align them to the top node
-- 		"_Clavicle", 
-- 		"_UpperArm",
-- 		"_Forearm",
-- 		"_Hand",
-- 		"_Finger00"
		)
		
-- 	realignTheseJoints = #(
-- 		"SKEL_L_Clavicle",
-- 		"SKEL_L_UpperArm",
-- 		"SKEL_L_Forearm",
-- 		"SKEL_L_Hand",
-- 		"SKEL_L_Finger00",
-- 		"SKEL_R_Clavicle",
-- 		"SKEL_R_UpperArm",
-- 		"SKEL_R_Forearm",
-- 		"SKEL_R_Hand",
-- 		"SKEL_R_Finger00"
-- 		)
		
		
	CTRLRIGPelvisArray = #( --used during ctrl rig generation to reparent pelvis area
 			
		-- Name of new parent Node followed by array of all nodes that become its children. Array ordering important. Item wanted as highest in hierarchy goes last
				
		--#(#("CTRLRIG_PelvisRoot"), #("CTRLRIG_Spine_Root")),
			
		--#(#("CTRLRIG_Pelvis1"), #("CTRLRIG_Spine0","CTRLRIG_PelvisRoot")),	
			
		#(#("CTRLRIG_Pelvis"), #("CTRLRIG_ROOT" )),	
		
		#(#("CTRLRIG_Pelvis_AUX"), #( "CTRLRIG_L_Thigh", "CTRLRIG_R_Thigh", "CTRLRIG_Tail_01", "CTRLRIG_Pelvis", "CTRLRIG_Spine_Root"))
			

			
	)			
)

fn henSkelType = 
(
	planarMarkerArray = #( --multi dimensional array of marker nodes which need to be planarised.

-- 		#("Marker_L_Thigh", "Marker_L_Toe1_NUB-END", "Marker_L_Toe1_NUB", "Marker_L_Calf"),
-- 		#("Marker_L_Thigh", "Marker_L_Toe1_NUB-END", "Marker_L_Toe1_NUB", "Marker_L_Foot"),
-- 		#("Marker_L_Thigh", "Marker_L_Toe1_NUB-END", "Marker_L_Toe1_NUB", "Marker_L_Toe0"),
-- 		#("Marker_L_Thigh", "Marker_L_Toe1_NUB-END", "Marker_L_Toe1_NUB", "Marker_L_Toe1"),

-- 		#("Marker_R_Thigh", "Marker_R_Toe1_NUB-END", "Marker_R_Toe1_NUB", "Marker_R_Calf"),
-- 		#("Marker_R_Thigh", "Marker_R_Toe1_NUB-END", "Marker_R_Toe1_NUB", "Marker_R_Foot"),
-- 		#("Marker_R_Thigh", "Marker_R_Toe1_NUB-END", "Marker_R_Toe1_NUB", "Marker_R_Toe0"),
-- 		#("Marker_R_Thigh", "Marker_R_Toe1_NUB-END", "Marker_R_Toe1_NUB", "Marker_R_Toe1"),			
		
-- 		#("Marker_L_Clavicle", "Marker_L_Finger01_NUB-END", "Marker_L_Finger01_NUB", "Marker_L_UpperArm"),
-- 		#("Marker_L_Clavicle", "Marker_L_Finger01_NUB-END", "Marker_L_Finger01_NUB", "Marker_L_Forearm"),
-- 		#("Marker_L_Clavicle", "Marker_L_Finger01_NUB-END", "Marker_L_Finger01_NUB", "Marker_L_Hand"),
-- 		#("Marker_L_Clavicle", "Marker_L_Finger01_NUB-END", "Marker_L_Finger01_NUB", "Marker_L_Finger00"),
-- 			#("Marker_L_Clavicle", "Marker_L_Forearm", "Marker_L_Hand", "Marker_L_Finger01"),

-- 		#("Marker_R_Clavicle", "Marker_R_Finger01_NUB-END", "Marker_R_Finger01_NUB", "Marker_R_UpperArm"),
-- 		#("Marker_R_Clavicle", "Marker_R_Finger01_NUB-END", "Marker_R_Finger01_NUB", "Marker_R_Forearm"),
-- 		#("Marker_R_Clavicle", "Marker_R_Finger01_NUB-END", "Marker_R_Finger01_NUB", "Marker_R_Hand"),
-- 		#("Marker_R_Clavicle", "Marker_R_Finger01_NUB-END", "Marker_R_Finger01_NUB", "Marker_R_Finger00"),
-- 			#("Marker_R_Clavicle", "Marker_R_Forearm", "Marker_R_Hand", "Marker_R_Finger01")

-- 		#("Marker_L_UpperArm", "Marker_L_Finger01_NUB-END", "Marker_L_Finger01_NUB", "Marker_L_Finger01"),
-- 		#("Marker_L_UpperArm", "Marker_L_Finger01_NUB-END", "Marker_L_Finger01_NUB", "Marker_L_Forearm"),
-- 		#("Marker_L_UpperArm", "Marker_L_Finger01_NUB-END", "Marker_L_Finger01_NUB", "Marker_L_Hand"),
-- 		#("Marker_L_UpperArm", "Marker_L_Finger01_NUB-END", "Marker_L_Finger01_NUB", "Marker_L_Finger00"),
-- 			#("Marker_L_UpperArm", "Marker_L_Forearm", "Marker_L_Hand", "Marker_L_Finger00"),

-- 		#("Marker_R_UpperArm", "Marker_R_Finger01_NUB-END", "Marker_R_Finger01_NUB", "Marker_R_Finger01"),
-- 		#("Marker_R_UpperArm", "Marker_R_Finger01_NUB-END", "Marker_R_Finger01_NUB", "Marker_R_Forearm"),
-- 		#("Marker_R_UpperArm", "Marker_R_Finger01_NUB-END", "Marker_R_Finger01_NUB", "Marker_R_Hand"),
-- 		#("Marker_R_UpperArm", "Marker_R_Finger01_NUB-END", "Marker_R_Finger01_NUB", "Marker_R_Finger00"),
-- 			#("Marker_R_UpperArm", "Marker_R_Forearm", "Marker_R_Hand", "Marker_R_Finger00")
		)

	legArray =   #(
	"_L_Thigh",
	"_L_Calf",
	"_L_Foot",
	"_L_Toe0",
	"_L_Toe0_NUB",
	"_R_Thigh",
	"_R_Calf",
	"_R_Foot",
	"_R_Toe0",
	"_R_Toe0_NUB",
	"_L_Clavicle",
	"_R_Clavicle"
	)
	
	spineForwardArray = #(
		"_Spine_Root",
		"_Spine0",
		"_Spine1",
		"_Spine2",
		"_Spine3",
		"_Neck_1",
		"_Head",
		"_Head_NUB"
		)
	
	spineBackArray = #(
		"_PelvisRoot",
		"_Pelvis1",
		"_Pelvis",
		"_Tail_01",
		"_Tail_02",
		"_Tail_02_NUB"
		)	
	
				
	rotationOffsetOffNodes = #( --array of nodes which have several children and need to be handled differently so that they align along X rather than a random child
		"Stretchy_Spine1",
		"Stretchy_Pelvis",
		"Stretchy_Facial_root"
		)		
		
	connectedMarkerNodes = #( --array of coincident marker nodes which get callbacked so movement of one updates the other
		#("Marker_ROOT", "Marker_Spine_Root"),
-- 		#("Marker_Pelvis1", "Marker_Spine0")--,
		#("Marker_PelvisRoot", "Marker_Spine0")--, --added in with new pelvisRoot
-- 			#("Marker_Spine1", "Marker_Spine2")
		)
		
	transTaggedBones = #( --array of bones which need to have export trans tag added to them
-- 		"SKEL_L_Forearm",
-- 		"SKEL_R_Forearm",
-- 		"SKEL_L_UpperArm",
-- 		"SKEL_R_UpperArm"
		)
	
-- 		--orientNodes is called during stretchy skel generation
-- 		orientNodes = #( -- this is used for coincident joints which do not have an ideal lookat as their child is in the same position as them. So here we do name of the joint and 2nd element is their new lookat target
-- 	-- 			#("Stretchy_Spine1", "Marker_Spine3"),
-- 			#("Stretchy_ROOT", "Marker_Pelvis1")
-- 			)
		
	feetNodes = #( --used for defining which nodes to realign after creation. First element is joint itself then 2nd is node to use as alignement vector to generate offset from
-- 		#("_R_Finger00", "_R_Hand"),
-- 		#("_L_Finger00", "_L_Hand"),
-- 		#("_R_Finger01", "_R_Hand"),
-- 		#("_L_Finger01", "_L_Hand"),
-- 		#("_R_Toe0", "_R_Foot"),
-- 		#("_L_Toe0", "_L_Foot"),
-- 		#("_R_Toe1", "_R_Foot"),
-- 		#("_L_Toe1", "_L_Foot")
		)
		
	orderedSkeletonArray = #( --used during the skeleton reorientation process
		"_ROOT",
-- 		"_PelvisRoot",
-- 		"_Pelvis1",
		"_Pelvis",
		"_Tail_01",
		"_Tail_02",
-- 		"_Tail_m_01",
-- 		"_Tail_m_02",
-- 		"_Tail_m_03",
		"_Tail_02_NUB",
		"_L_Thigh",
		"_L_Calf",
		"_L_Foot",
		"_L_Toe0",
-- 		"_L_Toe1",
		"_L_Toe0_NUB",
		"_R_Thigh",
		"_R_Calf",
		"_R_Foot",
		"_R_Toe0",
-- 		"_R_Toe1",
		"_R_Toe0_NUB",
		"_Spine_Root",
		"_Spine0",
		"_Spine1",
		"_Spine2",
		"_Spine3",
		"_Neck_1",
-- 		"_Neck_2",
		"_Head",
		"_Head_NUB",
		"_L_Clavicle",
		"_L_Clavicle_NUB",
-- 		"_L_UpperArm",
-- 		"_L_Forearm",
-- 		"_L_Hand",
-- 		"_L_Finger00",
-- 		"_L_Finger01",
-- 		"_L_Forearm_NUB",
		"_R_Clavicle",
		"_R_Clavicle_NUB"
-- 		"_R_UpperArm",
-- 		"_R_Forearm",
-- 		"_R_Hand",
-- 		"_R_Finger00",
-- 		"_R_Finger01",
-- 		"_R_Forearm_NUB"
		
	)	

-- 	realignTheseJoints = #()
	
	
	CTRLRIGPelvisArray = #( --used during ctrl rig generation to reparent pelvis area
 			
		-- Name of new parent Node followed by array of all nodes that become its children. Array ordering important. Item wanted as highest in hierarchy goes last
		--first index is parent, second index is array of 1st nodes children
		#(#("CTRLRIG_PelvisRoot"), #("CTRLRIG_Spine_Root")),
			
-- 		#(#("CTRLRIG_Pelvis1"), #("CTRLRIG_Spine0","CTRLRIG_PelvisRoot")),	
			
		#(#("CTRLRIG_Pelvis"), #("CTRLRIG_ROOT", "CTRLRIG_PelvisRoot" )),	
		
		#(#("CTRLRIG_Pelvis_AUX"), #( "CTRLRIG_L_Thigh", "CTRLRIG_R_Thigh", "CTRLRIG_Tail_01", "CTRLRIG_Pelvis"))
			
	)		
)

fn defineSkeletonSettings skelType = 
(
	if skelType == ddListArray[2] do --human
	(
		bipedSkelType()
	)
	
	if skelType == ddListArray[3] do --"Quad - Dog Large", --3
	(
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/quad_DogLarge.mkr") --marker file
		
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Finger00", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Finger00", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Toe0", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Toe0", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
			)
		
		
		quadrupedSkelType()
	)
	
	if skelType == ddListArray[4] do --"Quad - Dog Small", --4
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Finger00", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Finger00", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Toe0", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Toe0", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)
		
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/quad_DogSmall.mkr") --marker file
		quadrupedSkelType()
	)

	if skelType == ddListArray[5] do --"Quad - Horse", --5
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Finger01", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Finger01", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Toe1", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Toe1", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)
		
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/quad_Horse.mkr") --marker file
		quadrupedSkelType()
	)

	if skelType == ddListArray[6] do --"Quad - Deer", --6
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Finger01", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Finger01", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Toe1", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Toe1", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)
		
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/quad_Deer.mkr") --marker file
		quadrupedSkelType()
	)	
	
	if skelType == ddListArray[7] do --"Quad - Cow", --7
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Finger01", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Finger01", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Toe1", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Toe1", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)
		
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/quad_Cow.mkr") --marker file
		quadrupedSkelType()
	)	

	if skelType == ddListArray[8] do --"Quad - Boar", --8
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Finger01", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Finger01", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Toe1", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Toe1", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)		
		
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/quad_Boar.mkr") --marker file
		quadrupedSkelType()
	)	

	if skelType == ddListArray[9] do --"Quad - Coyote", --9
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Finger00", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Finger00", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Toe0", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Toe0", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
			)
			
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/quad_Coyote.mkr") --marker file
		quadrupedSkelType()
	)		
	
	if skelType == ddListArray[10] do --"Quad - Mt Lion", --10
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Finger00", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Finger00", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Toe0", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Toe0", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
			)
			
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/quad_MtLion.mkr") --marker file
		quadrupedSkelType()
	)	

	if skelType == ddListArray[11] do --"Quad - Mt Pig", --11
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Finger01", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Finger01", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Toe1", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Toe1", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)
		
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/quad_Pig.mkr") --marker file
		quadrupedSkelType()
	)	

	if skelType == ddListArray[12] do --"Quad - Rat", --12
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Forearm", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Forearm", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Foot", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Foot", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)		
		
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/loQuad_Rat.mkr") --marker file
		lowQuadrupedSkelType()
	)	
	
	if skelType == ddListArray[13] do --"Bird Seagull" --13
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Finger00", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Finger00", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Toe0", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Toe0", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)			
		
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/bird_Seagull.mkr") --marker file
		birdSkelType()
	)	
	
	if skelType == ddListArray[14] do --"Bird - Pigeon" --14
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Finger00", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Finger00", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Toe0", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Toe0", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)		
		
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/bird_Pigeon.mkr") --marker file
		birdSkelType()
	)	

	if skelType == ddListArray[15] do --"Bird - Hen" --15
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Clavicle", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Clavicle", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Toe0", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Toe0", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)			
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/bird_Hen.mkr") --marker file
		henSkelType()
	)	

	if skelType == ddListArray[16] do --"Bird - Crow" --16
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Finger00", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Finger00", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Toe0", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Toe0", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)			
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/bird_Crow.mkr") --marker file
		birdSkelType()
	)	

	if skelType == ddListArray[17] do --"Bird - Chikenhawk" --17
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Finger00", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Finger00", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Toe0", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Toe0", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)
		
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/bird_ChickenHawk.mkr") --marker file
		birdSkelType()
	)	

	if skelType == ddListArray[18] do --"Bird - Cormorant" --18
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Finger00", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Finger00", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Foot", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Foot", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)
				
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/bird_Cormorant.mkr") --marker file
		birdSkelType()
	)	
	
	if skelType == ddListArray[19] do --"Aqua - Fish" --19
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Clavicle", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Clavicle", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Thigh", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Thigh", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)
		
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/aqua_Fish.mkr") --marker file
		fishSkelType()
	)		
	
	if skelType == ddListArray[20] do --"Aqua - Shark" --20
	(
		--array of which joints the PH nodes are attached to
		phParents = #( --derived from rottweiler so may need adjusting?
			#("SKEL_L_Clavicle", "Marker_PH_L_Hand", "PH_L_Hand", "IK_L_Hand"),
			#("SKEL_R_Clavicle", "Marker_PH_R_Hand", "PH_R_Hand", "IK_R_Hand"),
			#("SKEL_L_Thigh", "Marker_PH_L_Foot", "PH_L_Foot", "IK_L_Foot"),
			#("SKEL_R_Thigh", "Marker_PH_R_Foot", "PH_R_Foot", "IK_R_Foot")
		)
		
		doMkrFilesExist (theProjectRoot + "art/peds/Skeletons/mrSkeleton/aqua_Shark.mkr") --marker file
		fishSkelType()
	)			
)	