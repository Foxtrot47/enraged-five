--tool to auto generate the marker arrays used as defaults for the skeleton generation tool

masterMarkerArray = #()	
-- 	markerParentingArray = #()
-- 	regularParentingMarkerArray = #()		

fn buildNewMarkerSet = 
(
	
	
	for m = 1 to masterMarkerArray.count do
	(
		marker = Point pos:[0,0,0]
		marker.transform = masterMarkerArray[m][2]
		marker.size = 0.1
		marker.cross = on
		marker.centermarker = off
		marker.axistripod = off
		marker.box = off
		marker.wirecolor = green	
		marker.name = masterMarkerArray[m][1]
	)
	
	for m = 1 to masterMarkerArray.count do
	(
-- 		print ("masterMarkerArray["+(m as string)+"][3] = "+masterMarkerArray[m][3])
		if (masterMarkerArray[m][3] as string) != "undefined" do
		(
			thisPar = getNodeByName masterMarkerArray[m][3]
			marker = getNodeByName masterMarkerArray[m][1]
			print ("Parented "+marker.name+" to "+thisPar.name)
			marker.parent = thisPar
		)		
	)
)

fn queryExistingMarkers = 
(
	--FIRST OFF WE NEED TO ENSURE THE Marker_mover & Marker_ROOT TRANSLATIONS MATCH
	
	aPos = ($Marker_mover.transform as string)
	bPos = ($Marker_ROOT.transform as string)
	
	if aPos != bPos then
	(
		Messagebox ("WARNING! Marker_mover transform does not match Marker_root transform. This may produce invalid markers.") beep:true title:"Cannot save markers"
		format ("WARNING! Marker_mover transform does not match Marker_root transform. This may produce invalid markers."+"\n")
	)
		
		markerObjects = #()
		
		for o in objects do
		(
			if (substring o.name 1 6) == "Marker" do
			(
				debugPrint ("Found maker "+o.name)
				append markerObjects o
			)
		)
		
		debugPrint ((markerObjects.count as string)+" total Markers found in scene.")
		
		masterMarkerArray = #()
		
		output_name = getSaveFileName caption:"Marker output file" types:"MarkerData (*.mkr)|*.mkr|All Files (*.*)|*.*|"
	-- 		
		if output_name != undefined then 
		(
			deleteFile output_name
			
			output_file = createfile output_name		

			format ("masterMarkerArray = #( "+"\n") to:output_file
	-- 		format ("skelArr = #( "+"\n") to:output_file
			
			for m = 1 to markerObjects.count do --this creates an array of all marker names and their positions
			(
				if markerObjects[m].parent != undefined then
				(
					markerPar = markerObjects[m].parent.name
				)
				else
				(
					markerPar = "undefined"
				)
				thisStr = ("#("+"\""+markerObjects[m].name+"\""+", "+(in coordsys world markerObjects[m].transform as string)+","+"\""+markerPar+"\""+")")
				finalStr = ("append masterMarkerArray "+thisStr)
	-- 			execute finalStr
				if m < markerObjects.count then
				(
					format (thisStr+","+"\n") to:output_file
				)
				else
				(
					format (thisStr+"\n") to:output_file
				)
			)	

			format (")") to:output_file
			
	-- 		format ("masterMarkerArray ="+(masterMarkerArray as string)) to:output_file
			
			close output_file
				--edit output_name --opens the file in a maxscript window so it can be checked.
			
		)--end if	
	
)

fn queryTwoMarkerSets = 
(
	clearListener()
	markrs = #($Marker_ROOT, $Marker_Spine_Root, $Marker_Spine0, $Marker_Spine1, $Marker_L_Clavicle, $Marker_L_UpperArm, $Marker_L_Forearm, $Marker_L_Hand, $Marker_L_Finger00, $Marker_L_Finger01, $Marker_L_Finger01_NUB, $Marker_R_Clavicle, $Marker_R_UpperArm, $Marker_R_Forearm, $Marker_R_Hand, $Marker_R_Finger00, $Marker_R_Finger01, $Marker_R_Finger01_NUB, $Marker_Spine2, $Marker_Spine3, $Marker_Neck_1, $Marker_Neck_2, $Marker_head, $Marker_Facial_root, $Marker_ear_r, $Marker_ear_l, $Marker_Eyelid_r, $Marker_Eyelid_l, $Marker_Eye_r, $Marker_Eye_l, $Marker_nose_l, $Marker_nose_r, $Marker_BiteMuscle_l, $Marker_BiteMuscle_r, $Marker_UpLipz, $Marker_UpLip1_l, $Marker_UpLip1_r, $Marker_Jaw, $Marker_LowLip1, $Marker_LowLip1_l, $Marker_LowLip1_r, $Marker_Pelvis1, $Marker_Pelvis, $Marker_L_Thigh, $Marker_L_Calf, $Marker_L_Foot, $Marker_L_Toe0, $Marker_L_Toe1, $Marker_L_Toe1_NUB, $Marker_R_Thigh, $Marker_R_Calf, $Marker_R_Foot, $Marker_R_Toe0, $Marker_R_Toe1, $Marker_R_Toe1_NUB, $Marker_tail_01, $Marker_tail_02, $Marker_tail_m_01, $Marker_tail_m_02, $Marker_tail_m_03, $Marker_tail_m_03NUB)
	
	for o = 1 to markrs.count do
	(
		marker = markrs[o]
		oldMarker = getNodeByName ("OLD_"+marker.name)
		
		markerPos = (in coordsys world marker.position)
		oldMarkerPos = (in coordsys world oldmarker.position)		
		
-- 		markerPosX = (abs (in coordsys world marker.position[1]))
-- 		markerPosY = (abs (in coordsys world marker.position[2]))	
-- 		markerPosZ = (abs (in coordsys world marker.position[3]))		
-- 		oldMarkerPosX = (abs (in coordsys world oldmarker.position[1]))
-- 		oldMarkerPosY = (abs (in coordsys world oldmarker.position[2]))
-- 		oldMarkerPosZ = (abs (in coordsys world oldmarker.position[3]))			
		
-- 		finalPosX = markerPosX - oldMarkerPosX
-- 			print ("finalPosX = "+(finalPosX as string))
-- 		finalPosY = markerPosY - oldMarkerPosY
-- 			print ("finalPosY = "+(finalPosY as string))
-- 		finalPosZ = markerPosZ - oldMarkerPosZ
-- 			print ("finalPosZ = "+(finalPosZ as string))	
-- 			
-- 		finalPos = [finalPosX, finalPosY, finalPosZ]	
-- 		print("finalPos = "+(finalPos as string))
		if markerPos != oldMarkerPos do
-- 		if finalPos != [0,0,0] do
		(
			print "--------"
			print (marker.name+"     = "+(in coordsys world marker.position as string)+" is different to:")
			print (oldmarker.name+" = "+(in coordsys world oldmarker.position as string))
		)
	)
)




if ((markerGenGUI!= undefined) and (skelGenGUI.isDisplayed)) do
	(destroyDialog skelGenGUI)

rollout markerGenGUI "Skel Gen"
(
-- 	dropDownList ddSkelType "Type" items:ddListArray
-- 	spinner spnBoneSize "Bone Width" default:0.05 range:[-100.000,100.000,0.000]
-- 	spinner spnFinSize "Fin Size" default:0.01
	button btnQueryMarkers "Gen Marker File" width:110
	button btnGenMarkers "Gen Marker Objs" width:110
	button btnQueryTwoMarkrs "Query 2 markers " width:110

	
	on btnQueryMarkers pressed do
	(
		queryExistingMarkers()
	)

	on btnGenMarkers pressed do
	(
		buildNewMarkerSet()
	)
	
	on btnQueryTwoMarkrs pressed do
	(
		queryTwoMarkerSets()
	)
	
)

CreateDialog markerGenGUI width:125 pos:[1450, 100] 