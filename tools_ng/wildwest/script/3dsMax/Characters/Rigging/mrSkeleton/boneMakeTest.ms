bonePrefix = "SKEL_"

parentNode = $Marker_Pelvis1
childNode = $Marker_Spine1
parentPos = in coordsys world parentNode.position
childPos = in coordsys world childNode.position

-- debugprint ("ParentNode.name = "+parentNode.name)

thisBoneName = (bonePrefix+(substring parentNode.name 7 100))
doesBoneExist = getNodeByName thisBoneName
	
if doesBoneExist == undefined do
(
	(
		createdBone = boneSys.createBone parentPos childPos  [-1,0,0]
		createdBone.width = defaultbonewidth 
		createdBone.height = defaultbonewidth 
		createdBone.sidefins = on
		createdBone.sidefinssize = defaultfinsize 			
		createdBone.Name = thisBoneName
	)
)