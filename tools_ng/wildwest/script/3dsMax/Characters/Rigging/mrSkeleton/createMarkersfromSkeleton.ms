-- Load the common maxscript functions 
--include "rockstar/export/settings.ms" -- This is SLOW! 
filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()


-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_bone_tagger.ms")
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_Rigging.ms")
filein (theProjectRoot + "tools/dcc/current/max2011/scripts/pipeline/util/xml.ms")


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
ddListArray = #(
	"------",
	"Human",
	"Quadruped",
	"Bird",
	"Custom"
	)

markerParentingArray  = #() --dynamically created multi dim array with parent as first node then children as next ones	this is different to the reg version as this makes use of ensuring that only centre line nodes are used 
regularParentingMarkerArray = #() --dynamically created multi dim array with parent as first node then children as next ones	
	
skelType = ddListArray[1]	
	
defaultbonewidth = 0.05
-- defaultbonewidth = 3
defaultfinsize = 0.01

SkelArr = #()
skelType = undefined
	
debugPrintVal = true
	
rotationOffsetOffNodes = undefined --this gets defined depending upon which skeleton type we choose
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn debugPrint printStr = --this is a little function to replace the debugPrint command. This allows us then to be able to globally turn on and off debugPrint cstatements.
(
	if debugPrintVal == true do
	(
		Print printStr
	)
)	

--FOR MARKER ARR IT GOES CHILD IN 1ST ELEMENT AND PARENT IN 2ND	
	

fn defineSkeletonSettings skelType = 
(
	if skelType == ddListArray[2] do --human
	(
			rotationOffsetOffNodes = #(
		)
	)
	
	if skelType == ddListArray[3] do --quad
	(
-- 		messagebox ("setting skeleton array to Quad settings")
		
		rotationOffsetOffNodes = #(
			"Stretchy_Spine1",
			"Stretchy_Pelvis",
			"Stretchy_Facial_root"
			)		
	)
	
	if skelType == ddListArray[4] do --bird
	(
			rotationOffsetOffNodes = #(
		)		
	)	
)

fn removeStretchyCallBack = 
(
	deleteAllChangeHandlers()
	print "Change handlers removed."
)

fn addBoneLengthCallback thisBone parentMarker childMarker =
(
	theBoneNode = getNodeByName thisBone
	parentNode = getNodeByName parentMarker
	childNode = getNodeByName childMarker
	
	if theBoneNode != undefined do
	(
		execStr = ("when transform $"+thisBone+" changes do "+"\r\n"+"("+"\r\n\t"+"$"+thisBone+".length = distance $"+parentMarker+" $"+childMarker+"\r\n"+")")
		execute execStr		
		print ("Bone length change callback activated for "+thisBone+".")
	)
-- 	
-- 		when transform $Stretchy_tail_01 changes do
-- 		(
-- 			theBoneNode.length = distance $Marker_tail_01 $Marker_tail_02
-- 		)	
)

fn applyConstraints marker markerChild thisBone =
(

	debugPrint ("Constraining "+thisBone.name+" positionally to "+marker.name+". Looking at "+markerChild.name)
	thisBone.position.controller.available.controller = Position_Constraint ()
	ConstraintInterface = thisBone.position.controller.position_Constraint
	ConstraintInterface.appendTarget marker 50 --setting to 50 allows bone stretchiness
	ConstraintInterface.relative = false

	--now we need to add the lookat pointing to the child of marker
	thisBone.rotation.controller.available.controller = LookAt_Constraint ()
	ConstraintInterface = thisBone.rotation.controller.LookAt_Constraint
	ConstraintInterface.appendTarget markerChild 50
	ConstraintInterface.viewline_length_abs = false
	ConstraintInterface.relative = true

	--now we need to set the bone length

	thisBone.length = (distance marker markerChild)

	if thisBone.name == "Stretchy_Spine2" do --may need to change this to looping thru an array of 'coincident bones'
	(	
		markerParent = marker.parent
		
		debugPrint ("Marker Constraining "+marker.name+" positionally to "+markerParent.name)
		marker.position.controller.available.controller = Position_Constraint ()
		ConstraintInterface = marker.position.controller.position_Constraint
		ConstraintInterface.appendTarget marker 100
		ConstraintInterface.relative = false			
	)

)

fn createSkeletonOLD isStretchy  = 
(
	--isStretchy is either true or false
	objArray = objects
	markerArr = #(#(),#())
	tmpArr = #()
	
	--first build an array of the marker nodes
	for o = 1 to objArray.count do
	(
		if (substring objArray[o].name 1 6) == "Marker" do
		(
			debugPrint ("Adding "+objArray[o].name+" to tmpArr")
			appendIfUnique tmpArr objArray[o]
		)
-- 			debugPrint "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
	)
	
	for o = 1 to tmpArr.count do
	(
		chi = tmpArr[o]
		thisArr = undefined
		par = tmpArr[o].parent
-- 			if par != undefined then
-- 			(
		if par == undefined do
		(
			par = "undefined"
		)
			debugPrint ("child = "+chi.name+". Parent = "+(par as string))
			append markerArr[1] chi
			append markerArr[2] par			
	)	
	
	debugPrint "-------------------------------------------------------------------------------------------------------------------------------------------------------"
	debugPrint ("MarkerArr[1].count = "+(markerArr[1].count as string)+". MarkerArr[2].count = "+(markerArr[2].count as string))
	debugPrint "-------------------------------------------------------------------------------------------------------------------------------------------------------"
-- 	cui.commandPanelOpen = false
-- 		
 	--now we need to make the stretchy skeleton joints.
	for i = 1 to markerArr[1].count do
	(
		debugPrint ("Index = "+(i as string))

		debugPrint ("[1]["+(i as string)+"] = "+(markerArr[1][i] as string))						
		debugPrint ("[2]["+(i as string)+"] = "+(markerArr[2][i] as string))				
		
		parentMarker = (markerArr[2][i])
		if (parentMarker as string) != "undefined" do
		(
			debugPrint ("Found parent in index "+(i as string)+" - "+(markerArr[2][i] as string))
			parentPos = parentMarker.position
			childPos = undefined
			childMarker = (markerArr[1][i])
			debugPrint ("Looking for child in index "+(i as string)+" - "+(markerArr[2][i] as string))
			
			thisBoneName = ("Stretchy"+(substring markerArr[2][i].name 7 100))
			doesBoneExist = getNodeByName thisBoneName 
			
			if doesBoneExist == undefined do --we need to ensure we dont create a bone multiple times
			(
				createdBone = undefined
				
				if (childMarker as string) != "undefined" then
				(
					childPos = childMarker.position
-- 					createdBone = boneSys.createBone parentPos childPos  [1,0,0]
					createdBone = boneSys.createBone parentPos childPos  [-1,0,0]
				)
				else
				(
					childPos = [(parentPos[1] + 0.1), parentPos[2],parentPos[3]]
					createdBone = boneSys.createBone parentPos childPos [0,1,0]
				)
				
				--need to figure out how to set orientation of the bone...
-- 				print ("parentMarker.rotation = "+(parentMarker.rotation as string))
-- 				in coordsys world createdBone.Rotation = in coordsys world parentMarker.rotation
-- 				print ("createdBone.Rotation = "+(createdBone.Rotation  as string))
-- 				print "rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr"
					
		-- 		createdBone = boneSys.createBone childPos parentPos [1,0,0]
				createdBone.width = defaultbonewidth 
				createdBone.height = defaultbonewidth 
				createdBone.sidefins = on
				createdBone.sidefinssize = defaultfinsize 			
				createdBone.Name = thisBoneName
					
				if (substring createdBone.name 9 3) == "_L_" then
				(
					bCol = Green
				)
				else
				(
					if (substring createdBone.name 9 3) == "_R_" then
					(
						bCol = Red
					)
					else
					(
						bCol = Blue
					)
				)
				createdBone.wirecolor = bCol
			)
		)
	)
	
	if isStretchy == true do
	(
		debugPrint "------------------------------------------------------------------------"
		debugPrint "Starting Constraints..."
		
		--now we constrain those stretchy skel nodes to the markers
		for s = 1 to markerArr[1].count do
		(
			debugPrint ("s INDEX = "+(s as string)+".")
			--first we find the skeleton then constrain it positionally to the corresponding marker
			if (markerArr[2][s] as string) != "undefined" do
			(
				debugPrint ("Looking for "+"Stretchy"+(substring markerArr[2][s].name 7 100))
				thisBone = getNodeByName ("Stretchy"+(substring markerArr[2][s].name 7 100))
				
				if thisBone != undefined do
				(
					select thisBone 
						
					freezeTransform()
						
					markerChild = (markerArr[1][s])
						
					if markerChild == undefined do 
					(
						debugPrint ("Couldn't find CHILD "+markerArr[1][s])
					)
						
					marker = (markerArr[2][s])
					debugPrint ("marker = "+(marker as string))
					
					if (marker as string) != "undefined" then
					(
						debugPrint ("WEWT Found (Parent) "+marker.name)
						if (markerChild as string) != "undefined" do
						(
							debugPrint ("WEWT Found (Child) "+markerChild.name)
			-- 				if thisBone.name == "Stretchy_Spine3" do
			-- 				(
			-- 					messagebox (("marker = "+(marker.name))+(". markerChild = "+(markerChild.name))+(". thisBone = "+thisBone.name)	)
			-- 				)
							debugPrint "---------------------------------------------"
							debugPrint ("marker = "+(marker.name))
							debugPrint ("markerChild = "+(markerChild.name))	
							debugPrint ("thisBone = "+thisBone.name)
							debugPrint ("Going to constrain from "+(marker.name)+" and "+(markerChild.name))
							applyConstraints marker markerChild thisBone
							debugPrint "---------------------------------------------"
						)
					)
					else
					(
						debugPrint ("Couldn't find CHILD "+markerArr[1][s].name+" so skipping.")
					)
				)
			)
		)
		
		for i = 1 to rotationOffsetOffNodes.count do
		(
			turnMeOff = getNodeByName rotationOffsetOffNodes[i]
			if turnMeOff.rotation.controller.LookAt_Constraint != undefined do
			(
				ConstraintInterface = turnMeOff.rotation.controller.LookAt_Constraint
					
				debugPrint  ("Turning off ofset on "+turnMeOff.name)
			
				ConstraintInterface.relative = false
			)
		)		

		debugPrint "Finished Constraints..."
	)
	cui.commandPanelOpen = true
	
-- 	now all the bones are made we need to parent them

		debugPrint ("markerArr[1].count  = "+(markerArr[1].count  as string))
		for i = 1 to markerArr[1].count do
		(
			print ("i = "+(i as string))
			if (markerArr[2][i] as string) != "undefined" do
			(
-- 				print ("Marker[2]["+(i as string)+"] = "+(markerArr[2][i] as string) )
-- 				debugPrint ("Looking for PAR "+("Stretchy"+(substring markerArr[2][i].name 7 100)))
				BoneParent = getNodeByName ("Stretchy"+(substring markerArr[2][i].name 7 100))
-- 					if boneParent != undefined do (print "Found PAR"+boneParent.name)
				if (markerArr[1][i] as string) != "undefined" do
				(
-- 					print ("Looking for CHI "+("Stretchy"+(substring markerArr[1][i].name 7 100)))
					BoneChild = getNodeByName ("Stretchy"+(substring markerArr[1][i].name 7 100))
						if boneChild != undefined then
						(
-- 							print "Found CHI "+boneChild.name
							boneChild.parent = boneParent
							debugPrint ("Linked "+boneChild.name+" to "+boneParent.name)
						)
						else
						(
							debugPrint ("Couldn't find child: "+("Stretchy"+(substring markerArr[1][i].name 7 100)))
						)
				)
			)
		)	
		debugPrint markerArr
)

fn createSkeleton bonePrefix isStretchy = 
(
	for i = 1 to markerParentingArray.count do
	(
		for m = 2 to markerParentingArray[i].count do --only do from 2 cos 1 is always the parent
		(
			parentNode = getNodeByName markerParentingArray[i][1]
			childNode = getNodeByName markerParentingArray[i][m]
			parentPos = parentNode.position
			childPos = childNode.position
			
			
			thisBoneName = (bonePrefix+(substring parentNode.name 7 100))
			doesBoneExist = getNodeByName thisBoneName
			
			if doesBoneExist == undefined do
			(
				createdBone = boneSys.createBone parentPos childPos  [-1,0,0]
				createdBone.width = defaultbonewidth 
				createdBone.height = defaultbonewidth 
				createdBone.sidefins = on
				createdBone.sidefinssize = defaultfinsize 			
				createdBone.Name = thisBoneName
					
				if (substring createdBone.name 9 3) == "_L_" then
				(
					bCol = Green
				)
				else
				(
					if (substring createdBone.name 9 3) == "_R_" then
					(
						bCol = Red
					)
					else
					(
						bCol = Blue
					)
				)
				createdBone.wirecolor = bCol			
			)
		)
	)
	
	--now we need to parent the skeleton
	for i = 1 to regularParentingMarkerArray.count do
	(
		for m = 2 to regularParentingMarkerArray[i].count do
		(
			parentNode = getNodeByName (bonePrefix+(substring regularParentingMarkerArray[i][1] 7 100))
			childNode = getNodeByName (bonePrefix+(substring regularParentingMarkerArray[i][m] 7 100))
			
			if childNode != undefined then
			(
				childNode.parent = parentNode
				debugPrint ("Linking "+childNode.name+" to "+parentNode.name)
			)
			else
			(
				debugPrint ("Couldn't find "+(bonePrefix+(substring regularParentingMarkerArray[i][m] 7 100)))
			)
		)
	)
	--now we need to freeze the skeleton transforms
	clearSelection()
	for o in objects do
	(
		bonePrefixLength = bonePrefix.count
		if (substring o.name 1 (bonePrefixLength)) == bonePrefix do
		(
			selectMore o
		)
	)
	
	FreezeTransform()
	
-- isStretchy = false
	if isStretchy == true do
	(
		print "Preparing Stretchy Skeleton..."
		
		for i = 1 to markerParentingArray.count do
		(
			for m = 2 to markerParentingArray[i].count do	
			(
				thisBone = getNodeByName ("Stretchy"+(substring markerParentingArray[i][1] 7 100))
				positionMarker = getNodeByName markerParentingArray[i][1]
				aimMarker = getNodeByName markerParentingArray[i][m]
				
				if thisBone != undefined do
				(
					if positionMarker != undefined do
					(
						if aimMarker != undefined do
						(
							applyConstraints positionMarker aimMarker thisBone
							addBoneLengthCallback thisBone.name positionMarker.name aimMarker.name
							thisBone.boneEnable=false
						)
					)
				)
			)
		)
	)
	
-- 		when transform $Stretchy_tail_01 changes do
-- 		(
-- 			$Stretchy_tail_01.length = distance $Marker_tail_01 $Marker_tail_02
-- 		)	

)

fn createMarkerArrays = 
(
	
	markerObjects = #()
	
	for o in objects do
	(
		if (substring o.name 1 6) == "Marker" do
		(
			debugPrint ("Found maker "+o.name)
			append markerObjects o
		)
	)
	
	debugPrint ((markerObjects.count as string)+" total Markers found in scene.")
	
	markerParentingArray = #()
	regularParentingMarkerArray = #()		
	
	for m = 1 to markerObjects.count do
	(
		ignoreThis = false
		for tst = 1 to rotationOffsetOffNodes.count do
		(
-- 			messagebox ("rotationOffsetOffNodes["+(tst as string)+"] = "+rotationOffsetOffNodes[tst]+". MarkerObjects["+(m as string)+"] = "+markerObjects[m].name)
			if ( substring rotationOffsetOffNodes[tst] 10 100 ) == (substring markerObjects[m].name 8 98)do
			(
				ignoreThis = true
			)
		)
		
			debugPrint ("current marker = "+markerObjects[m].name)
			debugPrint ("child count = "+(markerObjects[m].children.count as string))
			if markerObjects[m].children.count != 0 then
			(
				markerChildArray = markerObjects[m].children
				debugPrint ("markerChildArray = "+(markerChildArray as string))
				
				thisStr = ("\""+markerObjects[m].name+"\"")
				for i = 1 to markerChildArray.count do
				(
					thisStr = (thisStr+", "+"\""+(markerChildArray[i].name)+"\"")
				)
				thisStr = (thisStr)
				finalStr = ("append regularParentingMarkerArray "+"#("+thisStr+")")
				debugPrint ("finalStr = "+finalStr)
				execute finalStr
			)

		
		if ignoreThis != true then
		(
			debugPrint ("current marker = "+markerObjects[m].name)
			debugPrint ("child count = "+(markerObjects[m].children.count as string))
			if markerObjects[m].children.count != 0 then
			(
				markerChildArray = markerObjects[m].children
				debugPrint ("markerChildArray = "+(markerChildArray as string))
				
				thisStr = ("\""+markerObjects[m].name+"\"")
				for i = 1 to markerChildArray.count do
				(
					thisStr = (thisStr+", "+"\""+(markerChildArray[i].name)+"\"")
				)
				thisStr = (thisStr)
				finalStr = ("append markerParentingArray "+"#("+"\r\n"+thisStr+")")
				debugPrint ("finalStr = "+finalStr)
				execute finalStr
			)
		)
		else
		(
			--do stuff for children nodes here
			if markerObjects[m].children.count != 0 do
			(
				--we need to look at all the children and find which child is on the same x position
				markerChildArray = markerObjects[m].children
				
-- 				childXPos = 10000
				childXPos = in coordsys world markerChildArray[1].position[1]
				smallestChild = undefined
				for x = 1 to markerChildArray.count do
				(
					thisXPos = (in coordsys world markerChildArray[x].position[1]) 
					closeToZero = 0
					
					if thisXPos < 0 then
					(
						thisXPos = (0 - thisXpos)
					)
					else
					(
						thisXPos = (0 + thisXpos)
					)
					
					if childXPos < 0 do (childXPos = childXPos * -1) --convert so its a positive sign 
					if thisXpos < 0 do (thisXPos = thisXpos * -1) --convert so its a positive sign 
					
-- 					messagebox (markerChildArray[x].name+" x pos = "+(thisXPos as string))
					if thisXPos < childXPos do
					(
						
						childXPos = markerChildArray[x].position[1] 
						smallestChild = markerChildArray[x]
-- 						messagebox ("\r\n"+"<<<<<<<<<<< USING "+smallestChild.name+" as smallest for "+markerObjects[m].name+"\r\n")
					)
				)
				
				thisStr = ("\""+markerObjects[m].name+"\"")
				thisStr = (thisStr+", "+"\""+(smallestChild.name)+"\"")
				
				thisStr = (thisStr)
				finalStr = ("append markerParentingArray "+"#("+"\r\n"+thisStr+")")
				debugPrint ("finalStr = "+finalStr)
				execute finalStr		
			)			
		)
-- 		messagebox ("regularParentingMarkerArray.count = "+(regularParentingMarkerArray.count as string))		
		debugPrint "---------"
	)
	
	debugPrint ("markerParentingArray.count = "+(markerParentingArray.count as string))
	debugPrint ("markerParentingArray = "+(markerParentingArray as string))
)

fn createMarkersFromSkeleton = 
(
	SkelArr = #()
	markArr = #()
	objArray = objects as array

	
	--first build an array of the skeleton nodes
	for o = 1 to objArray.count do
	(
		if (substring objArray[o].name 1 4) == "SKEL" do
		(
			chi = objArray[o]
			par = objArray[o].parent
			
			if par != undefined do
			(
				skelStr = ("#(("+"\""+chi.name +"\""+"),("+"\""+par.name+"\""+"))")
					thisArr = execute skelStr

				appendIfUnique skelArr thisArr 
				debugPrint skelStr
			)
		)
	)
	debugPrint "============================================================================="
	debugPrint skelArr
	debugPrint ("skelArr count = "+(skelArr.count as string))
	debugPrint "============================================================================="
	
	cui.commandPanelOpen = false
		
	if skelArr.count != 0 then		
	(
		--first we create the markers
		for skl = 1 to skelArr.count do
		(
	-- 		debugPrint ("skelArr["+(skl as string)+"][2] = "+skelArr[skl][2])
			thisBone = getNodeByName skelArr[skl][1]
			markerName = ("Marker"+(substring thisBone.name 5 100))
			doesMarkerExist = getNodeByName markerName 
				if doesMarkerExist != undefined do --remove any existing markers
				(
					delete doesMarkerExist
				)
			marker = Point pos:[3.36035,3.35986,0]
			marker.transform = thisBone.transform
			marker.size = 0.1
			marker.cross = on
			marker.centermarker = off
			marker.axistripod = off
			marker.box = off
			marker.wirecolor = green	
			marker.name = markerName
			debugPrint ("Made "+markerName)
		)
		debugPrint "Markers made..."
		--then using string matching we parent the markers once they are all made
		
		for skl = 1 to skelArr.count do
		(
			marker = getNodeByName ("Marker"+(substring skelArr[skl][1] 5 100))
			markerParent = getNodeByName ("Marker"+(substring skelArr[skl][2] 5 100))
			marker.parent = markerParent
		)
		
		for skl = 1 to skelArr.count do
		(
			marker = getNodeByName ("Marker"+(substring skelArr[skl][1] 5 100))
			select marker 
			freezeTransform()
		)
		debugPrint "Markers parented."
	)
	else
	(
		Messagebox "WARNING! Couldn't find any SKEL joints with which to generate markers." beep:true
	)
	cui.commandPanelOpen = true
)


clearListener()





if ((skelGenGUI != undefined) and (skelGenGUI.isDisplayed)) do
	(destroyDialog skelGenGUI)

rollout skelGenGUI "Skel Gen"
(
	dropDownList ddSkelType "Type" items:ddListArray
-- 	spinner spnBoneSize "Bone Width" default:0.05 range:[-100.000,100.000,0.000]
-- 	spinner spnFinSize "Fin Size" default:0.01
	button btnCreateMarkers "Create Markers" width:110
	button btnStretchyGen "Stretchy Skeleton" width:110
	button btnSkelGen "Final Skeleton" width:110
	button btnDoAll "Do All" width:110
	
	on skelGenGUI open do
	(
		skelType = ddListArray[1]
		debugPrint ("skelType defaulting to "+ddListArray[1])
		defaultbonewidth = 0.05
		defaultfinsize = 0.01
	)
	
	
	on ddSkelType selected i do
	(
		skelType = ddListArray[i]
		debugPrint ("You picked "+skelType)		
		defineSkeletonSettings skelType
	)
	
-- 	on spnBoneSize changed do
-- 	(
-- 		defaultBonewidth = spnBoneSize.value
-- 		print ("defaultBonewidth = "+(defaultBonewidth  as string))
-- 	)
-- 	
-- 	on spnFinSize changed do
-- 	(
-- 		defaultFinSize = spnFinSize.value
-- 		print ("defaultFinSize = "+(defaultFinSize  as string))
-- 	)
	
	on btnCreateMarkers pressed do
	(
		if skelType != ddListArray[1] then
		(
			createMarkersFromSkeleton()
		)
		else
		(
			print "Please make a valid skeleton type selection"
		)		
	)

	on btnStretchyGen pressed do
	(
		if skelType != ddListArray[1] then
		(	
			createMarkerArrays()
-- 			createStretchyRig()
			createSkeleton "Stretchy" true
		)
		else
		(
			print "Please make a valid skeleton type selection"
		)		
	)
	
	on btnSkelGen pressed do
	(
		if skelType != ddListArray[1] then
		(		
			createMarkerArrays()
-- 			createStretchyRig()
			createSkeleton "SKEL" false
		)
		else
		(
			print "Please make a valid skeleton type selection"
		)		
	)

	on btnDoAll pressed do
	(
		if skelType != ddListArray[1] then
		(
			createMarkerArrays()
			createSkeleton() 
		)
		else
		(
			print "Please make a valid skeleton type selection"
		)
	)	
)

CreateDialog skelGenGUI width:125 pos:[1450, 100] 