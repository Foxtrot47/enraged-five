fn whenBoneChangesDo =
(
	when transform $stretchy_Pelvis changes do
	(
		$Stretchy_Pelvis.length = distance $Marker_Pelvis $Marker_tail_01
	)
)

whenBoneChangesDo()