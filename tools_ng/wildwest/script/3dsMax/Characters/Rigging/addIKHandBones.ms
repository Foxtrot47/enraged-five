-- script to add IK_Hand joints to rigs
--Matt Rennie
--Aug 2011

fn addIKBones = 
(
-- 	headGeo = getnodebyName "head_000_r"
	headGeo = undefined
	
	for o in objects do
	(
		if (substring o.name 2 8) == "ead_000_" do
		(
			headGeo = o
		)
	)
	
-- 	if headGeo != undefined then
-- 	(
		
		currentlySelected = undefined
		if selection.count != 0 do (currentlySelected = selection as array)
		
		max select all
		existingShit = selection as array
		clearSelection()
		
		if currentlySelected != undefined do (select currentlySelected)
		
		foundL = false
		foundR = false
		nameL = "IK_L_Hand"
		nameR = "IK_R_Hand"
		
		fL = getNodeByName nameL
	
		if fL != undefined do (foundL = true)
		
		fR = getNodeByName nameR
		
		if fR != undefined do (foundR = true)
	
		for i = 1 to existingShit.count do
		(
			if existingShit[i].name == nameL do
			(
				foundL = true
			)
			if existingShit[i].name == nameR do
			(
				foundR = true
			)
		)
		
		if foundL == false then --ok we need to make the IK nodes
		(
			phNode = getNodeByName "PH_L_Hand"
			
			newName = nameL
			maxOps.cloneNodes phNode cloneType:#copy actualNodeList:&c newNodes:&nnl
			ikObj= nnl[1]
			ikObj.name = newName
			print ("Created "+newName)
			--now add to the skiun modifier
			if headGeo != undefined do
			(
				select headGeo
				max modify mode 
				modPanel.setCurrentObject headGeo.modifiers[#Skin] 
				skinMod = headGeo.modifiers[#Skin]
				skinops.addBone skinMod ikObj 1
			)
			setUserPropBuffer ikObj ("tag = "+newName+"\r\n"+"exportTrans = True")
		)
		else
		(
			print (nameL+" already exists so not recreating.")
		)
		
		if foundR == false then --ok we need to make the IK nodes
		(
			phNode = getNodeByName "PH_R_Hand"
			
			newName = nameR
			maxOps.cloneNodes phNode cloneType:#copy actualNodeList:&c newNodes:&nnl
			ikObj= nnl[1]
			ikObj.name = newName
			print ("Created "+newName)
			
			--now add to the skiun modifier
			if headGeo != undefined do
			(
				select headGeo
				max modify mode 
				modPanel.setCurrentObject headGeo.modifiers[#Skin] 
				skinMod = headGeo.modifiers[#Skin]
				skinops.addBone skinMod ikObj 1
			)
			setUserPropBuffer ikObj ("tag = "+newName+"\r\n"+"exportTrans = True")
		)	
		else
		(
			print (nameR+" already exists so not recreating.")
		)
-- 	
-- 	)
-- 	else
-- 	(
-- 		messagebox ("WARNING! Could not find head_000_r. We cannot continue")
-- 	)
)

addIKBones()