controllersToInclude = #(
	"Float_Expression"--,
-- 	"Controller:Float_Expression" --,
-- 	"SubAnim:RsSpring"	
	)

scalarMin = undefined
scalarMax = undefined
scalarDefault = undefined

ncaName = undefined

cattFile = undefined
ncaFile = undefined


driverXML = (maxFilePath+"driverXML.xml") 
cAttXML = (maxfilepath+"cAttXML.xml")

	ncaName = driverXML
	cattName = cAttXML

debugPrintVal = false

global startedNcaFile = false --used to track if the ncafile has been opened previously
-- global startedCattFile = false --used to track if the cvatt file has been opened previously

addedDriven = false --used to check if the opening section of a driven has been added
-- addedCattStart = false --used to check if the opening section of a driven has been added

-- foundCAObject  = #() --used to track if we have queried an attr from this object already

caDataArray = #(
	#(), --start of catt section (we can then look for duplicates of this in element 2 to know they go tohether
	#() --name of ca and its params
	)
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

fn debugPrint printStr = --this is a little function to replace the debugPrint command. This allows us then to be able to globally turn on and off debugPrint cstatements.
(
	if debugPrintVal == true do
	(
		Print printStr
	)
)	


fn conCatXML = --concats the driver and custom attribute xml files into one final xml
(
	thisPath = maxFilePath
	thisMaxFile = (filterstring (maxFileName as string) ".")
		thisMaxFile = thisMaxFile[1]
	output_name = (thisPath+"/"+thisMaxFile+"_RSNXML.xml")
	
	debugPrint ("Trying to concat to "+output_name)
-- 	messagebox ("Trying to concat to "+output_name)
	
	--first read in the caat data 
	cattXmlData = #()	
-- 	catt_name = cAttXML
	
	foundCattXml = doesfileExist cAttXML
	if foundCattXml == true do
	(
		f = openfile cattName
		while not eof f do
		(
			append cattXmlData (filterstring (readLine f) "�")
		)
		close f
	)	  
	--now read in driver data
	driverxmlData = #()
-- 	driver_name = driverXML
	foundDriverXML = doesfileexist driverXML
	if foundDriverXML == true do
	(
		f = openfile ncaName
		while not eof f do
		(
			thisLine = (readLine f)
			thisValue = (filterstring (thisLine) "�")
-- 			append driverxmlData (filterstring (readLine f) "�")
			append driverxmlData thisValue
		)
		close f	
	)
		
	if output_name != undefined then 
	(
		output_file = createfile output_name		
		
		format ("<?xml version="+"\""+"1.0"+"\""+"?>"+"\n"+"<drivers>"+"\n") to:output_file
		
		if foundCattXml == true do
		(
			for ca = 1 to cattXMLData.count do
			(
				format (cattXMLData[ca][1]+"\n") to:output_file
			)
		)

		if foundDriverXML == true do
		(
			for dr = 1 to driverXMLData.count do
			(
				format (driverXMLData[dr][1]+"\n") to:output_file
			)		
		)
		
		format ("</drivers>") to:output_file
		
		close output_file
	)		
	
	debugPrint ("Expressions exported to: "+output_name)
)

fn prepCattXML = --takes data from the custom attribute data array filled during readExpression to build custom attribute xml file
(
	cattFile = createfile cattName
	
	for i = 1 to caDataArray[1].count do
	(
		masterLine = caDataArray[1][i]
		if masterLine != "USED" do 
		(
			format (masterLine+"\n") to:cattFile
			--)
			
			for c = 1 to caDataArray[1].count do	
			(
				if caDataArray[1][c] == masterLine do
				(
					debugprint (caDataArray[1][c]+" == "+masterLine )
					--ok we cann take 2nd element and add is a a child of this stub thing
					
					if caDataArray[2][c] != "USED" do
					(
						format (caDataArray[2][c]+"\n") to:cattFile
						
						--we now invalidate this from the array so we dont duplicate?
						caDataArray[1][c] = "USED"
						caDataArray[2][c] = "USED"
						debugprint ("Deleting "+(c as string)+" index from arrays")
					)
				)
			)	
		)
	)
	format (("\t"+"</cAtt>"+"\n")) to:cattFile	
	close cattFile
)

fn queryCARanges obj attrToSearchFor = --queries custom attribute spinner ranges
(
	--attrToSearchFor is a string such as jawBackFwd_A
	--which is the name of the attribute we're looking for
		
	caString = ("CustAttributes.getdefs $"+obj+".modifiers[#Attribute_Holder]")
	caDefs = execute caString

	for def in caDefs do
	(
		debugPrint ("def.name = "+(def.name as string))
		debugPrint ("attrToSearchFor = "+attrToSearchFor)
		if (def.name as string) == (attrToSearchFor) do
		(
			debugPrint def.name --will debugPrint #jawBackFwd_A
			defSourceString = def.source
			
			debugPrint ("----------------"+"\r\n")
			debugPrint (defSourceString as string)
			debugPrint ("\r\n")
			debugPrint ("-----------------")
			
			stringToSearch = "Range:["
			foundString = findString defSourceString stringToSearch
			debugPrint ("FoundString = "+(foundstring as string))
			--foundString will be the character at which Range:[ starts. 
			--We then need to search on from there until we find 
			--a closing ] then we can query inside that
			
			foundString = (foundString  + (stringToSearch.count))
				
			nextStringChunk = (substring defSourceString foundString 40) --using 60 as arbitrary character count
			closingSquare = (findString nextStringChunk "]")
			closingSquare = (closingSquare - 1)
			bracketedChunk = (substring nextStringChunk 1 closingSquare)
			debugPrint ("BC: "+(bracketedChunk as string))
			
			filteredBC= (filterString bracketedChunk ",")
			minVal = filteredBC[1]
			maxVal = filteredBC[2]
			defaultVal = filteredBC[3]

			debugPrint ("min:"+minVal+". max:"+maxVal+". Default:"+defaultVal)
				debugPrint "----------"
				
			scalarMin = minVal
			scalarMax = maxVal
			scalarDefault = defaultVal
		)
	)
)

fn removeCarriageReturnsInExpr theexpr = -- does formatting on the expression strings to remove carriage returns
(

	debugPrint "----------------------"
	debugPrint ("Initial Expression String: "+"\n"+theexpr)
	debugPrint "----------------------"

	thisExprFilt = (filterString theexpr "\n" )

	strCount = thisExprFilt.count
	
	if strCount >0 do
	(
		thisExpr = ""
		for i = 1 to strCount do
		(
			--theexpr = (thisExpr+thisExprFilt[i])
			thisExpr = (thisExpr+thisExprFilt[i])
			debugprint ("newEXPR = "+theExpr)
		)
	)
	theexpr = thisExpr	
	debugPrint ("Final string = "+theexpr)
	
	theexpr
)


fn readExpression controller = 
(
	if startedNcaFile  == false do --set wether we have opened the ncaFile
	(
		startedNcaFile = true
		--first we prep the driver file
		
		ncaFile = createfile ncaName	
	)
	

	
	exprScalarNames=#()
	scalarTargets=#()
	expressionStrings=#()
	
	theExprStr = (controller+".getExpression()")
	
	theexpr = execute theExprStr

	theexpr = removeCarriageReturnsInExpr theexpr 
	
-- 		if addedDriven == false do
-- 		(
-- 			addedDriven = true
-- 			--now we need to find the parameters for the expression object
-- 			filteredController = (filterString controller ".")

-- 	-- 		expCont = filteredController[6]

-- 	--*************************************************		
-- 	--have to find a better way of finding the Z_Rotation / Translation charactersfor expCont as filteredController[x] is not working well
-- 	--*************************************************	

-- 			controllerToFindArray = #(
-- 				"_Position",
-- 				"_position",
-- 				"_Translation",
-- 				"_translation",
-- 				"_Rotation",
-- 				"_rotation",
-- 				"_Scale",
-- 				"_scale"
-- 				)
-- 			
-- 			expCont = undefined 			
-- 				
-- 			for i = 1 to filteredController.count do
-- 			(
-- 				for c = 1 to controllerToFindArray.count do
-- 				(
-- 					ctf = controllerToFindArray[c]
-- 					ctfLength = ctf.count
-- 					
-- 	-- 							print ("Testing "+filteredController[i]+" against "+ctf)
-- 					
-- 					if (substring filteredController[i] 2 (filteredController[i].count - 1)) == controllerToFindArray[c] then
-- 					(
-- 	-- 					print ("YYYYYYYAAAAAAAAYYYYYYY "+(substring filteredController[i] 2 (filteredController[i].count - 1))+" == "+controllerToFindArray[c] )
-- 						expCont = filteredController[i]
-- 					)
-- 					else
-- 					(
-- 	-- 					print ("poo "+(substring filteredController[i] 2 (filteredController[i].count - 1))+" != "+controllerToFindArray[c] )
-- 					)
-- 				)
-- 			)

-- 			expObj = filteredController[1]
-- 			
-- 			if (substring expObj 1 1) == "$" do
-- 			(
-- 				expObj = (substring expObj 2 100)
-- 			)
-- 			
-- 			expString = theexpr
-- 				
-- 			driven = (("\t"+"<driven expCont="+"\""+(expCont)+"\""+" expObj="+"\""+(expObj)+"\""+" expString="+"\""+(expString)+"\""+">")+",")
-- 			format (driven+"\n") to:ncaFile	
-- 		)	
	
	if theexpr  == "0" then --check to see if the expression actually has shit in it.
	(
		debugPrint ("Expression on "+controller+" is a "+"\""+"0"+"\"")
	)
	else
	(
		theScalarStr = (controller+".NumScalars()")
		theScalarCount = execute theScalarStr
			
		debugPrint ("Found "+(theScalarCount  as string)+" scalar controllers")
		
		if theScalarCount != 4 do --need to remember there are always 4 for ticks, frames, secs and normalised-time
		(
			for i = 5 to theScalarCount do
			(
				--get scalar names
				thisScalarNameStr = (controller+".GetScalarName "+(i as string))
				thisScalarName = execute thisScalarNameStr
					debugPrint ("ScalarName: "+thisScalarName )
				appendIfUnique exprScalarNames thisScalarName
				
				--get scalar targets
				thisScalarTgtStr = (controller+".GetScalarTarget "+"\""+thisScalarName+"\""+" asController:true")	
					
				debugPrint ("trying to execute "+thisScalarTgtStr )
				thisScalarTgt = execute thisScalarTgtStr
					
				if thisScalarTgt == undefined  do
				(
					debugPrint ("WARNING! "+thisScalarTgtStr+" does not appear to have a valid controller.") beep:true
					messagebox ("WARNING! "+thisScalarTgtStr+" does not appear to have a valid controller.") beep:true
				)
					
				debugPrint ("Scalar value = "+(thisScalarTgt as string))	

				thisScalarTgtCont = exprformaxobject thisScalarTgt
				appendIfUnique scalarTargets (thisScalarTgtCont as string)					
				debugPrint ("appending "+(thisScalarTgtCont as string)+" to controller array...")	
			)
		)	
		
		if addedDriven == false do
		(
			addedDriven = true
			--now we need to find the parameters for the expression object
			filteredController = (filterString controller ".")

	-- 		expCont = filteredController[6]

	--*************************************************		
	--have to find a better way of finding the Z_Rotation / Translation charactersfor expCont as filteredController[x] is not working well
	--*************************************************	

			controllerToFindArray = #(
				"_Position",
				"_position",
				"_Translation",
				"_translation",
				"_Rotation",
				"_rotation",
				"_Scale",
				"_scale"
				)
			
			expCont = undefined 			
				
			for i = 1 to filteredController.count do
			(
				for c = 1 to controllerToFindArray.count do
				(
					ctf = controllerToFindArray[c]
					ctfLength = ctf.count
					
	-- 							print ("Testing "+filteredController[i]+" against "+ctf)
					
					if (substring filteredController[i] 2 (filteredController[i].count - 1)) == controllerToFindArray[c] then
					(
	-- 					print ("YYYYYYYAAAAAAAAYYYYYYY "+(substring filteredController[i] 2 (filteredController[i].count - 1))+" == "+controllerToFindArray[c] )
						expCont = filteredController[i]
					)
					else
					(
	-- 					print ("poo "+(substring filteredController[i] 2 (filteredController[i].count - 1))+" != "+controllerToFindArray[c] )
					)
				)
			)

			expObj = filteredController[1]
			
			if (substring expObj 1 1) == "$" do
			(
				expObj = (substring expObj 2 100)
			)
			
			expString = theexpr
				
			driven = (("\t"+"<driven expCont="+"\""+(expCont)+"\""+" expObj="+"\""+(expObj)+"\""+" expString="+"\""+(expString)+"\""+">")+"�")
			format (driven+"\n") to:ncaFile	
		)			
		
		for i = 1 to scalartargets.count do --have to do this twice so we can first of all set the headers up above
		(
			thisScalar = scalarTargets[i]
			filteredScalar = (filterString thisScalar ".")
			if filteredScalar[2] == "modifiers[#Attribute_Holder]" then
			(
				--ok we have found a attribute we need to prep its data to go into the line of the catt file as follows:
				--<rolloutParams attrMax="1.0" attrMin="0.0" attrName="c_squintInnerUL"/>
				--this means we have to firstly query the ranges of the attribute
				
-- 				print ("****     filtered: "+thisScalar+"    *****")
				
				thisObj = (substring filteredScalar[1] 2 50)
				attrToFind = filteredScalar[3]
				
				queryCARanges thisObj attrToFind
				attrName = (filteredScalar[4])
				--now we prep the line to format in the cattFile

				rolloutName = (filteredScalar[3])
				scalarCont = (filteredScalar[4])
-- 				
-- 				scalarCont = undefined
-- 				
-- 				controllerToFindArray = #(
-- 					"_Pos",
-- 					"_pos",
-- 					"_Position",
-- 					"_position",
-- 					"_Translation",
-- 					"_translation",
-- 					"_Rotation",
-- 					"_rotation",
-- 					"_Scale",
-- 					"_scale"
-- 					)				
-- 				
-- 				for d = 1 to filteredScalar.count do
-- 				(
-- 					print ("Testing "+filteredScalar[d])
-- 					for c = 1 to controllerToFindArray.count do
-- 					(
-- 						print (controllerToFindArray[c])
-- 						ctf = controllerToFindArray[c]
-- 						ctfLength = ctf.count
-- 						
-- 						--if (substring filteredScalar[d] 2 (filteredScalar[d].count - 1)) == controllerToFindArray[c] then
-- 						if (substring filteredScalar[d] 2 (filteredScalar[d].count - 1)) == controllerToFindArray[c] then
-- 						(
-- 							print ("YYYYYYYYYYYYYYYYYYYYAAAAAAAAAAAAAAAAAAAAAAYYYYYYYYYYYY "+(substring filteredScalar[d] 2 (filteredScalar[d].count - 1))+" == "+controllerToFindArray[c] )
-- 							scalarCont = filteredScalar[d]
-- 						)
-- 						else
-- 						(
-- 							print ("shit "+(substring filteredScalar[d] 2 (filteredScalar[d].count - 1))+" != "+controllerToFindArray[c] )
-- 						)
-- 						print "============"
-- 					)
-- 					print "-------------"
-- 				)	
				
				
				
				scalarName = exprScalarNames[i]
				scalarObj = (substring filteredScalar[1] 2 200)				

				caData1 = (("\t"+"<cAtt objName="+"\""+thisObj+"\""+" rolloutName="+"\""+rolloutName+"\""+">"))
				
				
				
				debugPrint ("scalarMax = "+(scalarMax as string))
				debugPrint ("scalarMin = "+(scalarMin as string))
				debugPrint ("attrName = "+(attrName as string))
					
				rolloutLine = (("\t"+"\t"+"<rolloutParams attrMax="+"\""+scalarMax+"\""+" attrMin="+"\""+scalarMin+"\""+" attrName="+"\""+attrName+"\""+"/>"))
				
				append caDataArray[1] caData1
				append caDataArray[2] rolloutLine
					
				--we also need to output this catt as a driver in the driver file as follows:
				--<driver rolloutName="eye_C_CTRL_A" scalarConts="c_squintInnerUL" scalarNames="c_eye_C_CTRL_squintInnerUL" scalarObjects="eye_C_CTRL"/>				
				driver = (("\t"+"\t"+"<driver rolloutName="+"\""+rolloutName+"\""+" scalarConts="+"\""+scalarCont+"\""+" scalarNames="+"\""+scalarName+"\""+" scalarObjects="+"\""+scalarObj+"\""+"/"+">"))
				format (driver+"\n") to:ncaFile
			)
			else
			(
				--ok this is not a custom attribute scalar
				
				--<driver scalarConts="Y_Position" scalarNames="outerBrow_L_CTRL_YPos" scalarObjects="outerBrow_L_CTRL"/>
				
				rolloutName = filteredScalar[3]
				
				scalarCont = undefined
				
				controllerToFindArray = #(
					"_Pos",
					"_pos",
					"_Position",
					"_position",
					"_Translation",
					"_translation",
					"_Rotation",
					"_rotation",
					"_Scale",
					"_scale"
					)				
				
				for d = 1 to filteredScalar.count do
				(
-- 					print ("Testing "+filteredScalar[d])
					for c = 1 to controllerToFindArray.count do
					(
-- 						print (controllerToFindArray[c])
						ctf = controllerToFindArray[c]
						ctfLength = ctf.count
						
-- 						if (substring filteredScalar[d] 2 (filteredScalar[d].count - 1)) == controllerToFindArray[c] then
						if (substring filteredScalar[d] 2 (filteredScalar[d].count - 1)) == controllerToFindArray[c] do
						(
-- 							print ("YYYYYYYYYYYYYYYYYYYYAAAAAAAAAAAAAAAAAAAAAAYYYYYYYYYYYY "+(substring filteredScalar[d] 2 (filteredScalar[d].count - 1))+" == "+controllerToFindArray[c] )
							scalarCont = filteredScalar[d]
						)
-- 						else
-- 						(
-- 							print ("shit "+(substring filteredScalar[d] 2 (filteredScalar[d].count - 1))+" != "+controllerToFindArray[c] )
-- 						)
-- 						print "============"
					)
-- 					print "-------------"
				)				
				
				
-- 				scalarCont = (filteredScalar[5])
				scalarName = exprScalarNames[i]
				scalarObj = (substring filteredScalar[1] 2 200)
				

-- 				print ("scalarCont = "+(scalarCont as string))
-- 				print ("scalarName = "+(scalarName as string))
-- 				print ("scalarObj = "+(scalarObj as string))
				
				driver = (("\t"+"\t"+"<driver scalarConts="+"\""+scalarCont+"\""+" scalarNames="+"\""+scalarName+"\""+" scalarObjects="+"\""+scalarObj+"\""+"/>"))
				format (driver+"\n") to:ncaFile
			)
			
			if i == scalarTargets.count do
			(
				if addedDriven == true do
				(
					--ok we need to close this driver section and reset the open section to false
					format ("\t"+"</driven>"+"\n") to:ncaFile
					addedDriven = false
				)
			)
		)
	)
)










-------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------
	
fn posList obj = 
(
	for i = 2 to obj.position.controller.count do --skip 1st controller as thats the frozen one
	(
		for cont in controllersToInclude do
		(
			if (obj.position.controller[i] as string) == "SubAnim:RsSpring" then
			(
-- 				appendIfUnique objWithExprArray obj
			)
			else
			(
				if (classof obj.position.controller[i].X_Position.controller as string) == cont do
				(
					controller  = ("$'"+obj.name+"'.position.controller["+(i as string)+"].X_Position.controller" as string)
						debugPrint ("Querying expression on "+controller)
					readExpression controller
				)
				if (classof obj.position.controller[i].Y_Position.controller as string) == cont do
				(
					controller  = ("$'"+obj.name+"'.position.controller["+(i as string)+"].Y_Position.controller" as string)
						debugPrint ("Querying expression on "+controller)
					readExpression controller
				)
				if (classof obj.position.controller[i].Z_Position.controller as string) == cont do
				(
					controller  = ("$'"+obj.name+"'.position.controller["+(i as string)+"].Z_Position.controller" as string)
						debugPrint ("Querying expression on "+controller)
					readExpression controller
				)		
			)
		)
	)	
)

fn rotList obj = 
(
	for i = 2 to obj.rotation.controller.count do --skip 1st controller as thats the frozen one
	(
		for cont in controllersToInclude do
		(
			if (obj.rotation.controller[i] as string) == "SubAnim:RsSpring" then
			(
-- 				appendIfUnique objWithExprArray obj
			)
			else
			(
				if (obj.rotation.controller[i] as string) == "SubAnim:LookAt_Constraint" then
				(
-- 					appendIfUnique objWithExprArray obj
				)
				else
				(
					if (classof obj.rotation.controller[i].X_rotation.controller as string) == cont do
					(
						controller  = ("$'"+obj.name+"'.rotation.controller["+(i as string)+"].X_rotation.controller" as string)
							debugPrint ("Querying expression on "+controller)
						readExpression controller						
					)
					if (classof obj.rotation.controller[i].Y_rotation.controller as string) == cont do
					(
						controller  = ("$'"+obj.name+"'.rotation.controller["+(i as string)+"].Y_rotation.controller" as string)
							debugPrint ("Querying expression on "+controller)
						readExpression controller						
					)
					if (classof obj.rotation.controller[i].Z_rotation.controller as string) == cont do
					(
						controller  = ("$'"+obj.name+"'.rotation.controller["+(i as string)+"].Z_rotation.controller" as string)
							debugPrint ("Querying expression on "+controller)
						readExpression controller						
					)		
				)
			)
		)
	)		
)

fn sclList obj = 
(
	for i = 2 to obj.scale.controller.count do --skip 1st controller as thats the frozen one
	(
		for cont in controllersToInclude do
		(
			if (obj.scale.controller[i] as string) == "SubAnim:RsSpring" then
			(
-- 				appendIfUnique objWithExprArray obj
			)
			else
			(			
				if (classof obj.scale.controller[i].X_scale.controller as string) == cont do
				(
					controller  = ("$'"+obj.name+"'.scale.controller["+(i as string)+"].X_scale.controller" as string)
						debugPrint ("Querying expression on "+controller)
					readExpression controller	
				)
				if (classof obj.scale.controller[i].Y_scale.controller as string) == cont do
				(
					controller  = ("$'"+obj.name+"'.scale.controller["+(i as string)+"].Y_scale.controller" as string)
						debugPrint ("Querying expression on "+controller)
					readExpression controller	
				)
				if (classof obj.scale.controller[i].Z_scale.controller as string) == cont do
				(
					controller  = ("$'"+obj.name+"'.scale.controller["+(i as string)+"].Z_scale.controller" as string)
						debugPrint ("Querying expression on "+controller)
					readExpression controller	
				)		
			)
		)
	)		
)
	
fn sclXYZ obj = 
(
	for cont in controllersToInclude do
	(
		if (obj.scale.controller as string) == "SubAnim:RsSpring" then
		(
-- 			appendIfUnique objWithExprArray obj
		)
		else
		(		
			if (classof obj.Scale.controller.X_Scale.controller as string) == cont do
			(
				controller  = ("$'"+obj.name+"'.Scale.controller.X_Scale.controller" as string)
					debugPrint ("Querying expression on "+controller)
				readExpression controller
			)
			if (classof obj.Scale.controller.Y_Scale.controller as string) == cont do
			(
				controller  = ("$'"+obj.name+"'.Scale.controller.Y_Scale.controller" as string)
					debugPrint ("Querying expression on "+controller)
				readExpression controller
			)	
			if (classof obj.Scale.controller.Z_Scale.controller as string) == cont do
			(
				controller  = ("$'"+obj.name+"'.Scale.controller.Z_Scale.controller" as string)
					debugPrint ("Querying expression on "+controller)
				readExpression controller
			)		
		)
	)
)

fn rotEulerXYZ obj = 
(
	for cont in controllersToInclude do
	(
		if (obj.rotation.controller as string) == "SubAnim:RsSpring" then
		(
-- 			appendIfUnique objWithExprArray obj
		)
		else
		(
			if (obj.rotation.controller[i] as string) == "SubAnim:LookAt_Constraint" then
			(
-- 				appendIfUnique objWithExprArray obj
			)
			else			
			(
				if (classof obj.Rotation.controller.X_Rotation.controller as string) == cont do
				(
					controller  = ("$'"+obj.name+"'.Rotation.controller.X_Rotation.controller" as string)
						debugPrint ("Querying expression on "+controller)
					readExpression controller
				)
				if (classof obj.Rotation.controller.Y_Rotation.controller as string) == cont do
				(
					controller  = ("$'"+obj.name+"'.Rotation.controller.Y_Rotation.controller" as string)
						debugPrint ("Querying expression on "+controller)
					readExpression controller
				)	
				if (classof obj.Rotation.controller.Z_Rotation.controller as string) == cont do
				(
					controller  = ("$'"+obj.name+"'.Rotation.controller.Z_Rotation.controller" as string)
						debugPrint ("Querying expression on "+controller)
					readExpression controller
				)	
			)
		)
	)
)

fn posXYZ obj = 
(
	for cont in controllersToInclude do
	(
		if (obj.position.controller as string) == "SubAnim:RsSpring" then
		(
-- 			appendIfUnique objWithExprArray obj
		)
		else
		(		
			if (classof obj.position.controller.X_Position.controller as string) == cont do
			(
				controller  = ("$'"+obj.name+"'.position.controller.X_Position.controller" as string)
					debugPrint ("Querying expression on "+controller)
				readExpression controller
			)
			if (classof obj.position.controller.Y_Position.controller as string) == cont do
			(
				controller  = ("$'"+obj.name+"'.position.controller.Y_Position.controller" as string)
					debugPrint ("Querying expression on "+controller)
				readExpression controller
			)	
			if (classof obj.position.controller.Z_Position.controller as string) == cont do
			(
				controller  = ("$'"+obj.name+"'.position.controller.Z_Position.controller" as string)
					debugPrint ("Querying expression on "+controller)
				readExpression controller
			)
		)
	)
)



fn queryFloatExpression obj = 
(
	--debugPrint ("Testing "+obj.name)
	if (classof obj.position.controller as string) == "position_list" do
	(
		--debugPrint ("Position List found on "+obj.name)
		posList obj
	)
	if (classof obj.position.controller as string) == "Position_XYZ" do
	(
		--debugPrint ("Position XYZ found on "+obj.name)
		posXYZ obj
	)
	---------
	if (classof obj.rotation.controller as string) == "rotation_list" do
	(
		--debugPrint ("Rotation List found on "+obj.name)
		rotList obj
	)
	if (classof obj.position.controller as string) == "Euler_XYZ" do
	(
		--debugPrint ("Euler XYZ found on "+obj.name)
		rotEulerXYZ obj
	)
	--------------
	if (classof obj.scale.controller as string) == "scale_list" do
	(
		--debugPrint ("Scale List found on "+obj.name)
		sclList obj
	)
	if (classof obj.scale.controller as string) == "Controller:ScaleXYZ" do
	(
		--debugPrint ("Scale XYZ found on "+obj.name)
		sclXYZ obj
	)
	
)

fn findExpressionObjects selectedArray = 
(
-- 	output_name = driverXML
-- 	catt_output_name = cAttXML

-- 	if driverXML != undefined do
	if (doesFileExist driverXML) == true do
	(
-- 		close driverXML
		debugPrint ("deleting old "+driverXML)
		deleteFile driverXML
	)

			-- 		
-- 	if cAttXML != undefined then 
	if (doesFileExist cAttXML) == true do
	(
-- 		close driverXML
		debugPrint ("deleting old "+cAttXML)
		deletefile cAttXML
	)
	

	
	for o = 1 to selectedArray.count do
	(
		queryFloatExpression selectedArray[o]
	)
		
	if ncaFile != undefined do
	(
		debugPrint ("closing ncafile")
		--add the end driver section and close
-- 		endDriven = (("\t"+"</driven>"))
-- 		format (endDriven+"\n") to:ncaFile
-- 		
		close ncaFile
	)
	
	--now prep the catt file
	
	prepCattXML()
	
	concatXML()

)


clearListener()

selectedArray = selection as array
findExpressionObjects selectedArray
