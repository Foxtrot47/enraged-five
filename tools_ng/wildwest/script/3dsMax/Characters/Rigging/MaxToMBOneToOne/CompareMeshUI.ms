try (destroyDialog  compareMeshUI) catch()

rollout compareMeshUI "Compare Mesh"
(
	
	global db_meter
	global db_distances
	local showDistance = undefined
	
	on compareMeshUI close do
	(
		format "Removing callbacks...\n"
		
		unregisterRedrawViewsCallback showDistance
	)
	
	fn applyVertColour obj mapChannel vert colourToSet = 
	(
		polyop.setVertColor obj mapChannel vert colourToSet
	)
	
	fn createMeasure compareMesh =
	(
		db_meter = getNodeByName "measure"
		
		if db_meter != undefined do
		(
			delete db_meter
		)

		maxPoint = compareMesh.max[2]
		minPoint = compareMesh.min[2]

		planeLength = maxPoint - minPoint 

		db_meter = Plane()
		db_meter.name = "measure"
		db_meter.width = 0.025
		db_meter.widthsegs = 1
		db_meter.length = planeLength
		db_meter.lengthsegs = 4
		convertTo db_meter PolyMeshObject
		db_meter.showVertexColors = on

		move db_meter compareMesh.center
		move db_meter [0.175,0,0]
		rotate db_meter (angleaxis 90 [1,0,0])

		applyVertColour db_meter 0 #{9..10}	(color 255  0   0 )
		applyVertColour db_meter 0 #{7..8}	(color 255 255  0 )
		applyVertColour db_meter 0 #{5..6} 	(color  0  255  0 )
		applyVertColour db_meter 0 #{3..4} 	(color  0  255 255)
		applyVertColour db_meter 0 #{1..2} 	(color  0   0  255)
		
	-- 	showDistance meter #
		
		redrawViews() 
		
	)
	
	group "Select : "
	(
		button btnPickMesh1 "Select Base Mesh" width:140 
		edittext edtMesh1
		button btnPickMesh2 "Select Compare Mesh" width:140 
		edittext edtMesh2
	)
	
	group "Compare : "
	(
		button btnCompare "Compare Meshes" width:140
	)
	

	on btnPickMesh1 pressed do
	(
		geo = pickObject  message:"Select Mesh 1..." prompt:"Select Mesh 1" select:true 
		if geo != undefined do 
		(
			edtMesh1.text = (geo.Name as string)
		)
	)
	
	on btnPickMesh2 pressed do
	(
		geo = pickObject  message:"Select Mesh 2..." prompt:"Select Mesh 2" select:true 
		if geo != undefined do 
		(		
			edtMesh2.text = (geo.Name as string)
		)
	)
	
	on btnCompare pressed do
	(
		numBaseVerts = 0
		numCompareVerts = 0
		baseMesh = undefined
		compareMesh = undefined
		
		if edtMesh1.text != undefined do
		(
			baseMesh = getnodebyname edtMesh1.text
			convertToPoly baseMesh
			numBaseVerts = polyop.getNumVerts baseMesh
			hide baseMesh
		)
		
		if edtMesh2.text != undefined do
		(
			compareMesh = getnodebyname edtMesh2.text
			convertToPoly compareMesh
			compareMesh.showVertexColors = on
			numCompareVerts = polyop.getNumVerts compareMesh
		)
		
		if numBaseVerts == numCompareVerts and numBaseVerts != 0 and numCompareVerts != 0 then
		(

			distanceArray = #()

			for vID = 1 to numBaseVerts do
			(
				vec = polyOp.getVert baseMesh vID
				vec1 = polyOp.getVert compareMesh vID
				append distanceArray (distance vec vec1)
			)
			
			sortArray = deepcopy distanceArray
			sort sortArray
			minVar = sortArray[1]
			sortCount = sortArray.count 
			
			quarterVar = sortArray[(sortArray.count / 4)]
			midVar = sortArray[(sortArray.count / 2)]
			threeQuartVar = sortArray[((sortArray.count / 4) * 3)]
			largestVar = sortArray[sortArray.count]

			db_distances = #(minVar, quarterVar, midVar, threeQuartVar, largestVar)

			mapChannel = 0
			
			maxOps.cloneNodes compareMesh cloneType:#copy newNodes:&nnl
			select nnl
			
			$.name = (compareMesh.name+"_comparisonResultMesh")
			
			comparisonResultMesh = getNodeByName (compareMesh.name+"_comparisonResultMesh")
			
			hide compareMesh
			
			for vID = 1 to numBaseVerts do
			(
				
				grayValue = (distanceArray[vID] / largestVar) * 255
				
				if (distanceArray[vID] / largestVar) < 0.25 then
				(
				
					colourToSet = (color 0 (255 * ((distanceArray[vID] / largestVar) / 0.25)) 255)
				
				)else if (distanceArray[vID] / largestVar) < 0.5 then(
					
					colourToSet = (color 0 255 (255 - (255 * (((distanceArray[vID] / largestVar) - 0.25) / 0.25))))
					
				)else if (distanceArray[vID] / largestVar) < 0.75 then(
					
					colourToSet = (color (255 * (((distanceArray[vID] / largestVar) - 0.5) / 0.25)) 255  0 )
				
				)else(
					
					colourToSet = (color 255 (255 - (255 * (((distanceArray[vID] / largestVar) - 0.75) / 0.25))) 0 )
					
				)
				
-- 				applyVertColour compareMesh mapChannel vID colourToSet
				applyVertColour comparisonResultMesh mapChannel vID colourToSet
					
			)
			
			createMeasure compareMesh
			
			unregisterRedrawViewsCallback showDistance
			
			fn showDistance =
			(
				gw.setTransform(Matrix3 1)
				gw.text ((polyOp.getVert db_meter 10)  + [0.02, 0, -0.005])  ((db_distances[5] as string)) color:(color 255  0   0)
				gw.text ((polyOp.getVert db_meter 8)   + [0.02, 0, -0.005])  ((db_distances[4] as string)) color:(color 255 255  0)
				gw.text ((polyOp.getVert db_meter 6)   + [0.02, 0, -0.005])  ((db_distances[3] as string)) color:(color  0  255  0)
				gw.text ((polyOp.getVert db_meter 4)   + [0.02, 0, -0.005])  ((db_distances[2] as string)) color:(color  0  255 255)
				gw.text ((polyOp.getVert db_meter 2)   + [0.02, 0, -0.005])  ((db_distances[1] as string)) color:(color  0   0  255)
				gw.enlargeUpdateRect #whole
				gw.updateScreen()
			)
			registerRedrawViewsCallback showDistance 

			redrawViews() 
	
		)
		else
		(
			format (basemesh.name+" vert count ("+(numBaseVerts as string)+" != "+compareMesh.name+" vert count ("+(numCompareVerts as string)+") \n")
		)

	)
)

createDialog compareMeshUI