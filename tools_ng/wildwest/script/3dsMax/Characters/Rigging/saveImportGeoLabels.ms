/*
Save/Import Geo Labels script-
Stewart Wright 15/06/12
A script to save selection set and mesh names
Writes Selection Set and mesh description information to the user defined properties
Can save date externaly
*/
-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")


-------------------------------------------------------------------------------------------------------------------------
--attempt to find the export dummy
fn findTheDummy =
(
	characterNode = undefined
	try
	(
		selectByWildCard "head_000" --there should always be a head mesh
		if selection.count > 0 then --if we find at least 1 mesh
		(
			obj = selection[1]
			exportDummy = WWfindRootObject obj
			clearSelection()
			exportDummy --pass out the export dummy
		)
		else
		(
			print "I couldn't find my head"
		)
	)catch()
)--end findTheDummy


-------------------------------------------------------------------------------------------------------------------------
-- tidy up the user defined properties by sorting it alphabetically
fn reorderTags tagMe = 
(
		currentUDP = getUserPropbuffer tagMe
		filterUDP = filterString currentUDP "\r\n"
		alphUDP = sort filterUDP
	
		tag = ""	
		for t = 1 to alphUDP.count do
		(
			tag = tag + alphUDP[t] + "\r\n"
			setUserPropbuffer tagMe tag
		)
)-- end reorderTags


-------------------------------------------------------------------------------------------------------------------------
-- delete any mesh info tags.  this includes selection sets and meshnames
-- deleteFromThese = the meshes to delete from
fn delMeshInfoTags deleteFromThese =
(
	for deleteThisMesh = 1 to deleteFromThese.count do
	(
		checkGeo = deleteFromThese[deleteThisMesh]

		keepUDP = #()
		currentUDP = getUserPropbuffer	checkGeo
		filterUDP = filterString currentUDP "\r\n"

		for f = 1 to filterUDP.count do (
			tempUDP = uppercase filterUDP[f]
			shortUDP = substring tempUDP 1 7

			if (shortUDP != "SELECTI" and shortUDP != "IGNORE=" and shortUDP != "MESHNAM") then
			append keepUDP tempUDP
		)

		tag = ""

		if keepUDP.count == 0 then setUserPropbuffer checkGeo tag

		else 

		for k = 1 to keepUDP.count do
		(
			tag = tag + keepUDP[k] + "\r\n"
			setUserPropbuffer checkGeo tag
		)
		reorderTags checkGeo
	)
)-- end delMeshInfoTags


-------------------------------------------------------------------------------------------------------------------------
-- go through all the selection sets and tag geometry
fn tagSelectionSetInfo tagTheseMeshes =
(
	tagThis = ""
	
	for ss = 1 to selectionsets.count do
	(
		geoFound = #()
		-- convert the selection set into an array
		matchSS = for obj in SelectionSets[ss] collect obj --turn selection set into array
		-- check the selection set array for geo only and add it to a seperate array
		geoFound = for obj in tagTheseMeshes where findItem matchSS obj > 0 collect obj -- makes an array with the geo only

		for tag = 1 to geoFound.count do
		(	
			tempString = substring (selectionsets[ss] as string) 14 1
			if tempString == "*" then 
			(
				tempIgnoreString = substring (selectionsets[ss] as string) 15 50
				tempIgnoreString = "Ignore=" + tempIgnoreString
				tagThis = tempIgnoreString
			)
			else
			(
				tempSelectionString = substring (selectionsets[ss] as string) 14 50
				tempSelectionString = "SelectionSet=" + tempSelectionString
				tagThis = tempSelectionString
			)

			tagMe = geoFound[tag] 

			oldTag = getUserPropbuffer tagMe
			tagThis = oldTag+"\r\n"+tagThis
			tagThis = uppercase tagThis
			setUserPropbuffer tagMe tagThis

			reorderTags tagMe --tidy up the tags
		)
	)
)-- end tagSelectionInfo


-------------------------------------------------------------------------------------------------------------------------
-- go through all geometry and tag it with its name
fn tagMeshNameInfo tagTheseMeshes=
(
	tagThis = ""

	for meshToTag=1 to tagTheseMeshes.count do
	(
		tagMe = tagTheseMeshes[meshToTag] --the mesh we are going to tag
		
		tempMeshString = "MeshName=" + tagMe.name
		tagThis = tempMeshString
		
		oldTag = getUserPropbuffer tagMe
		tagThis = oldTag+"\r\n"+tagThis
		tagThis = uppercase tagThis
		setUserPropbuffer tagMe tagThis

		reorderTags tagMe --tidy up the tags
	)
)-- end tagMeshNameInfo


-------------------------------------------------------------------------------------------------------------------------
--read in an external text file and update the UDP and mesh names
--updateTheseMeshes = meshes to update
fn readInUDPLabels updateTheseMeshes = 
(
	print ("Attempting to add labels")
	
	input_name = getOpenFileName caption:"Geo Labels" types:"geoNames (*.geoNames)|*.geoNames|All Files (*.*)|*.*|"

	modelNameAndSelSetData = #()
	if input_name != undefined do
	(
		inputFile = openfile input_name
		inputArray = readline inputFile
		execute inputArray
	)	
	
	inputMeshes = #()
	for inputMeshData = 1 to modelNameAndSelSetData.count do
	(
		inputMesh = substring modelNameAndSelSetData[inputMeshData][1] 10 255
		append inputMeshes inputMesh
	)

	if inputMeshes.count > 0 do
	(
		for o=1 to updateTheseMeshes.count do
		(
			thisname = filterstring updateTheseMeshes[o].name " "
			thisname = uppercase thisname[1]
			thisNameLength = thisName.count
			
			for i = 1 to inputMeshes.count do
			(
				if (substring inputMeshes[i] 1 thisNameLength) == thisname do
				(
					if (uppercase updateTheseMeshes[o].name) != inputMeshes[i] do
					(
						print ("Changing "+updateTheseMeshes[o].name +" to "+inputMeshes[i])
						updateTheseMeshes[o].name = inputMeshes[i]
					)
				)
			)
		)
	)
)--end readInUDPLabels


-------------------------------------------------------------------------------------------------------------------------
--write out the UDP values to a text file.  we only care about selection sets and meshnames
--readTheseMeshes = the meshes to read from
fn writeOutUDPLabels readTheseMeshes = 
(
	objData = #()
	
	for readThis = 1 to readTheseMeshes.count do
	(
		readGeo = readTheseMeshes[readThis]

		keepUDP = #()
		currentUDP = getUserPropbuffer	readGeo
		filterUDP = filterString currentUDP "\r\n"

		for f = 1 to filterUDP.count do
		(
			tempUDP = uppercase filterUDP[f]
			shortUDP = substring tempUDP 1 7

 			if (shortUDP == "SELECTI" or shortUDP == "IGNORE=" or shortUDP == "MESHNAM") then
 			append keepUDP tempUDP
		)
		
		shortUDPSearch=#()
		for short = 1 to keepUDP.count do
		(
			tempUDP = uppercase keepUDP[short]
			shortUDP = substring tempUDP 1 7
			
			append shortUDPSearch shortUDP
		)
		
		meshLocation = findItem shortUDPSearch "MESHNAM"--find where the mesh name is in the array

		tempArray = for i = 1 to keepUDP.count where i != meshLocation collect keepUDP[i]
		keepUDP = join #(keepUDP[meshLocation]) tempArray
		
		append objData keepUDP
	)

	--ok now we need to output the geoObjects data to a file
	output_name = getSaveFileName caption:"Geo Labels" types:"geoNames (*.geoNames)|*.geoNames|All Files (*.*)|*.*|"
	
	if output_name != undefined do 
	(
		output_file = createfile output_name		

		
		with printAllElements true (format "modelNameAndSelSetData = %\n" (objData as string) to:output_file)

		close output_file
		print ("Data saved to "+output_name)
	)
)--end writeOutUDPLabels


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
	if ((geoLabelGUI != undefined) and (geoLabelGUI.isDisplayed)) do
	(destroyDialog geoLabelGUI)
-------------------------------------------------------------------------------------------------------------------------
rollout geoLabelGUI "Save Geo Data"
(
	dotNetControl rsBannerPanel "Panel" pos:[0,0] width:geoLabelGUI.width height:32
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Character_Selectionset_to_UDP" filename:(getThisScriptFilename())
	
	checkBox chkDebugPrnt "DebugPrint" checked:false tooltip:"Check to enable debug print output."

	button btnWriteTags "Write Tags" width:200 height:60 tooltip:"Write the selection set and mesh name tags to the geometry"
	button btnReadTags "Read Tags" width:200 height:60 tooltip:"Read the mesh name tags and update the geometry"
	button btnDelTags "Delete Tags" width:200 height:60 tooltip:"Delete any existing selection set and mesh name tags."

	on geoLabelGUI open do banner.setup()
	
	on chkDebugPrnt changed theState do
	(
		if chkDebugPrnt.state == false then
		(
			debugPrintVal = false
			print "Debug printing disabled."
		)
		else
		(
			debugPrintVal = true
			print "Debug printing enabled."
		)
	)--end chkDebugPrnt
	
	on btnWriteTags pressed do
	(
		characterNode = findTheDummy() --find the export dummy
		if characterNode != undefined then
		(
			exportMeshes = characterNode.children --collect all the meshes linked to the dummy
			delMeshInfoTags exportMeshes --delete the tags off the children of the export dummy
			tagSelectionSetInfo exportMeshes --tag selection set information
			tagMeshNameInfo exportMeshes --tag mesh information
			
			writeOutUDPLabels exportMeshes --write out the UDP
		)
		else
		(
			messagebox "I couldn't find the character node"
		)
	)--end btnWriteTags

	on btnReadTags pressed do
	(
		characterNode = findTheDummy() --find the export dummy
		if characterNode != undefined then
		(
			exportMeshes = characterNode.children --collect all the meshes linked to the dummy
			readInUDPLabels exportMeshes --delete the tags off the children of the export dummy
		)
		else
		(
			messagebox "I couldn't find the character node"
		)
	)--end btnReadTags
	
	on btnDelTags pressed do
	(
		characterNode = findTheDummy() --find the export dummy
		if characterNode != undefined then
		(
			exportMeshes = characterNode.children --collect all the meshes linked to the dummy
			delMeshInfoTags exportMeshes --delete the tags off the children of the export dummy
		)
		else
		(
			messagebox "I couldn't find the character node"
		)
	)--end btnDelTags
)
-------------------------------------------------------------------------------------------------------------------------
createDialog geoLabelGUI width:220