filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
global rs_SkeletonUpdater
(
---------------------------------------------------------
-- UI --------------------------------------------------
---------------------------------------------------------
(
	try (destroyDialog rs_SkeletonUpdater) catch()
	---------------------------------------------------------
	rollout rs_SkeletonUpdater "Skeleton Updater"
	(
		dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:rs_SkeletonUpdater.width
		local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"" filename:(getThisScriptFilename())

		button btnUpdateSkelIK "Add IK Bones" width:130
		button btnUpdateSkelThigh "Reparent Thighs" width:130

		on btnUpdateSkelIK pressed do
		(
			print "Running addExtraIKones script"
			filein (RsConfigGetWildWestDir() + "script/3dsMax/characters/rigging/addExtraIKbones.ms")
		)
		---------------------------------------------------------
		on btnUpdateSkelThigh pressed do
		(
			print "Running reparentThighRollBonesToThighs script"
			filein (RsConfigGetWildWestDir() + "script/3dsMax/characters/rigging/usefulScripts/reparentThighRollBonesToThighs.ms")
		)
		---------------------------------------------------------
		---------------------------------------------------------
		on rs_SkeletonUpdater open do
		(
			rs_dialogPosition "get" rs_SkeletonUpdater	
			rs_SkeletonUpdater.banner.setup()
		)
		---------------------------------------------------------
		on rs_SkeletonUpdater close do
		(
			rs_dialogPosition "set" rs_SkeletonUpdater
		)
	)
	createDialog rs_SkeletonUpdater width:165 style:#(#style_border,#style_toolwindow,#style_sysmenu)
)--end UI
)