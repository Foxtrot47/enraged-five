/*
Stewart Wright - Rockstar North
27/09/12

A script to generate IK helpers for feet, hands, root and head as well as point helpers for feet
*/
--start fileins
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

ikHelperColour = (color 108 8 136)
bonesToAdd = #()

fn addExtraIKBones = 
(
--build hand helpers
	--build lhand helpers
	phlHandSkelNode = getNodeByName "PH_L_Hand"
	if phlHandSkelNode != undefined do--if we find the object we'll parent to
	(
		ikLHandName = "IK_L_Hand"
		
		foundIt = getNodeByName ikLHandName
		if foundIt != undefined do (delete foundIt)--if it exists alread delete it
		ikLHand = buildPointHelper ikLHandName phlHandSkelNode.pos 0.02 ikHelperColour --build the helper
		ikLHand.transform = phlHandSkelNode.transform --use the parents transform
		ikLHand.parent = phlHandSkelNode.parent --parent the new helper
		
		setUserPropBuffer ikLHand ("tag = "+ikLHandName+"\r\n"+"exportTrans = True")
		
		appendIfUnique bonesToAdd ikLHand --add the new helper to an array for adding to skinning later
		
		print "Left hand IK Helper set up."--print a message on completion
	)

	--build rhand helpers
	phRHandSkelNode = getNodeByName "PH_R_Hand"
	if phRHandSkelNode != undefined do
	(
		ikRHandName = "IK_R_Hand"
		
		foundIt = getNodeByName ikRHandName
		if foundIt != undefined do (delete foundIt)
		ikRHand = buildPointHelper ikRHandName phRHandSkelNode.pos 0.02 ikHelperColour
		ikRHand.transform = phRHandSkelNode.transform
		ikRHand.parent = phRHandSkelNode.parent
		
		setUserPropBuffer ikRHand ("tag = "+ikRHandName+"\r\n"+"exportTrans = True")
		
		appendIfUnique bonesToAdd ikRHand
		
		print "Right hand IK Helper set up."
	)

--build foot helpers
	--build lfoot helpers
	lFootSkelNode = getNodeByName "SKEL_L_Foot"
	if lFootSkelNode != undefined do
	(
		ikLFootName = "IK_L_Foot"
		phLFootName = "PH_L_Foot"
		lFootSkelNodePosition = lFootSkelNode.center
		
		foundIt = getNodeByName ikLFootName
		if foundIt != undefined do (delete foundIt)
		ikLFoot = buildPointHelper ikLFootName [lFootSkelNodePosition[1], lFootSkelNodePosition[2], 0] 0.02 ikHelperColour
		ikLFoot.parent = lFootSkelNode
		
		setUserPropBuffer ikLFoot ("tag = "+ikLFootName+"\r\n"+"exportTrans = True")

		foundIt = getNodeByName phLFootName
		if foundIt != undefined do (delete foundIt)
		phLFoot = buildPointHelper phLFootName [lFootSkelNodePosition[1], lFootSkelNodePosition[2], 0] 0.02 ikHelperColour
		phLFoot.parent = lFootSkelNode
		
		setUserPropBuffer phLFoot ("tag = BONETAG_L_PH_Foot"+"\r\n"+"exportTrans = True")
		
		appendIfUnique bonesToAdd ikLFoot
		appendIfUnique bonesToAdd phLFoot
		
		print "Left foot IK and Point Helpers set up."
	)

	--build rfoot helpers
	rFootSkelNode = getNodeByName "SKEL_R_Foot"
	if rFootSkelNode != undefined do
	(
		ikRFootName = "IK_R_Foot"
		phRFootName = "PH_R_Foot"
		rFootSkelNodePosition = rFootSkelNode.center
		
		foundIt = getNodeByName ikRFootName
		if foundIt != undefined do (delete foundIt)
		ikRFoot = buildPointHelper ikRFootName [rFootSkelNodePosition[1], rFootSkelNodePosition[2], 0] 0.02 ikHelperColour
		ikRFoot.parent = rFootSkelNode
		
		setUserPropBuffer ikRFoot ("tag = "+ikRFootName+"\r\n"+"exportTrans = True")

		foundIt = getNodeByName phRFootName
		if foundIt != undefined do (delete foundIt)
		phRFoot = buildPointHelper phRFootName [rFootSkelNodePosition[1], rFootSkelNodePosition[2], 0] 0.02 ikHelperColour
		phRFoot.parent = rFootSkelNode
		
		setUserPropBuffer phRFoot ("tag = BONETAG_R_PH_Foot"+"\r\n"+"exportTrans = True")
		
		appendIfUnique bonesToAdd ikRFoot
		appendIfUnique bonesToAdd phRFoot
		
		print "Right foot IK and Point Helpers set up."
	)

--build root helper
	rootSkelNode = getNodeByName "SKEL_Root"
	if rootSkelNode != undefined do
	(
		ikRootName = "IK_Root"
		
		foundIt = getNodeByName ikRootName
		if foundIt != undefined do (delete foundIt)
		ikRoot = buildPointHelper ikRootName rootSkelNode.pos 0.02 ikHelperColour
		ikRoot.transform = rootSkelNode.transform
		ikRoot.parent = rootSkelNode
		
		setUserPropBuffer ikRoot ("tag = "+ikRootName+"\r\n"+"exportTrans = True")

		appendIfUnique bonesToAdd ikRoot
		
		print "Root IK Helper set up."
	)

--build head helper
	headSkelNode = getNodeByName "SKEL_Head"
	if headSkelNode != undefined do
	(
		ikHeadName = "IK_Head"
		foundIt = getNodeByName ikHeadName
		if foundIt != undefined do (delete foundIt)
		ikHead = buildPointHelper ikHeadName headSkelNode.pos 0.02 ikHelperColour
		ikHead.transform = headSkelNode.transform
		ikHead.parent = headSkelNode
		
		setUserPropBuffer ikHead ("tag = "+ikHeadName+"\r\n"+"exportTrans = True")
		
		appendIfUnique bonesToAdd ikHead
		
		print "Head IK Helper set up."
	)

	--find the head meshes in the scene
	headGeo = #()
	for obj in objects do
	(
		if (upperCase (substring obj.name 1 7)) == "HEAD_00" do
		(
			append headGeo obj
		)
	)

	--now add to the skiun modifier if we found some heads
	if (headGeo.count != 0) and (bonesToAdd.count != 0)do
	(
		for selHead = 1 to headGeo.count do
		(
			select headGeo
			max modify mode 
			skinMod = headGeo[selHead].modifiers[#Skin]
			modPanel.setCurrentObject skinMod
			for addBone = 1 to bonesToAdd.count do
			(
				skinops.addBone skinMod bonesToAdd[addBone] 1
			)
		)
	)
)--end addExtraIKBones

addExtraIKBones()