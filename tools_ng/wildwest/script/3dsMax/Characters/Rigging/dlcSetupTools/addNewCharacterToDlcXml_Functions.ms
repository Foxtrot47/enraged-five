--script to add new character entry to dlc xml
clearListener()
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_xml.ms")


fn rsta_loadDlcXmlData dlcXmlFile newPedType pedOutfitName =
(
	/*
	newPedType - 1 == compont, 2 == streamed, 3 == cutsped
	*/
	
-- 	local xmlIO = rsta_xml_io()
-- 	xmlIO.xmlFile = dlcXmlFile
-- 	format ("Loading "+dlcXmlFile+"\n")
-- 	xmlIO.load()
	
	xmlDoc = XmlDocument()	
	xmlDoc.init()
	xmlDoc.load dlcXmlFile
	xmlRoot = xmlDoc.document.DocumentElement		
	
	format ("xmlRoot: "+(xmlRoot as string)+"\n")
	
	local pedSources = #()
	local pedExportNodes = #()
	local pedProcesses = #()
	local propProcesses = #()
	
	-- Parse the XML
	rootChildren = xmlRoot.childnodes	
	
	for i = 0 to (rootChildren.Count - 1 ) do
	(
		rootElement = rootChildren.itemof(i)
		
		if rootElement.name == "graph" then
		(	
			local rootElementVal = rootElement.getAttribute "id"
			
-- 			print ("rootElementVal: "+(rootElementVal as string)+"\n")
			
			local isPed = findstring rootElementVal "Peds"
			
			if isPed != undefined do 
			(
				local finalPedSourceElement = undefined
				local finalPedExportElement = undefined
				local finalPedProcess = undefined
				local finalPropProcess = undefined
				local pedNode = undefined
				local exportNode = undefined
				
				for pedChildren = 0 to (rootElement.childNodes.count - 1) do
				(
-- 					local pedNode = rootElement.childNodes.itemof(pedChildren)	
					pedNode = rootElement.childNodes.itemof(pedChildren)	
-- 					format ("pedNode.name: "+(pedNode.name as string)+"\n")
					
					if pedNode.name == "node" do 
					(
						local nodeValue = pedNode.getAttribute "id"
						
						local isPedSource = findstring (toLower(nodeValue)) "ped:source"
						
						
							
						if isPedSource != undefined do 
						(
-- 							finalPedSourceElement = pedNode.childnodes.itemof(pedNode.childnodes.count - 1)
							finalPedSourceElement = pedNode
							
							local id = nodeValue
							local contentType = undefined
							local pathData = undefined
							local outfitName = undefined
							local maxType = undefined
							
							for sourceChildren = 0 to (pedNode.childnodes.count - 1) do 						
							(
								thisChild = pedNode.childnodes.itemof(sourceChildren)
								
								local childValue = thisChild.getAttribute "key"
								--format ("childValue: "+(childValue as string)+"\n")
								
								if childValue == "content_type" do
								(
									contentType = thisChild.innerText
								)
								if childValue == "path" do
								(
									pathData = thisChild.innerText
								)
								if childValue == "outfit_name" do 
								(
									outfitName = thisChild.innerText
								)
								if childValue == "max_type" do 
								(
									maxType = thisChild.innerText
								)
							)	
							
							local dataArray = #()
							append dataArray id
							append dataArray contentType
							append dataArray pathData
							append dataArray outfitName
							append dataArray maxType
							
							format ("source:"+(dataArray as string)+"\n")
							
							append pedSources dataArray
						
						)
						
						local isPedExport = findstring (toLower(nodeValue)) "ped:export"
							
						if isPedExport != undefined do
						(
							isActualExportNode = findString (toLower(nodeValue)) "ped:export:"
								
							if isActualExportNode == undefined do 
							(
								finalPedExportElement = pedNode
								
								local id = nodeValue
								local contentType = undefined
								local pathData = undefined
								
								for sourceChildren = 0 to (pedNode.childnodes.count - 1) do 						
								(
									thisChild = pedNode.childnodes.itemof(sourceChildren)
									
									local childValue = thisChild.getAttribute "key"
									
									if childValue == "content_type" do 
									(
										contentType = thisChild.innerText
									)
									if childValue == "path" do
									(
										pathData = thisChild.innerText
									)	
								
									local dataArray = #()
									append dataArray id
									append dataArray contentType
									append dataArray pathData
									
									format ("export:"+(dataArray as string)+"\n")
									
									append pedExportNodes dataArray								
								)
							)
						)	
					)
					
					if pedNode.name == "edge" do 
					(	
-- 						finalPedProcess	= undefined
						local edgeSourceValue = pedNode.getAttribute "source"
						local edgeTargetValue = pedNode.getAttribute "target"
						local edgeText = undefined
						
						local dataArray = #()
						
						local thisChild = undefined
						for edgeChildren = 0 to (pedNode.childnodes.count - 1) do 						
						(
							thisChild = pedNode.childnodes.itemof(edgeChildren)
							
							edgeText = thisChild.innertext
						)
						
-- 						format ("ThisChild: "+(thisChild.name as string)+"\n")
						
						local isPedSource = findstring (toLower(edgeSourceValue)) "ped:source"
						
						if isPedSource != undefined do 
						(	
							local isValidSource = findString (toLower(edgeSourceValue)) "ped:source:"
								
							if isValidSource == undefined do 
							(
								append dataArray edgeSourceValue
								
								local isPedTarget = findstring (toLower(edgeTargetValue)) "ped:export"
								
								local isPropProcessTarget = findString (toLower(edgeTargetValue)) ":props"
								
								if isPropProcessTarget != undefined then
								(								
									append dataArray edgeTargetValue
									
									append dataArray edgeText
									
									format ("PropProcesses: "+(dataArray as string)+"\n")
									
									append propProcesses dataArray
									
									finalPropProcess = pedNode
									
									format ("finalPropProcess: "+(finalPropProcess as string)+"\n")
								)
								else
								(
									if isPedTarget != undefined do 
									(
										isValidTarget = findString (toLower(edgeTargetValue)) "ped:export:" 
											
										if isValidTarget == undefined do
										(
											append dataArray edgeTargetValue
											
											append dataArray edgeText
											
											format ("processes: "+(dataArray as string)+"\n")
											
											append pedProcesses dataArray
											
											finalPedProcess = pedNode
											
											format ("finalPedProcess: "+(finalPedProcess as string)+"\n")
										)
									)								
								)
							)
						)
					)					
				)
				
				--now we'll try and insert new source Data
				
				local totalExistingSources = pedSources.count
				
				local newSourceId = ("ped:source"+((totalExistingSources + 1) as string))
				
				local newPathIndex = findstring maxFilePath "peds"
				local newPath = (substring maxFilePath (newPathIndex - 1) -1)
				
				local newPathData = rsMakeSafeSlashes("$(art)"+newPath+maxfileName)
				
				--********************** NEED TO DO A UI TO BASE THE OUTFIT NAME ON THE FOLDER BUT HAVE THE OPTION TO OVERRIDE BY TYPING A NEW ONE *******

				
				local newOutfitName = undefined
				
				if pedOutfitName == undefined then
				(
					newOutfitName = pedOutfitName
				)
				else
				(
					newOutfitName = substring newPath 7 (newPath.count - 7)
				)

				local newMaxType = "character"

				format ("xmlDoc:"+(xmlDoc as string)+"\n")
				local nodeElem = xmlDoc.createElement("node") --ie the element name						
				local idElem = xmlDoc.createAttribute "id"
				idElem.value = newSourceId
				nodeElem.setAttributeNode idElem
												
					local dataElem = xmlDoc.createElement ("data")
					local keyElem = xmlDoc.createAttribute "key"
					keyElem.value = "content_type"
					dataElem.setAttributeNode keyElem
					nodeElem.appendChild dataElem
					dataElem.InnerText = "file"
					
					local dataElem = xmlDoc.createElement ("data")
					local keyElem = xmlDoc.createAttribute "key"
					keyElem.value = "path"
					dataElem.setAttributeNode keyElem
					nodeElem.appendChild dataElem							
					dataElem.InnerText = newPathData
					
					local dataElem = xmlDoc.createElement ("data")
					local keyElem = xmlDoc.createAttribute "key"
					keyElem.value = "outfit_name"
					dataElem.setAttributeNode keyElem
					nodeElem.appendChild dataElem							
					dataElem.InnerText = newOutfitName
					
					local dataElem = xmlDoc.createElement ("data")
					local keyElem = xmlDoc.createAttribute "key"
					keyElem.value = "max_type"
					dataElem.setAttributeNode keyElem
					nodeElem.appendChild dataElem							
					dataElem.InnerText = "character"
					
				if finalPedSourceElement != undefined then
				(
					format "inserting after\n"
					firstRootChild = xmlRoot.FirstChild
					print firstRootChild.name
					print "----"
					print nodeElem.innerxml
					print "----"
					print finalPedSourceElement.name
					
					firstRootChild.appendChild nodeElem
					
					print firstRootChild.innerxml
					
					firstRootChild.InsertAfter nodeElem finalPedSourceElement --this should insert the new ped thingy
				)
				else
				(
					--this should insert into the xml if no others were found
					rootElement.appendChild nodeElem
				)
				
				local newExportId = ("ped:export"+((totalExistingSources + 1) as string))

				local newExportPath = undefined
				if newPedType == 1 then
				(
					newExportPath = ("$(export)/models/cdimages/componentpeds/")
				)
				if newPedType == 2 then
				(
					newExportPath = ("$(export)/models/cdimages/streamedpeds/")
				)
				if newPedType == 3 then
				(
					newExportPath = ("$(export)/models/cdimages/cutspeds/")
				)
				
				local newPedExportPath = (newExportPath+maxfileName+".ped.zip")
				local newPropExportPath = (newExportPath+maxfileName+".prop.zip")
				
				--now we need to output all the exportNode data ** DO PROPS FIRST
				local nodeElem = xmlDoc.createElement("node") --ie the element name						
				local idElem = xmlDoc.createAttribute "id"
				idElem.value = (newExportId+":props")
				nodeElem.setAttributeNode idElem

					local dataElem = xmlDoc.createElement ("data")
					local keyElem = xmlDoc.createAttribute "key"
					keyElem.value = "content_type"
					dataElem.setAttributeNode keyElem
					nodeElem.appendChild dataElem
					dataElem.InnerText = "file"
				
					local dataElem = xmlDoc.createElement ("data")
					local keyElem = xmlDoc.createAttribute "key"
					keyElem.value = "path"
					dataElem.setAttributeNode keyElem
					nodeElem.appendChild dataElem
					dataElem.InnerText = newPropExportPath
					
				firstRootChild.appendChild nodeElem
										
				firstRootChild.InsertAfter nodeElem finalPedExportElement --this should insert the new ped thingy
				
				local nodeElem = xmlDoc.createElement("node") --ie the element name						
				local idElem = xmlDoc.createAttribute "id"
				idElem.value = newExportId
				nodeElem.setAttributeNode idElem

					local dataElem = xmlDoc.createElement ("data")
					local keyElem = xmlDoc.createAttribute "key"
					keyElem.value = "content_type"
					dataElem.setAttributeNode keyElem
					nodeElem.appendChild dataElem
					dataElem.InnerText = "file"
					
					local dataElem = xmlDoc.createElement ("data")
					local keyElem = xmlDoc.createAttribute "key"
					keyElem.value = "path"
					dataElem.setAttributeNode keyElem
					nodeElem.appendChild dataElem
					dataElem.InnerText = newPedExportPath
					
				firstRootChild.appendChild nodeElem
					
				firstRootChild.InsertAfter nodeElem finalPedExportElement --this should insert the new ped thingy				
				
				--now we need to output all the PedProcesses data 
				local nodeElem = xmlDoc.createElement("edge") --ie the element name						
				local idElem = xmlDoc.createAttribute "source"
				idElem.value = newSourceId
				nodeElem.setAttributeNode idElem
				local idElemB = xmlDoc.createAttribute "target"
				idElemB.value = newExportId
				nodeElem.setAttributeNode idElemB
				
					local dataElem = xmlDoc.createElement ("data")
					local keyElem = xmlDoc.createAttribute "key"
					keyElem.value = "processor"
					dataElem.setAttributeNode keyElem
					nodeElem.appendChild dataElem
					dataElem.InnerText = "RSG.Pipeline.Processor.Common.Dcc3dsmaxExportProcessor"
										
				firstRootChild.appendChild nodeElem
										
				firstRootChild.InsertAfter nodeElem finalPedProcess --this should insert the new ped thingy	

				--now we need to output all the PedProcesses data 
				local nodeElem = xmlDoc.createElement("edge") --ie the element name						
				local idElem = xmlDoc.createAttribute "source"
				idElem.value = newSourceId
				nodeElem.setAttributeNode idElem
				local idElemB = xmlDoc.createAttribute "target"
				idElemB.value = (newExportId+"props")
				nodeElem.setAttributeNode idElemB
				
					local dataElem = xmlDoc.createElement ("data")
					local keyElem = xmlDoc.createAttribute "key"
					keyElem.value = "processor"
					dataElem.setAttributeNode keyElem
					nodeElem.appendChild dataElem
					dataElem.InnerText = "RSG.Pipeline.Processor.Common.Dcc3dsmaxExportProcessor"
										
				firstRootChild.appendChild nodeElem
										
				firstRootChild.InsertAfter nodeElem finalPropProcess --this should insert the new ped thingy	
				
--***********************************
				xmlDoc.save(dlcXmlFile)					
			)
			
		)
		else
		(
			format ("Skipping "+rootElement.name+"\n")
		)
	)
)


-- dlcXmlFile = "x:\\gta5_dlc\\mpPacks\\mpHeist\\test_content_peds.xml"

-- loadDlcXmlData dlcXmlFile 2