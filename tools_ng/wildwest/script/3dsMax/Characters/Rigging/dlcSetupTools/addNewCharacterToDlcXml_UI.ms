filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging/dlcSetupTools/addNewCharacterToDlcXml_Functions.ms")



fn RSTA_getUiPosFile = 
(
	--THIS HAS TO BE DECLARED FROM  WITHIN THIS SCRIPT 
	
	uiPosFile = ((getThisScriptFilename()))
-- 	format (uiPosFile+"\n")

	filePath = filterString uiPosFile "."
		
	uiPosFile = (filePath[1]+"_UIPOSFILE.TXT")
		
-- 	format ("final "+uiPosFile+"\n")
		
	return uiPosFile
)


--//////////////////////////////////////////////////////////////////////////////////////////////
--			UI
--//////////////////////////////////////////////////////////////////////////////////////////////
if addPedToContentXML_UI != undefined then destroyDialog addPedToContentXML_UI
	
rollout addPedToContentXML_UI "DLC Ped Content Tree Update" --this is the main rollout that contains all the tabs and sub rollouts.
(
	--//////////////////////////////////////////////////////////////////////////////
	-- 	VARIABLES
	--//////////////////////////////////////////////////////////////////////////////

	local tabRollouts = #(::subRollPedToXml)
-- 	local weaponSubRolls = #(::subRollParts, ::subRollAAP, ::subRollRigging)
-- 	local markerSubRolls = #(::subRollMarkerCheckboxes, ::subRollMarkerRepair)
-- 	local expressionSubRolls = #(::subRollExpressionsCopyPaste)
-- 	local propSubRolls = #(::subRollMarkerCheckboxes, ::subRollPropBoneConstraints, ::subRollPropBoneXml, ::subRollPropBoneFragConvert, ::subRollPropTransferSkinPropRoll, ::subRollPropBoneDebugRoll)
	--//////////////////////////////////////////////////////////////////////////////
	-- 	CONTROLS
	--//////////////////////////////////////////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:addPedToContentXML_UI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"MrSkeletonv2" filename:(getThisScriptFilename())
		
-- 	dotNetControl dnTabs "system.windows.forms.tabControl" width:addPedToContentXML_UI.width height:25 offset:[-12, 0]
	dotNetControl dnTabs "system.windows.forms.tabControl" width:addPedToContentXML_UI.width height:20 offset:[-12, 0]
	
	subRollout theSubRollout width:(addPedToContentXML_UI.width - 2) height:(addPedToContentXML_UI.Height - (dnTabs.height + rsBannerPanel.height))offset:[-12, 0]--pos:[-1, dnTabs.pos.y + 50]
		
	--//////////////////////////////////////////////////////////////////////////////
	-- 	FUNCTIONS
	--//////////////////////////////////////////////////////////////////////////////
	fn SetPage index = 
	(
		if index > tabRollouts.count do messageBox "invalid tab"
		
		for roll in theSubRollout.rollouts do
		(
			removeRollout roll
		)

		AddSubRollout theSubRollout tabRollouts[index] rolledup:false border:true
		
		if index == 1 do --ie markers
		(
		)
	)
	
	on addPedToContentXML_UI open do
	(
		banner.setup()
		
		dnTabs.multiline = true --this lets us have multiple lines of tabs
		
		for tab in tabRollouts do dnTabs.tabPages.Add tab.title
		SetPage 1
		
		--now we need to find a previous saved position and apply that if found
		uiPosFile = RSTA_getUiPosFile() --get the dialogPos file
		dialogPos = RSTA_DialogPosLoad uiPosFile --get the position form the dialog pos file
		format ("Attempting to set dialog pos to "+(dialogPos as string)+"\n")
		SetDialogPos addPedToContentXML_UI dialogPos
	)
	
	on addPedToContentXML_UI close do 
	(
		gc light:true		
		if boneDebugTextBoxRollout_UI != undefined then destroyDialog boneDebugTextBoxRollout_UI
	)	
	
	on addPedToContentXML_UI moved position do 
	(
		uiPosFile = RSTA_getUiPosFile() --get the dialogPos file
		RSTA_DialogPosRec addPedToContentXML_UI uiPosFile
		
		if boneDebugTextBoxRollout_UI != undefined do 
		(
			boneDebugPositioner addPedToContentXML_UI boneDebugTextBoxRollout_UI
		)
	)
	
	
)

rollout subRollPedToXml "Add Ped To Xml"
(		
	local nOutfitName = (filterstring maxfileName ".")
	local newOutfitName  = nOutfitName[1]
	local contentXmlFile = undefined
	
	button btn_selectXmlFile "Select content xml" width:180 height:30 tooltip:"Select the content xml file you want to add this ped to."

	edittext txt_outfitName "Outfit Name:" fieldWidth:160 labelOnTop:true tooltip:"The name for this outfit"
	
	radiobuttons rad_PedType "Ped Type:" labels:#("None","Streamed", "Component", "Cutsped") default:1 columns:2
	
	button btn_AddToXml "Add to xml" width:180 height:30 tooltip:"Add an entry for this ped to the content xml"
	
	
	
	on subRollPedToXml open do
	(
		txt_outfitName.text = newOutfitName
	)
	
	on btn_selectXmlFile pressed do
	(
		contentXmlFile = getOpenFileName caption:"Content xml File" types:"XML File (*.xml)|*.xml|All Files (*.*)|*.*|"
		format ("Picked "+contentXmlFile+" as content xml file.\n")
	)
	
	on rad_PedType changed state do
	(
		format ("Ped type changed to "+(state as string)+"\n")
	)
	
	on txt_outfitName entered txt do
	(
		if txt != "" do
		(
			txt_outfitName.text = txt
			
			newOutfitName = txt
			
			format ("Outfit name changed to "+newOutfitName+"\n")
		)
	)
	
	on btn_AddToXml pressed do 
	(
		if contentXmlFile != undefined then
		(
			if rad_PedType != 1 then
			(
				format ("Adding ped to "+contentXmlFile+" of type "+((rad_PedType.state- 1) as string)+" as an outfit "+newOutfitName+"\n")
				rsta_loadDlcXmlData contentXmlFile (rad_PedType.state - 1) newOutfitName 	
			)
			else
			(
				format "Please select a valid Ped Type.\n"
				messagebox "Please select a valid Ped Type."
			)
		)
		else
		(
			format "Please pick a valid content xml file.\n"
			messagebox "Please pick a valid content xml file."
		)
	)
)

createDialog addPedToContentXML_UI width:200 height:250 style:#(#style_titlebar, #style_border, #style_sysmenu)