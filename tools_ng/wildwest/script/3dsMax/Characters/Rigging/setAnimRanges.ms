--script to adjust playback ranges

startFrame = undefined
endFrame = undefined


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



if ((animRangeGUI != undefined) and (animRangeGUI.isDisplayed)) do
	(destroyDialog animRangeGUI)

rollout animRangeGUI "Anim Rng"
(
	spinner spnStartFrame "Start Frame" range:[-10000,10000,0] type:#integer
	spinner spnEndFrame "End Frame" range:[-10000,10000,0] type:#integer

	button btnSetRange "Set Ranges"
	
	on animRangeGUI open do
	(
		animRng = (animationRange as string)
		filterStr = filterString animRng " "
		ind2Count = filterStr[2].count
		str2 = (substring filterStr[2] 1 (ind2Count - 1) )
		startFrame = (str2 as float)
			spnStartFrame.value = startFrame
		ind3count = filterStr[3].count
		str3 = (substring filterStr[3] 1 (ind3Count - 2) )
		endFrame = (str3 as float)
			spnEndFrame.value = endFrame
	)
	
	on spnStartFrame changed val do 
	(
		startFrame = spnStartFrame.value
	)

	on spnEndFrame changed val do 
	(
		endFrame = spnEndFrame.value
	)
		

	on btnSetRange pressed do
	(
		startFrame = startFrame as time
		endFrame = endFrame as time
		animationRange = interval startFrame endFrame
	)
	
)

CreateDialog animRangeGUI width:125 pos:[1450, 100] 