-- Bone tag checker script
-- Rick Stirling, Rockstar North
-- November 2011

-- This tool will check all the bones in a ped/animal skeleton for bone tags
-- It will make sure that the bone has a tag and that it is a valid tag


-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

boneTagArray =#()
BoneNameArray=#() 




fn checkskeleton  =
(
	-- Assume a root bone from our ped_rootbone variable (a project definition)
	-- Get the entire skeleton from this.
	
	boneList = #()
	boneTagArray =#()
	BoneNameArray=#() 
	
	-- populate the bone tagarray with with XML data
	populateMappingArrays()
	

	select ped_rootbone
	for thebone in selection do
	(
		try (selectmore thebone.children) catch ()
	)	
	
	-- Fill the array with the bone names and an empty string for the bone tags.
	for thebone in selection do append boneList #(thebone.name,"","EMPTY")

	clearSelection()
	
	
	-- We now have an array of all the bone names in the hierarchy 
	-- Check the bonetag of each
	for boneIndex = 1 to boneList.count  do
	(
		thebone = boneList[boneIndex]
		thebonename = thebone[1]
		thetag= ""
		boneMatchIndex = -1
		
		for bonenameIndex = 1 to BoneNameArray.count do
		(
			if BoneNameArray[bonenameIndex] == thebonename then boneMatchIndex = bonenameIndex
		)	
		
		-- Search for the bonetag
		filterUDP = filterString (getUserPropbuffer (getNodeByname thebone[1])) "\n\r"
		for UDPEntry in filterUDP do
		(
			isTag = uppercase (substring UDPEntry 1 3)
			if isTag == "TAG" then
			(
				tagstring = filterString UDPEntry " "
				
				thetag =  (uppercase tagstring[tagstring.count])
				thebone[2] = thetag
				thebone[3] = "TAG EXISTS"
			)	
			
			-- Just because a tag is there, it doesn't mean that it is correct.
			-- correct entry can be found with boneMatchIndex
			if boneMatchIndex > -1 then
			(	
				if boneTagArray[boneMatchIndex] == thetag then thebone[3] = "VALID" else thebone[3] = "INVALID"
			)
		)-- end UDP check
	) -- End bone data gather loop
		
	
	-- Sort the data by the result
	qsort boneList sortArraybySubIndex subIndex:3

	
	debug = newScript()

	
	-- Output the data
	for outputIndex = 1 to bonelist.count do
	(
		format "Bone:%, \t\t Tag:%,  Result:%\n" boneList[outputIndex][1] boneList[outputIndex][2] boneList[outputIndex][3]		to:debug
	)	
	

	
	
)



-------------------------------------------------------------------------------------------------------------------------
rollout bonetagchecker "Bone Tag Checker" width:220 height:80
(
	
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/bone_tag_checker" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)


	--button btnGReadSelected "Read Selected Geo" width:170 height:25
	button btnCheckSkeleton "Check selected Skeleton" width:170 height:25
	
	on btnCheckSkeleton pressed do checkskeleton()
		

)



createDialog bonetagchecker
