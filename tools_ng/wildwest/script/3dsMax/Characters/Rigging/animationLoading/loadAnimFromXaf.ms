fn saveMapping nodeNameArray = 
(
	output_File = undefined
	if nodeNameArray.count > 0 then
	(
-- 		output_name = getSaveFileName caption:"XMM output file" types:"XMM (*.xmm)|*.xmm|All Files (*.*)|*.*|"
		output_name = "x:/tmpMapping.xmm"
		
		if output_name != undefined then 
		(	
			objArray = #()
			
			for nam in nodeNameArray do 
			(
				obj = getNodeByName nam
				
				if obj != undefined then
				(
					append objArray obj
				)
				else
				(
					format ("Couldn't find object called "+nam+"\n")
				)
			)
			
			if objArray.count > 0 do 
			(
				output_file = createfile output_name		
				
				format ("<MaxMapFile version="+"\""+"0.40"+"\""+" date="+"\""+"Tue Jan 28 11:22:33 2014"+"\""+">"+"\n") to:output_file
				format ("\t"+"<CustomData />"+"\n") to:output_file
				
				for obj in objArray do 
				(
					obName = obj.name
					obLength = obName.count
					
					if toLower(substring obj.name (obLength - 2) 3) != "nub" do
					(
-- 						l1 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Position \ Frozen Position"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Position \ Frozen Position"+"\""+" />"+"\n")
-- 						l2 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Position \ Zero Pos XYZ \ X Position \ Limited Controller: Bezier Float"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Position \ Zero Pos XYZ \ X Position \ Limited Controller: Bezier Float"+"\""+" />"+"\n")
-- 						l3 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Position \ Zero Pos XYZ \ Y Position \ Limited Controller: Bezier Float"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Position \ Zero Pos XYZ \ Y Position \ Limited Controller: Bezier Float"+"\""+" />"+"\n")
-- 						l4 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Position \ Zero Pos XYZ \ Z Position \ Limited Controller: Bezier Float"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Position \ Zero Pos XYZ \ Z Position \ Limited Controller: Bezier Float"+"\""+" />"+"\n")
-- 						
-- 						l5 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Rotation \ Frozen Rotation \ X Rotation"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Rotation \ Frozen Rotation \ X Rotation"+"\""+" />"+"\n")
-- 						l6 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Rotation \ Frozen Rotation \ Y Rotation"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Rotation \ Frozen Rotation \ Y Rotation"+"\""+" />"+"\n")
-- 						l7 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Rotation \ Frozen Rotation \ Z Rotation"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Rotation \ Frozen Rotation \ Z Rotation"+"\""+" />"+"\n")
-- 						l8 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Rotation \ Zero Euler XYZ \ X Rotation"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Rotation \ Zero Euler XYZ \ X Rotation"+"\""+" />"+"\n")
-- 	 					l9 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Rotation \ Zero Euler XYZ \ Y Rotation"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Rotation \ Zero Euler XYZ \ Y Rotation"+"\""+" />"+"\n")
-- 	 					l10= ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Rotation \ Zero Euler XYZ \ Z Rotation"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Rotation \ Zero Euler XYZ \ Z Rotation"+"\""+" />"+"\n")

-- 	 					format (l1+l2+l3+l4+l5+l6+l7+l8+l9+l10) to:output_file
						
						l1 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Position"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Position"+"\""+" />"+"\n")
-- 						l1 = ""
						l2 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Rotation"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Rotation"+"\""+" />"+"\n")
						
						format (l1+l2) to:output_file
					)
				)
				format ("</MaxMapFile>"+"\n") to:output_file
				
				flush output_file
				close output_file
				
				format ("XMM File saved to "+output_name+"\n")
			)
			
		)
		else
		(
			format ("XMM Output file was undefind.\n")
		)
	)
	else
	(
		format ("Please select objects to generate mapping for\n")
		messagebox ("No objects selected. Aborting.") beep:true
	)
-- 	return output_File
	return output_name
)

fn parseXafToFindMapping selectedAnim = 
(
-- 	selectedAnim = undefined
-- 	
-- 	if selectedAnim == undefined do
-- 	(
-- 		selectedAnim = getOpenFileName caption:"Pose Animation" types:"Pose Anima (*.xaf)|*.xaf|All Files (*.*)|*.*|"
-- 	)	
	
	returnArray = #(
		#(), --nodeNameArray 
		#() -- map file
	)
	nodeNameArray = returnArray[1]
	
	if selectedAnim != undefined do 
	(
		
		xmlDoc = XmlDocument()	
		xmlDoc.init()
		xmlDoc.load selectedAnim
		xmlRoot = xmlDoc.document.DocumentElement	

		-- Parse the XML
		rootNodeElement = xmlRoot.childnodes
		--print "Beginning Normal XML Parse..."
	
-- 		nodeNameArray = #()
		
		rootElemCount = (rootNodeElement.Count - 1 )
		
		format ("rootElemCount: "+(rootElemCount as string)+"\n")
		
		for i = 0 to rootElemCount do
		(
			rootElement = rootNodeElement.itemof(i)
			
			if rootElement.name == "Node" then
			(
				--ok we now need to find a child called Object
				childNodeElement = rootElement.childNodes
				
				--format ("Parsing "+(childNodeElement.count as string)+" child nodes..."+"\n")
				
				nodeName = rootElement.getAttribute "name"
				
				if nodeName != undefined do 
				(
					--format ("nodeName: "+nodeName+"\n")
					
					appendIfUnique nodeNameArray nodeName
				)
			)
-- 			else
-- 			(
-- 				format ("rootElement.name: "+(rootElement.name)+"\n")
-- 			)
		)
		
		format ("nodeNameArray.count: "+(nodeNameArray.count as string)+"\n")
		mapFile = saveMapping nodeNameArray
		
		append returnArray[2] mapFile
		
		xmlDoc = undefined -- try to flush from memory
	)
-- 	return nodeNameArray
	return returnArray
)

fn parseMappingFile animmappingfile = 
(
	nodeNameArray = #()
	
	xmlDoc = XmlDocument()	
	xmlDoc.init()
	xmlDoc.load animmappingfile
	local xmlRoot = xmlDoc.document.DocumentElement	

	local startParse = timestamp()
	-- Parse the XML
-- 	heapsize = heapsize * 2
	local rootNodeElement = xmlRoot.childnodes
	--print "Beginning Normal XML Parse..."

	local endParse = timestamp()
	format ("Parsing childnodes took % seconds\n") ((endParse - startParse) / 1000.0)				
	
	local nodeNameArray = #()
	
	startParse = timestamp()
	
	rnc = (rootNodeElement.Count - 1 ) 
	for i = 0 to rnc do
	(
		local rootElement = rootNodeElement.itemof(i)
		
		try(
			if heapFree < 10000000 do
			(
				heapSize += 100000000
				format ("incremented heapSize to "+(heapSize as string)+"\n")
			)	
		)catch (Format "Couldnt increase heapsize")
		
		if rootElement.name == "Map" then
		(
			--ok we now need to find a child called Object
			local childNodeElement = rootElement.childNodes
			
			nNode = rootElement.getAttribute "name"
			
-- 			format ("nNode: "+(nNode as string)+"\n")
			
			nodeNameFilt = filterString nNode " "
			
-- 			format ("name: "+nodeNameFilt[1]+"WWWW"+"\n")
			appendIfUnique nodeNameArray nodeNameFilt[1]
		)
		else		
		(
			format (rootElement.name+" != Map\n")
		)
	)
	
	format ("Counted "+(nodeNameArray.count as string)+" objects in mapping file...\n")
	
	return nodeNameArray
)

fn loadCSFacialAnim = 
(
	selectedAnim = undefined
		
	-- 	messagebox "Reset pose file"
	-- facePoseTextFile = (theProjectRoot+"art/peds/FaceFX/ambientFaceRigPosesTEST.txt")
-- 	facePoseTextFile = (theProjectRoot+"art/peds/FaceFX/ambientFaceRigPoses.txt")		
	
	if selectedAnim == undefined do
	(
		selectedAnim = getOpenFileName caption:"Pose Animation" types:"Pose Anima (*.xaf)|*.xaf|All Files (*.*)|*.*|"
	)
	
	faceanimmappingfile = undefined
	
	if faceanimmappingfile == undefined do 
	(
		faceanimmappingfile  = getOpenFileName caption:"Mapping File" types:"Mapping File(*.xmm)|*.xmm|All Files (*.*)|*.*|"
	)
	
-- 	"X:/gta5/art/peds/Skeletons/Animations/ThreeLateralJoysticks.xmm"
	
	if selectedAnim != undefined do
	(
		if faceanimmappingfile != undefined do 
		(
	
			currsel = selection as array
			clearSelection()
-- 			for js in faceJoysticks do 
-- 			(
-- 				thisJs = getNodeByName js
-- 				selectMore thisJs
-- 			)
			
-- 			nodeNameArray = parseXafToFindMapping selectedAnim
-- 			dataArray = parseXafToFindMapping selectedAnim
		
			nodeNameArray = parseMappingFile faceanimmappingfile
			
-- 			nodeNameArray = dataArray[1]
				format ("nodeNameArray.count: "+(nodeNameArray.count as string)+"\n")
-- 			animmappingfile = dataArray[2][1]
				format ("animmappingfile: "+(faceanimmappingfile as string)+"\n")

			objArray = #()
			
			currSel = (selection as array)
			
			for nam in nodeNameArray do 
			(
				obj = getNodeByName nam
				
				if obj != undefined then
				(
					append objArray obj
				)
				else
				(
					format ("Couldn't find object called "+nam+"\n")
				)
			)

			select objArray
			
			format ("selected "+(selection.count as string)+" objects\n")

-- 			try (	
				LoadSaveAnimation.loadAnimation selectedAnim selection useMapFile:true mapFileName:faceanimmappingfile 
-- 				LoadSaveAnimation.loadAnimation selectedanim selection relative:false insertTime:0f useMapFile:true mapFileName:animmappingfile  
			
				select currSel
				messagebox "Loading Complete." title:"Animation Loading." beep:true
				
-- 			)catch(messageBox "Loading Failed." title:"ERROR" beep:true)
		)
	)
)	

loadCSFacialAnim()