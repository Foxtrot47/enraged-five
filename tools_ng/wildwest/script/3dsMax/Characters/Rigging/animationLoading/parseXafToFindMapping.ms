fn saveMapping nodeNameArray = 
(
	if nodeNameArray.count > 0 then
	(
		output_name = getSaveFileName caption:"XMM output file" types:"XMM (*.xmm)|*.xmm|All Files (*.*)|*.*|"
		
		if output_name != undefined then 
		(	
			objArray = #()
			
			for nam in nodeNameArray do 
			(
				obj = getNodeByName nam
				
				if obj != undefined then
				(
					append objArray obj
				)
				else
				(
					format ("Couldn't find object called "+nam+"\n")
				)
			)
			
			if objArray.count > 0 do 
			(
				output_file = createfile output_name		
				
				format ("<MaxMapFile version="+"\""+"0.40"+"\""+" date="+"\""+"Tue Jan 28 11:22:33 2014"+"\""+">"+"\n") to:output_file
				format ("\t"+"<CustomData />"+"\n") to:output_file
				
				for obj in objArray do 
				(
-- 					l1 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Position \ Frozen Position"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Position \ Frozen Position"+"\""+" />"+"\n")
-- 					l2 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Position \ Zero Pos XYZ \ X Position \ Limited Controller: Bezier Float"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Position \ Zero Pos XYZ \ X Position \ Limited Controller: Bezier Float"+"\""+" />"+"\n")
-- 					l3 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Position \ Zero Pos XYZ \ Y Position \ Limited Controller: Bezier Float"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Position \ Zero Pos XYZ \ Y Position \ Limited Controller: Bezier Float"+"\""+" />"+"\n")
-- 					l4 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Position \ Zero Pos XYZ \ Z Position \ Limited Controller: Bezier Float"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Position \ Zero Pos XYZ \ Z Position \ Limited Controller: Bezier Float"+"\""+" />"+"\n")
-- 					
-- 					l5 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Rotation \ Frozen Rotation \ X Rotation"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Rotation \ Frozen Rotation \ X Rotation"+"\""+" />"+"\n")
-- 					l6 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Rotation \ Frozen Rotation \ Y Rotation"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Rotation \ Frozen Rotation \ Y Rotation"+"\""+" />"+"\n")
-- 					l7 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Rotation \ Frozen Rotation \ Z Rotation"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Rotation \ Frozen Rotation \ Z Rotation"+"\""+" />"+"\n")
-- 					l8 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Rotation \ Zero Euler XYZ \ X Rotation"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Rotation \ Zero Euler XYZ \ X Rotation"+"\""+" />"+"\n")
-- 					l9 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Rotation \ Zero Euler XYZ \ Y Rotation"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Rotation \ Zero Euler XYZ \ Y Rotation"+"\""+" />"+"\n")
-- 					l10= ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Rotation \ Zero Euler XYZ \ Z Rotation"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Rotation \ Zero Euler XYZ \ Z Rotation"+"\""+" />"+"\n")

-- 					format (l1+l2+l3+l4+l5+l6+l7+l8+l9+l10) to:output_file
					
					l1 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Position"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Position"+"\""+" />"+"\n")
					l2 = ("\t"+"<Map name="+"\""+obj.name+" \ Transform \ Rotation"+"\""+" mapName="+"\""+obj.name+" \ Transform \ Rotation"+"\""+" />"+"\n")
					
					format (l1+l2) to:output_file
				)
				format ("</MaxMapFile>"+"\n") to:output_file
				
				flush output_file
				close output_file
				
				format ("XMM File saved to "+output_name+"\n")
			)
			
		)
		else
		(
			format ("XMM Output file was undefind.\n")
		)
	)
	else
	(
		format ("Please select objects to generate mapping for\n")
		messagebox ("No objects selected. Aborting.") beep:true
	)
)

fn saveControllers nodeNameArray = 
(
	if nodeNameArray.count > 0 do 
	(
		local startParse = timestamp()
		
		local output_name = getSaveFileName caption:"XMM output file" types:"XMM (*.xmm)|*.xmm|All Files (*.*)|*.*|"
		
		if output_name != undefined then 
		(	
			local output_file = createfile output_name		
			
			format ("beginning output\n")
			
			format ("<MaxMapFile version="+"\""+"0.40"+"\""+" date="+"\""+"Tue Jan 28 11:22:33 2014"+"\""+">"+"\n") to:output_file
			format ("\t"+"<CustomData />"+"\n") to:output_file
			
			local mainStr = ""
			for obj in nodeNameArray do 
			(
				try(
					if heapFree < 10000000 do
					(
						heapSize += 100000000
						format ("incremented heapSize to "+(heapSize as string)+"\n")
					)	
				)catch (Format "Couldnt increase heapsize")				
				
-- 				format ("\t"+"<Map name="+"\""+obj+"\""+" mapName="+"\""+obj+"\""+"/>"+"\n") to:output_file
-- 				format ("\t<Map name=\""+obj+"\" mapName=\""+obj+"\"/>\n") to:output_file
				mainStr = (mainStr+("\t<Map name=\""+obj+"\" mapName=\""+obj+"\"/>\n"))
			)
			
			if mainStr != "" do
			(
				format mainStr to:output_file
			)
			format ("</MaxMapFile>"+"\n") to:output_file
			
			flush output_file
			close output_file
			
			format ("XMM File saved to "+output_name+"\n")
			endParse = timestamp()
			format ("Parsing took % seconds\n") ((endParse - startParse) / 1000.0)				
		)	
	)
)

fn parseXafToFindMapping = 
(
	selectedAnim = undefined
	
	if selectedAnim == undefined do
	(
		selectedAnim = getOpenFileName caption:"Pose Animation" types:"Pose Anima (*.xaf)|*.xaf|All Files (*.*)|*.*|"
	)	
	
	if selectedAnim != undefined do 
	(
		
		xmlDoc = XmlDocument()	
		xmlDoc.init()
		xmlDoc.load selectedAnim
		local xmlRoot = xmlDoc.document.DocumentElement	

		local startParse = timestamp()
		-- Parse the XML
		heapsize = heapsize * 2
		local rootNodeElement = xmlRoot.childnodes
		--print "Beginning Normal XML Parse..."
	
		local endParse = timestamp()
		format ("Parsing childnodes took % seconds\n") ((endParse - startParse) / 1000.0)				
		
		local nodeNameArray = #()
		
		startParse = timestamp()
		
		--try and set a big heapsize
		local one = 1000000
		local gig = one * 1000
		local elevenGig = gig * 11
		
-- 		heapsize = elevenGig
		
		rnc = (rootNodeElement.Count - 1 ) 
		for i = 0 to rnc do
		(
			local rootElement = rootNodeElement.itemof(i)
			
			try(
				if heapFree < 10000000 do
				(
					heapSize += 100000000
					format ("incremented heapSize to "+(heapSize as string)+"\n")
				)	
			)catch (Format "Couldnt increase heapsize")
			
			if rootElement.name == "Node" then
			(
				--ok we now need to find a child called Object
				local childNodeElement = rootElement.childNodes
				
				--format ("Parsing "+(childNodeElement.count as string)+" child nodes..."+"\n")
				
				local cne = (childNodeElement.count - 1) 
				for obj = 0 to cne do 
				(
					local childElement = childNodeElement.itemof(obj)

					--local thisData = exprCreationData()
					
					if childElement.name == "Controller" do 
					(	
						local cont = childElement.getAttribute "name"
						
						if cont != undefined do 
						(
							appendIfUnique nodeNameArray cont
						)
					)						
				)
			)
			else
			(
				format ("rootElement.name: "+(rootElement.name)+"\n")
			)
		)
		
		format ("nodeNameArray.count: "+(nodeNameArray.count as string)+"\n")
-- 		saveMapping nodeNameArray
		
		local endParse = timestamp()

		format ("Parsing took % seconds\n") ((endParse - startParse) / 1000.0)				
		saveControllers nodeNameArray
	)
)

parseXafToFindMapping()