struct Struct_Ped_FFD_Scaler
(
	MyObject = undefined,
	femaleLatPoints = #([-0.588234,-0.144813,-0.00603089],
						[-0.544777,-0.175445,1.73418],
						[-0.588234,0.114353,-0.00603089],
						[-0.544777,0.118223,1.73463],
						[0.588234,-0.144813,-0.00603089],
						[0.544777,-0.175445,1.73418],
						[0.588234,0.114353,-0.00603089],
						[0.544777,0.118223,1.73463]),
	femaleModBoxSize = [1.17647,0.297663,1.82208],
	femaleModBoxPosArr = #([-0.588234,-0.167646,-0.00603089],
							[0.588234,-0.167646,-0.00603089],
							[-0.588234,0.130017,-0.00603089],	
							[0.588234,0.130017,-0.00603089],	
							[-0.588234,-0.167646,1.81605],
							[0.588234,-0.167646,1.81605],	
							[-0.588234,0.130017,1.81605],
							[0.588234,0.130017,1.81605]),
							


	-- Change world pos to ffd mod local pos
	fn ffdWorldToLocalPos obj ffdmod pos = (
		objTM = obj.objecttransform
		modTM = (getModContextTM obj ffdmod) * ffdmod.lattice_transform.value
		modBBMin = getModContextBBoxMin obj ffdmod
		modBBMax = getModContextBBoxMax obj ffdmod
		(pos - modBBMin) * (inverse objTM) * inverse modTM / (modBBMax-modBBMin)
	),

	/*
	****************************************************************************************************************************************
	set the ffd modifier ControllPoints to get the exact scale everytime. The values are from a manual trial. The ControlPoints need 
	to be set in World values which need to be changed to local values. As the local values depend on the size of the FFD Mod.
	****************************************************************************************************************************************
	*/
	fn SetFfdControlPoints ffdModBox ffdMod =
	(
		for i = 1 to femaleLatPoints.count do 
		(
			cp = ffdmod["Master"][i]
			cp.value = ffdWorldToLocalPos ffdModBox ffdMod femaleLatPoints[i]
		)
		
		return ffdMod

	),

	/*
	****************************************************************************************************************************************
	create a box which has the same size as the bounding box of the female character. Then move the verts of the box to the position 
	the FFD on the non scaled character would have. This is to make sure, that we gonna recreate the exact same FFD which would appear on 
	the female character.
	****************************************************************************************************************************************
	*/
	fn createFFDModBox =
	(
		ffdModBox = box width:femaleModBoxSize[1] length:femaleModBoxSize[2] height:femaleModBoxSize[3]
		convertTo ffdModBox PolyMeshObject
		for i=1 to femaleModBoxPosArr.count do 
		(
			polyOp.setVert ffdModBox i femaleModBoxPosArr[i]
		)

		return ffdModBox
	),

	/*
	****************************************************************************************************************************************
	create a FFD Modifier of a specific size. For this Scale Operation its important the modifier is at exactly the same size and the same 
	scale every time. As the objects scale when the FFD Box Points gets moved, its gonna be created on a dummy object, which resembles the 
	female character model. Its then scaled down, through moving the ControlPoints, so the scale is exactly the same each time.
	As Copy/Paste the modifier in script doesnt copy all of its settings, the object which should be scaled needs to be attached to the dummy
	object after. This will scale the object to the right size.
	!!! This is removing the material, can have a look later how to preserve it if needed!!!
	****************************************************************************************************************************************
	*/
	fn doFFD = 
	(
		-- create dummy box and apply ffd modifier
		ffdModBox = createFFDModBox()
		ffdMod = FFD_2x2x2 ()

		-- deform type All Vertices
		ffdMod.deformType = 1
		ffdMod.name = "FFD_Scale"	
		addModifier ffdModBox ffdMod before:(ffdModBox.modifiers.count)
		modPanel.setCurrentObject ffdModBox.modifiers["FFD_Scale"]

		-- Access ControlPoints
		subobjectLevel = 1

		-- In order to move the control Points "animateAll" needs to be enabled
		animateAll ffdModBox.modifiers["FFD_Scale"]

		-- Set the control Points 
		ffdMod = SetFfdControlPoints ffdModBox ffdMod
	
		-- remember object attributes as we have to combine it with the dummy object
		objectName = MyObject.name
		objectParent = MyObject.parent

		-- remove any face selection from the object and select all the faces of the dummy object, store them, so they can 
		-- be removed after combining the objects
		polyop.setFaceSelection MyObject #()
		polyop.setFaceSelection ffdModBox #all
		
		-- attach the object to the dummy object, collapse the stack and delete the faces of the dummy object
		ffdModBox.EditablePoly.attach MyObject ffdModBox
		collapseStack ffdModBox
		selFacesArr = polyop.getFaceSelection ffdModBox
		polyop.deletefaces ffdModBox selFacesArr

		-- rename and reparent the object to the original position
		MyObject = ffdModBox
		MyObject.name = objectName
		MyObject.parent = objectParent
	),

	-- Reset the XForm on the object in case there is scaling or other weird stuff
	fn doXForm =
	(		
		ResetXForm MyObject
		/*
		*********************************************************************************************************
		Cant manage to get the XForm at the last position at the stack, max throws a system error trying
		to access the last position with either $.modifiers.count or the int value (i.e. 3 or 4).
		This works find in Max 2015 and 2019
		***********************************************************************************************************
		*/

		-- reorder the xForm to the bottom of the stack
		--myMods = for m in MyObject.modifiers collect m

-- 		for m in myMods where classof m == XForm do 
-- 		(
-- 		--instance it to the bottom of the stack
-- 		addModifier MyObject m before:MyObject.modifiers.count off

-- 		-- delete the original instance from its place in the stack
-- 		deleteModifier MyObject m 
-- 		)

		maxOps.CollapseNodeTo MyObject MyObject.modifiers.count off
	),

	/*
	**************************************************************************************************************************
	Couldnt find a way to move XForm to the last position on the stack. So we check if the Stack has Modifiers and ask the 
	Artist to collapse them.
	!!! This will remove the skinning, can look into a way to preserve it later if needed. There are scripts for max 2015 and 2019
	not sure how much modification they need to work with 2012.
	**************************************************************************************************************************
	*/

	fn CheckModifiers =
	(
		if MyObject.modifiers.count > 0 then 
		(
		messageBox ("Please remove or collapse modifier on \"" + MyObject.name + "\"")
		return true
		)
		return false
	)

)


Strct_Ped_FFD_Scaler = Struct_Ped_FFD_Scaler()


try (destroydialog Ped_FFD_Scaler_Rollout)catch()
rollout Ped_FFD_Scaler_Rollout "Ped_FFD_Scaler"
(
	-- BANNER ------------------------------------------------
	dotNetControl RsBannerPanel "Panel" pos:[0,0] height:32 width:Ped_FFD_Scaler_Rollout.width
	local bannerStruct = makeRsBanner dn_Panel:RsBannerPanel versionNum:0.01 versionName:"Swell" wiki:"tab_ui_example" 

	button btn_scaleFFD "FFD_Scale"  width: 150 height:30  align:#center

		on Ped_FFD_Scaler_Rollout open do
		(
			-- BANNER SET UP --------------------------
			bannerStruct.setup()
		)

		on btn_scaleFFD pressed do
		(
			max modify mode
			Strct_Ped_FFD_Scaler.MyObject = selection[1]

			-- check if the Stack has modifiers
			hasMods = Strct_Ped_FFD_Scaler.CheckModifiers()

			-- If the stack has modifiers we dont do anything, otherwise do XForm and the FFD Scale
			if (hasMods == false) do 
			(
				Strct_Ped_FFD_Scaler.doXForm()
				Strct_Ped_FFD_Scaler.doFFD()
			)
		)
)

createdialog Ped_FFD_Scaler_Rollout width:200  style:#(#style_titlebar, #style_border, #style_sysmenu)
