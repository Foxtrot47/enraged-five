RSTA_LoadCommonFunction #("FN_common.ms","FN_RSTA_UI.ms", "FN_RSTA_rageMat.ms")

filein (::RsConfigGetWildwestDir() + "/script/3dsmax/characters/PedValidationTool_Struct.ms")

myStruct_PedValidationTool = struct_PedValidationTool()

try(destroyDialog PedValidationTool_Rollout)catch()
rollout PedValidationTool_Rollout "Ped Validation Tool" width:250
(
    dotNetControl RsBannerPanel "Panel" pos:[0,0] height:32 width:PedValidationTool_Rollout.width
    local bannerStruct = makeRsBanner dn_Panel:RsBannerPanel versionName:"Ped Validation Tool" versionNum:1.60 wiki:"tab_ui_example"

    button btn_StartValidation "Start Validation" height:30 width:200

    on PedValidationTool_Rollout open do
	(
		bannerStruct.setup()
    )

    on btn_StartValidation pressed do
    (
        myStruct_PedValidationTool.fn_BeginValidation()
    )
)
createDialog PedValidationTool_Rollout
