-- Set up a default scene for characters
-- Kill all scene materials, then load a default texture setup
-- Copy the default materials to the working folder
-- Set up the correct names and paths to the textures

-- Rick Stirling
-- 2005, initial version for GTA4
-- October 2009, Major cleanup. Replaced ifs with case, repalced hardcoded games with project lookup
-- February 2010, moved to GTA Wildwest
-- 12th April 2011, moved to new wildwest and cleaned up. Should work for all projects.

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")






-- Copy the base template textures into the new peds texture folder
-- Some artists like this
fn copyTemplateTextures destdir =
(

	basetextures = returnImageFiles defaultBasePedTextures 
		
	for f in basetextures do
	(
		copyFile f (destdir + "/"+ getFilenameFile f + getFilenameType f)
	)
)




fn create_character mNAME cptext =
(

	Playerbasefolder = theProjectPedFolder + "Player/" 
	Pedbasefolder = theProjectPedFolder + "Ped_Models/" 

	
	-- Set up the folder names that we need to create.
	modelfolder = Pedbasefolder + mNAME
	texturesfolder = modelfolder + "/Textures"
	toCreateOBJ = modelfolder + "/ZBrush_OBJ_Files"
	toCreateMaster = texturesfolder  + "/PSD"
	toCreateHigh = texturesfolder  + "/HighRes"
	toCreateConsole = texturesfolder + "/ConsoleRes"

	toCreateMF = modelfolder+ "/" + mNAME

	-- Create the character folder
	try
	(
		makedir modelfolder
		makedir texturesfolder
		makedir toCreateOBJ
		makedir toCreateMaster
		makedir toCreateHigh
		makedir toCreateConsole
	)
	catch messageBox "Couldn't create the folders"
		
	-- Save scene with character name
	savemaxfile toCreateMF 
	
	--Copy default textures?
	if cptext  == true then copyTemplateTextures toCreateHigh
	
	-- Copy imageresize batch file across too
	copyfile (theWildWest + "script/dos/Rockstar_North/ImageResize.bat") (texturesfolder+"\\ImageResize.bat")	
)






-- ********************************************************************************************
-- Rollouts
-- ********************************************************************************************

rollout modelcreation "Model/Folder Creation"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Ped_CharacterSetup" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	label lblModelInfo "Enter the name (e.g. GenStreet_01 or Fireman), choose options then press the Create button." width:250 height:40 pos:[10,30]
	editText edtModelName "" width:220 height:30 
	radioButtons rbtnSex width:50 height:30 labels:#("Male     ", "Female") default:1 pos:[60,120] 
	radioButtons rbtnAge  width:50 height:30 labels:#("Young  ", "Middle   ", "Old") default:1 pos:[40,140]
	radioButtons rbtnPedType width:10 height:30 labels:#("Ambient", "Goon", "Service", "Unique") default:1 pos:[20,160] 
	checkbox chkTemplateTextures "Copy Template Textures to folder"  pos:[40,190] checked:false
	button btnCreateChar "Create Folder Structure" pos:[30,220] width:200 height:40
	
	on btncreateChar pressed do
	(
		profession=modelcreation.edtModelName.text
		
		-- Assume some default values
		theSex= "Male"
		theAge="Young"
		theptype="Ambient"
		

		-- If the user has entered a model name
		-- Build up a model name with the attributes too.
		if profession != "" then
		(
			
			case modelcreation.rbtnSex.state of (
				1: theSex="M_"
				2: theSex="F_"
			)

			case modelcreation.rbtnAge.state of (
				1: theAge="Y_"
				2: theAge="M_"
				3: theAge="O_"
			)
			
			case modelcreation.rbtnPedType.state of (
				1: theptype="A_"
				2: theptype="G_"
				3: theptype="S_"
				4: theptype="U_"
			)
			
			-- Pass the character name and the game to the functions
			constructedName=theptype + thesex+theage+profession
			if constructedName.count < ped_modelNameLength then (
				create_character constructedName modelcreation.chkTemplateTextures.state
			)
			else (
				messagebox ("Ped name " + constructedName + " is too long, please shorten. You can use a maximum" + (ped_modelNameLength as string) + "characters.")
			)
		)
	)
)




rollout matsett "Material settings"
(

	button btnResetMat "Reset all Materials" pos:[30,10] width:200 height:25
	button btnKillSM "Kill scene materials" pos:[30,45] width:200 height:25
	
	on btnResetMat pressed do clear_all_materials()
	on btnKillSM pressed do kill_materials()

)

-- Create floater 
theNewFloater = newRolloutFloater "Rockstar Character Setup" 280 430

-- Add the rollout section
addRollout modelcreation theNewFloater
addRollout matsett theNewFloater
	