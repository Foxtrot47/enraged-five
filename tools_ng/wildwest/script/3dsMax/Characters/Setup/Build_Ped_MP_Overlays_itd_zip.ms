filein (RsConfigGetWildWestDir() + "script/3dsmax/_config_files/wildwest_header.ms")
filein "pipeline/export/AP3Asset.ms"
filein "pipeline/export/AP3Invoke.ms"

try( destroydialog BuildPedMPOverlayUI )catch()
rollout BuildPedMPOverlayUI "Build Ped Overlay Itd"
(
	--///////////////////////////////////////////////////////////////////////
	--	VARS
	--///////////////////////////////////////////////////////////////////////
	local MP_OverlaysDirs = #()
	local SP_OverlaysDirs = #()
	local MP_OverlaysPath = (RsConfigGetProjRootDir() + "art/peds/MP_Overlays")
	local SP_OverlaysPath = (RsConfigGetProjRootDir() + "art/peds/SP_Overlays")
	local converter = "rageTextureConvert.exe"
	local converterOpts = " -format A8R8G8B8"
	local SP_metaData = ( RsConfigGetAssetsDir() + "metadata/textures/ped_sp_overlays" )
	local MP_metaData = ( RsConfigGetAssetsDir() + "metadata/textures/ped_mp_overlays" )
	local template = ( RsConfigGetAssetsDir() + "metadata/textures/templates/peds/Diffuse" )
	local RS_TOOLSLIB = (RsConfigGetToolsDir() + "ironlib")
	local RS_PROJECT = RsConfigGetProjectName()
	local SP_OUTPUTDIR = (RsConfigGetExportDir() + "models/cdimages/ped_SP_Overlay_txds.zip")
	local MP_OUTPUTDIR = (RsConfigGetExportDir() + "models/cdimages/ped_MP_Overlay_txds.zip")
	local processType = "single"
	local packName = undefined
		
	--///////////////////////////////////////////////////////////////////////
	--	WIDGETS
	--///////////////////////////////////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:BuildPedMPOverlayUI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Build_Ped_SP_Overlays" versionNum:1.06 versionName:"Nonkle Nork" filename:(getThisScriptFilename())
	local iniPath = (RsConfigGetWildWestDir() + "etc/config/characters/PedOverlays.ini")
	
	dropdownlist ddlPacks items:#()
	radioButtons radType labels:#("Single", "Multi")
	multilistbox mlbFolders items:SP_OverlaysDirs height:30
	button btnProcess "Process" across:2 width:225 align:#left
	checkbox chkAll "All" checked:false offset:[100, 3]
	
	edittext edtFeedback enabled:false text:"Process Feedback"
	
	--///////////////////////////////////////////////////////////////////////
	--	FUNCTIONS
	--///////////////////////////////////////////////////////////////////////
	
	--///////////////////////////////////////////////////////////////////////
	fn getMPContents =
	(
		--mutli
		MP_OverlaysDirs = getDirectories (MP_OverlaysPath + "/*")
	)
	
	fn getBranchAssetPath = 
	(
		local fullpath = filterString gRsBranch.Assets "\\"
		return fullpath[fullpath.count]
	)
	
	--///////////////////////////////////////////////////////////////////////	
	fn ddlSelected arg =
	(
		format "Arg: % \n" arg
		if (ddlPacks.items[arg] == "Disk") then
		(
			print ddlPacks.items[arg]
			packName = undefined
			
			MP_OverlaysPath = (RsConfigGetProjRootDir() + "art/peds/MP_Overlays")
			MP_OverlaysDirs = getDirectories (MP_OverlaysPath + "/*")
			mlbFolders.items = for p in MP_OverlaysDirs collect pathConfig.stripPathToLeaf p
			processType = "multi"
			
			MP_OUTPUTDIR = (RsConfigGetExportDir() + "models/cdimages/ped_MP_Overlay_txds.zip")
		)
		else
		(
			print ddlPacks.items[arg]
			
			packName = ddlPacks.items[arg]
			
			MP_OverlaysPath = (gRsConfig.Project.Root + "_dlc/mpPacks/" + packName + "/art/peds/MP_Overlay")
			MP_OverlaysDirs = getDirectories (MP_OverlaysPath + "/*")
			mlbFolders.items = for p in MP_OverlaysDirs collect pathConfig.stripPathToLeaf p
			processType = "multi"
			
			local assetPath = getBranchAssetPath()
			
			MP_OUTPUTDIR = ("X:/gta5_dlc/mpPacks/" + packName + "/" + assetPath + "/export/models/cdimages/" + packName + "_ped_MP_Overlay_txds.zip")
			
		)
	)
		
	--///////////////////////////////////////////////////////////////////////
	--	EVENTS dear boy
	--///////////////////////////////////////////////////////////////////////
		
	--///////////////////////////////////////////////////////////////////////
	on ddlPacks selected arg do
	(
		ddlSelected arg
	)
	
	--///////////////////////////////////////////////////////////////////////
	on chkAll changed arg do
	(
		if chkAll.checked  == true then
		(
			mlbFolders.selection = #{1..mlbFolders.items.count}
		)
		else
		(
			mlbFolders.selection = #{}
		)
	)
	
	--///////////////////////////////////////////////////////////////////////
	on radType changed state do
	(
		if state == 1 then	--single
		(
			mlbFolders.items = for p in SP_OverlaysDirs collect pathConfig.stripPathToLeaf p
			processType = "single"
		)
		else -- mutli
		(
			mlbFolders.items = for p in MP_OverlaysDirs collect pathConfig.stripPathToLeaf p
			processType = "multi"
		)
	)
	
	--///////////////////////////////////////////////////////////////////////
	
	
	--///////////////////////////////////////////////////////////////////////
	on btnProcess pressed do
	(
		gRsUlog.ClearLogDirectory()
		--process selected folders
		local selectedDirsIdx = mlbFolders.selection as Array
		
		local metaData
		case processType of
		(
			"single":
			(
				local selectedDirs = for i in selectedDirsIdx collect SP_OverlaysDirs[i]
				--set working dir
				sysInfo.currentdir = SP_OverlaysPath
				metaData = ( RsConfigGetAssetsDir() + "metadata/textures/" + packName + "_ped_sp_overlays" )
				OUTPUTDIR = SP_OUTPUTDIR
				OverlaysPath = SP_OverlaysPath
				edtFeedback.text = "Building ped_SP_Overlay_txds.zip.."
			)
			
			"multi":
			(
				local selectedDirs = for i in selectedDirsIdx collect MP_OverlaysDirs[i]
				--set working dir
				sysInfo.currentdir = MP_OverlaysPath
				if (ddlPacks.selected == "Disk") then
				(
					metaData = MP_metaData
				)
				else
				(
					local assetPath = getBranchAssetPath()
					metaData = (gRsConfig.Project.Root + "_dlc/mpPacks/" + packName + "/" + assetPath + "/metadata/textures/" + packName + "_ped_mp_overlays" )
				)
				OUTPUTDIR = MP_OUTPUTDIR
				OverlaysPath = MP_OverlaysPath
				edtFeedback.text = "Building <pack name>_ped_MP_Overlay_txds.zip.."
			)			
		)		
		
		local count = 1
		--ProgressStart "Processing Folders..."
		
		local overlayAssetFile = AP3AssetFile()
		overlayAssetFile.Initialize()
		local overlayArchive = overlayAssetFile.AddZipArchive OUTPUTDIR
		
		for dir in selectedDirs do
		(
			--get the files to be converted
			local theTgas = getFiles (dir + "*.tga")
			
			edtFeedback.text = ("Processing folders: " + (count as String) + " of " + selectedDirs.count as String)
			windows.processPostedMessages()
			
			--convert the files
			for f in theTgas do
			(
				local outDDS = (getFilenamePath f) + (getFileNameFile f) + ".DDS"
				
				--check if it exists in perforce already
				local DDSExistsInP4 = gRsPerforce.exists #(outDDS)
				
				if (not DDSExistsInP4[1]) then
				(
					local command = converter + " -in " + f + " -out " + outDDS + converterOpts
				
					--execute
					HiddenDOSCommand command
					
					--now add that DDS
					gRsPerforce.add outDDS
				)
				else
				(
					gRsPerforce.add_or_edit #(outDDS)
					
					local command = converter + " -in " + f + " -out " + outDDS + converterOpts
				
					--execute
					HiddenDOSCommand command
				)
			)
			
			--build the itd.zip
			local itdzipPath = (filterString (pathConfig.stripPathToLeaf dir) "\\")[1]
			local theDDS = getFiles (dir + "*.dds")
			
			
			for f in theDDS do
			(
				local baseName = RsRemovePathAndExtension f
				local sourceTextureName = baseName + ".tga"
				local sourceTexturePath = dir + sourceTextureName
				
				basePath = (dir + itdzipPath)
				itdZipName = basePath + ".itd.zip"
				
				local textureArchive = overlayAssetFile.AddZipArchive itdZipName
				
				overlayAssetFile.AddFile textureArchive f
				
				-- .tcl file generation, just fire them out to the same folder as the texture and itd.zip
				tcloutputfile = dir + baseName + ".tcl"
				tcsreference = (RsMakeSafeSlashes (metadata + "/" + (RsRemovePathAndExtension tcloutputfile)))

				--check if the tcs exists in p4 first
				gRsPerforce.add_or_edit #((tcsreference + ".tcs"))
				
				--check the type of the bitmap and assign the appropriate template
				if (matchpattern baseName pattern:"*_n") then	--normal
				(
					RsGenerateTemplateReferenceFile (tcsreference + ".tcs") @"${RS_ASSETS}/metadata/textures/templates/peds/Streamed/MP_NormalMap" noop:false useSCC:false	
				)
				else	--diffuse
				(
					RsGenerateTemplateReferenceFile (tcsreference + ".tcs") @"${RS_ASSETS}/metadata/textures/templates/peds/Streamed/MP_Diffuse" noop:false useSCC:false
				)

				-- changed the substituteString to here as needed for the tcs file creation.
				RsGenerateTemplateReferenceFile tcloutputfile (substituteString tcsreference (RsConfigGetAssetsDir()) "$(assets)/")
				
				-- Add the tcl to the itd.zip, and the itd.zip to the main overlay zip
				overlayAssetFile.AddFile textureArchive tcloutputfile				
				overlayAssetFile.AddFile overlayArchive itdZipName
			)	
			
			--ProgressUpdate (100.0 * (count / selectedDirs.count))
			count += 1
		)

		
		gRsPerforce.add_or_edit #(OUTPUTDIR)
		overlayAssetFile.BuildAssetFiles()
		
		--return false	
		
		windows.processPostedMessages()	
	
		--build the platform data
		AP3Invoke.convert #(OUTPUTDIR)  debug:true
		
		--ProgressEnd()
		edtFeedback.text = ""
		--return false
	)
	
	--///////////////////////////////////////////////////////////////////////
	on BuildPedMPOverlayUI open do	--populate list
	(
		banner.setup()
		--single
		SP_OverlaysDirs = getDirectories (SP_OverlaysPath + "/*")
		
		--mutli
		MP_OverlaysDirs = getDirectories (MP_OverlaysPath + "/*")
		
		if processType == "single" then
		(
			mlbFolders.items = for p in SP_OverlaysDirs collect pathConfig.stripPathToLeaf p
		)
		else
		(
			mlbFolders.items = for p in MP_OverlaysDirs collect pathConfig.stripPathToLeaf p
		)
		
		--set menu opts
		gRsPerforce.sync #(iniPath)
		ddlPacks.items = (getIniSetting iniPath "Overlays")
	)
	
)

createDialog BuildPedMPOverlayUI width:300
