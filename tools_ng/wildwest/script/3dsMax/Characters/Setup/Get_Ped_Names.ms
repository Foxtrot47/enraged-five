-- Tool to extract ped names from peds.meta
-- Rick Stirling, Rockstar North
-- January 2012



-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())



pednamelist =#()
	
	
	
-- Populate mapping lookup arrays from a project specific XML file
fn parsePedsMeta = (
	
	-- Make sure the predefined arrays are empty
	pednamelist =#()
	
	
	
	-- Define the XML file
	mappingXmlFile = theProjectRoot + "build/dev/common/data/peds.meta" 
	xmlDoc = XmlDocument()	
	xmlDoc.init()
	xmlDoc.load mappingXmlFile
	xmlRoot = xmlDoc.document.DocumentElement
	
	
	-- Parse the XML
	-- This could be looped I suspect
	if xmlRoot != undefined then (
		dataElems = xmlRoot.childnodes
	
		for i = 1 to (dataElems.Count - 1 ) do (
			dataElement = dataElems.itemof(i)
			
			-- Look for ped bones
			if dataelement.name == "InitDatas" then (
				-- Look for the bones ids
				pedItems = dataelement.childnodes
				
				for ped = 0 to (pedItems.Count - 1 ) do (
					pedEntry = pedItems.itemof(ped)
					
					
					if pedEntry.name == "Item" then (
						
						pedList = pedEntry.childnodes
						
						for pl = 0 to (pedList.Count - 1 ) do (
						
							pedDataLine = pedList.itemof(pl)
							
								if pedDataLine.name == "Name" then (
									
									pedname = pedDataLine.innertext
							
									append pednamelist pedname 
								)

							
						)
					) -- end bone elements
				)
			)-- end ped data elements
		)		
	)-- end xmlfound
	
	return pednamelist
	

)


pednamelist = parsePedsMeta()


pednamelist = sort pednamelist


	ToolUsageDataPath = "C:\\Temp\\"
	ToolUsageDataFilename = "pedlist"
	ToolUsageDataFilename = ToolUsageDataPath  + ToolUsageDataFilename   + ".txt"
	
	try (		
		makeDir (ToolUsageDataPath)	
		
		local output_file = createfile ToolUsageDataFilename 
		for dataentry in pednamelist do
		(	
			format (dataentry + "\r") to:output_file
		)	
		close output_file	
	) catch()	

