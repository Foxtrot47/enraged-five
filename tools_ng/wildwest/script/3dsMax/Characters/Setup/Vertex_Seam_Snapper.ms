-- C_Vertex_Tools
-- Rick Stirling
-- Rockstar North
-- July 2010
-- 
-- Changed by Adam Munson (Oct 2010)
--

-- Moved to new Wildwest by Rick, June 2011
-- Rewritten by Rick Stirling April 2012 to do some more error detection and add transform resetting

global RsCharSeamSnapper
global RsCharSeamSnapperVerts
try(destroyDialog RsCharSeamSnapper)catch()

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

filein (::RsConfigGetWildwestDir() + "script/3dsmax/characters/setup/CharEyeNormals.ms")

baseVertDistanceFactor = 0.003	-- The threshold for checking if vert positions are matches

AllVertInfoLists = #()	-- Contains vertData structs for every selected object's selected vertices
AllVertexDataLists = #()	-- Array for the PreprocessVerts and AverageNormals function
boneList = #()

saveColour = [225, 132, 0]--amber
destroyColour = [225, 0, 0]--red
createColour = [0, 255, 0]--green
	
struct vertData 
(
	vertNumber,
	vertPos,
	vertNormal
)

-------------------------------------------------------------------------------------------------------------------------
-- Checks if two verts (across objects) are within the vert distance
--	factor from the spinner, to pair them together (for snapping later).
fn CheckIfVertsMatch vertDistanceFactor vertPos1 vertPos2 =
(
	distanceVal = vertPos1 - vertPos2	

	if (not (distanceVal.x > vertDistanceFactor or 
			distanceVal.y > vertDistanceFactor or 
			distanceVal.z > vertDistanceFactor or
			distanceVal.x < -vertDistanceFactor or 
			distanceVal.y < -vertDistanceFactor or 
			distanceVal.z < -vertDistanceFactor)) then 
	(
		true
	)
	else
	(
		false
	)
)

-------------------------------------------------------------------------------------------------------------------------
-- Add an Edit Poly modifier to the stack of the selected objects 
-- Also clears any vert/edge selections 
fn AddEditPolyMod =
(

	theSelection = ($selection as array)
	if (theSelection.count > 1) do
	(
		-- Add an Edit Poly modifer to all the selected objects
		modPanel.addModToSelection (Edit_Poly ()) ui:on

		-- Go to border selection as default
		subobjectlevel = 3

		-- Clear selections for each node incase there are some selected already somehow
		for obj = 1 to theSelection.count do
		(
			theSelection[obj].modifiers[#Edit_Poly].SetSelection #Edge #{} node:theSelection[obj]
			theSelection[obj].modifiers[#Edit_Poly].SetSelection #Vertex #{} node:theSelection[obj]
		)		
	)
)

-------------------------------------------------------------------------------------------------------------------------
--fn: 	RsPreprocessVerts
--desc:	It goes through brute force to get every vertex on each selected object and 
-- 	their associated normals, which are put into a multi dimensional array of 
--	structs for each object selected
fn PreprocessVerts =
(
	theSelection = $selection as array

	AllVertexDataLists = #()
		
	-- Goes through every vertex for every selected object and gets their normals
	for o = 1 to theSelection.count do 
	(	
		if ((progBar != undefined) and (progBar.isDisplayed)) do (destroyDialog progBar)
		CreateDialog progBar width:300 Height:30
		
		select theSelection[o]
		numVerts = theSelection[o].Vertices.count
		vertexDataList = #()

		for v = 1 to numVerts do
		(
			vData = vertData()
			vertList = #{v}
			normalsList = #{} 
			
			if ( theSelection[o].Edit_Normals != undefined ) do 
			(
				theSelection[o].Edit_Normals.ConvertVertexSelection &vertList &normalsList
				vData.vertNormal = (normalsList as array)
			)
			vData.vertNumber = v
			vData.vertPos = theSelection[o].vertices[v].pos
			
			append vertexDataList vData
		)
		append AllVertexDataLists vertexDataList	
		
		progBar.prog.value = ((100* o)/theSelection.count)
		progBar.prog.color = saveColour
	)
	
	(destroyDialog progBar)
	select theSelection	
)

-------------------------------------------------------------------------------------------------------------------------
--CollectVerts takes the border edge selection and finds the verts, building the global struct
fn CollectVerts vertDistanceFactor =
(

	AllVertInfoLists = #()
	theSelection = getCurrentSelection() as array
	
	for obj =1 to theSelection.count do
	(
		if ( theSelection[obj].modifiers[#Edit_Poly] == undefined ) do 
		(
			Messagebox "Not Edit_Poly modifier on at least one of the objects"
			return false
		)
	)
	
	local vertSelection = true
	
	-- Edge or Border selection
	if ( subobjectlevel == 2 or subobjectlevel == 3 ) then vertSelection = false
	
	for objIndex = 1 to theSelection.count do
	(
		select theSelection[objIndex]
		edgeList = #()
		vertList = #()
		
		-- The user will have selected either verts or edges. If edges, convert the selection to verts
		if ( vertSelection == false ) then 
		(
			edgeList = theSelection[objIndex].modifiers[#Edit_Poly].GetSelection #Edge		
			vertList = (polyop.getVertsUsingEdge theSelection[objIndex] edgeList) as array
		)
		
		else
		(
			vertList = ( theSelection[objIndex].modifiers[#Edit_Poly].GetSelection #Vertex ) as array
		)
		
		
		if (vertList.count < 1) do 
		(
			MessageBox "Nothing selected on one of the selected objects"
			select theSelection
			return false
		)
		
		-- Now process the vert list
		vertInfoList = #()
		for vert in vertList do
		(
			vertInfo = vertData() -- create an instance of the vertdata struct 
			vertInfo.vertNumber = vert
			vertInfo.vertPos = theSelection[objIndex].vertices[vert].pos

			-- If first object vertices being found, just add them in the found order
			if ( objIndex == 1 ) then 
			(
				append vertInfoList vertInfo
			)
			-- For the remaining objects in the selection, check what vertices they match from
			-- the first objects vertices and then order them in the same way to allow easy
			-- snapping later
			else
			( 
				for v = 1 to AllVertInfoLists[1].count do
				(
					if ( CheckIfVertsMatch vertDistanceFactor vertInfo.vertPos AllVertInfoLists[1][v].vertPos ) do
					(
						vertInfoList[v] = vertInfo
					)						
				)
			)
		)
		append AllVertInfoLists vertInfoList
	)
	select theSelection
	true
)

-------------------------------------------------------------------------------------------------------------------------
fn CollectVertsSkin vertDistanceFactor SortedNodeList =
(
	AllVertInfoLists = #()
	theSelection = #()
	for objname in SortedNodeList do append theSelection (getnodebyname objname)
	
	for obj =1 to theSelection.count do
	(
		if (theSelection[obj].modifiers[#Mesh_Select] == undefined ) do 
		(
			Messagebox "No Mesh_Select modifier on at least one of the objects"
			return false
		)
	)
	
	local vertSelection = true
	
	for obj = 1 to theSelection.count do
	(
		if ((progBar != undefined) and (progBar.isDisplayed)) do (destroyDialog progBar)
		CreateDialog progBar width:300 Height:30
		
		-- for each object in the selection get a list of the selected verts
		select theSelection[obj]
		vertList = #()
		vertList = getVertSelection theSelection[obj] as array
	
		
		if (vertList.count < 1) do 
		(
			MessageBox "Nothing selected on one of the selected objects"
			select theSelection
			return false
		)
		
		vertInfoList = #()
		for vert in vertList do
		(
			vertInfo = vertData()
			vertInfo.vertNumber = vert
			vertInfo.vertPos = theSelection[obj].vertices[vert].pos
		
			-- If first object vertices being found, just add them in the found order
			if ( obj == 1 ) then 
			(
				append vertInfoList vertInfo
			)
			-- For the remaining objects in the selection, check what vertices they match from
			-- the first objects vertices and then order them in the same way to allow easy
			-- snapping later
			else
			( 
				for v = 1 to AllVertInfoLists[1].count do
				(
					if ( CheckIfVertsMatch VertDistanceFactor vertInfo.vertPos AllVertInfoLists[1][v].vertPos ) do
					(
						vertInfoList[v] = vertInfo
					)						
				)
			)
		)
		append AllVertInfoLists vertInfoList
		
		progBar.prog.value = ((100* obj)/theSelection.count)
		progBar.prog.color = saveColour
	)
	(destroyDialog progBar)
	select theSelection
	true
)

-------------------------------------------------------------------------------------------------------------------------
--fn:	CopySkinWeightsAcrossVerts
--desc:	Copies vert weights to all matching verts, from the first skinned object it finds.
fn CopySkinWeightsAcrossVerts SortedNodeList =
(
	foundSkinnedObject = undefined 	--declaring for scope issues
	--SortedNodeList is all the objects, with the chosen skin object first

	-- Go through all objects and perform the copying	

	for obj = 2 to SortedNodeList.count do 
	(
		foundSkinnedObject = undefined --reset from any previous usages
		pasteObject = getnodebyname SortedNodeList[obj]
	
		if (pasteObject.modifiers[#Skin] != undefined ) do
		(		
			foundSkinnedObject = true
			fromNode = getnodebyname(SortedNodeList[1]) -- always copy from the first one
			fromSkin = fromNode.modifiers[#Skin]
			
			-- loop through each vert in each object apart from the first object
			-- I do the copy each time, wasteful, but we have a small number of verts so I don't care at this point.
			for vertInfo = 1 to AllVertInfoLists[obj].count do 
			(
				if ((progBar != undefined) and (progBar.isDisplayed)) do (destroyDialog progBar)
				CreateDialog progBar width:300 Height:30
				
				toSkin = pasteObject.modifiers[#Skin]
				fromVert = AllVertInfoLists[1][vertInfo].vertNumber
				toVert = AllVertInfoLists[Obj][vertInfo].vertNumber
				-- get skinning data for that vert.
				
				fBoneArray = #()
				fWeightArray = #()
				fBoneNames = #()
				tBoneNames = #()
				tBoneArray = #()
				
				modPanel.setCurrentObject fromSkin
				fBNumber = skinOps.getVertexWeightCount fromSkin fromVert
				
				for i = 1 to fBNumber do
				(
					boneID = skinOps.getVertexWeightBoneId fromSkin fromVert i
					boneWeight = skinOps.getVertexWeight fromSkin fromVert i
					boneName = skinOps.GetBoneName fromSkin boneID 0

					append fBoneArray boneID
					append fWeightArray boneWeight
					append fBoneNames boneName
				)

--------------------------------------
--add missing bones
				modPanel.setCurrentObject toSkin

				--work out the bone names in our to mesh
				tBoneCount = skinOps.getNumberBones toSkin
				for bID=1 to tBoneCount do
				(
					appendIfUnique tBoneNames (skinOps.GetBoneName toSkin bID 0)
				)
				--search our to mesh for missing bones listed in the from mesh
				for b = 1 to fBoneNames.count do
				(
					foundIt = findItem tBoneNames fBoneNames[b]
					if foundIt == 0 do
					(
						modPanel.setCurrentObject fromSkin
						realBone = getBoneFromSkin fromSkin fBoneArray[b]--we are missing a bone, lets find it...
						modPanel.setCurrentObject toSkin
						skinOps.AddBone toSkin realBone 1--... and add it
						print "=============="
						print (realBone.name + "added")
					)
				)
--------------------------------------
--because the meshes might have different skinned skeletons we need to bring them in to line
				tBoneNames = #()--empty this, we'll fill it up with the new bones
				modPanel.setCurrentObject toSkin
				
				--work out the bone names in our to mesh
				tBoneCount = skinOps.getNumberBones toSkin--work out the bone names in our to mesh
				for bID=1 to tBoneCount do
				(
					appendIfUnique tBoneNames (skinOps.GetBoneName toSkin bID 0)
				)

				--find matches for the bones and store their positions, this will be their bone id.
				for i=1 to fBoneArray.count do
				(
					bID = findItem tBoneNames fBoneNames[i]
					append tBoneArray bID
				)
--------------------------------------
				skinOps.ReplaceVertexWeights toSkin toVert tBoneArray fWeightArray
				
				progBar.prog.value = ((100* vertInfo)/AllVertInfoLists[obj].count)
				progBar.prog.color = createColour
			)
		)
		(destroyDialog progBar)

		-- If a skinned object has already been found and the weights copied, exit
		if (foundSkinnedObject == true ) do break
	)

	if (foundSkinnedObject == true ) then
	(
		true
	)
	else
	(
		Messagebox "No skinned objects found to copy weights from"
		false
	)
	
	--now remove any mesh selects that were added.
	for i = 1 to SortedNodeList.count do
	(
		thisObj = getNodeByName SortedNodeList[i]
		
		for md = 1 to thisObj.modifiers.count do 
		(
			if (classof $.modifiers[md] as string) == "Mesh_Select" do
			(
				deleteModifier thisObj md
			)
		)
	)
)

-------------------------------------------------------------------------------------------------------------------------
--fn: 	AverageSnapSelectedVerts
--desc:	Snaps together related verts after getting their average position.  
fn AverageSnapSelectedVerts vertexTolerence =
(
	if ((progBar != undefined) and (progBar.isDisplayed)) do (destroyDialog progBar)
	CreateDialog progBar width:300 Height:30
	
	theSelection = getCurrentSelection() as array
	
	for i=1 to theSelection.count do
	(
		outputSkinningData theSelection[i]--copy skinning
		progBar.prog.value = ((100* i)/theSelection.count)
		progBar.prog.color = saveColour
	)
	
	if theSelection.count < 2 do
	(
		MessageBox "Less than 2 objects selected to snap verts"
		return false
		(destroyDialog progBar)
	)
	
	for objIndex = 1 to theSelection.count do
	(
		deleteModifier theSelection[objIndex] theSelection[objIndex].modifiers[#Edit_Poly]
		progBar.prog.value = ((100* objIndex)/theSelection.count)
		progBar.prog.color = destroyColour
	)
	
	-- Added this so that we can select the borders in the Edit_poly, which is then removed 
	-- and the stack collapsed before converting to a mesh (characters are editable_polys)
	-- This was done because of some weird behaviour on some verts when trying to set their
	-- positions, but the meshop's don't seem to have the same problem.
	
	for objIndex = 1 to theSelection.count do
	(
		collapseStack theSelection[objIndex]
		convertToMesh theSelection[objIndex]
		progBar.prog.value = ((100* objIndex)/theSelection.count)
		progBar.prog.color = destroyColour
	)	
	
	select theSelection
	
	for vertInfo = 1 to AllVertInfoLists[1].count do
	(
		local averagePos = [0,0,0]
		local averageVertColor = [0,0,0]
		
		--these specific map channels are to be averaged across the seam
		local mapValueSet = #	
		(
			(dataPair channel:-2 value:[0,0,0]),
			(dataPair channel:-1 value:[0,0,0]),
			(dataPair channel:0 value:[0,0,0]),
			(dataPair channel:11 value:[0,0,0]),
			(dataPair channel:14 value:[0,0,0]),
			(dataPair channel:15 value:[0,0,0])
		)
		
		for objIndex = 1 to theSelection.count do 
		(
			if ( AllVertInfoLists[objIndex][vertInfo] != undefined ) then
			(
				averagePos += AllVertInfoLists[objIndex][vertInfo].vertPos
				
				if (RsCharSeamSnapperVerts.cbBlendMapChannels.checked) then
				(
					for item in mapValueSet do
					(
						item.value += RSMesh_GetVert_MapValue theSelection[objIndex] AllVertInfoLists[objIndex][vertInfo].vertNumber item.channel
					)
				)
			)
			else
			(
				Messagebox "Matching verts were not found - try upping the vertex distance factor"
				return false
			)
		)	
		
		--average the values
		averagePos /= theSelection.count
		
		if (RsCharSeamSnapperVerts.cbBlendMapChannels.checked) then
		(
			for item in mapValueSet do
			(
				item.value /= theSelection.count
			)
		)
			
		-- For each item in the group, apply the average position and map channel values
		for objTest = 1 to theSelection.count do
		(					
			vertNum = #(AllVertInfoLists[objTest][vertInfo].vertNumber) as bitArray
			AllVertInfoLists[objTest][vertInfo].vertPos = averagePos
			meshop.setVert theSelection[objTest] vertNum averagePos node:(getnodebyname theSelection[objTest].name)

			if (RsCharSeamSnapperVerts.cbBlendMapChannels.checked) then
			(
				for item in mapValueSet do
				(
					RSMesh_SetVert_MapValue theSelection[objTest] AllVertInfoLists[objTest][vertInfo].vertNumber item.channel item.value
				)
			)
		)
		progBar.prog.value = ((100* vertInfo)/AllVertInfoLists[1].count)
		progBar.prog.color = destroyColour
	)
	
	select theSelection
	
	-- Put back to Editable_Poly
	for objIndex = 1 to theSelection.count do
	(
		resetxform theSelection[objIndex]
		maxOps.CollapseNodeTo theSelection[objIndex] 1 True
		convertToPoly theSelection[objIndex]
		progBar.prog.value = ((100* objIndex)/theSelection.count)
		progBar.prog.color = createColour
	)
	
	true
	
	--reapply skinning
	for i=1 to theSelection.count do
	(
		inputSkinningData theSelection[i]
		progBar.prog.value = ((100* i)/theSelection.count)
		progBar.prog.color = createColour
	)
	
	select theSelection
	
	(destroyDialog progBar)
)

-------------------------------------------------------------------------------------------------------------------------
--fn: 	RsAddEditNormals
--desc:	Adds an Edit_Normal modifier and unifies them for
fn AddEditNormals =
(
	theSelection = getcurrentselection() as array
	
	-- Create Edit normals modifier and add it to the selection
	edtn = Edit_normals()
	edtn.displayLength = 0.01
	edtn.SelLevel = #Vertex
	edtn.ignoreBackfacing = off
	addModifier theSelection edtn
	
	-- Refresh the screen	
	forceCompleteRedraw()
	select theSelection
)

-------------------------------------------------------------------------------------------------------------------------
fn UnifyNormals = (
	
	theSelection = getcurrentselection() as array
	
	-- Unify the normals 
	for c = 1 to theSelection.count do (
		theSelection[c].modifiers[#Edit_Normals].SetSelection #{1..1000000} node:theSelection[c]
		theSelection[c].modifiers[#Edit_Normals].unify node:theSelection[c]
		theSelection[c].modifiers[#Edit_Normals].SetSelection #{} node:theSelection[c]
	)
	forceCompleteRedraw()

	select theSelection
)

-------------------------------------------------------------------------------------------------------------------------
--fn:	RsSortNormalsByVertPos
--desc:	Sorts an array of vertex data structs based on vertex position, so averaging normals 
--	can be done.
fn SortNormalsByVertPos vertDistanceFactor selectedNormals =
(
	retArray = #()
	
	-- For each object we need to loop to see what vertices are matched by position
	for obj = 1 to selectedNormals.count do
	(
		objRowArray = #()
		
		-- Go through each vert data struct and check the vertPos
		for idxVertData = 1 to selectedNormals[1].count do
		(
			-- If first row, just copy that across
			if ( 1 == obj ) then
			(
				append objRowArray selectedNormals[obj][idxVertData]
			)
			else
			(
				-- If not the first object's vertices, loop through all vertex data
				-- structs to see which vertex positions match the first objects, for this
				-- indiviual object then add it to the objRowArray which is later appended
				-- to make the complete array that is returned, sorted.
				firstObjVertPos = retArray[1][idxVertData].vertPos
				-- go through all 
				for vData = 1 to selectedNormals[1].count do
				(					
					if ( selectedNormals[obj][vData] != undefined ) then
					(
						secondObjVertPos = selectedNormals[obj][vData].vertPos
					
						if ( true == CheckIfVertsMatch VertDistanceFactor firstObjVertPos secondObjVertPos ) do
						(
							append objRowArray selectedNormals[obj][vData]
						)
					)
					else
					(
						-- Incase we have a situation where there are objects selected that
						-- don't have any normals selected, just skip checking for any entries 
						-- for this object entirely
						break
					)
				)
			)
		)
		append retArray objRowArray
	)
	retArray
)

-------------------------------------------------------------------------------------------------------------------------
--fn:	RsAverageNormals
--desc:	Collects the normals for each vert now that the edit_normals modifier is
--	on the stack, then averages them across vertices.  Since averaging is between
--	just two normals, if we have more than 2 objects selected, we just copy the 
--	averaged normal to them.
fn AverageNormals vertDistanceFactor =
(
	
	theSelection = getCurrentSelection() as array
	allMatchingVertData = #()	-- Stores all matches for each object
	
	-- Make sure we've an Edit_Normals mod on
	if ( theSelection[1].Edit_Normals != undefined ) then 
	(
		if ((progBar != undefined) and (progBar.isDisplayed)) do (destroyDialog progBar)
		CreateDialog progBar width:300 Height:30
		
		for obj = 1 to theSelection.count do
		(
			matchingVertData = #()	-- stores matches for just this object
			vertNormalsSelArray = #()
			
			-- Get the selected normals as bit array and convert to standard array
			vertNormalsSelArray = (theSelection[obj].modifiers[#Edit_Normals].getSelection node:theSelection[obj]) as array			
			
			for vertNorm = 1 to vertNormalsSelArray.count do
			(
				selNormal = vertNormalsSelArray[vertNorm]
				
				-- Loop through all vertex data previously collected for the object to
				-- find out which vertex this normal is for
				for vertData = 1 to AllVertexDataLists[obj].count do
				(
					-- There should only be one normal by this point due to the previous Unify operation
					if ( AllVertexDataLists[obj][vertData].vertNormal[1] == selNormal ) do
					(
						append matchingVertData AllVertexDataLists[obj][vertData]
					)
				)
			)

			progBar.prog.value = ((100* obj)/theSelection.count)
			progBar.prog.color = saveColour
			
			append allMatchingVertData matchingVertData
		)
		
		allMatchingVertData = SortNormalsByVertPos vertDistanceFactor allMatchingVertData
		
		for vertInfo = 1 to allMatchingVertData[1].count do
		(
			local idxFirstNode = 1
			local idxSecondNode			
			
			if ( theSelection.count != 2 ) then
			(
				-- Added this so that we try and use different body part normals for the average
				-- as I found a problem where sometimes if just two hand normals were used for the average,
				-- it could mess up arm normals for example.
				local patternSubStr = substring theSelection[idxFirstNode].name 1 4

				-- Add * so the pattern will end up like "head*"
				patternSubStr += "*"				

				for obj = 2 to theSelection.count do
				(
					if ( not matchpattern theSelection[obj].name pattern:patternSubStr ) do
					(
						idxSecondNode = obj
						break
					)
				)	

				if ( idxSecondNode == undefined ) do
				(
					idxSecondNode = 2
				)
			)
			else
			(
				idxSecondNode = 2
			)
			
			-- Get the first two normals for the first two verts and average
			node1 = getNodeByName theSelection[idxFirstNode].name
			node2 = getNodeByName theSelection[idxSecondNode].name

			normalID1 = allMatchingVertData[idxFirstNode][vertInfo].vertNormal[1]
			normalID2 = allMatchingVertData[idxSecondNode][vertInfo].vertNormal[1]			
			
			node1.modifiers[#Edit_Normals].AverageTwo node1 normalID1 node2 normalID2	
			format "Averaged Normal:% from % and Normal:% from %\n" normalID1 node1 normalID2 node2

			objectCount = theSelection.count
			if ( objectCount > 2 ) do 
			(
				normalCopy = node1.modifiers[#Edit_Normals].GetNormal normalID1 node:node1	

				-- Check all objects and paste the normal (if it isn't the 2nd node used for normal averaging)
				for obj = 2 to objectCount do
				(
					-- Make sure the vert info isn't undefined (can be if an object is selected but
					-- has no normals selected!
					if ( obj != idxSecondNode and allMatchingVertData[obj][vertInfo] != undefined ) do
					(
						normalPasteID = allMatchingVertData[obj][vertInfo].vertNormal[1]			
						pasteNode = getnodebyname theSelection[obj].name									
						pasteNode.modifiers[#Edit_Normals].SetNormal normalPasteID normalCopy node:pasteNode
					)
				) -- end object loop
			)
			
			progBar.prog.value = ((100* vertInfo)/allMatchingVertData[1].count)
			progBar.prog.color = createColour
		) -- end vert loop
		
		-- Make the normals explicit
		for objindex = 1 to theSelection.count do
		(
			normalSelArray = theSelection[objindex].modifiers[#Edit_Normals].getSelection node:theSelection[objindex]
			theSelection[objindex].modifiers[#Edit_Normals].MakeExplicit selection:normalSelArray node:theSelection[objindex]
		)
		(destroyDialog progBar)
	)
	
	else
	(
		Messagebox "No Edit_Normal modifier on the selection!"
	)
)

fn collapseToNormals =
(
	if ((progBar != undefined) and (progBar.isDisplayed)) do (destroyDialog progBar)
	CreateDialog progBar width:300 Height:30
	
	sliderTime = 0f
		
	if selection.count > 0 do
	(
		selA = selection as array
		for i=1 to selA.count do
		(
			outputSkinningData selA[i]--copy skinning
			maxOps.CollapseNode selA[i] off--collapse all
			inputSkinningData selA[i]--load skinning
			
			progBar.prog.value = ((100* i)/selA.count)
			progBar.prog.color = createColour
		)
		select selA
		(destroyDialog progBar)
	)
)

-------------------------------------------------------------------------------------------------------------------------
rollout RsCharSeamSnapperVerts "Average Snapping"
(
	button btnApplyEPModifier "Apply Edit Poly to Selected Objects" width:185 height:30
	checkbox cbBlendMapChannels "Blend Map Channels" checked:true
	spinner spnDistFactorVal "Vertex distance factor" range:[0.001,0.500, baseVertDistanceFactor] scale:0.001 fieldwidth:50
	button btnAverageSnapSelected "Average Snap Selected" width:185 height:50
	
	on RsCharSeamSnapperVerts open do
	(
		RsCharSeamSnapper.height = 220
	)
	
	on btnApplyEPModifier pressed do AddEditPolyMod()

	on btnAverageSnapSelected pressed do
	(
		-- Collect the verts on the selected objects, then do the snapping
		if ((CollectVerts RsCharSeamSnapperVerts.spnDistFactorVal.value) != false) do
		(
			AverageSnapSelectedVerts RsCharSeamSnapperVerts.spnDistFactorVal.value 
		)
	)
	
	-- Clear the data lists
	on RsCharSeamSnapperVerts close do
	(
		AllVertInfoLists = #()
	)
)	

-------------------------------------------------------------------------------------------------------------------------
rollout RsCharSeamSnapperNormals "Normals"
(
	button btnAddEditNormals "Add Edit Normals to selection" width:185 height:30
	button btnUnifyNormals "Unify Normals on selection" width:185 height:30
	spinner spnDistFactorVal "Vertex distance factor" range:[0.001,0.500, baseVertDistanceFactor] scale:0.001 fieldwidth:50
	button btnAverageNormals "Average Selected Normals" width:185 height:40
	button btnCollapseEditNormals "Collapse Edit Normals on selection" width:185 height:50
	
	on RsCharSeamSnapperNormals open do
	(
		RsCharSeamSnapper.height = 300
	)
	
	on btnAddEditNormals pressed do
	(		
		AddEditNormals()	
	)	

	on btnUnifyNormals pressed do
	(
		if queryBox "This will unify ALL normals.\r\nThis will break any hard seams or smoothing groups.\r\nYou can unify selected normals if you want to preserve others.\r\nDo you want to continue?" beep:false do
		(			
			UnifyNormals()
		)	
	)
	on btnAverageNormals pressed do
	(
		PreprocessVerts()
		AverageNormals RsCharSeamSnapperNormals.spnDistFactorVal.value
		forceCompleteRedraw()
	)
	
	on btnCollapseEditNormals pressed do
	(
		collapseToNormals()
	)
	
	on RsCharSeamSnapperNormals close do
	(
		AllVertexDataLists = #()
	)
)

-------------------------------------------------------------------------------------------------------------------------
rollout RsCharSeamSnapperSkinning "Skinning"
(
	button btnMeshSelect "Add Mesh Select" width:185 height:30
	spinner spnDistFactorVal "Vertex distance factor" range:[0.001,0.500, baseVertDistanceFactor] scale:0.001 fieldwidth:50
	dropDownList ddCopyFrom "" width:180 height:40 items:#()
	button btncopySkinning "Copy Skinning from/to Selected" width:185 height:50
	
	on RsCharSeamSnapperSkinning open do
	(
		RsCharSeamSnapper.height = 245
	)
	
	on btnMeshSelect pressed do 
	(
		try (ddCopyFrom.items = "-NONE") catch ()
		objSelection = #()
		for obj in selection do append objSelection obj.name
		sort objselection
		try (ddCopyFrom.items = objselection) catch ()
		
		modPanel.addModToSelection (Mesh_Select ()) ui:on
		subobjectLevel = 1
	)
	
	on btncopySkinning pressed do 
	(
		-- Need to collect the selected verts and order them by position
		userselection = getcurrentselection()
		copyFromNode = RsCharSeamSnapperSkinning.ddCopyFrom.selected
		theSelection = #()
		SortedNodeList = #()
		append SortedNodeList copyFromNode
		for obj in selection do appendifunique SortedNodeList obj.name
		
		-- We've sorted the list with the good skin object at the start
		CollectVertsSkin RsCharSeamSnapperSkinning.spnDistFactorVal.value SortedNodeList 
		
		-- Now we need to copy and paste the verts from the UI selection to the other objects	
		CopySkinWeightsAcrossVerts SortedNodeList  
		
		-- Clean shit up
		select userselection 
		try (subobjectLevel = 0) catch()
		
		-- Clear the global array list 
		AllVertInfoLists = #()
	)
)

rollout RsCharSeamSnapper "R* Character Seam Snapping Tool" width:220
(
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:(RsCharSeamSnapper.width)
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Ped_VertexTools" filename:(getThisScriptFilename())
	
	dotNetControl dn_tabs "System.Windows.Forms.TabControl" height:20 width:(RsCharSeamSnapper.width) align:#center pos:[-1, (RsBannerPanel.height + 2)]
	subRollout theSubRollout height:750 align:#center width:(RsCharSeamSnapper.width) pos:[0, dn_tabs.pos.y + dn_tabs.height]
		
	local lastClickNum
	local maxAutoHeight = 900
	local keepWidth
	
	local tabRollouts = 
	#(
		dataPair name:"Verts" rollouts:#(RsCharSeamSnapperVerts),
		dataPair name:"Normals" rollouts:#(RsCharSeamSnapperNormals, RsCharEyeNormals), 
		dataPair name:"Skinning" rollouts:#(RsCharSeamSnapperSkinning)
	)
	
	fn setSubrollSize = 
	(
		theSubRollout.height = (RsCharSeamSnapper.height - theSubRollout.pos.y)
	)
	
	fn showTab clickNum = 
	(
		if (lastClickNum != clickNum) do --do not update if the same tab clicked twice
		(
			lastClickNum = clickNum
			
			for subRoll in theSubRollout.rollouts do 
			(
				removeSubRollout theSubRollout subroll
			)
			
			local setHeight = theSubRollout.pos.y + 6

			for subRoll in tabRollouts[clickNum].rollouts do 
			(
				addSubRollout theSubRollout subRoll
				setHeight += (subRoll.height + 24)
			)
			
			if (setHeight > maxAutoHeight) do 
			(
				setHeight = maxAutoHeight
			)
			
			RsCharSeamSnapper.height = setHeight
			setSubrollSize()
		) 
	)
	
	on dn_tabs Selected itm do
	(
		local clickNum = (itm.TabPageIndex + 1)
		showTab clickNum
		
		RsSettingWrite "RsCharSeamSnapper" "lastTab" tabRollouts[clickNum].name
	)
	
	on RsCharSeamSnapper resized newSize do 
	(
		if (newSize.x != keepWidth) and (keepWidth != undefined) do 
		(
			RsCharSeamSnapper.width = keepWidth
		)
		
		setSubrollSize()
	)

	on RsCharSeamSnapper open do
	(
		banner.setup()
		
		keepWidth = RsCharSeamSnapper.width
		
		local savedTabName = RsSettingsReadString "RsCharSeamSnapper" "lastTab" undefined
		local savedTabNum = 1
		
		for tabNum = 1 to tabRollouts.count do
		(
			local tabName = tabRollouts[tabNum].name
			dn_tabs.TabPages.add tabName
			
			if (tabName == savedTabName) do 
			(
				savedTabNum = tabNum
			)
		)

		dn_tabs.SelectedIndex = (savedTabNum - 1)
		showTab savedTabNum
	)
)

createDialog RsCharSeamSnapper 220 400 style:#(#style_resizing,#style_titlebar, #style_toolwindow, #style_sysmenu)