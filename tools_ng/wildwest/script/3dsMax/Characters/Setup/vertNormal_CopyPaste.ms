-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
-------------------------------------------------------------------------------------------------------------------------

allMeshes = #()
AllVertInfoLists = #()	-- Contains vertData structs for every selected object's selected vertices
AllVertexDataLists = #()	-- Array for the PreprocessVerts and AverageNormals function
boneList = #()

-- GLOBALS -------------------------------------------
global rs_vertNormalCopyPaste

struct vertData 
(
	vertNumber,
	vertPos,
	vertNormal
)

-------------------------------------------------------------------------------------------------------------------------
-- Checks if two verts (across objects) are within the vert distance
--	factor from the spinner, to pair them together (for snapping later).
fn CheckIfVertsMatch vertDistanceFactor vertPos1 vertPos2 =
(
	distanceVal = vertPos1 - vertPos2	

	if (not (distanceVal.x > vertDistanceFactor or 
			distanceVal.y > vertDistanceFactor or 
			distanceVal.z > vertDistanceFactor or
			distanceVal.x < -vertDistanceFactor or 
			distanceVal.y < -vertDistanceFactor or 
			distanceVal.z < -vertDistanceFactor)) then 
	(
		true
	)
	else
	(
		false
	)
)

-------------------------------------------------------------------------------------------------------------------------
--fn: 	RsPreprocessVerts
--desc:	It goes through brute force to get every vertex on each selected object and 
-- 	their associated normals, which are put into a multi dimensional array of 
--	structs for each object selected
fn PreprocessVerts =
(
	theSelection = $selection as array

	AllVertexDataLists = #()

	if ((progBar != undefined) and (progBar.isDisplayed)) do (destroyDialog progBar)

	-- Goes through every vertex for every selected object and gets their normals
	for o = 1 to theSelection.count do 
	(	
		CreateDialog progBar width:300 Height:30
		
		select theSelection[o]
		numVerts = theSelection[o].Vertices.count
		vertexDataList = #()

		for v = 1 to numVerts do
		(
			vData = vertData()
			vertList = #{v}
			normalsList = #{} 
			
			if ( theSelection[o].Edit_Normals != undefined ) do 
			(
				theSelection[o].Edit_Normals.ConvertVertexSelection &vertList &normalsList
				vData.vertNormal = (normalsList as array)
			)
			vData.vertNumber = v
			vData.vertPos = theSelection[o].vertices[v].pos
			
			append vertexDataList vData
		)
		append AllVertexDataLists vertexDataList	
		
		progBar.prog.value = ((100* o)/theSelection.count)
		progBar.prog.color = orange
	)
	
	(destroyDialog progBar)
	select theSelection	
)


--fn: 	RsAddEditNormals
--desc:	Adds an Edit_Normal modifier and unifies them for
fn AddEditNormals =
(
	theSelection = getcurrentselection() as array
	
	-- Create Edit normals modifier and add it to the selection
	edtn = Edit_normals()
	edtn.displayLength = 0.01
	edtn.SelLevel = #Vertex
	edtn.ignoreBackfacing = off
	addModifier theSelection edtn
	
	-- Refresh the screen	
	forceCompleteRedraw()
	select theSelection
)

-------------------------------------------------------------------------------------------------------------------------
--fn:	RsSortNormalsByVertPos
--desc:	Sorts an array of vertex data structs based on vertex position, so averaging normals 
--	can be done.
fn SortNormalsByVertPos vertDistanceFactor selectedNormals =
(
	retArray = #()
	
	-- For each object we need to loop to see what vertices are matched by position
	for obj = 1 to selectedNormals.count do
	(
		objRowArray = #()
		
		-- Go through each vert data struct and check the vertPos
		for idxVertData = 1 to selectedNormals[1].count do
		(
			-- If first row, just copy that across
			if ( 1 == obj ) then
			(
				append objRowArray selectedNormals[obj][idxVertData]
			)
			else
			(
				-- If not the first object's vertices, loop through all vertex data
				-- structs to see which vertex positions match the first objects, for this
				-- indiviual object then add it to the objRowArray which is later appended
				-- to make the complete array that is returned, sorted.
				firstObjVertPos = retArray[1][idxVertData].vertPos
				-- go through all 
				for vData = 1 to selectedNormals[1].count do
				(					
					if ( selectedNormals[obj][vData] != undefined ) then
					(
						secondObjVertPos = selectedNormals[obj][vData].vertPos
					
						if ( true == CheckIfVertsMatch VertDistanceFactor firstObjVertPos secondObjVertPos ) do
						(
							append objRowArray selectedNormals[obj][vData]
						)
					)
					else
					(
						-- Incase we have a situation where there are objects selected that
						-- don't have any normals selected, just skip checking for any entries 
						-- for this object entirely
						break
					)
				)
			)
		)
		append retArray objRowArray
	)
	retArray
)

-------------------------------------------------------------------------------------------------------------------------
--fn:	RsAverageNormals
--desc:	Collects the normals for each vert now that the edit_normals modifier is
--	on the stack, then averages them across vertices.  Since averaging is between
--	just two normals, if we have more than 2 objects selected, we just copy the 
--	averaged normal to them.
fn AverageNormals vertDistanceFactor masterNormalsMesh allNormalsMeshes =
(
	allMatchingVertData = #() -- Stores all matches for each object

	for obj = 1 to allNormalsMeshes.count do
	(
		matchingVertData = #() -- stores matches for just this object
		vertNormalsSelArray = #()

		-- Get the selected normals as bit array and convert to standard array
		vertNormalsSelArray = (allNormalsMeshes[obj].modifiers[#Edit_Normals].getSelection node:allNormalsMeshes[obj]) as array

		for vertNorm = 1 to vertNormalsSelArray.count do
		(
			selNormal = vertNormalsSelArray[vertNorm]

			-- Loop through all vertex data previously collected for the object to
			-- find out which vertex this normal is for
			for vertData = 1 to AllVertexDataLists[obj].count do
			(
				-- There should only be one normal by this point due to the previous Unify operation
				if ( AllVertexDataLists[obj][vertData].vertNormal[1] == selNormal ) do
				(
					append matchingVertData AllVertexDataLists[obj][vertData]
				)
			)
		)
		append allMatchingVertData matchingVertData
	)

	allMatchingVertData = SortNormalsByVertPos vertDistanceFactor allMatchingVertData
	
	for vertInfo = 1 to allMatchingVertData[1].count do
	(
		-- Get the first two normals for the first two verts and average
		sourceNode = masterNormalsMesh[1]
		sourceNodePosition = findItem allNormalsMeshes masterNormalsMesh[1]
		sourceNormalID = allMatchingVertData[sourceNodePosition][vertInfo].vertNormal[1]
	
		if (allNormalsMeshes.count > 1) do 
		(
			normalCopy = sourceNode.modifiers[#Edit_Normals].GetNormal sourceNormalID node:sourceNode

			-- Check all objects and paste the normal if it isn't the source
			for obj = 1 to allNormalsMeshes.count do
			(
				-- Make sure the vert info isn't undefined (can be if an object is selected but
				-- has no normals selected!
				if (obj != sourceNodePosition) and (allMatchingVertData[1][vertInfo] != undefined) do
				(
					normalPasteID = allMatchingVertData[obj][vertInfo].vertNormal[1]--this part needs to know the vert on the other mesh.  it is pasting on to itself
					pasteNode = allNormalsMeshes[obj]
					pasteNode.modifiers[#Edit_Normals].SetNormal normalPasteID normalCopy node:pasteNode
				)
			) -- end object loop
		)
	) -- end vert loop
	
	-- Make the normals explicit
	for objindex = 1 to allNormalsMeshes.count do
	(
		normalSelArray = allNormalsMeshes[objindex].modifiers[#Edit_Normals].getSelection node:allNormalsMeshes[objindex]
		allNormalsMeshes[objindex].modifiers[#Edit_Normals].MakeExplicit selection:normalSelArray node:allNormalsMeshes[objindex]
	)
)--end AverageNormals

-------------------------------------------------------------------------------------------------------------------------
fn collapseToNormals =
(
	if ((progBar != undefined) and (progBar.isDisplayed)) do (destroyDialog progBar)
	CreateDialog progBar width:300 Height:30
	
	sliderTime = 0f
		
	if selection.count > 0 do
	(
		selA = selection as array
		for i=1 to selA.count do
		(
			outputSkinningData selA[i]--copy skinning
			maxOps.CollapseNode selA[i] off--collapse all
			inputSkinningData selA[i]--load skinning
			
			progBar.prog.value = ((100* i)/selA.count)
			progBar.prog.color = green
		)
		select selA
		(destroyDialog progBar)
	)
)--end collapseToNormals
---------------------------------------------------------
-- UI --------------------------------------------------
---------------------------------------------------------
(
	try (destroyDialog rs_vertNormalCopyPaste) catch()
	---------------------------------------------------------
	rollout rs_vertNormalCopyPaste "Vert Normal Copy/Paste"
	(
		dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:rs_vertNormalCopyPaste.width
		local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"" filename:(getThisScriptFilename())

		button btnDefineMeshes "Select Meshes" width:130
		button btnAddNormals "Add Edit Normals" width:130
		label label1 "Copy from:" align:#left
		dropdownlist ddlSourceMesh width:140 items:#()
		button btnPasteNormal "Paste Vertex Normals" width:130

		---------------------------------------------------------
		-- EVENTS ------------------------------------------
		-----------------------------------------------------------
		on radObjectOrMaterial changed newState do (theState = newState)
		-----------------------------------------------------------
		on btnDefineMeshes pressed do
		(
			if selection.count != undefined do
			(
				allMeshes = getCurrentSelection()

				listMeshes = #()
				for x=1 to allMeshes.count do
				(
					append listMeshes allMeshes[x].name
				)
				rs_vertNormalCopyPaste.ddlSourceMesh.items = listMeshes
			)
		)
		
		on btnAddNormals pressed do
		(
			if (allMeshes != undefined) and (rs_vertNormalCopyPaste.ddlSourceMesh.selection != 0) do (AddEditNormals())
		)

		on btnPasteNormal pressed do
		(
			selectedItem = rs_vertNormalCopyPaste.ddlSourceMesh.selection
			if (allMeshes != undefined) and (rs_vertNormalCopyPaste.ddlSourceMesh.selection != 0) do
			(
				copyMesh = #(allMeshes[selectedItem]) --the mesh to copy from
				pasteMeshes = deepCopy allMeshes --the meshes we'll copy to
-- 				deleteItem pasteMeshes selectedItem --remove the mesh we're copying from
				
				PreprocessVerts()
				AverageNormals 0.003 copyMesh pasteMeshes
				--collapseToNormals()
				forceCompleteRedraw()
			)
		)
		---------------------------------------------------------
		---------------------------------------------------------
		on rs_vertNormalCopyPaste open do
		(
			rs_dialogPosition "get" rs_vertNormalCopyPaste	
			rs_vertNormalCopyPaste.banner.setup()
		)
		---------------------------------------------------------
		on rs_vertNormalCopyPaste close do
		(
			rs_dialogPosition "set" rs_vertNormalCopyPaste
		)
	)
	createDialog rs_vertNormalCopyPaste width:165 style:#(#style_border,#style_toolwindow,#style_sysmenu)
)--end UI