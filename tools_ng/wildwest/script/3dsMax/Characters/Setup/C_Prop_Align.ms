-- Prop Align Script
-- Stewart Wright
-- January 2010

-- Update 22/02/10 - Added button and message boxes.  Help link added.
-- Update 10/03/10 - Added functionality for linking props to their parent bones.  This allows the max file to better reflect the ingame characters appearance.

-- Work out the project so we don't have to use hardcoded filein paths etc

-- Load common functions	
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/Fn_RSTA_Characters.ms")
filein "rockstar/export/settings.ms"


-- Load project specific prop settings
propsettingsfile = (theWildWest + "etc/config/characters/max_prop_settings.dat")

-- Figure out the project data
theProjectPedFolder = theProjectRoot + "art/peds/"





-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--create rollout
rollout AutoPropAlign "Auto Prop Align"
	
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Character_Prop_Align" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	--create buttons
	button btnAlignProp "Align Props" width:120 height:40 toolTip:"Automatically align props to their respective bones"
	button btnLinkProp "Link Props" width:120 height:40 toolTip:"Automatically link props to their respective bones using contraints"
	button btnUnlinkProp "Unlink Props" width:120 height:40 toolTip:"Automatically remove any constraint links between props and their respective bones"

	On btnAlignProp pressed do
	(
	select $p_*
	if selection.count == 0 then
		(	
		msgNoProps = "I can't find any props in the scene.  Are they named correctly?"
		messagebox msgNoProps
		)
	else
		(
		RSTA_alignPedProps ()
		msgPropsAligned = "All props should now be correctly aligned."
		messagebox msgPropsAligned
		)
	)
	
	On btnLinkProp pressed do
	(
	select $p_*
	if selection.count == 0 then
		(	
		msgNoProps = "I can't find any props in the scene.  Are they named correctly?"
		messagebox msgNoProps
		)
	else
		(
		RSTA_unlinkPedProps ()
		RSTA_linkPedProps ()
		msgPropsLinked = "All props should now be linked."
		messagebox msgPropsLinked
		)
	)
	
	On btnUnlinkProp pressed do
	(
	select $p_*
	if selection.count == 0 then
		(	
		msgNoProps = "I can't find any props in the scene.  Are they named correctly?"
		messagebox msgNoProps
		)
	else
		(
		RSTA_unlinkPedProps ()
		msgPropsUnlinked = "All props should now be unlinked in the scene."
		messagebox msgPropsUnlinked
		)
	)

	
	
)--end of rollout "Auto Prop Align"
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--Create the interface
createDialog AutoPropAlign width:130