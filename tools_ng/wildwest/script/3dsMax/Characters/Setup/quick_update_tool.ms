/*
A script to help artists update scenes with skinned meshes.
*/
--start fileins
	filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
	RsCollectToolUsageData (getThisScriptFilename())
(
---------------------------------------------------------
-- LOCALS --------------------------------------------
local allMeshes = #()
local skinnedMeshes = #()
local ddlAllMeshes = #()
local ddlSkinnedMeshes = #()
---------------------------------------------------------
-- GLOBALS -------------------------------------------
global rs_skinnedPedUpdateTool
---------------------------------------------------------
-- FUNCTIONS --------------------------------------
---------------------------------------------------------
--create an array with just names for the dropdown lists
fn ddlArrayInfo =
(
	ddlAllMeshes = #()
	ddlSkinnedMeshes = #()
	for allItem =1 to allMeshes.count do (append ddlAllMeshes allMeshes[allItem].name)
	for skinnedItem =1 to skinnedMeshes.count do (append ddlSkinnedMeshes skinnedMeshes[skinnedItem].name)
	
	rs_skinnedPedUpdateTool.ddlUnskinned.items = ddlAllMeshes
	rs_skinnedPedUpdateTool.ddlSkinned.items = ddlSkinnedMeshes
)--end ddlArrayInfo
---------------------------------------------------------
--fill some arrays with info about skinned and unskinned meshes.
fn collectSceneInfo =
(
	allMeshes = #()--empty
	skinnedMeshes = #()--empty

	for obj in selection do --for all objects in the scene
	(
		objRoot = WWfindRootObject obj --find the root of this object
		if ((superClassOf obj) == GeometryClass) do --if the object is geometry
		(
			propCheck = filterString obj.name "_"
			if (upperCase propCheck[1]) != "P" do appendIfUnique allMeshes obj --store the mesh if it isn't a prop
			if (isMeshSkinned obj) == true do appendIfUnique skinnedMeshes obj --if the mesh is skinned store it
		)
	)
	ddlArrayInfo()
)--end collectSceneInfo
---------------------------------------------------------
--skinwrap one mesh off another
fn skinwrapLODs skinTo skinFrom =
-- skinTo = mesh to skin
-- skinFrom = mesh to skin from
(
	print ("Skinwrapping " + (skinTo as string) + " off of " + (skinFrom as string))

	convertto skinTo editable_poly --convert mesh to edit poly
	ResetTransform skinTo --reset xforms
	convertto skinTo editable_poly --convert mesh to edit poly again to be sure
	select skinTo

	modPanel.addModToSelection (Skin_Wrap ()) ui:on

	skinWrapMod = skinTo.modifiers[#Skin_Wrap]

	skinWrapMod.meshList = #(skinFrom) --add the high mesh to the skinwrap list
	skinWrapMod.falloff = 1
	skinWrapMod.engine = 0 --face deformation
	skinWrapMod.threshold = 0.001
	skinWrapMod.weightAllVerts = on
	skinWrapMod.ConvertToSkin true

	print "Collapsing to Skin Wrap"
	maxOps.CollapseNodeTo skinTo 2 off

	skinMod = skinTo.modifiers[#Skin]
	
	print "Applying some default skin values"
	--toggle always deform off and on
	skinMod.always_deform = false
	skinMod.always_deform = true
	skinMod.cross_radius = 0.0834036
	skinMod.mirrorEnabled = off
	skinMod.bone_Limit = 4
	skinMod.clearZeroLimit = 0.0
	skinOps.RemoveZeroWeights skinTo.modifiers[#Skin]
	skinMod.Filter_Vertices = on
	skinMod.shadeweights = on
	skinMod.showNoEnvelopes = on

	print ((skinTo as string) + " skinwrapped.")
)--end skinwrapLODs
---------------------------------------------------------
-- UI --------------------------------------------------
---------------------------------------------------------
(
	try (destroyDialog rs_skinnedPedUpdateTool) catch()
	---------------------------------------------------------
	rollout rs_skinnedPedUpdateTool "Skinned Ped Update Tools"
	(
		dotNetControl rsBannerPanel "System.Windows.Forms.Panel" pos:[5,9] height:32	-- .NET FORM TO HOLD THE IMAGES
		local banner = makeRsBanner dn_Panel:rsBannerPanel width:162 studio:"north" mail:"Default_Tech_Art@rockstarnorth.com" wiki:"" -- INSTANCE THE STRUCT
		groupbox grpHeader width:157 height:44 pos:[4, 0]

		label lblSkinThis "Skin This Mesh:" align:#left
		dropdownlist ddlUnskinned width:140 items:ddlAllMeshes
		label lblSkinFrom "From This Mesh:" align:#left
		dropdownlist ddlSkinned width:140 items:ddlSkinnedMeshes
		button btnSkinNow "Skinwrap!" width:60 offset:[-30,0] tooltip:"Skinwrap the mesh"
		button btnReload "Reload" width:45 offset:[30,-26] tooltip:"Reload"
		---------------------------------------------------------
		-- EVENTS -------------------------------------------
		---------------------------------------------------------
		on btnSkinNow pressed do
		(
			if (ddlAllMeshes.count != 0) and (ddlSkinnedMeshes.count != 0) then --if we have some meshes to work with
			(
				whatMeshToSkin = findItem ddlAllMeshes rs_skinnedPedUpdateTool.ddlUnskinned.selected
				whatMeshToSkinFrom = findItem ddlSkinnedMeshes rs_skinnedPedUpdateTool.ddlSkinned.selected
		
				if (isMeshSkinned allMeshes[whatMeshToSkin] == true) then--if the mesh we are going to skinwrap is already skinned
				(
					if (queryBox "This mesh appears to already be skinned.\r\nYou are about to replace the skinning.\r\nDo you want to continue?" title:"Confirm!") do
					(
						skinwrapLODs allMeshes[whatMeshToSkin] skinnedMeshes[whatMeshToSkinFrom]--skinwrap
						collectSceneInfo()--update dropdown lists
					)
				)
				else
				(
					skinwrapLODs allMeshes[whatMeshToSkin] skinnedMeshes[whatMeshToSkinFrom]--skinwrap
					collectSceneInfo()--update dropdown lists
				)
			)
			else (messageBox "Please select some meshes and press Reload.")
		)
		---------------------------------------------------------
		on btnReload pressed do (collectSceneInfo())--update dropdown lists
		---------------------------------------------------------
		on rs_skinnedPedUpdateTool open do
		(
			collectSceneInfo()

			rs_dialogPosition "get" rs_skinnedPedUpdateTool	
			rs_skinnedPedUpdateTool.banner.setup()
		)
		---------------------------------------------------------
		on rs_skinnedPedUpdateTool close do
		(
			rs_dialogPosition "set" rs_skinnedPedUpdateTool
		)
	)
	createDialog rs_skinnedPedUpdateTool width:165 style:#(#style_border,#style_toolwindow,#style_sysmenu)
)--end UI
)