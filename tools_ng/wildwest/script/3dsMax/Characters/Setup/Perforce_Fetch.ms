/*
A quick script to fetch the crap that our exporter seems to have a habit of forgetting.
*/
-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

/*assetsCharacters = #("x:\\gta5\\assets\\characters\\...")--dds and xml files
WWP4vSync assetsCharacters

assetsMetadataCharacters = #("x:\\gta5\\assets\\metadata\\characters\\...")--meta
WWP4vSync assetsMetadataCharacters

assetsTexturesComp = #("x:\\gta5\\assets\\metadata\\textures\\componentpeds\\...")--componentpeds
assetsTexturesCuts = #("x:\\gta5\\assets\\metadata\\textures\\cutspeds\\...")--cutspeds
assetsTexturesStream = #("x:\\gta5\\assets\\metadata\\textures\\streamedpeds\\...")--streamedpeds
assetsTexturesStrProp = #("x:\\gta5\\assets\\metadata\\textures\\streamedpedprops\\...")--streamedpedprops
assetsTexturesProp = #("x:\\gta5\\assets\\metadata\\textures\\pedprops\\...")--pedprops
WWP4vSync assetsTexturesComp
WWP4vSync assetsTexturesCuts
WWP4vSync assetsTexturesStream
WWP4vSync assetsTexturesStrProp
WWP4vSync assetsTexturesProp
*/


exportAssets = 
#(
	RsConfigGetAssetsDir() + "characters/...",
	RsConfigGetAssetsDir() + "metadata/textures/componentpeds/...",
	RsConfigGetAssetsDir() + "metadata/textures/cutspeds/...",
	RsConfigGetAssetsDir() + "metadata/textures/streamedpeds/...",
	RsConfigGetAssetsDir() + "metadata/textures/streamedpedprops/...",
	RsConfigGetAssetsDir() + "metadata/textures/pedprops/..."
)



WWP4vSync exportAssets