-- Cloth Tools
-- Rick Stirling February 2011

try (destroyDialog clothTools) catch ()

-- Common script headers, setup paths and load common functions
--start fileins
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

-- ****************************************************************************************
rollout clothTools "Cloth Tools"
(
	dotNetControl rsBannerPanel "System.Windows.Forms.Panel" pos:[0,0] height:32 width:clothTools.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Character_cloth" versionNum:1.12 versionName:"Aromatic Front"
	
	button btnMakeCloth "Make Cloth From Face Selection" width:190 height:25
	button btnCleanupCloth "Cleanup Sim Mesh selected" width:190 height:25
	
	local idxIsCloth = (GetAttrIndex "Gta Object" "Is Cloth")
	
	fn CleanSmoothingGroups Obj =
	(
		PolyOp.setFaceSmoothGroup Obj.Baseobject #All 1
	)

	-- Set vertex-colour/illumination/alpha to black:
	fn BlackoutVertChannels Obj =
	(
		Obj.VertexColorType = 0
		--Obj.showVertexColors = on
		
		for Chan in #(0,-1,-2) do 
		(
			PolyOp.SetFaceColor Obj Chan #All Black
		)
	)

	fn ResetEditNormals Obj =
	(
		local EditNorms = Edit_Normals DisplayLength:0.01
		Addmodifier Obj EditNorms
		
		local SelNorms = #{1..EditNorms.GetNumNormals()}
		EditNorms.Select SelNorms
		EditNorms.Reset()
	)

	fn resetUVW meshSelected = 
	(
		PolyOp.ApplyUVWMap MeshSelected #Planar
	)

	------------------------------------------------------------------------------------
	-- Collapse modifiers and convert to poly, keeping skin-modifier:
	------------------------------------------------------------------------------------
	fn CollapseObj Obj =
	(
		-- Find 'Skin' modifier, if used:
		local SkinMod
		for ThisMod in Obj.Modifiers while (SkinMod == undefined) do 
		(
			if (isKindOf ThisMod Skin) do 
			(
				SkinMod = ThisMod
			)
		)
		
		-- Collapse/Convert object:
		ConvertToPoly Obj
		
		-- Reapply Skin modifier, if found:
		if (SkinMod != undefined) do 
		(
			Addmodifier Obj SkinMod
		)
	)
	
	---------------------------------------------------------------------------------
	-- Validate selection, and return selected object if valid:
	---------------------------------------------------------------------------------
	fn GetSelObj ClothCheck:False = 
	(
		local SelObjs = (GetCurrentSelection())
		local SelObj = undefined
		
		-- Is face-select mode currently active?
		case of 
		(
			(SelObjs.Count == 0):
			(
				MessageBox "Please select faces on an object, to generate Cloth mesh" Title:"Error: No object selected"
			)
			(SelObjs.Count > 1):
			(
				MessageBox "Please select just one object to generate Cloth mesh" Title:"Error: Multiple objects selected"
			)
			(ClothCheck and (not GetAttr SelObjs[1] idxIsCloth)):
			(
				MessageBox "Please select an object with 'Is Cloth' attribute set" Title:"Error: Non-cloth object selected"
			)
			Default:
			(
				SelObj = SelObjs[1]
			)
		)
		
		return SelObj
	)
	
	---------------------------------------------------------------------------------
	-- Apply cleanup-steps to Cloth Sim mesh
	---------------------------------------------------------------------------------
	fn CleanCloth SimMesh = 
	(
		CleanSmoothingGroups simMesh -- Cleans up smoothing groups
		BlackoutVertChannels simMesh -- Blacks out vertex-colour channels
		ResetEditNormals simMesh --Resets all the normals on the mesh
		ResetUVW simMesh --Resets UV using a UVW_Map modifier
		CollapseObj simMesh --Collapses the stack, keeping the skin on top
	)

	---------------------------------------------------------------------------------
	-- Generate Cloth Sim mesh from selected faces
	---------------------------------------------------------------------------------
	fn MakeCloth =
	(
		local RenderMesh = GetSelObj()
		if (RenderMesh == undefined) do return False
		
		local SelFaces = #{}
		
		-- Get selected faces, if in face-select mode:
		if (RsGetSubObjLevelName() == #Face) do 
		(
			SelFaces = RsGetSelFaceNums Rendermesh
		)
		
		-- Are any faces selected?
		if (SelFaces.NumberSet == 0) do 
		(
			MessageBox "Please select faces to generate Cloth mesh" Title:"Error: No faces selected"
			return False
		)
		
		-- Make ClothSim mesh as clone of SimMesh:
		local SimMesh = copy rendermesh
		SimMesh.name = "CLOTHSIM_" + rendermesh.name
		SimMesh.wirecolor = (RsGetRandomColour Excludes:#(RenderMesh.WireColor))
		
		select SimMesh
		
		-- Set 'Is Cloth' attribute, and link cloth-simulation mesh to the rendermesh:
		SetAttr SimMesh idxIsCloth True
		RsSceneLink.SetParent LinkType_RENDERSIM RenderMesh SimMesh
		
		-- Converts the mesh to Editable Poly (if it's an Editable Mesh) and collapse modifiers (apart from 'Skin')
		CollapseObj SimMesh
		
		-- Make sure baseobject is selected, in Face subobject mode:
		ModPanel.SetCurrentObject SimMesh.BaseObject
		SubobjectLevel = 4
		
		-- Update face-selection (earlier count may be different if object was converted)
		SelFaces = PolyOp.GetFaceSelection SimMesh.BaseObject
		
		-- Delete non-selected faces:
		local DelFaces = -SelFaces
		PolyOp.DeleteFaces SimMesh.BaseObject DelFaces
		
		-- Now that we have a Cloth mesh it needs to be cleaned up and processed:
		CleanCloth SimMesh
	)
	
	on clothTools open do
	(
		banner.setup()
	)
	
	on btnMakeCloth pressed do
	(
		undo "Make Cloth-Sim Mesh" on 
		(
			MakeCloth()
		)
	)
	
	on btnCleanupCloth pressed do
	(
		local SimMesh = (GetSelObj ClothCheck:True)
		if (not isValidNode SimMesh) do return False
		
		undo "Clean Cloth-Sim Mesh" on 
		(
			CollapseObj SimMesh
			CleanCloth SimMesh
		)
	)	
)

createDialog clothTools 210 400
