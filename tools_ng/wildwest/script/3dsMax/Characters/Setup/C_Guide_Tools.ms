/*
Guide Merge Script
Stewart Wright
Rockstar North
18/03/10
Try and work out what gender the model is and then merge in the correct guide file.  If model gender can't be determined
user is prompted to define it.
*/
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")



currentModelGender = ""
guideFile = undefined
mcbState = undefined
fcbState = undefined
selectedGuides = "Rigging Guides"
artistGuideFilesDepot = #(
						"//gta5_dlc/art/dev/peds/art_ped_foundations/World_Ped_Avatar_Guides/Female/F_Avatar_Garment_Standardisation_Guides.max",
						"//gta5_dlc/art/dev/peds/art_ped_foundations/World_Ped_Avatar_Guides/Male/M_Avatar_Garment_Standardisation_Guides.max"
						)
artistGuideFilesLocal = #(
						"x:\gta5_dlc\art\dev\peds\art_ped_foundations\World_Ped_Avatar_Guides\Male\M_Avatar_Garment_Standardisation_Guides.max",
						"x:\gta5_dlc\art\dev\peds\art_ped_foundations\World_Ped_Avatar_Guides\Female\F_Avatar_Garment_Standardisation_Guides.max"
						)


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- handles the missing dll Dialog and presses the default button (ok)
fn handleBox = 
 (
 	local windowHandle = DialogMonitorOPS.GetWindowHandle()
 	if (windowHandle != 0) then 
 	(
 		UIAccessor.PressDefaultButton()
 		true
 	)
 	else ( false )
 )

--  get all the child objects from the given object
 fn GetAllChildren theChildren &xMergeArr  =
(
	for c in theChildren do
	(
		append xMergeArr c.name
		GetAllChildren c.children &xMergeArr 
	)
)

-- try to figure out the gender based on the models name
-- set up some boolean states based on this
fn defineGender = 
(
	tempModelName = uppercase maxfilename
	currentModelGender = tempModelName

	if currentModelGender[3] == "M" then (
		defaultSkeleton = "Male Skeleton"
		mcbState = true
		fcbState = false
		)
	else	(
			if currentModelGender[3] == "F" then (
				defaultSkeleton = "Female Skeleton"
				mcbState = false
				fcbState = true
			)
		else	(
				defaultSkeleton = "Custom Skeleton"
				mcbState = false
				fcbState = false
			)
	)
) -- end defineGender


-- Need to decide what guides we are going to use
fn defineGuideFolder mcbState fcbState = 
(
	if (selectedGuides == "Rigging Guides") then
	(
		guessGuideFolder = (theProjectRoot + "art/peds/Skeletons//")
		
		if mcbState == True do (
			guessFilename = ("Male Guides.max")
			guideFile = (guessGuideFolder+guessFilename)
		)
		
		if fcbState == True do (
			guessFilename = ("Female Guides.max")
			guideFile = (guessGuideFolder+guessFilename)
		)
		
		if mcbState == false and fcbState == false do (
			guideFile = undefined
		)
	)
	else
	(
		if mcbState == True do 
		(			
			guideFile = artistGuideFilesLocal[1]
		)

		if fcbState == True do
		(			
			guideFile = artistGuideFilesLocal[2]
		)

		if mcbState == false and fcbState == false do 
		(
			guideFile = undefined
		)
	)
) -- End defineGuideFolder

-- to be able to load only the objects living under "Reference_Meshes", load an XRef Scene and get the names of all the children under 
-- that note. Save the name so it can be fed into the mergeFile command
fn LoadXRefScene =
(
	topNodeArr = #("REFERENCE_MESHES")
	xMergeArr = #()

	xrefs.deleteAllXRefs()

	-- register a callback to handle the "missing dll" warning
	DialogMonitorOPS.RegisterNotification handleBox id:#stopDllWarning
	DialogMonitorOPS.Enabled = true

	-- add a xref scene
	xrefs.addnewxreffile guidefile #noload

	-- returns first XRef Scene object
	aXRef=xRefs.getXRefFile 1
	aXRef.hidden = true 

	-- load the added xRef Scene
	updateXRef aXRef 

	-- unregister the callback
	DialogMonitorOPS.unRegisterNotification id:#stopDllWarning
	DialogMonitorOPS.Enabled = false

	-- for all objects in the XRef Scene under the top Note
	for ref in aXRef.tree.Children do
	(
		-- find the node with the specified name
		myRefMeshes=finditem topNodeArr  ref.name

		-- get all the children of the node with the specified name
		if myRefMeshes!=0 then
		(
			append xMergeArr ref.name
			GetAllChildren ref.children &xMergeArr
		)
	)

	-- delete all objects from the xRefScene
	xrefs.deleteAllXRefs()
	return xMergeArr

)

-- Merge the guides and supress all confirmations
fn mergeGuides objsToLoadArr:#() =
(
	if(selectedGuides == "Rigging Guides") then
	(
		mergeMaxFile guideFile #deleteOldDups #useSceneMtlDups #alwaysReparent quiet:true
	)
	-- only load the specifid objects
	else
	(
		mergeMaxFile guideFile objsToLoadArr #deleteOldDups #useSceneMtlDups #alwaysReparent quiet:true
	)
)

-- delete guides if they exist
fn deleteGuides delObj =
(
	try (select $m_Guide_Dummy) catch()
	try (select $f_Guide_Dummy) catch()
	
	try 
	(
	-- I can't find a cleaner way to select the objects heirachy - this'll have to do
	actionMan.executeAction 0 "40180"
	) catch ()
	
	-- check the stack of the low mesh and delete any projection modifiers
	for checkProj = 1 to obj.modifiers.count	do
	(
	if classof obj.modifiers[checkProj] == projection then
		(
		deleteModifier obj checkProj
		)
	)
) -- End deleteGuides


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- collects all children recursively. arguments:
-- found on http://forums.cgsociety.org/showpost.php?p=4427477&postcount=2
fn collectChildren obj &allChildren includeParent:false includeHidden:false =
(
	-- check if object should be collected
	if (includeHidden or not obj.isHiddenInVpt) and includeParent and finditem allChildren obj == 0 then
	
	append allChildren obj

	-- collect current object's children
	for c in obj.children do collectChildren c &allChildren includeParent:true includeHidden:includeHidden
) -- End collectChildren


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
rollout GuideMergeTool "Guide Tools"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Character_Guide_Merge_Tool" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	checkBox chkbxMale "Male" pos:[30, 22] checked:mcbState tooltip:"Choose default Male skeleton"
	checkBox chkbxFemale "Female" pos:[90, 22] checked:fcbState tooltip:"Choose default Female skeleton"
	dropdownlist ddl_guides  items:#("Rigging Guides", "Artist Guides")
	button btnHideCrap "Hide Collision & Constraints" width:150 height:30
	button btnMergeGuides "Merge Guides" width:150 height:60
	button btnDelGuides "Delete Guides" width:150 height:60
	
	on GuideMergeTool open do (
		defineGuideFolder mcbState fcbState
		WWP4vSync pedGuideFiles

		-- unregister the dll warning callback, in case its still hanging around because the tool broke 
		DialogMonitorOPS.unRegisterNotification id:#stopDllWarning
		DialogMonitorOPS.Enabled = false

		-- Sync the latest Guides files when tool opens, before user interaction || bug:7770707
		with redraw off
		(
			try
			(
				gRsPerforce.Connect()
				gRsPerforce.Sync artistGuideFilesDepot callerName:(getFileNameFile (getThisScriptFileName()))
			)
			catch
			(
				format "\nERROR: Could not sync latest Guides files.\n"
			)
		)
		
		)
	
	on btnHideCrap pressed do (
			hideCollAndConst()
		)
	
	on btnMergeGuides pressed do (

		if (selectedGuides == "Rigging Guides") then
		(

			if guideFile != undefined then 
			(
				-- merge the guides
				mergeGuides ()
			)
			
			else 
			(
				messagebox "Please define the model's gender"	
			)
		)
		else 
		(
			if guideFile != undefined then 
			(
				-- get a XRefScene first to get the names of the meshes we wanna merge, as we dont wanna merge the whole scene
				xMergeArr = LoadXRefScene()
			
				if (xMergeArr.count!=0) do 
					(
						-- merge the meshes stored in the xMergeArr
						mergeGuides objsToLoadArr:xMergeArr
						messageBox "Guides Loaded"
					)
			)
			else 
			(
				messagebox "Please define the model's gender"	
			)
		)
	)
	
	
	on btnDelGuides pressed do (
		if (selectedGuides == "Rigging Guides") then
		(	
			try (
				allChildren = #()
				collectChildren $m_Guide_Dummy &allChildren includeParent:true includeHidden:true
				delete allChildren
				layerManager.deleteLayerByName "m_Guides"
			) catch()
			
			try (
				allChildren = #()
				collectChildren $f_Guide_Dummy &allChildren includeParent:true includeHidden:true
				delete allChildren
				layerManager.deleteLayerByName "f_Guides"
			) catch()
			allChildren = #()
		)
		else 
		(
			try
			(
			allChildren = #()
			collectChildren $REFERENCE_MESHES &allChildren includeParent:true includeHidden:true
			delete allChildren
			)
			catch()
		)
	)
	
	
	on chkbxMale changed theState do (
		if chkbxMale.state == true then (
		chkbxFemale.checked = false
						
		mcbState = true
		fcbState = false
		)
		
		else (
		mcbState = false
		fcbState = false
		)
		defineGuideFolder mcbState fcbState
	)
	
	
	on chkbxFemale changed theState do (
		if chkbxFemale.state == true then (
		chkbxMale.checked = false
					
		mcbState = false
		fcbState = true
		)
		
		else (
		mcbState = false
		fcbState = false
		)
		defineGuideFolder mcbState fcbState
	)

	on ddl_guides selected sel do 
	(			
		selectedGuides = ddl_guides.selected
		defineGuideFolder mcbState fcbState
	)

)

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

defineGender()

createDialog GuideMergeTool width:160