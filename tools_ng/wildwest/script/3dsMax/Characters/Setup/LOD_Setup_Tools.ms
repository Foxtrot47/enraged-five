-------------------------------------------------------------------------------------------------------------------------
--start fileins
	filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
	
	-- Light settings are stored in a config file.  Load the vertex and light settings required for the tool
	vbakesettings = pathToConfigFiles + "general/max_vertex_bake_settings.dat"
	try (filein vbakesettings) catch (messagebox ("Couldn't find project specific light settings file. \r\nWas expecting " + vbakesettings ))
--end fileins
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--common variables
	--common variables for AutoLOD section
	skinnedState = false
	lowLOD = #()--all the LOD meshes
	highLOD = #()--all the High meshes (no props)
	boneNames = #()--the names of the bones in the skin modifier (handier than bone ID's)
	lowLODStatus = false
	unskinnedMeshes = #()
	vertSkinValues = #()--an array to store the vert skin weight values while we work on them
	addedWeight = undefined--a toggle so I know if a bone needs added or just updated
	tempPed_stdVSkelToLODSkel = ""
		
	matchedParts = #()
	LOD2MesheNames = #()
	LOD2Meshes = #()
	targetCount = undefined
	maleLOD2Parts = "x:\gta5\art\peds\Ped_Models\LOD2_Male\LOD2_Male.max"
	femaleLOD2Parts = "x:\gta5\art\peds\Ped_Models\LOD2_Female\LOD2_Female.max"
	maleMergeParts = getmaxfileobjectnames "x:\gta5\art\peds\Ped_Models\LOD2_Male\LOD2_Male.max"
	femaleMergeParts = getmaxfileobjectnames "x:\gta5\art\peds\Ped_Models\LOD2_Female\LOD2_Female.max"
		
	--common variables for camera script
	targetPos = [-3.488e-005,0.0319061,1.28313]
	camFOV = 45
	camNC = 1
	camFC = 1000
	camNR = 0
	camFR = 1000
	camMPass = off
	camPos = undefined
	camSel = off
	autoToggle = false
	LODCam = undefined
	camClose = -4.5
	camFar = -50
	--end common variables
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--attempt to find the export dummy
fn findTheDummy =
(
	characterNode = undefined
	try
	(
		selectByWildCard "head_000" --there should always be a head mesh
		if selection.count > 0 then --if we find at least 1 mesh
		(
			exportDummy = undefined
			for obj in selection do
			(
				filtered = filterstring (WWfindRootObject obj).name "-"--filter the dummy for -
				if filtered.count == 1 do--if we found a - we will have a count greater than 1, meaning it is not the main dummy
				(
					exportDummy = WWfindRootObject obj
				)
			)
			exportDummy
		)
		else
		(
			print "I couldn't find my head"
		)
	)catch()
)--end findTheDummy

-------------------------------------------------------------------------------------------------------------------------
-- collects all children recursively. arguments:
-- found on http://forums.cgsociety.org/showpost.php?p=4427477&postcount=2
fn collectChildren obj &allChildren includeParent:false includeHidden:false =
(
	-- check if object should be collected
	if (includeHidden or not obj.isHiddenInVpt) and includeParent and finditem allChildren obj == 0 then
	(
		append allChildren obj
	)
	-- collect current object's children
	for c in obj.children do
	(
		collectChildren c &allChildren includeParent:true includeHidden:includeHidden
	)
) -- end collectChildren

-------------------------------------------------------------------------------------------------------------------------
--create the dummy
fn makeDummy dummyScale dummyName linkMeshes=
--dummyScale = the scale of the dummy
--dummyName = the name of the dummy
--linkMeshes = an array of meshes to link
(
	-- use our function to delete dummy if it exists
	actualLODDummy = (getNodebyName dummyName)
	try
	(
		delete actualLODDummy
		print (dummyName + " dummy deleted")
	)catch(print ("no dummy called " + dummyName + " to delete"))
	
	--make LOD dummy
	newDummy = Dummy pos:[0,0,4] isSelected:on
	scale newDummy dummyScale
	ResetTransform newDummy
	newDummy.name = dummyName
	newDummy--pass out the dummy node we just created
	
	if linkMeshes != undefined do (linkMeshes.parent = newDummy)
) -- end makeDummy

-------------------------------------------------------------------------------------------------------------------------
--prebake all selected model parts to a default gray
fn prebake bakeObj =
(
	setCommandPanelTaskMode #modify

	bakeObj.showVertexColors = on
	bakeObj.vertexColorType = 0
	
	-- For each object in turn, go to the base mesh  
	try (
		modPanel.setCurrentObject bakeObj.baseObject
		subobjectLevel = 1 --go to vertex mode

		nv = bakeObj.numverts --get number of verts in the object  
		if classof bakeObj == Editable_Poly do
		(
			for i = 1 to nv do
			(
				polyop.setvertcolor $ TheColourChannel i Ped_BakeColour
				polyop.setvertcolor $ TheRimChannel i Ped_RimColour
				polyop.setvertcolor $ TheWindChannel i Ped_WindColour
				polyop.setvertcolor $ TheEnvEffChannel i Ped_EnvEffColour
				polyop.setvertcolor $ TheAlphaChannel i Ped_AlphaColour
				polyop.setvertcolor $ 20 i black
				polyop.setvertcolor $ 21 i black
				polyop.setvertcolor $ 22 i black
			)	
		)
		subobjectLevel = 0 --exit vertex mode
	)catch (print "Couldn't get to object base Epoly/EMesh")
)--end prebake

-------------------------------------------------------------------------------------------------------------------------
--unify vet normals
fn unifyVertNormals meshObj =
(
	setCommandPanelTaskMode #modify
	
	select meshObj
	
	-- Create Edit normals modifier and add it to the selection
	edtn = Edit_normals()
	edtn.displayLength = 0.01
	edtn.SelLevel = #Vertex
	edtn.ignoreBackfacing = off
	addModifier meshObj edtn
	
	-- Unify the normals 
	meshObj.modifiers[#Edit_Normals].SetSelection #{1..1000000} node:meshObj
	meshObj.modifiers[#Edit_Normals].unify node:meshObj
	meshObj.modifiers[#Edit_Normals].SetSelection #{} node:meshObj
	
	convertto meshObj editable_poly --convert mesh to edit poly again to be sure
	
	deselect meshObj
	forceCompleteRedraw() -- Refresh the screen
)--end unifyVertNormals

-------------------------------------------------------------------------------------------------------------------------
--"clean" the meshes
fn cleanGeo cleanObj normState bakeState =
(
	convertto cleanObj editable_poly --convert mesh to edit poly
	ResetTransform cleanObj --reset xforms
	convertto cleanObj editable_poly --convert mesh to edit poly again to be sure

	if normState == true then
	(
		print ("Unifying normals on " + (cleanObj.name as string))
		unifyVertNormals cleanObj
	)else(print ("Vertex Normals Untouched This Time On " + (cleanObj.name as string)))

	if bakeState == true then
	(
		print ("Starting Vertex Bake on " + (cleanObj.name as string))
		prebake cleanObj
	)else(print ("No Vertex Bake This Time On " + (cleanObj.name as string)))
)--end cleanGeo

-------------------------------------------------------------------------------------------------------------------------
--work out what our high LOD meshes are
fn getHigh =
(
	tempAll = #()
	highLOD = #()
	theModelDummy = findTheDummy()
	collectChildren theModelDummy &tempAll includeParent:false includeHidden:true --collect all the LOD heirarchy
	selectionsets["*Complete"] = tempAll --create selection set of everything
	
	for obj in selectionsets["*Complete"] do
	(
		if uppercase (substring obj.name 1 2) != "P_" do append highLOD obj
	)
	selectionsets["*HighLOD"] = highLOD --create selection set of highLOD meshes
)--end getHigh

-------------------------------------------------------------------------------------------------------------------------
--check the LOD situation. people might have renamed them or moved them etc
fn checkLow =
(
	theModelDummy = findTheDummy()
	LODDummyName = theModelDummy.name + "-l"
	actualObject = (getNodebyName LODDummyName)

	try
	(
		collectChildren actualObject &lowLOD includeParent:false includeHidden:true --collect all the LOD heirarchy
		
		if selectionsets["*LowLOD"] != undefined then
		(
			if (selectionsets["*LowLOD"].count == lowLOD.count) and (lowLOD.count == highLOD.count) then
			(
				print "lowLOD meshes found"
				lowLODStatus = true
			)
			else
			(
				msgText = "There are " + selectionsets["*HighLOD"].count as string +" objects in the HighLOD selection set.\r\nThere are " + selectionsets["*LowLOD"].count as string +" objects in the LowLOD selection set.\r\nThere are " + lowLOD.count as string + " tagged as LODs.\r\nThese numbers should be equal.\r\nPlease check the following:\r\n-Suffix LOD to all LOD mesh names\r\n-Add all LOD meshes to the *LowLOD selection set\r\n-Ensure all meshes are linked to the correct export dummy."
				messagebox msgText
				lowLODStatus = false
			)
		)
		else
		(
				msgText = "Unable to find *LowLOD selection set.\r\nPlease create one."
				messagebox msgText
				lowLODStatus = false
		)
	)catch (messagebox "Can't Find LOD Dummy") --attempt to select the export dummy
)--end checkLow

-------------------------------------------------------------------------------------------------------------------------
--create the low LOD meshes from the high LOD ones
fn makeLODMeshes =
(
	lowLOD = #()
	
	--clone the high objects, rename them and add them to a LOD array
	for c=1 to highLOD.count do with redraw off -- speed up operation doing it with no redraw
	(
		singleLOD = #() -- an array for new objects 
		maxOps.CloneNodes highLOD[c] cloneType:#copy newNodes:&singleLOD --clone as copy and add to copied object array

		cleanGeo singleLOD[1] false false--run the cleangeo function to reset xforms and switch to editpoly

		singleLOD[1].name = (highLOD[c].name +" LOD") --set the new name for the LOD mesh

		appendIfUnique lowLOD singleLOD[1]
	)
	selectionsets["*LowLOD"] = lowLOD
	print "low LOD meshes built"
	lowLOD --pass out the meshes we built
)--end makeLODMeshes

-------------------------------------------------------------------------------------------------------------------------
--Merge the LOD head and hands and supress all confirmations
fn mergeLODFile mergeFile =
--mergeFile = defines the merge source
(
	selectedHeads = #()
	tempSelectedHeads = #()
	
	select $head_*
	deselect $*LOD --deselect the lods
	tempSelectedHeads = getCurrentSelection()
	clearSelection()
	
	for s=1 to tempSelectedHeads.count do
	(
		if classof tempSelectedHeads[s] == PolyMeshObject then append selectedHeads tempSelectedHeads[s]
	)
	
	for h=1 to selectedHeads.count do
	(
		highHead = selectedHeads[h].name
		LODHeadName = highHead + " LOD"
		
		actualLOD = (getNodeByName LODHeadName)
		try (delete actualLOD)catch()--attempt to delete existing lod head and hands if they exist

		--merge in the gizmo thing as mover
		print ("Merging LOD head and hands for " + highHead)
		mergeLOD = #("LOD_Head_and_Hands")--what do we want to merge?
		
		try --and merge in the LOD hands
		(
			mergeMaxFile mergeFile mergeLOD #deleteOldDups #useSceneMtlDups #alwaysReparent quiet:true
			$LOD_Head_and_Hands.name = LODHeadName
			print (highHead + "'s LOD Head and Hands merged")
			newLODHands = (getNodeByName LODHeadName)
			
			lowLOD = #()
			for obj in selectionsets["*LowLOD"] do append lowLOD obj
			appendIfUnique lowLOD newLODHands
			selectionsets["*LowLOD"] = lowLOD
		)catch(print "LOD not merged")
	)
)--end mergeLODFile

-------------------------------------------------------------------------------------------------------------------------
--skinwrap the low mesh off of the high mesh if their names match
fn skinwrapLODs lowmesh skinMeshes =
(
	shortSkin = #()
	shortLow = #()
	
	for sn=1 to skinMeshes.count do
	(
		shortenedName = substring skinMeshes[sn].name 1 10
		shortenedName = uppercase shortenedName
		append shortSkin shortenedName
	)
	
	findMe = substring lowmesh.name 1 10
	findMe = uppercase findMe
	foundSkinPos = findItem shortSkin findMe
		
	if foundSkinPos != 0 then
	(
		skinMesh = skinMeshes[foundSkinPos]
		checkMe = findMe = substring lowmesh.name 1 10
		checkMe = uppercase checkMe
		print ("Skinwrapping " + findMe + " off of " + checkMe)
		--skinwrap the low off of the high and convert to skin
		select lowMesh
		modPanel.addModToSelection (Skin_Wrap ()) ui:on
			
		lowMesh.modifiers[#Skin_Wrap].meshList = #(skinMesh) --add the high mesh to the skinwrap list
		lowMesh.modifiers[#Skin_Wrap].falloff = 1
		lowMesh.modifiers[#Skin_Wrap].engine = 0 --face deformation
		lowMesh.modifiers[#Skin_Wrap].threshold = 0.001
		lowMesh.modifiers[#Skin_Wrap].weightAllVerts = on
		lowMesh.modifiers[#Skin_Wrap].ConvertToSkin true

		print "Collapsing to Skin Wrap"
		maxOps.CollapseNodeTo lowMesh 2 off

		print "Applying some default skin values"
		--toggle always deform off and on
		lowMesh.modifiers[#Skin].always_deform = false
		lowMesh.modifiers[#Skin].always_deform = true
		lowMesh.modifiers[#Skin].cross_radius = 0.0834036
		lowMesh.modifiers[#Skin].mirrorEnabled = off
		lowMesh.modifiers[#Skin].bone_Limit = 4
		lowMesh.modifiers[#Skin].clearZeroLimit = 0.0
		skinOps.RemoveZeroWeights lowMesh.modifiers[#Skin]
		lowMesh.modifiers[#Skin].Filter_Vertices = on
		lowMesh.modifiers[#Skin].shadeweights = on
		lowMesh.modifiers[#Skin].showNoEnvelopes = on

		skinnedState = true
		print (findMe + " skinwrapped.")
	)
	else
	(
		skinnedState = false
		print ("Couldn't find a match for " + findMe + ".")
	)
)--end skinwrapLODs

-------------------------------------------------------------------------------------------------------------------------
--get the skinned verts for a mesh (theMesh) and add them to the vertSkinValues array
fn vertSkin theMesh =
(	
	print ("getting vertex skin weights off of " + theMesh.name)
	vertSkinValues = #()
	
	vertCount = skinOps.getNumberVertices theMesh.modifiers[#skin]
	
	for x=1 to vertCount do
	(
		vertArray = #()
		BNumber = skinOps.getVertexWeightCount theMesh.modifiers[#skin] x
		
		for i = 1 to BNumber do
		(
			boneID = skinOps.getVertexWeightBoneId theMesh.modifiers[#skin] x i
			boneName = skinOps.GetBoneName theMesh.modifiers[#skin] boneID 0
			boneWeight = skinOps.getVertexWeight theMesh.modifiers[#skin] x i

			tempArray = #(boneID, boneName, boneWeight)
			append vertArray tempArray
		)
		append vertSkinValues vertArray
	)
	print ("vertex skin weights grabbed off of " + theMesh.name)
)--end vertSkin

-------------------------------------------------------------------------------------------------------------------------
--check the vert weights for the bone we want to remove (fromBone).  If found, we add the weight on the the parent bone (toBone).
fn replaceWeights fromBone toBone toBoneID =
(
	for mainArray=1 to vertSkinValues.count do --for every vert do
	(
		for vertBoneArray=1 to vertSkinValues[mainArray].count do --for every weight do
		(
			if fromBone == vertSkinValues[mainArray][vertBoneArray][2] do
			(
				addedWeight = false
				
				print ("remove bone "+ vertSkinValues[mainArray][vertBoneArray][2] + " on vert "+ mainArray as string)
				
				weightToMove = vertSkinValues[mainArray][vertBoneArray][3]
				
				--lets check the other weights on this vert for a match with our parent bone
				for recheckVertBoneArray=1 to vertSkinValues[mainArray].count do
				(
					if vertSkinValues[mainArray][recheckVertBoneArray][2] == toBone do
					(
						weightToAdd = vertSkinValues[mainArray][recheckVertBoneArray][3]
						weightToWrite = weightToAdd + weightToMove
						vertSkinValues[mainArray][recheckVertBoneArray][3] = weightToWrite -- set parent to new weight
						vertSkinValues[mainArray][vertBoneArray][3] = 0 -- clear child weight
						print ("added " + vertSkinValues[mainArray][vertBoneArray][2] + "'s weight to " + vertSkinValues[mainArray][recheckVertBoneArray][2])
						addedWeight = true
					)--end add weight loop
				)
				if addedWeight != true do
				(
					print ("adding " + toBone + " with weight of " + weightToMove as string +" to new array")
					newArray = #(toBoneID, toBone, weightToMove)
					vertSkinValues[mainArray][vertBoneArray] = newArray
					addedWeight = true
				)--end new bone and weight loop
			)--end found toBone loop
		)
	)
)--end replaceWeights

-------------------------------------------------------------------------------------------------------------------------
--add bones from the ped_stdVSkelToLODSkel back to the modifier if they are missing
fn addBones2LOD theMesh =
(
	bonesInSkin = #()
	bonesMissing = #()
	bonesWeNeed = #()
	
	for ac=1 to ped_stdVSkelToLODSkel.count do
	(
		append bonesWeNeed ped_stdVSkelToLODSkel[ac][1]
	)
	
	numberSkinBones = skinops.getNumberBones theMesh.modifiers[#Skin]
	
	for bs = 1 to numberSkinBones do
	(
		boneName = skinOps.GetBoneName theMesh.modifiers[#Skin] bs 1
		appendIfUnique bonesInSkin boneName --append to this array so we get a list of all bones that are actually in the skin
	)
	
	--now compare the array of all bones vs the ones actually skinned to
	for bs = 1 to bonesWeNeed.count do
	(
		found = (findItem bonesInSkin bonesWeNeed[bs])
		if found == 0 do
		(
			appendIfUnique bonesMissing bonesWeNeed[bs]
		)
	)
	
	if bonesMissing.count > 0 do
	(
		print "-----------------------------------"
		print ("Bones not used on "+theMesh.name+" are:")
		print bonesMissing
		print "================================"
	)
	
	for addThese = 1 to bonesMissing.count do
	(
		boneToAdd = getNodeByName bonesMissing[addThese]

		boneUpdateInt = undefined
		if addThese != bonesMissing.count then --this checks wether we need to redraw view which MASSIVELY speeds this up
		(
			boneUpdateInt = 1
		)
		else
		(
			boneUpdateInt = 0
		)
		
		if boneToAdd != undefined do
		(
			skinOps.addbone theMesh.modifiers[#Skin] boneToAdd boneUpdateInt
			print "-----------------------------------"
			print (bonesMissing[addThese] + " added to " + theMesh.name)
			print "================================"
		)
	)
)

-------------------------------------------------------------------------------------------------------------------------
--find the parent of the bones in the modifier
fn whosYourDaddy =
(
	parentBones = #()
	
	print "finding bone parents"
	for parentBone=1 to boneNames.count do
	(
		actualBone = (getNodeByName boneNames[parentBone])
		daddy = actualBone.parent.name
		tempArray = #(boneNames[parentBone], daddy)
		append parentBones tempArray
	)
	
	--make a temporary array with the child LODs from our master array
	tempLODchild = #()
	for temp=1 to tempPed_stdVSkelToLODSkel.count do
	(
		append tempLODchild tempPed_stdVSkelToLODSkel[temp][1]
	)
	
	--check the LOD master array for missing bones, add them if needed
	for tempChild=1 to parentBones.count do
	(
		if findItem tempLODchild parentBones[tempChild][1] == 0 do
		(
			findWhat = parentBones[tempChild][1]
			
			foundIt = false
			findDaddy = findWhat
			
			while foundIt == false do
			(
				actualBone = (getNodeByName findDaddy)
				daddy = actualBone.parent.name
				
				findLOD = findItem ped_vLODBones daddy
				if findLOD == 0 then
				(
					findDaddy = daddy
					foundIt = false
				)
				else
				(
					daddy = ped_vLODBones[findLOD]
					tempArray = #(findWhat, daddy)
					print findDaddy + "'s parent bone " + daddy + " found."
					foundIt = true
				)
			)
			append tempPed_stdVSkelToLODSkel tempArray
			print ("missing bone " + findWhat + " added with parent " + daddy)
		)
	)
	
	tempLODClone = deepCopy tempPed_stdVSkelToLODSkel
	--make a temporary array with the child LODs from our master array
	tempLODchild = #()
	for temp=1 to tempPed_stdVSkelToLODSkel.count do
	(
		append tempLODchild tempPed_stdVSkelToLODSkel[temp][1]
	)
	
	--match the position of bones in our main array against their position in the boneNames array
	print "updating master LOD array with new parent information"
	for bonePos=1 to parentBones.count do
	(
		LODArrayPos  = findItem tempLODchild parentBones[bonePos][1]
		if LODArrayPos != 0 then
		(
			tempLODClone[bonePos] = tempPed_stdVSkelToLODSkel[LODArrayPos]
			print (parentBones[bonePos][1] + "'s array position updated")
		)else(print "no match")
	)
	
	if tempLODClone.count == boneNames.count then
		(
			tempPed_stdVSkelToLODSkel = tempLODClone
			print "Parents Found"
		)else (print "Lost Child")
)--end whosYourDaddy

-------------------------------------------------------------------------------------------------------------------------
--update the skinning on the LOD mesh using our filtered vertSkinValues array
fn updateLODSkin theMesh =
(
	meshName = theMesh.name
	print ("updating the skinning on " + theMesh.name)
	
	for vertNum = 1 to vertSkinValues.count do
	(
		tempBoneArray = #()
		tempWeightArray = #()
		
		for affectingBones=1 to vertSkinValues[vertNum].count do
		(
			boneID = findItem boneNames vertSkinValues[vertNum][affectingBones][2]
			append tempBoneArray boneID
			append tempWeightArray vertSkinValues[vertNum][affectingBones][3]
		)
		
		theVert = vertNum
		theSkin = theMesh.modifiers[#skin]
		boneArray = tempBoneArray
		weightArray = tempWeightArray
		
		skinOps.ReplaceVertexWeights theskin theVert boneArray weightArray
	)
	print ("skinning updated on " + theMesh.name)
)--end updateLODSkin

-------------------------------------------------------------------------------------------------------------------------
fn LODSkinSet LODMesh =
(
	tempPed_stdVSkelToLODSkel = deepCopy ped_stdVSkelToLODSkel --make a copy of the source list.  we'll do this for each mesh to support per component skeletons
	boneNames = #()
	
	addBones2LOD LODMesh
	
	selBoneCount = skinOps.GetNumberBones LODMesh.modifiers[#Skin]

	--get the bone name order
	for sb=1 to selBoneCount do
	(
		singleBoneName = skinOps.GetBoneName  LODMesh.modifiers[#Skin] sb 0
		append boneNames singleBoneName
	)

	whosYourDaddy() --work out the parent of each bone

	vertSkin LODMesh --go build an array of the meshes skinned vert information
	
	--check the current bone list for a match in the LOD bone list
	for bn = boneNames.count to 1 by -1 do
	(
		if findItem ped_vLODBones boneNames[bn] == 0 do
		(
			boneOut = tempPed_stdVSkelToLODSkel[bn][1]
			boneIn = tempPed_stdVSkelToLODSkel[bn][2]
			boneInID = findItem boneNames tempPed_stdVSkelToLODSkel[bn][2]
			replaceWeights boneOut boneIn boneInID
		)
	)
	
	updateLODSkin LODMesh --update the skinning
	
	--some housekeeping
	LODMesh.modifiers[#Skin].clearZeroLimit = 0.001
)--end LODSkinSet

fn LOD2MeshMerge theFile theMesh theMaterial theName =
(
	--we'll try to delete any old meshes
	actualMesh = (getNodebyName theName)
	try
	(
		delete actualMesh
		print "Old Mesh Deleted"
	)catch(print "no old mesh to delete")

	mergedPart = mergeMaxFile theFile theMesh #deleteOldDups #useSceneMtlDups #alwaysReparent quiet:true
	mergedMesh = getNodeByName theMesh[1]

	cleanGeo mergedMesh false false
	mergedMesh.name = theName
	mergedMesh.material = theMaterial
	
	tempLOD2Array = #(mergedMesh)
	if selectionsets["*LowLOD2"] != undefined then
	(
		for o in selectionsets["*LowLOD2"] do append tempLOD2Array o
		selectionsets["*LowLOD2"] = tempLOD2Array --create a selection set of the LOD2 meshes
	)
	else
	(
		selectionsets["*LowLOD2"] = tempLOD2Array --create a selection set of the LOD2 meshes
	)

	tempSelectionSetArray=#()--a temp array for our selection set contents

	for obj in selectionsets["*LowLOD2"] do append tempSelectionSetArray obj

	modelDummy = findTheDummy()
	lod2DummyName = (modelDummy.name + "-l2")
	makeDummy [0.05, 0.05, 0.05] lod2DummyName tempSelectionSetArray
)--end LOD2MeshMerge

-------------------------------------------------------------------------------------------------------------------------
--create a camera using the predefined variables and name it
fn camCreate howFar =
(
	try
	(
		gotSpine = getCurrentSelection()
		targetPos = $SKEL_Spine3.pos
	)catch(print "Couldn't find Spine 3.  Using default lookat height.") --try and use spine 3's position as a lookat height for the camera
	
	LODCam = Targetcamera fov:camFOV nearclip:camNC farclip:camFC nearrange:camNR farrange:camFR mpassEnabled:off mpassRenderPerPass:off pos:[0, howFar, 2] isSelected:camSel target:(Targetobject transform:(matrix3 [1,0,0] [0,1,0] [0,0,1] targetPos))
	LODCam.name = "LOD_Camera"
)--end camCreate

-------------------------------------------------------------------------------------------------------------------------
fn camRefresh =
(
	hideByCategory.none()--unhide everything by category
	unhide (for obj in objects where obj.ishidden collect obj) --unhide all
	try
	(
		delete $LOD_Camera
		print "old camera deleted, creating new camera"
		camCreate camClose
	)
	catch
	(
		print "creating camera"
		camCreate camClose
	)
	viewport.setCamera LODCam --set the view to the camera
	viewport.SetRenderLevel #smoothhighlights --set the viewport to smooth + highlights

	hideByCategory.all()
	hideByCategory.geometry = false --leave only geo visible
	try (hide $mover)catch() --hide the mover
)--end camRefresh

(
	global pedLODTools
	try (cui.UnRegisterDialogBar pedLODTools)catch()
	try(destroyDialog pedLODTools)catch()
	local LastSubRollout = 1

	-------------------------------------------------------------------------------------------------------------------------
	rollout mediumLODrollout "Medium LODs"
	(
		group "Ped Type Override:"
		(
		radiobuttons radGender labels: #("Male", "Female") default:0 columns:2 tooltip:"What gender is the ped?"
		)
		button btnCreateLOD "Create LOD Meshes" width:175 height:35 tooltip:"Create the LOD meshes using the export dummy."
		button btnMergeLODHeadHands "Merge LOD Head & Hands" width:175 height:35 tooltip:"Merge base LOD head and hands."
		button btnSkinLODs "Skin LOD Meshes" width:175 height:35 tooltip:"Skin the LOD meshes off their High Detail relatives."
	-------------------------------------------------------------------------------------------------------------------------

		on mediumLODrollout open do
		(
			pedLODTools.theSubRollout.height = 545
			pedLODTools.height = 395
			
			theModelDummy = findTheDummy()
			case of--try and work out the gender
			(
				(substring (upperCase theModelDummy.name) 3 1 == "M"): radGender.state = 1
				(substring (upperCase theModelDummy.name) 3 1 == "F"): radGender.state = 2
				default: radGender.state = 0
			)
		)
		
		on btnCreateLOD pressed do
		(
			getHigh()--work out what the high meshes are
			newLODs = makeLODMeshes()--clone the high meshes and rename them as LODs
			
			theModelDummy = findTheDummy()
			LODDummyName = theModelDummy.name + "-l"
			makeDummy [0.1,0.1,0.1] LODDummyName newLODs
		)--end on btnCreateLOD pressed
		
		on btnMergeLODHeadHands pressed do
		(
			if queryBox "This will import a LODDED base head and hand object for each Head* mesh in the scene.\r\nThe merged objects will replace any existing Head* LODs.  Do you want to continue?" beep:false do
			(
				getHigh()--work out what the high meshes are
				checkLow()--check that the LODs are present and correct
				
				theModelDummy = findTheDummy()
				
				case of
				(
					(radGender.state == 1): theFile = (theProjectPedFolder + "ped_parts/LOD/Male head and hands LOD.max")
					(radGender.state == 2): theFile = (theProjectPedFolder + "ped_parts/LOD/Female head and hands LOD.max")
				)
				mergeLODFile theFile
				
				LODDummyName = theModelDummy.name + "-l"
				makeDummy [0.1,0.1,0.1] LODDummyName lowLOD

				messageBox "LOD Head and Hands Merged.\r\nPlease check the mesh names, add to *LowLOD selection\r\nset and remove duplicates if they exist."
			)
		)

		on btnSkinLODs pressed do
		(
			getHigh()--work out what the high meshes are
			checkLow()--check that the LODs are present and correct
			if lowLODStatus == true then
			(
				unskinnedMeshes = #()
				
				for isHighSkinned =1 to highLOD.count do
				(
					if highLOD[isHighSkinned].modifiers[#Skin] == undefined do
					(
						appendifunique unskinnedMeshes highLOD[isHighSkinned]
					)
				)
				
				if unskinnedMeshes.count == 0 then
				(
					for s=1 to lowLOD.count do
					(
						cleanGeo lowLOD[s] false true--reset xforms, make edit poly and vert bake to clear out any weirdness
						skinwrapLODs lowLOD[s] highLOD --skinwrap the LODs
						if skinnedState == true then
						(
							LODSkinSet lowLOD[s]--update the LOD skinning to use the LOD skeleton
						)
						else
						(
							msgText = ((lowLOD[s] as string) + " isn't skinned.\r\nThere could be a problem with the High LOD mesh.")
							messagebox msgText
						)
					)
				)
				else
				(
					insertText = ""
					for x=1 to unskinnedMeshes.count do
					(
						insertText = insertText + "\r\n" + unskinnedMeshes[x] as string
					)
					msgText = "I can't find a skin modifier on these HighLOD meshes:" + insertText
					messagebox msgText
					print "Skinning pass stopped"
				)
			)else(print "LODs not ready")
		)--end on btnSkinLODs pressed
	)--end mediumLODrollout rollout
	
	rollout lowLODrollout "Low LODs"
	(
		button btnPrepScene "Analyse Scene" width:160 height:35 tooltip:"Analyse the scene."
		group "Ped Type Override:"
		(
		radiobuttons radGender labels: #("Male", "Female") default:0 columns:2 tooltip:"What gender is the ped?"
		)
		group "LOD2 Merge"
		(
		listbox listboxMeshes "LOD Meshes:" readOnly:false height:9
		label label1 "LOD2 Progres:" align:#left
		progressbar pBarLOD2Merge color:green
		dropdownlist ddlLOD2Meshes "LOD2 Meshes To Merge:"
		button btnMergeHighlighted "Merge LOD2 Mesh" width:160 height:35 tooltip:"Merge highlighted LOD2 mesh."
		)
		button btnSkinLOD2 "Skin LOD2 Meshes" width:160 height:35 tooltip:"Skine the LOD2 meshes."

		
		fn populateDropDownList =
		(
			matchedParts = #()
			case of
			(
				(radGender.state == 1):
				(
					toFind = uppercase (substring LOD2MesheNames[listboxMeshes.selection] 1 4)
					for x=1 to maleMergeParts.count do
					(
						if uppercase (substring maleMergeParts[x] 1 4) == toFind do
						append matchedParts maleMergeParts[x]
					)
					ddlLOD2Meshes.items = matchedParts
				)
				(radGender.state == 2):
				(
					toFind = uppercase (substring LOD2MesheNames[listboxMeshes.selection] 1 4)
					for x=1 to femaleMergeParts.count do
					(
						if uppercase (substring femaleMergeParts[x] 1 4) == toFind do
						append matchedParts femaleMergeParts[x]
					)
					ddlLOD2Meshes.items = matchedParts
				)
			)
		)--end populateDropDownList
		
		on lowLODrollout open do
		(
			pedLODTools.theSubRollout.height = 530
			pedLODTools.height = 650
		)
		
		on btnPrepScene pressed do
		(
			modelDummy = findTheDummy()
			lodDummy = modelDummy.name + "-l"
			selectLODDummy = getNodeByName lodDummy
			if selectLODDummy != undefined then
			(
				hide selectionSets["*Complete"] --hides the objects in the NSS
				
				allLODMeshes=#()
				LOD2Meshes=#()

				case of--try and work out the gender
				(
					(substring (upperCase modelDummy.name) 3 1 == "M"): radGender.state = 1
					(substring (upperCase modelDummy.name) 3 1 == "F"): radGender.state = 2
					default: radGender.state = 0
				)

				collectChildren selectLODDummy &allLODMeshes includeParent:false includeHidden:true --collect all the LOD heirarchy
				
				for aMesh=1 to allLODMeshes.count do--grab only the head, uppr and lowr meshes
				(
					case of
					(
						(substring (upperCase allLODMeshes[aMesh].name) 1 4 == "HEAD"): appendIfUnique LOD2Meshes allLODMeshes[aMesh]
						(substring (upperCase allLODMeshes[aMesh].name) 1 4 == "UPPR"): appendIfUnique LOD2Meshes allLODMeshes[aMesh]
						(substring (upperCase allLODMeshes[aMesh].name) 1 4 == "LOWR"): appendIfUnique LOD2Meshes allLODMeshes[aMesh]
					)
				)

				targetCount = LOD2Meshes.count
				LOD2MesheNames = #()

				for meshName=1 to LOD2Meshes.count do
				(
					append LOD2MesheNames LOD2Meshes[meshName].name
				)

				listboxMeshes.items = LOD2MesheNames
				populateDropDownList()

				if selectionsets["*LowLOD2"] != undefined then (pBarLOD2Merge.value = ((100* selectionsets["*LowLOD2"].count)/targetCount))
				else (pBarLOD2Merge.value = 0)
			)
			else (messageBox "I couldn't find any medium LODs.\r\nPlease add medium LODs before running this script.")
		)
		
		on listboxMeshes selected selMesh do (populateDropDownList ())
		
		on btnMergeHighlighted pressed do
		(
			if ddlLOD2Meshes.items.count != 0 do
			(
				theNewName = LOD2MesheNames[listboxMeshes.selection] + "2"
				theNewMaterial = LOD2Meshes[listboxMeshes.selection].material
				theMergeMesh = #(matchedParts[ddlLOD2Meshes.selection])

				case of
				(
					(radGender.state == 1): theMergeFile = maleLOD2Parts
					(radGender.state == 2): theMergeFile = femaleLOD2Parts
				)

				LOD2MeshMerge theMergeFile theMergeMesh theNewMaterial theNewName

				pBarLOD2Merge.value = ((100* selectionsets["*LowLOD2"].count)/targetCount)
			)
		)
		
		on btnSkinLOD2 pressed do
		(
			if selectionsets["*LowLOD2"] != undefined do
			(
				tempLOD2Array = #()
				for o in selectionsets["*LowLOD2"] do append tempLOD2Array o

				unskinnedMeshes = #()
				
				for isHighSkinned =1 to LOD2Meshes.count do
				(
					if LOD2Meshes[isHighSkinned].modifiers[#Skin] == undefined do
					(
						appendifunique unskinnedMeshes LOD2Meshes[isHighSkinned]
					)
				)
				
				if unskinnedMeshes.count == 0 then
				(
					for s=1 to tempLOD2Array.count do
					(
						cleanGeo tempLOD2Array[s] true true--reset xforms, make edit poly and vert bake to clear out any weirdness
						skinwrapLODs tempLOD2Array[s] LOD2Meshes --skinwrap the LODs
						if skinnedState == true then
						(
							LODSkinSet tempLOD2Array[s]--update the LOD skinning to use the LOD skeleton
						)
						else
						(
							msgText = ((lowLOD[s] as string) + " isn't skinned.\r\nThere could be a problem with the High LOD mesh.")
							messagebox msgText
						)
					)
				)
				else
				(
					insertText = ""
					for x=1 to unskinnedMeshes.count do
					(
						insertText = insertText + "\r\n" + unskinnedMeshes[x] as string
					)
					msgText = "I can't find a skin modifier on these LOD meshes:" + insertText
					messagebox msgText
					print "Skinning pass stopped"
				)
			)
		)
	)--end lowLODrollout rollout

	rollout LODCamera "Camera SIM"
	(
		slider camDist "Camera Distance:" orient:#horizontal ticks:7 range:[camClose,camFar,camClose]
		checkbox chkAutoToggleMeshes "Auto-Toggle LOD Levels" checked:autoToggle
		button btnRefreshCam "R" width:15 height:20 offset:[75,-23] tooltip:"Refresh camera setup"
	-------------------------------------------------------------------------------------------------------------------------
		-- populate the button with default settings
		on LODCamera open do (camRefresh())

	-------------------------------------------------------------------------------------------------------------------------
		on btnRefreshCam pressed do (camRefresh())

	-------------------------------------------------------------------------------------------------------------------------
		on chkAutoToggleMeshes changed theState do
		(
			if (selectionsets["*HighLOD"] != undefined) and (selectionsets["*LowLOD"] != undefined) then
			(
				autoToggle = theState
			)
			else
			(
				messageBox "I couldn't find the *HighLOD or *LowLOD selection sets.\r\nPlease define these and try again."
				chkAutoToggleMeshes.state = false
			)
		)

	-------------------------------------------------------------------------------------------------------------------------
		on camDist changed val do 
		(
			if autoToggle == true do --if the toggle is true then we'll Automatically hide and unhide the levels of detail
			(
				case of
				(
					(val > -13):
					(
						hide selectionSets["*LowLOD"] --hides the objects in the NSS
						try (hide selectionSets["*LowLOD2"])catch() --hides the objects in the NSS)
						unhide selectionSets["*Complete"] --unhides the objects in the NSS
					)
					((val < -13) and (val > -30)):
					(

						hide selectionSets["*Complete"]
						try (hide selectionSets["*LowLOD2"])catch()
						unhide selectionSets["*LowLOD"]
					)
					(val < -30):
					(
						hide selectionSets["*Complete"]
						if selectionSets["*LowLOD2"] !=undefined then
						(
							unhide selectionSets["*LowLOD2"]
							hide selectionSets["*LowLOD"]
						)
						else
						(
							unhide selectionSets["*LowLOD"]
						)
					)
				)
			)
			LODCam.pos = [0, val ,2] --move the camera on the y axis based on the slider value
		)
	)--end LODCamera rollout

-------------------------------------------------------------------------------------------------------------------------
	pedLODTools_Rollouts = #(#("Medium LODs",#(mediumLODrollout)), #("Low LODs",#(lowLODrollout)))       
-------------------------------------------------------------------------------------------------------------------------
	rollout pedLODTools "Ped LOD Tools"
	(
		dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:pedLODTools.width
		local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Character_LOD_Tool" doOutline:false filename:(getThisScriptFilename())
		
		subRollout LODCameraRollout width:210 height:115 pos:[0, rsBannerPanel.height] align:#center
		
		dotNetControl dn_tabs "System.Windows.Forms.TabControl" height:20 width:210 align:#center
		subRollout theSubRollout width:210 height:285 align:#center
		
		on dn_tabs Selected itm do
		(
			if LastSubRollout != (itm.TabPageIndex+1) do --do not update if the same tab clicked twice
			(
				for subroll in pedLODTools_Rollouts[LastSubRollout][2] do
					removeSubRollout theSubRollout subroll
				for subroll in pedLODTools_Rollouts[LastSubRollout = itm.TabPageIndex+1][2] do     
					addSubRollout theSubRollout subroll
			) 
		)--end tabs clicked
-------------------------------------------------------------------------------------------------------------------------
		on pedLODTools open do
		(
			banner.setup()
			addSubRollout LODCameraRollout LODCamera
			for aTab in pedLODTools_Rollouts do
				(dn_tabs.TabPages.add aTab[1])
			for subroll in pedLODTools_Rollouts[1][2] do
				addSubRollout theSubRollout subroll
		)
	)
)
createDialog pedLODTools 210 400
