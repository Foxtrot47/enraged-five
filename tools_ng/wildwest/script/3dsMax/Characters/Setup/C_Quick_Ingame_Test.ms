/*
Quick Ingame Test
Stewart Wright
Rockstar North
22/03/10
A collection of scripts to help create a testable game asset.
Sets up materials, aligns props and creates basic 100% skinning
Rework 22/03/12
Streamlined and cleaned up.  Moved to new wildwest
*/-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())
	
-- Light settings are stored in a config file.  Load the vertex and light settings required for the tool
vbakesettings = pathToConfigFiles + "general/max_vertex_bake_settings.dat"
try (filein vbakesettings) catch (messagebox ("Couldn't find project specific light settings file. \r\nWas expecting " + vbakesettings ))
	
-------------------------------------------------------------------------------------------------------------------------
scenePrepared = false

stdVSkel = 1 --define the part of the multisub array ped_stdVSkelToLODSkel that we want to use
skelFile = theProjectPedFolder + "Skeletons/Male Skeleton.max"
tempVert = #()

theKitchenSink = #()
sceneComponenets = #()
sceneProps = #()

-------------------------------------------------------------------------------------------------------------------------
-- This deletes old skeleton data
fn stripToGeo = 
(
	try
	(	
		sliderTime = 0f
		select $dummy01
		for thebone in selection do
		(
			try (selectmore thebone.children) catch ()
		)	
		max delete
	) catch()

	try
	(	
		sliderTime = 0f
		selectbywildcard "SKEL"
		for thebone in selection do
		(
			try (selectmore thebone.children) catch ()
		)
		max delete
	) catch()
)

-------------------------------------------------------------------------------------------------------------------------
-- from the selected objects work out what is a prop and what is a ped component
fn pedAndProps =
(
	-- unhide all by categry
	hideByCategory.none()	
	-- unhide all
	unhide (for obj in objects where obj.ishidden collect obj)
	
	-- delete those pesky particle views
	try (delete $particle*) catch ()
	
	--hide everything that isn't geometry by category
	hideByCategory.shapes = true
	hideByCategory.lights = true
	hideByCategory.cameras = true
	hideByCategory.helpers = true
	hideByCategory.spacewarps = true
	hideByCategory.particles = true
	hideByCategory.bones = true
	
	-- select everything that's visible
	max select all
	theKitchenSink = getCurrentSelection()
	-- unselect the props
	deselect $p_*
	sceneComponenets = getCurrentSelection()	
	-- work out the props
	select $p_*
	sceneProps = getCurrentSelection()	
	
	-- reset xforms and turn on backface cull
	for resetObj = 1 to sceneComponenets.count do
	(
	$.backfacecull = on
	resetxform sceneComponenets[resetObj]
	maxOps.CollapseNodeTo sceneComponenets[resetObj] 1 True
	)

	-- unhide all by categry
	hideByCategory.none()
	-- clear selection
	clearSelection()
)

fn compareVerts mesh1 mesh2 =
(
	local count1
	local count2
	
	if (classOf mesh1.baseObject == Editable_Poly) then count1 = polyOp.getNumVerts mesh1
	else if (classOf mesh1.baseObject == Editable_Mesh) then count1 = meshOp.getNumVerts mesh1
	
	if (classOf mesh2.baseObject == Editable_Poly) then count2 = polyOp.getNumVerts mesh2
	else if (classOf mesh2.baseObject == Editable_Mesh) then count2 = meshOp.getNumVerts mesh2
	
	local d = (polyOp.getNumVerts count1) - (polyOp.getNumVerts count2)
	case of
	(
		(d < 0.): -1
		(d > 0.): 1
		default: 0
	)
)

fn sortLODs =
(
	local compTypeAr = #()
		
	for x in sceneComponenets do
	(
		local match = 0
		local type = substring x.name 1 4
		for y in compTypeAr do if (toLower type) == (toLower y) then match = 1
		if match == 0 then append compTypeAr type
	)
	for x in compTypeAr do
	(
		local selectString = "select $"+x+"*"
		execute selectString
		local count = 0
		for y in selection do
		(
			if ((substring y.name 6 3) > (count as string)) then count = ((substring y.name 6 3) as integer)
		)
		for i = 0 to count do
		(
			if count < 10 then selectString = "select $"+x+"_00"+(i as string)+"*"
			else selectString = "select $"+x+"_0"+(i as string)+"*"
			execute selectString
			
			local meshes = #()
			for x in selection do append meshes x

			qsort meshes compareVerts
			
			if (meshes[1] != undefined and meshes[2] == undefined and meshes[3] == undefined) then meshes[1].parent = $Z_PreviewPed
			else if (meshes[1] != undefined and meshes[2] != undefined and meshes[3] == undefined) then
			(
				meshes[1].parent = $'Z_PreviewPed-L'
				meshes[2].parent = $Z_PreviewPed
			)
			else if (meshes[1] != undefined and meshes[2] != undefined and meshes[3] != undefined) then
			(
				meshes[1].parent = $'Z_PreviewPed-L2'
				meshes[2].parent = $'Z_PreviewPed-L'
				meshes[3].parent = $Z_PreviewPed
			)
		)
	)	
)

-------------------------------------------------------------------------------------------------------------------------
fn makeDummy =
(
	--delete dummy if it already exists
	try (delete $Z_PreviewPed) catch ()
	try (delete $'Z_PreviewPed-L') catch ()
	try (delete $'Z_PreviewPed-L2') catch ()
	--make dummy
	Dummy pos:[0,0,0] isSelected:on
	$.name = "Z_PreviewPed"
	Dummy pos:[0,0,4] isSelected:on
	$.name = "Z_PreviewPed-L"
	$.boxSize = [1,1,1]
	Dummy pos:[0,0,4] isSelected:on
	$.name = "Z_PreviewPed-L2"
	$.boxSize = [.5,.5,.5]
	-- link stuff to it
	sortLODs()
	
	-- clear selection
	clearSelection()
) -- end makeDummy

-------------------------------------------------------------------------------------------------------------------------
-- A function here will prebake all selected model parts to a default gray
fn vertPrebake pbar:undefined =
(
	setCommandPanelTaskMode #modify
	displayColor.shaded = #object
	
	-- Try to hide ragdoll informaion
	try (hide $constraints*)catch()
	try (hide $collision*)catch()

	-- Loop through the selected objects
	toBake = theKitchenSink.count
	pbarProgress = 0
	
	for i = 1 to theKitchenSink.count do
	(
		obj = theKitchenSink[i]

		if ( undefined != pbar ) then pbar.value = pbarProgress --pbar.value = 0
		
		-- Turn on the Vertex colours, set to channel 0
		obj.showVertexColors = on
		obj.vertexColorType = 0	
		
		try-- For each object in turn, go to the base mesh  
		(
			modPanel.setCurrentObject obj.baseObject
			subobjectLevel = 1 -- Go to vertex mode
			nv=0 -- get number of verts in the object 

			if classof obj == Editable_Poly then
			(
				nv = obj.numverts
				for i = 1 to nv do
				(
					polyop.setvertcolor $ TheColourChannel i Ped_BakeColour
					polyop.setvertcolor $ TheRimChannel i Ped_RimColour
					polyop.setvertcolor $ TheWindChannel i Ped_WindColour
					polyop.setvertcolor $ TheEnvEffChannel i Ped_EnvEffColour
					polyop.setvertcolor $ TheAlphaChannel i Ped_AlphaColour
				)					
			)

			if classof obj == Editable_Mesh then
			(
				nv = obj.numverts
				for i = 1 to nv  do
				(
					meshop.setvertcolor $ TheColourChannel i Ped_BakeColour
					meshop.setvertcolor $ TheRimChannel i Ped_RimColour
					meshop.setvertcolor $ TheWindChannel i Ped_WindColour
					meshop.setvertcolor $ TheEnvEffChannel i Ped_EnvEffColour
					meshop.setvertcolor $ TheAlphaChannel i Ped_AlphaColour
				)	
			)
			
			if ( undefined != pbar ) then
			(
				pbar.value = ( 100.0 * i ) / toBake
				pbarProgress = ( 100.0 * i ) / toBake
				pbar.color = color (50 +(pbar.value * 2) ) 0 0
			)	
			subobjectLevel = 0
		)catch (print"Couldn't get to object base Epoly/EMesh")
	) -- object selection loop
	pbar.color = color 0 170 0
	clearSelection()
) -- End vertPrebake

-------------------------------------------------------------------------------------------------------------------------
-- Merge the guides and supress all confirmations
fn mergeSkel =
(
	mergeMaxFile skelFile #deleteOldDups #useSceneMtlDups #alwaysReparent quiet:true
	-- clear selection
	clearSelection()
) --end mergeSkel

-------------------------------------------------------------------------------------------------------------------------
fn skinEmUp pbar:undefined =
(	
	-- Put the objects into an array
	objCount = sceneComponenets.count
	
	-- for each selected object add a skin modifier, add the bones and weight the verts 100% to 1 bone
	for i = 1 to objCount do 
	(
		obj = sceneComponenets[i] -- pick an object out of the array.
		select obj
		
		--This was added to try and stop the fucking stupid character jumping about shit
		if obj.modifiers[#Skin] != undefined do
		(
			deleteModifier obj 1
			modPanel.addModToSelection (XForm ()) ui:on
			collapseStack obj
		)
		
 		-- This part adds a skin modifier
		if obj.modifiers[#Skin] == undefined do
		(
			modPanel.addModToSelection (XForm ()) ui:on
			collapseStack obj
			modPanel.addModToSelection (Skin ()) ui:on
		)
		
		-- Setup some skin pref stuff
		obj.modifiers[#Skin].bone_Limit = 4
		obj.modifiers[#Skin].showNoEnvelopes = on
		obj.modifiers[#Skin].clearZeroLimit = 0
		
		-- Use the bone ordering that we loaded in
		-- Add each bone to the skin modifier, using try/catch each time since not all bones will be used 
		for bc = 1 to ped_stdVSkelToLODSkel.count do
		(
			try (skinOps.addBone obj.modifiers[#Skin] (getnodebyname ped_stdVSkelToLODSkel[bc][stdVSkel]) 1) catch()
		)
		
		if ( undefined != pbar ) then
		(
			pbar.value = ( 50.0 * i ) / objCount
			pbar.color = color (50 +(pbar.value * 2) ) 0 0
		)
	) -- object selection loop
	pbar.color = color 175 105 0
	-- clear selection
	clearSelection()
) -- End skinEmUp

-------------------------------------------------------------------------------------------------------------------------
fn weighEmDown pbar:undefined =
(
	--objSelectedArray = sceneComponenets
	objCount = sceneComponenets.count
	
	for i = 1 to objCount do
	(
		
		obj = sceneComponenets[i] -- pick an object out of the array.
		select obj
		
		-- work out the vert count and skin them all to 1 bone.
		noVerts = skinops.getNumberVertices obj.modifiers[#Skin]
		tempVert = #() --empty this before we fill it up
		for nv = 1 to noVerts do
		(
			append tempVert nv
		)
		
		skinOps.SelectBone obj.modifiers[#Skin] 2 --select the bone we want to skin to
		skinOps.SelectVertices obj.modifiers[#Skin] tempVert -- select all the vertices
		skinOps.setweight obj.modifiers[#Skin] 1 --skin all vert 100% to the selected bone
		skinOps.RemoveZeroWeights obj.modifiers[#Skin] --keep it clean and clear zero weights
		
		if ( undefined != pbar ) then
		(
			pbar.value = 55
			pbar.value = ((((100.0 * i ) / objCount)/2) +50)
			pbar.color = color (50 +(pbar.value * 2) ) (30 +(pbar.value * 2) ) 0
		)		
	)
	pbar.color = color 0 170 0
	-- clear selection
	clearSelection()
) -- End weighEmDown

-- align and link props so they work in the render pose
fn sortTheProps =
(
	print "Sorting Props..."
	if sceneProps.count != 0 then
	(
		AlignProps ()
		unlinkProps ()
		LinkProps ()
		print "...Props Linked and Aligned"
	)
	else(print "...No Props in scene")
)-- end sortTheProps

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
rollout QuickSkinTool "Quick and Dirty Skin Tool"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Character_Quick_Skin_Tool" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)
	group "I'm feeling lucky"
	(
		button btnImFeelingLucky "Cross Your Fingers" width:120 height:60 tooltip:"Try and prepare the scene, apply vertex bake and skin meshes."
	)
	group "Setup"
	(
		button btnPrepScene "Prepare the Scene" width:120 height:30 tooltip:"Prepare the scene."
		progressbar pbPrepDone
		button btnVertBake "Prebake Vert Colours" width:120 height:30 tooltip:"Apply a nice even wash of colour to the model."
		progressbar pbVertProgress
		button btnSkinMesh "Skin Pack" width:120 height:30 tooltip:"Skin every selected object 100% to the pelvis."
		progressbar pbSkinProgress
		button btnMaterialTools "Setup Materials" width:120 height:30 tooltip:"Open the material setup tools."
	)
	group "Export"
	(
		button btnLaunchExporter "Open Ped Exporter" width:120 height:30 tooltip:"Open the Ped Exporter."
	)
-------------------------------------------------------------------------------------------------------------------------
	on QuickSkinTool open do
	(
		WWP4vSync pedSkeletonFiles
	)
		
	on btnImFeelingLucky pressed do
	(
		--prep scene
		stripToGeo()
		pedAndProps ()
		makeDummy ()
		pbPrepDone.value = 100
		pbPrepDone.color = color 0 170 0
		--vert bake
		pbVertProgress.value = 0
		pbVertProgress.color = color 170 0 0
		vertPreBake pbar:pbVertProgress ()
		-- merge the skeleton
		mergeSkel ()
		pbSkinProgress.value = 0
		pbSkinProgress.color = color 0 170 0
		-- skin the meshes
		pbSkinProgress	
		pbSkinProgress.value = 10
		pbSkinProgress.color = color 170 0 0
		skinEmUp pbar:pbSkinProgress ()
		pbSkinProgress	
		pbSkinProgress.value = 55
		pbSkinProgress.color = color 170 0 0
		weighEmDown pbar:pbSkinProgress ()
		-- align and link props
		sortTheProps()
	)
-------------------------------------------------------------------------------------------------------------------------
	on btnPrepScene pressed do
	(
		stripToGeo()
		pedAndProps ()
		makeDummy ()
		pbPrepDone.value = 100
		pbPrepDone.color = color 0 170 0
		
		scenePrepared = true
	)
-------------------------------------------------------------------------------------------------------------------------	
	on btnVertBake pressed do
	(
		if scenePrepared == true then
		(
			pbVertProgress.value = 0
			pbVertProgress.color = color 170 0 0
			vertPreBake pbar:pbVertProgress ()
		)
		else
		(
			msgPrepareScene = "Please run Prep the Scene first"
			messagebox msgPrepareScene
		)
	)
-------------------------------------------------------------------------------------------------------------------------
	on btnSkinMesh pressed do
	(
		if scenePrepared == true then
		(
			-- merge the skeleton
			mergeSkel ()
			pbSkinProgress.value = 0
			pbSkinProgress.color = color 0 170 0
			-- skin the meshes
			pbSkinProgress	
			pbSkinProgress.value = 10
			pbSkinProgress.color = color 170 0 0
			skinEmUp pbar:pbSkinProgress ()
			pbSkinProgress	
			pbSkinProgress.value = 55
			pbSkinProgress.color = color 170 0 0
			weighEmDown pbar:pbSkinProgress ()
			-- align and link props
			sortTheProps()
		)
		else
		(
			msgPrepareScene = "Please run Prep the Scene first"
			messagebox msgPrepareScene
		)
	)
-------------------------------------------------------------------------------------------------------------------------
	on btnMaterialTools pressed do
	(
		filein (theWildWest + "script/3dsMax/Characters/Materials/Ped_Shader_Setup.ms")
	)
-------------------------------------------------------------------------------------------------------------------------
	on btnLaunchExporter pressed do
	(
		filein "pipeline/export/ui/ped_ui.ms"
	)
)

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
try(closeRolloutFloater QuickInGameTestFloater)catch() -- Destroy the UI if it exists 
QuickInGameTestFloater = newRolloutFloater "Quick Setup for In-Game Test" 160 430
addRollout QuickSkinTool QuickInGameTestFloater rolledUp:false