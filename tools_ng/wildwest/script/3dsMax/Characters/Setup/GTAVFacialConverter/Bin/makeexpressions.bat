@echo off

CALL %RS_TOOLSROOT%\bin\setenv.bat >NUL

SET "CDIR=%~dp1"
SET "CDIR=%CDIR:~0,-1%"
FOR %%i IN ("%CDIR%") DO SET "PARENT_DIR=%%~nxi"
set XML_FILE="%~dp1%~n1.xml"
set HASH_FILE=%~dp1%~n1.expr.hashnames"
set EXPR_FILE="%~dp1%~n1.expr"

if "%~x1" == ".xml" (

	REM Generate expression file
	p4 add -t binary %EXPR_FILE%
	p4 add -t text %HASH_FILE%
	p4 edit -t binary %EXPR_FILE%
	p4 edit -t text %HASH_FILE%
	%RS_TOOLSROOT%/wildwest/script/3dsMax/Characters/Setup/GTAVFacialConverter/Bin/makeexpressions.exe -exprdata %XML_FILE% -out %EXPR_FILE%
)

REM PAUSE