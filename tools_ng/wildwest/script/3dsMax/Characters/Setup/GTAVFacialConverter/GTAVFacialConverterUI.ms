FileIn (RsConfigGetToolsDir() + "dcc/current/max2012/scripts/pipeline/util/RSProcess.ms")
FileIn (RsConfigGetWildwestDir() + "script/3dsMax/_common_functions/FN_RSTA_xml.ms")
FileIn (RsConfigGetWildwestDir() + "script/3dsMax/Characters/Setup/GTAVFacialConverter/GTAVSkeletonExporter.ms")
FileIn (RsConfigGetWildwestDir() + "script/3dsMax/Characters/Setup/GTAVFacialConverter/GTAVFacialConverter.ms")
FileIn (RsConfigGetWildwestDir() + "script/3dsMax/Characters/Setup/GTAVFacialConverter/ExpressionFinder.ms")


try( DestroyDialog rGTAVFacialConverter )catch()

rollout rGTAVFacialConverter "GTAV Facial Converter"
(
	---------------------------------------------------------------------------
	-- LOCALS
	---------------------------------------------------------------------------
	local btnWidth = 186
		--	hard code GTA expression path since this will be run from other projects for now
	local exprPath = (RsConfigGetAssetsDir core:true) + "anim/expressions/"
	local converter = undefined
	local exprFinder = undefined
	
	---------------------------------------------------------------------------
	-- UI
	---------------------------------------------------------------------------
	editText txt_pedName "Ped Name"
	listbox lbx_facialExprs "Facial Expressions"
	button btn_addExpr "Add Expression XML" align:#left across:3
	button btn_removeExpr "Remove Expression XML" align:#right
	button btn_findExpr "Auto-Find Expression" align:#right
	
	button btn_convert "CONVERT" width:375 height:30

	---------------------------------------------------------------------------
	-- FUNCTIONS 
	---------------------------------------------------------------------------
	fn GetDefaultNameFromMaxFile =
	(
		local fName = GetFileNameFile MaxFileName
		
		if fName != undefined then
		(
			txt_pedName.text = fName
			converter.pedName = fName
		)
	)
	
	fn GetDefaultExpressionsFromName converter =
	(
		converter.GetPedExpressions()
		lbx_facialExprs.items = converter.expressionList
	)
	
	---------------------------------------------------------------------------
	-- EVENTS
	---------------------------------------------------------------------------
	on rGTAVFacialConverter Open do
	(
		gRsPerforce.Sync @"x:\gta5\art\animation\resources\characters\solver_assets\..."
		
		converter = sGTAVFacialConverter()
		converter.Init()
		
		GetDefaultNameFromMaxFile()
		GetDefaultExpressionsFromName converter

		-- Auto-Expression Finder (Enable to autorun on load)
		exprFinder = RstExpressionFinder()
		exprFinder.run()
		print 11111
		if (exprFinder.expressionToUse != undefined) then
		(
			print 2222
			lbx_facialExprs.items = #(exprFinder.expressionToUse)
			print ("exprFinder.expressionToUse:" + exprFinder.expressionToUse as string)
			print ("lbx_facialExprs:" + lbx_facialExprs as string)
		)
		else messageBox "Unable to auto-find a valid expression."
	)
	
	on btn_addExpr pressed do
	(
		local exprXML = GetOpenFileName caption:"Select Expression XML" filename:exprPath types:"XML(*.xml)|*.xml"
		
		if( exprXML != undefined )then
		(
			AppendIfUnique converter.expressionList (RsMakeSafeSlashes exprXML)
			lbx_facialExprs.items = converter.expressionList
		)
	)
	
	on btn_removeExpr pressed do
	(
		if( lbx_facialExprs.selection != 0 ) then
		(
			DeleteItem converter.expressionList lbx_facialExprs.selection
			lbx_facialExprs.items = converter.expressionList
		)
	)

	on btn_findExpr pressed do
	(
		exprFinder = RstExpressionFinder()
		if (selection.count != 1) do 
		(
			messageBox "Please select a single target ped (dummy)."
			return false
		)

		exprFinder.run triggeredByUser:true
		if (exprFinder.expressionToUse != undefined) then
		(
			lbx_facialExprs.items = #(exprFinder.expressionToUse)
		)
		else messageBox "Unable to find a valid expression."
	)
	
	on btn_convert pressed do
	(
		converter.pedName = txt_pedName.text
		
		if( converter.pedName != undefined and converter.pedName != "" )then
		(
			converter.ValidateSavePath()
			
			converter.ExportSkeleton()
			
			if( converter.expressionList.count > 0 )then
			(
				converter.ProcessExpressions()
			)
		)
	)
)
CreateDialog rGTAVFacialConverter 400 245