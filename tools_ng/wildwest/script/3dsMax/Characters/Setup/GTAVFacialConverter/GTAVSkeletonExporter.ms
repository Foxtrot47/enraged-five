struct sGTAVSkeletonExporter
(
	---------------------------------------------------------------------------
	-- LOCALS
	---------------------------------------------------------------------------
	xml = undefined,
	pedName = undefined,
	outputPath = undefined,
	
	boneGuids = undefined,
	guidClass = DotNetClass "System.Guid",
	gtaBonelist = (RsConfigGetProjRootDir core:true) + "art/animation/resources/characters/solver_assets/masterlists/gtav.bonelist",
	
	
	---------------------------------------------------------------------------
	-- FUNCTIONS 
	---------------------------------------------------------------------------
	
	---------------------------------------------------------------------------
	--	build a table of all current bones and their guids from the bonelist
	fn ReadBonelist =
	(
		this.boneGuids = DotNetObject "System.Collections.HashTable"
		local bonelist = RSTA_XML_IO xmlFile:this.gtaBonelist
		
		local boneIter = (bonelist.root.SelectNodes "Bones/Item").GetEnumerator()
		while boneIter.MoveNext() do
		(
			this.boneGuids.Add (ToLower ((boneIter.current.SelectSingleNode "Name").innerText)) (boneIter.current.SelectSingleNode "Guid").innerText
		)
		
		bonelist.Dispose()
	),
	---------------------------------------------------------------------------
	--	add a new bone to the bonelist
	fn AddToBonelist b guid =
	(
		gRsPerforce.Edit this.gtaBonelist
		
		local bonelist = RSTA_XML_IO xmlFile:this.gtaBonelist
		
		local nextBone = undefined
		
		local boneIter = (bonelist.root.SelectNodes "Bones/Item").GetEnumerator()
		while boneIter.MoveNext() do
		(
			if( nextBone == undefined )then
			(
				local bName = (boneIter.current.SelectSingleNode "Name").innertext
				
				if( bName != "SKEL_ROOT" )then
				(
					if( bName > b.name )then nextBone = boneIter.current
				)
			)
		)
		
		local bonesNode = bonelist.root.SelectSingleNode "Bones"
		
		local iNode = RSTA_XML.MakeNode "Item" bonesNode
		RSTA_XML.MakeNode "Name" iNode innertext:b.name
		RSTA_XML.MakeNode "Alias" iNode
		RSTA_XML.MakeNode "Guid" iNode innertext:guid
		local tracksNode = RSTA_XML.MakeNode "Tracks" iNode
		RSTA_XML.MakeNode "Item" tracksNode innertext:"rotX"
		RSTA_XML.MakeNode "Item" tracksNode innertext:"rotY"
		RSTA_XML.MakeNode "Item" tracksNode innertext:"rotZ"
		
		
		if( nextBone != undefined )then
		(
			bonesNode.InsertBefore iNode nextBone
		)
		
		bonelist.Save saveWarning:false
		bonelist.Dispose()
	),
	---------------------------------------------------------------------------
	--	write the root level nodes that will be constant in all skels
	fn WriteRootNodes =
	(
		RSTA_XML.MakeNode "AuthoredOrientation" this.xml.root attr:"value" value:"true"
		RSTA_XML.MakeNode "UnitTypeLinearConversionFactor" this.xml.root attr:"value" value:"1.000000"
		RSTA_XML.MakeNode "IgnoreSkeletonRootXform" this.xml.root attr:"value" value:"false"
		RSTA_XML.MakeNode "ExportEulers" this.xml.root attr:"value" value:"false"
		RSTA_XML.MakeNode "ExportQuats" this.xml.root attr:"value" value:"true"
		RSTA_XML.MakeNode "Name" this.xml.root innerText:this.pedName
	),
	---------------------------------------------------------------------------
	--	write out the skeleton one bone at a time
	--	some nodes will be constant in all skels; mostly just the name, guid and transform that change
	fn WriteBoneNode b pNode &count =
	(
		if( ClassOf b != Col_Capsule )then
		(
			--	increase counter used to fill in the NumberOfBones node afterwards
			count += 1
			
			local iNode = RSTA_XML.MakeNode "Item" pNode attr:"type" value:"rage__BoneInstance"
			
			local guid = this.boneGuids.item[(ToLower b.name)]
			if( guid == undefined )then
			(
				guid = (this.guidClass.NewGuid()).ToString()
				this.AddToBonelist b guid
			)
			
			RSTA_XML.MakeNode "Name" iNode innertext:b.name
			RSTA_XML.MakeNode "Guid" iNode innertext:guid
			
			RSTA_XML.MakeNode "AttachedGuid" iNode
			
			local transNode = RSTA_XML.MakeNode "Transform" iNode attr:"content" value:"matrix34"
			local bTrans = (b.transform * Inverse b.parent.transform)
			
			local xFormData = #(
				"\n" + (FormattedPrint bTrans.row1.x format:"#.6f") + "\t" + (FormattedPrint bTrans.row2.x format:"#.6f") + "\t" + (FormattedPrint bTrans.row3.x format:"#.6f") + "\t" + (FormattedPrint bTrans.row4.x format:"#.6f"),
				"\n" + (FormattedPrint bTrans.row1.y format:"#.6f") + "\t" + (FormattedPrint bTrans.row2.y format:"#.6f") + "\t" + (FormattedPrint bTrans.row3.y format:"#.6f") + "\t" + (FormattedPrint bTrans.row4.y format:"#.6f"),
				"\n" + (FormattedPrint bTrans.row1.z format:"#.6f") + "\t" + (FormattedPrint bTrans.row2.z format:"#.6f") + "\t" + (FormattedPrint bTrans.row3.z format:"#.6f") + "\t" + (FormattedPrint bTrans.row4.z format:"#.6f") + "\n"
			)
			for i = 1 to 3 do
			(
				local textNode = this.xml.xDoc.CreateTextNode xFormData[i]
				transNode.AppendChild textNode
			)
			
			RSTA_XML.MakeNode "Tracks" iNode
			RSTA_XML.MakeNode "IsJoint" iNode attr:"value" value:"true"
			RSTA_XML.MakeNode "Split" iNode attr:"value" value:"false"
			RSTA_XML.MakeNode "Lod1" iNode attr:"value" value:"true"
			RSTA_XML.MakeNode "Lod2" iNode attr:"value" value:"true"
			RSTA_XML.MakeNode "Lod3" iNode attr:"value" value:"true"
			RSTA_XML.MakeNode "PackComponents" iNode
			
			local childRoot = RSTA_XML.MakeNode "Children" iNode
			for c in b.children do WriteBoneNode c childRoot &count
		)
	),
	---------------------------------------------------------------------------
	--	set up the root level data and then recurse the skeleton to the write the bone information
	fn WriteSkeleton =
	(
		local countNode = RSTA_XML.MakeNode "NumberOfBones" this.xml.root attr:"value" value:"0"
		this.WriteRootNodes()
		
		local skelRoot = $SKEL_ROOT
		local bCount = 0
		
		if( skelRoot != undefined )then
		(
			local rootsNode = RSTA_XML.MakeNode "Roots" this.xml.root
			this.WriteBoneNode skelRoot rootsNode &bCount
		)
		
		countNode.SetAttribute "value" (bCount as string)
		
		if( DoesFileExist this.outputPath )then
		(
			gRsPerforce.Edit this.outputPath
			this.xml.SaveAs this.outputPath saveWarning:false
		)
		else
		(
			this.xml.SaveAs this.outputPath saveWarning:false
			gRsPerforce.Add_Or_Edit this.outputPath
		)
	),
	---------------------------------------------------------------------------
	--	write a skelset to use in the solver
	fn WriteSkelset assetPath outPath =
	(
		local xml = RSTA_XML_IO()
		local skelset = (outPath + "skeleton/" + this.PedName + ".skelset")
		
		this.outputPath = (outPath + "skeleton/" + this.pedName + ".skel.meta")
		
		xml.New "rage__SkeletonSet"
		
		RSTA_XML.MakeNode "MainSkeleton" xml.root innerText:this.outputPath
		RSTA_XML.MakeNode "Skeletons" xml.root
		RSTA_XML.MakeNode "MasterBoneList" xml.root innerText:(assetPath + "masterlists/gtav.bonelist")
		
		if( DoesFileExist skelset )then
		(
			gRsPerforce.Edit skelset
			xml.SaveAs skelset saveWarning:false
		)
		else
		(
			xml.SaveAs skelset saveWarning:false
			gRsPerforce.Add_Or_Edit skelset
		)
	),
	---------------------------------------------------------------------------
	--	initialize a new xml to write the skeleton into
	--	collect the current bonelist bones and guids
	fn Init =
	(
		this.xml = RSTA_xml_IO()
		this.xml.New "rage__Skeleton"
		
		this.ReadBonelist()
	)
)