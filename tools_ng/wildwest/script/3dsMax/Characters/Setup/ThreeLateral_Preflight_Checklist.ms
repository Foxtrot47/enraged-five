-- 3Lateral Checklist script
-- A script to automate the 3 Lateral checklist
-- Rick Stirling
-- Rockstar North
-- December 2011



-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())


-- Take an object and apply/remove an stl check
fn stlChecker meshObj stlSetValue =
(
	select meshObj
	
	-- If true, then add and activate the STL check
	if stlSetValue == true then
	(	
		max modify mode
		addmodifier meshObj (STL_Check ()) ui:on
		$.modifiers[#STL_Check].Check_Now = 1
		--actionMan.executeAction 0 "281"  -- Tools: Hide Unselected
		--max tool zoomextents
	)
	
	-- If false then remove the STL check
	if stlSetValue == false then
	(	
		modPanel.setCurrentObject $.modifiers[#STL_Check]
		STLmod = $.modifiers[#STL_Check]
		if STLmod != undefined then deleteModifier $ STLmod
	)	
	
	
)	



-- We have 2 required named components for rigging.
fn CheckComponentNames = 
(
	allgood = true
	clearSelection()
	selectByWildcard "head_000"
	curSel = getCurrentSelection()
	
	if curSel.count < 1 then 
	(
		messagebox "Could not find an object named as a head"
		allgood = false
	)

	else 
	(
		splitname = filterstring $.name " "
		if splitname[1].count != 10 then 
		(
			messagebox "Could not find an object named head_000_R - probably almost correct though"
			allgood = false
		)	
	)
	
	clearSelection()
	selectByWildcard "teef_000"
	curSel = getCurrentSelection()
	
	if curSel.count < 1 then 
	(
		messagebox "Could not find an object named as teef"
		allgood = false
	)

	else 
	(
		splitname = filterstring $.name " "
		if splitname[1].count != 10 then 
		(
			messagebox "Could not find an object named teef_000_R - probably almost correct though"
			allgood = false
		)	
	)	
	
	
	clearSelection()
	
	if allgood == true then messagebox "Components seem good"
)




-------------------------------------------------------------------------------------------------------------------------
rollout Preflight "3Lateral Checklist Tool" width:200 height:350
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/3LateralChecklist" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	
	button btnCheckComponentNames "Check Component Names" width:170 height:25
	button btnCheckTeef "Check Teef" width:170 height:25
	button btnSTLCheckOn "Add STL Checks" width:170 height:25
	button btnSTLCheckOff "Remove STL Checks" width:170 height:25
	
	button btntidyPhysics "Remove Collision Constraints" width:170 height:25
	
	button btnCheckForSkeleton "Check For Skeleton" width:170 height:25
	button btnCheckForSkin "Check For Skin" width:170 height:25
	button btnHideNonMesh "Hide Bones/Helpers" width:170 height:25
	
	button btnFinalViews "Final Views" width:170 height:25
	
	button btnExportHeadOBJ "Export Head OBJ" width:170 height:25
	
	
	on Preflight open do
	(
		sliderTime = 0f
	)	
	
	
	


	on btnCheckComponentNames pressed do CheckComponentNames()
	
	on btnSTLCheckOn pressed do
	(
		selectByWildcard "head_000_R"
		headmesh = getcurrentSelection()
			
		selectByWildcard "teef_000_U"
		teethmesh = getcurrentSelection()
		
		stlChecker headmesh true
		stlChecker teethmesh true
		
		select headmesh
		selectmore teethmesh
	)


		
	
	on btnCheckTeef pressed do
	(
		DefaultTeefValue = 1000
		
     try 
		 (
		selectByWildcard "teef_000_U"
		teethmesh = getcurrentSelection()
		teethmesh = teethmesh[1]
		theVertCount = getNumVerts teethmesh 
		 
		 if theVertCount > DefaultTeefValue do
			(
				messageBox "Teef ok" 
			)
			
			 if theVertCount < DefaultTeefValue do
			(
				messageBox "You should check the teef" 
			)
		
		 )
		  
		  
	 catch (messageBox "no teef object")

	)
	

	on btnSTLCheckOff pressed do
	(
		selectByWildcard "head_000_R"
		headmesh = getcurrentSelection()
			
		selectByWildcard "teef_000_U"
		teethmesh = getcurrentSelection()
		
		stlChecker headmesh false
		stlChecker teethmesh false
	)	
	
	
	on btntidyPhysics pressed do
	(
		try 
		(
			clearSelection()
			select $collis*
			max delete
		) 
		catch ()	
		
		try 
		(
			clearSelection()
			select $constraint*
			max delete
		) 
		catch ()			
		
	)	
	
	
	
	on btnCheckForSkeleton pressed do 
	(
		clearSelection()
		select ped_rootbone
		for thebone in selection do
		(
			try (selectmore thebone.children) catch ()
		)	
	
		selectedbones = $
		if selectedbones.count < 70 then 
		(messagebox ("Skeleton seems wrong, found " + (selectedbones.count as string)+ ", expected at least 70"))	
		else
		(messagebox "Skeleton seems to be OK")	
	)
	
	
	on btnCheckForSkin pressed do 
	(
		selectByWildcard "uppr_000_"
		uppermesh = getcurrentSelection()
		skinmod = $.modifiers[#Skin]
		if skinmod == undefined then 
		(messagebox "Couldn't find a skin on the upper, assuming no skinning in scene")
		else
		(messagebox "Found a skin on the upper, assuming there is skinning in scene - but double check")
	)
	
	on btnHideNonMesh pressed do 
	(
		hideByCategory.bones = true
		hideByCategory.helpers = true
		try
		(
			select $mover
			max hide selection
		)
		catch()		
	)
	
	
	on btnFinalViews pressed do
	(
		max vpt front
		max tool zoomextents
	)	
	
	on btnExportHeadOBJ pressed do
	(
		clearSelection()
		selectByWildcard "head_000"
		OBJOutputPath = "c:/temp/headOBJs"
		makeDir (OBJOutputPath)
		OBJOutputName = OBJOutputPath + "/" + (getFilenameFile maxFileName) 
		
		
		exportClasses =exporterPlugin.classes
		--find the obj index
		objExpIndex = undefined
		for exportclassIndex = 1 to exportClasses.count do
		(
			ec = exportClasses[exportclassIndex]
			if (uppercase (substring (ec as string) 1 3 )) == "OBJ" then objExpIndex = exportclassIndex
		)	
			
		-- Export the the obj
		exportFile (OBJOutputName) using:exportClasses[objExpIndex] selectedOnly:true

	)		
		
)



createDialog Preflight

