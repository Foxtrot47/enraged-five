/*
Weld Without Quantise Setup
Stewart Wright
Rockstar North
18/4/12
Set or remove Weld Withou Quantise attribute from meshes.
*/-------------------------------------------------------------------------------------------------------------------------


-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())

-------------------------------------------------------------------------------------------------------------------------
quantiseAttr = "Weld Without Quantise"
defaultQuantiseMeshes = #("head_00", "teef_00")
msgNoMeshes = "I couldn't find any meshes."
-------------------------------------------------------------------------------------------------------------------------
--a function to "weld without quantise" on selected or defined meshes.  if selectionFlag is true we will apply default values to the default meshes.  attValue defines the state we want to set.
fn setQuantise selectionFlag attValue=
(
	if selectionFlag == true then --we want to apply "weld without quantise" to head and teef meshes so lets find if they are in the scene
	(
		selectMeshes2Quantise = #()
		
		for meshes2Quantise=1 to defaultQuantiseMeshes.count do --for each mesh defined in the defaultQuantiseMeshes array we will try and select it.  we'll add it to our selectMeshes2Quantise array if found
		(
			selectByWildcard defaultQuantiseMeshes[meshes2Quantise]
			tempSelection = getCurrentSelection()
			appendIfUnique selectMeshes2Quantise tempSelection[1]
		)
		
	)
	else --we want to apply "weld without quantise" to all selected meshes
	(
		selectMeshes2Quantise = getCurrentSelection()
	)

	if selectMeshes2Quantise.count > 0 then
	(
		for quantiseThis=1 to selectMeshes2Quantise.count do
		(
			setAttribute selectMeshes2Quantise[quantiseThis] quantiseAttr attValue
		)
	)
	
	else
	(
		messagebox msgNoMeshes
	)
)--end setQuantise
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

rollout quantiseSetup "Setup Quantise Weld"
(
	group "Set Weld Without Quantise"
	(
	button btnAutoSetup "Apply to default meshes" width:160 height:35 tooltip:"Setup default meshes using default values."
	button btnSetSelected "Apply to selected meshes" width:160 height:35 tooltip:"Set Weld Without Quantise from selected meshes."
	)
	group "Remove Weld Without Quantise"
	(
	button btnRemoveSelected "Apply to selected meshes" width:160 height:35 tooltip:"Remove Weld Without Quantise from selected meshes."
	)

	on btnAutoSetup pressed do
	(
		setQuantise true true
	)
	
	on btnSetSelected pressed do
	(
		setQuantise false true
	)
	on btnRemoveSelected pressed do
	(
		setQuantise false false
	)
)--end quantiseSetup
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

try(closeRolloutFloater quantiseSetupFloater)catch() -- Destroy the UI if it exists 
quantiseSetupFloater = newRolloutFloater "Setup Quantise Weld" 200 210
addRollout quantiseSetup quantiseSetupFloater rolledUp:false