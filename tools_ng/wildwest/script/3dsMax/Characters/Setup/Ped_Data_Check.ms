-- Export tester script
-- Rick Stirling, created "A long time ago in a galaxy far, far away...."
-- February 2012, Dug up, cleaned up, converted to new Wildwest
-- Checks for various things before skinning and before Export



filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
--RsCollectToolUsageData (getThisScriptFilename())
	
	




fn reportBadMesh theMeshName theErrorMessage =
(
	messagebox theErrorMessage
	select (getNodeByName theMeshName)
)	



fn collectMeshes ExportDummy =
(
	numkids = ExportDummy.children.count
	meshSelectionSet=#()

	for dummyChild = 1 to numkids do
	(
		append meshSelectionSet ExportDummy.children[dummyChild].name
	)
	
	meshSelectionSet -- return the mesh collection
)	

	

fn findPedExportDummy =
(
	-- Is the dummy already selected
	selectedobject = $
	
	-- Is a dummy selected?
	if classof selectedobject == Dummy then
	(
		--Assume that the user has selected the Dummy
		selectedobject = $
	)
	
	
	-- A dummy is not selected 
	else
	(
		-- Try to select the dummy based on the ped name
		clearSelection()
		
		--whats the name of the character?
		thename = maxfilename as string
		shortname = filterstring thename "."
		thedummyname = uppercase shortname[1]
		
		for obj in helpers do 
		(
			if uppercase (obj.name) == thedummyname then 
			(
				selectedobject = obj
			)	
		)
	)

	if selectedobject == undefined then messageBox "Could not find the export dummy automatically - please select it and try again"
		
	selectedobject -- Return the dummy name (or undefined)
)



-- check the names of all the components
fn check_prop_names =
(
	-- Select the dummy
	ExportDummy = findPedExportDummy()
	
	if ExportDummy != undefined then 
	(	
		-- first get a list of all the names (children of the dummy)
		meshSelectionSet = collectMeshes ExportDummy
		
		-- Loop through the meshes finding the props	
		for childindex = 1 to ExportDummy.children.count do
		(
			theMeshName = uppercase (meshSelectionSet[childindex])
			propSearch1 = uppercase (substring theMeshName 1 1)
			propSearch2 = uppercase (substring theMeshName 2 1)
			
			-- Name begins with P
			if propSearch1 != "P" then
			(
				-- this is not a prop, so we do nothing at this point. Might do something here later
			)	
			
			else if ((propSearch1 == "P") and (propSearch2 != "_")) then
			(
				-- Seems to be a prop, but not named P_
				theErrorMessage = ("The mesh name " + theMeshName  + " begins with P, but not P_ - it might be a misnamed prop. The mesh will now be selected and the script will stop.") as string
				reportBadMesh theMeshName theErrorMessage
				return false
			)	
			
			-- Mesh begins with P_
			else
			(
				-- This is a prop, so start the checks
				-- Is the name too long
				try 
				(
					-- Names are only valid up to the first space, after that it's just a description, so trim to the first space.
					trimmedMeshName = (filterstring theMeshName " ")
					trimmedMeshName = trimmedMeshName[1]
					toolong = substring trimmedMeshName 11 1
					
					if (toolong != "") then
					(
						theErrorMessage = ("The prop name " + theMeshName  + " is too long (before any spaces). The prop will now be selected and the script will stop.") as string
						reportBadMesh theMeshName theErrorMessage
						return false
					)
				) 	catch ()
				
				
				
				-- Now make sure that the component name is valid 
				--ped_propTypes comes from a project specific config file to make this script easy to port to other projects
				propType = substring theMeshName 3 4
			
				matchedNameFlag = false
				
				-- Check the name of the component against the list of correct names.
				-- Loop through all the valid component types looking for a match
				for nc = 1 to ped_propTypes.count do
				(
						validname = substring (ped_propTypes[nc]) 1 4
						if (propType == validname) then matchedNameFlag = true
				)	
				
				-- If not match was found, then the current component must not be valid.
				if matchedNameFlag == false then
				(	
					theErrorMessage = "The prop name " + theMeshName  + " isn't a valid prop name - the type " + propType + " wasn't recognised. The prop will now be selected and the script will stop." as string
					reportBadMesh theMeshName theErrorMessage
					return false
				)	
				
				
				
				-- The first part is correct, check the underbars are there
				theunderbar1 = substring theMeshName 2 1
				theunderbar2 = substring theMeshName 7 1
				
				if ((theunderbar1 == "_") AND (theunderbar2 == "_")) then
				(
					print "The underbar at position 2 and 7 is good"
				)
				
				else 
				(
					theErrorMessage = "Check the position of the underbars in the of " + theMeshName + " which should be the 2th and 7th character. The prop will now be selected and the script will stop." as string
					reportBadMesh theMeshName theErrorMessage
					return false 
				)
				
				-- now check the 3 digits by trying to convert to an integer. If it break, then the name is bad.
				thedigits = substring theMeshName 8 3
				variationCheck = thedigits as integer

				if variationCheck == undefined then
				(
					theErrorMessage = "Check the 3 numeric digits of " + theMeshName + ". The component will now be selected and the script will stop." as string
					reportBadMesh theMeshName theErrorMessage
					return false 
				)
				
			) -- End if this is a prop
		) -- End mesh loop 
		
		messageBox "The props seem to be good"
		
	) -- Is child of the dummy
) -- End function




-- check the names of all the components
fn check_component_names =
(
	-- Select the dummy
	ExportDummy = findPedExportDummy()
	
	if ExportDummy != undefined then 
	(	
		-- first get a list of all the names (children of the dummy)
		meshSelectionSet = collectMeshes ExportDummy
		
		-- Loop through the meshes	
		for childindex = 1 to ExportDummy.children.count do
		(
			theMeshName = uppercase (meshSelectionSet[childindex])

			-- Discard props
			propSearch = uppercase (substring theMeshName 1 2)
			if propSearch == "P_" then
			(
				-- this is a prop, so we do nothing at this point. Might do something here later
			)	
			
			else
			(
				-- This is not a prop, so it's probably a valid component mesh
				-- Start the checks
			
				-- Is the name longer than 10 characters?
				try 
				(
					-- Names are only valid up to the first space, after that it's just a description, so trim to the first space.
					trimmedMeshName = (filterstring theMeshName " ")
					trimmedMeshName = trimmedMeshName[1]
					toolong = substring trimmedMeshName 11 1
					
					if (toolong != "") then
					(
						theErrorMessage = ("The component name " + theMeshName  + " is too long (before any spaces). The component will now be selected and the script will stop.") as string
						reportBadMesh theMeshName theErrorMessage
						return false
					)
				) 	catch ()
				
				
				
				-- Now make sure that the component name is valid 
				-- ped_componenttypes comes from a project specific config file to make this script easy to port to other projects
				componentType = substring theMeshName 1 4		
				matchedNameFlag = false
				
				-- Check the name of the component against the list of correct names.
				-- Loop through all the valid component types looking for a match
				for nc = 1 to ped_componenttypes.count do
				(
						validname = ped_componenttypes[nc]
						if (componentType == validname) then matchedNameFlag = true
				)	
				
				-- If not match was found, then the current component must not be valid.
				if matchedNameFlag == false then
				(	
					theErrorMessage = "The component name " + theMeshName  + " isn't a valid component name - the type " + componentType + " wasn't recognised. The component will now be selected and the script will stop." as string
					reportBadMesh theMeshName theErrorMessage
					return false
				)	
				
				
				
				-- The first part is correct, check the underbars are there
				theunderbar1 = substring theMeshName 5 1
				theunderbar2 = substring theMeshName 9 1
				
				if ((theunderbar1 == "_") AND (theunderbar2 == "_")) then
				(
					print "The underbar at position 5 and 9 is good"
				)
				
				else 
				(
					theErrorMessage = "Check the position of the underbars in the of " + theMeshName + " which should be the 5th and 9th character. The component will now be selected and the script will stop." as string
					reportBadMesh theMeshName theErrorMessage
					return false 
				)
				
				-- now check the 3 digits by trying to convert to an integer. If it break, then the name is bad.
				thedigits = substring theMeshName 6 3
				variationCheck = thedigits as integer

				if variationCheck == undefined then
				(
					theErrorMessage = "Check the 3 numeric digits of " + theMeshName + ". The component will now be selected and the script will stop." as string
					reportBadMesh theMeshName theErrorMessage
					return false 
				)
				
				
				-- make sure its now U or R at tenth place 
				compRacialIndentifier = substring theMeshName 10 1
				
				if (compRacialIndentifier == "U" or compRacialIndentifier == "R" or compRacialIndentifier == "M") then 
				(
					print "The Racial Indentifier bit is good"
				)
				
				else   
				(
					theErrorMessage = "Check the racial identifier of " + theMeshName + " which should be U, R or M. The component will now be selected and the script will stop." as string
					reportBadMesh theMeshName theErrorMessage
					return false  
				)
			) -- End if this is a component (not a prop)
			
		) -- End mesh loop 
		
		messageBox "The components seem to be good"
		
	) -- Is child of the dummy
) -- End function






fn checkrage=
(
	-- This bit checks that rage shaders are aplpied to components and props
	-- Select the dummy, get the kids, set their name to uppercase
	ExportDummy = findPedExportDummy()
	
	if ExportDummy != undefined then 
	(
		meshSelectionSet = collectMeshes ExportDummy
		
		-- Loop through the meshes finding the props	
		for childindex = 1 to ExportDummy.children.count do		
		(
			theMeshName = uppercase (meshSelectionSet[childindex])
			theMesh = getNodeByName theMeshName
			
			theMaterial = theMesh.material as string
			
			foundRage = undefined
			foundRage = findString theMaterial "Rage"
		
			if foundRage == undefined then
			(
				theErrorMessage = ("The mesh " + theMeshName  + " doesn't seem to have a Rage Shader applied to it") as string
				reportBadMesh theMeshName theErrorMessage
				return false
			)
			
		) -- end mesh loop	
		
		messageBox "The shaders seem to be good"
		
	) -- end children of Dummy		
	
) --End the Function





fn checkTexturesFolder =
(
	-- Get the textures folder from a config file
	imageFilesPath = returnImageFiles theProjectPedTexturesFolder
	imageFileNames = #()
	
	for imf in imageFilesPath do
	(
		append imageFileNames (uppercase(getfilenamefile imf))
	)

	
	validComponentTextures= #()
	validPropTextures= #()
	notComponents = #()
	
	
	for x = 1 to imageFileNames.count do
	(
		-- Create a flag for flagging matched textures
		foundMatch = false
		textureComp = substring imageFileNames[x] 1 4

		-- loop through the array of Valid compent names seeing if the textures look like they match
		-- if we find a match, store that as a component texture, otherwise store that next in a different array
		for matchindex = 1 to ped_componenttypes.count do 
		(
			lookForComp = ped_componenttypes[matchindex]
			
			if (textureComp == lookForComp) then
			(
				-- This looks like a component texture
				foundMatch = true
				append validComponentTextures imageFileNames[x]
			)
		)	
		
		-- This isn't a component texture, but it might be a prop. Store for more tests
		if foundMatch == false then append notComponents imageFileNames[x]
	)

	
	 -- Now see if the left overs are valid props 
	for x = 1 to notComponents.count do (
		propcheck = substring notComponents[x] 1 2
		if propcheck != "P_" then
		(
			errorText =  notComponents[x] + " does not seem to be a valid texture name for this project." as string
			messagebox  errorText
		)
		
		else
		(
			append validPropTextures notComponents[x]
		)
	)
		
	
	-- At this point I have two arrays of textures, one from components and one for props
	-- I need to further validate these texture names
	
	-- Components first
	for compIndex = 1 to validComponentTextures.count do
	(

		-- This is a wee bit messy, when I get a chance to polish this come back to it.
		-- To be honest this should be a project specific config file.
		-- At the very least a multi dimensional array.
		allCompType = substring validComponentTextures[compIndex] 1 4
		normalComptype = substring validComponentTextures[compIndex] 6 6
		allCompUnderbar1 = substring validComponentTextures[compIndex] 5 1
		diffspecCompUnderbar2 = substring validComponentTextures[compIndex] 10 1
		normalCompUnderbar2 = substring validComponentTextures[compIndex] 12 1

		-- All components should have an underbar at the 4th position
		if allCompUnderbar1 != "_" then
		(
			errorMessage = validComponentTextures[compIndex] + " is invalid for a component, it doesn't seem to have an underbar at position 4 in the name" as string
			messagebox  errorMessage
			print errorMessage
		)

		-- No we have a nested system based on the position of the second underbar in the title.
		-- Diff and spec should have an underbar at the 10th Placement_place
		if (diffspecCompUnderbar2 != "_") then
		(
			-- Is it a normal map?
			-- Might need to add extra checks here later
			-- A good reason for making this data driven
			if normalComptype == "NORMAL" then
			(
				-- It's a normalmap
				-- Is it bad?
				if (normalCompUnderbar2 != "_") then
				(
					errorMessage = validComponentTextures[compIndex] + " is invalid for a normal component texture, it doesn't seem to have an underbar at position 4 in the name" as string
					messagebox  errorMessage
					print errorMessage
				)
				
				else
				(
					tm = (validComponentTextures[compIndex] + " is a normal map")
					print tm
				)
				
				
			)
			
			else
			(
				errorMessage = validComponentTextures[compIndex] + " is invalid for a diffuse or spec component texture, it doesn't seem to have an underbar at position 4 in the name" as string
				messagebox  errorMessage
				print errorMessage
			)
		)	

		-- This is a diffuse or spec map
		else
		(
			tm = (validComponentTextures[compIndex] + " is a diffuse or spec map")
			print tm
		)
	) -- End component texture checking loop
	
	
	-- Do the props here
	for propIndex = 1 to validPropTextures.count do
	(
		theTexture = validPropTextures[propIndex]
	)
	
) -- end the texture checking Function






rollout pedExportRollout "Ped Pre Export Checks" width:120 height:200 
(
	
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Ped_Pre_Export_Checks" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	
	button btnCheckComponents "Check Component names" width:140 height:30 
	button btnCheckProps "Check Prop names" width:140 height:30 
	button btnCheckRage "Check Rage Applied"  width:140 height:30 
	button btnCheckTextureFolder "Check texture names"  width:140 height:30 
	
	
	
	on btnCheckComponents pressed do
	(
		check_component_names()
	)

	on btnCheckProps pressed do
	(
		check_prop_names()
	)

	
	on btnCheckRage pressed do
	(
		checkrage()
	)

	on btnCheckTextureFolder pressed do
	(
		checkTexturesFolder()
	)

	
)




-- Create floater  
theNewFloater = newRolloutFloater "Export checks" 180 220


-- Add the rollout section 
addRollout pedExportRollout theNewFloater


