--try (destroyDialog RsCharEyeNormals) catch ()

rollout RsCharEyeNormals "Eyelid Normals" 
(
	button btnFixEyeNorms "Fix Eyelid Normals" \
		tooltip:"Set character's eyelid-normals to how they are in their closed state, in order to take lighting better."
	
	-- Sets scene's eye-controls to closed/open state:
	fn setClosedEyes state = 
	(
		local blinkControls = #()
		for objCollection in #($CIRC_blink?, $CTRL_?_Blink) collect 
		(
			join blinkControls (objCollection as array)
		)
		
		local setCtrlWeight = if state then 0 else 1
		local setObjPos = if state then [1,1,1] else [0,0,0]
		
		for obj in blinkControls do 
		(
			obj.position.controller.weight[3] = setCtrlWeight
			in coordSys parent obj.position = setObjPos
		)
	)

	fn fixEyeNorms = 
	(
		if (selection.count == 0) do 
		(
			messageBox "Please select an object with a Skin modifier" title:"Invalid Selection"
			return False
		)
		if (selection.count > 1) do 
		(
			messageBox "Please select just one object" title:"Invalid Selection"
			return False
		)
		
		local headObj = selection[1]
		
		if (not isEditPolyMesh headObj) do 
		(
			messageBox "Please select a poly/mesh object" title:"Invalid Selection"
			return False
		)
		
		local skinModIdx, skinMod
		for n = 1 to headObj.modifiers.count while (skinModIdx == undefined) do 
		(
			local thisMod = headObj.modifiers[n]
			if (isKindOf thisMod skin) do 
			(
				skinModIdx = n
				skinMod = thisMod
			)
		)

		if (skinModIdx == undefined) do 
		(
			messageBox "Please select an object with a Skin modifier" title:"Invalid Selection"
			return False
		)
		
		setCommandPanelTaskMode #modify
		modPanel.setCurrentObject skinMod ui:True
		
		-- Find skin-modifier's bone-names that include "eye" in their name:
		local skinBoneCount = skinOps.getNumberBones skinMod
		local useBones = #{}
		useBones.count = skinBoneCount
		
		for boneNum = 1 to skinBoneCount do 
		(
			local boneName = skinOps.getBoneName skinMod boneNum 1
			
			local notMatch = True
			
			for pat in #("*eye*") while notMatch do 
			(
				if (matchPattern boneName pattern:pat) do 
				(			
					notMatch = False
				)
			)
			
			if not notMatch do 
			(
				--print boneName
				useBones[boneNum] = True
			)
		)
		
		local skinVertCount = skinOps.getNumberVertices skinMod
		local useVerts = #{}
		useVerts.count = skinVertCount
		
		for skinVertNum = 1 to skinVertCount do 
		(
			for vertBoneNum = 1 to (skinOps.getVertexWeightCount skinMod skinVertNum) do 
			(
				getBoneID = skinOps.GetVertexWeightBoneID skinMod skinVertNum vertBoneNum
				
				if useBones[getBoneID] do 
				(
					useVerts[skinVertNum] = True
				)
			)
		)
		
		-- Close the eyes:
		setClosedEyes True

		-- Add first Edit Normals modifier to reset normals
		-- (I couldn't find a way to reset normals then make them explicit again on the same modifier automatically)
		local tempNormMod = (Edit_Normals())
		addModifier headObj tempNormMod
		modPanel.setCurrentObject tempNormMod ui:True
		showEndResult = true
			
		-- Reset Normals:
		--local normCount = tempNormMod.GetNumNormals()
		local useVertNorms = #{}
		tempNormMod.ConvertVertexSelection &useVerts &useVertNorms
		tempNormMod.Reset selection:useVertNorms
		
		-- Add explicit-normals modifier:
		local normalMod = Edit_Normals displayLength:0.01
		addModifier headObj normalMod
		modPanel.setCurrentObject normalMod ui:True

		-- Remove reset-normals modifier:
		deleteModifier headObj tempNormMod

		-- Set verts to explicit:
		local useVertNorms = #{}
		normalMod.ConvertVertexSelection &useVerts &useVertNorms
		for n = useVertNorms do 
		(
			normalMod.SetNormalExplicit n
		)
		
		-- Open the eyes again:
		setClosedEyes False

		-- Turn modifier off/on to update mesh:
		normalMod.enabled = False
		normalMod.enabled = True
		
		return OK
	)
	
	on btnFixEyeNorms pressed do 
	(
		fixEyeNorms()
	)
)

--createDialog RsCharEyeNormals