-- Tag Geo with "Remove Degenerate Polys" attribute
-- Rockstar North
-- Stewart Wright August 2010
-------------------------------------------------------------------------------------------------------------------------
-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
	
-------------------------------------------------------------------------------------------------------------------------
tagAs = true
-------------------------------------------------------------------------------------------------------------------------
rollout removeDegenPolys "Remove Degenerate Polys"
(
	checkbox chkTagState "Remove Degenerate Polys" checked:tagAs
	button btnTagIt "Tag It" width:100 height:60 
-------------------------------------------------------------------------------------------------------------------------
	on chkTagState changed theState do	(tagAs = theState)
-------------------------------------------------------------------------------------------------------------------------
	on btnTagIt pressed do
	(
		tempSel = #()
		
		for sa = 1 to selection.count do
			(
				if GetAttrClass selection[sa] == "Gta Object" then
				(
					append tempSel selection[sa]
				)else()
			)
		
		for o = 1 to tempSel.count do
		(
			setAttribute tempSel[o] "Remove Degenerate Polys" tagAs
		)
	)--end btnTagIt pressed
-------------------------------------------------------------------------------------------------------------------------
)--end removeDegenPolys rollout
-------------------------------------------------------------------------------------------------------------------------
--Create the interface
createDialog removeDegenPolys width:180