-- Vertex_Bake_Painter
-- A script to bake Skylight based ambient occlusion to models
-- Rick Stirling 2008/2009

-- 12 June 2009, added prebake to vertex illumination channel to make it dark. Set the illumination bake to 60%
-- 27th August 2009: Rick Stirling. Renamed the tool and started adding CPV Wind editing
-- 2nd September 2009: Rick Stirling. Bugfixes, added vertex alpha baking.
-- OCTOBER 09 - notes
-- Removed all game choice for now. Will add in for GTA5 or Max Payne 3/Bully if requested

-- December 2009:  Moved to Wild West. Added some extra features for bakes props/map objects. Renamed to Vertex_Bake_Painter
-- Jan 2009, each object gets a standard material during the bake.

--August 2010: David Evans. Bugfixes, moved the h,v, and rate modifiers to channels 20, 21, and 22 and added a button to condense them
--down to channel 11.

-- Novemeber 2010: Rick Stirling. Rewrote the construction of the wind channels.  
-- Killed off the autorate and wind cavity. UI updates
-- Luke Openshaw rewrote the channel condensing at this stage.

--December 2010: Stewart Wright.
--Added preserve checkboxes for prebake.
--Added emesh version of the condense wind channel function
--Added check to collapse mesh to 1st vertexpaint modifier

-- January 2011: Rick Stirling
--- MAJOR rewrite. Moved functions to another include to make them easier to use. 
--- Split lighting and baking into two different data files
--- Added Artype variable plus local environment variables
--- Added contrast types


-- July 2011, Rick Stirling
-- Moved to Wildwest 2.

--May 2013, Andy Davis
--Condense Wind Channels function moved locally to the function
--Skin and Edit_Normals modifiers preserved after condense action

-- May 2013, Matt Rennie
-- Added new button for adding a sweat bake vert col modifier

-- October 2013, Kevin Ala-Pantti
-- Set up prebaking to flat colors before baking AO and rim

-- November 2013, Kevin Ala-Pantti
-- Set up loading from / saving to textures for CPV wind
-- Switched UI to rollout floater for cleaner scrolling and rollups

-- May 2014, Matt Harrad
-- Added a bunch of object type checkes for the micromovement transfers as it was crashing. 
-- Also added source and target from selection if objects are selected as well as object check (yew/no dialog) before transfering vert colours.

-- ***************************************************************************************************************************


-- ***************************************************************************************************************************
-- Common script headers, setup paths and load common functions
-- This line loads the custom header

theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()


filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")


cpvChannelList=#()

-- Load the vertex and light settings required for the tool
vbakesettings = pathToConfigFiles + "general/max_vertex_bake_settings.dat"
Skylightsettings = pathToConfigFiles + "general/max_skylight_settings.dat"

try (filein SkylightSettings) 
catch (messagebox ("Couldn't find project specific light settings file. \r\nWas expecting " + SkylightSettings))

try (filein vbakesettings) 
catch (messagebox ("Couldn't find project specific light settings file. \r\nWas expecting " + vbakesettings ))

cpvFromTexPath = RsConfigGetWildWestDir() + "script/3dsMax/Characters/Materials/FN_CPVFromTexture.ms"
try (filein cpvFromTexPath)
catch (messagebox ("Couldn't find CPV From Texture functions. \r\nWas expecting " + cpvFromTexPath ))

-- Load common functions	
filein "pipeline/util/radiosity.ms"
filein (pathToCommonFunctions + "FN_VertexPaint.ms")
	

	
-- Default the art type to peds
arttype = "Ped_"
ddatitem = 1


case arttype of
(
	"Ped_" :ddatitem = 1
	"Map_" :ddatitem = 2
	"Veh_" :ddatitem = 3
)



fn getInstallerArtType = (
	-- Get the art type using the data from the installer
	UIUserType = RsUserGetFriendlyName()	
	ShortUserType = substring UIUserType 1 3
)


	
-- ****************************************************************************************
-- This array is the channels 
-- Vertex channel id, flat colour for that channel, UI checkbox status
fn populateChannelData = (
	cpvChannelList = #(
			#(TheColourChannel, 	(readvalue ((artType + "BakeColour") as stringstream)), 	false),
			#(TheWindChannel,  	(readvalue ((artType + "WindColour") as stringstream)), 	false),
			#(TheWindChannelR, 	(readvalue ((artType + "WindColourR") as stringstream)), 	false),
			#(TheWindChannelG, 	(readvalue ((artType + "WindColourG") as stringstream)), 	false),
			#(TheWindChannelB, 	(readvalue ((artType + "WindColourB") as stringstream)), 	false),
			#(TheEnvEffChannel, 	(readvalue ((artType + "EnvEffColour") as stringstream)), 	false),
			#(TheFatChannel, 		(readvalue ((artType + "FatColour") as stringstream)), 		false),
			#(TheAlphaChannel, 	(readvalue ((artType + "AlphaColour") as stringstream)),	 false),
			#(TheRimChannel, 		(readvalue ((artType + "RimColour") as stringstream)), 	false)
	)
)	
populateChannelData()


-- ****************************************************************************************
	global vertexPaintTools
	try (cui.UnRegisterDialogBar vertexPaintTools)catch()
	try(CloseRolloutFloater vertexPaintTools)catch()

-- **************************************************************
/*
	VERTEX COLOUR BAKING ROLLOUT
*/
rollout colourbaker "Vertex Baker"
(	
	label lblDdArtType "Art Type:" align:#left
	dropDownList ddArtType "" width:100 height:40 items:#("Ped Art", "Map Props", "Vehicles") align:#center offset:[0,-20]
	
	group "Prebake Vertex Colours"
	(
		label lblPreservegroup "Preserve Data During Prebake:" offset:[-25,0]
		checkbox chkPreserveColour "Vertex Colours" checked:cpvChannelList[1][3] offset:[0,0]
		checkbox chkPreserveWind "Micro Movements" checked:cpvChannelList[2][3] offset:[100,-20]
		checkbox chkPreserveEnvEff "Dirt Effects" checked:cpvChannelList[6][3] offset:[0,0]
		checkbox chkPreserveFat "Ped Sweat" checked:cpvChannelList[7][3] offset:[100,-20]
		checkbox chkPreserveAlpha "Alpha" checked:cpvChannelList[8][3] offset:[0,0]
		checkbox chkPreserveRim "Rim/Illum" checked:cpvChannelList[9][3] offset:[100,-20]
		
		button btnPrebake "Prebake Selected with Flat Colours" width:215 height:30 offset:[0,0]
		
		progressbar pbProgress offset:[0,0]
	)
	group "Calculate Radiosity"
	(
		radiobuttons contrastType "Bake Settings:" labels:#("Cavity", "Contrast", "Static") columns:3 default:1 width:150 offset:[-15,0]
		radiobuttons edgeType labels:#("By Face", "By Vertex") columns:2 default:1 width:150 offset:[0,0]
		
		button btnMakelight "Make Skylight"   width:100  height:30 offset:[-55,0]
		button btnCalcRad "Recalculate lighting" width:100 height:30 offset:[55,-35]
		button btnBakeselAO "Assign AO Bake" width:100 height:30 offset:[-55,0]
		button btnBakeselRim "Assign Rim Bake" width:100 height:30 offset:[55,-35]

		checkbox chkCollapseEPoly "Collapse to Editable Poly after Bake" checked:false

		button btnCleanup "Cleanup lights and lighting" width:215 height:30
	)

-- ****************************************************************************************
	
	on colourbaker open do 
	(
		ddArtType.selection = ddatitem
		populateChannelData()
		chkCollapseEPoly.state = (readvalue ((artType + "CollapseEPoly") as stringstream))
	)

	on ddArtType selected item do
	(
		artTypeChosen = ddArtType.selected
		artType = (substring artTypeChosen 1 3) + "_"
		populateChannelData()
		chkCollapseEPoly.state = (readvalue ((artType + "CollapseEPoly") as stringstream))
	)	

	on chkPreserveColour changed theState do cpvChannelList[1][3] = theState

	on chkPreserveWind changed theState do
	(
		cpvChannelList[2][3] = theState
		cpvChannelList[3][3] = theState
		cpvChannelList[4][3] = theState
		cpvChannelList[5][3] = theState
	)
	on chkPreserveEnvEff changed theState do cpvChannelList[6][3] = theState
	on chkPreserveFat changed theState do cpvChannelList[7][3] = theState
	on chkPreserveAlpha changed theState do cpvChannelList[8][3] = theState
	on chkPreserveRim changed theState do cpvChannelList[9][3] = theState

	on btnPrebake pressed do
	(
		if isCLOTHSIM_Selection() then return false
		
		pbProgress.value = 0
		pbProgress.color = color 0 50 0
		prebakeSelected artType cpvChannelList pbar:pbProgress -- removed map wind mode check
	)
	
	on btnMakelight pressed do makeSkyLight artType contrastType.state
	on btnCalcrad pressed do
	(
		if isCLOTHSIM_Selection() then return false
		
		calcRad artType contrastType.state
	)
	on btnBakeselAO pressed do 
	(
		if isCLOTHSIM_Selection() then return false
		
		--prebake to flat colour to prevent multiplication
		--match checkbox states to required prebake states; just a visual change
		local chkArray = #(chkPreserveColour, chkPreserveWind, chkPreserveEnvEff, chkPreserveFat, chkPreserveAlpha, chkPreserveRim)
		chkArray[1].checked = false
		for i = 2 to 6 do chkArray[i].checked = true
		--set all channel preserve states to true except the one being prebaked (vertex colour (1))
		for i = 2 to 9 do cpvChannelList[i][3] = true
		cpvChannelList[1][3] = false
		
		local theSel = getCurrentSelection()
		
		--prebake vertex colour channel
		prebakeSelected artType cpvChannelList pbar:pbProgress
		
		if theSel.count != 0 then select theSel
		
		bakedselection = bakesel TheColourChannel "VBake AO/Colour" artType contrastType.state (edgeType.state - 1)
		--print "bakedselection"
		--print bakedselection
		if chkCollapseEPoly.state == true then  macros.run "Modifier Stack" "Convert_to_Poly"
	)
	
	on btnBakeselRim pressed do (
		--prebake to flat colour to prevent multiplication
		--match checkbox states to required prebake states; just a visual change
		local chkArray = #(chkPreserveColour, chkPreserveWind, chkPreserveEnvEff, chkPreserveFat, chkPreserveAlpha, chkPreserveRim)
		chkArray[6].checked = false
		for i = 1 to 5 do chkArray[i].checked = true
		--set all channel preserve states to true except the one being prebaked (vertex illumination (9))
		for i = 1 to 8 do cpvChannelList[i][3] = true
		cpvChannelList[9][3] = false
		
		local theSel = getCurrentSelection()
		
		--prebake vertex illumination channel
		prebakeSelected artType cpvChannelList pbar:pbProgress
		
		if theSel.count != 0 then select theSel
		
		bakesel TheRimChannel "VBake Rim/Illum" artType contrastType.state (edgeType.state - 1)
		if chkCollapseEPoly.state == true then  macros.run "Modifier Stack" "Convert_to_Poly"
	)
	
	on btnCleanup pressed do cleanuplights()
)


-- **************************************************************
/*
	WIND BAKING ROLLOUT
*/
rollout windBaker "Wind Baker (Micro Movements)"
(	
	button btnAddWind "Add Wind Layers" pos:[5,5] width:215 height:30
	button btnWindHor "(R) Hor" pos:[5,40] width:50 height:30
	button btnWindVert "(B) Vert" pos:[60,40] width:50 height:30
	button btnWindRate "(G) Rate" pos:[115,40] width:50 height:30
	button btnWindBLACK "Black" pos:[170,40] width:50 height:30	
	button btnCompressWindChannels "Condense wind channels for exporting" pos:[5,75] width:215 height:30
	button btnWindTransfer "Wind Transfer" pos:[5,115] width:215 height:30 tooltip:"Transfer wind data from one mesh to a selection of others"
	
	checkbox chx_windTrans_matchPos "Temp match target position to source." pos:[10,150] checked:true
	
	label lblPrsrve "Preserve channels:" pos:[10,170]
	checkBox chkPrsvR "R" checked:false pos:[110,170] tooltip:"Preserve (R) Horizontal" 
	checkBox chkPrsvB "B" checked:false pos:[140,170] tooltip:"Preserve (B) Vertical" 
	checkBox chkPrsvG "G" checked:false pos:[170,170] tooltip:"Preserve (G) Rate" 
	
	button btnActivateWindShaders "Activate Wind Shaders" pos:[5, 190] width:215 height:30 tooltip:"Enable wind settings on all rage shaders" 
	
	global boneList = #()	

	fn setPedWindShaderValues matid mlib newWindVal =
	(
		howmanysubs = mlib[matid].numsubs
			
		for subid = 1 to howmanysubs do
		(
			try
			(
				print ("Trying medit slot "+(matid as string)+" subid: "+(subid as string))
					
				rageChannelName = "WIND:SCLHV/FRQHV"
				setWindVal = WW_RageShaderGetSet mlib[MatID].materialList[SubID] rageChannelName newWindVal
					
				
			) catch()
		)	
	)

	fn activateWindShader = 
	(
		mlib = meditMaterials	
		
		newWindVal= [0.002,0.002,7,7]
	-- 	for matid = 8 to 9 do --this will do it only on uppers and lowrs
		for matid = 1 to 24 do --do it on everything
		(
			setPedWindShaderValues matid mlib newWindVal
		)
	)	
	
	fn projectWindData source target prsvR prsvB prsvG = 
	(
		-- myCurrSel = selection as array		
		-- first off we'll preserve the skin data
		
		--SAVE OUT SKIN DATA
		::OutputSkinningData target
		
		if prsvR != true do
		(
			--r
			RsProjectVertexMap source target SRCchannel:20 TGTchannel:20 subChannels:"all" sampleBomb:false showMessages:true
		)
		
		if prsvG != true do
		(
			--g
			RsProjectVertexMap source target SRCchannel:22 TGTchannel:22 subChannels:"all" sampleBomb:false showMessages:true
		)
		
		if prsvB != true do
		(
			--b
			RsProjectVertexMap source target SRCchannel:21 TGTchannel:21 subChannels:"all" sampleBomb:false showMessages:true
		)
		
		--condensed
		--RsProjectVertexMap source target SRCchannel:11 TGTchannel:11 subChannels:"all" sampleBomb:false showMessages:true
		--we'll do a manual condense in case we preserved some channels
		--check for channel 9 data		
		--obj = selection[1]
		
		obj = target
		if obj == undefined then
		(
			messageBox "Nothing selected!" title:"selection error"
			return false
		)
		else if isCLOTHSIM_Selection() then
		(
			return false
		)
		
		getMapSupport = undefined
		setMapSupport = undefined
		case (classOf obj.baseobject) of
		(
			Editable_mesh:
			(
				getMapSupport = meshop.getMapSupport
				setMapSupport = meshop.setMapSupport
			)
			Editable_Poly:
			(
				getMapSupport = polyop.getMapSupport
				setMapSupport = polyop.setMapSupport
			)
		)
		
		if getMapSupport obj 9 == true then
		(
			answer = queryBox "Data has been found on vertex colour channel 9\nDo you want to remove it?" title:"Channel9 ?"
			if answer == false then
			(
				messagebox "Aborted" title:"Exit"
				return false
			)
			else if answer == true then
			(
				setMapSupport obj 9 false
			)
		)
		
		for obj in selection where classOf_array #(Editable_Poly,Editable_Mesh) obj do
		(
			windBaker.CondenseWindData obj
		)
		
		currsel = selection as array --store selection for reselection later			
		select target
		
		--now we'll convert back to edit poly and relaod the skinning
		AddModifier target (Edit_Poly())
		collapseStack target	
		
		AddModifier target (Skin())
		::InputSkinningData target		
		
		--now we'll add the wind modfiers
			
		
		CreateWindModifiers()	
		select currsel	
			
		format ("Wind data projected from "+source.name+" to "+target.name)
			
	)
	
	fn passWind prsvR prsvB prsvG = 
	(
		Local SourceMesh = undefined
		Local targetMeshes = #()
		
		if (selection.count == 2) then -- SELECTION BASED
		(
			if (classof_array #(Editable_Poly, Editable_Mesh, PolyMeshObject) selection[1]) then
			(
				SourceMesh = selection[1]
				format "SOURCE : %\n" sourceMesh.name
				-- REMOVE INVALID OBJECTS
				targetMeshes = for tMesh in selection where (classof_array #(Editable_Poly, Editable_Mesh, PolyMeshObject) tMesh) collect tMesh
				-- REMOVE SOURCE MESH 
				targetMeshes = for tMesh in targetMeshes where tMesh != SourceMesh collect tMesh
			)
			else MessageBox "Seleciton only works with mesh/poly objects"
		)
		else -- OBJECT PICKER
		(
			SourceMesh = selectByName title:"Pick Source object" showHidden:true single:true

			if SourceMesh != undefined do 
			(
				format "SOURCE : %\n" sourceMesh.name
				
				if (classof_array #(Editable_Poly, Editable_Mesh, PolyMeshObject) sourceMesh) then
				(
					targetMeshes = selectByName title:"Pick Tagret objects" showHidden:true	single:false
					-- REMOVE INVALID OBJECTS
					targetMeshes = for tMesh in targetMeshes where (classof_array #(Editable_Poly, Editable_Mesh, PolyMeshObject) tMesh) collect tMesh
					-- REMOVE SOURCE MESH 
					targetMeshes = for tMesh in targetMeshes where tMesh != SourceMesh collect tMesh
				)
				else messagebox "Please only pick a mesh/poly object!" beep:true					
			)
		)
		
		if SourceMesh != undefined and targetMeshes.count != 0 do 
		(
			local message = "Source : " + SourceMesh.name + "\n\nTargets:\n" 
			for i in targetMeshes do message += i.name + "\n"
			if (queryBox message title:"Is this correct?") do
			(
				masterStart = timeStamp()						
				createDialog progBar width:300 height:30
				
				for targetMesh in targetMeshes do 
				(
					local target_pos = targetMesh.pos
					if (chx_windTrans_matchPos.state) do targetMesh.pos = SourceMesh.pos
					format "TARGET : % - Projecting Wind Data\n" targetMesh.name

					projectWindData sourceMesh targetMesh prsvR prsvB prsvG
					
					iVal = finditem targetMeshes targetMesh
					progBar.prog.value = (100.*iVal/targetMeshes.count)	
					targetMesh.pos = target_pos
				)
				
				destroyDialog progBar			
				masterEnd = timeStamp()
				
				ng = ((masterEnd - masterStart) / 1000.0)
				proctime =  ("Processing took "+(ng as string)+" seconds\n")					
				
				Format (proctime+"\n")
				messagebox ("Done!"+"\n"+proctime) beep:true
			)
		)		
	)
	
	fn SetViewMode obj =
	(
		displayColor.shaded = #object
		max modify mode
		obj.showVertexColors = on
	)
	
	fn MoveModifierToStackTop obj modType =
	(
		local numModifiers = obj.modifiers.count
		local modIndex = 0
		local modFound = undefined
		
		for i = 1 to numModifiers where ((numModifiers > 0) and modFound == undefined) do
		(
			if classOf obj.modifiers[i] == modType then
			(
				modFound = obj.modifiers[i]
				AddModifier obj modFound
				DeleteModifier obj (i + 1)
				return true
			)
		)
		
		if modFound == undefined then return false
	)	
	
	fn CondenseWindData  obj =
	(
		--GET WIND DATA
		global boneList = #()	--necessary for skin operations
		local WindLayers = #("Wind Vertical", "Wind Rate", "Wind Horizontal")
		local ValidItem = true
		local WindData = #()
		local ChannelID = 11
		
		for item in WindLayers where ValidItem do
		(
			if obj.modifiers[item] == undefined then
			(
				messagebox ("Unable to find the " + item + " channel for the object : " + (obj.name as string)) title:"Wind Baker"
				ValidItem = false
			)
		)
		
		if ValidItem then
		(
			local numMapVerts = polyop.getNumMapVerts obj ChannelID
			local GetMapVert, SetMapVert
			
			if classOf $.baseObject == Editable_Mesh then
			(
				GetMapVert = meshop.getMapVert
				SetMapVert = meshop.setMapVert
			)
			else
			(
				GetMapVert = polyop.getMapVert
				SetMapVert = polyop.setMapVert
			)
			
			for mapVert = 1 to numMapVerts do
			(
				local rval = (GetMapVert obj 20 mapVert)[1]
				local gval = (GetMapVert obj 22 mapVert)[2]
				local bval = (GetMapVert obj 21 mapVert)[3]
				
				WindData[mapVert] = [rval, gval, bval]
			)
			
			--SAVE OUT SKIN DATA
			::OutputSkinningData obj
			
			--EDIT NORMALS TO TOP OF STACK
			local NormalModFound = MoveModifierToStackTop obj EditNormals
			
			--COLLAPSE TO MOD (preserving EDIT NORMAL)
			if NormalModFound then maxOps.CollapseNodeTo $ 2 true
			else maxOps.CollapseNodeTo $ 1 true
			
			--RESTORE SKIN MOD
			if NormalModFound then AddModifier obj (Skin()) before:1
			else AddModifier obj (Skin())
			::InputSkinningData obj
			
			--APPLY WIND DATA TO BASE
			for mapVert = 1 to numMapVerts do
			(
				SetMapVert obj ChannelID mapVert WindData[mapVert]
			)
			
			obj.vertexColorType = 5
			obj.vertexColorMapChannel = ChannelID
			polyop.CollapseDeadStructs obj
			SetViewMode obj
		)
	)
	
	on btnAddWind pressed do
	(		
		if not isCLOTHSIM_Selection() then
		(			
			SetViewMode	selection[1]
			CreateWindModifiers()
		)
	)
	
	on btnWindHor pressed do
	(
		if (selection.count) == 0 then
		(
			messagebox "Nothing selected"
			return false
		)
		
		if isCLOTHSIM_Selection() then return false
		if ($.modifiers[#Wind_Horizontal] != undefined) then
		(			
			SetViewMode $
			vpt = VertexPaintTool()
			vpt.paintColor = windHorizontalColour
		
			objSelectedArray = getCurrentSelection()
			clearselection()
			select objSelectedArray[1]
			modPanel.setCurrentObject $.modifiers[#Wind_Horizontal]
			thePainterInterface.maxSize =readvalue ((artType + "paintbrushsize") as stringstream)
			vpt.curPaintMode = 1
		)
		else (	Messagebox "Unable to select the horizontal modifier, it hasn't been created yet!" title:"Horizontal Modifier!")
	)
	
	on btnWindVert pressed do
	(
		if (selection.count) == 0 then
		(
			messagebox "Nothing selected"
			return false
		)
		
		for item in selection where (matchpattern item.name pattern:"CLOTHSIM_*") do
		(
			messagebox "Clothsim meshes cannot be baked"
			return false
		)
		
		if isCLOTHSIM_Selection() then return false
		
		if ($.modifiers[#Wind_Vertical] != undefined) Then
		(
			SetViewMode $
			vpt = VertexPaintTool()
			vpt.paintColor = windVerticalColour
			if artType == "Map_" then vpt.paintColor = inverseWindVerticalColour


			objSelectedArray = getCurrentSelection()
			clearselection()
			select objSelectedArray[1]
			modPanel.setCurrentObject $.modifiers[#Wind_Vertical]
			thePainterInterface.maxSize =readvalue ((artType + "paintbrushsize") as stringstream)
			vpt.curPaintMode = 1
		)
		else (Messagebox "Unable to select the vertical modifier, it hasn't been created yet!" title:"Vertical Modifier!")
		
	)
	
	on btnWindRate pressed do
	(
		if (selection.count) == 0 then
		(
			messagebox "Nothing selected"
			return false
		)
		
		if isCLOTHSIM_Selection() then return false
		
		if ($.modifiers[#Wind_Rate] != undefined) Then
		(
			SetViewMode $
			vpt = VertexPaintTool()
			vpt.paintColor = windRateColour		

			objSelectedArray = getCurrentSelection()
			clearselection()
			select objSelectedArray[1]
			modPanel.setCurrentObject $.modifiers[#Wind_Rate]
			thePainterInterface.maxSize =readvalue ((artType + "paintbrushsize") as stringstream)
			vpt.curPaintMode = 1
		)
		else (Messagebox "Unable to select the wind rate modifier, it hasn't been created yet!" title:"Rate Modifier!")
	)

	on btnWindBLACK pressed do
	(
		if (selection.count) == 0 then
		(
			messagebox "Nothing selected"
			return false
		)
		
		if isCLOTHSIM_Selection() then return false
		
		if (classof (modPanel.getCurrentObject()) == VertexPaint) do
		(
			SetViewMode $
			currentmodifer = modPanel.getCurrentObject()
			currentmodifername = currentmodifer.name
			vpt = VertexPaintTool()
			vpt.paintColor = (color 0 0 0)

			objSelectedArray = getCurrentSelection()
			clearselection()
			select objSelectedArray[1]
			modPanel.setCurrentObject  $.modifiers[currentmodifername ]
			thePainterInterface.maxSize = readvalue ((artType + "paintbrushsize") as stringstream)

			vpt.curPaintMode = 1
		)
	)
	
	on btnCompressWindChannels pressed do
	(
		--check for channel 9 data
		obj = selection[1]
		if obj == undefined then
		(
			messageBox "Nothing selected!" title:"selection error"
			return false
		)
		else if isCLOTHSIM_Selection() then
		(
			return false
		)
		
		getMapSupport = undefined
		setMapSupport = undefined
		case (classOf obj.baseobject) of
		(
			Editable_mesh:
			(
				getMapSupport = meshop.getMapSupport
				setMapSupprot = meshop.setMapSupport
			)
			Editable_Poly:
			(
				getMapSupport = polyop.getMapSupport
				setMapSupport = polyop.setMapSupport
			)
		)
		
		if getMapSupport obj 9 == true then
		(
			answer = queryBox "Data has been found on vertex colour channel 9\nDo you want to remove it?" title:"Channel9 ?"
			if answer == false then
			(
				messagebox "Aborted" title:"Exit"
				return false
			)
			else if answer == true then
			(
				setMapSupport obj 9 false
			)
		)
		
		for obj in selection where classOf obj.baseObject == Editable_Poly or classOf obj.baseObject == Editable_Mesh do
			CondenseWindData obj
		
-- 		CondenseWindChannels()	--old command from FN_VertexPaint generic functions, now replaced by a local function
	)
	
	on btnWindTransfer pressed do 
	(
		prsvR = chkPrsvR.state
		prsvB = chkPrsvB.state
		prsvG = chkPrsvG.state
		
		passWind prsvR prsvB prsvG
-- 		filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Materials/passWind.ms")
	)
	
	on btnActivateWindShaders pressed do 
	(
		activateWindShader()
		format ("Wind shaders activated."+"\n")
	)
)

-- **************************************************************
/*
	SWEAT AND DIRT ROLLOUT
*/
rollout sweatDirtTools "Sweat and Dirt"
(
	fn addSweat obj = 
	(
		if selection.count == 1 do 
		(
			if (obj.baseObject as string) == "Editable Poly" then
			(
				addModifier obj (VertexPaint ())
				
				obj.modifiers[1].name = "Sweat Colour"
				obj.modifiers[1].mapchannel = 15	
					
				max create mode --stupid hack to get the command panel to redraw
				max modify mode

			)
			else
			(
				messagebox "Please run on an Editable Poly mesh." beep:true
			)
		)
	)	
	
	button btnAddSweat "Add Sweat Layers" pos:[5,5] width:215 height:30 tooltip:"Add a new vertex paint modifier for ped sweat."
	button btnDirtUI "Open Dirt UI" pos:[5,40] width:215 height:30 tooltip:"Open the Ped Dirt Baker tool."
	
	on btnAddSweat pressed do 
	(
		addSweat $
	)
	
	on btnDirtUI pressed do 
	(
-- 		filein (RsConfigGetWildWestDir()  + "script/3dsMax/Characters/Materials/pedDirtBaker.ms")
		filein (RsConfigGetWildWestDir() + "/script/3dsMax/Characters/Materials/pedDirtBaker.ms")
	)
)	


-- **************************************************************
/*
	LOADING CPV FROM TEXTURE ROLLOUT
*/
rollout TextureBasedCPVStorage "Texture Based CPV Storage" width:200
(
	local texToLoad = undefined
	
	button btnSaveCPV "Save CPV" width:(TextureBasedCPVStorage.width-6) height:30
	dropdownlist ddlImageOutputSize "Image Output Size" width:100 across:2
	spinner spnUVChannel "UV" range:[0,100,1] type:#integer width:50 offset:[0,20]

	button btnPickCPVTex "Pick Texture to Load" width:(TextureBasedCPVStorage.width-6) height:30
	button btnLoadCPV "Load CPV" width:(TextureBasedCPVStorage.width-6) height:30
	
	on TextureBasedCPVStorage open do
	(
		ddlImageOutputSizeItems = #()
		for imageSizeItem in TBCPV_UVimageOutputSize do 
		(
			append ddlImageOutputSizeItems (imageSizeItem as string)
		)
		TextureBasedCPVStorage.ddlImageOutputSize.items = ddlImageOutputSizeItems
		TextureBasedCPVStorage.ddlImageOutputSize.selection = 2
	)
	
	
	on btnSaveCPV pressed do 
	(
		theSelection = getcurrentselection()

		--only work on 1 object
		if (theSelection.count == 1) then
		(
			theObject = theSelection[1]
			
			--set vertex colours to display wind channel
			RsTa_TBCPV_setDisplayMode theObject
			
			UVChannelToUse = TextureBasedCPVStorage.spnUVChannel.value
			outputTextureSize = TBCPV_UVimageOutputSize[TextureBasedCPVStorage.ddlImageOutputSize.selection]
			outputTextureFolder = TBCPV_outputTextureFolder
			saveDataName = theObject.name

			--get relevant mesh data
			collectedMeshData = RsTa_TBCPV_CollectMeshData theObject UVChannelToUse
			
			--print collectedMeshData
			if (collectedMeshData[1].vertCPVColour == undefined) then
			(
				messageBox "It seems the vert colours failed to collect. Try making your mesh an editable poly."
			)
			else
			(
				--write out collected data to an image
				RsTa_TBCPV_writeCPVToTexture theObject collectedMeshData outputTextureFolder saveDataName outputTextureSize TBCPV_paddingSizeArray
			)
		) 
		
		else
		(
			messagebox "Nothing/Multiple selected - please select a single skinned mesh"
		)
		
	)	
	
	
	
	on btnPickCPVTex pressed do
	(
		fName = TBCPV_outputTextureFolder + "/"
		texToLoad = getOpenFileName types:"Targa(*.tga)|*.tga" filename:fName
	)
	
	
	
	on btnLoadCPV pressed do 
	(
		if (texToLoad != undefined) then
		(
			theSelection = getcurrentselection()

			--only work on 1 object
			if (theSelection.count == 1) then
			(
				theObject = theSelection[1]
				
				--set vertex colours to display wind channel
				RsTa_TBCPV_setDisplayMode theObject
				
				--print texToLoad
				UVChannelToUse = TextureBasedCPVStorage.spnUVChannel.value
				collectedMeshData = RsTa_TBCPV_CollectMeshData theObject UVChannelToUse

				--load data from an image
				RsTa_TBCPV_loadCPVFromTexture theObject collectedMeshData texToLoad
			) 
			
			else
			(
				messagebox "Nothing/multiple selected - please select a single skinned mesh"
			)
		)
		else
		(
			messageBox "Please choose a texture to load."
		)
	)
	
)




-- **************************************************************
/*
	BANNER ROLLOUT
*/
rollout RsVertexPaintToolsBannerRoll ""
(
	dotNetControl RsBannerPanel "Panel" pos:[6,3] height:32 width:(RsvertexPaintToolsBannerRoll.width-6)
	local bannerStruct = makeRsBanner dn_Panel:rsBannerPanel wiki:"Vertex_Paint_Tools" doOutline:false filename:(getThisScriptFilename())
	
	-- Refresh banner-icons when floater's rollouts are adjusted:
	on RsBannerPanel Paint Sender Args do 
	(
		for n = 0 to (RsBannerPanel.Controls.Count - 1) do 
		(
			RsBannerPanel.Controls.Item[n].Invalidate()
		)
	)
	
	--------------------------------------------------------------
	-- Rollout open/close
	--------------------------------------------------------------
	on RsVertexPaintToolsBannerRoll open do
	(
		-- Initialise tech-art banner:
		bannerStruct.setup()
	)
)



-- **************************************************************
/*
	MAIN ROLLOUT FLOATER
*/
(
	vertexPaintTools = newRolloutFloater "Vertex Paint Tools" 235 950 50 96

	addRollout RsvertexPaintToolsBannerRoll vertexPaintTools border:False

	for thisRoll in #(colourbaker, windBaker, sweatDirtTools, TextureBasedCPVStorage) do 
	(
		addRollout thisRoll vertexPaintTools rolledup:(RsSettingsReadBoolean thisRoll.name "rollup" false)
	)
)