-- Material Tools for character artists.
-- Parses a ped with max shaders to make Rage Versions
-- Rockstar North
-- Rick Stirling
-- Edited 12/05/09 by Stewart Wright.  Texture folder user select added.
-- Edited 09/12/09 by Stewart Wright.  New file paths edited.
-- Fix 11/03/10 by Stewart Wright.  Folder selection and updated patsh working
-- Fix 17/05/10 by Stewart Wright.  Passing in arrays to make rage functions, fixed 1st run crash (hopefully).

-- June 2011, Rick Stirling. Converted to WW2 and tidied up.
-- October 2012 Allan Hayburn added button to remove missing texture links


-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")


textureFolder = ((rsconfigmakesafeslashes maxfilepath) + "textures/highres" + "/")
chosentexturefolder=""


fn autosetPedMaterialIDS =
(	
	-- Lets get arrays of all the components
	allgeo = #()
	thegeo = #()


	-- Create an array to hold ALL the valid components
	for obj in geometry do
	(
		componentname = lowercase obj.name
		shortname = substring componentname 1 4
		
		for c = 1 to ped_textureprefix.count do
		(	
			if shortname == ped_textureprefix[c] then setComponentMatID componentName c
		)
	)
		
)	



fn updateTextureFolderText = (
	filtered = filterstring texturefolder "/"	
	TextureFolderText = filtered[filtered.count - 2]+ "/" + filtered[filtered.count - 1]+ "/" +  filtered[filtered.count]
	TextureFolderText  -- return this
)





rollout maxmattools "Ped Material Tools"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Ped_Material_Tool" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	button btnresetMat "Reset Material Editor" width:180 height:25
	button btnremoveBadTex "Remove Missing Texture Links" width:180 height:25
	button btnkillSM "Reset Scene Materials" width:180 height:25
	button btnchooseTexturePath "Choose Texture Folder"  width:180 height:25 
	
	edittext edttexturePath "" fieldWidth:220  readOnly:true
	
--	button fake ""  width:180 height:10
	button btncreatebasetextures "Auto-build 3 standard Materials"  width:180 height:25
	button btncreatehats "Auto-build p_head Textures"  width:180 height:25
	button btncreateglasses "Auto-build p_eyes Textures"  width:180 height:25
--	button btnimageResizing "Auto-Resize Textures"  width:180 height:25
	button btnautoMatID "Auto-set MatIDs (Components)"  width:180 height:25
	
	
	on maxmattools open do
		(
			WWP4vSync pedSharedTex--fetch all the shared textures
			edttexturePath.text = (updateTextureFolderText() as string)
		)
	
	on btnresetMat pressed do clear_all_materials()
	on btnremoveBadTex pressed do removeBadTextureLinks()
	on btnkillSM pressed do kill_materials()
	
	on btnchooseTexturePath pressed do
	(
		-- Assume Highres here, since artists might not have created the ConsoleRes
		dstextureFolder
		BtextureFolder = getSavePath caption:"Choose your base texture folder" initialDir: texturefolder
		if Btexturefolder != undefined then (
			BtextureFolder = ((rsconfigmakesafeslashes BtextureFolder) + "/")
			chosentexturefolder=BtextureFolder
			textureFolder=BtextureFolder
			edttexturePath.text = (updateTextureFolderText() as string)
		)
	)
	
	on btncreatebasetextures pressed do
	(
		create_ped_multi_component()
		fill_ped_multi_component textureFolder 3
	)
	
	on btncreatehats pressed do (
		create_ped_multi_prop "p_head" 6 1
		fill_ped_multi_prop textureFolder "p_head" 6 1
	)

	on btncreateglasses pressed do (
		create_ped_multi_prop "p_eyes" 5 2
		fill_ped_multi_prop textureFolder "p_eyes" 5 2
	)	
	

	on btnimageResizing  pressed do (
		-- copy batchfile to the correct place
		copyfile (theWildWest + "script/dos/Rockstar_North/ImageResize.bat") (maxfilepath + "Textures\\ImageResize.bat")
		theshellcommand = (maxfilepath + "Textures\\ImageResize.bat")
		ShellLaunch theshellcommand ""	
	)	

	on btnautoMatID pressed do	autosetPedMaterialIDS()
		
)




	rollout ragemattools "Rage Shader Creation"
	(

		button btnCreateRageComponent  "Make Rage from Max (components)"  width:200 height:25
		button btnCreateRageProp "Make Rage from Max (props)"  width:200 height:25
		button btnCreateRageComponentsPlayer  "PLAYER Make Rage Components"  width:200 height:25
		button btnCreateRagePropsPlayer " PLAYER Make Rage Props"  width:200 height:25
		checkbox chkOnlyLinkedComponents "Only include children of scene root" checked:true
		button btnCreateSingleRageComponentPlayer  "PLAYER Make Single Rage Component"  width:200 height:25
		-- button unrage "Convert Rage to Standard"  width:180 height:25
		

		on ragemattools open do
		(

		)
		
		on btnCreateRageComponent pressed do	
		(
			isplayer = false
			CreateRageComponent textureFolder isplayer false
			
			
			thefilename = maxFileName
			shortfilename = substring thefilename 1 3 

			-- If CS Ped add Eyelashes function  
			
			If  ( shortfilename == "CS_" ) then 
			(
				for m = 13 to 20 do
				(
					try (meditmaterials[m][13] = copy meditmaterials[m][15]) catch()
					try (meditMaterials[m].names[13] = "Eyelashes") catch()
					try (RstSetShaderName meditMaterials[m].materialList[13] "ped_hair_cutout_alpha") catch() 
					try (meditMaterials[m].materialList[13].name = "Rage_Shader") catch()
					
					try (WW_RageShaderGetSet [m].materialList[13] "aniso noise map" ped_hair_anis_map)catch()

				)
				
					try (
						clearSelection()
						selectByWildcard "teef_000"
						curSel = getCurrentSelection()
						
						if curSel.count < 1 then 
						(
						 print "no teef detected"
						)

						else 
						(							
							(
								messagebox "Instructions for eyelashes set-up: Set the MatID on the eyelashes to 13 (teeth, eyeballs and highlight spots should still be 15)"
							)	
						)	
						clearSelection()
					)
					catch ()
						

			)	

			
			


			
			
			 
		)	
		
		
		on btnCreateRageComponentsPlayer pressed do	
		(
			isplayer = true
			CreateRageComponent textureFolder isplayer ragemattools.chkOnlyLinkedComponents.state
			
			
			 --Rename DECL Material
			 meditMaterials[15].name = "DECL_RAGE"
			 
			 --Assign the good shader preset on DECL
			 Try (
			 
			 NumSubMaterials = getNumSubMtls meditMaterials[15] 
			 
							 for m = 1 to NumSubMaterials do
							 ( 
								 (RstSetShaderName meditMaterials[15].materialList[m] "ped_default_decal")
							 ) 
			
		          ) catch ()
		)	
		
		
		on btnCreateSingleRageComponentPlayer pressed do	
		(
			isplayer = true
			createSinglePlayerComponent textureFolder 
		)	
		
		
		on btnCreateRageProp pressed do
		(
			try (CreateRageProps 5 11 2) catch()
			try (CreateRageProps 6 12 0) catch()
		)		

	
		on btnCreateRagePropsPlayer pressed do
		(
			try (CreateRageProps 5 20 2) catch()
			try (CreateRageProps 6 21 0) catch()
			
			
			try ( $'p_eyes_*'.material = meditMaterials[20] ) catch ()
			try ( $'p_head_*'.material = meditMaterials[21] ) catch ()
		)		
		

		on unrage pressed do  
		(
		--	fileIn "unrage.ms"
		)	

	)




-- this rollout is old, but should be brought back
	
	rollout shadertools "Shader Adjustments"
	(

		button hairalpha "Set ALL Hair to Hair Shader"  width:180 height:25
		button headskin "Set Head to Skin Shader"  width:180 height:25
		button headblend "Set Head to Blendshape"  width:180 height:25

		on hairalpha  pressed do
		(
			for thesubid = 8 to 14 do
			(
				try	(RstSetShaderName meditMaterials[13].materialList[thesubid] "hair_sorted_alpha.fx")	catch()
				try	(RstSetShaderName meditMaterials[14].materialList[thesubid] "hair_sorted_alpha.fx")	catch() 
				try	(RstSetShaderName meditMaterials[15].materialList[thesubid] "hair_sorted_alpha.fx")	catch()  
				try	(RstSetShaderName meditMaterials[16].materialList[thesubid] "hair_sorted_alpha.fx")	catch() 
			)
		)

		on headskin pressed do
		(
			try	(RstSetShaderName meditMaterials[13].materialList[1] "ped_skin.fx")	catch()
			try	(RstSetShaderName meditMaterials[14].materialList[1] "ped_skin.fx")	catch() 
			try	(RstSetShaderName meditMaterials[15].materialList[1] "ped_skin.fx")	catch()  
			try	(RstSetShaderName meditMaterials[16].materialList[1] "ped_skin.fx")	catch()  
		)
		
		
		on headblend pressed do
		(
			try	(RstSetShaderName meditMaterials[13].materialList[1] "ped_skin_blendshape.fx")	catch()
			try	(RstSetShaderName meditMaterials[14].materialList[1] "ped_skin_blendshape.fx")	catch() 
			try	(RstSetShaderName meditMaterials[15].materialList[1] "ped_skin_blendshape.fx")	catch() 
			try	(RstSetShaderName meditMaterials[16].materialList[1] "ped_skin_blendshape.fx")	catch()   
		)

	)


	rollout shaderSavingTools "Shader Save/Load Tools"
	(

		button btnLaunchShaderSaver  "Shader Saver"  width:180 height:25

		on btnLaunchShaderSaver pressed do
		(
			filein (theWildWest + "script/max/Rockstar_North/character/Shader_tools/Save_Shader_Settings.ms")
		)
	)
	

try (closeRolloutFloater charMenuMat) catch()

-- Create floater 
charMenuMat = newRolloutFloater "Material Tools" 260 490

-- Add the rollout section
addRollout maxmattools charMenuMat
addRollout ragemattools charMenuMat
addRollout shaderSavingTools charMenuMat rolledUp:true
-- addRollout shadertools theNewFloater
	