-- Standard Max Wrinkle Maps 
-- A Tool to set up composite wrinkles maps on standard Max shaders for preview.
-- Rick Stirling, Rockstar North, March 2012


-- Load the usual WW headers
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
--RsCollectToolUsageData (getThisScriptFilename())


-- This requires the masks to be grayscale
-- Perhaps we need a photoshop action to create these masks?

-- Need to detect the shader that is on the object
-- Different shaders have different numbers of masks 

-- Need to pass in an object to get the rage shader from it 



fn convertRageToStandard =
(	
	theMaterial = theMesh.material
	if theMaterial == undefined then return false
	else
	(
		-- Need to go to the base of the stack
		max modify mode
		modPanel.setCurrentObject theMesh.baseObject
		
		-- Need to get id of the material of face 1
		
		theMaterial = theMesh.material
		if classOf theMaterial == MultiMaterial then
		(
			matEditIndex = undefined
			if classOf theMesh == Editable_poly then
			(
				matEditIndex = polyop.getFaceMatID theMesh 1
			)
			else
			(
				matEditIndex = getFaceMatId theMesh faces[1]
			)
			
			theMaterial = theMaterial.materialList[matEditIndex]
		)
		
		
		meditMaterials[24] = theMaterial
		medit.SetActiveMtlSlot 24 false
		
		-- If thats a rage material, find out what sort fo rage material it is
		-- I'm assuming RAGE for now, add error checking here later
		RageShaderName = RstGetShaderName meditMaterials[24] 
	)
)





-- Tool to create a normal bump with masks
-- Create a new material using the maps from another shader, plus wrinkles and masks in array format
-- The source matID should be a standard max shader
fn createCompositeBump sourceMatId sourceSub targetMatid targetSub compositeTextureArray driverSliders =
(
	-- Firstly, clone the material
	meditMaterials[targetMatid] = copy meditMaterials[sourceMatId]
	meditMaterials[targetMatid].name = "CompositeMaps"
	

	
	targetMat = meditMaterials[targetMatid].material[targetSub]
	-- Set the bump to be a normalbump material, then make it a composite map
	targetMat.bumpMap = Normal_Bump ()
	targetMat.bumpMapAmount = 100
	targetMat.bumpMap.normal_map = CompositeTexturemap ()
	
	-- Create the Layers, this is based on passed in array of textures
	baseLayerCount = compositeTextureArray[1].count -- should always be 1!
	wrinkleLayerCount = compositeTextureArray[2].count
	
	targetMat.bumpMap.normal_map.mapEnabled.count = (baseLayerCount + wrinkleLayerCount)
	
	-- Fill in the layers
	-- Layer 1 is the base map
	targetMat.bumpMap.normal_map.layername[1] = "Base"
	targetMat.bumpMap.normal_map.mapList[1] = Bitmaptexture fileName:compositeTextureArray[1][1]

	
	-- For the other layers, loop through the number of wrinklemaps and set them up.
	-- Layer will have an offset of 1
	
	for wrinkleMapIndex = 1 to wrinkleLayerCount do 
	(
		theLayer = wrinkleMapIndex + 1
		theMap = compositeTextureArray[2][wrinkleMapIndex]
		theMapName = getFileNameFile theMap
		
		-- Add the correct wrinkle map 
		targetMat.bumpMap.normal_map.layername[thelayer] = theMapName
		targetMat.bumpMap.normal_map.mapList[thelayer] = Bitmaptexture fileName:theMap
		
		-- Fill in the masks now
		-- The masks are part of a composite, so create that
		targetMat.bumpMap.normal_map.mask[thelayer] = CompositeTexturemap ()
		NumCompositeMasks =  (compositeTextureArray[4].count/wrinkleLayerCount) -- half the masks are for this map
		targetMat.bumpMap.normal_map.mask[thelayer].mapEnabled.count = NumCompositeMasks 
		
		Maskoffset = ((NumCompositeMasks * wrinklemapindex) - NumCompositeMasks)
		
		-- Populate the mask images and link to the slider
		for maskId = 1 to NumCompositeMasks do 
		(
			theMapIndex = maskId + Maskoffset 
			theDriverSlider =  getnodebyname (driverSliders[theMapIndex])
			themask = compositeTextureArray[4][theMapIndex]
			themaskname = getFileNameFile themask
			targetMat.bumpMap.normal_map.mask[thelayer].layername[maskId] = themaskname
			--targetMat.bumpMap.normal_map.mask[thelayer].mask[maskId] = Bitmaptexture fileName:themask
			targetMat.bumpMap.normal_map.mask[thelayer].mapList[maskId] = Bitmaptexture fileName:themask

			
			
			targetMat.bumpMap.normal_map.mask[thelayer].blendMode[maskId] = 2
			
			-- Link the opacity to the slider			
			-- Assume Y is the driver axis 
			
			theTarget = targetMat.bumpMap.normal_map.mask[thelayer].opacity[maskId].controller = bezier_float()
			theDriver = theDriverSlider.pos.controller.Zero_Pos_XYZ.controller.Y_Position.controller -- FE
			fc = targetMat.bumpMap.normal_map.mask[thelayer].opacity[maskId].controller  = Float_Expression ()
			SVName = "Wrinkle"
			fc.AddScalarTarget SVName theDriver
			
			finalExpression = "Wrinkle * 100"
			fc.SetExpression finalExpression
			
		)	
		
	) -- end wrinkle loop
)



-- Take a texture folder and process the subfolders to get the normal maps
fn buildWrinkleTextureArray meshName texturesSourcefolder =
(
	textureList = #()
	
	-- Get the base normal map first
	textureName = uppercase((substring meshName 1 4) + "_NORMAL_" + (substring meshName 6 3))
	baseNormal = returnImageFiles texturesSourcefolder optionalFilter:textureName
	append textureList baseNormal
	
	-- Now get the wrinkle maps
	textureName = uppercase((substring meshName 1 8) + "_WRINKLE*")
	wrinkleNormals = returnImageFiles (texturesSourcefolder + "Wrinkles/" + (substring meshName 1 8) + "/")  optionalFilter:textureName
	wrinkleNormals = sort wrinkleNormals
	append textureList wrinkleNormals 
	
	-- Now the RGBA Masks
	textureName = uppercase((substring meshName 1 8) + "_WRINKLE_Mask*")
	wrinkleMasks = returnImageFiles (texturesSourcefolder + "Wrinkles/" + (substring meshName 1 8) + "/MasksRGBA/")  optionalFilter:textureName
	wrinkleMasks = sort wrinkleMasks
	append textureList wrinkleMasks 

	-- And now the B&W Masks
	textureName = uppercase((substring meshName 1 8) + "_WRINKLE_Mask*")
	wrinkleMasks = returnImageFiles (texturesSourcefolder + "Wrinkles/" + (substring meshName 1 8) + "/MasksBW/")  optionalFilter:textureName
	wrinkleMasks = sort wrinkleMasks
	
	-- each block of 4 need to reversed
	-- split them by channel
	WmapR = #()
	WmapG = #()
	WmapB = #()
	WmapA = #() 
	
	for wm = 1 to wrinkleMasks.count do 
	(
		tname = getFileNameFile wrinkleMasks[wm]
		-- get the last letter
		lastLetter = substring tname tname.count 1
		
		case lastLetter of 
		(
			"R": append WmapR wrinkleMasks[wm]
			"G": append WmapG wrinkleMasks[wm]
			"B": append WmapB wrinkleMasks[wm]
			"A": append WmapA wrinkleMasks[wm]
		)	
	)	
	
	WmapR = sort WmapR
	WmapG = sort WmapG
	WmapB = sort WmapB
	WmapA = sort WmapA
	
	-- Now create a new final list of ordered wrinklemasks
	wrinkleMasks = #()
	for wm = 1 to WmapR.count do 
	(
		append wrinkleMasks WmapR[wm]
		append wrinkleMasks WmapG[wm]
		append wrinkleMasks WmapB[wm]
		append wrinkleMasks WmapA[wm]
	)
	
	append textureList wrinkleMasks 
	
	textureList  -- return this
)




-- I need to take a selection of objects and find the slider controllers from that collection
fn collectSliders objArray =
(
	assumedController = #()
	for i = 1 to objArray.count do 
	(
		theObj = objArray[i]
		if classof theObj == circle then appendIfUnique assumedController theObj.name
	)	
	
	assumedController = sort assumedController
	assumedController  -- return this
)	






-- some hardcoded data for tests 

theMesh = $
meshName = theMesh.name


-- Assume we have a default shader in slot 1, 1 and we are setting up the new one in 2, 1
sourceMatId = 1
sourceSub = 1
targetMatid = 2
targetSub = 1


-- Build a data array of the wrinkle textures
texturesSourcefolder = textureFolder = ((rsconfigmakesafeslashes maxfilepath) + "textures/highres" + "/")
compositeTextureArray = buildWrinkleTextureArray meshName texturesSourcefolder


selectByWildcard "Head_W"
probableheadsliders = getCurrentSelection()

-- Build an array of the sliders
-- I need to assume that the sliders are circles - not the best assumption
driverSliders = collectSliders probableheadsliders


-- verify that that the number of sliders match the number of wrinkle maps!
slidercount = driverSliders.count
maskcount = compositeTextureArray[4].count

-- Create the shader and link the sliders to it
createCompositeBump sourceMatId sourceSub targetMatid targetSub compositeTextureArray driverSliders 