/*
Rag Shader Settings Loader
Stewart Wright
Rockstar North
20/03/12

A script to load edited shader settings from Rag in to a rage shader.
*/
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--start fileins
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())
--end fileins

-------------------------------------------------------------------------------------------------------------------------
mlib = meditMaterials
-------------------------------------------------------------------------------------------------------------------------
-- Variables for load and save tools
ragShaderOutput = "x:\ped_shader_settings.txt"
maxShaderOutput = maxfilepath + "ShaderOutput.txt"
whichFile = ragShaderOutput

readData = ""
pedName = ""
foundPed = #()
meshesToSelect =#()
inputDataNames = 1
inputDataValues = 2
matID = 24
-------------------------------------------------------------------------------------------------------------------------
-- Variables for value updater
shadersettingsfile = (pathToConfigFiles + "characters/max_shader_settings.dat")
--filein shadersettingsfile

newNormStrnVal = normStrnVal
newFresStrnVal = fresStrnVal
newSpecFallVal = specFallVal
newSpecStrnVal = specStrnVal
newEnvEff_FatVal = EnvEff_FatVal
newWindVal = WindVal
newStubVal = stubVal
newDetailVal = detailVal
newHairFresStrnVal = hairFresStrnVal 
newpGlassFresStrnVal = pGlassFresStrnVal
newpGlassspecFallVal = pGlassspecFallVal
newpGlassspecStrnVal = pGlassspecStrnVal

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--load the output shader settings file
fn loadRagShaderOutput =
(
	try(readData = filein ragShaderOutput)
	catch
		(
			mText = "I couldn't find " + ragShaderOutput + "."
			messagebox mText title:"Error"
		)
)--end loadRagShaderOutput

-------------------------------------------------------------------------------------------------------------------------
--function for reading in the shader values from a text file
fn loadMaxShaderOutput =
(
	readData = #()  --housekeeping.  empty the array before we start
	sourceFolder = maxfilepath
	sourceFile = sourceFolder + "ShaderOutput.txt"
	shaderFile = openFile sourceFile
	
	if shaderFile != undefined then
	(	
		while not eof shaderFile do  --as long as we haven't reached the end of the file keep going
		(
			checkLine = readExpr shaderFile  --readExpr gets the info from the txt file
			append readData checkLine  --adds each line of the text file to our array
		)
		close shaderFile  --housekeeping.  this closes the txt file
	)
	else
	(
		mText = "I couldn't find " + sourcefile + "."
		messagebox mText title:"Error"
	)
)--end loadMaxShaderOutput

-------------------------------------------------------------------------------------------------------------------------
--function for writing out the shader values to a text file
fn writeShaderValues =
(
	outputFile = maxfilepath + "ShaderOutput.txt"  --save the file as "shadersettings.txt"
	maxShaderWrite = createFile outputFile  --now we make the text file using the above settings
	print readData to: maxShaderWrite  --write the contents of our array to the text file
	close maxShaderWrite  --housekeeping.  this closes the txt file
	print ("Shader values written out to " + outputFile)
)--end writeShaderValues

-------------------------------------------------------------------------------------------------------------------------
--function for determining the ped name
fn dummyName =
(
	try 
	(
		selectByWildcard "head_000_r"
		selectedMeshes = getCurrentSelection()
		
		for sel=1 to selectedMeshes.count do
		(
			exportDummy = selectedMeshes[sel].parent.name
			tempFilter = filterString exportDummy "-"
			if tempFilter.count == 1 do
			(
				pedName = tempFilter[1]
			)
		)
	)catch(messagebox "Unable to determine ped name.")
)--end dummyName

-------------------------------------------------------------------------------------------------------------------------
-- function to find data relating to the currently loaded max scene
fn findPed =
(
	foundPed = #()
	
	for settingsArray=1 to readData.count do --for all the data read from the ragShaderOutput file we are going to look for a match to our ped
	(
		for pairedArray=1 to readData[settingsArray].count do
		(
			if readData[settingsArray][pairedArray][inputDataNames] == "ped_name" do --search for a sub array that contains a model name
			(
				if uppercase readData[settingsArray][pairedArray][inputDataValues] == uppercase pedName do --check the model name we found against our open file's name
				(
					appendIfUnique foundPed readData[settingsArray] --store the array
				)
			)
		)
	)
)--end findPed

-------------------------------------------------------------------------------------------------------------------------
fn findMeshesToEdit =
(
	meshesToSelect =#()
	for settingsArray=1 to foundPed.count do --for all the data arrays matching our ped we are going to find a drawable name
	(
		for pairedArray=1 to foundPed[settingsArray].count do
		(
			if foundPed[settingsArray][pairedArray][inputDataNames] == "drawable_name" do --find the sub array that contains the drawable name
			(
				appendIfUnique meshesToSelect foundPed[settingsArray][pairedArray][inputDataValues] --store the drawable name if it is unique
			)
		)
	)
)--end findMeshesToEdit

-------------------------------------------------------------------------------------------------------------------------
fn updateShaderValues =
(
	for selectThese=1 to meshesToSelect.count do
	(
		selectByWildcard meshesToSelect[selectThese]
		selectedMeshes = getCurrentSelection()
		if selectedMeshes.count > 0 do
		(
			selectedMeshes[1].EditablePoly.SetSelection #Face #{1} --select a face
			selectedMatID = selectedMeshes[1].getMaterialIndex true --find out the material ID of the face
			selectedMaterial = selectedMeshes[1].material --find out the material applied to the selected mesh
			mlib[matID] = selectedMaterial --copy the material to slot 24 for editing
			
			if classof mlib[matID][selectedMatID] == Rage_Shader then --make sure the material is a rage shader
			(
				printText = ("Updating shaders on " + meshesToSelect[selectThese])
				print printText
				
				for shaderVars=1 to RstGetVariableCount mlib[matID][selectedMatID] do --for every shader variable
				(
					varName = RstGetVariableName mlib[matID][selectedMatID] shaderVars --find out the shader variable name
					
					for shaderInputData=1 to foundPed[selectThese].count do --for all the ragShaderOutput data we are going to look for a match to our shader variable's name
					(
						if uppercase varName == uppercase foundPed[selectThese][shaderInputData][inputDataNames] do --if we find a match
						(
							RstSetVariable mlib[matID][selectedMatID] shaderVars foundPed[1][shaderInputData][inputDataValues] --change the variable's values
							
							printText = (foundPed[selectThese][shaderInputData][inputDataNames] + " value(s) updated to " + (foundPed[1][shaderInputData][inputDataValues] as string))
							print printText
						)
					)					
				)
				
				printText = ("Shaders updated on " + meshesToSelect[selectThese])
				print printText
			)
		)
	)
)--end updateShaderValues

-------------------------------------------------------------------------------------------------------------------------
--function for getting the shader/material values and storing them
fn getShaderValues matID = 
(
	if classof mlib[MatID] == Multimaterial then  --if the material is a multisub then we want to look in it
	(
		multisubArray = #() --lets make a 'disposable' array for storing the multisub
		for SubID = 1 to mlib[MatID].numSubs do --for every multisub in this material
		(
			if classof mlib[matid][subid] == Rage_Shader then
			(
				shadertype = RstGetShaderName mlib[matid][subid]

				shaderArray = #() --lets make a 'disposable' array for storing the settings

				for x=1 to RstGetVariableCount mlib[matid][subid] do
				(
					tempArray = #()
					varName = RstGetVariableName mlib[matid][subid] x
					append tempArray varName
					varValue = RstGetVariable mlib[matid][subid] x
					append tempArray varValue
					append shaderArray tempArray
				)
				subIDNameAndNumber = ("Slot Name=" + mlib[MatID].names[subID] + ", subID=" + subID as string)
				tempArray = #(subIDNameAndNumber, subID, mlib[MatID].names[subID], shadertype, shaderArray)
				append multisubArray tempArray
			)
			else()
		)
		shaderNameAndNumber = ("Material Name=" + mlib[MatID].name + ", MatID=" + matID as string)
		tempArray = #(shaderNameAndNumber, mlib[MatID].name, matID, multisubArray)
		append readData tempArray --ammend our main array with the temp array and continue through the other material slots
	)
	else()
)--end getShaderValues

-------------------------------------------------------------------------------------------------------------------------
--function for setting the read shader/material values
fn setShaderValues =
(
	if readData.count !=0 then
	(
		--setup material
		for cnt = 1 to readData.count do 
		(
			tempArray = readData[cnt][4] as array

			--setup the material slot
			matID= readData[cnt][3]
			mlib[matID] = Multimaterial numsubs:(tempArray.count)
			mlib[matID].name = readData[cnt][2]

			--setup the sub-material
			for subM=1 to tempArray.count do
			(
				subID = tempArray[subM][2]
				mlib[matID].names[subID] = tempArray[subM][3]
				subShader = tempArray[subM][4]
			
				mlib[matID].materialList[subID] = Rage_Shader ()
				RstSetShaderName mlib[matID].materialList[subID] subShader --set the shader preset

				--set shader settings
				for shaderSettings = 1 to tempArray[subM][5].count do
				(
					setTo = tempArray[subM][5][shaderSettings][2]
					if setTo == "" do (setTo = default)

					try (RstSetVariable mlib[matID].materialList[subID] shaderSettings setTo) catch(getCurrentException())
				)--end set shader settings
			)--end sub-mat setup
		)--end material setup
	)else (print "no shader settings found")
)--end setShaderValues

-------------------------------------------------------------------------------------------------------------------------
-- load in shader values from external max_shader_settings.dat and populate the shader variables in the script
fn loadShaderValues =
(	
	filein shadersettingsfile 
	
	newNormStrnVal = normStrnVal
	newFresStrnVal = fresStrnVal
	newSpecFallVal = specFallVal
	newSpecStrnVal = specStrnVal
	newEnvEff_FatVal = EnvEff_FatVal
	newWindVal = WindVal
	newStubVal = stubVal
	newDetailVal = detailVal

	newHairFresStrnVal = hairFresStrnVal 
	newpGlassFresStrnVal = pGlassFresStrnVal
	newpGlassspecFallVal = pGlassspecFallVal
	newpGlassspecStrnVal = pGlassspecStrnVal
	print "Shader values loaded"
)--end loadShaderValues

-------------------------------------------------------------------------------------------------------------------------
fn setPedShaderValues matid =
(
	howmanysubs = mlib[matid].numsubs

	for subid = 1 to howmanysubs do
	(
		if classof mlib[matid][subid] == Rage_Shader then
		(
			try
			(
				setNormalStrnValue = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "bumpiness" newNormStrnVal
				setFresStrnValue = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "specular fresnel" newFresStrnVal
				setSpecFallValue = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "specular falloff" newSpecFallVal
				setSpecStrnValue = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "specular intensity" newSpecStrnVal	
				setEnvEff_FatVal = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "enveffatmaxthick" newEnvEff_FatVal
				setWindVal = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "wind:sclhv/frqhv" newWindVal
				
				setStubValue = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "stubble growth" newStubVal
				setDetailVal = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "detail inten bump scale" newDetailVal
				
				-- Overides
				-- Test for Hair
				RageShaderName = RstGetShaderName mlib[matid].materialList[subid]
				ishair = findString RageShaderName "hair"
				print "checking for hair"
				print ishair
				if ishair !=undefined then setFresStrnValue = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "specular fresnel" newHairFresStrnVal
			) catch()
		)
	)
)--end setPedShaderValues

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--create rollout
rollout shaderLoadSaveTools "Load and Save Tools"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Shader_Saver_Tool" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	--create buttons
	button btnReadMaxValues "Read Shader Values From Max" width:200 height:40 toolTip:"Reads your material editor and stores all the shader settings"
	group "Shader Loader"
	(
	label load "-[       Select Output Type to Load:       ]-"
	radiobuttons radRagOrMaxValues labels:#("Rag Output", "Max Output") toolTip:"Select which shader settings file to load"
	button btnApplyShaderValues "Load & Apply Shader Values" width:200 height:40 toolTip:"Reads in the shader values from an external file and applys them"
	)
	
	on radRagOrMaxValues changed theState do
	(
		if theState == 1 then (whichFile = ragShaderOutput)
		else if theState == 2 then (whichFile = maxShaderOutput)
	)
	
	on btnReadMaxValues pressed do
	(
		readData = #()
		--lets go through the material editor and check for rage materials, if we find any we'll read their settings
		for matSlot=1 to mlib.count do
		(
			if classof mlib[matSlot] == Multimaterial do --I am making the assumption that everything we want is a multimaterial
			(
				if classof mlib[matSlot][1] == Rage_Shader do --if the sub material is a rage material
				(
					getShaderValues matSlot
				)
			)
		)
		writeShaderValues ()
	)
	
	on btnApplyShaderValues pressed do
	(
		if whichFile == maxShaderOutput do
		(
			loadMaxShaderOutput()
			print "Loading max shader settings"
			setShaderValues()
		)
		if whichFile == ragShaderOutput do
		(
			loadRagShaderOutput()
			print "Loading Rag shader settings"
			dummyName()
			findPed()
			findMeshesToEdit()
			updateShaderValues()
		)
	)
	
	on btnApplyRagValues pressed do
	(
		loadRagShaderOutput()
		dummyName()
		findPed()
		findMeshesToEdit()
		updateShaderValues()
	)
)--end shaderLoadSaveTools rollout

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
rollout shaderUpdater "Value Updater"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Character_Shader_Updaterl" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	group "Stubble"
	(
		spinner spnStubVal "Stubble Growth" range:[0,100,0] type:#float scale:0.01
	)
	group "Detail Maps"
	(
		spinner spnDetailVal1 "Detail Intensity" range:[0,100,0] type:#float scale:0.01
		spinner spnDetailVal2 "Detail Bump" range:[0,100,0] type:#float scale:0.01
		spinner spnDetailVal3 "Detail Scale" range:[0,100,0] type:#float scale:0.01
	)
	group "Normal Strength"
	(
		spinner spnNormStr "Normal Strength" range:[0,4,0] type:#float scale:0.01
	)
	group "Specular"
	(
		spinner spnSpecFres "Specular Fresnel" range:[0,1,0] type:#float scale:0.01
		spinner spnSpecFresHair "Hair Fresnel" range:[0,1,0] type:#float scale:0.01
		spinner spnSpecFall "Specular Falloff" range:[0,512,0] type:#float scale:0.1
		spinner spnSpecStr "Specular Strength" range:[0,1,0] type:#float scale:0.01
	)
	group "Enviroment and Fatness"
	(
		spinner spnEnvEff_FatVal1 "Enviroment Effects" range:[0,100,0] type:#float scale:1
		spinner spnEnvEff_FatVal2 "Fat" range:[0,100,0] type:#float scale:1
	)
	group "Wind"
	(
		spinner spnWindVal1 "Wind Scale H" range:[0,10,0] type:#float scale:0.01
		spinner spnWindVal2 "Wind Scale V" range:[0,10,0] type:#float scale:0.01
		spinner spnWindVal3 "Wind Frequency H" range:[0,10,0] type:#float scale:0.01
		spinner spnWindVal4 "Wind Frequency V" range:[0,10,0] type:#float scale:0.01
	)
	group ""
	(
	button btnReloadVal "Reload Template Shader Values" width:190 height:25
	button btnUpdateMat "Update Material" width:190 height:50
	)
	
	-- ****************************************************************************************
	-- populate the spinner values
	fn popSpinners =
	(
		shaderUpdater.spnStubVal.range = [0,100,newStubVal]
		shaderUpdater.spnDetailVal1.range = [0,100,newDetailVal[1]]
		shaderUpdater.spnDetailVal2.range = [0,100,newDetailVal[2]]
		shaderUpdater.spnDetailVal3.range = [0,100,newDetailVal[3]]
		shaderUpdater.spnNormStr.range = [0,4,newNormStrnVal]
		shaderUpdater.spnSpecFres.range = [0,1,newFresStrnVal]
		shaderUpdater.spnSpecFresHair.range = [0,1,newHairFresStrnVal]
		shaderUpdater.spnSpecFall.range = [0,512,newSpecFallVal]
		shaderUpdater.spnSpecStr.range = [0,1,newSpecStrnVal]
		shaderUpdater.spnEnvEff_FatVal1.range = [0,10,newEnvEff_FatVal[1]]
		shaderUpdater.spnEnvEff_FatVal2.range = [0,10,newEnvEff_FatVal[2]]
		shaderUpdater.spnWindVal1.range = [0,100,newWindVal[1]]
		shaderUpdater.spnWindVal2.range = [0,100,newWindVal[2]]
		shaderUpdater.spnWindVal3.range = [0,100,newWindVal[3]]
		shaderUpdater.spnWindVal4.range = [0,100,newWindVal[4]]
	)

	-- ****************************************************************************************
	on shaderUpdater open do
	(
		loadShaderValues()
		popSpinners()
	)

	-- ****************************************************************************************
	--monitor the spinners
	on spnStubVal changed val do (newStubVal = val)
	on spnDetailVal1 changed val do (newDetailVal[1] = val)
	on spnDetailVal2 changed val do (newDetailVal[2] = val)
	on spnDetailVal3 changed val do (newDetailVal[3] = val)
	on spnNormStr changed val do (newNormStrnVal = val)
	on spnSpecFres changed val do (newFresStrnVal = val)
	on spnSpecFresHair changed val do (newHairFresStrnVal = val)
	on spnSpecFall changed val do (newSpecFallVal = val)
	on spnSpecStr changed val do (newSpecStrnVal = val)
	on spnEnvEff_FatVal1 changed val do (newEnvEff_FatVal[1] = val)
	on spnEnvEff_FatVal2 changed val do (newEnvEff_FatVal[2] = val)
	on spnWindVal1 changed val do (newWindVal[1] = val)
	on spnWindVal2 changed val do (newWindVal[2] = val)
	on spnWindVal3 changed val do	(newWindVal[3] = val)
	on spnWindVal4 changed val do	(newWindVal[4] = val)

	-- ****************************************************************************************
	on btnReloadVal pressed do
	(
		loadShaderValues()
		popSpinners()
	)

	-- ****************************************************************************************
	on btnUpdateMat pressed do
	(
		-- For every material
		for m = 1 to 24 do
		(
			setPedShaderValues m
		)
	)
)--end shaderUpdater

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
try(closeRolloutFloater theShaderToolsFloater)catch() -- Destroy the UI if it exists 
theShaderToolsFloater = newRolloutFloater "Shader Tools" 230 840
addRollout shaderLoadSaveTools theShaderToolsFloater rolledUp:false
addRollout shaderUpdater theShaderToolsFloater rolledUp:false
