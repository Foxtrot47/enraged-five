-- Tool originally by Stewart Wright, Rockstar North
-- Moved to new Wildwest and re-written by Rick Stirling
-- February 2012




-- load the common functions
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

RsCollectToolUsageData (getThisScriptFilename())
	
	


-- Load project specific shader settings
shadersettingsfile = (pathToConfigFiles + "/characters/max_shader_settings.dat")
filein shadersettingsfile 

	
mlib = meditMaterials

	
-- Variables for the ped shaders

newNormStrnVal = normStrnVal
newFresStrnVal = fresStrnVal
newSpecFallVal = specFallVal
newSpecStrnVal = specStrnVal
newEnvEff_FatVal = EnvEff_FatVal
newWindVal = WindVal
newStubVal = stubVal
newDetailVal = detailVal

newHairFresStrnVal = hairFresStrnVal 
newpGlassFresStrnVal = pGlassFresStrnVal
newpGlassspecFallVal = pGlassspecFallVal
newpGlassspecStrnVal = pGlassspecStrnVal



-- ****************************************************************************************
-- load in shader values from external max_shader_settings.dat
-- populate the shader variables in the script
fn loadShaderValues =
(	
	filein shadersettingsfile 
	
	newNormStrnVal = normStrnVal
	newFresStrnVal = fresStrnVal
	newSpecFallVal = specFallVal
	newSpecStrnVal = specStrnVal
	newEnvEff_FatVal = EnvEff_FatVal
	newWindVal = WindVal
	newStubVal = stubVal
	newDetailVal = detailVal

	newHairFresStrnVal = hairFresStrnVal 
	newpGlassFresStrnVal = pGlassFresStrnVal
	newpGlassspecFallVal = pGlassspecFallVal
	newpGlassspecStrnVal = pGlassspecStrnVal
)





-- ****************************************************************************************
fn setPedShaderValues matid =
(
	howmanysubs = mlib[matid].numsubs
		
	for subid = 1 to howmanysubs do
	(
		try
		(

			setNormalStrnValue = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "bumpiness" newNormStrnVal
			setFresStrnValue = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "specular fresnel" newFresStrnVal
			setSpecFallValue = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "specular falloff" newSpecFallVal
			setSpecStrnValue = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "specular intensity" newSpecStrnVal	
			setEnvEff_FatVal = WW_RageShaderGetSet mlib[MatID].materialList[SubID]D "enveff+fat: max thickness" newEnvEff_FatVal
			setWindVal = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "wind: sclh/sclv/frqh/frqv" newWindVal
			
			setStubValue = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "stubble growth" newStubVal
			setDetailVal = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "detail inten bump scale" newDetailVal
			
			-- Overides
			-- Test for Hair
			RageShaderName = RstGetShaderName mlib[matid].materialList[subid]
			ishair = findString RageShaderName "hair"
			print ""
			print "checking for hair"
			print ishair
			if ishair !=undefined then setFresStrnValue = WW_RageShaderGetSet mlib[MatID].materialList[SubID] "specular fresnel" newHairFresStrnVal
				
			
		) catch()
	)	
)




-- ****************************************************************************************
if ((shaderUpdater != undefined) and (shaderUpdater.isDisplayed)) do
	(destroyDialog shaderUpdater)

-- ****************************************************************************************
rollout shaderUpdater "Shader Updater"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Character_Shader_Updaterl" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	group "Stubble"
	(
		spinner spnStubVal "Stubble Growth" range:[0,100,0] type:#float scale:0.01
	)
	group "Detail Maps"
	(
		spinner spnDetailVal1 "Detail Intensity" range:[0,100,0] type:#float scale:0.01
		spinner spnDetailVal2 "Detail Bump" range:[0,100,0] type:#float scale:0.01
		spinner spnDetailVal3 "Detail Scale" range:[0,100,0] type:#float scale:0.01
	)
	group "Normal Strength"
	(
		spinner spnNormStr "Normal Strength" range:[0,4,0] type:#float scale:0.01
	)
	group "Specular"
	(
		spinner spnSpecFres "Specular Fresnel" range:[0,1,0] type:#float scale:0.01
		spinner spnSpecFresHair "Hair Fresnel" range:[0,1,0] type:#float scale:0.01
		spinner spnSpecFall "Specular Falloff" range:[0,512,0] type:#float scale:0.1
		spinner spnSpecStr "Specular Strength" range:[0,1,0] type:#float scale:0.01
	)
	group "Enviroment and Fatness"
	(
		spinner spnEnvEff_FatVal1 "Enviroment Effects" range:[0,100,0] type:#float scale:1
		spinner spnEnvEff_FatVal2 "Fat" range:[0,100,0] type:#float scale:1
	)
	group "Wind"
	(
		spinner spnWindVal1 "Wind Scale H" range:[0,10,0] type:#float scale:0.01
		spinner spnWindVal2 "Wind Scale V" range:[0,10,0] type:#float scale:0.01
		spinner spnWindVal3 "Wind Frequency H" range:[0,10,0] type:#float scale:0.01
		spinner spnWindVal4 "Wind Frequency V" range:[0,10,0] type:#float scale:0.01
	)
	button btnReloadVal "Reload Shader Values" width:190 height:25
	button btnUpdateMat "Update Material" width:190 height:50
	
	
	-- ****************************************************************************************
	-- populate the spinner values
	fn popSpinners =
	(
		shaderUpdater.spnStubVal.range = [0,100,newStubVal]
		shaderUpdater.spnDetailVal1.range = [0,100,newDetailVal[1]]
		shaderUpdater.spnDetailVal2.range = [0,100,newDetailVal[2]]
		shaderUpdater.spnDetailVal3.range = [0,100,newDetailVal[3]]
		shaderUpdater.spnNormStr.range = [0,4,newNormStrnVal]
		shaderUpdater.spnSpecFres.range = [0,1,newFresStrnVal]
		shaderUpdater.spnSpecFresHair.range = [0,1,newHairFresStrnVal]
		shaderUpdater.spnSpecFall.range = [0,512,newSpecFallVal]
		shaderUpdater.spnSpecStr.range = [0,1,newSpecStrnVal]
		shaderUpdater.spnEnvEff_FatVal1.range = [0,10,newEnvEff_FatVal[1]]
		shaderUpdater.spnEnvEff_FatVal2.range = [0,10,newEnvEff_FatVal[2]]
		shaderUpdater.spnWindVal1.range = [0,100,newWindVal[1]]
		shaderUpdater.spnWindVal2.range = [0,100,newWindVal[2]]
		shaderUpdater.spnWindVal3.range = [0,100,newWindVal[3]]
		shaderUpdater.spnWindVal4.range = [0,100,newWindVal[4]]
	)

	-- ****************************************************************************************
	on shaderUpdater open do
	(
		loadShaderValues()
		popSpinners()
	)

	-- ****************************************************************************************
	--monitor the spinners
	on spnStubVal changed val do (newStubVal = val)
	on spnDetailVal1 changed val do (newDetailVal[1] = val)
	on spnDetailVal2 changed val do (newDetailVal[2] = val)
	on spnDetailVal3 changed val do (newDetailVal[3] = val)
	on spnNormStr changed val do (newNormStrnVal = val)
	on spnSpecFres changed val do (newFresStrnVal = val)
	on spnSpecFresHair changed val do (newHairFresStrnVal = val)
	on spnSpecFall changed val do (newSpecFallVal = val)
	on spnSpecStr changed val do (newSpecStrnVal = val)
	on spnEnvEff_FatVal1 changed val do (newEnvEff_FatVal[1] = val)
	on spnEnvEff_FatVal2 changed val do (newEnvEff_FatVal[2] = val)
	on spnWindVal1 changed val do (newWindVal[1] = val)
	on spnWindVal2 changed val do (newWindVal[2] = val)
	on spnWindVal3 changed val do	(newWindVal[3] = val)
	on spnWindVal4 changed val do	(newWindVal[4] = val)

	-- ****************************************************************************************
	on btnReloadVal pressed do
	(
		loadShaderValues()
		popSpinners()
	)

	-- ****************************************************************************************
	on btnUpdateMat pressed do
	(
		-- For every material
		for m = 1 to 24 do
		(
			setPedShaderValues m
		)
	)
)--end shaderUpdater

createDialog shaderUpdater width:200