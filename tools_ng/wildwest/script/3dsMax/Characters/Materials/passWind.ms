-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()


--Load common functions	
-- filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
-- filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_bone_tagger.ms")
-- filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_Rigging.ms")
-- -- filein (theProjectRoot + "tools/dcc/current/max2010/scripts/pipeline/util/xml.ms")

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_VertexPaint.ms")
RsCollectToolUsageData (getThisScriptFilename())

	
global boneList = #()	

fn projectWindData source target = 
(
-- first off we'll preserve the skin data
	--SAVE OUT SKIN DATA
	::OutputSkinningData target
	
	--r
	RsProjectVertexMap source target SRCchannel:20 TGTchannel:20 subChannels:"all" sampleBomb:false showMessages:true
		
	--g
	RsProjectVertexMap source target SRCchannel:22 TGTchannel:22 subChannels:"all" sampleBomb:false showMessages:true
	
	--b
	RsProjectVertexMap source target SRCchannel:21 TGTchannel:21 subChannels:"all" sampleBomb:false showMessages:true
	
	--condensed
	RsProjectVertexMap source target SRCchannel:11 TGTchannel:11 subChannels:"all" sampleBomb:false showMessages:true
	
	--now we'll convert back to edit poly and relaod the skinning
	AddModifier target (Edit_Poly())
	collapseStack target	
	
	AddModifier target (Skin())
	::InputSkinningData target		
	
	--now we'll add the wind modfiers
		
	currsel = selection as array --store selection for reselection later
		
	select target
	CreateWindModifiers()	
	select currsel	
		
	format ("Wind data projected from "+source.name+" to "+target.name)
		
)

-- meshes = #(
-- 	"Uppr_000_U Default T Shirt",
-- 	"UPPR_006_U SCUBA"
-- 	)

-- 	
-- 	
-- sourceMesh = getNodeByName meshes[1]
-- targetMesh = getNodeByName meshes[2]
-- 	
-- projectWindData sourceMesh targetMesh	
	
fn passWind = 
(	
	SourceMesh = selectByName title:"Pick Source object" showHidden:true single:true

	if SourceMesh != undefined do 
	(
		print ("SourceMesh: "+(sourceMesh as string))
		
-- 		if SourceMesh.count == 1 then
-- 		(
-- 			sourceMesh = pSourceMesh[1]
			if (superclassOf sourceMesh as string) == "GeometryClass" then
			(
				targetMeshes = selectByName title:"Pick Tagret objects" showHidden:true	single:false
				
				if targetMeshes != undefined do
				(
					masterStart = timeStamp()
					
					createDialog progBar width:300 height:30
					
					for targetMesh in targetMeshes do 
					(						
						if (superClassOf targetMesh as string) == "GeometryClass" then
						(
							projectWindData sourceMesh targetMesh
						)
						else
						(
	-- 						messagebox ((targetMesh as string)+" is not a valid Target. Skipping.")
							format ((targetMesh as string)+" is not a valid Target. Skipping."+"\n")
						)
						iVal = finditem targetMeshes targetMesh
						progBar.prog.value = (100.*iVal/targetMeshes.count)						
					)
					destroyDialog progBar
					
					masterEnd = timeStamp()
					
					ng = ((masterEnd - masterStart) / 1000.0)
					proctime =  ("Processing took "+(ng as string)+" seconds\n")					
					
					Format (proctime+"\n")
					messagebox ("Done!"+"\n"+proctime) beep:true					
				)
			)
			else
			(
				messagebox "Please only pick a mesh/poly object!" beep:true
			)
-- 		)
-- 		else
-- 		(
-- 			messagebox "Please only pick 1 object!" beep:true
-- 		)
		
		
	-- 	projectWindData sourceMesh targetMesh
		
		
		
	)
)

passWind()