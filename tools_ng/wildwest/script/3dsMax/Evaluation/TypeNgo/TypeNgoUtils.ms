
global gTypeNgoUtil

struct sTypeNgoUtil
(
	private
	_commands = #(),			-- List of loaded command-descriptors
	_commandNames = #(),		-- Searchable list of TypeNgo command-names
	_commandString = Undefined,		-- Commands (plus tags) as regexable string
	
	public
	
	fn LoadCommands =
	(
		-- Reset data
		_commandString = (StringStream "")
		_commands.count = 0
		_commandNames.count = 0
		
		-- Filter out menu-items with number-names
		--	Our menu-builder scripts collects the items it creates as 'rsMenuItems'
		local validItems = for mItem in rsMenuItems where (mItem.Name as Number == undefined) collect mItem
		local hasMenuPath = HasProperty rsMenuItem #MenuPath
		
		for MItem in ValidItems do 
		(
			local newCmd = DataPair menuItem:mItem searchString:""
			Append _commands newCmd
			
			local cmdString = (StringStream "")
			Format "%" mItem.name to:cmdString
			
			if (mItem.topMenuName != undefined and hasMenuPath) do
				Format " | %%" mItem.topMenuName mItem.menuPath to:cmdString
			
			local cmdTags = mItem.Tags
			if (cmdTags != undefined) and (cmdTags != "") do 
				Format " | %" cmdTags to:CmdString
			
			newCmd.searchString = (cmdString as String)
			Append _commandNames (ToLower newCmd.searchString)
			
			Format "%\n" newCmd.searchString to:_commandString
		)
		
		_commandString = (_commandString as String)
		
		return OK
	),
	
	fn GetCommandString = 
	(
		if (_commandString == Undefined) do 
			this.LoadCommands()
		
		return _commandString
	),
	fn GetCommands = 
	(
		if (_commandString == Undefined) do 
			this.LoadCommands()
		
		return _commands
	),
	fn GetCommandNames = 
	(
		if (_commandString == Undefined) do 
			this.LoadCommands()
		
		return _commandNames
	)
)

gTypeNgoUtil = sTypeNgoUtil()
gTypeNgoUtil.LoadCommands()
