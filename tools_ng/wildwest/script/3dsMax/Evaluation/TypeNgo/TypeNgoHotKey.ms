filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/KeyboardActionManager.ms")


if (queryBox "Do you want to set Alt+Space as the TypeNGo hotkey?" title:"TypeNgo Hotkey") do
(
	kbMan = KeyboardActionManager()
	kbMan.readActions()
	
	--set the key
	kbMan.addAction 19 32 647394 macro_name:"TypeNGo" macro_category:"RS Wildwest" replace:true
	
	--save
	kbMan.writeActions()
	kbMan.maxReloadKeyboardFile()
)