try (DestroyDialog TypeNgo_UI) catch ()

-- Load dependencies:
RSTA_LoadCommonFunction #("FN_RSTA_userSettings")
FileIn (RsConfigGetWildWestDir()+"/script/3dsmax/Evaluation/TypeNgo/TypeNgoUtils.ms")

rollout TypeNgo_UI "Type 'n' Go" width:800 height:65
(
	dotNetControl RsBannerPanel "Panel" pos:[0,0] height:32 width:typeNgo_UI.width
	local bannerStruct = MakeRsBanner dn_Panel:RsBannerPanel versionNum:1.15 versionName:"Careless Lizards"
	
	local cmdHistory = rsta_userSettings app:"TypeNGo"
	local historyItems = undefined
	local historyActive = true
	local lastCommand = undefined
	
	local comboFont = dotNetObject "System.Drawing.Font" "Microsoft Sans Serif" 16 (dotNetClass "System.Drawing.FontStyle").Regular (dotNetClass "System.Drawing.GraphicsUnit").Point 0
	
	-- No Break Space character, used as an invisible separator
	local nbspChar = (Bit.IntAsChar 160)
	
	-- Set up regex object & options:
	local RegEx = (dotnetclass "System.Text.RegularExpressions.Regex")
	local RegExOpts = (dotnetclass "System.Text.RegularExpressions.RegexOptions")
	local RegExOptions = (dotnet.combineEnums RegExOpts.IgnoreCase RegExOpts.MultiLine)
	
	dotNetControl ComboboxCtrl "Combobox" Pos:[0,RsBannerPanel.Height] Width:TypeNgo_UI.width
	
	local cmdSearchList = #()
	local cmdList = #()
	
	fn compareFN v1 v2 searchStr: =
	(
		val = 0
		if matchPattern v1 pattern:searchStr then
		(
			val = -1
		)
		else
		(
			val = 1
		)
		val
	)
		

	fn commandListBuilder InString =
	(
		-- Split InString into quote-tokens, splitting chunks surrounded by double-quotes:
		local WordTokens = #()
		
		(
			local SpecialChars = ".\+*?[^]$(){}=!<>|:-"
			
			local InQuotes = False
			local CurrentToken = ""
			for n = 1 to InString.Count do 
			(
				local Char = InString[n]
				
				case Char of 
				(
					-- Quotes split word-tokens, and toggle 'InQuotes':
					"\"":
					(
						if (CurrentToken != "") do 
						(
							append WordTokens CurrentToken
							CurrentToken = ""
						)
						InQuotes = Not InQuotes
					)
					
					-- Splits split word-tokens, unless 'InQuotes' is active:
					" ":
					(
						if (InQuotes) then 
						(
							append CurrentToken Char
						)
						else 
						(
							if (CurrentToken != "") do 
							(
								append WordTokens CurrentToken
								CurrentToken = ""
							)
						)
					)
					
					-- All other characters are added to current token:
					Default:
					(
						-- Add escape-characters to regex special-characters:
						local Idx = findString SpecialChars Char
						if (Idx != undefined) do 
						(
							append CurrentToken "\\"
						)
						
						append CurrentToken Char
					)
				)
			)
			
			-- Add last token to list:
			if (CurrentToken != "") do 
			(
				append WordTokens CurrentToken
			)
			
			-- Collect unique lowercase tokens:
			WordTokens = (for WordToken in WordTokens collect (ToLower WordToken))
			WordTokens = MakeUniqueArray WordTokens
		)
		
		-- Build regex-query to search for any line containing all word-tokens, in any order:
		local query = StringStream ""
		Format "^" to:query
		for thisToken in wordTokens do 
			Format "(?=.*?%)" thisToken to:query
		Format ".*$" to:query
		query = (query as String)
		
		-- Run regex-query:
		local matches = regex.Matches (gTypeNgoUtil.GetCommandString()) query regExOptions
		
		-- Extract string-matches:
		local CommandList = for m = 0 to (matches.count - 1) collect 
		(
			local ThisMatch = Matches.Item[m]
			ThisMatch.Value
		)
		
		CommandList = makeUniqueArray CommandList
		
		--sort by inString
		if (inString.count == 0) then
		(
			sort CommandList
		)
		else
		(
			qsort CommandList compareFN searchStr:(InString + "*")
		)
		
		if (InString.count == 0) then
		(
			lastCommand = (cmdHistory.getValue "LastCommand")
			if (lastCommand != undefined) then
			(
				insertItem lastCommand CommandList 1
			)
		)

		return CommandList
	)
	
	-- This function adds elements to history dropdown in the Type 'N' Go. The History dropdown is the dropdown first seen when the tool is run,
	-- but no text has been input.
	fn RefreshHistory =
	(
		local commandsString = gTypeNgoUtil.GetCommandString()
		
		if (historyItems == undefined) then
		(
			historyItems = #()
			
			-- Collect history-items that were found in the main commands-string
			local historyCount = 10
			for i = 1 to historyCount do
			(
				local key = "History_" + (i as String)
				local val = cmdHistory.GetValue key
				
				if (IsKindOf val String) and (val != "") and (MatchPattern commandsString pattern:("*"+ val +"\n*" )) do 
					Append historyItems val
			)
			
			-- Fill the rest of the list with empty strings
			for n = (historyItems.count + 1) to historyCount do 
				Append historyItems ""
		)
	)
	
	fn AddToHistory cmdName =
	(
		if (historyItems == Undefined) or (cmdName == "") do 
			return False
		
		local lowerHistory = for item in historyItems collect (ToLower item)
		local foundIdx = FindItem lowerHistory (ToLower cmdName)
		
		if (foundIdx == 0) then
		(
			--increment all history items
			local newHistory = #()
			for i=1 to 9 do
			(
				newHistory[i+1] = historyItems[i]
			)
			newHistory[1] = cmdName
			
			historyItems = newHistory
		)
		else if (foundIdx > 1) then	--we already have it, so push it back to the top
		(
			local newHistory = historyItems
			for i=foundIdx to 2 by -1 do
			(
				historyItems[i] = historyItems[i-1]
			)
			historyItems[1] = cmdName
		)
		
		-- Update history-file
		for i = 1 to 10 do
		(
			local key = "History_" + (i as String)
			cmdHistory.setValue key historyItems[i]
		)
		
		return True
	)
	
	
	fn ExecuteCommand cmdName =
	(
		local cmdNum = FindItem (gTypeNgoUtil.GetCommandNames()) (ToLower cmdName)
		
		if (cmdNum == 0) do  
		(
			Format "Command not found!\n"
			DestroyDialog TypeNgo_UI
			return False
		)

		local cmd = (gTypeNgoUtil.GetCommands())[cmdNum]
		local macro = cmd.menuitem.macro
		local category = cmd.menuitem.category 
		Format "TypeNgo cmd: % \n" cmd
		
		-- Close TypeNgo
		DestroyDialog TypeNgo_UI
		
		try
		(
			format "macros.run \"%\" \"%\"\n" Category Macro
			cmdHistory.setValue "LastCommand" cmdName
			AddToHistory cmdName
			macros.run Category Macro
		)
		catch
		(
			Stack()
			messageBox "Problem running that tool :(" title:"Error"
			return False
		)
		
		return True
	)
	
	
	fn UpdateComboWithHistory =
	(
		--refresh from empty
		RefreshHistory()
		
		--build history
		if (historyItems != undefined) and (historyItems.count != 0) then
		(
			--collect all the entries and add to the list
			ComboboxCtrl.BeginUpdate()
			
			for i = 1 to historyItems.count do
			(
				-- 'nbspChar' looks like a space, but is used as a separator
				local hItem = (i as String) + ":" + nbspChar + historyItems[i]
				if (i < 10) do 
					hItem = ("  " + hItem)
				
				if (historyItems[i] != "undefined") then
				(
					ComboboxCtrl.Items.Add hItem
				)
				else
				(
					ComboboxCtrl.Items.Add ""
				)
			)
			
			if (not ComboboxCtrl.DroppedDown) do 
			(
				ComboboxCtrl.DroppedDown = True
			)
			ComboboxCtrl.EndUpdate()
		)	
		
		return OK
	)
	
	
	fn UpdateCombo Sender Args = 
	(
		if historyActive then
		(
			UpdateComboWithHistory()
		)
		else
		(
			if (not Sender.DroppedDown) do
			(
				Sender.DroppedDown = True
			)
			
			local SelStart = Sender.SelectionStart
			local ThisText = Sender.Text
			
			-- Build the matching-items list:
			local CommandList = CommandListBuilder ThisText
			
			Sender.BeginUpdate()
			Sender.Items.Clear()
			
			for m = 1 to CommandList.count do
			(
				Sender.Items.Add CommandList[m]
			)
			
			Sender.SelectionStart = ThisText.count
			Sender.EndUpdate()
		)

		return OK
	)
	
	
	on ComboboxCtrl KeyUp Sender Args do
	(
		case of 
		(
			(Args.KeyCode == Args.KeyCode.Escape):
			(
				-- Close TypeNgo:
				DestroyDialog TypeNgo_UI
			)
			(Args.KeyCode == Args.KeyCode.Enter):
			(
				if (Sender.Items.Count != 0) then 
				(
					local cmdStr = sender.text
					
					-- Strip numbers from start of history-numbered string:
					if historyActive do 
					(
						cmdStr = TrimLeft cmdStr "0123456789"
						cmdStr = TrimLeft cmdStr "."
					)
					
					-- Do nothing if command-string is empty:
					if (cmdStr != "") do 
					(
						ExecuteCommand cmdStr
					)
				)
				else
				(
					DestroyDialog TypeNgo_UI
				)
			)
			(Args.KeyCode == Args.KeyCode.Delete):(UpdateCombo Sender Args)
			(Args.KeyCode == Args.KeyCode.Back):(UpdateCombo Sender Args)
			(Args.KeyCode == Args.KeyCode.Space):(UpdateCombo Sender Args)
			(Args.KeyValue < 48):()
			
			-- If a number-character was pressed
			(historyActive and #{48..57}[args.keyValue]):
			(
				-- Get number-character:
				--	We'll interpret '0' keypress as '10'
				local numStr = bit.IntAsChar args.keyValue
				if (numStr == "0") do (numStr = "10")
				
				-- Find the history-command matching this number:
				local historySlot = ("History_" + numStr)
				local historyValue = cmdHistory.GetValue historySlot
				
				if(historyValue != "undefined") then
				(
					historyActive = false
					ExecuteCommand historyValue
				)
				else
				(
					Sender.Text = ""
				)
			)
			
			(Args.KeyValue < 91): -- Main alphanumerics, some punctuation
			(
				historyActive = false
				UpdateCombo Sender Args
			)	
			(Args.KeyValue < 96):()
			(Args.KeyValue < 112):(UpdateCombo Sender Args)	-- Numeric keypad keys
			(Args.KeyValue < 96):()
			(Args.KeyValue > 145):(UpdateCombo Sender Args)	-- Other punctuation
		)
	)
	
	
	--on click item
	on ComboboxCtrl SelectionChangeCommitted sender ev do
	(
		local selStr = sender.selectedItem
		
		if not (IsKindOf selStr String) do 
			return False
		
		-- Strip history-index characters from start of string, if found
		--	We use a No-Break Space character as a separator here
		local charIdx = FindString selStr nbspChar
		if (charIdx != Undefined) do 
			selStr = SubString selStr (charIdx + 1) -1
		
		ExecuteCommand selStr
		
		return True
	)
	
	
	on ComboboxCtrl LostFocus sender ev do
	(
		DestroyDialog TypeNgo_UI
	)
	
	
	on TypeNgo_UI open do
	(
		-- UI Setup:
		bannerStruct.Setup()

		local windowClr = (colorMan.getColor #window) * 255
		windowClr = (dotNetClass "System.Drawing.Color").FromArgb windowClr[1] windowClr[2] windowClr[3]
		ComboboxCtrl.BackColor = windowClr
		
		local textClr = (colorMan.getColor #windowText) * 255
		textClr = (dotNetClass "System.Drawing.Color").FromArgb textClr[1] textClr[2] textClr[3]
		ComboboxCtrl.ForeColor = textClr
		
		ComboboxCtrl.Text = ""
		ComboboxCtrl.font = comboFont
		ComboboxCtrl.dropDownHeight = 275
		
		-- Reload data if source-file has changed:
		gTypeNgoUtil.LoadCommands()
		
		UpdateComboWithHistory()
		ComboboxCtrl.Focus()
	)
	
	-- Function displays TypeNgo dialog:
	fn Show = 
	(
		DestroyDialog TypeNgo_UI
		CreateDialog TypeNgo_UI style:#(#style_border)
	)
)

TypeNgo_UI.Show()
