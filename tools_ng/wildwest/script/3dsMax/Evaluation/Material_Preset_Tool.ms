------------------------------------------------------
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
(
---------------------------------------------------------
-- LOCALS --------------------------------------------
local iniPath = (theWildWest + "script/3dsMax/_config_files/materialPresets/")
-- GLOBALS -------------------------------------------
global rs_debugPopOut
global rs_materialSaver
---------------------------------------------------------
-- FUNCTIONS --------------------------------------
---------------------------------------------------------
fn readINIFile =
(		
	debugPresetName = rs_materialSaver.ddlPresets.selected
	iniFile = iniPath + debugPresetName + ".ini"

	readText = ""
	shaderFile = openFile iniFile

	if shaderFile != undefined do
	(	
		while not eof shaderFile do  --as long as we haven't reached the end of the file keep going
		(
			newText = readLine shaderFile -- gets the info from the txt file
			if readText == "" then (readText = newText)
			else (readText = readText + "\r\n" + newText)
		)
	)
	close shaderFile  --housekeeping.  this closes the txt file
	rs_debugPopOut.txtPresetText.text = readText
	rs_debugPopOut.labelPresetPopout.text = "Editing " + debugPresetName + ".ini"
)--end readINIFile
---------------------------------------------------------
fn writeDebugINIFile =
(
	readText = filterString rs_debugPopOut.txtPresetText.text "\r\n"
	debugPresetName = rs_materialSaver.ddlPresets.selected
	iniFile = iniPath + debugPresetName + ".ini"
	maxShaderWrite = createFile iniFile --now we make the text file using the above settings
	for readTextItem=1 to readText.count do
	(
		format (readText[readTextItem] + "\r") to: maxShaderWrite  --write the contents of our array to the text file
	)
	close maxShaderWrite  --housekeeping.  this closes the txt file
)--end writeDebugINIFile
---------------------------------------------------------
fn workOutMaterialFace selMesh=
(
	theMesh = snapshotasmesh selMesh --store a snapshot of the mesh in memory
	theFace = (getFaceSelection theMesh) as array --get the selected faces on the snapshotted mesh

	if theFace.count > 0 then
	(
		theMatID = #()--create an empty array for storing all the matIDs we find
		for aFace=1 to theFace.count do--we'll check the matID on every face to see if we have different IDs anywhere
		(
			appendIfUnique theMatID (getFaceMatID theMesh theFace[aFace])
		)
		if theMatID.count == 1 then--if we only found 1 matID then we're happy
		(
			if classOf selMesh.material[theMatID[1]] == Rage_Shader then
			(
				theMaterial = selMesh.material[theMatID[1]]
			)
			else
			(
				theMaterial = undefined
				messageBox "The material applied to this face is not a rage shader."
			)
			theMaterial --pass out the material
		)
		else
		(
			theMaterial = undefined
			messageBox "The selected face(s) have multiple Material IDs"
		)
		theMaterial --pass out the material
	)
	else
	(
		theMaterial = undefined
		messageBox "There are no faces selected on this mesh."
	)
	theMaterial --pass out the material
)--end workOutMaterialFace
---------------------------------------------------------
fn get_presetList =
(
	returnArray = #()
	presetFiles = getFiles (iniPath + "*.ini")		
	for i in presetFiles do
	(
		tempArray = filterString i @"\ /" 
		justName = tempArray[tempArray.count]
		justName = substring justName 1 (justName.count-4)
		append returnArray justName			
	)
	return returnArray
)--end get_presetList
---------------------------------------------------------
fn load_matPreset getMat =
(
	presetName = rs_materialSaver.ddlPresets.selected
	iniFile = iniPath +  presetName + ".ini"
	settingsArray = (getINISetting iniFile "settings")
	-- SET SHADER TYPE ----------------------
	if (rs_materialSaver.cbx_sType.checked) do
	(
		RstSetShaderName getMat (getINISetting iniFile "settings" settingsArray[1])
	)
	-- SET SHADER VARS ----------------------
	for s in settingsArray do
	(
		for i = 1 to (RstGetVariableCount getMat) where (RstGetVariableName getMat i) == s do
		(	
			type = RstGetVariableType getMat i
			ini_set = getINISetting iniFile "settings" s
			
			if (type == "texmap") then
			(
				if (rs_materialSaver.cbx_Textures.checked) do RstSetVariable getMat i ini_set 
			)
			else
			(
				if (rs_materialSaver.cbx_settings.checked) do
				(
					if (type  == "float") do ini_set = ini_set as float
					if (type  == "vector3") do 
					(
						values = filterString ini_set  "[ , ]"
						temp_point = [0,0,0]
						temp_point[1]= values[1] as float
						temp_point[2]= values[2] as float
						temp_point[3]= values[3] as float
						ini_set = temp_point
					)
					if (type  == "vector4") do 
					(
						values = filterString ini_set  "[ , ]"
						temp_point = [0,0,0,0]
						temp_point[1]= values[1] as float
						temp_point[2]= values[2] as float
						temp_point[3]= values[3] as float
						temp_point[4]= values[4] as float
						ini_set = temp_point
					)						
					RstSetVariable getMat i ini_set
				)
			)
		)
	)
)--end load_matPreset
--------------------------------------------------------
fn save_matPreset getMat = 
(
	presetName = rs_materialSaver.edtName.text
	if (presetName!= "") do
	(
		safeToSave = true			
		iniFile = (iniPath + presetName + ".ini")
		iniFile = rsMakeSafeSlashes iniFile
		if (doesFileExist iniFile) do
		(
			if not(queryBox "Preset already exists, Do you want to continue?" title:"Warning!") do safeToSave = false
		)
		if (safeToSave) do
		(
			wipeFile = createFile iniFile  --now we make the text file using the above settings
			close wipeFile  --housekeeping.  this closes the txt file
			
			setINISetting iniFile "settings" "shaderName" (RstGetShaderName getMat)	
			-----------------------------------------------------------
			for shaderVar=1 to RstGetVariableCount getMat do
			(
				varName = RstGetVariableName getMat shaderVar --get the variable name
				varValue = (RstGetVariable getMat shaderVar) as string --get the variable value
				setINISetting iniFile "settings" varName varValue	
			)

			rs_materialSaver.ddlPresets.items = (get_presetList())
		)
	)
)--end save_matPreset
---------------------------------------------------------
fn UIPos =
	(
	userScreenSize = sysInfo.desktopSize --find out the desktop size
	mainWinPos = getDialogPos rs_materialSaver --work out the position of the presets window
	posOnMonitor = userScreenSize[1] - mainWinPos[1]
	
	case of
	(
		(posOnMonitor > 2600): (debugWinPos = [(mainWinPos[1] + 185),mainWinPos[2]]) --right
		(posOnMonitor < 2599): (debugWinPos = [(mainWinPos[1] - 570),mainWinPos[2]]) --left
	)
	setDialogPos rs_debugPopOut debugWinPos
)--end UIPos
---------------------------------------------------------
-- UI --------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------
(
	try (destroyDialog rs_materialSaver) catch()
	-- LOCALS -------------------------------------------
	local theState = 1
	---------------------------------------------------------
	rollout rs_debugPopOut "Preset Debug" width:200
	(
		label labelPresetPopout "Preset Debug:" align:#left
		editText txtPresetText "" width: 520 height: 200
		button btnSaveEdit "Save Edits" width:100 offset:[-80,0] toolTip:"Save Edits."
		button btnReloadData "Revert" width:100 offset:[50,-26] toolTip:"Revert Edits."
		
		-- EVENTS ---------------------------------------------
		on btnSaveEdit pressed do
		(
			writeDebugINIFile()
		)
		---------------------------------------------------------
		on btnReloadData pressed do
		(
			readINIFile()
		)
	)--end rs_debugPopOut
	---------------------------------------------------------
	rollout rs_materialSaver "Material Presets"
	(
		dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:rs_materialSaver.Width
		local banner = makeRsBanner colourScheme:#eval dn_Panel:rsBannerPanel filename:(getThisScriptFilename())
		
		group "Material Source"
		(
			label materialSource "Read Material From Selected:" align:#left
			radiobuttons radFaceOrMaterial labels:#("Face", "Material") toolTip:"Select how you want to define the material to update"
		)
		group "Create New Preset"
		(
			label presetName "Preset Name:" align:#left
			editText edtName width:140 offset:[0,0]
			button btnSave "Save Rage Material" width:130 tooltip:"Create preset from selecte material"
		)	
		group "Shader Preset"
		(
			dropdownlist ddlPresets width:140 items:(get_presetList())
			label shaderOptions "Shader Set Options:" align:#left
			checkbox cbx_sType "Shader Type" checked:true
			checkbox cbx_Textures "Textures Paths" checked:true
			checkbox cbx_settings "Shader Values" checked:true
			button btnLoad "Set Rage Material" width:130 toolTip:"Load preset on to selected material."
		)
		group "Preset Debug"
		(
			button btnDebug "Debug" width:130
		)
		-- EVENTS ---------------------------------------------
		on radFaceOrMaterial changed newState do (theState = newState)
		---------------------------------------------------------
		on ddlPresets selected sel do (readINIFile())
		---------------------------------------------------------
		on btnSave pressed do
		(
			if rs_materialSaver.edtName.text != "" then
			(
				whatMaterial = undefined
				case of
				(
					(theState == 1):--if the 'face' button is checked
					(
						if selection.count == 1 then
						(
							if superClassOf selection[1] == GeometryClass then
							(
								whatMaterial = workOutMaterialFace selection[1]
							)
							else
							(
								messageBox "The selected object is not geometry."
							)
						)
						else
						(
							messageBox "Please select a single mesh."
						)
					)
					(theState == 2):--if the 'material' button is checked
					(
						if classOf (medit.getcurmtl()) == Rage_Shader then--if the material is a rage shader we'll use it
						(
							whatMaterial = medit.getcurmtl()
						)
						else
						(
							messageBox "Please open the material editor and select a rage shader."
						)
					)
				)
				if whatMaterial != undefined do
				(
					save_matPreset whatMaterial
					readINIFile()
				)
			)
			else
			(
				messageBox "Please define a name for the preset."
			)
		)--end on btnSave pressed
		---------------------------------------------------------
		on btnLoad pressed do
		(
			whatMaterial = undefined
			case of
			(
				(theState == 1):--if the 'face' button is checked
				(
					if selection.count == 1 then
					(
						if superClassOf selection[1] == GeometryClass then
						(
							whatMaterial = workOutMaterialFace selection[1]
						)
						else
						(
							messageBox "The selected object is not geometry."
						)
					)
					else
					(
						messageBox "Please select a single mesh."
					)
				)
				(theState == 2):--if the 'material' button is checked
				(
					if classOf (medit.getcurmtl()) == Rage_Shader then--if the material is a rage shader we'll use it
					(
						whatMaterial = medit.getcurmtl()
					)
					else
					(
						messageBox "Please open the material editor and select a rage shader."
					)
				)
			)
			if whatMaterial != undefined do
			(
				load_matPreset whatMaterial
			)
		)--end on btnLoad pressed
		---------------------------------------------------------
		on btnDebug pressed do
		(
			try (destroyDialog  rs_debugPopOut) catch()
			createDialog rs_debugPopOut width:550 style:#(#style_border,#style_toolwindow,#style_sysmenu)
			UIPos()
			readINIFile()
		)--end on btnDebug pressed
		-----------------------------------------------------------
		on rs_materialSaver open do
		(
			rs_dialogPosition "get" rs_materialSaver	
			rs_materialSaver.banner.setup()
		)
		on rs_materialSaver close do
		(
			try (destroyDialog  rs_debugPopOut) catch()
			rs_dialogPosition "set" rs_materialSaver
		)
	)--end rs_materialSaver
)
createDialog rs_materialSaver width:165 style:#(#style_border,#style_toolwindow,#style_sysmenu)
)