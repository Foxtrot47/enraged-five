try( destroyDialog RsFindScriptRollout ) catch ()


rollout RsFindScriptRollout "Find R* Script"
(
	-- Local defines
	local results = #()
	
	
	-- Controls
	hyperlink	urlHelp				"Help" address:"" align:#right visitedColor:( color 200 200 200 )
	edittext 	etSearch 			"Search:" labelOnTop:true
	checkbox	chkExactPhrase 	"Exact Phrase"
	listbox		lstResults 			"Results:" height:20
	
	
	-- Functions
	
	fn sortResults a b = (
		if a[ 1 ] > b[ 1 ] then (
			return 1
			
		) else (
			return -1
		)
	)
	
	
	fn runSearch = (
		results = RsFindScript.search etSearch.text exactPhrase:chkExactPhrase.checked
		qsort results sortResults
		
		lstResults.items = for i in results collect i[ 1 ]
		
		if lstResults.items.count == 0 then (
			lstResults.items = #( "No results!" )
		)
	)
	
	
	fn runScript = (
		if results.count > 0 then (
			
			-- The menu item doesn't filein a script, but has code directly in it, so execute it.
			if results[ lstResults.selection ][ 3 ] != undefined then (
				execute results[ lstResults.selection ][ 3 ]
				
			-- Run the same script the macroscript would run.
			) else (
				scriptFile = results[ lstResults.selection ][ 2 ]
				fileIn scriptFile
			)
		)
	)
	

	-- Event Handlers

	on etSearch entered txt do (
		runSearch()
	)
	
	
	on lstResults doubleClicked item do (
		runScript()
	)
	
	
	on etSearch changed txt do (
		runSearch()
	)
	
	
	on RsFindScriptRollout resized size do (
		lstResults.width = size[ 1 ] - 26
		lstResults.height = size[ 2 ] - 118
		etSearch.width = size[ 1 ] - 26
		urlHelp.pos.x = size[ 1 ] - 34
	)
	
	
	on RsFindScriptRollout open do (
		
		-- Trigger the system to build the necessary data to run.  This should only be needed one time, unless someone
		-- calls RsFindScript.flush()
		if not RsFindScript.isBuilt() do (
			RsFindScriptRollout.title = "Find R* Script - Building search database..."
			RsFindScript.search ""
			RsFindScriptRollout.title = "Find R* Script"
		)
	)
	
	
	on RsFindScriptRollout close do (
		
	)
)

createDialog RsFindScriptRollout width:400 height:384 style:#( #style_titlebar, #style_resizing, #style_toolwindow, #style_sysmenu )