
fn sdCreateMacro macroName macroCategory buttonText scriptCall =
(
	
	strNewMacro = ("macroscript " + macroName + "\ncategory:\"" + macroCategory + "\"\nbuttontext:\"" + buttonText + "\"\n(\n\ton execute do \n\t(\n\t\tfileIn \"" + scriptCall + "\" \n\t)\n)")
	execute strNewMacro
)