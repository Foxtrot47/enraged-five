--Prohibit (Exclusion) setup tools
--Stewart Wright
--Rockstar North
--19/11/08
--Update 11/12/08: fix for clear exclusions (was only working IF there was other text in the userpropbuffer)
--Update 25/09/09: Updated object heirachy to reflect post GTA IV projects.  (SUSE replaced with ACCS, SUS2 replaced with TASK)
--Update 22/02/10: added help link.  removed absoloute paths.
--Update 12/04/10:  Exclusion order updated according to https://devstar.rockstargames.com/wiki/index.php?title=Ped_Components&oldid=5811#Ped_Component_Draw_Order
--Update 07/10/10:  Updated exclusion order for GTAV only.
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- Common script headers, setup paths and load common functions
--start fileins
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())

--setup arrays
selObj = #()
selObjOrder = #()
parObj = 0
childObj = 0
splitUDP = #()


fn resetArrays =
(
	selObj = #()
	selObjOrder = #()
	parObj = 0
	childObj = 0
	splitUDP = #()
)


fn reorderTags theParent = 
	(
		currentUDP = getUserPropbuffer theParent
		filterUDP = filterString currentUDP "\r\n"
		alphUDP = sort filterUDP
	
		tag = ""	
		for t = 1 to alphUDP.count do
		(
			tag = tag + alphUDP[t] + "\r\n"
			setUserPropbuffer theParent tag
		)
		
	)

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--create rollout
rollout ExclusionTools "Exclusion Tools"
	
(
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Character_Exclusion_setup_tool" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	label desc "  Exclusion Setup Tools" width:120 height:20

	--create buttons
	button btnDispPro "Show Exclusions" width:120 height:40 toolTip:"Display exclusions on currently selected objects."
	button btnCreatePro "Create Exclusion" width:120 height:40 toolTip:"Select 2 objects to exclude them from drawing together."
	button btnDelPro "Clear Exclusions" width:120 height:40 toolTip:"Delete any and all exclusion settings from the selected objects."

	
	--start of on dispPro pressed do
	On btnDispPro pressed do
	(
		resetArrays () 
		if  selection.count < 1 then	(
			messagebox "Must select something!"
			return 0
			)
			
		else	(
			for objSel in selection do (
				curPro = #()
				currentUDP = getUserPropbuffer	objSel
				filterUDP = filterString currentUDP "\n"
				for x = 1 to filterUDP.count do (
					tempUDP = uppercase filterUDP[x]
					shortUDP = substring tempUDP 1 8
					if shortUDP == "PROHIBIT" then append curPro tempUDP
				)
			
				sVar = ""
				for s = 1 to curPro.count do (
					sVar = sVar + curPro[s]
				)
				
				if sVar == "" then (
					mText = objSel.name + " has no exclusions set."
					messagebox mText title:objSel.name	
				)
				else (
					mText = objSel.name + " excludes; \n\r" + sVar
					messagebox mText title:objSel.name
				)
			)
		)
	
		)--end of on dispPro pressed do
	
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
		
	--start of on createPro pressed do	
	On btnCreatePro pressed do	(
		resetArrays ()
		if  selection.count > 2 then (
			messagebox "Only select 2 components at a time"
			return 0
		)
	
		if selection.count < 2 then (
			messagebox "Must select 2 components"
			return 0
		)


		--add selected objects to array	
		for objSel in selection do (append selObj (uppercase objSel.name))



		--get short names and work out how fall along the heirachy they are (x)
		for arrObj in selObj do (
		
		shortname = substring arrObj 1 4 --shorten names to 4 characters
		
			for x = 1 to ped_vExclusionOrder.count do (
				if shortname == ped_vExclusionOrder[x] then
				(
				append selObjOrder x
				)	
			)
		)
	
		--find out which object is parent
		if selObjOrder[1] > selObjOrder[2] then (
			parObj = 2
			childObj = 1
		)

		else
		(
			parObj=1
			childObj = 2
		)

	
		--select the parent object and get ready to tag it
		theParent = getNodeByName selObj[parObj] --setting a variable for the parent object "theParent"
		currentUDP = getUserPropbuffer theParent --gets the current user defined properties and adds them to variable "currentUDP"
	
		filterUDP = filterString currentUDP "\r\n"

		proFlag1 = 0
		proFlag2 = 0
	
		for x = 1 to filterUDP.count do	(
			tempUDP = uppercase filterUDP[x]
			shortUDP = substring tempUDP 1 9
			shortChild = substring tempUDP 11 60
			
			if shortchild == selObj[childObj] then (
				mText = selObj[childObj] + " is already excluded from drawing with " + theParent.name + "."
				messagebox mText title:theParent.name
				
				reorderTags theParent
				return 0
			)
			
			if shortUDP == "PROHIBIT1" then proFlag1 = 1
			if shortUDP == "PROHIBIT2" then proFlag2 = 1
			else print "no prohibits found"

		)

		baseP = "prohibit"

		if proFlag1 == 1 and proFlag2 == 1 then (
			mText = theParent.name + " is already set to exlude the maximum components."
			messagebox mText title:theParent.name
		)
		
		if proFlag1 == 0 then (
			tag = baseP+"1="+selObj[childObj]
			oldTag = getUserPropbuffer theParent
			tag = oldTag+"\r\n"+tag
			setUserPropbuffer theParent tag
		)

		if proFlag2 == 0 and proFlag1 != 0 then (
			tag = baseP+"2="+selObj[childObj]
			oldTag = getUserPropbuffer theParent
			tag = oldTag+"\r\n"+tag
			setUserPropbuffer theParent tag
		)

		reorderTags theParent
		
	)--end of on createPro do
	
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
	
	--start of on delPro pressed do
	On btnDelPro pressed do
	(
		resetArrays ()
		if selection.count < 1 then (
			messagebox "Nothing Selected!"
			return 0
		)
			
		else	(
			for objSel in selection do (
					keepUDP = #()
					currentUDP = getUserPropbuffer	objSel
					filterUDP = filterString currentUDP "\r\n"
					for x = 1 to filterUDP.count do (
						tempUDP = uppercase filterUDP[x]
						shortUDP = substring tempUDP 1 8
						if shortUDP != "PROHIBIT" then
						append keepUDP tempUDP
					)
				
					mText = "This will delete all the exclusions from " + objSel.name +".  Are you sure?"
					if queryBox mText title:"Delete Exlcusions" beep:false then (
						tag = ""
						
						if keepUDP.count == 0 then setUserPropbuffer objSel tag
						else
						
						for x = 1 to keepUDP.count do (
							tag = tag + keepUDP[x] + "\r\n"
							setUserPropbuffer objSel tag
						)
					)
				)--end of for objsel in selection do
			)
	
	)--end of on delPro pressed do
	
)--end of rollout "Prohibit Tools"

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--Create the interface
createDialog ExclusionTools width:130