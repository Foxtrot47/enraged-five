

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())

fn showSkinningAndBones = 
(
	--clearListener()
	skinnedObjects = #()
	skinningBones = #()
	objectsToShow = #()
	geoParent = undefined

	max unfreeze all
	max unhide all
	
	if $Dummy01 != undefined do 
	(
		appendIfUnique objectsToShow $Dummy01
	)
	
	--FIRST FIND ALL SKINNED OBJECTS SO WE CAN LOOP THROUGH THEM
	for obj in objects do
	(
		if obj.modifiers[#Skin] != undefined do 
		(
			appendIfUnique skinnedObjects obj
			appendIfUnique objectsToShow obj
			if obj.parent != undefined do
			(
				geoParent = obj.parent
				appendIfUnique objectsToShow obj.parent --this ensures we get the root of the skinned objects in there
			)
		)
	)
	if childrenArray != undefined then
	(
		childrenArray = geoParent.children
		
		for chld in childrenArray do
		(
			appendIfUnique objectsToShow chld
		)

		if ((progBar != undefined) and (progBar.isDisplayed)) do
		(destroyDialog progBar)
		CreateDialog progBar width:300 Height:30
		progBar.prog.color = [10,210,10] --green
		
		
		for i = 1 to skinnedObjects.count do
		(
			max modify mode
			select skinnedObjects[i]
			modPanel.setCurrentObject skinnedObjects[i].modifiers[#Skin]
			print ("testing skinning on "+skinnedObjects[i].name)
			numberSkinBones = skinops.getNumberBones skinnedObjects[i].modifiers[#Skin]

			for bs = 1 to numberSkinBones do
			(
				boneName = skinOps.GetBoneName skinnedObjects[i].modifiers[#Skin] bs 1
				boneObject = (getNodeByName boneName)
				appendIfUnique skinningBones boneObject --append to this array so we get a list of all bones that are actually in the skin
				appendIfUnique objectsToShow boneObject 
			)
			clearSelection()
			
			progBar.prog.value = ((100* i)/skinnedObjects.count)
		)

		(destroyDialog progBar)
		
		clearSelection()


		for obj in objectsToShow do
		(
			selectMore obj
		)
		
		max select invert
		max hide selection
		
		messageBox "TADA!"
	)
	else
	(
		messageBox "No Nodes in the scene!"
	)
)

fn prepShaders = 
(
	filein (RsConfigGetWildWestDir() + "/script/3dsMax/Cutscene/setMaterialToDiffOnly.ms")	
	
	showSkinningAndBones()
	
)
prepShaders ()
	