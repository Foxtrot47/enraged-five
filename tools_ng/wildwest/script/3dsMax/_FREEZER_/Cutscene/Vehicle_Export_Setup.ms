/*
script rquest for bug 163534

Vehicle Export Setup

Stewart Wright
December 2011
Rockstar North

Script will create dummy and link the vehicle skeleton to it before exporting everything (Dummy, skeleton and skinned mesh) as an FBX
*/
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--start fileins
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())
-------------------------------------------------------------------------------------------------------------------------
--common variables
savePath = theProjectRoot + "/art/animation/resources/vehicles/"
	
maxName = undefined
vehicleMesh = #()
parentDummy = undefined
vehicleSkel = #()
everything = #()
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
fn createDummy =
(
	maxName = filterstring maxFileName "_" --this should get the prefix of vehicle name which is always vehiclename_skinned.max
	chassisBoneName = (maxName[1]+"_Skel")

	try (delete $Dummy01) catch()--try and delete a dummy mesh if it exists already
	
	parentDummy = dummy pos:[0,0,0]
	parentDummy.name = "Dummy01"

	skel = getNodeByName chassisBoneName
	skel.parent = parentDummy

	print (chassisBoneName+" parented to "+parentDummy.name)
)--end createDummy

-------------------------------------------------------------------------------------------------------------------------
--this is a neater way of getting all children.  alt method to using actionMan.executeAction 0 "40180"
fn selectHierarchy whatGeo =
(
	for p in whatGeo do
	(
		if p.children != undefined do
		(
			selectmore p.children
		)
	)
)--end selectHierarchy

-------------------------------------------------------------------------------------------------------------------------
fn selectGeo =
(
	hideByCategory.none()--unhide everything by category
	unhide (for obj in objects where obj.ishidden collect obj) --unhide all
	
	maxName = filterstring maxFileName "_"
	meshName = maxName[1] + "_skin"
	try
	(
		vehicleMesh = getNodeByName meshName
	) catch (print ("Couldn't find " +meshName))
)--end selectGeo

-------------------------------------------------------------------------------------------------------------------------
fn selectSkel =
(
	hideByCategory.none()--unhide everything by category
	unhide (for obj in objects where obj.ishidden collect obj) --unhide all
	try
	(
		selectHierarchy parentDummy --select the children (and children's children) of the parentdummy
		selectmore parentDummy --add the dummy to the selection
		selectionsets["*vehicleSkel"] = selection --create selection set of everything
		vehicleSkel = getCurrentSelection()
		clearSelection()

		print "vehicle skeleton found"
	) catch (messagebox "Can't Find Parent Dummy") --attempt to select the export dummy
)--end selectSkel

-------------------------------------------------------------------------------------------------------------------------
fn exportAsFBX =
(
	everything = vehicleSkel
	appendIfUnique everything vehicleMesh
	select everything
	
	saveFile = savePath + maxName[1]
    exportFile saveFile #noPrompt selectedOnly:true using:FBXEXP
	
	messageText = maxName[1] + " exported as FBX to " + saveFile + ".fbx"
	messageBox messageText
)--end exportAsFBX

-------------------------------------------------------------------------------------------------------------------------
if $selection.count != 0 and maxFileName != "" then
(
	createDummy()
	selectGeo()
	selectSkel()
	exportAsFBX()
)