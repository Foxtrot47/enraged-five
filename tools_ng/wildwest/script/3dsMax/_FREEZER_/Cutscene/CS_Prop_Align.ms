-- Prop Align Script
-- Stewart Wright
-- January 2010

-- Update 22/02/10 - Added button and message boxes.  Help link added.
-- Update 10/03/10 - Added functionality for linking props to their parent bones.  This allows the max file to better reflect the ingame characters appearance.

-- Work out the project so we don't have to use hardcoded filein paths etc



-- Figure out the project data
theProjectPedFolder = theProjectRoot + "art/peds/"


-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())
	




-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

-- define a couple of arrays so we can keep things neat later on
MasterPropList = #(#("P_HE","P_EY","P_EA","P_MO"),#("P_LH"),#("P_RH"),#("P_LW", "P_LF"),#("P_RW", "P_RF"),#("P_HI"))

propBones = #($Skel_Head,$SKEL_L_Hand,$SKEL_R_Hand,$SKEL_L_Forearm,$SKEL_R_Forearm,$SKEL_Pelvis)

linkBones = #($Skel_Head,$SKEL_L_Hand,$SKEL_R_Hand,$SKEL_L_Forearm,$SKEL_R_Forearm,$SKEL_Pelvis)

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
	
fn AlignProps =
(
-- set the timeline to 0, that should be 'safe'
sliderTime = 0f
-- adding some reset xform stuff to clean up the mesh
select $p_*
allProps = getCurrentSelection()	
for resetObj = 1 to allProps.count do
	(
	resetxform allProps[resetObj]
	maxOps.CollapseNodeTo allProps[resetObj] 1 True
	)	

-- now lets align the array contents to each other
for MatchSel = 1 to MasterPropList.count do
	(
	for PropSel = 1 to MasterPropList[MatchSel].count do
		(
		try
			(
			MatchProp = MasterPropList[MatchSel][PropSel] -- select the prop within the group
			MatchBone = propBones[MatchSel] -- select the matching bone
			selectbywildcard matchprop		
			propsToAlign = getCurrentselection()
			-- aligns the selected props to their matching bone
			for p = 1 to propsToAlign.count do (
				AlignPivotto propsToAlign[p] MatchBone -- aligns the selected props to their matching bone
				)
			) catch()
		)
	)
)


fn LinkProps =
(
-- set the timeline to 0, that should be 'safe'
sliderTime = 0f
select $p_*
-- now lets align the array contents to each other
for MatchSel = 1 to MasterPropList.count do
	(
	for PropSel = 1 to MasterPropList[MatchSel].count do
		(
		try
			(
			MatchProp = MasterPropList[MatchSel][PropSel] -- select the prop within the group
			MatchBone = propBones[MatchSel] -- select the matching bone
			selectbywildcard matchprop		
			propsToLink = getCurrentselection()
			
			for p = 1 to propsToLink.count do (
				-- adds a link constraint
				propsToLink[p].controller = link_constraint()
				propsToLink[p].controller.addTarget MatchBone 0
				propsToLink[p].parent = MatchBone
				)
			) catch()
		)
	)
)


fn unlinkProps =
(
-- set the timeline to 0, that should be 'safe'
sliderTime = 0f	
select $p_*
-- now lets align the array contents to each other
for MatchSel = 1 to MasterPropList.count do
	(
	for PropSel = 1 to MasterPropList[MatchSel].count do
		(
		try
			(
			MatchProp = MasterPropList[MatchSel][PropSel] -- select the prop within the group
			MatchBone = propBones[MatchSel] -- select the matching bone
			selectbywildcard matchprop		
			propsToLink = getCurrentselection()
			
			for p = 1 to propsToLink.count do
				(
				--delete any existing links
				try
					(
					numTargets = propsToLink[p].controller.getNumTargets()
						for delTargets = 1 to numTargets do
						(	
							propsToLink[p].controller.deleteTarget delTargets
							propsToLink[p].parent = undefined
						)
					) catch()	
				)
			) catch()
		)
	)
)


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--create rollout
rollout AutoPropAlign "Auto Prop Align"
	
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Character_Prop_Align" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	--create buttons
	button btnAlignProp "Align Props" width:120 height:40 toolTip:"Automatically align props to their respective bones"
	button btnLinkProp "Link Props" width:120 height:40 toolTip:"Automatically link props to their respective bones using contraints"
	button btnUnlinkProp "Unlink Props" width:120 height:40 toolTip:"Automatically remove any constraint links between props and their respective bones"

	On btnAlignProp pressed do
	(
	select $p_*
	if selection.count == 0 then
		(	
		msgNoProps = "I can't find any props in the scene.  Are they named correctly?"
		messagebox msgNoProps
		)
	else
		(
		AlignProps ()
		msgPropsAligned = "All props should now be correctly aligned."
		messagebox msgPropsAligned
		)
	)
	
	On btnLinkProp pressed do
	(
	select $p_*
	if selection.count == 0 then
		(	
		msgNoProps = "I can't find any props in the scene.  Are they named correctly?"
		messagebox msgNoProps
		)
	else
		(
		unlinkProps ()
		LinkProps ()
		msgPropsLinked = "All props should now be linked."
		messagebox msgPropsLinked
		)
	)
	
	On btnUnlinkProp pressed do
	(
	select $p_*
	if selection.count == 0 then
		(	
		msgNoProps = "I can't find any props in the scene.  Are they named correctly?"
		messagebox msgNoProps
		)
	else
		(
		unlinkProps ()
		msgPropsUnlinked = "All props should now be unlinked in the scene."
		messagebox msgPropsUnlinked
		)
	)

	
	
)--end of rollout "Auto Prop Align"
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--Create the interface
createDialog AutoPropAlign width:130