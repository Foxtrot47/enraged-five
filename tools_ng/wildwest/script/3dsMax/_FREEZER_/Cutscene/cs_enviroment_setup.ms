-- Environment setup, re-worked
-- Dermot Bailie 02/12/10
-- Kyle Hansen 22/10/12
-- NOTES:  I reformatted the script to work with a selection or the old method of using a name.  I also made the script use more standard ways of processing.



	-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())

-- User Notes
-- User must create a "Dummy" at the location they wish the action area to be. The Dummy must be named "Dummy01", or select the root they wish to process

------------------Begin-----------------------
fn csEnvironmentSetupCall=
(
	--Unhide & unfreeze the dummy01 node
	max unhide all
	max unfreeze all
			
	--Find the original scene root, it can be called either Dummy01 or Dummy001
	local sceneRoot = undefined
		
	local selectedObject = (selection as array)

	if selectedObject.count >= 1 then
	(
		sceneRoot = selectedObject
	)

	-- If there is no selection lets look for the old names
	if sceneRoot == undefined then
	(
		sceneRoot = $Dummy01
	)
	-- if we cant find the correct name, look for an numerated name
	if sceneRoot == undefined then
	(
		sceneRoot = $Dummy001
	)
	
	-- If there is no selection lets look for the old names
	if sceneRoot == undefined then
	(
		sceneRoot = $Point01
	)
	-- if we cant find the correct name, look for an numerated name
	if sceneRoot == undefined then
	(
		sceneRoot = $Point001
	)
	
	-- If we have found a selected node or found a hard coded node, continue!
	if sceneRoot != undefined then
	(
		
		sceneRoot.name = "PointOrig"
		
		local sceneRootCopy = undefined
		maxOps.cloneNodes sceneRoot cloneType:#copy newNodes:&sceneRootCopy
		
		-- create the new scene root'
		local newSceneRoot = sceneRootCopy[1]
		newSceneRoot.name = "SetRoot"
		
		-- get all of the meshes in the scene and parent them to the new root node
		local allMeshes = for obj in objects where superclassof obj == GeometryClass collect obj
		allMeshes.parent = newSceneRoot
		
		-- Create a new point to act as the offset point
		local newSnapPoint = Point pos:[0,0,0] isSelected:on
		newSnapPoint.name = "PointZerod2"
		
		newSceneRoot.pos = newSnapPoint.pos
		-- parent everthing under this node
		newSnapPoint.parent = newSceneRoot
		sceneRoot.parent = newSceneRoot
		
		-- lets get the scene offset
		local offsetPosition = sceneRoot[1].pos 
		local Positionfilter = (offsetPosition as string)
		local Position_offset_values = filterstring Positionfilter "[ ]"
		local PointOrigAngle = sceneRoot[1].rotation as eulerAngles
		local offsetRotation= PointOrigAngle.Z
		local Rotation_offset_values = (offsetRotation as string )
		
		--Time & user stamp 
		local timedate = localtime 
		local Updatedby = sysInfo.username
		local Filename = maxFileName as string 
		local Setname = filterString Filename "." 
		
		-- create an offset co-ordinate file 
		makeDir @"X:\gta5\art\animation\resources\sets\Offsets" 
		local output_name =  ("X:\\gta5\\art\\animation\\resources\\sets\\Offsets\\"+ Setname[1] +".log")
		local outputFile = createfile output_name
			format (("<<<<<< " +Setname[1] +" >>>>>>" )+"\n") to:outputFile
			format (("Last updated by " + Updatedby + " on the " + timedate)+"\n") to:outputFile
			format (("" )+"\n") to:outputFile
			format (("Position Offset = " + Position_offset_values[1] )+"\n") to:outputFile
			format (("Rotation Offset = " + Rotation_offset_values )+"\n") to:outputFile
		close outputFile
	)
	else
	(
			messageBox "Could not find the root node of the scene, please select the root of the scene, or name the root 'Dummy01' or 'Dummy001'"
	)
)

csEnvironmentSetupCall()