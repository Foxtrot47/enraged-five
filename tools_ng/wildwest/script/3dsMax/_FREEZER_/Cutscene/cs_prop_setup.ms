--CS Prop Setup
--Stewart Wright
--26/07/10
-------------------------------------------------------------------------------------------------------------------------
--Tool for creating movers and dummys on selected props and exporting the lot.
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())

-- Figure out the project data
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()




prop = undefined

-------------------------------------------------------------------------------------------------------------------------
rollout CS_Prop_Setup "Cutscene Prop Setup"
(
	button btnPrepProp "Prepare Prop" width:100 height:50
	
	on btnPrepProp pressed do
	(
		if  selection.count != 1 then
			(
			messagebox "I can only cope with 1 thing at a time!"
			return 0
			)
		else
			(
			prop = getCurrentSelection()	
			BuildRigRootSystem prop[1]
			
			--hacky select children method.  needs better way				
			select prop
			
			actionMan.executeAction 0 "40193"  -- Selection: Select Ancestor.  Should be mover
			actionMan.executeAction 0 "40193"  -- Selection: Select Ancestor.  Should be Dummy01

			actionMan.executeAction 0 "40180"  --select all children and main object
			print "Full hierarchy selected"

			--hacky export selected as method.  needs better way.
			actionMan.executeAction 0 "40373"  -- File: Export Selected
			)
	)
)--end CS_Prop_Setup
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
createDialog CS_Prop_Setup