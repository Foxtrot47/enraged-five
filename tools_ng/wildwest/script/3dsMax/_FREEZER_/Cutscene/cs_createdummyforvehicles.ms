-- script to create dummy for cutscene vehicles
--Matt Rennie
--April 2010

--moved to ww2 Stewart Wright May 2012

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())
	
maxName = filterstring maxFileName "_" --this should get the prefix of vehicle name which is always vehiclename_skinned.max

print maxName

chassisBoneName = (maxName[1]+"_Skel")
-- chassisBoneName = "jet2"

parentDummy = dummy pos:[0,0,0]

parentDummy.name = "Dummy01"

skel = getNodeByName chassisBoneName
skel.parent = parentDummy

print (chassisBoneName+" parented to "+parentDummy.name)