--Mover Replacer
--Stewart Wright
--26/07/10
-------------------------------------------------------------------------------------------------------------------------
--Tool for updating a selected mover
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- load the common functions
filein "rockstar/export/settings.ms"

-- Figure out the project data
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())
-------------------------------------------------------------------------------------------------------------------------

fn replaceMover =
(	
	oldMover = $
	oldMoverName = oldMover.name
	oldMover.name = "DeleteMe"
	actionMan.executeAction 0 "40194"  -- Selection: Select Child
	oldMoverChild = getcurrentselection()
	select oldMover
	actionMan.executeAction 0 "40193"  -- Selection: Select Ancestor
	tempParent = getcurrentselection()
	if tempParent[1] != oldMover then
	(
		oldMoverParent = getcurrentselection()
	) else (oldMoverParent = undefined)
	
	--merge in the gizmo thing as mover
	print "Merging mover gizmo"
	gizmoFile = theWildWest + "assets/max_files/gnomon_mover_mesh.max"
	mergeObj = #("Mover")--what do we want to merge?
	try (mergeMaxFile gizmoFile mergeObj #deleteOldDups #useSceneMtlDups #alwaysReparent quiet:true) catch()
	newMover = $Mover
	print "New Mover merged"
	
	--align the new mover to the old one
	resetxform newMover
	maxOps.CollapseNodeTo newMover 1 True
	--oldCenter = oldMover.center --find the center of the old mover (should this be the pivot?)
	--newMover.center = oldCenter --align the new mover to the old
	AlignPivotto newMover oldMover-- aligns pivot of the new mover to the old one
	print "New mover algined to old mover"
	
	--delete the old mover
	delete oldMover
	print "Old mover deleted"
	--rename the new mover
	newMover.name = oldMoverName
	print "New mover renamed"
	
	--link stuff prop > mover > dummy
	newMover.parent = oldMoverParent[1]
	oldMoverChild[1].parent = newMover
	
	--set some GTA Attributes
	setAttribute newMover "Dont Export" true
	setAttribute newMover "Dont Add To IPL" true
)--end replaceMover

-------------------------------------------------------------------------------------------------------------------------
rollout moverUpdate "Mover Updater"
(
	button btnUpdateMover "Update Mover" width:100 height:50
	
	on btnUpdateMover pressed do
	(
		if  selection.count == 1 then
			(
				replaceMover ()
			)
		else
			(
			if selection.count == 0 then
				(				
				messagebox "Please select the mover you want to replace."
				return 0
				)
			if selection.count > 1 then
				(
					messagebox "1 mover at a time please"
					return 0
				)
			)
	)
)--end moverUpdate
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
createDialog moverUpdate