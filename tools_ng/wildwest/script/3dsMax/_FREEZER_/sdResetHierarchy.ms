--///////////////////////////////////////////////////////////////////////////////////////////////////////////
--
--	      :!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	  :!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	 '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Copyright (c) Rockstar, San Diego
--	!!!!!!!!!!!!!!!RRRRRRRRRRRR!!!!!!!!!!!!!!!!  A division of Rockstar Games Inc. & Take Two Interactive
--	!!!!!!!!!!!!!!!R           RR!!!!!!!!!!!!!!	
--	!!!!!!!!!!!!!!!R             R!!!!!!!!!!!!!
--	!!!!!!!!!!!!!!R    RRRRRR    R!!!!!!!!!!!!!  sdResetHierarchy.ms 
--	!!!!!!!!!!!!!!R    RRRRRR    R!!!!!!!!!!!!!
--	!!!!!!!!!!!!!!R    ````    .R!!!!!!!!!!!!!!  Version: Max 2009
--	!!!!!!!!!!!!!R            RR!!!!!!!!!!!!!!!
--	!!!!!!!!!!!!!R    RRRRR    R!!!!!!!!!!!!!!!
--	!!!!!!!!!!!!R    R!!!!R    R!!! !!!!!!!!!!!
--	!!!!!!!!!!!!R    R!!!!R    R!! *!!!!!!!!!!!  ROCKSTAR SAN DIEGO DISCLAIMS ALL WARRANTIES WITH REGARD
--	!!!!!!!!!!!!R....R!!!!R.   RX **'!!!!!!!!!!  TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF 
--	!!!!!!!!!!!!RRRRRR!!!!RRRRRR.***.... .!!!!!  MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL ROCKSTAR
--	!!!!!!!!!!!!!!!!!!!!!!!!! ********":!!!!!!!  SAN DIEGO BE LIABLE FOR ANY SPECIAL, INDIRECT OR 
--	!!!!!!!!!!!!!!!!!!!!!!!!!! ****** !!!!!!!!!  CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
--	!!!!!!!!!!!!!!!!!!!!!!!!! *** ***'!!!!!!!!!  FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
--	!!!!!!!!!!!!!!!!!!!!!!!!! *:!!!.* !!!!!!!!!  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
--	!!!!!!!!!!!!!!!!!!!!!!!!.!!!!!!!!:!!!!!!!!!  OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF 
--	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  THIS SOFTWARE.
--	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	    `~!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!~~	     MODIFY AT YOUR OWN RISK!
--
--///////////////////////////////////////////////////////////////////////////////////////////////////////////

fn sdResetHierarchy =
(
	--gather selected objects and loop through them
	selObjs = selection
	for selObj in selObjs do
	(
		--initilaize holding array and incrementer
		childHolder = #()
		i = 1
		
		--pass all the child names into holding array
		for childNode in  selObj.children do
		(
			childHolder[i] = childNode
			i += 1
		)

		--iterate through the names in the holder and unlink the children temporarily
		for childName in childHolder do
		(
			childName.parent = undefined
		)
		
		--unparent the object reset the transforms collapse stack and reparent
		parentHolder = selObj.parent
		selObj.parent = undefined
		resetXform selObj
		collapseStack selObj
		selObj.parent = parentHolder

		--go through the child nodes in the holder and reparent them to the object
		for reparentNode in childHolder do
		(
		reparentNode.parent = selObj
		)
	)
)

sdResetHierarchy()