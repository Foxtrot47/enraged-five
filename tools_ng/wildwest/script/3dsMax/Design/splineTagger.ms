--/////////////////////////////////////////
--	UI
--/////////////////////////////////////////
try(destroyDialog SplineMarkupUI)catch()
rollout SplineMarkupUI "Spline Tagger" width:200 height:140
(
	--/////////////////////////////////////////
	--	VARIABLES
	--/////////////////////////////////////////
	local propertyMap = #("inTunnel")
	local selectedFilter = "inTunnel"
	
	--/////////////////////////////////////////
	--	CONTROLS
	--/////////////////////////////////////////
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:SplineMarkupUI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"CHANGEME" filename:(getThisScriptFilename())
	
	group "Filter"
	(
		checkbox chkInTunnel "In Tunnel" tooltip:"inTunnel" checked:true
	)
		
	button btnSelectTagged "Select Tagged" width:(SplineMarkupUI.width - 20)
	button btnTagSelected "Tag Selected" width:(SplineMarkupUI.width - 20)
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////
	
	
	--/////////////////////////////////////////
	--	EVENTS
	--/////////////////////////////////////////
		
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on checkbox changed active do
	(
		if active then
		(
			selectedFilter = "inTunnel"
		)
		else
		(
			selectedFilter = undefined
		)
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btnSelectTagged pressed do
	(
		if selection.count == 0 do
		(
			messageBox "Nothing Selected" title:"Bad Selection"
			return false
		)
		
		--get first selected spline
		local splineSel = if (isKindOf selection[1] line) then splineSel = selection[1] else (messageBox "Not a Spline" title:"Bad Selection"; return false)
		--print "spline"
		
		--select point info based on filter
		case selectedFilter of
		(
			"inTunnel":
			(
				local userData = getUserProp splineSel "inTunnel"
				if userData == undefined then
				(
					messageBox "No inTunnel Data Defined" title:"Missing Data"
					return false
				)
				else
				(
					local bits = filterString userData ","
					local indices = for num in bits collect num as Integer
					
					subobjectLevel = 1
					setKnotSelection splineSel 1 indices
				)
			)
			default:messageBox "No Filter Selected" title:"Filter Selection"
		)
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btnTagSelected pressed do
	(
		if selection.count == 0 do
		(
			messageBox "Nothing Selected" title:"Bad Selection"
			return false
		)
		
		--get first selected spline
		local splineSel = if (isKindOf selection[1] line) then splineSel = selection[1] else (messageBox "Not a Spline" title:"Bad Selection"; return false)
		
		if (subobjectLevel != 1) do subobjectLevel = 1
		
		local knotSel = getKnotSelection splineSel 1
		
		if knotSel.count == 0 do
		(
			messageBox "No points selected" title:"Bad Selection"
			return false
		)
		
		local userData = stringStream ""
		
		for i=1 to (knotSel.count - 1) do
		(
			format "%," knotSel[i] to:userData
		)
		format "%" knotSel[knotSel.count] to:userData
		
		--to string for writing
		userData = userData as String
		
		case selectedFilter of
		(
			"inTunnel":
			(
				setUserProp splineSel "inTunnel" userData
			)
		)
		
		messageBox "Done"
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on SplineMarkupUI open do
	(
		banner.setup()
	)
	
	
)
createDialog SplineMarkupUI style:#(#style_titlebar, #style_border, #style_sysmenu, #style_minimizebox)
