filein (RsConfigGetWildWestDir() + "script/3dsmax/_config_files/wildwest_header.ms")

clearListener()

fn loadRigPositions dataArray = 
(
	--this will loop through all the items inside 'dataArray' and try to set the object transform to the one stored.
	
	for i = 1 to dataArray.count do 
	(
		thisData = dataArray[i]
		
		format ((thisData as string)+"\n")
		
		obj = getNodeByName thisData[1] --try to find the object
		objPos = thisData[2]
		
		if obj != undefined then
		(
			in coordsys parent obj.transform = objPos
		)
		else
		(
			format ("WARNING! We couldn't find an object called "+thisData[1]+"\n")
		)
	)
)

fn inputDataFromFile input_name = 
(
	local inputData = #() -- define as array
	
	if input_name != undefined then
	(
		fileData = openfile input_name
		
		while not eof fileData do
		(
			dataWeHaveRead =  (filterstring (readLine fileData) "\n")
			
-- 			format ("thisData : "+(thisData )+"\n")
			
			--dataWeHaveRead = execute thisData
			
			append inputData dataWeHaveRead
		)
		close fileData
	)
	-- 				print ("BoneList = " +(boneList as String))	
	
	return inputData
)



fn outputDataToFile dataToOutput = 
(
	if dataToOutput.count >= 1 then
	(
		local output_name = getSaveFileName caption:"Output file" types:"(*.*)|*.*|All Files (*.*)|*.*|"
		
		if output_name != undefined then 
		(
			output_file = createfile output_name		

			for i = 1 to dataToOutput.count do
			(
				theDataToSave = dataToOutput[i]
				
				--format ((theDataToSave as string)+"\n")
				
-- 				format ((theDataToSave as string)+","+"\n") to:output_file
				format ((theDataToSave)) to:output_file
			)
			
			close output_file
				--edit output_name --opens the file in a maxscript window so it can be checked.
			
		)--end if		
		else
		(
			FORMAT "Please select an output file.\n"
		)
	)
	else
	(
		format ("The data array to save was empty.\n")
	)
)

fn queryRigPositions selectedObjects = 
(
	--this will loop through all of the objects in the sleectedObjects array and store thier name and their transform
	thisData = #()
	
	for obj in selectedObjects do 
	(
		local objPos = in coordsys parent obj.transform
			
		tmpData = #(obj.name, objPos)
		
		append thisData tmpData
	)
-- 	return thisData
	
-- 	outputDataToFile thisData
	
	for i = 1 to thisData.count do 
	(
		format ((thisData[i] as string)+","+"\n")
	)
)

-- selectedObjects = selection as array

-- myDataArray = queryRigPositions selectedObjects  --RUN THIS TO STORE THE TRANSFORM DATA


--myDataICopiedFromTheListener_A = #(#("SeatBone", (matrix3 [1,0,0] [0,0,1] [0,-1,0] [2.0103,1.68012e-006,-30.237])), #("SeatBone2", (matrix3 [1,0,0] [0,0,1] [0,-1,0] [-28.5715,1.42931e-006,-24.4991])), #("WheelBone", (matrix3 [1,0,0] [0,0,1] [0,-1,0] [28.733,1.2158e-006,-19.6145])))
-- 	myDataForLAYOUT_STANDARD = #(#("Modular_Root", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0,0,0])), #("Modular_Seat01", (matrix3 [-1.64842e-007,-0.998833,0.0483064] [0,0.0483064,0.998833] [-1,1.62921e-007,0] [-0.449336,0.159701,0.346294])), #("Modular_SeatBack01", (matrix3 [-1.4689e-007,-0.968457,-0.249181] [0,-0.249181,0.968457] [-1,1.6319e-007,0] [-0.449335,-0.195698,0.377563])), #("Modular_SeatHeadRest01", (matrix3 [-1.4689e-007,-0.968457,-0.249181] [0,-0.249181,0.968457] [-1,1.6319e-007,0] [-0.449335,-0.374414,1.0374])), #("Modular_Wheel", (matrix3 [1,-3.00599e-007,1.8139e-007] [3.41566e-007,0.952484,-0.304588] [0,0.304588,0.952484] [-0.438351,0.397901,0.869141])), #("Modular_Pedal02", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.399468,1.00882,0.394039])), #("Modular_Pedal01", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.518364,1.00882,0.394039])), #("Modular_Pedal03", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.265679,0.978542,0.337886])), #("Modular_Floor", (matrix3 [-1.46995e-007,-0.90225,0] [0,0,0.298777] [-1,1.62921e-007,0] [2.98972e-007,0.0467814,0.241272])), #("Modular_Floor_Left", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.879125,0.00643624,0.282737])), #("Modular_Floor_Right", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0.879125,0.00643624,0.282737])), #("Modular_Left_Side", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.737138,-0.244037,0.836364])), #("Modular_Right_Side", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0.737138,-0.244037,0.836364])), #("Modular_Left_Side_Front", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.839296,0.970533,0.575758])), #("Modular_Right_Side_Front", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0.839296,0.970533,0.575758])), #("Modular_Ceiling", (matrix3 [-1.63217e-007,-0.999976,0.00687497] [0,0.00687497,0.999976] [-1,1.62921e-007,0] [0,-0.450567,1.35691])), #("Modular_Ceiling_Left", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.570917,-0.397431,1.37219])), #("Modular_Ceiling_Right", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0.570917,-0.397431,1.37219])), #("Modular_Ceiling_Front", (matrix3 [-1.78748e-007,-1,0] [0,0,1] [-1.09715,1.62921e-007,0] [0,0.36559,1.35184])), #("Modular_FrontGlass_Left", (matrix3 [-1.65135e-007,-0.998394,0.0566505] [0,0.0566505,0.998394] [-1,1.62921e-007,0] [-0.679298,0.697884,1.14456])), ...)
-- 	myDataForLAYOUT_LOW = #(#("Modular_Root", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0,0,0])), #("Modular_Seat01", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.421932,-0.0598158,0.268636])), #("Modular_SeatBack01", (matrix3 [-1.41776e-007,-0.952184,-0.305524] [0,-0.305524,0.952184] [-1,1.6325e-007,0] [-0.421932,-0.307942,0.30174])), #("Modular_SeatHeadRest01", (matrix3 [-1.52046e-007,-0.982464,-0.186453] [0,-0.186453,0.982464] [-1,1.6325e-007,0] [-0.421932,-0.503459,0.806497])), #("Modular_Wheel", (matrix3 [0.936571,-2.81533e-007,1.69885e-007] [3.199e-007,0.892069,-0.285269] [0,0.285269,0.892069] [-0.414739,0.241986,0.729853])), #("Modular_Pedal02", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.381068,0.855877,0.243282])), #("Modular_Pedal01", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.499965,0.855877,0.243282])), #("Modular_Pedal03", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.247279,0.825603,0.187129])))
-- 	myDataForLAYOUT_VAN = #(#("Modular_Root", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0,0,0])), #("Modular_Seat01", (matrix3 [-1.57193e-007,-0.994055,-0.108883] [0,-0.108883,0.994055] [-1,1.62921e-007,0] [-0.634069,0.888583,0.781961])), #("Modular_SeatBack01", (matrix3 [-1.48391e-007,-0.972878,-0.231317] [0,-0.231317,0.972878] [-1,1.6317e-007,0] [-0.634069,0.674822,0.796529])), #("Modular_SeatHeadRest01", (matrix3 [-1.48391e-007,-0.972878,-0.231317] [0,-0.231317,0.972878] [-1,1.6317e-007,0] [-0.634069,0.473337,1.51477])), #("Modular_Wheel", (matrix3 [1,-3.00599e-007,1.8139e-007] [3.37728e-007,0.964781,-0.263054] [0,0.263054,0.964781] [-0.629216,1.18223,1.27045])), #("Modular_Pedal02", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.500263,1.53008,0.509821])), #("Modular_Pedal01", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.61916,1.53008,0.509821])), #("Modular_Pedal03", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.366474,1.49981,0.453668])))
-- 	myDataForLAYOUT_TRUCK = #(#("Modular_Root", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0,0,0])), #("Modular_Seat01", (matrix3 [-1.58844e-007,-0.996713,-0.0810185] [0,-0.0810185,0.996713] [-1,1.62921e-007,0] [-0.595447,1.73832,1.79242])), #("Modular_SeatBack01", (matrix3 [-1.50505e-007,-0.978777,-0.204929] [0,-0.204929,0.978777] [-1,1.63142e-007,0] [-0.595447,1.4524,1.79779])), #("Modular_SeatHeadRest01", (matrix3 [-1.50505e-007,-0.978777,-0.204929] [0,-0.204929,0.978777] [-1,1.63142e-007,0] [-0.595447,1.31539,2.41729])), #("Modular_Wheel", (matrix3 [1.16728,-3.50885e-007,2.11734e-007] [4.07613e-007,1.05652,-0.496297] [0,0.496297,1.05652] [-0.600888,2.10883,2.21001])), #("Modular_Pedal02", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.526835,2.48043,1.51078])), #("Modular_Pedal01", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.645732,2.48043,1.51078])), #("Modular_Pedal03", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.393046,2.45016,1.45463])))

myDataForLAYOUT_STANDARD = #(
		#("Modular_Root", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0,0,0])),
		#("Modular_Seat01", (matrix3 [-1.64611e-007,-0.998833,0.0483064] [0,0.0483064,0.998833] [-1,1.61352e-007,0] [-0.449336,0.159701,0.346294])),
		#("Modular_SeatBack01", (matrix3 [-1.41561e-007,-0.968457,-0.249181] [0,-0.249181,0.968457] [-1,2.01166e-007,-1.3411e-007] [-0.449336,-0.195698,0.377563])),
		#("Modular_SeatHeadRest01", (matrix3 [-1.41561e-007,-0.968457,-0.249181] [0,-0.249181,0.968457] [-1,2.01166e-007,-1.3411e-007] [-0.449335,-0.374414,1.0374])),
		#("Modular_Wheel", (matrix3 [0.995434,-2.69691e-007,2.5332e-007] [3.27826e-007,0.892069,-0.303198] [0,0.285269,0.948135] [-0.438061,0.394732,0.870894])),
		#("Modular_Pedal02", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.381068,0.855877,0.362754])),
		#("Modular_Pedal01", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.499965,0.855877,0.362754])),
		#("Modular_Pedal03", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.247279,0.825603,0.306601])),
		#("Modular_Floor", (matrix3 [-1.46995e-007,-0.90225,0] [0,0,0.298777] [-1,1.62921e-007,0] [2.98972e-007,0.0467814,0.241272])),
		#("Modular_Floor_Left", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.879125,0.00643622,0.282737])),
		#("Modular_Floor_Right", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0.879125,0.00643622,0.282737])),
		#("Modular_Left_Side", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.737138,-0.244037,0.836364])),
		#("Modular_Right_Side", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0.737138,-0.244037,0.836364])),
		#("Modular_Left_Side_Front", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.839296,0.970533,0.575758])),
		#("Modular_Right_Side_Front", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0.839296,0.970533,0.575758])),
		#("Modular_Ceiling", (matrix3 [-1.6304e-007,-0.999976,0.00687492] [0,0.00687492,0.999976] [-1,1.62167e-007,0] [0,-0.450567,1.35691])),
		#("Modular_Ceiling_Left", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.570917,-0.397431,1.37219])),
		#("Modular_Ceiling_Right", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0.570917,-0.397431,1.37219])),
		#("Modular_Ceiling_Front", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1.09715,1.78748e-007,0] [0,0.36559,1.35184])),
		#("Modular_FrontGlass_Left", (matrix3 [-1.71363e-007,-0.998394,0.0566503] [0,0.0566503,0.998394] [-1,1.56462e-007,0] [-0.679298,0.697884,1.14456])),
		#("Modular_FrontGlass_Right", (matrix3 [-1.71363e-007,-0.998394,0.0566503] [0,0.0566503,0.998394] [-1,1.60187e-007,0] [0.679298,0.697884,1.14456])),
		#("Modular_Seat02", (matrix3 [-1.6531e-007,-0.998833,0.0483064] [0,0.0483064,0.998833] [-1,1.59955e-007,0] [0.449336,0.159701,0.346294])),
		#("Modular_SeatBack02", (matrix3 [-1.3411e-007,-0.968457,-0.249181] [0,-0.249181,0.968457] [-1,1.71363e-007,0] [0.449336,-0.195698,0.377563])),
		#("Modular_SeatHeadRest02", (matrix3 [-1.28523e-007,-0.968457,-0.249181] [0,-0.249181,0.968457] [-1,1.75089e-007,0] [0.449335,-0.374414,1.0374])),
		#("Modular_Seat03", (matrix3 [-1.62564e-007,-0.997791,0.0664239] [0,0.0664239,0.997791] [-1.18175,1.89111e-007,0] [0,-0.865991,0.549081])),
		#("Modular_SeatBack03", (matrix3 [-1.52737e-007,-0.913665,-0.406468] [-1.78814e-007,-0.406468,0.913665] [-1.18175,1.71363e-007,0] [-1.93122e-007,-1.10368,0.566351])),
		#("Modular_SeatHeadRest03", (matrix3 [-2.03028e-007,-0.957878,0.287175] [0,0.287175,0.957878] [-1.18175,1.51806e-007,0] [-2.64814e-007,-1.35854,0.991302]))	
	)

	myDataForLAYOUT_LOW = #(
		#("Modular_Root", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0,0,0])),
		#("Modular_Seat01", (matrix3 [-1.65e-007,-0.998607,0.0527589] [0,0.0527589,0.998607] [-1,1.62921e-007,0] [-0.42211,-0.116512,0.224177])),
		#("Modular_SeatBack01", (matrix3 [0,-0.853335,-0.521363] [-1.23321e-007,-0.521363,0.853335] [-1,1.63483e-007,0] [-0.42211,-0.18758,0.270326])),
		#("Modular_SeatHeadRest01", (matrix3 [0,-0.704938,-0.709269] [-1.47417e-007,-0.709269,0.704938] [-1,1.63483e-007,0] [-0.42211,-0.443337,0.827352])),
		#("Modular_Wheel", (matrix3 [0.932134,-2.80199e-007,1.6908e-007] [3.42713e-007,0.947943,-0.31844] [0,0.296829,0.88361] [-0.415065,0.240927,0.73116])),
		#("Modular_Pedal02", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.396207,0.830495,0.28174])),
		#("Modular_Pedal01", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.515104,0.830495,0.28174])),
		#("Modular_Pedal03", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.262418,0.800222,0.225588])),
		#("Modular_Floor", (matrix3 [-1.45228e-007,-0.891403,0] [0,0,0.398894] [-1.08429,1.76654e-007,0] [2.98972e-007,-0.0660103,0.135146])),
		#("Modular_Floor_Left", (matrix3 [0.0108933,-0.999941,0] [0,0,1] [-0.999941,-0.0108933,0] [-0.901385,-0.106129,0.194261])),
		#("Modular_Floor_Right", (matrix3 [-0.0278259,-0.999613,0] [0,0,1] [-0.999613,0.0278259,0] [0.901385,-0.106129,0.194262])),
		#("Modular_Left_Side", (matrix3 [-1.62921e-007,-1,0] [0,0,0.865527] [-0.771356,1.2567e-007,0] [-0.754487,-0.543856,0.624287])),
		#("Modular_Right_Side", (matrix3 [-1.62921e-007,-1,0] [0,0,0.865527] [-0.771356,1.2567e-007,0] [0.754487,-0.543856,0.624287])),
		#("Modular_Left_Side_Front", (matrix3 [0.122446,-0.992456,-0.00612143] [0.0499308,0,0.998753] [-0.991218,-0.122599,0.0495541] [-0.854974,0.74615,0.483488])),
		#("Modular_Right_Side_Front", (matrix3 [-0.069018,-0.997517,-0.0140037] [-0.0624524,-0.0096894,0.998001] [-0.995659,0.0697546,-0.0616286] [0.854974,0.74615,0.483488])),
		#("Modular_Ceiling", (matrix3 [-1.61959e-007,-0.999776,-0.021916] [0,-0.0211548,1.03575] [-1,1.62921e-007,0] [0,-0.445994,1.0627])),
		#("Modular_Ceiling_Left", (matrix3 [-1.62172e-007,-0.999862,-0.0166049] [0,-0.0166049,0.999862] [-1,1.62921e-007,0] [-0.630161,-0.387678,1.07415])),
		#("Modular_Ceiling_Right", (matrix3 [-1.62172e-007,-0.999862,-0.0166049] [0,-0.0166049,0.999862] [-1,1.62921e-007,0] [0.630161,-0.387678,1.07415])),
		#("Modular_Ceiling_Front", (matrix3 [-1.93938e-007,-1,0] [0,0,1] [-1.19039,1.62921e-007,0] [0,0.307501,1.08459])),
		#("Modular_FrontGlass_Left", (matrix3 [0.0357253,-0.999025,-0.0259271] [0.000926332,-0.0259106,0.999664] [-0.999361,-0.0357373,-2.34954e-007] [-0.714838,0.627756,0.9252])),
		#("Modular_FrontGlass_Right", (matrix3 [-0.0393324,-0.99889,-0.0259271] [-0.00102015,-0.0259068,0.999664] [-0.999226,0.0393457,0] [0.714838,0.627756,0.9252])),
		#("Modular_Seat02", (matrix3 [-1.65e-007,-0.998607,0.0527589] [0,0.0527589,0.998607] [-1,1.62921e-007,0] [0.42211,-0.116512,0.224177])),
		#("Modular_SeatBack02", (matrix3 [0,-0.853335,-0.521363] [-1.23321e-007,-0.521363,0.853335] [-1,1.63483e-007,0] [0.42211,-0.18758,0.270326])),
		#("Modular_SeatHeadRest02", (matrix3 [0,-0.704938,-0.709269] [-1.47417e-007,-0.709269,0.704938] [-1,1.63483e-007,0] [0.42211,-0.443337,0.827352])),
		#("Modular_Seat03", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0,-4.87452,2.6766])),
		#("Modular_SeatBack03", (matrix3 [-1.38785e-007,-0.941943,-0.335773] [0,-0.335773,0.941943] [-1,1.63773e-007,0] [-1.6369e-007,-5.11282,2.67804])),
		#("Modular_SeatHeadRest03", (matrix3 [-1.69402e-007,-0.933251,0.359224] [0,0.359224,0.933251] [-1,1.5998e-007,0] [-2.18687e-007,-5.30207,3.19872]))
	)
	
	myDataForLAYOUT_VAN = #(
		#("Modular_Root", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0,0,0])),
		#("Modular_Seat01", (matrix3 [-1.57072e-007,-0.993839,-0.110833] [0,-0.110833,0.993839] [-1,1.62921e-007,0] [-0.641645,0.910442,0.808428])),
		#("Modular_SeatBack01", (matrix3 [-1.45234e-007,-0.963382,-0.268133] [0,-0.268133,0.963382] [-1,1.6321e-007,0] [-0.641645,0.701563,0.823877])),
		#("Modular_SeatHeadRest01", (matrix3 [-1.5352e-007,-0.986133,-0.165956] [0,-0.165956,0.986133] [-1,1.6321e-007,0] [-0.641645,0.47149,1.54788])),
		#("Modular_Wheel", (matrix3 [1.01396,-3.00599e-007,1.83907e-007] [3.42671e-007,0.964122,-0.269067] [0,0.265462,0.977608] [-0.628501,1.18169,1.26935])),
		#("Modular_Pedal02", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.529834,1.54607,0.621692])),
		#("Modular_Pedal01", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.648731,1.54607,0.621692])),
		#("Modular_Pedal03", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.396045,1.5158,0.56554])),
		#("Modular_Floor", (matrix3 [-2.06241e-007,-1.2659,0] [0,0,0.261264] [-1.28709,2.09694e-007,0] [2.98972e-007,0.067864,0.282663])),
		#("Modular_Floor_Left", (matrix3 [-1.62921e-007,-1.4533,0] [-0.191124,0,0.981566] [-0.981566,2.32408e-007,-0.191124] [-1.03017,-0.00301629,0.341468])),
		#("Modular_Floor_Right", (matrix3 [-2.36772e-007,-1.4533,0] [0.363736,0,0.931502] [-0.931502,1.51761e-007,0.363736] [1.03017,-0.00301628,0.341468])),
		#("Modular_Left_Side", (matrix3 [-1.62921e-007,-1,0] [0,0,1.5481] [-0.743546,1.21139e-007,0] [-0.979981,0.488752,1.14619])),
		#("Modular_Right_Side", (matrix3 [-1.62921e-007,-1,0] [0,0,1.5481] [-0.743546,1.21139e-007,0] [0.979981,0.488752,1.14619])),
		#("Modular_Left_Side_Front", (matrix3 [0,-1,0] [0.0728581,0,0.888071] [-0.49512,1.62706e-007,0.0322067] [-1.04043,1.68342,1.0618])),
		#("Modular_Right_Side_Front", (matrix3 [-1.62921e-007,-1,0] [-0.0622831,0,0.887106] [-0.499188,0,0.0115884] [1.04043,1.68342,1.0618])),
		#("Modular_Ceiling", (matrix3 [-3.75438e-007,-2.30442,0] [0,0,1] [-1.44906,2.36081e-007,0] [0,-0.818925,1.96712])),
		#("Modular_Ceiling_Left", (matrix3 [-3.0785e-007,-1.88957,0] [0,0,1] [-1,1.62921e-007,0] [-0.833679,-0.368916,1.98507])),
		#("Modular_Ceiling_Right", (matrix3 [-3.0785e-007,-1.88957,0] [0,0,1] [-1,1.62921e-007,0] [0.833679,-0.368916,1.98507])),
		#("Modular_Ceiling_Front", (matrix3 [-2.64077e-007,-0.98687,0.161513] [0,0.161513,0.98687] [-1.58985,1.68024e-007,0] [0,1.04613,1.96445])),
		#("Modular_FrontGlass_Left", (matrix3 [-1.6862e-007,-1.06733,0.25533] [0,0.25533,1.06733] [-1,1.78796e-007,0] [-0.923532,1.44343,1.6384])),
		#("Modular_FrontGlass_Right", (matrix3 [-1.8505e-007,-1.06733,0.25533] [0,0.25533,1.06733] [-1,1.62921e-007,0] [0.923532,1.44343,1.6384])),
		#("Modular_Seat02", (matrix3 [-1.57072e-007,-0.993839,-0.110833] [0,-0.110833,0.993839] [-1,1.62921e-007,0] [0.641645,0.910442,0.808428])),
		#("Modular_SeatBack02", (matrix3 [0,-0.963382,-0.268133] [0,-0.268133,0.963382] [-1,0,0] [0.641645,0.701563,0.823877])),
		#("Modular_SeatHeadRest02", (matrix3 [0,-0.986133,-0.165956] [0,-0.165956,0.986133] [-1,0,0] [0.641645,0.47149,1.54788])),
		#("Modular_Seat03", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0,-5.16004,3.1704])),
		#("Modular_SeatBack03", (matrix3 [-1.38785e-007,-0.941943,-0.335773] [0,-0.335773,0.941943] [-1,1.63773e-007,0] [-1.6369e-007,-5.39835,3.17185])),
		#("Modular_SeatHeadRest03", (matrix3 [-1.69402e-007,-0.933251,0.359224] [0,0.359224,0.933251] [-1,1.5998e-007,0] [-2.18687e-007,-5.58759,3.69252]))
	)
	
	myDataForLAYOUT_TRUCK = #(
		#("Modular_Root", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0,0,0])),
		#("Modular_Seat01", (matrix3 [-1.58244e-007,-0.995815,-0.0913947] [0,-0.0913947,0.995815] [-1,1.62921e-007,0] [-0.594399,1.79113,1.8177])),
		#("Modular_SeatBack01", (matrix3 [-1.41436e-007,-0.951045,-0.309053] [0,-0.309053,0.951045] [-1,1.63254e-007,0] [-0.594399,1.54102,1.82799])),
		#("Modular_SeatHeadRest01", (matrix3 [-1.41436e-007,-0.951045,-0.309053] [0,-0.309053,0.951045] [-1,1.63254e-007,0] [-0.594399,1.33521,2.44097])),
		#("Modular_Wheel", (matrix3 [1.16182,-3.12924e-007,3.57628e-007] [3.75439e-007,1.05158,-0.493198] [0,0.49334,1.05188] [-0.601706,2.10922,2.20948])),
		#("Modular_Pedal02", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.51216,2.34621,1.63465])),
		#("Modular_Pedal01", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.631056,2.34621,1.63465])),
		#("Modular_Pedal03", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [-0.378371,2.31594,1.57849])),
		#("Modular_Floor", (matrix3 [-2.02723e-007,-1.24431,0] [0,0,1] [-1.45165,2.36504e-007,0] [2.98972e-007,1.20572,1.37947])),
		#("Modular_Floor_Left", (matrix3 [0,-0.550657,0] [0,0,1] [-1,1.62921e-007,0] [-1.11074,1.93853,1.33804])),
		#("Modular_Floor_Right", (matrix3 [0,-0.550657,0] [0,0,1] [-1,1.62921e-007,0] [1.11074,1.93853,1.33804])),
		#("Modular_Left_Side", (matrix3 [-1.62921e-007,-1,0] [0,0,1.42283] [-0.332175,0,0] [-1.09808,1.49946,2.1488])),
		#("Modular_Right_Side", (matrix3 [-1.62921e-007,-1,0] [0,0,1.42283] [-0.332175,0,0] [1.09808,1.49946,2.1488])),
		#("Modular_Left_Side_Front", (matrix3 [-1.62921e-007,-1,0] [0,0,1.43775] [-0.380464,0,0] [-1.0655,2.33176,1.78417])),
		#("Modular_Right_Side_Front", (matrix3 [-1.62921e-007,-1,0] [0,0,1.43775] [-0.380464,0,0] [1.0655,2.33176,1.78417])),
		#("Modular_Ceiling", (matrix3 [-3.45028e-007,-0.999239,-0.0178946] [0,-0.0390029,0.458454] [-2.14181,1.62921e-007,0] [0,1.44949,2.88309])),
		#("Modular_Ceiling_Left", (matrix3 [-0.0335529,-0.475117,-0.0161922] [-0.344342,0,0.713533] [-0.897856,0.0781736,-0.433295] [-1.08877,1.84502,2.93533])),
		#("Modular_Ceiling_Right", (matrix3 [0.0388525,-0.475016,-0.00135595] [0.518724,0.0393734,0.597556] [-0.733541,-0.192757,0.651733] [1.08876,1.84502,2.93533])),
		#("Modular_Ceiling_Front", (matrix3 [-2.97278e-007,-0.959066,-0.283182] [-1.29217e-007,-0.283182,0.959066] [-1.94159,1.57545e-007,0] [0,2.31506,2.90256])),
		#("Modular_FrontGlass_Left", (matrix3 [0.265758,-0.600791,0.753938] [-0.785208,0.318828,0.530845] [-0.559304,-0.733075,-0.387015] [-1.09773,2.3032,2.492])),
		#("Modular_FrontGlass_Right", (matrix3 [-0.271287,-0.596669,0.755242] [0.788848,0.311749,0.529651] [-0.551472,0.739459,0.386108] [1.09773,2.3032,2.492])),
		#("Modular_Seat02", (matrix3 [-1.58244e-007,-0.995815,-0.0913947] [0,-0.0913947,0.995815] [-1,1.62921e-007,0] [0.594399,1.79113,1.8177])),
		#("Modular_SeatBack02", (matrix3 [-1.41436e-007,-0.951045,-0.309053] [0,-0.309053,0.951045] [-1,1.63254e-007,0] [0.594399,1.54102,1.82799])),
		#("Modular_SeatHeadRest02", (matrix3 [-1.41436e-007,-0.951045,-0.309053] [0,-0.309053,0.951045] [-1,1.63254e-007,0] [0.594399,1.33521,2.44097])),
		#("Modular_Seat03", (matrix3 [-1.62921e-007,-1,0] [0,0,1] [-1,1.62921e-007,0] [0,-3.8496,3.69813])),
		#("Modular_SeatBack03", (matrix3 [-1.38785e-007,-0.941943,-0.335773] [0,-0.335773,0.941943] [-1,1.63773e-007,0] [-1.6369e-007,-4.08791,3.69957])),
		#("Modular_SeatHeadRest03", (matrix3 [-1.69402e-007,-0.933251,0.359224] [0,0.359224,0.933251] [-1,1.5998e-007,0] [-2.18687e-007,-4.27715,4.22025]))
	)

-- loadRigPositions myDataICopiedFromTheListener_a --RUN THIS TO RESET THE TRANSFORM  DATA
-- loadRigPositions myDataForLAYOUT_STANDARD
try( DestroyDialog VehicleSwitcher )catch()
rollout VehicleSwitcher "Vehicle Template Switcher" width:225
(
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:VehicleSwitcher.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel versionName:"Crash Course" versionNum:1.0 wiki:"VehicleModularRigging" filename:(getThisScriptFilename())
		
	button BringRig "Merge Modular Rigging" width:175
	button StandardButton "Standard Template (Tailgater)" width:175
	button LowButton "Low Template (Adder)" width:175
	button VanButton "Van Template (Burrito)" width:175
	button TruckButton "Truck Template (Packer)" width:175	
	button btnQueryRigPositions "Query Rig Positions (Rigging)" width:175
-- 	button btnLoadRigPositions "Load rig positions" width:175
	

	on BringRig pressed do
		mergeMAXFile (RsConfigGetProjRootDir() + "art/vehicles/script_assets/BasicStructure_Extended.max") #mergeDups #renameMtlDups #alwaysReparent 
	on StandardButton pressed do
		loadRigPositions myDataForLAYOUT_STANDARD
	on LowButton pressed do
		loadRigPositions myDataForLAYOUT_LOW
	on VanButton pressed do
		loadRigPositions myDataForLAYOUT_VAN
	on TruckButton pressed do
		loadRigPositions myDataForLAYOUT_TRUCK
	
	on btnQueryRigPositions pressed do 
	(
		selectedObjects = selection as array
		
		if selectedObjects.count > 0 then
		(
			queryRigPositions selectedObjects
		)
		else
		(
			format "Please pick an object\n"
		)
	)
	
	on btnLoadRigPositions pressed do 
	(
		local input_name = undefined 
		if input_name == undefined do
		(
			input_name = getOpenFileName caption:"Output file" types:"(*.*)|*.*|All Files (*.*)|*.*|"
			
			local dataWeHaveRead = inputDataFromFile input_name
			
			for i = 1 to dataWeHaveRead.count do
			(
				format ((dataWeHaveRead[i] as string)+"\n")
			)			
		)		
	)
	
	on VehicleSwitcher open do
	(
		banner.setup()
	)
)


createDialog VehicleSwitcher