-- Scene Info Browser for optimisation
-- Rick Stirling, Rockstar North,  October 2011

-- The original request from Aaron was
-- Would it be possible to quickly knowck something up that displays the selections polycount, shader count, and a bunch of attributes�
-- Lod distance
-- Txd
-- Low priority
-- Don�t add to ipl


-------------------------------------------------------------------------------------------------------------------------
-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")





-------------------------------------------------------------------------------------------------------------------------
rollout sceneInfo "Scene Info Browser" width:220 height:100
(
	
	button btndontexport "Tag as DONT EXPORT" width:170 height:25

	edittext edtTXDName "    TXD" text:"TXD NAME" fieldWidth:150 labelOnTop:false readOnly:false

	button btntagthem "Add TXD and Is Fragment" width:170 height:25


	on sceneInfo open do
	(
		possibleTXDname = (filterstring maxfilename "_")[1]
		sceneInfo.edtTXDName.text = possibleTXDname
	)	

	
	on btndontexport  pressed do
	(
		for selObj in selection do
		(
			try 
			(
				indextoSet = GetAttrIndex (getAttrClass selObj) "Dont Export"
				AttrSet = SetAttr selObj indextoSet   true
			) catch(messagebox "Failed to set dont export flag")			
			
			try 
			(
				indextoSet = GetAttrIndex (getAttrClass selObj) "Dont Add To IPL"
				AttrSet = SetAttr selObj indextoSet   true
			) catch(messagebox "Failed to set dont add to IPL")		
		
			try 
			(			
				indextoSet = GetAttrIndex (getAttrClass selObj) "TXD"
				AttrSet = SetAttr selObj indextoSet "DONTEXPORT"
			) catch(messagebox "Failed to set TXD")		
			
		)			
		
	)	
	
	

	on btntagthem pressed do
	(
		
		
		for selObj in selection do
		(
			try 
			(
				indextoSet = GetAttrIndex (getAttrClass selObj) "Is Fragment"
				AttrSet = SetAttr selObj indextoSet   true
			) catch(messagebox "Failed to set fragment")		
			
			try 
			(	
				indextoSet = GetAttrIndex (getAttrClass selObj) "Dont Export"
				AttrSet = SetAttr selObj indextoSet false
			) catch(messagebox "Failed to set export")		
			
			try 
			(
				indextoSet = GetAttrIndex (getAttrClass selObj) "Dont Add To IPL"
				AttrSet = SetAttr selObj indextoSet  false
			) catch(messagebox "Failed to set dont add to IPL")		


			try 
			(
				indextoSet = GetAttrIndex (getAttrClass selObj) "TXD"
				AttrSet = SetAttr selObj indextoSet sceneInfo.edtTXDName.text
			) catch(messagebox "Failed to set TXD")		
			
			
		)	
		
	)
	
)



createDialog sceneInfo
