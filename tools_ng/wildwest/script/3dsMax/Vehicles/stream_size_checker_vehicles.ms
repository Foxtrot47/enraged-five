-- Stream Size Checker
-- Check a specified stream folder for a specified file type and sort the results in decending order.
-- Rockstar North
-- Stewart Wright September 2010

--moved to ww2 Stewart Wright May 2012

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
whichStreamState = 7
whichFilesState = 1

pathMSA = 1
fileTypeMSA = 2
filetypeNameMSA = 1
fileTypeExtMSA = 2

streamAndFiles = #(#(#("x:/streamGTA5/maps/"), #(#("Drawable", "Texture"), #("*.idr", "*.itd"))), #(#("n:/rsgedi/gta/gtaexport/streamGTA5/maps/"), #(#("Drawable", "Texture"), #("*.idr", "*.itd"))),
	#(#("x:/streamGTA5/componentpeds/"), #(#("Drawable", "Texture"), #("*.idd", "*.itd"))), #(#("x:/streamGTA5/streamedpeds/"), #(#("Drawable", "Texture"), #("*.idd", "*.itd"))),
	#(#("x:/streamGTA5/pedprops/"), #(#("Drawable", "Texture"), #("*.idd", "*.itd"))), #(#("x:/streamGTA5/weapons/"), #(#("Srawable", "Texture"), #("*.idr", "*.itd"))),
	#(#("n:/rsgedi/gta/gtaexport/streamGTA5/vehicles/"), #(#("Frag", "Texture"), #("*.ift", "*.itd"))))

fileSizeMaster = #()
foundFiles = #()

outputName = undefined
-------------------------------------------------------------------------------------------------------------------------

--changing the X parameter you pass determines which array to sort by
fn sortByXMember arr1 arr2 x:1 =
(
	case of
	(
		(arr1[x] > arr2[x]):-1
		(arr1[x] < arr2[x]):1
		default:0
	)
)--end sortByXMember
-------------------------------------------------------------------------------------------------------------------------

fn getFilesRecursive root pattern =
(
	subDirArray = #(root)
	for d in subDirArray do
	join subDirArray (GetDirectories (d+"/*"))

	foundFiles = #()

	for f in subDirArray do
	join foundFiles (getFiles (f + pattern))
	foundFiles
)--end getFilesRecursive
-------------------------------------------------------------------------------------------------------------------------

fn calcSizes =
(
	--get all files from the folder and all its subfolders
	getFilesRecursive streamAndFiles[whichStreamState][pathMSA][1] streamAndFiles[whichStreamState][fileTypeMSA][fileTypeExtMSA][whichFilesState]
	
	fileSizeMaster = #()

	for t=1 to foundFiles.count do
	(
		append fileSizeMaster #(#(),#())
	)

	for f=1 to foundFiles.count do
	(
		fileSizeMaster[f][1] = uppercase((getfilenamepath foundFiles[f]) + (getfilenamefile foundFiles[f]))
		fileSizeMaster[f][2] = getFileSize foundFiles[f]/1024
	)

	--sort the master array from highest to lowest based on the file size
	qsort fileSizeMaster sortByXMember x:2
)-- end calcSizes
-------------------------------------------------------------------------------------------------------------------------

fn csvOutput =
(
	outputName = streamAndFiles[whichStreamState][pathMSA][1] + " " + streamAndFiles[whichStreamState][fileTypeMSA][fileTypeNameMSA][whichFilesState] + "_size.csv"
	backupName = streamAndFiles[whichStreamState][pathMSA][1]  + " old_" + streamAndFiles[whichStreamState][fileTypeMSA][fileTypeNameMSA][whichFilesState]  + "_size.csv"

	try deleteFile backupName catch()
	try copyFile outputName backupName catch()

	outputFile = createFile outputName

	for p=1 to fileSizeMaster.count do
	(
		--format (whichStream[whichStreamState] + whichFiles[whichFilesState] + ",") to:outputFile --"," moves to the next column
		format (fileSizeMaster[p][1] as string + ",") to:outputFile --"," moves to the next column
		format (fileSizeMaster[p][2] as string + "KB\n") to:outputFile --"\n" moves to the next row
	)	
	close outputFile
)-- end csvOutput
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
rollout streamChecker "Stream Size Checker"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Size_Checker_Tool" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	dropdownlist ddlStreamType "Stream Type" items:#("Maps (Local)", "Maps (Network)", "Peds: Ambient", "Peds: Story", "Peds: Props", "Weapons", "Vehicles") selection:whichStreamState
	dropdownlist ddlstreamFile "File Type" items:#()
	button btnCalcSizes "Calculate Sizes" width:200 height:60
	button btnOpenResults "Open File" width:90 height:40 pos:[15,185]
	button btnOpenFolder "Open Folder" width:90 height:40 pos:[115,185]
-------------------------------------------------------------------------------------------------------------------------
	on streamChecker open do
	(
		ddlstreamFile.items = streamAndFiles[whichStreamState][2][1]
	)

	--monitor ddlStreamType buttons
	on ddlStreamType selected sel do
		(
			whichStreamState = ddlStreamType.selection
			ddlstreamFile.items = streamAndFiles[whichStreamState][2][1]
			whichFilesState = 1
			ddlstreamFile.selection = whichFilesState
		)
	--monitor ddlStreamType buttons
	on ddlstreamFile selected sel do
		(whichFilesState = ddlstreamFile.selection)
	
	on btnCalcSizes pressed do
		(
			calcSizes()
			csvOutput()
		)
	on btnOpenResults pressed do
		(
			try (ShellLaunch outputName "") catch()
		)
	on btnOpenFolder pressed do
		(
			try (ShellLaunch streamAndFiles[whichStreamState][pathMSA][1] "") catch()
		)
)--end  streamchecker tool
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- Create floater 
sizeCheckFloater = newRolloutFloater "Size Checking Tools" 230 310
-- Add the rollout section
addRollout streamChecker sizeCheckFloater