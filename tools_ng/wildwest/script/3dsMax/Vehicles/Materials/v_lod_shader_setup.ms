--Vehicle LOD Shader setup sctipt
--Stewart Wright
--Rockstar North
--04/05/11
--a script that will rebuild DX versions of Rage shaders and save files out to another 'safe' location
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

clearListener()  --housekeeping
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--moved to ww2 Stewart Wright May 2012

-- This line loads the custom header
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--setup the array to store all the material settings
masterArray = #()
usesTheMat = #()

matchThisTex = "VEHICLE_GENERIC" --"VEHICLE_GENERIC_" cut out some textures that didnt have the last underscore

dxShaderPath = "c:/vehicles/vehicle_shaders/"
OSSavePath = "C:/vehicles/models/"
OSTexPath = "C:/vehicles/textures/"
try (makedir OSSavePath) catch()
try (makedir OSTexPath) catch()
modelSavePath = undefined
fileSavePath = undefined

rememberMe = undefined
subDXShader = undefined

gtaFolders = #("GTA_5_RC", "GTA5_2_DOOR", "GTA5_4_DOOR", "GTA5_BIG", "GTA5_BIKES", "GTA5_BOATS", "GTA5_FLYERS", "GTA5_INDUSTRIAL",
					"GTA5_MILITARY", "GTA5_SERVICES", "GTA5_SPACE", "GTA5_TRAINS", "GTA5_UTILITY", "GTA5_VANS")
OSFolders = #("RC", "2DR", "4DR", "BIG", "BK", "BOT", "FLY", "IND", "MIL", "SRV", "SPC", "TRN", "UTL", "VAN")

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--reset the scene and merge it into the empty max file
fn resetAndMerge =
(
	thePath = maxFilePath as string
	theFile = maxFileName as string
	rememberMe = thePath + theFile
	max reset file

	mergeMaxFile rememberMe quiet:true

	newFileName = rememberMe + "_merged"
	saveMaxFile newFileName
	
	try
	(
		select $chassis
		max zoomext sel all
		max tool maximize
	)catch(messagebox "Failed to automatically select the chassis mesh.\r\nPlease select it manually before the next step")
)--end resetAndMerge

--wipe all trace of textures from the file
fn wipeItAll =
(
	-- unhide all by categry
	hideByCategory.none()	
	-- unhide all
	unhide (for obj in objects where obj.ishidden collect obj)
	unfreeze (for obj in objects where obj.isfrozen collect obj)
	max select all
	wipeThese = getCurrentSelection()
	for w=1 to wipeThese.count do
	(
		select wipeThese[w]
		$.material = meditMaterials[1]
	)
	clear_all_materials()
	kill_materials()
)
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- function for finding what uses the material
fn whatUsesMat matID =
(
	usesTheMat = #()
	hideByCategory.none()-- unhide all by categry
	unhide (for obj in objects where obj.ishidden collect obj)-- unhide all
	max select all
	everything = getCurrentSelection()
	for sel=1 to everything.count do
	(
		if everything[sel].material == meditMaterials[matID] then
		(
			append usesTheMat everything[sel]
		)
	)
	--max select all
	deselect usesTheMat
	hide selection
)--end whatUsesMat
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--save function
fn saveCopy =
(
	--get the file folder and work out where it should be saved for outsource
		thePath = maxFilePath as string
		filteredPath = filterString thePath "\\"
		filteredPath = uppercase filteredPath[filteredPath.count]
		findIt = findItem gtaFolders filteredPath
	--create folder for file
		modelSavePath = OSSavePath + OSFolders[findIt] + "/"
		try (makedir modelSavePath) catch()
	--get the filename and clean it up
		theFile = maxFileName as string
		theFile = substring theFile 1 3
		theFile = uppercase theFile
		theFile = OSFolders[findIt] + "_" + theFile
	--save the file
		fileSavePath = modelSavePath + theFile
		saveMaxFile fileSavePath
)--end saveCopy

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--function for getting the shader/material values and storing them
fn getShaderValues matID = 
(
	if classof meditMaterials[MatID] == Multimaterial then  --if the material is a multisub then we want to look in it
	(
		multisubArray = #() --lets make a 'disposable' array for storing the multisub
		for SubID = 1 to meditMaterials[MatID].materialIDList.count do --for every multisub in this material
		(
			realSubID = meditMaterials[matid].materialIDList[subID]
			
			if classof meditMaterials[matid][realSubID] == Rage_Shader then
			(
				shadertype = RstGetShaderName meditMaterials[matid][realSubID]

				shaderArray = #() --lets make a 'disposable' array for storing the settings

				for x=1 to RstGetVariableCount meditMaterials[matid][realSubID] do
				(
					tempArray = #()
					varType = RstGetVariableType meditMaterials[matid][realSubID] x
					if varType == "texmap" then
					(
						varName = RstGetVariableName meditMaterials[matid][realSubID] x
						append tempArray varName
						varValue = RstGetVariable meditMaterials[matid][realSubID] x

						--lets copy all the materials that are used
						try
						(
							if getFileSize varValue != 0 then
							(
								print "rage texture"
								print varValue
								--if the file exists it will have a filesize...
								oldTex = varValue
								filteredTexFolder = filterString oldTex "\\" --filter the path so we have just the texture name
								actualTexFile = filteredTexFolder[filteredTexFolder.count]
								
								filteredTexFile = substring actualTexFile 1 15
								filteredTexFile = uppercase filteredTexFile
								
								if filteredTexFile == matchThisTex then --its a common texture, lets chuck it in the shared folder
								(
									textureSavePath = OSTexPath + "shared/"
									try (makedir textureSavePath) catch()

									newTex = textureSavePath + actualTexFile
									copyFile oldTex newTex
								)
								else --its a unique texture, we need to diguise it!
								(
									--figure out what the texture folder should be
										theFile = maxFileName as string
										theFile = substring theFile 1 3
										theFile = uppercase theFile

										thePath = maxFilePath as string
										filteredPath = filterString thePath "\\"
										filteredPath = uppercase filteredPath[filteredPath.count]
										findIt = findItem gtaFolders filteredPath

										textureSavePath = OSTexPath + OSFolders[findIt] + "/" + OSFolders[findIt] + "_" + theFile + "/"
										try (makedir textureSavePath) catch()

									--rename the texture file something more cryptic
										texFileType = filterstring actualTexFile "."
										newTexFile = OSFolders[findIt] + "_" + theFile + "_" + (matID as string) + (subID as string) + (x as string) + "." + texFileType[texFileType.count]

										newTex = textureSavePath + newTexFile
										copyFile oldTex newTex
								)
								print "safe texture"
								print newTex
							)
							else
							(
								print "Texture not found.  Using 'unknown.bmp'"
								--if the file doesn't exist it won't have a size so we'll use a default texture
								textureSavePath = OSTexPath + "shared/"
								newTex = textureSavePath + "unknown.bmp"
							)
						)catch(getCurrentException())

						append tempArray newTex
						append shaderArray tempArray
					)else()
				)
				subIDNameAndNumber = ("Slot Name=" + meditMaterials[MatID].names[SubID] + ", subID=" + SubID as string)
				tempArray = #(subIDNameAndNumber, realSubID, meditMaterials[MatID].names[SubID], shadertype, shaderArray)
				append multisubArray tempArray
			)
			else()
		)
		shaderNameAndNumber = ("Material Name=" + meditMaterials[MatID].name + ", MatID=" + matID as string)
		tempArray = #(shaderNameAndNumber, meditMaterials[MatID].name, matID, multisubArray)
		append masterArray tempArray --ammend our main array with the temp array and continue through the other material slots
	)
	else()
)
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--function for writing out the shader values to a text file
fn writeShaderValues =
(
	outputFolder = maxfilepath  --output folder is the same as the max file
	outputFile = outputFolder + "ShaderOutput.txt"  --save the file as "shadersettings.txt"
	shaderFile = createFile outputFile  --now we make the text file using the above settings
	print masterArray to: shaderFile  --write the contents of our array to the text file
	close shaderFile  --housekeeping.  this closes the txt file
)
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--function for reading in the shader values from a text file
fn readShaderValues =
(
	masterArray = #()  --housekeeping.  empty the array before we start
	sourceFolder = maxfilepath
	sourceFile = sourceFolder + "ShaderOutput.txt"
	shaderSettingsFile = openFile sourceFile
	
	if shaderSettingsFile != undefined then
	(	
		while not eof shaderSettingsFile do  --as long as we haven't reached the end of the file keep going
		(
			checkLine = readExpr shaderSettingsFile  --readExpr gets the info from the txt file
			append masterArray checkLine  --adds each line of the text file to our array
		)
		close shaderSettingsFile  --housekeeping.  this closes the txt file
	)
	else
	(
		mText = "I couldn't find " + sourcefile + "."
		messagebox mText title:"Error"
	)
)--end readShaderValues
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--function for reading in the shader file
fn readSPSShader whatShader=
(
	shaderPath = (RsConfigGetBuildDir() + "common/shaders/db/" + whatShader)
	
	shaderArray = #() --housekeeping.  empty the array before we start

	shaderFile = openFile shaderPath
	
	if shaderFile != undefined then
	(	
		while not eof shaderFile do  --as long as we haven't reached the end of the file keep going
		(
			checkLine = readLine shaderFile  --readExpr gets the info from the txt file
			append shaderArray checkLine  --adds each line of the text file to our array
		)
		close shaderFile  --housekeeping.  this closes the txt file
		
		useThisShader = filterString shaderArray[1] " "
		subDXShader = useThisShader[2] + ".fx"
	)
	else
	(
		subDXShader = "vehicle_generic.fx"--set it as default if we can't find a match
		mText = "I couldn't find " + shaderPath + "."
		messagebox mText title:"Error"
	)
)--end readSPSShader
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--function for setting the read shader/material values
fn setShaderValues =
(
	if masterArray.count !=0 then
	(
		--setup material
		for cnt = 1 to masterArray.count do 
		(
			tempArray = masterArray[cnt][4] as array

			--setup the material slot
			matID= masterArray[cnt][3]
			meditMaterials[matID] = Multimaterial numsubs:(tempArray.count)
			meditMaterials[matID].name = masterArray[cnt][2]

			--setup the sub-material
			for subM=1 to tempArray.count do
			(
				subID = tempArray[subM][2]
				meditMaterials[matID].materialIDList[subM] = subID
				meditMaterials[matID].names[subM] = tempArray[subM][3]
				subShader = tempArray[subM][4]

				readSPSShader subShader --run the function to see what fx shader to use

				subDXShaderPath = (dxShaderPath + subDXShader)
				
				meditMaterials[matID].materialList[subM] = DirectX_9_Shader ()
				meditMaterials[matID].materialList[subM].effectFile = subDXShaderPath --set the shader preset
				
				--fill the bmp slots with materials from the rage shader
				-------------------------------------------------------------------------------------------------------------------------
				-------------------------------------------------------------------------------------------------------------------------
					--diffuse texture
					--need to loop through our master array and search within it
					findRageBMP = 0
					for s=1 to tempArray[subM][5].count do
					(
						tempFind = findItem tempArray[subM][5][s] "1: Diffuse Texture"
						if tempFind != 0 then
						(
						findRageBMP = s --if we find what we're looking for then remember which array it was in
						)
					)--end of findRageBMP loop

					if findRageBMP !=0 then
					(
					--now I know where the bitmaps can go in the shader I need to load some in
					setTo = tempArray[subM][5][findRageBMP][2]
					if setTo == "" do (setTo = default)
						try
						(
							meditMaterials[matID].materiallist[subM].diffuseTexture = openBitmap setTo
						)catch(getCurrentException())
					)
					-------------------------------------------------------------------------------------------------------------------------
					--diffuse texture 2
					findRageBMP = 0
					for s=1 to tempArray[subM][5].count do
					(
						tempFind = findItem tempArray[subM][5][s] "1: Diffuse"
						if tempFind != 0 then
						(
						findRageBMP = s
						)
					)--end of findRageBMP loop

					if findRageBMP !=0 then
					(
					setTo = tempArray[subM][5][findRageBMP][2]
					if setTo == "" do (setTo = default)
						try
						(
							meditMaterials[matID].materiallist[subM].diffuseTexture = openBitmap setTo
						)catch(getCurrentException())
					)
				-------------------------------------------------------------------------------------------------------------------------
				-------------------------------------------------------------------------------------------------------------------------
					--bump texture
					findRageBMP = 0
					for s=1 to tempArray[subM][5].count do
					(
						tempFind = findItem tempArray[subM][5][s] "1: Bump Texture"
						if tempFind != 0 then
						(
						findRageBMP = s
						)
					)--end of findRageBMP loop

					if findRageBMP !=0 then
					(
					setTo = tempArray[subM][5][findRageBMP][2]
					if setTo == "" do (setTo = default)

						try
						(
							meditMaterials[matID].materiallist[subM].bumpTexture = openBitmap setTo
						)catch(getCurrentException())
					)
				-------------------------------------------------------------------------------------------------------------------------
				-------------------------------------------------------------------------------------------------------------------------
					--dirt texture
					findRageBMP = 0
					for s=1 to tempArray[subM][5].count do
					(
						tempFind = findItem tempArray[subM][5][s] "2: Dirt Texture"
						if tempFind != 0 then
						(
						findRageBMP = s
						)
					)--end of findRageBMP loop

					if findRageBMP !=0 then
					(
						setTo = tempArray[subM][5][findRageBMP][2]
						if setTo == "" do (setTo = default)

						try
						(
							meditMaterials[matID].materiallist[subM].dirtTexture = openBitmap setTo
						)catch(getCurrentException())
					)
					-------------------------------------------------------------------------------------------------------------------------
					--dirt texture 2
					findRageBMP = 0
					for s=1 to tempArray[subM][5].count do
					(
						tempFind = findItem tempArray[subM][5][s] "1: Dirt Texture"
						if tempFind != 0 then
						(
						findRageBMP = s
						)
					)--end of findRageBMP loop

					if findRageBMP !=0 then
					(
						setTo = tempArray[subM][5][findRageBMP][2]
						if setTo == "" do (setTo = default)

						try
						(
							meditMaterials[matID].materiallist[subM].dirtTexture = openBitmap setTo
						)catch(getCurrentException())
					)
					-------------------------------------------------------------------------------------------------------------------------
					--dirt texture 3
					findRageBMP = 0
					for s=1 to tempArray[subM][5].count do
					(
						tempFind = findItem tempArray[subM][5][s] "2: Dirt Texture"
						if tempFind != 0 then
						(
						findRageBMP = s
						)
					)--end of findRageBMP loop

					if findRageBMP !=0 then
					(
						setTo = tempArray[subM][5][findRageBMP][2]
						if setTo == "" do (setTo = default)

						try
						(
							meditMaterials[matID].materiallist[subM].dirtTexture = openBitmap setTo
						)catch(getCurrentException())
					)
				-------------------------------------------------------------------------------------------------------------------------
				-------------------------------------------------------------------------------------------------------------------------
					--specular texture
					findRageBMP = 0
					for s=1 to tempArray[subM][5].count do
					(
						tempFind = findItem tempArray[subM][5][s] "1: Specular Texture"
						if tempFind != 0 then
						(
						findRageBMP = s
						)
					)--end of findRageBMP loop

					if findRageBMP !=0 then
					(
						setTo = tempArray[subM][5][findRageBMP][2]
						if setTo == "" do (setTo = default)

						try
						(
							meditMaterials[matID].materiallist[subM].specTexture = openBitmap setTo
						)catch(getCurrentException())
					)
					-------------------------------------------------------------------------------------------------------------------------
					--specular texture 2
					findRageBMP = 0
					for s=1 to tempArray[subM][5].count do
					(
						tempFind = findItem tempArray[subM][5][s] "1: Specular"
						if tempFind != 0 then
						(
						findRageBMP = s
						)
					)--end of findRageBMP loop

					if findRageBMP !=0 then
					(
						setTo = tempArray[subM][5][findRageBMP][2]
						if setTo == "" do (setTo = default)

						try
						(
							meditMaterials[matID].materiallist[subM].specTexture = openBitmap setTo
						)catch(getCurrentException())
					)
					-------------------------------------------------------------------------------------------------------------------------
					--specular texture 3
					findRageBMP = 0
					for s=1 to tempArray[subM][5].count do
					(
						tempFind = findItem tempArray[subM][5][s] "2: Specular"
						if tempFind != 0 then
						(
						findRageBMP = s
						)
					)--end of findRageBMP loop

					if findRageBMP !=0 then
					(
						setTo = tempArray[subM][5][findRageBMP][2]
						if setTo == "" do (setTo = default)

						try
						(
							meditMaterials[matID].materiallist[subM].specTexture = openBitmap setTo
						)catch(getCurrentException())
					)
					-------------------------------------------------------------------------------------------------------------------------
					--specular texture 3
					findRageBMP = 0
					for s=1 to tempArray[subM][5].count do
					(
						tempFind = findItem tempArray[subM][5][s] "2: Specular Texture"
						if tempFind != 0 then
						(
						findRageBMP = s
						)
					)--end of findRageBMP loop

					if findRageBMP !=0 then
					(
						setTo = tempArray[subM][5][findRageBMP][2]
						if setTo == "" do (setTo = default)

						try
						(
							meditMaterials[matID].materiallist[subM].specTexture = openBitmap setTo
						)catch(getCurrentException())
					)
				-------------------------------------------------------------------------------------------------------------------------
				-------------------------------------------------------------------------------------------------------------------------
					--enveff texture
					findRageBMP = 0
					for s=1 to tempArray[subM][5].count do
					(
						tempFind = findItem tempArray[subM][5][s] "2: EnvEff"
						if tempFind != 0 then
						(
						findRageBMP = s
						)
					)--end of findRageBMP loop

					if findRageBMP !=0 then
					(
					setTo = tempArray[subM][5][findRageBMP][2]
					if setTo == "" do (setTo = default)
						try
						(
							meditMaterials[matID].materiallist[subM].snowTexture0 = openBitmap setTo
						)catch(getCurrentException())
					)
					--enveff texture 2
					findRageBMP = 0
					for s=1 to tempArray[subM][5].count do
					(
						tempFind = findItem tempArray[subM][5][s] "1: EnvEff"
						if tempFind != 0 then
						(
						findRageBMP = s
						)
					)--end of findRageBMP loop

					if findRageBMP !=0 then
					(
					setTo = tempArray[subM][5][findRageBMP][2]
					if setTo == "" do (setTo = default)
						try
						(
							meditMaterials[matID].materiallist[subM].snowTexture0 = openBitmap setTo
						)catch(getCurrentException())
					)
				-------------------------------------------------------------------------------------------------------------------------
				-------------------------------------------------------------------------------------------------------------------------
					--secondary
					findRageBMP = 0
					for s=1 to tempArray[subM][5].count do
					(
						tempFind = findItem tempArray[subM][5][s] "2: Secondary"
						if tempFind != 0 then
						(
						findRageBMP = s
						)
					)--end of findRageBMP loop

					if findRageBMP !=0 then
					(
					--now I know where the bitmaps can go in the shader I need to load some in
					setTo = tempArray[subM][5][findRageBMP][2]
					if setTo == "" do (setTo = default)
						try
						(
							meditMaterials[matID].materiallist[subM].diffuseTexture2 = openBitmap setTo
						)catch(getCurrentException())
					)
					-------------------------------------------------------------------------------------------------------------------------
					--secondary 2
					findRageBMP = 0
					for s=1 to tempArray[subM][5].count do
					(
						tempFind = findItem tempArray[subM][5][s] "2: Secondary Texture"
						if tempFind != 0 then
						(
						findRageBMP = s
						)
					)--end of findRageBMP loop

					if findRageBMP !=0 then
					(
					--now I know where the bitmaps can go in the shader I need to load some in
					setTo = tempArray[subM][5][findRageBMP][2]
					if setTo == "" do (setTo = default)
						try
						(
							meditMaterials[matID].materiallist[subM].diffuseTexture2 = openBitmap setTo
						)catch(getCurrentException())
					)
				-------------------------------------------------------------------------------------------------------------------------
				-------------------------------------------------------------------------------------------------------------------------
			)--end sub-mat setup
		)--end material setup
	)else (print "no shader settings found")
)--end setShaderValues
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
if ((VehicleLODShaders != undefined) and (VehicleLODShaders.isDisplayed)) do
	(destroyDialog VehicleLODShaders)
-------------------------------------------------------------------------------------------------------------------------
--create rollout
rollout VehicleLODShaders "Vehicle Shader LOD Prep"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Shader_Saver_Tool" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	label desc "Replace Rage Shaders with DX versions"
	
	--create buttons
	button btnResetAndMerge "Reset and merge the file" width:210 height:40 toolTip:"Merges the file in to a clean scene and resaves it"
	button btnGetShader "Read Rage Shader Values" width:210 height:40 toolTip:"Reads your material editor and stores all the shader settings"
	button btnWriteShader "Create DX Shaders" width:210 height:40 toolTip:"Reads in the shader values from an external file and applys them"

	on btnResetAndMerge pressed do
	(
		rememberMe = undefined
		resetAndMerge()
	)
	
	On btnGetShader pressed do
	(
		if selection.count == 1 then
		(
			matID = 24
			tempSel = getcurrentSelection()
			meditMaterials[matID] = $.material --copy the selected material to slot 24 so we can use it easily
			whatUsesMat matID
			clear_all_materials()
			select tempSel
			meditMaterials[matID] = $.material --copy the selected material to slot 24 so we can use it easily
			if classof meditMaterials[matID]== Multimaterial then --I am making the assumption that everything we want is a multimaterial
			(
				realSubID = meditMaterials[matid].materialIDList[1]
				if classof meditMaterials[matID][realSubID] == Rage_Shader do --if the sub material is a rage material
				(
					getShaderValues matID
				)
			)
			else(print "not a rage material")
			saveCopy ()
			writeShaderValues ()
			wipeItAll()
		)
		else (messagebox "Please select one mesh with rage shaders applied")
	)
	
	On btnWriteShader pressed do
	(
		wipeItAll()
		clear_all_materials()
		unhide (for obj in objects where obj.ishidden collect obj)
		kill_materials()
		
		readShaderValues()
		setShaderValues()
		
		max select all
		deselect usesTheMat
		hide selection
		usesTheMat.material = meditMaterials[24]
		
		saveMaxFile fileSavePath
		
		try
		(
			delSettings = modelSavePath + "ShaderOutput.txt"
			deleteFile delSettings
		)catch()
	)
)--end of rollout "Vehicle Shader LOD Prep"
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--Create new floater
createDialog VehicleLODShaders width:230


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------