filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

try( DestroyDialog VehicleHDSplit_UI)catch()

rollout VehicleHDSplit_UI "Vehicle HDSplit Processor" width:250
(
	local changeListNum = undefined
	local dlcList = #()
	
	----------------------------------------------------------------------------------
	--		CONTROLS
	----------------------------------------------------------------------------------
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:VehicleHDSplit_UI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Vehicle HDSplitter" versionName:"Aqua Mariner" versionNum:1.02 filename:(getThisScriptFilename())
		
	dropdownlist lstDLC
		
	button btn_process "Process!" width:(VehicleHDSplit_UI.width - 10)
-- 	group ""
-- 	(
		label lbl_feedback "Process update"
-- 	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn isHDSplitSet tcsPath =
	(
		--get tcs
		--tcsPath = tcsPathRoot + (tolower texName) + ".tcs"
		
		if doesFileExist tcsPath == false then return false
		
		xmlDoc = XMlDocument()
		xmlDoc.init()
		xmlDoc.load ( tcsPath )
		xmlRoot = xmlDoc.document.DocumentElement
		imageSplitHDNode = xmlRoot.getElementsByTagName "imageSplitHD"
		
		if imageSplitHDNode.count != 1 then return false else return true
		
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn add_imageSplitHD tcsPath =
	(
		xmlDoc = XMlDocument()
		xmlDoc.init()
		xmlDoc.load ( tcsPath )
		xmlRoot = xmlDoc.document.DocumentElement
		
		if xmlRoot != undefined then
		(
			--create the imageSplit node
			imageSplitHD = xmlDoc.createElement "imageSplitHD"
			imageSplitHDAttr = xmlDoc.createAttribute "value"
			imageSplitHDAttr.value = "true"
			imageSplitHD.setAttributeNode imageSplitHDAttr
			
			--check for existing imageSplit nodes
			imageSplitNodes = xmlRoot.getElementsByTagName "imageSplitHD"
			if imageSplitNodes.count == 0 then
			(
				xmlRoot.appendChild imageSplitHD
			)
			else --lets remove the imageSplit duplicate nodes and replace with a fresh singular one
			(
				for i = (xmlRoot.childNodes.count - 1) to 0 by -1 do
				(
					thisNode = xmlRoot.childNodes.itemOf[i]
					if thisNode.name == "imageSplitHD" then
					(
						xmlRoot.removeChild thisNode
					)
				)
				--make sure we have at least one imageSplitHD node added
				xmlRoot.appendChild imageSplitHD
			)
			gRsPerforce.add_or_edit #( tcsPath ) silent:false
			xmlDoc.save ( tcsPath )
			gRsPerforce.addToChangelist changeListNum tcsPath
		)
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	fn populateDLCList =
	(
		dlcList = #()
		
		--append core project
		local coreDirList = (getDirectories (RsConfigGetAssetsDir() + "metadata/textures/vehicles/*"))
		join coreDirList (getDirectories (RsConfigGetAssetsDir() + "metadata/textures/vehicles_mp/*"))
		
		append dlcList (DataPair key:"Core" value:coreDirList)
		
			
		--DLC
		local dlcit = gRsConfig.DLCProjects.GetEnumerator()
		while dlcit.MoveNext() do
		(
			local thisDLC = dlcit.Current.Value
			--print thisDLC.name
			local DLCRoot = RsMakeSafeSlashes thisDLC.Root
			local devBranch = try( thisDLC.Branches.item "dev" )catch()
			local dev_ngBranch = try( thisDLC.Branches.item "dev_ng" ) catch()
			
			--look for mpPacks or spPacks or patchPacks
			if (matchPattern DLCRoot pattern:"*/mpPacks/*") or (matchPattern DLCRoot pattern:"*/spPacks/*" or (matchPattern DLCRoot pattern:"*/patchPacks/*")) then
			(
				local vehicleDirs = #()
				if (devBranch != undefined) then
				(
					join vehicleDirs (getDirectories (devBranch.Assets + "/metadata/textures/vehicles/*"))
					gRsPerforce.sync (devBranch.Assets + "/metadata/textures/vehicles/....tcs")
						
					join vehicleDirs (getDirectories (devBranch.Assets + "/metadata/textures/vehicles_mp/*"))
					gRsPerforce.sync (getDirectories (devBranch.Assets + "/metadata/textures/vehicles_mp/....tcs"))
				)
				
				if (dev_ngBranch != undefined) then
				(
					join vehicleDirs (getDirectories (dev_ngBranch.Assets + "/metadata/textures/vehicles/*"))
					gRsPerforce.sync (dev_ngBranch.Assets + "/metadata/textures/vehicles/....tcs")
						
					join vehicleDirs (getDirectories (dev_ngBranch.Assets + "/metadata/textures/vehicles_mp/*"))
					gRsPerforce.sync (dev_ngBranch.Assets + "/metadata/textures/vehicles_mp/....tcs")
				)
				
				local tcsFiles = #()
				for dir in vehicleDirs do 
				(
					join tcsFiles (getFiles (dir + "*.tcs"))
				)
				
				if (tcsFiles.count > 0) then
				(
					--gRsPerforce.sync tcsFiles
					append dlcList (DataPair key:dlcit.Current.Value.Name value:vehicleDirs)
				)
			)
		)
		
		local dlcNames = for dlc in dlcList collect dlc.key
			
		lstDLC.items = dlcNames
		
		lbl_feedback.text = "Ready!"
		
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btn_process pressed do
	(
		if (changeListNum == undefined) then
		(
			changeListNum = (gRsPerforce.createChangeList "Vehicle HDSplit Changed TCS Files")
		)
		
		dirs = #()
		for item in dlcList where item.key == lstDlc.selected do
		(
			dirs = item.value
		)

		--dirs = getDirectories (RsConfigGetAssetsDir() + "metadata/textures/vehicles/*")
		--join dirs (getDirectories (RsConfigGetAssetsDir() + "metadata/textures/vehicles_mp/*"))
	
		for dir in dirs where (pathConfig.stripPathToLeaf dir != "shared\\") do
		(
			local files = (getFiles (dir + "*.tcs"))
			lbl_feedback.text = "Syncing: " + (pathConfig.stripPathToLeaf dir)
			gRsPerforce.sync files
			
			for tcsPath in files where (not(isHDSplitSet tcsPath)) and (doesFileExist tcsPath) do
			(
				lbl_feedback.text = (pathConfig.stripPathToLeaf tcsPath) + (getFilenameFile tcsPath)
				add_imageSplitHD tcsPath
			)
		)
		lbl_feedback.text = "Done!"
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on VehicleHDSplit_UI open do
	(
		banner.setup()
		if gRsPerforce.connected() == false then gRsPerforce.connect()
		
		lbl_feedback.text = "Syncing TCS"
		populateDLCList()
	)
)

CreateDialog VehicleHDSplit_UI 