filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")

try( DestroyDialog DeleteOrphanVehicleAssets_UI )catch()

rollout DeleteOrphanVehicleAssets_UI "Delete Orphan Vehicle Assets" width:300
(
	local vehicleXML = getFiles (RsConfigGetAssetsDir() + "vehicles/*.xml")
	local vehicleTCS_P4 = (RsConfigGetAssetsDir() + "metadata/textures/vehicles/")
	local vehicleLiveryXML = getFilesRecursive (RsConfigGetArtDir() + "vehicles/") "*.xml"
	
	local orphanTCS = #()
	local changeListNum = undefined
	local textureNames = #()
	
	--
	--CONTROLS
	--
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:DeleteOrphanVehicleAssets_UI.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"PropTexture HDSplitter" filename:(getThisScriptFilename())
		
	button btn_DoIt "Analyse" width:(DeleteOrphanVehicleAssets_UI.width - 20)
	multilistbox lstOrphans height:20
	button btn_Delete "Delete Selected" width:(DeleteOrphanVehicleAssets_UI.width - 20)
	--checkbox chk_All "All" offset:[100, 0]
		
	group "Process Info"
	(
		label lbl_info "Ready."
	)

	
	--/////////////////////////////////////////
	--	FUNCTIONS
	--/////////////////////////////////////////
	fn XMLTextureScanner xmlList xpath fileattr =
	(
		for xmlPath in xmlList do
		(
			local xmlName = getFilenameFile xmlPath
			
			xmlStream = xmlStreamHandler xmlFile:xmlPath
			xmlStream.open()
				
			xmlTextureNodes = xmlStream.root.SelectNodes xpath
			
			for n=0 to (xmlTextureNodes.count - 1) do
			(
				local thisNode = xmlTextureNodes.item[n]
				local fileName = thisNode.GetAttribute fileattr
				
				appendIfUnique textureNames (getFilenameFile fileName)
			)
			
			xmlStream.close()
		)
	)
	
	fn processVehicles =
	(
		--make sure we synced all the relevant files
		lbl_info.text = "Sync Vehicle livery XML"
		gRsPerforce.sync (RsConfigGetArtDir() + "vehicles/.../*.xml")
		vehicleLiveryXML = getFilesRecursive (RsConfigGetArtDir() + "vehicles/") "*.xml"
		
		lbl_info.text = "Sync Vehicle XML"
		gRsPerforce.sync (RsConfigGetAssetsDir() + "vehicles/.../*.xml")
		vehicleXML = getFiles (RsConfigGetAssetsDir() + "vehicles/*.xml")
		
		lbl_info.text = "Sync Vehicle TCS"
		gRsPerforce.sync (RsConfigGetAssetsDir() + "metadata/textures/vehicles/.../*.tcs")

		
		XMLTextureScanner vehicleLiveryXML ".//texture" "file"
		
		--break()
		
		XMLTextureScanner vehicleXML ".//texture" "filename"
		
		sort textureNames
		print textureNames.count
		
		local vehicleTCS = getFilesRecursive (RsConfigGetAssetsDir() + "metadata/textures/vehicles/") "*.tcs"
			
		for tcs in vehicleTCS do
		(
			if (trimright (pathconfig.strippathtoleaf (getfilenamepath tcs)) "\\") == "shared" then continue
				
			if findItem textureNames (getFilenameFile tcs) == 0 then
			(
				local uiName = (pathConfig.stripPathToLeaf (getFilenamePath tcs)) + (getFilenameFile tcs)
				append orphanTCS (DataPair name:uiName path:tcs)
			)
			
			lbl_info.text = ("Processed: " + tcs)
		)
			
		lbl_info.text = "Ready."
		
		--present the list
		lstOrphans.items = for item in orphanTCS collect item.name
			
		free textureNames
	)
	
	--add the orphan tcs to a changelist for deletion
	--print orphanTCS
	
	--get all the tcs for the vehicles and compare against the dict we just made
	--local vehicleTCS = getFilesRecursive "x:/gta5/assets/metadata/textures/vehicles/" "*.tcs"
	
	--/////////////////////////////////////////
	-- 
	--/////////////////////////////////////////
	fn getMeAChangelist =
	(
		if changeListNum == undefined then
		(
			changeListNum = gRsPerforce.createChangeList ("Delete orphan Vehicle tcs" )
		)
	)
	
	
	--/////////////////////////////////////////
	-- EVENTS
	--/////////////////////////////////////////
	
	--/////////////////////////////////////////
	-- 
	--/////////////////////////////////////////
	on btn_DoIt pressed do
	(
		processVehicles()
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on btn_Delete pressed do
	(
		local selected = lstOrphans.selection as Array
		if selected.count == 0 then return false
			
		for i=1 to selected.count do
		(
			local tcsPathForDelete = undefined
			for elem in orphanTCS while tcsPathForDelete == undefined do
			(
				if elem.name == lstOrphans.items[selected[i]] then tcsPathForDelete = elem.path
			)
			
			--print tcsPathForDelete
			
			--set the path for delete
			getMeAChangelist()
			
			gRsPerforce.addDeleteFilesToChangelist changeListNum tcsPathForDelete
		)
		
		messageBox ("Deleted files in changelist: " + (changeListNum as String) + "\nHave a look")
	)
	
	--/////////////////////////////////////////
	--
	--/////////////////////////////////////////
	on DeleteOrphanVehicleAssets_UI open do
	(
		banner.setup()
		if gRsPerforce.connected() == false then gRsPerforce.connect()
	)
	
)

createDialog DeleteOrphanVehicleAssets_UI