-- tool to create a rool bone from two selected bones.

filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")	

-- Hardcoded Axis at the minute
theAxis = "Z"




fn createrollboneEXP parentbone theDriver theAxis = 
(
	-- check class of both objects
	
	-- The parent needs a zero transform too!
	clearselection()
	rollbonepos = theDriver.pos
	rollbonename = parentbone.name + "_ROLL"
	
	buildPointHelper rollbonename rollbonepos 0.25 (color 255 255 0)
	select (getnodebyname rollbonename)
	theRollbone = $	
	try (theRollbone.parent = parentbone) catch(messagebox "Unable to parent the bone")
	theRollbone.transform = theDriver.transform
	setUserPropbuffer theRollbone "exportTrans = true"
	FreezeTransform()	
	

	
	-- Set a float controller on the correct rotation
	fc = undefined
	drvr = undefined

	case theAxis of
	(
		"X": (
			fc = theRollBone.rotation.controller.Zero_Euler_XYZ.controller.X_Rotation.controller = Float_Expression () 
			drvr = theDriver.rotation.controller.X_Rotation.controller
		)
		"Y": (
			fc = theRollBone.rotation.controller.Zero_Euler_XYZ.controller.Y_Rotation.controller = Float_Expression ()
			drvr = theDriver.rotation.controller.Y_Rotation.controller			
		)
		"Z": (
			fc = theRollBone.rotation.controller.Zero_Euler_XYZ.controller.Z_Rotation.controller = Float_Expression () 
			drvr = theDriver.rotation.controller.Z_Rotation.controller
		)
	)	
	
	SVN = "roll"
	fc.AddScalarTarget SVN drvr 
	fc.SetExpression (svn + " * " + 0.5 as string)

)	


fn getBoneSelection = 
(
	boneSelection = #()
	
	if selection[1] == undefined then (
		messagebox "Please select a valid bone for building a bone list - the first bone in the roof"
	)
	
	else
	(
		startbone = selection[1] 
		
		while startbone != undefined do 
		(
			childbone = startbone.children[1]
			if childbone != undefined then
			(
				chainEntry = #(startbone, childbone)
				append boneSelection chainEntry
			)
			startbone = childbone
		)
		
	)
	
	boneSelection -- retun this array
)




try(DestroyDialog vehicleRoofExpressionTool)catch()


rollout vehicleRoofExpressionTool "Vehicle Roof Expression Tool"
(
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:vehicleRoofExpressionTool.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"vehicleRoofExpressionTool" filename:(getThisScriptFilename())
	
	on vehicleRoofExpressionTool open do banner.setup()
	
	-- Get the hierarchy from the selected bone.
	button btnSelectBones "Grab children of selected bone" width:180 height:30
	button btnGenerateRig "Generate the rig" width:180 height:30
	button btnAddBones "A-D-D BONES TO M-E-S-H" width:180 height:30
	
	local boneSelection = undefined
	
	on btnSelectBones pressed do boneSelection = getBoneSelection()
	
	on btnGenerateRig pressed do 
	(
		if boneSelection != undefined then
		(	
			for bonePairs = 1 to boneSelection.count do
			(	
				parentbone = boneSelection[bonePairs][1]
				theDriver = boneSelection[bonePairs][2]
				createrollboneEXP parentbone theDriver theAxis
			)
		)	
	)	
	
	on btnAddBones pressed do messagebox "Not done this bit yet, add them manually!"
	
)



createDialog vehicleRoofExpressionTool 200 200 --pos:[1150, 100] --main window size






