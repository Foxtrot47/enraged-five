--  AUTH: Allan Hayburn  - Rockstar San Diego:  Allan.Hayburn@rockstarsandiego.com
--  1.0 - 08-10-2012
--  Sets object shadow attributes
-- Update Oct 2012
--=============================================================--


filein (RsConfigGetWildWestDir() + "script/3dsmax/_config_files/wildwest_header.ms")

try(destroyDialog rsShadowAttribute_Rollout)catch()
rollout rsShadowAttribute_Rollout "Shadow Attribute Editor"
(		
	-- UI
	-- Rockstar Banner
	dotNetControl rsBannerPanel "Panel" pos:[0,0] height:32 width:rsShadowAttribute_Rollout.width
	local banner = makeRsBanner dn_Panel:rsBannerPanel wiki:"Shadow Attribute Editor" filename:(getThisScriptFilename())
		
	groupBox grp_Unhidden "Unhidden:" width:335 height:505	
	dotNetControl lv_objects "System.Windows.Forms.ListView" width:310 height:300 align:#left pos:[25,60]

	checkbox chk_1 "1."  pos:[25,370]
	label lbl_1 "|"  pos:[181,365] 
	label lbl_1a "Dont Cast Shadows------"  pos:[62,371]
		
	checkbox chk_2 "2."  pos:[25,385] 
	label lbl_2 "|"  pos:[201,380] 			
	label lbl_2a "Shadow Only-------------------"  pos:[62,386] 
	label lbl_2b "|"  pos:[201,365] 
		
	checkbox chk_3 "3."  pos:[25,400] 
	label lbl_3a "Dont Render in Shadows-----------"  pos:[62,401] 
	label lbl_3 "|"  pos:[221,365] 	
	label lbl_3b "|"  pos:[221,380] 
	label lbl_3c "|"  pos:[221,395] 
		
	checkbox chk_4 "4."  pos:[25,415] 
	label lbl_4a "Only Render in Shadows----------------"  pos:[62,416] 
	label lbl_4 "|"  pos:[241,365] 
	label lbl_4b "|"  pos:[241,380] 
	label lbl_4c "|"  pos:[241,395] 
	label lbl_4d "|"  pos:[241,410] 
	
	checkbox chk_5 "5."  pos:[25,430] 
	label lbl_5a "Static Shadows--------------------------------"  pos:[62,431] 
	label lbl_5 "|"  pos:[261,365] 
	label lbl_5b "|"  pos:[261,380] 
	label lbl_5c "|"  pos:[261,395] 
	label lbl_5d "|"  pos:[261,410] 
	label lbl_5e "|"  pos:[261,425]
	
	checkbox chk_6 "6."  pos:[25,445] 
	label lbl_6a "Dynamic Shadows----------------------------------"  pos:[62,446] 
	label lbl_6 "|"  pos:[281,365] 
	label lbl_6b "|"  pos:[281,380] 
	label lbl_6c "|"  pos:[281,395] 
	label lbl_6d "|"  pos:[281,410]
	label lbl_6e "|"  pos:[281,425] 
	label lbl_6f "|"  pos:[281,440] 
	
	checkbox chk_7 "7."  pos:[25,460] 
	label lbl_7a "Cast texture shadows----------------------------------"  pos:[62,461] 
	label lbl_7 "|"  pos:[301,365] 
	label lbl_7b "|"  pos:[301,380] 
	label lbl_7c "|"  pos:[301,395] 
	label lbl_7d "|"  pos:[301,410] 
	label lbl_7e "|"  pos:[301,425] 
	label lbl_7f "|"  pos:[301,440] 
	label lbl_7g "|"  pos:[301,455] 

	button btn_SelectClear "Clear Selection" width:150  across:2 align:#left offset:[10,10]
	button btn_SelectChk "Select Checked" width:150   align:#right offset:[-10,10]
	button btn_Hide "Hide Selected" width:150   align:#left  across:2 offset:[10,0]
	button btn_Refresh "Refresh" width:150  align:#right offset:[-10,0]

	groupBox grp_Hidden "Hidden:" width:335 height:262 offset:[0,15]	
	dotNetControl lv_HiddenObjects "System.Windows.Forms.ListView" width:310 height:200 align:#left offset:[12,-245] 
	button btn_unHide "Unhide Selected" width:120 offset:[-10,3] align:#right
	
	-- LOCALS
	-- setup the treeview colors
	local ColorWhite = (dotNetClass "System.Drawing.Color").fromARGB 255.0 255.0 255.0
	local ColorBlack = (dotNetClass "System.Drawing.Color").fromARGB 0.0 0.0 0.0
	local ColorGrey = (dotNetClass "System.Drawing.Color").fromARGB 80.0 80.0 80.0
	local ColorBlue = (dotNetClass "System.Drawing.Color").fromARGB 51.0 153.0 255.0
	
	local attributeList = #("Dont cast shadows", "Shadow Only", "Dont Render In Shadows","Only Render In Shadows", "Static Shadows", "Dynamic Shadows", "Cast texture shadows")
	local unHiddenArray = #(#("Object Name", "1", "2", "3","4","5","6","7"),#(148,20,20,20,20,20,20,20))
	local hiddenArray = #(#("Object Name"),#(303))
	local theSubArray = #(#(),#(),#(),#(),#(),#(),#())
	local	theChkArray = #(chk_1,chk_2,chk_3,chk_4,chk_5,chk_6,chk_7) 
	local theCheckedArray	= #()
	
	--FUNCTIONS		
	fn initListView lv theArray =
	(
		lv.backcolor = ColorGrey
		lv.forecolor = Colorwhite
		lv.checkboxes = false
		lv.gridLines = false 
		--lv.Columns = false 
		lv.View = (dotNetClass "System.Windows.Forms.View").details
		lv.fullRowSelect = true
		layout_def = theArray[1]
		layout_width = theArray[2]
		for i in 1 to layout_def.count do
		lv.Columns.add  layout_def[i] layout_width[i] 
	)
	
	fn getAttributes theObj AttrNum = 
	(
		local theAttribute = (getAttr theObj AttrNum)
		local attrAsString = (toupper (substring (theAttribute as string) 1 1))
		attrAsString
	)
	
	fn fillInSpreadSheet lv hidden:false =
	(		
		theRange = #() --array to collect the list items
		for o in objects where getattrclass o == "Gta Object" do		
		(
			if hidden ==false then -- fills the unhidden lv with unhidden obs
			(	
				if o.isHidden == false then 
				(
					li = dotNetObject "System.Windows.Forms.ListViewItem" o.name -- create a ListViewItem of object's name:
					for i in 1 to attributeList.count do -- add all the sub-item string values:
					(
						local theVal =	(getAttributes o (GetAttrIndex "Gta Object" attributeList[i]))
						if theVal == "F" then 
						(
							theVal= "" 						
						) else (
							theVal = "x"						
						)
						sub_li = li.SubItems.add theVal
					)
					append theRange li--we add the list item to the array
				)				
			) else ( -- fills the hidden lv with hidden obs
				if o.isHidden == true then 
				(
					li = dotNetObject "System.Windows.Forms.ListViewItem" o.name -- create a ListViewItem of object's name:
					append theRange li--we add the list item to the array
				)				
			)		
		) 
		lv.Items.AddRange theRange--populate ListView
	)		

	fn reInit lv hidden:false =
	(
		lv.clear()		
		if hidden == false then 
		(
			initListView lv unHiddenArray
			fillInSpreadSheet lv hidden:hidden
		) else (
			initListView lv hiddenArray
			fillInSpreadSheet lv hidden:hidden
		)
	)
	
	fn genCheckedBoxes = 
	(
		theCheckedArray = #()
		for i in 1 to theChkArray.count do
		(
			if theChkArray[i].checked == true then append theCheckedArray 1 else append theCheckedArray 0
		)
		theCheckedArray
	)
	
	fn genCheckedItems theItem = 
	(
		theCheckedItems = #()
		for i in 1 to theChkArray.count do
		(
			if theItem.subitems.item[i].text == "x" then append theCheckedItems 1 else append theCheckedItems 0
		)
		theCheckedItems
	)
	
	fn clearChecks = 
	(
		for i in 1 to theChkArray.count do
		(
			theChkArray[i].checked = false
		)
	)
	
	fn doCheckBox theNum value:false= 
	(		
		theChkArray[theNum].checked = value
	)
	
	fn setAttributes theItem theAttr theVal = 
	(
		theUncheck = case theAttr of
		(
			1 : 2
			2 : 1
			3 : 4
			4 : 3	
		)
		local theobj = getnodebyname theItem.text
		if theobj != undefined then
		(
		--	local theVal =	(getAttributes theobj (GetAttrIndex "Gta Object" attributeList[theAttr]))
			if theVal == true then 
			(
				theTextVal= "x" 
				(setAttr theobj (GetAttrIndex "Gta Object" attributeList[theAttr]) true)
			) else (
				theTextVal = ""
				(setAttr theobj (GetAttrIndex "Gta Object" attributeList[theAttr]) false)
			)
			theItem.subitems.item[theAttr].text = theTextVal
 			if theUncheck != undefined then
			(
			if theVal == false then 
			(
					theTextVal= "x" 
					(setAttr theobj (GetAttrIndex "Gta Object" attributeList[theUncheck]) true)
					doCheckBox theUncheck value:true				
				) else (
					theTextVal = ""
					(setAttr theobj (GetAttrIndex "Gta Object" attributeList[theUncheck]) false)					
					theChkArray[theUncheck].checked = false
					doCheckBox theUncheck value:false		
				)
				theItem.subitems.item[theUncheck].text = theTextVal
 			)
		)
	)
	
	-- EVENTS
	on rsShadowAttribute_Rollout open do
	(
		banner.setup() -- add the banner
		initListView lv_objects unHiddenArray
		fillInSpreadSheet lv_objects hidden:false
		initListView lv_HiddenObjects hiddenArray
		fillInSpreadSheet lv_HiddenObjects hidden:true		
	)
	
	on btn_SelectChk pressed do 
	(
		checkBoxes = genCheckedBoxes()		
		theListItems = lv_objects.items
	--	lv_objects.SelectedItems.Clear()
		for i in 0 to theListItems.count - 1 do
		(
			itemChecks = genCheckedItems theListItems.item[i]
			if (itemChecks as string) == (checkBoxes as string) then
			(
				theListItems.item[i].selected = true
				theListItems.item[i].backcolor =colorBlue
			)
		)
	)
	
	on btn_SelectClear pressed do
	(
		for i in 0 to (lv_objects.items.count - 1) do
		(
			lv_objects.items.item[i].backColor = ColorGrey				
		)
		lv_objects.SelectedItems.Clear()
		clearChecks()
	)

	on chk_1 changed val do
	(
		for i in 0 to lv_objects.SelectedItems.count - 1 do
		(
			setAttributes lv_objects.SelectedItems.item[i] 1 val
		)		
	)
	
	on chk_2 changed val do
	(
		for i in 0 to lv_objects.SelectedItems.count - 1 do
		(
			setAttributes lv_objects.SelectedItems.item[i] 2 val
		)		
	)
	
	on chk_3 changed val do
	(
		for i in 0 to lv_objects.SelectedItems.count - 1 do
		(
			setAttributes lv_objects.SelectedItems.item[i] 3 val
		)		
	)
	
	on chk_4 changed val do
	(
		for i in 0 to lv_objects.SelectedItems.count - 1 do
		(
			setAttributes lv_objects.SelectedItems.item[i] 4 val
		)		
	)
	
	on chk_5 changed val do
	(
		for i in 0 to lv_objects.SelectedItems.count - 1 do
		(
			setAttributes lv_objects.SelectedItems.item[i] 5 val
		)		
	)
	
	on chk_6 changed val do
	(
		for i in 0 to lv_objects.SelectedItems.count - 1 do
		(
			setAttributes lv_objects.SelectedItems.item[i] 6 val
		)		
	)
	
	on chk_7 changed val do
	(
		for i in 0 to lv_objects.SelectedItems.count - 1 do
		(
			setAttributes lv_objects.SelectedItems.item[i] 7 val
		)
	)
		
	on btn_Hide pressed do
	(
		for i in 0 to (lv_objects.SelectedItems.count - 1) do
		(
			hide (getnodebyname lv_objects.SelectedItems.item[i].text)			
		)
		reInit lv_objects hidden:false
		reInit lv_hiddenObjects hidden:true
		clearChecks()
	)
	
	on btn_unHide pressed do
	(
		for i in 0 to (lv_HiddenObjects.SelectedItems.count - 1) do
		(
			unhide (getnodebyname lv_HiddenObjects.SelectedItems.item[i].text)			
		)
		reInit lv_objects hidden:false
		reInit lv_hiddenObjects hidden:true
		clearChecks()
	)
	
	on btn_Refresh pressed do
	(
		reInit lv_objects hidden:false
		reInit lv_hiddenObjects hidden:true
		clearChecks()
	)

	on lv_objects mousedown lv_objects arg do
	(
		if arg.button==arg.button.right then
		(				
				for i in 0 to (lv_objects.items.count - 1) do
				(
					lv_objects.items.item[i].backColor = ColorGrey				
				)
				lv_objects.SelectedItems.Clear()
				clearChecks()
		)
		if arg.button==arg.button.left then
		(	
			if  lv_objects.modifierkeys.value__ != 131072  and  lv_objects.modifierkeys.value__ !=196608 then 
			(
				lv_objects.SelectedItems.Clear()
				for i in 0 to (lv_objects.items.count - 1) do
				(
					lv_objects.items.item[i].backColor = ColorGrey				
				)
			)
		)
	)
	
	on lv_objects mouseUp lv_objects arg do
	(
		if arg.button==arg.button.left then
		(	
			for i in 0 to (lv_objects.items.count - 1) do
			(
				if lv_objects.items.item[i].selected == true	then lv_objects.items.item[i].backColor = ColorBlue	
			)
			--format "% Items Selected\n" lv_objects.SelectedItems.count 
			theSubArray = #(#(),#(),#(),#(),#(),#(),#())
			for sel in 0 to (lv_objects.SelectedItems.count - 1) do
			(
				theSubItems = lv_objects.SelectedItems.item[sel].subitems
				for subs in 1 to theSubItems.count - 1 do
				(
					if theSubItems.item[subs].text != "" then
					(
						doCheckBox subs value:true
						appendIfUnique theSubArray[subs] theSubItems.item[subs].text
					) else (
						doCheckBox subs value:false
						appendIfUnique theSubArray[subs] theSubItems.item[subs].text
					)
				)
			)
			for i in 1 to theSubArray.count do
			(
				if theSubArray[i].count > 1 then theChkArray[i].checked = false
			)
		)

	)
)
createDialog rsShadowAttribute_Rollout 360 830 style:#(#style_resizing,#style_titlebar, #style_toolwindow, #style_sysmenu)
