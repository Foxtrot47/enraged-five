--LightSwitch

try(destroyDialog LightSwitcher)catch()
rollout LightSwitcher "LightSwitcher"
(
	group "Light Switch"
	(
		button btn_switch_gl "Game Lights" width:90 offset:[-50,0] toolTip:"Switch to this layer"
		button btn_sel_gl "Sel" width:30  offset:[14,-26] toolTip:"Select the lights on this layer"
		button btn_add_gl "Add" width:30  offset:[46,-26]  toolTip:"Add selected lights to this layer"
		--button btn_visible_gl "S/H" width:30  offset:[78,-26] toolTip:"Show/hide the lights in this layer"
		checkbutton btn_visible_gl "S/H" width:30  offset:[78,-26] checked:false
			---------------------------------------------------------------
		button btn_switch_rl "Render Lights" width:90 offset:[-50,0] toolTip:"Switch to this layer"
		button btn_sel_rl "Sel" width:30  offset:[14,-26] toolTip:"Select the lights on this layer"
		button btn_add_rl "Add" width:30  offset:[46,-26]  toolTip:"Add selected lights to this layer"
		--button btn_visible_gl "S/H" width:30  offset:[78,-26] toolTip:"Show/hide the lights in this layer"
		checkbutton btn_visible_rl "S/H" width:30  offset:[78,-26] checked:false
			---------------------------------------------------------------
		button btn_switch_cl "Colour Lights" width:90 offset:[-50,0] toolTip:"Switch to this layer"
		button btn_sel_cl "Sel" width:30 offset:[14,-26] toolTip:"Select the lights on this layer"
		button btn_add_cl "Add" width:30  offset:[46,-26]  toolTip:"Add selected lights to this layer"
		--button btn_visible_cl "S/H" width:30  offset:[78,-26] toolTip:"Show/hide the lights in this layer"
		checkbutton btn_visible_cl "S/H" width:30  offset:[78,-26] checked:false
			---------------------------------------------------------------
		button btn_switch_il "Illum Lights" width:90 offset:[-50,0] toolTip:"Switch to this layer"
		button btn_sel_il "Sel" width:30 offset:[14,-26] toolTip:"Select the lights on this layer"
		button btn_add_il "Add" width:30 offset:[46,-26]  toolTip:"Add selected lights to this layer"
		--button btn_visible_il "S/H" width:30 offset:[78,-26] toolTip:"Show/hide the lights in this layer"
		checkbutton btn_visible_il "S/H" width:30  offset:[78,-26] checked:false		
	)
	
	fn makeLayers =
	(
		layerNames = #("L_GameLights", "L_RenderLights", "L_VertColour","L_VertIllum")
		for i in layerNames do 
		(
			LayerManager.newLayerFromName i			
			layer = LayerManager.getLayerFromName i
			
			if i == "L_GameLights" then btn_visible_gl.checked = (layer.on)
			if i == "L_RenderLights" then btn_visible_rl.checked = (layer.on)
			if i == "L_VertColour" then btn_visible_cl.checked = (layer.on)
			if i == "L_VertIllum" then btn_visible_il.checked = (layer.on)
		)			
	)
	
	----------------------------------------------------------
	-- LIGHT ON AND OFFS FOR DIFFERENT TYPES
	----------------------------------------------------------
	fn toggleLights i arg = 
	(
		 try (i.enabled = arg) catch() -- PHOTOMETRIC
		 try (i.on = arg) catch() -- NORMAL
		 try (i.lightEnabled = arg) catch() -- RAGE
	)
	
	fn layerSwitch switches title =
	(
		--mh_bakedLightingTool.title = ("BLT " + title)
		
		layer = LayerManager.getLayerFromName "L_GameLights"
		layer.nodes &theLights
		for i in theLights do toggleLights i switches[1]
	
		layer = LayerManager.getLayerFromName "L_RenderLights"
		layer.nodes &theLights
		for i in theLights do toggleLights i switches[2]
		
		layer = LayerManager.getLayerFromName "L_VertColour"
		layer.nodes &theLights
		for i in theLights do toggleLights i switches[3]
		
		layer = LayerManager.getLayerFromName "L_VertIllum"
		layer.nodes &theLights
		for i in theLights do toggleLights i switches[4]

		redrawViews() 	
	)
	
	----------------------------------------------------------
	-- SELECT LAYER LIGHTS
	----------------------------------------------------------
	fn selectLayer layerName =
	(
		layer = layermanager.getLayerFromName layerName
		layer.select true
	)
	
	----------------------------------------------------------
	-- VISIBILITY FOR THE LAYERS
	----------------------------------------------------------
	fn layerToggle layerName  = 
	(
		layer = LayerManager.getLayerFromName layerName
		if layer !=undefined do layer.on = not(layer.on)
		
		if layerName == "L_GameLights" then btn_visible_gl.checked = (layer.on)
		if layerName == "L_RenderLights" then btn_visible_rl.checked = (layer.on)
		if layerName == "L_VertColour" then btn_visible_cl.checked = (layer.on)
		if layerName == "L_VertIllum" then btn_visible_il.checked = (layer.on)
		
		redrawViews() 	
	)
	
	----------------------------------------------------------
	-- ADD LIGHTS TO LAYER
	----------------------------------------------------------
	fn addToLayer layerName =
	(
		sel = selection
		editLayer = LayerManager.getLayerFromName layerName
		for i in sel do if (superClassof i)==light then editLayer.addNode i
	)
	
	------------------------------------------------------------------------
	-- EVENTS
	------------------------------------------------------------------------
	on btn_switch_gl pressed do layerSwitch #(true, false, false, false) "[Game]"
	on btn_sel_gl pressed do selectLayer "L_GameLights"
	on btn_visible_gl changed state do layerToggle "L_GameLights" 
	on btn_add_gl pressed do addToLayer "L_GameLights"
	------------------------------------------------------------------
	on btn_switch_rl pressed do layerSwitch #(false, true, false, false) "[Render]"
	on btn_sel_rl pressed do selectLayer "L_RenderLights"
	on btn_visible_rl changed state do layerToggle "L_RenderLights"
	on btn_add_rl pressed do addToLayer "L_RenderLights"
	---------------------------------------------------------------
	on btn_switch_cl pressed do layerSwitch #(false, false, true, false) "[Colour]"
	on btn_sel_cl pressed do selectLayer "L_VertColour"
	on btn_visible_cl changed state do  layerToggle "L_VertColour"
	on btn_add_cl pressed do addToLayer "L_VertColour"
	---------------------------------------------------------------
	on btn_switch_il pressed do layerSwitch #(false, false, false, true) "[Illum]"
	on btn_sel_il pressed do selectLayer "L_VertIllum"
	on btn_visible_il changed state do layerToggle "L_VertIllum"
	on btn_add_il pressed do addToLayer "L_VertIllum"	
	
	on LightSwitcher open do
	(
		makeLayers()
	)
	
)

createDialog LightSwitcher width:210 style:#( #style_sysmenu, #style_toolwindow) 