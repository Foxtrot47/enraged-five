--------------------------------------------------------------------
-- NAME: RSTA_image_rankedColours
-- DATE: 14.11.13
-- AUTHOR: Matt Harrad	
-- ABOUT: Pulls out the most used colours in an image.
--------------------------------------------------------------------
struct RSTA_image_rankedColours
(
	---------------------------------------------------------
	-- VARIABLES 
	---------------------------------------------------------
	source_bmp,
	source_size = 512,
	progBarUI,
	pixelArray_all,
	pixelArray_grouped,
	colourDistance = 50,
	---------------------------------------------------------
	-- FUNCTIONS 
	---------------------------------------------------------
	fn get_pixelArray_all = 
	(
		if (source_bmp != undefined) do
		(
			-- RESIZE IMAGE
			theNewMap = bitmap source_size source_size	
			source_bmp = (copy source_bmp theNewMap)		
			----------------------------------------------
			pixelArray_all=#()
			local colour_array =#()
			local w = source_bmp.width
			local h = source_bmp.height

			for i = 0 to w-1 do 
			(
				for j=0 to h-1 do
				(
					p = (getPixels source_bmp [i,j] 1)[1]			
					idx = findItem colour_array p 
					if (idx == 0) then 
					(
						append pixelArray_all (dataPair colour:p count:1)	
						append colour_array p
					)
					else pixelArray_all[idx].count +=1		
				)			
			)
		)
	),
	-----------------------------------------------------------------------------
	fn groupColours colourCount_array =
	(
		local groupped_array =#()
		--- LOOP THE COLOURS
		for c in colourCount_array do
		(
			match_idx = undefined	
			-- LOOP THROUGH THE GROUPS
			for i=1 to groupped_array.count do
			(
				g = groupped_array[i]
				d = distance (c.colour as point3) (g.colour as point3)
				if d < colourDistance do match_idx = i
			)
			-- IS IT A MATCH?
			if (match_idx != undefined) then groupped_array[match_idx].count += c.count
			else append groupped_array (dataPair colour:c.colour count:c.count)			

		)	
		----------------------------------------------	
		return groupped_array
	),
	--------------------------------------------------------------------------
	fn sortByCount a b = 
	(
		if a.count > b.count then -1 
		else if a.count < b.count then 1 
		else 0 
	),
	-------------------------------------------------------------------------
	fn get_topColours count = 
	(	
		get_pixelArray_all()
		
		qsort pixelArray_all sortByCount 
		pixelArray_grouped = groupColours pixelArray_all 
		qsort pixelArray_grouped sortByCount 
		
		if count > pixelArray_grouped.count do count = pixelArray_grouped.count
		return (for idx=1 to count collect pixelArray_grouped[idx].colour)		
	)
)