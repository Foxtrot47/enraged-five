--------------------------------------------------------------------
-- NAME: RSTA_ray_faceToTextureSample 
-- DATE: 14.11.13
-- AUTHOR: Matt Harrad
-- ABOUT: Cast a ray from a face to object and gets its pixel colour from the texture.
--------------------------------------------------------------------
struct RSTA_ray_faceToTextureSample
(
	-------------------------------------------------------------------
	-- VARIBLES 
	-------------------------------------------------------------------
	target = undefined,
	source = undefined, 
	sourceTexture = undefined,
	ray_dir = [0,0,1],
	-------------------------------------------------------------------
	-- FUNCTIONS 
	-------------------------------------------------------------------
	fn rayToSource startPos =
	(
		-- MAKE THE RAY
		local theRay = ray startPos ray_dir	
		local hitData = intersectRayEx source theRay
		-------------------------------------------------------------------------
		return hitData
	),
	--------------------------------------------------------------------
	fn hitData_to_Colour hitData = 
	(
		if (hitData != undefined) then
		(
			-- WHATS IN THE HIT DATA
			local hitRay = hitData[1]
			local faceIdx = hitData[2]
			local barry = hitData[3]
			-- GET THE TEXTURE FACE
			local textureFace = getTVFace source faceIdx	
			-- GET THE UVW VERTS OF THE FACE
			local tVert_1 = getTVert source textureFace.x
			local tVert_2 = getTVert source textureFace.y
			local tVert_3 = getTVert source textureFace.z
			-- CALCULATE THE TEXTURE VERTICES AT POINT OF INTERSECTION FROM  THE BARYCENTRIC COORDINATES
			local tv = tVert_1*barry.x + tVert_2*barry.y + tVert_3*barry.z			
			local textureCoords = [(tv.x)*(sourceTexture.width-1),sourceTexture.height-(tv.y)*(sourceTexture.height-1)]			
			local pixelColour = (getPixels source.material.diffuseMap.bitmap textureCoords 1)[1]
			-----------------------------------------------------------------------
			return pixelColour
		)
	),
	----------------------------------------------------------------------------------------
	fn getColourPerFace faceIdx =
	(
		sourceTexture = source.material.diffuseMap.bitmap	
		local pixelColour = undefined
		local polyCenter = polyop.getFaceCenter target faceIdx
		local hitData = rayToSource polyCenter
		-- CHECK TO SEE IF THERE IS A HIT FOR THIS RAY
		if (hitData != undefined) then pixelColour = hitData_to_Colour hitData	
	
		--------------------------------------------------------------------------
		return pixelColour	
	)
)