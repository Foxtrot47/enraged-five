struct CSharp
(
	/*---------------------------------------------------
	Compiles an array of .cs file to memory. Allows construction of 'on-the-fly' .net types.
	*/---------------------------------------------------
	fn CompileToMemory SourceFileArray =
	(
		--First we process all the paths so that are correct
		for i = 1 to SourceFileArray.Count do
		(
			SourceFileArray[i] = RsMakeBackSlashes( SourceFileArray[i] )
		)

		--Set up our compiler parameters
		CompilerParameters = DotNetObject "System.CodeDom.Compiler.CompilerParameters"
		CompilerParameters.GenerateExecutable = false
		CompilerParameters.GenerateInMemory = false
		CompilerParameters.CompilerOptions = "/platform:x64"
		
		--Add in all the assemblies being used in this domain (Max)
		--This should cover everything that we need to use.
		AppDomain = DotNetClass "System.AppDomain"
		AppDomainAssemblies = AppDomain.CurrentDomain.GetAssemblies()
		for Assembly in AppDomainAssemblies do
		(
			--Don't load dynamic assemblies as this causes problems
			if not( Assembly.IsDynamic) then
			(
			CompilerParameters.ReferencedAssemblies.Add(Assembly.Location);
			)
		)
		
		--Setup our compiler
		CodeDomProvider = DotNetClass "System.CodeDom.Compiler.CodeDomProvider"
		CodeDomProvider = CodeDomProvider.CreateProvider("CSharp")
		CompilerResult = CodeDomProvider.CompileAssemblyFromFile CompilerParameters SourceFileArray
		
		if not ( CompilerResult.Errors.HasErrors ) then
		(
			--If we don't have any errors then return the assembly
			return CompilerResult.CompiledAssembly
		)
		else
		(
			--If we do have errors then print them to the listener
			for i = 1 to CompilerResult.Errors.Count do
			(
				CompileError = CompilerResult.Errors.Item(i-1)
				Format "Compile Error: %\n" CompileError.ErrorText
			)
		)		
	)
)

struct WPF
(
	/*---------------------------------------------------
	Reads in a Xaml file and returns the root element. Almost always a Window.
	*/---------------------------------------------------
	fn ReadXamlFile XamlFile =
	(
		XamlFile = RsMakeBackSlashes(XamlFile)
		XamlReader = DotNetClass "Windows.Markup.XamlReader"
		XamlFileStream = DotNetObject "IO.FileStream" XamlFile (DotNetClass "IO.FileMode").Open (DotNetClass "IO.FileAccess").Read
		WPFElement = XamlReader.Load XamlFileStream
		return WPFElement
	),
	
	fn GetElement RootElement TargetElementName =
	(
		return RootElement.FindName TargetElementName
	)
)

-----------------------------------------------------------------
-- ADDS THE EVENT HANDLERS TO A DOTNET CONTROL
-----------------------------------------------------------------
struct dn_window
(
	fn disable_accelerators = enableAccelerators = false,
	fn enable_accelerators = enableAccelerators = true,	
	fn setup dn_control =
	(
		dotNet.addEventHandler dn_control "Deactivated" enable_accelerators
		dotNet.addEventHandler dn_control "Activated" disable_accelerators
		dotNet.setLifetimeControl dn_control #dotnet
		
		dn_control.ChangeOwner (DotNetObject "System.IntPtr" (Windows.GetMAXHWND()))	
	)
)
dn_window = dn_window()


------------------------------------------------------------------------------------------------------------
-- RsShowMaxform:
--	Function to show a given MaxCustomControls.MaxForm either modally or non-modally.
--		This method ensures that the Max window has focus at the moment of form-creation.
--			This is a workaround for a Max bug that stops new windows from always staying in 
--			front of Max, if the user tabs to another application before the new window is shown.
------------------------------------------------------------------------------------------------------------
fn RsShowMaxform theForm modal:False = 
(
	-- Get HWND for first viewport-panel:
	local viewportHwnd = undefined
	for w in (windows.getChildrenHWND #max) while (viewportHwnd == undefined) do 
	(
		if (w[4] == "ViewPanel") do 
		(
			local hwnds = (windows.getChildrenHWND w[1])
			viewportHwnd = hwnds[1][1]
		)
	)
	
	-- Simulate middle-clicking viewport, to give Max focus if the user has tabbed away:
	if (viewportHwnd != undefined) do 
	(
		local WM_MBUTTONDOWN = 0x0207
		local WM_MBUTTONUP = 0x0208
		Windows.SendMessage viewportHwnd WM_MBUTTONDOWN 0 0
		Windows.SendMessage viewportHwnd WM_MBUTTONUP 0 0
	)
	
	-- Show requested MaxForm
	if modal then 
	(
		-- Show modal dialog
		theForm.ShowModal()
		
		-- url:bugstar:5794906 - Temporarily show a non-modal dialog, as workaround for max-crash
		local tempForm = RS_NoFocusDotNetForm()
		tempForm.FormBorderStyle = tempForm.FormBorderStyle.FixedToolWindow
		tempForm.controlBox = False
		tempForm.Show()
		tempForm.Close()
	)
	else 
	(
		theForm.ShowModeless()
	)
)

------------------------------------------------------------------------------------------------------------
-- RS_convertBitmapToBytes
-- Converts a Max bitmap into a BMP-format memorystram, for use with dotNet objects
--	(overrides core-tools version of function, with updates from Americas)
------------------------------------------------------------------------------------------------------------
fn RS_convertBitmapToBytes imageBmp doAlpha:true = 
(	
	-- Currently only generates non-alpha bitmaps
	local BitsPerPixel = if doAlpha then 32 else 24
	local BytesPerPixel = (BitsPerPixel / 8)
	
	local ImgWidth = ImageBmp.Width
	
	-- Number of bytes required to encode a row of colours:
	local RowSize = (ImgWidth * BytesPerPixel)
	
	-- Number of extra padding-bytes to be added to end of each row:
	local RowPadSize = Integer (mod RowSize 4)
	
	-- Size of row-data:
	local FullRowSize = Integer64 (RowSize + RowPadSize)
	
	-- Get overall size of pixel-data:
	local PixelDataSize = (FullRowSize * ImageBmp.Height)
	
	local HeaderSize = 54 -- Header is always this size
	
	-- Overall size of bitmap-data: 
	local BmpSize = (HeaderSize + PixelDataSize)
	
	-- Build Bmp header-array:
	local BmpHeader = #()
	(
		-- Bitmap File Header: 14 bytes
		--	(2 bytes) "BM"
		join BmpHeader #(66,77)
		--	(4 bytes) filesize
		join BmpHeader (RS_intTo4Bytes BmpSize)
		--	(4 bytes) reserved
		join BmpHeader #(0,0,0,0)
		--	(4 bytes) data-start address (unused?)
		join BmpHeader (RS_intTo4Bytes HeaderSize)
		
		-- DIB Header:
		--	(4 bytes) Size of DIB header: (40)
		join BmpHeader #(40,0,0,0)
		--	(4 bytes) Bitmap width:
		join BmpHeader (RS_intTo4Bytes ImageBmp.Width)
		--	(4 bytes) Bitmap Height:
		join BmpHeader (RS_intTo4Bytes ImageBmp.Height)
		--	(2 bytes) Colour-planes
		--	(2 bytes) Bits-per-pixel
		--	(4 bytes) Compression method
		join BmpHeader #(1,0, BitsPerPixel,0, 0,0,0,0)
		--	(4 bytes) Size of raw bitmap data
		join BmpHeader (RS_intTo4Bytes PixelDataSize)
		--	(4 bytes) horizontal resolution (pixel per meter)
		--	(4 bytes) vertical resolution (pixel per meter)
		--	(4 bytes) colours in palette
		--	(4 bytes) number of important colours
		join BmpHeader #(32,46,0,0, 32,46,0,0, 0,0,0,0, 0,0,0,0)
		
		-- Add imagesize to header, now we know how big the header is:
		local BmpSizeBytes = (RS_intTo4Bytes BmpSize)
		for n = 1 to 4 do 
		(
			BmpHeader[n + 2] = BmpSizeBytes[n]
		)
	)
	
	-- Create memorystream for bitmap:
	local MemStream = (DotnetObject "System.IO.MemoryStream" BmpSize)
	
	-- Write header to stream:
	MemStream.Write BmpHeader 0 BmpHeader.Count
	
	for rowNum = (ImageBmp.Height - 1) to 0 by -1 do 
	(
		local RowData = #()
		for Pixel in (GetPixels ImageBmp [0, RowNum] ImgWidth) do 
		(
			if doAlpha then 
			(
				join RowData #(Pixel.B, Pixel.G, Pixel.R, Pixel.A)
			)
			else 
			(
				join RowData #(Pixel.B, Pixel.G, Pixel.R)
			)
		)
		
		MemStream.Write RowData 0 FullRowSize
	)
	
	return MemStream
)

------------------------------------------------------------------------------------------------------------
-- RS_bitmapToImage
-- Converts a Max bitmap into a dotNet image
--	(overrides core-tools version of function, with updates from Americas)
------------------------------------------------------------------------------------------------------------
fn RS_bitmapToImage inBmp =
(
	local memstream = RS_convertBitmapToBytes inBmp
	
	outImg = (dotnetclass "system.drawing.image").fromstream memstream
	memstream.close()
	
	return outImg
)