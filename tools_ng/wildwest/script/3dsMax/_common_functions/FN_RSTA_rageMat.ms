struct rageMatVar 
(
	name, 
	value,
	type,
	idx
)

struct rsta_rageMat 
(	
	--------------------------------------------------------------------------------------------------
	-- UTILITY 
	--------------------------------------------------------------------------------------------------
	-- GET ALL THE ATTRS
	fn getAttrData rageMat =
	(
		local attrArray = #()
		for n=1 to (RstGetVariableCount rageMat) do 
		(
			local rmv = rageMatVar idx:n
			rmv.name = RstGetVariableName rageMat n
			rmv.value = RstGetVariable rageMat n
			rmv.type = RstGetVariableType rageMat n 
			append attrArray rmv
		)
		return attrArray
	),
	
	-- IS THIS A RAGE SHADER
	fn isRageMat rageMat = 
	(
		if (classof rageMat == Rage_Shader) then return true
		else 
		(
			format "% isn't a rage shader.\n" rageMat.name
			return false
		)
	),
	
	-- TEST TEXMAP FOR PATTERN
	fn getTexMapPatternArray rageMat pattern =
	(
		return (for var in (getAttrData rageMat) where (var.type == "texmap") AND (matchPattern var.name pattern:pattern) collect var)
	),
	
	-- SET BY PATTERN 
	fn setTexMapByPattern rageMat pattern value idx: =
	(
		local wasSet = false
		for var in (getAttrData rageMat) where (var.type == "texmap") AND (matchPattern var.name pattern:pattern) do
		(
			if (idx == unsupplied OR var.idx == idx) do
			(
				RstSetVariable rageMat var.idx value 
				wasSet = true
			)
		)
		return wasSet
	),
	
	-- SET TEXMAP BY PATTERN AND IDX
	fn setTextMapByPatternIdx rageMat pattern idx value = 
	(		
		return (setTexMapByPattern rageMat pattern value idx:idx)
	),

	-- LAUNCH THE WIKI
	fn help =
	(
		Shelllaunch  "https://devstar.rockstargames.com/wiki/index.php/Rsta_rageMat" ""
	),
	
	--------------------------------------------------------------------------------------------------
	-- SHADER TYPE 
	--------------------------------------------------------------------------------------------------
	-- GET SHADER TYPE 
	fn getShaderType rageMat ext:true =
	(
		if (ext) then return (RstGetShaderName rageMat)
		else return getFilenameFile (RstGetShaderName rageMat)
	),
	
	-- SET SHADER TYPE 
	fn setShaderType rageMat type = 
	(
		if (RsShaderExists (getFilenameFile type)) do
		(
			RstSetShaderName rageMat ((getFilenameFile type) + ".sps")
			return true
		)
		return false
	),
	
	--------------------------------------------------------------------------------------------------
	-- TEX MAPS  
	--------------------------------------------------------------------------------------------------
	
	-- GET THE DIFFUSE TEXTURE 
	fn getDiffuse rageMat idx:1 = 
	(
		local diffuseArray = getTexMapPatternArray rageMat "Diffuse*"
		if (idx <= diffuseArray.count) do return diffuseArray[idx].value
		return undefined
	),	
	
	-- SET THE DIFFUSE TEXTURE 
	fn setDiffuse rageMat value idx:1 = 
	(
		return (setTextMapByPatternIdx rageMat "Diffuse*" idx value)
	),

	-- GET THE NORMAL TEXTURE 
	fn getBump rageMat idx:1 = 
	(
		local diffuseArray = getTexMapPatternArray rageMat "Bump*"
		if (idx <= diffuseArray.count) do return diffuseArray[idx].value
		return undefined
	),	
	
	-- GET THE LOOKUP TEXTURE 
	fn getLookup rageMat idx:1 = 
	(
		local diffuseArray = getTexMapPatternArray rageMat "Lookup*"
		if (idx <= diffuseArray.count) do return diffuseArray[idx].value
		return undefined
	),	
	
	-- SET THE NORMAL TEXTURE 
	fn setBump rageMat value idx:1 = 
	(
		return (setTextMapByPatternIdx rageMat "Bump*" idx value)
	),	

	-- GET THE SPEC TEXTURE 
	fn getSpec rageMat idx:1 = 
	(
		local diffuseArray = getTexMapPatternArray rageMat "Specular*"
		if (idx <= diffuseArray.count) do return diffuseArray[idx].value
		return undefined
	),	
	
	-- SET THE SPEC TEXTURE 
	fn setSpec rageMat value idx:1 = 
	(
		return (setTextMapByPatternIdx rageMat "Specular*" idx value)
	),			
	
	-------------------------------------------------------------------------------------------------
	-- GET/SET BY IDX 
	------------------------------------------------------------------------------------------------
	
	-- SET BY IDX
	fn setVarByIdx rageMat idx varValue = 
	(
		if (isRageMat rageMat) do for var in (getAttrData rageMat) where var.idx == idx do 
		(
			RstSetVariable rageMat var.idx varValue	
			return true
		)
		return false
	),
	
	-- GET A VARIBLE BY IDX 
	fn getVarByIdx rageMat idx =
	(
		if (isRageMat rageMat) do return (for var in (getAttrData rageMat) where var.idx == idx do return var.value)
		return undefined		
	),
	
	-- GET TYPE BY IDX 
	fn getVarTypeByIdx rageMat idx = 
	(
		if (isRageMat rageMat) do return (for var in (getAttrData rageMat) where var.idx == idx do return var.type)
		return undefined	
	),
	
	-- GET THE ALPHA BY IDX
	fn getAlphaByIdx rageMat varIdx =
	(
		if (isRageMat rageMat AND RstGetIsAlphaShader rageMat) do for var in (getAttrData rageMat) where (var.idx == varIdx) do return (RstGetAlphaTextureName rageMat var.idx)
		return undefined
	),
	
	-- GET THE ALPHA BY IDX
	fn setAlphaByIdx rageMat varIdx varValue =
	(
		if (isRageMat rageMat AND RstGetIsAlphaShader rageMat) do for var in (getAttrData rageMat) where (var.idx == varIdx) do return (RstSetAlphaTextureName rageMat var.idx varValue)
		return undefined
	),
	
	--------------------------------------------------------------------------------------------------
	-- GET/SET BY NAME 
	-------------------------------------------------------------------------------------------------
	
	-- SET A VARIBLE BY NAME 
	fn setVarByName rageMat varName varValue = 
	(
		if (isRageMat rageMat) do for var in (getAttrData rageMat) where var.name == varName do 
		(
			RstSetVariable rageMat var.idx varValue	
			return true
		)
		return false
	),
	
	-- GET A VARIBLE BY NAME 
	fn getVarByName rageMat varName =
	(
		if (isRageMat rageMat) do return (for var in (getAttrData rageMat) where var.name == varName do return var.value)
		return undefined		
	),
	
	-- GET TYPE BY NAME 
	fn getVarTypeByName rageMat varName = 
	(
		if (isRageMat rageMat) do return (for var in (getAttrData rageMat) where var.name == varName do return var.type)
		return undefined	
	),
	
	-- GET THE ALPHA BY NAME
	fn getAlphaByName rageMat varName =
	(
		if (isRageMat rageMat AND RstGetIsAlphaShader rageMat) do for var in (this.getTexMapIdx rageMat) where (var.name == varName) do return (RstGetAlphaTextureName rageMat var.idx)
		return undefined
	),
	
	-- GET THE ALPHA BY NAME
	fn setAlphaByName rageMat varName varValue =
	(
		if (isRageMat rageMat AND RstGetIsAlphaShader rageMat) do for var in (this.getTexMapIdx rageMat) where (var.name == varName) do return (RstSetAlphaTextureName rageMat var.idx varValue)
		return undefined
	),
	
	------------------------------------------------------------------------------------------------
	-- GET ALL 
	------------------------------------------------------------------------------------------------
	
	-- GET JUST THE TEXMAPS AND RESET THE INDEX'S
	fn getTexMapIdx rageMat =
	(
		if (isRageMat rageMat) do
		(
			local justTexMaps = for var in (getAttrData rageMat) where var.type == "texmap" collect var
				
			-- REMAP THE INDEX
			for i=1 to justTexMaps.count do 
			(
				justTexMaps[i].idx = i
			)
			
			return justTexMaps
		)
	),

	-- GET ALL TEXTURES
	fn getAllTextures rageMat =
	(
		if (isRageMat rageMat) do return (for var in (getAttrData rageMat) where var.type == "texmap" collect var.value)
	),	
	
	-- GET ALL THE TYPES AS ARRAY 
	fn getAllTypes rageMat = 
	(
		if (isRageMat rageMat) do return (for var in (getAttrData rageMat) collect var.type)
	), 
	
	-- RETURN AN ARRAY OF ALL THE VARIBLES ON A RAGE SHADER
	fn getAllNames rageMat = 
	(
		if (isRageMat rageMat) do return (for var in (getAttrData rageMat) collect var.name)
	),	
	
	-- GET ALL THE VARIBLES AS RAGEMATATTR
	fn getAllVars rageMat = 
	(
		if (isRageMat rageMat) do return (getAttrData rageMat)
	),
	
	-- GET ALL THE VARIBLES VALUES
	fn getAllValues rageMat = 
	(
		if (isRageMat rageMat) do return (for var in (getAttrData rageMat) collect var.value)
	),
	
	-- PRINT OUT ALL THE VARIBLES ON A RAGE SHADER
	fn printAllVars rageMat = 
	(
		if (isRageMat rageMat) do for var in (getAttrData rageMat) do format "[%] \"%\" <%> : %\n" (formattedPrint var.idx format:"02i") var.name var.type  var.value	
	)	
)
rsta_rageMat = rsta_rageMat()
