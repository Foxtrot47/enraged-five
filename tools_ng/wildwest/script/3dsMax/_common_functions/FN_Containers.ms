--FN_Containers.ms

--RSTA_ContainerData
--Author: Andy Davis
--Studio: Rockstar London
--Date: June 2014
--Description: Script gets container data
--Recycled code from X:\wildwest\script\3dsMax\Maps\containerVectorMapper.ms
--container data taken from maps.xml file
global RSTA_ContainerData

--data struct used in the RSTA_ContainerDataStruct
struct RSTA_ContainerItem
(
	name,					--string describing name as set out in the maps.xml file
	position = [0,0,0],	--location of the centre of the container
	minXY = [0,0],		--bounds
	maxXY = [0,0]		--bounds
)

struct RSTA_ContainerDataStruct
(
	mapsPath = rsconfiggettoolsdir() + "etc/content/maps.xml",
	terrainNode = undefined,
	letterArray = #(),
	numberArray = #(),
	containerSize = 512,	--default container size (correct for RDR) - adjust this to suit other projects where appropriate
	containerInfoArray = #(),	--array of RSTA_ContainerItem structs
	minXY = [0,0],
	maxXY = [0,0],
	width,
	height,
	numRows,
	numColumns,
		
	-----------------------------------------------------------------------------------
	--DATA INITIALIZATION
	-----------------------------------------------------------------------------------

	fn GetXMLNode parent attr =
	(
		for i = 0 to parent.ChildNodes.Count - 1 do
		(
			local child = parent.ChildNodes.ItemOf[i]
			if (child.attributes != undefined) then
			(
				local pathAttr = child.GetAttribute "path"
				if (pathAttr != undefined and pathAttr == attr) then
					return child
			)
		)
	),
	
	--call this function on struct instantiation to initialise the data
	fn InitData =
	(
		local xmlDoc = DotNetObject "System.XML.XMLDocument"
		xmlDoc.load this.mapsPath		
		local rootElement = xmlDoc.DocumentElement
		local artNode = undefined		
		artNode = this.GetXMLNode rootElement "$(art)/models" --find the art node in maps.xml
		
		if (artNode != undefined) then
			this.terrainNode = this.GetXMLNode artNode "terrain"
		
		if (this.terrainNode != undefined) then
		(
			this.InitLetterNumberArrays()
			this.InitContainerInfoArray()
		)
	),
	
	--sets up the containerInfoArray with ContainerInfo items
	fn InitContainerInfoArray =
	(
		local numRows = this.letterArray.count
		local numColumns = this.numberArray.count
		local halfRows = numRows / 2	--half rows and columns to get how many are left/right/above/below [0,0]
		local halfColumns = numColumns / 2
		local startX = (((halfRows * this.ContainerSize) - (this.ContainerSize / 2)) * (-1))	--find proper location to start grid
		local startY = ((halfColumns * this.ContainerSize) - (this.ContainerSize / 2))
		local containerPosition = [startX, startY, 0]
		
		--loop through columns and rows
		for c = 1 to numColumns do
		(
			containerPosition[2] = startY	--reset Y position to top for new column
			for r = 1 to numRows do
			(
				local newItem = RSTA_ContainerItem()
				local shunt = this.containerSize / 2 --distance of the container bounds from the container centre
				newItem.name = this.letterArray[r] + "_" + this.numberArray[c]
				newItem.position = [containerPosition[1], containerPosition[2], containerPosition[3]] 
				newItem.minXY = [(containerPosition[1] - shunt), (containerPosition[2] - shunt)]
				newItem.maxXY = [(containerPosition[1] + shunt), (containerPosition[2] + shunt)]
				append this.containerInfoArray newItem
				containerPosition[2] -= this.containerSize
			)
			containerPosition[1] += this.containerSize
		)
		
		--calculate the bounds of the container collection
		local first = this.containerInfoArray[1]
		local last = this.containerInfoArray[this.containerInfoArray.count]
		this.minXY = [first.minXY.x, last.minXY.y]
		this.maxXY = [last.maxXY.x, first.maxXY.y]
		this.width = numColumns * this.ContainerSize
		this.height = numRows * this.ContainerSize
		this.numRows = this.letterArray.count
		this.numColumns = this.numberArray.count
	),
	
	fn InitLetterNumberArrays =
	(		
		for i = 0 to this.terrainNode.ChildNodes.Count-1 do
		(
			local child = this.terrainNode.ChildNodes.ItemOf[i]	--loop through children
			if ( child.attributes != undefined ) then
			(
				local nameAttr = child.GetAttribute "name"
				if (nameAttr != undefined and findString nameAttr "placement" == undefined) then	--eliminate placement containers
				(
					local tokens = filterString nameAttr "_"	--break up name by underscores
					
					if ( tokens.count > 1 ) then
					(
						if ( tokens[1].count == 1 and tokens[2].count == 2 ) then	--pattern is x_xx so [1]==1 and [2]==2
						(
							appendIfUnique this.letterArray tokens[1]
							appendIfUnique this.numberArray tokens[2]
						)
					)
				)
			)
		)
	),
	
	fn PrintData =
	(
		format "Total number of containers: % rows x % columns: %\n" this.numberArray.count this.letterArray.count this.containerInfoArray.count
		format "Individual container size: %m\n" this.ContainerSize
		format "Letters: % to %\t" this.letterArray[1] this.letterArray[this.letterArray.count]
		format "Numbers: % to %\n" this.numberArray[1] this.numberArray[this.numberArray.count]
		format "MinXY: %\tMaxXY: %\n" this.minXY this.maxXY
		format "Width: %\tHeight: %\n" this.width this.height
		for item in this.containerInfoArray do
		(
			format "Name: %\t" item.name
			format "Position: %\t" item.position
			format "Bounds: % %\n" item.minXY item.maxXY
		)
	),
	
	-----------------------------------------------------------------------------------
	--SEARCH FUNCTIONS
	-----------------------------------------------------------------------------------
	
	--get the container information by the name of the container
	--token: name of the container as a string
	fn GetDataByName token =
	(
		local data = undefined
		local itemNotFound = true
		
		for item in this.containerInfoArray where (itemNotFound) do
		(
			if (item.name == token) then
			(
				data = item
				itemNotFound = false
			)
		)
		
		return data
	),
	
	--returns a list of containers that match the location of a given x,y point2 coordinate and info about whether the location is on a corner or edge
	--get containers for a given location
	fn GetDataByLocation xy =
	(
		local data = dataPair items:#() notes:undefined
		local xBorder = false
		local yBorder = false
		local columnIDs = #()
		local rowIDs = #()
		
		--check if the point is outside the bounds
		if (xy.x >= this.minXY.x and xy.x <= this.maxXY.x and xy.y >= this.minXY.y and xy.y <= this.maxXY.y) then
		(
			--find the column(s): convert the x value into a column number(s)
			local value = (xy.x - this.minXY.x) / this.containerSize
			
			--is value on a border between two containers?
			if ((mod value 1) == 0) then
			(
				xBorder = true
				if (value > 0) then
					append columnIDs (int value)
				if ((value + 1) < this.numColumns) then
					append columnIDs (int (value + 1))
			)
			else
				append columnIDs (int (ceil value))
				
			--find the row(s): convert the y value into a row number(s)
			value = (xy.y - this.minXY.y) / this.containerSize
			
			--work out if the location is on a corner or an edgex
			if ((mod value 1) == 0) then
			(
				yBorder = true
				if (value > 0) then
					append rowIDs (this.numRows - (int value) + 1)
				if (value < this.numRows) then
					append rowIDs (this.numRows - (int (value + 1)) + 1)
			)
			else
				append rowIDs (this.numRows - (int (ceil value)) + 1)
			columnIDs = sort columnIDs
			rowIDs = sort rowIDs
					
			--establish whether point on a border, a corner or inside a container
			if (xBorder == true and yBorder == true) then
				data.notes = #corner
			else if (xBorder == true and yBorder == false) then
				data.notes = #edgeVertical
			else if (xBorder == false and yBorder == true) then
				data.notes = #edgeHorizontal
			else if (xBorder == false and yBorder == false) then
				data.notes = #inside
			
			--work out the containers
			for r in rowIDs do
			(
				for c in columnIDs do
					append data.items (this.letterArray[r] + "_" + this.numberArray[c])
			)
		)
		else
			data.notes = #outside
		data
	),
	
	on create do
	(
		this.InitData()
	)
)

RSTA_ContainerData = RSTA_ContainerDataStruct()

-----------------------------------------------------------------------------------
--TEST
-----------------------------------------------------------------------------------
-- ClearListener()
-- RSTA_ContainerData.PrintData()
-- ClearListener()
-- poot = RSTA_ContainerData.GetDataByLocation [-3000,0]
-- poot = RSTA_ContainerData.GetDataByLocation [-4096,-4096]
-- poot = RSTA_ContainerData.GetDataByLocation [1500,1200]
poot = RSTA_ContainerData.GetDataByLocation [$test.position.x, $test.position.y]
print poot.items
print poot.notes
-- poot = RSTA_ContainerData.GetDataByName "f_02"
