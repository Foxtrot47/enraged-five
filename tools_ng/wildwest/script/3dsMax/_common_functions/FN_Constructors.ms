-- Build an Expose Transform Helper with name, position, size and colour
-- Thankfully not required so much these days
fn buildETM etmname etmExposer etmPos etmSize etmColour = (
	newetm = ExposeTm pos:etmPos isSelected:on name:etmName size: etmSize wirecolor:etmColour
	newetm.exposenode = etmExposer
)


-- Build a Point Helper with name, position, size and colour
fn buildPointHelper phName phPos phSize phColour = (
	newph = Point pos:phPos name:phName size:phSize wirecolor:phColour
	newph.Box = off
	newph.cross = on
	newph.centermarker = off
	
	return newph
)


-- Build a Dummy elper with name, position and size
-- Size is done by scaling and restting the transform afterwards
fn buildDummyHelper dName dPos dSize = (

	newdummy = Dummy pos:dPos name:dName isselected:on 
	scale newdummy [dsize,dsize,dsize]
	ResetTransform newdummy
	newdummy
)


-- Create a translation constraint
-- Parent and align it to an object, and use the objects name unless a name is supplied
fn createRSTranslationConstraint TCparentObj TCAxis TCmin TCmax TCoptionalName = (
	RSTC = RsTranslationConstraint isSelected:on	
	case TCAxis of 
	(
		"X": RSTC.axis = 1
		"Y": RSTC.axis = 2
		"Z": RSTC.axis = 3
	)	
	
	RSTC.LimitMin = TCmin
	RSTC.LimitMax = TCmax
	
	compositeName = (TCparentObj.name + "_TrCons_" + TCAxis)
	
	if TCoptionalName != "" then RSTC.name = TCoptionalName else RSTC.name = compositeName
	RSTC.transform = TCparentObj.transform
	RSTC.parent = TCparentObj
)


-- Create a rotation constraint
-- Parent and align it to an object, and use the objects name unless a name is supplied
fn createRSRotationConstraint RCparentObj RCAxis RCmin RCmax RCoptionalName = (
	RSRC = RsRotationConstraint isSelected:on	
	case RCAxis of 
	(
		"X": RSRC.axis = 1
		"Y": RSRC.axis = 2
		"Z": RSRC.axis = 3
	)	
	
	RSRC.LimitMin = RCmin
	RSRC.LimitMax = RCmax
	
	compositeName = (RCparentObj.name + "_RoCons_" + RCAxis)
	
	if RCoptionalName != "" then RSRC.name = RCoptionalName else RSRC.name = compositeName
	RSRC.transform = RCparentObj.transform
	RSRC.parent = RCparentObj
)



fn alignall alignme tome =(
	alignme.transform=tome.transform
)



fn alignrot alignme tome =(
	in coordsys (transMatrix alignme.pos) alignme.rotation=inverse tome.rotation
)


-- This should really take in a axis: Rick, October 2009
-- The X axis is the most common, so no hurry on this.
fn mirrorObject copyMe PasteMe =
(
	-- Assume we are mirroring across the X axis.
	copyPos = copyme.pos * [-1,1,1]
	pasteMe.pos = copyPos
)


-- Free Transforms, very useful for constructing rigs
-- Stolen from the Freezetransform that came with max.
fn FreezeTransform = 	--freezes transforms. Copied from the max macroscript as this does not like being run from other scripts.
( 		
	local Obj = Selection as array 	
	suspendEditing which:#motion
	for i = 1 to Obj.count do 		
	( 
		Try
		(	
			local CurObj = Obj[i] 	

			if classof CurObj.rotation.controller != Rotation_Layer do
			(

				-- freeze rotation		
				CurObj.rotation.controller = Euler_Xyz() 		
				CurObj.rotation.controller = Rotation_list() 			
				CurObj.rotation.controller.available.controller = Euler_xyz() 		
				
				/* "Localization on" */  
			
				CurObj.rotation.controller.setname 1 "Frozen Rotation" 		
				CurObj.rotation.controller.setname 2 "Zero Euler XYZ" 		
			
				/* "Localization off" */  
				
				CurObj.rotation.controller.SetActive 2 		
			)
			if classof CurObj.position.controller != Position_Layer do
			(

				-- freeze position
				CurObj.position.controller = Bezier_Position() 			
				CurObj.position.controller = position_list() 			
				CurObj.position.controller.available.controller = Position_XYZ() 	
	
				/* "Localization on" */  
						
				CurObj.position.controller.setname 1 "Frozen Position" 	
				CurObj.position.controller.setname 2 "Zero Pos XYZ" 			
				
				/* "Localization off" */  
				
				CurObj.position.controller.SetActive 2 		
	
				-- position to zero
				CurObj.Position.controller[2].x_Position = 0
				CurObj.Position.controller[2].y_Position = 0
				CurObj.Position.controller[2].z_Position = 0
			)
			if classof CurObj.scale.controller != Scale_Layer do
			(

				-- freeze scale
				CurObj.scale.controller = Bezier_Scale() 			
				CurObj.scale.controller = scale_list() 			
				CurObj.scale.controller.available.controller = ScaleXYZ() 	
	
				/* "Localization on" */  
						
				CurObj.scale.controller.setname 1 "Frozen Scale" 	
				CurObj.scale.controller.setname 2 "Zero Scale XYZ" 			
				
				/* "Localization off" */  
				
				CurObj.scale.controller.SetActive 2 		
	
				-- scale to zero
				CurObj.scale.controller[2].x_Scale = 100
				CurObj.scale.controller[2].y_Scale = 100
				CurObj.scale.controller[2].z_Scale = 100
			)				
			
		)	
		/* "Localization on" */  
				
		Catch( messagebox "A failure occurred while freezing an object's transform." title:"Freeze Transform")
				
		/* "Localization off" */  	
	)
	resumeEditing which:#motion
)


fn linkorient theManip theVictim alignManip theWeight =
(
	if alignManip == 1 then
	(
		in coordsys (transMatrix theManip.pos) theManip.rotation= theVictim.rotation
	)
	
	FreezeTransform()
	
	theVictim.rotation.controller = Orientation_constraint()
	thevictim.rotation.controller.appendtarget themanip theWeight
	thevictim.rotation.controller.relative = on
)