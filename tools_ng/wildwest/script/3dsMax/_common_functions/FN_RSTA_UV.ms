struct rsta_uv
(
	-- GET DISTANCE BETWEEN 2 UV VERTS
	fn get_vertDistance uvMod uvgeoVert_a uvVert_b =
	(
		local pos_a = uvMod.getVertexPosition 1 uvgeoVert_a
		local pos_b = uvMod.getVertexPosition 1 uvVert_b		
		return (distance pos_a pos_b)	
	), 
	
	-- GET A BITARRAY OF UV VERT INDEXS FROM THE GEO VERT INDEX
	fn uv_get_uvVertFromGeoVert uvMod geoVert_a =
	(
		if (classof geoVert_a == integer) do geoVert_a = #{geoVert_a}
		uvMod.setSelectedGeomVertsByNode geoVert_a obj
		return (uvMod.getSelectedVertices())	
	), 
	
	-- GET THE ANGLE BETWEEN 2 VERTS, ACCEPTS VERT INDEX OR POINT3
	fn angleBetweenVerts uvMod vert_a vert_b =
	(
		local ptA = if (classof vert_a == integer) then uvMod.getVertexPosition 1 vert_a else vert_a
		local ptB = if (classof vert_b == integer) then uvMod.getVertexPosition 1 vert_b else vert_b
		local dx = ptB.x - ptA.x
		local dy = ptB.y - ptA.y
		local ang = mod ((atan2 dy dx)) 360	
	
		return ang
	),
	
	-- GET THE BOUNDING BOX OF THE CURRENT UV SELECTION
	fn get_selBoundingBox uvMod =
	(	
		local bbox = #()
		
		local subObjMode = uvMod.getTVSubObjectMode()
		if (subObjMode == 3) do uvMod.faceToVertSelect() 
		if (subObjMode == 2) do uvMod.edgeToVertSelect() 		
		
		-- ENTER VERTEX MODE 
		uvMod.setTVSubObjectMode 1
			
		verts = (uvMod.getSelectedVertices()) as array			
	
		local i = 1
		for i = 1 to verts.count do
		(
			local v = verts[i]
			local pt = uvMod.getVertexPosition 1 v 
			if (i == 1) then
			(
				bbox[1] = bbox[2] = pt.x
				bbox[3] = bbox[4] = pt.y
			)
			else
			(
				bbox[1] = amin #(bbox[1],pt.x) --minX
				bbox[2] = amax #(bbox[2],pt.x) --maxX
				bbox[3]  = amin #(bbox[3],pt.y) --minY
				bbox[4]  = amax #(bbox[4],pt.y) --maxY
			)			
		)
		
		-- RESET MODE
		uvMod.setTVSubObjectMode subObjMode
		
		return bbox
	), 
	
	-- SET THE BOUNDING BOX OF THE CURRENT UV SELECTION  target_bb = #(xMin,xMax,yMin,yMax) dir=0 both 1= width 2=height
	fn set_selBoundingBox uvMod target_bb dir:0 = 
	(
		local current_bb = get_selBoundingBox uvMod
		if (current_bb.count == 4) do 
		(		
			local target_w = target_bb[2] - target_bb[1]
			local target_h = target_bb[4] - target_bb[3]
			
			local actual_w = current_bb[2] - current_bb[1]
			local actual_h = current_bb[4] - current_bb[3]			

			if (dir == 0 OR dir == 1) do uvMod.scaleSelectedCenter (target_w/actual_w) 1
			if (dir == 0 OR dir == 2) do uvMod.scaleSelectedCenter (target_h/actual_h) 2
					
			current_bb = get_selBoundingBox uvMod 		
					
			uvMod.snapPivot 2 -- BOTTOM LEFT
			local moveTarget = [0,0,0]
			if (dir == 0 OR dir == 1) do moveTarget.x = target_bb[1] - current_bb[1]
			if (dir == 0 OR dir == 2) do moveTarget.y =  target_bb[3] - current_bb[3]
			
			uvMod.moveSelected moveTarget 
		)			
	),

	-- GET INFORMATION OUT OF BOUNDING BOX
	fn get_boundingBoxdata bb arg = 
	(
		case arg of 
		(
			#width:(return bb[2] - bb[1])
			#height:(return bb[4] - bb[3])
			#leftOffset:(return bb[1])
			#rightOffset:(return 1 - bb[2])
			#bottomOffset:(return bb[3])
			#topOffset:(return 1 - bb[4])
			#ratio:((get_boundingBoxdata bb #width)/(get_boundingBoxdata bb #height))	
		)
	),
	
	-- UNIFORM SCALE TO WIDTH/HEIGHT   DIR : 1 = width 2 = height
	fn uniformScaleToTarget uvMod target dir =
	(
		local current_bb = get_selBoundingBox uvMod
		local s = 1
		
		if (dir == 1) do -- WIDTH 
		(
			local width = current_bb[2] - current_bb[1]
			s = target as float/width
		)
		if (dir == 2) do -- HEIGHT 
		(
			local height = current_bb[4] - current_bb[3]
			s = target as float/height
		)	
	
		uvMod.scaleSelectedCenter s 0
	),
	
	-- SNAP ROTATE THE SHELL TO THE SELECTED EDGE
	fn snapRotateShellToEdge uvMod =
	(
		local edges = uvMod.getSelectedEdges()
		if (edges.numberSet != 0) do 
		(
			uvMod.edgeToVertSelect()
			local verts = uvMod.getSelectedVertices() as array
			
			local ptA = uvMod.getVertexPosition 1 verts[1]
			local ptB = uvMod.getVertexPosition 1 verts[2]
			local dx = ptB.x - ptA.x
			local dy = ptB.y - ptA.y

			
			local ang = (angleBetweenVerts uvMod  ptA ptB) 
			local ang = (mod (angleBetweenVerts uvMod  ptA ptB) 90)
			if (ang > 45) do (ang = -(90 - ang))
				
			uvMod.selectElement()
			uvMod.RotateSelected (-ang * PI/180) [(ptA.x + dx/2),(ptA.y + dy/2),0]	
			uvMod.selectEdges edges;				
		)
	)		
)

rsta_uv = rsta_uv()
