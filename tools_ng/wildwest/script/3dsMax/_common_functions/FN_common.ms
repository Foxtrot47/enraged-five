-- Rick's Common Maxscript functions

-- added fn AlignPivotTo function Jan 2010

-- Load the rockstar export settings
filein "rockstar\export\settings.ms"

-- *********************************************************************************************************************
-- Math Functions
-- *********************************************************************************************************************

--Random Number Buckets
struct RandomBucketGenerator
(
	out = #(),
	bucketValues = #(),
	
	--Args:
	--low is the low range value
	--high is the high range value
	--buckets is how many to group the random numbers into
	--items is how many random numbers per bucket
	--count is how many random numbers are finally required
	fn generate low high buckets items count= 
	(
		bucket = (high as float - low as float) / buckets
		
		--generate bucket values
		for b = 1 to buckets do
		(
			local cell = #()
			for i = 1 to items do
			(
				append cell (random ((b-1) * bucket) (b * bucket))
				--append bucketValues (random ((b-1) * bucket) (b * bucket))
			)
			append bucketValues cell
		)
		
		--now choose random numbers from the number of buckets required
		out = #()
		for i = 1 to count do
		(
			luckyDip = random 1 buckets  --choose a bucket
			append out bucketValues[luckyDip][(random 1 items)]
		)
		
	)
)

randomBucketGenerator = RandomBucketGenerator()

-- *********************************************************************************************************************
-- FILE OPS
-- *********************************************************************************************************************

-- Get Local APPDATA RockStar dir
fn RsConfigGetLocalAppData =
(
	pathConfig.appendPath (systemTools.getEnvVariable "LOCALAPPDATA") "Rockstar_Games"
)

-------------------------------------------------------------------------------------------------------------------------
-- Get Local APPDATA RockStar  wildwest dir
fn RsConfigGetLocalWildWestAppData =
(
	pathConfig.appendPath (RsConfigGetLocalAppData()) "wildwest"
)

-------------------------------------------------------------------------------------------------------------------------
-- Get P4 Depot Files with wildcard
fn getDepotFiles assetDir wildcard:"*.*" walk:false =
(
	if gRsPerforce.connected() == false then gRsPerforce.connect()
	if gRsPerforce.connected() == false then return false
	
	depotSearch = (assetDir + "/" + wildcard) as string
	if walk == true then depotSearch = (assetDir+"/.../"+wildcard) as string
	depFiles = gRsPerforce.p4.run "files" #(depotSearch)

	depFileData = #()
	if depFiles.records.count > 0 then --we have some matches
	(
		for i = 0 to (depFiles.records.count - 1) do append depFileData	(gRsPerforce.record2struct depFiles.item[i])
	)
	
	--return
	depFileData
)

-------------------------------------------------------------------------------------------------------------------------
-- Function to make filenames perforce friendly
-- Takes an array of files
fn WWMakeP4vFriendly makeTheseFilesSafe =
(
	P4vFriendlyFiles = #()
	
	for thisFile=1 to makeTheseFilesSafe.count do
	(
		safeFile = RSMakeSafeSlashes makeTheseFilesSafe[thisFile]
		safeFile = gRsPerforce.local2depot safeFile
		append P4vFriendlyFiles safeFile
	)
	P4vFriendlyFiles -- Return this
)--end WWMakeP4vFriendly

-------------------------------------------------------------------------------------------------------------------------
--function to check the perforce version of a file against the local version
--takes a single value
fn WWCheckP4vRevision theFile =
--theFile is the file we want to check
(
	result = gRsPerforce.run "fstat" theFile returnResult:true
	returnHave = result.Item[0].Item["haveRev"]--find out the version we've got
	returnHead = result.Item[0].Item["headRev"]--find out the latest version in perforce
	returnValues = #(returnHave, returnHead)--return the values
	--returnValue = (returnHead == returnHave)--return true if we have latest, false if we don't
)--end WWCheckP4vRevision

-------------------------------------------------------------------------------------------------------------------------
--function to sync an array of files from perforce
--takes an array of files
fn WWP4vSync filesToSync =
(
	friendlyFiles = WWMakeP4vFriendly filesToSync

	for syncMe=1 to friendlyFiles.count do
	(
		try
		(
			gRsPerforce.p4.run "sync" #(friendlyFiles[syncMe]) silent:true
		)
		catch
		(
			print "=========================="
			print "Couldn't sync the file(s):"
			print filesToSync[syncMe]
			print "=========================="
		)
	)
)--end WWP4vSync

--recursively collect files based on a root and a wildcard pattern
fn getFilesRecursive root pattern getRootFiles:false=
(
	local my_files = #()
	if getRootFiles then
	(
		join my_files (getFiles (root +"/" + pattern))
	)
	
	local dir_array = GetDirectories (root+"/*")
	for d in dir_array do
	  join dir_array (GetDirectories (d+"/*"))
	
	for f in dir_array do
	  join my_files (getFiles (f + pattern))
	my_files
)

/***
	Get a list of files recurcively using the supplied root path and pattern wildcard
***/
fn getFilesRecursiveDN root pattern =
(
	local fileList = #()
	
	if (classof root != String) or (classof pattern != String) then
	(
		return undefined
	)
	
	root = RsMakeSafeSlashes root
	
	local files = (dotnetclass "System.IO.Directory").EnumerateFiles root pattern ((dotnetclass "System.IO.SearchOption").AllDirectories)
	local filesIt = files.GetEnumerator()
	
	while (filesIt.MoveNext() != false) do
	(
		append fileList filesIt.current
	)
	
	--return
	fileList
)

-- *********************************************************************************************************************
-- CSV FUNCTIONS
-- *********************************************************************************************************************

struct CSVReader
(
	--------------------------------------------------------------
	--USAGE
	--
	-- csv = CSVReader
	-- set the file path
	-- 	csv.csvFile = "c:/somepath/csvfile.csv"
	-- set the processingFn to process each line
	-- 	csv.processingFn = dataProcessor()
	-- Then call the open() method to process the csv file
	--When finished call the close() method to clen up used resources
	--------------------------------------------------------------
	stream = undefined,
	csvFile = undefined,
	processingFn = undefined,
	
	fn open =
	(
		if processingFn != undefined then
		(
			-- Open the xml with the streamReader 
			stream = dotNetObject "System.IO.StreamReader" csvFile
			
			while stream.EndOfStream != true do
			(
				--pass to the processing function
				theLine = stream.readline()
				processingFn theLine
			)
		)
	),
	
	fn close =
	(
		-- Close/Dispose the steam
		-- this is all needed to make sure the memory used is freed from big csv files
		stream.close()
		stream.dispose()
		stream = undefined
		gc() 
	)
)



-- *********************************************************************************************************************
-- XML FUNCTIONS
-- *********************************************************************************************************************


-- Opens/Holds/Closes XML stream 
-- CREATED: 09/12/2011 Matt Harrad
-- ADDDED: save functions
struct xmlStreamHandler
(
	--------------------------------------------------------------
	-- USAGE: xmlStream = xmlStreamHandler fileName:<FILE PATH>
	-- xmlStream.open()
	-- DO STUFF WITH THE XML USING xmlStream.root
	-- xmlStream.close()
	---------------------------------------------------------------------------
	-- LOCALS------------------------------------------------------------
	---------------------------------------------------------------------------
	stream = undefined,
	xmlFile = undefined,
	root = undefined, -- ROOT NODE OF THE XML FILE
	XML = undefined,
	error = false,
	---------------------------------------------------------------------------
	-- FUNCTIONS ----------------------------------------------------
	---------------------------------------------------------------------------
	fn xmlNode = 
	(
		return dotNetObject "System.Xml.XmlNode"
	),
	---------------------------------------------------------------------------
	fn open =
	(
		if (doesFileExist xmlFile) then
		(
			-- Open the xml with the streamReader 
			stream = dotNetObject "System.IO.StreamReader" xmlFile
			XML = dotNetObject "System.Xml.XmlDocument"
			try (XML.load stream) catch -- ERROR CHECKING
			(
				messageBox (xmlFile) title:"XML Formatting error!"
				error = true
				this.close() -- CLOSE OUT ALL THE VARIBLES AND FILES
			)
			if not(error) do 
			(
				root = XML.documentElement  
				stream.close() -- UNLOCK THE FILE
				return true
			)
		)
		else
		(
			messageBox (xmlFile + " : File Does Not Exist!") title:"xmlStreamHandler"
			return false
		)
	),
	---------------------------------------------------------------------------
	fn close =
	(
		-- Close/Dispose the the steam and xml varbiles
		-- this is all needed to make sure the memory used is freed from big XML files
		doc = undefined 
		stream.close() 
		stream.dispose() 
		stream = undefined 
		gc() 
	),
	---------------------------------------------------------------------------
	fn save  = 
	(
		if (getFileAttribute xmlFile #readOnly) then messagebox ("File Set to READ ONLY : \n\t" +xmlFile)	
		else XML.save xmlFile	
	),
	---------------------------------------------------------------------------
	fn saveas filePath =
	(
		if (getFileAttribute filePath #readOnly) then messagebox ("File Set to READ ONLY : \n\t" +filePath)	
		else XML.save filePath	
	)
)

-- *********************************************************************************************************************
-- String Functions
-- *********************************************************************************************************************

-- Convert text string to uppercase
global uppercase = toUpper

-------------------------------------------------------------------------------------------------------------------------
-- Convert text string to lowercase
global lowercase = toLower


-------------------------------------------------------------------------------------------------------------------------
-- Replace spaces in strings with underscores
-- This is vital for max variables in expressions
global replaceSpace = RsRemoveSpaces

-------------------------------------------------------------------------------------------------------------------------
-- Replace underscores in strings with spaces
-- This is vital for max variables in expressions
fn replaceUnderbar instring = 
(  
	local outstring=copy instring 

	for i = 1 to outstring.count where (outstring[i] == "_") do 
	(	
		outstring[i] = " "
	) 
	
	outstring
) 

-------------------------------------------------------------------------------------------------------------------------
-- Looks for an underscore in a string at a given position
fn findUnderscore ubstring spos = 
(
	ubstring[spos] == "_"
)

-------------------------------------------------------------------------------------------------------------------------
-- This is a little function to replace the Print command. 
-- This allows us then to be able to globally turn on and off debugPrint statements.
-- The user must set a variable called debugPrintVal to true which will then allow 
-- prints to occur otherwise the debugPrints never occur
fn debugPrint printStr = 
(
	if debugPrintVal == true do
	(
		Print printStr
		print "this function is old, please update to RSTA_debugPrint printStr instead  "
	)
)	

-------------------------------------------------------------------------------------------------------------------------
-- Function to collect tools data usage

global ToolsUsageUtility

(
		local dllPath = (RsConfigGetToolsDir() + "techart/dcc/3dsMax/RSG.TechArt.toolsusage.dll")
		local dllExistsLocally = doesFileExist dllPath

		if dllExistsLocally then
		(
			dotnet.loadAssembly dllPath
			ToolsUsageUtility = dotnetobject "RSG.TechArt.ToolsUsage.Utility"
		)
)

struct RsUsageCollectItem (filename="", versionNum=1.00, versionName="", toolName="", logPath="")
fn RsCollectToolUsageData filename versionNum:1.00 versionName:"" =
(
	return ""
)

-- *********************************************************************************************************************
-- Array  Functions
-- *********************************************************************************************************************
-- This returns a difference array from two arrays

fn differenceInArray masterArray subsetArray =
(
	newarray = #()
	for mi = 1 to masterArray.count do (
		
		foundMatch=0
		MitemtoFind = masterArray[mi]
		
		for si = 1 to subsetarray.count do (
			SitemtoFind = subsetArray[si]
			if MitemtoFind == SitemtoFind then 
			(
				foundMatch = 1
				exit	-- for speed
			)
		)	
			
		if foundMatch == 0 then append newArray MitemToFind	
	)	
	
	newArray
)
-------------------------------------------------------------------------------------------------------------------------
fn sortArraybySubIndex arr1 arr2 subIndex:1 =
(
	case of
	(
		(arr1[subIndex] < arr2[subIndex]):-1
		(arr1[subIndex] > arr2[subIndex]):1
		default:0
	)
)
-------------------------------------------------------------------------------------------------------------------------
-- Matt Harrad [11/04/12]
-- Array version of classOf(), returns true is the class of the obj is the classArray
fn classOf_Array classArray obj =
(
	r = false	
	if (findItem classArray (classof obj)) != 0 do r = true	
	return r
)

-------------------------------------------------------------------------------------------------------------------------
--Given a 2d array of dimensions width(w) and Height(h)
--return an array of Point2 values that represent cell [column, row]
--that starts in the middle and spirals out anti-clockwise to the outer bounds

fn spiralArraySequence w h =
(
	local x = ceil(w/2.0)
	local y = ceil(h/2.0)
	
	local right = [1, 0]
	local up = [0, 1]
	local left = [-1, 0]
	local down = [0, -1]
	local moveDir = right
	
	local dx = 1
	local delta = 0
	
	local newPos = [x, y]
	
	--the spiral sequence to return
	spiralSequence = #(newPos)
	--print newPos
	local counter = 1
	
	for i=1 to (w^2.0 + h^2.0) do
	(
		if (counter == 0) then
		(
			case of
			(
				(moveDir == right):
				(
					--print "right->up"
					moveDir = up
					dx += delta
					counter = dx
					
					if delta == 1 then delta = 0 else delta = 1
				)
			
				(moveDir == up):
				(
					--print "up->left"
					moveDir = left
					dx += delta
					counter = dx
					
					if delta == 1 then delta = 0 else delta = 1
				)
				
				(moveDir == left):
				(
					--print "left->down"
					moveDir = down
					dx += delta
					counter = dx
					
					if delta == 1 then delta = 0 else delta = 1
				)
				
				(moveDir == down):
				(
					--print "down->right"
					moveDir = right
					dx += delta
					counter = dx
					
					if delta == 1 then delta = 0 else delta = 1
				)
				
			)
		)
		
		newPos = newPos + moveDir
		
		counter -= 1
		
		if (newPos.x > 0 and newPos.x < (w+1) and newPos.y > 0 and newPos.y < (h+1)) then
		(
			--print newPos
			append spiralSequence newPos
		)
		
	)
	
	--return
	spiralSequence
)

-- *********************************************************************************************************************
-- Positional  Functions
-- *********************************************************************************************************************
-- taking in a bonename, such as $'Char Head', return the x,y,z position

fn getObjPos theObject = (
	-- taking in a bone, convert it to a string, parse the position
	p3 = theObject.transform.pos
	p3
)
-------------------------------------------------------------------------------------------------------------------------
-- Calculate a point in between two point3 positions
-- Can actually be used beyond 100% to find offsets too
fn inbetweenpoint startpoint endpoint whichpoint = (
	wp = endpoint + ((startpoint-endpoint) * (1.0 - whichpoint))
	wp
)
-------------------------------------------------------------------------------------------------------------------------
fn AlignPivotTo Obj Trgt =

      (
            -- Get matrix from object
            if classOf Trgt != matrix3 then Trgt = Trgt.transform

			-- Store child transforms
            local ChldTms = in coordSys Trgt ( for Chld in Obj.children collect Chld.transform )

            -- Current offset transform matrix
            local TmScale = scaleMatrix Obj.objectOffsetScale
            local TmRot = Obj.objectOffsetRot as matrix3
            local TmPos = transMatrix Obj.objectOffsetPos
            local TmOffset = TmScale * TmRot * TmPos

            -- New offset transform matrix
            TmOffset *= obj.transform * inverse Trgt

            -- Apply matrix
            Obj.transform = Trgt

            -- Restore offsets
            Obj.objectOffsetPos = TmOffset.translation
            Obj.objectOffsetRot = TmOffset.rotation
            Obj.objectOffsetScale = TmOffset.scale

            -- Restore child transforms
            for i = 1 to Obj.children.count do Obj.children[i].transform = ChldTms[i] * inverse Trgt * Obj.transform
      )



-- *********************************************************************************************************************
-- Building things.
-- *********************************************************************************************************************
--****Moved to FN_Constructors -- bug 416591*******


-- *********************************************************************************************************************
-- GTA Attribute functions
-- *********************************************************************************************************************
-- a function that will edit the attribute on on/off attributes
-- selType is the superclass to check, obj is the passed in object, selAttr is the attribute to set and selAttrState is what to set it to

fn setAttribute obj selAttr selAttrState = (
	if ((obj != undefined) and (superclassof obj != undefined)) then
	(
		-- Set the Attributes:
		indexAttr = GetAttrIndex (getAttrClass obj) selAttr
		attrSet = SetAttr obj indexAttr selAttrState
	)
)--end setCollAttribute


-------------------------------------------------------------------------------------------------------------------------
-- a function that will read the attributes of selected meshes
-- selType is the superclass to check, obj is the passed in object, selAttr is the attribute to read and setSelState is what store it in
fn readAttribute obj selAttr setSelState = (
	if ((obj != undefined) and (superclassof obj != undefined)) then
	(
		try (
			--work out the index for the attribute
			indexAttr = GetAttrIndex (getAttrClass obj) selAttr
			--get the attribute value
			attrGet = GetAttr obj indexAttr
			readAttr = attrGet
		)	
		catch ()	
	)
)--end readCollAttribute
-------------------------------------------------------------------------------------------------------------------------




-- *********************************************************************************************************************
-- Selection functions
-- *********************************************************************************************************************

--From the passed in object selection walk the heirarchy structure creating an array as we go
--The result is an array that starts at the trunk and extends out to the leaves of the heirarchy
fn heirarchyWalk obj =
(
	hStack = #(obj)
	
	children = obj.children
	for child in children do append hStack child
	
	while children.count != 0 do
	(
		tempChildren = #()
		for child in children do
		(
			innerChildren = child.children
			if innerChildren.count != 0 then
			(
				join hStack innerChildren
				join tempChildren innerChildren
			)
		)
		 children = tempChildren
	)
	--return
	hStack
)

-------------------------------------------------------------------------------------------------------------------------
-- From a passed in object walk the heirarchy structure to find the root/parent object
fn WWfindRootObject obj =
(
	local retVal = obj
	
	while (retVal.parent != undefined) do
	(
		retVal = retVal.parent
	)
	
	return retVal
)

-------------------------------------------------------------------------------------------------------------------------
-- Lets you select items by the start of their name:
fn selectByWildcard nodeName=
(
	clearSelection()
	execute ("select $" + nodeName + "*")
)

-------------------------------------------------------------------------------------------------------------------------
--search an objects user defined properties
fn UDPsearch searchObj searchString =
--searchObj = the object we want to check the UDP of
--searchString = the string we are looking for
(
	local searchString = toUpper searchString
	
	local currentUDP = getUserPropbuffer searchObj
	local UDPtokens = for token in (filterString currentUDP "\r\n") collect (toUpper token)
	
	return (findItem UDPtokens searchString)
)--end UDPSearch

-------------------------------------------------------------------------------------------------------------------------
-- hide all the collision and constraints in a scene
fn hideCollAndConst = 
(
	local hideObjs = #()
	join hideObjs ($Collision_Capsule* as array)
	join hideObjs ($Constraint* as array)

	hide hideObjs
)

-- *********************************************************************************************************************
-- Geometry Functions
-- *********************************************************************************************************************

--@summary A function that returns an array specifying list of faces related to a given vertex and the proportion or wedge size 
--out of the total arc around the vertex
--@params inMesh vIndex
--@note inmesh is the EditablePoly mesh to use. vIndex is the vertex index into the mesh
fn getFaceWedgeSizeFromMeshVert inMesh vIndex = 
(
	--array to return
	wedgeSizes = #()
	
	--Get the faces for the vIndex
	aFaceArray = (polyop.getFacesUsingVert inMesh vIndex) as array
	
	--for each face get its edges and match them to the edges attached to this vert
	for face in aFaceArray do
	(
		--get the edges
		aFaceEdges = (polyop.getEdgesUsingFace inMesh face) as array
		
		--get verts from edge, check one of them is vIndex
		connFaceEdgeVerts = #()
		for fEdge in aFaceEdges do
		(
			--get edgeVerts
			edgeVerts = (polyop.getVertsUsingEdge inMesh fEdge) as array
			--if one of the edgeverts is vIndex then add to array
			if( edgeVerts[1] == vIndex) then
			(
				append connFaceEdgeVerts edgeVerts[2]
			)
			
			if( edgeVerts[2] == vIndex) then
			(
				append connFaceEdgeVerts edgeVerts[1]
			)
		) --end fEdge
		
		--get normalized vector for each edge
		edge1Vec = normalize( inMesh.vertices[vIndex].pos - inMesh.vertices[connFaceEdgeVerts[1]].pos )
		edge2Vec = normalize( inMesh.vertices[vIndex].pos - inMesh.vertices[connFaceEdgeVerts[2]].pos )
		
		--dot the vecs to get angle
		--add to wedgesizes array with the face
		append wedgeSizes #(face, (abs(dot edge1Vec edge2Vec)))
	) -- end face
	
	--print wedgeSizes
	--DEBUG
	/*
	total = 0
	for w in wedgeSizes do
	(
		--print (acos(w))
		total += (acos(w))
	)
	print total
	*/
	
	return wedgeSizes
	
) --end function

-----------------------------------
--@summary A function to retreive map verts for the given channel from a supplied mesh vertex index
--@params inMesh vIndex vChannel
--@note inMesh is the polymesh to work with, vIndex is the mesh vertex to find the map verts from, vChannel is the map channel to use.
--for v = 1 to $.vertices.count do
--( c = getMapVertColoursFromMeshVert $ v 0
--print c
--)
fn getMapVertColoursFromMeshVert inMesh vIndex vChannel =
(
    --array to return
	colours = #()
	
	--get the TriMesh into memory
	theMesh = snapshotasmesh inMesh 
    
    -- Get all faces using the vert as array
    faceArray = (meshop.getFacesUsingVert theMesh vIndex) as array
    
	--iterate over the faces
	for f in faceArray do
    (
		--Get geometry face - vertex is either its first, second or third component    
		geoFace = getFace theMesh f
	
		-- Get map face (index corresponds with geometry face)
		--There may not be a map channel set for this, so catch it accordingly
		try
		(
			mapFace = meshop.getMapFace theMesh vChannel f
		)
		catch
		(
			--add a pure white for a missing channel colour
			append colours #(255, 255, 255)
			--onto the next
			continue
		)

		-- Find order inside the mesh face - vertex (component) order will be the same in both
		mapVert = case of
		(
			(geoFace.x == vIndex): mapFace.x
			(geoFace.y == vIndex): mapFace.y
			(geoFace.z == vIndex): mapFace.z
		)
		
		--collect the mapVert values found
		append colours (meshop.getMapVert theMesh vChannel mapVert)
	
	)--end f loop
	
	--release the mesh from memory
    delete theMesh 
	
	--return the mapVert values found
	colours
	
)--end function

-------------------------------------------------------------------------------------------------------------------------
-- Returns the positon and direction of an intersection between two supplied nodes
fn find_Z_intersection z_node node_to_z =
( 
	local testRay = ray node_to_z.pos [0,0,-1] 
	local nodeMaxZ = z_node.max.z 
	testRay.pos.z = nodeMaxZ + 0.0001 * abs nodeMaxZ 
	intersectRay z_node testRay 
)

-- *********************************************************************************************************************/
--MATERIAL FUNCTIONS
-- *********************************************************************************************************************

--Returns an array of material IDs for a multmaterial containing decal shaders
--before you call it check your object does have a multimaterial assigned otherwise you wont get owt.
--returns undefined if none found
--Pass in an object reference
fn multiMaterialHasDecals obj =
(
	meshType = classOf obj
	local aDecalMaterials = undefined
	local aDecalStore = #()
	
	if (classOf obj.material) == multimaterial then
	(
		for i = 1 to obj.material.materialList.count do 
		(
			local subMaterial = obj.material.materialList[i]
			
			if (classOf subMaterial) == Rage_Shader and matchPattern (RstGetShaderName subMaterial) pattern:"*decal*" then
			(
				local ID = obj.material.materialIDList[i]
				
				append aDecalStore ID
			)
		)
	)
	
	if aDecalStore.count > 0 then aDecalMaterials = aDecalStore
	
	--return
	aDecalMaterials
)

-------------------------------------------------------------------------------------------------------------------------
--Get the multisub indices for assigned material on the given object
fn getAppliedMaterials obj =
(
	if obj.material == undefined then
	(
		return #()
	)
	theMesh = snapShotAsMesh obj
	theMats = for f = 1 to theMesh.numfaces collect getFaceMatID theMesh f
	uniqueIDs = makeUniqueArray theMats
	if uniqueIds.count == 0 then
	(
		delete theMesh
		--print "bail"
		return #()
	)
	
	delete theMesh
	
	uniqueIDs
)

-- *********************************************************************************************************************
-- DIALOG FUNCTIONS
-- *********************************************************************************************************************

-- MATT HARRAD [16/12/11]
-- GETS/SETS THE DIALOG POSITIONS
-- SO THEY WILL OPEN IN THE SAME PLACE THEY DID WHEN YOU SHUT THEM DOWN

fn rs_dialogPosition switch dialog =
( 
	scriptPath = (theWildWest + "script\\3dsMax\\_config_files\\general\\")
	iniSettings = (scriptPath + "dialogPositions.ini")
	
	if switch=="get" do
	(
		winPosX = getINISetting iniSettings dialog.name "windowPosX"
		winPosY = getINISetting iniSettings dialog.name "windowPosY"
		SetDialogPos dialog [winPosX as float,winPosY as float]
	)

	if switch=="set" do
	(
		winPos = GetDialogPos dialog		
		setINISetting iniSettings dialog.name "windowPosX" ((winPos.x as integer) as string) 
		setINISetting iniSettings dialog.name "windowPosY" ((winPos.y as integer) as string)
	)
)
-------------------------------------------------------------------------------------------------------------------------
-- Max modifier stack reordering
-- SCript found here: http://deniskorkh.wordpress.com/2012/01/13/5/

fn reOrderModifier objectRef modifierType targetIndex = (
	/*

	Bring the specified modifier to the bottom of the stack

	objectRef � node
	modifierType � class of the target modifier
	target index � where to move the modifier to

	*/

	-- create a clone and sswap names and base geos
	objectRefCopy = copy objectRef
	objectRefCopy.transform = objectRef.transform
	modifierList = for objectModifier in objectRef.modifiers collect objectModifier
	originalStackCount = modifierList.count
	swap objectRef.baseObject objectRefCopy.baseObject
	swap objectRef.name objectRefCopy.name

	-- store a modifier list and reorder as needed
	modifierList = for objectModifier in objectRef.modifiers collect objectModifier
	modifierIndex = findModifier objectRef modifierType --get the index of the modifier within the stack
	swap modifierList[targetIndex] modifierList[modifierIndex]

	--reapply the modifiers in the new order
	for counter = modifierList.count to 1 by -1 do (
		addModifier objectRefCopy modifierList[counter]
		print ("adding >>" + modifierList[counter] as string)
	)

	--delete the old modifiers
	for counter = 1 to originalStackCount by 1 do (
		deleteModifier objectRefCopy objectRefCopy.modifiers.count
	)

	--delete the old object
	delete objectRef
)

-------------------------------------------------------------------------------------------------------------------------
fn findModifier objectRef modifierType = (
	/*Returns the index in the stack of the top modifier that matches the provided modifier type
	Returns undefined if one does exist*/
	for objectModifier in objectRef.modifiers do (
		if classOf objectModifier == modifierType then (
			modifierIndex = modPanel.getModifierIndex objectRef objectModifier
			return modifierIndex
		)
	)
)
-- *********************************************************************************************************************
-- HELPER FUNCTIONS
-- *********************************************************************************************************************
-- MATT HARRAD [07/05/12]
-- RETURNS A LIST OF MILO ROOMS 
fn RsGetMloRooms names:false milo: =
(
	local mloRooms = for obj in helpers where (isKindOf obj GtaMloRoom) collect obj
	
	if (milo != unsupplied) do 
	(
		if (classof milo == Gta_MILO) do mloRooms = for room in mloRooms where (room.parent == milo) collect room
		if (classof milo == String) do mloRooms = for room in mloRooms where (room.parent.Name == milo) collect room
	)
	
	if (names) do 
	(
		mloRooms = for obj in mloRooms collect obj.name
	)
	
	return mloRooms
)
-------------------------------------------------------------------------------------------------------------------------

-- Generates a random phrase for version-naming.
-- Example: "Ablaze Eggnog"
fn RsRandomPhrase randomSeed: count: = 
(
	local doList = (isKindOf count number)
	if not doList do (count = 1)
	
	if (randomSeed != unsupplied) do 
	(
		seed randomSeed
	)
	
	local wordFolder = (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/general/")
	local adjsPath = (wordFolder + "words_adjective.txt")
	local nounsPath = (wordFolder + "words_noun.txt")
	
	local wordSources = #(adjsPath, nounsPath)
	
	local words = for filename in wordSources collect 
	(
		local readFile = openFile filename
		local fileWords = #()
		while not eof readFile do 
		(
			append fileWords (readLine readFile)
		)
		close readFile
		
		for n = 1 to count collect 
		(
			local wordNum = random 1 fileWords.count

			-- Get word, removing any whitespace:
			local getWord = trimLeft (trimRight fileWords[wordNum])

			-- Set first letter to uppercase:
			getWord[1] = toUpper getWord[1]
			
			getWord
		)
	)
	
	local retVal = for n = 1 to count collect (words[1][n] + " " + words[2][n])
	
	if doList then (return retVal) else (return retVal[1])
)

-- *********************************************************************************************************************
-- OTHER FUNCTIONS
-- *********************************************************************************************************************
--returns true if any items in the passed bitarrays match items in the other
--ba1: bitarray
--ba2: bitarray
--returns: true if any items in the passed bitarrays match
fn RSCompareBitarrays ba1 ba2 =
(
	if ((ba1 * ba2) as array).count > 0 then return true
	else	return false
)


-- Gets node in scene by name (but only if it is geometry)
fn RsGetGeomNodeByName nodeName = 
(
	local namedNodes = (getNodeByName nodeName all:True)
	local geomNodes = for obj in namedNodes where (isKindOf obj GeometryClass) collect obj
	return geomNodes[1]
)


-- Gets node in scene by name (but only if it is a helper)
fn RsGetHelperNodeByName nodeName = 
(
	local namedNodes = (getNodeByName nodeName all:True)
	local helperNodes = for obj in namedNodes where (isKindOf obj Helper) collect obj
	return helperNodes[1]
)

/***
	Function to return the screen resolution of the supplied monitor index
	e.g RSTA_GetMonitorResolution 1 --Primary monitor
	RSTA_GetMonitorResolution 2 --second monitor
	RSTA_GetMonitorResolution 3 --lucky bastard :)

***/
fn RSTA_GetMonitorResolution monitorIndex =
(
	local screens = (dotnetclass "Screen").AllScreens   --gives an array of screen objects you can query
	if (monitorIndex > screens.count) then
	(
		return undefined
	)
	local result = DataPair width:screens[monitorIndex].bounds.width height:screens[monitorIndex].bounds.height
	
	result
)
/***
	Function to temporarily set RsConfigGetWildWestDir to return the wildwest branch, this is overwriten with the Wildwest_header.ms
***/
fn RSTA_wildwestMode  =
(
	fn RsConfigGetWildWestDir = return RsMakeSafeSlashes((substring (getdir #scripts) 1 3 + "wildwest/"))
)
/***
	Function which returns true is there are any milo's in the file, this means its an interior file
***/
fn RSTA_isInterior =
(
	if (((for h in helpers where (classof h == Gta_MILO) collect h).count) == 0) then return false
	else return true
)


/***
	Maxscript Dictionary implementation

***/
struct Dictionary
(
	private
		_keys = #(),
		_values = #(),
		
	public
		fn keyID key =
		(
			finditem _keys key
		),
		
		fn hasKey key = 
		(
			keyID key > 0
		),
		
		fn keys =
		(
			_keys
		),
		
		fn values = 
		(
			_values
		),
		
		fn addKey key value overwrite:on ifunique:off =  
		(
			if (k = keyID key) == 0 then 
			(
				if not ifunique or (finditem _values value) == 0 do
				(
					k = _keys.count + 1
					_keys[k] = key
					_values[k] = value 
				)
			)
			else if overwrite and (not ifunique or (finditem _values value) == 0) do
			(
				_values[k] = value 
			)
		),
		
		fn removeKey key = if (k = keyID key) > 0 do 
		(
			deleteitem _keys k
			deleteitem _values k
		),
		
		fn getValue key = if (k = keyID key) > 0 do _values[k], 
		
		fn setValue key value = if (k = keyID key) > 0 do _values[k] = value,
		
		fn findKeys value = 
		(
			for k=1 to _values.count where _values[k] == value collect _keys[k]
		),
		
		fn makeUniqueValues =
		(
			local unique = makeUniqueArray _values
			_keys = for v in unique collect _keys[finditem _values v]
			_values = unique
		),
		
		fn clear = 
		(
			_keys = #()
		   _values = #()
		)
) 
