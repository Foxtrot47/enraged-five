--------------------------------------------------------------------------------------------------------------------------
-- R* TechArt thumbnails manager
--------------------------------------------------------------------------------------------------------------------------
--	System asynchronously loads thumbnails, and updates associated dotnet controls.
-- 
--	'GetThumbsStruct' is used to generate/get active struct, and registers active tools.
--	'DeregisterTool' is used to let struct know that a calling tool is no longer active;
--		(this function disposes of struct if no tools are currently registered)
--	'SetThumb' queues a bitmap-path & control for load/update.
--------------------------------------------------------------------------------------------------------------------------
--	Neal D Corbett (R* Leeds, 07/2014)
--------------------------------------------------------------------------------------------------------------------------

RSTA_LoadCommonFunction #("FN_CSharp.ms")

global gRSTA_ThumbsMgr
struct RSTA_ThumbsMgr
(
	-- All thumbnails are scaled to this size; tools use this to size their controls.
	ThumbSize = 80,
	
	-- List of loaded thumbnails:
	ImageList, 
	
	-- Names of tools using this instance of struct:
	RegisteredTools = #(),
	
	-- Background-worker thread:
	ImageLoaderThread,
	
	-- Default-image indices:
	ImgLoadingIdx, ImgMissingIdx, ImgEmptyIdx, ImgInvalidIdx,
	
	-- Lowercase list of image-paths, for finding thumb-idxs:
	Paths = #(), 
	Idxs = #(), ChangeVals = #(), 
	
	-- Thumb-idxs that are queued for load:
	ToLoadNums = #{},
	
	-- Lists of controls (per path) that want their background-image to be updated once their thumbnail has been loaded
	UpdateCtrls = #(), CtrlPaths = #(),
	
	TextFont, TextBrush, 
	
	thumbTex, alphaTex, alphaCompTex,
	
	------------------------------------------------------------------------
	-- Function returns dotnet image for given texmap path:
	------------------------------------------------------------------------
	fn LoadTexmap texPath thumbSize:[64,64] = 
	(
		--PushPrompt ("Opening: " + TexPath as string)
		--format "\n\nLoading: %\n\n" TexPath
		
		-- Load image using Max bitmap-open methods:
		local loadedBmp = (OpenBitmap texPath)
		local loadedImage = Undefined
		
		if (loadedBmp != Undefined) do with undo off 
		(
			Close loadedBmp
			
			-- Create texturemap objects:
			if (this.thumbTex == Undefined) do 
			(
				-- Create rgb-channel texmap:
				this.thumbTex = (BitmapTexture alphaSource:2)
				
				-- Create alpha-channel texmap:
				this.alphaTex = (BitmapTexture rgbOutput:1)
				
				-- Create texmap to use as half-and-half mask:
				local maskerTex = (Checker())
				maskerTex.coords.w_Angle = 45
				maskerTex.coords.u_Tiling = 0.1
				maskerTex.coords.v_Tiling = 0.1
				maskerTex.coords.u_Offset = 1
				maskerTex.coords.v_Offset = 1
				
				-- Create alpha-composite texturemap:
				this.alphaCompTex = CompositeTexture MapList:#(alphaTex, thumbTex) Mask:#(Undefined, maskerTex)
			)
			
			-- Set up texmaps for current loaded bitmap:
			thumbTex.bitmap = loadedBmp
			alphaTex.bitmap = loadedBmp
			
			local renderTex = if (loadedBmp.hasAlpha) then alphaCompTex else thumbTex
			
			-- Render texturemap to smaller bitmap:
			loadedBmp = (RenderMap renderTex size:thumbSize filter:True)
			
			-- Clear texmap-filenames:
			thumbTex.filename = ""
			alphaTex.filename = ""
			
			-- Convert Max bitmap to Dotnet image:
			--ReplacePrompt ("Converting: " + TexPath as string)
			loadedImage = RS_bitmapToImage loadedBmp
		)
		
		return loadedImage
	),
	
	------------------------------------------------------------------------
	-- Function run by image-loader thread when active:
	------------------------------------------------------------------------
	fn ImageLoaderFunc Sender Args = 
	(
		--format "\n>>> STARTING IMAGE-LOADER\n\n"
		--local LogMsg = Stringstream ""
		
		-- Get parent-struct from global value:
		local thumbsData = gRSTA_ThumbsMgr
		if (thumbsData == undefined) do return False
		
		-- Fetch values/functions from global struct:
		local thumbSize = [thumbsData.thumbSize, thumbsData.thumbSize]
		local ImageList = ThumbsData.ImageList
		local LoadTexmap = thumbsData.LoadTexmap
		
		local UpdateCtrls = ThumbsData.UpdateCtrls
		local CtrlPaths = ThumbsData.CtrlPaths
		
		local Paths = ThumbsData.Paths
		local Idxs = ThumbsData.Idxs
		local ChangeVals = ThumbsData.ChangeVals
		
		-- Loop until no images are waiting to be served:
		local UpdateStart = 1
		local UpdateEnd = (UpdateCtrls.Count)
		
		local Failed = False
		
		-- Wait a moment before starting loop, to allow further orders to queue up:
		if (Sender != #NonAsync) do sleep 0.2
		
		while (UpdateStart <= UpdateEnd) do 
		(
			--format ":::IMAGE-LOADER LOOP::: (%/%)\n\n" UpdateStart UpdateEnd To:LogMsg
			DoLoop = False
			local PathNum = 0
			
			for CtrlNum = UpdateStart to UpdateEnd while (not Failed) and ((Sender == #NonAsync) or (not Sender.CancellationPending)) do 
			(
				Try 
				(
					local ThisCtrl = UpdateCtrls[CtrlNum]
					UpdateCtrls[CtrlNum] = undefined
					
					-- Only process visible unprocessed controls:
					if ThisCtrl.Visible and (ThisCtrl.ImageIndex == -1) do 
					(
						local TexPath = CtrlPaths[CtrlNum]
						local ImgIdx = 0
						
						if (TexPath == undefined) or (TexPath == "") then 
						(
							ImgIdx = ThumbsData.ImgEmptyIdx
						)
						else 
						(
							-- Get file's info, and change-time: (will return zero-time if file doesn't exist)
							local FileInfo = (dotNetObject "System.IO.FileInfo" TexPath)
							local FileChangeVal = FileInfo.LastWriteTime.ToBinary()
							
							-- Has this already been looked at?
							local PathNum = FindItem Paths TexPath
							
							-- We'll reload previously-processed images if they've changed:
							local ReloadThumb = False
							if (PathNum != 0) do 
							(
								ReloadThumb = (ChangeVals[PathNum] != FileChangeVal)
							)
							
							if (PathNum == 0) or ReloadThumb then 
							(
								if (not FileInfo.Exists) then 
								(
									ImgIdx = ThumbsData.ImgMissingIdx
								)
								else 
								(
									-- Load image from file:
									local loadedImage = LoadTexmap texPath thumbSize:thumbSize
									
									if (LoadedImage == undefined) then 
									(
										ImgIdx = ThumbsData.ImgInvalidIdx
									)
									else 
									(
										ImageList.Images.Add LoadedImage
										ImgIdx = (ImageList.Images.Count - 1)
									)
									
									--PopPrompt()
								)
								
								if (ReloadThumb) then 
								(
									Idxs[PathNum] = ImgIdx
									ChangeVals[PathNum] = FileChangeVal
								)
								else 
								(
									append Paths TexPath
									append Idxs ImgIdx
									append ChangeVals FileChangeVal
								)
							)
							else 
							(
								-- Get previously-loaded thumbnail:
								ImgIdx = Idxs[PathNum]
							)
						)
						
						if (ThisCtrl.Visible) do 
						(
							ThisCtrl.ImageIndex = ImgIdx
							ThisCtrl.BackgroundImage = (ImageList.Images.Item[ImgIdx])
							ThisCtrl.Refresh()
							
							if (Sender == #NonAsync) do 
							(
								ThisCtrl.Visible = false
								ThisCtrl.Visible = true
							)
						)
					)
				)
				Catch
				(
					Failed = True
					--Stack()
					format "Thumb-load failure...\n%\n%\n" (CtrlPaths[CtrlNum]) (GetCurrentException())
				)
			)
			
			-- Wait a moment before looping again, to allow further orders to queue up:
			if (Sender != #NonAsync) do sleep 0.2
			
			-- Update for-loop range:
			UpdateStart = (UpdateEnd + 1)
			UpdateEnd = (UpdateCtrls.Count)
		)
		
		--format ":::LOOP FINISHED::: (%/%)\n\n" UpdateStart UpdateEnd To:LogMsg
		--format (LogMsg as string)
		
		-- Clear update-lists:
		UpdateCtrls.Count = 0
		CtrlPaths.Count = 0
		
		--format "\n<<< ENDING IMAGE-LOADER\n\n"
	),
	------------------------------------------------------------------------
	-- Starts up and run a file-loading thread, if required:
	------------------------------------------------------------------------
	fn StartImageLoad async:true = 
	(
		if (async) then 
		(
			-- Initialise an image-loader thread, if not done already:
			if (ImageLoaderThread == undefined) do 
			(
				ImageLoaderThread = DotNetObject "CSharpUtilities.SynchronizingBackgroundWorker"
				ImageLoaderThread.WorkerSupportsCancellation = True
				DotNet.addEventHandler ImageLoaderThread "DoWork" ImageLoaderFunc
			)
			
			--format "Busy? %, %\n" UpdateCtrls.Count (ImageLoaderThread.isBusy)
			
			-- Start/restart the image-loader thread:
			if (not ImageLoaderThread.isBusy) do 
			(
				ImageLoaderThread.RunWorkerAsync()
			)
		)
		else
		(
			ImageLoaderFunc #NonAsync undefined 
		)
	),
	
	------------------------------------------------------------------------
	-- Stops image-loader thread if it's running:
	------------------------------------------------------------------------
	fn StopImageLoader = 
	(
		if (ImageLoaderThread != undefined) and (ImageLoaderThread.IsBusy) do 
		(
			ImageLoaderThread.CancelAsync()
		)
	),
	
	------------------------------------------------------------------------
	-- Stop loader-thread, and clear thumbnail-data:
	------------------------------------------------------------------------
	fn ClearThumbsList = 
	(
		StopImageLoader()
		ImageLoaderThread = undefined
		
		-- Clear images:
		if (ImageList != undefined) do 
		(
			ImageList.Images.Clear()
		)
		
		-- Dispose of leftover controls:
		for Ctrl in UpdateCtrls where (Ctrl != undefined) do 
		(
			Ctrl.Dispose()
		)
		UpdateCtrls.Count = 0
		
		-- Reset arrays:
		Paths.Count = 0
		Idxs.Count = 0
		
		-- Clear global:
		gRSTA_ThumbsMgr = undefined
		
		-- Garbage-collect:
		(
			gc light:true
			(dotnetclass "system.gc").collect()
		)
		
		return OK
	),

	-----------------------------------------------------------------------------
	-- Get/create instance of struct, registering name of tool using it:
	-----------------------------------------------------------------------------
	fn GetThumbsStruct ToolName = 
	(
		if (gRSTA_ThumbsMgr == undefined) do 
		(
			gRSTA_ThumbsMgr = RSTA_ThumbsMgr()
		)
		
		appendIfUnique gRSTA_ThumbsMgr.RegisteredTools ToolName
		
		return gRSTA_ThumbsMgr
	),
	
	------------------------------------------------------------------------
	-- Clears thumbs-list when all registered tools are closed:
	------------------------------------------------------------------------
	fn DeregisterTool ToolName = 
	(
		-- Remove tool-name:
		RegisteredTools = for ThisName in RegisteredTools where (ThisName != ToolName) collect ThisName
		
		-- Dispose of this struct-instance if it's no longer being used:
		if (RegisteredTools.Count == 0) do 
		(
			ClearThumbsList()
		)
	),
	
	------------------------------------------------------------------------
	-- Queue thumbnail for thumb-load:
	------------------------------------------------------------------------
	fn SetThumb Ctrl ImagePath= 
	(
		if (ImagePath == undefined) or (ImagePath == "") then 
		(
			-- Set 'Empty' thumbnail:
			Ctrl.BackgroundImage = ImageList.Images.Item[ImgEmptyIdx]
			Ctrl.ImageIndex = ImgEmptyIdx
		)
		else 
		(
			-- Show basic 'Loading' thumbnail by default:
			if (Ctrl.BackgroundImage == undefined) do 
			(
				Ctrl.BackgroundImage = ImageList.Images.Item[ImgLoadingIdx]
			)
			
			if Ctrl.Visible do 
			(
				-- Add control/path to queue:
				append CtrlPaths ImagePath
				append UpdateCtrls Ctrl
				
				-- Start loader-thread if it's not running already:
				StartImageLoad()
			)
		)
	),
	
	------------------------------------------------------------------------
	-- Initial struct-setup:
	------------------------------------------------------------------------
	on Create do 
	(
		ImageList = (dotNetObject "ImageList")
		ImageList.ColorDepth = (ImageList.ColorDepth.Depth24Bit)
		ImageList.ImageSize = (dotNetObject "System.Drawing.Size" ThumbSize ThumbSize)
		
		-- Get some DotNet values:
		local TextClr = (ColorMan.GetColor #windowText) * 255; 
		TextClr = ((dotNetClass "System.Drawing.Color").FromArgb TextClr[1] TextClr[2] TextClr[3])
		TextBrush = (dotNetObject "System.Drawing.SolidBrush" TextClr)
		TextFont = (DotNetObject "Panel").Font
		
		-- Generate default images:
		(
			for ThisText in #("[Loading]", "[Missing]", "[Empty]", "[Invalid]") do 
			(
				local NewImg = (dotNetObject "System.Drawing.Bitmap" ThumbSize ThumbSize)
				((dotNetClass "System.Drawing.Graphics").FromImage NewImg).DrawString ThisText TextFont TextBrush 4 4
				ImageList.Images.Add NewImg
			)
			ImgLoadingIdx = 0
			ImgMissingIdx = 1
			ImgEmptyIdx = 2
			ImgInvalidIdx = 3
		)
	)
)
