rsta_loadCommonFunction #("FN_RSTA_xml")
------------------------------------------------------------------------------
-- A STRUCT THAT ALLOWS THE SAVING AND LOADING OF USER SETTINGS PER TOOL
------------------------------------------------------------------------------
struct rsta_userSettings
(
	-- LOCALS ---------------------------------------------------------
	app, 
	userSettingsPath = RsConfigGetWildWestDir() + "etc/config/userSettings/",
	xmlFile,
	xml_io,
	-- FUNCTIONS ---------------------------------------------------
	fn wpf_windowPos win switch =
	(
		case (switch) of 
		(
			#set :
			(
				if (hasProperty win "Left") do  this.setValue "windowLeft"  win.Left
				if (hasProperty win "Top") do  this.setValue "windowTop"  win.Top
			)
			#get :
			(
				if (this.getValue "windowLeft") != undefined do win.Left = (this.getValue "windowLeft")
				if (this.getValue "windowTop") != undefined do win.Top = (this.getValue "windowTop")	
			)
		)		
	),
	---------------------------------------------------------------------------
	fn dialog_windowPos dialog switch =
	(
		case (switch) of 
		(
			#set :
			(
				this.setValue "windowPos" (GetDialogPos dialog)
			)
			#get :
			(
				if (this.getValue "windowPos") != undefined do SetDialogPos dialog (this.getValue "windowPos")
			)
		)
	),
	---------------------------------------------------------------------------
	fn recursiveSetDnProp object property value =
	(
		local bits = filterstring property "."
		if (bits.count > 1) then 
		(	
			local newProp = substring property (bits[1].count + 2) property.count		
			recursiveSetDnProp (getProperty object bits[1]) newProp value
		)
		else if (hasProperty object property) do setProperty object property value		
	),
	---------------------------------------------------------------------------
	fn setDnProp settingName object property =
	(
		local val = (this.getValue settingName)	
		if (val != undefined) do recursiveSetDnProp object property val	
	),
	---------------------------------------------------------------------------
	fn recursiveGetDnProp object property  =
	(
		local bits = filterstring property "."
		if (bits.count > 1) then 
		(	
			local newProp = substring property (bits[1].count + 2) property.count		
			return (recursiveGetDnProp (getProperty object bits[1]) newProp)
		)
		else if (hasProperty object property) do return getProperty object property 		
	),
	---------------------------------------------------------------------------
	fn getDnProp settingName object property =
	(
		this.setValue settingName (recursiveGetDnProp object property)
	),
	---------------------------------------------------------------------------
	fn getValue attr = 
	(
		local attr_node = xml_io.root.SelectSingleNode attr
		if (attr_node == undefined) then return undefined
		else 
		(
			local attr_type = attr_node.getAttribute "type"
			local attr_text = attr_node.innerText
			case (attr_type) of 
			(
				"Integer" : return (attr_text as Integer)
				"Float" : return (attr_text as Float)
				"Double" : return (attr_text as Double)
				"BooleanClass" : return  (attr_text as BooleanClass)
				"Color": 
				(
					local cA = FilterString attr_text "() "
					return (color (cA[2] as float) (cA[3] as float) (cA[4] as float))
				)
				"Point2":
				(
					local pA =  FilterString attr_text "[],"
					return [(pA[1] as float), (pA[2] as float)]
				)
				"Point3":
				(
					local pA =  FilterString attr_text "[],"
					return [(pA[1] as float), (pA[2] as float), (pA[3] as float)]
				)
				"Quat":
				(
					local qA =  FilterString attr_text "() "
					return (quat (qA[2] as float) (qA[3] as float) (qA[4] as float) (qA[5] as float))
				)
				-- AND STRING 
				default: return attr_text
			)
		)
		
	), 
	---------------------------------------------------------------------------
	fn setValue attr value =
	(
		local attr_node = rsta_xml.makeNode attr xml_io.root unique:true
		attr_node.setAttribute "type" (classof value as string)
		attr_node.innerText = value as string
		xml_io.save saveWarning:false
	),
	---------------------------------------------------------------------------
	fn hasAttr attr = 
	(
		local attr_node = xml_io.root.SelectSingleNode attr
		return (attr_node != undefined) 
	),
	-- EVENTS ----------------------------------------------------------
	on create do
	(
		makeDir userSettingsPath	
		if (app != undefined) do 
		(
			xml_io = rsta_xml_io()
			xmlFile = userSettingsPath + app + ".xml"
			if (doesFileExist xmlFile) then 
			(
				xml_io.xmlFile = xmlFile
				xml_io.load()
			)
			else
			(
				xml_io.xmlFile = xmlFile
				xml_io.new "UserSettings"
				xml_io.save()
			)
		)
	)
)