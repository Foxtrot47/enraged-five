/*--------------------------------------------------
Returns the passed number rounded up to the closet increment
*/--------------------------------------------------
fn RoundToIncrement InputFloatValue Increment:1 = 
(
	( floor ( 1.0 * InputFloatValue/Increment + 0.5 ) * Increment) as integer 
)

fn TriangleArea InputPositionList =
(
	LengthA = Distance InputPositionList[1] InputPositionList[2]
	LengthB = Distance InputPositionList[2] InputPositionList[3]
	LengthC = Distance InputPositionList[3] InputPositionList[1]
	
	Semiperimeter = ( LengthA + LengthB + LengthC ) / 2
	
	return ( Sqrt ( Semiperimeter * ( ( Semiperimeter - LengthA )*( Semiperimeter - LengthB )*( Semiperimeter - LengthC ) ) ) )
)

fn roundOff accuracy thisValue =
(
	(floor ((thisValue / accuracy) + 0.5)) * accuracy
)


-------------------------------------------------------------------
-- WILL RETURN THE OUTTER MOST POINTS IN ORDER FROM A POINT3 ARRAY
-------------------------------------------------------------------
struct rsta_math_convexHull_2D
(
	-----------------------------------------------------------------------------------------------
	-- GET THE MIN AND MAX IN X OF POINT ARRAY
	-----------------------------------------------------------------------------------------------
	fn minMaxOfArray pntArray =
	(
		local minX = undefined
		local maxX = undefined
		
		for i=1 to pntArray.count do 
		(
			pPos = pntArray[i]
			
			if (minX == undefined) do minX = i
			if (maxX == undefined) do maxX = i
			
			if (pntArray[minX].x > pPos.x) do minX = i 
			if (pntArray[maxX].x < pPos.x) do maxX = i
		)	
		
		return #(minX, maxX)
	),

	-----------------------------------------------------------------------------------------------
	-- REMOVE GIVEN IDX FROM ARRAY
	-----------------------------------------------------------------------------------------------
	fn remove_from_array inArray idxArray =
	(
		return (for i = 1 to inArray.count where (finditem idxArray i == 0) collect inArray[i])	
	),

	-----------------------------------------------------------------------------------------------
	-- GET FURTHEST POINT FROM LINE
	-----------------------------------------------------------------------------------------------
	fn furthestPointFromLine p1 p2 pntArray =
	(
		local MaxDistance = 0 
		local MaxPointIdx 
		
		for i=1 to pntArray.count do 
		(
			local distanceToLine = rsta_math.DistanceFromPointToLine p1 p2 pntArray[i] 
			if (distanceToLine > MaxDistance) do 
			(
				MaxDistance = distanceToLine
				MaxPointIdx = i
			)
		)	
		return MaxPointIdx
	),

	-----------------------------------------------------------------------------------------------
	-- MAKE THE TRIANGLE 
	-----------------------------------------------------------------------------------------------
	fn makeTri pntArray p1 p2 =
	(
		if (pntArray.count != 0) do 
		(
			local leftArray = #()
			
			for p in pntArray do 
			(
				if (rsta_math.IsPointLeftOfLine p1 p2 p) do append leftArray p			
			)		

			if (leftArray.count != 0) do 
			(
				-- DO LEFT SIDE
				local furthestPtnIdx = furthestPointFromLine p1 p2 leftArray
				local triPtns = #(p1, leftArray[furthestPtnIdx] ,p2) 		
				
				-- REMOVE PTN
				local ptnsLeft = remove_from_array leftArray #(furthestPtnIdx)
				local ptnsInTri = #()
				for i=1 to ptnsLeft.count do 
				(
					if (RSGeom_PointInTriangle triPtns[1] triPtns[2] triPtns[3] ptnsLeft[i]) do 
					(
						append ptnsInTri i							
					)				
				)	
				
				local ptnsLeft = remove_from_array ptnsLeft ptnsInTri			
				
				-- RETURNS		
				local returnPtns = #(triPtns[2]) 
				local firstReturn =  makeTri ptnsLeft triPtns[1] triPtns[2]
				if (firstReturn != undefined) do returnPtns = join firstReturn returnPtns
				local secondReturn = makeTri ptnsLeft triPtns[2] triPtns[3]
				if (secondReturn != undefined) do returnPtns = join returnPtns secondReturn		
			)
			
			return returnPtns
		)
	),

	-----------------------------------------------------------------------------------------------
	-- GET THE CONVEX HULL OF POINT ARRAY
	-----------------------------------------------------------------------------------------------
	fn GetHull pntArray = 
	(
		if (pntArray.count != 4) do 
		(			
			local minMaxIdx = minMaxOfArray pntArray
			local ptnToGo = remove_from_array pntArray minMaxIdx
			
			local returnPoints_A = makeTri ptnToGo pntArray[minMaxIdx[1]] pntArray[minMaxIdx[2]]
			local returnPoints_B = makeTri ptnToGo pntArray[minMaxIdx[2]] pntArray[minMaxIdx[1]] 
			
			local allReturns = join #(pntArray[minMaxIdx[1]]) returnPoints_A
			allReturns = join allReturns  #(pntArray[minMaxIdx[2]])
			allReturns = join allReturns returnPoints_B
			
			-- ADD BACK IN THE FIRST ONE
			allReturns = join allReturns #(pntArray[minMaxIdx[1]])			
			return allReturns 	
		)		
	)
)

------------------------------------------------------------------------------------
-- HOLDS A BUNCH OF MATHS RELATED FUNCTIONS
------------------------------------------------------------------------------------
struct rsta_math
(
	-- GET RANDOM POINT ON/IN MESH 
	fn getRandomPointsOnMesh pCount obj volume:false = 
	(
		-- MAKE TEMP COPY
		local source = copy obj
		convertToMesh source
		
		-- GET POINTS 
		local pSys
		if (volume) then
		(	
			pSys = PCloud formation:3 emitter:source speed:0 Total_Number:pCount \
			quantityMethod:1 viewPercent:100 Emitter_Start:0f \
			Emitter_Stop:0f seed:(random 1 5000) viewType:1 			
		)
		else 
		(
			pSys = 	PArray formation:3 numDistinctPoints:pCount quantityMethod:1 \
			Total_Number:pCount viewPercent:100 \
			emitter:source subsampleCreationTime:off \
			seed:(random 1 5000) Emitter_Start:0f Emitter_Stop:0f
		)
		pPoints = for i = 1 to pCount collect particlePos pSys i	
		
		-- CLEAN UP AND RETURN
		delete pSys	
		delete source
		return pPoints 
	),

	-- REMOVES ROUNDING ERRORS IN FLOATS 
	fn AreFloatsEqual f1 f2 =
	(
		local fAccuracy = 2.^(int(log f1 / log 2) - 23)
		if f1 < f2 do swap f1 f2
		return ((f1 - f2) <= fAccuracy)
	),
	
	-- VOLUME OF A TETRAHEDRON 
	fn VolumeOfTetra p1 p2 p3 p4 =
	(	
		local triArea = TriangleArea #(p1,p2,p3)
		
		local c = (p1 + p2 + p3)/3
		local v = p4 - c
		local normal = cross (p2-p1) (p3-p1)
		local dist = distance p4 c	
		local height = dist * abs((dot (normalize(normal)) (normalize(v)))) 
		
		return (1.0/3) * triArea * height	
	),
	
	
	-- DISTANCE FROM LINE TO POINT  --------------------------------------------------	
	fn DistanceFromPointToLine p1 p2 pX =
	(
		local px = p2.x-p1.x
		local py = p2.y-p1.y

		local pxS_pyS = px*px + py*py

		local u =  ((pX.x - p1.x) * px + (pX.y - p1.y) * py) / pxS_pyS

		if u > 1 then u = 1
		else if u < 0 do u = 0

		local x = p1.x + u * px
		local y = p1.y + u * py

		local dx = x - pX.x
		local dy = y - pX.y

		local dist = sqrt(dx*dx + dy*dy)
		return dist
	), 

	-- IS POINT LEFT OF LINE ----------------------------------------------------------------------
	fn IsPointLeftOfLine p1 p2 pX =
	(
		return ((p2.x - p1.x)*(pX.y - p1.y) - (p2.y - p1.y)*(pX.x - p1.x)) > 0
	), 
	
	-- ARE THE 4 POINT UNIQUE
	fn are4PointUnique p1 p2 p3 p4 =
	(
		if (p1 == p2) do return false
		if (p1 == p3) do return false
		if (p1 == p4) do return false
		
		if (p2 == p3) do return false
		if (p2 == p4) do return false
		 
		if (p3 == p4) do return false
		 
		return true
	),
	
	-- ARE 4 POINTS COPLANAR
	fn isTetraCoplanar p1 p2 p3 p4 =
	(
		if not(are4PointUnique p1 p2 p3 p4) do return true
		
		local v1 = p2 - p1 
		local v2 = p3 - p1 
		local CrossA = normalize (cross v1 v2)
		
		local v3 = p2 - p4 
		local v4 = p3 - p4 
		local CrossB = normalize (cross v3 v4)
		local dotP = dot CrossA CrossB		
		if (dotP == 1 OR dotP == -1) do return true
		return false
	),
	
	-- GET THE CIRCUMCENTER OF A TETRAHEDRON
	fn GetCircumCenterOfTetra a b c d =
	(	
		if not (isTetraCoplanar a b c d) do 
		(
			-- EDGES
			local xba = b.x - a.x
			local yba = b.y - a.y
			local zba = b.z - a.z
			
			local xca = c.x - a.x
			local yca = c.y - a.y
			local zca = c.z - a.z
			
			local xda = d.x - a.x
			local yda = d.y - a.y
			local zda = d.z - a.z	
			
			-- LENGTHS
			local balength = (xba^2) + (yba^2) + (zba^2)
			local calength = (xca^2) + (yca^2) + (zca^2)
			local dalength = (xda^2) + (yda^2) + (zda^2)
			
			-- CROSS
			local xcrosscd = yca * zda - yda * zca
			local ycrosscd = zca * xda - zda * xca
			local zcrosscd = xca * yda - xda * yca
			
			local xcrossdb = yda * zba - yba * zda
			local ycrossdb = zda * xba - zba * xda
			local zcrossdb = xda * yba - xba * yda
			
			local xcrossbc = yba * zca - yca * zba
			local ycrossbc = zba * xca - zca * xba
			local zcrossbc = xba * yca - xca * yba
			
			-- DENOMINATOR
			local denominator = 0.5 / (xba * xcrosscd + yba * ycrosscd + zba * zcrosscd)
			
			local xcirca = (balength * xcrosscd + calength * xcrossdb + dalength * xcrossbc) * denominator
			local ycirca = (balength * ycrosscd + calength * ycrossdb + dalength * ycrossbc) * denominator
			local zcirca = (balength * zcrosscd + calength * zcrossdb + dalength * zcrossbc) * denominator
			
			-- ADD POINT A BACK IN
			local circumcenter = a + [xcirca, ycirca ,zcirca]
			
			return circumcenter
		)
		-- POINTS ARE COPLANER
		return undefined
	), 
	
	-- IS POINT IN SPHERE, sCenter is the center of the sphere, sRadius is the radius of the sphere and pX is the Test point
	fn isPointInSphere sCenter sRadius pX surface:false = 
	(
		local vecDist =  distance sCenter pX
		if not(surface) AND (AreFloatsEqual vecDist sRadius) do return false 
		if(vecDist < sRadius) do  return true	
		return false
	),
	
	-- IS POINT IN TETRA / pX is the test
	fn isPointInTetra p1 p2 p3 p4 pX = 
	(
		local faceArray = #(#(p1,p2,p3),#(p1,p4,p2),#(p2,p4,p3),#(p3,p4,p1))
		for f in faceArray do 
		(
			local v_normal = cross (normalize (f[2]-f[1])) (normalize (f[3]-f[1]))
			local c = (f[1] + f[2] + f[3])/3
			local v_cPx = pX - c		
			local result = dot v_cPx v_normal
			-- IF 0 ITS ON THE SURFACE, BELOW IS OUTSIDE THE TETRA
			if (result >= 0) do return false
		)		
		return true
	),	
	
	-- DOES A VECTOR PASS THROUGH A TRIANGLE (p1,p2,p3), RETURN A DATA PAIR <i.hit (bool) i.pos (point3)>
	fn vecTri_instersection p1 p2 p3 v1 v2 dir:false offset:0 =
	(	
		-- Möller–Trumbore intersection algorithm
		
		local hit = dataPair hit:false pos:[0,0,0]		

		Local D = if (dir) then (v2) else (v2 - v1)
		local e1 = p2 - p1
		local e2 = p3 - p1	

		local P = cross D e2
		local det = dot e1 P
			
		if (det == 0) do return hit
		local inv_det = 1/det
		
		local T = v1 - p1
		local U = (dot T P) * inv_det
		
		if (U < 0 OR U > 1) do return hit
		
		local Q = cross T e1
		local V = (dot D Q) * inv_det 

		if (V < 0 OR U + V  > 1) do return hit
	 
		T = (dot e2 Q) * inv_det
	 
		if(T > 0) do 
		(
			-- CHECK HIT DISTANCE  
			local hitPos =  v1 + (D * t)
			if ((distance hitPos v1) + offset < distance v1 v2) do 
			(		
				hit.pos = hitPos
				hit.hit = true
			)		
			return hit
		)

		return hit
	),
	
	-- DOES A TRI INTERSECT ANOTHER TRI
	fn triTri_intersection p1 p2 p3 p4 p5 p6 =
	(
		for v in #(#(p4,p5),#(p5,p6),#(p6,p4)) do 
		(
			if ((vecTri_instersection p1 p2 p3 v[1] v[2]).hit) do return true
		)
		return false
	),
	
	fn getVertsPosFromFaceOfMesh mesh f =
	(
		local faceVerts = getFace mesh f
		local p1 = getVert mesh faceVerts[1]
		local p2 = getVert mesh faceVerts[2]
		local p3 = getVert mesh faceVerts[3]
		return #(p1,p2,p3)
	),
	
	fn getNormalOfTri p1 p2 p3 =
	(
		return normalize ((cross (p2 - p1) (p3 - p1)) * -1)
	),
	
	fn closestVertOnMeshFromPoint obj pX = 
	(
		local closestVertIdx
		local closestVertDist
		
		for idx=1 to obj.numVerts do 
		(
			local v = getvert obj idx
			local dist = distance v pX
			
			if (closestVertIdx == undefined OR dist < closestVertDist) do 
			(
				closestVertIdx = idx
				closestVertDist = dist
			)
		)
		
		return closestVertIdx
	),

	fn closestFaceOnMeshFromPoint obj pX = 
	(
		local closestFaceIdx
		local closestFaceDist
		
		for idx=1 to obj.numFaces do 
		(
			local f = meshop.getFaceCenter obj idx
			local dist = distance f pX
			
			if (closestFaceIdx == undefined OR dist < closestFaceDist) do 
			(
				closestFaceIdx = idx
				closestFaceDist = dist
			)
		)
		
		return closestFaceIdx
	),

	fn closetPointOnMeshFromPoint obj pX =
	(	
		local closestVertIdx = closestVertOnMeshFromPoint obj pX
		local closetsFaceIdx = closestFaceOnMeshFromPoint obj pX	
		
		local closestVertPos = getvert obj closestVertIdx
		local closetsFacePos = meshop.getFaceCenter obj closetsFaceIdx
		
		local normal, closestPoint, closetsDist
		
		if (distance closestVertPos px) < (distance closetsFacePos px) then 
		(
			closetsDist = distance closestVertPos px
			closestPoint = closestVertPos
			normal = normalize ((getnormal obj closestVertIdx) * -1)
		)
		else 
		(
			closetsDist = distance closetsFacePos pX
			closestPoint = closetsFacePos			
			
			local faceVertsPos = getVertsPosFromFaceOfMesh obj closetsFaceIdx
			normal = getNormalOfTri faceVertsPos[1] faceVertsPos[2] faceVertsPos[3]
		)	 

		local theRay = ray ( pX - (normal * 100)) normal
		local hit = intersectRayEx obj theRay			
			
		if (hit != undefined) AND ((distance hit[1].pos pX) < closetsDist) do closestPoint = hit[1].pos		

		return closestPoint
	)
)
rsta_math = rsta_math()
