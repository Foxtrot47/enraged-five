fn RSTA_getToolRevision toolFileName versionNumber = 
(
	--toolFileName = (RsConfigGetWildWestDir() + "script\\3dsMax\\Characters\\Rigging\\ambientFacial\\faceRigger_arcGuiCreation.ms")

	if runFromRigToolsPallette == true do
	(
		if rigToolsFileName != undeifned do 
		(
			toolFileName = rigToolsFileName
		)
	)
	fileLength = toolFileName.count 
	subst = (substring toolFileName (fileLength - 2) -1)
	
	if subst == ".ms" then
	(
		testSync = true
	) 
	else
	(
		if (substring toolFileName (fileLength - 3) -1) == ".max" then
		( 
			testSync = true
		)
		else
		(
			testSync = false
		)
	)
	
-- 	versionNumber = undefined 
	if testSync == true then 
	(
		local fstats = ( gRsPerforce.getFileStats toolFileName )[ 1 ]
		local haveRev = fstats.Item[ "haveRev" ] as integer
			format ("haveRev: "+(haveRev as string)+"\n")
		local headRev = fstats.Item[ "headRev" ] as integer
			format ("headRev: "+(headRev as string)+"\n")		
		if versionNumber != undefined then
		(		
			versionNumber = ((versionNumber as string)+"."+(haveRev as string))
		)
		else
		(
			thisData = #()
			append thisData haveRev
			append thisData headRev
			
			versionNumber = thisData
		)
	)
	else
	(
		messagebox ("WARNING! Could not version rig as could not deduct which version of this tool you are running from p4") beep:true title:"Versioning"
	)
	--messagebox ("version # :"+(versionNumber as string))
	format ("version # :"+(versionNumber as string)+"\n")
	return versionNumber
)

fn RSTA_addEditFileInPerforce output_name =
(
	existing = doesFileExist output_name
	
	if existing == true then  
	(
		gRsPerforce.sync output_name silent:true 
		gRsPerforce.edit output_name silent:true
		
		format ("Updated "+output_name+" in p4.\n")
	)	
	else --ok we need to add to p4
	(
		gRsPerforce.add #(output_name) silent:true
		format ("Added "+output_name+" to p4.\n")
	)				
)