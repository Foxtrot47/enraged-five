-------------------------------
-- Light type preset Core --
-------------------------------

--Author: Neil Gregory
--Date: 12/2011
--Version: 1.0

--------------
--Includes
--------------
try( filein "pipeline/util/xml.ms") catch()

try( filein "script/stratup/objects/util/ragelight.ms") catch()

struct LightPresetCore
(
	RsLightTypePresetXMLFile = (RsConfigGetWildWestDir() + "/script/max/Rockstar_North/LightTypePresets.xml"),
	gLightPresetDict = dotnetobject "RSG.MaxUtils.MaxDictionary",
	gUIColours = #(),
	UIColSwatch = undefined,
	UI_ddPreset = undefined,
	flashinessValues = #("Constant", "Random", "Random -On if Wet", "One a second", "Two a second", "Five a second", \
							"RandomFlashiness", "Off (Traffic Light)", "UNDEFINED", "Alarm", "On when Raining", "Cycle 1", \
							"Cycle 2", "Cycle 3", "Disco", "Candle", "Plane", "Fire"),
	
	presetComparatorDict = dotNetObject "RSG.MaxUtils.MaxDictionary",
	
	----------------
	--Functions
	----------------
	fn typeCast inType inValue =
	(
		outValue = undefined
		
		case inType of
		(
			"bool":
			(
				outValue = inValue as booleanClass
			)
			
			"int":
			(
				outValue = inValue as Integer
			)
			
			"float":
			(
				outValue = inValue as Float
			)
			
			"string":
			(
				outValue = inValue as String
			)
			
			"Point3":
			(
				--val[1] = val[1] as Point3
				pa = filterString inValue "[],"
				outValue = point3 ((pa[1] as float) * 255) ((pa[2] as float)*255) ((pa[3] as float)*255)
			)
			
			
		) --end case
		
		outValue
	
	),
	
	fn floatToColourArray RGBArray = 
	(
		rgbOut = #()
		for element in RGBArray do
		(
			conv = int(element * 255)
			append rgbOut conv
		)
		
		rgbOut
	),
	
	fn presetAttrToPoint3 input =
	(
		pa = filterString input "[],"
		pa[1] = (pa[1] as float) 
		pa[2] = (pa[2] as float) 
		pa[3] = (pa[3] as float) 
		val = point3 pa[1] pa[2] pa[3]
		val
	),
	
	--return the attributes an xml element has as an array
	fn getElementAttrValues element = 
	(
		attrs = #()
		
		--format "Element: %\n" element.name
		--print element.Attributes
		
		--check its not a comment
		if element.name != "#comment" and element.HasAttributes == true then
		(
			--format "Element: % has attributes" element.name
			attrCollection = element.Attributes
			--test = attrCollection.count
			--print test
			/*
			struct XmlAttr
			(
				value,
				type
			)
			
			*/	
			for attr = 0 to (attrCollection.Count - 1) do
			(
				--attr struct
				--attrDict = XmlAttr()
				--show attrCollection.ItemOf(attr)
				--attrDict.value = attrCollection.ItemOf[attr].name
				--attrDict.type = attrCollection.ItemOf[attr].value
			
				append attrs attrCollection.ItemOf[attr].value
			)
		)
		--return
		attrs
	),
	
	
	fn getLightTypePresets =
	(
		stream = dotNetObject "System.IO.StreamReader" RsLightTypePresetXMLFile
		local XML = dotNetObject "System.Xml.XmlDocument"
		XML.load stream
		root = XML.documentElement
		
		presets = root.selectNodes("Preset")
		
		--Child node values dictionary builder
		---------------------------------------------------------------
		fn buildValuesDict nodeList =
		(
			nodeListIt = nodeList.getEnumerator()
			dict = dotnetobject "RSG.MaxUtils.MaxDictionary"
			--local timeAttrs = #()
			
			while nodeListIt.moveNext() != false do
			(
				thisKid = nodeListIt.current
				--get node attrs value, type
				thisKidAttrs = thisKid.attributes
				
				--TimeDict				key		values:( value						type				)
				dict.add thisKid.name #(thisKidAttrs.ItemOf["value"].value, thisKidAttrs.ItemOf["type"].value)
			)
			
			--return 
			dict
		)
		---------------------------------------------------------------
		
		for p = 0 to presets.count - 1 do
		(
			presetNode = presets.itemOf[p]
			presetNodeAttrs = presetNode.attributes
			UIName = presetNodeAttrs.ItemOf["name"].value
			-----------------------------------
			--create light preset Dict
			-----------------------------------
			presetDict = dotnetobject "RSG.MaxUtils.MaxDictionary"
			
			-------------------
			--get children
			-------------------
			presetKids = presetNode.selectNodes("*")
			
			--iterate kids
			presetKidsIt = presetKids.getEnumerator()
			while presetKidsIt.MoveNext() != false do
			(
				--node
				presetKidNode = presetKidsIt.current
				--get name
				nodeName = presetKidNode.name
				
				--add time or light photo elements first then the reset
				case nodeName of
				(
					"GtaTime":
					(
						--get child nodes
						timeKids = presetKidNode.selectNodes("*")
						GtaTimeDict = buildValuesDict(timeKids)

						presetDict.add "GtaTime" GtaTimeDict
					)
					
					"GtaLightPhoto":
					(
						--get child nodes
						photoKids = presetKidNode.selectNodes("*")
						GtaPhotoDict = buildValuesDict(photoKids)

						presetDict.add "GtaLightPhoto" GtaPhotoDict
					)
					
					default:
					(
						--get node attrs
						--print nodeName
						thisKidAttrs = presetKidNode.attributes
						presetDict.add nodeName #(thisKidAttrs.ItemOf["value"].value, thisKidAttrs.ItemOf["type"].value)
					)
				)
			)
			
			--append it to the list of presets
			gLightPresetDict.add UIName presetDict
		)
		
		
		stream.dispose()
		stream = undefined
	),
	
	--LightTypePresets array returned
	--This function parses the xml file that contains the presets and creates structs that are added to a struct array
	--and from that an array containing the names of the presets to populate the dropdownlist with.
	
	
	--Gets the value of the specified attribute from the desired preset and returns it.
	fn getPresetValue presetName presetAttr = 
	(
		--return value
		val = undefined
		
		--dict
		preset = gLightPresetDict.Item presetName
		--format "preset: %\n" preset
		
		--quick out where nested dicts are not concerned
		if preset.containsKey presetAttr then return preset.item presetAttr
		
		--get value
		presetIt = preset.getEnumerator()
		while presetIt.movenext() != false do
		(
			kvPair = presetIt.current
			case kvPair.key of
			(
				"GtaTime":
				(
					dict = kvPair.value
					if dict.containsKey presetAttr then
					(
						val = dict.Item presetAttr
						return val
					)
				)
				
				"GtaLightPhoto":
				(
					dict = kvPair.value
					if dict.containsKey presetAttr then
					(
						val = dict.Item presetAttr
						return val
					)
				)
				
				default:
				(
					if kvPair.key == presetAttr then
					(
						val = kvPair.value
						return val
					)
				)
			)
			
		)
		/*
		if preset.containsKey presetAttr then
		(
			val = preset.Item presetAttr
		)
		*/
		--if val == undefined then format "Problem getting the preset value: %\n" presetAttr
		--format "val: % \n" val
		val
	),
	
	
	--Apply the preset chosen in the UI to the currently selected RAGE light(s)
	fn applyPreset presetName = 
	(
		
		selObjs = for o in getCurrentSelection() where classOf o == RageLight collect o
		--check we have something
		if selObjs.count == 0 then
		(
			messagebox "No objects selected"
		)
		
		--check for invalid preset like a divider
		if matchPattern presetName pattern:"--*--" == true then
		(
			return 0
		)
		
		--iterate through object selection
		
		--NOTE use this style? local refObjs = for obj in geometry where isRSref obj collect obj
		for obj in selObjs do
		(
			--if we got RageLights then apply the preset settings
			--if classOf obj == RageLight then
			--(
				-------------------------------------
				--Set ordinary properties first
				------------------------------------
				p = getPropNames obj
				
				--skip last property as it can changed name dependent on setting
				for i = 1 to p.count - 1 do
				(
					--get value from config
					--format "property: %\n" p[i]
					attr = p[i] as string
					--format "attr: %\n" attr
					
					val = getPresetValue presetName attr
					--format "val: %\n" val
					if val != undefined then
					(
						--adjust value based on type
						val[1] = typeCast val[2] val[1]
						
						--Set the property
						setProperty obj p[i] val[1]
					)

				)
			
				------------------------------------
				--Set GTA attributes
				------------------------------------
				
				--Gta LightPhoto
				attrGta_Count = GetNumAttr "Gta LightPhoto"
				timeAttrsDict = getPresetValue presetName "GtaTime" --is a dict
				lightPhotoAttrsDict = getPresetValue presetName "GtaLightPhoto" --is a dict
				
				--GtaLightPhotoAttrs = timeAttrs + lightPhotoAttrs
				
				index = 1 --First attr is a node id hash we dont store
				--iterators
				timeAttrsIt = timeAttrsDict.values.getEnumerator()
				timeLightPhotoIt = lightPhotoAttrsDict.values.getEnumerator()
				--check iterator counts against number of gta lightphoto attrs
				--if attrGta_Count != (timeAttrsIt.count + timeLightPhotoIt.count) then messagebox "attr count mismatch: 350" title:"Error"
				
				--iterate GtaTime dict first
				while timeAttrsIt.moveNext() != false do
				(
					thisAttr = timeAttrsIt.current
					val = typeCast thisAttr[2] thisAttr[1]
					SetAttr obj index val
					index += 1
				)
				
				
				--iterate GtaLightPhoto dict first
				timeLightPhotoIt = lightPhotoAttrsDict.values.getEnumerator()
				while timeLightPhotoIt.moveNext() != false do
				(
					thisAttr = timeLightPhotoIt.current
					--format "thisAttr: % \n" thisAttr
					--format "expected attr: % \n" (getattrname "Gta LightPhoto" index)
					val = typeCast thisAttr[2] thisAttr[1]
					--format "val: % \n" val
					
					--check for type error
					valid = assert_true ((GetAttrType "Gta LightPhoto" index) == thisAttr[2]) message:"Type mismatch"
					if valid then SetAttr obj index val
					index += 1
				)
				
				/*
				for index =1 to attrGta_Count do
				(
					
					attr = GtaLightPhotoAttrs[index]
					--format "index: % attr[1]: % attr[2] %\n" index attr[1] attr[2]
					
					val = typeCast attr[2] attr[1]
					
					SetAttr obj index val
					
				)
				*/
				--set the object data for future reference
				setAppData obj 1 presetName
				
			--)
		)
		
		--update the ragelights ui
		--$.refreshUI()
	),
	
	fn selectedPresetQuery = 
	(
		presetType = "None"
		--find what the current preset for this light is
		sel = getCurrentSelection()
		--print sel
		lightsOnly = for n in sel where classOf n == RageLight collect n
		
		presets = for n in lightsOnly where (getAppData n 1) != undefined collect getAppData n 1
		
		--check if all the same or not
		if presets.count > 1 then
		(
			--check for uniformity
			for p = 1 to presets.count - 1 do
			(
				if presets[p] != presets[p+1] then
				(
					presetType = "Mixed"
					LightTypePresetTool.txtPresetValue.text = presetType as string
					return presetType
					
				)
				else
				(
					presetType = presets[1]
				)
			)
		)
		else if presets.count == 1 then
		(
			presetType = presets[1]
		)
		else
		(
			presetType = "None"
		)
		
		--return
		--format "PresetType: % \n" presetType
		--set the UI for the selected
		LightTypePresetTool.txtPresetValue.text = presetType as string
		
		--return
		presetType
	),
	
	fn selectByPreset preset =
	(
		--print preset
		--get all the RageLights in the scene
		rageLights = for l in $lights where classOf l == RageLight collect l
			
		--check each for its current preset
		matches = for l in rageLights where (getAppData l 1) == preset collect l
		--print matches
		--select them
		clearSelection()
		select matches
		
	),
	
	fn getPresetComparatorDict =
	(
		--collect all the values for a preset and make them the key to a dict
		--the value will be the preset name
		--presetComparatorDict = dotNetObject "RSG.MaxUtils.MaxDictionary"
		--presetComparatorKeyValuePairs = #()
		
		rootPresetIt = gLightPresetDict.getEnumerator()
		while rootPresetIt.moveNext() == true do
		(
			presetName = ""
			comparatorKey = ""
			
			thisItem = rootPresetIt.current
			--keyStr = dotNetObject "System.String" thisItem.key
			--if keyStr.startsWith "--" == false then --not a divider
			if matchPattern thisItem.key pattern:"--*--" == false then
			(
				presetName = thisItem.key
				presetIt = thisItem.value.getEnumerator()
				while presetIt.moveNext() == true do
				(
					presetItem = presetIt.current
					--print (classOf presetItem.value)
					if classOf presetItem.value == Array then
					(
						comparatorKey += "|"
						comparatorKey += presetItem.value[1]
					)
					else --nested Dict
					(
						gtaAttrIt = presetItem.value.getEnumerator()
						while gtaAttrIt.moveNext() == true do
						(
							comparatorKey += "|"
							comparatorKey += gtaAttrIt.current.value[1]
						)
					)
				)
			)
			
			--format "presetname: % key: % \n" presetName comparatorKey
			try
			(
				if presetComparatorDict.ContainsKey comparatorKey == false then
				(
					presetComparatorDict.Add comparatorKey presetName
				)
			)
			catch()
			
		)
	
		--Now collect all the values for the light and try to make a key. if successful
		--then the lights attrs dont match a preset
		--if they do then you can find the preset from the value.
		
	),
	
	fn presetComparator theLight =
	(
		--Now collect all the values for the light and try to make a key. if successful
		--then the lights attrs dont match a preset
		--if they do then you can find the preset from the value.
		
		thePreset = "None"
		valueString = ""
		
		-------------------------------------
		--Get ordinary properties first
		------------------------------------
		p = getPropNames theLight
		
		--skip last property as it can changed name dependent on setting
		for i = 1 to p.count - 1 do
		(
			valid = true
			for test in #(#lightNodeRef, #lightMeshColourEnabled, #lightAttenShow, #lightShadowMap, #lightMeshResolution, #lightMeshSize, #lightMeshColour) do
			(
				if p[i] == test then valid = false
			)
			
			if valid then
			(
				--Get the property
				prop = getProperty theLight p[i]
				--fix colour format if we have a colour. convert to Point3 0-1
				if classOf prop == Color then
				(
					prop = (prop * [1,1,1]) as Point3
				)					
				valueString += "|"
				valueString += prop as string
			)
		)
	
		------------------------------------
		--Get GTA attributes
		------------------------------------
		/*
		--Gta LightPhoto
		attrGta_Count = GetNumAttr "Gta LightPhoto"
		timeAttrsDict = getPresetValue "Red" "GtaTime" --is a dict
		lightPhotoAttrsDict = getPresetValue "Red" "GtaLightPhoto" --is a dict
		
		--GtaLightPhotoAttrs = timeAttrs + lightPhotoAttrs
		
		index = 1 --First attr is a node id hash we dont store
		--iterators
		timeAttrsIt = timeAttrsDict.getEnumerator()
		--check iterator counts against number of gta lightphoto attrs
		--if attrGta_Count != (timeAttrsIt.count + timeLightPhotoIt.count) then messagebox "attr count mismatch: 350" title:"Error"
		*/
		for i = 1 to getNumAttr "Gta LightPhoto" do
		(
			valid = true
			--knock attrs
			
			for test in #("Attach", "Dont Export", "GlobalHandle", "INT and EXT") do
			(
				if (getAttrName "Gta LightPhoto" i) == test then valid = false
			)
			
			if valid then
			(
				valueString += "|"
				valueString += (getAttr theLight i) as string
			)
		)
		
		/*
		--iterate GtaTime dict first
		index = 1
		while timeAttrsIt.moveNext() != false do
		(
			--format "key: % \n" timeAttrsIt.current.key
			--gtaAttrIdx = getAttrIndex "GtaTime" timeAttrsIt.current.key
			valueString += (getAttr theLight index) as string
			index += 1
		)
		
		--iterate GtaLightPhoto dict first
		timeLightPhotoIt = lightPhotoAttrsDict.getEnumerator()
		while timeLightPhotoIt.moveNext() != false do
		(
			--valid = assert_true ((GetAttrType "Gta LightPhoto" index) == thisAttr[2]) message:"Type mismatch"
			--insert space before caps to build valid gta lightphoto name
			timeLightPhotoIt.current.key
			
			gtaAttrIdx = getAttrIndex "Gta LightPhoto" timeLightPhotoIt.current.key
			valueString += (getAttr theLight gtaAttrIdx) as string
		)
		*/
		--print valueString
		
		--try and add it to the presetComparatorDict
		if presetComparatorDict.ContainsKey valueString == true then --theLight matches a preset
		(
			--get the value of the key to get the presetname
			presetComparatorDict.tryGetValue valueString &thePreset
		)
		
		--return
		thePreset
	
	)
	
)--END STRUCT

lightPresetCore = LightPresetCore()