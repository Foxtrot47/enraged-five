-------------------------------------------------------------
-- Common object-data functions:
-------------------------------------------------------------

-------------------------------------------------------------
-- Will this object use quaternion rotations in game (Full Matrix) or just the z-rotation?
-- Returns True if obj has rotation great enough for game to set this:
-------------------------------------------------------------
fn RsHasAutoFullMatrix obj minQuatVal:0.05 = 
(
	local objRot = obj.rotation
	
	((abs objRot.X) > minQuatVal) or ((abs objRot.Y) > minQuatVal)
)
-------------------------------------------------------------
-- How far is object tilted from vertical? (degrees)
-------------------------------------------------------------
fn RsGetObjTilt obj = 
(
	local objDir = obj.dir
	
	atan (length [objDir.x, objDir.y] / objDir.z )
)
-------------------------------------------------------------
-- Will return true if the passed nodes are intersecting *OR* coplaner (this is how it is different to the Intersects built in function)
-------------------------------------------------------------
fn RSNodesAreProximal InputNodeA InputNodeB =
(
	if (InputNodeA.min.x <= InputNodeB.max.x and InputNodeA.min.y <= InputNodeB.max.y and InputNodeA.min.z <= InputNodeB.max.z) then
	(
		if (InputNodeA.max.x >= InputNodeB.min.x and InputNodeA.max.y >= InputNodeB.min.y and InputNodeA.max.z >= InputNodeB.min.z) then
		(
			return True
		)
	)
	
	return False
)
-------------------------------------------------------------
-- returns array of objects in entity sets
-------------------------------------------------------------
fn all_entityset_objects =
(
	object_array = #()
	for h in helpers where (classof h == InteriorGroupHelper) do
	(
		join object_array h.interiorGroupObjects
	)
	return object_array
)
-------------------------------------------------------------
-- returns the entity set of a given object
-------------------------------------------------------------
fn get_entityset_from_object obj =
(
	for h in helpers where (classof h == InteriorGroupHelper) do
	(
		if ((findItem h.interiorGroupObjects obj) != 0) do return h
	)
)