/*------------------------------------------------------------------------------
Summary:
	Copies all the animation tracks from SourceObject to TargetObject
*/------------------------------------------------------------------------------
fn RSCopyAllTracks SourceObject TargetObject =
(
	for TrackID = 1 to SourceObject.NumSubs do
	(
		CurrentSourceSubAnim = SourceObject[TrackID]
		
		if( CurrentSourceSubAnim.Controller != Undefined ) then
		(
			TargetObject[CurrentSourceSubAnim.Name].Controller = Copy CurrentSourceSubAnim.Controller
		
		)
	)
)

/*------------------------------------------------------------------------------
Summary:
	Copies the input animation track (TrackName) from SourceObject to TargetObject
*/------------------------------------------------------------------------------
fn RSCopyTrack SourceObject TargetObject TrackName =
(
	SourceSubAnim = SourceObject[TrackName]
	
	if( SourceSubAnim.Controller != Undefined ) then
	(
		TargetObject[TrackName].Controller = Copy SourceSubAnim.Controller
	)
)	
