struct rsta_dependency
(
	-- LOCALS ---------------------------------------
	enabled = true,
	files = #(),
	filesToSync = #(), 
	filesNotInP4 = #(),
	checked = #(),
	-- FUNCTIONS ----------------------------------
	fn add filePath =
	(
		append files filePath
	),
	---------------------------------------------------------
	fn email address subject body =
	(		
		Process = DotNetObject "System.Diagnostics.Process"	
		Process.Start ("mailto:" + address + "?subject=" + subject + "&body=" + body)
	),
	---------------------------------------------------------
	fn check niceName restart =
	(
		if (enabled) do
		(
			if (findItem checked niceName) == 0 AND files.count != 0 do 
			(
				append checked niceName
				
				local p4Exist = gRsPerforce.exists files			
				for fileIdx = 1 to files.count do
				(	
					local file = files[fileIdx]	
					
					-- DOES IT EXIST ON THE P4 
					if ((findItem p4Exist fileIdx) != 0) then 
					(
						local fstat = (gRsPerforce.getFileStats file)[1]	
						if fstat.item "haveRev" != fstat.item "headRev" do append filesToSync file	
					)
					else append filesNotInP4 file											
				)			
				
				-- DON'T RUN AS FILES ARE MISSING
				if (filesNotInP4.count != 0) then
				(
					format "filesNotInP4 : %\n" filesNotInP4
					local message = niceName + " has missing files and may not work correctly.\n\nWould you like to email TechArt?"
					if (queryBox message title:"RS TechArt Dependency") do
					(				
						local NEWLINE = "%0D%0A"
						local email_ad = "*Default_Tech_Art@rockstarnorth.com"
						local email_sub = "RSTA Dependency : Missing File for " + niceName
						local email_messsage = "***This is an automated message from rsta_dependency***" + NEWLINE + NEWLINE
						email_messsage += "While trying to launch " + niceName + " via " + restart + " these files were not found in perforce..." + NEWLINE + NEWLINE
						for file in filesNotInP4 do email_messsage += file + NEWLINE
						email_messsage += NEWLINE + "They are listed as dependencies, and should be submitted to perforce."
						--------------------------------------------------------------------------
						email email_ad email_sub email_messsage
					)
					filesNotInP4 = #()
				)
				-- GRAB LATEST 
				else if (filesToSync.count != 0) do 
				( 
					local message = niceName + " is out of date, do you want to sync to the latest version?\n\n"
					for f in filesToSync do message += f + "\n"
					if (queryBox message title:"RS TechArt Dependency") do gRsPerforce.sync filesToSync	
					filesToSync = #()
					
					-- AND RESTART
					filein restart
				)
			)
		)		
	)
)	
rsta_dependency = rsta_dependency()


