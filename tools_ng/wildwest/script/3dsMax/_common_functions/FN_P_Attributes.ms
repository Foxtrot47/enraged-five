-- ************************************************************
-- Stewart Wright
-- Rockstar North
-- Created on 30/07/10

-- Collection of functions that will be called in the main prop attribute tagging script
-- ************************************************************
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
attrMSAP = 1
stateMSAP = 2
triMSAP = 3
readAttr = undefined
readDamageTag = undefined
effectNames = #() --particle effects list

--drop down lists
ddlGGlassTypeItems = #()
ddlCCollTypeItems = #()
ddlEExplosionTypeItems = #()
ddlETriggerTypeItems = #()
ddlPTriggerTypeItems = #()




-- predefine geometry variables
comboGeoVarOrder = #(#("LOD distance",10, 0), #("Export Vertex Colours", false, 0), #("Level Design Object", false, 0), #("Dont Add To IPL", false,0), #("Dont Export", false, 0), 
		#("Is Fixed", false, 0), #("Is Dynamic", false, 0), #("Backface Cull", true, 1), #("Does Not Provide AI Cover", false, 0), #("Milo Preview", true, 1), 
		#("Is Fragment", false, 0), #("Dont cast shadows", false, 0), #("Has Door Physics", true, 0), #("Pack Textures", false, 0), #("Is Fixed For Navigation", false, 0),
		#("Has Anim", false, 0), #("Has UvAnim", false, 0), #("Enable Ambient Multiplier", true, 0), #("Static Shadows", true, 1), #("Dynamic Shadows", true, 1),
		#("Is Cloth", false, 0), #("Is Breakable Glass", false, 0), #("Glass type", "PANE", 2), #("TXD", "", 0), #("radTXDState", 3, 2), #("ISDAMAGE = TRUE", false, 0))
	
-- predefine collision variables
comboCollVarOrder = #(#("Coll Type", "DEFAULT", 1), #("Audio Material", "", 0), #("Non Climable", false, 0), #("See Through", false, 0), #("Shoot Through", false, 0),
		#("Non Camera Collidable", false, 0), #("radAudioMatState", 3, 2))

-- predefine explosion variables
comboExpVarOrder = #(#("Attach to All", false, 0), #("Explosion Tag", "Grenade", 2), #("Trigger", "Shot Generic", 2), #("Ignore Damage Model", false, 0), #("Play On Parent", false, 0),
		#("Only on damage model", false, 0))

-- predefine particle variables
comboParVarOrder = #(#("Name", "", 0), #("Trigger", "AMBIENT FX", 2), #("Attach to All", false, 0), #("Scale", 1, 0), #("Probability", 100, 0), #("Has Tint", false, 0),
		#("R", 255, 0), #("G", 255, 0), #("B", 255, 0), #("Emitter Size X", 1, 0), #("Emitter Size Y", 1, 0), #("Emitter Size Z", 1, 0), #("Ignore Damage Model", false, 0),
		#("Play On Parent", false, 0), #("Only on damage model", false, 0), #("radPNameState", 3, 2))

-- predefine audio emmiter variables
comboAudioVarOrder = #(#("Effect", "", 0), #("radAEffectState", 3, 2))


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- a function that will edit the attribute on on/off attributes
-- selType is the superclass to check, obj is the passed in object, selAttr is the attribute to set and selAttrState is what to set it to
fn setAttribute obj selAttr selAttrState = (
	if ((obj != undefined) and (superclassof obj != undefined)) then
	(
		indexAttr = GetAttrIndex (getAttrClass obj) selAttr --work out the index for the attribute
		attrSet = SetAttr obj indexAttr selAttrState --set the attributes
	)
)--end setCollAttribute
-------------------------------------------------------------------------------------------------------------------------
-- a function that will read the attributes of selected meshes
-- selType is the superclass to check, obj is the passed in object, selAttr is the attribute to read and setSelState is what store it in
fn readAttribute obj selAttr setSelState =
(
	if ((obj != undefined) and (superclassof obj != undefined)) then
	(
		indexAttr = GetAttrIndex (getAttrClass obj) selAttr --work out the index for the attribute
		attrGet = GetAttr obj indexAttr --get the attribute value
		readAttr = attrGet
	)
)--end readCollAttribute
-------------------------------------------------------------------------------------------------------------------------
-- a function that will write the damage tags to the user defined properties
-- obj is the passed in object, damageTag is the tag and whatToDo is the checkbox state
fn tagDamage obj damageTag whatToDo =
(
	if whatToDo == true then
	(
		unTagged = true --assume the mesh isn't tagged for damage already
		currentUDP = getUserPropbuffer obj --get current user defined properties
		filterUDP = filterString currentUDP "\r\n"
		for f = 1 to filterUDP.count do --check the UDP to see if the object is already tagged for damage
		(
			tempUDP = uppercase filterUDP[f]
			if tempUDP == damageTag then
			(
				unTagged = false --flag this object as tagged
			)
		)
		--if the object isn't flagged as tagged then tag it
		if unTagged == true then
		(
			oldTag = getUserPropbuffer obj
			if oldTag != "" then
			(
			newTag = oldTag + "\r\n" + damageTag
			setUserPropbuffer obj newTag
			)
			else
			(
			setUserPropbuffer obj damageTag
			)
			--print some debug info in the listener
			prInfo = (obj.name + " has been tagged")
			print prInfo
		)
	)
	else 
	(
		if whatToDo == false then
		(
			keepUDP = #()
			currentUDP = getUserPropbuffer	obj
			filterUDP = filterString currentUDP "\r\n"
			for x = 1 to filterUDP.count do (
				tempUDP = uppercase filterUDP[x]
				if tempUDP != damageTag then
				append keepUDP tempUDP
			)
			tag = ""
			if keepUDP.count == 0 then setUserPropbuffer obj tag
			else
			(
				for x = 1 to keepUDP.count do
				(
					tag = tag + keepUDP[x] + "\r\n"
					setUserPropbuffer obj tag
				)
			)
		)
	)
)--end of tagDamage
-------------------------------------------------------------------------------------------------------------------------
fn readDamage obj damageTag =
(
	currentUDP = getUserPropbuffer obj
	filterUDP = filterString currentUDP "\r\n"
	print filterUDP
	--check the UDP to see if the object is already tagged for damage
	for f = 1 to filterUDP.count do
	(
		tempUDP = uppercase filterUDP[f]
		if tempUDP == damageTag then
		(
			readDamageTag = true --flag this object as tagged
		) else (readDamageTag = false)
	)
)--end readDamage
-------------------------------------------------------------------------------------------------------------------------
--function for populating the settings
fn popTagSettings whatScript =
(
	if whatScript.name == "propGeoTagger" then
	(
		print "populating geometry tag checkboxes in script window"
			whatScript.radGTXDName.state = comboGeoVarOrder[25][stateMSAP] -- not using this will leave the state untouched
		------------------------------
			whatScript.chkGExpVertCol.state = comboGeoVarOrder[2][stateMSAP]
			whatScript.chkGExpVertCol.tristate = comboGeoVarOrder[2][triMSAP]
		------------------------------	
			whatScript.chkGLevDesObj.state = comboGeoVarOrder[3][stateMSAP]
			whatScript.chkGLevDesObj.tristate = comboGeoVarOrder[3][triMSAP]
		------------------------------	
			whatScript.chkGAddToIPL.state = comboGeoVarOrder[4][stateMSAP]
			whatScript.chkGAddToIPL.tristate = comboGeoVarOrder[4][triMSAP]
		------------------------------
			whatScript.chkGExport.state = comboGeoVarOrder[5][stateMSAP]
			whatScript.chkGExport.tristate = comboGeoVarOrder[5][triMSAP]
		------------------------------
			whatScript.chkGIsFixed.state = comboGeoVarOrder[6][stateMSAP]
			whatScript.chkGIsFixed.tristate = comboGeoVarOrder[6][triMSAP]
		------------------------------
			whatScript.chkGIsDynamic.state = comboGeoVarOrder[7][stateMSAP]
			whatScript.chkGIsDynamic.tristate = comboGeoVarOrder[7][triMSAP]
		------------------------------
			whatScript.chkGBackfaceCull.state = comboGeoVarOrder[8][stateMSAP]
			whatScript.chkGBackfaceCull.tristate = comboGeoVarOrder[8][triMSAP]
		------------------------------
			whatScript.chkGProvideAICover.state = comboGeoVarOrder[9][stateMSAP]
			whatScript.chkGProvideAICover.tristate = comboGeoVarOrder[9][triMSAP]
		------------------------------
			whatScript.chkGMiloPreview.state = comboGeoVarOrder[10][stateMSAP]
			whatScript.chkGMiloPreview.tristate = comboGeoVarOrder[10][triMSAP]
		------------------------------
			whatScript.chkGIsFrag.state = comboGeoVarOrder[11][stateMSAP]
			whatScript.chkGIsFrag.tristate = comboGeoVarOrder[11][triMSAP]
		------------------------------
			whatScript.chkGCastShadows.state = comboGeoVarOrder[12][stateMSAP]
			whatScript.chkGCastShadows.tristate = comboGeoVarOrder[12][triMSAP]
		------------------------------
			whatScript.chkGDoorPhys.state = comboGeoVarOrder[13][stateMSAP]
			whatScript.chkGDoorPhys.tristate = comboGeoVarOrder[13][triMSAP]
		------------------------------
			whatScript.chkGPackTex.state = comboGeoVarOrder[14][stateMSAP]
			whatScript.chkGPackTex.tristate = comboGeoVarOrder[14][triMSAP]
		------------------------------
			whatScript.chkGIsFixedForNav.state = comboGeoVarOrder[15][stateMSAP]
			whatScript.chkGIsFixedForNav.tristate = comboGeoVarOrder[15][triMSAP]
		------------------------------
			whatScript.chkGHasAnim.state = comboGeoVarOrder[16][stateMSAP]
			whatScript.chkGHasAnim.tristate = comboGeoVarOrder[16][triMSAP]
		------------------------------
			whatScript.chkGHasUVAnim.state = comboGeoVarOrder[17][stateMSAP]
			whatScript.chkGHasUVAnim.tristate = comboGeoVarOrder[17][triMSAP]
		------------------------------
			whatScript.chkGAmbientMulti.state = comboGeoVarOrder[18][stateMSAP]
			whatScript.chkGAmbientMulti.tristate = comboGeoVarOrder[18][triMSAP]
		------------------------------
			whatScript.chkGStaticShadows.state = comboGeoVarOrder[19][stateMSAP]
			whatScript.chkGStaticShadows.tristate = comboGeoVarOrder[19][triMSAP]
		------------------------------
			whatScript.chkGDynamicShadows.state = comboGeoVarOrder[20][stateMSAP]
			whatScript.chkGDynamicShadows.tristate = comboGeoVarOrder[20][triMSAP]
		------------------------------
			whatScript.chkGIsClothState.state = comboGeoVarOrder[21][stateMSAP]
			whatScript.chkGIsClothState.tristate = comboGeoVarOrder[21][triMSAP]
		------------------------------
			whatScript.chkGIsBreakGlassState.state = comboGeoVarOrder[22][stateMSAP]
			whatScript.chkGIsBreakGlassState.tristate = comboGeoVarOrder[22][triMSAP]
		------------------------------
			whatScript.chkGDamageTag.state = comboGeoVarOrder[26][stateMSAP]
			whatScript.chkGDamageTag.tristate = comboGeoVarOrder[26][triMSAP]
		------------------------------
			whatScript.ddlGGlassType.items = ddlGGlassTypeItems
		-------------------------------------------------------------------------------------------------------------------------
			whatScript.spnGLODVal.range = [0,100,comboGeoVarOrder[1][stateMSAP]]
				try
				(
					if comboGeoVarOrder[1][triMSAP] != true then
					(
						comboGeoVarOrder[1][triMSAP] = false
					)
					else ()
				) catch (print (getCurrentException()))
				whatScript.spnGLODVal.indeterminate = comboGeoVarOrder[1][triMSAP]
		------------------------------
			whatScript.edtGTXDName.text = comboGeoVarOrder[24][stateMSAP]
				try
				(
					if comboGeoVarOrder[24][triMSAP] != true then
					(
						comboGeoVarOrder[24][triMSAP] = false
					)
					else ()
				) catch (print (getCurrentException()))
				whatScript.edtGTXDName.bold = comboGeoVarOrder[24][triMSAP]
				
				if comboGeoVarOrder[24][triMSAP] == true then
					(
					whatScript.edtGTXDName.text = "Mismatch!"
					)
				else
					(
						whatScript.edtGTXDName.text = comboGeoVarOrder[24][stateMSAP]
					)
		------------------------------
			whatScript.ddlGGlassType.selection = findItem ddlGGlassTypeItems comboGeoVarOrder[23][stateMSAP]
	)
	else
	(
		if whatScript.name == "propCollTagger" then
		(
			print "populating collision tag checkboxes in script window"
			------------------------------
				whatScript.ddlCCollType.items = ddlCCollTypeItems
			------------------------------
				whatScript.radCAudioMatState.state = comboCollVarOrder[7][stateMSAP] -- not using this will leave the state untouched
			------------------------------
				whatScript.chkCNonClimb.state = comboCollVarOrder[3][stateMSAP]
				whatScript.chkCNonClimb.tristate = comboCollVarOrder[3][triMSAP]
			------------------------------
				whatScript.chkCSeeThrough.state = comboCollVarOrder[4][stateMSAP]
				whatScript.chkCSeeThrough.tristate = comboCollVarOrder[4][triMSAP]
			------------------------------
				whatScript.chkCShootThrough.state = comboCollVarOrder[5][stateMSAP]
				whatScript.chkCShootThrough.tristate = comboCollVarOrder[5][triMSAP]
			------------------------------
				whatScript.chkCNonCamCollide.state = comboCollVarOrder[6][stateMSAP]
				whatScript.chkCNonCamCollide.tristate = comboCollVarOrder[6][triMSAP]
			------------------------------
				whatScript.edtCAudioMat.text = comboCollVarOrder[2][stateMSAP]
					try
					(
						if comboCollVarOrder[2][triMSAP] != true then
						(
							comboCollVarOrder[2][triMSAP] = false
						)
						else ()
					) catch (print (getCurrentException()))
					whatScript.edtCAudioMat.bold = comboCollVarOrder[2][triMSAP]
					
					if comboCollVarOrder[2][triMSAP] == true then
					(
					whatScript.edtCAudioMat.text = "Mismatch!"
					)
					else
					(
						whatScript.edtCAudioMat.text = comboCollVarOrder[2][stateMSAP]
					)
			------------------------------
				whatScript.ddlCCollType.selection = findItem ddlCCollTypeItems comboCollVarOrder[1][stateMSAP]
		)
		else
		(
			if whatScript.name == "propExpTagger" then
			(
			print "populating explosion tag checkboxes in script window"
				whatScript.chkEAttachToAll.state = comboExpVarOrder[1][stateMSAP]
				whatScript.chkEAttachToAll.tristate = comboExpVarOrder[1][triMSAP]
			------------------------------
				whatScript.ddlEExplosionType.items = ddlEExplosionTypeItems
				whatScript.ddlEExplosionType.selection = (findItem ddlEExplosionTypeItems comboExpVarOrder[2][stateMSAP])
			------------------------------
				whatScript.ddlETriggerType.items = ddlETriggerTypeItems
				whatScript.ddlETriggerType.selection = (findItem ddlETriggerTypeItems comboExpVarOrder[3][stateMSAP])
			------------------------------
				whatScript.chkEIgnoreDamageModel.state = comboExpVarOrder[4][stateMSAP]
				whatScript.chkEIgnoreDamageModel.tristate = comboExpVarOrder[4][triMSAP]
			------------------------------
				whatScript.chkEPlayOnParent.state = comboExpVarOrder[5][stateMSAP]
				whatScript.chkEPlayOnParent.tristate = comboExpVarOrder[5][triMSAP]
			------------------------------
				whatScript.chkEOnlyonDamageModel.state = comboExpVarOrder[6][stateMSAP]
				whatScript.chkEOnlyonDamageModel.tristate = comboExpVarOrder[6][triMSAP]
			)
			else
			(
				if whatScript.name == "propParTagger" then
				(
				print "populating particle tag checkboxes in script window"
					whatScript.ddlPTriggerType.items = ddlPTriggerTypeItems
					whatScript.ddlPTriggerType.selection = (findItem ddlPTriggerTypeItems comboParVarOrder[2][stateMSAP])
				------------------------------
					whatScript.chkPAttachToAll.state = comboParVarOrder[3][stateMSAP]
					whatScript.chkPAttachToAll.tristate = comboParVarOrder[3][triMSAP]
				------------------------------
					whatScript.chkPHasTint.state = comboParVarOrder[6][stateMSAP]
					whatScript.chkPHasTint.tristate = comboParVarOrder[6][triMSAP]
				------------------------------
					whatScript.chkPIgnoreDamageModel.state = comboParVarOrder[13][stateMSAP]
					whatScript.chkPIgnoreDamageModel.tristate = comboParVarOrder[13][triMSAP]
				------------------------------
					whatScript.chkPPlayOnParent.state = comboParVarOrder[14][stateMSAP]
					whatScript.chkPPlayOnParent.tristate = comboParVarOrder[14][triMSAP]
				------------------------------
					whatScript.chkPOnlyonDamageModel.state = comboParVarOrder[15][stateMSAP]
					whatScript.chkPOnlyonDamageModel.tristate = comboParVarOrder[15][triMSAP]	
				------------------------------				
					whatScript.radPNameState.state = comboParVarOrder[16][stateMSAP] -- not using this will leave the state untouched
				------------------------------
					whatScript.edtPName.text = comboParVarOrder[1][stateMSAP]
						try
						(
							if comboParVarOrder[1][triMSAP] != true then
							(
								comboParVarOrder[1][triMSAP] = false
							)
							else ()
						) catch (print (getCurrentException()))
						whatScript.edtPName.bold = comboParVarOrder[1][triMSAP]
						
						if comboParVarOrder[1][triMSAP] == true then
						(
						whatScript.edtPName.text = "Mismatch!"
						)
						else
						(
							whatScript.edtPName.text = comboParVarOrder[1][stateMSAP]
						)				
				------------------------------
					whatScript.spnPScaleVal.range = [0,1,comboParVarOrder[4][stateMSAP]]
						try
						(
							if comboParVarOrder[4][triMSAP] != true then
							(
								comboParVarOrder[4][triMSAP] = false
							)
							else ()
						) catch (print (getCurrentException()))
						whatScript.spnPScaleVal.indeterminate = comboParVarOrder[4][triMSAP]
				------------------------------
					whatScript.spnPProbVal.range = [0,100,comboParVarOrder[5][stateMSAP]]
						try
						(
							if comboParVarOrder[5][triMSAP] != true then
							(
								comboParVarOrder[5][triMSAP] = false
							)
							else ()
						) catch (print (getCurrentException()))
						whatScript.spnPProbVal.indeterminate = comboParVarOrder[5][triMSAP]
				------------------------------
					whatScript.spnPRedVal.range = [0,255,comboParVarOrder[7][stateMSAP]]
									try
									(
										if comboParVarOrder[7][triMSAP] != true then
										(
											comboParVarOrder[7][triMSAP] = false
										)
										else ()
									) catch (print (getCurrentException()))
									whatScript.spnPRedVal.indeterminate = comboParVarOrder[7][triMSAP]
				------------------------------
					whatScript.spnPGreenVal.range = [0,255,comboParVarOrder[8][stateMSAP]]
						try
						(
							if comboParVarOrder[8][triMSAP] != true then
							(
								comboParVarOrder[8][triMSAP] = false
							)
							else ()
						) catch (print (getCurrentException()))
						whatScript.spnPGreenVal.indeterminate = comboParVarOrder[8][triMSAP]
				------------------------------
					whatScript.spnPBlueVal.range = [0,255,comboParVarOrder[9][stateMSAP]]
						try
						(
							if comboParVarOrder[9][triMSAP] != true then
							(
								comboParVarOrder[9][triMSAP] = false
							)
							else ()
						) catch (print (getCurrentException()))
						whatScript.spnPBlueVal.indeterminate = comboParVarOrder[9][triMSAP]
				------------------------------
					whatScript.spnPSizeXVal.range = [0,1,comboParVarOrder[10][stateMSAP]]
						try
						(
							if comboParVarOrder[10][triMSAP] != true then
							(
								comboParVarOrder[10][triMSAP] = false
							)
							else ()
						) catch (print (getCurrentException()))
						whatScript.spnPSizeXVal.indeterminate = comboParVarOrder[10][triMSAP]
				------------------------------
					whatScript.spnPSizeYVal.range = [0,1,comboParVarOrder[11][stateMSAP]]
						try
						(
							if comboParVarOrder[11][triMSAP] != true then
							(
								comboParVarOrder[11][triMSAP] = false
							)
							else ()
						) catch (print (getCurrentException()))
						whatScript.spnPSizeYVal.indeterminate = comboParVarOrder[11][triMSAP]
				------------------------------
					whatScript.spnPSizeZVal.range = [0,1,comboParVarOrder[12][stateMSAP]]
						try
						(
							if comboParVarOrder[12][triMSAP] != true then
							(
								comboParVarOrder[12][triMSAP] = false
							)
							else ()
						) catch (print (getCurrentException()))
						whatScript.spnPSizeZVal.indeterminate = comboParVarOrder[12][triMSAP]
				)
				else
				(
					if whatScript.name == "propAudioEmitTagger" then
					(
					print "populating audio emmiter tag checkboxes in script window"
					------------------------------
						whatScript.edtAEffect.text = comboAudioVarOrder[1][stateMSAP]
							try
							(
								if comboAudioVarOrder[1][triMSAP] != true then
								(
									comboAudioVarOrder[1][triMSAP] = false
								)
								else ()
							) catch (print (getCurrentException()))
							whatScript.edtAEffect.bold = comboAudioVarOrder[1][triMSAP]
							
							if comboAudioVarOrder[1][triMSAP] == true then
								(
								whatScript.edtAEffect.text = "Mismatch!"
								)
							else
								(
									whatScript.edtAEffect.text = comboAudioVarOrder[1][stateMSAP]
								)
					------------------------------
						whatScript.radAEffectState.state = comboAudioVarOrder[2][stateMSAP] -- not using this will leave the state untouched
					)else()
				)
			)
		)
	)
)--end popTagSettings
-------------------------------------------------------------------------------------------------------------------------
--populate particle effect list
--taken from dcc\current\max2011\scripts\pipeline\helpers\interiors\QuickDestructibleObjectSetup.ms
filein "startup/objects/util/RsRageParticleObject.ms"
fn loadParticleEntries =
(
	filename = (RsConfigGetCommonDir() + "/data/effects/entityFx.dat")
	intFile = openfile filename mode:"rb"
	newType = #none
	while eof intFile == false do (
		intLine = RsRemoveSpacesFromStartOfString(readline intFile)
		if intLine.count > 0 and intLine[1] != "#" then (
			intTokens = filterstring intLine " \t"
			if intTokens[1] == "ENTITYFX_AMBIENT_START" then (
				newType = #ambient
			) else if intTokens[1] == "ENTITYFX_COLLISION_START" then (
				newType = #collision
			) else if intTokens[1] == "ENTITYFX_SHOT_START" then (
				newType = #shot
			) else if intTokens[1] == "FRAGMENTFX_BREAK_START" then (
				newType = #break
			) else if intTokens[1] == "FRAGMENTFX_DESTROY_START" then (
				newType = #destruction
			) else if intTokens[1] == "ENTITYFX_ANIM_START" then (
				newType = #anim
			) 
			else if intTokens[1] == "ENTITYFX_SHOT_END" or  intTokens[1] == "ENTITYFX_AMBIENT_END" or  intTokens[1] == "ENTITYFX_COLLISION_END" or intTokens[1] == "FRAGMENTFX_BREAK_END" or intTokens[1] == "FRAGMENTFX_DESTROY_END" or intTokens[1] == "FRAGMENTFX_ANIM_END" then (
				newType = #none
			) else  if intTokens.count > 1 then (
				newItem = RsCollParticle intTokens[1] newType
				append CollParts newItem
				append effectNames intTokens[1]
			)
		)
	)
	close intFile
)--end loadParticleEntries
-------------------------------------------------------------------------------------------------------------------------