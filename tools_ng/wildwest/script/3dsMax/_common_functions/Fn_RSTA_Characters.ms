fn RSTA_alignPedProps =
(
	-- set the timeline to 0, that should be 'safe'
	sliderTime = 0f
	-- adding some reset xform stuff to clean up the mesh
	selectbywildcard ped_propPrefix
	
	local allProps = getCurrentSelection()	
	for resetObj = 1 to allProps.count do
	(
		resetxform allProps[resetObj]
		maxOps.CollapseNodeTo allProps[resetObj] 1 True
	)	

	-- now lets align the array contents to each other
	for MatchSel = 1 to ped_propList.count do
	(
		for PropSel = 1 to ped_propList[MatchSel].count do
		(
			try
			(
				MatchProp = ped_propList[MatchSel][PropSel] -- select the prop within the group
				MatchBone = (getNodebyName ped_propAlignAndLinkBones[MatchSel]) -- select the matching bone
				selectbywildcard matchprop
				propsToAlign = getCurrentselection()
				--propsToLink = getCurrentselection()
				-- aligns the selected props to their matching bone
				for p = 1 to propsToAlign.count do
				(
					AlignPivotto propsToAlign[p] MatchBone -- aligns the selected props to their matching bone
					-- adds a link constraint
					--propsToLink[p].controller = link_constraint()
					--propsToLink[p].controller.addTarget MatchBone 0
				)
			) catch()
		)
	)
)

fn RSTA_linkPedProps =
(
	-- set the timeline to 0, that should be 'safe'
	sliderTime = 0f
	selectbywildcard ped_propPrefix
	-- now lets align the array contents to each other
	for MatchSel = 1 to ped_propList.count do
	(
		for PropSel = 1 to ped_propList[MatchSel].count do
		(
			try
			(
				MatchProp = ped_propList[MatchSel][PropSel] -- select the prop within the group
				MatchBone = (getNodebyName ped_propAlignAndLinkBones[MatchSel]) -- select the matching bone
				selectbywildcard matchprop		
				propsToLink = getCurrentselection()
				
				for p = 1 to propsToLink.count do 
				(
					-- adds a link constraint
					propsToLink[p].controller = link_constraint()
					propsToLink[p].controller.addTarget MatchBone 0
				)
			) catch ()
		)
	)
)

fn RSTA_unlinkPedProps	=
(
	-- set the timeline to 0, that should be 'safe'
	sliderTime = 0f	
	selectbywildcard ped_propPrefix
	-- now lets align the array contents to each other
	for MatchSel = 1 to ped_propList.count do
	(
		for PropSel = 1 to ped_propList[MatchSel].count do
		(
			try
			(
				MatchProp = ped_propList[MatchSel][PropSel] -- select the prop within the group
				MatchBone = (getNodebyName ped_propAlignAndLinkBones[MatchSel]) -- select the matching bone
				selectbywildcard matchprop		
				propsToLink = getCurrentselection()
				
				for p = 1 to propsToLink.count do
				(
					--delete any existing links
					try
					(
						numTargets = propsToLink[p].controller.getNumTargets()
						for delTargets = 1 to numTargets do
						(	
							propsToLink[p].controller.deleteTarget delTargets
						)
					) catch()
				)
			) catch()
		)
	)
)



-- Populate mapping lookup arrays from a project specific XML file
fn RSTA_populatePedBoneTagMappingArrays = (
	
	-- Make sure the predefined arrays are empty
	boneTagArray =#()
	BoneNameArray=#()
	
	-- boneMapList =#() -- safe to delete?
	
	
	-- Define the XML file
	mappingXmlFile = RsConfigGetContentDir() + "animconfig/bone_tags.xml" 
	xmlDoc = XmlDocument()	
	xmlDoc.init()
	xmlDoc.load mappingXmlFile
	xmlRoot = xmlDoc.document.DocumentElement
	
	
	-- Parse the XML
	-- This could be looped I suspect
	if xmlRoot != undefined then (
		dataElems = xmlRoot.childnodes
	
		for i = 0 to (dataElems.Count - 1 ) do (
			dataElement = dataElems.itemof(i)
			
			-- Look for ped bones
			if dataelement.name == "peds" then (
				-- Look for the bones ids
				boneElems = dataelement.childnodes
				
				for b = 0 to (boneElems.Count - 1 ) do (
					boneElement = boneElems.itemof(b)
					
					-- If we've found the bone ids, populate the struct
					if boneElement.name == "boneids" then (
						
						boneTagElems = boneElement.childnodes
						
						for bt = 0 to (BoneTagElems.Count - 1 ) do (
						
							boneTagElement = boneTagElems.itemof(bt)
							
							nameattr = boneTagElement.Attributes.ItemOf("name")
							tagattr = boneTagElement.Attributes.ItemOf("id")
							
							append BoneNameArray nameattr.value
							append boneTagArray tagattr.value	
							
						)
					) -- end bone elements
				)
			)-- end ped data elements
		)		
	)-- end xmlfound
	
	print ("Mapping Arrays populated using "+mappingXmlFile+".")
)
