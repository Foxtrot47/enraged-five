--------------------------------------------------------------------
-- UI COMMON FUNCTIONS --------------------------------------------
--------------------------------------------------------------------
-- WIKI: https://devstar.rockstargames.com/wiki/index.php/Rsta_common_functions_ui
---------------------------------------------------------------------

--------------------------------------------------------------------
--- NAME: RSTA_ui_tabManager 
--- DATE: 25.10.13
--- AUTHOR: Tab Code By Neal Corbett, Struct by Matt Harrad 
--- ABOUT: Struct for managing tabs 
--------------------------------------------------------------------
struct RSTA_ui_tabManager 
(	
	---------------------------------------------------------------------------
	-- LOCALS
	---------------------------------------------------------------------------
	tabItems = #(),
	dn_Panel,
	sRollout, 
	scrollPosNames = #(),
	scrollPosList = #(),
	mRollout,
	---------------------------------------------------------------------------
	-- FUNCTIONS
	---------------------------------------------------------------------------
	-- The functions to be run on the rollout open event.
	fn setup =
	(
		this.setLastTab()
	),
	--------------------------------------------
	/*** D: Sets the last tab open from ***/
	fn setLastTab =
	(
		-- Get last-used tab-number:
		local lastTabName = RsSettingsReadString (mRollout as string) "lastTab" undefined
		local tabNames = for item in tabItems collect item.name
		local tabNum = findItem tabNames lastTabName

		if (tabNum == 0) or (tabNum > dn_Panel.tabPages.count) do 
		(
			tabNum = 1
		)
		dn_Panel.SelectedIndex = (tabNum - 1)
		this.SetPage tabNum
	),
	-- SET THE UI FOR THE TAB -------------------
	fn tabClick =
	(
		tabNum = dn_Panel.SelectedIndex + 1
		tabName = tabItems[tabNum].name
		
		RsSettingWrite (mRollout as string) "lastTab" tabName
		
		this.SetPage tabNum
	),
	-- ADD THE TAB TO THE TAB PANEL ------------
	fn addTab tabName rolloutArray = 
	(
		tabData = DataPair name:tabName rollouts:rolloutArray
		-- ADD TAB TO TAB LIST
		append tabItems tabData
		-- ADD TAB TO DOTNET UI
		dn_Panel.tabPages.add tabData.name
	),
	-- REMOVE TAB BY NAME -----------------------
	fn removeTab tabName = 
	(	
		for i=1 to tabItems.count do 
		(
			if (tabItems[i].name == tabName) do dn_Panel.tabPages.RemoveAt[i-1]
		)		
	),
	-- Reload stored scroll-position for top rollout:
	fn restoreScrollPos = 
	(
		if (sRollout.rollouts.count != 0) do 
		(
			local roll = sRollout.rollouts[1]
			local findNum = findItem scrollPosNames roll.name
			
			if (findNum != 0) do 
			(
				roll.scrollPos = scrollPosList[findNum]
			)
		)
	),
	-- Store the current scroll-position ----------------
	fn storeScrollPos = 
	(
		if (sRollout.rollouts.count != 0) do 
		(
			local roll = sRollout.rollouts[1]
			local findNum = findItem scrollPosNames roll.name
			
			if (findNum == 0) do 
			(
				append scrollPosNames roll.name
				findNum = scrollPosNames.count
			)
			
			scrollPosList[findNum] = roll.scrollPos
		)
	),
	-- Removes old rollouts and add the new ones to  the subRollout.
	fn setPage idx = 
	(
		if ( idx <= tabItems.count ) then
		(
			storeScrollPos()
			
			-- Remove existing rollouts from subrollout:
			for roll in sRollout.rollouts do
			(
				removeSubRollout sRollout roll
			)
	
			local tabItem = tabItems[idx]
			local tabRolls = #()
			
			generatedRolls = #()
			rollGenerators = #()
			
			for item in tabItem.rollouts do 
			(
				-- Generate rollouts if rollout-generator options are found:
				if (isKindOf item RsCreateObjEditRollDef) then 
				(
					local genRolls = (RsCreateObjEditRoll selection optionStruct:item)
					join tabRolls genRolls
					join generatedRolls genRolls
					append rollGenerators item
				)
				else 
				(
					append tabRolls item
				)
			)
			
			for roll in tabRolls do
			(
				if (roll != undefined) do
				(
					-- Set new sub-rollout as rolled-up, if the user has previously rolled it up:
					rolledUp = RsSettingsReadBoolean roll.name "rollup" false				
					addSubRollout sRollout roll rolledUp:rolledUp
				)	
			)
			
			restoreScrollPos()
		)
		else
		(
			MessageBox "Internal Error.  Tabs misconfigured.  Contact tools."
		)
	),
	---------------------------------
	fn extendToBottom =
	(
		sRollout.height = mRollout.height-sRollout.pos[2]
	)
)
--------------------------------------------------------------------
-- NAME: 
-- DATE: 19.11.13
-- AUTHOR: Matt Harrad 
-- ABOUT: 
--------------------------------------------------------------------
struct RSTA_ui_treeviewManager 
(
	-- LOCALS 
	treeview = undefined,
	iconFiles = #(),
	-- CLEAR THE TREEVIEW
	fn clear =
	(
		treeview.Nodes.Clear()	
	),
	-- FUNCTIONS
	fn init_icons =
	(
		ilTv = dotNetObject "System.Windows.Forms.ImageList"
		for i in iconFiles do
		(
			img = dotNetClass "System.Drawing.Image" --create an image
			ilTv.images.add (img.fromFile i)
		)	
		treeview.imageList = ilTv 
	),
	-- ADD NEW NODE
	fn addNode itemName parentNode tag:"" img:0 unique:false =
	(
		if not(unique) OR not(this.doesNodeExist itemName parentNode) then
		(
			objNode = (dotNetObject "System.Windows.Forms.TreeNode" (itemName as string))
			objNode.imageIndex = objNode.selectedImageIndex = img
			objNode.tag = tag
			if (parentNode != undefined) do parentNode.nodes.add objNode
			return objNode
		)
		else return (this.getNode itemName parentNode)
		
	),
	-- GET NODE
	fn getNode itemName parentNode =
	(
		itemList = parentNode.nodes
		for i=0 to itemList.count-1 do
		(
			item = itemList.item[i]
			if item.text == itemName do return item
		)
		return undefined
	),
	-- DOES NODE EXIST
	fn doesNodeExist itemName parentNode =
	(
		itemList = parentNode.nodes
		for i=0 to itemList.count-1 do
		(
			item = itemList.item[i]
			if item.text == itemName do return true
		)
		return false
	)
)
--------------------------------------------------------------------
-- NAME: 
-- DATE:
-- AUTHOR: 
-- ABOUT: 
--------------------------------------------------------------------
fn RSTA_DialogPosLoad uiPosFile =
(
	/**
	function for parsing a uiPosFile
	which can then be used to set the dialogs position
	**/
	
	input_name = uiPosFile
	
	UIDialogPos = undefined
	if (doesFileExist input_name) == true then
	(
		f = openfile input_name
		inputData = #() -- define as array

		while not eof f do
		(
			append inputData (filterstring (readLine f) ",")
		)
		close f

		UIDialogPos = [(inputData[1][1] as float),(inputData[1][2] as float)]
		
-- 		format ("using custom "+(mrSkeletonUIDialogPos  as string)+"\n")
	)
	else
	(
-- 		format ("Using default pos"+"\n")
		UIDialogPos = [1250, 100] --set to default position
	)
	return UIDialogPos
)

fn RSTA_DialogPosRec dialogName uiPosFile = 
(
	/**
	fn to save the position of the dialog
	(dialogName) to a file (uiPosFile)
	this can then be used to create the 
	dialog at that same position in future
	**/
	
	local output_name = uiPosFile
	
	if dialogName != undefined do 
	(		
		local UIDialogPos = getDialogPos dialogName
		
		if output_name != undefined then 
		(
			output_file = createfile output_name		

			local fX = (UIDialogPos[1] as string)
			local fY = (UIDialogPos[2] as string)
			
			format (fX+","+fY) to:output_file
			
			--print ("mrSkeletonUIDialogPos = "+(mrSkeletonUIDialogPos  as string))
			
			close output_file
		)
	)
)
--------------------------------------------------------------------
-- NAME: 
-- DATE:
-- AUTHOR: 
-- ABOUT: 
--------------------------------------------------------------------
rollout progBar "Progress..." 
(
	/**
	--rollout for progress bar
	**/
	progressbar prog pos:[10,10] color:red
)
--------------------------------------------------------------------
-- NAME: 
-- DATE:
-- AUTHOR: 
-- ABOUT: 
--------------------------------------------------------------------
fn initProgBar = 
(
	/**
	--fn to call initialision of a progress bar used throughout rigging tools
	**/
	if ((progBar != undefined) and (progBar.isDisplayed)) do
	(destroyDialog progBar)
	CreateDialog progBar width:300 Height:30
)


fn RSTA_multiFileSelector fileFilter caption initialDir default:1 =
(
	--modified version of RSgetOpenFilenames by neal corbett
	
	local openDialog = DotNetObject "System.Windows.Forms.OpenFileDialog"
	openDialog.multiSelect = true
	openDialog.title = caption
	 
-- 	local filePath = getFilenamePath filename
	if doesFileExist initialDir then
	(
		openDialog.initialDirectory = initialDir
	)
	else
	(
		openDialog.initialDirectory = maxFilePath
	)
	 
	-- MAXScript getOpenFilename uses trailing |; DotNet's OpenFileDialog filter does not.
-- 	if (types[types.count] == "|") do (types = (substring types 1 (types.count - 1)))

	openDialog.filter = fileFilter	
-- 	openDialog.filterIndex = default
	 
	local result = openDialog.ShowDialog()
	if (result.Equals result.OK) then openDialog.filenames else undefined
)
