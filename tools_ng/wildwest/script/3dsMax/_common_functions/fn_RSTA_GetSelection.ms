-- Returns a detailed description of the current geometry/subobject-selection:
struct RSTA_SelectionDetailsDef (obj, objSelType, selSubObjs)
fn RSTAGeometry_GetSelectionDetails objs: allowSoftSelect:False facesOnly:False selPanel: selType: = 
(
	local objsSupplied = (objs != Unsupplied)
	
	if (not objsSupplied) do 
	(
		objs = GetCurrentSelection()
	)
	
	-- Convert ObjectSets to Array:
	objs = (objs as Array)
	
	-- Collect valid selected objects:
	objs = for obj in objs where (IsKindOf obj GeometryClass) collect obj
	if (objs.count == 0) do return #()
	
	local subObjLev = SubObjectLevel
	if (selPanel == Unsupplied) do 
	(
		selPanel = ModPanel.GetCurrentObject()
	)
	
	-- Get current selection-panel state:
	local selClass = (ClassOf selPanel)
	
	-- Work out what the currently-set subobject-type is:
	local hasValidSelectionLevel = case selClass of
	(
		Editable_Poly:True
		Editable_Mesh:True
		Edit_Poly:True
		Edit_Mesh:True
		Default:False
	)
	if (numSubObjectLevels != 0) and hasValidSelectionLevel and (selPanel != Undefined) do 
	(
		subObjLev = GetSelectionLevel selPanel
	)
	
	-- Get subobject selection-type:
	--	(if SelType is supplied, selection will be converted to that subobject-type)
	if (selType == Unsupplied) do 
	(
		if objsSupplied then 
		(
			selType = #Object
		)
		else 
		(
			selType = RsGetSubObjLevelName selClass:selClass
		)
	)
	
	case of 
	(
		-- Only apply to faces:
		(facesOnly and (selType != #object)):
		(
			selType = #face
		)
		-- Convert edge-selection to vert-selection:
		(selType == #edge):
		(
			selType = #vertex
		)
	)
	
	-- Is soft-selection active?
	local hasSoftSel = case of 
	(
		(objsSupplied):False
		(selClass == undefined):(False)
		(IsKindOf selClass Editable_Mesh):(MeshOp.GetSoftSel selPanel)
		(IsKindOf selClass Editable_Poly):(selPanel.useSoftSelection)
		(IsKindOf selClass Poly_Select):(selPanel.useSoftSelection)
		(IsKindOf selClass Mesh_Select):(selPanel.useSoftSelection)
		(IsKindOf selClass Vol__Select):(selPanel.useAffectRegion)
		Default:(False)
	)
	
	-- Collect all subobject-selections:
	--	(doing these in one chunk so we can suspend editing for edit-loop)
	local selItems = #()
	for obj in objs do 
	(
		local SelItem = RSTA_SelectionDetailsDef obj:obj objSelType:selType
		local objOp = RsMeshPolyOp obj
		
		-- Get object's subobject-selection:
		if not (allowSoftSelect and hasSoftSel) then 
		(
			case SelType of 
			(
				#vertex:
				(
					-- Get vertex-selection:
					selItem.selSubObjs = RsGetSelVertNums obj selClass:selClass
				)
				#object:
				(
					-- This will allow faces to be counted for non-editable geometry-classes (e.g. Sphere)
					local objMesh = obj
					if (objOp == Undefined) do 
					(
						objOp = MeshOp
						objMesh = obj.mesh
					)
					
					local faceCount = objOp.GetNumFaces objMesh
					
					-- Select all faces:
					selItem.selSubObjs = if (faceCount == 0) then #{} else #{1..faceCount}
					--selItem.objSelType = #face
				)
				default:
				(
					-- Collect tri-to-poly lookup-list for 'Select Poly'
					--	(this removes any dead faces, so should be done before getting tri-selection)
					local triToPolyList = Undefined
					if (selClass == Poly_Select) do 
					(
						triToPolyList = RsMakeTriToPolyList obj
					)
					
					selItem.selSubObjs = case of 
					(
						(selClass == Editable_poly):(polyOp.getFaceSelection selPanel)
						((selClass == Edit_poly) or (selClass == PolyMeshObject)):(selPanel.GetSelection #face node:obj)
						default:(RsGetSelFaceNums obj selClass:selClass) -- Mesh face-selection
					)
					
					-- Convert triangles to polys:
					if (triToPolyList != Undefined) do 
					(
						local triToPolyList = RsMakeTriToPolyList obj
						local selTris = RsGetSelFaceNums obj
						
						local selFaces = #{}
						selFaces.count = objOp.GetNumFaces obj
						
						for triNum in selItem.selSubObjs do 
						(
							local faceNum = triToPolyList[triNum]
							selFaces[faceNum] = True
						)
						
						selItem.selSubObjs = selFaces
					)
					
					SelItem.ObjSelType = #face
				)
			)
		)
		else 
		(
			-- If object is using Soft Selection, get array of PointSelections:
			selItem.objSelType = #SoftSelect
			
			-- Hidden verts will not be touched, even if Soft Select covers them:
			local baseObjOp = RsMeshPolyOp obj.baseObject
			local hiddenVerts = baseObjOp.GetHiddenVerts obj.baseObject
			
			local vertCount = baseObjOp.GetNumVerts obj
			selItem.selSubObjs = for vertNum = 1 to vertCount collect 
			(
				if hiddenVerts[vertNum] then 0.0 else (PointSelection obj vertNum)
			)
		)
		
		-- Collect selection-data:
		Append selItems selItem
	)
	
	return selItems
)

--print (RSTAGeometry_GetSelectionDetails())
--print (RSTAGeometry_GetSelectionDetails objs:Objects allowSoftSelect:True)