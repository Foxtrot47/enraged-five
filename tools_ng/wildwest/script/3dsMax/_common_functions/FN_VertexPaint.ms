-- Functions fpr vertex bake painting
-- Rick Stirling 2008-2011

---------------------------------------------------------------------------------------------------------------------------------------
-- isCLOTHSIM_Selection()
-- 
-- Do we have a CLOTHSIM_ prefix name in our selection
---------------------------------------------------------------------------------------------------------------------------------------
fn isCLOTHSIM_Selection =
(
	local retVal = false
	for item in selection where (matchpattern item.name pattern:"CLOTHSIM_*") do
	(
		messagebox "CLOTHSIM_ mesh selected, exiting!" title:"Error - Bad Selection"
		retVal = true
	)
	
	retVal
)

---------------------------------------------------------------------------------------------------------------------------------------
-- getCommandHWND()
-- Paul Truss, Rockstar London
-- Used to figure out if the command panel is docked or not and to give us access to the Assign Vertex colours
---------------------------------------------------------------------------------------------------------------------------------------
fn getCommandHWND =
(
	local result
	
	for c in windows.getChildrenHWND #max do
	(
		if c[4] == "ModifyTask" then
		(
			print "ModifyTask"
			result = c[1]
		)
	)
	
	if result == undefined then
	(
		local dialogHWND = UIAccessor.GetPopupDialogs()
		
		for HWND in dialogHWND do
		(
			if (UIAccessor.GetWindowText HWND) == "Command Panel" then
			(	
				print  "Command Panel" 
				result = HWND
			)
		)
	)
	
	print result
	result
)
                


---------------------------------------------------------------------------------------------------------------------------------------
-- pressVPAssign()
-- By Paul Truss, Rockstar London
-- Presses the Assign button in the Vertex colour rollout
-- Better than the max applynodes button.
---------------------------------------------------------------------------------------------------------------------------------------
fn pressVPAssign =
(
	cui.commandPanelOpen = true
	setCommandPanelTaskMode #modify
	
	local assignRollHWND = windows.getChildHWND #max "Assign Vertex Colors"
	
	if assignRollHWND != undefined then 
	( 
		local assignHWND = windows.getChildHWND (UIAccessor.GetParentWindow assignRollHWND[1]) "Assign"
		
		if assignHWND != undefined then
		(
			UIAccessor.PressButton assignHWND[1]
		)
	)
	else
	(
		local dialogHWND = UIAccessor.GetPopupDialogs()
		local commandHWND
		
		for HWND in dialogHWND do
		(
			if (UIAccessor.GetWindowText HWND) == "Command Panel" then
			(
				commandHWND = HWND
			)
		)
		
		if commandHWND != undefined then
		(
			local children = UIAccessor.GetChildWindows commandHWND
			local assignHWND
			
			for HWND in children do 
			(
				local name = (UIAccessor.GetWindowText HWND)
				
				if name == "Assign" then
				(
					assignHWND = HWND
				)
			)
			
			if assignHWND != undefined then
			(
				print "pressed assign!"
			   UIAccessor.PressButton assignHWND
			)
		)
	)
)



-- ****************************************************************************************
-- Function to build the channels
-- takes an object, channel number and name to call it in the stack
fn AddVpaintChannel obj channelnumber channelname VPlightingModel VPradiosityOption VPcolorby  =
(
	
	addModifier obj (vertexpaint()) --before:2
	obj.modifiers[#VertexPaint].mapchannel = channelnumber
	obj.modifiers[#VertexPaint].lightingModel = VPlightingModel
	obj.modifiers[#VertexPaint].radiosityOption = VPradiosityOption
	obj.modifiers[#VertexPaint].colorBy  = VPcolorby
		
	-- Set the name last!	
	obj.modifiers[#VertexPaint].name = channelname	
		
		
	sleep 0.1
		
) --end AddVpaintChannel




-- ****************************************************************************************
fn populateRadiosity arttype contrastType= (
	radArtType = artType	
	
	-- Use the incoming arttype, but allow overrides based on the contrast
	-- Not all bakes will use a bounceplane
	case contrasttype of
	(
		1: (
				radArtType = artType 	
				try ($SkylightBouncePlane.wirecolor = bouncePlaneColourStandard) catch ()
			)

		2: (
				radArtType = "Map_EC_"
				try ($SkylightBouncePlane.wirecolor =bouncePlaneColourGray) catch ()
			)	
			
		3: (
				radArtType = "Map_MM_"
				try ($SkylightBouncePlane.wirecolor = bouncePlaneColourDark) catch ()
			)	
	)		

	
	-- Some radiosity settings here
	-- Construct the varaible name from arttype and the lighting setting
	sceneRadiosity.radiosity.radInitialQuality = readvalue ((radArtType + "radiosityQuality") as stringstream)
	sceneRadiosity.radiosity.radFiltering = readvalue ((radartType + "radiosityFiltering") as stringstream)
	sceneRadiosity.radiosity.radDirectFiltering = readvalue ((radartType + "RadiosityDirectFiltering") as stringstream)
	
	sceneRadiosity.radiosity.radGlobalRefineSteps = lightbounces
	
	-- turn off the viewport preview, that is confusing!
	sceneRadiosity.radiosity.radDisplayInViewport = off
	
	-- We want logarithmic exposure, so create a log exposure control and apply it
	log_exposure = Logarithmic_Exposure_Control()
	SceneExposureControl.exposureControl = log_exposure
	log_exposure.exteriorDaylight = readvalue ((radArtType + "ExteriorDaylight") as stringstream)	

	-- set the log exposure values from the data file
	log_exposure.brightness = readvalue ((radArtType + "Log_exposureBrightness") as stringstream)
	log_exposure.contrast = readvalue ((radArtType + "Log_exposureContrast") as stringstream)
	log_exposure.midtones = readvalue ((radArtType + "Log_exposureMidtones") as stringstream)	
	
) --end populateRadiosity






-- ****************************************************************************************
-- Create the skylight and set up all the values
-- The skylight rig gets parented to a dummy object so that it can be moved around the scene
fn makeSkyLight artType contrastType =
(

	-- delete existing lighting details
	try (delete $SkyBake01*) catch()
	try (delete $SkylightBouncePlane) catch()
	try (delete $lightingRigDummy) catch()
	
	-- A dummy to parent the rig to
	buildDummyHelper "lightingRigDummy" [0,0,0] 1

	theskylight = IES_Sky transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0,0,30]) isSelected:on target:(Targetobject transform:(matrix3 [1,0,0] [0,1,0] [0,0,1] [0,0,0]))
	theskylight.multiplier = skylightmultiplier
	theskylight.name = "SkyBake01"
	theskylight.parent = $lightingRigDummy
	theskylight.target.name = (theskylight.name + "Target")
	theskylight.target.parent = $lightingRigDummy
	theskylight.color = (color 242.25 242.25 255)
	

	
	-- Build a bounce plane
	Plane length:20 width:20 pos:[0,0,-2] isSelected:on wirecolor: bouncePlaneColourStandard
	$.name = "SkylightBouncePlane"
	$.parent = $lightingRigDummy
	
	
	-- We need to turn on radiosity
	sceneRadiosity.radiosity = radiosity()
	
	-- Populate all the lighting fields
	populateRadiosity arttype contrastType
		
	try (hide $coll*)catch()
	try (hide $cons*)catch()	
	hideByCategory.helpers = true
		
	clearSelection()
) --end makeSkyLight





-- ****************************************************************************************
-- Calculate the radiosity.
fn CalcRad artType contrastType =
(
	try (currentselection = $) catch()
	-- make sure the light exists if required
	
	if arttype != "Int_" then (
		
		try (select $SkyBake01)
		catch (
			messagebox "Could not find the Skylight"
			return 0
		)
	)
	
	clearSelection()
	
	-- Populate all the lighting fields
	
	populateRadiosity arttype contrastType
	
	-- Run the light simulation (after resetting)
	sceneRadiosity.radiosity.Reset true false
	sceneRadiosity.radiosity.Start()
	
	sleep 2 -- need to wait until this is complete
	
	try (select currentselection) catch()
) --end CalcRad





-- ****************************************************************************************
-- Clean up lights
-- Remove the radiosity settings
fn cleanuplights = (
	sceneRadiosity.radiosity = undefined
	SceneExposureControl.exposureControl = undefined
	
	clearSelection()
	try ( delete $lightingRigDummy.children ) catch ()
	try ( delete $lightingRigDummy ) catch ()
) --end cleanuplights





-- ****************************************************************************************
-- A function here will prebake all selected model parts to a default gray
-- It simply sets a value to vertex
-- The pbar is a progress bar so that uses know the script hasn't crashed
fn prebakeSelected arttype cpvChannelList pbar:undefined =
(
	setCommandPanelTaskMode #modify
	displayColor.shaded = #object
	
	-- Try to hide ragdoll information
	-- Might need to add more stuff here for other art types
	try (hide $constraints*)catch()
	try (hide $collision*)catch()
	
	-- Make sure we have objects selected
	-- Warn the user if nothing is selected
	if $ == undefined  then (messagebox "Nothing selected to prebake")
	
	-- Something is selected
	else ( 
		-- Create an array of all selected objects
		objSelectedArray = getCurrentSelection()

		-- Loop through the selected objects
		for i = 1 to objSelectedArray.count do (
			obj = objSelectedArray[i]

			-- This is the onscreen progress bar
			if ( undefined != pbar ) then pbar.value = 0		

			-- Make sure the mesh is not excluded from the GI calculations
			obj.isGIExcluded = false
			
			-- Turn on the Vertex colours, set to channel 0
			obj.showVertexColors = on
			obj.vertexColorType = 0	

			modPanel.setCurrentObject obj.baseObject
			subobjectLevel = 1 

			nv = obj.numverts

			for i = 1 to nv do (
						
				for ch = 1 to cpvChannelList.count do (
					currentChannel = cpvChannelList[ch][1]
					currentValue = cpvChannelList[ch][2]
					currentPreserve = cpvChannelList[ch][3]
					if classof obj == Editable_Poly then (if currentPreserve == false then (try (polyop.setvertcolor obj currentChannel i currentValue) catch ()))
					if classof obj == Editable_Mesh then (if currentPreserve == false then (try (meshop.setvertcolor obj currentChannel i currentValue) catch ()))
	
					-- Update the progress bar	
					if ( undefined != pbar ) then (
						pbar.value = ( 100.0 * i ) / nv
						pbar.color = color 0 (50 +(pbar.value * 2) ) 0
					)
				)	
			) -- end vert count loop


		subobjectLevel = 0
		) -- object selection loop
	)
	pbar.value = 0	
) --end prebakeSelected





-- ****************************************************************************************
-- The light baking section
-- It takes a channel (0=vertex colour, -1 = illumination etc). Other channels can be used later for other effects
-- It also takes a name to label the bake on the stack.
fn bakesel bakechannel stackname arttype contrasttype edgeType =
(
	
	displayColor.shaded = #object
	
	-- Make sure we have objects selected
	if $ == undefined  then (messagebox "Nothing selected to bake")
	
	-- Build array of all objects to be baked	
	else
	(
		objSelectedArray = getCurrentSelection()
		objCount = objSelectedArray.count
		
		-- prepare the materials in case they are Rage shaders
		ButtonPrepareMat false

		-- Run the lighting sim
		-- Will this be slow?
		-- calcRad artType contrastType
		
		for i = 1 to objCount  do (
			obj = objSelectedArray[i]

			try (
				select $SkyBake01
				objectposition = obj.pos
				$lightingRigDummy.pos = objectposition
				$lightingRigDummy.pos.z = 0	
			)
			catch (
				messagebox "Could not find the Skylight"
				return 0
			)
		
			
			-- Hide everything
			clearSelection()
			actionMan.executeAction 0 "281"  -- Tools: Hide Unselected

			-- Unhide the object we want to bake, and the bounceplane
			unhide obj
			try (unhide $SkylightBouncePlane) catch ()

			-- RAGE materials can come out black in the bake, so swap them out
			--storedMaterial = obj.material
			--obj.material = standardmaterial()
			--select obj

			
			obj.showvertexcolors = true
			obj.vertexColorsShaded = false
			
			-- Don't need ragdoll stuff in the scene if using peds
			try (hide $coll*)catch()
			try (hide $cons*)catch()
		

			-- Add the vertex paint channel to the object
			-- Set some details in the vertex paint modifier
			VPlightingModel = readvalue ((ArtType + "lightingModel") as stringstream)
			VPradiosityOption = readvalue ((ArtType + "radiosityOption") as stringstream)
		
			VPcolorby = edgetype
			
			AddVpaintChannel obj bakechannel stackname VPlightingModel VPradiosityOption VPcolorby 
			
			
			sleep 1

			-- This is just to refreshes the stack to make it easier to see what'd going on.
			clearselection()
			select obj
			
			-- Run the lighting sim
			-- Will this be slow?
			calcRad artType contrastType
			
			-- This is where we assign the colours
			-- AssignVertexColors.ApplyNodes obj vertexPaint:obj.modifiers[stackname]  -- Uses the maxscript command
			-- Try to force the Radiosity type since it doesn't always work for some bastard reason
			obj.modifiers[stackname].radiosityOption = 2
			
			-- This works better, it presses the assign vertex colours button
			-- Many thanks to Paul Truss for this workaround!
			
			pressVPAssign()
	
			
			-- Darken the Illumination channel for peds
			if (artType == "Ped_" and bakechannel == TheRimChannel) then
			(
				--obj.modifiers[#VBake_Rim_Illum].layerOpacity = 100
				--obj.modifiers[#VBake_Rim_Illum].layerMode = "Color Burn"
				obj.modifiers[stackname].layerOpacity = 100
				obj.modifiers[stackname].layerMode = "Color Burn"
			)

			-- Map props must always have identical AO and Rim
			if ((artType == "Map_") and (bakechannel == TheRimChannel or bakechannel == TheColourChannel))	then
			(
				CalcRad artType contrastType
				
				AddVpaintChannel obj TheRimChannel "Illum copy of AO" VPlightingModel VPradiosityOption VPcolorby 
				--AssignVertexColors.ApplyNodes obj vertexPaint:obj.modifiers[#Illum_copy_of_Rim] 
				pressVPAssign()
			)

			-- Need to refresh the stack
			clearSelection()
			select obj

			
			-- **********************************
			-- Reassign the correct material
			-- VERY IMPORTANT!!!!
			-- obj.material = storedMaterial
			
			
			-- **********************************
			
			unhide objSelectedArray 
			
		) -- object selection loop
		
		-- Use version written by our tools guys.
		RestoreMaterials()
	)
	
	objSelectedArray -- return the initial selection
) --end bakesel


















-- ****************************************************************************************
-- The light baking section
-- It takes a channel (0=vertex colour, -1 = illumination etc). Other channels can be used later for other effects
-- It also takes a name to label the bake on the stack.
fn bakeselInt bakechannel stackname arttype contrasttype edgeType =
(
	print "bakechannel stackname arttype contrasttype edgeType"
	
	print bakechannel 	
	print stackname 	
	print arttype 	
	print contrasttype 	
	print edgeType
	
	
	displayColor.shaded = #object
	
	-- Make sure we have objects selected
	if $ == undefined  then (messagebox "Nothing selected to bake")
	
	-- Build array of all objects to be baked	
	else
	(
		objSelectedArray = getCurrentSelection()
		objCount = objSelectedArray.count
		
		-- prepare the materials in case they are Rage shaders
		ButtonPrepareMat false

		-- Run the lighting sim
		-- Will this be slow?
		-- calcRad artType contrastType
		

		
		-- Don't need ragdoll stuff in the scene if using peds
		try (hide $coll*)catch()
		try (hide $cons*)catch()
	

		-- Add the vertex paint channel to the object
		-- Set some details in the vertex paint modifier
		VPlightingModel = readvalue ((ArtType + "lightingModel") as stringstream)
		VPradiosityOption = readvalue ((ArtType + "radiosityOption") as stringstream)
		
		-- Override for interior artificial bounce lighting
		if (arttype == "Int_" and bakechannel == -1) then (
			VPradiosityOption = 3
		)
		
		VPcolorby = edgetype
		
		for i = 1 to objCount  do (
			obj = objSelectedArray[i]
			obj.showvertexcolors = true
			obj.vertexColorsShaded = false
			AddVpaintChannel obj bakechannel stackname VPlightingModel VPradiosityOption VPcolorby 
		)	
		

		sleep 1

		-- This is just to refreshes the stack to make it easier to see what'd going on.
		clearselection()
		select objSelectedArray
		
		-- Run the lighting sim
		-- Will this be slow?
		calcRad artType contrastType
		
		-- Override for interior artificial bounce lighting
		if (arttype == "Int_" and bakechannel == -1) then (
			VPradiosityOption = 3
		)
		
		-- RESET THE  OVERRIDE HERE!
		
		-- This is where we assign the colours
		-- AssignVertexColors.ApplyNodes obj vertexPaint:obj.modifiers[stackname]  -- Uses the maxscript command
		-- Try to force the Radiosity type since it doesn't always work for some bastard reason
		-- obj.modifiers[stackname].radiosityOption = 2
		
		-- This works better, it presses the assign vertex colours button
		-- Many thanks to Paul Truss for this workaround!
		
		pressVPAssign()

		
		-- Darken the Illumination channel for peds
		if (artType == "Ped_" and bakechannel == TheRimChannel) then
		(
			--obj.modifiers[#VBake_Rim_Illum].layerOpacity = 100
			--obj.modifiers[#VBake_Rim_Illum].layerMode = "Color Burn"
			obj.modifiers[stackname].layerOpacity = 100
			obj.modifiers[stackname].layerMode = "Color Burn"
		)

		-- Map props must always have identical AO and Rim
		if ((artType == "Map_") and (bakechannel == TheRimChannel or bakechannel == TheColourChannel))	then
		(
			CalcRad artType contrastType
			
			AddVpaintChannel obj TheRimChannel "Illum copy of AO" VPlightingModel VPradiosityOption VPcolorby 
			--AssignVertexColors.ApplyNodes obj vertexPaint:obj.modifiers[#Illum_copy_of_Rim] 
			pressVPAssign()
		)

		-- Need to refresh the stack
		clearSelection()
		select obj

		
		-- **********************************
		-- Reassign the correct material
		-- VERY IMPORTANT!!!!
		-- obj.material = storedMaterial
		
		
		
		
		-- **********************************
		
		unhide objSelectedArray 
		

		
		-- Use version written by our tools guys.
		RestoreMaterials()
	)
	
	objSelectedArray -- return the initial selection
) --end bakesel









-- ****************************************************************************************
fn CreateWindModifiers =
(
	if $ == undefined  then
	(
		messagebox "Nothing selected"
	)
	else
	(
		-- Don't make an array with a single object, that is just silly!
		objSelectedArray = getCurrentSelection()
		objCount = objSelectedArray.count
		clearselection()

		for i = 1 to objCount do (
			obj = objSelectedArray[i]
			if obj.modifiers["Wind Condensed"] == undefined then (AddVpaintChannel obj TheWindChannel "Wind Condensed" 0 0 0)
 			if obj.modifiers["Wind Horizontal"] == undefined then (AddVpaintChannel obj TheWindChannelR "Wind Horizontal" 0 0 0)
			if obj.modifiers["Wind Rate"] == undefined then (AddVpaintChannel obj TheWindChannelG "Wind Rate" 0 0 0)
			if obj.modifiers["Wind Vertical"] == undefined then (AddVpaintChannel obj TheWindChannelB "Wind Vertical" 0 0 0)
		)
		select objSelectedArray[1]
	)
) --end CreateWindModifiers


-- ****************************************************************************************
fn CondenseWindChannels =
(
	if $ == undefined  then (messagebox "Nothing selected")
	
	else
	(
		objSelectedArray = getCurrentSelection()
		objCount = objSelectedArray.count

		clearselection()		

		for i = 1 to objCount do
		(
			obj = objSelectedArray[i]

			if obj.modifiers["Wind Condensed"] == undefined then (AddVpaintChannel obj 11 "Wind Condensed" 0 0 0)
			
 			if obj.modifiers["Wind Horizontal"] == undefined or classof obj.modifiers["Wind Horizontal"] != vertexPaint Then
 				(messagebox ("Unable to find the Wind Horizontal channel for the object : " + (obj.name as string)) title:"Horizontal Channel!")

 			else
 			(
 				if obj.modifiers["Wind Vertical"] == undefined or classof obj.modifiers["Wind Vertical"] != vertexPaint Then
					(messagebox ("Unable to find the Wind Vertical channel for the object : " + (obj.name as string)) title:"Vertical Channel!")
 				else
 				(
 					if obj.modifiers["Wind Rate"] == undefined or classof obj.modifiers["Wind Rate"] != vertexPaint Then
						(messagebox ("Unable to find the Wind Rate channel for the object : " + (obj.name as string)) title:"Wind Rate Channel!")

 					else
 					(
						collapseToClasses = #(VertexPaint)
						for i = obj.modifiers.count to 1 by -1 where findItem collapseToClasses (classof obj.modifiers[i]) != 0 do
						(
						maxOps.CollapseNodeTo obj i off
						)

						modPanel.setCurrentObject obj.baseObject

						-- get number of verts in the object  
						if classof obj == Editable_Poly then
						(
							nv = polyop.getnummapverts obj 20
							polyop.setNumMapVerts obj TheWindChannel nv

							for v = 1 to nv do
							(	
								-- Set the wind to black
								polyop.setmapvert obj TheWindChannel v (color 0 0 0)
							)

							for v = 1 to nv do
							(	
								r = (polyop.getmapvert $ 20 v).x 
								g = (polyop.getmapvert $ 22 v).y
								b = (polyop.getmapvert $ 21 v).z 
								
								CondensedColour = color r g b

								polyop.setmapvert obj TheWindChannel v CondensedColour 
							)	-- End of loop setting the correct condensed colour
						) -- end of epoly object

						if classof obj == Editable_Mesh then
						(
							nv = meshop.getnummapverts obj 20
							meshop.setNumMapVerts obj TheWindChannel nv

							for v = 1 to nv do
							(	
								-- Set the wind to black
								meshop.setmapvert obj TheWindChannel v (color 0 0 0)
							)

							for v = 1 to nv  do
							(	
								r = (meshop.getmapvert $ 20 v).x 
								g = (meshop.getmapvert $ 22 v).y
								b = (meshop.getmapvert $ 21 v).z 

								CondensedColour = color r g b

								meshop.setmapvert obj TheWindChannel v CondensedColour 
							)	-- End of loop setting the correct condensed colour
						)
					)
				) -- end has a rate
			) -- end has a vertical

			AddVpaintChannel obj 11 "Wind Condensed" 0 0 0
			clearSelection()
			select obj

		) -- end has horizontal
	) -- end valid object selection
) -- end CondenseWindChannels

--////////////////////////////////////////////////////////////////////////////////
-- Project vertex map vert data form one mesh to another
-- using closest face
-- Defaults to vertex colour but any channel can be used
--////////////////////////////////////////////////////////////////////////////////
-- filein (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_debug.ms")
fn RsProjectVertexMap source target SRCchannel:0 TGTchannel:0 subChannels:"all" rayDir: sampleBomb:false  pushVal:0.0 samplingFn: preserveMods:true showMessages:true = 
(
-- 	local __debugVis =  rsta_debug_visual()
	
	--local timerStart = timeStamp()
	local targetMods = #()
	
	--was an editable poly?
	local targetWasPoly = classof target.baseobject == Editable_Poly
	
	--does the target have any modifiers?
	if (preserveMods and (target.modifiers.count > 0)) then
	(
		local q = RsQueryBoxMultiBtn "Target mesh has modifiers" title:"What about the modifiers?" labels:#("Preserve", "Collapse", "Cancel") timeout:20
		case q of
		(
			1:		--preserve
			(
				targetMods = for tmod in target.modifiers collect tmod
			)
			2: collapseStack target		--collapse
			default: return ok
		)
	)
	else if not preserveMods then	--just collapse the mesh
	(
		collapseStack target
	)
	
	local sourceMesh = snapshotAsMesh source
	local targetMesh = convertToMesh target --needs to be editable mesh
	
	--return if mesh has no faces
	if (targetMesh.numFaces == 0 ) do return false
	
	--get a list of face normals from the target object
	local targetFaceNormals = for face=1 to targetMesh.numFaces collect (normalize(getFaceNormal targetMesh face))
		
	
	--check map support on SRCchannel
	if meshop.getMapSupport sourceMesh SRCchannel != true then
	(
		if( showMessages ) then
		(
			messageBox (source.name + " does not have the specified map channel" + (SRCchannel as String) +" available!") title:"Missing Map Channel"
		)
		return false
	)
	
	--make sure we have some faces otherwise the script will hang
	if( getNumFaces sourceMesh == 0 or getNumFaces targetMesh == 0 ) then return false
			
	--check map support, enable if currently unsupported
	if meshop.getMapSupport targetMesh TGTchannel == false then
	(
		print "missing map channel"
		local numChannels = meshop.getNumMaps targetMesh 
		if numChannels < (TGTchannel + 1) then meshop.setNumMaps targetMesh (TGTchannel + 1)
		meshop.setMapSupport targetMesh TGTchannel true
	)
	
	--setup flags for colour channels to project
	local subChannelMask = [0,0,0]
	local subChannelInvMask = [1,1,1]
	if subChannels == "all" then
	(
		subChannelMask = [1,1,1]
		subChannelInvMask = [0,0,0]
	)
	else
	(
		if (findString subChannels "r" != undefined) do
		(
			subChannelMask.x = 1
			subChannelInvMask.x = 0
		)
		if (findString subChannels "g" != undefined) do
		(
			subChannelMask.y = 1
			subChannelInvMask.y = 0
		)
		if (findString subChannels "b" != undefined) do
		(
			subChannelMask.z = 1
			subChannelInvMask.z = 0
		)
	)
	
	--setup RayMeshGridIntersect
	local rayMesh = RayMeshGridIntersect()
	rayMesh.Initialize 10
	rayMesh.addNode source
	raymesh.BuildGrid()
	
	--samples is the verts to sample from
	local samples = #()
	
	--function lookups
	local meshopGetVert = meshop.getVert
	local meshopGetFacesUsingVert = meshop.getFacesUsingVert
	local meshopGetFaceCenter = meshop.getFaceCenter
	local meshopGetFaceArea = meshop.getFaceArea
	local meshopGetMapFace = meshop.getMapFace
	local meshopGetMapVert = meshop.getMapVert
	local meshopGetFacesUsingVert = meshop.getFacesUsingVert
	local meshopSetMapVert = meshop.setMapVert

	--raycast from target vert normal into the the source
	progressStart "Projecting..."
	local numVerts = getNumVerts targetMesh
	for vtx=1 to numVerts while not (getProgressCancel()) do
	(
		
		--the Position
		local position = in coordsys #world (meshopGetVert targetMesh vtx node:target)
		local samples = #(position)
		local sampleColours = #()
		
		--sampleBomb?
		--take a sample poistion inside each face for each face this vert belongs to
		if sampleBomb then
		(
			local vFaces = (meshopGetFacesUsingVert targetMesh vtx) as array
			for f in vFaces do --find some point in each face to be a sample point
			(
				local fCentre = meshopGetFaceCenter targetMesh f
				local ptToCentreVec = normalize(fCentre - position)
				
				--local samplePoint = position + ptToCentreVec * (sqrt(meshopGetFaceArea targetMesh f) * (2.0 * (distance position fCentre)))
				local samplePoint = position + ptToCentreVec * (distance position fCentre)
				--point pos:samplePoint
				
				appendifunique samples samplePoint
			)
		)
		
		--get the normal
		local normal = [0,0,1]
		if (rayDir == unsupplied) then
		(
			--get the average face normal
			local vtxFaces = (meshop.getfacesusingvert targetMesh vtx) as Array
			local normal = [0,0,0]
			for idx in vtxFaces do normal += targetFaceNormals[idx]
			normal = normalize(normal / vtxFaces.count)
		)
		
		for position in samples do
		(
			position += (normal * pushVal)
			--cast a ray
-- 			__debugVis.add_ray (ray position -normal)
			local hits = raymesh.intersectRay position -normal true
			
			--if we get a hit then....
			if (hits > 0) do
			(
				--get the face hit
				local theIndex = rayMesh.getClosestHit() --get the index of the closest hit by the ray
				local theFace = rayMesh.getHitFace theIndex
				
				--get the barycentric coords of the face hit
				local baryPt = rayMesh.getHitBary theIndex
				
				--get the mapverts for the face hit
				local faceMapVerts = meshopGetMapFace sourceMesh SRCchannel theFace	--will be in index order
				--local faceMapVerts = (meshop.getMapVertsUsingMapFace targetMesh channel theFace) as Array
				
				--get the mapvert values for the face hit
				local mapVertValues = #((meshopGetMapVert sourceMesh SRCchannel faceMapVerts.x),
										(meshopGetMapVert sourceMesh SRCchannel faceMapVerts.y),
										(meshopGetMapVert sourceMesh SRCchannel faceMapVerts.z))
				
				--result is the barycentric offset weighted value of the mapvert values
				local colour = (baryPt.x * mapVertValues[1]) + (baryPt.y * mapVertValues[2]) + (baryPt.z * mapVertValues[3])
				
				append sampleColours colour
			)
		)
		
		--average colour at vtx or use samplingFn supplied
		local colour = [0,0,0]
		if (sampleColours.count > 0) do
		(
			if (samplingFn == unsupplied) then
			(
				for c in sampleColours do colour += c
				colour /= sampleColours.count
			)
			else
			(
				colour = samplingFn sampleColours
			)
		)
		
		/*
		else
		(
			print "\tsampleColours is empty..."
		)
		*/
		
		--set the colour
		local geoFaces = (meshopGetFacesUsingVert targetMesh vtx) as Array
		for f in geoFaces do
		(
			local geoFaceVerts = getFace targetMesh f
			local mapFaceVerts = meshopGetMapFace targetMesh TGTchannel f	--will be in index order
			
			--match up indices
			mapVert = case of
			(
				(geoFaceVerts.x == vtx): mapFaceVerts.x
				(geoFaceVerts.y == vtx): mapFaceVerts.y
				(geoFaceVerts.z == vtx): mapFaceVerts.z
			)
			
			--set by subChannels
			if subChannels == "all" then
			(
				meshopSetMapVert targetMesh TGTchannel mapVert colour
			)
			else --set channels specified
			(
				local currentTGTColour = meshopGetMapVert targetMesh TGTchannel mapVert
				
				local maskedExisting = currentTGTColour * subChannelInvMask
				local maskedSample = colour * subChannelMask
				
				--collect up the channels in the final colour
				colour = maskedExisting + maskedSample
				
				--set it
				meshopSetMapVert targetMesh TGTchannel mapVert colour
			)
		)
		progressUpdate (100.0 * (vtx as Float / numVerts))
	)
	progressEnd()
	
	update target
	
	if targetWasPoly do convertToPoly target
		
	--replace target mods if some were preserved
	if (targetMods.count > 0) do
	(
		setCommandPanelTaskMode #modify
		modPanel.setCurrentObject target
		for i=targetMods.count to 1 by -1 do
		(
			modPanel.addModToSelection targetMods[i]
		)
	)
	rayMesh.free()
	gc()
	
	--format "RsProjectVertexColour timer: % \n" ((timeStamp()) - timerStart)
	ok
)


fn RsProjectFaceMap source target SRCchannel:0 TGTchannel:0 sampleBomb:false bombSamples:1 samplingFn:unsupplied =
(
	local sourceMesh = snapshotAsMesh source
	
	local rayMesh = RayMeshGridIntersect()
	rayMesh.Initialize 10
	rayMesh.addNode source
	raymesh.BuildGrid()
	
	local objOp = RsMeshPolyOp target
	
	for face in target.faces do
	(
		local pos = objOp.getfaceCenter target face.index
		local samples = #(pos)
		local sampleColours = #()
		
		--sampleBomb?
		--take a sample poistions inside each face
		if sampleBomb then
		(
			local faceVerts = (objOp.getVertsUsingFace target face.index) as Array
			
			for fv in faceVerts do
			(
				local fvPos = objOp.getVert target fv
				local sampleVec = normalize(fvPos - pos)
				local sampleLine = distance pos fvPos
				local sampleStep = sampleLine / (bombSamples + 1)
				for i=1 to bombSamples do
				(
					append samples (pos + (sampleVec * (i * sampleStep)))
				)
			)
		)
		
		for position in samples do
		(
			local hits = rayMesh.closestFace position
			
			if (hits > 0) do
			(
				--get the face hit
				local theIndex = rayMesh.getClosestHit() --get the index of the closest hit by the ray
				local theFace = rayMesh.getHitFace theIndex
				
				--get the barycentric coords of the face hit
				local baryPt = rayMesh.getHitBary theIndex
				
				--get the mapverts for the face hit
				local faceMapVerts = meshOp.getMapFace sourceMesh SRCchannel theFace	--will be in index order
				--local faceMapVerts = (meshop.getMapVertsUsingMapFace targetMesh channel theFace) as Array
				
				--get the mapvert values for the face hit
				local mapVertValues = #((meshop.getMapVert sourceMesh SRCchannel faceMapVerts.x),
										(meshop.getMapVert sourceMesh SRCchannel faceMapVerts.y),
										(meshop.getMapVert sourceMesh SRCchannel faceMapVerts.z))
				
				--result is the barycentric offset weighted value of the mapvert values
				local colour = (baryPt.x * mapVertValues[1]) + (baryPt.y * mapVertValues[2]) + (baryPt.z * mapVertValues[3])
				
				append sampleColours colour
			)
		)
		
		--average colour at vtx or use samplingFn supplied
		local colour = [0, 0, 0]
		if (samplingFn == unsupplied) then
		(
			for c in sampleColours do colour += c
			colour /= sampleColours.count
		)
		else
		(
			colour = samplingFn sampleColours
		)
		
		colour = ((colour as Point4) as Color)
		
		
		--set the colour
		objOp.SetFaceColor target TGTchannel face.index colour
		
	)
	rayMesh.free()
		
	update target
	
	ok
)
