--=-=-=-=-=-=-=-=-=-=-=-=-= RSL_dotNetObjects =-=-=-=-=-=-=-=-=-=-=-=-=--
/*
	File:
		RSL_dotNetObjects.ms
	Description:
		Defines a stucture containing dotNet objects to speed up the process of creating dotNet tools
	Created by:
		Paul Truss
		Senior Technical Artist
		Rockstar London
	History:
		Created April 12 2009
*/
--=-=-=-=-=-=-=-=-=-=-=-=-= RSL_dotNetObjects =-=-=-=-=-=-=-=-=-=-=-=-=--
struct RSL_dotNetObjects
(
	--=-=-=-=-=-=-=-=-=-=-=-=-= dotNetObjects =-=-=-=-=-=-=-=-=-=-=-=-=--
	fn pointObject point2 = dotNetObject RS_dotNetClass.pointClass point2.x point2.y,
	fn sizeObject point2 = dotNetObject RS_dotNetClass.sizeClass point2.x point2.y,
	
	fn bitmapObject image = 
	(
		local fileExists = getFiles image
		local result = undefined
		if fileExists.count > 0 then
		(
			try
			(
				result = dotNetObject RS_dotNetClass.bitmapClass image
			)
			catch
			(
				result = dotNetObject RS_dotNetClass.bitmapClass (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\images\\TextureBrowseMissingImage.png")
			)
		)
		else
		(
			messageBox "The bitmap supplied cannot be found" title:"RS_dotNet.bitmapObject Error..."
		)
		result
	),
	
	fn iconObject image =
	(
		local fileExists = getFiles image
		local result = undefined
		if fileExists.count > 0 then
		(
			result = dotNetObject RS_dotNetClass.iconClass image
		)
		else
		(
			messageBox "The bitmap supplied cannot be found" title:"RS_dotNet.iconObject Error..."
		)
		result
	),
	
	fn imageObject image =
	(
		local fileExists = getFiles image
		local result = undefined
		if fileExists.count > 0 then
		(
			local type = getFilenameType image
			
			if type == ".ico" then
			(
				result = dotNetObject RS_dotNetClass.imageClass (iconObject image)
			)
			else
			(
				result = dotNetObject RS_dotNetClass.imageClass (bitmapObject image)
			)
		)
		else
		(
			messageBox "The bitmap supplied cannot be found" title:"RS_dotNet.imageObject Error..."
		)
		result
	),
	
	fn imageListObject imageArray imageSize:[16, 16] =--transparentColor:(color 125 125 125) =
	(
		local result = dotNetObject "System.Windows.Forms.ImageList"
-- 		local tColour = RS_dotNetClass.colourClass
		result.imagesize = sizeObject imageSize
		result.ColorDepth = (dotNetClass "ColorDepth").Depth32Bit
		
-- 		local col = transparentColor
-- 		result.transparentColor = tColour.fromARGB col.r col.g col.b
		
		for item in imageArray do
		(
			local fileName = symbolicPaths.expandFileName item
			local type = getFilenameType fileName
			
			if type == ".ico" then
			(
				result.images.add (iconObject fileName)
			)
			else
			(
				result.images.add (bitmapObject fileName)
			)
		)
		result
	),
	
	fn paddingObject pixels = dotNetObject RS_dotNetClass.paddingClass pixels,
	fn fontObject font size style = dotNetObject "System.Drawing.Font" font size style,
	fn listViewItemObject item = dotNetObject "System.Windows.Forms.ListViewItem" item,
		
	fn ColumnStyleObject type value =
	(
		local ColumnStyleType
		if (toLower type) == "percent" then
		(
			ColumnStyleType = RS_dotNetPreset.ST_Percent
		)
		else
		(
			ColumnStyleType = RS_dotNetPreset.ST_Absolute
		)
		
		dotNetObject "ColumnStyle" ColumnStyleType value
	),
	
	fn RowStyleObject type value =
	(
		local RowStyleType
		if (toLower type) == "percent" then
		(
			RowStyleType = RS_dotNetPreset.ST_Percent
		)
		else
		(
			RowStyleType = RS_dotNetPreset.ST_Absolute
		)
		
		dotNetObject "RowStyle" RowStyleType value
	)
)

RS_dotNetObject = RSL_dotNetObjects()
