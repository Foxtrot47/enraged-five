/*------------------------------------------------------------------------------ 	
Returns true if the passed 2D point is within the bounds of the polygon defined by the passed 2D point list.
Note the order of the points passed to the function is important in defining it's shape correctly.
*/------------------------------------------------------------------------------
fn RSMath2D_PointWithinPolygonBounds InputPoint InputPolyPointList =
(	
	InsidePolygonBounds = false
	
	InputPolyPointListCount = InputPolyPointList.Count
	
	i = 1
	j = InputPolyPointListCount
	while( i <= InputPolyPointListCount ) do
	(
		if ( ( ( InputPolyPointList[i].Y > InputPoint.Y ) != ( InputPolyPointList[j].Y > InputPoint.Y ) ) and ( InputPoint.X < ( InputPolyPointList[j].X - InputPolyPointList[i].X ) * (InputPoint.Y-InputPolyPointList[i].Y) / (InputPolyPointList[j].Y-InputPolyPointList[i].Y) + InputPolyPointList[i].X) ) then
		(
			if( InsidePolygonBounds == false ) then 
			( InsidePolygonBounds = true ) 
			else 
			( InsidePolygonBounds = false ) 
		)
		
		j = i
		i = i + 1
	)
	
	return InsidePolygonBounds
	
	
--	Source C code (http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html)
--   int i, j, c = 0;
--   for (i = 0, j = nvert-1; i < nvert; j = i++) {
-- 	if ( ((verty[i]>testy) != (verty[j]>testy)) &&
-- 	 (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
-- 	   c = !c;
--   }
--   return c;
	 
 )

/*------------------------------------------------------------------------------ 	
Returns the centre of the polygon defined by the passed points
*/------------------------------------------------------------------------------
fn RSMath2D_GetPolygonCenter InputPolyPointList =
(
	ReturnAveragePoint = [0,0]
	
	for InputPolyPoint in InputPolyPointList do
	(
		ReturnAveragePoint = ReturnAveragePoint + InputPolyPoint
	)
	
	return ( ReturnAveragePoint / InputPolyPointList.Count )
)

/*------------------------------------------------------------------------------ 	
Returns the area of the passed polygon
*/------------------------------------------------------------------------------
fn RSMath2D_GetPolygonArea InputPolyPointList =
(
	InputPolyPointListCount = InputPolyPointList.Count
	PolygonArea = 0.0
	
	i = 1
	j = InputPolyPointListCount
	while( i <= InputPolyPointListCount ) do
	(
		PolygonArea = PolygonArea + ( InputPolyPointList[j].X + InputPolyPointList[i].X ) * ( InputPolyPointList[j].Y - InputPolyPointList[i].Y )
	
		j = i
		i = i + 1
	)
	
	return PolygonArea
)

/*------------------------------------------------------------------------------ 	
Returns the pixels coords on the passed bitmap which appears within the bounding area 
of the 2D polygon defined by the passed 2D points.
*/------------------------------------------------------------------------------
fn RSImage_GetPixelCoordList InputBitmap InputPolyPointList =
(
	--First we must get the bounds of the poly in 2D Space
	MinPoint = undefined
	MaxPoint = undefined
	for PolyPoint in InputPolyPointList do
	(
		if( MinPoint == undefined ) then
		(
			MinPoint = Copy PolyPoint
		)
		else
		(
			if( PolyPoint.X < MinPoint.X ) then
			(
				MinPoint.X = PolyPoint.X
			)
			if( PolyPoint.Y < MinPoint.Y ) then
			(
				MinPoint.Y = PolyPoint.Y
			)
		)
		
		if( MaxPoint == undefined ) then
		(
			MaxPoint = Copy PolyPoint
		)
		else
		(
			if( PolyPoint.X > MaxPoint.X ) then
			(
				MaxPoint.X = PolyPoint.X
			)
			if( PolyPoint.Y > MaxPoint.Y ) then
			(
				MaxPoint.Y = PolyPoint.Y
			)
		)
	)	
	
	--Modulate them to the best pixel mid points for testing
	MinPoint = [ ( Floor Minpoint.X ) + 0.5, ( Floor Minpoint.Y ) + 0.5 ]
	MaxPoint = [ ( Ceil MaxPoint.X ) - 0.5, ( Ceil MaxPoint.Y ) - 0.5 ]
	
	--Go through each pixel midpoint within the bounds discovered and test to see if it's within the bounds of our polygon
	ReturnCoordList = #()
	CurrentPointX = MinPoint.X
	while( CurrentPointX <= MaxPoint.X ) do
	(
		CurrentPointY = MinPoint.Y
		while( CurrentPointY <= MaxPoint.Y ) do
		(
			if( RSMath2D_PointWithinPolygonBounds [ CurrentPointX, CurrentPointY ] InputPolyPointList ) then
			(
				Append ReturnCoordList [ ( Floor CurrentPointX ), ( Floor CurrentPointY ) ]
				
				--Testpoint = point size:0.25
				--Testpoint.position = [ CurrentPointX, CurrentPointY, 0 ]	
			)
			
			CurrentPointY = CurrentPointY + 1
		)
		CurrentPointX = CurrentPointX + 1
	)
	
	return ReturnCoordList
)

/*------------------------------------------------------------------------------ 	
Returns the pixel color which appears the most within the bounding area of the polygon defined by the passed points.
*/------------------------------------------------------------------------------
fn RSImage_GetMajorColor InputBitmap InputPolyPointList =
(
	--First check to see if the poly is so small it's likely to be subpixel
	PolygonArea = RSMath2D_GetPolygonArea InputPolyPointList
	
	TargetPixelCoordList = #()
	if( PolygonArea > 0.0 ) then
	(
		--Get pixel coords effected by the input polygon
		TargetPixelCoordList = RSImage_GetPixelCoordList InputBitmap InputPolyPointList
	)

	--If the TargetPixelCoordList is empty then the pixel must be tiny (subpixel) so we will get the 
	--centre of the poly and resolve a pixel from this
	if( TargetPixelCoordList.Count == 0 ) then
	(
		PolyCenterPoint = RSMath2D_GetPolygonCenter InputPolyPointList
		Append TargetPixelCoordList [ ( Floor PolyCenterPoint.X ), ( Floor PolyCenterPoint.Y ) ]
	)
	
	--For each pixel collect the color
	PixelColors = #()
	for TargetPixelCoord in TargetPixelCoordList do
	(
		Append PixelColors ( GetPixels InputBitmap TargetPixelCoord 1 ) 
	)

	--Collect how often the pixel is used
	PixelColorHT = dotNetObject "System.Collections.Hashtable"
	MajorPixelColor = Undefined
	for PixelColor in PixelColors do
	(
		PixelColorKey = PixelColor as String
		PixelColorValue = PixelColorHT.Get_Item PixelColorKey
		
		if( PixelColorValue == undefined ) then
		(
			PixelColorValue = 1
			PixelColorHT.Set_Item PixelColorKey PixelColorValue
		)
		else
		(
			PixelColorValue = PixelColorValue + 1
			PixelColorHT.Set_Item PixelColorKey PixelColorValue
		)
		
		if( MajorPixelColor	== undefined ) then
		(
			MajorPixelColor = DataPair PixelColor:PixelColor Count:PixelColorValue
		)
		else
		(
			if( PixelColorValue > MajorPixelColor.Count ) then
			(
				MajorPixelColor = DataPair PixelColor:PixelColor Count:PixelColorValue
			)
		)	
	)

	return (MajorPixelColor.PixelColor)[1]
)

/*------------------------------------------------------------------------------ 	
Sets all the pixels within the area defined by the passed points to the passed color
*/------------------------------------------------------------------------------
fn RSImage_SetColor InputBitmap InputPolyPointList InputColor =
(
	--Get pixel coords effected by the input polygon
	TargetPixelCoordList = RSImage_GetPixelCoordList InputBitmap InputPolyPointList
	
	--For each pixel coord set the color
	for TargetPixelCoord in TargetPixelCoordList do
	(
		SetPixels InputBitmap TargetPixelCoord #(InputColor)
	)
)
