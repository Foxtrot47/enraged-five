struct rsta_spline 
(
	-- GETS THE ANGLE OF BETWEEN A KNOT AND ITS 2 NEIGBOURS
	fn getAngleOfKnot spl seg index =
	(
		numK = numknots spl seg
		
		if index == 0 do index = numK
		if index > numK do index = 1	
		  
		inxP = index - 1
		inxN = index + 1

		if inxP < 1 then inxP = numK
		if inxN > numK then inxN = 1

		pP = getknotpoint spl seg inxP
		pC = getknotpoint spl seg index
		pN = getknotpoint spl seg inxN

		vec1 = pP - pC
		vec2 = pN - pC

		dotVal = dot (normalize vec1) (normalize vec2)
		angVal = acos(dotVal)
		 
		return angVal
	)	
)
rsta_spline = rsta_spline()