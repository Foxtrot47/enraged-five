
--Explicitly loading this dll. Most environments of max load this .net dll automatically. But not all.
DotNet.LoadAssembly "WindowsFormsIntegration.dll"

rollout RsPromptWindow "Information" Height:25
(
	Label PromptLabel "-------------"
)
	
fn RSPushPrompt InputPromptMessage =
(
	CreateDialog RsPromptWindow Style:#(#style_border)
	RsPromptWindow.PromptLabel.Text = InputPromptMessage
)

fn RSPopPrompt =
(
	try( DestroyDialog RsPromptWindow ) catch ()
)

/*------------------------------------------------------------------------------
Summary:
	Resizes the passed Form to fit the ContentHost that triggers the event
*/------------------------------------------------------------------------------
fn ResizeForm s e =
(
	--Set the client size of the form to the size of the content
	ContentHost = s
	Form = ContentHost.Parent
	Form.ClientSize = ContentHost.Size
)

/*------------------------------------------------------------------------------
Summary:
	Creates a tool window which hosts the content contained in 
	the passed UISource file (.xaml).
*/------------------------------------------------------------------------------
struct RSToolWindow
(
	--Source .xaml file that this tool will create it's UI from
	UISource,
	
	--This is the base windows form which hosts the content
	Form,
	
	--This is the windows forms control that hosts our WPF content
	ContentHost,
	
	--This is the actual WPF content which gets hosted
	Content,

	--The wiki link for help 
	WikiURL,
	
	--The name of the tool
	Name,
	
	--Indicates if we should track the usage of the tool
	TrackUsage = false,
	
	fn Open =
	(
		this.Form.ShowModeless()
		
		if( TrackUsage ) then
		(
			--Track the usage of the tool.
			--RsCollectToolUsageData
		)
	),
	
	fn OpenModal =
	(
		this.Form.ShowDialog()
	),
	
	fn Close =
	(
		this.Form.Close()
	),
	
	on Create do
	(
		--Make our form and content host
		this.Form = DotNetObject "MaxCustomControls.MaxForm"
		this.Form.Name = this.Name
		this.Form.Text = this.Name
		this.Form.FormBorderStyle = (DotNetClass "System.Windows.Forms.FormBorderStyle").FixedToolWindow
		this.Form.MaximizeBox = false
		this.Form.MinimizeBox = false
		
		this.ContentHost = DotNetObject "System.Windows.Forms.Integration.ElementHost"
		
		--Read in our UI content
		this.Content = WPF.ReadXamlFile this.UISource
		
		--Set our host to autorize and set our content
		this.ContentHost.AutoSize = true
		this.ContentHost.Child = this.Content
		
		--Add our content to our form
		this.Form.Controls.Add( ContentHost )
		
		--Track changes to the content size
		DotNet.AddEventHandler this.ContentHost "Layout" ResizeForm
		DotNet.AddEventHandler this.ContentHost "SizeChanged" ResizeForm
	)
)

ProgressWindowList = #()
fn ClearAllProgressWindows =
(
	for SubProgressWindow in ProgressWindowList do
	(
		try( DestroyDialog SubProgressWindow ) catch ()
	)
	
	ProgressWindowList = #()
)

fn PushProgressWindow InputProgressWindow =
(
	Append ProgressWindowList InputProgressWindow
)

fn PopProgressWindow InputProgressWindow =
(
	ProgressWindowIndex = FindItem ProgressWindowList InputProgressWindow
	if( ProgressWindowIndex != 0 ) then
	(
	DeleteItem ProgressWindowList ProgressWindowIndex
	)
)

/*------------------------------------------------------------------------------
Summary:
	Creates a basic progress bar window.
*/------------------------------------------------------------------------------
struct RSProgressWindow
(
	Title = "",
	Window,
	SizeWidth = 600,
	SizeHeight = 25,
	PositionX = 0,
	PositionY = 0,
		
	fn PostProgress InputProgressValue =
	(
		Window.Progress.Value = InputProgressValue
	),
	
	StartStep = 0,
	EndStep = 0,
	StepProgressValue = 0.0,
	CurrentStep = 0,
	LastUpdated = 0,
	
	fn PostProgressStep  =
	(
		CurrentStep = CurrentStep + 1
		
		--Check if it has been a certain amount of time since the last progress update.
		--It is much quicker to check a time stamp than it is to simply push an update to the UI (which causes a refresh)
 		if( TimeStamp() - LastUpdated > 250 ) then
 		(
			LastUpdated = TimeStamp()
			
			ProgressValue = StepProgressValue * CurrentStep
			
			Window.Progress.Value = ProgressValue
 		)
	),
	
	fn Start =
	(
		--If the positions are set to 0 we will assume the user wants the default position - centre of screen
		if( PositionX == 0 and PositionY == 0) then
		(
			CreateDialog this.Window Style:#(#Style_Border) Width:this.SizeWidth Height:this.SizeHeight
			
			DialogPosition = GetDialogPos this.Window
			PositionX = DialogPosition[1]
			PositionY = DialogPosition[2]
		)
		else
		(
			CreateDialog this.Window Style:#(#Style_Border) Width:this.SizeWidth Height:this.SizeHeight Pos:[this.PositionX, this.PositionY]
		)
		
		PushProgressWindow this.Window
	),
	
	fn End =
	(
		PopProgressWindow this.Window
		DestroyDialog this.Window
	),
	
	fn StartSubProgress SubStartStep SubEndStep SubTitle:"" =
	(
		ReturnSubProgress = RSProgressWindow Title:SubTitle StartStep:SubStartStep EndStep:SubEndStep PositionX:this.PositionX PositionY:(this.PositionY + this.Window.Height + 5) SizeWidth:this.SizeWidth SizeHeight:this.SizeHeight
		return ReturnSubProgress
	),
	
	on Create do
	(
		--Create our rollout dynamically
		RCreator = RolloutCreator ProgressTitle "Progress..."
		RCreator.Begin()
		
		--Progress text
		LabelWidth = 220
		LabelParamString = "Color:White Pos:[8,6] Width:" + LabelWidth as String + " Height:" + this.SizeHeight as String
		RCreator.AddControl #Label #ProgressText this.Title ParamStr:LabelParamString
		
		--Progress bar
		ProgressWidth = this.SizeWidth - LabelWidth
		ProgressPositionX = LabelWidth
		ProgressBarParamString = "Color:White Pos:[" + ProgressPositionX as String + ",0] Width:" + ProgressWidth as String + " Height:" + this.SizeHeight as String
		RCreator.AddControl #ProgressBar #Progress "" ParamStr:ProgressBarParamString
				
		RCreator.End()
		
		this.Window = RCreator.Def
		
		--Calculate how much each step of progress amounts to 
		StepProgressValue = 100.0 / (EndStep - StartStep) as Float 
	)
)
