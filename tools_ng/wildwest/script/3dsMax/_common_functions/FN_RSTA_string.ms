-----------------------------------------------------------
-- rsta_stringFind
-- Matt Rennie
-----------------------------------------------------------

fn RSTA_stringFind startStr endStr searchStr = 
(
	--if startsStr ==1 then we just start at 1
	--if endStr is -1 then we go through to the end ie -1
	local startStrIndex = undefined
	local endStrIndex = undefined
	
	if startStr == 1 then
	(
		startStrIndex = 1
	)
	else
	(
		--this will get the index that the string starts at
		--so we need to then find the length of the startstr and then add that to this value
		local fnd = findString searchStr startStr 
		
		local startStrLength = startStr.count
		if fnd != undefined do 
		(
			startStrIndex = fnd + startStrLength
		)
	)
	
	if endStr == -1 then
	(
		endStrIndex = -1
	)
	else
	(
		--now we need to do a substring of searchStr from the startStrIndex
		if startStrIndex != undefined do
		(
			local subst = substring searchStr startStrIndex -1
			local fnd = findString subst endStr
			
			if fnd != undefined do 
			(
				endStrIndex = fnd
			)
		)
	)
	
	local returnVal  = "undefined"
	if startStrIndex != undefined do
	(
		if endStrIndex != undefined do
		(
			returnVal = substring searchStr startStrIndex endStrIndex
		)
	)
	
-- 	format ("Returning: "+returnVal+"\n")
	return returnVal
)

fn RSTA_findNumberInString str = 
(
	/**
	WILL LOOP THRU STR TO FIND THE FIRST OCCURANCE OF ONE OF THE NUMERIC CHARACTERS PRESENT IN  NUYMBERcHARS
	**/
	numberChars = #(
		"0",
		"1",
		"2",
		"3",
		"4",
		"5",
		"6",
		"7",
		"8",
		"9"
		)
	index = undefined
	exitLoop = false
	d = 0	
	while d < numberChars.count do
	(
		for c = 1 to numberChars.count do 
		(
			
			char = numberChars[c]
			index1 = findString str char
			if index == undefined do 
			(
				if index1 != undefined do 
				(
					index = index1
				)
			)
			d = c
		)
	)
	
	return index
)

------------------------------------------------------------------------------------------------------
-- RsProgressString:
--	Appends a progressbar to string.  ProgVal should be a 0.0-1.0 float
------------------------------------------------------------------------------------------------------
fn RsProgressString InString ProgVal Length:20 = 
(
	local Msg = StringStream ""
	format "% [" InString To:Msg
	
	-- Replace characters to show progress:
	local ProgInt = (Integer (Ceil (Length * ProgVal)))
	for n = 1 to Length do 
	(
		format "%" (if (n > ProgInt) then "  " else "X") To:Msg
	)
	
	format "]" To:Msg
	
	-- Update current status-prompt message:
	return (Msg as String)
)
