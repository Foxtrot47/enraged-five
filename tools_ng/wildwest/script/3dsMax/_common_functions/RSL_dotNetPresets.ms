--=-=-=-=-=-=-=-=-=-=-=-=-= RSL_dotNetPresets =-=-=-=-=-=-=-=-=-=-=-=-=--
/*
	File:
		RSL_dotNetPresets.ms
	Description:
		Defines a stucture containing dotNet class presets to speed up the process of creating dotNet tools
	Created by:
		Paul Truss
		Senior Technical Artist
		Rockstar London
	History:
		Created January 27 2010
		Updated 10/12/2012 for integration into Wildwest Depot by Andy Davis
*/
filein (RsConfigGetWildWestDir()+ "script/3dsMax/_common_functions/RSL_dotNetClasses.ms")

--=-=-=-=-=-=-=-=-=-=-=-=-= RSL_dotNetPresets =-=-=-=-=-=-=-=-=-=-=-=-=--
struct RSL_dotNetPresets
(
	--=-=-=-=-=-=-=-=-=-=-=-=-= SizeType presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	ST_Percent = RS_dotNetClass.SizeTypeClass.Percent,
	ST_Absolute = RS_dotNetClass.SizeTypeClass.Absolute,
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= FormBorderStyle presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	FB_None = RS_dotNetClass.formBorderStyleClass.None,
	FB_FixedSingle = RS_dotNetClass.formBorderStyleClass.FixedSingle,
	FB_Fixed3D = RS_dotNetClass.formBorderStyleClass.Fixed3D,
	FB_FixedDialog = RS_dotNetClass.formBorderStyleClass.FixedDialog,
	FB_FixedToolWindow = RS_dotNetClass.formBorderStyleClass.FixedToolWindow,
	FB_SizableToolWindow = RS_dotNetClass.formBorderStyleClass.SizableToolWindow,
	FB_Sizable = RS_dotNetClass.formBorderStyleClass.Sizable,
	FBS = dotNetClass "System.Windows.Forms.FormBorderStyle",
	FBS_SizableToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").SizableToolWindow,
	FBS_FixedToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").FixedToolWindow,
	FBS_Sizable = (dotNetClass "System.Windows.Forms.FormBorderStyle").Sizable,
	FBS_None = (dotNetClass "System.Windows.Forms.FormBorderStyle").None,
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= borderStyle presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	BS_Fixed3D = RS_dotNetClass.borderStyleClass.Fixed3D,
	BS_FixedSingle = RS_dotNetClass.borderStyleClass.FixedSingle,
	BS_None = RS_dotNetClass.borderStyleClass.None,
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= DockStyle presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	DS = dotNetClass "System.Windows.Forms.DockStyle",
	DS_Top = RS_dotNetClass.dockStyleClass.Top,
	DS_Left = RS_dotNetClass.dockStyleClass.Left,
	DS_Fill = RS_dotNetClass.dockStyleClass.Fill,
-- 	DS_Fill = (dotNetClass "System.Windows.Forms.DockStyle").Fill,
	DS_Right = RS_dotNetClass.dockStyleClass.Right,
	DS_Bottom = RS_dotNetClass.dockStyleClass.Bottom,
	DS_None = RS_dotNetClass.dockStyleClass.None,
-- 	DS_None = (dotNetClass "System.Windows.Forms.DockStyle").None,

	--=-=-=-=-=-=-=-=-=-=-=-=-= FlatStyle presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	FS_Flat = RS_dotNetClass.flatStyleClass.Flat,
-- 	FS_Flat = (dotNetClass "System.Windows.Forms.FlatStyle").Flat,
	FS_Popup = RS_dotNetClass.flatStyleClass.Popup,
	FS_Standard = RS_dotNetClass.flatStyleClass.Standard,
-- 	FS_Standard = (dotNetClass "System.Windows.Forms.FlatStyle").Standard,
	FS_System = RS_dotNetClass.flatStyleClass.System,
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= CellBorderStyle presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	CBS_Inset = RS_dotNetClass.CellBorderClass.Inset,
	CBS_InsetDouble = RS_dotNetClass.CellBorderClass.InsetDouble,
	CBS_None = RS_dotNetClass.CellBorderClass.None,
	CBS_Outset = RS_dotNetClass.CellBorderClass.Outset,
	CBS_OutsetDouble = RS_dotNetClass.CellBorderClass.OutsetDouble,
	CBS_OutsetPartial = RS_dotNetClass.CellBorderClass.OutsetPartial,
	CBS_Single = RS_dotNetClass.CellBorderClass.Single,
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= imageLayout presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	IL_Center = RS_dotNetClass.imageLayoutClass.Center,
	IL_None = RS_dotNetClass.imageLayoutClass.None,
	IL_Stretch = RS_dotNetClass.imageLayoutClass.Stretch,
	IL_Tile = RS_dotNetClass.imageLayoutClass.Tile,
	IL_Zoom = RS_dotNetClass.imageLayoutClass.Zoom,
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= contentAllignment presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	CA_BottomCenter = RS_dotNetClass.contentAllignmentClass.BottomCenter,
	CA_BottomLeft = RS_dotNetClass.contentAllignmentClass.BottomLeft,
	CA_BottomRight = RS_dotNetClass.contentAllignmentClass.BottomRight,
	CA_MiddleCenter = RS_dotNetClass.contentAllignmentClass.MiddleCenter,
	CA_MiddleLeft = RS_dotNetClass.contentAllignmentClass.MiddleLeft,
	CA_MiddleRight = RS_dotNetClass.contentAllignmentClass.MiddleRight,
	CA_TopCenter = RS_dotNetClass.contentAllignmentClass.TopCenter,
	CA_TopLeft = RS_dotNetClass.contentAllignmentClass.TopLeft,
	CA_TopRight = RS_dotNetClass.contentAllignmentClass.TopRight,
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= Font presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	f_Regular = RS_dotNetClass.fontStyleClass.Regular,
	f_Bold = RS_dotNetClass.fontStyleClass.Bold,
	f_Italic = RS_dotNetClass.fontStyleClass.Italic,
	
	FontLarge = dotNetObject "System.Drawing.Font" "Trebuchet MS" 12 f_Bold,
	FontMedium = dotNetObject "System.Drawing.Font" "Trebuchet MS" 8.0 f_Bold,
	FontStandard = dotNetObject "System.Drawing.Font" "Trebuchet MS" 8.0 f_Regular,
	FontSmallBold = dotNetObject "System.Drawing.Font" "Small Fonts" 8.0 f_Bold,
	FontSmall = dotNetObject "System.Drawing.Font" "Small Fonts" 6.0 f_Regular,
	
	Font_Calibri = dotNetObject "System.Drawing.Font" "Calibri" 8,
	Font_Calibri8 = dotNetObject "System.Drawing.Font" "Calibri" 8,
	Font_Calibri10 = dotNetObject "System.Drawing.Font" "Calibri" 10,
	Font_MSSans = dotNetObject "System.Drawing.Font" "Microsoft Sans Serif" 8,
	Font_Main = dotNetObject "System.Drawing.Font" "Calibri" 8,
	Font_Futura6 = dotNetObject "System.Drawing.Font" "Futura" 6,
	
	--text align
	TA_Center =  (dotNetClass "System.Drawing.ContentAlignment").MiddleCenter,
	TA_Left =  (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft,
	TA_Right =  (dotNetClass "System.Drawing.ContentAlignment").MiddleRight,
	TA = dotNetClass "System.Drawing.ContentAlignment",
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= Appearance presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	AC_Normal = RS_dotNetClass.appearanceClass.Normal,
	AC_Button = RS_dotNetClass.appearanceClass.Button,
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= Appearance presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	SM_GrowAndShrink = RS_dotNetClass.autoSizeModeClass.GrowAndShrink,
	SM_GrowOnly = RS_dotNetClass.autoSizeModeClass.GrowOnly,
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= colour presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	
	ColorClass = (dotNetClass "System.Drawing.Color"),
	controlColour_Light = RS_dotNetClass.colourClass.Gainsboro,
	controlColour_Medium = RS_dotNetClass.colourClass.Silver,
	controlColour_dark = RS_dotNetClass.systemColourClass.ControlDark,
	mouseOverColour = RS_dotNetClass.colourClass.fromARGB 167 217 245, --223 193 159,
	warningColour = RS_dotNetClass.colourClass.fromARGB 188 229 252, --226 167 98,
	errorColour = RS_dotNetClass.colourClass.fromARGB 226 98 98,
	
	ARGB = (dotNetClass "System.Drawing.Color").FromARGB,
	RGB_Black = (dotNetClass "System.Drawing.Color").black,
	RGB_Charcoal = (dotNetClass "System.Drawing.Color").fromARGB 40 40 40,
	RGB_DarkGrey = (dotNetClass "System.Drawing.Color").fromARGB 102 102	102,
	RGB_Steel = (dotNetClass "System.Drawing.Color").FromARGB 200 210 220,
	RGB_Transparent = (dotNetClass "System.Drawing.Color").transparent,
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= other presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	
	--flat styles
	SP_Manual =  (dotNetClass "System.Windows.Forms.FormStartPosition").Manual,

	--padding
	Padding_None = dotNetObject "System.Windows.Forms.Padding" 0
)

RS_dotNetPreset = RSL_dotNetPresets()
