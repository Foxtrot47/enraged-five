-- This is a little function to replace the Print command. 
-- This allows us then to be able to globally turn on and off debugPrint statements.
-- The user must set a variable called debugPrintVal to true which will then allow 
-- prints to occur otherwise the debugPrints never occur
fn RSTA_debugPrint printStr tab:0 = 
(
	if RSTA_debugPrintVal == true do
	(
		local tabstring = ""
		for i=1 to tab do tabstring += "\t"
		format "%%\n" tabstring printStr
	)
)	


struct rsta_debug_visual
(
	debugMesh = #(), 	
	-----------------------------------------------------------------------
	fn dispose =
	(
		try (for i in debugMesh do delete i) catch()
		debugMesh = #()
	),
	-----------------------------------------------------------------------
	fn add_sphere pos c: =
	( 
		if (c == unsupplied) do c = (color 255 0 0)
		local p = sphere segs:8 radius:0.02 pos:pos wirecolor:c
		append debugMesh p
	), 
	-----------------------------------------------------------------------
	fn add_box pos c: = 
	(
		local size = 0.02
		if (c == unsupplied) do c = (color 255 0 0)
		local p = box length:size width:size height:size  wirecolor:c
		convertToPoly p
		CenterPivot p
		p.pos=pos
		append debugMesh p
	),
	-----------------------------------------------------------------------
	fn add_spline knots c: =
	(
		if knots.count > 1 do
		(
			if (c == unsupplied) do c = (color 255 0 0)		
			local ss = SplineShape pos:knots[1] 
			addNewSpline ss
			for k in knots do addKnot ss 1 #corner #line k	
			ss.wirecolor = c
			append debugMesh ss
		)
	),
	-----------------------------------------------------------------------
	fn add_ray r l: h: =
	(
		local p1 = r.pos
		local p2 = r.dir
		local c = color 252 255 0
		----------------------------------
		if (l == unsupplied) do l = 0.25
		
		if (h == unsupplied) then 
		(
			knots = #(p1, (p1+((normalize p2)*l)))
			add_sphere p1 c:c
		)
		else
		(
			if (h == undefined) then
			(
				knots = #(p1, (p1+((normalize p2)*l)))
				c = (color 255 0 0)
				add_sphere p1 c:c
			)
			else
			(
				p2 = h.pos
				format "p1 : % p2 : %\n" p1 p2
				knots = #(p1, p2)
				c = (color 0 255 0)
				add_sphere p1 c:c
				add_sphere p2 c:c
			)
		)		
		----------------------------------
		local ss = add_spline knots c:c
	)
)

-------------------------------------------------------------------------------------------------------------------------