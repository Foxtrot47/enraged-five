-- POINT3 TO QUAT
fn point3ToQuat p3 = 
(
	return (eulerToQuat (eulerAngles (p3[1] as float) (p3[2] as float) (p3[3] as float)))
)
-- QUAT TO POINT3 
fn quatToPoint3 q =
(
	local eulerAngle = quatToEuler q
	local fString = filterString (eulerAngle as string) "() "
	return [fString[2] as float,fString[3] as float,fString[4] as float]
)
