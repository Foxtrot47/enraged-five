fn numRounding num decimalPlaces = 
(
/*
	TAKES VALUE 'num' AND TRIMS IT TO THE NUMBER OF DECIMAL PLACES DEFINED BY decimalPlaces
	** DOES NOT ROUND UP OR DOWN **
	SO IF num was 0.28567 AND decimalPlaces WAS 2 THEN IT WOULD RETURN 0.28
*/	
	format ("rounding to "+(decimalPlaces as string)+" decimal places\n")
	num = num as float
	
	local numString = num as string
	filt = filterstring numString "."
	strNum = filt[1]+"."+(substring filt[2] 1 decimalPlaces)
	format ("Result: "+(result as string)+"\n")
	
	returnStr = strNum as float
	
	format ("returnStr: "+(returnStr as string)+"\n")
	return strNum as float
)

fn RSTA_near_enough f1 f2 tol = 
(
	/**
	will test if f1 and f2 are within the tolerance specified by tol 
	if tol is less than the difference between f1 and f2 then it returns false
	**/
	
	rVal = false
	
	if classof f1 == Point3 then
	(
		val1 = RSTA_near_enough f1[1] f2[1] tol
		val2 = RSTA_near_Enough f1[2] f2[2] tol
		val3 = RSTA_near_Enough f1[3] f2[3] tol
		
		if val1 == true then
		(
			if val2 == true then
			(
				if val3 == true then
				(
					rVal = true
				)
			)
		)
	)
	else
	(
		--SHOULD I USE CLOSE_ENOUGH HERE?
		if abs (f1 - f2) <= tol do
		(
			rVal = true
		)	
-- 		local f1V = (formattedPrint (f1) format:".4f")
-- 		local f2V = (formattedPrint (f2) format:".4f")
-- 		
-- 		local f1V2 = f1V as float
-- 		local f2v2 = f2v as float

-- 		--if (close_enough f1v2 f2v2 tol) then
-- 		if (close_enough f1 f2 tol) then
-- 		(
-- 			rVal = true
-- 		)
-- 		else
-- 		(
-- 			--format ((f1v2 as string)+" not close enough to "+(f2v2 as string)+" with tol: "+(tol as string)+"\n")
-- 		)
	)
	
	return rVal
) 

-- f1 = 0.0
-- f2 = 0.0
-- tol = 0.001

-- RSTA_near_enough f1 f2 tol 

-- fn RSTA_near_enough f1 f2 tol = 
-- (
-- 	/**
-- 	will test if f1 and f2 are within the tolerance specified by tol 
-- 	if tol is less than the difference between f1 and f2 then it returns false
-- 	**/
-- 	
-- 	rVal = false
-- 	
-- 	if classof f1 == Point3 then
-- 	(
-- 		val1 = RSTA_near_enough (f1[1] as integer64) (f2[1] as integer64) tol
-- 		val2 = RSTA_near_Enough (f1[2] as integer64) (f2[2] as integer64) tol
-- 		val3 = RSTA_near_Enough (f1[3] as integer64) (f2[3] as integer64) tol
-- 		
-- 		if val1 == true then
-- 		(
-- 			if val2 == true then
-- 			(
-- 				if val3 == true then
-- 				(
-- 					rVal = true
-- 				)
-- 			)
-- 		)
-- 	)
-- 	else
-- 	(
-- 		if abs ((f1 as integer64) - (f2 as integer64)) <= tol do
-- 		(
-- 			rVal = true
-- 		)
-- 	)
-- 	
-- 	return rVal
-- ) 