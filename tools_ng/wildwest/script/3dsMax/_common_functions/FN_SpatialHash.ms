struct SpatialHashSetEntryAxis
(
	Negative = #(),
	Positive = #(),
	
	fn Dispose =
	(
		for Direction in #( this.Negative, this.Positive ) do
		(
			for Element in Direction do
			(
				if( ClassOf Element == SpatialHashSetEntryAxis ) then
				(
					Element.Dispose()
				)
				Element == Undefined
			)
			
			Free Direction
		)
	),
	
	fn GetAllEntries =
	(
		MasterEntryList = #()
		for Direction in #( this.Negative, this.Positive ) do
		(
			for Element in Direction do
			(
				if( Element != Undefined )then
				(
					if( ClassOf Element == SpatialHashSetEntryAxis ) then
					(
						CurrentEntryList = Element.GetAllEntries() 
						Join MasterEntryList CurrentEntryList
					)
					else
					(
						Join MasterEntryList Element
					)
				)
			)
		)
		
		return MasterEntryList
	)
)

--Stores any form of data using positions as hash keys. This is useful for matching points from one mesh too many meshes.
struct SpatialHashSet
(
	--StringHash which allows us to compare content
	ContentHash = "",
	
	--Cell size in metres (greatly effects memory and speed)
	CellSize = 3.5,
	OffsetPositionList = #([0,0,0]),
	
	--Base entry axis
	XEntryAxis = SpatialHashSetEntryAxis(),
	
	/*------------------------------------------------------------------------------
	Gets the key for the passed value based off the current CellSize
	*/------------------------------------------------------------------------------
	fn GetEntryKey InputValue =
	(
		ReturnValue = ( ( Floor ( Abs ( InputValue ) / CellSize ) ) + 1 ) as Integer 
	),
		
	/*------------------------------------------------------------------------------
	Adds the passed InputValue into this SpatialHash data set using InputPosition as the key 
	*/------------------------------------------------------------------------------
	fn AddEntry InputPosition InputValue =
	(
		--Get X axis direction
		XEntryAxisDirection
		if( InputPosition.X > 0 ) then
		( XEntryAxisDirection = XEntryAxis.Positive )
		else
		( XEntryAxisDirection = XEntryAxis.Negative )
		
		--Get Y axis
		XKey = this.GetEntryKey InputPosition.X
		YEntryAxis = XEntryAxisDirection[XKey]
		if( YEntryAxis == Undefined) then
		(
			YEntryAxis  = SpatialHashSetEntryAxis()
			XEntryAxisDirection[XKey] = YEntryAxis
		)
		
		--Get Y axis direction
		YEntryAxisDirection
		if( InputPosition.Y > 0 ) then
		( YEntryAxisDirection = YEntryAxis.Positive )
		else
		( YEntryAxisDirection = YEntryAxis.Negative )
		
		--Get Z axis
		YKey = this.GetEntryKey InputPosition.Y
		ZEntryAxis = YEntryAxisDirection[YKey]
		if( ZEntryAxis == Undefined) then
		(
			ZEntryAxis  = SpatialHashSetEntryAxis()
			YEntryAxisDirection[YKey] = ZEntryAxis
		)
		
		--Get Z axis direction
		ZEntryAxisDirection
		if( InputPosition.Z > 0 ) then
		( ZEntryAxisDirection = ZEntryAxis.Positive )
		else
		( ZEntryAxisDirection = ZEntryAxis.Negative )
				
		--Set Value ---------------------------
		ZKey = this.GetEntryKey InputPosition.Z
		if( ZEntryAxisDirection[ZKey] == Undefined ) then
		(
			ZEntryAxisDirection[ZKey] = #( DataPair Position:InputPosition Value:InputValue )
		)
		else
		(
			Append ZEntryAxisDirection[ZKey] ( DataPair Position:InputPosition Value:InputValue )
		)
	),
	
	/*------------------------------------------------------------------------------
	Gets the value for the closest entry based of the passed InputPosition
	*/------------------------------------------------------------------------------
	fn GetEntries InputPosition =
	(
		--Get X axis direction
		XEntryAxisDirection
		if( InputPosition.X > 0 ) then
		( XEntryAxisDirection = XEntryAxis.Positive )
		else
		( XEntryAxisDirection = XEntryAxis.Negative )
		
		--Get Y axis
		XKey = this.GetEntryKey InputPosition.X
		YEntryAxis = XEntryAxisDirection[XKey]
		if( YEntryAxis == Undefined) then
		(
			return Undefined
		)
		
		--Get Y axis direction
		YEntryAxisDirection
		if( InputPosition.Y > 0 ) then
		( YEntryAxisDirection = YEntryAxis.Positive )
		else
		( YEntryAxisDirection = YEntryAxis.Negative )
		
		--Get Z axis
		YKey = this.GetEntryKey InputPosition.Y
		ZEntryAxis = YEntryAxisDirection[YKey]
		if( ZEntryAxis == Undefined) then
		(
			return Undefined
		)
		
		--Get Z axis direction
		ZEntryAxisDirection
		if( InputPosition.Z > 0 ) then
		( ZEntryAxisDirection = ZEntryAxis.Positive )
		else
		( ZEntryAxisDirection = ZEntryAxis.Negative )
				
		--Get Value ---------------------------
		ZKey = this.GetEntryKey InputPosition.Z
		if( ZEntryAxisDirection[ZKey] == Undefined ) then
		(
			return Undefined
		)
		else
		(
			return ZEntryAxisDirection[ZKey]
		)
	),
	
	/*------------------------------------------------------------------------------
	Gets closest entry to the passed position
	*/------------------------------------------------------------------------------
	fn GetClosestEntry InputPosition Threshold:10000.0 =
	(
		EntryList = this.GetEntriesWithOffset InputPosition
		ClosestEntry = Undefined
		for Entry in EntryList do
		(
			if( ClosestEntry == Undefined ) then
			(
				EntryDistance = Distance Entry.Position InputPosition
				ClosestEntry = DataPair Entry:Entry Distance:EntryDistance
			)
			else
			(
				EntryDistance = Distance Entry.Position InputPosition
				if( EntryDistance < ClosestEntry.Distance ) then
				(
					ClosestEntry = DataPair Entry:Entry Distance:EntryDistance
				)
			)
		)
		
		if( ClosestEntry != Undefined ) and ( ClosestEntry.Distance < Threshold ) then
		(
			return ClosestEntry.Entry
		)
		
		return Undefined
	),

	/*------------------------------------------------------------------------------
	Returns all the entries for the passed position. Will also check surrounding cells
	*/------------------------------------------------------------------------------
	fn GetEntriesWithOffset InputPosition =
	(		
		EntryList = #()
		for OffsetPosition in OffsetPositionList do
		(
			CurrentEntryList = this.GetEntries( InputPosition + OffsetPosition )
			if( CurrentEntryList != Undefined ) then
			(
				Join EntryList CurrentEntryList
			)
		)
		
		return EntryList
	),
	
	/*------------------------------------------------------------------------------
	Gets all entries from all cells
	*/------------------------------------------------------------------------------
	fn GetAllEntries =
	(
		EntryList = XEntryAxis.GetAllEntries()
	),
	
	/*------------------------------------------------------------------------------
	Disposes of all resources related to this spatial hash set
	*/------------------------------------------------------------------------------
	fn Dispose =
	(
		XEntryAxis.Dispose()
	),
	
	/*------------------------------------------------------------------------------
	Sets up offset array for searching surrounding cells.
	*/------------------------------------------------------------------------------
	fn SetCellSearchBreadth InputCellBreadth =
	(
		OffsetList = #()
		for i = 0 to InputCellBreadth do
		(
			AppendIfUnique OffsetList ( this.CellSize * i )
			AppendIfUnique OffsetList ( this.CellSize * -i )
		)
		
		this.OffsetPositionList = #()
		for OffsetX in OffsetList do
		(
			for OffsetY in OffsetList do
			(
				for	OffsetZ  in OffsetList do
				(	
					Append this.OffsetPositionList ( [ OffsetX, OffsetY, OffsetZ ] )
				)
			)
		)
	)
)