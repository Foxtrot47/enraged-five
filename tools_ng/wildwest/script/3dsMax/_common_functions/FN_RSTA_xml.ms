--------------------------------------------------------------------
-- XML COMMON FUNCTIONS --------------------------------------------
--------------------------------------------------------------------
-- WIKI: https://devstar.rockstargames.com/wiki/index.php/Rsta_common_functions_xml
---------------------------------------------------------------------

--------------------------------------------------------------------
-- NAME: RSTA_xml_IO 
-- AUTHOR: Matt Harrad 24.10.13
-- ABOUT: A struct for handeling dotnet xml file streams and xmlDocuments
--------------------------------------------------------------------
struct RSTA_xml_IO
(
	---------------------------------------------------------------------------
	-- LOCALS
	---------------------------------------------------------------------------
	stream = undefined,
	xmlFile = undefined,
	root = undefined,
	xDoc = dotNetObject "System.Xml.XmlDocument",
	error = false,
	autoLoad = true, 
	newRoot = undefined,
	---------------------------------------------------------------------------
	-- FUNCTIONS 
	---------------------------------------------------------------------------
	fn warning message =
	(
		messageBox message title:"RSTA_xml_IO"
	),
	---------------------------------------------------------------------------
	fn xmlNode = 
	(
		return dotNetObject "System.Xml.XmlNode"
	),
	---------------------------------------------------------------------------
	fn new rootString =
	(
		xDoc.AppendChild (xDoc.CreateXmlDeclaration "1.0" "utf-8" "")			
		xDoc.AppendChild (xDoc.CreateElement (substituteString rootString " " ""))			
		root = xDoc.documentElement 
		return true
	),
	---------------------------------------------------------------------------
	fn load =
	(
		if (xmlFile != undefined) then
		(
			if (doesFileExist xmlFile) then
			(
				-- Open the xml with the streamReader 
				stream = dotNetObject "System.IO.StreamReader" xmlFile
				try (xDoc.load stream) catch -- ERROR CHECKING
				(
					-- SOMETHING WENT WRONG, CLOSE OUT THE STREAM FILE
					warning ("Xml formatting error! Please check the file for mistakes : \n" + xmlFile)
					this.dispose()
					error = true					
					return false
				)
				if not(error) do 
				(
					root = xDoc.documentElement  
					stream.close() -- UNLOCK THE FILE
					return true
				)
			)
			else
			(
				warning (xmlFile + " : This file does not exist, please check the path.")
				error = true
				return false
			)
		)
		else
		(
			warning ("Please specifiy an xml file using RSTA_xml_IO.xmlFile <string>.")
			error = true
			return false
		)
	),
	---------------------------------------------------------------------------
	fn dispose =
	(
		stream.close() 
		stream.dispose() 
		stream = undefined 
		gc() 
	),
	---------------------------------------------------------------------------
	fn save saveWarning:true = 
	(
		if (xmlFile != undefined) then
		(
			if (doesFileExist xmlFile) then
			(
				if (getFileAttribute xmlFile #readOnly) then 
				(
					messagebox ("File Set to READ ONLY : \n\t" +xmlFile)
					return false				
				)
				else 
				(
					if (saveWarning) then
					(
						if (queryBox ("File already exists : \n\n" + xmlFile + "\n\nSave over file?") title:"RSTA_xml_IO") then
						(
							xDoc.save xmlFile	
							return true
						)
						else return false
					)
					else 
					(
						xDoc.save xmlFile	
						return true
					)			
				)
			)
			else
			(
				xDoc.save xmlFile
				return true
			)
		)
		else warning "Fn Save - XML File is undefined!"
	),
	---------------------------------------------------------------------------
	fn saveAs saveAsFilePath saveWarning:true =
	(
		if (doesFileExist saveAsFilePath) then
		(
			if (getFileAttribute saveAsFilePath #readOnly) then 
			(
				messagebox ("File Set to READ ONLY : \n\t" +saveAsFilePath)	
				return false
			)
			else 
			(
				if (saveWarning) then
				(
					if (queryBox ("File already exists : \n\n" + saveAsFilePath + "\n\nSave over file?") title:"RSTA_xml_IO") then
					(
						xDoc.save saveAsFilePath	
						return true
					)
					else return false
				)
				else 
				(
					xDoc.save saveAsFilePath	
					return true
				)
			)
		)
		else  
		(
			xDoc.save saveAsFilePath
			return true
		)			
	),
	---------------------------------------------------------------------------
	fn print =
	(
		local stringBuilder = dotNetObject "System.Text.StringBuilder"
		local stringWriter = dotNetObject "System.IO.StringWriter" stringBuilder
		local xmlWriter = dotNetObject "System.Xml.XmlTextWriter" stringWriter
		xmlWriter.Formatting = xmlWriter.Formatting.Indented
		xDoc.Save xmlWriter	
		format "----------------------------------\n%\n----------------------------------\n" (stringBuilder.ToString())
	),
	------------------------------------------------------------------------------------
	on create do 
	(
		if (newRoot == undefined) then 
		(
			if (autoLoad) AND (xmlFile != undefined) do load()
		)
		else
		(
			new newRoot
		)
	)
)

--------------------------------------------------------------------
-- NAME: 
-- AUTHOR:
-- ABOUT: 
--------------------------------------------------------------------
struct rsta_xml
(
	--////////////////////////////////////////////////////////////////
	-- NODE FUNCTIONS
	--////////////////////////////////////////////////////////////////
	---------------------------------------------------------------------
	-- MAKE CONDITION FOR XML STRING
	---------------------------------------------------------------------
	fn make_conditional attr value =
	(
		conditional = ""
		if (attr != undefined) do 
		(
			conditional = "[@" + attr 
			if (value != undefined) then conditional += "=\"" + value + "\"]"
			else conditional += "]"
		)
		return conditional
	),
	
	---------------------------------------------------------------------
	-- MAKES A NEW NODE WITH ARGUMENTS, UNIQUE WILL ONLY MAKE ONE INSTANCE OF THE NODE.
	---------------------------------------------------------------------
	fn makeNode node_name parent attr:undefined value:undefined unique:false innerText:undefined=
	(
		if (parent != undefined) then
		(
			theNode = parent.SelectSingleNode(node_name+(make_conditional attr value))	
			if (theNode == undefined OR not(unique)) then 
			(
				theNode = parent.OwnerDocument.CreateNode "element" node_name "" 				
				parent.AppendChild theNode
			)	
			if (attr != undefined AND value != undefined) do theNode.SetAttribute attr value
			if (innerText != undefined) do theNode.innerText = innerText
			return theNode
		)
		else messageBox "Parent Node is undefined" title:"rsta_xml.makeNode"
	),
	
	---------------------------------------------------------------------
	-- GETS ARRAY OF XML NODES FROM THE CHILDREN OF SUPPLIED NODE
	---------------------------------------------------------------------
	fn getChildren xmlNode_parent name:undefined attr:undefined value:undefined =
	(
		if (name == undefined) then
		(
			xmlNode_array = for i=0 to xmlNode_parent.ChildNodes.Count-1 collect xmlNode_parent.ChildNodes.ItemOf[i]
			return xmlNode_array
		)
		else
		(
			searchNodes = xmlNode_parent.Selectnodes(name+(make_conditional attr value))
			xmlNode_array = for i = 0 to searchNodes.count - 1 collect searchNodes.itemof[i]
			return xmlNode_array
		)
	),
	
	---------------------------------------------------------------------
	-- DOES NODE EXIST, CAN BE RECURSIVE.
	---------------------------------------------------------------------
	fn doesNodeExist xmlNode_parent node_name attr:undefined value:undefined recursive:false =
	(
		local theNode = xmlNode_parent.SelectSingleNode(node_name+(make_conditional attr value))	
		if (recursive) then
		(
			if (theNode != undefined) then return true
			else 
			(
				for c in getChildren xmlNode_parent do
				(
					if (doesNodeExist c node_name attr:attr value:value recursive:true) do return true
				)
				return false
			)
		)
		else
		(
			if (theNode != undefined) then return true
			else return false
		)
	),
	
	--////////////////////////////////////////////////////////////////
	-- NODE DATA TYPES 
	--////////////////////////////////////////////////////////////////
	
	---------------------------------------------------------------------
	-- MAKE A NODE THAT HAS POINT 3 DATA
	---------------------------------------------------------------------
	fn makeNode_point3 node_name p3 parent_xn unique:true precision:3 =
	(
		if (classof p3 == EulerAngles)do 
		(
			local p3_string = filterString (p3 as string) "() "
			p3 = [p3_string[2] as float,p3_string[3] as float,p3_string[4] as float]
		)
		if (classof p3 == String) do 
		(
			local p3_string = filterString p3 "[ ,]"
			if p3_string.count == 3 do p3 = [p3_string[1] as float,p3_string[2] as float,p3_string[3] as float]
		)
		if (classof p3 == Point3) do
		(
			local p3_xn = makeNode node_name parent_xn unique:unique
			p3_xn.SetAttribute "x" (formattedPrint p3.x format:("#." + precision as string + "f"))
			p3_xn.SetAttribute "y" (formattedPrint p3.y format:("#." + precision as string + "f"))
			p3_xn.SetAttribute "z" (formattedPrint p3.z format:("#." + precision as string + "f") )
			return p3_xn
		)
		return undefined
	),
	
	---------------------------------------------------------------------
	-- GET A POINT3 VALUE OUT OF NODE
	---------------------------------------------------------------------
	fn get_point3 node_xn =
	(
		if (node_xn != undefined) do
		(
			local x = node_xn.getAttribute "x"
			local y = node_xn.getAttribute "y"
			local z = node_xn.getAttribute "z"
			return [(x as float),(y as float),(z as float)]
		)
	),
	
	---------------------------------------------------------------------
	-- MAKE A NODE THAT HAS BOOL DATA
	---------------------------------------------------------------------
	fn makeNode_bool node_name value parent_xn unique:true = 
	(
		if value == 1 OR value == "1" then value = true
		if value == 0 OR value == "0" then value = false 
		local bool_xn = makeNode node_name parent_xn unique:unique
		bool_xn.SetAttribute "value" (value as string)
		return bool_xn
	),
	
	---------------------------------------------------------------------
	-- GET A BOOLEAN VALUE OUT OF NODE
	---------------------------------------------------------------------
	fn get_bool node_xn =
	(
		local value = node_xn.getAttribute "value"
		if value == "true" do value = true
		if value == "false" do value = false
		return value
	),
	
	---------------------------------------------------------------------
	-- MAKE A NODE THAT HAS POINT 3 DATA
	---------------------------------------------------------------------
	fn makeNode_quat node_name q parent_xn unique:true precision:5 =
	(
		/*
		if (classof p3 == EulerAngles)do 
		(
			local p3_string = filterString (p3 as string) "() "
			p3 = [p3_string[2] as float,p3_string[3] as float,p3_string[4] as float]
		)
		if (classof p3 == String) do 
		(
			local p3_string = filterString p3 "[ ,]"
			if p3_string.count == 3 do p3 = [p3_string[1] as float,p3_string[2] as float,p3_string[3] as float]
		)*/
		if (classof q == quat) do
		(
			local q_xn = makeNode node_name parent_xn unique:unique
			q_xn.SetAttribute "x" (formattedPrint q.x format:("#." + precision as string + "f"))
			q_xn.SetAttribute "y" (formattedPrint q.y format:("#." + precision as string + "f"))
			q_xn.SetAttribute "z" (formattedPrint q.z format:("#." + precision as string + "f") )
			q_xn.SetAttribute "w" (formattedPrint q.w format:("#." + precision as string + "f") )
			return q_xn
		)
		return undefined
	),
		
	---------------------------------------------------------------------
	-- GET A QUATERIAN VALUE OUT OF NODE
	---------------------------------------------------------------------
	fn get_quat node_xn =
	(
		if (node_xn != undefined) do
		(
			local x = node_xn.getAttribute "x"
			local y = node_xn.getAttribute "y"
			local z = node_xn.getAttribute "z"
			local w = node_xn.getAttribute "w"
			return (quat (x as float) (y as float)(z as float) (w as float))
		)
	)
)
-- SET UP GLOBAL XML FUNCTIONS 
rsta_xml = rsta_xml()
