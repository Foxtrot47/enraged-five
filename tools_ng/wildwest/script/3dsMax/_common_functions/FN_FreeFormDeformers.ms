-- return number of control points (doesn't check if they can be get/set)
fn RSFFD_GetNumCPs ffdmod =
(
	ffdmod["Master"].numSubs
)

-- convert FFD local (normalized) coordinates to world coordinates
-- obj = object containing the modifier
-- ffdmod = the actual modifier
-- pos = the local coordinate
fn RSFFD_LocalToWorldPos obj ffdmod pos =
(
	objTM = obj.objecttransform
	modTM = (getModContextTM obj ffdmod) * ffdmod.lattice_transform.value
	modBBMin = getModContextBBoxMin obj ffdmod
	modBBMax = getModContextBBoxMax obj ffdmod
	(modBBMin + (pos * (modBBMax-modBBMin)) * (inverse modTM) * objTM)
)

-- convert world coordinates to FFD local (normalized) coordinates
-- obj = object containing the modifier
-- ffdmod = the actual modifier
-- pos = the world coordinate
fn RSFFD_WorldToLocalPos obj ffdmod pos =
(
	objTM = obj.objecttransform
	modTM = (getModContextTM obj ffdmod) * ffdmod.lattice_transform.value
	modBBMin = getModContextBBoxMin obj ffdmod
	modBBMax = getModContextBBoxMax obj ffdmod
	(pos - modBBMin) * (inverse objTM) * inverse modTM / (modBBMax-modBBMin)
)

-- get an FFD mod's control point's world coordinate
-- obj = object containing the modifier
-- ffdmod = the actual modifier
-- i = control point index
fn RSFFD_GetWorldPos obj ffdmod i =
(
	cp = ffdmod["Master"][i]
	RSFFD_LocalToWorldPos obj ffdmod cp.value
)

-- set an FFD mod's control point's world coordinate
-- obj = object containing the modifier
-- ffdmod = the actual modifier
-- i = control point index
-- pos = new world position
fn RSFFD_SetWorldPos obj ffdmod i pos =
(
	cp = ffdmod["Master"][i]
	cp.value = RSFFD_WorldToLocalPos obj ffdmod pos
)