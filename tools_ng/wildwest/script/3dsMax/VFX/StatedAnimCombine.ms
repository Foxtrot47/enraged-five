
struct SOData
(
	fn CollectData InputSO = 
	(
		ReturnSOData = DotNetObject "StatedAnim.SOData" InputSO.GUID
		ReturnSOData.FaceCount = InputSO.GetFaceCount()
		ReturnSOData.VertCount = InputSO.GetVertCount() 
		ReturnSOData.BoneCount = InputSO.BoneCount()
		
		SourceMesh = InputSO.MeshNode
		SourceMaterial = SourceMesh.Material
		
		case ClassOf SourceMaterial of
		(
			Rage_Shader:
			(	
				CurrentMaterialHash = GetMaterialHash SourceMaterial
				ReturnSOData.Append CurrentMaterialHash ReturnSOData.FaceCount
			)
			MultiMaterial:
			(
				--Get all the material IDs on object
				MatIDArray = RSMesh_GetMaterialIDArray SourceMesh
				for MatID in MatIDArray do
				(
					--Get the current material. 
					CurrentMaterialHash = GetMaterialHash SourceMaterial[MatID]
					
					--Get the faces this material is applied to
					FaceBitArray = RSMesh_GetFaceBitArrayByMaterialID SourceMesh MatID
					
					--Add collected data to our SOData
					ReturnSOData.Append CurrentMaterialHash FaceBitArray.Count
				)
			)
			Default:
			(
				--Invalid material.
				CurrentMaterialHash = GetMaterialHash SourceMaterial
				ReturnSOData.Append CurrentMaterialHash ReturnSOData.FaceCount
			)
		)
		
		return ReturnSOData 
	)
)
