rollout RockstarFumeFx "Rockstar FumeFx"
(
		
	button btn_SimWaveRender "Simulate Wave Render" width:300 tooltip:"This will Simulate, Wavelet and Render"
	
	on btn_SimWaveRender pressed do
	(
		$fumefx001.runsimulation(0)
		$fumefx001.runsimulation(2)
		messagebox (maxfilename + "  \n" + "is complete")
	)
				

	button btn_SimWaveBatchRender "Simulate Wave Batch Render" width:300 tooltip:"This will Simulate, Wavelet and Batch Render"
	on go pressed do
	(
		$fumefx001.runsimulation(0)
		$fumefx001.runsimulation(2)
		batchRenderMgr.Render()
		messagebox (maxfilename + "  \n" + "is complete")
	)
			

	button btn_SimRenderSelection "Simulate Render Selection" width:300 tooltip:"This will Simulate and Render Selection"
	on btn_SimRenderSelection pressed do
	(
		$fumefx001.runsimulation(0)
		render framerange:#active outputfile:rendoutputfilename
		messagebox (maxfilename + "  \n" + "is complete")
	)
			
	button btn_SimWaveRenderSelection "Simulate, Wave and Render Selection" width:300 tooltip:"This will Simulate, Wavelet and Render Selection"
	on btn_SimWaveRenderSelection pressed do
	(
		$.runsimulation(0)
		$.runsimulation(2)
		render framerange:#active outputfile:rendoutputfilename
		messagebox (maxfilename + "  \n" + "is complete")
	)
			
	button btn_SimWave "Simulate Wave" width:300 tooltip:"This will Simulate and Wave"
	on btn_SimWave pressed do
	(
		$.runsimulation(0)
		$.runsimulation(2)
	)
			
				
	button btn_Sim "Simulate" width:300 tooltip:"This will Simulate"
	on btn_Sim pressed do
	(
		$.runsimulation(0)
	)
			
)
createdialog RockstarFumeFx width:300