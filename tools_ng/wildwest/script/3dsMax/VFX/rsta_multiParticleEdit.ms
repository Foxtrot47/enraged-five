filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
FileIn (RsConfigGetWildWestDir() + "script/3dsMax/_common_functions/FN_RSTA_userSettings.ms")

try (destroyDialog  MultiParticleEditRoll) catch()
rollout MultiParticleEditRoll "Mutli Particle Edit" width:200
(
	------------------------------------------------------------------
	-- LOCALS
	------------------------------------------------------------------
	local copyHelper
	local CollParts = #()
	local Settings = rsta_userSettings app:"MultiParticleEdit"
	
	------------------------------------------------------------------
	-- FUNTIONS
	------------------------------------------------------------------
	fn copy_pfx obj_a obj_b = 
	(	
		if (GetAttrClass obj_a == GetAttrClass obj_b) do 
		(
			for i = 1 to (GetNumAttr (GetAttrClass obj_a)) do setAttr obj_b i (GetAttr obj_a i)			
		
			obj_b.myType = obj_a.myType
			obj_b.hideDomain = obj_a.hideDomain
			obj_b.showGizmo = obj_a.showGizmo
			obj_b.active = obj_a.active
			obj_b.minZoom = obj_a.minZoom
			obj_b.maxZoom = obj_a.maxZoom
			obj_b.myZoom = obj_a.myZoom
			obj_b.myKeyFrame = obj_a.myKeyFrame
			obj_b.isWorldSpace = obj_a.isWorldSpace
			obj_b.hideLabel = obj_a.hideLabel
			obj_b.showScriptSpecific = obj_a.showScriptSpecific
		)
	)
	---------------------------------------------------------------------------
	fn do_copy = 
	(
		local sel = getCurrentSelection()
		if sel.count != 0 and classof sel[1] == RsRageParticleHelper then copyHelper = sel[1]
		else messageBox "Please select a particle helper first." 
	)
	---------------------------------------------------------------------------
	fn do_paste = 
	(
		if (copyHelper != undefined) then 
		(
			for obj in getCurrentSelection() do 
			(
				if classof obj == RsRageParticleHelper then copy_pfx copyHelper obj				
				else 
				(			
					local p = RsRageParticleHelper()
					p.parent = obj	
					in coordsys parent p.rotation = in coordsys parent copyHelper.rotation		
					in coordsys parent p.pos = in coordsys parent copyHelper.pos	
					copy_pfx copyHelper p
				)
			)
		)
		else messageBox "Please copy a particle helper first."
	)
	---------------------------------------------------------------------------
	fn do_addToSelection = 
	(
		local newItems = #()
		for obj in getCurrentSelection() do 
		(
			local p = RsRageParticleHelper()
			p.myType = MultiParticleEditRoll.ddl_type.selected
			p.parent = obj
			p.rotation = obj.rotation 
			p.pos = obj.pos
			append newItems p
		)
		select newItems
	)
	---------------------------------------------------------------------------
	fn GetTypes =
	(		
		MultiParticleEditRoll.ddl_type.items = (for i in CollParts where i.type == MultiParticleEditRoll.ddl_cat.selected collect i.name)
	)
	---------------------------------------------------------------------------
	fn do_applySettings = 
	(
		for i in getCurrentSelection() where classof i == RsRageParticleHelper do 
		(
			i.myType = MultiParticleEditRoll.ddl_type.selected
		)
	)	
	------------------------------------------------------------------
	-- CONTROLS
	------------------------------------------------------------------
	dotNetControl RsBannerPanel "Panel" pos:[0,0] height:32 width:MultiParticleEditRoll.width
	local banner = makeRsBanner dn_Panel:RsBannerPanel versionNum:1.0 versionName:"Zippy Art" wiki:"" filename:(getThisScriptFilename())
		
	button btn_copy "Copy" width:90 across:2 align:#left offset:[-5,0]
	button btn_paste "Paste" width:90 align:#right offset:[5,0]
		
	button btn_addToSelection "Add Particles To Selection" width:185
		
	group "Particle Type"
	(
		label lb_1 "Type : " align:#left across:2 offset:[0,2]
		dropdownlist ddl_cat "" items:gTriggerStates align:#right width:135
		
		label lb_2 "Effect : " align:#left across:2 offset:[0,2]
		dropdownlist ddl_type "" items:gTriggerStates align:#right width:135
		
		button btn_applySettings "Set Type On Selection" width:185
	)

	------------------------------------------------------------------
	-- EVENTS
	------------------------------------------------------------------
	on MultiParticleEditRoll open do 
	(
		settings.dialog_windowPos MultiParticleEditRoll #get
		banner.setup()	
		LoadEffectEntries CollParts
		GetTypes()
	)
	on MultiParticleEditRoll close do 	settings.dialog_windowPos MultiParticleEditRoll #set
	on btn_copy pressed do do_copy()
	on btn_paste pressed do do_paste()
	on btn_addToSelection pressed do do_addToSelection()
	on ddl_cat selected arg do GetTypes()
	on btn_applySettings pressed do do_applySettings()
	
)
createDialog MultiParticleEditRoll style:#(#style_titlebar, #style_border, #style_sysmenu,#style_toolwindow)