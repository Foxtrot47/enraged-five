FileIn "StatedAnim.ms"
CSharp.CompileToMemory #(RsConfigGetWildWestDir() + "script/3dsMax/VFX/StatedAnim.cs")

if( StatedAnimUI != Undefined ) then StatedAnimUI.Close()
/*---------------------------------------------------
UI Definition
*/---------------------------------------------------
StatedAnimUI = WPF.ReadXamlFile (RsConfigGetWildWestDir() + "script/3dsMax/UI/StatedAnimTool.xaml")
StatedAnimUI_Combo_Selected = WPF.GetElement StatedAnimUI "Combo_Selected"
StatedAnimUI_Button_Create = WPF.GetElement StatedAnimUI "Button_Create"
StatedAnimUI_Label_Count = WPF.GetElement StatedAnimUI "Label_Count"
StatedAnimUI_Progress_Create = WPF.GetElement StatedAnimUI "Progress_Create"
StatedAnimUI_Label_Progress_Status = WPF.GetElement StatedAnimUI "Label_Progress_Status"
StatedAnimUI_Label_EstimatedTime = WPF.GetElement StatedAnimUI "Label_EstimatedTime"
StatedAnimUI_Combo_SelectedStatedAnim = WPF.GetElement StatedAnimUI "Combo_SelectedStatedAnim"
StatedAnimUI_ListBox_GroupMembers = WPF.GetElement StatedAnimUI "ListBox_GroupMembers"
StatedAnimUI_Button_Add_State = WPF.GetElement StatedAnimUI "Button_Add_State"
StatedAnimUI_Button_Remove_State = WPF.GetElement StatedAnimUI "Button_Remove_State"
StatedAnimUI_Combo_Add_State_Type = WPF.GetElement StatedAnimUI "Combo_Add_State_Type"
StatedAnimUI_Button_Create_Proxy = WPF.GetElement StatedAnimUI "Button_Create_Proxy"

StatedAnimUI.Show()

--Register the window with max
WindowInterOp = DotNetObject "System.Windows.Interop.WindowInteropHelper" StatedAnimUI
MaxHandle = Windows.GetMAXHWND()
IntPtr = DotNetObject "System.IntPtr" MaxHandle
WindowInterOp.Owner = IntPtr

NETTimespan = DotNetClass "System.Timespan"
StatedAnimObjectList = DotNetObject "StatedAnim.StatedAnimObjectList"

/*---------------------------------------------------
Event handlers
*/---------------------------------------------------
fn PopulateValidSelectionSets =
(
	ValidSelectionSetArray = StatedAnimCore.GetValidSelectionSetArray()
	StatedAnimUI_Combo_Selected.Items.Clear()
	for SelectionSet in ValidSelectionSetArray do
	(
		StatedAnimUI_Combo_Selected.Items.Add SelectionSet.Name
	)
)

fn PopulateValidStatedAnimGroups =
(
	StatedAnimGroupArray = StatedAnimCore.GetGroupArray()
	
	StatedAnimUI_Combo_SelectedStatedAnim.Items.Clear()

	for StatedAnimGroup in StatedAnimGroupArray do 
		StatedAnimUI_Combo_SelectedStatedAnim.Items.Add(StatedAnimGroup)
)

fn SSetSelectionChanged =
(
	if(StatedAnimUI_Combo_Selected.Items.Count > 0) then
	(
		TargetSelectionSetName = StatedAnimUI_Combo_Selected.SelectedItem
		TargetObjectArray = StatedAnimCore.GetTargetObjectArray TargetSelectionSetName
		StatedAnimUI_Label_Count.Content = TargetObjectArray.Count
		EstimatedTime = NETTimespan.FromMilliseconds( TargetObjectArray.Count * 430 )
		StatedAnimUI_Label_EstimatedTime.Content = EstimatedTime.ToString @"mm\:ss"
		StatedAnimUI_Button_Create.IsEnabled = True
		StatedAnimUI_Progress_Create.Value = 0
		StatedAnimUI_Label_Progress_Status.Content = ""
	)
	else
	(
		StatedAnimUI_Label_EstimatedTime.Content = "N/A"
		StatedAnimUI_Label_Count.Content = "N/A"
		StatedAnimUI_Button_Create.IsEnabled = False
		StatedAnimUI_Progress_Create.Value = 0
		StatedAnimUI_Label_Progress_Status.Content = ""
	)
)

fn SelectedStatedAnimChanged =
(
	StatedAnimObjectList.Clear()
	SelectedGroupName = StatedAnimUI_Combo_SelectedStatedAnim.SelectedItem
	GroupObjectArray = StatedAnimCore.GetGroupObjectArray SelectedGroupName
		
	if GroupObjectArray.Count > 0 then
	(	
		StatedAnimUI_ListBox_GroupMembers.Visibility = (DotNetClass "System.Windows.Visibility").Visible
		
		for GroupObject in GroupObjectArray do
		(
			State = StatedAnimCore.GetState GroupObject
			NewStatedAnimObject = DotNetObject "StatedAnim.StatedAnimObject" GroupObject.Name State GroupObject.Handle
			StatedAnimObjectList.AddRegister( NewStatedAnimObject )
		)
		--StatedAnimObjectList.SortObjects()
		StatedAnimUI_ListBox_GroupMembers.ItemsSource = StatedAnimObjectList
		StatedAnimUI_Button_Add_State.IsEnabled = True
		StatedAnimUI_Button_Remove_State.IsEnabled = True
		StatedAnimUI_Button_Create_Proxy.IsEnabled = True
	)
	else
	(
		StatedAnimUI_ListBox_GroupMembers.Visibility = (DotNetClass "System.Windows.Visibility").Collapsed
		StatedAnimUI_Button_Add_State.IsEnabled = False
		StatedAnimUI_Button_Remove_State.IsEnabled = False
		StatedAnimUI_Button_Create_Proxy.IsEnabled = False
	)
	
)

fn ChangeState s e =
(
	TargetNode = MaxOps.GetNodeByHandle s.NodeHandle
	StatedAnimCore.SetState TargetNode s.State
)

fn ChangeGroup s e =
(
	if( e.NewItems != Undefined ) then
	(
	 	for i = 1 to e.NewItems.Count do
	 	(
			NewItem = e.NewItems.Item(i-1)
			
			TargetNode = MaxOps.GetNodeByHandle NewItem.NodeHandle
			StatedAnimCore.SetState TargetNode NewItem.State
			
			TargetGroup = StatedAnimUI_Combo_SelectedStatedAnim.SelectedItem
			StatedAnimCore.SetGroup TargetNode TargetGroup
	 	)
	)
	
	if( e.OldItems != Undefined ) then
	(
	 	for i = 1 to e.OldItems.Count do
	 	(
			OldItem = e.OldItems.Item(i-1)
			
			TargetNode = MaxOps.GetNodeByHandle OldItem.NodeHandle
			StatedAnimCore.SetState TargetNode "None"
			StatedAnimCore.SetGroup TargetNode "DEFAULT"
	 	)
	)
)

fn PostProgress ProgressValue StatusString =
(
	StatedAnimUI_Progress_Create.Value = ProgressValue
	StatedAnimUI_Label_Progress_Status.Content = StatusString
	DisplayTempPrompt StatusString 1000
	ProgressUpdate ProgressValue
)

fn CreateStatedAnim =
(
	if not( QueryBox "This operation will leave nothing in your scene except an exportable stated anim. It is advisable to save any changes to your WIP scene first. Continue?" ) then ( return() )

	ProgressStart "Starting...."
	ProgressEnd()
	
	TargetSelectionSetName = StatedAnimUI_Combo_Selected.SelectedItem
	StatedAnimCore.CreateStatedAnim SelectionSets[TargetSelectionSetName] PostProgress	
)

fn IsValidForPick Obj =
(
	if( StatedAnimObjectList.HandleExists Obj.Handle ) then return false
	
	if( Obj.Parent != Undefined ) then return false
	
	case ( ClassOf Obj ) of
		(
		Editable_Poly: return True
		Editable_Mesh: return True
		PolyMeshObject: return True
		)
		
	return false
)
fn AddStateClicked =
(
	TypeToAdd = StatedAnimUI_Combo_Add_State_Type.SelectedValue
	ObjectsToAdd = Undefined
	State = "Animation"
	
	case TypeToAdd of
	(
		"Animation":
		(
			ObjectsToAdd = SelectByName Title:"Add animation objects..." Filter:IsValidForPick ShowHidden:True
			State = "Animation"
		)
		"IMAP Start":
		(
			IMAPName = StatedAnimUI_Combo_SelectedStatedAnim.SelectedItem + "_START"
			IMAPGroupName = StatedAnimUI_Combo_SelectedStatedAnim.SelectedItem + "_IMAP_START"
			ObjectsToAdd = #( StatedAnimIPLDummy Name:IMAPGroupName IMAPName:IMAPName)
			State = "Start"
		)
		"IMAP End":
		(
			IMAPName = StatedAnimUI_Combo_SelectedStatedAnim.SelectedItem + "_END"
			IMAPGroupName = StatedAnimUI_Combo_SelectedStatedAnim.SelectedItem + "_IMAP_END"
			ObjectsToAdd = #( StatedAnimIPLDummy Name:IMAPGroupName IMAPName:IMAPName )
			State = "End"
		)
		"Mesh Start":
		(
			ObjectsToAdd = SelectByName Title:"Add mesh start states..." Filter:IsValidForPick ShowHidden:True
			State = "Start"
		)
		"Mesh End":
		(
			ObjectsToAdd = SelectByName Title:"Add mesh start states..." Filter:IsValidForPick ShowHidden:True
			State = "End"
		)
	)
	
	if( ObjectsToAdd != Undefined) then
	(
		for Obj in ObjectsToAdd do
		(
			NewStatedAnimObject = DotNetObject "StatedAnim.StatedAnimObject" Obj.Name State Obj.Handle
			StatedAnimObjectList.AddRegister( NewStatedAnimObject )
		)
	)

)
fn RemoveStateClicked =
(
	ToRemove = StatedAnimUI_ListBox_GroupMembers.SelectedItem
	
	if( ToRemove != Undefined ) then
	(
		StatedAnimObjectList.Remove ToRemove
	)
)
fn ListDoubleClick =
(
	Selected = StatedAnimUI_ListBox_GroupMembers.SelectedItem
	if(Selected != Undefined) then
	(
		TargetNode = MaxOps.GetNodeByHandle Selected.NodeHandle
		ClearSelection()
		Select TargetNode
	)
)
fn CreateProxy =
(
	SelectedStatedAnim = StatedAnimUI_Combo_SelectedStatedAnim.SelectedItem
	StatedAnimCore.CreateProxy SelectedStatedAnim 
)
/*---------------------------------------------------
Register handlers
*/---------------------------------------------------
DotNet.AddEventHandler StatedAnimUI_Combo_SelectedStatedAnim "DropDownOpened" PopulateValidStatedAnimGroups
DotNet.AddEventHandler StatedAnimUI_Combo_SelectedStatedAnim "SelectionChanged" SelectedStatedAnimChanged
DotNet.AddEventHandler StatedAnimUI_Combo_Selected "DropDownOpened" PopulateValidSelectionSets	
DotNet.AddEventHandler StatedAnimUI_Combo_Selected "SelectionChanged" SSetSelectionChanged	
DotNet.AddEventHandler StatedAnimUI_Button_Create "Click" CreateStatedAnim
DotNet.AddEventHandler StatedAnimUI_Button_Add_State "Click" AddStateClicked
DotNet.AddEventHandler StatedAnimUI_Button_Remove_State "Click" RemoveStateClicked
DotNet.AddEventHandler StatedAnimUI_Button_Create_Proxy "Click" CreateProxy
DotNet.AddEventHandler StatedAnimUI_ListBox_GroupMembers "MouseDoubleClick" ListDoubleClick
DotNet.AddEventHandler StatedAnimObjectList "ContentChanged" ChangeState
DotNet.AddEventHandler StatedAnimObjectList "CollectionChanged" ChangeGroup


