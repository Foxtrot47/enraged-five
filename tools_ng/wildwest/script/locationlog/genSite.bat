set CurrentDate=%1
set DLLDir=%RS_BUILDBRANCH%\common\data
xmlstarlet tr DebugLocationSelection.xsl -s currentDate=%CurrentDate% %DLLDir%\debugLocationList.xml  > %CurrentDate%\index.html
xmlstarlet tr GeneratedDebugLocFile.xsl -s platform=360 -s currentDate=%CurrentDate%  %DLLDir%\debugLocationList.xml  > %CurrentDate%\pages_360.html
xmlstarlet tr GeneratedDebugLocFile.xsl -s platform=PS3 -s currentDate=%CurrentDate% %DLLDir%\debugLocationList.xml  > %CurrentDate%\pages_PS3.html
xmlstarlet tr GeneratedDebugLocFile.xsl -s platform=Concept -s currentDate=%CurrentDate% %DLLDir%\debugLocationList.xml  > %CurrentDate%\pages_Concept.html
