<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="yes" version="4.0"/>
	<xsl:template match="/">

	<html>
	<head>
		<link href="../common/style.css" rel="stylesheet" type="text/css"/>
		<title>Location Log Menu</title>
	</head>
	<link rel="shortcut icon" href="http://rsgediwiki1/rockstargallery/lib/images/favicon.ico" type="image/x-icon" />
      <body>
 <xsl:copy-of select="document('indexfragment.xsl')"/> 
  <div id="Header">Location Log Menu</div>
  
<div id="Content">
	
	<xsl:for-each select="debugLocationList/locationList/Item">
		<span style="float:left;">
		<a><xsl:attribute name="href">pages_360.html#<xsl:value-of select="name" /></xsl:attribute>		
			
			<img>
				<xsl:attribute name="title"><xsl:value-of select="name" /></xsl:attribute>
				<xsl:attribute name="width">320</xsl:attribute>
				<xsl:attribute name="hieght">240</xsl:attribute>
				<xsl:attribute name="src">X:/gta5/build/dev/autoCapture.360.<xsl:value-of select="$currentDate" />.<xsl:value-of select="name" />.jpg</xsl:attribute>
			</img>		
			
		</a>
		</span>
    </xsl:for-each> 

		</div>
		
			<div id="Menu" >
	<xsl:for-each select="debugLocationList/locationList/Item">
		<a ><xsl:attribute name="href">pages_360.html#<xsl:value-of select="name" /></xsl:attribute>		
			<xsl:value-of select="name" />
		</a><br />
	</xsl:for-each> 
	</div>
	
      </body>
    </html>
	
</xsl:template>
</xsl:stylesheet>