<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" indent="yes" />
<xsl:template match="/">

	  <ul id="tabnav">
		<xsl:for-each select="CaptureDates/Date">
				<li class="tab1">
					<a ><xsl:attribute name="href">../<xsl:value-of select="." />/index.html</xsl:attribute><xsl:value-of select="." /></a>
				</li>
		 </xsl:for-each> 
		</ul>
</xsl:template>
</xsl:stylesheet>