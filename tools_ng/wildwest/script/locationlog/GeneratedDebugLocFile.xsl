<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="yes" version="4.0"/>
<xsl:template match="/">
    <html>
	<head>
		<link href="../common/style.css" rel="stylesheet" type="text/css"/>
		<title>Location Log</title>
		<link rel="shortcut icon" href="http://rsgediwiki1/rockstargallery/lib/images/favicon.ico" type="image/x-icon" />
	</head>
      <body>
   <xsl:copy-of select="document('indexfragment.xsl')"/> 
  <div id="Header">Location Log</div>
  <div id="Content">
	<xsl:for-each select="debugLocationList/locationList/Item">
		
		<a><xsl:attribute name="name"><xsl:value-of select="name" /></xsl:attribute>	
			<h1><xsl:value-of select="name" /></h1>
		</a>
		<ul id="tabnav">
			<li class="tab1">
				<a ><xsl:attribute name="href">pages_360.html#<xsl:value-of select="name" /></xsl:attribute>360</a>
			</li>
			<li class="tab2">
				<a ><xsl:attribute name="href">pages_PS3.html#<xsl:value-of select="name" /></xsl:attribute>PS3</a>			
			</li>
			<li class="tab3">
				<a ><xsl:attribute name="href">pages_Concept.html#<xsl:value-of select="name" /></xsl:attribute>Concept</a>			
			</li>
		</ul>
		<div id="360Images" >
		<img>
			<xsl:attribute name="width">960</xsl:attribute>
			<xsl:attribute name="hieght">540</xsl:attribute>
			<xsl:attribute name="src">X:/gta5/build/dev/autoCapture.<xsl:value-of select="$platform" />.<xsl:value-of select="$currentDate" />.<xsl:value-of select="name" />.jpg</xsl:attribute>
		</img>
		</div>
			<xsl:for-each select="desc">
			<h2>Purpose</h2>
			<p> <xsl:value-of select="purpose" /></p>
			<h2>Tests</h2>
			<p> <xsl:value-of select="tests" /></p>
			<h2>Improvements Required</h2>
			<p><xsl:value-of select="required" /></p>
			<xsl:for-each select="bug">
				<a>
					<xsl:attribute name="href">bugstar:<xsl:value-of select="." /></xsl:attribute>
					BUG <xsl:value-of select="." />
				</a>, 
			</xsl:for-each>
		</xsl:for-each>
		
		<hr/>
			
    </xsl:for-each> 
	</div>
	
	
	<div id="Menu" >
	<xsl:for-each select="debugLocationList/locationList/Item">
		<a ><xsl:attribute name="href">#<xsl:value-of select="name" /></xsl:attribute>
			<xsl:value-of select="name" />
		</a><br />
	</xsl:for-each> 
	</div>

      </body>
    </html>
</xsl:template>
</xsl:stylesheet>