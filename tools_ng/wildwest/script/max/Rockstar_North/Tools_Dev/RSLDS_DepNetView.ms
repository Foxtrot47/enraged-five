-- DependencyNetView
-- by Neal D Corbett, R*Leeds
-- 04/05/2010

try (DestroyDialog DependencyNetView) catch ()

global depNodesSearchList

rollout DependencyNetView "Dependency-Net View" width:240
(	
	fn ForwardSlasher inString = 
	(
		local StringArray = filterString inString "/\\"
		local OutString = ""
		if ((findString "/\\" inString[1]) != undefined) do (append OutString "/") -- Add starting slash
		append OutString StringArray[1]
		for n = 2 to StringArray.count do (append OutString ("/" + StringArray[n])) -- Add betweener slashes
		if ((findString "/\\" inString[inString.count]) != undefined) do (append OutString "/") -- Add ending slash
		outString
	)

	fn GetSubDirFiles ThisDir Filenames = 
	(
		local SearchFiles = #()
		for Filename in Filenames do 
		(
			join SearchFiles (
				for File in (getFiles (ThisDir + Filename)) collect 
				(
					ForwardSlasher File
				)
			)
		)
		for SubDir in (getDirectories (ThisDir + "*.*")) do (join SearchFiles (GetSubDirFiles SubDir Filenames ))
		SearchFiles
	)

	button SelectUpBtn "Select node Parents" width:160 tooltip:"Add parents of selected dependency-net nodes to selection"
	button SelectDownBtn "Select node Children" width:160 offset:[0,-4] tooltip:"Add children of selected dependency-net nodes to selection"
	button SelectNodeHelpersBtn "Select node helpers" width:160 tooltip:"Select helper-objects associated with selected dependency-net nodes (tapes, springs,etc)"
	button SelectOnlyLeavesBtn "Unselect non-leaf nodes" width:160 offset:[0,-4] tooltip:"Remove dependency-net nodes that load other nodes from the selection"
	button DeleteNodeBtn "Delete selected nodes" width:160 tooltip:"Deletes the selected dependency-net nodes, plus any associated helpers, and removes them from reactor-sim list"
	button OpenFileBtn "Open selected nodes' files" width:160 tooltip:"Opens files represented by selected dependency-net nodes in the script-editor"	
	
	group "" 
	(
		button GenDepNetBtn "Generate/Update Dependency Net" offset:[0,-6] tooltip:"Generates/updates objects representing file dependency network.\n\nIf nodes already exist, their positions are retained, and only the connections are changed.\n\nSingleton nodes (files that are not loaded by anything, and don't load anything themselves) are not displayed."
		checkbox SetStartsUnyieldingBtn "Set new start-nodes as fixed" checked:true tooltip:"Sets new start-nodes as immovable, so they don't move around during Reactor simulation.\n\nAny nodes can be set as unyielding after they've been created, if you don't want them moving any more."
		checkbox AddConnectSpringsBtn "Add connection-springs" checked:false tooltip:"Add Reactor springs between connected nodes, to aid laying them out.\n\nEach node will attempt to maintain an equal distance to each of its connected nodes.\n\nTurn this off for faster network-updating."
		checkbox AddRepellerSpringsBtn "Add repeller-springs" enabled:false checked:false tooltip:"Add repelling Reactor springs between unconnected nodes, to avoid overlaps, aiding layout.\n\nThe number of these connections increases exponentially with number of nodes, so they're a bad idea for larger networks."
		button SimulateSpringsBtn "Run Reactor Simulation" tooltip:"Runs Reactor simulation for current scene, then deletes all keyframes and re-aligns nodes to face upwards"
	)
	-- Turning off connector-springs also turns off repellers
	on AddConnectSpringsBtn changed State do 
	(
		AddRepellerSpringsBtn.enabled = AddRepellerSpringsBtn.checked = State
	)
	group "Generator file-read settings" 
	(
		label PresetsLbl "Presets:" align:#left
		
		button GTA5MaxMapPresetBtn "GTA5 Map.ms" align:#right offset:[0,-20]
		button GTA5RubyProjectPresetBtn "GTA5 Projects.rb" align:#right
		button GTA5MaxPresetBtn "All GTA5 Maxscript/Macros" align:#right
		button GTA5RubyPresetBtn "All GTA5 Ruby-scripts" align:#right
		radiobuttons FileTypeCtrl "File-type:" default:1 align:#left offset:[0,-6] labels:#("MaxScript [.m*]",	"Ruby [.rb]")
		comboBox ReadFilesCtrl "Start-files:" width:214 selection:3 height:5 items:\
		#(
			RsConfigGetToolsDir() + "dcc/current/max2012/scripts/rockstar/export/map.ms",
			RsConfigGetToolsDir() + "lib/pipeline/config/projects.rb",
			RsConfigGetToolsDir() + "dcc/current/max2012/scripts/startup/*.ms", RsConfigGetToolsDir() + "dcc/current/max2012/UI/MacroScripts/*.mcr",
			RsConfigGetToolsDir() + "lib/*.rb"
		)
		comboBox ScriptDirCtrl "Default scripts-path:" width:214 selection:1 height:3 items:\
		#(
			RsConfigGetToolsDir() + "dcc/current/max2012/scripts/",
			RsConfigGetToolsDir() + "lib/"
		)
	)
	local ReadMaxscript = True
	local ReadRuby = False
	fn SetScriptType = 
	(
		ReadMaxscript = ReadRuby = False
		case FileTypeCtrl.state of 
		(
			1: (ReadMaxscript = True)
			2: (ReadRuby = True)
		)
	)
	on FileTypeCtrl changed Value do (SetScriptType())
	on GTA5MaxMapPresetBtn pressed do 
	(
		ReadFilesCtrl.selection = 1
		ScriptDirCtrl.selection = FileTypeCtrl.state = 1
		SetScriptType()
	)
	on GTA5RubyProjectPresetBtn pressed do 
	(
		ReadFilesCtrl.selection = 2
		ScriptDirCtrl.selection = FileTypeCtrl.state = 2
		SetScriptType()
	)
	on GTA5MaxPresetBtn pressed do 
	(
		ReadFilesCtrl.selection = 3
		ScriptDirCtrl.selection = FileTypeCtrl.state = 1
		SetScriptType()
	)
	on GTA5RubyPresetBtn pressed do 
	(
		ReadFilesCtrl.selection = 4
		ScriptDirCtrl.selection = FileTypeCtrl.state = 2
		SetScriptType()
	)

	on SelectOnlyLeavesBtn pressed do 
	(
		local Sel = 
		(
			for obj in selection where ((isKindOf obj Editable_Poly) and (obj.children.count == 1)) collect obj
		)
		clearselection()
		select Sel
	)
	
	on GenDepNetBtn pressed do 
	(
		undo off
		(
			local timestart = timestamp()
			local FileNodes = #()
			depNodesSearchList = #()
			try (DestroyDialog RSLProgressDialog) catch ()
			
			-- Set up scene if it's empty
			if (objects.count == 0) do 
			(
				resetMaxFile #noPrompt
				viewport.setGridVisibility #all off
				viewport.setType #view_top
				max tool maximize
			)
--			reactor.updateViewports = True
--			reactor.substeps = 20
--			reactor.linearDrag = 10
--			reactor.angularDrag = 10
--			reactor.gravity = [0,0,0]
			
			local ScriptsDir = ForwardSlasher ScriptDirCtrl.text
			local MacroDir = substituteString (symbolicPaths.getPathValue "$userMacros") "\\" "/"
			local FilesToRead = #()
			local FileNodeNames = #()
			
			for DirBit in (filterString (ForwardSlasher ReadFilesCtrl.text) ";") do 
			(
				local ReadDir = ForwardSlasher (getFilenamePath DirBit)
				for Filename in (GetSubDirFiles ReadDir #(filenameFromPath DirBit)) do 
				(
					append FilesToRead Filename
					local FileNodeName = case of 
					(
						(matchPattern ReadDir pattern:(ScriptsDir + "*")):(substring Filename (ScriptsDir.count + 1) -1)
						(matchPattern ReadDir pattern:(MacroDir + "*")):("macroscripts" + (substring Filename (MacroDir.count + 1) -1))
						Default:Filename
					)
					append FileNodeNames FileNodeName
/*
					print ReadDir
					print ScriptsDir
					print FileNodeName
*/
				)
			)

			--join FilesToRead (getFiles (ScriptsDir + "RS_startup/*.ms") + getFiles (ScriptsDir + "RS_startup/etc/*.ms"))
			--join FilesToRead (getFiles (ScriptsDir + "startup/*.ms"))
				--join FilesToRead #(ScriptsDir + "rockstar/export/map.ms")

			local StartFileCount = FileNodeNames.count

			delete helpers
--			local BodyCollection = RBCollection isHidden:True

			local NodeConnects = #()
			local ParentNodes = #()
			local LoadOrders = #()

			local n = 0
			ProgressStart "Reading Files"
			local KeepGoing = True
		
			while ((n < FilesToRead.count) and KeepGoing) do
			(
				KeepGoing = ProgressUpdate (100.0 * n/FilesToRead.count)
				n += 1
				
				local Filename = FilesToRead[n]
				local FileNodeName = FileNodeNames[n]
				if (NodeConnects[n] == undefined) do (NodeConnects[n] = #{}; ParentNodes[n] = #())
				append LoadOrders #()
				
				local SearchFile = openfile Filename mode:"rtS"

				local CallFile, CallFileName, IsFileIn
				while not eof SearchFile and KeepGoing do
				(
					KeepGoing = not getProgressCancel()
					local ThisLine = trimLeft (readLine SearchFile)
					if ReadMaxscript and (matchPattern ThisLine pattern:"*RsSettingsRoll*") do (appendIfUnique depNodesSearchList FileNodeName)
					if (
						case of 
						(
							ReadMaxscript:
							(
								(not (matchPattern ThisLine pattern:"--*")) and 
								(not (matchPattern ThisLine pattern:"format ")) and 
								(
									(matchPattern ThisLine pattern:"fileIn *") or 
									(matchPattern ThisLine pattern:"* fileIn *") or 
									(matchPattern ThisLine pattern:"*(fileIn *") or 
									(matchPattern ThisLine pattern:"include *") or 
									(matchPattern ThisLine pattern:"(include *") or 
									(matchPattern ThisLine pattern:"* include *") or 
									((not matchPattern ThisLine pattern:"fn *") and (matchPattern ThisLine pattern:"*fileInTimer *")) 
								)
							)
							ReadRuby:
							(
								(not (matchPattern ThisLine pattern:"#*")) and 
								(
									(matchPattern ThisLine pattern:"require *") or 
									(matchPattern ThisLine pattern:"* require *") 
								)
							)
						)
					) do 
					(
						ThisLine = filterString ThisLine (if ReadRuby then "'" else "\"")
						if (ThisLine.count >= 2) then  
						(
							CallFileName = ForwardSlasher ThisLine[2]
							--if (matchPattern ThisLine[1] pattern:"*RsConfigGetWildWestDir()*") then (CallFile = RsConfigGetWildWestDir() + CallFileName) else 
							
							local DefaultExtension = "ms"
							if ReadRuby do (DefaultExtension = "rb")
							
							 -- Add extension if missing
							local FileExtension = getFilenameType CallFileName
							case FileExtension of 
							(
								(""):(CallFileName += ("." + DefaultExtension))
								("."):(CallFileName += DefaultExtension)
							)
							
							CallFile = if (doesFileExist CallFileName) then CallFileName else (ScriptsDir + CallFileName)
						) else (CallFile = CallFileName = undefined)
						
--							if (matchPattern CallFileName pattern:"RSwildwest/rswildwest.ms") 
--							do (format "WHAT?!? %, %, %, %\n" Filename CallName CallFilename (ThisLine as string))

						if ((CallFile != undefined) and (doesFileExist CallFile)) then 
						(
							local CalledFileNum = findItem FileNodeNames CallFileName

							if (CalledFileNum == 0) do 
							(
								append FilesToRead CallFile
								append FileNodeNames CallFileName
								CalledFileNum = FileNodeNames.count
								NodeConnects[CalledFileNum] = #{}
								ParentNodes[CalledFileNum] = #()
								
								--format "%: % -> %\n " CalledFileNum Filename CallFile
							)
							
							append LoadOrders[n] CalledFileNum
							NodeConnects[n][CalledFileNum] = True
							if (ParentNodes[CalledFileNum] == undefined) 
								then (ParentNodes[CalledFileNum] = #(n))
								else (append ParentNodes[CalledFileNum] n)
						) else 
						(
							format "NOT FOUND, from %: %\n" (getFiles Filename)[1] (ThisLine as string)
						) 
					)
				)
				close SearchFile
			)

			if KeepGoing do -- Don't continue if earlier section was cancelled
			(
				ProgressEnd(); ProgressStart "Connection cleanup"; ProgressUpdate 100.0
				local NodeCount = FileNodeNames.count
				local NodeWeights = for n = 1 to NodeCount collect 0
				local FullConnects = for n = 1 to NodeCount collect #{}
				local PlacedNodes = #{}
				local NodePositions = #()
				for n = 1 to NodeCount do 
				(
					local ExistingNode = getNodeByName FileNodeNames[n]
					if (isValidNode ExistingNode) do 
					(
						PlacedNodes[n] = True
						NodePositions[n] = ExistingNode.pos
					)
					for m = NodeConnects[n] do 
					(
						if (n != m) do 
						(
							FullConnects[n][m] = FullConnects[m][n] = True
						)
						NodeWeights[n] += 1
						NodeWeights[m] += 1
						NodeConnects[m][n] = False
					)
				)

				local PlaceX = 0
				local PlaceY = 0
				local PosStep = 200.0
				
				local KeepGoing = True
				local WhileCount = 0
				local NoneChangedLastTurn = False
				
				ProgressEnd(); ProgressStart "Arranging new nodes"; ProgressUpdate 100.0

				for n = 1 to NodeCount where (not PlacedNodes[n]) do 
				(
					local UsePos
					case of 
					(
						(n <= StartFileCount):
						--((n == 1) or (ParentNodes[n][1] == undefined)):
						(
							UsePos = [0,0,0]
						)
						(NodePositions[ParentNodes[n][1]] != undefined):
						--Default:
						(
							local ThisAngle = ((-180.0 / LoadOrders[ParentNodes[n][1]].count) * ((findItem LoadOrders[ParentNodes[n][1]] n) - 1)) + 90
							UsePos = NodePositions[ParentNodes[n][1]] + (500 * [cos ThisAngle, sin ThisAngle, 0])
						)
					)
					
					if (UsePos != undefined) do 
					(
						PlacedNodes[n] = True
						local PlaceMe = False
						while not PlaceMe do 
						(
							PlaceMe = True
							UsePos += random [100,0,0] [200, 200, 0]
							for m = 1 to NodeCount while PlaceMe where (NodePositions[m] != undefined) do 
							(
								PlaceMe = ((distance UsePos NodePositions[m]) > 200)
							)
						)
						NodePositions[n] = UsePos
					)
				)
				
				ProgressEnd(); ProgressStart "Creating connections"; ProgressUpdate 100.0

				for n = 1 to NodeCount where (not PlacedNodes[n]) do 
				(
					NodePositions[n] = random [4000,-1000,0] [5000, 1000, 0]
					PlacedNodes[n] = True
				)

				local FileNodeList = #()
				local BaseTextObj = Text size:6 steps:0 font:"Aharoni Bold" 
				for n = 1 to NodeCount do 
				(
					local ThisNode = getNodeByName FileNodeNames[n]
					if (ThisNode == undefined) do 
					(
						ThisNode = copy BaseTextObj
						ThisNode.name = ThisNode.Text = FileNodeNames[n]
						convertToPoly ThisNode
						
						ThisNode.pos = NodePositions[n] --random [-1000,-1000,-0] [1000,1000,0]
					)
					--setUserProp ThisNode "Disable_Collisions" 1
					setUserProp ThisNode "Simulation_Geometry" 1
					setUserProp ThisNode "Mass" (1 + (NodeWeights[n] ^ 2))
					if (SetStartsUnyieldingBtn.checked and (ParentNodes[n].count == 0) and (FullConnects[n].numberSet != 0)) do (setUserProp ThisNode "Unyielding" 1)
					append FileNodes ThisNode
				)
				delete BaseTextObj

--				for Obj in geometry do (BodyCollection.AddRigidBody Obj)
				geometry.rotation = quat 0 0 0 0
				geometry.pos.z = 0

				--delete (for Obj in helpers where ((isKindOf Obj SpringHelper) and (Obj.childBody == undefined)) collect Obj)

				local TapeStandOff = 20				
				local TapeOffset = [0,0,-5]
				local StartNode, EndNode
				local CallType = "LOAD:"
				for n = 1 to NodeCount do 
				(
					StartNode = FileNodes[n]
					
					local LoadsFiles = (LoadOrders[n].count != 0)
					local NodesAngle = -180.0 / LoadOrders[n].count
					case of 
					(
						(FullConnects[n].numberSet == 0):(StartNode.wireColor = Green) -- Singleton nodes
						((ParentNodes[n].count == 0) and (FullConnects[n].numberSet != 0)):(StartNode.wireColor = Yellow) -- Initially-loaded nodes
						(not LoadsFiles):(StartNode.wireColor = Red) -- Leaf nodes
						Default:(StartNode.wireColor = Blue + Green)
					)
/*
					if (StartNode.name == "pipeline/log/rollingdatefileoutputter.rb") do 
					(
						format "OHO!\n"
						for m in LoadOrders[n] do (format "\t%\n" FileNodes[m].name)
						format "Connects: %\n" NodeConnects[n]
					) 
*/
					for m = 1 to NodeCount where (n != m) do 
					(
						EndNode = FileNodes[m]

						--local CallType = if fileIns[m] then "_fileIn:" else "_include:"
						if NodeConnects[n][m] do 
						(
							if AddConnectSpringsBtn.checked do 
							(
								local NewSpring = Springhelper hasParent:True isHidden:True childBody:EndNode parentBody:StartNode
								NewSpring.restitution = 30
								NewSpring.damping = 10
								NewSpring.restLength = 600 --10 * (NodeWeights[n] + NodeWeights[m])
							)
							
							local LoadNum = findItem LoadOrders[n] m
							local ThisAngle = (NodesAngle * (LoadNum - 1)) + 90
							local TapePos = StartNode.pos + TapeOffset + (TapeStandOff * [cos ThisAngle, sin ThisAngle, 0])
								
							-- Draw tape-helper
							local SubNodeLine = Tape name:(uniquename (CallType + EndNode.name)) pos:TapePos target:(targetObject pos:(EndNode.pos + TapeOffset))
								
							SubNodeLine.parent = StartNode
							--SubNodeLine.isFrozen = True
							SubNodeLine.target.parent = EndNode
							SubNodeLine.target.isFrozen = True
							
							--if (EndNode.parent == undefined) do (EndNode.parent = StartNode)
						)

						if AddRepellerSpringsBtn.checked do 
						(
							if ((not FullConnects[n][m]) and (n < m) and (FullConnects[n].numberSet != 0)) do 
							(
/*
								-- Strong repeller-spring
								local NewSpring = Springhelper hasParent:True isHidden:True childBody:StartNode parentBody:EndNode
								NewSpring.restitution = 3
								NewSpring.damping = 1
								NewSpring.restLength = 2000
								NewSpring.onExtension = False
*/										
								-- Weak repeller-spring
								local NewSpring = Springhelper hasParent:True isHidden:True childBody:StartNode parentBody:EndNode
								NewSpring.restitution = 7
								NewSpring.damping = 1
								NewSpring.restLength = 600 --10 * (NodeWeights[n] + NodeWeights[m])
								NewSpring.onExtension = False
/*
								-- Puller-spring
								local NewSpring = Springhelper hasParent:True isHidden:True childBody:StartNode parentBody:EndNode
								NewSpring.restitution = 1
								NewSpring.damping = 1
								NewSpring.restLength = 1000
								NewSpring.onCompression = False
*/
							)
						)
					)
				)
				delete (for Obj in geometry where ((not (matchPattern Obj.name pattern:"Box??")) and (Obj.children.count == 0)) collect Obj)
				delete (for obj in $Spring* where (not ((isValidNode obj.childBody) and (isValidNode obj.parentBody))) collect obj)

				--max tool zoomextents all
				
				if FALSE then
				(
					reactor.CreateAnimation True
					sliderTime = 100f
					geometry.rotation = quat 0 0 0 0
					--max tool zoomextents all

					selectKeys geometry (interval 0f 99f)
					deleteKeys geometry #selection
					deleteKeys geometry #allKeys

					delete $Spring*
				)
			)
			
			ProgressEnd()
			format "That took: % seconds\n" (0.001 * (Timestamp() - TimeStart))
		)
	)
	
	on SelectUpBtn pressed do 
	(
		local Selecteds = for obj in selection where (isKindOf obj Editable_Poly) collect obj
		local ChildObjs = for ThisTape in helpers where 
		(
			(isKindOf ThisTape Tape) and 
			(isValidNode ThisTape.target.parent) and 
			((findItem Selecteds ThisTape.target.parent) != 0)
		) do (selectMore ThisTape.parent)
	)
	
	on SelectDownBtn pressed do 
	(
		local Selecteds = for obj in selection where (isKindOf obj Editable_Poly) collect obj
		for ThisTape in helpers where 
		(
			(isKindOf ThisTape Tape) and 
			(isValidNode ThisTape.parent) and 
			((findItem Selecteds ThisTape.parent) != 0)
		) do (selectMore ThisTape.target.parent)
	)
	
	on OpenFileBtn pressed do 
	(
		local Selecteds = for obj in selection where (isKindOf obj Editable_Poly) collect obj
		local NodeFilenames = for obj in Selecteds collect (filenameFromPath obj.name)
		
		-- Build list of places where scripts are to be found
		local AllSearchDirs = #(RsConfigGetWildwestDir())
		for Item in ScriptDirCtrl.items do (append AllSearchDirs Item)
		for Item in ReadFilesCtrl.items do 
			(for SubItem in (filterString Item ";") do (append AllSearchDirs (getFilenamePath SubItem)))
		append AllSearchDirs ScriptDirCtrl.text
		
		local SearchDirs = #()
		for Item in AllSearchDirs where ((findItem SearchDirs Item) == 0) do (append SearchDirs Item)
		
		local ScriptFiles = #()
		for DirName in SearchDirs do (join ScriptFiles (GetSubDirFiles DirName NodeFilenames))
	
		for obj in Selecteds do 
		(
			local KeepLooking = True
			for File in ScriptFiles while KeepLooking where (matchpattern File pattern:("*" + obj.name)) do 
			(
				edit File
				KeepLooking = False
			)
		)
	)
	
	on DeleteNodeBtn pressed do 
	(
		local Selecteds = for obj in selection where (isKindOf obj Editable_Poly) collect obj
		local Deletes = deepCopy Selecteds
		for Obj in helpers where 
		(
			(
				(isKindOf Obj Tape) and 
				(
					((findItem Selecteds Obj.parent) != 0) or  
					((findItem Selecteds Obj.target.parent) != 0)
				)
			) or 
			(
				(isKindOf Obj SpringHelper) and 
				(
					((findItem Selecteds Obj.parentBody) != 0) or  
					((findItem Selecteds Obj.childBody) != 0)
				)
			)
		) do (append Deletes Obj)
		delete Deletes
	)
	
	on SelectNodeHelpersBtn pressed do 
	(
		local Selecteds = for obj in selection where (isKindOf obj Editable_Poly) collect obj
		local ToSelect = deepCopy Selecteds
		for Obj in helpers where 
		(
			(isKindOf Obj Tape) and 
			(
				((findItem Selecteds Obj.parent) != 0) or  
				((findItem Selecteds Obj.target.parent) != 0)
			)
		) do (append ToSelect Obj)
		select ToSelect
	)
	
	on SimulateSpringsBtn pressed do 
	(
		undo off 
		(
			delete (for obj in $Spring* where (not ((isValidNode obj.childBody) and (isValidNode obj.parentBody))) collect obj)
			$'Spring*'.isHidden = True
			$'Angular*'.isHidden = True
			$'RBCollection*'.isHidden = True
			$'Target*'.isFrozen = True
				
			for ThisNode in geometry do (ThisNode.pos.z = 0; setUserProp ThisNode "Simulation_Geometry" 1)

				
--			reactor.CreateAnimation True	
			
			sliderTime = 100f
			geometry.rotation = quat 0 0 0 0
			geometry.pos.z = 0
			--max tool zoomextents all

			selectKeys geometry (interval 0f 99f)
			deleteKeys geometry #selection
			deleteKeys geometry #allKeys
				
			--delete $Spring*
		)
	)
)
createDialog DependencyNetView