--renameSelectedObjects.ms
--Spet2010
--Matt Rennie
-- script to rename selected objects to a name supplied by the user


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())


global renameSelection
global textVal = ""

fn renameSelected nameToRenameTo =
(
	myArray = selection as array
	for i = 1 to myArray.count do
	(
		print ("renaming "+myArray[i].name+" to "+nameToRenameTo+"_"+(i as string))
		myArray[i].name = nameToRenameTo+"_"+(i as string)
	)
	print "Renaming complete."

)

fn renameUI = 
(
	textVal = ""
	rollout renameSelection ""

	(
		edittext renameText "Rename to:" fieldWidth:200 labelOnTop:true width:260 height:25
		button btnOK "Rename" width:170 height:25
		
		on renameText entered txt do
		(
			if txt != "" do
			(
				textVal = txt
			)
		)
		
		on btnOK pressed do
		(
			if textVal != "" then 
			(
				renameSelected textVal
			)
			else
			(
				print "No text value specified"
			)
		)
	)

	renameSelectionFloater = newRolloutFloater "Rename Selection" 300 120

	addRollout renameSelection renameSelectionFloater

 )

 renameUI()