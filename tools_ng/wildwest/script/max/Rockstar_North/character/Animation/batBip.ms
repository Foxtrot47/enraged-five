--=========================================================================================================================
-- Batch Biped Files to FBX
-- Matt Rennie
--July 2010

--=========================================================================================================================

-- Script to batch through a folder of bip files to a user specified export folder
--the script should load the biped onto //depot/gta5/art/animation/resources/Bip_Export/Max_Player.max
--then adjust key extents to match the anim
--select all bones in the bip
--save out an fbx named the same as original bip to the output folder

-- v 0.5
--=========================================================================================================================

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())


clearListener()

pickedExportPath = undefined
--Max_Player.max not available under art/ng so using earlier path
masterBipFile = "X:/gta5/art/animation/resources/Bip_Export/Max_Player.max"
lastFrame = 0
furthestKey = 0f 
progBar -- define here so it can be destroyed elsewhere

rollout progBar "Progress..."
(
	progressbar prog pos:[10,10] color:green
)	

fn spitFbx singleBipFile = --function to spit out the fbx file
(
	--need to ensure we're only exporting what we want
	notChar = #()

	for o in objects do
	(
		if (substring o.name 1 4 ) != "Char" do
		(
			if o.name != "Dummy01" do
			(
				appendIfUnique notChar o
			)
		)
	)

	delete notChar

	--now need to strip name of bip out
	exportName = (filterstring singleBipFile "\\")
	exportNameCount = exportName.count
	singleBipFile= exportName[exportNameCount]
	exportName = (filterString singleBipFile ".")
	
	--*************
	exportPath = (pickedExportPath as string)
-- 	print ("should export to "+(exportPath+"\\"+(exportName[1]+".fbx")))
		--FBXEXporterSetParam "FileVersion" "FBX200602_MB75" --PRESUMABLY THIS GOES BEFORE WE ACTUALLY EXPORT
		FBXEXporterSetParam "FileVersion" "FBX200611" --PRESUMABLY THIS GOES BEFORE WE ACTUALLY EXPORT
		FBXEXporterSetParam "Animation" True  --PRESUMABLY THIS GOES BEFORE WE ACTUALLY EXPORT
		FBXEXporterSetParam "ShowWarnings" False --PRESUMABLY THIS GOES BEFORE WE ACTUALLY EXPORT
 	exportFile (exportPath+"\\"+(exportName[1]+".fbx")) #noPrompt 


)

fn getKeys singleBipFile = --this finds the furthest key along the timeline on objects to allow setting of timeline range
(
--this is the variable used to find the furthest key along the timeline

	lastFrame = 0
	
	clearselection()
-- 	max select all
	allBones = #($Char, 
		$'Char Pelvis', 
		$'Char L Thigh', 
		$'Char L Calf', 
		$'Char L Foot', 
		$'Char L Toe0', 
		$'Char R Thigh', 
		$'Char R Calf', 
		$'Char R Foot', 
		$'Char R Toe0', 
		$'Char Spine', 
		$'Char Spine1', 
		$'Char Spine2', 
		$'Char Spine3', 
		$'Char Neck', 
		$'Char Head', 
		$'Char L Clavicle', 
		$'Char L UpperArm', 
		$'Char L Forearm', 
		$'Char L Hand', 
		$'Char L Finger0', 
		$'Char L Finger01', 
		$'Char L Finger02', 
		$'Char L Finger1', 
		$'Char L Finger11', 
		$'Char L Finger12', 
		$'Char L Finger2', 
		$'Char L Finger21', 
		$'Char L Finger22', 
		$'Char L Finger3', 
		$'Char L Finger31', 
		$'Char L Finger32', 
		$'Char L Finger4', 
		$'Char L Finger41', 
		$'Char L Finger42', 
		$'Char R Clavicle', 
		$'Char R UpperArm', 
		$'Char R Forearm', 
		$'Char R Hand', 
		$'Char R Finger0', 
		$'Char R Finger01', 
		$'Char R Finger02', 
		$'Char R Finger1', 
		$'Char R Finger11', 
		$'Char R Finger12', 
		$'Char R Finger2', 
		$'Char R Finger21', 
		$'Char R Finger22', 
		$'Char R Finger3', 
		$'Char R Finger31', 
		$'Char R Finger32', 
		$'Char R Finger4', 
		$'Char R Finger41', 
		$'Char R Finger42'
		)
-- 	allBones = selection as array
		
	for fb in allBones do
	(
		cnt = fb.controller.keys.count
-- 			print ("cnt = "+cnt as string)
		if cnt != -1 do
		(
			biggestKeyString = (fb.controller.keys[cnt] as string)
			fromIndex = ((findString biggestKeyString "@") + 2)
			toIndex = ((biggestKeyString.count) - 3)
			keyValueStr = (substring biggestKeyString fromIndex 20)
				fromIndex = 1
				toIndex = ((findString keyValueStr "f")-1)
				keyValueStr = (substring keyValueStr fromIndex toIndex) as Integer
-- 			print ("keyValueStr = "+keyValueStr as string)
				
				--now test if keyValueStr > lastFrame and if so update lastFrame to this value
				if keyValueStr > lastFrame do
				(
					lastFrame = keyValueStr
				)
		)
	)
	
-- 	print ("lastFrame = "+(lastFrame as string))
	furthestKey = (lastFrame as time)
		if furthestKey <= 0f do 
			(furthestKey = 0f +1f)
			
-- 		messageBox ("Setting final key to "+(furthestKey as string))
	animationRange = interval 0f furthestKey
		
		spitFbx singleBipFile
)

fn exportBip singleBipFile pickedExportPath = --function to actually perform the bip options
(
	--singleBipFile is the name of the current biped file
	pickedExportPathString = pickedBipedPath as string
	
	resetMaxFile #noPrompt
	
	mergeMaxFile masterBipFile quiet:true
	
	--now load the bip file onto the skeleton
	
	biped.loadBipFile $Char.controller singleBipFile --as string)
-- 		print ("loaded "+singleBipFile+" on to $Char")
	
-- 	max select all
	
	getKeys singleBipFile
	
	
	
	-- now export the file
)


fn batBip =
(
--FIRST GET USER TO PICK THE FOLDER CONTAINING THE BIP FILES

	pickedBipedPath = getSavePath caption:"Choose source bip folder" initialDir:"X:\\" --wanted to pick a starting point intelligently but unfortunately not all animators have drives set the same
	pickedBipedPathString = pickedBipedPath as string
	
-- NOW PICK THE EXPORT FOLDER - SHOULD THIS JUST BE A SUB FOLDER OF THE ABOVE?

	pickedExportPath = getSavePath caption:"Choose destination fbx folder" initialDir:(RsConfigGetArtDir() + "anim/source_fbx")--wanted to pick a starting point intelligently but unfortunately not all animators have drives set the same
	
	
	if pickedBipedPath != undefined then
	(
		if pickedExportPath != undefined then
		(
			
			bipFiles = getFiles (pickedBipedPath+"\\*.bip")
			
			noOfBips = bipFiles.count
			for f in bipFiles do
			(
				print f
				exportBip f pickedExportPath
				iVal = finditem bipFiles f
				progBar.prog.value = (100.*iVal/bipfiles.count)
				
				if iVal == bipFiles.count do
				(
					if ((progBar != undefined) and (progBar.isDisplayed)) do
					(destroyDialog progBar)
					MessageBox "Completed Batching"
				)
			)
		)
		else
		(
			messagebox "Please try again & pick a destination location." Beep:true
		)
	)
	else
	(
		messagebox "Please try again & pick a source location." Beep:true
	)
)

		if ((progBar != undefined) and (progBar.isDisplayed)) do
		(destroyDialog progBar)
		
		CreateDialog progBar width:300 Height:30

batBip()