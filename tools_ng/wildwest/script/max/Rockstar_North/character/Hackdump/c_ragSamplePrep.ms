
fileIn "pipeline/util/file.ms"

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

global pickedITD = undefined
global characterFolder = undefined
global componentFolder = undefined
global characterName = undefined
global componentName = undefined
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



fn createBatchFile fileFolder = --creates batch file to actually run this rage sample
(
	batchFileName = ("X:\\gta5\\"+characterName+"_"+componentName+"_Sample.bat")
	batchFileName = rsMakeSafeSlashes batchFileName 
	
	files = getFiles "X:\\gta5\\"
	for f in files do
	(
		if f == batchFileName do
			deleteFile f
-- 			--print ("Deleted "+f)
	)
	
	input_name = (fileFolder +"\\entity.type")
-- 	batchFileName = "X:\\gta5\\exptest.bat"
	entityStr = (" "+"\""+input_name+"\""+" ")
	entityStr = rsMakeSafeSlashes entityStr
	print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	print ("input_name = "+input_name)
	print ("entityStr = "+entityStr)
	
	expressionPath = ("\""+"X:\\gta5\\assets\\anim\\expressions\\test\\test.expr"+"\"")
	batchStr = ("x:\\gta5\\tools\\bin\\anim\\sample_expressions.exe -dx9 -rag -zup -file" +entityStr +" -expr "+expressionPath/*+" -anim"*/)
	batchStr = rsmakesafeslashes batchStr
	print ("batchStr = "+batchStr)
	print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	
		bfile = createfile batchFileName
		format "%" (batchStr) to:bfile
		close bfile
	
-- 	shellLaunch "X:\\gta5\\tools\\bin\\rag\\rag.exe" ""
	doesRagExist = DOSCommand( "tasklist | find /I \"rag.exe\" > NUL" )
	if doesRagExist == 1 do
	(
		shellLaunch "X:\\gta5\\tools\\bin\\rag\\rag.exe" ""
	)
	DOSCommand batchFileName
)



fn renameFiles fileFolder f1 f2 = 
(
			--now rename to correct file names
		toDel = (fileFolder+"\\"+f1)
		toDel = rsmakesafeslashes toDel
		deleteFile toDel
-- 		messageBox ("check if "+toDel+" has been deleted please") beep:true
		renameFrom = (fileFolder+"\\"+f2)
		renameFrom = rsmakesafeslashes renameFrom
		renameTo = (fileFolder+"\\"+f1)
		renameTo = rsMakeSafeSlashes renameTo
-- 		print ("gonna rename "+renameFrom+" to "+renameTo)
		renameFile renameFrom renameTo
-- 		messagebox ("Please look if "+f2+" has been renamed to "+f1)
)



fn prepTextFiles componentFolder pickedITD = 
(
	
-- 	print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
-- 	print (" looking in "+componentFolder)
	files = getFiles (componentFolder+"//"+"*.type")
	for i = 1 to files.count do
	(
		foundIt = doesFileExist (componentFolder+"\\"+"entity.test") 
-- 		print ("foundIt = "+foundIt)
		if foundIt != false do
		(
			print ("Deleting an existing entity.test")
			deleteFile (componentFolder+"\\"+"entity.test")
		)
		
-- 		print ((i as string)+" = "+files[i])
		
		input_name = (componentFolder +"\\entity.type")
		output_name = (componentFolder+"\\entity.test") 
			
		if input_name != undefined then
		(
			f = openfile input_name
			output_file = createfile output_name	
			while not eof f do
			(
				currentLine = ((readLine f) as string)
				replaceThis = "ped.sps"
				replaceThisLength = replaceThis.count 
				replaceWithThis = "rage_bump_spec"
				if (findString currentLine replaceThis) != undefined then
					(
						lengthofStr = currentLine.count
						startStrIndex = (findstring currentLine replaceThis)
						startStr = ((substring currentLine 1 startStrIndex) )
						endStr = (replace currentLine startStrIndex replaceThisLength replaceWithThis)
						currentLine = (endStr+"\n")
					)
				else
					(
						currentLine = (currentLine+"\n")
					)
				format "%" (currentLine) to:output_file
			)
			close f
			close output_file
		)
		
		renameFiles componentFolder "entity.type" "entity.test"	
		--now check if we can find ped_wrinkle.sps		
		input_name = (componentFolder +"\\entity.type")
		output_name = (componentFolder+"\\entity.test") 
		if input_name != undefined then
		(
			f = openfile input_name
			output_file = createfile output_name	
			while not eof f do
			(
				currentLine = ((readLine f) as string)
				replaceThis = "ped_wrinkle.sps"
				replaceThisLength = replaceThis.count 
				replaceWithThis = "rage_bump_spec"
				if (findString currentLine replaceThis) != undefined then
					(
						lengthofStr = currentLine.count
						startStrIndex = (findstring currentLine replaceThis)
						startStr = ((substring currentLine 1 startStrIndex) )
						endStr = (replace currentLine startStrIndex replaceThisLength replaceWithThis)
						currentLine = (endStr+"\n")
					)
				else
					(
						currentLine = (currentLine+"\n")
					)
				format "%" (currentLine) to:output_file
			)
			close f
			close output_file	
		)
		
		


	)
	
	--now do the entity.textures
	
	files = getFiles (componentFolder+"//"+"*.textures")
	for i = 1 to files.count do
	(
		foundIt = doesFileExist (componentFolder+"\\"+"textures.test") 
		if foundIt != false do
		(
			print ("Deleting an existing textures.test")
			deleteFile (componentFolder+"\\"+"textures.test")
		)		
		
-- 		print ((i as string)+" = "+files[i])
		
		input_name = (componentFolder +"\\entity.textures")
		output_name = (componentFolder+"\\textures.test") 
			
		if input_name != undefined then
		(
			f = openfile input_name
			output_file = createfile output_name	
			currentLine = ""
			while not eof f do
			(
				--need to read in every line and then add the fileNom in there at the end
				cl = ((readLine f) as string)
				currentLine = (currentLine+"\r\n"+cl)
				-- 				format "%" (currentLine) to:output_file
			)
			
-- 			print ("itdName = "+itdName)
			itdName = getFilenameFile pickedITD
			currentLine = currentLine+"\r\n"+itdName 
			format "%" (currentLine) to:output_file
		)
	    close f
		close output_file	
		
	)

	renameFiles componentFolder "entity.textures" "textures.test"
	renameFiles componentFolder "entity.type" "entity.test"	
	
	createBatchFile componentFolder
)




fn getTextures = 
(
	--now we know what the mesh file is we need ot extract the itd relative to that into outputFolder

	filterMeshName = filterString componentName "_"
	
	modifiedItdName1 = filterMeshName[1]+"_"+"diff"+"_"+filterMeshName[2]+"_"+"a"+"_"+"uni.itd"
	pathToCheck = RsMakeSafeSlashes (characterFolder+modifiedItdName1 )
	found = doesFileExist pathToCheck 
	if found == true then
	(
		inputFile = modifiedItDName1
		print ("Found "+inputFile)
	)
	else
	(
		modifiedItdName2 = filterMeshName[1]+"_"+"diff"+"_"+filterMeshName[2]+"_"+"a"+"_"+"whi.itd"
		pathToCheck = RsMakeSafeSlashes (characterFolder+modifiedItdName2 )
		found = doesFileExist pathToCheck 
		if found == true then
		(
			inputFile = modifiedItDName2
-- 			print ("Found "+inputFile)
		)
		else
		(
			print ("couldnt find "+modifiedItdName1+" or "+modifiedItdName2)
		)
	)	

	pickedITD = inputFile
	if inputFile != undefined do
	(		
-- 		print ("extractItd "+inputFile+" "+finalOutputFolder)
-- 		OP = rsmakeSafeSlashes (outputFolder+"\\"+fileNom)
-- 		print ("extractItd "+inputFile+" "+OP)
-- 		outputFolder = OP
		print ("Extracting dds files from "+inputFile+" to "+componentFolder)
-- 		extractItDFile inputFile componentFolder
		
		command = ("%RS_TOOLSROOT%\\bin\\ruby\\bin\\ruby.exe %RS_TOOLSROOT%\\lib\\util\\data_extract_rpf.rb --project="+RsProjectGetName()+" --output="+componentFolder+" "+inputFile)
		DosCommand command
	)
)



fn copyFiles = 
(
	--first we need to get the dds textures
	getTextures()
	
	testPath = (characterFolder+"\\"+characterName+".textures")
	testPath  = rsmakeSafeSlashes testPath 
	found = doesFileExist testPath
	if found == true then
	(
		newFileName = (componentFolder+"\\"+"entity.textures")
		newFileName  = rsMakeSafeSlashes newFileName 
		copyFile testPath newFileName
	)
	else
	(
		messagebox ("Couldnt find "+testPath)
		print ("Couldnt find "+testPath)
	)
)



fn pickIdd = 
(
	pickedIdd = ""
-- 	pickedIdd = getOpenFileName filename:"X:\\streamGTA5\\" caption:"Pick idd file" types:"GTA idd |*.idd"
	pickedIdd = getOpenFileName filename:characterFolder caption:"Pick Component idd file" types:"GTA idd |*.idd"
	
	if pickedIdd != undefined then
	(
		componentFolder = filterstring pickedIdd "."
		componentFolder = (componentFolder[1]+"\\")
		componentFolder  = rsMakeSafeSlashes componentFolder 
		print ("pickedCharacter = "+componentFolder)
		
		myFilePath = getFilenamePath pickedIdd
		originalFilePath = (filenameFromPath pickedIdd)
		filterStr = filterString originalFilePath "."
		componentName = filterStr[1]
-- 		originalFilePath = (myFilePath+"\\"+component)
-- 		originalFilePath = RsMakeSafeSlashes originalFilePath
		
-- 		print ("originalFilePath  = "+originalFilePath )
		

-- 		outputFolder = getFilenamePath pickedIdd
-- 		print ("sending "+pickedIdd+" to "+componentFolder)
-- 		extractIddFile pickedIdd outPutFolder 
		
		print ("Extracting "+pickedIdd+" to "+characterFolder)
		RsMakeSurePathExists (componentFolder)
		DosCommand ("setEnv")
		command = ("%RS_TOOLSROOT%\\bin\\ruby\\bin\\ruby.exe %RS_TOOLSROOT%\\lib\\util\\data_extract_rpf.rb --project="+RsProjectGetName()+" --output="+characterFolder+" "+pickedIdd)
		DosCommand command
		
	)
	else
	(
		messagebox ("Please pick an idd file") beep:true
	)
)


fn pickIft = 
(
	pickedIft = ""
	pickedIft = getOpenFileName filename:"X:\\streamGTA5\\" caption:"Pick Character ift file" types:"GTA ift |*.ift"
	
	if pickedIft != undefined then
	(
-- 		filePath = filterstring pickedIdd "."
-- 		outputFolder = (filePath[1]+"\\")
-- 		filePath = filterstring pickedIdd "\\"
-- 		filePathCount = filePath.count
-- 		fP = filePath[1]
		
		characterFolder = filterstring pickedIft "."
		characterFolder = (characterFolder[1]+"\\")
		characterFolder  = rsMakeSafeSlashes characterFolder 
		print ("pickedCharacter = "+characterFolder)
		
		myFilePath = getFilenamePath pickedIft
		originalFilePath = (filenameFromPath pickedIft)
		filterStr = filterString originalFilePath "."
		characterName = filterStr[1]
		print ("characterName = "+characterName )
		
-- 		print ("originalFilePath  = "+originalFilePath )
		

-- 		characterFolder = getFilenamePath pickedIft
-- 		characterFolder = pickedCharacter
		print ("sending "+characterFolder+" to extractIdd")
-- 		extractItdFile pickedIft characterFolder 
		print ("Extracting "+pickedIft+" to "+characterFolder)
		DosCommand ("setEnv")
		print ("SetEnv run...")
		
		command = ("%RS_TOOLSROOT%\\bin\\ruby\\bin\\ruby.exe %RS_TOOLSROOT%\\lib\\util\\data_extract_rpf.rb --project="+RsProjectGetName()+" --output="+characterFolder+" "+(pickedIft))
		DosCommand command
	)
	else
	(
		messagebox ("Please pick an ift file") beep:true
	)
)

clearListener()
pickIft()
pickIdd()
copyFiles()
getTextures()
prepTextFiles componentFolder pickedITD 