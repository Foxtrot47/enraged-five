-- Forearm fix

-- Load the common maxscript functions 
zaxis = [0,-1,0]
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_common.ms"
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_Rigging.ms"
filein "X:/tools/dcc/current/max2009/scripts/rockstar/util/xml.ms"


-- Load the marker arrays
filein "X:/tools/dcc/current/max2009/scripts/character/rigging_tools/Jimmy_Rig_Bone_Tag_Mapping.dat"


boneprefix ="SKEL_" -- I prefer Skel


-- Forearms, 50% of the hand x rotation, 0 y and z 
FECTwistBone $'RB_L_ForearmRoll' (getnodebyname (boneprefix +"L_Hand")) 0.5 0 0
FECTwistBone $'RB_R_ForearmRoll' (getnodebyname (boneprefix +"R_Hand")) 0.5 0 0
		