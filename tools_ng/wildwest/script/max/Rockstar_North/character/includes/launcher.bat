@echo off

rem Run Custom Load Menu

:MENU
cls
ECHO.
ECHO   GTA V Loader v1
ECHO.
ECHO   P - Load PS3 Build
ECHO   X - Load 360 Build
ECHO.
ECHO   G - Get Current Build
ECHO   S - Show Preview Folder
ECHO   K - Kill Rag and Systray
ECHO.
ECHO   Q - Quit Launcher
ECHO.

SET /P M=  Type your selection then press ENTER : 


IF %M%==p GOTO GTA5_PS3
IF %M%==P GOTO GTA5_PS3

IF %M%==x GOTO GTA5_360
IF %M%==X GOTO GTA5_360

IF %M%==g GOTO Get
IF %M%==G GOTO Get

IF %M%==s GOTO Show
IF %M%==S GOTO Show

IF %M%==k GOTO Kill
IF %M%==K GOTO Kill

IF %M%==q GOTO Quit
IF %M%==Q GOTO Quit


:GTA5_PS3
rem Run GTA 5
cd /d X:\gta5\build\dev\
START X:\gta5\build\dev\game_psn_beta_SNC.bat -noautoload -previewfolder -nocars -nopeds
exit

:GTA5_360
rem Run GTA 5
cd /d X:\gta5\build\dev\
START X:\gta5\build\dev\game_xenon_beta.bat -noautoload -previewfolder -nocars -nopeds
exit


:Get
cd /d X:\gta5\tools\script\util\
START X:\gta5\tools\script\util\data_get_current_build.bat
GOTO Menu


:Show
explorer /root,x:\gta5\build\dev\preview
GOTO Menu

:Kill
rem Kill Rag and SysTrayRfs processes
taskkill /im SysTrayRfs.exe /f /t
taskkill /im rag.exe /f /t
GOTO Menu

:Quit
exit

exit