-- Shader functions
-- Provides useful functions for textures and shader
-- Rockstar North
-- Rick Stirling
-- Fix 17/05/10 by Stewart Wright.  Passing in arrays to make rage functions, fixed 1st run crash (hopefully).
-- Added 17/05/10 by Stewart Wright.  Added fresnel setting.

-- load the common functions
filein "rockstar/export/settings.ms"

-- Figure out the project data
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()
theProjectPedFolder = theProjectRoot + "art/peds/"

-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")

-- Set up some common variables
-- Clean this up and make these function variables instead!
maxmats=3
maxsub=17

-- Set up some arrays
componentnames = #()
textureprefix = #()
image_diff_files = #()
image_files = #()
image_type = ".tga"

-- ***************************************************************************
-- Some old GTA specific code that has nowhere else to go. 
GTAcomponentnames =#("Head", "Upper", "Lower", "Suse", "Sus2", "Hands", "Feet", "Hair 0", "Hair 1", "Hair 2", "Hair 3", "Hair 4", "Hair 5","","Teeth")
GTAtextureprefix = #("head","uppr", "lowr", "suse", "sus2", "hand", "feet","hair","hair","hair","hair","hair","hair","hair", "teef")

GTA5componentnames =#("Head", "Upper", "Lower", "Accs", "Task", "Hands", "Feet", "Hair 0", "Hair 1", "Hair 2", "Hair 3", "Beard", "JBib","Decal","Teeth","-----","Cloth")
GTA5textureprefix = #("head","uppr", "lowr", "accs", "task", "hand", "feet","hair","hair","hair","hair","berd","jbib","decl", "teef","","")
	
	
if theproject == "gta" then 
( 	
	componentnames = GTAcomponentnames
	textureprefix = GTAtextureprefix
)

if theproject == "gta5" then 
( 	
	componentnames = GTA5componentnames
	textureprefix = GTA5textureprefix
)

if theproject == "rdr3" then 
( 	
	componentnames = GTA5componentnames
	textureprefix = GTA5textureprefix
)

-- ***************************************************************************



-- Material/Shader functions
fn returnDiff diffTofind =
(
	diffToFind = uppercase diffTofind
	founddiff=""	
	for df = 1 to image_diff_files.count do
	(
		storedName = (substring (image_diff_files[df]) 1 diffToFind.count)
		if storedname == difftofind then 
		(
			print "found a match"
			founddiff = image_diff_files[df]
			print founddiff
		)
	)
	
	founddiff = founddiff + ".tga"
)



fn doesTextureExist theTexture =
(
	dfe = getFiles theTexture
	if dfe.count == 0 then theTexture =""
	
	theTexture -- return theTexture, either untouched or erased.
)



-- Create standard multisub materials to hold textures
fn create_multi_component =
(
	for mat=1 to maxmats do
	(
		matname = "Standard 00" + ((mat-1) as string)
		meditMaterials[mat].name = matname
		meditMaterials[mat] = Multimaterial numsubs:maxsub
		for sub = 1 to maxsub do 
		(
			meditMaterials[mat].names[sub] = (componentnames[sub] as string)
			meditMaterials[mat].materialList[sub].name = componentnames[sub]
		)
	)
)



fn fill_multi_component textureFolder howmanymaxmaterials = 	
(
	image_diff_files = #()

	
	-- Now get all the TGA diffuse files	
	diff_files = textureFolder  + "*diff*" + image_type
	thefiles = getfiles diff_files	
	
	for f in thefiles do	
	(
		append image_diff_files (uppercase(getfilenamefile f))	
	)
	
	-- Build probable texture paths
	for mmloop = 1 to howmanymaxmaterials do
	(	
		for tloop = 1 to (meditMaterials[mmloop].numsubs) do 
		(
			-- If the diffuse exists, then create the material
			dtx = textureFolder + returnDiff (textureprefix[tloop] + "_diff_00" + ((mmloop - 1) as string)  +"_a")

			if (doesTextureExist dtx) != "" then
			(
				stx = textureFolder + (textureprefix[tloop] + "_spec_00" + ((mmloop - 1) as string) + image_type)
				ntx = textureFolder + (textureprefix[tloop] + "_normal_00"+ ((mmloop - 1) as string) + image_type)
				
				try (meditMaterials[mmloop].materialList[tloop].diffuseMap = Bitmaptexture fileName:dtx ) catch()
				try (meditMaterials[mmloop].materialList[tloop].diffuseMap.alphaSource = 2) catch()
				try (meditMaterials[mmloop].materialList[tloop].specularLevelMap = Bitmaptexture fileName:stx) catch()
				try (meditMaterials[mmloop].materialList[tloop].specularLevelMap.alphaSource = 2) catch()
				
				try (meditMaterials[mmloop].materialList[tloop].bumpMap = Normal_Bump ()) catch()
				try (meditMaterials[mmloop].materialList[tloop].bumpMapAmount = 100) catch()
				try (meditMaterials[mmloop].materialList[tloop].bumpMap.normal_Map = Bitmaptexture fileName:ntx) catch()
				
				--settings for the renderer
				try (meditMaterials[mmloop].materialList[tloop].specularLevelMapAmount = 50) catch()
				try (meditMaterials[mmloop].materialList[tloop].glossinessMap = Bitmaptexture fileName:stx) catch()
				try (meditMaterials[mmloop].materialList[tloop].glossinessMapAmount = 50) catch()
				--alpha settings
				try (meditMaterials[mmloop].materialList[tloop].twoSided = on) catch()
				try (meditMaterials[mmloop].materialList[tloop].opacityMap = Bitmaptexture fileName:dtx ) catch()
				try (meditMaterials[mmloop].materialList[tloop].opacityMap.monoOutput = 1) catch()
				try (meditMaterials[mmloop].materialList[tloop].opacityMap.rgbOutput = 1) catch()
				try (meditMaterials[mmloop].materialList[tloop].opacityMap.alphaSource = 0) catch()					
			)
		
		) -- end sub material loop
	) -- End max material Loop
)



-- Create a multisub material
-- Takes a prop type, destination slot and number of subs per prop
fn create_multi_prop proptype matSlot numsubmats = 
(
	proparray = #()
	
	-- Try to select any props of the spe
	try 
	(
		selectbywildcard proptype
		proparray = getcurrentselection()
		propcount= proparray.count
		
		-- Create array to hold suffix
		suffixarray =#()
		for p = 1 to propcount do (
			for s = 1 to numsubmats do (
				append suffixArray (p - 1)
			)
		)
		
		-- Create a new multisub material based on the number of props and slots per prop
		meditMaterials[matSlot].name = (proptype as string)
		meditMaterials[matSlot] = Multimaterial numsubs:(propcount * numsubmats )
		
		for subMat = 1 to (propcount * numsubmats) do (
			submatname = (proptype + "_00")+ (suffixarray[subMat] as string)
			meditMaterials[matSlot].names[submat] = submatname
			meditMaterials[matSlot].materialList[subMat].name = submatname
		)
	)
		
	catch 
	(
		messagebox ("Couldn't find any props of type " + proptype)
	)
)




fn fill_multi_prop textureFolder proptype matSlot numsubmats = 
(
	image_diff_files = #()
	
	-- Now get all the p_head TGA diffuse files	
	diff_files = textureFolder  + proptype + "*diff*" + image_type
	thefiles = getfiles diff_files	
	
	for f in thefiles do	
	(
		append image_diff_files (uppercase(getfilenamefile f))	
	)
	
	
	-- how many props?
	propcount = (meditMaterials[matslot].numsubs)/numsubmats
	
	-- Create array to hold suffix
	suffixarray =#()
	for p = 1 to propcount do (
		for s = 1 to numsubmats do (
			append suffixArray (p - 1)
		)
	)
	
	-- Build the texture list
	for subMat= 1 to (propcount * numsubmats) do
	(
		-- Build probable texture paths
		phd = textureFolder + returnDiff (proptype + "_diff_00" + (suffixarray[subMat] as string) +"_a")
		phs = textureFolder + proptype + "_spec_00" + (suffixarray[subMat] as string)+ image_type
		phn = textureFolder + proptype + "_normal_00" + (suffixarray[subMat] as string)+ image_type
		
		if (doesTextureExist phd) != "" then
		(
			--rejiggedsubMat = suffixarray[subMat]
			
			meditMaterials[matSlot].materialList[subMat].diffuseMap = Bitmaptexture fileName:phd
			meditMaterials[matSlot].materialList[subMat].diffuseMap.alphaSource = 2
			meditMaterials[matSlot].materialList[subMat].specularLevelMap = Bitmaptexture fileName:phs
			meditMaterials[matSlot].materialList[subMat].specularLevelMap.alphaSource = 2
			
			try (meditMaterials[matSlot].materialList[subMat].bumpMap = Normal_Bump ()) catch()
			try (meditMaterials[matSlot].materialList[subMat].bumpMapAmount = 100) catch()
			try (meditMaterials[matSlot].materialList[subMat].bumpMap.normal_Map = Bitmaptexture fileName:phn) catch()
				
			--settings for the renderer
			try (meditMaterials[matSlot].materialList[subMat].specularLevelMapAmount = 50) catch()
			try (meditMaterials[matSlot].materialList[subMat].glossinessMap = Bitmaptexture fileName:phs) catch()
			try (meditMaterials[matSlot].materialList[subMat].glossinessMapAmount = 50) catch()
				
			--alpha settings
			try (meditMaterials[matSlot].materialList[subMat].twoSided = on) catch()
			try (meditMaterials[matSlot].materialList[subMat].opacityMap = Bitmaptexture fileName:dtx ) catch()
			try (meditMaterials[matSlot].materialList[subMat].opacityMap.monoOutput = 1) catch()
			try (meditMaterials[matSlot].materialList[subMat].opacityMap.rgbOutput = 1) catch()
			try (meditMaterials[matSlot].materialList[subMat].opacityMap.alphaSource = 0) catch()		

		)	
	)


	-- need to apply the material
	try 
	(
		selectbywildcard proptype
		proparray = getcurrentselection()
		$.material = meditMaterials[matSlot]
		
		for prop = 1 to proparray.count do
		(

			-- We need to set the correct material ID on each hat now
			theprop = getNodeByName (proptype + "_00" + (prop - 1) as string)
			select theprop
			subobjectLevel = 4
			numface = theprop.GetNumFaces()
			theprop.EditablePoly.SetSelection #Face #{1..numface}
			theprop.EditablePoly.setMaterialIndex (((prop - 1) * numsubmats) + 1) 0
			theprop.EditablePoly.SetSelection #Face #{}
			subobjectLevel = 0
		)
	
	) catch ()

	clearSelection()

)



fn create_checker =
(
	meditMaterials[6].name = "Checker"
	meditMaterials[6].diffuseMap = Bitmaptexture fileName:"X:\GTA\gta_art\peds\Base_Stuff\checker_512.gif"
)



fn kill_materials = 
(
	max select all
	$.material = undefined
)



fn create_transparent_mat =
(
	meditMaterials[5].opacity = 0
	meditMaterials[5].name = "Transparent"
)



fn get_textures CNAME pedfolder =
(
	-- Copy the base textures to the correct folder
	defaultBase = "x:\\GTA\\gta_art\\peds\\Base_stuff\\"
	fileExt = "*.tga"
	toCopy = defaultBase+fileExt
	copyTo = pedfolder + CNAME + "\\Textures\\ConsoleRes\\"
		
	for f in getFiles toCopy do
	(
		copyFile f (copyTo + getFilenameFile f + getFilenameType f)
	)
)



-- What does this do?
fn sortComponentArray compSoArray compSoName = 
(
	if compSoArray.count > 0 then 
	(
		append sortarrayn compSoArray.count
		append sortarrayl compSoName
		compSoArray = sort compSoArray 
	)
)



-- This function sets a map in a rage
-- MatID and SubID should be obvious
-- maptype is a string and imagefile is a path
fn RAGEsetV MatID SubID slottype slotvalue =(
		
	RageSlotNames =#()
	RageSlotTypes =#()
	
	-- Need to know the shadertype
	RageShaderName = RstGetShaderName meditMaterials[matid].materialList[subid]
	RageShaderSlotCount = RstGetVariableCount meditMaterials[matid].materialList[subid] 
	

	for ss = 1 to RageShaderSlotCount do
	(
		append RageSlotNames (lowercase (RstGetVariableName meditMaterials[matid].materialList[subid] ss))
		append RageSlotTypes (RstGetVariableType meditMaterials[matid].materialList[subid] ss)
	)

	
	shaderslot = finditem RageSlotNames slottype

	
	-- Now put that file in the correct slot.
	RstSetVariable meditMaterials[MatID].materialList[SubID] shaderslot slotvalue
	
)



fn setComponentMatID componentName MatID = (

	try
	(
		theComponent =  (getnodebyname componentname)
		modPanel.setCurrentObject theComponent.baseObject
		
		-- Add material modifier
		modPanel.addModToSelection (Materialmodifier ()) ui:on
		theComponent.modifiers[#Material].materialID = MatID
		maxOps.CollapseNodeTo theComponent 2 off
	)
	catch ()
)	



-- Replace the textures ??
fn replaceTextures texturefolder thearray comBase shaderid subid matid image_files =
(
	print "in Replace Textures"
	print "thearray" 
	print thearray 
	print "comBase" 
	print comBase 
	print "shaderid"
	print shaderid 
	print "subid" 	
	print subid 
	print "matid"
	print matid
	
	for i = 1 to thearray.count do
	(
		-- This is name dependent
		-- I need to make this more generic some time
		tocheck = substring thearray[i] 6 3
		
		if (tocheck == shaderid) then
		(

			-- Build a filename string for the expected maps
			expdi = comBase + "_DIFF_" + shaderid + "_A_" -- we don't know the racial identifier
			expsp = comBase + "_SPEC_" + shaderid 
			expno = comBase + "_NORMAL_" + shaderid 

			-- Empty variables
			tempDiffMap = tempSpecMap = tempNormMap = ""
			
			-- We grabbed a list of all the filenames a long time ago
			-- cycle through the list of textures to find a match.
			for t = 1 to image_files.count do
			(
				dtgacheck = substring image_files[t] 1 expdi.count
				stgacheck = substring image_files[t] 1 expsp.count
				ntgacheck = substring image_files[t] 1 expno.count
				
				if (dtgacheck == expdi) then tempDiffMap = image_files[t]+ image_type
				if (stgacheck == expsp) then tempSpecMap = image_files[t]+ image_type
				if (ntgacheck == expno) then tempNormMap = image_files[t]+ image_type
			)
			

			-- If we have found some textures, put them into the right place.
			if (tempDiffMap != "") then	(
				RAGEsetV matid subid "diffuse texture" (texturefolder + tempDiffMap)
			)
			
			else 	messagebox ("No image file found for " + expdi as string + " - THIS IS BAD")
					
		
			if (tempSpecMap != "") then	(
				RAGEsetV matid subid  "specular texture"  (texturefolder + tempSpecMap)
			)
			
			else 	messagebox ("No image file found for " + expsp as string + " - OK if this is a dummy object")
			
			
			if (tempNormMap != "") then (
				RAGEsetV matid subid  "bump texture" (texturefolder + tempNormMap)	
			)
			
			else 	messagebox ("No image file found for " + expno as string + " - OK if this is a dummy object")
		)
	)
) -- End Function	



-- *********************************************************************************
-- Material cleaning functions
fn clear_all_materials = 
(
	for i = 1 to 24 do 
	(
		meditMaterials[i] = Standardmaterial ()
	)
)



fn kill_materials = 
(
	max select all
	$.material = undefined
)



-- Convert a max material into a RAGE one for every component
fn CreateRageComponent textureFolder =(

		-- Get all the image files for the model and store them in two arrays
	short_image_files = #()
	long_image_files = getfiles (textureFolder + "*" + image_type)
		
	for f in long_image_files do	
	(
		append short_image_files (uppercase(getfilenamefile f))	
	)

	-- I need arrays to store every possible component
	-- I'll use a predefined array of component names (componenttypes)
	
	component_arrays=#()
	
	-- How many sub materials do we set up?
	-- It's based on the max material in slot maxshaderslot 
	howmanysubs = meditMaterials[maxshaderslot].numsubs

	
	-- Create the base RAGE shader
	meditMaterials[RageShaderslot] = Multimaterial ()
	meditMaterials[RageShaderslot] = Multimaterial numsubs: howmanysubs
	meditMaterials[RageShaderslot].name = shadernameprefix + (shadernamevalue as string)

	-- Name each slot for easier identification
	for slotnum = 1 to howmanysubs do
	(
		meditMaterials[RageShaderslot].names[slotnum] = componentnames[slotnum]
	)

	
	-- Loop through this new rage material and set up all the textures
	-- This are defined in the material set up by another function
	for matLoop = 1 to howmanysubs do
	(
		-- Make the sub id into a Rage Shader
		meditMaterials[RageShaderslot].material[matLoop] = Rage_Shader ()

		-- And set it to the correct default shader for characters
		RstSetShaderName meditMaterials[RageShaderslot].materialList[matLoop] shadertype
		
		
		-- Scale should be 100% with new Rage shaders
		try(RstSetTextureScaleValue meditMaterials[RageShaderslot].materialList[matLoop]  1)catch()
		
		-- enable the lighting too
		try(RstSetLightingMode meditMaterials[RageShaderslot].materialList[matLoop]  true)catch()

		RAGEsetV RageShaderslot matLoop "bumpiness" normStrnVal
		RAGEsetV RageShaderslot matLoop "specular falloff" specFallVal
		RAGEsetV RageShaderslot matLoop "specular intensity" specStrnVal
	
		--an if statement that will set the hair fresnel differently
		if (matLoop >= 8) and (matLoop <= 11) then 
		(RAGEsetV RageShaderslot matLoop "specular fresnel" hairFresStrnVal)
		else
		(RAGEsetV RageShaderslot matLoop "specular fresnel" FresStrnVal)
		
		RAGEsetV RageShaderslot matLoop "enveff+fat: max thickness" EnvEff_FatVal
		RAGEsetV RageShaderslot matLoop "wind: sclh/sclv/frqh/frqv" windVal
		
		RAGEsetV RageShaderslot matLoop "detail inten bump scale" detailVal
		
		try (RAGEsetV RageShaderslot matLoop "diffuse texture" meditMaterials[maxshaderslot].materialList[matLoop].diffuseMap.filename) catch()
		try (RAGEsetV RageShaderslot matLoop "bump texture" meditMaterials[maxshaderslot].materialList[matLoop].BumpMap.filename) catch() -- Bump Map
		try (RAGEsetV RageShaderslot matLoop "bump texture" meditMaterials[maxshaderslot].materialList[matLoop].BumpMap.Normal_Map.filename) catch() -- Normal map
		try (RAGEsetV RageShaderslot matLoop "specular texture" meditMaterials[maxshaderslot].materialList[matLoop].specularLevelMap.filename) catch()
		
	)

	-- At this point I have a RAGE shader setup for the default ped components
	-- Now I want to find every component in the scene
	
	-- Lets get arrays of all the components
	allgeoNames = #()


	-- Create an array to hold ALL the valid components
	for obj in geometry do
	(
		-- Get the name of every bit of geometry in the scene, and derive the shortname from it
		objname = uppercase obj.name
		shortname = substring objname 1 (componenttypes[1].count)

		-- Loop through the array of allowable component types to see if the current object is valid
		for namecheck = 1 to componenttypes.count do
		(	
			thecompname = componenttypes[namecheck]
		
			-- If this is a valid component, store the name
			if (shortname == thecompname) then 
			(	
				append allgeoNames objname
			)
		) -- End namecheck loop
	)-- end geo loop
	
	
	-- Sort this name array to make things a little easier
	allgeonames = sort allgeonames

	
	-- There is now a (possibly large) array holding all the possible components in the scene
	-- I'll split that large array into a 2d array of components by type
	
	-- Create the two array and fill it with empty arrays
	componentArrays = #()
	for ca = 1 to componenttypes.count do componentArrays[ca] = #()
	
	-- Now filter the component names into it
	for x = 1 to allgeoNames.count do
	(
		shortname = substring allgeoNames[x] 1 (componenttypes[1].count)
		
		-- find the short name in the array of component names
		for namecheck = 1 to componenttypes.count do
		(	
			thecompname = componenttypes[namecheck]
			if thecompname == shortname then append componentarrays[namecheck] allgeoNames[x]	
		)
	)
		

	-- What is the biggest array?
	-- I need to know this to know how many Rage materials I need.
	-- This could possible become a common array function?
	biggestSubArray =0
	biggestSubArrayCount = 0
	
	for b = 1 to componenttypes.count do (
		if (componentarrays[b].count > biggestSubArrayCount) then (
			biggestSubArray = b
			biggestSubArrayCount = componentarrays[b].count
		)
	)	
	
	-- Copy the Base Rage shader until we have a new shader to cover 
	-- all possible components in the scene. After that I'll replace the
	-- textures in the duplicate shaders.

	-- Duplicate the base rage shader
	howmanyRage = biggestSubArrayCount - 1
	for x = 1 to howmanyRage do
	(
		-- get the name and copy the base rage shader
		thename = shadernameprefix + x as string
		meditMaterials[(RageShaderslot+x)] = copy meditMaterials[Rageshaderslot]
		meditMaterials[(RageShaderslot+x)].name = thename
		
		-- what shader are we in?
		shaderid = "00" + x as string

		-- Update each slot with the correct materials		
		for c = 1 to componenttypes.count do
		(
			if (componentarrays[c].count >= (x+1)) then replaceTextures texturefolder componentarrays[c] componenttypes[c] shaderid componenttypeSLOT[c]  (RageShaderslot+x) short_image_files 
		)
		
	)

	
	-- Add shaders created!

	-- ********************
	-- hey kids, let's APPLY the shaders to the components in the scene!
	-- extra bonus points



	for i = 0 to (biggestSubArrayCount - 1) do
	(
		-- what component are we applying?
		copyFromShader = "00" + i as string
		applyToShader = (RageShaderslot + i)
		
		for g = 1 to allgeoNames.count do
		(
			underbarSplit = filterstring allgeoNames[g] "_"
			thecom = underbarSplit [2] as string

			if (thecom == copyFromShader) then
			(
				applyto = getnodebyname (allgeonames[g])
				applyto.material = meditMaterials[RageShaderslot+i]
			)
		)
	)
	
)



-- Copy prop shaders and convert the copies into Rage shader
fn createRageProps fromMaxSlot ToRageSlot alphasub= (
		
	howmanysubs = (meditMaterials[fromMaxslot].numsubs)
	
	alphasubarray=#()
	
	--do we need alpha?
	if alphasub != 0 then (
		for t = 0 to howmanysubs by alphasub do append alphasubarray t
	)
	
	-- Copy the multisub and name it
	thename = substring (meditMaterials[fromMaxslot].names[1]) 1 6
	meditMaterials[toRageSlot] = copy meditMaterials[fromMaxSlot]
	meditMaterials[toRageslot].name = thename
	
	for matloop = 1 to howmanysubs do
	(
		-- Convert this sub matieral into a Rage shader
		meditMaterials[toRageslot].material[matloop] = Rage_Shader () 
		RstSetShaderName meditMaterials[toRageSlot].materialList[matLoop] shadertype
		
		-- Now we need to populate the shaders with the textures
		-- Scale should be 100% with new Rage shaders
		try(RstSetTextureScaleValue meditMaterials[toRageSlot].materialList[matLoop]  1)catch()
		
		-- enable the lighting too
		try(RstSetLightingMode meditMaterials[toRageSlot].materialList[matLoop]  true)catch()

		RAGEsetV toRageSlot matLoop "bumpiness" normStrnVal
		RAGEsetV toRageSlot matLoop "specular falloff" specFallVal
		RAGEsetV toRageSlot matLoop "specular intensity" specStrnVal
		RAGEsetV toRageSlot matLoop "specular fresnel" FresStrnVal
		RAGEsetV toRageSlot matLoop "enveff+fat: max thickness" EnvEff_FatVal
		RAGEsetV toRageSlot matLoop "wind: sclh/sclv/frqh/frqv" windVal
		RAGEsetV RageShaderslot matLoop "detail inten bump scale" detailVal
		
		try (RAGEsetV toRageSlot matLoop "diffuse texture" meditMaterials[fromMaxSlot].materialList[matLoop].diffuseMap.filename) catch()
		try (RAGEsetV toRageSlot matLoop "bump texture" meditMaterials[fromMaxSlot].materialList[matLoop].BumpMap.filename) catch() -- Bump Map
		try (RAGEsetV toRageSlot matLoop "bump texture" meditMaterials[fromMaxSlot].materialList[matLoop].BumpMap.Normal_Map.filename) catch() -- Normal map
		try (RAGEsetV toRageSlot matLoop "specular texture" meditMaterials[fromMaxSlot].materialList[matLoop].specularLevelMap.filename) catch()
		
		-- set to alpha?
		for x = 1 to alphasubarray.count do (
			print "read this"
			print alphasubarray[x]
			print toRageSlot
			print matLoop
			print "end"
			
			if matloop == alphasubarray[x] then 
			(
				RstSetShaderName meditMaterials[toRageSlot].materialList[matLoop] alphashadertype
				RAGEsetV toRageSlot matLoop "specular falloff" pGlassspecFallVal
				RAGEsetV toRageSlot matLoop "specular intensity" pGlassspecStrnVal
				RAGEsetV toRageSlot matLoop "specular fresnel" pGlassFresStrnVal	
			)
		)
	)	-- End of creation loop
	
	if ToRageSlot == 11 do
	(
		select $p_ey*
		eyeProps = getCurrentSelection()	
		for applyMat = 1 to eyeProps.count do
		(
			applyto = eyeProps[applyMat]
			applyto.material = meditMaterials[ToRageSlot]
		)
	)
	if ToRageSlot == 12 do
	(
		select $p_he*
		headProps = getCurrentSelection()	
		for applyMat = 1 to headProps.count do
		(
			applyto = headProps[applyMat]
			applyto.material = meditMaterials[ToRageSlot]
		)
	)
)



