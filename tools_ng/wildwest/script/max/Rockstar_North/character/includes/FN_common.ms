-- Ricks Common Maxscript functions
-- Modified July 2009 to make BuildPointHelper return the point helper 

-- added fn AlignPivotTo function Jan 2010

-- Load the rockstar export settings
filein "rockstar\export\settings.ms"



-- *********************************************************************************************************************
-- *********************************************************************************************************************
-- String Functions

-- Convert text string to uppercase
fn uppercase instring = (  
	local upper, lower, outstring 
	upper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lower="abcdefghijklmnopqrstuvwxyz"

	outstring=copy instring

	for i=1 to outstring.count do 
	( 
		j=findString lower outstring[i]
		if (j != undefined) do outstring[i]=upper[j]
	)

	outstring 
) 


-- Convert text string to lowercase
fn lowercase instring = (  
	local upper, lower, outstring 
	upper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lower="abcdefghijklmnopqrstuvwxyz"

	outstring=copy instring

	for i=1 to outstring.count do 
	( 
		j=findString upper outstring[i]
		if (j != undefined) do outstring[i]=lower[j]
	)

	--return outstring 
	outstring 
) 



-- Replace spaces in strings with underscores
-- This is vital for max variables in expressions
fn replacespace instring = (  
	local upper, lower, outstring 
	thespace=" "
	thereplace="_"

	outstring=copy instring

	for i=1 to outstring.count do 
	( 
		j=findString thespace outstring[i]
		if (j != undefined) do outstring[i]=thereplace[j]
	)

	outstring 
) 




-- Looks for an underscore in a string at a given position
fn findUnderscore ubstring spos = 
(

	returnvalue = undefined
	
	findUndersc = substring ubstring spos 1
	
			if findUndersc == "_" then
			(	
			returnvalue = True
			)
			else
			(
			returnvalue = False
			)
	returnvalue	
)

----------------------------------------------------------------------
----------------------------------------------------------------------

--copied from cgtalk 
--http://forums.cgsociety.org/showthread.php?f=98&t=958314&page=2&pp=15
--this turns off window-ghosting for that session of Max
fn DisableProcessWindowsGhosting =
(
	if not isKindOf (dotnet.GetType "DisableWindowsGhosting") dotNetObject do
	(
		local source = StringStream ("
			using System.Runtime.InteropServices;

			public class DisableWindowsGhosting
			{
				[DllImport(\"user32.dll\")]
				public static extern bool DisableProcessWindowsGhosting();
			}")

		compilerParams = dotnetobject "System.CodeDom.Compiler.CompilerParameters"
		compilerParams.ReferencedAssemblies.Add("System.dll");
		compilerParams.GenerateInMemory = on
		csharpProvider = dotnetobject "Microsoft.CSharp.CSharpCodeProvider"
		compilerResults = csharpProvider.CompileAssemblyFromSource compilerParams #(source as String)
		flush source
		close source
		if (compilerResults.Errors.Count > 0 ) then
		(
			local errs = stringstream ""
			for i = 0 to (compilerResults.Errors.Count-1) do
			(
				local err = compilerResults.Errors.Item[i]
				format "Error:% Line:% Column:% %\n" err.ErrorNumber err.Line err.Column err.ErrorText to:errs
			)
			format "%\n" errs
			undefined
		)
		else
		(
			compilerResults.CompiledAssembly.CreateInstance "DisableWindowsGhosting"
		)
	)
	
	if isKindOf (dotnet.GetType "DisableWindowsGhosting") dotNetObject do
	(
		(dotNetObject "DisableWindowsGhosting").DisableProcessWindowsGhosting()
	)
)

-- DisableWindowsGhosting =     DisableProcessWindowsGhosting()
--     
-- DisableWindowsGhosting.DisableProcessWindowsGhosting()
----------------------------------------------------------------------
----------------------------------------------------------------------

-- *********************************************************************************************************************
-- *********************************************************************************************************************
-- Array  Functions

-- This returns a difference array from two arrays
fn differenceInArray masterArray subsetArray =
(
	newarray = #()
	for mi = 1 to masterArray.count do (
		
		foundMatch=0
		MitemtoFind = masterArray[mi]
		
		for si = 1 to subsetarray.count do (
			SitemtoFind = subsetArray[si]
			if MitemtoFind == SitemtoFind then 
			(
				foundMatch = 1
				exit	-- for speed
			)
		)	
			
		if foundMatch == 0 then append newArray MitemToFind	
	)	
	
	newArray
)





-- *********************************************************************************************************************
-- *********************************************************************************************************************
-- Positional  Functions


-- taking in a bonename, such as $'Char Head', return the x,y,z position
fn getObjPos theObject = (
	-- taking in a bone, convert it to a string, parse the position
	p3 = theObject.transform.pos
	p3
)




-- Calculate a point in between two point3 positions
-- Can actually be used beyond 100% to find offsets too
fn inbetweenpoint startpoint endpoint whichpoint = (
	wp = endpoint + ((startpoint-endpoint) * (1.0 - whichpoint))
	wp
)

fn AlignPivotTo Obj Trgt =

      (
            -- Get matrix from object
            if classOf Trgt != matrix3 then Trgt = Trgt.transform

			-- Store child transforms
            local ChldTms = in coordSys Trgt ( for Chld in Obj.children collect Chld.transform )

            -- Current offset transform matrix
            local TmScale = scaleMatrix Obj.objectOffsetScale
            local TmRot = Obj.objectOffsetRot as matrix3
            local TmPos = transMatrix Obj.objectOffsetPos
            local TmOffset = TmScale * TmRot * TmPos

            -- New offset transform matrix
            TmOffset *= obj.transform * inverse Trgt

            -- Apply matrix
            Obj.transform = Trgt

            -- Restore offsets
            Obj.objectOffsetPos = TmOffset.translation
            Obj.objectOffsetRot = TmOffset.rotation
            Obj.objectOffsetScale = TmOffset.scale

            -- Restore child transforms
            for i = 1 to Obj.children.count do Obj.children[i].transform = ChldTms[i] * inverse Trgt * Obj.transform
      )



-- *********************************************************************************************************************
-- *********************************************************************************************************************
-- Prop things.


fn AlignProps =
(
-- set the timeline to 0, that should be 'safe'
sliderTime = 0f
-- adding some reset xform stuff to clean up the mesh
select $p_*
allProps = getCurrentSelection()	
for resetObj = 1 to allProps.count do
	(
	resetxform allProps[resetObj]
	maxOps.CollapseNodeTo allProps[resetObj] 1 True
	)	

-- now lets align the array contents to each other
for MatchSel = 1 to MasterPropList.count do
	(
	for PropSel = 1 to MasterPropList[MatchSel].count do
		(
		try
			(
			MatchProp = MasterPropList[MatchSel][PropSel] -- select the prop within the group
			MatchBone = propBones[MatchSel] -- select the matching bone
			selectbywildcard matchprop		
			propsToAlign = getCurrentselection()
			-- aligns the selected props to their matching bone
			for p = 1 to propsToAlign.count do (
				AlignPivotto propsToAlign[p] MatchBone -- aligns the selected props to their matching bone
				)
			) catch()
		)
	)
)


fn LinkProps =
(
-- set the timeline to 0, that should be 'safe'
sliderTime = 0f
select $p_*
-- now lets align the array contents to each other
for MatchSel = 1 to MasterPropList.count do
	(
	for PropSel = 1 to MasterPropList[MatchSel].count do
		(
		try
			(
			MatchProp = MasterPropList[MatchSel][PropSel] -- select the prop within the group
			MatchBone = propBones[MatchSel] -- select the matching bone
			selectbywildcard matchprop		
			propsToLink = getCurrentselection()
			
			for p = 1 to propsToLink.count do (
				-- adds a link constraint
				propsToLink[p].controller = link_constraint()
				propsToLink[p].controller.addTarget MatchBone 0
				)
			) catch()
		)
	)
)


fn unlinkProps =
(
-- set the timeline to 0, that should be 'safe'
sliderTime = 0f	
select $p_*
-- now lets align the array contents to each other
for MatchSel = 1 to MasterPropList.count do
	(
	for PropSel = 1 to MasterPropList[MatchSel].count do
		(
		try
			(
			MatchProp = MasterPropList[MatchSel][PropSel] -- select the prop within the group
			MatchBone = propBones[MatchSel] -- select the matching bone
			selectbywildcard matchprop		
			propsToLink = getCurrentselection()
			
			for p = 1 to propsToLink.count do
				(
				--delete any existing links
				try
					(
					numTargets = propsToLink[p].controller.getNumTargets()
						for delTargets = 1 to numTargets do
						(	
							propsToLink[p].controller.deleteTarget delTargets
						)
					) catch()	
				)
			) catch()
		)
	)
)
-- *********************************************************************************************************************
-- *********************************************************************************************************************
-- Building things.

-- Build an Expose Transform Helper with name, position, size and colour
-- Thankfully not required so much these days
fn buildETM etmname etmExposer etmPos etmSize etmColour = (
	newetm = ExposeTm pos:etmPos isSelected:on name:etmName size: etmSize wirecolor:etmColour
	newetm.exposenode = etmExposer
)


-- Build a Point Helper with name, position, size and colour
fn buildPointHelper phName phPos phSize phColour = (
	newph = Point pos:phPos name:phName size:phSize wirecolor:phColour
	newph.Box = off
	newph.cross = on
	newph.centermarker = off
	
	return newph
)


-- Build a Dummy elper with name, position and size
-- Size is done by scaling and restting the transform afterwards
fn buildDummyHelper dName dPos dSize = (

	newdummy = Dummy pos:dPos name:dName isselected:on 
	scale $[dsize,dsize,dsize]
	ResetTransform $
)






fn alignall alignme tome =(
	alignme.transform=tome.transform
)




fn alignrot alignme tome =(
	in coordsys (transMatrix alignme.pos) alignme.rotation=inverse tome.rotation
)



-- This should really take in a axis: Rick, October 2009
-- The X axis is the most common, so no hurry on this.
fn mirrorObject copyMe PasteMe =
(
	-- Assume we are mirroring across the X axis.
	copyPos = copyme.pos * [-1,1,1]
	pasteMe.pos = copyPos
)




-- Free Transforms, very useful for constructing rigs
-- Stolen from the Freezetransform that came with max.
	fn FreezeTransform = 	--freezes transforms. Copied from the max macroscript as this does not like being run from other scripts.
	( 		
		local Obj = Selection as array 	
		suspendEditing which:#motion
		for i = 1 to Obj.count do 		
		( 
			Try
			(	
				local CurObj = Obj[i] 	
	
				if classof CurObj.rotation.controller != Rotation_Layer do
				(

					-- freeze rotation		
					CurObj.rotation.controller = Euler_Xyz() 		
					CurObj.rotation.controller = Rotation_list() 			
					CurObj.rotation.controller.available.controller = Euler_xyz() 		
					
					/* "Localization on" */  
				
					CurObj.rotation.controller.setname 1 "Frozen Rotation" 		
					CurObj.rotation.controller.setname 2 "Zero Euler XYZ" 		
				
					/* "Localization off" */  
					
					CurObj.rotation.controller.SetActive 2 		
				)
				if classof CurObj.position.controller != Position_Layer do
				(

					-- freeze position
					CurObj.position.controller = Bezier_Position() 			
					CurObj.position.controller = position_list() 			
					CurObj.position.controller.available.controller = Position_XYZ() 	
		
					/* "Localization on" */  
							
					CurObj.position.controller.setname 1 "Frozen Position" 	
					CurObj.position.controller.setname 2 "Zero Pos XYZ" 			
					
					/* "Localization off" */  
					
					CurObj.position.controller.SetActive 2 		
		
					-- position to zero
					CurObj.Position.controller[2].x_Position = 0
					CurObj.Position.controller[2].y_Position = 0
					CurObj.Position.controller[2].z_Position = 0
				)
				if classof CurObj.scale.controller != Scale_Layer do
				(

					-- freeze scale
					CurObj.scale.controller = Bezier_Scale() 			
					CurObj.scale.controller = scale_list() 			
					CurObj.scale.controller.available.controller = ScaleXYZ() 	
		
					/* "Localization on" */  
							
					CurObj.scale.controller.setname 1 "Frozen Scale" 	
					CurObj.scale.controller.setname 2 "Zero Scale XYZ" 			
					
					/* "Localization off" */  
					
					CurObj.scale.controller.SetActive 2 		
		
					-- scale to zero
					CurObj.scale.controller[2].x_Scale = 100
					CurObj.scale.controller[2].y_Scale = 100
					CurObj.scale.controller[2].z_Scale = 100
				)				
				
			)	
			/* "Localization on" */  
					
			Catch( messagebox "A failure occurred while freezing an object's transform." title:"Freeze Transform")
					
			/* "Localization off" */  	
		)
		resumeEditing which:#motion
	)


	
	


fn linkorient theManip theVictim alignManip theWeight =
(
	if alignManip == 1 then
	(
		in coordsys (transMatrix theManip.pos) theManip.rotation= theVictim.rotation
	)
	
	FreezeTransform()
	
	theVictim.rotation.controller = Orientation_constraint()
	thevictim.rotation.controller.appendtarget themanip theWeight
	thevictim.rotation.controller.relative = on
)






-- A function to toggle biped figuremore on/off
fn fig_toggle = (

	-- figure mode toggle
	biped_ctrl=$char.controller

	-- get mode
	cmode = biped_ctrl.figureMode

	-- toggle it

	if cmode == true then 
		(
			biped_ctrl.figureMode= false
		)
		
	else 
		(
		biped_ctrl.figureMode= true
		)


	--cancel bone position lag	
	--variable mytime stores the current time on timeline
	mytime=currentTime
	--sets timeline 1 frame further
	sliderTime = mytime + 1
	--moves timeline back to whatever frame it was before
	sliderTime = mytime
	
)

-- *******************************************************************************************
-- GTA Attribute functions
-------------------------------------------------------------------------------------------------------------------------
-- a function that will edit the attribute on on/off attributes
-- selType is the superclass to check, obj is the passed in object, selAttr is the attribute to set and selAttrState is what to set it to
fn setAttribute obj selAttr selAttrState = (
	if ((obj != undefined) and (superclassof obj != undefined)) then
	(
		-- Set the Attributes:
		indexAttr = GetAttrIndex (getAttrClass obj) selAttr
		attrSet = SetAttr obj indexAttr selAttrState
	)
)--end setCollAttribute
-------------------------------------------------------------------------------------------------------------------------
-- a function that will read the attributes of selected meshes
-- selType is the superclass to check, obj is the passed in object, selAttr is the attribute to read and setSelState is what store it in
fn readAttribute obj selAttr setSelState = (
	if ((obj != undefined) and (superclassof obj != undefined)) then
	(
		--work out the index for the attribute
		indexAttr = GetAttrIndex (getAttrClass obj) selAttr
		--get the attribute value
		attrGet = GetAttr obj indexAttr
		readAttr = attrGet
	)
)--end readCollAttribute
-------------------------------------------------------------------------------------------------------------------------




-- *******************************************************************************************
-- Selection functions

-- Lets you select items by name using wildcards
fn selectByWildcard nodeName=
(
	-- Create 	
	local myNodeList = #()
	nnc = nodename.count
	
	select $*
	
	for obj in selection do
	(
		ckn = substring obj.name 1 nnc
		if ((uppercase ckn) == (uppercase nodeName)) then
		(
			append myNodeList obj
		)
	) 
	clearSelection()
	select myNodeList
)	

--store and restore the state of a scene (visible objects)
sceneState = #()
sceneVisible = #()

fn storeSceneState =
(
	sceneState = #()
	-- read and store the category display states
	append sceneState hideByCategory.geometry 
	append sceneState hideByCategory.shapes
	append sceneState hideByCategory.lights
	append sceneState hideByCategory.cameras
	append sceneState hideByCategory.helpers
	append sceneState hideByCategory.spacewarps
	append sceneState hideByCategory.particles
	append sceneState hideByCategory.bones
	
	sceneVisible = #()
	-- select everything that's visible
	max select all
	sceneVisible = getCurrentSelection()
	clearSelection()
)

fn restoreSceneState sceneState sceneVisible = 
(
	-- unhide all by categry
	hideByCategory.none()	
	-- unhide all
	unhide (for obj in objects where obj.ishidden collect obj)
	
	hideByCategory.geometry = sceneState[1]
	hideByCategory.shapes = sceneState[2]
	hideByCategory.lights = sceneState[3]
	hideByCategory.cameras = sceneState[4]
	hideByCategory.helpers = sceneState[5]
	hideByCategory.spacewarps = sceneState[6]
	hideByCategory.particles = sceneState[7]
	hideByCategory.bones = sceneState[8]
	
	select sceneVisible
	max hide inv  --hide unselected
	clearSelection()
)

-- hide all the collision and constraints in a scene
fn hideCollAndConst = 
(
	storeSceneState()
	clearSelection()
	select $Collision_Capsule*
	selectmore $Constraint*
	hideCandC = getCurrentSelection()
	restoreSceneState sceneState sceneVisible
	hide hideCandC
)

--********************************************************************************************
--Geometry Functions
----------------------------------------------------------------------------------------------------------------

--@summary A function that returns an array specifying list of faces related to a given vertex and the proportion or wedge size 
--out of the total arc around the vertex
--@params inMesh vIndex
--@note inmesh is the EditablePoly mesh to use. vIndex is the vertex index into the mesh
fn getFaceWedgeSizeFromMeshVert inMesh vIndex = 
(
	--array to return
	wedgeSizes = #()
	
	--Get the faces for the vIndex
	aFaceArray = (polyop.getFacesUsingVert inMesh vIndex) as array
	
	--for each face get its edges and match them to the edges attached to this vert
	for face in aFaceArray do
	(
		--get the edges
		aFaceEdges = (polyop.getEdgesUsingFace inMesh face) as array
		
		--get verts from edge, check one of them is vIndex
		connFaceEdgeVerts = #()
		for fEdge in aFaceEdges do
		(
			--get edgeVerts
			edgeVerts = (polyop.getVertsUsingEdge inMesh fEdge) as array
			--if one of the edgeverts is vIndex then add to array
			if( edgeVerts[1] == vIndex) then
			(
				append connFaceEdgeVerts edgeVerts[2]
			)
			
			if( edgeVerts[2] == vIndex) then
			(
				append connFaceEdgeVerts edgeVerts[1]
			)
		) --end fEdge
		
		--get normalized vector for each edge
		edge1Vec = normalize( inMesh.vertices[vIndex].pos - inMesh.vertices[connFaceEdgeVerts[1]].pos )
		edge2Vec = normalize( inMesh.vertices[vIndex].pos - inMesh.vertices[connFaceEdgeVerts[2]].pos )
		
		--dot the vecs to get angle
		--add to wedgesizes array with the face
		append wedgeSizes #(face, (abs(dot edge1Vec edge2Vec)))
	) -- end face
	
	--print wedgeSizes
	--DEBUG
	/*
	total = 0
	for w in wedgeSizes do
	(
		--print (acos(w))
		total += (acos(w))
	)
	print total
	*/
	
	return wedgeSizes
	
) --end function


--@summary A function to retreive map verts for the given channel from a supplied mesh vertex index
--@params inMesh vIndex vChannel
--@note inMesh is the polymesh to work with, vIndex is the mesh vertex to find the map verts from, vChannel is the map channel to use.
--for v = 1 to $.vertices.count do
--( c = getMapVertColoursFromMeshVert $ v 0
--print c
--)
fn getMapVertColoursFromMeshVert inMesh vIndex vChannel =
(
    --array to return
	colours = #()
	
	--get the TriMesh into memory
	theMesh = snapshotasmesh inMesh 
    
    -- Get all faces using the vert as array
    faceArray = (meshop.getFacesUsingVert theMesh vIndex) as array
    
	--iterate over the faces
	for f in faceArray do
    (
		--Get geometry face - vertex is either its first, second or third component    
		geoFace = getFace theMesh f
	
		-- Get map face (index corresponds with geometry face)
		--There may not be a map channel set for this, so catch it accordingly
		try
		(
			mapFace = meshop.getMapFace theMesh vChannel f
		)
		catch
		(
			--add a pure white for a missing channel colour
			append colours #(255, 255, 255)
			--onto the next
			continue
		)

		-- Find order inside the mesh face - vertex (component) order will be the same in both
		mapVert = case of
		(
			(geoFace.x == vIndex): mapFace.x
			(geoFace.y == vIndex): mapFace.y
			(geoFace.z == vIndex): mapFace.z
		)
		
		--collect the mapVert values found
		append colours (meshop.getMapVert theMesh vChannel mapVert)
	
	)--end f loop
	
	--release the mesh from memory
    delete theMesh 
	
	--return the mapVert values found
	colours
	
)--end function