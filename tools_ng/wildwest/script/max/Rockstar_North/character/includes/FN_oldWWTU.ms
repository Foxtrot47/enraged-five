
	
-- Function to collect tools data usage

fn OldWWCollectToolUsageData scriptData =
(
	-- Construct an array of output data
	toolsUsage =#()
	
	toolname = filterstring  scriptData "\\"
	toolname = "OLD_WW "+ (toolname[toolname.count])
	toolusername = (RsUserGetUserName())
	tooluserstudio = (RsUserGetStudio())
	toolusetime = localtime
		
	append toolsUsage toolname
	append toolsUsage scriptData
	append toolsUsage toolusername
	append toolsUsage tooluserstudio 
	append toolsUsage toolusetime
	append toolsUsage (getFileModDate scriptData)
	
		
	print toolsUsage
		
	-- Get the data out of Max
		
		
	-- The first version is just going to create a text file
	--  This should go to a database
	ToolUsageDataPath = "N:\\RSGEDI\\Temp\\ToolUsage\\Old_WW\\" + toolname + "\\"	
		
	ToolUsageDataFilename = toolname +"_"+ toolusername
	randomhash = (athash16u (ToolUsageDataFilename +toolusetime)) as string
	ToolUsageDataFilename = ToolUsageDataPath  + ToolUsageDataFilename + randomhash  + ".txt"
	
	try (		
		makeDir (ToolUsageDataPath)	
		
		local output_file = createfile ToolUsageDataFilename 
		for dataentry in toolsUsage do
		(	
			format (dataentry+",") to:output_file
		)	
		close output_file	
	) catch()	
		
)	

