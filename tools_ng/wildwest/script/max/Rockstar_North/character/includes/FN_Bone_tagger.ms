boneTagArray =#()
BoneNameArray=#()

-- Populate mapping lookup arrays from a project specific XML file
fn populateMappingArrays = (
	
	-- Make sure the predefined arrays are empty
	boneTagArray =#()
	BoneNameArray=#()
	
	-- boneMapList =#() -- safe to delete?
	
	
	-- Define the XML file
	mappingXmlFile = RsConfigGetContentDir() + "animconfig/bone_tags.xml" 
	xmlDoc = XmlDocument()	
	xmlDoc.init()
	xmlDoc.load mappingXmlFile
	xmlRoot = xmlDoc.document.DocumentElement
	
	
	-- Parse the XML
	-- This could be looped I suspect
	if xmlRoot != undefined then (
		dataElems = xmlRoot.childnodes
	
		for i = 0 to (dataElems.Count - 1 ) do (
			dataElement = dataElems.itemof(i)
			
			-- Look for ped bones
			if dataelement.name == "peds" then (
				-- Look for the bones ids
				boneElems = dataelement.childnodes
				
				for b = 0 to (boneElems.Count - 1 ) do (
					boneElement = boneElems.itemof(b)
					
					-- If we've found the bone ids, populate the struct
					if boneElement.name == "boneids" then (
						
						boneTagElems = boneElement.childnodes
						
						for bt = 0 to (BoneTagElems.Count - 1 ) do (
						
							boneTagElement = boneTagElems.itemof(bt)
							
							nameattr = boneTagElement.Attributes.ItemOf("name")
							tagattr = boneTagElement.Attributes.ItemOf("id")
							
							append BoneNameArray nameattr.value
							append boneTagArray tagattr.value	
							
						)
					) -- end bone elements
				)
			)-- end ped data elements
		)		
	)-- end xmlfound
)




-- Given a bone name string, perform a lookup to find the bonetag.
-- This lets us assign sensible project specific bone tags
fn findBoneTag bonename = (

	foundtag ="UNKNOWN" -- Set the default bonetag to be unknown. This is useful for bug tracking
	
	for bonecount = 1 to boneTagArray.count do (
		if boneNameArray[bonecount] == bonename then foundtag=boneTagArray[bonecount]
	)	
	
	foundtag -- return the tag.
)	

