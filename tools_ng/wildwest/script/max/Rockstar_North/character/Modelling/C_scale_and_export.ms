-- Clone, scale and expot as OBJ script
-- Stewart Wright
-- January 2010

-- Work out the project so we don't have to use hardcoded filein paths etc
project = rsconfiggetprojectname()
filein ("X:/"+project+"/tools/dcc/current/max2009/scripts/character/includes/FN_common.ms")


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

--create global elements
scaleFactor = 10 -- default scale factor
exportPrompt = false -- no popup export prompt

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
fn CloneNScale =
	(
		ObjectArray=#()
		SortedObjectArray=#()
		ObjectFaceCount = #()
		CopiedObjects = #()
		ScaleObjectArray = #()
		
		ObjectArray = getCurrentSelection()
		
		for objsel in ObjectArray do (
			cc = copy objsel
			resetxform $
			append CopiedObjects cc
			)
			
			
		select CopiedObjects
		CopiedObjects = getCurrentSelection()			
					
		for objsel in CopiedObjects do (
			append ObjectFaceCount (getnumfaces objsel)
			)
				
			-- Only ever 2 objects in these arrays
			-- Assume we ALWAYS want low then high
			-- now find the biggest number in the face count.
		if (ObjectFaceCount[2] < ObjectFaceCount[1]) then (
			-- Swap them over
			append SortedObjectArray CopiedObjects[2]
			append SortedObjectArray CopiedObjects[1]
			)
			
		else (SortedObjectArray = CopiedObjects)
			
		-- rename the meshes to reflect their polycounts
		-- add it as a suffix to the existing mesh name
		SortedObjectArray[1].name = SortedObjectArray[1].name + "_Low"
		SortedObjectArray[2].name = SortedObjectArray[2].name + "_High"
			
		-- align the pivot point to a dummy mesh that we've created
		-- lets make a teapot to align everything to
		teapot radius:17.196 smooth:on segs:4 body:on handle:on spout:on lid:on mapcoords:on pos:[0,0,0] isSelected:on
		$.name = "AlignTeapot"

		-- call our alignpivot fn to align things up
		for objsel in SortedObjectArray do (
			AlignPivotTo objsel $AlignTeapot
			)
			
		-- we've had our fun with the teapot, time to throw it away			
		delete $AlignTeapot
			
		-- make an array to hold the meshes we want to scale
		ScaleObjectArray = SortedObjectArray
		-- lets use X for our scale factor, I can maybe add a spinner or something for people to set their own scale value?
		-- set selected objects scale property
		ScaleObjectArray.scale = [scaleFactor,scaleFactor,scaleFactor]
			
		select CopiedObjects
		CopiedObjects	
	)
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- the export part of the script
fn ExportOBJ CopiedObjects=
(
	for es = 1 to CopiedObjects.count do
	(
		-- create an array to store the currently avaliable exporters
		theClasses = exporterPlugin.classes
		-- lets make this into a string so we can search it
		strClasses =#()
			for ec = 1 to theclasses.count do append strClasses  (theClasses[ec] as string)
			
		-- we're going to have to find the "ObjExp" within this...
		ObjExpPos = findItem strClasses "ObjExp"
		-- exports the selected objects to the current open file path with no popups
			
		select CopiedObjects[es]
		if exportPrompt == true then
		(	
		exportFile (GetDir #scene + "/" + $.name ) #noPrompt selectedOnly:True using:theClasses[ObjExpPos]
		)
		else
		(
		exportFile (GetDir #scene + "/" + $.name ) selectedOnly:True using:theClasses[ObjExpPos]
		)
	)	
)


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--create rollout
rollout CloneScaleExport "Clone, Scale 'n Export"
	
(
	label desc "Export meshes for baking" width:120 height:20
	
	--create buttons
	button btnExportSel "Export 'em" width:120 height:40 toolTip:"Export currently selected objects to OBJ's."
	spinner spnScaleFactor "Custom Scale Factor:" range:[0,100,(scaleFactor as float)] type:#float scale: 5 fieldWidth:40 
	checkbox chkExportPopup "Hide Export Popup" checked:True toolTip:"Hide the export popup."

	on btnExportSel pressed do
	(
		if chkExportPopup.state == true then exportPrompt = true else exportPrompt = false
		
		-- Lets check that there are 2 objects selected
		if  selection.count == 2 then
		(
		CopiedObjects = CloneNScale ()
		ExportOBJ CopiedObjects

		)
		else
		(
		messagebox "Please select 2 objects"
		)
	)
		
	on spnScaleFactor changed val do 
	(
		scaleFactor = val
	)
	
)--end of rollout "Clone, Scale 'n Export"
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
createDialog  CloneScaleExport width:130