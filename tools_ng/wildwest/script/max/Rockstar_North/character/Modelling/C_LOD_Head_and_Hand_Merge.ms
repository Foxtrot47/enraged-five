-- LOD Merge Script
-- Stewart Wright
-- October 2010
-- Work out the gender of a model and merge the LOD Head and Hand parts for each head in the scene.  Rename the merged LOD parts correctly.

--Load the common functions
filein "rockstar/export/settings.ms"




--Figure out the project data
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()
theProjectPedFolder = theProjectRoot + "art/peds/"

--Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

currentModelGender = ""
LODFile = undefined

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--try to figure out the gender based on the models name
--set up some boolean states based on this
fn defineGender = 
(
	guessLODFolder = (theProjectPedFolder + "ped_parts/LOD/")
	
	tempModelName = uppercase maxfilename
	currentModelGender = tempModelName

	if currentModelGender[3] == "M" then
		(
			defaultSkeleton = "Male Skeleton"
			guessFilename = ("Male head and hands LOD.max")
			LODFile = (guessLODFolder+guessFilename)
			print LODFile
		)
	else
		(
			if currentModelGender[3] == "F" then
				(
					defaultSkeleton = "Female Skeleton"
					guessFilename = ("Female head and hands LOD.max")
					LODFile = (guessLODFolder+guessFilename)
					print LODFile
				)
			else
				(
					Print ("Unable to define skeleton.  Expected M or F in file name, found " + currentModelGender[3])
				)
		)
)--end defineGender

-------------------------------------------------------------------------------------------------------------------------
--Merge the LOD head and hands and supress all confirmations
fn mergeLODFile =
(
	selectedHeads = #()
	tempSelectedHeads = #()
	
	select $head_*
	tempSelectedHeads = getCurrentSelection()
	clearSelection()
	
	for s=1 to tempSelectedHeads.count do
	(
		if classof tempSelectedHeads[s] == PolyMeshObject then append selectedHeads tempSelectedHeads[s]
	)
	
	for h=1 to selectedHeads.count do
	(
		highHead = selectedHeads[h].name
		LODHeadName = highHead + " LOD"

		--merge in the gizmo thing as mover
		print ("Merging LOD head and hands for " + highHead)
		mergeLOD = #("LOD_Head_and_Hands")--what do we want to merge?
		try
		(
			mergeMaxFile LODFile mergeLOD #deleteOldDups #useSceneMtlDups #alwaysReparent quiet:true
			$LOD_Head_and_Hands.name = LODHeadName
			print (highHead + "'s LOD Head and Hands merged")
		) catch()
	)
)--end mergeLODFile

-------------------------------------------------------------------------------------------------------------------------
rollout LODHeadHandsMerge "LOD Merge"
(
	button btnMergeLODHeadHands "Merge LOD Head and Hands" width:150 height:50
	
	on btnMergeLODHeadHands pressed do
	(
		defineGender()
		mergeLODFile()
	)
)--end LODHeadHandsMerge
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
createDialog LODHeadHandsMerge