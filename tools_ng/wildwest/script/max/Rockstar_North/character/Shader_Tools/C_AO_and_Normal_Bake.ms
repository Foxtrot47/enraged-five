-- AO and Normal Map Bake
-- A script to prepare and export AO and Normal maps off of a high res mesh.

-- Stewart Wright
-- Jan 2010

-- 29/01/10 - added save paths.  if default path doesn't exist i'll create a time stamped folder in c:\temp\baked\
-- 29/01/10 - added switch to modify tab after applying projection modifier
-- 29/01/10 - added checking for existing bake elements (light, plane and projection modifiers on low mesh)
-- 29/01/10 - added button pressed state to check that scene is prepared before attempting to bake textures
-- 29/01/10 - added "clean scene" function and button
-- 29/01/10 - added drop down menu to select export size
-- 01/02/10 - added some array resets for scenes with multiple meshes
-- 01/02/10 - added exposure control = undefined
-- 05/02/10 - added global lighting settings
-- 29/10/10 - added control over background colour on map bakes

-- ***************************************************************************************************************************
-- ***************************************************************************************************************************
-- Common script headers, setup paths and load common functions

include "rockstar/export/settings.ms"

-- Figure out the project
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")

-- Light settings are stored in a config file	
vBakesettingsfile = (theWildWest + "etc/config/characters/max_bake_settings.dat")

try (filein vBakesettingsfile) 
catch (
	messagebox ("Couldn't find project specific bake settings file. \r\nWas expecting " + vBakesettingsfile )
)

-- ***************************************************************************************************************************
-- ***************************************************************************************************************************
-- Setup some arrays that we'll be using
ObjectArray = #()
ObjectFaceCount = #()
SortedObjectArray = #()

LowMesh = null
HighMesh = null

ExportTexSize = 512

saveTexPath = null

scenePrepared = false

filterTime = null

----------------------------------------------------------------------------------------------------------------------------------
-- work out where to save to
fn saveTo =
(
	if maxfilepath == "" then
	(
		loTime = localtime
		filtTime = filterString loTime "/: "
		folTime = filtTime[1] + "_" + filtTime[2] + "_" + filtTime[3] + "-" + filtTime[4] + filtTime[5]
		
		thePath = "c:\\"
		BakePath = thePath + "Temp\\Baked\\" + folTime
		try 
		(
			makedir BakePath
		) catch()
		saveTexPath = BakePath
	)
	else
	(
		-- get filename
		thePath = maxfilepath as string
		BakePath = thePath + "Baked"
		-- create folder if needed
		try 
		(
			makedir BakePath
		) catch()
		saveTexPath = BakePath
	)
)

----------------------------------------------------------------------------------------------------------------------------------
fn saveTimeStamp =
(
	tempTime = getLocalTime()
	filterTime = "t_" + (tempTime[5] as string) + "-" + (tempTime[6] as string) + "-" + (tempTime[7] as string) + "s_d_" + (tempTime[4] as string) + "-" + (tempTime[2] as string) + "-" + (tempTime[1] as string)
)


----------------------------------------------------------------------------------------------------------------------------------
-- work out which mesh to bake off and which to bake on
fn HighLow =
	(
	-- reset some arrays so people can bake multiple meshes in a single scene
	SortedObjectArray = #()	
	ObjectFaceCount = #()
	
	ObjectArray = getCurrentSelection()
		for objsel in ObjectArray do (
			append ObjectFaceCount (getnumfaces objsel)
			)
		-- Only ever 2 objects in these arrays
		-- Assume we ALWAYS want LOW then High
		-- now find the biggest number in the face count.
		if (ObjectFaceCount[2] < ObjectFaceCount[1]) then (
			-- Swap them over
			append SortedObjectArray ObjectArray[2]
			append SortedObjectArray ObjectArray[1]
			)
		else (SortedObjectArray = ObjectArray)
	-- lets store which mesh is which
	LowMesh = SortedObjectArray[1]
	HighMesh = SortedObjectArray[2]
	)




----------------------------------------------------------------------------------------------------------------------------------
-- Start sorting out the file for baking
fn sceneSetup =
	(
	-- delete existing lighting details
	try (delete $BakerLight) catch()
	try (delete $BakerPlane) catch()
	-- add the skylight
	Skylight name:"BakerLight" pos:[lX,lY,lZ] multiplier:0.9 sky_mode:1 color:(color 255 255 255) enabled:true sky_color_map_on:false castShadows:false
	-- create the bounce plane
	Plane name:"BakerPlane" Pos:[pX,pY,pZ] length:50 lengthsegs:2 width:50 widthsegs:2
	-- create a blank white texture with default values
	meditMaterials[TempMatSlot] = standardmaterial ()
	meditMaterials[TempMatSlot].diffuse = white
	meditMaterials[TempMatSlot].specular = white
	meditMaterials[TempMatSlot].specularlevel = 0
	meditMaterials[TempMatSlot].glossiness = 10
	meditMaterials[TempMatSlot].Soften = 0.1
	-- asign our material to the selected meshes and the bounce plane
	LowMesh.material = meditMaterials[TempMatSlot]
	HighMesh.material = meditMaterials[TempMatSlot]
	$BakerPlane.material = meditMaterials[TempMatSlot]

	-- assign renderer
	renderers.current = default_scanline_renderer()

	-- Start setting up the light tracer settings
	sceneRadiosity.radiosity = SRadRad
	-- these are the one's we want to alter/make sure are active
	sceneRadiosity.radiosity.global_multiplier = SRadRadGlobMulti
	sceneRadiosity.radiosity.rays = SRadRadRays
	sceneRadiosity.radiosity.cone_angle = SRadRadConeAng
	sceneRadiosity.radiosity.color_filter = SRadRadColFilt
	sceneRadiosity.radiosity.extra_ambient = SRadRadXtraAmb
	sceneRadiosity.radiosity.bounces = SRadRadBounces
	sceneRadiosity.radiosity.volumes_on = SRadVolState
	-- the adaptive undersampling bits
	sceneRadiosity.radiosity.adaptive_undersampling_on = SRadAdapUSState
	sceneRadiosity.radiosity.initial_sample_spacing = SRadSampSpac
	sceneRadiosity.radiosity.subdivision_contrast = SRadSubdCon
	sceneRadiosity.radiosity.subdivide_down_to = SRadSubdDown
	sceneRadiosity.radiosity.show_samples = SRadShowSamp
	-- these are the defaults for things we don't need to change
	sceneRadiosity.radiosity.sky_lights_on = SRadSkyLightsState
	sceneRadiosity.radiosity.sky_lights = SRadSkyLightsCount
	sceneRadiosity.radiosity.filter_size = SRadFilterSz
	sceneRadiosity.radiosity.ray_bias = SRadRayBias
	sceneRadiosity.radiosity.object_multiplier = SRadObjMulti
	sceneRadiosity.radiosity.color_bleed = SRadObjMulti
	sceneRadiosity.radiosity.volumes = SRadVol
	
	-- turn off any exposure controls that might be enabled
	SceneExposureControl.exposureControl = undefined
	--set global lighting colours
	ambientColor = color 200 200 200
	lightTintColor = color 255 255 255
	lightLevel = 1
	

	--need more settings here, cant find the script for it though
	-- enable global supersampler
	-- set it to max 2.5 star
	-- enable supersample maps
	)




----------------------------------------------------------------------------------------------------------------------------------
-- cleanup the scene
fn cleanScene obj =
(
	try (delete $BakerLight) catch()
	try (delete $BakerPlane) catch()
	
	try 
	(	
	R2TMesh = obj.INodeBakeProperties
	R2TMesh.removeAllBakeElements() -- clear any existing bake elements
	) catch ()
	
	-- check the stack of the low mesh and delete any projection modifiers
	for checkProj = 1 to obj.modifiers.count	do
	(
	if classof obj.modifiers[checkProj] == projection then
		(
		deleteModifier obj checkProj
		)
	)
)




----------------------------------------------------------------------------------------------------------------------------------
-- add projection modifier on to LowMesh and give it a bunch of defaults
fn applyProjectionMod objLow objHigh =
(
	-- check the stack of the low mesh and delete any projection modifiers
	for checkProj = 1 to objLow.modifiers.count	do
	(
	if classof objLow.modifiers[checkProj] == projection then
		(
		deleteModifier objLow checkProj
		)
	)
	
	activeProjMod = projection()
	addmodifier objLow activeProjMod

	pmodInterface = activeProjMod.projectionModOps
	pmodInterface.AddObjectNode objHigh
	activeProjMod.resetCage()
	completeRedraw()
	
	msgText = "The scene has been setup and we're almost ready to go.\nPlease adjust the projection cage on " + (objLow.name as string) + " before baking the materials."
	messagebox msgText
	select objLow
	
	setCommandPanelTaskMode #modify -- switch to the modify panel (needed for the below code to work)
	activeProjMod.resetCage()
	activeProjMod.pushCage 0.020
	completeRedraw()
	)
	
	
	

----------------------------------------------------------------------------------------------------------------------------------
-- a function for adding elemtents to the RTT menu and exporting them
fn renderTexMaps obj MapSize uvChannel expFilePath virtualFrameBufferState =
	(
	R2TMeshPME = obj.INodeBakeProjProperties
	R2TMeshPME.enabled = true -- enable projection mapping
	
	R2TMesh = obj.INodeBakeProperties
	R2TMesh.removeAllBakeElements() -- clear any existing bake elements
	
	AO = LightingMap ()
	AO.backgroundColor=aoCol
	AO.outputSzX=MapSize
	AO.outputSzY=MapSize
	AO.fileType=expFilePath + "\AOMap_" + filterTime + ".tga"

	NM = NormalsMap ()
	NM.backgroundColor=normCol
	NM.outputSzX=MapSize
	NM.outputSzY=MapSize
	NM.fileType=expFilePath + "\NormalMap_" + filterTime + ".tga"

	R2TMesh.addBakeElement AO
	R2TMesh.addBakeElement NM
	R2TMesh.bakeChannel= uvChannel
	R2TMesh.bakeEnabled = true
	
	 --  open and close RTT.  this has to be done for the script to render properly
	macros.run "Render" "BakeDialog"
	render renderType:#bakeSelected outputwidth:MapSize outputheight:MapSize progressBar:true vfb:virtualFrameBufferState -- outputfile:(fPath+fName+fType) was overwriting the actual lightmap generated with the framebuffer render? 
	format "rendering node:% channel:%\n" obj.name R2TMesh.bakeChannel
	try (destroyDialog gTextureBakeDialog) catch()
	)
	
	
	

----------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------
--create rollout
rollout MapBake "Shake 'n Bake"
	
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Character_AO_and_Normal_Bake_Tool" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	--create buttons
	dropdownlist ddlExportSize "Export Size" items:#("128x128", "256x256", "512x512", "1024x1024", "2048x2048") selection:3 toolTip:"Choose your desired export size for the bakes"
	button btnPrepareScene "Prepare Scene" width:150 height:40 toolTip:"Get the scene ready for some material baking"
	button btnBakeMats "Bake Materials" width:150 height:40 toolTip:"Bake your AO and Normal map off the high mesh"
	button btnCleanScene "Clean-Up Scene" width:150 height:40 toolTip:"Clean up some of the stuff we added to the scene"


	on ddlExportSize selected i do
	(
		sortedSize = #(128, 256, 512, 1024, 2048)
		ExportTexSize = sortedSize[i]
	)
	
	on btnPrepareScene pressed do
	(
		-- Lets check that there are 3 objects selected
		if  selection.count == 2 then
		(
			HighLow ()
			SceneSetup ()
			applyProjectionMod LowMesh HighMesh	
		)
		else
		(
			messagebox "Please select 2 objects (Your High and Low meshes)."
		)
		scenePrepared = true
	)
	
	on btnBakeMats pressed do
	(
		if scenePrepared == true then
		(
		saveTo () -- go off and figure out where to save the textures
		saveTimeStamp () --work out the timestamp so people dont overwrite files
		-- this will setup the maps and export them
		select LowMesh -- needs to be selected for the next line to work?
		renderTexMaps LowMesh ExportTexSize 1 (saveTexPath as string) false
		clearSelection ()
		
		msgTextPaths = "The textures have been exported.\nThey were saved to " + saveTexPath
		messagebox msgTextPaths
		)
		else
		(
		msgPrepareScene = "Please run the PREPARE SCENE button first"
		messagebox msgPrepareScene
		)
	)
	
	on btnCleanScene pressed do
	(	
		if scenePrepared == true then
		(
		cleanScene LowMesh
		scenePrepared = false
		)
		else
		(
		msgCleanScene = "Have you done anything to the scene yet?"
		messagebox msgCleanScene
		)
	)
)--end of rollout "Shake 'n Bake"


----------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------
createDialog  MapBake width:180