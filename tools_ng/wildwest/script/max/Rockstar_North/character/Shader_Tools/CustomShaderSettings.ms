--Shader Value Save/Load Tools
--Stewart Wright
--Rockstar North
--12/12/08
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
clearListener()  --housekeeping
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- Load the common fucntions file
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_common.ms"
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_Rage_Shaders.ms"

--setup the array to store all the material settings
masterArray = #()
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--function for getting the shader/material values and storing them
fn getShaderValues = 
(
	masterArray = #()  --housekeeping.  empty the array before we start
	
	for MatID = 13 to 16 do  --I'm assumiong that the rage materials are in slot 13 with no more than 4 component variations (ie uppr_003)
	(
		if classof meditMaterials[MatID] == Multimaterial then  --if the material is a multisub then we want to look in it
		(
			subCount = meditMaterials[MatID].numSubs  --how many sub materials are in this bad boy?
			for SubID = 1 to subCount do
			(
				--lets make a 'disposable' array for storing the settings
				shaderArray = #()
				
				-- Need to know the shadertype
				shadertype = WhatRageShader MatID SubID
				-- diffuse, normal, specular, relect maps then normstr, specstr, specfall, reflstr, SPARE, SPARE
				theMapSlots = RAGEslots MatID SubID
				
				nmrs = RstGetVariable meditMaterials[MatID].materialList[SubID] theMapSlots[5]
				sfall = RstGetVariable meditMaterials[MatID].materialList[SubID] theMapSlots[7]
				sint = RstGetVariable meditMaterials[MatID].materialList[SubID] theMapSlots[6]
				
				append shaderArray MatID  --material slot
				append shaderArray SubID  --submaterial slot
				append shaderArray nmrs  --normal settings (bumpiness)
				append shaderArray sfall  --specular falloff
				append shaderArray sint  --specular intensity
				append shaderArray shadertype
				
				append masterArray shaderArray  --ammend our main array with the temp array and continue through the other material slots
			)
		)
		else
		(
			mText = "It doesn't seem that you have a Multisub Material setup in material channel " + MatID as string
			messagebox mText title:"Error"
		)
	)
	
)
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--function for writing out the shader values to a text file
fn writeShaderValues =
(
	outputFolder = maxfilepath  --output folder is the same as the max file
	outputFile = outputFolder + "shadersettings.txt"  --save the file as "shadersettings.txt"
	shaderFile = createFile outputFile  --now we make the text file using the above settings
	print masterArray to: shaderFile  --write the contents of our array to the text file
	close shaderFile  --housekeeping.  this closes the txt file
)
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--function for reading in the shader values from a text file
fn readShaderValues =
(
	masterArray = #()  --housekeeping.  empty the array before we start
	sourceFolder = maxfilepath
	sourceFile = sourceFolder + "shadersettings.txt"
	shaderFile = openFile sourceFile
	
	while not eof shaderFile do  --as long as we haven't reached the end of the file keep going
	(
		checkLine = readExpr shaderFile  --readExpr gets the info from the txt file
		append masterArray checkLine  --adds each line of the text file to our array
		
		--checkLine = readLine shaderFile --reads each line of the txt file
		--append masterArray checkLine  --adds each line of the text file to our array
	)
	
	close shaderFile  --housekeeping.  this closes the txt file
)
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--function for setting the read shader/material values
fn setShaderValues =
(
for cnt = 1 to masterArray.count do 
	(
		tempArray = masterArray[cnt] as array
		
		MatID = tempArray[1]  --telling max that the 1st value in the tempArray is the MatID
		SubID = tempArray[2]  --telling max that the 2nd value in the tempArray is the SubID
		nmrs = tempArray[3]  --etc
		sfall = tempArray[4]
		sint = tempArray[5]
		stype = tempArray[6]
		
		--diffuse, normal, specular, relect maps then normstr, specstr, specfall, reflstr, SPARE, SPARE
		theMapSlots = RAGEslots MatID SubID
		
		RstSetShaderName meditMaterials[MatID].materialList[SubID] stype  --this should set the shader
		
		RstSetVariable meditMaterials[MatID].materialList[SubID] theMapSlots[5] nmrs --setting the normal value
		RstSetVariable meditMaterials[MatID].materialList[SubID] theMapSlots[7] sfall  --setting the spec falloff
		RstSetVariable meditMaterials[MatID].materialList[SubID] theMapSlots[6] sint  --setting the spec intensity
	)
)
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--create rollout
rollout ShaderRoll "Shader Saver"
(
	
	label desc "Save Your Custom Shader Settings" pos:[5,5] align:#right width:120 height:35 
	
	--create buttons
	button getShader "Get Shader Values" width:120 height:40 toolTip:"Checks your material editor and stores all the shader settings"
	button writeShader "Write Shader Values" width:120 height:40 toolTip:"Reads in the shader values from an external file"

	On getShader pressed do
	(
		getShaderValues ()
		writeShaderValues ()
	)
	
	On writeShader pressed do
	(
		readShaderValues()
		setShaderValues()
	)

)--end of rollout "Shader Saver"
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--Create new floater
ShaderFloater = newRolloutFloater "Shader Saver" 145 165

--Add the rollouts to floater 
addRollout ShaderRoll ShaderFloater