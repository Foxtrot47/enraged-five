/*
Selection Set to User Defined Properties script
Stewart Wright
Rockstar North
06/04/10
07/04/10 - added functions to store and restore scene state during script use
*/
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- load the common functions
filein "rockstar/export/settings.ms"

-- Figure out the project data
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()
theProjectPedFolder = theProjectRoot + "art/peds/"

-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

theKitchenSink = #()

-------------------------------------------------------------------------------------------------------------------------
-- create an array with only the geo in it.
fn geoOnly = (
	-- make sure the array is empty before we move on
	theKitchenSink = #()
	
	-- unhide all by categry
	hideByCategory.none()	
	-- unhide all
	unhide (for obj in objects where obj.ishidden collect obj)
	
	-- delete those pesky particle views
	try (delete $particle*) catch ()
	
	--hide everything that isn't geometry by category
	hideByCategory.shapes = true
	hideByCategory.lights = true
	hideByCategory.cameras = true
	hideByCategory.helpers = true
	hideByCategory.spacewarps = true
	hideByCategory.particles = true
	hideByCategory.bones = true
	
	-- select everything that's visible
	max select all
	theKitchenSink = getCurrentSelection()
	
	-- unhide all by categry
	hideByCategory.none()
	
	-- clear selection
	clearSelection()
)-- end geoOnly


-------------------------------------------------------------------------------------------------------------------------
-- tidy up the user defined properties by sorting ti alphabetically
fn reorderTags tagMe = 
(
		currentUDP = getUserPropbuffer tagMe
		filterUDP = filterString currentUDP "\r\n"
		alphUDP = sort filterUDP
	
		tag = ""	
		for t = 1 to alphUDP.count do
		(
			tag = tag + alphUDP[t] + "\r\n"
			setUserPropbuffer tagMe tag
		)
)-- end reorderTags


-------------------------------------------------------------------------------------------------------------------------
-- delete any selectionset tags
fn delSelectionTags =
(
	for os = 1 to theKitchenSink.count do
	(
		checkGeo = theKitchenSink[os]
		
		keepUDP = #()
		currentUDP = getUserPropbuffer	checkGeo
		filterUDP = filterString currentUDP "\r\n"
		
		for f = 1 to filterUDP.count do (
			tempUDP = uppercase filterUDP[f]
			shortUDP = substring tempUDP 1 7

			if (shortUDP != "SELECTI" and shortUDP != "IGNORE=") then
			append keepUDP tempUDP
		)

		tag = ""
			
		if keepUDP.count == 0 then setUserPropbuffer checkGeo tag
		
		else 
		
		for k = 1 to keepUDP.count do
		(
			tag = tag + keepUDP[k] + "\r\n"
			setUserPropbuffer checkGeo tag
		)
		reorderTags checkGeo
	)
)-- end delSelectionTags


-------------------------------------------------------------------------------------------------------------------------
-- go through all the selection sets and tag geometry
fn tagSelectionInfo =
(
	tagThis = ""
	
	delSelectionTags () --delete any existing selectionset tags
	
	for ss = 1 to selectionsets.count do
	(
		geoFound = #()
		-- convert the selection set into an array
		matchSS = for obj in SelectionSets[ss] collect obj --turn sel.set into array
		-- check the selection set array for geo only and add it to a seperate array
		geoFound = for obj in theKitchenSink where findItem matchSS obj > 0 collect obj -- makes an array with the geo only
			
		for tag = 1 to geoFound.count do
		(	
			tempString = substring (selectionsets[ss] as string) 14 1
			if tempString == "*" then 
			(
				tempIgnoreString = substring (selectionsets[ss] as string) 15 50
				tempIgnoreString = "Ignore=" + tempIgnoreString
				tagThis = tempIgnoreString
			)
			else
			(
				tempSelectionString = substring (selectionsets[ss] as string) 14 50
				tempSelectionString = "SelectionSet=" + tempSelectionString
				tagThis = tempSelectionString
			)
			
			tagMe = geoFound[tag] 
			
			oldTag = getUserPropbuffer tagMe
			tagThis = oldTag+"\r\n"+tagThis
			tagThis = uppercase tagThis
			setUserPropbuffer tagMe tagThis

			reorderTags tagMe --tidy up the tags
		)
	)
)-- end tagSelectionInfo
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
rollout SelectionToUDP "Quick and Dirty Skin Tool"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Character_Selectionset_to_UDP" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)
	
	button btnWriteTags "Write Selection Set Tags" width:140 height:60 tooltip:"Write the selection set tags to the geometry"
	button btnDelTags "Delete Selection Set Tags" width:140 height:60 tooltip:"Delete any existing selectionset tags."

	on SelectionToUDP open do
		(
		storeSceneState()
		)
	
	on btnWriteTags pressed do
	(
		--storeSceneState()
		geoOnly()
		tagSelectionInfo()
		restoreSceneState sceneState sceneVisible
	)
	
	on btnDelTags pressed do
	(
		--storeSceneState()
		geoOnly()
		delSelectionTags()
		restoreSceneState sceneState sceneVisible
	)
)-- end SelectionToUDP


createDialog SelectionToUDP width:150