-- Set up a default scene for characters
-- Kill all scene materials, then load a default texture setup
-- Copy the default materials to the working folder
-- Set up the correct names and paths to the textures

-- Rick Stirling
-- 2005

-- ********************************************************************************************
-- Functions
-- ********************************************************************************************


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())



-- ********************************************************************************************
-- Rollouts
-- ********************************************************************************************


rollout modelname "Rockstar Character Tools"
(




	button makedummy "Make Dummy" pos:[5,20] width:100 height:30
	button loddummy "Lod Dummy" pos:[110,20] width:100 height:30
	button rootdummy "Root Dummy" pos:[110,60] width:100 height:30
	button csrootdummy "CS Root Dummy" pos:[5,60] width:100 height:30
	--button superdummy "Super Lod Dummy" pos:[5,60] width:100 height:30





	


	on makedummy pressed do
	(
		fileIn "create_parent_dummy.ms"
	)


	on loddummy pressed do
	(
		fileIn "create_lod_dummy.ms"
	)

	on rootdummy pressed do
	(
		fileIn "root_dummy.ms"
	)


	on csrootdummy pressed do
	(
		fileIn "root_dummy_CS.ms"
	)
)







-- Create floater 
theNewFloater = newRolloutFloater "Rockstar Character Tools" 230 160

-- Add the rollout section
addRollout modelname theNewFloater
	