-- Create lod set
-- add to lod components


-- 2nd September 2009: Stewart Wright. Converted to Agent rigs
-- 2nd September 2009: Rick Stirling. Bug fixing and tidy up
-- 8th December 2009: Stewart Wright.  Hard coded file paths switched to dynamic


--Work out the project so we don't have to use hardcoded filein paths etc
project = rsconfiggetprojectname()
filein ("X:/"+project+"/tools/dcc/current/max2009/scripts/character/includes/FN_common.ms")
project = rsconfiggetprojectname()
filein ("X:/"+project+"/tools/dcc/current/max2009/scripts/character/Rigging_tools/Jimmy_Rig_Bone_Skin_Order.dat")


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())


-- Creates a selection set of the LOD bones.
fn makelodbones = 
(
	clearSelection()
	
	-- Loop through the bones named in the data file and select them all.
	for bc = 1 to LodBoneNameOrder.count do (
		selectmore (getnodebyname BoneNameOrder[bc])
	)	
	
	-- Create the new selection set
	selectionsets["LODbones"]=$
	
)




-- Adds the skin modifier and LOD bones to selected components
fn skinselectedLODS = 
(
	comps = #()
	
	-- for selected object, apply skin modifier, load the skin bones
	if $ == undefined  then (
		messagebox "Nothing selected"
		return 0
	)
	

	comps = getCurrentSelection()
	
	
	for i = 1 to comps.count do
	(
		-- select each component in turn
		select comps[i]
	
		-- add a skin modifier
		modPanel.addModToSelection (Skin ()) ui:on
		
		-- Add the lod bones
		--  Loop through the bones defined in the data file.
		for bc = 1 to LODBoneNameOrder.count do (
			try (skinOps.addBone $.modifiers[#Skin] (getnodebyname LODBoneNameOrder[bc]) 1) catch()
		)
		
		-- set the bone limit and clear zero weight limit, hide envelopes
		$.modifiers[#Skin].bone_Limit = 4
		$.modifiers[#Skin].clearZeroLimit = 0.06
		$.modifiers[#Skin].showNoEnvelopes = on
	)
	
	select comps
)






-- Clear any weights that are set to 0
fn clearzero = (

	comps = #()
	
	-- for selected object, apply skin modifier, load the skin bones
	if $ == undefined  then (
		messagebox "Nothing selected"
		return 0
	)
	
	comps = getCurrentSelection()
	
	for i = 1 to comps.count do 	(
		
		select comps[i]
		
		-- select all the verts
		subobjectLevel = 1
		$.modifiers[#Skin].Filter_Vertices = on
		
		theSkin = selection[1].modifiers[#Skin]
		n = skinOps.getNumberVertices theSkin
		
		skinOps.SelectVertices $.modifiers[#Skin] #{1..n}
		
	) -- end for loop
)






fn bakeweights = (

	comps = #()
	
	-- for selected object, apply skin modifier, load the skin bones
	if $ == undefined  then (
		messagebox "Nothing selected"
		return 0
	)
	
	comps = getCurrentSelection()
	
	for i = 1 to comps.count do 
	(
		-- select each component in turn
		select comps[i]

		-- select all the verts
		subobjectLevel = 1
		$.modifiers[#Skin].Filter_Vertices = on
		
		theSkin = selection[1].modifiers[#Skin]
		n = skinOps.getNumberVertices theSkin
		
		skinOps.SelectVertices $.modifiers[#Skin] #{1..n}
		skinOps.bakeSelectedVerts $.modifiers[#Skin]
		
	) -- end for loop
)






fn addallbonestoLODS = (
	-- for selected object, apply skin modifier, load the skin bones
	comps = #()
	
	-- for selected object, apply skin modifier, load the skin bones
	if $ == undefined  then (
		messagebox "Nothing selected"
		return 0
	)
	
	comps = getCurrentSelection()
	
	for i = 1 to comps.count do 	(
		-- select each component in turn
		select comps[i]

		-- add the entire skeleton
		-- Use the bone ordering that we loaded in
		-- Add each bone to the skin modifier, using try/catch each time since not all bones will be used 
		-- Make sure the bone is not already in the skin modifer
		
		missingBoneArray =  differenceInArray BoneNameOrder LODBoneNameOrder
		
		print missingBoneArray
		 
		for bc = 1 to missingBoneArray.count do (

			try (skinOps.addBone $.modifiers[#Skin] (getnodebyname missingBoneArray[bc]) 1) catch()
		)
		
	)
)









rollout modelname "LOD tools"
(


	button btncomset "LOD SelSet" pos:[10,10] width:120 height:30
	button btnLODboneset "LOD boneset" pos:[10,50] width:120 height:30
	button btnskinLODS "Skin LODS in Set" pos:[10,90] width:120 height:30
	button btnclearem "Clear Zero" pos:[10,130] width:120 height:30
	button btnbakeem "Bake 'em" pos:[10,170] width:120 height:30
	button btnaddallbones "Add all bones" pos:[10,210] width:120 height:30
	


	on btncomset pressed do selectionsets["LOD Geo"]=$
	on btnLODboneset pressed do makelodbones()



	on btnskinLODS pressed do
	(
		set1 = selectionSets ["LOD Geo"]
		try (select set1) catch()
		skinselectedLODS()
	)


	



	on btnclearem pressed do
	(
		clearzero()
		set1 = selectionSets ["LOD Geo"]
		select set1
	)

	
	on btnbakeem pressed do
	(
		bakeweights()
		set1 = selectionSets ["LOD Geo"]
		select set1
	)
	
	
	on btnaddallbones pressed do
	(
		addallbonestoLODS()
		
			
		--whats the name of the character?
		thename = maxfilename as string
		
		shortname = filterstring thename "."
		
		thedummyname = shortname[1] + "-L"
		
		actionMan.executeAction 0 "40021"  -- Selection: Select All
		

		for obj in selection do
		(
			if obj.name == thedummyname then
			(
				delete obj
			)
		) 
		
		Dummy pos:[0,0,4] isSelected:on
		scale $ [0.1,0.1,0.1]
		
		ResetTransform $
		
		$.name = thedummyname
		theparent = $
		
		-- link the lods to the dummy
		set1 = selectionSets ["LOD Geo"]
		select set1
		
		$.parent = theparent
			
	)
	
)



-- Create floater 
theNewFloater = newRolloutFloater "Rockstar Character Tools" 150 280

-- Add the rollout section
addRollout modelname theNewFloater