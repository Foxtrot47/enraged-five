/*
Create Dummy Menu
Stewart Wright
Rockstar North
26/03/10

Make the Dummy and link components to it.
Filter LOD components and link them to the LOD dummy

Update 29/03/10 - moved deleteDummy function to prevent errors on 1st run.  Added auto dummy function and checks for LOD objects
*/----------------------------------------------------------------------------------------------------------------------
filein "rockstar/export/settings.ms"

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
-------------------------------------------------------------------------------------------------------------------------
theKitchenSink = #()
High = #()
Low = #()
dummyName = ""
LODDummyName = ""
deleteWhat = ""
deleteThisDummy = ""


-------------------------------------------------------------------------------------------------------------------------
-- prep the scene
fn highAndLow = (
	-- unhide all by categry
	hideByCategory.none()	
	-- unhide all
	unhide (for obj in objects where obj.ishidden collect obj)
	
	-- delete those pesky particle views
	try (delete $particle*) catch ()
	
	--hide everything that isn't geometry by category
	hideByCategory.shapes = true
	hideByCategory.lights = true
	hideByCategory.cameras = true
	hideByCategory.helpers = true
	hideByCategory.spacewarps = true
	hideByCategory.particles = true
	hideByCategory.bones = true
	
	-- select everything that's visible
	max select all
	theKitchenSink = getCurrentSelection()
	-- unselect the LODs
	deselect $*LOD
	High = getCurrentSelection()	
	-- work out the LODs
	clearselection ()
	select $*LOD
	Low = getCurrentSelection()	

	-- unhide all by categry
	hideByCategory.none()
	
	-- clear selection
	clearSelection()
	
	fileName = uppercase maxfilename as string
	shortName = filterstring filename "."
	dummyName = shortName[1]
	LODDummyName = dummyName + "-L"
)  -- end highAndLow

-------------------------------------------------------------------------------------------------------------------------
fn makeBigDummy = (
	-- use our function to delete dummy if it exists
	--deleteDummy DummyName
	
	--make dummy
	bigDummy = Dummy pos:[0,0,0] isSelected:on
	bigDummy.name = dummyName
	
	-- link stuff to it
	High.parent = bigDummy
	
	-- clear selection
	clearSelection()

	print "Export Dummy Created"
) -- end makeBigDummy

-------------------------------------------------------------------------------------------------------------------------
fn makeLODDummy = (
	-- use our function to delete dummy if it exists
	--LODDummyName = dummyName + "-L"
	
	deleteDummy LODDummyName
	
	--make LOD dummy
	LODDummy = Dummy pos:[0,0,4] isSelected:on
	scale $ [0.1,0.1,0.1]
	ResetTransform $
	LODDummy.name = LODDummyName
	
	-- link stuff to it
	Low.parent = LODDummy
	
	-- clear selection
	clearSelection()
	
	print "LOD Dummy Created"
) -- end makeLODDummy

-------------------------------------------------------------------------------------------------------------------------
fn deleteDummy deleteWhat = (
	-- delete dummy if it exists
	for h in helpers do -- iterate through all of the helpers in the scene
	(
		if (classof h==dummy) and (isgrouphead h==false) then -- check to see that the object is a dummy, and not a group head
		(
			matchDummy = uppercase h.name
			if matchDummy == deleteWhat then 
			(
				deleteThisDummy = h
			)
		)
	)
	try
		(
			delete deleteThisDummy
			print "Dummy Deleted"
		) catch ()
) -- end deleteDummy


-------------------------------------------------------------------------------------------------------------------------
rollout DummyTools "Character Dummy Tools"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Character_Dummy_Tool" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)
	
	button btnAutoDummy "Auto Dummy" width:100 height:50 tooltip:"Make an Export and LOD dummy if needed and link all components."
	button btnExportDummy "Export Dummy" width:100 height:30 tooltip:"Make the main export dummy and link the non LOD components to it."
	button btnLODDummy "LOD Dummy" width:100 height:30 tooltip:"Make a LOD dummy and link every mesh suffixed with 'LOD' to it."

	on btnExportDummy pressed do
	(
		highAndLow ()
		-- use our function to delete dummy if it exists
		deleteDummy DummyName
		makeBigDummy ()
	)

	on btnLODDummy pressed do
	(
		highAndLow()
		if low.count != 0 then
			(
				deleteDummy LODDummyName
				makeLODDummy ()
			)
		else
			(
				messagebox "I can't find any LODs.  Are they tagged?"
			)
	)
	
	on btnAutoDummy pressed do
	(
		highAndLow ()
		if low.count != 0 then
			(
				-- make the big stuff
				deleteDummy DummyName
				makeBigDummy ()
				-- make the wee stuff
				deleteDummy LODDummyName
				makeLODDummy ()
			)
		else
			(
				deleteDummy DummyName
				makeBigDummy ()
			)
	)
)

createDialog DummyTools width:120