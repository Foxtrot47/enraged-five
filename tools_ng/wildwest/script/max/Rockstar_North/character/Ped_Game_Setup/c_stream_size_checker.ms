-- Figure out the project
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

-------------------------------------------------------------------------------------------------------------------------
whichStreamState = 1
whichFilesState = 1

whichStream = #("x:/streamGTA5/componentpeds/", "x:/streamGTA5/pedprops/", "x:/streamGTA5/streamedpeds/")
whichFiles = #("*.itd", "*.idd")

itdMaster = #()

outputName = undefined
-------------------------------------------------------------------------------------------------------------------------

--changing the X parameter you pass determines which array to sort by
fn sortByXMember arr1 arr2 x:1 =
(
	case of
	(
		(arr1[x] > arr2[x]):-1
		(arr1[x] < arr2[x]):1
		default:0
	)
)--end sortByXMember
-------------------------------------------------------------------------------------------------------------------------

fn getFilesRecursive root pattern =
(
	subDirArray = #(root)
	for d in subDirArray do
	join subDirArray (GetDirectories (d+"/*"))

	foundFiles = #()

	for f in subDirArray do
	join foundFiles (getFiles (f + pattern))
	foundFiles
)--end getFilesRecursive
-------------------------------------------------------------------------------------------------------------------------

fn calcSizes =
(
	whatStream = whichStream[whichStreamState] + whichFiles[whichFilesState]
	theFiles = getfiles whatStream 

	/*--get all files from the folder and all its subfolders
	getFilesRecursive whichStream[whichStreamState] whichFiles[whichFilesState]
	theFiles = foundFiles*/
	
	itdMaster = #()

	for t=1 to theFiles.count do
	(
		append itdMaster #(#(),#())
	)

	for f=1 to theFiles.count do
	(
		itdMaster[f][1] = uppercase(getfilenamefile theFiles[f])
		itdMaster[f][2] = getFileSize theFiles[f]/1024
	)

	--sort the master array from highest to lowest based on the file size
	qsort itdMaster sortByXMember x:2
)-- end calcSizes
-------------------------------------------------------------------------------------------------------------------------

fn csvOutput =
(
	if whichFilesState == 1 do
	(
		outputName = whichStream[whichStreamState] + " texture_size.csv"
		backupName = whichStream[whichStreamState] + " old_texture_size.csv"
	)
	if whichFilesState == 2 do
	(
		outputName = whichStream[whichStreamState] + " drawable_size.csv"
		backupName = whichStream[whichStreamState] + " old_drawable_size.csv"
	)

	try deleteFile backupName catch()
	try copyFile outputName backupName catch()

	outputFile = createFile outputName

	for p=1 to itdMaster.count do
	(
		--format (whichStream[whichStreamState] + whichFiles[whichFilesState] + ",") to:outputFile --"," moves to the next column
		format (itdMaster[p][1] as string + ",") to:outputFile --"," moves to the next column
		format (itdMaster[p][2] as string + "KB\n") to:outputFile --"\n" moves to the next row
	)	
	close outputFile
)-- end csvOutput
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
rollout streamChecker "Stream Size Checker"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Size_Checker_Tool" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	radiobuttons radStream labels:#("Peds", "Ped Props", "Players") default:whichStreamState
	radiobuttons radFiles labels:#("Textures", "Meshes") default:whichFilesState
	button btnCalcSizes "Calculate Sizes" width:205 height:40
	button btnOpenResults "Open File" width:165 height:40
	button btnOpenFolder "Open Folder" width:165 height:40
	
	--monitor radStream buttons
	on radStream changed theState do
		(whichStreamState = theState)
	--monitor radradFiles buttons
	on radFiles changed theState do
		(whichFilesState = theState)
	
	on btnCalcSizes pressed do
		(
			calcSizes()
			csvOutput()
		)
	on btnOpenResults pressed do
		(
			try (ShellLaunch outputName "") catch()
		)
	on btnOpenFolder pressed do
		(
			try (ShellLaunch whichStream[whichStreamState] "") catch()
		)
)--end  streamchecker tool
-------------------------------------------------------------------------------------------------------------------------

rollout pedChecker "Ped Stats"
(
	button btnPedSize "Ped Stats" width:205 height:40
	
	on btnPedSize pressed do
		(
			filein (theWildWest + "script/max/Rockstar_North/character/Ped_Game_Setup/c_ped_size.ms")
		)
)--end  pedchecker tool
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- Create floater 
sizeCheckFloater = newRolloutFloater "Size Checking Tools" 230 310
-- Add the rollout section
addRollout streamChecker sizeCheckFloater
addRollout pedChecker sizeCheckFloater