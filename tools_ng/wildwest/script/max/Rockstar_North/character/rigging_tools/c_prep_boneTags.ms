--tool to auto generate the xml needed for the bonetag.xml file used by motionbuilder exporter

clearListener()

for i in selection do
(
	currentUDP = getUserPropbuffer i
	filterUDP = filterString currentUDP "\r\n"
	if filterUDP[1] != undefined then
	(
		NewfilterUDP = filterstring filterUDP[1] "= "
		if newFilterUDP[2] != undefined do
		(
			udpBoneTag = newFilterUDP[2]
			print ("<param name='"+i.name+"' id='"+udpBoneTag+"' />")--+"\r\n")
		)
	)
	else
	(
		print ("WARNING! ** "+i.name+" has no bonetag.")
	)
)