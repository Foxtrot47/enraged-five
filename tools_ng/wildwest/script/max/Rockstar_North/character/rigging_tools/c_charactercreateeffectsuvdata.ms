-- characterCreateEffectsUVData.ms
-- Rockstar North
-- Matt Rennie Nov 2010
--Tool to create multiple uv data channels for effects data based off proximity to bones
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Includes\\FN_Rigging.ms")	



filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())
	
	

global initialObjectArray = #()
global volObjects = #()
global geoMesh = #()

-- global meshToSplit = undefined
global uvEffectsGuiRoll = undefined
global skinOrNot = undefined

global polySelections = #( --array which gets dynamically populated with poly selections for each volume
	#(),
	#(),
	#(),
	#(),
	#(),
	#()
	)
	
struct rigBoneStruct (rb_bone, rb_Radius, rb_Length, rb_Trans)


rigBoneListStruct = #( --array of all skeleton bones which we want to split mesh for

	--IT IS ESSENTIAL THIS IS DONE IN A SPECIFIC ORDER
-- 	( rigBoneStruct rb_bone:$SKEL_Head  rb_Radius: 0.15 rb_Length:($SKEL_Head.length * 2) rb_Trans:(matrix3 [-1,0.000145016,-0.000328264] [-5.81659e-005,-0.96816,-0.250334] [-0.000354256,-0.250334,0.96816] [-0.000112729,0.0139014,1.59917]) ),	
	( rigBoneStruct rb_bone:$SKEL_L_UpperArm  rb_Radius: 0.1 rb_Length:($SKEL_L_UpperArm.length * 2.8) rb_Trans:undefined ), 

	( rigBoneStruct rb_bone:$SKEL_R_UpperArm  rb_Radius: 0.1 rb_Length:($SKEL_R_UpperArm.length  * 2.8) rb_Trans:undefined ),
	
	( rigBoneStruct rb_bone:$SKEL_Neck_1  rb_Radius: 0.15 rb_Length:($SKEL_Neck_1.length * 3) rb_Trans:(matrix3 [1.05,0.0,0.0] [-0.0,1.05,6.7] [-0.0,-6.69,1.05] [-8.27,0.01,1.45]) ),
	
	( rigBoneStruct rb_bone:$SKEL_L_Thigh  rb_Radius: 0.18 rb_Length:($SKEL_L_Thigh.length * 2.4) rb_Trans:undefined ), 
	
	( rigBoneStruct rb_bone:$SKEL_R_Thigh  rb_Radius: 0.18 rb_Length:($SKEL_R_Thigh.length * 2.4) rb_Trans:undefined ), 
	
	( rigBoneStruct rb_bone:$SKEL_Spine0  rb_Radius: 0.25 rb_Length:($SKEL_Spine0.length * 13) rb_Trans:(matrix3 [-1,-0.000168886,1.60497e-005] [0.000169019,-0.999968,0.00799049] [1.47042e-005,0.00799066,0.999969] [1.22934e-007,0.0171712,0.972031]) )

	)

	
	
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------


fn createVols = 
(
	--firstly remove any existing volSelectors
	currentSelection = #()
	if selection != undefined do
	(
		currentSelection = selection --this gets whatever we currently have selected
	)
	clearSelection()
	select $volSelector_*
	delete selection
	
	for obj in currentSelection do
	(
		if obj != undefined do
		(
			print ("trying to pick "+currentSelection[obj].name)
			selectMore currentSelection[obj] 
		)
	)
	
		--******************************************************************************************
	--NOW CREATE THE VOLUMES FOR DETACHING THE MESH INTO PIECES
	--******************************************************************************************
	for i = 1 to rigBoneListStruct.count do
	(
		volTrans = undefined
		
		currentObj = getNodeByName ("volSel_"+rigBoneListStruct[i].rb_bone.name)
		if currentObj != undefined do
		(
			delete currentObj
		)
		volSelector = Cylinder smooth:on heightsegs:1 capsegs:1 sides:8 height:rigBoneListStruct[i].rb_Length radius:rigBoneListStruct[i].rb_Radius mapcoords:on pos:[0,0,0] isSelected:on
		volSelector.name = ("volSel_"+rigBoneListStruct[i].rb_bone.name)
		volSelector.parent = rigBoneListStruct[i].rb_bone
		
		if rigBoneListStruct[i].rb_Trans == undefined then 
		(
			volTrans = rigBoneListStruct[i].rb_bone.transform
			volSelector.transform = ( in coordsys parent volTrans)
			in coordsys parent rotate volSelector (angleaxis 90 [0,1,0])
		)
		else --this allows us to offset the transforms from the bone.
		(
			volTrans = rigBoneListStruct[i].rb_Trans
			volSelector.transform = ( in coordsys parent volTrans)
		)
		volSelector.scale = [1.05,1.05,1.05]
		convertTo volSelector PolyMeshObject
		if (substring volSelector.name 15 7) == "R_Thigh" then --this bit will trim the thigh volume so it doesnt pick the other leg
		(
			print ("Trimming "+volSelector.name)
			trimmerBox = Box lengthsegs:1 widthsegs:1 heightsegs:1 length:2 width:1 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
			boolObj.createBooleanObject volSelector trimmerBox 4 5
			boolObj.setBoolOp volSelector 3
-- 			convertTo volSelector PolyMeshObject
		)
		else
		(
			if (substring volSelector.name 15 7) == "L_Thigh" do
			(
				print ("Trimming "+volSelector.name)
				trimmerBox = Box lengthsegs:1 widthsegs:1 heightsegs:1 length:2 width:1 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [-0.49,1.06454,0.862417]) isSelected:on
				boolObj.createBooleanObject volSelector trimmerBox 4 5
				boolObj.setBoolOp volSelector 3
-- 				convertTo volSelector PolyMeshObject				
			)
		)
		
		if (substring volSelector.name 15 10) == "L_UpperArm" do
			(
				print ("Trimming "+volSelector.name)
				trimmerBox = Box lengthsegs:1 widthsegs:1 heightsegs:1 length:2 width:1 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [-0.337027,1.06454,0.862417]) isSelected:on
				in coordsys world trimmerBox.position = [($Skel_L_UpperArm.position[1] - 0.498314), 1, 1]
				boolObj.createBooleanObject volSelector trimmerBox 4 5
				boolObj.setBoolOp volSelector 3
-- 				convertTo volSelector PolyMeshObject				
			)
			
		if (substring volSelector.name 15 10) == "R_UpperArm" do
			(
				print ("Trimming "+volSelector.name)
				trimmerBox = Box lengthsegs:1 widthsegs:1 heightsegs:1 length:2 width:1 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.337,1.06454,0.862417]) isSelected:on
				in coordsys world trimmerBox.position = [($Skel_R_UpperArm.position[1] + 0.498314), 1, 1]
				boolObj.createBooleanObject volSelector trimmerBox 4 5
				boolObj.setBoolOp volSelector 3
-- 				convertTo volSelector PolyMeshObject				
			)
			
		if volSelector.name == "volSel_SKEL_Spine0" do
			(
				--first we need to offset the position so the bottom of this volume is at the height of the thighs.
				print ("adjusting volSel_SKEL_Spine0")
				thighPos = (in coordsys world $SKEL_R_Thigh.position[3])
				volSelectorPos = (in coordsys world volSelector.position)
				in coordsys world volSelector.position = [volSelectorPos[1],volSelectorPos[2],thighPos]

			)
			
		append volObjects volSelector		
	)
	
	clearSelection()
	for picked = 1 to geoMesh.count do
	(
		selectMore geoMesh[picked]
	)
)

global volSelectedVerts = #()

fn selectMeshes currentObj = 
(
	
	runSkelTransfer currentObj --save out skinning
	print "skin weights saved off"
-- 	select currentObj
	collapseStack currentObj
	print "stack collapsed"
	createDialog progBar width:300 Height:30
	undo off
	(
-- 		geoMeshes = #()
		
-- 		disableSceneRedraw()
-- 		for i = 1 to volObjects.count do
		for i = 1 to 1 do
		(
			print ("current vol object = "+(volObjects[i].name as string))
			--we need to build selections of faces per volume object
			-- once we have these lists, we then need to ensure that we dont have duplicate faces in each array
			--once this is done we then know which faces belong to which volume (these need to be exclusive)

			modPanel.setCurrentObject currentObj.baseObject
			modPanel.addModToSelection (Vol__Select ()) ui:on
			currentObj.modifiers[#Vol__Select].level = 2
			currentObj.modifiers[#Vol__Select].node = volObjects[i]
			currentObj.modifiers[#Vol__Select].volume = 3
				
			modPanel.addModToSelection (Edit_Poly ()) ui:on
				subobjectlevel = 4
			currentObj.modifiers[#Edit_Poly].useStackSelection = true
			
				print ("vol selects configured")
-- 			polySelections[i] = currentObj.modifiers[#Edit_Poly].GetSelection #Face	
			
			--volSelectedVerts = currentObj.modifiers[#Edit_Poly].GetSelection #Vertex

-- 			select currentObj 
			modPanel.addModToSelection (Uvwmap ()) ui:on
			currentObj.modifiers[#UVW_Mapping].maptype = 1

			currentObj.modifiers[#UVW_Mapping].gizmo.rotation = (volObjects[i].Rotation as eulerangles)
			currentObj.modifiers[#UVW_Mapping].gizmo.scale = volObjects[i].Scale
				
			modPanel.addModToSelection (Unwrap_UVW ()) ui:on	
			subobjectLevel = 1
			currentObj.modifiers[#unwrap_uvw].unwrap.edit ()
-- 			currentObj.modifiers[#unwrap_uvw].unwrap5.setSelectedGeomVerts volSelectedVerts
				
				volSelectedVerts = currentObj.modifiers[#unwrap_uvw].unwrap5.getSelectedGeomVerts()

				print ("currentObj.modifiers[#unwrap_uvw].unwrap6.selectVerticesByNode "+volSelectedVerts+" currentObj")
			currentObj.modifiers[#unwrap_uvw].unwrap6.selectVerticesByNode volSelectedVerts currentObj

--******************************************************************************************************				
-- need to pause here so i can examine results...				
--******************************************************************************************************				
			currentObj.modifiers[#unwrap_uvw].unwrap.detachEdgeVertices
				
			currentObj.modifiers[#unwrap_uvw].MoveSelected [(i +1),(i +1),0]
				print ("uv-ing done")
	

-- 			collapseStack currentObj
				messageBox ("collapsed stack after "+volObjects[i].name)
				--print "stack collapsed"
			progBar.prog.value = (100.*i/volObjects.count)
		)
		
		--OK NOW WE HAVE BUILT POLYGON ARRAYS. NOW ITS TIME TO DO THE UV SHITE
	)
	
	-- now reload skinning
-- 	modPanel.addModToSelection (Skin ()) ui:on
-- 	currentObj.modifiers[#Skin].bone_Limit = 4

-- 	
-- 	importBonesAndWeights currentObj
-- 	print "weights reloaded"
	
		destroyDialog progBar
		

)

fn uvEffects = 
(
	
	if ((uvEffectsGuiRoll != undefined) and (uvEffectsGuiRoll.isDisplayed)) do
		(destroyDialog uvEffectsGuiRoll)
	
	
	rollout uvEffectsGuiRoll "uvEffects" 
	(
		
-- 		button btnCombineGeo "Combine Geo" pos:[15,05] width:120 height:25
		button btnGenVol "Generate Volumes" pos:[15,30] width:120 height:25
		button btnSelMesh "Selection" pos:[15,55] width:120 height:25


		on btnGenVol pressed do
		(
			
			for obj in selection do
			(
				appendIfUnique geoMesh obj
			)
			createVols()
		)

		on btnSelMesh pressed do
		(

			
			validObjs = 0
			
			for i = 1 to geoMesh.count do
			(
				if (geoMesh[i].baseObject as string) != ("Editable Poly") do
				(
					validObjs = (validObjs  + 1)
					print (geoMesh[i].name+" is not a editable poly baseobject")
				)
				
			)
			
			if validObjs == 0 then
			(
				for i = 1 to geoMesh.count do
				(
					print ("running selectMeshes on "+geoMesh[i].name)
					selectMeshes geoMesh[i]
				)
			)
			else
			(
				messagebox "Warning please select only editable poly objects"
			)
		)		
		

	)--end uvEffectsGuiRoll	

	createDialog uvEffectsGuiRoll 150 135 pos:[1250, 100] --main window size
)

clearListener()
uvEffects()