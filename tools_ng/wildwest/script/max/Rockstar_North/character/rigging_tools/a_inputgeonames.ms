
filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

fn inputGeoNames = 
(
	clearListener()
	input_name = undefined

	if input_name == undefined do
	(
		input_name = getOpenFileName caption:"Name input file" types:"NameData (*.nam)|*.nam|All Files (*.*)|*.*|"
	)

	if input_name != undefined then
	(
		f = openfile input_name
		inputData = #() -- define as array
		while not eof f do
		(
			append inputData (filterstring (readLine f) ",")
		)
		close f

		for i = 1 to inputData.count do
		(
			inputObjName = inputData[i][1] as string
			print inputObjName
			for obj in objects do
			(
				objName = obj.name
				objNameLength = objName.count
				subStringName = substring inputObjName 1 objNameLength 
				if subStringName == objName do
				(
					print ("changing "+obj.name+" to "+inputObjName)
					obj.name = inputObjName
				)
-- 				else
-- 				(
-- 					print ("subStringName = "+subStringName+" but currObjname = "+obj.name)
-- 				)
			)
		)
	)
)

inputGeoNames()