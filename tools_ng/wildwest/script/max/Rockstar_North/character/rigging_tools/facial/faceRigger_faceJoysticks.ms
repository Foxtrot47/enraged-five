-- faceJoysticks.ms
-- Rockstar North
--Part of the facial rigging tools
--script which builds facial control joysticks
--Matt Rennie Sept 2010
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------


fn createFaceUI = 
(
	print ("Starting joystick generation")
	if $Facial_UI == undefined do
	(
		print "Creating FacialUI..."
		FacialUI = text text:"Facial UI" size:4.0 kerning:0
		FacialUI.name = "Facial_UI"
		rotate facialUI (angleaxis 90 [1,0,0])
		FacialUI.wireColor = Blue
			FacialUI.render_displayRenderMesh = true
			FacialUI.thickness = 0.3
			FacialUI.sides = 3
		
		headBoneObject = getNodeByName headBone
		FacialUI.pos = headBoneObject.pos + [0.25,0,0.17]
			addModifier FacialUI (EmptyModifier ()) 
							
		facialMultipliersCA = attributes FacialMultipliers
		(
			parameters main rollout:params
			(
				Zeroed type:#float ui:Zeroed default:0
			)
			
			rollout params "Facial Multipliers"
			(
				spinner Zeroed "Zero" type:#float
			)
		)

		custAttributes.add FacialUI.modifiers[#'Attribute holder'] facialMultipliersCA	

		visualsCA = attributes faceRigVisuals
		(
			parameters main rollout:rigVisuals
			(
				ShowLinks type:#Boolean ui:ShowLinks default:true
			)
			
			rollout rigVisuals "Facial Multipliers"
			(
				checkBox ShowLinks "Show Links" checked:true tooltip:"Togle facial skeleton links"
				
				on ShowLinks changed theState do			
				(
					if ShowLinks.state == true then
					(
						for hlpr in helpers do
						(
							if (substring (hlpr.name) 1 3) == "FB_" do
							(
								hlpr.showLinks = true
							)
						)
					)
					else
					(
						for hlpr in helpers do
						(
							if (substring (hlpr.name) 1 3) == "FB_" do
							(
								hlpr.showLinks = false
							)
						)
					)
				)
			)
		)
		
		custAttributes.add FacialUI.modifiers[#'Attribute holder'] visualsCA 	
		
		FacialUI.modifiers[#'Attribute_Holder'].Zeroed.controller = bezier_float ()
	)

	for joystick = 1 to facialJoysticks.count do
	(
		jpos = [0,0,0]
		currentJoystick = getNodeByName facialJoysticks[joystick].jsName
-- 		if currentJoystick == undefined then
		if currentJoystick == undefined do
		(
print ("facialJoysticks["+(joystick as string)+"].jsName = "+(facialJoysticks[joystick].jsName))
			if facialJoysticks[joystick].jsShape == 1 then
			(
				print ("creating Vert joystick for "+facialJoysticks[joystick].jsName)				
				JoyStck = createJoystick facialJoysticks[joystick].jsName  2 jpos 1
-- 				JoyStck = getNodeByName facialJoysticks[joystick].jsName
-- 				JoyStckRect = getNodeByName ("FFX_RECT_"+(substring facialJoysticks[joystick].jsName 6 50))				
				JoyStckRect = getNodeByName ("RECT_"+(substring facialJoysticks[joystick].jsName 6 50))					
-- 					messagebox ("joystckRect = "+joyStckRect.name)
-- 				messagebox ("current joystck = "+JoyStck)
				joystck = joystckrect
				JoyStck.scale = [1,1,1]
-- 				JoyStck= getNodeByName ("FFX_RECT_"+(substring joystck.name 6 50))
				
				JoyStck.parent = FacialUI
					
print ("Parenting "+joystck.name+" to "+FacialUI.name)								
				in coordsys parent JoyStck.pos = facialJoysticks[joystick].jsPos
print ("positioning "+Joystck.name+" to "+(facialJoysticks[joystick].jsPos as string))					
			) 
			else
			(
				print ("creating Square joystick for "+facialJoysticks[joystick].jsName)
				JoyStck = createJoystick facialJoysticks[joystick].jsName 1 jpos 1
-- 				JoyStck = getNodeByName facialJoysticks[joystick].jsName
-- 				JoyStckRect = getNodeByName ("FFX_RECT_"+(substring facialJoysticks[joystick].jsName 6 50))					
				JoyStckRect = getNodeByName ("RECT_"+(substring facialJoysticks[joystick].jsName 6 50))					
-- 				messagebox ("current joystck = "+JoyStck .name)
				JoyStck = JoyStckRect
				JoyStck.parent = FacialUI
print ("Parenting "+joystck.name+" to "+FacialUI.name)				
				JoyStck.scale = [1,1,1]
				in coordsys parent JoyStck.pos = facialJoysticks[joystick].jsPos
print ("positioning "+Joystck.name+" to "+(facialJoysticks[joystick].jsPos as string))										
			)
		)
-- 		else
-- 		(
-- 			print (currentJoystick.name+" already exists so skipping creation...")
-- 		)
	)
	if FacialUI == undefined do
	(
		FacialUI = getNodeByName "Facial UI"
	)
 	FacialUI.scale = [0.02,0.02,0.02]
	hasMult = 4
)