-- regenArcGuis.ms
-- April 2010
--Matt Rennie
-- Script to regenerate arcGui file fopr heads which have had their bone positions updated after rig creation

faceBonesToRegen = #()
fbName = ()
agName = ()
tmpGuis = #()
geo = ()

fn saveArcGuis = 
(
	clearSelection()
	
	for a in tmpGuis do
	(
				selectMore a
	)
		
	max unlink
	
		geoName = geo.name
		currentFileName = maxFileName
		maxStr = ".max"
		repMax = ""
			currentFileName = substituteString currentFileName maxStr repMax
		currentPath = maxFilePath
		arcGuiName = (currentPath+currentFileName+"_"+geoName+"_ArcGuis")
	
	print ("Saving regenerated arcGuis to "+arcGuiName)
	saveNodes selection arcGuiName quiet:true
	messageBox ("Arcguis saved. Please see listener for full path.")
)

fn createGuiObject arcgName faceBone=
(
	arcGuiParts = #() --local array of parts to make the arcGui

	origCircle = Circle radius:0.0215 pos:[0,0,0]
		appendIfUnique arcGuiParts origCircle
	secondCircle = Circle radius:0.0215 pos:[0,0,0]
		appendIfUnique arcGuiParts secondCircle
	thirdCircle = Circle radius:0.0215 pos:[0,0,0]
		appendIfUnique arcGuiParts thirdCircle
		
	secondCircle.rotation.controller.X_Rotation = 90
	thirdCircle.rotation.controller.Y_Rotation = 90

	PointA = [0.0215,0.0015,0]
	PointB= [0.0215,-0.0015,0]
	
	LText = text size:0.01 kerning:0 leading:0 pos:[0.0214844,1.1634e-005,0] 
		LText.Text = "X"
		CenterPivot LText
		LText.rotation = (quat 0.5 0.5 0.5 0.5)
		LText.pos = [0.0214844,1.1634e-005,0] 			
		appendIfUnique arcGuiParts LText
	
	RText = text size:0.01 kerning:0 leading:0 pos:[-0.021,1.1634e-005,0] 
		RText.Text = "X"
		CenterPivot RText
		RText.rotation = (quat 0.5 -0.5 -0.5 0.5)
		RText.pos = [-0.021,1.1634e-005,0] 
		appendIfUnique arcGuiParts RText
	
		for i in arcGuiParts do
		(
			convertTo i SplineShape
		)
	
		addAndWeld origCircle thirdCircle -1.0
			updateShape origCircle
		addAndWeld origCircle secondCircle -1.0
			updateShape origCircle		
		addAndWeld origCircle LText -1.0
			updateShape origCircle
		addAndWeld origCircle RText -1.0
			updateShape origCircle
		
		--now need to add custom attribute for radius and hooko this up to the scale on a two way wire
		
		select origCircle
		$.thickness = 0.0
		$.scale.controller = ScaleXYZ ()
		
		modPanel.addModToSelection (EmptyModifier ()) ui:on
			bname = "arcGui_Radius"
			
		rolloutCreation = ("arcGuiCA = attributes "+bname +"\n" +"(" +"\n" +"\t" +"parameters main rollout:params"+"\n" +"\t"+"(" +"\n" +"\t" +"\t" +"Radius" +" type:#float ui:" +"Radius"+" default:" +"1.0"+"\n" +"\t"+")" +"\n" +"\t"  +"rollout params "+" \"" +bname+" \"" +"\n" +"\t" +"(" +"\n" +"\t"+"\t"  +"spinner "+"Radius" +" \"" +"Radius" +"\"" +" "+"fieldwidth:40 range:[0,100,1] type:#float" +"\n" +"\t" +")" +"\n"+")"+"\n" +"\n" +"\t" +"custAttributes.add $" +$.name+".modifiers[#'Attribute holder'] "+"arcGuiCA")
			execute rolloutCreation				
			
		--now add a bezier float controller onto the newly created custom attribute		
		arcRad = ("$"+".modifiers[#Attribute_Holder]."+bname+"."+"Radius"+".controller = bezier_float ()")
			execute arcRad
			
		paramWire.connect2way $.modifiers[#Attribute_Holder].arcGui_Radius[#radius] $.scale.controller[#X_Scale] "X_Scale" "radius"		
		paramWire.connect2way $.modifiers[#Attribute_Holder].arcGui_Radius[#radius] $.scale.controller[#Y_Scale] "Y_Scale" "radius"		
		paramWire.connect2way $.modifiers[#Attribute_Holder].arcGui_Radius[#radius] $.scale.controller[#Z_Scale] "Z_Scale" "radius"

		origCircle.transform = faceBone.transform	
		origCircle.name = arcgName
			
		appendIfUnique tmpGuis origCircle
			print ("appended "+origCircle.name+" to tmpguis")
				
)	


fn regenArcGui geo =
(
	arcGuis = #()
	faceBones = #()
	
	geoName = geo.name
	currentFileName = maxFileName
	maxStr = ".max"
	repMax = ""
	currentFileName = substituteString currentFileName maxStr repMax
	currentPath = maxFilePath
	aG_Name = (currentPath+currentFileName+"_"+geoName+"_ArcGuis.max")
	geoNameLength = geoName.count
	fromInt = (geoNameLength - 4)
	faceNumerical = (substring geoName 5 4)
	
	for obj in objects do
	(
		objName = obj.name
		objNameLength = objName.count
		objNameFromInt = (objNameLength - 3)
				
		if (substring obj.name 1 3) == "FB_" do
		(
			numberOnName = (substring obj.Name objNameFromInt objNameLength) 
			if numberOnName == faceNumerical then
			(
				append faceBonesToRegen obj
			)
			else
			(
				print (numberOnName+" did not match "+faceNumerical)
			)
		)
		if (substring obj.name 1 6) =="arcGUI" do --get rid of any existing arcGuis
		(
			delete obj
		)
	)

	for fbone = 1 to faceBonesToRegen.count do
	(
		fbName = faceBonesToRegen[fbone].name
		fbNameLength = fbName.count
		fbName = (substring fbName 1 (fbNameLength - 4)) --this should strip the _000 suffix
		arcgName = ("arcGUI_"+fbName)
		createGuiObject arcgName faceBonesToRegen[fbone]
	)
	
 	saveArcGuis()
	
	clearSelection()
	
	for obj in tmpGuis do
	(
		selectMore obj
	)

	delete selection
	print "Deleted arcGuis."
	
	if ((regenArcGuiWindow != undefined) and (regenArcGuiWindow.isDisplayed)) do
	(destroyDialog regenArcGuiWindow)

)


fn startRegen =
(
	geo = $

	if geo != undefined then
	(
		selSize = selection as array
		if selSize.count != 1 then
		(
			messageBox "Please pick only one object and try again."
		)
		else
		(
			if (classof geo.baseObject == Editable_Poly )then
			(
				headValid = false
				headPrefix = (substring geo.name 1 5 ) --should give "head_"
				afterNumber = (substring geo.name 9 1 ) --should give "_"
				if headPrefix == "head_" do (headValid = true)
				if headPrefix == "Head_" do (headValid = true)
				
				if headValid != true then
				(
					messageBox "Geometry naming pattern is incorrect - first 4 character should be head "
				)
				else
				(
	-- 				if afterPart != "_" then
					if geo.name.count <= 9 then
					(
						messageBox "Geometry naming pattern is incorrect."
					)
					else
					(

						regenArcGui geo
					)
				)
			)
			else
			(
				messageBox "Please pick a valid head geo."
			)
		)
	)
	else
	(
		MessageBox "Please select the head for these arcGuis"
	)
)

clearListener()
startRegen()