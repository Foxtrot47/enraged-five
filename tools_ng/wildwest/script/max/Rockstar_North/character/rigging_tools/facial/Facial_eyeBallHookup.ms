--Facial_eyeBallHookup.ms
--Jan 2010
--script to add look at constraints to eyeball bones and configure lookat joystick to enable/disable these

eyeBones = #(
	$FB_R_Eye,
	$FB_L_Eye
	)

eyeJoysticks = $CTRL_LookAT_Activator

fn lookAtHookup = 
(
--	clearListener()
	
	if $EyeLookat == undefined do
	(
		iT = ((($FB_R_Eye.position +$FB_L_Eye.position) / 2 ) +[0,-0.5,0])
-- 		iR = [0,0,0]
		PointHelper = Point pos:iT 
		PointHelper.name =  "EyeLookat"
-- 		PointHelper.rotation = iR
		PointHelper.position = iT
		PointHelper.size = 0.06
		PointHelper.cross = on
		PointHelper.axistripod = on
		PointHelper.centermarker = off
		PointHelper.Box = off
		PointHelper.constantscreensize = off
		PointHelper.drawontop = off
		PointHelper.wirecolor = Red
-- 		)
	)
	
	for i = 1 to eyeBones.count do
	(
		g = 4
		eyeBones[i].rotation.controller[g].controller = LookAt_Constraint ()
			eyeBones[i].rotation.controller[g].controller.appendtarget $EyeLookat 50 --target geo and weight
			eyeBones[i].rotation.controller[g].controller.relative = true --keep initial offset
			eyeBones[i].rotation.controller[g].controller.viewline_length_abs = false --viewline length off
			eyeBones[i].rotation.controller[g].controller.upnode_world= false --upnode control set to local
			eyeBones[i].rotation.controller[g].controller.upnode_ctrl = 0 --set upnode control state
			eyeBones[i].rotation.controller[g].controller.StoUp_axisFlip = true --upnode alignment flip turned on
		
		eyeBones[i].rotation.controller.weight[g].controller = Float_Expression ()

			fj_Trans = "Y"
			controllerName = (substring (eyeJoysticks.name ) 6 100) --this strips off the first 5 characters i,e. CTRL_ off the beginning of the name stored.	
		
				--BEGIN CONNECTING Y TRANSLATION OF JOYSTICK TO LOOKAT WEIGHT
				print ("\r\n")
				print "****************************************************"
					print ("CONNECTING "+fj_Trans+" TRANSLATION OF "+controllerName+" to " +(eyeBones[i].name as string))
						newfcStr = eyeBones[i].rotation.controller.weight[g].controller
				
				SVN = (controllerName + "_" +fj_Trans) -- Name of Scalar Variable.	
				fjStr = ( "$CTRL_LookAT_Activator.position.controller.Zero_Pos_XYZ.Y_Position.controller" )						
				fjStr = execute fjStr
				--		print ("fjStr = "+fjStr)
			
			newfcStr.AddScalarTarget SVN fjStr --add scalar pointing to the translation of the joystick object
				--print "Scalar added..."
			expCond =( "("+SVN +" > 0.0)," ) --Conditional for expression
			expTrue = ("("+SVN+" *1)," )--True condition for expression						
			expFalse = ("(0)" )--False condition for expression
			
			print "Creating Expression"
			newfcStr.SetExpression	("if "+"\r\n" +"(" +"\r\n" +"\t"+expCond+"\r\n" +"\t"+expTrue+"\r\n"+"\t"+expFalse +"\r\n" +")") --set the expression here (NEED TO CHANGE THIS TO USE A STRUCT PROBABLY SO DIFFERING EXPRESSIONS CAN BE ADDED ALTHOUGH MAY BE ABLE TO GET AWAY BY JUST BEING CLEVER WITH THE MULTIPLIERS)
	)
	in coordsys parent $CTRL_LookAT_Activator.pos = [0,-1,0]
	print "Eye LookAt configured."
)

 lookAtHookup()