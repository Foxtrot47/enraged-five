-- 	-- Skel animation loader.
-- 	-- Rick Stirling July 2009
-- 	--Modified by Matt Rennie March 2010
--  --Updated with filein and wildwest paths so the script works - Stewart Wright 29/2/12

-- Common script headers, setup paths and load common functions
-- load the common functions
filein "rockstar/export/settings.ms"
-- Figure out the project data
theWildWest = RsConfigGetWildWestDir()


filein (theWildwest + "script/3dsMax/Characters/Rigging/Anim_Library_Interface.ms")