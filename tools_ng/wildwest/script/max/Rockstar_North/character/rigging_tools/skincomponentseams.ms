filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

clearListener()

allObjects = #() --array of all picked objects which we will loop through
selectedObjs = #()
vertArray1 = #()--verts from mesh 1
vertArray2 = #()--verts from mesh 2
matchingVertArray = #( --dynamically populated array of matching vert pairs
#(),
#()
)

	global obj1BoneArray = #() --dynamically populated array of bones for object1
	global obj1WeightArray = #() --dynamically populated array of weights for object1
	global obj2BoneArray = #() --dynamically populated array of bones for object2
	global obj2WeightArray = #() --dynamically populated array of weights for object2
	global newWeightArray = #() --array of new averaged weights


filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-- filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Ped_Game_Setup\\C_Vertex_Tools.ms")	

fn RsCopyWeight fromVert fromSkin toVert toSkin =
(	
	BoneArray = #()
	WeightArray = #()
	
	modPanel.setCurrentObject fromSkin
	BNumber = skinOps.getVertexWeightCount fromSkin fromVert
	
	for i = 1 to BNumber do
	(
		boneID = skinOps.getVertexWeightBoneId fromSkin fromVert i
		boneWeight = skinOps.getVertexWeight fromSkin fromVert i
		
		append BoneArray boneID
		append WeightArray boneWeight
	)
	modPanel.setCurrentObject toSkin
	skinOps.ReplaceVertexWeights toSkin toVert BoneArray WeightArray
	--format "Bone weight info replaced in Vert %, with boneArray % and weightArray %\n" toVert BoneArray WeightArray
)

fn RsCheckIfVertsMatch vertPos1 vertPos2 = (

	distanceVal = vertPos1 - vertPos2	

	if (not (distanceVal.x > vertDistanceFactor or 
			distanceVal.y > vertDistanceFactor or 
			distanceVal.z > vertDistanceFactor or
			distanceVal.x < -vertDistanceFactor or 
			distanceVal.y < -vertDistanceFactor or 
			distanceVal.z < -vertDistanceFactor)) then 
	(
		return true
	)
	else
	(
		return false
	)
)


--------------------------------------------------------
--------------------------------------------------------

fn averageVertWeights obj1WeightArray obj1Verts obj2WeightArray obj2Verts = 
(
	newWeightArray = #(#(),#())
	newWeight = undefined
	
	if obj1WeightArray.count >= obj2WeightArray.count then
	(
		array1 = obj1WeightArray
		array2 = obj2WeightArray
	)
	else
	(
		array1 = obj2WeightArray
		array2 = obj1WeightArray
	)
	
	for i = 1 to array1.count do
	(
-- 		newWightArray[1]
		obj1Weight = array1[i]
		obj2Weight = array2[i]
		if obj2Weight == undefined do
		(
			obj2Weight = 0
		)
		
		if obj1Weight != obj2Weight then
		(
			if obj1Weight > obj2Weight then
			(
				if obj2Weight == 0 then
				(
					newWeight = obj1Weight / 2
				)
				else
				(
					newWeight = (((obj1Weight - obj2Weight) / 2) + obj2Weight)
				)
			)
			else
			(
				if obj1Weight == 0 then
				(
					newWeight = obj2Weight / 2
				)
				else
				(
					newWeight = (((obj2Weight - obj1Weight) / 2) + obj1Weight)
				)
			)
		)
		else
		(
			newWeight = obj1Weight
		)
		newWeightArray[1][i] = obj2BoneArray[1][i]
		newWeightArray[2][i] = newWeight
		print "---------------------------------------"
		print ("initial weight obj1 = "+(obj1Weight as string))
		print ("initial weight obj2 = "+(obj2Weight as string))
		print ("new weight for bone index "+(obj2BoneArray[1][i] as string)+" = "+(newWeight as string))
			
	
	)
	
		print "newWeightArray = "
		print newWeightArray	
	
		
		modPanel.setCurrentObject obj1.modifiers[#Skin]
		skinOps.ReplaceVertexWeights obj1.modifiers[#Skin] obj1Verts 1 1 --this hacks the skin to 100%
		skinOps.isUnNormalizeVertex obj1.modifiers[#Skin] false
		skinOps.ReplaceVertexWeights obj1.modifiers[#Skin] obj1Verts 1 1 --this hacks the skin to 100%
		skinOps.ReplaceVertexWeights obj1.modifiers[#Skin] obj1Verts newWeightArray[1][1] newWeightArray[2][i]
			
		modPanel.setCurrentObject obj2.modifiers[#Skin]
		skinOps.ReplaceVertexWeights obj2.modifiers[#Skin] obj2Verts newWeightArray[1][1] newWeightArray[2][i]		
)


fn buildWeightArrays obj1Verts obj1 obj2Verts obj2 = 
(
	--SHOULD I TURN THE BONE ARRAYS INOT A MULTI DIMENDIONAL ONE COMBINGING THE BONE AND WEIGHT ARRAYS SO I CAN MATCH BONE TO THE WEIGHT?
	obj1BoneArray = #(#(),#())
-- 	obj1WeightArray = #()
	
	modPanel.setCurrentObject obj1.modifiers[#Skin]
	obj1BNumber = skinOps.getVertexWeightCount obj1.modifiers[#Skin] obj1Verts
	
	for i = 1 to obj1BNumber do
	(
		obj1BoneID = skinOps.getVertexWeightBoneId obj1.modifiers[#Skin] obj1Verts i
		obj1BoneWeight = skinOps.getVertexWeight obj1.modifiers[#Skin] obj1Verts i
		
-- 		append obj1BoneArray obj1BoneID
-- 		append obj1WeightArray obj1BoneWeight
		append obj1BoneArray[1] obj1BoneID
		append obj1BoneArray[2] obj1BoneWeight
	)
	
	obj2BoneArray = #(#(),#())
-- 	obj2WeightArray = #()
	
	modPanel.setCurrentObject obj2.modifiers[#Skin]
	obj2BNumber = skinOps.getVertexWeightCount obj2.modifiers[#Skin] obj2Verts
	
	for i = 1 to obj2BNumber do
	(
		obj2BoneID = skinOps.getVertexWeightBoneId obj2.modifiers[#Skin] obj2Verts i
		obj2BoneWeight = skinOps.getVertexWeight obj2.modifiers[#Skin] obj2Verts i
		
-- 		append obj2BoneArray obj2BoneID
-- 		append obj2WeightArray obj2BoneWeight
		append obj2BoneArray[1] obj2BoneID
		append obj2BoneArray[2] obj2BoneWeight		
	)	
	
	--ok now we have an array of weights for each vert, now we need to average them
-- 	averageVertWeights obj1WeightArray obj2WeightArray 
	averageVertWeights obj1BoneArray[2] obj1Verts obj2BoneArray[2] obj2Verts
)	


fn prepSkinWeights obj1 obj2 = 
(
	print ("Building weights Arrays for "+obj1.name+" and "+obj2.name+"...")
	for i = 1 to matchingVertArray[1].count do
	(
		--RsCopyWeight matchingVertArray[1][i] obj1.modifiers[#Skin] matchingVertArray[2][i] obj2.modifiers[#Skin]
		buildWeightArrays matchingVertArray[1][i] obj1 matchingVertArray[2][i] obj2
	)
	print "Weights Arrays Built."
)

fn findMatch obj1 obj2 =
(
	matchingVertArray = #( --re-initialise array
	#(),
	#()
	)

	for i = 1 to vertArray1.count do
	(
		for v = 1 to vertArray2.count do
		(
			
			vert1 = obj1.EditablePoly.getVertex vertArray1[i]
			vert2 = obj2.EditablePoly.getVertex vertArray2[v]
			
			
			vertMatches = (RsCheckIfVertsMatch vert1 vert2)
			if vertMatches == true do
			(
				append matchingVertArray[1] vertArray1[i]
				append matchingVertArray[2] vertArray2[v]
			)
		)
	)
	prepSkinWeights obj1 obj2
)

fn buildArrays obj1 obj2 = 
(
	max modify mode

	clearSelection()
	select obj1
	modPanel.setCurrentObject obj1.baseObject
	subobjectLevel = 1
	objVerts = (obj1.EditablePoly.SetSelection #Vertex #{1..(obj1.numverts)}) 
	vertArray1 = (polyOp.getVertSelection obj1 ) as array
	subobjectLevel = 0	
	clearSelection()
		
	select obj2
	modPanel.setCurrentObject obj2.baseObject
	subobjectLevel = 1
	objVerts = (obj2.EditablePoly.SetSelection #Vertex #{1..(obj2.numverts)}) 
	vertArray2 = (polyOp.getVertSelection obj2 ) as array
	subobjectLevel = 0

		print ("objVerts = "+(objVerts as string))
	print ("vertArray1 = "+(vertArray1 as string))
	print ("vertArray2 = "+(vertArray2 as string))	

	findMatch obj1 obj2
)

allObjects = selection as array
ind = 0 -- this will get incremented upon

if allObjects.count >= 2 then
(
	for i = 1 to allObjects.count do
	(
		--try and do a loop and increment the 2nd object by 1 so we start with 1 and 2 on which we do an average and then for 2 and 3 we do no average just copy 2 to 3, then 3 to 4 etc
		--*** NEED TO DO A TEST ACROSS ALL OBJECTS TO SEE IF THEY SHARE MATCHING PIECES WITH ANY OTHER PIECES
		-- THEN CREATE A LIST OF OBJECTS THAT MATCH EACH OTHER AND GO THROUGH IT
		--SET FLAG IN HERE TO DEFINE IF IT HAS ALREADY BEEN AVERAGED
		ind = (ind + 1)
		
		if allObjects[(ind + 1)] != undefined do --this makes sure we dont loop out of the count of the array of selected objects
		(
			print ("obj 1 = "+allObjects[ind].name)
			print ("obj 2 = "+allObjects[(ind + 1)].name)
			
			buildArrays allObjects[ind] allObjects[(ind + 1)]
		)
	)
)
else
(
	messagebox "WARNING! Please pick at least two objects." beep:true
)