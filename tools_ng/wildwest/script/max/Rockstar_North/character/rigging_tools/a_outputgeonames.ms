filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

fn outputGeoNames = 
(
objArray = selection as array

	output_name = getSaveFileName caption:"Name output file" types:"NameData (*.nam)|*.nam|All Files (*.*)|*.*|"
	-- 		
		if output_name != undefined then 
		(
			output_file = createfile output_name		

			for i = 1 to objArray.count do
			(
-- 					thebonename = skinOps.getBoneName skinMod  i 1
-- 					append boneList theBoneName
				objName = objArray[i].name as string
				format ((objName)+"\n") to:output_file
			)
			
			close output_file
				--edit output_name --opens the file in a maxscript window so it can be checked.
			
		)--end if
		
)		

outputGeoNames()