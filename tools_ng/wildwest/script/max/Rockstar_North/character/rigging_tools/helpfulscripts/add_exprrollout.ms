currentJoystick = $

if currentJoystick.modifiers[#Attribute_Holder] == undefined do
(
	modPanel.addModToSelection (EmptyModifier ()) ui:on
)

facialMultipliersCA = (currentJoystick.name +"testCA")

objName = $.name
boneName = ("jawBackFwd_A") -- name of the rollout


	uiNameMinZ = ("jawBackFwd") --name as seen inside the code and track view
-- 	uiNameMaxZ = (boneName+"_MaxZ")
-- 	uiNameMultZ = (boneName+"_MultZ")
	
	nameMinZ = "jawBackFwd" --name as seen in the ui
-- 	nameMaxZ = "MaxZ"
-- 	nameMultZ = "MultZ"

	posVal = "100"
	negval = "-100"		
			
rolloutCreation = (facialMultipliersCA +" = attributes "+boneName +"\n" +"(" +"\n" +"\t" +"parameters main rollout:params"+"\n" +"\t"+"(" +"\n" +"\t" +"\t" +uiNameMinZ +" type:#float ui:" +uiNameMinZ +" default:" +posVal +"\n" +"\t"+")" +"\n" +"\t"  +"rollout params "+" \"" +boneName+" \"" +"\n" +"\t" +"(" +"\n" +"\t"+"\t"  +"spinner "+uiNameMinZ +" \"" +NameMinZ+"\"" +" "+"fieldwidth:40 range:[-100,100,1] type:#float" +"\n" +"\t" +")" +"\n" +")"+"\n" +"\n" +"\t" +"custAttributes.add $" +currentJoystick.name+".modifiers[#'Attribute holder'] "+facialMultipliersCA)
	execute rolloutCreation		

--now add bezier floats controllers to these attributes

cont1 = ("currentJoystick.modifiers[#Attribute_Holder]."+boneName+"."+uiNameMinZ+".controller = bezier_float ()")
	execute cont1
-- cont2 =	("currentJoystick.modifiers[#Attribute_Holder]."+boneName+"."+uiNameMaxZ+".controller = bezier_float ()")
-- 	execute cont2
-- cont3 =	("currentJoystick.modifiers[#Attribute_Holder]."+boneName+"."+uiNameMultZ+".controller = bezier_float ()")
-- 	execute cont3
