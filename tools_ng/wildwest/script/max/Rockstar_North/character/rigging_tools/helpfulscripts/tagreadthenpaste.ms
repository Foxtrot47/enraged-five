selObj = #()

udptArray = #(
	#(),
	#()
	)

	
fn readTags = 
(
	for i = 1 to selObj.count do
	(
-- 		append udptArray[1][i] selObj[i].name
		udptArray[1][i] = selObj[i].name
		print ("adding "+selObj[i].name+" to array")
		udptag = getUserPropBuffer selObj[i]
-- 		append udptArray[2][i] udptTag
		udptArray[2][i] = udptag
	)
	print ("Array count = "+(udptArray[1].count as string))
)

fn pasteTags = 
(
	for i = 1 to udptArray[1].count do
	(
		thisNode = getNodeByName udptArray[1][i]
		if thisNode != undefined do
		(
-- 			setUserPropBuffer thisNode ("tag = "+udptArray[2][i])
			setUserPropBuffer thisNode (udptArray[2][i])
		)
	)
)




if ((udptChanger != undefined) and (udptChanger.isDisplayed)) do
	(destroyDialog udptChanger)

rollout udptChanger "udp"
(
	button btnRead "Read" width:110
	button btnPaste "Paste" width:110

		
	
	on btnRead pressed do
	(
		selObj = selection as array
		readTags()
	)
	
	on btnPaste pressed do
	(
		pasteTags()
	)
)

CreateDialog udptChanger width:125 pos:[1450, 100] 