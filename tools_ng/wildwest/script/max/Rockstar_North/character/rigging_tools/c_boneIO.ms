-- boneIOGui
-- a little floater gui to bring bone input output tools into one place
--Matt Rennie
--April 2011


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()



global boneIOGUI = undefined

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if ((boneIOGUI != undefined) and (boneIOGUI.isDisplayed)) do
	(destroyDialog boneIOGUI)

rollout boneIOGUI "Bone I/O"
(
	button btnInput "Add Bones" width:110
	button btnOutput "Save Bones" width:110

	on btnInput pressed do
	(
		filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Rigging_tools\\boneInput.ms")	
		destroyDialog boneIOGui
	)

	on btnOutput pressed do
	(
		filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Rigging_tools\\boneOutput.ms")	
		destroyDialog boneIOGui
	)

)

CreateDialog boneIOGUI width:125 pos:[1450, 100] 