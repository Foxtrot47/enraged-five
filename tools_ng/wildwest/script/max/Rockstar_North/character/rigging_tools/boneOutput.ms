--gta5_boneOutput.ms
--matt Rennie
--jan 2010
--Script to output all bones in a skin modifier to a .dat file


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

clearListener()

boneList = #()

--print boneList

fn outputBones = 
(
	max modify mode
	Geo = selection
	if Geo.count == 1 then
	(
	skinMod = $.modifiers[#Skin]
	totalSkinModBones = skinOps.getNumberBones skinMod

		if totalSkinModBones > 0 then
		(
			--output_name = getSaveFileName caption:"SpeedSheet File" types:"SpeedSheet (*.ssh)|*.ssh|All Files (*.*)|*.*|"

			output_name = getSaveFileName caption:"Bone output file" types:"BoneData (*.bon)|*.bon|All Files (*.*)|*.*|"
	-- 		
			if output_name != undefined then 
			(
				output_file = createfile output_name		

				for i = 1 to totalSkinModBones do
				(
					thebonename = skinOps.getBoneName skinMod  i 1
					append boneList theBoneName
					format ((theBoneName as string)+"\n") to:output_file
				)
				
				close output_file
					--edit output_name --opens the file in a maxscript window so it can be checked.
				
			)--end if
		)--end if
	)
	else
	(
	messageBox "Please run with only 1 object selected." title:"gta5_boneOutput"
	)
)

outPutBones()