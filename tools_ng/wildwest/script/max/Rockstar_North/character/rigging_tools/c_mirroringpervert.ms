-- Author: Mike Wilson
-- slight mods to ui layout by Matt Rennie.
-- Date: 26/03/2010
-- Updated: 28/05/2010

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-- filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Includes\\FN_Common.ms")	

filein (RsConfigGetWildWestDir() + "script/3dsMax/Characters/Rigging//usefulScripts/mirrorPerVert_v2.ms")
