-- build Knee markers


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())
	
	
fn createKneeExpression currentKneeBone LegBone=
(
		--BEGIN CONNECTING X TRANSLATION OF JOYSTICK				
		fcStr = ( "$"+currentKneeBone.name+".position.controller.Y_Position.controller = Float_Expression() ")
				execute fcStr
		fcStr = ( "$"+currentKneeBone.name+".position.controller.Y_Position.controller")
			newfcStr = execute fcStr
		
		--**************************************
		
		SVN = (LegBone.name+ "_Rot") -- Name of Scalar Variable.	
		--legBone should be a var passed to the expression 
		fjString = ("$"+LegBone.name+".rotation.controller.Zero_Euler_XYZ.controller.'Z Rotation'.controller" )
			fjStr = execute fjString
		
		newfcStr.AddScalarTarget SVN fjStr --add scalar pointing to the translation of the joystick object
		
		expCond = "if ("+SVN +" <= -0.75," --Conditional for expression
		expTrue = "(("+ SVN +" / 20 ) * (" +SVN+"/3) + (-0.099))," --True condition for expression
		expFalse = "-0.099)" --False condition for expression
		
		newfcStr.SetExpression	(expCond+"\r\n" +expTrue+"\r\n"+expFalse +"\r\n" ) --set the expression here (NEED TO CHANGE THIS TO USE A STRUCT PROBABLY SO DIFFERING EXPRESSIONS CAN BE ADDED ALTHOUGH MAY BE ABLE TO GET AWAY BY JUST BEING CLEVER WITH THE MULTIPLIERS)		
			
		print ("Expression set on "+currentKneeBone.name)
)

fn buildKneeDeformers = (

	if $MH_L_CalfBack == undefined do (
		Point pos:[0,0,0] name:"MH_L_CalfBack" size:0.1 
	)
	if $MH_R_CalfBack == undefined do (
		Point pos:[0,0,0] name:"MH_R_CalfBack" size:0.1
	)
	if $MH_L_ThighBack == undefined do (
		Point pos:[0,0,0] name:"MH_L_ThighBack" size:0.1
	)
	if $MH_R_ThighBack == undefined do	(
		Point pos:[0,0,0] name:"MH_R_ThighBack" size:0.1
	)

		$MH_L_CalfBack.parent = $SKEL_L_Calf
		$MH_L_CalfBack.wirecolor = green
		in coordsys parent $MH_L_CalfBack.pos = [0.064,-0.099,0]
			createKneeExpression $MH_L_CalfBack $MH_L_CalfBack .parent
		$MH_R_CalfBack.parent = $SKEL_R_Calf
		$MH_R_CalfBack.wirecolor = green
		in coordsys parent $MH_R_CalfBack.pos = [0.064,-0.099,0] 	
			createKneeExpression $MH_R_CalfBack $MH_R_CalfBack .parent
		$MH_L_ThighBack.parent = $SKEL_L_Thigh
		$MH_L_ThighBack.wirecolor = green
		in coordsys parent $MH_L_ThighBack.pos = [0.345,-0.099,0] 
			createKneeExpression $MH_L_ThighBack $MH_L_ThighBack.parent
		$MH_R_ThighBack.parent = $SKEL_R_Thigh
		$MH_R_ThighBack.wirecolor = green
		in coordsys parent $MH_R_ThighBack.pos = [0.345,-0.099,0] 
			createKneeExpression $MH_R_ThighBack $MH_R_ThighBack.parent
		
)

buildKneeDeformers()