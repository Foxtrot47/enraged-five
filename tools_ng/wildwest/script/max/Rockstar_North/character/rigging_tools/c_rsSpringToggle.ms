-- script to toggle strength on RSspring controllers
-- March 2011
--Matt Rennie



filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())
	
	
------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------

--***************************************************************************
--NEED TO MODIFY SO THAT IT FINDS FIRST SPRING
--TOGGLES ITS VALUE THEN SETS ALL OTHERS TO MATCH
--***************************************************************************


------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------

objArray = objects as array
springSetting = undefined
springSettingVal = undefined

while springSetting == undefined do --loop thru all controllers till we find a spring and query its strength
(
-- 	for (obj = 1 to objArray.count) while (springSetting == undefined) do 
	for obj = 1 to objArray.count do 
	(
		objCont = objArray[obj].position.controller 
		if (objCont as string) == "Controller:RsSpring" then
		(
			print ("Cant toggle RSSpring on "+objArray[obj].name+" as it is a root controller, not a list.")
		)
		else
		(
			if (objCont as string) == "Controller:Position_List" do
			(
				for i = 1 to  objCont.count do
				(
					if (objCont[i] as string) == "SubAnim:RsSpring" do
					(
						springSetting = true --ok we found a spring so stop looping thru
						if objArray[obj].pos.controller.weight[i] == 0 then
						(
							springSettingVal = 100 --ok first spring found is 0 so we need to set all to 100
						)
						else
						(
							springSettingVal = 0 --ok first spring found is 100 so we need to set all to 0
						)
					)
				)
			)
		)
	)
)

if springSetting != undefined do --now we have found a spring we need to invert its strength
(
	for obj = 1 to objArray.count do
	(
-- 		print ("springSettingVal = "+(springSettingVal as string))
		objCont = objArray[obj].position.controller 
		if (objCont as string) == "Controller:RsSpring" then
		(
			print ("Cant toggle RSSpring on "+objArray[obj].name+" as it is a root controller, not a list.")
		)
		else
		(
			if (objCont as string) == "Controller:Position_List" do
			(
				for i = 1 to  objCont.count do
				(
					if (objCont[i] as string) == "SubAnim:RsSpring" do
					(
							objArray[obj].pos.controller.weight[i] = springSettingVal --ok set the spring  value
							print ("Setting "+objArray[obj].name+" spring to "+(springSettingVal as string))
					)
				)
			)
		)
	)
)