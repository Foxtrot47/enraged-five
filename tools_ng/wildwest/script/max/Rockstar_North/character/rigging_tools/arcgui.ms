-- build all centre nodes one by one with user positioning then clicking ok to laod next one and add to an array every time
-- build all left nodes one by one with user positioning then clicking ok to laod next one and add to an array every time
--build a point at 0,0,0
--link all left nodes to point helper
--scale point helper -100 in x
-- buidl all right nodes and use $node.transform = $left.transform
-- scale point helper back to 100 in x
-- should now be done

global geo = ()

arcName = #( --array of all arcGui objects (last item is simply there as a label on final button)
	"arcGUI_FB_Skull", 
	"arcGUI_FB_L_Lip_Corner", 
	"arcGUI_FB_L_Lid_Upper_Mid", 
	"arcGUI_FB_L_Brow_Out", 
	"arcGUI_FB_L_CheekBone", 
	"arcGUI_FB_L_Nasolabial_Mid", 
	"arcGUI_FB_L_Eye", 
	"arcGUI_FB_L_Brow_Centre", 
	"arcGUI_FB_UpperLipRoot", 
	"arcGUI_FB_UpperLip", 
	"arcGUI_FB_Jaw", 
	"arcGUI_FB_LowerLipRoot", 
	"arcGUI_FB_LowerLip",
	"Gui's made"
	)	
	
rightGuis = #(
	"arcGUI_FB_R_Lid_Upper_Mid",
	"arcGUI_FB_R_Eye",
	"arcGUI_FB_R_Brow_Centre",
	"arcGUI_FB_R_CheekBone",
	"arcGUI_FB_R_Brow_Out",
	"arcGUI_FB_R_Nasolabial_Mid",
	"arcGUI_FB_R_Lip_Corner"
	)	
	
leftGuis = #(
	"arcGUI_FB_L_Lid_Upper_Mid",
	"arcGUI_FB_L_Eye",
	"arcGUI_FB_L_Brow_Centre",
	"arcGUI_FB_L_CheekBone",
	"arcGUI_FB_L_Brow_Out",
	"arcGUI_FB_L_Nasolabial_Mid",
	"arcGUI_FB_L_Lip_Corner"
	)		
	
global createdRightArcs = #()	
	
arcGuiTransforms = #(
	".transform = (matrix3 [0,0,0.2] [-0.2,0,0] [0,-0.2,0] [0,0.0342804,1.62646])", --arcGUI_FB_Skull
	".transform = (matrix3 [-3.82472,2.86655,0.0529154] [-2.81079,-3.73166,-1.01123] [-0.565122,-0.840254,4.67151] [-0.03,-0.0157866,1.63759])", --arcGUI_FB_L_Lip_Corner
	".transform = (matrix3 [0.0269366,-0.198053,0.00703428] [0.198175,0.0269534,4.09826e-006] [-0.000952046,0.00696956,0.199876] [0.0326932,-0.0694676,1.69968])",--arcGUI_FB_L_Lid_Upper_Mid
	".transform = (matrix3 [1.85789,-3.3145,-0.554925] [3.08575,1.93367,-1.21848] [1.33117,0.143603,3.59902] [0.00840005,-0.0187347,1.71216])", --arcGUI_FB_L_Brow_Out
	".transform = (matrix3 [0.0374396,-0.196007,-0.0133941] [0.184556,0.0397634,-0.0660152] [0.0673603,-1.84923e-006,0.188315] [0.013675,-0.0868942,1.68737])", --arcGUI_FB_L_CheekBone
	".transform = (matrix3 [0.593876,-1.24036,-0.262324] [1.05404,0.322093,0.863278] [-0.704489,-0.5637,1.07048] [0.0214,-0.0638874,1.63627])", --arcGUI_FB_L_Nasolabial_Mid
	".transform = (matrix3 [-7.65325e-007,-0.2,6.97771e-007] [0.199971,-7.77207e-007,-0.00343829] [0.00343829,6.84511e-007,0.199971] [0.0326607,-0.0693883,1.69967])", --arcGUI_FB_L_Eye
	".transform = (matrix3 [-0.156379,-2.6277,0.200848] [2.57899,-0.193984,-0.529905] [0.542194,0.164817,2.57846] [0.0160001,-0.0509544,1.71096])", --arcGUI_FB_L_Brow_Centre
	".transform = (matrix3 [1.714e-005,-2.26903,-0.376181] [2.3,1.61512e-005,7.37533e-006] [-4.63437e-006,-0.376181,2.26903] [3.50177e-007,-0.0592149,1.63757])", --arcGUI_FB_UpperLipRoot
	".transform = (matrix3 [2.38469e-006,-0.315691,-0.0523383] [0.32,2.27208e-006,8.756e-007] [-4.92193e-007,-0.0523383,0.315691] [3.85568e-007,-0.106206,1.64343])", --arcGUI_FB_UpperLip
	".transform = (matrix3 [4.55541e-006,-0.171643,-0.102658] [0.2,4.37793e-006,1.55507e-006] [9.12549e-007,-0.102658,0.171643] [0,-0.02052,1.62687])", --arcGUI_FB_Jaw
	".transform = (matrix3 [2.95265e-006,-0.395454,-0.192914] [0.44,2.72872e-006,1.14083e-006] [1.71048e-007,-0.192914,0.395454] [-1.25915e-006,-0.0278317,1.61195])", --arcGUI_FB_LowerLipRoot
	".transform = (matrix3 [2.95265e-006,-0.395455,-0.192915] [0.44,2.71273e-006,1.17362e-006] [1.34573e-007,-0.192915,0.395455] [3.42727e-007,-0.0979966,1.60043])"--,--arcGUI_FB_LowerLip
)	
	
global arcGuiParts = #() 
arcGuiArray = #()
global rci = rolloutCreator "arcGuiWindow" "arc gui generator"
global rciTemp = ()
	arcArrayItem = 1

fn arcGuiCreation guiName arcArrayItem = --function to build an arcGui object with name of arcGui to be created
(
	arcGuiParts = #() --local array of parts to make the arcGui
	
	Circle radius:0.0215 pos:[0,0,0]
		appendIfUnique arcGuiParts $Circle01
	maxOps.cloneNodes $Circle01 cloneType:#copy newNodes:&nnl 
		appendIfUnique arcGuiParts $Circle02
	maxOps.cloneNodes $Circle01 cloneType:#copy newNodes:&nnl
		appendIfUnique arcGuiParts $Circle03
	$Circle02.rotation.controller.X_Rotation = 90
	$Circle03.rotation.controller.Y_Rotation = 90

	PointA = [0.0215,0.0015,0]
	PointB= [0.0215,-0.0015,0]

	ss = SplineShape pos:pointA
	addNewSpline ss
	addKnot ss 1 #corner #line PointA
	addKnot ss 1 #corner #line PointB
	updateShape SS
	ss
	CenterPivot $Shape01
		appendIfUnique arcGuiParts $Shape01
	maxOps.cloneNodes $Shape01 cloneType:#copy newNodes:&nnl 
		appendIfUnique arcGuiParts $Shape02
	maxOps.cloneNodes $Shape01 cloneType:#copy newNodes:&nnl 
		appendIfUnique arcGuiParts $Shape03
	$Shape01.rotation.controller.Z_Rotation = 90
	$Shape02.rotation.controller.X_Rotation = 135
	$Shape03.rotation.controller.X_Rotation = 45	

	
	for i in arcGuiParts do
	(
		convertTo i SplineShape
	)
	
	addAndWeld $Circle01 $Circle03 -1.0
		updateShape $Circle01
	addAndWeld $Circle01 $Circle02 -1.0
		updateShape $Circle01
	addAndWeld $Circle01 $Shape01 -1.0
		updateShape $Circle01
	addAndWeld $Circle01 $Shape02 -1.0
		updateShape $Circle01	
	addAndWeld $Circle01 $Shape03 -1.0
		updateShape $Circle01		
	
	--now need to add custom attribute for radius and hooko this up to the scale on a two way wire
	
	select $Circle01
	$.thickness = 0.0
	$.scale.controller = ScaleXYZ ()
	modPanel.addModToSelection (EmptyModifier ()) ui:on
		bname = "arcGui_Radius"
		
	rolloutCreation = ("arcGuiCA = attributes "+bname +"\n" +"(" +"\n" +"\t" +"parameters main rollout:params"+"\n" +"\t"+"(" +"\n" +"\t" +"\t" +"Radius" +" type:#float ui:" +"Radius"+" default:" +"1.0"+"\n" +"\t"+")" +"\n" +"\t"  +"rollout params "+" \"" +bname+" \"" +"\n" +"\t" +"(" +"\n" +"\t"+"\t"  +"spinner "+"Radius" +" \"" +"Radius" +"\"" +" "+"fieldwidth:40 range:[0,100,1] type:#float" +"\n" +"\t" +")" +"\n"+")"+"\n" +"\n" +"\t" +"custAttributes.add $" +$.name+".modifiers[#'Attribute holder'] "+"arcGuiCA")
		execute rolloutCreation				
		
	--now add a bezier float controller onto the newly created custom attribute		
	arcRad = ("$"+".modifiers[#Attribute_Holder]."+bname+"."+"Radius"+".controller = bezier_float ()")
		execute arcRad
		
	paramWire.connect2way $.modifiers[#Attribute_Holder].arcGui_Radius[#radius] $.scale.controller[#X_Scale] "X_Scale" "radius"		
	paramWire.connect2way $.modifiers[#Attribute_Holder].arcGui_Radius[#radius] $.scale.controller[#Y_Scale] "Y_Scale" "radius"		
	paramWire.connect2way $.modifiers[#Attribute_Holder].arcGui_Radius[#radius] $.scale.controller[#Z_Scale] "Z_Scale" "radius"		
		
		appendIfUnique arcGuiArray $Circle01
		$Circle01.name = ( guiName as string )	
		transformStr = ("in coordsys parent $" +(guiName as string)+arcGuiTransforms[arcArrayItem] )
-- 		print ("transformStr = " +transformStr)
 			trans = execute transformStr
		
)

fn postGuiMake = 
(
	print "Creating Mirrored versions"
	
	MirrorPoint = Point pos:[0,0,0]
	MirrorPoint.name = "MirrorPoint"
	MirrorPoint.scale.controller = ScaleXYZ ()

	$arcGUI_FB_Jaw.parent = MirrorPoint
	$arcGUI_FB_LowerLipRoot.parent = MirrorPoint
	$arcGUI_FB_LowerLip.parent = MirrorPoint
	$arcGUI_FB_L_Lip_Corner.parent = MirrorPoint
	$arcGUI_FB_L_Lid_Upper_Mid.parent = MirrorPoint
	$arcGUI_FB_L_Brow_Out.parent = MirrorPoint
	$arcGUI_FB_L_CheekBone.parent = MirrorPoint
	$arcGUI_FB_L_Nasolabial_Mid.parent = MirrorPoint
	$arcGUI_FB_L_Eye.parent = MirrorPoint
	$arcGUI_FB_L_Brow_Centre.parent = MirrorPoint
	$arcGUI_FB_UpperLipRoot.parent =  MirrorPoint
	$arcGUI_FB_UpperLip.parent = MirrorPoint
	
	MirrorPoint.scale.controller.X_Scale = -100 --temporarily flip them

	for i =  1 to rightGuis.count do
	(
		aG = arcGuiCreation rightGuis[i] i
		
			myNode = getNodeByName rightGuis[i] exact:true
			appendIfUnique createdRightArcs myNode
		
	)

		for i = 1 to createdRightArcs.count do
		(
			createdRightArcs[i].parent = $SKEL_Head
		)	

			for i = 1 to rightGuis.count do
			(
				r = getNodeByName rightGuis[i]
				l = getNodeByName leftGuis[i]
				
				r.transform = l.transform
				r.modifiers[#Attribute_Holder].arcGui_Radius.radius = l.modifiers[#Attribute_Holder].arcGui_Radius.radius 
				in coordsys local rotate r (EulerAngles 0 0 180)
			)
			
			print "Reparenting guis"	
			MirrorPoint.scale.controller.X_Scale = 100 --temporarily flip them
			$arcGUI_FB_Jaw.parent = $Skel_Head
			$arcGUI_FB_LowerLipRoot.parent = $Skel_Head
			$arcGUI_FB_LowerLip.parent = $Skel_Head
			$arcGUI_FB_L_Lip_Corner.parent = $Skel_Head
			$arcGUI_FB_L_Lid_Upper_Mid.parent = $Skel_Head
			$arcGUI_FB_L_Brow_Out.parent = $Skel_Head
			$arcGUI_FB_L_CheekBone.parent = $Skel_Head
			$arcGUI_FB_L_Nasolabial_Mid.parent = $Skel_Head
			$arcGUI_FB_L_Eye.parent = $Skel_Head
			$arcGUI_FB_L_Brow_Centre.parent = $Skel_Head
			$arcGUI_FB_UpperLipRoot.parent =  $Skel_Head
			$arcGUI_FB_UpperLip.parent = $Skel_Head
			
			delete $MirrorPoint
			
			fileIn (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Rigging_tools\\gta5_createFace.ms")
			
)

fn createWindow = 
(
	arcArrayItem = (arcArrayItem + 1 )
	print "ArcArray val incremented"
	
	rci.begin()

	rci.addControl #button #myButton arcName[arcArrayItem]

	rci.addHandler #myButton #pressed filter:on codeStr:"onButtonClicked()"

	rciTemp  = rci.end()

	createDialog rciTemp 

	print ("arcArrayItem = "+arcArrayItem as string )
	print ("arcArrayCount = "+arcName.count as string )
)

fn onButtonClicked = 
( 
	n = arcName.count
	if arcArrayItem > n do 
	(
		destroyDialog rciTemp 	
		MessageBox "Warning we exceeded arcName count"
	)
		
	if arcArrayItem == n then --do arcgui creation
	(
		destroyDialog rciTemp 	
		print "we should do the gta5 face thang now"
		postGuiMake()
		--now actually run gta5createface as all arcguis have been generated
	)
	else
	(
		print ("creating " +arcName[arcArrayItem])		
		destroyDialog rciTemp 	
		arcGuiCreation arcName[arcArrayItem] arcArrayItem
-- 			print ("arcArrayItem (" +arcArrayItem as string +") <= to arcNameCount (" +arcName.count as string +" )")
		createWindow()
	)
)

geo = $

	if geo != undefined then
	(
		if (classof geo.baseObject == Editable_Poly )then
		(
		clearListener()
		createWindow()
		)
		else
		(
			messageBox "Please pick the head geo and try again."
		)
	)
	else
	(
		messageBox "Please pick the head geo and try again."
	)