filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())


fn getKeys = --this finds the furthest key along the timeline on objects to allow setting of timeline range
(
--this is the variable used to find the furthest key along the timeline

	lastFrame = 0
	allBones = (selection as array)	
-- 	clearselection()
-- 	max select all
-- 	allBones = #($Char, 
-- 		$'Char Pelvis', 
-- 		$'Char L Thigh', 
-- 		$'Char L Calf', 
-- 		$'Char L Foot', 
-- 		$'Char L Toe0', 
-- 		$'Char R Thigh', 
-- 		$'Char R Calf', 
-- 		$'Char R Foot', 
-- 		$'Char R Toe0', 
-- 		$'Char Spine', 
-- 		$'Char Spine1', 
-- 		$'Char Spine2', 
-- 		$'Char Spine3', 
-- 		$'Char Neck', 
-- 		$'Char Head', 
-- 		$'Char L Clavicle', 
-- 		$'Char L UpperArm', 
-- 		$'Char L Forearm', 
-- 		$'Char L Hand', 
-- 		$'Char L Finger0', 
-- 		$'Char L Finger01', 
-- 		$'Char L Finger02', 
-- 		$'Char L Finger1', 
-- 		$'Char L Finger11', 
-- 		$'Char L Finger12', 
-- 		$'Char L Finger2', 
-- 		$'Char L Finger21', 
-- 		$'Char L Finger22', 
-- 		$'Char L Finger3', 
-- 		$'Char L Finger31', 
-- 		$'Char L Finger32', 
-- 		$'Char L Finger4', 
-- 		$'Char L Finger41', 
-- 		$'Char L Finger42', 
-- 		$'Char R Clavicle', 
-- 		$'Char R UpperArm', 
-- 		$'Char R Forearm', 
-- 		$'Char R Hand', 
-- 		$'Char R Finger0', 
-- 		$'Char R Finger01', 
-- 		$'Char R Finger02', 
-- 		$'Char R Finger1', 
-- 		$'Char R Finger11', 
-- 		$'Char R Finger12', 
-- 		$'Char R Finger2', 
-- 		$'Char R Finger21', 
-- 		$'Char R Finger22', 
-- 		$'Char R Finger3', 
-- 		$'Char R Finger31', 
-- 		$'Char R Finger32', 
-- 		$'Char R Finger4', 
-- 		$'Char R Finger41', 
-- 		$'Char R Finger42'
-- 		)
	
		
	for fb in allBones do
	(
		cnt = fb.controller.keys.count
-- 			print ("cnt = "+cnt as string)
		if cnt != -1 do
		(
			biggestKeyString = (fb.controller.keys[cnt] as string)
			fromIndex = ((findString biggestKeyString "@") + 2)
			toIndex = ((biggestKeyString.count) - 3)
			keyValueStr = (substring biggestKeyString fromIndex 20)
				fromIndex = 1
				toIndex = ((findString keyValueStr "f")-1)
				keyValueStr = (substring keyValueStr fromIndex toIndex) as Integer
			print ("keyValueStr = "+keyValueStr as string)
				
				--now test if keyValueStr > lastFrame and if so update lastFrame to this value
				if keyValueStr > lastFrame do
				(
					lastFrame = keyValueStr
				)
		)
	)
	
	print ("lastFrame = "+(lastFrame as string))
	furthestKey = (lastFrame as time)
		if furthestKey <= 0f do 
			(furthestKey = 0f +1f)
			
-- 		messageBox ("Setting final key to "+(furthestKey as string))
	animationRange = interval 0f furthestKey
)

getKeys()