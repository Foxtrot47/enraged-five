--old script to manually copy a vertex weight and then select another vert and mirror paste the skin weights
-- 2007 BC
-- Rick Stirling.
--This is old as fuck and really shit (Rick's words.)


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

global BoneArray
global BoneNArray
global WeightArray
global PasteArray




-- Pass the vert id to extract all the weights to arrays
fn CopyWeight theVert =
(
	-- Empty the arrays
	BoneArray = #()
	BoneNArray =#()
	WeightArray = #()
	
	theSkin = $.modifiers[#skin]
	BNumber = skinOps.getVertexWeightCount theSkin theVert
	BN = BNumber as string
	
	for i = 1 to BNumber do
	(
		boneID = skinOps.getVertexWeightBoneId theSkin theVert i
		boneName = skinOps.GetBoneName theSkin boneID 0
		boneWeight = skinOps.getVertexWeight theSkin theVert i

		append BoneArray boneID
		append BoneNArray boneName
		append WeightArray boneWeight
	)
)




fn PasteWeight pVert =
(
	disableSceneredraw()

	theSkin = $.modifiers[#skin]
	
	print "these are the stored arrays for copying"
	print pastearray
	print weightarray
	
	skinOps.ReplaceVertexWeights theskin pVert pasteArray WeightArray
		

	enableSceneredraw()
	redrawViews()
)



fn find_mirror arrayIndex =
(
	-- Build arrays of all the bones in the system
	theSkin = $.modifiers[#skin]
	totalbones = skinops.getnumberbones theSkin
	
	totalBoneNArray =#()
	
	for i = 1 to totalbones do
	(
		boneName = skinOps.GetBoneName theSkin i 0
		append totalBoneNArray boneName
	)
	
	
	
	-- Now we know all the bones, lets parse of list of weighted bones
	

	SB = BoneNArray[arrayIndex] as string
	boneToFind=SB
	
	-- Find bone names beginning with CHAR
	if (substring SB 1 4 == "Char") then
	(
		BNprefix=substring SB 1 7 as string
		BNsuffix= substring SB 8 255 as string
		
		newPrefix=BNprefix
		if BNprefix == "Char L " then newPrefix= "Char R "
		if BNprefix == "Char R " then newPrefix= "Char L "
		
		boneToFind = newPrefix + BNsuffix
	)
	
	if (substring SB 2 1 == " ") then
	(
		BNprefix=substring SB 1 2 as string
		BNsuffix= substring SB 3 255 as string
		
		newPrefix=BNprefix
		if BNprefix == "L " then newPrefix= "R "
		if BNprefix == "R " then newPrefix= "L "
		
		boneToFind = newPrefix + BNsuffix
	)
	
	
	if (substring SB 1 1 == "l" or substring SB 1 1 == "r") then
	(
		BNprefix=substring SB 1 1 as string
		BNsuffix= substring SB 2 255 as string
		
		newPrefix=BNprefix
		if BNprefix == "l" then newPrefix= "r"
		if BNprefix == "r" then newPrefix= "l"
		
		boneToFind = newPrefix + BNsuffix
	)
	
	
	
	if (substring SB 1 4 == "FB_L" or substring SB 1 4 == "FB_R") then
	(
		BNprefix=substring SB 1 4 as string
		BNsuffix= substring SB 5 255 as string
		
		newPrefix=BNprefix
		if BNprefix == "FB_L" then newPrefix= "FB_R"
		if BNprefix == "FB_R" then newPrefix= "FB_L"
		
		boneToFind = newPrefix + BNsuffix
	)
	

	-- ok, at this stage we have an arracy of bone names, and we know what name
	-- we are looking for. so cycle the array and find the index!
	theIndex = 0
	
	-- Do we need to find a new bone?
		
	if (boneToFind != SB) then
	(
		print bonearray
		for i = 1 to totalbones do
		(
			thecheck= totalBoneNArray[i] as string
			if thecheck == bonetofind then theIndex = i
		)
	
		pasteArray[arrayIndex]=theIndex	
	
	)
)



rollout blank "Single Skin Copy and Paste"
(
	label lbl1 "Select vert, press copy, then select vert and press paste." pos:[9,8] width:197 height:48
	button ButtonC "Copy" pos:[18,40] width:75 height:24
	button ButtonP "Paste" pos:[110,40] width:75 height:24
	
	
	on ButtonP pressed do
	(
		theSkin = selection[1].modifiers[#Skin]
		n = skinOps.getNumberVertices theSkin
		
		selVert=0
		
		for i = 1 to n do 
		(
			if (skinops.isVertexSelected theSkin i == 1) then 
			(
				selVert=i
			)
		)	
		
		-- Call the copy function 
		PasteWeight selVert
	)
	
	
	

	on ButtonC pressed do 
	(
		-- Get vert id
		theSkin = selection[1].modifiers[#Skin]
		n = skinOps.getNumberVertices theSkin
		
		selVert=0
		
		for i = 1 to n do 
		(
			if (skinops.isVertexSelected theSkin i == 1) then 
			(
				selVert=i
			)
		)	
		
		-- Call the copy function
		CopyWeight selVert
		
		-- We now have 3 arrays - bone id, bone name, and bone weight.
		-- some of the bones might need to be mirrored so for each bone, find the possible mirror
		n = BoneArray.count
		pastearray = bonearray
		
		for i = 1 to n do
		(
			find_mirror i
		)
	)
)

-- Create floater 
theNewFloater = newRolloutFloater "Rollout title" 220 100

-- Add the rollout section
addRollout blank theNewFloater
