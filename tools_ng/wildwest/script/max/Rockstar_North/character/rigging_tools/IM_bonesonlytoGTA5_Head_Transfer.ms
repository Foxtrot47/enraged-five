-- Quick IM head tester
-- Transplants IM delivery onto 



-- Include all the standard useful shite.
filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()


-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_Rigging.ms")


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

bodyfile = (RsConfigGetArtDir() + "peds/Skeletons/MP3_GTA5_Conversion.max")


-- Folders to save skin data
try (makedir "c:/skins") catch()--Try to makes a skin data folder if it doesn't already exist.
try (makedir "c:/skins/bones") catch()





-- This functions bakes and saves weights for selected objects
fn saveSkinWeightsSO = (
	
	-- Put the selected objects into an array
	objSelectedArray = getCurrentSelection()
	objCount = objSelectedArray.count
	

	-- for each selected skinned object, bake the weights
	-- then save them out.
	for i = 1 to objCount do  (
		
		obj = objSelectedArray[i] -- pick an object out of the array.
		select obj
		
		skobj = ("c:/skins/" + maxFilename +"_" + $.name +".env") as string
						
		max modify mode
		modPanel.setCurrentObject obj.modifiers[#Skin]
		
		-- Skin edit mode, select all the verts
		subobjectLevel = 1
		obj.modifiers[#Skin].Filter_Vertices = on
		theSkin = selection[1].modifiers[#Skin]
		nsv = skinOps.getNumberVertices theSkin
		skinOps.SelectVertices obj.modifiers[#Skin] #{1..nsv}
		
		-- Now bake these weights
		-- Baking is a good thing to do. MMMM cookies
		skinOps.bakeSelectedVerts obj.modifiers[#Skin]
					
		-- And save the weights out to the weight folder	
		skinOps.saveEnvelope obj.modifiers[#Skin] skobj
			
		clearSelection()
	) -- object selection loop
)






try ($mp_head_000_r.name = "head_000_r") catch()
try ($mp_teef_000_u.name = "teef_000_u") catch()


-- Kill the arms and clavs
clearSelection()
try (
	select #($arm_r,  $clavicle_l, $arm_l, $clavicle_r)
	delete $
) catch()


-- Rename the spine, neck and head bones.
try ($def_sternumRoot.name = "SKEL_Spine3") catch ()
try ($def_neck_1.name = "SKEL_Neck_1") catch ()
try ($def_head.name = "SKEL_Head") catch ()

-- The head and teeth skin modifiers will have been automatically updated.
-- I'll bake the skin weights and save them anyway.

-- Make sure the head and teeth have the correct names
clearselection()
try (
	select $Head_000_*
	$.name = "head_000_r"
	) catch()
	
	
clearselection()
try (
	select $teef_000_*
	$.name = "teef_000_u"
	) catch()
	

	
	
clearselection()
select #($teef_000_u, $head_000_r)


SaveSkinWeightsSO() 

-- Merge in a body mesh
mergemaxfile bodyfile

-- Reparent the rig
$SKEL_Spine3.parent = $SKEL_Spine2

$SKEL_L_Clavicle.parent = $SKEL_Spine3
$SKEL_R_Clavicle.parent = $SKEL_Spine3
$SKEL_Neck_1.parent = $SKEL_Spine3


-- need to do some bone tagging
setUserPropbuffer $SKEL_Spine3 "tag = BONETAG_SPINE3"
setUserPropbuffer $SKEL_Neck_1 "tag = BONETAG_NECK"
setUserPropbuffer $SKEL_Head "tag = BONETAG_HEAD"

-- The head and teeth now need to have all the body bones added
-- select all the bones
select $SKEL_*
selectmore $RB_*
selectmore $PH_*

bodyBoneSelection = getcurrentselection()

clearselection()

faceboneSelection = #()
select $SKEL_Head


for p in selection do (
	if p.children != undefined do (
		selectmore p.children
	)
)
	
deselect $SKEL_Head
faceboneSelection = getcurrentselection()


allBoneSelection = faceboneSelection + bodyBoneSelection



-- for each skinned object, we need to add the missing bones
allMeshes = #($head_000_R, $teef_000_U, $uppr_000_U, $lowr_000_U)


for m = 1 to allMeshes.count do (
	obj = allMeshes[m]
	modPanel.setCurrentObject obj.modifiers[#Skin]
	totalSkinModBones = skinOps.getNumberBones obj.modifiers[#Skin]
	print ("Bones in mesh = " + (totalSkinModBones as string))
	currentskinnedbones = #()
	currentskinnedboneNames = #()
	for i = 1 to totalSkinModBones do (
		thebonename = (skinOps.getBoneName obj.modifiers[#Skin]  i 1)
		thebone = getnodebyname thebonename
		append currentskinnedboneNames thebonename
		append currentskinnedbones thebone
	)

	requiredBones = differenceInArray allBoneSelection currentskinnedbones
	print ("Bones to add = " +(requiredBones.count as string))

	for bc = 1 to requiredBones.count do (
			try (skinOps.addBone obj.modifiers[#Skin] (requiredBones[bc]) 1) catch()
	)	
	
)





-- Face bones too
select faceboneSelection
for thebone in selection do (	
	tag = "tag = " + (thebone.name as string)
	tag = tag +"\r\n"+ "exportTrans = true"
	setUserPropbuffer thebone tag
)


-- fix up the upper and lower
select $uppr_000_U
$uppr_000_U.modifiers[#Skin].mirrorEnabled = off
subobjectLevel = 1
skinOps.SelectVertices $uppr_000_U.modifiers[#Skin] #{1..4}
$uppr_000_U.modifiers[#Skin].effect = 0
skinOps.SelectBone $uppr_000_U.modifiers[#Skin] 2
skinOps.setWeight $uppr_000_U.modifiers[#Skin] 1
subobjectLevel = 0



select $lowr_000_U
$lowr_000_U.modifiers[#Skin].mirrorEnabled = off
subobjectLevel = 1
skinOps.SelectVertices $lowr_000_U.modifiers[#Skin] #{1..4}
$uppr_000_U.modifiers[#Skin].effect = 0
skinOps.SelectBone $lowr_000_U.modifiers[#Skin] 2
skinOps.setWeight $lowr_000_U.modifiers[#Skin] 1
subobjectLevel = 0



-- Parent this stuff
select allMeshes
$.parent = $Z_Z_MaxTest

$Z_Z_MaxTest.name = "Z_Z_MaxTest"

-- set the materials correctly

select $head_000_R
modPanel.setCurrentObject $.baseObject
subobjectLevel = 4
actionMan.executeAction 0 "40021"  -- Selection: Select All
$.EditablePoly.setMaterialIndex 1 0
subobjectLevel = 0


select $teef_000_U
modPanel.setCurrentObject $.baseObject
subobjectLevel = 4
actionMan.executeAction 0 "40021"  -- Selection: Select All
$.EditablePoly.setMaterialIndex 15 0
subobjectLevel = 0


meditMaterials[13] = $uppr_000_U.material

$head_000_R.material = meditMaterials[13]
$teef_000_U.material = meditMaterials[13]



-- The IM rig has strange orientations for the spine, neck and head
