filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())


-- load the common functions
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_common.ms"
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_Joystick_Bones.ms"


rollout jcb "Joystick Control Builder" width:163 height:175
(	

	-- UI here
	group "Create New Joystick" 
	(
		Edittext jsn "Joystick Name: "
		RadioButtons jstype labels:#("Square", "Vertical", "Horizontal") default:2
		Button crjstick "Create Joystick" width:150 height:40
	)

	
	on crjstick pressed do 
	(
		-- call the joystick creation function with the UI data
		jsname = jsn.text as string
		jstyle = jstype.state as integer
		jpos = [1,0,2] -- the position on screen. This can overridden later, perhaps by mouse click.

		seljoy = createJoystick jsname jstyle jpos 1
	)

) -- rollout end


createDialog jcb 250 150 