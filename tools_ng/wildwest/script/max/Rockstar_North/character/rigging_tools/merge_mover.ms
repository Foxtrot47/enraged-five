filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

--Script to automate the update of the ped mover.  Bug 85080
--Stewart Wright 08/03/11

filein "rockstar/export/settings.ms"

-- Figure out the project data
theProjectRoot = RsConfigGetProjRootDir()


fn mergeMover =
(
	--store some stuff about the current mover
	oldName = $mover.name
	oldMover = $mover
	oldParent = $mover.parent
	oldChild = $mover.children[1]

	--merge in the new mover
	print "Merging new mover.  Radius 0.25"
	moverFile = theProjectRoot + "art/peds/Skeletons/Transitional/NewMover.max"--filepath of the file we want to merge
	moverObj = #("NewMover")--what do we want to merge?
	try (mergeMaxFile moverFile moverObj #deleteOldDups #useSceneMtlDups #alwaysReparent quiet:true)
	catch
		(
			messagebox "Couldn't merge new mover file"
		)
	print "New mover merged"

	--delete the old mover and rename the new one
	delete $mover
	$NewMover.name = oldName
	newMover = $mover
	
	--link stuff skel_root > mover > dummy
	newMover.parent = oldParent
	oldChild.parent = newMover
	print (oldChild.name + " linked to mover.  Mover linked to " + oldParent.name)
	
	print "New mover merged and linked"
)--end mergeMover


mergeMover()