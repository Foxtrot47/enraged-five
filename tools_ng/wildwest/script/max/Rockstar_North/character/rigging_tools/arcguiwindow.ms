arcName = #( --array of all arcGui objects
	"arcGUI_FB_Skull", 
	"arcGUI_FB_L_Lip_Corner", 
	"arcGUI_FB_L_Lid_Upper_Mid", 
	"arcGUI_FB_L_Brow_Out", 
	"arcGUI_FB_R_Lid_Upper_Mid", 
	"arcGUI_FB_L_CheekBone", 
	"arcGUI_FB_R_CheekBone", 
	"arcGUI_FB_R_Brow_Out", 
	"arcGUI_FB_R_Nasolabial_Mid", 
	"arcGUI_FB_L_Nasolabial_Mid", 
	"arcGUI_FB_L_Eye", 
	"arcGUI_FB_R_Brow_Centre", 
	"arcGUI_FB_R_Eye", 
	"arcGUI_FB_L_Brow_Centre", 
	"arcGUI_FB_UpperLipRoot", 
	"arcGUI_FB_UpperLip", 
	"arcGUI_FB_Jaw", 
	"arcGUI_FB_LowerLipRoot", 
	"arcGUI_FB_LowerLip",
	"arcGUI_FB_R_Lip_Corner"
	)

global rci = rolloutCreator "arcGuiWindow" "arc gui generator"
global rciTemp = ()
	arcArrayItem = 1
	
fn onButtonClicked = 
( 
	if arcArrayItem <= arcName.count then --do arcgui creation
	(
		print ("clickety click " +arcName[arcArrayItem])
	arcArrayItem = (arcArrayItem + 1 )
	destroyDialog rciTemp 
	
		if arcArrayItem <= arcname.count then
		(
			print ("arcArrayItem (" +arcArrayItem as string +") <= to arcNameCount (" +arcName.count as string +" )")
			createWindow()
		)
		else
		(
			destroyDialog rciTemp
		)
	)
	else
	(
		print "we should do the gta5 face thang now"
		--now actually run gta5createface as all arcguis have been generated
	)
)
	
fn createWindow = 
(
	
	rci.begin()

	rci.addControl #button #myButton arcName[arcArrayItem]

	rci.addHandler #myButton #pressed filter:on codeStr:"onButtonClicked()"

	rciTemp  = rci.end()

	createDialog rciTemp 
	
)

createWindow()