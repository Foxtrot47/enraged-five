-- skinPpopulateWindow.ms
-- script to provide access to the boneInput/Output tools
-- Matt Rennie
-- May 2010

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())


if ((skinPopulateWindow != undefined) and (skinPopulateWindow.isDisplayed)) do
	(destroyDialog skinPopulateWindow)

rollout skinPopulateWindow "Skin Populater"
(
	button btn1 "Output Bones" tooltip:"Save out bones from current mesh."
	button btn2 "Input Bones" tooltip:"Populate skin modifier with bones form file."
-- 	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Build_Facial_Rig" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)
	
	on btn1 pressed do
	(
		fileIn (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Rigging_tools\\boneOutput.ms")
		
	)
	
	on btn2 pressed do
	(
		fileIn (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Rigging_tools\\boneInput.ms")
		--destroyDialog skinPopulateWindow
	)
	
)

CreateDialog skinPopulateWindow