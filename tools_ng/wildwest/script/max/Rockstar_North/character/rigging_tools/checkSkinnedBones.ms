-- script to print out bones which are in a skin modifier but which arent influencing any verts
--Dec 2010
-- Matt Rennie.


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())
	
	
skinnedObjects = selection as array

clearListener()

fn checkItsSkinned =
(
	for i = 1 to skinnedObjects.count do
	(
		select skinnedObjects[i]
		usedBones = #()
		bonesInSkin = #()
		bonesNotUsed = #()
		numberSkinBones = skinops.getNumberBones skinnedObjects[i].modifiers[#Skin]
		numberSkinVerts = skinOps.GetNumberVertices skinnedObjects[i].modifiers[#Skin]
-- 		vertexInfluences = skinOps.GetVertexWeightCount skinnedObjects[i].modifiers[#Skin]
		
		for bs = 1 to numberSkinBones do
		(
			boneName = skinOps.GetBoneName skinnedObjects[i].modifiers[#Skin] bs 1
			appendIfUnique bonesInSkin boneName --append to this array so we get a list of all bones that are actually in the skin
		)
		
		for v = 1 to numberSkinVerts do
		(
			vertexInfluences = skinOps.GetVertexWeightCount skinnedObjects[i].modifiers[#Skin] v
			for b = 1 to vertexInfluences do
			(
				boneIndex = skinOps.GetVertexWeightBoneID skinnedObjects[i].modifiers[#Skin] v b
				boneName = skinOps.GetBoneName skinnedObjects[i].modifiers[#Skin] boneIndex 1
				appendIfUnique usedBones boneName --append to this array so we get a list of all bones that are actually skinned to
			)
		)
		
		--now compare the array of all bones vs the ones actually skinned to
		
		for bs = 1 to bonesInSkin.count do
		(
			found = (findItem usedBones bonesInSkin[bs])
			if found == 0 do
			(
				appendIfUnique bonesNotUsed bonesInSkin[bs]
			)
		)
		
		print "-----------------------------------"
		print ("Bones not used on "+skinnedObjects[i].name+" are:")
		print bonesNotUsed
		print "================================"
	)
)

checkItsSkinned()