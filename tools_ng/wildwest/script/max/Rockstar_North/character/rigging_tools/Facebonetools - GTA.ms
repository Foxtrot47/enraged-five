-- Facial rigging tools
-- Rick Stirling
-- In need of a shave

-- This script'll create selection sets, merge bones, load and save animations


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())



fn tagtimebar = (

	tag1 = FrameTagManager.CreateNewTag "Neutral" 0
	tag1a = FrameTagManager.CreateNewTag "Blink" 3
	tag2 = FrameTagManager.CreateNewTag "AAA" 5
	tag3 = FrameTagManager.CreateNewTag "EEE" 10
	tag4 = FrameTagManager.CreateNewTag "OOO" 15
	tag5 = FrameTagManager.CreateNewTag "MMM" 20
	tag6 = FrameTagManager.CreateNewTag "FFF" 25
	tag7 = FrameTagManager.CreateNewTag "Neutral" 30
	tag8 = FrameTagManager.CreateNewTag "SMILE" 35
	tag9 = FrameTagManager.CreateNewTag "OPEN JAW" 40
	tag10 = FrameTagManager.CreateNewTag "ANGRY" 45
	tag11 = FrameTagManager.CreateNewTag "SURPRISE" 50
	tag12 = FrameTagManager.CreateNewTag "THINK" 55
	tag13 = FrameTagManager.CreateNewTag "PAIN" 60
	tag14 = FrameTagManager.CreateNewTag "Neutral" 65
	tag15 = FrameTagManager.CreateNewTag "Eyes Right" 70
	tag16 = FrameTagManager.CreateNewTag "Eyes Left" 75
	tag17 = FrameTagManager.CreateNewTag "Eyes Up" 80
	tag18 = FrameTagManager.CreateNewTag "Eyes Down" 85
	tag19 = FrameTagManager.CreateNewTag "Neutral" 90
	tag20 = FrameTagManager.CreateNewTag "Neutral" 95

)


fn mergeFaceBones =(

	-- Are they already there?
	try (
		select $FB_C_Brow
		messagebox "Face bones already exist - merge manually to override existing bones, or delete them from the scene"
		)
	catch (
		--load them 
		fobj_names = #("FB_C_Brow", "FB_C_Cheeks", "FB_L_Brow", "FB_L_Crn_Mouth", "FB_L_Eyeball", "FB_L_Eyelid", "FB_R_Brow", "FB_R_Crn_Mouth", "FB_R_Eyeball", "FB_R_Eyelid", "PointFB_C_Jaw", "FB_C_Jaw", "PointFB_R_LipLower", "FB_R_LipLower", "PointFB_L_LipLower", "FB_L_LipLower", "PointFB_L_LipUpper", "FB_L_LipUpper", "PointFB_R_LipUpper", "FB_R_LipUpper")

		bonefile = "x:\GTA\gta_art\peds\Base_Stuff\March_2007_base\Base Male Face Rig.max"
		mergemaxfile bonefile fobj_names #select
		
		messagebox "Face bones added - they will need to be positioned per head"

	)
)




fn addskinbones =
(
	-- We need to add the face bones to the SELECTED skinned components
	-- is an object selected?
	nc = selection.count
	
	-- No, so we quit.
	if nc == 0 then 
	(
		outtext = "Nothing selected"
		messagebox outtext
		return 0
	)
	
	
	-- Yes, so build an array to hold a list of the selected objects
	comparray =#()
	-- Don't make an array with a single object, that is just silly!	
	if nc > 1 then 
	(
		print "greater than 1"
		comparray = $ as array
	)
	else append comparray $
	
	
	-- We now have an array of components to loop through
	-- Loop through and add the facebones AND helpers
	
	for i = 1 to nc do (
		obj = comparray[i]
		print obj
		select obj

		-- Add face bones to the selected component
		skinOps.addBone $.modifiers[#Skin] $'FB_C_Brow' 1
		skinOps.addBone $.modifiers[#Skin] $'FB_C_Cheeks' 1
		skinOps.addBone $.modifiers[#Skin] $'FB_C_Jaw' 1
			
		skinOps.addBone $.modifiers[#Skin] $'FB_L_Brow' 1
		skinOps.addBone $.modifiers[#Skin] $'FB_L_Crn_Mouth' 1
		skinOps.addBone $.modifiers[#Skin] $'FB_L_Eyeball' 1
		skinOps.addBone $.modifiers[#Skin] $'FB_L_Eyelid' 1
			
		skinOps.addBone $.modifiers[#Skin] $'FB_R_Brow' 1
		skinOps.addBone $.modifiers[#Skin] $'FB_R_Crn_Mouth' 1
		skinOps.addBone $.modifiers[#Skin] $'FB_R_Eyeball' 1
		skinOps.addBone $.modifiers[#Skin] $'FB_R_Eyelid' 1
			
		skinOps.addBone $.modifiers[#Skin] $'FB_L_LipUpper' 1
		skinOps.addBone $.modifiers[#Skin] $'FB_L_LipLower' 1
		
		skinOps.addBone $.modifiers[#Skin] $'FB_R_LipUpper' 1
		skinOps.addBone $.modifiers[#Skin] $'FB_R_LipLower' 1
		
		skinOps.addBone $.modifiers[#Skin] $'PointFB_C_Jaw' 1
		skinOps.addBone $.modifiers[#Skin] $'PointFB_R_LipUpper' 1
		skinOps.addBone $.modifiers[#Skin] $'PointFB_L_LipUpper' 1
		skinOps.addBone $.modifiers[#Skin] $'PointFB_R_LipLower' 1
		skinOps.addBone $.modifiers[#Skin] $'PointFB_L_LipLower' 1
			
	)
)





fn saveFaceBones =
(
	-- is an object selected, is it a head? 
	obj = selection.count
	if obj > 0 then
	(
		-- get head name
		chn = $.name as string
		
		-- is this a head?
		checkit = substring chn 1 1
		
		if (checkit =="H" or checkit =="h") then
		(
			
			-- get filename
			-- GTA 
			thename = maxfilename as string
			thepath = maxfilepath as string
			shortname = filterstring thename "."
			
			headname = shortname[1]+"_"+chn
			
			xafp = thepath+"xaf"
			xafn = xafp +"\\" + headname + ".xaf"
			
			-- create folder if needed
			try 
			(
				makedir xafp
			) catch()
			
			-- select the facebones
			select #($FB_C_Brow, $FB_C_Cheeks, $FB_L_Brow, $FB_L_Crn_Mouth, $FB_L_Eyeball, $FB_L_Eyelid, $FB_R_Brow, $FB_R_Crn_Mouth, $FB_R_Eyeball, $FB_R_Eyelid, $FB_R_LipUpper, $FB_C_Jaw, $FB_L_LipUpper, $FB_R_LipLower, $FB_L_LipLower, $PointFB_L_LipUpper, $PointFB_R_LipLower, $PointFB_R_LipUpper, $PointFB_C_Jaw, $PointFB_L_LipLower)
				
			-- this is a bugfix from a forum
			attArray = #()
			usrArray = #()
			
			-- save out the animation, 100 frames only.
			currentanimrange = animationrange
			animationRange = interval 0f 100f
			LoadSaveAnimation.saveanimation xafn $ attArray usrArray segInterval: (interval 0f 100f)
			-- reset the animation range
			animationRange = currentanimrange
		)
		
		else 
		(
			messagebox "No head selected"
		)
	) -- it was a valid object
	
	else 		
	(
		messagebox "No object selected (A head would be a good choice)"
	)


)



fn loadCurrentFaceBones =(

	-- is an object selected, is it a head?
	obj = selection.count
	if obj >0 then
		(
		-- get head name
		chn = $.name as string
		
		-- is this a head?
		checkit = substring chn 1 1
		
		if (checkit =="H" or checkit =="h") then
		(
			
			-- get filename
			-- GTA 
			thename = maxfilename as string
			thepath = maxfilepath as string
			shortname = filterstring thename "."
			
			headname = shortname[1]+"_"+chn
			
			xafp = thepath+"xaf"
			xafn = xafp +"\\" + headname + ".xaf"
			
			
			-- select the facebones
			select #($FB_C_Brow, $FB_C_Cheeks, $FB_L_Brow, $FB_L_Crn_Mouth, $FB_L_Eyeball, $FB_L_Eyelid, $FB_R_Brow, $FB_R_Crn_Mouth, $FB_R_Eyeball, $FB_R_Eyelid, $FB_R_LipUpper, $FB_C_Jaw, $FB_L_LipUpper, $FB_R_LipLower, $FB_L_LipLower, $PointFB_L_LipUpper, $PointFB_R_LipLower, $PointFB_R_LipUpper, $PointFB_C_Jaw, $PointFB_L_LipLower)
			
			-- load animation onto them
			LoadsaveAnimation.loadanimation xafn $
		)
		
		else 
		(
			messagebox "No head selected"
		)
	) -- it was a valid object
	else 		
	(
		messagebox "No object selected (A head would be a good choice)"
	)

)



fn loadAllFaceBones =(

	-- Look for all xaf files.
	-- load them at offsets onto the heads in the scene
	
	headmatch=#()
	thename = maxfilename as string
	thepath = maxfilepath as string
	shortname = filterstring thename "."
			
	xafp = thepath+"xaf\\*.xaf"
	xfiles = getFiles xafp
	xfcount = xfiles.count
	
	-- ok, filter the string to read the head names
	for i = 1 to xfcount do
	(
		-- split filename by \, take the last one.
		meh = filterstring xfiles[i] "\\"
		anim = uppercase (meh[8]) 
		
		-- lets rip off the model name from the front
		ripoff = shortname[1].count + 2 
		
		hm = substring anim ripoff 100
		hm = (filterstring hm ".")[1]
		append headmatch hm
		
		-- now we have an array of animation files, and a matching list of head objects
	)
	
	-- right, now load the animations
	frameoffset = 0
	
	for i = 1 to xfcount do
	(
		print xfiles[i]
		print headmatch[i]
		
		
		
		-- select the bones
		select #($FB_C_Brow, $FB_C_Cheeks, $FB_L_Brow, $FB_L_Crn_Mouth, $FB_L_Eyeball, $FB_L_Eyelid, $FB_R_Brow, $FB_R_Crn_Mouth, $FB_R_Eyeball, $FB_R_Eyelid, $FB_R_LipUpper, $FB_C_Jaw, $FB_L_LipUpper, $FB_R_LipLower, $FB_L_LipLower, $PointFB_L_LipUpper, $PointFB_R_LipLower, $PointFB_R_LipUpper, $PointFB_C_Jaw, $PointFB_L_LipLower)

		-- load the animation onto them at given frame
		xafn = xfiles[i]
		LoadsaveAnimation.loadanimation xafn $ insertTime: frameoffset
		
		-- ofset a further 100 frame away
		frameoffset = frameoffset + 100

	)
	

)





rollout modelname "Ricks Facialiser"
(
	-- Buttons are cool.
	button mfb "Merge Face Bones" pos:[10,10] width:180 height:30
	button timetags "Tag the timebar" pos:[10,50] width:180 height:30
	button addbones "Add Skin bones" pos:[10,90] width:180 height:30
	button scfb "Save Current Face Bones" pos:[10,130] width:180 height:30
	button lcfb "Load Current Face Bones" pos:[10,170] width:180 height:30
	button lafb "Load All Face Bones" pos:[10,210] width:180 height:30

	button bmon "Box Mode On" pos:[10,250] width:90 height:30
	button bmoff "Box Mode Off" pos:[100,250] width:90 height:30


	on addbones pressed do
	(
		addskinbones()
	)

	on timetags pressed do
	(
		tagtimebar()
	)

	on mfb pressed do
	(
		mergeFaceBones()
	)

	on scfb pressed do
	(
		saveFaceBones()
	)
	
	on lcfb pressed do
	(
		loadCurrentFaceBones()
	)
	
	on lafb pressed do
	(
		loadAllFaceBones()
	)


	on bmon pressed do
	(
			cursel = $
			select #($FB_C_Brow, $FB_C_Cheeks, $FB_L_Brow, $FB_L_Crn_Mouth, $FB_L_Eyeball, $FB_L_Eyelid, $FB_R_Brow, $FB_R_Crn_Mouth, $FB_R_Eyeball, $FB_R_Eyelid, $FB_R_LipUpper, $FB_C_Jaw, $FB_L_LipUpper, $FB_R_LipLower, $FB_L_LipLower, $PointFB_L_LipUpper, $PointFB_R_LipLower, $PointFB_R_LipUpper, $PointFB_C_Jaw, $PointFB_L_LipLower)
			$.boxmode = on
			select cursel
	)


	on bmoff pressed do
	(
			cursel = $
			select #($FB_C_Brow, $FB_C_Cheeks, $FB_L_Brow, $FB_L_Crn_Mouth, $FB_L_Eyeball, $FB_L_Eyelid, $FB_R_Brow, $FB_R_Crn_Mouth, $FB_R_Eyeball, $FB_R_Eyelid, $FB_R_LipUpper, $FB_C_Jaw, $FB_L_LipUpper, $FB_R_LipLower, $FB_L_LipLower, $PointFB_L_LipUpper, $PointFB_R_LipLower, $PointFB_R_LipUpper, $PointFB_C_Jaw, $PointFB_L_LipLower)
			$.boxmode = off
			select cursel
	)


	
)



-- Create floater 
theNewFloater = newRolloutFloater "Ricks Facialiser" 210 380

-- Add the rollout section
addRollout modelname theNewFloater