--boneInput.ms
--matt Rennie
--jan 2010
--Script to input a list of bones to a skin modifier from a .dat file
-- clearListener()


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

boneList = #()

--print boneList

fn inputBones = 
(
	
	--*************************************************************************************
	-- NEED TO ADD A DISABLE UI HERE OTHERWISE IT TAKES AGES!
	--*************************************************************************************
	
	Geo = selection
	if Geo.count == 1 then
	(
		
		g = $
		if g.modifiers[#Skin] == undefined then
		(
			messageBox "Please pick an object with a skin modifier." title:"gta5_boneinput"
		)
		else
		(
			skinMod = $.modifiers[#Skin]
			if input_name == undefined do
			(
				input_name = getOpenFileName caption:"Bone input file" types:"BoneData (*.bon)|*.bon|All Files (*.*)|*.*|"
			)

			if input_name != undefined then
			(
			f = openfile input_name
			inputData = #() -- define as array
			  while not eof f do
			  (
			   append inputData (filterstring (readLine f) ",")
			  )
			  close f

				for i in inputData do
				(
					currBonename = (i as string)
					currBoneLength = currBonename.count
					currBone = ("$"+(substring currBonename 4 (currBonelength - 5)))
						executeStr = ("if "+currBone+" != undefined do ("+"select "+currBone+")")
						currentBone = execute executeStr
						currentBone = execute currBone
-- 						if currentBone != undefined do
						if currentBone != undefined then	
						(
						appendIfUnique boneList $
						)
						else
						(
							print ("Couldn't find "+currBone+" in this scene.")
						)
				)
-- 				print ("BoneList = " +(boneList as String))
				for i = 1 to boneList.count do
				(
					select g
					SetCommandPanelTaskMode mode:#modify
					boneToAdd = getNodeByName bonelist[i].name
					
					boneUpdateInt = undefined
					if i != bonelist.count then
					(
						boneUpdateInt = 1
					)
					else
					(
						boneUpdateInt = 0
					)
					
					if boneToAdd != undefined do
					(
						skinOps.addbone g.modifiers[#Skin] boneList[i] boneUpdateInt
					)
				)
			)
		)

	)
	else
	(
	messageBox "Please run with only 1 object selected." title:"gta5_boneinput"
	)
)

gc()
inputBones()