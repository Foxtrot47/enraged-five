--script to record bind pose

-- global recPasteBindPosGUI = undefined

bindPoseArray = #()

fn recBindPose = 
(
	selA = objects as array
	
				output_name = getSaveFileName caption:"BindPose file" types:"BPos Data (*.bPos)|*.bpos|All Files (*.*)|*.*|"
	-- 		
			if output_name != undefined then 
			(
				
				if (doesFileExist output_name) == true do
				(	
					deleteFile output_name
				)
				output_file = createfile output_name		

				for b = 1 to selA.count do
				(
					nom = selA[b].name
					if (substring nom 1 4) == "SKEL" do
					(
						bTrans = in coordsys parent selA[b].transform
						arrStr = ("if $"+nom+" != undefined do ("+("in coordsys parent $"+nom+".transform = "+(in coordsys parent selA[b].transform as string))+")")
						format ((arrStr as string)+"\r") to:output_file
					)
				)
				
				close output_file
	
			)
	print bindPoseArray
)


fn pasteBindPose = 
(
	if input_name == undefined do
	(
		input_name = getOpenFileName caption:"BindPose file" types:"BPosData (*.bPos)|*.bpos|All Files (*.*)|*.*|"
	)

	if input_name != undefined then
	(
		f = openfile input_name
		inputData = #() -- define as array
		while not eof f do
		(
			append inputData (filterstring (readLine f) "\n")
		)
		close f

		for i = 1 to inputData.count do
		(
			execute inputData[i][1]
		)
	)	
)



















------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if ((recPasteBindPosGUI != undefined) and (recPasteBindPosGUI.isDisplayed)) do
	(destroyDialog recPasteBindPosGUI)

rollout recPasteBindPosGUI "Bind Pose Tools"
(
	button btnRec "Record BindPose" width:110
	button btnPaste "LoadBindPose" width:110

	on btnRec pressed do
	(
		clearListener()
		recBindPose()
	)

	on btnPaste pressed do
	(
		pasteBindPose()
	)

)

CreateDialog recPasteBindPosGUI width:125 pos:[1450, 100] 