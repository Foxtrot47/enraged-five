-- c_regenArcGuiWindow.ms
-- script to create a gui for regenning arcGui objects
-- Matt Rennie
-- Apr 2010

global regenArcGuiWindow --declare the rollout as global so it can be destroyed

if ((regenArcGuiWindow != undefined) and (regenArcGuiWindow.isDisplayed)) do
	(destroyDialog regenArcGuiWindow)

rollout regenArcGuiWindow "Arc Regen"
(
	button btn1 "Regenerate ArcGui's" tooltip:"Pick head Geo and click to regenerate ArcGui objects."
		
	on btn1 pressed do
	(
		fileIn (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Rigging_tools\\facial\\regenArcGuis.ms")
	)
	
)

CreateDialog regenArcGuiWindow