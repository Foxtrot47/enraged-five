left = #(
	$arcGUI_FB_L_Nasolabial_Mid, 
	$arcGUI_FB_L_Lid_Upper_Mid, 
	$arcGUI_FB_L_Eye, 
	$arcGUI_FB_L_CheekBone, 
	$arcGUI_FB_L_Lip_Corner, 
	$arcGUI_FB_L_Brow_Out, 
	$arcGUI_FB_L_Brow_Centre
	)
	
right = #(
	$arcGUI_FB_R_Nasolabial_Mid, 
	$arcGUI_FB_R_Lid_Upper_Mid, 
	$arcGUI_FB_R_Eye, 
	$arcGUI_FB_R_CheekBone, 
	$arcGUI_FB_R_Lip_Corner, 
	$arcGUI_FB_R_Brow_Out, 
	$arcGUI_FB_R_Brow_Centre
	)	
	
for i = 1 to left.count do
(
	right[i].transform = left[i].transform
)	

$Point01.controller.scale = [1,1,1]

for i in left do
(
	i.parent = $arcGUI_FB_Skull
)

for i = 1 to left.count do
(
	right[i].modifiers[#Attribute_Holder].Custom_Attributes.radius = left[i].modifiers[#Attribute_Holder].Custom_Attributes.radius 
)