-- Character Stripper and FBX creator.
-- Rockstar North
-- Dermot Bailie
-- Updated April 2009 by Rick Stirling

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())
	
-- Load the common functions
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_common.ms"

-- Store the name of the root dummy in the scene
Global CharDummy = 0

hasFemaleFeet =0


------------------------------------------------------------------------------------------------------------------------------------
-- Convert RAGE materials to standard.
Fn materialfix = (

	for matsrc = 1 to 24 do 
	(
		--Are we using a RAGE material in this slot?
		mnamesh = substring (meditMaterials[matsrc].name ) 1 6 
			
		if (mnamesh == "Rage00") then ( 
			
			for subid = 1 to (meditMaterials[matsrc].numsubs) do
			(
				-- try and get the textures (diffuse only)
				try (
					dtex = RstGetVariable meditMaterials[matsrc].materialList[subid] 1
					meditMaterials[matsrc].material[subid] = Standardmaterial () 			
					meditMaterials[matsrc].material[subid].diffuseMap = Bitmaptexture fileName:dtex
				) catch()

			) -- close the SUB IDtexture change loop
											
		) -- close the loop for looking for rage shaders
				
		
		MMClean.fixall prompt:false			-- Clean out unused slots
		actionMan.executeAction 0 "40807"  -- Views: Activate All Maps
	
	)-- close the loop for everything materials!!!
) -- close materialfix



-- fix skeleton parenting for Motionbuilder
fn fixparenting = (
	-- Remove Biped animation	
			
	MYBIP = $char.controller
	mybip.figureMode = false
	biped.clearAllAnimation MYBIP
	mybip.figureMode = true
 		
	-- CS_Char spine and clavs correct parenting
	clearSelection()
	select $'Char Spine'
	$.parent = $'Char Pelvis'
	clearSelection()
	select $'Char L Clavicle'
	$.parent = $'Char Spine3'
	select $'Char R Clavicle'
	$.parent = $'Char Spine3'
	
	if (hasFemaleFeet ==1) then 
	(
		--try ($'Char L Toe'.name = "Char L Toe0") catch()
		--try ($'Char R Toe'.name = "Char R Toe0") catch()
	)
					
)






-- Strip out all the crap
fn strip_character_scene saveyn = (

	-- This selects and deletes unneeded parts for FBX setup, Char dummy has been found
	-- found character dummy selecting Geo, skeleton and etm nulls
			
	unhide objects
			
	select CharDummy -- Root of all geo
	selectmore CharDummy.children -- the geo
	Selectmore  $Char...* -- the rig
	selectmore $helpers/e* --the ETMS
					
	deselect $co* -- ditch the collision
					
				
	-- Set geo to keep in array and deleting unwanted geo
	Keepthese = $ as array
	select objects
	
	deselect Keepthese
	
	try (deselect $Char_Footsteps) catch
	delete $
	
	fixparenting() 
	
	materialfix()	
	
	clearSelection()
	
	-- save the file?
	savename = "stripped_" + CharDummy.name as string
	if saveyn ==1 then saveMaxFile (maxFilePath + savename)
	
	
	if (hasFemaleFeet ==1) then 
	(
		--try ($'Char L Toe0'.name = "Char L Toe") catch()
		--try ($'Char R Toe0'.name = "Char R Toe") catch()
	)
	
)






fn save_character_fbx theProjectPath = (
	
	-- ditch collision stuff
	try (delete $co*)catch()
	fixparenting() 

	CharDummy.name = lowercase CharDummy.name
	
	
	-- Max2009 does strange things with the FBX root

	-- creating an fbx_root for export
	Dummy pos:[0,0,0] isSelected:on size:0.05
	$.name = "FBX_Root"

	-- parenting the char to the fbx_root
	Select $Char 
	MYBIP = $char.controller
	mybip.figureMode = false
	mybip.inPlaceMode = false
	mybip.figureMode = true
	select chardummy
	selectmore $Char
	$.parent = $FBX_Root

	
	-- Export to FBX
	clearSelection()
		
	-- Select and export
	exportname = Chardummy.name
	theclasses = exporterPlugin.classes
	FBXclass = findItem theclasses FBXEXP
	select $*
	exportFile (theProjectPath + "\\" + exportname) #noPrompt selectedOnly:true using: theclasses [FBXclass]
		
	clearSelection()			
	
	--revert the chars state
	delete $FBX_Root
	
	if (hasFemaleFeet ==1) then 
	(
		try ($'Char L Toe0'.name = "Char L Toe") catch()
		try ($'Char R Toe0'.name = "Char R Toe") catch()
	)

)																	

-----------------------------------------------------------------------------------------------------------------------------------

-- create rollout panel

rollout Character_Stripper "Cutscene Character Stripper" width:200 height:250
(
	DropDownList Current_Project "Select project" pos:[8,5] width:120 height:40 items:#("GTAIV", "E1", "E2", "Agent","Auto")selection:5
	
	checkbox strip_char "Strip Character" pos:[10,90] width:180 height:20 checked:true
	checkbox saveMax "Save as Stripped Max File" pos:[10,120] width:180 height:20 checked:false
	checkbox exp_FBX "Export FBX file" pos:[10,150] width:180 height:20 checked:true
	
	button run_stripper "Convert Character" pos:[20,180] width:150 height:32 enabled:true toolTip:"Starts the strip show"
	
	
	-- Initialise the tool	
	on Character_Stripper open  do
	(
		
		-- females had differently named toes
		-- rename for the purposes of the script
		try 
		(
			select $'Char L Toe'
			hasFemaleFeet =1
		)
		
		catch (hasFemaleFeet =0)
		
		
		-- If an object is selected, assume that is it the character dummy. If not, attempt to select it
		ds = $
		if ds != undefined then (Global CharDummy = $)
		
		else
		(
			-- finding the main character dummy 
			-- Should be the parent of the head object.
			thehead = getnodebyname "Head_000_R"
			select thehead.parent
			Global CharDummy = $
		)	
	)-- close on character_stripper open do
	
	
	
	-- The work part
	on run_stripper pressed  do 	
	(	

		-- Setup the correct path
		if Current_Project.selection == 1 then ProjectPath = "X:\\gta\\gta_art\\anim\\cutscenes\\Models"
		if Current_Project.selection == 2 then ProjectPath = "X:\\gta_e1\\gta_e1_art\\cutscenes\\Characters"
		if Current_Project.selection == 3 then  ProjectPath = "X:\\gta_e2\\gta_e2_art\\Cutscenes\\Characters"
		if Current_Project.selection == 4 then  messagebox ("Agent path not setup yet")   --Jimmy project path not avialable yet ProjectPath = "X:\\gta\\gta_art\\anim\\cutscenes\\Models\\"
		if Current_Project.selection == 5 then ProjectPath = maxFilePath
				
		print Projectpath
		
		-- Perform the file stripping.
		if strip_char.state == true then  strip_character_scene saveMax.state

		-- Export an FBX file									
		if exp_fbx.state == true then save_character_fbx ProjectPath

	) -- End of main loop
	
	
) -- End of rollout
---------------------------------------------------------------------------------------------------------






createDialog Character_Stripper

