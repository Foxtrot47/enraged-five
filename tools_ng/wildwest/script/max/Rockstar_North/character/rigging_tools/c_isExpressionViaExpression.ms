filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

global badControls = #()




fn readExpression controller = --controller needs to be passed in as a string
(
	thisScalarName=#()
	scalarTargets=#()
	
	theExprStr = (controller+".getExpression()")
-- 		print ("theExprStr = "+theExprStr)
	theexpr = execute theExprStr
	
	theScalarStr = (controller+".NumScalars()")
	theScalarCount = execute theScalarStr
		
	if theScalarCount != 4 do --need to remember there are always 4 for ticks, frames, secs and normalised-time
	(
		for i = 5 to theScalarCount do
		(
			thisScalarNameStr = (controller+".GetScalarName "+(i as string))
			thisScalar = execute thisScalarNameStr
			appendIfUnique thisScalarName thisScalar
		)
	)
	
	if thisScalarName.count > 0 do
	(
	
-- 		print ("thisScalarName = "+"\r\n"+(thisScalarName as string))
		
		for i = 1 to thisScalarName.count do
		(
			--now check if its a valid target
			validScalarTargetStr = (controller+".GetScalarType "+"\""+thisScalarName[i]+"\"")
			validScalar = execute validScalarTargetStr 
-- 			print ("validScalar = "+validScalar )
			
			if (validScalar as string) == "scalarTarget" then
			(
				print ("WOOO "+controller+" has a valid scalar")
				thisScalarTgtStr = (controller+".GetScalarTarget "+"\""+thisScalarName[i]+"\""+" asController:true")

					thisScalarTgt = execute thisScalarTgtStr
				
					thisScalarTgtCont = exprformaxobject thisScalarTgt
				
				--now check if its a valid float expression
				scalarClassStr = ("getclassname "+(thisScalarTgtCont as string))
					scalarClass = execute scalarClassStr
-- 				print ("scalarClass = "+(scalarClass as string))
				if (scalarClass as string) == "Float Expression" then 
				(
					print ("!!! SHITE! "+(thisScalarTgtCont as string)+" THIS HAS AN EXPRESSION POINTING TO AN EXPRESSION")
					appendifUnique badControls (thisScalarTgtCont as string)
					print ("Appended "+(thisScalarTgtCont as string)+" to badControl. Array count = "+(badcontrols.count as string))
				)
				else
				(
					print ("----"+scalarClass+" is not a Float Expression so should be good")
				)
				
			)
			else
			(
-- 				print ("----"+controller+" has an inValid Scalar. I.E. the scalar value has not had a control track assigned. ")
			)
		)
	)
)

fn testCont obj = 
(
	transArray = #(
		"Position",
		"Rotation",
		"Scale"
		)
		
	for tType = 1 to transArray.count do
	(		
		posOrRot = undefined
		if tType == 1 then --have to do this due to rotation controls sometimes being called "Euler"
		(
			posOrRot = "Position"
		)
		else
		(
			posOrRot = "Euler"
		)
		
-- 		print "----------------------------------------------------------------------------"
		posContStr = ("$'"+obj.name+"'."+transArray[tType]+".controller")
		posCont = execute posContStr

		if transArray[ttype] == "Scale" then
		(
			obj.scale.controller = bezier_scale ()
		)	
		else
		(
	-- 		print ("Testing "+transArray[tType]+"'s...")
	-- 		print ("*** TESTING "+(posCont as string)+" against "+("Controller:"+transArray[tType]+"_List"))
			
			--Look inside Position / Rotation Lists
			if (posCont as string) == ("Controller:"+transArray[tType]+"_List") do
			(
				contCount = posCont.count
				
				for cc = 1 to contCount do
				(
					thisCntStr = ("$'"+obj.name+"'."+transArray[tType]+".controller["+(cc as string)+"]")
					thisCnt = execute thisCntStr
					if (thisCnt as string) != ("SubAnim:Frozen_"+transArray[tType])  do 
					(
						contArray = #()
						kXStr = ("$'"+obj.name+"'."+transArray[tType]+".controller["+(cc as string)+"].controller.X_"+transArray[tType]+".controller")
		-- 				print KXStr
							kx = execute KXStr
							appendIfUnique contArray kX
						kYStr = ("$'"+obj.name+"'."+transArray[tType]+".controller["+(cc as string)+"].controller.Y_"+transArray[tType]+".controller")
							kY = execute KYStr
							appendIfUnique contArray kY
						kZStr = ("$'"+obj.name+"'."+transArray[tType]+".controller["+(cc as string)+"].controller.Z_"+transArray[tType]+".controller")
							kZ = execute KZStr
							appendIfUnique contArray kY
						
						for myCont = 1 to contArray.count do
						(
	-- 	 					print ("contArray["+(myCont as string)+"] = "+(contArray[myCont] as string))
							if (contArray[myCont] as string) == "Controller:Float_Expression" do
							(
	-- 							print ("Found a "+transArray[tType]+" expression on "+(contArray[myCont] as string)+" within a Float List")
							--fire off to readexpr
	-- 							print "Passing to read expressions (1)..."
								controller = ((exprformaxobject contArray[myCont]) as string)
								readExpression controller 
							)
						)
					)
				)
			)				
			
	-- 		print ("*** TESTING "+(posCont as string)+" against "+("Controller:"+posOrRot+"_XYZ") )
			
			if posCont as string == ("Controller:"+posOrRot+"_XYZ") do --look in a position_xyz or euler_xyz
			(
	-- 			print ("Looking inside a "+posOrRot+"_XYZ")
				axisArray = #("X","Y","Z")
				
				for ar = 1 to axisArray.count do
				(
					AxisPosContStr = ("$'"+obj.name+"'."+transArray[tType]+".controller."+axisArray[ar]+"_"+transArray[tType]+".controller")
						AxisPosCont = execute AxisPosContStr
									
					if (AxisPosCont as string) == "Controller:Float_List" then --see if there is a list inside the pos/euler xyz controller
					(
	-- 					print "Looking in a float list"
						contCount = axisPosCont.count
				
						for cc = 1 to contCount do
						(
							kXStr = ("$'"+obj.name+"'."+transArray[tType]+".controller."+axisArray[ar]+"_"+transArray[tType]+".controller["+(cc as string)+"]")
								kx = execute KXStr
							if (getclassname kx as string) == "Float Expression" do
							(
	-- 							print ("***Found a "+transArray[tType]+" expression on a float list on "+(kx as string)+" within a position xyz")
								--fire off to readexpr
	-- 							print "Passing to read expressions (2)"
	-- 							controller = ((exprformaxobject kx) as string)
								controller = kXStr
								readExpression controller 
							)
						)		
					)
					else --look for a float in the standard xyz axes
					(
	-- 					print ("Looking in a pos xyz for axis "+axisArray[ar])
						kXStr = ("$'"+obj.name+"'."+transArray[tType]+".controller."+axisArray[ar]+"_"+transArray[tType]+".controller")
						
						kx = execute KXStr
						
							if (kx as string) == "Controller:Float_Expression" do
							(
	-- 							print ("Found a pos expression on "+(kx as string)+" within a "+transArray[tType]+" xyz")
								--fire off to readexpr
	-- 							print "Passing to read expressions (3)"
								controller = ((exprformaxobject kx) as string)
								readExpression controller 
							)
					)
				)
			)
	-- 		print "----------------------------------------------------------------------------"
		)
	)
)		



fn runCheckExp = 
(
-- 	badControls = #()
	selA = selection as array
-- 	clearListener()
	for mySel = 1 to selA.count do
	(
		testCont selA[mySel]
	)
-- 	clearListener()
	print "-------------------------------------------------------------------"
	print (badControls as string)
	print "-------------------------------------------------------------------"
)

runCheckExp()