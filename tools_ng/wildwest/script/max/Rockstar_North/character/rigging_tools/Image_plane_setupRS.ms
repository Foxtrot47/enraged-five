filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())



filein "X:\\tools_release\\Scripts\\Character\\common_functions.ms"
ambientColor = color 255 255 255
Image_Source_Folder

-- x:\gta_e2\gta_e2_art\peds\Actor_Data\Cast Shoot\


fn createplane thename mat theimage pr=
(
		-- use image to figure out dimensions
		Mybitmapfront = openBitMap theimage
		pl =Mybitmapfront.height
		pw =Mybitmapfront.width
	
		Plane length:pl width:pw transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0,0,0]) isSelected:on
		rotate $ pr
		$.lengthsegs = $.widthsegs = 1
		selection[1].name =thename 
		meditMaterials[mat].diffuseMapEnable = on
		meditMaterials[mat].diffuseMap = Bitmaptexture fileName:(theimage)
		meditMaterials[mat].name= thename
		$.material = meditMaterials[mat]
		$.pos.z = pl/2
		$.pivot = [0,0,0]
		
		ambientColor = color 255 255 255
)


rollout ImagePlane_creator "ImagePlane_creator" width:400 height:150
(
	
	dropDownList imagefiles "Image files" pos:[10,10] width:250 height:40 items:#() selection:1 
	Button populate "Populate" pos:[300,20] width:72 height:32 enabled:true
	--label fchoice "Choose the -F file from the above list" pos:[50, 70]
	Button setupselected "Setup selected" pos:[75,100] width:100 height:32 enabled:true
	Button setupall "Setup all" pos:[225,100] width:100 height:32 enabled:true
	
	
	-- Build image list
	on populate pressed do 
	 (
		-- get list of Image files
		f = getOpenFileName caption:"Browse for Image file:" filename:"x:/image.jpg"
		if (f == undefined) then return 0
		Image_Source_Folder = getfilenamepath f
		fullimagelist=#()
		shortimagelist=#()
        imagefileslist = Image_Source_Folder + "*.jpg"
		thefiles = getfiles imagefileslist

        for f in thefiles do         
        (
			append fullImagelist (uppercase(f))
			append shortimagelist (uppercase(filenameFromPath f))  										
        )

		--populate the file list
		imagefiles.items = shortimagelist 
		imagelistcount = shortimagelist.count
	)
	
	
	-- Setup selected plane
	on setupselected pressed do
	(
		
		resetMaxFile #noPrompt
		commonname = ""
		
		-- find the front file
		for imf in imagefiles.items do
		(
			i = (filterstring imf ".")[1]
			findf = substring i ((i.count)-1) 2
			if (findf == "-F") then commonname = substring i 1 ((i.count)-2)
		)
		

		-- setting paths for image files
		selectedtexturefront = Image_Source_Folder + commonname + "-f.jpg"
		selectedtextureside = Image_Source_Folder + commonname + "-s.jpg"

		-- creating planes from bitmap res and assigning texture
		createplane "Image_Frontplane" 5 selectedtexturefront (angleaxis 0 [0,0,1])
		createplane "Image_Sideplane" 6 selectedtextureside  (angleaxis 90 [0,0,1])
		
		meditMaterials[5].selfIllumAmount = 100
		meditMaterials[6].selfIllumAmount = 100


		-- create dummy and parnet image_planes 
		Dummy pos:[0,0,0] isSelected:on
		$.name = "Image_scale"
		Select Geometry
		$.parent = $Image_scale

		actionMan.executeAction 0 "63508"

		-- save file 
		saveMaxFile (Image_Source_Folder + commonname + ".max")
		
	)
)



createDialog ImagePlane_creator
