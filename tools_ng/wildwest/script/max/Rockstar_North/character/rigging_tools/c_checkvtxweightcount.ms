--script to test if a mesh has more than 4 weights per vertex actually assigned

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())


fn testVertexWeightCount =
(
	skinMod = $.modifiers[#Skin]
	vtxCount = skinops.getNumberVertices skinMod
	vtxWeights = 4
	vtxOverTheLimit = #()
	
	for v = 1 to vtxCount do
	(
		inf = skinOps.getVertexWeightCount skinMod v
		if inf > vtxWeights do
		(
			vtxWeights = inf
			appendIfUnique vtxOverTheLimit v
		)
	)
	
	if vtxWeight == 4 then --good
	(
		messagebox ("Vertex weighting set to 4 weights.") beep:true
	)
	else --bad!
	(	
		numberOverLimit = vtxOverTheLimit.count
		print "=============================================================================================="	
		print ("WARNING! Highest Number of influences = "+(vtxWeights as string)+"."+"\r\n"+"There are a total of "+(numberOverLimit as string)+" verts over.") 		
		print ("Vertices over the Limit are:")
		print vtxOverTheLimit
		print "=============================================================================================="
		messageBox ("WARNING! Highest Number of influences = "+(vtxWeights as string)+"."+"\r\n"+"There are a total of "+(numberOverLimit as string)+" verts over.") beep:true
	)	
)

clearListener()
testVertexWeightCount()