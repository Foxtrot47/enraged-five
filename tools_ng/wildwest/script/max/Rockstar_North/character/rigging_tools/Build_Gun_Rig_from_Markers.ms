-- This script builds a custom bone rig from a set of helpers
-- Rockstar North
-- Rick Stirling April 2009

--  Updated October 2009: Builds bones based on feedback from props team
-- Switches off bone mode so that the bones can translate
-- Use XML Lookup

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())


-- Load the common maxscript functions 
zaxis = [0,-1,0]
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_common.ms"
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_rigging.ms"
filein "X:/tools/dcc/current/max2009/scripts/pipeline/util/xml.ms"

-- Merge files
weapon_markers = "X:\\tools\\dcc\\current\\max2009\\scripts\\character\\Assets\\Weapon_Markers.max" 
weapon_grip_mesh = "X:\\gta5\\art\\peds\\skeletons\\Weapon_Pistol_Grip_Hand.max"

-- ***************************************************************************
-- Arrays are used to hold the markers.
-- This will become XML at some point.
ABoneName = #()
ABoneTag=#()
bonelist =#()


-- *****************************************************************
-- *****************************************************************
-- Set up some variables for the bones
-- *****************************************************************
-- Colours, just some useful defaults

colourRoot = (color 255 128 0) 
colourBone = (color 255 35 35) 
colourMarker = (color 5 5 135) 


-- useful default widths
defaultbonewidth = 0.05
defaultfinsize = 0.002
thinbone = 0.005
nofin = 0
smalldummy = 0.02
defaultTwist = 90





-- *****************************************************************
-- *****************************************************************
-- it's functions time. Sexy sexy function time.
-- *****************************************************************



-- Function to create a bone, parent it, and make it a bone/non-bone
fn createWeaponBone bonename bonestart boneend bonetag currentparent bonemode= (
	
	-- Create the main gun bones.
	buildNewBoneRA bonename bonetag bonestart boneend 0.01 0.01 colourBone currentparent defaultTwist -1
	$.boneEnable=bonemode
	
	transformtag = "tag = " + (bonetag as string) +"\r\n"+ "exportTrans = true"
	setUserPropbuffer $ transformtag
	
)	





-- Align the root bone to the hand helper
fn realignroot = (
	try (
		-- Does the alignment helper exist?
		select $PH_R_Hand
	)
	
	catch (
		messagebox "Could not find PH_R_Hand helper for alignment"
	)
	
	try (
		-- Does the root buffer bone exist?
		thegrip = getnodebyname ABoneName[2]
		select thegrip
		
		thegrip.parent = undefined
		thegrip.transform = $PH_R_Hand.transform
		thegrip.parent = $Gun_Root
	)
	
	catch (
		messagebox "Could not find the Gun_Root/Gun_Grip to align"
	)
)



fn buildWeaponRigFM =(

	-- Can we auto align the root?
	meshroot = undefined
	rootpos = $Marker_Root.pos
	
	if $ == undefined then (
		messagebox "No Weapon selected, you will need to manually align the Gun_Root bone"
	)
	
	else (
		meshroot = $
		rootpos = $.pos
	)
	
	
	-- Delete existing rig
	try (
		delete $Gun_*
		delete $Dummy01*
	) catch ()

	
	bonelist =#()
	currentparent=undefined
	
	
	-- Create the root bone
	-- This is the container for the rig
	bonename = ABoneName[1]
	btag = ABoneTag[1]
	thebone = buildPointHelper bonename rootpos 0.04 colourRoot
	setUserPropBuffer (getnodebyname bonename)("tag = " + btag)
	
	-- auto align?
	if meshroot != undefined then thebone.transform = meshroot.transform
		
	
	-- This is the new parent bone for the main bone
	currentparent = (getnodebyname bonename)
	append bonelist thebone


	-- Create the grip bone
	-- This is the container for the rig
	bonename = ABoneName[2]
	btag = ABoneTag[2]
	thebone = buildPointHelper bonename $Marker_Root.pos 0.02 colourRoot
	setUserPropBuffer (getnodebyname bonename)("tag = " + btag)
	
	-- Try to automatically align the root buffer bone to the hand.
	realignRoot()	
	
	thebone.parent = currentparent
	
	-- This is the new parent bone for the main bone
	currentparent = thebone
	append bonelist thebone
	
	

	
	
	
	-- Start at position 3 to skip the root bones
	for wb = 3 to ABoneName.count do (
 		
	-- Create the main object bone
		bonename = ABoneName[wb]
		bonetag = ABoneTag[wb] as string
		bonestart = (getnodebyname ("Marker_" + (substring ABoneName[wb] 5 255) + "_S")).pos
		boneend = (getnodebyname ("Marker_" + (substring ABoneName[wb] 5 255) + "_E")).pos
		createWeaponBone bonename bonestart boneend bonetag currentparent true	
		(getnodebyname bonename).parent = currentparent

		append bonelist $

		-- If this is the main bone	
		-- This is the new parent bone for the entire rig
		if bonename == "Gun_Main"  then 	currentparent = (getnodebyname bonename)
	)
	
	
	-- Create and place the rootdummy
	thedummy = buildDummyHelper "Dummy01" [0,0,0] 0.01
	$Dummy01.transform = $Gun_Root.transform
	$Gun_Root.parent = $Dummy01

)





-- Build a single bone from the selected marker(s)
fn buildweaponBoneFM = (
	
	-- Is anything selected, and if so, is it a Point help??
	if ($ == undefined or (classof $)!=Point)  then (
		messagebox "You need to select a weapon bone marker"
	)	
	
	
	else (
		markername=$.name
		checkMarker = lowercase (substring markername 1 6)
		
		if (checkMarker == "marker") then (
			-- Get marker name
			mlength = markername.count
			bonetype= (lowercase (substring markername 8 ((mlength - 2) - 7)))
			bonename= "gun_" + bonetype
			
			-- Find the bone in the array
			arraypos = 0	
			for p = 1 to ABoneName.count do
			(
				if (lowercase ABoneName[p]) == bonename then arraypos = p
			)	
				
			boneName = ABoneName[arraypos]
			boneTag = ABoneTag[arraypos] as string
			
			bonestart = (getnodebyname ("Marker_" + bonetype + "_S")).pos
			boneend = (getnodebyname ("Marker_" + bonetype + "_E")).pos
				
			if (bonename == "Gun_Main")  then currentparent =$'Gun_Root' else currentparent =$'Gun_Main'	
				
			createWeaponBone bonename bonestart boneend bonetag currentparent true	
			(getnodebyname bonename).parent = currentparent	
			

			-- Add it to the bonelist, remove duplicates
			append bonelist $
			makeUniqueArray bonelist
		)
		
		else (messagebox "Does not appear to be a weapon marker")
	)
	
	

)






fn addSkinToWeapon  =  (

	if $ != undefined then 
	(
		modPanel.addModToSelection (Skin ()) ui:on
		$.modifiers[#Skin].bone_Limit = 4
		$.modifiers[#Skin].showNoEnvelopes = on
				
		for wbone = 1 to bonelist.count do
		(
			print wbone
			try (skinOps.addBone $.modifiers[#Skin] bonelist[wbone] 1) catch ()
		)		
	)
)	




-- Figure out what the weapon bones are
-- This uses xml
fn populateMappingArrays = (
	
	
	-- Empty arrays
	ABoneName = #()
	ABoneTag=#()
		
	
	-- Define the XML file
	mappingXmlFile = RsConfigGetContentDir() + "animconfig/bone_tags.xml" 
	xmlDoc = XmlDocument()	
	xmlDoc.init()
	xmlDoc.load mappingXmlFile
	
	xmlRoot = xmlDoc.document.DocumentElement
	
	
	-- Parse the XML
	if xmlRoot != undefined then (
		dataElems = xmlRoot.childnodes
	
		for i = 0 to (dataElems.Count - 1 ) do (
			dataElement = dataElems.itemof(i)
			
			-- Look for ped bones
			if dataelement.name == "weapons" then (
				-- Look for the bones ids
				boneElems = dataelement.childnodes
				
				for b = 0 to (boneElems.Count - 1 ) do (
					boneElement = boneElems.itemof(b)
					
					-- If we've found the bone ids, populate the struct
					if boneElement.name == "boneids" then (
						
						boneTagElems = boneElement.childnodes
						
						for bt = 0 to (BoneTagElems.Count - 1 ) do (
						
							boneTagElement = boneTagElems.itemof(bt)
							
							nameattr = boneTagElement.Attributes.ItemOf("name")
							tagattr = boneTagElement.Attributes.ItemOf("id")
							
							append ABoneName nameattr.value
							append ABoneTag tagattr.value	
							
						) -- end found bone definition
					) -- end found bone 
					
				)
				
			) -- end found a weapon
		)		
	) -- end xml parse
	
	
)






-- Takes a true/false setting for boxmode 
-- Should become a common function perhaps?
fn BoxModeToggle boxState = (
	try (
		select $Gun_*
		$.boxmode = boxState
	) catch ()
)	




-- If the bones are in the skin modifier, keep them
-- If not, delete them
fn DeleteUnusedBones =(

	-- Is anything selected?
	if ($ == undefined or ($.modifiers[#Skin]==undefined))  then (
		messagebox  "You need to select a skinned weapon"
	)
	
	
	else (
		-- Get all bones 
		skweapon=$
		theSkin = skweapon.modifiers[#skin]
		
		select $Gun_*
		allGunBones = #()
		for gb in selection do append allGunbones gb.name
		
		-- Get all bones in skin modifier
		select skweapon
		howmanybones = skinOps.GetNumberBones theSkin 
		
		skinbonelist=#()
		for i = 1 to howmanybones do
		(
			thebonename = skinops.getbonename theSkin  i 1
			append skinbonelist thebonename
		)
		
		unusedBones = differenceInArray allGunBones  skinbonelist 
		
		for ub = 1 to unusedBones.count do (
			try (
				deleteme = getnodebyname unusedBones[ub]
				delete deleteme
			)
			catch()
		)
	)

)





rollout markerTools "Marker Tools" width:220 height:300
(
	button btnMergeMarkers "Merge Markers" width:180 height:30
	button btnMergeGripMesh "Merge Grip Mesh" width:180 height:30
	button btnBuildRig "Build Rig from Markers" width:180 height:30
	button btnbuildSelectedBone "Build Selected Bone" width:180 height:30
	button btnbuildRealignRoot "Re-align Root" width:180 height:30
	button btnBoxModeToggleOn "Box Mode On" width:85 height:30 across:2
	button btnBoxModeToggleOff "Box Mode Off" width:85 height:30 

	button btnSkinmainbody "Add Skin" width:180 height:30
	button btnDeleteUnusedBones "Delete Unused Bones" width:180 height:30

	
	on markertools open do populateMappingArrays()
	
	on btnMergeGripMesh pressed do mergemaxfile weapon_grip_mesh #select
	
	on btnMergeMarkers pressed do mergemaxfile weapon_markers #select

	on btnBuildRig pressed do buildWeaponRigFM()
	on btnBuildSelectedBone pressed do buildWeaponBoneFM()
	
	on btnbuildRealignRoot pressed do realignroot()
	
	on btnBoxModeToggleOn pressed do BoxModeToggle true
	on btnBoxModeToggleOff pressed do BoxModeToggle false
	
	
	on btnSkinMainBody pressed do addSkinToWeapon()

	on btnDeleteUnusedBones pressed do DeleteUnusedBones()
	
)







-- Add the rollout section

createDialog markertools
