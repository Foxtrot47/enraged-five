-- IM_Face_Disconecctor
-- Rockstar North
-- Tool to disconnect pup_allBlends from constraintInfo


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

-- Matt Rennie Sep 2010
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

fn im_faceDisconnector = 
(
	if selection.count != 0 then
	(
		selectedArray = selection as array
		
		for i = 1 to selectedArray.count do
		(
			currObj = selectedArray[i]
			
			if currObj.modifiers[#Attribute_Holder] != undefined then
			(
				im_Attr = custAttributes.get currObj.modifiers[#Attribute_Holder] 1

				im_Array = getPropNames im_Attr

				rolloutStr = (filterstring (im_Attr as string) ":")
				rolloutStr = rolloutStr[2]

				for i = 1 to im_array.count do
				(
					currentAttr = (im_Array[i] as string)
				-- 	print currentAttr
					executeString = ("$"+currObj.name+".modifiers[#Attribute_Holder]."+rolloutStr+"."+currentAttr+".controller = Bezier_Float()" )
						execute executeString
				)
			)
			else
			(
				messageBox (currObj.name+" has no Attribute Holder Modifier...") beep:true
			)
		)
	)
	else
	(
		messagebox "Please make sure you have something selected!" beep:true
	)
)