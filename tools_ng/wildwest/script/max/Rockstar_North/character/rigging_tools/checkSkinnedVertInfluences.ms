--tool to check if a mesh has more than 4 weights per vertex

fn checkSkinnedVerts skinnedObject =
(
	sela = selection as array
	for a = 1 to sela.count do
	(
		skinnedObject = sela[a]
		if skinnedObject.modifiers[#Skin] != undefined do
		(
			select skinnedObject
			numberSkinVerts = skinOps.GetNumberVertices skinnedObject.modifiers[#Skin]

			allowedVertexInfluences = 4
			
			maxVerts = 4
			
			for i = 1 to numberSkinVerts do
			(
				inf = skinOps.GetVertexWeightCount skinnedObject.modifiers[#Skin] i
				if inf > allowedVertexInfluences do
				(
					maxVerts = inf
					print ("Vertex #"+(i as string)+" has more than "+(allowedVertexInfluences as string)+" influences.")
				)
			)
			
			if maxVerts > allowedVertexInfluences then
			(
				messagebox (skinnedObject.name+" has more than "+(allowedVertexInfluences as string)+" influences per vert.")
				print "------"
				print (skinnedObject.name+" has more than "+(allowedVertexInfluences as string)+" influences per vert.")
			)
			else
			(
				messagebox(skinnedObject.name+" has "+(maxVerts as string)+" influences per vert.")
				print "------"
				print (skinnedObject.name+" has "+(maxVerts as string)+" influences per vert.")
			)
		)
	)
)


clearListener()

checkSkinnedVerts $
