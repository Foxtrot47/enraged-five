filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

faceBones = selection as array

fn cleanControllers =
(
	for fb = 1 to faceBones.count do
	(
		posCont = (fb.position.controller as string)
		rotCont = (fb.rotation.controller as string)
		
		if posCont == "Controller:Position_List" do
		(
			maxControllersPos = faceBones[fb].position.controller.count 
			
			for c in maxControllersPos to 2 by -1 do
			(			
				deleteControllerX = 0
				deleteControllerY = 0
				deleteControllerZ = 0
				
				bV = "Controller:Bezier_Float" 
				
				nX = (faceBones[fb].position.controller[c].X_Position.controller as string)
				nY = (faceBones[fb].position.controller[c].Y_Position.controller as string)
				nZ = (faceBones[fb].position.controller[c].Z_Position.controller as string)
				
				if nX == bv then
				(
					deleteControllerX = 0
				)
				else
				(
					deleteControllerX = 1
				)
				
					if nY == bV then
					(
						deleteControllerY = 0
					)
					else
					(
						deleteControllerY = 1
					)
						
						if nZ == bV then
						(
							deleteControllerZ = 0
						)	
						else
						(
							deleteControllerZ = 1
						)

				deleteTotal = (deleteControllerX + deleteControllerY + deleteControllerZ)

					if deleteTotal <= -0.1 then
					(
						faceBones[fb].position.controller.delete c --delete the controller
					)
					else
					(
						--err
					)
			)
		)
		
		if rotCont == "Controller:Rotation_List" do
		(
			maxControllersRot = faceBones[fb].rotation.controller.count 
					for c in maxControllersRot to 2 by -1 do
			(			
				deleteControllerX = 0
				deleteControllerY = 0
				deleteControllerZ = 0
				
				bV = "Controller:Bezier_Float" 
				
				nX = (faceBones[fb].rotation.controller[c].X_rotation.controller as string)
				nY = (faceBones[fb].rotation.controller[c].Y_rotation.controller as string)
				nZ = (faceBones[fb].rotation.controller[c].Z_rotation.controller as string)
				
				if nX == bv then
				(
					deleteControllerX = 0
				)
				else
				(
					deleteControllerX = 1
				)
				
					if nY == bV then
					(
						deleteControllerY = 0
					)
					else
					(
						deleteControllerY = 1
					)
						
						if nZ == bV then
						(
							deleteControllerZ = 0
						)	
						else
						(
							deleteControllerZ = 1
						)

				deleteTotal = (deleteControllerX + deleteControllerY + deleteControllerZ)

					if deleteTotal <= -0.1 then
					(
						faceBones[fb].rotation.controller.delete c --delete the controller
					)
					else
					(
						--err
					)
			)
		)
	)
)

cleanControllers()