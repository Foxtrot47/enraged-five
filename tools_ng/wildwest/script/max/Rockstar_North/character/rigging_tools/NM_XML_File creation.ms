filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())



-- write out XMl data
colArray =#()



objR = $.transform.controller.rotation as matrix3
objP = $.transform.controller.position
objS = $.transform.controller.scale

-- Loop through the rotations, extract each element.
for i = 1 to 3 do (

	v = objR[i]
	
	for ai = 1 to 3 do (

		append colArray v[ai]

	)
	
	-- The last value is always 0 for rotation
	append colArray 0
)	
	

-- Now do the positions
v = objP
for ai = 1 to 3 do (

		append colArray v[ai]

	)
	
-- The last valuefor position is always 1
append colArray 1

	
-- now we need to format that array into somthing the xml file wants

xmlstring = ""
mstart = "<m0"	
mend = " </m0"

arrayoffset = 1	
for i = 0 to 3 do (	
	
	arraypos= i + arrayoffset
	xmlstring = xmlstring +	mstart + (i as string) + "> " + (colArray[arraypos] as string) + mend  + (i as string) + ">  "
	
)	


xmlstring = xmlstring + "\n"


mstart = "<m1"	
mend = " </m1"

arrayoffset = 5	
for i = 0 to 3 do (	
	
	arraypos= i + arrayoffset
	xmlstring = xmlstring +	mstart + (i as string) + "> " + (colArray[arraypos] as string) + mend  + (i as string) + ">  "
	
)	



xmlstring = xmlstring + "\n"

mstart = "<m2"	
mend = " </m2"

arrayoffset = 9	
for i = 0 to 3 do (	
	
	arraypos= i + arrayoffset
	xmlstring = xmlstring +	mstart + (i as string) + "> " + (colArray[arraypos] as string) + mend  + (i as string) + ">  "
	
)	


xmlstring = xmlstring + "\n"

mstart = "<m3"	
mend = " </m3"

arrayoffset = 13	
for i = 0 to 3 do (	
	
	arraypos= i + arrayoffset
	xmlstring = xmlstring +	mstart + (i as string) + "> " + (colArray[arraypos] as string) + mend  + (i as string) + ">  "
	
)	


clearlistener()

xmlstring 