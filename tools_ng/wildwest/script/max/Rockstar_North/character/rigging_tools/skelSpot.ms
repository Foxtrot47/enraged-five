-- skelSpotGUI.ms
-- a little floater gui to bring commonly used tools together in one place
--Matt Rennie
--Jan 2011

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()




filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if ((spotToolsGUI != undefined) and (spotToolsGUI.isDisplayed)) do
	(destroyDialog spotToolsGUI)

rollout spotToolsGUI "Spotting Tools"
(
	button btnImagePlanes "Image Planes" width:110
	button btnMergeStretchy "Merge StretchySkel" width:110
	button btnAlignToGiant "Auto Giant Spot" width:110
	button btnHandPrep "Hand Prep" width:110
	button btnMirrorSel "Mirror Selection" width:110
	button btnSpotGeo "SpotGeo" width:110	
	

	on btnImagePlanes pressed do
	(
		filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Rigging_tools\\giant\\skeletonSpotImagePlaneSetup.ms")	
	)

	on btnMirrorSel pressed do
	(
		filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Rigging_tools\\giant\\mirrorMarkers.ms")	
	)
	
	on btnMergeStretchy pressed do
	(
		mergeMaxFile "X:\\gta5\\art\\peds\\Skeletons\\giantConstrainedSkel_Male.max"
		
		mergeMaxFile "X:\\gta5\\art\\peds\\Story_Characters\\CS_Male_Proxy\\CS_Male_Hands.max"
	)
	
	on btnAlignToGiant pressed do
	(
-- 		filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Rigging_tools\\giant\\markerToGiantMatch.ms")			
		filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Rigging_tools\\giant\\autoGiantSkeletonSPot.ms")			
	)
	
	on btnHandPrep pressed do
	(
		filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Rigging_tools\\giant\\handPrep.ms")			
	)
	
	on btnSpotGeo pressed do
	(
		filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Rigging_tools\\giant\\spotGeoSetup.ms")	
	)
)

CreateDialog spotToolsGUI width:125 pos:[1450, 100] 