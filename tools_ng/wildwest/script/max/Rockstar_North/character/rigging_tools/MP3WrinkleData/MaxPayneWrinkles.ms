headWrinkleData =#(
	#("head-000",1,0,"brows_raise_CL",$con_browraise_cl.pos.controller.'Y Position'.controller,"100 * brows_raise_CL"),
	
	#("head-001",1,1,"brows_raise_L",$con_browraise_l.pos.controller.'Y Position'.controller,"100 * brows_raise_L"),
	
	#("head-002",1,2,"sneer_L",$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A.StretchDownSneerL.controller,"100 * sneer_L"),
	#("head-002",1,2,"eyes_Squeeze_L",$con_eyeball_L.modifiers[#Attribute_Holder].IM_ATTRS_A.Openshut.controller,"50 * eyes_Squeeze_L"),
	
	#("head-003",1,3,"eyes_Squeeze_L",$con_eyeball_L.modifiers[#Attribute_Holder].IM_ATTRS_A.Openshut.controller,"100 * eyes_Squeeze_L"),
	#("head-003",1,3,"L_cheek_up",$con_cheek_l.pos.controller.'X Position'.controller,"-50 * L_cheek_up"),
	
	#("head-004",1,4,"brows_raise_CR",$con_browraise_cr.pos.controller.'Y Position'.controller,"100 * brows_raise_CR"),
	
	#("head-005",1,5,"brows_raise_R",$con_browraise_r.pos.controller.'Y Position'.controller,"100 * brows_raise_R"),
	
	#("head-006",1,6,"sneer_R",$con_nose_c.modifiers[#Attribute_Holder].IM_ATTRS_A.StretchDownSneerR.controller,"100 * sneer_R"),
	#("head-006",1,6,"eyes_Squeeze_R",$con_eyeball_R.modifiers[#Attribute_Holder].IM_ATTRS_A.Openshut.controller,"50 * eyes_Squeeze_R"),
	
	#("head-007",1,7,"eyes_Squeeze_R",$con_eyeball_R.modifiers[#Attribute_Holder].IM_ATTRS_A.Openshut.controller,"100 * eyes_Squeeze_R"),
	#("head-007",1,7,"R_cheek_up",$con_cheek_r.pos.controller.'X Position'.controller,"-50 * R_cheek_up"),

	
	
	#("head-008",2,0,"brows_squeeze_L",$con_browRaise_cl.modifiers[#Attribute_Holder].IM_ATTRS_A.Squeeze.controller,"100 * brows_squeeze_L"),
	
	#("head-010",2,2,"smile_L",$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A.FrownSmileL.controller,"50 * smile_L"),
	#("head-010",2,2,"open_smile_L",$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A.OpenSmileL.controller,"20 * open_smile_L"),
	#("head-010",2,2,"eyes_squint_inner_L",$con_eyeball_l.modifiers[#Attribute_Holder].IM_ATTRS_A.EyeSquintInner.controller,"20 * eyes_squint_inner_L"),
	#("head-010",2,2,"brows_down_L",$con_browRaise_cl.pos.controller.'Y Position'.controller,"-50 * brows_down_L"),
	
	#("head-011",2,3,"smile_L",$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A.FrownSmileL.controller,"80 * smile_L"),
	#("head-011",2,3,"open_smile_L",$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A.OpenSmileL.controller,"20 * open_smile_L"),
	#("head-011",2,3,"eyes_squint_inner_L",$con_eyeball_l.modifiers[#Attribute_Holder].IM_ATTRS_A.EyeSquintInner.controller,"30 * eyes_squint_inner_L"),
	#("head-011",2,3,"cheeks_squint_inner_L",$con_eyeball_l.modifiers[#Attribute_Holder].IM_ATTRS_A.CheekSquintInner.controller,"70 * cheeks_squint_inner_L"),
	
	#("head-012",2,4,"brows_squeeze_R",$con_browRaise_cr.modifiers[#Attribute_Holder].IM_ATTRS_A.Squeeze.controller,"100 * brows_squeeze_R"),
	
	#("head-014",2,6,"smile_R",$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A.FrownSmileR.controller,"50 * smile_R"),
	#("head-014",2,6,"open_smile_R",$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A.OpenSmileR.controller,"20 * open_smile_R"),
	#("head-014",2,6,"eyes_squint_inner_R",$con_eyeball_r.modifiers[#Attribute_Holder].IM_ATTRS_A.EyeSquintInner.controller,"20 * eyes_squint_inner_R"),
	#("head-014",2,6,"brows_down_R",$con_browRaise_cr.pos.controller.'Y Position'.controller,"-50 * brows_down_R"),
	
	#("head-015",2,7,"smile_R",$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A.FrownSmileR.controller,"80 * smile_R"),
	#("head-015",2,7,"open_smile_R",$con_mouth_c.modifiers[#Attribute_Holder].IM_ATTRS_A.OpenSmileR.controller,"20 * open_smile_R"),
	#("head-015",2,7,"eyes_squint_inner_R",$con_eyeball_r.modifiers[#Attribute_Holder].IM_ATTRS_A.EyeSquintInner.controller,"30 * eyes_squint_inner_R"),
	#("head-015",2,7,"cheeks_squint_inner_R",$con_eyeball_r.modifiers[#Attribute_Holder].IM_ATTRS_A.CheekSquintInner.controller,"70 * cheeks_squint_inner_R")
)
