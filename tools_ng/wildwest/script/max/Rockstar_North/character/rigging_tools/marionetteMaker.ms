-- MarionetteMaker.ms
-- Rockstar North
-- Matt Rennie Nov 2010
--Tool to split character meshes and then hard skin for use with giant mocap
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

filein "rockstar/export/settings.ms" -- This is fast

-- Figure out the project
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_North\\character\\Includes\\FN_Rigging.ms")	


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

global initialObjectArray = #()
global volObjects = #()
global geoNo = undefined
global originalMeshToSplit = undefined
global meshToSplit = undefined
global marionetteGuiRoll = undefined
global skinOrNot = undefined
	
struct rigBoneStruct (rb_bone, rb_Radius, rb_Length, rb_Trans)

-- rigBoneList = #($SKEL_Spine_Root, $SKEL_Spine0, $SKEL_Spine1, $SKEL_Spine2, $SKEL_Spine3, $SKEL_L_Clavicle, $SKEL_L_UpperArm, $SKEL_L_Forearm, $SKEL_L_Hand, $SKEL_L_Finger00, $SKEL_L_Finger01, $SKEL_L_Finger02, $SKEL_L_Finger10, $SKEL_L_Finger11, $SKEL_L_Finger12, $SKEL_L_Finger20, $SKEL_L_Finger21, $SKEL_L_Finger22, $SKEL_L_Finger30, $SKEL_L_Finger31, $SKEL_L_Finger32, $SKEL_L_Finger40, $SKEL_L_Finger41, $SKEL_L_Finger42, $SKEL_R_Clavicle, $SKEL_R_UpperArm, $SKEL_R_Forearm, $SKEL_R_Hand, $SKEL_R_Finger00, $SKEL_R_Finger01, $SKEL_R_Finger02, $SKEL_R_Finger10, $SKEL_R_Finger11, $SKEL_R_Finger12, $SKEL_R_Finger20, $SKEL_R_Finger21, $SKEL_R_Finger22, $SKEL_R_Finger30, $SKEL_R_Finger31, $SKEL_R_Finger32, $SKEL_R_Finger40, $SKEL_R_Finger41, $SKEL_R_Finger42, $SKEL_Neck_1, $SKEL_Head, $SKEL_Pelvis, $SKEL_L_Thigh, $SKEL_L_Calf, $SKEL_L_Foot, $SKEL_L_Toe0, $SKEL_R_Thigh, $SKEL_R_Calf, $SKEL_R_Foot, $SKEL_R_Toe0)


rigBoneListStruct = #( --array of all skeleton bones which we want to split mesh for
	( rigBoneStruct rb_bone:$SKEL_Pelvis  rb_Radius: 0.25 rb_Length:($SKEL_Pelvis.length * 4) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_Spine0  rb_Radius: 0.25 rb_Length:($SKEL_Spine0.length * 1.4) rb_Trans:(matrix3 [-1,-0.000168886,1.60497e-005] [0.000169019,-0.999968,0.00799049] [1.47042e-005,0.00799066,0.999969] [1.22934e-007,0.0171712,0.972031]) ), 
	( rigBoneStruct rb_bone:$SKEL_Spine1  rb_Radius: 0.25 rb_Length:($SKEL_Spine1.length * 1.4) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_Spine2  rb_Radius: 0.22 rb_Length:($SKEL_Spine2.length * 1.4) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_Spine3  rb_Radius: 0.24 rb_Length:($SKEL_Spine3.length * 0.8) rb_Trans:undefined ), 
-- 	( rigBoneStruct rb_bone:$SKEL_Neck_1  rb_Radius: 0.09 rb_Length:$SKEL_Neck_1.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_Head  rb_Radius: 0.15 rb_Length:($SKEL_Head.length * 2) rb_Trans:(matrix3 [-1,0.000145016,-0.000328264] [-5.81659e-005,-0.96816,-0.250334] [-0.000354256,-0.250334,0.96816] [-0.000112729,0.0139014,1.59917]) ),	
	
	( rigBoneStruct rb_bone:$SKEL_L_Thigh  rb_Radius: 0.14 rb_Length:$SKEL_L_Thigh.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Calf  rb_Radius: 0.12 rb_Length:$SKEL_L_Calf.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Foot  rb_Radius: 0.1 rb_Length:($SKEL_L_Foot.length * 1.5) rb_Trans:(matrix3 [0.999234,0.0391548,3.82012e-005] [-0.00134613,0.0333797,0.999442] [0.0391316,-0.998676,0.0334069] [0.155156,0.145386,0.0342977]) ), 
	( rigBoneStruct rb_bone:$SKEL_L_Toe0  rb_Radius: 0.1 rb_Length:$SKEL_L_Toe0.length rb_Trans:undefined ),
	
	( rigBoneStruct rb_bone:$SKEL_R_Thigh  rb_Radius: 0.14 rb_Length:$SKEL_R_Thigh.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Calf  rb_Radius: 0.12 rb_Length:$SKEL_R_Calf.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Foot  rb_Radius: 0.1 rb_Length:($SKEL_R_Foot.length * 1.5) rb_Trans:(matrix3 [0.999233,0.0391547,3.80804e-005] [-0.00134612,0.0333794,0.999442] [0.0391315,-0.998676,0.0334067] [-0.155,0.145386,0.0342977]) ), 
	( rigBoneStruct rb_bone:$SKEL_R_Toe0  rb_Radius: 0.1 rb_Length:$SKEL_R_Toe0.length  rb_Trans:undefined ), 
	
-- 	( rigBoneStruct rb_bone:$SKEL_Spine_Root  rb_Radius: 1 rb_Length: .length  rb_Trans:undefined ), 

	( rigBoneStruct rb_bone:$SKEL_L_Clavicle  rb_Radius: 0.15 rb_Length:$SKEL_L_Clavicle.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_UpperArm  rb_Radius: 0.1 rb_Length:$SKEL_L_UpperArm.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Forearm  rb_Radius: 0.1 rb_Length:($SKEL_L_Forearm.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Hand  rb_Radius: 0.05 rb_Length:$SKEL_L_Hand.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger00  rb_Radius: 0.018 rb_Length:($SKEL_L_Finger00.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger01  rb_Radius: 0.018 rb_Length:($SKEL_L_Finger01.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger02  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger02.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger10  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger10.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger11  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger11.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger12  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger12.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger20  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger20.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger21  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger21.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger22  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger22.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger30  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger30.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger31  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger31.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger32  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger32.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger40  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger40.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger41  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger41.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_L_Finger42  rb_Radius: 0.013 rb_Length:($SKEL_L_Finger42.length * 1.1) rb_Trans:undefined ), 

	( rigBoneStruct rb_bone:$SKEL_R_Clavicle  rb_Radius: 0.15 rb_Length:$SKEL_R_Clavicle.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_UpperArm  rb_Radius: 0.1 rb_Length:$SKEL_R_UpperArm.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Forearm  rb_Radius: 0.1 rb_Length:($SKEL_R_Forearm.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Hand  rb_Radius: 0.05 rb_Length:$SKEL_R_Hand.length  rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger00  rb_Radius: 0.018 rb_Length:($SKEL_R_Finger00.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger01  rb_Radius: 0.018 rb_Length:($SKEL_R_Finger01.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger02  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger02.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger10  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger10.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger11  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger11.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger12  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger12.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger20  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger20.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger21  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger21.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger22  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger22.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger30  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger30.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger31  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger31.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger32  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger32.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger40  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger40.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger41  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger41.length * 1.1) rb_Trans:undefined ), 
	( rigBoneStruct rb_bone:$SKEL_R_Finger42  rb_Radius: 0.013 rb_Length:($SKEL_R_Finger42.length * 1.1) rb_Trans:undefined )
	)

	
	
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
	
	

fn combineGeo = 
(
	initialObjectArray = #()
	volObjects = #()
	originalMeshToSplit = undefined
	meshToSplit = undefined	
	
	--******************************************************************************************
	--FIRSTLY ATTACH ALL OF THE GEO INTO ONE PIECE
	--******************************************************************************************

	--need to do some error checking to ensure that the objects are picked and also that theyr are epoly
	geo = $

	if geo != undefined then
	(
		validObjs = 0
		for obj in selection do
		(
			if (obj.baseObject as string) != ("Editable Poly") do
			(
				messageBox ("WARNING! "+obj.name+" is not an Editable Poly")
				validObjs = 1
			)
		)

		if validObjs == 0 do
		(
		--*****************************************************************************************
			for obj in selection do
			(
				collapseStack obj --collapse the stack of the box
				appendIfUnique initialObjectArray obj
			)
			
			initialObjectArraySize = initialObjectArray.count 
			print ("initialObjectArraySize = "+(initialObjectArraySize as string))
			if initialObjectArraySize != 1 then
			(
				geoNo = (substring initialObjectArray[1].name 6 3)
				for i = 1 to (initialObjectArray.count - 1) do
				(
					print ("Attaching "+initialObjectArray[i].name+" to "+initialObjectArray[initialObjectArraySize].name+"...")
					initialObjectArray[initialObjectArraySize].attach initialObjectArray[i] initialObjectArray[initialObjectArraySize]
					originalMeshToSplit = initialObjectArray[initialObjectArraySize]
					print ("originalMeshToSplit = "+originalMeshToSplit.name)
				)
			)
			else
			(
				geoNo = (substring $.name 6 3)
				originalMeshToSplit = $
				print ("originalMeshToSplit = "+originalMeshToSplit.name)
			)
		)
	)
	else
	(
		messageBox ("WARNING! Please select some geo") beep:true
	)
)

fn createVols = 
(
		--******************************************************************************************
	--NOW CREATE THE VOLUMES FOR DETACHING THE MESH INTO PIECES
	--******************************************************************************************
	for i = 1 to rigBoneListStruct.count do
	(
		volTrans = undefined
		
		currentObj = getNodeByName ("splitVol_"+rigBoneListStruct[i].rb_bone.name)
		if currentObj != undefined do
		(
			delete currentObj
		)
		splitPlane = Cylinder smooth:on heightsegs:1 capsegs:1 sides:8 height:rigBoneListStruct[i].rb_Length radius:rigBoneListStruct[i].rb_Radius mapcoords:on pos:[0,0,0] isSelected:on
		splitPlane.name = ("splitVol_"+rigBoneListStruct[i].rb_bone.name)
		splitPlane.parent = rigBoneListStruct[i].rb_bone
		
		if rigBoneListStruct[i].rb_Trans == undefined then 
		(
			volTrans = rigBoneListStruct[i].rb_bone.transform
			splitPlane.transform = ( in coordsys parent volTrans)
			in coordsys parent rotate splitPlane (angleaxis 90 [0,1,0])
		)
		else --this allows us to offset the transforms from the bone.
		(
			volTrans = rigBoneListStruct[i].rb_Trans
			splitPlane.transform = ( in coordsys parent volTrans)
		)
		splitPlane.scale = [1.05,1.05,1.05]
		convertTo splitPlane PolyMeshObject
		if (substring splitPlane.name 15 7) == "R_Thigh" then --this bit will trim the thigh volume so it doesnt pick the other leg
		(
			print ("Trimming "+splitPlane.name)
			trimmerBox = Box lengthsegs:1 widthsegs:1 heightsegs:1 length:2 width:1 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.49,1.06454,0.862417]) isSelected:on
			boolObj.createBooleanObject splitPlane trimmerBox 4 5
			boolObj.setBoolOp splitPlane 3
-- 			convertTo splitPlane PolyMeshObject
		)
		else
		(
			if (substring splitPlane.name 15 7) == "L_Thigh" do
			(
				print ("Trimming "+splitPlane.name)
				trimmerBox = Box lengthsegs:1 widthsegs:1 heightsegs:1 length:2 width:1 height:2 mapcoords:on transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [-0.49,1.06454,0.862417]) isSelected:on
				boolObj.createBooleanObject splitPlane trimmerBox 4 5
				boolObj.setBoolOp splitPlane 3
-- 				convertTo splitPlane PolyMeshObject				
			)
		)
		append volObjects splitPlane		
	)
)


fn splitMesh = 
(
	createDialog progBar width:300 Height:30
	undo off
	(
-- 		geoMeshes = #()
		
		disableSceneRedraw()
		for i = 1 to volObjects.count do
		(
			if (substring volObjects[i].name 17 6) == "Finger" then
			(
-- 				print ("substring found was "+(substring volObjects[i].name 17 6) )
-- 				print ("Found finger with obj "+volObjects[i].name)
				volPer = 90
			)
			else
			(
-- 				print ("substring found was "+(substring volObjects[i].name 17 6) )
-- 				print ("Didn't find finger with obj "+volObjects[i].name)
				volPer = 35
			)
			maxOps.cloneNodes originalMeshToSplit cloneType:#copy newNodes:&nnl
			meshToSplit = nnl[1]
			meshToSplit.name = ("geo_"+geoNo+"_"+(subString volObjects[i].name 15 20))
			boolObj.createBooleanObject meshToSplit volObjects[i] 4 5
			boolObj.setBoolOp meshToSplit 2
			convertTo meshToSplit PolyMeshObject

			print ("meshToSplit = "+meshToSplit.name)
			print ("i = "+(i as string))
			select meshToSplit

			modPanel.addModToSelection (ProOptimizer ()) ui:on
			meshToSplit.modifiers[#ProOptimizer].KeepUV = on
			meshToSplit.modifiers[#ProOptimizer].OptimizationMode = 1
			meshToSplit.modifiers[#ProOptimizer].Calculate = on
			meshToSplit.modifiers[#ProOptimizer].VertexPercent = volPer
			modPanel.addModToSelection (Cap_Holes ()) ui:on
			collapseStack meshToSplit
			
			if skinOrNot == true then
			(
				--skin it
				modPanel.addModToSelection (Skin ()) ui:on
				meshToSplit.modifiers[#Skin].bone_Limit = 1
				skinOps.addBone meshToSplit.modifiers[#Skin] rigBoneListStruct[i].rb_bone 1
			)
			else
			(
				meshToSplit.parent = rigBoneListStruct[i].rb_bone
			)
			
			progBar.prog.value = (100.*i/volObjects.count)
		)
		

		enableSceneRedraw()
	)
		destroyDialog progBar
		
		delete originalMeshToSplit

-- 	destroyDialog marionetteGuiRoll	
)

fn marionette = 
(
	global marionetteGuiRoll
	
	try (cui.UnRegisterDialogBar marionetteGuiRoll)catch()
	try (destroyDialog marionetteGuiRoll)catch()

	rollout marionetteGuiRoll "Marionette" 
	(
		
		button btnCombineGeo "Combine Geo" pos:[15,05] width:120 height:25
		button btnGenVol "Generate Volumes" pos:[15,30] width:120 height:25
		button btnSplitMesh "Split Meshes" pos:[15,55] width:120 height:25
		button btnBatch "Batch Scene" pos:[15,85] width:120 height:25
		
		checkBox chkSkin "Skin" checked:false tooltip:"Do you want to skin or parent the meshes?"
		
		on chkSkin changed theState do
		(
			if chkSkin.state == true then
			(
				skinOrNot = true
				print ("skinOrNot = "+(skinOrNot as string))
			)
			else
			(
				skinOrNot = false
				print ("skinOrNot = "+(skinOrNot as string))
			)
		)
		
		on btnCombineGeo pressed do
		(
			combineGeo()
		)

		on btnGenVol pressed do
		(
			createVols()
		)

		on btnSplitMesh pressed do
		(
			splitMesh()
		)		
		
		on btnBatch pressed do
		(
			--**************************************************************************************************************
			-- THIS IS DEPENDANT UPON SELECTION SETS BEING CREATED AND NAMED *000 OR *001 ETC
			--**************************************************************************************************************
			for i = 1 to selectionSets.count do
			(
				ssName = selectionSets[i].name
				if ssName.count == 4 do
				(
					if ssName != "*All" do
					(
						print ssName
						clearSelection()
						for ob = 1 to selectionSets[i].count do
						(
							selectMore selectionSets[i][ob]
						)
						maxOps.cloneNodes selection cloneType:#copy newNodes:&nnl --need to clone nodes first so we dont destroy the originals and also cos some sets may share items
						hide selection
						clearSelection()
						for obj = 1 to nnl.count do
						(
							selectMore nnl[obj]
						)
	 					combineGeo()
	 					createVols()
	 					splitMesh()
-- 						print ("LooLooLaLa "+(selection.count as string))	
					)
				)
			)
		)
	)--end marionetteGuiRoll	

	createDialog marionetteGuiRoll 150 135 pos:[1250, 100] --main window size
)

clearListener()
marionette()