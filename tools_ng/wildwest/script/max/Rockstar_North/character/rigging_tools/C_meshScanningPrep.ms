-- merge the marker mesh file
-- make sure the pivot on the character mesh is the same as the marker mesh box
-- align markers to bones (matts scipt)
-- make a snapshot of all the bones
-- attach all snapshot mesh parts to skel mesh box
-- attach all markers to the marker mesh box

-- export as OBJ 
-- marker mesh 
-- skel mesh
-- character mesh

-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------
markerMeshFile ="X:\\gta5\\art\\peds\\Skeletons\\Male MarkersGEOVERSIONS.max"
bodyMesh =undefined
markerAlignScript = (RsConfigGetWildWestDir() + "script/max/rockstar_north/character/Rigging_tools/c_alignMarkersToSkeleton.ms")
skelBoneArray = #(
	"SKEL_ROOT", 
	"SKEL_Pelvis", 
	"SKEL_L_Thigh", 
	"SKEL_L_Calf", 
	"SKEL_L_Foot", 
	"SKEL_L_Toe0", 
	"SKEL_R_Thigh", 
	"SKEL_R_Calf", 
	"SKEL_R_Foot", 
	"SKEL_R_Toe0", 
	"SKEL_Spine_Root", 
	"SKEL_Spine0", 
	"SKEL_Spine1", 
	"SKEL_Spine2", 
	"SKEL_Spine3", 
	"SKEL_L_Clavicle", 
	"SKEL_L_UpperArm", 
	"SKEL_L_Forearm", 
	"SKEL_L_Hand", 
	"SKEL_L_Finger00", 
	"SKEL_L_Finger01", 
	"SKEL_L_Finger02", 
	"SKEL_L_Finger10", 
	"SKEL_L_Finger11", 
	"SKEL_L_Finger12", 
	"SKEL_L_Finger20", 
	"SKEL_L_Finger21", 
	"SKEL_L_Finger22", 
	"SKEL_L_Finger30", 
	"SKEL_L_Finger31", 
	"SKEL_L_Finger32", 
	"SKEL_L_Finger40", 
	"SKEL_L_Finger41", 
	"SKEL_L_Finger42", 
	"SKEL_R_Clavicle", 
	"SKEL_R_UpperArm", 
	"SKEL_R_Forearm", 
	"SKEL_R_Hand", 
	"SKEL_R_Finger00", 
	"SKEL_R_Finger01", 
	"SKEL_R_Finger02", 
	"SKEL_R_Finger10", 
	"SKEL_R_Finger11", 
	"SKEL_R_Finger12", 
	"SKEL_R_Finger20", 
	"SKEL_R_Finger21", 
	"SKEL_R_Finger22", 
	"SKEL_R_Finger30", 
	"SKEL_R_Finger31", 
	"SKEL_R_Finger32", 
	"SKEL_R_Finger40", 
	"SKEL_R_Finger41", 
	"SKEL_R_Finger42", 
	"SKEL_Neck_1", 
	"SKEL_Head"
	)
snapShotArray = #()
markerArray = #(
	"Marker_L_Calf", 
	"Marker_R_Calf", 
	"Marker_R_Foot", 
	"Marker_R_Toe0", 
	"Marker_L_Toe0", 
	"Marker_L_Foot", 
	"Marker_L_Hand", 
	"Marker_R_Hand", 
	"Marker_R_Forearm", 
	"Marker_R_UpperArm", 
	"Marker_L_UpperArm", 
	"Marker_L_Forearm", 
	"Marker_L_Thigh", 
	"Marker_R_Thigh", 
	"Marker_Pelvis", 
	"Marker_Spine1", 
	"Marker_Spine0", 
	"Marker_Spine2", 
	"Marker_Spine3", 
	"Marker_L_Clavicle", 
	"Marker_R_Clavicle", 
	"Marker_Neck_1", 
	"Marker_Head"
	)
charMeshArray =#()
completedArray =#()
-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------
fn attachHands handmesh =
(
	bodyMesh.EditablePoly.attach handmesh bodyMesh
)
fn cleanScene = 
(
-- 	max unhide all
	unhide objects
	hide $Char_Mesh
	hide $Skel_Mesh
	hide $Marker_Mesh
	max select all
-- 	max delete selection
	delete $
-- 	max unhide all
	unhide objects
)
fn makeFolders = 
	(
		currentPath = maxfilepath
		subpath = "\\obj"
		makedir (currentPath + subpath)
	)
fn attachSnapShot myArray routeObj=
(
	for i = 1 to myArray.count do
	(
		currentobject = getnodebyname myArray[i]
		print ("attatched "+myArray[i]+ " "+routeObj.name)
		routeObj.EditablePoly.attach currentobject routeObj
	)
	appendifunique completedArray routeOjb
	print (routeObj.name +" Added to Array")
)
	
fn snapShotBones =
(
	for i = 1 to skelBoneArray.count do
	(
		currentBone = getnodebyname skelBoneArray[i]
		if currentbone != undefined then
		(
			snapShotBone = snapshot currentBone
			appendifunique snapShotArray snapShotBone.name
			print ("snapshoted "+skelBoneArray[i])
		)
		else
		(
			messageBox ("WARNING! Couldnt find "+skelBoneArray[i]) beep:true
		)
	)
)

fn pivotAlign =
(
-- 	validateMesh()
	if bodyMesh != undefined then
	(
		deletemodifier bodyMesh 1
		bodyMesh.pivot = $Char_Mesh.pivot
		print ("Aligned "+bodyMesh.name+"pivot to Char_Mesh pivot")
		print ("bodyMesh.pivot = "+bodyMesh.pivot as string)
		print ("CharMesh.pivot = "+$Char_Mesh.pivot as string )
		filein markerAlignScript
	)
	else
	(
		messageBox ("WARNING! Please select head_000_R") beep:true
	)	
)

fn validateMesh =
(
	if selection.count == 1 do
	(
		currObj = $
		if currObj.name == "head_000_R" do
		(
			bodyMesh = currObj
			appendifunique charMeshArray bodyMesh.name
			mergeMarkerMesh()
			PivotAlign()
			attachHands $Lowr_000_U
			attachHands $Uppr_000_U
			snapShotBones()
			attachSnapShot snapShotArray $Skel_Mesh
			attachSnapShot markerArray $Marker_Mesh
			attachSnapShot charMeshArray $Char_Mesh
			cleanScene()
			makeFolders()
		)
	)
)


fn mergeMarkerMesh =
(
	print ("merging "+markerMeshFile)
	mergeMaxFile markerMeshFile
)

-- mergeMarkerMesh()
clearListener()
ValidateMesh()
-- PivotAlign()
-- attachHands $Lowr_000_U
-- attachHands $Uppr_000_U
-- snapShotBones()
-- attachSnapShot snapShotArray $Skel_Mesh
-- attachSnapShot markerArray $Marker_Mesh
-- attachSnapShot charMeshArray $Char_Mesh
-- cleanScene()
-- makeFolders()
