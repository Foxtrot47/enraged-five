
filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())


markArray = #(
"Marker_Pelvis", 
"Marker_L_Thigh", 
"Marker_L_Calf", 
"Marker_L_Foot", 
"Marker_L_Toe0", 
"Marker_R_Thigh", 
"Marker_R_Calf", 
"Marker_R_Foot", 
"Marker_R_Toe0", 
"Marker_Spine0", 
"Marker_Spine1", 
"Marker_Spine2", 
"Marker_Spine3", 
"Marker_L_Clavicle", 
"Marker_L_UpperArm", 
"Marker_L_Forearm", 
"Marker_L_Hand", 
"Marker_R_Clavicle", 
"Marker_R_UpperArm", 
"Marker_R_Forearm", 
"Marker_R_Hand", 
"Marker_Neck_1", 
"Marker_Head"
)

skelArray = #(
"SKEL_Pelvis", 
"SKEL_L_Thigh", 
"SKEL_L_Calf", 
"SKEL_L_Foot", 
"SKEL_L_Toe0", 
"SKEL_R_Thigh", 
"SKEL_R_Calf", 
"SKEL_R_Foot", 
"SKEL_R_Toe0", 
"SKEL_Spine0", 
"SKEL_Spine1", 
"SKEL_Spine2", 
"SKEL_Spine3", 
"SKEL_L_Clavicle", 
"SKEL_L_UpperArm", 
"SKEL_L_Forearm", 
"SKEL_L_Hand", 
"SKEL_R_Clavicle", 
"SKEL_R_UpperArm", 
"SKEL_R_Forearm", 
"SKEL_R_Hand", 
"SKEL_Neck_1", 
"SKEL_Head"
)

fn alighMarkersToSkel =
(
	for i = 1 to markArray.count do
	(
		skel = getNodeByName skelArray[i]
		markr = getNodeByName markArray[i]
		if skel !=undefined then
		(
			if markr != undefined then
			(
				print ("Aligning "+markArray[i]+" to "+skelArray[i])
				skelPos = in coordsys world skel.transform
				in coordsys world markr.transform = skelPos
			)
			else
			(
				messagebox ("WARNING! Couldn't find "+markArray[i]+". Is it named correctly?") beep:true
			)
		)
		else
		(
			messagebox ("WARNING! Couldn't find "+skelArray[i]+". Is it named correctly?") beep:true
		)
	)
)

alighMarkersToSkel()