--====================================================================================================================================================
-- script to auto generate mapping files based off user selected set of objects and user supplied namespace for objects mapping from
--Matt Rennie
--May 2010

--====================================================================================================================================================

userNameSpace = ""
userSelection = #()
global userInputWindow --declare rollout global so it can be closed


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

--====================================================================================================================================================
-- END OF VARIABLE DECLARATIONS
--====================================================================================================================================================

fn generateMappingFile = 
(
	boneFolder = maxFilePath
	maxName = filterstring maxFileName "."
	fName = (maxName[1]+".xmm")
	output_name = (boneFolder + fName)
	print ("Output name = "+output_name)
	
	if output_name != undefined do
	(
		deleteFile output_name
		print ("Deleted old "+output_name)
		
		output_file = createFile output_name	
		print ("Created new "+output_name +" file.")
		
		--=== NOW START OUTPUTTING DATA. AT FIRST WE NEED THE HEADER SHIT...
		tAd = getLocalTime()
		
		format ("<MaxMapFile version="+"\""+"0.40"+"\""+"date="+"\""+(tAd[4] as string)+"th day of"+(tAd[2] as string)+"th month "+" "+(tAd[5]as string)+":"+(tAd[6]as string)+":"+(tAd[7]as string)+" "+(tAd[1]as string)+"\""+">"+"\n") to: output_file
		format ("\t"+"<CustomData "+"/"+">"+"\n") to: output_file
	
		if userNameSpace != "" do --this puts on the colon fbx puts after namespaces. If we dont add this here we may get issues on scenes with no namespace
		(
			userNameSpace = (userNameSpace+":")
		)
		
		for i = 1 to userSelection.count do
		(
			Destin = userSelection[i].name
			Source = (userNameSpace+Destin)
			format ("\t"+"<Map name="+"\""+Destin+" "+"\\"+" Transform "+"\\" +" Position"+"\""+" mapName="+"\""+Source+" "+"\\"+" Transform"+" "+"\\"+" Position"+"\""+" />"+"\n") to: output_file
			format ("\t"+"<Map name="+"\""+Destin+" "+"\\"+" Transform "+"\\" +" Rotation"+"\""+" mapName="+"\""+Source+" "+"\\"+" Transform"+" "+"\\"+" Rotation"+"\""+" />"+"\n") to: output_file
		)
		
		format ("</MaxMapFile>"+"\n") to: output_file
		
		close output_file
		PRINT ("Mapping file output to "+(output_file as string))		
		messageBox ("Mapping file output to "+(output_file as string)) title:"Success! :)"	
	)

	if ((userInputWindow != undefined) and (userInputWindow.isDisplayed)) do
	(destroyDialog userInputWindow)
)
 


fn createMappingFile = 
(
	if ((userInputWindow != undefined) and (userInputWindow.isDisplayed)) do
	(destroyDialog userInputWindow)

	rollout userInputWindow "Enter Namespace from Source"
	(
		editText namespace "Namespace:" fieldwidth:200 tooltip:"Type in the name of the namespace on your aim from fbx"
		button Go "Go " tooltip:"Generate mapping file."
		
		
		on namspace entered txt do
		(
			userNameSpace = namespace.text
		)
		on Go pressed do 
		(
			userNameSpace = namespace.text
			
			for obj in selection do
			(
				appendIfUnique userSelection obj
			)
			print "userSelection array = "
			print userSelection
			print "================"
			print ("user namespace = "+userNameSpace)
			
			generateMappingFile()
		)
	)
	
	createDialog userInputWindow 300 60
)

clearListener()
createMappingFile()