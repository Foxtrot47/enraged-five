-- Blendshape Tools
-- Rick Stirling
-- Rockstar North 
-- June 23rd 2010


-- Extracts blendshapes/morphs from a mesh, 
-- Propagates changes from the base mesh to all the extractions 
-- Rebuilds the blendshapes from the updated targetse

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

offsetMultiplier = 1.5
blendcount = 100
blendMesh = $head_000_r -- This shouldn't he hardcoded in here, should be a selected object
positionOffset =((blendMesh.max.x - blendMesh.min.x) * offsetMultiplier)


fn extractBlends  = (
			

	-- Extraction function
	with redraw off 
	for m=1 to blendcount do --100 do
	(
		-- Get morph name
		blendname = wm3_mc_getname blendMesh.morpher m
		
		-- Enable the morph
		WM3_MC_setLimitMIN blendMesh.morpher m 100.0
		WM3_MC_SetUseLimits blendMesh.morpher m true
		blendMesh.morpher.Use_Limits = 0
		
		-- Extract the mesh from the morpher
		clearselection()
		maxOps.cloneNodes blendMesh cloneType:#copy newNodes:&meshcopy
		clearselection()
		select meshcopy

		-- Collapse the copy
		maxops.collapsenode $ off
		$.position.x = $.position.x + (positionOffset * m)

		$.name = blendname
		$.wirecolor = color 228 214 153
		
		wm3_mc_setvalue blendMesh.morpher m 0.0
		
		-- Disable the morph
		WM3_MC_setLimitMIN blendMesh.morpher m 0.0
		WM3_MC_SetUseLimits blendMesh.morpher m false
		blendMesh.morpher.Use_Limits = 1
	)
)







fn rebuildBlends  = (
	
	-- Turn off Global Morpher Limits
	select blendmesh
	toolMode.coordsys #parent

	with redraw off 
	for m=1 to blendcount do 
	(
		-- Get the blendmesh name
		blendname = wm3_mc_getname blendMesh.morpher m
		
		-- find the node in the select
		targetnode = getnodebyname blendname 

		-- Retarget the morph to the new mesh?
		WM3_MC_BuildFromNode blendMesh.morpher m targetnode
		WM3_MC_Rebuild blendMesh.morpher m 
	)
				
)





fn progagateMeshEdits  = (
	
	-- Turn off Global Morpher Limits
	select blendmesh
	toolMode.coordsys #parent

	-- clone base mesh and kill the skin and morphs
	try (delete $clonedbase) catch()
	
	clonedbase= copy blendmesh -- create a temp copy from source
	clearselection()
	select clonedbase
	$.name =  "clonedbase"
	deleteModifier $ 1
	deleteModifier $ 1
	
	with redraw off 
	for m=1 to blendcount do 
	(
		-- Get the blendmesh name
		blendname = wm3_mc_getname blendMesh.morpher m
		
		-- find the node in the select
		targetnode = getnodebyname blendname 
		
		-- I found this code on The Area
		-- For some reason Max is a spastic case when it comes to copying modifiers
		-- This is an 'elegant hack'
		-- http://area.autodesk.com/forum/autodesk-3ds-max/maxscript/transfering-uvw-data-via-maxscript/page-2/
		
		tmpObj = copy clonedbase-- create a temp copy from source
		tmpObj.transform = targetnode.transform -- align to target object
		trgMods = targetnode.modifiers -- get modifiers to can transfer them too leter on
		-- the "core" trick is here:
		swap tmpObj.baseObject targetnode.baseObject -- swap base objects
		swap tmpObj.name targetnode.name -- swap their names as well
		-- now transfer modifiers:
		for x = trgMods.count to 1 by -1 do (addModifier tmpObj trgMods[m])
		delete targetnode -- delete tmp copy
	)
	
	delete $clonedbase
)




rollout blendshapetool "Blendshape Tool"
(
	
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Char_Blendshape_Tool" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)

	
	pickbutton pbtnblendmesh "Select Blendmesh" width:160 height:30
	
	button btnExtract "Extract all shapes" width:160 height:30
	button btnPropagate "Propagate to shapes" width:160 height:30
	button btnRebuildShapes "Rebuild all shapes" width:160 height:30

	
	on pbtnblendmesh picked obj do	(
		 if obj != undefined do (
			pbtnblendmesh.text  = obj.name
			 blendmesh = obj
		)
	)

	on btnExtract pressed do extractBlends ()
	on btnPropagate pressed do progagateMeshEdits()
	on btnRebuildShapes pressed do rebuildBlends ()


	
)


createDialog blendshapetool 




