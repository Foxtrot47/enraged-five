filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())


selectedObjs = selection as array

for i in selectedObjs do
(

	if i.modifiers[#Attribute_Holder] == undefined do
	(
		select i
		modPanel.addModToSelection (EmptyModifier ()) ui:on
	)
	
	currentJoystick = i

	facialMultipliersCA = (currentJoystick.name +"_mult")

	boneName = "Multipliers"

		uiNameXPos = (boneName+"_mult") --name as seen inside the code and track view
		nameXPos = "Multiplier" --name as seen in the ui

		posVal = "1"
		negval = "-1"		
				
	rolloutCreation = (facialMultipliersCA +" = attributes "+boneName +"\n"+"(" +"\n"+"\t" +"parameters main rollout:params"+"\n"+"\t"+"(" +"\n"+"\t" +"\t" +uiNameXPos +" type:#float ui:" +uiNameXpos +" default:" +posVal +"\n"+"\t"+")" +"\n"+"\t"  +"rollout params "+" \"" +boneName+" \"" +"\n"+"\t" +"(" +"\n"+"\t"+"\t"  +"spinner "+uiNameXPos +" \"" +nameXPos+"\"" +" "+"fieldwidth:40 range:[-100,100,1] type:#float" +"\n"+"\t" +")" +"\n"+")"+"\n"+"\n"+"\t" +"custAttributes.add $" +currentJoystick.name+".modifiers[#'Attribute holder'] "+facialMultipliersCA)
		execute rolloutCreation		
	
)

