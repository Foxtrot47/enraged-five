-- Ped polygon counter
-- Will be used for debug information and to calculate memory costs
-- Rick Stirling June 2009

-- Edit: Stewart Wright March 2010 - added new file paths

-- Figure out the project
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
filein "rockstar/util/material.ms"

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

-----------------------------------------------------------
-------------- Estimated memory sizes  ---------------
-- These are all very rough ballpark figures 

--Xbox360
-- Geo = 90k per component, 20k per props
-- Textures = 42% of Console texture folder size, or 11.5% of Highres folder size
xbox360cgeo=90
xbox360pgeo=20
xbox360hrt=11.5

--PS3
-- Geo = 110k per component, 30k per props
-- Textures = 27% of Console texture folder size, or 7% of Highres folder size
PS3cgeo=110
PS3pgeo=30
PS3hrt=7



-- get the size of the highres texture folder
pedfolder = maxFilePath
modelname = getFilenameFile maxfilename
Highresfolder = pedfolder + "Textures\\HighRes\\"
Texturefolder = HighResFolder + "*.tga"
thefiles = getfiles Texturefolder 

tgafiles = #()	
textureFolderSize = 0
for f in thefiles do (
	append tgafiles (uppercase(getfilenamefile f))	
	textureFolderSize = textureFolderSize + (getFileSize f)
)

textureFolderSize = textureFolderSize/1024
estx = (textureFolderSize/100)*xbox360hrt
estp = (textureFolderSize/100)*ps3hrt







------ polyop.getNumVerts pedPart
---- get UV vert count too.
-------- polyop.getNumMapVerts pedPart 1 -- 1 is the default UV channel

---- get the number of diffuse texture variations for each
------ get object name
------ use the list of high res texture names and count the number of matches

---- count number of shaders used?
/*
maplist = #()
RsMatGetMapIdsUsedOnObject pedPart maplist
numberOfMaterials = maplist.count
MaterialIdsUsed = maplist
*/


fn padout thestring =
(
		
		if thestring.count == 1 then thestring ="   "+ thestring
		if thestring.count == 2 then thestring ="  "+ thestring
		if thestring.count == 3 then thestring =" "+ thestring		

		thestring		
)


fn tricount quadmodel =
(	
	tc = 0
	for i in 1 to (polyop.getNumFaces quadmodel)  do 
	(
			local num_verts = (polyOp.getFaceVerts quadmodel i).count
			if num_verts > 3 then tc = tc + (num_verts-2)
			else tc = tc +1
	)
	
	tc
)
		



fn AnalyseScene = 
(
	resultsdata =""
	resultsdata = modelname +"\r\n"
	
	totaltri = 0
	totalvert = 0
	
	-- Set up initial selections
	objSelectedArray=#()
	if $ == undefined then (
		for obj in geometry do (if (classof obj ==Editable_poly) or (classof obj ==Editable_mesh) or (classof obj ==PolyMeshObject) then append objSelectedArray (uppercase obj.name))	
	)
	
	else (
		for obj in selection do (if (classof obj ==Editable_poly) or (classof obj ==Editable_mesh) or (classof obj ==PolyMeshObject) then append objSelectedArray (uppercase obj.name))
	)
	
	objCount = objSelectedArray.count
	
	objSelectedArray = sort objSelectedArray
	
	
	componentCount= 0
	propCount= 0
	
	for o = 1 to objCount do
	(
		pedPart =  objSelectedArray[o]
		if substring pedpart 1 2 == "P_" then propCount+=1 else componentCount+=1
	
	)
	
	componentResults = (componentCount as string) +" Components \r\nApprox  Xbox:" + ((componentCount * xbox360cgeo) as string) +"kb, PS3 " + ((componentCount * PS3cgeo) as string) +"kb" 
	propResults = (propCount as string) +" Props \r\nApprox Xbox:" + ((propCount * xbox360pgeo) as string) +"kb,  PS3 " + ((propCount* PS3pgeo) as string) +"kb \r\n" 

	textureresults = "Textures \t Xbox:" + (estx as string) + "kb \t PS3 " + (estp as string) + "kb \r\n" 
	
	resultsdata = resultsdata + componentResults +"\r\n"
	resultsdata = resultsdata + propResults +"\r\n"
	resultsdata = resultsdata + textureresults +"\r\n"
	
	
	
	-- Polcounts first
	for o = 1 to objCount do
	(
			pedPart = getnodebyname objSelectedArray[o]


			-- get the polycount and vert count of each. 
			polycountQ = polyop.getNumFaces pedPart -- this is in quads
			polycountT = tricount pedPart 
			vertcount = polyop.getNumVerts pedPart
		
			totaltri = totaltri + polycountT
			totalvert = totalvert + vertcount

			-- some formatting
			polycountQ = padout (polycountQ as string)
			polycountT = padout (polycountT as string)
			vertcount = padout (vertcount as string)
		
			
			print polycountQ
		print polycountt
		print vertcount

		
			dataline = objSelectedArray[o] + " \t Po:" +  polycountQ  + "\t Tri:" +  polycountT  + "\t Vert:" +  vertcount
	
			resultsdata = resultsdata + dataline +"\r\n"
	)	
		
	resultsdata = resultsdata + "\r\n"
	resultsdata = resultsdata + "Total triangle count:" + (totaltri as string) +"\r\n"
	resultsdata = resultsdata + "Total vertex count:" + (totalvert as string) +"\r\n"
	
	
	resultsdata
)




		
				
rollout pedData "Ped Data"
(
	
	button btnAnalyseScene "Analyse Scene" width:150 height:30
	dotNetControl edtPedResults "System.Windows.Forms.TextBox" height:250 readonly:true
	
	
	
	on btnAnalyseScene pressed do (
		resultstext = AnalyseScene ()
		font=dotNetObject "System.Drawing.Font" "Courier New" 8
		print font.name
		edtPedResults.font = font
		edtPedResults.text= resultstext 
	)
	
	
	on pedData open do
	(
		ScrollbarsEnum = dotNetClass "System.Windows.Forms.ScrollBars"
		edtPedResults.Multiline = true
		edtPedResults.ScrollBars = ScrollbarsEnum.Vertical
	)
	

)





-- Create floater 
theNewFloater = newRolloutFloater "Ped Data Tool" 600 400

-- Add the rollout section
addRollout pedData theNewFloater
