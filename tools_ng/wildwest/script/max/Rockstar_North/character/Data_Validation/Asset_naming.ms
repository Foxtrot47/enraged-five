-- Export tester script
-- Rick Stirling
-- April 2009
-- Checks for various things before skinning and before Export
-- Artists need to worry less about naming conventions

-- load the common functions and data definition files
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_common.ms"
filein "X:/tools/dcc/current/max2009/scripts/character/Data_Validation/valid_export_names.dat"


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())


-- Check selected component names
-- If nothing selected, select all geo in scene
fn checkComponentNames =(
	
	-- Use the current selection
	-- If nothing is selected, grab all the editable mesh/poly objects in the scene
	-- Make sure all data is valid!
	temparray=#()
	if $ == undefined then (
		for obj in geometry do (if (classof obj ==Editable_poly) or (classof obj ==Editable_mesh) then append temparray obj)	
	)
	
	else (
		for obj in selection do (if (classof obj ==Editable_poly) or (classof obj ==Editable_mesh) then append temparray obj)
	)
	
	select temparray
	
	currentComponents =#()
	currentComponents =getCurrentSelection()
	
	print currentComponents -- DEBUG PRINT
	
	-- Armed with an arry of the objects in the scene, parse the object name
	pedmeshcount = currentComponents.count
	
	pedmeshcount = 1 -- OVERRIDE FOR TESTING
	
	for pm = 1 to pedmeshcount do
	(
		pedmesh= currentComponents [pm]
		
		-- Split off the ending after the space (if that exists)
		pedmeshsplit = filterstring pedmesh.name " "
		if pedmeshsplit[1].count != 10 then
		(
			-- The name before the first space is too short
			-- Let's see if the person used spaces instead of _
			ctype = pedmeshsplit[1]
			
			-- Is this a valid component type?
			-- The MasterComponentArray contains all good data types and all the bad variations we can think of.
			for mcaLoop = 1 to MasterComponentArray.count do
			(
				print mcaLoop -- DEBUG PRINT
				comArray = MasterComponentArray[mcaLoop]
				print comArray -- DEBUG PRINT
			)

			
		)	
		
	)
	
	
)



rollout unnamedRollout "Asset Naming Tool" width:120 height:300 
(

	button btnCComN "Check Component names" width:140 height:30 
	button btnCPropN "Check Prop names" width:140 height:30 
	--button btnCTF "Check folder of textures"  width:140 height:30 
	
	on btnCComN pressed do
	(
		checkComponentNames ()
	)

	on btnCPropN pressed do
	(

	)

	

	
)




-- Create floater  
theNewFloater = newRolloutFloater "Asset Naming Tool" 180 320


-- Add the rollout section 
addRollout unnamedRollout theNewFloater


