-- Export tester script
-- Checks for various things before skinning and before Export

valid_cnames = #("HEAD","UPPR", "LOWR", "SUSE", "SUS2", "HAND", "FEET","HAIR","TEEF")
valid_pnames =#("HEAD", "EYES", "EARS", "LHAND", "RHAND", "LWRIST", "RWRIST")

-- load the common functions
filein "X:/tools/dcc/current/max2009/scripts/character//includes/FN_common.ms"

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

fn selectdummy =
(
	--whats the name of the character?
	thename = maxfilename as string
	shortname = filterstring thename "."
	thedummyname = shortname[1]
	
	for obj in helpers do 
	(
		if obj.name == thedummyname then select obj 
	)
)



-- check the names of all the components 
fn checkprops =
(


	-- Select the dummy
	selectdummy()
	
	-- first get a list of all the names (children of the dummy) 
	numkids = $.children.count  
	propsetobj=#() 
	propsetname=#()

	for i = 1 to numkids do 
	(
		kids = uppercase $.children[i].name
		isprop = substring kids 1 1
		if (isprop == "P") then 
		(
			append propsetname kids
			append propsetobj $.children[i]
			
		)			
	)


	-- At this stage we have a list of probable props (anything that starts with P)
	
	
	for p = 1 to propsetname.count do
	(
	
		-- look for "P_"
		theprop = propsetname[p]
		theunderbar = substring theprop 1 2
		
		if (theunderbar == "P_") then
		(
			print "The underbar at position 2 is good"
		)
		else 
		(
			temp1 = propsetname[p] + " doesn't start with 'P_'. Fix it you slag."
			messagebox temp1
		)
		
		-- let's check the slot bit
		theslot = filterstring theprop "_"
		pslot = theslot[2]
		
		print "the prop has the follow slot id"
		print pslot
		
		found = 0		
		for c = 1 to goodpropname.count do
		(

			if (pslot == goodpropname[c]) then
			(
				print "MATCH!"
				found = pslot
			)
		)
		
		-- match?
		if found != 0 then
		(
			print "MATCH!"
			t1 = "prop name was " + pslot
			print t1 
		)
		
		else
		(
			temp1 = propsetname[p] + " doesn't have a valid prop slot identifer."
			messagebox temp1

		)
		

		-- finally, the 3 digits
		thedigits = theslot[3]
		check = thedigits as integer

		print check
		
		if check == undefined then
		(
			temp1 = "Check the 3 numeric digits of " + propsetname[p]
			messagebox temp1
		)


		
	)
	
	
)














-- check the names of all the components
fn check_component_names =
(

	-- Select the dummy
	selectdummy()
	
	-- first get a list of all the names (children of the dummy)
	numkids = $.children.count
	selset=#()

	for i = 1 to numkids do
	(
		append selset $.children[i].name
	)

	-- for each, check that it is cccc_###_I
	
		
	for i = 1 to numkids do
	(
		thename = selset[i]
		thename = uppercase thename
	
		isprop = substring thename 1 1
		isprop = uppercase isprop
		
		-- any longer than 10 characters?
		try 
		(
			toolong = substring thename 11 1
		) 	catch ()
		
		
		if (toolong != "" AND isprop !="P") then
		(
			select $.children[i]
			objname = $.name
			outtext = "The component name " + objname  + " is too long " + thename + ". The component will now be selected and the script will stop." as string
			messagebox outtext
			return false
		)
		
	
		thecom = substring thename 1 4
		thefirst = substring thename 1 1
	
		matchedname = 0
		for nc = 1 to valid_cnames.count do
		(
				-- Check the name of the component against the list of correct names.
				validname = valid_cnames[nc]
				if (thecom == validname) then matchedname = 1
		)	
		
		-- If not match was found, then the current component must not be valid.
		if matchedname == 0 then
		(	
			select $.children[i]
			objname = $.name
			outtext = "The component name " + objname  + " isn't a valid component name. The component will now be selected and the script will stop." as string
			messagebox outtext
			return false

		)	
		
		-- The first part is correct in all
		-- make sure its now a _ at fifth place
		
		theunderbar = substring thename 5 1
		
		if (theunderbar == "_" or thefirst == "P" ) then
		(
			print "The underbar at position 5 is good"
		)
		
		else 
		(
			select $.children[i]
			outtext = "Check the first underbar of " + thename + " which should be the 5th character. The component will now be selected and the script will stop." as string
			messagebox outtext
			return false 
		)
		
		
		
		-- now check the 3 digits by trying to convert to an integer. If it break, then the name is bad.
		thedigits = substring thename 6 3
		check = thedigits as integer

		if (thefirst == "P") then
		(
			check = 999
		)

		print check
		
		if check == undefined then
		(
			select $.children[i]
			outtext = "Check the 3 numeric digits of " + thename + ". The component will now be selected and the script will stop." as string
			messagebox outtext
			return false 
		)
		
		
		-- make sure its now a _ at ninth place 
		
		theunderbar = substring thename 9 1
		
		if (theunderbar == "_" or thefirst == "P") then 
		(
			print "Theunderbar at position 9 is good"
		)
		
		else  
		(
			select $.children[i] 
			outtext = "Check the second underbar of " + thename + " which should be the 9th character. The component will now be selected and the script will stop." as string
			messagebox outtext
			return false 
		)
		
		
		-- make sure its now U or R at tenth place 
		
		theri = substring thename 10 1
		
		if (theri == "U" or theri == "R" or thefirst == "P") then 
		(
			print "The race bit is good"
		)
		
		else   
		(
			select $.children[i] 
			outtext = "Check the racial identifier of " + thename + " which should be R or U. The component will now be selected and the script will stop." as string
			messagebox outtext
			return false  
		)
		
	
	) 
	
	
)






fn checkrage=
(
	-- This bit checks that rage shaders are aplpied to components and props
	-- Select the dummy, get the kids, set their name to uppercase
	selectdummy ()

	numkids = $.children.count 
	pd = $

	print pd

	for obj in geometry do
	(
		-- select each child in turn
		if obj.parent == pd then
		(
			thename = obj.name
			themat = obj.material as string
			
			print thename
			print themat

	
			-- Each good material MIGHT me standalone or a multisub
			split = filterstring themat ":"
			shtype = split[2] as string
			print ""
			print shtype
			theshort = substring shtype 1 4

			
			if (shtype == "Rage_Shader" or theshort =="Rage") then
			(
				-- rage shader
				--print "Rage shader applied"
	
			)
			

			
			else
			(
				outtext = thename + " does not have a rage shader applied. The script will exit" as string
				messagebox outtext
				select obj
				return false
			)
			
		)
		
	)
	messagebox "Shaders SEEM good"
)





fn checkrace=
(
	-- Select the dummy 
	selectdummy 

	print "in race texture check bit"
	-- grab each head_a texture, check what the number is and the race
	-- then grab each lower and upper a texture for that number and check race is same or uni
	
	tp = maxfilepath + "Textures\TGA\\"
	tpf = tp + "*.tga"
	files = getfiles tpf
	
	afiles = #()
	
	for f in files do
	(
		filename = getfilenamefile f
		filename = uppercase filename
		
		ana= substring filename 15 1
		zero = substring filename 11 3
		
		if (ana == "A" and zero =="000") then
		(
			append afiles filename
		)	
	)

	c = afiles.count

	race =""
	
	for i = 1 to c do
	(
		com = substring afiles[i] 1 4
		
		if (com == "HEAD") then
		(
			--found the head, get its race
			race = substring afiles[i] 17 3
		)
	)
	
	for i = 1 to c do
	(
		com = substring afiles[i] 1 4
		if (com != "HEAD") then
		(
			-- not a head, get its race
			crace = substring afiles[i] 17 3
			if (crace == race or crace =="UNI") then
			(
				print "Race is good"
			) 
			
			else
			(
				print "race match error"
				outtext = afiles[i] + " does not match the race of head_a which is " +  race as string
				messagebox outtext
			)
			
		)
	)

	
)



rollout unnamedRollout "Export checks" width:120 height:300 
(
	button btanalyse "Analyse geo/Textures" width:140 height:30 
	button btncn "Check Component names" width:140 height:30 
	button btnpn "Check Prop names" width:140 height:30 
	button btnra "Check Rage Applied"  width:140 height:30 
	button btntf "Check folder of textures"  width:140 height:30 
	
	
	

	
	on btanalyse pressed do filein "x:/tools/dcc/current/max2009/scripts/character/Data_Validation/Ped_size.ms"
	
	on btncn pressed do
	(
		check_component_names()
	)

	on btnpn pressed do
	(
		checkprops()
	)

	
	on btnra pressed do
	(
		checkrage()
	)

	on btntf pressed do
	(
		fileIn "checktextures.ms"
	)

	
)




-- Create floater  
theNewFloater = newRolloutFloater "Export checks" 180 320


-- Add the rollout section 
addRollout unnamedRollout theNewFloater


