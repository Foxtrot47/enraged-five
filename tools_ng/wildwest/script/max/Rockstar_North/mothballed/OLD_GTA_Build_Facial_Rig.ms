-- Quick Face Rig builder
-- Rick Stirling
-- September 2009


-- Load the useful Functions
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_common.ms"
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_Rigging.ms"


-- Set up some empty arrays
faceControllerList =#()
faceBoneList =#()

-- Use variable in the rest of the script.
headBone = $'SKEL_Head'



faceslidersize = 0.4

-- A quick way to pass slider shapes into functions
Sq = 1
Ve = 2
Ho = 3



-- Setup the rig
-- This builds a series of sliders
fn buildFaceControls = 
(	
	
	-- Create a header for the face rig
	text size:0.05 kerning:0 leading:0 transform:(matrix3 [1,0,0] [0,0,1] [0,-1,0] [0.545,0,1.98]) isSelected:on name:"Face_Rig" text:"Face_Rig" wirecolor:(color 166 229 229)
	
	-- Empty the controller list
	faceControllerList =#()
	
	
	-- Create the controllers, and put each into the controller list for later
	
	-- Brows
	append faceControllerList (newstick = createJoystick ("L_Brow" as string) Sq [0.65 ,0 ,1.9] faceslidersize)
	append faceControllerList (newstick = createJoystick ("C_Brow" as string) Sq [0.55 ,0 ,1.9] faceslidersize)
	append faceControllerList (newstick = createJoystick ("R_Brow" as string) Sq [0.45 ,0 ,1.9] faceslidersize)

	-- Eyes
	append faceControllerList (newstick = createJoystick ("L_Eyeball" as string) Sq [0.60 ,0 ,1.77] faceslidersize)
	append faceControllerList (newstick = createJoystick ("R_Eyeball" as string) Sq [0.50 ,0 ,1.77] faceslidersize)


	-- Eyelids
	append faceControllerList (newstick = createJoystick ("L_Eyelid" as string) Ve [0.65 ,0 ,1.78] faceslidersize)
	append faceControllerList (newstick = createJoystick ("R_Eyelid" as string) Ve [0.45 ,0 ,1.78] faceslidersize)

	-- Cheeks
	append faceControllerList (newstick = createJoystick ("C_Cheeks" as string) Sq [0.55 ,0 ,1.70] faceslidersize)


	-- Mouth
	append faceControllerList (newstick = createJoystick ("L_LipUpper-R" as string) Sq [0.65 ,0 ,1.6] faceslidersize)
	append faceControllerList (newstick = createJoystick ("L_LipCorner-R" as string) Sq [0.70 ,0 ,1.54] faceslidersize)
	append faceControllerList (newstick = createJoystick ("L_LipLower-R" as string) Sq [0.61 ,0 ,1.48] faceslidersize)

	append faceControllerList (newstick = createJoystick ("L_LipUpper-Tr" as string) Sq [0.71 ,0 ,1.63] faceslidersize)
	append faceControllerList (newstick = createJoystick ("L_LipUpper-TrBF" as string) Ve [0.77 ,0 ,1.63] faceslidersize)

	


	append faceControllerList (newstick = createJoystick ("R_LipUpper-R" as string) Sq [0.45 ,0 ,1.6] faceslidersize)
	append faceControllerList (newstick = createJoystick ("R_LipCorner-R" as string) Sq [0.40 ,0 ,1.54] faceslidersize)
	append faceControllerList (newstick = createJoystick ("R_LipLower-R" as string) Sq [0.49 ,0 ,1.48] faceslidersize)

	append faceControllerList (newstick = createJoystick ("R_LipUpper-Tr" as string) sq [0.39 ,0 ,1.63] faceslidersize)
	append faceControllerList (newstick = createJoystick ("R_LipUpper-TrBF" as string) Ve [0.33 ,0 ,1.63] faceslidersize)


	-- Jaw
	append faceControllerList (newstick = createJoystick ("C_Jaw-R" as string) Sq [0.55 ,0 ,1.40] faceslidersize)
	append faceControllerList (newstick = createJoystick ("C_Jaw-Tr" as string) Sq [0.55 ,0 ,1.32] faceslidersize)
	
	
	for fc in faceControllerList do fc.parent = $Face_Rig

)







fn selectFaceBones =
(
	select headbone
	selectHierarchy()
	deselect headbone
	try (deselect $'*nub') catch()
	try (deselect $'collision*') catch()
	try (deselect $'constraint*') catch()
)	



fn selectFaceControllers = 
(
	obj = $ -- Something might already be sleected in the scene
	select $'JSC_*'
	faceControllerList = getCurrentSelection()
	clearSelection()
	try (select obj) catch() -- Try to set the selection back to the older object
)	


fn ZeroFacialRig =
(
	selectFaceBones()
	FreezeTransform()
)	



fn mergeFacialRig =
(
	-- Clean the list array
	faceBoneList =#()
	
	-- Load the rig
	mergemaxfile "x://jimmy//jimmy_art//Peds//Skeletons//Facial Rig.max" #select
	deselect $'FACE_RIG_PARENT'
	
	-- Populate the face bone array
	faceBoneList  = $ as array
	
	-- Reparent all the kids
	($'FACE_RIG_PARENT'.children).parent = headbone
	delete $'FACE_RIG_PARENT'
	
	selectFaceBones()
	FreezeTransform()
)	



-- Turn the face bones box mode on or off.
fn boneBoxMode onOff =
(
	selectFaceBones()
	if onOff == true then (
			$.boxmode = on
	)
	
	else (
		$.boxmode = off
	)
)



fn resetFacecontrollers =
(
	-- Reset the controllers
	selectFaceControllers ()
	resetControllers faceControllerList 	
)	


-- Connect the sliders to the face bones
-- This just replays a series of calls record from the Rig_Controllers tool.
fn AssignControlsFaceBones =
(

	-- You can only do this if the face rig exists
	-- Try to select the rig parent object
	clearSelection()

	try (select $Face_Rig) catch(messagebox "No face rig found in scene")
	
	if selection != undefined then
	(	
		resetFaceControllers()
		
		--FECSliderToBone "R/T" TheBone theController theBoneAxis theControleraxis 60 60 -1 true -1
		
		
		-- Eyeballs
		FECSliderToBone "R" $'FB_L_Eyeball' $'JSC_L_Eyeball' 3 2 45 45 -1 true -1
		FECSliderToBone "R" $'FB_L_Eyeball' $'JSC_L_Eyeball' 2 1 45 45 -1 true -1
		
		FECSliderToBone "R" $'FB_R_Eyeball' $'JSC_R_Eyeball' 3 2 45 45 -1 true -1
		FECSliderToBone "R" $'FB_R_Eyeball' $'JSC_R_Eyeball' 2 1 45 45 -1 true -1
		
		--Eyelids
		FECSliderToBone "R" $'FB_L_Eyelid' $'JSC_L_Eyelid' 3 2 20 20 -1 true -1
		FECSliderToBone "R" $'FB_R_Eyelid' $'JSC_R_Eyelid' 3 2 20 20 -1 true -1
		
		-- Brows
		FECSliderToBone "R" $'FB_L_Brow' $'JSC_L_Brow' 3 2 10 10 1 true 1
		FECSliderToBone "R" $'FB_L_Brow' $'JSC_L_Brow' 2 1 10 10 -1 true -1
		
		FECSliderToBone "R" $'FB_C_Brow' $'JSC_C_Brow' 3 2 10 10 1 true 1
		FECSliderToBone "R" $'FB_C_Brow' $'JSC_C_Brow' 2 1 10 10 -1 true -1
		
		FECSliderToBone "R" $'FB_R_Brow' $'JSC_R_Brow' 3 2 10 10 1 true 1
		FECSliderToBone "R" $'FB_R_Brow' $'JSC_R_Brow' 2 1 10 10 -1 true -1
		
		
		-- Cheeks
		FECSliderToBone "R" $'FB_C_Cheeks' $'JSC_C_Cheeks' 3 2 10 10 1 true 1
		FECSliderToBone "R" $'FB_C_Cheeks' $'JSC_C_Cheeks' 2 1 10 10 -1 true -1
		
		-- Jaw

		FECSliderToBone "R" $'FB_C_Jaw' $'JSC_C_Jaw-R' 3 2 5 20 1 true 1
        FECSliderToBone "R" $'FB_C_Jaw' $'JSC_C_Jaw-R' 2 1 5 5 -1 true -1
		FECSliderToBone "T" $'FB_C_Jaw' $'JSC_C_Jaw-Tr' 2 2 0.5 0 -1 true 0


          -- Lips

          -- Upper

        FECSliderToBone "R" $'FB_L_LipUpper' $'JSC_L_LipUpper-R' 3 2 45 45 -1 true -1
        FECSliderToBone "R" $'FB_L_LipUpper' $'JSC_L_LipUpper-R' 2 1 30 30 -1 true -1
        FECSliderToBone "T" $'FB_L_LipUpper' $'JSC_L_LipUpper-Tr' 1 2 0.5 0 1 true 0
        FECSliderToBone "T" $'FB_L_LipUpper' $'JSC_L_LipUpper-Tr' 3 1 0.5 0 1 true 0
        FECSliderToBone "T" $'FB_L_LipUpper' $'JSC_L_LipUpper-TRBF' 2 2 1.0 0 -1 true 0
                        
		FECSliderToBone "R" $'FB_R_LipUpper' $'JSC_R_LipUpper-R' 3 2 45 45 -1 true -1
		FECSliderToBone "R" $'FB_R_LipUpper' $'JSC_R_LipUpper-R' 2 1 30 30 -1 true -1
		FECSliderToBone "T" $'FB_R_LipUpper' $'JSC_R_LipUpper-Tr' 1 2 0.5 0 1 true 0
		FECSliderToBone "T" $'FB_R_LipUpper' $'JSC_R_LipUpper-Tr' 3 1 0.5 0 1 true 0
		FECSliderToBone "T" $'FB_R_LipUpper' $'JSC_R_LipUpper-TRBF' 2 2 1.0 0 -1 true 0

        -- Corners
        FECSliderToBone "R" $'FB_L_Crn_Mouth' $'JSC_L_LipCorner-R' 3 2 10 05 -1 true -1
		FECSliderToBone "R" $'FB_L_Crn_Mouth' $'JSC_L_LipCorner-R' 2 1 10 20 -1 true -1
        FECSliderToBone "R" $'FB_R_Crn_Mouth' $'JSC_R_LipCorner-R' 3 2 10 05 -1 true -1
        FECSliderToBone "R" $'FB_R_Crn_Mouth' $'JSC_R_LipCorner-R' 2 1 10 20 -1 true -1

			
		-- Lowers	
		FECSliderToBone "R" $'FB_L_LipLower' $'JSC_L_LipLower-R' 3 2 45 30 1 true -1
        FECSliderToBone "R" $'FB_L_LipLower' $'JSC_L_LipLower-R' 2 1 30 30 -1 true 1
        FECSliderToBone "R" $'FB_R_LipLower' $'JSC_R_LipLower-R' 3 2 45 30 1 true -1
		FECSliderToBone "R" $'FB_R_LipLower' $'JSC_R_LipLower-R' 2 1 30 30 -1 true 1


	)
	

	
)	







-- Store or load the position of all the facebones.
-- This allows different positions for different heads in the same file
-- loadstore = "load" or "store" as a text string.
fn StoreLoadFaceRig loadstore =
(
	-- Is an object selected
	if $ == undefined  then (
			outtext = "Nothing selected - please select a head"
			messagebox outtext
			return 0
	)
	

	-- The selected object must be a HEAD
	selectedObject = $
	selectedObjectName = $.name as string
			
	-- is this a head?
	checkForHead = lowercase (substring selectedObjectName 1 4)
			
	if (checkForHead == "head") then (
				
		-- Get some scene details.
		thename = maxfilename as string
		thepath = maxfilepath as string
		headname = ((filterstring thename ".")[1]) +"_"+selectedObjectName
				
		xafpath = thepath+"xaf"
		xafname = xafpath +"\\" + headname + ".xaf"
				
		-- create folder if needed
		try (makedir xafpath) catch()
	
		
		-- select the facebones
		resetFaceControllers()
		selectFaceBones()
		
		
		-- this is a bugfix from a forum
		attArray = #()
		usrArray = #()
			
		
		if loadstore == "store" then (
			-- save out the animation, 1 frame only.
			currentanimrange = animationrange
			animationRange = interval 0f 1f
			addNewKey $ 0	-- Bakes the key!
			
			LoadSaveAnimation.saveanimation xafname $ attArray usrArray segInterval: (interval 0f 1f)
			
			
			-- reset the animation and range
			deleteKeys $ #allKeys -- deletes the keys again
			animationRange = currentanimrange
		)	
		
		
		if loadstore == "load" then (
			-- load out the animation, 1 frame only.
			currentanimrange = animationrange
			animationRange = interval 0f 1f
			
			LoadSaveAnimation.loadanimation xafname $ 
			
			-- reset the animation range
			animationRange = currentanimrange
		)	
		
		
		
		select selectedObject


	) -- it was a valid head object
	
	else 		
	(
		messagebox "Selected object does seem to be a head object"
	)

)







rollout rigcontrollers "Facial Rig Tools"
(	

	
	
	Button btnMergeFaceBones "Merge Face Bones" width:190 height:30 
	Button btnStoreFaceRig "Store Current Facial Rig Setup" width:190 height:30 
	Button btnLoadFaceRig "Load Facial Rig Setup to Current" width:190 height:30 
	Button btnCreateControllers "Create Face Controllers" width:190 height:30 
	Button btnAssignControllers "Assign Controllers" width:190 height:30 
	Button btnResetControllers "Reset Face Controllers" width:190 height:30 
	Button btnAddFaceBones "Add Face Bones to Current" width:190 height:30 
	Button btnSkinBrush "Skin Brush Tool" width:190 height:30 
	
	Button btnBoxModeOn "Box Mode On" width:80 height:30 across:2
	Button btnBoxModeOff "Box Mode Off" width:80 height:30
	
	on btnCreateControllers pressed do buildFaceControls()
	on btnResetControllers pressed do resetFaceControllers()
	on btnMergeFaceBones pressed do mergeFacialRig()
	
	on btnStoreFaceRig pressed do StoreLoadFaceRig "store"
	on btnLoadFaceRig pressed do StoreLoadFaceRig "load"
	
	on btnAssignControllers pressed do AssignControlsFaceBones()
	on btnSkinBrush pressed do fileIn "X:/tools/dcc/current/max2009/scripts/character//Rigging_tools/skinbrush.ms"
		
	on btnBoxModeOn pressed do (boneBoxMode true)
	on btnBoxModeOff pressed do (boneBoxMode false)
	
	
	
)	





createDialog  rigcontrollers width:200