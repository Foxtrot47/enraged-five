-- IM Wrinkles to Rage wrinkles tool


-- Need to merge in the wrinkles, wrap this in a UI perhaps?

filein "MP3WrinkleData/MaxPayneWrinkles.ms"


fn FECWrinkleSlider theTargetNode theScalarName theControlNode theExpression= (

	-- Set a float controller on the zero'd out Y position of the slider
	fc = theTargetNode.pos.Zero_Pos_XYZ.controller.Y_Position.controller = Float_Expression () -- ZERO'd
	
	-- Add a scalar target to the expression
	fc.AddScalarTarget theScalarName theControlNode

	-- Add the expression string
	fc.SetExpression theExpression
)


-- Takes a simple expression and normalises it
fn maxExpWrap theExpression = (

	-- Compute the final expression
	-- IM run to 100%, we need to run normalised 0-1, hence the max and the * 0.01 code
	 wrappedexpression = "min(1,   (max  ((" + theExpression +" * 0.01),0))   )"
	
	--wrappedexpression = "(" + theExpression +" * 0.01)"
	
)




-- Head data array
-- This should be supplied by a tool.
-- Wrinkle map, wrinkle mask, scalar name, controller, expression
-- I use base 0 for counting wrinkles at the minute since that matches the max shader setup
-- This array needs to be sorted by wrinkle and then by mask.

-- Slider number, wrinkle map number, wrinklemask, scalar name, controller, expression



-- The loop that does shit

currentExpression = ""
finalExpression = ""
expressionList =#()

for we = 1 to headWrinkleData.count do (
	
	
	-- Figure out what mask we are using and what the previous mask was
	try (thePreviousMask = (headWrinkleData[we-1][3])) catch (thePreviousMask = undefined)
	thecurrentMask = (headWrinkleData[we][3])
			

	
	-- We may have multiple inputs to the same slider each with a different expression
	-- Adding the inputs is easy, but each new expression will overwrite the existing one
	if thecurrentMask == thePreviousMask then (

		currentExpression = "(" + headWrinkleData[we][6] + ")"
		append expressionList currentExpression
		finalExpression = ""
		
		-- Build up the composite expression
		partialExpression = expressionList[1]
		for ce = 2 to expressionList.count do ( 
			partialExpression = partialExpression + " + " + expressionList[ce]
		)	
		partialExpression = "(" + partialExpression + ")"
		finalExpression = maxExpWrap  partialExpression
	)	
	else (
		expressionList =#()
		currentExpression = "(" + headWrinkleData[we][6] + ")"
		append expressionList currentExpression
		finalExpression = maxExpWrap currentExpression
	)	
	
	
	-- Need to figure out the target node
	thetargetnodename = headWrinkleData[we][1]
	theTargetNode = getNodeByName theTargetNodeName
	
	--FECWrinkleSlider takes 3 parameters: theScalarName theControlNode theExpression
	FECWrinkleSlider theTargetNode headWrinkleData[we][4] headWrinkleData[we][5] finalExpression 
	
)