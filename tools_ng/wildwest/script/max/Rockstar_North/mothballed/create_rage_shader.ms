-- Create Rage Shaders
-- Parses a ped with max shaders to make Rage Versions
-- Rockstar North
-- Rick Stirling


-- July 2009: Added Agent specific component names.



-- Load the common fucntions file
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_common.ms"
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_Rage_Shaders.ms"


-- Array to hold textures.
tgafiles = #()	
texturesfolderHigh = maxfilepath + "Textures\\HighRes\\"	
texturesfolderConsole = maxfilepath + "Textures\\ConsoleRes\\"	

texturesfolder = texturesfolderHigh			-- Set textures folder to High
texturesfolder = texturesfolderConsole		-- Set textures folder to Console -- better default

--We might have (nastily!!!) set a global variable for the texture folder though
-- This checks to see if the artist has set the texture folder previously
if chosentexturefolder != "" then texturesfolder = chosentexturefolder


-- We use .tga files at Rockstar North.
texturesfolderfiles = texturesfolder + "*.tga"	


-- Sort array data
sortarrayn = #() -- number of components
sortarrayl = #() -- largest array



-- ***************************************************************************************************************************************************
-- ***************************************************************************************************************************************************
-- ***************************************************************************************************************************************************
-- Functions


-- Some kind of sorting function
-- I can't remember what I use it for 
-- Update this when I do know.
fn sortComponentArray compSoArray compSoName = 
(
	if compSoArray.count > 0 then 
	(
		append sortarrayn compSoArray.count
		append sortarrayl compSoName
		compSoArray = sort compSoArray 
	)
)







fn replaceTextures thearray comBase shaderid subid matid =
(

	for i = 1 to thearray.count do
	(
		tocheck = substring thearray[i] 6 3
		
		if (tocheck == shaderid) then
		(

			-- Build a filename string for the expected maps
			expdi = comBase + "_DIFF_" + shaderid + "_A_" -- we don't know the racial identifier
			expsp = comBase + "_SPEC_" + shaderid 
			expno = comBase + "_NORMAL_" + shaderid 

			-- Empty variables
			tempDiffMap = tempSpecMap = tempNormMap = ""
			
			-- We grabbed a list of all the filenames a long time ago
			-- cycle through the list of textures to find a match.
			for t = 1 to tgafiles.count do
			(
				dtgacheck = substring tgafiles[t] 1 expdi.count
				stgacheck = substring tgafiles[t] 1 expsp.count
				ntgacheck = substring tgafiles[t] 1 expno.count
				
				if (dtgacheck == expdi) then tempDiffMap  = tgafiles[t] + ".TGA"
				if (stgacheck == expsp) then tempSpecMap = tgafiles[t] + ".TGA"
				if (ntgacheck == expno) then tempNormMap = tgafiles[t] + ".TGA"
			)
			
			print "FOUND MAPS"

			-- If we have found some textures, put them into the right place.
			if (tempDiffMap != "") then	(
				RAGEsetV matid subid "diffuse texture" (texturesfolder + tempDiffMap)
			)
			
			else 	(
				temp1 = "No TGA found for " + expdi as string + " - BAD BAD BAD!"
				messagebox temp1
			)
					
		
			if (tempSpecMap != "") then	(
				RAGEsetV matid subid  "specular texture"  (texturesfolder + tempSpecMap)
			)
			
			else 	(
				temp1 = "No TGA found for " + expsp as string + " - OK if this is a dummy"
				messagebox temp1
			)
			
			
			if (tempNormMap != "") then (
				RAGEsetV matid subid  "bump texture" (texturesfolder + tempNormMap)	
			)
			
			else 	(
				temp1 = "No TGA found for " + expno as string + " - OK if this is a dummy"
				messagebox temp1
			)


		)
	)
	
) -- End Function	








-- ***************************************************************************************************************************************************
-- ***************************************************************************************************************************************************
-- ***************************************************************************************************************************************************
-- SCRIPT BODY STARTS HERE --




-- Now get all the TGA files	
thefiles = getfiles texturesfolderfiles	

for f in thefiles do	
(
	append tgafiles (uppercase(getfilenamefile f))	
)


-- The names of the slots are always the same.
GTAcomponentnames =#("Head", "Upper", "Lower", "Suse", "Sus2", "Hands", "Feet", "Hair 0", "Hair 1", "Hair 2", "Hair 3", "Hair 4", "Hair 5","","Teeth")
Agentcomponentnames =#("Head", "Upper", "Lower", "Accs", "Task", "Hands", "Feet", "Hair 0", "Hair 1", "Hair 2", "Pal-Dec", "Pal-Dec", "Pal-Dec","Pal-Dec","Teeth")

GTA4componenttypes = #("HEAD","UPPR","LOWR","SUSE","SUS2","HAND","FEET","HAIR","TEEF")
GTA4componenttypeSLOT = #(1,2,3,4,5,6,7,8,15)
Agentcomponenttypes = #("HEAD","UPPR","LOWR","ACCS","TASK","HAND","FEET","HAIR","DECL","TEEF")
AgentcomponenttypeSLOT = #(1,2,3,4,5,6,7,8,14,15)


-- Use an array to store each component type
-- Empty them all
arHead =#()
arUppr =#()
arLowr =#()
arSuse =#()
arSus2 =#()
arAccs =#()
arTask =#()
arFeet =#()
arHand =#()
arHair =#()
arDecl =#()
arTeef =#()

-- Now assign them to the correct project
GTA4componentArrays = #(arHead,arUppr,arLowr,arSuse,arSus2,arHand,arFeet,arHair,arTeef)
AgentcomponentArrays = #(arHead,arUppr,arLowr,arAccs,arTask,arHand,arFeet,arHair,arDecl,arTeef)
	



-- ***************************************************************
-- Create the base rage shader material 


-- Where to build it in the material editor
-- Some default settings
maxshaderslot =1
RageShaderslot = 13

shadernameprefix = "Rage00"
shadernamevalue=0
normStrnVal = 1.0
specStrnVal = 0.2
specFallVal = 35.0

shadertype = "gta_ped.fx"



componentnames = GTAcomponentnames
componenttypes = GTA4componenttypes
componentArrays = GTA4componentArrays
componenttypeSLOT = GTA4componenttypeSLOT


-- But the newer engine uses different shaders and settings, so lets set that automatically
-- Figure out the project
theproject= lowercase (rsconfiggetprojectname())
	
-- Apply custom settings	
if theproject == "jimmy" then 
( 	
	componentnames = Agentcomponentnames 
	componenttypes = Agentcomponenttypes
	componentArrays = AgentcomponentArrays
	componenttypeSLOT = AgentcomponenttypeSLOT
	
	shadertype = "ped.sps"
	specStrnVal = 0.04
	specFallVal = 40
	normStrnVal = 1.2
	
)




-- How many sub materials do we set up?
-- It's based on the max material in slot maxshaderslot 
howmanysubs = meditMaterials[maxshaderslot].numsubs




-- Create the base RAGE shader
meditMaterials[RageShaderslot] = Multimaterial ()
meditMaterials[RageShaderslot] = Multimaterial numsubs: howmanysubs
meditMaterials[RageShaderslot].name = shadernameprefix + (shadernamevalue as string)


-- Name each slot for easier identification
for slotnum = 1 to howmanysubs do
(
	meditMaterials[RageShaderslot].names[slotnum] = componentnames[slotnum]
	meditMaterials[RageShaderslot].materialList[slotnum].name = componentnames[slotnum]
)



-- Loop through this material and set up all the textures
-- This is based on the default max material from slot 1
for matLoop = 1 to howmanysubs do
(
	-- Make the sub id into a Rage Shader
	meditMaterials[RageShaderslot].material[matLoop] = Rage_Shader ()

	-- And set it to the correct default shader for characters
	RstSetShaderName meditMaterials[RageShaderslot].materialList[matLoop] shadertype
	
	
	-- Scale should be 100% with new Rage shaders
	try(RstSetTextureScaleValue meditMaterials[RageShaderslot].materialList[matLoop]  1)catch()
	
	-- enable the lighting too
	try(RstSetLightingMode meditMaterials[RageShaderslot].materialList[matLoop]  true)catch()

	RAGEsetV RageShaderslot matLoop "bumpiness" normStrnVal
	RAGEsetV RageShaderslot matLoop "specular falloff" specFallVal
	RAGEsetV RageShaderslot matLoop "specular intensity" specStrnVal
		
	try (RAGEsetV RageShaderslot matLoop "diffuse texture" meditMaterials[maxshaderslot].materialList[matLoop].diffuseMap.filename) catch()
	try (RAGEsetV RageShaderslot matLoop "bump texture" meditMaterials[maxshaderslot].materialList[matLoop].BumpMap.filename) catch() -- Bump Map
	try (RAGEsetV RageShaderslot matLoop "bump texture" meditMaterials[maxshaderslot].materialList[matLoop].BumpMap.Normal_Map.filename) catch() -- Normal map
	try (RAGEsetV RageShaderslot matLoop "specular texture" meditMaterials[maxshaderslot].materialList[matLoop].specularLevelMap.filename) catch()
)






-- Right, we now have a base rage shader
-- Lets get arrays of all the components
allgeoNames = #()
thegeoObject = #()




-- Create an array to hold ALL the valid components
for obj in geometry do
(
	objname = uppercase obj.name
	shortname = substring objname 1 4

	for namecheck = 1 to componenttypes.count do
	(	
		thecompname = componenttypes[namecheck]
	
		-- Fill the arrays of names and the objects in a 1 to 1 relationship
		-- This will allow us to select objects by name
		if (shortname == thecompname ) then 
		(	
			append allgeoNames objname
			append thegeoObject obj
		)
	)
)




-- Now split that large array into components
-- This could be made nicer!
for x = 1 to allgeoNames.count do
(
	shortname = substring allgeoNames[x] 1 4
	
	-- find the short name in the array of component names
	for namecheck = 1 to componentarrays.count do
	(	
		thecompname = componenttypes[namecheck]
		if thecompname == shortname then
		(
			append componentarrays[namecheck] allgeoNames[x] 
		)	
	)
)




-- Sort each array and get a count of the number of components in each sub array
-- We need this know how many rage shaders we need to create
for c = 1 to componentarrays.count do
(
	sc = componentarrays[c].count
	if sc >0 then sortComponentArray componentarrays[c] componenttypes[c]
)




biggest = amax (sortarrayn)
biggestpostion =finditem sortarrayn biggest
mostusedcomponent = sortarrayl[biggestpostion] -- might not be the only component used that many times.



-- We copy the Base Rage shader until we have a new shader to cover 
-- all possible components in the scene. After that I'll replace the
-- textures in the duplicate shaders.

-- lets duplicate the base rage shader


howmany = biggest - 1
for x = 1 to howmany do
(
	-- get the name and copy the base rage shader
	thename = shadernameprefix + x as string
	meditMaterials[(RageShaderslot+x)] = copy meditMaterials[Rageshaderslot]
	meditMaterials[(RageShaderslot+x)].name = thename
	
	-- what shader are we in?
	shaderid = "00" + x as string

	-- Update each slot with the correct materials		
	for c = 1 to componenttypes.count do
	(
		if (componentarrays[c].count >= (x+1)) then replaceTextures componentarrays[c] componenttypes[c] shaderid componenttypeSLOT[c]  (RageShaderslot+x)
	)
	
)





-- ********************
-- hey kids, let's APPLY the shaders to the components in the scene

maxshader = biggest - 1

for i = 0 to maxshader do
(
	-- what component are we applying?
	copyFromShader = "00" + i as string
	applyToShader = (RageShaderslot + i)
	
	for g = 1 to allgeoNames.count do
	(
		underbarSplit = filterstring allgeoNames[g] "_"
		thecom = underbarSplit [2] as string

		if (thecom == copyFromShader) then
		(
			select thegeoObject[g]
			$.material = meditMaterials[RageShaderslot+i]
		)
	)
)


