
-- The code that does the skin fixing in in this file:
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_common.ms"
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_Joystick_Bones.ms"


-- Here we want to set up a text file to write out the functions
-- Get the max filename
currentmodelname = (filterstring (maxfilename as string) ".")[1]
facebonefile = "C:\\" + currentmodelname+".fbf"
fbdatafile=""


-- These are hardcoded, so should be moved to a datafile at some point.
-- They are currently using temporary names for the controllers.
faceBones =  #($FB_C_Brow, $FB_L_Brow, $FB_R_Brow, $FB_L_Eyelid, $FB_L_Eyeball, $FB_R_Eyelid, $FB_R_Eyeball, $FB_C_Cheeks, $FB_C_Jaw, $FB_L_LipUpper, $FB_L_Crn_Mouth, $FB_L_LipLower, $FB_R_LipUpper, $FB_R_Crn_Mouth, $FB_R_LipLower)
faceControls = #($'JS_Circle_Central Eyebrow', $'JS_Circle_Left Eyebrow', $'JS_Circle_Right Eyebrow', $'JS_Circle_Left Eyelid', $'JS_Circle_Left Eye', $'JS_Circle_Right Eyelid',$'JS_Circle_Right Eye', $'JS_Circle_Cheeks', $'JS_Circle_Jaw', $'JS_Circle_Left Lip Upper', $'JS_Circle_Left Mouth Corner',$'JS_Circle_Left Lip Lower',$'JS_Circle_Right Lip Upper', $'JS_Circle_Right Mouth Corner', $'JS_Circle_Right Lip Lower', $'JS_Circle_TRANS Jaw', $'JS_Circle_TRANS BF Jaw',$'JS_Circle_TRANS Left Lip Upper', $'JS_Circle_TRANS Left Lip Lower',$'JS_Circle_TRANS BF Left Lip Upper', $'JS_Circle_TRANS BF Left Lip Lower', $'JS_Circle_TRANS Right Lip Upper', $'JS_Circle_TRANS Right Lip Lower', $'JS_Circle_TRANS BF Right Lip Upper', $'JS_Circle_TRANS BF Right Lip Lower')
faceBoneNames =  #("C_Brow", "L_Brow", "R_Brow", "L_Eyelid", "L_Eyeball", "R_Eyelid", "R_Eyeball", "C_Cheeks", "C_Jaw", "L_LipUpper", "L_Crn_Mouth", "L_LipLower", "R_LipUpper", "R_Crn_Mouth", "R_LipLower")
faceControlNames = #("Central Eyebrow", "Left Eyebrow", "Right Eyebrow", "Left Eyelid", "Left Eye", "Right Eyelid","Right Eye", "Cheeks", "Jaw", "Left Lip Upper", "Left Mouth Corner","Left Lip Lower","Right Lip Upper", "Right Mouth Corner", "Right Lip Lower", "TRANS Jaw", "TRANS BF Jaw","TRANS Left Lip Upper", "TRANS Left Lip Lower","TRANS BF Left Lip Upper", "TRANS BF Left Lip Lower", "TRANS Right Lip Upper", "TRANS Right Lip Lower", "TRANS BF Right Lip Upper", "TRANS BF Right Lip Lower")

initialdotproduct=#()




rollout blank "Facial UI"
(
	-- Build the User Interface
	button grabcontroller  "G" pos:[10,29] width:20 height:20
	dropdownlist controlList "Controller" items:#("") width:140  pos:[40,10] 
	label lm "" pos:[160,30] 
	radiobuttons controlAxis labels:#("Left/Right", "Up/Down") pos:[190,30] 
	

	button grabbone  "G" pos:[10,89] width:20 height:20
	dropdownlist boneList "Bone" items:#("") width:140 pos:[40,70]
	checkbox controlFlip "Invert controller effect" pos:[190,90] 
	
	
	label instr1 "You can connect a controller to rotate or translate a bone. Most bones SHOULD NOT TRANSLATE." width:290 height:60  pos:[10,130] 
	
	groupBox grp1 "Rotation (Local degrees)" pos:[10,170] width:145 height:150 
	groupBox grp2 "Translation (centimetres)" pos:[180,170] width:145 height:150
	
	radiobuttons RboneAxis labels:#("Spin (x)", "Left/Right (y)", "Up/Down (z)") pos:[30,190] default:2 columns:1
	spinner angleLimit "Angle Limit " range:[0,90,30] type:#integer pos:[70,250]  width:60
	button connectBoneRot "Rotation Connect" pos:[30,275] width:100 height:40
	
	radiobuttons TboneAxis labels:#("Up/Down (x)", "Back/Front (y)", "Left/Right (z)") pos:[200,190] default:1 columns:1
	spinner transLimit "Move Limit " range:[0,3,1] type:#float pos:[240,250]  width:60
	button connectBoneTrans "Translation Connect" pos:[200,275] width:110 height:40
		
		
		
	button BresetControllers "Re-centre Controllers" pos:[10,330] width:145 height:25
	button BunwireController "Unwire selected" pos:[180,330] width:145 height:25
	

	on blank open do
	(
		-- populate the dropdown boxes
		-- Use 'English' names and not actual max object names
		boneList.items = faceBoneNames
		controlList.items = faceControlNames
		
		-- Fill the dotproduct array
		for i = 1 to faceBones.count do
		(
			wiredvector = in coordsys world facebones[i].transform.row2
			wdp = dot [0,-1,0] wiredvector -- Shouldn't be more than 0.0
			append initialdotproduct wdp
		)
		
			
		-- Try to open facebonefile from c:\
		-- If fail, create a new one.

		try 
		(
			fbdatafile = openfile facebonefile mode:"a"
			print "found old file"
			
		) 
		catch 
		(
			fbdatafile = createfile facebonefile
			print "opening new file"
		)
	
		--clearListener()
		
	)

	
	
	
	on blank close do
	(
		close fbdatafile
	)
	
	
	on grabcontroller pressed do 
	(
		-- Let the user pick the controller by selecting it in the scene
		selectedobj = $
		
		-- Is it a circle?
		if classof selectedobj == Circle then
		(
			-- Yup, it's a circle
			-- Match it's name to the array.
			cn = selectedobj.name
			matchpos = 0
			for i = 1 to facecontrols.count do
			(
				if facecontrols[i].name == cn then matchpos = i
			)
			
			-- Set the dropdown box to be the controller.
			if matchpos > 0 then 
			(
				controlList.selection = matchpos
			)
		)
	)



	on grabbone pressed do 
	(
		-- Let the user pick the controller by selecting it in the scene
		selectedobj = $
		
		-- Is it a bone
		if classof selectedobj == BoneGeometry then
		(
			-- Yup, it's a bone
			-- Match it's name to the array.
			bn = selectedobj.name
			matchpos = 0
			for i = 1 to facebones.count do
			(
				if facebones[i].name == bn then matchpos = i
			)
			
			-- Set the dropdown box to be the controller.
			if matchpos > 0 then 
			(
				boneList.selection = matchpos
			)
		)
	)


	
	on BresetControllers pressed do
	(
		resetFaceControllers faceControls
	)
	
	
	on BunwireController pressed do
	(
		unwireController()
	)
	
	
	on connectBoneRot pressed do
	(
		-- use the fucntion to perform the wiring.

		theBoneMaxName = faceBones[boneList.selection]
		theControlMaxName =faceControls[controlList.selection]

		flipvalue = controlFlip.state
		flipit = 1 -- multiply by 1
		if flipvalue == true then flipit=-1 -- multiply by -1

		resetFaceControllers faceControls -- just to be sure
		
		-- Perform the wire
		wireRot_control_bone theControlMaxName controlAxis.state nrml theBoneMaxName RboneAxis.state angleLimit.value flipit 
		
		-- Build a text string of the command line used, for C&P into a script later.
		textwire = 	"wireRot_control_bone $'" +  theControlMaxName.name as string + "' " + controlAxis.state as string + " " + nrml as string + " $'" + theBoneMaxName.name as string + "' " + RboneAxis.state as string + " " + angleLimit.value as string + " " +  flipit as string
		print textwire
		
		print textwire to: fbdatafile
	
	)	
	
	
	on connectBoneTrans pressed do
	(
		-- use the fucntion to perform the wiring. 

		theBoneMaxName = faceBones[boneList.selection]
		theControlMaxName =faceControls[controlList.selection]

		flipvalue = controlFlip.state
		flipit = 1 -- multiply by 1
		if flipvalue == true then flipit=-1 -- multiply by -1

		gtascale = 100
		theDistance = transLimit.value as float
		theDistance = theDistance/gtascale
		
		resetFaceControllers faceControls -- just to be sure
		
		-- Perform the wire
		wireTrans_control_bone theControlMaxName controlAxis.state nrml theBoneMaxName TboneAxis.state theDistance flipit 
		
		-- Write out the command as text so that it can be copied into a script.
		textwire = 	"wireRot_control_bone $'" +  theControlMaxName.name as string + "' " + controlAxis.state as string + " " + nrml as string + " $'" + theBoneMaxName.name as string + "' " + TboneAxis.state as string + " " + theDistance as string + " " +  flipit as string
		print textwire
		
		print textwire to: fbdatafile
	)	
	
	
)






-- Create floater 
theNewFloater = newRolloutFloater "Wire controllers to face bones" 350 400

-- Add the rollout section
addRollout blank theNewFloater
