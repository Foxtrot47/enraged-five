-- Title
-- Wiremaker
-- Author
-- Stuart Macdonald
-- version
-- 1.0 11/11/10
-- notes
--

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

	global thisFloat
	global vertPosList = #()

	rollout Roll_wiremaker "Wire Maker"
	(
		checkbox ch_multiwire "multiwire" enabled:true checked:false
		checkbox ch_addSag "add Sag" enabled:true checked:false
		label lb_sag  "sag %"
		spinner sp_sagAmount "" align:#right width:60 range:[0.0,100.00,0.0] type:#float
		button btn_go "go"
		
		fn makeWireSpline theSpline =
		(
			
				for n = 1 to vertPosList.count do
				(
					
					addKnot theSpline 1 #corner #line vertPosList[n]
					
				)
			
		)
		
		on btn_go pressed do
		(
			vertPosList = #()
			selset = getCurrentSelection()
			
			if ch_multiwire.checked == false then
			(
				-- Just make single line connecting all verts
				for n = 1 to selset.count do
				(
					
					objSelVerts = (polyop.getVertSelection selset[n]) as array
					
					for v = 1 to objSelVerts.count do
					(
						vPos = polyop.getVert selset[n] objSelVerts[v]
						append vertPosList vPos
						
					)
					
				)
				--format "vPosList:% \n" vertPosList
				
				wireSpline = SplineShape pos:vertPosList[1]
				addNewSpline wireSpline
				
				makeWireSpline wireSpline
				
				updateShape wireSpline
				
				--add sag
					if ch_addSag.checked == true then
					(
						numSegs = vertPosList.count-1
						for s = 1 to numSegs do
						(
							
							refineSegment wireSpline 1 ((s * 2)-1) 0.5
							
							setKnotType wireSpline 1 ((s*2)-1) #corner
							setKnotType wireSpline 1 ((s*2)+1) #corner
							
							--sag bezier knot down
							kPos = getKnotPoint wireSpline 1 (s*2)
							kIVPos = getInVec wireSpline 1 (s*2)
							kOVPos = getOutVec wireSpline 1 (s*2)
							
							segLengths = getSegLengths wireSpline 1 
							numSegs = numSegments wireSpline 1
							format "segLen:% \n" segLengths
							segIndex = (s*2) + numSegs
							kOffset = (segLengths[segIndex] * (sp_sagAmount.value/100))
							format "kOffset:% \n" kOffset
							kPos[3] = kPos[3] - kOffset
							kIVPos[3] = kIVPos[3] - kOffset
							kOVPos[3] = kOVPos[3] - kOffset
							setKnotPoint wireSpline 1 (s*2) kPos
							setInVec wireSpline 1 (s*2) kIVPos
							setOutVec wireSpline 1 (s*2) kOVPos
						)
						
					)
					
			)
			else
			(
				--Multiple lines, connecting nth selected vert in all objects
				--Get number of splines count
				objSelVerts = (polyop.getVertSelection selset[1]) as array
				wireCount = objSelVerts.count
				
				for w = 1 to wireCount do
				(
					--clear vert store
					vertPosList = #()
					
					for n = 1 to selset.count do
					(
						
						objSelVerts = (polyop.getVertSelection selset[n]) as array
						
						
						vPos = polyop.getVert selset[n] objSelVerts[w]
						append vertPosList vPos
							
						
						
					)
					--format "vPosList:% \n" vertPosList
					
					wireSpline = SplineShape pos:vertPosList[1]
					addNewSpline wireSpline
					
					makeWireSpline wireSpline
					
					updateShape wireSpline
					
					--add sag
					if ch_addSag.checked == true then
					(
						numSegs = vertPosList.count-1
						for s = 1 to numSegs do
						(
							
							refineSegment wireSpline 1 ((s * 2)-1) 0.5
							
							setKnotType wireSpline 1 ((s*2)-1) #corner
							setKnotType wireSpline 1 ((s*2)+1) #corner
							
							--sag bezier knot down
							kPos = getKnotPoint wireSpline 1 (s*2)
							kIVPos = getInVec wireSpline 1 (s*2)
							kOVPos = getOutVec wireSpline 1 (s*2)
							
							segLengths = getSegLengths wireSpline 1 
							numSegs = numSegments wireSpline 1
							format "segLen:% \n" segLengths
							segIndex = (s*2) + numSegs
							kOffset = (segLengths[segIndex] * (sp_sagAmount.value/100))
							format "kOffset:% \n" kOffset
							kPos[3] = kPos[3] - kOffset
							kIVPos[3] = kIVPos[3] - kOffset
							kOVPos[3] = kOVPos[3] - kOffset
							setKnotPoint wireSpline 1 (s*2) kPos
							setInVec wireSpline 1 (s*2) kIVPos
							setOutVec wireSpline 1 (s*2) kOVPos
						)
						
					)
					
					
				)
				
			)
			
		)
	
	
	)
	
	-- rollout and floater generation
	if thisFloat != undefined then closerolloutfloater thisFloat
	thisFloat = newrolloutfloater "Wiremaker" 100 150 100 100
	addrollout Roll_wiremaker thisFloat


