-- creates a dummy, links the components

--whats the name of the character?
thename = maxfilename as string

shortname = filterstring thename "."

thedummyname = shortname[1] + "-L"

Dummy pos:[0,0,4] isSelected:on
scale $ [0.1,0.1,0.1]

ResetTransform $

$.name = thedummyname
