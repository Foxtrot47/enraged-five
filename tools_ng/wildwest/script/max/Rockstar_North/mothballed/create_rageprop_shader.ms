-- Create Rage Prop Shaders
-- Parses a ped with max shaders to make Rage Versions
-- Rockstar North
-- Rick Stirling


-- Load the common fucntions file
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_common.ms"
filein "X:/tools/dcc/current/max2009/scripts/character/includes/FN_Rage_Shaders.ms"
tgafiles = #()	



-- *************************************************
-- SCRIPT BODY STARTS HERE --
-- this script SIMPLE converts existing max materials to rage shaders.

p = lowercase (rsconfiggetprojectname())


-- Where to build it in the material editor
-- Some default settings
hatshaderslot = 6
eyeshaderslot = 7
tempshaderslot = 24
normStrnVal = 1.0
specStrnVal = 0.2
specFallVal = 35.0


-- Set a default shadertype
shadertype = "gta_ped.fx"
shadertypea = "gta_ped_alpha.fx"


if p == "gta_e1" then 
( 	
	shadertype = "gta_ped.fx"
	shadertypea = "gta_ped_alpha.fx"
)

if p == "jimmy" then 
( 	
	shadertype = "ped.sps"
	shadertypea = "ped_alpha.sps"
)

-- Do the hats first.
shaderslot = hatshaderslot

howmanysubs = meditMaterials[hatshaderslot].numsubs
meditMaterials[tempshaderslot] = Multimaterial ()
meditMaterials[tempshaderslot] = Multimaterial numsubs: howmanysubs
	
for matloop = 1 to howmanysubs do
(
	meditMaterials[tempshaderslot].name = "RAGE_HATS"
	meditMaterials[tempshaderslot].material[matloop] = Rage_Shader () 
	RstSetShaderName meditMaterials[tempshaderslot].materialList[matLoop] shadertype
	
	Dtex =""
	Ntex =""
	Stex =""
	
	-- Grab the filenames of the textures from the standard material 
	try (
		DTex = meditMaterials[hatshaderslot].materialList[matloop].diffuseMap.filename
	) catch (messagebox "Warning - No Diffuse Texture in the source material")	
			
	try (		
		STex = meditMaterials[hatshaderslot].materialList[matloop].specularLevelMap.filename
	) catch (messagebox "Warning - No Specular Texture in the source material")	
			
	try (		
		NTex = meditMaterials[hatshaderslot].materialList[matloop].BumpMap.filename
	) catch (messagebox "Warning - No Normalmap Texture in the source material")
			
	
	-- We need to check to see what slots in the rage shader these go into
	thecorrectslots = RAGEslots tempshaderslot 1
		
	-- Now we need to populate the shaders with the textures
	-- diffuse, normal, specular, relect maps then normstr, specstr, specfall, reflstr, SPARE, SPARE
	try (RAGEset tempshaderslot matLoop "diffuse" DTex) catch()
	try (RAGEset tempshaderslot matLoop "specular" STex) catch()
	try (RAGEset tempshaderslot matLoop "normal" NTex) catch()
	
	RstSetVariable meditMaterials[tempshaderslot].materialList[matLoop] thecorrectslots[5] normStrnVal
	RstSetVariable meditMaterials[tempshaderslot].materialList[matLoop] thecorrectslots[7] specFallVal
	RstSetVariable meditMaterials[tempshaderslot].materialList[matLoop] thecorrectslots[6] specStrnVal
	
)	-- End of creation loop



-- Now copy the new shader back over the original texture.
meditMaterials[hatshaderslot] = copy meditMaterials[tempshaderslot]

-- Need to reapply this to the hats.

try 
(
	select $p_head*
	$.material = meditMaterials[hatshaderslot]
) catch ()

clearSelection()


	
	
-- The eyes next, slightly different
-- We'll create a shader for eye prop
eyearray = #()
try 
(
	select $p_eyes*
	eyearray = selection as array
	ec= eyearray.count
	shaderslot = eyeshaderslot
	
	for eyeloop = 0 to (ec-1) do
	(
		meditMaterials[tempshaderslot] = Multimaterial ()
		meditMaterials[tempshaderslot] = Multimaterial numsubs: 2
		
		-- We've create a shader with two slots
		-- Set everything to default
		for matloop = 1 to 2 do
		(
			meditMaterials[tempshaderslot].name = "RAGE_EYES"	
			meditMaterials[tempshaderslot].material[matloop] = Rage_Shader () 
			meditMaterials[tempshaderslot].names[matloop] = "Frames"
			RstSetShaderName meditMaterials[tempshaderslot].materialList[matLoop] shadertype
			
			Dtex =""
			Ntex =""
			Stex =""
			
			grabfrom = eyeshaderslot + eyeloop

			
			-- Grab the filenames of the textures from the standard material 
			try (
				DTex = meditMaterials[grabfrom].materialList[matloop].diffuseMap.filename
			) catch (messagebox "Warning - No Diffuse Texture in the source material")	
					
			try (		
				STex = meditMaterials[grabfrom].materialList[matloop].specularLevelMap.filename
			) catch (messagebox "Warning - No Specular Texture in the source material")	
					
			try (		
				NTex = meditMaterials[grabfrom].materialList[matloop].BumpMap.filename
			) catch (messagebox "Warning - No Normalmap Texture in the source material")
					
			
			-- We need to check to see what slots in the rage shader these go into
			thecorrectslots = RAGEslots tempshaderslot 1
				
			-- Now we need to populate the shaders with the textures
			-- diffuse, normal, specular, relect maps then normstr, specstr, specfall, reflstr, SPARE, SPARE
			try (RAGEset tempshaderslot matLoop "diffuse" DTex) catch()
			try (RAGEset tempshaderslot matLoop "specular" STex) catch()
			try (RAGEset tempshaderslot matLoop "normal" NTex) catch()
			
			try (RstSetVariable meditMaterials[tempshaderslot].materialList[matLoop] thecorrectslots[5] normStrnVal) catch()
			try (RstSetVariable meditMaterials[tempshaderslot].materialList[matLoop] thecorrectslots[7] specFallVal) catch()
			try (RstSetVariable meditMaterials[tempshaderslot].materialList[matLoop] thecorrectslots[6] specStrnVal) catch() 

		) -- End of matloop
		
		
		-- Set slot 2 to alpha
		meditMaterials[tempshaderslot].names[2] = "Lenses"
		RstSetShaderName meditMaterials[tempshaderslot].materialList[2] shadertypea

		-- Copy the new rage shader over the original material
		print eyeloop
		meditMaterials[(eyeshaderslot + eyeloop)] = copy meditMaterials[tempshaderslot]
		
		-- Now select the glasses and apply the shader
		theeyeprop = getNodeByName ("p_eyes_00" + (eyeloop as string))
		select theeyeprop
		$.material = meditMaterials[(eyeshaderslot + eyeloop)]
		
	) -- End of eyeprop loop	


		
) catch ()

	
	


