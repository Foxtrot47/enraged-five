-- unrage materials
-- Rick Stirling


-- all components to max material
fn fnallcom =
(
	-- copy 13 to 19 etc - offset by 6 mat slots
	-- we need to do this for all the shaders that are rage
	-- we might as well cheat and do this by name
	
	biggest = 0

	for i = 13 to 18 do
	(
		-- What material are we looking at?
		msrc = i 

		mname = meditMaterials[msrc].name 
		mnamesh = substring mname 1 6 

		if (mnamesh == "Rage00") then ( 
			
			biggest = biggest +1
			
			-- This is a rage shader, so convert it.
			mdest = msrc+6
						
			meditMaterials[mdest] = Multimaterial () 
			meditMaterials[mdest].name = i as string
		
			-- Name each slot
			meditMaterials[mdest].names[1] = "Head"
			meditMaterials[mdest].names[2] = "Upper"
			meditMaterials[mdest].names[3] = "Lower"
			meditMaterials[mdest].names[4] = "Special"
			meditMaterials[mdest].names[5] = "Special"
			meditMaterials[mdest].names[6] = "Hands"
			meditMaterials[mdest].names[7] = "Feet"
			meditMaterials[mdest].names[8] = "Hair_a"
			meditMaterials[mdest].names[9] = "Hair_b"
			meditMaterials[mdest].names[10] = "Hair_c"
			meditMaterials[mdest].names[11] = "Hair_d"
			meditMaterials[mdest].names[12] = "Hair_e"
			meditMaterials[mdest].names[13] = "Hair_f"
			meditMaterials[mdest].names[14] = ""
			meditMaterials[mdest].names[15] = "Teeth"
			

			-- now copy the textures from src to dest
			howmany = meditMaterials[msrc].numsubs

			for subid = 1 to howmany do
			(
				-- try and get the textures
				try (dtex = RstGetVariable meditMaterials[msrc].materialList[subid] 1) catch()
				try (stex = RstGetVariable meditMaterials[msrc].materialList[subid] 6) catch()
				try (ntex = RstGetVariable meditMaterials[msrc].materialList[subid] 2) catch()
				
				-- try to set them
				
				try 
				(
					meditMaterials[mdest].materialList[subid].diffuseMap = Bitmaptexture fileName:dtex
					meditMaterials[mdest].materialList[subid].diffuseMap.alphaSource = 2
				) catch()
				
				
				try 
				(
					meditMaterials[mdest].materialList[subid].specularLevelMap = Bitmaptexture fileName:stex
					meditMaterials[mdest].materialList[subid].specularLevelMap.alphaSource = 2
				) catch()
				
				
				try 
				(
					meditMaterials[mdest].materialList[subid].bumpMapAmount = 100
					meditMaterials[mdest].materialList[subid].bumpMap = Bitmaptexture fileName:ntex
					meditMaterials[mdest].materialList[subid].bumpMap.alphaSource = 2
				) catch()
					
			) -- all textures copied and pasted

		) -- all rage materials parsed
		

		
	) -- end the loop that does each matslot


)



fn fnprop =
(

	biggest = 0

	for i = 6 to 12 do
	(
		-- What material are we looking at?
		msrc = i 

		mname = meditMaterials[msrc].name 
		mnamesh = substring mname 1 4

		if (mnamesh == "RAGE") then ( 
			
			biggest = biggest +1
			
			-- This is a rage shader, so convert it.
			mdest = 24
						
			meditMaterials[mdest] = Multimaterial () 
			meditMaterials[mdest].name =mnamesh as string
		
			howmanysubs = meditMaterials[i].numsubs
			
			meditMaterials[mdest].numsubs = howmanysubs 
			
			for ns = 1 to howmany do
			(
				meditMaterials[mdest].names[ns] = meditMaterials[i].names[ns] 
			)
			

			-- now copy the textures from src to dest
			howmany = meditMaterials[msrc].numsubs

			for subid = 1 to howmany do
			(
				-- try and get the textures
				try (dtex = RstGetVariable meditMaterials[msrc].materialList[subid] 1) catch()
				try (stex = RstGetVariable meditMaterials[msrc].materialList[subid] 6) catch()
				try (ntex = RstGetVariable meditMaterials[msrc].materialList[subid] 2) catch()
				
				-- try to set them
				
				try 
				(
					meditMaterials[mdest].materialList[subid].diffuseMap = Bitmaptexture fileName:dtex
					meditMaterials[mdest].materialList[subid].diffuseMap.alphaSource = 2
				) catch()
				
				
				try 
				(
					meditMaterials[mdest].materialList[subid].specularLevelMap = Bitmaptexture fileName:stex
					meditMaterials[mdest].materialList[subid].specularLevelMap.alphaSource = 2
				) catch()
				
				
				try 
				(
					meditMaterials[mdest].materialList[subid].bumpMapAmount = 100
					meditMaterials[mdest].materialList[subid].bumpMap = Bitmaptexture fileName:ntex
					meditMaterials[mdest].materialList[subid].bumpMap.alphaSource = 2
				) catch()
					
			) -- all textures copied and pasted

		) -- all rage materials parsed
		

		
	) -- end the loop that does each matslot


)





-- ********************************************************************************************
-- Rollouts
-- ********************************************************************************************


rollout unrage "Rockstar Character Tools"
(

	button allcom "Unrage All components" pos:[10,10] width:180 height:30
	button selprop "Unrage selected prop" pos:[10,50] width:180 height:30


	on allcom pressed do 
	(
		fnallcom()
	)	

	on selprop pressed do 
	(
		fnprop()
	)	
	


)






-- Create floater 
theNewFloater = newRolloutFloater "Unrage" 210 500

-- Add the rollout section
addRollout unrage theNewFloater
