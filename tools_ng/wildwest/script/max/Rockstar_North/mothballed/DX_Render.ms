-- This script will create renders for your character and store them in the right place
-- Rick Stirling, Summer 2005
-- Take me to the bar and tell me all the drinks are free

-- Get the current scene information
-- Figure out where you need to save the files and what to call them



sLT = ""
sLL = ""
sAC = ""
sBC = ""


rollout blank "Character renderer"
(

	-- The UI
	radioButtons rdrendervar "Variation" pos:[220,10] width:35 height:170 enabled:true labels:#("_a", "_b", "_c", "_d", "_e", "_f", "_g", "_h") default:1 columns:1
	button duptexture "Set up material in slot 24" pos:[10,10] width:150 height:35
	
	dropdownlist plinthType "Level" items:#("Alpine", "City", "Desert", "Space", "Underwater") width:160  pos:[10,50]	
	
	radioButtons renderQuality "Render Quality" pos:[10,110] width:35 height:170 enabled:true labels:#("Simple", "Final", "Wire", "Test") default:1 columns:23
	button renderit "RENDER BUTTON" pos:[10,150] width:150 height:50
	button deletelights "Delete the Lights and stuff" pos:[10,210] width:150 height:35
	
	

	
	-- Make the normal bump material
	on duptexture pressed do 
	(
		meditMaterials[24] = copy meditMaterials[1]
		meditMaterials[24].name = "copy"
		
		for x = 1 to meditMaterials[1].numsubs do 
		(
			meditMaterials[24].materialList[x].specularLevelMapEnable = on
			try 
			(
				thetex = meditMaterials[24].materialList[x].bumpMap.filename
				meditMaterials[24].materialList[x].bumpMap = Normal_Bump ()
				meditMaterials[24].materialList[x].bumpmap.normal_map = Bitmaptexture fileName: thetex
				meditMaterials[24].materialList[x].bumpmap.mult_spin=1.0
			) catch ()
		)

	)
	
	
	
	-- delete the lights
	on deletelights pressed do 
	(
		clearSelection()
		try(
			select $LCA*
			max delete
				
			-- Reset the scene light info
			lightTintColor =sLT
			lightLevel = sLL
			ambientColor =sAC
			backgroundColor = sBC 
			
		) catch ()
		
		environmentMap = undefined
	)
	
	
	
	-- the main bit
	on renderit pressed do 
	(
		-- We might need to store selected nodes.
		na = #()
		nsc = selection.count

		if nsc > 1 then na = $ as array else append na $
		
		try 
		(
			-- Hate those particle view things
			delete $particleview01
		)
		catch()
			
		try 
		(
			--is there a plint in the scene?
			delete $base_plinth
		)
		catch()
		
		
		-- save scene lighting information
		sLT = lightTintColor
		sLL = lightLevel
		sAC = ambientColor
		sBC = backgroundColor

		thePath = ""
		try 
		(
			thePath = "C:\\ModelRenders\\"
			makedir thepath
		) catch ()

		
		-- do we use a user driver rendername?
		rendername = getFilenameFile maxfilename
		renderbase = thePath + rendername

		-- do we append a letter?
		varletter = #("_a", "_b", "_c", "_d", "_e", "_f", "_g", "_h")
		thevarpos = rdrendervar.state		
		thevar = varletter[thevarpos]

		renderbase = renderbase + thevar

		clearSelection()
		
		try(
			select $LCA*
			max delete
		) catch ()

		theBase="x:\\jimmy\\tools\\dcc\\current\\max2009\\scripts\\character\\Assets\\renderlights.max"
		mergemaxfile theBase

		
		-- Merge in a plinth
		-- Set default to City
		thePlinth=""
		
		-- Read the value from the combo box
		selectedplinth = plinthtype.selected
		
		-- Select the correct plinth
		case selectedplinth of 
		(
			"Alpine": thePlinth="x:\\jimmy\\tools\\dcc\\current\\max2009\\scripts\\character\\Assets\\Base_Plinths\\Alpine\\base plinth.max"
			"City": thePlinth="x:\\jimmy\\tools\\dcc\\current\\max2009\\scripts\\character\\Assets\\Base_Plinths\\GTA\\base plinth.max"
			"Desert": thePlinth="x:\\jimmy\\tools\\dcc\\current\\max2009\\scripts\\character\\Assets\\Base_Plinths\\Desert\\base plinth.max"
			"Space": thePlinth="x:\\jimmy\\tools\\dcc\\current\\max2009\\scripts\\character\\Assets\\Base_Plinths\\Space\\base plinth.max"
			"Underwater": thePlinth="x:\\jimmy\\tools\\dcc\\current\\max2009\\scripts\\character\\Assets\\Base_Plinths\\Underwater\\base plinth.max"
		)
		
		mergemaxfile thePlinth
		
		
		
		-- Set the environment lighting for daytime
		lightTintColor = color 255 255 255
		lightLevel = 1.0
		ambientColor = color 0 0 0
		backgroundColor = color 60 60 60
		
		environmentMap = bitmapTexture filename:"x:\\jimmy\\tools\\dcc\\current\\max2009\\scripts\\character\\Assets\\render_background.bmp"
		
		-- Switch off exposure controls
		try (
			SceneExposureControl.exposureControl = undefined
		) catch ()
		
		-- Select camera
		max vpt camera
		
		scanlineRender.antiAliasFilter = catmull_rom()
		

		-- Exclude the plinth
		$LCAFDirect01.excludelist = #($base_plinth)
		$LCAFDirect02.excludelist = #($base_plinth)
		
		-- Set up any lighting/render things.
		if (renderquality.state == 1) then 
		(
			$LCAambient1.on = on
			$LCAambient2.on = off
			
			renderbase = renderbase + "_Qu"
		)
		
		
		-- Set up any lighting/render things.
		if (renderquality.state == 2) then 
		(
			$LCAambient1.on = off
			$LCAambient2.on = on
				
			renderbase = renderbase + "_Fi"
		)

		
		if (renderquality.state == 3) then 
		(
			$LCAambient1.on = on
			$LCAambient2.on = off
			
			renderbase = renderbase + "_Wi"
			
			meditMaterials[23].wire = on

				
			if nsc == 0 then 
			(
				messagebox "Nothing selected"
				return 0
			)
		

			maxOps.cloneNodes na cloneType:#copy newNodes:&nnl
			select nnl
			
			clones=$ as array

			sliderTime = 1f
			sliderTime = 0f
				
			modPanel.addModToSelection (Push ()) ui:on
			for pobj in selection do
			(	
				pobj.modifiers[#Push].Push_Value = 0.002
			)
				
			nnl.material = meditMaterials[23]

		)
		
		
		if (renderquality.state == 4) then 
		(
			sliderTime = 0f
			render camera:$LCACamera01 outputwidth:1280 outputheight:960 vfb:on
			return 0
		)
		
		sliderTime = 0f
		render camera:$LCACamera01 outputwidth:1280 outputheight:960 outputFile:( renderbase + "_FRONT.tga") vfb:on

		sliderTime = 3f
		render camera:$LCACamera01 outputwidth:1280 outputheight:960 outputFile:( renderbase + "_SIDE3.tga") vfb:on		
		
		sliderTime = 5f
		render camera:$LCACamera01 outputwidth:1280 outputheight:960 outputFile:( renderbase + "_SIDE5.tga") vfb:on	

		sliderTime = 10f
		render camera:$LCACamera01 outputwidth:1280 outputheight:960 outputFile:( renderbase + "_BACK.tga") vfb:on
		

		-- clear up wires
		clearSelection()
		try
		(
			select nnl
			delete $
		)
		catch()
	
	)

)

-- Create floater 
theNewFloater = newRolloutFloater "Rollout title" 310 320

-- Add the rollout section
addRollout blank theNewFloater




