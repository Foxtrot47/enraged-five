
-- load the common functions
filein "rockstar/export/settings.ms"

-- Figure out the project data
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()
theProjectPedFolder = theProjectRoot + "art/peds/"
-- Load the common fucntions file

-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_Rage_Shaders.ms")





newNormalValue = 0.75
newSpecValue = 0.4
newFresnelValue = 0.75





-- Get or set a rage shader variables
-- Pass in the material and the slot type we are looking for, eg "bumpiness", "diffuse texture"
-- Pass in a value to set, "undefined" gets ignored
fn getSetRageV MatID SubID slottype setSlotValue =(
		
	-- Arrays for storing data
	RageSlotNames =#()
	RageSlotTypes =#()
	
	-- Need to know the shadertype, this dictates the slot we need
	RageShaderName = RstGetShaderName mlib[matid].materialList[subid]
	RageShaderSlotCount = RstGetVariableCount mlib[matid].materialList[subid] 
	
	-- Populate the arrays
	for ss = 1 to RageShaderSlotCount do
	(
		append RageSlotNames (lowercase (RstGetVariableName mlib[matid].materialList[subid] ss))
		append RageSlotTypes (RstGetVariableType mlib[matid].materialList[subid] ss)
	)

	-- Now we have a mapping, lets find the index of the slottype we are interested in.
	shaderSlot = finditem RageSlotNames slottype

	returnValue = undefined
	
	if setSlotValue == undefined then (
		returnValue = RstGetVariable mlib[MatID].materialList[SubID] shaderslot SetSlotValue
	)
	
	else (
		RstSetVariable mlib[MatID].materialList[SubID] shaderslot SetSlotValue
	)
)






fn updatenormals matid= (
	
	slottype = "bumpiness"
	
	--for matid = 1 to mlib.count do(
	
		howmanysubs = mlib[matid].numsubs
		
		for subid = 1 to howmanysubs do (
			-- get the current normal value
			try (setNormalValue = getSetRageV MatID SubID slottype newNormalValue) catch()
		)	
	--)	
)


fn updatespec matid= (
	
	slottype = "specular intensity"
	
	--for matid = 1 to mlib.count do(
	
		howmanysubs = mlib[matid].numsubs
		
		for subid = 1 to howmanysubs do (
			-- get the current normal value
			try (setNormalValue = getSetRageV MatID SubID slottype newSpecValue) catch()
		)	
	--)	
)



fn updatefresnel matid= (
	
	slottype = "specular fresnel"
	
	--for matid = 1 to mlib.count do(
	
		howmanysubs = mlib[matid].numsubs
		
		for subid = 1 to howmanysubs do (
			-- get the current normal value
			try (setNormalValue = getSetRageV MatID SubID slottype newFresnelValue) catch()
		)	
	--)	
)


-- Use sceneMaterials


mlib = meditMaterials
mlib.count





-- For each selected object, fix the bumpiness
for m = 1 to 24 do (
	updatenormals m
	updatespec m
	updatefresnel m
)	




