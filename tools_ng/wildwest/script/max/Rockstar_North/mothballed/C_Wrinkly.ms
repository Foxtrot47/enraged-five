/*
Wrinkly Old Man

Stewart Wright
Rockstar North
18/10/10

Extract wrinkle data off materials and apply the data to sliders

genWrinkleData function originally by Neal Corbett - R* Leeds
FECWrinkleSlider, maxExpWrap and sliderSetup functions modified from original code by Rick Stirling - R* North
-------------------------------------------------------------------------------------------------------------------------*/
headWrinkleData = #()
missingSliders = #()
missingJS = false
suckedOff = false
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
fn genWrinkleData =
(
	if (isKindOf $.material standard) then
	(
		meditMaterials[24] = $.material --copy the selected material to slot 24 so we can read it
		local headMat = meditMaterials[24]
		local entryPrefix = "head-"
		local entryNum = 0
		
		if (isKindOf headMat standard) and (isKindOf headMat.bumpMap Normal_Bump) and (isKindOf headMat.bumpMap.normal CompositeTexturemap) do 
		(
			local compMap = headMat.bumpMap.normal
			
			for maskLayerNum = 1 to 2 do 
			(
				local maskLayerMap = compMap.mask[maskLayerNum + 1]
				
				for subMaskNum = 0 to 7 do 
				(
					local subMaskControl = maskLayerMap.opacity[subMaskNum + 1].controller
					
					if (isKindOf subMaskControl Float_List) do 
					(
						for listNum = 1 to subMaskControl.count do 
						(
							local maskControlName = subMaskControl.getName listNum
							local maskControl = getPropertyController subMaskControl maskControlName
							--local maskControlScal = exprForMAXObject (maskControl.GetScalarTarget 5 asController:true)
							local maskControlScal = maskControl.GetScalarTarget 5 asController:true
							local maskControlExpr = maskControl.GetExpression()

							--work out some stuff to get correct head numbering
							if maskLayerNum == 1 then (headCount = (subMaskNum + 0))
							if maskLayerNum == 2 then (headCount = (subMaskNum + 8))

							local itemArray = #(
														entryPrefix + (formattedPrint headCount format:"03i"), 
														maskLayerNum, 
														subMaskNum, 
														maskControlName, 
														maskControlScal, 
														maskControlExpr
														)

							append headWrinkleData itemArray
							entryNum += 1
						)
					)
				)
			)
			suckedOff = true
		)
	)
	else
	(
		msgText = $.name as string + " does not have a standard material assigned."
		messagebox msgText
		suckedOff = false
	)
	headWrinkleData
)--end genWrinkleData

-------------------------------------------------------------------------------------------------------------------------
--Check that there are 15 sliders in the scene to apply the values to
fn checkSliders =
(
	missingSliders = #()
	for x = 0 to 15 do
	(
		objName = "Head-" + (formattedPrint x format:"03i")
		actualObject = (getNodebyName objName)
		try
		(
			select actualObject
		)
		catch
		(
			append missingSliders objName
			missingJS = true
		)
	)
)--end checkSliders

-------------------------------------------------------------------------------------------------------------------------
-- IM Wrinkles to Rage wrinkles tool
fn FECWrinkleSlider theTargetNode theScalarName theControlNode theExpression =
(
	-- Set a float controller on the zero'd out Y position of the slider
	fc = theTargetNode.pos.Zero_Pos_XYZ.controller.Y_Position.controller = Float_Expression () -- ZERO'd
	-- Add a scalar target to the expression
	fc.AddScalarTarget theScalarName theControlNode
	-- Add the expression string
	fc.SetExpression theExpression
)--end FECWrinkleSlider

-------------------------------------------------------------------------------------------------------------------------
-- Takes a simple expression and normalises it
fn maxExpWrap theExpression =
(
	-- Compute the final expression
	-- IM run to 100%, we need to run normalised 0-1, hence the max and the * 0.01 code
	 wrappedexpression = "min(1,   (max  ((" + theExpression +" * 0.01),0))   )"
	
	--wrappedexpression = "(" + theExpression +" * 0.01)"
)--end maxExpWrap

-------------------------------------------------------------------------------------------------------------------------
fn sliderSetup =
(
	currentExpression = ""
	finalExpression = ""
	expressionList =#()

	for we = 1 to headWrinkleData.count do
	(
		-- Figure out what mask we are using and what the previous mask was
		try (thePreviousMask = (headWrinkleData[we-1][3])) catch (thePreviousMask = undefined)
		thecurrentMask = (headWrinkleData[we][3])

		-- We may have multiple inputs to the same slider each with a different expression
		-- Adding the inputs is easy, but each new expression will overwrite the existing one
		if thecurrentMask == thePreviousMask then (

			currentExpression = "(" + headWrinkleData[we][6] + ")"
			append expressionList currentExpression
			finalExpression = ""
			
			-- Build up the composite expression
			partialExpression = expressionList[1]
			for ce = 2 to expressionList.count do ( 
				partialExpression = partialExpression + " + " + expressionList[ce]
			)	
			partialExpression = "(" + partialExpression + ")"
			finalExpression = maxExpWrap  partialExpression
		)	
		else (
			expressionList =#()
			currentExpression = "(" + headWrinkleData[we][6] + ")"
			append expressionList currentExpression
			finalExpression = maxExpWrap currentExpression
		)	

		-- Need to figure out the target node
		thetargetnodename = headWrinkleData[we][1]
		theTargetNode = getNodeByName theTargetNodeName
		
		--FECWrinkleSlider takes 3 parameters: theScalarName theControlNode theExpression
		FECWrinkleSlider theTargetNode headWrinkleData[we][4] headWrinkleData[we][5] finalExpression 
	)
)--end sliderSetup

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--create rollout
rollout WrinklyOldMan "Wrinkly Old Man"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Ped_Wrinkle_Maps#Facial_Wrinkle_Tool" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)
	
	button btnSuckItOff "Suck It Off" toolTip:"Extract wrinkle expressions from standard max material (IM Setup)"
	button btnSpitItOut "Spit It Out" toolTip:"Set up sliders based on extracted material data"
	-------------------------------------------------------------------------------------------------------------------------
	
	on btnSuckItOff pressed do
	(
		if selection.count == 1 then
		(
			genWrinkleData()
			if suckedOff == true then
			(
				msgText = "Wrinkle expressions extracted from " + $.name as string +"."
				print "Sucked Off"
				messagebox msgText
			)else()
		)
		else
		(
			try
			(
				select $head_000_*
				if selection.count == 1 then
				(
					genWrinkleData()
					msgText = "Wrinkle expressions extracted from " + $.name as string + "."
					print "Sucked Off"
					messagebox msgText
				)else (messagebox "Unable to find the head mesh.\r\nPlease select it and try again.")
			)catch (messagebox "Unable to find the head mesh.\r\nPlease select it and try again.")
		)
	)
	
	on btnSpitItOut pressed do
	(
		if suckedOff == true then
		(
			checkSliders()
			
			if missingJS == false then
			(
			sliderSetup()
			print "Spat Out"
			messagebox "Sliders setup using extracted data."
			)
			else
			(
				msgText = "Couldn't find the following sliders in the scene: "
				for m = 1 to missingSliders.count do
				(
				msgText = msgText + "\r\n" + missingSliders[m] as string
				)
				messagebox msgText
			)
		)
		else
		(
			messagebox "Nothing to spit out.  Did you suck it off first?"
		)
	)
)--end WrinklyOldMan rollout
-------------------------------------------------------------------------------------------------------------------------

--Create the interface
createDialog WrinklyOldMan width:130