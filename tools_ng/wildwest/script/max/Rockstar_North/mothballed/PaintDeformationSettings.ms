-- Set poly paint deformation values
-- Rick Stirling August 2009


$.paintDeformValue = 0.01
$.paintDeformSize = 0.1
$.paintDeformStrength = 0.4



-- Soft Selection
$.falloff = 0.05
$.ssUseEdgeDist = on
$.ssEdgeDist = 3