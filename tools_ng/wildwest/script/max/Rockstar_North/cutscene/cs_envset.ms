--Environment Hierarchy Setting
--Mondo Ghulam

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

	
				--Unhide & unfreeze the dummy01 node
		max unhide all
		max unfreeze all
		
		--Select the original scene root
		select $'Dummy01'
		$.name = "PointOrig"
		
		--Clear the selection
		clearSelection()
		
		--Create a SetRoot Null at zero
		Point pos:[0,0,0] isSelected:on
		$.name = "SetRoot"
		
		--Clear the selection
		clearSelection()
		
		--create pointzerod null
		Point pos:[0,0,0] isSelected:on
		$.name = "PointZerod"
		
		--Select the geometry
		setSelectFilter 2 --selects geo only
		max select all
		
		--Parent Geo to PointZerod
		$.parent = $PointZerod
		
		--Return Selection filter to all
		SetSelectFilter 1
		
		--Clear the selection
		clearSelection()
		
		--Parent PointZerod to PointOrig
		select $'PointZerod'
		$.parent = $PointOrig
		
		--Parent pointOrig to SetRoot
		select $'PointOrig'
		$.Parent = $SetRoot
		
		print "allDoneNow"
	

