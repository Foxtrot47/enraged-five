
filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

------------------------------------------------------------------------------------------------------------------------------------
-- setting functions for character stripper tool
-- set string to lowercase

-- load the common functions

filein "rockstar/export/settings.ms"

 

-- Figure out the project data

theProjectRoot = RsConfigGetProjRootDir()

theProject = RSConfigGetProjectName()

theWildWest = RsConfigGetWildWestDir()

theProjectConfig = RsConfigGetProjBinConfigDir()

theProjectPedFolder = theProjectRoot + "art/peds/"


fn setasLowercase instring = (

	local upper, lower, outstring
	upper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lower="abcdefghijklmnopqrstuvwxyz" 
	
	outstring=copy instring 
		
	for i=1 to outstring.count do (
	
		j = findString upper outstring[i] 
		
		if (j != undefined) do (
		
			outstring[i] = lower[j] 
		)
	) 
	
	outstring
)

-- load common functions
--filein "X:\\tools_release\\Scripts\\Character\\common_functions.ms"


-- fix skeleton parenting for cutscenes

fn fixparenting = (

						-- Remove Biped animation	
			
							MYBIP = $char.controller
							mybip.figureMode = false
							biped.clearAllAnimation MYBIP
							mybip.figureMode = true
 		
						-- CS_Char spine and clavs correct parenting

							clearSelection()
							select $'Char Spine'
							$.parent = $'Char Pelvis'
							clearSelection()
							select $'Char L Clavicle'
							$.parent = $'Char Spine3'
							select $'Char R Clavicle'
							$.parent = $'Char Spine3'
							
					)

-- changing Rage shaders to standard shaders 
			
Fn materialfix = (

 	 
	 			for i = 1 to 24 do 
				
				
				
				(
				
						msrc = i 
						mname = meditMaterials[msrc].name 
						mnamesh = substring mname 1 6 
						
						if (mnamesh == "Rage00") then ( 
						

															howmany = meditMaterials[msrc].numsubs
															
															for subid = 1 to howmany do
															
															(
						
															-- try and get the textures (diffuse only)
															try (dtex = RstGetVariable meditMaterials[msrc].materialList[subid] 1) catch()

				
															-- try to set them by overiding the origanal rage shader
				
															try 
															(
															meditMaterials[msrc].material[subid] = Standardmaterial () 			
															meditMaterials[msrc].material[subid].diffuseMap = Bitmaptexture fileName:dtex

															) catch()
				
															) -- close the texture change loop
														
														) -- close the loop for looking for rage shaders
							
							MMClean.fixall prompt:false			-- Clean out unused slots
	
							actionMan.executeAction 0 "63508"  -- Views: Standard Display with Maps
				
				)-- close the loop for everything materials!!!
			) -- close materialfix


--End of functions

-----------------------------------------------------------------------------------------------------------------------------------

-- create rollout panel

rollout Character_Stripper "Character Stripper" width:136 height:320
(hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/cs_characterstripper" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)
	DropDownList Current_Project "Select project" pos:[8,5] width:96 height:40 items:#("GTAIV", "E1", "E2", "Jimmy", "GTAV")selection:5
	checkbox strip_char "Strip Character" pos:[8,55] width:104 height:19 checked:false 
	--checkbox rmv_face "Remove face bones" pos:[8,70] width:104 height:32 checked:false
	--checkbox rmv_Roll "Remove roll bones" pos:[8,101] width:112 height:19 checked:false
	-- removing reskin as its not used anymore
	--Button re_skin "Re-Skin tool" pos:[32,35] width:72 height:32 enabled:true toolTip:"Launch the skin weight reassigner" =
	button run_stripper "Run" pos:[32,181] width:72 height:32 enabled:true toolTip:"Starts the strip show"
	checkbox saveMax "Save as Stripped Max" pos:[8,85] width:112 height:32 checked:false
	checkbox exp_FBX "Export FBX" pos:[8,135] width:112 height:16 checked:false
    dropDownList animtestsfiles "Anim tests" pos:[22,225] width:96 height:40 items:#("None.bip", "arms.bip", "legs.bip", "hands.bip" ) selection:1
	button loadmyanim "load anim" pos:[32, 280] width:72 height:32 enabled:true toolTip:"loads selected test anim"

	
	
	on Character_Stripper open  do
(

				
				

-- finding the main character dummy 




-- unhide everything

	unhide objects
	hideByCategory.none()
	
-- checking file name to find character dummy

	Scenename = getFilenameFile maxFileName
	Scenenamelower = setasLowercase Scenename
	print Scenenamelower
	Select Helpers
	
	Helpersarray = $ as array
	helperscount = Helpersarray.count
	
	clearSelection()	
	
	for i = 1 to helperscount do 	
	(
	select Helpersarray[i]
	Print Helpersarray[i]
	Helpername = Helpersarray[i].name 
	print Helpername 
	Helpernamelower = setasLowercase Helpername 
	print Helpernamelower
	Iwant =  (Helpernamelower == Scenenamelower)
	print Iwant 
	if Iwant == true then ( exit )
	If i == helperscount and iwant == false then messageBox ("I can't find the Character Dummy that matches the file name") 
	)

Global CharDummy = $selection[1]

)-- close on character stripper open


---------------------------------------------------------------------------------------------------------


-- run selected check boxes


	on run_stripper pressed  do 	
	
			try(
			
			if Current_Project.selection == 1 then ProjectPath = "X:\\gta\\gta_art\\anim\\cutscenes\\Models"
			
			if Current_Project.selection == 2 then ProjectPath = "X:\\gta_e1\\gta_e1_art\\cutscenes\\Characters"

			if Current_Project.selection == 3 then  ProjectPath = "X:\\gta_e2\\gta_e2_art\\Cutscenes\\Characters"

			if Current_Project.selection == 4 then  ProjectPath = "X:\\jimmy\\jimmy_art\\Cutscenes\\Characters"
				
			if Current_Project.selection == 5 then  ProjectPath = "X:\\gtav\\art\\Cutscenes\\Characters"
			
			if strip_char.state == true then 	( 
				

															-- This selects and deletes unneeded parts for FBX setup, Char dummy has been found

															-- found character dummy selecting Geo, skeleton and etm nulls
					
															select CharDummy
	
															selectmore CharDummy.children

															Selectmore $Char*...*
															Selectmore $SKEL*...*
					
															selectmore $helpers
					
															deselect $co*
					
				

															-- Set geo to keep in array and deleting unwanted geo
	
															Keepthese = $ as array
	
															select objects
	
															deselect Keepthese
	
															try(deselect $Char_Footsteps)catch()	
															
															delete $
	

												-- if Current_Project.selection == 4 then false else fixparenting() 
												--	materialfix()
												  
						   						) 	
		
			--if rmv_face.state == true then fileIn "Rmvfacebone.ms"  quiet: True 

			--if rmv_Roll.state == true then fileIn "Rmvrollbone.ms"  quiet: True 
			
			if saveMax.state == true then (
			
											-- Save as Stripped Max file
	
											clearSelection()

											-- generate the naming 

	
											savename = CharDummy.name
											namecall = "stripped_"+savename
											-- save the file 
	
											saveMaxFile (maxFilePath + namecall)

											)
											
											
			if exp_fbx.state == true then (
														--if querybox "Are we exporting the model from max 2009?" beep:false then 
														
														(
																-- creating an fbx_root for export
																--Dummy pos:[0,0,0] isSelected:on size:0.05
																--$.name = "FBX_Root"
																--rotate $ (angleaxis -90 [1,0,0])
																
																-- fix parenting and offset on dummy nodes 
													
																
																
																-- parenting the char to the fbx_root
																
																 if Current_Project.selection == 4 then  (
																Try(	 
																select $Skel_root
																max unlink
																select $dummy01
																$.pos = $mover.pos
																select $mover
																$.pos = $dummy01.pos
																move $ [0,0,1]
																select $Skel_root
																$.parent = $mover
																select $mover
																$.parent = $dummy01)
																 catch(  
																Select $Char 
																MYBIP = $char.controller
																mybip.figureMode = false
																mybip.inPlaceMode = false
																mybip.figureMode = true
																		)
																
																selectmore chardummy
															
																Try( false
																	)catch(  $.parent = $FBX_Root
																				select $SKEL_ROOT 
																				$.parent =$mover
																				select $mover
																				$.parent = $FBX_Root
																				)
																

																
																-- Export to FBX
	
																clearSelection()
		
																-- Select and export

																exportname = Chardummy.name
																print exportname
																theclasses = exporterPlugin.classes
																print theclasses
																FBXclass = findItem theclasses FBXEXP
																print FBXclass
																select $*
																exportFile (ProjectPath + "\\" + exportname)  selectedOnly:true using: theclasses [FBXclass] 
																clearSelection()			
																
																-- reverting the chars state
																try(select $FBX_Root selectmore $mover delete$)catch()
																
																
																
															)
													
														else
																(
													
																-- Export to FBX
	
																clearSelection()
		
																-- Select and export

																exportname = Chardummy.name
	 	
																theclasses = exporterPlugin.classes
																FBXclass = findItem theclasses FBXEXP
																select $*
																exportFile (ProjectPath + "\\" + exportname) #noPrompt selectedOnly:true using: theclasses [FBXclass]
		
																clearSelection()			
																)
														)
			
													)
		)catch()

	
	on loadmyanim pressed do 
			
			 
			 (
					
				MYBIP = $char.controller

				mybip.figureMode = false

				biped.loadBipFile MYBIP  animtestsfiles.selected	
			
				)
	
	
	
	on re_skin pressed do
	
		(
			filein "X:\\tools_release\\Scripts\\Character\\Skinned_Vert_Swapper.ms"
			
		)
	
-- close the script	
	
	on Character_Stripper okToClose  do
(
	
	)
) -- close rollout

createDialog Character_Stripper

