--CS MaxProp Setup
--Stewart Wright
--04/11/10
-------------------------------------------------------------------------------------------------------------------------
--Tool for creating movers and dummys on selected props and exporting the lot.
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- load the common functions
filein "rockstar/export/settings.ms"

-- Figure out the project data
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

prop = undefined
-------------------------------------------------------------------------------------------------------------------------
fn buildHelpers =
(
	prop = getCurrentSelection()
	
	--take a guess at the root joint's name
	propRoot = prop[1].name + "_rootjoint"
	
	rootIRL = (getNodeByName propRoot)
	if rootIRL != undefined then
	(
		rootIRLPos = rootIRL.pos
		rootIRLRot = rootIRL.rotation
		--make point helper
		mainDummy = Point pos:rootIRLPos rot:rootIRLRot size:2 cross:on box:off centermarker:off axistripod:off
		mainDummy.name = "mover"
		print (propRoot + "'s mover built")
		
		select rootIRL
		actionMan.executeAction 0 "40194"  -- Selection: Select Child
		mainJoint = getCurrentSelection()
		
		mainDummy.parent = rootIRL
		mainJoint.parent = mainDummy
		prop.parent = rootIRL
		
		--move selected to 0,0,0 worldspace
		in coordsys world rootIRL.pos = [0,0,0]
		print "Root moved to center"
	)
	else
	(
		print (propRoot + " doesn't exist.  I'll try another name.")
		
		--take another guess at the root joint's name
		propRoot = "cs_" + propRoot
		rootIRL = (getNodeByName propRoot)
		if rootIRL != undefined then
		(
			rootIRLPos = rootIRL.pos
			rootIRLRot = rootIRL.rotation
			--make point helper
			mainDummy = Point pos:rootIRLPos rot:rootIRLRot size:2 cross:on box:off centermarker:off axistripod:off
			mainDummy.name = "mover"
			print (propRoot + "'s mover built")
			
			select rootIRL
			actionMan.executeAction 0 "40194"  -- Selection: Select Child
			mainJoint = getCurrentSelection()
			
			mainDummy.parent = rootIRL
			mainJoint.parent = mainDummy
			prop.parent = rootIRL
			
			--move selected to 0,0,0 worldspace
			in coordsys world rootIRL.pos = [0,0,0]
			print "Root moved to center"
		)
		else
		(
			print ("can't find " + prop[1].name + "'s root")
			messagebox ("Can't find " + prop[1].name + "'s root.\r\nDoes it follow the naming convention?")
		)
	)
)--end buildHelpers

-------------------------------------------------------------------------------------------------------------------------
rollout CS_MaxProp_Setup "Cutscene Payne Prop Setup"
(
	button btnPrepProp "Prepare Prop" width:100 height:50
	
	on btnPrepProp pressed do
	(
		if  selection.count != 1 then
			(
			messagebox "I can only cope with 1 thing at a time!"
			return 0
			)
		else
			(
			buildHelpers ()
			
			--hacky select children method.  needs better way				
			select prop
			
			actionMan.executeAction 0 "40193"  -- Selection: Select Ancestor.  Should be root
			actionMan.executeAction 0 "40180"  --select all children and main object
			print "Full hierarchy selected"

			--hacky export selected as method.  needs better way.
			actionMan.executeAction 0 "40373"  -- File: Export Selected
			)
	)
)--end CS_MaxProp_Setup
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
createDialog CS_MaxProp_Setup