
filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

--Environment re-set to Zero Origin
--Mondo Ghulam

		--this will help when a new scene root needs to be specified other than the one
		--provided by the map artists.
		
		--Unhide & unfreeze the dummy01 node
		max unhide all
		max unfreeze all
		
		--Select the new zero locator
		select $'PointZerod2'
		
		--Parent PointZerod2 to PointZerod
		$.parent = $PointZerod
		
		--Clear the selection
		clearSelection()
		
		--Select PointZerod
		select $'pointZerod'
		
		--Move to PointOrig
		$.pos = $'PointOrig'.pos
		
		--Select PointZerod2, we will clone while parented as we lose the parent transforms after unparenting
		select $'PointZerod2'
		max unlink $'PointZerod2'
		
	-- 	--Clone PointZerod2 and rename it PointOrig2
		select $'PointZerod2'
		maxOps.cloneNodes $ cloneType:#copy newNodes:&nnl
		select nnl
		$.name = "PointOrig2"
		
		--Reparent the new nodes
		$.parent = $'SetRoot'
		clearSelection()
		
		--parent PointZerod2 to PointOrig2
		select $'PointZerod2'
		$.parent = $'PointOrig2'
		
		clearSelection()
		
		--Select the geo and parent under PointOrig2
		setSelectFilter 2 --selects geo only
		max select all
		
		$.parent = $'PointZerod2'
		clearSelection()
		--Return Selection filter to all
		SetSelectFilter 1
		
		select $'PointZerod'
		$.parent = $'PointZerod2'
		clearSelection()
		
		select $'PointZerod2'
		$.pos = [0,0,0]
		
		print "allDoneNow"
