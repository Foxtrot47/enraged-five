--Prop Damage Tagging Script
--Stewart Wright
--Rockstar North
--03/06/10
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
-- load the common functions
filein "rockstar/export/settings.ms"

-- Figure out the project data
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/props/p_includes/FN_common.ms")


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
confState = true
taggedList = #()
damageTag = "ISDAMAGE = TRUE"
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
-- cycle through each selected object and tag as damaged
fn tagAsDamaged =
(
	toTag = getCurrentSelection ()
	
	for t = 1 to toTag.count do
	(
		--assume the mesh isn't tagged for damage already
		unTagged = true
		--get current user defined properties
		currentUDP = getUserPropbuffer toTag[t]
		--filter the udp for carrage returns and line breaks
		filterUDP = filterString currentUDP "\r\n"
		
		--check the UDP to see if the object is already tagged for damage
		for f = 1 to filterUDP.count do
		(
			tempUDP = uppercase filterUDP[f]
			if tempUDP == damageTag then
			(
				--flag this object as tagged
				unTagged = false
			)
		)
		print unTagged
		
		--if the object isn't flagged as tagged then tag it
		if unTagged == true then
		(
			oldTag = getUserPropbuffer toTag[t]
			if oldTag != "" then
			(
			newTag = oldTag + "\r\n" + damageTag
			setUserPropbuffer toTag[t] newTag
			)
			else
			(
			setUserPropbuffer toTag[t] damageTag
			)
			--print some debug info in the listener
			prInfo = (toTag[t].name + " has been tagged")
			print prInfo
			--keep track of what's been tagged
			append taggedList toTag[t].name
		)
	) -- end of for loop
) -- end tagAsDamaged
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
-- delete the damage tag on selected objects with confirmation
fn delDamageTagWC =
(
	toDel = getCurrentSelection ()
	
	for d = 1 to toDel.count do
		(
			keepUDP = #()
			currentUDP = getUserPropbuffer	toDel[d]
			filterUDP = filterString currentUDP "\r\n"
			for x = 1 to filterUDP.count do (
				tempUDP = uppercase filterUDP[x]
				if tempUDP != damageTag then
				append keepUDP tempUDP
			)
		
			msgText = "This will delete damage tags from " + toDel[d].name +".  Are you sure?"
			if queryBox msgText title:"Delete Damage Tags" beep:false then
				(
					tag = ""
					if keepUDP.count == 0 then setUserPropbuffer toDel[d] tag
					else
					(
						for x = 1 to keepUDP.count do
							(
								tag = tag + keepUDP[x] + "\r\n"
								setUserPropbuffer toDel[d] tag
							)
					)
				prInfo = (toDel[d].name + "'s tag has been removed")
				print prInfo
				)
		)--end of for objsel in selection do
) --end delDamageTagWC
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
-- delete the damage tag on selected objects with confirmation
fn delDamageTag =
(
	toDel = getCurrentSelection ()
	
	for d = 1 to toDel.count do
		(
			keepUDP = #()
			currentUDP = getUserPropbuffer	toDel[d]
			filterUDP = filterString currentUDP "\r\n"
			
			for x = 1 to filterUDP.count do
			(
				tempUDP = uppercase filterUDP[x]
				if tempUDP != damageTag then
				append keepUDP tempUDP
			)
			
			tag = ""
			
			if keepUDP.count == 0 then setUserPropbuffer toDel[d] tag
			else
			(
				for x = 1 to keepUDP.count do
					(
						tag = tag + keepUDP[x] + "\r\n"
						setUserPropbuffer toDel[d] tag
					)
			)
		)--end of for objsel in selection do
) --end delDamageTag
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
rollout tagAsDamagedTools "Tag Props As Damaged Tool"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Props_Damage_Tag_Tool" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)
	checkBox chkbxConf "Confirm On Delete" pos:[10, 22] checked:confState tooltip:"Do you want to be prompted before deleting tags?"

	button btnTagSelected "Tag Selected" width:170 height:60
	button btnDelSelected "Delete Tag From Selected" width:170 height:60
	
	on chkbxConf changed theState do
	(
		if chkbxConf.state == true then
		(
			confState = true
		)
		else
		(
			confState = false
		)
	)
	
	on btnTagSelected pressed do
	(
		if selection.count < 1 then
		(
			messagebox "Nothing Selected!"
			return 0
		)
		else
		(
			msgText = ""
			taggedList = #()
			tagAsDamaged ()
			--lets add some stuff to give the user feedback on what the script has done
			for m = 1 to taggedList.count do
				(
					msgText = msgText + "\r\n" + taggedList[m] as string
				)
				if msgText != "" then
				(
					messagebox msgText title:"These objects were tagged:"
				)
		)
	)
	
	on btnDelSelected pressed do
	(
		if chkbxConf.state == true then
			(
				delDamageTagWC ()
			)
			else
			(
				delDamageTag ()
				print "tags deleted from entire selection"
			)
	)
) --end of rollout
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
createDialog tagAsDamagedTools width:180