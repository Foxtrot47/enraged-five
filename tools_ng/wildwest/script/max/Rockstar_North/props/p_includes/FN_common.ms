-- Ricks Common Maxscript functions
-- Modified July 2009 to make BuildPointHelper return the point helper 

-- added fn AlignPivotTo function Jan 2010

-- Load the rockstar export settings
filein "rockstar\export\settings.ms"

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

-- *********************************************************************************************************************
-- *********************************************************************************************************************
-- String Functions

-- Convert text string to uppercase
fn uppercase instring = (  
	local upper, lower, outstring 
	upper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lower="abcdefghijklmnopqrstuvwxyz"

	outstring=copy instring

	for i=1 to outstring.count do 
	( 
		j=findString lower outstring[i]
		if (j != undefined) do outstring[i]=upper[j]
	)

	outstring 
) 


-- Convert text string to lowercase
fn lowercase instring = (  
	local upper, lower, outstring 
	upper="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lower="abcdefghijklmnopqrstuvwxyz"

	outstring=copy instring

	for i=1 to outstring.count do 
	( 
		j=findString upper outstring[i]
		if (j != undefined) do outstring[i]=lower[j]
	)

	--return outstring 
	outstring 
) 



-- Replace spaces in strings with underscores
-- This is vital for max variables in expressions
fn replacespace instring = (  
	local upper, lower, outstring 
	thespace=" "
	thereplace="_"

	outstring=copy instring

	for i=1 to outstring.count do 
	( 
		j=findString thespace outstring[i]
		if (j != undefined) do outstring[i]=thereplace[j]
	)

	outstring 
) 




-- Looks for an underscore in a string at a given position
fn findUnderscore ubstring spos = 
(

	returnvalue = undefined
	
	findUndersc = substring ubstring spos 1
	
			if findUndersc == "_" then
			(	
			returnvalue = True
			)
			else
			(
			returnvalue = False
			)
	returnvalue	
)




-- *********************************************************************************************************************
-- *********************************************************************************************************************
-- Array  Functions

-- This returns a difference array from two arrays
fn differenceInArray masterArray subsetArray =
(
	newarray = #()
	for mi = 1 to masterArray.count do (
		
		foundMatch=0
		MitemtoFind = masterArray[mi]
		
		for si = 1 to subsetarray.count do (
			SitemtoFind = subsetArray[si]
			if MitemtoFind == SitemtoFind then 
			(
				foundMatch = 1
				exit	-- for speed
			)
		)	
			
		if foundMatch == 0 then append newArray MitemToFind	
	)	
	
	newArray
)





-- *********************************************************************************************************************
-- *********************************************************************************************************************
-- Positional  Functions


-- taking in a bonename, such as $'Char Head', return the x,y,z position
fn getObjPos theObject = (
	-- taking in a bone, convert it to a string, parse the position
	p3 = theObject.transform.pos
	p3
)




-- Calculate a point in between two point3 positions
-- Can actually be used beyond 100% to find offsets too
fn inbetweenpoint startpoint endpoint whichpoint = (
	wp = endpoint + ((startpoint-endpoint) * (1.0 - whichpoint))
	wp
)

fn AlignPivotTo Obj Trgt =

      (
            -- Get matrix from object
            if classOf Trgt != matrix3 then Trgt = Trgt.transform

			-- Store child transforms
            local ChldTms = in coordSys Trgt ( for Chld in Obj.children collect Chld.transform )

            -- Current offset transform matrix
            local TmScale = scaleMatrix Obj.objectOffsetScale
            local TmRot = Obj.objectOffsetRot as matrix3
            local TmPos = transMatrix Obj.objectOffsetPos
            local TmOffset = TmScale * TmRot * TmPos

            -- New offset transform matrix
            TmOffset *= obj.transform * inverse Trgt

            -- Apply matrix
            Obj.transform = Trgt

            -- Restore offsets
            Obj.objectOffsetPos = TmOffset.translation
            Obj.objectOffsetRot = TmOffset.rotation
            Obj.objectOffsetScale = TmOffset.scale

            -- Restore child transforms
            for i = 1 to Obj.children.count do Obj.children[i].transform = ChldTms[i] * inverse Trgt * Obj.transform
      )




-- *********************************************************************************************************************
-- *********************************************************************************************************************
-- Building things.

-- Build an Expose Transform Helper with name, position, size and colour
-- Thankfully not required so much these days
fn buildETM etmname etmExposer etmPos etmSize etmColour = (
	newetm = ExposeTm pos:etmPos isSelected:on name:etmName size: etmSize wirecolor:etmColour
	newetm.exposenode = etmExposer
)


-- Build a Point Helper with name, position, size and colour
fn buildPointHelper phName phPos phSize phColour = (
	newph = Point pos:phPos name:phName size:phSize wirecolor:phColour
	newph.Box = off
	newph.cross = on
	newph.centermarker = off
	
	return newph
)


-- Build a Dummy elper with name, position and size
-- Size is done by scaling and restting the transform afterwards
fn buildDummyHelper dName dPos dSize = (

	newdummy = Dummy pos:dPos name:dName isselected:on 
	scale $[dsize,dsize,dsize]
	ResetTransform $
)






fn alignall alignme tome =(
	alignme.transform=tome.transform
)




fn alignrot alignme tome =(
	in coordsys (transMatrix alignme.pos) alignme.rotation=inverse tome.rotation
)



-- This should really take in a axis: Rick, October 2009
-- The X axis is the most common, so no hurry on this.
fn mirrorObject copyMe PasteMe =
(
	-- Assume we are mirroring across the X axis.
	copyPos = copyme.pos * [-1,1,1]
	pasteMe.pos = copyPos
)




-- Free Transforms, very useful for constructing rigs
-- Stolen from the Freezetransform that came with max.
fn FreezeTransform = 	
	( 		
		local Obj = Selection as array 	
		suspendEditing which:#motion
		for i = 1 to Obj.count do 		
		( 
			Try
			(	
				local CurObj = Obj[i] 	
	
				if classof CurObj.rotation.controller != Rotation_Layer do
				(

					-- freeze rotation		
					CurObj.rotation.controller = Euler_Xyz() 		
					CurObj.rotation.controller = Rotation_list() 			
					CurObj.rotation.controller.available.controller = Euler_xyz() 		
					
					/* "Localization on" */  
				
					CurObj.rotation.controller.setname 1 "Frozen Rotation" 		
					CurObj.rotation.controller.setname 2 "Zero Euler XYZ" 		
				
					/* "Localization off" */  
					
					CurObj.rotation.controller.SetActive 2 		
				)
				if classof CurObj.position.controller != Position_Layer do
				(

					-- freeze position
					CurObj.position.controller = Bezier_Position() 			
					CurObj.position.controller = position_list() 			
					CurObj.position.controller.available.controller = Position_XYZ() 	
		
					/* "Localization on" */  
							
					CurObj.position.controller.setname 1 "Frozen Position" 	
					CurObj.position.controller.setname 2 "Zero Pos XYZ" 			
					
					/* "Localization off" */  
					
					CurObj.position.controller.SetActive 2 		
		
					-- position to zero
					CurObj.Position.controller[2].x_Position = 0
					CurObj.Position.controller[2].y_Position = 0
					CurObj.Position.controller[2].z_Position = 0
				)
			)	
					
			Catch( messagebox "A failure occurred while freezing an object's transform." title:"Freeze Transform")	
		)
		resumeEditing which:#motion
	)
	

	
	


fn linkorient theManip theVictim alignManip theWeight =
(
	if alignManip == 1 then
	(
		in coordsys (transMatrix theManip.pos) theManip.rotation= theVictim.rotation
	)
	
	FreezeTransform()
	
	theVictim.rotation.controller = Orientation_constraint()
	thevictim.rotation.controller.appendtarget themanip theWeight
	thevictim.rotation.controller.relative = on
)






-- A function to toggle biped figuremore on/off
fn fig_toggle = (

	-- figure mode toggle
	biped_ctrl=$char.controller

	-- get mode
	cmode = biped_ctrl.figureMode

	-- toggle it

	if cmode == true then 
		(
			biped_ctrl.figureMode= false
		)
		
	else 
		(
		biped_ctrl.figureMode= true
		)


	--cancel bone position lag	
	--variable mytime stores the current time on timeline
	mytime=currentTime
	--sets timeline 1 frame further
	sliderTime = mytime + 1
	--moves timeline back to whatever frame it was before
	sliderTime = mytime
	
)







-- *******************************************************************************************
-- Selection functions


-- Lets you select items by name using wildcards
fn selectByWildcard nodeName=
(
	-- Create 	
	local myNodeList = #()
	nnc = nodename.count
	
	select $*
	
	for obj in selection do
	(
		ckn = substring obj.name 1 nnc
		if ((uppercase ckn) == (uppercase nodeName)) then
		(
			append myNodeList obj
		)
	) 
	clearSelection()
	select myNodeList
)	