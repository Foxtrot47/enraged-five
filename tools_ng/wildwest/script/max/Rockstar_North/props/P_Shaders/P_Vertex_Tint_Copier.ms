-- Vertex paint copier
-- Rick Stirling
-- Rockstar North
-- February 2011


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())


-- With the advent of vertex tinting we need a tool to propagate colours to LOD meshs

-- Portions copied from Ped Vertex_Tools partially written by Adam Munson

--v1.31
--Minor bug fix for orphaned progressbar

--v1.3
--Modified by Neil Gregory April 8 2011
--Added in progress bar
--Added functionality to identify nodes uniquely as previously just going by name string from the UI
--and finding node by name results in the first find with that name when there could be multiple objects with the same name.


--v1.2
--Modified by Neil Gregory March 30 2011
--Changed the vertex colour lookup function to now use a common function that converts mesh vertes to map verts
--and gets the colour for all the map verts and averages it to get the best colour to assign.
--optimized the preprocess function to only cache mesh data from the source object

--v1.1
-- Modified by Neil Gregory March 24 2011
-- Now just looks for the closest vertex on the target mesh to take the colour from
--removed unused ui components

-- NOTES

-- Gather Vertex Data
-- -- Poll through all the verts
-- -- Get their vertex colour and position in world space
-- -- -- Vertex data lives in channel 13 -- Set this in an include file so that we can reference this and make the tool more generic


-- Paste Vertex data
-- -- Go through all the verts
-- -- Get their position
-- -- Find a corresponding vert in the other list
-- -- Paste the data into channel 13 

escapeEnable = true
filein (RsConfigGetWildWestDir() + "script/3dsMax/_config_files/Wildwest_header.ms")
RsCollectToolUsageData (getThisScriptFilename())
try (filein "rockstar/export/settings.ms") catch()


-- Figure out the project
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

cpvChannelList=#()

-- Script specific data
-- Light settings are stored in a config file	
vbakesettings = (theWildWest + "etc/config/general/max_vertex_bake_settings.dat")


try (filein vbakesettings) 
catch (messagebox ("Couldn't find project specific vertex channel settings file. \r\nWas expecting " + vbakesettings ))


-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_VertexPaint.ms")


--RsAllVertInfoLists = #()	-- Contains vertData structs for every selected object's selected vertices
vertDistanceFactor = 0.1	-- The threshold for checking if vert positions are matches
RsAllVertexDataLists = #()	-- Array for the RsPreprocessVerts and RsAverageNormals function

--Vertex colour channels array
--Third entity in each array is for future use to determine if a particular channel is valid for colour interpolation
UIvertexChannels = #(
#("Colour Channel",0, "interpolate"),
#("Alpha Channel",-2, "interpolate"),
#("Illum Channel",-1, "interpolate"),
#("Wind Channel",11, "interpolate"),
#("Tint Channel",13, "exact"),
#("EnvEff Channel" ,14, "interpolate"),
#("Fat Channel",15, "interpolate")
)

defaultUIvertexChannelSelection = 5
	
-- TEMP VARIABLES
-- THESE WILL COME FROM THE DATA FILE

--Vertex data structure	
struct vertData 
(
	vertNumber,
	vertPos,
	vertColour
)

struct nodeID
(
	nameStr,
	UINameStr,
	UID,
	polyCount
)

--An array to hold the selected objects structs, which will be used to dtermine identity and for populating UI dropdowns
objSelectionSet = #()

-- Pass in a list of object names, 
-- For each create an array of data structures to hold the vert data
fn PreprocessVerts theObject  = 
(

	setCommandPanelTaskMode #modify

	obj = undefined
	for item in objSelectionSet do
	(
		if item.UINameStr == theObject then
		(
			obj = getAnimByHandle item.UID
		)
	)
	
	numVerts = obj.Vertices.count
	vertexDataList = #()
	
	--Setup the progressBar
	progressStart "PreProcessing Source Mesh:"
	
	progressInc = 100.0 / (numVerts as float)
	
	--iterate through the object verts
	for v = 1 to numVerts do
	(
		--create a vertex data instance
		vData = vertData()
		vertList = #{v} --not sure where this is used
		
		--populate vertex data properties
		vData.vertNumber = v
		vData.vertPos =obj.vertices[v].pos
		
		--select the current vertex 
		obj.EditablePoly.SetSelection #Vertex #{v}
		vertexColours = #()
		
		--iterate through user selected channels and append to the vertexColours array
		for vcc = 1 to UIvertexChannels.count do
		(
			--append vertexColours (obj.getVertexColor UIvertexChannels[vcc][2])
			--get the vertex colours for the vert
			
			
			--if this map channel is not supported then add white and continue to next channel
			--vccValid == false
			mapFaceCount = undefined
			try
			(
				mapFaceCount = polyop.getNumMapFaces obj UIvertexChannels[vcc][2]
			)
			catch()
			--vccValid = polyop.getMapSupport obj UIvertexChannels[vcc][2]
			--format "cnl: % valid %\n" UIvertexChannels[vcc][2] vccValid
			if mapFaceCount == undefined then
			(
				--append to the vertexColours Array
				append vertexColours (color 255 255 255)
				
				--next iteration
				continue
			)
			
			
			--Normally a mapchannel is found and valid
			aVColours = getMapVertColoursFromMeshVert obj v UIvertexChannels[vcc][2]

			--Now average the colours in the array
			colourAccumulator = #(0.0, 0.0, 0.0)
			for i = 1 to aVColours.count do
			(
				colourAccumulator[1] += aVColours[i][1]
				colourAccumulator[2] += aVColours[i][2]
				colourAccumulator[3] += aVColours[i][3]
			)
			
			colourAccumulator[1] = (colourAccumulator[1] / aVColours.count) * 255
			colourAccumulator[2] = (colourAccumulator[2] / aVColours.count) * 255
			colourAccumulator[3] =(colourAccumulator[3] / aVColours.count) * 255
			
			avgColour = color colourAccumulator[1] colourAccumulator[2] colourAccumulator[3]
			--DEBUG
			--format "vertex: % channel: % aVColours: % avgColour: %\n" v UIvertexChannels[vcc][1] aVColours avgColour
			
			--append to the vertexColours Array
			append vertexColours avgColour
		)
		--set the vertex data struct property for colour to the colours we have gathered
		vData.VertColour = vertexColours
		append vertexDataList vData
		
		--update Progress
		progressUpdate (v * progressInc)
	)
	--print numVerts
	--print vertexDataList
	append RsAllVertexDataLists vertexDataList	
	
	--reset progressBar
	progressEnd()
)

--
--Returns the vertex struct from the target mesh for the vertex closest to the srcVtxPos
--
fn getClosestVtx srcVtxPos vtxCloud =
(
	--number of verts in target mesh
	vtxCloudCount = vtxCloud.count
	
	--initial distance test value
	minDist =1000000	--should be based of object bounding box extents or similar metric
	closestVtx = 0
	
	for vtx = 1 to vtxCloudCount do 
	(
		--Get the distance between the points
		vMag = distance srcVtxPos vtxCloud[vtx].vertPos
		
		--check if its the closest
		if vMag < minDist then
		(
			minDist = vMag
			closestVtx = vtx
		)
	)
	
	return vtxCloud[closestVtx]
	
)



--Do the vertex colour copy
fn ProcessObjects copyCPVfrom copyCPVto channelArray = 
(
	--convert the passed in strings to objects we can use and are the correct one going by the unique node id
	copyFromObject = undefined
	copyToObject = undefined
	
	for obj in objSelectionSet do
	(
		if obj.UINameStr == copyCPVfrom then
		(
			copyFromObject = getAnimByHandle obj.UID
		)
		
		if obj.UINameStr == copyCPVto then
		(
			copyToObject = getAnimByHandle obj.UID
		)
	)
	
	--copyFromObject = getNodeByName copyCPVfrom 
	--copyToObject = getNodeByName copyCPVto
	
	--get the source mesh position
	copyFromObjectPos = copyFromObject.pos
	BACKUPcopyToObjectPos = copyToObject.pos
	--set the destination mesh position to the source position
	copyToObject.pos = copyFromObjectPos
	
	--init vertex data array
	RsAllVertexDataLists = #()
	
	--vertexcolourchannel = UIvertexChannels[VchannelLookup][2]
	
	-------------------------------------
	--PREPROCESS VCC DATA
	-------------------------------------
	preprocessVerts copyCPVfrom --vertexcolourchannel 
	--preprocessVerts copyCPVto --vertexcolourchannel 
	
	copyFromData = RsAllVertexDataLists[1]

	-- how many verts. This is off by one, but this doesn't matter.
	copyFromVertCount = copyFromData.count

	select copyToObject
	
	--vert count
	vertCount = copyToObject.Vertices.count
	
	--initialize progressbar
	--progressStart "Transposing vertex data:"
	--progress incrementor
	progressInc = 100.0 / (vertCount as float)
	
	-----------------------------------------------
	--FLUSH VERTEX COLOUR CHANNELS
	-----------------------------------------------
	--initialize progressbar
	progressStart "Flush vertex data:"
	
	for v = 1 to vertCount do
	(	
		-- Flush verts  with black
		flushColour = (color 255 255 255)
		copyToObject.EditablePoly.SetSelection #Vertex #{v}
		
		for pastechannel = 1 to channelArray.count do
		(
			if channelArray[pastechannel] == true then
			(
				copyToObject.setVertexColor flushColour UIvertexChannels[pastechannel][2]
			)
		)
		--update progress
		progressUpdate (v * progressInc)
		
	)
	--reset progressbar
	progressEnd()
	
	----------------------------------------------
	--TRANSPOSE VERTEX COLOUR DATA
	----------------------------------------------
	--initialize progressbar
	progressStart "Transposing vertex data:"
	--with redraw off 
	for v = 1 to vertCount do
	(	
		-- Get position of each vert in destination from precached data
		vpos = copyToObject.vertices[v].pos

		--find the closest vert int he source mesh to take the colour from
		--returns the vertData object
		closestVtx = getClosestVtx vpos copyfromData
		
		copyvertcolourArray = closestVtx.VertColour
		copyToObject.EditablePoly.SetSelection #Vertex #{v}
		
		for pasteVert = 1 to copyvertcolourArray.count do
		(
			if channelArray[pasteVert] == true then
			(
				copyToObject.setVertexColor copyvertcolourArray[pasteVert] UIvertexChannels[pasteVert][2]
			)
		)
		
		--update progress
		progressUpdate (v * progressInc)
	)	-- end destination vert loop
	
	
	-- Put the object back where it belongs
	copyToObject.pos = BACKUPcopyToObjectPos
	
	-- Wipe out the vert data
	backupVertData = RsAllVertexDataLists[1]
	
	RsAllVertexDataLists = #()
	
	--reset progressbar
	progressEnd()
	--vertexToolsTintCopier.prgVcc.value = 0 
	
)


fn sortPolycountArray v1 v2 = 
(
	return (v1.polycount - v2.polycount)
)



-- We get passed objects, we one want to copy along a lod chain
fn getObjectsForUI = 
(
	--reset the working selection array
	objSelectionSet = #()
	
	--get selection
	objSelection = getCurrentSelection()
	
	--Check for number of selected objects
	if objSelection[1]== undefined  then (messagebox "Nothing selected to populate UI with")
	else if objSelection.count > 4 then (messagebox "Too many objects selected")
	
	--For each of the objects in the selection lets get a unique ID so that later when we need them we wont have problems with nodes with duplicate names
	for obj in objSelection do
	(
		--crerate obj unique reference
		objStruct = nodeID()
		objStruct.nameStr = obj
		objStruct.UINameStr = obj.name
		objStruct.UID = getHandleByAnim obj
		objStruct.polyCount  =getnumfaces obj
		
		--Add to the objSelectedArray
		append objSelectionSet objStruct
	)
	
	--Check for any objects have a vertexpaint in their stack as this will cause problems copying vertex colours
	/*
	for i=1 to objSelectionSet.count do
	(
		assignedMods = objSelectionSet[i].nameStr.modifiers
		--print assignedMods
		foundIllegalMod = false
		for j=1 to assignedMods.count do
		(
			modStr = assignedMods[j] as string
			strSearch = findString modStr "VertexPaint"
			
			if strSearch != undefined then
			(
				msg = "VertexPaint modifier found on object: " + objSelectionSet[i].UINameStr + "\n\rPlease remove it before hitting GO!"
				messagebox msg
			)
		)
	)
	*/
	
	--sort into ascending face count order
	--an array of UID and faceCount for sorting
	--aToSort = #()
	--for obj in objSelectionSet do
	--(
	--	append aToSort #(obj.UID, obj.faceCount)
	--)
	qsort objSelectionSet sortPolycountArray
	
	--return array
	objSelectionSet
)


--Destroy the UI if its already opened
if ((vertexToolsTintCopier != undefined) and (vertexToolsTintCopier.isDisplayed)) do
(
	destroyDialog vertexToolsTintCopier
)
--create UI
rollout vertexToolsTintCopier "Vertex Tint Copier"
(
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Vertex_Colour_Project_Tool" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)
	
	button btnPopulateObjects "Add Selections to UI" width:180 height:30 
	
	group "Objects"
	(
		dropDownList ddCopyFrom "Copy" width:200 height:40 items:#()
		dropDownList ddPasteToOne "Paste" width:200 height:40 items:#()
		dropDownList ddPasteToTwo "Paste" width:200 height:40 items:#()
		dropDownList ddPasteToThree "Paste" width:200 height:40 items:#()
		
		radiobuttons rdbcopy_type labels:#("Chain Child to Child", "High to all children")
	)
	--dropDownList ddlVertexChannel "Channel Type" width:200 height:40 items:#()
	
	group "Channels"
	(
		checkbox chkvcc1 "Colour Channel" checked:true	
		checkbox chkvcc2 "Alpha Channel" checked:true	
		checkbox chkvcc3 "Illum Channel" checked:true	
		checkbox chkvcc4 "Wind Channel" checked:true	
		checkbox chkvcc5 "Tint Channel" checked:true	
		checkbox chkvcc6 "EnvEff Channel"checked:true	
		checkbox chkvcc7 "Fat Channel" checked:true	
	)	
	
	--colorpicker clrFillColour "Missed Vertex Colour" color:[128,128,128] modal:false 
	--checkbox chkUseFillColour "Use Fill" checked:false offset:[160,-22]

	
	--spinner distFactorVal "Vertex search distance" range:[0.001,0.500,vertDistanceFactor] scale:0.001 fieldwidth:50 offset:[-30,5]
	
	--action button
	button btnCopyAndPaste " GO! " width:180 height:30 offset:[0,10]
	
	--version info
	label lblInfo  "V1.3" style_sunkenedge:true width:240 height:16
	
	
	--progressbars removed for now due to update problems
	--progressbar prgObject color:green
	--progressbar prgVcc color:green
	

	
	on vertexToolsTintCopier open do
	(	
		vdataList = #()
		for ddlitem = 1 to UIvertexChannels.count do
		(
			append vdataList UIvertexChannels[ddlitem][1]
		)	
	)
	
	on distFactorVal changed val do
	(
		vertDistanceFactor = val
	)
	
	
	--Start copying vert colours
	on btnCopyAndPaste pressed do 
	(
		--disable scene redraws
		--disableSceneRedraw()
		
		--print vertexToolsTintCopier.rdbcopy_type.state
		
		--Vertex colour channels checkbox setting array
		channelArray = #(
			vertexToolsTintCopier.chkvcc1.state,
			vertexToolsTintCopier.chkvcc2.state,
			vertexToolsTintCopier.chkvcc3.state,
			vertexToolsTintCopier.chkvcc4.state,
			vertexToolsTintCopier.chkvcc5.state,
			vertexToolsTintCopier.chkvcc6.state,
			vertexToolsTintCopier.chkvcc7.state
			)
		
		--fillColour = vertexToolsTintCopier.clrFillColour.color
		--fillstate = vertexToolsTintCopier.chkUseFillColour.state 
		--VchannelLookup = vertexToolsTintCopier.ddlVertexChannel.selection
		
		--Populate an array with the objects to work with
		theSelection=#()
		if vertexToolsTintCopier.ddCopyFrom.selected != undefined then append theSelection vertexToolsTintCopier.ddCopyFrom.selected
		if vertexToolsTintCopier.ddPasteToOne.selected !=undefined  then append theSelection vertexToolsTintCopier.ddPasteToOne.selected
		if vertexToolsTintCopier.ddPasteToTwo.selected !=undefined  then append theSelection vertexToolsTintCopier.ddPasteToTwo.selected
		if vertexToolsTintCopier.ddPasteToThree.selected !=undefined then append theSelection vertexToolsTintCopier.ddPasteToThree.selected	
		
		-- Assume 1 and 2 are always filled
		copyCPVfrom = vertexToolsTintCopier.ddCopyFrom.selected
		copyCPVto = vertexToolsTintCopier.ddPasteToOne.selected
		 
		
		--progressbar reset
		--vertexToolsTintCopier.prgObject.value = 100*1/theSelection.count
		-- loop this bit
		for n = 1 to (theSelection.count - 1) do
		(	
			
			-- Where do we copy from?
			if vertexToolsTintCopier.rdbcopy_type.state == 1 then 
			(
				copyCPVfrom = theSelection[n]
			)
			
			else if vertexToolsTintCopier.rdbcopy_type.state == 2 then 
			(
				copyCPVfrom = theSelection[1]
			)	
			
			copyCPVto = theSelection[n+1]
						
			ProcessObjects copyCPVfrom copyCPVto channelArray
			
		)
	
		--re-enbale scene redraws
		--enableSceneRedraw()
	)
	
	--Populate UI with object selection
	on btnPopulateObjects pressed do
	(
		-- Reset all the drop down lists
		try (ddCopyFrom.items = "-NONE-") catch ()
		try (ddPasteToOne.items = "-NONE-") catch ()
		try (ddPasteToTwo.items = "-NONE-") catch ()
		try (ddPasteToThree.items = "-NONE-") catch ()
		
		--Get selection for adding to UI
		local ascdataArray= getObjectsForUI()
		
		-- reverse the array
		local dataArray= #()
		for i = ascdataArray.count to 1 by -1 do
		(
			append dataArray ascdataArray[i].UINameStr
		)
		
		-- add all items to all drop down lists
		dataList = #("-NONE-")
		for item in dataArray do
		(
			append dataList item
		)
		
		-- Populate all the drop down lists
		try (ddCopyFrom.items = dataList) catch ()
		try (ddPasteToOne.items = dataList) catch ()
		try (ddPasteToTwo.items = dataList) catch ()
		try (ddPasteToThree.items = dataList) catch ()
		
		-- Now set them 
		try(ddCopyFrom.selection= 2) catch()
		try(ddPasteToOne.selection= 3) catch(ddPasteToOne.selection= 1)
		try(ddPasteToTwo.selection= 4) catch(ddPasteToTwo.selection= 1)
		try(ddPasteToThree.selection= 5) catch(ddPasteToThree.selection=1)
		
	)	
	
	on vertexToolsTintCopier  close do
	(
		RsAllVertInfoLists = #()
	)
)	



createDialog  vertexToolsTintCopier width:250