-- Blue channel flipper
-- Rick Stirling, Rockstar North, February 2011

-- Common script headers, setup paths and load common functions
try (filein "rockstar/export/settings.ms") catch()


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

-- Figure out the project
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

cpvChannelList=#()

-- Script specific data
-- Light settings are stored in a config file	
vbakesettings = (theWildWest + "etc/config/general/max_vertex_bake_settings.dat")
Skylightsettings = (theWildWest + "etc/config/general/max_skylight_settings.dat")

try (filein SkylightSettings) 
catch (messagebox ("Couldn't find project specific light settings file. \r\nWas expecting " + SkylightSettings))

try (filein vbakesettings) 
catch (messagebox ("Couldn't find project specific light settings file. \r\nWas expecting " + vbakesettings ))


-- Load common functions	
filein "pipeline/util/radiosity.ms"
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_VertexPaint.ms")
	
-- Default the art type to peds
arttype = "Map_"

Vegwindchannel = TheWindChannel
theVerticalChannel =  21 
	
fn flipBlue objectSelection = (

	-- For each object...
	for obj in objectSelection do (
	-- Get the data in the wind channel
		
		select obj
					
		if ((classof obj == Editable_Poly) or (classof obj ==PolyMeshObject)) then 
		(
			nv = obj.numverts
			for v = 1 to nv do (
				obj.EditablePoly.SetSelection #Vertex #{v}
				VertColour = obj.getVertexColor Vegwindchannel
				blueComponent= VertColour.b
				inverseBlue = 255 - blueComponent
				VertColour.b = inverseBlue 
				polyop.setvertcolor obj Vegwindchannel v VertColour
			)
		)
				
				
-- 		if classof obj == Editable_Mesh then
-- 		(
-- 			nv = obj.numverts
-- 			for v = 1 to nv do (
-- 				SetVertSelection obj #{v}
-- 				VertColour = obj.getVertexColor Vegwindchannel
-- 				blueComponent= VertColour.b
-- 				inverseBlue = 255 - blueComponent
-- 				VertColour.b = inverseBlue 
-- 				polyop.setvertcolor obj Vegwindchannel v VertColour
-- 			)
-- 		)
-- 				
				

		
	-- Also get the data in the blue channel from the separated parts

		if ((classof obj == Editable_Poly) or (classof obj ==PolyMeshObject)) then 
		(
			try ( 
				nv = polyop.getnummapverts obj theVerticalChannel 
				polyop.setNumMapVerts obj TheWindChannel nv


				for v = 1 to nv do
				(	
					r = (polyop.getmapvert obj theVerticalChannel v).x 
					g = (polyop.getmapvert obj theVerticalChannel v).y
					b = (polyop.getmapvert obj theVerticalChannel v).z 
					
					inverseBlue = 1.0 - b
					InverseColour = color r g inverseBlue
					
					polyop.setmapvert obj theVerticalChannel v InverseColour
				)
				
				clearSelection()
				select obj
				) catch()
				
			) -- end of separate channel
		

	) 
)	

	

rollout blueflipper "Blue Wind Channel Flip"
(
		button btnFlipSelection "Flip Blue Wind on Selected" width:180 height:30 

		on btnFlipSelection pressed do
		(
			objectlist = getcurrentselection()
			flipBlue objectlist 
		)
)

createDialog  blueflipper width:200
