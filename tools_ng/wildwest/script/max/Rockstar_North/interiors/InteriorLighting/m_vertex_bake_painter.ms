-- Vertex_Bake_Painter
-- A script to bake Skylight based ambient occlusion to models
-- Rick Stirling 2008-2011

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

-- March 2011, writing this for interior bakes
-- Based on ped vertex painter


-- ***************************************************************************************************************************


-- ***************************************************************************************************************************
-- Common script headers, setup paths and load common functions
try (filein "rockstar/export/settings.ms") catch()




-- Figure out the project
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

cpvChannelList=#()

-- Script specific data
-- Light settings are stored in a config file	
vbakesettings = (theWildWest + "etc/config/general/max_vertex_bake_settings.dat")
Skylightsettings = (theWildWest + "etc/config/general/max_skylight_settings.dat")

try (filein SkylightSettings) 
catch (messagebox ("Couldn't find project specific light settings file. \r\nWas expecting " + SkylightSettings))

try (filein vbakesettings) 
catch (messagebox ("Couldn't find project specific light settings file. \r\nWas expecting " + vbakesettings ))


-- Load common functions	
filein "pipeline/util/radiosity.ms"
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_VertexPaint.ms")
	
-- Default the art type to peds
arttype = "Int_"
ddatitem = 1

/*	
-- Now try to load a local file that will override the art typespace
localArttype = ("c:/temp/RSWildwest.dat")

try (filein localArttype ) catch ()

case arttype of
(
	"Ped_" :ddatitem = 1
	"Map_" :ddatitem = 2
	"Veh_" :ddatitem = 3
)
*/
	
	
-- ****************************************************************************************
-- This array is the channels 
-- Vertex channel id, flat colour for that channel, UI checkbox status
fn populateChannelData = (
	cpvChannelList = #(
			#(TheColourChannel, 	(readvalue ((artType + "BakeColour") as stringstream)), 	false),
			#(TheWindChannel,  	(readvalue ((artType + "WindColour") as stringstream)), 	false),
			#(TheWindChannelR, 	(readvalue ((artType + "WindColourR") as stringstream)), 	false),
			#(TheWindChannelG, 	(readvalue ((artType + "WindColourG") as stringstream)), 	false),
			#(TheWindChannelB, 	(readvalue ((artType + "WindColourB") as stringstream)), 	false),
			#(TheEnvEffChannel, 	(readvalue ((artType + "EnvEffColour") as stringstream)), 	false),
			#(TheFatChannel, 		(readvalue ((artType + "FatColour") as stringstream)), 		false),
			#(TheAlphaChannel, 	(readvalue ((artType + "AlphaColour") as stringstream)),	 false),
			#(TheRimChannel, 		(readvalue ((artType + "RimColour") as stringstream)), 	false)
	)
)	
populateChannelData()








fn setGeoShadowCasting shadowState = (
	
	-- Make sure we have objects selected
	if $ == undefined  then (messagebox "Nothing selected to set shadow properties on")
	
	
	-- Build array of all objects for shadows
	else
	(
		objSelectedArray = getCurrentSelection()
		objCount = objSelectedArray.count
		
		for i = 1 to objCount  do (
			obj = objSelectedArray[i]
			obj.isGIOccluder = shadowState
		)
		
	)
	
)





-- ****************************************************************************************
if theVertPaintToolsFloater != undefined do
(
	try (closeRolloutFloater theVertPaintToolsFloater) catch()
)

-- ****************************************************************************************
rollout colourbaker "Vertex Baker"
(	
	hyperlink lnkHelp "Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Vertex_Paint_Tools" align:#right color:(color 20 20 255) hoverColor:(color 255 255 255) visitedColor:(color 0 0 255)
	
	dropDownList ddArtType "" pos:[10,10] width:100 height:40 items:#("Interiors")
	
	--groupBox grpPreservegroup "Preserve Checked Data" pos:[5,40] width:215 height:65
	--checkbox chkPreserveColour "Vertex Colours" pos:[10,55] checked:cpvChannelList[1][3]
	--checkbox chkPreserveWind "Micro Movements" pos:[10,70] checked:cpvChannelList[2][3]
	--checkbox chkPreserveEnvEff "Enviroment Effects" pos:[10,85] checked:cpvChannelList[6][3]
	--checkbox chkPreserveFat "Ped Fat" pos:[140,55] checked:cpvChannelList[7][3]
	--checkbox chkPreserveAlpha "Alpha" pos:[140,70] checked:cpvChannelList[8][3]
	--checkbox chkPreserveRim "Rim/Illum" pos:[140,85] checked:cpvChannelList[9][3]
	
	--button btnPrebake "Prebake Selected with Flat Colours" width:215 height:30 pos:[5,110]
	
	--progressbar pbProgress pos:[10,148]
	
	
	button btnCastShadowsOn "Cast Shadows" pos:[5,170] width:100  height:30
	button btnCastShadowsOff "No Shadows" pos:[120,170] width:100  height:30
	
	
	button btnBakeNatual "Natural Bake" pos:[5,210] width:100  height:30
	button btnBakeArtificial "Artificial Bake" pos:[120,210] width:100  height:30
	
	--radiobuttons contrastType labels:#("Cavity", "Contrast  ", "Static  ") columns:3 default:1 width:150 pos:[10,264]
	--radiobuttons edgeType labels:#("By Face", "By Vertex") columns:2 default:1 width:150 pos:[40,286]
	
	--button btnMakelight "Make Skylight"  pos:[5,310] width:100  height:30
	--button btnCalcRad "Recalculate lighting" pos:[120,310] width:100 height:30
	--button btnBakeselAO "Assign AO Bake" pos:[5,340] width:100 height:30
	--button btnBakeselRim "Assign Rim Bake" pos:[120,340] width:100 height:30

	--checkbox chkCollapseEPoly "Collapse to Editable Poly after Bake" pos:[10,380] checked:false

	--button btnCleanup "Cleanup lights and lighting" pos:[5,400] width:215 height:30


-- ****************************************************************************************
	on colourbaker open do 
	(
		ddArtType .selection = ddatitem
		populateChannelData()
		--chkCollapseEPoly.state = (readvalue ((artType + "CollapseEPoly") as stringstream))
	)

	on ddArtType selected item do
	(
		artTypeChosen = ddArtType.selected
		artType = (substring artTypeChosen 1 3) + "_"
		populateChannelData()
		--chkCollapseEPoly.state = (readvalue ((artType + "CollapseEPoly") as stringstream))
	)	

	--on chkPreserveColour changed theState do cpvChannelList[1][3] = theState

-- 	on chkPreserveWind changed theState do
-- 	(
-- 		cpvChannelList[2][3] = theState
-- 		cpvChannelList[3][3] = theState
-- 		cpvChannelList[4][3] = theState
-- 		cpvChannelList[5][3] = theState
-- 	)
-- 	on chkPreserveEnvEff changed theState do cpvChannelList[6][3] = theState
-- 	on chkPreserveFat changed theState do cpvChannelList[7][3] = theState
-- 	on chkPreserveAlpha changed theState do cpvChannelList[8][3] = theState
-- 	on chkPreserveRim changed theState do cpvChannelList[9][3] = theState

	on btnPrebake pressed do
	(
		pbProgress.value = 0
		pbProgress.color = color 0 50 0
		prebakeSelected artType pbar:pbProgress -- removed map wind mode check
	)
	
	on btnMakelight pressed do makeSkyLight artType contrastType.state
	on btnCalcrad pressed do calcRad artType contrastType.state
	on btnBakeselAO pressed do (
		bakedselection = bakesel TheColourChannel "VBake AO/Colour" artType contrastType.state (edgeType.state - 1)
		--print "bakedselection"
		--print bakedselection
		if chkCollapseEPoly.state == true then  macros.run "Modifier Stack" "Convert_to_Poly"
	)
	
	on btnBakeselRim pressed do (
		bakesel TheRimChannel "VBake Rim/Illum" artType contrastType.state (edgeType.state - 1)
		if chkCollapseEPoly.state == true then  macros.run "Modifier Stack" "Convert_to_Poly"
	)
	
	on btnCleanup pressed do cleanuplights()
	
	
	
	on btnCastShadowsOn pressed do setGeoShadowCasting true
	on btnCastShadowsOff pressed do setGeoShadowCasting false	
		
	
	on btnBakeArtificial  pressed do (
		bakedselection = bakeselInt TheRimChannel "Artificial" artType 0 1
	)	
)


-- ****************************************************************************************
rollout windBaker "Wind Baker (Micro Movements)"
(	
	button btnAddWind "Add Wind Layers" pos:[5,5] width:215 height:30
	button btnWindHor "(R) Hor" pos:[5,40] width:50 height:30
	button btnWindVert "(B) Vert" pos:[60,40] width:50 height:30
	button btnWindRate "(G) Rate" pos:[115,40] width:50 height:30
	button btnWindBLACK "Black" pos:[170,40] width:50 height:30
	
	button btnCompressWindChannels "Condense wind channels for exporting" pos:[5,75] width:215 height:30	
	
	on btnAddWind pressed do CreateWindModifiers()
	
	on btnWindHor pressed do
	(
		if $ == undefined  then (messagebox "Nothing selected" )
		else
		(
			if $.modifiers[#Wind_Horizontal] != undefined Then
			(
				vpt = VertexPaintTool()
				vpt.paintColor = windHorizontalColour
			
				objSelectedArray = getCurrentSelection()
				clearselection()
				select objSelectedArray[1]
				modPanel.setCurrentObject $.modifiers[#Wind_Horizontal]
				thePainterInterface.maxSize =readvalue ((artType + "paintbrushsize") as stringstream)
				vpt.curPaintMode = 1
			)
			else (	Messagebox "Unable to select the horizontal modifier, it hasn't been created yet!" title:"Horizontal Modifier!")
		)
	)
	
	on btnWindVert pressed do
	(
		if $ == undefined  then (messagebox "Nothing selected")
		else
		(
			if $.modifiers[#Wind_Vertical] != undefined Then
			(
				vpt = VertexPaintTool()
				vpt.paintColor = windVerticalColour
				if artType == "Map_" then vpt.paintColor = inverseWindVerticalColour


				objSelectedArray = getCurrentSelection()
				clearselection()
				select objSelectedArray[1]
				modPanel.setCurrentObject $.modifiers[#Wind_Vertical]
				thePainterInterface.maxSize =readvalue ((artType + "paintbrushsize") as stringstream)
				vpt.curPaintMode = 1
			)
			else (Messagebox "Unable to select the vertical modifier, it hasn't been created yet!" title:"Vertical Modifier!")
		)
	)
	
	on btnWindRate pressed do
	(
		if $ == undefined  then (messagebox "Nothing selected")
		else
		(
			if $.modifiers[#Wind_Rate] != undefined Then
			(
				vpt = VertexPaintTool()
				vpt.paintColor = windRateColour		

				objSelectedArray = getCurrentSelection()
				clearselection()
				select objSelectedArray[1]
				modPanel.setCurrentObject $.modifiers[#Wind_Rate]
				thePainterInterface.maxSize =readvalue ((artType + "paintbrushsize") as stringstream)
				vpt.curPaintMode = 1
			)
			else (Messagebox "Unable to select the wind rate modifier, it hasn't been created yet!" title:"Rate Modifier!")
		)
	)

	on btnWindBLACK pressed do
	(
		if $ == undefined  then (messagebox "Nothing selected")
		else
		(
			if classof (modPanel.getCurrentObject()) == VertexPaint do
			(
				currentmodifer = modPanel.getCurrentObject()
				currentmodifername = currentmodifer.name
				vpt = VertexPaintTool()
				vpt.paintColor = (color 0 0 0)

				objSelectedArray = getCurrentSelection()
				clearselection()
				select objSelectedArray[1]
				modPanel.setCurrentObject  $.modifiers[currentmodifername ]
				thePainterInterface.maxSize = readvalue ((artType + "paintbrushsize") as stringstream)

				vpt.curPaintMode = 1
			)
		)
	)
	
	on btnCompressWindChannels pressed do CondenseWindChannels()
)


-- ****************************************************************************************
theVertPaintToolsFloater = newRolloutFloater "Vertex Paint Tools" 240 570

addRollout colourbaker theVertPaintToolsFloater
--addRollout windBaker theVertPaintToolsFloater