--GTAV Billboard Generator

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())
	
	
----------------
--VARIABLES
----------------
try (destroyDialog RsBillboardGenRoll) catch ()

------------------
-- FUNCTIONS
------------------

---------------------
--Create Billboard
---------------------
fn RsCreateBillboard frontProj:true =
(
	--selection
	local selObjs = for obj in getCurrentSelection() where (isProperty obj "mesh") collect obj
	
	if selObjs.count == 0 then
	(
		messagebox "No meshes selected"
		return 0
	)
	
	undo "generate billboards" on
	
	for obj in selObjs do 
	(
		--get object bounding box
		in coordsys world
		local bb = in coordsys world nodeLocalBoundingBox obj

		--print bb
		local bbLength = undefined
		local bbWidth = undefined
		
		if frontProj == false then
		(
			bbLength = abs( bb[1].y - bb[2].y)
			bbWidth = abs(bb[1].z - bb[2].z)
		)
		else
		(
			bbLength = abs(bb[1].z - bb[2].z)
			bbWidth = abs(bb[1].x - bb[2].x)
		)
		--format "bbLength: % bbWidth: % \n" bbLength bbWidth

		--create plane
		local RS_billboard = Plane pos:obj.pos length:bbLength width:bbWidth lengthsegs:1 widthsegs:1 mapCoords:true
		addModifier RS_billboard (Turn_To_Poly())
		collapseStack RS_billboard
			
		--apply uvw mapping
		uvMod = uvwmap vflip:true
		addModifier RS_billboard uvMod
			
		--Give it the correct name
		RS_billboard.name = "SLOD_" + obj.name

		--adjust pivot
		oldPivot = RS_billboard.pivot
		if frontProj == false then
		(
			newX = (oldPivot.x + (bbWidth / 2.0))
			newPivot =  [newX,  oldPivot.y, oldPivot.z]
			RS_billboard.pivot	= newPivot
		)
		else
		(
			newY = (oldPivot.y + (bbLength / 2.0))
			newPivot =  [oldPivot.x, newY, oldPivot.z]
			RS_billboard.pivot	= newPivot
		)
		--format "oldPivot: % newX: % newPivot: %\n" oldPivot newX newPivot
			
		--rotate billboard upright
		if frontProj == false then
		(
			rot = eulerangles 0 90 0
			rotate RS_billboard rot
		)
		else
		(
			rot = eulerangles -90 0 0
			rotate RS_billboard rot
		)
			
		--move to source mesh position
		RS_billboard.pos = obj.pos
			
		--apply shader
		rs = Rage_Shader()
		RstSetShaderName rs "trees_lod2.sps"
		RstSetTextureScaleValue rs  1.0

		--set bounding box values
		bb_Shader = Point4 (bbWidth as float) (bbLength as float) 0.0 0.0
		RstSetVariable rs 2 bb_Shader
		
		--Reset XForm
		ResetXform RS_billboard
		collapseStack RS_billboard
		
		--debug
		bbVal = 	RstGetVariable rs 2
		format "bbVal: %\n" bbVal
		
		--assign material
		RS_billboard.material = rs
		
		--populate medit 1 so we can check its values and change the texture etc
		meditmaterials[1] = rs
	)
)

rollout RsBillboardGenRoll "Billboard Generator"
(
	group "Projection"
	(
		checkbox chkFront "Front" checked:true tooltip:"Front projection" across:2
		checkbox chkSide "Side" checked:(not chkFront.checked) tooltip:"Side projection"
	)
	
	button btnDo "Create"
	label lblInfo "V0.3" style_sunkenedge:true width:140 height:16
	
	on btnDo pressed do
	(
		RsCreateBillboard frontProj:chkFront.checked
	)
	
	----------
	--Events
	----------
	on chkFront changed theState do
	(
		chkSide.checked = not theState
	)
	
	on chkSide changed theState do
	(
		chkFront.checked = not theState
	)
)

createDialog RsBillboardGenRoll 180 100
