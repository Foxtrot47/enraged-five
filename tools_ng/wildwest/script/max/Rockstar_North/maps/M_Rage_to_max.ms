-- Shader value editor
-- Rick Stirling
-- Rockstar North
-- July 2010

-- Update 28/07/10 Stewart Wright - added checks for material types and added ui elements

-- Script initially designed to halve the value of all normal maps in a SCENE
-- Will it work with containers?
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- Load the common functions
filein "rockstar/export/settings.ms"

-- Figure out the project data
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

-------------------------------------------------------------------------------------------------------------------------
mlib = undefined
sceneMatArrCheck = #()
sceneMatArr = #()

-- Get or set a rage shader variables
-- Pass in the material and the slot type we are looking for, eg "bumpiness", "diffuse texture"
-- Pass in a value to set, "undefined" gets ignored
fn getSetRageV MatID SubID slottype setSlotValue =
(
	-- Arrays for storing data
	RageSlotNames =#()
	RageSlotTypes =#()
	
	if SubID != undefined then
	(
		-- Need to know the shadertype, this dictates the slot we need
		RageShaderName = RstGetShaderName mlib[matid].materialList[subid]
		RageShaderSlotCount = RstGetVariableCount mlib[matid].materialList[subid] 
		
		-- Populate the arrays
		for ss = 1 to RageShaderSlotCount do
		(
			append RageSlotNames (lowercase (RstGetVariableName mlib[matid].materialList[subid] ss))
			append RageSlotTypes (RstGetVariableType mlib[matid].materialList[subid] ss)
		)

		-- Now we have a mapping, lets find the index of the slottype we are interested in.
		shaderSlot = finditem RageSlotNames slottype

		returnValue = undefined
		
		if setSlotValue == undefined then
		(
			returnValue = RstGetVariable mlib[MatID].materialList[SubID] shaderslot SetSlotValue
		)
		else
		(
			RstSetVariable mlib[MatID].materialList[SubID] shaderslot SetSlotValue
		)
	)
	else
	(
		-- Need to know the shadertype, this dictates the slot we need
		RageShaderName = RstGetShaderName mlib[matid]
		RageShaderSlotCount = RstGetVariableCount mlib[matid] 
			
		-- Populate the arrays
		for ss = 1 to RageShaderSlotCount do
		(
			append RageSlotNames (lowercase (RstGetVariableName mlib[matid] ss))
			append RageSlotTypes (RstGetVariableType mlib[matid] ss)
		)

		-- Now we have a mapping, lets find the index of the slottype we are interested in.
		shaderSlot = finditem RageSlotNames slottype

		returnValue = undefined
		
		if setSlotValue == undefined then
		(
			returnValue = RstGetVariable mlib[matid] shaderslot SetSlotValue
		)
		else
		(
			RstSetVariable mlib[matid] shaderslot SetSlotValue
		)
	)
)--end getSetRageV
-------------------------------------------------------------------------------------------------------------------------

fn RageToMax matid= (
	
	theMaterial = mlib[matid]
	
	if classof theMaterial == Rage_Shader then
	(
		diffmaps = #()
		specmaps = #()
		bumpmaps = #()

		-- build a list of all the materials used in a shader
		-- get the current normal value
		try (currentdiffMap = getSetRageV MatID SubID "diffuse texture" undefined) catch (print (getCurrentException()))
		try (currentspecMap = getSetRageV MatID SubID "specular texture" undefined) catch (print (getCurrentException()))
		try (currentbumpMap = getSetRageV MatID SubID "bump texture" undefined) catch (print (getCurrentException()))
			
		append subidnames subname
		append diffmaps currentdiffMap
		append specmaps currentspecMap
		append bumpmaps currentbumpMap
		
		
		-- Create a standard material multisub
		-- Create the base RAGE shader
		meditMaterials[MatId] = Multimaterial ()
		meditMaterials[Matid] = Multimaterial numsubs: howmanysubs

		try (meditMaterials[matid].materialList[subid].diffuseMap = Bitmaptexture fileName:diffmaps[subid]) catch()
		try (meditMaterials[matid].materialList[subid].diffuseMap.alphaSource = 2) catch()
		try (meditMaterials[matid].materialList[subid].specularLevelMap = Bitmaptexture fileName:specmaps[subid]) catch()
		try (meditMaterials[matid].materialList[subid].specularLevelMap.alphaSource = 2) catch()
				
		try (meditMaterials[matid].materialList[subid].bumpMap = Normal_Bump ()) catch()
		try (meditMaterials[matid].materialList[subid].bumpMapAmount = 100) catch()
		try (meditMaterials[matid].materialList[subid].bumpMap.normal_Map = Bitmaptexture ffileName:bumpmaps[subid]) catch()
		
	)

	
	else if classof theMaterial == Multimaterial then
	(
		howmanysubs = mlib[matid].numsubs
		subidnames =#()
		diffmaps = #()
		specmaps = #()
		bumpmaps = #()

		-- build a list of all the materials used in a shader
		for subid = 1 to howmanysubs do (
			-- get the current normal value
			subname = mlib[matid][subid].name
			try (currentdiffMap = getSetRageV MatID SubID "diffuse texture" undefined) catch (print (getCurrentException()))
			try (currentspecMap = getSetRageV MatID SubID "specular texture" undefined) catch (print (getCurrentException()))
			try (currentbumpMap = getSetRageV MatID SubID "bump texture" undefined) catch (print (getCurrentException()))
			
			if (currentdiffMap == undefined) then try (currentdiffMap = meditMaterials[matid].materialList[subid].diffuseMap.filename) catch()
			if (currentspecMap == undefined) then try (currentspecMap =meditMaterials[matid].materialList[subid].specularLevelMap.filename) catch()
			if (currentbumpMap == undefined) then try (currentbumpMap =meditMaterials[matid].materialList[subid].BumpMap.filename) catch()
			if (currentbumpMap == undefined) then try (currentbumpMap =meditMaterials[matid].materialList[subid].BumpMap.Normal_Map.filename) catch()

	
				
			append subidnames subname
			append diffmaps currentdiffMap
			append specmaps currentspecMap
			append bumpmaps currentbumpMap
		)
		
		-- Create a standard material multisub
		-- Create the base RAGE shader
		meditMaterials[MatId] = Multimaterial ()
		meditMaterials[Matid] = Multimaterial numsubs: howmanysubs

		for subid = 1 to howmanysubs do (

			if (diffmaps[subid] != undefined) then try (meditMaterials[matid].materialList[subid].diffuseMap = Bitmaptexture fileName:diffmaps[subid]) catch()
			if (diffmaps[subid] != undefined) then try (meditMaterials[matid].materialList[subid].diffuseMap.alphaSource = 2) catch()
			if (specmaps[subid] != undefined) then try (meditMaterials[matid].materialList[subid].specularLevelMap = Bitmaptexture fileName:specmaps[subid]) catch()
			if (specmaps[subid] != undefined) then try (meditMaterials[matid].materialList[subid].specularLevelMap.alphaSource = 2) catch()
				
			if (bumpmaps[subid] != undefined) then try (meditMaterials[matid].materialList[subid].bumpMap = Normal_Bump ()) catch()
			if (bumpmaps[subid] != undefined) then try (meditMaterials[matid].materialList[subid].bumpMapAmount = 100) catch()
			if (bumpmaps[subid] != undefined) then try (meditMaterials[matid].materialList[subid].bumpMap.normal_Map = Bitmaptexture fileName:bumpmaps[subid]) catch()
		)
	)

	
	else
	(
		print "There's a problem with the material"
		print (classof theMaterial)
	)
	
)--end ragetomax



-------------------------------------------------------------------------------------------------------------------------
rollout Map_Shader_Editor "Map Shader Editor"
(
	button btnUpdateShaders "Update Shaders" width:140 height:50
	
	on Map_Shader_Editor open do
	(
		for sm = 1 to sceneMaterials.count do
		(
			append sceneMatArr sceneMaterials[sm]
			append sceneMatArrCheck false
		)
		messagebox "This script will attempt to remember what materials it has updated.\r\nPlease keep open until you have finished updating ALL of the necessary materials."
	)
	
	on btnUpdateShaders pressed do
	(
		-- Use sceneMaterials
		mlib = meditMaterials
		mlib.count

		-- Create object for storing normals
		objectsToFix = getcurrentselection()

		-- Make a backup of the material in slot 24

		-- For each selected object, fix the bumpiness
		for obj in objectsToFix do (
			-- set material 24 to the objects material
			try
			(
				meditMaterials[24] = obj.material
				RageToMax 24
				obj.material = meditMaterials[24]
			)

			catch (print (getCurrentException()))
		)	

	)
)--end Map_Shader_Editor
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
createDialog Map_Shader_Editor