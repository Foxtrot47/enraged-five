-- Shader value editor
-- Rick Stirling
-- Rockstar North
-- July 2010

-- Update 28/07/10 Stewart Wright - added checks for material types and added ui elements

-- Script initially designed to halve the value of all normal maps in a SCENE
-- Will it work with containers?
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- Load the common functions
filein "rockstar/export/settings.ms"

-- Figure out the project data
theProjectRoot = RsConfigGetProjRootDir()
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()
theProjectConfig = RsConfigGetProjBinConfigDir()

-- Load common functions	
filein (theWildWest + "script/max/Rockstar_North/character/includes/FN_common.ms")


filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

-------------------------------------------------------------------------------------------------------------------------
mlib = undefined
sceneMatArrCheck = #()
sceneMatArr = #()

-- Get or set a rage shader variables
-- Pass in the material and the slot type we are looking for, eg "bumpiness", "diffuse texture"
-- Pass in a value to set, "undefined" gets ignored
fn getSetRageV MatID SubID slottype setSlotValue =
(
	-- Arrays for storing data
	RageSlotNames =#()
	RageSlotTypes =#()
	
	if SubID != undefined then
	(
		-- Need to know the shadertype, this dictates the slot we need
		RageShaderName = RstGetShaderName mlib[matid].materialList[subid]
		RageShaderSlotCount = RstGetVariableCount mlib[matid].materialList[subid] 
		
		-- Populate the arrays
		for ss = 1 to RageShaderSlotCount do
		(
			append RageSlotNames (lowercase (RstGetVariableName mlib[matid].materialList[subid] ss))
			append RageSlotTypes (RstGetVariableType mlib[matid].materialList[subid] ss)
		)

		-- Now we have a mapping, lets find the index of the slottype we are interested in.
		shaderSlot = finditem RageSlotNames slottype

		returnValue = undefined
		
		if setSlotValue == undefined then
		(
			returnValue = RstGetVariable mlib[MatID].materialList[SubID] shaderslot SetSlotValue
		)
		else
		(
			RstSetVariable mlib[MatID].materialList[SubID] shaderslot SetSlotValue
		)
	)
	else
	(
		-- Need to know the shadertype, this dictates the slot we need
		RageShaderName = RstGetShaderName mlib[matid]
		RageShaderSlotCount = RstGetVariableCount mlib[matid] 
			
		-- Populate the arrays
		for ss = 1 to RageShaderSlotCount do
		(
			append RageSlotNames (lowercase (RstGetVariableName mlib[matid] ss))
			append RageSlotTypes (RstGetVariableType mlib[matid] ss)
		)

		-- Now we have a mapping, lets find the index of the slottype we are interested in.
		shaderSlot = finditem RageSlotNames slottype

		returnValue = undefined
		
		if setSlotValue == undefined then
		(
			returnValue = RstGetVariable mlib[matid] shaderslot SetSlotValue
		)
		else
		(
			RstSetVariable mlib[matid] shaderslot SetSlotValue
		)
	)
)--end getSetRageV
-------------------------------------------------------------------------------------------------------------------------

fn halveNormals matid= (
	slottype = "bumpiness"
	currentNormalValue = undefined
	
	theMaterial = mlib[matid]
	
	if classof theMaterial == Rage_Shader then
	(
		SubID = undefined
		
		try (currentNormalValue = getSetRageV MatID SubID slottype undefined) catch (print (getCurrentException()))
		print currentNormalValue
		newNormalValue = currentNormalValue
		
		try (newNormalValue = currentNormalValue * 0.5) catch(print (getCurrentException()))
		print newNormalValue 
		
		try (setNormalValue = getSetRageV MatID SubID slottype newNormalValue) catch(print (getCurrentException()))
	)
	
	else if classof theMaterial == Multimaterial then
	(
		howmanysubs = mlib[matid].numsubs

		for subid = 1 to howmanysubs do
		(
			-- get the current normal value
			subname = mlib[matid][subid].name
			try (currentNormalValue = getSetRageV MatID SubID slottype undefined) catch (print (getCurrentException()))
			print currentNormalValue
			newNormalValue = currentNormalValue
			
			try (newNormalValue = currentNormalValue * 0.5) catch(print (getCurrentException()))
			print newNormalValue 
			
			try (setNormalValue = getSetRageV MatID SubID slottype newNormalValue) catch(print (getCurrentException()))
		)
	)
	
	else
	(
		print "There's a problem with the material"
		print (classof theMaterial)
	)
)--end halveNormals
-------------------------------------------------------------------------------------------------------------------------
rollout Map_Shader_Editor "Map Shader Editor"
(
	button btnUpdateShaders "Update Shaders" width:140 height:50
	
	on Map_Shader_Editor open do
	(
		for sm = 1 to sceneMaterials.count do
		(
			append sceneMatArr sceneMaterials[sm]
			append sceneMatArrCheck false
		)
		messagebox "This script will attempt to remember what materials it has updated.\r\nPlease keep open until you have finished updating ALL of the necessary materials."
	)
	
	on btnUpdateShaders pressed do
	(
		-- Use sceneMaterials
		mlib = meditMaterials
		mlib.count

		-- Create object for storing normals
		objectsToFix = getcurrentselection()

		-- Make a backup of the material in slot 24
		Box lengthsegs:1 widthsegs:1 heightsegs:1 length:1 width:1 height:1 mapcoords:on pos:[0,0,0] isSelected:on
		$.name = "matHolder"
		$matHolder.material = meditMaterials[24]

		-- For each selected object, fix the bumpiness
		for obj in objectsToFix do (
			-- set material 24 to the objects material
			try
			(
				whatMat = obj.material
				whereInArr = findItem sceneMatArr whatMat
				if sceneMatArrCheck[whereInArr] == false then
				(	
					printText = sceneMatArr[whereInArr][1] as string
					print ("Starting to Update Material: " + printText)
					
					meditMaterials[24] = obj.material
					halveNormals 24
					
					print ("Finished Material Update on: " + printText)
					sceneMatArrCheck[whereInArr] = true
				)
				else
				(
					print "Material has already been updated"
				)
			) catch (print (getCurrentException()))
		)	

		meditMaterials[24] = $matHolder.material
		delete $matHolder

		select objectsToFix
	)
)--end Map_Shader_Editor
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
createDialog Map_Shader_Editor