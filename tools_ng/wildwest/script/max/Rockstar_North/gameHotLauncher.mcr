macroScript GameHotLauncher category:"gameHotLauncher" icon:#("LaunchIcon", 1) tooltip:"Launch game with last Settings"
(
	tempDir = getDir #temp
	configFile = pathConfig.appendPath tempDir "gameLauncher.ini"
	
	--get the last command
	if hasINISetting configFile "Session" "LastCommand" then
	(
		command = getINISetting configFile "Session" "LastCommand"
		
		--3..2..1..0!
		HiddenDOSCommand command startpath:(RsConfigGetBuildDir()) donotwait:true
	)
	
	
)

