-- Water-shader replacer
-- Replaces all geometry-shaders with name "water_*" with a blue standardMaterial, to make it easier to pick out of top-down renders.
-- Rockstar Leeds
-- Neal D Corbett
-- 06/06/2012

(
	local newWaterMat = standardMaterial name:"waterRender" diffuseColor:blue
	local waterPattern = "water_*"
	
	local selObjs = #()
	
	for obj in geometry do 
	(
		local objMat = obj.material

		local foundObj = false

		case classOf objMat of 
		(
			Rage_Shader:
			(
				local shadername = RstGetShaderName objMat
				
				if matchPattern shadername pattern:waterPattern do
				(			
					obj.material = newWaterMat
					foundObj = true
				)
			)
			Multimaterial:
			(
				-- Get the map-IDs used on the selected object, and only check those materials:
				local matNums = #()
				RsMatGetMapIdsUsedOnObjectWithCount obj matNums
				
				local maxMatId = objMat.numSubs
				
				for matNum in matNums do 
				(
					-- Deal with out-of-range matIds:
					if (matNum > maxMatId) do 
					(
						matNum = (mod matNum maxMatId) as integer
						if (matNum == 0) do (matNum = maxMatId)
					)
					
					local matIdx = findItem objMat.materialIdList matNum
					
					if matIdx != 0 do 
					(
						local subMat = objMat.materialList[matIdx]
						
						if (isKindOf subMat Rage_Shader) do 
						(
							local shadername = RstGetShaderName subMat
						
							if matchPattern shadername pattern:waterPattern do
							(			
								objMat.materialList[matIdx] = newWaterMat
								foundObj = true
							)
						)
					)
				)
			)
		)
		
		if foundObj do (append selObjs obj)
	)
	
	select selObjs
)