-- Converts VehicleNode links in scene into a spline object
-- Neal D Corbett
-- R*Leeds
-- 08/09/2011

(
	local timeStart = timeStamp()
	
	local nodeObjs = for obj in objects where isKindOf obj vehicleNode collect obj
	local endNodes = for obj in nodeObjs where ((VehNodeNumConn obj) != 2) collect obj
	
	local endsDone = #()
	local lineNodesDone = #()
	local lineList = #()
	
	progressStart "Finding path-lines:"
	
	for endNodeNum = 1 to endNodes.count while (progressUpdate (100.0 * endNodeNum/endNodes.count)) do 
	(
		local obj = endNodes[endNodeNum]
		RSsetBigArrayVal endsDone obj.handle true
		
		local nodeLines = #()
		
		for pathNum = 1 to (VehNodeNumConn obj) do 
		(
			local childNode = try (VehNodeGetConn obj pathNum) catch undefined
			
			if childNode != undefined do 
			(
				local childHandle = childNode.handle
				
				-- Don't create lines to already-processed endpoints or line-nodes:
				if ((RSgetBigArrayVal lineNodesDone childHandle) == undefined) and ((RSgetBigArrayVal endsDone childHandle) == undefined) do 
				(
					local parentNode = obj
					local lineNodes = #(parentNode)
					append nodeLines lineNodes
					
					local keepAdding = true
					
					while keepAdding do 
					(
						append lineNodes childNode
						
						if (VehNodeNumConn childNode) == 2 then 
						(
							RSsetBigArrayVal lineNodesDone childNode.handle true
							
							local currentNode = childNode
							childNode = undefined
							
							for n = 1 to 2 while (childNode == undefined) do 
							(
								childNode = try (VehNodeGetConn currentNode n) catch undefined
								
								if (childNode != undefined) and (childNode == parentNode) do 
								(
									childNode = undefined
								)
							)
							
							parentNode = currentNode
							
							if (childNode == undefined) do (keepAdding = false)
						)
						else 
						(
							keepAdding = false
						)
					)
				)
			)
		)
		
		join lineList nodeLines 
	)
	
	progressEnd()
	
	if (lineList.count != 0) do 
	(
		local roadsLineObj = line()

		for splineNum = 1 to lineList.count do 
		(
			addNewSpline roadsLineObj

			for obj in lineList[splineNum] do 
			(
				addKnot roadsLineObj splineNum #corner #curve obj.pos [0,0,0] [0,0,0]
			)
		)
		
		select roadsLineObj
	)
	
	format "Time taken: % seconds\n" ((TimeStamp() - TimeStart) / 1000.0)
)