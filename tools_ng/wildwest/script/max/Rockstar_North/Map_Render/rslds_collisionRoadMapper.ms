--
-- Rockstar Leeds
-- 01/03/2011
-- Neal D Corbett
--
-- Converts selected collision-mesh objects to meshes, and removes faces/objects that don't use road collision-types.
-- This allows objects to be cut down to just the faces that probably should be shown on the game's overhead map.
--
(
	local colObjs = for obj in selection where isKindOf obj Col_Mesh collect obj
	
	fn isRoadMat mat = 
	(
		(isKindOf mat RexBoundMtl) and 
		(
			local colName = RexGetCollisionName mat
			
			case of 
			(
				(matchPattern colName pattern:"tarmac*"):true
				(matchPattern colName pattern:"*manhole*"):true
				(matchPattern colName pattern:"*_track"):true
				default:false
			)
			true
		)
	)
	
	for obj in colObjs do 
	(
		col2Mesh obj
		
		local objMatList, objMatIDList, matCount
		local roadMats = #{}
		
		if isKindOf obj.material multimaterial then 
		(
			local objMatList = obj.material.materialList
			local objMatIDList = obj.material.materialIDList
			matCount = obj.material.count
			
			-- Make list of road-collision matIDs:
			for matListNum = 1 to objMatList.count do 
			(
				roadMats[objMatIDList[matListNum]] = (isRoadMat objMatList[matListNum])
			)
		)
		else 
		(
			matCount = 1
			
			roadMats[1] = (isRoadMat obj.material)
		)
		
		local delFaces = #{}
		delFaces.count = obj.numFaces
		
		for faceNum = 1 to obj.numFaces do 
		(
			matNum = getFaceMatID obj faceNum
			
			if (matNum > matCount) do 
			(
				matNum = (mod matNum matCount) as integer
				if (matNum == 0) do (matNum = matCount)
			)
			
			delFaces[faceNum] = not roadMats[matNum]
		)
		
		-- Remove non-road faces, or delete object if it has none:
		if (delFaces.numberSet == delFaces.count) then 
		(
			delete obj
		)
		else 
		(
			meshop.deleteFaces obj delFaces
		)
	)
	
	colObjs = (for obj in colObjs where isValidNode obj collect obj)
	colObjs.material = standardMaterial diffuse:white
	
	clearSelection()
	select colObjs
)
