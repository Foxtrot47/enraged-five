fn RsGetChilds obj outVal:#() = 
(
	join outVal obj.children
	
	for child in obj.children do 
	(
		RsGetChilds child outVal:outVal
	)
	
	return outVal
)

fn RsRenderVehicles = 
(
	if not CheckForSave() do 
	(
		return false
	)
	
	local vehicleNames = for filename in (getFiles (RsConfigGetAssetsDir() + "vehicles/*.xml")) collect (toLower (getFilenameFile filename))
		
	local vehicleMaxfiles = #()
	local dirPattern = "*"
		
	for vehTypeDir in getDirectories (RsConfigGetArtDir() + "vehicles/*") where (matchpattern vehTypeDir pattern:dirPattern) do 
	(
		for filename in getFiles (vehTypeDir + "*.max") where (findItem vehicleNames (toLower (getFilenameFile filename))) != 0 do 
		(
			append vehicleMaxfiles filename
		)
	)
	
	local renderBmp
	
	local renderDir = "x:/vehicleRenders/"
	makeDir renderDir
	
	local renderMat = InkNPaint paint_on:off
	
	for filename in vehicleMaxfiles while not keyboard.escPressed do 
	(
		gc()
		loadMaxFile filename quiet:True missingExtFilesAction:#logmsg missingDLLsAction:#logmsg
		
		delete lights
		local vehicleName = getFilenameFile filename
		local rootDummy = getNodeByName vehicleName
		
		if (rootDummy != undefined) do 
		(
			objects.isHidden = true
			
			local visObjs = RsGetChilds rootDummy
			visObjs.isHidden = false
			visObjs.material = renderMat
			
			local minExtent
			local maxExtent
			
			for obj in visObjs where (superClassOf obj == GeometryClass) do 
			(
				if (minExtent == undefined) or (obj.min.y < minExtent) do minExtent = obj.min.y
				if (maxExtent == undefined) or (obj.max.y > maxExtent) do maxExtent = obj.max.y
			)
			
			local renderCam = TargetCamera target:(TargetObject()) orthoProjection:true fov:27
			select renderCam
			viewport.setType #view_camera
				
			renderCam.target.pos = [rootDummy.pos.x , minExtent + ((maxExtent - minExtent) * 0.5), 2]
			renderCam.pos = renderCam.target.pos + [-20, 0, 0]
		/*
			local groundPlane = box width:20 length:20 height:-0.01
			groundPlane.pos.y = renderCam.pos.y
			groundPlane.material = renderMat
		*/
			backgroundColor = white
			lightLevel = 100.0
			ambientColor = white
			useEnvironmentMap = False
			SceneExposureControl.exposureControl = undefined
			RadiosityPreferences.computeRadiosity = false

			renderBmp = render camera:renderCam outputwidth:800 outputheight:600 outputfile:(renderDir + vehicleName + ".bmp") pos:#vfb_vprimary_position
		)
	)

	resetMaxFile #noPrompt
	
	if (renderBmp != undefined) do 
	(
		unDisplay renderBmp
		shellLaunch "explorer.exe" (RsMakeBackSlashes renderDir)
	)

)

RsRenderVehicles()