-- Common script headers, setup paths and load common functions
try (filein "rockstar/export/settings.ms") catch()

-- Figure out the project
theProject = RSConfigGetProjectName()
theWildWest = RsConfigGetWildWestDir()

filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())

filein (theWildWest + "script/max/Rockstar_North/character/Shader_Tools/C_Vertex_Bake_Painter.ms")