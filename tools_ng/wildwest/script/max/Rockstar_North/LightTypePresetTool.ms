-------------------------------
-- Light type preset tool --
-------------------------------
filein (RsConfigGetWildWestDir() + "script/max/Rockstar_North/character/includes/FN_oldWWTU.ms")
OldWWCollectToolUsageData (getThisScriptFilename())


--Author: Neil Gregory
--Date: 6 April 2011
--Version: 0.1

--------------
--Includes
--------------
try( filein "pipeline/util/xml.ms") catch()

try( filein "script/stratup/objects/util/ragelight.ms") catch()

struct LightPresetTool
(
	RsLightTypePresetXMLFile = (RsConfigGetWildWestDir() + "/script/max/Rockstar_North/LightTypePresets.xml"),
	gLightPresetDict = dotnetobject "RSG.MaxUtils.MaxDictionary",
	gUIColours = #(),
	UIColSwatch = undefined,
	UI_ddPreset = undefined,
	flashinessValues = #("Constant", "Random", "Random -On if Wet", "One a second", "Two a second", "Five a second", \
							"RandomFlashiness", "Off (Traffic Light)", "UNDEFINED", "Alarm", "On when Raining", "Cycle 1", \
							"Cycle 2", "Cycle 3", "Disco", "Candle", "Plane", "Fire"),
	
	----------------
	--Functions
	----------------
	fn typeCast inType inValue =
	(
		outValue = undefined
		
		case inType of
		(
			"bool":
			(
				outValue = inValue as booleanClass
			)
			
			"int":
			(
				outValue = inValue as Integer
			)
			
			"float":
			(
				outValue = inValue as Float
			)
			
			"string":
			(
				outValue = inValue as String
			)
			
			"Point3":
			(
				--val[1] = val[1] as Point3
				pa = filterString inValue "[],"
				outValue = point3 ((pa[1] as float) * 255) ((pa[2] as float)*255) ((pa[3] as float)*255)
			)
			
			
		) --end case
		
		outValue
	
	),
	
	fn floatToColourArray RGBArray = 
	(
		rgbOut = #()
		for element in RGBArray do
		(
			conv = int(element * 255)
			append rgbOut conv
		)
		
		rgbOut
	),
	
	fn presetAttrToPoint3 input =
	(
		pa = filterString input "[],"
		pa[1] = (pa[1] as float) 
		pa[2] = (pa[2] as float) 
		pa[3] = (pa[3] as float) 
		val = point3 pa[1] pa[2] pa[3]
		val
	),
	
	--return the attributes an xml element has as an array
	fn getElementAttrValues element = 
	(
		attrs = #()
		
		--format "Element: %\n" element.name
		--print element.Attributes
		
		--check its not a comment
		if element.name != "#comment" and element.HasAttributes == true then
		(
			--format "Element: % has attributes" element.name
			attrCollection = element.Attributes
			--test = attrCollection.count
			--print test
			/*
			struct XmlAttr
			(
				value,
				type
			)
			
			*/	
			for attr = 0 to (attrCollection.Count - 1) do
			(
				--attr struct
				--attrDict = XmlAttr()
				--show attrCollection.ItemOf(attr)
				--attrDict.value = attrCollection.ItemOf[attr].name
				--attrDict.type = attrCollection.ItemOf[attr].value
			
				append attrs attrCollection.ItemOf[attr].value
			)
		)
		--return
		attrs
	),
	
	
	fn getLightTypePresets =
	(
		stream = dotNetObject "System.IO.StreamReader" RsLightTypePresetXMLFile
		local XML = dotNetObject "System.Xml.XmlDocument"
		XML.load stream
		root = XML.documentElement
		
		presets = root.selectNodes("Preset")
		
		--Child node values dictionary builder
		---------------------------------------------------------------
		fn buildValuesDict nodeList =
		(
			nodeListIt = nodeList.getEnumerator()
			dict = dotnetobject "RSG.MaxUtils.MaxDictionary"
			--local timeAttrs = #()
			
			while nodeListIt.moveNext() != false do
			(
				thisKid = nodeListIt.current
				--get node attrs value, type
				thisKidAttrs = thisKid.attributes
				
				--TimeDict				key		values:( value						type				)
				dict.add thisKid.name #(thisKidAttrs.ItemOf["value"].value, thisKidAttrs.ItemOf["type"].value)
			)
			
			--return 
			dict
		)
		---------------------------------------------------------------
		
		for p = 0 to presets.count - 1 do
		(
			presetNode = presets.itemOf[p]
			presetNodeAttrs = presetNode.attributes
			UIName = presetNodeAttrs.ItemOf["name"].value
			-----------------------------------
			--create light preset Dict
			-----------------------------------
			presetDict = dotnetobject "RSG.MaxUtils.MaxDictionary"
			
			-------------------
			--get children
			-------------------
			presetKids = presetNode.selectNodes("*")
			
			--iterate kids
			presetKidsIt = presetKids.getEnumerator()
			while presetKidsIt.MoveNext() != false do
			(
				--node
				presetKidNode = presetKidsIt.current
				--get name
				nodeName = presetKidNode.name
				
				--add time or light photo elements first then the reset
				case nodeName of
				(
					"GtaTime":
					(
						--get child nodes
						timeKids = presetKidNode.selectNodes("*")
						GtaTimeDict = buildValuesDict(timeKids)

						presetDict.add "GtaTime" GtaTimeDict
					)
					
					"GtaLightPhoto":
					(
						--get child nodes
						photoKids = presetKidNode.selectNodes("*")
						GtaPhotoDict = buildValuesDict(photoKids)

						presetDict.add "GtaLightPhoto" GtaPhotoDict
					)
					
					default:
					(
						--get node attrs
						--print nodeName
						thisKidAttrs = presetKidNode.attributes
						presetDict.add nodeName #(thisKidAttrs.ItemOf["value"].value, thisKidAttrs.ItemOf["type"].value)
					)
				)
			)
			
			--append it to the list of presets
			gLightPresetDict.add UIName presetDict
		)
		
		
		stream.dispose()
		stream = undefined
	),
	
	--LightTypePresets array returned
	--This function parses the xml file that contains the presets and creates structs that are added to a struct array
	--and from that an array containing the names of the presets to populate the dropdownlist with.
	
	
	--Gets the value of the specified attribute from the desired preset and returns it.
	fn getPresetValue presetName presetAttr = 
	(
		--return value
		val = undefined
		
		--dict
		preset = gLightPresetDict.Item presetName
		--format "preset: %\n" preset
		
		--quick out where nested dicts are not concerned
		if preset.containsKey presetAttr then return preset.item presetAttr
		
		--get value
		presetIt = preset.getEnumerator()
		while presetIt.movenext() != false do
		(
			kvPair = presetIt.current
			case kvPair.key of
			(
				"GtaTime":
				(
					dict = kvPair.value
					if dict.containsKey presetAttr then
					(
						val = dict.Item presetAttr
						return val
					)
				)
				
				"GtaLightPhoto":
				(
					dict = kvPair.value
					if dict.containsKey presetAttr then
					(
						val = dict.Item presetAttr
						return val
					)
				)
				
				default:
				(
					if kvPair.key == presetAttr then
					(
						val = kvPair.value
						return val
					)
				)
			)
			
		)
		/*
		if preset.containsKey presetAttr then
		(
			val = preset.Item presetAttr
		)
		*/
		--if val == undefined then format "Problem getting the preset value: %\n" presetAttr
		--format "val: % \n" val
		val
	),
	
	
	--Apply the preset chosen in the UI to the currently selected RAGE light(s)
	fn applyPreset presetName = 
	(
		
		selObjs = for o in getCurrentSelection() where classOf o == RageLight collect o
		--check we have something
		if selObjs.count == 0 then
		(
			messagebox "No objects selected"
		)
		
		--check for invalid preset like a divider
		if matchPattern presetName pattern:"--*--" == true then
		(
			return 0
		)
		
		--iterate through object selection
		
		--NOTE use this style? local refObjs = for obj in geometry where isRSref obj collect obj
		for obj in selObjs do
		(
			--if we got RageLights then apply the preset settings
			--if classOf obj == RageLight then
			--(
				-------------------------------------
				--Set ordinary properties first
				------------------------------------
				p = getPropNames obj
				
				--skip last property as it can changed name dependent on setting
				for i = 1 to p.count - 1 do
				(
					--get value from config
					--format "property: %\n" p[i]
					attr = p[i] as string
					--format "attr: %\n" attr
					
					val = getPresetValue presetName attr
					--format "val: %\n" val
					if val != undefined then
					(
						--adjust value based on type
						val[1] = typeCast val[2] val[1]
						
						--Set the property
						setProperty obj p[i] val[1]
					)

				)
				
				------------------------------------
				--Set GTA attributes
				------------------------------------
				
				--Gta LightPhoto
				attrGta_Count = GetNumAttr "Gta LightPhoto"
				timeAttrsDict = getPresetValue presetName "GtaTime" --is a dict
				lightPhotoAttrsDict = getPresetValue presetName "GtaLightPhoto" --is a dict
				
				--GtaLightPhotoAttrs = timeAttrs + lightPhotoAttrs
				
				index = 1 --First attr is a node id hash we dont store
				--iterators
				timeAttrsIt = timeAttrsDict.values.getEnumerator()
				timeLightPhotoIt = lightPhotoAttrsDict.values.getEnumerator()
				--check iterator counts against number of gta lightphoto attrs
				--if attrGta_Count != (timeAttrsIt.count + timeLightPhotoIt.count) then messagebox "attr count mismatch: 350" title:"Error"
				
				--iterate GtaTime dict first
				while timeAttrsIt.moveNext() != false do
				(
					thisAttr = timeAttrsIt.current
					val = typeCast thisAttr[2] thisAttr[1]
					SetAttr obj index val
					index += 1
				)
				
				--iterate GtaLightPhoto dict first
				timeLightPhotoIt = lightPhotoAttrsDict.values.getEnumerator()
				while timeLightPhotoIt.moveNext() != false do
				(
					thisAttr = timeLightPhotoIt.current
					val = typeCast thisAttr[2] thisAttr[1]
					SetAttr obj index val
					index += 1
				)
				
				/*
				for index =1 to attrGta_Count do
				(
					
					attr = GtaLightPhotoAttrs[index]
					--format "index: % attr[1]: % attr[2] %\n" index attr[1] attr[2]
					
					val = typeCast attr[2] attr[1]
					
					SetAttr obj index val
					
				)
				*/
				--set the object data for future reference
				setAppData obj 1 presetName
				
			--)
		)
		
		--update the ragelights ui
		--$.refreshUI()
	),
	
	fn selectedPresetQuery = 
	(
		presetType = "None"
		--find what the current preset for this light is
		sel = getCurrentSelection()
		--print sel
		lightsOnly = for n in sel where classOf n == RageLight collect n
		
		presets = for n in lightsOnly where (getAppData n 1) != undefined collect getAppData n 1
		
		--check if all the same or not
		if presets.count > 1 then
		(
			--check for uniformity
			for p = 1 to presets.count - 1 do
			(
				if presets[p] != presets[p+1] then
				(
					presetType = "Mixed"
					LightTypePresetTool.txtPresetValue.text = presetType as string
					return presetType
					
				)
				else
				(
					presetType = presets[1]
				)
			)
		)
		else if presets.count == 1 then
		(
			presetType = presets[1]
		)
		else
		(
			presetType = "None"
		)
		
		--return
		--format "PresetType: % \n" presetType
		--set the UI for the selected
		LightTypePresetTool.txtPresetValue.text = presetType as string
		
		--return
		presetType
	),
	
	fn selectByPreset preset = 
	(
		--print preset
		--get all the RageLights in the scene
		rageLights = for l in $lights where classOf l == RageLight collect l
			
		--check each for its current preset
		matches = for l in rageLights where (getAppData l 1) == preset collect l
		--print matches
		--select them
		clearSelection()
		select matches
		
	)
	
)--END STRUCT

lightPresetTool = LightPresetTool()


--Destroy the UI if its already opened
if ((LightTypePresetTool != undefined) and (LightTypePresetTool.isDisplayed)) do
(
	destroyDialog LightTypePresetTool
)

if (PresetInfo != undefined and PresetInfo.isDisplayed) do destroyDialog PresetInfo

--create UI


rollout PresetAttrs "Preset Attrs"
(
	--build UI
	local bitmapX =130
	local bitmapY = 40
	local theCanvasBitmap = bitmap bitmapX bitmapY color:gray
	
		--label infolbl "RAGE Light scripted plugin\nAuthor:Gunnar Droege\nRockstar North 2010" pos:[5,0] height:40
	--checkbox enabledCheckbox "Enabled" 
	--dropdownlist cmbType "Type:" items:gRageLightTypes
	colorpicker colourSwatch "light colour: "
	spinner spnIntensity "Intensity:" type:#float value:1.0 align:#left
	spinner spnAttenEnd "Range:" type:#float range:[0,1000,50] width:95 align:#left
	group "Spot Light Params"
	(
		spinner spnHotspot "Hotspot:" type:#float range:[0,1000,1]
		spinner spnFallsize "Radial Falloff:" type:#float range:[0,1000,1]
		--mapbutton buttShadowMap "<<Shadow map>>" width:120
		checkbox chckSquare "Square Projection" checked:false
	)
	/*
	group "Longitudinal Falloff"
	(
		spinner spnExp "Exponent" type:#float range:[0,64,1] scale:0.01
		bitmap bmpExpCurve "" width:bitmapX height:bitmapY bitmap:theCanvasBitmap
	)
	
	group "Sausage light params"
	(
		spinner spnWidthFalloff "Width Falloff:" type:#float range:[0,1000,1]
	)
	group "Viewport Mesh"
	(
		spinner spnRes "Resolution" type:#integer range:[5,100,1] enabled:false
		spinner spnSize "Mesh Size" type:#float range:[0.1,100,1]
		colorpicker pickCol "Mesh Colour" width:100 height:20 across:2
		checkbox chkMeshColour "" checked:false align:#right
	)
	*/
	
	on PresetAttrs open do
	(
		local presetName = ::LightTypePresetTool.ddPreset.selected
		local getPresetValue = lightPresetTool.getPresetValue
		
		--colour
		local col = getPresetValue presetName "lightColour"
		col = lightPresetTool.typeCast "Point3" col[1]
		colourSwatch.color = (color col.x col.y col.z)
		
		--Intensity
		local intensity = getPresetValue presetName "lightIntensity"
		spnIntensity.value = intensity[1] as float
		
		--attenuation end
		local attenEnd = getPresetValue presetName "lightAttenEnd"
		spnAttenEnd.value = attenEnd[1] as float
		
		--Longitudanal falloff
		--local long_fallof = getPresetValue presetName
	)
	
)


rollout PresetInfo "Preset Spec"
(
	subrollout standardAttrs "Main Properties" height:190
	subRollout gtaTime "GtaTime_Rollout" height:200
	subrollout gtaLightPhoto "Gta Light Photo" height:510
)

rcmenu RCPresetInfo
(
	menuItem infoItem "Preset Info"
	
	
	fn createTimeAttrsRollout = 
	(
		rciName = "GtaTime_Rollout"
		rci = rolloutCreator rciName "Gta Time"
		rci.begin()
		
		local presetName = ::LightTypePresetTool.ddPreset.selected
		presetDict = lightPresetTool.gLightPresetDict.item[presetName]
		gtaTimeDict = presetDict.item["GtaTime"]
		gtaTimeDictIt = gtaTimeDict.getEnumerator()
		counter = 1
		
		while gtaTimeDictIt.moveNext() != false do
		(
			kv = gtaTimeDictIt.current
			local params = " checked:"+kv.value[1] as string
			if counter < 25 then
			(
				if (mod counter 4) as integer == 1 then
						params += " across:4 "
				rci.addControl #checkbox ("chkHr"+kv.key as string) (substring kv.key 3 10) paramStr:params
			)
			else
			(
				rci.addControl #checkbox ("chkHr"+kv.key as string) (kv.key as string) paramStr:params
			)
			
			counter += 1
		)
		
		/*
		--add hour switches
		for h = 1 to 24 do
		(
			local params = " checked:"+aGtaTimeAttrs[h][1] as string
			if (mod h 4) as integer == 1 then
					params += " across:4 "
			rci.addControl #checkbox ("chkHr"+h as string) (h as string) paramStr:params
			
		)
		--add last 2 attrs
		local params = " checked:"+aGtaTimeAttrs[25][1] as string
		rci.addControl #checkbox "chkTimeObj" "Is time object" paramStr:params
		
		params = " checked:"+aGtaTimeAttrs[26][1] as string
		rci.addControl #checkbox "chkAllowVanish" "Allow Vanish Whilst Viewed" paramStr:params
		*/
		rci.end()
	)
	
	fn createGtaPhotoAttrsRollout = 
	(
		--local flashinessValues = #("Constant", "Random", "Random -On if Wet", "One a second", "Two a second", "Five a second", \
		--							"RandomFlashiness", "Off (Traffic Light)", "UNDEFINED", "Alarm", "On when Raining", "Cycle 1", \
		--							"Cycle 2", "Cycle 3", "Disco", "Candle", "Plane", "Fire")
									
		local presetName = ::LightTypePresetTool.ddPreset.selected
		presetDict = lightPresetTool.gLightPresetDict.item[presetName]
		gtaTimeAttrs = presetDict.item["GtaLightPhoto"]
		gtaTimeAttrsIt = gtaTimeAttrs.getEnumerator()
		
		
		rciName = "GtaLightPhoto_Rollout"
		rci = rolloutCreator rciName "Gta Light Photo"
		rci.begin()
		
		while gtaTimeAttrsIt.moveNext() != false do
		(
			kv = gtaTimeAttrsIt.current
			v = kv.value
			format "key: % v1: % v2: %\n" kv.key v[1] v[2]
			--print kv.key
			case v[2] of
			(
				"float": --spinner
				(
					params = (" value:"+v[1] as string)
					--format "spinner params: %\n" params
					rci.addControl #spinner ("spnGtaLightPhoto"+kv.key) kv.key paramStr:params
				)
				
				"int":	
				(
					
					if kv.key == "Flashiness" then
					(
						params = " items:lightPresetTool.flashinessValues selection:"+v[1]
						rci.addControl #dropdownlist ("spnGtaLightPhoto"+kv.key) kv.key paramStr:params
					)
					else
					(
						params = " type:#integer" + (" value:"+v[1] as string)
						rci.addControl #spinner ("spnGtaLightPhoto"+kv.key) kv.key paramStr:params
					)
				)
				
				"string": --edittext
				(
					rci.addControl #edittext ("spnGtaLightPhoto"+kv.key) kv.key paramStr:(" text:"+"\""+v[1] as string+"\"")
				)
				
				"bool":	--checkbox
				(
					rci.addControl #checkbox ("spnGtaLightPhoto"+kv.key) kv.key paramStr:(" checked:"+v[1] as string)
				)
			)
		)
		
		--iterate through the attr array creating controls as we go.
		--assume an integer type is a dropdown unless otherwise specified
		/*
		for attr = 1 to aGtaTimeAttrs.count do
		(
			ctrlType = aGtaTimeAttrs[attr][2]
			
			case ctrlType of
			(
				
				
				
			)
		)
		*/
		rci.end()
		
	)
	
	--call the info function on select
	on infoItem picked do
	(
		print "info!"
		
		createTimeAttrsRollout()
		createGtaPhotoAttrsRollout()
		
		CreateDialog PresetInfo 220 900
		addSubRollout PresetInfo.standardAttrs PresetAttrs
		--PresetInfo.standardAttrs.height = 400
		
		--Gta Time
		--createTimeAttrsRollout()
		addSubRollout PresetInfo.gtaTime ::GtaTime_Rollout
		--PresetInfo.gtaTime.height = 300
		--CreateAttrRoll otherRollouts
		
		--Gta LightPhoto
		addSubRollout PresetInfo.gtaLightPhoto ::GtaLightPhoto_Rollout
		--PresetInfo.gtaLightPhoto.height = 300
	)
	
)

rollout LightTypePresetTool "LightType Preset Tool"
(
	--Head
	button btnLogo tooltip:"Feedback" border:false align:#left offset:[0, 2] images:#("RockStarNorthLogo.bmp", "RockStarNorthLogo_Mask.bmp", 1, 1, 1, 1, 1, false) across:2
	button btnHelp tooltip:"Help?" border:false align:#right offset:[0, 2] images:#("HelpIcon_32.bmp", "HelpIcon_32a.bmp", 1, 1, 1, 1, 1, false)
	groupbox grpHeader width:267 height:46 pos:[4, 0]
	
	group "Preset"
	(
		dropdownlist ddPreset "" width:200 align:#left across:2
		colorpicker colSwatch "" color:[0, 0, 0] modal:true align:#right offset:[0, 0]
		edittext txtPresetValue "Selection:" text:"None" readOnly:true bold:true
		
		button btnSelectPreset "Select Preset" across:2 width:125 offset:[-1, 0]
		button btnSelectSimilar "Select Similar" width:125
		button btnApply "Apply To Selected" width:250
	)
	
	--button btnCreate "Create Light"
	
	--version info
	--group ""
	--(
	--	label lblInfo  "V0.9" style_sunkenedge:true width:240 height:16
	--)
	------------------------
	--Events
	------------------------
	----------------
	--UI
	----------------
	--function to handle lame dropDownList handler work around
	fn ddPreset_selected preset = 
	(
		--if passed a divider item then move to the next menu item
		if matchPattern preset pattern:"--*--" == true then
		(
			--get the current menuitem index and increment by 1
			index  = ddPreset.selection + 1
			ddPreset.selection = index
			
			--Now bung this back into the preset var via its string value
			preset = ddPreset.selected
		)
		
		--use dictionary to llokuo colSwatch colour
		--presetDict = gLightPresetDict.Item preset
		UIColour = lightPresetTool.getPresetValue preset "lightColour"
		
		format "UIColour: %\n" UIColour
		colour = lightPresetTool.presetAttrToPoint3 UIColour[1]
		colour = lightPresetTool.floatToColourArray #(colour[1], colour[2], colour[3])
		lightPresetTool.UIColSwatch.color = color colour[1] colour[2] colour[3]
		--format "Swatch Now: %\n" UIColSwatch.color
	)
	
	--help menu
	on btnHelp pressed do
	(
		process = dotnetobject "System.Diagnostics.Process"
		process.start "https://devstar.rockstargames.com/wiki/index.php/LightPresetTool"
		
		process.dispose()
	)
	
	--Right click context preset info
	on ddPreset rightClick do
	(
		PopUpMenu RCPresetInfo pos:[10, 70] rollout:LightTypePresetTool	
	)
	
	--Select lights matching current preset  menu item
	on btnSelectPreset pressed do
	(
		lightPresetTool.selectByPreset ddPreset.selected
	)
	
	--Select lights with the same preset as the currently selected light 
	on btnSelectSimilar pressed do
	(
		lightPresetTool.selectByPreset txtPresetValue.text
		--lightPresetTool.selectBySimilar txtPresetValue.text
	)
	
	--Apply to selected event
	on btnApply pressed do
	(
		opt = ddPreset.selected
		--print opt
		
		--check for valid selection
		if matchpattern opt pattern:"--*--" == false then
		(
			--print opt
			lightPresetTool.applyPreset (opt)
		)
	)
	
	--On dropdown selection changed modify the colour swatch colour
	on ddPreset selected itm do
	(
		ddPreset_selected (ddPreset.selected)
	)
	
	on LightTypePresetTool open do
	(
		--Assign UI controls to global var for menu select event to work
		lightPresetTool.UIColSwatch = colSwatch
		lightPresetTool.UI_ddPreset = ddPreset
		
		--set colSwatch to disabled, its just for show
		colSwatch.enabled = true
		
		-----------------------------------
		--Get presets from xml config
		-----------------------------------
		lightPresetTool.getLightTypePresets()
		
		UINames = #()
		
		--Build dropdown list names
		
		--Get enumerator
		it = lightPresetTool.gLightPresetDict.Keys.GetEnumerator()
		while it.MoveNext() != false do
		(
			--get name
			append UINames it.Current
			
			if matchPattern it.current pattern:"--*--" == true then
			(
				UIColour = #("[0.0, 0.0, 0.0]", "Point3")
				append lightPresetTool.gUIColours #(it.Current, UIColour)
			)
			else
			(
				presetDict = lightPresetTool.gLightPresetDict.Item it.Current
				UIColour = presetDict.Item "lightColour"
				append lightPresetTool.gUIColours #(it.Current, UIColour)
			)
		)
			
		--populate dropdownlist items
		ddPreset.Items = UINames
		--make sure first chosen preset is not a divider
		if matchPattern ddPreset.Items[1] pattern:"--*--" == true then
		(
			ddPreset.selection = 2
			ddPreset_selected ddPreset.Items[2]
		)
		
		--selection change callaback
		callbacks.removeScripts id:#lightPreset
		callbacks.addscript #selectionSetChanged "lightPresetTool.selectedPresetQuery()" id:#lightPreset
	)
	
	on LightTypePresetTool close do
	(
		callbacks.removeScripts id:#lightPreset
		
		--kill the preset info window if open
		if (PresetInfo.isDisplayed) do destroyDialog PresetInfo
	)
	
	/*
	on btnCreate pressed do
	(
		opt = ddPreset.selected
		print opt
	)
	*/
	
)

createDialog LightTypePresetTool 275 175
