(
	query = queryBox "This will grab latest wildwest tools and then shutdown max\n Do you want to continue?" title:"Proceed?"
	if query == false then return 0
	
	--connect to perforce if we need to
	if gRsPerforce.connected() == false then gRsPerforce.connect()
	
	--get the latest wildwest tools and the max menus
	gRsPerforce.p4.run "sync"  #(RsConfigGetToolsDir() + "dcc/current/max2012/ui/macroscripts/rswildwest.mcr",
											RsConfigGetToolsDir() + "dcc/current/max2012/ui/macroscripts/rstecharttools.mcr",
											RsConfigGetToolsDir() + "dcc/current/max2012/scripts/pipeline/rswildwest.ms",
											RsConfigGetToolsDir() + "dcc/current/max2012/scripts/pipeline/rstecharttools.ms",
											RsConfigGetWildWestDir() + "...")
								
	quitMax()
)


