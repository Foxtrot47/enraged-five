(
	--connect to perforce if we need to
	if gRsPerforce.connected() == false then gRsPerforce.connect()
	
	--get the latest wildwest tools and the max menus
	gRsPerforce.p4.run "sync" #((RsConfigGetWildwestDir()+ "..."))
)
