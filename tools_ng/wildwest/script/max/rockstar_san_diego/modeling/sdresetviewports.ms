--///////////////////////////////////////////////////////////////////////////////////////////////////////////
--
--	      :!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	  :!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	 '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Copyright (c) Rockstar, San Diego
--	!!!!!!!!!!!!!!!RRRRRRRRRRRR!!!!!!!!!!!!!!!!  A division of Rockstar Games Inc. & Take Two Interactive
--	!!!!!!!!!!!!!!!R           RR!!!!!!!!!!!!!!	
--	!!!!!!!!!!!!!!!R             R!!!!!!!!!!!!!
--	!!!!!!!!!!!!!!R    RRRRRR    R!!!!!!!!!!!!!  sdResetViewports.ms 
--	!!!!!!!!!!!!!!R    RRRRRR    R!!!!!!!!!!!!!
--	!!!!!!!!!!!!!!R    ````    .R!!!!!!!!!!!!!!  Version: Max 2009
--	!!!!!!!!!!!!!R            RR!!!!!!!!!!!!!!!
--	!!!!!!!!!!!!!R    RRRRR    R!!!!!!!!!!!!!!!
--	!!!!!!!!!!!!R    R!!!!R    R!!! !!!!!!!!!!!
--	!!!!!!!!!!!!R    R!!!!R    R!! *!!!!!!!!!!!  ROCKSTAR SAN DIEGO DISCLAIMS ALL WARRANTIES WITH REGARD
--	!!!!!!!!!!!!R....R!!!!R.   RX **'!!!!!!!!!!  TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF 
--	!!!!!!!!!!!!RRRRRR!!!!RRRRRR.***.... .!!!!!  MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL ROCKSTAR
--	!!!!!!!!!!!!!!!!!!!!!!!!! ********":!!!!!!!  SAN DIEGO BE LIABLE FOR ANY SPECIAL, INDIRECT OR 
--	!!!!!!!!!!!!!!!!!!!!!!!!!! ****** !!!!!!!!!  CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
--	!!!!!!!!!!!!!!!!!!!!!!!!! *** ***'!!!!!!!!!  FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
--	!!!!!!!!!!!!!!!!!!!!!!!!! *:!!!.* !!!!!!!!!  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
--	!!!!!!!!!!!!!!!!!!!!!!!!.!!!!!!!!:!!!!!!!!!  OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF 
--	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  THIS SOFTWARE.
--	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	    `~!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!~~	     MODIFY AT YOUR OWN RISK!
--
--///////////////////////////////////////////////////////////////////////////////////////////////////////////--script to weld at very low tolerance

fn sdResetViewports =
(
	disableSceneRedraw()

	--get the active viewport and its ID
	temp = viewport.activeViewport
	tempID = viewport.GetID temp

	--initialize flag to determine whether viewports are maximized
	maxedFlag = 0
	
	--if IDs dont match this means viewport is maximized on view other than 1
	if tempID != temp do
	(
		--set the original held viewport to the correct index flip the flag, run maximize to go to 4 view mode
		temp = tempID
		maxedFlag = 1
		max tool maximize
	)
	
	--set 4 panel layout
	viewport.setLayout #layout_4
	
	--set each panel to its default view, 1 last so its the active if we were maxed
	viewport.activeViewport = 4
	viewport.setType #view_persp_user
	viewport.activeViewport = 3
	viewport.setType #view_left
	viewport.activeViewport = 2
	viewport.setType #view_front
	viewport.activeViewport = 1
	viewport.setType #view_top
	
	--set viewport back to original selection
	viewport.activeViewport = temp
	enableSceneRedraw()
	completeRedraw()
		
	--if we flipped to 4 view earlier flip back to maxed view
	if maxedFlag == 1 then
	(
		max tool maximize
	)else
	(
		--if we didnt have to flip then flip 4 times to clean up bad drawing issues max gives us for free
		max tool maximize
		max tool maximize
		max tool maximize
		max tool maximize
	)
)
sdResetViewports()