--///////////////////////////////////////////////////////////////////////////////////////////////////////////
--
--	      :!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	  :!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	 '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  Copyright (c) Rockstar, San Diego
--	!!!!!!!!!!!!!!!RRRRRRRRRRRR!!!!!!!!!!!!!!!!  A division of Rockstar Games Inc. & Take Two Interactive
--	!!!!!!!!!!!!!!!R           RR!!!!!!!!!!!!!!	
--	!!!!!!!!!!!!!!!R             R!!!!!!!!!!!!!
--	!!!!!!!!!!!!!!R    RRRRRR    R!!!!!!!!!!!!!  sdShowAllSubobjs.ms 
--	!!!!!!!!!!!!!!R    RRRRRR    R!!!!!!!!!!!!!
--	!!!!!!!!!!!!!!R    ````    .R!!!!!!!!!!!!!!  Version: Max 2009
--	!!!!!!!!!!!!!R            RR!!!!!!!!!!!!!!!
--	!!!!!!!!!!!!!R    RRRRR    R!!!!!!!!!!!!!!!
--	!!!!!!!!!!!!R    R!!!!R    R!!! !!!!!!!!!!!
--	!!!!!!!!!!!!R    R!!!!R    R!! *!!!!!!!!!!!  ROCKSTAR SAN DIEGO DISCLAIMS ALL WARRANTIES WITH REGARD
--	!!!!!!!!!!!!R....R!!!!R.   RX **'!!!!!!!!!!  TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF 
--	!!!!!!!!!!!!RRRRRR!!!!RRRRRR.***.... .!!!!!  MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL ROCKSTAR
--	!!!!!!!!!!!!!!!!!!!!!!!!! ********":!!!!!!!  SAN DIEGO BE LIABLE FOR ANY SPECIAL, INDIRECT OR 
--	!!!!!!!!!!!!!!!!!!!!!!!!!! ****** !!!!!!!!!  CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
--	!!!!!!!!!!!!!!!!!!!!!!!!! *** ***'!!!!!!!!!  FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
--	!!!!!!!!!!!!!!!!!!!!!!!!! *:!!!.* !!!!!!!!!  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
--	!!!!!!!!!!!!!!!!!!!!!!!!.!!!!!!!!:!!!!!!!!!  OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF 
--	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  THIS SOFTWARE.
--	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--	    `~!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!~~	     MODIFY AT YOUR OWN RISK!
--
--///////////////////////////////////////////////////////////////////////////////////////////////////////////--script to weld at very low tolerance

fn sdShowAllSubobjs =
(
	if $ != undefined do
	(	
		buffer = getCurrentSelection()
	)
	
	for curr in geometry do
	(					
		if classof curr == Editable_Poly do
		(
			curr.EditablePoly.unhideAll #Face
			curr.EditablePoly.unhideAll #Vertex
			curr.EditablePoly.unhideAll #Object
		)
	)

	if buffer != undefined do
	(	
			select buffer
	)
)
sdShowAllSubobjs()