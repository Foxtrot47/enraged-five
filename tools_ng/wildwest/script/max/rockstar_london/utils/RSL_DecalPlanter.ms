clearlistener()

try (destroyDialog rollout_DecalPlanter) catch ()

rollout rollout_DecalPlanter "Decal Planter" width:160 height:370
(
	spinner spn1 "Offset Value"  width:80 height:16 range:[0,10.0,0.01] type:#float scale:0.01 across:1
	checkbutton chk_int "interactive" width:70 align:#right
	radioButtons rdo_axis_dir "Axis"  width:103 height:30 labels:#("X", "Y", "Z") default:3
	
	button btn_source "Add Source Decals"  width:140 height:22
	button btn_clear "Clear List"  width:140 height:22
	listbox lstbox "Decals to be planted:"
	button btn_target "Pick Target Surface" width:140 height:22
	button btn_Plant "Plant!" width:119 height:44
	
	-- Globals
	local axis_dir = [0,0,-1] -- Default to Z
	local target_mesh = undefined
	local source_mesh = undefined
	local offset = 0.01;
	local offsetDir = [0,0,-0.1]
	local source_Decals = #()
	
	struct objInfo
	(
		name,
		node		
	)
	
	
	-- Functions --
	-- Used to make sure we select geometry from the pick object button
	fn g_filter o = (superClassOf o == GeometryClass)
	
	
	-- Update the Ray Direction
	fn updateButton = 
		(
		axis_dir = case rdo_axis_dir.state of 
			(
			1: [-1,0,0] 
			2: [0,-1,0] 
			3: [0,0,-1] 
			)
		)
		
	fn getObjDir obj =
	(
		case axis_dir of
		(
			([-1,0,0]):obj.max.z 
			([0,-1,0]):obj.max.y 
			([0,0,-1]):obj.max.x
		)
	)
		
	fn getOffset  =
	(
		offset = spn1.value
		
		offsetDir = case axis_dir of 
		(
			([-1,0,0]): [-offset,0,0]
			([0,-1,0]): [0,-offset,0]
			([0,0,-1]): [0,0,-offset]
		)
	)
	
	
	-- Check source and target object to create a vector
	-- Do a dot product to get angle direction
	-- use angle to do intersection means we don't need to specify which direction we want to porject the decals
	fn getdirection sourceObj targetObj =
	(
		vector = sourceObj - targetObj
		
	)
	
		
	-- Do raycat for each vertice against the target obejc tand return the intersection point
	fn find_intersection targetNodeZ sourceNodeZ =
	( 
		--print ("targetNode =" +targetNodeZ as string)
		--print ("SOurceNode = "+sourceNodeZ as string)
	
	 local testRay = ray sourceNodeZ [0,0,-1] 
	 local nodeMaxZ = getObjDir(targetNodeZ) --targetNodeZ.max.z 
	--testRay.pos.z = nodeMaxZ + 0.0001 * abs nodeMaxZ 
	print 	("targetNodeZ = "+targetNodeZ as string)
	print ("testRay ="+testRay as string)
		
	intersectRay targetNodeZ testRay 
		
	)
	
	-- Proces the vertices in teh source object
	fn processObjectVertices decalObj targetObj=
	(
		--print decalObj
		
		decalObj = convertToMesh(decalObj)
		targetObj = convertToMesh(targetObj)
		
		--local objNVerts  = polyop.getNumVerts decalObj		-- This will include dead verts
		local objNVerts =  meshop.getNumVerts decalObj
		--print objNVerts
		for i = 1 to objNVerts do
		(
			--vertpos = polyop.getVert decalObj i
			vertpos = meshop.getVert decalObj i
			--print vertpos			
			-- We then need to move each vert a specififed distnace from the source object
			int_point = find_intersection targetObj vertpos
			--print int_point
			if int_point != undefined then 
			(	
				--polyop.setVert decalObj i (int_point.pos - getOffset() )	
				meshop.setVert decalObj i (int_point.pos - getOffset() )	
			)
		)
		decalObj = convertToMesh(decalObj)
		targetObj = convertToMesh(targetObj)
	)
	
	fn UpdateListBox  =
	(
		lstbox.items = #()
		local tempobj = #()
		for obj in source_Decals do
		(
			append tempobj obj.name
		)
		lstbox.items = tempobj
		
	)
	
	fn updateDecal =
	(
		
	)
	

	---- EVENT HANDLERS ---
		
	on rdo_axis_dir changed state do updateButton()
	
	on btn_clear pressed do
	(
		lstbox.items = #()
		source_Decals = #()
	)
	
	on btn_source pressed do
	(
		currentSel = $
		print currentSel
		if currentSel!= undefined then
		(
			--local listarry = #()
			for eachObj in currentSel do
			(
				if g_filter eachObj then
				(
					local newobjInfo = objInfo()
					newobjInfo.name = eachObj.name
					newobjInfo.node = eachObj
					
					local toadd = true
					
					for i in source_Decals do
					(
						if i.node == newobjInfo.node then toadd = false
					)
					
					
					if toadd then
						append source_Decals newobjInfo
				)
			)
			
			print source_Decals
			UpdateListBox()
			
		)
		/*
		source_mesh = pickObject message:"Pick Decal Surface:" filter:g_filter
		if source_mesh != undefined then
		(
		btn_source.text = source_mesh.name
		)
		*/
	)
	
	on btn_target pressed do
	(
		
		
		target_mesh = pickObject message:"Pick Target Object:" filter:g_filter
		if target_mesh != undefined then
		(
		btn_target.text = target_mesh.name
		)
		
	)
	
	on btn_Plant pressed do
	(
		if lstbox.items.count == 0 then messagebox "Please Pick at least 1 Source Object"
			else if target_mesh == undefined then messagebox "Please Pick Target surface Object"
				else
				(		
						for decal in source_Decals do
						(
							processObjectVertices decal.node target_mesh
							CenterPivot decal.node
						)
				)
	)
	
	
	on spn1 changed Svalue do 
	(
		if chk_int.checked then
		(
			for decal in source_Decals do
				(
					processObjectVertices decal.node target_mesh
						CenterPivot decal.node
				)
		)
	)
	
)
createDialog rollout_DecalPlanter
