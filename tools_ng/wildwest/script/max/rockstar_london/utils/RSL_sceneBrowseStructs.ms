
struct RSL_sceneBrowseData
(
	type,
	masterArray = #(),
	currentArray = #(),
	listViewCache = #(),
	columnArray = #(),
	sortArray = #(),
	first = 0,
	
	fn sortAlphabetical v1 v2 =
	(
-- 		format "% > % = %\n" v1.name v2.name (v1.name > v2.name)
		
		local name1 = toLower (getFilenameFile (v1.name as string))
		local name2 = toLower (getFilenameFile (v2.name as string))
		
		case of
		(
			(name1 > name2): 1
			(name1 < name2): -1
			default:0
		)
	),
	
	fn sortSurface v1 v2 =
	(
-- 		format "% > % = %\n" v1.name v2.name (v1.name > v2.name)
		
		local name1 = toLower (getFilenameFile (v1.surfaceType as string))
		local name2 = toLower (getFilenameFile (v2.surfaceType as string))
		
		case of
		(
			(name1 > name2): 1
			(name1 < name2): -1
			default:sortAlphabetical v1 v2
		)
	),
	
	fn sortTextureCount v1 v2 =
	(
		case of
		(
			(v1.textureArray.count > v2.textureArray.count): -1
			(v1.textureArray.count < v2.textureArray.count): 1
			default:sortAlphabetical v1 v2
		)
	),
	
	fn sortMaterialCount v1 v2 =
	(
		case of
		(
			(v1.materialArray.count > v2.materialArray.count): -1
			(v1.materialArray.count < v2.materialArray.count): 1
			default:sortAlphabetical v1 v2
		)
	),
	
	fn sortObjectCount v1 v2 =
	(
		case of
		(
			(v1.objectArray.count > v2.objectArray.count): -1
			(v1.objectArray.count < v2.objectArray.count): 1
			default:sortAlphabetical v1 v2
		)
	),
	
	fn sortSharedCount v1 v2 =
	(
		case of
		(
			(v1.sharedArray.count > v2.sharedArray.count): -1
			(v1.sharedArray.count < v2.sharedArray.count): 1
			default:sortAlphabetical v1 v2
		)
	),
	
	fn sortTXDCount v1 v2 =
	(
		case of
		(
			(v1.TXDArray.count > v2.TXDArray.count): -1
			(v1.TXDArray.count < v2.TXDArray.count): 1
			default:sortAlphabetical v1 v2
		)
	),
	
	fn sortSize v1 v2 =
	(
		local result = (v1.size.x * v1.size.y) - (v2.size.x * v2.size.y) 
		
		case of
		(
			(result < 0.0): 1
			(result > 0.0): -1
			default:sortAlphabetical v1 v2
		)
	),
	
	fn sortBy sortType =
	(
-- 		print sortType
		case sortType of
		(
			"Name": --Alphabetical
			(
				qSort masterArray sortAlphabetical
			)
			"Surface": --Alphabetical
			(
				qSort masterArray sortSurface
			)
			"Textures": --Texture Count
			(
				qSort masterArray sortTextureCount
			)
			"Materials": --Material Count
			(
				qSort masterArray sortMaterialCount
			)
			"Objects": --Object Count
			(
				qSort masterArray sortObjectCount
			)
			"Shared": --Shared Count
			(
				qSort masterArray sortSharedCount
			)
			"TXDs": --TXD Count
			(
				qSort masterArray sortTXDCount
			)
			"Size":
			(
				qSort masterArray sortSize
			)
		)
	),
	
	fn indexOf name =
	(
		local result = 0
		
		for i = 1 to masterArray.count do 
		(
			if (toLower masterArray[i].name) == (toLower name) then
			(
				result = i
			)
		)
		
		result
	),
	
	fn initialise =
	(
		currentArray =  for entry in masterArray collect entry
		listViewCache = #()
		first = 0
	),
	
	fn reset =
	(
		masterArray = #()
		currentArray = #()
		listViewCache = #()
		columnArray = #()
		sortArray = #()
		first = 0
	)
)

struct RSL_SceneBrowseObject
(
	name,
	node,
	thumbnail = RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\images\\texturebrowseiconobject_image.png",
	TXD,
	textureArray = #(),
	materialArray = #(),
	
	fn isUsedInObject object ID =
	(
		local tempMesh = snapshotasmesh object
		local result = false
		
		for face in tempMesh.faces do
		(
			if (getFaceMatID tempMesh face.index) == ID then
			(
				result = true
				exit
			)
		)
		
		delete tempMesh
		
		result
	),
	
	fn getProperties searchArray =
	(
		materialArray = #()
		local material = node.material
		
		if (classOf material) == Multimaterial then
		(
			for i = 1 to material.materialList.count do 
			(
				local subMaterial = material.materialList[i]
				local ID = material.materialIDList[i]
				
				if (isUsedInObject node ID) then
				(
					append materialArray subMaterial
				)
			)
		)
		else
		(
			if material != undefined then
			(
				materialArray = #(material)
			)
		)
		
		textureArray = #()
		for entry in searchArray do 
		(
			if (findItem entry.objectArray node) != 0 then
			(
				append textureArray entry
			)
		)
		
	),
	
	fn toTreeView rootTreeNode =
	(
		local nameNode = rootTreeNode.Nodes.Add name
		
		local TXDNode = rootTreeNode.Nodes.Add TXD
		
		local materialsNode = nameNode.Nodes.Add ("Materials : " + materialArray.count as string)
		materialsNode.tag = dotNetMXSValue materialArray
		
		for entry in materialArray do 
		(
			local materialNode = materialsNode.Nodes.Add (entry.name + " : " + (classOf entry) as string)
			materialNode.tag = dotNetMXSValue entry
		)
		materialsNode.expand()
		
		local texturesNode = nameNode.Nodes.Add ("textures : " + textureArray.count as string)
		texturesNode.tag = dotNetMXSValue materialArray
		
		for entry in textureArray do 
		(
			local textureNode = texturesNode.Nodes.Add entry.name
			textureNode.tag = dotNetMXSValue entry
		)
		texturesNode.expand()
		
		nameNode.expand()
	),
	
	fn toListView listView =
	(
		local txdItem = dotNetObject "ListViewItem" "TXD"
		txdItem.subItems.add TXD
		
		local materialsItems = dotNetObject "ListViewItem" "Materials"
		materialsItems.subItems.add (materialArray.count as string)
		materialsItems.tag = dotNetMXSValue materialArray
		
		local texturesItem = dotNetObject "ListViewItem" "Textures"
		texturesItem.subItems.add (textureArray.count as string)
		texturesItem.tag = dotNetMXSValue textureArray
		
		listView.items.addRange #(txdItem, materialsItems, texturesItem)
	),
	
	fn toString =
	(
	)
)

struct RSL_SceneBrowseTXD
(
	name,
	thumbnail = RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\images\\texturebrowseicontxd_image.png",
	textureArray = #(),
	objectArray = #(),
	sharedArray = #(),
	
	fn appendIfTextureUnique texture =
	(
		local found = false
		
		for entry in textureArray do 
		(
			if entry.name == texture.name then
			(
				found = true
			)
		)
		
		if not found then
		(
			append textureArray texture
		)
	),
	
	mapped fn appendIfFound object texture =
	(
		if (findItem texture.objectArray object) != 0 then
		(
			appendIfTextureUnique texture
		)
	),
	
	fn getProperties searchArray =
	(
		textureArray = #()
		for entry in searchArray do 
		(
			appendIfFound objectArray entry
		)
		
		sharedArray = #()
		for entry in textureArray do 
		(
			if entry.TXDArray.count > 1 then
			(
				append sharedArray entry
			)
		)
	),
	
	fn toTreeView rootTreeNode =
	(
		local nameNode = rootTreeNode.Nodes.Add name
		
		local texturesNode = nameNode.Nodes.Add ("textures : " + textureArray.count as string)
		texturesNode.tag = dotNetMXSValue materialArray
		
		for entry in textureArray do 
		(
			local textureNode = texturesNode.Nodes.Add entry.name
			textureNode.tag = dotNetMXSValue entry
		)
		texturesNode.expand()
		
		local sharedNode = nameNode.Nodes.Add ("Shared : " + sharedArray.count as string)
		sharedNode.tag = dotNetMXSValue sharedArray
		
		for entry in sharedArray do 
		(
			local textureNode = sharedNode.Nodes.Add (entry.name)
			textureNode.tag = dotNetMXSValue entry
		)
		sharedNode.expand()
		
		local objectsNode = nameNode.Nodes.Add ("Objects : " + objectArray.count as string)
		objectsNode.tag = dotNetMXSValue objectArray
		
		for entry in objectArray do 
		(
			local objectNode = objectsNode.Nodes.Add (entry.name)
			objectNode.tag = dotNetMXSValue entry
		)
		objectsNode.expand()
		
		nameNode.expand()
	),
	
	fn toListView listView =
	(
		local texturesItem = dotNetObject "ListViewItem" "Textures"
		texturesItem.subItems.add (textureArray.count as string)
		texturesItem.tag = dotNetMXSValue textureArray
		
		local sharedItem = dotNetObject "ListViewItem" "Shared Textures"
		sharedItem.subItems.add (sharedArray.count as string)
		sharedItem.tag = dotNetMXSValue sharedArray
		
		local objectsItem = dotNetObject "ListViewItem" "Objects"
		objectsItem.subItems.add (objectArray.count as string)
		objectsItem.tag = dotNetMXSValue objectArray
		
		listView.items.addRange #(texturesItem, sharedItem, objectsItem)
	),
	
	fn toString =
	(
	)
)

struct RSL_SceneBrowseBitmap
(
	name,
	filename,
	thumbnail,
	map,
	size = [0,0],
	surfaceType,
	materialArray = #(),
	objectArray = #(),
	TXDArray = #(),
	
	idxTXD = getattrindex "Gta Object" "TXD",
	globalINIfile =  rsConfig.toolsroot + "\\tools\\dcc\\current\\max2009\\tex2col\\global.ini",
	
	fn getBitDepth bitmap =
	(
		local info = getBitmapInfo bitmap
		local RGBbits = info[5]
		local Alphabits = info[6]
		
		local result = (RGBbits * 3) + Alphabits
	),
	
	fn isUsedInObject object ID =
	(
		local tempMesh = snapshotasmesh object
		local result = false
		
		for face in tempMesh.faces do
		(
			if (getFaceMatID tempMesh face.index) == ID then
			(
				result = true
				exit
			)
		)
		
		delete tempMesh
		
		result
	),
	
	fn appendObjectIfUnique object =
	(
		local found = false
		
		for entry in objectArray do 
		(
			if entry.name == object.name then
			(
				found = true
			)
		)
		
		if not found then
		(
			append objectArray object
		)
	),
	
	mapped fn materialUsedOnObject object material =
	(
		if (getAttrClass object) == "Gta Object" and (classOf object) != XRefObject then
		(
			local TXD = getAttr object idxTXD
			
			if object.material == material then
			(
				appendIfUnique objectArray object
				appendIfUnique TXDArray TXD
			)
			else
			(
				if (classOf object.material) == Multimaterial then
				(
					local index = findItem object.material.materialList material
					
					if index != 0 then
					(
						local id = object.material.materialIDList[index]
						
						if (isUsedInObject object id) then
						(
							appendIfUnique objectArray object
							appendIfUnique TXDArray TXD
						)
					)
				)
			)
		)
	),
	
	mapped fn getObjectsAndTXDs material =
	(
		local objArray = refs.dependentNodes material
		
		materialUsedOnObject objArray material
	),
	
	fn isReadable =
	(
		try
		(
			local temp = dotNetObject RS_dotNetClass.bitmapClass filename
			temp.dispose()
			true
		)
		catch
		(
			false
		)
	),
	
	fn getProperties =
	(
		if (doesFileExist filename) and (isReadable()) then
		(
			size = [map.bitmap.width as integer, map.bitmap.height as integer]
			
			local bitDepth = getBitDepth filename
			
			if bitDepth < 24 or bitDepth > 32 then
			(
				thumbnail = RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\images\\TextureBrowseWrongBitDepth.png"
			)
			else
			(
				thumbnail = filename
			)
		)
		else
		(
			if not (isReadable()) then
			(
				format "IMAGE UNREADABLE: %\n" filename
			)
			
			thumbnail = RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\images\\TextureBrowseMissingImage.png"
		)
		
		surfaceType = getINISetting globalINIfile "match" Name
		if surfaceType == "" then surfaceType = undefined
		
		getObjectsAndTXDs materialArray
	),
	
	mapped fn removeReplace material newMap =
	(
		local count = getNumSubTexmaps material
		
		for i = 1 to count do 
		(
			local subMap = getSubTexmap material i
			
			if subMap == map then
			(
				setSubTexmap material i newMap
			)
		)
	),
	
	fn removeFromMaterials =
	(
		removeReplace materialArray undefined
	),
	
	fn replaceWith newMap =
	(
		removeReplace materialArray newMap.map
		
		for material in materialArray do
		(
			appendIfUnique newMap.materialArray material
		)
		
		for object in objectArray do 
		(
			appendIfUnique newMap.objectArray object
		)
		
		getObjectsAndTXDs materialArray
	),
	
	fn getNewBitmap =
	(
		local newBitmap = selectBitMap caption:"Pick new bitmap..."
		
		if newBitmap != undefined then
		(
			name = (filenameFromPath newBitmap.filename)
			filename = newBitmap.filename
			size = [map.bitmap.width as integer, map.bitmap.height as integer]
			
			local bitDepth = getBitDepth filename
			
			if bitDepth < 24 or bitDepth > 32 then
			(
				thumbnail = RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\images\\TextureBrowseWrongBitDepth.png"
			)
			else
			(
				thumbnail = filename
			)
			
			local newMap = Bitmaptexture bitmap:newBitmap filename:filename
			
			removeReplace materialArray newMap
			map = newMap
			
			true
		)
		else
		(
			false
		)
	),
	
	fn toTreeView rootTreeNode =
	(
		local nameNode = rootTreeNode.Nodes.Add name
-- 		nameNode.tag = dotNetMXSValue (DataPair maxFile:maxFile xmlFile:xmlFile)
		
		local sizeNode = nameNode.Nodes.Add ("Size : " + size.x as string + " x " + size.y as string)
		
		local surfaceTypeNode = nameNode.Nodes.Add ("Surface Type : " + surfaceType as string)
		
		local materialsNode = nameNode.Nodes.Add ("Materials : " + materialArray.count as string)
		materialsNode.tag = dotNetMXSValue materialArray
		
		for entry in materialArray do 
		(
			local materialNode = materialsNode.Nodes.Add (entry.name + " : " + (classOf entry) as string)
			materialNode.tag = dotNetMXSValue entry
		)
		materialsNode.expand()
		
		local objectsNode = nameNode.Nodes.Add ("Objects : " + objectArray.count as string)
		objectsNode.tag = dotNetMXSValue objectArray
		
		for entry in objectArray do 
		(
			local objectNode = objectsNode.Nodes.Add (entry.name)
			objectNode.tag = dotNetMXSValue entry
		)
		objectsNode.expand()
		
		local TXDsNode = nameNode.Nodes.Add ("TXDs : " + TXDArray.count as string)
		
		for entry in TXDArray do 
		(
			local TXDNode = TXDsNode.Nodes.Add entry
		)
		TXDsNode.expand()
		
		nameNode.expand()
	),
	
	fn toListView listView =
	(
		local sizeItem = dotNetObject "ListViewItem" "Size"
		sizeItem.subItems.add ((size.x as integer) as string + " x " + (size.y as integer) as string)
		
		local sufaceTypeItem = dotNetObject "ListViewItem" "Surface Type"
		sufaceTypeItem.subItems.add (surfaceType as string)
		
		local materialsItems = dotNetObject "ListViewItem" "Materials"
		materialsItems.subItems.add (materialArray.count as string)
		materialsItems.tag = dotNetMXSValue materialArray
		
		local objectsItem = dotNetObject "ListViewItem" "Objects"
		objectsItem.subItems.add (objectArray.count as string)
		objectsItem.tag = dotNetMXSValue objectArray
		
		local TXDsItem = dotNetObject "ListViewItem" "TXDs"
		TXDsItem.subItems.add (TXDArray.count as string)
		TXDsItem.tag = dotNetMXSValue TXDArray
		
		listView.items.addRange #(sizeItem, sufaceTypeItem, materialsItems, objectsItem, TXDsItem)
	),
	
	fn toString =
	(
		format "Name:%\n" name
		format "\tFilename:%\n" filename
		format "\tThumbnail:%\n\n" thumbnail
	)
)

-- struct RSL_bitmap
-- (
-- 	filename,
-- 	materials,
-- )