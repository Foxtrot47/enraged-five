
fn RsSetAllCollisionToNotAvoidByPeds = 
(
	for childobj in rootnode.children do
	(
		
		if classof childobj == Editable_poly or classof childobj == Editable_mesh or classof childobj == PolyMeshObject then
		(
			-- Check if object does NOT have attribute "Is Door".
			idxAttribute = getattrindex "Gta Object" "Attribute"
			if getattr childobj idxAttribute != 7 then
			(
				idxFixed = getattrindex "Gta Object" "Is Fixed"
				idxDynamic = getattrindex "Gta Object" "Is Dynamic"
				idxFragment = getattrindex "Gta Object" "Is Fragment"
				
				--Check if object is "Dynamic" and is NOT "Fixed".
				if getattr childobj idxDynamic == true and getattr childobj idxFixed == false then
				(
					idxDoesNotAvoidByPeds = getattrindex "Gta Object" "Don't Avoid By Peds"
					
					setattr childobj idxDoesNotAvoidByPeds true				
				)
				--Check if object is "Fragment"
				else
				if getattr childobj idxFragment == true then
				(
					print childobj.name
					idxDoesNotAvoidByPeds = getattrindex "Gta Object" "Don't Avoid By Peds"
					
					setattr childobj idxDoesNotAvoidByPeds true				
				)
			)
		)
	)
)

RsSetAllCollisionToNotAvoidByPeds()