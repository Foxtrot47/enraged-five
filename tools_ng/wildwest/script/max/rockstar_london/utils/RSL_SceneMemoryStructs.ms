
filein "pipeline\\util\\drawablelod.ms"

struct RSL_ObjectMemoryData
(
	name,
	parent,
	node,
	handle = 0,
	memory = 0,
	objectColour,
	materialDataArray = #(),
	childArray = #(),
	
	fn appendMaterial materialData =
	(
		local found = false
		
		for m in materialDataArray do 
		(
			if m.name == materialData.name then found = true
		)
		
		if not found then
		(
			append materialDataArray materialData
		)
	),
	
	fn getMemory =
	(
		memory = 0
		
		local objectFiles
		local type = #streamPayne
		local mapNode = RsProjectContentFind RsMapName type:"map"
		local realName = substituteString name "_frag_" ""
		local file = "*.*"
		
		if parent != undefined then
		(
			realName = substituteString parent "_frag_" ""
			file = name + "*.*"
		)
		
		if not mapNode.instances then
		(
			type = #buildcache
		)
		
		case type of
		(
			#streamPayne:
			(
				objectFiles = getFiles (project.netstream + "/maps/" + (getFilenameFile maxfilename) + "/" + realName + "/" + file)
			)
			#buildcache:
			(
				objectFiles = getFiles (project.network + "/streamPayne/maps/" + (getFilenameFile maxfilename) + "/" + realName + "/" + file)
			)
		)
		
		for file in objectFiles where (getFilenameType file) != ".dds" do 
		(
			memory += ((getFileSize file) / 1024.0)
		)
		
		for child in childArray do 
		(
			child.getMemory()
		)
	),
	
	fn isLODObject object =
	(
		local result = false
		local dependents = refs.dependents object
		
		for entry in dependents where not (isdeleted entry) do
		(
			if (classOf entry) == LodAttributes then
			(
				result = true
				
				exit
			)
		)
		
		result
	),
	
	fn getAreaFacePercentage object =
	(
		local areaVal = 0
		local result = 0
		local testMesh = snapShotAsMesh object
		
		if testMesh.verts.count > 0 then
		(
			for f in testMesh.faces do
			(
				areaVal += meshOp.getFaceArea testMesh f.index
			)
			
			result = testMesh.faces.count / areaVal
			
			if result < 0 then
			(
				result = 0.0
			)
			
			local fCountPerQuarter = 12
			
			if (RsLodDrawable_GetHigherDetailModel node) != undefined or (isLODObject object) then
			(
				fCountPerQuarter = 4
			)
			
			local facePercent = testMesh.faces.count as float / ((areaVal / 0.25) * fCountPerQuarter)
			
			result *= facePercent
			if result > 100 then
			(
				result = 100.0
			)
		)
		else
		(
			result = 100.0
		)
		
		result
	),
	
	fn getObjectColour lookupGradient =
	(
		local areaFacePercent = getAreaFacePercentage node
		local xDim = int ((lookupGradient.width - 1) / 100.0 * areaFacePercent)
		
		objectColour = lookupGradient.GetPixel xDim 0
	),
	
	fn toTreeview treeViewNode this lookupGradient = --lookupGradient
	(
		local objectRoot= treeViewNode.Nodes.Add (name + " - " + memory as string + " K")
		if objectColour == undefined then
		(
			getObjectColour lookupGradient
		)
		
		objectRoot.backColor = objectColour
		objectRoot.tag = dotNetMXSValue this
		
		if childArray.count > 0 then
		(
			local childRoot = objectRoot.Nodes.Add ("Children: " + childArray.count as string)
			
			for child in childArray do 
			(
				child.toTreeview childRoot child lookupGradient
				
-- 				local childNode = childRoot.Nodes.Add (child.name + " - " + child.memory as string + " K" )
-- 				if child.objectColour == undefined then
-- 				(
-- 					child.getObjectColour lookupGradient
-- 				)
-- 				
-- 				childNode.backColor = objectColour
-- 				childNode.tag = dotNetMXSValue child
			)
			
			objectRoot.expand()
		)
		
		local materialRoot = objectRoot.Nodes.Add ("Materials: " + materialDataArray.count as string)
		
		for mat in materialDataArray do 
		(
			local materialNode = materialRoot.Nodes.Add (mat.name + " - " + mat.memory as string + " K")
		)
	)
)

struct RSL_RageShaderMemoryData
(
	name,
	material,
	objectArray = #(),
	textureDataArray = #(),
	TXDArray = #(),
	memory = 0,
	
	memory1024 = 699192,
	memory1024Alpha = 1398256,
	
	fn appendTXD TXD =
	(
		appendIfUnique TXDArray TXD
		
-- 		for texture in textureDataArray do
-- 		(
-- 			appendIfUnique texture.TXDArray TXD
-- 		)
	),
	
	fn calculateMemory size alpha =
	(
		if alpha then
		(
			ceil ((size.x * size.y) / (1024.0 * 1024.0) * memory1024Alpha) / 1024.0
		)
		else
		(
			ceil ((size.x * size.y) / (1024.0 * 1024.0) * memory1024) / 1024.0
		)
	),
	
	fn getMemory scale =
	(
		memory = 0
		
		for textureData in textureDataArray do 
		(
			memory += textureData.memory = calculateMemory (textureData.size * scale) textureData.hasAlpha
		)
	),
	
	fn toTreeView treeViewNode this lookupGradient = --lookupGradient
	(
		local MaterialRoot= treeViewNode.Nodes.Add (this.name + " - " + this.memory as string + " K")
		MaterialRoot.tag = dotNetMXSValue this
		
		for texture in textureDataArray do 
		(
			local textureNode = MaterialRoot.Nodes.Add (texture.name + " - " + texture.memory as string + " K")
		)
	)
)

struct RSL_TextureMemoryData
(
	name = "",
	type,
	memory = 0,
	size = [0,0],
	hasAlpha = false,
	filenameArray = #(),
	materialArray = #(),
	TXD,
	
	fn validateMaterialArray =
	(
		for i = materialArray.count to 1 by -1 do 
		(
			if (isDeleted materialArray[i]) then
			(
				deleteItem materialArray i
			)
		)
	),
	
	fn toTreeview treeViewNode this lookupGradient = --lookupGradient
	(
		local textureNode = treeViewNode.Nodes.Add (this.name + " - " + this.memory as string + " K")
		textureNode.tag = dotNetMXSValue this
	)
)

struct RSL_ParentTXDData
(
	name,
	type,
	textureArray = #(),
	tagData
)
