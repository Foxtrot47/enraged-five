function CleanUpEmptyLayers=
(
	local emptyLayers = #();

	for i = 0 to layerManager.count-1 do
	(
		ilayer = layerManager.getLayer i
		layerName = ilayer.name 
		layer = ILayerManager.getLayerObject i
		layerNodes = refs.dependents layer
			
		layer.Nodes &theNodesTemp

		if theNodesTemp.count == 0  do 
		(
			append emptyLayers (layerName as string)	
		)
	)

	local layerCount = emptyLayers.count;

	for i = 1 to emptyLayers.count do 
	( 
		layermanager.deleteLayerByName emptyLayers[i]
	)
	messageBox ("Deleted " + (layerCount as string) + " empty layers");
)

CleanUpEmptyLayers();
