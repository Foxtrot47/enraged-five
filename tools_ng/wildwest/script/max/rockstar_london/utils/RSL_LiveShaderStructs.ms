
struct RSL_LiveShaderObject
(
	object,
	shaderArray = #(),
	
	fn toTreeView rootTreeNode =
	(
		local nameNode = rootTreeNode.Nodes.Add object.name
		
		for entry in shaderArray do
		(
			entry.toTreeView nameNode object
		)
		
		nameNode.expand()
	)
)

struct RSL_ImportedShader
(
	shader,
	modified = false,
	index,
	instanceArray = #(),
	variableArray = #(),
	
	epsilon = 0.0001,
	
	fn compareInstance importedShader =
	(
		local result = true
		
		for inst in instanceArray do
		(
			local found = false
			
			for entry in importedShader.instanceArray do
			(
-- 				format "% : % = %\n\t\"%\" : \"%\" = %\n" inst.type entry.type (stricmp inst.type entry.type) inst.value entry.value (stricmp inst.value entry.value)
				if inst.type == entry.type and (stricmp inst.value entry.value) == 0 then
				(
					found = true
				)
			)
			
			if not found then
			(
				result = false
			)
		)
		
		result
	),
	
	fn isUpdated variable material =
	(
		local result = true
		local diff
		
		for i = 1 to (RstGetVariableCount material) do 
		(
			local type = RstGetVariableName material i
			local value = RstGetVariable material i
			
			if type == variable.type then
			(
				if value == variable.value then
				(
					result = false
				)
				else
				(
					diff = variable.value - value
				)
			)
		)
		
		case (classOf diff) of
		(
			Float:
			(
				if diff < epsilon then
				(
					result = false
				)
			)
			Point3:
			(
				if (length diff) < epsilon then
				(
					result = false
				)
			)
			Point4:
			(
				if (length diff) < epsilon then
				(
					result = false
				)
			)
		)
		
		result
	),
	
	fn getDifference variable material =
	(
		local result = 0
		
		for i = 1 to (RstGetVariableCount material) do 
		(
			local type = RstGetVariableName material i
			local value = RstGetVariable material i
			
			if type == variable.type then
			(
				result = variable.value - value
			)
		)
		
		result
	),
	
	fn toTreeView rootTreeNode object =
	(
		local foreColour = RS_dotNetClass.colourClass.fromARGB 63 160 0
		
		if not modified then
		(
			foreColour = RS_dotNetClass.colourClass.fromARGB 128 128 128
		)
		
		if index == undefined then
		(
			foreColour = RS_dotNetClass.colourClass.fromARGB 160 0 0
		)
		
		local name = shader
		local material
		
		if index != undefined then
		(
			if index > 0 then
			(
				name += " - " + object.material.materialList[index].name
				material = object.material.materialList[index]
			)
			else
			(
				name += " - " + object.material.name
				material = object.material
			)
			
			local updated = false
			
			for entry in variableArray do
			(
				if (isUpdated entry material) then
				(
					updated = true
					
					if not modified then
					(
						foreColour = RS_dotNetClass.colourClass.fromARGB 63 63 255
						modified = true
					)
				)
			)
			
			if not updated and modified then
			(
				modified = false
				foreColour = RS_dotNetClass.colourClass.fromARGB 128 128 128
			)
		)
		
		local shaderNode = rootTreeNode.Nodes.Add name
		shaderNode.foreColor = foreColour
		
		if index != undefined then
		(
			for entry in variableArray do
			(
				if (isUpdated entry material) then
				(
					local diff = getDifference entry material
					local variableEntry = shaderNode.Nodes.Add (entry.type + " : " + entry.value as string + " / " + diff as string)
					variableEntry.foreColor = foreColour
				)
			)
		)
		
		if modified then
		(
			shaderNode.expand()
		)
	),
	
	fn toString =
	(
		format "Shader:%\nModified:%\n" shader modified
		
		for entry in instanceArray do
		(
			format "\t%:%\n"entry.type entry.value
		)
		
		for entry in variableArray do
		(
			format "\t%:%\n"entry.type entry.value
		)
	)
)