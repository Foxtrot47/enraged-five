
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/pipeline/RSL_SceneXml.ms")

try (DestroyDialog RSL_LightOps) catch()

rollout RSL_LightOps "R* Lighting Ops" width:176
(
	group "Rage Light"
	(
		button btn_convertToRage "Convert Selected to Rage" width:150 offset:[-1,0]
		button btn_convertAllToRage "Convert All to Rage" width:150
	)
	
	group "Rage Instance Light"
	(
		button btn_selectProps "Select Light Props" width:150
		button btn_import "Import From Selected" width:150
		button btn_delete "Delete All Instances" width:150
	)
	
	mapped fn convertToRage oldLight =
	(
		local newRageLight = RageLight lightColour:oldLight.rgb lightFalloff:(if (classof oldLight) != Omnilight then oldLight.falloff else 50.0)
		newRageLight.name = oldLight.name
		newRageLight.transform = oldLight.transform
		newRageLight.parent = oldLight.parent
		for c in oldLight.children do c.parent = newRageLight
		
		newRageLight.lightType = findItem gLightTypes (oldLight.type)
		
		copyAttrs oldLight
		pasteattrs newRageLight
		
		for p in propCopyMap do
		(
			local theProp = getproperty oldLight p[2]
			setProperty oldLight p[2] theProp
			setProperty newRageLight p[1] theProp
		)
		
		if oldLight.type != #omni then
		(
			for p in nonOmniPropCopyMap do
			(
				local theProp = getproperty oldLight p[2]
				setProperty oldLight p[2] theProp
				setProperty newRageLight p[1] theProp
			)
		)
		
		delete oldLight
	)
	
	on btn_convertToRage pressed do 
	(
		local lightArray = for object in (selection as array) where (superClassOf object) == light and (classOf object) != RageLight and (classOf object) != RageLightInstance collect object 
		
		if lightArray.count > 0 then
		(
			convertToRage lightArray
		)
		else
		(
			messageBox "There are no lights in the selection." title:"Error..."
		)
	)
	
	on btn_convertAllToRage pressed do 
	(
		local lightArray = for object in lights where (superClassOf object) == light and (classOf object) != RageLight and (classOf object) != RageLightInstance collect object 
		
		if lightArray.count > 0 then
		(
			convertToRage lightArray
		)
		else
		(
			messageBox "There are no lights in the scene." title:"Error..."
		)
	)
	
	on btn_selectProps pressed do 
	(
		local lightProps = (RSL_SceneXmlOps.getPropsWithLights())
		
		if lightProps.count > 0 then
		(
			select lightProps
		)
		else
		(
			messageBox "There are no light props in the scene." title:"Warning..."
		)
	)
	
	on btn_import pressed do 
	(
		RSL_SceneXmlOps.importLights()
	)
	
	on btn_delete pressed do 
	(
		delete (for L in lights where (classOf L) == RageLightInstance collect L)
	)
)

createDialog RSL_LightOps style:#(#style_toolwindow,#style_sysmenu)