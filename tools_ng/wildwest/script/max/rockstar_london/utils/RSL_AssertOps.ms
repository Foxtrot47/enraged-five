
-- dotnet.loadAssembly (RsConfigGetWildWestDir() + "script/max/Rockstar_London/dll/RGLExportHelpers.dll")
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetClasses.ms")
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_dotNetUIOps.ms")

global RSL_AssertHelper


struct RSL_ArtAssert
(
	type,
	mapFile,
	MILO,
	objectName,
	position,
	LODDistance
)

struct RSL_LevelInfo
(
	name,
	mapFiles = #()
)

struct RSL_mapFileInfo
(
	name,
	type,
	maxFile,
	propArray = #(),
	MILO
)

struct RSL_AssertOps
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- Struct variables
	---------------------------------------------------------------------------------------------------------------------------------------
	idxLodDistance = GetAttrIndex "Gta Object" "LOD distance",
	idxIplGroup = GetAttrIndex "Gta Object" "IPL Group",
	idxDontExport = getAttrIndex "Gta Object" "Dont Export",
	idxDontAdd = getAttrIndex "Gta Object" "Dont Add To IPL",
	idxAttribute = getattrindex "Gta Object" "Attribute",
	
	LevelFileArray = #(),
	
	assertArray = #(),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- UI variables
	---------------------------------------------------------------------------------------------------------------------------------------
	assertForm,
	mainListView,
	infoLabel,
	infoTextBox,
	infoBrowser,
	
	loadFileButton,
	openFileDialog,
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	fn getMILOName file =
	(
		local result
		
		if (doesFileExist file) then
		(
			local dataStream = openFile file mode:"r"
			
			skiptostring dataStream "mlo"
			line = readLine dataStream
			line = readLine dataStream
			
			result = (filterString line ", ")[1]
			
			close dataStream
		)
		
		result
	),
	
	
	fn getProps file =
	(
		local result = #()
		
		if (doesFileExist file) then
		(
			local dataStream = openFile file mode:"r"
			
			skiptostring dataStream "objs"
			local line = readLine dataStream
			
			while line != "end" and not (eof dataStream) do
			(
				line = readLine dataStream
				
				append result (filterString line ", ")[1]
			)
			
			skiptostring dataStream "anim"
			local line = readLine dataStream
			
			while line != "end" and not (eof dataStream) do
			(
				line = readLine dataStream
				
				append result (filterString line ", ")[1]
			)
			
			close dataStream
		)
		
		result
	),
	
	
	fn getMapFileNames =
	(
		LevelFileArray = #()
		
		local mapPath = project.independent + "/levels/*"
		local levelDirArray = getDirectories (project.independent + "/levels/*")
		LevelFileArray.count = levelDirArray.count
		
		for i = 1 to levelDirArray.count do 
		(
			local dir = levelDirArray[i]
			local level = substituteString (pathConfig.stripPathToLeaf dir) "\\" ""
			local mapfiles = RS_dotNetClass.directoryClass.GetFiles dir "*.rpf" (DotNetClass "System.IO.SearchOption").AllDirectories
			local mapFileInfoArray = #()
			mapFileInfoArray.count = mapfiles.count
			
			for i = 1 to mapfiles.count do
			(
				local mapFile = mapfiles[i]
				local parentDir = substituteString (pathConfig.stripPathToLeaf (getFilenamePath mapFile)) "\\" ""
				
				if (stricmp parentDir level) == 0 then
				(
					parentDir = ""
				)
				
				if parentDir.count > 0 then parentDir += "/"
				
				local name = (getFilenameFile mapFile)
				local type
				local MILO
				local propArray = #()
				
				case of
				(
					(parentDir == ""):type = #mapFile
					(matchPattern parentDir pattern:"*interiors*"):
					(
						type = #interior
						MILO = getMILOName (subStituteString mapFile ".rpf" ".ide")
					)
					(matchPattern parentDir pattern:"*props*"):
					(
						type = #prop
						propArray = getProps (subStituteString mapFile ".rpf" ".ide")
					)
				)
				
				local maxFile = project.art + "/maps/" + level + "/" + parentDir + name + ".max"
				
				mapFileInfoArray[i] = RSL_mapFileInfo name:name type:type maxFile:maxFile propArray:propArray MILO:MILO
			)
			
			LevelFileArray[i] = RSL_LevelInfo name:level mapFiles:mapFileInfoArray
		)
	),
	
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- parseAssert()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn parseAssert assert =
	(
		local result
		
		case of
		(
			(matchPattern assert pattern:"*[art] Cannot find model from*"):
			(
				local dataStream = assert as stringStream
				local mapFile
				
				if (matchPattern assert pattern:"*Cannot find model from platform:*") then
				(
					skipToString dataStream "[art] Cannot find model from platform:"
					local stream = readDelimitedString dataStream " "
					mapFile = getFilenameFile stream
				)
				else
				(
					skipToString dataStream "[art] Cannot find model from "
					local stream = readDelimitedString dataStream " "
					local index = (findString stream "_stream")
					
					if index == undefined then
					(
						index = (findString stream "_strbig")
					)
					
					index -= 1
					
					mapFile = (substring stream 1 index)
				)
				
				skipToString dataStream "at "
				local x = (readDelimitedString dataStream " ") as float
				local y = (readDelimitedString dataStream " ") as float
				local z = (readDelimitedString dataStream " ") as float
				
				result = RSL_ArtAssert type:"Missing Prop/Not exported?" mapFile:mapFile position:[x,y,z]
				
				close dataStream
			)
			(matchPattern assert pattern:"*Small IPL*contains an object*with too large a LOD distance*"):
			(
				local dataStream = assert as stringStream
				
				skipToString dataStream "Small IPL "
				local stream = readDelimitedString dataStream " "
				local index = (findString stream "_stream") - 1
				local mapFile = (substring stream 1 index)
				
				skipToString dataStream "contains an object "
				local name = readDelimitedString dataStream " "
				
				skipToString dataStream "too large a LOD distance ("
				local LODDistance = readValue dataStream
				
				result = RSL_ArtAssert type:"LOD Distance too Large" mapFile:mapFile objectName:name LODDistance:LODDistance
				
				close dataStream
			)
			(matchPattern assert pattern:"*Object in MLO does not exist!"):
			(
				local dataStream = assert as stringStream
				
				skipToString dataStream "FAILED: "
				local name = readDelimitedString dataStream " "
				
				skipToString dataStream "in "
				local MILO = readDelimitedString dataStream " "
				
				result = RSL_ArtAssert type:"Missing Prop in MILO/Not exported?" objectName:name MILO:MILO
				
				close dataStream
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- loadAssertFile()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn loadAssertFile file =
	(
		assertArray = #()
		
		if (doesFileExist file) then
		(
			local dataStream = openfile file
			skipToString dataStream "Scene : "
			local assertCount = readValue dataStream
			
			for i = 1 to assertCount do
			(
				local parsed = parseAssert (readLine dataStream)
				
				if parsed != undefined then
				(
					append assertArray parsed
				)
			)
			
			close dataStream
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- addAssertItem()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn addAssertItem assert =
	(
		local assertString = assert.type
		
		case assert.type of
		(
			"Missing Prop/Not exported?":
			(
				assertString += " - Missing from map: " + assert.mapFile + " - At position: " + assert.position as string
			)
			"LOD Distance too Large":
			(
				assertString += " - Found in map: " + assert.mapFile + " - Object: " + assert.objectName as string + " - LOD Distance: " + assert.LODDistance as string
			)
			"Missing Prop in MILO/Not exported?":
			(
				assertString += " - Missing from MILO: " + assert.MILO + " - Object: " + assert.objectName as string
			)
		)
		
		local newItem = dotNetObject "ListViewItem" assertString
		newItem.tag = dotNetMXSValue assert
		
		mainListView.items.add newItem
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getObjectAt()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getObjectAt position =
	(
		local result
		
		for object in geometry do 
		(
			if (distance object.pos position) < 0.001 then
			(
				result = object
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getNodeByObjectName()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getNodeByObjectName name =
	(
		local result = #()
		
		for object in geometry where (classOf object) == XrefObject do
		(
			if (stricmp object.objectName name) == 0 then
			(
				append result object
			)
		)
		
		result
	),
	
	fn getLevelInfo name =
	(
		local result
		
		for level in LevelFileArray do
		(
			if (stricmp level.name name) == 0 then
			(
				result = level
			)
		)
		
		result
	),
	
	fn getMapFileInfo name =
	(
		local result
		
		for level in LevelFileArray do
		(
			for mapFile in level.mapFiles do
			(
				if (stricmp mapFile.name name) == 0 then
				(
					result = mapFile
				)
			)
		)
		
		result
	),
	
	fn propFileContainsProp mapFile name =
	(
		local result = false
		
		for prop in mapFile.propArray do
		(
			if (stricmp prop name) == 0 then
			(
				result = true
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getMaxFileByMILOName()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getMaxFileByMILOName name =
	(
		local result
		
		for level in LevelFileArray do 
		(
			for mapFile in level.mapFiles where mapFile.type ==#interior do 
			(
				if (doesFileExist mapFile.maxFile) and mapFile.MILO != undefined then
				(
					if (stricmp mapFile.MILO name) == 0 then
					(
						result = mapFile.maxFile
					)
				)
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getMILOByName()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getMILOByName name =
	(
		local result
		
		for helper in helpers where (classOf helper) == Gta_MILO do
		(
			if (stricmp helper.name name) == 0 then
			(
				result = helper
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- executeData()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn executeData data =
	(
		case data.type of
		(
			"Missing Prop/Not exported?":
			(
				local mapFileInfo = getMapFileInfo data.mapFile
				
				if mapFileInfo != undefined and (doesFileExist mapFileInfo.maxFile) then
				(
					local okToContinue = true
					
					if stricmp (getFilenameFile mapFileInfo.maxFile) (getFilenameFile maxFilename) != 0 then
					(
						okToContinue = queryBox "Do you want to load the max file for this assert?" title:"Query..."
						
						if okToContinue then
						(
							loadMaxFile mapFileInfo.maxFile
						)
					)
					
					if okToContinue then
					(
						local object = getObjectAt data.position
						
						if (isValidNode object) then
						(
							select object
							max zoomext sel
							
							completeRedraw()
							
							if (classOf object) == XrefObject and (doesFileExist object.filename) and not object.unresolved then
							(
								local objectName = object.ObjectName
								local sourceMapFileInfo = getMapFileInfo (getFilenameFile object.filename)
								
								if sourceMapFileInfo != undefined then
								(
									if not (propFileContainsProp sourceMapFileInfo objectName) then
									(
										okToContinue = queryBox "The prop is not being exported from its source file.\nDo you want to load the source file?" title:"Query..."
									)
								)
								else
								(
									if (queryBox "The source rpf file for this prop is missing.\nThis could mean that the prop file is no longer used in the level or that the rpf has been mistakenly deleted from perforce.\nMark the Xref as Don't Export?" title:"Error...") then
									(
										setAttr object idxDontAdd true
										setAttr object idxDontExport true
										
										messageBox "The Xref has been marked as Don't Export, Don't Add to IPL."
									)
								)
								
								if okToContinue then
								(
									loadMaxFile object.filename
									
									local node = getNodeByName objectName exact:true
									
									if (isValidNode node) then
									(
										node.isHidden = false
										select node
										max zoomext sel
									)
									else
									(
										messageBox ("Unable to find object " + objectName) title:"Error..."
									)
								)
								else
								(
									if (queryBox "Mark all instances of the Xref as Don't Export?" title:"Query...") then
									(
										local objectArray = getNodeByObjectName object.objectName
										
										for obj in objectArray do
										(
											setAttr obj idxDontAdd true
											setAttr obj idxDontExport true
										)
										
										messageBox "All instances of the Xref have been marked as Don't Export, Don't Add to IPL."
									)
								)
							)
							else
							(
								if (classOf object) == XrefObject and not (doesFileExist object.filename) then
								(
									if (object.unresolved) then
									(
										messageBox ("The source file does not exist for this prop.\n" + object.filename) title:"Error..."
									)
									else
									(
										messageBox ("The source file does not exist for this prop and yet it is not un-resolved. This could be a nested Xref.\n" + object.filename) title:"Error..."
									)
								)
								else
								(
									if (getAttr object idxAttribute) == 6 and (getAttr object idxIplGroup) == "undefined" then
									(
										messageBox "This object is marked as a \"Horizon Object\" but does not have a valid IPL Group set in its attributes." title:"The problem is..."
									)
								)
							)
						)
						else
						(
							messageBox ("Unable to find object at " + data.position as string) title:"Error..."
						)
					)
				)
				else
				(
					
				)
			)
			"LOD Distance too Large":
			(
				local mapFileInfo = getMapFileInfo data.mapFile
				
				if mapFileInfo != undefined and (doesFileExist mapFileInfo.maxFile) then
				(
					local okToContinue = true
					
					if (stricmp (getFilenameFile maxFilename) mapFileInfo.maxFile) != 0 then
					(
						okToContinue = queryBox "Do you want to load the max file for this assert?" title:"Query..."
						
						if okToContinue then
						(
							loadMaxFile mapFileInfo.maxFile
						)
					)
					
					if okToContinue then
					(
						local objectArray = getNodeByObjectName data.objectName
						
						if objectArray.count > 0 then
						(
							select objectArray
							max zoomext sel
							
							completeRedraw()
							
							local object = objectArray[1]
							
							if (classOf object) == XrefObject and (doesFileExist object.filename) and not object.unresolved then
							(
								local objectName = object.ObjectName
								local sourceMapFileInfo = getMapFileInfo (getFilenameFile object.filename)
								
								if sourceMapFileInfo != undefined then
								(
									okToContinue = queryBox "Do you want to load the source file for this prop?" title:"Query..."
								)
								else
								(
									if (queryBox "The source rpf file for this prop is missing.\nThis could mean that the prop file is no longer used in the level or that the rpf has been mistakenly deleted from perforce.\nMark all instances of the Xref as Don't Export?" title:"Error...") then
									(
										for obj in objectArray do
										(
											setAttr obj idxDontAdd true
											setAttr obj idxDontExport true
										)
										
										messageBox "All instances of the Xref have been marked as Don't Export, Don't Add to IPL."
									)
								)
								
								if okToContinue then
								(
									loadMaxFile object.filename
									
									local node = getNodeByName objectName exact:true
									local frag = getNodeByName (objectName + "_frag_") exact:true
									
									if (isValidNode frag) then
									(
										node = frag
										objectName += "_frag_"
									)
									
									if (isValidNode node) then
									(
										node.isHidden = false
										select node
										max zoomext sel
										
										completeRedraw()
										
										if (isValidNode frag) then
										(
											
										)
										
										
										messageBox ("This object's LOD Distance is set to " + data.LODDistance as string + ".\nIt must be set to no greater than 150") title:"The problem is..."
									)
									else
									(
										messageBox ("Unable to find object " + objectName) title:"Error..."
									)
								)
							)
						)
						else
						(
							messageBox ("Unable to find object " + data.objectName as string) title:"Error..."
						)
					)
				)
			)
			"Missing Prop in MILO/Not exported?":
			(
				local maxFile = getMaxFileByMILOName data.MILO
				
				if maxFile != undefined then
				(
					local okToContinue = true
					
					if stricmp (getFilenameFile maxFile) (getFilenameFile maxFilename) != 0 then
					(
						okToContinue = queryBox "Do you want to load the max file for this MILO?" title:"Query..."
						
						if okToContinue then
						(
							loadMaxFile maxFile
						)
					)
					
					if okToContinue then
					(
						local MILONode = getMILOByName data.MILO
						
						if (isValidNode MILONode) then
						(
							local objectArray = getNodeByObjectName data.objectName
							
							if objectArray.count > 0 then
							(
								select objectArray
								max zoomext sel
								
								completeRedraw()
								
								local object = objectArray[1]
								
								if (classOf object) == XrefObject and (doesFileExist object.filename) and not object.unresolved then
								(
									local objectName = object.ObjectName
									local sourceMapFileInfo = getMapFileInfo (getFilenameFile object.filename)
									
									if sourceMapFileInfo != undefined then
									(
										if not (propFileContainsProp sourceMapFileInfo objectName) then
										(
											okToContinue = queryBox "The prop is not being exported from its source file.\nDo you want to load the source file?" title:"Query..."
										)
									)
									else
									(
										if (queryBox "The source rpf file for this prop is missing.\nThis could mean that the prop file is no longer used in the level or that the rpf has been mistakenly deleted from perforce.\nMark all instances of the Xref as Don't Export?" title:"Error...") then
										(
											for obj in objectArray do
											(
												setAttr obj idxDontAdd true
												setAttr obj idxDontExport true
											)
											
											messageBox "All instances of the Xref have been marked as Don't Export, Don't Add to IPL."
										)
									)
									
									if okToContinue then
									(
										loadMaxFile object.filename
										
										local node = getNodeByName objectName exact:true
										
										if (isValidNode node) then
										(
											node.isHidden = false
											select node
											max zoomext sel
										)
										else
										(
											messageBox ("Unable to find object " + objectName) title:"Error..."
										)
									)
									else
									(
										if (queryBox "Mark all instances of the Xref in this map file as Don't Export?" title:"Query...") then
										(
											for obj in objectArray do
											(
												setAttr obj idxDontAdd true
												setAttr obj idxDontExport true
											)
											
											messageBox "All instances of the Xref have been marked as Don't Export, Don't Add to IPL."
										)
									)
								)
								else
								(
									if (classOf object) == XrefObject and not (doesFileExist object.filename) then
									(
										if (object.unresolved) then
										(
											messageBox ("The source file does not exist for this prop.\n" + object.filename) title:"Error..."
										)
										else
										(
											messageBox ("The source file does not exist for this prop and yet it is not un-resolved. This could be a nested Xref.\n" + object.filename) title:"Error..."
										)
									)
									else
									(
										if (getAttr object idxAttribute) == 6 and (getAttr object idxIplGroup) == "undefined" then
										(
											messageBox "This object is marked as a \"Horizon Object\" but does not have a valid IPL Group set in its attributes." title:"The problem is..."
										)
									)
								)
							)
							else
							(
								messageBox ("Unable to find any instance of object " + data.objectName as string) title:"Error..."
							)
						)
						else
						(
							messageBox ("The MILO: " + data.MILO + " does not exist in this max file.") title:"Error..."
						)
					)
				)
			)
		)
	),
	
	
	
	
	
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- UI EVENTS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- disableAccelerators() Called when the user enters any control that needs keyboard input e.g. a textBox
	---------------------------------------------------------------------------------------------------------------------------------------
	fn disableAccelerators s e =
	(
		enableAccelerators = false
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOnFormClosing() Dispatches onFormClosing() Called when the user closes collision ops
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOnFormClosing s e =
	(
		RSL_AssertHelper.onFormClosing s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- onFormClosing() Called when collision ops is closed
	---------------------------------------------------------------------------------------------------------------------------------------
	fn onFormClosing s e =
	(
		
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchLoadPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchLoadPressed s e =
	(
		RSL_AssertHelper.loadPressed s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- loadPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn loadPressed s e  =
	(
		if openFileDialog.ShowDialog() == (dotNetClass "DialogResult").OK then
		(
			mainListView.items.clear()
-- 			loadAssertFile (rsConfig.toolsRoot + "/DocksSP_asserts.txt") --"/1_Client_asserts.txt"
			loadAssertFile openFileDialog.FileName
			
			addAssertItem assertArray
			mainListView.AutoResizeColumns (dotNetClass "ColumnHeaderAutoResizeStyle").HeaderSize
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchListViewClick()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchListViewClick s e =
	(
		RSL_AssertHelper.listViewClick s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- listViewClick()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn listViewClick s e =
	(
		if s.selectedItems.count == 1 then
		(
			local assertData = s.selectedItems.item[0].tag.value
-- 			local infoFile
			local infoLink
			
			case assertData.type of
			(
				"Missing Prop/Not exported?":
				(
					infoLink = dotNetObject "Uri" "https://mp.rockstargames.com/index.php/Missing_Prop/Not_Exported%3F"
					
-- 					infoFile = rsConfigGetWildWestDir() + "/script/max/rockstar_london/config/Missing_Prop_Not_exported.rtf"
				)
				"LOD Distance too Large":
				(
					infoLink = dotNetObject "Uri"  "https://mp.rockstargames.com/index.php/LOD_Distance_Too_Large"
					
-- 					infoFile = rsConfigGetWildWestDir() + "/script/max/rockstar_london/config/LOD_Distance_Too_Large_SIPL.rtf"
				)
				"Missing Prop in MILO/Not exported?":
				(
					infoLink = dotNetObject "Uri" "https://mp.rockstargames.com/index.php/Missing_Prop/Not_Exported%3F"
					
-- 					infoFile = rsConfigGetWildWestDir() + "/script/max/rockstar_london/config/Missing_Prop_Not_exported.rtf"
				)
			)
			
-- 			if infoFile != undefined and (doesFileExist infoFile) then
-- 			(
-- 				infoTextBox.LoadFile infoFile
-- 			)
			
			if infoLink != undefined then
			(
				infoBrowser.Url = infoLink
			)
		)
-- 		else
-- 		(
-- 			infoLabel.text = ""
-- 		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchListViewDoubleClick()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchListViewDoubleClick s e =
	(
		RSL_AssertHelper.listViewDoubleClick s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- listViewDoubleClick()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn listViewDoubleClick s e =
	(
		if s.selectedItems.count == 1 then
		(
			local assertData = s.selectedItems.item[0].tag.value
			
			executeData assertData
		)
	),
	
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- createForm() Creates the main texture tagger dialog
	---------------------------------------------------------------------------------------------------------------------------------------
	fn createForm = 
	(
		clearListener()
		
		--In this case, we're going to use a standard form rather than the custom maxForm written for max. this is because I want to change the 
		-- form back colour which you can't do if you use the custom maxForm. For this to work, we need to set the parent of the form to be Max. 
		-- USING THIS TYPE OF FORM MEANS WE NEED TO DISSABLE ACCELERATORS WHEN WE FOCUS ON A CONTROL THAT TAKES
		-- KEY PRESSES SUCH AS SPINNERS AND TEXT BOXES
		
		--Get the max handle pointer.
		local maxHandlePointer=(Windows.GetMAXHWND())
		
		--Convert the HWND handle of Max to a dotNet system pointer
		local sysPointer = DotNetObject "System.IntPtr" maxHandlePointer
		
		--Create a dotNet wrapper containing the maxHWND
		local maxHwnd = DotNetObject "MaxCustomControls.Win32HandleWrapper" sysPointer
		
		assertForm = RS_dotNetUI.form "assertForm" text:" R* Assert Helper" size:[512, 768] max:[2048, 2048] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		assertForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		dotnet.addEventHandler assertForm "FormClosing" dispatchOnFormClosing
		dotnet.addEventHandler assertForm "Enter" disableAccelerators
		
		openFileDialog = dotNetObject "openFileDialog"
		openFileDialog.Filter = "Text Files|*.txt"
		
		loadFileButton = RS_dotNetUI.toolStripButton "loadFileButton" text:"Load Assert File..." CheckOnClick:false
		loadFileButton.ToolTipText = "Allows you to select and load an assert file."
		dotnet.addEventHandler loadFileButton "Click" dispatchLoadPressed
		
		mainToolStrip = RS_dotNetUI.ToolStrip "mainToolStrip" dockStyle:RS_dotNetPreset.DS_Fill
		mainToolStrip.GripStyle = (dotNetClass "System.Windows.Forms.ToolStripGripStyle").Hidden
		mainToolStrip.items.addRange #(loadFileButton)
		
		mainListView = RS_dotNetUI.listView "mainListView" dockStyle:RS_dotNetPreset.DS_Fill
		mainListView.view = (dotNetClass "System.Windows.Forms.View").Details
		mainListView.Sorting = (dotNetClass "System.Windows.Forms.SortOrder").None
		mainListView.multiSelect = true
		mainListView.HideSelection = false
		mainListView.scrollable = true
		mainListView.fullRowSelect = true
		mainListView.backColor = RS_dotNetPreset.controlColour_Medium
-- 		mainListView.ContextMenuStrip = mainListViewRickClick
		dotnet.addEventHandler mainListView "Click" dispatchListViewClick
		dotnet.addEventHandler mainListView "DoubleClick" dispatchListViewDoubleClick
		
		mainListView.Columns.Add "Assert"
		
-- 		infoLabel = RS_dotNetUI.Label "objectMemoryLabel" text:"" \
-- 													textAlign:RS_dotNetPreset.CA_MiddleLeft borderStyle:RS_dotNetPreset.BS_None \
-- 													dockStyle:RS_dotNetPreset.DS_Fill
-- 		infoLabel.margin = (RS_dotNetObject.paddingObject 8)
		
		infoTextBox = dotNetObject "RichTextBox"
		infoTextBox.dock = RS_dotNetPreset.DS_Fill
		infoTextBox.BorderStyle = (dotNetClass "System.Windows.Forms.BorderStyle").FixedSingle
		infoTextBox.ReadOnly = true
		
		infoBrowser = dotNetObject "WebBrowser"
		infoBrowser.dock = RS_dotNetPreset.DS_Fill
		
		mainSplitContainer = RS_dotNetUI.SplitContainer "mainSplitContainer" borderStyle:RS_dotNetPreset.BS_FixedSingle dockStyle:RS_dotNetPreset.DS_Fill 
		mainSplitContainer.Orientation = (dotNetClass "System.Windows.Forms.Orientation").Horizontal
		mainSplitContainer.SplitterWidth = 6
		
		mainSplitContainer.Panel1.Controls.Add mainListView
		mainSplitContainer.Panel2.Controls.Add infoBrowser --infoTextBox
		
		mainTable = RS_dotNetUI.tableLayout "mainTable" text:"mainTable" collumns:#((dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		mainTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		mainTable.RowCount = 2
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "Absolute" 24)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 50)
		
		mainTable.controls.add mainToolStrip 0 0
		mainTable.controls.add mainSplitContainer 0 1
		
		assertForm.controls.add mainTable
		
		assertForm.show maxHwnd
		
		getMapFileNames()
	)
)

if RSL_AssertHelper != undefined then
(
	RSL_AssertHelper.assertForm.close()
)

RSL_AssertHelper = RSL_AssertOps()
RSL_AssertHelper.createForm()