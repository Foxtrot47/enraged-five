
fn RecursiveSetAllCollisionToNotProvideCover obj =
(
	for childobj in obj.children do
	(
		if classof childobj == Col_Mesh or classof childobj == Col_Box or classof childobj == Col_Sphere or classof childobj == Col_Cylinder or classof childobj == Col_Capsule then
		(
			idxDoesNotProvideCover = getattrindex "Gta Collision" "Does Not Provide Cover"
			
			setattr childobj idxDoesNotProvideCover true
		)
		
		RecursiveSetAllCollisionToNotProvideCover childobj
	)
)

fn RsSetAllCollisionToNotProvideCover = 
(
	for childobj in rootnode.children do
	(
		
		if classof childobj == Editable_poly or classof childobj == Editable_mesh or classof childobj == PolyMeshObject then
		(
			idxFixed = getattrindex "Gta Object" "Is Fixed"
			idxDynamic = getattrindex "Gta Object" "Is Dynamic"
			idxFragment = getattrindex "Gta Object" "Is Fragment"
			
			if getattr childobj idxDynamic == true and getattr childobj idxFixed == false then
			(
				idxDoesNotProvideCover = getattrindex "Gta Object" "Does Not Provide AI Cover"
				
				setattr childobj idxDoesNotProvideCover true				
			)
			else
			if getattr childobj idxFragment == true then
			(
				print childobj.name
				idxDoesNotProvideCover = getattrindex "Gta Object" "Does Not Provide AI Cover"
				
				setattr childobj idxDoesNotProvideCover true				
			)
/*			
			if getattr childobj idxFixed == false and then
			(
				idxDoesNotProvideCover = getattrindex "Gta Object" "Does Not Provide AI Cover"
				
				setattr childobj idxDoesNotProvideCover true
				--RecursiveSetAllCollisionToNotProvideCover childobj
			)
			else
				print childobj.name
*/
		)
	)
)

RsSetAllCollisionToNotProvideCover()