struct RS_CleanUpOnSaveOps
(
	isMapFile = false,
	shadedViews = #(),
	materialEditorArray = #(),
	badClasses = #("Kaldera_Session", "Missing_Helper"),
	
	mapped fn deleteBadObjects object =
	(
		local class = (classOf object) as string
		local bad = (findItem badClasses class) != 0
		
		if bad then
		(
			delete object
		)
	),
	
	fn cb_preSave =
	(
		isMapFile = matchPattern maxFilePath pattern:"*\\payne_art\\maps\\*"
		
		if isMapFile then
		(
			local saveType = callbacks.notificationParam()
			
			if saveType[1] == 1 then
			(
				materialEditorArray = for material in meditMaterials collect material
				
				for i = 1 to meditMaterials.count do
				(
					local count = i as string
					if i < 10 then
					(
						count = "0" + i as string
					)
					
					meditMaterials[i] = standardmaterial name:(count + " - Default")
				)
				
				deleteBadObjects objects
				
				shadedViews = #()
				
				for i = 1 to viewport.numViews do
				(
					viewport.activeViewport = i
					
					if not viewport.isWire() then
					(
						append shadedViews true
						max wire smooth
					)
					else
					(
						append shadedViews false
					)
				)
			)
		)
	),
	
	fn cb_postSave =
	(
		if isMapFile then
		(
			local saveType = callbacks.notificationParam()
			
			if saveType[1] == 1 then
			(
				for i = 1 to materialEditorArray.count do
				(
					meditMaterials[i] = materialEditorArray[i]
				)
				
				for i = 1 to shadedViews.count do
				(
					viewport.activeViewport = i
					
					if shadedViews[i] then
					(
						max wire smooth
					)
				)
			)
			
			setSaveRequired false
		)
	),
	
	fn initialise =
	(
		callbacks.addScript #filePreSaveProcess "RS_cleanUpOnSave.cb_preSave()" id:#RS_CleanUpOnSave
		callbacks.addScript #filePostSaveProcess "RS_cleanUpOnSave.cb_postSave()" id:#RS_CleanUpOnSave
	)
)

RS_cleanUpOnSave = RS_CleanUpOnSaveOps()
RS_cleanUpOnSave.initialise()