-- IMG Functions

-------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------
-- Define a global collection list
global outputlist = #()
-- Define a global Default Ext
global ext = ""
global imageFile = ""

-- DotNet Event Handler Function --
 fn outputDataReceivedHandler obj evt =
 (
 	if evt.data != undefined then
 	(	
		if getFilenameType (evt.data) == ("."+ext) then
		(			
			append outputlist #((evt.data),(filenameFromPath imageFile))
		)
 	)
 )
 
-- DotNet Process Run command Function --
fn RunRageBuilderScript arguments =
(
	proc = dotNetObject "System.Diagnostics.Process"
	proc.EnableRaisingEvents = true
	
	proc.StartInfo.WorkingDirectory = RsConfig.toolsbin
	proc.StartInfo.FileName = project.targets[1].ragebuilder
	proc.StartInfo.UseShellExecute = false
	proc.StartInfo.RedirectStandardOutput = true
	proc.StartInfo.RedirectStandardError = false
	proc.StartInfo.CreateNoWindow = true
	  
	dotNet.AddEventHandler proc "OutputDataReceived" outputDataReceivedHandler
	
	proc.StartInfo.Arguments = (" "+arguments)
	print arguments
	proc.Start()
	proc.BeginOutputReadLine()
	
	proc.WaitForExit()

)
 
-- Standard Basic Max Functions
-------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------

-- Simple recursive Function
fn recuse root pattern =
(
	local my_files = #()
	join my_files (getFiles (root +"\\"+ pattern))
	dir_array = GetDirectories (root+"/*")
	for d in dir_array do
		join dir_array (GetDirectories (d+"/*"))	
	for f in dir_array do
		join my_files (getFiles (f + pattern))
	my_files 
)

-- 		Seacrh each image for file tpyes	 --
---------------------------------------------------
fn ListFromImg imagelist=
(
	progressStart "Checking Level Image Files..."
	outputlist = #() -- Clear the read list
	local cnt = 0
	for eachimage in imagelist do
	(
		cnt+=1
		progressUpdate ( 100.0*cnt/imagelist.count)
		imageFile = eachimage
		local arguments = (RsConfigGetWildWestDir()+ "/script/max/rockstar_london/scripts/Inspectimage.rbs -input "+eachimage +" -ext "+ext ) 
		RunRageBuilderScript arguments
	)
	progressEnd()
)


fn removeFromList imagename imagelist =
(
	local newimagelist = #()
	for eachimg in imagelist do
	(
		if (tolower (getFilenameFile eachimg)) != (tolower imagename) then
		(
			append newimagelist eachimg
		)
	)
	newimagelist
)

fn readIPLstream filename objlist=
(	
	
	iplfile = ( openfile filename mode:"r" )	
	if iplfile != undefined then
	(
	while eof iplfile == false do
	(
		fileline = readline iplfile
		objfiletokens = filterstring fileline ", "
		if ( objfiletokens.count >4 ) then 
		(
			appendifunique objlist objfiletokens[1]
		)
	)
)
	--objlist
)

fn readstreamIPLlist pathname objlist=
(

	ipl_list =  getfiles (pathname+"/*.ipl")
	for eachfile in ipl_list do
	(
		readIPLstream eachfile objlist
	)
	--objlist
)

fn readIDE filename objlist ext:"" =
(
	local idefile = ( openfile filename mode:"r" )	
	if ext != "" then
	(	
		skipToString idefile ext	
		while fileline != "end" do
		(	
	
			fileline = readline idefile
			objfiletokens = filterstring fileline ", "
			if ( objfiletokens.count == 10 ) then -- Set to 10 as standard milo count
			(
				objname = (filterString objfiletokens[1] "\t" )	-- Remove tabs
				
				appendifunique objlist objname[1]
			
			)
		)
	)	
	else
	(	
		while eof iplfile == false do
		(
			fileline = readline idefile
			objfiletokens = filterstring fileline ", "
			if ( objfiletokens.count >10 ) then 
			(
				objname = (filterString objfiletokens[1] "\t" )	-- Remove tabs
				appendifunique objlist objfiletokens[1]
			)
		)
	)
	
)
	









-- Extract files by type from the image file --
---------------------------------------------------
fn ExtractFromImg imagelist=
(
	-- Get stream ipls
	local objlist = #()
	for eachimage in imagelist do
	(
		print "----------------------------------------------------------------------------------------------"
	
		local dir = getFilenameFile(eachimage)
		makeDir ("c:/ipltemp/"+dir)

		local arguments = (RsConfigGetWildWestDir()+ "/script/max/rockstar_london/scripts/ExtractIPL.rbs -input "+eachimage +" -output c:/ipltemp/"+dir +"/") 
		RunRageBuilderScript arguments
		
		print eachimage
		
		readstreamIPLlist ("c:/ipltemp/"+dir)  objlist
		
		print (objlist.count)
		
		-- Get the non stream ipl
		local iplfilename = (getFilenamePath eachimage+getFilenameFile eachimage + ".ipl")		
		readIPLstream iplfilename objlist
		
		print iplfilename
		print (objlist.count)
	
		print (objlist.count)
		
		-- Get IDE object data if the img file is a interior
		--print (filenamefrompath (pathConfig.removePathLeaf eachimage))
		if filenamefrompath (pathConfig.removePathLeaf eachimage) == "interiors" then
		(
			local idefilename = (getFilenamePath eachimage+getFilenameFile eachimage + ".ide")	
			
			print idefilename
			print (objlist.count)
		
		
			readIDE idefilename objlist ext:"mlo"
		)
		
		--print idefilename
		print (objlist.count)
		
	)
	
	objlist
	
	
)

-- Base Functions
-------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------
-- Get types from all images for the level
fn _imgGetAllTypes mappath e:"" =
(
	outputlist = #()
	ext = e
	imagelist =recuse mappath "*.rpf"	
	ListfromImg imagelist 
	outputlist
)

fn _imgGetAllobjs mappath e:"" =
(
	
)

-- Get types from lall images for the level except the current image/max file
-- Image is the current maxfile
-- you can use levelconfig.level to use as image
fn _imgGetValidTypes mappath image e:"" =
(
	outputlist = #()
	ext = e
	imagelist =recuse mappath "*.rpf"		
	validImageList =removeFromList image imagelist
	ListfromImg validImageList 
	outputlist
)

fn _imgGetStreamIPL mappath =
(
		filename = project.targets[1].ragebuilder --"ragebuilder_0327.exe"
		arguments = (RsConfigGetWildWestDir()+ "/script/max/rockstar_london/scripts/ExtractIPL.rbs -input "+eachimage + " -output c:\\tempIPL\\"+(pathConfig.stripPathToLeaf mappath))
		--print (filename +" "+ arguments)
		--HiddenDOSCommand (filename +" "+ arguments) startpath:"x:\\tools\\bin\\" 			
		RunRageBuilderScript arguments
)

--	Example Usage
-----------------------------------------------------------------------------------------------
-- levelconfig.level will give you the current image name/maxfile
-- To retrive all the texture dictionaries (itd) for the level
-- _imgGetAllTypes "X:\\payne\\build\\dev\\independent\\levels\\c_pana\\" e:"itd"
-- _imgGetValidTypes "X:\\payne\\build\\dev\\independent\\levels\\c_pana\\" "test" e:"itd" 

