
for material in sceneMaterials do
(
	case (classOf material) of
	(
		Rage_Shader:
		(
			local currentShader = RstGetShaderName material
-- 			print currentShader
			local isGTAshader = matchPattern currentShader pattern:"GTA_*"
			if isGTAshader then
			(
				local newShader = subString currentShader 5 currentShader.count
				
				format "Changing:% Was:% Now:%\n" material.name currentShader newShader to:listener
				
				RstSetShaderName material newShader
			)
		)
		Multimaterial:
		(
			for subMaterial in material where (classOf subMaterial) == Rage_Shader do
			(
				local currentShader = RstGetShaderName subMaterial
-- 				print currentShader
				local isGTAshader = matchPattern currentShader pattern:"GTA_*"
				if isGTAshader then
				(
					local newShader = subString currentShader 5 currentShader.count
					
					format "Changing:% Was:% Now:%\n" subMaterial.name currentShader newShader to:listener
					
					RstSetShaderName subMaterial newShader
				)
				else
				(
				)
			)
		)
		default:
		(
-- 			print (classOf material)
		)
	)
)