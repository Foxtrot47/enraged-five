--Autoprimitive
--version 1.1
--ammended to support subobjects
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetUIOps.ms")
global Autoprimitive

struct AutoprimitiveStruct
(
	AutoprimitiveForm,
	AutoprimitiveFlowLayoutPanel,
	SwatchButtonArray = #(),
	IniFilePath = RsConfigGetWildWestDir()+"script/max/rockstar_london/config/AD_Tools.ini",
	
	--preset definitions
	DS_Fill = (dotNetClass "System.Windows.Forms.DockStyle").Fill,
	FBS_SizableToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").SizableToolWindow,
	FBS_FixedToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").FixedToolWindow,
	FS_Flat = (dotNetClass "System.Windows.Forms.FlatStyle").Flat,
	FS_Standard = (dotNetClass "System.Windows.Forms.FlatStyle").Standard,
	SP_Manual =  (dotNetClass "System.Windows.Forms.FormStartPosition").Manual,
	Font_Calibri = dotNetObject "System.Drawing.Font" "Calibri" 8,
	
	--colours
	RGB_Black = (dotNetClass "System.Drawing.Color").black,
	RGB_DarkGrey = (dotNetClass "System.Drawing.Color").fromARGB 102 102	102,
	RGB_Steel = (dotNetClass "System.Drawing.Color").FromARGB 200 210 220,
	RGB_Transparent = (dotNetClass "System.Drawing.Color").transparent,

	WinLocX = 0,
	WinLocY = 40,
	WinWidth = 60,
	WinHeight = 272,
	PrimitiveType,
	ConvertState = true,
	ConvertCheckbox,
	SelectCheckbox,
	Divisions,
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--GENERAL FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn ProcessVerts vertSel =
	(
		maxminx = #(vertSel[1].pos.x, vertSel[1].pos.x)
		maxminy = #(vertSel[1].pos.y, vertSel[1].pos.y)
		maxminz = #(vertSel[1].pos.z, vertSel[1].pos.z)
		for i = 2 to vertSel.count do
		(
			if (vertSel[i].pos.x > maxminx[1]) then maxminx[1] = vertSel[i].pos.x
			if (vertSel[i].pos.x < maxminx[2]) then maxminx[2] = vertSel[i].pos.x
			if (vertSel[i].pos.y > maxminy[1]) then maxminy[1] = vertSel[i].pos.y
			if (vertSel[i].pos.y < maxminy[2]) then maxminy[2] = vertSel[i].pos.y
			if (vertSel[i].pos.z > maxminz[1]) then maxminz[1] = vertSel[i].pos.z
			if (vertSel[i].pos.z < maxminz[2]) then maxminz[2] = vertSel[i].pos.z
		)
		xval = (maxminx[1] - maxminx[2])
		yval = (maxminy[1] - maxminy[2])
		zval = (maxminz[1] - maxminz[2])
		selsize = #(xval, yval, zval)
		xval = ( (maxminx[1] + maxminx[2]) / 2)
		yval = ( (maxminy[1] + maxminy[2]) / 2)
		zval = ( (maxminz[1] + maxminz[2]) / 2)
		selcenter = #(xval, yval, zval)
		DimData = #(selsize, selcenter)
		return DimData
	),

	fn ProcessObject target =
	(
		selcenter = target.center
		xval = target.max.x - target.min.x
		yval =target.max.y - target.min.y
		zval = target.max.z - target.min.z
		selsize = #(xval, yval, zval)
		DimData = #(selsize, selcenter)
		return DimData
	),
	
	fn GetSubObjectData target =
	(
		DimData = #()
		SubObjectMode = SubObjectLevel
		case SubObjectMode of
		(
			1: --verts
			(
				vertSel = target.selectedVerts
				if vertSel.count > 1 then
				(
					DimData = ProcessVerts vertSel
				)
				else --if no verts selected, use the whole object
				(
					DimData = ProcessObject target
				)
			)
			2: --edges
			(
				edgeSel = target.selectedEdges
				if edgeSel.count > 0 then
				(
					objSet[1].ConvertSelection #CurrentLevel #Vertex
					vertSel = objSet[1].selectedVerts
					DimData = ProcessVerts vertSel
					SubObjectLevel = SubObjectMode
				)
				else --if no verts selected, use the whole object
				(
					DimData = ProcessObject target
				)
			)
			4: --polygons
			(
				polySel = target.selectedFaces
				if polySel.count > 0 then
				(
					target.ConvertSelection #CurrentLevel #Vertex
					vertSel = target.selectedVerts
					DimData = ProcessVerts vertSel
					SubObjectLevel = SubObjectMode
				)
				else --if no verts selected, use the whole object
				(
					DimData = ProcessObject target
				)
			)
			5: --elements
			(
				polySel = target.selectedFaces
				if polySel.count > 0 then
				(
					target.ConvertSelection #CurrentLevel #Vertex
					vertSel = target.selectedVerts
					DimData = ProcessVerts vertSel
					SubObjectLevel = SubObjectMode
				)
				else --if no verts selected, use the whole object
				(
					DimData = ProcessObject target
				)
			)
			default:
			(
				DimData = ProcessObject target
			)
		)
		return DimData
	),
	
	fn CreatePrimitive DimData =
	(
		newPrimitive
		xval = DimData[1][1]
		yval = DimData[1][2]
		zval = DimData[1][3]
		PrimitiveColor = (color 192 255 0) --lime
		divisionsValue = Autoprimitive.Divisions.Value
		case Autoprimitive.PrimitiveType of
		(
			"box":
			(
				newPrimitive = box length:yval width:xval height:zval lengthsegs:divisionsValue widthsegs:divisionsValue heightsegs:divisionsValue
				newPrimitive.pivot = newPrimitive.center
			)
			"sphere":
			(
				radius = xval
				if (yval > radius) then radius = yval
				if (zval > radius) then radius = zval
				newPrimitive = sphere radius:(radius/2) segments:divisionsValue
			)
			"cylinderx":
			(
				radius = yval
				if zval > radius then radius = zval
				newPrimitive = cylinder radius:(radius/2) height:xval sides:divisionsValue
				in coordsys local rotate newPrimitive (eulerangles 0 90 0)
			)
			"cylindery":
			(
				radius = xval
				if zval > radius then radius = zval
				newPrimitive = cylinder radius:(radius/2) height:yval sides:divisionsValue
				in coordsys local rotate newPrimitive (eulerangles 90 0 0)
			)
			"cylinderz":
			(
				radius = xval
				if yval > radius then radius = yval
				newPrimitive = cylinder radius:(radius/2) height:zval sides:divisionsValue
			)
		)
		newPrimitive.wirecolor = PrimitiveColor
		newPrimitive.center = [DimData[2][1], DimData[2][2], DimData[2][3]]
-- 		newPrimitive.pivot = target.pivot
		if (Autoprimitive.ConvertCheckbox.Checked == true) then
		(
			ConvertToPoly newPrimitive
		)
		newPrimitive
	),
	
	fn DispatchCreatePrimitive s e =
	(
		for this in selection where classOf this == Editable_mesh do convertToPoly this
		objSet = for this in selection where classOf this == Editable_Poly collect this
		if objSet.count > 0 then
		(
			primitives = #()
			
			if s.Text == "Box" then Autoprimitive.PrimitiveType = "box"
			if s.Text == "Sphere" then Autoprimitive.PrimitiveType = "sphere"
			if s.Text == "Cylinder (x-axis)" then Autoprimitive.PrimitiveType = "cylinderx"
			if s.Text == "Cylinder (y-axis)" then Autoprimitive.PrimitiveType = "cylindery"
			if s.Text == "Cylinder (z-axis)" then Autoprimitive.PrimitiveType = "cylinderz"
				
			if (objSet.count == 1 and SubObjectLevel != 0) then
			(
				SubObjectMode = SubObjectLevel
				DimData = Autoprimitive.GetSubObjectData objSet[1]
				primitives = Autoprimitive.CreatePrimitive DimData
				SubObjectLevel = SubObjectMode
			)
			else
			(
				for this in objSet do
				(
					DimData = Autoprimitive.ProcessObject this
					newPrimitive = Autoprimitive.CreatePrimitive DimData
					append primitives newPrimitive
				)
			)
			if Autoprimitive.SelectCheckbox.Checked == true then select primitives
		)
		else
		(
			messagebox "Please select some an editable poly object"
		)
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn UpdateLocalConfigFileSave =
	(
		setINISetting IniFilePath "Autoprimitive" "WinLocX" (WinLocX as string)
		setINISetting IniFilePath "Autoprimitive" "WinLocY" (WinLocY as string)
		setINISetting IniFilePath "Autoprimitive" "ConvertState" (ConvertCheckbox.Checked as string)
		setINISetting IniFilePath "Autoprimitive" "SelectState" (SelectCheckbox.Checked as string)
		setINISetting IniFilePath "Autoprimitive" "DivisionsValue" (Autoprimitive.Divisions.Value as string)		
	),
	
	fn UpdateLocalConfigFileLoad =
	(
		local convert = true
		local selectState = true
		try
		(
			WinLocX = getINISetting IniFilePath "Autoprimitive" "WinLocX" as integer
			WinLocY = getINISetting IniFilePath "Autoprimitive" "WinLocY" as integer
			convert = getINISetting IniFilePath "Autoprimitive" "ConvertState" as booleanClass
			selectState = getINISetting IniFilePath "Autoprimitive" "SelectState" as booleanClass
			Autoprimitive.Divisions.Value = getINISetting IniFilePath "Autoprimitive" "DivisionsValue" as integer
		)
		catch()
		AutoprimitiveForm.Location = dotNetObject "system.drawing.point" WinLocX WinLocY
		Autoprimitive.ConvertCheckbox.Checked = convert
		Autoprimitive.SelectCheckbox.Checked = selectState
	),
	fn DispatchLoadIniFile s e = Autoprimitive.UpdateLocalConfigFileLoad(),	
	fn DispatchSaveIniFile s e = Autoprimitive.UpdateLocalConfigFileSave(),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--EVENT HANDLERS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn DispatchSetWinLoc s e = Autoprimitive.SetWinLoc s e,
	fn SetWinLoc s e =
	(
		WinLocX = AutoprimitiveForm.Location.X
		WinLocY = AutoprimitiveForm.Location.Y
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn CreateUI =
	(		
		-- form setup		
		AutoprimitiveForm = dotNetObject "maxCustomControls.maxForm"
		AutoprimitiveForm.Size = dotNetObject "System.Drawing.Size" WinWidth WinHeight
		AutoprimitiveForm.Text = "Autoprimitive"
		AutoprimitiveForm.startPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").manual
		AutoprimitiveForm.Location = dotNetObject "system.drawing.point" 0 80
		AutoprimitiveForm.FormBorderStyle = FBS_SizableToolWindow
		AutoprimitiveForm.sizeGripStyle = (dotNetClass "SizeGripStyle").show
		AutoprimitiveForm.startPosition = SP_Manual
		dotNet.AddEventHandler AutoprimitiveForm "Load" DispatchLoadIniFile
		dotNet.AddEventHandler AutoprimitiveForm "Closing" DispatchSaveIniFile
		dotNet.AddEventHandler AutoprimitiveForm "Move" DispatchSetWinLoc
		
		--content
		AutoprimitiveToolTip = dotnetobject "ToolTip"
		
		AutoprimitiveTable = dotNetObject "TableLayoutPanel"
		AutoprimitiveTable.Dock = DS_Fill
		AutoprimitiveForm.Controls.Add AutoprimitiveTable
		AutoprimitiveTable.RowCount = 8
		AutoprimitiveTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		AutoprimitiveTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		AutoprimitiveTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		AutoprimitiveTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		AutoprimitiveTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		AutoprimitiveTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		AutoprimitiveTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		AutoprimitiveTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		AutoprimitiveTable.ColumnCount = 1
		AutoprimitiveTable.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		AutoprimitiveForm.Controls.Add AutoprimitiveTable
		
		
		BoxButton = dotnetobject "Button"
		BoxButton.Text = "Box"
		BoxButton.Dock = DS_Fill
		BoxButton.BackColor = RGB_Transparent
		AutoprimitiveTable.Controls.Add BoxButton 0 0
		dotNet.AddEventHandler BoxButton "Click" DispatchCreatePrimitive
		SphereButton = dotnetobject "Button"
		SphereButton.Text = "Sphere"
		SphereButton.Dock = DS_Fill
		SphereButton.BackColor = RGB_Transparent
		AutoprimitiveTable.Controls.Add SphereButton 0 1
		dotNet.AddEventHandler SphereButton "Click" DispatchCreatePrimitive
		CylinderXButton = dotnetobject "Button"
		CylinderXButton.Text = "Cylinder (x-axis)"
		CylinderXButton.Dock = DS_Fill
		CylinderXButton.BackColor = RGB_Transparent
		AutoprimitiveTable.Controls.Add CylinderXButton 0 2
		dotNet.AddEventHandler CylinderXButton "Click" DispatchCreatePrimitive
		CylinderYButton = dotnetobject "Button"
		CylinderYButton.Text = "Cylinder (y-axis)"
		CylinderYButton.Dock = DS_Fill
		CylinderYButton.BackColor = RGB_Transparent
		AutoprimitiveTable.Controls.Add CylinderYButton 0 3
		dotNet.AddEventHandler CylinderYButton "Click" DispatchCreatePrimitive
		CylinderZButton = dotnetobject "Button"
		CylinderZButton.Text = "Cylinder (z-axis)"
		CylinderZButton.Dock = DS_Fill
		CylinderZButton.BackColor = RGB_Transparent
		AutoprimitiveTable.Controls.Add CylinderZButton 0 4
		dotNet.AddEventHandler CylinderZButton "Click" DispatchCreatePrimitive
		
		DivisionsPanel = dotNetObject "TableLayoutPanel"
		DivisionsPanel.ColumnCount = 2
		DivisionsPanel.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 55)
		DivisionsPanel.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 45)
		AutoprimitiveTable.Controls.Add DivisionsPanel 0 5
		
		DivisionsLabel = dotNetObject "Label"
		DivisionsLabel.Text = "Divisions"
		DivisionsLabel.Dock = DS_Fill
		DivisionsLabel.TextAlign = (dotNetClass "System.Drawing.ContentAlignment").MiddleCenter
		DivisionsPanel.Controls.Add DivisionsLabel 0 0
		
		
		Divisions = dotNetObject "NumericUpDown"
		Divisions.Minimum = 1
		Divisions.Maximum = 32
		Divisions.Dock = DS_Fill
		DivisionsPanel.Controls.Add Divisions 1 0
		
		ConvertCheckbox = dotNetObject "CheckBox"
		ConvertCheckbox.Text = "convert to poly"
		AutoprimitiveTable.Controls.Add ConvertCheckbox 0 6
		
		SelectCheckbox = dotNetObject "CheckBox"
		SelectCheckbox.Text = "select created"
		AutoprimitiveTable.Controls.Add SelectCheckbox 0 7
		
		--draw form
		AutoprimitiveForm.showModeless()
		AutoprimitiveForm
	)
)

if Autoprimitive != undefined then
(
	Autoprimitive.AutoprimitiveForm.close()
)

Autoprimitive = AutoprimitiveStruct()
Autoprimitive.CreateUI()