
if RsMapSetupGlobals == undefined then
(
	fileIn "pipeline/export/maps/globals.ms"
)

-- perforce integration script
filein "pipeline/util/p4.ms"

try (DestroyDialog RSL_SpawnPointMarker) catch()

rollout RSL_SpawnPointMarker "Spawn Point Marker"
(
	local p4 = RsPerforce()
	
	local mapType
	local offset = [0,0,0]
	
	local levelSpawnPosArray = #()
	local mapFileSpawnPosArray = #()
	
	local spawnPointManip
	
	fn readPlayerSpawns file =
	(
		local dataStream = openFile file
		skipToString dataStream "//== Player Spawn Points"
		local line = "  "
		
		while line != "//== Checkpoints" and not (eof dataStream) do 
		(
			line = (readLine dataStream) as stringStream
			
			if (matchPattern line pattern:"sev_PlayerSPs*") then
			(
				skipToString line "<<"
				local positionString = readDelimitedString line ">>"
				local filtered = FilterString positionString ", "
				
				skipToString line "\""
				local interior = readDelimitedString line "\""
				
				append levelSpawnPosArray (DataPair pos:[filtered[1] as float, filtered[2] as float, filtered[3] as float] interior:interior)
			)
		)
		
		close dataStream
	)
	
	mapped fn appendIfInMap data =
	(
		if data.interior.count > 0 then
		(
			local miloRoomNode = getNodeByName data.interior
			
			if miloRoomNode != undefined and (classOf miloRoomNode) == GtaMloRoom then
			(
				append mapFileSpawnPosArray (data.pos - offset) --((data.pos - offset) - geometry.center)
			)
		)
		else
		(
			if mapType == #mapFile then
			(
				if data.pos.x >= geometry.min.x and data.pos.x <= geometry.max.x and \
					data.pos.y >= geometry.min.y and data.pos.y <= geometry.max.y and \
					data.pos.z >= geometry.min.z and data.pos.z <= geometry.max.z then
				(
					append mapFileSpawnPosArray data.pos --(data.pos - geometry.center)
				)
			)
		)
	)
	
	fn getMapType =
	(
		mapType = #mapFile
		
		if (matchPattern maxFilePath pattern:"*props*") then
		(
			mapType = #prop
		)
		
		if (matchPattern maxFilePath pattern:"*interiors*") then
		(
			mapType = #interior
		)
		
		if (matchPattern maxFilePath pattern:"*buildings*") then
		(
			mapType = #building
		)
	)
	
	fn getMILO = 
	(
		local result
		
		for object in helpers do 
		(
			if (classOf object) == Gta_MILO then
			(
				result = object
			)
		)
		
		result
	)
	--------------------------------------------------------------------------------------------
	-- 						getMILOpositionFromIPLs()
	--------------------------------------------------------------------------------------------
	fn getMILOpositionFromIPLs mapPath MILO =
	(
		local result = false
		
		local IPLFiles = getFiles (mapPath + "*.ipl")
		
		for IPL in IPLFiles do
		(
			local dataStream = openFile IPL mode:"r"
			
			if (skipToString dataStream MILO) != undefined then
			(
				result = [0,0,0]
				local filtered = filterString (readLine dataStream) ", 	"
				
				result.x = filtered[2] as float
				result.y = filtered[3] as float
				result.z = filtered[4] as float
			)
			
			close dataStream
		)
		
		result
	)
	
	fn getOffset =
	(
		offset = [0,0,0]
		
		if mapType == #interior then
		(
			local MILO = getMILO()
			
			if MILO != undefined then
			(
				local mapPath = project.independent + "/levels/" + rsMapLevel + "/"
				local MILOpos = getMILOpositionFromIPLs mapPath MILO.name
				
				if MILOpos != undefined then
				(
					offset = MILOpos
				)
			)
		)
	)
	
	CheckButton chkb_toggle "Show Spawn Points" width:150 enabled:false
	
	on RSL_SpawnPointMarker open do 
	(
		if (p4.connected()) then
		(
			(p4.connect (project.intermediate))
			
			RsMapSetupGlobals()
			
			if rsMapLevel == "" then
			(
				chkb_toggle.enabled = false
			)
			else
			(
				local schFile = project.root + "/script/dev/Code/" + rsMapLevel + "/sev_imported_data.sch"
				
				p4.sync (p4.local2depot schFile)
				
				if (doesFileExist schFile) then
				(
					readPlayerSpawns schFile
					getMapType()
					getOffset()
					
					chkb_toggle.enabled = true
				)
				else
				(
					messageBox ("Unable to find sev_imported_data.sch\n" + schFile) title:"Error..."
				)
			)
		)
		else
		(
			messageBox "You are not currently connected to perforce.\nLogin to perforce and then run the tool again" title:"Error..."
		)
	)
	
	on chkb_toggle changed state do 
	(
		mapFileSpawnPosArray = #()
		
		spawnPointManip = getNodeByName "RSL_spawnPointManip"
		
		if state then
		(
			appendIfInMap levelSpawnPosArray
			
			if spawnPointManip != undefined and (classOf spawnPointManip) == RSL_SpawnPointManip then
			(
-- 				spawnPointManip.pos = geometry.center
				spawnPointManip.posArray = mapFileSpawnPosArray
			)
			else
			(
				spawnPointManip = RSL_SpawnPointManip posArray:mapFileSpawnPosArray name:"RSL_spawnPointManip" --pos:geometry.center
			)
		)
		else
		(
			if (isValidNode spawnPointManip) then
			(
				delete spawnPointManip
			)
		)
	)
)

CreateDialog RSL_SpawnPointMarker style:#(#style_toolwindow,#style_sysmenu)