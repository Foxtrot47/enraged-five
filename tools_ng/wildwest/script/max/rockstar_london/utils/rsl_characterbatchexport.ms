-- Batch Character Exporter --
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\pipeline\\RSL_OutputLog.ms")
filein (scriptspath+"pipeline\\export\\ui\\ped_ui.ms")


pathDir = "X:\\payne\\payne_art\\characters\\background"	-- Default Path

	fn findmaxfiles pathDir =
	(
		dir_array = GetDirectories (pathDir+"/*")
		for d in dir_array do
			join dir_array (GetDirectories (d+"/*"))

		Found_files = #()
		for f in dir_array do
			join Found_files (getFiles (f + "*.max"))
		Found_files 
	)
	
	fn strippaths filelist pathCnt=
	(
		shortlist = #()
		for i in filelist do		 
			append shortlist  (substring i pathCnt 100 )
		shortlist
	)
	
	

try (destroyDialog rollout_CharacterBatch) catch ()

rollout rollout_CharacterBatch "Character Batch Export" width:500 height:250
(
	fn updatelist pathDir =
	(
		rollout_CharacterBatch.lbx_Maxfiles.items = strippaths (findmaxfiles (pathDir)) (pathDir.count+1)
		rollout_CharacterBatch.lab_path.text  = 	pathDir
		rollout_CharacterBatch.lab_Filecnt.text = ("Files to Process:"+rollout_CharacterBatch.lbx_Maxfiles.items.count as string)
	)
	
	fn processMaxfiles filelist=
	(
		filein (scriptspath+"pipeline\\export\\ui\\ped_ui.ms")
		RsPedCompRoll.chkNoImageBuild.checked = true
		root = rollout_CharacterBatch.lab_path.text
		cnt = 0
		
		if outputlog != undefined then
		(
			outputlog.close()
		)
		outputlog = init_OutputLog()
		outputlog.start()
		outputlog.clear()
		
		
		
		
		for f in filelist  do
		(
			loadmaxfile (pathConfig.appendPath root f) quiet:true
			
			outputlog.log ("Loading: "+pathConfig.appendPath root f) type:1
			
			selectedNode = undefined
			for i in helpers do
			(
				if i.parent == undefined then 
				(
					if i.name == getFilenameFile maxfilename then
					(
					selectedNode =  i
					)

				)
			)
			if selectedNode != undefined then
			(
				select selectedNode
				RsPedTexturesRoll.btnAutoFill.pressed()
				RsPedCompRoll.btnExport.pressed()
			)
			else
			(
				outputlog.log "No Valid Dummy Node found,skipping" type:3
			)
			cnt+=1
			rollout_CharacterBatch.lab_Filecnt.text = ("Files to Process:"+(rollout_CharacterBatch.lbx_Maxfiles.items.count - cnt) as string)
	
		)
	)
	
	Button btn_Root "Root" align:#left across:2
	Button btn_Help "Help" align:#Right
	label lab_path "Path = " pos:[60,10] align:#left
	MultiListBox lbx_Maxfiles ""  width:480 height:14
	Button btn_BatchExport "Batch Export" align:#left  across:2	
	label lab_Filecnt "Files to Process:" align:#Right
	Button btn_BatchExportSel "Batch Export Selected" pos: [90,222]
	
	-- Scan the default path fro character files
	------------------------------------------------	
	on rollout_CharacterBatch open do
	(
		
		NewDir = getINISetting "c:/rs_utils.ini" "CharacterTools" "defaultpath"
		if NewDir != "" then pathDir = NewDir
		lab_path.text = pathDir
		
		lbx_Maxfiles.items = strippaths (findmaxfiles (pathDir)) (pathDir.count+1)
		lab_Filecnt.text = ("Files to Process:"+lbx_Maxfiles.items.count as string)
			
		
		
	)
	
	on btn_Root pressed do
	(
		filepath = getSavePath caption:"Character Directory" initialDir:pathDir
		if filepath != undefined then
		(
			updatelist filepath
			setINISetting "c:/rs_utils.ini" "CharacterTools" "defaultpath" (filepath as string)
		)
	)
	
	on btn_BatchExport pressed do
	(
		processMaxfiles (rollout_CharacterBatch.lbx_Maxfiles.items)
	)
	
	on btn_BatchExportSel pressed do
	(
		exportlist = #()
		for i in (lbx_Maxfiles.selection) do
		(
			append exportlist (lbx_Maxfiles.items[i])
		)
		processMaxfiles exportlist
	)
	
	on btn_Help pressed do
	(
		shelllaunch "https://mp.rockstargames.com/index.php/Character_Bacth_Export" ""
	)
	
)


createDialog rollout_CharacterBatch