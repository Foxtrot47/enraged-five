(
	-- Dot Net UI Template
	
	sizeType = dotNetClass "System.Windows.Forms.SizeType"
	CurrentSelection = (for object in selection where Superclassof object == GeometryClass and classof object != Xrefobject collect object)
	
	--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
	-- Main ListViewForm
	--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
	
	-- UI Elements declatation
	
	processForm = dotNetObject "MaxCustomControls.MaxForm"
	processForm.Width = 829
	processForm.Height = 750
	processForm.Text = "Rage Shader Spec Edit Tool"
	
	-- Table Layout Declaration
	
	TableLayout = dotNetObject "TableLayoutPanel"
		TableLayout.ColumnCount = 1
		TableLayout.RowCount = 2
		TableLayout.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		TableLayout.CellBorderStyle = (dotNetClass "TableLayoutPanelCellBorderStyle").None
		TableLayout.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		TableLayout.ColumnStyles.add (dotNetObject "System.Windows.Forms.ColumnStyle" sizeType.Percent 100)
	
	-- Menu Bars
	
	EditRefresh = dotNetObject "ToolStripMenuItem"
	EditRefresh.name = "Refresh Selection"
	EditRefresh.Text = "Refresh Selection"
			
	EditUndo = dotNetObject "ToolStripMenuItem"
	EditUndo.name = "Undo"
	EditUndo.Text = "Undo"
	
	EditMenu = dotNetObject "ToolStripMenuItem"
	EditMenu.DropDownItems.AddRange #(EditUndo, EditRefresh)
	EditMenu.name = "Edit"
	EditMenu.Text = "Edit"
		
	AboutMenu = dotNetObject "ToolStripMenuItem"
	AboutMenu.name = "About"
	AboutMenu.Text = "About"
			
	WikiHelp = dotNetObject "ToolStripMenuItem"
	WikiHelp.name = "Wiki Help"
	WikiHelp.Text = "Wiki Help"
	
	HelpMenu = dotNetObject "ToolStripMenuItem"
	HelpMenu.DropDownItems.AddRange #(AboutMenu, WikiHelp)
	HelpMenu.name = "Help"
	HelpMenu.Text = "Help"

	menuStrip = dotNetObject "MenuStrip"
	menuStrip.Items.AddRange #(EditMenu, HelpMenu)
	TableLayout.Controls.Add menuStrip 0 1
	
	-- end Menu Bars
	
	ListBox1 = dotNetObject "ListView"
		ListBox1.Height = 200
		ListBox1.View = (dotNetClass "View").Details
		ListBox1.multiselect = true
		ListBox1.gridlines = true
		ListBox1.fullRowSelect = true
		ListBox1.Sorting = (DotNetClass "System.Windows.Forms.SortOrder").None

	-- UI Element Docking
		
		TableLayout.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		processForm.Controls.Add TableLayout
		
			ListBox1.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
			TableLayout.Controls.Add ListBox1 0 2
	
	--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
	-- Popup Form
	--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
	
		PopupForm = dotNetObject "MaxCustomControls.MaxForm"
		PopupForm.Width = 200
		PopupForm.Height = 100
		PopupForm.Text = "Set Shader Value"
		
		NumericUpDown1 = dotNetObject "numericUpDown"
		NumericUpDown1.DecimalPlaces = 3
		NumericUpDown1.Increment = 0.1
		
		BtnSetValueTo = dotNetObject "Button"
		GroupBox1 = dotNetObject "GroupBox"

		-- UI Element Docking

		GroupBox1.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		PopupForm.Controls.Add GroupBox1 
	
			NumericUpDown1.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Top
			GroupBox1.Controls.Add NumericUpDown1 	
			
			BtnSetValueTo.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Top
			GroupBox1.Controls.Add BtnSetValueTo 
	
	--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
	
	-- Shader Info Variables 
	
	AllSpecValues=#()
	AllSpecFalloff=#()
	AllBumpieness=#()
	AllReflectivity=#()
	AllEmissive=#()
	AllMatNames=#()
	AllMatNodes=#()
	AllSourceObjects=#()
	AllShaderTypes=#()
	
	-- Used to check uneque Values in the collection of data to remove duplicates
	
	UnequeCheckArray=#()
	
	-- Used with Toggles for sorting
	
	SortingMode = "Ascending"
	NumbericSortingMode = "Decending"
	
	-- Used to Set Shader values
	
	ShaderValueToSet = undefined
	
	-- Undo Array
	
	UndoBackupArray=#()
	
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- DATA COLLECTION FUNCTIONS
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	
	-- To get all the Material Info from the selction of objects
	
	fn GetMeshesAssignedTo Mat =
	(
		AllDependents = refs.dependents Mat
		MatObjects = (for object in AllDependents where Superclassof object == GeometryClass and classof object != Xrefobject collect object)
	 
		Local AllObjects = ""
		  
		for Item in MatObjects do
		(
			AllObjects += (Item.name + ", ")
		)
		
		AllObjects
	)
	
	-- Collects Values from each shader - Used by "findShader" 
	
	fn ShaderInfo Mat Obj =
	(
		Local HasSpec = 0
		Local HasFalloff = 0
		Local HasBump = 0
		Local HasReflect = 0
		Local HasEmissive = 0
		
		UnequeCheck = appendIfUnique UnequeCheckArray Mat
				
		if UnequeCheck == true then
		(
			if classof Mat == Rage_Shader then
			(
				
				ShaderType = RstGetShaderName Mat
				RageVarCount = RstGetVariableCount Mat
				
				for i = 1 to RageVarCount do
				(
					if (RstGetVariableName Mat i) == "Specular Intensity" then
					(
						SpecValue = RstGetVariable Mat i
						append AllSpecValues SpecValue
						HasSpec = 1
					)
					
					if (RstGetVariableName Mat i) == "Specular Falloff" then
					(
						SpecFalloff = RstGetVariable Mat i
						append AllSpecFalloff SpecFalloff
						HasFalloff = 1
					)
					
					if (RstGetVariableName Mat i) == "Bumpiness" then
					(
						Bumpiness = RstGetVariable Mat i
						append AllBumpieness Bumpiness
						HasBump = 1
					)
					
					if (RstGetVariableName Mat i) == "Reflectivity" then
					(
						Reflect = RstGetVariable Mat i
						append AllReflectivity Reflect
						HasReflect = 1
					)
					
					if (RstGetVariableName Mat i) == "Emissive HDR" then
					(
						Emissive = RstGetVariable Mat i
						append AllEmissive Emissive
						HasEmissive = 1
					)
				)
				
				if (HasSpec + HasFalloff + HasBump + HasReflect + HasEmissive) != 0 then
				(
					if HasSpec == 0 then
					(
						append AllSpecValues "-"
					)
					if HasFalloff == 0 then
					(
						append AllSpecFalloff "-"
					)
					if HasBump == 0 then
					(
						append AllBumpieness "-"
					)
					if HasReflect == 0 then
					(
						append AllReflectivity "-"
					)
					if HasEmissive == 0 then
					(
						append AllEmissive "-"
					)
					
					append AllShaderTypes ShaderType
					append AllMatNodes Mat
					append AllMatNames Mat.name
					append AllSourceObjects (GetMeshesAssignedTo Mat)
				)
			)
		)
	)
	
	-- Recursive functiont to get all materials
	
	fn findShader mat obj =
	(
		if classof(mat) == Rage_Shader then 
		(
			ShaderInfo mat obj
		)
		else if classof(mat) == multimaterial then
		(
			for SubMat in mat do
			(
				if SubMat != undefined then findShader SubMat obj
			)
		)
	)
	
	-- Clear Array to hold data and creates multidimentional array of collected data.
	
	fn GetMaterialInfo ObjSelection =
	(
		AllSpecValues=#()
		AllSpecFalloff=#()
		AllBumpieness=#()
		AllReflectivity=#()
		AllEmissive=#()
		AllMatNames=#()
		AllMatNodes=#()
		AllSourceObjects=#()
		AllShaderTypes=#()
		UnequeCheckArray=#()
		
		Local ReturnArray=#()
	
		for Obj in ObjSelection do
		(
			findShader obj.Mat Obj
		)
		
		ReturnArray[1] = AllSpecValues
		ReturnArray[2] = AllSpecFalloff
		ReturnArray[3] = AllBumpieness
		ReturnArray[4] = AllSourceObjects
		ReturnArray[5] = AllMatNames
		ReturnArray[6] = AllMatNodes
		ReturnArray[7] = AllShaderTypes
		ReturnArray[8] = AllReflectivity
		ReturnArray[9] = AllEmissive
		
		ReturnArray
	)
	
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- HELP MENU FUNCTIONS
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	
	fn HelpLink=
	(
		result = ShellLaunch "firefox.exe" "https://mp.rockstarlondon.com/index.php?title=Rage_Shader_Value_Editor"
		if result == false then
		(
			ShellLaunch "iexplore.exe" "https://mp.rockstarlondon.com/index.php?title=Rage_Shader_Value_Editor"
		)
	)
		
	fn AboutMenuPopup =
	(
		messagebox "Version 0.1 Rage Shader Value Editor"
	)
	
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- EVENT HANDLER FUNCTIONS
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	
	fn ShowInMatBrowser s e=
	(
		ListBoxSelection = ListBox1.selectedItems
		
		if MatEditor.isOpen() != true then
		(
			MatEditor.Open()
		)
		
		setMeditMaterial 1 ListBoxSelection.Item[0].tag.Value
	)
	
	-- Populate ListView1
	
	fn PopulateList MaterialInfoArray =
	(
		Local CompiledArray =#()
		
		ListBox1.Clear()
		
		ListBox1.Columns.Add "Shader Type"
			(ListBox1.Columns.Item 0).width = 200
		ListBox1.Columns.Add "Material Name"
			(ListBox1.Columns.Item 1).width = 100
		ListBox1.Columns.Add "Spec Intensity"
			(ListBox1.Columns.Item 2).width = 80
		ListBox1.Columns.Add "Spec Falloff"
			(ListBox1.Columns.Item 3).width = 80
		ListBox1.Columns.Add "Bump"
			(ListBox1.Columns.Item 4).width = 70
		ListBox1.Columns.Add "Reflect"
			(ListBox1.Columns.Item 5).width = 70
		ListBox1.Columns.Add "Emissive"
			(ListBox1.Columns.Item 6).width = 70
		ListBox1.Columns.Add "Source Obj"
			(ListBox1.Columns.Item 7).width = 150
			
		for Index = 1 to MaterialInfoArray[1].count do
		(
			MyListItem = dotNetObject "ListViewItem" MaterialInfoArray[7][Index]
			MyListItem.SubItems.Add MaterialInfoArray[5][Index]
			MyListItem.SubItems.Item[1].tag = undefined
			MyListItem.SubItems.Add (MaterialInfoArray[1][Index] as string)
			MyListItem.SubItems.Item[2].tag = "Specular Intensity"
			MyListItem.SubItems.Add (MaterialInfoArray[2][Index] as string)
			MyListItem.SubItems.Item[3].tag = "Specular Falloff"
			MyListItem.SubItems.Add (MaterialInfoArray[3][Index] as string)
			MyListItem.SubItems.Item[4].tag = "Bumpiness"
			MyListItem.SubItems.Add (MaterialInfoArray[8][Index] as string)
			MyListItem.SubItems.Item[5].tag = "Reflectivity"
			MyListItem.SubItems.Add (MaterialInfoArray[9][Index] as string)
			MyListItem.SubItems.Item[6].tag = "Emissive"
			MyListItem.SubItems.Add MaterialInfoArray[4][Index]
			MyListItem.SubItems.Item[7].tag = undefined
			
			MyListItem.tag = dotNetMXSValue MaterialInfoArray[6][Index]
			
			append CompiledArray MyListItem
		)
		ListBox1.Items.AddRange CompiledArray
	)
	
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- COMPARE AND SORT FUNCTION
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	
	fn RefreshSelection =
	(
		CurrentSelection = (for object in selection where Superclassof object == GeometryClass and classof object != Xrefobject collect object)
		PopulateList (GetMaterialInfo CurrentSelection)
	)
	
	fn compareNumeric v1 v2 valArray: sortAcending:true =
	(
		local c1 = valArray[v1]
		local c2 = valArray[v2]
		local sv1 = 1
		local sv2 = -1
		
		if c1 == "-" then (c1 = -1)
		if c2 == "-" then (c2 = -1)
		
		if sortAcending == true then
		(
			local sv1 = -1
			local sv2 = 1
		)

		case of
		(
			(c1 < c2): sv1
			(c1 > c2): sv2
			default: 0
		)
	)

	fn QsortArray SortByArry =
	(
		indexArray = for i = 1 to SortByArry.count collect i
			
		if NumbericSortingMode == "Ascending" then
		(
			qsort indexArray compareNumeric valArray:SortByArry sortAcending:false
			NumbericSortingMode = "Decending"
		)
		else 
		(
			qsort indexArray compareNumeric valArray:SortByArry sortAcending:true
			NumbericSortingMode = "Ascending"
		)
		
		ReturnArray=#()
		
		TempAllSpecValues=#()
		TempAllSpecFalloff=#()
		TempAllBumpieness=#()
		TempAllReflectivity=#()
		TempAllEmissive=#()
		TempAllMatNames=#()
		TempAllMatNodes=#()
		TempAllSourceObjects=#()
		TempAllShaderTypes=#()
			
		for i = 1 to SortByArry.count do (TempAllSpecValues[i] = AllSpecValues[indexArray[i]])
		for i = 1 to SortByArry.count do (TempAllSpecFalloff[i] = AllSpecFalloff[indexArray[i]])
		for i = 1 to SortByArry.count do (TempAllBumpieness[i] = AllBumpieness[indexArray[i]])
		for i = 1 to SortByArry.count do (TempAllReflectivity[i] = AllReflectivity[indexArray[i]])
		for i = 1 to SortByArry.count do (TempAllEmissive[i] = AllEmissive[indexArray[i]])
		for i = 1 to SortByArry.count do (TempAllMatNames[i] = AllMatNames[indexArray[i]])
		for i = 1 to SortByArry.count do (TempAllMatNodes[i] = AllMatNodes[indexArray[i]])
		for i = 1 to SortByArry.count do (TempAllSourceObjects[i] = AllSourceObjects[indexArray[i]])
		for i = 1 to SortByArry.count do (TempAllShaderTypes[i] = AllShaderTypes[indexArray[i]])
		
		ReturnArray[1] = TempAllSpecValues
		ReturnArray[2] = TempAllSpecFalloff
		ReturnArray[3] = TempAllBumpieness
		ReturnArray[4] = TempAllSourceObjects
		ReturnArray[5] = TempAllMatNames
		ReturnArray[6] = TempAllMatNodes
		ReturnArray[7] = TempAllShaderTypes
		ReturnArray[8] = TempAllReflectivity
		ReturnArray[9] = TempAllEmissive
	
		ReturnArray
	)

	fn ColumnClickEvent s e=
	(
		SortReturn
		Sorted = false
		
		if e.Column == 2 then
		(
			SortReturn = QsortArray AllSpecValues
			Sorted = true
		)
		if e.Column == 3 then
		(
			SortReturn = QsortArray AllSpecFalloff
			Sorted = true
		)
		if e.Column == 4 then
		(
			SortReturn = QsortArray AllBumpieness
			Sorted = true
		)
		if e.Column == 5 then
		(
			SortReturn = QsortArray AllReflectivity
			Sorted = true
		)
		if e.Column == 6 then
		(
			SortReturn = QsortArray AllEmissive
			Sorted = true
		)
		
		if Sorted == false then
		(
			If SortingMode == "Ascending" then
			(
				ListBox1.Sorting = (DotNetClass "System.Windows.Forms.SortOrder").Descending
				SortingMode = "Descending"
			)
			else 
			(
				ListBox1.Sorting = (DotNetClass "System.Windows.Forms.SortOrder").Ascending
				SortingMode = "Ascending"			
			)
		)
		else
		(
			ListBox1.Sorting = (DotNetClass "System.Windows.Forms.SortOrder").None
			PopulateList SortReturn
		)
	)

	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- UNDO FUNCTIONS
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	
	fn RestorUndoArray =
	(
		if UndoBackupArray.count != 0 then
		(
			for Item in UndoBackupArray do 
			(
				RstSetVariable Item[1] Item[2] Item [3]
			)
			
			PopulateList (GetMaterialInfo CurrentSelection)
			
		)
	)
	
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- POPUP EVENT FUNCTIONS
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	
	fn SetShaderValues =
	(
		ListBoxSelection = ListBox1.selectedItems
		
		UndoBackupArray=#()
		
		for X = 0 to (ListBoxSelection.count - 1) do
		(
			Mat = ListBoxSelection.Item[X].tag.value
			
			if classof Mat == Rage_Shader then
			(
				RageVarCount = RstGetVariableCount Mat
					
				for i = 1 to RageVarCount do
				(
					if (RstGetVariableName Mat I) == ShaderValueToSet then
					(
						UndoEntry=#()
						UndoEntry[1] = Mat
						UndoEntry[2] = I 
						UndoEntry[3] = RstGetVariable Mat I 
						
						UndoBackupArray[(X+1)] = UndoEntry
					
						RstSetVariable Mat I (NumericUpDown1.text as float)
					)
				)
			)
		)
		
		PopulateList (GetMaterialInfo CurrentSelection)
		
	)
		
	fn PopUpClose s e=
	(
		e.Cancel = true
		PopupForm.visible = false
	)
	
	fn RightClickEvent sender mouseargs=
	(
		mouseButtonsEnum = dotNetClass "MouseButtons"
		
		if mouseargs.Button == mouseButtonsEnum.Right then
		(
			-- Get the Clicked Sub Item			
			SubItem = (ListBox1.HitTest mouseargs.x mouseargs.y).SubItem
			
			if  SubItem == undefined or SubItem.tag == undefined then 
			(
				BtnSetValueTo.Text = "NO Value"
				NumericUpDown1.text = "0.000"
				PopupForm.visible = false
				ShaderValueToSet = undefined
			)
			else
			(
				if PopupForm.visible != true then
				(
					PopupForm.ShowModeless()
				)
					
				BtnSetValueTo.Text = ("Click To Set " + SubItem.tag as string)
				ShaderValueToSet = (SubItem.tag as string)
				
				NumericUpDown1.text = SubItem.Text 
				
				PopupForm.Location = processForm.PointToScreen mouseargs.Location
				PopupForm.StartPosition = (dotNetClass "FormStartPosition").Manual
				PopupForm.TopMost = true
			)
		)
	)
	
	-- Creates Event handlers and creats the form
	
	fn CreateUI MaterialInfoArray =
	(
		-- Populate List Box 1 ----------------------------------------------
		
		for i = 1 to AllReflectivity.count do
		(
			print AllMatNames[i]
		)
					
		PopulateList MaterialInfoArray
		
		-- Event Handlers --------------------------------------------------
		
		dotnet.addEventHandler EditRefresh "Click" RefreshSelection
		dotnet.addEventHandler EditUndo "Click" RestorUndoArray
		
		dotnet.addEventHandler WikiHelp "Click" HelpLink
		dotnet.addEventHandler AboutMenu "Click" AboutMenuPopup
				
		dotnet.addEventHandler BtnSetValueTo "Click" SetShaderValues
		dotnet.addEventHandler PopupForm "Closing"  PopUpClose
		dotnet.addEventHandler ListBox1 "DoubleClick" ShowInMatBrowser
		dotnet.addEventHandler ListBox1 "ColumnClick" ColumnClickEvent
		dotnet.addEventHandler ListBox1 "MouseDown" RightClickEvent
		
		-- Create Form ------------------------------------------------------
		
		processForm.ShowModeless()
		gc()
	)
		
	CreateUI (GetMaterialInfo CurrentSelection)
)


