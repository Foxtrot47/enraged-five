-- DotNetPresets.ms
--Andy Davis October 2012
--Rockstar London
--an extension to the Rockstar London DotNetPresets
--the original file was compiled by Paul Truss
--these presets are designed to work independently of Paul's

DS_Fill = (dotNetClass "System.Windows.Forms.DockStyle").Fill
DS_None = (dotNetClass "System.Windows.Forms.DockStyle").None
FBS_SizableToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").SizableToolWindow
FBS_FixedToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").FixedToolWindow
FBS_Sizable = (dotNetClass "System.Windows.Forms.FormBorderStyle").Sizable
FS_Flat = (dotNetClass "System.Windows.Forms.FlatStyle").Flat
FS_Standard = (dotNetClass "System.Windows.Forms.FlatStyle").Standard
SP_Manual =  (dotNetClass "System.Windows.Forms.FormStartPosition").Manual

TA_Center =  (dotNetClass "System.Drawing.ContentAlignment").MiddleCenter
TA_Left =  (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft
TA_Right =  (dotNetClass "System.Drawing.ContentAlignment").MiddleRight

Padding_None = dotNetObject "System.Windows.Forms.Padding" 0

--colours
RGB_Black = (dotNetClass "System.Drawing.Color").black
RGB_DarkGrey = (dotNetClass "System.Drawing.Color").fromARGB 102 102	102
RGB_Steel = (dotNetClass "System.Drawing.Color").FromARGB 200 210 220
RGB_Transparent = (dotNetClass "System.Drawing.Color").transparent
ARGB = (dotNetClass "System.Drawing.Color").FromARGB
ColorClass = (dotNetClass "System.Drawing.Color")

--fonts
Font_Calibri = dotNetObject "System.Drawing.Font" "Calibri" 8
Font_MSSans = dotNetObject "System.Drawing.Font" "Microsoft Sans Serif" 8