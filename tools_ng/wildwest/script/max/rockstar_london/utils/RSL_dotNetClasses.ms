--=-=-=-=-=-=-=-=-=-=-=-=-= RSL_dotNetClasses =-=-=-=-=-=-=-=-=-=-=-=-=--
/*
	File:
		RSL_DdotNetClasses.ms
	Description:
		Defines a stucture containing dotNet classes to speed up the process of creating dotNet tools
	Created by:
		Paul Truss
		Senior Technical Artist
		Rockstar London
	History:
		Created January 27 2010
*/
--=-=-=-=-=-=-=-=-=-=-=-=-= RSL_dotNetClasses =-=-=-=-=-=-=-=-=-=-=-=-=--
struct RSL_dotNetClasses
(
	--=-=-=-=-=-=-=-=-=-=-=-=-= dotNetClasses =-=-=-=-=-=-=-=-=-=-=-=-=--
	appClass = dotNetClass "System.Windows.Forms.Application",
	pointClass = dotNetClass "System.Drawing.Point",
	sizeClass = dotNetClass "System.Drawing.Size",
	SizeTypeClass = dotNetClass "System.Windows.Forms.SizeType",
	formBorderStyleClass = dotNetClass "System.Windows.Forms.FormBorderStyle",
	borderStyleClass = dotNetClass "System.Windows.Forms.BorderStyle",
	dockStyleClass = dotNetClass "System.Windows.Forms.DockStyle",
	flatStyleClass = dotNetClass "System.Windows.Forms.FlatStyle",
	CellBorderClass = dotNetClass "TableLayoutPanelCellBorderStyle",
	colourClass = dotNetClass "System.Drawing.Color",
	systemColourClass = dotNetClass "System.Drawing.SystemColors",
	imageClass = dotNetClass "System.Drawing.Image",
	bitmapClass = dotNetClass "System.Drawing.Bitmap",
	iconClass = dotNetClass "System.Drawing.Icon",
	imageLayoutClass = dotNetClass "System.Windows.Forms.ImageLayout",
	contentAllignmentClass = dotNetClass "System.Drawing.ContentAlignment",
	paddingClass = dotNetClass "System.Windows.Forms.Padding",
	fontStyleClass = dotNetClass "System.Drawing.FontStyle",
	appearanceClass = dotNetClass "System.Windows.Forms.Appearance",
	autoSizeModeClass = dotNetClass "System.Windows.Forms.AutoSizeMode",
	dragDropEffect = dotNetClass "System.Windows.Forms.DragDropEffects",
	dataFormats = dotnetclass "dataformats",
	directoryClass = DotNetClass "System.IO.Directory",
	drawingClass = dotNetClass "System.Drawing",
	graphicsClass = dotNetClass "System.Drawing.Graphics",
	brushClass = dotNetClass "System.Drawing.Brush",
	brushesClass = dotNetClass "System.Drawing.Brushes",
	solidBushClass = dotNetClass "System.Drawing.SolidBrush",
	rectangleClass = dotNetClass "System.Drawing.Rectangle",
	pixelFormatClass = dotNetClass "System.Drawing.Imaging.PixelFormat",
	imageFormatClass = dotNetClass "System.Drawing.Imaging.ImageFormat"
)

global RS_dotNetClass = RSL_dotNetClasses()
global RS_dotNetObject
global RS_dotNetPreset
global RS_dotNetUI