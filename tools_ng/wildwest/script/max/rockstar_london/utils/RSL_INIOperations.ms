
global RSL_INIOps

struct RSL_INIOperations
(
	fn writeLocalConfigINIStdRollout parent localConfigINI =
	(
		if (doesFileExist localConfigINI) then
		(
			local newFile = createFile localConfigINI
			close newFile
		)
		
		setINISetting localConfigINI "Main" "Position" ((GetDialogPos parent) as string)
		
		for control in parent.controls do
		(
			case (classOf control) of
			(
				CheckBoxControl:
				(
					setINISetting localConfigINI "Main" control.name (control.checked as string)
				)
				
				SpinnerControl:
				(
					setINISetting localConfigINI "Main" control.name (control.value as string)
				)
			)
		)
	),
	
	fn readLocalConfigINIStdRollout parent localConfigINI =
	(
		if (doesFileExist localConfigINI) then
		(
			for control in parent.controls do
			(
				case (classOf control) of
				(
					CheckBoxControl:
					(
						local INISetting = getINISetting localConfigINI "Main" control.name
						
						if INISetting != "" then
						(
							control.checked = readValue (INISetting as stringStream)
						)
					)
					
					SpinnerControl:
					(
						local INISetting = getINISetting localConfigINI "Main" control.name
						
						if INISetting != "" then
						(
							control.value = readValue (INISetting as stringStream)
						)
					)
				)
			)
		)
		else
		(
			local newFile = createFile localConfigINI
			close newFile
			
			writeLocalConfigINIStdRollout parent localConfigINI
		)
	),
	
	
	
	fn getType type =
	(
		local filtered = filterString type "."
		
		filtered[filtered.count]
	),
	
	fn readDotNetControlSetting control localConfigINI =
	(
		local type
		local INISetting
		
		if control != undefined then
		(
			type = getType ((control.GetType()).toString())
			INISetting = getINISetting localConfigINI "Main" control.name
		)
		
		case type of
		(
-- 			default:print type
			"CheckBox":
			(
				if INISetting != "" then
				(
					control.checked = readValue (INISetting as stringStream)
				)
			)
			"NumericUpDown":
			(
				if INISetting != "" then
				(
					control.value = readValue (INISetting as stringStream)
				)
			)
			"ToolStripMenuItem":
			(
				if INISetting != "" and control.CheckOnClick then
				(
					control.checked = readValue (INISetting as stringStream)
				)
				
				for i = 0 to control.DropDownItems.count - 1 do
				(
					local subControl = control.DropDownItems.Item[i]
					
					readDotNetControlSetting subControl localConfigINI
				)
			)
			"MenuStrip":
			(
				for i = 0 to control.Items.count - 1 do
				(
					local subControl = control.Items.Item[i]
					
					readDotNetControlSetting subControl localConfigINI
				)
			)
		)
		
		if (isProperty control #controls) then
		(
			for i = 0 to control.controls.count - 1 do
			(
				local subControl = control.controls.Item[i]
				
				readDotNetControlSetting subControl localConfigINI
			)
		)
	),
	
	fn readLocalConfigINIDotNetForm form localConfigINI variableArray:undefined fileArray:undefined =
	(
		local result = #()
		
		if (doesFileExist localConfigINI) then
		(
			local positionINISetting = (getINISetting localConfigINI "Main" "Position")
			local sizeINISetting = (getINISetting localConfigINI "Main" "Size")
			
			if positionINISetting != "" then
			(
				local formPosition = readValue ((getINISetting localConfigINI "Main" "Position") as stringStream)
				
				form.location.x = formPosition.x
				form.location.y = formPosition.y
			)
			
			if sizeINISetting != "" then
			(
				local formSize = readValue ((getINISetting localConfigINI "Main" "Size") as stringStream)
				
				form.width = formSize.x
				form.height = formSize.y
			)
			
			if variableArray != undefined then
			(
				for entry in variableArray do
				(
					local INISetting = ((getINISetting localConfigINI "Main" entry) as stringStream)
					
					if INISetting != "" then
					(
						local value = readValue ((getINISetting localConfigINI "Main" entry) as stringStream)
						local variableString = entry + " = "
						
						case of
						(
							(matchPattern entry pattern:"*color*"):variableString += "RS_dotNetClass.colourClass.fromARGB " + value.x as string + " " + value.y as string + " " + value.z as string
						)
						
						try
						(
							execute variableString
						)
						catch
						(
							format "RSL_INIOps.readLocalConfigINIDotNetForm()\nFailed to execute...\nINI:%\n%\n" localConfigINI variableString
						)
					)
				)
			)
			
			if fileArray != undefined then
			(
				local keyArray = getINISetting localConfigINI "Files"
				result = for entry in keyArray collect (getINISetting localConfigINI "Files" entry)
			)
			
			readDotNetControlSetting form localConfigINI
		)
		else
		(
			format "% not found, creating file.\n%\n" (filenameFromPath localConfigINI) localConfigINI
			local newFile = createFile localConfigINI
			close newFile
		)
		
		result
	),
	
	fn writeDotNetControlSetting control localConfigINI =
	(
		local type = getType ((control.GetType()).toString())
		
-- 		format "Type:% name:%\n" type control.name
		
		case type of
		(
-- 			default:print type
			"CheckBox":
			(
				setINISetting localConfigINI "Main" control.name (control.checked as string)
			)
			"NumericUpDown":
			(
				local dValue = getProperty control #value asDotNetObject:true
				local fValue = (dotNetClass "System.Decimal").ToSingle dValue
				
				setINISetting localConfigINI "Main" control.name (fValue as string)
			)
			"ToolStripMenuItem":
			(
				if control.CheckOnClick then
				(
					setINISetting localConfigINI "Main" control.name (control.checked as string)
				)
				
				for i = 0 to control.DropDownItems.count - 1 do
				(
					local subControl = control.DropDownItems.Item[i]
					
					writeDotNetControlSetting subControl localConfigINI
				)
			)
			"MenuStrip":
			(
				for i = 0 to control.Items.count - 1 do
				(
					local subControl = control.Items.Item[i]
					
					writeDotNetControlSetting subControl localConfigINI
				)
			)
		)
		
		if (isProperty control #controls) then
		(
			for i = 0 to control.controls.count - 1 do
			(
				local subControl = control.controls.Item[i]
				
				writeDotNetControlSetting subControl localConfigINI
			)
		)
	),
	
	fn writeLocalConfigINIDotNetForm form localConfigINI variableArray:undefined fileArray:undefined =
	(
		if (doesFileExist localConfigINI) then
		(
			local newFile = createFile localConfigINI
			close newFile
		)
		
		setINISetting localConfigINI "Main" "Position" ([form.location.x,form.location.y] as string)
		setINISetting localConfigINI "Main" "Size" ([form.width,form.height] as string)
		
		if variableArray != undefined then
		(
			for entry in variableArray do
			(
				setINISetting localConfigINI "Main" entry.name (entry.value as string)
			)
		)
		
		if fileArray != undefined then
		(
			for i = 1 to fileArray.count do 
			(
				setINISetting localConfigINI "Files" ("file" + i as String) (fileArray[i] as string)
			)
		)
		
		writeDotNetControlSetting form localConfigINI
	)
)

RSL_INIOps = RSL_INIOperations()