plugin simpleObject IPLStreamingBox
name:"IPL Box"
classID:#(0x9e89f5f, 0x28af7ed9)
category:"RSL Primitives"
(
	parameters main rollout:paramRollout
	(
		rpfFile type:#string animatable:false ui:imageName default:""
		u32BoxID type:#string animatable:false ui:mapName default:"CHANGEME"
		top type:#float animatable:false ui:spnTop default: 1.0
		bottom type:#float animatable:false ui:spnBottom default: -1.0
		left type:#float animatable:false ui:spnLeft default: -1.0
		right type:#float animatable:false ui:spnRight default: 1.0
		front type:#float animatable:false ui:spnFront default: 1.0
		back type:#float animatable:false ui:spnBack default: -1.0
		
		on top set val do this.botright.z = val
		on bottom set val do this.topleft.z = val
		on left set val do this.topleft.x = val
		on right set val do this.botright.x = val
		on front set val do this.botright.y = val
		on back set val do this.topleft.y = val
		
		topleft type:#point3 default:[-1.0, 1.0, 1.0]
		botright type:#point3 default:[1.0, -1.0, -1.0]
		linkdata type:#stringTab tabSize:0 tabSizeVariable:true animatable:false
		
		colourRed type:#point3 default:[1,0,0]
		colourGreen type:#point3 default:[0,1,0]
		colourChild type:#point3 default:[0,0,1]
		colourChildSelected type:#point3 default:[0.5,0.5,1]
		colorParent type:#point3 default:[1,0,0]
		colourGold type:#point3 default:[1.0,0.8,0]
		colourBase type:#point3 default:((color 214 229 166) / 255.0)
		colourCurrent type:#point3 default:((color 214 229 166) / 255.0)
	)
	
	rollout paramRollout "Parameters"
	(
		Edittext imageName "Image:" text:"" labelOnTop:true readOnly:true
		Edittext mapName "ID:" text:"" labelOnTop:true --readOnly:true
		Spinner spnTop "Top:" range:[0.0, 1000.0, 1.0] scale:0.01
		Spinner spnBottom "Bottom:" range:[-1000.0, 0.0, -1.0] scale:0.01
		Spinner spnLeft "Left:" range:[-1000.0, 0.0, -1.0] scale:0.01
		Spinner spnRight "Right:" range:[0.0, 1000.0, 1.0] scale:0.01
		Spinner spnFront "Front:" range:[0.0, 1000.0, 1.0] scale:0.01
		Spinner spnBack "Back:" range:[-1000.0, 0.0, -1.0] scale:0.01
		button btnReset "Reset [1, 1, 1]" width:140
		
		on btnReset pressed do
		(
			undo "Reset IPLBox" on
			(
				spnTop.value = 1.0
				spnBottom.value = -1.0
				spnLeft.value = -1.0
				spnRight.value = 1.0
				spnFront.value = 1.0
				spnBack.value = -1.0
			)
		)
	)
	
	tool create
	(
		on mousePoint click do
		(
			nodeTM.translation = worldPoint
			#stop
		)
	)
	
	on buildMesh do
	(
		setMesh mesh \
			verts:#([topleft.x, botright.y, topleft.z], [botright.x, botright.y, topleft.z], [topleft.x, topleft.y, topleft.z], \
						[botright.x, topleft.y, topleft.z], [topleft.x, botright.y, botright.z], [botright.x, botright.y, botright.z], \
						[topleft.x, topleft.y, botright.z], [botright.x, topleft.y, botright.z]) \
			faces:#([4,3,1], [1,2,4], [8,6,5], [5,7,8], [6,2,1], [1,5,6], [8,4,2], [2,6,8], [7,3,4], [4,8,7], [5,1,3], [3,7,5])
		
		meshop.autoSmooth mesh (for face in mesh.faces collect face.index) 1.0
		meshop.autoEdge mesh (for edge in mesh.edges collect edge.index) 1.0
		
		for o in refs.dependents this where isProperty o #material do
		(
			if o.material == undefined then
			(
				o.material = standardMaterial()
				o.material.opacity = 50.0
			)
			
			o.material.diffuse = colourCurrent * 255
			o.wireColor = colourCurrent * 255
		)
	)
	
)
