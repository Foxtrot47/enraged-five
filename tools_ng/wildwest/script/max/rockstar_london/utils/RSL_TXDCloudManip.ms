plugin simpleManipulator TXDCloudManip
name:"TXD Cloud"
classID:#(0x6e13cca, 0x63d11d2b)
category:"Manipulators"
invisible:true
(
	parameters main --rollout:paramRollout
	(
		TXD type:#string animatable:false default: ""
		objectArray type:#nodeTab tabSize:0 tabSizeVariable:true animatable:false
		textureArray type:#stringTab tabSize:0 tabSizeVariable:true animatable:false --texturemapTab
		memory type:#float animatable:false default:0.0
		uniqueTextureArray type:#stringTab tabSize:0 tabSizeVariable:true animatable:false
		TXDLinks type:#stringTab tabSize:0 tabSizeVariable:true animatable:false
		centerPoint type:#point3 animatable:false default:[0,0,0]
		showObjectLinks type:#boolean default:true
		showTXDLinks type:#boolean default:true
	)
	
	fn getTXDCloud TXDname =
	(
		local result
		
		for o in rootNode.children where isProperty o #TXD do
		(
			if o.txd == TXDname then
			(
				result = o
			)
		)
		
		result
	)
	
	on canManipulate target return (classOf target) == TXDCloudManip
	
	tool create
	(
		on mousePoint click do
		(
			nodeTM.translation = worldPoint
			#stop
		)
	)
	
	on updateGizmos do
	(
		this.clearGizmos()
		
		if (target != undefined) then
		(
			this.TXD = target.TXD
			this.objectArray = target.objectArray
			this.textureArray = target.textureArray
			this.memory = target.memory
			this.uniquetextureArray = target.uniquetextureArray
			this.TXDLinks = target.TXDLinks
			this.centerPoint = target.centerPoint
		)
		
		this.centerPoint = [0,0,0]
		
		for object in this.objectArray do
		(
			this.centerPoint += object.center
		)
		
		this.centerPoint /= this.objectArray.count
		
		if showObjectLinks then
		(
			local objectLines = manip.makeGizmoShape()
			
			for i = 1 to this.objectArray.count do
			(
				if i > 1 then
				(
					objectLines.startNewLine()
				)
				
				objectLines.addPoint this.centerPoint
				objectLines.addPoint this.objectArray[i].center
				
				this.addGizmoMarker #circle this.objectArray[i].center 0 [1.0,0.8,0] [1.0,0.8,0]
				
				if this.uniqueTextureArray[i] != undefined then
				(
					local filtered = filterString this.uniqueTextureArray[i] " :"
					
					if filtered[2] != "0" then
					(
						this.addGizmoText this.uniqueTextureArray[i] (this.objectArray[i].center - [0,0,0.25]) 0 [0.25,0.25,0.25] [0.25,0.25,0.25]
					)
				)
			)
			
			this.addGizmoShape objectLines gizmoDontHitTest [0.5,0.5,1] [0.5,0.5,1]
		)
		
		this.addGizmoText (this.TXD + " : " + this.textureArray.count as string + " : " + this.memory as string + "K") this.centerPoint gizmoDontHitTest [0,0,0] [0,0,0]
		
		if showTXDLinks and this.TXDLinks.count > 0 then
		(
			local TXDLinkLines = manip.makeGizmoShape()
			
			for i = 1 to this.TXDLinks.count do
			(
				local fitered = filterString this.TXDLinks[i] ","
				local linkedTXD = fitered[1]
				local TXDCloud = getTXDCloud linkedTXD
				
				if TXDCloud != undefined then
				(
					if i > 1 then
					(
						TXDLinkLines.startNewLine()
					)
					
					TXDLinkLines.addPoint this.centerPoint
					TXDLinkLines.addPoint TXDCloud.centerPoint
					
					local textString = fitered[2] + " : " + fitered[3]
					local textPosition = (this.centerPoint + ((TXDCloud.centerPoint - this.centerPoint) * 0.5))
					
					this.addGizmoText textString textPosition gizmoDontHitTest [0.5,0,0] [0.5,0,0]
					this.addGizmoMarker #smallDot textPosition 0 [1,0,0] [1,0,0]
				)
			)
			
			this.addGizmoShape TXDLinkLines gizmoDontHitTest [1,0,0] [1,0,0]
		)
		
		return ""
	)
)