
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/pipeline/RSL_SceneXml.ms")

try (DestroyDialog RSL_InstanceLighting) catch()

rollout RSL_InstanceLighting "R* Instance Lighting" width:140
(
	button btn_import "Import From Selected" width:120 align:#left
	button btn_delete "Delete All Instances" width:120 align:#left
	
	on RSL_InstanceLighting open do 
	(
		
	)
	
	on btn_import pressed do 
	(
		RSL_SceneXmlOps.importLights()
	)
	
	on btn_delete pressed do 
	(
		delete (for L in lights where (classOf L) == RageLightInstance collect L)
	)
)

createDialog RSL_InstanceLighting style:#(#style_toolwindow,#style_sysmenu)