-- Simple utility to copy the materials from one obect to another and set to Rage Default

rollout RSL_SelectElemetnsBySize "Poly Selection Tools" width:170
(
	hyperlink lnkHelp	"Help?" address:"https://mp.rockstargames.com/index.php/LOD_Optimizer_Tools" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btn_SelectElements "Select Elements by Size" width:160 align:#center
	spinner spn_VolumeSize "< Volume Size" range:[0,4.0,0.1]  type:#float
	
	fn getfaceArea obj FaceArray FaceVol =
	(
		local faceArea = 0
		for i in FaceArray do
		(
			faceArea += polyop.getFaceArea obj i	
		)
		result = false
		if faceArea < FaceVol then result = true
		result
	)

	fn selectelementsbySIze obj VolumeSize =
	(
		-- set the object to face selection
		setSelectionLevel  obj #face
		subObjectLevel = 5
		
		local facesel = #{}--polyop.getFaceSelection obj
		facecount = obj.getNumFaces() 
		for i = 1 to facecount do
		(		
				polyop.setFaceSelection obj i --#{1}
				obj.selectElement()
				ele = polyop.getFaceSelection obj
			
			if (getfaceArea obj ele VolumeSize) then
			(
				facesel += ele
			)
		)
		polyop.setFaceSelection obj facesel

	)
	
	on btn_SelectElements pressed do
	(
		local objtype  = selection[1]
		if classof objtype == Editable_Poly or classof objtype == Editable_Mesh then
		(
			selectelementsbySIze selection[1] spn_VolumeSize.value
		)
	)
	
	
	
	
)

rollout RSL_SimpleShader "RS Material Duper" width:170
(
	hyperlink lnkHelp	"Help?" address:"https://mp.rockstargames.com/index.php/LOD_Optimizer_Tools" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btn_copyMaterial "Copy Material" width:160 align:#center
	checkbox chk_converttodefault "Convert to default" checked:true
	checkbox chk_removeunusedmats "Remove Unused Shaders" checked:false
	
	fn CountFacesUsedOn obj materialIndex =
	(
		local ValidMaterialIndexs = #()
		
		local facecount = 0
		if classOf Obj == editable_mesh then (facecount = polyop.getNumFaces obj)
		if classOf Obj == editable_poly then (facecount = polyop.getNumFaces obj)

		for i = 1 to facecount do
		(
			case (classOf obj) of
			(
				editable_mesh:
				(
					print (getFaceMatID obj i)
					/*
					if (getFaceMatID object face.index) == ID then
					(
						result += 1
					)
					*/
				)
				editable_poly:
				(
					local faceMatID = (polyOp.getFaceMatID obj i) 
					
					local x = findItem materialIndex faceMatID
					if x != 0 then
					(
						appendifunique ValidMaterialIndexs faceMatID
					)
				)
			)
		)
		
		ValidMaterialIndexs
	)
	
	fn converttoDefault mat =
	(
		for i in mat do
		(
			if classof i == Rage_Shader then
			(
				if  (RstGetShaderName i) != "default.sps" then
				(
					RstSetShaderName i "default.sps"
				)
			)
		)
	)
	
	
	fn getmaterial obj =
	(
		-- GO through teh objects face and collect the material IDs and its material
		-- get the material asigned to the object		
		
		local newmaterial = copy obj.material
		assignNewName newmaterial -- Make material unique
		local materialIndexList = newmaterial.materialIDList  -- Get the material List ID's
		obj.material = newmaterial
		
		if chk_converttodefault.checked then
		(
			converttoDefault newmaterial
		)
		
		/*
		if chk_removeunusedmats.checked then
		(
			print materialIndexList
			local ValidMatIndexs = CountFacesUsedOn obj materialIndexList
			
			print ValidMatIndexs
		)
		*/
		
		
	)

	on btn_copyMaterial pressed do
	(
		
		if selection.count == 1 do
		(
			if classof selection[1].material == Multimaterial then
			(
			getmaterial selection[1]
			)
		)
	)

)


try 
(
	CloseRolloutFloater RsOptimzerTools	
)
catch()
RsOptimzerTools = newRolloutFloater ("Rockstar Optimizer Tools" ) 250 240 50 126 
addRollout RSL_SimpleShader RsOptimzerTools rolledup:false
addRollout RSL_SelectElemetnsBySize RsOptimzerTools rolledup:false

--createDialog RSL_SimpleShader style:#(#style_toolwindow,#style_sysmenu)