filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetClasses.ms")
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_dotNetUIOps.ms")
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_INIOperations.ms")
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_LiveShaderStructs.ms")

global RSL_LiveShader

Struct RSL_LiveShaderImportOps
(
	localConfigINI = (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_LiveShaderImportOps.ini"),
	liveShaderFile = "C:/LiveShaderEditing.txt",
	
	fileTimeStamp,
	liveShaderData,
	backgroundImage,
	
	delegateUpdateTreeView,
	
	liveShaderForm,
-- 	checkFile,
	statusLabel,
	fileCheckTimer,
	propertiesTreeView,
-- 	fileCheckThread,
	forceUpdate,
	updateShaders,
	autoUpdate,
	
	fn readInstanceVariables dataStream =
	(
		local result = #()
		
		skipToString dataStream "Instance Variables\n"
		local line = readLine dataStream
		
		while line != "Material Variables" do
		(
			local filtered = filterString line ":"
			local typeMatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(..)(\w.+)")
			local type = (typeMatch.match filtered[1]).groups.item[2].value
			local value = getFilenameFile (substituteString filtered[filtered.count] "/" "")
			value = substituteString value " " ""
			
			append result (DataPair type:type value:value)
			
			line = readLine dataStream
		)
		
		result
	),
	
	fn readMaterialVariables dataStream =
	(
		local result = #()
		
		local line = readLine dataStream
		
		while line.count > 0 and not (eof dataStream) do
		(
			local filtered = filterString line ":"
			local typeMatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(..)(\w.+)")
			local type = (typeMatch.match filtered[1]).groups.item[2].value
			
			local vFiltered = filterString filtered[2] ", "
			
-- 			for i = 1 to vFiltered.count do
-- 			(
-- 				local stream = vFiltered[i] as stringStream
-- 				skipToString stream "."
-- 				local decimal = readLine stream
-- 				
-- 				if decimal.count > 4 then
-- 				(
-- 					vFiltered[i] = (formattedPrint vFiltered[i] format:".4f")
-- 				)
-- 			)
			
			case vFiltered.count of
			(
				1:(value = vFiltered[1] as float)
				2:(value = [vFiltered[1] as float, vFiltered[2] as float])
				3:(value = [vFiltered[1] as float, vFiltered[2] as float, vFiltered[3] as float])
				4:(value = [vFiltered[1] as float, vFiltered[2] as float, vFiltered[3] as float, vFiltered[4] as float])
			)
			
			append result (DataPair type:type value:value)
			
			line = readLine dataStream
		)
		
		result
	),
	
	fn loadLiveShaderFile file =
	(
		local result
		
		if (doesFileExist file) then
		(
			local dataStream = openfile file
			
			local line = readDelimitedString dataStream ":"
			
			if line == "Object" then
			(
				seek dataStream 0
				
				line = readLine dataStream
				local filtered = filterString line ": "
				local name = filtered[filtered.count]
				local object = getNodeByName name exact:true
				
				if (isValidNode object) then
				(
					result = RSL_LiveShaderObject object:object
					
					skipToString dataStream "Modified: "
					
					while not (eof dataStream) do
					(
						local modified = ((readValue dataStream) == 1)
						
						skipToString dataStream "Shader: "
						
						local shader = readLine dataStream
						local newShader = RSL_ImportedShader shader:shader
						newShader.modified = modified
						newShader.instanceArray += (readInstanceVariables dataStream)
						newShader.variableArray += (readMaterialVariables dataStream)
						
						append result.shaderArray newShader
						
						skipToString dataStream "Modified: "
					)
				)
				else
				(
					updateShaders.enabled = false
					
					statusLabel.text = " Status: Cannot find " + name
					fileTimeStamp = undefined
				)
			)
			else
			(
				updateShaders.enabled = false
				
				statusLabel.text = " Status: LiveShaderEditing.txt is corrupt."
				fileTimeStamp = undefined
			)
			
			close dataStream
			
			result
		)
		else
		(
			updateShaders.enabled = false
			
			statusLabel.text = " Status: LiveShaderEditing.txt is missing."
			fileTimeStamp = undefined
		)
	),
	
	fn updateTreeView =
	(
		propertiesTreeView.nodes.clear()
		liveShaderData.toTreeView propertiesTreeView 
	),
	
	fn convertToImportedShader rageShader =
	(
		local result = RSL_ImportedShader shader:(getFilenameFile (RstGetShaderName rageShader))
		local isAlpha = RstGetIsAlphaShader rageShader
		
		local count = RstGetVariableCount rageShader
		local textureCount = getNumSubTexmaps rageShader
		
		if isAlpha then
		(
			textureCount /= 2
		)
		
		for i = 1 to count do
		(
			local name = (RstGetVariableName rageShader i)
			local variableType = (RstGetVariableType rageShader i)
			local variable = (RstGetVariable rageShader i)
			
			if variableType == "texmap" then
			(
				local alphaTexture = ""
				
				if isAlpha then
				(
					textureCount += 1
					local alphaTexture
					local subTexture = (getSubTexmap rageShader textureCount)
					
					if subTexture != undefined then
					(
						alphaTexture = subTexture.filename
					)
				)
				
				local value = (getFilenameFile (toLower variable)) + (getFilenameFile (toLower alphaTexture))
				
				append result.instanceArray (DataPair type:name value:value)
			)
			else
			(
				local value = variable
				
-- 				local stream = (value as string) as stringStream
-- 				
-- 				if (skipToString stream ".") != undefined then
-- 				(
-- 					local decimal = readLine stream
-- 					
-- 					if decimal.count > 4 then
-- 					(
-- 						value = (formattedPrint stream format:".4f") as float
-- 					)
-- 				)
				
				append result.variableArray (DataPair type:name value:value)
			)
		)
		
		result
	),
	
	fn uniqueIndex liveShaderObject index =
	(
		local result = true
		
		for entry in liveShaderObject.shaderArray do 
		(
			if entry.index == index then
			(
				result = false
			)
		)
		
		result
	),
	
	fn findInMultimaterial material importedShader liveShaderObject =
	(
		local result
		
		for i = 1 to material.materialList.count do 
		(
			local subMaterial = material.materialList[i]
			
			if subMaterial != undefined and (classOf subMaterial) == Rage_Shader then
			(
				local convertedMaterial = convertToImportedShader subMaterial
				local isUniqueIndex = uniqueIndex liveShaderObject i
				
				if (importedShader.compareInstance convertedMaterial) and isUniqueIndex then
				(
					if isUniqueIndex then
					(
						result = i
					)
					else
					(
						format "Duplicate Found!\n"
						(importedShader.toString())
					)
				)
			)
		)
		
		if result == undefined then
		(
			format "Failed to find Material!\n"
			(importedShader.toString())
		)
		
		result
	),
	
	fn findMaterials liveShaderObject =
	(
		local material = liveShaderObject.object.material
		
		case (classOf material) of
		(
			default:
			(
				print (classOf material)
			)
			Rage_Shader:
			(
				if liveShaderObject.shaderArray.count == 1 then
				(
					local importedShader = convertToImportedShader material
					
					if (liveShaderObject.shaderArray[1].compareInstance importedShader) then
					(
						liveShaderObject.shaderArray[1].index = 0
					)
				)
				else
				(
					updateShaders.enabled = false
					
					fileCheckTimer.enabled = false
					messageBox ("The in-game object has more materials than the scene object.\nYou probably need to re-export the object and re-modify the shaders\nObject:" + liveShaderObject.object.name) title:"Error..."
					fileCheckTimer.enabled = true
				)
			)
			Multimaterial:
			(
				for shader in liveShaderObject.shaderArray do
				(
					shader.index = findInMultimaterial material shader liveShaderObject
				)
			)
		)
		
		local foundSome = false
		local missingSome = false
		local modified = false
		
		for entry in liveShaderObject.shaderArray do 
		(
			if entry.index != undefined then
			(
				foundSome = true
			)
			else
			(
				missingSome = true
			)
			
			if entry.modified then
			(
				modified = true
			)
		)
		
		if foundSome then
		(
			if missingSome then
			(
				fileCheckTimer.enabled = false
				messageBox ("Some of the imported shaders cannot be matched to materials on the object.\nObject:" + liveShaderObject.object.name) title:"Error..."
				fileCheckTimer.enabled = true
			)
		)
		else
		(
			updateShaders.enabled = false
			autoUpdate.checked = false
-- 			autoUpdate.BackgroundImage = undefined
-- 			autoUpdate.BackColor = RS_dotNetClass.colourClass.Transparent
			
			fileCheckTimer.enabled = false
			messageBox ("None of the imported shaders can be matched to materials on the object.\nObject:" + liveShaderObject.object.name) title:"Error..."
			fileCheckTimer.enabled = true
		)
		
		if not modified then
		(
			updateShaders.enabled = false
		)
		else
		(
			if not autoUpdate.checked then
			(
				updateShaders.enabled = true
			)
		)
	),
	
	mapped fn updateRageShaderVariables variable material =
	(
		for i = 1 to (RstGetVariableCount material) do 
		(
			local type = RstGetVariableName material i
			
-- 			format "\"%\" : \"%\" = %\n" type variable.type (type == variable.type)
			
			if type == variable.type then
			(
				RstSetVariable material i variable.value
			)
		)
	),
	
	fn updateShadersFromImported =
	(
		for shader in liveShaderData.shaderArray do 
		(
			if shader.modified and shader.index != undefined then
			(
				local material = liveShaderData.object.material
				
				if shader.index > 0 then
				(
					material = liveShaderData.object.material.materialList[shader.index]
				)
				
				updateRageShaderVariables shader.variableArray material
			)
		)
	),
	
	fn getStatus =
	(
		local result = false
		
		if liveShaderData != undefined then
		(
			local foundSome = false
			local modified = false
			
			for entry in liveShaderData.shaderArray do 
			(
				if entry.index != undefined then
				(
					foundSome = true
				)
				
				if entry.modified then
				(
					modified = true
				)
			)
			
			if foundSome and modified then
			(
				result = true
			)
		)
		
		result
	),
	
	
	
	
	
	
	
	fn dispatchOnFormClosing s e =
	(
		RSL_LiveShader.onFormClosing s e
	),
	
	fn onFormClosing s e =
	(
-- 		if fileCheckThread.IsBusy do fileCheckThread.CancelAsync()
		
		RSL_INIOps.writeLocalConfigINIDotNetForm liveShaderForm localConfigINI
		
		gc()
	),
	
-- 	fn dispatchCheckFileChanged s e =
-- 	(
-- 		RSL_LiveShader.checkFileChanged s e
-- 	),
-- 	
-- 	fn checkFileChanged s e =
-- 	(
-- 		if s.checked then
-- 		(
-- 			fileCheckTimer.enabled = true
-- 		)
-- 		else
-- 		(
-- 			fileCheckTimer.enabled = false
-- 		)
-- 	),
	
	fn dispatchPropertiesTreeViewClicked s e =
	(
		RSL_LiveShader.propertiesTreeViewClicked s e
	),
	
	fn propertiesTreeViewClicked s e =
	(
		local hitNode = s.getNodeAt (dotNetObject "System.Drawing.Point" e.x e.y)
		
		if hitNode != undefined then
		(
			s.SelectedNode = hitNode
		)
	),
	
	fn dispatchFileCheckTick s e =
	(
		RSL_LiveShader.fileCheckTick s e
	),
	
	fn fileCheckTick s e =
	(
		if (isProperty s #name) then
		(
			fileTimeStamp = undefined
		)
		
		if (doesFileExist liveShaderFile) then
		(
			if fileTimeStamp == undefined then
			(
				statusLabel.text = " Status: File Loaded " + localTime
				
				fileTimeStamp = getFileModDate liveShaderFile
				
				liveShaderData = loadLiveShaderFile liveShaderFile
				
				if liveShaderData != undefined then
				(
					liveShaderForm.enabled = false
					
					findMaterials liveShaderData
					updateTreeView()
					
					select liveShaderData.object
					
					max zoomext sel 
					
					if autoUpdate.checked then
					(
						updateShadersFromImported()
					)
					
-- 					updateShaders.enabled = false
					
					liveShaderForm.enabled = true
				)
				else
				(
					propertiesTreeView.nodes.clear()
				)
			)
			else
			(
				try
				(
					if (getFileModDate liveShaderFile) != fileTimeStamp then
					(
						statusLabel.text = " Status: File Updated " + localTime
-- 						print "File Update Found!"
						
						liveShaderData = loadLiveShaderFile liveShaderFile
						
						if liveShaderData != undefined then
						(
							liveShaderForm.enabled = false
							
							fileTimeStamp = getFileModDate liveShaderFile
							
							findMaterials liveShaderData
							updateTreeView()
							
							select liveShaderData.object
							
							max zoomext sel 
							
							if autoUpdate.checked then
							(
								updateShadersFromImported()
							)
							
-- 							updateShaders.enabled = false
							
							liveShaderForm.enabled = true
						)
						else
						(
							propertiesTreeView.nodes.clear()
						)
					)
				)
				catch(statusLabel.text = " Status: Failed to access data.")
			)
		)
		else
		(
			statusLabel.text = " Status: File not found."
		)
	),
	
-- 	fn dispatchCheckForUpdate s e =
-- 	(
-- 		RSL_LiveShader.checkForUpdate s e
-- 	),
-- 	
-- 	fn checkForUpdate s e =
-- 	(
-- 		while not fileCheckThread.CancellationPending do
-- 		(
-- 			if (doesFileExist liveShaderFile) then
-- 			(
-- 				if fileTimeStamp == undefined then
-- 				(
-- 					fileTimeStamp = getFileModDate liveShaderFile
-- 					
-- 					RSL_LiveShader.liveShaderData = loadLiveShaderFile liveShaderFile
-- 					
-- 					if liveShaderData != undefined then
-- 					(
-- 						RSL_LiveShader.updateTreeView()
-- 						
-- 						updateShaders.enabled = true
-- 					)
-- 				)
-- 				else
-- 				(
-- 					try
-- 					(
-- 						if (getFileModDate liveShaderFile) != fileTimeStamp then
-- 						(
-- 							print "File Update Found!"
-- 							
-- 							RSL_LiveShader.liveShaderData = loadLiveShaderFile liveShaderFile
-- 							
-- 							if liveShaderData != undefined then
-- 							(
-- 								fileTimeStamp = getFileModDate liveShaderFile
-- 								
-- 								RSL_LiveShader.updateTreeView()
-- 								
-- 								updateShaders.enabled = true
-- 							)
-- 						)
-- 						else
-- 						(
-- 							print "Not Updated..."
-- 						)
-- 					)
-- 					catch()
-- 				)
-- 			)
-- 			
-- 			sleep 1.0
-- 		)
-- 		
-- 		If fileCheckThread.CancellationPending Then
-- 		(
-- 			print "Thread closed"
-- 			e.cancel = true
-- 		)
-- 	),

	fn dispatchUpdateShadersClick s e =
	(
		RSL_LiveShader.updateShadersClick s e
	),
	
	fn updateShadersClick s e =
	(
		updateShadersFromImported()
		
		updateShaders.enabled = false
	),
	
	fn dispatchAutoUpdateClick s e =
	(
		RSL_LiveShader.autoUpdateClick s e
	),
	
	fn autoUpdateClick s e =
	(
		if s.checked then
		(
-- 			autoUpdate.BackColor = RS_dotNetClass.colourClass.fromARGB 100 180 255
-- 			autoUpdate.BackgroundImage = backgroundImage
			updateShadersFromImported()
			
			updateShaders.enabled = false
		)
		else
		(
-- 			autoUpdate.BackgroundImage = undefined
-- 			autoUpdate.BackColor = RS_dotNetClass.colourClass.Transparent
			updateShaders.enabled = getStatus()
		)
	),
	
	fn createForm =
	(
		clearListener()
		
		(DotNetClass "System.Windows.Forms.Application").EnableVisualStyles()
		
		liveShaderForm = RS_dotNetUI.maxForm "liveShaderForm" text:"Live Shader Import V1.0" size:[256,460] min:[256,460] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		liveShaderForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		liveShaderForm.icon = RS_dotNetObject.iconObject (RsConfigGetWildWestDir() + "script/max/Rockstar_London/images/TextureBrowseIconRS.ico")
		dotnet.addEventHandler liveShaderForm "FormClosing" dispatchOnFormClosing
		
-- 		checkFile = RS_dotNetUI.checkBox "checkFile" text:(toUpper "Auto Check") appearance:RS_dotNetPreset.AC_Button flatStyle:RS_dotNetPreset.FS_Flat dockStyle:RS_dotNetPreset.DS_Fill
-- 		checkFile.checked = false
-- 		checkFile.BackColor = RS_dotNetPreset.controlColour_Light
-- 		dotNet.addEventHandler checkFile "CheckedChanged" dispatchCheckFileChanged
		
-- 		fileCheckThread = dotnetobject "CSharpUtilities.SynchronizingBackgroundWorker"
-- 		fileCheckThread.WorkerSupportsCancellation = true
-- 		dotNet.addEventHandler fileCheckThread "DoWork" dispatchCheckForUpdate
		
		statusLabel = RS_dotNetUI.Label "statusLabel" text:" Status:..." \
												textAlign:RS_dotNetPreset.CA_MiddleLeft borderStyle:RS_dotNetPreset.BS_None \
												dockStyle:RS_dotNetPreset.DS_Fill 
		statusLabel.Font = RS_dotNetPreset.FontMedium
		
		fileCheckTimer = dotNetObject "Timer"
		fileCheckTimer.interval = 1000
		fileCheckTimer.enabled = true
		dotNet.addEventHandler fileCheckTimer "Tick" dispatchFileCheckTick
		
		propertiesTreeView = RS_dotNetUI.TreeView "dictionaryTreeView" dockStyle:RS_dotNetPreset.DS_Fill
		propertiesTreeView.borderStyle = RS_dotNetPreset.BS_None
		propertiesTreeView.BackColor = RS_dotNetPreset.controlColour_Medium
		propertiesTreeView.Sorted = false
		propertiesTreeView.HideSelection = false
		propertiesTreeView.Font = RS_dotNetPreset.FontMedium
		dotnet.addEventHandler propertiesTreeView "Click" dispatchPropertiesTreeViewClicked
		
		forceUpdate = RS_dotNetUI.Button "forceUpdate" text:(toUpper "Force File Update") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler forceUpdate "click" dispatchFileCheckTick
		
		updateShaders = RS_dotNetUI.Button "updateShaders" text:(toUpper "Update Shaders") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler updateShaders "click" dispatchUpdateShadersClick
		
		backgroundImage = RS_dotNetObject.imageObject (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\images\\autoBackground.png")
		
		autoUpdate = RS_dotNetUI.checkBox "autoUpdate" text:(toUpper "Auto") appearance:RS_dotNetPreset.AC_Button dockStyle:RS_dotNetPreset.DS_Fill --flatStyle:RS_dotNetPreset.FS_Flat
		autoUpdate.checked = false
		autoUpdate.BackColor = RS_dotNetClass.colourClass.Transparent
		autoUpdate.BackgroundImageLayout = (dotNetClass "System.Windows.Forms.ImageLayout").Stretch
		dotnet.addEventHandler autoUpdate "click" dispatchAutoUpdateClick
		
		mainTable = RS_dotNetUI.tableLayout "viewTable" text:"mainTable" collumns:#((dataPair type:"Percent" value:75), (dataPair type:"Percent" value:25)) rows:#((dataPair type:"Absolute" value:24), (dataPair type:"Percent" value:50), (dataPair type:"Absolute" value:24), (dataPair type:"Absolute" value:24)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		mainTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		mainTable.controls.add statusLabel 0 0
		mainTable.controls.add propertiesTreeView 0 1
		mainTable.controls.add forceUpdate 0 2
		mainTable.controls.add updateShaders 0 3
		mainTable.controls.add autoUpdate 1 3
		
		mainTable.SetColumnSpan statusLabel 2
		mainTable.SetColumnSpan propertiesTreeView 2
		mainTable.SetColumnSpan forceUpdate 2
		
		liveShaderForm.controls.add mainTable
		
		RSL_INIOps.readLocalConfigINIDotNetForm liveShaderForm localConfigINI
		
-- 		fileCheckThread.RunWorkerAsync()
		
		liveShaderForm.showModeless()
	)
)

if RSL_LiveShader != undefined then
(
	RSL_LiveShader.liveShaderForm.close()
)

RSL_LiveShader = RSL_LiveShaderImportOps()
RSL_LiveShader.createForm()
-- RSL_LiveShader.loadLiveShaderFile "C:/LiveShaderEditing.txt"