-- Find Prop Instaces --

-----------------------------------------------------
-- .NET Reference --
-----------------------------------------------------

/*
  .Attributes : <System.Xml.XmlAttributeCollection>, read-only
  .BaseURI : <System.String>, read-only
  .ChildNodes : <System.Xml.XmlNodeList>, read-only
  .FirstChild : <System.Xml.XmlNode>, read-only
  .HasAttributes : <System.Boolean>, read-only
  .HasChildNodes : <System.Boolean>, read-only
  .InnerText : <System.String>
  .InnerXml : <System.String>
  .IsEmpty : <System.Boolean>
  .IsReadOnly : <System.Boolean>, read-only
  .Item[<System.String>name] : <System.Xml.XmlElement>, read-only
  .Item[<System.String>localname, <System.String>ns] : <System.Xml.XmlElement>, read-only
  .LastChild : <System.Xml.XmlNode>, read-only
  .LocalName : <System.String>, read-only
  .Name : <System.String>, read-only
  .NamespaceURI : <System.String>, read-only
  .NextSibling : <System.Xml.XmlNode>, read-only
  .NodeType : <System.Xml.XmlNodeType>, read-only
  .OuterXml : <System.String>, read-only
  .OwnerDocument : <System.Xml.XmlDocument>, read-only
  .ParentNode : <System.Xml.XmlNode>, read-only
  .Prefix : <System.String>
  .PreviousSibling : <System.Xml.XmlNode>, read-only
  .SchemaInfo : <System.Xml.Schema.IXmlSchemaInfo>, read-only
  .Value : <System.String>
*/

-- Best to extract and read xml files

filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/pipeline/RSL_LevelConfig.ms")
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_ImageUtils.ms")
filein "X:/tools/dcc/current/max2009/scripts/pipeline/util/xml.ms"

try CloseRolloutFloater RsObjectFinder catch () 


rollout RsSearchName "Find My Object!  v0.1"
(
	
	editText edt2 "Obect Name" text:"P_dr_secu_Aa_L_H" pos:[14,17] width:303 height:19
	listBox lbx1 "Results" pos:[18,43] width:395 height:9
	button btn_Search "Search" pos:[332,20] width:80 height:18
	button btn_load "Load Max file" pos:[332,190] width:80 height:18
	
	
	MultiListBox  lbx_levels "Select Levels to search In" pos:[15,224]
	checkbox chk_single "Single" across:5 checked:true
	checkbox chk_versus "Versus"
	checkbox chk_coop "Coop"
	button btn_selectall "Select All"
	button btn_selectnone "Select None"
	
	groupBox group1 "Filter Search" pos:[5,208] width:430 height:200

	
	
	-- Used to make sure we have no white spaces in the string
	fn validatestring stringname=
	(
		validstring = filterString stringname " " 
		
		validstring[1]
	)
	
	-- Simple functiom to update the listbox with a new item
	fn updateListbox Newpathname=
	(
		
		currentlist = lbx1.items		
		appendifunique currentlist Newpathname
		lbx1.items = currentlist
	)
	
	
	-- Recursive find fucntion for a passed in object name
	fn recursiveXML Nodename ObjectName xmlfile= (

		if (Nodename.HasChildNodes) then
		(
		
		for i = 0 to Nodename.ChildNodes.Count - 1 do
				(
					local childNode = Nodename.ChildNodes.Item[i]
					if (childNode.name) == "object" then 
					(
						if (childNode.HasAttributes) then
						(
							local class = (childNode.Attributes.ItemOf "class").value
							if (class == "xref_object" ) then
							(	
								local refName= (childNode.Attributes.ItemOf "refname").value
								--print refName
								if (tolower ObjectName  == tolower refName) then
								(
									updateListbox xmlfile
								)
							)
							
						)
						
						if (childNode.HasChildNodes) then
						(
							recursiveXML childNode ObjectName xmlfile
						)
					)
					
					if (childNode.name)  == "children" then  
					(
						recursiveXML childNode ObjectName xmlfile
					)
				)
		)
		
		
	)
	
	
	-- Fn to fix path names so that they are valid names for loading
	
	fn validate xmlfilename =
	(
		local temparray = filterString xmlfilename "\\/"
		local index = finditem temparray "payne_art" -- Harcoded for now
		newstring = xmlfilename
		if index != 0 then
		(
			newstring = "x:/payne"
			for n = index to temparray.count do
			(
				newstring+=("/"+temparray[n])
			)
		)
		newstring
		
	)
	
	
	
	-- FN Intal load XML function. This is in a try catch as some xmls have bianry at teh end
	fn readXML xmlpath ObjectName =	(
		
		try(
		local xmlDoc = XmlDocument()
		xmlDoc.init()
		xmlDoc.load xmlpath

		xmlRoot = xmlDoc.document.DocumentElement
		local xmlfilename = (xmlRoot.Attributes.ItemOf "filename").value
		
		xmlfilename = validate xmlfilename
		
			
		if xmlRoot != undefined then
		(
			objectsElement = RsGetXmlElement xmlRoot "objects"
			recursiveXML objectsElement ObjectName xmlfilename

		)

		)
		catch(
			print ( "Can not load XML, prpananly a prop file: "+xmlpath)
			)
		)
		
		
	-- MAke out filename all forward slashes
	fn makesafefilename filename =
	(
		temp = filterString filename "\\/"
		newstring = ""
		for s in temp do
		(
			newstring+=(s+"/")
		)
		newstring
	)
	
	-- Gather the level names based on teh checkboxs/type of levels we have		
	fn getLevelNames = (
		foundDirs = getDirectories (project.independent+"/levels/*")
		dirlist = #()
		-- Make Safe Path Names
		for dir in foundDirs do
		(
			
			local mapName = pathConfig.stripPathToLeaf dir
			
			if matchPattern mapName pattern:"s_*" and chk_single.checked do
			(
				append dirlist (makesafefilename mapName)	
			)
			if matchPattern mapName pattern:"c_*" and chk_coop.checked do
			(
				append dirlist (makesafefilename mapName)	
			)
			if matchPattern mapName pattern:"v_*" and chk_versus.checked do
			(
				append dirlist (makesafefilename mapName)	
			)
		
		)
		dirlist
		
	)
	


	-- Process each extracted xml file 
	fn processXMLS levelpath = (
		--print levelpath
		files = getFiles (levelpath+"\*.xml")
		if files.count > 0 then
		(
			for eachfile in files do
			(
				propname = validatestring (edt2.text)
				print propname
				validref = (readXML eachfile propname)
				if validref != "" then
				(
					print "-------------------------------------------------------------"
					print ("found file at :"+ eachfile)
					print "-------------------------------------------------------------"
				)
			)
		)
	)
	
	-- Extract the xml from the rpf. Uses ragebuikder scritps
	fn extractlevelXMLs levelpath = (		
		
		inputPath =  levelpath
		outputPath = "c:/temp_xml/"+(pathConfig.stripPathToLeaf levelpath)
		makeDir outputPath
		deleteFile  (outputPath+"\*.xml")
		
		args =(" X:\\tools\\bin\script\\ExtractLevelXML.rbs -input "+inputPath+" -output "+outputPath) 
		cmd = "X:\\tools\\bin\\ragebuilder_0327.exe"
		--print (cmd+args)
		HiddenDOSCommand (cmd+args) startpath:"c:\\"
		
		processXMLS outputPath

	)
	
	-- Get the list of selected levels to seach trhough. ALso rebuilds the full paths
	fn getlistLevelNames =
	(
		levelslist = #()
		if (lbx_levels.selection != 0) then
		(
			for sel in (lbx_levels.selection as array) do(
				append levelslist (makesafefilename (project.independent+"/levels/"+lbx_levels.items[sel]))
			)
		)
		levelslist
	)


	

	
	------------------------------------------------------------------------------------------------------------------------------
	-- 												EVENT HANDLERS 													--
	------------------------------------------------------------------------------------------------------------------------------
	
	on RsSearchName open do
	(
		lbx_levels.items = getLevelNames()
	)
	
	on chk_coop changed theState do
	(
		lbx_levels.items = getLevelNames()
	)
	on chk_versus changed theState do
	(
		lbx_levels.items = getLevelNames()
	)
	on chk_single changed theState do
	(
		lbx_levels.items = getLevelNames()
	)
	
	on btn_selectall pressed do
	(
		selall = #()
		for i = 0 to lbx_levels.items.count do
		(
			append selall i
		)			
		
		lbx_levels.selection=selall
		

	)
	
	on btn_selectnone pressed do
	(
		--lbx_levels.selection
		lbx_levels.selection = #()

	)
	
	
	on btn_Search pressed do
	(	
		lbx1.items = #()
		
		if edt2.text != "" then
		(
			

			
		levelnames = getlistLevelNames()

		if (levelnames.count) > 0 then
		(
			
			for levelpath in levelnames do
			(
				extractlevelXMLs levelpath
			)
			gc()	
		)		
		)

	)
	
	on btn_load pressed do
	(
		if (lbx1.selection !=0)then	(

			loadMaxFile (lbx1.items[lbx1.selection]) quiet:true	
			
			
		)
	)
	
)

RsObjectFinder= newRolloutFloater ("Object Finder v0.1" ) 450 440 50 126
addRollout RsSearchName RsObjectFinder rolledup:false