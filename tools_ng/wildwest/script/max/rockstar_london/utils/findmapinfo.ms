
fn findMapByFile rageShader file =
(
	local result = undefined
	local count = getNumSubTexmaps rageShader
	
	if (classOf file) == string then
	(
		for i = 1 to count do
		(
			local map = (getSubTexmap rageShader i)
			--print map.filename
			if (classOf map) == bitmapTexture then
			(
				if map != undefined and map.filename != undefined then
				(
					
					map.filename = substituteString map.filename rsConfig.toolsroot "."
					
					if map.filename == (substituteString file rsConfig.toolsroot ".") then result = map
				)
			)
		)
	)
	result
)

mapped fn findMaterialsInDependents entry &materialArray =
(
	local class = (classOf entry)
	if class == standardMaterial or class == Rage_shader or class == DirectX_9_Shader then
	(
		append materialArray entry
	)
)


fn animatedUVs map = 
(
	local result = false
	
	local uAnim = map.coords.U_Offset.controller
	local vAnim = map.coords.V_Offset.controller
	
	if uAnim != undefined or vAnim != undefined then
	(
		result = true
	)
	
	result
)

fn instanceMap old new =
(
	local materialArray = #()
	
	findMaterialsInDependents (refs.dependents old) &materialArray
	
	for material in materialArray do
	(
		local count = getNumSubTexmaps material
		
		for i = 1 to count do
		(
			local map = getSubTexmap material i
			
			if map == old then
			(
				setSubTexmap material i new
			)
		)
	)
)

fn findInStandardMaterial material map =
(
	local result = 0
	local index = findItem material.maps map
	
	case index of
	(
		default:print index
		0:
		(
			local normalTexture = material.bumpMap
			if (classOf normalTexture) == Normal_Bump then normalTexture = material.bumpMap.normal_map
			
			if normalTexture != undefined and normalTexture == map then
			(
				/* BumpMap */
				result = 2
			)
			else
			(
				
			)
		)
		2:
		(
			/* DiffuseMap */
			result = 1
		)
		3:
		(
			result = 3
		)
		4:
		(
			result = 3
		)
		7:
		(
			result = 4
		)
		10:
		(
			result = 5
		)
	)
	result
)

fn findInRageShader rageShader map =
(
	local result = 0
	
	local count = RstGetVariableCount rageShader
	local isAlphaShader = RstGetIsAlphaShader rageShader
	
	local countArray = for i = 1 to count where (matchPattern (RstGetVariableType rageShader i) pattern:"texmap") collect i
	
	local alphaIndex = 0
	
	for i = 1 to countArray.count do
	(
		local index = countArray[i]
		
		local name = (RstGetVariableName rageShader index)
		local variable = (RstGetVariable rageShader index)
		local currentMap = (getSubTexmap rageShader i)
		
		if (classOf currentMap) == compositeTextureMap then
		(
			local index = findItem currentMap.mapList map
			
			if index != 0 then currentMap = currentMap.mapList[index]
		)
		
		if name == "Diffuse Texture" then
		(
			alphaIndex = (countArray.count + i)
		)
		
		if currentMap == map then
		(
			case name of
			(
				"Diffuse Texture":result = 1
				"Diffuse 1":result = 1
				"Diffuse 2":result = 1
				"Diffuse 3":result = 1
				"Diffuse 4":result = 1
				"Diffuse 5":result = 1
				"Diffuse 6":result = 1
				"Diffuse 7":result = 1
				"Diffuse 8":result = 1
				"Bump Texture":result = 2
				"Bump 1":result = 2
				"Bump 2":result = 2
				"Bump 3":result = 2
				"Bump 4":result = 2
				"Bump 5":result = 2
				"Bump 6":result = 2
				"Bump 7":result = 2
				"Bump 8":result = 2
				"Specular Texture":result = 3
				"Environment Texture":result = 5
			)
		)
	)
	
	if result == 0 and isAlphaShader then
	(
		local alphaMap = (getSubTexmap rageShader alphaIndex)
		
		if alphaMap != undefined and alphaMap == map then result = 4
	)
	result
)

fn findInDX9Material DX9Shader map =
(
	local result = 0
	local shader = (getFilenameFile DX9Shader.effectFilename)
-- 	local count = DX9Shader.numberofbitmaps()
	
	case shader of
	(
		default:
		(
			format "UNSUPPORTED DIRECTX_9 SHADER: % in: %\n" shader DX9Shader.name
			illegalMaterials = true
		)
		"terrain_cb_4lyr":result = findInRageShader DX9Shader.renderMaterial map
		"terrain_cb_8lyr":result = findInRageShader DX9Shader.renderMaterial map
	)
	result
)

fn appendMapIfUnique map index =
(
	if map.filename != undefined then
	(
		local unique = true
		local animated = animatedUVs map
		
		if not animated then
		(
			for i = 1 to textureArray[index].count do
			(
				local entry = textureArray[index][i]
				local match = matchPattern entry.filename pattern:map.filename
				
				if match then
				(
					unique = false
					instanceMap entry map
				)
			)
		)
		
		if unique then
		(
			append textureArray[index] map
		)
	)
)

mapped fn assignToArray material map =
(
	case (classOf material) of
	(
		default:
		(
-- 			print material
			print (classOf material)
		)
		standardMaterial:
		(
			local index = findInStandardMaterial material map
			
			if index == 0 then
			(
				format "-=-=-=-= Map not found in Standard Material =-=-=-=-\n" 
				format "Map:%\n" map.filename
				format "Standard Material:%\n\n" material
			)
			
			appendMapIfUnique map index
		)
		Rage_shader:
		(
			local index = findInRageShader material map
			
			if index == 0 then
			(
				format "-=-=-=-= Map not found in Rage shader =-=-=-=-\n" 
				format "Map:%\n" map.filename
				format "Rage Shader:%\n\n" material
				
-- 				meditMaterials[1] = map
-- 				meditMaterials[2] = material
			)
			
			appendMapIfUnique map index
		)
		DirectX_9_Shader:
		(
			local index = findInDX9Material material map
			
			appendMapIfUnique map index
		)
	)
)

mapped fn createTextureArray map =
(
	if map.filename != undefined then
	(
		map.filename = (substituteString map.filename ".\\" (rsConfig.toolsroot + "\\"))
		
		local materialArray = #()
		
		findMaterialsInDependents (refs.dependents map) &materialArray
		
		assignToArray materialArray map
	)
)

textureArray = #(#(),#(),#(),#(),#())

start = timeStamp()
createTextureArray (getClassInstances bitmapTexture)
end = timeStamp()

format "Processing took % seconds\n" ((end - start) / 1000.0)
textureArray
