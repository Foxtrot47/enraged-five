------------------------------------------------------------------------------------------------------------------------------------------------------
-- RSL_FFDConverter
-- By Paul Truss
-- Senior Technical Artist
-- Rockstar London
-- 12/10/2010
--
-- Converts FFD control points to bones and then skins the object to the bones.
------------------------------------------------------------------------------------------------------------------------------------------------------

struct RSL_FFDBone
(
	bone, -- must be a valid node
	cpArray -- array of FFD control point names, names not strings
)

try (DestroyDialog RSL_FFDConverter) catch()

rollout RSL_FFDConverter "FFD Converter" width:240
(
	------------------------------------------------------------------------------------------------------------------------------------------------------
	--
	-- Local Variables
	--
	------------------------------------------------------------------------------------------------------------------------------------------------------
	local ffdClassArray = #(FFD2x2x2, FFD3x3x3, FFD4x4x4, FFDBox, FFDCyl) -- FFD classes used to check the top modifier
	local fallOffTypeArray = #("Linear", "Sinual", "Fast Out", "Slow Out") -- SkinOps bone falloff types
	
	-- Attribute indices used to set attributes on the object and bones
	local idxIsDynamic = getAttrIndex "Gta Object" "Is Dynamic"
	local idxHasAnim = getAttrIndex "Gta Object" "Has Anim"
	local idxDontExport = getAttrIndex "Gta Object" "Dont Export"
	local idxDontAdd = getAttrIndex "Gta Object" "Dont Add To IPL"
	
	------------------------------------------------------------------------------------------------------------------------------------------------------
	--
	-- Rollout Controls
	--
	------------------------------------------------------------------------------------------------------------------------------------------------------
	group "Bone Creation"
	(
		spinner spn_boneSize "Bone Size %" fieldwidth:80 range:[0.01,1.0, 0.15] scale:0.01 offset:[2,0]
		spinner spn_boneDist "Bone Distance Threshold" fieldwidth:80 range:[0.0,100.0, 1.0] scale:0.01 offset:[2,0]
	)
	
	group "Skin Options"
	(
		dropdownList ddl_falloff "Bone Falloff" width:218 items:fallOffTypeArray
	)
	
	button btn_convert "Convert Selected" width:230
	
	------------------------------------------------------------------------------------------------------------------------------------------------------
	--
	-- Functions
	--
	------------------------------------------------------------------------------------------------------------------------------------------------------
	
	------------------------------------------------------------------------------------------------------------------------------------------------------
	-- getFFDControlPoints() gets all the control points names via getPropNames as this is the only way to access FFD control points
	------------------------------------------------------------------------------------------------------------------------------------------------------
	fn getFFDControlPoints object =
	(
		local result = #()
		
		-- get the property names for every property in the FFD modifier
		local propNames = getPropNames object.modifiers[1] #dynamicOnly
		
		-- Loop through propNames and match the control point properties
		for entry in propNames do 
		(
			if matchPattern (entry as string) pattern:"Control_Point*" then
			(
				append result entry
			)
		)
		
		result
	)
	
	------------------------------------------------------------------------------------------------------------------------------------------------------
	-- getInitialPosition() gets the initial position of the supplied control point
	-- controlPoint is a name e.g. #Control_Point_1. The returned position is generally in a [0,0,0] to [1,1,1] range though it can be less than
	-- 0 or greater than 1 if the point is outside the bounds of the modifier volume. The modifier volume is always the same and is generated
	-- when the modifier is applied to the object. You get the minimum and maximum bounds via getModContextBBoxMin and getModContextBBoxMax
	------------------------------------------------------------------------------------------------------------------------------------------------------
	fn getInitialPosition object controlPoint =
	(
		-- Make sure we're getting positional information from the start of the animation
		at time animationRange.start
		(
			-- Get the min and the max bounds for modifier volume
			local bbMin = (getModContextBBoxMin object object.modifiers[1])
			local bbMax = (getModContextBBoxMax object object.modifiers[1])
			-- Caculate the size of the bounds
			local bound = bbMax - bbMin
			-- Get the volume position of the control point inside the modifier volume
			local latticePos = getProperty object.modifiers[1] controlPoint
			-- Caculate the position
			bbMin + (bound * latticePos)
		)
	)
	
	------------------------------------------------------------------------------------------------------------------------------------------------------
	-- getTransform() gets the transform for the supplied control point.
	-- This works in a similar fashion to getInitialPosition though it returns a matrix3 value. The result matrix is multiplied by the object
	-- transform to ensure that the final transform is an amalgamation of the local control point position and the object transform so that
	-- if the object is moved of rotated, the final transform is correct.
	------------------------------------------------------------------------------------------------------------------------------------------------------
	fn getTransform controlPoint object t =
	(
		local result = Matrix3 1
		
		-- Make sure we're getting positional information from the correct time using variable t
		at time t
		(
			-- Get the min and the max bounds for modifier volume
			local bbMin = (getModContextBBoxMin object object.modifiers[1])
			local bbMax = (getModContextBBoxMax object object.modifiers[1])
			-- Caculate the size of the bounds
			local bound = bbMax - bbMin
			-- Get the volume position of the control point inside the modifier volume
			local latticePos = getProperty object.modifiers[1] controlPoint
			-- Caculate the position
			local controlPos = bbMin + (bound * latticePos)
			-- Assign the position to the matrix
			result.row4 = controlPos
			-- Multiply the result matrix by the object transform
			result *= object.transform
		)
		
		result
	)
	
	------------------------------------------------------------------------------------------------------------------------------------------------------
	-- findControlPoint() gets the index in the array variable of the FFDBone struct that contains the control point cp variable.
	-- If a control point is already assigned to a FFDBone struct we do not create a bone for it because it will be merged with the other
	-- control points in the array to create the final position for the bone.
	------------------------------------------------------------------------------------------------------------------------------------------------------
	fn findControlPoint cp array =
	(
		local result = 0
		-- Loop through the array to find the cp in the array[i].cpArray
		for i = 1 to array.count do
		(
			if (findItem array[i].cpArray cp) > 0 then
			(
				result = i
			)
		)
		
		result
	)
	
	------------------------------------------------------------------------------------------------------------------------------------------------------
	-- averageMatrix() gets the average transform for all the control points in the cpArray variable. Depending on the Bone Distance Threshold,
	-- control points will get merged to reduce the amount of bones that get created so this function makes sure we have a single transform
	-- based on all the merged control points in the FFDBone struct. The t variable is a time variable that gets supplied to getTransform so
	-- that it gets the transform at the required time.
	------------------------------------------------------------------------------------------------------------------------------------------------------
	fn averageMatrix cpArray object t =
	(
		local row1 = [0,0,0]
		local row2 = [0,0,0]
		local row3 = [0,0,0]
		local row4 = [0,0,0]
		-- Collect a transform for each control point in the cpArray variable
		local matrixArray = for cp in cpArray collect (getTransform cp object t)
		-- Loop through the matrixArray adding up the matrix rows
		for i = 1 to matrixArray.count do 
		(
			row1 += matrixArray[i][1]
			row2 += matrixArray[i][2]
			row3 += matrixArray[i][3]
			row4 += matrixArray[i][4]
		)
		-- normalize each row after we divide by the matrixArray.count
		row1 = normalize (row1 / matrixArray.count)
		row2 = normalize (row2 / matrixArray.count)
		row3 = normalize (row3 / matrixArray.count)
		row4 = (row4 / matrixArray.count)
		
		matrix3 row1 row2 row3 row4
	)
	
	------------------------------------------------------------------------------------------------------------------------------------------------------
	-- createBones() creates FFDBone structs based on the supplied control points in the pointArray variable.
	-- The control points get merged together if the distance between them is within the Bone Distance Threshold.
	-- A FFDBone struct is created for each group of control points.
	------------------------------------------------------------------------------------------------------------------------------------------------------
	fn createBones pointArray object =
	(
		local result = #()
		-- Caculate the bone size based on the length between the minimum and maximum bounds of the object * the spn_boneSize.value
		local boneSize = (length (object.max - object.min)) * spn_boneSize.value
		
		progressStart "Creating Bones..."
		-- Loop though the pointArray creating FFDBone structs and bone objects when needed
		for i = 1 to pointArray.count - 1 where (findControlPoint pointArray[i] result) == 0 do 
		(
			local FFDBone = RSL_FFDBone cpArray:#(pointArray[i])
			local cp1 = getInitialPosition object pointArray[i]
			
			for s = (i + 1) to pointArray.count where (findControlPoint pointArray[s] result) == 0 do 
			(
				local cp2 = getInitialPosition object pointArray[s]
				-- Check to see if the control points are closer than the spn_boneDist.value. If they are, group them into one bone
				if (distance cp1 cp2) <= spn_boneDist.value then
				(
					append FFDBone.cpArray pointArray[s]
				)
			)
			
			append result FFDBone
			
			progressUpdate (i as float / pointArray.count * 100.0)
		)
		
		progressEnd()
		
		result
	)
	
	------------------------------------------------------------------------------------------------------------------------------------------------------
	-- convertToSkinned() starts the whole process of creating bones from the control points. It then animates the created bones
	-- and then sets up the skinning. The FFD modifier is only disabled so that if something goes wrong, you can delete the skin modifier, the 
	-- bones and restart the process.
	------------------------------------------------------------------------------------------------------------------------------------------------------
	fn convertToSkinned object =
	(
		animateAll object.modifiers[1] -- force all the control points to be animated so that when we getFFDControlPoints, we get all of then even if they are not atually animated
		local controlPointArray = getFFDControlPoints object
		
		local boneArray = (createBones controlPointArray object) -- create the bones
		
		-- the bone count must be less than 128 because thats the limit for a single skinned object
		if boneArray.count < 128 then
		(
			-- Set the command panel to something other than the modify panel to ensure we don't get any modify updates whilst we're creating and animating bone objects
			SetCommandPanelTaskMode #display
			
			-- Caculate the bone size based on the length between the minimum and maximum bounds of the object * the spn_boneSize.value
			local boneSize = (length (object.max - object.min)) * spn_boneSize.value
			
			-- Create the root bone at the object position
			local rootName = uniqueName (object.name + "_RootBone_")
			local rootBone = sphere radius:boneSize pos:object.pos segments:4 name:rootName wireColor:red
			convertToMesh rootBone
			
			-- Set attributes for the root bone
			setAttr rootBone idxHasAnim true
			setAttr rootBone idxDontExport true
			setAttr rootBone idxDontAdd true
			
			-- Create the bones and set the root bone as the parent for all the bones
			for entry in boneArray do 
			(
				local boneName = uniqueName (object.name + "_FFDBone_")
				
				local newBone = sphere radius:boneSize segments:4 name:boneName wireColor:green
				newBone.transform = averageMatrix entry.cpArray object animationRange.start 
				newBone.xray  = true
				convertToMesh newBone
				
				-- Set attributes for the bone
				setAttr newBone idxHasAnim true
				setAttr newBone idxDontExport true
				setAttr newBone idxDontAdd true
				
				-- Assign the bone to the struct
				entry.bone = newBone
				
				entry.bone.parent = rootBone
			)
			
			-- set the time slider to the start of the animtion, not actually required but just to be safe
			sliderTime = animationRange.start
			
			progressStart "Animating Bones..."
			
			-- get the total number of frames, use to calculate the progress
			local count = (animationRange.end - animationRange.start).frame as integer
			
			-- loop through the frames and animate the bones
			for t = animationRange.start to animationRange.end do 
			(
				with animate on
				(
					for i = 1 to boneArray.count do 
					(
						local FFDBone = boneArray[i]
						
						-- Rather than setting the time slider, set the transform at the required time. This stops any screen update from happening
						at time t
						(
							FFDBone.bone.transform = averageMatrix FFDBone.cpArray object t
						)
					)
				)
				
				progressUpdate (t.frame as float / count * 100.0)
			)
			
			progressEnd()
			
			-- Disable the FFD modifier, not delete just incase something goes wrong
			object.modifiers[1].enabled = false
			
			-- Create the skin modifer and apply it to the object
			local skinMod = Skin()
			addModifier object skinMod
			
			-- Make sure we're on the modify panel because we have to be to use the skinOps struct to add bones and edit them
			SetCommandPanelTaskMode #Modify
			
			-- Add the root bone to the skin modifier and make the envelopes are tiny so that they do not affect the object
			skinOps.AddBone skinMod rootBone 0
			
			local cCount = skinOps.getNumberCrossSections skinMod 1
			for i = 1 to cCount do 
			(
				skinOps.SetInnerRadius skinMod 1 i 0.0
				skinOps.SetOuterRadius skinMod 1 i 0.0
			)
			
			-- Loop through all the bones adding and setting them up
			for i = 1 to boneArray.count do 
			(
				-- If i is less than the number of bones we can add a bone without updating the object.
				-- When the last bone is added, we also update the object
				if i < boneArray.count then
				(
					skinOps.AddBone skinMod boneArray[i].bone 0
				)
				else
				(
					skinOps.AddBone skinMod boneArray[i].bone 1
				)
				
				-- Set the fallof based on ddl_falloff and set the start and end points so that they are very close. This makes the bone envelopes spherical in shape.
				-- (i + 1) because we added the root bone first so the index for the added bone will be 1 more than i
				skinOps.setBonePropFalloff skinMod (i + 1) ddl_falloff.selection
				skinOps.SetStartPoint skinMod (i + 1) [-0.01,0,0]
				skinOps.SetEndPoint skinMod (i + 1) [0.01,0,0]
			)
			
			-- Set the attributes required for the object to export with skinned animation
			setAttr object idxHasAnim true
-- 			setAttr object idxIsDynamic true
		)
		else
		(
			messageBox ("There are too many bones :" + boneArray.count as string + "\nReduce the FFD control point count and/or increase the bone distance threshold.") title:"Error..."
			
			-- Make the boneArray undefined to get rid of the data
			boneArray = undefined
		)
	)
	
	------------------------------------------------------------------------------------------------------------------------------------------------------
	--
	-- Event Handlers
	--
	------------------------------------------------------------------------------------------------------------------------------------------------------
	on btn_convert pressed do 
	(
		if selection.count > 0 then
		(
			if selection.count == 1 then
			(
				local theObject = selection[1]
				-- Check that the top modifier is an FFD modifer. cIndex will be greater than 0 if it is
				cIndex = findItem ffdClassArray (classOf theObject.modifiers[1])
				
				if cIndex > 0 then
				(
					convertToSkinned theObject
				)
				else
				(
					messageBox "The top modifier is not an FFD modifier. Is there an FFD modifier present?" title:"Error..."
				)
			)
			else
			(
				messageBox "You must select only one object." title:"Error..."
			)
		)
		else
		(
			messageBox "You must select an object and only one object." title:"Error..."
		)
	)
	
)

CreateDialog RSL_FFDConverter style:#(#style_toolwindow, #style_sysmenu)