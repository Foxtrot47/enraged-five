
try (destroyDialog RSL_MM_ChannelSwap) catch()

struct RSL_MMObject
(
	node,
	mmNode,
	faceArray = #()
)

rollout RSL_MM_ChannelSwap "Micro-movement channel swap" width:190
(
	local microArray = #("micromovement", "trees") --#("micromovement", "trees")
	local objectArray = #()
	local count = 0
	local totalCount = 0
	
	button btn_selectMM "Select Micro-movment Objects" width:180
	button btn_convertSelected "Convert Selected" width:180 --enabled:false
	button btn_convertAll "Convert All" width:180
-- 	checkBox chkb_reset "Reset Vertex Colours"
	
	fn getFacesByID object ID =
	(
		local result = #()
		
		for face in object.faces do
		(
			if (polyOp.getFaceMatID object face.index) == ID then
			(
				append result face.index
			)
		)
		
		result
	)
	
	fn findInMcroArray name =
	(
		local result = false
		
		for entry in microArray do
		(
			if (MatchPattern name pattern:("*" + entry + "*")) then
			(
				result = true
			)
		)
		
		result
	)
	
	fn getMicroMovement object =
	(
		local result
		
		case (classOf object.material) of
		(
			Rage_Shader:
			(
				if (findInMcroArray (RstGetShaderName object.material)) then
				(
					if (classOf object) != Editable_Poly then
					(
						convertToPoly object
					)
					
					result = RSL_MMObject node:object faceArray:(for face in object.faces collect face.index)
				)
			)
			Multimaterial:
			(
				for i = 1 to object.material.materialList.count do
				(
					local subMaterial = object.material.materialList[i]
					
					if (classOf subMaterial) == Rage_Shader then
					(
						local ID = object.material.materialIDList[i]
						
						if (findInMcroArray (RstGetShaderName subMaterial)) then
						(
							if (classOf object) != Editable_Poly then
							(
								convertToPoly object
							)
							
							local faceArray = getFacesByID object ID
							
							if faceArray.count > 0 then
							(
								if result == undefined then
								(
									result = RSL_MMObject node:object faceArray:faceArray
								)
								else
								(
									result.faceArray += faceArray
								)
							)
						)
					)
				)
			)
		)
		
		result
	)
	
	mapped fn convertMicroMovement mmObject =
	(
		if (polyop.getMapSupport mmObject.Node 0) then
		(
-- 			convertToPoly mmObject.Node
			local name = uniqueName (mmObject.node.name + "_MMObject")
			polyOp.detachFaces mmObject.node mmObject.faceArray delete:true asNode:true name:name
			
			mmObject.mmNode = getNodeByName name exact:true
			
			polyop.setMapSupport mmObject.Node 11 false
			
			polyop.setMapSupport mmObject.mmNode 11 true
			polyop.setMapSupport mmObject.Node 11 true
			
			polyop.defaultMapFaces mmObject.Node 11
			
			local mapVertCount = polyop.getNumMapVerts mmObject.mmNode 0
			polyop.setNumMapVerts mmObject.mmNode 11 mapVertCount keep:false
			
			for face in mmObject.mmNode.faces do
			(
				local mapVertIndices = polyop.getMapFace mmObject.mmNode 0 face.index
				local mapVertArray = for index in mapVertIndices collect (polyop.getMapVert mmObject.mmNode 0 index)
				
				polyop.setMapFace mmObject.mmNode 11 face.index mapVertIndices
				
				for i = 1 to mapVertArray.count do
				(
					polyop.setMapVert mmObject.mmNode 11 mapVertIndices[i] mapVertArray[i]
				)
			)
			
			local mapVertCount = polyop.getNumMapVerts mmObject.Node 0
			polyop.setNumMapVerts mmObject.Node 11 mapVertCount keep:false
			
			for face in mmObject.Node.faces do
			(
				local mapVertIndices = polyop.getMapFace mmObject.Node 0 face.index
				local mapVertArray = for index in mapVertIndices collect (polyop.getMapVert mmObject.Node 0 index)
				
				polyop.setMapFace mmObject.Node 11 face.index mapVertIndices
				
				for i = 1 to mapVertArray.count do
				(
					polyop.setMapVert mmObject.Node 11 mapVertIndices[i] mapVertArray[i]
				)
			)
		
			if (polyop.getMapSupport mmObject.mmNode -2) then
			(
				local mapVertCount = polyop.getNumMapVerts mmObject.mmNode -2
				polyop.setNumMapVerts mmObject.mmNode 0 mapVertCount keep:false
				
				for face in mmObject.mmNode.faces do
				(
					local mapVertIndices = polyop.getMapFace mmObject.mmNode -2 face.index
					
					polyop.setMapFace mmObject.mmNode 0 face.index mapVertIndices
					
					local mapVertArray = for index in mapVertIndices collect (polyop.getMapVert mmObject.mmNode -2 index)
					
					for i = 1 to mapVertArray.count do
					(
						polyop.setMapVert mmObject.mmNode 0 mapVertIndices[i] mapVertArray[i]
					)
				)
			)
			else
			(
				polyop.defaultMapFaces mmObject.mmNode 0
			)
			
			polyOp.attach mmObject.node mmObject.mmNode
			
			for i = 2 to 10 do 
			(
				polyop.setMapSupport mmObject.node i false
			)
			
			mmObject.node.vertexColorType = #map_channel
			mmObject.node.vertexColorMapChannel = 11
		)
		
		count += 1
		
		progressUpdate (((count as float) / totalCount) * 100.0)
	)
	
	on btn_convertSelected pressed do
	(
		count = 0
		totalCount = 0
		
		if (queryBox "This will collapse the selected objects and copy their vertex colours over to channel 11.\nAre you sure you want to continue?\n\nDO NOT RUN THIS MORE THAN ONCE ON THE SAME OBJECT IF YOU HAVE RESET VERTEX COLOURS CHECKED!" title:"Warning...") then
		(
			objectArray = #()
			
			for object in (selection as array) where (getAttrClass object) == "Gta Object" and (classOf object) != XRefObject do
			(
				local mmObject = getMicroMovement object
				
				if mmObject != undefined then
				(
					append objectArray mmObject
				)
			)
			
			if objectArray.count > 0 then
			(
				totalCount = objectArray.count
				progressStart "Converting..."
				convertMicroMovement objectArray
				progressEnd()
			)
			else
			(
				messageBox "Either you have nothing selected or there are no objects with micro-movement shaders in the selection." title:"Error..."
			)
		)
	)
	
	on btn_selectMM pressed do 
	(
		local tempArray = #()
		
		for object in geometry where (getAttrClass object) == "Gta Object" and (classOf object) != XRefObject do
		(
			local mmObject = getMicroMovement object
			
			if mmObject != undefined then
			(
				append tempArray object
			)
		)
		
		select tempArray
	)
	
	on btn_convertAll pressed do
	(
		count = 0
		totalCount = 0
		
		if (queryBox "This will collapse all micro movement objects and copy their vertex colours over to channel 11.\nAre you sure you want to continue?\n\nDO NOT RUN THIS MORE THAN ONCE ON THE SAME OBJECT IF YOU HAVE RESET VERTEX COLOURS CHECKED!" title:"Warning...") then
		(
			objectArray = #()
			
			for object in geometry where (getAttrClass object) == "Gta Object" and (classOf object) != XRefObject do
			(
				local mmObject = getMicroMovement object
				
				if mmObject != undefined then
				(
					append objectArray mmObject
				)
			)
			
			if objectArray.count > 0 then
			(
				totalCount = objectArray.count
				progressStart "Converting..."
				convertMicroMovement objectArray
				progressEnd()
			)
			else
			(
				messageBox "There are no objects with micro-movement shaders in the scene." title:"Error..."
			)
		)
	)
)

createDialog RSL_MM_ChannelSwap style:#(#style_toolwindow,#style_sysmenu)