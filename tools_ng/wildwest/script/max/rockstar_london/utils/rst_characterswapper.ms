global swapBoneNames = #(
	"Char",
	"Char C NeckA",
	"Char Head TracerHACK",
	"Char Head Tracer",
	"Char HairParent",
	"Char HairMidA",
	"Char HairMidB",
	"Char HairMidC",
	"Char HairMidD",
	"Char HairMidE",
	"Char HairLeftA",
	"Char HairLeftB",
	"Char HairRightA",
	"Char HairRightB",
	"Char L Elbow",
	"Char L InnerElbow",
	"Char L Pit",
	"Char L Bicep_Parent",
	"Char L Bicep_Scale_C",
	"Char L Bicep_Scale_B",
	"Char L Bicep_Scale_A",
	"Char L NeckComp",
	"Char L Neck_HelperB",
	"Char R Elbow",
	"Char R InnerElbow",
	"Char R Pit",
	"Char R Bicep_Parent",
	"Char R Bicep_Scale_C",
	"Char R Bicep_Scale_B",
	"Char R Bicep_Scale_A",
	"Char R NeckComp",
	"Char R Neck_HelperB",
	"Char LungM",
	"Char LungR",
	"Char LungBR",
	"Char LungL",
	"Char LungBL",
	"Char Neckhelper",
	"Char L Neck_HelperA",
	"Char C NeckB",
	"Char C NeckC",
	"Char HairGRAVITYmaster",
	"Char HairBOUNCEMASTER",
	"Char HairGRAVITYSecondary",
	"Char HairMidGravityB",
	"Char HairBounceCGravity",
	"Char L Pit_Helper",
	"Char R Pit_Helper",
	"Char BellyBoneA",
	"Char BellyBoneB",
	"Char Belly_Parent",
	"ROOT DRIVER",
	"CAMERA CONTROL",
	"Char HairMidEGravityA"
)

fn IsTransBone name =
(
	-- Go through all the bone names in our list that we want to tag "exportTrans" too.
	for boneName in swapBoneNames do
	(
		if (matchPattern boneName pattern:name ignoreCase:true) then
			return true
	)
	
	return false
)

fn FixupCharacter rigSize =
(
		local MPEDObj = undefined
		local MPEDPosition = undefined
		local arrowObj = undefined
		local rootObj = undefined
		local charForeObj = undefined
	
		-- Unhide all the objects in the scene before we start.
		max unhide all
		
		-- Loop through the objects in the scene and grab the different objects we need to work with.
		for obj in objects do
		(
			try
			(
				if (matchPattern obj.name pattern:"arrow" ignoreCase:true) then
					arrowObj = obj
				
				if (matchPattern obj.name pattern:"root" ignoreCase:true) then
					rootObj = obj
				
				-- Only need to rename the one bone when we are working with a Large rig.
				if (rigSize == "Large") then
				(
					if (matchPattern obj.name pattern:"Char_R_ForeTwist" ignoreCase:true) then
						charForeObj = obj
				)
				
				if (matchPattern obj.name pattern:"mped" ignoreCase:true) then
					MPEDObj = obj
			)
			catch()
		)
		
		-- Store the position of the MPED object in the scene because we create a new dummy +1 metre from the origin.
		if (MPEDObj != undefined) then
			MPEDPosition = MPEDObj.position
		
		-- Rename the arrow object to be called "mover".
		if (arrowObj == undefined) then
			messagebox "No arrow node was found in the scene"
		else
			arrowObj.name = "mover"
		
		-- Rename the Char_R_ForeTwist to Char_R_ForeTwistA but only for the Large rig now.
		if (rigSize == "Large") then
		(
			if (charForeObj != undefined) then
				charForeObj.name = "Char_R_ForeTwistA"
		)
		
		-- We need to be able to find the root bone otherwise bail out.
		if (rootObj == undefined) then
			messagebox "Cannot find the Root node of the skeleton"
		else
		(
			-- Rename the root bone to "Char"
			rootObj.name = "Char"
			
			-- Set the tag user property to 0.
			if (getUserProp rootObj "tag" == undefined) then
				setUserProp rootObj "tag" "0"
			
			-- Unlink the root object from the current MPED object.
			select rootObj
			max unlink
			
			-- Delete the MPED object.
			if (MPEDObj != undefined) then
			(
				select MPEDObj
				delete MPEDObj
				MPEDObj = undefined
			)
			
			-- Create a new dummy in the same place as the MPED object +1 M on the Z axis.
			local newDummyObj = Dummy pos:[MPEDPosition.x, MPEDPosition.y, MPEDPosition.z + 1] boxsize:[0.15, 0.15, 0.15]
			
			-- Append the root object and the now "mover" object to the dummy01 object.
			append newDummyObj.children rootObj
			append newDummyObj.children arrowObj
		)
		
		-- Go through all the bones in the scene and make sure the exportTrans user property is set.
		for obj in objects do
		(
			if (classof obj == BoneGeometry) then
			(
				if (IsTransBone obj.name) then
					setUserProp obj "exportTrans" true
			)
		)
)

fn CheckIfDoneAlready =
(
	-- Go through our objects and if we have a Dummy01 we probably already had a convert pass on us.
	for obj in objects do
	(
		if (matchPattern obj.name pattern:"Dummy01" ignoreCase:true) then
		(
			messagebox "This character already seems to be converted."
			return true
		)
	)
	
	return false
)

fn ParentCurrentMeshes =
(
	local newDummyObj = undefined
	local meshObjs = #()
	
	-- Go through the scene roots children to parent meshes.
	for obj in rootNode.children do
	(
		-- Find our dummy object so we can use it as a base for our character dummy object.
		if (matchPattern obj.name pattern:"dummy01" ignoreCase:true) then
		(
			newDummyObj = Dummy pos:[obj.position.x, obj.position.y, obj.position.z] boxsize:[5, 5, 5]
			newDummyObj.name = getFilenameFile maxfilename
		)
		
		-- Keep track of all the possible meshes for the character.
		if (classof obj == PolyMeshObject or classof obj == Editable_Mesh) then
			append meshObjs obj
	)
	
	-- Parent all the meshes to our new dummy object with the same name as the character.
	for child in meshObjs do
	(
		append newDummyObj.children child
	)
)

rollout charSwapRollout "Character Swapper" width:144 height:144
(
	button btnLargeRig "Large Rig" pos:[8,8] width:128 height:24
	button btnMediumRig "Medium Rig" pos:[8,40] width:128 height:24
	
	on btnLargeRig pressed do
	(
		if (not CheckIfDoneAlready()) then
		(
			FixupCharacter "Large"
			if (queryBox "Would you like to parent current meshes to new dummy object named after the character?\nThis is needed for the Jimmy engine!") then
				ParentCurrentMeshes()
		)
	)
	
	on btnMediumRig pressed do
	(
		if (not CheckIfDoneAlready()) then
		(
			FixupCharacter "Medium"
			if (queryBox "Would you like to parent current meshes to new dummy object named after the character?\nThis is needed for the Jimmy engine!") then
				ParentCurrentMeshes()
		)
	)
)

try CloseRolloutFloater theNewFloater catch()
-- Create floater 
theNewFloater = newRolloutFloater "Character Tools" 160 100

-- Add the rollout section
addRollout charSwapRollout theNewFloater