
struct RSL_MapFaceCluster
(
	mapFaceArray = #(),
	intersectionPointArray = #()
)

struct RSL_MapFace
(
	mapVertIndexArray = #(),
	mapVertArray = #()
-- 	mapEdgeArray = #(),
	
-- 	fn getEdges =
-- 	(
-- 		mapEdgeArray = #()
-- 		mapEdgeArray.count = mapVertArray.count
-- 		
-- 		local count = 1
-- 		
-- 		for i = 1 to mapEdgeArray.count do 
-- 		(
-- 			local count = i + 1
-- 			
-- 			if count > mapEdgeArray.count then
-- 			(
-- 				count = 1
-- 			)
-- 			
-- 			mapEdgeArray[i] = mapVertArray[count] - mapVertArray[i]
-- 		)
-- 	)
)

struct RSL_SceneBrowsePropertyOps
(
	temp,
	
	fn getSceneBrowseObject object array =
	(
		local result
		
		for entry in array do 
		(
			if entry.node == object then
			(
				result = entry
			)
		)
		
		result
	),
	
	fn findTextureInTXD texture TXD =
	(
		local result = 0
		
		for i = 1 to TXD.textureArray.count do 
		(
			if TXD.textureArray[i].name == texture.name then
			(
				result = i
			)
		)
		
		result
	),
	
	fn getTXDPercentUsed object TXD sceneObjectArray =
	(
		local sceneBrowseObject = getSceneBrowseObject object sceneObjectArray
		local textureCount = 0
		
		for texture in sceneBrowseObject.textureArray do 
		(
			if (findTextureInTXD texture TXD) != 0 then
			(
				textureCount += 1
			)
		)
		
		textureCount as float / TXD.textureArray.count * 100.0
	),
	
	fn getFacesByID object IDArray =
	(
		local result = #()
-- 		local testMesh = snapshotAsMesh object
		
		if (classOf IDArray) == Array then
		(
			for face in object.faces do
			(
				case (classOf object) of
				(
					Editable_mesh:
					(
						if (findItem IDArray (getFaceMatID object face.index)) > 0 then
						(
							append result face.index
						)
					)
					Editable_Poly:
					(
						if (findItem IDArray (polyop.getFaceMatID object face.index)) > 0 then
						(
							append result face.index
						)
					)
				)
			)
		)
		else
		(
			result = for face in object.faces collect face.index
		)
		
-- 		delete testMesh
		result
	),
	
	fn isUsedInObject object ID =
	(
		local result = false
		
		for face in object.faces do
		(
			if (getFaceMatID object face.index) == ID then
			(
				result = true
				exit
			)
		)
		
		result
	),
	
	fn getID object texture =
	(
		local result = #()
		local testMesh = snapshotAsMesh object
		
		case (classOf object.material) of
		(
			default:
			(
				result = 0
			)
			Multimaterial:
			(
				for material in texture.materialArray do 
				(
					local index = findItem object.material.MaterialList material
					
					if index > 0 then
					(
						local ID = object.material.MaterialIDList[index]
						if (isUsedInObject testMesh ID) then
						(
							format "Texture:% ID:%\n" texture.name ID
							append result ID
						)
					)
				)
			)
		)
		
		delete testMesh
		
		result
	),
	
	fn lineLineIntersect pA pB pC pD =
	(
		local a=pB-pA
		local b=pD-pC
		local c=pC-pA
		local cross1 = cross a b
		local cross2 = cross c b
		
		pA + ( a*( (dot cross2 cross1)/((length cross1)^2) ) )
	),
	
	fn getIntersectionPoints this other =
	(
		local result = #()
		
		for i = 1 to this.mapVertArray.count do 
		(
			for s = 1 to other.mapVertArray.count do 
			(
-- 				local 
			)
		)
		
		result
	),
	
	fn checkForIntersection this other =
	(
		local result = #()
		
		for i = 1 to this.mapFaceArray.count do 
		(
			for s = 1 to other.mapFaceArray.count do 
			(
				local intersectArray = getIntersectionPoints this.mapFaceArray[i] other.mapFaceArray[s]
				
				if intersectArray.count > 0 then
				(
					result += intersectArray
				)
			)
		)
		
		result
	),
	
	fn packClusters clusterArray =
	(
		local tempArray = for entry in clusterArray collect entry
		
		for i = 1 to clusterArray.count - 1 do 
		(
			local this = clusterArray[i]
			
			if this != undefined then
			(
				for s = i to clusterArray.count do 
				(
					local other = clusterArray[s]
					
					if other != undefined then
					(
						local intersectionPoints = checkForIntersection this other
						
						if intersectionPoints.count > 0 then
						(
							
						)
					)
				)
			)
		)
		
		
		clusterArray = for entry in tempArray where entry != undefined collect entry
	),
	
	fn getTexturePercentUsed object faceArray =
	(
		local result = 0
		local testMesh = snapshotAsMesh object
		local clusterArray = #()
		clusterArray.count = faceArray.count
		
		for i = 1 to faceArray.count do 
		(
			local mapVertIndices = (meshop.getMapVertsUsingMapFace object 1 faceArray[i]) as array
			local mapFace = RSL_MapFace()
-- 			mapFace.mapVertIndexArray.count = mapVertIndices.count
			mapFace.mapVertArray.count = mapVertIndices.count
			
			for i = 1 to mapVertIndices.count do
			(
-- 				mapFace.mapVertIndexArray[i] = mapVertIndices[i]
				local mapVert = meshop.getMapVert object 1 mapVertIndices[i]
				mapVert.z = 0.0
				
				mapFace.mapVertArray[i] = mapVert
			)
			
-- 			mapFace.getEdges()
			
			clusterArray[i] = RSL_MapFaceCluster mapFaceArray:#(mapFace)
		)
		
		clusterArray = packClusters clusterArray
		
		
		
		
		clusterArray = undefined
		
		delete testMesh
		result
	)
)