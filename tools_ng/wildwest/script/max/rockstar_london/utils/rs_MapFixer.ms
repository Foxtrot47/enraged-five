
try CloseRolloutFloater RsFixMyMap catch () 

rollout Info "About"
(
		
	bitmap the_bmp fileName:(RsConfigGetWildWestDir()+"/script/max/rockstar_london/images/FixMyMapAbout.jpg")
)

rollout RsFixMyGeometry "Fix My Geometry v0.1"
(
	button btn_fixgeo "Validate Geometry"
	label lbl_geo "This will go through all the static mesh objects"
	label lbl_geo1 "in your scene and reset Xforms on them"
	
	mapped fn resetgeoTransform geomesh =
	(
		local parentobj = undefined
		local childobjs = #()
		local objclass = classof geomesh
		
		-- Unlink any parents and children
		if geomesh.parent != undefined then	(
			parentobj = geomesh.parent -- Remember the parent
			geomesh.parent = undefined -- unlink the object
		)
		
		if geomesh.children != undefined then 	(
			childobjs = #()
			
			for i in geomesh.children do (
				append childobjs i
				i.parent = undefined
			)	
			--print (":: "+childobjs as string)
			
		)

		-- Reset Xform on the geometry mesh
		ResetXForm geomesh
		
		if objclass == Editable_mesh then (converttomesh geomesh)
		if objclass == Editable_Poly then (converttopoly geomesh)
		
		
		-- Relink back any parents/children
		if parentobj != undefined then (
			geomesh.parent = parentobj
		)
		if childobjs.count > 0 then (
			for i in childobjs do
			(
				i.parent = geomesh
			)
		)
		
	)
	
	fn _main_ =
	(	
		
		local ValidGeoMeshes = for i in geometry where classof i == Editable_Poly or  classof i == Editable_Mesh collect  i	
		print ValidGeoMeshes
		if ValidGeoMeshes.count > 0 then
			resetgeoTransform ValidGeoMeshes
		messagebox ("Reset "+ValidGeoMeshes.count as string + " Geometry meshes!")
		
	)
	
	on btn_fixgeo pressed do
	(
		_main_()
	)
	
)

rollout RsFixMyCollisions "Fix My Collision v0.1"
(
	button btn_fixCol "Validate Collisions"
	label lbl_col "This will go through all the mesh collision"
	label lbl_col1 "in your scene and reset Xforms on them"
	
	
	mapped fn resetCollisionTransform collmesh =
	(
		col2mesh collmesh
		ResetXForm collmesh
		mesh2col collmesh
	)
	
	fn _main_ =
	(	
		
		ValidColMeshes = for i in helpers where classof i == Col_Mesh collect  i		
		resetCollisionTransform ValidColMeshes
		messagebox ("Reset "+ValidColMeshes.count as string + " Collision meshes!")
		
	)
	
	on btn_fixCol pressed do
	(
		_main_()
	)
	
	
)

rollout RsFixMyTxds "Fix My Txds v0.1"
(
	fn returnValidTXDName txdname =
	(	
		if txdname.count > 20 then
		(
			local newtxdname = ""
			for i = 1 to 20 do
			(
				newtxdname+=txdname[i]
			)
			print ("OLD: "+txdname + "NEW: "+newtxdname)
			txdname= newtxdname
		)
		txdname
	)

	fn _main_ =
	(
		for i in objects where GetAttrClass i == "Gta Object" do
		(
			
			local txdName = getattr i 50
			setattr i 50 (returnValidTXDName txdName)
			
		)
		messagebox ("Done!")
	)
	
	button btn_fixtxd "Validate TXDs"
	label lbl_txd "This will check all the TXD's name lengths"
	label lbl_txd1 "in your scene and rename them"
	label lbl_txd2 "if they exceed 20 characters" 
	
	on btn_fixtxd pressed do
	(
		_main_()
	)
)

RsFixMyMap= newRolloutFloater ("Fix My Map v0.1" ) 240 320 50 126
addRollout info RsFixMyMap rolledup:true
addRollout RsFixMyCollisions RsFixMyMap rolledup:false
addRollout RsFixMyTxds RsFixMyMap rolledup:false
addRollout RsFixMyGeometry RsFixMyMap rolledup:false
