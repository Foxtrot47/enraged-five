
try (destroyDialog RS_CreateTopDownMap) catch()

rollout RS_CreateTopDownMap "Set up Top Down Map Render" width:170
(
	
	button btn_SetMap "Set up Map Plane on Selected!" width:160 align:#center
	
	fn createMapPlane =
	(
		if selection.count > 0 then
		(
			local minBound = selection.min
			local maxBound = selection.max
			local size = maxBound - minBound
			local dimention = amax #(size.x, size.y)
			dimention *= 1.5
			
			plane name:"Map Plane" width:dimention length:dimention pos:([selection.center.x,selection.center.y,minBound.z])
			freeCamera pos:([selection.center.x,selection.center.y,maxBound.z * 10]) dir:[0,0,1]
		)
		else
		(
			Messagebox "Please Select the level Geometry to define the map bounds!"
		)
	)
	
	
	
	on  btn_SetMap pressed do
	(
		createMapPlane()
	)
	
	
)

createDialog RS_CreateTopDownMap style:#(#style_toolwindow,#style_sysmenu)