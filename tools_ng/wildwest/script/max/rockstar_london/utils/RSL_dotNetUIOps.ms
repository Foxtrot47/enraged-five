--=-=-=-=-=-=-=-=-=-=-=-=-= RSL_dotNetUIOps =-=-=-=-=-=-=-=-=-=-=-=-=--
/*
	File:
		RSL_dotNetUIOps.ms
	Description:
		Defines a stucture containing dotNet UI functions to speed up the process of creating dotNet tools
	Created by:
		Paul Truss
		Senior Technical Artist
		Rockstar London
	History:
		Created April 12 2009

*/

filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetPresets.ms")
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetObjects.ms")

dotnet.loadAssembly (RsConfigGetWildWestDir() + "script/max/Rockstar_London/dll/RGLExportHelpers.dll")
(DotNetClass "System.Windows.Forms.Application").EnableVisualStyles()

--=-=-=-=-=-=-=-=-=-=-=-=-= RSL_dotNetUIOps =-=-=-=-=-=-=-=-=-=-=-=-=--
struct RSL_dotNetUIOps
(
	--=-=-=-=-=-=-=-=-=-=-=-=-= dotNet UI =-=-=-=-=-=-=-=-=-=-=-=-=--
	fn maxForm name Text:"New Form" size:[320,240] min:undefined max:undefined borderStyle:undefined =
	(
		local result = dotNetObject "MaxCustomControls.MaxForm"
		result.name = name
		result.Text = Text
		result.Size = RS_dotNetObject.sizeObject size
-- 		result.BackColor = RS_dotNetClass.systemColourClass.HotTrack
		
		if (classOf min) == Point2 then
		(
			result.MinimumSize = dotNetObject "System.Drawing.Size" min.x min.y
		)
		
		if (classOf max) == Point2 then
		(
			result.MaximumSize = dotNetObject "System.Drawing.Size" max.x max.y
		)
		
		if borderStyle != undefined then
		(
			result.FormBorderStyle = borderStyle
		)
		
		result
	),
	
	fn form name Text:"New Form" size:[320,240] min:undefined max:undefined borderStyle:undefined =
	(
		local result = dotNetObject "Form"
		result.name = name
		result.Text = Text
		result.Size = RS_dotNetObject.sizeObject size
		result.ShowInTaskbar = False
-- 		result.BackColor = RS_dotNetClass.systemColourClass.HotTrack
		
		if (classOf min) == Point2 then
		(
			result.MinimumSize = dotNetObject "System.Drawing.Size" min.x min.y
		)
		
		if (classOf max) == Point2 then
		(
			result.MaximumSize = dotNetObject "System.Drawing.Size" max.x max.y
		)
		
		if borderStyle != undefined then
		(
			result.FormBorderStyle = borderStyle
		)
		
		result
	),
	
	fn button name Text:"New Button" size:[128,24] location:[8,8] dockStyle:undefined icon:"" margin:undefined textAlign:undefined =
	(
		local result = dotNetObject "Button"
		result.name = name
		result.Text = Text
		result.Font = RS_dotNetPreset.FontStandard
		result.Size = RS_dotNetObject.sizeObject size
		result.location = RS_dotNetObject.pointObject location
		
		if margin == undefined then
		(
			result.margin = (RS_dotNetObject.paddingObject 1)
		)
		else
		(
			result.margin = margin
		)
		
-- 		result.FlatStyle = RS_dotNetPreset.FS_System --FS_Flat
		result.BackColor = RS_dotNetClass.colourClass.Transparent--RS_dotNetPreset.controlColour_Light
		
		result.FlatAppearance.MouseOverBackColor = RS_dotNetPreset.mouseOverColour
		result.FlatAppearance.CheckedBackColor = RS_dotNetPreset.warningColour
		
		if textAlign != undefined then
		(
			result.TextAlign = textAlign
		)
		else
		(
			result.TextAlign = RS_dotNetPreset.CA_MiddleCenter
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
		)
		
		if result.dock.value__ > 0 then
		(
			result.AutoSize = true
			result.AutoSizeMode = RS_dotNetPreset.SM_GrowAndShrink
		)
		
		if icon != "" and icon != undefined then
		(
			result.Image = RS_dotNetObject.imageObject icon
			result.ImageAlign = RS_dotNetPreset.CA_MiddleCenter
			result.TextImageRelation = (dotNetClass "System.Windows.Forms.TextImageRelation").ImageBeforeText
			result.Text = ""
		)
		
		result
	),
	
	fn checkBox name Text:"New Checkbox" size:undefined location:undefined appearance:RS_dotNetPreset.AC_Normal textAlign:undefined checkAlign:undefined margin:undefined dockStyle:undefined flatStyle:undefined =
	(
		local result = dotNetObject "CheckBox"
		result.name = name
		result.Text = Text
		result.Font = RS_dotNetPreset.FontStandard
		result.BackColor = RS_dotNetClass.colourClass.Transparent
		
		if textAlign != undefined then
		(
			result.TextAlign = textAlign
		)
		else
		(
			result.TextAlign = RS_dotNetPreset.CA_MiddleCenter
		)
		
		if size!= undefined then result.Size = RS_dotNetObject.sizeObject size
		
		if location != undefined then	result.location = RS_dotNetObject.pointObject location
		
		result.Appearance = appearance
		
		if result.Appearance.value__ == 1 then
		(
			result.margin = (RS_dotNetObject.paddingObject 1)
-- 			result.BackColor = RS_dotNetPreset.controlColour_Light
			result.FlatAppearance.MouseOverBackColor = RS_dotNetPreset.mouseOverColour
			result.FlatAppearance.CheckedBackColor = RS_dotNetPreset.warningColour
		)
		else
		(
			if checkAlign != undefined then
			(
				result.CheckAlign = checkAlign
			)
		)
		
		if margin == undefined then
		(
			result.margin = (RS_dotNetObject.paddingObject 1)
		)
		else
		(
			result.margin = margin
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
		)
		
		if flatStyle != undefined then
		(
			result.FlatStyle = flatStyle
		)
		else
		(
-- 			result.FlatStyle = RS_dotNetPreset.FS_Flat
		)
		
		result
	),
	
	fn checkedListBox name size:[128,128] location:[8,56] items:undefined borderStyle:RS_dotNetPreset.BS_FixedSingle dockStyle:RS_dotNetPreset.DS_Fill =
	(
		local result = dotNetObject "CheckedListBox"
		result.name = name
		result.Size = RS_dotNetObject.sizeObject size
		result.location = RS_dotNetObject.pointObject location
		if items!= undefined then result.Items.AddRange items
		
		if borderStyle != undefined then
		(
			result.BorderStyle = borderStyle
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
		)
		
		result
	),
	
	fn label name Text:"New Label" size:undefined location:[128,32] textAlign:undefined borderStyle:RS_dotNetPreset.BS_None dockStyle:RS_dotNetPreset.DS_Fill = 
	(
		local result = dotNetObject "Label"
		result.name = name
		result.Text = Text
		result.Font = RS_dotNetPreset.FontStandard
		if size!= undefined then result.Size = RS_dotNetObject.sizeObject size
		result.location = RS_dotNetObject.pointObject location
		result.AutoEllipsis = true
		
		if textAlign != undefined then
		(
			result.TextAlign = textAlign
		)
		
		if borderStyle != undefined then
		(
			result.BorderStyle = borderStyle
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
		)
		
		result.padding = (RS_dotNetObject.paddingObject 0)
		result.margin = (RS_dotNetObject.paddingObject 0)
		
		result
	),
	
	fn listBox name size:[128,128] location:[128,56] items:undefined =
	(
		local result = dotNetObject "ListBox"
		result.name = name
		result.Size = RS_dotNetObject.sizeObject size
		result.location = RS_dotNetObject.pointObject location
		if items!= undefined then result.Items.AddRange items
		
		result
	),
	
	fn listView name size:[128,128] location:[128,56] items:undefined MultiSelect:false borderStyle:RS_dotNetPreset.BS_FixedSingle dockStyle:undefined =
	(
		local result = dotNetObject "ListView"
		result.name = name
		result.Size = RS_dotNetObject.sizeObject size
		result.location = RS_dotNetObject.pointObject location
		result.BackColor = RS_dotNetPreset.controlColour_Dark
		result.MultiSelect = MultiSelect
		result.view = (dotNetClass "System.Windows.Forms.View").Details
		
		if borderStyle != undefined then
		(
			result.BorderStyle = borderStyle
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
		)
		
-- 		if items!= undefined then result.Items.AddRange items
		
		result
	),
	
	fn tableLayout name text:"New TableLayout" collumns:undefined rows:undefined dockStyle:undefined cellStyle:false =
	(
		local result = dotNetObject "TableLayoutPanel"
		result.name = name
		result.Text = Text
		result.ContextMenuStrip = undefined
-- 		result.padding = (RS_dotNetObject.paddingObject 0)
		result.margin = (RS_dotNetObject.paddingObject 0)
		result.AutoSize = true
		result.BackColor = RS_dotNetClass.colourClass.Transparent
		
		if (classOf collumns) == Array then
		(
			result.ColumnCount = collumns.count
			
			for entry in collumns do
			(
				result.ColumnStyles.add (RS_dotNetObject.ColumnStyleObject entry.type entry.value)
			)
		)
		
		if (classOf rows) == Array then
		(
			result.RowCount = rows.count
			
			for entry in rows do
			(
				result.RowStyles.add (RS_dotNetObject.RowStyleObject entry.type entry.value)
			)
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
		)
		
		if cellStyle != false then
		(
			result.CellBorderStyle = cellStyle
		)
		
		result
	),
	
	fn panel name text:"New Panel" size:[128,128] location:undefined borderStyle:RS_dotNetPreset.BS_FixedSingle dockStyle:undefined =
	(
		local result = dotNetObject "Panel"
		result.name = name
		result.Text = Text
		result.Size = RS_dotNetObject.sizeObject size
		result.padding = (RS_dotNetObject.paddingObject 0)
		result.margin = (RS_dotNetObject.paddingObject 0)
		result.ContextMenuStrip = undefined
		
		if location != undefined then
		(
			result.location = RS_dotNetObject.pointObject location
		)
		
		if borderStyle != undefined then
		(
			result.BorderStyle = borderStyle
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
		)
		
		result
	),
	
	fn ContextMenuStrip name items:undefined renderMode:undefined =
	(
		local result = dotNetObject "ContextMenuStrip"
		result.name = name
		
		if items != undefined then
		(
			result.Items.AddRange items
		)
		
		if renderMode == undefined then
		(
			result.RenderMode = (dotNetClass "System.Windows.Forms.ToolStripRenderMode").Professional
		)
		else
		(
			result.RenderMode = renderMode
		)
		
		result
	),
	
	fn ToolStripMenuItem name text:"New MenuItem" menuItems:undefined =
	(
		local result = dotNetObject "ToolStripMenuItem"
		result.name = name
		result.text = text
		
		if menuItems != undefined and (classOf menuItems) == Array then
		(
			result.DropDownItems.AddRange menuItems
		)
		
		result
	),
	
	fn separator =
	(
		local result = dotNetObject "ToolStripSeparator"
	),
	
	fn menuStrip name text:"New tabPage" menuItems:undefined =
	(
		local result = dotNetObject "MenuStrip"
		result.name = name
		result.Text = Text
		
		if menuItems != undefined and (classOf menuItems) == Array then
		(
			result.Items.AddRange menuItems
		)
		
		result
	),
	
	fn textBox name text:"New MenuItem" size:undefined location:undefined borderStyle:RS_dotNetPreset.BS_FixedSingle dockStyle:undefined =
	(
		local result = dotNetObject "TextBox"
		result.name = name
		result.Text = Text
-- 		result.padding = (RS_dotNetObject.paddingObject 0)
-- 		result.margin = (RS_dotNetObject.paddingObject 0)
		
		if size != undefined then
		(
			result.Size = RS_dotNetObject.sizeObject size
		)
		
		if location != undefined then
		(
			result.location = RS_dotNetObject.pointObject location
		)
		
		if borderStyle != undefined then
		(
			result.BorderStyle = borderStyle
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
			result.AutoSize = true
		)
		
		result
	),
	
	fn Numeric name text:"New Numeric" size:undefined location:undefined value:undefined range:undefined borderStyle:undefined dockStyle:undefined margin:undefined =
	(
		local result = dotNetObject "NumericUpDown"
		result.name = name
		result.Text = Text
-- 		result.padding = (RS_dotNetObject.paddingObject 0)
-- 		result.margin = (RS_dotNetObject.paddingObject 0)
		result.BackColor = RS_dotNetPreset.controlColour_Light
		
		if size != undefined then
		(
			result.Size = RS_dotNetObject.sizeObject size
		)
		
		if location != undefined then
		(
			result.location = RS_dotNetObject.pointObject location
		)
		
		if range != undefined and range.count == 4 then
		(
			result.Maximum = range[1]
			result.Minimum = range[2]
			result.Increment = range[3]
			result.DecimalPlaces = range[4]
		)
		
		if value != undefined then
		(
			result.value = value
		)
		
		if borderStyle != undefined then
		(
			result.BorderStyle = borderStyle
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
			result.AutoSize = true
		)
		
		if margin == undefined then
		(
			result.margin = (RS_dotNetObject.paddingObject 1)
		)
		else
		(
			result.margin = margin
		)
		
		result
	),
	
	fn GroupBox name text:"New GroupBox" size:undefined location:undefined dockStyle:undefined =
	(
		local result = dotNetObject "GroupBox"
		result.name = name
		result.Text = Text
-- 		result.padding = (RS_dotNetObject.paddingObject 0)
-- 		result.margin = (RS_dotNetObject.paddingObject 0)
		result.AutoSize = true
		
		if size != undefined then
		(
			result.Size = RS_dotNetObject.sizeObject size
		)
		
		if location != undefined then
		(
			result.location = RS_dotNetObject.pointObject location
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
			result.AutoSize = true
		)
		
		result
	),
	
	fn treeView name size:undefined location:undefined borderStyle:undefined dockStyle:undefined =
	(
		local result = dotNetObject "TreeView"
		result.name = name
		result.indent = 16
		result.BackColor = RS_dotNetPreset.controlColour_Dark
		result.lineColor = RS_dotNetClass.colourClass.Firebrick
		result.Sorted = true
		result.FullRowSelect = true
		result.HideSelection = false
		
		if size != undefined then
		(
			result.Size = RS_dotNetObject.sizeObject size
		)
		
		if location != undefined then
		(
			result.location = RS_dotNetObject.pointObject location
		)
		
		if borderStyle != undefined then
		(
			result.BorderStyle = borderStyle
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
			result.AutoSize = true
		)
		
		result
	),
	
	fn ToolStrip name size:undefined location:undefined renderMode:undefined dockStyle:undefined =
	(
		local result = dotNetObject "ToolStrip"
		result.name = name
		result.GripStyle = (dotNetClass "System.Windows.Forms.ToolStripGripStyle").Hidden
		
		if size != undefined then
		(
			result.Size = RS_dotNetObject.sizeObject size
		)
		
		if location != undefined then
		(
			result.location = RS_dotNetObject.pointObject location
		)
		
		if renderMode != undefined then
		(
			result.renderMode = renderMode
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
			result.AutoSize = true
		)
		
		result
	),
	
	fn ToolStripCombo name text:undefined size:undefined autoSize:undefined =
	(
		local result = dotNetObject "ToolStripComboBox"
		result.name = name
		
		if text != undefined then
		(
			result.text = text
		)
		
		if size != undefined then
		(
			result.Size = RS_dotNetObject.sizeObject size
		)
		
		if autoSize != undefined then
		(
			result.autoSize = autoSize
		)
		
		result
	),
	
	fn toolStripButton name text:undefined size:undefined autoSize:undefined CheckOnClick:false =
	(
		local result = dotNetObject "ToolStripButton"
		result.name = name
		result.CheckOnClick = CheckOnClick
		
		if text != undefined then
		(
			result.text = text
		)
		
		if size != undefined then
		(
			result.Size = RS_dotNetObject.sizeObject size
		)
		
		if autoSize != undefined then
		(
			result.autoSize = autoSize
		)
		
		result
	),
	
	fn SplitContainer name size:undefined location:undefined borderStyle:undefined dockStyle:undefined =
	(
		local result = dotNetObject "SplitContainer"
		result.name = name
		result.SplitterWidth = 4
		result.SplitterDistance = 74
		result.padding = (RS_dotNetObject.paddingObject 0)
		result.margin = (RS_dotNetObject.paddingObject 0)
		
		
		if size != undefined then
		(
			result.Size = RS_dotNetObject.sizeObject size
		)
		
		if location != undefined then
		(
			result.location = RS_dotNetObject.pointObject location
		)
		
		if borderStyle != undefined then
		(
			result.BorderStyle = borderStyle
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
			result.AutoSize = true
		)
		
		result
	),
	
	fn tabControl name size:undefined location:undefined controls:#() borderStyle:undefined dockStyle:undefined =
	(
		local result = dotNetObject "tabControl"
		result.name = name
-- 		result.padding = (RS_dotNetObject.paddingObject 0)
-- 		result.margin = (RS_dotNetObject.paddingObject 0)
		
		if size != undefined then
		(
			result.Size = RS_dotNetObject.sizeObject size
		)
		
		if location != undefined then
		(
			result.location = RS_dotNetObject.pointObject location
		)
		
		if controls.count > 0 then
		(
			result.Controls.AddRange controls
		)
		
		if borderStyle != undefined then
		(
			result.BorderStyle = borderStyle
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
			result.AutoSize = true
		)
		
		result
	),
	
	fn tabPage name text:"New tabPage" control:undefined =
	(
		local result = dotNetObject "TabPage"
		result.name = name
		result.Text = Text
		result.BackColor = RS_dotNetPreset.controlColour_Light
		
		if control != undefined then
		(
			result.Controls.Add control
		)
		
		result
	),
	
	fn taskTreeView name size:undefined location:undefined borderStyle:undefined dockStyle:undefined =
	(
		local result = dotNetObject "RGLExportHelpers.TaskTreeView"
		result.name = name
		result.indent = 16
		result.TaskRectWidth = 85
		result.BackColor = RS_dotNetPreset.controlColour_Dark
		result.lineColor = RS_dotNetClass.colourClass.Firebrick
		
		result.TaskGroupBorderColor = RS_dotNetClass.systemColourClass.ControlDarkDark
		result.JobFillIncompleteColor = RS_dotNetClass.systemColourClass.ControlDarkDark
		result.TaskGroupFillColor = RS_dotNetClass.colourClass.Lime--YellowGreen
		result.JobFillCompleteColor = RS_dotNetClass.colourClass.LimeGreen
		
		result.Sorted = true
		result.FullRowSelect = true
		result.HideSelection = false
		
		if size != undefined then
		(
			result.Size = RS_dotNetObject.sizeObject size
		)
		
		if location != undefined then
		(
			result.location = RS_dotNetObject.pointObject location
		)
		
		if borderStyle != undefined then
		(
			result.BorderStyle = borderStyle
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
			result.AutoSize = true
		)
		
		result
	),
	
	fn taskTreeNode text:"New Node" completion:1 tag:undefined =
	(
		local result = dotNetObject "RGLExportHelpers.PropTaskTreeNode"
		result.text = text
		result.NodeFont = RS_dotNetPreset.FontMedium
		result.completion = completion
		
		if tag != undefined then
		(
			result.tag = dotNetMXSValue tag
		)
		
		result
	),
	
	fn pictureBox name text:"New PictureBox" size:undefined location:undefined borderStyle:undefined dockStyle:undefined =
	(
		local result = dotNetObject "PictureBox"
		result.name = name
		result.Text = Text
		result.margin = (RS_dotNetObject.paddingObject 0)
		
		if size != undefined then
		(
			result.Size = RS_dotNetObject.sizeObject size
		)
		
		if location != undefined then
		(
			result.location = RS_dotNetObject.pointObject location
		)
		
		if borderStyle != undefined then
		(
			result.BorderStyle = borderStyle
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
			result.AutoSize = true
		)
		
		result
	),
	
	fn textBox name text:"" size:undefined location:undefined borderStyle:undefined dockStyle:undefined =
	(
		local result = dotNetObject "textBox"
		result.name = name
		result.Text = Text
-- 		result.margin = (RS_dotNetObject.paddingObject 0)
		
		if size != undefined then
		(
			result.Size = RS_dotNetObject.sizeObject size
		)
		
		if location != undefined then
		(
			result.location = RS_dotNetObject.pointObject location
		)
		
		if borderStyle != undefined then
		(
			result.BorderStyle = borderStyle
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
			result.AutoSize = true
		)
		
		result
	),
	
	fn dataGridView name size:undefined location:undefined borderStyle:undefined dockStyle:undefined =
	(
		local result = dotNetObject "System.Windows.Forms.DataGridView"
		result.name = name
		result.margin = (RS_dotNetObject.paddingObject 0)
		
		if size != undefined then
		(
			result.Size = RS_dotNetObject.sizeObject size
		)
		
		if location != undefined then
		(
			result.location = RS_dotNetObject.pointObject location
		)
		
		if borderStyle != undefined then
		(
			result.BorderStyle = borderStyle
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
			result.AutoSize = true
		)
		
		result.multiselect = false
		result.Visible = true
		result.RowHeadersVisible = false			
		result.ColumnHeadersHeight = 16
		result.ColumnHeadersDefaultCellStyle = dotnetobject "system.Windows.Forms.DataGridViewCellStyle"
		result.cellborderstyle= (dotnetclass "System.Windows.Forms.DataGridViewCellBorderStyle").none
		result.AllowUserToAddRows = false
		result.AllowUserToDeleteRows = false
		result.AllowUserToOrderColumns = false
		result.AllowUserToResizeRows = false
		result.ColumnHeadersHeightSizeMode = (dotnetclass "System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode").DisableResizing    
-- 		result.EnableHeadersVisualStyles = False
		result.ColumnHeadersBorderStyle = (dotnetclass "System.Windows.Forms.DataGridViewHeaderBorderStyle").none	
		
		result
	),
	
	fn trackBar name size:undefined location:undefined range:undefined dockStyle:undefined =
	(
		local result = dotNetObject "System.Windows.Forms.TrackBar"
		result.name = name
		
		if range != undefined then
		(
			result.maximum = range[1]
			result.minimum = range[2]
			result.value = range[3]
		)
		
		if size != undefined then
		(
			result.Size = RS_dotNetObject.sizeObject size
		)
		
		if location != undefined then
		(
			result.location = RS_dotNetObject.pointObject location
		)
		
		if dockStyle != undefined then
		(
			result.dock = dockStyle
			result.AutoSize = true
		)
		
		result
	)
)

RS_dotNetUI = RSL_dotNetUIOps()
