--script to find out the dimensions and center of a sub-object selection
fn ProcessVerts vertSel =
(
	maxminx = #(vertSel[1].pos.x, vertSel[1].pos.x)
	maxminy = #(vertSel[1].pos.y, vertSel[1].pos.y)
	maxminz = #(vertSel[1].pos.z, vertSel[1].pos.z)
	for i = 2 to vertSel.count do
	(
		if (vertSel[i].pos.x > maxminx[1]) then maxminx[1] = vertSel[i].pos.x
		if (vertSel[i].pos.x < maxminx[2]) then maxminx[2] = vertSel[i].pos.x
		if (vertSel[i].pos.y > maxminy[1]) then maxminy[1] = vertSel[i].pos.y
		if (vertSel[i].pos.y < maxminy[2]) then maxminy[2] = vertSel[i].pos.y
		if (vertSel[i].pos.z > maxminz[1]) then maxminz[1] = vertSel[i].pos.z
		if (vertSel[i].pos.z < maxminz[2]) then maxminz[2] = vertSel[i].pos.z
	)
	xval = (maxminx[1] - maxminx[2])
	yval = (maxminy[1] - maxminy[2])
	zval = (maxminz[1] - maxminz[2])
	selsize = #(xval, yval, zval)
	xval = ( (maxminx[1] + maxminx[2]) / 2)
	yval = ( (maxminy[1] + maxminy[2]) / 2)
	zval = ( (maxminz[1] + maxminz[2]) / 2)
	selcenter = #(xval, yval, zval)
	DimData = #(selsize, selcenter)
	DimData
)

fn ProcessObject target =
(
	selcenter = target.center
	xval = target.max.x - objSet[1].min.x
	yval =target.max.y - objSet[1].min.y
	zval = target.max.z - objSet[1].min.z
	selsize = #(xval, yval, zval)
	DimData = #(selsize, selcenter)
	DimData
)

fn GetSubObjectData =
(
	DimData = #()
	SubObjectMode = SubObjectLevel
	objSet = for this in selection where (classOf this == Editable_Poly) collect this
	if objSet.count == 1 then
	(
		case SubObjectMode of
		(
			1: --verts
			(
				vertSel = objSet[1].selectedVerts
				if vertSel.count > 1 then
				(
					DimData = ProcessVerts vertSel
				)
				else --if no verts selected, use the whole object
				(
					DimData = ProcessObject objSet[1]
				)
			)
			2: --edges
			(
				edgeSel = objSet[1].selectedEdges
				if edgeSel.count > 0 then
				(
					objSet[1].ConvertSelection #CurrentLevel #Vertex
					vertSel = objSet[1].selectedVerts
					DimData = ProcessVerts vertSel
					SubObjectLevel = SubObjectMode
				)
				else --if no verts selected, use the whole object
				(
					DimData = ProcessObject objSet[1]
				)
			)
			4: --polygons
			(
				polySel = objSet[1].selectedFaces
				if polySel.count > 0 then
				(
					objSet[1].ConvertSelection #CurrentLevel #Vertex
					vertSel = objSet[1].selectedVerts
					DimData = ProcessVerts vertSel
					SubObjectLevel = SubObjectMode
				)
				else --if no verts selected, use the whole object
				(
					DimData = ProcessObject objSet[1]
				)
			)
			5: --elements
			(
				polySel = objSet[1].selectedFaces
				if polySel.count > 0 then
				(
					objSet[1].ConvertSelection #CurrentLevel #Vertex
					vertSel = objSet[1].selectedVerts
					DimData = ProcessVerts vertSel
					SubObjectLevel = SubObjectMode
				)
				else --if no verts selected, use the whole object
				(
					DimData = ProcessObject objSet[1]
				)
			)
			default:
			(
				DimData = ProcessObject objSet[1]
			)
		)
	)
	else
	(
		messagebox "Please select one object"
	)
	DimData
)

-- myData = GetSubobjectData()
-- try
-- (
-- 	print myData[2][3]
-- )
-- catch()