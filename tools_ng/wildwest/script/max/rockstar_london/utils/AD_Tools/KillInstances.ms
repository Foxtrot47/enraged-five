sel = geometry as array
count = 1

if sel.count > 0 then
(
	undo on
	(
		while count <= sel.count do
		(
			select sel[count]
			InstanceMgr.GetInstances $ &instances
			if instances.count > 0 do InstanceMgr.MakeObjectsUnique &instances #individual
			deselect $
			count += 1
		)
	)
)
else
(
	messageBox "You must select at least one or more objects" title:"MatID Random Material Assigner"
)