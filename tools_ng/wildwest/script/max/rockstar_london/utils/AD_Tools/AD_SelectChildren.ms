if selection.count > 0 then
(
	childrenSet = #()
	for this in selection do
	(
		join childrenSet this.children
	)
	select childrenSet 
)
else
(
	messagebox "[Select child]\nplease select an object"
)