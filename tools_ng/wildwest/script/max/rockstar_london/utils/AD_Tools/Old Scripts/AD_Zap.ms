--script removes an edge loop
if (selection.count !=1) then
(
	messagebox"please select one object"
)
else
(
	if (getAttrClass $ == "Gta Object") then
	(
		$.SelectEdgeLoop()
		macros.run "Editable Polygon Object" "EPoly_Remove"
	)
	else
	(
		messagebox "non-valid geometry"
	)
)