objSet = for this in selection where (classOf this != XrefObject) collect this
if objSet.count > 1 then
(
	newSelection = #()
	pivotPosition = objSet[objSet.count].pivot
	for i = 1 to (objSet.count - 1) do
	(
		objSet[i].pivot = pivotPosition
		append newSelection objSet[i]
	)
	print ("pivots matched to " + objSet[objSet.count].name)
	select newSelection
)
else messagebox "Please select more than one object"