-- duplicate clone
(
	local copySet = #()
	for this in selection do
	(
		newObj = copy this
-- 		setAttr newObj (getAttrIndex "Gta Object" "TXD") (getFilenameFile maxFileName) -- get TXD from scene name
		if (superClassOf this == GeometryClass) then
		(
			setAttr newObj (getAttrIndex "Gta Object" "TXD") (getattr this (getattrindex "Gta Object" "TXD")) -- get TXD from copied object
			setAttr newObj (getAttrIndex "Gta Object" "LOD distance") (getattr this (getattrindex "Gta Object" "LOD distance"))
		)
		append copySet newObj
	)
	select copySet
)