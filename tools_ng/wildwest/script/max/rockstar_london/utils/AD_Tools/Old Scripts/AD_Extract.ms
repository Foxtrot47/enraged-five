--extract face

if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 5) then
(
	subObjectLevel = 4
)
if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 4) then
(
	if $.selectedFaces.count > 0 then
	(
		$.EditablePoly.detachToElement #Face keepOriginal:off
		if $.selectedFaces.count == 1 then
		(
			print ("One face extracted")
		)
		else
		(
			print ($.selectedFaces.count as string + " faces extracted")
		)
	)
	else
	(
		messagebox "Please select a face"
	)
)
else
(
	messagebox "Please select faces on an editable poly object"
)