-- FUNCTION: find indexes of "vertex" snapMode
fn vertexSnapIndex =
(
	for i = 1 to snapmode.numOSnaps do
	( 
		for j = 1 to (snapmode.getOSnapNumItems i) do
			if (snapmode.getOSnapItemName i j) == "Vertex" then
				return [i,j]
	)
)

-- FUNCTION: gets numeric indexes (of scene objects array) of selected objects, and returns them as a bitArray
fn getSelectedIndex =
(
	nodes = #()
	for i  = 1 to objects.count do
	(
		for obj in selection do
			
			if objects[i] ==obj then
			(
				append nodes i 
			)
	)
	return nodes as bitArray
)

-- FUNCTION: gets number of selected vertex
fn pickVertexNum target =
(
	vertNum = 0
	theNode = getNodeByName target
	print theNode
	-- set snapping:
	initSnap = snapMode.active
	initSnapDisplay = snapMode.display
	vertexSnapIndexes = vertexSnapIndex()
	initSnapVertex = snapmode.getOSnapItemActive vertexSnapIndexes[1] vertexSnapIndexes[2]
	if not initSnap then snapMode.active = true
	if not initSnapDisplay then snapMode.display = true
	if not initSnapVertex then snapmode.setOSnapItemActive vertexSnapIndexes[1] vertexSnapIndexes[2] true

	-- let user pick a vertex in viewport
	vertPos = pickPoint prompt:"pick vertex in viewport" snap:#3d
	print vertPos
	for i = 1 to getNumVerts (snapshotasmesh theNode) do (
		if (getVert (snapshotasmesh theNode) i) == vertPos then (
			vertNum = i
			print "got one!"
		)
	)
	if vertNum == 0 then
	(
		vertNum = 1
		print ("Not a vertex in first selected object. Using default #1")
	)
	print ("picked vertex #" + (vertNum as string) )
	
	-- restore user snap settings
	if not initSnap then snapMode.active = false
	if not initSnapDisplay then snapMode.display = false
	if not initSnapVertex then snapmode.setOSnapItemActive vertexSnapIndexes[1] vertexSnapIndexes[2] false
	
	return vertNum
)
	
	-- main FUNCTION - places locator referenced to "parentObject", using "method". optional: specify a vertex number.
	fn placeLocator parentObject method vertNumber = (
		-- Create locator Point Dummy, and name it "locator_#"
		undo "stickLocator " on
		(
			locator=Point()
			locator.name = uniquename "locator"
			-- Change locator's position controller to Position List
			locator.pos.controller = position_List ()
			-- Add Position Script to available Position controller
			locator.pos.available.controller = position_Script ()
			-- Place locator, depending on "method" chosen
			case method of (
				1: (
					-------- OBJECT CENTER --------
					-- Assign script to Position_Script controller: takes position from center of parentObject
					cmdstring = "$" + parentObject.name + ".center"
				)			
				2: (
					-------- BY SPECIFIC VERTEX # --------
					
					--POSITION
					-- Assign script to Position_Script controller: takes position from vertex # of parentObject
					cmdstring = "getVert (snapshotasmesh $" + parentObject.name + ") " + (vertNumber as string)
					-- ROTATION
					-- Change locator's rotation controller to Rotation List
					locator.rotation.controller = rotation_List ()
					-- Add Rotation Script to available Rotation controller
					locator.rotation.available.controller = rotation_Script ()
					-- Assign script to Rotation_Script controller: dependent on vertex normal
					rotCmdstring = "pointVector = [0,0,1]\n"+
									"normal = getNormal (snapshotasmesh $" + parentObject.name + ") " + (vertNumber as string) + "\n"+
									"rightVector= normalize (cross pointVector normal)\n"+
									"upVector = normalize (cross normal rightVector)\n"+
									"print (\"upVector:\" + (upVector as string))\n"+
									"theMatrix = matrix3 rightVector upVector normal [0,0,0]\n"+
									"return ( (theMatrix) as quat)"
					locator.rotation.controller[2].setExpression rotCmdstring
				)
			)
			locator.pos.controller[2].setExpression cmdstring
		)
	)

	-- Define tool rollout
	rollout stickLocator_rollout ("Stick Locator v" + version as string) width:213 height:377
	(
		
		MultiListbox lstTarget "Select reference object:" pos:[13,5] width:187 height:13 items:(for o in objects where not ( (classOf o == Dummy) or (classOf o == Point ) ) collect o.name) selection:#{}
		radiobuttons rdoMethod "Placement method:" pos:[14,225] width:105 height:38 labels:#("obj. center    ", "vertexNum") default:1
		button btnPlaceLocator "Place Locator" pos:[50,320] width:113 height:25
		label lbl_vertNum "#" pos:[105,260] width:53 height:18
		spinner spn_vertNum "" pos:[120,260] width:58 height:17 type:#integer range:[1,99999,1] enabled:false
		button btnPickVertex "pick vertex" pos:[115,280] width:65 height:18 enabled:false
		button btnAbout "about" pos:[75,350] width:65 height:18
		
		on stickLocator_rollout open do (
			lstTarget.selection = getSelectedIndex()
		)
		
		on rdoMethod changed state do (
			if state == 1 then
			(
				spn_vertNum.enabled = false
				btnPickVertex.enabled = false
			)
			else
			(
				spn_vertNum.enabled = true
				btnPickVertex.enabled = true
			)
		)
		
		on btnPickVertex pressed do (
			if (lstTarget.selection as array).count > 0 then
			(
				pickingNode = lstTarget.items[(lstTarget.selection as array)[1]]
				spn_vertNum.value = pickVertexNum(pickingNode)
			)
			else
				messageBox "you have to pick at least one reference object"
		)
		
		on btnAbout pressed do
		(
			messagebox (
				"\"Stick Locator\"\n\n"+
				"Creates a point dummy, and parents it to the reference object's vertex/center, using controller expressions.\n\n"+
				"This is useful to parent other objects to a specific part of a deforming mesh, or an object whose transforms have been 'baked' (e.g. point cache).\n\n"+
				"I have found it specially useful, in FX work, for particle/fluid emitters.\n\n"+
				"Notes:\n"+
				"  - Only the 'center' method can be used with splines.\n"+
				"  - When using 'vertex' method, the dummy's rotation will align to the vertex's normal.\n"+
				"\nVersion: "+version as string+"\n\nWritten by \nMayec Rancel \nhttp://www.mayec.eu"
			)
		)

		on btnPlaceLocator pressed do
		(
			-- Call main "placeLocator" function, then close the dialog
			targetNames = for i in lstTarget.selection collect lstTarget.items[i]
			for targetName in targetNames do
			(
				-- filter splines if method is not "center"
				if rdoMethod.state == 2 and ( classOf (getNodeByName targetName) == SplineShape) then
					print ("skipped " + targetName + " because splines are incompatible with \"vertex\" method ")
				else
				(
					placeLocator (getNodeByName targetName) rdoMethod.state spn_vertNum.value
				)
			)
			destroyDialog stickLocator_rollout
		)
	)
	-- Opens stickLocator dialog
	createDialog stickLocator_rollout