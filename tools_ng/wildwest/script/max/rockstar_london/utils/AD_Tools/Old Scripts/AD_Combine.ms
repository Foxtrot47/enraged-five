-- Maya style combine
objset = for this in selection where (classOf this != XrefObject) collect this
if objset.count < 2 then
(
	messagebox "please select at least two objects"
)
else
(
	masterobj = convertToPoly(objset[objset.count])
	for i = 1 to (objset.count - 1) do
	(
		masterobj.attach objset[i] masterobj
	)
)