--detach

if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 5) then
(
	subObjectLevel = 4
)
if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 4) then
(
	if $.selectedFaces.count > 0 then
	(
		newname = ($.name + "_detached")
		polyop.detachFaces $ $.selectedFaces delete:true asNode:true name:newname
		if $.selectedFaces.count == 1 then
		(
			print ("One face detached")
		)
		else
		(
			print ($.selectedFaces.count as string + " faces detached")
		)
		execute ("select $" + newname)
		subObjectLevel = 4
	)
	else
	(
		messagebox "Please select a face"
	)
)
else
(
	messagebox "Please select faces on an editable poly object"
)