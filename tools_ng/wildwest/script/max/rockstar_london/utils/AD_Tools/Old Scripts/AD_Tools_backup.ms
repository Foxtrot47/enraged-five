--AD_Tools.ms
--Andy Davis �2010 Rockstar London
--Toolbox for creating game artwork

--version 1.1
--new layout and new grey icons

----------------------------------------------------------------------------------------------------------------
--GLOBALS
----------------------------------------------------------------------------------------------------------------
global ADT_Instance
----------------------------------------------------------------------------------------------------------------
--STRUCTURE
----------------------------------------------------------------------------------------------------------------
struct ADT_Struct
(
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI VARIABLES
	------------------------------------------------------------------------------------------------------------------------------------------
	
	ADT_Form,
	ADT_WinLocX = 0,
	ADT_WinLocY = 120,
	ADT_WinWidth = 320,
	ADT_WinHeight = 240,
	ADT_ButtonSize = 32,
	ADT_NumButtonsAcross = 6,
	ADT_IconFolder = "X:/tools/dcc/current/max2009/scripts/Payne/images/AD_Icons/",
	ADT_Iconsize = 32,
	ADT_IniFilePath = "X:/tools/dcc/current/max2009/plugcfg/AD_Tools.ini",
	ADT_StartTab = 0,
	ADT_InfoPanel,
	ADT_TabPanel,
	
	--DOTNET VARIABLES
	Icon32 = dotNetObject "System.Drawing.Size" 32 32,
	CBS_None = (dotNetClass "System.Windows.Forms.TableLayoutPanelCellBorderStyle").None,
	CBS_Single = (dotNetClass "System.Windows.Forms.TableLayoutPanelCellBorderStyle").Single,
	DS_Fill = (dotNetClass "System.Windows.Forms.DockStyle").Fill,
	FBS_FixedToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").FixedToolWindow,
	FBS_SizableToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").SizableToolWindow,
	Font_Calibri = dotNetObject "System.Drawing.Font" "Calibri" 8,
	FS_Flat = (dotNetClass "System.Windows.Forms.FlatStyle").Flat,
	FS_Popup = (dotNetClass "System.Windows.Forms.FlatStyle").Popup,
	SP_Manual =  (dotNetClass "System.Windows.Forms.FormStartPosition").Manual,
	TA_Center =  (dotNetClass "System.Drawing.ContentAlignment").MiddleCenter,
	TA_Left =  (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft,
	XTL_Checkbox = dotnetobject "DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit",
	XTL_Num = dotnetobject "DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit",
	XTL_Textbox = dotnetobject "DevExpress.XtraEditors.Repository.RepositoryItemTextEdit",

	--colours
	RGB_LimeGreen = (dotNetClass "System.Drawing.Color").fromARGB 204 255 0,
	RGB_YellowOrange = (dotNetClass "System.Drawing.Color").fromARGB 255 204 0,
	RGB_Pink = (dotNetClass "System.Drawing.Color").fromARGB 255 0 204,
	RGB_Purple = (dotNetClass "System.Drawing.Color").fromARGB 204 0 255,
	RGB_SkyBlue = (dotNetClass "System.Drawing.Color").fromARGB 0 205 255,
	RGB_Black = (dotNetClass "System.Drawing.Color").black,
	RGB_Charcoal = (dotNetClass "System.Drawing.Color").fromARGB 40 40 40,
	RGB_DarkGrey = (dotNetClass "System.Drawing.Color").fromARGB 102 102	102,
	RGB_MidGrey = (dotNetClass "System.Drawing.Color").fromARGB 153 153 153,
	RGB_PaleGrey = (dotNetClass "System.Drawing.Color").fromARGB 204 204 204,
	RGB_LightGrey = (dotNetClass "System.Drawing.Color").fromARGB 229 229 229,
	RGB_OffWhite = (dotNetClass "System.Drawing.Color").fromARGB 241 241 241,
	RGB_White = (dotNetClass "System.Drawing.Color").white,
	RGB_Red = (dotNetClass "System.Drawing.Color").red,
	RGB_DarkRed = (dotNetClass "System.Drawing.Color").fromARGB 102 0 0,
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--GENERAL FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn ADT_FileInWithArg script argument =
	(
		filein script
		local scriptCommand = getFilenameFile  script
		execute (scriptCommand + " \"" + argument + "\"")
	),
	
	fn ADT_MapImage imageName =
	(
		local iconPath = ADT_IconFolder + imageName
		return (dotNetClass "System.Drawing.Image").FromFile(iconPath)
	),
	
	fn ADT_InitButton =
	(
		local newButton = dotNetObject "Button"
		newButton.Size = dotNetObject "System.Drawing.Size" ADT_ButtonSize ADT_ButtonSize
		newButton.FlatStyle = (dotNetClass "System.Windows.Forms.FlatStyle").Popup
		newButton.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		return newButton
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn UpdateLocalConfigFileSave =
	(
-- 		print ("saving location: [" + ADT_WinLocX as string + ", " + ADT_WinLocY as string + "]")
		setINISetting ADT_IniFilePath "ADTools" "ADT_StartTab" (ADT_StartTab as string)
		setINISetting ADT_IniFilePath "ADTools" "ADT_WinLocX" (ADT_WinLocX as string)
		setINISetting ADT_IniFilePath "ADTools" "ADT_WinLocY" (ADT_WinLocY as string)
		setINISetting ADT_IniFilePath "ADTools" "ADT_WinWidth" (ADT_WinWidth as string)
		setINISetting ADT_IniFilePath "ADTools" "ADT_WinHeight" (ADT_WinHeight as string)
	),
	
	fn UpdateLocalConfigFileLoad =
	(
		try
		(
			ADT_StartTab = getINISetting ADT_IniFilePath "ADTools" "ADT_StartTab" as integer
			ADT_WinLocX = getINISetting ADT_IniFilePath "ADTools" "ADT_WinLocX" as integer
			ADT_WinLocY = getINISetting ADT_IniFilePath "ADTools" "ADT_WinLocY" as integer
			ADT_WinWidth = getINISetting ADT_IniFilePath "ADTools" "ADT_WinWidth" as integer
			ADT_WinHeight = getINISetting ADT_IniFilePath "ADTools" "ADT_WinHeight" as integer
			ADT_ToggleTestState = getINISetting ADT_IniFilePath "ADTools" "ADT_ToggleTestState" as bool
		)
		catch()
		ADT_TabPanel.selectedIndex = ADT_StartTab
		ADT_Form.Location = dotNetObject "system.drawing.point" ADT_WinLocX ADT_WinLocY
		ADT_Form.Size = dotNetObject "System.Drawing.Size" ADT_WinWidth ADT_WinHeight	
	),
	
	fn DispatchLoadIniFile s e =
	(
		ADT_Instance.UpdateLocalConfigFileLoad()
	),
	
	fn DispatchSaveIniFile s e =
	(
		ADT_Instance.UpdateLocalConfigFileSave()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--EVENT HANDLERS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn DispatchTabClicked s e =
	(
		ADT_Instance.TabClicked s e
	),
	
	fn TabClicked s e =
	(
		ADT_StartTab = ADT_TabPanel.selectedIndex
	),
	
	fn DispatchSetWinLoc s e = ADT_Instance.SetWinLoc s e,
	fn SetWinLoc s e =
	(
		ADT_WinLocX = ADT_Form.Location.X
		ADT_WinLocY = ADT_Form.Location.Y
	),

	fn DispatchSetWinSize s e = ADT_Instance.SetWinSize s e,	
	fn SetWinSize s e =
	(
		ADT_WinWidth = ADT_Form.Size.Width
		ADT_WinHeight = ADT_Form.Size.Height
	),
	
	fn AxisConstraintsToggle s e =
	(
		if snapMode.axisConstraint == true then
		(
			snapMode.axisConstraint = false
			ADT_AxisConstraintsButton.BackgroundImage = ADT_Instance.ADT_MapImage "AD_AxisConstraintsOff.bmp"
		)
		else
		(
			snapMode.axisConstraint = true
			ADT_AxisConstraintsButton.BackgroundImage = ADT_Instance.ADT_MapImage "AD_AxisConstraintsOn.bmp"
		)
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------	
	--general
	fn TestScript s e = filein "AD_TestScript.ms",
	fn ToggleOutliner s e = filein "AD_Outliner.ms",
	fn StickLocator s e = filein "ZZ_StickLocator.ms",
	fn Rename s e = filein "AD_Renamer.ms",
	fn UnhideAll = max unhide all,
	fn ToggleShadeSelected s e = max shade selected,
	fn ToggleTest s e = filein "AD_ToggleTest.ms",
	fn PivotMatch s e = filein "AD_PivotMatch.ms",
	fn ToggleXray s e = filein "AD_ToggleXray.ms",
	fn ToggleHelpers = max hide helper toggle,
	
	--geometry
	fn EdgeLoop s e = filein "AD_EdgeLoop.ms",
	fn Zap s e = filein "AD_Zap2.ms",
	fn PivotBase s e = ADT_Instance.ADT_FileInWithArg "AD_SetPivot.ms" "base",
	fn PivotCenter s e = ADT_Instance.ADT_FileInWithArg "AD_SetPivot.ms" "center",
	fn PivotOrigin s e = ADT_Instance.ADT_FileInWithArg "AD_SetPivot.ms" "origin",
	fn MoveToLast s e = filein "AD_MoveToLast.ms",
	fn ToggleBoxMode s e = filein "AD_BoxModeToggle.ms",
	fn XFormReset s e = filein "AD_ResetXForm.ms",
	fn XFormFreeze s e = filein "AD_FreezeXForm.ms",
	fn MoveToOrigin s e = filein "AD_MoveToOrigin.ms",
	fn ConnectEdges s e = filein "AD_Connect.ms",
	fn Combine s e = filein "AD_Combine.ms",
		
	------------------------------------------------------------------------------------------------------------------------------------------	
	--uv-mapping
	fn TestTexture s e = filein "AD_TestTexture.ms",
	fn MaxUVTransform s e = filein "AD_UVTransform.ms",
	fn BoxMap = filein "AD_BoxMap.ms",
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI
	------------------------------------------------------------------------------------------------------------------------------------------
	fn CreateUI =
	(		
		-- form setup
		ADT_Form = dotNetObject "maxCustomControls.maxForm"
		ADT_Form.MaximumSize = dotNetObject "System.Drawing.Size" 1600 1280
		ADT_Form.MinimumSize = dotNetObject "System.Drawing.Size" 220 138
		ADT_Form.Size = dotNetObject "System.Drawing.Size" ADT_WinWidth ADT_WinHeight	
		ADT_Form.Text = "AD Tools 1.1"
		ADT_Form.startPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").manual
		ADT_Form.FormBorderStyle = FBS_SizableToolWindow
		dotNet.AddEventHandler ADT_Form "Load" DispatchLoadIniFile
		dotNet.AddEventHandler ADT_Form "Closing" DispatchSaveIniFile
		dotNet.AddEventHandler ADT_Form "Move" DispatchSetWinLoc
		dotNet.AddEventHandler ADT_Form "Resize" DispatchSetWinSize
		
		ADT_Table = dotNetObject "TableLayoutPanel"
		ADT_Table.Dock = DS_Fill
		ADT_Form.Controls.Add ADT_Table
		ADT_Table.RowCount = 3
		ADT_Table.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		ADT_Table.rowStyles.add (RS_dotNetObject.rowStyleObject "percent" 100)
		ADT_Table.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		ADT_Form.Controls.Add ADT_Table
		
		------------------------------------------------------------------------------------------------------------------------------------------
		--MENU PANEL
		------------------------------------------------------------------------------------------------------------------------------------------
		
		AD_ToolsMenu = dotNetObject "MenuStrip"
		AD_ToolsMenu.Font = Font_Calibri
		AD_ToolsMenu.BackColor = RGB_Charcoal
		AD_ToolsMenu.ForeColor = RGB_White
		AD_ToolsMenu.BackgroundImage = ADT_MapImage "UberIcons_MenuGraphic.png"
		ADT_Table.Controls.Add AD_ToolsMenu 0 0
		AD_ToolsFileMenu = dotNetObject "ToolStripMenuItem"
		AD_ToolsFileMenu.Text = "File"
		AD_ToolsMenu.Items.Add AD_ToolsFileMenu
		AD_ToolsOptionsMenu = dotNetObject "ToolStripMenuItem"
		AD_ToolsOptionsMenu.Text = "Options"
		AD_ToolsMenu.Items.Add AD_ToolsOptionsMenu
		AD_ToolsHelpMenu = dotNetObject "ToolStripMenuItem"
		AD_ToolsHelpMenu.Text = "Help"
		AD_ToolsMenu.Items.Add AD_ToolsHelpMenu
		
		ADT_ToolTip = dotnetobject "ToolTip"
		
		ADT_TabPanel = dotNetObject "System.Windows.Forms.TabControl"
		ADT_TabPanel.Dock = DS_Fill
		ADT_Table.Controls.Add ADT_TabPanel 0 1
		ADT_Tab1 = dotNetObject "System.Windows.Forms.TabPage"
		ADT_Tab2 = dotNetObject "System.Windows.Forms.TabPage"
		ADT_Tab3 = dotNetObject "System.Windows.Forms.TabPage"
		ADT_Tab1.text = "General"
		ADT_Tab2.text = "Geometry"
		ADT_Tab3.text = "UV-Mapping"
		ADT_TabPanel.Controls.Add ADT_Tab1
		ADT_TabPanel.Controls.Add ADT_Tab2
		ADT_TabPanel.Controls.Add ADT_Tab3
		dotNet.AddEventHandler ADT_TabPanel "SelectedIndexChanged" DispatchTabClicked
		
		------------------------------------------------------------------------------------------------------------------------------------------
		--GENERAL PANEL
		------------------------------------------------------------------------------------------------------------------------------------------
		ADT_GeneralPanel = dotNetObject "System.Windows.Forms.FlowLayoutPanel"
		ADT_GeneralPanel.Dock = DS_Fill
		ADT_GeneralPanel.BackColor = RGB_Charcoal
		ADT_Tab1.Controls.Add ADT_GeneralPanel
		
		ADT_genbutton1 = ADT_InitButton()
		ADT_genbutton1.BackgroundImage = ADT_MapImage "AD_PivotToOrigin.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton1 "Set pivot to origin"
		dotNet.AddEventHandler ADT_genbutton1 "Click" PivotOrigin
		ADT_GeneralPanel.Controls.Add ADT_genbutton1
		
		ADT_genbutton2 = ADT_InitButton()
		ADT_genbutton2.BackgroundImage = ADT_MapImage "AD_PivotToBase.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton2 "Set pivot to base"
		dotNet.AddEventHandler ADT_genbutton2 "Click" PivotBase
		ADT_GeneralPanel.Controls.Add ADT_genbutton2
		
		ADT_genbutton3 = ADT_InitButton()
		ADT_genbutton3.BackgroundImage = ADT_MapImage "AD_PivotToCenter.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton3 "Set pivot to center"
		dotNet.AddEventHandler ADT_genbutton3 "Click" PivotCenter
		ADT_GeneralPanel.Controls.Add ADT_genbutton3
		
		ADT_genbutton4 = ADT_InitButton()
		ADT_genbutton4.BackgroundImage = ADT_MapImage "AD_PivotMatch.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton4 "Pivot match"
		dotNet.AddEventHandler ADT_genbutton4 "Click" PivotMatch
		ADT_GeneralPanel.Controls.Add ADT_genbutton4
		
		ADT_genbutton5 = ADT_InitButton()
		ADT_genbutton5.BackgroundImage = ADT_MapImage "AD_MoveToLast.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton5 "Move objects to the last selected object"
		dotNet.AddEventHandler ADT_genbutton5 "Click" MoveToLast
		ADT_GeneralPanel.Controls.Add ADT_genbutton5
		
		ADT_genbutton6 = ADT_InitButton()
		ADT_genbutton6.BackgroundImage = ADT_MapImage "AD_ResetXForm.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton6 "Reset transform"
		dotNet.AddEventHandler ADT_genbutton6 "Click" XFormReset
		ADT_GeneralPanel.Controls.Add ADT_genbutton6
		
		ADT_genbutton7 = ADT_InitButton()
		ADT_genbutton7.BackgroundImage = ADT_MapImage "AD_Xray.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton7 "Toggle Xray"
		dotNet.AddEventHandler ADT_genbutton7 "Click" ToggleXray
		ADT_GeneralPanel.Controls.Add ADT_genbutton7
		
		ADT_genbutton8 = ADT_InitButton()
		ADT_genbutton8.BackgroundImage = ADT_MapImage "AD_Renamer.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton8 "Rename objects"
		dotNet.AddEventHandler ADT_genbutton8 "Click" Rename
		ADT_GeneralPanel.Controls.Add ADT_genbutton8
		
		ADT_genbutton9 = ADT_InitButton()
		ADT_genbutton9.BackgroundImage = ADT_MapImage "AD_UnhideAll.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton9 "Unhide all"
		dotNet.AddEventHandler ADT_genbutton9 "Click" UnhideAll
		ADT_GeneralPanel.Controls.Add ADT_genbutton9
		
		ADT_genbutton10 = ADT_InitButton()
		ADT_genbutton10.BackgroundImage = ADT_MapImage "AD_TestScript.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton10 "Evaluate test script"
		dotNet.AddEventHandler ADT_genbutton10 "Click" TestScript
		ADT_GeneralPanel.Controls.Add ADT_genbutton10
		
		ADT_genbutton11 = ADT_InitButton()
		ADT_genbutton11.BackgroundImage = ADT_MapImage "AD_BoxModeToggle.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton11 "Toggle box mode"
		dotNet.AddEventHandler ADT_genbutton11 "Click" ToggleBoxMode
		ADT_GeneralPanel.Controls.Add ADT_genbutton11
		
		ADT_genbutton12 = ADT_InitButton()
		ADT_genbutton12.BackgroundImage = ADT_MapImage "AD_MoveToOrigin.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton12 "Move to origin"
		dotNet.AddEventHandler ADT_genbutton12 "Click" MoveToOrigin
		ADT_GeneralPanel.Controls.Add ADT_genbutton12
		
		ADT_genbutton13 = ADT_InitButton()
		ADT_genbutton13.BackgroundImage = ADT_MapImage "AD_Outliner.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton13 "Toggle outliner"
		dotNet.AddEventHandler ADT_genbutton13 "Click" ToggleOutliner
		ADT_GeneralPanel.Controls.Add ADT_genbutton13
		
		ADT_genbutton14 = ADT_InitButton()
		ADT_genbutton14.BackgroundImage = ADT_MapImage "AD_ShadeSelected.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton14 "Toggle shade selected"
		dotNet.AddEventHandler ADT_genbutton14 "Click" ToggleShadeSelected
		ADT_GeneralPanel.Controls.Add ADT_genbutton14
		
		global ADT_genbutton15 = ADT_InitButton()
		if ADT_ToggleTestState == true then
		(
			ADT_genbutton15.BackgroundImage = ADT_MapImage "AD_ToggleOn.bmp"
		)
		else
		(
			ADT_genbutton15.BackgroundImage = ADT_MapImage "AD_ToggleOff.bmp"
		)
		ADT_ToolTip.SetToolTip ADT_genbutton15 "Toggle test"
		dotNet.AddEventHandler ADT_genbutton15 "Click" ToggleTest
		ADT_GeneralPanel.Controls.Add ADT_genbutton15
		
		ADT_genbutton16 = ADT_InitButton()
		ADT_genbutton16.BackgroundImage = ADT_MapImage "AD_Locator.bmp"
		ADT_ToolTip.SetToolTip ADT_genbutton16 "Place Locator"
		dotNet.AddEventHandler ADT_genbutton16 "Click" StickLocator
		ADT_GeneralPanel.Controls.Add ADT_genbutton16
		
		ADT_ToggleHelpersButton = ADT_InitButton()
		ADT_ToggleHelpersButton.BackgroundImage = ADT_MapImage "AD_ToggleHelpers.bmp"
		ADT_ToolTip.SetToolTip ADT_ToggleHelpersButton "Toggle helpers"
		dotNet.AddEventHandler ADT_ToggleHelpersButton "Click" ToggleHelpers
		ADT_GeneralPanel.Controls.Add ADT_ToggleHelpersButton

		------------------------------------------------------------------------------------------------------------------------------------------
		--GEOMETRY PANEL
		------------------------------------------------------------------------------------------------------------------------------------------
		ADT_GeometryPanel = dotNetObject "System.Windows.Forms.FlowLayoutPanel"
		ADT_GeometryPanel.Dock = DS_Fill
		ADT_GeometryPanel.BackColor = RGB_Charcoal
		ADT_Tab2.Controls.Add ADT_GeometryPanel
		
		ADT_geobutton1 = ADT_InitButton()
		ADT_geobutton1.BackgroundImage = ADT_MapImage "AD_EdgeLoop.bmp"
		ADT_ToolTip.SetToolTip ADT_geobutton1 "Connect edges"
		dotNet.AddEventHandler ADT_geobutton1 "Click" EdgeLoop
		ADT_GeometryPanel.Controls.Add ADT_geobutton1
		
		ADT_geobutton2 = ADT_InitButton()
		ADT_geobutton2.BackgroundImage = ADT_MapImage "AD_Connect.bmp"
		ADT_ToolTip.SetToolTip ADT_geobutton2 "Connect edges"
		dotNet.AddEventHandler ADT_geobutton2 "Click" ConnectEdges
		ADT_GeometryPanel.Controls.Add ADT_geobutton2
		
		ADT_geobutton3 = ADT_InitButton()
		ADT_geobutton3.BackgroundImage = ADT_MapImage "AD_Combine.bmp"
		ADT_ToolTip.SetToolTip ADT_geobutton3 "Combine geometry"
		dotNet.AddEventHandler ADT_geobutton3 "Click" Combine
		ADT_GeometryPanel.Controls.Add ADT_geobutton3
		
		ADT_geobutton4 = ADT_InitButton()
		ADT_geobutton4.BackgroundImage = ADT_MapImage "AD_Zap.bmp"
		ADT_ToolTip.SetToolTip ADT_geobutton4 "Delete edge loop"
		dotNet.AddEventHandler ADT_geobutton4 "Click" Zap
		ADT_GeometryPanel.Controls.Add ADT_geobutton4
		
		------------------------------------------------------------------------------------------------------------------------------------------
		--UV PANEL
		------------------------------------------------------------------------------------------------------------------------------------------
		ADT_UVPanel = dotNetObject "System.Windows.Forms.FlowLayoutPanel"
		ADT_UVPanel.Dock = DS_Fill
		ADT_UVPanel.BackColor = RGB_Charcoal
		ADT_Tab3.Controls.Add ADT_UVPanel
		
		ADT_uvbutton1 = ADT_InitButton()
		ADT_uvbutton1.BackgroundImage = ADT_MapImage "AD_PreserveUVs.bmp"
		ADT_ToolTip.SetToolTip ADT_uvbutton1 "Toggle preserve UVs"
-- 		dotNet.AddEventHandler ADT_uvbutton1 "Click" PreserveUVs
		ADT_UVPanel.Controls.Add ADT_uvbutton1
		
		ADT_uvbutton2 = ADT_InitButton()
		ADT_uvbutton2.BackgroundImage = ADT_MapImage "AD_TestShader.bmp"
		ADT_ToolTip.SetToolTip ADT_uvbutton2 "Apply test texture"
-- 		dotNet.AddEventHandler ADT_uvbutton2 "Click" TestTexture
		ADT_UVPanel.Controls.Add ADT_uvbutton2
		
		ADT_uvbutton3 = ADT_InitButton()
		ADT_uvbutton3.BackgroundImage = ADT_MapImage "AD_UVTransform.bmp"
		ADT_ToolTip.SetToolTip ADT_uvbutton3 "Transform UVs modifier"
		dotNet.AddEventHandler ADT_uvbutton3 "Click" MaxUVTransform
		ADT_UVPanel.Controls.Add ADT_uvbutton3
		
		ADT_BoxMapButton = ADT_InitButton()
		ADT_BoxMapButton.BackgroundImage = ADT_MapImage "AD_BoxMap.bmp"
		ADT_ToolTip.SetToolTip ADT_BoxMapButton "Box map"
		dotNet.AddEventHandler ADT_BoxMapButton "Click" BoxMap
		ADT_UVPanel.Controls.Add ADT_BoxMapButton
		
		
		------------------------------------------------------------------------------------------------------------------------------------------
		--INFO PANEL
		------------------------------------------------------------------------------------------------------------------------------------------
		ADT_InfoPanel = dotNetObject "Label"
		ADT_InfoPanel.Dock = DS_Fill
		ADT_InfoPanel.Text = "Welcome to AD_Tools"
		ADT_InfoPanel.TextAlign = TA_Left
		ADT_InfoPanel.Font = Font_Calibri
		ADT_Table.Controls.Add ADT_InfoPanel
		------------------------------------------------------------------------------------------------------------------------------------------
		--DRAW FORM
		------------------------------------------------------------------------------------------------------------------------------------------
		ADT_Form.showModeless()
		ADT_Form
	)
)
----------------------------------------------------------------------------------------------------------------
--RUN TOOL COMMANDS
----------------------------------------------------------------------------------------------------------------
if ADT_Instance != undefined then
(
	ADT_Instance.ADT_Form.close()
)
ADT_Instance = ADT_Struct()
ADT_Instance.CreateUI()