--AD_Tools.ms
--Andy Davis �2010 Rockstar London
--Toolbox for creating game artwork
----------------------------------------------------------------------------------------------------------------
--GLOBALS
----------------------------------------------------------------------------------------------------------------
global AD_ToolsInstance
----------------------------------------------------------------------------------------------------------------
--STRUCTURE
----------------------------------------------------------------------------------------------------------------
struct AD_ToolsStruct
(
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI VARIABLES	------------------------------------------------------------------------------------------------------------------------------------------
	
	AD_ToolsForm,
	winLocX = 0,
	winLocY = 120,
	winWidth = 320,
	winHeight = 240,
	buttonSize = 32,
	numButtonsAcross = 6,
	CBS_None = (dotNetClass "System.Windows.Forms.TableLayoutPanelCellBorderStyle").None,
	CBS_Single = (dotNetClass "System.Windows.Forms.TableLayoutPanelCellBorderStyle").Single,
	DS_Fill = (dotNetClass "System.Windows.Forms.DockStyle").Fill,
	FBS_FixedToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").FixedToolWindow,
	FBS_SizableToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").SizableToolWindow,
	Font_Calibri = dotNetObject "System.Drawing.Font" "Calibri" 8,
	FS_Flat = (dotNetClass "System.Windows.Forms.FlatStyle").Flat,
	FS_Popup = (dotNetClass "System.Windows.Forms.FlatStyle").Popup,
	SP_Manual =  (dotNetClass "System.Windows.Forms.FormStartPosition").Manual,
	TA_Center =  (dotNetClass "System.Drawing.ContentAlignment").MiddleCenter,
	TA_Left =  (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft,
	XTL_Checkbox = dotnetobject "DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit",
	XTL_Num = dotnetobject "DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit",
	XTL_Textbox = dotnetobject "DevExpress.XtraEditors.Repository.RepositoryItemTextEdit",
	iconFolder = "X:\\tools\\dcc\\current\\max2009\\scripts\\Payne\\images\\AD_Icons\\",
	iconsize = 32,
	Icon32 = dotNetObject "System.Drawing.Size" 32 32,
-- 	iniFilePath = "X:\\tools\\dcc\\current\\max2009\\plugcfg\\AD_Tools.ini",
	iniFilePath = "X:/tools/dcc/current/max2009/plugcfg/AD_Tools.ini",
	startTab = 0,
	infoPanel,
	tabPanel,
	
	--colours
	RGB_LimeGreen = (dotNetClass "System.Drawing.Color").fromARGB 204 255 0,
	RGB_YellowOrange = (dotNetClass "System.Drawing.Color").fromARGB 255 204 0,
	RGB_Pink = (dotNetClass "System.Drawing.Color").fromARGB 255 0 204,
	RGB_Purple = (dotNetClass "System.Drawing.Color").fromARGB 204 0 255,
	RGB_SkyBlue = (dotNetClass "System.Drawing.Color").fromARGB 0 205 255,
	RGB_Black = (dotNetClass "System.Drawing.Color").black,
	RGB_Charcoal = (dotNetClass "System.Drawing.Color").fromARGB 40 40 40,
	RGB_DarkGrey = (dotNetClass "System.Drawing.Color").fromARGB 102 102	102,
	RGB_MidGrey = (dotNetClass "System.Drawing.Color").fromARGB 153 153 153,
	RGB_PaleGrey = (dotNetClass "System.Drawing.Color").fromARGB 204 204 204,
	RGB_LightGrey = (dotNetClass "System.Drawing.Color").fromARGB 229 229 229,
	RGB_OffWhite = (dotNetClass "System.Drawing.Color").fromARGB 241 241 241,
	RGB_White = (dotNetClass "System.Drawing.Color").white,
	RGB_Red = (dotNetClass "System.Drawing.Color").red,
	RGB_DarkRed = (dotNetClass "System.Drawing.Color").fromARGB 102 0 0,
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--GENERAL FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn FileInWithArg script argument =
	(
		filein script
		local scriptCommand = getFilenameFile  script
		execute (scriptCommand + " \"" + argument + "\"")
	),
	
	fn MapImage imageName =
	(
		local iconPath = iconFolder + imageName
		return (dotNetClass "System.Drawing.Image").FromFile(iconPath)
	),
	
	fn InitButton =
	(
		local newButton = dotNetObject "Button"
		newButton.Size = dotNetObject "System.Drawing.Size" buttonSize buttonSize
		newButton.FlatStyle = (dotNetClass "System.Windows.Forms.FlatStyle").Popup
		newButton.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		return newButton
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn UpdateLocalConfigFileSave =
	(
-- 		print ("saving location: [" + winLocX as string + ", " + winLocY as string + "]")
		setINISetting iniFilePath "ADTools" "startTab" (startTab as string)
		setINISetting iniFilePath "ADTools" "winLocX" (winLocX as string)
		setINISetting iniFilePath "ADTools" "winLocY" (winLocY as string)
		setINISetting iniFilePath "ADTools" "winWidth" (winWidth as string)
		setINISetting iniFilePath "ADTools" "winHeight" (winHeight as string)
	),
	
	fn UpdateLocalConfigFileLoad =
	(
		try
		(
			startTab = getINISetting iniFilePath "ADTools" "startTab" as integer
			winLocX = getINISetting iniFilePath "ADTools" "winLocX" as integer
			winLocY = getINISetting iniFilePath "ADTools" "winLocY" as integer
			winWidth = getINISetting iniFilePath "ADTools" "winWidth" as integer
			winHeight = getINISetting iniFilePath "ADTools" "winHeight" as integer
			toggleTestState = getINISetting iniFilePath "ADTools" "toggleTestState" as bool
		)
		catch()
		tabPanel.selectedIndex = startTab
		AD_ToolsForm.Location = dotNetObject "system.drawing.point" winLocX winLocY
		AD_ToolsForm.Size = dotNetObject "System.Drawing.Size" winWidth winHeight	
	),
	
	fn DispatchLoadIniFile s e =
	(
		AD_ToolsInstance.UpdateLocalConfigFileLoad()
	),
	
	fn DispatchSaveIniFile s e =
	(
		AD_ToolsInstance.UpdateLocalConfigFileSave()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--EVENT HANDLERS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn DispatchTabClicked s e =
	(
		AD_ToolsInstance.TabClicked s e
	),
	
	fn TabClicked s e =
	(
		startTab = tabPanel.selectedIndex
	),
	
	fn DispatchSetWinLoc s e = AD_ToolsInstance.SetWinLoc s e,
	fn SetWinLoc s e =
	(
		winLocX = AD_ToolsForm.Location.X
		winLocY = AD_ToolsForm.Location.Y
	),

	fn DispatchSetWinSize s e = AD_ToolsInstance.SetWinSize s e,	
	fn SetWinSize s e =
	(
		winWidth = AD_ToolsForm.Size.Width
		winHeight = AD_ToolsForm.Size.Height
	),
	
	fn AxisConstraintsToggle s e =
	(
		if snapMode.axisConstraint == true then
		(
			snapMode.axisConstraint = false
			axisConstraintsButton.BackgroundImage = AD_ToolsInstance.MapImage "AD_AxisConstraintsOff.bmp"
		)
		else
		(
			snapMode.axisConstraint = true
			axisConstraintsButton.BackgroundImage = AD_ToolsInstance.MapImage "AD_AxisConstraintsOn.bmp"
		)
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------	
	--general
	fn TestScript s e = filein "AD_TestScript.ms",
	fn ToggleOutliner s e = filein "AD_Outliner.ms",
	fn StickLocator s e = filein "ZZ_StickLocator.ms",
	fn Rename s e = filein "AD_Renamer.ms",
	fn UnhideAll s e = max unhide all,
	fn ToggleShadeSelected s e = max shade selected,
	fn ToggleTest s e = filein "AD_ToggleTest.ms",
	fn PivotMatch s e = filein "AD_PivotMatch.ms",
	fn ToggleXray s e = filein "AD_ToggleXray.ms",
	
	--geometry
	fn EdgeLoop s e = filein "AD_EdgeLoop.ms",
	fn Zap s e = filein "AD_Zap2.ms",
	fn PivotBase s e = AD_ToolsInstance.FileInWithArg "AD_SetPivot.ms" "base",
	fn PivotCenter s e = AD_ToolsInstance.FileInWithArg "AD_SetPivot.ms" "center",
	fn PivotOrigin s e = AD_ToolsInstance.FileInWithArg "AD_SetPivot.ms" "origin",
	fn MoveToLast s e = filein "AD_MoveToLast.ms",
	fn ToggleBoxMode s e = filein "AD_BoxModeToggle.ms",
	fn XFormReset s e = filein "AD_ResetXForm.ms",
	fn XFormFreeze s e = filein "AD_FreezeXForm.ms",
	fn MoveToOrigin s e = filein "AD_MoveToOrigin.ms",
	fn ConnectEdges s e = filein "AD_Connect.ms",
	------------------------------------------------------------------------------------------------------------------------------------------	
	--uv-mapping
	fn TestTexture s e = filein "AD_TestTexture.ms",
	fn MaxUVTransform s e = filein "AD_UVTransform.ms",
	fn BoxMap = filein "AD_BoxMap.ms",
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI
	------------------------------------------------------------------------------------------------------------------------------------------
	fn CreateUI =
	(		
		-- form setup
		AD_ToolsForm = dotNetObject "maxCustomControls.maxForm"
		AD_ToolsForm.MaximumSize = dotNetObject "System.Drawing.Size" 1600 1280
		AD_ToolsForm.MinimumSize = dotNetObject "System.Drawing.Size" 220 138
		AD_ToolsForm.Size = dotNetObject "System.Drawing.Size" winWidth winHeight	
		AD_ToolsForm.Text = "AD Tools 1.1"
		AD_ToolsForm.startPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").manual
		AD_ToolsForm.FormBorderStyle = FBS_SizableToolWindow
		dotNet.AddEventHandler AD_ToolsForm "Load" DispatchLoadIniFile
		dotNet.AddEventHandler AD_ToolsForm "Closing" DispatchSaveIniFile
		dotNet.AddEventHandler AD_ToolsForm "Move" DispatchSetWinLoc
		dotNet.AddEventHandler AD_ToolsForm "Resize" DispatchSetWinSize
		
		AD_ToolsTable = dotNetObject "TableLayoutPanel"
		AD_ToolsTable.Dock = DS_Fill
		AD_ToolsForm.Controls.Add AD_ToolsTable
		AD_ToolsTable.RowCount = 3
		AD_ToolsTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		AD_ToolsTable.rowStyles.add (RS_dotNetObject.rowStyleObject "percent" 100)
		AD_ToolsTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		AD_ToolsForm.Controls.Add AD_ToolsTable
		
		------------------------------------------------------------------------------------------------------------------------------------------
		--MENU PANEL
		------------------------------------------------------------------------------------------------------------------------------------------
		
		AD_ToolsMenu = dotNetObject "MenuStrip"
		AD_ToolsMenu.Font = Font_Calibri
		AD_ToolsMenu.BackColor = RGB_Charcoal
		AD_ToolsMenu.ForeColor = RGB_White
		AD_ToolsMenu.BackgroundImage = MapImage "UberIcons_MenuGraphic.png"
		AD_ToolsTable.Controls.Add AD_ToolsMenu 0 0
		AD_ToolsFileMenu = dotNetObject "ToolStripMenuItem"
		AD_ToolsFileMenu.Text = "File"
		AD_ToolsMenu.Items.Add AD_ToolsFileMenu
		AD_ToolsOptionsMenu = dotNetObject "ToolStripMenuItem"
		AD_ToolsOptionsMenu.Text = "Options"
		AD_ToolsMenu.Items.Add AD_ToolsOptionsMenu
		AD_ToolsHelpMenu = dotNetObject "ToolStripMenuItem"
		AD_ToolsHelpMenu.Text = "Help"
		AD_ToolsMenu.Items.Add AD_ToolsHelpMenu
		
		AD_ToolsToolTip = dotnetobject "ToolTip"
		
		tabPanel = dotNetObject "System.Windows.Forms.TabControl"
		tabPanel.Dock = DS_Fill
		AD_ToolsTable.Controls.Add tabPanel 0 1
		tab1 = dotNetObject "System.Windows.Forms.TabPage"
		tab2 = dotNetObject "System.Windows.Forms.TabPage"
		tab3 = dotNetObject "System.Windows.Forms.TabPage"
		tab1.text = "General"
		tab2.text = "Geometry"
		tab3.text = "UV-Mapping"
		tabPanel.Controls.Add tab1
		tabPanel.Controls.Add tab2
		tabPanel.Controls.Add tab3
		dotNet.AddEventHandler tabPanel "SelectedIndexChanged" DispatchTabClicked
		
		------------------------------------------------------------------------------------------------------------------------------------------
		--GENERAL PANEL
		------------------------------------------------------------------------------------------------------------------------------------------
		generalPanel = dotNetObject "System.Windows.Forms.FlowLayoutPanel"
		generalPanel.Dock = DS_Fill
		generalPanel.BackColor = RGB_Charcoal
		tab1.Controls.Add generalPanel
		
		genbutton1 = InitButton()
		genbutton1.BackgroundImage = MapImage "AD_PivotToOrigin.bmp"
		AD_ToolsToolTip.SetToolTip genbutton1 "Set pivot to origin"
		dotNet.AddEventHandler genbutton1 "Click" PivotOrigin
		generalPanel.Controls.Add genbutton1
		
		genbutton2 = InitButton()
		genbutton2.BackgroundImage = MapImage "AD_PivotToBase.bmp"
		AD_ToolsToolTip.SetToolTip genbutton2 "Set pivot to base"
		dotNet.AddEventHandler genbutton2 "Click" PivotBase
		generalPanel.Controls.Add genbutton2
		
		genbutton3 = InitButton()
		genbutton3.BackgroundImage = MapImage "AD_PivotToCenter.bmp"
		AD_ToolsToolTip.SetToolTip genbutton3 "Set pivot to center"
		dotNet.AddEventHandler genbutton3 "Click" PivotCenter
		generalPanel.Controls.Add genbutton3
		
		genbutton4 = InitButton()
		genbutton4.BackgroundImage = MapImage "AD_PivotMatch.bmp"
		AD_ToolsToolTip.SetToolTip genbutton4 "Pivot match"
		dotNet.AddEventHandler genbutton4 "Click" PivotMatch
		generalPanel.Controls.Add genbutton4
		
		genbutton5 = InitButton()
		genbutton5.BackgroundImage = MapImage "AD_MoveToLast.bmp"
		AD_ToolsToolTip.SetToolTip genbutton5 "Move objects to the last selected object"
		dotNet.AddEventHandler genbutton5 "Click" MoveToLast
		generalPanel.Controls.Add genbutton5
		
		genbutton6 = InitButton()
		genbutton6.BackgroundImage = MapImage "AD_ResetXForm.bmp"
		AD_ToolsToolTip.SetToolTip genbutton6 "Reset transform"
		dotNet.AddEventHandler genbutton6 "Click" XFormReset
		generalPanel.Controls.Add genbutton6
		
		genButton7 = InitButton()
		genButton7.BackgroundImage = MapImage "AD_Xray.bmp"
		AD_ToolsToolTip.SetToolTip genButton7 "Toggle Xray"
		dotNet.AddEventHandler genButton7 "Click" ToggleXray
		generalPanel.Controls.Add genButton7
		
		genButton8 = InitButton()
		genButton8.BackgroundImage = MapImage "AD_Renamer.bmp"
		AD_ToolsToolTip.SetToolTip genButton8 "Rename objects"
		dotNet.AddEventHandler genButton8 "Click" Rename
		generalPanel.Controls.Add genButton8
		
		genButton9 = InitButton()
		genButton9.BackgroundImage = MapImage "AD_UnhideAll.bmp"
		AD_ToolsToolTip.SetToolTip genButton9 "Unhide all"
		dotNet.AddEventHandler genButton9 "Click" UnhideAll
		generalPanel.Controls.Add genButton9
		
		genbutton10 = InitButton()
		genbutton10.BackgroundImage = MapImage "AD_TestScript.bmp"
		AD_ToolsToolTip.SetToolTip genbutton10 "Evaluate test script"
		dotNet.AddEventHandler genbutton10 "Click" TestScript
		generalPanel.Controls.Add genbutton10
		
		genbutton11 = InitButton()
		genbutton11.BackgroundImage = MapImage "AD_BoxModeToggle.bmp"
		AD_ToolsToolTip.SetToolTip genbutton11 "Toggle box mode"
		dotNet.AddEventHandler genbutton11 "Click" ToggleBoxMode
		generalPanel.Controls.Add genbutton11
		
		genbutton12 = InitButton()
		genbutton12.BackgroundImage = MapImage "AD_MoveToOrigin.bmp"
		AD_ToolsToolTip.SetToolTip genbutton12 "Move to origin"
		dotNet.AddEventHandler genbutton12 "Click" MoveToOrigin
		generalPanel.Controls.Add genbutton12
		
		genbutton13 = InitButton()
		genbutton13.BackgroundImage = MapImage "AD_Outliner.bmp"
		AD_ToolsToolTip.SetToolTip genbutton13 "Toggle outliner"
		dotNet.AddEventHandler genbutton13 "Click" ToggleOutliner
		generalPanel.Controls.Add genbutton13
		
		genbutton14 = InitButton()
		genbutton14.BackgroundImage = MapImage "AD_ShadeSelected.bmp"
		AD_ToolsToolTip.SetToolTip genbutton14 "Toggle shade selected"
		dotNet.AddEventHandler genbutton14 "Click" ToggleShadeSelected
		generalPanel.Controls.Add genbutton14
		
		global genbutton15 = InitButton()
		if toggleTestState == true then
		(
			genbutton15.BackgroundImage = MapImage "AD_ToggleOn.bmp"
		)
		else
		(
			genbutton15.BackgroundImage = MapImage "AD_ToggleOff.bmp"
		)
		AD_ToolsToolTip.SetToolTip genbutton15 "Toggle test"
		dotNet.AddEventHandler genbutton15 "Click" ToggleTest
		generalPanel.Controls.Add genbutton15
		
		genButton16 = InitButton()
		genButton16.BackgroundImage = MapImage "AD_Locator.bmp"
		AD_ToolsToolTip.SetToolTip genButton16 "Place Locator"
		dotNet.AddEventHandler genButton16 "Click" StickLocator
		generalPanel.Controls.Add genButton16
		
		global axisConstraintsButton = InitButton()
		if snapMode.axisConstraint == true then
		(
			axisConstraintsButton.BackgroundImage = MapImage "AD_AxisConstraintsOn.bmp"
		)
		else
		(
			axisConstraintsButton.BackgroundImage = MapImage "AD_AxisConstraintsOff.bmp"
		)
		AD_ToolsToolTip.SetToolTip axisConstraintsButton "Toggle Axis Constraints"
		dotNet.AddEventHandler axisConstraintsButton "Click" AxisConstraintsToggle
		generalPanel.Controls.Add axisConstraintsButton

		------------------------------------------------------------------------------------------------------------------------------------------
		--GEOMETRY PANEL
		------------------------------------------------------------------------------------------------------------------------------------------
		geometryPanel = dotNetObject "System.Windows.Forms.FlowLayoutPanel"
		geometryPanel.Dock = DS_Fill
		geometryPanel.BackColor = RGB_Charcoal
		tab2.Controls.Add geometryPanel
		
		geobutton1 = InitButton()
		geobutton1.BackgroundImage = MapImage "AD_EdgeLoop.bmp"
		AD_ToolsToolTip.SetToolTip geobutton1 "Connect edges"
		dotNet.AddEventHandler geobutton1 "Click" EdgeLoop
		geometryPanel.Controls.Add geobutton1
		
		geobutton2 = InitButton()
		geobutton2.BackgroundImage = MapImage "AD_Connect.bmp"
		AD_ToolsToolTip.SetToolTip geobutton2 "Connect edges"
		dotNet.AddEventHandler geobutton2 "Click" ConnectEdges
		geometryPanel.Controls.Add geobutton2
		
		geobutton3 = InitButton()
		geobutton3.BackgroundImage = MapImage "AD_Combine.bmp"
		AD_ToolsToolTip.SetToolTip geobutton3 "Combine geometry"
-- 		dotNet.AddEventHandler geobutton3 "Click" Combine
		geometryPanel.Controls.Add geobutton3
		
		geobutton4 = InitButton()
		geobutton4.BackgroundImage = MapImage "AD_Zap.bmp"
		AD_ToolsToolTip.SetToolTip geobutton4 "Delete edge loop"
		dotNet.AddEventHandler geobutton4 "Click" Zap
		geometryPanel.Controls.Add geobutton4
		
		------------------------------------------------------------------------------------------------------------------------------------------
		--UV PANEL
		------------------------------------------------------------------------------------------------------------------------------------------
		uvPanel = dotNetObject "System.Windows.Forms.FlowLayoutPanel"
		uvPanel.Dock = DS_Fill
		uvPanel.BackColor = RGB_Charcoal
		tab3.Controls.Add uvPanel
		
		uvButton1 = InitButton()
		uvButton1.BackgroundImage = MapImage "AD_PreserveUVs.bmp"
		AD_ToolsToolTip.SetToolTip uvButton1 "Toggle preserve UVs"
-- 		dotNet.AddEventHandler uvButton1 "Click" PreserveUVs
		uvPanel.Controls.Add uvButton1
		
		uvButton2 = InitButton()
		uvButton2.BackgroundImage = MapImage "AD_TestShader.bmp"
		AD_ToolsToolTip.SetToolTip uvButton2 "Apply test texture"
-- 		dotNet.AddEventHandler uvButton2 "Click" TestTexture
		uvPanel.Controls.Add uvButton2
		
		uvButton3 = InitButton()
		uvButton3.BackgroundImage = MapImage "AD_UVTransform.bmp"
		AD_ToolsToolTip.SetToolTip uvButton3 "Transform UVs modifier"
		dotNet.AddEventHandler uvButton3 "Click" MaxUVTransform
		uvPanel.Controls.Add uvButton3
		
		boxMapButton = InitButton()
		boxMapButton.BackgroundImage = MapImage "AD_BoxMap.bmp"
		AD_ToolsToolTip.SetToolTip boxMapButton "Box map"
		dotNet.AddEventHandler boxMapButton "Click" BoxMap
		uvPanel.Controls.Add boxMapButton
		
		
		------------------------------------------------------------------------------------------------------------------------------------------
		--INFO PANEL
		------------------------------------------------------------------------------------------------------------------------------------------
		infoPanel = dotNetObject "Label"
		infoPanel.Dock = DS_Fill
		infoPanel.Text = "Welcome to AD_Tools"
		infoPanel.TextAlign = TA_Left
		infoPanel.Font = Font_Calibri
		AD_ToolsTable.Controls.Add infoPanel
		------------------------------------------------------------------------------------------------------------------------------------------
		--DRAW FORM
		------------------------------------------------------------------------------------------------------------------------------------------
		AD_ToolsForm.showModeless()
		AD_ToolsForm
	)
)
----------------------------------------------------------------------------------------------------------------
--RUN TOOL COMMANDS
----------------------------------------------------------------------------------------------------------------
if AD_ToolsInstance != undefined then
(
	AD_ToolsInstance.AD_ToolsForm.close()
)
AD_ToolsInstance = AD_ToolsStruct()
AD_ToolsInstance.CreateUI()