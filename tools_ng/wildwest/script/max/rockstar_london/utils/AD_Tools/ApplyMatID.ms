--ApplyMatID.ms
fn ProcessFaces ID =
(
	polyop.setFaceMatID  selection[1] selection[1].selectedFaces (ID as integer)
	if selection[1].material == undefined then materialName = "[no material]"
	else	materialName = selection[1].material.name as string
	if classOf selection[1].material == Multimaterial then
	(
		materialName = "Unused material"
		for i = 1 to selection[1].material.materialIDList.count do
		(
			if selection[1].material.materialIDList[i] == (ID as integer) then
			(
				materialName = selection[1].material.MaterialList[i].name
				if (selection[1].material.Names[i] != "") then
				materialName = (materialName + "(" + selection[1].material.Names[i] + ")")
			)
		)
	)
	else 
	(
		if selection[1].material == undefined then materialName = "[no material]"
		else	materialName = selection[1].material.name as string
	)
	try
	(
		ADTools.InfoPanel.Text = ("id " + ID as string + " applied to " + selection[1].selectedFaces.count as string + " faces\n" + materialName)
	)
	catch()
)


fn ApplyMatID ID =
(
	if (subObjectLevel == 4 or subObjectLevel == 5) and (selection[1].selectedFaces.count > 0) then
	(
		ProcessFaces ID as integer
	)
	else
	(
		if selection.count == 1 and classOf selection[1] == Editable_Poly and subObjectLevel == 0 then
		(
			subObjectLevel = 4
			select $.faces
			ProcessFaces ID as integer
			deselect $.faces
			subObjectLevel = 0
		)
		else
		(
			try
			(
				ADTools.InfoPanel.Text = "Warning: No faces selected"
			)
			catch()
		)
	)
)