--lists dimensions of a single object

dimensions = #()
dimensionsString = undefined
if selection.count == 1 then
(
	dimobj = selection[1]
-- 	if  (getAttrClass dimobj == "Gta Object") and (classOf dimobj != XrefObject) then
	if  (getAttrClass dimobj == "Gta Object") then
	(
		nodeBBox = nodeLocalBoundingBox dimobj
		minCoords = nodeBBox[1]
		maxCoords = nodeBBox[2]
		dimensions[1] = maxCoords[1] - minCoords[1]
		dimensions[2] = maxCoords[2] - minCoords[2]
		dimensions[3] = maxCoords[3] - minCoords[3]
		dimensionsString = ("Dimensions x:" + dimensions[1] as string + "\t  y:" + dimensions[2] as string + "\t  z:" + dimensions[3] as string)
	)
	else
	(
		dimensionsString = "Please select a valid object"
	)
)
else dimensionsString = "Please select one object"
dimensionsString