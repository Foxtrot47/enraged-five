-- version 1.0
-- this version, we have to list the colours 3 times to get them to hook up
-- version 1.1
-- color swatches are loaded from a dynamic array

global SetWirecolorInstance
--replace SetWirecolor with the name of the control

struct SetWirecolorStruct
(
	SetWirecolorForm,
	SetWirecolorFlowLayoutPanel,
	SwatchButtonArray = #(),
	IniFilePath = RsConfigGetWildWestDir()+"script/max/rockstar_london/config/AD_SetWirecolor.ini",
	
	--preset definitions
	DS_Fill = (dotNetClass "System.Windows.Forms.DockStyle").Fill,
	FBS_SizableToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").SizableToolWindow,
	FS_Flat = (dotNetClass "System.Windows.Forms.FlatStyle").Flat,
	SP_Manual =  (dotNetClass "System.Windows.Forms.FormStartPosition").Manual,
	Font_Calibri = dotNetObject "System.Drawing.Font" "Calibri" 8,
	
	--colours
	RGB_Black = (dotNetClass "System.Drawing.Color").black,
	RGB_DarkGrey = (dotNetClass "System.Drawing.Color").fromARGB 102 102	102,
	RGB_Steel = (dotNetClass "System.Drawing.Color").FromARGB 200 210 220,
	
	ColorArray = #(
		#(0,0,0,				"black"),
		#(51,51,51,		"charcoal"),
		#(102,102,102,	"dark grey"),
		#(128,128,128,	"mid grey"),
		#(216,216,216,	"light grey"),
		#(255,255,255,	"white"),
		#(102,0,0,			"dark red"),
		#(255,0,0,			"red"),
		#(255,174,0,		"orange"),
		#(255,204,0,		"yellow-orange"),
		#(204,255,0,		"lime green"),
		#(0,102,0,			"dark green"),
		#(0,0,153,			"dark blue"),
		#(128,0,128,		"purple"),
		#(199,0,192,		"magenta")
	),
	SwatchButtonSize = 16,
	WinLocX = 0,
	WinLocY = 40,
	WinWidth = 320,
	WinHeight = 240,
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--GENERAL FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn MapIcon iconName =
	(
		iconPath = IconFolder + iconName
		return (dotNetClass "System.Drawing.Image").FromFile(iconPath)
	),
	
	fn InitSwatchButton =
	(
		newButton = dotNetObject "Button"
		newButton.Size = dotNetObject "System.Drawing.Size" SwatchButtonSize SwatchButtonSize
		newButton.FlatStyle = FS_Flat
		newButton.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		return newButton
	),
	
	fn SetWirecolor rgbcolor =
	(
		objSet = for this in selection where ((getAttrClass this) == "Gta Object") or ((getAttrClass this) == "Gta Collision") collect this
		if objSet.count > 0 then
		(
			for this in objSet do
			(
				this.wirecolor = rgbcolor
			)
		)
		else messagebox "Nothing select"
	),
	
	fn ProcessWirecolor s e =
	(
		SetWirecolorInstance.SetWirecolor (color s.BackColor.r s.BackColor.g s.BackColor.b)
	),
	
		------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn UpdateLocalConfigFileSave =
	(
		setINISetting IniFilePath "Window" "WinLocX" (WinLocX as string)
		setINISetting IniFilePath "Window" "WinLocY" (WinLocY as string)
		setINISetting IniFilePath "Window" "WinWidth" (WinWidth as string)
		setINISetting IniFilePath "Window" "WinHeight" (WinHeight as string)
	),
	
	fn UpdateLocalConfigFileLoad =
	(
		try
		(
			WinLocX = getINISetting IniFilePath "Window" "WinLocX" as integer
			WinLocY = getINISetting IniFilePath "Window" "WinLocY" as integer
			WinWidth = getINISetting IniFilePath "Window" "WinWidth" as integer
			WinHeight = getINISetting IniFilePath "Window" "WinHeight" as integer
		)
		catch()
		SetWirecolorForm.Location = dotNetObject "system.drawing.point" WinLocX WinLocY
		SetWirecolorForm.Size = dotNetObject "System.Drawing.Size" WinWidth WinHeight
	),
	
	fn DispatchLoadIniFile s e = SetWirecolorInstance.UpdateLocalConfigFileLoad(),	
	fn DispatchSaveIniFile s e = SetWirecolorInstance.UpdateLocalConfigFileSave(),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--EVENT HANDLERS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn DispatchSetWinLoc s e = SetWirecolorInstance.SetWinLoc s e,
	fn SetWinLoc s e =
	(
		WinLocX = SetWirecolorForm.Location.X
		WinLocY = SetWirecolorForm.Location.Y
	),

	fn DispatchSetWinSize s e = SetWirecolorInstance.SetWinSize s e,	
	fn SetWinSize s e =
	(
		WinWidth = SetWirecolorForm.Size.Width
		WinHeight = SetWirecolorForm.Size.Height
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn CreateUI =
	(		
		-- form setup
		clearListener()
		
		local footerHeight = 30
		local WinWidth = (ColorArray.count * SwatchButtonSize) + 16
		local MinWidth = (12 * SwatchButtonSize) + 20
		local WinHeight = SwatchButtonSize + 40 + footerHeight
		
		
		SetWirecolorForm = dotNetObject "maxCustomControls.maxForm"
		SetWirecolorForm.Size = dotNetObject "System.Drawing.Size" WinWidth WinHeight
		SetWirecolorForm.MaximumSize = dotNetObject "System.Drawing.Size" 1600 1280
		SetWirecolorForm.MinimumSize = dotNetObject "System.Drawing.Size" MinWidth WinHeight
		SetWirecolorForm.Text = "Wirecolor Tool"
		SetWirecolorForm.startPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").manual
		SetWirecolorForm.Location = dotNetObject "system.drawing.point" 0 80
		SetWirecolorForm.FormBorderStyle = FBS_SizableToolWindow
		SetWirecolorForm.sizeGripStyle = (dotNetClass "SizeGripStyle").show
		SetWirecolorForm.startPosition = SP_Manual
		dotNet.AddEventHandler SetWirecolorForm "Load" DispatchLoadIniFile
		dotNet.AddEventHandler SetWirecolorForm "Closing" DispatchSaveIniFile
		dotNet.AddEventHandler SetWirecolorForm "Move" DispatchSetWinLoc
		dotNet.AddEventHandler SetWirecolorForm "Resize" DispatchSetWinSize
		
		--content
		SetWirecolorToolTip = dotnetobject "ToolTip"
		
		SetWirecolorTable = dotNetObject "TableLayoutPanel"
		SetWirecolorTable.Dock = DS_Fill
		SetWirecolorForm.Controls.Add SetWirecolorTable
		SetWirecolorTable.RowCount = 2
		SetWirecolorTable.rowStyles.add (RS_dotNetObject.rowStyleObject "percent" 100)
		SetWirecolorTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" footerHeight)
		SetWirecolorForm.Controls.Add SetWirecolorTable
		
		SetWirecolorFlowLayoutPanel = dotNetObject "System.Windows.Forms.FlowLayoutPanel"
		SetWirecolorFlowLayoutPanel.Dock = DS_Fill
		SetWirecolorFlowLayoutPanel.BackColor = RGB_DarkGrey
		SetWirecolorTable.Controls.Add SetWirecolorFlowLayoutPanel
		
-- 		SetSwatchButtons
		for i = 1 to ColorArray.count do
		(
			local RVal = ColorArray[i][1]
			local GVal = ColorArray[i][2]
			local BVal = ColorArray[i][3]
			local ColorName = ColorArray[i][4]
			SwatchButtonArray[i] = InitSwatchButton()
			SwatchButtonArray[i].BackColor = (dotNetClass "System.Drawing.Color").fromARGB RVal GVal BVal
			SetWirecolorToolTip.SetToolTip SwatchButtonArray[i] ColorName
			dotNet.AddEventHandler SwatchButtonArray[i] "Click" ProcessWirecolor
			SetWirecolorFlowLayoutPanel.Controls.Add SwatchButtonArray[i]
		)
		
		--radio buttons
		SetWirecolorPanel = dotnetobject "TableLayoutPanel"
		SetWirecolorPanel.Dock = DS_Fill
		SetWirecolorPanel.ColumnCount = 2
		SetWirecolorPanel.columnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 70)
		SetWirecolorPanel.columnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 70)
		SetWirecolorPanel.BackColor = (dotNetClass "System.Drawing.Color").fromARGB 240 240 240
		SetWirecolorTable.Controls.Add SetWirecolorPanel
		RadioButton1 = dotnetobject "RadioButton"
		RadioButton1.Text = "inactive"
		RadioButton1.Checked = true
		RadioButton1.Font = Font_Calibri
		showProperties RadioButton1
		RadioButton2 = dotnetobject "RadioButton"
		RadioButton2.Text = "active"
		RadioButton2.Font = Font_Calibri
		SetWirecolorPanel.Controls.Add RadioButton1
		SetWirecolorPanel.Controls.Add RadioButton2
		
		--draw form
		SetWirecolorForm.showModeless()
		SetWirecolorForm
	)
)

if SetWirecolorInstance != undefined then
(
	SetWirecolorInstance.SetWirecolorForm.close()
)

SetWirecolorInstance = SetWirecolorStruct()
SetWirecolorInstance.CreateUI()