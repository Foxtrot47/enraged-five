objs = selection as array
if objs.count > 1 then
(
	childrenset = #()
	for i = 1 to (objs.count - 1) do
	(
		objs[i].parent = objs[objs.count]
		append childrenset objs[i]
	)
	select childrenset
)
else
(
	messagebox "Please select more than one object"
)