--vertex locator script
--places a locator at the location of each selected vert

fn VertexLocator locatorSize =
(
	if (selection.count != 1) then
	(
		messagebox "Select one object only"
	)
	else
	(
		local inObject = selection[1]
		local vertselection = 
		subobjectlevel = 1
		case (classof inObject) of
		(
			Editable_Poly:
			(
				vertselection = polyop.getVertSelection inObject
			)
			
			Editable_Mesh:
			(
				vertselection = getVertSelection inObject
			)
			
			default:
			(
				messagebox "Unsupported object type"
			)
		)
		print inObject.verts[50]
	)
)

VertexLocator 1