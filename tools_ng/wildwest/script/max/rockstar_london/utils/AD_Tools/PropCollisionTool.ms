--PropCollisionTool.ms

global PropCollisionTool

struct PropCollisionToolStruct
(
	PropCollisionToolForm,
	PropCollisionToolFlowLayoutPanel,
	SwatchButtonArray = #(),
	IniFilePath = RsConfigGetWildWestDir()+"script/max/rockstar_london/config/AD_Tools.ini",
	IconFolder = RsConfigGetWildWestDir()+"script/max/rockstar_london/images/AD_Icons/",
	
	--preset definitions
	DS_Fill = (dotNetClass "System.Windows.Forms.DockStyle").Fill,
	FBS_SizableToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").SizableToolWindow,
	FBS_FixedToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").FixedToolWindow,
	FS_Flat = (dotNetClass "System.Windows.Forms.FlatStyle").Flat,
	FS_Standard = (dotNetClass "System.Windows.Forms.FlatStyle").Standard,
	FS_System = (dotNetClass "System.Windows.Forms.FlatStyle").System,
	FS_Popup = (dotNetClass "System.Windows.Forms.FlatStyle").Popup,
	SP_Manual =  (dotNetClass "System.Windows.Forms.FormStartPosition").Manual,
	Font_Calibri = dotNetObject "System.Drawing.Font" "Calibri" 8,
	TA_left =  (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft,
	
	--colours
	RGB_Black = (dotNetClass "System.Drawing.Color").black,
	RGB_DarkGrey = (dotNetClass "System.Drawing.Color").fromARGB 102 102	102,
	RGB_Steel = (dotNetClass "System.Drawing.Color").FromARGB 200 210 220,
	RGB_White = (dotNetClass "System.Drawing.Color").FromARGB 255 255 255,
	RGB_Transparent = (dotNetClass "System.Drawing.Color").transparent,

	WinLocX = 0,
	WinLocY = 40,
	
	PropCollisionToolForm,
	InfoPanel,
	TypeButton1,
	TypeButton2,
	TypeButton3,
	TypeButton4,
	TypeButton5,
	MatButton1,
	MatButton2,
	MatButton3,
	MatButton4,
	MatButton5,
	MatButton6,
	MatButton7,
	MatButton8,
	MatButton9,
	MatButton10,
	MatButton11,
	objSet,
	camcol,
	physicscol,
	target,
	CameraCollisionType,
	
	DefaultText = "Select physics collision object first, then geometry",
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--GENERAL FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	fn ProcessPhysicsCollision =
	(
		--create world collision
		if (classOf objSet[1] != Editable_Poly) then convertToPoly objSet[1]
		if (classOf objSet[2] != Editable_Poly) then convertToPoly objSet[2]
		mesh2col objSet[1]
		setattr objSet[1] (getattrindex "Gta Collision" "Camera") false
		setattr objSet[1] (getattrindex "Gta Collision" "Physics") true
		setattr objSet[1] (getattrindex "Gta Collision" "Hi Detail") true
		objSet[1].pivot = objSet[2].pivot
		objSet[1].parent = objSet[2]
		objSet[1].name = (uniqueName (objSet[2].name + "_col"))
		objSet[1].wirecolor = orange
		objSet[2].wirecolor = (color 128 0 128)
	),
	
	fn ProcessCameraCollision target =
	(		
		--create camera collision
		colsize = #(target.max.x - target.min.x, target.max.y - target.min.y, target.max.z - target.min.z)
		case PropCollisionTool.CameraCollisionType of
		(
			"box":
			(
				camcol = Col_Box name:(uniqueName (target.name + "_camcol"))
				camcol.length = colsize[1] * 0.5
				camcol.width = colsize[2] * 0.5
				camcol.height = colsize[3] * 0.5
				camcol.transform = target.transform
				camcol.position = target.center
				camcol.parent = target
				setattr camcol (getattrindex "Gta Collision" "Camera") true
				setattr camcol (getattrindex "Gta Collision" "Physics") false
				setattr camcol (getattrindex "Gta Collision" "Hi Detail") false
			)
			"sphere":
			(
				camcol = Col_Sphere name:(uniqueName (target.name + "_camcol"))
				local newradius = colsize[1]
				if colsize[2] > newradius then newradius = colsize[2]
				if colsize[3] > newradius then newradius = colsize[3]
				camcol.radius = newradius * 0.5				
				camcol.transform = target.transform
				in coordsys local rotate camcol (eulerangles 90 0 0)
				camcol.position = target.center
				camcol.parent = target
				setattr camcol (getattrindex "Gta Collision" "Camera") true
				setattr camcol (getattrindex "Gta Collision" "Physics") false
				setattr camcol (getattrindex "Gta Collision" "Hi Detail") false
			)
			"xcylinder":
			(
				camcol = Col_Cylinder name:(uniqueName (target.name + "_camcol"))
				if colsize[2] > colsize[3] then camcol.radius = colsize[2] * 0.5
				else camcol.radius = colsize[3] * 0.5
				camcol.length = colsize[1]
				camcol.transform = target.transform
				in coordsys local rotate camcol (eulerangles 0 0 90)
				camcol.position = target.center
				camcol.parent = target
				setattr camcol (getattrindex "Gta Collision" "Camera") true
				setattr camcol (getattrindex "Gta Collision" "Physics") false
				setattr camcol (getattrindex "Gta Collision" "Hi Detail") false
			)
			"ycylinder":
			(
				camcol = Col_Cylinder name:(uniqueName (target.name + "_camcol"))
				if colsize[1] > colsize[3] then camcol.radius = colsize[1] * 0.5
				else camcol.radius = colsize[3] * 0.5
				camcol.length = colsize[2]
				camcol.transform = target.transform
				camcol.position = target.center
				camcol.parent = target
				setattr camcol (getattrindex "Gta Collision" "Camera") true
				setattr camcol (getattrindex "Gta Collision" "Physics") false
				setattr camcol (getattrindex "Gta Collision" "Hi Detail") false
			)
			"zcylinder":
			(
				camcol = Col_Cylinder name:(uniqueName (target.name + "_camcol"))
				if colsize[2] > colsize[1] then camcol.radius = colsize[2] * 0.5
				else camcol.radius = colsize[1] * 0.5
				camcol.length = colsize[3]
				camcol.transform = target.transform
				in coordsys local rotate camcol (eulerangles 90 0 0)
				camcol.position = target.center
				camcol.parent = target
				setattr camcol (getattrindex "Gta Collision" "Camera") true
				setattr camcol (getattrindex "Gta Collision" "Physics") false
				setattr camcol (getattrindex "Gta Collision" "Hi Detail") false
			)
		)
		camcol.wirecolor = red
	),
	
	fn DispatchProcessPhysicsAndCameraCollision =
	(
		-- select the world collision poly object, then the target object
		PropCollisionTool.objSet = for this in selection where superClassOf this == GeometryClass collect this
		if PropCollisionTool.objSet.count == 2 then
		(
			PropCollisionTool.ProcessPhysicsCollision()
			--process the camera collision
			if (PropCollisionTool.TypeButton1.Checked == true) then PropCollisionTool.CameraCollisionType = "box"
			if (PropCollisionTool.TypeButton2.Checked == true) then PropCollisionTool.CameraCollisionType = "sphere"
			if (PropCollisionTool.TypeButton3.Checked == true) then PropCollisionTool.CameraCollisionType = "xcylinder"
			if (PropCollisionTool.TypeButton4.Checked == true) then PropCollisionTool.CameraCollisionType = "ycylinder"
			if (PropCollisionTool.TypeButton5.Checked == true) then PropCollisionTool.CameraCollisionType = "zcylinder"
			PropCollisionTool.ProcessCameraCollision PropCollisionTool.objSet[2]
			--process the mesh material type
			local Collmaterial
			if (PropCollisionTool.MatButton1.Checked == true) then Collmaterial = "NONE"
			if (PropCollisionTool.MatButton2.Checked == true) then Collmaterial = "CONCRETE"
			if (PropCollisionTool.MatButton3.Checked == true) then Collmaterial = "METAL_SOLID_MEDIUM"
			if (PropCollisionTool.MatButton4.Checked == true) then Collmaterial = "WOOD_SOLID_MEDIUM"
			if (PropCollisionTool.MatButton5.Checked == true) then Collmaterial = "PLASTIC"
			if (PropCollisionTool.MatButton6.Checked == true) then Collmaterial = "CLOTH"
			if (PropCollisionTool.MatButton7.Checked == true) then Collmaterial = "RUBBER"
			if (PropCollisionTool.MatButton8.Checked == true) then Collmaterial = "GLASS_MEDIUM"
			if (PropCollisionTool.MatButton9.Checked == true) then Collmaterial = "BRICK_RED"
			if (PropCollisionTool.MatButton10.Checked == true) then Collmaterial = "METAL_CORRUGATED_IRON"
			if (PropCollisionTool.MatButton11.Checked == true) then Collmaterial = "BREEZE_BLOCK"
			setattr PropCollisionTool.objSet[1] (getattrindex "Gta Collision" "Coll Type") Collmaterial
			GeometryText = ("Geo: " + PropCollisionTool.objSet[2].name + "\n")
			CollisionText = ("Col: " + PropCollisionTool.objSet[1].name + "\n")
			CamcolText = ("Cam: " + PropCollisionTool.camcol.name)
			PropCollisionTool.InfoPanel.Text = (GeometryText + CollisionText + CamcolText)
			select PropCollisionTool.objSet[1]
		)
		else
		(
			PropCollisionTool.InfoPanel.Text = "Please select the collision and the target object"
		)
	),
	
	fn DispatchCreateCameraCollision =
	(
		local OperandSet = #()
		OperandSet = for this in selection where superClassOf this == GeometryClass collect this
		if (OperandSet.count > 0) then
		(
			if (PropCollisionTool.TypeButton1.Checked == true) then PropCollisionTool.CameraCollisionType = "box"
			if (PropCollisionTool.TypeButton2.Checked == true) then PropCollisionTool.CameraCollisionType = "sphere"
			if (PropCollisionTool.TypeButton3.Checked == true) then PropCollisionTool.CameraCollisionType = "xcylinder"
			if (PropCollisionTool.TypeButton4.Checked == true) then PropCollisionTool.CameraCollisionType = "ycylinder"
			if (PropCollisionTool.TypeButton5.Checked == true) then PropCollisionTool.CameraCollisionType = "zcylinder"
			for this in OperandSet do
			(
				PropCollisionTool.ProcessCameraCollision this
				OperandSet.wirecolor = (color 128 0 128)
			)
			PropCollisionTool.InfoPanel.Text = (PropCollisionTool.CameraCollisionType + " camera collision applied to selection")
		)
		else
		(
			PropCollisionTool.InfoPanel.Text = "Please select an object"
		)
		
	),
	
	fn DispatchApplyCollisionMaterial =
	(
		local Collmaterial
		if (PropCollisionTool.MatButton1.Checked == true) then Collmaterial = #("NONE",0)
		if (PropCollisionTool.MatButton2.Checked == true) then Collmaterial = #("CONCRETE",1)
		if (PropCollisionTool.MatButton3.Checked == true) then Collmaterial = #("METAL_SOLID_MEDIUM",2)
		if (PropCollisionTool.MatButton4.Checked == true) then Collmaterial = #("WOOD_SOLID_MEDIUM",3)
		if (PropCollisionTool.MatButton5.Checked == true) then Collmaterial = #("PLASTIC",4)
		if (PropCollisionTool.MatButton6.Checked == true) then Collmaterial = #("CLOTH",5)
		if (PropCollisionTool.MatButton7.Checked == true) then Collmaterial = #("RUBBER",6)
		if (PropCollisionTool.MatButton8.Checked == true) then Collmaterial = #("GLASS",7)
		if (PropCollisionTool.MatButton9.Checked == true ) then Collmaterial = #("BRICK_RED",8)
		if (PropCollisionTool.MatButton10.Checked == true ) then Collmaterial = #("METAL_CORRUGATED_IRON",9)
		if (PropCollisionTool.MatButton11.Checked == true ) then Collmaterial = #("BREEZE_BLOCK",10)
		PropCollisionTool.objSet = #()
		PropCollisionTool.objSet = for this in selection where ((getAttrClass this) == "Gta Collision") collect this
		if (PropCollisionTool.objSet.count > 0) then
		(
			for this in PropCollisionTool.objSet do
			(
				setattr this (getattrindex "Gta Collision" "Coll Type") Collmaterial[1]
			)
			PropCollisionTool.InfoPanel.Text = (Collmaterial[1] + " material applied to selection")
		)
		else
		(
			geomSet = for this in selection where ((superClassOf this) == GeometryClass) collect this
			if geomSet.count > 0 then
			(
				if geomSet.count == 1 and (subObjectLevel == 4 or subObjectLevel == 5) then
				(
					if Collmaterial[2] == 0 then
					(
						subObjectLevel = 0
						geomSet[1].material = undefined
						subObjectLevel = 4
					)
					else
					(
						geomSet[1].material = getMeditMaterial 1
						convertToPoly geomSet[1]
						polyOp.setFaceMatID geomSet[1] (polyop.getFaceSelection geomSet[1]) Collmaterial[2]
						subObjectLevel = 4
					)
				)
				else
				(
					for this in geomSet do
					(
						if Collmaterial[2] == 0 then
						(
							this.material = undefined
						)
						else
						(
							this.material = getMeditMaterial 1
							addModifier this (Materialmodifier materialID: Collmaterial[2])
							convertToPoly this
						)
					)
				)
				PropCollisionTool.InfoPanel.Text = (Collmaterial[1] + " applied to selected geometry")
			)
			else
			(
				PropCollisionTool.InfoPanel.Text = "Please select an object"
			)
		)
	),
	
	fn GetCollisionType inObject =
	(
		collisionType = ""
		if ((getAttrClass inObject) == "Gta Collision") then
		(
			camcol = (getattr inObject (getattrindex "Gta Collision" "Camera"))
			physcol = (getattr inObject (getattrindex "Gta Collision" "Physics"))
			hicol =  (getattr inObject (getattrindex "Gta Collision" "Hi Detail"))
			
			if (camcol == true and physcol == false and hicol == false) then collisionType = "camera"
			if (camcol == false and physcol == true and hicol == true) then collisionType = "physics"
			if (camcol == false and physcol == false and hicol == false) then collisionType = "none"
			if (camcol == true and physcol == true and hicol == true) then collisionType = "combi"
			if (collisionType == "") then collisionType == "other"
		)
		else collisionType = "n/a"
		return collisionType
	),
	
	fn QueryCollisionMaterials =
	(
		collisionMaterials = #()
		collisionTypes = #()
		infoString = ""
		for this in selection where ((getAttrClass this) == "Gta Collision") do
		(
			appendIfUnique collisionMaterials (GetAttr this (GetAttrIndex "Gta Collision" "Coll Type"))
			appendIfUnique collisionTypes (PropCollisionTool.GetCollisionType this)
		)
		if (collisionMaterials.count > 0) then
		(
			infoString = "materials: "
			for this in collisionMaterials do
			(
				infoString = (infoString + this as string + ", ")
			)
			infoString = ((substring infoString 1 (infoString.count - 2)) + "\ntypes: ")
			for this in collisionTypes do
			(
				infoString = (infoString + this as string + ", ")
			)
			infoString = substring infoString 1 (infoString.count - 2)
			PropCollisionTool.InfoPanel.Text = infoString
		)
		else
		(
			infoString = "No collision materials detected."
		)
		PropCollisionTool.InfoPanel.Text = infoString
	),
	
	fn SetCameraCollisionTrue =
	(
		for this in selection where ((getAttrClass this) == "Gta Collision") do
		(
			setattr this (getattrindex "Gta Collision" "Camera") true
			setattr this (getattrindex "Gta Collision" "Physics") false
			setattr this (getattrindex "Gta Collision" "Hi Detail") false
			PropCollisionTool.InfoPanel.Text = (this.name + " set to cameara collision")
		)
	),

	fn SetPhysicsCollisionTrue =
	(
		for this in selection where ((getAttrClass this) == "Gta Collision") do
		(
			setattr this (getattrindex "Gta Collision" "Camera") false
			setattr this (getattrindex "Gta Collision" "Physics") true
			setattr this (getattrindex "Gta Collision" "Hi Detail") true
			PropCollisionTool.InfoPanel.Text = (this.name + " set to physics collision")
		)
	),

	fn SetAllCollisionTrue =
	(
		for this in selection where ((getAttrClass this) == "Gta Collision") do
		(
			setattr this (getattrindex "Gta Collision" "Camera") true
			setattr this (getattrindex "Gta Collision" "Physics") true
			setattr this (getattrindex "Gta Collision" "Hi Detail") true
			PropCollisionTool.InfoPanel.Text = (this.name + " set to combi-collision")
		)
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn UpdateLocalConfigFileSave =
	(
		setINISetting IniFilePath "PropCollisionTool" "WinLocX" (WinLocX as string)
		setINISetting IniFilePath "PropCollisionTool" "WinLocY" (WinLocY as string)
	),
	
	fn UpdateLocalConfigFileLoad =
	(
		try
		(
			WinLocX = getINISetting IniFilePath "PropCollisionTool" "WinLocX" as integer
			WinLocY = getINISetting IniFilePath "PropCollisionTool" "WinLocY" as integer
		)
		catch()
		PropCollisionToolForm.Location = dotNetObject "system.drawing.point" WinLocX WinLocY
	),
	
	fn DispatchLoadIniFile s e = PropCollisionTool.UpdateLocalConfigFileLoad(),	
	fn DispatchSaveIniFile s e = PropCollisionTool.UpdateLocalConfigFileSave(),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--EVENT HANDLERS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn DispatchSetWinLoc s e = PropCollisionTool.SetWinLoc s e,
	fn SetWinLoc s e =
	(
		WinLocX = PropCollisionToolForm.Location.X
		WinLocY = PropCollisionToolForm.Location.Y
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn CreateUI =
	(		
		-- form setup
		PropCollisionToolForm = dotNetObject "maxCustomControls.maxForm"
		PropCollisionToolForm.Size = dotNetObject "System.Drawing.Size" 240 440
		PropCollisionToolForm.Text = "Prop Collision Tool"
		PropCollisionToolForm.startPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").manual
		PropCollisionToolForm.Location = dotNetObject "system.drawing.point" 0 80
		PropCollisionToolForm.FormBorderStyle = FBS_FixedToolWindow
		PropCollisionToolForm.sizeGripStyle = (dotNetClass "SizeGripStyle").show
		PropCollisionToolForm.startPosition = SP_Manual
		dotNet.AddEventHandler PropCollisionToolForm "Load" DispatchLoadIniFile
		dotNet.AddEventHandler PropCollisionToolForm "Closing" DispatchSaveIniFile
		dotNet.AddEventHandler PropCollisionToolForm "Move" DispatchSetWinLoc
		
		--content
		PropCollisionToolToolTip = dotnetobject "ToolTip"
		
		PropCollisionToolTable = dotNetObject "TableLayoutPanel"
		PropCollisionToolTable.Dock = DS_Fill
		PropCollisionToolForm.Controls.Add PropCollisionToolTable
		PropCollisionToolTable.RowCount = 4
		PropCollisionToolTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 280)
		PropCollisionToolTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 60)
		PropCollisionToolTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		PropCollisionToolTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		PropCollisionToolTable.ColumnCount = 1
		PropCollisionToolTable.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		PropCollisionToolTable.BackColor = RGB_Transparent
		local ImagePath = RsConfigGetWildWestDir()+"script/max/rockstar_london/images/AD_Icons/PropCollisionToolBackground.bmp"
		PropCollisionToolTable.BackgroundImage = (dotNetClass "System.Drawing.Image").FromFile ImagePath
		PropCollisionToolForm.Controls.Add PropCollisionToolTable
		
		TwinPanel = dotNetObject "TableLayoutPanel"
		TwinPanel.ColumnCount = 2
		TwinPanel.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 50)
		TwinPanel.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 50)
		TwinPanel.Dock = DS_Fill
		TwinPanel.CellBorderStyle = (dotNetClass "System.Windows.Forms.TableLayoutPanelCellBorderStyle").single
		PropCollisionToolTable.Controls.Add TwinPanel 0 0
		
		TypeButtonPanel = dotNetObject "TableLayoutPanel"
		TypeButtonPanel.RowCount = 11
		TypeButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		TypeButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		TypeButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		TypeButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		TypeButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		TypeButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		TypeButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		TypeButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		TypeButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		TypeButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		TypeButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		TypeButtonPanel.Dock = DS_Fill
		TwinPanel.Controls.Add TypeButtonPanel 0 0
		
		TypeLabel = dotNetObject "Label"
		TypeLabel.Text = "Camera collision"
		TypeLabel.Font = Font_Calibri
		TypeLabel.Dock = DS_Fill
		TypeButtonPanel.Controls.Add TypeLabel 0 0
		TypeButton1 = dotnetobject "RadioButton"
		TypeButton1.Text = "Box"
		TypeButton1.Checked = true
		TypeButton1.Font = Font_Calibri
		TypeButtonPanel.Controls.Add TypeButton1 0 1
		TypeButton2 = dotnetobject "RadioButton"
		TypeButton2.Text = "Sphere"
		TypeButton2.Font = Font_Calibri
		TypeButtonPanel.Controls.Add TypeButton2 0 2
		TypeButton3 = dotnetobject "RadioButton"
		TypeButton3.Text = "Cylinder x"
		TypeButton3.Font = Font_Calibri
		TypeButtonPanel.Controls.Add TypeButton3 0 3
		TypeButton4 = dotnetobject "RadioButton"
		TypeButton4.Text = "Cylinder y"
		TypeButton4.Font = Font_Calibri
		TypeButtonPanel.Controls.Add TypeButton4 0 4
		TypeButton5 = dotnetobject "RadioButton"
		TypeButton5.Text = "Cylinder z"
		TypeButton5.Font = Font_Calibri
		TypeButtonPanel.Controls.Add TypeButton5 0 5
		
		CameraCollisionButton = dotNetObject "Button"
		CameraCollisionButton.Text = "Create camera col"
		CameraCollisionButton.Flatstyle = FS_Standard
		CameraCollisionButton.Font = Font_Calibri
		CameraCollisionButton.BackColor = RGB_Transparent
		CameraCollisionButton.Width = (PropCollisionToolForm.Width/2 - 18)
		TypeButtonPanel.Controls.Add CameraCollisionButton 0 6
		dotNet.AddEventHandler CameraCollisionButton "Click" DispatchCreateCameraCollision
		
		CameraButton = dotNetObject "Button"
		CameraButton.Text = "Set camera col"
		CameraButton.Flatstyle = FS_Standard
		CameraButton.Font = Font_Calibri
		CameraButton.BackColor = RGB_Transparent
		CameraButton.Width = (PropCollisionToolForm.Width/2 - 18)
		TypeButtonPanel.Controls.Add CameraButton 0 8
		dotNet.AddEventHandler CameraButton "Click" SetCameraCollisionTrue
		
		PhysicsButton = dotNetObject "Button"
		PhysicsButton.Text = "Set physics col"
		PhysicsButton.Flatstyle = FS_Standard
		PhysicsButton.Font = Font_Calibri
		PhysicsButton.BackColor = RGB_Transparent
		PhysicsButton.Width = (PropCollisionToolForm.Width/2 - 18)
		TypeButtonPanel.Controls.Add PhysicsButton 0 9
		dotNet.AddEventHandler PhysicsButton "Click" SetPhysicsCollisionTrue
		
		AllButton = dotNetObject "Button"
		AllButton.Text = "Set all collision"
		AllButton.Flatstyle = FS_Standard
		AllButton.Font = Font_Calibri
		AllButton.BackColor = RGB_Transparent
		AllButton.Width = (PropCollisionToolForm.Width/2 - 18)
		TypeButtonPanel.Controls.Add AllButton 0 10
		dotNet.AddEventHandler AllButton "Click" SetAllCollisionTrue
		
		MatButtonPanel = dotNetObject "TableLayoutPanel"
		MatButtonPanel.RowCount = 13
		MatButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		MatButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		MatButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		MatButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		MatButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		MatButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		MatButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		MatButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		MatButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		MatButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		MatButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		MatButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		MatButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		MatButtonPanel.Dock = DS_Fill
		TwinPanel.Controls.Add MatButtonPanel 1 0
		
		MatLabel = dotNetObject "Label"
		MatLabel.Text = "Collision material"
		MatLabel.Font = Font_Calibri
		MatLabel.Dock = DS_Fill
		MatButtonPanel.Controls.Add MatLabel 0 0
		MatButton1 = dotnetobject "RadioButton"
		MatButton1.Text = "0: None"
		MatButton1.Checked = true
		MatButton1.Font = Font_Calibri
		MatButtonPanel.Controls.Add MatButton1 0 1
		MatButton2 = dotnetobject "RadioButton"
		MatButton2.Text = "1: Concrete"
		MatButton2.Font = Font_Calibri
		MatButtonPanel.Controls.Add MatButton2 0 2
		MatButton3 = dotnetobject "RadioButton"
		MatButton3.Text = "2: Metal"
		MatButton3.Font = Font_Calibri
		MatButtonPanel.Controls.Add MatButton3 0 3
		MatButton4 = dotnetobject "RadioButton"
		MatButton4.Text = "3: Wood"
		MatButton4.Font = Font_Calibri
		MatButtonPanel.Controls.Add MatButton4 0 4
		MatButton5 = dotnetobject "RadioButton"
		MatButton5.Text = "4: Plastic"
		MatButton5.Font = Font_Calibri
		MatButtonPanel.Controls.Add MatButton5 0 5
		MatButton6 = dotnetobject "RadioButton"
		MatButton6.Text = "5: Cloth"
		MatButton6.Font = Font_Calibri
		MatButtonPanel.Controls.Add MatButton6 0 6
		MatButton7 = dotnetobject "RadioButton"
		MatButton7.Text = "6: Rubber"
		MatButton7.Font = Font_Calibri
		MatButtonPanel.Controls.Add MatButton7 0 7
		MatButton8 = dotnetobject "RadioButton"
		MatButton8.Text = "7: Glass"
		MatButton8.Font = Font_Calibri
		MatButtonPanel.Controls.Add MatButton8 0 8
		MatButton9 = dotnetobject "RadioButton"
		MatButton9.Text = "8: Red Brick"
		MatButton9.Font = Font_Calibri
		MatButtonPanel.Controls.Add MatButton9 0 9
		MatButton10 = dotnetobject "RadioButton"
		MatButton10.Text = "9: Corrug Iron"
		MatButton10.Font = Font_Calibri
		MatButtonPanel.Controls.Add MatButton10 0 10
		MatButton11 = dotnetobject "RadioButton"
		MatButton11.Text = "10: Breeze Block"
		MatButton11.Font = Font_Calibri
		MatButtonPanel.Controls.Add MatButton11 0 11
		
		CollisionMaterialButton = dotNetObject "Button"
		CollisionMaterialButton.Text = "Apply col material"
		CollisionMaterialButton.Flatstyle = FS_Standard
		CollisionMaterialButton.Font = Font_Calibri
		CollisionMaterialButton.BackColor = RGB_Transparent
		CollisionMaterialButton.Width = (PropCollisionToolForm.Width/2 - 18)
		MatButtonPanel.Controls.Add CollisionMaterialButton 0 12
		dotNet.AddEventHandler CollisionMaterialButton "Click" DispatchApplyCollisionMaterial
		
		InfoPanel = dotNetObject "Label"
		InfoPanel.Text = DefaultText
		InfoPanel.Dock = DS_Fill
		InfoPanel.Font = Font_Calibri
		InfoPanel.TextAlign = TA_left
		PropCollisionToolTable.Controls.Add InfoPanel 0 1
		
		ProcessButton = dotNetObject "Button"
		ProcessButton.Text = "Create physics + camera collision"
		ProcessButton.Dock = DS_Fill
		ProcessButton.Flatstyle = FS_Standard
		ProcessButton.Font = Font_Calibri
		ProcessButton.BackColor = RGB_Transparent
		ProcessButton.ForeColor = (dotNetClass "System.Drawing.Color").Crimson
-- 		ProcessButton.BackColor =  (dotNetClass "System.Drawing.Color").fromARGB 216 255 0
		PropCollisionToolTable.Controls.Add ProcessButton 0 3
		dotNet.AddEventHandler ProcessButton "Click" DispatchProcessPhysicsAndCameraCollision
		
		QueryButton = dotNetObject "Button"
		QueryButton.Text = "Query collision materials"
		QueryButton.Dock = DS_Fill
		QueryButton.Flatstyle = FS_Standard
		QueryButton.Font = Font_Calibri
		QueryButton.BackColor = RGB_Transparent
		PropCollisionToolTable.Controls.Add QueryButton 0 2
		dotNet.AddEventHandler QueryButton "Click" QueryCollisionMaterials
		
		--draw form
		PropCollisionToolForm.showModeless()
		PropCollisionToolForm
	)
)

if PropCollisionTool != undefined then
(
	PropCollisionTool.PropCollisionToolForm.close()
)

PropCollisionTool = PropCollisionToolStruct()
PropCollisionTool.CreateUI()