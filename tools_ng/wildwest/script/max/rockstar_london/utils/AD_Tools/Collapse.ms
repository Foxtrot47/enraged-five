--Collapse.ms
subObjectLevel = 0
objset = #()
objset = for this in selection where ((getAttrClass this) == "Gta Object") collect this
if objset.count > 0 then
(
	for this in objset do
	(
		select this
		macros.run "Modifier Stack" "Convert_to_Poly"
	)
	select objset
)
