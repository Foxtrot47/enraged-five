--PivotMatch.ms
--Matches selected objects' pivots to last selected object

objSet = for this in selection where (classOf this != XrefObject) collect this
if objSet.count > 1 then
(
	newSelection = #()
	pivotPosition = objSet[objSet.count].pivot
	for i = 1 to (objSet.count - 1) do
	(
		objSet[i].pivot = pivotPosition
		append newSelection objSet[i]
	)
	print ("pivots matched to " + objSet[objSet.count].name)
	select newSelection
	try
	(
		if objSet.count > 2 then
		(
			objstring = ((objSet.count - 1) as string + " objects' pivots")
		)
		else objstring = "1 object's pivot"
		ADTools.InfoPanel.Text = (objstring + " matched to " + objSet[objSet.count].name)
	)
	catch()
)
else messagebox "Please select more than one object"