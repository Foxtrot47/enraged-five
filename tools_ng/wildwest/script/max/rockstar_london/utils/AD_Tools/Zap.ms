--Zap.ms
--deletes an edge loop from a selected edge
--�2010 Rockstar London
--Author: Andrew Davis
--Version 1: December 2010

objSet = for this in selection where (classOf this == Editable_Poly) collect this
if (objSet.count == 1 and SubObjectLevel == 2) then
(
	edgeSel = objSet[1].selectedEdges
	if (edgeSel.count > 0) then
	(
		macros.run "Editable Polygon Object" "EPoly_Select_Loop"
		macros.run "Editable Polygon Object" "EPoly_Remove"
		SubObjectLevel = 2
		try
		(
			ADTools.InfoPanel.Text = "Zap!"
		)
		catch()
	)
	else
	(
		messagebox "Please select an edge."
	)
)
else
(
	messagebox "Please select edges on an editable poly object."
)