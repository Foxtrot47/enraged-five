--Detach.ms
--edit 27/01/2011: ADTools info added

if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 5) then
(
	subObjectLevel = 4
)
if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 4) then
(
	if $.selectedFaces.count > 0 then
	(
		numFaces = $.selectedFaces.count
		newname = ($.name + "_detached")
		polyop.detachFaces $ $.selectedFaces delete:true asNode:true name:newname
		if numFaces == 1 then
		(
			print ("One face detached")
			try (ADTools.InfoPanel.Text = ("One face detached"))
			catch()
		)
		else
		(
			print ($.selectedFaces.count as string + " faces detached")
			try (ADTools.InfoPanel.Text = (numFaces as string + " faces detached"))
			catch()
		)
		execute ("select $" + newname)
		subObjectLevel = 4
	)
	else
	(
		messagebox "Please select a face"
	)
)
else
(
	messagebox "Please select faces on an editable poly object"
)