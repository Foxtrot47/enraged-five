--CapHoles.ms

if (selection.count == 1) and (classOf selection[1] == Editable_Poly) and (subObjectLevel == 2) then
(
	try
	(
		ADTools.InfoPanel.Text = "Cap holes"
	)
	catch()
	if (selection[1].selectedEdges.count > 0) then
	(
		modPanel.addModToSelection  (Cap_Holes())
		collapseStack $
	subObjectLevel = 2
	)
)