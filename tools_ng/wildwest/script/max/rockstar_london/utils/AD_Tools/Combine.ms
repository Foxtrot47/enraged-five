--Combine.ms
-- Maya style combine

--edit 27/01/2011: added ADTools info

objset = for this in selection where (classOf this != XrefObject) collect this
if objset.count < 2 then
(
	messagebox "please select at least two objects"
)
else
(
	masterobj = convertToPoly(objset[objset.count])
	for i = 1 to (objset.count - 1) do
	(
		masterobj.attach objset[i] masterobj
	)
	try (ADTools.InfoPanel.Text = (objset.count as string + " objects combined"))
	catch()
)