--script to toggle edge constraints
--also toggles preserve uvs

-- <Editable_Poly>.constrainType Integer default: 0 -- integer; Vertex_Constraint_Type
-- Get/Set the Edit Geometry > Constraints radio buttons selection.
-- Possible values are:
-- 0 - None
-- 1 - Edge
-- 2 - Face
-- 3 - Normal


if (selection.count == 1) and (classOf $ == Editable_Poly) then
(
	if $.constrainType == 1 then
	(
		$.constrainType = 0
		$.preserveUVs = false
	)
	else
	(
		$.constrainType = 1
		$.preserveUVs = true
	)
)
else
(
	messagebox "Please select an editable poly object"
)