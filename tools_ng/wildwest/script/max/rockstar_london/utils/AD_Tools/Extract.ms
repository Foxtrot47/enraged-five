--Extract.ms
--revision 02/02/2011: ADTools.InfoPanel text added

if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 5) then
(
	subObjectLevel = 4
)
if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 4) then
(
	if $.selectedFaces.count > 0 then
	(
		$.EditablePoly.detachToElement #Face keepOriginal:off
		if $.selectedFaces.count == 1 then
		(
			try (ADTools.InfoPanel.Text = "One face extracted")
			catch()
		)
		else
		(
			try (ADTools.InfoPanel.Text = ($.selectedFaces.count as string + " faces extracted"))
			catch()
		)
	)
	else
	(
		messagebox "Please select a face"
	)
)
else
(
	messagebox "Please select faces on an editable poly object"
)