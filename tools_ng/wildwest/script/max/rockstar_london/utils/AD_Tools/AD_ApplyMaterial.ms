fn AD_ApplyMaterial slot =
(
	objSet = for this in selection where getAttrClass this == "Gta Object" and classOf this != XRefObject collect this
	if objSet.count > 0 then
	(
		SlotMaterial = getMeditMaterial (slot as integer)
		for this in objSet do
		(
			this.material = SlotMaterial
		)
		try
		(
			ADTools.infoPanel.Text = ("Slot " + slot as string  + " (" + SlotMaterial.name + ") material applied")
		)
		catch()
	)
	else
	(
		try
		(
			ADTools.infoPanel.Text = ("No objects selected")
		)
		catch()
	)
)