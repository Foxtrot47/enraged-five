--Duplicate.ms
--edited 27/01/2011: ADTools info added

(
	local copySet = #()
	for this in selection do
	(
		newObj = copy this
-- 		setAttr newObj (getAttrIndex "Gta Object" "TXD") (getFilenameFile maxFileName) -- get TXD from scene name
-- 		boxes are used as cull zones, so exclude
		if (superClassOf this == GeometryClass and classOf this != Box) then
		(
			setAttr newObj (getAttrIndex "Gta Object" "TXD") (getattr this (getattrindex "Gta Object" "TXD")) -- get TXD from copied object
			setAttr newObj (getAttrIndex "Gta Object" "LOD distance") (getattr this (getattrindex "Gta Object" "LOD distance"))
		)
		append copySet newObj
	)
	select copySet
	if copySet.count == 1 then
	(
		try (ADTools.InfoPanel.Text = "1 object duplicated")
		catch()
	)
	if copySet.count > 1 then
	(
		try (ADTools.InfoPanel.Text = (copySet.count as string + " objects duplicated"))
		catch()		
	)
	if copySet.count == 0 then
	(
		try (ADTools.InfoPanel.Text = "Nothing selected")
		catch()	
	)
)