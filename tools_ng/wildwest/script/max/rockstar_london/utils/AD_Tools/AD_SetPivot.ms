--Set Pivot
--Andy Davis �2010 Rockstar London
--Tool sets selected objects' pivots

fn AD_SetPivot mode=
(
	if selection.Count > 0 then
	(
		case (mode) of
		(
			"base":
			(
				for this in selection do (
				this.pivot = this.center
				this.pivot.z = this.min.z
				)
			)
			"center":
			(
				for this in selection do this.pivot = this.center
			)
			"origin":
			(
				for this in selection do this.pivot = [0,0,0]
			)
			default:
			(
				for this in selection do this.pivot = this.center
			)
		)
	)
	else
	(
		messagebox "nothing selected"
	)
)