--check for and create switch layers
result = LayerManager.getLayerFromName "switch1"
if (result == undefined) then
(
	LayerManager.newLayerFromName "switch1"
)
result = LayerManager.getLayerFromName "switch2"
if (result == undefined) then
(
	LayerManager.newLayerFromName "switch2"
)
--toggle switch states