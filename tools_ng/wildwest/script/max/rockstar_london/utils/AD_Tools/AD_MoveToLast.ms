-- objSet = for this in selection where (getAttrClass this == "Gta Object") and (classOf this != XrefObject) collect this
objSet = selection
if objSet.count > 1 then
(
	newSelection = #()
	objPosition = objSet[objSet.count].transform
	for i = 1 to (objSet.count - 1) do
	(
		objSet[i].transform = objPosition
		append newSelection objSet[i]
	)
	print ("transforms matched to " + objSet[objSet.count].name)
	select newSelection
)
else messagebox "Please select more than one object"