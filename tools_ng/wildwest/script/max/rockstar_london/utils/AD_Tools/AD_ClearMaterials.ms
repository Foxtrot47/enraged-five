--clear materials

objSet = for this in selection where (getAttrClass this == "Gta Object") and (classOf this != XrefObject) collect this
if objSet.count > 0 then
(
	for this in objSet do
	(
		this.material = undefined
	)
)