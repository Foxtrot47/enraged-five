/*
polyop.weldVertsByThreshold <Poly poly> <vertlist>
Welds the specified vertices that are within the threshold distance. The threshold distance is a property (weldThreshold) of the editable poly. Only vertices on a border can be welded.
*/

if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 1) then
(
	if $.selectedVerts.count > 1 then
	(
		numstart = $.selectedVerts.count
		$.weldThreshold = 0.01
		polyop.weldVertsByThreshold $ $.selectedVerts
		numend = $.selectedVerts.count
		if numstart - numend == 1 then
		(
			print "1 vertex welded"
		)
		else
		(
			print ((numstart - numend) as string + " vertices welded")
		)
	)
	else
	(
		messagebox "Please select more than one vertex"
	)
)
else
(
	messagebox "Please select some vertices"
)