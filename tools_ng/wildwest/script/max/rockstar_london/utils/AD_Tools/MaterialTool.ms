--MaterialTool.ms
--lists textures in selected object

--add clean up function: creates a new clean mm and sorts out ids
--add info for collision objects
--hook up apply/select
--link to official rexbound materials and material libraries according to level being worked on
--thumbnail of material where possible
--face id functionality
--pick function

filein (RsConfigGetWildWestDir() + "/script/max/rockstar_london/utils/RSL_dotNetUIOps.ms")
global MaterialTool

struct MaterialToolStruct
(
	Form,
	MMTable,
	SingleTable,
	Table,
	ToolTip,
	InfoPanel,
	ApplyButton,
	IniFilePath = (RsConfigGetWildWestDir() + "script/max/rockstar_london/config/AD_Tools.ini"),
	MaterialArray,
	ButtonStrip,
	RowHeight = 20,
	TargetMaterial,
	
	--preset definitions
	DS_Fill = (dotNetClass "System.Windows.Forms.DockStyle").Fill,
	FBS_SizableToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").SizableToolWindow,
	FBS_FixedToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").FixedToolWindow,
	FS_Flat = (dotNetClass "System.Windows.Forms.FlatStyle").Flat,
	FS_Standard = (dotNetClass "System.Windows.Forms.FlatStyle").Standard,
	SP_Manual =  (dotNetClass "System.Windows.Forms.FormStartPosition").Manual,
	Font_Calibri = dotNetObject "System.Drawing.Font" "Calibri" 8,
	TA_Center =  (dotNetClass "System.Drawing.ContentAlignment").MiddleCenter,
	TA_Left =  (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft,
	Padding_None = dotNetObject "System.Windows.Forms.Padding" 0,
	
	--colours
	RGB_Black = (dotNetClass "System.Drawing.Color").black,
	RGB_DarkGrey = (dotNetClass "System.Drawing.Color").fromARGB 102 102	102,
	RGB_Steel = (dotNetClass "System.Drawing.Color").FromARGB 200 210 220,
	RGB_Transparent = (dotNetClass "System.Drawing.Color").transparent,
	
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--GENERAL FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn Apply =
	(
		try
		(
			ADTools.InfoPanel.Text = "MaterialTool"
			MaterialTool.InfoPanel.Text = "MaterialTool"
		)
		catch()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn SaveIniFile =
	(
		setINISetting MaterialTool.IniFilePath "MaterialTool" "WinLocX" (MaterialTool.Form.Location.x as string)
		setINISetting MaterialTool.IniFilePath "MaterialTool" "WinLocY" (MaterialTool.Form.Location.y as string)
	),
	
	fn LoadIniFile =
	(
		--default values
		local WinLocX = 0
		local WinLocY = 40
		try
		(
			WinLocX = getINISetting MaterialTool.IniFilePath "MaterialTool" "WinLocX" as integer
			WinLocY = getINISetting MaterialTool.IniFilePath "MaterialTool" "WinLocY" as integer
		)
		catch()
		Form.Location = dotNetObject "system.drawing.point" WinLocX WinLocY
	),
	
	fn InitLabel tag align =
	(
		NewLabel = dotNetObject "Label"
		NewLabel.Text = tag
		NewLabel.Tag = tag
		NewLabel.TextAlign = align
		NewLabel.Font = Font_Calibri
		NewLabel.Dock = DS_Fill
		NewLabel.Margin = Padding_None
		return NewLabel
	),
	
	fn InitButton tag =
	(
		NewButton = dotNetObject "Button"
		NewButton.Tag = tag
		NewButton.FlatStyle = FS_Standard
		NewButton.BackColor = RGB_Transparent
		NewButton.Text = tag
		NewButton.Dock = DS_Fill
		NewButton.Margin = Padding_None
		NewButton.Font = Font_Calibri
		NewButton.TextAlign = TA_Center
		return NewButton
	),
	
	fn RefreshTable_Single Target =
	(
		SingleTable.Controls.Clear()
		SingleTable.Controls.Add (InitLabel "Material" TA_Left) 0 0
		SingleTable.Controls.Add (InitLabel "Type" TA_Left) 1 0
		if Target == "undefined" then
		(
			MaterialTool.InfoPanel.Text = "No geometry selected"
			SingleTable.Controls.Add (InitLabel "not applicable" TA_Left) 0 1
			SingleTable.Controls.Add (InitLabel "not applicable" TA_Left) 1 1
		)
		else
		(
			MaterialTool.InfoPanel.Text = (classOf Target as string + ": " + Target.name)
			if Target.material == undefined then
			(
				MaterialName = "none"
				MaterialType = "not applicable"
			)
			else
			(
				MaterialName = Target.material.name
				MaterialType = classOf Target.material as String
			)
			SingleTable.Controls.Add (InitLabel MaterialName TA_Left) 0 1
			SingleTable.Controls.Add (InitLabel MaterialType TA_Left) 1 1
		)
		FormHeight = (63 + 2 * RowHeight)
		MaterialTool.Form.Size = dotNetObject "System.Drawing.Size" 240 FormHeight
		MaterialTool.Table.Controls.Add SingleTable 0 0
		MaterialTool.Table.Controls.Add ButtonStrip 0 1
	),
	
	fn RefreshTable_Multimaterial Target =
	--processes the table contents
	(
		MaterialTool.MaterialArray = #()
		RowHeight = 20
		MMTable = dotNetObject "TableLayoutPanel"
		MMTable.Dock = DS_Fill
		MMTable.Autoscroll = true
		MMTable.RowCount = 1
		MMTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" RowHeight)
		MMTable.ColumnCount = 6
		MMTable.CellBorderStyle = MMTable.CellBorderStyle.Single		
		MMTable.columnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 25)
		MMTable.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		MMTable.columnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 140)
		MMTable.columnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 85)
		MMTable.columnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 50)
		MMTable.columnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 50)
		MMTable.Controls.Add (InitLabel "ID" TA_Center) 0 0
		MMTable.Controls.Add (InitLabel "Material" TA_Center) 1 0
		MMTable.Controls.Add (InitLabel "Name" TA_Center) 2 0
		MMTable.Controls.Add (InitLabel "Type" TA_Center) 3 0
		MMTable.Controls.Add (InitLabel "Apply" TA_Center) 4 0
		MMTable.Controls.Add (InitLabel "Select" TA_Center) 5 0
		MaterialCount = Target.material.materialList.count
		MMNumCols = MMTable.ColumnCount
		MMTable.RowCount = MaterialCount
		
		for i = 1 to MaterialCount do
		(
			MMTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" RowHeight)
			IDLabel = InitLabel "ID" TA_Left
			IDLabel.TextAlign = TA_Center
			IDLabel.Text = (Target.material.materialIDList[i] as string)
			MMTable.Controls.Add IDLabel 0 i
			MaterialLabel = InitLabel "Material" TA_Left
			MaterialLabel.Text = Target.material.materialList[i].name
			MMTable.Controls.Add MaterialLabel 1 i
			NameLabel = InitLabel "Name" TA_Left
			NameLabel.Text = Target.material.names[i]
			MMTable.Controls.Add NameLabel 2 i
			TypeLabel = InitLabel "Type" TA_Center
			TypeLabel.Text = classOf Target.material.materialList[i] as string
			MMTable.Controls.Add TypeLabel 3 i
			ApplyButton = InitButton "Apply"
			MMTable.Controls.Add ApplyButton 4 i
			SelectButton = InitButton "Select"
			MMTable.Controls.Add SelectButton 5 i
			--row striping
			if (mod i 2 == 0) then
			(
				MMTable.Controls.Item[(i * MMNumCols) + 0].BackColor = 	(dotNetClass "System.Drawing.Color").FromARGB 180 185 190
				MMTable.Controls.Item[(i * MMNumCols) + 1].BackColor = 	(dotNetClass "System.Drawing.Color").FromARGB 190 195 200
				MMTable.Controls.Item[(i * MMNumCols) + 2].BackColor = 	(dotNetClass "System.Drawing.Color").FromARGB 200 205 210
				MMTable.Controls.Item[(i * MMNumCols) + 3].BackColor = 	(dotNetClass "System.Drawing.Color").FromARGB 210 215 220
			)
			else
			(
				MMTable.Controls.Item[(i * MMNumCols) + 0].BackColor = 	(dotNetClass "System.Drawing.Color").FromARGB 190 195 200
				MMTable.Controls.Item[(i * MMNumCols) + 1].BackColor = 	(dotNetClass "System.Drawing.Color").FromARGB 200 205 210
				MMTable.Controls.Item[(i * MMNumCols) + 2].BackColor = 	(dotNetClass "System.Drawing.Color").FromARGB 210 215 220
				MMTable.Controls.Item[(i * MMNumCols) + 3].BackColor = 	(dotNetClass "System.Drawing.Color").FromARGB 220 225 230
			)
		)
		
		FormHeight = 62 + (MaterialCount + 1) * (RowHeight + 1)
		MaterialTool.Form.Height = FormHeight
		MaterialTool.Form.Size = dotNetObject "System.Drawing.Size" 512 FormHeight
		MaterialTool.Table.Controls.Add MMTable 0 0
		MaterialTool.Table.Controls.Add ButtonStrip 0 1
	),
	
	fn RefreshMaterialTool =
	--processes the selected object
	(
		MaterialTool.Table.Controls.Clear()
		ObjSet = for this in selection where superClassOf this == GeometryClass collect this
		Target = "undefined"
		if ObjSet.count == 1 then
		(
			MaterialType = "none"
			Target = ObjSet[1]
			
			if Target.material == undefined then
			(
				MaterialType == "undefined"
				MaterialTool.InfoPanel.Text = ("Object: " + Target.Name + "\nMaterial: " + "none")
			)
			else
			(
				MaterialTool.TargetMaterial = Target.Material
				MaterialTool.Form.Tag = dotNetMXSValue (Target.material)
				MaterialName = Target.material.name
				MaterialType = classOf Target.material as string
				MaterialTool.InfoPanel.Text = ("Object: " + Target.Name + "\nMaterial: " + MaterialName)
			)
			if MaterialType == "Multimaterial" then
			(
				MaterialTool.RefreshTable_Multimaterial Target
			)
			else MaterialTool.RefreshTable_Single Target
		)
		else
		(
			ColSet = for this in selection where ClassOf this == "Gta Collision" collect this
			--if blah blah do collision info...
			
			--otherwise
			MaterialTool.RefreshTable_Single Target
		)
	),
	
	fn Clean =
	(
		MMClean.fix MaterialTool.TargetMaterial
		MaterialTool.RefreshMaterialTool()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn CreateUI =
	(
		-- form setup		
		Form = dotNetObject "maxCustomControls.maxForm"
		Form.Text = "Material Tool"
		Form.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").manual
		Form.MaximumSize = dotNetObject "System.Drawing.Size" 1600 1200
		Form.Location = dotNetObject "system.drawing.point" 0 80
		Form.FormBorderStyle = FBS_FixedToolWindow
		Form.SizeGripStyle = (dotNetClass "SizeGripStyle").show
		Form.StartPosition = SP_Manual
		MaterialTool.Form.Size = dotNetObject "System.Drawing.Size" 470 (74 + 2 * RowHeight)
		dotNet.AddEventHandler Form "Load" LoadIniFile
		dotNet.AddEventHandler Form "Closing" SaveIniFile
		
		--content
		ToolTip = dotnetobject "ToolTip"
		Table = dotNetObject "TableLayoutPanel"
		Table.Dock = DS_Fill
		Form.Controls.Add Table
		Table.RowCount = 2
		Table.rowStyles.add (RS_dotNetObject.rowStyleObject "percent" 100)
		Table.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		Table.ColumnCount = 1
		Table.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		Form.Controls.Add Table
		
		ButtonStrip = dotNetObject "TableLayoutPanel"
		ButtonStrip.Dock = DS_Fill
		ButtonStrip.ColumnCount = 3
		ButtonStrip.columnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 50)
		ButtonStrip.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		ButtonStrip.columnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 50)
		ButtonStrip.RowCount = 1
		ButtonStrip.rowStyles.add (RS_dotNetObject.rowStyleObject "percent" 100)
		RefreshButton = InitButton "Refresh"
		dotNet.AddEventHandler RefreshButton "Click" RefreshMaterialTool
		ButtonStrip.Controls.Add RefreshButton 0 0
		InfoPanel = (InitLabel "info panel" TA_Left)
		ButtonStrip.Controls.Add InfoPanel 1 0
		CleanButton = InitButton "Clean"
		ButtonStrip.Controls.Add CleanButton 2 0
		dotNet.addEventHandler CleanButton "Click" Clean
		
		SingleTable = dotNetObject "TableLayoutPanel"
		SingleTable.Dock = DS_Fill
		SingleTable.Autoscroll = true
		SingleTable.ColumnCount = 2
		SingleTable.CellBorderStyle = SingleTable.CellBorderStyle.Single
		SingleTable.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		SingleTable.columnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 100)
		SingleTable.RowCount = 2
		SingleTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" RowHeight)
		SingleTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" RowHeight)
		
		RefreshMaterialTool()
		--draw form
		Form.ShowModeless()
		Form
	)
)

if MaterialTool != undefined then
(
	MaterialTool.Form.Close()
)

MaterialTool = MaterialToolStruct()
MaterialTool.CreateUI()