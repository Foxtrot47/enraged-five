--bridge two edges

--<bool><EditablePoly>.Bridge selLevel:<enum> flag:<DWORD>
-- selLevel enums: {#Object|#Vertex|#Edge|#Border|#Face|#Element|#CurrentLevel}
-- selLevel default value: #CurrentLevel
-- flag default value: 1

if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 2) then
(
	selection[1].bridgeSegments = 1
	selection[1].bridge selLevel:#Edge
)

else
(
	if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 4) then
	(
		selection[1].bridgeSegments = 1
		selection[1].bridge selLevel:#Face
	)
	else messagebox "Select edges or faces to bridge"
)