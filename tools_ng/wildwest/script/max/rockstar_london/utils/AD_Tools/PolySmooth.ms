--PolySmooth.ms
fn PolySmooth threshold =
(
	threshold = threshold as integer
	if (selection.count == 1) and (classOf selection[1] == Editable_Poly) then
	(
		setCommandPanelTaskMode #modify --necessary to obtain subObjectLevel
		subObjectMode = subObjectLevel
		if subObjectLevel != 4 then selection[1].ConvertSelection #CurrentLevel #Face
		if threshold > 180 then threshold = 180
		if threshold < 0 then threshold = 0
		selection[1].autoSmoothThreshold = threshold
		polyop.autoSmooth selection[1]
		subObjectLevel = subObjectMode
		infoString = ("Selection smoothed to " + threshold as string + "�")
		try
		(
			ADTools.InfoPanel.Text = infoString
		)
		catch
		(
			messagebox infoString
		)
	)
	else
	(
		try
		(
			ADTools.InfoPanel.Text = "Please select an editable poly"
		)
		catch
		(
			messagebox "Please select an editable poly"
		)
	)
)