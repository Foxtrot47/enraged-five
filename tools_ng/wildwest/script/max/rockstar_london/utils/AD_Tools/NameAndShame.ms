objString = ""
infoString = ""
if selection.count > 0 then
(
	objString = selection[1].name
	infoString = objString
	if selection.count > 1 then
	(
		for i = 2 to selection.count do
		(
			objString = (objString + "\n" + selection[i].name)
			infoString = (infoString + ", " + selection[i].name)
		)
	)
)
else
(
	objString = "Nothing selected"
	infoString = objString
)
-- messagebox objString
try
(
	ADTools.InfoPanel.Text = infoString
)
catch()