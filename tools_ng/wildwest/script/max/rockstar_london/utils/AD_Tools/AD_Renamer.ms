global InitRenamer
--replace Renamer with the name of the control

struct RenamerStruct
(
	RenamerWin,
	patternString,
	pattern, separator, startNumber, padding,
	
	--preset definitions
	DS_fill = (dotNetClass "System.Windows.Forms.DockStyle").fill,
	TA_center =  (dotNetClass "System.Drawing.ContentAlignment").MiddleCenter,
	TA_left =  (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft,
	limeGreen = (dotNetClass "System.Drawing.Color").fromARGB 204 255 0,
	yellowOrange = (dotNetClass "System.Drawing.Color").fromARGB 255 204 0,
	shockingPink = (dotNetClass "System.Drawing.Color").fromARGB 255 0 204,
	fullBlack = (dotNetClass "System.Drawing.Color").fromARGB 0 0 0,
	darkGrey = (dotNetClass "System.Drawing.Color").fromARGB 102 102	102,
	midGrey = (dotNetClass "System.Drawing.Color").fromARGB 153 153 153,
	paleGrey = (dotNetClass "System.Drawing.Color").fromARGB 204 204 204,
	lightGrey = (dotNetClass "System.Drawing.Color").fromARGB 229 229 229,
	offWhite = (dotNetClass "System.Drawing.Color").fromARGB 241 241 241,
	fullWhite = (dotNetClass "System.Drawing.Color").fromARGB 255 255 255,
	darkRed = (dotNetClass "System.Drawing.Color").fromARGB 102 0 0,
	
	fn GetPattern =
	(
		if padding == 1 then thenumber = startNumber as string
		template = pattern + separator
	),
	
	fn CreateUI =
	(		
		-- form setup
		clearListener()
		RenamerWin = RS_dotNetUI.maxForm "Renamer" text:"Renamer" size:[200,200] min:[200,200] max:[200,200]
		RenamerWin.sizeGripStyle = (dotNetClass "SizeGripStyle").show
		RenamerWin.startPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").manual
		
		--table setup
		MainTable = RS_dotNetUI.tableLayout "MainTable"
		MainTable.backColor = darkGrey
		MainTable.dock = DS_fill
		MainTable.cellBorderStyle = (dotNetClass "System.Windows.Forms.TableLayoutPanelCellBorderStyle").none
		MainTable.columnCount = 2
		MainTable.columnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 80)
		MainTable.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		MainTable.rowCount = 6
		MainTable.rowStyles.add (RS_dotNetObject.rowStyleObject "percent" 16)
		MainTable.rowStyles.add (RS_dotNetObject.rowStyleObject "percent" 16)
		MainTable.rowStyles.add (RS_dotNetObject.rowStyleObject "percent" 16)
		MainTable.rowStyles.add (RS_dotNetObject.rowStyleObject "percent" 16)
		MainTable.rowStyles.add (RS_dotNetObject.rowStyleObject "percent" 16)
		MainTable.rowStyles.add (RS_dotNetObject.rowStyleObject "percent" 16)
		RenamerWin.controls.add MainTable
		
		--content
		label1 = dotNetObject "label"
		label1.text = "Pattern"
		label1.dock = DS_fill
		label1.ForeColor = fullWhite
		label1.TextAlign = TA_Center
		MainTable.controls.add label1 0 0
		
		pattern = dotNetObject "TextBox"
		pattern.text = "newname"
		pattern.MaxLength = 16
		pattern.dock = DS_fill
		MainTable.controls.add pattern 1 0
		
		label2 = dotNetObject "label"
		label2.text = "Separator"
		label2.dock = DS_fill
		label2.ForeColor = fullWhite
		label2.TextAlign = TA_Center
		MainTable.controls.add label2 0 1
		
		separator = dotNetObject "TextBox"
		separator.text = "_"
		separator.MaxLength = 1
		MainTable.controls.add separator 1 1
		
		label3 = dotNetObject "label"
		label3.text = "Start number"
		label3.dock = DS_fill
		label3.ForeColor = fullWhite
		label3.TextAlign = TA_Center
		MainTable.controls.add label3 0 2
		
		startNumber = dotNetObject "NumericUpDown"
		startNumber.Value = 1
		MainTable.controls.add startNumber 1 2
		
		label4 = dotNetObject "label"
		label4.text = "Padding"
		label4.dock = DS_fill
		label4.ForeColor = fullWhite
		label4.TextAlign = TA_Center
		MainTable.controls.add label4 0 3
		
		padding = dotNetObject "NumericUpDown"
		padding.Value = 1
		padding.Minimum = 1
		padding.Maximum = 4
		MainTable.controls.add padding 1 3
		
		patternString = "Template: " + pattern.text
		template = dotNetObject "label"
		template.text = patternString
		template.dock = DS_fill
		template.foreColor = fullWhite
		template.TextAlign = TA_left
		MainTable.controls.add template 0 4
		MainTable.setColumnSpan template 2
		
		doit = dotNetObject "Button"
		doit.dock = DS_fill
		doit.text = "Do it..."
		doit.backColor = limeGreen
		MainTable.controls.add doit 0 5
		MainTable.setColumnSpan doit 2
		--showProperties label1
		
		--draw form
		RenamerWin.showModeless()
		RenamerWin
	)
)

if InitRenamer != undefined then
(
	InitRenamer.RenamerWin.close()
)

InitRenamer = RenamerStruct()
InitRenamer.CreateUI()