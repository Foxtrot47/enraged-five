--Extrude.ms
--script does context sensitve extrude

if selection.count > 0 then
(
	setCommandPanelTaskMode #modify
	if subObjectLevel == 0 then
	(
		for this in selection do
		(
			if classOf this == line then
			(
-- 				addModifier this (Extrude amount:1)
				convertToPoly this
				subObjectLevel = 4
				macros.run "Editable Polygon Object" "EPoly_Extrude"
			)
		)
	)
	if subObjectLevel ==4 then
	(
		convertToPoly selection[1]
		subObjectLevel = 4
		macros.run "Editable Polygon Object" "EPoly_Extrude"
	)
	if subObjectLevel != 0 and subObjectLevel != 4 then
	(
		messagebox "Please select a shape or a face"
	)
)
else
(
	messagebox "Please select an object"
)