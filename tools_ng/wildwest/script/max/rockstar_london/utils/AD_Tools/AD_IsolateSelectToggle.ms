try
(
	if Iso2Roll.open then
	(
		DestroyDialog Iso2Roll
	)
	else
	(
		--if uberhelper exists, add uberhelper to isolation set
		objset = selection
		fullset = objset
		if (isValidNode $uberhelper) and (classOf $uberhelper == Point) then appendIfUnique fullset $uberhelper
		select fullset
		CreateDialog Iso2Roll
	-- 	SetDialogPos Iso2Roll [1530,80]
		select objset
	)
)
catch
(
	CreateDialog Iso2Roll
)