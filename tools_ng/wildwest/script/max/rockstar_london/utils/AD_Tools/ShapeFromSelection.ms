--ShapeFromSelection.ms

--if face selected, deletes face, and converts to shape
--if edge selected, converts to shape

if (selection.count == 1) and superClassOf selection[1] == GeometryClass then
(
	newShape
	subObjectMode = subObjectLevel
	if classOf selection[1] != Editable_Poly then convertToPoly selection[1]
	case subObjectLevel of
	(
		2:	--edge
		(
			if selection[1].selectedEdges.count > 0 then
			(
				polyop.createShape selection[1] selection[1].selectedEdges name:(selection[1].name + "_shape")
				select (for this in shapes where this.name == (selection[1].name + "_shape") collect this)
				try(ADTools.InfoPanel.Text = "Shape created from edge")
				catch()
			)
			else
			(
				try(ADTools.InfoPanel.Text = "No edges selected")
				catch()
			)
		)
		4:	--face
		(
			if selection[1].selectedFaces.count > 0 then
			(
				selection[1].ConvertSelection #CurrentLevel #Edge
				polyop.deleteFaces selection[1] selection[1].selectedFaces
				subObjectLevel = 2
				polyop.createShape selection[1] selection[1].selectedEdges name:(selection[1].name + "_shape")
				select (for this in shapes where this.name == (selection[1].name + "_shape") collect this)
				subObjectLevel = 3
				splineops.close selection[1]
				subObjectLevel = 0
				try(ADTools.InfoPanel.Text = "Shape created from face")
				catch()
			)
			else
			(
				try(ADTools.InfoPanel.Text = "No faces selected")
				catch()
			)
		)
		default:
		(
			try(ADTools.InfoPanel.Text = "Op failed: Please select a face or an edge")
			catch()
		)
	)
)
else
(
	try(ADTools.InfoPanel.Text = "Op failed: Please select an object")
	catch()
)