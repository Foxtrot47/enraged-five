--SetCollisionType.ms

fn SetCollisionType type =
(
	objset = for this in selection where getAttrClass this == "Gta Collision" collect this
	if objset.count > 0 then
	(
		collisionWirecolor = (color 255 127.5 0)
		case type of
		(
			"default":
			(
				for this in objset do
				(
					this.wirecolor = collisionWirecolor
					setattr this (getattrindex "Gta Collision" "Coll Type") "DEFAULT"
				)
			)
			"physics":
			(
				for this in objset do
				(
					this.wirecolor = collisionWirecolor
					setattr this (getattrindex "Gta Collision" "Camera") false
					setattr this (getattrindex "Gta Collision" "Physics") true
					setattr this (getattrindex "Gta Collision" "Hi Detail") true
					setattr this (getattrindex "Gta Collision" "Loco") false
				)
			)
			"camera":
			(
				for this in objset do
				(
					this.wirecolor = (color 192 255 0)
					setattr this (getattrindex "Gta Collision" "Camera") true
					setattr this (getattrindex "Gta Collision" "Physics") false
					setattr this (getattrindex "Gta Collision" "Hi Detail") false
					setattr this (getattrindex "Gta Collision" "Loco") false
					setattr this (getattrindex "Gta Collision" "Coll Type") "DEFAULT"
				)
			)
			"loco":
			(
				for this in objset do
				(
					this.wirecolor = collisionWirecolor
					setattr this (getattrindex "Gta Collision" "Camera") false
					setattr this (getattrindex "Gta Collision" "Physics") false
					setattr this (getattrindex "Gta Collision" "Hi Detail") true
					setattr this (getattrindex "Gta Collision" "Loco") true
				)
			)
			"low detail":
			(
				for this in objset do
				(
					this.wirecolor = collisionWirecolor
					setattr this (getattrindex "Gta Collision" "Camera") false
					setattr this (getattrindex "Gta Collision" "Physics") true
					setattr this (getattrindex "Gta Collision" "Hi Detail") false
					setattr this (getattrindex "Gta Collision" "Loco") false
					setattr this (getattrindex "Gta Collision" "Coll Type") "DEFAULT"
				)
			)
			"combi":
			(
				for this in objset do
				(
					this.wirecolor = collisionWirecolor
					setattr this (getattrindex "Gta Collision" "Camera") true
					setattr this (getattrindex "Gta Collision" "Physics") true
					setattr this (getattrindex "Gta Collision" "Hi Detail") true
					setattr this (getattrindex "Gta Collision" "Loco") false
				)
			)
		)
		try (ADTools.InfoPanel.Text = ("Collision set to ") + type + " collision")
		catch()
	)
	else
	(
		try (ADTools.InfoPanel.Text = "No collision selected")
		catch()
	)
)