if selection.count > 0 then
(
	newset = #()
	for this in selection do
	(
		print this.name
		if this.parent != undefined then
		(
			appendIfUnique newset this.parent
		)
	)
	select newset
)
else
(
	messagebox "[Select parent]\nplease select an object"
)