--ProcessCollision.ms

fn ProcessCollision type =
(
	-- select the world collision poly object, then the target object
	
	objSet = for this in selection where superClassOf this == GeometryClass collect this
	if objSet.count == 2 then
	(
		--create world collision
		if (classOf objSet[1] != Editable_Poly) then convertToPoly objSet[1]
		if (classOf objSet[2] != Editable_Poly) then convertToPoly objSet[2]
		mesh2col objSet[1]
		setattr objSet[1] (getattrindex "Gta Collision" "Camera") false
		setattr objSet[1] (getattrindex "Gta Collision" "Physics") true
		setattr objSet[1] (getattrindex "Gta Collision" "Hi Detail") true
		objSet[1].pivot = objSet[2].pivot
		objSet[1].parent = objSet[2]
		objSet[1].name = (uniqueName (objSet[2].name + "_col"))
		objSet[1].wirecolor = orange
		objSet[2].wirecolor = (color 128 0 128)
		
		--create camera collision
		local camcol
		colsize = #(objSet[2].max.x - objSet[2].min.x, objSet[2].max.y - objSet[2].min.y, objSet[2].max.z - objSet[2].min.z)
		case type of
		(
			"box":
			(
				camcol = Col_Box name:(uniqueName (objSet[2].name + "_camcol"))
				camcol.length = colsize[1] * 0.5
				camcol.width = colsize[2] * 0.5
				camcol.height = colsize[3] * 0.5
				camcol.transform = objSet[2].transform
				camcol.wirecolor = orange
				camcol.position = objSet[2].center
				camcol.parent = objSet[2]
				setattr camcol (getattrindex "Gta Collision" "Camera") true
				setattr camcol (getattrindex "Gta Collision" "Physics") false
				setattr camcol (getattrindex "Gta Collision" "Hi Detail") false
				print (objSet[1].name + " > " + objSet[2].name + ", " + camcol.name)
			)
			"xcylinder":
			(
				camcol = Col_Cylinder name:(uniqueName (objSet[2].name + "_camcol"))
				if colsize[2] > colsize[3] then camcol.radius = colsize[2] * 0.5
				else camcol.radius = colsize[3] * 0.5
				camcol.length = colsize[1]
				camcol.transform = objSet[2].transform
				in coordsys local rotate camcol (eulerangles 0 0 90)
				camcol.wirecolor = orange
				camcol.position = objSet[2].center
				camcol.parent = objSet[2]
				setattr camcol (getattrindex "Gta Collision" "Camera") true
				setattr camcol (getattrindex "Gta Collision" "Physics") false
				setattr camcol (getattrindex "Gta Collision" "Hi Detail") false
				print (objSet[1].name + " > " + objSet[2].name + ", " + camcol.name)
			)
			"ycylinder":
			(
				camcol = Col_Cylinder name:(uniqueName (objSet[2].name + "_camcol"))
				if colsize[1] > colsize[3] then camcol.radius = colsize[1] * 0.5
				else camcol.radius = colsize[3] * 0.5
				camcol.length = colsize[2]
				camcol.transform = objSet[2].transform
				camcol.wirecolor = orange
				camcol.position = objSet[2].center
				camcol.parent = objSet[2]
				setattr camcol (getattrindex "Gta Collision" "Camera") true
				setattr camcol (getattrindex "Gta Collision" "Physics") false
				setattr camcol (getattrindex "Gta Collision" "Hi Detail") false
				print (objSet[1].name + " > " + objSet[2].name + ", " + camcol.name)
			)
			"zcylinder":
			(
				camcol = Col_Cylinder name:(uniqueName (objSet[2].name + "_camcol"))
				if colsize[2] > colsize[1] then camcol.radius = colsize[2] * 0.5
				else camcol.radius = colsize[1] * 0.5
				camcol.length = colsize[3]
				camcol.transform = objSet[2].transform
				in coordsys local rotate camcol (eulerangles 90 0 0)
				camcol.wirecolor = orange
				camcol.position = objSet[2].center
				camcol.parent = objSet[2]
				setattr camcol (getattrindex "Gta Collision" "Camera") true
				setattr camcol (getattrindex "Gta Collision" "Physics") false
				setattr camcol (getattrindex "Gta Collision" "Hi Detail") false
				print (objSet[1].name + " > " + objSet[2].name + ", " + camcol.name)
			)
			"sphere":
			(
				camcol = Col_Sphere name:(uniqueName (objSet[2].name + "_camcol"))
				local newradius = colsize[1]
				if colsize[2] > newradius then newradius = colsize[2]
				if colsize[3] > newradius then newradius = colsize[3]
				camcol.radius = newradius * 0.5				
				camcol.transform = objSet[2].transform
				in coordsys local rotate camcol (eulerangles 90 0 0)
				camcol.wirecolor = orange
				camcol.position = objSet[2].center
				camcol.parent = objSet[2]
				setattr camcol (getattrindex "Gta Collision" "Camera") true
				setattr camcol (getattrindex "Gta Collision" "Physics") false
				setattr camcol (getattrindex "Gta Collision" "Hi Detail") false
				print (objSet[1].name + " > " + objSet[2].name + ", " + camcol.name)
			)
			"none":
			(
				print "no camera collision"
			)
		)
		select objSet[2]
	)
	else messagebox "Please select two objects"
)