fn resetXFormU obj = 
(
	-- Get the original node transform matrix
	local ntm = obj.transform
	
	-- The new object transform only inherits position
	obj.transform=transMatrix obj.pos
	
	-- Compute the pivot transform matrix
	local piv=obj.objecttransform * inverse obj.transform
	
	-- Reset the object offsets to 0
	obj.objectoffsetPos  = [0,0,0]
	obj.objectoffsetRot = (quat 0 0 0 1)
	obj.objectoffsetScale = [1,1,1]

	-- Take the position out of the original node transform matrix since we don't reset position
	ntm.translation=[0,0,0]
	
	-- Apply the pivot transform matrix to the modified original node transform matrix
	-- to get the XForm gizmo transform
	ntm = piv * ntm
	
	-- apply an XForm modifier to the node
	local xformMod=xform()
	addmodifier obj xformMod
	
	-- set the XForm modifier's gizmo tranform
	xformMod.gizmo.transform=ntm
	
	-- return value of ok
	ok
)

for nObject in selection do
(
	resetXFormU nObject
)
collapseStack(selection)