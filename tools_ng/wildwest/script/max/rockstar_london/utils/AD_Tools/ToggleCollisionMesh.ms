--ToggleCollisionMesh.ms
--script toggles selected meshes between collision mesh and editable poly
--�2010 Rockstar London
--Author: Andrew Davis
--Revision 1: 14/12/2010

if selection.count > 0 then
(
	for this in selection do
	(
		if (getAttrClass this == "Gta Collision") then
		(
			setUserProp this "CollisionMaterial" (getAttr this (getAttrIndex "Gta Collision" "Coll Type"))
			col2mesh this
			convertToPoly this
			this.wirecolor = (color 128 128 128)
			print (this.name + " converted to poly")
			try (ADTools.InfoPanel.Text = (this.name + " converted to poly"))
			catch()
		)
		else
		(
			if (superClassOf this == GeometryClass) then
			(
				local colParent = this.parent
				this.parent = undefined
				convertToPoly this
				mesh2col this
				this.parent = colParent
				this.wirecolor = orange
				local CollisionMaterial = getUserProp this "CollisionMaterial"
				if (CollisionMaterial != undefined) then
				(
					setAttr this (getAttrIndex "Gta Collision" "Coll Type") CollisionMaterial
				)
				print (this.name + " converted to collision object")
				try (ADTools.InfoPanel.Text = (this.name + " converted to collision object"))
				catch()
			)
		)
	)
)

else
(
	messagebox "Please select an object"
)