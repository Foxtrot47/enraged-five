-- Weld.ms
--Welds the specified vertices that are within the threshold distance. The threshold distance is a property (weldThreshold) of the editable poly. Only vertices on a border can be welded.


if (selection.count == 1) and (classOf $ == Editable_Poly) then
(
	subObjectMode = subObjectLevel
	if subObjectLevel != 1 then selection[1].ConvertSelection #CurrentLevel #Face
	if $.selectedVerts.count > 1 then
	(
		local thestring
		local numstart = $.selectedVerts.count
		$.weldThreshold = 0.01
		polyop.weldVertsByThreshold $ $.selectedVerts
		local numend = $.selectedVerts.count
		if numstart - numend == 1 then
		(
			thestring = "1 vertex welded"
			print thestring
		)
		else
		(
			thestring = ((numstart - numend) as string + " vertices welded")
		)
		try
		(
			ADTools.infoPanel.Text = thestring
		)
		catch
		(
			print thestring
		)
	)
	else
	(
		messagebox "Please select more than one vertex"
	)
	subObjectLevel = subObjectMode
)
else
(
	messagebox "Please select some vertices"
)