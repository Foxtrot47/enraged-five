--VertexEdgeModeToggle.ms

if selection.count == 1 and ((classOf selection[1] == Editable_Poly) or (classOf selection[1] == Editable_Mesh)) then
(
	setCommandPanelTaskMode  #modify
	if subObjectLevel != 1 then
	(
		subObjectLevel = 1
		try
		(
			ADTools.InfoPanel.Text = "Vertex mode"
		)
		catch
		(
			print "Vertex mode"
		)
	)
	else
	(
		subObjectLevel = 2
		try
		(
			ADTools.InfoPanel.Text = "Edge mode"
		)
		catch
		(
			print "Edge mode"
		)
	)
)