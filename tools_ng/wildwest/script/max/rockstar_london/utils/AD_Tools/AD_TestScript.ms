----------------------------------------------------------------------------------------------------
--FUNCTION DEFINTIONS
----------------------------------------------------------------------------------------------------

fn ApplyMaterialIDToSelection myID =
(
	modPanel.addModToSelection  (materialModifier materialID: myID)
)

fn ApplyMaterialIDToObject myID =
(
	addModifier $ (materialModifier materialID:myID)
)

fn ParentAndMove =
(
	if selection.count == 2 then
	(
		selection[1].parent = selection[2]
		selection[1].pos = selection[2].pos
	)
)

fn RingConnect =
(
	$.ringEdgeSel()
	$.ConnectEdges()
)

fn ProcessCollisionSpline matSlot matID =
(
	if selection.count == 2 and classOf selection[1] == line and (classOf selection[2] == Editable_Poly or classOf selection[2] == PolyMeshObject or classOf selection[2] == Editable_mesh) then
	(
		--variables initialised
		splineObj = selection[1]
		targetObj = selection[2]
		nodeBBox = nodeLocalBoundingBox targetObj
		minCoords = nodeBBox[1]
		maxCoords = nodeBBox[2]
		targetObjDepth = maxCoords[2] - minCoords[2]
	
		--spline processed
		ConvertToPoly(splineObj)
		select splineObj
		setCommandPanelTaskMode #modify
		subObjectLevel = 4
		polyOp.extrudeFaces splineObj #(1) targetObjDepth
		subObjectLevel = 0
		addModifier splineObj (Cap_Holes())
		splineObj.material = meditmaterials[matSlot]
		addModifier splineObj (materialModifier materialID:matID)
		collapseStack splineObj
		splineObj.name = (targetObj.name + "_coll")
		splineObj.pivot = targetObj.pivot
		filein (RsConfigGetWildWestDir() + "script\\max\\rockstar_london\\utils\\AD_Tools\\AD_ResetXForm.ms")
		splineObj.parent = targetObj
		splineObj.wirecolor = (color 255 174 0)
		select splineObj
	)
	else
	(
		messagebox "Select a spline and a target"
	)
)

fn executeStringCommand stringCommand =
(
-- 	stringCommand = "print (\"yay\")"
	execute stringCommand
)

fn EditPolyCrown =
(
	inObject = $
	modifierType = modPanel.getCurrentObject()
	if classOf modifierType != Editable_Poly and classOf modifierType != Editable_mesh and classOf modifierType != Edit_Poly then
	(
		addModifier inObject (Edit_Poly())
	)
)

fn SetTXD TXDName =
(
	for this in selection where ((getAttrClass this) == "Gta Object") and (classOf this != XrefObject) do
	(
		setattr this (getattrindex "Gta Object" "TXD") TXDName
	)
	print ("New TXD name set to " + TXDName as string)
)

fn SetLODDistance newvalue =
(
	for this in selection where ((getAttrClass this) == "Gta Object") and (classOf this != XrefObject) do
	(
		setattr this (getattrindex "Gta Object" "LOD distance") newvalue
	)
	print ("New LOD distance set to " + newvalue as string)
)

fn SelectMinLODValue value =
(
	selset = #()
	objset = for this in objects where ((getAttrClass this) == "Gta Object") collect this
	for this in objset where ((getattr this (getattrindex "Gta Object" "LOD distance")) > 300) do
	(
		append selset this
	)
	select selset
	print selset.count
)

fn SelectNoneExportables =
(
	noneExportables = #()
	for this in objects where ((getAttrClass this) == "Gta Object") do
	(
		if ((getattr this (getattrindex "Gta Object" "Dont Export")) == true) then append noneExportables this
	)
	select noneExportables
)

fn SelectCollision =
(
	select (for this in objects where ((getAttrClass this) == "Gta Collision") collect this)
)

fn SetPhysicsCollisionFlags =
(
	for this in selection where ((getAttrClass this) == "Gta Collision") do
	(
		setattr this (getattrindex "Gta Collision" "Camera") false
		setattr this (getattrindex "Gta Collision" "Physics") true
		setattr this (getattrindex "Gta Collision" "Hi Detail") true
		this.wirecolor = (color 128 0 128)
		print ("physics collision flags set for " + this.name)
		ADTools.InfoPanel.Text = ("physics collision flags set for: " + this.name)
	)
)

fn SetCameraCollisionTrue =
(
	for this in selection where ((getAttrClass this) == "Gta Collision") do
	(
		setattr this (getattrindex "Gta Collision" "Camera") true
		print ("camera collision flag set for " + this.name)
		ADTools.InfoPanel.Text = ("camera collision flag set true for: " + this.name)
	)
)

fn SetPhysicsCollisionTrue =
(
	for this in selection where ((getAttrClass this) == "Gta Collision") do
	(
		setattr this (getattrindex "Gta Collision" "Camera") false
		setattr this (getattrindex "Gta Collision" "Physics") true
		setattr this (getattrindex "Gta Collision" "Hi Detail") true
		print ("physics collision flags set for " + this.name)
		ADTools.InfoPanel.Text = ("physics collision flags set true for: " + this.name)
	)
)

fn SetCollisionMaterial =
(
-- 	collmaterial = "METAL_SOLID_MEDIUM"
-- 	collmaterial = "METAL_ELECTRIC"
-- 	collmaterial = "CARDBOARD_BOX"
-- 	collmaterial = "PAPER"
-- 	collmaterial = "UPHOLSTERY"
	collmaterial = "PLASTIC_FULL_LIQUID"
-- 	collmaterial = "PLASTIC"
-- 	collmaterial = "NONE"
	for this in selection where ((getAttrClass this) == "Gta Collision") do
	(
		setattr this (getattrindex "Gta Collision" "Coll Type") collmaterial
		print (this.name + " collision material set to " + collmaterial)
	)
)

fn Zap =
(
	objSet = for this in selection where (classOf this == Editable_Poly) collect this
	if (objSet.count == 1 and SubObjectLevel == 2) then
	(
		edgeSel = objSet[1].selectedEdges
		if (edgeSel.count > 0) then
		(
-- 			messagebox (edgeSel.count as string)
			macros.run "Editable Polygon Object" "EPoly_Select_Loop"
			macros.run "Editable Polygon Object" "EPoly_Remove"
		)
		else
		(
			messagebox "Please select an edge."
		)
	)
	else
	(
		messagebox "Please select edges on an editable poly object."
	)
)

fn RandomColours =
(
	for this in objects do
	(
		this.wirecolor = color (random 0 255) (random 0 255) (random 0 255)
	)
)

----------------------------------------------------------------------------------------------------
--FUNCTION CALLS
----------------------------------------------------------------------------------------------------
-- clearListener()

RandomColours()
-- SelectNoneExportables()
-- filein (RsConfigGetWildWestDir() + "script/max/rockstar_london/utils/AD_Tools/MaterialViewer.ms")
-- filein (RsConfigGetWildWestDir() + "script/max/rockstar_london/utils/AD_Tools/ListMaterialIDs.ms")


-- Zap()
-- filein (RsConfigGetWildWestDir() + "script/max/rockstar_london/utils/AD_Tools/Extrude.ms")
-- SetPhysicsCollisionTrue()
-- messagebox "test"
-- SetPhysicsCollisionFlags()
-- SetCollisionMaterial()
-- SetCameraCollisionTrue()

-- CreateCameraCollision "zcylinder"
-- CreateCameraCollision "box"

-- macros.run "Modifier Stack" "Convert_to_Poly"
-- SelectCollision()
-- SelectMinLODValue 1000
-- subObjectLevel = 5
-- ApplyMaterialIDToSelection 1
-- collapseStack($)
-- subObjectLevel = 5
-- collapseStack $
-- ParentAndMove()
-- RingConnect()
-- messagebox "test message"
-- ProcessCollisionSpline 2 2
-- uberfile = "Automap.ms"
-- filein (RsConfigGetWildWestDir() + "script/max/rockstar_london/utils/AD_Tools/" + uberfile)
-- SetTXD "v_bus_bg"
-- SetLODDistance 300