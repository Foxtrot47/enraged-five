fn  AD_FlipUVs axis =
(
	if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 5) then
	(
		subObjectLevel = 4
	)
	if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 4) then
	(
		if $.selectedFaces.count > 0 then
		(
			selectedPolys = $.EditablePoly.GetSelection #Face
			modPanel.addModToSelection  (Unwrap_UVW())
			subObjectLevel = 3
			$.modifiers[#unwrap_uvw].edit()
			$.modifiers[#unwrap_uvw].selectFacesByNode selectedPolys $
			case axis of
			(
				"horizontal": $.modifiers[#unwrap_uvw].unwrap.mirrorh()
				"vertical": $.modifiers[#unwrap_uvw].unwrap.mirrorv()
				"default": messagebox "invalid axis"
			)
			
			collapseStack $
			subObjectLevel = 4
		)
		else
		(
			messagebox "Please select a face"
		)
	)
	else
	(
		messagebox "Please select faces on an editable poly object"
	)
)