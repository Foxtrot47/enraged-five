--Uberpoint.ms
--Creates/selects point helper

objSet = for this in helpers where this.name == "Uberpoint" collect this
if objSet.count > 0 then
(
	select objSet[1]
	max move
	try
	(
		ADTools.InfoPanel.Text = ("Uberpoint is at " + objSet[1].position as string)
	)
	catch()
)
else
(
	helperColor = (color 192 255 0)
	try
	(
		IniFilePath = RsConfigGetWildWestDir()+"script/max/rockstar_london/config/AD_Tools.ini"
		helperColor.r = getINISetting IniFilePath "SceneControl" ("HelperR") as integer
		helperColor.g = getINISetting IniFilePath "SceneControl" ("HelperG") as integer
		helperColor.b = getINISetting IniFilePath "SceneControl" ("HelperB") as integer
	)
	catch()
	
	uberpoint = Point name:"Uberpoint" size: 10 box:true drawOnTop:true constantScreenSize:true
	uberpoint.pos = [0, 0, 0]
	uberpoint.wirecolor = helperColor
	select uberpoint
	max move
	try
	(
		ADTools.InfoPanel.Text = ("Uberpoint created at [0,0,0]")
	)
	catch()
)