--ToggleCommandPanel.ms

commandPanelWidth = 183
if (cui.commandPanelOpen == true) then
(
	cui.commandPanelOpen = false
	try (ADTools.Form.Location.x = (ADTools.Form.Location.x + commandPanelWidth)) catch()
	try (SceneControl.Form.Location.x = (SceneControl.Form.Location.x + commandPanelWidth)) catch()
	ADTools.InfoPanel.Text = "Command panel off"
)
else
(
	cui.commandPanelOpen = true
	ADTools.Form.Location.x = (ADTools.Form.Location.x - commandPanelWidth)
	try (SceneControl.Form.Location.x = (SceneControl.Form.Location.x - commandPanelWidth))
	catch()
)