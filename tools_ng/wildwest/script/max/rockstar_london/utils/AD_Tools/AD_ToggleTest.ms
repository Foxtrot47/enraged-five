iniFilePath = "X:/tools/dcc/current/max2009/plugcfg/AD_Tools.ini"

try
(
	ADT_ToggleTestState = getINISetting iniFilePath "ADTools" "ADT_ToggleTestState" as bool
)
catch()

if (ADT_ToggleTestState == false) or (ADT_ToggleTestState == undefined) then
(
	ADT_ToggleTestState = true
	ADT_genbutton15.BackgroundImage = ADT_Instance.ADT_MapImage "AD_ToggleOn.bmp"
)
else
(
	ADT_ToggleTestState = false
	ADT_genbutton15.BackgroundImage = ADT_Instance.ADT_MapImage "AD_ToggleOff.bmp"
)

setINISetting iniFilePath "ADTools" "ADT_ToggleTestState" (ADT_ToggleTestState as string)
print ADT_ToggleTestState as string