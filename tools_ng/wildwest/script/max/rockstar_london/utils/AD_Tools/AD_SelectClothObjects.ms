--select cloth objects
(
	local idxIsCloth = getAttrIndex "Gta Object" "Is Cloth"
	local objectArray = for object in objects where (getAttrClass object) == "Gta Object" and (getAttr object idxIsCloth) collect object
	select objectArray
	if objectArray.count == 1 then messagebox "there is 1 cloth object in the scene"
	else messagebox ("there are " + objectArray.count as string + " cloth objects in the scene")
)