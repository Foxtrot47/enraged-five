--fixTXDNames.ms
--Andy Davis
--November 2010
--script finds all models with the TXD 'Change Me' and changes to a default TXD based on the name of the filein

objectCount = 0
newTXDName = (getFilenameFile maxFileName)
objSet = for this in objects where ((getAttrClass this) == "Gta Object") and (classOf this != XrefObject) collect this

for this in objSet where (getAttr this (getAttrIndex "Gta Object" "TXD") == "CHANGEME") do
(
	objectCount = objectCount + 1
	setAttr this (getAttrIndex "Gta Object" "TXD") newTXDName
	print this.name
)
if objectCount == 1 then objectString = " object "
else objectString = " objects "
(objectCount as string + objectString + "changed to TXD name: " + newTXDName)