--TargetWeld.ms
--script enters target weld mode
--�2010 Rockstar London
--Author: Andrew Davis
--Revsion 1: December 2010

if (selection.count == 1) and (classOf selection[1] == Editable_Poly) then
(
	subObjectLevel = 1
	macros.run "Editable Polygon Object" "EPoly_TargetWeld"
)
else
(
	messagebox "Please select an editable poly object"
)