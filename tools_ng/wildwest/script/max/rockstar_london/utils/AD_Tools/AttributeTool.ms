--AttributeTool
--allows user to view and edit some GTA attributes
--future buttons: PickTXDNameFromExisting, CalcLODDistanceFromOrigin, FixAllTXDNames
--also: selected only checkbox
--info box with object info: count, different value warnings etc

global AttributeToolInstance

struct AttributeToolStruct
(
	AttributeToolForm,
	AttributeToolFlowLayoutPanel,
	SwatchButtonArray = #(),
	IniFilePath = RsConfigGetWildWestDir()+"script/max/rockstar_london/config/AD_Tools.ini",
	
	--preset definitions
	DS_Fill = (dotNetClass "System.Windows.Forms.DockStyle").Fill,
	FS_Flat = (dotNetClass "System.Windows.Forms.FlatStyle").Flat,
	SP_Manual =  (dotNetClass "System.Windows.Forms.FormStartPosition").Manual,
	Font_Calibri = dotNetObject "System.Drawing.Font" "Calibri" 8,
	
	--colours
	RGB_Black = (dotNetClass "System.Drawing.Color").black,
	RGB_DarkGrey = (dotNetClass "System.Drawing.Color").fromARGB 102 102	102,
	RGB_Steel = (dotNetClass "System.Drawing.Color").FromARGB 200 210 220,

	WinLocX = 0,
	WinLocY = 40,
	LODNumBox,
	LODTextBox,
	TXDTextBox,
	objSet,
	InfoLabel,
	LODAssignButton,
	TXDAssignButton,
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--GENERAL FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn GetAttributes =
	(
		LODValue = 0
		TXDValue = ""
		objSet = for this in selection  where ((getAttrClass this) == "Gta Object") and (classOf this != XrefObject) collect this
		if objSet.Count > 0 then
		(
			LODValue = getattr objSet[objSet.count] (getattrindex "Gta Object" "LOD distance")
			TXDValue = getattr objSet[objSet.count] (getattrindex "Gta Object" "TXD")
		)
		return #(LODValue, TXDValue)
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn UpdateLocalConfigFileSave =
	(
		setINISetting IniFilePath "AttributeTool" "WinLocX" (WinLocX as string)
		setINISetting IniFilePath "AttributeTool" "WinLocY" (WinLocY as string)
	),
	
	fn UpdateLocalConfigFileLoad =
	(
		try
		(
			WinLocX = getINISetting IniFilePath "AttributeTool" "WinLocX" as integer
			WinLocY = getINISetting IniFilePath "AttributeTool" "WinLocY" as integer
		)
		catch()
		AttributeToolForm.Location = dotNetObject "system.drawing.point" WinLocX WinLocY
	),
	
	fn DispatchLoadIniFile s e = AttributeToolInstance.UpdateLocalConfigFileLoad(),	
	fn DispatchSaveIniFile s e = AttributeToolInstance.UpdateLocalConfigFileSave(),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--EVENT HANDLERS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn DispatchSetWinLoc s e = AttributeToolInstance.SetWinLoc s e,
	fn SetWinLoc s e =
	(
		WinLocX = AttributeToolForm.Location.X
		WinLocY = AttributeToolForm.Location.Y
	),
	
	fn RefreshValues =
	(
		NewValues = AttributeToolInstance.GetAttributes()
		AttributeToolInstance.LODNumBox.Value = NewValues[1]
		AttributeToolInstance.TXDTextBox.Text = NewValues[2]
		case (AttributeToolInstance.objSet.Count) of
		(
			0:
			(
				AttributeToolInstance.InfoLabel.Text = ("No objects selected")
				AttributeToolInstance.LODAssignButton.Enabled = false
				AttributeToolInstance.TXDAssignButton.Enabled = false
			)
			1:
			(
				AttributeToolInstance.InfoLabel.Text = ("1 object selected\n" +  AttributeToolInstance.objSet[1].name)
				AttributeToolInstance.LODAssignButton.Enabled = true
				AttributeToolInstance.TXDAssignButton.Enabled = true
			)
			default:
			(
				local leadObject = AttributeToolInstance.objSet[AttributeToolInstance.objSet.count]
				AttributeToolInstance.InfoLabel.Text = (AttributeToolInstance.objSet.Count as string + " objects selected\nlead object: " + leadObject.name)
				AttributeToolInstance.LODAssignButton.Enabled = true
				AttributeToolInstance.TXDAssignButton.Enabled = true
			)
		)
	),
	
	fn AssignLODValue =
	(
		newValue = AttributeToolInstance.LODNumBox.Value
		for this in AttributeToolInstance.objSet do
		(
			setattr this (getattrindex "Gta Object" "LOD distance") newValue
		)
		if AttributeToolInstance.objSet.Count == 1 then objString = "object" else objString = "objects"
		AttributeToolInstance.InfoLabel.Text = ("LOD value " + newValue as string + " assigned to " + objString)
	),
	
	fn AssignTXDValue =
	(
		newValue = AttributeToolInstance.TXDTextBox.Text
		for this in AttributeToolInstance.objSet do
		(
			setattr this (getattrindex "Gta Object" "TXD") newValue
		)
		if AttributeToolInstance.objSet.Count == 1 then objString = "object" else objString = "objects"
		AttributeToolInstance.InfoLabel.Text = ("TXD " + newValue as string + " assigned to " + objString)
	),
	
	fn AutoAssign =
	(
		AttributeToolInstance.InfoLabel.Text = "It's not plugged in yet"
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn CreateUI =
	(		
		-- form setup
		AttributeToolForm = dotNetObject "maxCustomControls.maxForm"
		AttributeToolForm.Size = dotNetObject "System.Drawing.Size" 220 152
		AttributeToolForm.Text = "Attribute Tool"
		AttributeToolForm.startPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").manual
		AttributeToolForm.Location = dotNetObject "system.drawing.point" 0 80
		AttributeToolForm.FormBorderStyle = (dotNetClass "System.Windows.Forms.FormBorderStyle").FixedToolWindow
		AttributeToolForm.sizeGripStyle = (dotNetClass "SizeGripStyle").show
		AttributeToolForm.startPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		dotNet.AddEventHandler AttributeToolForm "Load" DispatchLoadIniFile
		dotNet.AddEventHandler AttributeToolForm "Closing" DispatchSaveIniFile
		dotNet.AddEventHandler AttributeToolForm "Move" DispatchSetWinLoc
		
		--content
		AttributeToolToolTip = dotnetobject "ToolTip"
		
		AttributeToolTable = dotNetObject "TableLayoutPanel"
		AttributeToolTable.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		AttributeToolTable.ColumnCount = 1
		AttributeToolTable.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		AttributeToolTable.RowCount = 4
		AttributeToolTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		AttributeToolTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		AttributeToolTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		AttributeToolTable.rowStyles.add (RS_dotNetObject.rowStyleObject "percent" 100)
		AttributeToolForm.Controls.Add AttributeToolTable		
		
		LODStrip = dotNetObject "TableLayoutPanel"
		LODStrip.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		LODStrip.ColumnCount = 3
		LODStrip.ColumnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 30)
		LODStrip.ColumnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		LODStrip.ColumnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 55)
		AttributeToolTable.Controls.Add LODStrip 0 0
		
		LODLabel = dotNetObject "Label"
		LODLabel.Text = "LOD"
		LODLabel.TextAlign = (dotNetClass "System.Drawing.ContentAlignment").MiddleCenter
		LODLabel.Font = Font_Calibri
		LODStrip.Controls.Add LODLabel 0 0
		
		LODNumBox = dotNetObject "NumericUpDown"
		LODNumBox.Dock = DS_Fill
		LODNumBox.Minimum = 0
		LODNumBox.Maximum = 9999
		LODStrip.Controls.Add LODNumBox 1 0
		
		LODAssignButton = dotNetObject "Button"
		LODAssignButton.Text = "Assign"
		LODAssignButton.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		LODAssignButton.FlatStyle = (dotNetClass "System.Windows.Forms.Flatstyle").Flat
		LODAssignButton.Font = Font_Calibri
		LODAssignButton.Size = dotNetObject "System.Drawing.Size" 50 15
		LODStrip.Controls.Add LODAssignButton 2 0
		dotNet.AddEventHandler LODAssignButton "Click" AssignLODValue
		
		TXDStrip = dotNetObject "TableLayoutPanel"
		TXDStrip.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		TXDStrip.ColumnCount = 3
		TXDStrip.ColumnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 30)
		TXDStrip.ColumnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		TXDStrip.ColumnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 55)
		AttributeToolTable.Controls.Add TXDStrip 0 1
		
		TXDTextBox = dotNetObject "TextBox"
		TXDTextBox.Dock = DS_Fill
		TXDStrip.Controls.Add TXDTextBox 1 0
		
		TXDLabel = dotNetObject "Label"
		TXDLabel.Text = "TXD"
		TXDLabel.TextAlign = (dotNetClass "System.Drawing.ContentAlignment").MiddleCenter
		TXDLabel.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		TXDLabel.Font = Font_Calibri
		TXDStrip.Controls.Add TXDLabel 0 0
		
		TXDAssignButton = dotNetObject "Button"
		TXDAssignButton.Text = "Assign"
		TXDAssignButton.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		TXDAssignButton.FlatStyle = (dotNetClass "System.Windows.Forms.Flatstyle").Flat
		TXDAssignButton.Font = Font_Calibri
		TXDAssignButton.Size = dotNetObject "System.Drawing.Size" 50 15
		TXDStrip.Controls.Add TXDAssignButton 2 0
		dotNet.AddEventHandler TXDAssignButton "Click" AssignTXDValue
		
		InfoLabel = dotNetObject "Label"
		InfoLabel.Font = Font_Calibri
		InfoLabel.Dock = DS_Fill
		InfoLabel.TextAlign = (dotNetClass "System.Drawing.ContentAlignment").MiddleCenter
		AttributeToolTable.Controls.Add InfoLabel 0 2
		
		ButtonStrip = dotNetObject "FlowLayoutPanel"
		ButtonStrip.Dock = DS_Fill
		AttributeToolTable.Controls.Add ButtonStrip 0 3

		
		RefreshButton = dotNetObject "Button"
		RefreshButton.Text = "Refresh"
		RefreshButton.FlatStyle = (dotNetClass "System.Windows.Forms.Flatstyle").Flat
		RefreshButton.Font = Font_Calibri
		RefreshButton.Width = (ButtonStrip.Width/2 - 6)
		RefreshButton.Height = 20
		ButtonStrip.Controls.Add RefreshButton
		dotNet.AddEventHandler RefreshButton "Click" RefreshValues
		
		AutoAssignButton = dotNetObject "Button"
		AutoAssignButton.Text = "Auto Assign"
		AutoAssignButton.FlatStyle = (dotNetClass "System.Windows.Forms.Flatstyle").Flat
		AutoAssignButton.Font = Font_Calibri
		AutoAssignButton.Width = (ButtonStrip.Width/2 - 6)
		AutoAssignButton.Height = RefreshButton.Height
		ButtonStrip.Controls.Add AutoAssignButton
		dotNet.AddEventHandler AutoAssignButton "Click" AutoAssign
		
-- 		--draw form
		AttributeToolForm.showModeless()
		AttributeToolForm
		RefreshValues()
	)
)

if AttributeToolInstance != undefined then
(
	AttributeToolInstance.AttributeToolForm.close()
)

AttributeToolInstance = AttributeToolStruct()
AttributeToolInstance.CreateUI()