--DuplicateFace.ms
--edit 27/01/2011: ADTools info added

subObjectMode = subObjectLevel
if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 5) then
(
	subObjectLevel = 4
)
if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 4) then
(
	if $.selectedFaces.count > 0 then
	(
		$.EditablePoly.detachToElement #Face keepOriginal:on
		if $.selectedFaces.count == 1 then
		(
			print ("One face duplicated")
			try (ADTools.InfoPanel.Text = ("One face duplicated"))
			catch()	
		)
		else
		(
			print ($.selectedFaces.count as string + " faces duplicated")
			try (ADTools.InfoPanel.Text = ($.selectedFaces.count as string + " faces duplicated"))
			catch()	
		)
	)
	else
	(
		messagebox "Please select a face"
	)
	subObjectLevel = subObjectMode
)
else
(
	messagebox "Please select faces on an editable poly object"
)