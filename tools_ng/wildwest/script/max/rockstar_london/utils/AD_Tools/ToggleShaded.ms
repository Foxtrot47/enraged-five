--ToggleShaded.ms

for this in selection where superClassOf this == GeometryClass do
(
	if this.vertexColorsShaded == true then
	(
		this.vertexColorsShaded = false
	)
	else
	(
		this.vertexColorsShaded  = true
	)
)