--script to manage polygon hiding
fn AD_HidePolys mode =
(
	if (selection.count == 1) and (classOf $ == Editable_Poly) then
	(
		subObjectLevel = 4
		Try (
			local A = Filters.GetModOrObj()
			case(mode) of
			(
			   "hideselected":
				(
					if (Filters.Is_This_EditPolyMod A) then
					(
						if (subobjectlevel == 1) then A.ButtonOp #HideVertex
						else A.ButtonOp #HideFace
					)
					else A.buttonOp #HideSelection
				)
				"hideunselected":
				(
					if (Filters.Is_This_EditPolyMod A) then
					(
						if (subobjectlevel == 1) then A.ButtonOp #HideUnselectedVertex
						else A.ButtonOp #HideUnselectedFace
					)
					else A.buttonOp #HideUnselected
				)
				"unhideall":
				(
					if (Filters.Is_This_EditPolyMod A) then
					(
						if (subobjectlevel == 1) then A.ButtonOp #UnhideAllVertex
						else A.ButtonOp #UnhideAllFace
					)
					else A.buttonOp #UnhideAll
				)
			)
		)
		Catch (MessageBox "Operation Failed" Title:"Poly Editing")
	)
	else messagebox "Select polys on an editable poly object"
)