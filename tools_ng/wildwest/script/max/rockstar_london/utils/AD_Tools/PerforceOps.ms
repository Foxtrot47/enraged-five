--PerforceOps.ms

filein (RsConfigGetProjRootDir() + "tools\\dcc\\current\\max2012\\scripts\\pipeline\\util\\p4.ms")

function PerforceCheckout filename =
(
-- 	local docname = getFilenameFile filename
	if queryBox "About to check out " + filename + ". Do you want to continue?" beep:true then
	(
		local p4 = RsPerforce()
		p4.connect "x:\\payne"
		p4.edit filename
		p4.disconnect
		messagebox (filename + " checked out")
	)
	else
	(
		messagebox "Checkout aborted"
	)
)

function PerforceAdd filename =
(
	local p4 = RsPerforce()
	p4.connect "x:\\payne"
	p4.add filename
	p4.disconnect
)

function PerforceSync filename =
(
	local p4 = RsPerforce()
	p4.connect "x:\\payne"
	p4.sync filename
	p4.disconnect
)

function PerforceIsCheckedOut filename =
-- can't see a way to check this...
(
	if (filename.name == "test") then
	return true
	else
	return false
)

function PerforceReturnDepotPath filename =
(
	local p4 = RsPerforce()
	p4.connect "x:\\payne"
	p4.local2depot filename
	p4.disconnect
)

function CheckoutCurrentScene =
(
	local filename = (maxFilePath + maxFileName)
	local infoString
	try
	(
		local p4 = RsPerforce()
		p4.connect "x:\\payne"
		p4.edit filename
		p4.disconnect
		infoString = ((filenameFromPath filename) + " checked out")
	)
	catch
	(
		infoString = ((filenameFromPath filename) + " checkout failed")
	)
	try
	(
		ADTools.InfoPanel.Text = infoString
	)
	catch
	(
		messagebox infoString
	)
)

-- CheckoutCurrentScene()

-- myfile = (RsConfigGetWildWestDir()+"script/max/rockstar_london/utils/AD_Tools/Old Scripts/AD_Combine.ms")

-- PerforceCheckout (RsConfigGetWildWestDir()+"script/max/rockstar_london/utils/AD_Tools/Old Scripts/AD_Combine.ms")
