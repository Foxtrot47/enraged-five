--script locks or unlocks selected objects' transforms

fn AD_LockTransforms mode =
(
	if selection.count > 0 then
	(
		case(mode) of
		(
			"on":
			(
				setTransformLockFlags selection #all
				print "Selection locked"
			)
			"off":
			(
				setTransformLockFlags selection #none
				print "Selection unlocked"
			)
		)
	)
	else
	(
		messagebox "Please select objects to lock"
	)
)

-- AD_TransformsLock "on"