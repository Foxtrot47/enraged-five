--boxmap
factor = 2.8
-- factor = 1.4
objset = #()
objset = for this in selection where classof this == Editable_Poly collect this
clearListener()
print objset
print objset.count

if objset.count > 0 then
(
	ubermod =  (uvwmap maptype: 4 length: factor width: factor height: factor name: "Automap")
	for this in objset do
	(
		select this
		if subObjectLevel == 1 then
		(
			addModifier this ubermod
		)
		else
		(
			subObjectLevel = 4
			if this.selectedFaces.count > 0 then
			(
				modPanel.addModToSelection  ubermod
			)
			else
			(
				addModifier this ubermod
			)
		)
	)
	select objset[objset.count]
	subObjectLevel = 1
)
else messagebox "Please select an editable poly"