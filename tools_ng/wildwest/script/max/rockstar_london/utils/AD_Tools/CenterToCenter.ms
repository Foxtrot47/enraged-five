--CenterToCenter.ms
--moves selected objects to the center of the last selected object

if selection.count > 1 then
(
	local objSet = #()
	for i = 1 to (selection.count - 1) do
	(
		selection[i].center = selection[selection.count].center
		append objSet selection[i]
	)
	try
	(
		if selection.count == 2 then
		(
			thestring = "1 object"
		)
		else
		(
			thestring = ((selection.count - 1) as string + " objects")
		)			
		ADTools.InfoPanel.Text = (thestring + " moved to center of " + selection[selection.count].name)
	)
	catch()
	select objSet
)
else
(
	messagebox "Please select at least two objects"
)