--ColorCode
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetUIOps.ms")
filein "pipeline\\util\\drawableLOD.ms"
filein "gta\\utils\\GtaUtilityFunc.ms"

global ColorCode

struct ColorCodeStruct
(
	ColorCodeForm,
	ColorCodeFlowLayoutPanel,
	SwatchButtonArray = #(),
	IniFilePath = RsConfigGetWildWestDir()+"script/max/rockstar_london/config/AD_Tools.ini",
	
	--preset definitions
	DS_Fill = (dotNetClass "System.Windows.Forms.DockStyle").Fill,
	FBS_FixedToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").FixedToolWindow,
	FBS_SizableToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").SizableToolWindow,
	FS_Flat = (dotNetClass "System.Windows.Forms.FlatStyle").Flat,
	FS_Standard = (dotNetClass "System.Windows.Forms.FlatStyle").Standard,
	SP_Manual =  (dotNetClass "System.Windows.Forms.FormStartPosition").Manual,
	Font_Calibri = dotNetObject "System.Drawing.Font" "Calibri" 8,
	Padding_One = dotNetObject "System.Windows.Forms.Padding" 1,
	Padding_None = dotNetObject "System.Windows.Forms.Padding" 0,
	
	--colours
	RGB_Black = (dotNetClass "System.Drawing.Color").black,
	RGB_DarkGrey = (dotNetClass "System.Drawing.Color").fromARGB 102 102	102,
	RGB_Steel = (dotNetClass "System.Drawing.Color").FromARGB 200 210 220,
	RGB_Transparent = (dotNetClass "System.Drawing.Color").transparent,

	WinLocX = 0,
	WinLocY = 40,
	GeometryCB,
	CollisionCB,
	XRefCB,
	StoreCB,
	LODCB,
	GeometryButton,
	CollisionButton,
	XRefButton,
	LODButton,
	Tooltip,
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--GENERAL FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn PickColor s e =
	(
		myColor = colorPickerDlg (color s.BackColor.r s.BackColor.g s.BackColor.b) "Pick wirecolor" alpha:false pos:[-1,-1]
		s.BackColor = (dotNetClass "System.Drawing.Color").fromARGB myColor.r myColor.g myColor.b
	),
	
	fn SelectColor s e =
	(
		myColor
		case s.Tag of
		(
			"geometry": myColor = (color ColorCode.GeometryButton.BackColor.r ColorCode.GeometryButton.BackColor.g ColorCode.GeometryButton.BackColor.b)
			"collision": myColor = (color ColorCode.CollisionButton.BackColor.r ColorCode.CollisionButton.BackColor.g ColorCode.CollisionButton.BackColor.b)
			"XRef": myColor = (color ColorCode.XRefButton.BackColor.r ColorCode.XRefButton.BackColor.g ColorCode.XRefButton.BackColor.b)
			"LOD": myColor = (color ColorCode.LODButton.BackColor.r ColorCode.LODButton.BackColor.g ColorCode.LODButton.BackColor.b)
		)
		clearSelection()
		select (for this in objects where this.wirecolor == myColor collect this)
		if selection.count > 0 then
		(
			try(ADTools.InfoPanel.Text = (selection.count as string + " " + s.Tag + " objects selected"))
			catch()
		)
		else
		(
			try(ADTools.InfoPanel.Text = ("No color-coded " + s.Tag + " in scene"))
			catch()
		)
	),
	
	fn HideColor s e =
	(
		myColor
		case s.Tag of
		(
			"geometry": myColor = (color ColorCode.GeometryButton.BackColor.r ColorCode.GeometryButton.BackColor.g ColorCode.GeometryButton.BackColor.b)
			"collision": myColor = (color ColorCode.CollisionButton.BackColor.r ColorCode.CollisionButton.BackColor.g ColorCode.CollisionButton.BackColor.b)
			"XRef": myColor = (color ColorCode.XRefButton.BackColor.r ColorCode.XRefButton.BackColor.g ColorCode.XRefButton.BackColor.b)
			"LOD": myColor = (color ColorCode.LODButton.BackColor.r ColorCode.LODButton.BackColor.g ColorCode.LODButton.BackColor.b)
		)
		clearSelection()
		hideSet = for this in objects where this.wirecolor == myColor collect this
		hide hideSet
		if hideSet.count > 0 then
		(
			try(ADTools.InfoPanel.Text = (hideSet.count as string + " " + s.Tag + " objects hidden"))
			catch()
		)
		else
		(
			try(ADTools.InfoPanel.Text = ("No color-coded " + s.Tag + " in scene"))
			catch()
		)
	),
	
	fn UnhideColor s e =
	(
		myColor
		case s.Tag of
		(
			"geometry": myColor = (color ColorCode.GeometryButton.BackColor.r ColorCode.GeometryButton.BackColor.g ColorCode.GeometryButton.BackColor.b)
			"collision": myColor = (color ColorCode.CollisionButton.BackColor.r ColorCode.CollisionButton.BackColor.g ColorCode.CollisionButton.BackColor.b)
			"XRef": myColor = (color ColorCode.XRefButton.BackColor.r ColorCode.XRefButton.BackColor.g ColorCode.XRefButton.BackColor.b)
			"LOD": myColor = (color ColorCode.LODButton.BackColor.r ColorCode.LODButton.BackColor.g ColorCode.LODButton.BackColor.b)
		)
		clearSelection()
		unhideSet = for this in objects where this.wirecolor == myColor collect this
		unhide unhideSet
		if unhideSet.count > 0 then
		(
			try(ADTools.InfoPanel.Text = (unhideSet.count as string + " " + s.Tag + " objects unhidden"))
			catch()
		)
		else
		(
			try(ADTools.InfoPanel.Text = ("No color-coded " + s.Tag + " in scene"))
			catch()
		)
	),
	
	fn InitSwatchButton =
	(
		newButton = dotNetObject "Button"
		newButton.Size = dotNetObject "System.Drawing.Size" 12 12
		newButton.FlatStyle = FS_Flat
		newButton.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		newButton.Anchor = (dotNetClass "System.Windows.Forms.AnchorStyles").None
		ToolTip.SetToolTip newButton "Pick color"
		dotNet.AddEventHandler newButton "Click" PickColor
		return newButton
	),
	
	fn InitCB =
	(
		newCB = dotNetObject "CheckBox"
		newCB.Size = dotNetObject "System.Drawing.Size" 18 18
		newCB.Margin = dotNetObject "System.Windows.Forms.Padding" 1
		newCB.Dock = DS_Fill
		ToolTip.SetToolTip newCB "Arm"
		newCB.Dock = DS_Fill
		return newCB
	),
	
	fn InitFunctionButton =
	(
		newButton = dotNetObject "Button"
		newButton.Size = dotNetObject "System.Drawing.Size" 16 16
		newButton.FlatStyle = FS_Standard
		newButton.Font = dotNetObject "System.Drawing.Font" "Calibri" 7
		newButton.Margin = Padding_None
		newButton.BackColor = RGB_Transparent
		newButton.Anchor = (dotNetClass "System.Windows.Forms.AnchorStyles").None
		return newButton
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn UpdateLocalConfigFileSave =
	(
		setINISetting IniFilePath "ColorCode" "WinLocX" (WinLocX as string)
		setINISetting IniFilePath "ColorCode" "WinLocY" (WinLocY as string)
		setINISetting IniFilePath "ColorCode" "GeometryState" (ColorCode.GeometryCB.Checked as string)
		setINISetting IniFilePath "ColorCode" "GeometryColorR" (GeometryButton.BackColor.r as string)
		setINISetting IniFilePath "ColorCode" "GeometryColorG" (GeometryButton.BackColor.g as string)
		setINISetting IniFilePath "ColorCode" "GeometryColorB" (GeometryButton.BackColor.b as string)
		setINISetting IniFilePath "ColorCode" "CollisionState" (ColorCode.CollisionCB.Checked as string)
		setINISetting IniFilePath "ColorCode" "CollisionColorR" (CollisionButton.BackColor.r as string)
		setINISetting IniFilePath "ColorCode" "CollisionColorG" (CollisionButton.BackColor.g as string)
		setINISetting IniFilePath "ColorCode" "CollisionColorB" (CollisionButton.BackColor.b as string)
		setINISetting IniFilePath "ColorCode" "XRefState" (ColorCode.XRefCB.Checked as string)
		setINISetting IniFilePath "ColorCode" "XRefColorR" (XRefButton.BackColor.r as string)
		setINISetting IniFilePath "ColorCode" "XRefColorG" (XRefButton.BackColor.g as string)
		setINISetting IniFilePath "ColorCode" "XRefColorB" (XRefButton.BackColor.b as string)
		setINISetting IniFilePath "ColorCode" "LODColorR" (LODButton.BackColor.r as string)
		setINISetting IniFilePath "ColorCode" "LODColorG" (LODButton.BackColor.g as string)
		setINISetting IniFilePath "ColorCode" "LODColorB" (LODButton.BackColor.b as string)
		setINISetting IniFilePath "ColorCode" "StoreState" (ColorCode.StoreCB.Checked as string)
	),
	
	fn UpdateLocalConfigFileLoad =
	(
		GeometryState = true
		CollisionState = true
		XRefState = true
		LODState = true
		StoreState = true
		GeometryColor = (color 128 128 128)
		CollisionColor = (color 255 140 0)
		XRefColor = (color 0 120 0)
		LODColor = (color 0 255 192)
		
		try
		(
			WinLocX = getINISetting IniFilePath "ColorCode" "WinLocX" as integer
			WinLocY = getINISetting IniFilePath "ColorCode" "WinLocY" as integer
			GeometryState = getINISetting IniFilePath "ColorCode" "GeometryState" as booleanClass
			CollisionState = getINISetting IniFilePath "ColorCode" "CollisionState" as booleanClass
			XRefState = getINISetting IniFilePath "ColorCode" "XRefState" as booleanClass
			StoreState = getINISetting IniFilePath "ColorCode" "StoreState" as booleanClass
			GeometryColor.r = getINISetting IniFilePath "ColorCode" "GeometryColorR" as integer
			GeometryColor.g = getINISetting IniFilePath "ColorCode" "GeometryColorG" as integer
			GeometryColor.b = getINISetting IniFilePath "ColorCode" "GeometryColorB" as integer
			CollisionColor.r = getINISetting IniFilePath "ColorCode" "CollisionColorR" as integer
			CollisionColor.g = getINISetting IniFilePath "ColorCode" "CollisionColorG" as integer
			CollisionColor.b = getINISetting IniFilePath "ColorCode" "CollisionColorB" as integer
			XRefColor.r = getINISetting IniFilePath "ColorCode" "XRefColorR" as integer
			XRefColor.g = getINISetting IniFilePath "ColorCode" "XRefColorG" as integer
			XRefColor.b = getINISetting IniFilePath "ColorCode" "XRefColorB" as integer
			LODColor.r = getINISetting IniFilePath "ColorCode" "LODColorR" as integer
			LODColor.g = getINISetting IniFilePath "ColorCode" "LODColorG" as integer
			LODColor.b = getINISetting IniFilePath "ColorCode" "LODColorB" as integer
		)
		catch()
		ColorCodeForm.Location = dotNetObject "system.drawing.point" WinLocX WinLocY
		ColorCode.GeometryCB.Checked = GeometryState
		ColorCode.CollisionCB.Checked = CollisionState
		ColorCode.XRefCB.Checked = XRefState
		ColorCode.LODCB.Checked = LODState
		ColorCode.StoreCB.Checked = StoreState
		ColorCode.GeometryButton.BackColor = (dotNetClass "System.Drawing.Color").FromARGB GeometryColor.r GeometryColor.g GeometryColor.b
		ColorCode.XRefButton.BackColor = (dotNetClass "System.Drawing.Color").FromARGB XRefColor.r XRefColor.g XRefColor.b
		ColorCode.CollisionButton.BackColor = (dotNetClass "System.Drawing.Color").FromARGB CollisionColor.r CollisionColor.g CollisionColor.b
		ColorCode.LODButton.BackColor = (dotNetClass "System.Drawing.Color").FromARGB LODColor.r LODColor.g LODColor.b
	),
	
	fn DispatchLoadIniFile s e = ColorCode.UpdateLocalConfigFileLoad(),	
	fn DispatchSaveIniFile s e = ColorCode.UpdateLocalConfigFileSave(),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--EVENT HANDLERS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn DispatchSetWinLoc s e = ColorCode.SetWinLoc s e,
	fn SetWinLoc s e =
	(
		WinLocX = ColorCodeForm.Location.X
		WinLocY = ColorCodeForm.Location.Y
	),
	
	fn Restore =
	(
		try
		(
			ADTools.InfoPanel.Text = "It's not plugged in yet"
		)
		catch()
	),
	
	fn RestoreUnderConstruction =
	(
		for this in objects where (getUserProp this "OriginalColorR" != undefined) do
		(
			rgbval = #(getUserProp this "OriginalColorR", getUserProp this "OriginalColorG", getUserProp this "OriginalColorB")
			this.wirecolor =  (color rgbval[1] rgbval[2] rgbval[3])
			setUserProp this "OriginalColorR" undefined
			setUserProp this "OriginalColorG" undefined
			setUserProp this "OriginalColorB" undefined
		)
	),
	
	fn ApplyColors =
	(
		if (ColorCode.StoreCB.Checked == true) then
		(
			for this in objects where ((getAttrClass this) == "Gta Object") or ((getAttrClass this) == "Gta Collision") do
			if (getUserProp this "OriginalColorR" == undefined) then
			(
				setUserProp this "OriginalColorR" this.wirecolor.r
				setUserProp this "OriginalColorG" this.wirecolor.g
				setUserProp this "OriginalColorB" this.wirecolor.b
			)
		)
		if (ColorCode.GeometryCB.Checked == true) then
		(
			local newColor = ColorCode.GeometryButton.BackColor
			for this in objects where ((getAttrClass this) == "Gta Object") and (classOf this != XrefObject) do
			(
				this.wirecolor = (color newColor.r newColor.g newColor.b)
			)
		)
		
		if (ColorCode.CollisionCB.Checked == true) then
		(
			local newColor = ColorCode.CollisionButton.BackColor
			for this in objects where ((getAttrClass this) == "Gta Collision") and (classOf this != XrefObject) do
			(
				this.wirecolor = (color newColor.r newColor.g newColor.b)
			)
		)
		if (ColorCode.XRefCB.Checked == true) then
		(
			local newColor = ColorCode.XRefButton.BackColor
			for this in objects where (classOf this == XrefObject) do
			(
				this.wirecolor = (color newColor.r newColor.g newColor.b)
			)			
		)
		if (ColorCode.LODCB.Checked == true) then
		(
			local newColor = ColorCode.LODButton.BackColor
			for this in objects where (((getLODChildren this).Count > 0) or (RsLodDrawable_GetHigherDetailModel this != undefined)) do
			(
				this.wirecolor = (color newColor.r newColor.g newColor.b)
			)			
		)
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn CreateUI =
	(		
		-- form setup
		local rowHeight = 20
		
		ColorCodeForm = dotNetObject "maxCustomControls.maxForm"
		ColorCodeForm.Size = dotNetObject "System.Drawing.Size" 155 165
		ColorCodeForm.Text = "Color Code"
		ColorCodeForm.startPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").manual
		ColorCodeForm.Location = dotNetObject "system.drawing.point" 0 80
		ColorCodeForm.FormBorderStyle = FBS_FixedToolWindow
		ColorCodeForm.sizeGripStyle = (dotNetClass "SizeGripStyle").show
		ColorCodeForm.startPosition = SP_Manual
		
		--content
		ToolTip = dotnetobject "ToolTip"
		
		ColorCodeTable = dotNetObject "TableLayoutPanel"
		ColorCodeTable.Dock = DS_Fill
		ColorCodeForm.Controls.Add ColorCodeTable
		ColorCodeTable.ColumnCount = 6
		ColorCodeTable.ColumnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		ColorCodeTable.ColumnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 16)
		ColorCodeTable.ColumnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 16)
		ColorCodeTable.ColumnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 16)
		ColorCodeTable.ColumnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 16)
		ColorCodeTable.ColumnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 16)
		ColorCodeTable.RowCount = 6
		ColorCodeTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" rowHeight)
		ColorCodeTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" rowHeight)
		ColorCodeTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" rowHeight)
		ColorCodeTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" rowHeight)
		ColorCodeTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" rowHeight)
		ColorCodeTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 45)
		ColorCodeForm.Controls.Add ColorCodeTable
		
		GeometryLabel = dotNetObject "Label"
		GeometryLabel.Text = "Geometry"
		GeometryLabel.Font = Font_Calibri
		GeometryLabel.TextAlign = (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft
		ColorCodeTable.Controls.Add GeometryLabel 0 0
		
		GeometryButton = InitSwatchButton()		
		ColorCodeTable.Controls.Add GeometryButton 1 0
		
		GeometrySelectButton = InitFunctionButton()
		GeometrySelectButton.Tag = "geometry"
		GeometrySelectButton.Text = "S"
		dotNet.AddEventHandler GeometrySelectButton "Click" SelectColor
		ToolTip.SetToolTip GeometrySelectButton "Select"
		ColorCodeTable.Controls.Add GeometrySelectButton 2 0
		
		GeometryHideButton = InitFunctionButton()
		GeometryHideButton.Tag = "geometry"
		GeometryHideButton.Text = "H"
		dotNet.AddEventHandler GeometryHideButton "Click" HideColor
		ToolTip.SetToolTip GeometryHideButton "Hide"
		ColorCodeTable.Controls.Add GeometryHideButton 3 0
		
		GeometryUnhideButton = InitFunctionButton()
		GeometryUnhideButton.Tag = "geometry"
		GeometryUnhideButton.Text = "U"
		dotNet.AddEventHandler GeometryUnhideButton "Click" UnhideColor
		ToolTip.SetToolTip GeometryUnhideButton "Unhide"
		ColorCodeTable.Controls.Add GeometryUnhideButton 4 0
		
		GeometryCB = InitCB()
		ColorCodeTable.Controls.Add GeometryCB 5 0
		
		CollisionLabel = dotNetObject "Label"
		CollisionLabel.Text = "Collision"
		CollisionLabel.Font = Font_Calibri
		CollisionLabel.TextAlign = (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft
		ColorCodeTable.Controls.Add CollisionLabel 0 1
		
		CollisionButton = InitSwatchButton()
		ColorCodeTable.Controls.Add CollisionButton 1 1
		
		CollisionSelectButton = InitFunctionButton()
		CollisionSelectButton.Tag = "collision"
		CollisionSelectButton.Text = "S"
		dotNet.AddEventHandler CollisionSelectButton "Click" SelectColor
		ToolTip.SetToolTip CollisionSelectButton "Select"
		ColorCodeTable.Controls.Add CollisionSelectButton 2 1
		
		CollisionHideButton = InitFunctionButton()
		CollisionHideButton.Tag = "collision"
		CollisionHideButton.Text = "H"
		dotNet.AddEventHandler CollisionHideButton "Click" HideColor
		ToolTip.SetToolTip CollisionHideButton "Hide"
		ColorCodeTable.Controls.Add CollisionHideButton 3 1
		
		CollisionUnhideButton = InitFunctionButton()
		CollisionUnhideButton.Tag = "collision"
		CollisionUnhideButton.Text = "U"
		dotNet.AddEventHandler CollisionUnhideButton "Click" UnhideColor
		ToolTip.SetToolTip CollisionUnhideButton "Unhide"
		ColorCodeTable.Controls.Add CollisionUnhideButton 4 1
		
		CollisionCB = InitCB()
		ColorCodeTable.Controls.Add CollisionCB 5 1
		
		XRefLabel = dotNetObject "Label"
		XRefLabel.Text = "XRef"
		XRefLabel.Font = Font_Calibri
		XRefLabel.TextAlign = (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft
		ColorCodeTable.Controls.Add XRefLabel 0 2
		
		XRefButton = InitSwatchButton()
		ColorCodeTable.Controls.Add XRefButton 1 2
		
		XRefSelectButton = InitFunctionButton()
		XRefSelectButton.Tag = "XRef"
		XRefSelectButton.Text = "S"
		dotNet.AddEventHandler XRefSelectButton "Click" SelectColor
		ToolTip.SetToolTip XRefSelectButton "Select"
		ColorCodeTable.Controls.Add XRefSelectButton 2 2
		
		XRefHideButton = InitFunctionButton()
		XRefHideButton.Tag = "XRef"
		XRefHideButton.Text = "H"
		dotNet.AddEventHandler XRefHideButton "Click" HideColor
		ToolTip.SetToolTip XRefHideButton "Hide"
		ColorCodeTable.Controls.Add XRefHideButton 3 2
		
		XRefUnhideButton = InitFunctionButton()
		XRefUnhideButton.Tag = "XRef"
		XRefUnhideButton.Text = "U"
		dotNet.AddEventHandler XRefUnhideButton "Click" UnhideColor
		ToolTip.SetToolTip XRefUnhideButton "Unhide"
		ColorCodeTable.Controls.Add XRefUnhideButton 4 2
		
		XRefCB = InitCB()
		ColorCodeTable.Controls.Add XRefCB 5 2
		
		LODLabel = dotNetObject "Label"
		LODLabel.Text = "LOD"
		LODLabel.Font = Font_Calibri
		LODLabel.TextAlign = (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft
		ColorCodeTable.Controls.Add LODLabel 0 3
		
		LODButton = InitSwatchButton()
		ColorCodeTable.Controls.Add LODButton 1 3
		
		LODSelectButton = InitFunctionButton()
		LODSelectButton.Tag = "LOD"
		LODSelectButton.Text = "S"
		dotNet.AddEventHandler LODSelectButton "Click" SelectColor
		ToolTip.SetToolTip LODSelectButton "Select"
		ColorCodeTable.Controls.Add LODSelectButton 2 3
		
		LODHideButton = InitFunctionButton()
		LODHideButton.Tag = "LOD"
		LODHideButton.Text = "H"
		dotNet.AddEventHandler LODHideButton "Click" HideColor
		ToolTip.SetToolTip LODHideButton "Hide"
		ColorCodeTable.Controls.Add LODHideButton 3 3
		
		LODUnhideButton = InitFunctionButton()
		LODUnhideButton.Tag = "LOD"
		LODUnhideButton.Text = "U"
		dotNet.AddEventHandler LODUnhideButton "Click" UnhideColor
		ToolTip.SetToolTip LODUnhideButton "Unhide"
		ColorCodeTable.Controls.Add LODUnhideButton 4 3
		
		LODCB = InitCB()
		ColorCodeTable.Controls.Add LODCB 5 3
		
		StoreLabel = dotNetObject "Label"
		StoreLabel.Text = "Store original color"
		StoreLabel.Font = Font_Calibri
		StoreLabel.TextAlign = (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft
		ColorCodeTable.Controls.Add StoreLabel 0 4
		ColorCodeTable.SetColumnSpan StoreLabel 5
		
		StoreCB = InitCB()
		ColorCodeTable.Controls.Add StoreCB 5 4
		
		ButtonStrip = dotNetObject "TableLayoutPanel"
		ButtonStrip.Dock = DS_Fill
		ButtonStrip.RowCount = 5
		ButtonStrip.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		ButtonStrip.ColumnCount = 3
-- 		ButtonStrip.BackColor = RGB_Black
		ButtonStrip.Margin = Padding_Zero
		ButtonStrip.ColumnStyles.add (RS_dotNetObject.columnStyleObject "percent" 50)
		ButtonStrip.ColumnStyles.add (RS_dotNetObject.columnStyleObject "percent" 50)
		ColorCodeTable.Controls.Add ButtonStrip 0 5
		ColorCodeTable.SetColumnSpan ButtonStrip 6
		
		ApplyButton = dotNetObject "Button"
		ApplyButton.Text = "Apply"
		ApplyButton.FlatStyle = FS_Standard
		ApplyButton.Font = Font_Calibri
		ApplyButton.Dock = DS_Fill
		ApplyButton.Margin = Padding_Zero
		ApplyButton.BackColor = RGB_Transparent
		ApplyButton.Anchor = (dotNetClass "System.Windows.Forms.AnchorStyles").None
		ButtonStrip.Controls.Add ApplyButton 0 0
		dotNet.AddEventHandler ApplyButton "Click" ApplyColors
		
		RestoreButton = dotNetObject "Button"
		RestoreButton.Text = "Restore"
		RestoreButton.FlatStyle = FS_Standard
		RestoreButton.Font = Font_Calibri
		RestoreButton.Dock = DS_Fill
		RestoreButton.Margin = Padding_Zero
		RestoreButton.BackColor = RGB_Transparent
		RestoreButton.Anchor = (dotNetClass "System.Windows.Forms.AnchorStyles").None
		ButtonStrip.Controls.Add RestoreButton 1 0
		dotNet.AddEventHandler RestoreButton "Click" Restore
		
		dotNet.AddEventHandler ColorCodeForm "Load" DispatchLoadIniFile
		dotNet.AddEventHandler ColorCodeForm "Closing" DispatchSaveIniFile
		dotNet.AddEventHandler ColorCodeForm "Move" DispatchSetWinLoc
		
		--draw form
		ColorCodeForm.showModeless()
		ColorCodeForm
	)
)

if ColorCode != undefined then
(
	ColorCode.ColorCodeForm.close()
)

ColorCode = ColorCodeStruct()
ColorCode.CreateUI()