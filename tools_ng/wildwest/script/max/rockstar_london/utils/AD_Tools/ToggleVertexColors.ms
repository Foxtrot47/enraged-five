--ToggleVertexColors

for this in selection where superClassOf this == GeometryClass do
(
	if this.showVertexColors == true then
	(
		this.showVertexColors = false
	)
	else
	(
		this.showVertexColors  = true
		this.vertexColorsShaded = false
	)
)