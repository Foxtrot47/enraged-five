-- script sets selected to no export

fn AD_NoExportSetTrue inobject =
(
	suffixNum = findString inobject.name "_NO_EXPORT"
	if suffixNum == undefined then
	(
		inobject.name = (inobject.name + "_NO_EXPORT")
	)
	setAttr inobject (getattrindex "Gta Object" "Export Geometry") false
	setAttr inobject (getattrindex "Gta Object" "Dont Export") true
	setAttr inobject (getattrindex "Gta Object" "Dont Add To IPL") true
)

fn AD_NoExportSetFalse inobject =
(
	suffixNum = findString inobject.name "_NO_EXPORT"
	if suffixNum != undefined then
	(
		newstring = replace inobject.name suffixNum 10 ""
		inobject.name = newstring
	)
	setAttr inobject (getattrindex "Gta Object" "Export Geometry") true
	setAttr inobject (getattrindex "Gta Object" "Dont Export") false
	setAttr inobject (getattrindex "Gta Object" "Dont Add To IPL") false
)

fn AD_NoExport mode =
(
	objSet = for this in selection where (getAttrClass this) == "Gta Object" collect this
	if objSet.count > 0 then
	(
		case(mode) of
		(
			"on":
			(
				print "on mode"
				for this in objSet do
				(
					AD_NoExportSetTrue this
				)
			)
			"off":
			(
				for this in objSet do
				(
					AD_NoExportSetFalse this
				)
				print ("No export flags removed on " + objSet.count as string + " objects")
			)
			"toggle":
			(
				print "toggle mode"
				for this in objSet do
				(
					if (GetAttr this (GetAttrIndex "Gta Object" "Export Geometry") == false) then
					(
						AD_NoExportSetFalse this
					)
					else
					(
						AD_NoExportSetTrue this
					)
				)
			)
			default: messagebox "invalid mode"
		)
	)
	else messagebox "no valid objects selected"
)

-- AD_NoExport "toggle"