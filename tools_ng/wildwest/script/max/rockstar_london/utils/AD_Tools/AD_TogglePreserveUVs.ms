-- print $.preserveUVs
if (selection.count == 1) and (classOf $ == Editable_Poly) then
(
	if $.preserveUVs == true then
	(
		$.preserveUVs = false
	)
	else
	(
		$.preserveUVs = true
	)
)
else
(
	messagebox "Please select valid geometry"
)