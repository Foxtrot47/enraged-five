--Parent.ms
--Revision 08/02/2011: ADTools info added

objs = selection as array
if objs.count > 1 then
(
	childrenset = #()
	for i = 1 to (objs.count - 1) do
	(
		objs[i].parent = objs[objs.count]
		append childrenset objs[i]
	)
	select childrenset
	try
	(
		if objs.count == 2 then ADTools.InfoPanel.Text = (objs[1].name + " parented to " + objs[2].name)
		else ADTools.InfoPanel.Text = ((objs.count - 1) as string + " objects parented to " + objs[objs.count].name)
	)
	catch()
)
else
(
	messagebox "Please select more than one object"
)