--Template.ms

filein (RsConfigGetWildWestDir() + "/script/max/rockstar_london/utils/RSL_dotNetUIOps.ms")
filein (RsConfigGetWildWestDir() + "/script/max/rockstar_london/utils/RSL_DotNetPresets2.ms")

global Template

struct TemplateStruct
(
	Form,
	ToolTip,
	InfoPanel,
	ApplyButton,
	IniFilePath = (RsConfigGetWildWestDir() + "script/max/rockstar_london/config/AD_Tools.ini"),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--GENERAL FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn Apply =
	(
		infoString = Template.SplitCasing "Template"
		try
		(
			ADTools.InfoPanel.Text = infoString
			Template.InfoPanel.Text = infoString
		)
		catch(messagebox infoString)
	),
	
	fn InitLabel tag align =
	(
		NewLabel = dotNetObject "Label"
		NewLabel.Text = tag
		NewLabel.Tag = tag
		NewLabel.TextAlign = align
		NewLabel.Font = Font_Calibri
		NewLabel.Dock = DS_Fill
		NewLabel.Margin = Padding_None
		return NewLabel
	),
	
	fn InitButton tag =
	(
		NewButton = dotNetObject "Button"
		NewButton.Tag = tag
		NewButton.FlatStyle = FS_Standard
		NewButton.BackColor = RGB_Transparent
		NewButton.ForeColor = RGB_Black
		NewButton.Text = tag
		NewButton.Dock = DS_Fill
		NewButton.Margin = Padding_None
		NewButton.Font = Font_Calibri
		NewButton.TextAlign = TA_Center
		return NewButton
	),
	
	fn SplitCasing instring capstart:true =
	(
		local regexObject = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) (@"(?<!^)(?=[A-Z])")
		local stringArray = regexObject.split instring
		local returnString = stringArray[1]
		if (capstart == true) then returnString[1] = toUpper returnString[1]
		if stringArray.count > 1 then
		(
			for i = 2 to stringArray.count do
			(
				returnString =  returnString + " " + toLower (stringArray[i])
			)
		)
		return returnString
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn SaveIniFile =
	(
		setINISetting Template.IniFilePath "Template" "WinLocX" (Template.Form.Location.x as string)
		setINISetting Template.IniFilePath "Template" "WinLocY" (Template.Form.Location.y as string)
		setINISetting Template.IniFilePath "Template" "WinWidth" (Template.Form.Width as string)
		setINISetting Template.IniFilePath "Template" "WinHeight" (Template.Form.Height as string)
	),
	
	fn LoadIniFile =
	(
		--default values
		local WinLocX = 0
		local WinLocY = 40
		local WinWidth = 60
		local WinHeight = 272
		
		try
		(
			WinLocX = getINISetting Template.IniFilePath "Template" "WinLocX" as integer
			WinLocY = getINISetting Template.IniFilePath "Template" "WinLocY" as integer
			WinWidth = getINISetting Template.IniFilePath "Template" "WinWidth" as integer
			WinHeight = getINISetting Template.IniFilePath "Template" "WinHeight" as integer
		)
		
		catch()
		
		Form.Location = dotNetObject "system.drawing.point" WinLocX WinLocY
		Form.Size = dotNetObject "System.Drawing.Size" WinWidth WinHeight
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn CreateUI =
	(
		-- form setup		
		Form = dotNetObject "maxCustomControls.maxForm"
		Form.Text = SplitCasing "Template"
		Form.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").manual
		Form.Location = dotNetObject "system.drawing.point" 0 80
		Form.MaximumSize = dotNetObject "System.Drawing.Size" 1600 1200
		Form.MinimumSize = dotNetObject "System.Drawing.Size" 100 100
		Form.FormBorderStyle = FBS_SizableToolWindow
		Form.SizeGripStyle = (dotNetClass "SizeGripStyle").show
		dotNet.AddEventHandler Form "Load" LoadIniFile
		dotNet.AddEventHandler Form "Closing" SaveIniFile
		
		--content
		ToolTip = dotnetobject "ToolTip"
		Table = dotNetObject "TableLayoutPanel"
		Table.Dock = DS_Fill
		Table.RowCount = 2
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "percent" 100)
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		Table.ColumnCount = 1
		Table.ColumnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		Form.Controls.Add Table
		
		InfoPanel = InitLabel "Test" TA_Center
		Table.Controls.Add InfoPanel 0 0

		ApplyButton = InitButton "Apply"
		dotNet.AddEventHandler ApplyButton "Click" Apply
		Table.Controls.Add ApplyButton 0 1
		
		--draw form
		Form.ShowModeless()
		Form
	)
)

if Template != undefined then
(
	Template.Form.Close()
)

Template = TemplateStruct()
Template.CreateUI()