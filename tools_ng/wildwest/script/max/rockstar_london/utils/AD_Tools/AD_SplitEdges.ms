-- split edges

-- polyop.splitEdges <Poly poly> <edgelist>

if (selection.count == 1) and (classOf $ == Editable_Poly) and (subObjectLevel == 2) then
(
	polyop.splitEdges selection[1] #selection
)

else
(
	messagebox "Select edges on a poly object to split"
)