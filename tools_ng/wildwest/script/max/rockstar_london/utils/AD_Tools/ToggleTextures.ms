--ToggleTextures.ms

if displayColor.shaded == #object then
(
	displayColor.shaded = #material
	try (ADTools.InfoPanel.Text = "Textures turned on")
	catch()
)
else
(
	displayColor.shaded = #object
	try (ADTools.InfoPanel.Text = "Textures turned off")
	catch()
)