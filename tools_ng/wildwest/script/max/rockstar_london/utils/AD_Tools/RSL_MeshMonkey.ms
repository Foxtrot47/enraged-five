--MeshMonkey.ms
--Andy Davis �2010 Rockstar London
--Toolbox for creating game artwork

--version 1.0

filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetUIOps.ms")

----------------------------------------------------------------------------------------------------------------
--GLOBALS
----------------------------------------------------------------------------------------------------------------
global MeshMonkey
----------------------------------------------------------------------------------------------------------------
--STRUCTURE
----------------------------------------------------------------------------------------------------------------

struct MeshMonkey_Struct
(	
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI VARIABLES
	------------------------------------------------------------------------------------------------------------------------------------------
	
	Form,
	WinLocX = 0,
	WinLocY = 120,
	WinWidth = 320,
	WinHeight = 240,
	IconFolder = RsConfigGetWildWestDir()+"script/max/rockstar_london/images/AD_Icons/",
	IniFilePath = RsConfigGetWildWestDir()+"script/max/rockstar_london/config/AD_Tools.ini",
	Iconsize = 32,
	TabPanel,
	StartTab = 0,
	InfoPanel,
	
	--DOTNET VARIABLES
	CBS_None = (dotNetClass "System.Windows.Forms.TableLayoutPanelCellBorderStyle").None,
	CBS_Single = (dotNetClass "System.Windows.Forms.TableLayoutPanelCellBorderStyle").Single,
	DS_Fill = (dotNetClass "System.Windows.Forms.DockStyle").Fill,
	Font_Calibri = dotNetObject "System.Drawing.Font" "Calibri" 8,
	RGB_Charcoal = (dotNetClass "System.Drawing.Color").fromARGB 40 40 40,
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--GENERAL FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn FileInWithArg script argument =
	(
		filein script
		local scriptCommand = getFilenameFile  script
		execute (scriptCommand + " \"" + argument + "\"")
	),
	
	fn MapImage imageName =
	(
		local iconPath = IconFolder + imageName
		return (dotNetClass "System.Drawing.Image").FromFile(iconPath)
	),
	
	fn InitButton =
	(
		local newButton = dotNetObject "Button"
		newButton.Size = dotNetObject "System.Drawing.Size" 32 32
		newButton.FlatStyle = (dotNetClass "System.Windows.Forms.FlatStyle").Popup
		newButton.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		return newButton
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn UpdateLocalConfigFileSave =
	(
		setINISetting IniFilePath "MeshMonkey" "StartTab" (StartTab as string)
		setINISetting IniFilePath "MeshMonkey" "WinLocX" (WinLocX as string)
		setINISetting IniFilePath "MeshMonkey" "WinLocY" (WinLocY as string)
		setINISetting IniFilePath "MeshMonkey" "WinWidth" (WinWidth as string)
		setINISetting IniFilePath "MeshMonkey" "WinHeight" (WinHeight as string)
	),
	
	fn UpdateLocalConfigFileLoad =
	(
		try
		(
			StartTab = getINISetting IniFilePath "MeshMonkey" "StartTab" as integer
			WinLocX = getINISetting IniFilePath "MeshMonkey" "WinLocX" as integer
			WinLocY = getINISetting IniFilePath "MeshMonkey" "WinLocY" as integer
			WinWidth = getINISetting IniFilePath "MeshMonkey" "WinWidth" as integer
			WinHeight = getINISetting IniFilePath "MeshMonkey" "WinHeight" as integer
		)
		catch()
		TabPanel.selectedIndex = StartTab
		Form.Location = dotNetObject "system.drawing.point" WinLocX WinLocY
		Form.Size = dotNetObject "System.Drawing.Size" WinWidth WinHeight	
	),
	
	fn DispatchLoadIniFile s e = MeshMonkey.UpdateLocalConfigFileLoad(),
	fn DispatchSaveIniFile s e =	MeshMonkey.UpdateLocalConfigFileSave(),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--EVENT HANDLERS
	------------------------------------------------------------------------------------------------------------------------------------------
	--window
	fn DispatchTabClicked s e = MeshMonkey.TabClicked s e,
	fn DispatchSetWinSize s e = MeshMonkey.SetWinSize s e,	
	fn DispatchSetWinLoc s e = MeshMonkey.SetWinLoc s e,
	fn TabClicked s e =	(StartTab = TabPanel.SelectedIndex),
		
	fn SetWinLoc s e =
	(
		WinLocX = Form.Location.X
		WinLocY = Form.Location.Y
	),
	
	fn SetWinSize s e =
	(
		WinWidth = Form.Size.Width
		WinHeight = Form.Size.Height
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--TAB CONTENT
	------------------------------------------------------------------------------------------------------------------------------------------
	
	--general tab
	fn PivotBase s e = MeshMonkey.FileInWithArg "AD_SetPivot.ms" "base",
	fn PivotCenter s e = MeshMonkey.FileInWithArg "AD_SetPivot.ms" "center",
	fn PivotOrigin s e = MeshMonkey.FileInWithArg "AD_SetPivot.ms" "origin",
	fn UnhideAll = max unhide all,
	fn WirecolorTool =
	(
		MeshMonkey.InfoPanel.Text = "Wirecolor Tool"
		filein "WirecolorTool.ms"
	),
	fn SelectByColor = max select by color,
	fn AttributeTool =
	(
		MeshMonkey.InfoPanel.Text = "Attribute Tool"
		filein "AttributeTool.ms"
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn CreateUI =
	(		
		-- form setup
		Form = dotNetObject "maxCustomControls.maxForm"
		Form.MaximumSize = dotNetObject "System.Drawing.Size" 1600 1280
		Form.MinimumSize = dotNetObject "System.Drawing.Size" 254 113
		Form.Size = dotNetObject "System.Drawing.Size" WinWidth WinHeight	
		Form.Text = "Mesh Monkey"
		Form.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		Form.FormBorderStyle = (dotNetClass "System.Windows.Forms.FormBorderStyle").SizableToolWindow
		dotNet.AddEventHandler Form "Load" DispatchLoadIniFile
		dotNet.AddEventHandler Form "Closing" DispatchSaveIniFile
		dotNet.AddEventHandler Form "Move" DispatchSetWinLoc
		dotNet.AddEventHandler Form "Resize" DispatchSetWinSize
		
		ToolTip = dotnetobject "ToolTip"
		
		Table = dotNetObject "TableLayoutPanel"
		Table = dotNetObject "TableLayoutPanel"
		Table.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		Table.ColumnCount = 1
		Table.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		Table.RowCount = 2
		Table.rowStyles.add (RS_dotNetObject.rowStyleObject "percent" 100)
		Table.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 15)
		Form.Controls.Add Table		
		
		TabPanel = dotNetObject "System.Windows.Forms.TabControl"
		TabPanel.Dock = DS_Fill
		Table.Controls.Add TabPanel
		Tab1 = dotNetObject "System.Windows.Forms.TabPage"
		Tab2 = dotNetObject "System.Windows.Forms.TabPage"
		Tab3 = dotNetObject "System.Windows.Forms.TabPage"
		Tab1.text = "General"
		Tab2.text = "Geometry"
		Tab3.text = "UV-Mapping"
		TabPanel.Controls.Add Tab1
		TabPanel.Controls.Add Tab2
		TabPanel.Controls.Add Tab3
		dotNet.AddEventHandler TabPanel "SelectedIndexChanged" DispatchTabClicked
		
		InfoPanel = dotNetObject "System.Windows.Forms.Label"
		InfoPanel.Text = "tools for mesh monkeys..."
		InfoPanel.Font = Font_Calibri
		InfoPanel.Dock = DS_Fill
		Table.Controls.Add InfoPanel
		
		------------------------------------------------------------------------------------------------------------------------------------------
		--GENERAL PANEL
		------------------------------------------------------------------------------------------------------------------------------------------
		
		GeneralPanel = dotNetObject "System.Windows.Forms.FlowLayoutPanel"
		GeneralPanel.Dock = DS_Fill
		GeneralPanel.BackgroundImage = MapImage "MonkeyBackground.bmp"
		GeneralPanel.BackColor = (dotNetClass "System.Drawing.Color").fromARGB 192 192 192
		Tab1.Controls.Add GeneralPanel
		
		PivotToOriginButton = InitButton()
		PivotToOriginButton.BackgroundImage = MeshMonkey.MapImage "AD_PivotToOrigin.bmp"
		ToolTip.SetToolTip PivotToOriginButton "Set pivot to origin"
		dotNet.AddEventHandler PivotToOriginButton "Click" PivotOrigin
		GeneralPanel.Controls.Add PivotToOriginButton
		
		PivotToBaseButton = InitButton()
		PivotToBaseButton.BackgroundImage = MapImage "AD_PivotToBase.bmp"
		ToolTip.SetToolTip PivotToBaseButton "Set pivot to base"
		dotNet.AddEventHandler PivotToBaseButton "Click" PivotBase
		GeneralPanel.Controls.Add PivotToBaseButton
		
		PivotToCenterButton = InitButton()
		PivotToCenterButton.BackgroundImage = MapImage "AD_PivotToCenter.bmp"
		ToolTip.SetToolTip PivotToCenterButton "Set pivot to center"
		dotNet.AddEventHandler PivotToCenterButton "Click" PivotCenter
		GeneralPanel.Controls.Add PivotToCenterButton
		
		UnhideAllButton = InitButton()
		UnhideAllButton.BackgroundImage = MapImage "AD_UnhideAll.bmp"
		ToolTip.SetToolTip UnhideAllButton "Unhide all"
		dotNet.AddEventHandler UnhideAllButton "Click" UnhideAll
		GeneralPanel.Controls.Add UnhideAllButton
		
		WirecolorToolButton = InitButton()
		WirecolorToolButton.BackgroundImage = MapImage "WirecolorTool.bmp"
		ToolTip.SetToolTip WirecolorToolButton "Wirecolor tool"
		dotNet.AddEventHandler WirecolorToolButton "Click" WirecolorTool
		GeneralPanel.Controls.Add WirecolorToolButton
		
		SelectByColorButton = InitButton()
		SelectByColorButton.BackgroundImage = MapImage "AD_SelectByColor.bmp"
		ToolTip.SEtToolTip SelectByColorButton "Select by Color"
		dotNet.AddEventHandler SelectByColorButton "Click" SelectByColor
		GeneralPanel.Controls.Add SelectByColorButton
		
		AttributeToolButton = InitButton()
		AttributeToolButton.BackgroundImage = MapImage "AttributeTool.bmp"
		ToolTip.SEtToolTip AttributeToolButton "Attribute Tool"
		dotNet.AddEventHandler AttributeToolButton "Click" AttributeTool
		GeneralPanel.Controls.Add AttributeToolButton
		
		GeometryPanel = dotNetObject "System.Windows.Forms.FlowLayoutPanel"
		GeometryPanel.Dock = DS_Fill
-- 		GeometryPanel.BackgroundImage = MapImage "MonkeyBackground.bmp"
		GeometryPanel.Text = "Coming soon..."
		Tab2.Controls.Add GeometryPanel
		
		TempLabel1 = dotNetObject "System.Windows.Forms.Label"
		TempLabel1.Text = "coming soon..."
		GeometryPanel.Controls.Add TempLabel1
		
		UVPanel = dotNetObject "System.Windows.Forms.FlowLayoutPanel"
		UVPanel.Dock = DS_Fill
-- 		UVPanel.BackgroundImage = MapImage "MonkeyBackground.bmp"
		UVPanel.Text = "Coming soon..."
		Tab3.Controls.Add UVPanel
		
		TempLabel2 = dotNetObject "System.Windows.Forms.Label"
		TempLabel2.Text = "coming soon..."
		UVPanel.Controls.Add TempLabel2
		
		------------------------------------------------------------------------------------------------------------------------------------------
		--DRAW FORM
		------------------------------------------------------------------------------------------------------------------------------------------
		Form.ShowModeless()
		Form
	)
)
----------------------------------------------------------------------------------------------------------------
--RUN TOOL COMMANDS
----------------------------------------------------------------------------------------------------------------
if MeshMonkey != undefined then
(
	MeshMonkey.Form.Close()
)
MeshMonkey = MeshMonkey_Struct()
MeshMonkey.CreateUI()