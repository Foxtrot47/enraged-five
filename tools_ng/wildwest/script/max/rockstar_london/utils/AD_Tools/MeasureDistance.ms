--MeasureDistance
--measures the distance between two objects' pivots
newDistance = undefined
if selection.count != 2 then
(
	print "Please select two objects"
)
else
(
	xdist = selection[1].pos.x - selection[2].pos.x
	ydist = selection[1].pos.y - selection[2].pos.y
	zdist = selection[1].pos.z - selection[2].pos.z
	newDistance = (sqrt (xdist * xdist + ydist * ydist + zdist * zdist))
	print newDistance
)
newDistance