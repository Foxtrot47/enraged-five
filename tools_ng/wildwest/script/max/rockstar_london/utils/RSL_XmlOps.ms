--------------------------------------------------------------------------------------------
-- 						RSL_XmlOps.ms
--							By Paul Truss
--							Senior Technical Artist
--							Rockstar Games
--							London
--							V1.0 28/4/2010
--
--							Contains common functions for gathering information from scene xml files
--
--------------------------------------------------------------------------------------------

global RSL_Xml

fileIn (scriptsPath + "pipeline\\util\\xml.ms")

struct RSL_XmlOps
(
	--------------------------------------------------------------------------------------------
	-- 						fixXml()
	--							Removes lines in xml files that cause the dotNet xml load function to crash.
	--
	--						XMLFile: the xml file to fix
	--------------------------------------------------------------------------------------------
	fn fixXml XMLFile =
	(
		local dataStream = openFile XMLFile
		local outStream = stringStream ""
		
		while not (eof dataStream) do
		(
			local line = (readLine dataStream)
			
			if line.count > 0 then
			(
				format "%\n" line to:outStream
			)
		)
		
		close dataStream
		
		dataStream = openFile XMLFile mode:"w+"
		
		seek outStream 0
		
		while not (eof outStream) do
		(
			local line = (readLine outStream)
			
			if line.count > 0 then
			(
				format "%\n" line to:dataStream
			)
		)
		
		close dataStream
	),
	
	--------------------------------------------------------------------------------------------
	-- 						getXMLNodeByName()
	--							Searches through the children of XMLNode to find the node that matches the name.
	--
	--						XMLNode: the xml node to start looking from
	--						nodeName: the name to search for
	--------------------------------------------------------------------------------------------
	
	fn getXMLNodeByName XMLNode nodeName =
	(
		local result
		
		for i = 0 to XMLNode.ChildNodes.Count - 1 do
		(
			local currentNode = XMLNode.ChildNodes.Item[i]
			
			if (toLower currentNode.name) == (toLower nodeName) then
			(
				result = currentNode
			)
		)
		
		result
	),
	
	--------------------------------------------------------------------------------------------
	-- 						findNodeByNameAttribute()
	--							Recursive function, searches through the children of XMLNode to find the node that matches the name attribute.
	--
	--						XMLNode: the xml node to start looking from
	--						nodeName: the name to search for
	--						&foundNode: Out parameter set to the node if found
	--------------------------------------------------------------------------------------------
	fn findNodeByNameAttribute XMLNode nodeName &foundNode =
	(
		for i = 0 to XMLNode.ChildNodes.Count - 1 do
		(
			local currentNode = XMLNode.ChildNodes.Item[i]
			local name = (currentNode.Attributes.ItemOf "name")
			
			if name != undefined and (toLower name.value) == (toLower nodeName) then
			(
				foundNode = currentNode
				exit
			)
			else
			(
				if currentNode.ChildNodes.Count > 0 then
				(
					findNodeByNameAttribute currentNode nodeName &foundNode
				)
			)
		)
	),
	
	--------------------------------------------------------------------------------------------
	-- 						getObjectTransform()
	--							Searches the xml file for the node that matches objectName and returns the postion attribute.
	--							Returns [0,0,0] if the boundName cannot be found
	--
	--						xmlFile: the axml file to read
	--						objectName: the object name to search for in the xml
	--------------------------------------------------------------------------------------------
	fn getObjectTransform xmlFile objectName =
	(
		local result = #([0,0,0], quat 0 0 0 0)
		
		local xml = XmlDocument()
		xml.init()
		xml.load xmlFile
		
		local objectsElement = getXMLNodeByName xml.document.DocumentElement "objects"
		
		findNodeByNameAttribute objectsElement objectName &foundNode
		
		if foundNode != undefined then
		(
			local transformNode = (getXMLNodeByName (getXMLNodeByName foundNode "transform") "object")
			
			local positionElement = getXMLNodeByName transformNode "position"
			
			if positionElement != undefined then
			(
				result[1] = [(positionElement.Attributes.ItemOf "x").value as float, \
							(positionElement.Attributes.ItemOf "y").value as float, \
							(positionElement.Attributes.ItemOf "z").value as float]
			)
			
			local rotationElement = RSL_Xml.getXMLNodeByName transformNode "rotation"
			
			if rotationElement != undefined then
			(
				result[2] = (quat ((rotationElement.Attributes.ItemOf "x").value as float) \
									((rotationElement.Attributes.ItemOf "y").value as float) \
									((rotationElement.Attributes.ItemOf "z").value as float) \
									((rotationElement.Attributes.ItemOf "w").value as float))
			)
			
-- 			format "Name:%\n\tPos:%\n\tRot:%\n" result[1] result[2]
		)
		else
		(
			local errorStream = stringStream ""
			format "RSL_Xml.getObjectPositionRotationFromXml() --- Exception\n\tUnable to find %\nXML File: %\nFile exists: %\n\n" objectName xmlFile (doesFileExist xmlFile) to:errorStream
			format "%" (errorStream as string) to:listener
			
			messageBox (errorStream as string) title:"R* Max Script Error..."
		)
		
		result
	)
)

RSL_Xml = RSL_XmlOps()