
try (destroyDialog RSL_ModelRenderer) catch()

dotnet.loadAssembly (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\dll\\RGLExportHelpers.dll")

rollout RSL_ModelRenderer " R* Model Renderer" width:240 height:100
(
	local drawFunctions = dotNetClass "RGLExportHelpers.DrawFunctions"
	local point3Class = dotNetClass "RGLExportHelpers.point3"
	
	local resolution = [1280,1024]
	local resolutionArray = #([1280,1024],[640,480],[320,240])
	local planResolution = [1280,1024]
	
	local padding = 16
	local paddingArray = #(16, 8, 4)
	
	local masterDims
	
	local colourDataFile = project.common + "/data/carcols.dat"
	local savePath = undefined
	
	local presetColours = #()
	local colourSettings = #(#(),#())
	
	local topArray = #()
	local frontArray = #()
	local sideArray = #()
	local cameraArray = #()
	
	local renderObjects = #()
	
	fn readCarCols =
	(
		local dataStream = openFile colourDataFile
		
		if dataStream != undefined then
		(
			skipToString dataStream "col\n"
			
			local line = readLine dataStream
			
			while line != "end" do
			(
				if line[1] != "#" and line.count > 0 then
				(
					local filtered = filterString line ", \t#"
					
					local preset = color (filtered[1] as float) (filtered[2] as float) (filtered[3] as float)
					
					append presetColours preset
				)
				
				line = readLine dataStream
			)
			
			skipToString dataStream "car3\n"
			
			while not (eof dataStream) do
			(
				if line.count > 0 and line != "end" and line != "car4" and line[1] != "#" then
				(
					local filtered = filterString line ", \t"
					
					append colourSettings[1] filtered[1]
					append colourSettings[2] filtered[2]
				)
				
				line = readLine dataStream
			)
		)
		else
		(
			format "% not found\n" colourDataFile
		)
	)

	mapped fn getSkin object =
	(
		local l1 = matchPattern object.name pattern:"*l1*"
		local l2 = matchPattern object.name pattern:"*l2*"
		local coll = matchPattern object.name pattern:"*_coll"
		
		if not l1 and not l2 and not coll then
		(
			if (superClassOf object) == geometryClass then
			(
				append renderObjects object
			)
		)
	)

	mapped fn hideNonRenderObjects object =
	(
		if findItem renderObjects object == 0 then
		(
			object.isHidden = true
		)
	)

	fn collectRageShaderAttributes rageShader = -- collects the variable names and values from the rage shader
	(
		local outArray = #(#(),#(),#())
		if (classOf rageShader) == Rage_Shader then
		(
			local count = RstGetVariableCount rageShader
			local isAlphaShader = RstGetIsAlphaShader rageShader
			
			append outArray[1] "Name"
			append outArray[2] "string"
			append outArray[3]  rageShader.name
			
			append outArray[1] "Shader"
			append outArray[2] "shaderList"
			append outArray[3]  (RstGetShaderName rageShader)
			
			local textureCount = 0
			try(for i = 1 to count where (matchPattern (RstGetVariableType rageShader i) pattern:"Texmap") do textureCount += 1)catch()
			
			for i = 1 to count do
			(
				local name = (RstGetVariableName rageShader i)
				local variableType = (RstGetVariableType rageShader i)
				local variable = (RstGetVariable rageShader i)
				
				if (classOf variable) == float and variable < 0.01 then variable = 0.0
				
				append outArray[1] name
				append outArray[2] variableType
				append outArray[3] variable
				
				if name == "Diffuse Texture" and isAlphaShader == true then
				(
					local alphaTexture
					local subTexture = (getSubTexmap rageShader (textureCount + i))
					
					if subTexture != undefined then
					(
						alphaTexture = (getSubTexmap rageShader (textureCount + i)).filename
					)
					else
					(
						alphaTexture = ""
					)
					
					append outArray[1] "Alpha Texture"
					append outArray[2] "texmap"
					append outArray[3] alphaTexture
				)
			)
		)
		outArray
	)

	fn convertSpecularValues intensity fallOff = -- converts the glossiness and specular level values
	(
		local outPoint2 = [0,0]
		
		outPoint2.x = (fallOff / 2000) * 100
		outPoint2.y = 100.0 * intensity
		outPoint2
	)

	fn getColourFromSetting name =
	(
		local result
		local index = 0
		
		for i = 1 to colourSettings[1].count do
		(
			local setting = colourSettings[1][i]
			
			if setting == name then
			(
				index = i
			)
		)
		
		if index > 0 then
		(
			result = presetColours[index + 1]
		)
		
		result
	)

	fn substituteStringByArray string array =
	(
		local result = string
		
		for entry in array do
		(
			result = substituteString result entry ""
		)
		
		result
	)

	fn createStandardMaterial rageShaderProperties =
	(
		local material = StandardMaterial()
		
		material.glossinessMapAmount = 25
		
		for i = 1 to rageShaderProperties[1].count do
		(
			case (rageShaderProperties[1][i]) of
			(
				"Name":
				(
					material.name = rageShaderProperties[3][i]
				)
				"Shader":
				(
					if rageShaderProperties[3][i] == "vehicle_vehglass.sps" or rageShaderProperties[3][i] == "vehicle_lights.sps" then
					(
						material.opacity = 0
						material.opacityMap = fallOff type:2
					)
					if  rageShaderProperties[3][i] == "vehicle_paint1.sps" then
					(
						local carColour = getColourFromSetting (substituteStringByArray maxFilename #("_skinned",".max"))
						
						if carColour != undefined then
						(
							material.Diffuse = carColour
							material.diffuseMapAmount = 0
						)
					)
				)
				"Diffuse Texture":
				(
					local diffuseAndSpecularColour = bitmaptexture filename:rageShaderProperties[3][i]
					
					material.diffuseMap = diffuseAndSpecularColour
					material.specularMap = diffuseAndSpecularColour
				)
				"Alpha Texture":
				(
					local opacityMap = bitmaptexture filename:rageShaderProperties[3][i]
					
					if opacityMap.filename != "" then
					(
						material.opacityMap = opacityMap
					)
				)
				"Bump Texture":
				(
					local normal = Normal_Bump normal_map:(bitmaptexture filename:rageShaderProperties[3][i])
					
					material.bumpMap = normal
					material.bumpMapAmount = 100
				)
				"Specular Texture":
				(
					local specularLevelAndGlossiness = bitmaptexture filename:rageShaderProperties[3][i]
					
					material.specularLevelMap = specularLevelAndGlossiness
					material.glossinessMap = specularLevelAndGlossiness
				)
				"specular map intensity mask color":
				(
					material.Specular = (rageShaderProperties[3][i] * 255) as color
				)
				"Specular Falloff":
				(
					material.specularLevel = (convertSpecularValues (rageShaderProperties[3][i] as float) (rageShaderProperties[3][i] as float)).x
				)
				"Specular Intensity":
				(
					material.glossiness = (convertSpecularValues (rageShaderProperties[3][i] as float) (rageShaderProperties[3][i] as float)).y
				)
				"Environment Texture":
				(
					local reflectionMap = bitmaptexture filename:rageShaderProperties[3][i]
					
					material.reflectionMap = reflectionMap
				)
				"Reflectivity":
				(
					material.reflectionMapAmount = (rageShaderProperties[3][i] as float)
				)
				
				default:
				(
	-- 				format "Name: % Type: % Variable: %\n" rageShaderProperties[1][i] rageShaderProperties[2][i] rageShaderProperties[3][i]
				)
			)
		)
		material
	)

	fn processMaterial material =
	(
		local newMaterial = material
		case (classOf material) of
		(
			rage_shader:
			(
				local shaderProps = collectRageShaderAttributes material
				newMaterial = (createStandardMaterial shaderProps)
			)
			MultiMaterial:
			(
				local newMulti = multimaterial numSubs:(material.materialList.count)
				newMulti.name = material.name + "_StandardConverted"
				
				for i = 1 to material.MaterialList.count do
				(
					local subMaterial = material.MaterialList[i]
					
					if (classOf submaterial) ==  rage_shader then
					(
						local shaderProps = collectRageShaderAttributes submaterial
						
						newMulti.MaterialList[i] = (createStandardMaterial shaderProps)
					)
					else
					(
						newMulti.MaterialList[i] = submaterial
					)
					newMulti.names[i] = material.names[i]
					newMulti.MaterialIDList[i] = material.MaterialIDList[i]
				)
				
				newMaterial = newMulti
			)
		)
		newMaterial
	)

	fn getPerspectiveCorrectedCenter node =
	(
		local zPlane = -2
		local maximum = [-9999999,-9999999,zPlane]
		local minimum = [9999999,9999999,zPlane]
		
		for object in selection do
		(
			local mesh = snapShot object
			
			for vert in mesh.verts do
			(
				in coordsys node position = getVert mesh vert.index
				
				position.x *= zPlane / position.z
				position.y *= zPlane / position.z
				position.z = zPlane
				
				if position.x > maximum.x then maximum.x = position.x
				if position.y > maximum.y then maximum.y = position.y
				
				if position.x < minimum.x then minimum.x = position.x
				if position.y < minimum.y then minimum.y = position.y
				
	-- 			if (mod vert.index 8) == 0.0 then
	-- 			(
	-- 				p = point pos:(position *= node.transform) size:0.04 wirecolor:red
	-- 			)
			)
			
			if isValidNode mesh then delete mesh
		)
		
		local bound = maximum - minimum
		local width = -9999999
		
		for i = 1 to 3 do
		(
			if bound[i] > width then width = bound[i]
		)
		
		maximum *= node.transform
		minimum *= node.transform
		local newCenter = minimum + ((maximum - minimum) * 0.5)
		
	-- 	p = point pos:maximum size:1
	-- 	p = point pos:minimum size:1
	-- 	p = point pos:newCenter size:3
		
		#(newCenter, width)
	)

	mapped fn switchLights light =
	(
		light.enabled = not light.enabled 
	)
	
	fn bitmapToArray bitmap array =
	(
		for h = 0 to bitmap.height - 1 do
		(
			append array (getPixels bitmap [0,h] bitmap.width)
		)
	)
	
	fn arrayToBitmap array =
	(
		local result = bitmap array[1].count array.count
		
		for i = 0 to array.count - 1 do
		(
			setPixels result [0, i] array[i + 1]
		)
		
-- 		display result
		
		result
	)

	fn hasAlpha array =
	(
		local result = false
		
		for entry in array do
		(
			if entry.a > 0 then
			(
				result = true
			)
		)
		
		result
	)

	fn cropBitmapByAlpha array =
	(
		local topLeft = [0,0]
		local bottomRight = [0,0]
		local size = [0,0]
		
		local top = array.count / 2
		local bottom = array.count / 2
		local left = array[1].count / 2
		local right = array[1].count / 2
		
		local amount = array.count / 2
		local last = 9999
		
		local finished = false
		
		while not finished do
		(
			if top != last then
			(
				last = top
				amount = amount / 2
				
				if hasAlpha array[top] then
				(
					topLeft.y = top
					
					top -= amount
				)
				else
				(
					top += amount
				)
			)
			else
			(
				finished = true
			)
		)
		
		amount = array.count / 2
		last = 9999
		
		finished = false
		
		while not finished do
		(
			if bottom != last then
			(
				last = bottom
				amount = amount / 2
				
				if hasAlpha array[bottom] then
				(
					bottomRight.y = bottom
					
					bottom += amount
				)
				else
				(
					bottom -= amount
				)
			)
			else
			(
				finished = true
			)
		)
		
		amount = array[1].count / 2
		last = 9999
		
		finished = false
		
		while not finished do
		(
			if left != last then
			(
				last = left
				amount = amount / 2
				
				verticalArray = for i = 1 to array.count collect array[i][left]
				
				if hasAlpha verticalArray then
				(
					topLeft.x = left
					
					left -= amount
				)
				else
				(
					left += amount
				)
			)
			else
			(
				finished = true
			)
		)
		
		amount = array[1].count / 2
		last = 9999
		
		finished = false
		
		while not finished do
		(
			if right != last then
			(
				last = right
				amount = amount / 2
				
				verticalArray = for i = 1 to array.count collect array[i][right]
				
				if hasAlpha verticalArray then
				(
					bottomRight.x = right
					
					right += amount
				)
				else
				(
					right -= amount
				)
			)
			else
			(
				finished = true
			)
		)
		
		local result = #()
		
		for i = topLeft.y to bottomRight.y do
		(
			local croppedArray = for c = topLeft.x to bottomRight.x collect array[i][c]
			
			append result croppedArray
		)
		
		result
	)
	
	fn toInteger float =
	(
		ceil (abs float) as integer
	)

	fn getPixelSizes =
	(
		local xPixelDistance = 0.0
		local yPixelDistance = 0.0
		local zPixelDistance = 0.0
		
		local screenSpaceMax = selection.max * viewport.getTM()
		local screenMaxOrigin = mapScreenToView [0,0] screenSpaceMax.z resolution
		local screenMaxEnd = mapScreenToView resolution screenSpaceMax.z resolution
		local worldSizeMax = screenMaxOrigin - screenMaxEnd
		
		local maxXAspect = resolution.x / (abs worldSizeMax.x)
		local maxYAspect = resolution.y / (abs worldSizeMax.Y)
		
		local screenSpaceMin = selection.min * viewport.getTM()
		local screenMinOrigin = mapScreenToView [0,0] screenSpaceMin.z resolution
		local screenMinEnd = mapScreenToView resolution screenSpaceMin.z resolution
		local worldSizeMin = screenMinOrigin - screenMinEnd
		
		local minXAspect = resolution.x / (abs worldSizeMin.x)
		local minYAspect = resolution.y / (abs worldSizeMin.Y)
		
		local screenCoordsMax = point2 (maxXAspect * (screenSpaceMax.x - screenMaxOrigin.x)) -(maxYAspect * (screenSpaceMax.y - screenMaxOrigin.y))
		local screenCoordsMin = point2 (minXAspect * (screenSpaceMin.x - screenMinOrigin.x)) -(minYAspect * (screenSpaceMin.y - screenMinOrigin.y))
		
		local xPixelDistance = (toInteger (screenCoordsMax.x - screenCoordsMin.x))
		local yPixelDistance = (toInteger (screenCoordsMax.y - screenCoordsMin.y))
		
		local xDistance = screenSpaceMax.x - screenSpaceMin.x
		local zDistance = screenSpaceMax.z - screenSpaceMin.z
		
		local scale = 1 / (xDistance / zDistance)
		
		local zPixelDistance = toInteger (xPixelDistance * scale)
		
		[xPixelDistance, yPixelDistance, zPixelDistance]
	)

	fn setMasterDimentions =
	(
		max zoomext sel
		local pixelSizes = getPixelSizes()
		
		local width = pixelSizes.x + pixelSizes.y + 64
		local height = pixelSizes.y + pixelSizes.z + 64
		
		local finished = false
		local last = [0,0,0]
		
		while not finished do
		(
			masterDims = getPixelSizes()
			local masterWidth = (masterDims.x + masterDims.y + (padding * 4)) as float
			local scale = 1 / (resolution.x / masterWidth)
			
			if  abs (1 - scale) > 0.001 and masterDims != last then
			(
				viewport.Zoom scale
				completeRedraw()
				
				last = masterDims
			)
			else
			(
				finished = true
			)
		)
		
		if (masterDims.y + masterDims.z + 64) > resolution.y then
		(
			finished = false
			last = [0,0,0]
			
			while not finished do
			(
				masterDims = getPixelSizes()
				local masterHeight = (masterDims.y + masterDims.z + (padding * 4)) as float
				local scale = 1 / (resolution.y / masterHeight)
				
				if  abs (1 - scale) > 0.001 and masterDims != last then
				(
					viewport.Zoom scale
					completeRedraw()
					
					last = masterDims
				)
				else
				(
					finished = true
				)
			)
		)
		
		masterDims
	)
	
	fn zoomToFit view =
	(
		max zoomext sel
		local finished = false
		local last = [0,0,0]
		
		while not finished do
		(
			local currentDims = getPixelSizes()
			
			local currentWidth = (currentDims.x + currentDims.z + (padding * 4)) as float
			local scale = 1 / ((masterDims.x + masterDims.y + (padding * 4)) / currentWidth)
			
			if  abs (1 - scale) > 0.002 and currentDims != last then
			(
				viewport.Zoom scale
				completeRedraw()
				
				last = currentDims
			)
			else
			(
				finished = true
			)
		)
	)

	fn renderViews objectArray =
	(
		switchLights lights
		
		select objectArray
		
		local mainPos = selection.center + ((selection.max - selection.min) * 1.5)
		mainPos.z *= 2.5
		mainPos.x = -mainPos.x
		
		local mainDirect = directionalLight name:"mainDirect" pos:mainPos
		mainDirect.shadowGenerator = raytraceShadow()
		mainDirect.baseObject.castShadows = on
		mainDirect.raytraceBias = 0.2
		mainDirect.rgb = color 255 250 232

		mainDirect.dir = normalize (mainPos - selection.center)
		
		local sky = skyLight name:"Sky"
		sky.multiplier = 0.75
		sky.color = color 173 181 210
		
		viewport.ResetAllViews()
		if viewport.isWire() then max wire smooth
		
		max zoomext sel all
		
		local currentRenderer = renderers.current
-- 		renderers.current = mental_ray_renderer()
-- 		renderers.current.GlobalIllumEnable = true
		renderers.current = Default_Scanline_Renderer()
		scanlineRender.antiAliasFilter = Catmull_Rom()
		
		local advancedLighting = RadiosityPreferences.useRadiosity
		RadiosityPreferences.useRadiosity = false
		
		local currentExposure = SceneExposureControl.exposureControl
		SceneExposureControl.exposureControl = undefined
		
		local back_Ground = backgroundColor
	-- 	local ambient = ambientColor
		
		backgroundColor = color 124 124 124
	-- 	ambientColor = color 120 120 120
		
		for i = 1 to 4 do
		(
			viewport.activeViewport = i
			
			local type = viewport.getType()
			
			if type == #view_top and i == 1 then
			(
				setMasterDimentions()
			)
			
			if type == #view_front then viewport.setType #view_back
			
			if type == #view_persp_user then
			(
				largest = distance selection.max selection.min
				local cameraPos = selection.center + [-largest, largest, largest * 0.5]
				
				local renderCam = freeCamera name:"renderCam" pos:cameraPos
				renderCam.dir = normalize (renderCam.pos - selection.center)
				local center = (getPerspectiveCorrectedCenter renderCam)[1]
				renderCam.dir = normalize (renderCam.pos - center)
				
				local renderBitmap = bitmap resolution.x resolution.y
				
				render camera:renderCam vfb:true to:renderBitmap
				
				bitmapToArray renderBitmap cameraArray
				renderBitmap = undefined
				
				if isValidNode renderCam then delete renderCam
				
				gc()
			)
			else
			(
				if i > 1 then
				(
					zoomToFit type
				)
				
				local renderBitmap = bitmap planResolution.x planResolution.y
				
				render vfb:true to:renderBitmap
				
				case type of
				(
					#view_top:(bitmapToArray renderBitmap topArray)
					#view_front:(bitmapToArray renderBitmap frontArray)
					#view_left:(bitmapToArray renderBitmap sideArray)
				)
				
				renderBitmap = undefined
				
				gc()
			)
		)
		
		delete mainDirect
		delete sky
		
		renderers.current = currentRenderer
		
		backgroundColor = back_Ground
	-- 	ambientColor = ambient
		
		SceneExposureControl.exposureControl = currentExposure
		
		RadiosityPreferences.useRadiosity = advancedLighting
	)
	
	fn toDotNetPoint x y =
	(
		result = dotNetObject "System.Drawing.Point" x y
	)
	
	fn compositeFinalImage =
	(
		freeSceneBitmaps()
		
		local topImage = arrayToBitmap (cropBitmapByAlpha topArray)
		topArray =undefined
		local frontImage = arrayToBitmap (cropBitmapByAlpha frontArray)
		frontArray =undefined
		local sideImage = arrayToBitmap (cropBitmapByAlpha sideArray)
		sideArray =undefined
		local cameraImage = arrayToBitmap (cropBitmapByAlpha cameraArray)
		cameraArray =undefined
		
		local widthMeasure = dotNetObject point3Class (padding - 1) (padding + frontImage.height + padding + topImage.height) topImage.width
		local lenghtMeasure = dotNetObject point3Class ((padding - 1) + topImage.width + padding) (padding + (frontImage.height - 1) + padding) topImage.height
		local heightMeasure = dotNetObject point3Class (padding + frontImage.width + padding + sideImage.width) (padding - 1) frontImage.height
		
		gc()
		
		local finalImage = bitmap resolution.x resolution.y filename:savePath color:(color 124 124 124 0)
		local planComposite = bitmap resolution.x resolution.y color:(color 124 124 124 0)
		
		pasteBitmap frontImage planComposite [0,0] [(padding - 1),(padding - 1)] type:#blend
		pasteBitmap sideImage planComposite [0,0] [padding + (frontImage.width - 1) + padding,(padding - 1)] type:#blend
		pasteBitmap topImage planComposite [0,0] [(padding - 1),padding + (frontImage.height - 1) + padding] type:#blend
		
		topImage = undefined
		frontImage = undefined
		sideImage = undefined
		
		pasteBitmap planComposite finalImage [0,0] [0,0] type:#blend
		pasteBitmap cameraImage finalImage [0,0] [finalImage.width - (cameraImage.width + (padding * 4)), finalImage.height - (cameraImage.height + (padding * 4))] type:#blend
		
		cameraImage = undefined
		freeSceneBitmaps()
		
-- 		display finalImage
		
		save finalImage
		close finalImage
		finalImage = undefined
		freeSceneBitmaps()
		
		local bound = selection.max - selection.min
		
		local widthText = (abs bound.x) as string
		local lengthText = (abs bound.y) as string
		local heightText = (abs bound.z) as string
		
		drawFunctions.drawMeasurements widthMeasure lenghtMeasure heightMeasure widthText lengthText heightText savePath
		drawFunctions.nameOnBitmap (toUpper maxFilename) savePath
		
		finalImage = openBitmap savePath
		display finalImage
	)

	fn processScene = 
	(
		local visible = for object in objects where not object.isHidden collect object
		
		renderObjects = #()
		getSkin objects
		
		hideNonRenderObjects objects
		
		for object in renderObjects do
		(
			local newMaterial = processMaterial object.material
			
			object.material = newMaterial
		)
		
		renderViews renderObjects
		
		compositeFinalImage()
	)
	
	
	
	
	
	editText edt_path width:206 across:2 align:#left offset:[-8,0] readOnly:true
	button btn_pickPath "..." width:20 height:17 align:#right offset:[6,0]
	label lbl_res "Resolution" across:2 aligh:#left offset:[-32,4]
	dropDownList ddl_res width:90 offset:[24,0]
	label lbl_planMulti "Plan Multiplier" across:2 aligh:#left offset:[-25,0]
	spinner spn_planMulti width:90 offset:[-12,0] range:[0.25, 1.0, 1.0]
	button btn_render "Render" width:224 height:17 enabled:false
	
	on RSL_ModelRenderer open do
	(
		readCarCols()
		
		ddl_res.Items = for entry in resolutionArray collect entry as string
		
		
	)
	
	on btn_pickPath pressed do
	(
		savePath = getSaveFileName caption:"Pick save path..." types:"bmp(*.bmp)|*.bmp|tga(*.tga)|*.tga|tiff(*.tif)|*.tif"
		
		if savePath != undefined then
		(
			local okToContinue = true
			
			if (getFiles savePath).count > 0 then
			(
				okToContinue = not (getFileAttribute savePath #readOnly)
			)
			
			if okToContinue then
			(
				edt_path.text = savePath
				
				btn_render.enabled = true
			)
			else
			(
				messageBox "File is read only." title:"Error..."
			)
		)
	)
	
	on ddl_res selected index do
	(
		resolution = readValue (ddl_res.selected as stringStream)
		padding = paddingArray[index]
		
		planResolution = resolution * spn_planMulti.value
	)
	
	on spn_planMulti changed value do
	(
		planResolution = resolution * value
	)
	
	on btn_render pressed do
	(
		processScene()
	)
)

createDialog RSL_ModelRenderer style:#(#style_toolwindow,#style_sysmenu)