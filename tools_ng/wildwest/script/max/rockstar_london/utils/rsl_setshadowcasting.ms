


try (destroyDialog RSL_AutoShadows) catch()

rollout RSL_AutoShadows "RS Set Shadow Casting by Size" width:180
(
	fn processScene =
	(
	staticIndex = getAttrIndex "Gta Object" "Static Shadows"
	dynamicIndex = getAttrIndex "Gta Object" "Dynamic Shadows"
	castIndex = getAttrIndex "Gta Object" "Dont cast shadows"
	minimumSize = RSL_AutoShadows.spn_min.value
	maximunSize = RSL_AutoShadows.spn_max.value

		for object in geometry where (getAttrClass object) == "Gta Object" do
		(
			local bounds = object.max - object.min
			local volume = bounds.x + bounds.y + bounds.z
			local avgsize = volume/3 
			
			--local size = (length bounds)
		-- 	format "% : %\n" size minimumSize
			if avgsize >= minimumSize and avgsize <= maximunSize then
			(
		 		print ("Object: "+ object.name+" Setting Static and Dynamic shadow flags to false ")
				
				if (RSL_AutoShadows.chk_castShadows.checked) then 	setAttr object castIndex true  
				if (RSL_AutoShadows.chk_recieveShadows.checked) then setAttr object staticIndex false
				if (RSL_AutoShadows.chk_recieveShadowsDyn.checked) then setAttr object dynamicIndex false
	
			
			)
		)
	)
	
	spinner spn_min "Set Min Size  (meter)" range:[0,1,0.0] type:#float align:#center	
	spinner spn_max "Set Max Size  (meter)" range:[0,100,10.0] type:#float align:#center	
	button btn_autoLOD "Update Shadow Casting Objects" width:160 align:#center
	checkbox chk_recieveShadows "Recieve Static Shadows" checked:true
	checkbox chk_recieveShadowsDyn "recieve Dynamic Shadows" checked:true
	checkbox chk_castShadows "Cast Shadows" checked:true
	
	
	on btn_autoLOD pressed do
	(
		processScene()
	)
	
	
	
)

createDialog RSL_AutoShadows style:#(#style_toolwindow,#style_sysmenu)
