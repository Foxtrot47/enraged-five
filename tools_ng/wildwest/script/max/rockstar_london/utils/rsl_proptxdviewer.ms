-- Prop TXD USage Viewer --
clearlistener()
try (destroyDialog Prop_TXD_map) catch ()
mapprops = #()
ipltemp = "c:/ipltemp/"
-- get m
range =80
minx =0
miny =0
mapscale =0
border = 12

-- collect xrefs in scene --

fn collectprops = (
	proparray = #()
	for i in objects where classof(i) == xrefobject do
	(
		append proparray i
	)
	proparray
)

fn getpropbounds proplist= (
	bnds = #()
	for i in proplist do
	(
		
	)

	
)
-----------------------------------------------------------------------------------------
---					TXD SIZE													--
------------------------------------------------------------------------------------------
fn gettxdsize mapname propfolder txdname= (
	
	local txdfile = Project.localdir+"\\convert\\dev\\levels\\"+mapname+"\\props\\"+propfolder+"\\"+txdname+".itd"
	--print txdfile
	local txdsize = getFileSize txdfile 
	if txdsize == 0 then
 	(
		--print txdfile
		txdfile = Project.network+"\\stream\\maps\\"+mapname+"\\"+propfolder+"\\"+txdname+".itd"
		txdsize = getFileSize txdfile 
	)
 	if txdsize == 0 then
 	(
		txdfile = Project.netstream+"\\maps\\"+mapname+"\\"+propfolder+"\\"+txdname+".itd"
		txdsize = getFileSize txdfile 
	)
	txdsize
)

-----------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
---					get map name from filename										--
------------------------------------------------------------------------------------------
fn getmapname = (
	s=0
	d = filterstring maxfilepath "\\"
	if d[d.count] == "interiors" then s = 1
	if d[d.count] == "props" then s = 1
	mapname = d[d.count-s]
	return mapname
)

-----------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
---					getlist of prop ide files 										   --
------------------------------------------------------------------------------------------

fn getpropfiles mapname= (
	
	proppath = project.independent +"\\levels\\" +mapname--+"\\props"
	--print proppath
	files = getFiles (proppath+"\*.ide")
	proppath = project.independent +"\\levels\\" +mapname+"\\props"
	join files (getFiles (proppath+"\*.ide"))
)

------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
---					Load ide file and get list of unique props					   --
------------------------------------------------------------------------------------------

fn loadIDEfile filename= (
	fileHandle  = openFile filename
	local temparray = #()	
	while eof fileHandle == false do
			(	
				--local Nmatch = keymatch.Match (i.name)
				append temparray (readline fileHandle )
			)	
	close  fileHandle 
				
	local blockmatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\w.+)")
	local keymatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("((\w+)\,\s(\w+)\,\s\w.+)")
	found = false
	local propnames = #()
	propfolder = getFilenameFile filename
	mapname = getmapname()
	for eachline in temparray do
	(
		local Nmatch = blockmatch.Match (eachline)
		if Nmatch.groups.item[1].value == "objs" then found = true
		if Nmatch.groups.item[1].value == "end" then found =  false
		if found == true then 
		(
			local propmatch = keymatch.Match (eachline)
			if propmatch.groups.item[1].success == true then
			(
				-- getTXD size here --

				--print "calling txd funtion"
				local txdsize = gettxdsize mapname propfolder (propmatch.groups.item[3].value)
				--print txdsize
				append propnames #(propmatch.groups.item[2].value,propmatch.groups.item[3].value,txdsize)
			)
		)
	)
	propnames
)


------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
---						read file and return data  	
--   Need to put this and above into one function 									--
------------------------------------------------------------------------------------------

fn loadIPLfile filename = 
(
	fileHandle  = openFile filename
	local temparray = #()	
	while eof fileHandle == false do
			(	
				--local Nmatch = keymatch.Match (i.name)
				append temparray (readline fileHandle )
			)	
	close  fileHandle 
			
	local blockmatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\w.+)")
	local keymatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("((\w+)\,\s\w+\,\s(.[0-9]+\.[0-9]+)\,\s(.[0-9]+\.[0-9]+)\w.+)")	-- OMG. name/posX/posY
	found = false
	local propnames = #()
		
	for eachline in temparray do
	(
		local Nmatch = blockmatch.Match (eachline)
		if Nmatch.groups.item[1].value == "inst" then found = true
		if Nmatch.groups.item[1].value == "end" then found =  false
		if found == true then 
		(
			local propmatch = keymatch.Match (eachline)
			if propmatch.groups.item[1].success == true then
			(
				append propnames #(propmatch.groups.item[2].value,propmatch.groups.item[3].value,propmatch.groups.item[4].value)
			)
		)
	)
	propnames
)

------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
---						Extract map ipl's								     	--
------------------------------------------------------------------------------------------

fn extractipl image  = (
	dir = (filterstring image "\\")
	map = (dir[dir.count-1]+"/")
	makeDir (ipltemp +map+"/")-- create a temp dir regardless if it exists or not
	Ragebuilder = "ragebuilder_0327.exe "
	args = rsconfig.toolsbin+"/util/data_extract_ipl.rb -i "+image +" -o " +(ipltemp+map)
	HiddenDOSCommand (Ragebuilder+args) startpath:(rsconfig.toolsbin+"/bin/")
	
)

-----------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
---						Get Global used map ipls								     	--
------------------------------------------------------------------------------------------
fn loadmapsIPLs mappath= (
	dir = (filterstring mappath "\\")
	map = (dir[dir.count]+"/")
	local files = getFiles (mappath+"\*.img")
	-- Lets extract all the ipl's fromt eh image files
	for filename in files do (extractipl filename)
	local files = getFiles (ipltemp+map+"\*.ipl")
	allpropsused =#()
	for filename in files do 
		(
			join allpropsused (loadIPLfile filename)
		)
	allpropsused
	--print allpropsused
)

-----------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
---						Get Global used props								     	--
------------------------------------------------------------------------------------------
fn filterproplist proplist usedprops = (
	newproplist = #()
	for i in proplist do
	(
		--print i.name
		for s in usedprops do
		(
			if i.name == s[1] then 
			(
				append newproplist #(i.name,i.pos.x,i.pos.y,s[2],s[3])
			)
		)
	)
	newproplist 
)

-----------------------------------------------------------------------------------------
---						Get Global used props on all maps						     	--
------------------------------------------------------------------------------------------

fn filterproplistall proplist usedprops = (
	newproplist = #()
	for i in proplist do
	(
		--print i.name
		for s in usedprops do
		(
			if i[1] == s[1] then 
			(
				append newproplist #(i[1],i[2] as float,i[3] as float ,s[2],s[3])
			)
		)
	)
	newproplist 
)


-----------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

fn calcmem memlist = (
	local val = 0
	for v in memlist do
	(
		val+=v
	)
	val
)

rollout Prop_TXD_map "Untitled" width:1380 height:1050
(
	multilistBox lbx1 "ListBox" width:330 height:8 align:#left
	button btn1 "Show TXDs" width:118 height:20 align:#left
	button btn2 "Show Global TXDs" width:118 height:20 align:#left

	slider Scanrange "Scan Radius" orient:#horizontal ticks:0 range:[0,800,80] pos:[180,150] width:160 
	label labobjname "object" align:#left 	
	label labname "" style_sunkenedge:true width:166 height:696 align:#left 	
	label labobjtxd "TXD" pos:[180,192]
	label labtxd ""  style_sunkenedge:true pos:[180,213] width:84 height:696 align:#left
	label labtxdsize ""  style_sunkenedge:true pos:[266,213] width:70 height:696 align:#left
	label labtxt "Total Memory" align:#left 
	label txdmem "0k" style_sunkenedge:true width:150 height:16 align:#left 
	label lab2 "x: y:" style_sunkenedge:true width:150 height:16 align:#left
	bitmap bmpImage pos:[350,10] height:1024 width:1024-- align:#right
	
	fn createblockmap proplist = 
		(
		try(unDisplay bm1)catch()
		
		
		proptxds = #()
		for i in proplist do
		(
			appendifunique proptxds i[2]
		)

	
		--print proptxds.count
		colours = #()
		for c =  1 to proptxds.count do
		(
			xcol =  (random (color 0 0 0) (color 255 255 240))
				appendifunique colours xcol
		)
		--colours = #(red,green,blue,yellow,orange,black,brown,gray,(color 1 1 1),(color 9 9 9),(color 128 9 9),(color 80 23 78),(color 80 5 5))
		
		offset = 600
		bm1 = bitmap 1024 1024 color:white
		--bm1 = bitmap 1018 1018 filename:"X:\\Art\\localDB\\Map_iamges\\s_fav1.bmp"
		--"X:\\Art\\localDB\\Thumbs_props\\MissingDefult.bmp"
		--bm1 = bitmap 1018 1018 filename:"X:\\Art\\\localDB\\\Map_iamges\\s_fav1.jpg"
		--bm1 = openBitMap "X:\\Art\\\localDB\\\Map_iamges\\s_fav1.bmp"
		
		-- This is used to find the lowest number to start from . i,e if lowest coridante = -20 then we need to - -20 from the postion data to display correclty on the map
		local tempx = #()
		local tempy = #()
		for p in proplist do
		(
			append tempx (p[2]) -- pos.x
			append tempy (p[3]) -- pos.y
		)
		minx = amin(tempx)
		miny = amin(tempy)

		-- Get the Min/Max of the prop sizes
		--print ("minx = "+minx as string)
		--print ("miny = "+miny as string)
		range = #(( amax(tempx) - amin(tempx) ),( amax(tempy)-amin(tempy) ))
		print range

		-- Lets get the minimun range value to scale with to keep the aspect ratio
		mapscale = (1000/amax(range)) -- taken 24 off as border
		

		--print rangey	
		
		-- Add some padding
		
		for p in proplist do
		(
			colID= findItem proptxds p[2] 

			bm2 = bitmap 6 6 color:colours[colID]
			--pasteBitmap bm2 bm1 [0,0] [((p[1].pos.x-16)*22),((p[1].pos.y*-1)-240)*22] --pos
			posx = (p[2] - minx)
			posy = (p[3] - miny)
				
				
			pasteBitmap bm2 bm1 [0,0] [(posx*mapscale)+border+130,(posy*mapscale)+border]
		)
	Prop_TXD_map.bmpImage.bitmap = bm1
	--display bm1
	)
	
	on Prop_TXD_map open do
	(
		local props = getpropfiles (getmapname())
		lbx1.items = props
		bmpImage.bitmap = bitmap 1024 1024 color:gray
		--print (Scanrange.value)
	)
	on btn1 pressed do 
	(
		gc()
		props =  lbx1.items
		globalproplist = #()
		usedprops = #()
		for i in lbx1.selection do
		(
			usedprops = loadIDEfile props[i]
			join globalproplist usedprops
		)
		propsinflevel = collectprops() 	
		--print propsinflevel 
		if globalproplist.count > 0 then(
		mapprops = filterproplist propsinflevel globalproplist
		--print mapprops
		createblockmap mapprops
			)
		--startTool foo prompt:"Hello!"
		
	)
	
	on btn2 pressed do 
	(
		gc()
		props =  lbx1.items
		globalproplist = #()
		usedprops = #()
		mapname = getmapname()
		for i in lbx1.selection do
		(
			usedprops = loadIDEfile props[i]
			join globalproplist usedprops
		)
		
		propsinflevel = loadmapsIPLs (Project.independent +"\\levels\\"+mapname)
		if globalproplist.count > 0 then(
		mapprops = filterproplistall propsinflevel globalproplist
		--print mapprops
		createblockmap mapprops
			)
		
		
		
	)
	
	
	on Prop_TXD_map mousemove pos do
	(
		setSysCur #select
		--clearlistener()
		offsetx =((abs(pos.x))-350)	-- coords of the 1024 bitmap screen 
		offsety=((abs(pos.y))-10) -- coords of the 1024 bitmap screen 

		
		if mapprops.count > 0 then
		(
		--print (ceil((mapprops[1][1].pos.x-16)*22))
		)
		
		if offsetx > 0 and offsety > 0 then
		(
			local found = ""			
			local txdlist = ""
			local txdsizelist = ""
			local txdnames = #()
			local totalmemory = #()
			for i in mapprops do
			(
				--px = (ceil((i[1].pos.x-16)*22))
				--py = (ceil(((i[1].pos.y*-1)-240)*22))
				
				local posx = (((i[2] - minx)*mapscale)+border)
				local posy =(((i[3] - miny)*mapscale)+ border)

				
					
				if offsetx > posx-(Scanrange.value/2) and offsetx < posx+(Scanrange.value/2) then
				(
					if  offsety > posy-(Scanrange.value/2) and offsety < posy+(Scanrange.value/2) then
					(
							found += ((i[1]) +" / "+ i[4]+"\n")
							if appendifunique txdnames (tolower i[4]) then
							(
							txdlist += (i[4] as string+"\n")
							txdsizelist += (i[5]  as string +"\n")
							)
							appendifunique totalmemory (i[5])
					)

				)
			)
			--print totalmemory
			
			
			if found != "" then
			(
				labname.text = found 
				labtxd.text = txdlist 
				labtxdsize.text = txdsizelist
				local mem = calcmem totalmemory
				txdmem.text = (mem as string + " /  " + ((mem/1024) as string)+"KB / "+((mem/1024)/1024)as string+" Mb")
				
			)
			else
			(
				labname.text = ""
				labtxd.text = ""
				labtxdsize.text = ""
				txdmem.text = "0"
			)
		lab2.text = (offsetx as string+" "+offsety as string)
	
	
		)
	gc()

	)

	
	
)

createDialog Prop_TXD_map

