-- Not a direct Script.
-- USed as a struct by other scripts.


struct RSL_maxFile_LevelReport
(
	name,
	textures,
	polys,
	collisionPolys,
	LOD_Distance,
	memory,
	TXD,
	TXD_memory,
	TXD_textures,
	Volume,
	polyDensity,
	memoryDensity,
	
-- 	this is here purely because using getPropNames() on a struct does not return the properties in the order they were written in. It must be ignored by any generic report functions.
-- 	See HTMLtableReport, it is ignored because it is used to access the properties of the struct and the "reportOrder" property is not in the array
	
	reportOrder =
	#(
		#name,
		#textures,
		#polys,
		#collisionPolys,
		#LOD_Distance,
		#memory,
		#TXD,
		#TXD_memory,
		#TXD_textures,
		#Volume,
		#polyDensity,
		#memoryDensity
	)
)

struct RSL_reporter
(
	fn getMaterialIDsFromObject object =
	(
		local outArray = #()
		
		case (classOf object) of
		(
			Editable_mesh:
			(
				for i = 1 to object.faces.count do
				(
					local currentID = getFaceMatID object i
					
					if (findItem outArray currentID) == 0 and currentID != undefined  then
					(
						append outArray currentID
					)
				)
			)
			Editable_poly:
			(
				for i = 1 to object.faces.count do
				(
					local currentID = polyop.getFaceMatID object i
					
					if (findItem outArray currentID) == 0 and currentID != undefined then
					(
						append outArray currentID
					)
				)
			)
		)
		sort outArray
	),
	
	fn getTextureCount object outputTextures:false =
	(
		local result = 0
		local material = object.material
		local texturesFound = #()
		
		if material != undefined do
		(
			case (classOf material) of
			(
				Standardmaterial:
				(
					local textureCount = getNumSubTexmaps material
					
					for i = 1 to textureCount do
					(
						local subTexture = getSubTexmap material i
						
						if (classOf subTexture) == bitmapTexture and subTexture.filename != undefined then 
						(
							local alreadyFound = findItem texturesFound subTexture.filename
							
							if alreadyFound == 0 then
							(
								result += 1
								append texturesFound subTexture.filename
							)
						)
					)
				)
				
				Rage_Shader:
				(
					local textureCount = getNumSubTexmaps material
					
					for i = 1 to textureCount do
					(
						local subTexture = getSubTexmap material i
						
						if (classOf subTexture) == bitmapTexture and subTexture.filename != undefined then
						(
							local alreadyFound = findItem texturesFound subTexture.filename
							
							if alreadyFound == 0 then
							(
								result += 1
								append texturesFound subTexture.filename
							)
						)
					)
				)
				
				Multimaterial:
				(
					local IDarray = getMaterialIDsFromObject object
					
					for i = 1 to material.count do
					(
						local subMaterial = material.materialList[i] 
						local foundID = findItem IDarray material.materialIDList[i] 
						
						if foundID != 0 and subMaterial != undefined then
						(
							local textureCount = getNumSubTexmaps subMaterial
							
							for i = 1 to textureCount do
							(
								local subTexture = getSubTexmap subMaterial i
								
								if (classOf subTexture) == bitmapTexture and subTexture.filename != undefined then
								(
									local alreadyFound = findItem texturesFound subTexture.filename
									
									if alreadyFound == 0 then
									(
										result += 1
										append texturesFound subTexture.filename
									)
								)
							)
						)
					)
				)
			)
		)
		if outputTextures then result = #(result, texturesFound)
		result
	),
	
	fn getPolyCount object =
	(
		local result = 0
		try result = object.numFaces catch ()
		result
	),
	
	fn getCollisionPolyCount object =
	(
		local result = 0
		for child in object.children where (classOf child) == Col_Mesh do
		(
			result += getcollpolycount child
		)
		result
	),
	
	fn getLODdistance object =
	(
		local result = undefined
		local idxLODDistance = getattrindex "Gta Object" "LOD distance"
		try result = (getattr object idxLODDistance) catch ()
		result
	),
	
	fn getDirectoriesRecursive master =
	(
		local outArray = getDirectories (master + "/*")
		for dir in outArray do
		(
			join outArray (getDirectories (dir + "/*"))
		)
		outArray
	),
	
	fn findFile file directories =
	(
		local result = 0
		for dir in directories do
		(
			local pathAndFile = (dir + file)
			if (getFiles pathAndFile).count > 0 then result = pathAndFile
		)
		result
	),
	
	fn getObjectMemory object master =
	(
		local result = 0
		local idrFileName = object.name + ".idr"
		local directories = getDirectoriesRecursive master
		local fileFound = (findFile idrFileName directories)
		
		if fileFound != 0 then
		(
			local size = (getFileSize fileFound) / 1024
			
			if size >= 1024 then
			(
				local mbSize = (size / 1024.0) as string
				if mbSize.count > 5 then mbSize = subString mbSize 1 5
					
				result = mbSize + "MB"
			)
			else
			(
				result = size as string + "K"
			)
		)
		result
	),
	
	fn getTXD object =
	(
		local result = undefined
		local idxTXD = getattrindex "Gta Object" "TXD"
		try result = getattr object idxTXD catch ()
		result
	),
	
	fn getTXDmemory object master TXD:undefined=
	(
		local result = 0
		local TXDname
		
		if TXD != undefined then
		(
			TXDname = TXD
		)
		else
		(
			TXDname = getTXD object
		)
		
		if TXDname != undefined then
		(
			local itdFileName = TXDname + ".itd"
			local directories = getDirectoriesRecursive master
			local fileFound = (findFile itdFileName directories)
			
			if fileFound != 0 then
			(
				local size = (getFileSize fileFound) / 1024
				
				if size >= 1024 then
				(
					local mbSize = (size / 1024.0) as string
					if mbSize.count > 5 then mbSize = subString mbSize 1 5
						
					result = mbSize + "MB"
				)
				else
				(
					result = size as string + "K"
				)
			)
		)
		result
	),
	
	fn getTXDtextureCount object outputTextures:false =
	(
		local result = 0
		local TXD = getTXD object
		local texturesFound = #()
		
		if TXD != 0 then
		(
			local TXDobjects = for obj in objects where (getattrclass obj) == "Gta Object" and (classOf obj) != XRefObject and (getTXD obj) == TXD collect obj
			
			for obj in TXDobjects do
			(
				local material = obj.material
				
				if material != undefined do
				(
					case (classOf material) of
					(
						Standardmaterial:
						(
							local textureCount = getNumSubTexmaps material
							
							for i = 1 to textureCount do
							(
								local subTexture = getSubTexmap material i
								
								if (classOf subTexture) == bitmapTexture and subTexture.filename != undefined then 
								(
									local alreadyFound = findItem texturesFound subTexture.filename
									
									if alreadyFound == 0 then
									(
										result += 1
										append texturesFound subTexture.filename
									)
								)
							)
						)
						
						Rage_Shader:
						(
							local textureCount = getNumSubTexmaps material
							
							for i = 1 to textureCount do
							(
								local subTexture = getSubTexmap material i
								
								if (classOf subTexture) == bitmapTexture and subTexture.filename != undefined then
								(
									local alreadyFound = findItem texturesFound subTexture.filename
									
									if alreadyFound == 0 then
									(
										result += 1
										append texturesFound subTexture.filename
									)
								)
							)
						)
						
						Multimaterial:
						(
							local IDarray = getMaterialIDsFromObject obj
							
							for i = 1 to material.materialList.count do
							(
								local subMaterial = material.materialList[i] 
								
								if subMaterial != undefined then
								(
									local foundID = findItem IDarray material.materialIDList[i] 
									
									if foundID != 0 then
									(
										local textureCount = getNumSubTexmaps subMaterial
										
										for i = 1 to textureCount do
										(
											local subTexture = getSubTexmap subMaterial i
											
											if (classOf subTexture) == bitmapTexture and subTexture.filename != undefined then
											(
												local alreadyFound = findItem texturesFound subTexture.filename
												
												if alreadyFound == 0 then
												(
													result += 1
													append texturesFound subTexture.filename
												)
											)
										)
									)
								)
							)
						)
					)
				)
			)
		)
		if outputTextures then result = #(result, texturesFound)
		result
	),
	
	fn getObjectVolume object = 
	( 
		local Volume= 0.0 
		local theMesh = snapshotasmesh object
		local numFaces = theMesh.numfaces 
		
		for i = 1 to numFaces do 
		( 
			local Face= getFace theMesh i 
			local vert2 = getVert theMesh Face.z 
			local vert1 = getVert theMesh Face.y 
			local vert0 = getVert theMesh Face.x 
			local dV = Dot (Cross (vert1 - vert0) (vert2 - vert0)) vert0
			Volume+= dV 
		) 
		delete theMesh
		Volume /= 6 
		Volume
	),
	
	fn getPolyDensity object =
	(
		local result = 0
		local polyCount = getPolyCount object
		local volume = getObjectVolume object
		if polyCount != 0 and volume != undefined then
		(
			result = (polyCount as float) / volume
		)
		result
	),
	
	fn getMemoryDensity object master =
	(
		local result = 0
		local memory = getObjectMemory object master
		local volume = getObjectVolume object
		if memory != 0 and volume != undefined then
		(
			local typeK = matchPattern memory pattern:"*K"
			local typeMB = matchPattern memory pattern:"*MB"
			local convertToValue = 0
			
			if typeK do convertToValue = (subString memory 1 (memory.count - 1)) as float
			if typeMB do convertToValue = ((subString memory 1 (memory.count - 2)) as float) * 1024.0
				
			result = convertToValue / volume
		)
		result
	),
	
	fn HTMLtableReport reports order header file = -- reports must be an array of stucts and order must be an array of property names from the struct.
	(															--  This is because using getPropNames() on a struct does not return the properties in the order they were written in.
		local result = false
		
		if (getFiles file).count != 0 then
		(
			deleteFile file
		)
		
		local newHTML = createFile file
		
		if newHTML != undefined do
		(
			format "<A HREF=\"N:\RSGLDN\Projects\MP3\Builds\MaxBuildReport.html\">Max Build Report</A>" to:newHTML -- default link to main page, needs to be changed
			format "<html>\n<body>\n\n" to:newHTML
			format "<h3>%</h3>\n" header to:newHTML
			format "<p><i><small>Generated on:%</small></i></p>\n" localTime to:newHTML
			if (superClassOf reports[1]) == StructDef then
			(
				format "<table border=\"1\" CELLSPACING=0 CELLPADDING=2 BORDERCOLOR=BLACK RULES=ALL FRAME=BOX>\n<tr>\n" to:newHTML
				for item in order do -- assign headings to the table
				(
					format "<th>%</th>\n" (item as string) to:newHTML
				)
				format "</tr>\n" to:newHTML
				
				for report in reports do
				(
					if (superClassOf report) == StructDef then
					(
						format "<tr>\n" to:newHTML
						
						for item in order do -- assign values to the headings
						(
							local writeValue = getProperty report item
							if writeValue == undefined or writeValue == 0 then writeValue = "unknown"
							case item of
							(
								#name:
								(
									format "<td bgcolor=\"#72ff75\">%</td>\n" writeValue to:newHTML
								)
								default:
								(
									format "<td bgcolor=\"#D8D8D8\">%</td>\n" writeValue to:newHTML
								)
							)
						)
						format "</tr>\n" to:newHTML
						
						
					)
					else
					(
						format "Report is not a struct superClass, got:%\n" (superClassOf report) to:listener
					)
				)
				format "</table>\n" to:newHTML
				result = true
			)
			else
			(
				print "bugger"
			)
			format "\n\n</body>\n</html>" to:newHTML
			close newHTML
		)
		result
	),
	
	fn reportFailure header file =
	(
		if (getFiles file).count != 0 then
		(
			deleteFile file
		)
		
		local newHTML = createFile file
		
		if newHTML != undefined do
		(
			format "<A HREF=\"N:\RSGLDN\Projects\MP3\Builds\MaxBuildReport.html\">Max Build Report</A>" to:newHTML -- default link to main page, needs to be changed
			format "<html>\n<body>\n\n" to:newHTML
			format "<h3>%</h3>\n" header to:newHTML
			format "<p><i><small>Generated on:%</small></i></p>\n" localTime to:newHTML
			format "<p>Export script failure</p>\n" to:newHTML
			format "\n\n</body>\n</html>" to:newHTML
			close newHTML
		)
	)
)

-- initReport = RSL_reporter()
-- initReport.getMaterialIDsFromObject $

-- masterDirectory = "X:\\cache\\local"

-- objectArray = for object in objects collect (RSL_maxFile_LevelReport name:object.name textures:(initReport.getTextureCount object)\
-- polys:(initReport.getPolyCount object) collisionPolys:(initReport.getCollisionPolyCount object) LOD_Distance:(initReport.getLODdistance object) memory:(initReport.getObjectMemory object masterDirectory)\
-- TXD:(initReport.getTXD object) TXD_memory:(initReport.getTXDmemory object masterDirectory) TXD_textures:(initReport.getTXDtextureCount object) Volume:(initReport.getObjectVolume object)\
-- polyDensity:(initReport.getPolyDensity object) memoryDensity:(initReport.getMemoryDensity object masterDirectory))


-- 	name,
-- 	textures,
-- 	polys,
-- 	collisionPolys,
-- 	LOD_Distance,
-- 	memory,
-- 	TXD,
-- 	TXD_memory,
-- 	TXD_textures,
-- 	Volume,
-- 	polyDensity,
-- 	memoryDensity

-- initReport.HTMLtableReport objectArray objectArray[1].reportOrder ("Object Report for: " + maxFileName) "X:/tempHTMLReport.html"
