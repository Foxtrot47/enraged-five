

filein "pipeline/util/p4.ms"
filein "rockstar/export/settings.ms"
clearlistener()

RsCollCats = #()
RsCollItems = #()
RsIdxPartName = getattrindex "RAGE Particle" "Name"
RsIdxTriggerName = getattrindex "RAGE Particle" "Trigger"

unRegisterRedrawViewsCallback showPHelpers

struct RsCollParticle (name, type)

RsCollParts = #()
RsCollCurrPartNames = #()
selst = #()

-- Move this to global functions list
fn revisionN =
(
	rev = " V 0.10"
	return(rev as string)
)


fn showPHelpers  =
(
	
	
	for i in helpers where not i.ishidden do
	(
	i	if classof(i) == RAGE_Particle then
			(
				
				if i.pos.z < 0 then gw.setColor #line (color 0 180 0)
					else gw.setColor #line green
				
			gw.setTransform i.transform
			PointA = [0,0,0]
			PointB = [0,0,1]

			gw.polyline #(PointA, PointB) false
			gw.Polyline #([-0.2,0.2,0], [0.2,0.2,0], [0.2,-0.2,0],[-0.2,-0.2,0])true 

			--////////////////////////////////////////////////////////////
			-- Draw Connection Lines. Currently Disabled as not really needed
			--////////////////////////////////////////////////////////////
			/*
			if (i.parent) != undefined then
				(
					gw.setColor #line blue
					gw.setTransform (matrix3 1)
					PointA = i.pos
					PointB = i.parent.pos
					
					gw.polyline #(PointA, PointB) false
			)
			*/
			--gw.setTransform (matrix3 1)
			--gw.text (i.pos) "test" color:[255,255,255]--i.name

			gw.enlargeUpdateRect #whole
			gw.updateScreen()
			)

	)
)

rollout AddTint "Tint"
(
	group ""
	(
	colorPicker theColor  "" width:96 height:64 color:(color 255 255 255)
	button btnSet "Set" width:60 pos:[114,28]
	checkbox HasTint "Has Tint" checked:false pos:[114,60]
	checkbox IgnoreDmgMdl "Ignore Damage Model" checked:false pos:[18,100] enabled:true
	checkbox PlayonPrnt "Play On Parent" checked:false pos:[18,120] enabled:true
	)
	
	on btnSet pressed do
	(
		Tcolor = theColor.color

		
		for i in selection do
		(
			if classof(i) == RAGE_Particle then
			(
				if HasTint.checked then --7 Has Tint"
				(
					setattr i 7 True
					
					setattr i 8 Tcolor.red
					setattr i 9 Tcolor.green
					setattr i 10 Tcolor.blue
				)
				else
				(
					setattr i 7 false
				)
				
				if IgnoreDmgMdl.checked then	(setattr i 11 True)	else (setattr i 11 false)		--11
				if PlayonPrnt.checked then	(setattr i 12 True)	else (setattr i 12 false)			--12

			)
			
			
		)
	
		
	)
)


rollout ListHelpers "Helpers Selection List"
(
	MultiListBox mlb "" height:18
	
	
	on mlb selected val do
	(
		Csel = #()
		for id in mlb.selection do
		(
		--print id
		n=getnodebyname mlb.items[id]
			
		AddTint.theColor.color = [(getattr n 8),(getattr n 9),(getattr n 10)]
		-- Add check for Has Tint

		AddTint.HasTint.checked	= (getattr n 7)
	
			
		if n != undefined then (append Csel n)
		)
		select Csel

	)

)

rollout AddHelper "Particle Helper"
(
	group "Helpers"
	(
	button btnAdd "Add Helpers" width:100
	button btnSelHelpers "Select Helpers" width:100
	checkbutton btnShowH "Display Helpers" width:100
	)
	
	fn Updatelistbox = (
		currentSel = selection as array
		Plist = #()
		for i in currentSel do
			(
			if classof(i) == RAGE_Particle then (append Plist i.name)
			)
		ListHelpers.mlb.items = Plist
		-- get 1st object in list box
		--local SelectedItem = (ListHelpers.mlb.selection)
		
		local ItemCount =  ListHelpers.mlb.items.count
		if ItemCount == 1 then
		(
		ListHelpers.mlb.selection = 1
		n=getnodebyname ListHelpers.mlb.items[1]			
		AddTint.theColor.color = [(getattr n 8),(getattr n 9),(getattr n 10)]
		AddTint.HasTint.checked	= (getattr n 7)
		)
		else
		(
			AddTint.HasTint.checked = false
			AddTint.theColor.color = [255,255,255]
		)
			
	
		
		
		)
		
	fn selecthelpers node = (

		if classof(node) == RAGE_Particle then
		(
			selectMore node
		)
		for child in node.children do (selecthelpers child)
		
	)

	on btnShowH changed theState do
	(
		if btnShowH.checked then
		(
			registerRedrawViewsCallback showPHelpers
			completeRedraw()
		)	
		else
		(
			unRegisterRedrawViewsCallback showPHelpers
			completeRedraw()
		)
		
	)
	
	on btnAdd pressed do
	(
		CurrentSel = selection as array
		clearselection()
		for i in CurrentSel  do
		(
			if superclassof(i) == geometryclass then
			(
			Phelper = RAGE_Particle()
			
			
			try (CentrePos = [(i.max.x+i.min.x)/2, (i.max.y+i.min.y)/2, (i.max.z+i.min.z)/2]) catch (CentrePos= i.pos)
			Phelper.pos = CentrePos
			Phelper.parent = i
			selectmore Phelper 
			)
		)
		Updatelistbox()
	)
	
	
	on btnSelHelpers  pressed do
	(
		CurrentSel = selection as array
		clearselection()
		for i in CurrentSel  do
		(
			selecthelpers i
		)
		Updatelistbox()
	)
	
)

rollout RsPartSetupRoll "Particle Setup" 
(
	--////////////////////////////////////////////////////////////
	-- Saved Settings
	--////////////////////////////////////////////////////////////
	
	fn UpdateLocalConfigFileSave =
	(
		setINISetting "c:/PayneParticleHelperTool.ini" "ParticleTools" "chkAmbient" (RsPartSetupRoll.chkAmbient.checked as string)
		setINISetting "c:/PayneParticleHelperTool.ini" "ParticleTools" "chkBreak" (RsPartSetupRoll.chkBreak.checked as string)
		setINISetting "c:/PayneParticleHelperTool.ini" "ParticleTools" "chkShot" (RsPartSetupRoll.chkShot.checked as string)
		setINISetting "c:/PayneParticleHelperTool.ini" "ParticleTools" "chkCollision" (RsPartSetupRoll.chkCollision.checked as string)
		setINISetting "c:/PayneParticleHelperTool.ini" "ParticleTools" "chkDestruction" (RsPartSetupRoll.chkDestruction.checked as string)
		setINISetting "c:/PayneParticleHelperTool.ini" "ParticleTools" "chkAnim" (RsPartSetupRoll.chkAnim.checked as string)
	)
	fn UpdateLocalConfigFileLoad =
	(
		try(
		RsPartSetupRoll.chkAmbient.checked = getINISetting "c:/PayneParticleHelperTool.ini" "ParticleTools" "chkAmbient" as BooleanClass
		RsPartSetupRoll.chkBreak.checked = getINISetting "c:/PayneParticleHelperTool.ini" "ParticleTools" "chkBreak" as BooleanClass
		RsPartSetupRoll.chkShot.checked = getINISetting "c:/PayneParticleHelperTool.ini" "ParticleTools" "chkShot" as BooleanClass
		RsPartSetupRoll.chkCollision.checked = getINISetting "c:/PayneParticleHelperTool.ini" "ParticleTools" "chkCollision" as BooleanClass
		RsPartSetupRoll.chkDestruction.checked = getINISetting "c:/PayneParticleHelperTool.ini" "ParticleTools" "chkDestruction" as BooleanClass
		RsPartSetupRoll.chkAnim.checked = getINISetting "c:/PayneParticleHelperTool.ini" "ParticleTools" "chkAnim" as BooleanClass
		)
		catch()
	)
	
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	
	group "Filter"
	(
	checkbutton chkAmbient "Ambient" checked:true	width:220 height:18 pos:[10,20]
	checkbutton chkBreak "Break" checked:true width:220 height:18 pos:[10,40]
	checkbutton chkShot "Shot" checked:true width:220 height:18 pos:[10,60]
	checkbutton chkCollision "Collision" checked:true width:220 height:18 pos:[10,80]
	checkbutton chkDestruction "Destruction" checked:true width:220 height:18 pos:[10,100]
	checkbutton chkAnim "Anim" checked:true width:220 height:18 pos:[10,120]
		
	)
	
	
	group "Add Effect"
	(
	dropdownlist cboParticle ""
	button btnSet "Set" width:100
	button btnClone "Clone" width:100
	button btnID "Generate Unique ID's" width:100
	checkbox chkTuneData "Update Fragment Tune Data" checked: true
		
	--checkbox attachAll "Attach to All" checked:false enabled:true   -- This actually asigns to effect to ALL particle effects so disabling this.
	)
	
	
	
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn LoadEntries = (
	
		filename = ( RsConfigGetCommonDir() + "/data/effects/entityFx.dat" )

		intFile = openfile filename mode:"rb"

		newType = #none
		
		while eof intFile == false do (
		
			intLine = RsRemoveSpacesFromStartOfString(readline intFile)
			
			if intLine.count > 0 and intLine[1] != "#" then (

				intTokens = filterstring intLine " \t"
				

								
				if intTokens[1] == "ENTITYFX_AMBIENT_START" then (

					newType = #ambient
				) else if intTokens[1] == "ENTITYFX_COLLISION_START" then (

					newType = #collision
				) else if intTokens[1] == "ENTITYFX_SHOT_START" then (

					newType = #shot
				) else if intTokens[1] == "FRAGMENTFX_BREAK_START" then (

					newType = #break
				) else if intTokens[1] == "FRAGMENTFX_DESTROY_START" then (

					newType = #destruction
				) else if intTokens[1] == "ENTITYFX_ANIM_START" then (

					newType = #anim
				) 
				else if intTokens[1] == "ENTITYFX_SHOT_END" or  intTokens[1] == "ENTITYFX_AMBIENT_END" or  intTokens[1] == "ENTITYFX_COLLISION_END" or intTokens[1] == "FRAGMENTFX_BREAK_END" or intTokens[1] == "FRAGMENTFX_DESTROY_END" or intTokens[1] == "FRAGMENTFX_ANIM_END" then (

					newType = #none
				) else  if intTokens.count > 1 then (

					newItem = RsCollParticle intTokens[1] newType
					append RsCollParts newItem
				)
			)
		)
		
		close intFile
	)

	--------------------------------------------------------------
	-- 	Check If File Exists
	--------------------------------------------------------------		
	
	fn existFile fname = (getfiles fname).count != 0
	
	--------------------------------------------------------------
	-- 
	-------------------------------------------------------------
	fn GetRoot node = (
		
		if node.parent != undefined then(
		GetRoot node.parent
		)
		else	(
			node=node
		)
	)
	--------------------------------------------------------------
	-- 
	-------------------------------------------------------------
	fn selectObjhelpers node = (
		
		if classof(node) == RAGE_Particle then
		(
			append selst node

		)
		for child in node.children do (selectObjhelpers child)
		

	)
	--------------------------------------------------------------
	-- 
	-------------------------------------------------------------

	fn getrootlist = (
		local FragList = #()
		for i in selection do
		(
			root = GetRoot I
			if superclassof(root) == GeometryClass then
			(
			appendifunique FragList root.name
			)
		)
			
		return FragList 

	)
	
	------------------------------------------------------------
	-- 
	-------------------------------------------------------------
	fn Readfile file =
		(
		local fstream = openFile file mode:"r"
		local temparray = #()
		while eof fstream == false do
			(
			line = readline fstream
			append temparray line
			)
			close fstream
			
		return temparray	
		)
	
	fn Writefile file data = 
		(
		local fstream = openFile file mode:"w"
		for line in data do
			(
				try(format "%\n" line to: fstream)catch(messagebox "Error writing to File: please revert \n"+file as string)
			)
		close fstream
		
		)
	
	
	fn UpdateFile file FragList= 
	(
		data = Readfile file
		NewData = #()
		local keymatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\t+)(disappearsWhenDead)")
		local rootmatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\t+)(group\s+\w.+{)")
		local root = false
			
		for line in data do
		(
			
			local DSTmatch = keymatch.Match line
			if (DSTmatch.groups.item[2].value == "disappearsWhenDead") then
			(
				Rm_DST = true
			)
			else
			(
				append NewData line
			)

			AddedtoBlock = false
			for frag in FragList do
			(
				local regEx = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\s+)group\s+("+frag[1]+")")
				
				local blockmatch = regEx.Match line
				--print 
				if (blockmatch.groups.item[2].value == frag[1]) then 
					(	

						if frag[2] == 1 and AddedtoBlock == false then
							(
							AddedtoBlock = true	
							Print "Adding disappearsWhenDead to file"
							append NewData (blockmatch.groups.item[1].value +"\tdisappearsWhenDead")
							)
						
						Rm_DST = false
					)
			)
		)
		Writefile file NewData

		
	)
	------------------------------------------------------------
	-- 
	-------------------------------------------------------------
	
	------------------------------------------------------------------------------------------------------------------------
	-- Below Fn will get the root node and find all the children then chk the particles that are attached and add to the array
	------------------------------------------------------------------------------------------------------------------------
	
	fn collectFrags root = 
	(
		selst = #()
		selectObjhelpers root 	-- get all the particle children and add to the selst array
		local keymatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\w+)(_frag_)")
		---------------------------------------------------------------------------
		--
		-----------------------------------------------
		fragmentList = #()
		
		for ptype in selst do			-- get all the parent objects from the children particle effects
			(
				DST = 0

				if getattr ptype 2 == 4 then (DST = 1)	-- check if the particle is a destroy type
				FgType = #(ptype.parent.name,DST)
				
				if fragmentList.count > 0 then
				(
					
					
					
					
					
					for i in fragmentList do
					(
						if i[1] == FgType[1] then
						(
							-- Found a dup
							--print ("Index is set to:"+ FgType[2] as string)
							if FgType[2] == 1 then
							(
								-- lets change to 1 as there is a particel that is destyable attached to the parent
								i[2] = 1
							)
						)
						else
						(
							local Fragmatch = keymatch.Match FgType[1]
							if Fragmatch.success then
							(
								FgType[1] = Fragmatch.groups.item[1].value
							)

							appendifunique fragmentList FgType
						)
					)
				)
				else
				(

					
					appendifunique fragmentList FgType
				)
			)
		return fragmentList
	)
	
	------------------------------------------------------------
	-- 
	-------------------------------------------------------------
	
	fn UpdateFragFile = (
		
		Print "Updating Tune File....."

		local PropNames = getrootlist()
		--print PropNames
		local keymatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\w+)(_frag_)")
		--local frags
		Print ("PropNames to Process: "+PropNames as string)

		for i in PropNames do
		(	
			--------------------------------------------------------------------------------------------------
			-- Added this to check for objs that have the _frag_ as the root name
			--------------------------------------------------------------------------------------------------
			
			local Fragmatch = keymatch.Match i
			if Fragmatch.success then
			(
				Fname = Fragmatch.groups.item[1].value
			)
			else
			(
				Fname = i
			)
			print ("faname = "+Fname as string)
			
			--------------------------------------------------------------------------------------------------
			--Fname = i
			--------------------------------------------------------------------------------------------------
			
			local file= (project.common +"\\data\\fragments\\"+Fname+".tune")
			
			--fileList = (p4.path()+"\\projects\\"+project.name+ "\\build\\dev\\common\\data\\fragments\\"+Fname+".tune")
			
			-- Need to sort out the working path
			
			
			--((systemTools.getEnvVariable "PAYNE_DIR")
			
			if existFile file then
			(
				if (getFileAttribute file #readonly) == true then
				(
					local p4 = RsPerforce()
					
					if (p4.connected()) then
					(
						local depotFiles = #()
						p4.connect (project.independent)
						print ("filename = "+Fname as string)			
						
						print ("Atempting to Update :" + file)							
						depotFiles = p4.local2depot file							
						
						p4.sync depotFiles 
						p4.edit depotFiles
						p4.disconnect()
					)
					else
					(
						messageBox ("cannot check out: "+file as string +" Making file writable.") title:"Warning..."
						SetFileAttribute file #readonly false
					)
				)
				UpdateFile file (collectFrags (getnodebyname i))
			)
			else
			(
				Print "File Does not Exist, Please Generate a Tune File!"
			)
			
		)

		
	)
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn SetType = (
		inc = 0
		for item in selection do (
			
			if getattrclass item == "RAGE Particle" then (
				inc +=1
				
				-----------------------------------------------------------------
				--Disbaled as this applys the effec name  to ALL effects
				--setattr item 4 attachAll.checked
				
				
				-----------------------------------------------------------------
				
				for i = 1 to RsCollParts.count do (
				
					if RsCollParts[i].name == cboParticle.items[cboParticle.selection] then (
					
						setattr item RsIdxPartName RsCollParts[i].name

						
						setval = 0
						
						if RsCollParts[i].type == #ambient then (
						
							setval = 0
						) else if RsCollParts[i].type == #collision then (
						
							setval = 1
						) else if RsCollParts[i].type == #shot then (
						
							setval = 2
						) else if RsCollParts[i].type == #break then (
						
							setval = 3
						) else if RsCollParts[i].type == #destruction then (
						
							setval = 4
							
						) else if RsCollParts[i].type == #anim then (
						
							setval = 5
						)						
						
						setattr item RsIdxTriggerName setval
						
						
						
						-- Mark item as dirty so representation is rebuild
						 rageparticle_fxrefresh item
						--Plist = (item.parent).children
						seed (random 1 6555)
						ID = (random 1000 9999)
						
						item.name = (RsCollParts[i].name+"_"+ID as string)
					)
					
				
					
				)
				
			)
		)

		-------------------------------------------------------------------
		-- Main Call to update tune file --
		-- Calll the update Fragment tune files --
		-------------------------------------------------------------------
		if chkTuneData.checked then UpdateFragFile()
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	
	
	fn UpdateTypes = (
	
	
	
		RsCollCurrPartNames = #()
		
		for item in RsCollParts do (
				
			show = false

			if chkAmbient.checked and item.type == #ambient then show = true
			if chkBreak.checked and item.type == #break then show = true
			if chkShot.checked and item.type == #shot then show = true
			if chkCollision.checked and item.type == #collision then show = true
			if chkDestruction.checked and item.type == #destruction then show = true
			if chkAnim.checked and item.type == #anim then show = true

			if show then (

				append RsCollCurrPartNames item.name
			)
		)
	
		sort RsCollCurrPartNames
	
		cboParticle.items = RsCollCurrPartNames
	)


	fn checkparticle_Names = (
		local keymatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(Pt_(\w+_)[0-9]+)")
		local keyIDmatch= dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("((\w+_)[0-9]+)")
		seed (random 1 6555)
		for i in helpers do
		(
				if classof(i) == RAGE_Particle then
					(
					local PTmatch = keymatch.Match i.name
					if PTmatch.groups.item[1].success then
						(
							ID = (random 1000 9999)
							--print (PTmatch.groups.item[2].value+ID as string)
							i.name = (PTmatch.groups.item[2].value+ID as string)
						)
						else
						(
							local PTmatchID = keyIDmatch.Match i.name
							if PTmatchID.groups.item[1].success then
								(
									print "match"
									ID = (random 1000 9999)
									--print (PTmatch.groups.item[2].value+ID as string)
									i.name = (PTmatchID.groups.item[2].value+ID as string)
								)
			
						)	
					)
		)
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	on btnID pressed do
	(
		checkparticle_Names()
	)
	
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkAmbient changed arg do (
	
		UpdateTypes()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkBreak changed arg do (
	
		UpdateTypes()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkCollision changed arg do (
	
		UpdateTypes()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkShot changed arg do (
	
		UpdateTypes()
	)	
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkDestruction changed arg do (
	
		UpdateTypes()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnSet pressed do (
	
		SetType()
		AddHelper.Updatelistbox()
	)
	
	--------------------------------------------------------------
	-- Clone
	--------------------------------------------------------------	
	on btnClone pressed do (
	
		if selection.count > 0 then (
		
			if ( "RAGE Particle" == GetAttrClass selection[1] ) then
			(
				nodeToCopy = selection[1]
				CopyAttrs()
				
				nodeCopy = copy nodeToCopy
			
				if ( undefined == nodeCopy ) then
				(
					MessageBox "Error cloning RAGE_Particle node.  Contact tools."
					return false
				)

				clearSelection()
				selectMore nodeCopy
				PasteAttrs()
			)
		)
	)
	
	--------------------------------------------------------------
	-- Rollup Startup
	--------------------------------------------------------------
	on RsPartSetupRoll open do (
	
		LoadEntries()		
		UpdateLocalConfigFileLoad()	
		UpdateTypes()
	)

	on RsPartSetupRoll close do
	(
		UpdateLocalConfigFileSave()
	)
	
)







try 
(
	cui.UnRegisterDialogBar RsPartSetupUtil 
	CloseRolloutFloater RsPartSetupUtil 
	
)
catch()
RsPartSetupUtil = newRolloutFloater ("Rockstar Particle Helper" + revisionN()) 250 900 50 126
cui.RegisterDialogBar RsPartSetupUtil --style:#(#cui_dock_left,#cui_floatable,#cui_handles)
--cui.DockDialogBar RsPartSetupUtil #cui_dock_left

addRollout AddHelper RsPartSetupUtil rolledup:false
addRollout RsPartSetupRoll RsPartSetupUtil rolledup:false
addRollout AddTint RsPartSetupUtil rolledup:false
addRollout ListHelpers RsPartSetupUtil rolledup:false

