--Global MainFloater
--filein "startup\\RSVINI.ms"

try 
(
	cui.unRegisterDialogBar AmbientLighting
	destroyDialog AmbientLighting
) 
catch()

fn addLight =
(
	if $sky01 != undefined then
	(
		print "There's already a sky in the scene."
	)
	else
	(
		Ies_Sky hasTarget:off targetDistance:150 pos:[0,0,30]
		$sky01.color = [255,255,255]
		hide $sky01
	)
)

fn calculateLighting =
(
	addlight()

	if sceneRadiosity.Radiosity.doesSolutionExist() == true do
	(
		sceneRadiosity.Radiosity.Reset true false
	)

	if sceneRadiosity.Radiosity.doesSolutionExist() == false do
	(
		sceneRadiosity.Radiosity.radGlobalRefineSteps = 3
		sceneRadiosity.Radiosity.radFiltering = 1
		sceneRadiosity.Radiosity.radDirectFiltering = 1
		sceneRadiosity.Radiosity.meshingEnabled = on
		sceneRadiosity.Radiosity.minimumMeshSize = 0.3
		sceneRadiosity.Radiosity.includeSkylight = on
		
		SceneExposureControl.exposureControl = Logarithmic_Exposure_Control()
		
		SceneExposureControl.exposureControl.brightness = 75
		SceneExposureControl.exposureControl.contrast = 100
		SceneExposureControl.exposureControl.midTones = 1.5
		SceneExposureControl.exposureControl.exteriorDaylight = on
		
		sceneRadiosity.radiosity.start()
	)	
)

fn assignLight =
(
    objs = #()
    objs = (selection as array)
 
    for obj in objs do
    (
	    if obj != undefined then
	    (
		    if sceneRadiosity.Radiosity.doesSolutionExist() == true then
		    (
			    ambientColor = color 0 0 0
			    obj.showVertexColors = on
			    obj.vertexColorsShaded = off   		 				

--			    sceneRadiosity.Radiosity.Reset true false
		    )
		    else
		    (
			    Messagebox("You need to Calcuate Lighting first.")
		    )
	    )
	    else
	    (
		    Messagebox("Nothing is selected.")
	    )
     ) 


     modPanel.addModToSelection (VertexPaint()) ui:on
				
	 for obj in objs do
     (
	    if obj != undefined then
		(
	       obj.modifiers[#VertexPaint].radiosityOption = 1
	       obj.modifiers[#VertexPaint].colorBy = 0
 	       obj.modifiers[#VertexPaint].lightingModel = 0
		 )
     ) 

)

fn sceneCleanup =
(
	if sceneRadiosity.Radiosity.doesSolutionExist() == true do
	(
		sceneRadiosity.Radiosity.Reset true false
	)

	if $sky01 != undefined do
	(
		delete $sky01
	)

	if $ != undefined then
	(
		convertToMesh $
	)
	else
	(
		messagebox "Please select an object."
	)
	
	useEnvironmentMap = off
	environmentMap = undefined
)

rollout AmbientLighting "Ambient Lighting"-- width:150 height:120
(
	button calculateLight_b "Calculate Lighting"
	button assignLights_b "Assign Lighting"
	button sceneCleanUp_b "Scene Cleanup"

	on calculateLight_b pressed do calculateLighting()	
	on assignLights_b pressed do assignLight()
	on sceneCleanUp_b pressed do sceneCleanup()
)

theNewFloater = newRolloutFloater "Ambient Lighting" 150 120

-- Add the rollout section
addRollout AmbientLighting theNewFloater