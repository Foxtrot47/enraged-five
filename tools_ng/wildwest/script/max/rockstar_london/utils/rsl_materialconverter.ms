
-- ******************** struct setup ********************
struct RS_maxMaterial (material, materialName, diffuseMap, specularSettings = #(), emissive, opacityMap, \
								normalMap, reflectionMap, refractionMap, rsShader = "undefined", objectList = #(#(), #()))
-- specularSettings = #(specularColour, specularMap, specularLevel, Glossiness)
-- emissive = self-Illumination

struct RS_RageShader (shaderName, variables = #(#(), #()))
-- variables = #(#(),#()) the first array contains the variable names and the second array contains the variable types

try
(
	destroyDialog RS_materialConverter
)
catch ()

-- ******************** Options menu ********************
rcmenu RS_materialConverter_OptionsMenu
(
	subMenu "Options"
	(
		subMenu "Process"
		(
			menuItem processSelected "Selected Objects" enabled:true
			menuItem processScene "Scene" enabled:true
		)
		
		subMenu "Maps"
		(
			menuItem normal "Normal Maps" enabled:true
			menuItem specular "Specular Maps" enabled:true
			menuItem reflect "Reflection Maps" enabled:true
		)
		
	)
	
	on processSelected picked do
	(
		processScene.checked = false
		processSelected.checked = true
	)
	on processScene picked do
	(
		processSelected.checked = false
		processScene.checked = true
	)
	
	on normal picked do
	(
		if normal.checked then normal.checked = false else normal.checked = true
	)
	on specular picked do
	(
		if specular.checked then specular.checked = false else specular.checked = true
	)
	on reflect picked do
	(
		if reflect.checked then reflect.checked = false else reflect.checked = true
	)
)


-- MAIN ROLLOUT
rollout RS_materialConverter "R* Material Converter" width:528 height:448
(
	local shaderArray = undefined
	local materialArray = undefined
-- 	local defaultEnvironmentMap = "W:\\payne\\payne\\Art\\Textures\\EnvironmentMaps\\Default_CUBE_ENV.dds"
	local objectsToProcess = undefined
	
	

	-- ******************** functions ********************
	
	fn generateShaderListArray = -- writes out an INI file containg the name and variables of each shader, outputs an array of structs containing the names and variables of each shader
	( -- INI OUTPUT IS CURRENTLY COMMENTED OUT
		local shaderINI = "X:\\rageShaders.ini"
		
		local shaderFilePaths = getFiles (project.shaders + "\\db\\*.sps")
		local rageShader = Rage_Shader ()
		
		local outArr = #()
		
	-- 	try (deleteFile shaderINI) catch ()
	-- 	local newINI = createFile shaderINI
		
		local currentCount = 0
		RS_materialConverter.lbl_progressBar.text = "Processing: Shader List"
		
		for shader in shaderFilePaths do
		(
			RS_materialConverter.pb_process.value = (100.0 * currentCount / shaderFilePaths.count)
			
			local shaderName = getFilenameFile shader
			RstSetShaderName rageShader (filenameFromPath shader)
			local shaderVariableCount = RstGetVariableCount rageShader
			
			local shaderStruct = RS_RageShader shaderName:(getFilenameFile shader)
			
			for v = 1 to shaderVariableCount do
			(
	-- 			setINISetting shaderINI shaderName ((RstGetVariableName rageShader v) as string) ((RstGetVariableType  rageShader v) as string)
				append shaderStruct.variables[1] ((RstGetVariableName rageShader v) as string)
				append shaderStruct.variables[2] ((RstGetVariableType  rageShader v) as string)
			)
			append outArr shaderStruct 
			currentCount += 1
		)
		RS_materialConverter.lbl_progressBar.text = "Processing: Finished"
		RS_materialConverter.pb_process.value = 0.0
		gc ()
	-- 	close newINI
		outArr
	)

	fn getFilename bitmap = -- outputs either the filename and path of the bitmap or an "undefined" string if the bitmap is undefined or has not got a texture assigned to it
	(
		local out = "undefined"
		try
		(
			if bitmap != undefined and bitmap.filename != "" then out = bitmap.filename
		)
		catch ()
		out
	)
	
	fn findMaterialInStructArray material structArray =
	(
		local outIndex = 0
		for i = 1 to structArray.count do
		(
			if material == structArray[i].material then
			(
				outIndex = i
			)
		)
		outIndex
	)
	
	fn generateMaterialArray objectArray = -- generates an array of structures, based each unique material in the scene
	(
		local outStructureArray = #()
		local processedMaterials = #()

		local currentCount = 0
		
		RS_materialConverter.lbl_progressBar.text = "Processing: Material Array"
		
		for object in objectArray where (classOf object) != XRefObject and (classOf object) != Col_Mesh do
		(
			local material = object.material
			
			RS_materialConverter.pb_process.value = (100.0 * currentCount / sceneMaterials.count)
			
			case (classOf material) of
			(
				Standardmaterial:
				(
					local checkProcessed = findItem processedMaterials material
					checkProcessed = 0
					
					if checkProcessed == 0 then
					(
						local specularFilename
						
						if (classOf material.specularLevelMap) == bitmapTexture and material.specularLevelMap.filename != undefined then
						(
							specularFilename = material.specularLevelMap.filename
						)
						else
						(
							specularFilename = getFilename material.specularMap
						)
						
						specSetting = material.specularColor as string + ", \"" + specularFilename + "\", " + \
						material.specularLevel as string + ", " + material.glossiness as string
						
						outStructureArray += #(RS_maxMaterial material:material \
							materialName:material.name \
							diffuseMap:(getFilename material.diffuseMap) \
							specularSettings:specSetting \
							emissive:(getFilename material.selfillumMap) \
							opacityMap:(getFilename material.opacityMap) \
							normalMap:(getFilename material.bumpMap) \
							reflectionMap:(getFilename material.reflectionMap) \
							refractionMap:(getFilename material.refractionMap) \
							)
						append outStructureArray[outStructureArray.count].objectList[1] object
						append outStructureArray[outStructureArray.count].objectList[2] undefined
							
						append processedMaterials material
					)
					else
					(
						local materialIndex = findMaterialInStructArray material outStructureArray
						append outStructureArray[materialIndex].objectList object
					)
				)
				Multimaterial:
				(
					local subMaterialCount = 0
					for subMaterial in material do
					(
						subMaterialCount += 1
						
						local checkProcessed = findItem processedMaterials subMaterial
						checkProcessed = 0
						
						if (classOf subMaterial) == Standardmaterial do
						(
							if checkProcessed == 0 then
							(
								local specularFilename
								
								if (classOf subMaterial.specularLevelMap) == bitmapTexture and subMaterial.specularLevelMap.filename != undefined then
								(
									specularFilename = subMaterial.specularLevelMap.filename
								)
								else
								(
									specularFilename = getFilename subMaterial.specularMap
								)
								
								specSetting = subMaterial.specularColor as string +", \"" + specularFilename + "\", " + \
								subMaterial.specularLevel as string + ", " + subMaterial.glossiness as string
								
								outStructureArray += #(RS_maxMaterial material:subMaterial \
									materialName:subMaterial.name \
									diffuseMap:(getFilename subMaterial.diffuseMap) \
									specularSettings:specSetting \
									emissive:(getFilename subMaterial.selfillumMap) \
									opacityMap:(getFilename subMaterial.opacityMap) \
									normalMap:(getFilename subMaterial.bumpMap) \
									reflectionMap:(getFilename subMaterial.reflectionMap) \
									refractionMap:(getFilename subMaterial.refractionMap) \
									)
								append outStructureArray[outStructureArray.count].objectList[1] object
								append outStructureArray[outStructureArray.count].objectList[2] material.materialIDList[subMaterialCount]
								
								append processedMaterials subMaterial
							)
							else
							(
								local materialIndex = findMaterialInStructArray subMaterial outStructureArray
								append outStructureArray[materialIndex].objectList[1] object
								append outStructureArray[outStructureArray.count].objectList[2] subMaterialCount
							)
						)
					)
				)
				default: (print "neither a standard nor multiMaterial")
			)
			currentCount += 1
		)
		RS_materialConverter.lbl_progressBar.text = "Processing: Finished"
		RS_materialConverter.pb_process.value = 0.0
		gc ()
		outStructureArray
	)

	fn createMaterialListINI materialStructArray = -- generates an INI file containing information about each material in the scene that is a standard max material 
	(
		local materialListINI = maxFilePath + (getFilenameFile maxFileName) + "_MATERIALS" + ".ini"
		
		try (deleteFile materialListINI) catch ()
		
		local newINI = createFile materialListINI
		local currentCount = 0
		
		RS_materialConverter.lbl_progressBar.text = "Processing: Material INI"
		
		for materialStruct in materialStructArray do
		(
			RS_materialConverter.pb_process.value = (100.0 * currentCount / materialStructArray.count)
			setINISetting materialListINI materialStruct.materialName "DiffuseMap" materialStruct.diffuseMap as string
			setINISetting materialListINI materialStruct.materialName "SpecularSettings" materialStruct.specularSettings as string
			setINISetting materialListINI materialStruct.materialName "EmissiveMap" materialStruct.emissive as string
			setINISetting materialListINI materialStruct.materialName "OpacityMap" materialStruct.opacityMap as string
			setINISetting materialListINI materialStruct.materialName "NormalMap" materialStruct.normalMap as string
			setINISetting materialListINI materialStruct.materialName "ReflectionMap" materialStruct.reflectionMap as string
	-- 		setINISetting materialListINI materialStruct.object "RefractionMap" materialStruct.refractionMap as string
			currentCount += 1
		)
		RS_materialConverter.lbl_progressBar.text = "Processing: Finished"
		RS_materialConverter.pb_process.value = 0.0
		close newINI
	)

	fn pickRageShader materialStructs shaderList allowed =
	(
		local currentCount = 0
		
		RS_materialConverter.lbl_progressBar.text = "Processing: Picking Rage Shader"
		
		for material in materialStructs do
		( -- (shaderName, variables = #(#(), #()))
			RS_materialConverter.pb_process.value = (100.0 * currentCount / materialStructs.count)
			
			local rsShader = shaderList[11]
			local typeArray = #()
			local specSettings = (filterString (filterString material.specularSettings ",")[2] " \"")[1]
			
			local lookForDecal = (matchPattern material.materialName pattern:"*decal*")
			
	-- 		if lookForDecal do typeArray += #("decal")
			
			if material.normalMap != "undefined" and allowed[1] == true do typeArray += #("normal")
			if specSettings != "undefined" and allowed[2] == true do typeArray += #("spec")
			if material.emissive != "undefined" do typeArray += #("emissive")
			if material.opacityMap != "undefined" do typeArray += #("alpha")
			if material.reflectionMap != "undefined" and allowed[3] == true do typeArray += #("reflect")
			
	-- 		if material.refractionMap != "undefined" do typeArray += #("refract")
			
			for shader in shaderList do
			(
				local hitCount = 0
				
				local shaderAttributes = filterString shader.shaderName "_"
				
				if shaderAttributes.count == typeArray.count do
				(
					for type in typeArray do
					(
						if (findItem shaderAttributes type) != 0 then
						(
							hitCount += 1
						)
					)
					if hitCount == typeArray.count then
					(
						rsShader = shader
					)
				)
			)
			material.rsShader = rsShader
			currentCount += 1
		)
		RS_materialConverter.lbl_progressBar.text = "Processing: Finished"
		RS_materialConverter.pb_process.value = 0.0
	)

	fn findSelectionNumberByName structArray name =
	(
		local outNumber = 0
		for i = 1 to structArray.count do
		(
			if structArray[i].shaderName == name then outNumber = i
		)
		outNumber
	)

	fn collectListBoxItems materialArray =
	(
		outItems = #()
		for material in materialArray do
		(
			local nameString = material.materialName
			local spaceString = ""
			local stringGap = (50 - nameString.count)
			if stringGap > 0 do for i = 1 to stringGap do spaceString += " "
			append outItems (nameString + spaceString + "    -    Rage Shader: " + material.rsShader.shaderName)
		)
		outItems
	)
	
	fn convertSpecularValues fallOff intensity =
	(
		format "falloff:% intensity:%\n" fallOff intensity
		local outPoint2 = [0,0]
		outPoint2.x = (200.0 / 100) * fallOff
		outPoint2.y = intensity / 100.0 
		outPoint2
	)
	
	fn assignShaderToObjectList shader objectList material =
	(
		for i = 1 to objectList[1].count do
		(
			local subMaterialID = objectList[2][i]
			
			if subMaterialID == undefined then
			(
				objectList[1][i].material = shader
			)
			else
			(
				if (classOf objectList[1][i].material) != Rage_Shader then
				(
					objectList[1][i].material.materialList[subMaterialID] = shader
				)
			)
		)
		gc ()
	)
	
	fn showTheDamnTextures objectArray =
	(
		for object in objectArray do
		(
			local material = object.material
			case (classOf material) of
			(
				Standardmaterial:
				(
					material.showInViewport = true
				)
				Multimaterial:
				(
					for subMaterial in material.materialList do
					(
						subMaterial.showInViewport = true
					)
				)
			)
		)
	)
	
	fn convertToRageShader material =
	(
		local shaderVariables = material.rsShader.variables[1]
		local shaderVariableTypes = material.rsShader.variables[2]
		
		local specularArray = (filterString material.specularSettings ", ()\"")
		local rageShader = Rage_Shader ()
		
		rageShader.name = material.materialName
		
		RstSetShaderName rageShader material.rsShader.shaderName
		setMTLMEditFlags rageShader (getMTLMeditFlags material.material)
		
		if material.material.twoSided do
		(
-- 			format "% is a two sided material, currently there is no support to set this in the rage shader\n" material.materialName to:listener
-- 			rageShader.twoSided = true
		)
		
		local isAlphaShader = RstGetIsAlphaShader rageShader
		local textureCount = 0
		
		for i = 1 to RstGetVariableCount rageShader where (RstGetVariableType rageShader i) == "texmap" do textureCount += 1
		
		for i = 1 to shaderVariables.count do
		(
			try
			(
				if shaderVariables[i] == "Diffuse Texture" do
				(
					RstSetVariable rageShader i material.diffuseMap
					if isAlphaShader == true do
					(
						local alphaBitMapTexture = bitmaptexture filename:material.opacityMap
						setSubTexmap rageShader (textureCount + i) alphaBitMapTexture
					)
				)
			)
			catch ()
			
			try (if shaderVariables[i] == "Bump Texture" do RstSetVariable rageShader i material.normalMap)
			catch ()
			
			try (if shaderVariables[i] == "Specular Texture" do RstSetVariable rageShader i specularArray[5])
			catch ()
			
			if shaderVariables[i] == "Environment Texture" do
			(
				if material.reflectionMap != "undefined" then
				(
					RstSetVariable rageShader i material.reflectionMap
				)
				else
				(
-- 					if (doesFileExist defaultEnvironmentMap) == true and RS_materialConverter_OptionsMenu.reflect.checked == true then RstSetVariable rageShader i defaultEnvironmentMap
				)
			)
			
			try
			(
				local specularValues = convertSpecularValues (specularArray[6] as float) (specularArray[7] as float)
				
				if shaderVariables[i] == "Specular Falloff" do RstSetVariable rageShader i specularValues.x
				if shaderVariables[i] == "Specular Intensity" do RstSetVariable rageShader i specularValues.y
				
				if shaderVariables[i] == "specular map intensity mask color" do
				(
					local specularColor = [specularArray[2] as number, specularArray[3] as number, specularArray[7] as number] / 256
					RstSetVariable rageShader i specularColor
				)
			)
			catch ()
		)
		assignShaderToObjectList rageShader material.objectList material.material
	)
	
	fn checkObjectCount array selectedObjects = -- only used to check that the material structures created actually do contain all the legal objects in the scene
	(
		local found = #()
		local notFound = #()
		local collected = #()
		
		for obj in array do
		(
			for o in obj.objectList do
			(
				if (findItem collected o) == 0 then append collected o
			)
		)
		for obj in selectedObjects do
		(
			if (findItem collected obj) == 0 then
			(
				append notFound obj
			)
			else
			(
				append found obj
			)
		)
		#(found, notFound)
	)
	
	fn generateAndSortFaceMaterialIDArray object =
	(
		local outArray = #(#(), #())
		
		for face in object.faces do
		(
			case (classOf object) of
			(
				Editable_mesh:
				(
					local materialID = (getFaceMatID object face.index)
					local IDIndex = findItem outArray[1] materialID
					if IDIndex == 0 then
					(
						append outArray[1] materialID
						append outArray[2] #(face.index)
					)
					else
					(
						append outArray[2][IDIndex] face.index
					)
				)
				Editable_Poly:
				(
					local materialID = (polyOp.getFaceMatID object face.index)
					local IDIndex = findItem outArray[1] materialID
					if IDIndex == 0 then
					(
						append outArray[1] materialID
						append outArray[2] #(face.index)
					)
					else
					(
						append outArray[2][IDIndex] face.index
					)
				)
			)
		)
		
		local arrayCount = outArray[1].count
		local sorted = false
		
		while not sorted do
		(
			sorted = true
			
			for i = 1 to arrayCount do
			(
				if i < arrayCount do
				(
					if outArray[1][i] > outArray[1][i+1] then
					(
						local tempID = outArray[1][i]
						
						outArray[1][i] = outArray[1][i+1]
						outArray[1][i+1] = tempID
						
						local tempFaceArray = outArray[2][i]
						
						outArray[2][i] = outArray[2][i+1]
						outArray[2][i+1] = tempFaceArray
						sorted = false
					)
				)
			)
		)
		
		local subMaterialCount = object.material.numSubs
		local fixIndexArray = #(#(), #())
		
		for i = 1 to outArray[1].count do
		(
			if outArray[1][i] > subMaterialCount then
			(
				local convertToRangeID = (mod outArray[1][i] subMaterialCount) as integer
				if convertToRangeID == 0.0 then convertToRangeID = subMaterialCount
				
				local tempFaceArray = outArray[2][i]
				
				local faceArrayIndex = findItem outArray[1] convertToRangeID
				
				if faceArrayIndex != 0 then
				(
					fixIndexArray[2][faceArrayIndex] += tempFaceArray
				)
				else
				(
					append fixIndexArray[1] convertToRangeID
					append fixIndexArray[2] tempFaceArray
				)
			)
			else
			(
				append fixIndexArray[1] outArray[1][i]
				append fixIndexArray[2] outArray[2][i]
			)
		)
		outArray = fixIndexArray
		
		outArray
	)

	fn optimiseMultiMaterial object =
	(
		local material = object.material
		if (classOf material) == Multimaterial do
		(
			local newMultiMaterial = Multimaterial numSubs:1
			newMultiMaterial.name = object.name
			
			local materiaList = material.materialList
			local materialIDList = material.materialIDList
			
			local faceMaterialIDList = generateAndSortFaceMaterialIDArray object
			
			for i = 1 to faceMaterialIDList[1].count do
			(
				if i > 1 do newMultiMaterial.numSubs += 1
				
				local currentID = faceMaterialIDList[1][i]
				
				local indexFromID = findItem materialIDList currentID
				if indexFromID == 0 then print "bugger"
				
				newMultiMaterial.materialList[i] = materiaList[indexFromID]
				newMultiMaterial.names[i] = material.names[indexFromID]
				
				case (classOf object) of
				(
					Editable_mesh:
					(
						for faceIndex in faceMaterialIDList[2][i] do setFaceMatID object faceIndex i
					)
					Editable_Poly:
					(
						for faceIndex in faceMaterialIDList[2][i] do polyOp.setFaceMatID object faceIndex i
					)
				)
			)
			object.material = newMultiMaterial
		)
	)	
	
	fn convertNormalBumpToBitmapTexture objectArray =
	(
		local outError = #()
		outError[1] = #()
		outError[2] = #()
		
		local currentCount = 0
		
		RS_materialConverter.lbl_progressBar.text = "Processing: Fixing Normal Bumps"
		
		for object in objectArray do
		(
			local material = object.material

			RS_materialConverter.pb_process.value = (100.0 * currentCount / sceneMaterials.count)
			case (classOf material) of
			(
				Standardmaterial:
				(
					local bumpMapSlot = material.bumpMap
					local bumpMapSlotClass = classOf bumpMapSlot
					
					if bumpMapSlotClass == Normal_Bump then
					(
						
						local bumpMapSlotFilename = bumpMapSlot.normal_map.filename
						
						if bumpMapSlotFilename != "" then
						(
							local newBumpMap = bitmaptexture filename:bumpMapSlotFilename
							newBumpMap.name = (getFilenameFile bumpMapSlotFilename)
							
							material.bumpMap = newBumpMap
						)
					)
					else
					(
						
					)
					gc ()
				)
				Multimaterial:
				(
					for subMaterial in material where (classOf subMaterial) == Standardmaterial do
					(
						local bumpMapSlot = subMaterial.bumpMap
						local bumpMapSlotClass = classOf bumpMapSlot
						
						if bumpMapSlotClass == Normal_Bump then
						(
							local bumpMapSlotFilename = bumpMapSlot.normal_map.filename
							
							if bumpMapSlotFilename != "" then
							(
								local newBumpMap = bitmaptexture filename:bumpMapSlotFilename
								newBumpMap.name = (getFilenameFile bumpMapSlotFilename)
								
								subMaterial.bumpMap = newBumpMap
							)
						)
						else
						(
							
						)
					)
					gc ()
				)
				default:
				(
					append outError[1] material
					append outError[2] (classOf material)
				)
			)
			currentCount += 1
		)
		RS_materialConverter.lbl_progressBar.text = "Processing: Finished"
		RS_materialConverter.pb_process.value = 0.0
		gc ()
		outError
	)
	
	
	-- ******************** Rollout controls ********************
	listbox lbx_materialList "Materials" pos:[8,40] width:512 height:19
	button btn_preProcess "Pre-Process Geometry" pos:[8,8] width:512 height:24
	dropdownList ddl_shaderList "Shader List" pos:[8,320] width:512 height:40
	button btn_assignAllRageShaders "Assign All Rage Shaders" pos:[264,368] width:256 height:24 enabled:false
	button btn_assignSelectedRageShader "Assign Selected Rage Shader" pos:[8,368] width:256 height:24 enabled:false
	progressBar pb_process "ProgressBar" pos:[8,424] width:512 height:16 color:(color 255 0 0) orient:#horizontal
	label lbl_progressBar "Processing:" pos:[8,400] width:512 height:16
	
	-- ******************** Event Handlers ********************
	
	
	on RS_materialConverter open do
	(
		RS_materialConverter_OptionsMenu.processSelected.checked = true
		RS_materialConverter_OptionsMenu.normal.checked = true
	)
	on lbx_materialList selected sel do
	(
		if shaderArray != undefined and materialArray != undefined then
		(
			local selectedMaterial = lbx_materialList.selection
			
			meditMaterials[1] = materialArray[selectedMaterial].material
			
			ddl_shaderList.selection = findSelectionNumberByName shaderArray materialArray[selectedMaterial].rsShader.shaderName
			
			select materialArray[selectedMaterial].objectList[1]
		)
	)
	on btn_preProcess pressed do
	(
	-- 		include "X:\\Tools\\dcc\\current\\max2009\\wip_scripts\\convertNormalBumpToBitmapTexture.ms"
	-- 		include "X:\\Tools\\dcc\\current\\max2009\\wip_scripts\\OptimiseMultiMaterial.ms"
		
		for object in $objects where (classOf object) == XRefObject or (superClassOf object) == helper or (classOf object) == GtaBlock do object.isHidden = true
		for object in $objects where (superClassOf object) != GeometryClass do object.isHidden = true
		
		local currentCount = 0
		
		if RS_materialConverter_OptionsMenu.processSelected.checked == true then
		(
			objectsToProcess = for object in selection where (classOf object) != XRefObject and (getAttrClass object) == "Gta Object" collect object
			selection = objectsToProcess
		)
		else
		(
			objectsToProcess = for object in $geometry where (classOf object) != XRefObject and (getAttrClass object) == "Gta Object"  collect object
		)
		gc ()
		
-- 		lbl_progressBar.text = "Processing: Optimising Multi Materials"
-- 		
-- 		for object in objectsToProcess do
-- 		(
-- 			pb_process.value = (100.0 * currentCount / objectsToProcess.count)
-- 			
-- 			optimiseMultiMaterial object
-- 			
-- 			currentCount += 1
-- 		)
		lbl_progressBar.text = "Processing: Finished"
		pb_process.value = 0
		
		convertNormalBumpToBitmapTexture objectsToProcess
		
		shaderArray = generateShaderListArray ()
		materialArray = generateMaterialArray objectsToProcess
		
		if materialArray.count > 0 then
		(
			local allowedMaps = #(RS_materialConverter_OptionsMenu.normal.checked, RS_materialConverter_OptionsMenu.specular.checked, RS_materialConverter_OptionsMenu.reflect.checked)
			
			pickRageShader materialArray shaderArray allowedMaps
			
			-- (material, materialName, diffuseMap, specularSettings = #(), emissive, opacityMap, normalMap, reflectionMap, refractionMap, rsShader = "undefined")
			-- (shaderName, variables = #(#(), #()))
			
			local listBoxItems = (collectListBoxItems materialArray)
			
			lbx_materialList.items = listBoxItems
			
-- 			print materialArray[1].specularSettings
			
			local dropDownListItems = for shader in shaderArray collect shader.shaderName
			
			ddl_shaderList.items = dropDownListItems
			ddl_shaderList.selection = findSelectionNumberByName shaderArray materialArray[1].rsShader.shaderName
			
			btn_assignAllRageShaders.enabled = true
-- 			btn_assignSelectedRageShader.enabled = true
		)
		else
		(
			materialArray = undefined
			lbx_materialList.items = #()
			messageBox "There are no valid materials that need converting" title:"No valid Materials Found"
		)
	)
	on ddl_shaderList selected sel do
	(
		if shaderArray != undefined and materialArray != undefined then
		(
			local selectedMaterial = lbx_materialList.selection
			local shaderSelection = ddl_shaderList.selection
			local new_rsShader = shaderArray[shaderSelection]
			
			materialArray[selectedMaterial].rsShader = new_rsShader
			
			local listBoxItems = (collectListBoxItems materialArray)
			
			lbx_materialList.items = listBoxItems
		)
	)
	on btn_assignAllRageShaders pressed do
	(
		local selectedMaterial = lbx_materialList.selection
		local currentCount = 0
		
		lbl_progressBar.text = "Processing: Assigning All Rage Shaders"
		
		for material in materialArray do
		(
			pb_process.value = (100.0 * currentCount / materialArray.count)
			
			convertToRageShader material
			currentCount += 1
		)
		showTheDamnTextures objectsToProcess
		
		lbl_progressBar.text = "Processing: Finished"
		pb_process.value = 0
		
		local listBoxItems = #()
		lbx_materialList.items = listBoxItems
		
		btn_assignAllRageShaders.enabled = false
		materialArray = undefined
	)
	on btn_assignSelectedRageShader pressed do
	(
		local selectedMaterial = lbx_materialList.selection
		
		objectsToProcess = for object in materialArray[selectedMaterial].objectList collect object
		
		convertToRageShader materialArray[selectedMaterial]
		showTheDamnTextures objectsToProcess
		
		deleteItem materialArray selectedMaterial
		
		local listBoxItems = (collectListBoxItems materialArray)
		
		lbx_materialList.items = listBoxItems
		
	)
)

createDialog RS_materialConverter menu:RS_materialConverter_OptionsMenu
	
