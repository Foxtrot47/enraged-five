plugin simpleManipulator RSL_VehiclePathManip
name:"Vehicle Path"
classID:#(0x64802231, 0x3fd1da09)
category:"Manipulators"
invisible:true
(
	parameters main
	(
		filename type:#string 
		timeData type:#floatTab tabSize:0 tabSizeVariable:true animatable:false
		matrixData type:#matrix3Tab tabSize:0 tabSizeVariable:true animatable:false
		stringData type:#stringTab tabSize:0 tabSizeVariable:true animatable:false
		
		colourPoint type:#point3 default:[1.0,0.8,0]
		colourPath type:#point3 default:[0.5,0.5,1]
		colourRed type:#point3 default:[1,0,0]
	)
	
	on canManipulate target return (classOf target) == RSL_VehiclePathManip
	
	tool create
	(
		on mousePoint click do
		(
			nodeTM.translation = worldPoint
			#stop
		)
	)
	
	fn createGizmoShape tm =
	(
		local triangle = manip.makeGizmoShape()
		triangle.startNewLine()
		
		triangle.addPoint ([0.25, 0, 0] * tm)
		triangle.addPoint ([-0.25, 0, 0] * tm)
		triangle.addPoint ([0, 0.25, 0] * tm)
		triangle.addPoint ([0.25, 0, 0] * tm)
		
		triangle
	)
	
	on updateGizmos do
	(
-- 		print "updating RS_VehiclePath gizmos"
		this.clearGizmos()
		
		if (target != undefined) then
		(
			this.timeData = target.timeData
			this.matrixData = target.matrixData
			this.stringData = target.stringData
		)
		
		local vehiclePath = manip.makeGizmoShape()
		vehiclePath.startNewLine()
		
		for i = 1 to matrixData.count do
		(
			this.addGizmoMarker #dot this.matrixData[i].row4 0 colourPoint colourRed
		)
		
		for i = 1 to matrixData.count do
		(
			local triangle = createGizmoShape this.matrixData[i]
			
			this.addGizmoShape triangle gizmoDontHitTest (colourPoint * 0.75) colourRed
			
			vehiclePath.addPoint this.matrixData[i].row4
		)
		
		this.addGizmoShape vehiclePath gizmoDontHitTest colourPath colourPath
		
		return ""
	)
	
	on mouseDown mPos which do
	(
-- 		print "mouse down"
	)
	
	on mouseMove mPos i do
	(
		local projectedPoint = [0,0,0]
		local viewRay = this.getLocalViewRay mPos
		
		local pl = manip.makePlaneFromNormal z_axis matrixData[i].row4
		local result = pl.intersect viewRay &projectedPoint
		
		if result then
		(
			target.matrixData[i + 1] = (matrix3 target.matrixData[i].row1 target.matrixData[i].row2 target.matrixData[i].row3 projectedPoint)
			
			return true
		)
		
		pl = manip.makePlaneFromNormal viewRay.dir [0, 0, 0]
		projectedPoint = [0,0,0]
		local result = pl.intersect viewRay &projectedPoint
		
		if result then
		(
			target.matrixData[i + 1] = (matrix3 target.matrixData[i].row1 target.matrixData[i].row2 target.matrixData[i].row3 projectedPoint)
		)
	)
)