
try (destroyDialog RSL_multiMatCleaner) catch()

rollout RSL_multiMatCleaner "Multimaterial Cleaner"
(
	local total = 0
	local count = 0
	
	local badMeshArray = #()
	
	button btn_clean "Clean" width:150
	
	fn findID array ID =
	(
		local result = 0
		
		for i = 1 to array.count where array[i] != undefined do 
		(
			if array[i].ID == ID then
			(
				result = i
			)
		)
		
		result
	)
	
	fn comareID v1 v2 =
	(
		case of
		(
			(v1.ID > v2.ID):1
			(v1.ID < v2.ID):-1
			default:0
		)
	)
	
	fn hasValidFaces object =
	(
		local result = true
		
		for face in object.faces do 
		(
			local ID = (polyop.getFaceMatID object face.index)
			
			if ID == undefined then
			(
				result = false
			)
		)
		
		result
	)
	
	fn getIDFaceArray object subMaterialCount =
	(
		local result = #()
		
		for face in object.faces do 
		(
			local ID = (polyop.getFaceMatID object face.index)
-- 			format "Face:% ID:%\n" face.index ID
			
			local index = findID result ID
			
			if index > 0 then
			(
				append result[index].faceArray face.index
			)
			else
			(
				append result (dataPair ID:ID faceArray:#(face.index))
			)
		)
		
		local fixedResult = #()
		
		for entry in result do 
		(
			if entry.ID > subMaterialCount then
			(
				local newID = int (mod entry.ID subMaterialCount)
				
				if newID == 0 then
				(
					newID = subMaterialCount
				)
				
				local index = findID result newID
				
				if index > 0 then
				(
					local fIndex = findID fixedResult newID
					
					if fIndex > 0 then
					(
						fixedResult[fIndex].faceArray = makeUniqueArray (fixedResult[fIndex].faceArray + entry.faceArray)
					)
					else
					(
						append fixedResult (dataPair ID:newID faceArray:(result[index].faceArray + entry.faceArray))
					)
				)
				else
				(
					append fixedResult (dataPair ID:newID faceArray:entry.faceArray)
				)
			)
			else
			(
				append fixedResult entry
			)
		)
		
-- 		print fixedResult
		
		qSort fixedResult comareID
		
		fixedResult
	)
	
	fn getSubMaterialsFromIDs material IDFaceArray =
	(
		local result = #()
		result.count = IDFaceArray.count
		
		for i = 1 to IDFaceArray.count do 
		(
			local ID = IDFaceArray[i].ID
			local index = findItem material.materialIDList ID
			
			result[i] = dataPair material:material.materialList[index] name:material.names[index]
		)
		
		result
	)
	
	mapped fn clean object =
	(
		convertToPoly object
		
		if (hasValidFaces object) then
		(
			if (classOf object.material) == Multimaterial then
			(
				local IDFaceArray = getIDFaceArray object object.material.numsubs
				local materialArray = getSubMaterialsFromIDs object.material IDFaceArray
				
				local materialData = dataPair IDArray:IDFaceArray materialArray:materialArray
				
				local newMulti = Multimaterial numsubs:materialData.materialArray.count name:(object.name + "_Multi")
				
				for i = 1 to materialData.IDArray.count do 
				(
					local subMat = materialData.materialArray[i].material
					local subName = materialData.materialArray[i].name
					local faceArray = materialData.IDArray[i].faceArray
					
					newMulti.materialList[i] = subMat
					newMulti.names[i] = subName
					
					polyOp.setFaceMatID object faceArray i
				)
				
				if newMulti.count == 1 then
				(
					newMulti = newMulti.materialList[1]
				)
				
				object.material = newMulti
			)
		)
		else
		(
			append badMeshArray object
		)
		
		count += 1
		
		progressUpdate (count as float / total * 100.0)
	)
	
	on btn_clean pressed do 
	(
		local objectArray = for object in selection where (getAttrClass object) == "Gta Object" and (classOf object) != XRefObject collect object
		local okToContinue
		
		if objectArray.count > 0 then
		(
			okToContinue = queryBox "Clean the multimaterials for the selected objects?" title:"Warning..."
		)
		else
		(
			objectArray = for object in objects where (getAttrClass object) == "Gta Object" and (classOf object) != XRefObject collect object
			
			okToContinue = queryBox "Nothing selected or invalid selection. Clean the multimaterials for all objects in the scene?" title:"Warning..."
		)
		
		total = objectArray.count
		count = 0
		badMeshArray = #()
		
		if okToContinue then
		(
			progressStart "Cleaning Multimats..."
			
			clean objectArray
			
			progressEnd()
			
			if badMeshArray.count > 0 then
			(
				select badMeshArray
				messageBox "The selected objects have corrupt mesh data.\nUse the Object Cleaner in the Toolbox to fix them but make sure you detach any children before you run it." title:"Error..."
			)
		)
	)
	
)

createDialog RSL_multiMatCleaner style:#(#style_toolwindow,#style_sysmenu)
