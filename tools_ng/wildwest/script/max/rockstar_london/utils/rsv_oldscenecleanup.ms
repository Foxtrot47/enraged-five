-------------------------------------------------------------------------------------
-- RSV_OldSceneCleanup.ms
--
-- Used to clean up, in part, old format art assets to the new Jimmy pipeline.
--
--	1.	Destroys the old Rub Collision and Payne Detail Collision.
--	2.	Removes old cover flag, removes old fragment state tags.
--			Removes the following tags:
--					_N1
--					_N2
--					_S2
--					_F2
--					_C_
--
--	Author:	Dustin Sutherland
--	Date:	October 5th, 2009
--
--	Copyright Rockstar Games, 2009
-------------------------------------------------------------------------------------

g_AssetTags = #("_C", "_N1", "_N2", "_S2", "_F2" );
g_PayneColTag = "Payne_Col"
g_PayneRubTag = "Payne_Rub"

-- Purpose:	Takes a string, searches for a substring and will then remove all instances of
--			that substring and return the filtered string.
fn ReplaceSubstringInStringWithString string theSubstring theReplacementString=
(
	
	local recursiveTestIndex = findString ( theReplacementString ) ( theSubstring );

	if ( recursiveTestIndex != undefined ) then
	(
		format "Error: ReplaceSubstringInStringWithString() - Recursive Replacement Error Detected. You cannot find a string\n"
		return undefined;
	)

	local subStringIndex = findString (string) (theSubstring);
	while ( subStringIndex != undefined ) do
	(
		string = replace (string) (subStringIndex) (theSubstring.count) (theReplacementString);
		subStringIndex = findString (string) (theSubstring)
	)

	return string;
)

fn RecursiveDeleteNode theNode=
(
	for child in theNode.Children do
	(
		RecursiveDeleteNode ( child );
	)
	format "Deleted Node: <%>\n" theNode.name;
	delete theNode;
)

-------------------------------------------------------------------------------------
-- 1.

-- Purpose:	Recursively Deletes all old Rub Collision from a scene.
fn DeleteSceneRubCollision=
(
	local foundObject = false;
	format ("Deleting Rub Collision\n");
	format ("----------------------\n");
	for obj in rootNode.children do
	(
		local recursiveTestIndex = findString ( obj.name ) ( g_PayneRubTag );
		if ( recursiveTestIndex != undefined ) then
		(
			foundObject = true;
			RecursiveDeleteNode ( obj );
		)
	)
	if ( not foundObject ) then
	(
		format "No Rub Collision in Scene.\n"
	)
	format ("\n");
)

-- Purpose:	Recursively Deletes all old Payne Collision from a scene.
fn DeleteScenePayneCollision=
(
	local foundObject = false;
	format ("Deleting Payne Collision\n");
	format ("------------------------\n");
	for obj in rootNode.children do
	(
		local recursiveTestIndex = findString ( obj.name ) ( g_PayneColTag );
		if ( recursiveTestIndex != undefined ) then
		(
			foundObject = true;
			RecursiveDeleteNode ( obj );
		)
	)
	if ( not foundObject ) then
	(
		format "No Payne Collision in Scene.\n"
	)
	format ("\n");
)

fn RemoveStateFlags node=
(
	local returnVal = false;
	local oldName = node.name;
	local newName = oldName;
	for tag in g_AssetTags do
	(
		newName = ReplaceSubstringInStringWithString ( newName ) ( tag ) ( "" );
		returnVal = true;
	)
	format ( "Changed Name of <%> to <%>\n" ) ( oldName ) ( newName );
	node.name = newName;

	return returnVal;
)

-- Purpose:	Goes through every object on the root node and will remove tags as outlined in the
--			comment header above. It will not touch the children nodes, as they will be adjusted
--			later in another pass on the scene.
fn RemoveSceneStateFlags=
(
	format ("Removing Object State Flags\n");
	format ("---------------------------\n");

	local iTotalRenamed = 0;
	for obj in rootnode.children do
	(
		if ( RemoveStateFlags ( obj ) ) then
		(
			iTotalRenamed += 1;
		)
	)
	format ("---------------------------\n");
	format ("Modified % names in total.\n") iTotalRenamed;
	format ("\n");
)

-------------------------------------------------------------------------------------
-- 2.

-- Purpose:	Renames all the children pieces on this object so that they are consistent with the parent.
-- Returns:	The number of chilren renamed.
fn ReAlignChildNames node=
(
	local localCount = 0;
	local iAdjustedNamesCount = 0;

	for child in node.children do
	(
		local oldName = child.name;
		iAdjustedNamesCount = iAdjustedNamesCount + 1;
		localCount = localCount + 1;

		local newName = ""; 

		if ( node.parent == rootNode ) then
		(
			newName = ( node.name + "_P-" + ( localCount as string ) );
		)
		else
		(
			newName = ( node.name + "-" + ( localCount as string ) );
		)
		
		child.name = newName;
		format ( "Changed Name of <%> to <%>\n" ) ( oldName ) ( newName );


		iAdjustedNamesCount += ( ReAlignChildNames ( child ) );

	)

	return iAdjustedNamesCount;
)

fn ReAlignSceneChildrenNames=
(
	format ("Realigning Children Names\n");
	format ("-------------------------\n");
	local iNumberOfAffectedChildren = 0;
	
	for obj in rootNode.children do
	(
		iNumberOfAffectedChildren += ( ReAlignChildNames ( obj ) );
	)
	
	if ( iNumberOfAffectedChildren <= 0 ) then
	(
		format("No Fragment Objects to rename.\n")
	)
	format ("-------------------------\n");
	format ("Modified % names in total.\n") iNumberOfAffectedChildren;
	format ("\n");

)

fn ProcessScene=
(
	local result = yesNoCancelBox "Are You Sure You Want to Reformat This Prop Scene? Any Unsaved Changes Will be lost!" 
	-- Popup some form that has some check boxes on which options they would like to do.

	if ( result == #yes ) then
	(
		DeleteSceneRubCollision();
		DeleteScenePayneCollision();
		RemoveSceneStateFlags();
		ReAlignSceneChildrenNames();
	)
)


ProcessScene();