-- dotNet class shortcuts script
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetClasses.ms")
-- dotNet UI functions script
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetUIOps.ms")

global RSL_PrimitiveConvert

struct RSL_collisionPrimitiveConvert
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- Struct variables
	---------------------------------------------------------------------------------------------------------------------------------------
	
	miloRoomArray = for object in helpers where (classOf object) == GtaMloRoom collect object,
	
	columnClicked,
	count = 1,
	
	-- standard attribute indices
	idxColType = getAttrIndex "Gta Collision" "Coll Type",
	idxCamera = getAttrIndex "Gta Collision" "Camera",
	idxPhysics = getAttrIndex "Gta Collision" "Physics",
	idxHiDetail = getAttrIndex "Gta Collision" "Hi Detail",
	idxLoco = getAttrIndex "Gta Collision" "Loco",
	
	idxStairs = getAttrIndex "Gta Collision" "Stairs",
	idxNoCover = getAttrIndex "Gta Collision" "Does Not Provide Cover",
	idxNoClimb = getAttrIndex "Gta Collision" "Non Climbable",
	idxNoWalk = getAttrIndex "Gta Collision" "Non Walkable",
	idxSee = getAttrIndex "Gta Collision" "See Through",
	idxNoCamera = getAttrIndex "Gta Collision" "Non Camera Collidable",
	idxShoot = getAttrIndex "Gta Collision" "Shoot Through",
	idxPiece = getAttrIndex "Gta Collision" "Piece Type",
	idxDay = getAttrIndex "Gta Collision" "Day Brightness",
	idxNight = getAttrIndex "Gta Collision" "Night Brightness",
	idxAudio = getAttrIndex "Gta Collision" "Audio Material",
	idxRoomID = getAttrIndex "Gta Collision" "Room ID",
	idxLeeds = getAttrIndex "Gta Collision" "Edited by Leeds",
	idxBVH = getAttrIndex "Gta Collision" "BVH Bound",
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- UI variables
	---------------------------------------------------------------------------------------------------------------------------------------
	convertForm,
	refreshButton,
	primitiveList,
	listViewRickClick,
	convertButton,
	mainProgress,
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- recurseGetMILORoom()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn recurseGetMILORoom object &result =
	(
		if (classOf object) == GtaMloRoom then
		(
			result = object
		)
		else
		(
			if (isValidNode object.parent) then
			(
				recurseGetMILORoom object.parent &result
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- addToListView()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn addToListView object =
	(
		if (getAttrClass object) == "Gta Collision" and (classOf object) != Col_Mesh then
		(
			recurseGetMILORoom object &miloRoom
			
			local newItem = dotNetObject "ListViewItem" object.name
			newItem.tag = dotNetMXSValue object
			newItem.subItems.add ((classOf object) as string)
			
			if object.parent != undefined then
			(
				local subItem = newItem.subItems.add object.parent.name
				subItem.tag = dotNetMXSValue object.parent
			)
			else
			(
				newItem.subItems.add "N/A"
			)
			
			if miloRoom != undefined then
			(
				local subItem = newItem.subItems.add miloRoom.name
				subItem.tag = dotNetMXSValue miloRoom
			)
			else
			(
				newItem.subItems.add "N/A"
			)
			
			primitiveList.items.add newItem
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchMiloItemClicked()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchMiloItemClicked s e =
	(
		RSL_PrimitiveConvert.miloItemClicked s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- miloItemClicked()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn miloItemClicked s e =
	(
		for i = 0 to primitiveList.selectedItems.count - 1 do 
		(
			local item = primitiveList.selectedItems.item[i]
			
			item.subItems.item[3].text = s.text
			item.subItems.item[3].tag = s.tag
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchObjectItemClicked()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchObjectItemClicked s e =
	(
		RSL_PrimitiveConvert.objectItemClicked s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- objectItemClicked()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn objectItemClicked s e =
	(
		for i = 0 to primitiveList.selectedItems.count - 1 do 
		(
			local item = primitiveList.selectedItems.item[i]
			
			item.subItems.item[2].text = s.text
			item.subItems.item[2].tag = s.tag
			
			recurseGetMILORoom s.tag.value &miloRoom
			
			if miloRoom != undefined then
			(
				item.subItems.item[3].text = miloRoom.name
				item.subItems.item[3].tag = dotNetMXSValue miloRoom
			)
			
			item.tag.value.parent = s.tag.value
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getContextItems()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getContextItems type =
	(
		result = #()
		
		case type of
		(
			#miloRoom:
			(
				result.count = miloRoomArray.count
				
				for i = 1 to miloRoomArray.count do 
				(
					local contextItem = RS_dotNetUI.ToolStripMenuItem miloRoomArray[i].name text:miloRoomArray[i].name
					contextItem.tag = dotNetMXSValue miloRoomArray[i]
					dotnet.addEventHandler contextItem "Click" dispatchMiloItemClicked
					
					result[i] = contextItem
				)
			)
			#object:
			(
				local objectArray = for object in geometry where (getAttrClass object) == "Gta Object" and (classOf object) != XRefObject collect object
				result.count = objectArray.count
				
				for i = 1 to objectArray.count do 
				(
					local contextItem = RS_dotNetUI.ToolStripMenuItem objectArray[i].name text:objectArray[i].name
					contextItem.tag = dotNetMXSValue objectArray[i]
					dotnet.addEventHandler contextItem "Click" dispatchObjectItemClicked
					
					result[i] = contextItem
				)
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getFlagsFromAttributes() Generates a rexBound flag integer from the standard attributes of the parsed object
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getFlagsFromAttributes object =
	(
		local result = 0
		
		if (try(getAttr object idxStairs) catch (false)) == true then
		(
			result = bit.or result (bit.shift 1 0)
		)
		if (try(getAttr object idxNoCover) catch (false)) == true then
		(
			result = bit.or result (bit.shift 1 4)
		)
		if (try(getAttr object idxNoClimb) catch (false)) == true then
		(
			result = bit.or result (bit.shift 1 1)
		)
		if (try(getAttr object idxNoWalk) catch (false)) == true then
		(
			result = bit.or result (bit.shift 1 5)
		)
		if (try(getAttr object idxSee) catch (false)) == true then
		(
			result = bit.or result (bit.shift 1 2)
		)
		if (try(getAttr object idxNoCamera) catch (false)) == true then
		(
			result = bit.or result (bit.shift 1 6)
		)
		if (try(getAttr object idxShoot) catch (false)) == true then
		(
			result = bit.or result (bit.shift 1 3)
		)
		
		result
	),
	
	fn seRexBoundMaterial newObject oldObject miloRoom =
	(
		local surfaceType = (getAttr oldObject idxColType)
		local flags = getFlagsFromAttributes oldObject
		
		local newMaterial = RexBoundMtl name:newObject.name
		
		RexSetCollisionName newMaterial surfaceType
		RexSetRoomNode newMaterial miloRoom
		RexsetCollisionFlags newMaterial flags
		
		newObject.material = newMaterial
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- convertBox()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn convertToBox object miloRoom =
	(
		local newBox = box width:(object.length * 2) length:(object.width * 2) height:(object.height * 2) wireColor:orange
		newBox.transform = object.transform
		in coordsys newBox newBox.pos.z -= object.height
		newBox.name = uniqueName (object.parent.name + " - Col_Mesh")
		newBox.parent = object.parent
		
		seRexBoundMaterial newBox object miloRoom
		
		mesh2col newBox
		
		select object
		CopyAttrs()
		
		select newBox
		PasteAttrs()
		
		delete object
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- convertToCylinder()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn convertToCylinder object miloRoom =
	(
		
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- convert()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn convert data total =
	(
		case (classOf data.node) of
		(
			Col_Box:
			(
				convertToBox data.node data.miloRoom
			)
			Col_Cylinder:
			(
				
			)
		)
		
		local value = (count as float / total * 100.0)
		mainProgress.value = value
		progressUpdate value
		
		count += 1
	),
	
	
	
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- UI FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- disableAccelerators() Called when the user enters any control that needs keyboard input e.g. a textBox
	---------------------------------------------------------------------------------------------------------------------------------------
	fn disableAccelerators s e =
	(
		enableAccelerators = false
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOnFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOnFormClosing s e =
	(
		RSL_PrimitiveConvert.formClosing s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- formClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn formClosing s e =
	(
		
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOnFormResize() Dispatches formResize() Called when the user resizes the form
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOnFormResize s e =
	(
		RSL_PrimitiveConvert.formResize s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- formResize()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn formResize s e =
	(
		primitiveList.AutoResizeColumns (dotNetClass "ColumnHeaderAutoResizeStyle").HeaderSize
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchRefreshPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchRefreshPressed s e =
	(
		RSL_PrimitiveConvert.refreshPressed s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- refreshPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn refreshPressed s e =
	(
		miloRoomArray = for object in helpers where (classOf object) == GtaMloRoom collect object
		primitiveList.items.clear()
		addToListView helpers
		primitiveList.AutoResizeColumns (dotNetClass "ColumnHeaderAutoResizeStyle").HeaderSize
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchPrimitiveListMouseDown()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchPrimitiveListMouseDown s e =
	(
		RSL_PrimitiveConvert.primitiveListMouseDown s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- primitiveListMouseDown()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn primitiveListMouseDown s e =
	(
		listViewRickClick.items.clear()
		local item = primitiveList.getItemAt e.x e.y
		local parent = if item.subItems.item[2].tag != undefined then item.subItems.item[2].tag.value else undefined
		
		if item != undefined then
		(
			local total = 0
			local index = -1
			
			for i = 0 to primitiveList.columns.count - 1 do
			(
				local column = primitiveList.columns.item[i]
				
				if e.x > total and e.x < (total + column.width) then
				(
					index = i
				)
				
				total += column.width
			)
			
			case of
			(
				default:(columnClicked = #null)
				(index == 2):(columnClicked = #object)
				(index == 3 and parent != undefined):(columnClicked = #miloRoom)
			)
			
			listViewRickClick.items.addRange (getContextItems columnClicked)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchPrimitiveListDoubleClick()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchPrimitiveListDoubleClick s e =
	(
		RSL_PrimitiveConvert.primitiveListDoubleClick s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- primitiveListDoubleClick()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn primitiveListDoubleClick s e =
	(
		if s.selectedItems.count == 1 then
		(
			local item = s.selectedItems.item[0]
			select item.tag.value
			
			max zoomext sel
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchConvertPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchConvertPressed s e =
	(
		RSL_PrimitiveConvert.convertPressed s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- convertPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn convertPressed s e =
	(
		if primitiveList.selectedItems.count > 0 then
		(
			local selectedArray = #()
			
			for i = 0 to primitiveList.selectedItems.count - 1 do 
			(
				local item = primitiveList.selectedItems.item[i]
				local parent = if item.subItems.item[2].tag != undefined then item.subItems.item[2].tag.value else undefined
				local miloRoom = if item.subItems.item[3].tag != undefined then item.subItems.item[3].tag.value else undefined
				
				if parent != undefined and miloRoom != undefined then
				(
					append selectedArray (dataPair node:item.tag.value miloRoom:miloRoom)
				)
				else
				(
					
				)
			)
			
			if selectedArray.count > 0 then
			(
				local okToContinue = queryBox "Are you sure you want to convert the selected primitives?\nYou cannot undo this so be sure you've saved the file!" title:"Warning..."
				
				if okToContinue then
				(
					count = 1
					
					convert selectedArray selectedArray.count
					
					mainProgress.value = 0.0
					
					refreshPressed "" ""
				)
			)
			else
			(
				messageBox "None of the selected items can be converted because they do not have a MILO Room set." title:"Error..."
			)
		)
		else
		(
			messageBox "You have no collision primitives selected in the list view.\nYou must select at least one." title:"Error..."
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- optionsDialog()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn createForm =
	(
		clearListener()
		
		--In this case, we're going to use a standard form rather than the custom maxForm written for max. this is because I want to change the 
		-- form back colour which you can't do if you use the custom maxForm. For this to work, we need to set the parent of the form to be Max. 
		-- USING THIS TYPE OF FORM MEANS WE NEED TO DISSABLE ACCELERATORS WHEN WE FOCUS ON A CONTROL THAT TAKES
		-- KEY PRESSES SUCH AS SPINNERS AND TEXT BOXES
		
		--Get the max handle pointer.
		local maxHandlePointer=(Windows.GetMAXHWND())
		
		--Convert the HWND handle of Max to a dotNet system pointer
		local sysPointer = DotNetObject "System.IntPtr" maxHandlePointer
		
		--Create a dotNet wrapper containing the maxHWND
		local maxHwnd = DotNetObject "MaxCustomControls.Win32HandleWrapper" sysPointer
		
		convertForm = RS_dotNetUI.form "convertForm" text:" R* Collision Primitive Converter" size:[256, 256] min:[256, 256] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		convertForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		dotnet.addEventHandler convertForm "FormClosing" dispatchOnFormClosing
		dotnet.addEventHandler convertForm "Enter" disableAccelerators
		dotnet.addEventHandler convertForm "Resize" dispatchOnFormResize
		
		refreshButton = RS_dotNetUI.Button "refreshButton" text:("Refresh") dockStyle:RS_dotNetPreset.DS_Fill textAlign:RS_dotNetPreset.CA_BottomCenter
		refreshButton.margin = (RS_dotNetObject.paddingObject 0)
		dotnet.addEventHandler refreshButton "click" dispatchRefreshPressed
		
		listViewRickClick = RS_dotNetUI.ContextMenuStrip "mainListViewRickClick" --items:(getContextItems())
		
		primitiveList = RS_dotNetUI.listView "mainListView" dockStyle:RS_dotNetPreset.DS_Fill
		primitiveList.fullRowSelect = true
		primitiveList.HideSelection = false
		primitiveList.MultiSelect = true
-- 		primitiveList.ContextMenuStrip = listViewRickClick
		dotnet.addEventHandler primitiveList "Enter" disableAccelerators
-- 		dotnet.addEventHandler primitiveList "MouseDown" dispatchPrimitiveListMouseDown
		dotnet.addEventHandler primitiveList "DoubleClick" dispatchPrimitiveListDoubleClick
		
		primitiveList.columns.add "Name"
		primitiveList.columns.add "Type"
		primitiveList.columns.add "Parent"
		primitiveList.columns.add "MILO Room"
		
		convertButton = RS_dotNetUI.Button "convertButton" text:("Convert Selected") dockStyle:RS_dotNetPreset.DS_Fill textAlign:RS_dotNetPreset.CA_BottomCenter
		convertButton.margin = (RS_dotNetObject.paddingObject 0)
		dotnet.addEventHandler convertButton "click" dispatchConvertPressed
		
		mainProgress = dotNetObject "progressBar"
		mainProgress.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		mainProgress.Style = (dotNetClass "System.Windows.Forms.ProgressBarStyle").Continuous
		mainProgress.BackColor = RS_dotNetClass.colourClass.DimGray
		mainProgress.ForeColor = RS_dotNetClass.colourClass.fromARGB 255 127.5 0
		
		mainTableLayout = RS_dotNetUI.tableLayout "mainTableLayout" text:"mainTableLayout" collumns:#((dataPair type:"Percent" value:50)) \
									rows:#((dataPair type:"Absolute" value:24),(dataPair type:"Percent" value:50),(dataPair type:"Absolute" value:24),(dataPair type:"Absolute" value:24)) dockStyle:RS_dotNetPreset.DS_Fill
		
		mainTableLayout.controls.add refreshButton 0 0
		mainTableLayout.controls.add primitiveList 0 1
		mainTableLayout.controls.add convertButton 0 2
		mainTableLayout.controls.add mainProgress 0 3
		
		convertForm.controls.add mainTableLayout
		
		addToListView helpers
		primitiveList.AutoResizeColumns (dotNetClass "ColumnHeaderAutoResizeStyle").HeaderSize
		
		convertForm.show maxHwnd
	)
)

if RSL_PrimitiveConvert != undefined then
(
	try (RSL_PrimitiveConvert.convertForm.close()) catch()
)

RSL_PrimitiveConvert = RSL_collisionPrimitiveConvert()
RSL_PrimitiveConvert.createForm()