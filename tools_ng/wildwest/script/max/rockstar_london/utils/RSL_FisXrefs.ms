try (destroyDialog RSL_RebuildXrefs) catch()
ValidXrefs = #()
rollout RSL_RebuildXrefs "RS Rebuild Xrefs v1.0" width:190
(

	button btn_Process "ReBuild Xrefs" width:160 align:#center	
	radiobuttons MaterialDup "How to Handle Duplciate Materials" labels:#("Use Scene Material", "use Merged Material", "Auto Rename Merged Material") align:#left	
	label lbl_info "" align:#centre	
	groupBox group1 "" width:160 pos:[14,86]
	hyperlink lnkHelp	"Help?" address:"https://mp.rockstargames.com/index.php/RS_Rebuild_Xrefs" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	------------------------------------------------------------------------------------------
	-- Simple struct to collect the relevant info about a Xref object
	------------------------------------------------------------------------------------------
	struct xrefprop
	(
		objectname,
		name,
		transform,
		srcFileName,
		node	
	)
	------------------------------------------------------------------------------------------
	-- This function is called whne the show duplaicte material dialog is displayed.
	-- It will set the action to use Scene Materials CUrrently
	------------------------------------------------------------------------------------------
	fn checkDialog = (
		local hwnd = dialogMonitorOps.getWindowHandle()
		if (uiAccessor.getWindowText hwnd == "Duplicate Material Name") then (
			materialText = case MaterialDup.state of (
				1: "Use Scene Material"
				2: "Use Merged Material"
				3: "Auto-Rename Merged Material" )			
			uiAccessor.pressButtonByName hwnd materialText
		)
		true
	)


	------------------------------------------------------------------------------------------
	-- Collects all valid unresolved Xrefs.
	-- Will collect the transform, name etc as a struct. This way we can keep a history of the objects
	------------------------------------------------------------------------------------------
	fn collectXrefInfo =
	(
		for xref in objects where classof(xref) == xrefobject and xref.unresolved == true do
		(
			oldxref =xrefprop()
			oldxref.objectname = xref.objectname
			oldxref.name = xref.name
			oldxref.transform = xref.transform
			oldxref.srcFileName = xref.srcFileName
			oldxref.node = xref
			append ValidXrefs oldxref
			
		)
	)

	---------------------------------------------------
	-- Main Function to rebuild the Xref Data
	---------------------------------------------------
	fn reCreateXrefData XrefList =
	(
		-- We Regeister the Duplicate Material Dialog message box with a function to automatically handle the input
		-- Instead of teh user input

		DialogMonitorOPS.unRegisterNotification id:#DuplicateMaterialDialog
		dialogMonitorOps.enabled = true
		dialogMonitorOps.interactive = false
		dialogMonitorOps.registerNotification checkDialog id:#DuplicateMaterialDialog
		
		progressStart "rebuilding xrefs"
		local i = 0
		for xref in XrefList do
		(
			progressupdate ((100.0 * i) / XrefList.count)		
			i+=1
			local createObjXrefsObj = xrefs.addNewXRefObject (xref.srcFileName) (xref.objectname) manipulators:#merge modifiers:#drop
			-- If the successfully load the unresolved xref then we can delte the old one and update the attributes
			if createObjXrefsObj != undefined then
			(
				delete xref.node -- Delete the old Xref
				createObjXrefsObj.updateMaterial = false
				createObjXrefsObj.transform = xref.transform
				createObjXrefsObj.name =  xref.name
				
			)
		)
		progressend()
		dialogMonitorOps.enabled = false
	)
	

	-- Get from the scene all valid un-resolved sxrefs
	-- Update the display with the number found
	--
	fn getUnresolvedXrefs =
	(
		ValidXrefs = #()
		collectXrefInfo()
		lbl_info.text = ("Found: "+ValidXrefs.count as string+" Unresolved Xrefs")
	)


	---------------------------------------------------
	-- Event Handlers
	---------------------------------------------------
	on RSL_RebuildXrefs open do
	(
		getUnresolvedXrefs()
	)
	
	on btn_Process pressed do
	(
		getUnresolvedXrefs() -- Added incase a new file is opened while the tool is still open
		reCreateXrefData ValidXrefs
		getUnresolvedXrefs()
	)
)





createDialog RSL_RebuildXrefs style:#(#style_toolwindow,#style_sysmenu)