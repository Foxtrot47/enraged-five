--------------------------------------------------------------------------------------------
-- 						RSL_BoundOps.ms
--							By Paul Truss
--							Senior Technical Artist
--							Rockstar Games
--							London
--							V1.0 28/4/2010
--
--							Contains common functions for extracting files from packs and building meshes from bound files
--
--------------------------------------------------------------------------------------------

global RSL_bndOps
global RSL_Xml

filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/pipeline/RSL_SceneXml.ms")

struct RSL_BoundOps
(
	randomColourArray = #(),
	
	masterScale = 0.07,
	--------------------------------------------------------------------------------------------
	-- 						extract()
	--							Extracts the chosen file type from the give pack files.
	--							Returns an array of the extracted files.
	--
	--						level: level name (string)
	--						list: list of files to extract from
	--						fileType: the type of file to extract
	--						mapType: Optional, the type of mapfile e.g. "interiors" or "props"
	--------------------------------------------------------------------------------------------
	fn extract level list filetype mapType:"" =
	(
		local rageEXE = "ragebuilder_0327.exe"
		local outputDirectory
		local result = #()
		
		progressStart ("Extracting ." + filetype)
		
		for i = 1 to list.count do
		(
			local image = list[i]
			
			local imageName = substituteString (pathConfig.stripPathToLeaf (getFilenamePath list[i])) "\\" ""
			
			if (toLower (getFilenameType image)) == ".rpf" then
			(
				imageName = (getFilenameFile image)
			)
			
			outputDirectory = "c:\\temp_" + filetype + "\\" + level + "\\" + mapType + "\\" + imageName
			makedir outputDirectory
			outputDirectory += "\\"
			
			local arguments = (" x:\\tools\\bin\\script\\Extracter.rbs -input " + image + " -output " + outputDirectory + " -filetype " + filetype)
			local rageCommand = (rageEXE + arguments)
			
			HiddenDOSCommand rageCommand startpath:"x:\\tools\\bin\\"
			
			result += (getFiles (outputDirectory + "*." + filetype))
			
			local updateValue = (i as float / list.count) * 100.0
			progressUpdate updateValue
		)
		progressEnd()
		
		result
	),
	
	fn getScale bitmap =
	(
		local result = [1.0,1.0,1.0]
		
		if bitmap.width > bitmap.height then
		(
			result.y = 1.0 / (bitmap.height / (bitmap.width as float))
		)
		else
		(
			result.x = 1.0 / (bitmap.width / (bitmap.height as float))
		)
		
		result
	),
	
	
	--------------------------------------------------------------------------------------------
	-- 						buildMesh()
	--							Builds a new mesh from the give data.
	--
	--						vertArray: array of vertex positions
	--						faceList: array of face vert indexes
	--						name: the name of the object
	--						offset: Optional, the offset postion of the object
	--						quat: Optional, the rotation of the object
	--						layer: Optional, the layer the object is assigned to
	--						wireColour: Optional the wireColor of the new object
	--------------------------------------------------------------------------------------------
	fn buildMesh vertArray faceList name offset:[0,0,0] quat:(quat 0 0 0 0) layer:undefined wireColour:undefined material:undefined =
	(
		local newMesh = mesh pos:offset name:(name + "_collision") vertices:vertArray faces:faceList
		
		if wireColour != undefined then
		(
			newMesh.wireColor = wireColour
		)
		
		meshop.autoSmooth newMesh (for face in newMesh.faces collect face.index) 30.0
		meshop.autoEdge newMesh (for edge in newMesh.edges collect edge.index) 1.0 --type:#SetClear
		
-- 		format "Name:% Offset:% Quat:%\n" name offset quat
		
		rotate newMesh (inverse quat)
		
		convertToPoly newMesh
		
		if material != undefined then
		(
			newMesh.material = material
			
			local scale = (getScale material.diffuseMap.bitmap)
			polyOp.applyUVWMap newMesh #planar uTile:(masterScale * scale.x) vTile:(masterScale * scale.y) wtile:(masterScale * scale.z)
		)
		
		if layer != undefined then
		(
			layer.addNode newMesh
		)
	),
	
	--------------------------------------------------------------------------------------------
	-- 						readBVH()
	--							Extracts mesh data from a BVH type bnd file.
	--
	--						dataStream: the opened bnd file to read from
	--						name: the name of the object
	--						offset: Optional, the offset postion of the object
	--						quat: Optional, the rotation of the object
	--						layer: Optional, the layer the object is assigned to
	--						wireColour: Optional the wireColor of the new object
	--------------------------------------------------------------------------------------------
	fn readBVH dataStream name offset:[0,0,0] quat:(quat 0 0 0 0) layer:undefined wireColour:undefined material:undefined =
	(
		local vertexArray = #()
		local faceArray = #()
		
		seek dataStream 0
		
		while not eof dataStream do
		(
			local line = filterstring (Readline dataStream) " "
			
			case line[1] of
			(
				(tolower "v"):
				(
					append vertexArray ([(readValue (line[2] as stringStream)), (readValue (line[3] as stringStream)), (readValue (line[4] as stringStream))] )
				)
				(tolower "tri"):
				(
					append faceArray [(readValue (line[2] as stringStream)) +1, (readValue (line[3] as stringStream)) +1, (readValue (line[4] as stringStream)) +1]
				)
			)
		)
		
-- 		print vertexArray #noMap
-- 		print faceArray #noMap
		
		buildMesh vertexArray faceArray name offset:offset quat:quat layer:layer wireColour:wireColour material:material
	),
	
	--------------------------------------------------------------------------------------------
	-- 						readComposite()
	--							Extracts mesh data from a Composite type bnd file.
	--
	--						dataStream: the opened bnd file to read from
	--						name: the name of the object
	--						offset: Optional, the offset postion of the object
	--						quat: Optional, the rotation of the object
	--						layer: Optional, the layer the object is assigned to
	--						wireColour: Optional the wireColor of the new object
	--------------------------------------------------------------------------------------------
	fn readComposite dataStream name offset:[0,0,0] quat:(quat 0 0 0 0) layer:undefined wireColour:undefined material:undefined =
	(
		local bndsection = "ignore"
		local vertexArray = #()
		local faceArray = #()
		
		while not eof dataStream do
		(
			local line = filterstring (Readline dataStream) " "
			
			case line[1] of
			(
				default:
				(
					bndsection = "ignore"
				)
				(tolower "v"):
				(
					bndsection = "vertices"
				)
				(tolower "tri"):
				(
					bndsection = "tris"
				)
				(tolower "matrix:"):
				(
					bndsection = "newmesh"
				)
			)
			
			if line.count < 2 then continue				
			
			case bndsection of
			(
				"newmesh":
				(
					bndsection = "ignore"
					
					buildMesh vertexArray faceArray name offset:offset quat:quat layer:layer wireColour:wireColour material:material
					
					vertexArray = #()
					faceArray = #()
				)
				"vertices":
				(
					append vertexArray ([(readValue (line[2] as stringStream)), (readValue (line[3] as stringStream)), (readValue (line[4] as stringStream))])
				)
				"tris":
				(
					append faceArray [(readValue (line[2] as stringStream)) +1, (readValue (line[3] as stringStream)) +1, (readValue (line[4] as stringStream)) +1]
				)
			)
		)
	),
	
	--------------------------------------------------------------------------------------------
	-- 						readBox()
	--							Extracts mesh data and builds the object from a Box type bnd file.
	--
	--						dataStream: the opened bnd file to read from
	--						name: the name of the object
	--						offset: Optional, the offset postion of the object
	--						quat: Optional, the rotation of the object
	--						layer: Optional, the layer the object is assigned to
	--						wireColour: Optional the wireColor of the new object
	--------------------------------------------------------------------------------------------
	fn readBox dataStream name offset:[0,0,0] quat:(quat 0 0 0 0) layer:undefined wireColour:undefined material:undefined =
	(
		seek dataStream 0
		skipToString dataStream "size: "
		
		local line = readLine dataStream
		local lineFiltered = filterString line " 	"
		
		local width = (lineFiltered[1] as float)
		local length = (lineFiltered[2] as float)
		local height = (lineFiltered[3] as float)
		
		local position = offset
		position.z += height * 0.5
		
		local newMesh = box name:(name + "collision") pos:position width:width length:length height:height
		
		if wireColour != undefined then
		(
			newMesh.wireColor = wireColour
		)
		
		rotate newMesh quat
		convertToPoly newMesh
		
		if material != undefined then
		(
			newMesh.material = material
			
			local scale = (getScale material.diffuseMap.bitmap)
			polyOp.applyUVWMap newMesh #planar uTile:(masterScale * scale.x) vTile:(masterScale * scale.y) wtile:(masterScale * scale.z)
		)
		
		if layer != undefined then
		(
			layer.addNode newMesh
		)
	),
	
	--------------------------------------------------------------------------------------------
	-- 						getSubStream()
	--							Returns a sub string stream based on the parsed dataStream and end string.
	--							the dataStream is read until the end string is found. All string data found
	--							before that is returned as a stringStream
	--
	--						dataStream: a stringstream or filestream
	--						end: a string containing characters we look for to end the function.
	--------------------------------------------------------------------------------------------
	fn getSubStream dataStream end =
	(
		local result = stringStream ""
		
		local line = readLine dataStream
		local lineStart = filePos dataStream
		
		while not (MatchPattern line pattern:(end + "*")) and not (eof dataStream) do 
		(
			lineStart = filePos dataStream
			format "%\n" line to:result
			local line = readLine dataStream
		)
		
		seek dataStream lineStart
		
		result
	),
	
	--------------------------------------------------------------------------------------------
	-- 						getFlag()
	--							Returns a flag string based on the parsed string. If the string
	--							contains "version*" it's not a flag string and we return "hi".
	--							Some bnd file contain entries with no collision type flags set.
	--
	--						string: a string that might contain the flag string
	--------------------------------------------------------------------------------------------
	fn getFlag string =
	(
		if (matchPattern string pattern:"version*") then
		(
			"phys"
		)
		else
		(
			local newString = substituteString string "flags: " ""
			substituteString newString "\"" ""
		)
	),
	
	fn getPoint3 string =
	(
		local result = [0,0,0]
		local filtered = filterString string "\t"
		
		for i = 1 to filtered.count do 
		(
			result[i] = readValue (filtered[i] as stringStream)
		)
		
		result
	),
	
	fn readMatrix dataStream =
	(
		local trans = Matrix3 0
		
		trans.row1 = getPoint3 (readLine dataStream)
		trans.row2 = getPoint3 (readLine dataStream)
		trans.row3 = getPoint3 (readLine dataStream)
		trans.row4 = getPoint3 (readLine dataStream)
		
		trans
	),
	
	--------------------------------------------------------------------------------------------
	-- 						readBoundfile()
	--							Reads the bound file to get the type and hands it off to the relevant read function.
	--
	--						filename: the bnd filename to open
	--						name: the name of the object
	--						offset: Optional, the offset postion of the milo
	--						quat: Optional, the rotation of the milo
	--						layer: Optional, the layer the object is assigned to
	--						wireColour: Optional the wireColor of the new object
	--------------------------------------------------------------------------------------------
	fn readBoundfile filename offset:[0,0,0] quat:(quat 0 0 0 0) layer:undefined wireColour:undefined material:undefined =
	(
		local name = getFilenameFile filename
		local dataStream = openFile filename

		if dataStream != undefined then
		(
			skipToString dataStream "type: "
			local type = readLine dataStream
			
			if type == "composite" then
			(
				skipToString dataStream "NumBounds: "
				local count = readValue dataStream
				
				for i = 1 to count do
				(
					skipToString dataStream "bound: "
					skipToNextLine dataStream
					local flags = getFlag (readLine dataStream)
					
					if (matchPattern flags pattern:"*phys*") then
					(
						skipToString dataStream "type: "
						local subType = readLine dataStream
						
						local subStream = getSubStream dataStream "matrix:"
						skipToString dataStream "matrix:"
						local trans = readMatrix dataStream
						
						case subType of
						(
							default:print subType
							"bvh":
							(
								readBVH subStream name offset:(offset + trans.row4) quat:(quat * trans.rotationpart) layer:layer wireColour:wireColour material:material 
							)
							"composite":
							(
								readComposite subStream name offset:(offset + trans.row4) quat:(quat * trans.rotationpart) layer:layer wireColour:wireColour material:material 
							)
							"box":
							(
								readBox subStream name offset:(offset + trans.row4) quat:(quat * trans.rotationpart) layer:layer wireColour:wireColour material:material 
							)
						)
					)
					else
					(
-- 						format "Skipping Bound: %\n" i
					)
				)
			)
			else
			(
				case type of
				(
					default:print type
					"bvh":
					(
						readBVH dataStream name offset:offset quat:quat layer:layer wireColour:wireColour
					)
					"composite":
					(
						readComposite dataStream name offset:offset quat:quat layer:layer wireColour:wireColour
					)
					"box":
					(
						readBox dataStream name offset:offset quat:quat layer:layer wireColour:wireColour
					)
				)
			)
			
			close dataStream
		)
		else
		(
			print (doesFileExist filename)
		)
	),
	
	fn isSimilar colour =
	(
		local result = false
		
		for entry in randomColourArray do 
		(
			if abs (entry.r - colour.r) < 16 and abs (entry.g - colour.g) < 16 and abs (entry.b - colour.b) < 16 then
			(
				result = true
				exit
			)
		)
		
		result
	),
	
	fn getRandomColour =
	(
		local result = random (color 0 0 0) (color 220 220 220)
		
		while (findItem randomColourArray result) > 0 and not (isSimilar result) do 
		(
			result = random (color 0 0 0) (color 220 220 220)
		)
		
		append randomColourArray result 
		
		result
	),
	
	fn getFontColour colour =
	(
		local fontColour = colour + 16
		
		if fontColour.r > 255 then fontColour.r = 255
		if fontColour.g > 255 then fontColour.g = 255
		if fontColour.b > 255 then fontColour.b = 255
		if fontColour.a > 255 then fontColour.a = 255
		
		dotNetObject RS_dotNetClass.solidBushClass (RS_dotNetClass.colourClass.fromARGB fontColour.r fontColour.g fontColour.b)
	),
	
	fn getUserMaterial level name wireColour interiorXml:undefined =
	(
		local xml = "c:\\temp_xml\\" + level + "\\" + name + "\\" + name + ".xml"
		local file = "c:\\temp_xml\\" + level + "\\" + name + "\\" + name + ".bmp"
		
		if interiorXml != undefined then
		(
			xml = interiorXml
			file = (getFilenamePath xml) + name + ".bmp"
		)
		
		local user = toUpper (RSL_SceneXmlOps.getUser xml)
		
		deleteFile file
		
		local colour = RS_dotNetClass.colourClass.fromARGB wireColour.r wireColour.g wireColour.b
		
		local font = dotNetObject "System.Drawing.Font" "Trebuchet MS" 48 RS_dotNetPreset.f_Bold (dotNetClass "System.Drawing.GraphicsUnit").Pixel
		local fontColour = getFontColour wireColour
		
		local textRenderer = dotNetClass "System.Windows.Forms.TextRenderer"
		local size = textRenderer.MeasureText user font
		
		local dimW = size.width --(size.width + 4)
		local dimH = size.height --(size.height + 4)
		
		local bitmapImage = dotNetObject RS_dotNetClass.bitmapClass dimW dimH
		graphics = (dotNetClass "System.Drawing.Graphics").FromImage bitmapImage
		
		-- fill the bitmap with the colour and write the name string to the bitmap
		graphics.clear colour --FillRectangle backBrush (dotNetObject RS_dotNetClass.rectangleClass 0 0 dimW dimH)
		graphics.SmoothingMode = (dotNetClass "System.Drawing.Drawing2D.SmoothingMode").AntiAlias
		graphics.TextRenderingHint = (dotNetClass "System.Drawing.Text.TextRenderingHint").AntiAlias
		
		graphics.DrawString user font fontColour 0 0 --stringFormat
		
		bitmapImage.save file (RS_dotNetClass.imageFormatClass.bmp)
		
		local newMaterial = Standardmaterial name:name diffuseMap:(bitmapTexture filename:file)
-- 		local scale = getScale newMaterial.diffuseMap.bitmap
-- 		newMaterial.diffuseMap.coordinates.U_Tiling = scale.x
-- 		newMaterial.diffuseMap.coordinates.V_Tiling = scale.y
		
		showTextureMap newMaterial newMaterial.diffuseMap true
		
		newMaterial
	),
	
	--------------------------------------------------------------------------------------------
	-- 						parseBoundFiles()
	--							processes each bnd in bndList.
	--							Creates a new layer based on the image the bnd files were extracted from if needed.
	--
	--						bndList: the array of bnd files to process
	--						offset: Optional, the offset postion of the milo
	--						quat: Optional, the rotation of the milo
	--						interiorXml: Optional the interior xml file if you're parsing a set of interior bnd files
	--						layer: Optional, the layer the object is assigned to
	--						wireColour: Optional the wireColor of the new objects
	--------------------------------------------------------------------------------------------
	fn parseBoundFiles bndList level offset:[0,0,0] quat:(quat 0 0 0 0) interiorXml:undefined layers:true wireColour:undefined randomColour:false userName:false =
	(
		progressStart "Reading Bounds"
		
		randomColourArray = #()
		
		for i = 1 to bndList.count do
		(
			local currentLayer
			
			if layers then
			(
				local imageName = substituteString (pathConfig.stripPathToLeaf (getFilenamePath bndlist[i])) "\\" ""
				currentLayer = LayerManager.getLayerFromName imageName
				
				if currentLayer == undefined then
				(
					currentLayer = LayerManager.newLayerFromName imageName
				)
			)
			
			local finalOffset = offset
			local finalQuat = quat
			
			if interiorXml != undefined then
			(
				local finalQuat = quatToEuler quat
				local interiorTransform = RSL_Xml.getObjectTransform interiorXml (getFilenameFile bndlist[i])
				
				finalOffset += interiorTransform[1]
				
				local tempEuler = quatToEuler interiorTransform[2]
				
				finalQuat.x += tempEuler.x
				finalQuat.y += tempEuler.y
				finalQuat.z += tempEuler.z
				
				finalQuat = eulerToQuat finalQuat
				
-- 				format "Offset:% Interior:% Final:%\nQuat:% Interior:% Final:%\n" offset interiorTransform[1] finalOffset quat interiorTransform[2] finalQuat
			)
			
			if randomColour then
			(
				wireColour = getRandomColour()
			)
			
			local material 
			
			if userName then
			(
				local path = (getFilenamePath bndlist[i])
				local filtered = (filterString path "\\")
				local name = filtered[filtered.count]
				
				material = getUserMaterial level name wireColour interiorXml:interiorXml
			)
			
			readBoundfile bndlist[i] offset:finalOffset quat:finalQuat layer:currentLayer wireColour:wireColour material:material
			
			local updateValue = (i as float / bndlist.count) * 100.0
			progressUpdate updateValue
		)
		
		progressEnd()
	)
)

RSL_bndOps = RSL_BoundOps()