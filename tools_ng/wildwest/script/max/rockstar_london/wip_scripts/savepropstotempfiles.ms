XrefArray = #()

for object in objects where (classOf object) == XrefObject do
(
	local isMilo = (matchPattern object.objectName pattern:"*_milo_")
	
	if not isMilo then
	(
		append XrefArray object
	)
)

savePath = maxFilePath + "tempfiles\\"
filename = (getFilenameFile maxFileName) + "_PROPS.max"

saveNodes XrefArray (savePath + filename) 