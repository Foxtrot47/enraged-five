objectArray = for object in objects where (classOf object) == XRefObject collect object
deleteArray = #()

for i = 1 to objectArray.count - 1 do
(
	local first = objectArray[i]
	
	if (isValidNode first) do
	(
		for j = i + 1 to objectArray.count do
		(
			local second = objectArray[j]
			
			if (isValidNode second) do
			(
				if first.objectName == second.objectName and (distance first.pos second.pos) < 0.001 then
				(
					appendIfUnique deleteArray second
				)
			)
		)
	)
)

try (delete deleteArray) catch()