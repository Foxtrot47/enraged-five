clearlistener()


rollout XrefProgress "Processing" width:162 height:31
(
	progressbar doit_prog color:red
)

try (destroyDialog Rollout_MigrateTools) catch ()
rollout Rollout_MigrateTools "MigrateTools V0.32" width:384 height:543
(
	--////////////////////////////////////////////////////////////
	-- GLOBALS
	--////////////////////////////////////////////////////////////
	
	local Invalidfiles = #()
	local mapfiles=#()
	local filehistory = 10	
	local initialDir =rsconfig.toolsroot
	global my_files = #()
	
	--////////////////////////////////////////////////////////////
	-- FUNCTIONS
	--////////////////////////////////////////////////////////////

	-------------------------------------------------------------
	--		Test the filename, if texturte exists or not.
	-------------------------------------------------------------
	fn fixTextureName subTexture =
	(		
		local basepath = getFilenamePath subTexture.filename as string
		local texname =  filenameFromPath subTexture.filename as string
				
		newPath = substituteString (tolower basepath + texname) (tolower Rollout_MigrateTools.edttext1.text) (tolower Rollout_MigrateTools.edttext2.text)
		--print ("New Path: "+newPath as string)
		local file = getFiles newPath
		if file.count > 0 then 
		(
			subTexture.filename = newPath
		)
	)
	
	mapped fn fixTextureATSOpsPaths filename =
	(		
		local basepath = getFilenamePath filename
		--local texname =  filenameFromPath filename
				
		newPath = substituteString (tolower filename) (tolower Rollout_MigrateTools.edttext1.text) (tolower Rollout_MigrateTools.edttext2.text)
		print ("New Path: "+newPath as string)
		local file = getFiles newPath
		if file.count > 0 then 
		(
			ATSOps.SetPathOnSelection basepath
			--filename = newPath
			--subTexture.filename = newPath
		)
	)
	
	------------------------------------------------------------------------------------------------------------------------
	--		Recursive Find All  bitmap textures within shader.
	--		TODO: if there is a multimaterial within the shader itself then this will need to be checked for
	------------------------------------------------------------------------------------------------------------------------
	fn collectTextures mat =
	(
		local submaterials = (getNumSubTexmaps mat)
		--print mat
		for i = 1 to submaterials do
		(
			m = getSubTexmap mat i
			if classof m == Bitmaptexture then 
			(
				fixTextureName m
			)
			else if m != undefined do
			(
				collectTextures m
			)
		)
	)

	fn scanAssetTracking =
	(
		
		inc = 0
		
		ATSOps.Silent = true
		fList = #()
		ATSOps.Refresh()
		ATSOps.GetFilesByFileSystemStatus #Missing &fList
		fixTextureATSOpsPaths fList
		for i = 1 to fList.count do
			(
				inc +=1
				XrefProgress.doit_prog.value = 100.*inc /fList.count
			f = fList[i]
			if (ATSOps.IsInputFile f) then 
				(
				-- check if a bitmap type/or not a max file
				ATSOps.SelectFiles f
					
				--selFile = #()
				--ATSOps.GetSelectedFiles &selFile
				fixTextureATSOpsPaths f
				--print selFile
				--ATSOps.SetPathOnSelection "c:\\test"
				)
			)
	)		
	
	-------------------------------------------------------------
	--		Upadate the material slots
	-------------------------------------------------------------
	mapped fn updatemat mat =
	(
		updateMTLInMedit mat
	)
	
	-------------------------------------------------------------
	--		Recursive Find Shader on colleciton
	-------------------------------------------------------------
	mapped fn findShader mat =
	(	
		print mat
		local submaterial = (getNumSubMtls mat)
		
		if submaterial == 0 then (collectTextures mat)

				for i = 1 to submaterial do
				(
					submat = getSubMtl mat i
					if submat != undefined then getshader submat
					--getshader submat
				)
		
	)
	
	-------------------------------------------------------------
	--		Main Find Missing Xrefs/texture Functions
	-------------------------------------------------------------
	
	fn processXrefs =
	(
		try (destroyDialog XrefProgress) catch ()
		createDialog XrefProgress style:#(#style_toolwindow)
		
		local xreflist = objXRefs.getAllXRefObjects()	

		inc = 0
		for xref in objects where classof(xref) == xrefobject do
			(
				inc +=1
				XrefProgress.doit_prog.value = 100.*inc /xreflist.count
				oldpath = xref.srcFileName
				newPath = substituteString (tolower oldpath) (tolower Rollout_MigrateTools.edttext1.text) (tolower Rollout_MigrateTools.edttext2.text)
				xref.srcFileName = newPath
			
				-- Bring over the materail if its resolved	
				if xref.unresolved == false then
				(
					try(xref.material.srcFileName = newPath)catch()
				)
				else
				(
					print "Failed to find xref at this path: "+ newPath +" Reverting to Old Path"
					xref.srcFileName = oldpath
					try(xref.material.srcFileName = oldpath)catch()
				)
			)
		try (destroyDialog XrefProgress) catch ()
		
	)
	
	fn processTexs =
	(
		
		try (destroyDialog XrefProgress) catch ()
		createDialog XrefProgress style:#(#style_toolwindow)
		--scanAssetTracking()
		inc = 0
		
		for m in getClassInstances BitmapTexture do 
			(
				inc +=1
				XrefProgress.doit_prog.value = 100.*inc /(getClassInstances BitmapTexture).count
				--print m.filename
				fixTextureName m
			)
		--materials = for mat in sceneMaterials  collect mat

		--findShader materials
		--updatemat materials

		try (destroyDialog XrefProgress) catch ()
		
	)
	
	----------------------------------------------------
	--		Saved History Functions
	----------------------------------------------------
	
	fn savehistory =
	(
		for i = 1 to filehistory do
		(
			if Rollout_MigrateTools.edttext1_dd.items[i] != undefined then
			(
			setINISetting "c:/rs_utils.ini" "migratetools" ("replacehistory"+i as string) (Rollout_MigrateTools.edttext1_dd.items[i])
			)
			else	setINISetting "c:/rs_utils.ini" "migratetools" ("replacehistory"+i as string) ""

			
		)
		for i = 1 to filehistory do
		(
			if Rollout_MigrateTools.edttext2_dd.items[i] != undefined then
			(
			setINISetting "c:/rs_utils.ini" "migratetools" ("withhistory"+i as string) (Rollout_MigrateTools.edttext2_dd.items[i])
			)
			else	setINISetting "c:/rs_utils.ini" "migratetools" ("withhistory"+i as string) ""
			
		)
	)
	
	fn loadhistory =
	(
		local currentlist = #()
		for i = 1 to filehistory do
		(
			local item =getINISetting "c:/rs_utils.ini" "migratetools" ("replacehistory"+i as string)
			if item != "" then append currentlist item
		)
		Rollout_MigrateTools.edttext1_dd.items = currentlist
		currentlist = #()
		for i = 1 to filehistory do
		(
			local item =getINISetting "c:/rs_utils.ini" "migratetools" ("withhistory"+i as string)
			if item != "" then append currentlist item
		)
		Rollout_MigrateTools.edttext2_dd.items = currentlist
	)
	

	----------------------------------------------------
	--		Update Missing List Functions
	----------------------------------------------------
	
	fn maps mapfile =
	(
	local mapfileN=mapfile as name
	local index=finditem mapfiles mapfileN
	if index == 0 do append mapfiles mapfileN
	)
/*
	fn updateglobalmissingTex =
	(
		for m in getClassInstances BitmapTexture do 
			(
				print m.filename
				fixTextureName m
			)
	)
	*/
	
	fn updateglobalmissingTex =
	(
		mapfiles=#()		
		enumeratefiles maps #missing
		sort mapfiles
		Rollout_MigrateTools.lbx1.items = mapfiles
		local missingfiles = ("Missing Textures: "+mapfiles.count as string)
		Rollout_MigrateTools.lbx1.text = missingfiles
		mapfiles
	)
	
	
	fn updateglobalmissngXrefs =
	(
		xreflist = objXRefs.getAllXRefObjects()
		local missingxrefs = #()
		for xref in xreflist do
			(
				if xref.unresolved == true then append missingxrefs xref.filename
			)
		Rollout_MigrateTools.lbx1.text = ("Missing Xrefs: "+missingxrefs.count as string)
		Rollout_MigrateTools.lbx1.items  = missingxrefs
			
	)
	fn updatemissing =
	(
		if Rollout_MigrateTools.ckb1.checked then updateglobalmissingTex() else updateglobalmissngXrefs()
	)
	fn processall =
	(
		for pathN in Rollout_MigrateTools.edttext1_dd.items do
		(			
			Rollout_MigrateTools.edttext1.text = pathN
			processXrefs()
			processTexs()
		)
	)
	fn filtermapPaths =
	(
		props = #()
		interiors = #()
		maps = #()
		
		for eachfile in my_files do
		(
			testpaths = filterString (eachfile as string) "/"
			for eachname in testpaths do
			(
				if tolower eachname == "props" then append props eachfile
				if tolower eachname == "interiors" then append props interiors
			)
		)
	)
	----------------------------------------------------
	--		Get recusrive max files from dir
	----------------------------------------------------
	fn getMaxFilesRecursive dir =
	(
		join my_files (getFiles ( dir + "/*.max"))			
		dir_array = GetDirectories (dir+"/*")	
		if Rollout_MigrateTools.chk1.checked then
			for d in dir_array do	
			(
				getMaxFilesRecursive d
			)
	)
	--////////////////////////////////////////////////////////////
	-- INTERFACE
	--////////////////////////////////////////////////////////////
	
	group "Replace"
	(
	editText edttext1 "" text:"x:\art" pos:[6,18] width:154 height:17
	dropDownList edttext1_dd "" pos:[192,18] width:152 height:21
	button btn2 "+" pos:[161,18] width:21 height:21
	button btn6 "-" pos:[344,18] width:21 height:21
	)
	group "With"
	(
	editText edttext2 "" text:"x:\jimmy\jimmy_art" pos:[6,62] width:154 height:17
	dropDownList edttext2_dd "" pos:[192,62] width:152 height:21
	button btn3 "+" pos:[161,62] width:21 height:21
	button btn7 "-" pos:[344,62] width:21 height:21
	)
	
	button btn1 "Process" align:#left width:80 height:24
	button btn12 "Process All" pos:[94,94] width:80 height:24 
	button btn13 "Process Batch" pos:[174,94] width:80 height:24 
	checkbox chk1 "Recursive" pos:[256,104] checked:false
	checkButton ckb1 "Textures" pos:[216,120] width:80 height:16 checked:true
	checkButton ckb2 "Xrefs" pos:[296,120] width:80 height:16
	listBox lbx1 "Missing Textures:                           " pos:[9,120] width:367 height:28
	button btn5 "Refresh Current Scene" align:#left across:2
	button btn4 "Save to CSV" align:#right
	checkbutton btn11 "+" pos:[273,512] width:21 height:21 checked:true
	button btn10 "ClearHistory" pos:[290,89]  width:90 height:18 --pos:[194,94]
	
	--////////////////////////////////////////////////////////////
	-- EVENTS
	--////////////////////////////////////////////////////////////
	
	on btn2 pressed do
	(
		local currentlist = edttext1_dd.items
		if currentlist.count < 11 then appendifunique currentlist edttext1.text else currentlist[10] =edttext1.text
		edttext1_dd.items = currentlist
		edttext1_dd.selection = currentlist.count
	)
	on btn6 pressed do
	(
		local currentitem = edttext1_dd.selected
		local newlist = #()
		for i in  edttext1_dd.items do
		(
			if i != currentitem then append newlist i
		)
		edttext1_dd.items = newlist
	)
	-------------------------------------
	on btn3 pressed do
	(
		local currentlist = edttext2_dd.items
		if currentlist.count < 11 then appendifunique currentlist edttext2.text else currentlist[10] =edttext2.text		
		edttext2_dd.items = currentlist
		edttext2_dd.selection = currentlist.count
	)
	on btn7 pressed do
	(
		local currentitem = edttext2_dd.selected
		local newlist = #()
		for i in  edttext2_dd.items do
		(
			if i != currentitem then append newlist i
		)
		edttext2_dd.items = newlist
	)

	
	on ckb2 changed state do		
	(
		if ckb2.checked == true then ckb1.checked = false else ckb2.checked =true		
		updateglobalmissngXrefs()

	)
	
	on ckb1 changed state do
	(
		if ckb1.checked == true then ckb2.checked = false else ckb1.checked =true
		updateglobalmissingTex()
		--Rollout_MigrateTools.lbx1.text = "Missing Textures:"
	)
	-------------------------------------
	on btn5 pressed do
	(
		if ckb1.checked then updateglobalmissingTex() else updateglobalmissngXrefs()
	)
	-------------------------------------
	on btn4 pressed do
	(
		if btn11.checked then type="ab" else type="wb"
		f = getSaveFileName caption:"Open A Test File:" filename:"MissngTextures.csv"  types:"Comma Seperated (*.csv)|*.csv"		
		if f != undefined then 
			(
				saveFile = openfile f mode:type		
				if saveFile != undefined then
				(
					for i in lbx1.items do format (i as string+","+maxfilename+"\n") to:saveFile
					
				)
				close saveFile
			)

	)
	-------------------------------------
	on btn10 pressed do
	(
		edttext2_dd.items = #()
		edttext1_dd.items = #()
	)
	
	on Rollout_MigrateTools open do
	(
		try(Rollout_MigrateTools.edttext1.text = getINISetting "c:/rs_utils.ini" "migratetools" "default1")catch()
		try(Rollout_MigrateTools.edttext2.text = getINISetting "c:/rs_utils.ini" "migratetools" "default2")catch()
		try(if (getINISetting "c:/rs_utils.ini" "migratetools" "processTextures") as booleanClass then 
			(ckb1.checked = true;ckb2.checked = false;)
			else (ckb1.checked = false; ckb2.checked = true))catch()
		--try()
		
		loadhistory()
		Invalidfiles = #()
		updatemissing()
		
	)
	on Rollout_MigrateTools close do
	(
		setINISetting "c:/rs_utils.ini" "migratetools" "default1" (Rollout_MigrateTools.edttext1.text)
		setINISetting "c:/rs_utils.ini" "migratetools" "default2" (Rollout_MigrateTools.edttext2.text)
		setINISetting "c:/rs_utils.ini" "migratetools" "processTextures" (ckb1.checked as string)
		
		savehistory()
	)
	
	on edttext1_dd selected i do
	(
		edttext1.text = edttext1_dd.items[i]
	)
	
	on edttext2_dd selected i do
	(
		edttext2.text = edttext2_dd.items[i]
	)
	
	on btn1 pressed do
	(
		Invalidfiles = #()
		if ckb1.checked then
		(
			if edttext1.text !="" and edttext2.text != "" then		
			(
				clearlistener()	
				processTexs()
			)
		)
		else
		(
			clearlistener()
			processXrefs()
		)
		updatemissing()
		
	)
	on btn12 pressed do
	(
		processall()
		updatemissing()
	)
	on btn13 pressed do
	(
		f = getSavePath caption:"Choose Folder to find Max files" initialDir:initialDir
		print f
		if f != undefined then
		(
		my_files =#()
		getMaxFilesRecursive f
		
		--print ("files: "+ my_files as string)
		if ckb2.checked == true then ckb2.checked = false;ckb1.checked = true
		for eachfile in my_files do
			(
				--print eachfile
				local errorlist = #()
				loadmaxfile eachfile quiet: true missingDLLsList:&errorlist
				if CheckForSave() == true then
				(
				processall()
				saveMaxFile (maxfilepath+maxfilename)
				result = updateglobalmissingTex()
				print result
				if doesFileExist "c:\\batchlog.csv" == false then createFile "c:\\batchlog.csv"
				saveFile = openfile "c:\\batchlog.csv" mode:"ab"	
				for i in result do format (i as string+","+maxfilename+"\n") to:saveFile	
				close saveFile			
				
				)
				else
				(
					print "can not save file. Skipping"
				)
				
			)
			
	
		)

		messagebox "Log file saved to: c:\\batchlog.csv"
			
		
	)
	
)


createDialog Rollout_MigrateTools