
global RSL_XrefMatToLibMat

filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_dotNetUIOps.ms")

struct RSL_XrefMatToLibMatOps
(
	missingArray = #(),
	skippedArray = #(),
	
	missingForm,
	skippedForm,
	
	fn findInLibrary name =
	(
		local result
		
		for entry in currentMaterialLibrary do 
		(
			if entry.name == name then
			(
				result = entry
			)
		)
		
		result
	),
	
	fn isSceneMaterial objectArray =
	(
		local result = true
		
		for object in objectArray do 
		(
			if (classOf object) == XRefObject then
			(
				result = false
			)
		)
		
		result
	),
	
	fn replaceFromLibrary material =
	(
-- 		format "Material:%\n" material.name
		case (classOf material) of
		(
			XRef_Material:
			(
				local libraryMaterial = findInLibrary material.srcItemName
				local nodeArray = refs.dependentNodes material
				local sceneMaterial = isSceneMaterial nodeArray
				
				if libraryMaterial != undefined and sceneMaterial then
				(
-- 					format "\tSrcName:%\n\tLibMat:%\n" material.srcItemName libraryMaterial.name
					for object in nodeArray where (classOf object.material) == XRef_Material do 
					(
-- 						format "\tObject:%\n" object.name
						object.material = libraryMaterial
					)
				)
				else
				(
					if sceneMaterial then
					(
						append missingArray material
					)
					else
					(
						append skippedArray material
					)
				)
			)
			Multimaterial:
			(
				for i = 1 to material.materialList.count do 
				(
					local subMaterial = material.materialList[i]
					
					if subMaterial != undefined and (classOf subMaterial) == XRef_Material then
					(
						local libraryMaterial = findInLibrary subMaterial.srcItemName
						
						local nodeArray = refs.dependentNodes subMaterial
						local sceneMaterial = isSceneMaterial nodeArray
						
						if libraryMaterial != undefined and sceneMaterial then
						(
-- 							format "\tSrcName:% tLibMat:%\n" subMaterial.srcItemName libraryMaterial.name
							material.materialList[i] = libraryMaterial
						)
						else
						(
							if sceneMaterial then
							(
								append missingArray subMaterial
							)
							else
							(
								append skippedArray material
							)
						)
					)
				)
			)
		)
	),
	
	fn dispatchMaterialListDoubleClick s e =
	(
		RSL_XrefMatToLibMat.materialListDoubleClick s e
	),
	
	fn materialListDoubleClick s e =
	(
		local index = s.SelectedIndices.Item[0] + 1
		local array = s.TopLevelControl.tag.value
		local objectArray = refs.dependentNodes array[index]
		
		meditMaterials[activeMeditSlot] = array[index]
		
		for object in objectArray do 
		(
			object.isHidden = false
		)
		
		select objectArray
	),
	
	fn dispatchLoadXrefClick s e =
	(
		RSL_RSL_XrefMatToLibMat.loadXrefClick s e
	),
	
	fn loadXrefClick s e =
	(
		local index = s.tag.value.SelectedIndices.Item[0] + 1
		local array = s.tag.value.TopLevelControl.tag.value
		
		if index > 0 then
		(
			local file = array[index].srcFileName
			local objectName = array[index].srcItemName
			
			if (doesFileExist file) then
			(
				if (checkForSave()) then
				(
					loadMaxFile file
					
					local object = getNodeByName objectName exact:true
					
					if object != undefined then
					(
						select object
						object.isHidden = false
						max zoomext sel
					)
					else
					(
						messageBox ("Unable to find object in the scene: " + objectName) title:"Error..."
					)
				)
			)
			else
			(
				messageBox (file + "\nThis file does not exist on your machine. Check perforce.") title:"Error..."
			)
		)
		else
		(
			messagebox "Please select a material from the list box." title:"Error..."
		)
	),
	
	fn createReport name title array =
	(
		reportForm = RS_dotNetUI.maxForm name text:title size:[256,460] min:[256,460] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		reportForm.tag = dotNetMXSValue Array
		
		materialList = RS_dotNetUI.listView "materialList" dockStyle:RS_dotNetPreset.DS_Fill
		materialList.view = (dotNetClass "System.Windows.Forms.View").List --Details
		materialList.HeaderStyle = (dotNetClass "System.Windows.Forms.ColumnHeaderStyle").Nonclickable
		materialList.fullRowSelect = true
		materialList.HideSelection = false
		materialList.columns.add "Xref Materials" 200
		dotnet.addEventHandler materialList "DoubleClick" dispatchMaterialListDoubleClick
		
		for material in array do 
		(
			materialList.Items.Add (dotNetObject "System.Windows.Forms.ListViewItem" material.name)
		)
		
		loadXrefButton = RS_dotNetUI.Button "loadXrefButton" text:(toUpper "Load Xref File") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		loadXrefButton.tag = dotNetMXSValue materialList
		dotnet.addEventHandler loadXrefButton "click" dispatchLoadXrefClick
		
		mainTable = RS_dotNetUI.tableLayout "mainTable" text:"mainTable" collumns:#((dataPair type:"Percent" value:50)) rows:#((dataPair type:"Percent" value:50), (dataPair type:"Absolute" value:24)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		mainTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		mainTable.Controls.Add materialList 0 0
		mainTable.Controls.Add loadXrefButton 0 1
		
		reportForm.Controls.Add mainTable
		
		reportForm.showModeless()
		
		reportForm
	),
	
	fn processScene =
	(
		if (queryBox "This will replace Scene Xref materials with Rage Shaders from the Current library.\n\nAre you sure you want to continue?" title:"Warning") then
		(
			local matLib = getOpenFileName caption:"Pick Material Library File..." types:"Material Library(*.mat)|*.mat"
			
			if matLib != undefined then
			(
				loadMaterialLibrary matLib
				
				for material in sceneMaterials do 
				(
					replaceFromLibrary material
				)
				
				if missingArray.count > 0 then
				(
					missingForm = createReport "missingForm" "Scene Xref materials missing from library" missingArray
				)
				
				if skippedArray.count > 0 then
				(
					skippedForm = createReport "skippedForm" "Skipped Materials assigned to Xref Objects" skippedArray
				)
			)
		)
	)
)

if RSL_XrefMatToLibMat != undefined then
(
	try
	(
		RSL_XrefMatToLibMat.missingForm.close()
		RSL_XrefMatToLibMat.skippedForm.close()
	)
	catch()
)

RSL_XrefMatToLibMat = RSL_XrefMatToLibMatOps()
RSL_XrefMatToLibMat.processScene()
