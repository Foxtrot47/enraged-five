
try (destroyDialog RSL_assignMiloRoom) catch()

rollout RSL_assignMiloRoom "MILO Room assigner"
(
	local MILORoomArray = #()
	local objectSelection = #()
	local unhiddenObjectArray = #()
	
	fn createMILORoomList =
	(
		MILORoomArray = for object in objects where (classOf object) == GtaMloRoom collect object
		
		RSL_assignMiloRoom.lbx_miloList.items = (for room in MILORoomArray collect room.name)
		
		RSL_assignMiloRoom.lbx_miloList.selection = 0
		
		RSL_assignMiloRoom.btn_assign.enabled = false
	)
	
	fn checkSelection =
	(
		local result = false
		
		for object in objectSelection do
		(
			if (getAttrClass object) == "Gta Object" then
			(
				result = true
			)
		)
		
		result
	)
	
	mapped fn displayMILOObjects object MILO =
	(
		local childIndex = findItem MILO.children object
		local selectedIndex = findItem objectSelection object
		
		if object != MILO and childIndex == 0 and selectedIndex == 0 then
		(
			object.isHidden = true
		)
		else
		(
			object.isHidden = false
		)
	)
	
	fn recurseFindMILO object =
	(
		local result = object.parent
		
		if result != undefined then
		(
			if (classOf result) != GtaMloRoom then
			(
				recurseFindMILO result
			)
		)
		
		result
	)
	
	fn findClosestRoom =
	(
		local center = selection.center
		local nearDistance = 9999999
		local nearest
		
		for object in objects do
		(
			local currentDistance = (distance object.pos center)
			local parent = object.parent
			
			if currentDistance < nearDistance and (classOf parent) == GtaMloRoom then
			(
				nearDistance = currentDistance
				nearest = parent
			)
		)
		
		local index = findItem MILORoomArray nearest
		
		RSL_assignMiloRoom.lbx_miloList.selection = index
		
		if index != 0 then
		(
			displayMILOObjects objects MILORoomArray[RSL_assignMiloRoom.lbx_miloList.selection]
			
			if (checkSelection()) then
			(
				RSL_assignMiloRoom.btn_assign.enabled = true
			)
			else
			(
				RSL_assignMiloRoom.btn_assign.enabled = false
			)
		)
		else
		(
			RSL_assignMiloRoom.btn_assign.enabled = false
		)
	)
	
	fn cb_RSL_assignMiloRoomSelectionChanged =
	(
		objectSelection = selection as array
		
		if (checkSelection()) then
		(
			RSL_assignMiloRoom.btn_assign.enabled = true
			
			RSL_assignMiloRoom.lbl_selectionReport.caption = "Valid Selection"
		)
		else
		(
			RSL_assignMiloRoom.btn_assign.enabled = false
			
			RSL_assignMiloRoom.lbl_selectionReport.caption = "Invalid Object Selected"
		)
		
		if objectSelection.count == 0 then
		(
			RSL_assignMiloRoom.btn_assign.enabled = false
			
			RSL_assignMiloRoom.lbl_selectionReport.caption = "No Objects Selected"
		)
	)
	
	
	listBox lbx_miloList "Rooms in scene..." height:16
	button btn_getClosest "Select Closest Room"
	button btn_assign "Assign Selected..."
	label lbl_selectionReport ""
	
	on RSL_assignMiloRoom open do
	(
		objectSelection = selection as array
		unhiddenObjectArray = for object in objects where not object.isHidden collect object
		
		if (checkSelection()) then
		(
			RSL_assignMiloRoom.btn_assign.enabled = true
			
			RSL_assignMiloRoom.lbl_selectionReport.caption = "Valid Selection"
		)
		else
		(
			RSL_assignMiloRoom.btn_assign.enabled = false
			
			RSL_assignMiloRoom.lbl_selectionReport.caption = "Invalid Object Selected"
		)
		
		if objectSelection.count == 0 then
		(
			RSL_assignMiloRoom.lbl_selectionReport.caption = "No Objects Selected"
		)
		
		callbacks.addScript #selectionSetChanged "RSL_assignMiloRoom.cb_RSL_assignMiloRoomSelectionChanged()" id:#RSL_assignMiloRoom_callBacks
		
		createMILORoomList ()
	)
	
	on RSL_assignMiloRoom close do
	(
		callbacks.removeScripts id:#RSL_assignMiloRoom_callBacks
	)
	
	on lbx_miloList selected index do
	(
		if index != 0 then
		(
			displayMILOObjects objects MILORoomArray[lbx_miloList.selection]
			
			if (checkSelection()) then
			(
				RSL_assignMiloRoom.btn_assign.enabled = true
			)
			else
			(
				RSL_assignMiloRoom.btn_assign.enabled = false
			)
		)
		else
		(
			RSL_assignMiloRoom.btn_assign.enabled = false
		)
	)
	
	on btn_getClosest pressed do
	(
		local index = findClosestRoom()
	)
	
	on btn_assign pressed do
	(
		for object in objectSelection where (getAttrClass object) == "Gta Object" do
		(
			object.parent = MILORoomArray[lbx_miloList.selection]
		)
	)
)

createDialog RSL_assignMiloRoom style:#(#style_toolwindow,#style_sysmenu)