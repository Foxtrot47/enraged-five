try (destroyDialog RSL_ParticleEmitters) catch()

rollout RSL_ParticleEmitters "R* Particle Emitters" width:240
(
	local entityFX = RsConfigGetCommonDir() + "/data/effects/entityFx.dat"
	local FXArray = #("undefined")
	local selectedArray = #()
	
	local selectedDummyObj
	local checkBoxArray = #()
	local creationDirection = "chk_y"
	
	fn readEntityFX =
	(
		dataStream = openFile entityFX
		
		if dataStream != undefined then
		(
			while not (eof dataStream) do
			(
				local line = (readLine dataStream)
				
				if line[1] != "#" then
				(
					local filtered = (filterString line "\t")
					
					if filtered.count > 1 then
					(
						append FXArray filtered[1]
					)
				)
			)
			
			close dataStream
		)
	)
	
	-- Callback function for when the user selects an object in the scene.
	fn UpdateObj =
	(
		-- Make sure we are only selecting one object.
		if (selection.count == 1) then
		(
			local selObj = selection[1]
			
			-- Make sure the object is a dummy and has the proper 'AnimParticle' user property.
			if (classof(selObj) == Dummy) then
			(
				selectedDummyObj = selObj
				local pType = getUserProp selObj "AnimParticle"
				
				if (pType != undefined) then
				(
					-- Update the UI with the dummies particle time and name.
					local pTime = getUserProp selObj "AnimParticleTime"
					local pName = getUserProp selObj "AnimParticleName"
					
					if (pName != undefined) do
					(
						local index = findItem FXArray pName
						
						if index == 0 then
						(
							index = 1
						)
						
						RSL_ParticleEmitters.ddl_fxName.selection = index
					)
					
					if (pTime != undefined) do
						RSL_ParticleEmitters.spn_start.value = pTime
				)
				-- If it's not a dummy object then clear out the UI as to not confuse people.
				else
				(
					selectedDummyObj = undefined
					RSL_ParticleEmitters.spn_start.value = 0.0
					RSL_ParticleEmitters.ddl_fxName.selection = 1
				)
			)
			-- If it's not a dummy object then clear out the UI as to not confuse people.
			else
			(
				selectedDummyObj = undefined
				RSL_ParticleEmitters.spn_start.value = 0.0
				RSL_ParticleEmitters.ddl_fxName.selection = 1
			)
		)
		-- If it's not a dummy object then clear out the UI as to not confuse people.
		else
		(
			selectedDummyObj = undefined
			RSL_ParticleEmitters.spn_start.value = 0.0
			RSL_ParticleEmitters.ddl_fxName.selection = 1
		)
	)
	
	fn updateSelection =
	(
		if selection.count > 0 then
		(
			selectedArray = for object in selection where (classOf object) == dummy and (getUserProp object "AnimParticle") != undefined collect object
			
			if selectedArray.count > 0 then
			(
				case of
				(
					(selectedArray.count == 1):
					(
						local pTime = getUserProp selectedArray[1] "AnimParticleTime"
						local pName = getUserProp selectedArray[1] "AnimParticleName"
						
						if (pName != undefined) do
						(
							local index = findItem FXArray pName
							
							if index == 0 then
							(
								index = 1
							)
							
							RSL_ParticleEmitters.ddl_fxName.selection = index
						)
						
						if (pTime != undefined) do
						(
							RSL_ParticleEmitters.spn_start.value = pTime
						)
					)
					(selectedArray.count > 1):
					(
						local timeArray = #()
						local typeArray = #()
						
						for particleHelper in selectedArray do
						(
							appendIfUnique timeArray (getUserProp selectedArray[1] "AnimParticleTime")
							appendIfUnique typeArray (getUserProp selectedArray[1] "AnimParticleName")
						)
						
						if timeArray.count == 1 then
						(
							RSL_ParticleEmitters.spn_start.value = timeArray[1]
						)
						else
						(
							RSL_ParticleEmitters.spn_start.value = 0.0
						)
						
						if typeArray.count == 1 then
						(
							local index = findItem FXArray typeArray[1]
							
							RSL_ParticleEmitters.ddl_fxName.selection = index
						)
						else
						(
							RSL_ParticleEmitters.ddl_fxName.selection = 1
						)
					)
				)
			)
			else
			(
				RSL_ParticleEmitters.spn_start.value = 0.0
				RSL_ParticleEmitters.ddl_fxName.selection = 1
			)
		)
		else
		(
			selectedArray = #()
		)
	)
	
	-- Find all the dummy objects in the scene. (Used to gather all dummies to be saved)
	fn gatherEmitters =
	(
		local helperList = helpers as array
		local dummyList = #()
		
		for pHelper in helperList do
		(
			local pType = getUserProp pHelper "AnimParticle"
			
			if (pType != undefined) then
				append dummyList pHelper
		)
		
		return dummyList
	)
	
	fn setCheckState checkBox =
	(
		checkBox.checked = true
		creationDirection = checkBox.name
		
		for control in checkBoxArray where control.name != checkBox.name do
		(
			control.checked = false
		)
	)
	
	fn setDirection emitter object =
	(
		case creationDirection of
		(
			default:print creationDirection
			"chk_x":
			(
				local dir = [1,0,0]
				emitter.dir = dir
			)
			"chk_y":
			(
				local dir = [0,1,0]
				emitter.dir = dir
			)
			"chk_z":
			(
				local dir = [0,0,1]
				emitter.dir = dir
			)
			"chk_dir":
			(
				local pos1 = at time sliderTime object.pos
				local pos2 = at time (sliderTime + 1) object.pos
				
				local dir = normalize (pos2 - pos1)
				
				emitter.dir = dir
			)
		)
	)
	
	fn create selected =
	(
		if selected then
		(
			for object in selection where (getAttrClass object) == "Gta Object" do
			(
				local position = object.pos
				newEmitter = dummy name:(uniqueName (object.name + "_PartAnim")) pos:position boxSize:[0.5, 0.5, 0.5] wireColor:red
				
				setDirection newEmitter object
				
				setUserProp newEmitter "AnimParticle" true
				setUserProp newEmitter "AnimParticleTime" sliderTime
				setUserProp newEmitter "AnimParticleName" "undefined"
			)
		)
		else
		(
			newEmitter = dummy name:(uniqueName "PartAnim") boxSize:[0.5, 0.5, 0.5] wireColor:red
			
			setUserProp newEmitter "AnimParticle" true
			setUserProp newEmitter "AnimParticleTime" sliderTime
			setUserProp newEmitter "AnimParticleName" "undefined"
		)
	)
	
	group "Emitter Properties"
	(
		dropdownList ddl_fxName "Effect Name: " --labelOnTop:true
		label lbl_startTime "Start Time: " offset:[-8,0] across:3
		spinner spn_start "" offset:[28,0] fieldwidth:50 range:[0, 1000, 0]
		button btn_set "Set" width:42 height:16 offset:[16,0]
	)
	
	group "Create"
	(
		checkBox chk_select "From Selection" checked:true
		checkBox chk_x "X:" across:4
		checkBox chk_y "Y:"
		checkBox chk_z "Z:" checked:true
		checkBox chk_dir "Velocity"
		button btn_create "Create" width:106 offset:[-3,0] across:2
		button btn_delete "Delete" width:106 offset:[3,0]
	)
	
	button btn_save "Save Emitters" width:230
	
	-- When the rollout opens, add a selection callback so we can update the UI when a user selects a particle dummy.
	on RSL_ParticleEmitters open do
	(
		checkBoxArray = #(chk_x, chk_y, chk_z, chk_dir)
		
		readEntityFX()
		ddl_fxName.items = FXArray
		
		callbacks.addscript #selectionSetChanged "RSL_ParticleEmitters.updateSelection()" id:#UpdatePartAnimObj
	)
	
	-- When the rollout closes, clear the selection callback.
	on RSL_ParticleEmitters close do
	(
		callbacks.removescripts id:#UpdatePartAnimObj
	)
	
	-- When the user edits the particle name, update the user property for the dummy.
	on ddl_fxName selected index do
	(
		if (selectedArray.count > 0) then
		(
			for particleHelper in selectedArray do
			(
				setUserProp particleHelper "AnimParticleName" ddl_fxName.selected
			)
		)
		else
		(
			print "undefined"
		)
	)
	
	-- Set the current animation slider time to the particle dummy.
	on btn_set pressed  do
	(
		if (selectedArray.count > 0) then
		(
			for particleHelper in selectedArray do
			(
				setUserProp particleHelper "AnimParticleTime" sliderTime
			)
			
			RSL_ParticleEmitters.spn_start.value = sliderTime
		)
	)
	
	on chk_x changed state do
	(
		setCheckState chk_x
	)
	
	on chk_y changed state do
	(
		setCheckState chk_y
	)
	
	on chk_z changed state do
	(
		setCheckState chk_z
	)
	
	on chk_dir changed state do
	(
		setCheckState chk_dir
	)
	
	on btn_create pressed do
	(
		create chk_select.checked
	)
	
	-- Deletes a dummy object.
	on btn_delete pressed  do
	(
		if (selection.count > 0) then
		(
			for obj in selection do
			(
				-- Only delete the object if it's a dummy and it has the proper object properties
				if (classof(obj) == Dummy) then
				(
					local pType = getUserProp obj "AnimParticle"
					
					if (pType != undefined) then
						delete obj
				)
			)
		)
	)
	
	-- Save the dummies in the scene out to a text file that can be given to the scripters.
	on btn_save pressed  do
	(
		local dummyList = gatherEmitters()
		local scriptFile = createfile "C:\ParticleScriptHelpers.txt"
		
		for pDummy in dummyList do
		(
			local pName = getUserProp pDummy "AnimParticleName"
			local pTime = getUserProp pDummy "AnimParticleTime"
			
			-- Convert the time to milliseconds so it's easier for scripters to read.
			pTime = (pTime / frameRate) * 1000
			
			local position = pDummy.position
			local rotation = pDummy.rotation
			
			-- Write the actual dummy information into the file.
			-- Written to a certain degree that the scripter can copy and paste the particle spawn call.
			format "---------------------------------------------------------------------------------------\n" to:scriptFile
			format "START_PARTICLE_FX_NON_LOOPED_AT_COORD(\"%\", <<%, %, %>>, <<%, %, %>>, 1.0)\n" pName position.x position.y position.z rotation.x rotation.y rotation.z to:scriptFile
			format "Particle Start Time (Milliseconds): %\n" pTime to:scriptFile
			format "---------------------------------------------------------------------------------------\n\n" to:scriptFile
		)
		
		close scriptFile
	)
)

createDialog RSL_ParticleEmitters style:#(#style_toolwindow,#style_sysmenu)