plugin simpleManipulator IPLStreamingBox
name:"IPL Box"
classID:#(0x4190c5be, 0x564bb4a4)
category:"Manipulators"
(
	fn transformMesh theMesh theTM =
	(
		for v = 1 to theMesh.numverts do 
			setVert theMesh v ((getVert theMesh v)*theTM)
		theMesh	
	)
	
	parameters main rollout:paramRollout
	(
		u32BoxID type:#string animatable:false ui:mapName default:""
		top type:#float animatable:false ui:spnTop default: 1.0
		bottom type:#float animatable:false ui:spnBottom default: -1.0
		left type:#float animatable:false ui:spnLeft default: -1.0
		right type:#float animatable:false ui:spnRight default: 1.0
		front type:#float animatable:false ui:spnFront default: 1.0
		back type:#float animatable:false ui:spnBack default: -1.0
		
		on top set val do this.topleft.z = val
		on bottom set val do this.botright.z = val
		on left set val do this.topleft.x = val
		on right set val do this.botright.x = val
		on front set val do this.topleft.y = val
		on back set val do this.botright.y = val
		
		topleft type:#point3 default:[-1.0, 1.0, 1.0]
		botright type:#point3 default:[1.0, -1.0, -1.0]
		linkdata type:#stringTab tabSize:0 tabSizeVariable:true animatable:false
		
		colourRed type:#point3 default:[1,0,0]
		colourGreen type:#point3 default:[0,1,0]
		colourChild type:#point3 default:[0,0,1]
		colourChildSelected type:#point3 default:[0.5,0.5,1]
		colorParent type:#point3 default:[1,0,0]
		colourGold type:#point3 default:[1.0,0.8,0]
		colourBase type:#point3 default:((color 214 229 166) / 255.0)
		colourCurrent type:#point3 default:((color 214 229 166) / 255.0)
	)
	
	rollout paramRollout "Parameters"
	(
		Edittext mapName "Map:" text:"" readOnly:true
		Spinner spnTop "Top:" range:[0.0, 1000.0, 1.0] scale:0.01
		Spinner spnBottom "Bottom:" range:[-1000.0, 0.0, -1.0] scale:0.01
		Spinner spnLeft "Left:" range:[-1000.0, 0.0, -1.0] scale:0.01
		Spinner spnRight "Right:" range:[0.0, 1000.0, 1.0] scale:0.01
		Spinner spnFront "Front:" range:[0.0, 1000.0, 1.0] scale:0.01
		Spinner spnBack "Back:" range:[-1000.0, 0.0, -1.0] scale:0.01
	)
	
	on canManipulate target return (classOf target) == IPLStreamingBox
	
	tool create
	(
		on mousePoint click do
		(
			nodeTM.translation = worldPoint
			#stop
		)
	)
	
	on updateGizmos do
	(
		for o in refs.dependents this where isProperty o #wirecolor do o.wirecolor = colourCurrent * 255
		
		this.clearGizmos()
		
		if (target != undefined) then
		(
			this.u32BoxID = target.u32BoxID 
			this.top = target.top
			this.bottom = target.bottom
			this.left = target.left
			this.right = target.right
			this.front = target.front
			this.back = target.back
			
			this.topleft = target.topleft 
			this.botright = target.botright 
			this.linkdata = target.linkdata
			
			this.colourRed = target.colourRed
			this.colourGreen = target.colourGreen
			this.colourChild = target.colourChild
			this.colourChildSelected = target.colourChildSelected
			this.colorParent = target.colorParent
			this.colourGold = target.colourGold
			this.colourBase = target.colourBase
			this.colourCurrent = target.colourCurrent
		)
		
		local tbCenter = (this.top + this.bottom) / 2
		local lrCenter = (this.left + this.right) / 2
		local fbCenter = (this.front + this.back) / 2
		
		local tbWidth = (this.top - this.bottom)  * 0.125
		local lrWidth = (this.right - this.left) * 0.125
		local fbWidth = (this.front - this.back) * 0.125
		
		
		
		-- gizmo 0
		
		local boxGiz = manip.makeBox [lrCenter, fbCenter, tbCenter] (this.front - this.back) (this.right - this.left) (this.top - this.bottom) 1 1 1
		
		this.addGizmoMesh boxGiz gizmoDontHitTest colourCurrent colourRed 
		
		-- gizmo 1
		local topManipGiz = (createInstance Pyramid width:2 depth:2 height:2).mesh
		local topTM = matrix3 1
		topTM.row4 = [lrCenter, fbCenter, this.top]
		topManipGiz = transformMesh topManipGiz topTM
		
		this.addGizmoMesh topManipGiz 0 colourGold colourRed 
		
		-- gizmo 2
		local bottomManipGiz = (createInstance Pyramid width:2 depth:2 height:2).mesh
		local bottomTM = rotateXMatrix 180
		bottomTM.row4 = [lrCenter, fbCenter, this.bottom]
		bottomManipGiz = transformMesh bottomManipGiz bottomTM
		
		this.addGizmoMesh bottomManipGiz 0 colourGold colourRed 
		
		-- gizmo 3
		local leftManipGiz = (createInstance Pyramid width:2 depth:2 height:2).mesh
		local leftTM = rotateYMatrix -90
		leftTM.row4 = [this.left, fbCenter, tbCenter]
		leftManipGiz = transformMesh leftManipGiz leftTM
		
		this.addGizmoMesh leftManipGiz 0 colourGold colourRed 
		
		-- gizmo 4
		local rightManipGiz = (createInstance Pyramid width:2 depth:2 height:2).mesh
		local rightTM = rotateYMatrix 90
		rightTM.row4 = [this.right, fbCenter, tbCenter]
		rightManipGiz = transformMesh rightManipGiz rightTM
		
		this.addGizmoMesh rightManipGiz 0 colourGold colourRed 
		
		-- gizmo 5
		local frontManipGiz = (createInstance Pyramid width:2 depth:2 height:2).mesh
		local frontTM = rotateXMatrix -90
		frontTM.row4 = [lrCenter, this.front, tbCenter]
		frontManipGiz = transformMesh frontManipGiz frontTM
		
		this.addGizmoMesh frontManipGiz 0 colourGold colourRed 
		
		-- gizmo 6
		local backManipGiz = (createInstance Pyramid width:1 depth:1 height:1).mesh
		local backTM = rotateXMatrix 90
		backTM.row4 = [lrCenter, this.back, tbCenter]
		backManipGiz = transformMesh backManipGiz backTM
		
		this.addGizmoMesh backManipGiz 0 colourGold colourRed 
		
		-- gizmo 7
		this.addGizmoMarker #hollowBox this.topleft gizmoDontHitTest colourGold colourRed
		-- gizmo 8
		this.addGizmoMarker #hollowBox this.botRight gizmoDontHitTest colourGold colourRed
	)
	
	on mouseMove mPos which do
	(
		local projectedPoint = [0,0,0]
		local viewRay = this.getLocalViewRay mPos
		
		case which of
		(
			1:
			(
				local pl = manip.makePlaneFromNormal (Inverse (getViewTM())).row3 [(this.left + this.right) / 2, (this.front + this.back) / 2, this.top]
				local result = pl.intersect viewRay &projectedPoint
				
				if projectedPoint.z < 1.0 then
				(
					projectedPoint.z = 1.0
				)
				
				target.top = projectedPoint.z
			)
			2:
			(
				local pl = manip.makePlaneFromNormal (Inverse (getViewTM())).row3 [(this.left + this.right) / 2, (this.front + this.back) / 2, this.bottom]
				local result = pl.intersect viewRay &projectedPoint
				
				if projectedPoint.z > -1.0 then
				(
					projectedPoint.z = -1.0
				)
				
				target.bottom = projectedPoint.z
			)
			3:
			(
				local pl = manip.makePlaneFromNormal (Inverse (getViewTM())).row3 [this.left, (this.front + this.back) / 2, (this.top + this.bottom) / 2]
				local result = pl.intersect viewRay &projectedPoint
				
				if projectedPoint.x > -1.0 then
				(
					projectedPoint.x = -1.0
				)
				
				target.left = projectedPoint.x
			)
			4:
			(
				local pl = manip.makePlaneFromNormal (Inverse (getViewTM())).row3 [this.right, (this.front + this.back) / 2, (this.top + this.bottom) / 2]
				local result = pl.intersect viewRay &projectedPoint
				
				if projectedPoint.x < 1.0 then
				(
					projectedPoint.x = 1.0
				)
					
				target.right = projectedPoint.x
			)
			5:
			(
				local pl = manip.makePlaneFromNormal (Inverse (getViewTM())).row3 [(this.left + this.right) / 2, this.front, (this.top + this.bottom) / 2]
				local result = pl.intersect viewRay &projectedPoint
				
				if projectedPoint.y < 1.0 then
				(
					projectedPoint.y = 1.0
				)
					
				target.front = projectedPoint.y
			)
			6:
			(
				local pl = manip.makePlaneFromNormal (Inverse (getViewTM())).row3 [(this.left + this.right) / 2, this.back, (this.top + this.bottom) / 2]
				local result = pl.intersect viewRay &projectedPoint
				
				if projectedPoint.y > -1.0 then
				(
					projectedPoint.y = -1.0
				)
					
				target.back = projectedPoint.y
			)
		)
	)
	
-- 	on mouseDown mPos which do
-- 	(
-- 		
-- 	)
	
-- 	on mouseUp mPos which do
-- 	(
-- 		this.mouseStart = [0,0]
-- 	)
)