

try (destroyDialog RsParticleConverter) catch ()

vfx_List = #()	
--- Read back IDE FILE for particel settings
Clearlistener()filein "payne/pipeline/RSL_LevelConfig.ms"
--filein "pipeline/export/Config.ms"
----------------------------------------------------------------------------------------------
--								Main Functions
----------------------------------------------------------------------------------------------

--------------------------------------------------------------
-- NAME		:
-- PURPOSE	: Simple strcuct to hold 2dfx Particle Info
--------------------------------------------------------------
struct twoDFX
(
	NodeName,
	localPos = point3,
	EffectName = "",
	Trigger = 0,
	Attach = 0,
	AttachtoAll = false,
	Scale = 0,
	Probability = 100,
	Flags,
	HasTint = false,
	R =255,
	G =255,
	B =255,
	IgnoreDamageModel = false,
	PlayonParent = false,
	OnlyonDamageModel = false
)

--------------------------------------------------------------
-- NAME		:
-- PURPOSE	: Load all the valid PArticle entries
--------------------------------------------------------------

fn LoadEntries = 
	(	
	filename = ( RsConfigGetCommonDir() + "/data/effects/entityFx.dat" )
	intFile = openfile filename mode:"rb"
	newType = #none
	while eof intFile == false do 
		(
		intLine = RsRemoveSpacesFromStartOfString(readline intFile)			
		if intLine.count > 0 and intLine[1] != "#" then
			(
			intTokens = filterstring intLine " \t"
			newItem = intTokens[1]
			append vfx_List newItem
			)
		)
	close intFile
	)
	
--------------------------------------------------------------
-- NAME		:
-- PURPOSE	:Validate a PArticle Effect
--------------------------------------------------------------
fn ValidateEffectName effect =
(
	valid = false
	for eachName in vfx_List do
	(		
		if tolower effect == tolower eachName then
		(
			valid = true
		)
	)
	return valid
)

--------------------------------------------------------------
-- NAME		:
-- PURPOSE	:Strips the last number characters until we have a valid name
--------------------------------------------------------------

fn striptoBaseName effectname =
(
	eNameLength = effectname.count
	inc = 0
	while char != tolower "_" do
	(		
		char = effectname[eNameLength-inc]
		if (eNameLength-inc) == 1 then exit
		inc+=1
	)	
	effectname = substring effectname 1 (eNameLength-inc)
	
)



--------------------------------------------------------------
-- NAME		:
-- PURPOSE	:Fix PArticles based on the Node Name
--------------------------------------------------------------
fn FixParticles = (
	for i in helpers do
	(
		if classof i == RAGE_Particle then
		(
			--print i.name
			if getRageParticleAttr i then
			(
				
				effectname = striptoBaseName i.name
				if effectname == "" then effectname = i.name
				
				
				
				if ValidateEffectName effectname then
				(
					Print ("Updated Particle: "+effectname)
					setattr i 1 effectname	
				)
				else (print "No PAricle FOund" + i as string)
	
			)
		)
	)
)
	
--------------------------------------------------------------
-- NAME		:
-- PURPOSE	: Read the Particle Flags and set the apporaite attribute
--------------------------------------------------------------
fn GetParticleFlags Flags newParticle=
(
	-- From Rage
	--TWODFX_PARTICLE_HAS_TINT			= ( 1 << 0 ),
	--TWODFX_PARTICLE_IGNORE_DAMAGE_MODEL	= ( 1 << 1 ),
	--TWODFX_PARTICLE_PLAY_ON_PARENT = (1 << 2),
    --TWODFX_PARTICLE_ONLY_ON_DAMAGE_MODEL = (1 << 3)

	if bit.and flags (bit.shift 1 0) > 0 then (newParticle.HasTint = true	)
	if bit.and flags (bit.shift 1 1) > 0 then (newParticle.IgnoreDamageModel = true)
	if bit.and flags (bit.shift 1 2) > 0 then (newParticle.PlayonParent = true)
	if bit.and flags (bit.shift 1 3) > 0 then (newParticle.OnlyonDamageModel = true)

	
)	


--------------------------------------------------------------
-- NAME		:
-- PURPOSE	:Read the map IDE file
--------------------------------------------------------------

fn getmapIDEfilename =
(
	Levelconfig = GetLevelconfig()
	image = Levelconfig.image
	IDE_Path = project.independent+"\\levels\\"+Levelconfig.level+"\\"+Levelconfig.imagetype+"\\"+image+".ide"
	IDE_Path
	
)

--------------------------------------------------------------
-- NAME		:
-- PURPOSE	: Read back the 2dfx line args
--------------------------------------------------------------
fn Process2DFXline two2FXarray=
(
	if execute(trimLeft two2FXarray[5]) == 1 then
	(	
		newParticle = twoDFX()
		newParticle.NodeName = trimLeft two2FXarray[1]
		newParticle.localPos = [execute(trimLeft two2FXarray[2]),execute(trimLeft two2FXarray[3]),execute(trimLeft two2FXarray[4])]
		newParticle.EffectName = trimLeft two2FXarray[10]
		newParticle.Trigger = execute(trimLeft two2FXarray[11])
		newParticle.Scale = execute(trimLeft two2FXarray[13])
		newParticle.Probability =execute(trimLeft two2FXarray[14])
		newParticle.Flags = execute(trimLeft two2FXarray[15])
		newParticle.R = execute(trimLeft two2FXarray[16])
		newParticle.G = execute(trimLeft two2FXarray[17])
		newParticle.B= execute(trimLeft two2FXarray[18])
		GetParticleFlags newParticle.Flags newParticle
		return newParticle
	)
	else return false
	
)


--------------------------------------------------------------
-- NAME		:
-- PURPOSE	:Read the IDE file
--------------------------------------------------------------
fn processIDE filename=
(
	print filename
	local IDE2dfx = #()
	-- Example Line:
	-- Aprt6_Books, 0, -9.536743E-07, 0, 1, 0, 0, 0, -1, DST_PAPER_PLAIN_S, 4, 0, 0.85, 100, 1, 210, 210, 206
	filestream = openFile filename
	skipToString filestream "2dfx"
	local twoDFXline = Readline filestream
	while twoDFXline != "end" do
	(
		twoDFXline = Readline filestream
		twoDFXarray = filterString twoDFXline ","
		-- Check if line relates to a particle effect. Quick check is number of args though we could check the flag which would be "1"
		if twoDFXline != "end" then -- Seems to skip the while too loop
		(
		local valid2dfx = Process2DFXline twoDFXarray
		if valid2dFX != false then append IDE2dfx(valid2dFX)
		)

	)
	close filestream
	return IDE2dfx

)

--------------------------------------------------------------
-- NAME		:
-- PURPOSE	:Check input and Obj pos
--------------------------------------------------------------
fn testObjPos Inpos TestPos=
(
	local valid = false
	if Inpos == TesPos then
	(
		valid = True
	)
	else
	(
	if close_enough Inpos.x TestPos.x 10000.0 then
		if close_enough Inpos.y TestPos.y 10000.0 then
			if close_enough Inpos.z TestPos.z 10000.0 then
				valid = true

	)
	return valid
	
)
--------------------------------------------------------------
-- NAME		:
-- PURPOSE	:Get Effect Node from Input Pos
--------------------------------------------------------------
fn GetEffectNode obj Particle=
(	
	if obj.children.count > 0 then
	(		
		for eachObj in obj.children do
		(
			if classof(eachObj) == RAGE_Particle then
			(
				if matchpattern eachObj.name pattern:(Particle.EffectName+"*")  then
				(
					print ("updating attributes for "+eachObj.name)
				)

			)
			GetEffectNode eachObj Particle
		)
	)
)

--------------------------------------------------------------
-- NAME		:
-- PURPOSE	:Set Effect Attributes
--------------------------------------------------------------
fn SetPHelperAttr obj attribures =
(
	setattr obj 1 attribures.EffectName
	setattr obj 2 attribures.Trigger
	setattr obj 3 attribures.Attach
	setattr obj 4 attribures.AttachtoAll
	setattr obj 5 attribures.Scale
	setattr obj 6 attribures.Probability
	setattr obj 7 attribures.HasTint
	setattr obj 8 attribures.R
	setattr obj 9 attribures.G
	setattr obj 10 attribures.B
	setattr obj 11 attribures.IgnoreDamageModel
	setattr obj 12 attribures.PlayonParent
	setattr obj 13 attribures.OnlyonDamageModel

)
--------------------------------------------------------------
-- NAME		:
-- PURPOSE	:Process Invalid Particles
--------------------------------------------------------------
fn UpdateInvalidParticles Invalid_Particles =
(	
	print "testing"
	local IDE2dfxList = processIDE(getmapIDEfilename())
		
	if IDE2dfxList.count > 0 then
	(	
		index = 1
		for eachParticle in IDE2dfxList do
		(
			-- check Index of particle form our invalid particle list
			
			if index > Invalid_Particles.count then index=Invalid_Particles.count
			if matchpattern (Invalid_Particles[index][1].name) pattern:(eachParticle.NodeName+"*")  then
			(
				print ("Updated GTA Particle: " +(Invalid_Particles[index][1].name)  +" to Rage Particle  ")--+ eachParticle.NodeName)
				SetPHelperAttr (Invalid_Particles[index][2]) eachParticle
			)
			else
			(
				Print "No Helper Found, Please check if the IDE file contains Particle Nodes"
			)
			index+=1
			
		
			
			
			/*
			local obj = getnodebyname (eachParticle.nodename+"_frag_") exact:false
		
			if obj != undefined then
			(
				print obj
				local validEffect = GetEffectNode obj eachParticle
				if validEffect != ok then
				(
				print ("*Valid   "+validEffect as string)
				)
				else
				(
					--Print "unable to Locate Effect"
					--print obj
				)
			)
			*/

			
		)
	)
	else
	(
		Print "IDE File is out of sync with Max File. Please get a earlier revision of the IDE file to fix invalid GTA Particles"
	)
	
		
)
--------------------------------------------------------------
-- NAME		:
-- PURPOSE	:Bool: Return if Particle has a Name or not
--------------------------------------------------------------
fn getRageParticleAttr obj =
(
	Name = getattr obj 1 
	if Name == "" then return true
		else return False
	
)

--------------------------------------------------------------
-- NAME		:
-- PURPOSE	:Get Top Parent Name
--------------------------------------------------------------
fn getTopParentName obj =
(
	if obj.parent != undefined then
	(
	
	getTopParentName obj.parent
	)
	else
	(
	obj
	)
)

--------------------------------------------------------------
-- NAME		:
-- PURPOSE	:Collect all the particles in the scene and test if they have a name
--------------------------------------------------------------
fn collectParticles =
(

	local Invalid_Particles = #()
	for i in helpers do
	(
		if classof i == RAGE_Particle then
		(
			--print i.name
			if getRageParticleAttr i then
			(
				-- Get Parent object				
				append Invalid_Particles #((getTopParentName i) , i) 
			)
		)
	)
	
	
	if Invalid_Particles.count > 0 then
	(
		--print Invalid_Particles
		UpdateInvalidParticles Invalid_Particles
	)
	else print "No invalid Particles Found"
	-- Crappy Hack
	
)


fn TestParticles =
(
	--local Invalid_Particles = #()
	local inc = 0
	for i in helpers do
	(
		if classof i == RAGE_Particle then
		(
			inc+=1
			--print i.name
			if getRageParticleAttr i then
			(
				-- Get Parent object				
				--append Invalid_Particles #((getTopParentName i) , i) 
				print ("Invalid Particle Found: "+i.name)
				
			)
		)
	)
	print ("Found "+inc as string + " Particles")
	print "Finished Testing Scene"
)

rollout RsParticleConverter "Particle Converter" 
(
	button btnTestScenet "Test Scene" width:160
	button btnUpdatefromIDE "Update from IDE" width:160
	button btnUpdatefromNode "Update from Node Name" width:160
	
	on btnTestScenet pressed do
	(
		clearlistener()
		TestParticles()
	)
	
	on btnUpdatefromIDE pressed do
	(
		clearlistener()
		collectParticles()
	)
	
	on btnUpdatefromNode pressed do
	(
		clearlistener()
		FixParticles()
	)
	
	on RsParticleConverter open do
	(
		LoadEntries()
		clearlistener()

	)
	
	
	
)

createDialog RsParticleConverter




-----------------------------------------------------------------------------
-- Install Callbacks
-----------------------------------------------------------------------------


--callbacks.removeScripts id:#testCallback
--callbacks.addScript #filePostOpen "collectParticles()" id:#testCallback

