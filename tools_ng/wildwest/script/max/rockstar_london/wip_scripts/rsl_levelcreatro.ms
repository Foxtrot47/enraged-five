try (destroyDialog RSL_simpleLevelCreator) catch()
clearlistener()




rollout RSL_simpleLevelCreator " RS Simple Level Creator WIP" width:220 height:530
(
	----------------------------------------------------------------------------------------------
	--								Constructors
	----------------------------------------------------------------------------------------------
	local mapRoot = project.art+"\\Maps\\"
	local levels = #(#(),#())
	
	--------------------------------------------------------------
	-- NAME		:
	-- PURPOSE	:Get all teh Level Names from the game
	--------------------------------------------------------------
	fn gatherLevelNames = 
	(
		local mapDirectories = getDirectories (mapRoot + "*")
		
		for directory in mapDirectories do
		(
			local dataStream = directory as stringStream
			skipToString dataStream "\\Maps\\"
			local mapName = readDelimitedString dataStream "\\"
			
			append levels[1] mapName
			append levels[2] directory
		)
	)
	
	
	

	
	fn buildMesh vertArray faceList =
	(
		--print faceList.count
		--print vertArray.count
		temp = mesh vertices:vertArray faces:faceList
		
	)
	
	
	
	
	fn readBoundfile filename =
	(
		file_stream = openFile filename
		bndsection = "ignore"
		tempV = #()
		tempF = #()

		if file_stream != undefined then
		(
			skipToString file_stream "NumBounds"	-- Vertices List

			while not eof file_stream do
			(
				curline = filterstring (Readline file_stream) " "				
				
				if curline[1]== tolower "v" then bndsection = "vertices"
				else if curline[1] == tolower "tri" then bndsection = "tris"
				else if curline[1] == tolower "matrix:" then bndsection = "newmesh"
				else bndsection = "ignore"
				if curline.count < 2 then continue				

				case bndsection of
				(
					"newmesh":	(
						print "NewMesh"
						bndsection = "ignore"
						buildMesh tempV tempF
						tempV = #()
						tempF = #()
						)					
					"vertices":(
						vertarray = [execute curline[2] +1 ,execute curline[3] +1,execute curline[4] +1]
						append tempV vertarray
						)
					"tris":(
						facearray =  [execute curline[2] +1,execute curline[3] +1,execute curline[4] +1]
						append tempF facearray
						)
					quad:x=1
				)
			)
		)
	)
	
	
	fn extract mappath list filetype =
	(
		filename = "ragebuilder_0327.exe"
		makedir ("c:\\tempBnd\\"+pathConfig.stripPathToLeaf mappath)	
		HiddenDOSCommand ("del c:\\tempBnd\\"+pathConfig.stripPathToLeaf mappath+"*.bnd") startpath:"c:\\"			
		for eachimage in list do
		(			
		arguments = ("x:\\tools\\bin\\script\\Extracter.rbs -input "+eachimage + " -output c:\\tempbnd\\"+(pathConfig.stripPathToLeaf mappath) + " -filetype "+filetype)
		print (filename +" "+ arguments)
		HiddenDOSCommand (filename +" "+ arguments) startpath:"x:\\tools\\bin\\" 
		)
	)
	
	fn extractbnds mappath =
	(		
		print mappath
		imglist =  getfiles (mappath+"*.rpf")
		print imglist
		extract mappath imglist "ibn"
	)
	
	fn extractIBDS temppath =
	(
		bndlist =  getfiles (temppath+"*.ibn")
		extract temppath bndlist "bnd"		
	)
	
	fn parseBoundFiles temppath =
	(
		bndlist =  getfiles (temppath+"*.bnd")
		for bnd in bndlist do
		(
			readBoundfile bnd
		)
		
	)
	
	fn processMap levelindex=
	(
		
		
		
		---path_name = "X:\\payne\\build\\dev\\independent\\levels\\s_marina\\"
		
		path_name = project.independent +"/levels/"+ levels[1][levelindex]+"/"
		print path_name
		temp_path = ("c:\\tempBnd\\"+pathConfig.stripPathToLeaf path_name)	
		print temp_path
		--print (buildMesh())
		--readBoundfile "X:\\payne\\payne_art\\maps\\s_marina\\ma_sec2_1.bnd"
		--readBoundfile "X:\\payne\\payne_art\\maps\\s_marina\\ma_sec2_1.bnd"
		extractbnds path_name
		extractIBDS temp_path
		parseBoundFiles temp_path
	)
	
	
	
	listBox lbx_levels "Levels" height:20
	button btn_create "Create From Selected Files" width:196
	

	----------------------------------------------------------------------------------------------
	--									EVENTS		
	----------------------------------------------------------------------------------------------
	--------------------------------------------------------------
	-- NAME		:
	-- PURPOSE	:
	--------------------------------------------------------------
	On RSL_simpleLevelCreator open do
	(
		gatherLevelNames()		
		lbx_levels.items = levels[1]
	)
	
	on btn_create pressed do
	(
		processMap (lbx_levels.selection)
	)
	
)


createDialog RSL_simpleLevelCreator style:#(#style_toolwindow,#style_sysmenu)