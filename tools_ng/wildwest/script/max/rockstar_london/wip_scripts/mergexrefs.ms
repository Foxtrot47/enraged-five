clearlistener()

fn mergeobj maxfile xMerge =
(
	
	mergeMaxFile maxfile xMerge 
)

fn getValidObjs objlist maxfile =
(
	patternlist = #("","_frag_")
	
	mObjs=objlist
	xMerge=#()
	xrefs.deleteAllXRefs()	
	xrefs.addnewxreffile maxfile #noLoad	
	aXRef=xRefs.getXRefFile 1	
	aXRef.hidden=true
	updateXRef aXRef
	
	cref = aXRef.tree.children	
	for i in cref do
	(
		if i.children.count!=0 then
		(
			for p in patternlist do
			(
				mBol=finditem mObjs (i.name+p)
				if mBol!=0 then
				(
					append xMerge i.name
					for j in i.children do append xMerge j.name
				)
			)
		)
	)
	xrefs.deleteAllXRefs()

	if xMerge.count!=0 then 
	(
		mergeobj maxfile xMerge
	)
	
)

-- Reset the Maxfile/New Maxfile
fn newMaxfile =
(
	clearlistener()
	resetMaxFile #noPrompt
)

-- Save the Maxfile
fn save_Maxfile filename =
(
	local result = saveMaxFile filename quiet:true
	result
)


-- Process the File List
fn processXrefList xrefList =
(
	newMaxfile()
	for i in xrefList do
	(
		getValidObjs (i[2]) (i[1])

	)
)

-- return a list of all the scene Xrefs
fn collectSceneXrefs =
(
	xrefsList = for i in getClassInstances xrefobject collect (i)
)


-- Simple find if unique in the Array fn
fn findFilenameinArray arraylist match =
(
	for i = 1 to arraylist.count do
	(
		if arraylist[i][1] == match then return i
	)
	return 0
)

-- check the souce filename and return valid filename
fn checkFilename filename =
(
	returnPath = filename
	-- Normailize the Paths	
	shortName = toLower (pathConfig.normalizePath project.art)
	filename = toLower (pathConfig.normalizePath filename)
	
	
	shortName = filterString shortName ":"


	local index = findString filename shortName[shortName.count]
	index = (index+shortName[shortName.count].count)
	if index != undefined then
	(	
		ss = substring filename index 100
		returnPath = project.art+ss		
	)
	returnPath
)

-- go through our list and build a new array that is sorted by filename
fn buildObjList =
(
	sceneXrefs = collectSceneXrefs()
	xrefList = #()
	for i in sceneXrefs do
	(
		validFilename = checkFilename (i.filename)
		-- If not in our array add the filename and object name
		result = findFilenameinArray xreflist validFilename

		if result == 0 then
		(			
			append xrefList #(validFilename,#(i.objectname))
		)		
		
		-- if filename is in our array then just add the object name
		if result > 0 then
		(
			--index = findItem xrefList i.filename
			index = findFilenameinArray xrefList validFilename
			if index != 0 then
			(
				-- Lets add the object to the correct filename if it does not already exist
				appendIfUnique xreflist[index][2] i.objectname
			)
		)
	)
	xrefList
)

fn main =
(
	local xreflist =buildObjList()
	processXrefList xreflist
	
	--testfile = "x:/payne/payne_art\maps\c_cemet\props\prop_c_sniper.max"
	--proplist = #("2GTA_grave03", "P_cemUrn01_H", "P_cemWallTop02_H")
	
	
	
	
)

main()


