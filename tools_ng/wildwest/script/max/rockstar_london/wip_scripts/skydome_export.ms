-- Skydome Export Process --

-- export to intermediate fodler the itd/idr
-- build all the meshs and pack into the 
-- do some other shit

filein "rockstar/export/ragebuilder.ms"
filein "rockstar/util/attribute.ms"
filein "rockstar/util/file.ms"
filein "rockstar/util/rexreport.ms"
filein "pipeline/util/rb_script_generator.ms"
filein "pipeline/export/models/globals.ms"
filein "pipeline/util/drawablelod.ms"

filein "payne/pipeline/RSL_outputLog.ms"


logoutput = init_OutputLog ()


-- DotNet Event Handler Function --
 fn outputDataReceivedHandler obj evt =
 (
 	if evt.data != undefined then
 	(	

		logoutput.log evt.data type:1

 	)
 )
	
-- DotNet Process Run command Function --
fn RunCommand cmd arguments =
(
	proc = dotNetObject "System.Diagnostics.Process"
	proc.EnableRaisingEvents = true
	
	proc.StartInfo.WorkingDirectory = (rsconfig.toolsbin+"/bin/")
	proc.StartInfo.FileName = cmd
	proc.StartInfo.UseShellExecute = false
	proc.StartInfo.RedirectStandardOutput = true
	proc.StartInfo.RedirectStandardError = true
	proc.StartInfo.CreateNoWindow = true
	  
	dotNet.AddEventHandler proc "OutputDataReceived" outputDataReceivedHandler
	
	proc.StartInfo.Arguments = (" "+arguments)
	print arguments
	proc.Start()
	proc.BeginOutputReadLine()
	
	proc.WaitForExit()

)

--
-- name: RsSkydomeBuildImage
-- desc: builds the skydome Idd
--
fn RsSkydomeBuildImage rebuild:false = (

	-- Build image.
	rbFileName = RsConfigGetScriptDir() + "skydomes/skydomes_build_image.rb"
	RsMakeSurePathExists rbFileName
	
	rbFile = openfile rbFileName mode:"w+"
	RBGenSkydomeResourcingScriptPre rbFile undefined true
	RBGenSkydomeBuildImage rbFile
	close rbFile
	
	-- Run the Ruby resourcing script to build our vehicle image.
	cmdline = "ruby " + rbFileName
	RunCommand "ruby" rbFileName
	/*
	if doscommand cmdline != 0 then (
		
		messagebox "failed to build skydome image"
		return false
	)
	*/
	return true
)


fn buildresourcefiles skydomeFile =
(
	rbFileName = RsConfigGetScriptDir() + "skydomes/"+skydomeFile + ".rb"
	RsMakeSurePathExists rbFileName
	rbFile = openfile rbFileName mode:"w+"
	

	RBGenSkydomeResourcingScriptPre rbFile skydomeFile false
	RBGenSkydomeResourcingScriptModel rbFile
	RBGenResourcingScriptPost rbFile
	if isPatch == true then (
		RBGenResourcingScriptPatch rbFile
	)
	
	close rbFile

	-- Run Ruby resource script for the model.
	cmdline = "ruby " + rbFileName
	
	RunCommand "ruby" rbFileName
	/*
	if doscommand cmdline != 0 then (
		messagebox "failed to build skinned model resource"
		return false
	)	
*/	

	if RsSkydomeBuildImage() == false then return false
	
	logoutput.log "Finished" type:1
	return true
)

fn exportskydome =
(

	
	logoutput.start()
	--logoutput.log "text" type:2
	
	--exit()
	
	-- Check for single mesh export
	logoutput.close()

	exit()
	
	local obj = selection[1]
	
	print obj
	
	--local modelFile = ( RsModelOutputDirectory + "/" + obj.name )
	--local modelDir = ( RsModelOutputDirectory + "/" + obj.name )
	
		
	local modelDir = RsConfigGetStreamDir() + "skydomes/" 
	local modelFile = RsConfigGetStreamDir() + "skydomes/" + obj.name
	
	
	
	
	RsDeleteFiles (modelDir + ".*")
	RsDeleteDirectory modelDir
	
	rexReset()
	graphRoot = rexGetRoot()
	-----------------------------
	entity = rexAddChild graphRoot "entity" "Entity"
	rexSetPropertyValue entity "OutputPath" modelFile
	
	-- RAGE Drawable LOD Support
	local drawobjs = RsLodDrawable_SetUserProps obj
	selectMore drawobjs
	
	mesh = rexAddChild entity "mesh" "Mesh"
	rexSetPropertyValue mesh "OutputPath" modelFile
	rexSetPropertyValue mesh "ExportTextures" "true"
	rexSetPropertyValue mesh "TextureOutputPath" modelFile
	rexSetPropertyValue mesh "MeshAsAscii" (asciiMesh as String)
	rexSetNodes mesh

	skel = rexAddChild entity "skeleton" "Skeleton"
	rexSetPropertyValue skel "OutputPath" modelFile
	rexSetPropertyValue skel "SkeletonUseBoneIDs" "true"
	rexSetNodes skel

	
	

	select obj

	rexExport()
	
	-- Build the resource files for the skydome
	buildresourcefiles(obj.name)
		

	return true	
	
	
	
)

exportskydome()