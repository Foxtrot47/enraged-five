clearlistener()

try (destroyDialog rollout_DecalPlanter) catch ()

rollout rollout_DecalPlanter "Decal Planter" width:160 height:320
(
	spinner spn1 "Offset Value"  width:80 height:16 range:[0,10.0,0.01] type:#float scale:0.01
	radioButtons rdo_axis_dir "Axis"  width:103 height:30 labels:#("X", "Y", "Z") default:3
	
	button btn_source "Add Source Decals"  width:140 height:22
	listbox lstbox "Decals to be planted:"
	button btn_target "Pick Target Surface" width:140 height:22
	button btn_Plant "Plant!" width:119 height:44
	
	-- Globals
	local axis_dir = [0,0,-1] -- Default to Z
	local target_mesh = undefined
	local source_mesh = undefined
	local offset = 0.01;
	local offsetDir = [0,0,-0.1]
	
	
	-- Functions --
	-- Used to make sure we select geometry from the pick object button
	fn g_filter o = (superClassOf o == GeometryClass)
	
	
	-- Update the Ray Direction
	fn updateButton = 
		(
		axis_dir = case rdo_axis_dir.state of 
			(
			1: [-1,0,0] 
			2: [0,-1,0] 
			3: [0,0,-1] 
			)
		)
		
	fn getObjDir obj =
	(
		case axis_dir of
		(
			([-1,0,0]):obj.max.z 
			([0,-1,0]):obj.max.y 
			([0,0,-1]):obj.max.x
		)
	)
		
	fn getOffset  =
	(
		offset = spn1.value
		
		offsetDir = case axis_dir of 
		(
			([-1,0,0]): [-offset,0,0]
			([0,-1,0]): [0,-offset,0]
			([0,0,-1]): [0,0,-offset]
		)
	)
	
	
	-- Check source and target object to create a vector
	-- Do a dot product to get angle direction
	-- use angle to do intersection
	fn getdirection sourceObj targetObj =
	(
		
	)
	
		
	-- Do raycat fro each vertice against the target obejc tand return the intersection point
	fn find_intersection targetNodeZ sourceNodeZ =
	( 
		
	 local testRay = ray sourceNodeZ axis_dir
	 local nodeMaxZ = getObjDir(targetNodeZ) --targetNodeZ.max.z 
	testRay.pos.z = nodeMaxZ + 0.0001 * abs nodeMaxZ 
	intersectRay targetNodeZ testRay 
		
	)
	
	-- Proces the vertices in teh source object
	fn processObjectVertices decalObj targetObj=
	(
		decalObj = convertToMesh(decalObj)
		targetObj = convertToMesh(targetObj)
		
		--local objNVerts  = polyop.getNumVerts decalObj		-- This will include dead verts
		local objNVerts =  meshop.getNumVerts decalObj
		for i = 1 to objNVerts do
		(
			--vertpos = polyop.getVert decalObj i
			vertpos = meshop.getVert decalObj i
			
			-- We then need to move each vert a specififed distnace from the source object
			int_point = find_intersection targetObj vertpos
			if int_point != undefined then 
			(	
				--polyop.setVert decalObj i (int_point.pos - getOffset() )	
				meshop.setVert decalObj i (int_point.pos - getOffset() )	
			)
		)
		decalObj = convertToMesh(decalObj)
		targetObj = convertToMesh(targetObj)
	)
	


	
	
	
	
	
	
	---- EVENT HANDLERS ---
		
	on rdo_axis_dir changed state do updateButton()
	
	on btn_source pressed do
	(
		currentSel = $
		print currentSel
		if currentSel!= undefined then
		(
			for eachObj in currentSel do
			(
				if g_filter eachObj then
				(
					
					lstbox.items = lstbox.items + "chhe";
				)
			)
		)
		/*
		source_mesh = pickObject message:"Pick Decal Surface:" filter:g_filter
		if source_mesh != undefined then
		(
		btn_source.text = source_mesh.name
		)
		*/
	)
	
	on btn_target pressed do
	(
		
		
		target_mesh = pickObject message:"Pick Target Object:" filter:g_filter
		if target_mesh != undefined then
		(
		btn_target.text = target_mesh.name
		)
		
	)
	
	on btn_Plant pressed do
	(
				
		if source_mesh == undefined then messagebox "Please Pick at least 1 Source Object"
			else if target_mesh == undefined then messagebox "Please Pick Target surface Object"
				else
				(					
					processObjectVertices source_mesh target_mesh
					CenterPivot source_mesh
				)
	)
	
)
createDialog rollout_DecalPlanter
