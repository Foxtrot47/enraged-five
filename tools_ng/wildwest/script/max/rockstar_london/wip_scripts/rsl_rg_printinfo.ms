filein "RSL_RG_Global.ms"

try (destroyDialog RSL_RG_PrintInfo) catch()

rollout RSL_RG_PrintInfo "Making standard layers" width:176 height:32
(
	progressBar pb1 "ProgressBar" pos:[8,8] width:160 height:16
		
	on RSL_RG_PrintInfo open do
	(	
		clearListener()
		ObjectArray = selection as array
		clearSelection()
		Increment = 100.0/ObjectArray.count as float
		Total = 0.0
		
	for Obj in ObjectArray do
	(	
		format "[Name]% [Position] x:% y:% z:% [Rotation] x:% y:% z:%\n" Obj.name Obj.pos.x Obj.pos.y Obj.pos.z Obj.rotation.x_rotation Obj.rotation.y_rotation Obj.rotation.z_rotation
		
		Total += Increment
		pb1.value = Total 
	)
	
	SelectNodes ObjectArray
	destroyDialog RSL_RG_PrintInfo
	)
)

createDialog RSL_RG_PrintInfo style:#(#style_toolwindow,#style_sysmenu)