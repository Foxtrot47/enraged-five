
dotnet.loadAssembly (scriptsPath + "payne\\dll\\RGLExportHelpers.dll")
OBBGenerator = dotNetObject "RGLExportHelpers.OBBGenerator"

fn gatherVertextPositions object =
(
	local vertPositions = for vert in object.verts collect vert.pos
	
	if object.SelectedFaces.count > 0 then
	(
		case (classOf object.baseObject) of
		(
			editable_mesh:
			(
				vertPositions = for index in (meshOp.getVertsUsingFace object object.SelectedFaces) collect object.verts[index].pos
			)
			editable_poly:
			(
				vertPositions = for index in (polyOp.getVertsUsingFace object object.SelectedFaces) collect object.verts[index].pos
			)
		)
	)
	
	local result = dotNetObject "RGLExportHelpers.point3[]" vertPositions.count
	
	for i = 1 to vertPositions.count do
	(
		local pos = vertPositions[i]
		
		local dotNetPoint3 = dotNetObject "RGLExportHelpers.point3" pos.x pos.y pos.z
		
		result.SetValue dotNetPoint3 (i - 1)
	)
	
	result
)

fn matrixFromDotNetMatrix matrix =
(
	local result = matrix3 0
	
	result.row1 = [matrix.row1.x,matrix.row1.y,matrix.row1.z]
	result.row2 = [matrix.row2.x,matrix.row2.y,matrix.row2.z]
	result.row3 = [matrix.row3.x,matrix.row3.y,matrix.row3.z]
	result.row4 = [matrix.row4.x,matrix.row4.y,matrix.row4.z]
	
	result
)

fn generateOBB theObject =
(
	if theObject != undefined then
	(
		local dotNetVertPositionArray = gatherVertextPositions theObject
		
		start = timeStamp()
		local bound = OBBGenerator.findSmallestOBB dotNetVertPositionArray (degToRad 10.0)
		end = timeStamp()
		
		format "Processing took % seconds\n" ((end - start) / 1000.0)
		
		format "Size: x:% y:% z:%\n" bound.size.x bound.size.y bound.size.z
		
-- 		b = box width:bound.size.x length:bound.size.y height:bound.size.z wireColor:green
-- 		b.pivot = [0,0,bound.size.z * 0.5]
-- 		
-- 		b.transform = (matrixFromDotNetMatrix bound.matrix)
		
		newCollisionMesh = Col_Box name:(uniqueName (theObject.name + " - colBox"))
		newCollisionMesh.wireColor = orange
		
-- 		newCollisionMesh.pivot = [0,0,bound.size.z * 0.5]
		
		newCollisionMesh.transform = (matrixFromDotNetMatrix bound.matrix)
		
		newCollisionMesh.length = bound.size.y * 0.5
		newCollisionMesh.width = bound.size.x * 0.5
		newCollisionMesh.height = bound.size.z * 0.5
		
		newCollisionMesh.parent = theObject
		
		setAttr newCollisionMesh (getattrindex "Gta Collision" "Coll Type") "WOOD_SOLID_MEDIUM"
	)
)

objectArray = selection as array

for object in objectArray do
(
	generateOBB object
)


