
try (destroyDialog RSL_Fragmenter) catch()

rollout RSL_Fragmenter "R* Fragmenter" width:210
(
	local theObject
	local theObjectName
	local theObjectPivot
	local theTrackObject
	local theCutterGuide
	local theCutterMaterial
	local cutter1
	local IntRay
	local mousePressed = 0
	local mousePos
	local initialSize
	local trackMouse
	local cutting = false
	local seedCount = 0
	local pointArray = #()
	
	fn validObject object =
	(
		local result = getAttrClass object == "Gta Object" and classOf object != XRefObject
		
		result
	)
	
	fn pickPoints =
	(
		local result = true
		
		for i = 1 to 2 do
		(
			newPoint = pickPoint snap:#3D
			
			if newPoint == undefined or newPoint == #rightClick then
			(
				result = false
				exit
			)
			else
			(
				append pointArray newPoint
			)
		)
		
		result
	)
	
	fn generateRandomArray count =
	(
		local result = #()
		result.count = count
		result[1] = 0
		result[2] = (count - 1)
		
		for i = 3 to count do
		(
			local randomValue
			local unique = false
			
			while not unique do
			(
				randomValue = random 1 (count - 2)
				
				unique = (findItem result randomValue) == 0
				
				if unique then
				(
					result[i] = randomValue
				)
			)
		)
		
		result
	)
	
	mapped fn setSmoothingGroup face object index =
	(
		setFaceSmoothGroup object.mesh face index
	)
	
	fn findCutterMaterial material =
	(
		local result = 0
		
		if (classOf material) == multimaterial then
		(
			for i = 1 to material.materialList.count do
			(
				local subMaterial = material.materialList[i]
				
				if subMaterial == theCutterMaterial then
				(
					result = material.materialIDList[i]
				)
			)
		)
		else
		(
			if theCutterMaterial == material then
			(
				result = 1
			)
		)
		
		result
	)
	
	fn cut ir fragmentLength:undefined =
	(
		seed seedCount
		local initialRadius = RSL_Fragmenter.spn_size.value * 0.5
		local variation = initialRadius / 100.0 * RSL_Fragmenter.spn_sizeVar.value
		local randomAmount = random -variation variation
		local finalRadius = (initialRadius + randomAmount)
		
		local cutterRotation = eulerAngles (random -RSL_Fragmenter.spn_rotate.value RSL_Fragmenter.spn_rotate.value) \
														(random -RSL_Fragmenter.spn_rotate.value RSL_Fragmenter.spn_rotate.value) \
														(random -RSL_Fragmenter.spn_rotate.value RSL_Fragmenter.spn_rotate.value)
		
		local cutterScale = [RSL_Fragmenter.spn_x.value,RSL_Fragmenter.spn_y.value,RSL_Fragmenter.spn_z.value]
		if fragmentLength != undefined then
		(
			cutterScale.z = (fragmentLength / (finalRadius * RSL_Fragmenter.spn_stretch.value))
		)
		
		if RSL_Fragmenter.chkb_sphere.checked then
		(
			cutter1 = sphere segs:RSL_Fragmenter.spn_seg.value pos:ir.pos radius:finalRadius mapCoords:true
			
			rotate cutter1 (eulerAngles 0 90 0)
			resetXForm cutter1
			convertToMesh cutter1
		)
		else
		(
			if RSL_Fragmenter.chkb_geo.checked then
			(
				cutter1 = geosphere segs:RSL_Fragmenter.spn_seg.value pos:ir.pos radius:finalRadius
				
				convertToMesh cutter1
			)
			else
			(
				cutter1 = box length:(finalRadius * 2) width:(finalRadius * 2) height:(finalRadius * 2) \
									lengthSegs:RSL_Fragmenter.spn_seg.value widthSegs:RSL_Fragmenter.spn_seg.value heightSegs:RSL_Fragmenter.spn_seg.value
				cutter1.pivot = cutter1.center
				resetXForm cutter1
				convertToMesh cutter1
				cutter1.pos = ir.pos
			)
		)
		
		setSmoothingGroup (for face in cutter1.faces collect face.index) cutter1 (2^RSL_Fragmenter.spn_smGroup.value)
		update cutter1
		
		rotate cutter1 cutterRotation
		resetXForm cutter1
		convertToMesh cutter1
		cutter1.dir = ir.dir
		cutter1.scale *= cutterScale
		cutter1.name = theObjectName + "_FRAGMENTED"
		cutter1.wireColor = theObject.wireColor
		
		if RSL_Fragmenter.chkb_ID.checked then
		(
			cutter1.material = theObject.material
			
			for face in cutter1.faces do
			(
				setFaceMatID cutter1.mesh face.index RSL_Fragmenter.spn_ID.value
			)
		)
		else
		(
			if theCutterMaterial != undefined then
			(
				local ID = findCutterMaterial theObject.material
				
				if ID != 0 then
				(
					cutter1.material = theObject.material
					
					for face in cutter1.faces do
					(
						setFaceMatID cutter1.mesh face.index ID
					)
				)
				else
				(
					cutter1.material = theCutterMaterial
				)
			)
			else
			(
				cutter1.material = theObject.material
			)
		)
		
		local displaceMap = noise type:1 size:(finalRadius * RSL_Fragmenter.spn_noiseSize.value)
		displaceMap.coords.offset = ir.pos
		local displaceMod
		
		if RSL_Fragmenter.chkb_sphere.checked then
		(
			displaceMod = displace strength:(finalRadius * (RSL_Fragmenter.spn_noise.value * 2)) lumCenterEnable:true center:0.5 map:displaceMap maptype:3
		)
		else
		(
			if RSL_Fragmenter.chkb_geo.checked or RSL_Fragmenter.chkb_sphere.checked then
			(
				displaceMod = displace strength:(finalRadius * (RSL_Fragmenter.spn_noise.value * 2)) lumCenterEnable:true center:0.5 map:displaceMap useMap:true
			)
			else
			(
				displaceMod = displace strength:(finalRadius * (RSL_Fragmenter.spn_noise.value * 2)) lumCenterEnable:true center:0.5 map:displaceMap maptype:2
				addModifier cutter1 (spherify percent:RSL_Fragmenter.spn_spherify.value)
			)
		)
		
		addModifier cutter1 displaceMod
		
		local cutter2 = copy cutter1
		cutter2.name = uniqueName (theObjectName + "_Fragment")
		
		ProBoolean.CreateBooleanObjects (cutter2) #(copy theObject) 1 2 1
		ProBoolean.CreateBooleanObjects (theObject) #(cutter1) 2 2 1
		
		resetXForm cutter2
		convertToPoly cutter2
		
		resetXForm theObject
		convertToPoly theObject
		
		select theObject
		
		if theTrackObject == undefined or not (isValidNode theTrackObject) then
		(
			theTrackObject = theObject
		)
		
		seedCount += 1
	)
	
	fn updateCutterGuide ir =
	(
		if RSL_Fragmenter.chkb_geo.checked or RSL_Fragmenter.chkb_sphere.checked then
		(
			theCutterGuide.radius = (RSL_Fragmenter.spn_size.value * 0.5)
		)
		else
		(
			theCutterGuide.length = RSL_Fragmenter.spn_size.value
			theCutterGuide.width = RSL_Fragmenter.spn_size.value
			theCutterGuide.height = RSL_Fragmenter.spn_size.value
		)
		
		theCutterGuide.scale = [1,1,1] * [RSL_Fragmenter.spn_x.value,RSL_Fragmenter.spn_y.value,RSL_Fragmenter.spn_z.value]
		
		if ir != undefined then
		(
			theCutterGuide.pos = ir.pos
			theCutterGuide.dir = ir.dir
		)
	)
	
	fn trackMouse msg ir obj faceNum shift ctrl alt =
	(
		case msg of
		(
			#freeMove:
			(
				updateCutterGuide ir
				#continue
			)
			#mousePoint:
			(
				IntRay = ir
			)
			#mouseMove:
			(
				#continue
			)
			#mouseAbort:
			(
				cutting = false
				RSL_Fragmenter.chkb_frag.checked = false
				RSL_Fragmenter.btn_undo.enabled = true
				
				try (delete theCutterGuide) catch()
			)
		)
	)
	
	group "Object"
	(
		pickbutton btn_pickObject "Pick Object..." width:190 filter:validObject
		button btn_autoSmooth "Auto Smooth" width:92 across:2 offset:[-4,0]
		spinner spn_thresh width:92 offset:[-19,3] range:[0.0, 180.0, 5.0]
	)
	
	on btn_pickObject picked object do
	(
		if object != undefined then
		(
			theObject = object
			btn_pickObject.caption = object.name
			RSL_Fragmenter.chkb_frag.enabled = true
		)
		else
		(
			theObject = undefined
			btn_pickObject.caption = "Pick Object..."
			RSL_Fragmenter.chkb_frag.enabled = false
		)
	)
	
	on btn_autoSmooth pressed do
	(
		undo off
		(
			try
			(
				meshop.autoSmooth theObject theObject.faces spn_thresh.value
				update theObject
			)
			catch()
		)
	)
	
	group "Fragment Options"
	(
		label lbl_type "Type:" align:#left
		checkBox chkb_sphere "Sphere" across:3
		checkBox chkb_geo "Geo Sphere" checked:true
		checkBox chkb_box "Box" offset:[24,0]
		spinner spn_seg "Segments" range:[0, 20, 1] type:#integer
		spinner spn_size "Size" range:[0.0, 1000.0, 0.25]
		spinner spn_sizeVar "Variation" range:[0.0, 100, 25.0]
		spinner spn_spherify "Spherify" range:[0.0, 100, 90.0] enabled:false
		spinner spn_rotate "Rotation" range:[0.0, 180, 45.0]
		label lbl_scale "Scale X/Y/Z" align:#left
		spinner spn_x "X" width:60 range:[0.1, 10.0, 1.0] across:3 offset:[-14,0]
		spinner spn_y "Y" width:60 range:[0.1, 10.0, 1.0] offset:[-12,0]
		spinner spn_z "Z" width:60 range:[0.1, 10.0, 1.0] offset:[-10,0]
		spinner spn_stretch "Stretch (P to P)" range:[1.0, 2, 1.0]
		spinner spn_noise "Noise Strength" range:[0.0, 1.0, 0.5]
		spinner spn_noiseSize "Noise Size" range:[0.0, 1.0, 0.5]
		spinner spn_smGroup "Smoothing Group" range:[1, 32, 9] type:#integer
		colorpicker col_fragColour "Wire Colour" width:152 align:#right
		materialbutton mat_cutterMaterial "Pick Cutter Material" width:190
		checkBox chkb_ID "Assign ID" across:2
		spinner spn_ID width:94 range:[1, 99, 1] type:#integer offset:[-23,0]
	)
	
	on chkb_sphere changed state do
	(
		chkb_sphere.checked = true
		chkb_geo.checked = false
		chkb_box.checked = false
		spn_spherify.enabled = false
		
		spn_seg.range = [4, 64, 16]
	)
	
	on chkb_geo changed state do
	(
		chkb_sphere.checked = false
		chkb_geo.checked = true
		chkb_box.checked = false
		spn_spherify.enabled = false
		
		spn_seg.range = [0, 20, 1]
	)
	
	on chkb_box changed state do
	(
		chkb_sphere.checked = false
		chkb_geo.checked = false
		chkb_box.checked = true
		spn_spherify.enabled = true
		
		spn_seg.range = [0, 20, 1]
	)
	
	on mat_cutterMaterial picked mat do
	(
		theCutterMaterial = mat
		mat_cutterMaterial.caption = "Material: " + mat.name
	)
	
	on chkb_ID changed state do
	(
		mat_cutterMaterial.enabled = not state
	)
	
	group "Fragmentation Options"
	(
		checkBox chkb_int "Interactive" checked:true
		checkBox chkb_guide "Copy as guide" offset:[90,0] --align:#right
		checkBox chkb_smooth "Smooth Edges" offset:[90,0]--align:#right
		checkBox chkb_point "Point to Point"
		spinner spn_fragmentCount "Fragments" range:[2, 100, 5] type:#integer enabled:false
	)
	
	on chkb_int changed state do
	(
		chkb_int.checked = true
		chkb_point.checked = false
		spn_fragmentCount.enabled = false
		spn_z.enabled = true
		chkb_guide.enabled = true
		chkb_smooth.enabled = true
	)
	
	on chkb_guide changed state do
	(
		chkb_smooth.enabled = state
	)
	
	on chkb_point changed state do
	(
		chkb_point.checked = true
		chkb_int.checked = false
		spn_fragmentCount.enabled = true
		spn_z.enabled = false
		chkb_guide.enabled = false
		chkb_smooth.enabled = false
	)
	
	checkButton chkb_frag "Fragment" width:200 enabled:false
	button btn_undo "Undo" width:200 enabled:false
	
	on chkb_frag changed state do
	(
		undo off
		(
			if state and (isValidNode theObject) then
			(
				setCommandPanelTaskMode #create
				holdMaxFile()
				btn_undo.enabled = true
				
				theCopy = (getNodeByName (theObject.name + "_PREFRAGMENTATION"))
				
				if theCopy == undefined then
				(
					theCopy = convertToMesh (copy theObject)
					theCopy.name = (theObject.name + "_PREFRAGMENTATION")
					theCopy.isHidden = true
					
					setAttr theCopy (getattrindex "Gta Object" "Dont Add To IPL") true
					setAttr theCopy (getattrindex "Gta Object" "Dont Export") true
				)
				
				theObjectName = theObject.name
				theObjectPivot = theObject.pivot
				
				select theObject
				
				if chkb_int.checked then
				(
					btn_undo.enabled = false
					IntRay = undefined
					cutting = true
					
					if chkb_geo.checked or chkb_sphere.checked then
					(
						theCutterGuide = SphereGizmo radius:(spn_size.value * 0.5)
					)
					else
					(
						theCutterGuide = BoxGizmo length:spn_size.value width:spn_size.value height:spn_size.value
						theCutterGuide.pivot = theCutterGuide.center
					)
					
					theCutterGuide.scale = [spn_x.value,spn_y.value,spn_z.value]
					theCutterGuide.name = "Cutting Guide"
					
					initialSize = (spn_size.value * 0.5)
					
					if chkb_guide.checked then
					(
						try (delete theTrackObject) catch()
						theTrackObject = copy theCopy
						
						addModifier theTrackObject (tessellate tension:0 faceType:1 iterations:2)
						convertToMesh theTrackObject
						
						theTrackObject.name = "R* Fragmenter TRACK_OBJECT"
						theTrackObject.isHidden = true
						
						if chkb_smooth.checked then
						(
							meshop.autoSmooth theTrackObject theTrackObject.faces 180
							update theTrackObject
						)
					)
					else
					(
						theTrackObject = theObject
					)
					
					while cutting do
					(
						if IntRay != undefined then
						(
							cut IntRay
						)
						
						mouseTrack on:theTrackObject trackCallback:trackMouse
					)
					
					try (delete (getNodeByName "R* Fragmenter TRACK_OBJECT" exact:true)) catch()
				)
				else
				(
					pointArray = #()
					
					local okToContinue = pickPoints()
					
					if okToContinue then
					(
						local direction = (pointArray[2] - pointArray[1])
						local offset = direction / (spn_fragmentCount.value - 1)
						
						local randomPositionArray = generateRandomArray spn_fragmentCount.value
						
						for i = 1 to spn_fragmentCount.value do
						(
							local newPosition = pointArray[1] + (randomPositionArray[i] * offset)
							
							cut (ray newPosition (normalize direction)) fragmentLength:(length offset)
						)
					)
					
					chkb_frag.checked = false
				)
			)
			else
			(
				cutting = false
				
				try (delete theCutterGuide) catch()
				
				chkb_frag.checked = false
				btn_undo.enabled = false
			)
		)
	)
	
	on btn_undo pressed do
	(
		fetchMaxFile quiet:true
		
		try
		(
			theObject = (getNodeByName theObjectName)
			select theObject
			
			max shade selected
		)
		catch()
	)
	
)

createDialog RSL_Fragmenter style:#(#style_toolwindow,#style_sysmenu)