-- Export Cover Points --


--Format is VEH_NAME <name of vehicle> to start a list of cover points.
-- POINT <x> <y> <z> <height> <usage> <arc> <angle>
-- <height> can be one of LOW, MED, HIGH, TOOHIGH
-- <usage> can be one of WALLTOLEFT, WALLTORIGHT, WALLTOBOTH, WALLTONEITHER
-- <arc> can be one of ARC_180, ARC_120, ARC_90, ARC_0TO60, ARC_300TO0, ARC_0TO45, ARC_315TO0
-- <angle> is in degrees

-- Name		X		y		Z	Height		
--	POINT -1.197  2.501 0.0 TOOHIGH WALLTORIGHT   ARC_300TO0 180


For obj in selection do
(
	if classof obj == dummy then
	(
		if matchpattern (obj.name) pattern:"cover*" then
		(
			format ("POINT " +obj.pos.x as string+"\t"+obj.pos.y as string +"\t"+obj.pos.z as string +"\tMED"+ "\tWALLTONEITHER"+ "\tARC_180"+ "\t90\n")
		)
	)		
	
)