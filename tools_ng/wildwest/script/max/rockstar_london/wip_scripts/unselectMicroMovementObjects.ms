(
	local microArray = #("micromovement", "trees", "grass")
	local objectArray = #()
	
	fn findInMcroArray name =
	(
		local result = false
		
		for entry in microArray do
		(
			if (MatchPattern name pattern:("*" + entry + "*")) then
			(
				result = true
			)
		)
		
		result
	)
	
	fn isMicroMovement object =
	(
		local result = false
		
		case (classOf object.material) of
		(
			Rage_Shader:
			(
				result = findInMcroArray (RstGetShaderName object.material)
			)
			Multimaterial:
			(
				for i = 1 to object.material.materialList.count do
				(
					local subMaterial = object.material.materialList[i]
					
					if (classOf object.material) == Rage_Shader then
					(
						if (findInMcroArray (RstGetShaderName subMaterial)) then
						(
							result = true
							exit
						)
					)
				)
			)
		)
		
		result
	)
	
	for object in (selection as Array) do 
	(
		if not (isMicroMovement object) then
		(
			append objectArray object
		)
	)
	
	select objectArray
)