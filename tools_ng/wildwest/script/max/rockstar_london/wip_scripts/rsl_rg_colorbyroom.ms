filein "RSL_RG_Global.ms"

try (destroyDialog RSL_RG_ColorByRoom) catch()

rollout RSL_RG_ColorByRoom "Coloring by room." width:176 height:32
(
	fn ProcessObject Obj WireColor =
	(
		Obj.wireColor = WireColor
		Obj.colorByLayer = false
		
  			for Child in Obj.children do
  			(
  				ProcessObject Child WireColor
  			)
	)
		
	progressBar pb1 "ProgressBar" pos:[8,8] width:160 height:16
		
	on RSL_RG_ColorByRoom open do
	(	
	Increment = 100.0/rootNode.children.count as float
	Total = 0.0

	for Obj in Helpers do
	(	
		if(IsMiloRoom Obj) then
		(
			ProcessObject Obj (random black white)
		)
		
		Total += Increment
		pb1.value = Total 
	)
	
	destroyDialog RSL_RG_ColorByRoom
	)
)

createDialog RSL_RG_ColorByRoom style:#(#style_toolwindow,#style_sysmenu)