
try (destroyDialog RSL_RG_FixSelectedXRefs) catch()

rollout RSL_RG_FixSelectedXRefs "Show materials that are used on selected objects" width:176 height:32
(
	progressBar pb1 "ProgressBar" pos:[8,8] width:160 height:16

	fn FixXRef OldXRef = 
(
				--objXRefMgr.dupMtlNameAction = #useScene
				--objXRefMgr.dupObjNameAction = #autoRename
				--objXRefMgr.AddXRefItemsFromFile OldXRef.srcFileName objNames:OldXRef.srcItemName promptObjNames:false
	
				
				--XRefInterface = objXRefMgr.AddXRefItemsFromFile OldXRef.srcFileName objNames:OldXRef.srcItemName promptObjNames:false
				--NewXRef = XRefInterface.GetItem 1 #XRefObjectType
				
				--updateXRef NewXRef
				--print (classOf NewXRef)
	
				--SetQuietMode true
				NewXRef = xrefs.addNewXRefObject OldXRef.srcFileName  OldXRef.srcItemName
				NewXRef.transform = OldXRef.transform
				OldXRef.layer.addNode OldXRef
				NewXRef.isHidden = false;
				
				if OldXRef.parent != undefined then 
				(
					NewXRef.parent =  OldXRef.parent
				)
				
				--SetQuietMode false
				updateXref NewXRef
				delete OldXRef
)

fn CheckObject parentObj = 
(
	for childObj in parentObj.children do 
				(
				CheckObject childObj
				)
				
	if classof parentObj == XRefObject then 
	(
		if (parentObj.unresolved != true) then
		(
		FixXRef parentObj		
		)
	)
)
 
fn checkDialog = (
	local hwnd = dialogMonitorOps.getWindowHandle()
	if (uiAccessor.getWindowText hwnd == "Duplicate Material Name") then (
		-- uiAccessor.pressButtonByName hwnd "Use Merged Material"
		uiAccessor.pressButtonByName hwnd "Use Scene Material"
		-- uiAccessor.pressButtonByName hwnd "Auto-Rename Merged Material"
	)
	true
)

	on RSL_RG_FixSelectedXRefs open do
	(
		DialogMonitorOPS.unRegisterNotification id:#DuplicateMaterialDialog
		dialogMonitorOps.enabled = true
		dialogMonitorOps.interactive = false
		dialogMonitorOps.registerNotification checkDialog id:#DuplicateMaterialDialog
		
		--RemoveMaterialPreview()
		ObjectSelection = selection as array
		clearSelection()
		Increment = 100.0/ObjectSelection.count as float
		Total = 0.0
        for obj in ObjectSelection do
        (
		CheckObject obj
		Total += Increment
		pb1.value = Total 
		)
		
		
		dialogMonitorOps.enabled = false
	destroyDialog RSL_RG_FixSelectedXRefs
	)
)

createDialog RSL_RG_FixSelectedXRefs style:#(#style_toolwindow,#style_sysmenu)