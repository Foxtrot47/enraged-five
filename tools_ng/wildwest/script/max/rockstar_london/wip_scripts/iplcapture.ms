--global processOutput_RLT
 --try (destroyDialog processOutput_RLT) catch()
 
 /*
 rollout processOutput_RLT "Results" width:640 height:350
 (
 	dotnetControl text_RTB "RichTextBox" width:640 height:350 offset:[-14,-4]
 	
 	on processOutput_RLT open do
 	(
 		text_RTB.BorderStyle = text_RTB.BorderStyle.None
 		text_RTB.BackColor = text_RTB.BackColor.WhiteSmoke
 		text_RTB.ReadOnly= true
 	)
 )
 */
 
 --createDialog processOutput_RLT 
 
 
 fn outputDataReceivedHandler obj evt =
 (
 	if evt.data != undefined then
 	(
		
		if getFilenameType (evt.data) == ".ipl" then
		(
			
			print (evt.data)
		)
		

 	)
 )
 
/*
 fn errorDataReceivedHandler obj evt =
 (
 	if evt.data != undefined then
 	(
 		processOutput_RLT.text_RTB.text += ("Error: " + evt.data + "\n")
 		format "Error: %\n" evt.data
 	)
 )
 */

 /*
 fn onExitHandler obj evt =
 (
 	processOutput_RLT.text_RTB.text += "Finished!"
 	format "Finished!\n"
 )
 */
 
 proc = dotNetObject "System.Diagnostics.Process"
 proc.EnableRaisingEvents = true
 -- add program info here
 proc.StartInfo.WorkingDirectory = "x:\\tools\\bin\\"
 proc.StartInfo.FileName = "x:\\tools\\bin\\ragebuilder_0327.exe"
 proc.StartInfo.Arguments = "x:\\tools\\bin\\script\\InspectImage.rbs -input X:\\payne\\build\\dev\\independent\\levels\\c_pana\\pana_doc.img"
 -- x:\\tools\\bin\\script\\InspectImage.rbs -input X:\\payne\\build\\dev\\independent\\levels\\t_anim\\t_anim.img
 --
 proc.StartInfo.UseShellExecute = false
 proc.StartInfo.RedirectStandardOutput = true
 proc.StartInfo.RedirectStandardError = false
 proc.StartInfo.CreateNoWindow = true
  
 dotNet.AddEventHandler proc "OutputDataReceived" outputDataReceivedHandler
 --dotNet.AddEventHandler proc "ErrorDataReceived" errorDataReceivedHandler
 --dotNet.AddEventHandler proc "Exited" onExitHandler
  
 proc.Start()
 
 --proc.BeginErrorReadLine()
 proc.BeginOutputReadLine()