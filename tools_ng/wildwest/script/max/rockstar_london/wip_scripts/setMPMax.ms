maxmodelname = "MaxPayne"
MPmaxmodelname = "mp_MaxPayne"
MpTex = "X:\\payne\\payne_art\\characters\\multiplayer\\Male\\MP_MaxPayne\\texture\\"

try (destroyDialog RSL_CharacterPED) catch()
--
-- Process the Max model

rollout RSL_CharacterPED "RS Character Ped Packer" width:170
(

	
	button btn_Process "AddtoPedPack" width:160 align:#center	
	edittext txt_MPtextDir ""
	
	checkbox chk_useIndex "Use Index" checked:true across:2
	spinner spn_index "" range:[0,16,1] type:#integer
	
	
	fn GetIndex =
	(
		local indexval = ""			
		local index = 	spn_index.value
		
		if index > 9 then 
		(
			indexval = "0"+index as string
		)
		else
		(
			indexval = "00"+index as string
		)
		
		indexval
	)
	
	
	fn UpdateCharacterIndex = 
	(
		local rootmatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\w+._)([0-9][0-9][0-9])(_.)")
		
		local rootmodelnode = $
		if rootmodelnode != undefined then	(
			for i in rootmodelnode.children do
			(
				if getattrclass i == "Gta Object" then		(
					local DSTmatch = rootmatch.Match i.name
					if DSTmatch.Success then
					(
						index = GetIndex()
						local newname = (DSTmatch.groups.item[1].value)+index+(DSTmatch.groups.item[3].value)	
						print newname
						i.name = newname
					)
					
				)
			)
		)
		
	)
	
--
	fn setMPMax = (	
		
		local nymax = #("uppr_000_u","lowr_000_U","feet_000_U","hair_000_U","hand_000_R","head_000_r","teef_000_u")  -- rename to this
		--lowr_007_U
		local current = #("uppr_003_U","lowr_003_U","feet_001_U","hair_000_U","hand_001_R","head_000_r","teef_000_u") -- NY MAX PAYNE
		
		
		local rootmodelnode = getnodebyname maxmodelname
		if rootmodelnode != undefined then	(
			for i in rootmodelnode.children do
			(
				if getattrclass i == "Gta Object" then		(
					local valid = false
					local index = undefined
					for x = 1 to current.count do
					(
						if tolower (i.name) == tolower(current[x]) then (
							valid = true
							index = x
						)
					)
					if valid != true then (
						print ("Deleting Obj: "+i as string)
						delete i
						
					)
					else	(
						i.name = nymax[index]
						
					)
				)
			)
		)
		rootmodelnode.name =  MPmaxmodelname
	)

	--
	-- Changes the sting to a 000 type string
	-- Uses Regex to fix up name
	fn fixstring matpath = (
		local newname ;
		soureTex = filenamefrompath matpath
		local rootmatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\w+._)([0-9][0-9][0-9])(.+)")
		local DSTmatch = rootmatch.Match soureTex
		--print (DSTmatch.groups.item[3].value)
		--- Rebuild Name ---
		
		indexval = GetIndex()
	
		
		if chk_useIndex.checked then
		(
			newname= (DSTmatch.groups.item[1].value)+indexval+(DSTmatch.groups.item[3].value)	
		)
		else
		(
			newname = (DSTmatch.groups.item[1].value)+"000"+(DSTmatch.groups.item[3].value)	
		)
		newname
			

	)

	--
	-- Simple File Exits funtion
	--
	fn existFile fname = (getfiles fname).count != 0

	--
	-- Copy Tetxures over
	--
	fn copytextures textureList = (
		--copyFile
		for texname in textureList do
		(
			local newfile = MpTex + texname[2] -- New file location
			local soureFile = texname[1]
			if (existFile newfile) then (
				
				setFileAttribute newfile #readOnly false
				deleteFile newfile
			)
			if (existFile soureFile) then (
				local result = copyFile soureFile newfile
				print ("Copied File: "+soureFile + " : "+newfile +" :"+result as string)			
			)
		)
	)


	
	
	fn UpdatedTextureIndex = (
		
		local rootmodelnode = $
		
		local texturelist = #()
		if rootmodelnode != undefined then (
			for model in rootmodelnode.children do (
				matid = model.material
				if classof matid == Rage_Shader then	(
					matVar = RstGetVariableCount matid
					for x = 1 to matVar do	(
						if RstGetVariableType  matid x == "texmap" then	(
							materialname = RstGetVariable matid x
								if materialname != "" then (
									local newMatname = fixstring(materialname)
									append texturelist #(materialname,newMatname)
									RstSetVariable matid x (MpTex +newMatname)
										
								)
							)
						)
					)
				)
			)
		print texturelist
		copytextures texturelist
		
	)
	
	
	
	--
	-- Process the MP tetxures
	--
	fn setMPTextures = (
		local rootmodelnode = getnodebyname MPmaxmodelname
		local texturelist = #()
		if rootmodelnode != undefined then (
			for model in rootmodelnode.children do (
				matid = model.material
				if classof matid == Rage_Shader then	(
					matVar = RstGetVariableCount matid
					for x = 1 to matVar do	(
						if RstGetVariableType  matid x == "texmap" then	(
							materialname = RstGetVariable matid x
								if materialname != "" then (
									local newMatname = fixstring(materialname)
									append texturelist #(materialname,newMatname)
									RstSetVariable matid x (MpTex +newMatname)
										
								)
							)
						)
					)
				)
			)
		print texturelist
		copytextures texturelist
	)
	
	
	on RSL_CharacterPED open do
	(
		txt_MPtextDir.text = MpTex
		print spn_index.value
	)
	
	on btn_Process pressed do
	(
		UpdateCharacterIndex()
		UpdatedTextureIndex()
	)
	

)

createDialog RSL_CharacterPED style:#(#style_toolwindow,#style_sysmenu)

--setMPMax()

--setMPTextures()