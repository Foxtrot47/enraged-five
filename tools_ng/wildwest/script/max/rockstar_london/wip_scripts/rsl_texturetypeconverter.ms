
try (destroyDialog RSL_textureTypeConverter) catch()

rollout RSL_textureTypeConverter "RS Texture Type Converter" width:192
(
	local fileTypeArray = #(".bmp", ".tga", ".tif", ".png", ".dds")
	local hasAlphaArray = #(".tga", ".png", ".dds")
	local mapArray = getClassInstances bitmaptexture
	local sceneFileTypeArray = for map in mapArray where map.filename != undefined collect (getFilenameType map.filename)
	local currentTypeMapArray = #()
	local currentSelected = #()
	local progressCount = 1
	
	rollout RSL_showProgress "Converting:" width:192
	(
		progressBar pb_convert color:orange width:178 align:#center
	)
	
	mapped fn gatherCurrentType map type =
	(
		local fileType = (getFilenameType map.filename)
		
		if fileType == type then
		(
			appendIfUnique currentTypeMapArray map
		)
	)
	
	mapped fn convert map type =
	(
		local oldFilename = map.filename
		
		RSL_showProgress.title = "Converting: " + (getFilenameFile oldFilename)
		RSL_showProgress.pb_convert.value = (progressCount / currentTypeMapArray.count as float) * 100.0
		
		if oldFilename != undefined and (doesFileExist oldFilename) then
		(
			local newFilename = (getFilenamePath oldFilename) + (getFilenameFile oldFilename) + type
			
			if not (doesFileExist newFilename) then
			(
				if type == ".tif" then TIF.itifio.setAlpha #true
				
				local newBitmap = bitmap map.bitmap.width map.bitmap.height filename:newFilename
				
				pasteBitmap map.bitmap newBitmap [0,0] [0,0]
				
				save newBitmap
				close newBitmap
				newBitmap = undefined
			)
			
			map.filename = newFilename
		)
		
		if (mod progressCount 10) == 0.0 then gc()
		
		progressCount += 1
	)
	
	button btn_update "Update From Scene" width:168
	dropdownlist ddl_fromType "From:"
	multiListBox lbx_files " files in the scene..." height:24
	button btn_all "All" width:54 across:3
	button btn_inv "Invert" width:54
	button btn_none "None" width:54
	dropdownlist ddl_toType "To:" items:fileTypeArray
	button btn_convert "Convert" width:168 enabled:false
	
	on RSL_textureTypeConverter open do
	(
		sceneFileTypeArray = makeUniqueArray sceneFileTypeArray
		
		ddl_fromType.items = sceneFileTypeArray
		
		lbx_files.text = ddl_fromType.selected + " files in the scene."
		
		gatherCurrentType mapArray ddl_fromType.selected
		
		lbx_files.items = (for item in currentTypeMapArray collect (filenameFromPath item.filename))
		
		ddl_fromType.items = sceneFileTypeArray
	)
	
	on btn_update pressed do
	(
		local mapArray = getClassInstances bitmaptexture
		
		sceneFileTypeArray = for map in mapArray where map.filename != undefined collect (getFilenameType map.filename)
		
		sceneFileTypeArray = makeUniqueArray sceneFileTypeArray
		
		ddl_fromType.items = sceneFileTypeArray
	)
	
	on ddl_fromType selected index do
	(
		lbx_files.text = ddl_fromType.selected + " files in the scene."
		
		currentTypeMapArray = #()
		
		gatherCurrentType mapArray ddl_fromType.selected
		
		lbx_files.items = (for item in currentTypeMapArray collect (filenameFromPath item.filename))
		
		if ddl_fromType.selected != ddl_toType.selected and lbx_files.items.count != 0 then
		(
			btn_convert.enabled = true
		)
		else
		(
			btn_convert.enabled = false
		)
	)
	
	on btn_all pressed do
	(
		lbx_files.selection = (for i = 1 to lbx_files.items.count collect i)
	)
	
	on btn_inv pressed do
	(
		local tempBitArray = #{}
		tempBitArray.count = lbx_files.selection.count
		
		for i = 1 to lbx_files.selection.count do
		(
			tempBitArray[i] = not lbx_files.selection[i]
		)
		
		lbx_files.selection = tempBitArray
	)
	
	on btn_none pressed do
	(
		lbx_files.selection = 0
	)
	
	on ddl_toType selected index do
	(
		if ddl_fromType.selected != ddl_toType.selected and lbx_files.items.count != 0 then
		(
			btn_convert.enabled = true
		)
		else
		(
			btn_convert.enabled = false
		)
	)
	
	on btn_convert pressed do
	(
		local okToContinue = true
		
		if ddl_toType.selected == ".bmp" then
		(
			okToContinue = queryBox "You are about convert to .bmp. Any alpha channel information will not be included in the converted textures.\n Are you sure you want to continue?" title:"Warning..."
		)
		
		if okToContinue then
		(
			progressCount = 1
			
			try (destroyDialog RSL_showProgress) catch()
			createDialog RSL_showProgress style:#(#style_toolwindow,#style_sysmenu)
			
			currentSelected = for i = 1 to lbx_files.selection.count where lbx_files.selection[i] collect currentTypeMapArray[i]
			
			convert currentSelected ddl_toType.selected
			
			destroyDialog RSL_showProgress
			
			freeSceneBitmaps()
			
			currentTypeMapArray = #()
			
			gatherCurrentType mapArray ddl_fromType.selected
			
			lbx_files.items = (for item in currentTypeMapArray collect (filenameFromPath item.filename))
			
			local mapArray = getClassInstances bitmaptexture
			
			sceneFileTypeArray = for map in mapArray where map.filename != undefined collect (getFilenameType map.filename)
			sceneFileTypeArray = makeUniqueArray sceneFileTypeArray
			
			local lastType = ddl_fromType.selected
			
			ddl_fromType.items = sceneFileTypeArray
			
			local index =  findItem ddl_fromType.items lastType
			
			if index != 0 then
			(
				ddl_fromType.selection = index
			)
			else
			(
				lbx_files.text = ddl_fromType.selected + " files in the scene."
				
				currentTypeMapArray = #()
				
				gatherCurrentType mapArray ddl_fromType.selected
				
				lbx_files.items = (for item in currentTypeMapArray collect (filenameFromPath item.filename))
			)
		)
	)
)

createDialog RSL_textureTypeConverter style:#(#style_toolwindow,#style_sysmenu)