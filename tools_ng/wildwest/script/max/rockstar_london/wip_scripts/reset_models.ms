fn returnchildarray NodeChildren =
(
	local temparray = #()
	for eachchild in NodeChildren do
	(
		append temparray eachchild
	)
	temparray
)

fn addChildrenFromArray obj childrenArray =
(

	for eachChild in childrenArray do
	(

		append obj.children eachChild
	)
	
	
)


clearlistener()
fn generateValidObjList =
(
	validObjList = #()
	list = geometry as array
	for obj in list do
	(
		if getattrclass obj == "Gta Object" and classof obj != XRefObject then
		(
			append validObjList obj
		)
	)
	validObjList
)




fn main =
(

	objList = generateValidObjList()
	index = getattrindex "Gta Object" "TXD"
	for eachObj in objList do
	(
		--print eachObj
		
		local parentName = eachObj.parent
		local childrenNames = eachObj.children	
		--local objectmaterial = eachObj.material
		
		childList = #()
		for eachChild in childrenNames do
		(
			append childList eachChild
		)
		
		--print ("childern = "+childrenNames as string)
		
		
		TXDNAME =(getattr eachObj index)
		
		local newobj = resetObj eachObj
		
		SetAttr newobj index TXDNAME
		
		--newobj.material = objectmaterial
		
		newobj.parent = parentName	
		
		
		
		if childList.count > 0 then 
		(
			addChildrenFromArray newobj childList
		)
		
		--addChildrenFromArray newobj childrenNames
		
		

		
	)

	
	
)


fn resetObj obj =
(	
	

	local tempmaterial = obj.material
	
	local x = box()
	x.pos = obj.pos
	convertToMesh x
	n = obj.name	
	convertToMesh obj
	attach x obj
	x.name = n
	convertToPoly x
	--------------------
	a = polyop.getElementsUsingFace X 1
	polyop.deleteFaces X A
	--------------------
	ResetXForm x
	CenterPivot x
	collapseStack x
	x.backfacecull = true
	------------------------
	
	x


)

main()

