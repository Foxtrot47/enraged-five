filein "gta\\utils\\gtautilityfunc.ms"
filein "rockstar\\util\\file.ms"
filein "rockstar\\export\\settings.ms"
filein "rockstar\\export\\mapsetup.ms"

global InitLODTL
--LOD TreeList

struct LODTLStruct
(
	
	----------------------------------------------------------------------------------------------------------------
	--VARIABLES
	----------------------------------------------------------------------------------------------------------------
	TLWin,						--variable to store the window
	daddyList,					--array to hold all the root objects in the tree list
	tl,								--variable to store tree list object
	buttonPanel,				--variable to store the button panel
	infoPanel,					--variable to store the info panel label at the bottom of the screen
	nodeArray,					--array to hold all the objects in the tree list
	mainTable,					--variable to store the table layout
	LDInput,						--spinner control to change LOD distance
	LDLabel,						--label for LOD distance panel
	newLD,						--variable to store the new value for LOD distance
	selectedNodes = #(),	--array to store nodes selected in the tree list
	selectedObjects = #(),	--array to store objects selected in the scene
	buttonPanelHeight, infoHeight, winHeight,	TLRowHt = 19, borderHt = 72,
	selectedList = for this in selection where (getAttrClass this) == "Gta Object" and (classOf this) != XrefObject collect this,
	objectList = for this in objects where (getAttrClass this) == "Gta Object" and (classOf this) != XrefObject collect this,
	winWidth = 460,
	
	----------------------------------------------------------------------------------------------------------------
	--PRESETS
	----------------------------------------------------------------------------------------------------------------

	DS_Fill = (dotNetClass "System.Windows.Forms.DockStyle").fill,
	TA_center =  (dotNetClass "System.Drawing.ContentAlignment").MiddleCenter,
	TA_left =  (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft,
	Font_Lucida = dotNetObject "System.Drawing.Font" "Lucida Console" 8,
	
	--colours
	RGB_LimeGreen = (dotNetClass "System.Drawing.Color").fromARGB 204 255 0,
	RGB_YellowOrange = (dotNetClass "System.Drawing.Color").fromARGB 255 204 0,
	RGB_Orange  = (dotNetClass "System.Drawing.Color").fromARGB 255 153 0,
	RGB_PaleOrange  = (dotNetClass "System.Drawing.Color").fromARGB 255 245 225,
	RGB_Pink = (dotNetClass "System.Drawing.Color").fromARGB 255 0 204,
	RGB_Purple = (dotNetClass "System.Drawing.Color").fromARGB 204 0 255,
	RGB_SkyBlue = (dotNetClass "System.Drawing.Color").fromARGB 0 205 255,
	RGB_PaleBlue = (dotNetClass "System.Drawing.Color").fromARGB 235 245 255,
	RGB_Black = (dotNetClass "System.Drawing.Color").black,
	RGB_DarkGrey = (dotNetClass "System.Drawing.Color").fromARGB 102 102	102,
	RGB_MidGrey = (dotNetClass "System.Drawing.Color").fromARGB 153 153 153,
	RGB_PaleGrey = (dotNetClass "System.Drawing.Color").fromARGB 204 204 204,
	RGB_LightGrey = (dotNetClass "System.Drawing.Color").fromARGB 229 229 229,
	RGB_OffWhite = (dotNetClass "System.Drawing.Color").fromARGB 241 241 241,
	RGB_White = (dotNetClass "System.Drawing.Color").white,
	RGB_Red = (dotNetClass "System.Drawing.Color").red,
	RGB_DarkRed = (dotNetClass "System.Drawing.Color").fromARGB 102 0 0,
	
	----------------------------------------------------------------------------------------------------------------
	--TREE LIST FUNCTIONS
	----------------------------------------------------------------------------------------------------------------
	
	fn InitDaddyList =
		--function gets a list of all the root objects in the scene for the tree list
	(
		objectList = for this in objects where (getAttrClass this) == "Gta Object" and (classOf this) != XrefObject collect this
		daddyList = #()
		for this in objectList do
		(
			if getlodattrparent this == undefined then append daddyList this
		)
	),
	
	fn PopulateTL =
	--function picks up all the objects in the scene, reads the LOD hierarchy and then places puts the data into the treelist
	--selected objects highlighted are those whose scene objects are selected
	--nodeArray updated in this function
	(
		tl.BeginUnboundLoad()
		InitDaddyList()
		--add the root objects
		nodeArray = #()
		for this in daddyList do
		(
			distanceValue = getattr this (getattrindex "Gta Object" "LOD distance")
			txdValue = getattr this (getattrindex "Gta Object" "TXD")
			gdaddyNode = tl.AppendNode #(this.name, distanceValue, txdvalue) -1		-- value of -1 sets root object
			gdaddyNode.tag = dotNetMXSValue this
			append nodeArray gdaddyNode
			childList = getLODChildren this
 			if childList.count > 0 then
			(
				for thisChild in childList do
				(
					distanceValue = getattr thisChild (getattrindex "Gta Object" "LOD distance")
					txdValue = getattr this (getattrindex "Gta Object" "TXD")
					daddyNode = tl.AppendNode #(thisChild.name, distanceValue, txdvalue) gdaddyNode
					daddyNode.tag = dotNetMXSValue thisChild
					append nodeArray daddyNode
					gchildList = getLODChildren thisChild
					if gchildList.count > 0 then
					(
						for thisgchild in gchildlist do
						(
							distanceValue = getattr thisgchild (getattrindex "Gta Object" "LOD distance")
							txdValue = getattr this (getattrindex "Gta Object" "TXD")
							childNode = tl.AppendNode #(thisgchild.name, distanceValue, txdvalue) daddyNode
							childNode.tag = dotNetMXSValue thisgchild
							append nodeArray childNode
						)
					)
				)
			)
		)
		tl.EndUnboundLoad()
		
		--when the table inits, one node is incorrectly selected, so need to deselect all the nodes
		for this in nodeArray do
		(
			this.Selected = false
			this.Expanded = true
		)
		
		--select all nodes whose scene objects are selected
		selectedObjects = for this in selection where (getAttrClass this) == "Gta Object" and (classOf this) != XrefObject collect this
		for this in nodeArray do
		(
			for thisObject in selectedObjects do
			(
				if this.tag.value == thisObject then
				(
					this.Selected = true
				)
			)
		)
	),
	
	fn InitTL =
	--function sets up the initial tree list when the script is triggered
	(
		tl = dotNetObject "DevExpress.XtraTreeList.TreeList"
		tl.Dock = DS_Fill
		tl.OptionsSelection.MultiSelect = true
		tl.OptionsView.ShowRoot = true
		tl.OptionsView.ShowIndicator = false
		tl.OptionsBehavior.Editable = false 						-- set to true on right click...
		tl.OptionsView.EnableAppearanceEvenRow = true	--colours the rows alternately
		tl.OptionsView.EnableAppearanceOddRow = true	--colours the rows alternately
		
		tl.BeginUpdate()
		
		--first column for tree nodes
		tl.Columns.Add()
		tl.Columns.Item[0].Caption = "Object Tree"
		tl.Columns.Item[0].Name = "tree"
		tl.Columns.Item[0].VisibleIndex = 0
		tl.Columns.Item[0].Width = 200
		tl.columns.item[0].sortorder = (dotNetClass "SortOrder").ascending
		
		--second column for LOD distances
		tl.Columns.Add()
		tl.Columns.Item[1].Caption = "LOD Distance"
		tl.Columns.Item[1].Name = "distance"
		tl.Columns.Item[1].VisibleIndex = 1
		tl.Columns.Item[1].Width = 60
		
		--third column for TXD
		tl.Columns.Add()
		tl.Columns.Item[2].Caption = "TXD"
		tl.Columns.Item[2].Name = "TXD"
		tl.Columns.Item[2].VisibleIndex = 2
		tl.Columns.Item[2].Width = 120
		
		tl.EndUpdate()
	),
	
	fn UpdateTL =
	--useful function to update the TL
	(
		selectedNodes = #()
		selectedObjects = #()
		 for this in nodeArray where (this.Selected == true) and (isValidNode this.tag.value == true) do
		(
			append selectedNodes this
			append selectedObjects this.Tag.Value
		)
		tl.ClearNodes()
		PopulateTL()
		
		--restore info panel if it doesn't exist
		if mainTable.Controls.Item[2].Name != "infoPanel" then
		(
			mainTable.Controls.Remove mainTable.Controls.Item[2]
			InitLODTL.InitInfoPanel()
		)
		
		--objectList = for this in objects where (getAttrClass this) == "Gta Object" and (classOf this) != XrefObject collect this
		--winHeight = borderHt + (objectList.count * TLRowHt) + buttonPanelHeight + infoHeight
		winHeight = borderHt + (nodeArray.count * TLRowHt) + buttonPanelHeight + infoHeight
		TLWin.height = winHeight - 14	--nb: this is a hack, don't know why I have to adjust the figure by 14... but it works...
		UpdateInfoPanel()
		messagebox "asdlfkjasdlfk"
	),
	
	fn UpdateInfoPanel =
	(
		selectedObjects = for this in nodeArray where (this.Selected == true) and (isValidNode this.Tag.Value == true) collect this.Tag.Value
		infoPanel.text = (nodeArray.Count as string) + " object(s) in list, " + (selectedObjects.Count as string) + " object(s) selected"		
	),
	
	----------------------------------------------------------------------------------------------------------------
	--EVENT HANDLERS
	----------------------------------------------------------------------------------------------------------------
	
	--apply LOD distance
	fn DispatchApplyLD s e =
	(
		InitLODTL.ApplyLD s e
		
	),
	
	fn ApplyLD s e =
	(
		--find selected nodes and select their objects
		selectedNodes = #()
		selectedObjects = #()
		for this in nodeArray where (this.Selected == true) and (isValidNode this.tag.value == true) do
		(
			append selectedNodes this
			append selectedObjects this.Tag.Value
		)
		
		if selectedNodes.count == 0 then
		(
			LDLabel.text = "no objects selected"
		)
		else
		(
			select selectedObjects
			
			--apply new value
			newLD = LDInput.Value
			for this in selectedNodes do
			(
				setAttr this.tag.value (getattrindex "Gta Object" "LOD distance") newLD
			)
			
			--update treelist
			tl.ClearNodes()
			PopulateTL()
			objectList = for this in objects where (getAttrClass this) == "Gta Object" and (classOf this) != XrefObject collect this
			winHeight = borderHt + (objectList.count * TLRowHt) + buttonPanelHeight + infoHeight
			TLWin.height = winHeight - 14
			for this in selectedNodes do
			(
				this.Selected = true
			)
		)
	),
	
	----------------------------------------------------------------------------------------------------------------
	
	--select scene > list
	fn DispatchSceneToList s e =
	(f
		InitLODTL.SceneToList s e
	),
	
	fn SceneToList s e =
	(
		selectedNodes = #()
		for this in nodeArray where (this.Selected == true) do
		(
			append selectedNodes this
		)
		
		tl.ClearNodes()
		PopulateTL()
		
		if mainTable.Controls.Item[2].Name != "infoPanel" then
		(
			mainTable.Controls.Remove mainTable.Controls.Item[2]
			InitLODTL.InitInfoPanel()
		)
		
		objectList = for this in objects where (getAttrClass this) == "Gta Object" and (classOf this) != XrefObject collect this
		winHeight = borderHt + (objectList.count * TLRowHt) + buttonPanelHeight + infoHeight
		TLWin.height = winHeight - 14	--nb: this is a hack, don't know why I have to adjust the figure by 14... but it works...
		UpdateInfoPanel()
	),
	
	----------------------------------------------------------------------------------------------------------------
	
	--exapand all
	fn DispatchExpand s e =
	(
		InitLODTL.Expand s e
	),
	
	fn Expand s e =
	(
		for this in nodeArray do
		(
			this.expanded = true
		)
	),
	
	----------------------------------------------------------------------------------------------------------------
	
	--contract all
	fn DispatchContract s e =
	(
		InitLODTL.Contract s e
	),
	
	fn Contract s e =
	(
		for this in nodeArray do
		(
			this.expanded = false
		)
	),
	
	----------------------------------------------------------------------------------------------------------------
	
	--select list > scene
	fn DispatchListToScene s e =
	(
		InitLODTL.ListToScene s e
	),
	
	fn ListToScene s e =
	(
		selectedObjects = for this in nodeArray where (this.Selected == true) and (isValidNode this.tag.value == true) collect this.tag.value
		--isValidNode needed to check if the scene object has already been deleted
		if mainTable.Controls.Item[2].Name != "infoPanel" then
		(
			mainTable.Controls.Remove mainTable.Controls.Item[2]
			InitLODTL.InitInfoPanel()
		)
		select selectedObjects
		
		tl.ClearNodes()
		PopulateTL()
		
		--restore info panel if it doesn't exist
		if mainTable.Controls.Item[2].Name != "infoPanel" then
		(
			mainTable.Controls.Remove mainTable.Controls.Item[2]
			InitLODTL.InitInfoPanel()
		)
		
		objectList = for this in objects where (getAttrClass this) == "Gta Object" and (classOf this) != XrefObject collect this
		winHeight = borderHt + (objectList.count * TLRowHt) + buttonPanelHeight + infoHeight
		TLWin.height = winHeight - 14	--nb: this is a hack, don't know why I have to adjust the figure by 14... but it works...
		UpdateInfoPanel()
	),
	
	----------------------------------------------------------------------------------------------------------------
	
	--set LOD distance
	fn DispatchSetLODDistance s e =
	(
		InitLODTL.SetLODDistance s e
	),
	
	fn SetLODDistance s e =
	(
		selectedObjects = for this in nodeArray where (this.Selected == true) and (isValidNode this.tag.value == true) collect this.tag.value
		if selectedObjects.count == 0 then
		(
			LDLabel.text = "no objects selected"
			updateTL()
		)
		else
		(
			if (mainTable.Controls.Item[2].Name != "LODDistancePanel") then
			(
				mainTable.Controls.Remove mainTable.Controls.Item[2]
				InitLODTL.InitLODDistancePanel()				
			)
		)
	),
	
	----------------------------------------------------------------------------------------------------------------
	--BUTTONS
	----------------------------------------------------------------------------------------------------------------
	
	fn InitButtons =
	(
		--buttonWidth = 110
		buttonWidth = (winWidth - 40)/4
		buttonHeight = 20
		
		button1 = dotNetObject "Button"
		button1.text = "Scene > List"
		button1.width = buttonWidth
		button1.height = buttonHeight
		button1.backColor = RGB_PaleOrange
		buttonPanel.Controls.Add button1
		dotNet.addEventHandler button1 "Click" DispatchSceneToList
		
		button2 = dotNetObject "Button"
		button2.text = "Expand All"
		button2.width = buttonWidth
		button2.height = buttonHeight
		button2.backColor = button2.backColor.silver
		buttonPanel.Controls.Add button2
		dotNet.addEventHandler button2 "Click" DispatchExpand
		
		button3 = dotNetObject "Button"
		button3.text = "Contract All"
		button3.width = buttonWidth
		button3.height = buttonHeight
		button3.backColor = button3.backColor.silver
		buttonPanel.Controls.Add button3
		dotNet.addEventHandler button3 "Click" DispatchContract
		
		button4 = dotNetObject "Button"
		button4.text = "List > Scene"
		button4.width = buttonWidth
		button4.height = buttonHeight
		button4.backColor = RGB_PaleBlue
		buttonPanel.Controls.Add button4
		dotNet.addEventHandler button4 "Click" DispatchListToScene
		
		button5 = dotNetObject "Button"
		button5.text = "Set LOD Distance"
		button5.width = buttonWidth
		button5.height = buttonHeight
		button5.backColor = button5.backColor.silver
		buttonPanel.Controls.Add button5
		dotNet.addEventHandler button5 "Click" DispatchSetLODDistance
		
		button6 = dotNetObject "Button"
		button6.text = "Set TXD"
		button6.width = buttonWidth
		button6.height = buttonHeight
		button6.backColor = button6.backColor.silver
		buttonPanel.Controls.Add button6
		
		button7 = dotNetObject "Button"
		button7.text = "Parent To LOD"
		button7.width = buttonWidth
		button7.height = buttonHeight
		button7.backColor = button7.backColor.whitesmoke
		buttonPanel.Controls.Add button7
		
		button8 = dotNetObject "Button"
		button8.text = "UnLOD"
		button8.width = buttonWidth
		button8.height = buttonHeight
		button8.backColor = button8.backColor.whitesmoke
		buttonPanel.Controls.Add button8
	),
	
	----------------------------------------------------------------------------------------------------------------
	--INFO PANEL
	----------------------------------------------------------------------------------------------------------------
		
	fn InitInfoPanel =
	(
		infoPanel = dotnetobject "Label"
		infoPanel.Dock = DS_Fill
		infoPanel.TextAlign = infoPanel.TextAlign.MiddleCenter
		infoPanel.Name = "infoPanel"
		mainTable.Controls.Add infoPanel
	),
	
	----------------------------------------------------------------------------------------------------------------
	--LOD DISTANCE PANEL
	----------------------------------------------------------------------------------------------------------------
		
	fn InitLODDistancePanel =
	(
		newLD = getAttr selectedObjects[1] (getattrindex "Gta Object" "LOD distance")
		subTable = dotNetObject "TableLayoutPanel"
		subTable.Dock = DS_Fill
		subTable.Name = "LODDistancePanel"
		subTable.CellBorderStyle = (dotNetClass "System.Windows.Forms.TableLayoutPanelCellBorderStyle").none
		subTable.ColumnCount = 3
		subTable.ColumnStyles.Add (RS_dotNetObject.columnStyleObject "percent" 70)
		subTable.ColumnStyles.Add (RS_dotNetObject.columnStyleObject "percent" 15)
		subTable.ColumnStyles.Add (RS_dotNetObject.columnStyleObject "percent" 15)
		subTable.RowCount = 1
		subTable.RowStyles.Add (RS_dotNetObject.rowStyleObject "percent" 100)
		mainTable.Controls.Add subTable
		
		LDLabel = dotNetObject "Label"
		LDLabel.Text = "Set new LOD distance:"
		LDLabel.Dock = DS_Fill
		LDLabel.TextAlign = LDLabel.TextAlign.MiddleCenter
		subTable.Controls.Add LDLabel
		
		LDInput = dotNetObject "NumericUpDown"
		LDInput.Minimum = 0
		LDInput.Maximum = 9999
		LDInput.Value = newLD
		LDInput.Increment = 10
		LDInput.Dock = DS_Fill
		subTable.Controls.Add LDInput
		
		LDButton = dotNetObject "Button"
		LDButton.Dock = DS_Fill
		LDButton.BackColor = RGB_LimeGreen
		LDButton.Text = "Apply"
		subTable.Controls.Add LDButton
		dotNet.addEventHandler LDButton "Click" DispatchApplyLD
	),
	
	----------------------------------------------------------------------------------------------------------------
	--UI
	----------------------------------------------------------------------------------------------------------------
	
	fn CreateUI =
	(		
		-- form setup
		buttonPanelHeight = 58
		infoHeight = 35
		winHeight = borderHt + (objectList.count * TLRowHt) + buttonPanelHeight + infoHeight
		TLWin = RS_dotNetUI.maxForm "LODTL" text:"LODTL" size:[winWidth,winHeight] min:[winWidth,100] max:[winWidth,1024]
		TLWin.sizeGripStyle = (dotNetClass "SizeGripStyle").show
		TLWin.StartPosition = TLWin.StartPosition.Manual
		TLWin.Location.X = 0
		TLWin.Location.Y = 120
		TLWin.FormBorderStyle  = TLWin.FormBorderStyle.FixedToolWindow		
		--content
		
		--table
		mainTable = dotNetObject "TableLayoutPanel"
		mainTable.Dock = DS_fill
		mainTable.CellBorderStyle = (dotNetClass "System.Windows.Forms.TableLayoutPanelCellBorderStyle").single
		mainTable.ColumnCount = 1
		mainTable.ColumnStyles.Add (RS_dotNetObject.columnStyleObject "percent" 100)
		mainTable.RowCount = 3
		mainTable.RowStyles.Add (RS_dotNetObject.rowStyleObject "percent" 100)
		mainTable.RowStyles.Add (RS_dotNetObject.rowStyleObject "absolute" buttonPanelHeight)
		mainTable.RowStyles.Add (RS_dotNetObject.rowStyleObject "absolute" infoHeight)
		TLWin.Controls.Add mainTable
		
		--tree list view
		InitTL()
		PopulateTL()
		mainTable.Controls.Add tl
		
		--button panel
		buttonPanel = dotNetObject "FlowLayoutPanel"
		buttonPanel.dock = DS_Fill
		buttonPanel.backColor = RGB_MidGrey
		buttonPanel.margin.all = 0
		mainTable.Controls.Add buttonPanel
		
		--buttons
		InitButtons()
		
		--informationPanel
		InitInfoPanel()
		UpdateInfoPanel()
		
		--draw form
		TLWin.showModeless()
		TLWin
		clearListener()
	)
)

if InitLODTL != undefined then
(
	InitLODTL.TLWin.close()
)

InitLODTL = LODTLStruct()
InitLODTL.CreateUI()