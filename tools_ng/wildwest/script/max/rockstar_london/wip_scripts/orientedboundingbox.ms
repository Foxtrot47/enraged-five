struct RSL_OBB
(
	size = [0,0,0],
	matrix
)

mapped fn calculateMaxMin pos &maximum &minimum =
(
	if pos.x > maximum.x then maximum.x = pos.x
	if pos.y > maximum.y then maximum.y = pos.y
	if pos.z > maximum.z then maximum.z = pos.z
	
	if pos.x < minimum.x then minimum.x = pos.x
	if pos.y < minimum.y then minimum.y = pos.y
	if pos.z < minimum.z then minimum.z = pos.z
)

fn OBBFromAngle vertPositions rotation =
(
	local maximum = [-999999999,-999999999,-999999999]
	local minimum = [999999999,999999999,999999999]
	
	calculateMaxMin vertPositions maximum minimum
	
	local size = (maximum - minimum)
	local center = ((maximum + minimum) * 0.5) * rotation
	local matrix = rotation as matrix3
	matrix.translation = center
	
	RSL_OBB size:size matrix:matrix
)


fn gatherVertextPositions object =
(
	local result = for vert in object.verts collect vert.pos
	
	if object.SelectedFaces.count > 0 then
	(
		case (classOf object.baseObject) of
		(
			editable_mesh:
			(
				result = for index in (meshOp.getVertsUsingFace object object.SelectedFaces) collect object.verts[index].pos
			)
			editable_poly:
			(
				result = for index in (polyOp.getVertsUsingFace object object.SelectedFaces) collect object.verts[index].pos
			)
		)
	)
	
	result
)

fn findOBB object angleStep maximumAngle baseMatrix =
(
	local vertPositions = gatherVertextPositions object
	local firstOBB = OBBFromAngle vertPositions (eulerToQuat (eulerAngles 0 0 0))
	
	local smallestOBB = firstOBB
	local smallestSize = smallestOBB.size.x + smallestOBB.size.y + smallestOBB.size.z
	
	for x = -maximumAngle to maximumAngle by angleStep do
	(
		for y = -maximumAngle to maximumAngle by angleStep do
		(
			for z = -maximumAngle to maximumAngle by angleStep do
			(
				local baseEuler = (baseMatrix as eulerAngles)
				local euler = eulerAngles x y z
				
				euler.x += baseEuler.x
				euler.y += baseEuler.y
				euler.z += baseEuler.z
				
				local quatRotation = eulerToQuat euler
				local currentVertPositions = (for pos in vertPositions collect (pos * (inverse quatRotation)))
				
				local currentOBB = (OBBFromAngle currentVertPositions quatRotation)
				local currentSize = currentOBB.size.x + currentOBB.size.y + currentOBB.size.z
				
				if currentSize < smallestSize then
				(
					smallestOBB = currentOBB
					smallestSize = currentSize
				)
			)
		)
	)
	
	smallestOBB
)

fn generateOBB =
(
	local theObject = selection[1]
	
	if theObject != undefined then
	(
		start = timeStamp()
		local bound = findOBB theObject 10.0 45.0 (matrix3 1)
		end = timeStamp()
		
		format "Processing took % seconds\n" ((end - start) / 1000.0)
		
		start = timeStamp()
		local bound = findOBB theObject 2 10.0 bound.matrix
		end = timeStamp()
		
		format "Processing took % seconds\n" ((end - start) / 1000.0)
		
-- 		b = box width:bound.size.x length:bound.size.y height:bound.size.z wireColor:green
-- 		b.pivot = [0,0,bound.size.z * 0.5]
-- 		b.transform = bound.matrix
		
		newCollisionMesh = Col_Cylinder name:(uniqueName (theObject.name + " - colBox"))
		newCollisionMesh.wireColor = orange
		
		newCollisionMesh.transform = bound.matrix
		
		
-- 		newCollisionMesh.length = bound.size.x * 0.5
-- 		newCollisionMesh.width = bound.size.y * 0.5
-- 		newCollisionMesh.height = bound.size.z * 0.5
		
		newCollisionMesh.radius = (amin #(bound.size.x, bound.size.y, bound.size.z)) * 0.5
		
		newCollisionMesh.length = amax #(bound.size.x, bound.size.y, bound.size.z)
		
	)
)

generateOBB()
