fn SelectNodes ObjectArray =
(
	for obj in ObjectArray do
        (
			selectMore obj
		)
)

fn AddObjectToLayer Obj LayerName LayerColor =
(
	
		ThisLayer = layermanager.getLayerFromName LayerName
		if (ThisLayer == undefined) then 
	(
		ThisLayer = LayerManager.newLayerFromName LayerName
	)

		ThisLayer.wireColor = LayerColor 
		ThisLayer.addnode Obj
)

fn IsShapeType Obj =
(
	if (superclassof Obj == shape) then
		( return true )
		else
		( return false )
)

fn IsParticleType Obj =
(
if (classof Obj == RAGE_Particle) then
		( return true )
		else
		( return false )
)

fn IsLightType Obj =
(
	if (superclassof Obj == light) then
		( return true )
		else
		( return false )
)

fn IsCollisionType Obj =
(
	case classOf Obj of
	(
		(Col_Mesh): return true
		(Col_Sphere): return true
		(Col_Box): return true
		(Col_Plane): return true
		(Col_Cylinder): return true
		(Shad_Mesh): return true
	)
	return false
)

fn IsXRefType Obj =
(
	if (classof Obj == XRefObject) then
		( return true )
		else
		( return false )
)

fn IsGeometryType Obj =
(
	if (superclassof Obj == GeometryClass) then
		( 
		if(classOf Obj != XRefObject) then (return true)
		)
		
	return false
)

fn IsShell Obj = 
(

)

fn IsMiloRoom Obj =
(
	if (classof Obj == GtaMloRoom) then
		( return true )
		else
		( return false )
)

fn IsMilo Obj =
(
	if (classof Obj == Gta_MILO) then
		( return true )
		else
		( return false )
)

