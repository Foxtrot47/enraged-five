try (destroyDialog RSL_IPLStream) catch ()

rollout RSL_IPLStream "IPL Stream Helpers" width:270 height:494
(
	local directoryList = #()
	local IPLBoxArray = #()
	
	
	fn getIPLBoxByID ID =
	(
		local result = undefined
		
		for object in helpers where (classOf object) == IPLStreamingBox do
		(
			if object.u32BoxID == ID then
			(
				result = object
			)
		)
		
		result
	)
	
	--------------------------------------------------------------------------------------------
	-- 						updateIPLBoxes()
	--------------------------------------------------------------------------------------------
	fn updateIPLBoxes listBox =
	(
		local currentParentList = #()
		
		for object in helpers where (classOf object) == IPLStreamingBox do
		(
			append currentParentList object.name
			append IPLBoxArray object
		)
		
		listBox.items = currentParentList
	)
	
	--------------------------------------------------------------------------------------------
	-- 						getBoundsFromIPL()
	--------------------------------------------------------------------------------------------
	fn getBoundsFromIPL file xArray yArray zArray =
	(
		local keymatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\w+,\s[0-9]*,\s([-]*[0-9]*.[0-9]*),\s([-]*[0-9]*.[0-9]*),\s([-]*[0-9]*.[0-9]*),)")
		local dataStream = openFile file mode:"r"
		
		skiptostring dataStream "inst"
		
		while not eof dataStream do
		(
			local linematch = keymatch.Match (readline dataStream)
			
			if (linematch.success) then
			(
				append xArray (linematch.groups.item[2].value as float)
				append yArray (linematch.groups.item[3].value as float)
				append zArray (linematch.groups.item[4].value as float)
			)
		)
		
		close dataStream
	)
	
	--------------------------------------------------------------------------------------------
	-- 						gatherBoundsFromIPLs()
	--------------------------------------------------------------------------------------------
	fn gatherBoundsFromIPLs mapPath =
	(
		local result  = false
		local iplposX = #()
		local iplposY = #()
		local iplposZ = #()
		
		local IPLFiles = getFiles mapPath
		
		if IPLFiles.count > 0 then
		(
			result  = #([0,0,0],[0,0,0])
			
			for file in IPLFiles do
			(
				getBoundsFromIPL file iplposX iplposY iplposZ
			)
			
			result[1].x = amin iplposX
			result[1].y = amin iplposY
			result[1].z = amin iplposZ
			
			result[2].x = amax iplposX
			result[2].y = amax iplposY
			result[2].z = amax iplposZ
		)
		
		result
	)
	
	--------------------------------------------------------------------------------------------
	-- 						getBoundsFromIDE()
	--------------------------------------------------------------------------------------------
	fn getBoundsFromIDE mapPath =
	(
		local result  = false
		local iplposX = #()
		local iplposY = #()
		local iplposZ = #()
		
		local keymatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\w+,\s[0-9]*,\s([-]*[0-9]*.[0-9]*),\s([-]*[0-9]*.[0-9]*),\s([-]*[0-9]*.[0-9]*),)")
		local IDEFiles = getFiles mapPath
		
		if IDEFiles.count == 1 then
		(
			result  = #([0,0,0],[0,0,0],"")
			
			local dataStream = openFile IDEFiles[1] mode:"r"
			
			skiptostring dataStream "mlo"
			line = readLine dataStream
			line = readLine dataStream
			
			result[3] = (filterString line ", ")[1]
			
			line = readLine dataStream
			while line != "mloroomstart" do
			(
				local filtered = filterString line ", 	"
				
				append iplposX (filtered[2] as float)
				append iplposY (filtered[3] as float)
				append iplposZ (filtered[4] as float)
				
				line = readLine dataStream
			)
			
			close dataStream
			
			result[1].x = amin iplposX
			result[1].y = amin iplposY
			result[1].z = amin iplposZ
			
			result[2].x = amax iplposX
			result[2].y = amax iplposY
			result[2].z = amax iplposZ
		)
		
		result
	)
	
	--------------------------------------------------------------------------------------------
	-- 						getMILOpositionFromIPLs()
	--------------------------------------------------------------------------------------------
	fn getMILOpositionFromIPLs mapPath MILO =
	(
		local result = false
		
		local IPLFiles = getFiles (mapPath + "*.ipl")
		
		for IPL in IPLFiles do
		(
			local dataStream = openFile IPL mode:"r"
			
			if (skipToString dataStream MILO) != undefined then
			(
				result = [0,0,0]
				local filtered = filterString (readLine dataStream) ", 	"
				
				result.x = filtered[2] as float
				result.y = filtered[3] as float
				result.z = filtered[4] as float
			)
			
			close dataStream
		)
		
		result
	)
	
	--------------------------------------------------------------------------------------------
	-- 						extractIPL()
	--------------------------------------------------------------------------------------------
	fn extractIPL mapPath image =
	(
		-- Clean dir image extraction
		HiddenDOSCommand ("del c:\\tempIPL\\" + pathConfig.stripPathToLeaf mapPath + "*.ipl") startpath:"c:\\"
		
		-- mount image and extract the ipl data
		local rageBuilderExe = "ragebuilder_0327.exe"
		arguments = ("x:\\tools\\bin\\script\\ExtractIPL.rbs -input "+ image + " -output c:\\tempIPL\\"+(pathConfig.stripPathToLeaf mapPath))
		HiddenDOSCommand (rageBuilderExe +" "+ arguments) startpath:"x:\\tools\\bin\\"
	)
	
	--------------------------------------------------------------------------------------------
	-- 						generateMapIPLBox()
	--------------------------------------------------------------------------------------------
	fn generateMapIPLBox mapPath image =
	(
		local IPLPath = ("c:\\tempIPL\\" + pathConfig.stripPathToLeaf mapPath + (getFilenameFile image) + "*.ipl")
		local bounds = gatherBoundsFromIPLs IPLPath
		
		if bounds != false then
		(
			local position = bounds[1] + ((bounds[2] - bounds[1]) / 2)
			local newIPLBox = IPLStreamingBox pos:position name:(getFilenameFile image) top:(abs (position.z - bounds[2].z)) bottom:-(abs (position.z - bounds[1].z)) \
															left:-(abs (position.x - bounds[1].x)) right:(abs (position.x - bounds[2].x)) \
															front:(abs (position.y - bounds[2].y)) back:-(abs (position.y - bounds[1].y)) \
															u32BoxID:(getFilenameFile image)
			
			append IPLBoxArray newIPLBox
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						generateInteriorIPLBox()
	--------------------------------------------------------------------------------------------
	fn generateInteriorIPLBox mapPath image =
	(
		local bounds = getBoundsFromIDE image
		
		if bounds != false then
		(
			local offset = getMILOpositionFromIPLs mapPath bounds[3]
			bounds[1] += offset
			bounds[2] += offset
			
			local position = bounds[1] + ((bounds[2] - bounds[1]) / 2)
			local newIPLBox = IPLStreamingBox pos:position name:(bounds[3] + "_interior") top:(abs (position.z - bounds[2].z)) bottom:-(abs (position.z - bounds[1].z)) \
															left:-(abs (position.x - bounds[1].x)) right:(abs (position.x - bounds[2].x)) \
															front:(abs (position.y - bounds[2].y)) back:-(abs (position.y - bounds[1].y)) \
															u32BoxID:(bounds[3] + "_interior")
			
			append IPLBoxArray newIPLBox
		)
	)

	--------------------------------------------------------------------------------------------
	-- 						GenIPLBoxes()
	--------------------------------------------------------------------------------------------
	fn GenIPLBoxes mapPath =
	(
		makeDir ("c:\\tempIPL\\"+pathConfig.stripPathToLeaf mapPath)
		
		local rpfList =  getfiles (mapPath + "*.rpf")
		
		for image in rpfList do
		(
			extractIPL mapPath image
			generateMapIPLBox mapPath image
		)
		
		rpfList =  getfiles (mapPath + "\\interiors\\*.ide")
		
		for image in rpfList do
		(
			generateInteriorIPLBox mapPath image
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						updateVisualLinks()
	--------------------------------------------------------------------------------------------
	fn updateVisualLinks IPLBox =
	(
		IPLBox.colourCurrent = IPLBox.colorParent
		
		for entry in IPLBox.linkdata do
		(
			local child = getIPLBoxByID entry
			
			if (classOf child) == IPLStreamingBox then
			(
				child.colourCurrent = child.colourChild
			)
		)
		
		for entry in IPLBoxArray where entry.u32BoxID != IPLBox.u32BoxID and (findItem IPLBox.linkdata entry.u32BoxID) == 0 do
		(
			entry.colourCurrent = entry.colourBase
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						UI
	--------------------------------------------------------------------------------------------
	dropdownlist maps_dd "" width:260 height:20 offset:[-6,0]
	button btn_generateIPL "Generate IPL Boxes from streams" width:260 height:24
	listBox lbx_IPLbox "IPL Box Parent" width:126 height:20 across:2 offset:[-6,0]
	listBox lbx_IPLLinks "IPL Box Children" width:126 height:20 offset:[4,0]
	
	button btn_Xrefimage "Load Xref IPL Maps" width:126 height:24 across:2 offset:[-4,0]
	button btn_AddLink "Add link" width:126 height:24 offset:[6,0]
	
	checkbutton btn_Xrefshow "off/on Xrefs" width:126 height:24 across:2 offset:[-4,0] checked:false
	button btn_DelLink "Delete link" width:126 height:24 offset:[6,0]
	
	button btn_AddIPLParent "Add IPL Parent" width:126 height:24 across:2 offset:[-4,0]
	button btn_RemIPLParent "Remove IPL Parent" width:126 height:24 offset:[5,0]
	
	button btn_ImportDat "Import IPL Dat File"  width:260 height:24
	button btn_ExportDat "Export IPL Dat File"  width:260 height:24
	
	--------------------------------------------------------------------------------------------
	-- 						RSL_IPLStream open
	--------------------------------------------------------------------------------------------
	on RSL_IPLStream open do
	(
		directoryList = getDirectories (project.independent+"/levels/*")
		maps_dd.items = for entry in directoryList collect (pathConfig.stripPathToLeaf entry)
		
		updateIPLBoxes lbx_IPLbox
		
		if lbx_IPLbox.selected != undefined then
		(
			local currentIPLBox = IPLBoxArray[lbx_IPLbox.selection]
			
			lbx_IPLLinks.items = currentIPLBox.linkdata as array
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						btn_generateIPL pressed
	--------------------------------------------------------------------------------------------
	on btn_generateIPL pressed do
	(
		-- Remove old IPLBoxes and clear listboxes
		delete (for object in helpers where (classOf object) == IPLStreamingBox collect object)
		
		lbx_IPLbox.items = #()
		lbx_IPLLinks.items = #()
		IPLBoxArray = #()
		
		GenIPLBoxes directoryList[maps_dd.selection]
		
		updateIPLBoxes lbx_IPLbox
		updateVisualLinks IPLBoxArray[lbx_IPLbox.selection]
	)
	
	--------------------------------------------------------------------------------------------
	-- 						lbx_IPLbox selected index
	--------------------------------------------------------------------------------------------
	on lbx_IPLbox selected index do
	(
		updateVisualLinks IPLBoxArray[lbx_IPLbox.selection]
		
		lbx_IPLLinks.items = IPLBoxArray[lbx_IPLbox.selection].linkdata as array
	)
	
	--------------------------------------------------------------------------------------------
	-- 						lbx_IPLLinks selected index
	--------------------------------------------------------------------------------------------
	on lbx_IPLLinks selected index do
	(
		for entry in IPLBoxArray[lbx_IPLbox.selection].linkdata do
		(
			local child = getIPLBoxByID entry
			
			if child.u32BoxID == lbx_IPLLinks.selected then
			(
				child.colourCurrent = child.colourChildSelected
			)
			else
			(
				child.colourCurrent = child.colourChild
			)
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						btn_AddLink pressed
	--------------------------------------------------------------------------------------------
	on btn_AddLink pressed do
	(
		local selectedArray = for object in (selection as array) where (classOf object) == IPLStreamingBox collect object
		
		for entry in selectedArray do
		(
			local validParentIdx = findItem lbx_IPLbox.items entry.u32BoxID
			
			if validParentIdx != 0 and lbx_IPLbox.selected != entry.u32BoxID then
			(
				appendIfUnique IPLBoxArray[lbx_IPLbox.selection].linkdata entry.u32BoxID
			)
		)
		
		lbx_IPLLinks.items = IPLBoxArray[lbx_IPLbox.selection].linkdata as array
		
		updateVisualLinks IPLBoxArray[lbx_IPLbox.selection]
	)
	
	--------------------------------------------------------------------------------------------
	-- 						btn_DelLink pressed
	--------------------------------------------------------------------------------------------
	on btn_DelLink pressed do
	(
		if lbx_IPLLinks.selection > 0 then
		(
			local index = (findItem IPLBoxArray[lbx_IPLbox.selection].linkdata lbx_IPLLinks.selected)
			deleteItem IPLBoxArray[lbx_IPLbox.selection].linkdata index
			
			lbx_IPLLinks.items = IPLBoxArray[lbx_IPLbox.selection].linkdata as array
			
			updateVisualLinks IPLBoxArray[lbx_IPLbox.selection]
		)
	)
)

createDialog RSL_IPLStream