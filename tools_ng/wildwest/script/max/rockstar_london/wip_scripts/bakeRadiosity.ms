fn bakeRad obj =
(
	if superClassOf obj == GeometryClass then
	(
		if radiosityMeshOps.isNodeInSolution obj then
		(
			vcMesh = radiosityMeshOps.getRadiosityMesh obj
				vcMeshVCs = for i in 1 to vcMesh.numcpvverts collect getVertColor vcMesh i
				convertToMesh obj
				defaultVCfaces obj
				if vcMesh.numVerts == obj.numVerts then
				(
					setNumCPVverts obj vcMesh.numCPVverts 
					
					for i in 1 to obj.numFaces do
					(
						objArray = #((getface obj i).x,(getface obj i).y,(getface obj i).z) 
						vcMeshArray = #((getface vcMesh i).x,(getface vcMesh i).y,(getface vcMesh i).z)
						vcmArray = #((getVCface vcMesh i).x,(getVCface vcMesh i).y,(getVCface vcMesh i).z)
						facex = finditem vcMeshArray ((getFace obj i).x)
						facey = finditem vcMeshArray ((getFace obj i).y)
						facez = finditem vcMeshArray ((getFace obj i).z)
						setVCface obj i vcmArray[facex] vcmArray[facey] vcmArray[facez]
					)
					for i in 1 to vcMeshVCs.count do setVertColor obj  i vcMeshVCs[i]
					update obj
					obj.showVertexColors = on
				)
			else
			(
				messagebox "Radiosity topology is different from base\n Vertex count must be the same.\n Try making sure subdivisions are off."
			)
		)
		else messageBox ("Something went wrong.\n\nSkipping: '" + obj.name as string + "'\n\nObject is not in radiosity solution.")
	)
	else messageBox ("Something went wrong.\n\nSkipping: '" + obj.name as string + "'\n\nObject is not valid geometry.")
)

progressStart "Baking Radiosity..."

for i = 1 to selection.count do
(
	
	bakeRad selection[i]
	
	progressUpdate (i as float / selection.count * 100.0)
)

progressEnd()