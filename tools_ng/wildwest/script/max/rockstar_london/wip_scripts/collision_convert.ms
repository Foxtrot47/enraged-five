clearlistener()
filein "rockstar/export/settings.ms"
struct RsCollCategory (name, entries)

RsCollCategories = #()
global RsCollCats = #()
RsCollItems = #()
collnames = #()
RsIdxCollType = getattrindex "Gta Collision" "Coll Type"

rollout RsCollConvert "Untitled" width:162 height:133
(
	dropDownList ddl1 "Convert Collision from:" pos:[7,8] width:147 height:40
	dropDownList ddl2 "to:" pos:[7,54] width:147 height:40
	button btn1 "Convert Selected objs in Scene" pos:[7,101] width:147 height:23
	
		--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	fn LoadEntriesFromFile filename = (
	
		intFile = openfile filename mode:"rb"
		
		while eof intFile == false do (
		
			intLine = RsRemoveSpacesFromStartOfString(readline intFile)
	
			if intLine.count > 0 and intLine[1] != "#" then (
	
				intTokens = filterstring intLine " \t"
					
				found = false
				
				for collCat in RsCollCategories do (
					
					if collCat.name == intTokens[2] then (
						append collnames intTokens[1]
						append collCat.entries intTokens[1]
						found = true
					)
				)
				
				if found == false then (
				
					collCat = RsCollCategory intTokens[2] #(intTokens[1])
					append RsCollCategories collCat
				)
			)
		)
		
		close intFile	
			
	)
	
		fn LoadEntries = (
	
		intFilename = ( RsConfigGetCommonDir() ) + "data/materials/materials.dat"
	
		LoadEntriesFromFile intFilename
		
		RsCollCats = #()
		
		for collCat in RsCollCategories do (
				
			if collCat.name != undefined then (
		
				append RsCollCats collCat.name
			)
		)
		

		
	)
	
		--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn UpdateSelection = (
		sort collnames
	ddl1.items = collnames
	ddl2.items = collnames
	)
	
	mapped fn updatemesh list =
	(
		
		attrib = (getattr list RsIdxCollType)
		if attrib == ddl1.selected then
		(
			setattr list RsIdxCollType ddl2.selected
			print ("Object :"+list.name as string +" has been upadted to "+ddl2.selected as string)
		)
	)
	

	
	fn UpdateCollision = 
	(
		list = for i in getClassInstances Col_Box asTrackViewPick:true collect (i.client)
		updatemesh list
	)
	
	on btn1 pressed do
	(
		UpdateCollision()
	)
	
	on RsCollConvert open do (
	
		LoadEntries()
		UpdateSelection()
	)
)


createDialog RsCollConvert style:#(#style_toolwindow,#style_sysmenu)