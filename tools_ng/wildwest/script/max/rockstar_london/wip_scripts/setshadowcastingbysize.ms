staticIndex = getAttrIndex "Gta Object" "Static Shadows"
dynamicIndex = getAttrIndex "Gta Object" "Dynamic Shadows"

minimumSize = 0.87
count = 0

for object in selection where (getAttrClass object) == "Gta Object" do
(
	local bounds = object.max - object.min
	
	local size = (length bounds)
-- 	format "% : %\n" size minimumSize
	if size < minimumSize then
	(
		count += 1
-- 		print "Setting Static and Dynamic shadow flags to false"
		
		setAttr object staticIndex false
		setAttr object dynamicIndex false
	)
)

format "Modified % objects\n" count
