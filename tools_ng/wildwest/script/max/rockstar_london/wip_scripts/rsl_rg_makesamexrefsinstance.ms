XRefObjects = #()

try (destroyDialog RSL_MakeSameXRefsInstance) catch()

rollout RSL_MakeSameXRefsInstance "R* Make Same XRefs Instance" width:176 height:32
(
	progressBar pb1 "ProgressBar" pos:[8,8] width:160 height:16
	
		
fn FindXRef XRefSrc =
	(
		for Element in XRefObjects do
		(
			if (Element.srcItemName == XRefSrc.srcItemName) do
			(
				if (Element.srcFileName == XRefSrc.srcFileName) do
				(
					return Element
				)
			)
		)
		append XRefObjects XRefSrc
		return XRefSrc
	)

fn CheckObject Obj = 
(
	if classof Obj == XRefObject then 
	(
		InstanceSrc = FindXRef Obj
		instanceReplace Obj InstanceSrc
	)
)

	on RSL_MakeSameXRefsInstance open do
	(
	suspendEditing()
	XrefObjects = #()
	Increment = 100.0/geometry.count as float
	Total = 0.0
	for Obj in Geometry do 
		(
		CheckObject Obj
		Total += Increment
		pb1.value = Total 
		)
	resumeEditing()
	destroyDialog RSL_MakeSameXRefsInstance
	)
)

createDialog RSL_MakeSameXRefsInstance style:#(#style_toolwindow,#style_sysmenu)