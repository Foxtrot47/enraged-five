-- IPL STREAM
clearlistener()

/*
struct iplstream
(
	vect_TopLefl,
	vect_BottomRight,
	u32BoxIDs,
	Iplname,
	u32BoxID
)

struct boxname (name, boxsel)
*/

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------


the_boxIPLData = attributes boxIPLData
attribID:#(0x3d43c8d4, 0x4e566f56)
(
	parameters main
        (
			
			u32BoxID type:#string
			topleft type:#string
			botright type:#string
			linkcnt type:#integer
			linkdata type:#string
			Iplname type:#string
			
        )
)

fn convertlisttoString list=
(
	newstring = ""
	for i in list do
	(
		newstring += i as string+" "
	)
	newstring
)

fn convertstringtolist obj=
(
	if obj.linkdata != undefined then
	(
	templist = obj.linkdata 
	newlist = filterString templist " "
	)
	else
	(
		newlist =#()
	)
	newlist
)
	

fn addcustdata obj iplname linkcnt linklist = 
(
	custAttributes.add obj the_boxIPLData	
	bnd = nodeLocalBoundingBox obj	
	
	obj.u32BoxID = obj.name
	obj.topleft = (bnd[1].x as string+" "+bnd[1].y as string+" "+bnd[1].z as string)
	obj.botright = (bnd[2].x as string+" "+bnd[2].y as string+" "+bnd[2].z as string)
	obj.linkcnt = linkcnt
	obj.linkdata = linklist
	
	obj.Iplname = iplname
	

)
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------



-- Dot net stuff
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- This process reads standard output, which calls the ragbuilder script i've created to just list the ipl files 

/*
 fn outputDataReceivedHandler obj evt =
 (
 	if evt.data != undefined then
 	(		
		if getFilenameType (evt.data) == ".ipl" then
		(			
			append iplList (evt.data)
		)
 	)
 )

global iplList = #()
*/

  

/*
fn readipl imgname =
(
	proc = dotNetObject "System.Diagnostics.Process"
	proc.EnableRaisingEvents = true

	proc.StartInfo.WorkingDirectory = "x:\\tools\\bin\\"
	proc.StartInfo.FileName = "x:\\tools\\bin\\ragebuilder_0327.exe"
	proc.StartInfo.UseShellExecute = false
	proc.StartInfo.RedirectStandardOutput = true
	proc.StartInfo.RedirectStandardError = false
	proc.StartInfo.CreateNoWindow = true
	  
	dotNet.AddEventHandler proc "OutputDataReceived" outputDataReceivedHandler

	iplList = #()
	proc.StartInfo.Arguments = ("x:\\tools\\bin\\script\\InspectImage.rbs -input "+imgname)
	proc.Start()
	proc.BeginOutputReadLine()
	
	proc.WaitForExit()
	--proc.close()
	--proc.CancelOutputRead()

	
	rollout_iplstream.lbx2.items = iplList
)
*/

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------

struct iplbox
(
	iplname,
	minX,
	minY,
	minZ,
	maxX,
	maxY,
	maxZ
)
New_iplbox = iplbox()


fn readiplfile pathname =
(
	iplposX = #()
	iplposY = #()
	iplposZ = #()
	
	ipllist =  getfiles (pathname+"*.ipl")
	
	print (ipllist)
	
	
	
	local keymatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\w+,\s[0-9]*,\s([-]*[0-9]*.[0-9]*),\s([-]*[0-9]*.[0-9]*),\s([-]*[0-9]*.[0-9]*),)")
	
	if ipllist.count > 0 then
	(
		for iplname in ipllist do
		(
			print iplname
			local fstream = openFile iplname mode:"r"
			

			skiptostring fstream "inst"
				
			while eof fstream == false do
					(
					local linematch = keymatch.Match (readline fstream)
						if (linematch.success) then
						(
							--print (linematch.groups.item[1].value)
							--append iplpos #((linematch.groups.item[2].value),(linematch.groups.item[3].value),(linematch.groups.item[4].value))
							append iplposX (linematch.groups.item[2].value)
							append iplposY (linematch.groups.item[3].value)
							append iplposZ (linematch.groups.item[4].value)
						)

					)
				
			close fstream
		)
		print 
		New_iplbox.iplname = (pathConfig.stripPathToLeaf ipllist[1]) as string
		New_iplbox.minx = amin iplposX as float
		New_iplbox.miny = amin iplposY as float
		New_iplbox.minz = amin iplposZ as float
		New_iplbox.maxx = amax iplposX as float
		New_iplbox.maxy = amax iplposY as float
		New_iplbox.maxz = amax iplposZ as float
	)
	else
	(
		return false
	)
			
	
	--New_iplbox
			
)



fn genIPLbox pathname imagename =
(
	

	if (readiplfile pathname) != false then
	(
		posx =  (New_iplbox.maxX - New_iplbox.minX)
		posy =  (New_iplbox.maxY - New_iplbox.minY)
		posz =  (New_iplbox.maxZ - New_iplbox.minZ)
		iplbox = Box name:imagename pos:[posx/2+New_iplbox.minX,posy/2+New_iplbox.minY,New_iplbox.minZ] width:posx length:posy height:posz
		iplbox.wirecolor = (color 214 229 166)
	)


)

fn GenIplstreamBoxes mappath =
(
	
	makeDir ("c:\\tempIPL\\"+pathConfig.stripPathToLeaf mappath)

	imglist =  getfiles (mappath+"*.img")
	for eachimage in imglist do
	(
		-- Clean dir for each image extraction
		HiddenDOSCommand ("del c:\\tempIPL\\"+pathConfig.stripPathToLeaf mappath+"*.ipl") startpath:"c:\\"
		-- mount it each and extract the ipl data
		filename = "ragebuilder_0327.exe"
		arguments = ("x:\\tools\\bin\\script\\ExtractIPL.rbs -input "+eachimage + " -output c:\\tempIPL\\"+(pathConfig.stripPathToLeaf mappath))
		--print (filename +" "+ arguments)
		HiddenDOSCommand (filename +" "+ arguments) startpath:"x:\\tools\\bin\\" 
			
		genIPLbox ("c:\\tempIPL\\"+(pathConfig.stripPathToLeaf mappath)) (getFilenameFile eachimage) --eachimage
	)
	 
	
)

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------


try (destroyDialog rollout_iplstream) catch ()

rollout rollout_iplstream "Untitled" width:270 height:355
(
	fn updatemapnames =
	(
		rollout_iplstream.maps_dd.items = getDirectories (project.independent+"/levels/*")
	)
	
	
	/*
	fn updateimgnames mappath=
	(
		templist =  getfiles (mappath+"*.img")
		local imglist= #()
		for efile in templist do
			append imglist (getFilenameFile efile)
		
		
		rollout_iplstream.lbx3.items =imglist
		
	)
	*/
	
	
	fn getiplstreamdata obj =
	(
		obj.linkdata = linklist as string
		obj.u32BoxID = obj.name
		obj.topleft = bnd[1]
		obj.botright = bnd[2]
		obj.Iplname = iplname
	)
	
	fn updatecurrentboxes =
	(
		for obj in geometry do
		(
			if classof obj == box then
			(
				if (custAttributes.count obj) > 0 then
				(
					--addBox obj.name obj		

					local currentlist = rollout_iplstream.lbx1.items 
					append currentlist (obj.name)
					rollout_iplstream.lbx1.items = currentlist
					
				)

			)
		)
	)
	
	fn setboxipls =
	(
		for obj in geometry do
		(
			if classof obj == box then
			(
				addcustdata obj obj.name 0 ""
			)
		)
	)
	

	
	fn exportIPLstreamdata filename =
	(
		if filename != undefined then
		(
			file = createfile filename	
			-- header
			format "IPL_BOXES_BEGIN\n" to:file
			for obj in geometry do
			(
				if classof obj == box then
				(
					if (custAttributes.count obj) > 0 then
					(
						format (obj.u32BoxID +" ") to:file
						format (obj.topleft as string+" ") to:file
						format (obj.botright as string+" ") to:file
						format (obj.linkcnt as string+" ") to:file
						format (obj.linkdata +"\n") to:file
					)
				)
			)
			format "IPL_BOXES_END" to:file
			close file
		)
		
		
	)
	
	fn removeiplboxes =
	(
		deletelist= #()
		for obj in geometry do
		(
			if classof obj == box then
			(
				if (custAttributes.count obj) > 0 then
				(
					append deletelist obj					
				)
			)
		)
		delete deletelist
	)
	
	fn updateVisualcolors =
	(
		for objname in (rollout_iplstream.lbx1.items) do
		(
			--obj = (getNodeByName objname)
			(getNodeByName objname).wirecolor = (color 214 229 166)
		)
	)
	fn updatelinkcolors =
	(
		for objname in (rollout_iplstream.lbx4.items) do
		(
			(getNodeByName objname).wirecolor =blue
		)
	)
	

	listBox lbx1 "BOX_ID" pos:[4,60] width:100 height:18
	listBox lbx4 "BOX_Links" pos:[140,60] width:100 height:16
	--listBox lbx2 "IPL_STREAM" pos:[140,210] width:124 height:6
	--listBox lbx3 "IMG File" pos:[140,48] width:124 height:10
	
	dropdownlist maps_dd "" pos:[4,4] width:262 height:20
	
	button btn_generateIPL "Generate IPL Boxs from streams" width:260 height:24 --pos:[138,317] 
	
	--button btn2 "Set IPL" pos:[138,317] width:60 height:24
	--button btn1 "Add Box" pos:[8,317] width:60 height:24
	button btn4 "Add link" pos:[140,290] width:100 height:24
	button btn3 "Export"  width:260 height:24
	
	
	
	on btn_generateIPL pressed do
	(
		-- Remove old objects and clear listboxes
		removeiplboxes()
		lbx1.items = #()
		lbx4.items = #()
		-- generate new boxes
		GenIplstreamBoxes (maps_dd.selected)
		setboxipls()
		updatecurrentboxes()
	)
	
	on btn3 pressed do
	(
		exportIPLstreamdata (getSaveFilename caption:"IPLstream File" types:"dat file (*.dat)|*.dat")
	)
	
	--------------------------------------------------------------------------------------------
	-- 						GENERATE IPL BOXES
	--------------------------------------------------------------------------------------------
	on rollout_iplstream open do
	(
		--Levelconfig = GetLevelconfig()
		updatemapnames()
		--updateimgnames (maps_dd.selected)
		updatecurrentboxes()
		--lbx1.items = objN
		
		-- Update list boxes if any objects are populated in the listbox
		if lbx1.selected != undefined then
		(
		obj = (getNodeByName (lbx1.selected)) 
		lbx4.items = (convertstringtolist obj)
		)
	)
	
	/*
	on btn1 pressed do
	(
		for obj in selection do
		(
		if obj != undefined then
			(
				local currentlist = lbx1.items
				appendifunique currentlist obj.name
			--addBox obj.name obj
				lbx1.items = currentlist
				
				linkcnt = 0
				linklist = ""
				iplname = ""
				
			    addcustdata obj iplname linkcnt linklist 	
			)
		)
	)
	*/
	
	on lbx1 selected nameIndex do
	(
	obj = (getNodeByName lbx1.items[nameIndex]) 
	select obj
	updateVisualcolors()
	obj.wirecolor = red
	-- update link list box
	lbx4.items = (convertstringtolist obj)
	updatelinkcolors()
	
	)
	
	/*
	
	on btn2 pressed do
	(
		if lbx1.selected != undefined  then
		(
			print "adding IPL"
			
			--iplname = lbx2.selected
			linklist = lbx4.items
			obj = $
			iplname = $.name as string
			addcustdata obj iplname linklist.count linklist
		)
	)
	*/
	
	--------------------------------------------------------------------------------------------
	-- 						ADD LINK
	--------------------------------------------------------------------------------------------
	
	on btn4 pressed do
	(
		if $ != undefined then
		(
			
			if lbx1.selected != $.name then
			(
				print "adding link"
				local currentlist = lbx4.items
				appendifunique currentlist $.name
				lbx4.items = currentlist
				
				
				iplname = $.name				
				
				sort (linklist = lbx4.items)
				--print (lbx1.selected)
				
				obj = (getNodeByName (lbx1.selected)) 
				$.wirecolor = blue	
				linkcnt = (lbx4.items).count
				--print obj
				--print linklist
				--tl = convertlisttoString obj					
				
				-- Add
					
					
				addcustdata obj iplname linkcnt (convertlisttoString linklist) 
					
				--addcustdata $ iplname 0 "" 	
			)
		)
	)
	
	
)

createDialog rollout_iplstream