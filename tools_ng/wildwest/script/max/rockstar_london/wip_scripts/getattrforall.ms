
fn getAttributes object class =
(
	local result = #(#(),#(),#())
	local count = 1
	local end = false
	
	format "Attributes for %\n" class
	
	while not end do
	(
		try
		(
			local attributeName = (getAttrName class count)
			local index = getattrindex class attributeName
			local attibuteValue = getattr object index
			
			append result[1] attributeName
			append result[2] index
			append result[3] attibuteValue
			
			format "Name:% Index:%\n" attributeName index
		)
		catch
		(
			end = true
		)
		count += 1
	)
	result
)

-- attributeArray = #(#(),#())

-- for object in objects do
-- (
-- 	local class = GetAttrClass object
-- 	
-- 	if class != undefined then
-- 	(
-- 		append attributeArray[1] object
-- 		append attributeArray[2] (getAttributes object class)
-- 	)
-- )

getAttributes $ (GetAttrClass $)

-- getattrindex "Gta Object" "Does Not Provide AI Cover"