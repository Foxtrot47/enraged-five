RsSetMenu "Rockstar Payne" #(
) menuParentName:"RS Wildwest"




RsSetMenu "Character" #(			
			"CharacterSwapper",
			"CharacterBatchExport",
			"",
			"AmbientLighting"

) menuParentName:"Rockstar Payne"

RsSetMenu "Collision" #(
			"CollisionOps",
			"CollisionPolyChecker",
			"",
			"AutoSurfaceTypeAssign",
			"AutoWindowTypeSize",
			"",
			"CollisionTagger",
			"",
			"AutoRoomID"

) menuParentName:"Rockstar Payne"

RsSetMenu "Design" #(
			"PESSceneEditor",
			"PESSceneSearcher",
			"",
			"SimpleLevelCreator",	
			"",	
			"ScriptHelper",	
			"",
			"FireBoxExporter",						
			"TriggerBoxHelpUI",
			"LevelCleaner"
			

) menuParentName:"Rockstar Payne"

RsSetMenu "Helpers" #(
			"IPLStreamExport",
			"",
			"ObjectTypeOps",
			"",
			"Toolbox",
			"",
			"MaterialLibraryOps"
			"AutoSurfaceTypeAssign",
			"AutoWindowTypeSize",
			"",
			"VehicleAnimator"

) menuParentName:"Rockstar Payne"

RsSetMenu "Interior" #(
			"MiloOps",
			"",
			"MiloCollisionToolV1",
			"MiloInfo"
			

) menuParentName:"Rockstar Payne"

RsSetMenu "Lighting" #(
			"LightOps"

) menuParentName:"Rockstar Payne"

RsSetMenu "Misc" #(
			"FragTuner"
		

) menuParentName:"Rockstar Payne"

RsSetMenu "Multiplayer" #(
			"GameModeFileCreator",
			"",
			"SpawnPointMarker"
		

) menuParentName:"Rockstar Payne"

RsSetMenu "Modeling" #(
			"DetachByID",
			"DecalPlanter",
			"",
			"mmPainter"

) menuParentName:"Rockstar Payne"

RsSetMenu "Optimization" #(
			"RSLSceneStats",
			"RSLTextureStats",
			"",
			"SceneBrowser",
			"SceneCleaner",
			"",
			"AutoLODDistance",
			"SetShadowCasting",
			"",	
			"lodOptimizerTools",
			"",			
			"LightLister",
			"",
			"LodBaker",
			"LodEditor",
			"",
			"TerrainBaker",
			"",
			"MemoryStats"

) menuParentName:"Rockstar Payne"

RsSetMenu "Particle" #(
			"ParticleHelper",
			"",
			"ValidateParticles"

) menuParentName:"Rockstar Payne"

RsSetMenu "Prop" #(
			"PropViewer",
			"",
			"PropPlacer",
			"PropDictionaryEditorDotNet",
			"",
			"PropThumbsRenderer",
			"BatchPropRender",
			"MergeProp",
			"",
			"NoAICover",
			"",
			"objectfinder",
			"",
			"rebuildxrefs"


) menuParentName:"Rockstar Payne"

RsSetMenu "Shaders" #(
			"RageShaderValueEditor",
			"LiveShaderEditorImport",
			"",
			"GTAToRage",
			"MaterialConverter",
			"XRefMaterialTools"

) menuParentName:"Rockstar Payne"

RsSetMenu "Spatial Data" #(			
			"CoverEdge"


) menuParentName:"Rockstar Payne"


RsSetMenu "Texture" #(			
			"TextureTag2",
			"",
			"TXDOpsLite",
			"TXDOps2",
			"TXDReparent",
			"ExportParents",
			"BatchTXDOps"

) menuParentName:"Rockstar Payne"

RsSetMenu "Utils" #(			
			"PathsRemapTool",
			"",			
			"ModelRenderer",			
			"PropTxdViewer",			
			"VehicleEditor",
			"",
			"MapRenderSetup",
			"",
			"FixMyMap",
			"",
			"DeleteEmptyLayers",
			"",
			"RSL_MM_ChannelSwap"
			

) menuParentName:"Rockstar Payne"