global RSL_showNormals
global RSL_NormalDisplay

plugin simpleManipulator RSL_normalDisplayManip
name:"Normal Display"
classID:#(0x839bb3d, 0x3acbcc41)
category:"Manipulators"
invisible:true
(
	local size = 1.0
	local lineColor = [0,0,1]
	
	on canManipulate target return
	(
		RSL_showNormals == true and (classOf target == editable_mesh or classOf target == editable_poly)
	)
	
	on updateGizmos do
	(
		this.clearGizmos()
		
		local faceArray = for face in node.faces collect face
		
		if RSL_NormalDisplay != undefined then
		(
			size = RSL_NormalDisplay.spn_normalSize.value
			
			if RSL_NormalDisplay.chk_selected.checked then
			(
				faceArray = for face in node.selectedFaces collect face
			)
			
			lineColor = (RSL_NormalDisplay.cpk_lineColour.color / 256)
		)
		else
		(
			print "RSL_NormalDisplay is undefined"
		)
		
		local normalShape = manip.makeGizmoShape()
		
		if subObjectLevel == 3 or subObjectLevel == 4 then
		(
			for face in faceArray do 
			(
				normalShape.startNewLine()
				
				local fCenter
				local fNormal
				
				case (classOf target) of
				(
					Editable_mesh:
					(
						in coordSys local
						(
							fCenter = meshop.getFaceCenter node face.index
							fNormal = getFaceNormal node face.index
						)
					)
					Editable_Poly:
					(
						in coordSys local
						(
							fCenter = polyOp.getFaceCenter node face.index
							fNormal = polyop.getFaceNormal node face.index
						)
					)
				)
				
				normalShape.addPoint fCenter
				normalShape.addPoint (fCenter + (fNormal * size))
			)
			
			this.addGizmoShape normalShape gizmoDontHitTest lineColor lineColor
		)
	)
)