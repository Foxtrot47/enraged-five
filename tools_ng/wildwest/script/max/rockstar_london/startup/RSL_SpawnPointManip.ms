global RSL_SpawnPointMarker

plugin simpleManipulator RSL_SpawnPointManip
name:"Spawn Point Marker"
classID:#(0x43d52abf, 0x25d80ee9)
category:"Manipulators"
-- invisible:true
(
	parameters main
	(
		posArray	type:#point3Tab tabSizeVariable:true animatable:false
	)
	
	on canManipulate target return (classOf target) == RSL_SpawnPointManip
	
    -- Creation mouse procedure
    tool create
    (
        on mousePoint click do
		(
			nodeTM.translation = gridPoint
		)
    )
	
	on updateGizmos do
	(
		this.clearGizmos()
		
		if (target != undefined) then 
		(
			this.posArray = target.posArray
		)
		
		for pos in posArray do 
		(
			local circle1 = manip.MakeCircle pos 0.5 16
			this.addGizmoShape circle1 gizmoDontHitTest [1,1,0] [1,0,0]
			
			local circle2 = manip.MakeCircle (pos + [0,0,0.25]) 0.75 16
			this.addGizmoShape circle2 gizmoDontHitTest [1,1,0] [1,0,0]
			
			local lines = manip.makeGizmoShape()
			lines.startNewLine()
			lines.addPoint (pos + [0.5, 0, 0])
			lines.addPoint (pos + [0.75, 0, 0.25])
			
			lines.startNewLine()
			lines.addPoint (pos + [-0.5, 0, 0])
			lines.addPoint (pos + [-0.75, 0, 0.25])
			
			lines.startNewLine()
			lines.addPoint (pos + [0, 0.5, 0])
			lines.addPoint (pos + [0, 0.75, 0.25])
			
			lines.startNewLine()
			lines.addPoint (pos + [0, -0.5, 0])
			lines.addPoint (pos + [0, -0.75, 0.25])
			
			this.addGizmoShape lines gizmoDontHitTest [1,1,0] [1,0,0]
			
			local arrow = manip.makeGizmoShape()
			arrow.startNewLine()
			arrow.addPoint pos
			arrow.addPoint (pos + [-0.25, 0, 0.25])
			arrow.addPoint (pos + [-0.15, 0, 0.25])
			arrow.addPoint (pos + [-0.15, 0, 1])
			arrow.addPoint (pos + [0.15, 0, 1])
			arrow.addPoint (pos + [0.15, 0, 0.25])
			arrow.addPoint (pos + [0.25, 0, 0.25])
			arrow.addPoint pos
			
			this.addGizmoShape arrow gizmoDontHitTest [1,0.5,0] [1,0,0]
		)
	)
)