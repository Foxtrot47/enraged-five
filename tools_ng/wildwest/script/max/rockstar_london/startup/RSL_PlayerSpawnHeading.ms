plugin simpleManipulator RSL_PlayerSpawnHeading
name:"Player Spawn Heading"
classID:#(0x5209742a, 0x6a88fee4)
category:"Manipulators"
invisible:true
(
	parameters main
	(
		SP_headingDiv type:#float animatable:false default:0
	)
	
	on canManipulate target return
	(
		(isProperty target #PES) and (target.pesType) == "Player Spawn"
	)
	
	fn pointFromAngle d =
	(
		normalize [(sin d), -(cos d), 0]
	)
	
	fn angleFromPoint p =
	(
		local product = dot [0,-1,0] (normalize p)
		
		if product <= 1 then
		(
			target.SP_headingDiv = (acos product)
		)
	)
	
	on updateGizmos do
	(
		this.clearGizmos()
		
		if (target != undefined) then
		(
			this.SP_headingDiv = target.SP_headingDiv
		)
		
		local minPoint = pointFromAngle this.SP_headingDiv
		local maxPoint = pointFromAngle -this.SP_headingDiv
		
		this.addGizmoMarker #dot [0.4,0,0] 0 [0,1,0] [1,0,0]
		this.addGizmoMarker #hollowBox [0.55,0,0] 0 [0,1,0] [1,0,0]
		this.addGizmoMarker #bigBox [0.7,0,0] 0 [0,1,0] [1,0,0]
		
		this.addGizmoMarker #dot minPoint 0 [1,1,0] [1,0,0]
		this.addGizmoMarker #dot maxPoint 0 [1,1,0] [1,0,0]
		
		local piePath = manip.makeGizmoShape()
		piePath.startNewLine()
		
		piePath.addPoint minPoint
		piePath.addPoint [0,0,0]
		piePath.addPoint maxPoint
		
		this.addGizmoShape piePath gizmoDontHitTest [1,1,1] [1,1,1]
		
		return (this.SP_headingDiv as string + " Degrees")
	)
	
	on mouseDown mPos giz do
	(
		case giz of
		(
			0:(target.SP_headingDiv = 0.0)
			1:(target.SP_headingDiv = 90.0)
			2:(target.SP_headingDiv = 180.0)
		)
	)
	
	on mouseMove mPos giz do
	(
		if giz == 3 or giz == 4 then
		(
			local projectedPoint = [0,0,0]
			local viewRay = this.getLocalViewRay mPos
			
			local pl = manip.makePlaneFromNormal z_axis [0,0,0]
			local result = pl.intersect viewRay &projectedPoint
			
			if result then
			(
				angleFromPoint projectedPoint
			)
		)
	)
)