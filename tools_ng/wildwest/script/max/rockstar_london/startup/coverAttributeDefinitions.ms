global g_STAIR_SURFACE_VERSION = 1
global g_CURRENTLY_SELECTED_STAIR_SURFACE = undefined

-- Attribute tag for stair surface objects.
global StairSurfaceAttributes = attributes "Stair Surface Attributes"
(
	parameters main
	(
		StairSurfaceVersion type:#integer default:g_STAIR_SURFACE_VERSION
		type type:#string default:"Spawn Manager Attributes"

		-----------------------------------------------------------------
		-- Version 1 Information
		-----------------------------------------------------------------
		IsSlidable type:#boolean default:false
		-----------------------------------------------------------------

	)
)

-- Purpose: Cover Mesh custom attr so that we can reverse index based on a mesh.
global CoverMeshIndexATTR = attributes "Cover Mesh Index"
(
	parameters main
	(
		MeshIndex			type:#integer
		ParentCoverLine	type:#node
		INVALID			type:#boolean default:false
	)
)


global SoftCoverMeshATTR = attributes "Soft Cover Mesh Attributes"
(
	parameters main
	(
		KnotIndex			type:#integer
		SoftCoverRadius	type:#float
		ParentCoverLine	type:#node
		INVALID			type:#boolean default:false
	)
)

global HardCoverMeshATTR = attributes "Hard Cover Mesh Attributes"
(
	parameters main
	(
		IsHardCoverMesh	type:#boolean default:true
		KnotIndex			type:#integer
		HardCoverRadius	type:#float
		ParentCoverLine	type:#node
		INVALID			type:#boolean default:false
	)
)

global Cover_LineNormalMeshATTR = attributes "Cover Line Normal Mesh Attributes"
(
	parameters main
	(
		KnotIndex			type:#integer
		ParentCoverLine	type:#node
		INVALID			type:#boolean default:false
	)
)
