-- Payne_CornerPopout object.
-- Rockstar Vancouver
-- 03/17/2009
-- by Dustin Sutherland



plugin helper Payne_CornerPopout
	name:"Corner Position"
	classID:#(0x3843483d, 0x1d131335)
	category:"Payne"
	extends:dummy 
( 

	local lastSide = undefined;
	local meshObj = undefined;
	
	parameters pblock rollout:params
	(
		isrightside type:#boolean animatable:true ui:isrightside default:false
		bArcOverride type:#boolean animatable:true ui:arcOverride default:false
		height type:#float animatable:true ui:height default:1.0
		cornerArcDegrees type:#float animatable:true ui:cornerArc_spinner default:60.0
		SegmentIndex type:#integer animatable:true default:-1.0
		SideIndex type:#integer animatable:true default:-1.0
		flagString type:#string animatable:true default:""
		bDisablePopover type:#boolean animatable:true ui:disablePopover default:false
	)
 

	rollout params "Corner Popout Parameters"
	(
		checkbox isrightside "Is Right Cover"
		Spinner height "Size:" align:#left range:[0.0, 40.0, 1.0]
		checkbox disablePopover "Disable Popover"
		
		group "Cover Arc"
		(	
			checkbox arcOverride "Use Arc Override"
			Angle cornerArcDeg_Angle "Arc Deg" range:[0, 180.0, cornerArcDegrees] startdegrees:90 dir:#ccw color:red align:#center diameter:40 enabled:bArcOverride across:2
			Spinner cornerArc_spinner range:[0, 180.0, cornerArcDegrees] fieldwidth:40 across:2 offset:[-25,25]
		)
		
		on arcOverride changed val do 
			( 
				cornerArcDeg_Angle.enabled = val 
				cornerArc_spinner.enabled = val
				--cornerArcDeg_Angle.degrees = cornerArcDegrees
			)
		on cornerArcDeg_Angle changed val do cornerArc_spinner.value = val
		on cornerArc_spinner changed val do cornerArcDeg_Angle.degrees = val
		
	)

local vertArray = #(
	[1.9215e-005,0.250613,0],
	[-0.0499808,0.250613,0],
	[-0.0999808,0.250613,0],
	[1.92076e-005,0.200613,0],
	[-0.0499808,0.200613,0],
	[-0.0999808,0.200613,0],
	[-0.0499808,0.150613,0],
	[-0.0999808,0.150613,0],
	[1.91852e-005,0.100613,0],
	[-0.0499808,0.100613,0],
	[-0.0999808,0.100613,0],
	[1.91778e-005,0.0506134,0],
	[-0.0499808,0.0506134,0],
	[-0.0999808,0.0506134,0],
	[-0.149981,0.0506135,0],
	[1.91703e-005,0.000613436,0],
	[-0.0499808,0.000613451,0],
	[-0.0999808,0.000613451,0],
	[-0.149981,0.000613458,0],
	[-0.249981,0.000613473,0],
	[0.0451318,0.250613,0],
	[0.0451318,0.200613,0],
	[0.0933458,0.250613,0],
	[0.0933458,0.200613,0],
	[0.0692388,0.0949005,0],
	[-0.0699033,0.0508152,0.120605],
	[-0.0692219,0.0456388,0.120605],
	[-0.0672239,0.0408152,0.120605],
	[-0.0640455,0.036673,0.120605],
	[-0.0599033,0.0334947,0.120605],
	[-0.0550797,0.0314967,0.120605],
	[-0.0499033,0.0308152,0.120605],
	[-0.044727,0.0314966,0.120605],
	[-0.0399034,0.0334946,0.120605],
	[-0.0357612,0.036673,0.120605],
	[-0.0325828,0.0408152,0.120605],
	[-0.0305848,0.0456388,0.120605],
	[-0.0299033,0.0508152,0.120605],
	[-0.0305848,0.0559915,0.120605],
	[-0.0325828,0.0608151,0.120605],
	[-0.0357612,0.0649573,0.120605],
	[-0.0399033,0.0681357,0.120605],
	[-0.044727,0.0701337,0.120605],
	[-0.0499033,0.0708152,0.120605],
	[-0.0550797,0.0701337,0.120605],
	[-0.0599033,0.0681357,0.120605],
	[-0.0640455,0.0649573,0.120605],
	[-0.0672238,0.0608152,0.120605],
	[-0.0692219,0.0559916,0.120605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0499033,0.0508152,0.160605],
	[-0.0592073,0.0515949,0],
	[-0.0586042,0.0481746,0],
	[-0.0568677,0.045167,0],
	[-0.0542073,0.0429346,0],
	[-0.0509438,0.0417468,0],
	[-0.0474708,0.0417468,0],
	[-0.0442073,0.0429346,0],
	[-0.0415468,0.045167,0],
	[-0.0398104,0.0481746,0],
	[-0.0392073,0.0515948,0],
	[-0.0398104,0.055015,0],
	[-0.0415468,0.0580227,0],
	[-0.0442073,0.0602551,0],
	[-0.0474708,0.0614429,0],
	[-0.0509438,0.0614429,0],
	[-0.0542073,0.0602551,0],
	[-0.0568677,0.0580227,0],
	[-0.0586042,0.0550151,0],
	[-0.0592073,0.0515949,0.12],
	[-0.0586042,0.0481746,0.12],
	[-0.0568677,0.045167,0.12],
	[-0.0542073,0.0429346,0.12],
	[-0.0509438,0.0417468,0.12],
	[-0.0474708,0.0417468,0.12],
	[-0.0442073,0.0429346,0.12],
	[-0.0415468,0.045167,0.12],
	[-0.0398104,0.0481746,0.12],
	[-0.0392073,0.0515948,0.12],
	[-0.0398104,0.055015,0.12],
	[-0.0415468,0.0580227,0.12],
	[-0.0442073,0.0602551,0.12],
	[-0.0474708,0.0614429,0.12],
	[-0.0509438,0.0614429,0.12],
	[-0.0542073,0.0602551,0.12],
	[-0.0568677,0.0580227,0.12],
	[-0.0586042,0.0550151,0.12]
)

local edgeVis = #(
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,true),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,true),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,true),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(false,false,false),
	#(false,false,false),
	#(false,false,false),
	#(true,false,true),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(false,false,false),
	#(false,false,false),
	#(false,false,false),
	#(true,false,true),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(false,false,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(true,false,true),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(false,false,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(true,false,true)
)

local faceSmoothGroups = #(
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	0,
	0,
	0,
	0,
	0,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1
)

local faceList = #(
	[1,2,5],
	[5,4,1],
	[2,3,6],
	[6,5,2],
	[5,6,8],
	[8,7,5],
	[9,7,10],
	[7,8,11],
	[11,10,7],
	[9,10,13],
	[13,12,9],
	[10,11,14],
	[14,13,10],
	[11,8,15],
	[15,14,11],
	[12,13,17],
	[17,16,12],
	[13,14,18],
	[18,17,13],
	[14,15,19],
	[19,18,14],
	[19,15,20],
	[1,4,22],
	[22,21,1],
	[21,22,24],
	[24,23,21],
	[24,22,25],
	[26,27,51],
	[51,50,26],
	[27,28,52],
	[52,51,27],
	[28,29,53],
	[53,52,28],
	[29,30,54],
	[54,53,29],
	[30,31,55],
	[55,54,30],
	[31,32,56],
	[56,55,31],
	[32,33,57],
	[57,56,32],
	[33,34,58],
	[58,57,33],
	[34,35,59],
	[59,58,34],
	[35,36,60],
	[60,59,35],
	[36,37,61],
	[61,60,36],
	[37,38,62],
	[62,61,37],
	[38,39,63],
	[63,62,38],
	[39,40,64],
	[64,63,39],
	[40,41,65],
	[65,64,40],
	[41,42,66],
	[66,65,41],
	[42,43,67],
	[67,66,42],
	[43,44,68],
	[68,67,43],
	[44,45,69],
	[69,68,44],
	[45,46,70],
	[70,69,45],
	[46,47,71],
	[71,70,46],
	[47,48,72],
	[72,71,47],
	[48,49,73],
	[73,72,48],
	[49,26,50],
	[50,73,49],
	[48,47,46],
	[46,45,44],
	[44,43,42],
	[46,44,42],
	[42,41,40],
	[40,39,38],
	[42,40,38],
	[38,37,36],
	[36,35,34],
	[38,36,34],
	[42,38,34],
	[34,33,32],
	[32,31,30],
	[34,32,30],
	[30,29,28],
	[28,27,26],
	[30,28,26],
	[34,30,26],
	[42,34,26],
	[46,42,26],
	[48,46,26],
	[49,48,26],
	[51,52,53],
	[53,54,55],
	[55,56,57],
	[53,55,57],
	[57,58,59],
	[59,60,61],
	[57,59,61],
	[61,62,63],
	[63,64,65],
	[61,63,65],
	[57,61,65],
	[65,66,67],
	[67,68,69],
	[65,67,69],
	[69,70,71],
	[71,72,73],
	[69,71,73],
	[65,69,73],
	[57,65,73],
	[53,57,73],
	[51,53,73],
	[50,51,73],
	[74,75,93],
	[93,92,74],
	[75,76,94],
	[94,93,75],
	[76,77,95],
	[95,94,76],
	[77,78,96],
	[96,95,77],
	[78,79,97],
	[97,96,78],
	[79,80,98],
	[98,97,79],
	[80,81,99],
	[99,98,80],
	[81,82,100],
	[100,99,81],
	[82,83,101],
	[101,100,82],
	[83,84,102],
	[102,101,83],
	[84,85,103],
	[103,102,84],
	[85,86,104],
	[104,103,85],
	[86,87,105],
	[105,104,86],
	[87,88,106],
	[106,105,87],
	[88,89,107],
	[107,106,88],
	[89,90,108],
	[108,107,89],
	[90,91,109],
	[109,108,90],
	[91,74,92],
	[92,109,91],
	[90,89,88],
	[88,87,86],
	[86,85,84],
	[88,86,84],
	[84,83,82],
	[82,81,80],
	[84,82,80],
	[80,79,78],
	[78,77,76],
	[80,78,76],
	[84,80,76],
	[88,84,76],
	[76,75,74],
	[88,76,74],
	[90,88,74],
	[91,90,74],
	[93,94,95],
	[95,96,97],
	[97,98,99],
	[95,97,99],
	[99,100,101],
	[101,102,103],
	[99,101,103],
	[103,104,105],
	[105,106,107],
	[103,105,107],
	[99,103,107],
	[95,99,107],
	[107,108,109],
	[95,107,109],
	[93,95,109],
	[92,93,109]
)


--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
local vertArray2 = #(
	[-1.9215e-005,0.250613,0],
	[0.0499808,0.250613,0],
	[0.0999808,0.250613,0],
	[-1.92076e-005,0.200613,0],
	[0.0499808,0.200613,0],
	[0.0999808,0.200613,0],
	[0.0499808,0.150613,0],
	[0.0999808,0.150613,0],
	[-1.91852e-005,0.100613,0],
	[0.0499808,0.100613,0],
	[0.0999808,0.100613,0],
	[-1.91778e-005,0.0506134,0],
	[0.0499808,0.0506134,0],
	[0.0999808,0.0506134,0],
	[0.149981,0.0506135,0],
	[-1.91703e-005,0.000613436,0],
	[0.0499808,0.000613451,0],
	[0.0999808,0.000613451,0],
	[0.149981,0.000613458,0],
	[0.249981,0.000613473,0],
	[-0.0451318,0.250613,0],
	[-0.0451318,0.200613,0],
	[-0.0933458,0.250613,0],
	[-0.0933458,0.200613,0],
	[-0.0692388,0.0949005,0],
	[0.0699033,0.0508152,0.120605],
	[0.0692219,0.0456388,0.120605],
	[0.0672239,0.0408152,0.120605],
	[0.0640455,0.036673,0.120605],
	[0.0599033,0.0334947,0.120605],
	[0.0550797,0.0314966,0.120605],
	[0.0499033,0.0308152,0.120605],
	[0.044727,0.0314966,0.120605],
	[0.0399034,0.0334946,0.120605],
	[0.0357612,0.036673,0.120605],
	[0.0325828,0.0408151,0.120605],
	[0.0305848,0.0456388,0.120605],
	[0.0299033,0.0508151,0.120605],
	[0.0305848,0.0559915,0.120605],
	[0.0325828,0.0608151,0.120605],
	[0.0357612,0.0649573,0.120605],
	[0.0399033,0.0681356,0.120605],
	[0.044727,0.0701337,0.120605],
	[0.0499033,0.0708151,0.120605],
	[0.0550797,0.0701337,0.120605],
	[0.0599033,0.0681357,0.120605],
	[0.0640455,0.0649573,0.120605],
	[0.0672238,0.0608152,0.120605],
	[0.0692219,0.0559916,0.120605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0499033,0.0508151,0.160605],
	[0.0592073,0.0515949,0],
	[0.0586042,0.0481746,0],
	[0.0568677,0.045167,0],
	[0.0542073,0.0429346,0],
	[0.0509438,0.0417468,0],
	[0.0474708,0.0417468,0],
	[0.0442073,0.0429346,0],
	[0.0415468,0.045167,0],
	[0.0398104,0.0481746,0],
	[0.0392073,0.0515948,0],
	[0.0398104,0.055015,0],
	[0.0415468,0.0580227,0],
	[0.0442073,0.0602551,0],
	[0.0474708,0.0614429,0],
	[0.0509438,0.0614429,0],
	[0.0542073,0.0602551,0],
	[0.0568677,0.0580227,0],
	[0.0586042,0.0550151,0],
	[0.0592073,0.0515948,0.12],
	[0.0586042,0.0481746,0.12],
	[0.0568677,0.045167,0.12],
	[0.0542073,0.0429346,0.12],
	[0.0509438,0.0417468,0.12],
	[0.0474708,0.0417468,0.12],
	[0.0442073,0.0429346,0.12],
	[0.0415468,0.045167,0.12],
	[0.0398104,0.0481746,0.12],
	[0.0392073,0.0515948,0.12],
	[0.0398104,0.055015,0.12],
	[0.0415468,0.0580227,0.12],
	[0.0442073,0.0602551,0.12],
	[0.0474708,0.0614429,0.12],
	[0.0509438,0.0614429,0.12],
	[0.0542073,0.0602551,0.12],
	[0.0568677,0.0580227,0.12],
	[0.0586042,0.0550151,0.12]
)

local edgeVis2 = #(
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,true),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,true),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,true),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(true,true,false),
	#(false,false,false),
	#(true,false,true),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(true,true,false),
	#(false,false,false),
	#(true,false,true),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(false,false,false),
	#(true,true,false),
	#(false,false,false),
	#(true,false,true),
	#(true,true,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(true,true,false),
	#(true,true,false),
	#(false,false,false),
	#(false,false,false),
	#(false,false,false),
	#(true,true,false),
	#(false,false,false),
	#(true,false,true)
)

local faceSmoothGroups2 = #(
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	0,
	0,
	0,
	0,
	0,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	8,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1
)

local faceList2 = #(
	[1,4,5],
	[5,2,1],
	[2,5,6],
	[6,3,2],
	[5,7,8],
	[8,6,5],
	[9,10,7],
	[7,10,11],
	[11,8,7],
	[9,12,13],
	[13,10,9],
	[10,13,14],
	[14,11,10],
	[11,14,15],
	[15,8,11],
	[12,16,17],
	[17,13,12],
	[13,17,18],
	[18,14,13],
	[14,18,19],
	[19,15,14],
	[19,20,15],
	[1,21,22],
	[22,4,1],
	[21,23,24],
	[24,22,21],
	[24,25,22],
	[26,50,51],
	[51,27,26],
	[27,51,52],
	[52,28,27],
	[28,52,53],
	[53,29,28],
	[29,53,54],
	[54,30,29],
	[30,54,55],
	[55,31,30],
	[31,55,56],
	[56,32,31],
	[32,56,57],
	[57,33,32],
	[33,57,58],
	[58,34,33],
	[34,58,59],
	[59,35,34],
	[35,59,60],
	[60,36,35],
	[36,60,61],
	[61,37,36],
	[37,61,62],
	[62,38,37],
	[38,62,63],
	[63,39,38],
	[39,63,64],
	[64,40,39],
	[40,64,65],
	[65,41,40],
	[41,65,66],
	[66,42,41],
	[42,66,67],
	[67,43,42],
	[43,67,68],
	[68,44,43],
	[44,68,69],
	[69,45,44],
	[45,69,70],
	[70,46,45],
	[46,70,71],
	[71,47,46],
	[47,71,72],
	[72,48,47],
	[48,72,73],
	[73,49,48],
	[49,73,50],
	[50,26,49],
	[26,27,28],
	[28,29,30],
	[26,28,30],
	[30,31,32],
	[32,33,34],
	[30,32,34],
	[26,30,34],
	[34,35,36],
	[36,37,38],
	[34,36,38],
	[38,39,40],
	[40,41,42],
	[38,40,42],
	[34,38,42],
	[26,34,42],
	[42,43,44],
	[44,45,46],
	[42,44,46],
	[26,42,46],
	[46,47,48],
	[26,46,48],
	[49,26,48],
	[73,72,71],
	[71,70,69],
	[73,71,69],
	[69,68,67],
	[67,66,65],
	[69,67,65],
	[73,69,65],
	[65,64,63],
	[63,62,61],
	[65,63,61],
	[61,60,59],
	[59,58,57],
	[61,59,57],
	[65,61,57],
	[73,65,57],
	[57,56,55],
	[55,54,53],
	[57,55,53],
	[73,57,53],
	[53,52,51],
	[73,53,51],
	[50,73,51],
	[74,92,93],
	[93,75,74],
	[75,93,94],
	[94,76,75],
	[76,94,95],
	[95,77,76],
	[77,95,96],
	[96,78,77],
	[78,96,97],
	[97,79,78],
	[79,97,98],
	[98,80,79],
	[80,98,99],
	[99,81,80],
	[81,99,100],
	[100,82,81],
	[82,100,101],
	[101,83,82],
	[83,101,102],
	[102,84,83],
	[84,102,103],
	[103,85,84],
	[85,103,104],
	[104,86,85],
	[86,104,105],
	[105,87,86],
	[87,105,106],
	[106,88,87],
	[88,106,107],
	[107,89,88],
	[89,107,108],
	[108,90,89],
	[90,108,109],
	[109,91,90],
	[91,109,92],
	[92,74,91],
	[74,75,76],
	[76,77,78],
	[78,79,80],
	[76,78,80],
	[80,81,82],
	[82,83,84],
	[80,82,84],
	[76,80,84],
	[84,85,86],
	[86,87,88],
	[84,86,88],
	[76,84,88],
	[74,76,88],
	[88,89,90],
	[74,88,90],
	[91,74,90],
	[109,108,107],
	[107,106,105],
	[105,104,103],
	[107,105,103],
	[103,102,101],
	[101,100,99],
	[103,101,99],
	[107,103,99],
	[99,98,97],
	[97,96,95],
	[99,97,95],
	[107,99,95],
	[109,107,95],
	[95,94,93],
	[109,95,93],
	[92,109,93]
)


--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==

	on getDisplayMesh do 
	(
		if meshObj == undefined then 
		(
			meshObj = trimesh()
		)

		if ( lastSide == undefined ) or ( lastSide != isrightside ) then
		(
			if not isrightside then
			(
				setmesh meshObj vertices:vertArray faces:faceList
			
				for i = 1 to faceSmoothGroups.count do setFaceSmoothGroup meshObj i faceSmoothGroups[i]
				for i = 1 to faceSmoothGroups.count do 
				(
					for j = 1 to 3 do (setEdgeVis meshObj i j edgeVis[i][j])
				)
			)
			else
			(
				setmesh meshObj vertices:vertArray2 faces:faceList2
			
				for i = 1 to faceSmoothGroups2.count do setFaceSmoothGroup meshObj i faceSmoothGroups2[i]
				for i = 1 to faceSmoothGroups2.count do 
				(
					for j = 1 to 3 do (setEdgeVis meshObj i j edgeVis2[i][j])
				)
			)
		)

		lastSide = isrightside;

		return meshObj.mesh
	)

	on attachedToNode theNode do
	(
		local theWireColor = undefined;
		if height >= 0.0 then
		(
			theWireColor = orange;
		)
		else if ( isrightside ) then
		(
			theWireColor = red;
		)
		else
		(
			theWireColor = blue;
		)

		theNode.wirecolor = theWireColor;
	)

	tool create 
	( 
		on mousePoint click do 
		(
			nodeTM.translation = gridPoint;
			#stop 
		)

	)

)