--SceneControl.ms
--Allows selection, hiding and unhiding of Rockstar entities
--When defining new entity types to add to the scene control tool, add the new entity to TypeArray, and define the selection algorithm in the ColourScene Action function

--version 1.1	24/01/2011: 	helper category added, show all behaviour enhanced
--version 1.2	17/10/2011:	definition of stair collision chnaged, new category added, loco replaced with ramp and stair
--version 1.3	18/01/2011:	MILO category added

--to do:
--categories editor
--milo category
--particles category
--add selected row
--add lock/unlock functionality

filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetUIOps.ms")
filein "pipeline\\util\\drawablelod.ms"	--lod code
--filein "gta\utils\GtaUtilityFunc.ms"		--lod code

global SceneControl

struct SceneControlStruct
(
	Form,
	SceneControlFlowLayoutPanel,
	IniFilePath = RsConfigGetWildWestDir()+"script/max/rockstar_london/config/AD_Tools.ini",
	ToolTip,
	Banner,
	
	--preset definitions
	DS_Fill = (dotNetClass "System.Windows.Forms.DockStyle").Fill,
	FBS_SizableToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").SizableToolWindow,
	FBS_FixedToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").FixedToolWindow,
	FS_Flat = (dotNetClass "System.Windows.Forms.FlatStyle").Flat,
	FS_Standard = (dotNetClass "System.Windows.Forms.FlatStyle").Standard,
	SP_Manual =  (dotNetClass "System.Windows.Forms.FormStartPosition").Manual,
	Font_Calibri = dotNetObject "System.Drawing.Font" "Calibri" 8,
	TA_Center =  (dotNetClass "System.Drawing.ContentAlignment").MiddleCenter,
	TA_Left =  (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft,
	Padding_None = dotNetObject "System.Windows.Forms.Padding" 0,
	Padding_One = dotNetObject "System.Windows.Forms.Padding" 1,
	
	--colours
	RGB_Steel = (dotNetClass "System.Drawing.Color").FromARGB 200 210 220,
	RGB_Transparent = (dotNetClass "System.Drawing.Color").transparent,
	RGB_Black = (dotNetClass "System.Drawing.Color").Black,
	
	
	--variables
	TypeSet = #(),
	ButtonHeight = 18,
	BannerHeight = 30,
	ColourSceneButton,
	InfoPanel,
	NumButtonRows = 4,
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--EVENT HANDLERS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn GetColType this =
	(
		local colType = "general"
		local cameraflag = getAttr this (getattrindex "Gta Collision" "Camera")
		local physicsflag = getAttr this (getattrindex "Gta Collision" "Physics")
		local hdflag = getAttr this (getattrindex "Gta Collision" "Hi Detail")
		local locoflag = getAttr this (getattrindex "Gta Collision" "Loco")
		if (cameraflag and physicsflag and not hdflag and not locoflag) then colType = "physics"
		if (not cameraflag and not physicsflag and hdflag and not locoflag) then colType = "hidetail"
		if (not cameraflag and physicsflag and hdflag and not locoflag) then colType = "oldstair"
		if (not cameraflag and physicsflag and hdflag and locoflag) then colType = "stair"
		if (cameraflag and not physicsflag and not hdflag and locoflag) then colType = "ramp"
		
		return colType
	),
	
	fn NoneExportTest this =
	(
		Status = false
		if getAttr this (getattrindex "Gta Object" "Export Geometry") == true then Status = false
		if getAttr this (getattrindex "Gta Object" "Dont Export") == true then Status = true
		if getAttr this (getattrindex "Gta Object" "Dont Add To IPL") == true then Status = true
		return Status
	),
	
	fn SelectType Type Mode =
	(
		--selection algorithms
		--need to define selection critieria for each entity type here
		
		if Mode == "selected" then ModeSet = selection
		else ModeSet = objects
			
		ObjSet = #()
		
		case Type of
		(
			"Geometry":		
			(
				GeometryObjects = for this in ModeSet where (GetAttrClass this == "Gta Object") collect this
				ObjSet = (for this in GeometryObjects where (classOf this != XRefObject) and ((getLODChildren this).Count == 0) and (RsLodDrawable_GetHigherDetailModel this == undefined) and NoneExportTest this == false collect this)
			)
			"All":			ObjSet = Modeset
			"Light":		ObjSet = for this in ModeSet where superClassOf this == light collect this
			"General Collision":			
			(
				CollisionObjects = for this in ModeSet where (GetAttrClass this == "Gta Collision") collect this
				ObjSet = (for this in CollisionObjects where GetColType this == "general" collect this)
			)
			"Physics Collision":
			(
				CollisionObjects = for this in ModeSet where (GetAttrClass this == "Gta Collision") collect this
				ObjSet = (for this in CollisionObjects where GetColType this == "physics" collect this)
			)
			"Ramp Collision":
			(
				CollisionObjects = for this in ModeSet where (GetAttrClass this == "Gta Collision") collect this
				ObjSet = (for this in CollisionObjects where GetColType this == "ramp" collect this)
			)
			"Stair Collision":
			(
				CollisionObjects = for this in ModeSet where (GetAttrClass this == "Gta Collision") collect this
				ObjSet = (for this in CollisionObjects where GetColType this == "stair" collect this)
			)
			"Old Stair Collision":
			(
				CollisionObjects = for this in ModeSet where (GetAttrClass this == "Gta Collision") collect this
				ObjSet = (for this in CollisionObjects where GetColType this == "oldstair" collect this)
			)
			"HD Collision":
			(
				CollisionObjects = for this in ModeSet where (GetAttrClass this == "Gta Collision") collect this
				ObjSet = (for this in CollisionObjects where GetColType this == "hidetail" collect this)
			)
			"XRef":					
			(
				GeometryObjects = for this in ModeSet where (GetAttrClass this == "Gta Object") collect this
				ObjSet = (for this in GeometryObjects where classOf this == XRefObject and NoneExportTest this == false collect this)
			)
			"LOD":					
			(
				GeometryObjects = for this in ModeSet where (GetAttrClass this == "Gta Object") collect this
				ObjSet = (for this in GeometryObjects where (((getLODChildren this).Count > 0) or (RsLodDrawable_GetHigherDetailModel this != undefined)) and (NoneExportTest this == false) collect this)
			)
			"None Export":	
			(
				GeometryObjects = for this in ModeSet where (GetAttrClass this == "Gta Object") collect this
				ObjSet = (for this in GeometryObjects where NoneExportTest this == true collect this)
			)
			"Shape":
			(
				ObjSet = for this in Modeset where superClassOf this == shape collect this
			)
			"Helper":
			(
				ObjSet = (for this in Modeset where superClassOf this == helper and (GetAttrClass this != "Gta Collision") collect this)
			)
			"MILO":
			(
				ObjSet = (for this in ModeSet where (GetAttrClass this == "Gta MILOTri") collect this)
			)
			"Occlusion":
			(
				Objset = (for this in ModeSet where (classOf this == Box) collect this)
			)
		)
		return ObjSet
	),
	
	fn Help =
	(
		SceneControl.InfoPanel.Text = "Help"
		shellLaunch "https://mp.rockstargames.com/index.php/Scene_Control" ""
	),
	
	fn UnhideLayers =
	(
		for i = 0 to (LayerManager.count - 1) do
		(
			(LayerManager.getLayer i).ishidden = false
			hideByCategory.none()
		)
	),
	
	fn UnfreezeLayers =
	(
		for i = 0 to (LayerManager.count - 1) do
		(
			(LayerManager.getLayer i).isfrozen = false
		)
	),
	
	fn ReturnTypeColor type =
	(
		TypeColor = undefined
		if type != "All" then
		(
			for i = 0 to (SceneControl.Form.Controls.Item[0].Controls.Count - 1) do
			(		
				if (SceneControl.Form.Controls.Item[0].Controls.Item[i].tag == type) then
				(
					dotNetColor = SceneControl.Form.Controls.Item[0].Controls.Item[i].Controls.Item[0].BackColor
					TypeColor = (color dotNetColor.r dotNetColor.g dotNetColor.b)
				)
			)
		)
		return TypeColor
	),
	
	fn UnhideHelpers =
	(
		hideByCategory.helpers = false
	),
	
	fn UnhideShapes =
	(
		hideByCategory.shapes = false
	),
	
	fn ApplyAction s e =
	(
		Type = s.Parent.Tag
		Action = s.Tag		
		ObjSet = SceneControl.SelectType Type "all"
		
		--actions
		local ObjText
		if ObjSet.count == 1 then ObjText = " object" else ObjText = " objects"
		case Action of
		(
			"Select":	
			(
				select ObjSet
				SceneControl.InfoPanel.Text  = (ObjSet.count as string + " " + Type + ObjText + " selected")
				if (e.button == e.button.right) then print "yay"
				else print "nay"
-- 				showEvents s
			)
			"Hide":
			(
				hide ObjSet
				SceneControl.InfoPanel.Text  = (ObjSet.count as string + " " + Type + ObjText + " hidden")
			)
			"Show":
			(
				unhide ObjSet
				SceneControl.InfoPanel.Text  = (ObjSet.count as string + " " + Type + ObjText + " unhidden")
			)
			"Freeze":
			(
				freeze ObjSet
				SceneControl.InfoPanel.Text  = (ObjSet.count as string + " " + Type + ObjText + " frozen")
			)
			"Unfreeze":
			(
				unfreeze ObjSet
				SceneControl.InfoPanel.Text  = (ObjSet.count as string + " " + Type + ObjText + " unfrozen")
			)
		)
	),
	
	fn ColourSelected =
	(
		for this in SceneControl.TypeSet do
		(
			local ObjType = SceneControl.SelectType this.Tag "selected"
			local ObjColor = this.Controls.Item[0].BackColor
			ObjType.wirecolor = (color ObjColor.r ObjColor.g ObjColor.b)
		)
		SceneControl.InfoPanel.Text = "Wirecolors applied to selected"
	),
	
	fn ColourScene =
	(
		for this in SceneControl.TypeSet do
		(
			local ObjType = SceneControl.SelectType this.Tag "all"
			local ObjColor = this.Controls.Item[0].BackColor
			ObjType.wirecolor = (color ObjColor.r ObjColor.g ObjColor.b)
		)
		SceneControl.InfoPanel.Text = "Wirecolors applied to scene"
	),
	
	fn SelectNone = 
	(
		ClearSelection()
		SceneControl.InfoPanel.Text  = "Selection cleared"
	),
	
	fn HideSelected =
	(
		max hide selection
		SceneControl.InfoPanel.Text  = "Selected objects hidden"
	),
	
	fn HideUnselected =
	(
		max hide inv
		SceneControl.InfoPanel.Text  = "Unselected objects hidden"
	),
	
	fn FreezeSelected =
	(
		max freeze selection
		SceneControl.InfoPanel.Text = "Selected objects frozen"
	),
	
	fn FreezeUnselected =
	(
		max freeze inv
		SceneControl.InfoPanel.Text = "Unselected objects frozen"
	),
	
	fn Isolate =
	(
		if selection.count > 0 then
		(
			try
			(
				if Iso2Roll.open then
				(
					DestroyDialog Iso2Roll
				)
				else
				(
					objset = selection
					CreateDialog Iso2Roll
				)
			)
			catch
			(
				CreateDialog Iso2Roll
			)
			SceneControl.InfoPanel.Text  = "Selection isolated"
		)
		else
		(
			SceneControl.InfoPanel.Text = "Warning: Nothing selected"
		)
	),
	
	fn Mystery =
	(
		SceneControl.InfoPanel.Text = "?"
	),
	
	fn CreateActionButton ButtonText Action Type =
	(
		NewButton = dotNetObject "Button"
		NewButton.FlatStyle = FS_Standard
		NewButton.Dock = DS_Fill
		NewButton.BackColor = RGB_Transparent
		NewButton.ForeColor = RGB_Black
		NewButton.Text = ButtonText
		NewButton.Tag = Action
		ToolTip.SetToolTip NewButton (Action + " " + Type + " objects")
		NewButton.Font = Font_Calibri
		NewButton.TextAlign = TA_Center
		NewButton.Margin = Padding_None
		NewButton.Padding = Padding_None
		dotNet.AddEventHandler NewButton "Click" ApplyAction
		return NewButton
	),
	
	fn CreateStandardButton ButtonText =
	(
		NewButton = dotNetObject "Button"
		NewButton.Text = ButtonText
		NewButton.Dock = DS_Fill
		NewButton.BackColor = RGB_Transparent
		NewButton.ForeColor = RGB_Black
		NewButton.FlatStyle = FS_Standard
		NewButton.Margin = Padding_None
		NewButton.Font = Font_Calibri
		ToolTip.SetToolTip NewButton ButtonText
		return NewButton
	),
	
	fn PickColor s e =
	(
		myColor = colorPickerDlg (color s.BackColor.r s.BackColor.g s.BackColor.b) "Pick wirecolor" alpha:false pos:[-1,-1]
		s.BackColor = (dotNetClass "System.Drawing.Color").fromARGB myColor.r myColor.g myColor.b
	),
	
	fn CreateType Type =
	(
		ButtonWidth = ButtonHeight + 4
		NewType = dotNetObject "TableLayoutPanel"
		NewType.RowCount = 1
		NewType.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" ButtonHeight)
		NewType.ColumnCount = 6
		NewType.Margin = Padding_None
		NewType.Dock = DS_Fill
		NewType.Tag = Type
		NewType.ColumnStyles.add (RS_dotNetObject.ColumnStyleObject "absolute" ButtonHeight)
		NewType.ColumnStyles.add (RS_dotNetObject.ColumnStyleObject "percent" 100)
		NewType.ColumnStyles.add (RS_dotNetObject.ColumnStyleObject "absolute" ButtonHeight)
		NewType.ColumnStyles.add (RS_dotNetObject.ColumnStyleObject "absolute" ButtonHeight)
		NewType.ColumnStyles.add (RS_dotNetObject.ColumnStyleObject "absolute" ButtonHeight)
		NewType.ColumnStyles.add (RS_dotNetObject.ColumnStyleObject "absolute" ButtonHeight)
		
		SwatchButton = dotNetObject "Button"
		SwatchButton.FlatStyle = FS_Flat
		SwatchButton.Dock = DS_Fill
		SwatchButton.BackColor = RGB_Steel
		SwatchButton.Margin = dotNetObject "System.Windows.Forms.Padding" 1
		ToolTip.SetToolTip SwatchButton (Type + " Wirecolor")
		dotNet.AddEventHandler SwatchButton "Click" PickColor
		NewType.Controls.Add SwatchButton 0 0
		
		SelectButton = CreateActionButton Type "Select" Type
		NewType.Controls.Add SelectButton 1 0
		
		HideButton = CreateActionButton "H" "Hide" Type
		NewType.Controls.Add HideButton 2 0
		
		UnhideButton = CreateActionButton "S" "Show" Type
		NewType.Controls.Add UnhideButton 3 0
		
		FreezeButton = CreateActionButton "F" "Freeze" Type
		NewType.Controls.Add FreezeButton 4 0
		
		UnfreezeButton = CreateActionButton "U" "Unfreeze" Type
		NewType.Controls.Add UnfreezeButton 5 0
		
		return NewType
	),
	
	fn PickColor s e =
	(
		myColor = colorPickerDlg (color s.BackColor.r s.BackColor.g s.BackColor.b) "Pick wirecolor" alpha:false pos:[-1,-1]
		if myColor != undefined then s.BackColor = (dotNetClass "System.Drawing.Color").fromARGB myColor.r myColor.g myColor.b
	),
	
	fn GetRandomBanner =
	(
		local BannerImages = #(
			"scenecontrolbanner_cherry.png",
			"scenecontrolbanner_ice.png",
			"scenecontrolbanner_lime.png",
			"scenecontrolbanner_grey.png",
			"scenecontrolbanner_tango.png",
			"scenecontrolbanner_pink.png",
			"scenecontrolbanner_sepia.png"
		)
		
		local ImagePath = RsConfigGetWildWestDir()+"script/max/rockstar_london/images/AD_Icons/" + BannerImages[random 1 (BannerImages.count)]
		
		SceneControl.Banner.Image = (dotNetClass "System.Drawing.Image").FromFile ImagePath
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn UpdateLocalConfigFileSave =
	(
		setINISetting IniFilePath "SceneControl" "WinLocX" (Form.Location.x as string)
		setINISetting IniFilePath "SceneControl" "WinLocY" (Form.Location.y as string)
		setINISetting IniFilePath "SceneControl" "WinWidth" (Form.Width as string)
		setINISetting IniFilePath "SceneControl" "WinHeight" (Form.Height as string)
		for this in TypeSet do
		(
			setINISetting IniFilePath "SceneControl" (this.Tag + "R") (this.Controls.Item[0].BackColor.r as string)
			setINISetting IniFilePath "SceneControl" (this.Tag + "G") (this.Controls.Item[0].BackColor.g as string)
			setINISetting IniFilePath "SceneControl" (this.Tag + "B") (this.Controls.Item[0].BackColor.b as string)
		)
	),
	
	fn UpdateLocalConfigFileLoad =
	(
		--default values
		local WinLocX = 0
		local WinLocY = 40
		local WinWidth = 200
		local WinHeight = ((TypeSet.Count * ButtonHeight) + (ButtonHeight * NumButtonRows) + 66  + 4 + BannerHeight)
		local TypeColors = #()
		for this in TypeSet do
		(
			TypeColors[TypeColors.count + 1] = (color 200 210 220)
		)
		
		try
		(
			WinLocX = getINISetting IniFilePath "SceneControl" "WinLocX" as integer
			WinLocY = getINISetting IniFilePath "SceneControl" "WinLocY" as integer
			for i = 1 to TypeSet.count do
			(
				TypeColors[i].r = getINISetting IniFilePath "SceneControl" (TypeSet[i].Tag + "R") as integer
				TypeColors[i].g = getINISetting IniFilePath "SceneControl" (TypeSet[i].Tag + "G") as integer
				TypeColors[i].b = getINISetting IniFilePath "SceneControl" (TypeSet[i].Tag + "B") as integer
			)
		)
		catch()
		Form.Location = dotNetObject "System.Drawing.Point" WinLocX WinLocY
		Form.Size = dotNetObject "System.Drawing.Size" WinWidth WinHeight
		for i = 1 to TypeSet.count do
		(
			TypeSet[i].Controls.Item[0].BackColor = (dotNetClass "System.Drawing.Color").FromARGB TypeColors[i].r TypeColors[i].g TypeColors[i].b
		)
	),
	fn DispatchLoadIniFile s e = SceneControl.UpdateLocalConfigFileLoad(),	
	fn DispatchSaveIniFile s e = SceneControl.UpdateLocalConfigFileSave(),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn CreateUI =
	(		
		local TypeArray =
		#(
			"Geometry"
			,"General Collision"
			,"HD Collision"
			,"Physics Collision"
			,"Ramp Collision"
			,"Stair Collision"
			,"Old Stair Collision"
			,"XRef"
			,"LOD"
			,"None Export"
			,"Light"
			,"Shape"
			,"Helper"
			,"MILO"
			,"Occlusion"
		)
		
		-- form setup		
		Form = dotNetObject "maxCustomControls.maxForm"
		Form.Text = "Scene Control"
		Form.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		Form.Location = dotNetObject "system.drawing.point" 0 80
		Form.FormBorderStyle =  (dotNetClass "System.Windows.Forms.FormBorderStyle").FixedSingle
		Form.SizeGripStyle = (dotNetClass "SizeGripStyle").Show
		Form.StartPosition = SP_Manual
		dotNet.AddEventHandler Form "Load" DispatchLoadIniFile
		dotNet.AddEventHandler Form "Closing" DispatchSaveIniFile
		
		--content
		
		ToolTip = dotnetobject "ToolTip"
		Table = dotNetObject "TableLayoutPanel"
		Table.Dock = DS_Fill
		Form.Controls.Add Table
		Table.RowCount = (1 + TypeArray.Count + 3)
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" BannerHeight)
		
		
		for i = 2 to (TypeArray.Count + 2) do Table.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" ButtonHeight)	--type rows
		Table.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" (ButtonHeight * NumButtonRows))	--button panel row
		Table.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 24)				--helpPanel row
		Table.ColumnCount = 1
		Table.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		Form.Controls.Add Table
		
		Banner = dotnetobject "PictureBox"
		Banner.Dock = DS_Fill
		Banner.Margin = Padding_None
		GetRandomBanner()
		Table.Controls.Add Banner 0 0
		dotNet.AddEventHandler Banner "Click" GetRandomBanner
			
		for i = 1 to (TypeArray.Count) do
		(
			NewRow = CreateType (TypeArray[i])
			Table.Controls.Add NewRow 0 (i)
			append TypeSet NewRow
		)
		
		AllRow = CreateType "All"
		AllRow.Controls.RemoveAt 0
		Table.Controls.Add AllRow 0 (TypeArray.Count + 1)
		
		--special cases
		--these need extra functionality to get around different ways of hiding stuff in max
		for i = 0 to (Table.Controls.Count - 1) do
		(
			case Table.Controls.Item[i].Tag of
			(
				"Shape": dotNet.AddEventHandler Table.Controls.Item[i].Controls.Item[3] "Click" UnhideShapes
				"Helper": dotNet.AddEventHandler Table.Controls.Item[i].Controls.Item[3] "Click" UnhideHelpers
				"All":
				(
					dotNet.AddEventHandler Table.Controls.Item[i].Controls.Item[2] "Click" UnhideLayers
					dotNet.AddEventHandler Table.Controls.Item[i].Controls.Item[4] "Click" UnfreezeLayers
				)
			)
		)
		
		ButtonPanel = dotNetObject "TableLayoutPanel"
		ButtonPanel.RowCount = NumButtonRows
		for i = 1 to ButtonPanel.RowCount do	ButtonPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" ButtonHeight)
		ButtonPanel.ColumnCount = 2
		ButtonPanel.BackColor = RGB_Steel
		ButtonPanel.Dock = DS_Fill
		ButtonPanel.Margin = Padding_None
		ButtonPanel.Padding = Padding_None
		ButtonPanel.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 50)
		ButtonPanel.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 50)
		Table.Controls.Add ButtonPanel 0 (TypeArray.Count + 2)
		
		IsolateButton = CreateStandardButton "Isolate"
		dotNet.AddEventHandler IsolateButton "Click" Isolate
		ButtonPanel.Controls.Add IsolateButton 0 0
		
		ClearButton = CreateStandardButton "Clear Selection"
		dotNet.AddEventHandler ClearButton "Click" SelectNone
		ButtonPanel.Controls.Add ClearButton 1 0
		
		HideSelectedButton = CreateStandardButton "Hide Selected"
		dotNet.AddEventHandler HideSelectedButton "Click" HideSelected
		ButtonPanel.Controls.Add HideSelectedButton 0 1
		
		HideUnselectedButton = CreateStandardButton "Hide Unselected"
		dotNet.AddEventHandler HideUnselectedButton "Click" HideUnselected
		ButtonPanel.Controls.Add HideUnselectedButton 1 1
		
		FreezeSelectedButton = CreateStandardButton "Freeze Selected"
		dotNet.AddEventHandler FreezeSelectedButton "Click" FreezeSelected
		ButtonPanel.Controls.Add FreezeSelectedButton 0 2
		
		FreezeUnselectedButton = CreateStandardButton "Freeze Unselected"
		dotNet.AddEventHandler FreezeUnselectedButton "Click" FreezeUnselected
		ButtonPanel.Controls.Add FreezeUnselectedButton 1 2
		
		ColourSelectedButton = CreateStandardButton "Colour Selected"
		dotNet.AddEventHandler ColourSelectedButton "Click" ColourSelected
		ButtonPanel.Controls.Add ColourSelectedButton 0 3
		
		ColourSceneButton = CreateStandardButton "Colour Scene"
		dotNet.AddEventHandler ColourSceneButton "Click" ColourScene
		ButtonPanel.Controls.Add ColourSceneButton 1 3
		
		HelpPanel = dotNetObject "TableLayoutPanel"
		HelpPanel.RowCount = 1
		HelpPanel.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 24)
		HelpPanel.ColumnCount = 2
		HelpPanel.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 50)
		HelpPanel.columnStyles.add (RS_dotNetObject.columnStyleObject "absolute" 24)
		HelpPanel.Dock = DS_Fill
		HelpPanel.Margin = Padding_None
		HelpPanel.Padding = Padding_None
		Table.Controls.Add HelpPanel 0 (TypeArray.count + 3)
		
		InfoPanel = dotNetObject "Label"
		InfoPanel.Text = "Scene Control"
		InfoPanel.Dock = DS_Fill
		InfoPanel.Font = Font_Calibri
		InfoPanel.Margin = Padding_None
		InfoPanel.TextAlign = TA_Center
		InfoPanel.BackColor = RGB_Transparent
		InfoPanel.ForeColor = RGB_Black
		local ImagePath = RsConfigGetWildWestDir()+"script/max/rockstar_london/images/SceneControlBG.bmp"
		InfoPanel.BackgroundImage = (dotNetClass "System.Drawing.Image").FromFile ImagePath
		HelpPanel.Controls.Add InfoPanel 0 0
		
		HelpButton = CreateStandardButton "?"
		ToolTip.SetToolTip HelpButton "Help"
		dotNet.AddEventHandler HelpButton "Click" Help
		HelpPanel.Controls.Add HelpButton 1 0
		
		--draw form
		Form.ShowModeless()
		Form
	)
)

if SceneControl != undefined then
(
	SceneControl.Form.Close()
)

SceneControl = SceneControlStruct()
SceneControl.CreateUI()
