


fn workingPivotToEdge =
(
	local theObject = selection[1]
	local theGuide = getNodeByName "RSL_WorkingGrid" exact:true
	
	if theGuide == undefined or classOf theGuide != grid then
	(
		theGuide = grid name:"RSL_WorkingGrid" length:1.0 width:1.0 grid:0.1
		theGuide.isHidden = true
	)
	
	if theObject != undefined then
	(
		if theObject.selectedEdges.count == 1 then
		(
			local verts
			
			case (classOf theObject) of
			(
				editable_mesh:
				(
					verts = (meshOp.getVertsUsingEdge theObject theObject.selectedEdges[1]) as array
				)
				editable_poly:
				(
					verts = (polyOp.getVertsUsingEdge theObject theObject.selectedEdges[1]) as array
				)
			)
			
			local normal = normalize (theObject.verts[verts[1]].pos - theObject.verts[verts[2]].pos)
			local pos = theObject.verts[verts[2]].pos + ((theObject.verts[verts[1]].pos - theObject.verts[verts[2]].pos) * 0.5)
			
			local matrix = (matrixFromNormal normal)
			matrix.row4 = pos
			
			if theGuide == undefined or not (isValidNode theGuide) then
			(
				theGuide = grid name:"RSL_WorkingGrid" length:1.0 width:1.0 grid:0.1
				theGuide.isHidden = true
			)
			
			theGuide.transform = matrix
			
			toolMode.coordSysNode = theGuide
		)
	)
)

fn workingPivotToFace =
(
	local theObject = selection[1]
	local theGuide = getNodeByName "RSL_WorkingGrid" exact:true
	
	if theGuide == undefined or classOf theGuide != grid then
	(
		theGuide = grid name:"RSL_WorkingGrid" length:1.0 width:1.0 grid:0.1
		theGuide.isHidden = true
	)
	
	if theObject != undefined then
	(
		if theObject.selectedFaces.count == 1 then
		(
			local faceIndex = theObject.selectedFaces[1].index
			local normal
			local pos
			
			case (classOf theObject) of
			(
				editable_mesh:
				(
					normal = getFaceNormal theObject faceIndex
					pos = meshOp.getFaceCenter theObject faceIndex
				)
				editable_poly:
				(
					normal = polyOp.getFaceNormal theObject faceIndex
					pos = polyOp.getFaceCenter theObject faceIndex
				)
			)
			
			local matrix = (matrixFromNormal normal)
			matrix.row4 = pos
			
			theGuide.transform = matrix
			
			toolMode.coordSysNode = theGuide
		)
	)
)
