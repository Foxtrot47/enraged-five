
try (DestroyDialog RSL_NormalDisplay) catch()

rollout RSL_NormalDisplay "Normal Display"
(
	checkButton chkb_switch "Show Normals" width:150
	label lbl_colour "Line Colour" align:#left across:2
	colorPicker cpk_lineColour color:blue align:#right --offset:[12,0]
	label lbl_selected "Selected Only" align:#left across:2
	checkBox chk_selected "" checked:true align:#right offset:[12,0]
	label lbl_size "Size" align:#left across:2
	spinner spn_normalSize "" range:[0,100,1.0] scale:0.01 width:60 align:#right offset:[6,0]
	
	on RSL_NormalDisplay open do 
	(
		RSL_showNormals = false
		manipulateMode = false
		manipulateMode = true
	)
	
	on RSL_NormalDisplay close do 
	(
		RSL_showNormals = false
		manipulateMode = false
		manipulateMode = true
	)
	
	on chkb_switch changed state do 
	(
		RSL_showNormals = state
		
		if state and not manipulateMode then
		(
			manipulateMode = true
		)
		else
		(
			manipulateMode = false
			manipulateMode = true
		)
	)
	
	on cpk_lineColour changed color do 
	(
		manipulateMode = false
		manipulateMode = true
	)
	
	on spn_normalSize changed value do 
	(
		manipulateMode = false
		manipulateMode = true
	)
	
	on chk_selected changed state do 
	(
		manipulateMode = false
		manipulateMode = true
	)
)

createDialog RSL_NormalDisplay style:#(#style_toolwindow, #style_sysmenu)