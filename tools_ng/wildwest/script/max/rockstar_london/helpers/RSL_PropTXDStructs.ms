

struct RSL_PropData
(
	name,
	node,
	TXD,
	LOD,
	children = #(),
	usedArray = #(),
	
	idxDontExport = getAttrIndex "Gta Object" "Dont Export", -- dont export attribute index
	idxDontAdd = getAttrIndex "Gta Object" "Dont Add To IPL", -- dont add to IPL attribute index
	idxTXD = getattrindex "Gta Object" "TXD",
	idxFragment = getattrindex "Gta Object" "Is Fragment",
	
	mapped fn setChildTXDRec object invalidate:false =
	(
		if (getAttrClass object) == "Gta Object" then
		(
			setAttr object.node idxTXD TXD
			
			if invalidate then
			(
				setAttr object.node idxDontExport true
				setAttr object.node idxDontAdd true
			)
			
			for child in object.children do 
			(
				setChildTXDRec child
			)
		)
	),
	
	fn setLODTXDRec object invalidate:false =
	(
		if object != undefined then
		(
			setAttr object.node idxTXD TXD
			
			if invalidate then
			(
				setAttr object.node idxDontExport true
				setAttr object.node idxDontAdd true
			)
			
			setLODTXDRec object.LOD
		)
	),
	
	fn setTXD newTXD invalidate:false =
	(
		TXD = newTXD
		setAttr node idxTXD TXD
		
		setLODTXDRec LOD invalidate:invalidate
		setChildTXDRec children invalidate:invalidate
	),
	
	fn invalidate =
	(
		setAttr node idxDontExport true
		setAttr node idxDontAdd true
		
		setTXD "CHANGEME" invalidate:true
	),
	
	fn addLODNodeRec item root =
	(
		if item != undefined then
		(
			local LODNode = root.Nodes.Add item.node.name
			LODNode.imageIndex = 5
			LODNode.SelectedImageIndex = 5
			
			LODNode.tag = dotNetMXSValue item
			LODNode.expand()
			
			addLODNodeRec item.LOD LODNode
		)
	),
	
	mapped fn addChildNodeRec child root =
	(
		local childNode = root.Nodes.Add child.node.name
		childNode.imageIndex = 9
		childNode.SelectedImageIndex = 9
		
		childNode.tag = dotNetMXSValue child
		
		childNode.expand()
		
		addChildNodeRec child.children childNode
	),
	
	fn toTreeview root this =
	(
		local propRoot = root.Nodes.add (name + " : " + usedArray.count as string)
		propRoot.tag = dotNetMXSValue this
		
		if children.count > 0 or (getAttr node idxFragment) then
		(
			propRoot.imageIndex = 1
			propRoot.SelectedImageIndex = 1
		)
		else
		(
			propRoot.imageIndex = 9
			propRoot.SelectedImageIndex = 9
		)
		
		if usedArray.count == 0 then
		(
-- 			propRoot.backColor = RS_dotNetClass.colourClass.fromARGB 64 255 0 0
			propRoot.imageIndex += 1
			propRoot.SelectedImageIndex += 1
		)
		
		if LOD != undefined then
		(
			local LODNode = propRoot.nodes.add "LODs"
			LODNode.imageIndex = 5
			LODNode.SelectedImageIndex = 5
			
			addLODNodeRec LOD LODNode
			
			LODNode.expand()
		)
		
		if children.count > 0 then
		(
			local childrenNode = propRoot.nodes.add "Children"
			childrenNode.imageIndex = 0
			childrenNode.SelectedImageIndex = 0
			
			addChildNodeRec children childrenNode
			
			childrenNode.expand()
		)
		
		if usedArray.count > 0 then
		(
			local mapFilesNode = propRoot.nodes.add "Map Files"
			mapFilesNode.imageIndex = 7
			mapFilesNode.SelectedImageIndex = 7
			
			for mapFile in usedArray do 
			(
				local mapFileNode = mapFilesNode.nodes.add mapFile
				mapFileNode.imageIndex = 6
				mapFileNode.SelectedImageIndex = 6
			)
			
			mapFilesNode.expand()
		)
		
-- 		if usedArray.count > 0 then
-- 		(
-- 			propRoot.expand()
-- 		)
	)
)