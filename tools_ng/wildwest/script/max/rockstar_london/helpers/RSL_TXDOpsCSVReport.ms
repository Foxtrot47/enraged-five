-------------------------------------------------------------------------------------------------------------------------------------------
-- RSL_TXDOpsCSVReport
-- By Paul Truss
-- Senior Technical Artist
-- Rockstar Games London
-- 15/10/2010
--
-- Structure containing CSV report functions. Called on exporting CSV files from parentise mode.
-------------------------------------------------------------------------------------------------------------------------------------------

struct RSL_TXDOpsCSVReport
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- Struct variables
	---------------------------------------------------------------------------------------------------------------------------------------
	totalMapfileCount = 0, -- used to create collumns in the textureListView
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- UI variables
	---------------------------------------------------------------------------------------------------------------------------------------
	csvForm,parentListView,textureListView,
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- readCSV() reads the parsed file and appends a RSL_CSVTextureData to the parsed result which is an array.
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn readCSV file &result =
	(
		local dataStream = openFile file
		
		if dataStream != undefined then
		(
			local line = readLine dataStream
			
			while not (eof dataStream) do
			(
				local textureData = RSL_CSVTextureData()
				
				local filtered = filterString line ","
				
				textureData.name = filtered[1]
				textureData.size = filtered[2]
				
				for i = 3 to filtered.count do 
				(
					append textureData.mapfileArray filtered[i]
				)
				
				append result textureData
				
				line = readLine dataStream
			)
		)
		
		close dataStream
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- addParentItem() Adds an item to the parentListView
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn addParentItem data =
	(
		local newItem = dotNetObject "ListViewItem" data.name
		newItem.tag = dotNetMXSValue data.textureArray
		
		parentListView.Items.Add newItem
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- populateParentListView() Populates the parentListView with data from the array
	---------------------------------------------------------------------------------------------------------------------------------------
	fn populateParentListView array =
	(
		local parentArray = #()
		
		for data in array do 
		(
			local parentData = (DataPair name:data.name textureArray:#())
			
			-- read the CSV file. Read function appends texture entries to the parentData.textureArray
			readCSV data.file &parentData.textureArray
			
			append parentArray parentData
		)
		
		addParentItem parentArray
		
		parentListView.AutoResizeColumns (dotNetClass "ColumnHeaderAutoResizeStyle").HeaderSize
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- addTextureItem() Adds an item to the textureListView
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn addTextureItem texture =
	(
		local newItem = dotNetObject "ListViewItem" texture.name
		newItem.subItems.add texture.size
		
		local count = 0
		
		for mapfile in texture.mapfileArray do 
		(
			local subItem = newItem.subItems.add mapfile
			
			count += 1
		)
		
		-- make sure we have the largest number of mapfile so that we create the correct number of collumns in the textureListView
		if count > totalMapfileCount then
		(
			totalMapfileCount = count
		)
		
		textureListView.items.add newItem
	),
	
	
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- disableAccelerators() called by controls that can have keyboards input to ensure max lets them accept it
	---------------------------------------------------------------------------------------------------------------------------------------
	fn disableAccelerators s e =
	(
		enableAccelerators = false
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchParentListClick() Dispatches parentListClick() Called when the user clicks on a item in the parentListView
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchParentListClick s e =
	(
		RSL_TXDOps.CSVFuncs.parentListClick s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- parentListClick() Populates the textureListView with data from the selected item's tag.value which is a dataPair
	---------------------------------------------------------------------------------------------------------------------------------------
	fn parentListClick s e =
	(
		if s.selectedItems.count == 1 then
		(
			local data = s.selectedItems.item[0].tag.value
			
			textureListView.items.clear()
			
			addTextureItem data
			
			for i = 1 to totalMapfileCount do 
			(
				textureListView.columns.add "Mapfile"
			)
			
			textureListView.AutoResizeColumns (dotNetClass "ColumnHeaderAutoResizeStyle").HeaderSize
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- CSVReport() Creates an error report form from the items in the parsed array. Called after a CSV export.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn CSVReport array =
	(
		--Get the max handle pointer.
		local maxHandlePointer=(Windows.GetMAXHWND())
		
		--Convert the HWND handle of Max to a dotNet system pointer
		local sysPointer = DotNetObject "System.IntPtr" maxHandlePointer
		
		--Create a dotNet wrapper containing the maxHWND
		local maxHwnd = DotNetObject "MaxCustomControls.Win32HandleWrapper" sysPointer
		
		csvForm = RS_dotNetUI.form "csvForm" text:"Parent TXD Texture Report" Size:[512,768] min:[128,256] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		
		parentListView = RS_dotNetUI.listView "parentListView" dockStyle:RS_dotNetPreset.DS_Fill
		parentListView.view = (dotNetClass "System.Windows.Forms.View").Details
		parentListView.HeaderStyle = (dotNetClass "System.Windows.Forms.ColumnHeaderStyle").Nonclickable
		parentListView.fullRowSelect = true
		parentListView.HideSelection = false
		parentListView.columns.add "Name"
		dotnet.addEventHandler parentListView "click" dispatchParentListClick
		dotnet.addEventHandler parentListView "Enter" disableAccelerators
		
		textureListView = RS_dotNetUI.listView "textureListView" dockStyle:RS_dotNetPreset.DS_Fill
		textureListView.view = (dotNetClass "System.Windows.Forms.View").Details
		textureListView.HeaderStyle = (dotNetClass "System.Windows.Forms.ColumnHeaderStyle").Nonclickable
		textureListView.fullRowSelect = true
		textureListView.HideSelection = false
		textureListView.columns.add "Name"
		textureListView.columns.add "Size"
		dotnet.addEventHandler textureListView "Enter" disableAccelerators
		
		mainSplitContainer = RS_dotNetUI.SplitContainer "mainSplitContainer" borderStyle:RS_dotNetPreset.BS_FixedSingle dockStyle:RS_dotNetPreset.DS_Fill 
		mainSplitContainer.Orientation = (dotNetClass "System.Windows.Forms.Orientation").Horizontal
		
		mainSplitContainer.Panel1.Controls.Add parentListView
		mainSplitContainer.Panel2.Controls.Add textureListView
		
		populateParentListView array
		
		csvForm.controls.add mainSplitContainer
		
		mainSplitContainer.SplitterDistance = 256
		
		csvForm.show maxHwnd
	)	
)