
global init_RSL_ObjectTypeOps

fileIn (scriptsPath + "pipeline\\util\\xml.ms")
dotnet.loadAssembly (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\dll\\RGLExportHelpers.dll")

struct RSL_ObjectType
(
	name,
	class,
	flagArray = #()
)

struct RSL_ObjectTypeOps
(
	helpLink = "https://mp.rockstargames.com/index.php/Object_Type_Operations",
	localConfigINI = (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\ObjectTypeOps.ini"),
	typeXML = (getdir #plugCfg + "objectTypes.xml"),
	XmlEditor = (rsConfig.toolsRoot + "\\tools\\bin\\utils\\RSL_ObjectTypeXMLEditor.exe"),
	rsNorthAttFile = (getdir #plugCfg + "rsnorth.att"),
	GTAObjectAttributeList = #(),
	flagDefinition = #("Name","Value"),
	typeDefinition = #("type"),
	objectTypeArray = #(),
	theObject,
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= dotNet control variables =-=-=-=-=-=-=-=-=-=-=-=-=--
	ObjectTypeOpsForm,
	mainTableLayout,
	currentObjectNameLabel,currentObjectTypeListView,
	typeFlagsTableLayout,typeFlagLabel,typeFlagListView,
	descriptionTextBox,
	
	reportForm,
	
	classesTableLayout,
	--=-=-=-=-=-=-=-=-=-=-=-=-= preset colours =-=-=-=-=-=-=-=-=-=-=-=-=--
	controlColour = (dotNetClass "System.Drawing.Color").Gainsboro,
	controlColour_dark2 = (dotNetClass"System.Drawing.SystemColors").ControlDarkDark,
	controlColour_dark1 = (dotNetClass"System.Drawing.Color").Silver,
	
	warningColour = (dotNetClass"System.Drawing.Color").fromARGB 226 167 98,
	errorColour = (dotNetClass"System.Drawing.Color").fromARGB 226 98 98,
	
	mouseOverColour = (dotNetClass"System.Drawing.Color").fromARGB 223 193 159,
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= dotNet UI presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	dockStyle = dotNetClass "System.Windows.Forms.DockStyle",
	formBorderStyle = dotNetClass "System.Windows.Forms.FormBorderStyle",
	columnStyle = dotNetClass "System.Windows.Forms.ColumnStyle",
	FlatStyle = dotNetClass "System.Windows.Forms.FlatStyle",
	sizeType = dotNetClass "System.Windows.Forms.SizeType",
	FontLarge = dotNetObject "System.Drawing.Font" "Trebuchet MS" 12 (dotNetClass "System.Drawing.FontStyle").bold,
	FontMedium = dotNetObject "System.Drawing.Font" "Trebuchet MS" 8.0 (dotNetClass "System.Drawing.FontStyle").bold,
	FontStandard = dotNetObject "System.Drawing.Font" "Small Fonts" 8.0 (dotNetClass "System.Drawing.FontStyle").Regular,
	FontSmall = dotNetObject "System.Drawing.Font" "Small Fonts" 6.0 (dotNetClass "System.Drawing.FontStyle").Regular,	--Regular --Italic
	listViewFont = dotNetObject "System.Drawing.Font" "Small Fonts" 8.0 (dotNetClass "System.Drawing.FontStyle").bold,
	decimalConverterClass = dotNetClass "RGLExportHelpers.DecimalConverter",
	
	fn readLocalConfigINI =
	(
		local INIFile = getFiles localConfigINI
		
		if INIFile.count > 0 then
		(
			local formPosition = readValue ((getINISetting localConfigINI "Main" "Position") as stringStream)
			
			ObjectTypeOpsForm.location.x = formPosition.x
			ObjectTypeOpsForm.location.y = formPosition.y
		)
	),
	
	fn writeLocalConfigINI =
	(
		local INIFile = getFiles localConfigINI
		
		if INIFile.count == 0 then
		(
			local newFile = createFile localConfigINI
			close newFile
		)
		
		setINISetting localConfigINI "Main" "Position" ([ObjectTypeOpsForm.location.x,ObjectTypeOpsForm.location.y] as string)
	),
	
	fn readATTFile =
	(
		local dataStream = openFile rsNorthAttFile
		
		skipToString dataStream "list 333 \""
		
		local line = (readDelimitedString dataStream "\"")
		
		GTAObjectAttributeList = filterString line ","
		
		close dataStream
	),
	
	fn initListView lv attributeDefinition header:undefined = -- initialises the dotNet list view and sets up the layout based on the items in the attributeDefinition array
	(
		lv.columns.Clear()
		
		if header == undefined then
		(
			lv.HeaderStyle = (dotNetClass "System.Windows.Forms.ColumnHeaderStyle").Nonclickable --Nonclickable
		)
		else
		(
			lv.HeaderStyle = header
		)
		
		lv.view = (dotNetClass "System.Windows.Forms.View").Details
		lv.fullRowSelect = true
		lv.HideSelection = false
		
		for definition in attributeDefinition do
		(
			lv.columns.add definition -1
		)
	),
	
	fn populateListView lv array = -- populates the dotNet list view using items from the array
	(
		lv.Items.Clear()
		
		local listViewRange = #()
		
		for i = 1 to array[1].count do
		(
			local name = toUpper array[1][i]
			local value = toUpper array[2][i]
			
			if name != (toUpper "description") then
			(
				local listItem = dotNetObject "System.Windows.Forms.ListViewItem" name
				listItem.Tag = dotNetMXSValue array[i]
				
				local subItem = listItem.SubItems.add value
				subItem.Tag = dotNetMXSValue parent
				
				append listViewRange listItem
			)
		)
		
		lv.columns.Item[0].width = (lv.width / 100.0 * 55) as integer
		lv.columns.Item[1].width = (lv.width / 100.0 * 45) as integer
		
		lv.Items.AddRange listViewRange
	),
	
	fn populateCurrentObjectTypeListView array = -- populates the dotNet list view using items from the array
	(
		currentObjectTypeListView.Items.Clear()
		
		local listViewRange = #()
		
		for i = 1 to array.count do
		(
			local name = toUpper array[i]
			
			local listItem = dotNetObject "System.Windows.Forms.ListViewItem" name
-- 			listItem.Tag = dotNetMXSValue array[i]
			
			append listViewRange listItem
		)
		
		currentObjectTypeListView.columns.Item[0].width = (currentObjectTypeListView.width / 100.0 * 55) as integer
		
		currentObjectTypeListView.Items.AddRange listViewRange
	),
	
	fn validateObject object flags values &reportStream =
	(
		local result = true
		
		for i = 1 to flags.count do
		(
			local flag = flags[i]
			local value = values[i]
			
			case flag of
			(
-- 				default: print flag
				"Is Fragment":
				(
					if value == "true" and object.parent != undefined then
					(
						result = "    <It is a child of " + object.parent.name + ">\n"
					)
				)
			)
		)
		
		result
	),
	
	mapped fn assignFlags object objectType &reportStream =
	(
		local flags = objectType.flagArray[1]
		local values = objectType.flagArray[2]
		
		local success = validateObject object flags values reportStream
		
		if success == true then
		(
			for i = 1 to flags.count do
			(
				local flag = flags[i]
				
				if flag != "description" then
				(
					local index = getAttrIndex objectType.class flag
					local value = values[i]
					
					if value == "true" or value == "false" then
					(
						value = (readvalue (stringStream values[i]))
					)
					
					if flags[i] == "Attribute" then
					(
						value = (findItem GTAObjectAttributeList value) - 1
					)
					
					setAttr object index value
				)
			)
			
-- 			format "% has been set to %\n" object.name objectType.name to:reportStream
		)
		else
		(
			format "% cannot be set to % because...\n%\n \n" object.name objectType.name success to:reportStream
		)
	),
	
	fn checkClass array class =
	(
		local result = true
		
		for item in array do
		(
			if (getAttrClass item) != class then
			(
				result = false
			)
		)
		
		result
	),
	
	fn getValueFromInt name value =
	(
		local result
		
		if name == "Attribute" then
		(
			result = GTAObjectAttributeList[value + 1]
		)
		
		result
	),

	fn getObjectType object class =
	(
		local result = #()
		
		local knownType = true
		
		for item in objectTypeArray where (item.class == class)  do
		(
			local knownType = true
			
			for i = 1 to item.flagArray[1].count do
			(
				local flag = item.flagArray[1][i]
				local value = item.flagArray[2][i]
				
				if (flag != "description") then
				(
					local index = getAttrIndex class flag
					local currentValue = getAttr object index
					local attributeType = (GetAttrType class index)
					
					if attributeType == "int" then
					(
						currentValue = getValueFromInt flag currentValue
					)
					
					if (currentValue as string) != (value as string) then
					(
						knownType = false
					)
				)
			)
			
			if knownType then
			(
				local index = findItem result item.name
				
				if index == 0 then
				(
					append result item.name
				)
			)
		)
		
		result
	),
	
	fn cb_ObjectTypeOpsSelectionChanged =
	(
		try (init_RSL_CollisionOps.changeForm.Close()) catch()
		try (init_RSL_CollisionOps.convertToRexForm.Close()) catch()
		try (init_RSL_CollisionOps.selectAttributeForm.Close()) catch()
		
		if (selection.count == 1) then
		(
			local knownClass = false
			theObject = selection[1]
			currentObjectNameLabel.text = toUpper ("  SELECTION: " + theObject.name)
			
			local attrClass = getAttrClass theObject
			
			for item in objectTypeArray do
			(
				if item.class == attrClass then
				(
					knownClass = true
				)
			)
			
			if knownClass then
			(
				local types = getObjectType theObject attrClass
				
				if types.count != 0 then
				(
					populateCurrentObjectTypeListView types
				)
				else
				(
					currentObjectTypeListView.Items.Clear()
				)
			)
			else
			(
				currentObjectTypeListView.Items.Clear()
			)
		)
		else
		(
			currentObjectTypeListView.Items.Clear()
			
			if selection.count == 0 then
			(
				currentObjectNameLabel.text = "  SELECTION: NONE"
			)
			else
			(
				currentObjectNameLabel.text = "  SELECTION: " + selection.count as string + " OBJECTS"
				
				local types = #()
				
				for object in selection do
				(
					local attrClass = getAttrClass object
					objectTypes = getObjectType object attrClass
					
					for item in objectTypes do
					(
						local index = findItem types item
						
						if index == 0 then
						(
							append types item
						)
					)
					
				)
				
				populateCurrentObjectTypeListView types
			)
		)
	),	

	mapped fn gatherByType object type &objectArray =
	(
		local objectClass = getAttrClass object
		
		if objectClass == type.class then
		(
			local isType = true
			
			for i = 1 to type.flagArray[1].count do
			(
				local flag = type.flagArray[1][i]
				local value = type.flagArray[2][i]
				
				if (flag != "description") then
				(
					local index = getAttrIndex objectClass flag
					local currentValue = getAttr object index
					local attributeType = (GetAttrType objectClass index)
					
					if attributeType == "int" then
					(
						currentValue = getValueFromInt flag currentValue
					)
					
					if (currentValue as string) != (value as string) then
					(
						isType = false
					)
				)
			)
			
			if isType then
			(
				append objectArray object
			)
		)
	),
	
	mapped fn mappedSelect object bool =
	(
		object.isSelected = bool
	),
	
	
	
	
	
	
	
	
	
	fn dispatchOnReportFormClosing s e =
	(
		init_RSL_ObjectTypeOps.reportFormClosing s e
	),
	
	fn reportFormClosing s e =
	(
		ObjectTypeOpsForm.enabled = true
	),
	
	fn dispatchOnReportCloseButtonClicked s e =
	(
		init_RSL_ObjectTypeOps.reportCloseButtonClicked s e
	),
	
	fn reportCloseButtonClicked s e =
	(
		reportForm.Close()
	),
	
	fn creatReportForm reportStream =
	(
		ObjectTypeOpsForm.enabled = false
		
		reportForm = dotNetObject "MaxCustomControls.MaxForm"
		reportForm.Size = dotNetObject "System.Drawing.Size" 512 512
		reportForm.MinimumSize = dotNetObject "System.Drawing.Size" 512 128
		reportForm.FormBorderStyle = formBorderStyle.SizableToolWindow --FixedToolWindow --SizableToolWindow
		reportForm.Name = "reportForm"
		reportForm.Text = (" Complete...")
		reportForm.ShowInTaskbar = False
		reportForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		
		local xLoc = (ObjectTypeOpsForm.location.x - reportForm.width)
		
		if xLoc < 0 then
		(
			xLoc = (ObjectTypeOpsForm.location.x + ObjectTypeOpsForm.width)
		)
		
		local yLoc = ObjectTypeOpsForm.location.y
		
		if yLoc < 0 then
		(
			yLoc = 0
		)
		
		reportForm.location = dotNetObject "System.Drawing.Point" xLoc yLoc
		
		dotnet.addEventHandler reportForm "FormClosing" dispatchOnReportFormClosing
		
		reportTableLayout = dotNetObject "TableLayoutPanel"
		reportTableLayout.ColumnCount = 1
		reportTableLayout.RowCount = 3
		reportTableLayout.Dock = dockStyle.Fill
		reportTableLayout.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		reportTableLayout.CellBorderStyle = (dotNetClass "TableLayoutPanelCellBorderStyle").None  --Single
		reportTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 24)
		reportTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.percent 100)
		reportTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 24)
		reportTableLayout.AutoSize = true
		
		reportLabel = dotNetObject "Label"
		reportLabel.name = "reportLabel"
		reportLabel.text = toUpper " report: "
		reportLabel.autoSize = true
		reportLabel.Dock = dockStyle.Fill
		reportLabel.BorderStyle = (dotNetClass "System.Windows.Forms.BorderStyle").None
		reportLabel.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		reportLabel.font = FontSmall
		reportLabel.TextAlign = (DotNetClass "System.Drawing.ContentAlignment").MiddleLeft
		reportLabel.AutoEllipsis = true
		
		reportTextBox = dotNetObject "RichTextBox"
		reportTextBox.name = "reportLabel"
		reportTextBox.autoSize = true
		reportTextBox.Dock = dockStyle.Fill
		reportTextBox.BackColor = controlColour_dark1
		reportTextBox.BorderStyle = (dotNetClass "System.Windows.Forms.BorderStyle").FixedSingle
		reportTextBox.font = FontSmall
		reportTextBox.Multiline = true
		reportTextBox.ReadOnly = true
		reportTextBox.ScrollBars = (dotNetClass "System.Windows.Forms.RichTextBoxScrollBars").Both
		reportTextBox.MaxLength = 999999999
		
		local stringArray = filterString (toUpper (reportStream as string)) "\n"
		
		if stringArray.count == 0 then
		(
			stringArray = #((toUpper "No errors found"))
			
			reportForm.Height = 128
		)
		
		reportTextBox.Lines =stringArray
		
		reportCloseButton = dotNetObject "Button"
		reportCloseButton.name = "reportCloseButton"
		reportCloseButton.Font = FontSmall
		reportCloseButton.Text = toUpper "Close"
		reportCloseButton.BackColor = controlColour
		reportCloseButton.FlatAppearance.MouseOverBackColor = mouseOverColour
		reportCloseButton.FlatAppearance.CheckedBackColor = warningColour
		reportCloseButton.Dock = dockStyle.fill
		reportCloseButton.Margin = dotNetObject "System.Windows.Forms.Padding" 1
		reportCloseButton.FlatStyle = (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		reportCloseButton.AutoSize = true
		reportCloseButton.AutoSizeMode = (dotNetClass "System.Windows.Forms.AutoSizeMode").GrowAndShrink
		reportCloseButton.Tag = dotNetMXSValue objectStruct
		
		dotnet.addEventHandler reportCloseButton "Click" dispatchOnReportCloseButtonClicked
		
		reportTableLayout.Controls.Add reportLabel 0 0
		reportTableLayout.Controls.Add reportTextBox 0 1
		reportTableLayout.Controls.Add reportCloseButton 0 2
		
		reportForm.Controls.Add reportTableLayout
		
		reportForm.showModeless()
	),
	
	fn dispatchOnTypeControlClicked s e =
	(
		init_RSL_ObjectTypeOps.typeControlClicked s e
	),
	
	fn typeControlClicked s e =
	(
		local theSelection = selection as array
		local theObjectType = s.tag.value
		
		local okToContinue = queryBox ("This will set all objects in the selection to " + theObjectType.name + ". Are you sure?") title:"Warning..."
		
		if okToContinue then
		(
			if theSelection.count > 0 then
			(
				if (checkClass theSelection theObjectType.class) then
				(
					local report = stringStream ""
					
					assignFlags theSelection theObjectType report
					
					cb_ObjectTypeOpsSelectionChanged()
					
					creatReportForm report
				)
				else
				(
					messageBox ("One or more of the selected objects is not a " + theObjectType.class) title:"Error..."
				)
			)
			else
			(
				messageBox "No objects selected" title:"Error..."
			)
		)
	),
	
	fn dispatchOnTypeSelectControlClicked s e =
	(
		init_RSL_ObjectTypeOps.typeSelectControlClicked s e
	),
	
	fn typeSelectControlClicked s e =
	(
		local type = s.Tag.Value
		local objectArray = #()
		
		gatherByType objects type &objectArray
		
		deselect objects
		select objectArray
	),
	
	fn dispatchOnTypeControlEntered s e =
	(
		init_RSL_ObjectTypeOps.typeControlEntered s e
	),
	
	fn typeControlEntered s e =
	(
		typeFlagLabel.Text = toUpper (" Type: " + s.Text)
		populateListView typeFlagListView s.Tag.Value.FlagArray
		
		local index = findItem s.Tag.Value.FlagArray[1] "description"
		
		descriptionTextBox.Text = toUpper s.Tag.Value.FlagArray[2][index] 
	),
	
	fn loadTypes =
	(
		if classesTableLayout != undefined then
		(
			mainTableLayout.Controls.Remove classesTableLayout
			
			ObjectTypeOpsForm.height = (82 + 64 + 176)
		)
		
		xmlStruct = XmlDocument()
		xmlStruct.init()
		xmlStruct.load typeXML		
		
		local childNodes = xmlStruct.document.DocumentElement.ChildNodes
		local classTypes = for i = 0 to childNodes.count - 1 collect childNodes.item[i]
		local rowCount = 0
		local labelRow = 0
		
		classesTableLayout = dotNetObject "TableLayoutPanel"
		classesTableLayout.ColumnCount = 1
		classesTableLayout.RowCount = 2
		classesTableLayout.Dock = dockStyle.Fill
		classesTableLayout.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		classesTableLayout.CellBorderStyle = (dotNetClass "TableLayoutPanelCellBorderStyle").None  --Single
		classesTableLayout.AutoSize = true
		classesTableLayout.AutoSizeMode = (dotNetClass "System.Windows.Forms.AutoSizeMode").GrowAndShrink
		
		for i = 1 to classTypes.count where (classTypes[i].name) != "#comment" do
		(
			ObjectTypeOpsForm.height += 24
			
			local classType = classTypes[i]
			local types = for c = 0 to classType.ChildNodes.count - 1 collect classType.ChildNodes.item[c]
			
			classesTableLayout.RowCount += 1
			classesTableLayout.RowCount += types.count
			classesTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 24)
			classesTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute (types.count * 24))
			
			classLabel = dotNetObject "Label"
			classLabel.name = "classLabel"
			classLabel.text = toUpper (" " + (substituteString classType.name  "_" " "))
			classLabel.autoSize = true
			classLabel.Dock = dockStyle.Fill
			classLabel.BorderStyle = (dotNetClass "System.Windows.Forms.BorderStyle").None
			classLabel.Margin = dotNetObject "System.Windows.Forms.Padding" 0
			classLabel.font = FontSmall
			classLabel.TextAlign = (DotNetClass "System.Drawing.ContentAlignment").MiddleLeft
			
			local objectTypesTableLayout = dotNetObject "TableLayoutPanel"
			objectTypesTableLayout.ColumnCount = 2
			objectTypesTableLayout.RowCount = 2
			objectTypesTableLayout.Dock = dockStyle.Fill
			objectTypesTableLayout.Margin = dotNetObject "System.Windows.Forms.Padding" 0
			objectTypesTableLayout.CellBorderStyle = (dotNetClass "TableLayoutPanelCellBorderStyle").None  --Single
			objectTypesTableLayout.AutoSize = true
			objectTypesTableLayout.AutoSizeMode = (dotNetClass "System.Windows.Forms.AutoSizeMode").GrowAndShrink
			objectTypesTableLayout.ColumnStyles.add (dotNetObject "System.Windows.Forms.ColumnStyle" sizeType.percent 86)
			objectTypesTableLayout.ColumnStyles.add (dotNetObject "System.Windows.Forms.ColumnStyle" sizeType.percent 14)
			
			classesTableLayout.Controls.Add classLabel 0 labelRow
			classesTableLayout.Controls.Add objectTypesTableLayout 0 (labelRow + 1)
			
			rowCount += types.count
			labelRow += 2
			
			for c = 1 to types.count do
			(
				ObjectTypeOpsForm.height += 24
				objectTypesTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 24)
				
				local type = types[c]
				local flags = #(#(),#())
				
				for a = 0 to type.Attributes.Count - 1 do
				(
					local attribute = type.Attributes.item[a]
					
					append flags[1] (substituteString attribute.name "_" " ")
					append flags[2] attribute.value
				)
				
				local objectStruct = RSL_ObjectType name:(substituteString type.name "_" " ") class:(substituteString classType.Attributes.item[0].value "_" " ")
				objectStruct.flagArray = flags
				
				append objectTypeArray objectStruct
				
				typeControl = dotNetObject "Button"
				typeControl.name = "typeControl"
				typeControl.Font = FontSmall
				typeControl.Text = (toUpper objectStruct.name)
				typeControl.BackColor = controlColour
				typeControl.FlatAppearance.MouseOverBackColor = mouseOverColour
				typeControl.FlatAppearance.CheckedBackColor = warningColour
				typeControl.Dock = dockStyle.fill
				typeControl.Margin = dotNetObject "System.Windows.Forms.Padding" 1
				typeControl.FlatStyle = (dotNetClass "System.Windows.Forms.FlatStyle").Flat
				typeControl.AutoSize = true
				typeControl.AutoSizeMode = (dotNetClass "System.Windows.Forms.AutoSizeMode").GrowAndShrink
				typeControl.Tag = dotNetMXSValue objectStruct
				
				dotnet.addEventHandler typeControl "Click" dispatchOnTypeControlClicked
				dotnet.addEventHandler typeControl "MouseEnter" dispatchOnTypeControlEntered
				
				typeSelectControl = dotNetObject "Button"
				typeSelectControl.name = "typeSelectControl"
				typeSelectControl.Font = FontSmall
				typeSelectControl.Text = "+"
				typeSelectControl.BackColor = controlColour
				typeSelectControl.FlatAppearance.MouseOverBackColor = mouseOverColour
				typeSelectControl.FlatAppearance.CheckedBackColor = warningColour
				typeSelectControl.Dock = dockStyle.fill
				typeSelectControl.Margin = dotNetObject "System.Windows.Forms.Padding" 1
				typeSelectControl.FlatStyle = (dotNetClass "System.Windows.Forms.FlatStyle").Flat
				typeSelectControl.AutoSize = true
				typeSelectControl.AutoSizeMode = (dotNetClass "System.Windows.Forms.AutoSizeMode").GrowAndShrink
				typeSelectControl.Tag = dotNetMXSValue objectStruct
				
				dotnet.addEventHandler typeSelectControl "Click" dispatchOnTypeSelectControlClicked
				
				objectTypesTableLayout.Controls.Add typeControl 0 (c - 1)
				objectTypesTableLayout.Controls.Add typeSelectControl 1 (c - 1)
			)
		)
		
		mainTableLayout.Controls.Add classesTableLayout 0 3
		
		mainTableLayout.RowStyles.Item[3].height = (classTypes.Count * 24) + (rowCount * 24)
	),
	
	fn dispatchOnFormClosing s e =
	(
		init_RSL_ObjectTypeOps.onFormClosing s e
	),
	
	fn onFormClosing s e =
	(
		try (init_RSL_ObjectTypeOps.reportForm.Close()) catch()
		
		callbacks.removeScripts id:#RSL_ObjectTypeOps_selectionCallBacks
		
		writeLocalConfigINI()
	),
	
	fn dispatchOnHelpButtonClicked s e =
	(
		init_RSL_ObjectTypeOps.helpButtonClicked s e
	),
	
	fn helpButtonClicked s e =
	(
		shellLaunch helpLink ""
	),
	
	fn dispatchOnEditXmlButtonClicked s e =
	(
		init_RSL_ObjectTypeOps.editXmlButtonClicked s e
	),
	
	fn editXmlButtonClicked s e =
	(
		local isReadOnly = getFileAttribute typeXML #readOnly
		
		if not isReadOnly then
		(
			shellLaunch XmlEditor ""
		)
		else
		(
			messageBox (typeXML + " is read only. Check it out and try again.") title:"Error..."
		)
	),
	
	fn dispatchOnUpdateButtonClicked s e =
	(
		init_RSL_ObjectTypeOps.updateButtonClicked s e
	),
	
	fn updateButtonClicked s e =
	(
		loadTypes()
	),
	
	fn createForm = 
	(
		clearListener()
		
		ObjectTypeOpsForm = dotNetObject "MaxCustomControls.MaxForm"
		ObjectTypeOpsForm.Size = dotNetObject "System.Drawing.Size" 212 (82 + 64 + 176)
		ObjectTypeOpsForm.MinimumSize = dotNetObject "System.Drawing.Size" 212 26
		ObjectTypeOpsForm.FormBorderStyle = formBorderStyle.FixedToolWindow --FixedToolWindow --SizableToolWindow
		ObjectTypeOpsForm.Name = "ObjectTypeOpsForm"
		ObjectTypeOpsForm.Text = (" R* Object Type Operations")
		ObjectTypeOpsForm.ShowInTaskbar = False
		ObjectTypeOpsForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		
		dotnet.addEventHandler ObjectTypeOpsForm "FormClosing" dispatchOnFormClosing
		
		mainTableLayout = dotNetObject "TableLayoutPanel"
		mainTableLayout.ColumnCount = 1
		mainTableLayout.RowCount = 5
		mainTableLayout.Dock = dockStyle.Fill
		mainTableLayout.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		mainTableLayout.CellBorderStyle = (dotNetClass "TableLayoutPanelCellBorderStyle").None  --Single
		mainTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 24)
		mainTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 64)
		mainTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 24)
		mainTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 24)
		mainTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 176)
		mainTableLayout.AutoSize = true
		
		helpButton = dotNetObject "Button"
		helpButton.name = "helpButton"
		helpButton.Font = FontSmall
		helpButton.Text = toUpper "Help"
		helpButton.BackColor = controlColour
		helpButton.FlatAppearance.MouseOverBackColor = mouseOverColour
		helpButton.FlatAppearance.CheckedBackColor = warningColour
		helpButton.Dock = dockStyle.fill
		helpButton.Margin = dotNetObject "System.Windows.Forms.Padding" 1
		helpButton.FlatStyle = (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		helpButton.AutoSize = true
		helpButton.AutoSizeMode = (dotNetClass "System.Windows.Forms.AutoSizeMode").GrowAndShrink
		helpButton.Tag = dotNetMXSValue objectStruct
		
		dotnet.addEventHandler helpButton "Click" dispatchOnHelpButtonClicked
		
		XmlEditUpdateTableLayout = dotNetObject "TableLayoutPanel"
		XmlEditUpdateTableLayout.ColumnCount = 2
		XmlEditUpdateTableLayout.RowCount = 1
		XmlEditUpdateTableLayout.Dock = dockStyle.Fill
		XmlEditUpdateTableLayout.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		XmlEditUpdateTableLayout.CellBorderStyle = (dotNetClass "TableLayoutPanelCellBorderStyle").None  --Single
		XmlEditUpdateTableLayout.ColumnStyles.add (dotNetObject "System.Windows.Forms.ColumnStyle" sizeType.percent 50)
		XmlEditUpdateTableLayout.ColumnStyles.add (dotNetObject "System.Windows.Forms.ColumnStyle" sizeType.percent 50)
		XmlEditUpdateTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 24)
		XmlEditUpdateTableLayout.AutoSize = true
		
		editXmlButton = dotNetObject "Button"
		editXmlButton.name = "editXmlButton"
		editXmlButton.Font = FontSmall
		editXmlButton.Text = toUpper "Edit Types"
		editXmlButton.BackColor = controlColour
		editXmlButton.FlatAppearance.MouseOverBackColor = mouseOverColour
		editXmlButton.FlatAppearance.CheckedBackColor = warningColour
		editXmlButton.Dock = dockStyle.fill
		editXmlButton.Margin = dotNetObject "System.Windows.Forms.Padding" 1
		editXmlButton.FlatStyle = (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		editXmlButton.AutoSize = true
		editXmlButton.AutoSizeMode = (dotNetClass "System.Windows.Forms.AutoSizeMode").GrowAndShrink
		editXmlButton.Tag = dotNetMXSValue objectStruct
		
		dotnet.addEventHandler editXmlButton "Click" dispatchOnEditXmlButtonClicked
		
		updateButton = dotNetObject "Button"
		updateButton.name = "updateButton"
		updateButton.Font = FontSmall
		updateButton.Text = toUpper "Update"
		updateButton.BackColor = controlColour
		updateButton.FlatAppearance.MouseOverBackColor = mouseOverColour
		updateButton.FlatAppearance.CheckedBackColor = warningColour
		updateButton.Dock = dockStyle.fill
		updateButton.Margin = dotNetObject "System.Windows.Forms.Padding" 1
		updateButton.FlatStyle = (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		updateButton.AutoSize = true
		updateButton.AutoSizeMode = (dotNetClass "System.Windows.Forms.AutoSizeMode").GrowAndShrink
		updateButton.Tag = dotNetMXSValue objectStruct
		
		dotnet.addEventHandler updateButton "Click" dispatchOnUpdateButtonClicked
		
		XmlEditUpdateTableLayout.Controls.Add editXmlButton 0 0
		XmlEditUpdateTableLayout.Controls.Add updateButton 1 0
		
		currentObjectTableLayout = dotNetObject "TableLayoutPanel"
		currentObjectTableLayout.ColumnCount = 1
		currentObjectTableLayout.RowCount = 2
		currentObjectTableLayout.Dock = dockStyle.Fill
		currentObjectTableLayout.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		currentObjectTableLayout.CellBorderStyle = (dotNetClass "TableLayoutPanelCellBorderStyle").None  --Single
		currentObjectTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 16)
		currentObjectTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 48)
		currentObjectTableLayout.AutoSize = true
		
		currentObjectNameLabel = dotNetObject "Label"
		currentObjectNameLabel.name = "currentObjectNameLabel"
		currentObjectNameLabel.text = toUpper " Selection: "
-- 		currentObjectNameLabel.autoSize = true
		currentObjectNameLabel.Dock = dockStyle.Fill
		currentObjectNameLabel.BorderStyle = (dotNetClass "System.Windows.Forms.BorderStyle").None
		currentObjectNameLabel.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		currentObjectNameLabel.font = FontSmall
		currentObjectNameLabel.TextAlign = (DotNetClass "System.Drawing.ContentAlignment").MiddleLeft
		currentObjectNameLabel.AutoEllipsis = true
		
		currentObjectTypeListView = dotNetObject "ListView"
		currentObjectTypeListView.Dock = dockStyle.Fill
		currentObjectTypeListView.BackColor = controlColour_dark1
		currentObjectTypeListView.font = FontSmall
		currentObjectTypeListView.Sorting = (dotNetClass "System.Windows.Forms.SortOrder").Ascending
		currentObjectTypeListView.BorderStyle = (dotNetClass "System.Windows.Forms.BorderStyle").FixedSingle
		currentObjectTypeListView.Sorting = (dotNetClass "System.Windows.Forms.SortOrder").None
		
		initListView currentObjectTypeListView typeDefinition header:((dotNetClass "System.Windows.Forms.ColumnHeaderStyle").None)
		
		currentObjectTableLayout.Controls.Add currentObjectNameLabel 0 0
		currentObjectTableLayout.Controls.Add currentObjectTypeListView 0 1
		
		typeFlagsTableLayout = dotNetObject "TableLayoutPanel"
		typeFlagsTableLayout.ColumnCount = 1
		typeFlagsTableLayout.RowCount = 4
		typeFlagsTableLayout.Dock = dockStyle.Fill
		typeFlagsTableLayout.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		typeFlagsTableLayout.CellBorderStyle = (dotNetClass "TableLayoutPanelCellBorderStyle").None  --Single
		typeFlagsTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 24)
		typeFlagsTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 64)
		typeFlagsTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 24)
		typeFlagsTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 64)
		typeFlagsTableLayout.AutoSize = true
		
		typeFlagLabel = dotNetObject "Label"
		typeFlagLabel.name = "typeFlagLabel"
		typeFlagLabel.text = toUpper " Type: "
		typeFlagLabel.autoSize = true
		typeFlagLabel.Dock = dockStyle.Fill
		typeFlagLabel.BorderStyle = (dotNetClass "System.Windows.Forms.BorderStyle").None
		typeFlagLabel.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		typeFlagLabel.font = FontSmall
		typeFlagLabel.TextAlign = (DotNetClass "System.Drawing.ContentAlignment").MiddleLeft
		typeFlagLabel.AutoEllipsis = true
		
		typeFlagListView = dotNetObject "ListView"
		typeFlagListView.Dock = dockStyle.Fill
		typeFlagListView.BackColor = controlColour_dark1
		typeFlagListView.font = FontSmall
		typeFlagListView.Sorting = (dotNetClass "System.Windows.Forms.SortOrder").Ascending
		typeFlagListView.BorderStyle = (dotNetClass "System.Windows.Forms.BorderStyle").FixedSingle
		
		descriptionLabel = dotNetObject "Label"
		descriptionLabel.name = "descriptionLabel"
		descriptionLabel.text = toUpper " Description: "
		descriptionLabel.autoSize = true
		descriptionLabel.Dock = dockStyle.Fill
		descriptionLabel.BorderStyle = (dotNetClass "System.Windows.Forms.BorderStyle").None
		descriptionLabel.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		descriptionLabel.font = FontSmall
		descriptionLabel.TextAlign = (DotNetClass "System.Drawing.ContentAlignment").MiddleLeft
		descriptionLabel.AutoEllipsis = true
		
		descriptionTextBox = dotNetObject "TextBox"
		descriptionTextBox.name = "descriptionLabel"
		descriptionTextBox.autoSize = true
		descriptionTextBox.Dock = dockStyle.Fill
		descriptionTextBox.BackColor = controlColour_dark1
		descriptionTextBox.BorderStyle = (dotNetClass "System.Windows.Forms.BorderStyle").FixedSingle
		descriptionTextBox.font = FontSmall
		descriptionTextBox.Multiline = true
		descriptionTextBox.ReadOnly = true
		
		typeFlagsTableLayout.Controls.Add typeFlagLabel 0 0
		typeFlagsTableLayout.Controls.Add typeFlagListView 0 1
		typeFlagsTableLayout.Controls.Add descriptionLabel 0 2
		typeFlagsTableLayout.Controls.Add descriptionTextBox 0 3
		
		initListView typeFlagListView flagDefinition header:((dotNetClass "System.Windows.Forms.ColumnHeaderStyle").None)
		
		loadTypes()
		
		mainTableLayout.Controls.Add helpButton 0 0
		mainTableLayout.Controls.Add currentObjectTableLayout 0 1
		mainTableLayout.Controls.Add XmlEditUpdateTableLayout 0 2
		mainTableLayout.Controls.Add typeFlagsTableLayout 0 4
		
		ObjectTypeOpsForm.Controls.Add mainTableLayout
		
		readLocalConfigINI()
		readATTFile()
		
		cb_ObjectTypeOpsSelectionChanged()
		callbacks.addScript #selectionSetChanged "init_RSL_ObjectTypeOps.cb_ObjectTypeOpsSelectionChanged()" id:#RSL_ObjectTypeOps_selectionCallBacks
		
		ObjectTypeOpsForm.ShowModeless()
		
		ObjectTypeOpsForm
	)
)

if ObjectTypeOps_Form != undefined then
(
	ObjectTypeOps_Form.Close()
	try (init_RSL_ObjectTypeOps.reportForm.Close()) catch()
)

init_RSL_ObjectTypeOps = RSL_ObjectTypeOps()

ObjectTypeOps_Form = init_RSL_ObjectTypeOps.createForm()