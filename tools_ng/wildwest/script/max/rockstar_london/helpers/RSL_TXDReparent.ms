
-- perforce integration script
filein "pipeline/util/p4.ms"

filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetClasses.ms")
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_dotNetUIOps.ms")
-- TXD Ops classes e.g. RSL_TextureObject
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSL_TXDOpsStructs.ms")
-- TXD Ops functions for View/Edit mode
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSL_TXDOpsFunctions.ms")

global RSL_Reparent

struct RSL_ParentOutput
(
	name,
	parent = "None",
	type,
	textureArray = #(),
	
	fn toString =
	(
		local result = "" as stringStream
		
		format "txd\nname = %\nparent = %\ntype = %\n" (toLower name) (toLower parent) type to:result
		
		if type == 3 then
		(
			format "textures = %\n" name TextureArray.count to:result
			
			for texture in TextureArray do
			(
				format "texture = %\n" texture.name to:result
			)
		)
		
		if type == 1 then
		(
			format "textures = 0\n" to:result
		)
		
		result as string
	)
)


struct RSL_TXDReparent
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- Struct variables
	---------------------------------------------------------------------------------------------------------------------------------------
	p4 = RsPerforce(),
	
	mapName = (getFilenameFile maxFilename),
	parentiseGlobalFile = (RsConfigGetContentDir() + "txdparents\\global.txt"), -- global parents text file
	TXDmapFile = RsConfigGetContentDir() + "txdparents/" + mapName + ".txt",
	TXDFuncs = RSL_TXDOpsFunctions(),
	
	parentTXDArray = #(),
	validParentTXDArray = #(),
	TXDArray = #(),
	
	outputArray = #(),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- UI variables
	---------------------------------------------------------------------------------------------------------------------------------------
	reparentForm,
	TXDListView,
	parentTree,
	validOnlyCheckBox,
	assignAllButton,
	assignSelectedButton,
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	fn perforceSych =
	(
		if (p4.connected()) then
		(
			p4.connect (project.intermediate)
			p4.sync (p4.local2depot (getFiles (RsConfigGetContentDir() + "txdparents/*.*")))
			
			if (doesFileExist TXDmapFile) then
			(
				p4.edit (p4.local2depot TXDmapFile)
			)
		)
		else
		(
			messageBox "You are not currently connected to perforce.\nNo files will be synched or checked out." title:"Warning..."
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- readParentiseGlobal() Reads the struct variable parentiseGlobalFile and creates parent TXD from the entries it finds.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn readParentiseGlobal =
	(
		local result = true
		
		if (doesFileExist parentiseGlobalFile) then
		(
			local dataStream = openFile parentiseGlobalFile
			
			while not (eof dataStream) do
			(
				local line = readLine dataStream
				
				if line == "txd" then
				(
					local name = (filterString (readLine dataStream) "= ")[2]
					
					-- if name is not "root", we've found a parent TXD
					if name != "root" then
					(
						skipToNextLine dataStream
						skipToNextLine dataStream
						
						-- get the texture count
						local count = (filterString (readLine dataStream) "= ")[2] as integer
						
						local textureArray = #()
						
						-- if there are textures, collect them as RSL_TXDTextures
						if count > 0 then
						(
							textureArray.count = count
							for n = 1 to count do
							(
								textureArray[n] = (RSL_TXDTexture name:(filterString (readLine dataStream) "= ")[2])
							)
						)
						
						local newParent = RSL_ParentTXD name:name textureArray:textureArray
						
						append parentTXDArray newParent
					)
				)
			)
			
			close dataStream
		)
		else
		(
			result = false
			messageBox ("Failed to find global.txt\n" + parentiseGlobalFile) title:"Error..."
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- readTXDMapFile() Reads the parsed file and returns both RSL_ParentTXDs and RSL_ChildTXDs based on the contents
	-- of the file. These files are map file specific and contain information on which TXDs are parented. They also contain
	-- local parent TXD information. Local parent TXDs are parent TXDs that are local to that particular map file.
	-- Returns an array of TXD that are linked to a parent TXD or an empty array if none are found.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn readTXDMapFile =
	(
		append outputArray (RSL_ParentOutput name:"root" parent:"none" type:1)
		
		if (doesFileExist TXDmapFile) then
		(
			local dataStream = openFile TXDmapFile
			
			while not (eof dataStream) do
			(
				local line = readLine dataStream
				
				if line == "txd" then
				(
					local name = (filterString (readLine dataStream) "= ")[2]
					
					if name != "root" then
					(
						local parent = (filterString (readLine dataStream) "= ")[2]
						
						-- if parent is "root", we've found a local parent TXD
						if parent == "root" then
						(
							skipToNextLine dataStream
							-- get the number of textures in the parent TXD
							local count = (filterString (readLine dataStream) "= ")[2] as integer
							
							local textureArray = #()
							
							if count > 0 then
							(
								-- collect the texture entries as RSL_TXDTextures
								textureArray.count = count
								for n = 1 to count do
								(
									textureArray[n] = (RSL_TXDTexture name:(filterString (readLine dataStream) "= ")[2])
								)
							)
							
							local newOutput = RSL_ParentOutput name:name parent:parent type:3 textureArray:textureArray
						)
						else
						(
							-- we've found a TXD that is linked to a parent TXD
							local newOutput = RSL_ParentOutput name:name parent:parent type:4
							
							append outputArray newOutput
						)
					)
				)
			)
			
			close dataStream
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- findByName()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn findByName array name =
	(
		local result = 0
		
		for i = 1 to array.count do 
		(
			if (stricmp array[i].name name) == 0 then
			(
				result = i
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- appendValidParent()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn appendValidParent parent TXDArray =
	(
		local isValid = true
		
		for TXD in TXDArray do 
		(
			local thisValid = false
			local textureArray = for t in TXD.textureArray collect t
			
			for array in TXD.uniqueTextureArray do 
			(
				textureArray += (for t in array collect t)
			)
			
			for texture in textureArray do 
			(
				if (findByName parent.textureArray texture.name) > 0 then
				(
					thisValid = true
					exit
				)
			)
			
			if not thisValid then
			(
				isValid = false
				exit
			)
		)
		
		if isValid then
		(
			append validParentTXDArray parent
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- findOutputEntry()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn findOutputEntry name =
	(
		local result
		
		for entry in outputArray do 
		(
			if (stricmp entry.name name) == 0 then
			(
				result = entry
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- assignParentTo()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn assignParentTo TXD parent =
	(
		local entry  = findOutputEntry TXD.name
		
		if entry != undefined then
		(
			entry.parent = parent
		)
		else
		(
			append outputArray (RSL_ParentOutput name:TXD.name parent:parent type:4)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- writeFile()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn writeFile =
	(
		if (doesFileExist TXDmapFile) then
		(
			setFileAttribute TXDmapFile #readOnly false
		)
		else
		(
			createfile TXDmapFile
		)
		
		local dataStream = openfile TXDmapFile mode:"w"
		
		for entry in outputArray do 
		(
			format "%" (entry.toString()) to:dataStream
		)
		
		close dataStream
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- addToTree()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn addToListView TXD =
	(
		local newItem = dotNetObject "ListViewItem" TXD.name
		local outputEntry = findOutputEntry TXD.name
		local parent = "None"
		
		if outputEntry != undefined then
		(
			parent = outputEntry.parent
		)
		
		newItem.subItems.add parent
		
		TXDListView.items.add newItem
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- addToTree()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn addToTree parent =
	(
		local parentNode = parentTree.nodes.add parent.name
		
		for texture in parent.textureArray do 
		(
			parentNode.nodes.add texture.name
		)
	),
	
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- UI FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOnFormClosing() Dispatches onFormClosing() Called when the user closes collision ops
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOnFormClosing s e =
	(
		RSL_Reparent.onFormClosing s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- onFormClosing() Called when collision ops is closed
	---------------------------------------------------------------------------------------------------------------------------------------
	fn onFormClosing s e =
	(
		
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- disableAccelerators() Called when the user enters any control that needs keyboard input e.g. a textBox
	---------------------------------------------------------------------------------------------------------------------------------------
	fn disableAccelerators s e =
	(
		enableAccelerators = false
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchTXDListIndexChanged()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchTXDListIndexChanged s e =
	(
		RSL_Reparent.TXDListIndexChanged s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- TXDListIndexChanged()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn TXDListIndexChanged s e =
	(
		assignSelectedButton.enabled = s.selectedItems.count > 0
		
		if validOnlyCheckBox.checked then
		(
			validParentTXDArray = #()
			parentTree.nodes.clear()
			
			if s.selectedItems.count > 0 then
			(
				local tempArray = for i = 0 to s.selectedIndices.count - 1 collect TXDArray[(s.selectedIndices.item[i] + 1)]
				
				appendValidParent parentTXDArray tempArray
				
				addToTree validParentTXDArray
				
				local count = parentTree.nodes.count - 1
				
				if count > 0 then
				(
					parentTree.nodes.item[count].expand()
					parentTree.nodes.item[count].collapse()
				)
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchParentTreeClick()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchParentTreeClick s e =
	(
		RSL_Reparent.parentTreeClick s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- parentTreeClick()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn parentTreeClick s e =
	(
		local hitNode = s.getNodeAt (RS_dotNetObject.pointObject [e.x, e.y])
		
		assignAllButton.enabled = hitNode != undefined
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchValidOnlyChecked()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchValidOnlyChecked s e =
	(
		RSL_Reparent.validOnlyChecked s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- validOnlyChecked()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn validOnlyChecked s e =
	(
		parentTree.nodes.clear()
		
		if s.checked then
		(
			validParentTXDArray = #()
			
			if TXDListView.selectedItems.count > 0 then
			(
				local tempArray = for i = 0 to TXDListView.selectedIndices.count - 1 collect TXDArray[(TXDListView.selectedIndices.item[i] + 1)]
				
				appendValidParent parentTXDArray tempArray
				
				addToTree validParentTXDArray
			)
		)
		else
		(
			
			addToTree parentTXDArray
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchAssignAllClick()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchAssignAllClick s e =
	(
		RSL_Reparent.assignAllClick s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- assignAllClick()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn assignAllClick s e =
	(
		if TXDListView.selectedIndices.count > 0 then
		(
			local parent = parentTree.SelectedNode.text
			
			local okToContinue = queryBox ("Are you sure you want to assign " + parent + " as the parent TXD for all TXDs in the scene?") title:"Warning..."
			
			if okToContinue then
			(
				assignParentTo TXDArray parent
				
				TXDListView.items.clear()
				addToListView TXDArray
				
				writeFile()
			)
		)
		else
		(
			messageBox "You must select at least one TXD from the list view." title:"Error..."
		)
	),
	
	
	fn dispatchAssignSelectedClick s e =
	(
		RSL_Reparent.assignSelectedClick s e
	),
	
	
	fn assignSelectedClick s e =
	(
		if parentTree.SelectedNode != undefined then
		(
			local parent = parentTree.SelectedNode.text
			
			local okToContinue = queryBox ("Are you sure you want to assign " + parent + " as the parent TXD for the selected TXDs?") title:"Warning..."
			
			if okToContinue then
			(
				local tempArray = for i = 0 to TXDListView.selectedIndices.count - 1 collect TXDArray[(TXDListView.selectedIndices.item[i] + 1)]
				
				assignParentTo tempArray parent
				
				TXDListView.items.clear()
				addToListView TXDArray
				
				writeFile()
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- createForm()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn createForm =
	(
		clearListener()
		RsMapSetupGlobals()
		
		if (queryBox "Do you want to synch to perforce?") then
		(
			perforceSych()
		)
		
		if (doesFileExist parentiseGlobalFile) and rsMapLevel != "" then
		(
			--Get the max handle pointer.
			local maxHandlePointer=(Windows.GetMAXHWND())
			
			--Convert the HWND handle of Max to a dotNet system pointer
			local sysPointer = DotNetObject "System.IntPtr" maxHandlePointer
			
			--Create a dotNet wrapper containing the maxHWND
			local maxHwnd = DotNetObject "MaxCustomControls.Win32HandleWrapper" sysPointer
			
			reparentForm = RS_dotNetUI.form "reparentForm" text:" R* TXD Re-parenter" size:[200, 512] max:[2048, 2048] min:[200, 256] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
			reparentForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
			dotnet.addEventHandler reparentForm "FormClosing" dispatchOnFormClosing
			dotnet.addEventHandler reparentForm "Enter" disableAccelerators
-- 			dotnet.addEventHandler reparentForm "Resize" dispatchOnFormResize
			
			TXDLabel = RS_dotNetUI.Label "TXDLabel" text:("TXDs:") textAlign:RS_dotNetPreset.CA_MiddleLeft borderStyle:RS_dotNetPreset.BS_None \
														dockStyle:RS_dotNetPreset.DS_Fill 	
			
			TXDListView = RS_dotNetUI.listView "attributeListView" dockStyle:RS_dotNetPreset.DS_Fill
			TXDListView.HeaderStyle = (dotNetClass "System.Windows.Forms.ColumnHeaderStyle").Nonclickable
			TXDListView.BackColor = (DotNetClass "System.Drawing.SystemColors").Control
			TXDListView.fullRowSelect = true
			TXDListView.multiSelect = true
			TXDListView.HideSelection = false
			TXDListView.columns.add "TXD"
			TXDListView.columns.add "Parent"
			dotnet.addEventHandler TXDListView "SelectedIndexChanged" dispatchTXDListIndexChanged
			
			parentLabel = RS_dotNetUI.Label "parentLabel" text:("Parent TXDs:") textAlign:RS_dotNetPreset.CA_MiddleLeft borderStyle:RS_dotNetPreset.BS_None \
														dockStyle:RS_dotNetPreset.DS_Fill 	
			
			parentTree = RS_dotNetUI.treeView "TXDTree" dockStyle:RS_dotNetPreset.DS_Fill
			parentTree.Sorted = false
			parentTree.FullRowSelect = true
			parentTree.LabelEdit = true
-- 			parentTree.ContextMenuStrip = TXDTreeMenu
			parentTree.AllowDrop = true
			parentTree.BackColor = (DotNetClass "System.Drawing.SystemColors").Control
			dotnet.addEventHandler parentTree "click" dispatchParentTreeClick
			
			validOnlyCheckBox = RS_dotNetUI.checkBox "validOnlyCheckBox" text:"Show Valid Parents Only" \
									textAlign:RS_dotNetPreset.CA_MiddleLeft checkAlign:RS_dotNetPreset.CA_MiddleLeft \
									dockStyle:RS_dotNetPreset.DS_Fill
			dotnet.addEventHandler validOnlyCheckBox "click" dispatchValidOnlyChecked
			
			assignAllButton = RS_dotNetUI.Button "assignAllButton" text:"Assign to All TXDs" dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 0)
			assignAllButton.enabled = false
			dotnet.addEventHandler assignAllButton "click" dispatchAssignAllClick
			
			assignSelectedButton = RS_dotNetUI.Button "assignSelectedButton" text:"Assign to Selected TXDs" dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 0)
			assignSelectedButton.enabled = false
			dotnet.addEventHandler assignSelectedButton "click" dispatchAssignSelectedClick
			
			mainTableLayout = RS_dotNetUI.tableLayout "mainTableLayout" text:"mainTableLayout" collumns:#((dataPair type:"Percent" value:50)) \
										rows:#((dataPair type:"Absolute" value:24),(dataPair type:"Percent" value:50),(dataPair type:"Absolute" value:24) \
										,(dataPair type:"Percent" value:50),(dataPair type:"Absolute" value:24),(dataPair type:"Absolute" value:32),(dataPair type:"Absolute" value:24)) dockStyle:RS_dotNetPreset.DS_Fill
			
			mainTableLayout.controls.add TXDLabel 0 0
			mainTableLayout.controls.add TXDListView 0 1
			mainTableLayout.controls.add parentLabel 0 2
			mainTableLayout.controls.add parentTree 0 3
			mainTableLayout.controls.add validOnlyCheckBox 0 4
			mainTableLayout.controls.add assignAllButton 0 5
			mainTableLayout.controls.add assignSelectedButton 0 6
			
			reparentForm.controls.add mainTableLayout
			
			reparentForm.Show (maxHwnd)
			
			readParentiseGlobal()
			readTXDMapFile()
			
			local textureObjectArray = #()
			local errorStringArray = #()
			
			TXDFuncs.createTextureObjects textureObjectArray errorStringArray
			
			local tempArray = #()
			TXDFuncs.createTXDs textureObjectArray &tempArray
			
			TXDArray = tempArray
			tempArray = undefined
			
			addToListView TXDArray
			addToTree parentTXDArray
			
			TXDListView.AutoResizeColumns (dotNetClass "ColumnHeaderAutoResizeStyle").HeaderSize
		)
		else
		(
			if not (doesFileExist parentiseGlobalFile) then
			(
				messageBox ("You do not have globals.txt. Do a get from perforce.\n" + parentiseGlobalFile) title:"Error..."
			)
		)
	)
)

if RSL_Reparent != undefined then
(
	try
	(
		RSL_Reparent.reparentForm.close()
	)
	catch()
)

RSL_Reparent = RSL_TXDReparent()
RSL_Reparent.createForm()