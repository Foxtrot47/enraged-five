



struct RSL_MMPainterColorAdjust
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- Struct variables
	---------------------------------------------------------------------------------------------------------------------------------------
	
	brightness = 0.0,
	contrast = 0.0,
	
	sliderChange = false,
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- UI variables
	---------------------------------------------------------------------------------------------------------------------------------------
	colourAdjustForm,
	brightnessBar,brightnessNumeric,contrastBar,contrastNumeric,
	okButton,cancelButton,
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- setBrightnessContrast()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn setBrightnessContrast mapVert object channelData =
	(
		local currentColor = meshop.getMapVert object channelData.channel mapVert
		local displayColour = [0,0,0]
		local b = (brightness / 100.0)
		local c = (contrast / 100.0) + 1
		
		if channelData.r then
		(
			displayColour.x = currentColor.x + b
			displayColour.x = ((displayColour.x - 0.5) * c + 0.5)
		)
		
		if channelData.g then
		(
			displayColour.y = currentColor.y + b
			displayColour.y = ((displayColour.y - 0.5) * c + 0.5)
		)
		
		if channelData.b then
		(
			displayColour.z = currentColor.z + b
			displayColour.z = ((displayColour.z - 0.5) * c + 0.5)
		)
		
		displayColour = RSL_Painter.clamp displayColour
		
		meshOp.setMapVert object RSL_Painter.displayChannel mapVert displayColour
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- assignFromDisplay()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn assignFromDisplay mapVert object channelData =
	(
		local currentColor = meshop.getMapVert object channelData.channel mapVert
		local displayColour = meshop.getMapVert object RSL_Painter.displayChannel mapVert
		
		if channelData.r then currentColor.x = displayColour.x
		if channelData.g then currentColor.y = displayColour.y
		if channelData.b then currentColor.z = displayColour.z
		
		currentColor = RSL_Painter.clamp currentColor
		
		meshOp.setMapVert object channelData.channel mapVert currentColor
	),
	
	
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- UI FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- disableAccelerators() Called when the user enters any control that needs keyboard input e.g. a textBox
	---------------------------------------------------------------------------------------------------------------------------------------
	fn disableAccelerators s e =
	(
		enableAccelerators = false
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOnFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOnFormClosing s e =
	(
		RSL_Painter.adjust.OnFormClosing s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- OnFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn OnFormClosing s e =
	(
		try (RSL_Painter.fill.fillOptionsForm.enabled = true) catch()
		try (RSL_Painter.rand.randomOptionsForm.enabled = true) catch()
		try (RSL_Painter.AO.AOoptionsForm.enabled = true) catch()
		try (RSL_Painter.painterForm.enabled = true) catch()
		
		setINISetting RSL_Painter.INIfile "Colour Adjust" "Location" ([colourAdjustForm.location.x, colourAdjustForm.location.y] as string)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchBrightnessContrastChanged()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchBrightnessContrastChanged s e =
	(
		RSL_Painter.adjust.brightnessContrastChanged s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- brightnessContrastChanged()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn brightnessContrastChanged s e =
	(
		case s.name of
		(
			"brightnessBar":
			(
				sliderChange = true
				brightness = s.value
				brightnessNumeric.value = brightness
			)
			"contrastBar":
			(
				sliderChange = true
				contrast = s.value
				contrastNumeric.value = contrast
			)
		)
		
		local vCount = meshop.getNumMapVerts RSL_Painter.theObject RSL_Painter.displayChannel
		local mapVertArray = for i = 1 to vCount collect i
		
		setBrightnessContrast mapVertArray RSL_Painter.theObject RSL_Painter.currentChannel
		update RSL_Painter.theObject
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchBrightnessContrastVChanged()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchBrightnessContrastVChanged s e =
	(
		RSL_Painter.adjust.brightnessContrastVChanged s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- brightnessContrastVChanged()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn brightnessContrastVChanged s e =
	(
		if not sliderChange then
		(
			case s.name of
			(
				"brightnessNumeric":
				(
					brightness = s.value
					brightnessBar.value = brightness
				)
				"contrastNumeric":
				(
					contrast = s.value
					contrastBar.value = contrast
				)
			)
			
			local vCount = meshop.getNumMapVerts RSL_Painter.theObject RSL_Painter.displayChannel
			local mapVertArray = for i = 1 to vCount collect i
			
			setBrightnessContrast mapVertArray RSL_Painter.theObject RSL_Painter.currentChannel
			update RSL_Painter.theObject
		)
		else
		(
			sliderChange = false
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOkCancelPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOkCancelPressed s e =
	(
		RSL_Painter.adjust.OkCancelPressed s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- OkCancelPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn OkCancelPressed s e =
	(
		local vCount = meshop.getNumMapVerts RSL_Painter.theObject RSL_Painter.currentChannel.channel
		local mapVertArray = for i = 1 to vCount collect i
		
		if s.name == "okButton" then
		(
			assignFromDisplay mapVertArray RSL_Painter.theObject RSL_Painter.currentChannel
			update RSL_Painter.theObject
		)
		else
		(
			RSL_Painter.assignToDisplay mapVertArray
			update RSL_Painter.theObject
		)
		
		colourAdjustForm.close()
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- colorAdjustDialog()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn colorAdjustDialog =
	(
		--In this case, we're going to use a standard form rather than the custom maxForm written for max. this is because I want to change the 
		-- form back colour which you can't do if you use the custom maxForm. For this to work, we need to set the parent of the form to be Max. 
		-- USING THIS TYPE OF FORM MEANS WE NEED TO DISSABLE ACCELERATORS WHEN WE FOCUS ON A CONTROL THAT TAKES
		-- KEY PRESSES SUCH AS SPINNERS AND TEXT BOXES
		
		--Get the max handle pointer.
		local maxHandlePointer=(Windows.GetMAXHWND())
		
		--Convert the HWND handle of Max to a dotNet system pointer
		local sysPointer = DotNetObject "System.IntPtr" maxHandlePointer
		
		--Create a dotNet wrapper containing the maxHWND
		local maxHwnd = DotNetObject "MaxCustomControls.Win32HandleWrapper" sysPointer
		
		colourAdjustForm = RS_dotNetUI.form "colourAdjustForm" text:" Colour Adjust" size:[256, 176] borderStyle:RS_dotNetPreset.FB_FixedToolWindow
		colourAdjustForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		dotnet.addEventHandler colourAdjustForm "FormClosing" dispatchOnFormClosing
		dotnet.addEventHandler colourAdjustForm "Enter" disableAccelerators
		
		brightnessLabel = RS_dotNetUI.Label "brightnessLabel" text:(" Brightness:") \
												textAlign:RS_dotNetPreset.CA_MiddleLeft borderStyle:RS_dotNetPreset.BS_None \
												dockStyle:RS_dotNetPreset.DS_Fill
		
		brightnessBar = RS_dotNetUI.trackBar "brightnessBar" range:#(100, -100, 0) dockStyle:RS_dotNetPreset.DS_Fill
		brightnessBar.TickFrequency = 10
		dotnet.addEventHandler brightnessBar "MouseUp" dispatchBrightnessContrastChanged
		
		brightnessNumeric = RS_dotNetUI.Numeric "brightnessNumeric" value:0.0 range:#(100.0, -100.0, 1, 0) dockStyle:RS_dotNetPreset.DS_Fill \
								margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler brightnessNumeric "Enter" disableAccelerators 
		dotnet.addEventHandler brightnessNumeric "valueChanged" dispatchBrightnessContrastVChanged
		
		contrastLabel = RS_dotNetUI.Label "contrastLabel" text:(" Contrast:") \
												textAlign:RS_dotNetPreset.CA_MiddleLeft borderStyle:RS_dotNetPreset.BS_None \
												dockStyle:RS_dotNetPreset.DS_Fill
		
		contrastBar = RS_dotNetUI.trackBar "contrastBar" range:#(100, -100.0, 0) dockStyle:RS_dotNetPreset.DS_Fill
		contrastBar.TickFrequency = 10
		dotnet.addEventHandler contrastBar "MouseUp" dispatchBrightnessContrastChanged
		
		contrastNumeric = RS_dotNetUI.Numeric "contrastNumeric" value:0.0 range:#(100.0, -100.0, 1, 0) dockStyle:RS_dotNetPreset.DS_Fill \
								margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler contrastNumeric "Enter" disableAccelerators 
		dotnet.addEventHandler contrastNumeric "valueChanged" dispatchBrightnessContrastVChanged
		
		okButton = RS_dotNetUI.Button "okButton" text:("Ok") dockStyle:RS_dotNetPreset.DS_Fill textAlign:RS_dotNetPreset.CA_BottomCenter
		okButton.font = (dotNetObject "System.Drawing.Font" "Small Fonts" 8.0 RS_dotNetPreset.f_Regular)
		okButton.margin = (RS_dotNetObject.paddingObject 0)
		dotnet.addEventHandler okButton "click" dispatchOkCancelPressed
		
		cancelButton = RS_dotNetUI.Button "cancelButton" text:("Cancel") dockStyle:RS_dotNetPreset.DS_Fill textAlign:RS_dotNetPreset.CA_BottomCenter
		cancelButton.font = (dotNetObject "System.Drawing.Font" "Small Fonts" 8.0 RS_dotNetPreset.f_Regular)
		cancelButton.margin = (RS_dotNetObject.paddingObject 0)
		dotnet.addEventHandler cancelButton "click" dispatchOkCancelPressed
		
		okCancelTableLayout = RS_dotNetUI.tableLayout "okCancelTableLayout" text:"okCancelTableLayout" collumns:#((dataPair type:"Percent" value:50),(dataPair type:"Percent" value:50)) \
									rows:#((dataPair type:"Absolute" value:24)) dockStyle:RS_dotNetPreset.DS_Fill
		
		okCancelTableLayout.controls.add okButton 0 0
		okCancelTableLayout.controls.add cancelButton 1 0
		
		colourAdjustTableLayout = RS_dotNetUI.tableLayout "colourAdjustTableLayout" text:"colourAdjustTableLayout" collumns:#((dataPair type:"Percent" value:50),(dataPair type:"Absolute" value:44)) \
									rows:#((dataPair type:"Absolute" value:24),(dataPair type:"Absolute" value:40),(dataPair type:"Absolute" value:24),(dataPair type:"Absolute" value:40),(dataPair type:"Absolute" value:24)) dockStyle:RS_dotNetPreset.DS_Fill
		
		colourAdjustTableLayout.controls.add brightnessLabel 0 0
		colourAdjustTableLayout.controls.add brightnessBar 0 1
		colourAdjustTableLayout.controls.add brightnessNumeric 1 1
		colourAdjustTableLayout.controls.add contrastLabel 0 2
		colourAdjustTableLayout.controls.add contrastBar 0 3
		colourAdjustTableLayout.controls.add contrastNumeric 1 3
		colourAdjustTableLayout.controls.add okCancelTableLayout 0 4
		
		colourAdjustTableLayout.setColumnSpan okCancelTableLayout 2
		
		colourAdjustForm.controls.add colourAdjustTableLayout
		
		local INIlocation = getINISetting RSL_Painter.INIfile "Colour Adjust" "Location"
		
		if INIlocation.count > 0 then
		(
			local loc = readValue (INIlocation as stringStream)
			colourAdjustForm.location.x = loc.x
			colourAdjustForm.location.y = loc.y
		)
		else
		(
			colourAdjustForm.location.x = RSL_Painter.painterForm.location.x
			colourAdjustForm.location.y = RSL_Painter.painterForm.location.x
		)
		
		colourAdjustForm.show maxHwnd
	)
)