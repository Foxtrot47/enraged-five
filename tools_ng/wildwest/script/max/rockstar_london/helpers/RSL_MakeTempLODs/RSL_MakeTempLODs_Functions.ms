filein (RsConfigGetWildWestDir() + @"script\max\rockstar_london\helpers\RSL_Global.ms")
filein (RsConfigGetWildWestDir() + @"script\max\rockstar_london\helpers\RSL_MaterialFunctions.ms")
filein (RsConfigGetWildWestDir() + @"script\max\rockstar_london\helpers\RSL_FixPivots.ms")

DefaultTexMapPath = Project.Art + @"\Textures\Environments\Test\Grey_050_DM.tif"
Outputpath = Project.Art + @"\Textures\Environments\LOD\Proxies\"

LODMultiplier = 8
SLODMultiplier = 11

--ToDo add nconvert to the bin directory so that all users can use it.

	--Alters a MaterialID on a Editable_Poly or Editable_Mesh
	fn DeleteFacesByMaterialID Obj IDToDelete =
	(
		convertToMesh Obj
		
		if classof Obj == Editable_Mesh then 
			(
				FaceCount = getnumfaces Obj
				FaceArray = #()

				for FaceIterator = 1 to FaceCount do 
					(
					
					CurrentFaceID = getfacematid Obj FaceIterator
					if (CurrentFaceID == IDToDelete) then
						(
							append FaceArray FaceIterator
						)
					)
				meshop.deleteFaces Obj FaceArray
			) 
	)

	fn ReduceVertexCount MeshObj =
	(
		addModifier MeshObj (Optimize facethreshold1:65)
		collapseStack MeshObj
	)
	
	--Pulls the mesh in. Similar function to a push modifer with negative factor
	fn PullMesh MeshObj Factor =
	(
			--addModifier Obj (VertexWeld threshold:0.0001)
			addModifier MeshObj (Normalmodifier flip:on)
			addModifier MeshObj (Shell innerAmount:0.0 outerAmount:Factor autosmooth:off selectInnerFaces:on selectEdgeFaces:on selectOuterFaces:off straightenCorners:on)
			addModifier MeshObj (DeleteMesh())
			addModifier MeshObj (Poly_Select())	
			addModifier MeshObj (Normalmodifier flip:on)
			collapseStack MeshObj
	)
	
	fn PushMesh MeshObj Factor =
	(
			--addModifier Obj (VertexWeld threshold:0.0001)
			--addModifier Obj (Normalmodifier flip:on)
			addModifier MeshObj (Shell innerAmount:0.0 outerAmount:Factor autosmooth:off selectInnerFaces:on selectEdgeFaces:on selectOuterFaces:off straightenCorners:on)
			addModifier MeshObj (DeleteMesh())
			addModifier MeshObj (Poly_Select())	
			--addModifier Obj (Normalmodifier flip:on)
			collapseStack MeshObj
	)
	
	fn CheckIsValidShaderType RageShader ShaderTypes =
	(
		for CurrentShaderType in ShaderTypes do
		(
			if ((RageShader_GetShaderType RageShader ) == CurrentShaderType) then
			(return true)
		)
		
		return false
	)
		
	fn ProcessTexture InputPath OutputPath =
	(
		if(getFileSize OutputPath == 0) then
		(
			
 		ProcessCmdString = @"N:\RSGLDN\Transfer\Studio\ArtTextureProxies\nconvert.exe" + " -quiet -overwrite -out bmp -sharpen 20%% -resize 25%% 25%% -o \"" + OutputPath + "\" \"" + InputPath + "\""
 		DOSCommand (ProcessCmdString)
		--ProcessCmdString = @"N:\RSGLDN\Transfer\Studio\ArtTextureProxies\nconvert.exe" + " -quiet -overwrite -out bmp -sharpen 70%% -resize 25%% 25%% -o \"" + OutputPath + "\" \"" + OutputPath + "\""	
		--DOSCommand (ProcessCmdString)
			
		--ProcessCmdString = @"N:\RSGLDN\Transfer\Studio\ArtTextureProxies\nconvert.exe" + " -quiet -overwrite -out bmp -resize 25%% 25%% -o \"" + OutputPath + "\" \"" + OutputPath + "\""
		--DOSCommand (ProcessCmdString)	
		--ProcessCmdString = @"N:\RSGLDN\Transfer\Studio\ArtTextureProxies\nconvert.exe" + " -quiet -overwrite -out bmp -resize 25%% 25%% -o \"" + OutputPath + "\" \"" + OutputPath + "\""
		--HiddenDOSCommand  ProcessCmdString
		)
	)		
	
	fn AppendToString SourceString StringToAppend MaxStringLength =
	(
			Offset = MaxStringLength - StringToAppend.count
  			if(SourceString.count > Offset) then
  			(
  				SourceString = replace SourceString Offset (SourceString.count-(Offset)) ""
 			)
			SourceString = SourceString + StringToAppend	
			
			return SourceString
	)
	
	fn GetBoundingSphereRadius Obj =
	(
		result = 0
        --Take x2-x1, then square it
        part1 = pow (Obj.max.x - Obj.min.x) 2
            --Take y2-y1, then sqaure it
        part2 = pow (Obj.max.y - Obj.min.y) 2
            --Take z2-z1, then square it
        part3 = pow (Obj.max.z - Obj.min.z) 2
            --Add both of the parts together
        underRadical = part1 + part2 + part3
            --Get the square root of the parts
        result = (sqrt underRadical)/2
		return result
	)		
	
	fn SetLODDistance Obj Buffer = 
	(
		ObjBoundingSphereRadius = GetBoundingSphereRadius Obj
		LODDistance = ceil (ObjBoundingSphereRadius + Buffer)
		SetAttr Obj (GetAttrIndex "Gta Object" "LOD distance") (LODDistance)
	)
	
	fn SetupObject Obj =
	(
		CentrePivot Obj
		FixPivot Obj
		SetLODDistance Obj 6
	)
	
	fn GetFilenameFromPath FullPath=
	(
		FullFilename = filenameFromPath FullPath
		Filename = substituteString FullFilename (getFilenameType FullFilename) ""
	)
	
	fn MakeLODMaterial Obj LODObj =
	(
		try(
		LODMultiMaterial = Multimaterial name:(Obj.name + "_LOD") numsubs:1
		SourceMaterial = Obj.material
			
		if(classOf SourceMaterial == XRefMaterial) then
			(
				SourceMaterial  = SourceMaterial.GetSourceMaterial(false)
			)
					
		if(classOf SourceMaterial == Rage_Shader) then
			(
				SourceMaterial = RageShader_ConvertToMultimaterial SourceMaterial
				SetAllFacesToMaterialID LODObj 1
			)
						
		if(classOf SourceMaterial == Multimaterial)then
			(
				for CurrentID in SourceMaterial.materialIDList do
				(	
					if (CheckForMaterialID LODObj CurrentID == true) then
					(
						SubSourceMaterial = SourceMaterial[CurrentID]
						OutputMaterial = undefined
						IsValidMaterial = false
						
						if(classOf SubSourceMaterial == XRefMaterial)  then
						(
							SubSourceMaterial = SubSourceMaterial.GetSourceMaterial(false)
						)
						
						format "Found ID: % | Processing material: % | Material type: % | " CurrentID SubSourceMaterial.name (classOf SubSourceMaterial)	
						
						if(classOf SubSourceMaterial == Rage_Shader)then
						(					
							SourceShaderType = RageShader_GetShaderType SubSourceMaterial
							
							format "Shader Type:  % | " SourceShaderType

							if(matchPattern SourceShaderType pattern:"*terrain*") then
							(--can't handle any 'terrain' type shaders so cancel LOD creation
								throw("ERROR - Object has terrain shader applied to some faces.")
							)
							
							if(matchPattern SourceShaderType pattern:"*decal*") then
							(--make new decal type shader
								OutputMaterial = Rage_Shader name:(SubSourceMaterial.name + "_LOD")
								RageShader_SetShaderType OutputMaterial "Decal"
								
								include "RSL_MakeTempLODs_Functions_Sub.ms"
							)

							if	(IsValidMaterial == false AND matchPattern SourceShaderType pattern:"*cutout*" ) or
								(IsValidMaterial == false AND matchPattern SourceShaderType pattern:"*screendoor*" ) or
								(IsValidMaterial == false AND matchPattern SourceShaderType pattern:"*micromovement*" ) then
							(--make new cutout type shader
								OutputMaterial = Rage_Shader name:(SubSourceMaterial.name + "_LOD")
								RageShader_SetShaderType OutputMaterial "Cutout"
								
								include "RSL_MakeTempLODs_Functions_Sub.ms"
							)
							
							if(IsValidMaterial == false AND matchPattern SourceShaderType pattern:"*alpha*") then
							(--make new alpha type shader
								OutputMaterial = Rage_Shader name:(SubSourceMaterial.name + "_LOD")
								RageShader_SetShaderType OutputMaterial "Alpha"
								
								include "RSL_MakeTempLODs_Functions_Sub.ms"
							)
							
							if(IsValidMaterial == false) then
							(--failed to match any of the other types so make new default type shader
								OutputMaterial = Rage_Shader name:(SubSourceMaterial.name + "_LOD")
								RageShader_SetShaderType OutputMaterial "Default"
																
								include "RSL_MakeTempLODs_Functions_Sub.ms"
							)
						)
						
						if(IsValidMaterial == false) then
						(
							OutputMaterial = Rage_Shader name:(SubSourceMaterial.name + "_LOD")
							RageShader_SetShaderType OutputMaterial "Default"
							RageShader_SetTexMap OutputMaterial "Diffuse Texture" DefaultTexMapPath
							
							format "ERROR.\n"						
						)
						else
						(
							format "COMPLETE.\n"
						)
						
						MultiMat_AddMaterialWithID LODMultiMaterial OutputMaterial CurrentID
					)
				)
			)
			
			--LODMultiMaterial = Rage_Shader name:("TEMP")
			--RageShader_SetShaderType LODMultiMaterial "Default"
			--RageShader_SetTexMap LODMultiMaterial "Diffuse Texture" DefaultTexMapPath
			
			return LODMultiMaterial
		)
		catch
		(
			throw()
		)
	)
	
	fn ProcessObj Obj = 
	(	
		try
		(
			LclMaxFileName = maxFileName
			LclMaxFileName = toUpper LclMaxFileName
			LclMaxFileName = substituteString LclMaxFileName ".MAX" ""
			
			format "Setting up object.\n"
			
			SetupObject Obj
			
			---Setup LOD---
			if(true)then
			(
			LODObj = copy Obj
			collapseStack LODObj

			format "Processing material.\n"
				
			LODMaterial = MakeLODMaterial Obj LODObj
			LODName = AppendToString Obj.name "_LOD" 20
			
			PullMesh LODObj 0.002
			--ReduceVertexCount LODObj
				
			LODObj.name = LODName
			LODObj.material = LODMaterial
				
			AddObjectToLayer LODObj "GEOM_LOD"
				
			AddLodAttr Obj
			SetLodAttrParent Obj LODObj
			
			SetLODDistance LODObj 30
				
			SetAttr LODObj (GetAttrIndex "Gta Object" "TXD") (LclMaxFileName + "_LOD")
			)
					
			---Setup SuperLOD---
			if(false)then
			(
				SLODObj = copy Obj
				collapseStack LODObj
				
				SLODMaterial = Rage_Shader name:(Obj.name + "_SLOD")
					RageShader_SetShaderType SLODMaterial "Default"
					RageShader_SetTexMap SLODMaterial "Diffuse Texture" @"X:\payne\payne_art\Textures\Environments\Test\Solid_Grey_01.tif"
				SLODName = AppendToString Obj.name "_SLOD" 16
				
				PushMesh SLODObj 0.002
				ReduceVertexCount SLODObj
				
				SLODObj.name = SLODName
				SLODObj.material = SLODMaterial
				
			
				AddObjectToLayer SLODObj "GEOM_SLOD"
				
				AddLodAttr LODObj
				SetLodAttrParent LODObj SLODObj
				
				SetLODDistance SLODObj 80
				
				SetAttr SLODObj (GetAttrIndex "Gta Object" "TXD") (LclMaxFileName + "_SLOD")
			)
			else
			(
				SetLODDistance LODObj 65
			)
			
			format "Finished creating LOD object.\n"
		)
		catch
		(
			try(delete LODObj)catch()
			try(delete SLODObj)catch()
			SetLODDistance Obj 45
			format "%\n" (getCurrentException())
		)
	)
