filein "RSL_MakeTempLODs_Functions.ms"
filein "rockstar/util/lod.ms"

try (destroyDialog RSL_MakeTempLODs) catch()

rollout RSL_MakeTempLODs "Make Placeholder LOD(s)" width:152 height:92
(
	progressBar pb1_Progress "ProgressBar" pos:[8,8] width:136 height:12
	button btn_Create "Create LOD(s)" pos:[8,28] width:136 height:24
	button btn_Recreate "Recreate LOD(s)" pos:[8,60] width:136 height:24

	fn IsExportable Obj =
	(
		try(IsNonExportable = GetAttr Obj (GetAttrIndex "Gta Object" "Dont Export"))catch(IsNonExportable=true)
		if(IsNonExportable == false) then
		(
			return true
		)
		else
		(
			return false
		)
	)
	
	fn IsValidObject Obj =
	(
	if(Obj == undefined ) then
		(
			format "Object invalid possibly deleted. Skipping\n"
			return false
		)
	if(IsGeometryType Obj) then () else
		(
			format "Incorrect object not geometry. Skipping\n"
			return false
		)
	if(RsIsLOD Obj) then
		(
			format "Object is in a LOD heirachy. Skipping\n"
			return false
		)
	if(IsExportable Obj) then () else
		(
			format "Object is marked as not to export. Skipping\n"
			return false
		)
	
		return true
	)
	
	fn DeleteRelatedLODObjects ObjArray =
	(
		format "--------------------------------------------------------\n"			
		for Obj in ObjArray do
		(
			if(Obj != undefined) then
			(
			LODObj = RsGetRootMostLodObject Obj
				while LODObj != Obj do
				(
					format "Deleting old LOD object: %\n" LODObj.name
					delete LODObj
					LODObj = RsGetRootMostLodObject Obj
				)
			)
		)
		format "--------------------------------------------------------\n"
	)
	
	fn MakeLODs ObjectArray =
	(
	clearListener()
		
	Increment = 100.0/ObjectArray.count as float
	Total = 0.0
	
	DeleteRelatedLODObjects ObjectArray
		
		with redraw off
		(
			for Obj in ObjectArray do
			(	
				if (Obj != undefined) then
				(
					format "--------------------------------------------------------\n"			
					format "Processing object: %\n" Obj.name
					format "--------------------------------------------------------\n"
					
					if (IsValidObject Obj) then 
					(
					ProcessObj Obj 
					)
					
					format "--------------------------------------------------------\n"
				)
				Total = Total + Increment
				pb1_Progress.value = Total as integer
			)
		)
	)
	
	--Gets all the child objs (hi-detail) of LOD heirachys 
	fn GetSceneLODChildren =
	(
		ReturnObjectArray = #()
		
		for Obj in objects do 
		(
			if(RsIsLOD Obj) OR (RsIsSuperLOD Obj) then () else
			(
				if (RsGetRootMostLodObject Obj != Obj) then
				(
					append ReturnObjectArray Obj
				)
			)
		)
		
		return ReturnObjectArray
	)
	
	fn DeleteAllLODs = 
	(
		LODObjectsToDelete = #()
		for Obj in objects do
		(
			if (RsIsLOD Obj)then
			(
				append LODObjectsToDelete Obj
			)
		)
		
		for LODObj in LODObjectsToDelete do
		(
			delete LODObj
		)
	)
	
	fn RenameLODs =
	(
		LclMaxFileName = maxFileName
		LclMaxFileName = toUpper LclMaxFileName
		LclMaxFileName = substituteString LclMaxFileName ".MAX" ""	
		
		Index = 0
		
		for Obj in objects do
		(
			if (RsIsLOD Obj)then
			(
				Obj.name = LclMaxFileName + "_LOD_" + (Index as string)
				Index = Index + 1
			)
		)
		
		Index = 0
		
		for Obj in objects do
		(
			if (RsIsSuperLOD Obj)then
			(
				Obj.name = LclMaxFileName + "_SLOD_" + (Index as string)
				Index = Index + 1
			)
		)
	)
	
	on btn_Create pressed do
		(
			if(queryBox "This will delete old LODs for the objects you have selected and make new ones based off the Hi-Res geometry. Would you like to continue?" title:"Warning") then
			(
			--DeleteAllLODs()
			MakeLODs selection
			--MakeLODs objects
			RenameLODs()
			)
		)
		
	on btn_Recreate pressed do
		(
			if(queryBox "This will delete all the old LODs and make new ones based off the Hi-Res geometry. Would you like to continue?" title:"Warning") then
			(
		
			MakeLODs (GetSceneLODChildren())
			RenameLODs()
			)
		)
)

createDialog RSL_MakeTempLODs style:#(#style_toolwindow,#style_sysmenu)