SourceDiffuseTexMapPath = RageShader_GetTexMap SubSourceMaterial "Diffuse Texture"
SourceAlphaTexMapPath = RageShader_GetAlphaTexMap SubSourceMaterial 
							
if(SourceDiffuseTexMapPath != undefined ) then
	(
	SourceDiffuseTexMapFilename = GetFilenameFromPath SourceDiffuseTexMapPath
	OutputDiffuseTexMapPath = OutputPath+SourceDiffuseTexMapFilename+"_LOD.bmp"	
	ProcessTexture SourceDiffuseTexMapPath OutputDiffuseTexMapPath	
	RageShader_SetTexMap OutputMaterial "Diffuse Texture" OutputDiffuseTexMapPath
		
	if (SourceAlphaTexMapPath != undefined) then
		(
		SourceAlphaTexMapFilename = GetFilenameFromPath SourceAlphaTexMapPath
		OutputAlphaTexMapPath = OutputPath+SourceAlphaTexMapFilename+"_LOD.bmp"
		ProcessTexture SourceAlphaTexMapPath OutputAlphaTexMapPath
		RageShader_SetAlphaTexMap OutputMaterial (bitmaptexture filename:OutputAlphaTexMapPath) 
		)
							
	if(RstGetIsTwoSided SubSourceMaterial) then
		(
			RstSetIsTwoSided OutputMaterial true
		)
		
IsValidMaterial = true
)