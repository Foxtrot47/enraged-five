


struct RSL_MMPainterAO
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- Struct variables
	---------------------------------------------------------------------------------------------------------------------------------------
	
	currentMode = #prop,
	
	formOpened = false,
	
	sceneLights = #(),
	sceneObjects = #(),
	
	sky_Light,
	bouncePlane,
	
	total = 2.0,
	
	brightness = 100.0,
	contrast = 50.0,
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- UI variables
	---------------------------------------------------------------------------------------------------------------------------------------
	
	AOoptionsForm,
	propMode,sceneMode,
	brightnessNumeric,contrastNumeric,
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- switchLight()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn switchLight light mode =
	(
		if mode == #pre then
		(
			if (isProperty light #enabled) then
			(
				append sceneLights (dataPair node:light state:light.enabled)
				
				light.enabled = false
			)
		)
		else
		(
			light.node.enabled = light.state
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- switchObject()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn switchObject object mode =
	(
		if mode == #pre then
		(
			if (isProperty object #isGIExcluded) and object != RSL_Painter.theObject then
			(
				append sceneObjects (dataPair node:object state:object.isGIExcluded)
				
				object.isGIExcluded = true
			)
		)
		else
		(
			object.node.isGIExcluded = object.state
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- AOSetup()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn AOSetup object =
	(
		sceneLights = #()
		sceneObjects = #()
		
		switchLight lights #pre
		
		if currentMode == #prop then
		(
			switchObject geometry #pre
		)
		else
		(
			
		)
		
		object.isGIExcluded = false
		
		if sky_Light == undefined then
		(
			sky_Light = IES_Sky color:(color 255 255 255)
			sky_Light.isHidden  =true
		)
		
		if currentMode == #prop then
		(
			if bouncePlane == undefined then
			(
				local planePos = object.pos
				planePos.z = object.min.z - 0.25
				
				local size = (distance object.max object.min) * 10
				
				bouncePlane = plane pos:planePos width:size length:size wireColor:white
				bouncePlane.isHidden  =true
			)
		)
		
		sceneRadiosity.radiosity = undefined 
		sceneRadiosity.radiosity = radiosity()
		
		sceneRadiosity.radiosity.radFiltering = 3
		
		local log_exposure = Logarithmic_Exposure_Control()
		SceneExposureControl.exposureControl = log_exposure
		log_exposure.exteriorDaylight = true
		
		log_exposure.brightness = brightness
		log_exposure.contrast = contrast
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- calculateAO()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn calculateAO object =
	(
		SceneRadiosity.showPanel()
		
		sceneRadiosity.radiosity.Reset true false
		sceneRadiosity.radiosity.Start()
		
		local count = 1.0
		
		progressStart "Calculating Radiosity..."
		
		local abort = false
		
		while not (sceneRadiosity.radiosity.doesSolutionExist()) and not abort do
		(
			progressUpdate (count / total * 100.0)
			count += 1
			total = count + 1
			sleep 1.0
			
			if count == 60.0 then
			(
				sceneRadiosity.radiosity.stop()
				abort = true
			)
		)
		
		progressEnd()
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- pressVPAssign()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn pressVPAssign =
	(
		cui.commandPanelOpen = true
		setCommandPanelTaskMode #modify
		
		local assignRollHWND = windows.getChildHWND #max "Assign Vertex Colors"
		
		if assignRollHWND != undefined then 
		( 
			local assignHWND = windows.getChildHWND (UIAccessor.GetParentWindow assignRollHWND[1]) "Assign"
			
			if assignHWND != undefined then
			(
				UIAccessor.PressButton assignHWND[1]
			)
		)
		else
		(
			local dialogHWND = UIAccessor.GetPopupDialogs()
			local commandHWND
			
			for HWND in dialogHWND do
			(
				if (UIAccessor.GetWindowText HWND) == "Command Panel" then
				(
					commandHWND = HWND
				)
			)
			
			if commandHWND != undefined then
			(
				local children = UIAccessor.GetChildWindows commandHWND
				local assignHWND
				
				for HWND in children do 
				(
					local name = (UIAccessor.GetWindowText HWND)
					
					if name == "Assign" then
					(
						assignHWND = HWND
					)
				)
				
				if assignHWND != undefined then
				(
	 				UIAccessor.PressButton assignHWND
				)
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- bakeRadiosity()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn bakeRadiosity object channelData =
	(
		addModifier object (vertexpaint())
		object.modifiers[#VertexPaint].mapchannel = channelData.channel
		object.modifiers[#VertexPaint].lightingModel = 0
		object.modifiers[#VertexPaint].radiosityOption = 1
		
		pressVPAssign()
		
		convertToMesh object
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- AOPost()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn AOPost =
	(
		if (isValidNode sky_Light) then delete sky_Light
		if (isValidNode bouncePlane) then delete bouncePlane
		if (isValidNode hitObject) then delete hitObject
		
		sky_Light = undefined
		bouncePlane = undefined
		
		switchLight sceneLights #post
		if currentMode == #prop then switchObject sceneObjects #post
		
		sceneRadiosity.radiosity = undefined
		SceneExposureControl.exposureControl = undefined
		
		renderSceneDialog.close()
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- AOChannel()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn AOChannel object channelData =
	(
		AOSetup object
		
		calculateAO object
		
		bakeRadiosity object channelData
		
		RSL_Painter.showChannel RSL_Painter.theObject true
		
		AOPost()
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- UI FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- disableAccelerators() Called when the user enters any control that needs keyboard input e.g. a textBox
	---------------------------------------------------------------------------------------------------------------------------------------
	fn disableAccelerators s e =
	(
		enableAccelerators = false
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOnFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOnFormClosing s e =
	(
		RSL_Painter.AO.onFormClosing s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- onFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn onFormClosing s e =
	(
		RSL_Painter.AOoptionsButton.checked = false
		
		setINISetting RSL_Painter.INIfile "AO" "Location" ([AOoptionsForm.location.x, AOoptionsForm.location.y] as string)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchAOTypeCheck()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchAOTypeCheck s e =
	(
		RSL_Painter.AO.AOTypeCheck s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- AOTypeCheck()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn AOTypeCheck s e =
	(
		s.checked = true
		
		case s.name of
		(
			"propMode":
			(
				sceneMode.checked = false
			)
			"sceneMode":
			(
				propMode.checked = false
			)
		)
		
		currentMode = s.tag.value
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchAOValueChanged()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchAOValueChanged s e =
	(
		RSL_Painter.AO.AOValueChanged s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- AOValueChanged()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn AOValueChanged s e =
	(
		case s.name of
		(
			"brightnessNumeric":(brightness = (RSL_Painter.decimalToFloat s))
			"contrastNumeric":(contrast = (RSL_Painter.decimalToFloat s))
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- optionsDialog()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn optionsDialog =
	(
		formOpened = true
		
		--In this case, we're going to use a standard form rather than the custom maxForm written for max. this is because I want to change the 
		-- form back colour which you can't do if you use the custom maxForm. For this to work, we need to set the parent of the form to be Max. 
		-- USING THIS TYPE OF FORM MEANS WE NEED TO DISSABLE ACCELERATORS WHEN WE FOCUS ON A CONTROL THAT TAKES
		-- KEY PRESSES SUCH AS SPINNERS AND TEXT BOXES
		
		--Get the max handle pointer.
		local maxHandlePointer=(Windows.GetMAXHWND())
		
		--Convert the HWND handle of Max to a dotNet system pointer
		local sysPointer = DotNetObject "System.IntPtr" maxHandlePointer
		
		--Create a dotNet wrapper containing the maxHWND
		local maxHwnd = DotNetObject "MaxCustomControls.Win32HandleWrapper" sysPointer
		
		AOoptionsForm = RS_dotNetUI.form "AOoptionsForm" text:" AO Options" size:[146, 112] borderStyle:RS_dotNetPreset.FB_FixedToolWindow
		AOoptionsForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		dotnet.addEventHandler AOoptionsForm "FormClosing" dispatchOnFormClosing
		dotnet.addEventHandler AOoptionsForm "Enter" disableAccelerators
		
		optionsTableLayout = RS_dotNetUI.tableLayout "optionsTableLayout" text:"optionsTableLayout" collumns:#((dataPair type:"Percent" value:50),(dataPair type:"Percent" value:50)) \
									rows:#((dataPair type:"Absolute" value:40),(dataPair type:"Absolute" value:24),(dataPair type:"Absolute" value:24)) dockStyle:RS_dotNetPreset.DS_Fill
		
		propMode = RS_dotNetUI.checkBox "propMode" text:(toUpper "Prop") appearance:RS_dotNetPreset.AC_Button dockStyle:RS_dotNetPreset.DS_Fill
		propMode.margin = (RS_dotNetObject.paddingObject 0)
		propMode.font = RS_dotNetPreset.FontSmall--(dotNetObject "System.Drawing.Font" "Small Fonts" 8.0 RS_dotNetPreset.f_Regular)
		propMode.tag = dotNetMXSValue #prop
		dotnet.addEventHandler propMode "click" dispatchAOTypeCheck
		
		sceneMode = RS_dotNetUI.checkBox "sceneMode" text:(toUpper "Scene") appearance:RS_dotNetPreset.AC_Button dockStyle:RS_dotNetPreset.DS_Fill
		sceneMode.margin = (RS_dotNetObject.paddingObject 0)
		sceneMode.font = RS_dotNetPreset.FontSmall--(dotNetObject "System.Drawing.Font" "Small Fonts" 8.0 RS_dotNetPreset.f_Regular)
		sceneMode.tag = dotNetMXSValue #scene
		dotnet.addEventHandler sceneMode "click" dispatchAOTypeCheck
		
		brightnessLabel = RS_dotNetUI.Label "brightnessLabel" text:(" Brightness:") \
												textAlign:RS_dotNetPreset.CA_MiddleLeft borderStyle:RS_dotNetPreset.BS_None \
												dockStyle:RS_dotNetPreset.DS_Fill
		
		brightnessNumeric = RS_dotNetUI.Numeric "brightnessNumeric" value:brightness range:#(100.0, 0.0, 0.1, 1) dockStyle:RS_dotNetPreset.DS_Fill \
								margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler brightnessNumeric "Enter" disableAccelerators 
		dotnet.addEventHandler brightnessNumeric "ValueChanged" dispatchAOValueChanged
		
		contrastLabel = RS_dotNetUI.Label "contrastLabel" text:(" Contrast:") \
												textAlign:RS_dotNetPreset.CA_MiddleLeft borderStyle:RS_dotNetPreset.BS_None \
												dockStyle:RS_dotNetPreset.DS_Fill
		
		contrastNumeric = RS_dotNetUI.Numeric "contrastNumeric" value:contrast range:#(100.0, 0.0, 0.1, 1) dockStyle:RS_dotNetPreset.DS_Fill \
								margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler contrastNumeric "Enter" disableAccelerators 
		dotnet.addEventHandler contrastNumeric "ValueChanged" dispatchAOValueChanged
		
		optionsTableLayout.controls.add propMode 0 0
		optionsTableLayout.controls.add sceneMode 1 0
		optionsTableLayout.controls.add brightnessLabel 0 1
		optionsTableLayout.controls.add brightnessNumeric 1 1
		optionsTableLayout.controls.add contrastLabel 0 2
		optionsTableLayout.controls.add contrastNumeric 1 2
		
		propMode.checked = currentMode == #prop
		sceneMode.checked = currentMode == #scene
		
		local mode
		
		if propMode.checked then
		(
			mode = propMode
		)
		else
		(
			mode = sceneMode
		)
		
		AOTypeCheck mode ""
		
		AOoptionsForm.controls.add optionsTableLayout
		
		local INIlocation = getINISetting RSL_Painter.INIfile "AO" "Location"
		
		if INIlocation.count > 0 then
		(
			local loc = readValue (INIlocation as stringStream)
			AOoptionsForm.location.x = loc.x
			AOoptionsForm.location.y = loc.y
		)
		else
		(
			AOoptionsForm.location.x = RSL_Painter.painterForm.location.x
			AOoptionsForm.location.y = RSL_Painter.painterForm.location.x
		)
		
		AOoptionsForm.show maxHwnd
	)
)