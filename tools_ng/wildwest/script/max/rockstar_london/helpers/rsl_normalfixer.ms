
struct sharedVertex
(
	objectArray,
	vertexArray
)

struct sharedFace
(
	objectArray,
	faceArray,
	cornerArray
)

struct explicitObjectNormals
(
	object,
	normalArray = #()
)

struct explicitNormal
(
	face,
	corner,
	normal
)

fn getSmoothingArray object face =
(
	local result = #{}
	local smoothingInteger = polyOp.getFaceSmoothGroup object face
	
	if smoothingInteger < 0 then
	(
		result[32] = true
		smoothingInteger -= 2^31
	)
	
	for i = 1 to 31 do
	(
		result[i] = (mod smoothingInteger 2 > 0.5)
		smoothingInteger /= 2
	)
	
	result as array
)

fn testForShared array1 array2 =
(
	local result = false
	
	for item in array1 do
	(
		local isShared = (findItem array2 item) != 0
		
		if isShared then
		(
			result = true
		)
	)
	
	result
)

fn indexInFace object face vert =
(
	local result = 0
	local faceVertsArray = polyOp.getFaceVerts object face
	
	result = findItem faceVertsArray vert
)

fn checkTypes array =
(
	local result = true
	
	for object in array do
	(
		if not (canConvertTo object editable_poly) then
		(
			result = false
		)
	)
	
	result
)

fn indexInExplicitArray object explicitArray =
(
	local result = 0
	
	for i = 1 to explicitArray.count do
	(
		if explicitArray[i].object == object then
		(
			result = i
		)
	)
	
	result
)

fn indexInSharedArray object vertIndex sharedArray =
(
	local result = 0
	
	for i = 1 to sharedArray.count do
	(
		local objectIndex = (findItem sharedArray[i].objectArray object)
		if objectIndex != 0 and sharedArray[i].vertexArray[objectIndex] == vertIndex then
		(
			result = i
		)
	)
	
	result
)

fn isUniqueInShared shared object vertIndex =
(
	local result = true
	
	local objectIndex = findItem shared.objectArray object
	
	if objectIndex != 0 then
	(
		if shared.vertexArray[objectIndex] == vertIndex then
		(
			result = false
		)
	)
	
	result
)

fn indexInSharedFaceArray object faceIndex cornerIndex sharedArray =
(
	local result = 0
	
	for i = 1 to sharedArray.count do
	(
		local objectIndex = (findItem sharedArray[i].objectArray object)
		if objectIndex != 0 and sharedArray[i].faceArray[objectIndex] == faceIndex and sharedArray[i].cornerArray[objectIndex] == cornerIndex then
		(
			result = i
		)
	)
	
	result
)

fn isUniqueInSharedFace shared object faceIndex cornerIndex =
(
	local result = true
	
	local objectIndex = findItem shared.objectArray object
	
	if objectIndex != 0 then
	(
		if shared.faceArray[objectIndex] == faceIndex and shared.cornerArray[objectIndex] == cornerIndex then
		(
			result = false
		)
	)
	
	result
)


fn fixFragmentNormals =
(
	objectArray = selection as array
	
	if objectArray.count > 1 then
	(
		if (checkTypes objectArray) then
		(
			convertTo objectArray editable_poly
			
			local sharedArray = #()
			
			for i = 1 to objectArray.count - 1 do
			(
				local this = objectArray[i]
				
				for s = i + 1 to objectArray.count do
				(
					local other = objectArray[s]
					
					for vert in this.verts do
					(
						local sharedIndex = indexInSharedArray this vert.index sharedArray
						
						for otherVert in other.verts do
						(
							if (distance vert.pos otherVert.pos) < 0.001 then
							(
								if sharedIndex == 0 then
								(
									append sharedArray (sharedVertex objectArray:#(this, other) vertexArray:#(vert.index, otherVert.index))
								)
								else
								(
									if (isUniqueInShared sharedArray[sharedIndex] other otherVert.index) then
									(
										append sharedArray[sharedIndex].objectArray other
										append sharedArray[sharedIndex].vertexArray otherVert.index
									)
								)
							)
						)
					)
				)
			)
			
			local explicitArray = for object in objectArray collect (explicitObjectNormals object:object) --#()
			
			for entry in sharedArray do
			(
				local sharedFaceArray = #()
				local normalArray = #()
				
				for i = 1 to entry.objectArray.count - 1 do
				(
					local this = entry.objectArray[i]
					local thisVertex = entry.vertexArray[i]
					local thisFaceArray = (polyOp.getFacesUsingVert this thisVertex) as array
					
					for s = i + 1 to entry.objectArray.count do
					(
						local other = entry.objectArray[s] 
						local otherVertex = entry.vertexArray[s]
						local otherFaceArray = (polyOp.getFacesUsingVert other otherVertex) as array
						
						for thisFace in thisFaceArray do
						(
							local thisNormal = polyOp.getFaceNormal this thisFace
							local thisfaceVerts = polyop.getFaceVerts this thisFace
							local thisVertFaceIndex = findItem thisfaceVerts thisVertex
							
							local sharedIndex = indexInSharedFaceArray this thisFace thisVertFaceIndex sharedFaceArray 
							
							for otherFace in otherFaceArray do
							(
								local otherNormal = polyOp.getFaceNormal other otherFace
								local otherFaceVerts = polyop.getFaceVerts other otherFace
								local otherVertFaceIndex = findItem otherFaceVerts otherVertex
								
								if (dot thisNormal otherNormal) > 0.1 then
								(
									local thisSmoothingArray = getSmoothingArray this thisFace
									local otherSmoothingArray = getSmoothingArray other otherFace
									
									local sharedSmoothing = testForShared thisSmoothingArray otherSmoothingArray
									
									if sharedSmoothing then
									(
										if sharedIndex == 0 then
										(
											append sharedFaceArray (sharedFace objectArray:#(this, other) faceArray:#(thisFace, otherFace) cornerArray:#(thisVertFaceIndex, otherVertFaceIndex))
											append normalArray #(thisNormal, otherNormal)
										)
										else
										(
											if (isUniqueInSharedFace sharedFaceArray[sharedIndex] other otherFace otherVertFaceIndex) then
											(
												append sharedFaceArray[sharedIndex].objectArray other
												append sharedFaceArray[sharedIndex].faceArray otherFace
												append sharedFaceArray[sharedIndex].cornerArray otherVertFaceIndex
												append normalArray[sharedIndex] otherNormal
											)
										)
									)
								)
							)
						)
					)
				)
				
				for i = 1 to sharedFaceArray.count do
				(
					local entry = sharedFaceArray[i]
					local sharedNormals = normalArray[i]
					local newNormal = [0,0,0]
					
					for n in sharedNormals do
					(
						newNormal += n
					)
					
					newNormal = normalize (newNormal / sharedNormals.count)
					
					for i = 1 to entry.objectArray.count do
					(
						local object = entry.objectArray[i]
						local face = entry.faceArray[i]
						local corner = entry.cornerArray[i]
						
						local newExplicitNormal = (explicitNormal face:face corner:corner normal:newNormal)
						
						local index = indexInExplicitArray object explicitArray
						append explicitArray[index].normalArray newExplicitNormal
					)
				)
			)
			
-- 			print explicitArray
			
			for entry in explicitArray do
			(
				select entry.object
				local editNormalsMod = editNormals ()
				addModifier entry.object editNormalsMod
				
				editNormalsMod.RebuildNormals
				
				for explicitNormal in entry.normalArray do
				(
					local normalIndex = editNormalsMod.GetNormalID explicitNormal.face explicitNormal.corner
					
					editNormalsMod.SetNormalExplicit normalIndex explicit:true
					editNormalsMod.SetNormal normalIndex explicitNormal.normal
				)
				
				convertTo entry.object editable_poly
			)
			
			select objectArray
		)
		else
		(
			messageBox "One or more objects in the selection cannot be converted to an editable poly" title:"Error..."
		)
	)
	else
	(
		messageBox "You must have at least two objects selected." title:"Error..."
	)
)

setCommandPanelTaskMode mode:#modify
-- suspendEditing which:#modify
fixFragmentNormals()
-- resumeEditing which:#modify