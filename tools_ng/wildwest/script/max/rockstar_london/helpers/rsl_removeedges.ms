
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/Utils/RSL_INIOperations.ms")

try (destroyDialog RSL_RemoveEdges) catch()

rollout RSL_RemoveEdges "Remove Edges"
(
	local faceDataChannel
	local RSL_meshCleanIsSelected = #(1699710735, -200361706)
	local localConfigINI = (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_RemoveEdges.ini")
	
	checkBox chkb_selected "Selected Elements" checked:true offset:[-8,0]
	checkBox chkb_UV "Keep UV Edges" checked:true offset:[-8,0]
	checkBox chkb_selectedEdges "Keep Selected Edges" checked:false offset:[-8,0]
	checkBox chkb_preserveMats "Preserve Material Edges" checked:false offset:[-8,0]
	checkBox chkb_removeVerts "Remove Verts" checked:false offset:[-8,0]
	label lbl_faceThreshold "Face Threshold:" across:2 offset:[2,0]
	spinner spn_faceThreshold range:[0.0, 1.0, 0.999] scale:0.001 fieldWidth:40 offset:[8,0]
	spinner spn_edgeThreshold range:[0.0, 1.0, 0.99] scale:0.01 fieldWidth:40 offset:[8,0] enabled:false
	button btn_edgeRemove "Remove Edges" width:152 offset:[-1,0]
	progressBar pbar_progress value:100.0 color:orange width:154 offset:[-9,0]
	
	fn isSimilar face1 face2 object =
	(
		local vertArray1 = (polyOp.getVertsUsingFace face1 object) as array
		local vertArray2 = (polyOp.getVertsUsingFace face2 object) as array
	)
	
	mapped fn setSelectedFace face theObject =
	(
		faceDataChannel.setValue face (findItem theObject.selectedFaces face > 0)
	)
	
	mapped fn getSelectedFace face &outArray =
	(
		if (faceDataChannel.getValue face) then
		(
			append outArray face
		)
	)
	
	fn setFaceDataForSelected theObject =
	(
		try
		(
			simpleFaceManager.removeChannel theObject RSL_meshCleanIsSelected
		)
		catch()
		
		faceDataChannel = simpleFaceManager.addChannel theObject type:#boolean id:RSL_meshCleanIsSelected name:"RSL_RemoveEdges is Selected Channel"
		
		if faceDataChannel != undefined then
		(
			setSelectedFace (for face in theObject.faces collect face.index) theObject
		)
	)
	
	fn vertRemove vertexArray theObject =
	(
		local removeArray = #()
		
		for i = 1 to vertexArray.count do
		(
			local vert = vertexArray[i]
			
			pbar_progress.value = (i as float / vertexArray.count * 100.0)
			
			local edgeArray = (polyOp.getEdgesUsingVert theObject vert) as array
			
			if edgeArray.count == 2 then
			(
				local vertIndexes = makeUniqueArray (((polyOp.getVertsUsingEdge theObject edgeArray[1]) as array) + ((polyOp.getVertsUsingEdge theObject edgeArray[2]) as array))
				
				if vertIndexes.count == 3 then
				(
					local secondVert1
					local secondVert2
					
					for index in vertIndexes do
					(
						if index != vert then
						(
							if secondVert1 == undefined then
							(
								secondVert1 = index
							)
							else
							(
								secondVert2 = index
							)
						)
					)
					
					local vector1 = normalize (theObject.verts[secondVert1].pos - theObject.verts[vert].pos)
					local vector2 = normalize ( theObject.verts[secondVert2].pos - theObject.verts[vert].pos)
					
					local difference = (dot vector1 vector2) < -spn_edgeThreshold.value
					
					if difference then
					(
						append removeArray vert
					)
				)
				else
				(
					append removeArray vert
				)
			)
		)
		
		polyOp.setVertSelection theObject removeArray
		theObject.EditablePoly.remove selLevel:#Vertex
		
		subObjectLevel = 5
		
		polyop.retriangulate theObject #all
	)
	
	fn edgeRemove egdeArray theObject =
	(
		local removeArray = #()
		local vertArray = ()
		for i = 1 to egdeArray.count do
		(
			local edge = egdeArray[i]
			
			pbar_progress.value = (i as float / egdeArray.count * 100.0)
			
			local vertexArray = (polyOp.getVertsUsingEdge theObject edge) as array
			local faceArray = (polyOp.getFacesUsingEdge theObject edge) as array
			
			if faceArray.count == 2 then
			(
				local normal1 = polyOp.getFaceNormal theObject faceArray[1]
				local normal2 = polyOp.getFaceNormal theObject faceArray[2]
				
				local openEdge = false
				local selectedEdge = false
				local materialBoundary = false
				
				if chkb_UV.checked then
				(
					local index11 = findItem (polyOp.getFaceVerts theObject faceArray[1]) vertexArray[1]
					local index12 = findItem (polyOp.getFaceVerts theObject faceArray[2]) vertexArray[1]
					
					local index21 = findItem (polyOp.getFaceVerts theObject faceArray[1]) vertexArray[2]
					local index22 = findItem (polyOp.getFaceVerts theObject faceArray[2]) vertexArray[2]
					
					local mapIndex11 = (polyop.getMapFace theObject 1 faceArray[1])[index11]
					local mapIndex12 = (polyop.getMapFace theObject 1 faceArray[2])[index12]
					
					local mapIndex21 = (polyop.getMapFace theObject 1 faceArray[1])[index21]
					local mapIndex22 = (polyop.getMapFace theObject 1 faceArray[2])[index22]
					
					if mapIndex11 != mapIndex12 or mapIndex21 != mapIndex22 then
					(
						openEdge = true
					)
				)
				
				if chkb_selectedEdges.checked then
				(
					selectedEdge = (findItem theObject.selectedEdges edge) != 0
				)
				
				if chkb_preserveMats.checked then
				(
					local materialID1 = polyOp.getFaceMatID theObject faceArray[1]
					local materialID2 = polyOp.getFaceMatID theObject faceArray[2]
					
					materialBoundary = materialID1 != materialID2
				)
				
				if abs (dot normal1 normal2) >= spn_faceThreshold.value and not openEdge and not selectedEdge and not materialBoundary then
				(
					append removeArray edge
				)
			)
		)
		
		polyOp.setEdgeSelection theObject removeArray
		theObject.EditablePoly.remove selLevel:#Edge
		
		if chkb_removeVerts.checked then
		(
			local vertArray = #()
			
			if chkb_selected.checked then
			(
				vertArray = makeUniqueArray ((polyOp.getVertsUsingFace theObject theObject.selectedFaces) as array)
			)
			else
			(
				vertArray = makeUniqueArray ((polyOp.getVertsUsingFace theObject theObject.faces) as array)
			)
			
			vertRemove vertArray theObject
		)
		
		subObjectLevel = 5
		
		polyop.retriangulate theObject #all
	)
	
	on RSL_RemoveEdges open do
	(
		RSL_INIOps.readLocalConfigINIStdRollout RSL_RemoveEdges localConfigINI
	)
	
	on RSL_RemoveEdges close do
	(
		RSL_INIOps.writeLocalConfigINIStdRollout RSL_RemoveEdges localConfigINI
	)
	
	on chkb_removeVerts changed state do
	(
		spn_edgeThreshold.enabled = state
	)
	
	on btn_edgeRemove pressed do
	(
		pbar_progress.value = 0
		
		undo "Edge Remove" on
		(
			theObject = selection[1]
			
			if theObject != undefined then
			(
				if (classOf theObject) == editable_poly then
				(
					setFaceDataForSelected theObject
					
					local edgeArray = #()
					
					if chkb_selected.checked then
					(
						edgeArray = makeUniqueArray ((polyOp.getEdgesUsingFace theObject theObject.selectedFaces) as array)
					)
					else
					(
						edgeArray = makeUniqueArray ((polyOp.getEdgesUsingFace theObject theObject.faces) as array)
					)
					
					edgeRemove edgeArray theObject
					
					simpleFaceManager.removeChannel theObject RSL_meshCleanIsSelected
				)
				else
				(
					messageBox "The object is not an editable poly." title:"Error..."
				)
			)
		)
	)
)

positionINISetting = (getINISetting (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_RemoveEdges.ini") "Main" "Position")
dialogPos = undefined

if positionINISetting != "" then
(
	local dialogPosition = readValue (positionINISetting as stringStream)
	
	dialogPos = dialogPosition
)

if dialogPos != undefined then
(
	createDialog RSL_RemoveEdges pos:dialogPos style:#(#style_toolwindow,#style_sysmenu)
)
else
(
	createDialog RSL_RemoveEdges style:#(#style_toolwindow,#style_sysmenu)
)

positionINISetting = undefined
dialogPos = undefined
