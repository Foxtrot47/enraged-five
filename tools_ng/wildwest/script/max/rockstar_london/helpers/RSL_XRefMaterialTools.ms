
filein "RSL_MaterialFunctions.ms"

try (destroyDialog RSL_XRefMaterialTools) catch()

rollout RSL_XRefMaterialTools "XRef Material Tools" width:168 height:288
(
	struct MaterialIDKey (OldID, NewID)
	local MasterMaterial
	local MaterialIDIterator
	local InvalidMaterial
	
	editText TEXTBOX_MaterialLibraryFile "File" pos:[8,8] width:152 height:16 text:"X:\payne\payne_art\Maps\v_fav2\materials\fav_2_main.max"
	button BUTTON_MakeMaterialLibrary "Make Material Library" pos:[8,32] width:121 height:24 toolTip:"Make a XRef Material library from a .max file." 
	button BUTTON_BrowseFile "..." pos:[136,32] width:25 height:24 
	button BUTTON_UpdateMapList "Update Map List" pos:[8,64] width:152 height:24 toolTip:"Update/refresh the map list within a 'UVWUnwrap' modifer. Needs a 'UVWUnwrap' modifer at the top of the stack." 
	button BUTTON_PreviewMap "Preview Map" pos:[8,96] width:152 height:24 toolTip:"Preview the DIFFUSE map for the currently selected XRef Material" 
	button BUTTON_DisableXRefMaterial "Disable Material" pos:[8,128] enabled:false width:152 height:24 toolTip:"Disable the currently selected XRef Material" 
	button BUTTON_EnableXRefMaterial "Enable Material" pos:[8,160] enabled:false width:152 height:24 toolTip:"Enables the currently selected XRef Material" 
	button BUTTON_ProcessMaterialNames "Update Material Names" pos:[8,192] width:152 height:24 toolTip:"Updates the multimaterial slot names based on the material name." 
	button BUTTON_InstanceXRefMaterials "Instance Duplicates" enabled:false pos:[8,224] width:152 height:24 toolTip:"Makes all XRef Materials that are the same instances." 
	button BUTTON_ApplySceneMaterial "Apply Scene Material" enabled:true pos:[8,256] width:152 height:24 toolTip:"Applys scene material to selected object" 
	
	on BUTTON_MakeMaterialLibrary pressed do 
	(			
				FileName = TEXTBOX_MaterialLibraryFile.text
				MatLibrary_ClearLibrary()
				clearSelection()
				
				MatXRefInterface = objXRefMgr.AddXRefItemsFromFile FileName xrefOptions:#selectNodes #asProxy
				ObjectArray = selection as array
				
				for obj in ObjectArray do
				(
				if(obj.Material != undefined) then
					(
					obj.Material.showInViewport = true
					obj.Material.name = toupper(obj.name)
					append currentMaterialLibrary obj.Material
					)
				)

				delete ObjectArray
	)
	
	on BUTTON_BrowseFile pressed do
	(
		NewFile = getOpenFileName caption:"Select a .max file containing library materials" types: "3DS Max File" ".max"
		if (NewFile != undefined) then
		(
		TEXTBOX_MaterialLibraryFile.text = NewFile 
		)
	)
	
	on BUTTON_UpdateMapList pressed do
	(
		fn GetMatIDs Obj =
		(
			IDList = #()
			
			if classof Obj == Editable_Mesh or classof Obj == Editable_Poly or classof Obj == PolyMeshObject then 
			(
				if (hasProperty Obj.material "materialList") == true then 
					(	
					numFaces = getnumfaces Obj
			
					for j = 1 to numFaces do 
					(
						if classof Obj == Editable_Mesh then 
						(
							matID = getfacematid Obj j
						) 
						
						if classof obj == Editable_Poly or classof Obj == PolyMeshObject then 
						(
							matID = polyop.getfacematid Obj j
						)
			
						if matID != undefined then 
						(
							if finditem IDList matID == 0 then 
							(	
								append IDList matID
							)
						)
					)
					) 
			) 
			
			return IDList
		)

		fn GetSubMatByID Mat ID = 
		(
			if(classof Mat == Multimaterial) then
			(
				for i = 1 to Mat.numsubs do
				(
					if (Mat.materialIDList[i] == ID) then
					(
						return Mat.materialList[i]			
					)			
				)		
			)
			return undefined
		)

		if (selection.count != 1) then
			(
				print "No object selected"
				return()
			)
			
		Obj = selection[1]
			
		if(classOf Obj.Material == Multimaterial) then
				(
					IDList = GetMatIDs Obj
					
					print IDList
					for ID in IDList do 
					(
						Mat = GetSubMatByID Obj.Material ID
						
						if(classof Mat != UndefinedClass) then
						(
							if(classof Mat == XRef_Material) then 
							(
								Mat = Mat.GetSourceMaterial(false)
							)
							
							for i = 1 to (getNumSubTexmaps (Mat)) do
							(
								CurrentMap = getSubTexmap (Mat) i
								try
								(
								Obj.modifiers[#unwrap_uvw].AddMap CurrentMap
								)
								catch()
							)
						)
					)
				)
	)
	
	on BUTTON_PreviewMap pressed do
	(
		Mat = medit.GetCurMtl()
			
		if(classof Mat == XRef_Material) then 
		(
			Mat = Mat.GetSourceMaterial(false)
		)
		else
		(
			return()
		)
		
		if(classof Mat == UndefinedClass) then 
		(
			return()
		)
		
		CurrentMap = getSubTexmap (Mat) 1
		if(CurrentMap != undefined) then
		(
			CurrentBitmap = openBitMap (CurrentMap.filename)
			display(CurrentBitmap)
		)
	)
	
	on BUTTON_DisableXRefMaterial pressed do 
	(
		Mat = medit.GetCurMtl()
		
		if((classof Mat) == XRef_Material)then
				(
					if ((matchPattern Mat.srcItemName pattern:"*.DISABLED")==false) then
					(
						Mat.srcItemName = Mat.srcItemName + ".DISABLED"
						--print "false"
					)
				)	
	)
	
	on BUTTON_EnableXRefMaterial pressed do 
	(
		Mat = medit.GetCurMtl()
		
		if((classof Mat) == XRef_Material)then
				(
					if ((matchPattern Mat.srcItemName pattern:"*.DISABLED")==true) then
					(
						Mat.srcItemName = substituteString Mat.srcItemName ".DISABLED" ""
						--Mat.srcItemName = Mat.srcItemName - ".DISABLED"
						--print "false"
					)
				)	
	)
	
	on BUTTON_ProcessMaterialNames pressed do
	(
		--ToDo include Rexbound materials in this
		
		Mat = medit.GetCurMtl()
		
		fn ProcessMaterial MatToProcess =
		(
			if(classof MatToProcess == XRefMaterial) then
			(
				MatToProcess.name = toUpper MatToProcess.srcItemName
			)
			if(classof MatToProcess == RexBoundMtl) then
			(
				MatToProcess.name = RexGetCollisionName MatToProcess
			)
		)
				
		for Mat in sceneMaterials do
		(
			if((classof Mat) == multimaterial ) then
				(
					NumSubMat = Mat.materialList.count
				
					for i = 1 to NumSubMat do
					(
						CurrentMat = Mat[Mat.materialIDList[i]]
						ProcessMaterial CurrentMat
						if(classOf CurrentMat == XRefMaterial)Or(classOf CurrentMat == Rage_Shader)or(classOf CurrentMat == RexBoundMtl) then
						(
						Mat.names[i] = CurrentMat.name	
						)
						else
						(
						Mat.names[i] = "INVALID"
						)
					)
				)
		)
	)

	on BUTTON_ApplySceneMaterial pressed do
	(
		if ( queryBox "This script will edit the material ID(s) of the selected object(s) to bring them inline with the 'Master' scene MultiMaterial. The objects will need to be collapsed in order to do this safely. Do you wish to continue?" title:"Warning" beep:true) then
		(
			filein "RSL_XRefMaterialTools_ApplySceneMaterial.ms"
		)
	)

)

clearListener()
createDialog RSL_XRefMaterialTools style:#(#style_toolwindow,#style_sysmenu)