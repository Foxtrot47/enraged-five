try (destroyDialog RSL_TextureTagMenu) catch()

rollout RSL_TextureTagMenu "R* Texture Tagger" width:130
(
	button btn_full "Update and edit" width:120
	button btn_edit "Edit" width:120
	
	on btn_full pressed do
	(
		fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSL_TextureTag.ms")
		
		destroyDialog RSL_TextureTagMenu
	)
	
	on btn_edit pressed do
	(
		fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSL_TextureTagEdit.ms")
		
		destroyDialog RSL_TextureTagMenu
	)
)

createDialog RSL_TextureTagMenu style:#(#style_toolwindow,#style_sysmenu)