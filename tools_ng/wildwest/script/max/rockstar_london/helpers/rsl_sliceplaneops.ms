

fn slicePlaneToVerts =
(
	local theObject = selection[1]
	
	if theObject != undefined then
	(
		if (classOf theObject) == editable_poly then
		(
			if theObject.selectedVerts.count == 3 then
			(
				local theCenter = [0,0,0]
				
				for vert in theObject.selectedVerts do
				(
					theCenter += vert.pos
				)
				
				theCenter /= theObject.selectedVerts.count
				
				local theNormal = normalize (cross (theObject.selectedVerts[1].pos - theObject.selectedVerts[2].pos) (theObject.selectedVerts[2].pos - theObject.selectedVerts[3].pos))
				
				polyop.setSlicePlane theObject (ray theCenter theNormal) 0.5
			)
			else
			(
				messageBox "You must pick three verts" title:"Error..."
			)
		)
		else
		(
			messageBox "The object is not an editable poly" title:"Error..."
		)
	)
	else
	(
		messageBox "You must pick one object" title:"Error..."
	)
)

fn slicePlaneToEdges =
(
	local theObject = selection[1]
	
	if theObject != undefined then
	(
		if (classOf theObject) == editable_poly then
		(
			if theObject.selectedEdges.count == 2 then
			(
				local theVerts = (polyOp.getVertsUsingEdge theObject theObject.selectedEdges) as array
				local theCenter = [0,0,0]
				
				for index in theVerts do
				(
					theCenter += theObject.verts[index].pos
				)
				
				theCenter /= theVerts.count
				
				local theNormal = normalize (cross (theObject.verts[theVerts[1]].pos - theObject.verts[theVerts[2]].pos) (theObject.verts[theVerts[2]].pos - theObject.verts[theVerts[3]].pos))
				
				polyop.setSlicePlane theObject (ray theCenter theNormal) 0.5
			)
			else
			(
				messageBox "You must pick two edges" title:"Error..."
			)
		)
		else
		(
			messageBox "The object is not an editable poly" title:"Error..."
		)
	)
	else
	(
		messageBox "You must pick one object" title:"Error..."
	)
)

fn slicePlaneToFace =
(
	local theObject = selection[1]
	
	if theObject != undefined then
	(
		if (classOf theObject) == editable_poly then
		(
			if theObject.selectedFaces.count == 1 then
			(
				polyop.setSlicePlane theObject (ray (polyop.getFaceCenter theObject theObject.selectedFaces[1].index) (polyop.getFaceNormal theObject theObject.selectedFaces[1].index)) 0.5
			)
			else
			(
				messageBox "You must pick one face" title:"Error..."
			)
		)
		else
		(
			messageBox "The object is not an editable poly" title:"Error..."
		)
	)
	else
	(
		messageBox "You must pick one object" title:"Error..."
	)
)