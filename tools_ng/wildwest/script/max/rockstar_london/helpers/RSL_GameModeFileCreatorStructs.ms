
-- perforce integration script
filein "pipeline/util/p4.ms"

struct RSL_GameMode
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- Struct variables
	---------------------------------------------------------------------------------------------------------------------------------------
	mode,
	saved = false,
	modified = false,
	hasObjective = true,
	hasDestroyed = false,
	filePath,
	deathmatchFile,
	objectiveFile,
	destroyedFile,
	deathmatchArray = #(),
	objectiveArray = #(),
	destroyedArray = #(),
	
	p4 = RsPerforce(), -- struct instance
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- toString()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn toString =
	(
		format "-=-=-=-=-=-=-=-= % =-=-=-=-=-=-=-=-\n" mode
		format "-= Saved:		%\n" saved
		format "-= Modified:	%\n" modified
		format "-= Objective:	%\n" hasObjective
		format "-= Destroyed:	%\n" hasDestroyed
		format "-= File Path:	%\n" filePath
		format "-= Obj File:	%\n" objectiveFile
		format "-= Dest File:	%\n" destroyedFile
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- setFiles()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn setFiles mapName =
	(
		deathmatchFile = mapName + ".max"
		objectiveFile = mapName + "_" + mode + "o.max"
		
		if hasDestroyed then
		(
			destroyedFile = mapName + "_" + mode + "d.max"
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- initialise()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn initialise mapName =
	(
		hasDestroyed = (mode == "bm")
		
		filePath = maxFilePath
		setFiles mapName
-- 		toString()
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- remove()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn remove modeObject =
	(
		local index = findItem objectiveArray modeObject
		
		if index > 0 then
		(
			deleteItem objectiveArray index
		)
		
		index = findItem destroyedArray modeObject
		
		if index > 0 then
		(
			deleteItem destroyedArray index
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- remove()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn save =
	(
		if (p4.connected()) then
		(
			p4.connect (project.root)
			
			if deathmatchArray.count == 0 or objectiveArray.count == 0 or destroyedArray.count == 0 then
			(
				messageBox "One or more of the object lists has no objects." title:"Error..."
			)
			else
			(
				local okToContinue = true
				local addDM = false
				local addObj = false
				local addDes = false
				
				if (doesFileExist (filePath + deathmatchFile)) then
				(
					p4.edit (p4.local2depot (filePath + deathmatchFile))
					
					if (getFileAttribute (filePath + deathmatchFile) #readOnly) then
					(
						messageBox ("The Deathmatch file cannot be written to because it is locked in perforce (checked out by someone else?).\n" + (filePath + objectiveFile)) title:"Error..."
						okToContinue = false
					)
					else
					(
						okToContinue = queryBox "The Deathmatch max file already exists. Are you sure you want to overwrite it?" title:"Warning..."
					)
				)
				else
				(
					addDM = true
				)
				
				if okToContinue then
				(
					if (doesFileExist (filePath + objectiveFile)) then
					(
						p4.edit (p4.local2depot (filePath + objectiveFile))
						
						if (getFileAttribute (filePath + objectiveFile) #readOnly) then
						(
							messageBox ("The Objective file cannot be written to because it is locked in perforce (checked out by someone else?).\n" + (filePath + objectiveFile)) title:"Error..."
							okToContinue = false
						)
						else
						(
							okToContinue = queryBox "The Objective max file already exists. Are you sure you want to overwrite it?" title:"Warning..."
						)
					)
					else
					(
						addObj = true
					)
				)
				
				if okToContinue then
				(
					if (doesFileExist (filePath + destroyedFile)) then
					(
						p4.edit (p4.local2depot (filePath + destroyedFile))
						
						if (getFileAttribute (filePath + destroyedFile) #readOnly) then
						(
							messageBox ("The Destroyed file cannot be written to because it is locked in perforce (checked out by someone else?) or write protected.\n" + (filePath + destroyedFile)) title:"Error..."
							okToContinue = false
						)
						else
						(
							okToContinue = queryBox "The Destroyed max file already exists. Are you sure you want to overwrite it?" title:"Warning..."
						)
					)
					else
					(
						addDes = true
					)
				)
				
				if okToContinue then
				(
					saveNodes (for modeObject in deathmatchArray collect modeObject.node) (filePath + deathmatchFile) quiet:true
					
					messageBox ("Deathmatch Max file Saved!\n" + (filePath + deathmatchFile)) title:"Success!"
					
					if addDM then
					(
						p4.add (p4.local2depot (filePath + deathmatchFile))
					)
					
					for modeObject in objectiveArray do modeObject.prepForSave()
					
					saveNodes (for modeObject in objectiveArray collect modeObject.node) (filePath + objectiveFile) quiet:true
					
					for modeObject in objectiveArray do modeObject.postSave()
					
					messageBox ("Object Max file Saved!\n" + (filePath + objectiveFile)) title:"Success!"
					
					if addObj then
					(
						p4.add (p4.local2depot (filePath + objectiveFile))
					)
					
					for modeObject in destroyedArray do modeObject.prepForSave()
					
					saveNodes (for modeObject in destroyedArray collect modeObject.node) (filePath + destroyedFile) quiet:true
					
					for modeObject in destroyedArray do modeObject.postSave()
					
					messageBox ("Destroyed Max file Saved!\n" + (filePath + destroyedFile)) title:"Success!"
					
					if addDes then 
					(
						p4.add (p4.local2depot (filePath + destroyedFile))
					)
					
					delete (for modeObject in deathmatchArray collect modeObject.node)
					
					messageBox "All files have been saved.\n MAKE SURE YOU SAVE THE MAP FILE!" title:"Warning..."
				)
				else
				(
					messageBox "The files have not been saved." title:"Warning..."
				)
			)
			
			p4.disconnect()
			
			saved = true
			modified = false
		)
		else
		(
			messageBox "You are not currently connected to perforce.\nLogin to perforce and then try again. The files have not been saved." title:"Error..."
		)
	)
)

struct RSL_gameModeObject
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- Struct variables
	---------------------------------------------------------------------------------------------------------------------------------------
	name,
	objectName,
	node,
	file,
	oldName,
	oldObjectName,
	oldNode,
	oldFile,
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- toString()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn toString =
	(
		format "-=-=-=-=-=-=-=-= % =-=-=-=-=-=-=-=-\n" name
		format "-= Object Name		:%\n" objectName
		format "-= Node				:%\n" node
		format "-= File				:%\n" file
		
		if oldNode != undefined then
		(
			format "-= Old Name			:%\n" oldName
			format "-= Old Object Name	:%\n" oldObjectName
			format "-= Old Node			:%\n" oldNode
			format "-= Old File			:%\n" oldFile
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- initialise()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn initialise =
	(
		if node != undefined then
		(
			name = node.name
			objectName = node.objectName
			file = node.filename
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- swap()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn swap newNode =
	(
		if oldNode == undefined then
		(
			oldName = name
			oldObjectName = objectName
			oldNode = node
			oldFile = file
		)
		
		name = newNode.name
		objectName = newNode.objectName
		node = newNode.node
		file = newNode.file
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- revert()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn revert =
	(
		name = oldName
		objectName = oldObjectName
		node = oldNode
		file = oldFile
		
		oldName = undefined
		oldObjectName = undefined
		oldNode = undefined
		oldFile = undefined
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- contains()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn contains object =
	(
		(node == object) or (oldNode == object)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- prepForSave()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn prepForSave =
	(
		if oldNode != undefined then
		(
			node = xrefs.addNewXRefObject file objectName
			node.transform = oldNode.transform
			
			name = uniqueName objectName
			node.name = name
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- prepForSave()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn postSave =
	(
		if oldNode != undefined then
		(
			delete node
			name = objectName
		)
	)
)

