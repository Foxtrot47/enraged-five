


struct RSL_MMPainterRandom
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- Struct variables
	---------------------------------------------------------------------------------------------------------------------------------------
	
	currentMode = #vertex,
	overwrite = false,
	
	elementArray = #(),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- UI variables
	---------------------------------------------------------------------------------------------------------------------------------------
	randomOptionsForm,
	optionsTableLayout,
	vertexMode,elementMode,
	overwriteCheckBox,
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- randomiseVerts()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn randomiseVerts vert object channelData strength colour =
	(
		local currentColor = meshop.getMapVert object channelData.channel vert.index
		local colourValue = random 0.0 colour
		
		if overwrite then
		(
			local colourValue = random 0.0 colour
			local finalColour = currentColor + (strength * ([colourValue,colourValue,colourValue] - currentColor))
			
			if channelData.r then currentColor.x = finalColour.x
			if channelData.g then currentColor.y = finalColour.y
			if channelData.b then currentColor.z = finalColour.z
		)
		else
		(
			local strengthValue = random 0.0 strength
			local finalColour = currentColor + (strengthValue * ([colour,colour,colour] - currentColor))
			
			if channelData.r then currentColor.x = finalColour.x
			if channelData.g then currentColor.y = finalColour.y
			if channelData.b then currentColor.z = finalColour.z
		)
		
		currentColor = RSL_Painter.clamp currentColor
		
		meshop.setMapVert object channelData.channel vert.index currentColor
		RSL_Painter.assignToDisplay vert.index
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- colourVert()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn colourVert vert object channelData strength colour =
	(
		local currentColor = meshop.getMapVert object channelData.channel vert
		
		local finalColour = currentColor + (strength * ([colour,colour,colour] - currentColor))
		
		if channelData.r then currentColor.x = finalColour.x
		if channelData.g then currentColor.y = finalColour.y
		if channelData.b then currentColor.z = finalColour.z
		
		currentColor = RSL_Painter.clamp currentColor
		
		meshOp.setMapVert object channelData.channel vert currentColor
		RSL_Painter.assignToDisplay vert
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- randomiseElement()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn randomiseElements elementArray object channelData strength colour =
	(
		for array in elementArray do 
		(
			if overwrite then
			(
				local colourValue = random 0.0 colour
				
				colourVert array object channelData strength colourValue
			)
			else
			(
				local strengthValue = random 0.0 strength
				
				colourVert array object channelData strengthValue colour
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- gatherElementArray()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn gatherElementArray object =
	(
		elementArray = #()
		
		local processedArray = #()
		local FaceArray = (meshOp.getFacesUsingVert object (for v in RSL_Painter.selectedArray collect v.index)) as array
		
		for face in FaceArray do 
		(
			local index = findItem processedArray face
			
			if index == 0 then
			(
				local faceElementArray = (meshop.getElementsUsingFace object face) as array
				local vertArray = (meshop.getVertsUsingFace object faceElementArray) as array
				
				append elementArray vertArray
				processedArray += faceElementArray
			)
		)
		
-- 		print elementArray
		
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- randomiseChannel()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn randomiseChannel object selectedArray channelData strength colour =
	(
		case currentMode of
		(
			#vertex:
			(
				undo "Randomise Verts" on
				(
					randomiseVerts selectedArray object channelData strength colour
				)
			)
			#element:
			(
				gatherElementArray object
				
				undo "Randomise Elements" on
				(
					randomiseElements elementArray object channelData strength colour
				)
			)
		)
		
		update object
	),
	
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- UI FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- disableAccelerators() Called when the user enters any control that needs keyboard input e.g. a textBox
	---------------------------------------------------------------------------------------------------------------------------------------
	fn disableAccelerators s e =
	(
		enableAccelerators = false
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOnFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOnFormClosing s e =
	(
		RSL_Painter.rand.formClosing s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- formClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn formClosing s e =
	(
		RSL_Painter.randomOptionsButton.checked = false
		
		setINISetting RSL_Painter.INIfile "Random" "Location" ([randomOptionsForm.location.x, randomOptionsForm.location.y] as string)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchRandomTypeCheck()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchRandomTypeCheck s e =
	(
		RSL_Painter.rand.randomTypeCheck s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- randomTypeCheck()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn randomTypeCheck s e =
	(
		s.checked = true
		
		case s.name of
		(
			"vertexMode":
			(
				elementMode.checked = false
			)
			"elementMode":
			(
				vertexMode.checked = false
			)
		)
		
		currentMode = s.tag.value
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOverwriteCheck()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOverwriteCheck s e =
	(
		RSL_Painter.rand.overwriteCheck s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- overwriteCheck()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn overwriteCheck s e =
	(
		overwrite = s.checked
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- optionsDialog()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn optionsDialog =
	(
		--In this case, we're going to use a standard form rather than the custom maxForm written for max. this is because I want to change the 
		-- form back colour which you can't do if you use the custom maxForm. For this to work, we need to set the parent of the form to be Max. 
		-- USING THIS TYPE OF FORM MEANS WE NEED TO DISSABLE ACCELERATORS WHEN WE FOCUS ON A CONTROL THAT TAKES
		-- KEY PRESSES SUCH AS SPINNERS AND TEXT BOXES
		
		--Get the max handle pointer.
		local maxHandlePointer=(Windows.GetMAXHWND())
		
		--Convert the HWND handle of Max to a dotNet system pointer
		local sysPointer = DotNetObject "System.IntPtr" maxHandlePointer
		
		--Create a dotNet wrapper containing the maxHWND
		local maxHwnd = DotNetObject "MaxCustomControls.Win32HandleWrapper" sysPointer
		
		randomOptionsForm = RS_dotNetUI.form "randomOptionsForm" text:" Random Options" size:[146, 88] borderStyle:RS_dotNetPreset.FB_FixedToolWindow
		randomOptionsForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		dotnet.addEventHandler randomOptionsForm "FormClosing" dispatchOnFormClosing
		dotnet.addEventHandler randomOptionsForm "Enter" disableAccelerators
		
		optionsTableLayout = RS_dotNetUI.tableLayout "optionsTableLayout" text:"optionsTableLayout" collumns:#((dataPair type:"Percent" value:50),(dataPair type:"Percent" value:50)) \
									rows:#((dataPair type:"Absolute" value:40),(dataPair type:"Absolute" value:24),(dataPair type:"Absolute" value:24)) dockStyle:RS_dotNetPreset.DS_Fill
		
		vertexMode = RS_dotNetUI.checkBox "vertexMode" text:(toUpper "Vertex") appearance:RS_dotNetPreset.AC_Button dockStyle:RS_dotNetPreset.DS_Fill
		vertexMode.margin = (RS_dotNetObject.paddingObject 0)
		vertexMode.font = RS_dotNetPreset.FontSmall--(dotNetObject "System.Drawing.Font" "Small Fonts" 8.0 RS_dotNetPreset.f_Regular)
		vertexMode.tag = dotNetMXSValue #vertex
		dotnet.addEventHandler vertexMode "click" dispatchRandomTypeCheck
		
		elementMode = RS_dotNetUI.checkBox "elementMode" text:(toUpper "Element") appearance:RS_dotNetPreset.AC_Button dockStyle:RS_dotNetPreset.DS_Fill
		elementMode.margin = (RS_dotNetObject.paddingObject 0)
		elementMode.font = RS_dotNetPreset.FontSmall--(dotNetObject "System.Drawing.Font" "Small Fonts" 8.0 RS_dotNetPreset.f_Regular)
		elementMode.tag = dotNetMXSValue #element
		dotnet.addEventHandler elementMode "click" dispatchRandomTypeCheck
		
		overwriteCheckBox = RS_dotNetUI.checkBox "overwriteCheckBox" text:(toUpper " Overwrite") dockStyle:RS_dotNetPreset.DS_Fill \
									textAlign:RS_dotNetPreset.CA_MiddleLeft margin:(RS_dotNetObject.paddingObject 3)
		overwriteCheckBox.margin = (RS_dotNetObject.paddingObject 0)
		overwriteCheckBox.font = RS_dotNetPreset.FontSmall--(dotNetObject "System.Drawing.Font" "Small Fonts" 8.0 RS_dotNetPreset.f_Regular)
		overwriteCheckBox.CheckAlign = (dotNetClass "System.Drawing.ContentAlignment").MiddleRight
		overwriteCheckBox.checked = overwrite
		dotnet.addEventHandler overwriteCheckBox "click" dispatchOverwriteCheck
		
		optionsTableLayout.controls.add vertexMode 0 0
		optionsTableLayout.controls.add elementMode 1 0
		optionsTableLayout.controls.add overwriteCheckBox 0 1
		
		optionsTableLayout.setColumnSpan overwriteCheckBox 2
		
		randomOptionsForm.controls.add optionsTableLayout
		
		vertexMode.checked = currentMode == #vertex
		elementMode.checked = currentMode == #element
		
		local INIlocation = getINISetting RSL_Painter.INIfile "Random" "Location"
		
		if INIlocation.count > 0 then
		(
			local loc = readValue (INIlocation as stringStream)
			randomOptionsForm.location.x = loc.x
			randomOptionsForm.location.y = loc.y
		)
		else
		(
			randomOptionsForm.location.x = RSL_Painter.painterForm.location.x
			randomOptionsForm.location.y = RSL_Painter.painterForm.location.x
		)
		
		randomOptionsForm.show maxHwnd
	)
)