--|----------------------------------------------------------------------------------------------------------
--|	File:		RSL_PES_Attributes
--|	Author:	Gavin Skinner @ Rockstar London
--|	Purpose:	Manages custom attributes for PES objects
--|----------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------
function pes_RemoveAllAttributes obj = (
	if obj == undefined then
		return false
	local i = (custAttributes.count obj)
	while i > 0 do (
		custAttributes.delete obj i
		i -= 1
	)
)
------------------------------------------------------------------------------------------------------------
global pesAttributes = attributes "PES"
(
	parameters main
	(
		-- Generic
		pesType							type:#string default:"No Type"
		-- Level Info
		levelInfo_Name				type:#string default:""
		levelInfo_CreationType		type:#string default:"Player Spawn"
		levelInfo_CPsOrder			type:#string default:""
		levelInfo_ObjPrefix			type:#string default:""
		levelInfo_ObjPostfix			type:#string default:""
		levelInfo_LevelType			type:#string default:"Single Player/Coop"
		-- Trigger
		trigger_Type					type:#string default:"BOX"
		trigger_HelperType			type:#string default:"BOX_TRIGGER"
		trigger_StartDisabled		type:#boolean default:false
		trigger_LogShootEvents	type:#boolean default:false
		trigger_LookSpline			type:#string default:""
		trigger_SEV_PlyEntered		type:#boolean default:false
		trigger_SEV_PlyLeft			type:#boolean default:false
		trigger_SEV_PlyLookStart	type:#boolean default:false
		trigger_SEV_PlyLookEnd	type:#boolean default:false
		-- NPC Spawn Point
		NPC_IsEnemy					type:#boolean default:true
		NPC_BlockTempEvents		type:#boolean default:false
		NPC_AttachBulletCam		type:#boolean default:false
		NPC_RespawnAfterDeath	type:#boolean default:false
		NPC_OverrideDeleteCorpse	type:#boolean default:false
		NPC_StormPlayer				type:#boolean default:false
		NPC_SpawnInMP				type:#boolean default:true
		NPC_SpawnInSP				type:#boolean default:true
		NPC_RelationshipGroup		type:#string default:"GANG1"
		NPC_CombatRange			type:#string default:"MEDIUM"
		NPC_CombatAbility			type:#string default:"AVERAGE"
		NPC_CombatMovement		type:#string default:"DEFENSIVE"
		NPC_WeaponType			type:#string default:"RIFLE_AK47"
		NPC_ModelName				type:#string default:"g_cs_uc_avg"
		NPC_AttachedTrigger		type:#string default:""
		NPC_AttachedMILO			type:#string default:""
		NPC_SpawnDestination		type:#string default:""
		NPC_InitialTether				type:#string default:""
		NPC_SpawnDelay				type:#integer default:0
		NPC_StartingHealth			type:#integer default:-1
		NPC_DeleteCorpseDelay		type:#integer default:25000
		NPC_WeaponAddon			type:#string default:""
		NPC_Reinforcement			type:#string default:""
		NPC_MinSpawnDistance		type:#integer default:0
		NPC_WalkMove				type:#boolean default:false
		NPC_SafetyTeleport			type:#boolean default:false
		-- Pickups
		pickup_Type					type:#string default:"HEALTH_STANDARD"
		pickup_Fixed					type:#boolean default:true
		pickup_Regenerates			type:#boolean default:false
		-- Vehicles
		vehicle_Type					type:#string default:"PH_CAR"
		vehicle_Passenger1			type:#string default:""
		vehicle_Passenger2			type:#string default:""
		vehicle_Passenger3			type:#string default:""
		vehicle_Passenger4			type:#string default:""
		-- Checkpoints
		CP_Name						type:#string default:""
		CP_ObjectiveString			type:#string default:""
		CP_SpawnPlayer1				type:#string default:""
		CP_SpawnPlayer2				type:#string default:""
		CP_SpawnMILO1				type:#string default:""
		CP_SpawnMILO2				type:#string default:""
		-- Groups
		group_Members				type:#string default:""
		group_BulletCam				type:#boolean default:false
		group_TriggerNoSpawn		type:#string default:""
		group_TriggerGpNoSpawn	type:#string default:""
		-- SpawnPoint Groups
		spGroup_Members			type:#string default:""
		-- Trigger Groups
		trigGroup_Members			type:#string default:""
		-- Territories
		territory_Name				type:#string default:""
		territory_TriggerGroup		type:#string default:""
		territory_BlipSprite			type:#string default:"RADAR_TRACE_OBJECTIVE"
		-- Location & GW*
		GW_associatedLocation		type:#string default:""
		GW_Name						type:#string default:""
		-- Doors
		door_MinAngle					type:#float default:-81
		door_MaxAngle				type:#float default:81
		door_StartOpen				type:#boolean default:false
		door_AutoClose				type:#boolean default:true
		door_Locked					type:#boolean default:false
		door_IsArtSide					type:#boolean default:true
		-- Gen
		pes_order						type:#integer default:-1
		scriptResource					type:#string default:""
		associatedCheckpoint		type:#string default:""
		associatedTrigger				type:#string default:""
		associatedMILO				type:#string default:""
		modelName						type:#string default:""
		SP_headingDiv					type:#float default:0
	)
)
------------------------------------------------------------------------------------------------------------
function pes_ValidateAttributes obj = (
	local needsUpdating = false
	-- Default settings
	pesType							= "No Type"
	-- Level Info
	levelInfo_Name				= ""
	levelInfo_CreationType		= "Player Spawn"
	levelInfo_CPsOrder			= ""
	levelInfo_ObjPrefix			= ""
	levelInfo_ObjPostfix			= ""
	levelInfo_LevelType			= "Single Player/Coop"
	-- Trigger
	trigger_Type					= "BOX"
	trigger_HelperType			= "BOX_TRIGGER"
	trigger_StartDisabled		= false
	trigger_LogShootEvents 	= false
	trigger_LookSpline			= ""
	trigger_SEV_PlyEntered		= false
	trigger_SEV_PlyLeft			= false
	trigger_SEV_PlyLookStart	= false
	trigger_SEV_PlyLookEnd	= false
	-- NPC Spawn Point
	NPC_IsEnemy					= true
	NPC_BlockTempEvents		= false
	NPC_AttachBulletCam		= false
	NPC_RespawnAfterDeath	= false
	NPC_OverrideDeleteCorpse	= false
	NPC_StormPlayer				= false
	NPC_SpawnInMP				= true
	NPC_SpawnInSP				= true
	NPC_RelationshipGroup		= "COP"
	NPC_CombatRange			= "MEDIUM"
	NPC_CombatAbility			= "AVERAGE"
	NPC_CombatMovement		= "DEFENSIVE"
	NPC_WeaponType			= "RIFLE_AK47"
	NPC_ModelName				= "g_cs_uc_avg"
	NPC_AttachedTrigger		= ""
	NPC_AttachedMILO			= ""
	NPC_SpawnDestination		= ""
	NPC_InitialTether				= ""
	NPC_SpawnDelay				= 0
	NPC_StartingHealth			= -1
	NPC_DeleteCorpseDelay		= 25000
	NPC_WeaponAddon			= ""
	NPC_Reinforcement			= ""
	NPC_MinSpawnDistance		= 0
	NPC_WalkMove				= false
	NPC_SafetyTeleport			= false
	-- Pickups
	pickup_Type					= "HEALTH_STANDARD"
	pickup_Fixed					= true
	pickup_Regenerates			= false
	-- Vehicles
	vehicle_Type					= "PH_CAR"
	vehicle_Passenger1			= ""
	vehicle_Passenger2			= ""
	vehicle_Passenger3			= ""
	vehicle_Passenger4			= ""
	-- Checkpoints
	CP_Name						= ""
	CP_ObjectiveString			= ""
	CP_SpawnPlayer1				= ""
	CP_SpawnPlayer2				= ""
	CP_SpawnMILO1				= ""
	CP_SpawnMILO2				= ""
	-- Groups
	group_Members				= ""
	group_BulletCam				= false
	group_TriggerNoSpawn		= ""
	group_TriggerGpNoSpawn	= ""
	-- SpawnPoint Groups
	spGroup_Members			= ""
	-- Trigger Groups
	trigGroup_Members			= ""
	-- Territories
	territory_Name				= ""
	territory_TriggerGroup		= ""
	territory_BlipSprite			= "RADAR_TRACE_OBJECTIVE"
	-- Location
	GW_associatedLocation		= ""
	GW_Name						= ""
	-- Doors
	door_IsArtSide					= true
	door_MinAngle					= -81
	door_MaxAngle				= 81
	door_StartOpen				= false
	door_AutoClose				= true
	door_Locked					= false
	-- Gen
	pes_order						= 0
	scriptResource					= ""
	associatedCheckpoint		= ""
	associatedTrigger				= ""
	associatedMILO				= ""
	modelName						= ""
	SP_headingDiv					= 0
	-- Try to load settings
	try (pesType = obj.pesType) catch (needsUpdating = true)
	try (levelInfo_Name = obj.levelInfo_Name) catch (needsUpdating = true)
	try (levelInfo_CreationType = obj.levelInfo_CreationType) catch (needsUpdating = true)
	try (levelInfo_CPsOrder = obj.levelInfo_CPsOrder) catch (needsUpdating = true)
	try (levelInfo_ObjPrefix = obj.levelInfo_ObjPrefix) catch (needsUpdating = true)
	try (levelInfo_ObjPostfix = obj.levelInfo_ObjPostfix) catch (needsUpdating = true)
	try (levelInfo_LevelType = obj.levelInfo_LevelType) catch (needsUpdating = true)
	-- Trigger
	try (trigger_Type = obj.trigger_Type) catch (needsUpdating = true)
	try (trigger_HelperType = obj.trigger_HelperType) catch (needsUpdating = true)
	try (trigger_StartDisabled = obj.trigger_StartDisabled) catch (needsUpdating = true)
	try (trigger_LogShootEvents = obj.trigger_LogShootEvents) catch (needsUpdating = true)
	try (trigger_LookSpline = obj.trigger_LookSpline) catch (needsUpdating = true)
	try (trigger_SEV_PlyEntered = obj.trigger_SEV_PlyEntered) catch (needsUpdating = true)
	try (trigger_SEV_PlyLeft = obj.trigger_SEV_PlyLeft) catch (needsUpdating = true)
	try (trigger_SEV_PlyLookStart = obj.trigger_SEV_PlyLookStart) catch (needsUpdating = true)
	try (trigger_SEV_PlyLookEnd = obj.trigger_SEV_PlyLookEnd) catch (needsUpdating = true)
	-- NPC Spawn Point
	try (NPC_IsEnemy = obj.NPC_IsEnemy) catch (needsUpdating = true)
	try (NPC_BlockTempEvents = obj.NPC_BlockTempEvents) catch (needsUpdating = true)
	try (NPC_AttachBulletCam = obj.NPC_AttachBulletCam) catch (needsUpdating = true)
	try (NPC_RespawnAfterDeath = obj.NPC_RespawnAfterDeath) catch (needsUpdating = true)
	try (NPC_OverrideDeleteCorpse = obj.NPC_OverrideDeleteCorpse) catch (needsUpdating = true)
	try (NPC_RelationshipGroup = obj.NPC_RelationshipGroup) catch (needsUpdating = true)
	try (NPC_CombatRange = obj.NPC_CombatRange) catch (needsUpdating = true)
	try (NPC_CombatAbility = obj.NPC_CombatAbility) catch (needsUpdating = true)
	try (NPC_CombatMovement = obj.NPC_CombatMovement) catch (needsUpdating = true)
	try (NPC_WeaponType = obj.NPC_WeaponType) catch (needsUpdating = true)
	try (NPC_ModelName = obj.NPC_ModelName) catch (needsUpdating = true)
	try (NPC_AttachedTrigger = obj.NPC_AttachedTrigger) catch (needsUpdating = true)
	try (NPC_AttachedMILO = obj.NPC_AttachedMILO) catch (needsUpdating = true)
	try (NPC_SpawnDestination = obj.NPC_SpawnDestination) catch (needsUpdating = true)
	try (NPC_InitialTether = obj.NPC_InitialTether) catch (needsUpdating = true)
	try (NPC_StormPlayer = obj.NPC_StormPlayer) catch (needsUpdating = true)
	try (NPC_SpawnInMP = obj.NPC_SpawnInMP) catch (needsUpdating = true)
	try (NPC_SpawnInSP = obj.NPC_SpawnInSP) catch (needsUpdating = true)
	try (NPC_SpawnDelay = obj.NPC_SpawnDelay) catch (needsUpdating = true)
	try (NPC_StartingHealth = obj.NPC_StartingHealth) catch (needsUpdating = true)
	try (NPC_DeleteCorpseDelay = obj.NPC_DeleteCorpseDelay) catch (needsUpdating = true)
	try (NPC_WeaponAddon = obj.NPC_WeaponAddon) catch (needsUpdating = true)
	try (NPC_Reinforcement = obj.NPC_Reinforcement) catch (needsUpdating = true)
	try (NPC_MinSpawnDistance = obj.NPC_MinSpawnDistance) catch (needsUpdating = true)
	try (NPC_WalkMove = obj.NPC_WalkMove) catch (needsUpdating = true)
	try (NPC_SafetyTeleport = obj.NPC_SafetyTeleport) catch (needsUpdating = true)
	-- Pickups
	try (pickup_Type = obj.pickup_Type) catch (needsUpdating = true)
	try (pickup_Fixed = obj.pickup_Fixed) catch (needsUpdating = true)
	try (pickup_Regenerates = obj.pickup_Regenerates) catch (needsUpdating = true)
	-- Vehicles
	try (vehicle_Type = obj.vehicle_Type) catch (needsUpdating = true)
	try (vehicle_Passenger1 = obj.vehicle_Passenger1) catch (needsUpdating = true)
	try (vehicle_Passenger2 = obj.vehicle_Passenger2) catch (needsUpdating = true)
	try (vehicle_Passenger3 = obj.vehicle_Passenger3) catch (needsUpdating = true)
	try (vehicle_Passenger4 = obj.vehicle_Passenger4) catch (needsUpdating = true)
	-- Checkpoints
	try (CP_Name = obj.CP_Name) catch (needsUpdating = true)
	try (CP_SpawnPlayer1 = obj.CP_SpawnPlayer1) catch (needsUpdating = true)
	try (CP_SpawnPlayer2 = obj.CP_SpawnPlayer2) catch (needsUpdating = true)
	try (CP_SpawnMILO1 = obj.CP_SpawnMILO1) catch (needsUpdating = true)
	try (CP_SpawnMILO2 = obj.CP_SpawnMILO2) catch (needsUpdating = true)
	try (CP_ObjectiveString = obj.CP_ObjectiveString) catch (needsUpdating = true)
	-- Groups
	try (group_Members = obj.group_Members) catch (needsUpdating = true)
	try (group_BulletCam = obj.group_BulletCam) catch (needsUpdating = true)
	try (group_TriggerNoSpawn = obj.group_TriggerNoSpawn) catch (needsUpdating = true)
	try (group_TriggerGpNoSpawn = obj.group_TriggerGpNoSpawn) catch (needsUpdating = true)
	-- SpawnPoint Groups
	try (spGroup_Members = obj.spGroup_Members) catch (needsUpdating = true)
	-- Trigger Groups
	try (trigGroup_Members = obj.trigGroup_Members) catch (needsUpdating = true)
	-- Territories
	try (territory_Name = obj.territory_Name) catch (needsUpdating = true)
	try (territory_TriggerGroup = obj.territory_TriggerGroup) catch (needsUpdating = true)
	try (territory_BlipSprite = obj.territory_BlipSprite) catch (needsUpdating = true)
	-- Location
	try (GW_associatedLocation = obj.GW_associatedLocation) catch (needsUpdating = true)
	try (GW_name = obj.GW_name) catch (needsUpdating = true)
	-- Doors
	try (door_MinAngle = obj.door_MinAngle) catch (needsUpdating = true)
	try (door_MaxAngle = obj.door_MaxAngle) catch (needsUpdating = true)
	try (door_StartOpen = obj.door_StartOpen) catch (needsUpdating = true)
	try (door_AutoClose = obj.door_AutoClose) catch (needsUpdating = true)
	try (door_Locked = obj.door_Locked) catch (needsUpdating = true)
	try (door_IsArtSide = obj.door_IsArtSide) catch (needsUpdating = true)
	-- Gen
	try (pes_order = obj.pes_order) catch (needsUpdating = true)
	try (scriptResource = obj.scriptResource) catch (needsUpdating = true)
	try (associatedCheckpoint = obj.associatedCheckpoint) catch (needsUpdating = true)
	try (associatedTrigger = obj.associatedTrigger) catch (needsUpdating = true)
	try (associatedMILO = obj.associatedMILO) catch (needsUpdating = true)
	try (modelName = obj.modelName) catch (needsUpdating = true)
	try (SP_headingDiv = obj.SP_headingDiv) catch (needsUpdating = true)
	-- Somethings was out of date, so re-attribute
	if needsUpdating then (
		format "** Updating attributes for %\n" obj.name
		pes_RemoveAllAttributes obj
		custAttributes.add obj pesAttributes #unique
		-- Restore saved settings
		obj.pesType = pesType
		obj.levelInfo_Name = levelInfo_Name
		obj.levelInfo_CreationType = levelInfo_CreationType
		obj.levelInfo_CPsOrder = levelInfo_CPsOrder
		obj.levelInfo_ObjPrefix = levelInfo_ObjPrefix
		obj.levelInfo_ObjPostfix = levelInfo_ObjPostfix
		obj.levelInfo_LevelType = levelInfo_LevelType
		-- Trigger
		obj.trigger_Type = trigger_Type
		obj.trigger_HelperType = trigger_HelperType
		obj.trigger_StartDisabled = trigger_StartDisabled
		obj.trigger_LogShootEvents = trigger_LogShootEvents
		obj.trigger_LookSpline = trigger_LookSpline
		obj.trigger_SEV_PlyEntered = trigger_SEV_PlyEntered
		obj.trigger_SEV_PlyLeft = trigger_SEV_PlyLeft
		obj.trigger_SEV_PlyLookStart = trigger_SEV_PlyLookStart
		obj.trigger_SEV_PlyLookEnd = trigger_SEV_PlyLookEnd
		-- NPC Spawn Point
		obj.NPC_IsEnemy = NPC_IsEnemy
		obj.NPC_BlockTempEvents = NPC_BlockTempEvents
		obj.NPC_AttachBulletCam = NPC_AttachBulletCam
		obj.NPC_RespawnAfterDeath = NPC_RespawnAfterDeath
		obj.NPC_OverrideDeleteCorpse = NPC_OverrideDeleteCorpse
		obj.NPC_RelationshipGroup = NPC_RelationshipGroup
		obj.NPC_CombatRange = NPC_CombatRange
		obj.NPC_CombatAbility = NPC_CombatAbility
		obj.NPC_CombatMovement = NPC_CombatMovement
		obj.NPC_WeaponType = NPC_WeaponType
		obj.NPC_ModelName = NPC_ModelName
		obj.NPC_AttachedTrigger = NPC_AttachedTrigger
		obj.NPC_AttachedMILO = NPC_AttachedMILO
		obj.NPC_SpawnDestination = NPC_SpawnDestination
		obj.NPC_InitialTether = NPC_InitialTether
		obj.NPC_StormPlayer = NPC_StormPlayer
		obj.NPC_SpawnInMP = NPC_SpawnInMP
		obj.NPC_SpawnInSP = NPC_SpawnInSP
		obj.NPC_SpawnDelay = NPC_SpawnDelay
		obj.NPC_StartingHealth = NPC_StartingHealth
		obj.NPC_DeleteCorpseDelay = NPC_DeleteCorpseDelay
		obj.NPC_WeaponAddon = NPC_WeaponAddon
		obj.NPC_Reinforcement = NPC_Reinforcement
		obj.NPC_MinSpawnDistance = NPC_MinSpawnDistance
		obj.NPC_WalkMove = NPC_WalkMove
		obj.NPC_SafetyTeleport = NPC_SafetyTeleport
		-- Pickups
		obj.pickup_Type = pickup_Type
		obj.pickup_Fixed = pickup_Fixed
		obj.pickup_Regenerates = pickup_Regenerates
		-- Vehicles
		obj.vehicle_Type = vehicle_Type
		obj.vehicle_Passenger1 = vehicle_Passenger1
		obj.vehicle_Passenger2 = vehicle_Passenger2
		obj.vehicle_Passenger3 = vehicle_Passenger3
		obj.vehicle_Passenger4 = vehicle_Passenger4
		-- Checkpoints
		obj.CP_Name = CP_Name
		obj.CP_SpawnPlayer1 = CP_SpawnPlayer1
		obj.CP_SpawnPlayer2 = CP_SpawnPlayer2
		obj.CP_SpawnMILO1 = CP_SpawnMILO1
		obj.CP_SpawnMILO2 = CP_SpawnMILO2
		obj.CP_ObjectiveString = CP_ObjectiveString
		-- Groups
		obj.group_Members = group_Members
		obj.group_BulletCam = group_BulletCam
		obj.group_TriggerNoSpawn = group_TriggerNoSpawn
		obj.group_TriggerGpNoSpawn = group_TriggerGpNoSpawn
		-- SpawnPoint Groups
		obj.spGroup_Members = spGroup_Members
		-- Trigger Groups
		obj.trigGroup_Members = trigGroup_Members
		-- Territories
		obj.territory_Name = territory_Name
		obj.territory_TriggerGroup = territory_TriggerGroup
		obj.territory_BlipSprite = territory_BlipSprite
		-- Location
		obj.GW_associatedLocation = GW_associatedLocation
		obj.GW_name = GW_name
		-- Doors
		obj.door_MinAngle = door_MinAngle
		obj.door_MaxAngle = door_MaxAngle
		obj.door_StartOpen = door_StartOpen
		obj.door_AutoClose = door_AutoClose
		obj.door_Locked = door_Locked
		obj.door_IsArtSide = door_IsArtSide
		-- Gen
		obj.pes_order = pes_order
		obj.scriptResource = scriptResource
		obj.associatedCheckpoint = associatedCheckpoint
		obj.associatedTrigger = associatedTrigger
		obj.associatedMILO = associatedMILO
		obj.modelName = modelName
		obj.SP_headingDiv = SP_headingDiv
	) else format "** Attributes up to date for %\n" obj.name
)
------------------------------------------------------------------------------------------------------------
function pes_IsTaggedAsPES obj = (
	if obj == undefined then
		return false
	if (custAttributes.count obj) == 0 then
		return false
	for i = 1 to (custAttributes.count obj) do (
		if (custAttributes.get obj i).name == "PES" then
			return true
	)
	return false
)
------------------------------------------------------------------------------------------------------------
function pes_TagAsPES obj = (
	custAttributes.add obj pesAttributes #unique
	pes_ValidateAttributes obj
)
------------------------------------------------------------------------------------------------------------
function pes_CopyAttributes source dest = (
	pes_RemoveAllAttributes dest
	pes_TagAsPES dest
	-- Restore saved settings
	dest.pesType = source.pesType
	dest.levelInfo_Name = source.levelInfo_Name
	dest.levelInfo_CreationType = source.levelInfo_CreationType
	dest.levelInfo_CPsOrder = source.levelInfo_CPsOrder
	dest.levelInfo_ObjPrefix = source.levelInfo_ObjPrefix
	dest.levelInfo_ObjPostfix = source.levelInfo_ObjPostfix
	dest.levelInfo_LevelType = source.levelInfo_LevelType
	-- Trigger
	dest.trigger_Type = source.trigger_Type
	dest.trigger_HelperType = source.trigger_HelperType
	dest.trigger_StartDisabled = source.trigger_StartDisabled
	dest.trigger_LogShootEvents = source.trigger_LogShootEvents
	dest.trigger_LookSpline = source.trigger_LookSpline
	dest.trigger_SEV_PlyEntered = source.trigger_SEV_PlyEntered
	dest.trigger_SEV_PlyLeft = source.trigger_SEV_PlyLeft
	dest.trigger_SEV_PlyLookStart = source.trigger_SEV_PlyLookStart
	dest.trigger_SEV_PlyLookEnd = source.trigger_SEV_PlyLookEnd
	-- NPC Spawn Point
	dest.NPC_IsEnemy = source.NPC_IsEnemy
	dest.NPC_BlockTempEvents = source.NPC_BlockTempEvents
	dest.NPC_AttachBulletCam = source.NPC_AttachBulletCam
	dest.NPC_RespawnAfterDeath = source.NPC_RespawnAfterDeath
	dest.NPC_OverrideDeleteCorpse = source.NPC_OverrideDeleteCorpse
	dest.NPC_RelationshipGroup = source.NPC_RelationshipGroup
	dest.NPC_CombatRange = source.NPC_CombatRange
	dest.NPC_CombatAbility = source.NPC_CombatAbility
	dest.NPC_CombatMovement = source.NPC_CombatMovement
	dest.NPC_WeaponType = source.NPC_WeaponType
	dest.NPC_ModelName = source.NPC_ModelName
	dest.NPC_AttachedTrigger = source.NPC_AttachedTrigger
	dest.NPC_AttachedMILO = source.NPC_AttachedMILO
	dest.NPC_SpawnDestination = source.NPC_SpawnDestination
	dest.NPC_InitialTether = source.NPC_InitialTether
	dest.NPC_StormPlayer = source.NPC_StormPlayer
	dest.NPC_SpawnInMP = source.NPC_SpawnInMP
	dest.NPC_SpawnInSP = source.NPC_SpawnInSP
	dest.NPC_SpawnDelay = source.NPC_SpawnDelay
	dest.NPC_StartingHealth = source.NPC_StartingHealth
	dest.NPC_DeleteCorpseDelay = source.DeleteCorpseDelay
	dest.NPC_WeaponAddon = source.NPC_WeaponAddon
	dest.NPC_Reinforcement = source.NPC_Reinforcement
	dest.NPC_MinSpawnDistance = source.NPC_MinSpawnDistance
	dest.NPC_WalkMove = source.NPC_WalkMove
	dest.NPC_SafetyTeleport = source.NPC_SafetyTeleport
	-- Pickups
	dest.pickup_Type = source.pickup_Type
	dest.pickup_Fixed = source.pickup_Fixed
	dest.pickup_Regenerates = source.pickup_Regenerates
	-- Vehicles
	dest.vehicle_Type = source.vehicle_Type
	dest.vehicle_Passenger1 = source.vehicle_Passenger1
	dest.vehicle_Passenger2 = source.vehicle_Passenger2
	dest.vehicle_Passenger3 = source.vehicle_Passenger3
	dest.vehicle_Passenger4 = source.vehicle_Passenger4
	-- Checkpoints
	dest.CP_Name = source.CP_Name
	dest.CP_SpawnPlayer1 = source.CP_SpawnPlayer1
	dest.CP_SpawnPlayer2 = source.CP_SpawnPlayer2
	dest.CP_SpawnMILO1 = source.CP_SpawnMILO1
	dest.CP_SpawnMILO2 = source.CP_SpawnMILO2
	dest.CP_ObjectiveString = source.CP_ObjectiveString
	-- Groups
	dest.group_Members = source.group_Members
	dest.group_BulletCam = source.group_BulletCam
	dest.group_TriggerNoSpawn = source.group_TriggerNoSpawn
	dest.group_TriggerGpNoSpawn = source.group_TriggerGpNoSpawn
	-- SpawnPoint Groups
	dest.spGroup_Members = source.spGroup_Members
	-- Trigger Groups
	dest.trigGroup_Members = source.trigGroup_Members
	-- Territories
	dest.territory_Name = source.territory_Name
	dest.territory_TriggerGroup = source.territory_TriggerGroup
	dest.territory_BlipSprite = source.territory_BlipSprite
	-- Location
	dest.GW_associatedLocation = source.GW_associatedLocation
	dest.GW_name = source.GW_name
	-- Doors
	dest.door_MinAngle = source.door_MinAngle
	dest.door_MaxAngle = source.door_MaxAngle
	dest.door_StartOpen = source.door_StartOpen
	dest.door_AutoClose = source.door_AutoClose
	dest.door_Locked = source.door_Locked
	dest.door_IsArtSide = source.door_IsArtSide
	-- Gen
	dest.pes_order = source.pes_order
	dest.scriptResource = source.scriptResource
	dest.associatedCheckpoint = source.associatedCheckpoint
	dest.associatedTrigger = source.associatedTrigger
	dest.associatedMILO = source.associatedMILO
	dest.modelName = source.modelName
)
