--|----------------------------------------------------------------------------------------------------------
--|	File:		RSL_PES_Utils
--|	Author:	Gavin Skinner @ Rockstar London
--|	Purpose:	Common functions and values for the PES system
--|----------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------
function pes_GetObject str_name = (
	if str_name.count > 0 then (
		for obj in rootnode.children do (
			if pes_IsTaggedAsPES obj then (
				if obj.name == str_name then return obj
			)
		)
		return undefined
	)
	return undefined
)
------------------------------------------------------------------------------------------------------------
function pes_MakeArrayFromString instr = (
	local arr = #()
	if instr.count <= 0 or instr == ";" or instr == "" then (
	) else (
		local count = 1
		while count < instr.count do (
			local outstr = ""
			while instr[count] != ";" do (
				outstr = append outstr instr[count]
				count = count + 1
			)
			append arr outstr
			count = count + 1
		)
	)
	return arr
)
------------------------------------------------------------------------------------------------------------
function pes_MakeStringFromArray inarr = (
	local outstr = ""
	for str in inarr do (
		append outstr str
		append outstr ";"
	)
	return outstr
)
------------------------------------------------------------------------------------------------------------
function pes_DDL_GetSelectedIndex instr inarr = (
	local count = 1
	for str in inarr do (
		if str == instr then return count
		count += 1
	)
	return 0
)
------------------------------------------------------------------------------------------------------------
function pes_DDL_SetSelected ddl instr inarr = (
	local i = pes_DDL_GetSelectedIndex instr inarr
	if i > 0 then ddl.selection = i
)
------------------------------------------------------------------------------------------------------------
function pes_SetStandardColour obj = (
	if obj.material == undefined then obj.material = standardmaterial()
	local colour = blue
	local opacity = 100
	if obj.pesType == "Player Spawn" then colour = green
	else if obj.pesType == "Checkpoint" then colour = red
	else if obj.pesType == "NPC" then (
		colour = red
		if obj.NPC_RelationshipGroup == "GANG1" then colour = [180, 0, 0]
		if obj.NPC_RelationshipGroup == "GANG2" then colour = [80, 0, 0]
		if obj.NPC_RelationshipGroup == "MISSION1" then colour = [0, 80, 0]
		if obj.NPC_RelationshipGroup == "MISSION2" then colour = brown
	)
	else if obj.pesType == "Pickup" then colour = green
	else if obj.pesType == "Standard Door" then colour = brown
	else if obj.pesType == "Move Destination" then colour = white
	else if obj.pesType == "Vehicle" then colour = green
	else if obj.pesType == "Group" then colour = color 255 0 255
	else if obj.pesType == "Spawn Group" then colour = red
	else if obj.pesType == "Trigger Group" then colour = color 20 250 220
	else if obj.pesType == "Territory" then colour = orange
	else if obj.pesType == "Location" then colour = brown
	else if obj.pesType == "GW Bomb" then colour = orange
	else if obj.pesType == "GW Pickup" then colour = orange
	else if obj.pesType == "GW Escape Point" then colour = orange
	else if obj.pesType == "DM Spawn Beacon" then colour = green
	else if obj.pesType == "Trigger" then
	(
		if obj.trigger_HelperType == "BOX_TETHER" then colour = orange
		else if obj.trigger_HelperType == "BOX_FLEEZONE" then colour = green
		else colour = blue
		--opacity = 40
	)
	obj.material.diffuse = colour
	if PES.cbx_UseTransparency.state == true then
		obj.material.opacity = opacity
	else
		obj.material.opacity = 100
)
------------------------------------------------------------------------------------------------------------
function pes_SetObjectProperties obj = (
	if obj.material != undefined then obj.material = undefined
	obj.material = standardmaterial()
	obj.wirecolor = color 50 50 50
	pes_SetStandardColour obj
)
------------------------------------------------------------------------------------------------------------
-- Helper function for colourising dependents
function pes_AddDependentsToArray arr obj = (
	if obj.pesType == "Checkpoint" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if check.associatedCheckpoint == obj.name then (
					append arr check
					pes_AddDependentsToArray arr check
				)
			)
		)
	)
	if obj.pesType == "Trigger" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if check.NPC_AttachedTrigger == obj.name then append arr check
				if check.NPC_InitialTether == obj.name then append arr check
				if check.associatedTrigger == obj.name then append arr check
				if check.group_TriggerNoSpawn == obj.name then append arr check
			)
		)
	)
	else if obj.pesType == "Move Destination" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if check.NPC_SpawnDestination == obj.name then append arr check
			)
		)
	)
	else if obj.pesType == "Territory" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if obj.GW_associatedLocation == check.name then append arr check
				if obj.territory_TriggerGroup == check.name then append arr check
			)
		)
	)
	else if obj.pesType == "Location" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if check.GW_associatedLocation == obj.name then append arr check
				-- TODO - any/all highlighting here, and in associated sections
			)
		)
	)
	else if obj.pesType == "GW Bomb" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if obj.GW_associatedLocation == check.name then append arr check
			)
		)
	)
	else if obj.pesType == "GW Pickup" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if obj.GW_associatedLocation == check.name then append arr check
			)
		)
	)
	else if obj.pesType == "GW Escape Point" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if obj.GW_associatedLocation == check.name then append arr check
			)
		)
	)
	else if obj.pesType == "Player Spawn" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if obj.associatedCheckpoint == check.name then append arr check
			)
		)
	)
	else if obj.pesType == "NPC" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if obj.NPC_AttachedTrigger == check.name then append arr check
				if obj.NPC_AttachedMILO == check.name then append arr check
				if obj.associatedCheckpoint == check.name then append arr check
				if obj.NPC_SpawnDestination == check.name then append arr check
				if obj.NPC_InitialTether == check.name then append arr check
				if obj.NPC_Reinforcement == check.name then append arr check
			)
		)
	)
	else if obj.pesType == "Vehicle" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if obj.vehicle_Passenger1 == check.name then append arr check
				if obj.vehicle_Passenger2 == check.name then append arr check
				if obj.vehicle_Passenger3 == check.name then append arr check
				if obj.vehicle_Passenger4 == check.name then append arr check
			)
		)
	)
	else if obj.pesType == "Group" then (
		local members = pes_MakeArrayFromString obj.group_Members
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if obj.associatedTrigger == check.name then append arr check
				if obj.group_TriggerNoSpawn == check.name then append arr check
				if obj.group_TriggerGpNoSpawn == check.name then append arr check
				for mem in members do (
					if check.name == mem then append arr check
				)
			)
		)
	)
	else if obj.pesType == "Spawn Group" then (
		local members = pes_MakeArrayFromString obj.spGroup_Members
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if obj.GW_associatedLocation == check.name then append arr check
				if obj.associatedTrigger == check.name then append arr check
				for mem in members do (
					if check.name == mem then append arr check
				)
			)
		)
	)
	else if obj.pesType == "Trigger Group" then (
		local members = pes_MakeArrayFromString obj.trigGroup_Members
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if check.group_TriggerGpNoSpawn == obj.name then append arr check
				for mem in members do (
					if check.name == mem then append arr check
				)
			)
		)
	)
)
------------------------------------------------------------------------------------------------------------
-- obj == object being renamed
function pes_RenameDependents obj oldName newName = (
	if obj.pesType == "Checkpoint" then (
		for i = 1 to PES.checkpointOrder.count do (
			if PES.checkpointOrder[i] == oldName then PES.checkpointOrder[i] = newName
		)
		pes_StoreCheckpointOrder()
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if check.associatedCheckpoint == oldName then check.associatedCheckpoint = newName
			)
		)
	)
	if obj.pesType == "Trigger" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if check.NPC_AttachedTrigger == oldName then check.NPC_AttachedTrigger = newName
				if check.NPC_InitialTether == oldName then check.NPC_InitialTether = newName
				if check.associatedTrigger == oldName then check.associatedTrigger = newName
				if check.group_TriggerNoSpawn == oldName then check.group_TriggerNoSpawn = newName
			)
		)
	)
	else if obj.pesType == "Move Destination" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if check.NPC_SpawnDestination == oldName then check.NPC_SpawnDestination = newName
			)
		)
	)
	else if obj.pesType == "Player Spawn" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if check.CP_SpawnPlayer1 == oldName then check.CP_SpawnPlayer1 = newName
				if check.CP_SpawnPlayer2 == oldName then check.CP_SpawnPlayer2 = newName
			)
		)
	)
	else if obj.pesType == "NPC" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then (
				if check.vehicle_Passenger1 == oldName then check.vehicle_Passenger1 = newName
				if check.vehicle_Passenger2 == oldName then check.vehicle_Passenger2 = newName
				if check.vehicle_Passenger3 == oldName then check.vehicle_Passenger3 = newName
				if check.vehicle_Passenger4 == oldName then check.vehicle_Passenger4 = newName
				if check.NPC_Reinforcement == oldName then check.NPC_Reinforcement = newName
			)
		)
	)
	else if obj.pesType == "Group" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then ()
		)
	)
	else if obj.pesType == "Spawn Group" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then ()
		)
	)
	else if obj.pesType == "Trigger Group" then (
		for check in rootnode.children do (
			if (pes_IsTaggedAsPES check) then ()
		)
	)
)
------------------------------------------------------------------------------------------------------------
function pes_ValidateScene = (
	if not PES.sceneValidated then (
		local deletion = #()
		local totalObjects = rootnode.children.count
		local objectCount = 0
		if totalObjects > 0 then (
			progressStart "Validating Scene"
			for obj in rootnode.children do (
				if pes_IsTaggedAsPES obj then (
					pes_ValidateAttributes obj
					if obj.pesType == "Move Destination" then (
						if (classof obj) != XRefObject then (
							local newobj
							newobj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_Vector.max" "proxy" modifiers:#drop manipulator:#merge
							move newobj obj.position
							rotate newobj obj.rotation
							pes_CopyAttributes obj newobj
							newobj.name = obj.name
							obj.layer.addnode newobj
							append deletion obj
						)
					) else (
						if (classof obj) == XRefObject then (
							if (obj.fileName == "X:\payne\payne_art\Helpers\PED_PROXY.max") then (
								obj.fileName = "X:\payne\payne_art\Helpers\pes_NPC.max"
								obj.objectName = "proxy"
							)
							if (obj.fileName == "X:\payne\payne_art\Helpers\vehicleHelper.max") then (
								obj.fileName = "X:\payne\payne_art\Helpers\pes_Vehicle.max"
								obj.objectName = "proxy"
							)
						)
					)
					pes_SetObjectProperties obj
				)
				progressUpdate (objectCount as float / totalObjects * 100.0)
				objectCount += 1
			)
			progressEnd()
		)
		delete deletion
		PES.sceneValidated = true
		objXRefMgr.UpdateAllRecords()
	)
)
------------------------------------------------------------------------------------------------------------
function pes_StoreCheckpointOrder = (
	local store_str = ""
	for str in PES.checkpointOrder do (
		store_str = append store_str "["
		store_str = append store_str str
		store_str = append store_str "]"
	)
	store_str = append store_str ";"
	PES.levelInfo.levelInfo_CPsOrder = store_str
)
------------------------------------------------------------------------------------------------------------
function pes_ParseCheckpointOrder = (
	PES.checkpointOrder =#()
	local store_str = PES.levelInfo.levelInfo_CPsOrder
	if store_str.count <= 0 or store_str == ";" then (
		for obj in rootnode.children do (
			if (pes_IsTaggedAsPES obj) then (
				if obj.pesType == "Checkpoint" then append PES.checkpointOrder obj.name
			)
		)
		pes_StoreCheckpointOrder()
	) else (
		local count = 1
		while store_str[count] != ";" do (
			if store_str[count] == "[" then (
				count = count + 1
				local out_str = ""
				while store_str[count] != "]" do (
					out_str = append out_str store_str[count]
					count = count + 1
				)
				if out_str.count > 0 then append PES.checkpointOrder out_str
			)
			count = count + 1
		)
	)
)
------------------------------------------------------------------------------------------------------------
function pes_ClearPainterCursor = (
	if PES.painterCursor != undefined then (
		delete PES.painterCursor
		PES.painterCursor = undefined
	)
	local cursor = (getNodeByName "pes_painter_cursor")
	if cursor != undefined then delete cursor
)
------------------------------------------------------------------------------------------------------------
function pes_CheckLevelInfo = (
	local foundLevelInfo = false
	for obj in rootnode.children do (
		if (pes_IsTaggedAsPES obj) then (
			if obj.pesType == "Level Info" then (
				foundLevelInfo = true
				PES.levelInfo = obj
				PES.levelInfo.levelInfo_CreationType = "Player Spawn"
			)
		)
	)
	if not foundLevelInfo then (
		for obj in rootnode.children do (
			if obj.name == "LevelInfo" then (
				foundLevelInfo = true
				PES.levelInfo = obj
				pes_TagAsPES PES.levelInfo
				PES.levelInfo.pesType = "Level Info"
				pes_CheckLevelInfo()
			)
		)
	)
	if not foundLevelInfo then (
		PES.levelInfo = box name:"LevelInfo" position:[0,0,-100] width:0.1 length:0.1 height:0.1
		(LayerManager.getLayer 0).addnode PES.levelInfo
		pes_TagAsPES PES.levelInfo
		PES.levelInfo.pesType = "Level Info"
		freeze PES.levelInfo
		hide PES.levelInfo
		local layerName = "pes_MiscObjects"
		local layer = LayerManager.getLayerFromName layerName
		if layer == undefined then (
			layer = LayerManager.newLayer()
			layer.setname layerName
		)
		layer.addnode PES.levelInfo
		pes_CheckLevelInfo()
	)
)
------------------------------------------------------------------------------------------------------------
function pes_ColouriseSelection = (
	for oldObj in PES.selectionArray do pes_SetStandardColour oldObj
	PES.selectionArray = #()
	if selection.count > 0 then ( 
		for obj in selection do (
			if (pes_IsTaggedAsPES obj) then pes_AddDependentsToArray PES.selectionArray obj
		)
	)
	for newObj in PES.selectionArray do newObj.material.diffuseColor = yellow
)
------------------------------------------------------------------------------------------------------------
function pes_CreateFileSC levelName inFileName = (
	local out = undefined
	local filename = (PES.exportPath + levelName + "\\" + inFileName)
	if not doesFileExist filename then
		out = createFile filename
	else (
		out = openFile filename mode:"r+"
		if out == undefined then (
			if queryBox ("Couldn't open file for writing:\n\n" + filename + "\n\nWould you like to check out this file from Perforce?\n") then (
				local p4name = (PES.p4Path + levelName + "/" + inFileName)
				DOSCommand("p4 edit " + p4name)
				out = openFile filename mode:"r+"
				if out == undefined then (
					messageBox "Failed to check out file from Perforce."
					return undefined
				)
			) else return undefined
		)
	)
	if out != undefined then (
		close out
		out = openFile filename mode:"w+"
		format "//=====================================================================================\n" to:out
		format "// This file is autogenerated - Use caution when altering its structure manually.\n" to:out
		format "// Manual changes will be overwritten by the autogenerate process.\n" to:out
		format "//=====================================================================================\n\n" to:out
	)
	return out
)
------------------------------------------------------------------------------------------------------------
function pes_CloseFileSC file = (
	format "\n" to:file
	close file
)
------------------------------------------------------------------------------------------------------------
function pesCallback_MouseTrackCancel msg ir obj faceNum shift ctrl alt = (
	pes_ClearPainterCursor()
)
------------------------------------------------------------------------------------------------------------
function pesCallback_RenamedNode = (
	local params = callbacks.notificationParam()
	if pes_IsTaggedAsPES params[3] then
		pes_RenameDependents params[3] params[1] params[2]
)
------------------------------------------------------------------------------------------------------------
function pes_ConvertTrigger oldTrigger newType = (
	local obj
	local oldName = oldTrigger.name
	local oldLayer = oldTrigger.layer
	local oldPosition = oldTrigger.position
	local oldStartDisabled = oldTrigger.trigger_StartDisabled
	local oldLogShootEvents = oldTrigger.trigger_LogShootEvents
	delete oldTrigger
	if newType == "BOX" then (
		obj = box prefix:"tmp_" position:[0,0,0] width:3 length:3 height:2
		obj.name = oldName
		pes_TagAsPES obj
		obj.pesType = "Trigger"
		obj.trigger_Type = "BOX"
	) else (
		obj = sphere prefix:"tmp_" position:[0,0,0] radius:2
		obj.name = oldName
		pes_TagAsPES obj
		obj.pesType = "Trigger"
		obj.trigger_Type = "SPHERE"
	)
	if obj != undefined then (
		obj.position = oldPosition
		obj.trigger_StartDisabled = oldStartDisabled
		obj.trigger_LogShootEvents = oldLogShootEvents
		pes_SetObjectProperties obj
		oldLayer.addnode obj
	)
)
------------------------------------------------------------------------------------------------------------
function pes_CheckFilters = (
	for obj in rootnode.children do (
		if (pes_IsTaggedAsPES obj) then (
			if obj.pesType == "Player Spawn" then (
				if PES.cbx_ShowSpawnpoints.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "Checkpoint" then (
				if PES.cbx_ShowCheckpoints.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "NPC" then (
				if PES.cbx_ShowNPCs.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "Pickup" then (
				if PES.cbx_ShowPickups.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "Standard Door" then (
				if PES.cbx_ShowDoors.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "Group" then (
				if PES.cbx_ShowGroups.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "Spawn Group" then (
				if PES.cbx_ShowSPGroups.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "Trigger Group" then (
				if PES.cbx_ShowTrigGroups.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "Territory" then (
				if PES.cbx_ShowTerritories.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "GW Bomb" then (
				if PES.cbx_ShowGWBomb.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "GW Pickup" then (
				if PES.cbx_ShowGWPickup.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "GW Escape Point" then (
				if PES.cbx_ShowGWEscape.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "DM Spawn Beacon" then (
				if PES.cbx_ShowSpawnBeacons.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "Location" then (
				if PES.cbx_ShowLocations.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "Move Destination" then (
				if PES.cbx_ShowVectors.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "Vehicle" then (
				if PES.cbx_ShowVehicles.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
			else if obj.pesType == "Trigger" then (
				if PES.cbx_ShowTriggers.state == true then unhide obj else (
					deselect obj
					hide obj
				)
			)
		)
	)
)
------------------------------------------------------------------------------------------------------------
function pesCallback_SceneReset = (
	try(destroyDialog PES)  catch()
)
------------------------------------------------------------------------------------------------------------
function pesCallback_XrefHider msg ir obj faceNum shift ctrl alt = (
	case msg of (
		#freemove: (
			local closest = 9999999
			local thisRay = mapScreenToWorldRay mouse.pos
			for hitObj in PES.validScanObjects do (
				hitObj.wirecolor = white
				local intersect = (intersectRayEx hitObj thisRay)
				if intersect != undefined then (
					local thisDist = distance thisRay.pos intersect[1].pos
					if thisDist < closest and not hitObj.isHidden and hitObj != PES.painterCursor do (
						closest = thisDist
						PES.xRef = hitObj
					)
				)
			)
			if PES.xRef != undefined then PES.xRef.wirecolor = red
		)
		#mousePoint: (
			if PES.xRef != undefined then hide PES.xRef
		)
	)
	if msg == #mouseAbort then (
		PES.btn_XrefHider.state = false
		if PES.xRef != undefined then PES.xRef.wirecolor = white
		PES.xRef = undefined
	)
	else #continue
)
------------------------------------------------------------------------------------------------------------
function pesCallback_XrefDoorPicker msg ir obj faceNum shift ctrl alt = (
	case msg of (
		#freemove: (
			local closest = 9999999
			local thisRay = mapScreenToWorldRay mouse.pos
			for hitObj in PES.validScanObjects do (
				hitObj.wirecolor = white
				local intersect = (intersectRayEx hitObj thisRay)
				if intersect != undefined then (
					local thisDist = distance thisRay.pos intersect[1].pos
					if thisDist < closest and not hitObj.isHidden and hitObj != PES.painterCursor do (
						closest = thisDist
						PES.xRef = hitObj
					)
				)
			)
			if PES.xRef != undefined then PES.xRef.wirecolor = red
		)
		#mousePoint: (
			if PES.xRef != undefined then (
				if selection.count > 1 then messageBox "One at a time please!"
				else (
					local selObj = selection[1]
					selObj.modelName = PES.xRef.name
					selObj.position = PES.xRef.position
				)
				mouseTrack trackCallBack:pesCallback_MouseTrackCancel
				PES.pesRollout_StandardDoor.btn_PickFromScene.state = false
				if PES.xRef != undefined then PES.xRef.wirecolor = white
				PES.xRef = undefined
			)
		)
	)
	if msg == #mouseAbort then (
		PES.pesRollout_StandardDoor.btn_PickFromScene.state = false
		if PES.xRef != undefined then PES.xRef.wirecolor = white
		PES.xRef = undefined
	)
	else #continue
)
------------------------------------------------------------------------------------------------------------
function pesCallback_XrefObjectPicker msg ir obj faceNum shift ctrl alt = (
	case msg of (
		#freemove: (
			local closest = 9999999
			local thisRay = mapScreenToWorldRay mouse.pos
			for hitObj in PES.validScanObjects do (
				hitObj.wirecolor = white
				local intersect = (intersectRayEx hitObj thisRay)
				if intersect != undefined then (
					local thisDist = distance thisRay.pos intersect[1].pos
					if thisDist < closest and not hitObj.isHidden and hitObj != PES.painterCursor do (
						closest = thisDist
						PES.xRef = hitObj
					)
				)
			)
			if PES.xRef != undefined then PES.xRef.wirecolor = red
		)
		#mousePoint: (
			if PES.xRef != undefined then (
				if selection.count > 1 then messageBox "One at a time please!"
				else (
					local selObj = selection[1]
					selObj.position = PES.xRef.position
					selObj.position.z += 0.4
				)
				mouseTrack trackCallBack:pesCallback_MouseTrackCancel
				if PES.xRef != undefined then PES.xRef.wirecolor = white
				PES.xRef = undefined
			)
		)
	)
	if msg == #mouseAbort then (
		if PES.xRef != undefined then PES.xRef.wirecolor = white
		PES.xRef = undefined
	)
	else #continue
)
------------------------------------------------------------------------------------------------------------
function pes_CreatePESObjectAtPosition position = (
	if PES.levelInfo != undefined then (
		local prefix = PES.levelInfo.levelInfo_ObjPrefix
		local postfix = PES.levelInfo.levelInfo_ObjPostfix
		local type = PES.levelInfo.levelInfo_CreationType
		local offset = [0,0,0]
		local obj
		if type == "Player Spawn" then (
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_NPC.max" "proxy" modifiers:#drop manipulator:#merge
			obj.name  = (uniqueName (prefix + "playerSpawn_" + postfix))
		)
		else if type == "Checkpoint" then (
			obj = sphere prefix:(prefix + "checkpoint_" + postfix) position:[0,0,0] radius:1 segs:4 smooth:false
			offset = [0,0,1]
			append PES.checkpointOrder obj.name
			pes_StoreCheckpointOrder()
		)
		else if type == "Trigger" then (
			obj = box prefix:(prefix + "trigger_" + postfix) position:[0,0,0] width:3 length:3 height:2
		)
		else if type == "Territory" then (
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_Territory.max" "proxy" modifiers:#drop manipulator:#merge
			obj.name  = (uniqueName (prefix + "territory_" + postfix))
		)
		else if type == "Location" then (
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_Location.max" "proxy" modifiers:#drop manipulator:#merge
			obj.name  = (uniqueName (prefix + "location_" + postfix))
		)
		else if type == "GW Bomb" then (
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_Bomb.max" "proxy" modifiers:#drop manipulator:#merge
			obj.name  = (uniqueName (prefix + "bomb_" + postfix))
		)
		else if type == "GW Pickup" then (
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_GWPickup.max" "proxy" modifiers:#drop manipulator:#merge
			obj.name  = (uniqueName (prefix + "pickup_" + postfix))
		)
		else if type == "GW Escape Point" then (
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_GWDoor.max" "proxy" modifiers:#drop manipulator:#merge
			obj.name  = (uniqueName (prefix + "escape_" + postfix))
		)
		else if type == "DM Spawn Beacon" then (
			obj = cylinder prefix:(prefix + "spawnbeacon_" + postfix) position:[0,0,0] radius:35 height:10 heightsegs:1 sides:12
		)
		else if type == "NPC" then (
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_NPC.max" "proxy" modifiers:#drop manipulator:#merge
			obj.name  = (uniqueName (prefix + "npc_" + postfix))
		)
		else if type == "Pickup" then (
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\AK_47M.max" "Ak_47M" modifiers:#drop manipulator:#merge
			obj.name  = (uniqueName (prefix + "pickup_" + postfix))
			obj.rotation.x_rotation = 90
			offset = [0,0,0.05]
		)
		else if type == "Group" then (
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_Group.max" "proxy" modifiers:#drop manipulator:#merge
			obj.name  = (uniqueName (prefix + "group_" + postfix))
		)
		else if type == "Spawn Group" then (
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_Group.max" "proxy" modifiers:#drop manipulator:#merge
			obj.name  = (uniqueName (prefix + "spGroup_" + postfix))
		)
		else if type == "Trigger Group" then (
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_TriggerGroup.max" "proxy" modifiers:#drop manipulator:#merge
			obj.name  = (uniqueName (prefix + "trigGroup_" + postfix))
		)
		else if type == "Standard Door" then (
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_Door.max" "proxy" modifiers:#drop manipulator:#merge
			obj.name  = (uniqueName (prefix + "door_" + postfix))
			offset = [0,0,1]
		)
		else if type == "Move Destination" then (
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_Vector.max" "proxy" modifiers:#drop manipulator:#merge
			obj.name  = (uniqueName (prefix + "dest_" + postfix))
		)
		else if type == "Vehicle" then (
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_Vehicle.max" "proxy" modifiers:#drop manipulator:#xref
			obj.name  = (uniqueName (prefix + "vehicle_" + postfix))
		)
		else if type == "NPC + Move Dest + Tether" then (
			local obj2
			local obj3
			
			obj = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_NPC.max" "proxy" modifiers:#drop manipulator:#merge
			obj.name  = (uniqueName (prefix + "npc_" + postfix))
				
			obj2 = xrefs.addNewXRefObject "X:\payne\payne_art\Helpers\pes_Vector.max" "proxy" modifiers:#drop manipulator:#merge
			obj2.name  = (uniqueName (prefix + "dest_" + postfix))
				
			obj3 = box prefix:(prefix + "tether_" + postfix) position:[0,0,0] width:3 length:3 height:2
				
			if obj2 != undefined then (
				pes_TagAsPES obj2
				obj2.pesType = "Move Destination"
				pes_SetObjectProperties obj2
				local layerName = ("1_" + obj2.pesType)
				local layer = LayerManager.getLayerFromName layerName
				if layer == undefined then (
					layer = LayerManager.newLayer()
					layer.setname layerName
				)
				layer.addnode obj2
				move obj2 (position + offset)
				select obj2
			)
			
			if obj3 != undefined then (
				pes_TagAsPES obj3
				obj3.pesType = "Trigger"
				pes_SetObjectProperties obj3
				
				obj3.trigger_HelperType = "BOX_TETHER"
				pes_SetStandardColour obj3
				
				local layerName = ("1_" + obj3.pesType)
				local layer = LayerManager.getLayerFromName layerName
				if layer == undefined then (
					layer = LayerManager.newLayer()
					layer.setname layerName
				)
				layer.addnode obj3
				move obj3 (position + offset)
				select obj3
			)
		)
		
		if obj != undefined then (
			if type == "NPC + Move Dest + Tether" then (
				pes_TagAsPES obj
				obj.pesType = "NPC"
				pes_SetObjectProperties obj
				obj.NPC_SpawnDestination = obj2.name
				obj.NPC_InitialTether = obj3.name
			)
			else (
				pes_TagAsPES obj
				obj.pesType = type
				pes_SetObjectProperties obj
			)
	
			local layerName = ("1_" + obj.pesType)
			local layer = LayerManager.getLayerFromName layerName
			if layer == undefined then (
				layer = LayerManager.newLayer()
				layer.setname layerName
			)
			layer.addnode obj
			move obj (position + offset)
			select obj
			max modify mode
		)
		else messageBox "Error in PES object creation"
	)
)
------------------------------------------------------------------------------------------------------------
function pesCallback_CreatePESObject msg ir obj faceNum shift ctrl alt =
(
	case msg of
	(
		#freemove:
		(
-- 			if not snapMode.active then
-- 			(
-- 				max snap toggle
-- 			)
-- 			PES.intersect = undefined
-- 			local closest = 9999999
-- 			local thisRay = mapScreenToWorldRay mouse.pos
-- 			for hitObj in PES.validScanObjects do
-- 			(
-- 				local intersect = (intersectRayEx hitObj thisRay)
-- 				if intersect != undefined then
-- 				(
-- 					local thisDist = distance thisRay.pos intersect[1].pos
-- 					if thisDist < closest and hitObj != PES.painterCursor do (
-- 						closest = thisDist
-- 						PES.intersect = intersect[1]
-- 					)
-- 				)
-- 			)
			
			local mousePos
			
			if PES.intersect != undefined then
			(
				mousePos = PES.intersect.pos
			)
			else
			(
				mousePos = ir.pos
			)
			
			mousePos.z = 0.0
			
			local closest = 9999999
			local thisRay = mapScreenToWorldRay mouse.pos
			
			for object in PES.validScanObjects do
			(
				local centerYX = object.center
				centerYX.z = 0.0
				
				local objectBound = object.max - object.center
				
				if (distance mousePos centerYX) < (length objectBound) * 2 then
				(
					local intersect = (intersectRay object thisRay)
					
					if intersect != undefined then
					(
						local thisDist = distance thisRay.pos intersect.pos
						
						if thisDist < closest then
						(
							closest = thisDist
							PES.intersect = intersect
						)
					)
				)
			)
			
			if PES.intersect != undefined then
			(
				if PES.painterCursor == undefined then
				(
					PES.painterCursor = point name:"pes_painter_cursor" pos:PES.intersect.pos size:0.5--sphere name:"pes_painter_cursor" position:PES.intersect.pos radius:0.5
					PES.painterCursor.material = undefined
					PES.painterCursor.wireColor = color 255 0 0
				)
				else
				(
					PES.painterCursor.position = PES.intersect.pos
				)
			)
			else
			(
				pes_ClearPainterCursor()
			)
			
			#continue
		)
		#mousePoint:
		(
			if PES.painterCursor != undefined then
			(
				pes_CreatePESObjectAtPosition PES.painterCursor.position
			)
			
			#continue
		)
		#mouseAbort:
		(
			snapMode.setOSnapItemActive 5 5 false
			
			PES.btn_CreateObject.state = false
			pes_ClearPainterCursor()
		)
	)
)
------------------------------------------------------------------------------------------------------------
function pes_SwitchSubRollout = (
	pes_ClearPainterCursor()
	if PES.currentRollout != undefined then (
		removeSubRollout PES.sub_PesOps PES.currentRollout
		PES.currentRollout = undefined
	)
	if PES.rolloutMode == "Invalid Selection" then
		PES.currentRollout = PES.pesRollout_Invalid
	else if PES.rolloutMode == "No Type" then
		PES.currentRollout = PES.pesRollout_NoType
	else if PES.rolloutMode == "Level Info" then
		PES.currentRollout = PES.pesRollout_NoData
	else if PES.rolloutMode == "Trigger" then
		PES.currentRollout = PES.pesRollout_Trigger
	else if PES.rolloutMode == "NPC" then
		PES.currentRollout = PES.pesRollout_NPC
	else if PES.rolloutMode == "Pickup" then
		PES.currentRollout = PES.pesRollout_Pickup
	else if PES.rolloutMode == "Location" then
		PES.currentRollout = PES.pesRollout_Location
	else if PES.rolloutMode == "GW Bomb" then
		PES.currentRollout = PES.pesRollout_GWBomb
	else if PES.rolloutMode == "GW Pickup" then
		PES.currentRollout = PES.pesRollout_GWPickup
	else if PES.rolloutMode == "GW Escape Point" then
		PES.currentRollout = PES.pesRollout_GWEscape
	else if PES.rolloutMode == "Move Destination" then
		PES.currentRollout = PES.pesRollout_MoveDestination
	else if PES.rolloutMode == "Vehicle" then
		PES.currentRollout = PES.pesRollout_Vehicle
	else if PES.rolloutMode == "Player Spawn" then
		PES.currentRollout = PES.pesRollout_PlayerSpawnPoint
	else if PES.rolloutMode == "Standard Door" then
		PES.currentRollout = PES.pesRollout_StandardDoor
	else if PES.rolloutMode == "Not Visible" then
		PES.currentRollout = PES.pesRollout_EmptySelection
	else if PES.rolloutMode == "Territory" then (
		if selection.count > 1 then
			PES.currentRollout = PES.pesRollout_ModalSelection
		else
			PES.currentRollout = PES.pesRollout_Territory
	)
	else if PES.rolloutMode == "Checkpoint" then (
		if selection.count > 1 then
			PES.currentRollout = PES.pesRollout_ModalSelection
		else
			PES.currentRollout = PES.pesRollout_Checkpoint
	)
	else if PES.rolloutMode == "Group" then (
		if selection.count > 1 then
			PES.currentRollout = PES.pesRollout_ModalSelection
		else
			PES.currentRollout = PES.pesRollout_Group
	)
	else if PES.rolloutMode == "Spawn Group" then (
		if selection.count > 1 then
			PES.currentRollout = PES.pesRollout_ModalSelection
		else
			PES.currentRollout = PES.pesRollout_spGroup
	)
	else if PES.rolloutMode == "Trigger Group" then (
		if selection.count > 1 then
			PES.currentRollout = PES.pesRollout_ModalSelection
		else
			PES.currentRollout = PES.pesRollout_trigGroup
	)
	if PES.currentRollout != undefined then
		addSubRollout PES.sub_PesOps PES.currentRollout
)
------------------------------------------------------------------------------------------------------------
function pesCallback_SelectionChanged = (
	if selection.count > 0 then (
		local invalidSelection = true
		local leadObj = selection[1]
		local leadType = "No Type"
		if (pes_IsTaggedAsPES leadObj) then (
			leadType = leadObj.pesType
			invalidSelection = false
		)
		if selection.count > 1 then (
			for o = 2 to selection.count do (
				thisObj = selection[o]
				if not (pes_IsTaggedAsPES thisObj) then (
					invalidSelection = true
				) else (
					if thisObj.pesType != leadType then
						invalidSelection = true
				)
			)
		)
		if invalidSelection then
			PES.rolloutMode = "Invalid Selection"
		else
			PES.rolloutMode = leadObj.pesType
		
		if PES != undefined then (
			PES.btn_AddTags.enabled = true
			PES.btn_RemoveTags.enabled = true
		)
	) else (
		PES.rolloutMode = "Not Visible"
		if PES != undefined then (
			PES.btn_AddTags.enabled = false
			PES.btn_RemoveTags.enabled = false
		)
	)
	pes_ColouriseSelection()
	pes_SwitchSubRollout()
)
------------------------------------------------------------------------------------------------------------
