--|----------------------------------------------------------------------------------------------------------
--|	File:		RSL_PES_Enums
--|	Author:	Gavin Skinner @ Rockstar London
--|----------------------------------------------------------------------------------------------------------

pes_Types = #(
	"Player Spawn",
	"Spawn Group",
	"Checkpoint",
	"Trigger",
	"Trigger Group",
	"Territory",
	"Location",
	"GW Bomb",
	"GW Pickup",
	"GW Escape Point",
	"DM Spawn Beacon",
	"NPC",
	"Pickup",
	"Standard Door",
	"Move Destination",
	"Vehicle",
	"Group",
	"NPC + Move Dest + Tether"
	)

pes_LevelTypes = #(
	"Single Player/Coop",
	"DeathMatch"
	)
	
pes_RELGROUP = #(-- add RELGROUP_ & PEDTYPE_
	"GANG1",		
	"GANG2",
	"MISSION1",
	"MISSION2",
	"CIVMALE"
	)

pes_TRIGGER_TYPES = #(-- add TRIGGER_
	"BOX"
--	"SPHERE"
	)
	
pes_HELPER_BOX_TYPE = #( --add HELPER_
	"BOX_TRIGGER",
	"BOX_TETHER",
	"BOX_FLEEZONE",
	"BOX_NONE"
	)
	
pes_COMBAT_MOVEMENT = #( -- add CM_
	"STATIONARY",
	"DEFENSIVE",
	"WILLADVANCE"
	)

pes_COMBAT_ABILITY_LEVEL = #(-- add CAL_
	"POOR",
	"AVERAGE",
	"PROFESSIONAL"
	)

pes_COMBAT_RANGE = #(-- add CR_
	"NEAR",
	"MEDIUM",
	"FAR"
	)

pes_WEAPONTYPE = #(-- add WEAPONTYPE_
	"UNARMED",
	"PISTOL_38",
	"PISTOL_COLT1911",
	"PISTOL_DEAGLE",
	"PISTOL_GLOCK18",
	"PISTOL_PT92",
	"PISTOL_TAURUS608",
	"RIFLE_97LC",
	"RIFLE_AK47",
	"RIFLE_FAL",
	"RIFLE_G36K",
	"RIFLE_RUG30",
	"RPG",
	"SHOTGUN_BENELLIM3",
	"SHOTGUN_DOUBLEBARREL",
	"SHOTGUN_MOSSBERG590",
	"SHOTGUN_SPAS12",
	"MG_21E",
	"MG_RPD",
	"SMG_MAC10",
	"SMG_MP5",
	"SMG_PM12",
	"SMG_UZI",
	"SNIPER_M82",
	"SNIPER_SG1",
	"GRENADE_LAUNCHER",
	"THROWN_GRENADE"
	)

pes_PICKUP_TYPE = #(-- add PICKUP_
	"HEALTH_STANDARD",
	"ARMOUR_STANDARD",
	"WEAPON_GRENADE_LAUNCHER",
	"WEAPON_MG_21E",
	"WEAPON_MG_RPD",
	"WEAPON_PISTOL_38",
	"WEAPON_PISTOL_BROWNING",
	"WEAPON_PISTOL_BROWNING_SILENCED",
	"WEAPON_PISTOL_COLT1911",
	"WEAPON_PISTOL_DEAGLE",
	"WEAPON_PISTOL_GLOCK18",
	"WEAPON_PISTOL_PT92",
	"WEAPON_PISTOL_TAURUS608",
	"WEAPON_RIFLE_97LC",
	"WEAPON_RIFLE_AK47",
	"WEAPON_RIFLE_FAL",
	"WEAPON_RIFLE_G36K",
	"WEAPON_RIFLE_L1A1",
	"WEAPON_RIFLE_L1A1_GRENADE",
	"WEAPON_RIFLE_RUG30",
	"WEAPON_RPG",
	"WEAPON_SHOTGUN_BENELLIM3",
	"WEAPON_SHOTGUN_DOUBLEBARREL",
	"WEAPON_SHOTGUN_MOSSBERG590",
	"WEAPON_SHOTGUN_SPAS12",
	"WEAPON_SMG_MAC10",
	"WEAPON_SMG_MP5",
	"WEAPON_SMG_PM12",
	"WEAPON_SMG_SKORPION",
	"WEAPON_SMG_UZI",
	"WEAPON_SNIPER_ENFORCER",
	"WEAPON_SNIPER_M82",
	"WEAPON_SNIPER_SG1",
	"WEAPON_SPEARGUN",
	"WEAPON_STINGER",
	"WEAPON_THROWN_GRENADE",
	"WEAPON_THROWN_INCENDIARY"
	)

pes_ATTACHMENT_TYPES = #(-- add WEAPONATTACHMENT_
	"",
	"CLIP",
	"TRIGGER",
	"HAMMER",
	"DIAL",
	"SLIDE",
	"SUPPRESSOR",
	"BIPOD",
	"SNIPER_SCOPE",
	"RED_DOT_SCOPE",
	"LASER_SIGHT",
	"FLASHLIGHT"
	)

pes_MODEL_TYPE = #()
pes_VEHICLE_TYPE = #()
pes_DOOR_MODELS = #()
