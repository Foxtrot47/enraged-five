
	
fn PrintScene=
(
	clearListener();
	count = 0;
	format "======================================================================================\n\n";
	for obj in rootnode.children do
	(
		pointA = point3 obj.position.x obj.position.y obj.position.z
		--roundPoint pointA
		
		pointB = point3 obj.rotation.x_rotation obj.rotation.y_rotation obj.rotation.z_rotation
		--roundPoint pointB

		format ( "//" + obj.name + "\n" );
		
		format ("fire01[%].vPos = <<" +  (pointA.x as string) + ", " + (pointA.y as string) + ", " + (pointA.z as string)  + ">>\n") count;
		format ("fire01[%].vRot = <<" +  (pointB.x as string) + ", " + (pointB.y as string) + ", " + (pointB.z as string)  + ">>\n\n") count;

		count = count + 1;

	)

	format "======================================================================================\n";
)

PrintScene();
