
rollout CleanScene "Scene Cleaner" (
	listBox lbx_levels "Levels" height:19
	checkbox chkOnlySingle "Show Single Player" checked:true
	checkbox chkOnlyVersus "Show Versus" checked:false
	checkbox chkOnlyCoop "Show Coop" checked:false
	multiListBox mlbx_maxFiles "Max Files" height:9
	
	label lbl "Warning: You will lose any unsaved data"
	group "Create Scene"(
	editText sceneName "Enter Saved Scene Name" width:200 height:17 align:#left 
	button btnCreateSceen "Create Scene From Files" 
	)
	group "Update Cleaned Files" (
	button btnJustUpdate "Update Selected Sections" 
	button btnUpdateScene "Update Current Scene" 	
	)	
	progressBar pb_progress color:orange
	
	local mapRoot = RsConfigGetProjRootDir() + "Payne_Art\\Maps\\"
	local levels = #(#(),#())
	local levelFiles = #()
	local selectedLevels = 0
	local levelcount = 0	
		
	fn processMILO MILOArray = (

		for obj in (objects as array) do (
			if (matchPattern obj.name pattern:"*_milo_*") do (
				local filename = RsConfigGetProjRootDir() + "Payne_Art\\Maps\\" + "s_stad" + "\\interiors\\" + (filenameFromPath obj.filename)
				mergeMAXFile filename #select #mergeDups #useSceneMtlDups quiet:true
				
				local MILO = getNodeByName (substituteString obj.objectname "_milo_" "")
				
				if(MILO != undefined) then move MILO obj.pos
			)
		)
		max select none
	)
		
	fn CleanObjects = (
		pb_progress.color = yellow
		cleanLayer = layermanager.newLayerFromName ".Cleaned Objects"
		if cleanLayer == undefined do(
			cleanLayer = layermanager.GetLayerFromName ".Cleaned Objects"
		)		

		cleanArray = objects as array
		amtOfObj = cleanArray.count
		curObj = 0
		for obj in cleanArray do(
			curObj = curObj + 1
			if IsValidNode obj == true do(
				if classof obj == editable_poly or classof obj == editable_mesh then (  
					local bound = distance obj.max obj.min
					local height = (abs (obj.max.z - obj.min.z))
					
					if bound < 0.2 or height < 0 then	(
						delete obj
					) else (
						obj.parent = undefined
						obj.isHidden = false
						obj.isFrozen = false
						obj.xray = false
						obj.boxMode = false
						obj.backFaceCull = true
						obj.showFrozenInGray = false
						
						obj.material = standardmaterial()
						obj.material.diffuse = color 20 50 80
						obj.wireColor = color 200 200 200
						obj.showVertexColors = false
						
						cleanLayer.addNode obj
					)				
				) else (
					delete obj
				)
			)
			pb_progress.value = ((levelcount as float) / (selectedLevels as float) * 100) + ((curObj as float) / (amtOfObj as float)) * ( 1 / (selectedLevels as float) * 100 )
		)
	)
	
	fn gatherLevelNames = 
	(
		local mapDirectories = getDirectories (mapRoot + "*")
		
		levels = #(#(),#())
		
		for directory in mapDirectories do
		(
			local dataStream = directory as stringStream
			skipToString dataStream "\\Maps\\"
			local mapName = readDelimitedString dataStream "\\"
			
			if matchPattern mapName pattern:"s_*" and chkOnlySingle.checked == true do (
				append levels[1] mapName
				append levels[2] directory
			)
			if matchPattern mapName pattern:"c_*" and chkOnlyCoop.checked == true do (
				append levels[1] mapName
				append levels[2] directory
			)
			if matchPattern mapName pattern:"v_*" and chkOnlyVersus.checked == true do (
				append levels[1] mapName
				append levels[2] directory
			)
		)
	)
	
	fn processXref XrefArr = (
		xrefs.updateChangedXRefs
		pb_progress.color = orange
		count = 0
		for Xref in XrefArr do (	
			select Xref
			count = count+1
			convertToMesh Xref
			pb_progress.value = ((levelcount as float) / (selectedLevels as float) * 100) + ((count as float) / (XrefArr.count as float)) * ( 1 / (selectedLevels as float) * 100 / 3)
			
		)
	)
	
	fn ImportMLO toClean= (
		pb_progress.color = red
		count = 0
		for objMILO in toClean do(			
			count = count + 1
			if classof objMILO == xrefobject do (  
				if matchPattern objMILO.objectname pattern:"*_milo_*" do (					
					local filename = RsConfigGetProjRootDir() + "Payne_Art\\Maps\\" + lbx_levels.selected + "\\interiors\\" + (filenameFromPath objMILO.filename)
					mergeMAXFile filename #select #mergeDups #useSceneMtlDups quiet:true
					
					local MILO = getNodeByName (substituteString objMILO.objectname "_milo_" "")
					
					if(MILO != undefined) then move MILO objMILO.pos
					pb_progress.value = ((levelcount as float) / (selectedLevels as float) * 100) + ((count as float) / (toClean.count as float)) * ( 1 / (selectedLevels as float) * 100 / 2)
				)
			)
		)
	)
	
	fn CreateScene createScene= (
		local filesToRef = #()
		
		pb_progress.value = 0
		levelcount = -1
		
		selectedLevels = (mlbx_maxFiles.selection  as array).count
		for selected in (mlbx_maxFiles.selection  as array) do(
			levelcount = levelcount + 1
			file = getFilenameFile levelFiles[selected]
			local filename = RsConfigGetProjRootDir() + "Payne_Art\\Maps\\" + lbx_levels.selected + "\\" + file + ".max"

			local fileLoaded = loadMaxFile filename quiet:true

			allObj = objects as array
		
			ImportMLO allObj
			
			local xRefArray = for object in objects where (classOf object) == XrefObject and not (matchPattern object.name pattern:"*_milo_") collect object
			processMax = xRefArray.count
			processCount = 0
			processXref xRefArray
			
			CleanObjects()
						
			filename = RsConfigGetProjRootDir() + "Payne_Art\\Maps\\" + lbx_levels.selected + "\\tempfiles\\" + file + "_clean.max"
			saveMaxFile filename clearNeedSaveFlag:true quiet:true useNewFile:false 
			append filesToRef filename 	
		)

		if createScene == true do (
			delete objects
			pb_progress.value = 0
			pb_progress.color = green
			sceneLayer = layermanager.newLayerFromName "zSceneInfo"
			if sceneLayer == undefined do(
				sceneLayer = layermanager.GetLayerFromName "zSceneInfo"
			)
			xRefsToMake = filesToRef.count
			refCount = 0
			for file in filesToRef do (
				refCount = refCount + 1
				curscene = xrefs.addNewXRefFile file
	  			curscene.boxDisp = true
 				
				pointName = "_sceneRef_" + (fileNameFromPath file as string)
				newPoint = point size: 1.0 pos:[0,0,0] name:pointName cross:false
				sceneLayer.addNode newPoint
				
				pb_progress.value = (refCount as float / xRefsToMake as float *100)
			)
			sceneLayer.lock = true
			sceneLayer.isfrozen = true
	  		filename = RsConfigGetProjRootDir + "Payne_Art\\Maps\\" + lbx_levels.selected + "\\tempfiles\\" + sceneName.text + "_gameFile.max"
	  		saveMaxFile filename clearNeedSaveFlag:true quiet:true useNewFile:true
		)
	)
	
	fn gatherMaxFiles level =
	(
		levelFiles = getFiles (level + "\\*.max")
		propFiles = getFiles (level + "\\props\\*.max")
		interiorFiles = getFiles (level + "\\interiors\\*.max")
	)
	
	
	on chkOnlySingle changed state do (
		gatherLevelNames()
		
		lbx_levels.items = levels[1]
	)
	
	on chkOnlyCoop changed state do (
		gatherLevelNames()
		
		lbx_levels.items = levels[1]
	)
	
	on chkOnlyVersus changed state do (
		gatherLevelNames()
		
		lbx_levels.items = levels[1]
	)
	
	on lbx_levels selected index do
	(
		local selectedLevel = levels[2][index]
		
		gatherMaxFiles selectedLevel
		
		mlbx_maxFiles.items = (for file in levelFiles collect getFilenameFile file)
	)
	
	on btnCreateSceen pressed do(
		if sceneName.text == undefined or sceneName.text == "" then (
			messageBox "Enter a file name!" beep: true
		)else (
			CreateScene(true)
		)
	)
	
	on btnJustUpdate pressed do(
		CreateScene(false)
	)
	
	on CleanScene open do(
		gatherLevelNames()
		
		lbx_levels.items = levels[1]
		local selectedLevel = levels[2][1]
		
		if selectedLevel != undefined then gatherMaxFiles selectedLevel
		
		mlbx_maxFiles.items = (for file in levelFiles collect getFilenameFile file)
	)
)
	

try CloseRolloutFloater RSVLevelCleaner catch()
RSVLevelCleaner= newRolloutFloater "Rockstar Level Cleaner" 250 710 730 100 
addRollout CleanScene RSVLevelCleaner --rolledup:true
