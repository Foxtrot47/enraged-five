--Allows designers to create trigger boxes in max by reading P_CreateTriggerBox(... from text/script files--

rollout TriggerBoxCreator "Trigger Box Creator" (
	label lab1 "Select a script or text file to import Trigger Boxes and Trigger Areas from."
	label lab2 "Note that Trigger Areas are automatically converted to boxes"
	button btnCreateBoxes "Create Trigger Boxes from File"
	on btnCreateBoxes pressed do
	(
		in_name = getOpenFileName()
		if in_name != undefined then
		(
			in_file = openFile in_name
			bAllCreated = false
			startPos = filePos in_file
			if skipToString in_file "P_CreateTriggerBox(" != undefined do 
			(
				while not bAllCreated do		
				(
					dispString = (readDelimitedString in_file ",")
					skipToString in_file "\""
					boxName = (readDelimitedString in_file "\"")					
					skipToString in_file "<<"
					xPos = (readValue in_file)
					yPos = (readValue in_file)
					zPos = (readValue in_file)
					skipToString in_file ","
					boxWidth = (readValue in_file)	
					boxLength = (readValue in_file)
					boxHeight = (readValue in_file)
					skipToString in_file "<<"
					xRot = (readValue in_file)	
					yRot = (readValue in_file)
					zRot = (readValue in_file)
					
					boxExists = getnodebyname boxName exact:true ignoreCase:false
					if boxExists != undefined do (delete boxExists)			
					
					newbox = Box length:boxLength width:boxWidth height:boxHeight pos:[xPos,yPos,zPos] name:boxName
					setUserPropBuffer newBox dispString
					
					
					rotBox = eulerangles xRot yRot zRot
					rotate newbox rotBox 
					
					if skipToString in_file "P_CreateTriggerBox(" == undefined do (bAllCreated = true)
				)
			)
			bAllCreated = false
			seek in_file startPos
			if skipToString in_file "P_CreateTriggerArea(" != undefined do 
			(
				while not bAllCreated do		
				(			
					boxName = (readDelimitedString in_file ",")
					skipToString in_file "<<"
					xPos1 = (readValue in_file)
					yPos1 = (readValue in_file)
					zPos1 = (readValue in_file)
					skipToString in_file "<<"
					xPos2 = (readValue in_file)	
					yPos2 = (readValue in_file)
					zPos2 = (readValue in_file)
					
					
					boxWidth = abs(xPos1 - xPos2)
					boxLength = abs(yPos1 - yPos2)
					boxHeight = abs(zPos1 - zPos2)
					
					if xPos1 < xPos2 then (
						xPos = boxWidth/2 + xPos1
					) else (
						xPos = boxWidth/2 + xPos2
					)
					if yPos1 < yPos2 then (
						yPos = boxLength/2 + yPos1
					) else (
						yPos = boxLength/2 + yPos2
					)
					if zPos1 < zPos2 then (
						zPos = zPos1
					) else (
						zPos = zPos2
					)
					
					boxExists = getnodebyname boxName exact:true ignoreCase:false
					if boxExists != undefined do (delete boxExists)			
					
					newbox = Box length:boxLength width:boxWidth height:boxHeight pos:[xPos,yPos,zPos] name:boxName
					setUserPropBuffer newBox boxName
					
					if skipToString in_file "P_CreateTriggerArea(" == undefined do (bAllCreated = true)
				)
			)
			close in_file
		)
	)
)


fn roundFloat pFloat = (
	pFloat = (formattedPrint pFloat format:".2f") as Float
)

--Allows designers to export the code for selected trigger boxes from max to a text file--
rollout TriggerBoxExporter "Trigger Box Exporter" (
	label lab3 "Export the code for selected Trigger Boxes to an external text file."
	label lab4 "(Do not export directly to a script file)"
	button btnSetFile "Export to File"
	on btnSetFile pressed do
	(
		out_name = GetSaveFileName()
		if out_name != undefined then
		(			
			out_file = createfile out_name
			for obj in selection do
			(
				if classOf obj == Box then (
					
					temp = obj
					temp.position.x = roundFloat temp.position.x 
					temp.position.y = roundFloat temp.position.y 
					temp.position.z = roundFloat temp.position.z
					temp.width = roundFloat temp.width
					temp.length = roundFloat temp.length
					temp.height = roundFloat temp.height
					temp.rotation.z_rotation = roundFloat temp.rotation.z_rotation
					
					strName = obj.name
					dispString = getUserPropBuffer obj
					strCenter = "(<<" +  (temp.position.x as string) + ", " + (temp.position.y as string) + ", " + (temp.position.z as string)  + ">>)"
					strOtherParams = (temp.width as string)  + ", " + (temp.length as string)  + ", " + (temp.height as string)
					strRotation = "(<<" + (temp.rotation.x_rotation as string) + ", " + (temp.rotation.y_rotation as string) + ", " + (temp.rotation.z_rotation as string)  + ">>)"
					strOutput = "P_CreateTriggerBox(" + strName + ",\"" + dispString + "\", " + strCenter + ", " + strOtherParams + ", " + strRotation + ")"
								
					format  strOutput to: out_file
					format "\n" to: out_file					
				)	
			)
			close out_file
			edit out_name
		)
	)
)

try CloseRolloutFloater RSVTriggerHelper catch()
RSVTriggerHelper= newRolloutFloater "Rockstar Trigger Box Helper" 500 320 730 650
addRollout TriggerBoxCreator RSVTriggerHelper --rolledup:true
addRollout TriggerBoxExporter RSVTriggerHelper --rolledup:true
