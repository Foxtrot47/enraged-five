--|----------------------------------------------------------------------------------------------------------
--|	File:		RSL_PES_SceneSearcher
--|	Author:	Gavin Skinner @ Rockstar London
--|	Purpose:	Searches and selects PES objects by attributes
--|----------------------------------------------------------------------------------------------------------

-- Includes
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\design\\RSL_PES_Enums.ms")
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\design\\RSL_PES_Attributes.ms")
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\design\\RSL_PES_Utils.ms")

try (destroyDialog RSL_PES_SceneSearcher) catch()

-- Globals
global RSL_PES_SceneSearcher

rollout RSL_PES_SceneSearcher_NPC "NPCs" width:376 height:72
(
	button btn_DoSearch "Do Search" pos:[216,8] width:120 height:24
	dropdownList ddl_ModelName "" pos:[8,8] width:192 height:21
	dropdownList ddl_RelType "" pos:[8,32] width:192 height:21
	
	on RSL_PES_SceneSearcher_NPC open do
	(
		local modelNames = #("")
		local modelRelTypes = #("")
		for obj in rootnode.children do (
			if (pes_IsTaggedAsPES obj) then (
				if obj.pesType == "NPC" then (
					appendIfUnique modelNames obj.NPC_ModelName
					appendIfUnique modelRelTypes obj.NPC_RelationshipGroup
				)
			)
		)
		ddl_ModelName.items = modelNames
		ddl_RelType.items = modelRelTypes
	)
	on btn_DoSearch pressed do
	(
			clearSelection()
			local sel = #()
			for obj in rootnode.children do (
				if (pes_IsTaggedAsPES obj) then (
					if obj.pesType == "NPC" then (
						if	obj.NPC_ModelName == ddl_ModelName.selected or
							obj.NPC_RelationshipGroup == ddl_RelType.selected
							then append sel obj
					)
				)
			)
			if sel.count > 0 then select sel
			SetDialogPos RSL_PES_SceneSearcher (GetDialogPos RSL_PES_SceneSearcher)
		)
)

rollout RSL_PES_SceneSearcher_Trigger "Trigger"
(
	button btn_DoSearch "Do Search" pos:[216,8] width:120 height:24
	dropdownList ddl_AutoScript "" pos:[8,8] width:192 height:21

	on RSL_PES_SceneSearcher_Trigger open do
	(
		local autoScript = #("")
		for obj in rootnode.children do (
			if (pes_IsTaggedAsPES obj) then (
				if obj.pesType == "Trigger" then (
					appendIfUnique autoScript obj.scriptResource
				)
			)
		)
		ddl_AutoScript.items = autoScript
	)
	on btn_DoSearch pressed do
	(
			clearSelection()
			local sel = #()
			for obj in rootnode.children do (
				if (pes_IsTaggedAsPES obj) then (
					if obj.pesType == "Trigger" then (
						if	obj.scriptResource == ddl_AutoScript.selected
							then append sel obj
					)
				)
			)
			if sel.count > 0 then select sel
			SetDialogPos RSL_PES_SceneSearcher (GetDialogPos RSL_PES_SceneSearcher)
		)
)

rollout RSL_PES_SceneSearcher " PES Scene Searcher" width:376 height:400
(
	subrollout sub "" pos:[8,8] width:360 height:380
	on RSL_PES_SceneSearcher open do (
		addSubRollout sub RSL_PES_SceneSearcher_NPC
		addSubRollout sub RSL_PES_SceneSearcher_Trigger
	)
)

-- Create the main floater
createDialog RSL_PES_SceneSearcher style:#(#style_toolwindow,#style_sysmenu)
