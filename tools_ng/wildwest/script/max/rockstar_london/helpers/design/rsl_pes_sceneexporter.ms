--|----------------------------------------------------------------------------------------------------------
--|	File:		RSL_PES_SceneExporter
--|	Author:	Gavin Skinner @ Rockstar London
--|	Purpose:	Exports the scene data for the Payne Event System
--|----------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------
function pes_Export_WriteCheckpointSpawnData plyr_num obj_name str_name = (
	local obj = pes_GetObject str_name
	if obj != undefined then (
		format "sev_Checkpoints[%].sp_Player%_Position = <<%, %, %>>\n" obj_name plyr_num obj.position.x obj.position.y obj.position.z to:PES.outData
		format "sev_Checkpoints[%].sp_Player%_Heading = %\n" obj_name plyr_num obj.rotation.z_rotation to:PES.outData
	--	format "sev_Checkpoints[%].sp_Player%_MILO = \"%\"\n" obj_name plyr_num obj.CP_SpawnMILO1 to:PES.outData
	)
)
------------------------------------------------------------------------------------------------------------
function pes_Export_Checkpoint arr = (
	format "//== Checkpoints\n" to:PES.outData
	format "//== Checkpoints\n" to:PES.outConst
	format "ENUM SEV_CHECKPOINT_NAME\n" to:PES.outConst
	for i = 1 to arr.count do (
		local obj = arr[i]
		format "\t%,\n" obj.name to:PES.outConst
		format "sev_Checkpoints[%] = sev_CheckpointCreate(ENUM_TO_INT(%), \"%\")\n" obj.name obj.name obj.name to:PES.outData
	--	if obj.associatedTrigger.count > 0 then format "sev_Checkpoints[%].activationTrigger = %\n" obj.name obj.associatedTrigger to:PES.outData
		if obj.CP_SpawnPlayer1.count > 0 then pes_Export_WriteCheckpointSpawnData 1 obj.name obj.CP_SpawnPlayer1
		if obj.CP_SpawnPlayer2.count > 0 then pes_Export_WriteCheckpointSpawnData 2 obj.name obj.CP_SpawnPlayer2
	--	if obj.CP_ObjectiveString.count > 0 then format "sev_Checkpoints[%].objectiveString = \"%\"\n" obj.name obj.CP_ObjectiveString to:PES.outData
		format "\n" to:PES.outData
	)
	format "\tSEV_CHECKPOINT_COUNT\n" to:PES.outConst
	format "ENDENUM\n\n" to:PES.outConst
	format "\n" to:PES.outData
)
------------------------------------------------------------------------------------------------------------
function pes_Export_SortCheckpoints arr = (
	local sortedCPs = #()
	for str in PES.checkpointOrder do (
		for obj in arr do (
			if obj.name == str then append sortedCPs obj
		)
	)
	pes_Export_Checkpoint sortedCPs
)
------------------------------------------------------------------------------------------------------------
function pes_Export_PlayerSpawn arr = (
	format "//== Player Spawn Points\n" to:PES.outData
	format "//== Player Spawn Points\n" to:PES.outConst
	format "ENUM SEV_PLAYERSPAWN_NAME\n" to:PES.outConst
	for i = 1 to arr.count do (
		local obj = arr[i]
		format "\t%,\n" obj.name to:PES.outConst
		format "sev_PlayerSPs[%] = sev_PlayerSpawnPoint_Create(ENUM_TO_INT(%), <<%, %, %>>, %, \"%\", %)\n" obj.name obj.name obj.position.x obj.position.y obj.position.z obj.rotation.z_rotation obj.CP_SpawnMILO1 obj.SP_headingDiv to:PES.outData
	
		-- check for milos that are too short
		if (obj.CP_SpawnMILO1.count <= 5) and (obj.CP_SpawnMILO1.count > 0) then (
			messageBox ("Found bad milo label \""+ obj.CP_SpawnMILO1 +"\" for spawn point "+ obj.name +".\nIncorrect milo data breaks spawn points in-game.\nFix this if it's a typo, e.g. \"w\".") title:"Warning"
		)
	)
	format "\tSEV_PLAYERSPAWN_COUNT\n" to:PES.outConst
	format "ENDENUM\n\n" to:PES.outConst		
	format "\n" to:PES.outData
)
------------------------------------------------------------------------------------------------------------
function pes_Export_Trigger arr = (
	format "//== Triggers\n" to:PES.outData
	format "//== Triggers\n" to:PES.outConst
	if arr.count > 0 then (
		format "ENUM SEV_TRIGGER_NAME\n" to:PES.outConst
		for i = 1 to arr.count do (
			local obj = arr[i]
			format "\t%,\n" obj.name to:PES.outConst
			format "sev_Triggers[%] = sev_TriggerCreate(\"%\")\n" obj.name obj.name to:PES.outData
			if obj.associatedCheckpoint.count > 0 then format "sev_Triggers[%].associatedCheckpoint = ENUM_TO_INT(%)\n" obj.name obj.associatedCheckpoint to:PES.outData
			vmin = point3 (obj.position.x - (obj.width / 2)) (obj.position.y - (obj.length / 2)) (obj.position.z)
			vmax = point3 (obj.position.x + (obj.width / 2)) (obj.position.y + (obj.length / 2)) (obj.position.z + obj.height)
			if obj.rotation.x_rotation == 0 and obj.rotation.y_rotation == 0 and obj.rotation.z_rotation == 0 then (
				--format "IF NOT levelInitialised	GAMEPLAY_HELPER_BOX_CREATE(HELPER_%, \"%\", <<%, %, %>>, <<%, %, %>>) ENDIF\n" \
				format "GAMEPLAY_HELPER_BOX_CREATE(HELPER_%, \"%\", <<%, %, %>>, <<%, %, %>>)\n" \
					obj.trigger_HelperType obj.name vmin.x vmin.y vmin.z vmax.x vmax.y vmax.z to:PES.outData
			) else (
				--format "IF NOT levelInitialised	GAMEPLAY_HELPER_BOX_ANGLED_CREATE(HELPER_%, \"%\", <<%, %, %>>, %, %, %, <<%, %, %>>) ENDIF\n" \
				format "GAMEPLAY_HELPER_BOX_ANGLED_CREATE(HELPER_%, \"%\", <<%, %, %>>, %, %, %, <<%, %, %>>)\n" \
					obj.trigger_HelperType obj.name obj.position.x obj.position.y (obj.position.z + (obj.height / 2)) \
					obj.width obj.length obj.height \
					obj.rotation.x_rotation obj.rotation.y_rotation obj.rotation.z_rotation \
					to:PES.outData
			)
			if obj.trigger_SEV_PlyEntered == true then format "sev_BindPlayerEntered(%, \"%\", &OnPlayerEntered_%)\n" obj.name obj.name obj.name to:PES.outData
			if obj.trigger_SEV_PlyLeft == true then format "sev_BindPlayerLeft(%, \"%\", &OnPlayerLeft_%)\n" obj.name obj.name obj.name to:PES.outData
			if obj.trigger_LookSpline.count > 0 then (
				format "sev_Triggers[%].isLookTrigger = true\n" obj.name to:PES.outData
				local splineobj = undefined
				for tobj in rootnode.children do (
					if tobj.name == obj.trigger_LookSpline then splineobj = tobj
				)
				if splineobj != undefined then (
					if (classof splineobj) == line then (
						local v1 = getKnotPoint splineobj 1 1
						local v2 = getKnotPoint splineobj 1 2
						if obj.trigger_SEV_PlyLookStart == true then format "sev_BindPlayerLookStart(%, \"%\", <<%, %, %>>, <<%, %, %>>, &OnPlayerLookStart_%)\n" obj.name obj.name v1.x v1.y v1.z v2.x v2.y v2.z obj.name to:PES.outData
						if obj.trigger_SEV_PlyLookEnd == true then format "sev_BindPlayerLookEnd(%, \"%\", <<%, %, %>>, <<%, %, %>>, &OnPlayerLookEnd_%)\n" obj.name obj.name v1.x v1.y v1.z v2.x v2.y v2.z obj.name to:PES.outData
						format "sev_Triggers[%].lookPoint1 = <<%, %, %>>\n" obj.name v1.x v1.y v1.z to:PES.outData
						format "sev_Triggers[%].lookPoint2 = <<%, %, %>>\n" obj.name v2.x v2.y v2.z to:PES.outData
					) else (
						messageBox ("LookTrigger target is not a line object, for: " + obj.name)
						PES.exportFailed = true
					)
				) else (
					messageBox ("LookTrigger target invalid for: " + obj.name)
					PES.exportFailed = true
				)
			)
			format "sev_Triggers[%].centre = <<%, %, %>>\n" obj.name obj.position.x obj.position.y (obj.position.z + (obj.height / 2)) to:PES.outData
			format "\n" to:PES.outData
		)
		format "\tSEV_TRIGGER_COUNT\n" to:PES.outConst
		format "ENDENUM\n\n" to:PES.outConst
	)
)
------------------------------------------------------------------------------------------------------------
function pes_Export_Territory arr = (
	format "//== Territories\n" to:PES.outData
	format "//== Territories\n" to:PES.outConst
	if arr.count > 0 then (
		format "ENUM SEV_TERRITORY_NAME\n" to:PES.outConst
		for i = 1 to arr.count do (
			local obj = arr[i]
			format "\t%,\n" obj.name to:PES.outConst
			if obj.GW_associatedLocation == "" then (
				format "sev_Territory_Infos[%] = sev_Territory_Info_Create(\"%\", \"%\", <<%, %, %>>, 4.0, 4.0)\n" obj.name obj.name obj.territory_Name obj.position.x obj.position.y obj.position.z to:PES.outData
			)
			else (
				format "sev_Territory_Infos[%] = sev_Territory_Info_Create(\"%\", \"%\", <<%, %, %>>, 4.0, 4.0, %)\n" obj.name obj.name obj.territory_Name obj.position.x obj.position.y obj.position.z obj.GW_associatedLocation to:PES.outData
			)
		)
		format "\tSEV_TERRITORY_COUNT\n" to:PES.outConst
		format "ENDENUM\n\n" to:PES.outConst
		format "\n" to:PES.outData
	)
)
------------------------------------------------------------------------------------------------------------
function pes_Export_Location arr = (
	format "//== Locations\n" to:PES.outData
	format "//== Locations\n" to:PES.outConst
	if arr.count > 0 then (
		format "ENUM SEV_LOCATION_NAME\n" to:PES.outConst
		for i = 1 to arr.count do (
			local obj = arr[i]
			format "\t%,\n" obj.name to:PES.outConst
			format "sev_Locations[%] = sev_Location_Create()\n" obj.name to:PES.outData
		)
		format "\tSEV_LOCATION_COUNT\n" to:PES.outConst
		format "ENDENUM\n\n" to:PES.outConst
		format "\n" to:PES.outData
	)
)
------------------------------------------------------------------------------------------------------------
function pes_Export_SpawnBeacon arr = (
	format "//== Spawn Beacons (DM)\n" to:PES.outData
	format "//== Spawn Beacons (DM)\n" to:PES.outConst
	if arr.count > 0 then (
		format "ENUM SEV_SPAWNBEACON_NAME\n" to:PES.outConst
		for i = 1 to arr.count do (
			local obj = arr[i]
			format "\t%,\n" obj.name to:PES.outConst
			format "sev_SpawnBeacons[%] = sev_SpawnBeacon_Create(<<%,%,%>>, %)\n" obj.name obj.position.x obj.position.y obj.position.z obj.radius to:PES.outData
		)
		format "\tSEV_SPAWNBEACON_COUNT\n" to:PES.outConst
		format "ENDENUM\n\n" to:PES.outConst
		format "\n" to:PES.outData
	)
)
------------------------------------------------------------------------------------------------------------
function pes_Export_GWBomb arr = (
	format "//== Bombs (GW)\n" to:PES.outData
	format "//== Bombs (GW)\n" to:PES.outConst
	if arr.count > 0 then (
		format "ENUM SEV_BOMBLOCATION_NAME\n" to:PES.outConst
		for i = 1 to arr.count do (
			local obj = arr[i]
			format "\t%,\n" obj.name to:PES.outConst
			if obj.GW_associatedLocation == "" then (
				format "sev_BombLocations[%] = sev_BombLocation_Create(<<%, %, %>>, %, \"%\", \"%\")\n" obj.name obj.position.x obj.position.y obj.position.z obj.rotation.z_rotation obj.GW_Name obj.associatedMILO to:PES.outData
			)
			else (
				format "sev_BombLocations[%] = sev_BombLocation_Create(<<%, %, %>>, %, \"%\", \"%\", %)\n" obj.name obj.position.x obj.position.y obj.position.z obj.rotation.z_rotation obj.GW_Name obj.associatedMILO obj.GW_associatedLocation to:PES.outData
			)
		)
		format "\tSEV_BOMBLOCATION_COUNT\n" to:PES.outConst
		format "ENDENUM\n\n" to:PES.outConst
		format "\n" to:PES.outData
	)
)
------------------------------------------------------------------------------------------------------------
function pes_Export_GWPickup arr = (
	format "//== Pickups (GW)\n" to:PES.outData
	format "//== Pickups (GW)\n" to:PES.outConst
	if arr.count > 0 then (
		format "ENUM SEV_PICKUP_NAME\n" to:PES.outConst
		for i = 1 to arr.count do (
			local obj = arr[i]
			format "\t%,\n" obj.name to:PES.outConst
			if obj.GW_associatedLocation == "" then (
				format "sev_PickupInfos[%] = sev_PickupInfo_Create(<<%, %, %>>, \"%\")\n" obj.name obj.position.x obj.position.y obj.position.z obj.associatedMILO to:PES.outData
			)
			else (
				format "sev_PickupInfos[%] = sev_PickupInfo_Create(<<%, %, %>>, \"%\", %)\n" obj.name obj.position.x obj.position.y obj.position.z obj.associatedMILO obj.GW_associatedLocation to:PES.outData
			)
		)
		format "\tSEV_PICKUP_COUNT\n" to:PES.outConst
		format "ENDENUM\n\n" to:PES.outConst
		format "\n" to:PES.outData
	)
)
------------------------------------------------------------------------------------------------------------
function pes_Export_GWEscape arr = (
	format "//== Escape Points (GW)\n" to:PES.outData
	format "//== Escape Points (GW)\n" to:PES.outConst
	if arr.count > 0 then (
		format "ENUM SEV_ESCAPEPOINT_NAME\n" to:PES.outConst
		for i = 1 to arr.count do (
			local obj = arr[i]
			format "\t%,\n" obj.name to:PES.outConst
			if obj.GW_associatedLocation == "" then (
				format "sev_EscapePoints[%] = sev_EscapePoint_Create(<<%, %, %>>, \"%\")\n" obj.name obj.position.x obj.position.y obj.position.z obj.associatedMILO to:PES.outData
			)
			else (
				format "sev_EscapePoints[%] = sev_EscapePoint_Create(<<%, %, %>>, \"%\", %)\n" obj.name obj.position.x obj.position.y obj.position.z obj.associatedMILO obj.GW_associatedLocation to:PES.outData
			)
		)
		format "\tSEV_ESCAPEPOINT_COUNT\n" to:PES.outConst
		format "ENDENUM\n\n" to:PES.outConst
		format "\n" to:PES.outData
	)
)
------------------------------------------------------------------------------------------------------------
function pes_Export_NPC arr = (
	format "//== NPCs\n" to:PES.outData
	format "//== NPCs\n" to:PES.outConst
	if arr.count > 0 then (
		format "ENUM SEV_NPC_NAME\n" to:PES.outConst
		for i = 1 to arr.count do (
			local obj = arr[i]
			format "\t%,\n" obj.name to:PES.outConst
			format "sev_NPCs[%] = sev_CreateNPC(ENUM_TO_INT(%), \"%\")\n" obj.name obj.name obj.name to:PES.outData
			format "sev_NPCs[%].locator = sev_CreatePedLocator(<<%, %, %>>, %)\n" obj.name obj.position.x obj.position.y obj.position.z obj.rotation.z_rotation to:PES.outData
			format "sev_NPCs[%].attachBulletCam = %\n" obj.name obj.NPC_AttachBulletCam to:PES.outData
			format "sev_NPCs[%].shouldRespawnOnDeath = %\n" obj.name obj.NPC_RespawnAfterDeath to:PES.outData
			format "sev_NPCs[%].blockTempEvents = %\n" obj.name obj.NPC_BlockTempEvents to:PES.outData
			format "sev_NPCs[%].ignoreNavmesh = %\n" obj.name obj.NPC_StormPlayer to:PES.outData
			format "sev_NPCs[%].modelName = %\n" obj.name obj.NPC_ModelName to:PES.outData
			format "sev_NPCs[%].relationshipGroup = RELGROUP_%\n" obj.name obj.NPC_RelationshipGroup to:PES.outData
			format "sev_NPCs[%].combatRange = CR_%\n" obj.name obj.NPC_CombatRange to:PES.outData
			format "sev_NPCs[%].combatAbility = CAL_%\n" obj.name obj.NPC_CombatAbility to:PES.outData
			format "sev_NPCs[%].combatMovement = CM_%\n" obj.name obj.NPC_CombatMovement to:PES.outData
			format "sev_NPCs[%].weaponType = WEAPONTYPE_%\n" obj.name obj.NPC_WeaponType to:PES.outData
			if obj.NPC_WeaponAddon.count > 0 then format "sev_NPCs[%].weaponAttachment = \"WEAPONATTACHMENT_%\"\n" obj.name obj.NPC_WeaponAddon to:PES.outData
			if obj.NPC_SpawnDestination.count > 0 then format "sev_NPCs[%].postSpawnDest = ENUM_TO_INT(%)\n" obj.name obj.NPC_SpawnDestination to:PES.outData
			if obj.NPC_InitialTether.count > 0 then format "sev_NPCs[%].initialTether = \"%\"\n" obj.name obj.NPC_InitialTether to:PES.outData
			if obj.associatedCheckpoint.count > 0 then format "sev_NPCs[%].associatedCheckpoint = ENUM_TO_INT(%)\n" obj.name obj.associatedCheckpoint to:PES.outData
			if obj.NPC_AttachedTrigger.count > 0 then format "sev_NPCs[%].attachedTrigger = ENUM_TO_INT(%)\n" obj.name obj.NPC_AttachedTrigger to:PES.outData
			if obj.NPC_SpawnDelay > 0 then format "sev_NPCs[%].spawnDelay = %\n" obj.name obj.NPC_SpawnDelay to:PES.outData
			if obj.NPC_StartingHealth > 0 then format "sev_NPCs[%].startingHealth = %\n" obj.name obj.NPC_StartingHealth to:PES.outData
			if obj.NPC_Reinforcement.count > 0 then format "sev_NPCs[%].reinforcement = %\n" obj.name obj.NPC_Reinforcement to:PES.outData
		--	if obj.NPC_MinSpawnDistance > 0 then format "sev_NPCs[%].spawnMinPlayerDist = %\n" obj.name obj.NPC_MinSpawnDistance to:PES.outData
			if obj.NPC_WalkMove then format "sev_NPCs[%].walkMove = true\n" obj.name to:PES.outData
			if obj.NPC_SafetyTeleport then format "sev_NPCs[%].useSafetyTeleport = true\n" obj.name to:PES.outData
			if obj.NPC_OverrideDeleteCorpse then format "sev_NPCs[%].deleteCorpseDelay = %\n" obj.name obj.NPC_DeleteCorpseDelay to:PES.outData
			format "\n" to:PES.outData
		)
		format "\tSEV_NPC_COUNT\n" to:PES.outConst
		format "ENDENUM\n\n" to:PES.outConst
	)
)
------------------------------------------------------------------------------------------------------------
function pes_Export_Pickup arr = (
	format "//== Pickups\n" to:PES.outData
	format "//== Pickups\n" to:PES.outConst
	format "\n" to:PES.outData
)
------------------------------------------------------------------------------------------------------------
function pes_Export_Doors arr = (
	format "//== Doors\n" to:PES.outData
	format "//== Doors\n" to:PES.outConst
	if arr.count > 0 then (
		format "ENUM SEV_DOOR_NAME\n" to:PES.outConst
		for i = 1 to arr.count do (
			local obj = arr[i]
			format "\t%,\n" obj.name to:PES.outConst
			format "sev_Doors[%] = sev_DoorCreate(ENUM_TO_INT(%), %, <<%, %, %>>, %, %, %, %, %, %, %, \"%\")\n" \
				obj.name obj.name obj.door_IsArtSide obj.position.x obj.position.y obj.position.z obj.rotation.z_rotation \
				obj.modelName obj.door_MinAngle obj.door_MaxAngle obj.door_StartOpen obj.door_AutoClose obj.door_Locked obj.associatedMILO to:PES.outData
		)
		format "\tSEV_DOOR_COUNT\n" to:PES.outConst
		format "ENDENUM\n\n" to:PES.outConst
		format "\n" to:PES.outData
	)
)
------------------------------------------------------------------------------------------------------------
function pes_Export_MoveDestination arr = (
	format "//== Move Destinations\n" to:PES.outData
	format "//== Move Destinations\n" to:PES.outConst
	if arr.count > 0 then (
		format "ENUM SEV_MOVEDEST_NAME\n" to:PES.outConst
		for i = 1 to arr.count do (
			local obj = arr[i]
			format "\t%,\n" obj.name to:PES.outConst
			format "sev_MoveDestinations[%] = sev_MoveDestinationCreate(ENUM_TO_INT(%), <<%, %, %>>, %)\n" \
				obj.name obj.name obj.position.x obj.position.y obj.position.z obj.rotation.z_rotation to:PES.outData
			if obj.NPC_SpawnDestination.count > 0 then format "sev_MoveDestinations[%].nextIndex = %\n" obj.name obj.NPC_SpawnDestination to:PES.outData
		)
		format "\tSEV_MOVEDEST_COUNT\n" to:PES.outConst
		format "ENDENUM\n\n" to:PES.outConst
		format "\n" to:PES.outData
	)
)
------------------------------------------------------------------------------------------------------------
function pes_Export_Vehicles arr = (
	format "//== Vehicles\n" to:PES.outData
	format "//== Vehicles\n" to:PES.outConst
	if arr.count > 0 then (
		format "ENUM SEV_VEHICLE_NAME\n" to:PES.outConst
		for i = 1 to arr.count do (
			local obj = arr[i]
			format "\t%,\n" obj.name to:PES.outConst
			format "sev_Vehicles[%] = sev_VehicleCreate(ENUM_TO_INT(%), \"%\", <<%, %, %>>, %)\n" \
				obj.name obj.name obj.name obj.position.x obj.position.y obj.position.z obj.rotation.z_rotation to:PES.outData
			format "sev_Vehicles[%].modelName = %\n" obj.name obj.vehicle_Type to:PES.outData
			if obj.vehicle_Passenger1.count > 0 then format "sev_Vehicles[%].passIndex1 = %\n" obj.name obj.vehicle_Passenger1 to:PES.outData
			if obj.vehicle_Passenger2.count > 0 then format "sev_Vehicles[%].passIndex2 = %\n" obj.name obj.vehicle_Passenger2 to:PES.outData
			if obj.vehicle_Passenger3.count > 0 then format "sev_Vehicles[%].passIndex3 = %\n" obj.name obj.vehicle_Passenger3 to:PES.outData
			if obj.vehicle_Passenger4.count > 0 then format "sev_Vehicles[%].passIndex4 = %\n" obj.name obj.vehicle_Passenger4 to:PES.outData
			if obj.associatedCheckpoint.count > 0 then format "sev_Vehicles[%].associatedCheckpoint = ENUM_TO_INT(%)\n" obj.name obj.associatedCheckpoint to:PES.outData
			format "\n" to:PES.outData
		)
		format "\tSEV_VEHICLE_COUNT\n" to:PES.outConst
		format "ENDENUM\n\n" to:PES.outConst
		format "\n" to:PES.outData
	)
)
------------------------------------------------------------------------------------------------------------
function pes_Export_Groups arr = (
	format "//== Groups\n" to:PES.outData
	format "//== Groups\n" to:PES.outConst
	if arr.count > 0 then (
		format "ENUM SEV_GROUP_NAME\n" to:PES.outConst
		for i = 1 to arr.count do (
			local obj = arr[i]
			format "\t%,\n" obj.name to:PES.outConst
			local members = pes_MakeArrayFromString obj.group_Members
			format "sev_Groups[%] = sev_GroupCreate(%, %)\n" obj.name members.count obj.group_BulletCam to:PES.outData
			if obj.associatedTrigger.count > 0 then format "sev_Groups[%].spawnTrigger = %\n" obj.name obj.associatedTrigger to:PES.outData
			if obj.group_TriggerNoSpawn.count > 0 then format "sev_Groups[%].preventSpawnTrigger = %\n" obj.name obj.group_TriggerNoSpawn to:PES.outData
			if obj.group_TriggerGpNoSpawn.count > 0 then format "sev_Groups[%].preventSpawnTriggerGroup = %\n" obj.name obj.group_TriggerGpNoSpawn to:PES.outData
			local index = -1
			for str in members do format "sev_Groups[%].members[%] = %\n" obj.name (index += 1) str to:PES.outData
			format "\n" to:PES.outData
		)
		format "\tSEV_GROUP_COUNT\n" to:PES.outConst
		format "ENDENUM\n\n" to:PES.outConst
		format "\n" to:PES.outData
	)
)
------------------------------------------------------------------------------------------------------------
function pes_Export_spGroups arr = (
	format "//== Spawn Groups\n" to:PES.outData
	format "//== Spawn Groups\n" to:PES.outConst
	if arr.count > 0 then (
		format "ENUM SEV_SPGROUP_NAME\n" to:PES.outConst
		for i = 1 to arr.count do (
			local obj = arr[i]
			format "\t%,\n" obj.name to:PES.outConst
			local members = pes_MakeArrayFromString obj.spGroup_Members
			if obj.GW_associatedLocation == "" then (
				format "sev_SpGroups[%] = sev_SpGroupCreate(%)\n" obj.name members.count to:PES.outData
			)
			else (
				format "sev_SpGroups[%] = sev_SpGroupCreate(%, %)\n" obj.name members.count obj.GW_associatedLocation to:PES.outData
			)
			local index = -1
			for str in members do format "sev_SpGroups[%].members[%] = %\n" obj.name (index += 1) str to:PES.outData
			format "\n" to:PES.outData
		)
		format "\tSEV_SPGROUP_COUNT\n" to:PES.outConst
		format "ENDENUM\n\n" to:PES.outConst
		format "\n" to:PES.outData
	)
)
------------------------------------------------------------------------------------------------------------
function pes_Export_trigGroups arr = (
	format "//== Trigger Groups\n" to:PES.outData
	format "//== Trigger Groups\n" to:PES.outConst
	if arr.count > 0 then (
		format "ENUM SEV_TRIGGERGROUP_NAME\n" to:PES.outConst
		for i = 1 to arr.count do (
			local obj = arr[i]
			format "\t%,\n" obj.name to:PES.outConst
			local members = pes_MakeArrayFromString obj.trigGroup_Members
			format "sev_TriggerGroups[%] = sev_TriggerGroupCreate(%)\n" obj.name members.count to:PES.outData
			local index = -1
			for str in members do format "sev_TriggerGroups[%].members[%] = %\n" obj.name (index += 1) str to:PES.outData
			format "\n" to:PES.outData
		)
		format "\tSEV_TRIGGERGROUP_COUNT\n" to:PES.outConst
		format "ENDENUM\n\n" to:PES.outConst
		format "\n" to:PES.outData
	)
)
------------------------------------------------------------------------------------------------------------
function pes_DoExport = (
	PES.exportFailed = false
	PES.outData = undefined
	PES.outConst = undefined
	PES.outData = pes_CreateFileSC PES.levelInfo.levelInfo_Name PES.scDataFile_SEV
	PES.outConst = pes_CreateFileSC PES.levelInfo.levelInfo_Name PES.scConstFile_SEV
	if PES.outData != undefined and PES.outConst != undefined then (
		local arr_PlayerSpawn = #()
		local arr_Checkpoint = #()
		local arr_Trigger = #()
		local arr_Territory = #()
		local arr_Location = #()
		local arr_GWBomb = #()
		local arr_GWPickup = #()
		local arr_GWEscape = #()
		local arr_SpawnBeacons = #()
		local arr_NPC = #()
		local arr_Pickup = #()
		local arr_Door = #()
		local arr_MoveDestination = #()
		local arr_Vehicles = #()
		local arr_Groups = #()
		local arr_spGroups = #()
		local arr_trigGroups = #()
		for obj in rootnode.children do (
			if pes_IsTaggedAsPES obj then (
				if obj.pesType == "Player Spawn" then append arr_PlayerSpawn obj
				else if obj.pesType == "Checkpoint" then append arr_Checkpoint obj
				else if obj.pesType == "Trigger" then append arr_Trigger obj
				else if obj.pesType == "Territory" then append arr_Territory obj
				else if obj.pesType == "Location" then append arr_Location obj
				else if obj.pesType == "GW Bomb" then append arr_GWBomb obj
				else if obj.pesType == "GW Pickup" then append arr_GWPickup obj
				else if obj.pesType == "GW Escape Point" then append arr_GWEscape obj
				else if obj.pesType == "DM Spawn Beacon" then append arr_SpawnBeacons obj
				else if obj.pesType == "NPC" then append arr_NPC obj
				else if obj.pesType == "Pickup" then append arr_Pickup obj
				else if obj.pesType == "Standard Door" then append arr_Door obj
				else if obj.pesType == "Move Destination" then append arr_MoveDestination obj
				else if obj.pesType == "Vehicle" then append arr_Vehicles obj
				else if obj.pesType == "Group" then append arr_Groups obj
				else if obj.pesType == "Spawn Group" then append arr_spGroups obj
				else if obj.pesType == "Trigger Group" then append arr_trigGroups obj
			)
		)
		-- Do Exports
		format "PROC sev_LoadImportedData()\n\n" to:PES.outData
		pes_Export_PlayerSpawn arr_PlayerSpawn
		pes_Export_SortCheckpoints arr_Checkpoint
		pes_Export_Trigger arr_Trigger
		pes_Export_NPC arr_NPC
		pes_Export_Pickup arr_Pickup
		pes_Export_Doors arr_Door
		pes_Export_MoveDestination arr_MoveDestination
		pes_Export_Vehicles arr_Vehicles
		pes_Export_Groups arr_Groups
		pes_Export_spGroups arr_spGroups
		pes_Export_trigGroups arr_trigGroups
		pes_Export_Territory arr_Territory
		pes_Export_Location arr_Location
		pes_Export_SpawnBeacon arr_SpawnBeacons
		pes_Export_GWBomb arr_GWBomb
		pes_Export_GWPickup arr_GWPickup
		pes_Export_GWEscape arr_GWEscape
		format "ENDPROC\n" to:PES.outData
		-- Finished.  Close files
		pes_CloseFileSC PES.outData
		pes_CloseFileSC PES.outConst
		if not PES.exportFailed then
			messageBox "Scene Export Successful."
		else
			messageBox "Scene Export Failed."
	)
	else messageBox "Scene Export Failed."
	-- (
	--	messageBox "Scene Export Failed."
	--	if PES.outData != undefined then close PES.outData
	--	if PES.outConst != undefined then close PES.outConst
	--)
	PES.outData = undefined
	PES.outConst = undefined
)
------------------------------------------------------------------------------------------------------------