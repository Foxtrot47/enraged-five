
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\startup\\RSL_VehiclePath.ms")

try (destroyDialog RSL_vehicleAnimator) catch()

struct RSL_VehicleAnimFrame
(
	time,
	matrix,
	additional
)

rollout RSL_vehicleAnimator "R* Vehicle Animator" width:170
(
	local vehicleIDEFile = (RsConfigGetCommonDir() + "data/vehicles.ide")
	local vehiclesRPF = (RsConfigGetIndDir() + "models/cdimages/vehicles.rpf")
	local AAAFile
	local vehicleArray = #()
	local pathArray = #()
	local frameArray = #()
	local bndArray = #()
	local newSpline
	local currentVehiclePath
	local currentVehicle
	local presetVehicle
	local rpfExtracted = false
	
	fn pickFilter object =
	(
		superClassOf object == geometryClass
	)
	
	button btn_pickAAA "Load Vehicle Path File..." width:160
	label lbl_paths "Paths in Scene:" align:#left offset:[-8,0]
	dropdownlist ddl_paths "" width:160 height:8 offset:[-7,0]
	label lbl_vehicles "Vehicles:" align:#left offset:[-8,0]
	dropdownlist ddl_vehicles "" width:160 height:24 offset:[-7,0]
	pickbutton pbtn_pickVehicle "Pick Vehicle From Scene" width:160 filter:pickFilter
	checkBox chkb_instance "As Instance" checked:true
	button btn_createAnim "Create/Update Animation" width:160
-- 	button btn_updateAnim "Update Vehicle From Path" width:160
	button btn_updatePath "Update Path From Vehicle" width:160
	button btn_savePath "Save Path" width:160
	label lbl_progress "Progress:" align:#left offset:[-8,0]
	progressBar pb_progress color:orange width:160 offset:[-8,0]
	
	fn readVehicleIDE =
	(
		local dataStream = openFile vehicleIDEFile
		
		if dataStream != undefined then
		(
			local line = readLine dataStream
			
			while line != "end" and not (eof dataStream) do
			(
				if line[1] != "#" and line != "cars" and line.count > 0 then
				(
					append vehicleArray (filterString line " 	,")[1]
				)
				
				line = readLine dataStream
			)
			
			close dataStream
		)
	)
	
	fn readAAAFile file =
	(
		frameArray = #()
		
		local dataStream = openFile file mode:"r"
		
		while not eof dataStream do
		(
			local line = readLine dataStream
			
			while not (line == "") do
			(
	 			local newFrame = RSL_VehicleAnimFrame time:(line as float)
				
				line = readLine dataStream
				local lineFiltered = filterString line " "
				local newTransform = matrix3 0 
				newTransform.row4 = [lineFiltered[1] as float, lineFiltered[2] as float, lineFiltered[3] as float]
				
				line = readLine dataStream
				lineFiltered = filterString line " "
				newTransform.row1 = [lineFiltered[1] as float, lineFiltered[4] as float, lineFiltered[7] as float]
				newTransform.row2 = [lineFiltered[2] as float, lineFiltered[5] as float, lineFiltered[8] as float]
				newTransform.row3 = [lineFiltered[3] as float, lineFiltered[6] as float, lineFiltered[9] as float]
				
				newFrame.matrix = newTransform
				newFrame.additional  = readLine dataStream
				
				append frameArray newFrame
				
				line = readLine dataStream
			)
-- 			print "-=-=-=-=-=-=-=-=-=-=-"
		)
		
		close dataStream
	)
	
	fn writeAAAFile filename =
	(
		local newFile = createfile filename
		
		for i = 1 to currentVehiclePath.timeData.count do
		(
			local timeEntry = currentVehiclePath.timeData[i]
			local matrixEntry = currentVehiclePath.matrixData[i]
			matrixEntry.row4 += currentVehiclePath.pos
			
			local additionalEntry = currentVehiclePath.stringData[i]
			
			format "%\n" (formattedPrint timeEntry format:".4f") to:newFile
			format "% % %\n" (formattedPrint matrixEntry.row4.x format:".2f") (formattedPrint matrixEntry.row4.y format:".2f") (formattedPrint matrixEntry.row4.z format:".2f") to:newFile
			format "% % % " (formattedPrint matrixEntry.row1.x format:".4f") (formattedPrint matrixEntry.row2.x format:".4f") (formattedPrint matrixEntry.row3.x format:".4f") to:newFile
			format "% % % " (formattedPrint matrixEntry.row1.y format:".4f") (formattedPrint matrixEntry.row2.y format:".4f") (formattedPrint matrixEntry.row3.y format:".4f") to:newFile
			format "% % %\n" (formattedPrint matrixEntry.row1.z format:".4f") (formattedPrint matrixEntry.row2.z format:".4f") (formattedPrint matrixEntry.row3.z format:".4f") to:newFile
			format "%\n\n" additionalEntry to:newFile
		)
		
		close newFile
		
		messageBox "File saved." title:"Success!"
	)
	
	fn extract level list filetype mapType:"" =
	(
		local rageEXE = "ragebuilder_0327.exe "
		local outputDirectory = "c:\\temp_" + filetype + "\\" + level + "\\" + mapType
		local rbsfile = (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\scripts\\Extracter.rbs")
		
		makedir outputDirectory
		HiddenDOSCommand ("del " + outputDirectory + "\\*." + filetype) startpath:"c:\\"	
		
		outputDirectory += "\\"
		
		for i = 1 to list.count do
		(
			lbl_progress.text = "Progress: Extracting " + (filenameFromPath list[i])
			local image = list[i]
			
			local arguments = (rbsfile+" -input " + image + " -output " + outputDirectory + " -filetype " + filetype)
			local rageCommand = (rageEXE + arguments)
			local rootdir = RsConfigGetBinDir() 
			print rageCommand
			HiddenDOSCommand rageCommand startpath:rootdir
			
			pb_progress.value = (i as float / list.count) * 100.0
		)
		
		getFiles (outputDirectory + "*." + filetype)
	)
	
	fn buildMesh vertArray faceList name offset:[0,0,0] quat:(quat 0 0 0 0) =
	(
		local newMesh = mesh pos:offset name:name vertices:vertArray faces:faceList
		newMesh.wireColor = orange
		meshop.autoSmooth newMesh (for face in newMesh.faces collect face.index) 30.0
		meshop.autoEdge newMesh (for edge in newMesh.edges collect edge.index) 0.01 type:#SetClear
		
		rotate newMesh quat
		
		convertToPoly newMesh
		
		append bndArray newMesh
	)
	
	fn readBVH dataStream name offset:[0,0,0] quat:(quat 0 0 0 0) =
	(
		local vertexArray = #()
		local faceArray = #()
		
		while not eof dataStream do
		(
			local line = filterstring (Readline dataStream) " "
			
			case line[1] of
			(
				(tolower "v"):
				(
					append vertexArray ([(readValue (line[2] as stringStream)), (readValue (line[3] as stringStream)), (readValue (line[4] as stringStream))] )
				)
				(tolower "tri"):
				(
					append faceArray [(readValue (line[2] as stringStream)) +1, (readValue (line[3] as stringStream)) +1, (readValue (line[4] as stringStream)) +1]
				)
			)
		)
		
		buildMesh vertexArray faceArray name offset:offset quat:quat
	)
	
	fn readComposite dataStream name offset:[0,0,0] quat:(quat 0 0 0 0) =
	(
		local bndsection = "ignore"
		local vertexArray = #()
		local faceArray = #()
		
		while not eof dataStream do
		(
			local line = filterstring (Readline dataStream) " "
			
			case line[1] of
			(
				default:
				(
					bndsection = "ignore"
				)
				(tolower "v"):
				(
					bndsection = "vertices"
				)
				(tolower "tri"):
				(
					bndsection = "tris"
				)
				(tolower "matrix:"):
				(
					bndsection = "newmesh"
				)
			)
			
			if line.count < 2 then continue				
			
			case bndsection of
			(
				"newmesh":
				(
					bndsection = "ignore"
					
					buildMesh vertexArray faceArray name offset:offset quat:quat
					
					vertexArray = #()
					faceArray = #()
				)
				"vertices":
				(
					append vertexArray ([(readValue (line[2] as stringStream)), (readValue (line[3] as stringStream)), (readValue (line[4] as stringStream))])
				)
				"tris":
				(
					append faceArray [(readValue (line[2] as stringStream)) +1, (readValue (line[3] as stringStream)) +1, (readValue (line[4] as stringStream)) +1]
				)
			)
		)
	)
	
	fn readBox dataStream name offset:[0,0,0] quat:(quat 0 0 0 0) =
	(
		skipToString dataStream "size: "
		
		local line = readLine dataStream
		local lineFiltered = filterString line " 	"
		
		local width = (lineFiltered[1] as float)
		local length = (lineFiltered[2] as float)
		local height = (lineFiltered[3] as float)
		
		local position = offset
		position.z += height * 0.5
		
		local newMesh = box name:name pos:position width:width length:length height:height
		newMesh.wireColor = green
		rotate newMesh quat
		convertToPoly newMesh
		
-- 		append objectArray newMesh
	)
	
	fn readBoundfile filename offset:[0,0,0] quat:(quat 0 0 0 0) =
	(
		local name = getFilenameFile filename
		local dataStream = openFile filename

		if dataStream != undefined then
		(
			skipToString dataStream "type: "
			local type = readLine dataStream
			
			case type of
			(
				default:print type
				"bvh":
				(
					readBVH dataStream name offset:offset quat:quat
				)
				"composite":
				(
					readComposite dataStream name offset:offset quat:quat
				)
				"box":
				(
					readBox dataStream name offset:offset quat:quat
				)
				"geometry":
				(
					readBVH dataStream name
				)
				"geometry_curved":
				(
					
				)
			)
			
			close dataStream
		)
	)
	
	fn extractVehicle name =
	(
		local vehicleFiles = getFiles (project.intermediate + "/vehicles/*.ift") --("c:\\temp_ift\\vehicles\\*.ift")
		local vehicleIFT
		
		for file in vehicleFiles do
		(
			if (matchPattern file pattern:("*" + name + ".ift")) then
			(
				vehicleIFT = file
			)
		)
		
		if vehicleIFT != undefined then
		(
			local bndFiles = extract "vehicles" #(vehicleIFT) "bnd" mapType:name
			
			lbl_progress.text = "Progress: Extraction Complete"
			
			bndArray = #()
			
			for i = 1 to bndFiles.count do
			(
				lbl_progress.text = "Progress: Building " + (filenameFromPath bndFiles[i])
				
				readBoundfile bndFiles[i]
				
				pb_progress.value = (i as float / bndFiles.count) * 100.0
			)
			
			presetVehicle = bndArray[1]
			
			for i = 2 to bndArray.count do
			(
				if (superClassOf bndArray[i]) == geometryClass then
				(
					polyop.attach presetVehicle bndArray[i]
				)
			)
			
			lbl_progress.text = "Progress: Build Complete"
			
			presetVehicle.name = name
		)
		else
		(
			messageBox "Unable to find vehicle in rpf. Make sure it's not been removed." title:"Error..."
		)
	)
	
	on RSL_vehicleAnimator open do
	(
-- 		if (doesFileExist vehiclesRPF) then
-- 		(
-- 			extract "vehicles" #(vehiclesRPF) "ift"
-- 			
-- 			lbl_progress.text = "Progress: Extraction Complete"
		
			vehicleArray = getFiles (project.intermediate + "/vehicles/*.ift") --("c:\\temp_ift\\vehicles\\*.ift")
			
			ddl_vehicles.items = for entry in vehicleArray collect (getFilenameFile entry)
-- 		)
-- 		else
-- 		(
-- 			messageBox "vehicles.rpf not found. You will not be able to select a vehicle from the drop down menu." title:"Error..."
-- 		)
		
		pathArray = for object in objects where (classOf object) == RSL_VehiclePathManip collect object
		
		ddl_paths.items = (for path in pathArray collect path.name)
	)
	
	on btn_pickAAA pressed do
	(
		AAAFile = getOpenFilename caption:"Pick Vehicle Recording..." types:"Ascii(*.aaa)|*.aaa"
		
		if AAAFile != undefined then
		(
			try (delete currentVehiclePath) catch()
			
			lbl_progress.text = "Progress: Reading File"
			
			btn_pickAAA.text = (filenameFromPath AAAFile)
			readAAAFile AAAFile
			
			currentVehiclePath = RSL_VehiclePathManip()
			currentVehiclePath.filename = AAAFile
			currentVehiclePath.name = btn_pickAAA.text
			
			for entry in frameArray do
			(
				append currentVehiclePath.timeData entry.time
				append currentVehiclePath.matrixData entry.matrix
				append currentVehiclePath.stringData entry.additional
			)
			
			pathArray = for object in objects where (classOf object) == RSL_VehiclePathManip collect object
			
			ddl_paths.items = (for path in pathArray collect path.name)
			
			lbl_progress.text = "Progress: Read Complete"
		)
		else
		(
			btn_pickAAA.text = "Pick Vehicle Path File..."
		)
	)
	
	on ddl_paths selected index do
	(
		if (isValidNode pathArray[index]) then
		(
			currentVehiclePath = pathArray[index]
			
			btn_pickAAA.text = currentVehiclePath.name
			
			AAAFile = currentVehiclePath.filename
		)
		else
		(
			messageBox "The path is no longer in the scene." title:"Error..."
		)
	)
	
	on ddl_vehicles selected index do
	(
		local okToContinue = queryBox ("Ok to extract and build " + ddl_vehicles.selected + "?") title:"Warning..."
		
		if okToContinue then
		(
			extractVehicle ddl_vehicles.selected
		)
	)
	
	on pbtn_pickVehicle picked node do
	(
		pbtn_pickVehicle.text = node.name
		
		currentVehicle = node
		
		try (delete presetVehicle) catch()
		presetVehicle = undefined
	)
	
	on btn_createAnim pressed do
	(
		if (isValidNode presetVehicle) then
		(
			currentVehicle = presetVehicle
		)
		
		if (isValidNode currentVehiclePath) then
		(
			animationRange = interval ((currentVehiclePath.timeData[1] as string + "s") as time).frame ((currentVehiclePath.timeData[currentVehiclePath.timeData.count] as string + "s") as time).frame
		)
		
		if (isValidNode currentVehicle) then
		(
			if chkb_instance.checked then
			(
				currentVehicle = instance currentVehicle
			)
			
			for i = 1 to currentVehiclePath.timeData.count do
			(
				with animate on
				(
					local currentFrame = ((currentVehiclePath.timeData[i] as string + "s") as time).frame
					
					lbl_progress.text = "Progress: Animating " + currentFrame as string
					
					sliderTime = currentFrame
					
					currentVehicle.transform = currentVehiclePath.matrixData[i]
				)
			)
			
			lbl_progress.text = "Progress: Animation Complete"
		)
	)
	
-- 	on btn_updateAnim pressed do
-- 	(
-- 		if (isValidNode currentVehicle) and (isValidNode currentVehiclePath) then
-- 		(
-- 			for i = 1 to currentVehiclePath.timeData.count do
-- 			(
-- 				with animate on
-- 				(
-- 					sliderTime = ((currentVehiclePath.timeData[1] as string + "s") as time).frame
-- 					
-- 					currentVehicle.transform = currentVehiclePath.matrixData[i]
-- 				)
-- 			)
-- 		)
-- 	)
	
	on btn_updatePath pressed do
	(
		if (isValidNode currentVehicle) and (isValidNode currentVehiclePath) then
		(
			for i = 1 to currentVehiclePath.timeData.count do
			(
-- 				print ((currentVehiclePath.timeData[i] as string + "s") as time).frame
				local currentFrame = ((currentVehiclePath.timeData[i] as string + "s") as time).frame
				local matrix = at time currentFrame currentVehicle.transform
-- 				
-- 				format "F:% Pos:%\n" currentFrame position
				currentVehiclePath.matrixData[i] = matrix
			)
		)
	)
	
	on btn_savePath pressed do
	(
		if (isValidNode currentVehiclePath) and (classOf currentVehiclePath) == RSL_VehiclePathManip then
		(
			local saveFile = getSaveFilename caption:"Vehicle Path File" filename:AAAFile types:"AAA file (*.aaa)|*.aaa"
			
			if saveFile != undefined then
			(
				if (doesFileExist saveFile) then
				(
					if not (getFileAttribute saveFile #readOnly) then
					(
						writeAAAFile saveFile
					)
					else
					(
						messageBox "The file is read only." title:"Error..."
					)
				)
				else
				(				
					writeAAAFile saveFile
				)
			)
		)
	)
)

createDialog RSL_vehicleAnimator style:#(#style_toolwindow,#style_sysmenu)
