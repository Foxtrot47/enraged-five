
global RSL_TXDOpsErrorReport

struct RSL_TXDOpsError
(
	
	
	errorForm,errorListView,
	
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchErrorListViewResize() Dispatches errorListViewResize. Called when the list view is resized
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchErrorListViewResize s e =
	(
		RSL_TXDOpsErrorReport.errorListViewResize s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- errorListViewResize() Sets the column width when you resize the error report form
	---------------------------------------------------------------------------------------------------------------------------------------
	fn errorListViewResize s e =
	(
		if s.columns.count == 1 then
		(
			s.columns.Item[0].width = errorForm.width - 22
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchErrorListViewDoubleClick() Dispatches errorListViewDoubleClick. Called when you double click an item in the list view
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchErrorListViewDoubleClick s e =
	(
		RSL_TXDOpsErrorReport.errorListViewDoubleClick s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- errorListViewDoubleClick() Assigns the data to the active slot of the material editor if it is a valid class
	---------------------------------------------------------------------------------------------------------------------------------------
	fn errorListViewDoubleClick s e =
	(
		if s.selectedItems.count == 1 then
		(
			local data = s.selectedItems.Item[0].tag.value
			
			case (classOf data) of
			(
				default: -- This is just incase. It really should only ever be one of the classes bellow
				(
					format "Unknow Data Class in Error Report Listview Item : %\n" (classOf data)
				)
				Standardmaterial:
				(
					meditMaterials[1] = data
				)
				DirectX_9_Shader:
				(
					meditMaterials[1] = data
				)
				XRef_Material:
				(
					meditMaterials[1] = data
				)
				Multimaterial:
				(
					meditMaterials[1] = data
				)
				Bitmaptexture:
				(
					meditMaterials[1] = data
				)
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- errorReport() Creates an error report form from the items in the parsed array
	---------------------------------------------------------------------------------------------------------------------------------------
	fn errorReport array =
	(
		try (errorForm.close()) catch()
		
		errorForm = RS_dotNetUI.maxForm "errorForm" text:"TXD Optimiser Error Report" Size:[600,256] min:[94,90] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		
		errorListView = RS_dotNetUI.listView "errorListView" dockStyle:RS_dotNetPreset.DS_Fill
		errorListView.view = (dotNetClass "System.Windows.Forms.View").Details
		errorListView.HeaderStyle = (dotNetClass "System.Windows.Forms.ColumnHeaderStyle").Nonclickable
		errorListView.fullRowSelect = true
		errorListView.HideSelection = false
		dotNet.addEventHandler errorListView "Resize" dispatchErrorListViewResize
		dotnet.addEventHandler errorListView "MouseDoubleClick" dispatchErrorListViewDoubleClick
		
		local listViewEntries = #()
		
		for entry in array do
		(
			local listViewEntry = dotNetObject "System.Windows.Forms.ListViewItem" entry.error
			listViewEntry.tag = dotNetMXSValue entry.data
			
			append listViewEntries listViewEntry
		)
		
		errorListView.Items.AddRange listViewEntries
		
		errorForm.controls.Add errorListView
		
		errorListView.columns.add "Errors" (errorForm.width - 22)
		
		errorForm.showModeless()
	)
)

if RSL_TXDOpsErrorReport != undefined then
(
	try
	(
		RSL_TXDOpsErrorReport.errorForm.close()
	)
	catch()
)

RSL_TXDOpsErrorReport = RSL_TXDOpsError()