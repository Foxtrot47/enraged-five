
try (destroyDialog RSL_MeshCleaner) catch()

rollout RSL_MeshCleaner "R* Mesh Cleaner"
(
	local faceDataChannel
	local RSL_meshCleanIsSelected = #(1699710735, -200361706)
	group "Operate On"
	(
		checkBox chkb_selected "Selected Elements" checked:true
		CheckBox chkb_all "All"
	)
	
	on chkb_selected changed state do
	(
		chkb_selected.checked = true
		chkb_all.checked = false
	)
	
	on chkb_all changed state do
	(
		chkb_all.checked = true
		chkb_selected.checked = false
	)
	
	group "Faces"
	(
		checkBox chkb_weld "Weld Elements" checked:true
		label lbl_vertThreshold "Weld Threshold:" across:2 offset:[2,0]
		spinner spn_vertThreshold range:[0.0, 1.0, 0.0001] scale:0.0001 fieldWidth:40 offset:[3,0]
		button btn_faceRemove "Remove Interior Faces"
	)
	
	group "Edges"
	(
		checkBox chkb_UV "Keep UV Edges" checked:true
		checkBox chkb_selectedEdges "Keep Selected Edges" checked:false
		label lbl_faceThreshold "Face Threshold:" across:2 offset:[2,0]
		spinner spn_faceThreshold range:[0.0, 1.0, 0.999] scale:0.001 fieldWidth:40 offset:[3,0]
		button btn_edgeRemove "Remove Edges" width:142 offset:[-1,0]
	)
	
	group "Verts"
	(
		label lbl_edgeThreshold "Edge Threshold:" across:2 offset:[2,0]
		spinner spn_edgeThreshold range:[0.0, 1.0, 0.99] scale:0.01 fieldWidth:40 offset:[3,0]
		button btn_vertRemove "Remove Verts" width:142 offset:[-1,0]
	)
	
	progressBar pbar_progress value:100.0 color:orange width:154 offset:[-9,0]
	
	fn isSimilar face1 face2 object =
	(
		local vertArray1 = (polyOp.getVertsUsingFace face1 object) as array
		local vertArray2 = (polyOp.getVertsUsingFace face2 object) as array
	)
	
	mapped fn setSelectedFace face theObject =
	(
		faceDataChannel.setValue face (findItem theObject.selectedFaces face > 0)
	)
	
	mapped fn getSelectedFace face &outArray =
	(
		if (faceDataChannel.getValue face) then
		(
			append outArray face
		)
	)
	
	fn setFaceDataForSelected theObject =
	(
		try
		(
			simpleFaceManager.removeChannel theObject RSL_meshCleanIsSelected
		)
		catch()
		
		faceDataChannel = simpleFaceManager.addChannel theObject type:#boolean id:RSL_meshCleanIsSelected name:"RSL_MeshCleaner is Selected Channel"
		
		if faceDataChannel != undefined then
		(
			setSelectedFace (for face in theObject.faces collect face.index) theObject
		)
	)
	
	fn faceRemove faceArray theObject =
	(
		setCommandPanelTaskMode #create
		local removeArray = #()
		
		for i = 1 to faceArray.count - 1 do
		(
			pbar_progress.value = (i as float / faceArray.count * 100.0)
			
			local face1 = faceArray[i]
			local center1 = polyOp.getFaceCenter theObject face1
			local normal1 =polyOp.getFaceNormal theObject face1
			
			for f = i + 1 to faceArray.count do
			(
				local face2 = faceArray[f]
				local center2 = polyOp.getFaceCenter theObject face2
				local normal2 =polyOp.getFaceNormal theObject face2
				
				if face1 == 9 then
				(
					format "Face:% Distance:% dot:%\n" face2 (distance center1 center2) (dot normal1 normal2)
				)
				
				if (distance center1 center2) < 0.005 and (dot normal1 normal2) < -0.98 then
				(
					removeArray += #(face1, face2) 
				)
				else
				(
					if (distance center1 center2) < 0.005 then
					(
						format "Distance:% dot:%\n" (distance center1 center2) (dot normal1 normal2)
					)
				)
			)
		)
		
		polyOp.setFaceSelection theObject removeArray
		update theObject
		
		theObject.delete #face deleteIsoVerts:true
		
		if chkb_weld.checked then
		(
			theObject.weldThreshold = spn_vertThreshold.value
			polyop.setVertSelection theObject #All
			
			polyOp.weldVertsByThreshold theObject theObject.selectedVerts
			
			theUnwrapMod = UVWUnwrap()
			addModifier theObject theUnwrapMod
			
			faceArray = #()
			getSelectedFace (for face in theObject.faces collect face.index) &faceArray
			
			local faces = (for face in theObject.faces where (findItem faceArray face.index) > 0 collect face.index) as bitArray
			
			theUnwrapMod.setTVSubObjectMode 3
			theUnwrapMod.setWeldThreshold spn_vertThreshold.value
			theUnwrapMod.selectFaces faces
			theUnwrapMod.weldSelected()
		)
		
		convertToPoly theObject
		
		setCommandPanelTaskMode #modify
		
		subObjectLevel = 5
		
		faceArray = #()
		getSelectedFace (for face in theObject.faces collect face.index) &faceArray
		
		polyOp.setFaceSelection theObject faceArray
	)
	
	fn edgeRemove egdeArray theObject =
	(
		local removeArray = #()
		
		for i = 1 to egdeArray.count do
		(
			local edge = egdeArray[i]
			
			pbar_progress.value = (i as float / egdeArray.count * 100.0)
			
			local vertexArray = (polyOp.getVertsUsingEdge theObject edge) as array
			local faceArray = (polyOp.getFacesUsingEdge theObject edge) as array
			
			if faceArray.count == 2 then
			(
				local normal1 = polyOp.getFaceNormal theObject faceArray[1]
				local normal2 = polyOp.getFaceNormal theObject faceArray[2]
				
				local openEdge = false
				local selectedEdge = false
				
				if chkb_UV.checked then
				(
					local index11 = findItem (polyOp.getFaceVerts theObject faceArray[1]) vertexArray[1]
					local index12 = findItem (polyOp.getFaceVerts theObject faceArray[2]) vertexArray[1]
					
					local index21 = findItem (polyOp.getFaceVerts theObject faceArray[1]) vertexArray[2]
					local index22 = findItem (polyOp.getFaceVerts theObject faceArray[2]) vertexArray[2]
					
					local mapIndex11 = (polyop.getMapFace theObject 1 faceArray[1])[index11]
					local mapIndex12 = (polyop.getMapFace theObject 1 faceArray[2])[index12]
					
					local mapIndex21 = (polyop.getMapFace theObject 1 faceArray[1])[index21]
					local mapIndex22 = (polyop.getMapFace theObject 1 faceArray[2])[index22]
					
					if mapIndex11 != mapIndex12 or mapIndex21 != mapIndex22 then
					(
						openEdge = true
					)
				)
				
				if chkb_selectedEdges.checked then
				(
					selectedEdge = (findItem theObject.selectedEdges edge) != 0
				)
				
				if abs (dot normal1 normal2) >= spn_faceThreshold.value and not openEdge and not selectedEdge then
				(
					append removeArray edge
				)
			)
		)
		
		polyOp.setEdgeSelection theObject removeArray
		theObject.EditablePoly.remove selLevel:#Edge
		
		subObjectLevel = 5
		
		polyop.retriangulate theObject #all
	)
	
	fn vertRemove vertexArray theObject =
	(
		local removeArray = #()
		
		for i = 1 to vertexArray.count do
		(
			local vert = vertexArray[i]
			
			pbar_progress.value = (i as float / vertexArray.count * 100.0)
			
			local edgeArray = (polyOp.getEdgesUsingVert theObject vert) as array
			
			if edgeArray.count == 2 then
			(
				local vertIndexes = makeUniqueArray (((polyOp.getVertsUsingEdge theObject edgeArray[1]) as array) + ((polyOp.getVertsUsingEdge theObject edgeArray[2]) as array))
				
				if vertIndexes.count == 3 then
				(
					local secondVert1
					local secondVert2
					
					for index in vertIndexes do
					(
						if index != vert then
						(
							if secondVert1 == undefined then
							(
								secondVert1 = index
							)
							else
							(
								secondVert2 = index
							)
						)
					)
					
					local vector1 = normalize (theObject.verts[secondVert1].pos - theObject.verts[vert].pos)
					local vector2 = normalize ( theObject.verts[secondVert2].pos - theObject.verts[vert].pos)
					
					local difference = (dot vector1 vector2) < -spn_edgeThreshold.value
					
					if difference then
					(
						append removeArray vert
					)
				)
				else
				(
					append removeArray vert
				)
			)
		)
		
		polyOp.setVertSelection theObject removeArray
		theObject.EditablePoly.remove selLevel:#Vertex
		
		subObjectLevel = 5
		
		polyop.retriangulate theObject #all
	)
	
	on btn_faceRemove pressed do
	(
		pbar_progress.value = 0
		
		undo "Face Remove" on
		(
			theObject = selection[1]
			
			if theObject != undefined then
			(
				if (classOf theObject) == editable_poly then
				(
					setFaceDataForSelected theObject
					
					local faceArray = #()
					
					if chkb_selected.checked then
					(
						faceArray = for face in theObject.selectedFaces collect face.index
					)
					else
					(
						faceArray = for face in theObject.faces collect face.index
					)
					
					faceRemove faceArray theObject
					
					simpleFaceManager.removeChannel theObject RSL_meshCleanIsSelected
				)
				else
				(
					messageBox "The object is not an editable poly." title:"Error..."
				)
			)
		)
	)
	
	on btn_edgeRemove pressed do
	(
		pbar_progress.value = 0
		
		undo "Edge Remove" on
		(
			theObject = selection[1]
			
			if theObject != undefined then
			(
				if (classOf theObject) == editable_poly then
				(
					setFaceDataForSelected theObject
					
					local edgeArray = #()
					
					if chkb_selected.checked then
					(
						edgeArray = makeUniqueArray ((polyOp.getEdgesUsingFace theObject theObject.selectedFaces) as array)
					)
					else
					(
						edgeArray = makeUniqueArray ((polyOp.getEdgesUsingFace theObject theObject.faces) as array)
					)
					
					edgeRemove edgeArray theObject
					
					simpleFaceManager.removeChannel theObject RSL_meshCleanIsSelected
				)
				else
				(
					messageBox "The object is not an editable poly." title:"Error..."
				)
			)
		)
	)
	
	on btn_vertRemove pressed do
	(
		pbar_progress.value = 0
		
		undo "Vertex Remove" on
		(
			theObject = selection[1]
			
			if theObject != undefined then
			(
				if (classOf theObject) == editable_poly then
				(
					setFaceDataForSelected theObject
					
					local vertArray = #()
					
					if chkb_selected.checked then
					(
						vertArray = makeUniqueArray ((polyOp.getVertsUsingFace theObject theObject.selectedFaces) as array)
					)
					else
					(
						vertArray = makeUniqueArray ((polyOp.getVertsUsingFace theObject theObject.faces) as array)
					)
					
					vertRemove vertArray theObject
					
					simpleFaceManager.removeChannel theObject RSL_meshCleanIsSelected
				)
				else
				(
					messageBox "The object is not an editable poly." title:"Error..."
				)
			)
		)
	)
)

createDialog RSL_MeshCleaner style:#(#style_toolwindow,#style_sysmenu)