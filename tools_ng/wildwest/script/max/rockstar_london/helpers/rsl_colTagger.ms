filein "rockstar/util/lod.ms"

try CloseRolloutFloater RsCollUtil catch()

ShowObjSel = #()
HideObjSel = #()


rollout RSL_CollisionConvert "RS Collision Convert" width:170
(
	local idxNonClimable = getattrindex "Gta Collision" "Non Climbable"
	local idxSeeThrough = getattrindex "Gta Collision" "See Through"
	local idxShootThrough = getattrindex "Gta Collision" "Shoot Through"
	local idxDoesNotProvideCover = getattrindex "Gta Collision" "Does Not Provide Cover"
	local idxNonWalkable = getattrindex "Gta Collision" "Non Walkable"
	
	local idxNonCameraCollidable = getattrindex "Gta Collision" "Non Camera Collidable"
	
	local idxCollCameraTest = getattrindex "Gta Collision" "Camera"
	local idxCollPhysicsTest = getattrindex "Gta Collision" "Physics"
	local idxCollHiDetailTest = getattrindex "Gta Collision" "Hi Detail"

	
	
	group "Convert Collision System"
	(
		button btn_convertToOld "Camera Collidable to Hi|Physics" width:160 align:#right
		--button btn_convertToNew "Convert to New System"
	)
	
	fn converttoNew obj =
	(
		setattr obj idxNonClimable false
		setattr obj idxSeeThrough false
		setattr obj idxShootThrough false
		setattr obj idxNonWalkable false
		setattr obj idxNonCameraCollidable false
		
		setattr obj idxCollHiDetailTest true
		setattr obj idxCollPhysicsTest true		
		setattr obj idxCollCameraTest false
		
	)
	
	fn converttoOld obj =
	(
		
		
	)
	
	
	mapped fn convertCollision obj = --newsystem:true=
	(
		if getattrclass obj == "Gta Collision" then
		(
			if getattr obj idxNonCameraCollidable then( -- OBject is flagged to be excluded from Cmaera Test
				setattr obj idxCollCameraTest false
				setattr obj idxCollHiDetailTest true
				setattr obj idxCollPhysicsTest true					
				
			)
			else		(
				
			)
			
			/*
			IsValid = false
			case of
			(
				(getattr obj idxNonClimable):isValid = true
				(getattr obj idxSeeThrough):isValid = true
				(getattr obj idxShootThrough):isValid = true
				(getattr obj idxDoesNotProvideCover):isValid = true
				(getattr obj idxNonWalkable):isValid = true				
				(getattr obj idxNonCameraCollidable==false):isValid = true
			)
			*/
			
			
		)
	)
	
	
	-- Event Handlers --
	on btn_convertToOld pressed do
	(
		convertCollision (helpers)
	)
	
)

rollout RSL_ColFilter "RS Collision Filter" width:170
(
	group "Hide/Show by Category"
	(
		checkbutton chk_geometry "Geometry" width:150
		checkbutton chk_collision "Show Rage Collision" width:150
	)
		group "Filter By Collision Object Type"
	(
		checkbox chk_Coll_Mesh "Col Mesh" checked:false
		checkbox chk_Coll_Box "Col Box" checked:false
		checkbox chk_Coll_Capsule "Col Capsule" checked:false
		checkbox chk_Coll_Cylinder "Col Cylinder" checked:false
		checkbox chk_Coll_Plane "Col Plane" checked:false
		checkbox chk_Coll_Sphere "Col Sphere" checked:false
		
	)
	
	group "Collison Filter"
	(
	
	checkbutton chk_hiColl "Filter Hi Detail Coll" checked:false width:150
	checkbutton chk_CamColl "Filter Camera Coll" checked:false width:150
	checkbutton chk_PhyColl "Filter Physics Coll" checked:false width:150
	)
	

	
	button btn_SelectType "Select by Type" width:150
	
	group "Set Collision Flags on Selected"
	(		
		checkbox chk_Flag_Cam "Camera" checked:false
		checkbox chk_Flag_Phy "Physics" checked:false
		checkbox chk_Flag_HiCol "Hi Detail" checked:false
	)
	
	button btn_SetColFlags "Set Flags" width:150
	
	
	-- Additional check to filter object by its object type 
	fn RSFilterCollObjectType obj = (
		case of
			(
				(classof obj == Col_Sphere and chk_Coll_Sphere.checked):isValid = true
				(classof obj == Col_Mesh and chk_Coll_Mesh.checked):isValid = true
				(classof obj == Col_Plane and chk_Coll_Plane.checked):isValid = true
				(classof obj == Col_Capsule and chk_Coll_Capsule.checked):isValid = true
				(classof obj == Col_Cylinder and chk_Coll_Cylinder.checked):isValid = true
				(classof obj == Col_Box and chk_Coll_Box.checked):isValid = true
				(chk_Coll_Box.checked == false and chk_Coll_Cylinder.checked == false and chk_Coll_Capsule.checked == false \
					and chk_Coll_Plane.checked == false and chk_Coll_Mesh.checked == false \
					and chk_Coll_Sphere.checked == false):isValid = true
				default:isValid = false
				
			)
		isValid
	)
	
	
	mapped fn filterAllColl obj = (
		if RSFilterCollObjectType obj then	(		
			if RsIsColHI obj and chk_hiColl.checked == false then
				selectmore obj
			if RsIsColCamLod obj and chk_CamColl.checked == false then
				selectmore obj
			if RsIsColPhyLod obj and chk_PhyColl.checked == false then
				selectmore obj
		)

	)
	
	mapped fn RSshowCollHI obj = (	
		if RSFilterCollObjectType obj then(	
			
		if RsIsColHI obj then
			append ShowObjSel obj
		else
			append HideObjSel obj
		)
	)
	mapped fn RSshowCollCamLOD obj = (	
		if RSFilterCollObjectType obj then (				
			if RsIsColCamLod obj then
				append ShowObjSel obj
			else
				append HideObjSel obj			
		)
	)
	
	mapped fn RSshowCollPhyLOD obj = (
	if RSFilterCollObjectType obj then (
		if RsIsColPhyLod obj then 			
			append ShowObjSel obj
		else
			append HideObjSel obj		
		)
	)
	
	
	mapped fn FilterColl obj =
	(
		--if RSFilterCollObjectType obj then (		
		if getattrclass obj == "Gta Collision" then (
			case of
			(
				(RsIsColPhyLod obj and chk_PhyColl.checked):append ShowObjSel obj
				(RsIsColCamLod obj and chk_CamColl.checked):append ShowObjSel obj
				(RsIsColHI obj and chk_hiColl.checked):append ShowObjSel obj
				(chk_hiColl.checked == false and chk_CamColl.checked == false and chk_PhyColl.checked == false):append ShowObjSel obj
				default:append HideObjSel obj
			)
		)
		else (
			append HideObjSel obj
		)
		
		--)	
	)
	
	
	
	mapped fn HideShowCollision obj state = (
		-- We add valid objs to a selection as its much faster to hide them  than indivually	
		if state then	-- If the button is checked on then
		(
			if getattrclass obj == "Gta Collision" then
			(
				if RSFilterCollObjectType obj then	(						
						append ShowObjSel obj	
					)	
					else	(
						append HideObjSel obj
				)
			)	
			else
				append HideObjSel obj				
			
		)
		else(
			append ShowObjSel obj		
		)
		
		
		
	)
	
	mapped fn SetColFlags obj = (
		if getattrclass obj == "Gta Collision" then (
			idxCollCameraTest = getattrindex "Gta Collision" "Camera"
			idxCollPhysicsTest = getattrindex "Gta Collision" "Physics"
			idxCollHiDetailTest = getattrindex "Gta Collision" "Hi Detail"
			
			setattr obj idxCollCameraTest (chk_Flag_Cam.checked)
			setattr obj idxCollPhysicsTest (chk_Flag_Phy.checked)
			setattr obj idxCollHiDetailTest (chk_Flag_HiCol.checked)

		)
		
	)
	
	-- Simple functio to hide or show our selection defiined by the passed in state
	fn showHideObj =
	(
		unhide ShowObjSel
		hide HideObjSel
	)
	
	
	-- Generic funtion to process the collision categories
	fn processCollision obj =
	(
		ShowObjSel = #()
		HideObjSel = #()
		HideShowCollision helpers chk_collision.checked
		showHideObj()
	)
	
	-----------------------------------
	--	EVENT HANDLERS
	----------------------------------

	
	---------------
	--------------
	
	-- Show/Hide Hi Lod Collision --
	on chk_hiColl changed state do	(			
		ShowObjSel = #()
		HideObjSel = #()
		FilterColl (Helpers)
		showHideObj()			
	)	
	--
	
	on chk_CamColl changed state do	(
		ShowObjSel = #()
		HideObjSel = #()
		FilterColl (Helpers)
		showHideObj()		
	)
	
	on chk_PhyColl changed state do	(	
		ShowObjSel = #()
		HideObjSel = #()
		FilterColl (Helpers)
		showHideObj()
	)
	
	---------------
	--------------
	
	-- Checkbox filters --
	on chk_Coll_Mesh changed state do (
		processCollision helpers
	)	
	on chk_Coll_Box changed state do (
		processCollision helpers
	)	
	on chk_Coll_Capsule changed state do (
		processCollision helpers
	)	
	on chk_Coll_Cylinder changed state do (
		processCollision helpers
	)	
	on chk_Coll_Plane changed state do (
		processCollision helpers
	)
	on chk_Coll_Sphere changed state do (
		processCollision helpers
	)
	
	---------------
	--------------
	
	on btn_SelectType pressed do
	(
		clearSelection()
		filterAllColl (helpers)		
	)
	
	on chk_geometry changed state do (
		hideByCategory.geometry = state	
	)
	
	on chk_collision changed state do (
		hideByCategory.helpers = false -- Make sure we unhide any helpers
		processCollision helpers
	)

	on btn_SetColFlags pressed do
	(
		SetColFlags (selection)
	)
	
	
)


----------------------------------------
---		COLLISION TAGGER 	----
----------------------------------------

rollout RSL_ColTagger "RS Collision Tagger" width:170
(

	local idxNonClimable = getattrindex "Gta Collision" "Non Climbable"
	local idxNonWalkable = getattrindex "Gta Collision" "Non Walkable"
	local idxNonCameraCollidable = getattrindex "Gta Collision" "Non Camera Collidable"
	local idxdynamic = getattrindex "Gta Object" "Is Dynamic"
	local idxFixed = getattrindex "Gta Object" "Is Fixed"
	local idxFragment = getattrindex "Gta Object" "Is Fragment"
	local idxAttribute = getattrindex "Gta Object" "Attribute"
	
	
	-- RexGetCollisionFlags [material]. returns undefined on 
	-- RexsetCollisionFlags [material]. returns undefined on
	
	checkbox chk_nonClimbale "Non Climable" checked:true
	checkbox chk_nonWalkable "Non Walkable" checked:true
	group ""
	(
	checkbox chk_NonCameraCollidable "Non Camera Collidable" checked:true
	--checkbox chk_filterbySize "By Size" checked:false across:2
		spinner spn_size "Max(m)" align:#right range:[0,100,0.4]
	)
	
	button btn_Process "Set Flags" width:160 align:#center	
	button btn_Remove "Remove Flags" width:160 align:#center
	button btn_Select "Select Flagged Objects" width:160 align:#center
	label lbl_txt "Works on a selection or entire" align:#left
	label lbl_txt2 "scene if nothing is selected" align:#left
	checkbox chk_Rexbound "Process Rexbound Materials" checked:true
	checkbox chk_Collision "Process Collision Objects" checked:true
	
	-- Variable to determine if we are setting or removing the flags
	local setflags = true
	
	-- FUNCTIONS --	
	
	--
	-- Get the local size from a object
	--
	fn CalculateVol obj =
	(
		local valset = true
		local boundbox = obj.max - obj.min
		boundbox = boundbox / 2.0
		volume = length boundbox
		
		if volume > spn_size.value then	(	
			valset = false
		)
		valset
	)
	
	fn GetVolumefromMaterial mat =
	(
		local smallestbound = 0;
		local valset = true
		for objtype in (refs.dependents mat) do
		(
			
			if classof objtype == Col_Mesh then
			(
				
				if CalculateVol objtype == false then
					valset = false
					
				
				
			)
		)
		valset;
	)
	
	--
	-- Sets a defined flag from the passed in flag number
	--
	fn setObjflag flagfield flag =
	(
		local tempFlag = bit.shift 1 Flag
		flagField = bit.or flagField tempFlag
		flagField;
		
	)
	
	--
	-- Removes a defined flag from the passed in flag number
	--
	fn removeObjflag flagField flag=
	(
		local tempFlag = bit.shift 1 Flag
		tempFlag = bit.not tempFlag;
		flagField = bit.and flagField tempFlag
		flagField;
	)
	
	
	-- Set Collision/RexBound Material Attribute
	--
	-- Yes this looks crazy to read
	fn SetObj obj =
	(
		if classof obj == RexBoundMtl then(			
			print ("Found Valid RexBound Material: " + obj.name)
		)
		else(
			print ("Found Valid Collision Object: " + obj.name)
		)
		--------------------------------------------------------------------------------
		if chk_nonClimbale.checked then
		(				
			if setflags == true then
				(
				if classof obj == RexBoundMtl then(
					flags = RexGetCollisionFlags obj
					if not bit.and flags (bit.shift 1 1) > 0 then(													
						RexsetCollisionFlags obj (setObjflag flags 1)	
						)
					)
				else(					
					setattr obj idxNonClimable true
					)
			)
			else(   -- Remove the Flags
				if classof obj == RexBoundMtl then(
						flags = RexGetCollisionFlags obj
						if bit.and flags (bit.shift 1 1) > 0 then
							RexsetCollisionFlags obj (removeObjflag flags 1)	
					)
				else	(
						setattr obj idxNonClimable false
					)
			)
		)
		--------------------------------------------------------------------------------
		if chk_nonWalkable.checked then
		(			
			if setflags == true then(
				if classof obj == RexBoundMtl then(
					flags = RexGetCollisionFlags obj
					if not bit.and flags (bit.shift 1 5) > 0 then						
						RexsetCollisionFlags obj (setObjflag flags 5)						
				)
				else(	
					setattr obj idxNonWalkable true
				)
			)
			else(    -- Remove the Flags
					if classof obj == RexBoundMtl then(
						flags = RexGetCollisionFlags obj
						if bit.and flags (bit.shift 1 5) > 0 then
							RexsetCollisionFlags obj (removeObjflag flags 5)	
			
				)
				else
				(
				setattr obj idxNonWalkable false
				)
			)
		)	
		--------------------------------------------------------------------------------
		if chk_NonCameraCollidable.checked then
		(
			if setflags == true then(
				if classof obj == RexBoundMtl then(
					
					if GetVolumefromMaterial obj then(
						flags = RexGetCollisionFlags obj
						if not bit.and flags (bit.shift 1 6) > 0 then
						(
							RexsetCollisionFlags obj (setObjflag flags 6)	
						)
					)

				)
				else(	
					if CalculateVol obj then					
						setattr obj idxNonCameraCollidable true
					
				)
			)
			else(  -- Remove the Flags
					if classof obj == RexBoundMtl then(
						--if GetVolumefromMaterial obj then(
							flags = RexGetCollisionFlags obj
							if bit.and flags (bit.shift 1 6) > 0 then
							(
								RexsetCollisionFlags obj (removeObjflag flags 6)	
							)
						--)
			
				)
				else
				(
					if CalculateVol obj then					
						setattr obj idxNonCameraCollidable false
					
					
				)
			)
		)
		
	)
	
	mapped fn ProcessScene obj =
	(
		--print obj
		if classof obj == RexBoundMtl then(
			SetObj obj 
		)
		else
		(		
			
		isGTAObject = (getAttrClass obj) == "Gta Collision"		
		if isGTAObject then	(
			if classof obj != Col_Mesh then		-- Make sure we only tag primitive collsion and mesh collison uses rexbound materials		
				SetObj obj 
				/*
				if getattr obj idxdynamic == true and getattr obj idxFixed == false then
					SetObj obj 
				else
				if getattr obj idxFragment == true then
					SetObj obj	
				*/
			)
		)
	)	
	
	-- Get Entire Scene Rexbound Materials
	--
	--
	fn getRexBoundMaterials =
	(
		local RexMaterials = for i in getClassInstances RexBoundMtl asTrackViewPick:false collect (i)
	)
	
	-- Get Rexbound Materials from slected Object.
	--
	--
	fn getSelRexBoundMaterials obj=
	(
		RexMaterials = #()
		if obj.material != undefined then(
		local RexMaterials = for i in getClassInstances RexBoundMtl target:obj.material collect (i)
		)
		RexMaterials
	)
	
	-- Check a selected obj and determine if the flag has been set
	--
	--
	fn checkRexBoundMaterials obj flag=
	(
		local isflag = false		
		validRexBounds = getSelRexBoundMaterials obj	
		if validRexBounds.count > 0 then
		(			
			for RexBoundMat in validRexBounds do
			(
				flags = RexGetCollisionFlags RexBoundMat
				if bit.and flags (bit.shift 1 flag) > 0 then
					isflag = true
			)
		)
		isflag
		
	)
	
	
	--
	--
	--
	

	
	
	
	

	

	--
	--
	--
	fn getMeshVolume object =
	(
		--local theMesh = snapshotasmesh object
		local theMesh = snapshotasmesh (col2mesh (copy object))
		local numFaces = theMesh.numfaces 
			
			for i = 1 to numFaces do 
			( 
				local Face= getFace theMesh i 
				local vert2 = getVert theMesh Face.z 
				local vert1 = getVert theMesh Face.y 
				local vert0 = getVert theMesh Face.x 
				local dV = Dot (Cross (vert1 - vert0) (vert2 - vert0)) vert0
				result += dV 
			)
			
			delete theMesh
			
			result /= 6 
	)
	
	--
	--
	--
	fn main =
	(
		clearlistener()
		if selection.count == 0 then
		(
			if querybox ("This will Process the entire Scene! Continue?") then
			(
			if chk_Collision.checked then
				ProcessScene (helpers)
			if chk_Rexbound.checked then
				ProcessScene (getRexBoundMaterials())
				)
		)
		else
		(
			local validObjs = #()
			for i in selection where getAttrClass i == "Gta Collision" do
			(
				if chk_Collision.checked then
					append validObjs i
				if chk_Rexbound.checked then (					
					if i.material != undefined do(					
						append validObjs (getSelRexBoundMaterials i)
					)
				)
				
			)
			ProcessScene (validObjs)
		)			
	)
	
	fn selFlagObjects =
	(
		clearSelection()
		for obj in helpers do
		(
			isGTAObject = (getAttrClass obj) == "Gta Collision"
			if isGTAObject then
			(
				if chk_nonClimbale.checked then
				(
					if classof obj == Col_Mesh then
					(
						if chk_Rexbound.checked then
							if checkRexBoundMaterials obj 1 then selectmore obj
					)
					else
					(
						if chk_Collision.checked then
							if getattr obj idxNonClimable then selectmore obj
					)
				)
				if chk_nonWalkable.checked then
				(	
					if classof obj == Col_Mesh then
					(
						if chk_Rexbound.checked then
							if checkRexBoundMaterials obj 5 then selectmore obj
					)
					else
					(	
						if chk_Collision.checked then
							if getattr obj idxNonWalkable then selectmore obj
					)
				)
				if chk_NonCameraCollidable.checked then
				(	
					if classof obj == Col_Mesh then
					(
						if chk_Rexbound.checked then
							if checkRexBoundMaterials obj 6 then selectmore obj
					)
					else
					(	
						if chk_Collision.checked then
							if getattr obj idxNonCameraCollidable then selectmore obj
					)
				)
			)
		)
	)
	
	
	-- EVENT HANDLERS --
	
	on btn_Process pressed do
	(
		setflags = true
		main()
	)
	
	on btn_Remove pressed do
	(
		setflags = false
		main()
	)
	
	on btn_Select pressed do
	(
		selFlagObjects()
	)
	
)

RsCollutil = newRolloutFloater "Rockstar Collision Utils" 200 900 50 126 
addRollout RSL_ColTagger RsCollutil rolledup:false border:true
addRollout RSL_ColFilter RsCollutil rolledup:false border:true
addRollout RSL_CollisionConvert RsCollutil rolledup:false border:true





