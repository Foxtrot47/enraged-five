-------------------------------------------------------------------------------------------------------------------------------------------
-- RSL_TXDOpsStructs
-- By Paul Truss
-- Senior Technical Artist
-- Rockstar Games London
-- 15/10/2010
--
-- Class structures used by TXD Ops
-------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------
-- RSL_TXD
-- TXD class used in view/edit mode
---------------------------------------------------------------------------------------------------------------------------------------
struct RSL_TXD
(
	name = "", -- TXD name as it appeas in an objects attributes
	objectArray = #(), -- array of RSL_TextureObjects assigned to the TXD
	textureArray = #(), -- array of shared textures assigned to the TXD
	memory = 0.0, -- estimated memory for the textures in the TXD
	uniqueTextureArray = #(), -- array of arrays for each object containing textures applied to objects in the TXD which only appear on one object and are therefore packed with the object
	uniqueMemory = 0.0, -- estimated memory for the unique textures
	TXDLinks = #(), -- array of RSL_TXDLinks between other TXDs in the scene
	centerPoint = [0,0,0], -- center of the TXD in world co-ordinates, based on the average center of the objects assigned to the TXD
	TXDCloud, -- no longer used. The TXDCloud was a manipulator that showed the area the TXD covered by drawing lines from the centerPoint to each object.
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- isUnique() Checks to see if the parsed texture is in the parsed array. Returns true, or false if not found.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn isUnique array texture =
	(
		local result = true
		
		for entry in array do
		(
			if (toLower entry.name) == (toLower texture.name) then
			(
				result = false
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- isSharedTexture() Checks to see if the parsed texture is used by another object in the struct variable objectArray.
	-- Returns true, or false if the texture is not shared.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn isSharedTexture texture =
	(
		local result = false
		local count = 0
		
		-- search through the objects fr the texture 
		for entry in objectArray do
		(
			-- if the texture is not unique, we increase the count by 1
			if not (isUnique entry.textures texture) then
			(
				count += 1
			)
		)
		
		-- if the count is greater than 1, the texture is a shared texture
		if count > 1 then
		(
			result = true
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- sortTextures() Sorts the textures in the parsed array into shared and unique textures.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn sortTextures array index =
	(
		for texture in array do 
		(
			-- the texture is shared so we make sure it's not already in the struct variable textureArray before appending it and
			-- increasing to struct variable memory
			if (isSharedTexture texture) then
			(
				if (isUnique textureArray texture) then
				(
					append textureArray texture
					memory += texture.memory
				)
			)
			else
			(
				-- the texture is unique so we append it to the correct array via the parsed index in the struct variable uniqueTextureArray and
				-- increase the struct variable uniqueMemory
				append uniqueTextureArray[index] texture
				uniqueMemory += texture.memory
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- initialise() Sets up the struct variables. The optional single variable is used when we create an RSL_TXD with only one object.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn initialise single:false =
	(
		memory = 0.0
		uniqueMemory = 0.0
		textureArray = #()
		uniqueTextureArray = #()
		uniqueTextureArray.count = objectArray.count
		
		for i = 1 to objectArray.count do
		(
			-- set the TXD name for the RSL_TextureObject and the object's attributes
			objectArray[i].TXD = name
			setAttr objectArray[i].object (getattrindex "Gta Object" "TXD") name
			
			-- add the object center to the centerPoint
			centerPoint += objectArray[i].object.center
			
			uniqueTextureArray[i] = #()
			
			-- if the optional single variable is false, we can sort the textures as normal
			if not single then
			(
				sortTextures objectArray[i].textures i
			)
		)
		
		-- if the optional single variable is true, we add all the RSL_TextureObject's textures to the textureArray. This is to ensure the the otimisation functions
		-- will consider all textures. If we sorted the textures for just RSL_TextureObject, they would all be put into the uniqueTextureArray and would not get considered
		-- by the packing functions.
		if single then
		(
			textureArray = for texture in objectArray[1].textures collect
			(
				memory += texture.memory
				texture
			)
		)
		
		-- divide the centerPoint by the number of RSL_TextureObjects so that we end up with a center point in world co-ordinates
		centerPoint /= objectArray.count
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getMemory() Sets the struct variable memory based on the memory for each texture in the struct variable textureArray.
	-- Returns the memory.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getMemory =
	(
		local result = 0.0
		
		for texture in textureArray do
		(
			result += texture.memory
		)
		
		memory = result
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getCenterPoint() Sets the struct variable centerPoint based on the average center of all the objects assigned to the TXD.
	-- Returns the centerPoint.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getCenterPoint =
	(
		local result = [0,0,0]
		
		for object in objectArray do
		(
			result += object.object.center
		)
		
		result /= objectArray.count
		
		centerPoint = result
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- toString() Generates and returns a stringstream from the struct variables. Only used for debugging.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn toString =
	(
		local result = stringStream ""
		
		format "TXD:%\nObjects:\n" name
		
		for object in objectArray do
		(
			format "\t%\n" object.name
		)
		
		format "Shared Textures:\n"
		
		for texture in textureArray do
		(
			format "\t%\n" texture.name
		)
		
		format "Shared Texture Memory:%K\n" memory
		
		format "Unique Textures:\n"
		
		for i = 1 to objectArray.count do
		(
			if uniqueTextureArray[i].count > 0 then
			(
				format "\t%\n" objectArray[i].name
				
				for entry in uniqueTextureArray[i] do
				(
					format "\t\t%\n" entry.name
				)
			)
		)
		
		format "Unique Texture Memory:%K\n" uniqueMemory
		
		format "TXD Links:\n"
		
		for entry in TXDLinks do
		(
			format "\t%\n" entry
		)
		
		format "Center Point:%\n\n" centerPoint
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- toTreeView() Creates treeview nodes from the struct variables and assigns them to the parsed treeview. The parsed this 
	-- is the RSL_TXD that called the function and gets tagged to the root TXDRootNode.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn toTreeView treeView this =
	(
		-- this is the root node for the RSL_TXD. All other nodes created get added to this node
		local TXDRootNode = treeView.Nodes.Add name
		TXDRootNode.tag = dotNetMXSValue this
		
		-- create the objects node and for each object in the struct variable objectArray, create a node and add them to the objectsRootNode node
		local objectsRootNode = TXDRootNode.Nodes.Add ("Objects: " + objectArray.count as string)
		objectsRootNode.tag = dotNetMXSValue objectArray
		
		for i = 1 to objectArray.count do
		(
			local count = ""
			local objectNode = objectsRootNode.Nodes.Add objectArray[i].name
			objectNode.tag = dotNetMXSValue objectArray[i]
			
			if (classOf uniqueTextureArray[i]) == Array then
			(
				if uniqueTextureArray[i].count > 0 then
				(
					count = uniqueTextureArray[i].count as string
					objectNode.text = (objectArray[i].name + " : Unique Textures: " + count)
					
					for texture in uniqueTextureArray[i] do
					(
						local uniqueTextureNode = objectNode.Nodes.Add texture.name
						uniqueTextureNode.tag = dotNetMXSValue texture
					)
				)
			)
		)
		
		-- create the textures node and for each texture in the struct variable textureArray, create a node and add them to the texturesRootNode node
		local texturesRootNode = TXDRootNode.Nodes.Add ("Shared Textures: " + textureArray.count as string)
		texturesRootNode.tag = dotNetMXSValue textureArray
		
		for texture in textureArray do
		(
			local textureNode = texturesRootNode.Nodes.Add texture.name
			textureNode.tag = dotNetMXSValue texture
		)
		
		local sharedMemoryRootNode = TXDRootNode.Nodes.Add ("Shared Texture Memory: " + memory as string + "K")
		sharedMemoryRootNode.tag = dotNetMXSValue memory
		
		local uniqueMemoryRootNode = TXDRootNode.Nodes.Add ("Unique Texture Memory: " + uniqueMemory as string + "K")
		uniqueMemoryRootNode.tag = dotNetMXSValue uniqueMemory
		
		
		-- create the TXD links node and for each texture in the struct variable TXDLinks, create a node and add them to the TXDLinksRootNode node
		local TXDLinksRootNode = TXDRootNode.Nodes.Add ("TXD Links: " + TXDLinks.count as string)
		TXDLinksRootNode.tag = dotNetMXSValue TXDLinks
		
		for entry in TXDLinks do
		(
			local TXDName
			
			for TXD in entry.TXDArray where TXD.name != this.name do
			(
				TXDname = TXD.name
			)
			
			local TXDLinkNode = TXDLinksRootNode.Nodes.Add (TXDName + " : " + entry.textureArray.count as string)
			TXDLinkNode.tag = dotNetMXSValue entry
			
			local linkMemory = 0.0
			
			for texture in entry.textureArray do
			(
				local textureNode = TXDLinkNode.Nodes.Add texture.name
				textureNode.tag = dotNetMXSValue texture
				
				linkMemory += texture.memory
			)
			
			TXDLinkNode.text += " : " + linkMemory as string + "K"
		)
		
		-- if there are shared textures, we expand the node
		if textureArray.count > 0 then
		(
			TXDRootNode.expand()
		)
	)
)

---------------------------------------------------------------------------------------------------------------------------------------
-- RSL_TXDLink
-- TXD Link class used in view/edit and parentise modes
---------------------------------------------------------------------------------------------------------------------------------------
struct RSL_TXDLink
(
	name = "", -- the name of the TXD link which is based on the RSL_TXDs in the struct variable TXDArray
	ignored = false, -- if a TXD link is ignored, it does not get considered when creating parent TXDs
	TXDArray = #(), -- array of RSL_TXDs that are linked by shared textures
	TextureArray, -- array of textures shared by the RSL_TXDs in the struct variable TXDArray
	memory = 0.0, -- the estimated memory for all the textures in the struct variable TextureArray
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- toString() Generates and returns a stringstream from the struct variables. Only used for debugging.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn toString =
	(
		format "TXD Link:%\n\tTXDs:\n" name
		
		for TXD in TXDArray do
		(
			format "\t\t%\n" TXD.name
		)
		
		format "\tTextures:\n"
		
		for texture in TextureArray do
		(
			format "\t\t%\n" texture.name
		)
		
		format "\tMemory:%\n" memory
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- isUniqueTexture() Checks to see if the parsed texture is in the parsed array. Returns true, or false if not found.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn isUniqueTexture array texture =
	(
		local result = true
		
		for entry in array do
		(
			if (toLower entry.name) == (toLower texture.name) then
			(
				result = false
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- isUniqueTXD() Checks to see if the parsed TXD is in the parsed array. Returns true, or false if not found.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn isUniqueTXD array TXD =
	(
		local result = true
		
		for entry in array do
		(
			if entry.name == TXD.name then
			(
				result = false
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- sharedTexture() Checks to see if the parsed texture is in the any of the TXDs in the struct variable TXDArray.
	-- Returns true, or false if not found.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn sharedTexture texture thisTXD =
	(
		local result = false
		
		for otherTXD in TXDArray where otherTXD.name != thisTXD.name do 
		(
			for otherTexture in otherTXD.textureArray do
			(
				if otherTexture.name == texture.name then
				(
					result = true
					exit
				)
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- initialise() Sets up the struct variables. 
	---------------------------------------------------------------------------------------------------------------------------------------
	fn initialise =
	(
		TextureArray = #()
		
		-- get an array of unique TXDs and assign it to the struct variable TXDArray
		local tempTXDArray = #()
		
		for entry in TXDArray do
		(
			if (isUniqueTXD tempTXDArray entry) then
			(
				append tempTXDArray entry
			)
		)
		
		TXDArray = tempTXDArray
		tempTXDArray = undefined
		
		-- collect the unique shared textures and append them to the struct variable TextureArray
		for entry in TXDArray do
		(
			for texture in entry.textureArray do
			(
				if (isUniqueTexture TextureArray texture) and (sharedTexture texture entry) then
				(
					append TextureArray texture
					
					memory += texture.memory
				)
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getMemory() Sets the struct variable memory based on the memory for each texture in the struct variable textureArray.
	-- Returns the memory.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getMemory =
	(
		memory = 0.0
		
		for entry in textureArray do
		(
			memory += entry.memory
		)
		
		memory
	)
)

---------------------------------------------------------------------------------------------------------------------------------------
-- RSL_ParentTXD
-- Parent TXD class used in parentise mode.
---------------------------------------------------------------------------------------------------------------------------------------
struct RSL_ParentTXD
(
	name = "", -- the name of the parent TXD
	type = "Global", -- the type, either global "Global" or local "<The name of the map file>".
	export = true, -- controls weather the parent TXD can be exported or not
	ChildArray = #(), -- array of TXDs assigned to the parent TXD
	TextureArray = #(), -- array of textures assigned to the parent TXD
	memory = 0.0, -- the estimated memory for all the textures in the struct variable TextureArray
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- isSharedTexture() Checks to see if the parsed texture is in the struct variable textureArray.
	-- Returns true, or false if not found.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn isSharedTexture texture =
	(
		local result = false
		
		for entry in textureArray do
		(
			if entry.name == texture.name then
			(
				result = true
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- findRoot() searches for the node in the parsed treeView based on the parsed name
	---------------------------------------------------------------------------------------------------------------------------------------
	fn findRoot treeView name =
	(
		local result = -1
		
		for i = 0 to treeView.Nodes.count - 1 do
		(
			if treeView.Nodes.Item[i].text == name then
			(
				result = i
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getTextureMemory() gets the texture memory for the parsed texture by searching through the children in the struct
	-- variable ChildArray.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getTextureMemory texture =
	(
		local result = 0.0
		
		for child in ChildArray do
		(
			for childTexture in child.textureArray where childTexture.name == texture.name do
			(
				result = childTexture.memory
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getMemory() Sets the struct variable memory based on each texture in the struct variable textureArray.
	-- Also sets the memory for each texture in the struct variable textureArray.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getMemory =
	(
		memory = 0.0
		
		for entry in TextureArray do
		(
			local textureMemory = getTextureMemory entry
			entry.memory = textureMemory
			
			memory += textureMemory
		)
	),
	
	
	fn getTextureSize texture =
	(
		local result = [0,0]
		
		for child in ChildArray do
		(
			for childTexture in child.textureArray where childTexture.name == texture.name do
			(
				result = childTexture.size
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- toString() Returns a string based on the struct variables. Used when saving parent TXDs
	---------------------------------------------------------------------------------------------------------------------------------------
	fn toString =
	(
		local result = "" as stringStream
		
		format "txd\nname = %\nparent = root\ntype = 3\ntextures = %\n" name TextureArray.count to:result
		
		for entry in TextureArray do
		(
			format "texture = %\n" entry.name to:result
		)
		
		result as string
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- toCSV() Returns a string based on the struct variables that is comma separated
	---------------------------------------------------------------------------------------------------------------------------------------
	fn toCSV =
	(
		local result = "" as stringStream
		
		for texture in TextureArray do
		(
			texture.size = getTextureSize texture
			format "%,% x %," texture.name texture.size.x texture.size.y to:result
			
			local mapFileArray = #()
			
			for child in ChildArray do
			(
				for childTexture in child.textureArray do
				(
					if childTexture.name == texture.name then
					(
						appendIfUnique mapFileArray child.mapfile
					)
				)
			)
			
			for i = 1 to mapFileArray.count do
			(
				format "%" mapFileArray[i] to:result
				
				if i < mapFileArray.count then
				(
					format "," to:result
				)
			)
			
			format "\n" to:result
		)
		
		result as string
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- toTreeView() Creates treeview nodes from the struct variables and assigns them to the parsed treeview. The parsed this 
	-- is the RSL_ParentTXD that called the function and gets tagged to the TXDRootNode.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn toTreeView treeView this =
	(
		-- set the icon based on what the type is
		local imageIndex = 1
		
		if type != "Global" then
		(
			imageIndex = 2
		)
		
		-- check if the rootNode alread exists based on the type. This could either be a global or local root
		local index = findRoot treeView type
		local mapRoot
		
		if index > -1 then
		(
			mapRoot = treeView.Nodes.Item[index]
		)
		else
		(
			mapRoot = treeView.Nodes.Add type
			mapRoot.imageIndex = imageIndex
			mapRoot.SelectedImageIndex = imageIndex
			mapRoot.tag = dotNetMXSValue type
		)
		
		-- create the rootNode for the parent TXD and set the imageIndex which controls the icon used
		local TXDRootNode = mapRoot.Nodes.Add name
		TXDRootNode.imageIndex = imageIndex
		TXDRootNode.SelectedImageIndex = imageIndex
		TXDRootNode.tag = dotNetMXSValue this
		
		-- create a node for the children and for each child in the struct variable ChildArray, create a node and add it to the ChildRootNode
		local ChildRootNode = TXDRootNode.Nodes.Add ("Children: " + ChildArray.count as string)
		ChildRootNode.tag = dotNetMXSValue ChildArray
		
		for child in ChildArray do
		(
			local childNode = ChildRootNode.Nodes.Add child.name
			childNode.tag = dotNetMXSValue child
			
			local count = 0
			
			-- for each texture in the child.textureArray that is also in the parent TXD, add a node
			for texture in child.textureArray do
			(
				if (isSharedTexture texture) then
				(
					count += 1
					local textureNode = childNode.Nodes.Add texture.name
					textureNode.tag = dotNetMXSValue texture
				)
			)
			
			childNode.text += " : " + count as string
		)
		
		-- create the textures node and for each texture in the struct variable textureArray, create a node and add them to the texturesRootNode node
		local texturesRootNode = TXDRootNode.Nodes.Add ("Textures: " + textureArray.count as string)
		texturesRootNode.tag = dotNetMXSValue textureArray
		
		for texture in textureArray do
		(
			local textureNode = texturesRootNode.Nodes.Add texture.name
			textureNode.tag = dotNetMXSValue texture
		)
		
		local sharedMemoryRootNode = TXDRootNode.Nodes.Add ("Shared Texture Memory: " + memory as string + "K")
		sharedMemoryRootNode.tag = dotNetMXSValue memory
		
		if textureArray.count > 0 then
		(
			mapRoot.expand()
			TXDRootNode.expand()
		)
	)
)

---------------------------------------------------------------------------------------------------------------------------------------
-- RSL_ChildTXD
-- Child TXD class used in parentise mode.
---------------------------------------------------------------------------------------------------------------------------------------
struct RSL_ChildTXD
(
	name = "", -- the name of the TXD
	mapfile = "****", -- the map file the TXD is from
	parent = "None", -- the RSL_ParentTXD the child is linked to if any
-- 	hasLOD = false, -- true if any of the objects assigned to the TXD are LOD objects
	TextureArray = #(), -- array of textures assigned to the RSL_ChildTXD
	memory = 0.0, -- the estimated memory for all the textures in the struct variable TextureArray
	TXDLinks = #(), -- array of RSL_TXDLinks between other TXDs in the level
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- findRoot() searches for the node in the parsed treeView based on the parsed name
	---------------------------------------------------------------------------------------------------------------------------------------
	fn findRoot treeView name =
	(
		local result = -1
		
		for i = 0 to treeView.Nodes.count - 1 do
		(
			if treeView.Nodes.Item[i].text == name then
			(
				result = i
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getMemory() Sets the struct variable memory based on the memory for each texture in the struct variable textureArray.
	-- Returns the memory.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getMemory =
	(
		memory = 0.0
		
		for entry in textureArray do
		(
			memory += entry.memory
		)
		
		memory
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- toString() Returns a stringstream based on the struct variables. Used when saving parent TXDs
	---------------------------------------------------------------------------------------------------------------------------------------
	fn toString =
	(
		local result = "" as stringStream
		local parentName = parent
		
		if (classOf parent) == RSL_ParentTXD then
		(
			parentName = parent.name
		)
		
		format "txd\nname = %\nparent = %\ntype = 4\n" name parentName to:result
		
		result as string
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- toTreeView() Creates treeview nodes from the struct variables and assigns them to the parsed treeview. The parsed this 
	-- is the RSL_ChildTXD that called the function and gets tagged to the TXDRootNode.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn toTreeView treeView this =
	(
		-- check if the rootNode alread exists based on the mapfile.
		local index = findRoot treeView mapfile
		local mapRoot
		
		if index > -1 then
		(
			mapRoot = treeView.Nodes.Item[index]
		)
		else
		(
			mapRoot = treeView.Nodes.Add mapfile
			mapRoot.tag = dotNetMXSValue mapfile
		)
		
		local TXDRootNode = mapRoot.Nodes.Add name
		TXDRootNode.tag = dotNetMXSValue this
		
		-- get the name of the parent. If a parent has not been assigned, this will be "None"
		local parentName = parent
		
		if (classOf parent) == RSL_ParentTXD then
		(
			parentName = parent.name
		)
		
		-- create a node for the parent
		local parentRootNode = TXDRootNode.Nodes.Add ("Parent TXD: " + parentName)
		parentRootNode.tag = dotNetMXSValue parent
		
		-- create the textures node and for each texture in the struct variable textureArray, create a node and add them to the texturesRootNode node
		local texturesRootNode = TXDRootNode.Nodes.Add ("Textures: " + textureArray.count as string)
		texturesRootNode.tag = dotNetMXSValue textureArray
		
		for texture in textureArray do
		(
			local textureNode = texturesRootNode.Nodes.Add texture.name
			textureNode.tag = dotNetMXSValue texture
		)
		
		local sharedMemoryRootNode = TXDRootNode.Nodes.Add ("Shared Texture Memory: " + memory as string + "K")
		sharedMemoryRootNode.tag = dotNetMXSValue memory
		
		-- create the TXD links node and for each texture in the struct variable TXDLinks, create a node and add them to the TXDLinksRootNode node
		local TXDLinksRootNode = TXDRootNode.Nodes.Add ("TXD Links: " + TXDLinks.count as string)
		TXDLinksRootNode.tag = dotNetMXSValue TXDLinks
		
		for entry in TXDLinks do
		(
			local TXDName
			
			for TXD in entry.TXDArray where TXD.name != this.name do
			(
				TXDname = TXD.name + " : "  + entry.textureArray.count as string + " : " + entry.memory as string + "k"
-- 				TXDname = TXD.name + " : " + entry.memory as string + "k"
			)
			
			local TXDLinkNode = TXDLinksRootNode.Nodes.Add TXDName
			TXDLinkNode.tag = dotNetMXSValue entry
			
			for texture in entry.textureArray do
			(
				local textureNode = TXDLinkNode.Nodes.Add texture.name
				textureNode.tag = dotNetMXSValue texture
			)
		)
		
		if textureArray.count > 0 then
		(
			TXDRootNode.expand()
		)
		
		mapRoot.expand()
	)
)

---------------------------------------------------------------------------------------------------------------------------------------
-- RSL_TextureObject
-- Texture Object class.
---------------------------------------------------------------------------------------------------------------------------------------
struct RSL_TextureObject
(
	TXD, -- the name of the TXD the object is assigned to
	name, -- the name the object 
	object, -- the scene node
	textures, -- array of RSL_TXDTextures assigned to the object
	memory -- the estimated memory for all the textures in the struct variable textures
)

---------------------------------------------------------------------------------------------------------------------------------------
-- RSL_TXDTexture
-- TXD Texture class.
---------------------------------------------------------------------------------------------------------------------------------------
struct RSL_TXDTexture
(
	name = "", -- the full export name of the texture
	size = [0,0], -- the export size of the texture
	type, -- the type of the texture. NOT USED
	alpha = false, -- weather the texture has alpha or not
	composite = false, -- weather the texture is a composite or not
	objectArray = #(), -- the objects the texture is assigned to
	isGlobal = false, -- weather the texture is in a global parent TXD
	tagDuplicate = false, -- weather the texture is a texture tag duplicate
	tagScale = 1.0, -- the texture tag scale
	TXD, -- the TXD the texture is in
	realTXD, -- the TXD the texture gets exported to. This could be a parent TXD
	isLOD = false, --weather the texture is assigned to a LOD object or not
	textures = #(), -- the single textures that make the final texture e.g. diffuse and alpha or three composite textures
	memory = 0.0, -- the estimated memory for the texture
	
	fn toString =
	(
		format "%\n" name
		format "----------------------------\n"
		format "\tSize:% x %\n" size.x size.y
		format "\tType:%\n" type
		format "\tAlpha:%\n" alpha
		format "\tComposite:%" composite
		format "\tObjects:\n"
		
		for object in objectArray do 
		(
			format "\t\t% - %\n" object.name (classOf object)
		)
		
		format "\tisGlobal:%\n" isGlobal
		format "\tTag Duplicate:%\n" tagDuplicate
		format "\tTag Scale:%\n" tagScale
		format "\tTXD:%\n" TXD
		format "\tExport TXD:%\n" realTXD
		format "\tisLOD:%\n" isLOD
		format "Source Textures:\n"
		
		for texture in textures do 
		(
			format "\t\t%\n" texture
		)
		
		format "\Memory:%\n\n" memory
	)
)

---------------------------------------------------------------------------------------------------------------------------------------
-- RSL_CSVTextureData
-- CSV Texture Data class.
---------------------------------------------------------------------------------------------------------------------------------------
struct RSL_CSVTextureData
(
	name,
	size,
	mapfileArray = #()
)