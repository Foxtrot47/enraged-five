


struct RSL_MMPainterPreset
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- Struct variables
	---------------------------------------------------------------------------------------------------------------------------------------
	
	presetMM = false,
	presetAO = false,
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- UI variables
	---------------------------------------------------------------------------------------------------------------------------------------
	presetOptionsForm,
	presetMMCheckBox,presetAOCheckBox,
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- assignMicroMovement()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn assignMicroMovement =
	(
		local selectedArray = for v in RSL_Painter.theObject.verts collect v
		local channelData = RSL_VChannelInfo channel:RSL_Painter.mmChannel r:false g:false
		RSL_Painter.currentChannel = channelData
		local strength = 1.0
		
		local fillCurrentMode = RSL_Painter.Fill.currentMode
		RSL_Painter.Fill.currentMode = #gradient
		RSL_Painter.Fill.end *= 2
		
		RSL_Painter.Fill.fillChannel RSL_Painter.theObject selectedArray channelData strength
		
		channelData.r = true
		channelData.b = false
		RSL_Painter.currentChannel = channelData
		RSL_Painter.Fill.currentMode = #paintBucket
		
		RSL_Painter.Fill.fillChannel RSL_Painter.theObject selectedArray channelData strength
		
		RSL_Painter.Fill.currentMode = #radial
		RSL_Painter.fill.center = RSL_Painter.theObject.center - RSL_Painter.theObject.pos
		RSL_Painter.fill.center.z = - RSL_Painter.theObject.min.z - RSL_Painter.theObject.pos.z
		RSL_Painter.fill.outerRadius = (distance (RSL_Painter.fill.center + RSL_Painter.theObject.pos) RSL_Painter.theObject.max) * 2
		
		RSL_Painter.Fill.fillChannel RSL_Painter.theObject selectedArray channelData -strength
		RSL_Painter.Fill.fillChannel RSL_Painter.theObject selectedArray channelData -strength
		
		channelData.r = false
		channelData.g = true
		RSL_Painter.currentChannel = channelData
		
		RSL_Painter.Fill.fillChannel RSL_Painter.theObject selectedArray channelData strength
		
		RSL_Painter.Fill.currentMode = fillCurrentMode
		RSL_Painter.currentChannel = undefined
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- assignAO()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn assignAO =
	(
		local channelData = RSL_VChannelInfo()
		RSL_Painter.currentChannel = channelData
		local AOcurrentMode = RSL_Painter.AO.currentMode
		RSL_Painter.AO.currentMode = #prop
		
		RSL_Painter.AO.brightness = 85.0
		RSL_Painter.AO.contrast = 80.0
		
		RSL_Painter.AO.AOChannel RSL_Painter.theObject channelData
		
		RSL_Painter.AO.currentMode = AOcurrentMode
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- UI FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- disableAccelerators() Called when the user enters any control that needs keyboard input e.g. a textBox
	---------------------------------------------------------------------------------------------------------------------------------------
	fn disableAccelerators s e =
	(
		enableAccelerators = false
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOnFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOnFormClosing s e =
	(
		RSL_Painter.preset.OnFormClosing s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- OnFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn OnFormClosing s e =
	(
		RSL_Painter.initOptionsButton.checked = false
		
		setINISetting RSL_Painter.INIfile "Preset" "Location" ([presetOptionsForm.location.x, presetOptionsForm.location.y] as string)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchPresetMMCheck()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchPresetCheck s e =
	(
		RSL_Painter.preset.presetCheck s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- presetMMCheck()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn presetCheck s e =
	(
		case s.name of
		(
			"presetMMCheckBox":(presetMM = s.checked)
			"presetAOCheckBox":(presetAO = s.checked)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- optionsDialog()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn optionsDialog =
	(
		--In this case, we're going to use a standard form rather than the custom maxForm written for max. this is because I want to change the 
		-- form back colour which you can't do if you use the custom maxForm. For this to work, we need to set the parent of the form to be Max. 
		-- USING THIS TYPE OF FORM MEANS WE NEED TO DISSABLE ACCELERATORS WHEN WE FOCUS ON A CONTROL THAT TAKES
		-- KEY PRESSES SUCH AS SPINNERS AND TEXT BOXES
		
		--Get the max handle pointer.
		local maxHandlePointer=(Windows.GetMAXHWND())
		
		--Convert the HWND handle of Max to a dotNet system pointer
		local sysPointer = DotNetObject "System.IntPtr" maxHandlePointer
		
		--Create a dotNet wrapper containing the maxHWND
		local maxHwnd = DotNetObject "MaxCustomControls.Win32HandleWrapper" sysPointer
		
		presetOptionsForm = RS_dotNetUI.form "presetOptionsForm" text:" Presets" size:[160, 72] borderStyle:RS_dotNetPreset.FB_FixedToolWindow
		presetOptionsForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		dotnet.addEventHandler presetOptionsForm "FormClosing" dispatchOnFormClosing
		dotnet.addEventHandler presetOptionsForm "Enter" disableAccelerators
		
		presetMMCheckBox = RS_dotNetUI.checkBox "presetMMCheckBox" text:(toUpper " Default Micro-movement") dockStyle:RS_dotNetPreset.DS_Fill \
									textAlign:RS_dotNetPreset.CA_MiddleLeft margin:(RS_dotNetObject.paddingObject 3)
		presetMMCheckBox.margin = (RS_dotNetObject.paddingObject 0)
		presetMMCheckBox.font = RS_dotNetPreset.FontSmall--(dotNetObject "System.Drawing.Font" "Small Fonts" 8.0 RS_dotNetPreset.f_Regular)
		presetMMCheckBox.CheckAlign = (dotNetClass "System.Drawing.ContentAlignment").MiddleRight
		presetMMCheckBox.checked = presetMM
		dotnet.addEventHandler presetMMCheckBox "click" dispatchPresetCheck
		
		presetAOCheckBox = RS_dotNetUI.checkBox "presetAOCheckBox" text:(toUpper " Default AO") dockStyle:RS_dotNetPreset.DS_Fill \
									textAlign:RS_dotNetPreset.CA_MiddleLeft margin:(RS_dotNetObject.paddingObject 3)
		presetAOCheckBox.margin = (RS_dotNetObject.paddingObject 0)
		presetAOCheckBox.font = RS_dotNetPreset.FontSmall--(dotNetObject "System.Drawing.Font" "Small Fonts" 8.0 RS_dotNetPreset.f_Regular)
		presetAOCheckBox.CheckAlign = (dotNetClass "System.Drawing.ContentAlignment").MiddleRight
		presetAOCheckBox.checked = presetAO
		dotnet.addEventHandler presetAOCheckBox "click" dispatchPresetCheck
		
		presetTableLayout = RS_dotNetUI.tableLayout "presetTableLayout" text:"presetTableLayout" collumns:#((dataPair type:"Percent" value:50)) \
									rows:#((dataPair type:"Absolute" value:24),(dataPair type:"Absolute" value:24)) dockStyle:RS_dotNetPreset.DS_Fill
		
		presetTableLayout.controls.add presetMMCheckBox 0 0
		presetTableLayout.controls.add presetAOCheckBox 0 1
		
		presetOptionsForm.controls.add presetTableLayout
		
		local INIlocation = getINISetting RSL_Painter.INIfile "Preset" "Location"
		
		if INIlocation.count > 0 then
		(
			local loc = readValue (INIlocation as stringStream)
			presetOptionsForm.location.x = loc.x
			presetOptionsForm.location.y = loc.y
		)
		else
		(
			presetOptionsForm.location.x = RSL_Painter.painterForm.location.x
			presetOptionsForm.location.y = RSL_Painter.painterForm.location.x
		)
		
		presetOptionsForm.show maxHwnd
	)
)