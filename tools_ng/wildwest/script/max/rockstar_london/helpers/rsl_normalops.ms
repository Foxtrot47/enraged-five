

global RSL_NormalOps

struct sharedVertex
(
	objectArray,
	vertexArray
)

struct sharedFace
(
	objectArray,
	faceArray,
	cornerArray
)

struct explicitObjectNormals
(
	object,
	normalArray = #()
)

struct explicitNormal
(
	face,
	corner,
	normal
)

struct RSL_NormalOperations
(
	smoothingGroupArray = #(),
	targetObjectArray = #(),
	referenceObject,
	
	normalForm,blendModeButton,BakeModeButton,pickReference,BakeNormalsButton,progress1,
	
	fn filterReferenceObject object = ((superClassOf object) == geometryClass and (classOf object) != XRefObject and (getAttrClass object) == "Gta Object"),
	
	fn checkSelection =
	(
		local result = selection.count > 0
		
		if result then
		(
			theObjects = selection as array
			
			for object in theObjects do
			(
				local attrClass = getAttrClass object
				
				if attrClass != "Gta Object" or object == referenceObject then
				(
					result = false
				)
			)
		)
		
		result
	),
	
	fn checkSmoothingGroups =
	(
		local result = false
		
		for entry in smoothingGroupArray do
		(
			if entry.checked then
			(
				result = true
			)
		)
		
		result
	),
	
	fn cb_NormalBakerSelectionChanged =
	(
		local modeCheck = (isValidNode referenceObject) == true or blendModeButton.checked == true
		
		if selection.count > 1 and (checkSelection()) and (checkSmoothingGroups()) and modeCheck then
		(
			targetObjectArray = selection as array
			BakeNormalsButton.enabled = true
		)
		else
		(
			targetObjectArray = #()
			BakeNormalsButton.enabled = false
		)
	),
	
	fn indexInSharedArray object vertIndex sharedArray =
	(
		local result = 0
		
		for i = 1 to sharedArray.count do
		(
			local objectIndex = (findItem sharedArray[i].objectArray object)
			if objectIndex != 0 and sharedArray[i].vertexArray[objectIndex] == vertIndex then
			(
				result = i
			)
		)
		
		result
	),
	
	fn isUniqueInShared shared object vertIndex =
	(
		local result = true
		
		local objectIndex = findItem shared.objectArray object
		
		if objectIndex != 0 then
		(
			if shared.vertexArray[objectIndex] == vertIndex then
			(
				result = false
			)
		)
		
		result
	),
	
	fn getSmoothingArray object face =
	(
		local result = #{}
		local smoothingInteger = polyOp.getFaceSmoothGroup object face
		
		if smoothingInteger < 0 then
		(
			result[32] = true
			smoothingInteger -= 2^31
		)
		
		for i = 1 to 31 do
		(
			result[i] = (mod smoothingInteger 2 > 0.5)
			smoothingInteger /= 2
		)
		
		result as array
	),
	
	fn testForShared array1 array2 =
	(
		local result = false
		
		for item in array1 do
		(
			local isShared = (findItem array2 item) != 0
			
			if isShared then
			(
				result = true
			)
		)
		
		result
	),
	
	fn validSmoothing array =
	(
		local result = false
		
		for SG in array do
		(
			if smoothingGroupArray[SG].checked then
			(
				result = true
			)
		)
		
		result
	),
	
	fn collectVertsBySG object =
	(
		local result = #()
		
		for face in object.faces do
		(
			local SGArray = getSmoothingArray object face.index
			
			if (validSmoothing SGArray) then
			(
				result += (polyOp.getFaceVerts object face.index)
			)
		)
		
		makeUniqueArray result
	),
	
	fn createSharedVertexArray =
	(
		local result = #()
		
		for i = 1 to targetObjectArray.count - 1 do
		(
			local updateValue = (i as float / targetObjectArray.count * 100.0)
			progressUpdate updateValue
			progress1.Value = updateValue
			
			local this = targetObjectArray[i]
			local thisVerts = collectVertsBySG this
			
			for s = i + 1 to targetObjectArray.count do
			(
				local other = targetObjectArray[s]
				local otherVerts = collectVertsBySG other
				
				for vert in thisVerts do
				(
					local sharedIndex = indexInSharedArray this vert result
					
					for otherVert in otherVerts do
					(
						if (distance this.verts[vert].pos other.verts[otherVert].pos) < 0.001 then
						(
							if sharedIndex == 0 then
							(
								append result (sharedVertex objectArray:#(this, other) vertexArray:#(vert, otherVert))
							)
							else
							(
								if (isUniqueInShared result[sharedIndex] other otherVert) then
								(
									append result[sharedIndex].objectArray other
									append result[sharedIndex].vertexArray otherVert
								)
							)
						)
					)
				)
			)
		)
		
		result
	),
	
	fn indexInExplicitArray object explicitArray =
	(
		local result = 0
		
		for i = 1 to explicitArray.count do
		(
			if explicitArray[i].object == object then
			(
				result = i
			)
		)
		
		result
	),
	
	fn indexInSharedFaceArray object faceIndex cornerIndex sharedArray =
	(
		local result = 0
		
		for i = 1 to sharedArray.count do
		(
			local objectIndex = (findItem sharedArray[i].objectArray object)
			if objectIndex != 0 and sharedArray[i].faceArray[objectIndex] == faceIndex and sharedArray[i].cornerArray[objectIndex] == cornerIndex then
			(
				result = i
			)
		)
		
		result
	),

	fn isUniqueInSharedFace shared object faceIndex cornerIndex =
	(
		local result = true
		
		local objectIndex = findItem shared.objectArray object
		
		if objectIndex != 0 then
		(
			if shared.faceArray[objectIndex] == faceIndex and shared.cornerArray[objectIndex] == cornerIndex then
			(
				result = false
			)
		)
		
		result
	),
	
	fn indexInFace object face vert =
	(
		local result = 0
		local faceVertsArray = polyOp.getFaceVerts object face
		
		result = findItem faceVertsArray vert
	),
	
	fn createExplicitArray sharedVertexArray =
	(
		local explicitArray = for object in targetObjectArray collect (explicitObjectNormals object:object) --#()
		
		for i = 1 to sharedVertexArray.count do
		(
			local entry = sharedVertexArray[i]
			
			local updateValue = (i as float / sharedVertexArray.count * 100.0)
			progressUpdate updateValue
			progress1.Value = updateValue
			
			local sharedFaceArray = #()
			local normalArray = #()
			
			for i = 1 to entry.objectArray.count - 1 do
			(
				local this = entry.objectArray[i]
				local thisVertex = entry.vertexArray[i]
				local thisFaceArray = (polyOp.getFacesUsingVert this thisVertex) as array
				
				for s = i + 1 to entry.objectArray.count do
				(
					local other = entry.objectArray[s] 
					local otherVertex = entry.vertexArray[s]
					local otherFaceArray = (polyOp.getFacesUsingVert other otherVertex) as array
					
					for thisFace in thisFaceArray do
					(
						local thisNormal = polyOp.getFaceNormal this thisFace
						local thisfaceVerts = polyop.getFaceVerts this thisFace
						local thisVertFaceIndex = findItem thisfaceVerts thisVertex
						
						local sharedIndex = indexInSharedFaceArray this thisFace thisVertFaceIndex sharedFaceArray 
						
						for otherFace in otherFaceArray do
						(
							local otherNormal = polyOp.getFaceNormal other otherFace
							local otherFaceVerts = polyop.getFaceVerts other otherFace
							local otherVertFaceIndex = findItem otherFaceVerts otherVertex
							
							if (dot thisNormal otherNormal) > 0.1 then
							(
								local thisSmoothingArray = getSmoothingArray this thisFace
								local otherSmoothingArray = getSmoothingArray other otherFace
								
								local sharedSmoothing = testForShared thisSmoothingArray otherSmoothingArray
								
								if sharedSmoothing and (validSmoothing thisSmoothingArray) and (validSmoothing otherSmoothingArray) then
								(
									if sharedIndex == 0 then
									(
										append sharedFaceArray (sharedFace objectArray:#(this, other) faceArray:#(thisFace, otherFace) cornerArray:#(thisVertFaceIndex, otherVertFaceIndex))
										append normalArray #(thisNormal, otherNormal)
									)
									else
									(
										if (isUniqueInSharedFace sharedFaceArray[sharedIndex] other otherFace otherVertFaceIndex) then
										(
											append sharedFaceArray[sharedIndex].objectArray other
											append sharedFaceArray[sharedIndex].faceArray otherFace
											append sharedFaceArray[sharedIndex].cornerArray otherVertFaceIndex
											append normalArray[sharedIndex] otherNormal
										)
									)
								)
							)
						)
					)
				)
			)
			
			for i = 1 to sharedFaceArray.count do
			(
				local entry = sharedFaceArray[i]
				local sharedNormals = normalArray[i]
				local newNormal = [0,0,0]
				
				for n in sharedNormals do
				(
					newNormal += n
				)
				
				newNormal = normalize (newNormal / sharedNormals.count)
				
				for i = 1 to entry.objectArray.count do
				(
					local object = entry.objectArray[i]
					local face = entry.faceArray[i]
					local corner = entry.cornerArray[i]
					
					local newExplicitNormal = (explicitNormal face:face corner:corner normal:newNormal)
					
					local index = indexInExplicitArray object explicitArray
					append explicitArray[index].normalArray newExplicitNormal
				)
			)
		)
		
		explicitArray
	),
	
	fn BakeNormals =
	(
		progressStart "Baking Normals"
		convertTo targetObjectArray editable_poly
		
		local sharedVertexArray = createSharedVertexArray()
		local explicitArray = createExplicitArray sharedVertexArray
		local testMesh = copy (convertToMesh referenceObject)
		
		for entry in explicitArray do
		(
			select entry.object
			local editNormalsMod = editNormals ()
			addModifier entry.object editNormalsMod
			
			editNormalsMod.RebuildNormals
			
			for i = 1 to entry.normalArray.count do
			(
				local explicitNormal = entry.normalArray[i]
				
				local updateValue = (i as float / entry.normalArray.count * 100.0)
				progressUpdate updateValue
				progress1.Value = updateValue
				
				local vertexIndex = (polyop.getFaceVerts entry.object explicitNormal.face)[explicitNormal.corner]
				local vertPos = entry.object.verts[vertexIndex].pos
				local testRay = ray (vertPos + (explicitNormal.normal * 10)) -(explicitNormal.normal)
				
				local hitTest = intersectRay testMesh testRay
				
				if hitTest != undefined then
				(
					local normalIndex = editNormalsMod.GetNormalID explicitNormal.face explicitNormal.corner
					
					editNormalsMod.SetNormalExplicit normalIndex explicit:true
					editNormalsMod.SetNormal normalIndex hitTest.dir
				)
			)
			
			convertTo entry.object editable_poly
		)
		progressEnd()
		
		delete testMesh
		
		select targetObjectArray
	),
	
	fn blendNormals =
	(
		progressStart "Blending Normals"
		convertTo targetObjectArray editable_poly
		
		local sharedVertexArray = createSharedVertexArray()
		local explicitArray = createExplicitArray sharedVertexArray
		
		for entry in explicitArray do
		(
			select entry.object
			local editNormalsMod = editNormals ()
			addModifier entry.object editNormalsMod
			
			editNormalsMod.RebuildNormals
			
			for explicitNormal in entry.normalArray do
			(
				local normalIndex = editNormalsMod.GetNormalID explicitNormal.face explicitNormal.corner
				
				editNormalsMod.SetNormalExplicit normalIndex explicit:true
				editNormalsMod.SetNormal normalIndex explicitNormal.normal
			)
			
			convertTo entry.object editable_poly
		)
		progressEnd()
		select targetObjectArray
	),
	
	
	
	
	
	
	
	fn dispatchModeClick s e =
	(
		RSL_NormalOps.modeClick s e
	),
	
	fn modeClick s e =
	(
		s.checked = true
		
		case s.text of
		(
			"BLEND":
			(
				pickReference.enabled = false
				BakeModeButton.checked = false
				pickReference.text = toUpper ""
				referenceObject = undefined
				
				BakeNormalsButton.text = toUpper "Blend Normals"
			)
			"BAKE":
			(
				blendModeButton.checked = false
				pickReference.enabled = true
				pickReference.text = toUpper "Pick Reference Object..."
				
				BakeNormalsButton.text = toUpper "Bake Normals"
			)
		)
	),
	
	fn dispatchPickReferenceClick s e =
	(
		RSL_NormalOps.pickReferenceClick s e
	),
	
	fn pickReferenceClick s e =
	(
		normalForm.enabled = false
		
		referenceObject = pickObject filter:filterReferenceObject
		
		normalForm.enabled = true
		
		if referenceObject != undefined then
		(
			pickReference.text = toUpper referenceObject.name
		)
		else
		(
			pickReference.text = toUpper "Pick Reference Object..."
		)
	),
	
	fn dispatchSGButtonClick s e =
	(
		RSL_NormalOps.SGButtonClick s e
	),
	
	fn SGButtonClick s e =
	(
		cb_NormalBakerSelectionChanged()
	),
	
	fn dispatchBakeNormalsButtonClick s e =
	(
		RSL_NormalOps.BakeNormalsButtonClick s e
	),
	
	fn BakeNormalsButtonClick s e =
	(
		if BakeModeButton.checked then
		(
			BakeNormals()
		)
		else
		(
			blendNormals()
		)
	),
	
	fn dispatchOnFormClosing s e =
	(
		RSL_NormalOps.onFormClosing s e
	),
	
	fn onFormClosing s e =
	(
		callbacks.removeScripts id:#RSL_NormalOps_SelectCallBacks
		
		gc()
	),
	
	fn createNormalForm = 
	(
		clearListener()
		
		normalForm = RS_dotNetUI.maxForm "normalForm" text:"R* Normal Ops" size:[226,236] borderStyle:RS_dotNetPreset.FB_FixedToolWindow
		normalForm.SizeGripStyle = (dotNetClass "SizeGripStyle").Show
		normalForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		
-- 		dotNet.addEventHandler normalForm "Resize" dispatchNormalFormResize
		dotnet.addEventHandler normalForm "FormClosing" dispatchOnFormClosing
		
		mainTable = RS_dotNetUI.tableLayout "mainTable" text:"mainTable" dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNet.CBS_Single
		mainTable.BackColor = RS_dotNetPreset.controlColour_Medium
		mainTable.RowCount = 6
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 20)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 20)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 20)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 106)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 20)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 10)
		mainTable.ColumnCount = 2
		mainTable.ColumnStyles.add (RS_dotNetObject.ColumnStyleObject "percent" 50)
		mainTable.ColumnStyles.add (RS_dotNetObject.ColumnStyleObject "percent" 50)
-- 		mainTable.ColumnStyles.add (RS_dotNetObject.ColumnStyleObject "percent" 50)
		
		blendModeButton = RS_dotNetUI.checkBox "blendModeButton" text:(toUpper "Blend") appearance:RS_dotNetPreset.AC_Button flatStyle:RS_dotNetPreset.FS_Flat
		blendModeButton.BackColor = RS_dotNetPreset.controlColour_Light
		
		dotNet.addEventHandler blendModeButton "MouseClick" dispatchModeClick
		
		BakeModeButton = RS_dotNetUI.checkBox "BakeModeButton" text:(toUpper "Bake") appearance:RS_dotNetPreset.AC_Button flatStyle:RS_dotNetPreset.FS_Flat
		BakeModeButton.BackColor = RS_dotNetPreset.controlColour_Light
		BakeModeButton.checked = true
		
		dotNet.addEventHandler BakeModeButton "MouseClick" dispatchModeClick
		
		pickReference = RS_dotNetUI.Button "pickReference" text:(toUpper "Pick Reference Object...") size:[210,20]
		pickReference.TextAlign = RS_dotNetPreset.CA_MiddleCenter
		pickReference.BackColor = RS_dotNetPreset.controlColour_Medium
		
		dotNet.addEventHandler pickReference "MouseClick" dispatchPickReferenceClick
		
		smoothingLabel = RS_dotNetUI.label "smoothingLabel" text:(toUpper "Smoothing Groups") borderStyle:RS_dotNetPreset.BS_None
		smoothingLabel.TextAlign = RS_dotNetPreset.CA_MiddleLeft
		
		smoothGroupLayout = dotNetObject "FlowLayoutPanel"
		smoothGroupLayout.BorderStyle = RS_dotNetPreset.BS_FixedSingle
		smoothGroupLayout.dock = RS_dotNetPreset.DS_Fill
		smoothGroupLayout.Margin = (RS_dotNetObject.paddingObject 0)
		smoothGroupLayout.BackColor = RS_dotNetPreset.controlColour_Dark
		smoothGroupLayout.autoSize = true
		
		smoothingGroupArray.count = 32
		
		for i = 1 to 32 do
		(
			smoothGroupButton = RS_dotNetUI.checkBox ("SG_" + i as string) text:(i as string) size:[24,24] appearance:RS_dotNetPreset.AC_Button flatStyle:RS_dotNetPreset.FS_Flat
			smoothGroupButton.BackColor = RS_dotNetPreset.controlColour_Medium
			
			dotNet.addEventHandler smoothGroupButton "MouseClick" dispatchSGButtonClick
			
			smoothingGroupArray[i] = smoothGroupButton
			
			smoothGroupLayout.controls.add smoothGroupButton
		)
		
		BakeNormalsButton = RS_dotNetUI.Button "BakeNormalsButton" text:(toUpper "Bake Normals") size:[210,20]
		BakeNormalsButton.TextAlign = RS_dotNetPreset.CA_MiddleCenter
		BakeNormalsButton.BackColor = RS_dotNetPreset.controlColour_Medium
		BakeNormalsButton.enabled = false
		
		dotNet.addEventHandler BakeNormalsButton "MouseClick" dispatchBakeNormalsButtonClick
		
		progress1 = dotNetObject "progressBar"
		progress1.Dock = RS_dotNetPreset.DS_Fill --(dotNetClass "System.Windows.Forms.DockStyle").Fill
		progress1.Style = (dotNetClass "System.Windows.Forms.ProgressBarStyle").Continuous
		progress1.BackColor = (dotNetClass "System.Drawing.Color").Black --RS_dotNetPreset.controlColour_dark
		progress1.ForeColor = (DotNetClass "System.Drawing.Color").YellowGreen
		progress1.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		
		mainTable.controls.add blendModeButton 0 0
		mainTable.controls.add BakeModeButton 1 0
		mainTable.controls.add pickReference 0 1
		mainTable.controls.add smoothingLabel 0 2
		mainTable.controls.add smoothGroupLayout 0 3
		mainTable.controls.add BakeNormalsButton 0 4
		mainTable.controls.add progress1 0 5
		
		mainTable.SetColumnSpan pickReference 3
		mainTable.SetColumnSpan smoothingLabel 3
		mainTable.SetColumnSpan smoothGroupLayout 3
		mainTable.SetColumnSpan BakeNormalsButton 3
		mainTable.SetColumnSpan progress1 3
		
		normalForm.controls.add mainTable
		
		normalForm.showmodeless()
		
		callbacks.addScript #selectionSetChanged "RSL_NormalOps.cb_NormalBakerSelectionChanged()" id:#RSL_NormalOps_SelectCallBacks
		
		normalForm
	)
	
)

if RSL_NormalOps != undefined then
(
	RSL_NormalOps.normalForm.close()
)

RSL_NormalOps = RSL_NormalOperations()
RSL_NormalOps.createNormalForm()