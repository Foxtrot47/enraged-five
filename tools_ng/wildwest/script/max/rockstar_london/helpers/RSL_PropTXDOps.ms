-- perforce integration script
filein "pipeline/util/p4.ms"

if RsMapSetupGlobals == undefined then
(
	fileIn "pipeline/export/maps/globals.ms"
)
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/pipeline/RSL_SceneXml.ms")
filein "pipeline\\util\\drawablelod.ms"
-- dotNet class shortcuts script
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetClasses.ms")
-- dotNet UI functions script
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetUIOps.ms")

fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSL_PropTXDStructs.ms")

global RSL_PropTXD

struct RSL_PropTXDOps
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- Struct variables
	---------------------------------------------------------------------------------------------------------------------------------------
	idxTXD = getattrindex "Gta Object" "TXD", -- TXD attribute index
	idxFragment = getattrindex "Gta Object" "Is Fragment",
	idxFragProxy = getattrindex "Gta Object" "Is FragProxy",
	idxDontExport = getAttrIndex "Gta Object" "Dont Export", -- dont export attribute index
	idxDontAdd = getAttrIndex "Gta Object" "Dont Add To IPL", -- dont add to IPL attribute index
	
	TXDIcon = RsConfigGetWildWestDir() + "script/max/Rockstar_London/images/texturebrowseicontxd.png",
	
	IconArray = getFiles (RsConfigGetWildWestDir() + "script/max/Rockstar_London/images/gm*.png"),
	
	mapFileDataArray = #(),
	TXDArray = #(),
	UsedPropDataArray = #(),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- UI variables
	---------------------------------------------------------------------------------------------------------------------------------------
	propTXDForm,
	selectDisplayItem,
	mainTree,
	preFixTextBox,
	packButton,
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- disableAccelerators() Called when the user enters any control that needs keyboard input e.g. a textBox
	---------------------------------------------------------------------------------------------------------------------------------------
	fn disableAccelerators s e =
	(
		enableAccelerators = false
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getRpfs() Collects all the map file and interior rpfs for the level
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getRpfs =
	(
		local mapPath = (project.independent + "/levels/" + rsMapLevel)
		
		local result = RS_dotNetClass.directoryClass.GetFiles mapPath "*.rpf" (DotNetClass "System.IO.SearchOption").TopDirectoryOnly
		join result (RS_dotNetClass.directoryClass.GetFiles (mapPath + "/interiors") "*.rpf" (DotNetClass "System.IO.SearchOption").TopDirectoryOnly)		
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- extractXmls() Extracts xml files from the parsed rpfArray
	---------------------------------------------------------------------------------------------------------------------------------------
	fn extractXmls rpfArray =
	(
		local rageEXE = "ragebuilder_0327.exe"
		
		local outputDir = "c:\\txdOps_xml"
		makedir outputDir
		outputDir += "\\"
		for file in (getFiles (outputDir + "*.xml")) do deleteFile file
		
		for rpf in rpfArray do
		(
			local arguments = (" x:\\tools\\bin\\script\\Extracter.rbs -input " + rpf + " -output " + outputDir + " -filetype xml")
			local rageCommand = (rageEXE + arguments)
			
			HiddenDOSCommand rageCommand startpath:"x:\\tools\\bin\\"
		)
		
		(getFiles (outputDir + "*.xml"))
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getMapFileData() Gets the used prop data for each map file and interior in the level
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getMapFileData =
	(
		local rpfArray = getRpfs()
		local xmlArray = extractXmls rpfArray
		
		mapFileDataArray = #()
		RSL_SceneXmlOps.getMapFilePropData xmlArray &RSL_PropTXD.mapFileDataArray
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getGeometryChildrenRec() Recursive function that gets all the geometry children of the parsed object and appends them
	-- to the parsed result array variable
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn getGeometryChildrenRec object &result =
	(
		if (getAttrClass object) == "Gta Object" then
		(
			local newChild = dataPair node:object children:#()
			append result newChild
			
			getGeometryChildrenRec (for child in object.children collect child) &newChild.children
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getLODRec() Recursive function that gets the LOD drawables for the parsed object and assigns them as a datapair to
	-- the result.LOD
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getLODRec object &result =
	(
		local LODObject = RsLodDrawable_GetLowerDetailModel object
		
		if (isValidNode LODObject) then
		(
			result.LOD = dataPair node:LODObject LOD:undefined
			
			getLODRec LODObject &result.LOD
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- findTXDInArray() Searches the struct variable TXDArray for a TXD with the parsed name. Returns the index if found
	-- or zero
	---------------------------------------------------------------------------------------------------------------------------------------
	fn findTXDInArray name =
	(
		local result = 0
		
		for i = 1 to TXDArray.count while result == 0 do 
		(
			if (stricmp TXDArray[i].name name) == 0 then
			(
				result = i
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getLODParentRec() Recursive function that gets the root LOD parent of the parsed object. The parsed result variable
	-- should end up being the high detail object.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getLODParentRec object &result =
	(
		local parent = RsLodDrawable_GetHigherDetailModel object
		
		if (isValidNode parent) then
		(
			result = parent
			
			getLODParentRec parent &result
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- appendIfUsed() Checks the parsed mapfile data to see if the parsed prop is used. Appends the mapfile name to the
	-- parsed result array
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn appendIfUsed mapFile prop &result =
	(
		getLODParentRec prop &prop
		local name = substituteString prop.name "_frag_" ""
		
		if (findItem mapFile.propArray name) > 0 then
		(
			append result mapFile.name
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- findProp() Searches through the parsed array for a prop with the parsed name. returns the index if found or zero
	---------------------------------------------------------------------------------------------------------------------------------------
	fn findProp array name =
	(
		local result = 0
		
		for i = 1 to array.count while result == 0 do
		(
			if (stricmp array[i].name name) == 0 then
			(
				result = i
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getScenePropData() Gets the scene TXDs and the props assigned to them
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getSceneTXDData =
	(
		for object in rootNode.children where (getAttrClass object) == "Gta Object" and not (getAttr object idxDontExport) and (classOf object) != XRefObject do 
		(
			local node = object
			
			if (GetAttr object idxFragProxy) then
			(
				local frag = getNodeByName (object.name + "_frag_") exact:true
				
				if (isValidNode frag) then
				(
					node = frag
				)
			)
			
			local usedArray = #()
			appendIfUsed mapFileDataArray node &usedArray
			
			local newProp = RSL_PropData name:node.name node:node TXD:(getAttr node idxTXD) usedArray:usedArray
			getGeometryChildrenRec (for child in node.children collect child) &newProp.children
			getLODRec node newProp
			
			local index = findTXDInArray newProp.TXD
			
			if index == 0 then
			(
				append TXDArray (dataPair name:newProp.TXD propArray:#(newProp))
			)
			else
			(
				if (findProp TXDArray[index].propArray newProp.name) == 0 then
				(
					append TXDArray[index].propArray newProp
				)
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getUsedPropData() Gets the valid used props
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getUsedPropData =
	(
		UsedPropDataArray = #()
		
		for object in rootNode.children where (getAttrClass object) == "Gta Object" and not (getAttr object idxDontExport) and (RsLodDrawable_GetHigherDetailModel object) == undefined and (classOf object) != XRefObject do 
		(
			local node = object
			
			if (GetAttr object idxFragProxy) then
			(
				local frag = getNodeByName (object.name + "_frag_") exact:true
				
				if (isValidNode frag) then
				(
					node = frag
				)
			)
			
			local usedArray = #()
			appendIfUsed mapFileDataArray node &usedArray
			
			local newProp = RSL_PropData name:node.name node:node TXD:(getAttr node idxTXD) usedArray:usedArray
			getGeometryChildrenRec (for child in node.children collect child) &newProp.children
			getLODRec node newProp
			
			if usedArray.count > 0 then
			(
				if (findProp UsedPropDataArray newProp.name) == 0 then
				(
					append UsedPropDataArray newProp
				)
			)
			else
			(
				newProp.invalidate()
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- mapFilesMatch() Checks to see if the parsed this and other variables have the same map files. Returns true if they do
	---------------------------------------------------------------------------------------------------------------------------------------
	fn mapFilesMatch this other =
	(
		local result = true
		
		if this.propArray[1].usedArray.count == other.propArray[1].usedArray.count then
		(
			for mapFile in this.propArray[1].usedArray while result do 
			(
				if (findItem other.propArray[1].usedArray mapFile) == 0 then
				(
					result = false
				)
			)
		)
		else
		(
			result = false
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- sortPropsByMapFile() Sorts the props into TXDs based on the map file usage
	---------------------------------------------------------------------------------------------------------------------------------------
	fn sortPropsByMapFile =
	(
		TXDArray = for prop in UsedPropDataArray collect dataPair name:prop.name propArray:#(prop)
		
		local newCount = 1
		local finished = false
		
		progressStart "Sorting Props"
		
		while not finished do
		(
			finished = true
			
			for i = 1 to TXDArray.count do 
			(
				local thisTXD = TXDArray[i]
				
				if thisTXD != undefined then
				(
					for s = 1 to TXDArray.count while thisTXD != undefined do 
					(
						local otherTXD = TXDArray[s]
						
						if otherTXD != undefined and thisTXD.name != otherTXD.name then
						(
							if (mapFilesMatch thisTXD otherTXD) then
							(
								thisTXD.name = ("multiTXD_" + newCount as string)
								
								for prop in otherTXD.propArray do 
								(
									if (findProp thisTXD.propArray prop.name) == 0 then
									(
										append thisTXD.propArray prop
									)
									else
									(
										format "% already found in %\n" prop thisTXD
									)
								)
								
								TXDArray[s] = undefined
								
								newCount += 1
								finished = false
							)
						)
						
						progressUpdate (s as float / TXDArray.count * 100.0)
					)
				)
			)
		)
		
		progressEnd()
		
		TXDArray = for TXD in TXDArray where TXD != undefined collect TXD
	),
	
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- addTreviewNode() Adds the parsed TXD to the mainTree control
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn addTreviewNode TXD =
	(
		local TXDNode = mainTree.nodes.add TXD.name
		TXDNode.imageIndex = 8
		TXDNode.SelectedImageIndex = 8
		
		TXDNode.tag = dotNetMXSValue TXD
		
		for prop in TXD.propArray do 
		(
			prop.toTreeview TXDNode prop
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- setTXD() Sets the TXD name for all the props in the parsed TXDs
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn setTXD TXD =
	(
		for prop in TXD.propArray do 
		(
			prop.setTXD TXD.name
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- generateTXDNames() Generates a valid name for each TXD in the struct variable TXDArray.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn generateTXDNames =
	(
		mainTree.nodes.clear()
		
		local TXDCount = 0
		local uniCount = 0
		
		progressStart "Setting TXD Names..."
		
		for i = 1 to TXDArray.count do
		(
			local entry = TXDArray[i]
			
			if entry.propArray.count == 1 then
			(
				uniCount += 1
				-- create a TXD name 
				local TXDName = (preFixTextBox.text + "_uni" + uniCount as string) --preFixTextBox.text
				
				-- if the character count of the name is higher than 18, we must make a reduced character name
				if TXDName.count > 18 then
				(
					-- first we takle out the _ character from the preFixTextBox.text UI item
					local subName = substituteString preFixTextBox.text "_" --preFixTextBox.text
					local shortName = (subName + "_uni" + uniCount as string)
					
					-- if the character count of the shortName is still over 18 characters long, we must shorten it more
					if shortName.count > 18 then
					(
						-- Get the amount of characters we are allowed to have
						local sCount = subName.count - (shortName.count - 18)
						shortName = ""
						
						-- build the shortName string with only the characters that the count allows us
						for i = 1 to sCount do 
						(
							shortName += subName[i]
						)
						
						shortName +=  "uni" + uniCount as string
					)
					
					TXDName = shortName
				)
			)
			else
			(
				TXDCount += 1
				
				-- create a TXD name 
				local TXDName = (preFixTextBox.text + "_TXD" + TXDCount as string)
				
				-- if the character count of the name is higher than 18, we must make a reduced character name
				if TXDName.count > 18 then
				(
					-- first we takle out the _ character from the preFixTextBox.text UI item
					local subName = (substituteString preFixTextBox.text "_" "")
					local shortName = subName + "_TXD" + count as string
					
					-- if the character count of the shortName is still over 18 characters long, we must shorten it more
					if shortName.count > 18 then
					(
						-- Get the amount of characters we are allowed to have
						local sCount = subName.count - (shortName.count - 18)
						shortName = ""
						
						-- build the shortName string with only the characters that the count allows us
						for i = 1 to sCount do 
						(
							shortName += subName[i]
						)
						
						shortName += "_TXD" + (TXDCount as string)
					)
					
					TXDName = shortName
				)
			)
			
			entry.name = TXDName
			
			progressUpdate (i as float / TXDArray.count * 100.0)
		)
		
		progressEnd()
	),
	
	
	
	
	
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- UI FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOnFormClosing() Dispatches onFormClosing() Called when the user closes collision ops
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOnFormClosing s e =
	(
		RSL_PropTXD.onFormClosing s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- onFormClosing() Called when collision ops is closed
	---------------------------------------------------------------------------------------------------------------------------------------
	fn onFormClosing s e =
	(
		gc()
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOnFormResize() Dispatches formResize() Called when the user resizes the form
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOnFormResize s e =
	(
		RSL_PropTXD.formResize s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- formResize() Does nothing, only used for debug
	---------------------------------------------------------------------------------------------------------------------------------------
	fn formResize s e =
	(
-- 		format "w:% h:%\n" s.size.width s.size.height
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchSelectDisplayClick() Dispatches selectDisplayClick() Called when the user presses the selectDisplayItem button
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchSelectDisplayClick s e =
	(
		RSL_PropTXD.selectDisplayClick s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- selectDisplayClick() Selects the prop in the scene.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn selectDisplayClick s e =
	(
		if mainTree.selectedNode != undefined then
		(
			local data = mainTree.selectedNode.tag.value
			case (classOf data) of
			(
				RSL_PropData:
				(
					select data.node
					max zoomext sel
				)
				DataPair:
				(
					if mainTree.selectedNode.parent == undefined then
					(
						local propArray = for prop in data.propArray collect prop.node
						
						select propArray
						max zoomext sel
					)
				)
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchMainTreeMenuOpening() Dispatches mainTreeMenuOpening() Called when the user right clicks the treeview
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchMainTreeMenuOpening s e =
	(
		RSL_PropTXD.mainTreeMenuOpening s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- mainTreeMenuOpening() Checks to make sure the user has right clicked a valid treeview node.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn mainTreeMenuOpening s e =
	(
		local mousePoint = mainTree.PointToClient mainTree.Cursor.Position
		local hitNode = mainTree.getNodeAt mousePoint
		
		if hitNode != undefined then
		(
			if mainTree.selectedNode != hitNode then
			(
				mainTree.selectedNode = hitNode
			)
			
			if hitNode.tag != undefined then
			(
				case (classOf hitNode.tag.value) of
				(
					default:(e.Cancel = true)
					RSL_PropData:
					(
						selectDisplayItem.text = "Select Prop"
					)
					DataPair:
					(
						if hitNode.parent == undefined then
						(
							selectDisplayItem.text = "Select All Props"
						)
						else
						(
							e.Cancel = true
						)
					)
				)
			)
			else
			(
				e.Cancel = true
			)
		)
		else
		(
			e.Cancel = true
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchTreePreLabelEdit() Dispatches treePreLabelEdit() Called when the user starts to edit a node label
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchTreePreLabelEdit s e =
	(
		RSL_PropTXD.treePreLabelEdit s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- treePreLabelEdit() Checks to see if the node about to be edited is a valid node for label editing
	---------------------------------------------------------------------------------------------------------------------------------------
	fn treePreLabelEdit s e =
	(
		if mainTree.selectedNode != undefined then
		(
			if mainTree.selectedNode.parent != undefined then
			(
				e.CancelEdit = true
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchTreePostLabelEdit() Dispatches treePostLabelEdit() Called when the user finishes editing a node label
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchTreePostLabelEdit s e =
	(
		RSL_PropTXD.treePostLabelEdit s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- treePostLabelEdit() Checks to see if the new label is valid and then sets the new TXD name
	---------------------------------------------------------------------------------------------------------------------------------------
	fn treePostLabelEdit s e =
	(
		if e.label != undefined and e.label != "CHANGEME" then
		(
			if (findTXDInArray e.label) == 0 then
			(
				local TXD = mainTree.selectedNode.tag.value
				TXD.name = e.label
				
				for prop in TXD.propArray do 
				(
					prop.setTXD e.label
				)
			)
			else
			(
				messageBox (e.label + " is already in the scene. Try another name.") title:"Error..."
				e.CancelEdit = true
			)
		)
		else
		(
			e.CancelEdit = true
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchPackClick() Dispatches packClick() Called when the user presses the packButton
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchPackClick s e =
	(
		RSL_PropTXD.packClick s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- packClick() Runs the pack function to pack props by map file usage
	---------------------------------------------------------------------------------------------------------------------------------------
	fn packClick s e =
	(
		getUsedPropData()
		sortPropsByMapFile()
		generateTXDNames()
		
		setTXD TXDArray
		
		mainTree.nodes.clear()
		addTreviewNode TXDArray
	),
	
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- createForm() Creates teh main form when the script is run
	---------------------------------------------------------------------------------------------------------------------------------------
	fn createForm =
	(
		-- setup the globals so that the global variable rsMapLevel is valid if the map file is a valid map file
		RsMapSetupGlobals()
		
		if rsMapLevel != undefined then
		(
			if RsMapType == #Prop then
			(
				gc()
				clearListener()
				
				delete (for object in rootNode.children where (classOf object) == TXDCloudManip collect object)
				
				--Get the max handle pointer.
				local maxHandlePointer=(Windows.GetMAXHWND())

				--Convert the HWND handle of Max to a dotNet system pointer
				local sysPointer = DotNetObject "System.IntPtr" maxHandlePointer

				--Create a dotNet wrapper containing the maxHWND
				local maxHwnd = DotNetObject "MaxCustomControls.Win32HandleWrapper" sysPointer
				
				propTXDForm = RS_dotNetUI.form "propTXDForm" text:" R* Prop TXD Ops" size:[164, 244] max:[2048, 2048] min:[164, 244] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
				propTXDForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
				dotnet.addEventHandler propTXDForm "FormClosing" dispatchOnFormClosing
				dotnet.addEventHandler propTXDForm "Enter" disableAccelerators
				dotnet.addEventHandler propTXDForm "Resize" dispatchOnFormResize
				
				TXDLabel = RS_dotNetUI.Label "TXDLabel" text:(" TXD List:") \
														textAlign:RS_dotNetPreset.CA_MiddleLeft borderStyle:RS_dotNetPreset.BS_None \
														dockStyle:RS_dotNetPreset.DS_Fill 
				
				selectDisplayItem = RS_dotNetUI.ToolStripMenuItem "selectDisplayItem" text:"Select"
				dotnet.addEventHandler selectDisplayItem "Click" dispatchSelectDisplayClick
				
				mainTreeMenu = RS_dotNetUI.ContextMenuStrip "TXDTreeMenu" items:#(selectDisplayItem)
				dotNet.addEventHandler mainTreeMenu "Opening" dispatchMainTreeMenuOpening
				
				mainTree = RS_dotNetUI.treeView "mainTree" dockStyle:RS_dotNetPreset.DS_Fill
				mainTree.Sorted = false
				mainTree.FullRowSelect = true
				mainTree.LabelEdit = true
				mainTree.AllowDrop = false
				mainTree.ContextMenuStrip = mainTreeMenu
				mainTree.BackColor = (DotNetClass "System.Drawing.SystemColors").InactiveBorder --Control
				mainTree.imageList = RS_dotNetObject.imageListObject IconArray imageSize:[24,24]
				dotnet.addEventHandler mainTree "Enter" disableAccelerators
				dotnet.addEventHandler mainTree "BeforeLabelEdit" dispatchTreePreLabelEdit
				dotnet.addEventHandler mainTree "AfterLabelEdit" dispatchTreePostLabelEdit
	-- 			dotnet.addEventHandler mainTree "AfterSelect" dispatchMainTreeSelect
				
				preFixTextBox = RS_dotNetUI.textBox "preFixTextBox" borderStyle:RS_dotNetPreset.BS_Fixed3D dockStyle:RS_dotNetPreset.DS_Fill
				preFixTextBox.margin = (RS_dotNetObject.paddingObject 2)
				preFixTextBox.MaxLength = 14
				preFixTextBox.text = (getFilenameFile maxFilename)
				
				packButton = RS_dotNetUI.Button "packButton" text:"Pack By Usage" dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 0)
				dotnet.addEventHandler packButton "click" dispatchPackClick
				
				mainTableLayout = RS_dotNetUI.tableLayout "mainTableLayout" text:"mainTableLayout" collumns:#((dataPair type:"Percent" value:50)) \
											rows:#((dataPair type:"Absolute" value:24),(dataPair type:"percent" value:50),(dataPair type:"Absolute" value:24),(dataPair type:"Absolute" value:24)) \
											dockStyle:RS_dotNetPreset.DS_Fill
				
				mainTableLayout.controls.add TXDLabel 0 0
				mainTableLayout.controls.add mainTree 0 1
				mainTableLayout.controls.add preFixTextBox 0 2
				mainTableLayout.controls.add packButton 0 3
				
				propTXDForm.controls.add mainTableLayout
				
				propTXDForm.show maxHwnd
				
				getMapFileData()
				getSceneTXDData()
				
				addTreviewNode TXDArray
			)
			else
			(
				messageBox "This is not a prop file." title:"Error..."	
			)
		)
	)
)

-- if the global struct instance is not undefined the form is probably open so we close it
if RSL_PropTXD != undefined then
(
	try
	(
		RSL_PropTXD.propTXDForm.close()
	)
	catch()
)

RSL_PropTXD = RSL_PropTXDOps()
RSL_PropTXD.createForm()

