

struct RSL_AutoSurfaceTypeAssign
(
	surfaceTypeArray = #("BRICK", "BUSHES", "CARDBOARD_SHEET", "CARPET_SOLID", "CERAMIC", "CLOTH", "CONCRETE", "DIRT_TRACK", "GRASS", "GRAVEL_SMALL", "HAY", "ICE", "LEATHER", "MARBLE", "METAL_CORR_THICK", \
									"METAL_CORR_THIN", "METAL_ELECTRIC", "MUD_SOFT", "PAPER", "PLASTER_SOLID", "PLASTIC", "RUBBER", "SAND_LOOSE", "STONE", "WINDOW_GLASS_MEDIUM_MED", "WOOD_SOLID_MEDIUM"),

	pathAndKeyWordArray = #(#(#("Walls_ext"),#("brick")),#(#( "Vegetation"),#("hedge", "jungle", "overgrowth", "flowers")),#(#(),#("cardboard")),#(#(),#("carpet")),#(#(),#("ceramic")),#(#("Fabric"),#()), \
										#(#("Concrete", "Grounds_ext", "Roads"),#("concrete")),#(#(),#("dirt")),#(#(),#("grass", "ground", "mulch", "moss")),#(#(),#("gravel")),#(#(),#("hay")),#(#(),#("ice")), \
											#(#(),#("leather")),#(#("Tiles"),#("marble")),#(#("Doors", "Roof"), #()),#(#("Signage", "Signs"), #()),#(#("Electrical_misc"),#()),#(#(), #("mud")),#(#("Paper"), #("paper")), \
												#(#("Ceilings", "Masonry", "Walls_int"),#("plaster")),#(#("Plastic"), #("plastic")),#(#(), #("rubber")),#(#(), #("sand")),#(#("Stone"), #("stone", "granite", "slate")), \
													#(#("Glass", "Windows"), #("glass")),#(#("Fences"), #("wood"))),

	attrIndex = getattrindex "Gta Collision" "Coll Type",

	rexbound,

	fn isRexbound material =
	(
		case (classOf material) of
		(
			default:(rexbound = false)
			multimaterial:
			(
				for i = 1 to material.materialList.count do
				(
					local subMaterial = material.materialList[i]
					
					isRexBound subMaterial
				)
			)
			RexBoundMtl:
			(
				rexbound = true
			)
		)
		
		rexbound
	),

	fn getTypeIndex filename =
	(
		local result = 0
		
		for i = 1 to pathAndKeyWordArray.count do
		(
			local paths = pathAndKeyWordArray[i][1]
			local keywords = pathAndKeyWordArray[i][2]
			
			local pathMatch
			
			for path in paths do
			(
				local match = matchPattern filename pattern:("*" + path + "*")
				
				if match then
				(
					result = i
				)
			)
			
			local name = getFilenameFile filename
			
			for word in keywords do
			(
				local match = matchPattern name pattern:("*" + word + "*")
				
				if match then
				(
					result = i
				)
			)
		)
		
		result
	),

	mapped fn appendMatchingID face ID object array =
	(
		case (classOf object) of
		(
			triMesh:
			(
				local currentID = (getFaceMatID object face.index)
				if currentID == ID then append array face
			)
			Editable_mesh:
			(
				local currentID = (getFaceMatID object face.index)
				if currentID == ID then append array face
			)
			Editable_Poly:
			(
				local currentID = (polyop.getFaceMatID object face.index)
				if currentID == ID then append array face
			)
		)
	),

	fn gatherFacesByID ID object =
	(
		local result = #()
		local faceArray = for face in object.faces collect face
		
		appendMatchingID faceArray ID object result
		
		result
	),

	fn faceArea faceArray object =
	(
		local result = 0
		
		local indexArray = for face in faceArray collect face.index
		
		case (classOf object) of
		(
			triMesh:
			(
				result = meshop.getFaceArea object indexArray
			)
			Editable_mesh:
			(
				result = meshop.getFaceArea object indexArray
			)
			Editable_Poly:
			(
				for index in indexArray do
				(
					result += polyOp.getFaceArea object index
				)
			)
		)
		
		result
	),

	fn getDiffuseFromRageShader rageShader =
	(
		local result
		local count = RstGetVariableCount rageShader
		
		for i = 1 to count do
		(
			local name = (RstGetVariableName rageShader i)
			local variable = (RstGetVariable rageShader i)
			
			if name == "Diffuse Texture" then
			(
				result = variable
			)
		)
		
		result
	),

	fn diffuseFromMaterialStandard material object =
	(
		local result
		
		case (classOf material) of
		(
			Rage_Shader:
			(
				result = getDiffuseFromRageShader material
			)
			multimaterial:
			(
				local largestArea = 0
				local index = 0
				
				for i = 1 to material.materialList.count do
				(
					local currentIndex = material.materialIDList[i]
					local faceArray = gatherFacesByID currentIndex object
					local currentArea = faceArea faceArray object
					
					if currentArea > largestArea then
					(
						largestArea = currentArea
						index = currentIndex
					)
				)
				
				if index > 0 then
				(
					local subMaterial = material.materialList[index]
					
					diffuseFromMaterialStandard subMaterial object
				)
			)
		)
		
		result
	),

	fn asssignStandard filename collision =
	(
		if filename != undefined then
		(
			local typeIndex = getTypeIndex filename
			
			if typeIndex != 0 then
			(
				newType = surfaceTypeArray[typeIndex]
				
				setAttr collision attrIndex newType
			)
			else
			(
				setAttr collision attrIndex "CONCRETE"
			)
		)
		else
		(
			setAttr collision attrIndex "CONCRETE"
		)
	),

	mapped fn processStandard collision defaultOnly =
	(
		local okToContinue = true
		
		if defaultOnly then
		(
			local currentType = (toLower (getattr collision attrIndex))
			
			if currentType == "" or currentType == UndefinedClass then currentType = "default"
			
			okToContinue = currentType == "default"
		)
		
		if okToContinue then
		(
			local renderObject = collision.parent
			local filename
			
			if renderObject != undefined then
			(
				asssignStandard (diffuseFromMaterialStandard renderObject.material renderObject) collision
			)
		)
	),

	fn assignRexBound filename material =
	(
		case (classOf material) of
		(
			RexBoundMtl:
			(
				if filename != undefined then
				(
					local typeIndex = getTypeIndex filename
					
					if typeIndex != 0 then
					(
						newType = surfaceTypeArray[typeIndex]
						
						RexSetCollisionName material (toUpper newType)
					)
					else
					(
						RexSetCollisionName material (toUpper "CONCRETE")
					)
				)
				else
				(
					RexSetCollisionName material (toUpper "CONCRETE")
				)
			)
			multimaterial:
			(
				for i = 1 to material.materialList.count do
				(
					local subMaterial = material.materialList[i]
					
					assignRexBound filename subMaterial 
				)
			)
		)
	),

	mapped fn processRexBound collision defaultOnly =
	(
		local renderObject = collision.parent
		local filename
		
		if renderObject != undefined then
		(
			case (classOf renderObject.material) of
			(
				default:print (classOf renderObject.material)
				Rage_Shader:
				(
					assignRexBound (getDiffuseFromRageShader renderObject.material) collision.material
				)
				multimaterial:
				(
					case (classOf collision.material) of
					(
						RexBoundMtl:
						(
							assignRexBound (diffuseFromMaterialStandard renderObject.material renderObject) collision.material
						)
						multimaterial:
						(
							local hitMesh = copy (convertTo renderObject editable_mesh)
							col2Mesh collision
							
							for i = 1 to collision.material.materialList.count do
							(
								local subMaterial = collision.material.materialList[i]
								
								if subMaterial != undefined and (classOf subMaterial) == RexBoundMtl then
								(
									local okToContinue = true
									local currentType
									
									if defaultOnly then
									(
										currentType = (toLower (RexGetCollisionName subMaterial))
										if currentType == "" or currentType == UndefinedClass then currentType = "default"
										
										okToContinue = currentType == "default"
									)
									
									if okToContinue then
									(
										local currentIndex = collision.material.materialIDList[i]
										local faceArray = gatherFacesByID currentIndex collision
										local indexArray = #()
										
										for face in faceArray do
										(
											local index = face.index
											local dir = -(getFaceNormal collision index)
											local pos = (meshop.getFaceCenter collision index) + (-dir * 0.1)
											
											local newRay = ray pos dir
											
											local hit = intersectRayEx hitMesh newRay
											
											if hit != undefined then
											(
												local hitID = (getFaceMatID hitMesh hit[2])
												
												if indexArray[hitID] == undefined then
												(
													indexArray[hitID] = 1
												)
												else
												(
													indexArray[hitID] += 1
												)
											)
										)
										
										local ID = 0
										local largestIndex = 0
										
										for j = 1 to indexArray.count do
										(
											local index = indexArray[j]
											
											if index != undefined and index > largestIndex then
											(
												largestIndex = index
												ID = j
											)
										)
										
										if ID != 0 then
										(
											renderMaterial = renderObject.material.materialList[ID]
											
											if renderMaterial != undefined then
											(
												case (classOf renderMaterial) of
												(
													default:print (classOf renderMaterial)
													Rage_Shader:
													(
														assignRexBound (getDiffuseFromRageShader renderMaterial) subMaterial
													)
												)
											)
											else
											(
												RexSetCollisionName subMaterial (toUpper "CONCRETE")
											)
										)
										else
										(
											RexSetCollisionName subMaterial (toUpper "CONCRETE")
										)
									)
								)
							)
							
							
							mesh2Col collision
							try (delete hitMesh) catch()
						)
					)
				)
			)
			
			
			
		)
	),

	fn processCollision defaultOnly =
	(
		local okToContinue = queryBox ("This will change all DEFAULT surface types. It will attempt to match to a particular surface type based on texture names\n" \
													+ "or set to CONCRETE if it cannot match. Are you sure you want to continue?") title:"Warning..."
		
		if okToContinue then
		(
			local collisionArray = for object in objects where (getAttrClass object) == "Gta Collision" collect object
			local standardArray = #()
			local rexBoundArray = #()
			
			for collision in collisionArray do
			(
				if (isRexbound collision.material) then
				(
					append rexBoundArray collision
				)
				else
				(
					append standardArray collision
				)
			)
			
			if standardArray.count > 0 then
			(
				processStandard standardArray defaultOnly
			)
			
			if rexBoundArray.count > 0 then
			(
				processRexBound rexBoundArray defaultOnly
			)
		)
	)

-- 	processCollision true
)

RS_AutoAssign = RSL_AutoSurfaceTypeAssign()
RS_AutoAssign.processCollision true
