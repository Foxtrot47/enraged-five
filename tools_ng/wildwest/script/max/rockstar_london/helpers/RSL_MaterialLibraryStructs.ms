
struct RSL_RageShader
(
	name,
	node,
	shader,
	memory = 0.0,
	state = 0,
	alpha,
	properties = #(),
	
	memory1024 = 699192,
	memory1024Alpha = 1398256,
	
	fn calculateMemory size alpha =
	(
		if alpha then
		(
			ceil (((size.x * size.y) / (1024.0 * 1024.0) * memory1024Alpha) / 1024.0)
		)
		else
		(
			ceil (((size.x * size.y) / (1024.0 * 1024.0) * memory1024) / 1024.0)
		)
	),
	
	fn getMemory =
	(
		memory = 0.0
		
		for property in properties where property.type == "texmap" do 
		(
			memory += (calculateMemory property.size (property.alpha != undefined))
		)
	),
	
	fn getDiffuse =
	(
		local result
		
		for property in properties where property.type == "texmap" do 
		(
			if (stricmp property.name "diffuse texture") == 0 then
			(
				result = property.value
			)
		)
		
		result
	),
	
	fn itemOfName name =
	(
		local result
		
		for entry in properties do 
		(
			if entry.name == name then
			(
				result = entry
			)
		)
		
		result
	),
	
	fn compare other =
	(
		local result = (stricmp shader other.shader) == 0 and alpha == other.alpha and properties.count == other.properties.count
		
		if result then
		(
			for i = 1 to properties.count do
			(
				if not (properties[i].compare other.properties[i]) then
				(
					result = false
					exit
				)
			)
		)
		
		result
	),
	
	fn toTreeView rootTreeNode =
	(
		local memoryNode = rootTreeNode.Nodes.Add "Memory"
		
		memoryNode.Nodes.Add ("PC : " + (memory * RSL_MaterialLibrary.scalePC) as string + "K")
		memoryNode.Nodes.Add ("360 : " + (memory * RSL_MaterialLibrary.scale360) as string + "K")
		memoryNode.Nodes.Add ("PS3 : " + (memory * RSL_MaterialLibrary.scalePS3) as string + "K")
		
		memoryNode.expand()
		
		local shaderNode = rootTreeNode.Nodes.Add ("Shader : " + shader)
		local alphaNode = rootTreeNode.Nodes.Add ("Alpha : " + alpha as string)
		
		local propertiesNode = rootTreeNode.Nodes.Add "Properties"
		
		for entry in properties do 
		(
			local propertyNode = entry.asTreeNode()
			
			if propertyNode != undefined then
			(
				propertiesNode.Nodes.Add propertyNode
			)
		)
		
		propertiesNode.expand()
		
		if (isProperty rootTreeNode #expand) then
		(
			rootTreeNode.expand()
		)
	)
)

struct RSL_ShaderAttribute
(
	name = "",
	type = "",
	value,
	alpha,
	size = [0,0],
	
	fn compare other =
	(
		local result = (stricmp name other.name) == 0 and type == other.type
		
		if result then
		(
			case (classOf value) of
			(
				default:print (classOf value)
				UndefinedClass:()--(format "Unsupported rage shader variable : % Type:% Value:%\n" name type value)
				String:(result = (stricmp value other.value) == 0)
				Float:(result = abs (value - other.value) < 0.0001)
				Point3:(result = distance value other.value < 0.0001)
			)
		)
		
		result
	),
	
	fn asTreeNode =
	(
		if value != undefined then
		(
			local result = dotNetObject "TreeNode" name
			
			case type of
			(
				default:
				(
					print type
				)
				"texmap":
				(
					local valueNode = result.nodes.Add (filenameFromPath value)
					valueNode.tag = dotNetMXSValue value
					
					if alpha != undefined then
					(
						local alphaNode = result.nodes.Add (filenameFromPath alpha)
						alphaNode.tag = dotNetMXSValue alpha
					)
					
					result.expand()
				)
				"float":
				(
					result.text += " : " + value as String
					result.tag = dotNetMXSValue value
				)
				"vector3":
				(
					local valueNode = result.nodes.Add (value as string)
					valueNode.tag = dotNetMXSValue value
					
					result.expand()
				)
				"vector4":
				(
					local valueNode = result.nodes.Add (value as string)
					valueNode.tag = dotNetMXSValue value
					
					result.expand()
				)
			)
			
			result
		)
	)
)

struct RSL_MaterialLibraryData
(
	library,
	filename = "New Library",
	display = "displayList",
	editable = true,
	listViewCache = #(),
	first
)
