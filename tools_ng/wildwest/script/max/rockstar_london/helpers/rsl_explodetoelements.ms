theObject = selection[1]

rollout RSL_explodeToElements "Explode To Elements" width:140
(
	local typeBool = false
	
	fn explodeToElements theObject asOne:false =
	(	
		local faceArray = #()
		
		if theObject.selectedFaces.count > 0 then
		(
			faceArray = for face in theObject.selectedFaces collect face.index
		)
		else
		(
			faceArray = for face in theObject.faces collect face.index
		)
		
		if asOne then
		(
			polyop.detachFaces theObject faceArray delete:true asNode:false
		)
		else
		(
			for index in faceArray do
			(
				polyop.detachFaces theObject #(index) delete:true asNode:false
			)
		)
	)
	
	checkBox chkb_asOne "One Element"
	checkBox chkb_perFace "Per Face" checked:true
	button btn_explode "Explode" across:2 width:60 offset:[-4,0]
	button btn_cancel "Cancel" width:60 offset:[4,0]
	
	on chkb_asOne changed state do
	(
		chkb_asOne.checked = true
		chkb_perFace.checked = false
		
		typeBool = true
	)
	
	on chkb_perFace changed state do
	(
		chkb_perFace.checked = true
		chkb_asOne.checked = false
		
		typeBool = false
	)
	
	on btn_explode pressed do
	(
		explodeToElements theObject asOne:typeBool
		
		destroyDialog RSL_explodeToElements
	)
	
	on btn_cancel pressed do
	(
		destroyDialog RSL_explodeToElements
	)
)

if theObject != undefined and (classOf theObject) == editable_poly then
(
	try (destroyDialog RSL_explodeToElements) catch()

	createDialog RSL_explodeToElements style:#(#style_toolwindow,#style_sysmenu) modal:true
)
else (

	messagebox "No editable poly selected"
)