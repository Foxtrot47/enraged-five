(
	--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
	-- Local and UI Declarations
	--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
	
	local NodeArrayIndex

	processForm = dotNetObject "MaxCustomControls.MaxForm"
	ListBox1 = dotNetObject "ListBox"
	GroupBox1 = dotNetObject "GroupBox"
	BtnSelectMilo = dotNetObject "Button"
	BtnSetSelected = dotNetObject "Button"
	BtnSetAll = dotNetObject "Button"
	BtnSelAllPortals = dotNetObject "Button"
		
	--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
	-- Main Milo Collision Functions
	--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
	
	-- Pass it an Id and it will check each dependent object of the source object to see if any of the IDs are shared
	-- Will retun an array of with the Shared IDs in it.
	-- Paramiters = IDs - An array of IDs to check for Unequness SelOnj - The Source Object for the Ids to be checked.
	
	fn IsMaterialIDUnequeToObject IDs SelObj=
	(
		ObjectList = refs.dependentnodes SelObj.Mat
		local SharedIDs=#()
			
		for ObjectItem in ObjectList do
		(
			if classof ObjectItem == col_mesh then
			(
				if ObjectItem != SelObj then
				(
					--ToggleMeshToCollision ObjectItem
					col2mesh ObjectItem
					Faces = ObjectItem.Faces
					for I = 1 to (Faces.count) do
					(
						for ID in IDs do
						(
							if (getFaceMatID ObjectItem I) == ID then
							(
								appendIfUnique SharedIDs ID
							)
						)
					)
				)
				else
				(
					messageBox "You have objects with this mateiral assigned that are not Colision Meshes, They may need converting back to Col_Mesh"
				)
				mesh2Col ObjectItem
				--ToggleMeshToCollision ObjectItem
			)
		)
		SharedIDs
	)	

	-- Gets the Materil IDs used on the object

	fn GetMaterialsOnFaceSelection ObjectMaterial SelObj = 
	(
		SelectedFaces = SelObj.Faces 
		local MatIDsInSelection=#()
		
		for I = 1 to (SelectedFaces.count) do
		(	
			appendIfUnique MatIDsInSelection (getFaceMatID SelObj I)
		)	
		MatIDsInSelection
	)

	-- To set all faces of one ID to another ID - CurrentID is the Id that you want to change from and NewID is the ID you want to Change to
	-- Must be in face mode

	fn ChangeCurrentID SelObj CurrentID NewID =
	(
		col2mesh SelObj
		for I = 1 to (SelObj.Faces.count) do
		(
			if (getFaceMatID SelObj I) == CurrentID then
			(
				setFaceMatID SelObj I NewID
			)
		)
	)

	-- Toggle Mesh to Collision

	fn ToggleMeshToCollision selObj =
	(
		if (classof selObj) == Col_Mesh then
		(
			col2mesh selObj  
		)
		else
		(
			mesh2col selObj
		)
	)

	-- Main Function

	fn MatCheck SelObj MiloRoom =
	(
		MaterialClass = (classof SelObj.material) 
		
		if classof SelObj == Col_mesh then
		(
			col2mesh SelObj
		)
				
		if MaterialClass == Multimaterial then
		(
			local UsedMatIDs
			local SharedMatIDs
					
			ObjectMaterial = SelObj.material
			UsedMatIDs = GetMaterialsOnFaceSelection ObjectMaterial SelObj
			SharedMatIDs = IsMaterialIDUnequeToObject UsedMatIDs SelObj
			
			UsedMatIDs = sort(UsedMatIDs)
			
			print "+++++++++++++++++++++++++++++++"
			print "Used Material ID Numbers"
			print UsedMatIDs
			print "+++++++++++++++++++++++++++++++"
			print "Shared Material ID Numbers"
			print SharedMatIDs
			print "+++++++++++++++++++++++++++++++"
			
			CurrentIDCount = (SelObj.material.count)
		
			for ID in UsedMatIDs do
			(
				local SubMaterial = ObjectMaterial[ID]
				local NotUnequeID = false
				
				print "_+_+_++_+_+_+_+_+_+_+_+_+"
				print ID
				print "_+_+_++_+_+_+_+_+_+_+_+_+"
				
				--- To find if the current ID from UsedMatIDs is used only on this object.
				if (findItem SharedMatIDs ID) != 0 then
				(
					NotUnequeID = true
				)
				
				-- If its a RexBoundMtl then set the Milo Room. If the ID is not uneique to this object it will create a new material with a new ID
				if (classof SubMaterial) == RexBoundMtl then
				(
					print "====== Valid RexBound Material ======"
					print ("Setting " + SubMaterial.name + " to Milo Room " + MiloRoom.name)
					print "=============================="
					
					if NotUnequeID == true then
					(
						CopyMat = copy SubMaterial
						ObjectMaterial.count = (CurrentIDCount + 1)
						ObjectMaterial[(CurrentIDCount + 1)] = CopyMat
						RexSetRoomNode ObjectMaterial[(CurrentIDCount + 1)] MiloRoom
						ChangeCurrentID SelObj ID (CurrentIDCount + 1)
						CurrentIDCount += 1
					)
					else
					(
						RexSetRoomNode SubMaterial MiloRoom
					)
				)
				-- If its a valid material type as part of a Multisub object but is not a RexBoundMtl then it will offer the optiont to convert it over.
				else if (classof SubMaterial) != UndefinedClass and (classof SubMaterial) != RexBoundMtl then
				(
					print "====== Non Rex Bound ======"
					print ObjectMaterial[ID]
					print "========================"
					
					if queryBox ("This is a valid Material but is not a RexBoundMtl and is assigned to a collision Object.\n" + (ObjectMaterial[ID] as string) + "\nDo you Want to convert it to a RexBoundMtl and set the Milo Room?") beep:false then
					(
						print ("Converting " + (SubMaterial as string) + " To RexBoundMtl and setting Room " + (MiloRoom as string))
						
						if NotUnequeID == true then
						(
							CopyMat = copy SubMaterial
							ObjectMaterial.count = (CurrentIDCount + 1)
							ObjectMaterial[(CurrentIDCount + 1)] = CopyMat
							NewSubMaterial = (ObjectMaterial[(CurrentIDCount + 1)] = RexBoundMtl())
							RexSetRoomNode NewSubMaterial MiloRoom
							ChangeCurrentID SelObj ID (CurrentIDCount + 1)
							CurrentIDCount += 1	
						)
						else
						(
							NewSubMaterial = (ObjectMaterial[ID] = RexBoundMtl())
							RexSetRoomNode NewSubMaterial MiloRoom
						)
					)
				)
				-- An ID is on the object but has returned an unset material slot. Will offer to add a RexBoundMtl object into the empty slot
				else if (classof SubMaterial) == UndefinedClass then
				(
					print "====== Unnasigned Material Type ======"
					print ObjectMaterial[ID]
					print "================================"
					
					if queryBox ("This is not a valid ID but is assigned to a collision Object.\n" + (ObjectMaterial[ID] as string) + "\nDo you Want to Create a RexBoundMtl and set the Milo Room?") beep:false then
					(
						print ("Converting " + (SubMaterial as string) + " To RexBoundMtl and setting Room " + (MiloRoom as string))
							
						if NotUnequeID == true then
						(
							print "NON UNEQIE MAT"
							CopyMat = copy SubMaterial
							ObjectMaterial.count = (CurrentIDCount + 1)
							ObjectMaterial[(CurrentIDCount + 1)] = CopyMat
							NewSubMaterial = (ObjectMaterial[(CurrentIDCount + 1)] = RexBoundMtl())
							RexSetRoomNode NewSubMaterial MiloRoom
							ChangeCurrentID SelObj ID (CurrentIDCount + 1)
							CurrentIDCount += 1	
						)
						else
						(
							print "UNEQIE MAT"
							if ID > CurrentIDCount then
							(
								ObjectMaterial.count = (CurrentIDCount + 1)
								NewSubMaterial = (ObjectMaterial[(CurrentIDCount + 1)] = RexBoundMtl())
								RexSetRoomNode NewSubMaterial MiloRoom
								ChangeCurrentID SelObj ID (CurrentIDCount + 1)
								CurrentIDCount += 1
							)
							else
							(
								NewSubMaterial = (ObjectMaterial[ID] = RexBoundMtl())
								RexSetRoomNode NewSubMaterial MiloRoom
								CurrentIDCount += 1
							)
						)
					)
				)
			)
		)
		-- No Material has been found on this object for that ID, Will add a Muitli sub object material and then set ID 1 to a  RexBoundMtl.
		else if MaterialClass == UndefinedClass then
		(
			print  "Add Material and run material setup"
			if queryBox ("There is no Sub Object Material Applyed to this object.\n" + (SelObj.name as string) + "\nDo you want to create one?") beep:false then
			(
				SelObj.mat = Multimaterial()
				SelObj.mat.count = 1
				NewSubMaterial = SelObj.mat[1] = RexBoundMtl()
				RexSetRoomNode NewSubMaterial MiloRoom
				
				MatCheck SelObj MiloRoom
				
			)
		)
		
		mesh2col SelObj
	)

-- 	MiloRoom = selection[1]
-- 	SelObj = selection[1]
-- 	MatCheck SelObj MiloRoom

	--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
	-- UI Functions
	--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
	
	fn SelectMilo =
	(
		Selected = ListBox1.SelectedIndex
		if Selected != -1 then
		(			
			select NodeArrayIndex[Selected +1]
		)
	)
	
	fn RecursiveSelect obj =
	(
		for child in obj.children do
		(
			RecursiveSelect child
		)	
		selectMore obj
	)

	
	
	fn SelectAllPortals =
	(
		PortalList = for object in objects where (classOf object) == GtaMloPortal collect object
		max select none
		select PortalList
		
		
	)
	
	fn SelectAll =
	(
		MiloList = for object in objects where (classOf object) == GtaMloRoom collect object
		
		for i in MiloList do
		(
			RecursiveSelect i
		)

		
	)
	
	fn SetSelcted =
	(
		Selected = ListBox1.SelectedIndex
		if Selected != -1 then
		(
			MiloRoom = NodeArrayIndex[Selected +1]
			
			ObjectsToSet = selection as array
			for Item in ObjectsToSet do
			(
				if (Classof Item) == Col_Mesh then
				(
					MatCheck Item MiloRoom
				)
				else 
				(
					messagebox ("This Item is not a ColMesh, Do you need to convert it back to Colmesh? /n" + Item.name as string)	
				)
			)
		)
	)

	fn AddObjects Root Items =
	(
		NodeArrayIndex=#()
		for I in Items do
		(
			ItemIndex = Root.Items.add I.name 
			NodeArrayIndex[ItemIndex + 1] = I
		)
		NodeArrayIndex
	)
		
	fn CreateUI =
	(
		processForm.Width = 350
		processForm.Height = 400
		processForm.Text = "Milo Colision Tool"
		
		GroupBox1.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		processForm.Controls.Add 	GroupBox1
		
		ListBox1.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		GroupBox1.Controls.Add ListBox1
		
		BtnSelectMilo.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Bottom
		processForm.Controls.Add BtnSelectMilo
		BtnSetSelected.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Bottom
		processForm.Controls.Add BtnSetSelected
		BtnSetAll.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Bottom
		processForm.Controls.Add BtnSetAll
		BtnSelAllPortals.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Bottom
		processForm.Controls.Add BtnSelAllPortals

		BtnSelectMilo.Text = "Select Milo In Viewport"
		BtnSetSelected.Text = "Set Selected Collision Objects to Room"
		BtnSetAll.Text = "Select All Rooms and there Children"
		BtnSelAllPortals.Text = "Select All Portals"
		ListBox1.Text = "Select a milo room"
	
		processForm.ShowModeless()
						
		MiloList = for object in objects where (classOf object) == GtaMloRoom collect object

		NodeArrayIndex = AddObjects ListBox1 MiloList
				
		dotnet.addEventHandler BtnSelectMilo "Click" SelectMilo
		dotnet.addEventHandler BtnSetSelected "Click" SetSelcted
		dotnet.addEventHandler BtnSetAll "Click" SelectAll
		dotnet.addEventHandler BtnSelAllPortals "Click" SelectAllPortals
		
		
	)

	CreateUI()
)

	

