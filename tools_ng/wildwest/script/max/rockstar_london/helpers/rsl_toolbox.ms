global RSL_ToolBoxOps

fileIn "rockstar\\export\\settings.ms"
fileIn (scriptsPath + "pipeline\\util\\xml.ms")
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetUIOps.ms")


struct RSL_ToolBoxFunction
(
	name,
	group,
	code,
	icon,
	tooltip,
	hover
)

struct RSL_ToolBox
(
	toolBoxXMLFile = (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_ToolBox.xml"),
	xmlStruct,
	scriptsDirectory = (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\wip_scripts\\"),
	--(substituteString scriptsPath "scripts" "WIP_scripts"), --"x:\\tools\\dcc\\current\\max2009\\WIP_scripts\\"
	localConfigINI = RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_ToolBox.ini",
	functionBackground = RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\images\\background01.png",
	autoLoadBool,
	
	toolBox,mainTable,groupLayout,functionLayout,autoLoadMenu,hoverPanel,hoverFunctionLayout,toolBoxTimer,
	
	scriptGroups = #("Object", "Vertex", "Edge", "Poly", "Scripts"),
	groupFunctions = #(#("1","2","3","4"),#("a","b","c","d"),#("me","you","them","us"),#("ten","twenty","thirty","fourty"),#(),#()),
	sizeChange = false,
	lastLayoutChange = "width",
	
	groupButtons = #(),
	functionArrays = #(),
	
	lastGroup,
	
	fn readLocalConfigINI form =
	(
		if (doesFileExist localConfigINI) then
		(
			local positionINISetting = (getINISetting localConfigINI "Main" "Position")
			local sizeINISetting = (getINISetting localConfigINI "Main" "Size")
			
			if positionINISetting != "" then
			(
				local formPosition = readValue ((getINISetting localConfigINI "Main" "Position") as stringStream)
				
				form.location.x = formPosition.x
				form.location.y = formPosition.y
			)
			
			if sizeINISetting != "" then
			(
				local formSize = readValue ((getINISetting localConfigINI "Main" "Size") as stringStream)
				
				form.width = formSize.x
				form.height = formSize.y
			)
		)
		else
		(
			format "RSL_TooBox.ini not found, creating file."
			local newFile = createFile localConfigINI
			close newFile
		)
	),
	
	fn writeLocalConfigINI form =
	(
		if (doesFileExist localConfigINI == false) then
		(
			local newFile = createFile localConfigINI
			close newFile
		)
		
		setINISetting localConfigINI "Main" "Position" ([form.location.x,form.location.y] as string)
		setINISetting localConfigINI "Main" "Size" ([form.width,form.height] as string)
		setINISetting localConfigINI "Main" "LastGroup" lastGroup
		setINISetting localConfigINI "Main" "OpenOnLoad" (autoLoadBool as string)
	),
	
	fn getXMLNodeByName XMLNode name =
	(
		local result
		
		for i = 0 to XMLNode.ChildNodes.Count - 1 do
		(
			local currentNode = XMLNode.ChildNodes.Item[i]
			
			if (toLower currentNode.name) == (toLower name) then
			(
				result = currentNode
			)
		)
		
		result
	),
	
	fn readToolBoxXML =
	(
		local fileInNode = getXMLNodeByName xmlStruct.document.DocumentElement "Startup"
		local groupsNode = getXMLNodeByName xmlStruct.document.DocumentElement "groups"
		
		scriptGroups = #()
		scriptGroups.count = groupsNode.ChildNodes.Count
		groupFunctions = #()
		groupFunctions.count = scriptGroups.count
		
		for i = 0 to fileInNode.ChildNodes.Count - 1 do
		(
			local currentNode = fileInNode.ChildNodes.Item[i]
			local code = substituteString (currentNode.Attributes.ItemOf "code").value "'" "\""
			
			execute code
		)
		
		for i = 0 to groupsNode.ChildNodes.Count - 1 do
		(
			groupFunctions[i + 1] = #()
			
			local currentNode = groupsNode.ChildNodes.Item[i]
			
			scriptGroups[i + 1] = #(currentNode.name, currentNode.Attributes.Item[0].value)
			
			for c = 0 to currentNode.ChildNodes.count - 1 do
			(
				local childNode = currentNode.ChildNodes.Item[c]
				local name = (childNode.Attributes.ItemOf "name").value
				local code = substituteString (childNode.Attributes.ItemOf "code").value "'" "\""
				local icon = (childNode.Attributes.ItemOf "icon").value
				local toolTip = (childNode.Attributes.ItemOf "tooltip").value
				
				if toolTip == "" or toolTip == undefined then
				(
					toolTip = name
				)
				
				append groupFunctions[i + 1] (RSL_ToolBoxFunction name:name group:currentNode.name code:code icon:icon tooltip:toolTip)
			)
		)
	),
	
	fn spaceBeforeCapital text =
	(
		local result = ""
		
		for i = 1 to text.count do
		(
			if i > 1 and i < text.count then
			(
				if text[i] == (toUpper text[i]) and text[i + 1] != (toUpper text[i + 1]) then
				(
					result += " " + text[i] 
				)
				else
				(
					result += text[i] 
				)
			)
			else
			(
				result += text[i] 
			)
		)
		
		result
	),
	
	fn generateFromScriptsDir =
	(
		local scriptFiles = getFiles (scriptsDirectory + "*.ms")
		
		append scriptGroups #("Scripts", "Here Be Dragons")
		append groupFunctions #()
		
		for entry in scriptFiles do
		(
			local code = "fileIn \"" + (substituteString entry "\\" "/") + "\""
			local name = spaceBeforeCapital (substituteString (getFilenameFile entry) "_" " ")
			append groupFunctions[groupFunctions.count] (RSL_ToolBoxFunction name:name group:"Scripts" code:code toolTip:name)
		)
	),
	
	fn loadXML =
	(
		local result = true
		
		if (getFiles toolBoxXMLFile).count == 1 then
		(
			xmlStruct = XmlDocument()
			xmlStruct.init()
			xmlStruct.load toolBoxXMLFile
			
			if xmlStruct != undefined then
			(
				readToolBoxXML()
				
				generateFromScriptsDir()
			)
			else
			(
				result = false
			)
		)
		else
		(
			result = false
		)
		
		result
	),
	
	fn getGroupIndex group =
	(
		local result = 0
		for i = 1 to scriptGroups.count do
		(
			if (toLower scriptGroups[i][1]) == (toLower group) then
			(
				result = i
			)
		)
		
		result
	),
	
	
	
	
	
	
	fn dispatchOnEditPressed s e =
	(
		RSL_ToolBoxOps.editPressed s e
	),
	
	fn editPressed s e =
	(
		execute s.tag.value
	),
	
	fn dispatchFunctionClick s e =
	(
		RSL_ToolBoxOps.functionClick s e
	),
	
	fn functionClick s e =
	(
		try
		(
			execute s.tag.value.code
		)
		catch
		(
			messageBox (s.name + " failed to execute...\n" + getCurrentException()) title:"Error..."
		)
	),
	
	fn dispatchFunctionEnter s e =
	(
		RSL_ToolBoxOps.functionEnter s e
	),
	
	fn functionEnter s e =
	(
		toolBoxTimer.enabled = false
	),
	
	fn dispatchFunctionLeave s e =
	(
		RSL_ToolBoxOps.functionLeave s e
	),
	
	fn functionLeave s e =
	(
		toolBoxTimer.enabled = true
	),
	
	fn createFunctions =
	(
		local index = getGroupIndex lastGroup
		local functionToolTip = dotNetObject "ToolTip"
		
		for i = 1 to groupFunctions.count do
		(
			functionArrays[i] = #()
			
			for entry in groupFunctions[i] do
			(
				local editMenu = dotNetObject "ToolStripMenuItem"
				editMenu.text = "Edit..."
				editMenu.tag = dotNetMXSValue (substituteString entry.code "fileIn" "edit")
				
				dotnet.addEventHandler editMenu "Click" dispatchOnEditPressed
				
				local functionMenu = dotNetObject "ContextMenuStrip"
				functionMenu.RenderMode = (dotNetClass "System.Windows.Forms.ToolStripRenderMode").Professional
				functionMenu.Items.Add editMenu
				
				local newFunction = RS_dotNetUI.Button entry.name text:(toUpper entry.name) size:[64,32] icon:entry.icon
				newFunction.tag = dotNetMXSValue entry
-- 				newFunction.BackColor = RS_dotNetPreset.controlColour_Medium
				newFunction.ContextMenuStrip = functionMenu
				
				dotNet.addEventHandler newFunction "MouseClick" dispatchFunctionClick
				dotNet.addEventHandler newFunction "MouseEnter" dispatchFunctionEnter
				
				functionToolTip.SetToolTip newFunction entry.tooltip
				
				append functionArrays[i] newFunction
			)
		)
	),
	
	fn updateHoverPanel group =
	(
		toolBoxTimer.enabled = false
		hoverFunctionLayout.controls.Clear()
		
		local size
		local location
		
		if toolBox.width >= toolBox.height then
		(
			size = [130, 20]
			location = [group.location.x, group.location.y + 19]
		)
		else
		(
			size = [20, 130]
			location = [group.location.x + 19, group.location.y]
		)
		
		hoverPanel.size = RS_dotNetObject.sizeObject size
		hoverPanel.location = RS_dotNetObject.pointObject location
		hoverPanel.text = group.text
		
		local index = getGroupIndex group.text
		
		for control in functionArrays[index] do
		(
			control.width = 16
			control.height = 16
		)
		
		hoverFunctionLayout.controls.addRange functionArrays[index]
	),
	
	fn dispatchGroupClick s e =
	(
		RSL_ToolBoxOps.GroupClick s e
	),
	
	fn GroupClick s e =
	(
		s.checked = true
		lastGroup = s.name
		
		if (toLower hoverPanel.text) == (toLower lastGroup) then
		(
			hoverPanel.location = RS_dotNetObject.pointObject [toolBox.location.x, toolBox.location.y - 30]
		)
		
		functionLayout.Controls.Clear()
		
		local index = getGroupIndex lastGroup
		
		for control in functionArrays[index] do
		(
			control.width = 64
			control.height = 32
		)
		
		functionLayout.controls.addRange functionArrays[index]
		
		for control in groupButtons do
		(
			if control.name != s.name then
			(
				control.checked = false
			)
		)
	),
	
	fn dispatchGroupHover s e =
	(
		RSL_ToolBoxOps.groupHover s e
	),
	
	fn groupHover s e =
	(
		if (toLower s.text) != (toLower lastGroup) then
		(
			updateHoverPanel s
		)
		else
		(
			hoverPanel.location = RS_dotNetObject.pointObject [toolBox.location.x, toolBox.location.y - 30]
		)
	),
	
	fn createGroups =
	(
		groupButtons = #()
		groupButtons.count = scriptGroups.count
		
		local groupToolTip = dotNetObject "ToolTip"
		
		for i = 1 to scriptGroups.count do
		(
			local entry = scriptGroups[i][1]
			local toolTip = scriptGroups[i][2]
			
			local newGroup = RS_dotNetUI.checkBox entry text:(toUpper entry) appearance:RS_dotNetPreset.AC_Button --flatStyle:RS_dotNetPreset.FS_Flat
			newGroup.checked = lastGroup == entry
-- 			newGroup.BackColor = RS_dotNetPreset.controlColour_Light
			
			groupToolTip.SetToolTip newGroup toolTip
			
			if toolBox.width >= toolBox.height then
			(
				newGroup.size = RS_dotNetObject.sizeObject [64,18]
			)
			else
			(
				newGroup.size = RS_dotNetObject.sizeObject [18,64]
			)
			
			dotNet.addEventHandler newGroup "Click" dispatchGroupClick
			dotNet.addEventHandler newGroup "MouseHover" dispatchGroupHover
			
			groupButtons[i] =  newGroup
		)
		
	),
	
	fn updateGroupButtons =
	(
		for entry in groupButtons do
		(
			if toolBox.width >= toolBox.height then
			(
				entry.size = RS_dotNetObject.sizeObject [64,18]
			)
			else
			(
				entry.size = RS_dotNetObject.sizeObject [18,64]
			)
		)
	),
	
	fn dispatchOnAutoLoadPressed s e =
	(
		RSL_ToolBoxOps.autoLoadPressed s e
	),
	
	fn autoLoadPressed s e =
	(
		s.checked = not s.checked
		
		autoLoadBool = (s.checked as string)
	),
	
	fn dispatchOnReloadPressed s e =
	(
		RSL_ToolBoxOps.reloadPressed s e
	),
	
	fn reloadPressed s e =
	(
		fileIn (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_ToolBox.ms")
	),
	
	fn updateGroupLayout group =
	(
		autoLoadBool = (getINISetting localConfigINI "Main" "OpenOnLoad")
		
		if autoLoadBool == "true" then
		(
			autoLoadBool = true
		)
		else
		(
			autoLoadBool = false
		)
		
		local autoLoadMenu = dotNetObject "ToolStripMenuItem"
		autoLoadMenu.Text = "Auto Load"
		autoLoadMenu.checked = autoLoadBool
		
		dotnet.addEventHandler autoLoadMenu "Click" dispatchOnAutoLoadPressed
		
		local reloadMenu = dotNetObject "ToolStripMenuItem"
		reloadMenu.Text = "Reload ToolBox"
		
		dotnet.addEventHandler reloadMenu "Click" dispatchOnReloadPressed
		
		local groupMenu = dotNetObject "ContextMenuStrip"
		groupMenu.RenderMode = (dotNetClass "System.Windows.Forms.ToolStripRenderMode").Professional
		groupMenu.ShowCheckMargin = true
		groupMenu.Items.AddRange #(reloadMenu, (dotNetObject "ToolStripSeparator"), autoLoadMenu)
		
		groupLayout = dotNetObject "FlowLayoutPanel"
-- 		groupLayout.BorderStyle = RS_dotNetPreset.BS_FixedSingle
		groupLayout.dock = RS_dotNetPreset.DS_Fill
		groupLayout.Margin = (RS_dotNetObject.paddingObject 0)
		groupLayout.autoSize = true
		groupLayout.ContextMenuStrip = groupMenu
		dotNet.addEventHandler groupLayout "MouseEnter" dispatchFunctionLeave
		
		groupLayout.controls.AddRange groupButtons
	),
	
	fn dispatchFunctionLayoutMouseWheel s e =
	(
		print "dispatch mouse wheel"
		RSL_ToolBoxOps.functionLayoutMouseWheel s e
	),
	
	fn functionLayoutMouseWheel s e =
	(
		priint e.delta
	),
	
	fn createMainTable =
	(
		mainTable = RS_dotNetUI.tableLayout "mainTable" text:"mainTable" dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNet.CBS_Single
		mainTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		if toolBox.width >= toolBox.height then
		(
			mainTable.RowCount = 2
			mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 20)
			mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 50)
			
			mainTable.ColumnCount = 1
			mainTable.ColumnStyles.add (RS_dotNetObject.ColumnStyleObject "percent" 50)
		)
		else
		(
			mainTable.ColumnCount = 2
			mainTable.ColumnStyles.add (RS_dotNetObject.ColumnStyleObject "absolute" 20)
			mainTable.ColumnStyles.add (RS_dotNetObject.ColumnStyleObject "percent" 50)
			
			mainTable.RowCount = 1
			mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 50)
		)
		
		updateGroupLayout lastGroup
		
		functionLayout = dotNetObject "FlowLayoutPanel"
		functionLayout.BorderStyle = RS_dotNetPreset.BS_FixedSingle
		functionLayout.dock = RS_dotNetPreset.DS_Fill
		functionLayout.Margin = (RS_dotNetObject.paddingObject 0)
		functionLayout.BackColor = RS_dotNetPreset.controlColour_Dark
		functionLayout.autoSize = true
		dotNet.addEventHandler functionLayout "MouseEnter" dispatchFunctionLeave
		dotNet.addEventHandler functionLayout "MouseWheel" dispatchFunctionLayoutMouseWheel
		
-- 		if (doesFileExist functionBackground) then
-- 		(
-- 			functionLayout.backgroundImage = RS_dotNetObject.imageObject functionBackground
-- 			functionLayout.BackgroundImageLayout = RS_dotNetPreset.IL_Stretch
-- 		)
		
		local index = getGroupIndex lastGroup
		functionLayout.controls.addRange functionArrays[index]
		
		mainTable.Controls.Add groupLayout 0 0
		mainTable.Controls.Add functionLayout 1 0
		
		toolBox.controls.add mainTable
	),
	
	fn dispatchToolBoxResize s e =
	(
		RSL_ToolBoxOps.ToolBoxResize s e
	),
	
	fn ToolBoxResize s e =
	(
		if s.width >= s.height and lastLayoutChange != "width" then
		(
			sizeChange = true
			
			lastLayoutChange = "width"
		)
		
		if s.width < s.height and lastLayoutChange != "height" then
		(
			sizeChange = true
			
			lastLayoutChange = "height"
		)
		
		if sizeChange then
		(
			mainTable.controls.clear()
			mainTable.dispose()
			
			createMainTable()
			
			updateGroupButtons()
			
			sizeChange = false
		)
	),
	
	fn dispatchOnFormClosing s e =
	(
		RSL_ToolBoxOps.onFormClosing s e
	),
	
	fn onFormClosing s e =
	(
-- 		functionLayout.backgroundImage.dispose()
		
		writeLocalConfigINI toolBox
		
		gc()
	),
	
	fn dispatchTimerTick s e =
	(
		RSL_ToolBoxOps.timerTick s e
	),
	
	fn timerTick s e =
	(
		hoverPanel.location = RS_dotNetObject.pointObject [toolBox.location.x, toolBox.location.y - 30]
		s.enabled = false
	),

	fn createToolBox =
	(
		clearListener()
		
		local okToContinue = loadXML()
		
		if okToContinue then
		(	
			toolBox = RS_dotNetUI.maxForm "toolBox" text:"R* Tool Box V1.0" min:[94,90] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
			toolBox.SizeGripStyle = (dotNetClass "SizeGripStyle").Show
			toolBox.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
			
			dotNet.addEventHandler toolBox "Resize" dispatchToolBoxResize
			dotnet.addEventHandler toolBox "FormClosing" dispatchOnFormClosing
			
			if doesFileExist localConfigINI then
			(
				lastGroup = (getINISetting localConfigINI "Main" "LastGroup")
			)
			
			if lastGroup == "" or lastGroup == undefined then
			(
				lastGroup = scriptGroups[1][1]
			)
			
			hoverPanel = RS_dotNetUI.Panel "hoverPanel" size:[130, 20] location:[toolBox.location.x, toolBox.location.y - 30] borderStyle:RS_dotNetPreset.BS_FixedSingle
			dotNet.addEventHandler hoverPanel "MouseEnter" dispatchFunctionEnter
			
			hoverFunctionLayout = dotNetObject "FlowLayoutPanel"
			hoverFunctionLayout.BorderStyle = RS_dotNetPreset.BS_None
			hoverFunctionLayout.dock = RS_dotNetPreset.DS_Fill
			hoverFunctionLayout.Margin = (RS_dotNetObject.paddingObject 0)
			hoverFunctionLayout.BackColor = RS_dotNetPreset.controlColour_Dark
			hoverFunctionLayout.autoSize = true
			dotNet.addEventHandler hoverFunctionLayout "MouseEnter" dispatchFunctionEnter
			
			hoverPanel.Controls.Add hoverFunctionLayout
			
			toolBoxTimer = dotNetObject "Timer"
			toolBoxTimer.Interval = 1000
			dotNet.addEventHandler toolBoxTimer "Tick" dispatchTimerTick
			
			toolBox.Controls.Add hoverPanel
			
			createGroups()
			createFunctions()
			createMainTable()
			
			readLocalConfigINI toolBox
			
			toolBox.showmodeless()
			
			toolBox
		)
		else
		(
			messageBox ("RSL_ToolBox.xml not found...\n" + toolBoxXMLFile + "\nEither do a get or contact paul.truss@rockstarlondon.com if you're sure it's not in perforce.") title:"Error..."
		)
	)
)

if (RSL_ToolBoxOps != undefined and RSL_ToolBoxOps.toolBox != undefined) then
(
	RSL_ToolBoxOps.toolBox.close()
)

RSL_ToolBoxOps = RSL_ToolBox()
RSL_ToolBoxOps.createToolBox()
