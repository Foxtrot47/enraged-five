global RSL_DictionaryfilterOps

struct RSL_dictionaryFilter
(
	name,
	control,
	items = #(),
	
	fn checkFilter prop =
	(
		local result = false
		
		local property = prop.itemOf name
		
		if property != undefined then
		(
			for item in items where (item.value) do
			(
				if item.name == property.value then
				(
					result = true
					exit
				)
			)
		)
		
		result
	)
)

struct RSL_PropDictionaryFilterOps
(
	typeFilter = RSL_dictionaryFilter name:"Type" items:#(dataPair name:"Static" value:true, dataPair name:"Dynamic" value:true, \
																			dataPair name:"Fragment" value:true),
	collisionFilter = RSL_dictionaryFilter name:"Collision" items:#(dataPair name:"None" value:true, dataPair name:"Yes" value:true, \
																					dataPair name:"Multiple" value:true),
	MaterialFilter = RSL_dictionaryFilter name:"Material" items:#(dataPair name:"Rage_Shader" value:true, dataPair name:"Multimaterial" value:true, \
																					dataPair name:"Multiple_Classes" value:true, dataPair name:"Illegal_Material" value:true),
	TXDFilter = RSL_dictionaryFilter name:"TXD" items:#(dataPair name:"CHANGEME" value:true),
	EffectsFilter = RSL_dictionaryFilter name:"Effects" items:#(dataPair name:"None" value:true, dataPair name:"Yes" value:true, \
																					dataPair name:"Multiple" value:true),
	LightsFilter = RSL_dictionaryFilter name:"Lights" items:#(dataPair name:"None" value:true, dataPair name:"Yes" value:true, \
																					dataPair name:"Multiple" value:true),
	filterArray = #(typeFilter, collisionFilter, MaterialFilter, TXDFilter, EffectsFilter, LightsFilter),
	
	filterForm,
	
	
	fn isValidProp prop =
	(
		local result = false
		
		for filter in filterArray do
		(
			if (filter.checkFilter prop) then
			(
				result = true
				exit
			)
		)
		
		result
	),
	
	
	
	fn dispatchOnFilterControlChanged s e =
	(
		RSL_DictionaryfilterOps.filterControlChanged s e
	),
	
	fn filterControlChanged s e =
	(
		s.SelectedItem
		
		local filterData = s.tag.value
		
		for entry in filterData.items do
		(
			if entry.name == s.SelectedItem then
			(
				entry.value = (s.GetItemChecked s.SelectedIndex)
			)
		)
	),
	
	fn dispatchAllClick s e =
	(
		RSL_DictionaryfilterOps.allClick s e
	),
	
	fn allClick s e =
	(
		for filter in filterArray do
		(
			for i = 0 to filter.control.Items.Count - 1 do
			(
				filter.control.SetItemChecked i true
				
				filter.items[i + 1].value = true
			)
		)
	),
	
	fn dispatchNoneClick s e =
	(
		RSL_DictionaryfilterOps.noneClick s e
	),
	
	fn noneClick s e =
	(
		for filter in filterArray do
		(
			for i = 0 to filter.control.Items.Count - 1 do
			(
				filter.control.SetItemChecked i false
				
				filter.items[i + 1].value = false
			)
		)
	),
	
	fn dispatchInvertClick s e =
	(
		RSL_DictionaryfilterOps.InvertClick s e
	),
	
	fn InvertClick s e =
	(
		for filter in filterArray do
		(
			for i = 0 to filter.control.Items.Count - 1 do
			(
				local value = filter.control.GetItemChecked i
				filter.control.SetItemChecked i (not value)
				
				filter.items[i + 1].value = (not value)
			)
		)
	),
	
	fn dispatchUpdateClick s e =
	(
		RSL_DictionaryfilterOps.updateClick s e
	),
	
	fn updateClick s e =
	(
		RSL_PropDictionaryEd.enableFilters.checked = true
		RSL_PropDictionaryEd.updateDictionaryTreeView()
	),
	
	fn dispatchOnFilterFormClosing s e =
	(
		RSL_PropDictionaryEd.filterOps.onFormClosing s e
	),
	
	fn onFormClosing s e =
	(
		filterForm = undefined
	),
	
	fn createForm =
	(
		filterForm = RS_dotNetUI.maxForm "filterForm" text:"Dictionary Filters" size:[160,400] borderStyle:RS_dotNetPreset.FB_FixedToolWindow
		filterForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").CenterScreen
		dotnet.addEventHandler filterForm "FormClosing" dispatchOnFilterFormClosing
		
		mainTable = RS_dotNetUI.tableLayout "mainTable" text:"mainTable" collumns:#((dataPair type:"Percent" value:50), (dataPair type:"Percent" value:50), (dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		mainTable.BackColor = RS_dotNetPreset.controlColour_Medium		
		mainTable.RowCount = filterArray.count + 2
		
		local height = 0
		
		for i = 1 to filterArray.count do
		(
			local entry = filterArray[i]
			local size = (entry.items.count * 17) + 20
			
			if entry.items.count == 1 then
			(
				size = (entry.items.count * 17) + 24
			)
			height += size
			
			mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" size)
			
			local filterControl = RS_dotNetUI.checkedListBox "typeControl" borderStyle:RS_dotNetPreset.BS_None
			filterControl.BackColor = RS_dotNetPreset.controlColour_Medium
			filterControl.CheckOnClick = true
			filterControl.tag = dotNetMXSValue entry
			dotnet.addEventHandler filterControl "SelectedIndexChanged" dispatchOnFilterControlChanged
			
			for item in entry.items do
			(
				filterControl.items.add item.name
			)
			
			for s = 0 to filterControl.items.count - 1 do
			(
				filterControl.SetItemChecked s entry.items[s + 1].value
			)
			
			filterArray[i].control = filterControl
			
			filterGroup = RS_dotNetUI.GroupBox entry.name text:entry.name dockStyle:RS_dotNetPreset.DS_Fill 
			filterGroup.controls.add filterControl
			
			mainTable.controls.add filterGroup 0 (i - 1)
			
			mainTable.SetColumnSpan filterGroup 3
		)
		
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		
		allButton = RS_dotNetUI.Button "allButton" text:(toUpper "All") dockStyle:RS_dotNetPreset.DS_Fill --margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler allButton "click" dispatchAllClick
		
		noneButton = RS_dotNetUI.Button "noneButton" text:(toUpper "None") dockStyle:RS_dotNetPreset.DS_Fill --margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler noneButton "click" dispatchNoneClick
		
		invertButton = RS_dotNetUI.Button "invertButton" text:(toUpper "Invert") dockStyle:RS_dotNetPreset.DS_Fill --margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler invertButton "click" dispatchInvertClick
		
		updateButton = RS_dotNetUI.Button "updateButton" text:(toUpper "Update") dockStyle:RS_dotNetPreset.DS_Fill --margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler updateButton "click" dispatchUpdateClick
		
		mainTable.controls.add allButton 0 (filterArray.count)
		mainTable.controls.add noneButton 1 (filterArray.count)
		mainTable.controls.add invertButton 2 (filterArray.count)
		mainTable.controls.add updateButton 0 (filterArray.count + 1)
		
		mainTable.SetColumnSpan updateButton 3
		
		filterForm.controls.add mainTable
		
		filterForm.height = height + 72
		filterForm.showModeless()
		
		filterForm
	)
)

RSL_DictionaryfilterOps = RSL_PropDictionaryFilterOps()