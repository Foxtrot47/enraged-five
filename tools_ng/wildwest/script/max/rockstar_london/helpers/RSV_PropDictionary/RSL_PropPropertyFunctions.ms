
Struct RSL_PropPropertyFunctions
(
	idxTXD = getattrindex "Gta Object" "TXD",
	
	fn recurseGetTriangleCount object &result =
	(
		local testMesh = snapshotasmesh object
		result += testMesh.numfaces
		
		delete testMesh
		
		for child in object.children where (GetAttrClass child) == "Gta Object" do 
		(
			recurseGetTriangleCount child &result
		)
	),
	
	fn getTriangleCount object =
	(
		local result = RSL_propProperty name:"Triangles"
		local count = 0
		
		recurseGetTriangleCount object &count
		
		result.value = count
		result.data = #(count)
		
		result
	),
	
	fn recurseGetObjectVolume object &result =
	(
		local theMesh = snapshotasmesh object
		local numFaces = theMesh.numfaces 
		
		for i = 1 to numFaces do 
		( 
			local Face= getFace theMesh i 
			local vert2 = getVert theMesh Face.z 
			local vert1 = getVert theMesh Face.y 
			local vert0 = getVert theMesh Face.x 
			local dV = Dot (Cross (vert1 - vert0) (vert2 - vert0)) vert0
			result += dV 
		)
		
		delete theMesh
		
		result /= 6 
		
		for child in object.children where (GetAttrClass child) == "Gta Object" do 
		(
			recurseGetObjectVolume child &result
		)
	),
	
	fn getObjectVolume object =
	(
		local result = RSL_propProperty name:"Volume"
		local volume = 0
		
		recurseGetObjectVolume object &volume
		
		result.value = volume
		result.data = #(volume)
		
		result
	),
	
	fn getObjectMemory object mapFile =
	(
		local result = RSL_propProperty name:"Object_Memory" value:0.0 data:#(0.0)
	),
	
	fn recurseGetCollision object &col &colType =
	(
		for child in object.children where (superClassOf child) == helper and (getattrclass child) == "Gta Collision" do
		(
			append col.data child.name
			
			if col.value == "Yes" then
			(
				col.value = "Multiple"
			)
			else if col.value == "None" then col.value = "Yes"
			
			local type = getattr child (getattrindex "Gta Collision" "Coll Type")
			
			if type != undefined then
			(
				appendIfUnique colType.data type
			)
			
			recurseGetCollision child &col &colType
		)
	),
	
	fn getCollisionAndCollisionType object =
	(
		local result = #()
		result[1] = RSL_propProperty name:"Collision" value:"None" data:#()
		result[2] = RSL_propProperty name:"Collision_Type" value:"" data:#()
		
		recurseGetCollision object result[1] result[2]
		
		if result[2].data.count > 1 then
		(
			result[2].value = "Multiple"
		)
		else
		(
			result[2].value = result[2].data[1]
		)
		
		result
	),
	
	fn isUsedInObject object ID =
	(
		local tempMesh = snapshotasmesh object
		local result = false
		
		for face in tempMesh.faces do
		(
			if (getFaceMatID tempMesh face.index) == ID then
			(
				result = true
				exit
			)
		)
		
		delete tempMesh
		
		result
	),
	
	fn recurseGetMaterials object &result =
	(
		local materialClass = (classOf object.material) as string
		
		if materialClass != "Rage_Shader" and materialClass != "Multimaterial" then
		(
			result.value = "Illegal_Material"
		)
		else
		(
			if result.value == undefined then
			(
				result.value = materialClass
			)
			else
			(
				if materialClass != result.value then
				(
					result.value = "Multiple_Classes"
				)
			)
		)
		
		case (classOf object.material) of
		(
			default:
			(
				if (classOf object.material) != UndefinedClass then
				(
					appendIfUnique result.data object.material.name
				)
				else
				(
					appendIfUnique result.data "UndefinedClass"
				)
			)
			Multimaterial:
			(
				for i = 1 to object.material.materialList.count do
				(
					local subMaterial = object.material.materialList[i]
					
					if subMaterial != undefined then
					(
						if (isUsedInObject object object.material.materialIDList[i]) then
						(
							appendIfUnique result.data subMaterial.name
						)
					)
				)
			)
		)
		
		for child in object.children where (GetAttrClass child) == "Gta Object" do
		(
			recurseGetMaterials child &result
		)
	),
	
	fn getMaterials object =
	(
		local result = RSL_propProperty name:"Material" value:undefined data:#()
		
		recurseGetMaterials object &result
		
		result
	),
	
	fn recurseMaterialForTextures material object &result =
	(
		case (classOf material) of
		(
			default:
			(
			)
			StandardMaterial:
			(
				local subTextureMap = getSubTexmap material 1
				local isAlpha = false
				
				if subTextureMap == undefined then
				(
					isAlpha = false
					subTextureMap = getSubTexmap material 2
				)
				
				if (classOf subTextureMap) == Bitmaptexture then
				(
					if subTextureMap.filename != "" then
					(
						appendIfUnique result.data subTextureMap.filename
						
						if isAlpha then
						(
							local alphaTextureMap = getSubTexmap material 7
							
							if (classof alphaTextureMap) == Bitmaptexture and alphaTextureMap.filename != "" then
							(
								appendIfUnique result.data alphaTextureMap.filename
							)
						)
					)
				)
			)
			Rage_Shader:
			(
				local textureCount = getNumSubTexmaps material
				local isAlpha = false
				
				if (RstGetIsAlphaShader material) then
				(
					textureCount /= 2
					isAlpha = true
				)
				
				for i = 1 to textureCount do
				(
					local subTextureMap = getSubTexmap material i
					
					case (classof subTextureMap) of
					(
						Bitmaptexture:
						(
							if subTextureMap.filename != "" then
							(
								appendIfUnique result.data subTextureMap.filename
							)
							
							if isAlpha then
							(
								local alphaTextureMap = getSubTexmap material (i + textureCount)
								
								if (classof alphaTextureMap) == Bitmaptexture and alphaTextureMap.filename != "" then
								(
									appendIfUnique result.data alphaTextureMap.filename
								)
							)
						)
						CompositeTexturemap:
						(
							local compositeCount = getNumSubTexmaps subTextureMap
							
							for c = 1 to compositeCount do
							(
								local compTextureMap = getSubTexmap subTextureMap c
								
								if (classof compTextureMap) == Bitmaptexture and compTextureMap.filename != "" then
								(
									appendIfUnique result.data compTextureMap.filename
								)
							)
						)
					)
				)
			)
			DirectX_9_Shader:
			(
				if material.renderMaterial != undefined then
				(
					recurseMaterialForTextures material.renderMaterial object &textureArray
				)
			)
			multimaterial:
			(
				for i = 1 to material.materialList.count do
				(
					local subMaterial = material.materialList[i]
					
					if subMaterial != undefined then
					(
						if (isUsedInObject object material.materialIDList[i]) then
						(
							recurseMaterialForTextures subMaterial object &result
						)
					)
				)
			)
			XRef_Material:
			(
				local xrefSource = material.GetSrcItem()
				
				if xrefSource != undefined then
				(
					recurseMaterialForTextures xrefSource object &result
				)
			)
		)
	),
	
	fn recurseGetTextures object &result =
	(
		recurseMaterialForTextures object.material object &result
		
		for child in object.children where (GetAttrClass child) == "Gta Object" do
		(
			recurseGetTextures child &result
		)
	),
	
	fn getTextures object =
	(
		local result = RSL_propProperty name:"Textures" value:undefined data:#()
		
		recurseGetTextures object &result
		result.value = result.data.count
		
		result
	),
	
	fn recurseGetTXD object &result =
	(
		local TXD = getattr object idxTXD
		
		if result.value == undefined then
		(
			result.value = TXD
			append result.data TXD
		)
		else
		(
			if (toLower TXD) != (toLower result.value) then
			(
				result.value = "Multiple TXDs"
				appendIfUnique result.data TXD
			)
		)
		
		for child in object.children where (GetAttrClass child) == "Gta Object" do
		(
			recurseGetTXD child &result
		)
	),
	
	fn getTXD object =
	(
		local result = RSL_propProperty name:"TXD" value:undefined data:#()
		
		recurseGetTXD object &result
		
		result
	),
	
	fn getTXDMemory object mapFile =
	(
		local result = RSL_propProperty name:"TXD_Memory" value:0.0 data:#(0.0)
	),
	
	fn recurseGetEffects object &result =
	(
		for child in object.children do
		(
			if (classOf child) == RAGE_Particle then
			(
				if result.value == "None" then
				(
					result.value = "Yes"
				)
				else
				(
					if result.value == "Yes" then
					(
						result.value = "Multiple"
					)
				)
				
				appendIfUnique result.data child.name
			)
			
			recurseGetEffects child &result
		)
	),
	
	fn getEffects object =
	(
		local result = RSL_propProperty name:"Effects" value:"None" data:#()
		
		recurseGetEffects object &result 
		
		result
	),
	
	fn recurseGetLights object &result =
	(
		for child in object.children do
		(
			if (superClassOf child) == light then
			(
				if result.value == "None" then
				(
					result.value = "Yes"
				)
				else
				(
					if result.value == "Yes" then
					(
						result.value = "Multiple"
					)
				)
				
				appendIfUnique result.data child.name
			)
			
			recurseGetLights child &result
		)
	),
	
	fn getLights object =
	(
		local result = RSL_propProperty name:"Lights" value:"None" data:#()
		
		recurseGetLights object &result 
		
		result
	)
	
	
	
)
