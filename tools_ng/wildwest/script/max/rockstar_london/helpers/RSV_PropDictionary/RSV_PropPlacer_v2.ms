filein (RsConfigGetWildWestDir() + "script/max/Rockstar_London/pipeline/RSL_LevelConfig.ms")
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSV_PropDictionary/RSL_PropDictionaryOps_v2.ms")

global RSL_PropPlaceForm

struct RSL_PropPlacer
(
	thumbnailPath = (project.art + "\\localDB\\Thumbs_props\\"),
	loadLogo = (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\images\\RS_Logo.jpg"),
	
	dictionaryOps = RSL_propDictionaryOps(),
	
	dictionaryArray = #(),
	maxFiles = #(),
	masterPropArray = #(),
	currentPropArray = #(),
	thumbnailArray = #(),
	cacheArray = #(),
	firstItem = 0,
	
	currentListView,
	selectedProp,
	CurrentXref,
	currentSnapMode,
	HideHelpers,
	
	defaultLayer = (LayerManager.getLayer 0),
	
	mousePressed = 0,
	newCopy = undefined,
	currentPos = undefined,
	directionVector = undefined,
	newHeading = undefined, 
	
	propPlacerForm,
	
	mainTabControl,
	listTab,
	favouritesTab,
	
	listViewRightClick,
	addToFavourites,
	addAllToFavourites,
	removeFromFavourites,
	thumbnailSize,
	thumbnailLarge,
	thumbnailSmall,
	
	propListView,
	
	seachTextBox,
	maxFileCombo,
	
	hideCollisionCheckBox,
	placeSelectedButton,
	
	replaceAllButton,
	replaceSelectedButton,
	selectSimilarButton,
	selectFromListButton,
	testSceneButton,
	
	infoButton,
	
	fn initialiseListView =
	(
		dictionaryArray = dictionaryOps.loadLevelXmlDictionaries levelconfig.level
		masterPropArray = #()
		currentPropArray = #()
		maxFiles = #("All")
		currentListView = propListView
		
		for dictionary in dictionaryArray do
		(
			masterPropArray += dictionary.propArray
			
			appendIfUnique maxFiles (filenameFromPath dictionary.maxFile)
		)
		
		currentPropArray = masterPropArray
		
		propListView.virtualListSize = currentPropArray.count
	),
	
	fn indexOfThumbnail file =
	(
		local result = 0
		
		for entry in thumbnailArray do 
		(
			if entry.file == file then
			(
				result = entry.index
				exit
			)
		)
		
		result
	),
	
	fn imageFromThumbnail file =
	(
		local imageClass = dotNetClass "System.Drawing.Image"
		local temp = imageClass.FromFile file
		
		image = dotNetObject "System.Drawing.Bitmap" temp
		
		temp.Dispose()
		temp = undefined
		
		image
	),
	
	fn newListViewItem index =
	(
		local thumbIndex = indexOfThumbnail currentPropArray[index].thumbnail
		
		if thumbIndex == 0 then
		(
			propListView.LargeImageList.images.Add (imageFromThumbnail currentPropArray[index].thumbnail)
			propListView.SmallImageList = propListView.LargeImageList
			
			append thumbnailArray (dataPair file:currentPropArray[index].thumbnail index:propListView.LargeImageList.images.count)
			
			thumbIndex = propListView.LargeImageList.images.count
			
-- 			print propListView.LargeImageList.images.count
		)
		
		local newItem = dotNetObject "listViewItem" currentPropArray[index].name
		newItem.tag = dotNetMXSValue currentPropArray[index]
		newItem.imageIndex = thumbIndex - 1
		newItem
	),
	
	fn checkFilters prop =
	(
		local result = true
		
		if seachTextBox.text != "" then
		(
			local matchPartial = matchPattern prop.name pattern:("*" + seachTextBox.text + "*")
			local matchWhole = matchPattern prop.name pattern:(seachTextBox.text)
			
			result = matchPartial or matchWhole
		)
		
		case maxFileCombo.Text of
		(
			default:
			(
				result = (toLower (filenameFromPath prop.maxFile)) == (toLower maxFileCombo.Text)
			)
			"All":()
		)
		
		result
	),
	
	fn filterCurrentPropArray =
	(
		currentPropArray = #()
		
		for prop in masterPropArray do 
		(
			if (checkFilters prop) then
			(
				append currentPropArray prop
			)
		)
	),
	
	fn SnapModeOn = 
	(
		SnapSetting = snapMode.active
		if SnapSetting == false then
		(
			max snap toggle
		)
		
		if hideCollisionCheckBox.checked then
		(
			HideHelpers = hideByCategory.helpers
			hideByCategory.helpers = true
		)
		
		SnapSetting
	),
	
	fn ResetSnapMode SnapSetting = 
	(
		local CurrentSnapSetting = snapMode.active
		
		if SnapSetting != CurrentSnapSetting then
		(
			max snap toggle
		)
		
		if hideCollisionCheckBox.checked then
		(
			if HideHelpers != (hideByCategory.helpers) then
			(
				hideByCategory.helpers = false		
			)
		)
	),
	
	fn createProp position:undefined =
	(
		local result = false
		
		if selectedProp != undefined then
		(
			if (toLower (getFilenameFile selectedProp.maxFile)) != maxfilename then
			(
				if (doesFileExist selectedProp.maxFile) then
				(
					local objectNames = getMAXFileObjectNames selectedProp.maxFile quiet:true
					
					if (findItem objectNames selectedProp.name) != 0 then
					(
						objXRefMgr.dupMtlNameAction = #useXRefed
						local xrefRec = objXRefMgr.AddXRefItemsFromFile selectedProp.maxFile objNames:#(selectedProp.name)
						
						local xrefItem = (xrefRec.GetItem 1 #XRefObjectType)
						local XrefArray
						xrefItem.GetNodes &XrefArray
						xref = XrefArray[1]
						
						if xref != undefined then
						(
							xref.updateMaterial = true
							xref.xrefrecord.autoUpdate = true
							xref.isHidden = false
							xref.isFrozen = false
							
							select xref
							
							CurrentXref = xref
							currentSnapMode = SnapModeOn()
							
							result = true
						)
					)
					else
					(
						messageBox ("The object: " + selectedProp.name + " does not exist in the max file.\n" + selectedProp.maxFile) title:"Error..."
					)
				)
				else
				(
					messageBox ("The max file for the prop does not exist on your machine.\n" + selectedProp.maxFile) title:"Error..."
				)
			)
			else
			(
				messageBox "Cannot X-Ref a prop into the max file it exists in." title:"Error..."
			)
		)
		else
		(
			messageBox "A prop has not been selected." title:"Error..."
		)
		
		result
	),
	
	fn cb_PlacementCallback msg ir obj faceNum shift ctrl alt = 
	(
		case msg of 
		(
			#freeMove:
			(
				CurrentXref.pos = ir.pos
				#continue
			)
			#mousePoint: 
			(
				if mousePressed == 0 then
				(
					newHeading = transMatrix ir.pos
				)
				
				newCopy = copy CurrentXref
				defaultLayer.addNode newCopy
				
				newCopy.transform = newHeading
				
				mousePressed = 0
				
				#continue
			)
			#mouseMove:
			(
				mousePressed += 1
				if mousePressed == 1 then
				(
					currentPos = ir.pos
					CurrentXref.pos = currentPos
				)
				
				directionVector = (currentPos - ir.pos)
				directionVector.z = 0.0
				directionVector = normalize directionVector
				
				newHeading = transMatrix currentPos
				
				newHeading.row2 = directionVector
				newHeading.row3 = [0,0,1]
				newHeading.row1 = cross newHeading.row2 newHeading.row3 
				
				CurrentXref.transform = newHeading
				#continue
			)
			#mouseAbort:
			(
				delete CurrentXref
				ResetSnapMode CurrentSnapMode
				CurrentXref = undefined
				
				propPlacerForm.Enabled = true
			)
		)
		
	),	
	
	
	
	
	
	
	
	
	
	fn dispatchOnTabSelected s e =
	(
		RSL_PropPlaceForm.tabSelected s e
	),
	
	fn tabSelected s e =
	(
		if mainTabControl.SelectedTab == listTab then
		(
			currentListView = propListView
		)
		else
		(
			currentListView = favouritesListView
		)
	),
	
	fn dispatchOnListViewRightClickOpening s e =
	(
		RSL_PropPlaceForm.listViewRightClickOpening s e
	),
	
	fn listViewRightClickOpening s e =
	(
		if mainTabControl.SelectedTab == listTab then
		(
			addToFavourites.enabled = true
			addAllToFavourites.enabled = true
			removeFromFavourites.enabled = false
		)
		else
		(
			addToFavourites.enabled = false
			addAllToFavourites.enabled = false
			removeFromFavourites.enabled = true
		)
	),
	
	fn dispatchThumbnailSizeClick s e =
	(
		RSL_PropPlaceForm.thumbnailSizeClick s e
	),
	
	fn thumbnailSizeClick s e =
	(
		currentListView.LargeImageList.images.clear()
		gc()
		currentListView.LargeImageList.imagesize = s.tag.value
		currentListView.invalidate()
		
		if s.name == "thumbnailLarge" then
		(
			thumbnailLarge.checked = true
			thumbnailSmall.checked = false
		)
		else
		(
			thumbnailLarge.checked = false
			thumbnailSmall.checked = true
		)
	),
	
	fn dispatchCreateVirtualItem s e =
	(
		RSL_PropPlaceForm.createVirtualItem s e
	),
	
	fn createVirtualItem s e =
	(
		if cacheArray.count != 0 and e.ItemIndex >= firstItem and e.itemIndex < (firstItem + cacheArray.count) then
		(
			local index = e.ItemIndex - firstItem + 1
			e.Item = cacheArray[index]
		)
		else
		(
			e.item = newListViewItem (e.itemIndex + 1)
		)
	),
	
	fn dispatchCacheVirtualItems s e =
	(
		RSL_PropPlaceForm.cacheVirtualItems s e
	),
	
	fn cacheVirtualItems s e =
	(
		if cacheArray.count != 0 and e.startIndex >= firstItem and e.endIndex <= (firstItem + cacheArray.count) then
		(
			
		)
		else
		(
			propListView.LargeImageList.images.clear() --= imageList
			gc()
			
			firstItem = e.startIndex
			cacheArray = #()
			cacheArray.count = e.EndIndex - e.StartIndex + 1
			
			thumbnailArray = #()
			
			for i = 1 to cacheArray.count do 
			(
				local index = (i + firstItem)
				
				cacheArray[i] = newListViewItem index
			)
			
-- 			print heapfree
		)
	),
	
	fn dispatchOnListViewClicked s e =
	(
		RSL_PropPlaceForm.listViewClicked s e
	),
	
	fn listViewClicked s e =
	(
		if s.SelectedIndices.count == 1 then
		(
			local index = s.SelectedIndices.Item[0]
			
			selectedProp = currentPropArray[index + 1]
		)
	),
	
	fn dispatchFilterChanged s e =
	(
		RSL_PropPlaceForm.filterChanged s e
	),
	
	fn filterChanged s e =
	(
		filterCurrentPropArray()
		propListView.virtualListSize = currentPropArray.count
		propListView.invalidate()
	),
	
	fn dispatchPlaceSelectedClick s e =
	(
		RSL_PropPlaceForm.placeSelectedClick s e
	),
	
	fn placeSelectedClick s e =
	(
		print "Placing..."
-- 		s.BackColor = warningColour
-- 		s.FlatStyle = FlatStyle.Flat
		
		propPlacerForm.Enabled = false
		
		local okToContinue = createProp()
		
		if okToContinue then
		(
			mouseTrack snap:#3D trackCallBack:cb_PlacementCallback
		)
		else
		(
-- 			placeButton.BackColor = controlColour
-- 			placeButton.FlatStyle = FlatStyle.System
			
			propPlacerForm.Enabled = true
		)
	),
	
	fn createForm =
	(
		propPlacerForm = RS_dotNetUI.maxForm "propPlacerForm" text:"R* Prop Placer V2.0" size:[364,756] min:[256,256] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
-- 		propPlacerForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		
		addToFavourites = RS_dotNetUI.ToolStripMenuItem "addToFavourites" text:"Add to Favourites" 
-- 		dotnet.addEventHandler addToFavourites "Click" dispatchOnAddRemoveClicked
		
		addAllToFavourites = RS_dotNetUI.ToolStripMenuItem "addAllToFavourites" text:"Add All to Favourites" 
-- 		dotnet.addEventHandler addAllToFavourites "Click" dispatchOnAddRemoveClicked
		
		removeFromFavourites = RS_dotNetUI.ToolStripMenuItem "removeFromFavourites" text:"Remove from Favourites" 
-- 		dotnet.addEventHandler removeFromFavourites "Click" dispatchOnAddRemoveClicked
		
		thumbnailLarge = RS_dotNetUI.ToolStripMenuItem "thumbnailLarge" text:"Large"
		thumbnailLarge.tag = dotNetMXSValue (dotNetObject "System.Drawing.Size" 256 160)
		thumbnailLarge.checked = true
		dotnet.addEventHandler thumbnailLarge "Click" dispatchThumbnailSizeClick
		
		thumbnailSmall = RS_dotNetUI.ToolStripMenuItem "thumbnailSmall" text:"Small"
		thumbnailSmall.tag = dotNetMXSValue (dotNetObject "System.Drawing.Size" 128 80)
		dotnet.addEventHandler thumbnailSmall "Click" dispatchThumbnailSizeClick
		
		thumbnailSize = RS_dotNetUI.ToolStripMenuItem "thumbnailSize" text:"Thumbnail Size" 
		thumbnailSize.DropDownItems.AddRange #(thumbnailLarge, thumbnailSmall)
		
		listViewRightClick = RS_dotNetUI.ContextMenuStrip "listViewRightClick" items:#(addToFavourites, addAllToFavourites, removeFromFavourites, RS_dotNetUI.separator(), thumbnailSize)
		dotnet.addEventHandler listViewRightClick "Opening" dispatchOnListViewRightClickOpening
		
		propListView = RS_dotNetUI.listView "propListView" dockStyle:RS_dotNetPreset.DS_Fill
		propListView.view = (dotNetClass "System.Windows.Forms.View").LargeIcon --Details
		propListView.Sorting = (dotNetClass "System.Windows.Forms.SortOrder").Ascending
		propListView.HideSelection = false
		propListView.scrollable = true
		propListView.VirtualMode = true
		propListView.columns.add "Props" 128
		propListView.BackColor = ((dotNetClass"System.Drawing.Color").fromARGB 197 197 197) --RS_dotNetPreset.controlColour_Medium
		propListView.ContextMenuStrip = listViewRightClick
		
		local cDepth = dotNetClass "ColorDepth"
		local imageList = dotNetObject "System.Windows.Forms.ImageList"
		local largeImageSize = dotNetObject "System.Drawing.Size" 256 160 --128 64
		
		imageList.imagesize = largeImageSize
		imageList.ColorDepth = cDepth.Depth32Bit
		
		propListView.LargeImageList = imageList
		
		dotNet.addEventHandler propListView "RetrieveVirtualItem" dispatchCreateVirtualItem
		dotNet.addEventHandler propListView "CacheVirtualItems" dispatchCacheVirtualItems
		dotnet.addEventHandler propListView "Click" dispatchOnListViewClicked
		
		listTab = RS_dotNetUI.tabPage "listTab" text:"Props" control:sceneTreeView
		listTab.controls.add propListView
		
		favouritesListView = RS_dotNetUI.listView "favouritesListView" dockStyle:RS_dotNetPreset.DS_Fill
		favouritesListView.view = (dotNetClass "System.Windows.Forms.View").LargeIcon --Details
		favouritesListView.Sorting = (dotNetClass "System.Windows.Forms.SortOrder").Ascending
		favouritesListView.HideSelection = false
		favouritesListView.scrollable = true
		favouritesListView.VirtualMode = true
		favouritesListView.columns.add "Props" 128
		favouritesListView.BackColor = RS_dotNetPreset.controlColour_Medium
		favouritesListView.ContextMenuStrip = listViewRightClick
		
		local cDepth = dotNetClass "ColorDepth"
		local imageList = dotNetObject "System.Windows.Forms.ImageList"
		local largeImageSize = dotNetObject "System.Drawing.Size" 256 128 --128 64
		
		imageList.imagesize = largeImageSize
		imageList.ColorDepth = cDepth.Depth24Bit --cDepth.Depth32Bit
		
		favouritesListView.LargeImageList = imageList
		
-- 		dotNet.addEventHandler favouritesListView "RetrieveVirtualItem" dispatchFavCreateVirtualItem
-- 		dotNet.addEventHandler favouritesListView "CacheVirtualItems" dispatchFavCacheVirtualItems
-- 		dotnet.addEventHandler favouritesListView "Click" dispatchOnFavListViewClicked
		
		favouritesTab = RS_dotNetUI.tabPage "favouritesTab" text:"Favourites" control:sceneTreeView
		favouritesTab.controls.add favouritesListView
		
		mainTabControl = RS_dotNetUI.tabControl "mainTabControl" controls:#(listTab, favouritesTab) dockStyle:RS_dotNetPreset.DS_Fill
		dotnet.addEventHandler mainTabControl "Selected" dispatchOnTabSelected
		
		searchLabel = RS_dotNetUI.Label "searchLabel" text:("Search For...") textAlign:RS_dotNetPreset.CA_MiddleCenter borderStyle:RS_dotNetPreset.BS_None \
													dockStyle:RS_dotNetPreset.DS_Fill 	
		searchLabel.font = dotNetObject "System.Drawing.Font" "Trebuchet MS" 8 RS_dotNetClass.fontStyleClass.Bold
		
		seachTextBox = RS_dotNetUI.textBox "seachTextBox" borderStyle:RS_dotNetPreset.BS_Fixed3D dockStyle:RS_dotNetPreset.DS_Fill
		seachTextBox.margin = (RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler seachTextBox "TextChanged" dispatchFilterChanged
		
		maxFileLabel = RS_dotNetUI.Label "maxFileLabel" text:("Max File...") textAlign:RS_dotNetPreset.CA_MiddleCenter borderStyle:RS_dotNetPreset.BS_None \
													dockStyle:RS_dotNetPreset.DS_Fill 	
		maxFileLabel.font = dotNetObject "System.Drawing.Font" "Trebuchet MS" 8 RS_dotNetClass.fontStyleClass.Bold
		
		maxFileCombo = RS_dotNetUI.ToolStripCombo "maxFileCombo" text:"All"
		maxFileCombo.FlatStyle = RS_dotNetPreset.FS_System
		dotnet.addEventHandler maxFileCombo "SelectedIndexChanged" dispatchFilterChanged
		
		maxFileTooStrip = RS_dotNetUI.ToolStrip "maxFileTooStrip" dockStyle:RS_dotNetPreset.DS_Fill
		maxFileTooStrip.BackColor = RS_dotNetPreset.controlColour_Medium
		maxFileTooStrip.GripStyle = (dotNetClass "System.Windows.Forms.ToolStripGripStyle").Hidden
		maxFileTooStrip.RenderMode = (dotNetClass "System.Windows.Forms.ToolStripRenderMode").System --Professional
		maxFileTooStrip.Items.AddRange #(MaxFileCombo)
		
		hideCollisionCheckBox = RS_dotNetUI.checkBox "bestAlignButton" text:("Hide Collision") textAlign:RS_dotNetPreset.CA_MiddleCenter \
																	checkAlign:RS_dotNetPreset.CA_MiddleRight dockStyle:RS_dotNetPreset.DS_Fill
		hideCollisionCheckBox.checked = true
		hideCollisionCheckBox.font = dotNetObject "System.Drawing.Font" "Trebuchet MS" 8 RS_dotNetClass.fontStyleClass.Bold
		
		placeSelectedButton = RS_dotNetUI.Button "placeSelectedButton" text:(toUpper "Place Selected") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler placeSelectedButton "click" dispatchPlaceSelectedClick
		
		placementTable = RS_dotNetUI.tableLayout "placementTable" text:"placementTable" collumns:#((dataPair type:"Percent" value:50), (dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		placementTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		placementTable.controls.add hideCollisionCheckBox 0 0
		placementTable.controls.add placeSelectedButton 1 0
		
		placementGroup = RS_dotNetUI.GroupBox "placementGroup" text:"Placement" dockStyle:RS_dotNetPreset.DS_Fill 
		placementGroup.controls.add placementTable
		
		replaceAllButton = RS_dotNetUI.Button "replaceAllButton" text:(toUpper "Replace All") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
-- 		dotnet.addEventHandler replaceAllButton "click" dispatchReplaceAllClick
		
		replaceSelectedButton = RS_dotNetUI.Button "replaceSelectedButton" text:(toUpper "Replace Selected") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
-- 		dotnet.addEventHandler replaceSelectedButton "click" dispatchReplaceSelectedClick
		
		selectSimilarButton = RS_dotNetUI.Button "selectSimilarButton" text:(toUpper "Select Similar") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
-- 		dotnet.addEventHandler selectSimilarButton "click" dispatchSelectSimilarClick
		
		selectFromListButton = RS_dotNetUI.Button "placeSelectedButton" text:(toUpper "Select Similar From List") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
-- 		dotnet.addEventHandler selectFromListButton "click" dispatchSelectFromListClick
		
		testSceneButton = RS_dotNetUI.Button "testSceneButton" text:(toUpper "Test Scene") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
-- 		dotnet.addEventHandler testSceneButton "click" dispatchTestSceneClick
		
		conversionTable = RS_dotNetUI.tableLayout "conversionTable" text:"conversionTable" collumns:#((dataPair type:"Percent" value:50), (dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		conversionTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		conversionTable.controls.add replaceAllButton 0 0
		conversionTable.controls.add replaceSelectedButton 1 0
		conversionTable.controls.add selectSimilarButton 0 1
		conversionTable.controls.add selectFromListButton 1 1
		conversionTable.controls.add testSceneButton 0 2
		
		conversionTable.SetColumnSpan testSceneButton 2
		
		conversionGroup = RS_dotNetUI.GroupBox "conversionGroup" text:"Conversion Tools" dockStyle:RS_dotNetPreset.DS_Fill 
		conversionGroup.controls.add conversionTable
		
		infoButton = RS_dotNetUI.Button "infoButton" text:(toUpper "Information Panel") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
-- 		dotnet.addEventHandler infoButton "click" dispatchInfoClick
		
		mainTable = RS_dotNetUI.tableLayout "mainTable" text:"mainTable" collumns:#((dataPair type:"Percent" value:50), (dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		mainTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		mainTable.RowCount = 6
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 50)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 56)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 106)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		
		mainTable.controls.add mainTabControl 0 0
		mainTable.controls.add searchLabel 0 1
		mainTable.controls.add seachTextBox 1 1
		mainTable.controls.add maxFileLabel 0 2
		mainTable.controls.add maxFileTooStrip 1 2
		mainTable.controls.add placementGroup 0 3
		mainTable.controls.add conversionGroup 0 4
		mainTable.controls.add infoButton 0 5
		
		mainTable.SetColumnSpan mainTabControl 2
		mainTable.SetColumnSpan placementGroup 2
		mainTable.SetColumnSpan conversionGroup 2
		mainTable.SetColumnSpan infoButton 2
		
		propPlacerForm.controls.add mainTable
		
		if levelconfig.level != undefined then
		(
			loadForm = RS_dotNetUI.maxForm "loadForm" text:"Loading..." size:[160,160] borderStyle:RS_dotNetPreset.FB_FixedToolWindow
			loadForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").CenterScreen
			loadForm.BackgroundImageLayout = (dotNetClass "System.Windows.Forms.ImageLayout").Stretch
			loadForm.BackgroundImage = (RS_dotNetObject.imageObject loadLogo)
			
			loadForm.showModeless()
			loadForm.refresh()
			
			initialiseListView()
			maxFileCombo.items.AddRange maxFiles
			
			loadForm.Close()
			
			propPlacerForm.showModeless()
		)
		else
		(
			messageBox "This is not a valid map file. Make sure it's saved in the proper directory structure." title:"Error..."
		)
	)
)

if RSL_PropPlaceForm != undefined then
(
	RSL_PropPlaceForm.propPlacerForm.close()
)

RSL_PropPlaceForm = RSL_PropPlacer()
RSL_PropPlaceForm.createForm()