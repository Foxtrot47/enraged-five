--filein (RsConfigGetWildWestDir() + "script/max/Rockstar_London/pipeline/RSL_p4.ms")	-- load in p4class/struct
filein "pipeline/util/p4.ms"
clearlistener()

try (destroyDialog Rollout_PropDictPlacer) catch ()			-- Added this so if Prop placer or disotnay toool is running which will use the thumbsnails it will close them
if init_PropDictionaryEditor_Form != undefined then			-- Will close the Dictionary editor if its running
(
	init_PropDictionaryEditor_Form.Close()
)

if init_propPlacer_form != undefined then							-- Will close the Dot net Prop Placer if running
(
	init_propPlacer_form.Close()
)


gc()

fn GetLevelNameProp mode =
(
	local upperFilePath = toupper maxFilePath;
	local filePathArray = filterString upperFilePath "\\"
	local result = undefined

	if filePathArray.count == 0 then
	(
		result = "unsaved";
	)
		
	if mode == "lvl" then
	(
		local mapIndex = findItem filePathArray "MAPS";
		if mapIndex != 0 then
		(
			LevelName = filePathArray[mapIndex + 1]
			result = tolower (levelName);
		)
	)
	
	if mode == "prop" then
	(
		result = tolower (filePathArray[(filePathArray.count)])
	)
	result
)

fn selectchildren obj =
(	
	for child in obj.children do
	(
		selectmore child
		child.isHidden = false
		if child.children.count  > 0 then
		(
			selectchildren child
		)
	)

)

fn collectRageShaderAttributes rageShader = -- collects the variable names and values from the rage shader
(
	local outArray = #(#(),#(),#())
	if (classOf rageShader) == Rage_Shader then
	(
		local count = RstGetVariableCount rageShader
		local isAlphaShader = RstGetIsAlphaShader rageShader
		
		append outArray[1] "Name"
		append outArray[2] "string"
		append outArray[3]  rageShader.name
		
		append outArray[1] "Shader"
		append outArray[2] "shaderList"
		append outArray[3]  (RstGetShaderName rageShader)
		
		local textureCount = 0
		try(for i = 1 to count where (matchPattern (RstGetVariableType rageShader i) pattern:"Texmap") do textureCount += 1)catch()
		
		for i = 1 to count do
		(
			local name = (RstGetVariableName rageShader i)
			local variableType = (RstGetVariableType rageShader i)
			local variable = (RstGetVariable rageShader i)
			
			if (classOf variable) == float and variable < 0.01 then variable = 0.0
			
			append outArray[1] name
			append outArray[2] variableType
			append outArray[3] variable
			
			if name == "Diffuse Texture" and isAlphaShader == true then
			(
				local alphaTexture
				local subTexture = (getSubTexmap rageShader (textureCount + i))
				
				if subTexture != undefined then
				(
					alphaTexture = (getSubTexmap rageShader (textureCount + i)).filename
				)
				else
				(
					alphaTexture = ""
				)
				
				append outArray[1] "Alpha Texture"
				append outArray[2] "texmap"
				append outArray[3] alphaTexture
			)
		)
	)
	outArray
)

fn convertSpecularValues intensity fallOff = -- converts the glossiness and specular level values
(
	local outPoint2 = [0,0]
	
	outPoint2.x = (fallOff / 2000) * 100
	outPoint2.y = 100.0 * intensity
	outPoint2
)

fn createStandardMaterial rageShaderProperties =
(
	local material = StandardMaterial()
	
	for i = 1 to rageShaderProperties[1].count do
	(
		case (rageShaderProperties[1][i]) of
		(
			"Name":
			(
				material.name = rageShaderProperties[3][i]
			)
			"Diffuse Texture":
			(
				local diffuseAndSpecularColour = bitmaptexture filename:rageShaderProperties[3][i]
				
				material.diffuseMap = diffuseAndSpecularColour
				material.specularMap = diffuseAndSpecularColour
			)
			"Alpha Texture":
			(
				local opacityMap = bitmaptexture filename:rageShaderProperties[3][i]
				
				if opacityMap.filename != "" then
				(
					material.opacityMap = opacityMap
				)
			)
			"Bump Texture":
			(
				local normal = Normal_Bump normal_map:(bitmaptexture filename:rageShaderProperties[3][i])
				
				material.bumpMap = normal
				material.bumpMapAmount = 100
			)
			"Specular Texture":
			(
				local specularLevelAndGlossiness = bitmaptexture filename:rageShaderProperties[3][i]
				
				material.specularLevelMap = specularLevelAndGlossiness
				material.glossinessMap = specularLevelAndGlossiness
			)
			"specular map intensity mask color":
			(
				material.Specular = (rageShaderProperties[3][i] * 255) as color
			)
			"Specular Falloff":
			(
				material.specularLevel = (convertSpecularValues (rageShaderProperties[3][i] as float) (rageShaderProperties[3][i] as float)).x
			)
			"Specular Intensity":
			(
				material.glossiness = (convertSpecularValues (rageShaderProperties[3][i] as float) (rageShaderProperties[3][i] as float)).y
			)
			"Environment Texture":
			(
				local reflectionMap = bitmaptexture filename:rageShaderProperties[3][i]
				
				material.reflectionMap = reflectionMap
			)
			"Reflectivity":
			(
				material.reflectionMapAmount = (rageShaderProperties[3][i] as float)
			)
			
			default:
			(
-- 				format "Name: % Type: % Variable: %\n" rageShaderProperties[1][i] rageShaderProperties[2][i] rageShaderProperties[3][i]
			)
		)
	)
	material
)

fn processMaterial material =
(
	local newMaterial = material
	case (classOf material) of
	(
		rage_shader:
		(
			local shaderProps = collectRageShaderAttributes material
			newMaterial = (createStandardMaterial shaderProps)
			
		)
		MultiMaterial:
		(
			local newMulti = multimaterial numSubs:(material.materialList.count)
			newMulti.name = material.name + "_StandardConverted"
			
			for i = 1 to material.MaterialList.count do
			(
				
				local subMaterial = material.MaterialList[i]
				
				if (classOf submaterial) ==  rage_shader then
				(
					local shaderProps = collectRageShaderAttributes submaterial
					
					newMulti.MaterialList[i] = (createStandardMaterial shaderProps)
				)
				else
				(
					newMulti.MaterialList[i] = submaterial
				)
				newMulti.names[i] = material.names[i]
				newMulti.MaterialIDList[i] = material.MaterialIDList[i]
			)
			
			newMaterial = newMulti
		)
	)
	newMaterial
)

mapped fn switchLights light =
(
	light.enabled = not light.enabled 
)

fn gatherCurrent object &array =
(
	object.isHidden = false
	append array object
	
	for child in object.children where (getAttrClass child) == "Gta Object" do
	(
		gatherCurrent child &array
	)
)

fn RenderThumbsScene = 
(

	viewport.ResetAllViews()
	if viewport.isWire() then max wire smooth

	hideByCategory.helpers = true

	local ProcessedNew = 0	
	local ProcessedRerenderd = 0
	
	local currentRenderer = renderers.current
	renderers.current = Default_Scanline_Renderer()
	scanlineRender.antiAliasFilter = Catmull_Rom()

	local back_Ground = backgroundColor
	local ambient = ambientColor
	
	backgroundColor = color 197 197 197
	ambientColor = color 120 120 120
	
-- 	max default lighting toggle
	
	switchLights lights
	
	local mainLight = omniLight pos:[0,0,0]
	mainLight.name = "renderMain"
	mainLight.multiplier = 1.15
	
	local backLight = omniLight pos:[0,0,0]
	backLight.name = "renderBack"
	backLight.multiplier = 0.5
	
	local regEx = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\w+)(_frag_)")
	local regExFragTemp = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\w+)(_fragtemp_)")
	local regExLow = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\w+)(_L$)")
		
	local list = for object in objects where (superclassOf Object) == light collect object
	
	rollout pbRollout "Replacing Objects..." width:402 height:41
	(
		button but_cancel "Cancel";
		progressBar pbBar "ProgressBar" pos:[8,12] width:377 height:18 value:0
	)
	
	local objectsToProcess = for object in objects where object.parent == undefined and (classOf object) != XRefObject and (getAttrClass object) == "Gta Object" collect object
	
	local totalItems = objectsToProcess.count
	local currentItem = 0;
	pbRollout.pbBar.value = 100 * currentItem / totalItems
	
	local msg = ("Rendering Props (" + (currentItem as string) + " of " + (totalItems as string)) + ")";
	pbRollout.title = msg;
	
	CreateDialog pbRollout
	
	for i = 1 to objectsToProcess.count do 
	(
		local object = objectsToProcess[i]
		
		if (mod i 10) == 0.0 then
		(
			gc()
		)
		
		local test = regEx.Match object.name
		local testFragTemp = regExFragTemp.Match object.name
		local testlow = regExLow .Match object.name
		
		if (test.groups.item[2].value != "_frag_") and (testFragTemp.groups.item[2].value != "_fragtemp_") and (testlow.groups.item[2].value != "_L")then
		(
			local rageMaterial = object.material
			--local convertedMaterial = processMaterial rageMaterial
			
			--object.material = convertedMaterial
			
			local currentObject = #()
			
			gatherCurrent object &currentObject
			
			select currentObject
			
			max hide inv
			
			rotationObject = eulerangles 0 0 110
			rotate object rotationObject
			
			max zoomext sel all
			
			local offset1 = (object.max - object.center)
			offset1.x = offset1.z
			offset1.y = offset1.z
			
			offset1.x *= 4
			offset1.z *= 5
			offset1.x = -offset1.x
			
			local offset2 = (object.max - object.center)
			offset2.x *= 6
			offset2.y *= 10
			offset2.x = -offset2.x
			
			mainLight.pos = object.center + offset1
			backLight.pos = object.center - offset2
			
			
			local directory =(  project.art+"\\localDB\\Thumbs_props\\" + GetLevelNameProp("lvl"))
			
			local directoryExists = getDirectories directory
			
			if 	directoryExists.count == 0  then
			(
				makeDir directory
			)
			
			local filePath = (directory + "\\" + object.name+".bmp")
			local File = getFiles filePath
			
			if  File.count != 0 then
			(
				local readOnly = getFileAttribute filePath#readOnly
				
				if not readOnly then
				(
					deleteFile filePath
					
					render outputSize:[320,200] force2sided:true vfb:false outputfile:filePath
					gc()
					
					ProcessedRerenderd += 1
				)
			)
			else
			(
				render outputSize:[320,200] force2sided:true vfb:false outputfile:filePath
				gc()
				
				ProcessedNew += 1
			)
			
			rotationObject = eulerangles 0 0 -110
			rotate object rotationObject
			
			--object.material = rageMaterial
		)
		
		currentItem += 1
		pbRollout.pbBar.value = 100 * currentItem / totalItems
		local msg = ("Rendering Props (" + (currentItem as string) + " of " + (totalItems as string)) + ")";
		pbRollout.title = msg;
	)
	
	DestroyDialog pbRollout
	
	delete mainLight
	delete backLight
	
	switchLights lights
-- 	max default lighting toggle
	renderers.current = currentRenderer
	backgroundColor = back_Ground
	ambientColor = ambient
	hideByCategory.helpers = false
	
	------------------------------------------
	-- Source Control
	------------------------------------------
	local fileList = getFiles (project.art+"\\localDB\\Thumbs_props\\"+GetLevelNameProp("lvl")+"\\*.bmp")
	if fileList.count > 0 then	(
	local p4 = RsPerforce()
	local depotFiles = #()
	p4.connect (project.art)		
	depotFiles = p4.local2depot fileList
	p4.add depotFiles
	p4.disconnect()
	)
	
	
	------------------------------------------
	-- Source Control
	------------------------------------------
	
	
	if batchRender != true then
	(
		messagebox ("Rendering Complete.\nNew Files Added: " + (ProcessedNew as string) + "\nRenders Updated: " + (ProcessedRerenderd as string) + "\nFiles Renderd to: " + (project.art+"\\localDB\\Thumbs_props\\" + GetLevelNameProp("lvl")) + "\nPlease check this folder into perforce, Make sure to check for new renders and add them.") beep:false
	)
)

fn RenderThumbs All = 
(

	FilesList=#(
	"X:\\Art\\Maps\\c_cemet\\Props\\",
	"X:\\Art\\Maps\\c_cult\\Props\\",
	"X:\\Art\\Maps\\c_emera\\Props\\",	
	"X:\\Art\\Maps\\c_nyc\Props\\",
	"X:\\Art\\Maps\\c_pana\\Props\\",
	"X:\\Art\\Maps\\s_air\\props\\",
	"X:\\Art\\Maps\\s_binner\\props\\",
	"X:\\Art\\Maps\\s_bouter\\Props\\",
	"X:\\Art\\Maps\\s_busDep\\props\\",
	"X:\\Art\\Maps\\s_clinic\\Props\\",
	"X:\\Art\\Maps\\s_club\\Props\\",
	"X:\\Art\\Maps\\s_fashion\\Props\\",
	"X:\\Art\\Maps\\s_fav1\\props\\",
	"X:\\Art\\Maps\\s_fav2\\props\\",
	"X:\\Art\\Maps\\s_hotel\\props\\",
	"X:\\Art\\Maps\\s_marina\\props\\",
	"X:\\Art\\Maps\\s_office\\props\\",
	"X:\\Art\\Maps\\s_police\\props\\",
	"X:\\Art\\Maps\\s_stad\\props\\",
	"X:\\Art\\Maps\\s_vert\\props\\",	
	"X:\\Art\\Maps\\v_bus\\props\\",
	"X:\\Art\\Maps\\v_fav1\\props\\",
	"X:\\Art\\Maps\\v_mar\\props\\",	
	"X:\\Art\\Maps\\v_pent\\props\\"	
	)	

	if All == true then
	(
		for Items in FilesList do
		(
			MaxFileList = getFiles (Items + "*.max")
			
			if MaxFileList.count != 0 then
			(
				
				for Files in MaxFileList do
				(
					gc()
					loadMaxFile Files quiet:true
					
					RenderThumbsScene()
				)
			)
		)
			
	)
	else
	(
		RenderThumbsScene()
	)

)

fn RunRender =
(
	local result = true
	
	print batchRender
	if batchRender != true then
	(
		result = queryBox ("Are you sure you want to render out thumbnails for this scene.\nThis will check out " + (project.art+"\\localDB\\Thumbs_props\\" + GetLevelNameProp("lvl")) + " in perforce") beep:false
	)
	
	if result == true then
	(
		leveltype = GetLevelNameProp("prop")
		if leveltype == "props" then
		(
			
			------------------------------------------
			-- Source Control
			------------------------------------------
			local fileList = getFiles (project.art+"\\localDB\\Thumbs_props\\"+GetLevelNameProp("lvl")+"\\*.bmp")	
			if fileList.count > 0 then (
			local p4 = RsPerforce()
			local depotFiles = #()
			p4.connect (project.art)	
					
			depotFiles = p4.local2depot fileList
			p4.edit depotFiles
			p4.disconnect()	
			)
			------------------------------------------
			-- Source Control
			------------------------------------------
			
			
			
			
			--p4.edit(p4.path()+"\\projects\\"+project.name+"\\"+project.name+"_Art\\localDB\\Thumbs_props\\" + GetLevelNameProp("lvl")+"\\...")
			--print ("p4 edit: "+p4.path()+"\\projects\\"+project.name+"\\"+project.name+"_Art\\localDB\\Thumbs_props\\" + GetLevelNameProp("lvl")+"\\...")
			
			with redraw off
			(
				undo off
				(
					suspendEditing()
					
					RenderThumbsScene()
					
					resumeEditing()
				)
			)
			
		)
		else
		(
			messagebox "This is not a prop file"
		)
	)
)

RunRender()

