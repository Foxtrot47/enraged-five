

global RSL_PropDictionaryEd

filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_dotNetUIOps.ms")
filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_INIOperations.ms")
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSV_PropDictionary/RSL_PropDictionaryOps_v2.ms") -- load in RSLDN_propDictionaryOps stuct
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSV_PropDictionary/RSL_PropDictionaryStructs.ms")
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSV_PropDictionary/RSL_PropDictionaryFilterOps.ms")
filein "pipeline/util/p4.ms"

dotnet.loadAssembly (RsConfigGetWildWestDir() + "script/max/Rockstar_London/dll/RGLExportHelpers.dll")

struct RSL_PropDictionaryEditor
(
	localConfigINI = (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSV_PropDictionary\\RSL_PropDictionaryEditor.ini"),
	dictionaryRoot = (project.art + "\\localDB\\Dictionaries\\"),
	thumbnailPath = (project.art + "\\localDB\\Thumbs_props\\"),
	loadLogo = (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\images\\RS_Logo.jpg"),
	
	dictionaryOps = RSL_propDictionaryOps(),
	filterOps = RSL_PropDictionaryFilterOps(),
	p4 = RsPerforce(),
	
	idxTXD = getattrindex "Gta Object" "TXD",
	
	rootDictionaryNode,
	sceneDictionaryNode,
	sceneDictionary,
	stateArray,
	dictionaryArray,
	dictionaryChanged = false,
	
	initThread,
	
	loadForm,
	dictionaryForm,
	
	enableFilters,
	editFilters,
	
	dictionaryTreeView,
	changeTXDMenu,
	removeMenuItem,
	
	mainTabControl,
	propertiesTab,
	objectLabel,
	propertiesTreeView,
	statePanel,
	thumbNailView,
	
	sceneTreeView,
	
	removeButton,
	removeAllButton,
	addSelectedButton,
	addAllButton,
	saveDictionaryButton,
	
	
	fn indexInStateArray state =
	(
		local result = 0
		
		for i = 1 to stateArray.count do
		(
			if stateArray[i].name == state then
			(
				result = i
				exit
			)
		)
		
		result
	),
	
	fn dispatchOnStateControlChanged s e =
	(
		RSL_PropDictionaryEd.stateControlChanged s e
	),
	
	fn stateControlChanged s e =
	(
		local index = s.SelectedIndex
		
		for i = 0 to s.Items.Count - 1 do
		(
			if i != index then
			(
				s.SetItemChecked i false
			)
			else
			(
				s.SetItemChecked i true
			)
		)
		
		if s.Name == "state of play" then
		(
			local completion = findItem stateArray[1].Items s.SelectedItem
			
			dictionaryTreeView.SelectedNode.Completion = completion
			
			dictionaryTreeView.invalidate()
		)
		
		local stateIndex = indexInStateArray s.Name
		dictionaryTreeView.SelectedNode.tag.value.attributeArray[stateIndex] = s.SelectedItem
		
		dictionaryChanged = true
	),
	
	fn createStateControls =
	(
		statePanel.controls.clear()
		
		local rowArray = for i = 1 to stateArray.count collect
		(
			(dataPair type:"Absolute" value:(stateArray[i].items.count * 17))
		)
		
		stateTable = RS_dotNetUI.tableLayout "stateTable" collumns:#((dataPair type:"Percent" value:50), (dataPair type:"Percent" value:50)) rows:rowArray\
															dockStyle:RS_dotNetPreset.DS_Top --cellStyle:RS_dotNetPreset.CBS_Single
		stateTable.BackColor = RS_dotNetPreset.controlColour_Medium
		stateTable.AutoScroll = true
		
		for i = 1 to stateArray.count do
		(
			local currentState = stateArray[i]
			local stateLabel = RS_dotNetUI.Label "statesLabel" text:currentState.name \
													textAlign:RS_dotNetPreset.CA_TopLeft
			
			stateLabel.Font = RS_dotNetPreset.FontMedium
			
			stateControl = RS_dotNetUI.checkedListBox currentState.name items:currentState.Items borderStyle:RS_dotNetPreset.BS_None
			stateControl.BackColor = RS_dotNetPreset.controlColour_Medium
			stateControl.CheckOnClick = true
			dotnet.addEventHandler stateControl "SelectedIndexChanged" dispatchOnStateControlChanged
			
			stateArray[i].control = stateControl
			
			stateTable.controls.add stateLabel 0 (i - 1)
			stateTable.controls.add stateControl 1 (i - 1)
		)
		
		
		statePanel.controls.add stateTable
	),
	
	fn initialiseDictionaryTreeView =
	(
		if (p4.connected()) then
		(
			p4.connect (project.intermediate)
			local depotFiles = p4.local2depot (getFiles (dictionaryRoot + levelConfig.level + "/*.xml"))
			p4.sync depotFiles
		)
		else
		(
			messageBox "You are not currently connected to perforce.\nNo files will be synched." title:"Warning..."
		)
		
		rootDictionaryNode = dictionaryTreeView.Nodes.Add ("Dictionaries for: " + levelConfig.level)
		rootDictionaryNode.tag = dotNetMXSValue undefined
		rootDictionaryNode.expand()
		
		dictionaryArray = dictionaryOps.loadLevelXmlDictionaries levelconfig.level
		
		local index = dictionaryOps.indexOfCurrentDictionary dictionaryArray
		
		if index == 0 then
		(
			local okToContinue = queryBox "This prop file does not have a dictionary. Do you want to create one now?" title:"Warning..."
			
			if okToContinue then
			(
				local newDictionary = dictionaryOps.createDictionaryFromScene()
				
				if (p4.connected()) then
				(
					local depotFiles = p4.local2depot newDictionary.xmlFile
					p4.add depotFiles
				)
				else
				(
					messageBox "You are not currently connected to perforce.\nThe new fille will not be added." title:"Warning..."
				)
				
				append dictionaryArray newDictionary
				
				sceneDictionary = newDictionary
				
				dictionaryChanged = true
			)
		)
		else
		(
			if (p4.connected()) then
			(
				depotFiles = p4.local2depot dictionaryArray[index].xmlFile
				p4.edit depotFiles
			)
			else
			(
				messageBox "You are not currently connected to perforce.\nThe file will not be checked out." title:"Warning..."
			)
			
			sceneDictionary = dictionaryArray[index]
		)
		
		if (p4.connected()) then (p4.disconnect())
		
		for dictionary in dictionaryArray do 
		(
			dictionary.toTreeView rootDictionaryNode stateArray updateProperties:true
		)
		
		local sceneName = getFilenameFile maxFilename
		
		for i = 0 to rootDictionaryNode.Nodes.Count - 1 do
		(
			if rootDictionaryNode.Nodes.Item[i].text == sceneName then
			(
				sceneDictionaryNode = rootDictionaryNode.Nodes.Item[i]
			)
		)
	),
	
	fn updateDictionaryTreeView =
	(
		dictionaryTreeView.Nodes.Clear()
		
		rootDictionaryNode = dictionaryTreeView.Nodes.Add ("Dictionaries for: " + levelConfig.level)
		rootDictionaryNode.tag = dotNetMXSValue undefined
		
		for dictionary in dictionaryArray do 
		(
			dictionary.toTreeView rootDictionaryNode stateArray checkFilters:enableFilters.checked
		)
		
		rootDictionaryNode.expand()
	),
	
	fn getMaterialByName name =
	(
		local result
		
		for material in sceneMaterials do 
		(
			case (classOf material) of
			(
				default:
				(
					if material.name == name then
					(
						result = material
					)
				)
				Multimaterial:
				(
					for i = 1 to material.materialList.count do 
					(
						local subMaterial = material.materialList[i]
						
						if subMaterial != undefined then
						(
							if subMaterial.name == name then
							(
								result = subMaterial
							)
						)
					)
				)
			)
		)
		
		result
	),
	
	mapped fn populateData data type parent =
	(
		case type of
		(
			default:
			(
			)
			"Collision":
			(
				local dataItem = parent.Nodes.Add data
				dataItem.tag = dotNetMXSValue (getNodeByName data exact:true)
			)
			"Collision_Type":
			(
				local dataItem = parent.Nodes.Add data
			)
			"Material":
			(
				local dataItem = parent.Nodes.Add data
				local material = getMaterialByName data
				
				if material != undefined then
				(
					dataItem.text += " : " + ((classOf material) as string)
					dataItem.tag = dotNetMXSValue material
				)
			)
			"Textures":
			(
				local dataItem = parent.Nodes.Add (filenameFromPath data)
				dataItem.tag = dotNetMXSValue data
			)
			"TXD":
			(
				local dataItem = parent.Nodes.Add data
			)
			"Effects":
			(
				local dataItem = parent.Nodes.Add data
				dataItem.tag = dotNetMXSValue (getNodeByName data exact:true)
			)
			"Lights":
			(
				local dataItem = parent.Nodes.Add data
				dataItem.tag = dotNetMXSValue (getNodeByName data exact:true)
			)
		)
	),
	
	mapped fn populatePropertiesTreeView property =
	(
		local propertyNode = propertiesTreeView.Nodes.Add (property.name + " : " + property.value as string)
		
		case property.name of
		(
			default:
			(
			)
			"Collision":
			(
				if property.data.count > 0 then
				(
					populateData property.data property.name propertyNode
					propertyNode.ExpandAll()
				)
			)
			"Collision_Type":
			(
				if property.data.count > 1 then
				(
					populateData property.data property.name propertyNode
					propertyNode.ExpandAll()
				)
			)
			"Material":
			(
				if property.data.count > 0 then
				(
					populateData property.data property.name propertyNode
					propertyNode.ExpandAll()
				)
			)
			"Textures":
			(
				if property.data.count > 0 then
				(
					populateData property.data property.name propertyNode
					propertyNode.ExpandAll()
				)
			)
			"TXD":
			(
				if property.data.count > 1 then
				(
					populateData property.data property.name propertyNode
					propertyNode.ExpandAll()
				)
			)
			"Effects":
			(
				if property.data.count > 0 then
				(
					populateData property.data property.name propertyNode
					propertyNode.ExpandAll()
				)
			)
			"Lights":
			(
				if property.data.count > 0 then
				(
					populateData property.data property.name propertyNode
					propertyNode.ExpandAll()
				)
			)
		)
	),
	
	fn indexInListView listView string =
	(
		local result = -1
		
		for i = 0 to listView.Items.Count - 1 do
		(
			if listView.Items.Item[i] == string then
			(
				result = i
				exit
			)
		)
		
		result
	),
	
	fn updateStateControls data =
	(
		for i = 1 to stateArray.count do
		(
			local state = stateArray[i]
			local index = indexInListView state.control data[i]
			
			for i = 0 to state.control.Items.Count - 1 do
			(
				if i != index then
				(
					state.control.SetItemChecked i false
				)
				else
				(
					state.control.SetItemChecked i true
					state.control.SelectedIndex = i
				)
			)
		)
	),
	
	fn populateProperties data =
	(
		objectLabel.text = data.name
		
		propertiesTreeView.Nodes.Clear()
		
		populatePropertiesTreeView data.propertyArray
		
		updateStateControls data.attributeArray
	),
	
	fn recurseChangeTXD object TXD =
	(
		setattr object idxTXD TXD
		
		for child in object.children where (getAttrClass child) == "Gta Object" do
		(
			recurseChangeTXD child TXD
		)
	),
	
	fn dispatchChangeTXD s e =
	(
		RSL_PropDictionaryEd.changeTXD s e
	),
	
	fn changeTXD s e =
	(
		local propData = dictionaryTreeView.SelectedNode.tag.value
		
		recurseChangeTXD propData.object s.text
		
		propData.getProperties()
		
		populateProperties propData
	),
	
	fn initialiseSceneTreeView =
	(
		local sceenRootNode = sceneTreeView.Nodes.Add maxFilename
		
		local sceneRootNodes = for object in rootNode.children where (getAttrClass object) == "Gta Object" \
																		and (matchPattern object.name pattern:"*_frag_") == false \
																		and (matchPattern object.name pattern:"*_fragtemp_") == false \
																		and (matchPattern object.name pattern:"*_L") == false collect object
		
		local TXDArray = #()
		
		for object in sceneRootNodes do
		(
			local newNode = sceenRootNode.Nodes.add object.name --+ " - " + (getattrclass child))
			newNode.tag = dotNetMXSValue object
			
			local index = 0
			
			if sceneDictionary != undefined then
			(
				index = sceneDictionary.indexOf object.name
			)
			
			if index != 0 then
			(
				newNode.forecolor = (DotNetClass "System.Drawing.Color").LimeGreen
				newNode.NodeFont = RS_dotNetPreset.FontMedium
			)
			else
			(
				newNode.forecolor = (DotNetClass "System.Drawing.Color").Crimson
				newNode.NodeFont = RS_dotNetPreset.FontMedium
			)
			
			appendIfUnique TXDArray (getattr object idxTXD)
		)
		
		local menuItemArray = #()
		
		for item in TXDArray do
		(
			local menuItem = dotNetObject "ToolStripMenuItem"
			menuItem.name = item as string
			menuItem.Text = item as string
			
			dotnet.addEventHandler menuItem "Click" dispatchChangeTXD
			
			append menuItemArray menuItem
		)
		
		changeTXDMenu.DropDownItems.AddRange menuItemArray
		
		sceneTreeView.ExpandAll()
	),
	
	fn recurseTreeNodes treeView name &node =
	(
		for i = 0 to treeView.nodes.count - 1 do
		(
			local currentNode = treeView.nodes.Item[i]
			
			if (matchPattern currentNode.text pattern:name) then
			(
				node = currentNode
			)
			else
			(
				recurseTreeNodes currentNode name &node
			)
		)
	),
	
	fn getTreeNodeByName treeView name =
	(
		local result
		
		recurseTreeNodes treeView name &result
		
		result
	),
	
	fn updateThumbnail file =
	(
		local imageClass = dotNetClass "System.Drawing.Image"
-- 		local image = thumbnailPath + levelconfig.level + "\\" + name + ".bmp"
		local temp = imageClass.FromFile file
		
-- 		if not (doesFileExist image) then
-- 		(
-- 			temp = imageClass.FromFile (thumbnailPath + "MissingDefult.bmp")
-- 		)
-- 		else
-- 		(
-- 			temp = imageClass.FromFile image
-- 		)
		
		local image = dotNetObject "System.Drawing.Bitmap" temp
		thumbNailView.Image = image
		
		temp.Dispose()
		temp = undefined
	),
	
	mapped fn addSceneNodes currentNode =
	(
		local dictionaryNode = getTreeNodeByName dictionaryTreeView (getFilenameFile maxFilename)
		
		if dictionaryNode == undefined then
		(
			local rootTreeNode = dictionaryTreeView.Nodes.Item[0]
			dictionaryNode = rootTreeNode.nodes.add (getFilenameFile maxFile)
			dictionaryNode.tag = dotNetMXSValue (DataPair maxFile:sceneDictionary.maxFile xmlFile:sceneDictionary.xmlFile)
		)
		
		local object = currentNode.tag.value
		local propNode = getTreeNodeByName dictionaryNode object.name
		
		if propNode == undefined then
		(
			local prop = RSL_prop name:object.name
			prop.thumbnail = dictionaryOps.getPropThumbnail object.name
			prop.getProperties()
			prop.attributeArray = for i = 1 to stateArray.count collect stateArray[i].items[1]
			
			local propNode = RS_dotNetUI.taskTreeNode text:prop.name completion:1 \
																							tag:prop	
			propNode.forecolor = (DotNetClass "System.Drawing.Color").LimeGreen
			currentNode.forecolor = (DotNetClass "System.Drawing.Color").LimeGreen
			
			dictionaryNode.nodes.add propNode
			append sceneDictionary.propArray prop
			
			dictionaryChanged = true
		)
	),
	
	
	
	
	
	
	
	
	
	
	fn dispatchOnDictionaryTreeViewClicked s e =
	(
		RSL_PropDictionaryEd.dictionaryTreeViewClicked s e
	),
	
	fn dictionaryTreeViewClicked s e =
	(
		local hitNode = s.getNodeAt (dotNetObject "System.Drawing.Point" e.x e.y)
		if hitNode != undefined and hitNode.parent != undefined then
		(
			s.SelectedNode = hitNode
			
			local sceneNode = getTreeNodeByName sceneTreeView s.SelectedNode.text
			sceneTreeView.SelectedNode = sceneNode
			
			local propData = s.SelectedNode.tag.value
			
			if (classOf propData) == RSL_prop then
			(
				if (isValidNode propData.object) then
				(
					if (getCommandPanelTaskMode()) == #modify then
					(
						setCommandPanelTaskMode #display
					)
					
					select propData.object
					max zoomext sel
				)
				
				if mainTabControl.SelectedTab == propertiesTab then
				(
					updateThumbnail propData.thumbnail
					
					populateProperties propData
				)
			)
			else
			(
				
			)
		)
	),
	
	fn dispatchDictionaryRightClickOpening s e =
	(
		RSL_PropDictionaryEd.dictionaryRightClickOpening s e
	),
	
	fn dictionaryRightClickOpening s e =
	(
		if (getNodeByName dictionaryTreeView.SelectedNode.Text) == undefined then
		(
			changeTXDMenu.Enabled = false
			removeMenuItem.Enabled = false
		)
		else
		(
			changeTXDMenu.Enabled = true
			removeMenuItem.Enabled = true
		)
	),
	
	fn dispatchOnSceneTreeViewClicked s e =
	(
		RSL_PropDictionaryEd.sceneTreeViewClicked s e
	),
	
	fn sceneTreeViewClicked s e =
	(
		local hitNode = s.getNodeAt (dotNetObject "System.Drawing.Point" e.x e.y)
		if hitNode != undefined and hitNode.parent != undefined then
		(
			s.SelectedNode = hitNode
			
			local dictionaryNode = getTreeNodeByName dictionaryTreeView s.SelectedNode.text
			dictionaryTreeView.SelectedNode = dictionaryNode
			
			local sceneObject = s.SelectedNode.tag.value
			
			if (isValidNode sceneObject) then
			(
				select sceneObject
				max zoomext sel
			)
		)
	),
	
	fn dispatchOnTabSelected s e =
	(
		RSL_PropDictionaryEd.tabSelected s e
	),
	
	fn tabSelected s e =
	(
		if mainTabControl.SelectedTab == propertiesTab then
		(
			if dictionaryTreeView.SelectedNode != undefined then
			(
				local propData = dictionaryTreeView.SelectedNode.tag.value
				
				if (classOf propData) == RSL_prop then
				(
					updateThumbnail propData.thumbnail
-- 					thumbNailView.Image = propData.thumbnail
					
					populateProperties propData
				)
			)
		)
	),
	
	fn dispatchOnPropertiesTreeViewDoubleClicked s e =
	(
		RSL_PropDictionaryEd.propertiesTreeViewDoubleClicked s e
	),
	
	fn propertiesTreeViewDoubleClicked s e =
	(
		local hitNode = s.getNodeAt (dotNetObject "System.Drawing.Point" e.x e.y)
		if hitNode != undefined then
		(
			s.SelectedNode = hitNode
			
			if s.SelectedNode.tag != undefined then
			(
				local propertyData = s.SelectedNode.tag.value
				
				case (superClassOf propertyData) of
				(
					default:
					(
						format "Unrecognised property superClass: %\n" (superClassOf propertyData)
					)
					helper:
					(
						select propertyData
					)
					light:
					(
						select propertyData
					)
					Value:
					(
						case (classOf propertyData) of
						(
							default:
							(
								format "Unrecognised property class: %\n" (classOf propertyData)
							)
							String:
							(
								if pathConfig.isLegalPath propertyData then
								(
									if (doesFileExist propertyData) then
									(
										local image = openBitmap propertyData
										
										if image != undefined then
										(
											display image
										)
									)
									else
									(
										messageBox ("The file...\n" + propertyData + "\nDoes not exist.") title:"Error..."
									)
									display 
								)
							)
						)
					)
					material:
					(
						if not (MatEditor.isOpen()) then MatEditor.Open()
						
						meditMaterials[activeMeditSlot] = propertyData
					)
				)
			)
		)
	),
	
	fn dispatchRemoveClick s e =
	(
		RSL_PropDictionaryEd.removeClick s e
	),
	
	fn removeClick s e =
	(
		case s.name of
		(
			"removeButton":
			(
				local parent = dictionaryTreeView.SelectedNode.parent
				
				if parent != rootDictionaryNode and parent == sceneDictionaryNode then
				(
					local propIndex = sceneDictionary.indexOf dictionaryTreeView.SelectedNode.text
					local sceneNode = getTreeNodeByName sceneTreeView dictionaryTreeView.SelectedNode.text
					
					if sceneNode != undefined then
					(
						sceneNode.forecolor = (DotNetClass "System.Drawing.Color").Crimson
					)
					
					parent.Nodes.Remove dictionaryTreeView.SelectedNode
					
					deleteItem sceneDictionary.propArray propIndex
					
					dictionaryChanged = true
				)
			)
			"removeAllButton":
			(
				local removeArray = #()
				
				for i = 0 to sceneDictionaryNode.Nodes.Count - 1 do
				(
					local currentNode = sceneDictionaryNode.Nodes.Item[i]
					local propData = currentNode.tag.value
					
					if propData.object == undefined then
					(
						append removeArray currentNode
					)
				)
				
				for entry in removeArray do
				(
					local propIndex = sceneDictionary.indexOf entry.text
					
					local parent = entry.parent
					parent.Nodes.remove entry
					
					deleteItem sceneDictionary.propArray propIndex
				)
				
				if removeArray.count > 0 then
				(
					dictionaryChanged = true
				)
			)
		)
	),
	
	fn dispatchAddClick s e =
	(
		RSL_PropDictionaryEd.addClick s e
	),
	
	fn addClick s e =
	(
		local nodeArray = #()
		
		case s.name of
		(
			"addSelectedButton":
			(
				nodeArray = #(sceneTreeView.SelectedNode)
			)
			"addAllButton":
			(
				nodeArray = for i = 0 to sceneTreeView.Nodes.Item[0].Nodes.Count - 1 collect sceneTreeView.Nodes.Item[0].Nodes.Item[i]
			)
		)
		
		if sceneDictionary == undefined then
		(
			sceneDictionary = RSL_Dictionary maxFile:(maxFilePath + maxFilename) xmlFile:(dictionaryRoot + levelConfig.level + "\\" + (getFilenameFile maxFilename) +".xml")
		)
		
		addSceneNodes nodeArray
	),
	
	fn dispatchSaveDictionaryClick s e =
	(
		RSL_PropDictionaryEd.saveDictionaryClick s e
	),
	
	fn saveDictionaryClick s e =
	(
		local nodeData = sceneDictionaryNode.tag.value
		local currentDictionary = RSL_Dictionary maxFile:nodeData.maxFile xmlFile:nodeData.xmlFile
		currentDictionary.propArray.count = sceneDictionaryNode.Nodes.Count
		
		for i = 0 to sceneDictionaryNode.Nodes.Count - 1 do
		(
			local currentNode = sceneDictionaryNode.Nodes.Item[i]
			
			currentDictionary.propArray[i + 1] = currentNode.tag.value
		)
		
		local success = dictionaryOps.saveXmlDictionary sceneDictionary--currentDictionary
		
		if success then
		(
			messageBox "File saved!" title:"Success..."
			
			dictionaryChanged = false
		)
	),
	
	fn dispatchOnFormClosing s e=
	(
		RSL_PropDictionaryEd.onFormClosing s e
	),
	
	fn onFormClosing s e =
	(
		if dictionaryChanged then
		(
			local okToQuit = queryBox "The dictionary has been changed, are you sure you want to quit?" title:"Warning..."
			if not okToQuit then
			(
				e.Cancel = true
			)
		)
		
		if RSL_PropDictionaryEd.filterOps.filterForm != undefined then
		(
			RSL_PropDictionaryEd.filterOps.filterForm.Close()
		)
	),
	
	
	fn dispatchEnableFiltersPressed s e =
	(
		RSL_PropDictionaryEd.enableFiltersPressed s e
	),
	
	fn enableFiltersPressed s e =
	(
		if s.Checked then
		(
			s.Checked = false
			s.Text = "Off"
			
			updateDictionaryTreeView()
		)
		else
		(
			s.Checked = true
			s.Text = "On"
			
			updateDictionaryTreeView()
		)
	),
	
	fn dispatchEditFiltersForm s e =
	(
		RSL_PropDictionaryEd.editFiltersForm s e
	),
	
	fn editFiltersForm s e =
	(
		if filterOps.filterForm == undefined then
		(
			filterOps.createForm()
		)
	),
	
	fn createForm =
	(
		clearListener()
		
		(DotNetClass "System.Windows.Forms.Application").EnableVisualStyles()
		
		dictionaryForm = RS_dotNetUI.maxForm "dictionaryForm" text:"R* Prop Dictionary Editor V2.0" size:[620,1024] min:[220,746] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		dictionaryForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		dotnet.addEventHandler dictionaryForm "FormClosing" dispatchOnFormClosing
		
		editStateItems = RS_dotNetUI.ToolStripMenuItem "editStateItems" text:"Edit State Controls" 
-- 		dotnet.addEventHandler editStateItems "Click" dispatchCreateStateEditorForm
		
		stateMenu = RS_dotNetUI.ToolStripMenuItem "stateMenu" text:"States" menuItems:#(editStateItems) 
		
		enableFilters = RS_dotNetUI.ToolStripMenuItem "enableFilters" text:"Off"
		dotnet.addEventHandler enableFilters "Click" dispatchEnableFiltersPressed
		
		editFilters = RS_dotNetUI.ToolStripMenuItem "editFilters" text:"Edit Filters" 
		dotnet.addEventHandler editFilters "Click" dispatchEditFiltersForm
		
		filterMenu = RS_dotNetUI.ToolStripMenuItem "filterMenu" text:"Filters" menuItems:#(enableFilters, editFilters) 
		
		mainMenu = RS_dotNetUI.menuStrip "mainMenu" text:"mainMenu" menuItems:#(stateMenu, filterMenu)
		mainMenu.RenderMode = (dotNetClass "System.Windows.Forms.ToolStripRenderMode").Professional
		
		changeTXDMenu = RS_dotNetUI.ToolStripMenuItem "changeTXDMenu" text:"Change TXD To..." 
		
		removeMenuItem = RS_dotNetUI.ToolStripMenuItem "removeButton" text:"Remove" 
		dotnet.addEventHandler removeMenuItem "Click" dispatchRemoveClick
		
		dictionaryRightClick = RS_dotNetUI.ContextMenuStrip "dictionaryRightClick" items:#(changeTXDMenu, RS_dotNetUI.separator(), removeMenuItem)
		dotnet.addEventHandler dictionaryRightClick "Opening" dispatchdictionaryRightClickOpening
		
		dictionaryTreeView = RS_dotNetUI.taskTreeView "dictionaryTreeView" dockStyle:RS_dotNetPreset.DS_Fill
		dictionaryTreeView.BackColor = (DotNetClass "System.Drawing.SystemColors").InactiveBorder
		dictionaryTreeView.ContextMenuStrip = dictionaryRightClick
		dictionaryTreeView.HideSelection = false
		dotnet.addEventHandler dictionaryTreeView "Click" dispatchOnDictionaryTreeViewClicked
		
		objectLabel = RS_dotNetUI.Label "objectLabel" text:(toUpper "  Properties:") \
												textAlign:RS_dotNetPreset.CA_MiddleLeft borderStyle:RS_dotNetPreset.BS_None \
												dockStyle:RS_dotNetPreset.DS_Fill 	
		objectLabel.font = dotNetObject "System.Drawing.Font" "Trebuchet MS" 10 RS_dotNetClass.fontStyleClass.Bold --RS_dotNetPreset.FontMedium
		
		propertiesTreeView = RS_dotNetUI.TreeView "dictionaryTreeView" dockStyle:RS_dotNetPreset.DS_Fill
		propertiesTreeView.borderStyle = RS_dotNetPreset.BS_None
		propertiesTreeView.BackColor = RS_dotNetPreset.controlColour_Medium
		propertiesTreeView.Sorted = false
		propertiesTreeView.HideSelection = false
		propertiesTreeView.Font = RS_dotNetPreset.FontMedium
		dotnet.addEventHandler propertiesTreeView "MouseDoubleClick" dispatchOnPropertiesTreeViewDoubleClicked
		
		statesLabel = RS_dotNetUI.Label "statesLabel" text:(toUpper "  States:") \
												textAlign:RS_dotNetPreset.CA_MiddleLeft borderStyle:RS_dotNetPreset.BS_None \
												dockStyle:RS_dotNetPreset.DS_Fill 	
		statesLabel.font = dotNetObject "System.Drawing.Font" "Trebuchet MS" 10 RS_dotNetClass.fontStyleClass.Bold --RS_dotNetPreset.FontMedium
		
		statePanel = RSL_dotNetUIOps.panel "statePanel" borderStyle:RS_dotNetPreset.BS_None dockStyle:RS_dotNetPreset.DS_Fill
		statePanel.AutoScroll = true
		
		thumbNailView = RS_dotNetUI.pictureBox "thumbNailView" borderStyle:RS_dotNetPreset.BS_FixedSingle dockStyle:RS_dotNetPreset.DS_Fill 
		thumbNailView.SizeMode = (dotNetClass "System.Windows.Forms.PictureBoxSizeMode").StretchImage
		
		propertiesTable = RS_dotNetUI.tableLayout "propertiesTable" text:"propertiesTable" collumns:#((dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		propertiesTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		propertiesTable.RowCount = 5
		propertiesTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		propertiesTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 80)
-- 		propertiesTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
-- 		propertiesTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 25)
		propertiesTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		propertiesTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 58)
		propertiesTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 30)
		
		propertiesTable.controls.add objectLabel 0 0
		propertiesTable.controls.add propertiesTreeView 0 1
-- 		propertiesTable.controls.add propertiesListView 0 1
-- 		propertiesTable.controls.add propertyLabel 0 2
-- 		propertiesTable.controls.add selectedPropertyListView 0 3
		propertiesTable.controls.add statesLabel 0 2
		propertiesTable.controls.add statePanel 0 3
		propertiesTable.controls.add thumbNailView 0 4
		
		propertiesTab = RS_dotNetUI.tabPage "propertiesTab" text:"Properties & States" control:propertiesTable
		
		sceneTreeView = RS_dotNetUI.treeView "sceneTreeView" dockStyle:RS_dotNetPreset.DS_Fill
		sceneTreeView.BackColor = (DotNetClass "System.Drawing.SystemColors").InactiveBorder
		sceneTreeView.borderStyle = RS_dotNetPreset.BS_None
		sceneTreeView.HideSelection = false
		dotnet.addEventHandler sceneTreeView "Click" dispatchOnSceneTreeViewClicked
		
		SceneTab = RS_dotNetUI.tabPage "SceneTab" text:"Scene Tree-view" control:sceneTreeView
		
		mainTabControl = RS_dotNetUI.tabControl "mainTabControl" controls:#(propertiesTab, SceneTab) dockStyle:RS_dotNetPreset.DS_Fill
		dotnet.addEventHandler mainTabControl "Selected" dispatchOnTabSelected
		
		mainSplitContainer = RS_dotNetUI.SplitContainer "mainSplitContainer" borderStyle:RS_dotNetPreset.BS_FixedSingle dockStyle:RS_dotNetPreset.DS_Fill 
		
		mainSplitContainer.Panel1.Controls.Add dictionaryTreeView
		mainSplitContainer.Panel2.Controls.Add mainTabControl
		
		removeButton = RS_dotNetUI.Button "removeButton" text:(toUpper "Remove Selected") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler removeButton "click" dispatchRemoveClick
		
		removeAllButton = RS_dotNetUI.Button "removeAllButton" text:(toUpper "Remove All Invalid") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler removeAllButton "click" dispatchRemoveClick
		
		addSelectedButton = RS_dotNetUI.Button "addSelectedButton" text:(toUpper "Add Selected") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler addSelectedButton "click" dispatchAddClick
		
		addAllButton = RS_dotNetUI.Button "addAllButton" text:(toUpper "Add All Valid") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler addAllButton "click" dispatchAddClick
		
		saveDictionaryButton = RS_dotNetUI.Button "saveDictionaryButton" text:(toUpper "Save Dictionary") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler saveDictionaryButton "click" dispatchSaveDictionaryClick
		
		mainTable = RS_dotNetUI.tableLayout "mainTable" text:"mainTable" collumns:#((dataPair type:"Percent" value:50), (dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		mainTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		mainTable.RowCount = 5
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 22)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 50)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		
		mainTable.controls.add mainSplitContainer 0 1
		mainTable.controls.add removeButton 0 2
		mainTable.controls.add addSelectedButton 1 2
		mainTable.controls.add removeAllButton 0 3
		mainTable.controls.add addAllButton 1 3
		mainTable.controls.add saveDictionaryButton 0 4
		
		mainTable.SetColumnSpan mainSplitContainer 2
		mainTable.SetColumnSpan saveDictionaryButton 2
		
		dictionaryForm.Controls.Add mainMenu
		dictionaryForm.Controls.Add mainTable
		
		if (matchPattern maxFilePath pattern:"*\\props\\*") or (matchPattern maxFilePath pattern:"*\\buildings\\*") then
		(
			loadForm = RS_dotNetUI.maxForm "loadForm" text:"Loading..." size:[160,160] borderStyle:RS_dotNetPreset.FB_FixedToolWindow
			loadForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").CenterScreen
			loadForm.BackgroundImageLayout = (dotNetClass "System.Windows.Forms.ImageLayout").Stretch
			loadForm.BackgroundImage = (RS_dotNetObject.imageObject loadLogo)
			
			loadForm.showModeless()
			loadForm.refresh()
			
			stateArray = dictionaryOps.loadMasterStateXml()
			createStateControls()
			initialiseDictionaryTreeView()
			initialiseSceneTreeView()
			
			loadForm.Close()
			
			dictionaryForm.showModeless()
			
			dictionaryForm
		)
		else
		(
			messageBox "This is not a valid prop file and therefore no dictionaries will be loaded" title:"Error..."
		)
	)
)

if RSL_PropDictionaryEd != undefined then
(
	RSL_PropDictionaryEd.dictionaryForm.Close()
	
	if RSL_PropDictionaryEd.filterOps.filterForm != undefined then
	(
		RSL_PropDictionaryEd.filterOps.filterForm.Close()
	)
)

RSL_PropDictionaryEd = RSL_PropDictionaryEditor()
RSL_PropDictionaryEd.createForm()