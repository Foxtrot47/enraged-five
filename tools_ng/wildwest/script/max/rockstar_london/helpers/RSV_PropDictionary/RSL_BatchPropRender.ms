
try(destroydialog RSL_BatchPropRender) catch()

global batchRender

rollout RSL_BatchPropRender "R* Batch Prop File Renderer" width:240
(
	local directory
	local fileArray = #()
	
	
	fn processFiles files =
	(
		for file in files do
		(
			print file
			local fileLoaded = loadMaxFile file useFileUnits:true quiet:true
			
			if fileLoaded then
			(
				batchRender = true
				fileIn (scriptsPath + "Payne\\helpers\\RSV_PropDictionary\\RSV_QuickThumbnailRender.ms")
			)
		)
	)
	
	
	
	editText edt_path text:"Pick Directory..." width:206 across:2 align:#left offset:[-8,0] readOnly:true
	button btn_pickPath "..." width:19 height:17 align:#right offset:[5,0]
	button btn_process "Process Files" width:224 enabled:false
	
	on btn_pickPath pressed do
	(
		directory = getSavePath caption:"Pick Directory..."
		
		if directory != undefined then
		(
			edt_path.Text = directory
			btn_process.enabled = true
			
			local directoryArray = (getDirectories (directory + "\\*"))
			
			for dir in directoryArray do
			(
				join directoryArray (getDirectories (dir + "\\*"))
			)
			
			append directoryArray directory
			
			local finalArray = #()
			
			for dir in directoryArray do
			(
				local propMatch = matchPattern dir pattern:"*\\props\\"
				
				if propMatch then
				(
					append finalArray dir
				)
			)
			
			for dir in finalArray do
			(
				fileArray += getFiles (dir + "\\*.max")
			)
			
			print fileArray
			
-- 			fileArray = #("X:\payne\payne_art\Maps\c_pana\Props\pa_bg.max")
		)
	)
	
	on btn_process pressed do
	(
		processFiles fileArray
	)
)

createDialog RSL_BatchPropRender style:#(#style_toolwindow,#style_sysmenu)