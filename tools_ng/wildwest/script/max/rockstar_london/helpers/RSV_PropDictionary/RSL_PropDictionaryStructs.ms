filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_dotNetUIOps.ms")
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSV_PropDictionary/RSL_PropPropertyFunctions.ms")

struct RSL_Dictionary
(
	maxFile,
	xmlFile,
	propArray = #(),
	
	fn indexOf name =
	(
		local result = 0
		
		for i = 1 to propArray.count do 
		(
			if (toLower propArray[i].name) == (toLower name) then
			(
				result = i
				exit
			)
		)
		
		result
	),
	
	fn itemOf name =
	(
		local result
		
		for prop in propArray do 
		(
			if (toLower prop.name) == (toLower name) then
			(
				result = prop
				exit
			)
		)
		
		result
	),
	
-- 	fn 
	
	fn toTreeView rootTreeNode stateArray updateProperties:false checkFilters:false =
	(
		local DictRootNode = rootTreeNode.Nodes.Add (getFilenameFile maxFile)
		DictRootNode.tag = dotNetMXSValue (DataPair maxFile:maxFile xmlFile:xmlFile)
		
		local isLoaded = (toLower (getFilenameFile maxFile)) == (toLower (getFilenameFile maxFilename))
		
		if isLoaded and updateProperties then
		(
			progressStart "Updating Properties"
		)
		
		for i = 1 to propArray.count do 
		(
			local prop = propArray[i]
			
			if isLoaded and updateProperties then prop.getProperties()
			
			if stateArray.count == prop.attributeArray.count then
			(
				for a = 1 to stateArray.count do
				(
					if (findItem stateArray[a].items prop.attributeArray[a]) == 0 then
					(
						prop.attributeArray[a] = stateArray[a].items[1]
					)
				)
			)
			else
			(
				prop.attributeArray = #()
				prop.attributeArray.count = stateArray.count 
				
				for i = 1 to stateArray.count do
				(
					prop.attributeArray[i] = stateArray[i].items[1]
				)
			)
			
			local index = findItem stateArray[1].items prop.attributeArray[1]
			
			local okToContinue = true
			
			if checkFilters then
			(
				okToContinue = RSL_DictionaryfilterOps.isValidProp prop
			)
			
			if okToContinue then
			(
				local propNode = RS_dotNetUI.taskTreeNode text:prop.name completion:index \
																				tag:prop
				
				DictRootNode.Nodes.Add propNode
				
				if isLoaded then
				(
					if (isValidNode prop.object) then
					(
						propNode.forecolor = (DotNetClass "System.Drawing.Color").LimeGreen
					)
					else
					(
						propNode.forecolor = (DotNetClass "System.Drawing.Color").Crimson
					)
				)
				else
				(
					propNode.forecolor = RS_dotNetPreset.controlColour_dark --(DotNetClass "System.Drawing.Color").LimeGreen
				)
			)
			
			if isLoaded and updateProperties then
			(
				progressUpdate (i as float / propArray.count * 100.0)
			)
		)
		
		if isLoaded and updateProperties then progressEnd()
		
		if isLoaded then
		(
			DictRootNode.expand()
		)
		
		DictRootNode
	)
)

struct RSL_prop
(
	name,
	object,
	maxFile,
	thumbnail,
	propertyArray = #(),
	attributeArray = #(),
	
	propertyFunctions = RSL_PropPropertyFunctions(),
	
	fn indexOf property =
	(
		local result = 0
		
		for i = 1 to propertyArray.count do 
		(
			if (toLower propertyArray[i].name) == (toLower property) then
			(
				result = i
				exit
			)
		)
		
		result
	),
	
	fn itemOf property =
	(
		local result
		
		for prop in propertyArray do 
		(
			if (toLower prop.name) == (toLower property) then
			(
				result = prop
				exit
			)
		)
		
		result
	),
	
	fn getProperties =
	(
		object = getNodeByName name exact:true
		
		if object != undefined then
		(
			if (GetAttrClass object) == "Gta Object" then
			(
				propertyArray = #()
				
				local type = "Static"
				
				local fragObject = getNodeByName (name + "_frag_") exact:true
				if fragObject != undefined then
				(
					type = "Fragment"
					object = fragObject
				)
				else
				(
					local typeValue = getattr object (getattrindex "Gta Object" "Is Dynamic")
					if typeValue then type = "Dynamic"
				)
				
				append propertyArray (RSL_propProperty name:"Type" value:type data:#(type))
				append propertyArray (propertyFunctions.getTriangleCount object)
				append propertyArray (propertyFunctions.getObjectVolume object)
				append propertyArray (propertyFunctions.getObjectMemory object "")
				propertyArray += (propertyFunctions.getCollisionAndCollisionType object)
				append propertyArray (propertyFunctions.getMaterials object)
				append propertyArray (propertyFunctions.getTextures object)
				append propertyArray (propertyFunctions.getTXD object)
				append propertyArray (propertyFunctions.getTXDMemory object "")
				append propertyArray (propertyFunctions.getEffects object)
				append propertyArray (propertyFunctions.getLights object)
			)
		)
	),
	
	fn getMaterialByName name =
	(
		local result
		
		for material in sceneMaterials do 
		(
			case (classOf material) of
			(
				default:
				(
					if material.name == name then
					(
						result = material
					)
				)
				Multimaterial:
				(
					for i = 1 to material.materialList.count do 
					(
						local subMaterial = material.materialList[i]
						
						if subMaterial != undefined then
						(
							if subMaterial.name == name then
							(
								result = subMaterial
							)
						)
					)
				)
			)
		)
		
		result
	),
	
	mapped fn populateData data type parent =
	(
		case type of
		(
			default:
			(
			)
			"Collision":
			(
				local dataItem = parent.Nodes.Add data
				dataItem.tag = dotNetMXSValue (getNodeByName data exact:true)
			)
			"Collision_Type":
			(
				local dataItem = parent.Nodes.Add data
			)
			"Material":
			(
				local dataItem = parent.Nodes.Add data
				local material = getMaterialByName data
				
				if material != undefined then
				(
					dataItem.text += " : " + ((classOf material) as string)
					dataItem.tag = dotNetMXSValue material
				)
			)
			"Textures":
			(
				local dataItem = parent.Nodes.Add (filenameFromPath data)
				dataItem.tag = dotNetMXSValue data
			)
			"TXD":
			(
				local dataItem = parent.Nodes.Add data
			)
			"Effects":
			(
				local dataItem = parent.Nodes.Add data
				dataItem.tag = dotNetMXSValue (getNodeByName data exact:true)
			)
			"Lights":
			(
				local dataItem = parent.Nodes.Add data
				dataItem.tag = dotNetMXSValue (getNodeByName data exact:true)
			)
		)
	),
	
	fn toTreeView treeViewNode this =
	(
-- 		local propNode = dotNetObject "TreeNode" name
-- 		propNode.tag = dotNetMXSValue this
-- 		propNode.forecolor = (DotNetClass "System.Drawing.Color").LimeGreen
		
-- 		treeViewNode.Nodes.Add propNode
		
		for property in propertyArray do 
		(
			local propertyNode = dotNetObject "TreeNode" (property.name + " : " + property.value as string)
			
			case property.name of
			(
				default:
				(
				)
				"Collision":
				(
					if property.data.count > 0 then
					(
						populateData property.data property.name propertyNode
						propertyNode.ExpandAll()
					)
				)
				"Collision_Type":
				(
					if property.data.count > 1 then
					(
						populateData property.data property.name propertyNode
						propertyNode.ExpandAll()
					)
				)
				"Material":
				(
					if property.data.count > 0 then
					(
						populateData property.data property.name propertyNode
						propertyNode.ExpandAll()
					)
				)
				"Textures":
				(
					if property.data.count > 0 then
					(
						populateData property.data property.name propertyNode
						propertyNode.ExpandAll()
					)
				)
				"TXD":
				(
					if property.data.count > 1 then
					(
						populateData property.data property.name propertyNode
						propertyNode.ExpandAll()
					)
				)
				"Effects":
				(
					if property.data.count > 0 then
					(
						populateData property.data property.name propertyNode
						propertyNode.ExpandAll()
					)
				)
				"Lights":
				(
					if property.data.count > 0 then
					(
						populateData property.data property.name propertyNode
						propertyNode.ExpandAll()
					)
				)
			)
			
			treeViewNode.Nodes.Add propertyNode
		)
		
		treeViewNode.expand()
	)
)

struct RSL_propProperty
(
	name,
	value,
	data
)

struct RSL_State
(
	Name,
	Type,
	control,
	Items = #()
)