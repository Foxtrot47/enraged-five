fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/utils/RSL_reporter.ms")
filein (RsConfigGetWildWestDir() + "script/max/Rockstar_London/pipeline/RSL_LevelConfig.ms")
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSV_PropDictionary/RSL_PropDictionaryStructs.ms")
fileIn "pipeline/util/xml.ms"

global RSL_ConvertToXml

struct RSLDN_maxFileAndObjects
(
	maxFile,
	objectArray = #(),
	attributeArray = #(),
	propertyArray = #()
)

struct stateControl
(
	Name,
	Type,
	Items = #()
)

fn convertThread s e =
(
	for entry in RSL_ConvertToXml.processArray do
	(
		temp.convertDictionaryToXml entry
	)
)
	
mainThread = dotnetobject "CSharpUtilities.SynchronizingBackgroundWorker"
dotNet.addEventHandler MainThread "DoWork" convertThread

rollout RSL_ConvertToXml "Convert Dct To Xml"
(
	local p4workingPath = (project.art+ "/localdb/")
	local dictionaryPath = (p4workingPath +"Dictionaries/")
	local levelDirs = (getDirectories (dictionaryPath + "*"))
	local processArray = #()
	
	dropdownlist ddl_levels "Levels"
	button btn_convertSel "Convert Selected"
	button btn_convertAll "Convert All"
	label lbl_current ""
	progressBar pb_progress
	
	on RSL_ConvertToXml open do
	(
		ddl_levels.items = for entry in levelDirs collect
		(
			local filtered = (filterString entry "\\/")
			
			filtered[filtered.count]
		)
	)
	
	on btn_convertSel pressed do
	(
		pb_progress.value = 0.0
		processArray = #(ddl_levels.selected)
		
		if not mainThread.IsBusy do MainThread.RunWorkerAsync()
	)
	
	on btn_convertAll pressed do
	(
		pb_progress.value = 0.0
		processArray = ddl_levels.items
		
		if not mainThread.IsBusy do MainThread.RunWorkerAsync()
	)
)

struct RSLDN_propDictionaryOps
(
	p4workingPath = (project.art+ "/localdb/"),	
	dictionaryPath = (p4workingPath +"Dictionaries/"),
	masterStateFile = (dictionaryPath + "masterStates.sta"),
	
	reporterOps = RSL_reporter(),
	
	mainThread,
	
	--=-=-=-=-=-=-=-=-=-= MUST UPDATE getObjectProperties IF YOU WANT TO ADD ANOTHER PROPERTY =-=-=-=-=-=-=-=-=-=--
	--=-=-=-=-=-=-=-=-=-= MUST UPDATE getObjectProperties IF YOU WANT TO ADD ANOTHER PROPERTY =-=-=-=-=-=-=-=-=-=--
	--=-=-=-=-=-=-=-=-=-= MUST UPDATE getObjectProperties IF YOU WANT TO ADD ANOTHER PROPERTY =-=-=-=-=-=-=-=-=-=--
	objectPropertyCount = 12,
	
	fn convertPathToProject path =
	(
		local result = path
		
		if result[1] != "." then
		(
			local stream = path as stringStream
			skipToString stream "payne_art"
			result = ".\payne\payne_art" + (readLine stream)
		)
		
		result
	),
	
-- 	fn returnLevelName =
-- 	(
-- 		local result = undefined
-- 		local filePath = maxFilePath as stringStream
-- 		if (skipToString filePath "Maps\\") != undefined then
-- 		(
-- 			result = (readDelimitedString filePath "\\")
-- 		)
-- 		result
-- 		levelconfig.level
-- 	),
	
	fn readMasterStateFile file =
	(
		local result = undefined
		if (getFiles file).count > 0 then
		(
			local stateFile = openFile file
			result = #()
			
			while eof stateFile != true do 
			(
				line = readLine stateFile
-- 				print line
				local filtered = filterString line "="
				local control = stateControl Name:filtered[1] Type:"CheckedListBox" --filtered[2]
				
				line = readLine stateFile
-- 				print line
				local filtered = filterString line "="
				control.Items = execute filtered[2]
				
				append result control
				
			)
			
			close stateFile
		)
		else
		(
			
		)
		result
	),
	
	fn loadMasterStateXml =
	(
		local result = #()
		
		p4.connect (project.intermediate)
		local depotFiles = p4.local2depot (getFiles masterStateXml)
		p4.sync depotFiles
		p4.disconnect()
		
		xmlStruct = XmlDocument()
		xmlStruct.init()
		xmlStruct.load masterStateXml
		
		result.count = xmlStruct.document.DocumentElement.ChildNodes.Count
		
		for i = 0 to xmlStruct.document.DocumentElement.ChildNodes.Count - 1 do
		(
			local currentState = xmlStruct.document.DocumentElement.ChildNodes.Item[i]
			local name = (currentState.Attributes.ItemOf "name").value
			local type = (currentState.Attributes.ItemOf "type").value
			local itemArray = #()
			itemArray.count = currentState.ChildNodes.count
			
			for s = 0 to currentState.ChildNodes.count - 1 do 
			(
				local currentItem = currentState.ChildNodes.Item[s]
				itemArray[s + 1] = (currentItem.Attributes.ItemOf "value").value
			)
			
			result[i + 1] = RSL_State name:name type:type items:itemArray
		)
		
		result
	),
	
	fn writeMasterStateFile array file =
	(
		local result = false
		deleteFile file
		
		local newFile = createFile file
		
		for state in array do
		(
			format "%=%\n" state.Name state.Type to:newFile
			format "Items=%\n" (state.Items as string) to:newFile
		)
		close newFile
		true
	),
	
	mapped fn appendTXD object &array =
	(
		local TXD = reporterOps.getTXD object
		
		if TXD != undefined and TXD != "CHANGEME" then
		(
			local uniqueTXD = findItem array TXD
			
			if uniqueTXD == 0 then
			(
				append array TXD
			)
		)
	),
	
	fn gatherTXDs objectArray =
	(
		local result = #()
		
		appendTXD objectArray result
		
		result
	),
	
	fn getObjectProperties sceneObject =
	(
		options.printAllElements = true
		
-- 		local reporterOps = RSL_reporter()
		local propertyArray = #()
		
		local type = undefined
		local class = GetAttrClass sceneObject
		
		if class == "Gta Object" then
		(
			
			local attributeName = "Is Dynamic"
			local index = getattrindex class attributeName
			local attibuteValue = getattr sceneObject index
			
			if attibuteValue then type = "Dynamic" else type = "Static" 
			
			local isFrag = getNodeByName (sceneObject.name + "_frag_")
			if isFrag != undefined then
			(
				type = "Fragment_Object"
				sceneObject = isFrag
			)
			
			local collisionObject = "No"
			local collisionItems = #()
			
			local collisionType = undefined
			local collisionTypeItems = #()
			
			for child in sceneObject.children where (superClassOf child) == helper do
			(
				local type = undefined
				if (getattrclass child) == "Gta Collision" then
				(
					append collisionItems child.name
					
					if collisionObject == "Yes" then
					(
						collisionObject = "Multiple"
					)
					else if collisionObject == "No" then collisionObject = "Yes"
					
					type = getattr child (getattrindex "Gta Collision" "Coll Type")
					
					if type != undefined then
					(
						appendIfUnique collisionTypeItems type
					)
				)
			)
			
			if collisionTypeItems.count > 1 then
			(
				collisionType = "Multiple"
			)
			else
			(
				collisionType = collisionTypeItems[1]
			)
			
			local polyCount = 0
			case (classOf sceneObject) of
			(
				Editable_mesh:
				(
					polyCount = sceneObject.faces.count
				)
				Editable_poly:
				(
					polyCount = 0
					for face in sceneObject.faces do
					(
						try
						(
							local vertCount = polyop.getFaceDeg sceneObject face.index
							polyCount += vertCount - 2
						)
						catch(polyCount = 0 exit)
					)
				)
			)
			
			local volume = reporterOps.getObjectVolume sceneObject
			
			local materialType = undefined
			local materialItems = #()
			local material = sceneObject.material
			
			if material != undefined then
			(
				case (classOf material) of
				(
					Rage_Shader:
					(
						materialType = "Rage_Shader"
						materialItems = #(material.name)
					)
					Multimaterial:
					(
						materialType = "MultiMaterial"
						for i = 1 to material.materialList.count do
						(
							local subMaterial = material.materialList[i]
							if (classOf subMaterial) != Rage_Shader then
							(
								materialType = "Illegal_material"
							)
							else
							(
								local shaderName = RstGetShaderName subMaterial
								
								if (matchPattern shaderName pattern:"GTA_*") then
								(
									materialType = "Illegal_material"
								)
							)
							
							if  subMaterial != undefined then
							(
								append materialItems subMaterial.name
							)
							else
							(
								append materialItems "undefined"
							)
						)
					)
					default:
					(
						materialType = "Illegal_material"
						materialItems = #(material.name)
					)
				)
			)
			
			local textureCount = reporterOps.getTextureCount sceneObject outputTextures:true
-- 			if textureCount[2].count == 0 then textureCount[2] = undefined
			
			local memory = reporterOps.getObjectMemory sceneObject ("X:\\cache\\local\\convert\\dev\\levels\\" + levelconfig.level)
			if memory == 0 then memory = "idr not found"
			
			local TXDmemory = reporterOps.getTXDmemory sceneObject ("X:\\cache\\local\\convert\\dev\\levels\\" + levelconfig.level)
			if TXDmemory == 0 then TXDmemory = "itd not found"
			
			local effects = "No"
			local effectsItems = #()
			
			for child in sceneObject.children where (classOf child) == Gta_Particle do
			(
				append effectsItems child.name
				
				if effects == "Yes" then effects = "Multiple" else effects = "Yes"
			)
			
			local lightsAssigned = "No"
			local lightItems = #()
			
			for child in sceneObject.children where (superClassOf child) == light do
			(
				append lightItems child.name
				
				if lightsAssigned == "Yes" then lightsAssigned = "Multiple" else lightsAssigned = "Yes"
			)
			
			local TXD = reporterOps.getTXD sceneObject
			
			--=-=-=-=-=-=-=-=-=-= MUST UPDATE objectPropertyCount AT THE TOP IF YOU ADD ANOTHER PROPERTY =-=-=-=-=-=-=-=-=-=--
			--=-=-=-=-=-=-=-=-=-= MUST UPDATE objectPropertyCount AT THE TOP IF YOU ADD ANOTHER PROPERTY =-=-=-=-=-=-=-=-=-=--
			--=-=-=-=-=-=-=-=-=-= MUST UPDATE objectPropertyCount AT THE TOP IF YOU ADD ANOTHER PROPERTY =-=-=-=-=-=-=-=-=-=--
			--=-=-=-=-=-=-=-=-=-= MUST UPDATE objectPropertyCount AT THE TOP IF YOU ADD ANOTHER PROPERTY =-=-=-=-=-=-=-=-=-=--
			
			append propertyArray ("Type|" + type as string + "|" + (#(type as string) as string))
			append propertyArray ("Triangles|" + polyCount as string + "|" + (#(polyCount as string) as string))
			append propertyArray ("Volume|" + volume as string + "|" + (#(volume as string) as string))
			append propertyArray ("Object_Memory|" + memory as string + "|" + (#(memory as string) as string))
			
			append propertyArray ("Collision|" + collisionObject as string + "|" + (collisionItems as string))
			append propertyArray ("Collision_Type|" + collisionType as string + "|" + (collisionTypeItems as string))
-- 			local materialString = "#("
-- 			
-- 			for i = 1 to materialItems.count do
-- 			(
-- 				local item = materialItems[i]
-- 				
-- 				if i == materialItems.count then
-- 				(
-- 					materialString += ("\"" + item as string + "\"" + ")")
-- 				)
-- 				else
-- 				(
-- 					materialString += ("\"" + item as string + "\"" + ", ")
-- 				)
-- 			)
-- 			
-- 			if materialItems.count == 0 then materialString = "#()"
-- 			
-- 			local textureString = "#("
-- 			
-- 			for i = 1 to textureCount[2].count do
-- 			(
-- 				local texture = textureCount[2][i]
-- 				texture = substituteString texture "X:" "."
-- 				texture = substituteString texture "x:" "."
-- 				
-- 				if i == textureCount[2].count then
-- 				(

-- 					
-- 					textureString += ("\"" + texture + "\"" + ")")
-- 				)
-- 				else
-- 				(
-- 					textureString += ("\"" + texture + "\"" + ", ")
-- 				)
-- 			)
-- 			
-- 			if textureCount[2].count == 0 then textureString = "#()"
			
-- 			format "Properties for:%\n" sceneObject.name
-- 			format "Textures:%\n Array:%\n" textureCount[1] textureString
			
			append propertyArray ("Material|" + materialType as string + "|" + (materialItems as string))
			
			append propertyArray ("Textures|" + textureCount[1] as string + "|" + (textureCount[2] as string))
			append propertyArray ("TXD|" + TXD as string + "|" + (#(TXD) as string))
			append propertyArray ("TXD_Memory|" + TXDmemory as string + "|" + (#(TXDmemory as string) as string))
			
			append propertyArray ("Effects|" + effects as string + "|" + (effectsItems as string))
			append propertyArray ("Lights|" + lightsAssigned as string + "|" + (lightItems as string))
		)
		options.printAllElements = false
		
		propertyArray
	),
	
	fn assignObjectProperties array =
	(
		local processForm = dotNetObject "MaxCustomControls.MaxForm"
		
		processForm.Size = dotNetObject "System.Drawing.Size" 256 64
		processForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").CenterScreen
		processForm.FormBorderStyle = (dotNetClass "System.Windows.Forms.FormBorderStyle").FixedToolWindow
		processForm.Name = "assignProperties"
		processForm.Text = ("Assigning Object Properties")
		processForm.ShowInTaskbar = False
		
		local tableLayout = dotNetObject "TableLayoutPanel"
		tableLayout.ColumnCount = 1
		tableLayout.RowCount = 2
		tableLayout.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		tableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" (dotNetClass "System.Windows.Forms.SizeType").Percent 50)
		tableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" (dotNetClass "System.Windows.Forms.SizeType").Percent 50)
		
		local progress1 = dotNetObject "progressBar"
		progress1.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		progress1.Style = (dotNetClass "System.Windows.Forms.ProgressBarStyle").Continuous
		progress1.BackColor = (dotNetClass"System.Drawing.SystemColors").ControlDarkDark
		progress1.ForeColor = (DotNetClass "System.Drawing.Color").YellowGreen
		progress1.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		
		local progress2 = dotNetObject "progressBar"
		progress2.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		progress2.Style = (dotNetClass "System.Windows.Forms.ProgressBarStyle").Continuous
		progress2.BackColor = (dotNetClass"System.Drawing.SystemColors").ControlDarkDark
		progress2.ForeColor = (DotNetClass "System.Drawing.Color").YellowGreen
		progress2.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		
		tableLayout.Controls.Add progress1 0 0
		tableLayout.Controls.Add progress2 0 1
		
		processForm.Controls.Add tableLayout
		
		processForm.ShowModeless()
		
		progress1.Value = 0.0
		
		if array.count > 0 then
		(
			progress1.Step = 100 / array.count
		)
		
		for i = 1 to array.count do
		(
			progress1.PerformStep()
			
			local item = array[i]
			
			progress2.Value = 0.0
			
			if item.objectArray.count > 0 then
			(
				progress2.Step = 100 / item.objectArray.count
				
				for s = 1 to item.objectArray.count do
				(
					progress2.PerformStep()
					
					local sceneObject = (getNodeByName item.objectArray[s])
					
					if sceneObject != undefined then
					(
						item.propertyArray[s] = (getObjectProperties sceneObject)
					)
				)
			)
		)
		
		processForm.Close()
	),
	
	fn getLevelDictionary level file:undefined convert:false =
	(
		local dictionary = undefined
		
		local maxFileDictionary = (getFilenameFile maxFileName)
		
		if file != undefined then
		(
			dictionaryFile = file
		)
		else
		(
			local dictionaryFile = (dictionaryPath + level + "\\" + maxFileDictionary + ".dct")
		)
		
		local dictionaryExists = getFiles dictionaryFile
		
		if convert then
		(
			print "convert"
			dictionaryExists = getFiles (dictionaryPath + level + ".dct")
		)
		
		if dictionaryExists.count > 0 then
		(
			dictionary = #()
			
			local file = openFile dictionaryExists[1] --(dictionaryPath + level + ".dct")
			readLine file
			
			local fileCount = (readChar file) as number
			dictionary[fileCount] = undefined
			
			for i = 1 to fileCount do
			(
				dictionary[i] = RSLDN_maxFileAndObjects ()
				
				skipToString file "<"
				local maxFile = convertPathToProject (readDelimitedString file ">")
				
				dictionary[i].maxFile = maxFile
				
				skipToNextLine file
				skipToNextLine file
				local line = (readLine file)
				
				while line != "/objects" do
				(
					if line != "properties/" and line != "/properties" and line != "attributes/" and line != "a/ttributes"  then
					(
						append dictionary[i].objectArray line
					)
					
					if line == "properties/" then
					(
						local propertyArray = #()
						
						line = (readLine file)
						
						while line != "/properties"  do
						(
							local matchVolume = matchPattern line pattern:"Object Volume*"
							local matchMem = matchPattern line pattern:"Object Memory*"
							local matchTXDMem = matchPattern line pattern:"TXD Memory*"
							local matchColType = matchPattern line pattern:"Collision Type*"
							
							if matchVolume then line = substituteString line "Object Volume" "Object_Volume"
							if matchMem then line = substituteString line "Object Memory" "Object_Memory"
							if matchTXDMem then line = substituteString line "TXD Memory" "TXD_Memory"
							if matchColType then line = substituteString line "Collision Type" "Collision_Type"
							
							append propertyArray line
							
							line = (readLine file)
						)
						
						append dictionary[i].propertyArray propertyArray
					)
					
					if line == "attributes/" then
					(
						local attArray = #()
						
						line = (readLine file)
						
						while line != "/attributes"  do
						(
							append attArray line
							
							line = (readLine file)
						)
						append dictionary[i].attributeArray attArray
					)
					
					line = (readLine file)
				)
			)
			
			close file
		)
		dictionary
	),
	
	fn getDictionaries level =
	(
		local dictionary = #()
		local dictionaryFiles = getFiles (dictionaryPath + level + "\\*.dct")
		
		for file in dictionaryFiles do
		(
			local dict = (getLevelDictionary level file:file)
			dictionary += dict
		)
		dictionary
	),
	
	fn convertDictionaryToXml level =
	(
		local dictionaryArray = getDictionaries level
		
		for i = 1 to dictionaryArray.count do 
		(
			local dictionary = dictionaryArray[i]
			local XMLFile = dictionaryPath + level + "\\" + (getFilenameFile dictionary.maxFile) +".xml"
			
			if not (doesFileExist XMLFile) then
			(
				local newXML = createFile XMLFile
				
				format "<?xml version=\"1.0\"?>\n<objects>\n</objects>" to:newXML
				
				flush newXML
				close newXML
				
				xmlStruct = XmlDocument()
				xmlStruct.init()
				xmlStruct.load XMLFile
				
				print xmlStruct
				print dictionary.maxFile
				
				local attr = xmlStruct.createattribute "maxfile"
				attr.value = (substituteString dictionary.maxFile ".\payne" project.root)
				xmlStruct.document.DocumentElement.Attributes.Append (attr)
				
				for i = 1 to dictionary.objectArray.count do
				(
					local object = dictionary.objectArray[i]
					local propertyArray = dictionary.propertyArray[i]
					local attributeArray = dictionary.attributearray[i]
					
					local newObject = xmlStruct.createelement "prop" --(toLower object)
					local attr = xmlStruct.createattribute "name"
					attr.value = object
					newObject.Attributes.Append (attr)
					
					local newProperties = xmlStruct.createelement (toLower "properties")
					
					for entry in propertyArray do
					(
						local filtered = filterString entry "|"
						local value = (substituteString filtered[2] "\" " "'")
						local data
						
						if filtered[3] != undefined and filtered[3] != "undefined" and filtered[3][filtered[3].count - 1] != "." then
						(
							local tempSteam = (substituteString entry "\\" "/") as stringStream
							
							skipToString tempSteam "#"
							
							seek tempSteam ((filePos tempSteam) - 1)
							
							data = execute (readline tempSteam)
							
							close tempSteam
						)
						else
						(
							data = #()
						)
						
						if data[1] == undefined then
						(
							data = #()
						)
						
						local newProperty = xmlStruct.createelement filtered[1]
						
						local attr = xmlStruct.createattribute "value"
						attr.value = (value as string)
						newProperty.Attributes.Append (attr)
						
						for entry in data do
						(
							local newData = xmlStruct.createelement "data"
							
							attr = xmlStruct.createattribute "data"
							attr.value = (entry as string)
							newData.Attributes.Append (attr)
							
							newProperty.AppendChild (newData)
						)
						
						newProperties.AppendChild (newProperty)
					)
					
					local newAttributes = xmlStruct.createelement (toLower "attributes")
					
					for entry in attributeArray do
					(
						local newAttribute = xmlStruct.createelement "attribute"
						
						local attr = xmlStruct.createattribute "value"
						attr.value = (entry as string)
						newAttribute.Attributes.Append (attr)
						
						newAttributes.AppendChild (newAttribute)
					)
					
					newObject.AppendChild (newProperties)
					newObject.AppendChild (newAttributes)
					
					xmlStruct.document.DocumentElement.AppendChild (newObject)
				)
				
				xmlStruct.save XMLFile
			)
			
			if RSL_ConvertToXml != undefined then
			(
				RSL_ConvertToXml.lbl_current.text = (getFilenameFile dictionary.maxFile)
				RSL_ConvertToXml.pb_progress.value = (i as float / dictionaryArray.count * 100.0)
			)
		)
	),
	
	fn convertThread s e =
	(
		for entry in RSL_ConvertToXml.processArray do
		(
			temp.convertDictionaryToXml entry
		)
	),
	
	fn convertToXmlThread =
	(
		try (destroyDialog RSL_ConvertToXml) catch()
		createDialog RSL_ConvertToXml style:#(#style_toolwindow,#style_sysmenu)
	),
	
	fn loadXmlDictionary XMLFile =
	(
		local result
		
		if (doesFileExist XMLFile) then
		(
			xmlStruct = XmlDocument()
			xmlStruct.init()
			xmlStruct.load XMLFile
			
			result = RSL_Dictionary maxfile:xmlStruct.document.DocumentElement.Attributes.Item[0].value xmlFile:XMLFile
			result.propArray.count = xmlStruct.document.DocumentElement.ChildNodes.Count
			
			for i = 0 to xmlStruct.document.DocumentElement.ChildNodes.Count - 1 do
			(
				local currentNode = xmlStruct.document.DocumentElement.ChildNodes.Item[i]
				local newProp = RSL_prop name:currentNode.Attributes.Item[0].value
				local propertiesNode = currentNode.ChildNodes.Item[0]
				local attributesNode = currentNode.ChildNodes.Item[1]
				
				newProp.propertyArray.count = propertiesNode.ChildNodes.Count
				
				for p = 0 to propertiesNode.ChildNodes.Count - 1 do
				(
					local propertyNode = propertiesNode.ChildNodes.Item[p]
					local value = (propertyNode.attributes.ItemOf "value").value
					
					local data = #()
					data.count = propertyNode.ChildNodes.count
					
					for d = 0 to propertyNode.ChildNodes.count - 1 do 
					(
						data[d + 1] = (propertyNode.ChildNodes.Item[d].Attributes.ItemOf "data").value
					)
					
					local newProperty = RSL_propProperty name:propertyNode.name value:value data:data
					
					newProp.propertyArray[p + 1] = newProperty
				)
				
				newProp.attributeArray.count = attributesNode.ChildNodes.Count
				
				for a = 0 to attributesNode.ChildNodes.Count - 1 do
				(
					newProp.attributeArray[a + 1] = (attributesNode.ChildNodes.Item[a].Attributes.ItemOf "value").value
				)
				
				result.propArray[i + 1] = newProp
			)
		)
		
		result
	),
	
	fn saveXmlDictionary dictionary =
	(
		local result = true
		
		if (doesFileExist dictionary.xmlFile) then
		(
			local readOnly = getFileAttribute dictionary.xmlFile #readOnly
			
			if not readOnly then
			(
				local file = openFile dictionary.xmlFile mode:"w"
				format "<?xml version=\"1.0\"?>\n<objects>\n</objects>" to:file
				flush file
				close file
				
				xmlStruct = XmlDocument()
				xmlStruct.init()
				xmlStruct.load dictionary.xmlFile
				
				local attr = xmlStruct.createattribute "maxfile"
				attr.value = (substituteString dictionary.maxFile ".\payne" project.root)
				xmlStruct.document.DocumentElement.Attributes.Append (attr)
				
				for i = 1 to dictionary.propArray.count do
				(
					local currentProp = dictionary.propArray[i]
					
					local newObject = xmlStruct.createelement "prop" --(toLower object)
					local attr = xmlStruct.createattribute "name"
					attr.value = (toLower currentProp.name)
					newObject.Attributes.Append (attr)
					
					local newProperties = xmlStruct.createelement (toLower "properties")
					
					for property in currentProp.propertyArray do
					(
						local newProperty = xmlStruct.createelement property.name
						
						local attr = xmlStruct.createattribute "value"
						attr.value = property.value
						newProperty.Attributes.Append (attr)
						
						for entry in property.data do
						(
							local newData = xmlStruct.createelement "data"
							
							attr = xmlStruct.createattribute "data"
							attr.value = entry
							newData.Attributes.Append (attr)
							
							newProperty.AppendChild (newData)
						)
						
						newProperties.AppendChild (newProperty)
					)
					
					local newAttributes = xmlStruct.createelement (toLower "attributes")
					
					for entry in currentProp.attributeArray do
					(
						local newAttribute = xmlStruct.createelement "attribute"
						
						local attr = xmlStruct.createattribute "value"
						attr.value = entry
						newAttribute.Attributes.Append (attr)
						
						newAttributes.AppendChild (newAttribute)
					)
					
					newObject.AppendChild (newProperties)
					newObject.AppendChild (newAttributes)
					
					xmlStruct.document.DocumentElement.AppendChild (newObject)
				)
				
				xmlStruct.save dictionary.xmlFile
			)
			else
			(
				messageBox ("The Xml File: " + dictionary.xmlFile + " is Read Only. File has not been saved") title:"Save Error..."
				
				result = false
			)
		)
		
		result
	),
	
	fn loadLevelXmlDictionaries level =
	(
		local result = #()
		local dictionaryFiles = getFiles (dictionaryPath + level + "\\*.xml")
		result.count = dictionaryFiles.count
		
		for i = 1 to dictionaryFiles.count do
		(
			result[i] = (loadXmlDictionary dictionaryFiles[i])
		)
		
		result
	),
	
	fn getObjectNames structArray =
	(
		local result = #(#(),#())
		for item in structArray do
		(
			for i = 1 to item.objectArray.count do
			(
				append result[1] item.objectArray[i]
				append result[2] item.maxFile
			)
		)
		result
	),
	
	fn removeDefaultFavourite defaultFile item newEntry:"Default_Favourites" =
	(
		local fileExists = getFiles defaultFile
		
		if fileExists.count > 0 then
		(
			delIniSetting  defaultFile newEntry item.Text
		)
	),
	
	fn loadDefaultFavourites listArray favouritesArray =
	(
		local level = levelconfig.level
		local defaultPath = (dictionaryPath + level + "\\" + level + "\\")
		local defaultFile = (dictionaryPath + level + "\\" + level + "_defaultFavourites.ini")
		local favouritesExists = (getFiles defaultFile)
		
		if favouritesExists.count > 0 then
		(
			local names = getINISetting defaultFile "Default_Favourites"
			
			for name in names do
			(
				local found = false
				
				for item in listArray do
				(
					if item.Text == name then
					(
						append favouritesArray (copy item)
						found = true
					)
				)
				
				if not found then
				(
					
-- 					removeDefaultFavourite defaultFile "Default_Favourites" name
				)
			)
			
		)
		else
		(
			local directoryExists = getDirectories defaultPath
			
			if directoryExists.count == 0 then
			(
				makeDir defaultPath
			)
			local newFile = createFile defaultFile
			close newFile
		)
		defaultFile
	),
	
	fn saveDefaultFavourite defaultFile item newEntry:"Default_Favourites" =
	(
		local fileExists = getFiles defaultFile
		
		if fileExists.count > 0 then
		(
			setINISetting defaultFile newEntry item.Text item.Tag.Value
		)
	),
	
	fn writeDCTfile file array create properties:true =
	(
		local result = false
		local isReadOnly = undefined
		
		if create then
		(
			isReadOnly = false
		)
		else
		(
			isReadOnly = getFileAttribute file #readOnly
		)
		
		if not isReadOnly then
		(
			if properties then
			(
				assignObjectProperties array
			)
			
			deleteFile file
			local newFile = createFile file
			local name = getFilenameFile file
			format "%\n%\n" name array.count to:newFile
			
			for item in array do
			(
				format "<%>\nobjects/\n" item.maxFile to:newFile
				for i = 1 to item.objectArray.count do
				(
					local object = item.objectArray[i]
					format "%\n" object to:newFile
					
					format "properties/\n" to:newFile
					if item.propertyArray != undefined and item.propertyArray.count > 0 then
					(
						if item.propertyArray[i] != undefined and item.propertyArray[i].count > 0 then
						(
							for s = 1 to item.propertyArray[i].count do
							(
								format "%\n" item.propertyArray[i][s] to:newFile
							)
						)
					)
					format "/properties\n" to:newFile
					
					format "attributes/\n" to:newFile
					
					for s = 1 to item.attributeArray[i].count do
					(
						format "%\n" item.attributeArray[i][s] to:newFile
					)
					
					format "/attributes\n" to:newFile
				)
				format "/objects\n" to:newFile
			)
			result = true
			close newFile
		)
		else
		(
			messageBox "Dictionary is read only. You must have it checked out to make any changes to it" title:"Error..."
			result = false
		)
		result
	),
	
	fn writeCSVfile level all:false =
	(
-- 		local DCTfiles = getFiles  (dictionaryPath + level + "\\*.dct")
		local dictionary
		
		if all then
		(
			dictionary = getDictionaries level
		)
		else
		(
			dictionary = getLevelDictionary level
		)
		
		for item in dictionary do
		(
			local fileName = getFilenameFile item.maxFile
			local CSVfile = (dictionaryPath + level + "\\" + fileName + ".csv")
			local fileExists = getFiles CSVfile
			local okToContinue = true
			
			if fileExists.count > 0 then
			(
				local readOnly = getFileAttribute CSVfile #readOnly
				if readOnly then 
				(
					p4.edit(dictionaryOps.p4workingPath+"Dictionaries\\"+mapName+"\\"+(getFilenameFile maxFileName)+".csv")
					okToContinue = true
				)
				
				okToContinue = deleteFile CSVfile
			)
			
			if okToContinue then
			(
				local newCSV = createFile CSVfile
				
				for i = 1 to item.objectArray.count do
				(
					local name = item.objectArray[i]
					local attributeArray = item.attributeArray[i]
					local propertyArray = item.propertyArray[i]
					
					if i == 1 then
					(
						format "Object Name" to:newCSV
						
						for item in propertyArray do
						(
							local filtered = filterString item "|"
							local itemName = substituteString filtered[1] "_" " "
							
							format ",%" itemName to:newCSV
						)
						
						format "\n" to:newCSV
					)
					
					format "%" name to:newCSV
					
					for item in propertyArray do
					(
						local filtered = filterString item "|"
						local itemName = substituteString filtered[1] "_" " "
						local itemValue = substituteString filtered[2] "_" " "
						
						format ",%" itemValue to:newCSV
					)
					
					format "\n" to:newCSV
					
				)
				
				close newCSV
			)
			else
			(
				messageBox ("Could not update " + (filenameFromPath CSVfile) + ". Make sure it's not already open in an application.") title:"Error..."
			)
		)
	),
	
	fn createDictionary mapName masterStateArray =
	(
		local dictionaryArray = #()
		
		dictionaryArray[1] = RSLDN_maxFileAndObjects maxFile:(convertPathToProject (maxFilePath + maxFileName))
		dictionaryArray[1].objectArray = for object in objects where object.parent == undefined \
													and (matchPattern object.name pattern:"*_frag_") == false \
													and (matchPattern object.name pattern:"*_L") == false collect object.name
		
		for i = 1 to dictionaryArray[1].objectArray.count do
		(
			local attrArray = #(#(),#())
			for item in masterStateArray do
			(
				append attrArray[1] item.Name
				append attrArray[2] item.Items[1]
			)
			append dictionaryArray[1].attributeArray attrArray
		)
		
		local newDir = dictionaryPath + mapName
		
		if (getDirectories newDir).count == 0 then
		(
			makeDir newDir
		)
		
		writeDCTfile (newDir + "\\" + (getFilenameFile dictionaryArray[1].maxFile) + ".dct") dictionaryArray true
	),
	
	fn addPropFileToDictionary mapName dictionary masterStateArray =
	(
		local dictionaryArray = #()
		dictionaryArray[1] = RSLDN_maxFileAndObjects maxFile:(convertPathToProject (maxFilePath + maxFileName))
		
		dictionaryArray[1].objectArray = for object in objects where object.parent == undefined \
		and (matchPattern object.name pattern:"*_frag_") == false \
		and (matchPattern object.name pattern:"*_L") == false collect object.name
		
		for i = 1 to dictionaryArray[1].objectArray.count do
		(
			local attrArray = #()
			for item in masterStateArray do
			(
				append attrArray item.Items[1]
			)
			append dictionaryArray[1].attributeArray attrArray
		)
		dictionary += dictionaryArray
		
		writeDCTfile (dictionaryPath + mapName + ".dct") dictionary false
	),
	
	fn convertDictionaries =
	(
		local levelFiles = getFiles (dictionaryPath + "*.dct")
		
		clearListener()
		
		for file in levelFiles do
		(
			local newDir = (getFilenameFile file)
			local levelDir = dictionaryPath + newDir
			
			makeDir levelDir
			
			local dictionary = getLevelDictionary newDir convert:true
			
			for item in dictionary do
			(
				local newFile = levelDir + "\\" + (getFilenameFile item.maxFile) + ".dct"
				local newArray = #(item)
				writeDCTfile newFile newArray true 
			)
		)
	),
	
	mapped fn fixDictionaryPathEntries path =
	(
		local array = getLevelDictionary level file:path
		
		for item in array do
		(
			item.maxFile = substituteString item.maxFile ".\\payne_art" ".\\payne\\payne_art"
			item.maxFile = substituteString item.maxFile ".\\payne_payne_art" ".\\payne\\payne_art"
			item.maxFile = substituteString item.maxFile "x:\\" ".\\"
			item.maxFile = substituteString item.maxFile "X:\\" ".\\"
			
			for entry in item.propertyArray do
			(
				for i = 1 to entry.count do
				(
					entry[i] = substituteString entry[i] ".\\payne_art" ".\\payne\\payne_art"
					entry[i] = substituteString entry[i] ".\\payne_payne_art" ".\\payne\\payne_art"
					entry[i] = substituteString entry[i] "x:\\" ".\\"
					entry[i] = substituteString entry[i] "X:\\" ".\\"
				)
			)
		)
		
-- 		local saved = writeDCTfile path array false properties:false
-- 		
-- 		if saved then
-- 		(
-- 			format "Dictionary:% has been fixed and saved.\n" (filenameFromPath path)
-- 		)
-- 		else
-- 		(
-- 			format "Dictionary:% failed to save.\n" (filenameFromPath path)
-- 		)
	),
	
	mapped fn removeColon path =
	(
		local array = getLevelDictionary level file:path
		
		for item in array do
		(
			for entry in item.propertyArray do
			(
				for i = 1 to entry.count do
				(
					entry[i] = substituteString entry[i] ":" "|"
				)
			)
		)
		
		local saved = writeDCTfile path array false properties:false
		
		if saved then
		(
			format "Dictionary:% has been fixed and saved.\n" (filenameFromPath path)
		)
		else
		(
			format "Dictionary:% failed to save.\n" (filenameFromPath path)
		)
	),
	
	fn processDictionaries =
	(
		local dictionaryDirectories = getDirectories (dictionaryPath + "*")
		
-- 		print dictionaryDirectories
		
		for directory in dictionaryDirectories do
		(
			local dictionaries = getFiles (directory + "*.dct")
			
			fixDictionaryPathEntries dictionaries
			
			removeColon dictionaries
		)
	),
	
	fn deleteXmlFiles =
	(
		local dictionaryDirectories = getDirectories (dictionaryPath + "*")
		
		for directory in dictionaryDirectories do
		(
			local dictionaries = getFiles (directory + "*.xml")
			
			for entry in dictionaries do 
			(
				deleteFile entry
			)
		)
	)
)

-- global temp = RSLDN_propDictionaryOps()
-- temp.deleteXmlFiles()
-- temp.convertToXmlThread() --levelconfig.level
-- tempDictionaries = (temp.loadLevelXmlDictionaries levelconfig.level) --"X:\\payne\\payne_art\\localdb\\Dictionaries\\v_fav1\\fa_mp1int.xml"

-- temp.saveXmlDictionary tempDictionaries[3]
