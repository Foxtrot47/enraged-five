filein (RsConfigGetWildWestDir() + "script/max/Rockstar_London/pipeline/RSL_LevelConfig.ms")
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSV_PropDictionary/RSL_PropDictionaryOps_v2.ms")
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSV_PropDictionary/RSL_PropDictionaryStructs.ms")
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSV_PropDictionary/RSL_PropPlacerInfo.ms")
filein "pipeline/util/p4.ms"

global RSL_PropPlaceForm

struct RSL_PropPlacer
(
	dictionaryRoot = (project.art + "\\localDB\\Dictionaries\\"),
	thumbnailPath = (project.art + "\\localDB\\Thumbs_props\\"),
	loadLogo = (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\images\\RS_Logo.jpg"),
	
	dictionaryOps = RSL_propDictionaryOps(),
	infoOps = RSL_PropPlacerInfo(),
	p4 = RsPerforce(),
	
	dictionaryArray = #(),
	maxFiles = #(),
	TXDs = #(),
	masterPropArray = #(),
	currentPropArray = #(),
	thumbnailArray = #(),
	cacheArray = #(),
	
	firstItem = 0,
	filterUpdate = false,
	sceneInfoUpdate = false,
	
	selectedProp,
	CurrentXref,
	currentSnapMode,
	HideHelpers,
	
	defaultLayer = (LayerManager.getLayer 0),
	
	mousePressed = 0,
	newCopy = undefined,
	currentPos = undefined,
	directionVector = undefined,
	newHeading = undefined, 
	
	propPlacerForm,
	
	mainTabControl,
	listTab,
	favouritesTab,
	
	listViewRightClick,
	addToFavourites,
	addAllToFavourites,
	removeFromFavourites,
	thumbnailSize,
	thumbnailLarge,
	thumbnailSmall,
	
	propListView,
	
	seachTextBox,
	maxFileCombo,
	TXDcombo,
	
	hideCollisionCheckBox,
	placeSelectedButton,
	
	replaceAllButton,
	replaceSelectedButton,
	selectSimilarButton,
	selectFromListButton,
	testSceneButton,
	
	infoButton,
	
	mapped fn appendTXD TXD &result =
	(
		appendIfUnique result TXD
	),
	
	fn initialiseListView =
	(
		dictionaryArray = dictionaryOps.loadLevelXmlDictionaries levelconfig.level
		
		print dictionaryArray
		
		masterPropArray = #()
		currentPropArray = #()
		maxFiles = #("All")
		TXDs = #("All")
		
		for dictionary in dictionaryArray do
		(
			masterPropArray += dictionary.propArray
			
			appendIfUnique maxFiles (filenameFromPath dictionary.maxFile)
		)
		
		for prop in masterPropArray do 
		(
			local property = (prop.itemOf "TXD")
			
			if property != undefined then
			(
				appendTXD property.data &TXDs
			)
		)
		
		currentPropArray = masterPropArray
		
		propListView.virtualListSize = currentPropArray.count
	),
	
	fn indexOfThumbnail file =
	(
		local result = 0
		
		for entry in thumbnailArray do 
		(
			if entry.file == file then
			(
				result = entry.index
				exit
			)
		)
		
		result
	),
	
	fn imageFromThumbnail file =
	(
		local imageClass = dotNetClass "System.Drawing.Image"
		local temp = imageClass.FromFile file
		
		image = dotNetObject "System.Drawing.Bitmap" temp
		
		temp.Dispose()
		temp = undefined
		
		image
	),
	
	fn newListViewItem index =
	(
		local thumbIndex = indexOfThumbnail currentPropArray[index].thumbnail
		
		if thumbIndex == 0 then
		(
			propListView.LargeImageList.images.Add (imageFromThumbnail currentPropArray[index].thumbnail)
			propListView.SmallImageList = propListView.LargeImageList
			
			append thumbnailArray (dataPair file:currentPropArray[index].thumbnail index:propListView.LargeImageList.images.count)
			
			thumbIndex = propListView.LargeImageList.images.count
			
-- 			print propListView.LargeImageList.images.count
		)
		
		local newItem = dotNetObject "listViewItem" currentPropArray[index].name
		newItem.tag = dotNetMXSValue currentPropArray[index]
		newItem.imageIndex = thumbIndex - 1
		newItem
	),
	
	fn checkFilters prop =
	(
		local result = true
		
		if seachTextBox.text != "" then
		(
			result = matchPattern prop.name pattern:("*" + seachTextBox.text + "*")
		)
		
		case maxFileCombo.Text of
		(
			default:
			(
				if result then
				(
					result = (toLower (filenameFromPath prop.maxFile)) == (toLower maxFileCombo.Text)
				)
			)
			"All":()
		)
		
		case TXDcombo.Text of
		(
			default:
			(
				local property = (prop.itemOf "TXD")
				
				if property != undefined then
				(
					if result then
					(
						result = ((findItem property.data TXDcombo.Text) > 0)
					)
				)
				else
				(
					result = false
				)
			)
			"All":()
		)
		
		result
	),
	
	fn filterCurrentPropArray =
	(
		currentPropArray = #()
		
		for prop in masterPropArray do 
		(
			if (checkFilters prop) then
			(
				append currentPropArray prop
			)
		)
	),
	
	fn SnapModeOn = 
	(
		SnapSetting = snapMode.active
		if SnapSetting == false then
		(
			max snap toggle
		)
		
		if hideCollisionCheckBox.checked then
		(
			HideHelpers = hideByCategory.helpers
			hideByCategory.helpers = true
		)
		
		SnapSetting
	),
	
	fn ResetSnapMode SnapSetting = 
	(
		local CurrentSnapSetting = snapMode.active
		
		if SnapSetting != CurrentSnapSetting then
		(
			max snap toggle
		)
		
		if hideCollisionCheckBox.checked then
		(
			if HideHelpers != (hideByCategory.helpers) then
			(
				hideByCategory.helpers = false		
			)
		)
	),
	
	fn createProp position:undefined =
	(
		local result = false
		
		if selectedProp != undefined then
		(
			if (toLower (getFilenameFile selectedProp.maxFile)) != maxfilename then
			(
				if (doesFileExist selectedProp.maxFile) then
				(
					local objectNames = getMAXFileObjectNames selectedProp.maxFile quiet:true
					
					if (findItem objectNames selectedProp.name) != 0 then
					(
						objXRefMgr.dupMtlNameAction = #useXRefed
						local xrefRec = objXRefMgr.AddXRefItemsFromFile selectedProp.maxFile objNames:#(selectedProp.name)
						
						local xrefItem = (xrefRec.GetItem 1 #XRefObjectType)
						local XrefArray
						xrefItem.GetNodes &XrefArray
						xref = XrefArray[1]
						
						if xref != undefined then
						(
							xref.updateMaterial = true
							xref.xrefrecord.autoUpdate = true
							xref.isHidden = false
							xref.isFrozen = false
							
							select xref
							
							CurrentXref = xref
							currentSnapMode = SnapModeOn()
							
							result = true
						)
					)
					else
					(
						messageBox ("The object: " + selectedProp.name + " does not exist in the max file.\n" + selectedProp.maxFile) title:"Error..."
					)
				)
				else
				(
					messageBox ("The max file for the prop does not exist on your machine.\n" + selectedProp.maxFile) title:"Error..."
				)
			)
			else
			(
				messageBox "Cannot X-Ref a prop into the max file it exists in." title:"Error..."
			)
		)
		else
		(
			messageBox "A prop has not been selected." title:"Error..."
		)
		
		result
	),
	
	fn cb_PlacementCallback msg ir obj faceNum shift ctrl alt = 
	(
		case msg of 
		(
			#freeMove:
			(
				CurrentXref.pos = ir.pos
				#continue
			)
			#mousePoint: 
			(
				if mousePressed == 0 then
				(
					newHeading = transMatrix ir.pos
				)
				
				newCopy = copy CurrentXref
				defaultLayer.addNode newCopy
				
				newCopy.transform = newHeading
				
				mousePressed = 0
				
				if infoOps.infoForm != undefined then
				(
					sceneInfoUpdate = true
				)
				
				#continue
			)
			#mouseMove:
			(
				mousePressed += 1
				if mousePressed == 1 then
				(
					currentPos = ir.pos
					CurrentXref.pos = currentPos
				)
				
				directionVector = (currentPos - ir.pos)
				directionVector.z = 0.0
				directionVector = normalize directionVector
				
				newHeading = transMatrix currentPos
				
				newHeading.row2 = directionVector
				newHeading.row3 = [0,0,1]
				newHeading.row1 = cross newHeading.row2 newHeading.row3 
				
				CurrentXref.transform = newHeading
				#continue
			)
			#mouseAbort:
			(
				delete CurrentXref
				ResetSnapMode CurrentSnapMode
				CurrentXref = undefined
				
				if sceneInfoUpdate then
				(
					infoOps.updateSceneInfo()
				)
				
				propPlacerForm.Enabled = true
			)
		)
	),
	
	mapped fn itemOf entry name &result =
	(
		if (matchPattern entry.name pattern:name) then
		(
			result = entry
		)
	),
	
	fn itemOfProp name =
	(
		local result
		
		itemOf masterPropArray name &result
		
		result
	),
	
	mapped fn fixFragName xRef &c &count &errorArray =
	(
		local sourceName = substituteString xref.objectname "_fragtemp_" ""
		sourceName = substituteString sourceName "_frag_" ""
		
		xref.objectname = sourceName
		xref.name = uniqueName sourceName
		
		if xref.unresolved then
		(
			append errorArray (dataPair object:xref data:(filenameFromPath xref.filename))
		)
	),
	
	mapped fn addToReportList entry listView =
	(
		local newItem = dotNetObject "System.Windows.Forms.ListViewItem" entry.object.name
		newItem.subItems.Add entry.object.objectName
		newItem.subItems.Add entry.data
		newItem.tag = dotNetMXSValue entry.object
		
		listView.Items.Add newItem
	),
	
	fn dispatchErrorListDoubleClick s e =
	(
		RSL_PropPlaceForm.errorListDoubleClick s e
	),
	
	fn errorListDoubleClick s e =
	(
		if s.SelectedItems.count == 1 then
		(
			select s.SelectedItems.Item[0].tag.value
			max zoomext sel
		)
	),
	
	fn createReport name title array =
	(
		reportForm = RS_dotNetUI.maxForm name text:title size:[420,512] min:[256,256] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		reportForm.tag = dotNetMXSValue Array
		
		errorListView = RS_dotNetUI.listView "materialList" dockStyle:RS_dotNetPreset.DS_Fill
		errorListView.view = (dotNetClass "System.Windows.Forms.View").Details --Details
		errorListView.HeaderStyle = (dotNetClass "System.Windows.Forms.ColumnHeaderStyle").Nonclickable
		errorListView.fullRowSelect = true
		errorListView.HideSelection = false
		errorListView.columns.add "Object" 200
		errorListView.columns.add "Object Name" 100
		errorListView.columns.add "Data" 100
		dotnet.addEventHandler errorListView "DoubleClick" dispatchErrorListDoubleClick
		
		addToReportList array errorListView
		
		reportForm.Controls.Add errorListView
		
		reportForm.showModeless()
	),
	
	fn fixScene =
	(
		local commandPanel = getCommandPanelTaskMode()
		setCommandPanelTaskMode #utility
		
		local unresolvedXrefs = #()
		local fragTempXrefs = #()
		local notInDictionaryArray = #()
		
		for object in objects where (classOf object) == XRefObject and \
									not (matchPattern object.name pattern:"*_milo_") and \
									not (matchPattern object.name pattern:"AMB_*") do
		(
			if object.unresolved then
			(
				append unresolvedXrefs (dataPair object:object data:(filenameFromPath object.filename))
			)
			else
			(
				if (MatchPattern object.objectName pattern:"*_fragtemp_") or (MatchPattern object.objectName pattern:"*_frag_")  then
				(
					append fragTempXrefs object
				)
				
				if (itemOfProp object.objectName) == undefined then
				(
					append notInDictionaryArray (dataPair object:object data:(filenameFromPath object.filename))
				)
			)
		)
		
		local badPropFileArray = #()
		
		if fragTempXrefs.count > 0 then
		(
			local c = 1
			progressStart "Fixing FragTemps..."
			fixFragName fragTempXrefs &c &fragTempXrefs.count &badPropFileArray
			progressEnd()
		)
		
		if unresolvedXrefs.count > 0 then
		(
			createReport "UnresolvedReport" "Unresolved Xrefs" unresolvedXrefs 
		)
		
		if badPropFileArray.count > 0 then
		(
			createReport "BadPropReport" "Bad Prop files" badPropFileArray 
		)
		
		if notInDictionaryArray.count > 0 then
		(
			createReport "notInDictionaryReport" "Props Not in Dictionary" notInDictionaryArray 
		)
		
		setCommandPanelTaskMode commandPanel
	),
	
	mapped fn getSimilar object name &result =
	(
		if (classOf object) == XRefObject and (matchPattern object.objectName pattern:name) then
		(
			append result object
		)
	),
	
	mapped fn getSimilarProps object &result =
	(
		if classOf object == XRefObject then
		(
			getSimilar objects object.objectName &result
		)
		else
		(
			getSimilar objects object.name &result
		)
	),
	
	fn replaceProp all = 
	(
		local selectionArray = for object in (selection as array) where (classOf object) == XRefObject collect object
		
		if selectionArray.count > 0 then
		(
			if all then
			(
				local okToContinue = queryBox ("Are you sure you want to replace ALL INSTANCES of the selected prop\s with " + selectedProp.name + "?") title:"Warning..."
				
				if okToContinue then
				(
					local similarArray = #()
					getSimilarProps selectionArray &similarArray
					
					selectionArray = makeUniqueArray (similarArray + selectionArray)
				)
				else
				(
					selectionArray = #()
				)
			)
			else
			(
				local okToContinue = queryBox ("Are you sure you want to replace the selected prop\s with " + selectedProp.name + "?") title:"Warning..."
				
				if not okToContinue then
				(
					selectionArray = #()
				)
			)
			
			if selectionArray.count > 0 then
			(
				local newObjects = #()
				
				progressStart "Replacing Props..."
				
				CreateProp()
				
				for i = 1 to selectionArray.count do
				(
					local object = selectionArray[i]
					local oldTransForm = object.transform
					
					local theNewXref = copy CurrentXref
					
					if theNewXref != undefined then
					(
						theNewXref.transform = oldTransForm
						theNewXref.name = uniqueName theNewXref.objectname
						updatexref theNewXref
						
						append newObjects theNewXref
					)
					
					progressUpdate (i as float / selectionArray.count * 100.0)
				)
				
				delete CurrentXref
				
				progressEnd()
				
				if newObjects.count > 0 then
				(
					select newObjects
				)

				for Items in selectionArray do
				(
					if isValidNode Items then
					(
						delete Items
					)
				)
				
				MessageBox "Finished Replacing Objects!" title:"Success..."
			)
		)
	),
	
	
	
	
	
	
	
	
	
	
	fn dispatchThumbnailSizeClick s e =
	(
		RSL_PropPlaceForm.thumbnailSizeClick s e
	),
	
	fn thumbnailSizeClick s e =
	(
		propListView.LargeImageList.images.clear()
		gc()
		propListView.LargeImageList.imagesize = s.tag.value
		propListView.invalidate()
		
		if s.name == "thumbnailLarge" then
		(
			thumbnailLarge.checked = true
			thumbnailSmall.checked = false
		)
		else
		(
			thumbnailLarge.checked = false
			thumbnailSmall.checked = true
		)
	),
	
	fn dispatchCreateVirtualItem s e =
	(
		RSL_PropPlaceForm.createVirtualItem s e
	),
	
	fn createVirtualItem s e =
	(
		if cacheArray.count != 0 and e.ItemIndex >= firstItem and e.itemIndex < (firstItem + cacheArray.count) then
		(
			local index = e.ItemIndex - firstItem + 1
			e.Item = cacheArray[index]
		)
		else
		(
			e.item = newListViewItem (e.itemIndex + 1)
		)
	),
	
	fn dispatchCacheVirtualItems s e =
	(
		RSL_PropPlaceForm.cacheVirtualItems s e
	),
	
	fn cacheVirtualItems s e =
	(
		if cacheArray.count != 0 and e.startIndex >= firstItem and e.endIndex <= (firstItem + cacheArray.count) and not filterUpdate then
		(
			
		)
		else
		(
			filterUpdate = false
			
			propListView.LargeImageList.images.clear() --= imageList
			gc()
			
			firstItem = e.startIndex
			cacheArray = #()
			cacheArray.count = e.EndIndex - e.StartIndex + 1
			
			thumbnailArray = #()
			
			for i = 1 to cacheArray.count do 
			(
				local index = (i + firstItem)
				
				cacheArray[i] = newListViewItem index
			)
			
-- 			print heapfree
		)
	),
	
	fn dispatchOnListViewClicked s e =
	(
		RSL_PropPlaceForm.listViewClicked s e
	),
	
	fn listViewClicked s e =
	(
		if s.SelectedIndices.count == 1 then
		(
			local index = s.SelectedIndices.Item[0]
			
			selectedProp = currentPropArray[index + 1]
			
			if infoOps.infoForm != undefined then
			(
				infoOps.propNode.Nodes.Clear()
				infoOps.propNode.text = selectedProp.name
				
				local array = #()
				getSimilar objects selectedProp.name &Array
				
				local usedNode = dotNetObject "TreeNode" ("Used : " + array.count as string)
				infoOps.propNode.nodes.add usedNode
				
				selectedProp.toTreeView infoOps.propNode selectedProp
			)
		)
		else
		(
			selectedProp = undefined
			
			if infoOps.infoForm != undefined then
			(
				infoOps.propNode.Nodes.Clear()
				infoOps.propNode.text = "No Prop Selected"
			)
		)
	),
	
	fn dispatchFilterChanged s e =
	(
		RSL_PropPlaceForm.filterChanged s e
	),
	
	fn filterChanged s e =
	(
		filterUpdate = true
		selectedProp = undefined
		
		if infoOps.infoForm != undefined then
		(
			infoOps.propNode.Nodes.Clear()
			infoOps.propNode.text = "No Prop Selected"
		)
		
		filterCurrentPropArray()
		propListView.virtualListSize = currentPropArray.count
		propListView.invalidate()
	),
	
	fn dispatchPlaceSelectedClick s e =
	(
		RSL_PropPlaceForm.placeSelectedClick s e
	),
	
	fn placeSelectedClick s e =
	(
		print "Placing..."
		
		propPlacerForm.Enabled = false
		
		local okToContinue = createProp()
		
		if okToContinue then
		(
			mouseTrack snap:#3D trackCallBack:cb_PlacementCallback
		)
		else
		(
			propPlacerForm.Enabled = true
		)
	),
	
	fn dispatchReplaceClick s e =
	(
		RSL_PropPlaceForm.replaceClick s e
	),
	
	fn replaceClick s e =
	(
		if selectedProp != undefined then
		(
			replaceProp s.tag.value
		)
		else
		(
			messageBox "You must select a prop from the list first." title:"Error..."
		)
	),
	
	fn dispatchSelectSimilarClick s e =
	(
		RSL_PropPlaceForm.selectSimilarClick s e
	),
	
	fn selectSimilarClick s e =
	(
		case s.name of
		(
			"selectSimilarButton":
			(
				local sceneObject = selection[1]
				
				if sceneObject != undefined then
				(
					local similarArray = #()
					getSimilarProps #(sceneObject) &similarArray
					
					unhide similarArray
					
					select similarArray
				)
			)
			"selectFromListButton":
			(
				if selectedProp != undefined then
				(
					local similarArray = #()
					getSimilarProps #(selectedProp) &similarArray
					
					unhide similarArray
					
					select similarArray
				)
			)
		)
	),
	
	fn dispatchTestSceneClick s e =
	(
		RSL_PropPlaceForm.testSceneClick s e
	),
	
	fn testSceneClick s e =
	(
		fixScene()
	),
	
	fn dispatchInfoClick s e =
	(
		RSL_PropPlaceForm.infoClick s e
	),
	
	fn infoClick s e =
	(
		if infoOps.infoForm == undefined then
		(
			infoOps.createForm()
		)
	),
	
	fn dispatchOnFormClosing s e=
	(
		RSL_PropPlaceForm.onFormClosing s e
	),
	
	fn onFormClosing s e =
	(
		if infoOps.infoForm != undefined then
		(
			infoOps.infoForm.Close()
		)
	),
	
	fn createForm =
	(
		clearListener()
		
		propPlacerForm = RS_dotNetUI.maxForm "propPlacerForm" text:"R* Prop Placer V2.0" size:[364,756] min:[256,256] borderStyle:RS_dotNetPreset.FB_Sizable --FB_SizableToolWindow
		propPlacerForm.icon = RS_dotNetObject.iconObject (RsConfigGetWildWestDir() + "script/max/Rockstar_London/images/TextureBrowseIconRS.ico")
-- 		propPlacerForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		dotnet.addEventHandler propPlacerForm "FormClosing" dispatchOnFormClosing
		
		thumbnailLarge = RS_dotNetUI.ToolStripMenuItem "thumbnailLarge" text:"Large"
		thumbnailLarge.tag = dotNetMXSValue (dotNetObject "System.Drawing.Size" 256 160)
		thumbnailLarge.checked = true
		dotnet.addEventHandler thumbnailLarge "Click" dispatchThumbnailSizeClick
		
		thumbnailSmall = RS_dotNetUI.ToolStripMenuItem "thumbnailSmall" text:"Small"
		thumbnailSmall.tag = dotNetMXSValue (dotNetObject "System.Drawing.Size" 128 80)
		dotnet.addEventHandler thumbnailSmall "Click" dispatchThumbnailSizeClick
		
		thumbnailSize = RS_dotNetUI.ToolStripMenuItem "thumbnailSize" text:"Thumbnail Size" 
		thumbnailSize.DropDownItems.AddRange #(thumbnailLarge, thumbnailSmall)
		
		listViewRightClick = RS_dotNetUI.ContextMenuStrip "listViewRightClick" items:#(thumbnailSize)
		
		propListView = RS_dotNetUI.listView "propListView" dockStyle:RS_dotNetPreset.DS_Fill
		propListView.view = (dotNetClass "System.Windows.Forms.View").LargeIcon --Details
		propListView.Sorting = (dotNetClass "System.Windows.Forms.SortOrder").Ascending
		propListView.HideSelection = false
		propListView.scrollable = true
		propListView.VirtualMode = true
		propListView.columns.add "Props" 128
		propListView.BackColor = ((dotNetClass"System.Drawing.Color").fromARGB 197 197 197) --RS_dotNetPreset.controlColour_Medium
		propListView.ContextMenuStrip = listViewRightClick
		
		local cDepth = dotNetClass "ColorDepth"
		local imageList = dotNetObject "System.Windows.Forms.ImageList"
		local largeImageSize = dotNetObject "System.Drawing.Size" 256 160 --128 64
		
		imageList.imagesize = largeImageSize
		imageList.ColorDepth = cDepth.Depth32Bit
		
		propListView.LargeImageList = imageList
		
		dotNet.addEventHandler propListView "RetrieveVirtualItem" dispatchCreateVirtualItem
		dotNet.addEventHandler propListView "CacheVirtualItems" dispatchCacheVirtualItems
		dotnet.addEventHandler propListView "Click" dispatchOnListViewClicked
		
		searchLabel = RS_dotNetUI.Label "searchLabel" text:("Search For...") textAlign:RS_dotNetPreset.CA_MiddleCenter borderStyle:RS_dotNetPreset.BS_None \
													dockStyle:RS_dotNetPreset.DS_Fill 	
		searchLabel.font = dotNetObject "System.Drawing.Font" "Trebuchet MS" 8 RS_dotNetClass.fontStyleClass.Bold
		
		seachTextBox = RS_dotNetUI.textBox "seachTextBox" borderStyle:RS_dotNetPreset.BS_Fixed3D dockStyle:RS_dotNetPreset.DS_Fill
		seachTextBox.margin = (RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler seachTextBox "TextChanged" dispatchFilterChanged
		
		maxFileLabel = RS_dotNetUI.Label "maxFileLabel" text:("Max File...") textAlign:RS_dotNetPreset.CA_MiddleCenter borderStyle:RS_dotNetPreset.BS_None \
													dockStyle:RS_dotNetPreset.DS_Fill 	
		maxFileLabel.font = dotNetObject "System.Drawing.Font" "Trebuchet MS" 8 RS_dotNetClass.fontStyleClass.Bold
		
		maxFileCombo = RS_dotNetUI.ToolStripCombo "maxFileCombo" text:"All"
		maxFileCombo.FlatStyle = RS_dotNetPreset.FS_System
		maxFileCombo.AutoSize = false
		maxFileCombo.width = 160
		dotnet.addEventHandler maxFileCombo "SelectedIndexChanged" dispatchFilterChanged
		
		maxFileTooStrip = RS_dotNetUI.ToolStrip "maxFileTooStrip" dockStyle:RS_dotNetPreset.DS_Fill
		maxFileTooStrip.BackColor = RS_dotNetPreset.controlColour_Medium
		maxFileTooStrip.GripStyle = (dotNetClass "System.Windows.Forms.ToolStripGripStyle").Hidden
		maxFileTooStrip.RenderMode = (dotNetClass "System.Windows.Forms.ToolStripRenderMode").System --Professional
		maxFileTooStrip.Items.AddRange #(MaxFileCombo)
		
		TXDlabel = RS_dotNetUI.Label "TXDlabel" text:("TXD...") textAlign:RS_dotNetPreset.CA_MiddleCenter borderStyle:RS_dotNetPreset.BS_None \
													dockStyle:RS_dotNetPreset.DS_Fill 	
		TXDlabel.font = dotNetObject "System.Drawing.Font" "Trebuchet MS" 8 RS_dotNetClass.fontStyleClass.Bold
		
		TXDcombo = RS_dotNetUI.ToolStripCombo "TXDcombo" text:"All"
		TXDcombo.FlatStyle = RS_dotNetPreset.FS_System
		TXDcombo.AutoSize = false
		TXDcombo.width = 160
		dotnet.addEventHandler TXDcombo "SelectedIndexChanged" dispatchFilterChanged
		
		TXDtooStrip = RS_dotNetUI.ToolStrip "TXDtooStrip" dockStyle:RS_dotNetPreset.DS_Fill
		TXDtooStrip.BackColor = RS_dotNetPreset.controlColour_Medium
		TXDtooStrip.GripStyle = (dotNetClass "System.Windows.Forms.ToolStripGripStyle").Hidden
		TXDtooStrip.RenderMode = (dotNetClass "System.Windows.Forms.ToolStripRenderMode").System --Professional
		TXDtooStrip.Items.AddRange #(TXDcombo)
		
		hideCollisionCheckBox = RS_dotNetUI.checkBox "bestAlignButton" text:("Hide Collision") textAlign:RS_dotNetPreset.CA_MiddleCenter \
																	checkAlign:RS_dotNetPreset.CA_MiddleRight dockStyle:RS_dotNetPreset.DS_Fill
		hideCollisionCheckBox.checked = true
		hideCollisionCheckBox.font = dotNetObject "System.Drawing.Font" "Trebuchet MS" 8 RS_dotNetClass.fontStyleClass.Bold
		
		placeSelectedButton = RS_dotNetUI.Button "placeSelectedButton" text:(toUpper "Place Selected") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler placeSelectedButton "click" dispatchPlaceSelectedClick
		
		placementTable = RS_dotNetUI.tableLayout "placementTable" text:"placementTable" collumns:#((dataPair type:"Percent" value:50), (dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		placementTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		placementTable.controls.add hideCollisionCheckBox 0 0
		placementTable.controls.add placeSelectedButton 1 0
		
		placementGroup = RS_dotNetUI.GroupBox "placementGroup" text:"Placement" dockStyle:RS_dotNetPreset.DS_Fill 
		placementGroup.controls.add placementTable
		
		replaceAllButton = RS_dotNetUI.Button "replaceAllButton" text:(toUpper "Replace All") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		replaceAllButton.tag = dotNetMXSValue true
		dotnet.addEventHandler replaceAllButton "click" dispatchReplaceClick
		
		replaceSelectedButton = RS_dotNetUI.Button "replaceSelectedButton" text:(toUpper "Replace Selected") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		replaceSelectedButton.tag = dotNetMXSValue false
		dotnet.addEventHandler replaceSelectedButton "click" dispatchReplaceClick
		
		selectSimilarButton = RS_dotNetUI.Button "selectSimilarButton" text:(toUpper "Select Similar") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler selectSimilarButton "click" dispatchSelectSimilarClick
		
		selectFromListButton = RS_dotNetUI.Button "selectFromListButton" text:(toUpper "Select Similar From List") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler selectFromListButton "click" dispatchSelectSimilarClick
		
		testSceneButton = RS_dotNetUI.Button "testSceneButton" text:(toUpper "Test Scene") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler testSceneButton "click" dispatchTestSceneClick
		
		conversionTable = RS_dotNetUI.tableLayout "conversionTable" text:"conversionTable" collumns:#((dataPair type:"Percent" value:50), (dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		conversionTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		conversionTable.controls.add replaceAllButton 0 0
		conversionTable.controls.add replaceSelectedButton 1 0
		conversionTable.controls.add selectSimilarButton 0 1
		conversionTable.controls.add selectFromListButton 1 1
		conversionTable.controls.add testSceneButton 0 2
		
		conversionTable.SetColumnSpan testSceneButton 2
		
		conversionGroup = RS_dotNetUI.GroupBox "conversionGroup" text:"Conversion Tools" dockStyle:RS_dotNetPreset.DS_Fill 
		conversionGroup.controls.add conversionTable
		
		infoButton = RS_dotNetUI.Button "infoButton" text:(toUpper "Information Panel") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler infoButton "click" dispatchInfoClick
		
		mainTable = RS_dotNetUI.tableLayout "mainTable" text:"mainTable" collumns:#((dataPair type:"Percent" value:50), (dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		mainTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		mainTable.RowCount = 7
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 50)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 56)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 106)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		
		mainTable.controls.add propListView 0 0
		mainTable.controls.add searchLabel 0 1
		mainTable.controls.add seachTextBox 1 1
		mainTable.controls.add maxFileLabel 0 2
		mainTable.controls.add maxFileTooStrip 1 2
		mainTable.controls.add TXDlabel 0 3
		mainTable.controls.add TXDtooStrip 1 3
		
		mainTable.controls.add placementGroup 0 4
		mainTable.controls.add conversionGroup 0 5
		mainTable.controls.add infoButton 0 6
		
		mainTable.SetColumnSpan propListView 2
		mainTable.SetColumnSpan placementGroup 2
		mainTable.SetColumnSpan conversionGroup 2
		mainTable.SetColumnSpan infoButton 2
		
		propPlacerForm.controls.add mainTable
		
		if levelconfig.level != undefined then
		(
			loadForm = RS_dotNetUI.maxForm "loadForm" text:"Loading..." size:[160,160] borderStyle:RS_dotNetPreset.FB_FixedToolWindow
			loadForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").CenterScreen
			loadForm.BackgroundImageLayout = (dotNetClass "System.Windows.Forms.ImageLayout").Stretch
			loadForm.BackgroundImage = (RS_dotNetObject.imageObject loadLogo)
			
			loadForm.showModeless()
			loadForm.refresh()
			
			if (p4.connected()) then
			(
				p4.connect (project.intermediate)
				local depotFiles = p4.local2depot (getFiles (dictionaryRoot + levelConfig.level + "/*.xml"))
				p4.sync depotFiles
				p4.disconnect()
			)
			else
			(
				messageBox "You are not currently connected to perforce.\nThe dictionary files will not be synched." title:"Warning..."
			)
			
			initialiseListView()
			maxFileCombo.items.AddRange maxFiles
			TXDcombo.items.AddRange TXDs
			
			loadForm.Close()
			
			propPlacerForm.showModeless()
		)
		else
		(
			messageBox "This is not a valid map file. Make sure it's saved in the proper directory structure." title:"Error..."
		)
	)
)

if RSL_PropPlaceForm != undefined then
(
	RSL_PropPlaceForm.propPlacerForm.close()
)

RSL_PropPlaceForm = RSL_PropPlacer()
RSL_PropPlaceForm.createForm()