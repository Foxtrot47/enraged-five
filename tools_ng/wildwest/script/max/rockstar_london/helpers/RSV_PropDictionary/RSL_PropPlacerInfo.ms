

struct RSL_PropPlacerInfo
(
	sceneNode,
	propNode,
	
	propArray = #(),
	TXDArray = #(),
	
	propCount = 0,
	uniquePropCount = 0,
	
	mostUsed = undefined,
	leastUsed = undefined,
	
	infoForm,
	
	
	
	fn indexInArray array name =
	(
		local result = 0
		
		for i = 1 to array.count do 
		(
			if array[i].prop.name == name then
			(
				result = i
			)
		)
		
		result
	),
	
	mapped fn collectSceneInfo object =
	(
		if (classOf object) == XRefObject and not (matchPattern object.name pattern:"*_milo_") and not (matchPattern object.name pattern:"AMB_*") then
		(
			local prop = (RSL_PropPlaceForm.itemOfProp object.objectName)
			
			if prop != undefined then
			(
				propCount += 1
				
				local index = indexInArray propArray object.objectName
				
				if index == 0 then
				(
					local array = #()
					RSL_PropPlaceForm.getSimilar objects object.objectName &Array
					
					append propArray (DataPair prop:prop count:array.count)
				)
			)
		)
	),
	
	mapped fn getUniqueCount prop =
	(
		if prop.count == 1 then
		(
			uniquePropCount += 1
		)
	),
	
	mapped fn getUsed prop =
	(
		if mostUsed == undefined then
		(
			mostUsed = #(prop)
		)
		else
		(
			if prop.count > mostUsed[1].count then
			(
				mostUsed = #(prop)
			)
			if prop.count == mostUsed[1].count then
			(
				local index = indexInArray mostUsed prop.prop.name
				
				if index == 0 then
				(
					append mostUsed prop
				)
			)
		)
		
		if leastUsed == undefined then
		(
			leastUsed = #(prop)
		)
		else
		(
			if prop.count < leastUsed[1].count then
			(
				leastUsed = #(prop)
			)
			if prop.count == leastUsed[1].count then
			(
				local index = indexInArray leastUsed prop.prop.name
				
				if index == 0 then
				(
					append leastUsed prop
				)
			)
		)
	),
	
	mapped fn appendTXD TXD =
	(
		appendIfUnique TXDArray TXD
	),		
	
	mapped fn getTXDs prop =
	(
		local property = (prop.prop.itemOf "TXD")
		
		if property != undefined then
		(
			appendTXD property.data
		)
	),
	
	mapped fn addNode entry node =
	(
		node.nodes.add entry
	),
	
	fn updateSceneInfo =
	(
		sceneNode.nodes.clear()
		
		propArray = #()
		TXDArray = #()
		propCount = 0
		uniquePropCount = 0
		mostUsed = undefined
		leastUsed = undefined
		
		collectSceneInfo objects
		
		getUniqueCount propArray
		getUsed propArray
		getTXDs propArray
		
		sceneNode.nodes.add ("Total Prop Count : " + propCount as string)
		sceneNode.nodes.add ("Unique Prop Count : " + uniquePropCount as string)
		
		if mostUsed.count == 1 then
		(
			sceneNode.nodes.add ("Most Used : " + mostUsed[1].prop.name + " : " + mostUsed[1].count as string)
		)
		else
		(
			if mostUsed.count > 1 then
			(
				local mostNode = sceneNode.nodes.add ("Most Used : Multiple : " +mostUsed.count as string)
				
				addNode (for entry in mostUsed collect (entry.prop.name + " : " + entry.count as string)) mostNode
				
-- 				mostNode.expand()
			)
		)
		
		if leastUsed.count == 1 then
		(
			sceneNode.nodes.add ("Least Used : " + leastUsed[1].prop.name + " : " + leastUsed[1].count as string)
		)
		else
		(
			if leastUsed.count > 1 then
			(
				local leastNode = sceneNode.nodes.add ("Least Used : Multiple : " + leastUsed.count as string)
				
				addNode (for entry in leastUsed collect (entry.prop.name + " : " + entry.count as string)) leastNode
				
-- 				leastNode.expand()
			)
		)
		
		local TXDnode = sceneNode.nodes.add ("TXDs : " + TXDArray.count as string)
		
		addNode TXDArray TXDnode
		TXDnode.expand()
		
		sceneNode.expand()
	),
	
	
	
	
	
	
	
	
	fn dispatchOnPropertiesTreeViewDoubleClicked s e =
	(
		RSL_PropPlaceForm.infoOps.propertiesTreeViewDoubleClicked s e
	),
	
	fn propertiesTreeViewDoubleClicked s e =
	(
		local hitNode = s.getNodeAt (dotNetObject "System.Drawing.Point" e.x e.y)
		if hitNode != undefined then
		(
			s.SelectedNode = hitNode
			
			if s.SelectedNode.tag != undefined then
			(
				local propertyData = s.SelectedNode.tag.value
				
				case (superClassOf propertyData) of
				(
					default:
					(
						format "Unrecognised property superClass: %\n" (superClassOf propertyData)
					)
					helper:
					(
						select propertyData
					)
					light:
					(
						select propertyData
					)
					Value:
					(
						case (classOf propertyData) of
						(
							default:
							(
								format "Unrecognised property class: %\n" (classOf propertyData)
							)
							String:
							(
								if pathConfig.isLegalPath propertyData then
								(
									if (doesFileExist propertyData) then
									(
										local image = openBitmap propertyData
										
										if image != undefined then
										(
											display image
										)
									)
									else
									(
										messageBox ("The file...\n" + propertyData + "\nDoes not exist.") title:"Error..."
									)
									display 
								)
							)
						)
					)
					material:
					(
						if not (MatEditor.isOpen()) then MatEditor.Open()
						
						meditMaterials[activeMeditSlot] = propertyData
					)
				)
			)
		)
	),
	
	fn dispatchOnInfoFormClosing s e =
	(
		RSL_PropPlaceForm.infoOps.infoFormClosing s e
	),
	
	fn infoFormClosing s e =
	(
		infoForm = undefined
	),
	
	fn createForm =
	(
		infoForm = RS_dotNetUI.maxForm "filterForm" text:"Info Panel" size:[256,512] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		infoForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").CenterScreen
		dotnet.addEventHandler infoForm "FormClosing" dispatchOnInfoFormClosing
		
		propertiesTreeView = RS_dotNetUI.TreeView "dictionaryTreeView" dockStyle:RS_dotNetPreset.DS_Fill
		propertiesTreeView.borderStyle = RS_dotNetPreset.BS_FixedSingle
		propertiesTreeView.BackColor = RS_dotNetPreset.controlColour_Medium
		propertiesTreeView.Sorted = false
		propertiesTreeView.HideSelection = false
		propertiesTreeView.Font = RS_dotNetPreset.FontMedium
		dotnet.addEventHandler propertiesTreeView "MouseDoubleClick" dispatchOnPropertiesTreeViewDoubleClicked
		
		sceneNode = dotNetObject "TreeNode" "Scene Info"
		propertiesTreeView.Nodes.Add sceneNode
		
		propNode = dotNetObject "TreeNode" "No Prop Selected"
		propertiesTreeView.Nodes.Add propNode
		
		infoForm.controls.add propertiesTreeView
		
		updateSceneInfo()
		
		if RSL_PropPlaceForm.selectedProp != undefined then
		(
			propNode.text = RSL_PropPlaceForm.selectedProp.name
			
			local array = #()
			RSL_PropPlaceForm.getSimilar objects RSL_PropPlaceForm.selectedProp.name &Array
			
			local usedNode = dotNetObject "TreeNode" ("Used : " + array.count as string)
			propNode.nodes.add usedNode
			
			RSL_PropPlaceForm.selectedProp.toTreeView propNode RSL_PropPlaceForm.selectedProp 
		)
		
		infoForm.showModeless()
	)
)