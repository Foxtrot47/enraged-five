try (destroyDialog Rollout_PropDictEditor) catch ()

include "RSV\\RSV_PropDictionary\\RSV_PropDictUtils.ms"

rollout Rollout_PropDictEditor "Prop Dictionary Editor" width:400 height:550
(
	local SelectedProps = #();
	local SelectedNodes = #();

	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- UI Components
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	group "Prop Navigator"
	(
		--listbox ltbLevels "Level" height:25 across:2 
		MultiListBox ltbProps "Props" height:25
	)

	label lbCurrentDictionary;

	button btnResetLevel "Reset Selected Level" width:(Rollout_PropDictEditor.width - 37) 
	button btnBuildPropList "Build Dictionary For File" width:(Rollout_PropDictEditor.width - 37) 
	button btnDeleteProp "Delete Selected Props From Dictionary" width:(Rollout_PropDictEditor.width - 37) 
	button btnAddProp "Add Selected Props To The Dictionary" width:(Rollout_PropDictEditor.width - 37) 
	button btnSelectProp "Select Current Prop" width:(Rollout_PropDictEditor.width - 37)


	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- Helper Functions
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- Populate UI with data from dictionary.
	fn PopulatePropList=
	(
		ltbProps.items = #();
		local tempArray = GetPropListForCurrentLevelFromDict();
		if tempArray != undefined then
		(
			ltbProps.items = tempArray;
		)
		
	)

	-- Deletes the current level's dictionary.
	fn ResetSelectedLevel=
	(
		result = yesNoCancelBox ( "Are you sure you want to delete the prop dictionary for " + (levelname as string) + "?" );
		if result == #yes then
		(
			RS_DeleteCurrentPropDict();
			PopulatePropList();
		)
	)

	-- Deletes the selected prop(s) from the dictionary file.
	fn DeleteSelectedProp=
	(
		-- Ask Yes No to DO this
		result = yesNoCancelBox ( "Are you sure you want to delete the selected props?"  );
		if result == #yes then
		(
			for item in SelectedProps do
			(
				RS_RemovePropFromCurrentDict ( item );
				
			)

			PopulatePropList();
			ltbProps.items = #();
		)
	)

	-- Adds all the currently selected objects to the dictionary.
	fn AddSelectedPropsToDictionary=
	(
		for obj in selection do
		(
			RS_AddPropToCurrentDictionary ( obj );
		)
	)

	-- Resets the tool and reloads all the data from file.
	fn ResetTool=
	(
		PopulatePropList();
	)

	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	-- Event Handlers
	--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==
	on btnBuildPropList pressed do
	(
		AppendCurrentFilePropsToList();
		ResetTool();
	)

	-- Will destroy the dictionary file for the current level.
	on btnResetLevel pressed do
	(
		ResetSelectedLevel();
	)

	-- Remove the currently selected entry from the dictionary.
	on btnDeleteProp pressed do
	(
		DeleteSelectedProp();
	)

	-- Added the currently selected prop to the dictionary
	on btnAddProp pressed do
	(
		AddSelectedPropsToDictionary();
		ResetTool();
	)

	-- Build a list node references that have been selected.
	on ltbProps selected index do
	(
		local propArrayIndex = findItem SelectedProps ltbProps.items[index];
		if propArrayIndex > 0 then
		(
			deleteItem SelectedProps propArrayIndex;

			local propNodeIndex = findItem SelectedNodes ( FindNodeWithName ( ltbProps.items[index] ) );
			if propNodeIndex > 0 then
			(
				deleteItem SelectedNodes ( propNodeIndex );
			)
		)
		else
		(
			append SelectedProps ltbProps.items[index];
			local nodeToAdd = FindNodeWithName ( ltbProps.items[index] );
			if nodeToAdd != undefined then
			(
				append SelectedNodes ( nodeToAdd );
			)
		)
	)

	-- Will Select the props in the scene in the list.
	on btnSelectProp pressed do
	(
		clearSelection();
		select SelectedNodes;
	)

	-- Entry Point to rollout.
	on Rollout_PropDictEditor open do
	(
		ResetTool();
	)
)

createDialog Rollout_PropDictEditor