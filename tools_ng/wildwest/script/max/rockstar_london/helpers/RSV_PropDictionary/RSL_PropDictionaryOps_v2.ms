filein (RsConfigGetWildWestDir() + "script/max/Rockstar_London/pipeline/RSL_LevelConfig.ms")
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSV_PropDictionary/RSL_PropDictionaryStructs.ms")
fileIn "pipeline/util/xml.ms"
filein "pipeline/util/p4.ms"

struct RSL_propDictionaryOps
(
	p4workingPath = (project.art + "/localdb/"),	
	dictionaryPath = (p4workingPath + "Dictionaries/"),
	masterStateFile = (dictionaryPath + "masterStates.sta"),
	masterStateXml = (dictionaryPath + "masterStates.xml"),
	thumbnailPath = (project.art + "\\localDB\\Thumbs_props\\"),
	
	p4 = RsPerforce(),
	
	fn readMasterStateFile file =
	(
		local result = undefined
		if (getFiles file).count > 0 then
		(
			local stateFile = openFile file
			result = #()
			
			while eof stateFile != true do 
			(
				line = readLine stateFile
-- 				print line
				local filtered = filterString line "="
				local control = RSL_State Name:filtered[1] Type:"CheckedListBox" --filtered[2]
				
				line = readLine stateFile
-- 				print line
				local filtered = filterString line "="
				control.Items = execute filtered[2]
				
				append result control
				
			)
			
			close stateFile
		)
		else
		(
			
		)
		result
	),
	
	fn loadMasterStateXml =
	(
		local result = #()
		
		if (p4.connected()) then
		(
			p4.connect (project.intermediate)
			local depotFiles = p4.local2depot (getFiles masterStateXml)
			p4.sync depotFiles
			p4.disconnect()
		)
		else
		(
			messageBox "You are not currently connected to perforce.\nThe master state file will not be synched." title:"Warning..."
		)
		
		xmlStruct = XmlDocument()
		xmlStruct.init()
		xmlStruct.load masterStateXml
		
		result.count = xmlStruct.document.DocumentElement.ChildNodes.Count
		
		for i = 0 to xmlStruct.document.DocumentElement.ChildNodes.Count - 1 do
		(
			local currentState = xmlStruct.document.DocumentElement.ChildNodes.Item[i]
			local name = (currentState.Attributes.ItemOf "name").value
			local type = (currentState.Attributes.ItemOf "type").value
			local itemArray = #()
			itemArray.count = currentState.ChildNodes.count
			
			for s = 0 to currentState.ChildNodes.count - 1 do 
			(
				local currentItem = currentState.ChildNodes.Item[s]
				itemArray[s + 1] = (currentItem.Attributes.ItemOf "value").value
			)
			
			result[i + 1] = RSL_State name:name type:type items:itemArray
		)
		
		result
	),
	
	fn convertMasterToXml =
	(
		local stateArray = (readMasterStateFile masterStateFile)
		local XMLFile = (dictionaryPath + "masterStates.xml")
		
		local newXML = createFile XMLFile
		
		format "<?xml version=\"1.0\"?>\n<states>\n</states>" to:newXML
		
		flush newXML
		close newXML
		
		xmlStruct = XmlDocument()
		xmlStruct.init()
		xmlStruct.load XMLFile
		
		for i = 1 to stateArray.count do 
		(
			local state = stateArray[i]
			
			local newState = xmlStruct.createelement "state"
			local attr = xmlStruct.createattribute "name"
			attr.value = state.name
			newState.Attributes.Append (attr)
			
			attr = xmlStruct.createattribute "type"
			attr.value = state.type
			newState.Attributes.Append (attr)
			
			for entry in state.items do 
			(
				local newItem = xmlStruct.createelement "item"
				local attr = xmlStruct.createattribute "value"
				attr.value = entry
				newItem.Attributes.Append (attr)
				
				newState.AppendChild (newItem)
			)
			
			xmlStruct.document.DocumentElement.AppendChild (newState)
		)
		
		xmlStruct.save XMLFile
	),
	
	fn getPropThumbnail name =
	(
		local thumbnail = thumbnailPath + levelconfig.level + "\\" + name + ".bmp"
		
		if not (doesFileExist thumbnail) then
		(
			thumbnail = (thumbnailPath + "MissingDefult.bmp")
		)
		
		thumbnail
	),
	
	fn loadXmlDictionary XMLFile =
	(
		local result
		
		if (doesFileExist XMLFile) then
		(
			xmlStruct = XmlDocument()
			xmlStruct.init()
			xmlStruct.load XMLFile
			
			local maxFile = xmlStruct.document.DocumentElement.Attributes.Item[0].value
			
			result = RSL_Dictionary maxfile:maxFile xmlFile:XMLFile
			result.propArray.count = xmlStruct.document.DocumentElement.ChildNodes.Count
			
			for i = 0 to xmlStruct.document.DocumentElement.ChildNodes.Count - 1 do
			(
				local currentNode = xmlStruct.document.DocumentElement.ChildNodes.Item[i]
				local newProp = RSL_prop name:currentNode.Attributes.Item[0].value maxFile:maxFile thumbnail:(getPropThumbnail currentNode.Attributes.Item[0].value)
				local propertiesNode = currentNode.ChildNodes.Item[0]
				local attributesNode = currentNode.ChildNodes.Item[1]
				
				newProp.propertyArray.count = propertiesNode.ChildNodes.Count
				
				for p = 0 to propertiesNode.ChildNodes.Count - 1 do
				(
					local propertyNode = propertiesNode.ChildNodes.Item[p]
					local value = (propertyNode.attributes.ItemOf "value").value
					
					local data = #()
					data.count = propertyNode.ChildNodes.count
					
					for d = 0 to propertyNode.ChildNodes.count - 1 do 
					(
						data[d + 1] = (propertyNode.ChildNodes.Item[d].Attributes.ItemOf "data").value
					)
					
					local newProperty = RSL_propProperty name:propertyNode.name value:value data:data
					
					newProp.propertyArray[p + 1] = newProperty
				)
				
				newProp.attributeArray.count = attributesNode.ChildNodes.Count
				
				for a = 0 to attributesNode.ChildNodes.Count - 1 do
				(
					newProp.attributeArray[a + 1] = (attributesNode.ChildNodes.Item[a].Attributes.ItemOf "value").value
				)
				
				result.propArray[i + 1] = newProp
			)
		)
		
		result
	),
	
	fn saveXmlDictionary dictionary =
	(
		local result = false
		
		if not (doesFileExist dictionary.xmlFile) then
		(
			local newXML = createFile dictionary.xmlFile
			
			format "<?xml version=\"1.0\"?>\n<objects>\n</objects>" to:newXML
			
			flush newXML
			close newXML
		)
		
		local readOnly = getFileAttribute dictionary.xmlFile #readOnly
		
		if not readOnly then
		(
			local file = openFile dictionary.xmlFile mode:"w"
			format "<?xml version=\"1.0\"?>\n<objects>\n</objects>" to:file
			flush file
			close file
			
			xmlStruct = XmlDocument()
			xmlStruct.init()
			xmlStruct.load dictionary.xmlFile
			
			local attr = xmlStruct.createattribute "maxfile"
			attr.value = (substituteString dictionary.maxFile ".\payne" project.root)
			xmlStruct.document.DocumentElement.Attributes.Append (attr)
			
			for i = 1 to dictionary.propArray.count do
			(
				local currentProp = dictionary.propArray[i]
				
				local newObject = xmlStruct.createelement "prop" --(toLower object)
				local attr = xmlStruct.createattribute "name"
				attr.value = currentProp.name
				newObject.Attributes.Append (attr)
				
				local newProperties = xmlStruct.createelement (toLower "properties")
				
				for property in currentProp.propertyArray do
				(
					local newProperty = xmlStruct.createelement property.name
					
					local attr = xmlStruct.createattribute "value"
					attr.value = property.value as string
					newProperty.Attributes.Append (attr)
					
					for entry in property.data do
					(
						local newData = xmlStruct.createelement "data"
						
						attr = xmlStruct.createattribute "data"
						attr.value = entry as string
						newData.Attributes.Append (attr)
						
						newProperty.AppendChild (newData)
					)
					
					newProperties.AppendChild (newProperty)
				)
				
				local newAttributes = xmlStruct.createelement (toLower "attributes")
				
				for entry in currentProp.attributeArray do
				(
					local newAttribute = xmlStruct.createelement "attribute"
					
					local attr = xmlStruct.createattribute "value"
					attr.value = entry
					newAttribute.Attributes.Append (attr)
					
					newAttributes.AppendChild (newAttribute)
				)
				
				newObject.AppendChild (newProperties)
				newObject.AppendChild (newAttributes)
				
				xmlStruct.document.DocumentElement.AppendChild (newObject)
			)
			
			xmlStruct.save dictionary.xmlFile
			
			result = true
		)
		else
		(
			messageBox ("The Xml File: " + dictionary.xmlFile + " is Read Only. File has not been saved") title:"Save Error..."
		)
		
		result
	),
	
	fn loadLevelXmlDictionaries level =
	(
		local result = #()
		local dictionaryFiles = getFiles (dictionaryPath + level + "\\*.xml")
		result.count = dictionaryFiles.count
		
		for i = 1 to dictionaryFiles.count do
		(
			result[i] = (loadXmlDictionary dictionaryFiles[i])
		)
		
		result
	),
	
	fn indexOfCurrentDictionary dictionaryArray =
	(
		local result = 0
		
		for i = 1 to dictionaryArray.count do
		(
			if (getFilenameFile (toLower dictionaryArray[i].maxFile)) == (getFilenameFile (toLower maxFilename)) then
			(
				result = i
				
				exit
			)
		)
		
		result
	),
	
	fn createDictionaryFromScene =
	(
		local result = RSL_Dictionary maxFile:(maxFilePath + maxFilename) xmlFile:(dictionaryPath + levelConfig.level + "\\" + (getFilenameFile maxFilename) +".xml")
		local sceneRootNodes = for object in rootNode.children where (getAttrClass object) == "Gta Object" \
																		and (matchPattern object.name pattern:"*_frag_") == false \
																		and (matchPattern object.name pattern:"*_fragtemp_") == false \
																		and (matchPattern object.name pattern:"*_L") == false collect object
		
		result.propArray.count = sceneRootNodes.count
		
		for i = 1 to sceneRootNodes.count do
		(
			local object = sceneRootNodes[i]
			result.propArray[i] = RSL_Prop name:object.name object:object
			result.propArray[i].thumbnail = getPropThumbnail object.name
			result.propArray[i].getProperties()
		)
		
		result
	)
)
