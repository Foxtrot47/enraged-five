global init_RSL_TextureTagEdit

fileIn "pipeline/util/xml.ms"
filein (RsConfigGetWildWestDir() + "script/max/Rockstar_London/pipeline/RSL_LevelConfig.ms")
filein "pipeline/util/p4.ms"

struct RSL_TextureOutputEdit
(
	name,
	scale,
	filter
)

struct RSL_TextureTagEdit
(
	projectConfig = project.globalConfig,
	helpLink = "https://mp.rockstargames.com/index.php/Texture_Tagger",
	localConfigINI = (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/TextureTag.ini"),
	
	filterArray = #("A","B","C","D"),
	sizes = #("100.0%","50.0%","25.0%","12.5%"),
	rootPath = project.independent + ("\\intermediate\\levels\\"),
	XMLFile,
	xmlStruct,
	foundElement = undefined,
	foundAttribute = undefined,
	
	workPath = systemTools.getEnvVariable "RSG_ROOTDIR",
	
	listViewColumns = #("Name","Scale", "Filter"),
	
	scaleModified = true,
	--=-=-=-=-=-=-=-=-=-=-=-=-= dotNet control variables =-=-=-=-=-=-=-=-=-=-=-=-=--
	TextureTagEditForm,mainTableLayout,textureListView,
	
	processForm,progress1,
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= preset colours =-=-=-=-=-=-=-=-=-=-=-=-=--
	controlColour = (dotNetClass "System.Drawing.Color").Gainsboro,
	controlColour_dark2 = (dotNetClass"System.Drawing.SystemColors").ControlDarkDark,
	controlColour_dark1 = (dotNetClass"System.Drawing.Color").Silver,
	
	warningColour = (dotNetClass"System.Drawing.Color").fromARGB 226 167 98,
	errorColour = (dotNetClass"System.Drawing.Color").fromARGB 226 98 98,
	
	mouseOverColour = (dotNetClass"System.Drawing.Color").fromARGB 223 193 159,
	
	--=-=-=-=-=-=-=-=-=-=-=-=-= dotNet UI presets =-=-=-=-=-=-=-=-=-=-=-=-=--
	dockStyle = dotNetClass "System.Windows.Forms.DockStyle",
	formBorderStyle = dotNetClass "System.Windows.Forms.FormBorderStyle",
	columnStyle = dotNetClass "System.Windows.Forms.ColumnStyle",
	FlatStyle = dotNetClass "System.Windows.Forms.FlatStyle",
	sizeType = dotNetClass "System.Windows.Forms.SizeType",
	FontLarge = dotNetObject "System.Drawing.Font" "Trebuchet MS" 12 (dotNetClass "System.Drawing.FontStyle").bold,
	FontMedium = dotNetObject "System.Drawing.Font" "Trebuchet MS" 8.0 (dotNetClass "System.Drawing.FontStyle").bold,
	FontStandard = dotNetObject "System.Drawing.Font" "Small Fonts" 8.0 (dotNetClass "System.Drawing.FontStyle").Regular,
	FontSmall = dotNetObject "System.Drawing.Font" "Small Fonts" 6.0 (dotNetClass "System.Drawing.FontStyle").Regular,	--Regular --Italic
	listViewFont = dotNetObject "System.Drawing.Font" "Small Fonts" 8.0 (dotNetClass "System.Drawing.FontStyle").bold,
	decimalConverterClass = dotNetClass "RGLExportHelpers.DecimalConverter",
	
	fn loadProgress location:(getMAXWindowSize() / 2) =
	(
		processForm = dotNetObject "MaxCustomControls.MaxForm"
		
		processForm.Size = dotNetObject "System.Drawing.Size" 256 48
		processForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		processForm.FormBorderStyle = (dotNetClass "System.Windows.Forms.FormBorderStyle").FixedToolWindow
		processForm.Name = "loadDictionaries"
		processForm.Text = ("Loading Dictionaries")
		processForm.ShowInTaskbar = False
		processForm.UseWaitCursor = true
		
		progress1 = dotNetObject "progressBar"
		progress1.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		progress1.Style = (dotNetClass "System.Windows.Forms.ProgressBarStyle").Continuous
		progress1.BackColor = controlColour_dark2
		progress1.ForeColor = (DotNetClass "System.Drawing.Color").YellowGreen
		progress1.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		
		processForm.Controls.Add progress1
		
		processForm.ShowModeless()
		
		processForm.location.x = location.x - (processForm.width / 2)
		processForm.location.y = location.y - (processForm.height / 2)
		
		progress1.Value = 0.0
	),
	
	fn readLocalConfigINI =
	(
		local INIFile = getFiles localConfigINI
		
		if INIFile.count > 0 then
		(
			local formPosition = readValue ((getINISetting localConfigINI "Main" "Position") as stringStream)
			
			TextureTagEditForm.location.x = formPosition.x
			TextureTagEditForm.location.y = formPosition.y
		)
	),
	
	fn writeLocalConfigINI =
	(
		local INIFile = getFiles localConfigINI
		
		if INIFile.count == 0 then
		(
			local newFile = createFile localConfigINI
			close newFile
		)
		
		setINISetting localConfigINI "Main" "Position" ([TextureTagEditForm.location.x,TextureTagEditForm.location.y] as string)
	),
	
	fn assignIcon control file =
	(
		if file != undefined then
		(
			control.Icon = (dotNetObject "System.Drawing.Icon" file)
			
		)
	),	
	
	fn recurseElementNameSearch element searchString =
	(
		for i = 0 to element.ChildNodes.Count - 1 do
		(
			local currentNode = element.ChildNodes.Item[i]
			
			if currentNode.name == searchString then
			(
				foundElement = currentNode
			)
			else
			(
				recurseElementNameSearch currentNode searchString
			)
		)
	),
	
	fn recurseElementAtributeSearch element searchString =
	(
		for i = 0 to element.ChildNodes.Count - 1 do
		(
			local currentNode = element.ChildNodes.Item[i]
			
			if currentNode.name == "in" then
			(
				for a = 0 to currentNode.Attributes.count - 1 do
				(
					local currentAttribute = currentNode.Attributes.Item[a]
					
					if currentAttribute.value == searchString then
					(
						try
						(
							foundAttribute = currentNode.Attributes.Item[3].value
						)
						catch(foundAttribute = undefined)
					)
				)
			)
			
			if foundAttribute == undefined then
			(
				recurseElementAtributeSearch currentNode searchString
			)
		)
	),
	
	fn loadXML =
	(
		local result = true
		local map = getFilenameFile maxFilename --levelConfig.level
		
		if levelConfig.level != undefined then
		(
			makeDir (rootPath + levelConfig.level)
			
			XMLFile = rootPath + levelConfig.level + "\\" + map + "_TextureTag.xml"
			
			if (getFiles XMLFile).count == 1 then
			(
				local readOnly = getFileAttribute XMLFile #readOnly
				local okToContinue = true
				
				if readOnly then
				(
					okToContinue = queryBox "About to sync and check out. Ok?" title:"Warning..."
					
					if okToContinue then
					(
						--p4.sync(pathConfig.appendPath workPath (pathConfig.removePathTopParent project.independent)+"/intermediate/levels/...")
						--p4.edit(pathConfig.appendPath workPath (pathConfig.removePathTopParent project.independent)+"/intermediate/levels/" + levelConfig.level + "/" + (filenameFromPath XMLFile))
					
							------------------------------------------
							-- Source Control
							------------------------------------------
							local fileList = (project.intermediate+"/levels/" +levelConfig.level + "/" + (filenameFromPath XMLFile))							
							local p4 = RsPerforce()
							local depotFiles = #()
							p4.connect (project.intermediate)		
							depotFiles = p4.local2depot fileList
							p4.sync depotFiles
							p4.edit depotFiles
							p4.disconnect()
							
							------------------------------------------
							-- Source Control
							------------------------------------------
							
					)
				)
				
				if okToContinue then
				(
					xmlStruct = XmlDocument()
					xmlStruct.init()
					xmlStruct.load XMLFile
				)
			)
			else
			(
				local newXML = createFile XMLFile
				
				format "<?xml version=\"1.0\"?>\n<textures>\n</textures>" to:newXML
				
				flush newXML
				close newXML
				
				--p4.Add (pathConfig.appendPath workPath (pathConfig.removePathTopParent project.independent)+"/intermediate/levels/" + levelConfig.level + "/" + (filenameFromPath XMLFile))
				------------------------------------------
				-- Source Control
				------------------------------------------
				local fileList = (project.intermediate+"/levels/" + levelConfig.level + "/" + (filenameFromPath XMLFile))							
				local p4 = RsPerforce()
				local depotFiles = #()
				p4.connect (project.intermediate)		
				depotFiles = p4.local2depot fileList
				p4.add depotFiles
				p4.disconnect()
				------------------------------------------
				-- Source Control
				------------------------------------------
					
					
					
				xmlStruct = XmlDocument()
				xmlStruct.init()
				xmlStruct.load XMLFile
			)
		)
		else
		(
			result = false
			messageBox "This is not a level file" title:"Error..."
		)
		
		result
	),
	
	fn getScaleFromXML texture =
	(
		local result = "100.0%"
		
		for i = 0 to xmlStruct.document.DocumentElement.ChildNodes.Count - 1 do
		(
			local node = xmlStruct.document.DocumentElement.ChildNodes.Item[i]
			
			if texture == node.name then
			(
				result = (((readValue (node.Attributes.Item[0].value as stringStream)) * 100.0) as string) + "%"
			)
		)
		
		result
	),
	
	fn getFilterFromXML texture =
	(
		local result = filterArray[1]
		
		for i = 0 to xmlStruct.document.DocumentElement.ChildNodes.Count - 1 do
		(
			local node = xmlStruct.document.DocumentElement.ChildNodes.Item[i]
			
			if texture == node.name then
			(
				result = node.Attributes.Item[1].value
			)
		)
		
		result
	),
	
	fn populateTextureList =
	(
		loadXML()
		
		if xmlStruct != undefined then
		(
			loadProgress()
			
			textureListView.Items.Clear()
			textureListView.BeginUpdate()
			
			processForm.Text = "Creating Texture Entries..."
			progress1.Value = 0
			
			print xmlStruct.document.DocumentElement.name
			print xmlStruct.document.DocumentElement.ChildNodes.Count
			
			for i = 0 to xmlStruct.document.DocumentElement.ChildNodes.Count - 1 do
			(
				local currentNode = xmlStruct.document.DocumentElement.ChildNodes.Item[i]
				progress1.Value = (((i + 1) as float) / xmlStruct.document.DocumentElement.ChildNodes.Count) * 100.0
				processForm.Text = "Texture: " + currentNode.name
				
				local name = currentNode.name
				local scale = currentNode.Attributes.Item[0].value
				local scaleValue = ((scale as float) * 100.0) as string + "%"
				local filter = currentNode.Attributes.Item[1].value
				
				local listItem = dotNetObject "System.Windows.Forms.ListViewItem" name
				listItem.UseItemStyleForSubItems = false
				listItem.Tag = dotNetMXSValue (RSL_TextureOutputEdit name:name scale:scale filter:filter)
				listItem.SubItems.add scaleValue
				listItem.SubItems.add filter
				
				textureListView.Items.Add listItem
			)
			
			textureListView.EndUpdate()
			
			processForm.Close()
		)
		else
		(
			print "Failed to load XML"
		)
	),
	
	fn compareScale v1 v2 =
	(
		local result = 0
		
		local input1 = readValue ((substituteString v1.subItems.Item[(findItem listViewColumns "Scale") - 1].Text "%" "") as stringStream)
		local input2 = readValue ((substituteString v2.subItems.Item[(findItem listViewColumns "Scale") - 1].Text "%" "") as stringStream)
		
		result = input1 - input2
		
		case of
		(
			(result < 0.0): 1
			(result > 0.0): -1
			default:0
		)
	),
	
	fn compareFilter v1 v2 =
	(
		local input1 = v1.subItems.Item[(findItem listViewColumns "Filter") - 1].Text
		local input2 = v2.subItems.Item[(findItem listViewColumns "Filter") - 1].Text
		
		case of
		(
			(input1 < input2): -1
			(input1 > input2): 1
			default:0
		)
	),
	
	fn updateXML texture scale filter =
	(
		local found = false
		
		if xmlStruct != undefined then
		(
			for i = 0 to xmlStruct.document.DocumentElement.ChildNodes.Count - 1 do
			(
				local node = xmlStruct.document.DocumentElement.ChildNodes.Item[i]
				if (toLower texture) == (toLower node.name) then
				(
					found = true
					
					xmlStruct.document.DocumentElement.ChildNodes.Item[i].Attributes.Item[0].value = scale as string
					xmlStruct.document.DocumentElement.ChildNodes.Item[i].Attributes.Item[1].value = filter as string
					xmlStruct.save XMLFile
				)
			)
			
			if not found then
			(
				local newItem = xmlStruct.createelement (toLower texture)
				local attr = xmlStruct.createattribute "size"
				attr.value = (scale as string)
				newItem.Attributes.Append (attr)
				
				attr = xmlStruct.createattribute "filter"
				attr.value = (filter as string)
				newItem.Attributes.Append (attr)
				
				xmlStruct.document.DocumentElement.AppendChild (newItem)
				
				xmlStruct.save XMLFile
			)
		)
	),
	
	
	
	
	
	
	
	
	
	
	
	fn dispatchOnFormClosing s e =
	(
		init_RSL_TextureTagEdit.onFormClosing s e
	),
	
	fn onFormClosing s e =
	(
		writeLocalConfigINI()
		
		gc()
	),
	
	fn dispatchTextureListViewIndexChanged s e =
	(
		init_RSL_TextureTagEdit.TextureListViewIndexChanged s e
	),
	
	fn TextureListViewIndexChanged s e =
	(
		if s.SelectedIndices.count > 0 then
		(
			
		)
	),
	
	fn dispatchOntoolStripSortComboChanged s e =
	(
		init_RSL_TextureTagEdit.toolStripSortComboChanged s e
	),
	
	fn toolStripSortComboChanged s e =
	(
		textureListView.Sorting = (dotNetClass "System.Windows.Forms.SortOrder").None
		
		listItems = #()
		listItems.Count = textureListView.Items.Count
		
		for i = 0 to textureListView.Items.Count - 1 do
		(
			listItems[i + 1] = textureListView.Items.Item[i]
		)
		
		textureListView.Items.Clear()
		
		case s.Text of
		(
			default:print s.Text
			"Name":
			(
				textureListView.Sorting = (dotNetClass "System.Windows.Forms.SortOrder").Ascending
				
				textureListView.Items.AddRange listItems
			)
			"Scale":
			(
				qSort listItems compareScale
				
				textureListView.Items.AddRange listItems
			)
			"Filter":
			(
				qSort listItems compareFilter
				
				textureListView.Items.AddRange listItems
			)
		)
	),
	
	fn dispatchOnScaleComboChanged s e =
	(
		init_RSL_TextureTagEdit.ScaleComboChanged s e
	),
	
	fn ScaleComboChanged s e =
	(
		if scaleModified then
		(
			if textureListView.SelectedItems.count > 0 then
			(
				loadProgress location:[TextureTagEditForm.location.x + (TextureTagEditForm.width / 2), TextureTagEditForm.location.y + (TextureTagEditForm.height / 2)]
				processForm.Text = "Changing Scale..."
				
				for i = 0 to textureListView.SelectedItems.count - 1 do
				(
					try (progress1.Value = (((i + 1) as float) / textureListView.SelectedItems.count) * 100.0) catch()
					
					local texture = textureListView.SelectedItems.Item[i].tag.value.name
					local value = (readValue ((substituteString s.Text "%" "") as stringStream)) / 100.0
					local filter = textureListView.SelectedItems.Item[i].subItems.Item[(findItem listViewColumns "Filter") - 1].text
					
					updateXML texture value filter
					
					textureListView.SelectedItems.Item[i].SubItems.Item[(findItem listViewColumns "Scale") - 1].Text = s.Text
				)
				
				processForm.Close()
			)
		)
	),
	
	fn dispatchOnhelpButtonPressed s e =
	(
		init_RSL_TextureTagEdit.helpButtonPressed s e
	),
	
	fn helpButtonPressed s e =
	(
		ShellLaunch helpLink ""
	),
	
	fn dispatchOnScaleTypeChanged s e =
	(
		init_RSL_TextureTagEdit.ScaleTypeChanged s e
	),
	
	fn ScaleTypeChanged s e =
	(
		if scaleModified then
		(
			loadProgress location:[TextureTagEditForm.location.x + (TextureTagEditForm.width / 2), TextureTagEditForm.location.y + (TextureTagEditForm.height / 2)]
			processForm.Text = "Changing Scale..."
			
			for i = 0 to textureListView.Items.count - 1 do
			(
				try (progress1.Value = (((i + 1) as float) / textureListView.Items.count) * 100.0) catch()
				
				local currentItem = textureListView.Items.Item[i]
				local filename = getFilenameFile currentItem.tag.value.filename
				local isType = false
				
				for entry in s.tag.value do
				(
					if (matchPattern filename pattern:entry) then
					(
						isType = true
					)
				)
				
				if isType then
				(
					local texture = currentItem.tag.value.name
					local value = (readValue ((substituteString s.Text "%" "") as stringStream)) / 100.0
					local filter = currentItem.subItems.Item[(findItem listViewColumns "Filter") - 1].text
					
					updateXML texture value filter
					
					currentItem.SubItems.Item[(findItem listViewColumns "Scale") - 1].Text = s.Text
				)
			)
			
			processForm.Close()
		)
	),
	
	fn dispatchFilterTypeChanged s e =
	(
		init_RSL_TextureTagEdit.FilterTypeChanged s e
	),

	fn FilterTypeChanged s e =
	(
		loadProgress location:[TextureTagEditForm.location.x + (TextureTagEditForm.width / 2), TextureTagEditForm.location.y + (TextureTagEditForm.height / 2)]

		processForm.Text = "Changing Filter..."
		
		if s.tag != undefined then
		(
			for i = 0 to textureListView.Items.count - 1 do
			(
				try (progress1.Value = (((i + 1) as float) / textureListView.Items.count) * 100.0) catch()
				
				local currentItem = textureListView.Items.Item[i]
				local filename = getFilenameFile currentItem.tag.value.filename
				local isType = false
				
				for entry in s.tag.value do
				(
					if (matchPattern filename pattern:entry) then
					(
						isType = true
					)
				)
				
				if isType then
				(
					local alpha = (getFilenameFile currentItem.tag.value.alpha)
					if alpha == "none"then
					(
						alpha = ""
					)
					
					local texture = currentItem.tag.value.name + alpha
					local value = (readValue ((substituteString currentItem.subItems.Item[(findItem listViewColumns "Scale") - 1].text "%" "") as stringStream)) / 100.0
					local filter = s.Text
					
					updateXML texture value filter
					
					currentItem.SubItems.Item[(findItem listViewColumns "Filter") - 1].Text = s.Text
				)
			)
		)
		else
		(
			if textureListView.SelectedItems.count > 0 then
			(
				for i = 0 to textureListView.SelectedItems.count - 1 do
				(
					try (progress1.Value = (((i + 1) as float) / textureListView.SelectedItems.count) * 100.0) catch()
					
					local alpha = (getFilenameFile textureListView.SelectedItems.Item[i].tag.value.alpha)
					if alpha == "none"then
					(
						alpha = ""
					)
					
					local texture = textureListView.SelectedItems.Item[i].tag.value.name + alpha
					local value = (readValue ((substituteString textureListView.SelectedItems.Item[i].SubItems.Item[(findItem listViewColumns "Scale") - 1].Text "%" "") as stringStream)) / 100.0
					local filter = s.Text
					
					updateXML texture value filter
					
					textureListView.SelectedItems.Item[i].SubItems.Item[(findItem listViewColumns "Filter") - 1].Text = s.Text
				)
			)
		)
		
		processForm.Close()
	),
	
	fn CreateDropDownFilterItems menu eventHandler tag:undefined =
	(
		local filterMenu = dotNetObject "ToolStripMenuItem"
		filterMenu.text = "Filters..."
		
		filterMenu.enabled = false --THIS NEEDS REMOVING ONCE WE HAVE THE PROPER FILTER NAMES
		
		menu.DropDownItems.addRange #(filterMenu, (dotNetObject "ToolStripSeparator"))
		
		for entry in filterArray do
		(
			local filterItem = dotNetObject "ToolStripMenuItem"
			filterItem.text = entry
			
			if tag != undefined then
			(
				filterItem.tag = dotNetMXSValue tag
			)
			
			dotnet.addEventHandler filterItem "Click" eventHandler
			
			filterMenu.DropDownItems.Add filterItem
		)
	),
	
	fn CreateDropDownSizeItems menu eventHandler tag:undefined =
	(
		for entry in sizes do
		(
			local sizeMenu = dotNetObject "ToolStripMenuItem"
			sizeMenu.text = entry
			
			if tag != undefined then
			(
				sizeMenu.tag = dotNetMXSValue tag
			)
			
			dotnet.addEventHandler sizeMenu "Click" eventHandler
			
			menu.DropDownItems.Add sizeMenu
		)
	),
	
	fn dispatchOnSelectAllPressed s e =
	(
		init_RSL_TextureTagEdit.selectAllPressed s e
	),
	
	fn selectAllPressed s e =
	(
		for i = 0 to textureListView.Items.count - 1 do
		(
			textureListView.Items.Item[i].Selected = true
		)
	),
	
	fn dispatchOnResetPressed s e =
	(
		init_RSL_TextureTagEdit.ResetPressed s e
	),
	
	fn ResetPressed s e =
	(
		loadProgress location:[TextureTagEditForm.location.x + (TextureTagEditForm.width / 2), TextureTagEditForm.location.y + (TextureTagEditForm.height / 2)]
		processForm.Text = "Reseting All Textures to default..."
		
		for i = 0 to textureListView.Items.count - 1 do
		(
			try (progress1.Value = (((i + 1) as float) / textureListView.Items.count) * 100.0) catch()
			
			local currentItem = textureListView.Items.Item[i]
			
			local texture = currentItem.tag.value.name
			local value = (readValue ((substituteString sizes[1] "%" "") as stringStream)) / 100.0
			local filter = filterArray[1]
			
			updateXML texture value filter
			
			currentItem.SubItems.Item[(findItem listViewColumns "Scale") - 1].Text = sizes[1]
			currentItem.SubItems.Item[(findItem listViewColumns "Filter") - 1].Text = filterArray[1]
		)
		
		processForm.Close()
	),
	
	fn createForm = 
	(
		clearListener()
		
		TextureTagEditForm = dotNetObject "MaxCustomControls.MaxForm"
		TextureTagEditForm.Size = dotNetObject "System.Drawing.Size" 768 768
		TextureTagEditForm.MinimumSize = dotNetObject "System.Drawing.Size" 212 64
		TextureTagEditForm.FormBorderStyle = formBorderStyle.Sizable
		TextureTagEditForm.Name = "TextureTagEditForm"
		TextureTagEditForm.Text = (" R* Texture Tag Edit")
		TextureTagEditForm.ShowInTaskbar = False
		TextureTagEditForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		assignIcon TextureTagEditForm (RsConfigGetWildWestDir() + "script/max/Rockstar_London/images/TextureBrowseIconRS.ico")
		
		dotnet.addEventHandler TextureTagEditForm "FormClosing" dispatchOnFormClosing
		
		mainTableLayout = dotNetObject "TableLayoutPanel"
		mainTableLayout.ColumnCount = 1
		mainTableLayout.RowCount = 2
		mainTableLayout.Dock = dockStyle.Fill
		mainTableLayout.Margin = dotNetObject "System.Windows.Forms.Padding" 0
		mainTableLayout.CellBorderStyle = (dotNetClass "TableLayoutPanelCellBorderStyle").Single
		mainTableLayout.ColumnStyles.add (dotNetObject "System.Windows.Forms.ColumnStyle" sizeType.percent 100)
		mainTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.Absolute 24)
		mainTableLayout.RowStyles.add (dotNetObject "System.Windows.Forms.RowStyle" sizeType.percent 64)
		mainTableLayout.AutoSize = true
		
		toolStripSortLabel = dotNetObject "ToolStripLabel"
		toolStripSortLabel.name = "toolStripSortLabel"
		toolStripSortLabel.Text = toUpper "Sort By:"
		toolStripSortLabel.Font = FontSmall
		
		toolStripSortCombo = dotNetObject "ToolStripComboBox"
		toolStripSortCombo.name = "toolStripSortCombo"
		toolStripSortCombo.Text = listViewColumns[1]
		toolStripSortCombo.MaxDropDownItems = (listViewColumns.count)
		toolStripSortCombo.Items.AddRange (listViewColumns)
		
		dotnet.addEventHandler toolStripSortCombo "SelectedIndexChanged" dispatchOntoolStripSortComboChanged
		
		helpButton = dotNetObject "ToolStripButton"
		helpButton.Text = toUpper "Help"
		helpButton.ToolTipText = toUpper "Opens the help page on the RockstarGames wiki"
		helpButton.Font = FontSmall
		helpButton.CheckOnClick = false
		helpButton.Checked = false
		helpButton.DisplayStyle = (dotNetClass "System.Windows.Forms.ToolStripItemDisplayStyle").Text
		
		dotnet.addEventHandler helpButton "Click" dispatchOnhelpButtonPressed
		
		mainToolStrip = dotNetObject "ToolStrip"
		mainToolStrip.name = "Main Tool Strip"
		mainToolStrip.text = "Main Tool Strip"
		mainToolStrip.BackColor = controlColour
		mainToolStrip.GripStyle = (dotNetClass "System.Windows.Forms.ToolStripGripStyle").Hidden
		mainToolStrip.Items.AddRange #(toolStripSortLabel,toolStripSortCombo,helpButton)
		
		selectedMenu = dotNetObject "ToolStripMenuItem"
		selectedMenu.text = "Selected Items"
		
		CreateDropDownFilterItems selectedMenu dispatchFilterTypeChanged
		CreateDropDownSizeItems selectedMenu dispatchOnScaleComboChanged
		
		selectAllMenu = dotNetObject "ToolStripMenuItem"
		selectAllMenu.text = "Select All..."
		
		dotnet.addEventHandler selectAllMenu "Click" dispatchOnSelectAllPressed
		
		resetMenu = dotNetObject "ToolStripMenuItem"
		resetMenu.text = "Reset All..."
		
		dotnet.addEventHandler resetMenu "Click" dispatchOnResetPressed
		
		listViewMenu = dotNetObject "ContextMenuStrip"
		listViewMenu.RenderMode = (dotNetClass "System.Windows.Forms.ToolStripRenderMode").Professional
		listViewMenu.Items.AddRange #(selectedMenu, (dotNetObject "ToolStripSeparator"), selectAllMenu, resetMenu)
		
		textureListView = dotNetObject "ListView"
		textureListView.Dock = dockStyle.Fill
		textureListView.BackColor = controlColour_dark1
		textureListView.Sorting = (dotNetClass "System.Windows.Forms.SortOrder").Ascending
		textureListView.BorderStyle = (dotNetClass "System.Windows.Forms.BorderStyle").FixedSingle
		textureListView.view = (dotNetClass "System.Windows.Forms.View").Details
		textureListView.fullRowSelect = true
		textureListView.HideSelection = false
		textureListView.HeaderStyle = (dotNetClass "System.Windows.Forms.ColumnHeaderStyle").Nonclickable
		textureListView.ContextMenuStrip = listViewMenu
		
		dotnet.addEventHandler textureListView "SelectedIndexChanged" dispatchTextureListViewIndexChanged
		
		for column in listViewColumns do
		(
			textureListView.Columns.Add column -1
		)
		
		populateTextureList()
		
		toolStripScaleLabel = dotNetObject "ToolStripLabel"
		toolStripScaleLabel.name = "toolStripScaleLabel"
		toolStripScaleLabel.Text = toUpper "Scale:"
		toolStripScaleLabel.Font = FontSmall
		toolStripScaleLabel.Alignment = (dotNetClass "System.Windows.Forms.ToolStripItemAlignment").Right
		
		scaleCombo = dotNetObject "ToolStripComboBox"
		scaleCombo.name = "scaleCombo"
		scaleCombo.Text = sizes[1]
		scaleCombo.Items.AddRange sizes
		scaleCombo.Alignment = (dotNetClass "System.Windows.Forms.ToolStripItemAlignment").Right
		
		dotnet.addEventHandler scaleCombo "SelectedIndexChanged" dispatchOnScaleComboChanged
		
		scaleToolStrip = dotNetObject "ToolStrip"
		scaleToolStrip.name = "Main Tool Strip"
		scaleToolStrip.text = "Main Tool Strip"
		scaleToolStrip.BackColor = controlColour
		scaleToolStrip.GripStyle = (dotNetClass "System.Windows.Forms.ToolStripGripStyle").Hidden
		scaleToolStrip.Items.AddRange #(scaleCombo,toolStripScaleLabel)
		
		mainTableLayout.Controls.Add mainToolStrip 0 0
		mainTableLayout.Controls.Add textureListView 0 1
		
		textureCount = dotNetObject "ToolStripStatusLabel"
		textureCount.text = "Textures: " + textureListView.Items.Count as string
		textureCount.BackColor = controlColour
		
		statusStrip = dotNetObject "StatusStrip"
		statusStrip.Items.AddRange #(textureCount)
		statusStrip.BackColor = controlColour
		
		TextureTagEditForm.Controls.Add mainTableLayout
		TextureTagEditForm.Controls.Add statusStrip
		
		readLocalConfigINI()
		
		TextureTagEditForm.ShowModeless()
		
		TextureTagEditForm
	)
)

if TextureTagEdit_Form != undefined then
(
	TextureTagEdit_Form.Close()
	try (init_RSL_TextureTagEdit.reportForm.Close()) catch()
)

init_RSL_TextureTagEdit = RSL_TextureTagEdit()

TextureTagEdit_Form = init_RSL_TextureTagEdit.createForm()	
	
	