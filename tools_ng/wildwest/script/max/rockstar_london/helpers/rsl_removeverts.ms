
try (destroyDialog RSL_RemoveVerts) catch()

rollout RSL_RemoveVerts "Remove Verts"
(
	local faceDataChannel
	local RSL_meshCleanIsSelected = #(1699710735, -200361706)
	local localConfigINI = (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_RemoveVerts.ini")
	
	checkBox chkb_selected "Selected Elements" checked:true offset:[-8,0]
	label lbl_edgeThreshold "Edge Threshold:" across:2 offset:[2,0]
	spinner spn_edgeThreshold range:[0.0, 1.0, 0.99] scale:0.01 fieldWidth:40 offset:[8,0]
	button btn_vertRemove "Remove Verts" width:152 offset:[-1,0]
	progressBar pbar_progress value:100.0 color:orange width:154 offset:[-9,0]
	
	fn isSimilar face1 face2 object =
	(
		local vertArray1 = (polyOp.getVertsUsingFace face1 object) as array
		local vertArray2 = (polyOp.getVertsUsingFace face2 object) as array
	)
	
	mapped fn setSelectedFace face theObject =
	(
		faceDataChannel.setValue face (findItem theObject.selectedFaces face > 0)
	)
	
	mapped fn getSelectedFace face &outArray =
	(
		if (faceDataChannel.getValue face) then
		(
			append outArray face
		)
	)
	
	fn setFaceDataForSelected theObject =
	(
		try
		(
			simpleFaceManager.removeChannel theObject RSL_meshCleanIsSelected
		)
		catch()
		
		faceDataChannel = simpleFaceManager.addChannel theObject type:#boolean id:RSL_meshCleanIsSelected name:"RSL_RemoveVerts is Selected Channel"
		
		if faceDataChannel != undefined then
		(
			setSelectedFace (for face in theObject.faces collect face.index) theObject
		)
	)
	
	fn vertRemove vertexArray theObject =
	(
		local removeArray = #()
		
		for i = 1 to vertexArray.count do
		(
			local vert = vertexArray[i]
			
			pbar_progress.value = (i as float / vertexArray.count * 100.0)
			
			local edgeArray = (polyOp.getEdgesUsingVert theObject vert) as array
			
			if edgeArray.count == 2 then
			(
				local vertIndexes = makeUniqueArray (((polyOp.getVertsUsingEdge theObject edgeArray[1]) as array) + ((polyOp.getVertsUsingEdge theObject edgeArray[2]) as array))
				
				if vertIndexes.count == 3 then
				(
					local secondVert1
					local secondVert2
					
					for index in vertIndexes do
					(
						if index != vert then
						(
							if secondVert1 == undefined then
							(
								secondVert1 = index
							)
							else
							(
								secondVert2 = index
							)
						)
					)
					
					local vector1 = normalize (theObject.verts[secondVert1].pos - theObject.verts[vert].pos)
					local vector2 = normalize ( theObject.verts[secondVert2].pos - theObject.verts[vert].pos)
					
					local difference = (dot vector1 vector2) < -spn_edgeThreshold.value
					
					if difference then
					(
						append removeArray vert
					)
				)
				else
				(
					append removeArray vert
				)
			)
		)
		
		polyOp.setVertSelection theObject removeArray
		theObject.EditablePoly.remove selLevel:#Vertex
		
		subObjectLevel = 5
		
		polyop.retriangulate theObject #all
	)
	
	on RSL_RemoveVerts open do
	(
		RSL_INIOps.readLocalConfigINIStdRollout RSL_RemoveVerts localConfigINI
	)
	
	on RSL_RemoveVerts close do
	(
		RSL_INIOps.writeLocalConfigINIStdRollout RSL_RemoveVerts localConfigINI
	)
	
	on btn_vertRemove pressed do
	(
		pbar_progress.value = 0
		
		undo "Vertex Remove" on
		(
			theObject = selection[1]
			
			if theObject != undefined then
			(
				if (classOf theObject) == editable_poly then
				(
					setFaceDataForSelected theObject
					
					local vertArray = #()
					
					if chkb_selected.checked then
					(
						vertArray = makeUniqueArray ((polyOp.getVertsUsingFace theObject theObject.selectedFaces) as array)
					)
					else
					(
						vertArray = makeUniqueArray ((polyOp.getVertsUsingFace theObject theObject.faces) as array)
					)
					
					vertRemove vertArray theObject
					
					simpleFaceManager.removeChannel theObject RSL_meshCleanIsSelected
				)
				else
				(
					messageBox "The object is not an editable poly." title:"Error..."
				)
			)
		)
	)
)

positionINISetting = (getINISetting (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_RemoveVerts.ini") "Main" "Position")
dialogPos = undefined

if positionINISetting != "" then
(
	local dialogPosition = readValue (positionINISetting as stringStream)
	
	dialogPos = dialogPosition
)

if dialogPos != undefined then
(
	createDialog RSL_RemoveVerts pos:dialogPos style:#(#style_toolwindow,#style_sysmenu)
)
else
(
	createDialog RSL_RemoveVerts style:#(#style_toolwindow,#style_sysmenu)
)

positionINISetting = undefined
dialogPos = undefined
