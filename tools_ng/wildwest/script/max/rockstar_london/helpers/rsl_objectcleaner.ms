
filein (RsConfigGetWildWestDir()+"script/max/rockstar_london/utils/RSL_INIOperations.ms")

try (destroyDialog RSL_ObjectCleaner) catch()

rollout RSL_ObjectCleaner "R* Object Cleaner" width:120
(
	local newMeshArray = #()
	local localConfigINI = (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_ObjectCleaner.ini")
	
	group "Pivot"
	(
		checkBox chkb_pivotObject "Same"
		checkBox chkb_pivotParent "Parent"
		checkBox chkb_pivotCenter "Center" checked:true
		checkBox chkb_pivotSCenter "Selected Center"
		checkBox chkb_pivotBase "Base"
	)
	
	on chkb_pivotObject changed state do
	(
		chkb_pivotObject.checked = true
		
		chkb_pivotParent.checked = false
		chkb_pivotCenter.checked = false
		chkb_pivotSCenter.checked = false
		chkb_pivotBase.checked = false
	)
	on chkb_pivotParent changed state do
	(
		chkb_pivotParent.checked = true
		
		chkb_pivotObject.checked = false
		chkb_pivotCenter.checked = false
		chkb_pivotSCenter.checked = false
		chkb_pivotBase.checked = false
	)
	on chkb_pivotCenter changed state do
	(
		chkb_pivotCenter.checked = true
		
		chkb_pivotObject.checked = false
		chkb_pivotParent.checked = false
		chkb_pivotSCenter.checked = false
		chkb_pivotBase.checked = false
	)
	on chkb_pivotSCenter changed state do
	(
		chkb_pivotSCenter.checked = true
		
		chkb_pivotObject.checked = false
		chkb_pivotParent.checked = false
		chkb_pivotBase.checked = false
		chkb_pivotCenter.checked = false
	)
	on chkb_pivotBase changed state do
	(
		chkb_pivotBase.checked = true
		
		chkb_pivotObject.checked = false
		chkb_pivotParent.checked = false
		chkb_pivotSCenter.checked = false
		chkb_pivotCenter.checked = false
	)
	
	group "Keep"
	(
		checkBox chkb_attr "Attributes" checked:true
		checkBox chkb_user "User Props" checked:true
		checkBox chkb_material "Materials" checked:true
		checkBox chkb_parent "Parent" checked:true
		checkBox chkb_children "Children" checked:true
		checkBox chkb_animation "Animation" checked:false enabled:false
	)
	
	button btn_clean "Clean Object" width:110
	
	fn getCenter object =
	(
		local center = [0,0,0]
		
		for face in object.faces do
		(
			center += polyOp.GetFaceCenter object face.index
		)
		
		center /= object.faces.count
	)
	
	mapped fn cleanObject object =
	(
		convertTo object editable_poly
		
		local attrArray = for i = 1 to (GetNumAttr "Gta Object") collect (getAttr object i)
		
		if chkb_pivotParent.checked and object.parent != undefined then
		(
			object.pivot = object.parent.pivot
		)
		
		if chkb_pivotCenter.checked then
		(
			object.pivot = object.center --(getCenter object)
		)
		
		if chkb_pivotSCenter.checked then
		(
			local indexArray = (polyOp.getVertsUsingFace object object.selectedFaces) as array
			
			if indexArray.count > 0 then
			(
				local center = [0,0,0]
				
				for index in indexArray do
				(
					center += (polyop.getVert object index)
				)
				
				center /= indexArray.count
				
				object.pivot = center
			)
			else
			(
				object.pivot = object.center --(getCenter object)
			)
		)
		
		if chkb_pivotBase.checked then
		(
			object.pivot = object.center
			object.pivot.z = object.min.z
		)
		
		resetXform object
		convertToMesh object
		
		local posController = object.position.controller
		local rotController = object.rotation.controller
		
		local newMesh = box pos:object.pos name:object.name wireColor:object.wireColor
		convertTo newMesh editable_mesh
		newMesh.mesh = object.mesh
		
		convertTo newMesh editable_poly
		
		if chkb_attr.checked then
		(
			for i = 1 to attrArray.count do setAttr newMesh i attrArray[i]
		)
		
		if chkb_user.checked then
		(
			setUserPropBuffer newMesh (getUserPropBuffer object)
		)
		
		if chkb_material.checked then
		(
			newMesh.material = object.material
		)
		
		if chkb_parent.checked then
		(
			newMesh.parent = object.parent
		)
		
		if chkb_children.checked then
		(
			for child in object.children do
			(
				child.parent = newMesh
			)
		)
		
		if chkb_animation.checked then
		(
			newMesh.position.controller = posController
			newMesh.rotation.controller = rotController
		)
		
		append newMeshArray newMesh
		
		delete object
	)
	
	fn compareNames v1 v2 =
	(
		local number1
		local number2
		
		case of
		(
			(v1.name > v2.name):1
			(v1.name < v2.name):-1
			default:0
		)
	)
	
	on RSL_ObjectCleaner open do
	(
		RSL_INIOps.readLocalConfigINIStdRollout RSL_ObjectCleaner localConfigINI
	)
	
	on RSL_ObjectCleaner close do
	(
		RSL_INIOps.writeLocalConfigINIStdRollout RSL_ObjectCleaner localConfigINI
	)
	
	on btn_clean pressed do
	(
		local objectArray = selection as array
		--qsort objectArray compareNames
		--print objectArray
		
		newMeshArray = #()
		
		if objectArray.count > 0 then
		(
			local currentPanel = getCommandPanelTaskMode()
			
			if currentPanel == #modify then
			(
				setCommandPanelTaskMode #create
			)
			
			cleanObject objectArray
			
			setCommandPanelTaskMode currentPanel
			
			select newMeshArray
		)
	)
)

positionINISetting = (getINISetting (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_ObjectCleaner.ini") "Main" "Position")
dialogPos = undefined

if positionINISetting != "" then
(
	local dialogPosition = readValue (positionINISetting as stringStream)
	
	dialogPos = dialogPosition
)

if dialogPos != undefined then
(
	createDialog RSL_ObjectCleaner pos:dialogPos style:#(#style_toolwindow,#style_sysmenu)
)
else
(
	createDialog RSL_ObjectCleaner style:#(#style_toolwindow,#style_sysmenu)
)

positionINISetting = undefined
dialogPos = undefined

