
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/Utils/RSL_INIOperations.ms")

try (destroyDialog RSL_ExplodeElements) catch()

rollout RSL_ExplodeElements "R* Explode Elements" width:170
(
	local theObject
	local localConfigINI = (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_ExplodeElements.ini")
	
	checkBox chkb_parent "Keep Parent" width:160 checked:true offset:[-8,0]
	checkBox chkb_center "Center Pivot" width:160 checked:true offset:[-8,0]
	EditText edt_prefix "Prefix" width:160 labelOnTop:true offset:[-8,0]
	checkBox chkb_group "Group Exploded Nodes" width:160 checked:false offset:[-8,0]
	EditText edt_group "Group Name" width:160 labelOnTop:true enabled:false offset:[-8,0]
	button btn_explode "Explode" width:160 enabled:false 
	
	fn explode =
	(
		local explodedNodeArray = #()
		local count = 1
		
		suspendEditing()
		
		while theObject.faces.count > 0 do
		(
			local stringCount = ""
			if count < 10 then
			(
				stringCount += "0"
			)
			
			stringCount += (count as string)
			
			local faceArray = polyOp.getElementsUsingFace theObject theObject.faces[1]
			
			polyop.detachFaces theObject faceArray delete:true asNode:true name:"RSL_temporaryExplodedNode"
			
			local newNode = getNodeByName "RSL_temporaryExplodedNode" exact:true
			newNode.name = uniqueName edt_prefix.text --+ stringCount
			
			if chkb_center.checked then
			(
				newNode.pivot = newNode.center
				resetXform newNode
				convertToPoly newNode
			)
			
			if chkb_parent.checked and theObject.parent != undefined then
			(
				newNode.parent = theObject.parent
			)
			
			append explodedNodeArray newNode
			
			count += 1
		)
		
		if explodedNodeArray.count > 0 then
		(
			delete theObject
		)
		
		if chkb_group.checked then
		(
			group explodedNodeArray name:edt_group.text select:true
		)
		else
		(
			select explodedNodeArray
		)
		
		resumeEditing()
	)
	
	fn cb_selectionChanged =
	(
		theObject = selection[1]
		
		if theObject != undefined then
		(
			if (classOf theObject) == editable_poly then
			(
				btn_explode.enabled = true
				
				edt_prefix.text = theObject.name + "_f"
				
				if chkb_group.checked then
				(
					edt_group.text = edt_prefix.text + "_group"
				)
				else
				(
					edt_group.text = ""
				)
			)
			else
			(
				theObject = undefined
				
				btn_explode.enabled = false
				edt_prefix.text = "Non editable poly"
			)
		)
		else
		(
			edt_prefix.text = "No object selected"
		)
	)
	
	on RSL_ExplodeElements open do
	(
		RSL_INIOps.readLocalConfigINIStdRollout RSL_ExplodeElements localConfigINI
		
		cb_selectionChanged()
		callbacks.addScript #selectionSetChanged "RSL_ExplodeElements.cb_selectionChanged()" id:#RSL_ExplodeElementsCallBacks
	)
	
	on RSL_ExplodeElements close do
	(
		RSL_INIOps.writeLocalConfigINIStdRollout RSL_ExplodeElements localConfigINI
		
		callbacks.removeScripts id:#RSL_ExplodeElementsCallBacks
	)
	
	on edt_prefix entered txt do
	(
		btn_explode.enabled = (txt.count != 0)
	)
	
	on chkb_group changed state do
	(
		edt_group.enabled = state
		
		if state then
		(
			edt_group.text = edt_prefix.text + "_group"
		)
		else
		(
			edt_group.text = ""
			
			btn_explode.enabled = (edt_prefix.text.count != 0) and theObject != undefined and (classOf theObject) == editable_poly
		)
	)
	
	on edt_group entered txt do
	(
		if chkb_group.checked then
		(
			btn_explode.enabled = (txt.count != 0)
		)
	)
	
	on btn_explode pressed do
	(
		undo "Explode Elements" on
		(
			explode()
		)
	)
)

positionINISetting = (getINISetting (scriptsPath + "Payne\\helpers\\RSL_ExplodeElements.ini") "Main" "Position")
dialogPos = undefined

if positionINISetting != "" then
(
	local dialogPosition = readValue (positionINISetting as stringStream)
	
	dialogPos = dialogPosition
)

if dialogPos != undefined then
(
	createDialog RSL_ExplodeElements pos:dialogPos style:#(#style_toolwindow,#style_sysmenu)
)
else
(
	createDialog RSL_ExplodeElements style:#(#style_toolwindow,#style_sysmenu)
)

positionINISetting = undefined
dialogPos = undefined
