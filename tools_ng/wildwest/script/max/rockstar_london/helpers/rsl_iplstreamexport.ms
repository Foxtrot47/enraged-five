try (destroyDialog RSL_IPLStream) catch ()
try (destroyDialog RSL_IPLStream.IPLStream_PickMap) catch ()
try (destroyDialog RSL_IPLStream.IPLStream_PickXRef) catch ()
try (destroyDialog RSL_IPLStream.RSL_IPLStream_AddLink) catch ()

filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_IPLStreamingBoxManip.ms")
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_IPLStreamingBox.ms")


rollout RSL_IPLStream "IPL Stream Helpers" width:270 height:524
(
	local helpLink = "https://mp.rockstargames.com/index.php/IPL_Streaming_Helpers"
	local commonData = project.common + "\\data\\levels\\"
	local IPLStream_PickMap
	local IPLStream_PickXRef
	local firstTime = true
	
	local uniqueID = ""
	
	local directoryList = #()
	local IPLBoxArray = #()
	local mapNames = #()
	local mapFiles = #()
	
	--------------------------------------------------------------------------------------------
	-- 						UI
	--------------------------------------------------------------------------------------------
	button btn_help "Help" width:260 height:24
	dropdownlist maps_dd "" width:260 height:20 offset:[-6,0]
	button btn_generateIPL "Generate IPL Boxes from streams" width:260 height:24
	listBox lbx_IPLbox "IPL Box Parent" width:126 height:20 across:2 offset:[-6,0]
	listBox lbx_IPLLinks "IPL Box Children" width:126 height:20 offset:[4,0]
	
	button btn_AddIPLParent "Add IPL Parent" width:126 height:24 across:2 offset:[-4,0]
-- 	button btn_Xrefimage "Load Xref IPL Maps" width:126 height:24 across:2 offset:[-4,0] enabled:false
	button btn_AddLink "Add link" width:126 height:24 offset:[6,0]
	
	button btn_RemIPLParent "Remove IPL Parent" width:126 height:24 offset:[-5,0] across:2
	button btn_DelLink "Delete link" width:126 height:24 offset:[6,0]
	
-- 	button btn_AddIPLParent "Add IPL Parent" width:126 height:24 across:2 offset:[-4,0]
-- 	button btn_RemIPLParent "Remove IPL Parent" width:126 height:24 offset:[5,0]
	
	button btn_levelGeo "Import Level Geometry" width:260 height:24 
	
	button btn_ImportDat "Import IPL Dat File"  width:260 height:24
	button btn_ExportDat "Export IPL Dat File"  width:260 height:24
	
	progressBar pb_fuckingWorkYouTart width:260 offset:[-8,4]
	
	--------------------------------------------------------------------------------------------
	-- 						getIPLBoxByID()
	--------------------------------------------------------------------------------------------
	fn getIPLBoxByID ID =
	(
		local result = undefined
		
		for object in objects where (classOf object) == IPLStreamingBox do
		(
			if object.u32BoxID == ID then
			(
				result = object
			)
		)
		
		result
	)
	
	--------------------------------------------------------------------------------------------
	-- 						isMapInDatFile()
	--------------------------------------------------------------------------------------------
	fn isMapInDatFile datFile levelName mapFile type:"/" =
	(
		local result = false
		local dataStream = openFile datFile
		local datEntry = toLower ("IDE platform:/LEVELS/" + levelName + type + mapFile + ".IDE")
		
		while not (eof dataStream) do
		(
			local line = readLine dataStream
			
			if (toLower line) == datEntry then
			(
				result = true
			)
		)
		
		close dataStream
		
		result
	)
	
	--------------------------------------------------------------------------------------------
	-- 						gatherMapNamesForLevel()
	--------------------------------------------------------------------------------------------
	fn gatherMapNamesForLevel mapPath =
	(
		mapNames = #()
		local levelName = maps_dd.selected
		local datFile = commonData + levelName + "\\" + levelName + ".dat"
		local mapFiles = getFiles (mapPath + "*.rpf")
		
		for entry in mapFiles do
		(
			if (isMapInDatFile datFile levelName (getFilenameFile entry)) then
			(
				local mapEntry = dataPair name:(getFilenameFile entry) imageName:(getFilenameFile entry)
				
				append mapNames mapEntry
			)
		)
		
		local interiorFiles = getFiles (mapPath + "interiors\\*.ide")
		
		for interior in interiorFiles do
		(
			if (isMapInDatFile datFile levelName (getFilenameFile interior) type:"/interiors/") then
			(
				local dataStream = openFile interior mode:"r"
				
				skiptostring dataStream "mlo"
				line = readLine dataStream
				line = readLine dataStream
				
				local mapEntry = dataPair name:((filterString line ", ")[1] + "_interior") imageName:(getFilenameFile interior)
				
				append mapNames mapEntry
				
				close dataStream
			)
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						updateIPLBoxes()
	--------------------------------------------------------------------------------------------
	fn updateIPLBoxes listBox =
	(
		local currentParentList = #()
		IPLBoxArray = #()
		
		for object in objects where (classOf object) == IPLStreamingBox do
		(
			append currentParentList object.name
			append IPLBoxArray object
		)
		
		listBox.items = currentParentList
	)
	
	--------------------------------------------------------------------------------------------
	-- 						getBoundsFromIPL()
	--------------------------------------------------------------------------------------------
	fn getBoundsFromIPL file xArray yArray zArray =
	(
		local keymatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\w+,\s[0-9]*,\s([-]*[0-9]*.[0-9]*),\s([-]*[0-9]*.[0-9]*),\s([-]*[0-9]*.[0-9]*),)")
		local dataStream = openFile file mode:"r"
		
		skiptostring dataStream "inst"
		
		while not eof dataStream do
		(
			local linematch = keymatch.Match (readline dataStream)
			
			if (linematch.success) then
			(
				append xArray (linematch.groups.item[2].value as float)
				append yArray (linematch.groups.item[3].value as float)
				append zArray (linematch.groups.item[4].value as float)
			)
		)
		
		close dataStream
	)
	
	--------------------------------------------------------------------------------------------
	-- 						gatherBoundsFromIPLs()
	--------------------------------------------------------------------------------------------
	fn gatherBoundsFromIPLs mapPath =
	(
		local result  = false
		local iplposX = #()
		local iplposY = #()
		local iplposZ = #()
		
		local IPLFiles = getFiles mapPath
		
		if IPLFiles.count > 0 then
		(
			result  = #([0,0,0],[0,0,0])
			
			for file in IPLFiles do
			(
				getBoundsFromIPL file iplposX iplposY iplposZ
			)
			
			result[1].x = amin iplposX
			result[1].y = amin iplposY
			result[1].z = amin iplposZ
			
			result[2].x = amax iplposX
			result[2].y = amax iplposY
			result[2].z = amax iplposZ
		)
		
		result
	)
	
	--------------------------------------------------------------------------------------------
	-- 						getBoundsFromIDE()
	--------------------------------------------------------------------------------------------
	fn getBoundsFromIDE mapPath =
	(
		local result  = false
-- 		local keymatch = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) ("(\w+,\s[0-9]*,\s([-]*[0-9]*.[0-9]*),\s([-]*[0-9]*.[0-9]*),\s([-]*[0-9]*.[0-9]*),)")
		
		if (doesFileExist mapPath) then
		(
			result  = #([0,0,0],[0,0,0],"")
			
			local dataStream = openFile mapPath mode:"r"
			
			skiptostring dataStream "mlo"
			skipToNextLine dataStream
			local line = readLine dataStream
			
			if line != "end" then
			(
				local iplposX = #()
				local iplposY = #()
				local iplposZ = #()
				
				result[3] = (filterString line ", ")[1]
				
				line = readLine dataStream
				while line != "mloroomstart" do
				(
					local filtered = filterString line ", 	"
					
					append iplposX (filtered[2] as float)
					append iplposY (filtered[3] as float)
					append iplposZ (filtered[4] as float)
					
					line = readLine dataStream
				)
				
				close dataStream
				
				result[1].x = amin iplposX
				result[1].y = amin iplposY
				result[1].z = amin iplposZ
				
				result[2].x = amax iplposX
				result[2].y = amax iplposY
				result[2].z = amax iplposZ
			)
			else
			(
				messageBox ("The interior " + (getFilenamefile mapPath) + " has nothing in it. The IPL Box cannot be generated.") title:"Error..."
				result = false
			)
		)
		else
		(
			format "%\n\tFile does not exist\n" mapPath
		)
		
		result
	)
	
	--------------------------------------------------------------------------------------------
	-- 						getMILOpositionFromIPLs()
	--------------------------------------------------------------------------------------------
	fn getMILOpositionFromIPLs mapPath MILO =
	(
		local result = false
		
		local IPLFiles = getFiles (mapPath + "*.ipl")
		
		for IPL in IPLFiles do
		(
			local dataStream = openFile IPL mode:"r"
			
			if (skipToString dataStream MILO) != undefined then
			(
				result = [0,0,0]
				local filtered = filterString (readLine dataStream) ", 	"
				
				result.x = filtered[2] as float
				result.y = filtered[3] as float
				result.z = filtered[4] as float
			)
			
			close dataStream
		)
		
		result
	)
	
	--------------------------------------------------------------------------------------------
	-- 						extractIPL()
	--------------------------------------------------------------------------------------------
	fn extractIPL mapPath image =
	(
		-- Clean dir image extraction
		HiddenDOSCommand ("del c:\\tempIPL\\" + pathConfig.stripPathToLeaf mapPath + "*.ipl") startpath:"c:\\"
		
		-- mount image and extract the ipl data
		local rageBuilderExe = "ragebuilder_0327.exe"
		arguments = ("x:\\tools\\bin\\script\\ExtractIPL.rbs -input "+ image + " -output c:\\tempIPL\\"+(pathConfig.stripPathToLeaf mapPath))
		HiddenDOSCommand (rageBuilderExe +" "+ arguments) startpath:"x:\\tools\\bin\\"
	)
	
	--------------------------------------------------------------------------------------------
	-- 						generateMapIPLBox()
	--------------------------------------------------------------------------------------------
	fn generateMapIPLBox mapPath image =
	(
		local IPLPath = ("c:\\tempIPL\\" + pathConfig.stripPathToLeaf mapPath + (getFilenameFile image) + "*.ipl")
		local bounds = gatherBoundsFromIPLs IPLPath
		
		if bounds != false then
		(
			local rfpFile = image
			local position = bounds[1] + ((bounds[2] - bounds[1]) / 2)
			
			local newName = (getFilenameFile image)
			if (getIPLBoxByID newName) != undefined then
			(
				newName = uniqueName newName
				rfpFile = "NA"
			)
			
			local newIPLBox = IPLStreamingBox pos:position name:newName top:(abs (position.z - bounds[2].z)) bottom:-(abs (position.z - bounds[1].z)) \
															left:-(abs (position.x - bounds[1].x)) right:(abs (position.x - bounds[2].x)) \
															front:(abs (position.y - bounds[2].y)) back:-(abs (position.y - bounds[1].y)) \
															u32BoxID:newName rfpFile:rfpFile
			
			append IPLBoxArray newIPLBox
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						generateInteriorIPLBox()
	--------------------------------------------------------------------------------------------
	fn generateInteriorIPLBox mapPath image =
	(
		local bounds = getBoundsFromIDE image
		
		if bounds != false then
		(
			local offset = getMILOpositionFromIPLs mapPath bounds[3]
			if offset != false then
			(
			bounds[1] += offset
			bounds[2] += offset
			
			local rfpFile = substituteString image ".ide" ".rpf"
			local position = bounds[1] + ((bounds[2] - bounds[1]) / 2)
			local newName = (bounds[3] + "_interior")
			if (getIPLBoxByID newName) != undefined then
			(
				newName = uniqueName newName
				rfpFile = "NA"
			)
			
			local newIPLBox = IPLStreamingBox pos:position name:newName top:(abs (position.z - bounds[2].z)) bottom:-(abs (position.z - bounds[1].z)) \
															left:-(abs (position.x - bounds[1].x)) right:(abs (position.x - bounds[2].x)) \
															front:(abs (position.y - bounds[2].y)) back:-(abs (position.y - bounds[1].y)) \
															u32BoxID:newName rfpFile:rfpFile
			
			append IPLBoxArray newIPLBox
			)
		)
		else 
		(
			print ("Error finding offst for :" +mapPath)
		)
	)

	--------------------------------------------------------------------------------------------
	-- 						GenIPLBoxes()
	--------------------------------------------------------------------------------------------
	fn GenIPLBoxes mapPath =
	(
		makeDir ("c:\\tempIPL\\"+pathConfig.stripPathToLeaf mapPath)
		
		local rpfList =  getfiles (mapPath + "*.rpf")
		
		for image in rpfList do
		(
			extractIPL mapPath image
			generateMapIPLBox mapPath image
		)
		
		rpfList =  getfiles (mapPath + "\\interiors\\*.ide")
		
		for image in rpfList do
		(
			generateInteriorIPLBox mapPath image
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						updateVisualLinks()
	--------------------------------------------------------------------------------------------
	fn updateVisualLinks IPLBox =
	(
		IPLBox.colourCurrent = IPLBox.colorParent
		
		for entry in IPLBox.linkdata do
		(
			local child = getIPLBoxByID entry
			
			if (classOf child) == IPLStreamingBox then
			(
				child.colourCurrent = child.colourChild
			)
		)
		
		for entry in IPLBoxArray where entry.u32BoxID != IPLBox.u32BoxID and (findItem IPLBox.linkdata entry.u32BoxID) == 0 do
		(
			entry.colourCurrent = entry.colourBase
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						getDataPairByName()
	--------------------------------------------------------------------------------------------
	fn getDataPairByName name =
	(
		local result = false
		
		for entry in mapNames do
		(
			if entry.name == name then
			(
				result = entry
			)
		)
		
		result
	)
	
	--------------------------------------------------------------------------------------------
	-- 						pickMapFromList()
	--------------------------------------------------------------------------------------------
	fn pickMapFromList =
	(
		try (destroyDialog IPLStream_PickMap) catch()
		
		rollout IPLStream_PickMap "Pick Parent" width:180
		(
			multilistBox lbx_mapList "===============================================" width:170 height:20 offset:[-7,0]
			button btn_ok "OK" width:82 across:2 offset:[-6,0]
			button btn_cancel "Cancel" width:82 offset:[6,0]
			
			on IPLStream_PickMap open do
			(
				print mapNames
				lbx_mapList.text = "Available Maps For " + (substituteString RSL_IPLStream.maps_dd.selected "\\" "")
				
				lbx_mapList.items = (for entry in mapNames where (findItem RSL_IPLStream.lbx_IPLbox.items entry.name) == 0 collect entry.name)
-- 				lbx_mapList.items = (for entry in mapNames collect entry.name)
			)
			
			on btn_ok pressed do
			(
				if (lbx_mapList.selection as array).count > 0 then
				(
					local mapArray = for i = 1 to lbx_mapList.selection.count where (lbx_mapList.selection[i]) and not (matchPattern lbx_mapList.items[i] pattern:"*_interior") collect lbx_mapList.items[i]
					local interiorArray = for i = 1 to lbx_mapList.selection.count where (lbx_mapList.selection[i]) and (matchPattern lbx_mapList.items[i] pattern:"*_interior") collect lbx_mapList.items[i]
					
					local mapPath = directoryList[RSL_IPLStream.maps_dd.selection]
					makeDir ("c:\\tempIPL\\"+pathConfig.stripPathToLeaf mapPath)
					
					for image in mapArray do
					(
						extractIPL mapPath (mapPath + image + ".rpf")
						generateMapIPLBox mapPath (mapPath + image + ".rpf")
					)
					
					for image in interiorArray do
					(
						local interiorImage = getDataPairByName image
						
						generateInteriorIPLBox mapPath (mapPath + "\\interiors\\" + interiorImage.imageName + ".ide")
					)
					
					updateIPLBoxes RSL_IPLStream.lbx_IPLbox
				)
				
				destroyDialog IPLStream_PickMap
			)
			
			on btn_cancel pressed do
			(
				destroyDialog IPLStream_PickMap
			)
		)
		
		createDialog IPLStream_PickMap style:#(#style_toolwindow, #style_sysmenu) modal:true
	)
	
	--------------------------------------------------------------------------------------------
	-- 						removeChild()
	--------------------------------------------------------------------------------------------
	fn removeChild ID =
	(
		for entry in IPLBoxArray do
		(
			local index = findItem entry.linkdata ID
			
			if index > 0 then
			(
				deleteItem entry.linkdata index
			)
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						getImageByIPLBoxName()
	--------------------------------------------------------------------------------------------
	fn getImageByIPLBoxName name levelPath =
	(
		local result = "NA"
		
-- 		print levelPath
		
		if (matchPattern name pattern:"*_interior") then
		(
			local interiorIDEs = getFiles (levelPath + "interiors\\*.ide")
			
			for ide in interiorIDEs do
			(
				dataStream = openFile ide
				
				if dataStream != undefined then
				(
					skipToString dataStream "mlo"
					skipToNextLine dataStream
					
					local mloName = (filterString (readLine dataStream) ", 	")[1]
					if (toLower mloName) == (toLower (substituteString name "_interior" "")) then
					(
						result = ide
					)
				)
			)
		)
		else
		(
			local mapFiles = getFiles (levelPath + "*.rpf")
			
			for rfp in mapFiles do
			(
				if matchPattern rfp pattern:("*" + name + "*") then
				(
					result = rfp
				)
			)
		)
		
		result
	)
	
	--------------------------------------------------------------------------------------------
	-- 						importIPLstreamdata()
	--------------------------------------------------------------------------------------------
	fn importIPLstreamdata filename =
	(
		local dataStream = openfile filename mode:"r"
		local tag = skiptostring dataStream "IPL_BOXES_BEGIN"
		
		if tag != undefined then
		(
			readline dataStream
			
			while not eof dataStream do
			(				
				local line = readline dataStream
				
				if line != "IPL_BOXES_END" then
				(
					local filtered = filterString line " "
					local ID = filtered[1]
					local boundMax = [filtered[5] as float,filtered[6] as float,filtered[7] as float]
					local boundMin = [filtered[2] as float,filtered[3] as float,filtered[4] as float]
					local position = boundMin + ((boundMax - boundMin) / 2)
					
					local linkCount = filtered[8] as integer
					local linkData = #()
					
					if linkCount > 0 then
					(
						for i = 1 to linkCount do
						(
							append linkData filtered[8 + i]
						)
					)
					
					local levelPath = project.independent + "\\levels\\" + (substituteString (pathConfig.stripPathToLeaf (getFilenamePath filename)) "\\" "") + "\\"
					local rpfFile = getImageByIPLBoxName ID levelPath
					
					local newIPLBox = IPLStreamingBox pos:position name:ID top:(abs (position.z - boundMax.z)) bottom:-(abs (position.z - boundMin.z)) \
																	left:-(abs (position.x - boundMin.x)) right:(abs (position.x - boundMax.x)) \
																	front:(abs (position.y - boundMax.y)) back:-(abs (position.y - boundMin.y)) \
																	u32BoxID:ID linkdata:linkData rpfFile:rpfFile
					
					append IPLBoxArray newIPLBox
				)
			)
			
			updateIPLBoxes RSL_IPLStream.lbx_IPLbox
			RSL_IPLStream.lbx_IPLbox.selection = 1
			
			print RSL_IPLStream.lbx_IPLbox.selection
			
			updateVisualLinks IPLBoxArray[RSL_IPLStream.lbx_IPLbox.selection]
		)
		
		close dataStream
	)
	
	--------------------------------------------------------------------------------------------
	-- 						exportIPLstreamdata()
	--------------------------------------------------------------------------------------------
	fn exportIPLstreamdata filename =
	(
		local newFile = createfile filename	
		
		format "IPL_BOXES_BEGIN\n" to:newFile
		
		for IPLBox in IPLBoxArray do
		(
			local bounds = nodeLocalBoundingBox IPLBox
			local topLeft = ""
			local bottomRight = ""
			
			for i = 1 to 3 do
			(
				if i < 3 then
				(
					topLeft += ((IPLBox.pos[i] + IPLBox.topleft[i]) as string) + " "
					bottomRight += ((IPLBox.pos[i] + IPLBox.botright[i]) as string) + " "
				)
				else
				(
					topLeft += ((IPLBox.pos[i] + IPLBox.topLeft[i]) as string)
					bottomRight += ((IPLBox.pos[i] + IPLBox.botright[i]) as string)
				)
			)
			
			format "% % % %" IPLBox.u32BoxID topLeft bottomRight IPLBox.linkdata.count to:newFile
			
			if IPLBox.linkdata.count == 0 then
			(
				format "\n" to:newFile
			)
			else
			(
				format " " to:newFile
				
				for i = 1 to IPLBox.linkdata.count do
				(
					if i < IPLBox.linkdata.count then
					(
						format "% " IPLBox.linkdata[i] to:newFile
					)
					else
					(
						format "%\n" IPLBox.linkdata[i] to:newFile
					)
				)
			)
		)
		
		format "IPL_BOXES_END" to:newFile
		close newFile
	)
	
	--------------------------------------------------------------------------------------------
	-- 						getMapNameFromIPLBoxes()
	--------------------------------------------------------------------------------------------
	fn getMapNameFromIPLBoxes =
	(
		local result = undefined
		local foundLevel = false
		
		local mapArray = for i = 1 to RSL_IPLStream.lbx_IPLbox.items.count where not (matchPattern RSL_IPLStream.lbx_IPLbox.items[i] pattern:"*_interior") collect toLower RSL_IPLStream.lbx_IPLbox.items[i]
		
		for entry in directoryList while not foundLevel do
		(
			local rpfFiles = getFiles (entry + "*.rpf")
			local rpfNameArray = for rpf in rpfFiles collect toLower (getFilenameFile rpf)
			
			if rpfNameArray.count > 0 then
			(
				local foundMaps = true
				
				for map in mapArray do
				(
					if (findItem rpfNameArray map) == 0  then
					(
						foundMaps = false
					)
				)
				
				if foundMaps then
				(
					result = (substituteString (pathConfig.stripPathToLeaf entry) "\\" "")
					foundLevel = true
				)
			)
		)
		
		result
	)
	
	fn gatherMapFiles levelName mapDirectory =
	(
		local tempMapFiles = getFiles (mapDirectory + "*.rpf")
		
		local datFile = commonData + levelName + "/" + levelName +".dat"
		
		if (doesFileExist datFile) then
		(
			for mapFile in tempMapFiles do
			(
				if (isMapInDatFile datFile levelName (getFilenameFile mapFile)) then
				(
					append mapFiles mapFile
				)
			)
		)
	)
	
	fn readXML xmlFile =
	(
-- 		progressStart ("Reading " + (getFilenameFile xmlFile))
		
		local xml = XmlDocument()
		xml.init()
		xml.load xmlFile
		
		local objectsElement = RSL_Xml.getXMLNodeByName xml.document.DocumentElement "objects"
		
		for i = 0 to objectsElement.ChildNodes.Count - 1 do
		(
			local object = objectsElement.ChildNodes.Item[i]
			local class = (object.Attributes.ItemOf "class").value
			local attributesElement = RSL_Xml.getXMLNodeByName object "attributes"
			local attributesClass = (attributesElement.Attributes.ItemOf "class").value
			
			local updateValue = ((i + 1) as float / objectsElement.ChildNodes.Count) * 100.0
-- 			progressUpdate updateValue
			pb_fuckingWorkYouTart.value = updateValue
			
			if class == "xref_object" and attributesClass == "Gta MILOTri" then
			(
-- 				local refName = (object.Attributes.ItemOf "refname").value 
				local refFile = (object.Attributes.ItemOf "reffile").value
				
				local positionElement = RSL_Xml.getXMLNodeByName (RSL_Xml.getXMLNodeByName (RSL_Xml.getXMLNodeByName object "transform") "object") "position"
				local offset = [(positionElement.Attributes.ItemOf "x").value as float, \
									(positionElement.Attributes.ItemOf "y").value as float, \
									(positionElement.Attributes.ItemOf "z").value as float]
				
				local rotationElement = RSL_Xml.getXMLNodeByName (RSL_Xml.getXMLNodeByName (RSL_Xml.getXMLNodeByName object "transform") "object") "rotation"
				local rotationQuat = quat 0 0 0 0
				
				if rotationElement != undefined then
				(
					rotationQuat = (quat ((rotationElement.Attributes.ItemOf "x").value as float) \
										((rotationElement.Attributes.ItemOf "y").value as float) \
										((rotationElement.Attributes.ItemOf "z").value as float) \
										((rotationElement.Attributes.ItemOf "w").value as float))
				)
				
				local imageFile = (directoryList[maps_dd.selection] + "interiors\\" + (getFilenameFile refFile) + ".rpf")
				
				local ibdList = RSL_bndOps.extract maps_dd.selected #(imageFile) "ibd" mapType:"interiors"
				local bnd = RSL_bndOps.extract maps_dd.selected ibdList "bnd" mapType:"interiors"
				local MILOxmlFile = ("c:\\temp_xml\\" + maps_dd.selected + "\\interiors\\" + (getFilenameFile refFile) + "\\" + (getFilenameFile refFile) + ".xml")
				RSL_Xml.fixXml MILOxmlFile
				
				local imageName = (getFilenameFile refFile)
				local currentLayer = LayerManager.getLayerFromName imageName
				
				if currentLayer == undefined then
				(
					currentLayer = LayerManager.newLayerFromName imageName
				)
				
				RSL_bndOps.parseBoundFiles bnd offset:offset quat:rotationQuat interiorXml:MILOxmlFile layer:currentLayer wireColour:(color 127 127 255)
			)
		)
		
-- 		progressEnd()
	)
	
	fn cb_RSL_IPLStreamSelectionChanged =
	(
		local IPLArray = selection
		if IPLArray.count == 1 and (classOf IPLArray[1]) == IPLStreamingBox then
		(
			local index = findItem lbx_IPLbox.items IPLArray[1].u32BoxID
			
			if index > 0 then
			(
				lbx_IPLbox.selection = index
				updateVisualLinks IPLBoxArray[lbx_IPLbox.selection]
				
				lbx_IPLLinks.items = IPLBoxArray[lbx_IPLbox.selection].linkdata as array
			)
		)
	)
	
	fn addLink =
	(
		rollout RSL_IPLStream_AddLink "Add Link..." width:148
		(
			local validArray = #()
			
			multiListBox mlbx_validLinks "Valid Children" width:132 height:20 offset:[-4,0]
			button btn_one "One Way" width:64 across:2 offset:[-4,0]
			button btn_two "Two Way" width:64 offset:[4,0]
			
			on RSL_IPLStream_AddLink open do
			(
				for object in objects where (classOf object) == IPLStreamingBox do
				(
					if object.u32BoxID != IPLBoxArray[lbx_IPLbox.selection].u32BoxID and (findItem IPLBoxArray[lbx_IPLbox.selection].linkdata object.u32BoxID) == 0 then
					(
						append validArray object
					)
				)
				
				mlbx_validLinks.items = (for entry in validArray collect entry.u32BoxID)
			)
			
			on RSL_IPLStream_AddLink close do
			(
				lbx_IPLLinks.items = IPLBoxArray[lbx_IPLbox.selection].linkdata as array
				
				updateVisualLinks IPLBoxArray[lbx_IPLbox.selection]
			)
			
			on mlbx_validLinks selectionEnd do
			(
				if (mlbx_validLinks.selection as array).count > 0 then
				(
					for i = 1 to mlbx_validLinks.selection.count do
					(
						local IPLBox = validArray[i]
						
						if (mlbx_validLinks.selection[i]) then
						(
							IPLBox.colourCurrent = IPLBox.colourChild
						)
						else
						(
							IPLBox.colourCurrent = IPLBox.colourBase
						)
					)
				)	
			)
			
			on btn_one pressed do
			(
				if (mlbx_validLinks.selection as array).count > 0 then
				(
					for i = 1 to mlbx_validLinks.selection.count where (mlbx_validLinks.selection[i]) do
					(
						local IPLBox = validArray[i]
						
						appendIfUnique IPLBoxArray[lbx_IPLbox.selection].linkdata IPLBox.u32BoxID
					)
					
					destroyDialog RSL_IPLStream_AddLink
				)
			)
			
			on btn_two pressed do
			(
				if (mlbx_validLinks.selection as array).count > 0 then
				(
					for i = 1 to mlbx_validLinks.selection.count where (mlbx_validLinks.selection[i]) do
					(
						local IPLBox = validArray[i]
						
						appendIfUnique IPLBoxArray[lbx_IPLbox.selection].linkdata IPLBox.u32BoxID
						appendIfUnique IPLBox.linkdata IPLBoxArray[lbx_IPLbox.selection].u32BoxID
					)
					
					destroyDialog RSL_IPLStream_AddLink
				)
			)
		)
		
		createDialog RSL_IPLStream_AddLink style:#(#style_toolwindow,#style_sysmenu) modal:true
	)
	
	fn deleteLink =
	(
		rollout RSL_IPLStream_DeleteLink "Delete Link..." width:148
		(
			button btn_one "One Way" width:64 across:2 offset:[-4,0]
			button btn_two "Two Way" width:64 offset:[4,0]
			
			on btn_one pressed do
			(
				local index = findItem IPLBoxArray[lbx_IPLbox.selection].linkdata lbx_IPLLinks.selected
				deleteItem IPLBoxArray[lbx_IPLbox.selection].linkdata index
				
				lbx_IPLLinks.items = IPLBoxArray[lbx_IPLbox.selection].linkdata as array
				
				updateVisualLinks IPLBoxArray[lbx_IPLbox.selection]
				
				destroyDialog RSL_IPLStream_DeleteLink
			)
			
			on btn_two pressed do
			(
				local child = getIPLBoxByID lbx_IPLLinks.selected
				local index = findItem IPLBoxArray[lbx_IPLbox.selection].linkdata child.u32BoxID
				deleteItem IPLBoxArray[lbx_IPLbox.selection].linkdata index
				
				index = (findItem child.linkdata IPLBoxArray[lbx_IPLbox.selection].u32BoxID)
				
				if index > 0 then
				(
					deleteItem child.linkdata index
				)
				
				lbx_IPLLinks.items = IPLBoxArray[lbx_IPLbox.selection].linkdata as array
				
				updateVisualLinks IPLBoxArray[lbx_IPLbox.selection]
				
				destroyDialog RSL_IPLStream_DeleteLink
			)
		)
		
		createDialog RSL_IPLStream_DeleteLink style:#(#style_toolwindow,#style_sysmenu) modal:true
	)
	
	--------------------------------------------------------------------------------------------
	-- 						RSL_IPLStream open
	--------------------------------------------------------------------------------------------
	on RSL_IPLStream open do
	(
		manipulateMode = true
		directoryList = getDirectories (project.independent+"/levels/*")
		maps_dd.items = for entry in directoryList collect (substituteString (pathConfig.stripPathToLeaf entry) "\\" "")
		
		gatherMapNamesForLevel directoryList[maps_dd.selection]
		
		updateIPLBoxes lbx_IPLbox
		
		if lbx_IPLbox.selected != undefined then
		(
			local currentIPLBox = IPLBoxArray[lbx_IPLbox.selection]
			
			lbx_IPLLinks.items = currentIPLBox.linkdata as array
			
			local mapName = getMapNameFromIPLBoxes()
			
			if mapName != undefined then
			(
				local index = findItem maps_dd.items mapName
				
				maps_dd.selection = index
				
				gatherMapNamesForLevel directoryList[index]
			)
			else
			(
				messageBox "Unable to figure out which map this is. Make sure you select it from the list." title:"Warning..."
			)
		)
		
		cb_RSL_IPLStreamSelectionChanged()
		callbacks.addScript #selectionSetChanged "RSL_IPLStream.cb_RSL_IPLStreamSelectionChanged()" id:#RSL_IPLStreamCallbacks
	)
	
	--------------------------------------------------------------------------------------------
	-- 						RSL_IPLStream close
	--------------------------------------------------------------------------------------------
	on RSL_IPLStream close do
	(
		callbacks.removeScripts id:#RSL_IPLStreamCallbacks
	)
	
	--------------------------------------------------------------------------------------------
	-- 						btn_help pressed
	--------------------------------------------------------------------------------------------
	on btn_help pressed do
	(
		ShellLaunch helpLink ""
	)
	
	--------------------------------------------------------------------------------------------
	-- 						maps_dd selected index
	--------------------------------------------------------------------------------------------
	on maps_dd selected index do
	(
		gatherMapNamesForLevel directoryList[index]
	)
	
	--------------------------------------------------------------------------------------------
	-- 						btn_generateIPL pressed
	--------------------------------------------------------------------------------------------
	on btn_generateIPL pressed do
	(
		-- Remove old IPLBoxes and clear listboxes
		delete (for object in objects where (classOf object) == IPLStreamingBox collect object)
		
		lbx_IPLbox.items = #()
		lbx_IPLLinks.items = #()
		IPLBoxArray = #()
		
		GenIPLBoxes directoryList[maps_dd.selection]
		
		updateIPLBoxes lbx_IPLbox
		updateVisualLinks IPLBoxArray[lbx_IPLbox.selection]
	)
	
	--------------------------------------------------------------------------------------------
	-- 						lbx_IPLbox selected index
	--------------------------------------------------------------------------------------------
	on lbx_IPLbox selected index do
	(
		updateVisualLinks IPLBoxArray[lbx_IPLbox.selection]
		
		lbx_IPLLinks.items = IPLBoxArray[lbx_IPLbox.selection].linkdata as array
		
		select (getIPLBoxByID IPLBoxArray[lbx_IPLbox.selection].u32BoxID)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						lbx_IPLLinks selected index
	--------------------------------------------------------------------------------------------
	on lbx_IPLLinks selected index do
	(
		for entry in IPLBoxArray[lbx_IPLbox.selection].linkdata do
		(
			local child = getIPLBoxByID entry
			
			if child.u32BoxID == lbx_IPLLinks.selected then
			(
				child.colourCurrent = child.colourChildSelected
			)
			else
			(
				child.colourCurrent = child.colourChild
			)
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						btn_AddLink pressed
	--------------------------------------------------------------------------------------------
	on btn_AddLink pressed do
	(
		addLink()
	)
	
	--------------------------------------------------------------------------------------------
	-- 						btn_DelLink pressed
	--------------------------------------------------------------------------------------------
	on btn_DelLink pressed do
	(
		if lbx_IPLLinks.selection > 0 then
		(
			deleteLink()
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						btn_AddIPLParent pressed
	--------------------------------------------------------------------------------------------
	on btn_AddIPLParent pressed do
	(
		pickMapFromList()
	)
	
	--------------------------------------------------------------------------------------------
	-- 						btn_RemIPLParent pressed
	--------------------------------------------------------------------------------------------
	on btn_RemIPLParent pressed do
	(
		if lbx_IPLbox.selection > 0 then
		(
			local currentIPLBox = getIPLBoxByID lbx_IPLbox.items[lbx_IPLbox.selection]
			
			removeChild lbx_IPLbox.items[lbx_IPLbox.selection]
			
			delete currentIPLBox
			
			updateIPLBoxes lbx_IPLbox
			
			if lbx_IPLbox.items.count > 0 then
			(
				lbx_IPLbox.selection = 1
			)
			
			updateVisualLinks IPLBoxArray[lbx_IPLbox.selection]
			
			lbx_IPLLinks.items = IPLBoxArray[lbx_IPLbox.selection].linkData as array
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						btn_levelGeo pressed
	--------------------------------------------------------------------------------------------
	on btn_levelGeo pressed do
	(
		local okToContinue = queryBox ("ABOUT TO IMPORT " + (toUpper maps_dd.selected) +"\n\nThis is will delete all objects (excluding IPLBoxes) and empty layers before importing the Level geometry.\nIt may take a while to import. Ok to continue?") title:"Warning..."
		
		if okToContinue then
		(
			delete (for object in objects where (classOf object) != IPLStreamingBox collect object)
			
			local layerDeleteArray = #()
			
			for i = 1 to LayerManager.count do
			(
				local currentLayer = LayerManager.getLayer i
				
				if currentLayer != undefined then
				(
					currentLayer.nodes &nodeArray
					
					if nodeArray.count == 0 then
					(
						append layerDeleteArray currentLayer.name
					)
				)
			)
			
			if layerDeleteArray.count > 0 then
			(
				for entry in layerDeleteArray do
				(
					LayerManager.deleteLayerByName entry
				)
			)
			
			mapFiles = #()
			
			local levelName = maps_dd.selected
			local path = directoryList[maps_dd.selection]
			
			gatherMapFiles levelName path
			
			local ibnList = RSL_bndOps.extract levelName mapFiles "ibn"
			local bndList = RSL_bndOps.extract levelName ibnList "bnd"
			
			RSL_bndOps.parseBoundFiles bndList wireColour:(color 40 100 160)
			
			local interiorPath = path + "interiors\\*.rpf"
			local iArray = (getFiles interiorPath)
			RSL_bndOps.extract levelName iArray "xml" mapType:"interiors"
			
			local xmlList = RSL_bndOps.extract levelName mapFiles "xml"
			
			for i = 1 to xmlList.count do
			(
				local xml = xmlList[i]
				
				RSL_Xml.fixXml xml
				readXML xml
			)
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						btn_ImportDat pressed
	--------------------------------------------------------------------------------------------
	on btn_ImportDat pressed do
	(
		delete (for object in objects where (classOf object) == IPLStreamingBox collect object)
		
		local IPLStreamFile = getOpenFilename caption:"IPLstream File" types:"dat file (*.dat)|*.dat"
		
		if IPLStreamFile != undefined then
		(
			importIPLstreamdata IPLStreamFile
			
			local index = findItem maps_dd.items (substituteString (pathConfig.stripPathToLeaf (getFilenamePath IPLStreamFile)) "\\" "")
			
			if index > 0 then
			(
				maps_dd.selection = index
			)
		)
	)
	
	--------------------------------------------------------------------------------------------
	-- 						btn_ExportDat pressed
	--------------------------------------------------------------------------------------------
	on btn_ExportDat pressed do
	(
		local IPLStreamFile = getSaveFilename caption:"IPLstream File" filename:"streamhelpers.dat" types:"dat file (*.dat)|*.dat"
		
		if IPLStreamFile != undefined then
		(
			if (doesFileExist IPLStreamFile) then
			(
				if not (getFileAttribute IPLStreamFile #readOnly) then
				(
					exportIPLstreamdata IPLStreamFile
				)
				else
				(
					messageBox "The file is read only." title:"Error..."
				)
			)
			else
			(
				exportIPLstreamdata IPLStreamFile
			)
		)
	)
)

createDialog RSL_IPLStream style:#(#style_toolwindow,#style_sysmenu)