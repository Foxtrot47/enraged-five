try (destroyDialog RSL_AutoLod) catch()

rollout RSL_AutoLod "RS Auto LOD Distance" width:170
(
	mapped fn setLODBySize object multiplier = 
	(
		local index = getattrindex "Gta Object" "LOD distance"
		local node
		
		local frag = matchPattern object.name pattern:"*_frag_"
		
		if frag then
		(
			node = (getNodeByName (substituteString object.name "_frag_" ""))
		)
		if node == undefined then node = object
		
		if index != undefined then
		(
			local xDistance = abs (node.max.x - node.min.x)
			local yDistance = abs (node.max.y - node.min.y)
			local zDistance = abs (node.max.z - node.min.z)
			
-- 			local LODDistance = (ceil ((xDistance + yDistance + zDistance) * multiplier)) as integer
			local LODDistance = (ceil ((distance node.min node.max) * multiplier)) as integer
			
			if LODDistance > RSL_AutoLod.spn_max.value then LODDistance = RSL_AutoLod.spn_max.value
			if LODDistance < 2.0 then LODDistance = 2.0
			
			setattr object index LODDistance
		)
	)
	
	spinner spn_max "Maximum Range  " range:[1,500,100] type:#integer align:#center
	spinner spn_multiplier "Distance Multiplier" range:[1,50,10] type:#integer align:#center
	button btn_autoLOD "Auto Assign LOD" width:160 align:#center
	
	on btn_autoLOD pressed do
	(
		local objectsToProcess
		local okToContinue = false
		
		if (selection as array).count == 0 then
		(
			okToContinue = queryBox "Process the entire scene?" title:"Ok To Continue?" 
			
			if okToContinue then
			(
				objectsToProcess = for object in objects where (GetAttrClass object) == "Gta Object" collect object
			)
		)
		else
		(
			okToContinue = queryBox "Process the selection?" title:"Ok To Continue?" 
			
			if okToContinue then
			(
				objectsToProcess = for object in (selection as array) where (GetAttrClass object) == "Gta Object" collect object
			)
		)
		
		if okToContinue then
		(
			setLODBySize objectsToProcess spn_multiplier.value
		)
	)

)

createDialog RSL_AutoLod style:#(#style_toolwindow,#style_sysmenu)