
try (destroyDialog RSL_AutoWindowTypeSize) catch()

rollout RSL_AutoWindowTypeSize " R* Auto Window Size"
(
	local windowCollisionTypeArray = #()
	local sizeArray = #(1.5,3.0)
	local attrIndex = getattrindex "Gta Collision" "Coll Type"
	local rexbound = false

	fn readATTFile =
	(
		local rsNorthAttFile = (getdir #plugCfg + "rsnorth.att")
		local dataStream = openFile rsNorthAttFile
		
		if dataStream != undefined then
		(
			skipToString dataStream "startui \"Gta Collision\" #editing \"Gta Collision\""
			skipToString dataStream "tlist 367 \""
			
			local line = (readDelimitedString dataStream "\"")
			
			tempArray = filterString line ","
			
			for item in tempArray do
			(
				local windowMatch = matchPattern item patterm:"WINDOW_GLASS_MEDIUM*"
				
				if windowMatch then
				(
					append windowCollisionTypeArray item
				)
			)
		)
	)

	fn isRexbound material =
	(
		case (classOf material) of
		(
			default:(rexbound = false)
			multimaterial:
			(
				for i = 1 to material.materialList.count do
				(
					local subMaterial = material.materialList[i]
					
					isRexBound subMaterial
				)
			)
			RexBoundMtl:
			(
				rexbound = true
			)
		)
		
		rexbound
	)

	fn getFacesByID object ID =
	(
		local result = #()
		
		for face in object.faces do
		(
			if (getFaceMatID object face.index) == ID then
			(
				append result face.index
			)
		)
		
		result
	)

	fn assignToRexBound object material ID:undefined =
	(
		local currentType = RexGetCollisionName material
		local isWindow = matchPattern currentType pattern:"WINDOW_GLASS*"
		local matchStrong = matchPattern currentType pattern:"*_STRONG"
		
		if isWindow and not matchStrong then
		(
			local strength = ""
			local matchWeak = matchPattern currentType pattern:"*_WEAK_*"
			local matchMedium = matchPattern currentType pattern:"*_MEDIUM_*"
			
			if matchWeak then
			(
				strength = "_WEAK_"
			)
			if matchMedium then
			(
				strength = "_MEDIUM_"
			)
			
			local faceArray = object.faces
			
			if ID != undefined then
			(
				faceArray = getFacesByID object ID
			)
			
			local faceArea = meshOp.getFaceArea object faceArray
			
			if faceArea <= sizeArray[1] then
			(
				RexSetCollisionName material ("WINDOW_GLASS" + strength + "SML")
			)
			else
			(
				if faceArea <= sizeArray[2] then
				(
					RexSetCollisionName material ("WINDOW_GLASS" + strength + "MED")
				)
				else
				(
					RexSetCollisionName material ("WINDOW_GLASS" + strength + "LRG")
				)
			)
		)
	)

	fn processRexBound object material =
	(
		col2Mesh object
		
		case (classOf material) of
		(
			rexBoundMtl:
			(
				assignToRexBound object material
			)
			multimaterial:
			(
				for i = 1 to material.materialList.count do
				(
					local subMaterial = material.MaterialList[i]
					local ID = material.MaterialIDList[i]
					
					if (classOf subMaterial) == rexBoundMtl then
					(
						assignToRexBound object subMaterial ID:ID
					)
				)
			)
		)
		
		mesh2Col object
	)

	fn processAttribute object =
	(
		local currentType = getAttr object attrIndex
		local isWindow = matchPattern currentType pattern:"WINDOW_GLASS*"
		
		if isWindow then
		(
			local strength = "_MEDIUM_"
			local matchWeak = matchPattern currentType pattern:"*_WEAK_*"
			local matchMedium = matchPattern currentType pattern:"*_MEDIUM_*"
			local matchStrong = matchPattern currentType pattern:"*_STRONG"
			
			if matchWeak then
			(
				strength = "_WEAK_"
			)
			
			local parent = object.parent
			
			if parent != undefined and not matchStrong then
			(
				convertToMesh parent
				
				local faceArea = meshOp.getFaceArea parent parent.faces
				
				if faceArea <= sizeArray[1] then
				(
					setAttr object attrIndex ("WINDOW_GLASS" + strength + "SML")
				)
				else
				(
					if faceArea <= sizeArray[2] then
					(
						setAttr object attrIndex ("WINDOW_GLASS" + strength + "MED")
					)
					else
					(
						setAttr object attrIndex ("WINDOW_GLASS" + strength + "LRG")
					)
				)
			)
			else
			(
				format "% has no parent. Unable to set window glass by size.\n" object.name
			)
		)
	)

	mapped fn processWindowGlass object =
	(
		local material = object.material
		rexbound = false
		
		if (isRexbound material) then
		(
			processRexBound object material
		)
		else
		(
			processAttribute object
		)
	)
	
	label lbl_small "Small:" across:2 align:#left
	spinner spn_small range:[0.1,100,1.5] fieldwidth:48 align:#right offset:[7,0]
	label lbl_medium "Medium:" across:2 align:#left
	spinner spn_medium range:[0.1,100,3.0] fieldwidth:48 align:#right offset:[7,0]
	button btn_assign "Assign Collision Type by Size" width:151
	
	on btn_assign pressed do
	(
		sizeArray = #(spn_small.value,spn_medium.value)
		
		collisionArray = for object in objects where (getAttrClass object) == "Gta Collision" collect object
		
		processWindowGlass collisionArray
	)
)

createDialog RSL_AutoWindowTypeSize style:#(#style_toolwindow,#style_sysmenu)