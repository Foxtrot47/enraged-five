-------------------------------------------------------------------------------------------------------------------------------------------
-- RSL_batchTXDOptimiser
-- By Paul Truss
-- Senior Technical Artist
-- Rockstar Games London
-- 08/12/2010
--
-- Tool for batch optimising TXD and/or outputing TXDLists on multiple max file for a single level.
-------------------------------------------------------------------------------------------------------------------------------------------

filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_dotNetUIOps.ms")

fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/RSL_TXDOpsFunctions.ms")
-- perforce integration script
filein "pipeline/util/p4.ms"

if RsMapSetupGlobals == undefined then
(
	fileIn "pipeline/export/maps/globals.ms"
)

global RSL_batchTXDOptimise

struct RSL_batchTXDOptimiser
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- Struct variables
	---------------------------------------------------------------------------------------------------------------------------------------
	p4 = RsPerforce(), -- struct instance
	TXDFuncs = RSL_TXDOpsFunctions(), -- struct instance
	
	commonData = (RsConfigGetCommonDir() + "data\\levels\\"), -- common data levels path
	intermediate = project.independent + ("/intermediate/levels/"), -- intermediate levels path
	
	levelArray = #(),
	maxFileArray = #(),
	
	packType = #bestMatch,
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- UI variables
	---------------------------------------------------------------------------------------------------------------------------------------
	errorForm,
	
	batchForm,
	levelLabel,
	levelListView,
	maxFileLabel,
	maxFileTree,
	optimiseCheckButton,TXDListCheckButton,bestMatchMode,nearestNeighbourMode,
	runBatchButton,
	mainTable,
	
	TXDListTrue = RS_dotNetClass.colourClass.fromARGB 195 249 202,
	TXDListFalse = RS_dotNetClass.colourClass.fromARGB 246 198 202,
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- createErrorForm() Creates a dialog with errors listed from the parsed array variable.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn createErrorForm array =
	(
		errorForm = RS_dotNetUI.maxForm "errorForm" text:"Batch Complete with Errors..." Size:[512,256] min:[512,128] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		errorForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").CenterScreen
		
		logWindow = dotNetObject "TextBox"
		logWindow.Dock = (dotNetClass "System.Windows.Forms.DockStyle").Fill
		logWindow.Multiline = true
		logWindow.ScrollBars = (dotNetClass "System.Windows.Forms.ScrollBars").Vertical
		logWindow.AcceptsReturn = true
		logWindow.AcceptsTab = true
		
		errorForm.controls.add logWindow
		
		errorForm.showModeless()
		
		for entry in array do 
		(
			logWindow.appendText entry.file
			logWindow.appendText entry.error
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getLevels() Collects the level names & paths from the art directory and appends them to the levelArray struct variable.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getLevels =
	(
		local levelDirectories = getDirectories (RsProjectGetArtDir() + "/maps/*")
		
		levels = #(#(),#())
		
		for directory in levelDirectories do
		(
			local dataStream = directory as stringStream
			skipToString dataStream "\\maps\\"
			local mapName = readDelimitedString dataStream "\\"
			
			append levelArray (DataPair name:mapName directory:directory)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- addLevelItem() Mapped function that creates a listViewItem for each parsed levelData variable and assigns it to the 
	-- levelListView control.
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn addLevelItem levelData =
	(
		local newItem = dotNetObject "ListViewItem" levelData.name
		newItem.tag = dotNetMXSValue levelData
		
		levelListView.items.Add newItem
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- isMapInDatFile() Checks to see if the parsed mapFile variable if listed in the parsed datFile. Returns true if found and
	-- not commented out.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn isMapInDatFile datFile levelName mapFile type:"/" =
	(
		local result = false
		local dataStream = openFile datFile
		local datEntry = toLower ("IDE platform:/LEVELS/" + levelName + type + mapFile + ".IDE")
		
		while not (eof dataStream) do
		(
			local line = readLine dataStream
			
			if (toLower line) == datEntry then
			(
				result = true
			)
		)
		
		close dataStream
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- hasTXDList() Checks to see if the parsed mapFile variable for the parsed levelName variable has a TXDList file. Returns
	-- true if found.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn hasTXDList levelName mapFile =
	(
		local TXDListFile = intermediate + levelName + "\\" + mapFile + "_TXDList.xml"
		
		if (doesFileExist TXDListFile) then true else false
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getMaxFiles() Collects the max files listed in the level dat file for the parsed level variable and creates a treeNode
	-- for each level. Warns the user if the dat file does not exist. Adds a root node for each type e.g. prop files, map files etc...
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getMaxFiles level =
	(
		local datFile = commonData + level.name + "/" + level.name +".dat"
		
		-- if the data file exists, we can add nodes for each valid max file.
		if (doesFileExist datFile) then
		(
			local propFiles = getFiles (level.directory + "props\\*.max")
			local interiorFiles = getFiles (level.directory + "interiors\\*.max")
			local buildingFiles = getFiles (level.directory + "buildings\\*.max")
			local mapFiles = getFiles (level.directory + "*.max") 
			
			maxFileTree.nodes.clear()
			
			if propFiles.count > 0 then
			(
				local validProps = #()
				
				-- check to see if any of the prop files are listed in the dat file
				for file in propFiles do 
				(
					if (isMapInDatFile datFile level.name (getFilenameFile file) type:"/props/") then
					(
						append validProps file
					)
				)
				
				-- if we have valid, dat file listed prop files, we create the props root node and assign nodes for each prop max file
				if validProps.count > 0 then
				(
					local propsRoot = maxFileTree.Nodes.Add "Prop Files"
					
					for entry in validProps do 
					(
						local name = (getFilenameFile entry)
						local mapFile = propsRoot.Nodes.Add name
						mapFile.tag = dotNetMXSValue entry
						
						if (hasTXDList level.name name) then
						(
							mapFile.BackColor = TXDListTrue
						)
						else
						(
							mapFile.BackColor = TXDListFalse
						)
					)
					
					propsRoot.expand()
				)
			)
			
			-- check to see if any of the interior files are listed in the dat file
			if interiorFiles.count > 0 then
			(
				local validInteriors = #()
				
				for file in interiorFiles do 
				(
					if (isMapInDatFile datFile level.name (getFilenameFile file) type:"/interiors/") then
					(
						append validInteriors file
					)
				)
				
				-- if we have valid, dat file listed interior files, we create the interiors root node and assign nodes for each interior max file
				if validInteriors.count > 0 then
				(
					local interiorsRoot = maxFileTree.Nodes.Add "Interior Files"
					
					for entry in validInteriors do 
					(
						local name = (getFilenameFile entry)
						local mapFile = interiorsRoot.Nodes.Add name
						mapFile.tag = dotNetMXSValue entry
						
						if (hasTXDList level.name name) then
						(
							mapFile.BackColor = TXDListTrue
						)
						else
						(
							mapFile.BackColor = TXDListFalse
						)
					)
					
					interiorsRoot.expand()
				)
			)
			
			-- check to see if any of the building files are listed in the dat file
			if buildingFiles.count > 0 then
			(
				local validBuildings = #()
				
				for file in buildingFiles do 
				(
					if (isMapInDatFile datFile level.name (getFilenameFile file) type:"/buildings/") then
					(
						append validBuildings file
					)
				)
				
				-- if we have valid, dat file listed building files, we create the building root node and assign nodes for each building max file
				if validBuildings.count > 0 then
				(
					local buildingsRoot = maxFileTree.Nodes.Add "Building Files"
					
					for entry in validBuildings do 
					(
						local name = (getFilenameFile entry)
						local mapFile = buildingsRoot.Nodes.Add name
						mapFile.tag = dotNetMXSValue entry
						
						if (hasTXDList level.name name) then
						(
							mapFile.BackColor = TXDListTrue
						)
						else
						(
							mapFile.BackColor = TXDListFalse
						)
					)
					
					buildingsRoot.expand()
				)
			)
			
			-- check to see if any of the map files are listed in the dat file
			if mapFiles.count > 0 then
			(
				local validmaps = #()
				
				for file in mapFiles do 
				(
					if (isMapInDatFile datFile level.name (getFilenameFile file)) then
					(
						append validmaps file
					)
				)
				
				-- if we have valid, dat file listed map files, we create the map files root node and assign nodes for each map file max file
				if validmaps.count > 0 then
				(
					local mapsRoot = maxFileTree.Nodes.Add "Map Files"
					
					for entry in validmaps do 
					(
						local name = (getFilenameFile entry)
						local mapFile = mapsRoot.Nodes.Add name
						mapFile.tag = dotNetMXSValue entry
						
						if (hasTXDList level.name name) then
						(
							mapFile.BackColor = TXDListTrue
						)
						else
						(
							mapFile.BackColor = TXDListFalse
						)
					)
					
					mapsRoot.expand()
				)
			)
		)
		else
		(
			messagebox (level.name + " does not have a dat file. Make sure you've got the latest data.") title:"Error..."
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- recurseIsChecked() Recursive check to see if the parsed node variable is checked or not. This is used to enable the runBatchButton
	-- control. If any of the nodes are checked and they're not root nodes, we can run the batch.
	-- the parsed result variable is set to true if at least one of the valid nodes is checked.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn recurseIsChecked node &result =
	(
		if (isProperty node #checked) and node.checked and node.parent != undefined then
		(
			result = true
		)
		
		for i = 0 to node.Nodes.count - 1 do 
		(
			local subNode = node.Nodes.item[i]
			
			recurseIsChecked subNode &result
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- okToBatch() Sets the enabled state of the runBatchButton control.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn okToBatch =
	(
		local checked = false
		recurseIsChecked maxFileTree &checked
		
		runBatchButton.enabled = checked
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- recurseGetChecked() Recursive function that appends all checked node tag data to the parsed result variable. Used to
	--collect the max file nodes the user has chosen to run batch operations on.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn recurseGetChecked node &result =
	(
		if (isProperty node #checked) and node.checked and node.tag != undefined then
		(
			append result node.tag.value
		)
		
		for i = 0 to node.Nodes.count - 1 do 
		(
			local subNode = node.Nodes.item[i]
			
			recurseGetChecked subNode &result
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- runBatch() Batch function called when the user presses the runBatchButton control.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn runBatch =
	(
		local failedFiles = #()
		
		-- get the latest intermediate data
		if (p4.connected()) then
		(
			p4.connect (project.intermediate)
			depotFiles = p4.local2depot (getFiles (project.independent + "/intermediate/levels/*.*"))
			p4.sync depotFiles
			
				-- get an array of all the max file we want to run the batch on.
			maxFileArray = #()
			recurseGetChecked maxFileTree &maxFileArray
			
				-- get the latest versions of the max files
			p4.sync (p4.local2depot maxFileArray)
			
			for maxFile in maxFileArray do 
			(
				-- if the max file is read only and we want to optimse the TXDs, we need to check the file out because we're about to make changes to it
				if (getFileAttribute maxFile #readOnly) and not TXDListCheckButton.checked then
				(
					p4.edit (p4.local2depot maxFile)
				)
				
				-- if the max file is still read only after attempting to check out and we want to optimse the TXDs, someone else has the file checked out.
				-- We can optimise this file so we add an error to the failedFiles array variable and skip the file.
				if (getFileAttribute maxFile #readOnly) and not TXDListCheckButton.checked then
				(
					append failedFiles (dataPair file:((getFilenameFile maxFile) + "\r\n") error:"    File is read only. Checked out by someone else?\r\n")
				)
				else
				(
					-- load the max file and store any missing files (bitmaps etc...) to the missingFiles variable
					local missingFiles = #()
					local fileLoaded = loadMaxfile maxFile quiet:true missingExtFilesAction:#logmsg missingExtFilesList:&missingFiles
					
					-- if the file has loaded
					if fileLoaded then
					(
						-- if we have any missing files, we create a string with all the missing files and add an error to the failedFiles array variable.
						if missingFiles.count > 0 then
						(
							local stream = stringStream ""
							
							format "  Missing Files:\r\n" to:stream
							
							for entry in missingFiles do 
							(
								format "    %\r\n" entry to:stream
							)
							
							append failedFiles (dataPair file:((getFilenameFile maxFile) + "\r\n") error:(stream as string))
						)
						
						-- if we want to optimise the file, we run the autoPack function from RSL_TXDOpsFunctions
						if optimiseCheckButton.checked then
						(
							local packed = TXDFuncs.autoPack packType --#bestMatch --#nearestNeighbour
							
							-- if the file was packed, we save to max file
							if packed then
							(
								saveMaxFile maxFile clearNeedSaveFlag:true quiet:true
							)
							else
							(
								-- The pack failed so we add an error to the failedFiles array variable and do not save the max file.
								append failedFiles (dataPair file:((getFilenameFile maxFile) + "\r\n") error:"    Pack failed. Max file not saved.\r\n")
							)
						)
						else
						(
							-- if only want to output a TXDList file, we use the functions from RSL_TXDOpsFunctions to do so and do not save the file
							-- because we haven't actually changed the max file, just gathered data from it.
							local textureObjectArray = #()
							local errorStringArray = #()
							local TXDArray = #()
							
							TXDFuncs.createTextureObjects textureObjectArray errorStringArray
							
							TXDFuncs.createTXDs textureObjectArray &TXDArray
							
							TXDFuncs.writeTXDList TXDArray quiet:true
						)
						
						-- garbage cleanup because otherwise we'll use too much memory and max will eventually crash.
						gc()
					)
					else
					(
						-- if the file fails to load, we add an error to the failedFiles array variable.
						append failedFiles (dataPair file:((getFilenameFile maxFile) + "\r\n") error:"    File failed to load. File corrupt?\r\n")
					)
				)
			)
			
			-- if we have had errors during the batch run, we create the error form so that the user can see what errors were found.
			if failedFiles.count > 0 then
			(
				
				createErrorForm failedFiles
			)
			else
			(
				-- No errors!
				messageBox "All files optimised." title:"Batch Complete..."
			)
			
			p4.disconnect()
		)
		else
		(
			messageBox "You are not currently connected to perforce.\nLogin to perforce and then run the tool again" title:"Error..." 
		)
	),
	
	
	
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- UI FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchLevelListClick() Dispatches levelListClick() Called when the user selects an item in the LevelListView
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchLevelListClick s e =
	(
		RSL_batchTXDOptimise.levelListClick s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- levelListClick() Populates the max file tree view with valid max files from the chosen level.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn levelListClick s e =
	(
		if s.selectedItems.Count == 1 then
		(
			local levelData = s.selectedItems.item[0].tag.value
			
			getMaxFiles levelData
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchTreeNodeChecked() Dispatches treeNodeChecked() Called when the user selects an item in the max file treeView
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchTreeNodeChecked s e =
	(
		RSL_batchTXDOptimise.treeNodeChecked s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- treeNodeChecked() If the user checks a root node e.g. the props root node, all it's children get checked as well.
	-- Runs the okToBatch struct function to see if we can run a batch on the checked nodes.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn treeNodeChecked s e =
	(
		local checkedNode = e.node
		
		if checkedNode.nodes.count > 0 then
		(
			for i = 0 to checkedNode.nodes.count - 1 do 
			(
				checkedNode.nodes.item[i].checked = checkedNode.checked
			)
		)
		
		okToBatch() 
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchModeChanged() Dispatches modeChanged() Called when the user checks either the optimiseCheckButton or the
	-- TXDListCheckButton.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchModeChanged s e =
	(
		RSL_batchTXDOptimise.modeChanged s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- modeChanged() If the user wants to optimise TXDs, unchecks the TXDListCheckButton and vice versa. If optimiseCheckButton
	-- is checked, both the bestMatchMode and nearestNeighbourMode are dissabled.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn modeChanged s e =
	(
		case s.name of
		(
			"optimiseCheckButton":
			(
				TXDListCheckButton.checked = not s.checked
			)
			"TXDListCheckButton":
			(
				optimiseCheckButton.checked = not s.checked
			)
		)
		
		bestMatchMode.enabled = optimiseCheckButton.checked
		nearestNeighbourMode.enabled = optimiseCheckButton.checked
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchPackModeChanged() Dispatches packModeChanged() Called when the user checks either the bestMatchMode or the
	-- nearestNeighbourMode.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchPackModeChanged s e =
	(
		RSL_batchTXDOptimise.packModeChanged s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- packModeChanged() Used to change the pack mode when optimising TXDs. Sets the packType struct variable depending on
	-- which pack mode is chosen.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn packModeChanged s e =
	(
		case s.name of
		(
			"bestMatchMode":
			(
				nearestNeighbourMode.checked = not s.checked
				packType = #bestMatch
			)
			"nearestNeighbourMode":
			(
				bestMatchMode.checked = not s.checked
				packType = #nearestNeighbour
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchPackModeChanged() Dispatches runBatchClick() Called when the user presses the runBatchButton
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchRunBatchClick s e =
	(
		RSL_batchTXDOptimise.runBatchClick s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- runBatchClick() Runs the batch on the checked max files.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn runBatchClick s e =
	(
		local okToContinue = queryBox "Are you sure you want to batch optimise?\nThe currently loaded max file will not be saved." title:"Warning..."
		
		if okToContinue then
		(
			resetMaxFile #noPrompt
			SuspendEditing()
			disableSceneRedraw()
			runBatch()
			enableSceneRedraw()
			ResumeEditing alwaysSuspend:true
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- createForm() Creates the batch form.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn createForm =
	(
		clearListener()
		
		--Get the max handle pointer.
		local maxHandlePointer=(Windows.GetMAXHWND())

		--Convert the HWND handle of Max to a dotNet system pointer
		local sysPointer = DotNetObject "System.IntPtr" maxHandlePointer

		--Create a dotNet wrapper containing the maxHWND
		local maxHwnd = DotNetObject "MaxCustomControls.Win32HandleWrapper" sysPointer
		
		batchForm = RS_dotNetUI.Form "batchForm" text:"R* Batch TXD Optimiser V1.0" size:[256,512] min:[128,128] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		batchForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		
		levelLabel = RS_dotNetUI.Label "levelLabel" text:("  Levels") \
												textAlign:RS_dotNetPreset.CA_MiddleLeft borderStyle:RS_dotNetPreset.BS_None \
												dockStyle:RS_dotNetPreset.DS_Fill 
		
		levelListView = RS_dotNetUI.listView "levelListView" dockStyle:RS_dotNetPreset.DS_Fill
		levelListView.view = (dotNetClass "System.Windows.Forms.View").Details
		levelListView.HeaderStyle = (dotNetClass "System.Windows.Forms.ColumnHeaderStyle").None
		levelListView.fullRowSelect = true
		levelListView.HideSelection = false
		levelListView.BackColor = (DotNetClass "System.Drawing.SystemColors").InactiveBorder
-- 		levelListView.backColor = RS_dotNetPreset.controlColour_Medium
		dotnet.addEventHandler levelListView "click" dispatchLevelListClick
		
		levelListView.columns.add "Level" -1
		
		levelListTable = RS_dotNetUI.tableLayout "levelListTable" text:"mainTable" collumns:#((dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		levelListTable.BackColor = (dotNetClass "System.Drawing.SystemColors").Control
		
		levelListTable.RowCount = 2
		levelListTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		levelListTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 50)
		
		levelListTable.controls.add levelLabel 0 0
		levelListTable.controls.add levelListView 0 1
		
		maxFileLabel = RS_dotNetUI.Label "maxFileLabel" text:("  Max Files") \
												textAlign:RS_dotNetPreset.CA_MiddleLeft borderStyle:RS_dotNetPreset.BS_None \
												dockStyle:RS_dotNetPreset.DS_Fill 
		
		maxFileTree = RS_dotNetUI.treeView "maxFileTree" dockStyle:RS_dotNetPreset.DS_Fill
		maxFileTree.Sorted = false
		maxFileTree.FullRowSelect = true
		maxFileTree.AllowDrop = true
		maxFileTree.CheckBoxes = true
		maxFileTree.BackColor = (DotNetClass "System.Drawing.SystemColors").InactiveBorder
-- 		maxFileTree.backColor = RS_dotNetPreset.controlColour_Medium
		dotnet.addEventHandler maxFileTree "AfterCheck" dispatchTreeNodeChecked
		
		maxFileTable = RS_dotNetUI.tableLayout "maxFileTable" text:"mainTable" collumns:#((dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		maxFileTable.BackColor = (dotNetClass "System.Drawing.SystemColors").Control
		
		maxFileTable.RowCount = 2
		maxFileTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		maxFileTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 50)
		
		maxFileTable.controls.add maxFileLabel 0 0
		maxFileTable.controls.add maxFileTree 0 1
		
		mainSplitContainer = RS_dotNetUI.SplitContainer "mainSplitContainer" borderStyle:RS_dotNetPreset.BS_FixedSingle dockStyle:RS_dotNetPreset.DS_Fill 
-- 		dotnet.addEventHandler mainSplitContainer "splitterMoved" dispatchSplitterMoved
-- 		mainSplitContainer.backColor = RS_dotNetPreset.controlColour_Medium
		mainSplitContainer.BackColor = (dotNetClass "System.Drawing.SystemColors").ScrollBar
		mainSplitContainer.Orientation = (dotNetClass "System.Windows.Forms.Orientation").Horizontal
		mainSplitContainer.SplitterWidth = 4
		
		mainSplitContainer.Panel1.Controls.Add levelListTable
		mainSplitContainer.Panel2.Controls.Add maxFileTable
		
		optimiseCheckButton = RS_dotNetUI.checkBox "optimiseCheckButton" text:("Optimise") appearance:RS_dotNetPreset.AC_Button dockStyle:RS_dotNetPreset.DS_Fill
		optimiseCheckButton.checked = true
		dotNet.addEventHandler optimiseCheckButton "CheckedChanged" dispatchModeChanged
		
		TXDListCheckButton = RS_dotNetUI.checkBox "TXDListCheckButton" text:("TXDList") appearance:RS_dotNetPreset.AC_Button dockStyle:RS_dotNetPreset.DS_Fill
		TXDListCheckButton.checked = false
		dotNet.addEventHandler TXDListCheckButton "CheckedChanged" dispatchModeChanged
		
		bestMatchMode = RS_dotNetUI.checkBox "bestMatchMode" text:("Best Match") appearance:RS_dotNetPreset.AC_Button dockStyle:RS_dotNetPreset.DS_Fill
		bestMatchMode.checked = packType == #bestMatch
		dotNet.addEventHandler bestMatchMode "CheckedChanged" dispatchPackModeChanged
		
		nearestNeighbourMode = RS_dotNetUI.checkBox "nearestNeighbourMode" text:("Nearest Neighbour") appearance:RS_dotNetPreset.AC_Button dockStyle:RS_dotNetPreset.DS_Fill
		nearestNeighbourMode.checked = not bestMatchMode.checked
		dotNet.addEventHandler nearestNeighbourMode "CheckedChanged" dispatchPackModeChanged
		
		bestMatchMode.enabled = optimiseCheckButton.checked
		nearestNeighbourMode.enabled = optimiseCheckButton.checked
		
		runBatchButton = RS_dotNetUI.Button "runBatchButton" text:("Run Batch") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		runBatchButton.enabled = false
		dotnet.addEventHandler runBatchButton "click" dispatchRunBatchClick
		
		TXDGroupTable = RS_dotNetUI.tableLayout "TXDGroupTable" text:"mainTable" collumns:#((dataPair type:"Percent" value:50),(dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
-- 		TXDGroupTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		TXDGroupTable.RowCount = 2
		TXDGroupTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		TXDGroupTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		
		TXDGroupTable.controls.add optimiseCheckButton 0 0
		TXDGroupTable.controls.add TXDListCheckButton 1 0
		TXDGroupTable.controls.add bestMatchMode 0 1
		TXDGroupTable.controls.add nearestNeighbourMode 1 1
		
		TXDOpsGroup = RS_dotNetUI.GroupBox "TXDOpsGroup" text:"TXD Optimisation Options" dockStyle:RS_dotNetPreset.DS_Fill
		
		TXDOpsGroup.controls.add TXDGroupTable
		
		mainTable = RS_dotNetUI.tableLayout "mainTable" text:"mainTable" collumns:#((dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
-- 		mainTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		mainTable.RowCount = 3
-- 		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 50)
-- 		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
-- 		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 50)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 72)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 28)
-- 		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		
-- 		mainTable.controls.add levelLabel 0 0
-- 		mainTable.controls.add levelListView 0 1
-- 		mainTable.controls.add maxFileLabel 0 2
-- 		mainTable.controls.add maxFileTree 0 3
		mainTable.controls.add mainSplitContainer 0 0
-- 		mainTable.controls.add optimiseCheckButton 0 1
-- 		mainTable.controls.add TXDListCheckButton 1 1
-- 		mainTable.controls.add bestMatchMode 0 2
-- 		mainTable.controls.add nearestNeighbourMode 1 2
		mainTable.controls.add TXDOpsGroup 0 1
		mainTable.controls.add runBatchButton 0 2
		
-- 		mainTable.SetColumnSpan levelLabel 2
-- 		mainTable.SetColumnSpan levelListView 2
-- 		mainTable.SetColumnSpan maxFileLabel 2
-- 		mainTable.SetColumnSpan maxFileTree 2
-- 		mainTable.SetColumnSpan mainSplitContainer 2
-- 		mainTable.SetColumnSpan runBatchButton 2
		
		getLevels()
		
		addLevelItem levelArray
		levelListView.AutoResizeColumns (dotNetClass "ColumnHeaderAutoResizeStyle").HeaderSize
		
		batchForm.controls.add mainTable
		
		batchForm.show (maxHwnd)
		mainSplitContainer.SplitterDistance = 220
	)
)

if RSL_batchTXDOptimise != undefined then
(
	RSL_batchTXDOptimise.batchForm.close()
)

RSL_batchTXDOptimise = RSL_batchTXDOptimiser()
RSL_batchTXDOptimise.createForm()