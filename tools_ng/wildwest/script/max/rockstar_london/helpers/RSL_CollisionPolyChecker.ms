
global RSL_CollisionPolyOps

filein (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\utils\\RSL_dotNetUIOps.ms")

struct RSL_CollisionPolyChecker
(
	
	dynamicCollisionArray = #(),
	errorArray = #(),
	
	idxDynamic = (getattrindex "Gta Object" "Is Dynamic"),
	
	polyCheckerForm,
	errorListView,
	polyLimitNumeric,
	checkSceneButton,
	
	fn decimalToFloat control =
	(
		local dValue = getProperty control #value asDotNetObject:true
		local fValue = (dotNetClass "System.Decimal").ToSingle dValue
	),
	
	fn recurseGetParent object &result =
	(
		if object.parent != undefined then
		(
			if (getAttrClass object.parent) == "Gta Object" then
			(
				result = object.parent
			)
			
			recurseGetParent object.parent &result 
		)
	),
	
	mapped fn getDynamicCollision object =
	(
		local parent
		recurseGetParent object &parent
		
		if parent != undefined then
		(
			if (getattr parent idxDynamic) then
			(
				append dynamicCollisionArray object
			)
		)
	),
	
	mapped fn checkPolyCount object =
	(
		local polyCount = getcollpolycount object
		if polyCount > (decimalToFloat polyLimitNumeric) then
		(
			append errorArray (dataPair object:object polyCount:polyCount)
		)
	),
	
	mapped fn addToListView data =
	(
		local newItem = dotNetObject "listViewItem" data.object.name
		newItem.tag = dotNetMXSValue data.object
		newItem.subItems.Add (data.polyCount as string)
		
		errorListView.Items.Add newItem
	),
	
	fn updateListView =
	(
		dynamicCollisionArray = #()
		errorArray = #()
		errorListView.Items.Clear()
		
		getDynamicCollision (for object in objects where (classOf object) == Col_Mesh collect object)
		
		checkPolyCount dynamicCollisionArray
		
		addToListView errorArray
	),
	
	
	
	fn dispatchListDoubleClick s e =
	(
		RSL_CollisionPolyOps.listDoubleClick s e
	),
	
	fn listDoubleClick s e =
	(
		select s.SelectedItems.Item[0].tag.value
		max zoomext sel
	),
	
	fn dispatchCheckSceneClick s e =
	(
		RSL_CollisionPolyOps.checkSceneClick s e
	),
	
	fn checkSceneClick s e =
	(
		updateListView()
	),
	
	fn createForm =
	(
		polyCheckerForm = RS_dotNetUI.maxForm "polyCheckerForm" text:"R* Collision Poly Checker" size:[256,512] min:[128,256] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		polyCheckerForm.icon = RS_dotNetObject.iconObject (RsConfigGetWildWestDir() + "script/max/Rockstar_London/images/TextureBrowseIconRS.ico")
		
		errorListView = RS_dotNetUI.listView "propListView" dockStyle:RS_dotNetPreset.DS_Fill
		errorListView.view = (dotNetClass "System.Windows.Forms.View").Details --Details
		errorListView.Sorting = (dotNetClass "System.Windows.Forms.SortOrder").Ascending
		errorListView.HideSelection = false
		errorListView.scrollable = true
		errorListView.HeaderStyle = (dotNetClass "System.Windows.Forms.ColumnHeaderStyle").Nonclickable
		errorListView.columns.add "Collision" 128
		errorListView.columns.add "Poly Count" 64
		errorListView.BackColor = ((dotNetClass"System.Drawing.Color").fromARGB 197 197 197) --RS_dotNetPreset.controlColour_Medium
		dotnet.addEventHandler errorListView "DoubleClick" dispatchListDoubleClick
		
		polyLimitLabel = RS_dotNetUI.Label "polyLimitLabel" text:("Max Poly Count") textAlign:RS_dotNetPreset.CA_MiddleCenter borderStyle:RS_dotNetPreset.BS_None \
													dockStyle:RS_dotNetPreset.DS_Fill 	
		polyLimitLabel.font = dotNetObject "System.Drawing.Font" "Trebuchet MS" 8 RS_dotNetClass.fontStyleClass.Bold
		
		polyLimitNumeric = RS_dotNetUI.Numeric "minusXPadding" value:500 range:#(1000.0, 10, 1, 0) dockStyle:RS_dotNetPreset.DS_Fill
		
		checkSceneButton = RS_dotNetUI.Button "checkSceneButton" text:(toUpper "Check Scene") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler checkSceneButton "click" dispatchCheckSceneClick
		
		mainTable = RS_dotNetUI.tableLayout "mainTable" text:"mainTable" collumns:#((dataPair type:"Percent" value:50), (dataPair type:"Percent" value:50)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		mainTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		mainTable.RowCount = 3
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 50)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 24)
		
		mainTable.controls.add errorListView 0 0
		mainTable.controls.add polyLimitLabel 0 1
		mainTable.controls.add polyLimitNumeric 1 1
		mainTable.controls.add checkSceneButton 0 2
		
		mainTable.SetColumnSpan errorListView 2
		mainTable.SetColumnSpan checkSceneButton 2
		
		polyCheckerForm.controls.add mainTable
		
		updateListView()
		
		polyCheckerForm.showModeless()
	)
)

if RSL_CollisionPolyOps != undefined then
(
	RSL_CollisionPolyOps.polyCheckerForm.close()
)

RSL_CollisionPolyOps = RSL_CollisionPolyChecker()
RSL_CollisionPolyOps.createForm()
