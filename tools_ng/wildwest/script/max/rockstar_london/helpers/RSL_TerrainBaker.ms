
try (DestroyDialog RSL_TerrainBaker) catch()

rollout RSL_TerrainBaker "R* Terrain Baker" width:230
(
	local INIFile = RsConfigGetWildWestDir() + "/script/max/rockstar_london/config/terrainBaker.ini"
	
	local sizeArray = #([2048, 2048], [1024, 1024], [512, 512], [256, 256], [128, 128], [64, 64], [32, 32], [16, 16])
-- 	local fileTypeArray = #(".bmp", ".tif")
	local outputDir
	
	local MaskConverter
	local decalCompositer

	local convertThread
	local fileWatcher
	local convertInProgress
	
	local shaderType
	local lookupFilename
	local lookupResized
	local layerArray
	local maskArray = #()
	
	local startMapChannel = 60
	local vertexMaskSucess = false
	
	local bakeSource
	local decalSourceArray = #()
	local decalLayerArray = #()
	
	local renderBitmap
	
	fn getShaderType object =
	(
		local result
		local shaderName
		
		case (classOf object.material) of
		(
			Rage_Shader:
			(
				local shaderName = RstGetShaderName object.material
			)
-- 			DirectX_9_Shader:
-- 			(
-- 				local shaderName = object.material.effectfile
-- 			)
		)
		
		if shaderName != undefined then
		(
			if (MatchPattern shaderName pattern:"*4lyr*") then
			(
				result = dataPair count:4 type:#vertex
			)
			
			if (MatchPattern shaderName pattern:"*8lyr*") then
			(
				result = dataPair count:8 type:#vertex
			)
			
			if result != undefined then
			(
				if (MatchPattern shaderName pattern:"*_cm.*") then
				(
					result.type = #mask
				)
			)
		)
		
		result
	)
	
	fn pickValidSource object =
	(
		(getAttrClass object) == "Gta Object" and (getShaderType object) != undefined and (classOf object) != XrefObject and (findItem decalSourceArray object) == 0
	)
	
	fn pickValidDecalObject object =
	(
		(getAttrClass object) == "Gta Object" and (classOf object) != XrefObject and object != RSL_TerrainBaker.pbtn_source.object and object != RSL_TerrainBaker.pbtn_Target.object
	)
	
	fn pickValidTarget object =
	(
		(getAttrClass object) == "Gta Object" and (classOf object) != XrefObject and object != RSL_TerrainBaker.pbtn_source.object and (findItem decalSourceArray object) == 0
	)
	
	group "Bake Objects"
	(
		pickButton pbtn_source "Pick SourceTerrain" filter:pickValidSource width:200
		multiListBox mlbx_decals "Decals"
		button btn_fromSelection "From Selection" width:150 across:2 offset:[24,0]
		button btn_up "Up" width:50 offset:[26,0]
		button btn_removeDecal "Remove Selected" width:150 across:2 offset:[24,0]
		button btn_down "Down" width:50 offset:[26,0]
	)
	
	group "Target Object"
	(
		pickButton pbtn_Target "Pick Target Terrain" filter:pickValidTarget width:200
	)
	
	group "Bake Options"
	(
		label lbl_rayOffset "Ray Offset:" align:#left across:2
		spinner spn_rayOffset range:[0.0, 10000.0, 1.0]
		label lbl_padding "Padding:" align:#left across:2
		spinner spn_padding range:[0, 100, 2] type:#integer
		label lbl_backColour "Background Colour:" offset:[-4,0] across:2
		colorPicker cpk_completeDiffuse align:#right color:(color 255 127 255) fieldWidth:58 height:21 offset:[2,0]
		dropDownList ddl_size "Size" items:(for entry in sizeArray collect (entry as string)) selection:2
		button btn_saveDir "Save Directory..." width:200
	)
	
	button btn_bake "Bake" width:220
	
	fn getLayerArray type =
	(
		local result = #()
		
		case type.count of
		(
			4:(result = #(3,5,7))
			8:(result = #(3,5,7,2,4,6,8))
		)
		
		result
	)
	
	fn modalConvertDialog =
	(
		rollout convertInProgress "Converting Mask..." width:256
		(
			local total = 0
			
			label lbl_time "Time: " align:#left
			label lbl_status "Collecting Alpha Values" align:#left
			Timer tmr_convert interval:100 active:true
			
			on convertInProgress open do 
			(
				if RSL_TerrainBaker.shaderType.type == #mask then
				(
					convertInProgress.title = "Converting " + (filenameFromPath RSL_TerrainBaker.lookupFilename)
				)
				else
				(
					convertInProgress.title = "Converting " + RSL_TerrainBaker.bakeSource.name
				)
				
				convertThread.RunWorkerAsync()
			)
			
			on tmr_convert tick do 
			(
				if convertThread.IsBusy then
				(
					local time = tmr_convert.ticks * 0.10
					lbl_time.text = "Time: " + (time as string) + " s"
				)
				else
				(
					DestroyDialog convertInProgress
				)
			)
		)
		
		CreateDialog convertInProgress style:#(#style_titlebar) modal:true
	)
	
	fn fileCreated s e =
	(
		appendIfUnique maskArray e.FullPath
		convertInProgress.lbl_status.text = "Created " + e.name
	)
	
	fn setupChannel object channel =
	(
		local fCount = (polyop.getNumMapFaces object 9)
		local vCount = (polyop.getNumMapVerts object 9)
		
		polyop.defaultMapFaces object channel
		polyop.setNumMapVerts object channel vCount
		
		for i = 1 to fCount do 
		(
			local mapFace = polyop.getMapFace object 9 i
			polyop.setMapFace object channel i mapFace
		)
	)
	
	fn createVertexAlphaMasks object =
	(
		if (polyop.getMapSupport object 9) then
		(
			local mapVertCount = polyop.getNumMapVerts object 9
			local vertexAlphaArray = #()
			vertexAlphaArray.count = mapVertCount
			
			for i = 1 to mapVertCount do 
			(
				local mapVert = polyOp.getMapVert object 9 i
				local col = dotNetObject "RSL_ImageOps.Point3" mapVert.x mapVert.y mapVert.z
				
				vertexAlphaArray[i] = MaskConverter.getAlpha col layerArray
			)
			
			for i = 1 to layerArray.count do
			(
				local channel = startMapChannel + layerArray[i]
				
				setupChannel object channel
			)
			
			for v = 1 to mapVertCount do 
			(
				for i = 1 to layerArray.count do 
				(
					local channel = startMapChannel + layerArray[i]
					local alpha = vertexAlphaArray[v][i]
					
					polyop.setMapVert object channel v ([0,0,0] + alpha)
				)
			)
			
			true
		)
		else
		(
			false
		)
	)
	
	fn convert =
	(
		if shaderType.type == #mask then
		(
			local finalLookup = if lookupResized != undefined then lookupResized else lookupFilename
			
			fileWatcher.path = (getFilenamePath finalLookup)
			fileWatcher.EnableRaisingEvents = true
			
			MaskConverter.createAlphaMaps finalLookup layerArray
			
			fileWatcher.EnableRaisingEvents = false
		)
		else
		(
			vertexMaskSucess = (createVertexAlphaMasks bakeSource)
		)
	)
	
	fn getDiffuseLayers object =
	(
		local result = #()
		
		case (classOf object.material) of
		(
			Rage_Shader:
			(
				local count = RstGetVariableCount object.material
				
				for i = 1 to count do
				(
					if RstGetVariableType object.material i == "texmap" then
					(
						if (MatchPattern (RstGetVariableName object.material i) pattern:"Diffuse*") then
						(
							append result (RstGetVariable object.material i)
						)
					)
				)
			)
-- 			DirectX_9_Shader:
-- 			(
-- 			)
		)
		
		result
	)
	
	fn textureArrayFromFiles array channel:1 =
	(
		local result = #()
		result.count = array.count
		
		for i = 1 to array.count do 
		(
			local entry = array[i]
			
			if entry != undefined then
			(
				local texMap = Bitmaptexture bitmap:(openBitMap entry)
				texMap.coords.mapChannel = channel
				
				result[i] = texMap
			)
			else
			(
				result[i] = entry
			)
		)
		
		result
	)
	
	fn VCArrayFromLayers =
	(
		local result = #()
		
		for i = 1 to layerArray.count do 
		(
			result[i] = VertexColor map:(startMapChannel + layerArray[i])
		)
		
		result
	)
	
	fn createVertexMaterial object =
	(
		local diffuseLayers = getDiffuseLayers object
		local renderMaterial = Standardmaterial name:"RSL_BakeSource"
		
		local compMap = CompositeMap()
		
		for i = 1 to shaderType.count - 1 do 
		(
			compMap.add()
		)
		
		local diffuseTexArray = textureArrayFromFiles diffuseLayers
		local maskTexArray = VCArrayFromLayers()
		insertItem undefined maskTexArray 1
		
		compMap.mapList = diffuseTexArray
		compMap.mask = maskTexArray
		
		renderMaterial.diffuseMap = compMap
		
		object.material = renderMaterial
	)
	
	fn getMaskFile object =
	(
		local result = false
		lookupFilename = undefined
		
		case (classOf object.material) of
		(
			Rage_Shader:
			(
				local count = RstGetVariableCount object.material
				
				if count != undefined and count > 0 then
				(
					for i = 1 to count do
					(
						if RstGetVariableType object.material i == "texmap" then
						(
							if (RstGetVariableName object.material i) == "Lookup texture" then
							(
								local fileName = RstGetVariable object.material i
								
								if (doesFileExist fileName) then
								(
									lookupFilename = fileName
									result = true
								)
							)
						)
					)
				)
			)
-- 			DirectX_9_Shader:
-- 			(
-- 				local fileName = (object.material.geteffectbitmap (object.material.numberofbitmaps())).filename
-- 				
-- 				if (doesFileExist fileName) then
-- 				(
-- 					lookupFilename = fileName
-- 					result = true
-- 				)
-- 			)
		)
		
		result
	)
	
	
	fn createMaskMaterial object maskArray =
	(
		local diffuseLayers = getDiffuseLayers object
		local renderMaterial = Standardmaterial name:"RSL_BakeSource"
		
		local compMap = CompositeMap()
		
		for i = 1 to shaderType.count - 1 do 
		(
			compMap.add()
		)
		
		insertItem undefined maskArray 1
		
		local diffuseTexArray = textureArrayFromFiles diffuseLayers
		local maskTexArray = textureArrayFromFiles maskArray channel:2
		
		compMap.mapList = diffuseTexArray
		compMap.mask = maskTexArray
		
		renderMaterial.diffuseMap = compMap
		
		object.material = renderMaterial
	)
	
	fn checkSizeAndConvert =
	(
		local lookupBitmap = openBitMap lookupFilename
		local size = sizeArray[ddl_size.selection]
		
		if lookupBitmap.width > size.x or lookupBitmap.height > size.y then
		(
			if lookupBitmap.width > lookupBitmap.height then
			(
				local ratio = size.x / (lookupBitmap.width as float)
				
				size.y = ceil (lookupBitmap.height * ratio)
			)
			else
			(
				local ratio = size.y / (lookupBitmap.height as float)
				
				size.x = ceil (lookupBitmap.width * ratio)
			)
			
			lookupResized = (getFilenamePath lookupFilename) + (getFilenameFile lookupFilename) + "_temp.bmp"
			
			local newBitmap = bitmap size.x size.y filename:lookupResized
			copy lookupBitmap newBitmap
			
			save newBitmap
			close newBitmap
		)
	)
	
	fn preBake =
	(
		local result = true
		bakeSource = convertToPoly (copy pbtn_source.object)
		bakeSource.name = "RSL_BakeSource"
		bakeSource.isHidden = false
		
		shaderType = getShaderType bakeSource
		
		if shaderType != undefined then
		(
			layerArray = getLayerArray shaderType
			
			if shaderType.type == #vertex then
			(
				if not convertThread.IsBusy do
				(
					modalConvertDialog()
				)
				
				if vertexMaskSucess then
				(
					createVertexMaterial bakeSource 
				)
				else
				(
					result = false
				)
			)
			else
			(
				if (getMaskFile bakeSource) then
				(
					maskArray = #()
					lookupResized = undefined
					
					checkSizeAndConvert()
					
					if not convertThread.IsBusy do
					(
						modalConvertDialog()
					)
					
					createMaskMaterial bakeSource maskArray 
				)
				else
				(
					messageBox "Unable to get the lookup texture." title:"Error..."
					result = false
				)
			)
		)
		else
		(
			messageBox "The source terrain object does not have a terrain material or the terrain material is not currently supported.\nOnly Rage_Shaders are supported." title:"Error..."
			result = false
		)
		
		result
	)
	
	fn createRageShader name diffuseBitmap =
	(
		local newRageShader = Rage_Shader()
		newRageShader.name = name
		RstSetShaderName newRageShader "default.sps"
		
		local textureCount = getNumSubTexmaps newRageShader
		if (RstGetIsAlphaShader newRageShader) then
		(
			textureCount /= 2
		)
		
		for i = 1 to RstGetVariableCount newRageShader do
		(
			if ((RstGetVariableName newRageShader i) == "Diffuse Texture") then
			(
				RstSetVariable newRageShader i diffuseBitmap
			)
		)
		
		print newRageShader
		
		newRageShader
	)
	
	fn collectRageShaderAttributes rageShader = -- collects the variable names and values from the rage shader
	(
		local outArray = #(#(),#(),#())
		if (classOf rageShader) == Rage_Shader then
		(
			local count = RstGetVariableCount rageShader
			local isAlphaShader = RstGetIsAlphaShader rageShader
			
			append outArray[1] "Name"
			append outArray[2] "string"
			append outArray[3]  rageShader.name
			
			append outArray[1] "Shader"
			append outArray[2] "shaderList"
			append outArray[3]  (RstGetShaderName rageShader)
			
			local textureCount = 0
			
			--for i = 1 to count where (matchPattern (RstGetVariableType rageShader i) pattern:"Texmap") do textureCount += 1
			
			for i = 1 to count  do	(
				if (RstGetVariableType rageShader i) == "texmap" then textureCount += 1		
			)
			
			for i = 1 to count do
			(
				local name = (RstGetVariableName rageShader i)
				local variableType = (RstGetVariableType rageShader i)
				local variable = (RstGetVariable rageShader i)
				
				if (classOf variable) == float and variable < 0.01 then variable = 0.0
				
				append outArray[1] name
				append outArray[2] variableType
				append outArray[3] variable
				
				if name == "Diffuse Texture" and isAlphaShader == true then
				(
					local alphaTexture
					local subTexture = (getSubTexmap rageShader (textureCount + i))
					
					if subTexture != undefined then
					(
						alphaTexture = (getSubTexmap rageShader (textureCount + i)).filename
					)
					else
					(
						alphaTexture = ""
					)
					
					append outArray[1] "Alpha Texture"
					append outArray[2] "texmap"
					append outArray[3] alphaTexture
				)
			)
		)
		outArray
	)
	
	fn createStandardMaterial rageShaderProperties =
	(
		local material = StandardMaterial()
		
		local isDecal = false
		
		for i = 1 to rageShaderProperties[1].count do
		(
			case (rageShaderProperties[1][i]) of
			(
				"Name":
				(
					material.name = rageShaderProperties[3][i]
				)
				"Shader":
				(
					isDecal = (matchPattern rageShaderProperties[3][i] pattern:"*decal*")
					
-- 					if isDecal then
-- 					(
-- 						switchAlphaToVertex object
-- 					)
				)
				"Diffuse Texture":
				(
					local diffuseAndSpecularColour = bitmaptexture filename:rageShaderProperties[3][i]
					diffuseAndSpecularColour.alphasource  = 2
					
					material.diffuseMap = diffuseAndSpecularColour
					material.specularMap = diffuseAndSpecularColour
				)
				"Alpha Texture":
				(
					if isDecal then
					(
						opacityMap = mix map2:opacityTexure mask:(VertexColor())
						
						material.opacityMap = opacityMap
					)
				)
-- 				"Bump Texture":
-- 				(
-- 					local normal = Normal_Bump normal_map:(bitmaptexture filename:rageShaderProperties[3][i])
-- 					
-- 					material.bumpMap = normal
-- 					material.bumpMapAmount = 100
-- 				)
-- 				"Specular Texture":
-- 				(
-- 					local specularLevelAndGlossiness = bitmaptexture filename:rageShaderProperties[3][i]
-- 					specularLevelAndGlossiness.alphasource  = 2
-- 					
-- 					material.specularLevelMap = specularLevelAndGlossiness
-- 					material.glossinessMap = specularLevelAndGlossiness
-- 				)
-- 				"specular map intensity mask color":
-- 				(
-- 					material.Specular = (rageShaderProperties[3][i] * 255) as color
-- 				)
-- 				"Specular Falloff":
-- 				(
-- 					material.specularLevel = (convertSpecularValues (rageShaderProperties[3][i] as float) (rageShaderProperties[3][i] as float)).x
-- 				)
-- 				"Specular Intensity":
-- 				(
-- 					material.glossiness = (convertSpecularValues (rageShaderProperties[3][i] as float) (rageShaderProperties[3][i] as float)).y
-- 				)
-- 				"Environment Texture":
-- 				(
-- 					local reflectionMap = bitmaptexture filename:rageShaderProperties[3][i]
-- 					reflectionMap.alphasource  = 2
-- 					
-- 					material.reflectionMap = reflectionMap
-- 				)
-- 				"Reflectivity":
-- 				(
-- 					material.reflectionMapAmount = (rageShaderProperties[3][i] as float)
-- 				)
				
-- 				default:
-- 				(
-- 					format "Name: % Type: % Variable: %\n" rageShaderProperties[1][i] rageShaderProperties[2][i] rageShaderProperties[3][i]
-- 				)
			)
		)
		
		material
	)
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- findID() Searches the parsed array for any item with the same ID as the parsed ID. Returns the index if found or 0
	---------------------------------------------------------------------------------------------------------------------------------------
	fn findID array ID =
	(
		local result = 0
		
		for i = 1 to array.count where array[i] != undefined do 
		(
			if array[i].ID == ID then
			(
				result = i
			)
		)
		
		result
	)
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getIDFaceArray() Gathers a dataPair for each unique material ID found on the object and an array of faces assigned that ID.
	-- Returns an array of all the unique dataPairs
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getIDFaceArray object =
	(
		local result = #()
		
		for face in object.faces do 
		(
			-- get the ID from the face
			local ID = (polyOp.getFaceMatID object face.index)
			
			-- check that we don't alreadi have a dataPair for that ID
			local index = findID result ID
			
			-- if we do then append the face.index to the faceArray of the found dataPair. Other wise we
			-- create a new dataPair and append it to the result array
			if index > 0 then
			(
				append result[index].faceArray face.index
			)
			else
			(
				append result (dataPair ID:ID faceArray:#(face.index))
			)
		)
		
		result
	)
	
	fn getNonDecalFaces IDArray material =
	(
		local result = #()
		
		for i = IDArray.count to 1 by -1 do 
		(
			local entry = IDArray[i]
			local idx = findItem material.materialIDList entry.ID
			
			if idx > 0 then
			(
				local subMat = material.materialList[idx]
				
				if (classOf subMat) == Rage_Shader then
				(
					if not (matchPattern (RstGetShaderName subMat) pattern:"*decal*") then
					(
						join result entry.faceArray
						deleteItem IDArray i
					)
				)
				else
				(
					join result entry.faceArray
					deleteItem IDArray i
				)
			)
			else
			(
				deleteItem IDArray i
			)
		)
		
		result
	)
	
	fn switchAlphaToVertex object =
	(
		if (polyop.getMapSupport object -2) then
		(
			convertToPoly object
			
			polyop.defaultMapFaces object 0
			
			local mapVertCount = polyop.getNumMapVerts object -2
			polyop.setNumMapVerts object 0 mapVertCount keep:false
			
			for face in object.faces do
			(
				local mapVertIndices = polyop.getMapFace object -2 face.index
				
				polyop.setMapFace object 0 face.index mapVertIndices
				
				local mapVertArray = for index in mapVertIndices collect (polyop.getMapVert object -2 index)
				
				for i = 1 to mapVertArray.count do
				(
					polyop.setMapVert object 0 mapVertIndices[i] mapVertArray[i]
				)
			)
		)
	)	
	fn processDecal object =
	(
		local result = copy (convertToPoly object)
		result.name = "RSL_DecalSource"
		
		local material = object.material
		
		case (classOf material) of
		(
			rage_shader:
			(
				local shaderProps = collectRageShaderAttributes material
				
				if (matchPattern shaderProps[3][2] pattern:"*decal*") then
				(
					result.material = createStandardMaterial shaderProps
				)
				else
				(
					delete result
					result = undefined
				)
			)
			MultiMaterial:
			(
				local IDArray = getIDFaceArray result
				
				local faceArray = getNonDecalFaces IDArray material
				
				if faceArray.count == object.faces.count then
				(
					delete result
					result = undefined
				)
				else
				(
					switchAlphaToVertex result
					
					if faceArray.count > 0 then
					(
						polyop.deleteFaces result faceArray delIsoVerts:true
					)
					
					local newMulti = multimaterial numSubs:(IDArray.count)
					
					for i = 1 to IDArray.count do 
					(
						local ID = IDArray[i].ID
						local idx = findItem material.materialIDList ID
						local subMat = material.materialList[idx]
						
						local shaderProps = collectRageShaderAttributes subMat
						local newMaterial = createStandardMaterial shaderProps
						
						newMulti.MaterialList[i] = newMaterial
						newMulti.MaterialIDList[i] = ID
					)
					
					result.material = newMulti
				)
			)
		)
		
		result
	)
	
	mapped fn bakeDecal object projMod bakeProperties bakeProjProperties size =
	(
		projMod.deleteAll()
		
		local sourceDecal = processDecal object
		
		if sourceDecal != undefined then
		(
			projMod.addObjectNode sourceDecal
			
			bakeProperties.removeAllBakeElements()
			
			for i = 1 to (projMod.numGeomSels()) do
			(
				bakeProjProperties.projectionModTarget = projMod.getGeomSelName i
			)
			
			local outputName = object.name
			
			local outputFile = (outputDir + "\\" +  outputName + "_Decal_D.bmp") 
			
			local diffuseMapRender = DiffuseMap outputSzX:size.x outputSzY:size.y
			diffuseMapRender.fileName = (getFilenameFile outputFile) 
			diffuseMapRender.fileType = outputFile
			diffuseMapRender.backgroundColor = cpk_completeDiffuse.color
			
			bakeProperties.addBakeElement diffuseMapRender
			
			outputFile = (outputDir + "\\" +  outputName + "_Decal_A.bmp") 
			
			local alphaMapRender = AlphaMap outputSzX:size.x outputSzY:size.y
			alphaMapRender.fileName = (getFilenameFile outputFile) 
			alphaMapRender.fileType = outputFile
			alphaMapRender.backgroundColor = black
			
			bakeProperties.addBakeElement alphaMapRender
			
			renderBitmap = bitmap size.x size.y
			render rendertype:#bakeSelected to: renderBitmap vfb:true ditherTrueColor:false
			close renderBitmap
			
			insertItem (DataPair diffuse:diffuseMapRender.bitmap alpha:alphaMapRender.bitmap) decalLayerArray 1
			
-- 			append decalLayerArray (diffuse:diffuseMapRender.bitmap alpha:alphaMapRender.bitmap)
			
			delete sourceDecal
		)
	)
	
	fn bake =
	(
		freeSceneBitmaps()
		gc()
		
		select pbtn_Target.object
		
		local projMod = projection()
		addModifier pbtn_Target.object projMod
		
		local size = sizeArray[ddl_size.selection]
		local bakeProperties = pbtn_Target.object.INodeBakeProperties
		bakeProperties.bakeChannel = 1
		bakeProperties.bakeEnabled = true
		
		local bakeProjProperties = pbtn_Target.object.INodeBakeProjProperties
		bakeProjProperties.projectionMod = projMod
		bakeProjProperties.enabled = true
		bakeProjProperties.warnRayMiss = false
		bakeProjProperties.useCage = false
		bakeProjProperties.rayOffset = spn_rayOffset.value
		bakeProjProperties.hitResolveMode = #closest --ddl_hitType.selected as name --#furthest --#closest
		
		if decalSourceArray.count > 0 then
		(
			decalLayerArray = #()
			
			pbtn_Target.object.material = Standardmaterial diffuse:cpk_completeDiffuse.color opacity:0.0 selfIllumAmount:100.0
			
			bakeProperties.nDilations = 0
			bakeProjProperties.hitWorkingModel = true 
			
			bakeDecal decalSourceArray projMod bakeProperties bakeProjProperties size
			
			projMod.deleteAll()
		)
		
		projMod.addObjectNode bakeSource
		
		bakeProperties.removeAllBakeElements()
		bakeProperties.nDilations = spn_padding.value
		
		for i = 1 to (projMod.numGeomSels()) do
		(
			bakeProjProperties.projectionModTarget = projMod.getGeomSelName i
		)
		
		bakeProjProperties.hitWorkingModel = false
		
		local outputFile = (outputDir + "\\" +  pbtn_Target.object.name + "_D.bmp") 
		
		if (doesFileExist outputFile) and (getFileAttribute outputFile #readOnly) then
		(
			messageBox (outputFile + " is read only. Nothing will be rendered.") title:"Error..."
		)
		else
		(
			if (doesFileExist outputFile) then
			(
				deleteFile outputFile
			)
			
			local diffuseMapRender = DiffuseMap outputSzX:size.x outputSzY:size.y
			diffuseMapRender.fileName = (getFilenameFile outputFile) 
			diffuseMapRender.fileType = outputFile
			diffuseMapRender.backgroundColor = cpk_completeDiffuse.color
			
			bakeProperties.addBakeElement diffuseMapRender
			
			local oldFilter = scanlineRender.antiAliasFilter
			scanlineRender.antiAliasFilter = Catmull_Rom()
			
			if renderBitmap != undefined then
			(
				close renderBitmap
			)
			
			renderBitmap = bitmap size.x size.y
			
			render rendertype:#bakeSelected to:renderBitmap vfb:true ditherTrueColor:false
			
			close renderBitmap
			
			scanlineRender.antiAliasFilter = oldFilter
			
			if decalLayerArray.count > 0 then
			(
				for entry in decalLayerArray do 
				(
					decalCompositer.CompositeDecals entry.diffuse.filename entry.alpha.filename outputFile
				)
			)
			
			display (openBitMap outputFile)
			
			pbtn_Target.object.material = (createRageShader pbtn_Target.object.name outputFile)
			showTextureMap pbtn_Target.object.material true
			
			bakeProperties.bakeEnabled = false
			bakeProjProperties.enabled = false
			
			convertToPoly pbtn_Target.object
		)
		
		delete bakeSource
		
		if lookupResized != undefined then
		(
			deleteFile lookupResized
		)
		
		if maskArray.count > 0 then
		(
			for entry in maskArray where entry != undefined do deleteFile entry
			
			maskArray = #()
		)
	)
	
	fn enableBake =
	(
		(isValidNode pbtn_source.object) and (isValidNode pbtn_Target.object) and outputDir != undefined
	)
	
	
	
	
	
	
	on RSL_TerrainBaker open do
	(
		outputDir = getINISetting INIFile "Main" "OutputDir"
		if outputDir != "" then
		(
			local subbed = substituteString outputDir "\\\\" "/"
			local subbed = substituteString (toLower subbed) (project.art) ""
			
			btn_saveDir.text = subbed
		)
		else
		(
			outputDir = undefined
		)
		
		local rayOffset = getINISetting INIFile "Main" "rayOffset"
		if rayOffset != "" then
		(
			spn_rayOffset.value = readValue (rayOffset as StringStream)
		)
		
		local padding = getINISetting INIFile "Main" "padding"
		if padding != "" then
		(
			spn_padding.value = readValue (padding as StringStream)
		)
		
		local backColour = getINISetting INIFile "Main" "backColour"
		if backColour != "" then
		(
			cpk_completeDiffuse.color = readValue (backColour as StringStream)
		)
		
		dotNet.loadAssembly (RsConfigGetWildWestDir()+"/script/max/rockstar_london/dll/RSL_ImageOps.dll")
		MaskConverter = dotNetObject "RSL_ImageOps.TerrainMaskConverter"
		decalCompositer = dotNetObject "RSL_ImageOps.DecalCompositer"
		
		convertThread = dotNetObject "CSharpUtilities.SynchronizingBackgroundWorker"
		dotNet.addEventHandler convertThread "DoWork" convert
		
		fileWatcher = dotNetObject "System.IO.FileSystemWatcher"
		dotNet.addEventHandler fileWatcher "Created" fileCreated
		dotNet.addEventHandler fileWatcher "Changed" fileCreated
		
		btn_bake.enabled = enableBake()
	)
	
	on RSL_TerrainBaker close do 
	(
		
	)
	
	on pbtn_source picked node do 
	(
		pbtn_source.text = node.name
		
		btn_bake.enabled = enableBake()
	)
	
	on btn_fromSelection pressed do 
	(
		local selectionArray = selection as array
		
		if selectionArray.count > 0 then
		(
			for object in selectionArray do
			(
				if (pickValidDecalObject object) then
				(
					appendIfUnique decalSourceArray object
				)
			)
			
			if decalSourceArray.count == 0 then
			(
				messageBox "No objects were added. None of them are valid bake objects." title:"Error..."
			)
			else
			(
				mlbx_decals.items = for object in decalSourceArray collect object.name
			)
		)
		else
		(
			messageBox "No objects selected." title:"Error..."
		)
	)
	
	on btn_up pressed do 
	(
		local selArray = #{}
		selArray.count = mlbx_decals.items.count
		
		for index in (mlbx_decals.selection as array) do 
		(
			if index > 1 then
			(
				swap decalSourceArray[index] decalSourceArray[index - 1]
				
				selArray[index - 1] = true
			)
		)
		
		mlbx_decals.items = for object in decalSourceArray collect object.name
		mlbx_decals.selection = selArray
	)
	
	on btn_removeDecal pressed do 
	(
		for i = mlbx_decals.selection.count to 1 by -1 do
		(
			if mlbx_decals.selection[i] then
			(
				deleteItem decalSourceArray i
			)
		)
		
		mlbx_decals.items = for object in decalSourceArray collect object.name
	)
	
	on btn_down pressed do
	(
		local selArray = #{}
		selArray.count = mlbx_decals.items.count
		
		for index in (mlbx_decals.selection as array) do 
		(
			if index < decalSourceArray.count then
			(
				swap decalSourceArray[index] decalSourceArray[index + 1]
				
				selArray[index + 1] = true
			)
		)
		
		mlbx_decals.items = for object in decalSourceArray collect object.name
		mlbx_decals.selection = selArray
	)
	
	on pbtn_Target picked node do 
	(
		pbtn_Target.text = node.name
		
		btn_bake.enabled = enableBake()
	)
	
	on spn_rayOffset changed val do 
	(
		setINISetting INIFile "Main" "rayOffset" (val as string)
	)
	
	on spn_padding changed val do 
	(
		setINISetting INIFile "Main" "padding" (val as string)
	)
	
	on cpk_completeDiffuse changed colour do 
	(
		setINISetting INIFile "Main" "backColour" (colour as string)
	)
	
	on btn_saveDir pressed do 
	(
		outputDir = getsavePath caption:"Pick Output Directory..." initialDir:(project.art + "\\textures\\Environments\\LOD\\")
		
		if outputDir != undefined then
		(
			outputDir = substituteString outputDir "\\" "\\\\"
			
			setINISetting INIFile "Main" "OutputDir" outputDir
			
			local subbed = substituteString outputDir "\\\\" "/"
			local subbed = substituteString (toLower subbed) (project.art) ""
			
			btn_saveDir.text = subbed
		)
		else
		(
			btn_saveDir.text = "Pick Output Directory"
		)
		
		btn_bake.enabled = enableBake()
	)
	
	on btn_bake pressed do 
	(
		if (preBake()) then
		(
			bake()
		)
	)
)

CreateDialog RSL_TerrainBaker style:#(#style_toolwindow,#style_sysmenu)