filein "RSL_Global.ms"

fn CleverResetXFrom obj =
(
		maxOps.CollapseNode obj true
	
		-- Get the original node transform matrix
		local ntm = obj.transform
		
		-- The new object transform only inherits position
		obj.transform=transMatrix obj.pos
		
		-- Compute the pivot transform matrix
		local piv=obj.objecttransform * inverse obj.transform
		
		-- Reset the object offsets to 0
		obj.objectoffsetPos  = [0,0,0]
		obj.objectoffsetRot = (quat 0 0 0 1)
		obj.objectoffsetScale = [1,1,1]
		
		-- Take the position out of the original node transform matrix since we don't reset position
		ntm.translation=[0,0,0]
		
		-- Apply the pivot transform matrix to the modified original node transform matrix
		-- to get the XForm gizmo transform
		ntm = piv * ntm
		
		-- apply an XForm modifier to the node
		local xformMod=xform()
		addmodifier obj xformMod
		
		-- set the XForm modifier's gizmo tranform
		xformMod.gizmo.transform=ntm
		
		maxOps.CollapseNode obj true
)

fn StandardResetXFrom obj =
(
	maxOps.CollapseNode obj true
	
	--XFromModifer = XForm()
	
	--addModifier obj XFromModifer
	
	ResetXForm obj
	
	maxOps.CollapseNode obj true
)

fn ResetObjXForm obj = 
	(
		--print obj.name
		IsCollisionMesh = false
		ParentNode = obj.parent
		
		obj.parent = undefined
		
		if (classOf obj == Col_Mesh) then 
			(
				IsCollisionMesh = true
			)
		
		if (IsCollisionMesh == true) then
			(
				col2mesh obj
				obj.pivot = ParentNode.pivot --Align the pivot of the collision mesh to the pivot of the parent mesh.
			)
		
		if (IsGeometryType obj) then
			(
			--CleverResetXFrom obj
			StandardResetXFrom obj
			)
			
		if (IsCollisionMesh == true) then 
			(
				mesh2col obj
			)
			
		if (IsParticleType obj) then
			(
				obj.scale = [1,1,1]			
			)
			
		obj.parent = ParentNode
	)
	
fn FixPivot obj = 
	(
		
	loc_children = #()
	
	--print ("Fixing " + obj.name)
	
	--Make a list of the children
	for childObj in obj.children do 
		(
		append loc_children childObj
		)
		
	--Remove links to children
	for i = 1 to obj.children.count do
		(
		deleteItem obj.children obj.children[1]
		i += 1
		)

	--Reset the transform of object
 	ResetObjXForm obj
		
	--Relink the children
 	for i = 1 to loc_children.count do
		(
		append obj.children loc_children[i]
		i += 1
		)
		
	--Iterate over children last
	for childObj in obj.children do
		(
		FixPivot childObj
		)
	)

