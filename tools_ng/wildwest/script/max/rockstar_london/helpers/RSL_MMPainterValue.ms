

struct RSL_MMPainterValue
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- Struct variables
	---------------------------------------------------------------------------------------------------------------------------------------
	
	mouseDown = false,
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- UI variables
	---------------------------------------------------------------------------------------------------------------------------------------
	valueForm,
	valuePicture,
	valueNumeric,valueBar,
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- setValueImage()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn setValueImage =
	(
		if valuePicture != undefined then
		(
			try (valuePicture.Image.dispose()) catch()
			
			local colour = [0,0,0]
			
			if RSL_Painter.currentChannel.r then colour.x = 1.0
			if RSL_Painter.currentChannel.g then colour.y = 1.0
			if RSL_Painter.currentChannel.b then colour.z = 1.0
			
			colour *= 255
			
			local bitmapImage = dotNetObject RS_dotNetClass.bitmapClass valuePicture.size.Width valuePicture.size.Height
			local graphics = (dotNetClass "System.Drawing.Graphics").FromImage bitmapImage
			
			local startColour = RS_dotNetClass.colourClass.fromARGB 0 0 0
			local endColour = RS_dotNetClass.colourClass.fromARGB colour.x colour.y colour.z
			local startPoint = RS_dotNetObject.pointObject [0,0]
			local endPoint = RS_dotNetObject.pointObject [valuePicture.size.Width,0]
			
			local gradientBrush = dotNetObject "System.Drawing.Drawing2D.LinearGradientBrush" startPoint endPoint startColour endColour
			
			graphics.FillRectangle gradientBrush (dotNetObject RS_dotNetClass.rectangleClass 0 0 valuePicture.size.Width valuePicture.size.Height)
			
			valuePicture.Image = bitmapImage
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getColourValue()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getColourValue =
	(
		local result = 0
		
		if RSL_Painter.currentChannel.r then result = RSL_Painter.valueButton.backColor.r
		if RSL_Painter.currentChannel.g then result = RSL_Painter.valueButton.backColor.g
		if RSL_Painter.currentChannel.b then result = RSL_Painter.valueButton.backColor.b
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- UI FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- disableAccelerators() Called when the user enters any control that needs keyboard input e.g. a textBox
	---------------------------------------------------------------------------------------------------------------------------------------
	fn disableAccelerators s e =
	(
		enableAccelerators = false
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOnFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOnFormClosing s e =
	(
		RSL_Painter.colourValue.OnFormClosing s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- OnFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn OnFormClosing s e =
	(
		setINISetting RSL_Painter.INIfile "Colour Value" "Location" ([valueForm.location.x, valueForm.location.y] as string)
		
		valueForm = undefined
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- OnFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchValueChanged s e =
	(
		RSL_Painter.colourValue.valueChanged s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- OnFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn valueChanged s e =
	(
		case s.name of
		(
			"valueNumeric":
			(
				valueBar.value = s.value
			)
			"valueBar":
			(
				valueNumeric.value = s.value
			)
		)
		
		local colour = [0,0,0]
		
		if RSL_Painter.currentChannel.r then colour.x = s.value
		if RSL_Painter.currentChannel.g then colour.y = s.value
		if RSL_Painter.currentChannel.b then colour.z = s.value
		
		RSL_Painter.valueButton.backColor = RS_dotNetClass.colourClass.fromARGB colour.x colour.y colour.z
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchValueMouseDown()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchValueMouseDown s e =
	(
		RSL_Painter.colourValue.valueMouseDown s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- valueMouseDown()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn valueMouseDown s e =
	(
		mouseDown = true
		
		local percent = (e.x as float) / s.size.width * 100.0
		
		local value = int (floor (255.0 / 100.0 * percent))
		if value > 255 then value = 255
		if value < 0 then value = 0
		
		local colour = [0,0,0]
		
		if RSL_Painter.currentChannel.r then colour.x = value
		if RSL_Painter.currentChannel.g then colour.y = value
		if RSL_Painter.currentChannel.b then colour.z = value
		
		RSL_Painter.valueButton.backColor = RS_dotNetClass.colourClass.fromARGB colour.x colour.y colour.z
		
		valueBar.value = value
		valueNumeric.value = value
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchValueMouseMove()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchValueMouseMove s e =
	(
		RSL_Painter.colourValue.valueMouseMove s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- valueMouseMove()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn valueMouseMove s e =
	(
		if mouseDown then
		(
			local percent = (e.x as float) / s.size.width * 100.0
			
			local value = int (floor (255.0 / 100.0 * percent))
			if value > 255 then value = 255
			if value < 0 then value = 0
			
			local colour = [0,0,0]
			
			if RSL_Painter.currentChannel.r then colour.x = value
			if RSL_Painter.currentChannel.g then colour.y = value
			if RSL_Painter.currentChannel.b then colour.z = value
			
			RSL_Painter.valueButton.backColor = RS_dotNetClass.colourClass.fromARGB colour.x colour.y colour.z
			
			valueBar.value = value
			valueNumeric.value = value
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchValueMouseUp()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchValueMouseUp s e =
	(
		RSL_Painter.colourValue.valueMouseUp s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- valueMouseUp()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn valueMouseUp s e =
	(
		mouseDown = false
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- optionsDialog()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn optionsDialog =
	(
		--In this case, we're going to use a standard form rather than the custom maxForm written for max. this is because I want to change the 
		-- form back colour which you can't do if you use the custom maxForm. For this to work, we need to set the parent of the form to be Max. 
		-- USING THIS TYPE OF FORM MEANS WE NEED TO DISSABLE ACCELERATORS WHEN WE FOCUS ON A CONTROL THAT TAKES
		-- KEY PRESSES SUCH AS SPINNERS AND TEXT BOXES
		
		--Get the max handle pointer.
		local maxHandlePointer=(Windows.GetMAXHWND())
		
		--Convert the HWND handle of Max to a dotNet system pointer
		local sysPointer = DotNetObject "System.IntPtr" maxHandlePointer
		
		--Create a dotNet wrapper containing the maxHWND
		local maxHwnd = DotNetObject "MaxCustomControls.Win32HandleWrapper" sysPointer
		
		valueForm = RS_dotNetUI.form "valueForm" text:" Colour Value" size:[256, 96] borderStyle:RS_dotNetPreset.FB_FixedToolWindow
		valueForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		dotnet.addEventHandler valueForm "FormClosing" dispatchOnFormClosing
		dotnet.addEventHandler valueForm "Enter" disableAccelerators
		
		valueLabel = RS_dotNetUI.Label "valueLabel" text:(" Colour Value:") \
												textAlign:RS_dotNetPreset.CA_MiddleLeft borderStyle:RS_dotNetPreset.BS_None \
												dockStyle:RS_dotNetPreset.DS_Left
		
		local currentValue = getColourValue()
		
		valueNumeric = RS_dotNetUI.Numeric "valueNumeric" value:currentValue range:#(255, 0, 1, 0) dockStyle:RS_dotNetPreset.DS_Right \
								margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler valueNumeric "Enter" disableAccelerators 
		dotnet.addEventHandler valueNumeric "ValueChanged" dispatchValueChanged
		
		valuePicture = RS_dotNetUI.pictureBox "valuePicture" size:[218, 16] location:[19,0] borderStyle:RS_dotNetPreset.BS_FixedSingle dockStyle:RS_dotNetPreset.DS_None
		valuePicture.Anchor = (dotNetClass "System.Windows.Forms.AnchorStyles").Top
		valuePicture.autoSize = false
		dotnet.addEventHandler valuePicture "MouseDown" dispatchValueMouseDown
		dotnet.addEventHandler valuePicture "MouseMove" dispatchValueMouseMove
		dotnet.addEventHandler valuePicture "MouseUp" dispatchValueMouseUp
		
		setValueImage()
		
		valueBar = RS_dotNetUI.trackBar "valueBar" range:#(255, 0, currentValue) dockStyle:RS_dotNetPreset.DS_Fill
		valueBar.TickStyle = (dotNetClass "System.Windows.Forms.TickStyle").TopLeft
		valueBar.TickFrequency = 128
		dotnet.addEventHandler valueBar "ValueChanged" dispatchValueChanged
		
		valueTableLayout = RS_dotNetUI.tableLayout "valueTableLayout" text:"valueTableLayout" collumns:#((dataPair type:"Percent" value:50),(dataPair type:"Absolute" value:50)) \
									rows:#((dataPair type:"Absolute" value:24),(dataPair type:"Absolute" value:16),(dataPair type:"Absolute" value:40)) dockStyle:RS_dotNetPreset.DS_Fill
		
		valueTableLayout.controls.add valueLabel 0 0
		valueTableLayout.controls.add valueNumeric 1 0
		valueTableLayout.controls.add valuePicture 0 1
		valueTableLayout.controls.add valueBar 0 2
		
		valueTableLayout.setColumnSpan valuePicture 2
		valueTableLayout.setColumnSpan valueBar 2
		
		valueForm.controls.add valueTableLayout
		
		local INIlocation = getINISetting RSL_Painter.INIfile "Colour Value" "Location"
		
		if INIlocation.count > 0 then
		(
			local loc = readValue (INIlocation as stringStream)
			valueForm.location.x = loc.x
			valueForm.location.y = loc.y
		)
		else
		(
			valueForm.location.x = RSL_Painter.painterForm.location.x
			valueForm.location.y = RSL_Painter.painterForm.location.x
		)
		
		valueForm.show maxHwnd
	)
)