RsSetMenu "Rockstar Payne" #(
) menuParentName:"RS Wildwest"




RsSetMenu "Character" #(			
			"CharacterSwapper",
			"CharacterBatchExport",
			"",
			"AmbientLighting"

) menuParentName:"Rockstar Payne"

RsSetMenu "Collision" #(
			"CollisionOps",
			"",
			"AutoSurfaceTypeAssign",
			"AutoWindowTypeSize"

) menuParentName:"Rockstar Payne"

RsSetMenu "Design" #(
			"PESSceneEditor",
			"PESSceneSearcher",
			"",
			"SimpleLevelCreator",	
			"",	
			"ScriptHelper",	
			"",
			"FireBoxExporter",						
			"TriggerBoxHelpUI",
			"LevelCleaner"

) menuParentName:"Rockstar Payne"

RsSetMenu "Helpers" #(
			"IPLStreamExport",
			"",
			"ObjectTypeOps",
			"",
			"Toolbox",
			"",
			"AutoSurfaceTypeAssign",
			"AutoWindowTypeSize",
			"",
			"VehicleAnimator"

) menuParentName:"Rockstar Payne"

RsSetMenu "Interior" #(
			"MiloOps",
			"",
			"MiloCollisionToolV1",
			"MiloInfo"
			

) menuParentName:"Rockstar Payne"

RsSetMenu "Misc" #(
			"FragTuner",
			"GrassPainter"		
		

) menuParentName:"Rockstar Payne"

RsSetMenu "Optimization" #(
			"RSLSceneStats",
			"RSLTextureStats",
			"",
			"SceneBrowser",
			"SceneCleaner",
			"",
			"AutoLODDistance",
			"SetShadowCasting",
			"",	
			"lodOptimizerTools",
			"",			
			"LightLister",
			"",
			"LodBaker",
			"LodEditor"

) menuParentName:"Rockstar Payne"

RsSetMenu "Particle" #(
			"ParticleHelper",
			"",
			"ValidateParticles"

) menuParentName:"Rockstar Payne"

RsSetMenu "Prop" #(
			"PropViewer",
			"",
			"PropPlacer",
			"PropDictionaryEditorDotNet",
			"",
			"PropThumbsRenderer",
			"BatchPropRender",
			"MergeProp",
			"",
			"NoAICover",
			"",
			"objectfinder"


) menuParentName:"Rockstar Payne"

RsSetMenu "Shaders" #(
			"RageShaderValueEditor",
			"",
			"GTAToRage",
			"MaterialConverter",
			"XRefMaterialTools"

) menuParentName:"Rockstar Payne"

RsSetMenu "Spatial Data" #(			
			"CoverEdge"


) menuParentName:"Rockstar Payne"


RsSetMenu "Texture" #(			
			"TextureTagMenu",
			"",
			"TXDOps",
			"TXDOptimiserV2",
			"",
			"TXDViewer"

) menuParentName:"Rockstar Payne"

RsSetMenu "Utils" #(			
			"PathsRemapTool",
			"",			
			"ModelRenderer",			
			"PropTxdViewer",			
			"VehicleEditor",
			"",
			"MapRenderSetup",
			"",
			"FixMyMap",
			"",
			"DeleteEmptyLayers"
			

) menuParentName:"Rockstar Payne"