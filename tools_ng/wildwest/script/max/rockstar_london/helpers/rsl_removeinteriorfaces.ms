
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/Utils/RSL_INIOperations.ms")

try (destroyDialog RSL_RemoveInteriorFaces) catch()

rollout RSL_RemoveInteriorFaces "Remove Interior Faces"
(
	local faceDataChannel
	local RSL_meshCleanIsSelected = #(1699710735, -200361706)
	local localConfigINI = (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_RemoveInteriorFaces.ini")
	
	checkBox chkb_selected "Selected Elements" checked:true offset:[-8,0]
	checkBox chkb_weld "Weld Elements" checked:true across:2 offset:[-8,0]
	spinner spn_vertThreshold range:[0.0, 1.0, 0.0001] scale:0.0001 fieldWidth:40 offset:[9,0]
	button btn_faceRemove "Remove Interior Faces" width:154 offset:[-1,0]
	progressBar pbar_progress value:100.0 color:orange width:154 offset:[-9,0]
	
	fn isSimilar face1 face2 object =
	(
		local vertArray1 = (polyOp.getVertsUsingFace face1 object) as array
		local vertArray2 = (polyOp.getVertsUsingFace face2 object) as array
	)
	
	mapped fn setSelectedFace face theObject =
	(
		faceDataChannel.setValue face (findItem theObject.selectedFaces face > 0)
	)
	
	mapped fn getSelectedFace face &outArray =
	(
		if (faceDataChannel.getValue face) then
		(
			append outArray face
		)
	)
	
	fn setFaceDataForSelected theObject =
	(
		try
		(
			simpleFaceManager.removeChannel theObject RSL_meshCleanIsSelected
		)
		catch()
		
		faceDataChannel = simpleFaceManager.addChannel theObject type:#boolean id:RSL_meshCleanIsSelected name:"RSL_RemoveInteriorFaces is Selected Channel"
		
		if faceDataChannel != undefined then
		(
			setSelectedFace (for face in theObject.faces collect face.index) theObject
		)
	)
	
	fn faceRemove faceArray theObject =
	(
		setCommandPanelTaskMode #create
		local removeArray = #()
		
		for i = 1 to faceArray.count - 1 do
		(
			pbar_progress.value = (i as float / faceArray.count * 100.0)
			
			local face1 = faceArray[i]
			local center1 = polyOp.getFaceCenter theObject face1
			local normal1 =polyOp.getFaceNormal theObject face1
			
			for f = i + 1 to faceArray.count do
			(
				local face2 = faceArray[f]
				local center2 = polyOp.getFaceCenter theObject face2
				local normal2 =polyOp.getFaceNormal theObject face2
				
				if (distance center1 center2) < 0.005 and (dot normal1 normal2) < -0.98 then
				(
					removeArray += #(face1, face2) 
				)
				else
				(
-- 					if (distance center1 center2) < 0.005 then
-- 					(
-- 						format "Distance:% dot:%\n" (distance center1 center2) (dot normal1 normal2)
-- 					)
				)
			)
		)
		
		polyOp.setFaceSelection theObject removeArray
		update theObject
		
		theObject.delete #face deleteIsoVerts:true
		
		if chkb_weld.checked then
		(
			theObject.weldThreshold = spn_vertThreshold.value
			polyop.setVertSelection theObject #All
			
			polyOp.weldVertsByThreshold theObject theObject.selectedVerts
			
			theUnwrapMod = UVWUnwrap()
			addModifier theObject theUnwrapMod
			
			faceArray = #()
			getSelectedFace (for face in theObject.faces collect face.index) &faceArray
			
			local faces = (for face in theObject.faces where (findItem faceArray face.index) > 0 collect face.index) as bitArray
			
			theUnwrapMod.setTVSubObjectMode 3
			theUnwrapMod.setWeldThreshold spn_vertThreshold.value
			theUnwrapMod.selectFaces faces
			theUnwrapMod.weldSelected()
		)
		
		convertToPoly theObject
		
		setCommandPanelTaskMode #modify
		
		subObjectLevel = 5
		
		faceArray = #()
		getSelectedFace (for face in theObject.faces collect face.index) &faceArray
		
		polyOp.setFaceSelection theObject faceArray
	)
	
	on RSL_RemoveInteriorFaces open do
	(
		RSL_INIOps.readLocalConfigINIStdRollout RSL_RemoveInteriorFaces localConfigINI
	)
	
	on RSL_RemoveInteriorFaces close do
	(
		RSL_INIOps.writeLocalConfigINIStdRollout RSL_RemoveInteriorFaces localConfigINI
	)
	
	on btn_faceRemove pressed do
	(
		pbar_progress.value = 0
		
		undo "Face Remove" on
		(
			theObject = selection[1]
			
			if theObject != undefined then
			(
				if (classOf theObject) == editable_poly then
				(
					setFaceDataForSelected theObject
					
					local faceArray = #()
					
					if chkb_selected.checked then
					(
						faceArray = for face in theObject.selectedFaces collect face.index
					)
					else
					(
						faceArray = for face in theObject.faces collect face.index
					)
					
					faceRemove faceArray theObject
					
					simpleFaceManager.removeChannel theObject RSL_meshCleanIsSelected
				)
				else
				(
					messageBox "The object is not an editable poly." title:"Error..."
				)
			)
		)
	)
)

positionINISetting = (getINISetting (RsConfigGetWildWestDir() + "script\\max\\Rockstar_London\\helpers\\RSL_RemoveInteriorFaces.ini") "Main" "Position")
dialogPos = undefined

if positionINISetting != "" then
(
	local dialogPosition = readValue (positionINISetting as stringStream)
	
	dialogPos = dialogPosition
)

if dialogPos != undefined then
(
	createDialog RSL_RemoveInteriorFaces pos:dialogPos style:#(#style_toolwindow,#style_sysmenu)
)
else
(
	createDialog RSL_RemoveInteriorFaces style:#(#style_toolwindow,#style_sysmenu)
)

positionINISetting = undefined
dialogPos = undefined
