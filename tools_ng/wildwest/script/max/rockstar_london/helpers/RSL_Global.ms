fn SelectNodes ObjectArray =
(
	for obj in ObjectArray do
        (
			selectMore obj
		)
)

fn AddObjectToLayer Obj LayerName =
(
		ThisLayer = layermanager.getLayerFromName LayerName
		if (ThisLayer == undefined) then 
	(
		ThisLayer = LayerManager.newLayerFromName LayerName
	)

		
		ThisLayer.addnode Obj
)

fn IsShapeType Obj =
(
	if (superclassof Obj == shape) then
		( return true )
		else
		( return false )
)

fn IsParticleType Obj =
(
if (classof Obj == RAGE_Particle) then
		( return true )
		else
		( return false )
)

fn IsLightType Obj =
(
	if (superclassof Obj == light) then
		( return true )
		else
		( return false )
)

fn IsCollisionType Obj =
(
	case classOf Obj of
	(
		(Col_Mesh): return true
		(Col_Sphere): return true
		(Col_Box): return true
		(Col_Plane): return true
		(Col_Cylinder): return true
		(Shad_Mesh): return true
	)
	return false
)

fn IsXRefType Obj =
(
	if (classof Obj == XRefObject) then
		( return true )
		else
		( return false )
)

fn IsGeometryType Obj =
(
	if (classof Obj == Editable_Poly OR classof Obj == Editable_Mesh OR classof Obj == PolyMeshObject) then
		( 
		return true
		)
		
	return false
)

fn IsShell Obj = 
(

)

fn IsMiloRoom Obj =
(
	if (classof Obj == GtaMloRoom) then
		( return true )
		else
		( return false )
)

fn IsMilo Obj =
(
	if (classof Obj == Gta_MILO) then
		( return true )
		else
		( return false )
)

fn FilterExportableGeometry ObjectArray =	
(
	ReturnObjectArray = #()
	
	for Obj in ObjectArray do
	(
		try(IsNonExportable = GetAttr Obj (GetAttrIndex "Gta Object" "Dont Export"))catch(IsNonExportable=true)
		if(IsNonExportable == false) then
		(
			Append ReturnObjectArray Obj
		)
	)
	
	return ReturnObjectArray
)

fn CentrePivot Obj =
(
	Obj.pivot = ((Obj.max + Obj.min)/2)
)