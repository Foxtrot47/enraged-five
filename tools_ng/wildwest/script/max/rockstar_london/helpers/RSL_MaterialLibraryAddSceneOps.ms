
struct RSL_MaterialLibraryAddSceneOps
(
	
	materialArray = #(),
	listViewCache = #(),
	thumbnailArray = #(),
	first = 0,
	updateList = false,
	currentLibrary,
	
	sceneMaterialsForm,
	displayMenu,displayList,displayThumbs,
	materialList,
	addButton,cancelButton,
	
	fn indexOfThumbnail file =
	(
		local result = -1
		
		for entry in thumbnailArray do 
		(
			if entry.file == file then
			(
				result = entry.index
				exit
			)
		)
		
		result
	),
	
	fn newListViewItem material =
	(
		local listViewItem = dotNetObject "System.Windows.Forms.ListViewItem" material.name
		listViewItem.tag = dotNetMXSValue material
		
		local index = 0
		
		if displayList.checked then
		(
-- 			index = getMaterialStatus material
		)
		else
		(
			local diffuseTexture = (getSubTexmap material 1)
			local thumbnail
			
			if diffuseTexture != undefined then
			(
				thumbnail = diffuseTexture.filename
			)
			else
			(
				thumbnail = ""
			)
			
			index = indexOfThumbnail thumbnail
			
			if index < 0 then
			(
				materialList.LargeImageList.images.Add (RSL_MaterialLibrary.imageFromThumbnail thumbnail materialList)
				
				append thumbnailArray (dataPair file:thumbnail index:materialList.LargeImageList.images.count)
				
				index = materialList.LargeImageList.images.count - 1
			)
		)
		
		listViewItem.ImageIndex = index
		
		listViewItem
	),
	
	fn findMaterial array name =
	(
		local result = 0
		
		for i = 1 to array.count do 
		(
			if (stricmp array[i].name name) == 0 then
			(
				result = i
			)
		)
		
		result
	),
	
	mapped fn appendMaterial material =
	(
		case (classOf material) of
		(
			Rage_Shader:
			(
				local index = findMaterial currentLibrary material.name
-- 				local libMat = currentLibrary[material.name]
				
				if index == 0 then
				(
					appendIfUnique materialArray material
				)
				else
				(
					format "% found in library.\n" material.name
				)
			)
			Multimaterial:
			(
				for i = 1 to material.materialList.count do
				(
					local subMaterial = material.materialList[i]
					
					appendMaterial subMaterial
				)
			)
		)
	),
	
	fn initialiseListView =
	(
		appendMaterial (for object in selection where object.material != undefined collect object.material)
		
		materialList.virtualListSize = materialArray.count
	),
	
	
	
	
	
	
	
	fn dispatchDisplayClick s e =
	(
		RSL_MaterialLibrary.addSceneOps.displayClick s e
	),
	
	fn displayClick s e =
	(
		s.checked = true
		
		case s.name of
		(
			"displayList":
			(
				displayThumbs.checked = false
				materialList.view = (dotNetClass "System.Windows.Forms.View").List
				
				updateList = true
				materialList.invalidate()
			)
			"displayThumbs":
			(
				displayList.checked = false
				materialList.view = (dotNetClass "System.Windows.Forms.View").LargeIcon
				
				updateList = true
				materialList.invalidate()
			)
		)
	),
	
	fn dispatchCreateVirtualItem s e =
	(
		RSL_MaterialLibrary.addSceneOps.createVirtualItem s e
	),
	
	fn createVirtualItem s e =
	(
		if listViewCache.count != 0 and e.ItemIndex >= first and e.itemIndex < (first + listViewCache.count) and not updateList then
		(
			local index = e.ItemIndex - first + 1
			
			e.Item = listViewCache[index]
		)
		else
		(
			local material = materialArray[(e.itemIndex + 1)]
			
			e.item = newListViewItem material
		)
	),
	
	fn dispatchCacheVirtualItems s e =
	(
		RSL_MaterialLibrary.addSceneOps.cacheVirtualItems s e
	),
	
	fn cacheVirtualItems s e =
	(
-- 		local data = s.tag.value
		if listViewCache.count != 0 and e.startIndex >= first and e.endIndex <= (first + listViewCache.count) and not updateList then
		(
			-- do nothing, the cache is still valid!
		)
		else
		(
			thumbnailArray = #()
			s.LargeImageList.images.clear()
			gc()
			
			first = e.startIndex
			listViewCache = #()
			listViewCache.count = e.EndIndex - e.StartIndex + 1
			
			for i = 1 to listViewCache.count do 
			(
				local index = (i + first)
				local material = materialArray[index]
				
				listViewCache[i] = newListViewItem material
			)
			
			updateList = false
		)
	),
	
	fn dispatchAddClick s e =
	(
		RSL_MaterialLibrary.addSceneOps.addClick s e
	),
	
	fn addClick s e =
	(
		if materialList.selectedIndices.count > 0 then
		(
			local okToContinue = queryBox ("Are you sure you want to add the " + materialList.selectedIndices.count as string + " selected material\s to the current library?") title:"Warning..."
			
			if okToContinue then
			(
				local selectedMaterials = #()
				
				for i = 0 to materialList.selectedIndices.count - 1 do 
				(
					local index = materialList.selectedIndices.item[i]
					
					append selectedMaterials materialArray[index + 1]
				)
				
				local currentList = RSL_MaterialLibrary.mainTabControl.SelectedTab.tag.value
				
				RSL_MaterialLibrary.addMaterials selectedMaterials currentList
				
				currentList.virtualListSize += selectedMaterials.count
				updateList = true
				currentList.invalidate()
				
				RSL_MaterialLibrary.updateStats()
				
				sceneMaterialsForm.close()
			)
		)
		else
		(
			messageBox "No materials selected. Please select one or more materials from the list first." title:"Error..."
		)
	),
	
	fn dispatchCancelClick s e =
	(
		RSL_MaterialLibrary.addSceneOps.cancelClick s e
	),
	
	fn cancelClick s e =
	(
		sceneMaterialsForm.close()
	),
	
	fn dispatchOnFormClosing s e =
	(
		RSL_MaterialLibrary.addSceneOps.onFormClosing s e
	),
	
	fn onFormClosing s e =
	(
		materialList.dispose()
		
		materialArray = #()
		listViewCache = #()
		thumbnailArray = #()
		first = 0
		updateList = false
		currentLibrary = false
		
		sceneMaterialsForm = undefined
		materialList = undefined
		
		RSL_MaterialLibrary.libraryOpsForm.enabled = true
	),
	
	fn createForm =
	(
		sceneMaterialsForm = RS_dotNetUI.maxForm "libraryOpsForm" text:"Scene Materials" size:[256,460] min:[256,460] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		sceneMaterialsForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").CenterScreen
		dotnet.addEventHandler sceneMaterialsForm "FormClosing" dispatchOnFormClosing
		
		displayList = RS_dotNetUI.ToolStripMenuItem "displayList" text:"List"
		displayList.checked = true
		dotnet.addEventHandler displayList "Click" dispatchDisplayClick
		
		displayThumbs = RS_dotNetUI.ToolStripMenuItem "displayThumbs" text:"Thumbnails"
		dotnet.addEventHandler displayThumbs "Click" dispatchDisplayClick
		
		displayMenu = RS_dotNetUI.ToolStripMenuItem "displayMenu" text:"Display..."
		displayMenu.DropDownItems.AddRange #(displayList, displayThumbs)
		
		materialListRickClick = RS_dotNetUI.ContextMenuStrip "listViewRightClick" items:#(displayMenu)
		
		materialList = RS_dotNetUI.listView "Selected Materials" dockStyle:RS_dotNetPreset.DS_Fill
		materialList.view = (dotNetClass "System.Windows.Forms.View").List --List --Details
		materialList.Sorting = (dotNetClass "System.Windows.Forms.SortOrder").Ascending
		materialList.Multiselect = true --false
		materialList.HideSelection = false
		materialList.scrollable = true
		materialList.virtualMode = true
-- 		materialList.LabelEdit = true
		materialList.ContextMenuStrip = materialListRickClick
-- 		materialList.smallImageList = RS_dotNetObject.imageListObject materialLibraryIcons imageSize:[24,24]
-- 		materialList.virtualListSize = libraryArray[i].library.count
		
		local cDepth = dotNetClass "ColorDepth"
		local imageList = dotNetObject "System.Windows.Forms.ImageList"
		local largeImageSize = dotNetObject "System.Drawing.Size" 256 256
		
		imageList.imagesize = largeImageSize
		imageList.ColorDepth = cDepth.Depth32Bit
		
		materialList.LargeImageList = imageList
		
		dotNet.addEventHandler materialList "RetrieveVirtualItem" dispatchCreateVirtualItem
		dotNet.addEventHandler materialList "CacheVirtualItems" dispatchCacheVirtualItems
-- 		dotnet.addEventHandler materialList "click" dispatchMaterialListClick --SelectedIndexChanged
-- 		dotnet.addEventHandler materialList "DoubleClick" dispatchMaterialListDoubleClick
-- 		dotnet.addEventHandler materialList "BeforeLabelEdit" dispatchMaterialListPreLabelEdit
-- 		dotnet.addEventHandler materialList "AfterLabelEdit" dispatchMaterialListPostLabelEdit
		
		initialiseListView()
		
		addButton = RS_dotNetUI.Button "addButton" text:(toUpper "Add Selected") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler addButton "click" dispatchAddClick
		
		cancelButton = RS_dotNetUI.Button "cancelButton" text:(toUpper "Cancel") dockStyle:RS_dotNetPreset.DS_Fill margin:(RS_dotNetObject.paddingObject 2)
		dotnet.addEventHandler cancelButton "click" dispatchCancelClick
		
		mainTable = RS_dotNetUI.tableLayout "viewTable" text:"mainTable" collumns:#((dataPair type:"Percent" value:50), (dataPair type:"Percent" value:50)) rows:#((dataPair type:"Percent" value:50), (dataPair type:"Absolute" value:24)) \
															dockStyle:RS_dotNetPreset.DS_Fill --cellStyle:RS_dotNetPreset.CBS_Single
		mainTable.BackColor = RS_dotNetPreset.controlColour_Medium
		
		mainTable.Controls.Add materialList 0 0
		mainTable.Controls.Add addButton 0 1
		mainTable.Controls.Add cancelButton 1 1
		
		mainTable.SetColumnSpan materialList 2
		
		sceneMaterialsForm.controls.add mainTable
		
		if materialArray.count > 0 then
		(
			sceneMaterialsForm.showModeless()
			
			sceneMaterialsForm
		)
		else
		(
			messageBox "None of the materials assigned to the selected objects can be added to the library.\nEither there are already materials with the same names or they are not Rage Shaders." title:"Error..."
			onFormClosing 0 0
		)
	)
)