fn setGizmoTransform ir =
(
	local object = selection[1]
	local rightVector = normalize (cross [0,0,1] ir.dir)
	local upVector = normalize (cross rightVector ir.dir)
	
	local theMatrix = matrix3 rightVector upVector ir.dir (ir.pos - object.pos)
	
 	object.modifiers[1].gizmo.transform = theMatrix
)

fn trackMouse msg ir obj faceNum shift ctrl alt =
(
	case msg of
	(
		#freeMove:
		(
			#continue
		)
		#mousePoint:
		(
			if ir != undefined then
			(
				setGizmoTransform ir
			)
			#continue
		)
		#mouseMove:
		(
			if ir != undefined then
			(
				setGizmoTransform ir
			)
			#continue
		)
		#mouseAbort:
		(
			#exit
		)
	)
)

fn alignGizmoToFace =
(
	local theObject = selection[1]
	
	if theObject != undefined then
	(
		if theObject.modifiers.count > 0 and (isProperty theObject.modifiers[1] "gizmo") then
		(
			local theMesh = (convertToMesh (copy theObject))
			hide theMesh
			select theObject
			
			mouseTrack on:theMesh trackCallback:trackMouse
			try (delete theMesh) catch()
		)
	)
)

alignGizmoToFace()