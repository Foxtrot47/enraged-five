filein (RsConfigGetWildWestDir() + "script/max/Rockstar_London/pipeline/RSL_LevelConfig.ms")
filein (RsConfigGetWildWestDir() + "script/max/Rockstar_London/pipeline/rsl_p4.ms")
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetUIOps.ms")

global RSL_TXDOptimiseOps

struct RSL_TXD
(
	name = "",
	objectArray = #(),
	textureArray = #(),
	memory = 0.0,
	uniqueTextureArray = #(),
	TXDLinks = #(),
	centerPoint = [0,0,0],
	TXDCloud,
	
	fn isUnique texture array =
	(
		local result = true
		
		for entry in array do
		(
			if entry.name == texture.name then
			(
				result = false
			)
		)
		
		result
	),
	
	fn isSharedTexture texture =
	(
		local result = false
		local count = 0
		
		for entry in objectArray do
		(
			if not (isUnique texture entry.textures) then
			(
				count += 1
			)
		)
		
		if count > 1 then
		(
			result = true
		)
		
		result
	),
	
	mapped fn sortTextures texture index =
	(
		if (isSharedTexture texture) then
		(
			if (isUnique texture textureArray) then
			(
				append textureArray texture
				memory += texture.memory
			)
		)
		else
		(
			append uniqueTextureArray[index] texture
		)
	),
	
	fn initialise =
	(
		uniqueTextureArray.count = objectArray.count
		
		for i = 1 to objectArray.count do
		(
			uniqueTextureArray[i] = #()
			
			sortTextures objectArray[i].textures i
		)
	),
	
	fn formatUniqueTextureArray =
	(
		local result = #()
		
		for i = 1 to uniqueTextureArray.count do
		(
			local memory = 0.0
			
			for entry in uniqueTextureArray[i] do
			(
				memory += entry.memory
			)
			
			append result (objectArray[i].name + " : " + uniqueTextureArray[i].count as string + " : " + memory as string)
		)
		
		result
	),
	
	fn createCloud =
	(
		TXDCloud = TXDCloudManip \
				TXD:name \
				objectArray:(for textureObject in objectArray collect textureObject.object) \
				textureArray:(for texture in textureArray collect texture.name) \
				memory:memory \
				uniqueTextureArray:(formatUniqueTextureArray()) \
				TXDLinks:TXDLinks 
	),
	
	fn toString =
	(
		local result = stringStream ""
		
		format "TXD:%\nObjects:\n" name
		
		for object in objectArray do
		(
			format "\t%\n" object.name
		)
		
		format "Shared Textures:\n"
		
		for texture in textureArray do
		(
			format "\t%\n" texture.name
		)
		
		format "Memory:%K\n" memory
		
		format "Unique Textures:\n"
		
		for i = 1 to objectArray.count do
		(
			if uniqueTextureArray[i].count > 0 then
			(
				format "\t%\n" objectArray[i].name
				
				for entry in uniqueTextureArray[i] do
				(
					format "\t\t%\n" entry.name
				)
			)
		)
		
		format "TXD Links:\n"
		
		for entry in TXDLinks do
		(
			format "\t%\n" entry
		)
		
		format "Center Point:%\n\n" centerPoint
	),
	
	fn toTreeView treeView this =
	(
		local TXDRootNode = treeView.Nodes.Add name
		TXDRootNode.tag = dotNetMXSValue this
		
		local objectsRootNode = TXDRootNode.Nodes.Add ("Objects: " + objectArray.count as string)
		objectsRootNode.tag = dotNetMXSValue objectArray
		
		for i = 1 to objectArray.count do
		(
			local count = ""
			
			if uniqueTextureArray[i].count > 0 then
			(
				count = uniqueTextureArray[i].count as string
			)
			
			local objectNode = objectsRootNode.Nodes.Add objectArray[i].name
			objectNode.tag = dotNetMXSValue objectArray[i]
			
			if uniqueTextureArray[i].count > 0 then
			(
				objectNode.text = (objectArray[i].name + " : Unique Textures: " + count)
				
				for texture in uniqueTextureArray[i] do
				(
					local uniqueTextureNode = objectNode.Nodes.Add texture.name
					uniqueTextureNode.tag = dotNetMXSValue texture
				)
			)
		)
		
		objectsRootNode.expand()
		
		local texturesRootNode = TXDRootNode.Nodes.Add ("Textures: " + textureArray.count as string)
		
		for texture in textureArray do
		(
			local textureNode = texturesRootNode.Nodes.Add texture.name
			textureNode.tag = dotNetMXSValue texture
		)
		
		texturesRootNode.expandAll()
		
		local memoryRootNode = TXDRootNode.Nodes.Add ("Texture Memory: " + memory as string + "K")
		memoryRootNode.tag = dotNetMXSValue memory
		
		local TXDLinksRootNode = TXDRootNode.Nodes.Add ("TXD Links: " + TXDLinks.count as string)
		
		for entry in TXDLinks do
		(
			local filtered = filterString entry ","
			local TXDLinkNode = TXDLinksRootNode.Nodes.Add (filtered[1] + " : " + filtered[2] + " : " + filtered[3])
			TXDLinkNode.tag = dotNetMXSValue entry
		)
		
		TXDLinksRootNode.expand()
		
-- 		TXDRootNode.expand()
	)
)

struct RSL_SharedTXD
(
	name = "",
	TXDArray = #(),
	TextureArray = #(),
	memory
)

struct RSL_TextureObject
(
	TXD,
	name,
	object,
	textures,
	memory
)

struct RSL_TXDTexture
(
	name = "",
	size = [0,0],
	type,
	textures = #(),
	memory = 0.0
)

struct RSL_TXDOptimiser
(
	idxTXD = getattrindex "Gta Object" "TXD",
	idxDontExport = getAttrIndex "Gta Object" "Dont Export",
	idxDontAdd = getAttrIndex "Gta Object" "Dont Add To IPL",
	
	memory1024 = 699192,
	memory1024Alpha = 1398256,
	memoryLimit = 6000.0,
	
	workPath = systemTools.getEnvVariable "RSG_ROOTDIR",
	rootPath = project.independent + ("\\intermediate\\levels\\"),
	XML,
	
	bitmapArray = #(),
	textureObjectArray = #(),
	TXDArray = #(),
	cloudArray = #(),
	errorStringArray = #(),
	
	TXDOptForm,
	
	errorForm,errorListView,
	
	fn dispatchErrorListViewResize s e =
	(
		RSL_TXDOptimiseOps.errorListViewResize s e
	),
	
	fn errorListViewResize s e =
	(
		if errorListView.columns.count == 1 then
		(
			errorListView.columns.Item[0].width = errorForm.width - 22
		)
	),
	
	fn dispatchErrorListViewDoubleClick s e =
	(
		RSL_TXDOptimiseOps.errorListViewDoubleClick s e
	),
	
	fn errorListViewDoubleClick s e =
	(
		
		if s.selectedItems.count == 1 then
		(
			
			local data = s.selectedItems.Item[0].tag.value
			
			case (classOf data) of
			(
				default:
				(
					print (classOf data)
				)
				Standardmaterial:
				(
					meditMaterials[1] = data
				)
				Multimaterial:
				(
					meditMaterials[1] = data
				)
				Bitmaptexture:
				(
					meditMaterials[1] = data
				)
			)
		)
	),
	
	fn errorReport array =
	(
		errorForm = RS_dotNetUI.maxForm "errorForm" text:"TXD Optimiser Error Report" Size:[600,256] min:[94,90] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		
		errorListView = RS_dotNetUI.listView "errorListView" dockStyle:RS_dotNetPreset.DS_Fill
		errorListView.view = (dotNetClass "System.Windows.Forms.View").Details
		errorListView.HeaderStyle = (dotNetClass "System.Windows.Forms.ColumnHeaderStyle").Nonclickable
		errorListView.fullRowSelect = true
		errorListView.HideSelection = false
		dotNet.addEventHandler errorListView "Resize" dispatchErrorListViewResize
		dotnet.addEventHandler errorListView "MouseDoubleClick" dispatchErrorListViewDoubleClick
		
		local listViewEntries = #()
		
		for entry in array do
		(
			local listViewEntry = dotNetObject "System.Windows.Forms.ListViewItem" entry.error
			listViewEntry.tag = dotNetMXSValue entry.data
			
			append listViewEntries listViewEntry
		)
		
		errorListView.Items.AddRange listViewEntries
		
		errorForm.controls.Add errorListView
		
		errorListView.columns.add "Errors" (errorForm.width - 22)
		
		errorForm.showModeless()
	),
	
	fn isExported object =
	(
		not (getAttr object idxDontExport) and not (getAttr object idxDontAdd)
	),
	
	fn isUsedInObject object ID =
	(
		local result = false
		
		for face in object.faces do
		(
			case (classOf Object) of
			(
				editable_mesh:
				(
					if (getFaceMatID object face.index) == ID then
					(
						result = true
					)
				)
				editable_poly:
				(
					if (polyOp.getFaceMatID object face.index) == ID then
					(
						result = true
					)
				)
			)
		)
		
		result
	),
	
	fn whichType rageShader texture =
	(
		local result = "Alpha Texture"
		local count = 0
-- 		local filename = getFilenameFile (toLower texture.filename)
		
		for i = 1 to (RstGetVariableCount rageShader) do
		(
			local variableType = (RstGetVariableType rageShader i)
			
			if variableType != undefined then
			(
				if (matchPattern (RstGetVariableType rageShader i) pattern:"Texmap") then
				(
					count += 1
					local map = getSubTexmap rageShader count
					
					if (classOf map) == CompositeTexturemap then
					(
						for entry in map.mapList do
						(
							if entry == texture then
							(
								result = (RstGetVariableName rageShader i)
							)
						)
					)
					else
					(
						if map == texture then
						(
							result = (RstGetVariableName rageShader i)
						)
					)
				)
			)
		)
		
		result
	),
	
	fn isUniqueTexture texture array =
	(
		local result = true
		
		for entry in array do
		(
			if entry.name == texture.name then
			(
				result = false
			)
		)
		
		result
	),
	
	fn getScaleFromXML texture =
	(
		local result = 1.0
		
		if XML != undefined then
		(
			for i = 0 to XML.document.DocumentElement.ChildNodes.Count - 1 do
			(
				local node = XML.document.DocumentElement.ChildNodes.Item[i]
				
				if (toLower texture) == (toLower node.name) then
				(
					result = node.Attributes.Item[0].value as float
				)
			)
		)
		
		result
	),
	
	fn calculateMemory size alpha =
	(
		if alpha then
		(
			ceil ((size.x * size.y) / (1024.0 * 1024.0) * memory1024Alpha) / 1024
		)
		else
		(
			ceil ((size.x * size.y) / (1024.0 * 1024.0) * memory1024) / 1024
		)
	),
	
	fn recurseMaterialForTextures material object &textureArray =
	(
		case (classOf material) of
		(
			default:
			(
				append errorStringArray (dataPair error:("-- Data error: Material is not a Rage Shader: " + material.name + " Class: " + (classOf material) as string) data:material)
			)
			Rage_Shader:
			(
				local textureCount = getNumSubTexmaps material
				local isAlpha = false
				
				if (RstGetIsAlphaShader material) then
				(
					textureCount /= 2
					isAlpha = true
				)
				
				for i = 1 to textureCount do
				(
					local subTextureMap = getSubTexmap material i
					local texture = RSL_TXDTexture()
					
					case (classof subTextureMap) of
					(
						Bitmaptexture:
						(
							try
							(
								if subTextureMap.filename != "" then
								(
									texture.name = toLower (getFilenameFile subTextureMap.filename)
									texture.size = [subTextureMap.bitmap.width, subTextureMap.bitmap.height]
-- 									texture.type = whichType material subTextureMap
									
									append texture.textures subTextureMap.filename
								)
								
								if isAlpha then
								(
									local alphaTextureMap = getSubTexmap material (i + textureCount)
									
									if (classof alphaTextureMap) == Bitmaptexture and alphaTextureMap.filename != "" then
									(
										texture.name += "+" + (toLower (getFilenameFile alphaTextureMap.filename))
-- 										texture.type += "+alpha"
										
										append texture.textures alphaTextureMap.filename
									)
								)
							)
							catch
							(
								texture = RSL_TXDTexture()
								append errorStringArray (dataPair error:(getCurrentException()) data:subTextureMap)
							)
						)
						CompositeTexturemap:
						(
							local compositeCount = getNumSubTexmaps subTextureMap
-- 							texture.type = whichType material subTextureMap
							
							for c = 1 to compositeCount do
							(
								local compTextureMap = getSubTexmap subTextureMap c
								
								try
								(
									if (classof compTextureMap) == Bitmaptexture and compTextureMap.filename != "" then
									(
										if c > 1 then
										(
											texture.name += ">"
										)
										
										texture.name += toLower (getFilenameFile compTextureMap.filename)
										
										append texture.textures compTextureMap.filename
										
										local compSize = [compTextureMap.bitmap.width, compTextureMap.bitmap.height]
										if (length texture.size) < (length compSize) then
										(
											texture.size = compSize
										)
									)
								)
								catch
								(
									texture = RSL_TXDTexture()
									append errorStringArray (dataPair error:(getCurrentException()) data:compTextureMap)
								)
							)
						)
					)
					
					if texture.name != "" and (isUniqueTexture texture textureArray) then
					(
						texture.memory = calculateMemory ((texture.size * 0.5) * (getScaleFromXML (substituteString texture.name "+>" ""))) false
						append textureArray texture
					)
				)
			)
			multimaterial:
			(
				for i = 1 to material.materialList.count do
				(
					local subMaterial = material.materialList[i]
					
					if subMaterial != undefined then
					(
						if (isUsedInObject object material.materialIDList[i]) then
						(
							recurseMaterialForTextures subMaterial object &textureArray
						)
					)
-- 					else
-- 					(
-- 						append errorStringArray (dataPair error:("-- Data error: Sub-material is undefined: " + material.name + " ID: " + (material.materialIDList[i]) as string) data:material)
-- 					)
				)
			)
		)
	),
	
	fn getRSLTexturesFromObject object =
	(
		local result = #()
		
		local material = object.material
		
		if material != undefined then
		(
			recurseMaterialForTextures material object &result
		)
		
		result
	),
	
	fn createTextureObjects =
	(
		textureObjectArray = #()
		
		p4.sync(pathConfig.appendPath workPath (pathConfig.removePathTopParent project.independent)+"/intermediate/levels/...")
		local XMLFile = rootPath + levelConfig.level + "\\" + (getFilenameFile maxFilename) + "_TextureTag.xml"
		
		if (doesFileExist XMLFile) then
		(
			XML = XmlDocument()
			XML.init()
			XML.load XMLFile
		)
		
		progressStart "Creating Texture Objects..."
		
		local objectArray = for object in objects where (getAttrClass object) == "Gta Object" and (classOf object) != XrefObject and (isExported object) collect object
		
		for i = 1 to objectArray.count do
		(
			local object = objectArray[i]
			
			local textureArray = getRSLTexturesFromObject object
			
			if textureArray.count > 0 then
			(
				append textureObjectArray (RSL_TextureObject TXD:(getattr object idxTXD) name:object.name object:object textures:textureArray)
			)
			
			progressUpdate ((i as float) / objectArray.count * 100.0)
		)
		
		progressEnd()
	),
	
	fn findTXD TXD = 
	(
		local result = 0
		
		for i = 1 to TXDArray.count do
		(
			if TXDArray[i].name == TXD then
			(
				result = i
			)
		)
		
		result
	),
	
	fn createTXDs =
	(
		if textureObjectArray.count > 0 then
		(
			TXDArray = #()
			
			for i = 1 to textureObjectArray.count do
			(
				local textureObject = textureObjectArray[i]
				local TXDidx = findTXD textureObject.TXD
				
				if TXDidx > 0 then
				(
					local TXD = TXDArray[TXDidx]
					append TXD.objectArray textureObject
				)
				else
				(
					local newTXD = RSL_TXD name:textureObject.TXD objectArray:#(textureObject)
					
					append TXDArray newTXD
				)
			)
			
			for entry in TXDArray do
			(
				entry.initialise()
			)
		)
		else
		(
			messageBox "No texture objects in textureObjectArray. Run RSL_TXDOptimiseOps.createTextureObjects() first." title:"Error..."
		)
	),
	
	fn createClouds =
	(
		if TXDArray.count > 0 then
		(
			for object in helpers where (classOf object) == TXDCloudManip do delete object
			
			progressStart "Gathering TXD Links..."
			
			for i = 1 to TXDArray.count do
			(
				TXDArray[i].createCloud()
				
				progressUpdate ((i as float) / cloudArray.count * 100.0)
			)
			
			progressEnd()
		)
		else
		(
			messageBox "No TXDs found in TXDArray. Run RSL_TXDOptimiseOps.createTXDs() first." title:"Error..."
		)
	),
	
	fn getTXDLinks TXD =
	(
		local result = #()
		
		for entry in TXDArray where entry.name != TXD.name do
		(
			local linkEntry = ""
			local count = 0
			local memory = 0.0
			
			for texture in entry.textureArray do
			(
				for item in TXD.textureArray do
				(
					if item.name == texture.name then
					(
						linkEntry = entry.name
						count += 1
						
						memory += texture.memory
					)
				)
			)
			
			if count > 0 then
			(
				linkEntry += "," + (count as string) + "," + (memory as string) + "K"
				appendIfUnique result linkEntry
			)
		)
		
		result
	),
	
	fn gatherTXDLinks =
	(
		if TXDArray.count > 0 then
		(
			progressStart "Gathering TXD Links..."
			
			for i = 1 to TXDArray.count do
			(
				local entry = TXDArray[i]
				local TXDLinkArray = getTXDLinks entry
				
				if TXDLinkArray.count > 0 then
				(
					entry.TXDLinks = TXDLinkArray
				)
				
				progressUpdate ((i as float) / cloudArray.count * 100.0)
			)
			
			progressEnd()
		)
		else
		(
			messageBox "No TXDs found in TXDArray. Run RSL_TXDOptimiseOps.createTXDs() first." title:"Error..."
		)
	),
	
	fn findTextureObject textureObject array =
	(
		local result = 0
		
		for i = 1 to array.count do
		(
			local subArray = array[i]
			
			for object in subArray do
			(
				if object.name == textureObject.name then
				(
					result = i
				)
			)
		)
		
		result
	),
	
	fn getSameTextureCount this other =
	(
		local result = 0
		
		for thisTexture in this.textures do
		(
			for otherTexture in other.textures do
			(
				if thisTexture.name == otherTexture.name then
				(
					result += 1
				)
			)
		)
		
		result
	),
	
	fn sortTextureObjects =
	(
		
	),
	
	fn getTextureMemoryForTXD TXD =
	(
		local result = 0.0
		
		for textureObject in TXD do
		(
			for texture in textureObject.textures do
			(
				result += texture.memory
			)
		)
		
		result
	),
	
	fn mergeByMemory array =
	(
		
	),
	
	fn initialise =
	(
		clearListener()
		
		errorStringArray = #()
		createTextureObjects()
		createTXDs()
		gatherTXDLinks()
		
		local okToContinue = queryBox "Do you want to create TXD Clouds?" title:"TXD Clouds..."
		
		if errorStringArray.count > 0 then
		(
			okToContinue = queryBox "There are errors which means the TXD clouds could be VERY inaccurate. Do you still want to continue creating the TXD clouds?\n(the error report will be displayed regardless)" title:"Warning..."
			errorReport errorStringArray
		)
		
		if okToContinue then
		(
			createClouds()
			
			manipulateMode = false
			manipulateMode = true
		)
	),
	
	fn createForm =
	(
		clearListener()
		
		errorStringArray = #()
		createTextureObjects()
		
		local okToContinue = true
		
		if errorStringArray.count > 0 then
		(
			okToContinue = queryBox "There are errors in the scene which means the TXD data is likely to be VERY inaccurate. Do you want to continue?\n(The error report will be displayed regardless)" title:"Warning..."
			errorReport errorStringArray
		)
		
		if okToContinue then
		(
			TXDOptForm = RS_dotNetUI.maxForm "TXDOptForm" text:"R* TXD Optimiser V1.0" size:[256,512] min:[128,256] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
			TXDOptForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
			
-- 			dotNet.addEventHandler TXDOptForm "Resize" dispatchFormResize
-- 			dotnet.addEventHandler TXDOptForm "FormClosing" dispatchOnFormClosing
			
			
			
			TXDTree = RS_dotNetUI.treeView "TXDTree" dockStyle:RS_dotNetPreset.DS_Fill
			TXDTree.Sorted = false
-- 			dotnet.addEventHandler TXDTree "AfterCheck" dispatchTreeAfterCheck
			
			
			
			
			
			
			mainTable = RS_dotNetUI.tableLayout "mainTable" text:"mainTable" collumns:#((dataPair type:"Percent" value:50), (dataPair type:"Percent" value:50)) \
																dockStyle:RS_dotNetPreset.DS_Fill cellStyle:RS_dotNetPreset.CBS_Single
			mainTable.BackColor = RS_dotNetPreset.controlColour_Medium
			
			mainTable.RowCount = 8
			mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 20)
			mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "percent" 50)
			mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 20)
			mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 20)
			mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 20)
			mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 20)
			mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 20)
			mainTable.RowStyles.add (RS_dotNetObject.RowStyleObject "absolute" 20)
			
			mainTable.Controls.Add TXDTree 0 1
			
			mainTable.SetColumnSpan TXDTree 2
			
			TXDOptForm.controls.add mainTable
			
			createTXDs()
			gatherTXDLinks()
			
			TXDOptForm.showModeless()
			
			for entry in TXDArray do
			(
				entry.toTreeView TXDTree entry
			)
			
			TXDOptForm
		)
	)
)

if RSL_TXDOptimiseOps != undefined then
(
	try
	(
		RSL_TXDOptimiseOps.TXDOptForm.close()
	)
	catch()
	try
	(
		RSL_TXDOptimiseOps.errorForm.Close()
	)
	catch()
)

RSL_TXDOptimiseOps = RSL_TXDOptimiser()
RSL_TXDOptimiseOps.createForm()

