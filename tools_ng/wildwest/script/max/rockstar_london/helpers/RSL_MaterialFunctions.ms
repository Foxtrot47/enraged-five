--Alters a MaterialID on a Editable_Poly or Editable_Mesh
fn ChangeMaterialID Obj OldID NewID =
(
		if classof Obj == Editable_Mesh then 
			(
				FaceCount = getnumfaces Obj
				
				for FaceIterator = 1 to FaceCount do 
					(
					CurrentFaceID = getfacematid Obj FaceIterator
					if (CurrentFaceID == OldID) then
						(
							setFaceMatID Obj FaceIterator NewID
						)
					)
					return()
			) 
		
			
		if classof Obj == Editable_Poly then 
			(
				FaceCount = getnumfaces Obj
				
				for FaceIterator = 1 to FaceCount do 
					(
					CurrentFaceID = polyop.getfacematid Obj FaceIterator
						
					if (CurrentFaceID == OldID) then
						(
							polyop.setFaceMatID Obj FaceIterator NewID
						)
					)
					return()
			)
		
		format "Object:% must be collapsed in order to safely change MaterialID(s) \n" Obj.name
)

--Checks for a MaterialID on a Editable_Poly or Editable_Mesh and returns true if it exsists
fn CheckForMaterialID Obj IDToCheck = 
(
		if classof Obj == Editable_Mesh then 
			(
				FaceCount = getnumfaces Obj
				
				for FaceIterator = 1 to FaceCount do 
					(
					CurrentFaceID = getfacematid Obj FaceIterator
					if (CurrentFaceID == IDToCheck) then
						(
							return true
						)
					)
			) 
		
			
		if classof Obj == Editable_Poly then 
			(
				FaceCount = getnumfaces Obj
				
				for FaceIterator = 1 to FaceCount do 
					(
					CurrentFaceID = polyop.getfacematid Obj FaceIterator
					if (CurrentFaceID == IDToCheck) then
						(
							return true
						)
					)
			)
			
	return false
)

--Returns an array of MaterialID used on an object 
fn GetMatIDs Obj =
(
	MaterialIDList = #()
	
	if classof Obj == Editable_Mesh or classof Obj == Editable_Poly or classof Obj == PolyMeshObject then 
	(
			numFaces = getnumfaces Obj
	
			for i = 1 to numFaces do 
			(
				if classof Obj == Editable_Mesh then 
				(
					CurrentMatID = getfacematid Obj i
				) 
				
				if classof obj == Editable_Poly or classof Obj == PolyMeshObject then 
				(
					CurrentMatID = polyop.getfacematid Obj i
				)
	
				if CurrentMatID != undefined then 
				(
					if finditem MaterialIDList CurrentMatID == 0 then 
					(	
						append MaterialIDList CurrentMatID
					)
				)
			)
	) 
	
	return MaterialIDList
)

--Compares two XRefMaterials and returns true if they are the same
fn CompareXRefMaterials MatA MatB =
(
			if (classof MatA != XRefMaterial) Or (classof MatB != XRefMaterial) then
			(
				return false
			)
			
			if(MatA.srcFileName == MatB.srcFileName) and (MatA.srcItemName == MatB.srcItemName) then
				(
					return true
				)

			return false
)

--Gets the variable index value for the passed Rage_Shader
fn GetVariableIndexValue Mat VariableName = 
	(
		if ( classof Mat != Rage_Shader) then
			return undefined
	
		local numVars = ( RstGetVariableCount Mat )
		for i = 1 to numVars do
		(
			if ( VariableName == ( RstGetVariableName Mat i ) ) then
				return i
		)
		
		return undefined
	)

--Returns true if the two textures are deemed equal
fn CompareTextures TextureA TextureB =
(
	if((TextureA == undefined) and (TextureB == undefined))then 
	(
		return true
	)
	
	if((TextureA == undefined) or (TextureB == undefined))then
	(
		return false
	)

	TextureA = toLower TextureA
	TextureB = toLower TextureB

	--print TextureA
	--print TextureB
	
	if(TextureA == TextureB) then 
	(
		return true
	)
	
	return false
)

--Sets one of the texture maps in the passed rage_shader
fn RageShader_SetTexMap Mat TexMapType TexMapPath =
(
	TexMapTypeIndex = GetVariableIndexValue Mat TexMapType
	if(TexMapTypeIndex != undefined)  then
	(
	RstSetVariable Mat TexMapTypeIndex TexMapPath
	)
)

--Gets the texture alpha map associated with the passed rage_shader
fn RageShader_GetAlphaTexMap Mat =
(
	try
	(
		DiffTexMapTypeIndex = GetVariableIndexValue Mat "Diffuse Texture"
		AlphaTexMapTypeIndex = DiffTexMapTypeIndex + ( ( getNumSubTexmaps Mat ) / 2 )
		AlphaTexMap = getSubTexMap Mat AlphaTexMapTypeIndex
		if(AlphaTexMap != undefined) then
		(
		return AlphaTexMap.filename
		)
		else
		(
		return undefined
		)
	)
	catch
	(
		return undefined
	)
)

--Sets the texture alpha map associated with the passed rage_shader
fn RageShader_SetAlphaTexMap Mat AlphaTexMap =
(
	try(
	setSubTexmap Mat 2 AlphaTexMap
	)
	catch
	()
)

fn RageShader_GetTwoSided Mat =
(
	TexMapTypeIndex = GetVariableIndexValue Mat "Two Sided"
	
	print TexMapTypeIndex
	
	if(TexMapTypeIndex != undefined)  then
	(
	return (RstGetVariable Mat TexMapTypeIndex)
	)
	else
	(
	return (undefined)
	)
)	

--Gets one of the texture maps from the passed rage_shader
fn RageShader_GetTexMap Mat TexMapType =
(
	TexMapTypeIndex = GetVariableIndexValue Mat TexMapType
	
	if(TexMapTypeIndex != undefined)  then
	(
	return (RstGetVariable Mat TexMapTypeIndex)
	)
	else
	(
	return (undefined)
	)
)

--Sets the type of the rage_shader
fn RageShader_SetShaderType Mat ShaderType =
(
	RstSetShaderName Mat ShaderType
)

--Gets the type of the rage_shader
fn RageShader_GetShaderType Mat =
(
	return RstGetShaderName Mat
)

--Compares two Rage Shaders and returns true if they are equivalent
fn CompareRageMaterials MatA MatB =
(

	if (classof MatA != Rage_Shader) Or (classof MatB != Rage_Shader) then
 	(
		return false
 	)
	
	local MatAShaderType = RstGetShaderName MatA
	local MatADiffuseTexture
	local MatADiffuseTextureIndex = GetVariableIndexValue MatA "Diffuse Texture"
	local MatASpecTexture
	local MatASpecTextureIndex = GetVariableIndexValue MatA "Specular Texture"
	local MatABumpTexture
	local MatABumpTextureIndex = GetVariableIndexValue MatA "Bump Texture"
	
	local MatBShaderType = RstGetShaderName MatB
	local MatBDiffuseTexture
	local MatBDiffuseTextureIndex = GetVariableIndexValue MatB "Diffuse Texture"
	local MatBSpecTexture
	local MatBSpecTextureIndex = GetVariableIndexValue MatB "Specular Texture"
	local MatBBumpTexture
	local MatBBumpTextureIndex = GetVariableIndexValue MatB "Bump Texture"
		
	try (MatADiffuseTexture = RstGetVariable MatA MatADiffuseTextureIndex) catch()
	try (MatASpecTexture = RstGetVariable MatA MatASpecTextureIndex) catch()
	try (MatABumpTexture = RstGetVariable MatA MatABumpTextureIndex) catch()

	try (MatBDiffuseTexture = RstGetVariable MatB MatBDiffuseTextureIndex) catch()
	try (MatBSpecTexture = RstGetVariable MatB MatBSpecTextureIndex) catch()
	try (MatBBumpTexture = RstGetVariable MatB MatBBumpTextureIndex) catch()
	
	if(MatAShaderType == MatBShaderType) then
	(
		if(CompareTextures MatADiffuseTexture MatBDiffuseTexture) then()
		else (return false)
		if(CompareTextures MatASpecTexture MatBSpecTexture) then()
		else (return false)
		if(CompareTextures MatABumpTexture MatBBumpTexture) then()
		else (return false)
	)
	else
	(
		return false
	)
			--print MatAShaderType
			--print MatADiffuseTexture
			--print MatASpecTexture
			--print MatABumpTexture
				
return true
)

--Checks a multimaterial for a MaterialID returns true if it exsists
fn MultiMat_CheckForMatID MultiMat MatIDToCheck =
(
		for MatID in MultiMat.materialIDList do
		(
		if(MatID == MatIDToCheck) then
			(
			return true	
			)
		)
		
		return false
)

--Finds the next available MaterialID on a multimaterial.
fn MultiMat_FindNextAvailableMatID MultiMat =
(
	for CurrentID = 1 to 65000 do
	(
		if(MultiMat_CheckForMatID MultiMat CurrentID)then()else
		(
			return CurrentID
		)
	)	
)

--Adds a new slot to the multimaterial with the next available ID number and passes it back
fn MultiMat_AddNewMultiMaterialSlot MultiMat=
(
	NewSlotID = MultiMat_FindNextAvailableMatID MultiMat
	MultiMat.numsubs = MultiMat.materialList.count + 1
	MultiMat.materialIDList[MultiMat.materialList.count] = NewSlotID
	return NewSlotID
)

--Adds a new slot to the multimaterial with the ID passed
fn MultiMat_AddNewMultiMaterialSlotWithID MultiMat SlotID=
(
	MultiMat.numsubs = MultiMat.materialList.count + 1
	MultiMat.materialIDList[MultiMat.materialList.count] = SlotID
	return SlotID
)

--Clears the current material library
fn MatLibrary_ClearLibrary =
(
	for i = 1 to currentMaterialLibrary.count do
	(
					deleteItem currentMaterialLibrary 1
	)
)

--Adds the passed material to the multimaterial and returns the new ID
fn MultiMat_AddMaterial MultiMat MatToAdd =
(
	NewID = MultiMat_AddNewMultiMaterialSlot MultiMat
	MultiMat[NewID] = MatToAdd
	
	return NewID
)

--Adds the passed material to the multimaterial with a specific slot ID. Overwrites the material found in that slot ID if it already exsists.
fn MultiMat_AddMaterialWithID MultiMat MatToAdd SlotID =
(
	if(MultiMat_CheckForMatID MultiMat SlotID == false) then
	(
	MultiMat_AddNewMultiMaterialSlotWithID MultiMat SlotID
	)
	MultiMat[SlotID] = MatToAdd
	
	return NewID
)

--Searchs for a material in a multimaterial and returns the ID if it exsists. Returns false if it doesn't.
fn MultiMat_FindMaterial MultiMat MatToFind=
(
	for MatID in MultiMat.materialIDList do
    (
        if (CompareXRefMaterials MultiMat[MatID] MatToFind == true) then
		(
		return MatID
		)
		
		if (CompareRageMaterials MultiMat[MatID] MatToFind == true) then
		(
		return MatID
		)
    )
	
	return undefined
)

--Searchs for a material in a multimaterial and if it can't find it adds a new material. Returns ID of new or exsisting material.
fn MultiMat_FindOrAddMaterial MultiMat MatToAdd =
(
NewID = MultiMat_FindMaterial MultiMat MatToAdd

if(NewID != undefined) then
(
		return NewID
)

NewID = MultiMat_AddMaterial MultiMat MatToAdd

return NewID
)

--Finds a scene material by name
 fn FindSceneMatByName MatName =
 (
	 for Mat in sceneMaterials  do
	 (
		 if (Mat.name == MatName) then
		 (
			 return Mat
		 )
	 )
	 
	 return undefined
 )
 
 --Checks multimaterial or non-XRefMaterial materials
 fn MultiMat_NoNonXRefMaterials MultiMat = 
 (
	for MatID in MultiMat.materialIDList do
    (
        if (classof MultiMat[MatID] != XRefMaterial) then
		(
		format "Material not XRefMaterial - %\n" (classof MultiMat[MatID])
		return false
		)
    )
	
	return true
 )
 
 --Converts a Rage Shader to a Multimaterial
 fn RageShader_ConvertToMultimaterial ShaderToConvert =
(
	ReturnMultiMat = Multimaterial name: ShaderToConvert.name numsubs:1
	MultiMat_AddMaterialWithID ReturnMultiMat ShaderToConvert 1
	return ReturnMultiMat
)
 
--Sets all the faces to a passed MaterialID only operates on Editable_mesh
fn SetAllFacesToMaterialID Obj IDToSet =
	(
		if classof Obj == Editable_Mesh then 
			(
				FaceCount = getnumfaces Obj
				FaceArray = #()

				for FaceIterator = 1 to FaceCount do 
					(
					setfacematid Obj FaceIterator IDToSet
					)
			) 
	)