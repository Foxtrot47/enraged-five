---------------------------------------------------------------------------------------------------------------------------------------
-- RSL_TXDOpsLite
-- By Paul Truss
-- Senior Technical Artist
-- Rockstar Games London
-- 14/01/2011
--
-- TXD Ops Lite
-- Cut down version of the old TXD Ops
---------------------------------------------------------------------------------------------------------------------------------------

-- dotNet class shortcuts script
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetClasses.ms")
-- dotNet UI functions script
filein (RsConfigGetWildWestDir()+"/script/max/rockstar_london/utils/RSL_dotNetUIOps.ms")

global RSL_TXDOpsLiteUI

struct RSL_TXDOpsLite
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- Struct variables
	---------------------------------------------------------------------------------------------------------------------------------------
	INIFile = (RsConfigGetWildWestDir() + "script/max/Rockstar_London/helpers/TXDOps.ini" ),
	
	TXDArray = #(),
	unHiddenArray = #(),
	
	idxTXD = GetAttrIndex "Gta Object" "TXD",
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- UI variables
	---------------------------------------------------------------------------------------------------------------------------------------
	liteForm,
	TXDListView,
	isolateCheckBox,selectedCheckBox,
	createButton,assignButton,updateButton,
	
	newForm,
	newTXDTextBox,
	okButton,cancelButton,
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- getFormLocation()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn getFormLocation =
	(
		local result = [0,0]
		
		local pos = getINISetting INIFile "Main" "Position"
		
		if pos != "" then
		(
			result = readValue (pos as StringStream)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- findTXD()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn findTXD array TXD =
	(
		local result = 0
		
		for i = 1 to array.count do 
		(
			if (toLower array[i].TXD) == (toLower TXD) then
			(
				result = i
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- gatherTXDs()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn gatherTXDs object =
	(
		if (GetAttrClass object) == "Gta Object" and (classOf object) != XRefObject then
		(
			local fragNode = (getNodeByname (object.name + "_frag_"))
			
			if fragNode != undefined then
			(
				object = fragNode
			)
			
			local TXD = getAttr object idxTXD
			local index = findTXD TXDArray TXD
			
			if index == 0 then
			(
				append TXDArray (dataPair TXD:TXD objectArray:#(object))
			)
			else
			(
				append TXDArray[index].objectArray object
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- isSelected()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn isSelected array =
	(
		local result = false
		
		for entry in array do 
		(
			if entry.isSelected then
			(
				result = true
			)
		)
		
		result
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- addTXDItem()
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn addTXDItem data selected =
	(
		local okToAdd = true
		
		if selected then
		(
			okToAdd = isSelected data.objectArray
		)
		
		if okToAdd then
		(
			local newItem = dotNetObject "ListViewItem" data.TXD
			newItem.tag = dotNetMXSValue data
			newItem.SubItems.add (data.objectArray.count as string)
			
			if (matchPattern data.TXD pattern:"CHANGEME") then
			(
				newItem.backColor = RS_dotNetPreset.errorColour
			)
			
			TXDListView.items.add newItem
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- populateTXDList()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn populateTXDList selected =
	(
		TXDListView.items.clear()
		
		addTXDItem TXDArray selected
		
		TXDListView.AutoResizeColumns (dotNetClass "ColumnHeaderAutoResizeStyle").HeaderSize
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- cb_selectedCheck()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn cb_selectedCheck =
	(
		populateTXDList true
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- cb_newTXDSelectedCheck()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn cb_newTXDSelectedCheck =
	(
		local objectArray = selection as array
		local validSelection = false
		
		for object in objectArray do 
		(
			if (GetAttrClass object) == "Gta Object" and (classOf object) != XRefObject then
			(
				validSelection = true
			)
		)
		
		createButton.enabled = validSelection
		assignButton.enabled = validSelection and (TXDListView.selectedItems.count == 1)
	),
	
	
	mapped fn setTXD object TXD =
	(
		setAttr object idxTXD TXD
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	--
	-- UI FUNCTIONS
	--
	---------------------------------------------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- disableAccelerators() Called when the user enters any control that needs keyboard input e.g. a textBox
	---------------------------------------------------------------------------------------------------------------------------------------
	fn disableAccelerators s e =
	(
		enableAccelerators = false
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOnNewFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOnNewFormClosing s e =
	(
		RSL_TXDOpsLiteUI.newFormClosing s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- newFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn newFormClosing s e =
	(
		liteForm.enabled = true
		cb_newTXDSelectedCheck()
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOkPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOkPressed s e =
	(
		RSL_TXDOpsLiteUI.okPressed s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- okPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn okPressed s e =
	(
		local index = findTXD TXDArray newTXDTextBox.text
		local changeMe = (MatchPattern newTXDTextBox.text pattern:"changeme")
		
		if index == 0 and not changeMe then
		(
			local objectArray = selection as array
			setTXD objectArray newTXDTextBox.text
			
			newForm.close()
			
			TXDArray = #()
			gatherTXDs objects
			populateTXDList selectedCheckBox.checked
		)
		else
		(
			if changeMe then
			(
				messageBox ("The TXD: " + newTXDTextBox.text + " already exists. Please try another name.") title:"Error..."
			)
			else
			(
				messageBox ("You cannot use the name \"CHANGEME\" as a TXD. Please try another name.") title:"Error..."
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchCancelPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchCancelPressed s e =
	(
		RSL_TXDOpsLiteUI.cancelPressed s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- cancelPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn cancelPressed s e =
	(
		newForm.close()
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- newTXDForm()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn newTXDForm =
	(
		liteForm.enabled = false
		
		--Get the max handle pointer.
		local maxHandlePointer=(Windows.GetMAXHWND())
		
		--Convert the HWND handle of Max to a dotNet system pointer
		local sysPointer = DotNetObject "System.IntPtr" maxHandlePointer
		
		--Create a dotNet wrapper containing the maxHWND
		local maxHwnd = DotNetObject "MaxCustomControls.Win32HandleWrapper" sysPointer
		
		newForm = RS_dotNetUI.form "newForm" text:" New TXD" size:[200, 72] borderStyle:RS_dotNetPreset.FB_FixedToolWindow
		newForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").CenterScreen
		dotnet.addEventHandler newForm "FormClosing" dispatchOnNewFormClosing
		dotnet.addEventHandler newForm "Enter" disableAccelerators
		
		newTXDTextBox = RS_dotNetUI.textBox "newTXDTextBox" borderStyle:RS_dotNetPreset.BS_Fixed3D dockStyle:RS_dotNetPreset.DS_Fill
		newTXDTextBox.MaxLength = 18
		dotnet.addEventHandler newTXDTextBox "Enter" disableAccelerators
		
		okButton = RS_dotNetUI.Button "okButton" text:("Ok") dockStyle:RS_dotNetPreset.DS_Fill
		dotnet.addEventHandler okButton "click" dispatchOkPressed
		
		cancelButton = RS_dotNetUI.Button "cancelButton" text:("Cancel") dockStyle:RS_dotNetPreset.DS_Fill
		dotnet.addEventHandler cancelButton "click" dispatchCancelPressed
		
		newTableLayout = RS_dotNetUI.tableLayout "newTableLayout" text:"newTableLayout" collumns:#((dataPair type:"Percent" value:50),(dataPair type:"Percent" value:50)) \
									rows:#((dataPair type:"Absolute" value:24),(dataPair type:"Absolute" value:24)) \
									dockStyle:RS_dotNetPreset.DS_Fill
		
		newTableLayout.controls.add newTXDTextBox 0 0
		newTableLayout.controls.add okButton 0 1
		newTableLayout.controls.add cancelButton 1 1
		
		newTableLayout.setColumnSpan newTXDTextBox 2
		
		newForm.controls.add newTableLayout
		
		newForm.show maxHwnd
	),
	
	
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchOnFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchOnFormClosing s e =
	(
		RSL_TXDOpsLiteUI.onFormClosing s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- onFormClosing()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn onFormClosing s e =
	(
		setINISetting INIFile "Main" "Position" ([liteForm.location.x, liteForm.location.y] as string)
		callbacks.removeScripts id:#RSL_TXDOpsLite
		callbacks.removeScripts id:#RSL_TXDOpsLiteNew
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchTXDListDoubleClick()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchTXDListClick s e =
	(
		RSL_TXDOpsLiteUI.TXDListClick s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchTXDListDoubleClick()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn TXDListClick s e =
	(
		cb_newTXDSelectedCheck()
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchTXDListDoubleClick()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchTXDListDoubleClick s e =
	(
		RSL_TXDOpsLiteUI.TXDListDoubleClick s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- TXDListDoubleClick()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn TXDListDoubleClick s e =
	(
		if s.SelectedItems.count == 1 then
		(
			local TXD = s.selectedItems.item[0].tag.value
			
			select TXD.objectArray
			
			if isolateCheckBox.Checked then
			(
				for object in objects do
				(
					if object.isSelected then
					(
						object.isHidden = false
					)
					else
					(
						object.isHidden = true
					)
				)
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchIsolateChecked()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchIsolateChecked s e =
	(
		RSL_TXDOpsLiteUI.isolateChecked s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- isolateChecked()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn isolateChecked s e =
	(
		if s.checked then
		(
			unHiddenArray = for object in objects where not object.isHidden collect object
		)
		else
		(
			for object in objects do 
			(
				object.isHidden = ((findItem unHiddenArray object) == 0)
			)
			
			unHiddenArray = #()
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchSelectedChecked()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchSelectedChecked s e =
	(
		RSL_TXDOpsLiteUI.selectedChecked s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- selectedChecked()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn selectedChecked s e =
	(
		if s.checked then
		(
			callbacks.addScript #selectionSetChanged "RSL_TXDOpsLiteUI.cb_selectedCheck()" id:#RSL_TXDOpsLite
			cb_selectedCheck()
		)
		else
		(
			callbacks.removeScripts id:#RSL_TXDOpsLite
			populateTXDList false
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchCreatePressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchCreatePressed s e =
	(
		RSL_TXDOpsLiteUI.createPressed s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- createPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn createPressed s e =
	(
		newTXDForm()
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchAssignPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchAssignPressed s e =
	(
		RSL_TXDOpsLiteUI.assignPressed s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- assignPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn assignPressed s e =
	(
		if TXDListView.selectedItems.count == 1 then
		(
			setTXD (selection as array) TXDListView.selectedItems.item[0].text
			
			TXDArray = #()
			gatherTXDs objects
			populateTXDList selectedCheckBox.checked
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- dispatchAssignPressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn dispatchUpdatePressed s e =
	(
		RSL_TXDOpsLiteUI.updatePressed s e
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- updatePressed()
	---------------------------------------------------------------------------------------------------------------------------------------
	fn updatePressed s e =
	(
		TXDArray = #()
		gatherTXDs objects
		populateTXDList selectedCheckBox.checked
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- createForm() Creates the main form
	---------------------------------------------------------------------------------------------------------------------------------------
	fn createForm =
	(
		clearListener()
		
		--In this case, we're going to use a standard form rather than the custom maxForm written for max. this is because I want to change the 
		-- form back colour which you can't do if you use the custom maxForm. For this to work, we need to set the parent of the form to be Max. 
		-- USING THIS TYPE OF FORM MEANS WE NEED TO DISSABLE ACCELERATORS WHEN WE FOCUS ON A CONTROL THAT TAKES
		-- KEY PRESSES SUCH AS SPINNERS AND TEXT BOXES
		
		--Get the max handle pointer.
		local maxHandlePointer=(Windows.GetMAXHWND())
		
		--Convert the HWND handle of Max to a dotNet system pointer
		local sysPointer = DotNetObject "System.IntPtr" maxHandlePointer
		
		--Create a dotNet wrapper containing the maxHWND
		local maxHwnd = DotNetObject "MaxCustomControls.Win32HandleWrapper" sysPointer
		
		liteForm = RS_dotNetUI.form "liteForm" text:" R* TXD Ops Lite" size:[200, 400] min:[128, 128] borderStyle:RS_dotNetPreset.FB_SizableToolWindow
		liteForm.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").Manual
		dotnet.addEventHandler liteForm "FormClosing" dispatchOnFormClosing
		dotnet.addEventHandler liteForm "Enter" disableAccelerators
		
		local location = getFormLocation()
		liteForm.location.x = location.x
		liteForm.location.y = location.y
		
		TXDListView = RS_dotNetUI.listView "TXDListView" dockStyle:RS_dotNetPreset.DS_Fill
		TXDListView.view = (dotNetClass "System.Windows.Forms.View").Details
		TXDListView.HeaderStyle = (dotNetClass "System.Windows.Forms.ColumnHeaderStyle").NonClickable
		TXDListView.Sorting = (dotNetClass "System.Windows.Forms.SortOrder").None
		TXDListView.HideSelection = false
		TXDListView.scrollable = true
		TXDListView.fullRowSelect = true
		TXDListView.columns.add "TXD"
		TXDListView.columns.add "Objects"
		TXDListView.BackColor = RS_dotNetPreset.controlColour_Light
		dotnet.addEventHandler TXDListView "SelectedIndexChanged" dispatchTXDListClick
		dotnet.addEventHandler TXDListView "DoubleClick" dispatchTXDListDoubleClick
		
		isolateCheckBox = RS_dotNetUI.checkBox "isolateCheckBox" text:("Isolate on Select") dockStyle:RS_dotNetPreset.DS_Fill
		isolateCheckBox.TextAlign = (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft
		dotnet.addEventHandler isolateCheckBox "CheckedChanged" dispatchIsolateChecked
		
		selectedCheckBox = RS_dotNetUI.checkBox "selectedCheckBox" text:("Selected Objects") dockStyle:RS_dotNetPreset.DS_Fill
		selectedCheckBox.TextAlign = (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft
		dotnet.addEventHandler selectedCheckBox "CheckedChanged" dispatchSelectedChecked
		
		createButton = RS_dotNetUI.Button "createButton" text:("Create TXD") dockStyle:RS_dotNetPreset.DS_Fill
		dotnet.addEventHandler createButton "click" dispatchCreatePressed
		
		assignButton = RS_dotNetUI.Button "assignButton" text:("Assign Selected") dockStyle:RS_dotNetPreset.DS_Fill
		dotnet.addEventHandler assignButton "click" dispatchAssignPressed
		
		updateButton = RS_dotNetUI.Button "updateButton" text:("Update") dockStyle:RS_dotNetPreset.DS_Fill
		dotnet.addEventHandler updateButton "click" dispatchUpdatePressed
		
		mainTableLayout = RS_dotNetUI.tableLayout "mainTableLayout" text:"mainTableLayout" collumns:#((dataPair type:"Percent" value:50)) \
									rows:#((dataPair type:"Percent" value:50),(dataPair type:"Absolute" value:24),(dataPair type:"Absolute" value:24) \
												,(dataPair type:"Absolute" value:24),(dataPair type:"Absolute" value:24),(dataPair type:"Absolute" value:24)) \
									dockStyle:RS_dotNetPreset.DS_Fill
		
		mainTableLayout.controls.add TXDListView 0 0
		mainTableLayout.controls.add isolateCheckBox 0 1
		mainTableLayout.controls.add selectedCheckBox 0 2
		mainTableLayout.controls.add createButton 0 3
		mainTableLayout.controls.add assignButton 0 4
		mainTableLayout.controls.add updateButton 0 5
		
		liteForm.controls.add mainTableLayout
		
		gatherTXDs objects
		populateTXDList false
		
		callbacks.addScript #selectionSetChanged "RSL_TXDOpsLiteUI.cb_newTXDSelectedCheck()" id:#RSL_TXDOpsLiteNew
		cb_newTXDSelectedCheck()
		
		liteForm.show maxHwnd
	)
)

if RSL_TXDOpsLiteUI != undefined then
(
	try (RSL_TXDOpsLiteUI.liteForm.close()) catch()
)

RSL_TXDOpsLiteUI = RSL_TXDOpsLite()
RSL_TXDOpsLiteUI.createForm()
