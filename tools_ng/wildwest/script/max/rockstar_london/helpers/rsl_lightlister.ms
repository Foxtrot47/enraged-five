--Light Lister 1.3
--Andy Davis �2010 Rockstar London
--Tool provides information about lights in the scene and allows attributes to be edited
------------------------------------------------------------------------------------------------------------------------------------------
--REVISIONS
------------------------------------------------------------------------------------------------------------------------------------------
--1.1: basic functionality for displaying lights
--1.2: implementation of XtraTreeList for a faster control
--1.3: addition of the attribute editor so that a user can choose the listed attributes

fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/Utils/RSL_dotNetClasses.ms")
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/Utils/RSL_dotNetObjects.ms")
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/Utils/RSL_dotNetPresets.ms")
fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/Utils/RSL_dotNetUIOps.ms")
dotnet.loadAssembly (RsConfigGetWildWestDir() + "script/max/Rockstar_London/dll/RGLExportHelpers.dll")

global lightListerInstance
global currentLightListAttributeIDs	--has to be global because we assign the attributes from the ini file using execute
													--hence the studid name

struct LightListerStruct
(
	------------------------------------------------------------------------------------------------------------------------------------------
	--VARIABLES & PRESETS
	------------------------------------------------------------------------------------------------------------------------------------------
	--preset definitions
	CBS_None = (dotNetClass "System.Windows.Forms.TableLayoutPanelCellBorderStyle").None,
	DS_Fill = (dotNetClass "System.Windows.Forms.DockStyle").Fill,
	FBS_FixedToolWindow = (dotNetClass "System.Windows.Forms.FormBorderStyle").FixedToolWindow,
	Font_Calibri = dotNetObject "System.Drawing.Font" "Calibri" 8,
	FS_Flat = (dotNetClass "System.Windows.Forms.FlatStyle").Flat,
	SP_Manual =  (dotNetClass "System.Windows.Forms.FormStartPosition").Manual,
	TA_Center =  (dotNetClass "System.Drawing.ContentAlignment").MiddleCenter,
	TA_Left =  (dotNetClass "System.Drawing.ContentAlignment").MiddleLeft,
	XTL_Checkbox = dotnetobject "DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit",
	XTL_Num = dotnetobject "DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit",
	XTL_Textbox = dotnetobject "DevExpress.XtraEditors.Repository.RepositoryItemTextEdit",
	
	iniFilePath = "X:\\tools\\dcc\\current\\max2009\\plugcfg\\RSL_LightLister.ini",
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--colours
	RGB_OffWhite = (dotNetClass "System.Drawing.Color").fromARGB 241 241 241,
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--attribute variables	
	attrList,	--array to store all the current attribute names
	allAttrs,	--array to store all light attributes
	
	-- DHM 18/5/2010
	-- Ensure the default attribute list only contains attributes that are in the framework attributes file.
	defaultAttrIDs = #(
		( GetAttrIndex "Gta Light" "Cast Static Object Shadow" ),
		( GetAttrIndex "Gta Light" "Cast Dynamic Object Shadow" ),
		( GetAttrIndex "Gta Light" "Light Fade Distance" ),
		( GetAttrIndex "Gta Light" "Volume Fade Distance" ),
		-- DHM 18/5/2010
		-- Removed the following because they are not in the framework attributes file.
		--( GetAttrIndex "Gta Light" "Ambient Light" ), -- attribute added by MP3
		--( GetAttrIndex "Gta Light" "Soft Light" ), -- attribute added by MP3
		--( GetAttrIndex "Gta Light" "Blinding Light" ), -- attribute added by MP3
		( GetAttrIndex "Gta Light" "Day" ),
		( GetAttrIndex "Gta Light" "Night" )
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------	
	--UI variables/preset	
	lightListerForm,
	lightListerTable,
	buttonRowHeight = 34,
	formHeight = 400, --initial value: changed when the XtraTreeList is updated
	rowHeight = 19,
	treeListBorderHeight = 29,
	borderHeight = 28,
	buttonWidth = 58,
	treeListPanel,
	attrColumnWidth = 100,
	nameColumnWidth = 100,
	borderSides = 20,
	formWidth = 640, --init value only
	showActiveLightsOnly = true,	--default value only
	infoPanel,
	checkbox1,	--show active lights only checkbox
	nodeList,		--stores array of all the nodes in the XtraTreeList
	SetColumnsForm,
	SetColumnsTable,
	activeColumn,
	lightList,		--array to store all the scene lights currently in the list
	valueField,
	selectedLights,
	attrEditorForm,
	attrEditorList,
	dataType,
		
	------------------------------------------------------------------------------------------------------------------------------------------
	--LIST FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn UpdateSelectedLights =
	(
		selectedLights = for this in nodeList where (this.Selected == true) and (IsValidNode this.Tag.Value == true) collect this.Tag.Value
	),
	
	fn DecimalToFloat control =
	--necessary to convert XtraTreeList decimal values into Max useable floats
	(
		local dValue = getProperty control #value asDotNetObject:true
		local fValue = (dotNetClass "System.Decimal").ToSingle dValue
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn UpdateLocalConfigFileSave =
	(
		setINISetting iniFilePath "LightLister" "showActiveLightsOnly" (lightListerInstance.checkbox1.checked as string)
		setINISetting iniFilePath "LightLister" "currentAttributes" (currentLightListAttributeIDs as string)
-- 		currentLightListAttributeIDs = getINISetting iniFilePath "LightLister" "currentAttributes" as array
	),
	
	fn UpdateLocalConfigFileLoad =
	(
		try
		(
			lightListerInstance.checkbox1.checked = getINISetting iniFilePath "LightLister" "showActiveLightsOnly" as BooleanClass
		)
		catch()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--UPDATE TABLE
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn UpdateTable =
	(
		if selectedLights != undefined then
		(
			UpdateSelectedLights()
		)
		if (showActiveLightsOnly == true) then
		(
			lightList = for this in lights where (this.parent != undefined) collect this
		)
		else --showActiveLightsOnly == false
		(
			lightList = lights
		)
		if (lightList.Count > 0) then
		(
			activeLights = for this in lights where (this.parent !=undefined) collect this
			infoPanel.Text = "Scene contains " + lights.Count as string + " lights, "
			infoPanel.Text += activeLights.Count as string + " of which are active"
			nodeList = #()	--clear node array for updated nodes
			treeListPanel.ClearNodes()	--clear the nodes from the tree list, should they exist
			treeListPanel.BeginUnboundLoad()
			
			for this in lightList do
			(
				local nodeAttrValues = #(this.name)
				for thisAttr in attrList do
				(
					local attrIndex = ( GetAttrIndex "Gta Light" thisAttr )
					
					if ( undefined == attrIndex ) then
						append nodeAttrValues "AttrIndex Undefined"
					else
						append nodeAttrValues (getattr this attrIndex)
				)
				newNode = treeListPanel.AppendNode nodeAttrValues -1 -- value of -1 puts newNode at the root
				newNode.Tag = dotNetMXSValue this	-- need to tag the new node with the max light
				append nodeList newNode
			)
			treeListPanel.EndUnboundLoad()
		)
		else
		(
			if (showActiveLightsOnly == false) then
			(
				infoPanel.Text = "scene contains no lights"
			)
			else
			(
				if (lights.Count > 0) then infoPanel.Text = "scene contains no active lights"
				else infoPanel.Text = "scene contains no lights"
			)
		)
		
		--select current nodes in the list
		if (selectedLights != undefined) do
		(
			for thisLight in selectedLights do
			(
				for thisNode in nodeList do
				(
					if (thisNode.Tag.Value == thisLight) then
					(
						thisNode.Selected = true
					)
				)
			)
		)
		
		--resize form
		formWidth = nameColumnWidth + borderSides + (attrColumnWidth * attrList.Count)
		formHeight = borderHeight + buttonRowHeight + treeListBorderHeight + (rowHeight * lightList.Count)
		lightListerForm.Size = dotNetObject "System.Drawing.Size" formWidth formHeight
		infoPanel.Width = lightListerForm.Width - checkbox1.Width - (buttonWidth * 3) - 45
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--SETUP TREELIST FUNCTION
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn SetupTreeList =
	(
		--setup attribute list
		attrList = #()
		for id in currentLightListAttributeIDs do
		(
			if ( undefined == id ) then
				append attrList "ID Undefined"
			else
				append attrList allAttrs[id]
		)
		
		treeListPanel.Columns.Clear()
		treeListPanel.BeginUpdate()		
		
		--manually set up first column for light names
		treeListPanel.Columns.Add()
		treeListPanel.Columns.Item[0].Caption = "Light Name"
		treeListPanel.Columns.Item[0].VisibleIndex = 0
		treeListPanel.Columns.Item[0].Width = nameColumnWidth
		treeListPanel.columns.item[0].Sortorder = (dotNetClass "SortOrder").ascending
		treeListPanel.columns.item[0].OptionsColumn.AllowEdit = false
		treeListPanel.columns.item[0].OptionsColumn.AllowSize = false
		treeListPanel.columns.item[0].OptionsColumn.AllowMove = false
		
		--run through the attribute list setting up the appropriate type of control
		for i = 1 to attrList.Count do
		(
			treeListPanel.Columns.Add()
			if (undefined != attrList[i]) then (
				
				treeListPanel.Columns.Item[i].Caption = attrList[i]
			)
			else (
				
				treeListPanel.Columns.Item[i].Caption = "Undefined"
			)
			treeListPanel.Columns.Item[i].VisibleIndex = i
			treeListPanel.Columns.Item[i].Width = attrColumnWidth
			treeListPanel.Columns.Item[i].OptionsColumn.AllowSort = false
			treeListPanel.columns.item[i].OptionsColumn.AllowSize = false
			treeListPanel.columns.item[i].OptionsColumn.AllowMove = false
			
			--need to find out what kind of data the current attribute is
			if ( undefined == currentLightListAttributeIDs[i] ) then
			(
				treeListPanel.Columns.Item[i].ColumnEdit = XTL_Textbox
			)
			else
			(
				dataType = GetAttrType "Gta Light" currentLightListAttributeIDs[i]
				case (dataType) of
				(
					"bool":	treeListPanel.Columns.Item[i].ColumnEdit = XTL_Checkbox
					"float":
					(
						treeListPanel.Columns.Item[i].ColumnEdit = XTL_Num
						treeListPanel.Columns.Item[i].ColumnEdit.IsFloatValue = true
						treeListPanel.Columns.Item[i].ColumnEdit.MinValue = -1
						treeListPanel.Columns.Item[i].ColumnEdit.MaxValue = 9999
					)
					"int":
					(
						treeListPanel.Columns.Item[i].ColumnEdit = XTL_Num
						treeListPanel.Columns.Item[i].ColumnEdit.IsFloatValue = false
						treeListPanel.Columns.Item[i].ColumnEdit.MinValue = -1
						treeListPanel.Columns.Item[i].ColumnEdit.MaxValue = 9999
					)
					"string":
					(
						treeListPanel.Columns.Item[i].ColumnEdit = XTL_Textbox
					)
				)
			)
		)		
		treeListPanel.EndUpdate()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--EVENT HANDLERS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn DispatchRefresh s e =
	(
		lightListerInstance.Refresh s e
	),
	
	fn Refresh s e =
	(
		UpdateSelectedLights()
		lightListerInstance.UpdateTable()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn DispatchToggleActiveLights s e =
	(
		lightListerInstance.ToggleActiveLights s e
	),
	
	fn ToggleActiveLights s e =
	(
		showActiveLightsOnly = checkbox1.Checked
		lightListerInstance.UpdateTable()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------

	fn DispatchClosePopup s e =
	(
		lightListerInstance.ClosePopup s e
	),
	
	fn ClosePopup s e =
	(
		s.Parent.Parent.Close()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn DispatchLoadIniFile s e =
	(
		lightListerInstance.LoadIniFile s e
	),
	
	fn LoadIniFile s e =
	(
		UpdateLocalConfigFileLoad()
	),
	
	fn DispatchSaveIniFile s e =
	(
		lightListerInstance.SaveIniFile s e
	),
	
	fn SaveIniFile s e =
	(
		UpdateLocalConfigFileSave()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--EDIT SINGLE ATTRIBUTE
	------------------------------------------------------------------------------------------------------------------------------------------	
	
	fn DispatchEditLightAttr s e =
	(
		lightListerInstance.EditLightAttr s e
	),
	
	fn EditLightAttr s e =
	(
		objectToEdit = e.Node.Tag.Value																	--scene light
		colID = e.Column.AbsoluteIndex																		--column
		attrName = attrList[colID]																				--attribute being edited
		dataType = GetAttrType "Gta Light" (getAttrIndex "Gta Light" attrName)		--data type of the attribute
		if (IsValidNode objectToEdit) then --if object exists
		(
			if (dataType == "float") then newValue = DecimalToFloat e						--float values have to be cast
			else newValue = e.Value																			--other data types are ok
			setattr objectToEdit (getattrindex "Gta Light" attrList[colID]) newValue
		)
		
		else -- refresh table
		(
			UpdateTable()
		)
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--SELECT LIGHT
	------------------------------------------------------------------------------------------------------------------------------------------	
	
	fn DispatchSelectLight s e =
	(
		lightListerInstance.SelectLight s e
	),
	
	fn SelectLight s e =
	(
		for this in nodeList do
		(
			if (this.Selected == true) then
			(
				select this.Tag.Value
				infoPanel.Text = (this.Tag.Value.Name as string + " selected")
			)
		)
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--HELP WIKI
	------------------------------------------------------------------------------------------------------------------------------------------	
	
	fn DispatchHelpWiki s e =
	(
		lightListerInstance.HelpWiki s e
	),
	
	fn HelpWiki s e =
	(
		shellLaunch "https://mp.rockstargames.com/index.php/Light_Lister" ""
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--ATTRIBUTE EDITOR
	------------------------------------------------------------------------------------------------------------------------------------------	
	
	fn DispatchShowAttrEditor s e =
	(
		lightListerInstance.ShowAttrEditor s e
	),
	
	fn ShowAttrEditor s e =
	(
		attrEditorList.Items.Clear()
		for this in allAttrs do
		(
			attrEditorList.Items.Add this
		)
		for this in currentLightListAttributeIDs do
		(
			attrEditorList.SetItemChecked (this - 1) true
		)
		attrEditorForm.ShowModal()
	),
	
	fn DispatchRebuildAttrList s e =
	(
		lightListerInstance.RebuildAttrList s e
	),
	
	fn RebuildAttrList s e =
	(
		checkedAttrs = attrEditorList.CheckedIndices
		currentLightListAttributeIDs = #()
		for i = 0 to (checkedAttrs.Count - 1) do
		(
			append currentLightListAttributeIDs (checkedAttrs.Item[i] + 1)
		)
		SetupTreeList()
		UpdateTable()
	),
	
	fn DispatchResetAttributes s e =
	(
		lightListerInstance.ResetAttributes s e
	),
	
	fn ResetAttributes s e =
	(
		for i = 0 to (attrEditorList.Items.Count - 1) do
		(
			attrEditorList.SetItemChecked i false
		)
		currentLightListAttributeIDs = defaultAttrIDs
		for this in currentLightListAttributeIDs do
		(
			attrEditorList.SetItemChecked (this - 1) true
		)
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--SET COLUMN VALUES
	------------------------------------------------------------------------------------------------------------------------------------------	
	
	fn GetColumnIDFromX Xval =
	(
		local lastColumn = treeListPanel.Columns.Count - 1
		local columnID = 0
		local nextColumnDist = treeListPanel.Columns.Item[0].VisibleWidth
		while (Xval > nextColumnDist) do
		(
			if columnID == lastColumn then exit
			columnID += 1
			nextColumnDist += treeListPanel.Columns.Item[columnID].VisibleWidth
		)
		return columnID
	),
	
	fn DispatchCheckAll s e =
	(
		lightlisterInstance.CheckAll s e
	),
	
	fn CheckAll s e =
	(
		infoPanel.Text = "Check all lights: " + treeListPanel.Columns.Item[activeColumn].Caption
		for this in lightList do
		(
			if (isValidNode this) then
			(
				setattr this (getattrindex "Gta Light" attrList[activeColumn]) true
			)
		)
		lightListerInstance.UpdateTable()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------	
	
	fn DispatchCheckSelected s e =
	(
		lightListerInstance.CheckSelected s e
	),
	
	fn CheckSelected s e =
	(
		infoPanel.Text = "Check selected lights: " + treeListPanel.Columns.Item[activeColumn].Caption
		UpdateSelectedLights()
		for this in selectedLights do
		(
			if (isValidNode this) then
			(
				setattr this (getattrindex "Gta Light" attrList[activeColumn]) true
			)
		)
		lightListerInstance.UpdateTable()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------	
	
	fn DispatchUncheckAll s e =
	(
		lightlisterInstance.UncheckAll s e
	),
	
	fn UncheckAll s e =
	(
		infoPanel.Text = "Uncheck all lights: " + treeListPanel.Columns.Item[activeColumn].Caption
		for this in lightList do
		(
			if (isValidNode this) then
			(
				setattr this (getattrindex "Gta Light" attrList[activeColumn]) false
			)
		)
		lightListerInstance.UpdateTable()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------	
	
	fn DispatchUncheckSelected s e =
	(
		lightListerInstance.UncheckSelected s e
	),
	
	fn UncheckSelected s e =
	(
		infoPanel.Text = "Uncheck selected lights: " + treeListPanel.Columns.Item[activeColumn].Caption
		UpdateSelectedLights()
		for this in selectedLights do
		(
			if (isValidNode this) then
			(
				setattr this (getattrindex "Gta Light" attrList[activeColumn]) false
			)
		)
		lightListerInstance.UpdateTable()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------	
	
	fn DispatchApplyValue s e =
	(
		lightListerInstance.ApplyValue s e
	),
	
	fn ApplyValue s e =
	(
		case (dataType) of
		(
			"bool":		newValue = valueField.Checked
			"float":		newValue = DecimalToFloat valueField
			"int":		newValue = valueField.Value
			"string":	newValue = valueField.Text
		)		
		infoPanel.Text = "Apply value of " + newValue as string + " to " + treeListPanel.Columns.Item[activeColumn].Caption
		for this in lightList do
		(
			if (isValidNode this) then
			(
				setattr this (getattrindex "Gta Light" attrList[activeColumn]) newValue
			)
		)
		lightListerInstance.UpdateTable()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------	
	
	fn DispatchApplyToSelectedValue s e =
	(
		lightListerInstance.ApplyToSelectedValue s e
	),
	
	fn ApplyToSelectedValue s e =
	(
		case (dataType) of
		(
			"bool":		newValue = valueField.Checked
			"float":		newValue = DecimalToFloat valueField
			"int":		newValue = valueField.Value
			"string":	newValue = valueField.Text
		)
		infoPanel.Text = "Apply value of " + newValue as string + " to " + treeListPanel.Columns.Item[activeColumn].Caption
		UpdateSelectedLights()
		for this in selectedLights do
		(
			if (isValidNode this) then
			(
				setattr this (getattrindex "Gta Light" attrList[activeColumn]) newValue
			)
		)
		lightListerInstance.UpdateTable()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------	
	
	fn SetCheckboxes e =
	(
		columnName = treeListPanel.Columns.Item[activeColumn].Caption
		SetColumnsForm.Text = "Set " + columnName
		SetColumnsForm.Location.X = e.X + 60
		SetColumnsTable.Controls.Clear()
		CheckAllButton = dotNetObject "Button"
		CheckAllButton.Text = "Check All"
		CheckAllButton.Dock = DS_Fill
		CheckAllButton.Font = Font_Calibri
		CheckAllButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		dotNet.AddEventHandler CheckAllButton "Click" DispatchCheckAll
		SetColumnsTable.Controls.Add CheckAllButton 0 0
		CheckSelectedButton = dotNetObject "Button"
		CheckSelectedButton.Text = "Check Selected"
		CheckSelectedButton.Dock = DS_Fill
		CheckSelectedButton.Font = Font_Calibri
		CheckSelectedButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		dotNet.AddEventHandler CheckSelectedButton "Click" DispatchCheckSelected
		SetColumnsTable.Controls.Add CheckSelectedButton 0 1
		UncheckAllButton = dotNetObject "Button"
		UncheckAllButton.Text = "Uncheck All"
		UncheckAllButton.Dock = DS_Fill
		UncheckAllButton.Font = Font_Calibri
		UncheckAllButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		dotNet.AddEventHandler UncheckAllButton "Click" DispatchUncheckAll
		SetColumnsTable.Controls.Add UncheckAllButton 0 2
		UncheckSelectedButton = dotNetObject "Button"
		UncheckSelectedButton.Text = "Uncheck Selected"
		UncheckSelectedButton.Dock = DS_Fill
		UncheckSelectedButton.Font = Font_Calibri
		UncheckSelectedButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		dotNet.AddEventHandler UncheckSelectedButton "Click" DispatchUncheckSelected
		SetColumnsTable.Controls.Add UncheckSelectedButton 0 3
		CloseButton = dotNetObject "Button"
		CloseButton.Text = "Close"
		CloseButton.Dock = DS_Fill
		CloseButton.Font = Font_Calibri
		CloseButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		dotNet.AddEventHandler CloseButton "Click" DispatchClosePopup
		SetColumnsTable.Controls.Add CloseButton 0 4
		SetColumnsForm.ShowModal()
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------	
	
	fn SetNumbers e =
	(
		columnName = treeListPanel.Columns.Item[activeColumn].Caption
		SetColumnsForm.Text = "Set " + columnName
		SetColumnsForm.Location.X = e.X + 80
		SetColumnsTable.Controls.Clear()
		valueField = dotNetObject "NumericUpDown"
		valueField.Maximum = 9999
		valueField.Minimum = -1
		if (dataType == "float") then valueField.DecimalPlaces = 2
		else valueField.DecimalPlaces = 0
		valueField.Increment = 1
		valueField.TextAlign = (dotNetClass "System.Windows.Forms.HorizontalAlignment").Center
		valueField.Dock = DS_Fill
		valueField.Font = Font_Calibri
		valueField.Value = getattr lightList[1] (getattrindex "Gta Light" attrList[activeColumn])
		SetColumnsTable.Controls.Add valueField 0 0
		applyButton = dotNetObject "Button"
		applyButton.Text = "Apply to all lights"
		applyButton.Dock = DS_Fill
		applyButton.Font = Font_Calibri
		applyButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		dotNet.AddEventHandler applyButton "Click" DispatchApplyValue
		SetColumnsTable.Controls.Add applyButton 0 1
		applyToSelectedButton = dotNetObject "Button"
		applyToSelectedButton.Text = "Apply to selected lights"
		applyToSelectedButton.Dock = DS_Fill
		applyToSelectedButton.Font = Font_Calibri
		applyToSelectedButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		dotNet.AddEventHandler applyToSelectedButton "Click" DispatchApplyToSelectedValue
		SetColumnsTable.Controls.Add applyToSelectedButton 0 2
		CloseButton = dotNetObject "Button"
		CloseButton.Text = "Close"
		CloseButton.Dock = DS_Fill
		CloseButton.Font = Font_Calibri
		CloseButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		dotNet.AddEventHandler CloseButton "Click" DispatchClosePopup
		SetColumnsTable.Controls.Add CloseButton 0 4
		SetColumnsForm.ShowModal()		
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------	
	
	fn SetStrings e =
	(
		columnName = treeListPanel.Columns.Item[activeColumn].Caption
		SetColumnsForm.Text = "Set " + columnName
		SetColumnsForm.Location.X = e.X + 80
		SetColumnsTable.Controls.Clear()
		valueField = dotNetObject "TextBox"
		valueField.TextAlign = (dotNetClass "System.Windows.Forms.HorizontalAlignment").Center
		valueField.Dock = DS_Fill
		valueField.Font = Font_Calibri
		valueField.Text = getattr lightList[1] (getattrindex "Gta Light" attrList[activeColumn])
		SetColumnsTable.Controls.Add valueField 0 0
		applyButton = dotNetObject "Button"
		applyButton.Text = "Apply to all lights"
		applyButton.Dock = DS_Fill
		applyButton.Font = Font_Calibri
		applyButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		dotNet.AddEventHandler applyButton "Click" DispatchApplyValue
		SetColumnsTable.Controls.Add applyButton 0 1
		applyToSelectedButton = dotNetObject "Button"
		applyToSelectedButton.Text = "Apply to selected lights"
		applyToSelectedButton.Dock = DS_Fill
		applyToSelectedButton.Font = Font_Calibri
		applyToSelectedButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		dotNet.AddEventHandler applyToSelectedButton "Click" DispatchApplyToSelectedValue
		SetColumnsTable.Controls.Add applyToSelectedButton 0 2
		CloseButton = dotNetObject "Button"
		CloseButton.Text = "Close"
		CloseButton.Dock = DS_Fill
		CloseButton.Font = Font_Calibri
		CloseButton.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		dotNet.AddEventHandler CloseButton "Click" DispatchClosePopup
		SetColumnsTable.Controls.Add CloseButton 0 4
		SetColumnsForm.ShowModal()		
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------	
	
	fn DispatchContextMenu s e =
	(
		lightListerInstance.ContextMenu s e
	),
	
	fn ContextMenu s e =
	(
		if (e.Y < 21) then
		(
			activeColumn = GetColumnIDFromX(e.X)
			if (activeColumn == 0) then
			(
				infoPanel.Text = "double click light to select"
			)
			else
			(
				attrName = attrList[activeColumn]																	--attribute being edited
				dataType = GetAttrType "Gta Light" (getAttrIndex "Gta Light" attrName)		--data type of the attribute
				case (dataType) of
				(
					"bool": SetCheckboxes e
					"float": SetNumbers e
					"int": SetNumbers e
					"string": SetStrings e
				)
			)
		)
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--INITIAL UI SETUP
	------------------------------------------------------------------------------------------------------------------------------------------
	fn CreateUI =
	(		
		-- form setup
		lightListerForm = dotNetObject "maxCustomControls.maxForm"
		lightListerForm.Size = dotNetObject "System.Drawing.Size" formWidth formHeight
		lightListerForm.Text = "R* Light Lister"
		lightListerForm.startPosition = SP_Manual
		lightListerForm.Location = dotNetObject "system.drawing.point" 0 120
		lightListerForm.FormBorderStyle = FBS_FixedToolWindow
		lightListerForm.MaximumSize = dotNetObject "System.Drawing.Size" 1600 1024
		lightListerForm.MinimumSize = dotNetObject "System.Drawing.Size" 512 10
		dotNet.AddEventHandler lightListerForm "Load" DispatchLoadIniFile
		dotNet.AddEventHandler lightListerForm "Closing" DispatchSaveIniFile
		
		--table setup
		lightListerTable = dotNetObject "System.Windows.Forms.TableLayoutPanel"
		lightListerTable.backColor = RGB_OffWhite
		lightListerTable.dock = DS_fill
		lightListerTable.cellBorderStyle = (dotNetClass "System.Windows.Forms.TableLayoutPanelCellBorderStyle").single
		lightListerTable.columnCount = 1
		lightListerTable.columnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		lightListerTable.rowCount =2
		lightListerTable.rowStyles.add (RS_dotNetObject.rowStyleObject "absolute" buttonRowHeight)
		lightListerTable.rowStyles.add (RS_dotNetObject.rowStyleObject "percent" 100)
		lightListerForm.controls.add lightListerTable
		
		------------------------------------------------------------------------------------------------------------------------------------------
		
		--button panel
		buttonPanel = dotNetObject "FlowLayoutPanel"
		buttonPanel.Dock = DS_Fill
		lightListerTable.Controls.Add buttonPanel 0 0
		
		checkbox1 = dotNetObject "checkbox"
		checkbox1.Text = "show active lights only"
		checkbox1.Width = 130
		checkbox1.Checked = showActiveLightsOnly
		checkbox1.Font = Font_Calibri
		buttonPanel.Controls.Add checkbox1
		dotNet.AddEventHandler checkbox1 "CheckedChanged" DispatchToggleActiveLights
		
		infoPanel = dotNetObject "label"
		infoPanel.Width = 150
		infoPanel.Text = "test"
		infoPanel.TextAlign = TA_Center
		infoPanel.BackColor = (dotNetClass "System.Drawing.Color").fromARGB 216 216 216
		infoPanel.Height = buttonRowHeight - 6
		infoPanel.BorderStyle = infoPanel.BorderStyle.FixedSingle
		infoPanel.Font = Font_Calibri
		buttonPanel.Controls.Add infoPanel
		
		button1 = dotNetObject "button"
		button1.Width = buttonWidth
		button1.Text = "refresh"
		button1.Font = Font_Calibri
		button1.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		buttonPanel.Controls.Add button1
		dotNet.AddEventHandler button1 "Click" DispatchRefresh
		
		button2 = dotNetObject "button"
		button2.Width = buttonWidth
		button2.Text = "attributes"
		button2.Font = Font_Calibri
		button2.FlatStyle =  (dotNetClass "System.Windows.Forms.FlatStyle").Flat
		buttonPanel.Controls.Add button2
		dotNet.AddEventHandler button2 "Click" DispatchShowAttrEditor
		
		button3 = dotNetObject "button"
		button3.Width = buttonWidth
		button3.Text = "help"
		button3.Font = Font_Calibri
		button3.FlatStyle =  FS_Flat
		buttonPanel.Controls.Add button3
		dotNet.AddEventHandler button3 "Click" DispatchHelpWiki
		
		------------------------------------------------------------------------------------------------------------------------------------------
		
		--setup  attributes
		numAttr = GetNumAttr "Gta Light"
		allAttrs = #()
		for i = 1 to numAttr do
		(
			append allAttrs (GetAttrName "Gta Light" i)
		)
		--read in attributes from file
		--nb: I perform another atribute read in CreateUI()
		currentLightListAttributeIDs = #()
		try
		(
			attributeString = getINISetting iniFilePath "LightLister" "currentAttributes"
			execute ("currentLightListAttributeIDs = " + attributeString)
		)
		catch
		(
			currentLightListAttributeIDs = defaultAttrIDs
			print "no attributes found: using default attributes"
		)
		
		------------------------------------------------------------------------------------------------------------------------------------------
		
		--xtra tree list
		treeListPanel = dotNetObject "DevExpress.XtraTreeList.TreeList"
		treeListPanel.Dock = DS_Fill
		treeListPanel.OptionsSelection.MultiSelect = true
		treeListPanel.OptionsView.ShowRoot = false
		treeListPanel.OptionsView.ShowIndicator = false
		treeListPanel.OptionsBehavior.Editable = true 						-- set to true on right click...
		treeListPanel.OptionsView.EnableAppearanceEvenRow = true	--colours the rows alternately
		treeListPanel.OptionsView.EnableAppearanceOddRow = true	--colours the rows alternately
		dotNet.AddEventHandler treeListPanel "CellValueChanged" DispatchEditLightAttr
		dotNet.AddEventHandler treeListPanel "DoubleClick" DispatchSelectLight
		dotNet.AddEventHandler treeListPanel "MouseClick" DispatchContextMenu
		SetupTreeList()
		lightListerTable.Controls.Add treeListPanel 0 1
		
		------------------------------------------------------------------------------------------------------------------------------------------
		
		--draw form
		UpdateTable()
		lightListerForm.showModeless()
		lightListerForm
		
		------------------------------------------------------------------------------------------------------------------------------------------
		
		--setup pop-up window (not initially shown)
		SetColumnsForm = dotNetObject "maxCustomControls.maxForm"
		SetColumnsForm.Size = dotNetObject "System.Drawing.Size" 160 200
		SetColumnsForm.StartPosition = SP_Manual
		SetColumnsForm.Location = dotNetObject "System.Drawing.Point" 340 240
		SetColumnsForm.FormBorderStyle = FBS_FixedToolWindow
		SetColumnsTable = dotNetObject "TableLayoutPanel"
		SetColumnsTable.CellBorderStyle = CBS_None
		SetColumnsTable.Dock = DS_Fill
		SetColumnsTable.ColumnCount = 1
		SetColumnsTable.ColumnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		SetColumnsTable.RowCount = 5
		SetColumnsTable.RowStyles.add (RS_dotNetObject.rowStyleObject "percent" 20)
		SetColumnsTable.RowStyles.add (RS_dotNetObject.rowStyleObject "percent" 20)
		SetColumnsTable.RowStyles.add (RS_dotNetObject.rowStyleObject "percent" 20)
		SetColumnsTable.RowStyles.add (RS_dotNetObject.rowStyleObject "percent" 20)
		SetColumnsTable.RowStyles.add (RS_dotNetObject.rowStyleObject "percent" 20)
		SetColumnsForm.Controls.Add SetColumnsTable
		
		--setup attribute editor window (not initially shown)
		attrEditorForm = dotNetObject "maxCustomControls.maxForm"
		attrEditorForm.Size = dotNetObject "System.Drawing.Size" 200 (80 + allattrs.Count * 16)
		attrEditorForm.StartPosition = SP_Manual
		attrEditorForm.Location = dotNetObject "System.Drawing.Point" 640 240
		attrEditorForm.FormBorderStyle = FBS_FixedToolWindow
		attrEditorForm.Text = "Light Attributes Editor"
		attrEditorTable = dotNetObject "TableLayoutPanel"
		attrEditorTable.CellBorderStyle = CBS_None
		attrEditorTable.Dock = DS_Fill
		attrEditorTable.ColumnCount = 1
		attrEditorTable.ColumnStyles.Add (RS_dotNetObject.columnStyleObject "percent" 100)
		attrEditorTable.RowCount = 3
		attrEditorTable.RowStyles.Add (RS_dotNetObject.rowStyleObject "percent" 100)
		attrEditorTable.RowStyles.Add (RS_dotNetObject.rowStyleObject "absolute" 32)
		attrEditorTable.RowStyles.Add (RS_dotNetObject.rowStyleObject "absolute" 32)
		attrEditorForm.Controls.Add attrEditorTable
		attrEditorList = dotNetObject "CheckedListBox"
		attrEditorList.Dock = DS_Fill
		attrEditorList.CheckOnClick = true
		attrEditorTable.Controls.Add attrEditorList 0 0
		attrEditorButton1 = dotNetObject "Button"
		attrEditorButton1.Text = "Reset Attributes"
		attrEditorButton1.Dock = DS_Fill
		attrEditorButton1.Font = Font_Calibri
		attrEditorButton1.FlatStyle = FS_Flat
		attrEditorTable.Controls.Add attrEditorButton1 0 1
		attrEditorbutton2 = dotNetObject "Button"
		attrEditorbutton2.Text = "Close"
		attrEditorbutton2.Dock = DS_Fill
		attrEditorbutton2.Font = Font_Calibri
		attrEditorbutton2.FlatStyle = FS_Flat
		attrEditorTable.Controls.Add attrEditorbutton2 0 2
		dotNet.AddEventHandler attrEditorbutton1 "Click" DispatchResetAttributes
		dotNet.AddEventHandler attrEditorbutton2 "Click" DispatchClosePopup
		dotNet.AddEventHandler attrEditorForm "Closing" DispatchRebuildAttrList
	)
)

if lightListerInstance != undefined then
(
	lightListerInstance.lightListerForm.close()
)

lightListerInstance = LightListerStruct()
lightListerInstance.CreateUI()