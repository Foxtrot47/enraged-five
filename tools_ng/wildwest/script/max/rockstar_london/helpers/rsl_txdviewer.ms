
fileIn "pipeline/util/xml.ms"

struct RS_Geometry
(
	name,
	type,
	position,
	file,
	txd
)

struct RS_Prop
(
	name,
	txd
)

fn gatherXMLs level =
(
	local result = #(#(),#(),#())
	
	local levelPath = (project.independent +"\\levels\\" + level)
	
	local levelIMGs = getFiles (levelPath + "\\*.img")
	local interiorIMGs = getFiles (levelPath + "\\interiors\\*.img")
	local propIMGs = getFiles (levelPath + "\\props\\*.img")
	
	local levelDirectory = ("c:\\tempXML\\" + (pathConfig.stripPathToLeaf levelPath) + "\\")
	local interiorDirectory = ("c:\\tempXML\\" + (pathConfig.stripPathToLeaf levelPath) + "\\interiors\\")
	local propDirectory = ("c:\\tempXML\\" + (pathConfig.stripPathToLeaf levelPath) + "\\props\\")
	
	makeDir levelDirectory
	makeDir interiorDirectory
	makeDir propDirectory
	
	HiddenDOSCommand ("del" + levelDirectory + "*.xml") startpath:"c:\\"
	HiddenDOSCommand ("del" + interiorDirectory + "*.xml") startpath:"c:\\"
	HiddenDOSCommand ("del" + propDirectory + "*.xml") startpath:"c:\\"
	
	local rageEXE = "ragebuilder_0327.exe"
	
	for image in levelIMGs do
	(
		local arguments = (rsConfig.toolsbin + "\\bin\\script\\ExtractXML.rbs -input " + image + " -output " + levelDirectory)
		
		HiddenDOSCommand (rageEXE +" "+ arguments) startpath:(rsConfig.toolsbin + "\\bin\\")
	)
	
	result[1] = getFiles (levelDirectory + "*.xml")
	
	for image in interiorIMGs do
	(
		local arguments = (rsConfig.toolsbin + "\\bin\\script\\ExtractXML.rbs -input " + image + " -output " + interiorDirectory)
		
		HiddenDOSCommand (rageEXE +" "+ arguments) startpath:(rsConfig.toolsbin + "\\bin\\")
	)
	
	result[2] = getFiles (interiorDirectory + "*.xml")
	
	for image in propIMGs do
	(
		local arguments = (rsConfig.toolsbin + "\\bin\\script\\ExtractXML.rbs -input " + image + " -output " + propDirectory)
		
		HiddenDOSCommand (rageEXE +" "+ arguments) startpath:(rsConfig.toolsbin + "\\bin\\")
	)
	
	result[3] = getFiles (propDirectory + "*.xml")
	
	result
)

XMLFiles = gatherXMLs "c_pana"

print XMLFiles[1]

xmlStruct = undefined

fn fixXML xml =
(
	local dataStream = openFile xml
	
	local lineArray = #()
	
	while not eof dataStream do
	(
		append lineArray (readLine dataStream)
	)
	
	close dataStream
	
	local dataStream = openFile xml mode:"w"
	
	for i = 1 to lineArray.count do
	(
		if i < (lineArray.count) then
		(
			format "%\n" lineArray[i] to:dataStream
		)
		else
		(
			format "%" lineArray[i] to:dataStream
		)
	)
	
	flush dataStream
	close dataStream
)

txds = #()
props = #()

fn readPropXML xml =
(
	if (getFiles xml).count == 1 then
	(
		xmlStruct = XmlDocument()
		xmlStruct.init()
		xmlStruct.load xml
		
		local objectArray = #()
		
		for i = 0 to xmlStruct.document.DocumentElement.ChildNodes.Count - 1 do
		(
			local node = xmlStruct.document.DocumentElement.ChildNodes.Item[i]
			
			case node.name of
			(
-- 				default:print node.name
				"objects":
				(
					for j = 0 to node.ChildNodes.Count - 1 do
					(
						local object = node.ChildNodes.Item[j]
						
						append objectArray object
					)
				)
			)
		)
		
		for object in objectArray do
		(
			local name = object.Attributes.Item[1].Value
			local txd
			local type
			
			for i = 0 to object.Attributes.count - 1 do
			(
				if object.Attributes.Item[i].name == "superclass" then
				(
					type = object.Attributes.Item[i].value
				)
			)
			
			case type of
			(
-- 				default:print type
				"geomobject":
				(
					local properties = object.ChildNodes
					
					for i = 0 to properties.count - 1 do
					(
						local property = properties.Item[i]
						
						case property.name of
						(
-- 	 	 					default:print property.name
							"attributes":
							(
								for j = 0 to property.ChildNodes.count - 1 do
								(
									for a = 0 to property.ChildNodes.Item[j].Attributes.count - 1 do
									(
										local attr = property.ChildNodes.Item[j].Attributes.Item[a]
										
										if attr.value == "TXD" then
										(
											txd = property.ChildNodes.Item[j].Attributes.Item[a + 2].value
										)
									)
								)
							)
						)
					)
					appendIfUnique txds txd
					
					local newProp = RS_Prop name:name txd:txd
					
					append props newProp
				)
			)
		)
	)
)

fn getTXDforProp name =
(
	local result = "undefined"
	
	for entry in props do
	(
		if (toLower entry.name) == (toLower name) then
		(
			result = entry.txd
		)
	)
	
	result
)

for xml in XMLFiles[3] do
(
	fixXML xml
	
	format "Reading Prop File:%\n" (filenameFromPath xml)
	
	readPropXML xml
)

format "Total amount of props loaded:%\n" props.count

global geom = #()

fn readTransform node =
(
	local transformNode
	
	for i = 0 to node.ChildNodes.count - 1 do
	(
		local nodeChild = node.ChildNodes.Item[i]
		
		case nodeChild.name of
		(
			"transform":
			(
				transformNode = nodeChild.ChildNodes.Item[0].ChildNodes.Item[0]
			)
		)
	)
	
	local x = readValue ((transformNode.Attributes.ItemOf("x")).value as stringStream)
	local y = readValue ((transformNode.Attributes.ItemOf("y")).value as stringStream)
	local z = readValue ((transformNode.Attributes.ItemOf("z")).value as stringStream)
	
	position = [x,y,z]
)

fn readTXD node =
(
	local attibutesNode
	
	for i = 0 to node.ChildNodes.count - 1 do
	(
		local nodeChild = node.ChildNodes.Item[i]
		
		case nodeChild.name of
		(
			"attributes":
			(
				attibutesNode = nodeChild
			)
		)
	)
	
	local txd
	
	for i = 0 to attibutesNode.ChildNodes.count - 1 do
	(
		local node = attibutesNode.ChildNodes.Item[i]
		
		local txdAttribute = (node.Attributes.ItemOf("name")).value
		
		if txdAttribute == "TXD" then
		(
			txd = (node.Attributes.ItemOf("value")).value
		)
	)
	
	txd
)

fn gatherDataFromGeom node xml =
(
	local name = (node.Attributes.ItemOf("name")).value
	local type = (node.Attributes.ItemOf("superclass")).value
	local position = (readTransform node)
	local file = (getFilenameFile xml)
	local txd = (readTXD node)
	
	appendIfUnique txds txd
	
	RS_Geometry name:name type:type position:position file:file TXD:txd
)

fn gatherDataFromProp node =
(
	local name = (node.Attributes.ItemOf("refname")).value
	local type = "prop"
	local position = (readTransform node)
	local file = getFilenameFile (node.Attributes.ItemOf("reffile")).value
	local txd = (getTXDforProp name)
	
	RS_Geometry name:name type:type position:position file:file
)

fn getChildNode node =
(
	local result
-- 	print node.name
	for i = 0 to node.ChildNodes.count - 1 do
	(
		local nodeChild = node.ChildNodes.Item[i]
		
-- 		print nodeChild.name
		
		if nodeChild.name == "children" then
		(
			result = nodeChild
		)
	)
	
	result
)

fn readInteriorXML xml offset =
(
	if (getFiles xml).count == 1 then
	(
		fixXML xml
		xmlStruct = XmlDocument()
		xmlStruct.init()
		xmlStruct.load xml
		
		local miloNode
		
		for i = 0 to xmlStruct.document.DocumentElement.ChildNodes.Count - 1 do
		(
			local node = xmlStruct.document.DocumentElement.ChildNodes.Item[i]
			
			case node.name of
			(
-- 				default:print node.name
				"objects":
				(
					for j = 0 to node.ChildNodes.Count - 1 do
					(
						local object = node.ChildNodes.Item[j]
						
						local class = (object.Attributes.ItemOf("class")).value
						local superClassAttribute = (object.Attributes.ItemOf("superclass"))
						
						if superClassAttribute != undefined then
						(
							case superClassAttribute.value of
							(
								"geomobject":
								(
									if class != "gta_milotri" then
									(
										local newEntry = (gatherDataFromGeom object xml)
										newEntry.position += offset
										
										append geom newEntry
									)
								)
								"helper":
								(
									if class == "gta_milo" then
									(
										miloNode = object
									)
								)
							)
						)
						else
						(
							format "*****No superclass***** Class:%\n" class
						)
					)
				)
			)
		)
		
		for i = 0 to miloNode.Attributes.Count - 1 do
		(
			local attr = miloNode.Attributes.Item[i]
			format "Name:% Value:%\n" attr.name attr.value
		)
		
		local childrenNode = getChildNode miloNode
		
-- 		format "ChildrenNode Name:%\n" childrenNode.name
		
		local miloRooms = #()
		
		for i = 0 to childrenNode.ChildNodes.count - 1 do
		(
			local node = childrenNode.ChildNodes.Item[i]
-- 			print node.name
			local classAttribute =  (node.Attributes.ItemOf("class"))
			
			if classAttribute != undefined then
			(
-- 				print classAttribute.value
				if classAttribute.value == "gtamloroom" then
				(
					append miloRooms node
				)
			)
		)
		
		for room in miloRooms do
		(
-- 			print room.Attributes.ItemOf("name").value
			childrenNode = getChildNode room
			
			for i = 0 to childrenNode.ChildNodes.count - 1 do
			(
				local object = childrenNode.ChildNodes.Item[i]
-- 				print object.Attributes.ItemOf("name").value
				
				local superClassAttribute = (object.Attributes.ItemOf("superclass"))
				local ClassAttribute = (object.Attributes.ItemOf("class"))
				
				if superClassAttribute == undefined then
				(
					local attibutesNode
					
					for j = 0 to object.ChildNodes.count - 1 do
					(
						local nodeChild = object.ChildNodes.Item[j]
						
						case nodeChild.name of
						(
							"attributes":
							(
								attibutesNode = nodeChild
							)
						)
					)
					
					local attributeClass = (attibutesNode.Attributes.ItemOf("class")).value
					
					case attributeClass of
					(
-- 						default:print attributeClass
						"Gta Object":
						(
							local newEntry = (gatherDataFromProp object)
							newEntry.position += offset
							
							append geom newEntry
						)
					)
				)
				else
				(
					if superClassAttribute.value == "geomobject" then
					(
						local newEntry = (gatherDataFromGeom object xml)
						newEntry.position += offset
						
						append geom newEntry
					)
				)
			)
		)
	)
)

fn readLevelXML xml =
(
	if (getFiles xml).count == 1 then
	(
		fixXML xml
		xmlStruct = XmlDocument()
		xmlStruct.init()
		xmlStruct.load xml
		
		local objectArray = #()
		
		for i = 0 to xmlStruct.document.DocumentElement.ChildNodes.Count - 1 do
		(
			local node = xmlStruct.document.DocumentElement.ChildNodes.Item[i]
			
			case node.name of
			(
-- 				default:print node.name
				"objects":
				(
					for j = 0 to node.ChildNodes.Count - 1 do
					(
						local object = node.ChildNodes.Item[j]
						
						append objectArray object
					)
				)
			)
		)
		
		for object in objectArray do
		(
			local superClassAttribute = (object.Attributes.ItemOf("superclass"))
			local ClassAttribute = (object.Attributes.ItemOf("class"))
			
			if superClassAttribute == undefined then
			(
				local attibutesNode
				
				for i = 0 to object.ChildNodes.count - 1 do
				(
					local nodeChild = object.ChildNodes.Item[i]
					
					case nodeChild.name of
					(
						"attributes":
						(
							attibutesNode = nodeChild
						)
					)
				)
				
				local attributeClass = (attibutesNode.Attributes.ItemOf("class")).value
				
				case attributeClass of
				(
					default:print attributeClass
					"Gta MILOTri":
					(
						local miloXML
						
						for entry in XMLFiles[2] do
						(
							local fileMatch = matchPattern entry pattern:("*" + (getFilenameFile (object.Attributes.ItemOf("reffile")).value) + ".xml")
							
							if fileMatch then
							(
								miloXML = entry
							)
						)
						
						print miloXML
						
						local position = readTransform object
						
						readInteriorXML miloXML position 
					)
					"Gta Object":
					(
						type = "prop"
						append geom (gatherDataFromProp object)
					)
				)
			)
			else
			(
				if superClassAttribute.value == "geomobject" then
				(
					append geom (gatherDataFromGeom object xml)
				)
			)
		)
	)
	else
	(
		
	)
)

-- readLevelXML XMLFiles[1][2] --1

-- for entry in geom do
-- (
-- 	if entry.txd != undefined then
-- 	(
-- 		p = point pos:entry.position size:1.0 wireColor:blue name:entry.txd
-- 	)
-- 	else
-- 	(
-- 		entry.txd = getTXDforProp entry.name
-- 		
-- 		p = point pos:entry.position size:1.0 wireColor:red name:entry.txd
-- 	)
-- )

unRegisterRedrawViewsCallback cb_drawTXDs
completeRedraw()

fn cb_drawTXDs =
(
	for entry in geom do
	(
		drawTXDMarker entry
	)
)

try (delete (posHelp)) catch()

posHelp = point name:"PositionHelper" size:2.0 axistripod:true box:true

fn drawTXDMarker marker =
(
	if (isValidNode posHelp) then
	(
		gw.setTransform (Matrix3 1)
		
		local textColour
		
		if marker.txd == undefined then
		(
			marker.txd = getTXDforProp marker.name
		)
		
		if marker.txd != "undefined" then
		(
			local index = (findItem txds marker.txd)
			textColour = color ((255.0 / txds.count) * index) ((255.0 / txds.count) * index) 0
-- 			textColour = green
		)
		else
		(
			textColour = red
		)
		
		if marker.txd == "pa_gen_vis" then
		(
			local currentPos = marker.position
			
			if (distance currentPos posHelp.pos) < 10 then
			(
				gw.htext (gw.hTransPoint currentPos) (marker.TXD as string) color:textColour
				
				gw.htext (gw.hTransPoint (currentPos + [0,0,0.4])) (marker.name) color:textColour
				
				print marker.name
			)
			else
			(
				gw.hMarker (gw.hTransPoint currentPos) #plusSign color:textColour
			)
			
			gw.enlargeUpdateRect #whole
			gw.updateScreen()
		)
	)
	else
	(
		print "can't find posHelp"
		unRegisterRedrawViewsCallback cb_drawTXDs
		completeRedraw()
	)
)

-- registerRedrawViewsCallback cb_drawTXDs
completeRedraw()



