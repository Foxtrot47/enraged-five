filein "RSL_MaterialFunctions.ms"

try (destroyDialog RSL_XRefMaterialTools_ApplySceneMaterials) catch()

rollout RSL_XRefMaterialTools_ApplySceneMaterials "Applying scene materials" width:250 height:135
(
	progressBar PROGRESSOverall 	"ProgressOverall" pos:[5,5] width:(RSL_XRefMaterialTools_ApplySceneMaterials.width - 10) height:8
	progressBar PROGRESSLocal "ProgressLocal" pos:[5,17] width:(RSL_XRefMaterialTools_ApplySceneMaterials.width - 10) height:8
	edittext TEXTOutput pos:[5,29] width:(RSL_XRefMaterialTools_ApplySceneMaterials.width - 10) height:100 labelOnTop:true enabled:false
	
	struct MaterialIDKey (OldID, NewID)
	local MasterMaterial
	local MaterialIDIterator
	local InvalidMaterial
	
	fn ProcessObj Obj = 
	(			
		MaterialIDKeyArray = #()
	 
		LocalIncrement = 100.0/Obj.material.materialIDList.count as float
		LocalTotal = 0.0
		
		for CurrentID in Obj.material.materialIDList do
		(
			if (CheckForMaterialID Obj CurrentID == true) then
			(
				format "Found ID: % | Processing associated material: % | Material type: %\n" CurrentID Obj.material[CurrentID].name (classOf Obj.material[CurrentID])
				
				if(classOf Obj.material[CurrentID] == XRefMaterial) or (classOf Obj.material[CurrentID] == Rage_Shader) then ()
				else
				(
					if(classOf Obj.material[CurrentID] == Standard) then
					(
					Obj.material[CurrentID].name = "INVALID - CHANGEME"
					Obj.material[CurrentID].diffuseMapAmount = 0
					Obj.material[CurrentID].diffuse = red 
					Obj.material[CurrentID].selfIllumAmount = 50
					)
					else
					(
					Obj.material[CurrentID] = InvalidMaterial
					)
				)
					
				NewID = MultiMat_FindOrAddMaterial MasterMaterial Obj.material[CurrentID]
				OldID = MaterialIDIterator
				
				ChangeMaterialID Obj CurrentID MaterialIDIterator
			
				ThisMaterialIDKey = MaterialIDKey OldID:OldID NewID:NewID
				
				append MaterialIDKeyArray ThisMaterialIDKey
				
				MaterialIDIterator += 1
				
				LocalTotal += LocalIncrement
				PROGRESSLocal.value = LocalTotal 
			)
		)
		
		for SubMaterialIDKey in MaterialIDKeyArray do
		(
			ChangeMaterialID Obj SubMaterialIDKey.OldID SubMaterialIDKey.NewID
		)
		
		Obj.Material = MasterMaterial
		PROGRESSLocal.value = 0
	)

	on RSL_XRefMaterialTools_ApplySceneMaterials open do
	(	
		ObjectArray = selection as array
		clearSelection()	
		clearListener()
		OverallIncrement = 100.0/ObjectArray.count as float
		OverallTotal = 0.0
		
		MaterialIDIterator = 40000		
		MasterMaterial = FindSceneMatByName "Scene_Materials"
		InvalidMaterial = Standardmaterial name:"INVALID - CHANGEME" showInViewport:true diffuse:red selfIllumAmount:50
			
		if(MasterMaterial == undefined) then
		(
				MasterMaterial = Multimaterial name:"Scene_Materials" showInViewport:false numsubs:1
		)
		
		if(classof MasterMaterial != Multimaterial) then
		(
				MasterMaterial.name = MasterMaterial.name + "_OLD"
				MasterMaterial = undefined
				MasterMaterial = Multimaterial name:"Scene_Materials" showInViewport:false numsubs:1
		)
			
		for Obj in ObjectArray do
			(	
				format "--------------------------------------------------------\n"			
				format "Processing object: %\n" Obj.name
				format "--------------------------------------------------------\n"
				
				if superClassOf Obj == GeometryClass then 
				(
					if (classOf Obj == Editable_Poly) or (classOf Obj == Editable_mesh) or (classOf Obj == PolyMeshObject) then
						(
							collapseStack Obj
							
							if(classof Obj.material == multimaterial) or (classof Obj.material == Rage_Shader) then
							(
								if(classof Obj.material == Rage_Shader) then
								(
									NewMultiMatName = Obj.name + "_Material"
									NewMultiMat = Multimaterial name:NewMultiMatName showInViewport:false numsubs:1
									NewMultiMat[1] = Obj.material
									Obj.material = NewMultiMat 
								)
								
								if(Obj.material == MasterMaterial) then
								(
									format "Skipping - Master material already applied\n"
								)
								else
								(
								ProcessObj Obj
								)
							)
							else
							(
								format "Error - Incorrect material type - %\n" (classof Obj.material)
							)
						)
						else
						(
							format "Error - Incorrect class type - %\n" (classof Obj)
						)
				)
				else
				(
					format "Error - Incorrect class type - %\n" (superclassof Obj)
				)
				
				format "--------------------------------------------------------\n\n"
				
				OverallTotal += OverallIncrement
				PROGRESSOverall.value = OverallTotal 
			)
	)
)

CreateDialog RSL_XRefMaterialTools_ApplySceneMaterials style:#(#style_toolwindow,#style_sysmenu)