--RS_NormalTweaker.ms
--2012 Andy Davis
--Rockstar London
--Script provides UI to allow access to control over vertex normals

filein (RsConfigGetWildWestDir() + "/script/max/rockstar_london/utils/RSL_dotNetUIOps.ms")
filein (RsConfigGetWildWestDir() + "/script/max/rockstar_london/utils/RSL_DotNetPresets2.ms")

global NormalTweaker

struct NormalTweakerStruct
(
	--controls
	Form,
	Table,
	ToolTip,
	InfoPanel,
	AlignUpButton,
	ExplodeButton,
	ResetButton,
	IniFilePath = (RsConfigGetWildWestDir() + "script/max/rockstar_london/config/RSL_Tools.ini"),
	SliderLabel,
	SliderBar,
	BlankLabel,
	SinglePanel,
	MultiPanel,
	FlowPanel,
	ContentPanel,
	IDCheckBox,
	PreserveCheckBox,
	ProgBar,
	ProgressValue = 0.0,
	
	--data
	objList,
	faceList,
	
	--presets
	Font_Main = dotNetObject "System.Drawing.Font" "Calibri" 10,
	mainColor = color 100 108 132,
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--EVENT HANDLERS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn ToggleAffectAll s e =
	(
		count = s.Parent.Controls.Count - 1	-- minus 1 because the last control item is a blank label spacer
		s.Parent.Controls.Item[count].Enabled = not s.Parent.Controls.Item[count].Enabled
	),
	
	fn GetProcessData materialTab =
	(
		if (materialTab.Controls.Item[0].Checked) then
		(
			--objList stored in the objectLabel tag
			local objs = materialTab.Controls.Item[1].Tag.Value
			
			for this in objs do
			(
				addModifier this (Edit_Normals())
				append NormalTweaker.objList this
				append NormalTweaker.faceList (NormalTweaker.IntegerToBitArray (this.Faces.Count))
			)
		)
	),
	
	fn TweakNormals s e =
	(
		local QueryScript = (RsConfigGetWildWestDir() + "script/max/rockstar_london/helpers/RSL_NormalTweaker_CB.ms")
		local tweakValue = NormalTweaker.SliderBar.Value / 100.0
		local origSelection = selection as array
		
		--find out which faces are to be processed
		NormalTweaker.objList = #()	--objects to be processed
		NormalTweaker.faceList =#()	--bitarray of faces for each object
		
		--processing involves:
		--1) applying an edit normal modifier to objects that will get affected only
		--2) getting data for the list of objects
		--3) getting data for the respective faces on these objects
		--4) sending the data to functions to perform the appropriate operation
		
		--iterate through the materials
		local numMaterials = NormalTweaker.ContentPanel.Controls.Count - 1
		
		for i = 1 to numMaterials do
		(
			local thisMatTab = NormalTweaker.ContentPanel.Controls.Item[i - 1]
			local mat = thisMatTab.Tag.Value
			
			--process the undefined materials
			if (mat == undefined) then
			(
				NormalTweaker.GetProcessData thisMatTab
			)
			
			else
			(
				--process the single materials
				if (classOf mat != Multimaterial) then
				(
					NormalTweaker.GetProcessData thisMatTab				
				)
				
				--process the multimaterials
				else
				(		
					local objs = thisMatTab.Controls.Item[1].Tag.Value
					
					if (thisMatTab.Controls.Item[2].Checked) then
					(
						for this in objs do
						(
							addModifier this (Edit_Normals())
							append NormalTweaker.objList this
							append NormalTweaker.faceList (NormalTweaker.IntegerToBitArray (this.Faces.Count))
						)
					)
					
					else	--individual material faces are to be tweaked
					(
						local subMatList = #()	--get a list of the checked submaterials
						
						for j = 1 to (thisMatTab.Controls.Item[3].Controls.Count) do
						(
							if (thisMatTab.Controls.Item[3].Controls.Item[j - 1].Checked) then
								append subMatList thisMatTab.Controls.Item[3].Controls.Item[j - 1].Tag.Value
						)
						
						--find the corresponding faces on the objects and note them as bitarrays
						if (subMatList.Count > 0) then
						(
							for thisObj in objs do
							(
								local matFaces = #{}
								
								addModifier thisObj (Edit_Normals())
								
								for k = 1 to thisObj.Faces.Count do
								(
									local thisMatID = polyop.GetFaceMatID thisObj k
									
									if ((findItem subMatList thisMatID) != 0) then
									(
										append matFaces k
									)
								)
								
								if (matFaces.Count != 0) then
								(
									append NormalTweaker.objList thisObj
									append NormalTweaker.faceList matFaces
								)
							)
						)
					)
				)
			)
		)
		
		--process objects
		NormalTweaker.ProgressValue = 0.0
		NormalTweaker.ProgBar.Value = NormalTweaker.ProgressValue
		callbacks.removeScripts #selectionSetChanged id:#NormalTweakerCallbacks
		disableSceneRedraw()
		
		if (s.Tag == "reset") then
		(
			NormalTweaker.InfoPanel.Text = "Normals reset on selection."
			
			for i = 1 to NormalTweaker.objList.Count do
			(
				--addModifier NormalTweaker.objList[i] (Edit_Normals())
				NormalTweaker.ResetNormals NormalTweaker.objList[i] NormalTweaker.faceList[i]
			)	
		)
		
		else
		(
			NormalTweaker.InfoPanel.Text = ("Normals set to " + s.Tag + " on selection by " + NormalTweaker.SliderBar.Value as string  + "%.")
			
			for i = 1 to NormalTweaker.objList.Count do
			(
				--addModifier objList[i] (Edit_Normals())
				NormalTweaker.SetNormalsOnObject NormalTweaker.objList[i] NormalTweaker.faceList[i] tweakValue s.Tag
			)			
		)
		
		NormalTweaker.ProgressValue = 0.0
		NormalTweaker.ProgBar.Value = NormalTweaker.ProgressValue
		subObjectLevel = 0
		select origSelection
		enableSceneRedraw()
		callbacks.addScript #selectionSetChanged fileName:QueryScript id:#NormalTweakerCallbacks
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--GENERAL FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn IntegerToBitArray num =
	(
		ba = #{}
		
		for i = 1 to num do
			append ba i
		
		return ba
	),
	
	fn InitLabel tag align =
	(
		NewLabel = dotNetObject "Label"
		NewLabel.Text = tag
		NewLabel.Tag = tag
		NewLabel.TextAlign = align
		NewLabel.Font = Font_Main
		NewLabel.Dock = DS_Fill
		NewLabel.Margin = Padding_None
		
		return NewLabel
	),
	
	fn InitButton tag =
	(
		NewButton = dotNetObject "Button"
		NewButton.Tag = tag
		NewButton.FlatStyle = FS_Standard
		NewButton.BackColor = RGB_Transparent
		NewButton.ForeColor = RGB_Black
		NewButton.Text = tag
		NewButton.Dock = DS_Fill
		NewButton.Margin = Padding_None
		NewButton.Font = Font_Main
		NewButton.TextAlign = TA_Center
		
		return NewButton
	),
	
	fn InitCheckbox tag =
	(
		NewCheckbox = dotNetObject "Checkbox"
		NewCheckbox.Text = tag
		NewCheckbox.Checked = false
		NewCheckbox.Font = Font_Main
		NewCheckbox.Dock = DS_Fill
		
		return NewCheckbox
	),
	
	fn InitContentPanel =
	(
		NormalTweaker.ContentPanel = dotNetObject "TableLayoutPanel"
		NormalTweaker.ContentPanel.Dock = DS_Fill
		NormalTweaker.ContentPanel.ColumnCount = 1
		NormalTweaker.ContentPanel.BackColor = ARGB 48	48	48
	),
	
	fn InitUndefinedMatTable objList =
	(
		local NewCheckBox
		local NewTable = dotNetObject "TableLayoutPanel"
		
		NewTable.Dock = DS_Fill
		NewTable.RowCount = 2
		NewTable.ColumnCount = 1
		NewTable.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		NewTable.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		NewTable.BackColor = ARGB 128 0 0
		NewTable.Tag = dotNetMXSValue undefined
		
		local infoString = "Objects: " + objList[1].Name
		
		if (objList.Count > 1) then
		(
			for i = 2 to objList.Count do
				infoString += ", " + objList[i].Name
		)
		
		local NewLabel = NormalTweaker.InitLabel infoString TA_Left
		NewLabel.Tag = dotNetMXSValue objList
		
		NewCheckBox = NormalTweaker.InitCheckBox "Undefined Material"
		NewTable.Controls.Add NewCheckBox 0 0
		NewTable.Controls.Add NewLabel 0 1
		
		return NewTable
	),
	
	fn InitSingleMatTable mat objList =
	(
		local NewTable = dotNetObject "TableLayoutPanel"
		
		NewTable.Dock = DS_Fill
		NewTable.RowCount = 2
		NewTable.ColumnCount = 1
		NewTable.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		NewTable.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		NewTable.BackColor = ARGB 0 32 128
		NewTable.Tag = dotNetMXSValue mat
		
		local infoString = "Objects: " + objList[1].Name
		
		if (objList.Count > 1) then
		(
			for i = 2 to objList.Count do
				infoString += ", " + objList[i].Name
		)
		
		local NewLabel = NormalTweaker.InitLabel infoString TA_Left
		NewLabel.Tag = dotNetMXSValue objList
		local NewCheckBox = NormalTweaker.InitCheckBox (classOf mat as string + ": " + mat.Name)
		
		NewTable.Controls.Add NewCheckBox 0 0
		NewTable.Controls.Add NewLabel 0 1
		
		return NewTable
	),
	
	fn InitMultiMatTable mat objList =
	(
		local NewTable = dotNetObject "TableLayoutPanel"
		local matIDs = #()
		
		--get a list of material ID's and their materials from the objList
		
		for obj in objList do
		(
			for i = 1 to obj.faces.count do
				AppendIfUnique matIDs (polyop.GetFaceMatID obj i)
		)
		
		sort matIDs
		
		--build the table
		
		NewTable.Dock = DS_Fill
		NewTable.RowCount = 4
		NewTable.ColumnCount = 1
		NewTable.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		NewTable.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		NewTable.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		NewTable.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		
		NewTable.BackColor = ARGB 0 128 32
		NewTable.Tag = dotNetMXSValue mat
		NewLabel = NormalTweaker.InitLabel ("Multimaterial: " + mat.name) TA_Left
		NewTable.Controls.Add NewLabel 0 0
		
		local infoString = "Objects: " + objList[1].Name
		
		if (objList.Count > 1) then
		(
			for i = 2 to objList.Count do
				infoString += ", " + objList[i].Name
		)
		
		local NewLabel = NormalTweaker.InitLabel infoString TA_Left
		NewLabel.Tag = dotNetMXSValue objList
		
		NewTable.Controls.Add NewLabel 0 1
		local NewCheckBox = NormalTweaker.InitCheckBox "affect all material id's"
		NewCheckBox.Checked = false
		NewTable.Controls.Add NewCheckBox 0 2
		dotNet.AddEventHandler NewCheckBox "CheckedChanged" ToggleAffectAll
		
		local SubMatTable = dotNetObject "TableLayoutPanel"
		SubMatTable.Dock = DS_Fill
		SubMatTable.Tag = dotNetMXSValue matIDs
		SubMatTable.RowCount = matIDs.Count
		SubMatTable.BackColor = ARGB 0 108 12
		
		for i = 1 to matIDs.Count do
		(
			local matName
			local matNumber = findItem mat.MaterialIDList matIDs[i]
			
			if matNumber == 0 then
				matName = "(no material)"
			
			else
				matName = (classOf mat.MaterialList[i] as string + ": " + mat.MaterialList[i].Name)
			
			SubMatTable.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
			newCheckBox = NormalTweaker.InitCheckBox ("[" + matIDs[i] as string + "] " + matName)
			newCheckBox.Tag = dotNetMXSValue matIDs[i]
			SubMatTable.Controls.Add newCheckBox 0 (i - 1)
		)
		
		NewTable.Controls.Add SubMatTable 0 3
		
		return NewTable
	),
	
	fn SplitCasing instring capstart:true =
	(
		local regexObject = dotNetObject (regExType = (dotNetClass "System.Text.RegularExpressions.Regex")) (@"(?<!^)(?=[A-Z])")
		local stringArray = regexObject.split instring
		local returnString = stringArray[1]
			
		if (capstart == true) then
			returnString[1] = toUpper returnString[1]
			
		if stringArray.count > 1 then
		(
			for i = 2 to stringArray.count do
			(
				returnString =  returnString + " " + toLower (stringArray[i])
			)
		)
		
		return returnString
	),
	
	fn UpdateSliderValue s e =
	(
		NormalTweaker.SliderLabel.Text = ("Tweak amount: " + s.Value as string + "%")
	),
	
	fn Refresh =
	(
		--garbage collection
		dgc = dotnetclass "system.gc"
		dgc.collect()
		gc()

		meshObjects = for this in selection where ((superClassOf this == GeometryClass) and (classOf this != XRefObject)) collect this
		
		if (meshObjects.Count == 0) then
		(
			if (NormalTweaker.Table.Tag == "content") then
			(
				NormalTweaker.Table.Controls.Remove NormalTweaker.ContentPanel
				NormalTweaker.Table.Controls.Add NormalTweaker.BlankLabel 0 1
				NormalTweaker.InfoPanel.Text = "Selection: no suitable mesh object selected"
				NormalTweaker.AlignUpButton.Enabled = false
				NormalTweaker.ExplodeButton.Enabled = false
				NormalTweaker.ResetButton.Enabled = false
				NormalTweaker.Table.Tag = "blank"
			)
		)
		
		else
		(
			local materialSet = #()
			local objectSet = #()
			local undefinedSet = #()
			local color1 = NormalTweaker.mainColor
			local color2 = color (NormalTweaker.mainColor.r + 8) (NormalTweaker.mainColor.g + 8) (NormalTweaker.mainColor.b + 8)
			
			--get material data
			
			for this in meshObjects do
			(
				if this.material == undefined then
					append undefinedSet this
				
				else
				(
					local matItem = findItem materialSet this.material
					
					if (matItem == 0) then
					(
						append materialSet this.material
						append objectSet #(this)
					)
					
					else
					(
						append objectSet[matItem] this
					)
				)
			)
			
			--build material table
			NormalTweaker.ContentPanel.Controls.Clear()
			NormalTweaker.ContentPanel.RowStyles.Clear()
			NormalTweaker.ContentPanel.RowCount = materialSet.Count
			
			if (undefinedSet.Count > 0) then
			(
				NormalTweaker.ContentPanel.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 60)
				local matTable = NormalTweaker.InitUndefinedMatTable undefinedSet
				NormalTweaker.ContentPanel.Controls.Add matTable 0 0
			)
			
			if (materialSet.Count > 0) then
			(
				for i = 1 to materialSet.Count do
				(
					local matTable
					
					if (classOf materialSet[i] == MultiMaterial) then
					(
						matTable = NormalTweaker.InitMultiMatTable materialSet[i] objectSet[i]
						NormalTweaker.ContentPanel.Controls.Add matTable 0 NormalTweaker.ContentPanel.Controls.Count
						local numControls = NormalTweaker.ContentPanel.Controls.Count
						local currentMat = NormalTweaker.ContentPanel.Controls.Item[numControls - 1]
						local numSubMats = currentMat.Controls.Item[3].Controls.Count
						local blockHeight = 85 + 25 * numSubMats
						NormalTweaker.ContentPanel.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" blockHeight)
					)
					
					else -- must be a single material
					(
						NormalTweaker.ContentPanel.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 60)
						local matTable = InitSingleMatTable materialSet[i] objectSet[i]
						NormalTweaker.ContentPanel.Controls.Add matTable 0 NormalTweaker.ContentPanel.Controls.Count
					)
				)
			)
			
			--place spacer panel on table
			--annoyingly necessary or the bottom element on the table expands to fill the space
			NormalTweaker.ContentPanel.RowStyles.Add (RS_dotNetObject.rowStyleObject "percent" 100)
			spacerPanel = NormalTweaker.InitLabel "" TA_Center
			NormalTweaker.ContentPanel.Controls.Add spacerPanel 0 NormalTweaker.ContentPanel.Controls.Count
			
			if (NormalTweaker.Table.Tag == "blank") then
			(
				NormalTweaker.Table.Controls.Remove NormalTweaker.BlankLabel
				NormalTweaker.Table.Controls.Add NormalTweaker.ContentPanel 0 1
				NormalTweaker.AlignUpButton.Enabled = true
				NormalTweaker.ExplodeButton.Enabled = true
				NormalTweaker.ResetButton.Enabled = true
				NormalTweaker.Table.Tag = "content"
			)
			
			local matTypes = NormalTweaker.ContentPanel.Controls.Count - 1
			
			if (matTypes == 1) then
				NormalTweaker.InfoPanel.Text = "1 material detected"
			else
				NormalTweaker.InfoPanel.Text = (matTypes as string + " materials detected\nPlease select the materials that you want to affect.")
		)
	),
	
	fn ResetNormals obj faceList =
	(
		normalSet = #{}
		max modify mode
		select obj
		objEditNormalsModifier = obj.Edit_Normals
-- 		NormalTweaker.InfoPanel.Text = ("Resetting normals on " + obj.Name)
		
		--calculate normalSet based on faceList
		for thisFace in faceList do
		(
			faceDegree = objEditNormalsModifier.GetFaceDegree thisFace
		
			for thisCorner in 1 to faceDegree do
			(
				appendIfUnique normalSet (objEditNormalsModifier.getNormalID thisFace thisCorner)
			)
		)
			
		objEditNormalsModifier.Select normalSet
		objEditNormalsModifier.Reset()
		NormalTweaker.ProgressValue += (100 / NormalTweaker.objList.Count)
		NormalTweaker.ProgBar.Value = NormalTweaker.ProgressValue
		
		if (not NormalTweaker.PreserveCheckBox.Checked) then
			convertToPoly obj
	),
	
	fn SetNormalsOnObject obj faceList amount mode =
	(
		upVector = [0,0,1]
		max modify mode
		select obj
		objEditNormalsModifier = obj.Edit_Normals
-- 		NormalTweaker.InfoPanel.Text = ("Setting normals to " + mode + " on " + obj.Name)
		
		for thisFace in faceList do
		(
			faceDegree = objEditNormalsModifier.GetFaceDegree thisFace
			
			for thisCorner in 1 to faceDegree do
			(
				vertexID = objEditNormalsModifier.getVertexID thisFace thisCorner
				normalID = objEditNormalsModifier.getNormalID thisFace thisCorner
				vertexPosition = objEditNormalsModifier.GetVertex vertexID
				currentNormalVector = objEditNormalsModifier.getNormal normalID
				
				if (mode == "up") then
					tweakVector = upVector
				
				if (mode == "explode") then
					tweakVector = normalize (vertexPosition + obj.position  - obj.center)
				
				outputVector = normalize ((amount * (tweakVector - currentNormalVector)) + currentNormalVector)				
				objEditNormalsModifier.SetNormal normalID outputVector
				
				--we need to set the normal to explicit so that the normal tweak is preserved on collapse
				objEditNormalsModifier.SetNormalExplicit normalID
			)
			
			NormalTweaker.ProgressValue += (100.0 / NormalTweaker.objList.Count) / faceList.Count
			NormalTweaker.ProgBar.Value =  NormalTweaker.ProgressValue
		)
		
		--collapse the stack if required
		if (not NormalTweaker.PreserveCheckBox.Checked) then
			convertToPoly obj
	),
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--LOAD/SAVE/CALLBACK FUNCTIONS
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn SaveIniFile =
	(
		setINISetting NormalTweaker.IniFilePath "NormalTweaker" "WinLocX" (NormalTweaker.Form.Location.x as string)
		setINISetting NormalTweaker.IniFilePath "NormalTweaker" "WinLocY" (NormalTweaker.Form.Location.y as string)
		setINISetting NormalTweaker.IniFilePath "NormalTweaker" "WinWidth" (NormalTweaker.Form.Width as string)
		setINISetting NormalTweaker.IniFilePath "NormalTweaker" "WinHeight" (NormalTweaker.Form.Height as string)
		setINISetting NormalTweaker.IniFilePath "NormalTweaker" "TweakValue" (NormalTweaker.SliderBar.Value as string)
		setINISetting NormalTweaker.IniFilePath "NormalTweaker" "IDCheckState" (NormalTweaker.IDCheckBox.Checked as string)
		setINISetting NormalTweaker.IniFilePath "NormalTweaker" "PreserveCheckState" (NormalTweaker.PreserveCheckBox.Checked as string)
		callbacks.removeScripts #selectionSetChanged id:#NormalTweakerCallbacks
	),
	
	fn LoadIniFile =
	(
		--default values
		local WinLocX = 0
		local WinLocY = 40
		local WinWidth = 60
		local WinHeight = 272
		local TweakValue = 25
		local IDCheckState = false
		local PreserveCheckState = false
		local QueryScript = (RsConfigGetWildWestDir() + "script/max/rockstar_london/helpers/RSL_NormalTweaker_CB.ms")
		
		try
		(
			WinLocX = getINISetting NormalTweaker.IniFilePath "NormalTweaker" "WinLocX" as Integer
			WinLocY = getINISetting NormalTweaker.IniFilePath "NormalTweaker" "WinLocY" as Integer
			WinWidth = getINISetting NormalTweaker.IniFilePath "NormalTweaker" "WinWidth" as Integer
			WinHeight = getINISetting NormalTweaker.IniFilePath "NormalTweaker" "WinHeight" as Integer
			TweakValue = getINISetting NormalTweaker.IniFilePath "NormalTweaker" "TweakValue" as Float
			IDCheckState = getINISetting NormalTweaker.IniFilePath "NormalTweaker" "IDCheckState" as BooleanClass
			PreserveCheckState = getINISetting NormalTweaker.IniFilePath "NormalTweaker" "PreserveCheckState" as BooleanClass
		)
		
		catch()
		
		Form.Location = dotNetObject "system.drawing.point" WinLocX WinLocY
		Form.Size = dotNetObject "System.Drawing.Size" WinWidth WinHeight
		NormalTweaker.SliderBar.Value = TweakValue
		NormalTweaker.SliderLabel.Text = ("Tweak amount: " + TweakValue as string + "%")
		NormalTweaker.IDCheckBox.Checked = IDCheckState
		NormalTweaker.PreserveCheckBox.Checked = PreserveCheckState
		NormalTweaker.FlowPanel.Enabled = not NormalTweaker.IDCheckBox.Checked
		callbacks.addScript #selectionSetChanged fileName:QueryScript id:#NormalTweakerCallbacks
		NormalTweaker.Refresh()
	),	
	
	------------------------------------------------------------------------------------------------------------------------------------------
	--UI
	------------------------------------------------------------------------------------------------------------------------------------------
	
	fn CreateUI =
	(
		-- form setup		
		Form = dotNetObject "maxCustomControls.maxForm"
		Form.Text = "RSL Normal Tweaker"
		Form.StartPosition = (dotNetClass "System.Windows.Forms.FormStartPosition").manual
		Form.Location = dotNetObject "system.drawing.point" 0 80
		Form.MaximumSize = dotNetObject "System.Drawing.Size" 1600 1200
		Form.MinimumSize = dotNetObject "System.Drawing.Size" 320 480
		Form.FormBorderStyle = FBS_Sizable
		Form.SizeGripStyle = (dotNetClass "SizeGripStyle").show
		dotNet.AddEventHandler Form "Load" LoadIniFile
		dotNet.AddEventHandler Form "Closing" SaveIniFile
		
		--content
		ToolTip = dotnetobject "ToolTip"
		Table = dotNetObject "TableLayoutPanel"
		Table.Dock = DS_Fill
		Table.RowCount = 10
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 50)
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "percent" 100)
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 15)
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 30)
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 25)
		Table.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 15)
		Table.ColumnCount = 1
		Table.ColumnStyles.add (RS_dotNetObject.columnStyleObject "percent" 100)
		Form.Controls.Add Table
		
		InfoPanel = InitLabel "Selection: none" TA_Left
		Table.Controls.Add InfoPanel 0 0
		
		BlankLabel = InitLabel "Object selected needs to be an editable poly or editable mesh object." TA_LEFT
		BlankLabel.Dock = DS_Fill
		BlankLabel.BackColor = ARGB 48 48 48
		Table.Tag = "blank"
		Table.Controls.Add BlankLabel 0 1
		
		ContentPanel = dotNetObject "TableLayoutPanel"
		ContentPanel.Dock = DS_Fill
		ContentPanel.ColumnCount = 1
		ContentPanel.AutoScroll = true
		ContentPanel.BackColor = ARGB 48	48	48
		
		SliderLabel = InitLabel "Tweak amount: 50%" TA_Left
		Table.Controls.Add SliderLabel 0 2
		
		SliderBar = dotNetObject "TrackBar"
		SliderBar.Dock = DS_Fill
		SliderBar.Minimum = 0
		SliderBar.Maximum = 100
		SliderBar.Value = 50
		SliderBar.TickFrequency = 10
		ToolTip.SetToolTip SliderBar "Set the amount to affect the normals by"
		dotNet.AddEventHandler SliderBar "Scroll" UpdateSliderValue
		Table.Controls.Add SliderBar 0 3
		
		PreserveCheckBox = dotNetObject "Checkbox"
		PreserveCheckBox.Text = "Preserve Modifier Stack"
		PreserveCheckBox.Dock = DS_Fill
		ToolTip.SetToolTip PreserveCheckBox "Leave the edit normals modifier in the stack without collapsing"
		Table.Controls.Add PreserveCheckBox 0 4
		
		RefreshButton = InitButton "Refresh"
		ToolTip.SetToolTip RefreshButton "Refresh tool to reflect changes"
		dotNet.AddEventHandler RefreshButton "Click" Refresh
		Table.Controls.Add RefreshButton 0 5
		
		AlignUpButton = InitButton "Align Normals Up"
		AlignUpButton.Tag = "up"
		ToolTip.SetToolTip AlignUpButton "Align normals based on Z-axis"
		dotNet.AddEventHandler AlignUpButton "Click" TweakNormals
		Table.Controls.Add AlignUpButton 0 6
		
		ExplodeButton = InitButton "Explode Normals"
		ExplodeButton.Tag = "explode"
		ToolTip.SetToolTip ExplodeButton "Align normals based on center"
		dotNet.AddEventHandler ExplodeButton "Click" TweakNormals
		Table.Controls.Add ExplodeButton 0 7
		
		ResetButton = InitButton "Reset Normals"
		ResetButton.Tag = "reset"
		ToolTip.SetToolTip ResetButton "Reset vertex normals to default value"
		dotNet.AddEventHandler ResetButton "Click" TweakNormals
		Table.Controls.Add ResetButton 0 8
		
		ProgBar = dotNetObject "ProgressBar"
		ProgBar.Dock = DS_Fill
		ProgBar.Value = 0
		ProgBar.Minimum = 0
		ProgBar.Maximum = 100
		Table.Controls.Add ProgBar 0 9
		
		SinglePanel = dotNetObject "TableLayoutPanel"
		SinglePanel.Dock = DS_Fill
		SinglePanel.RowCount = 2
		SinglePanel.RowStyles.add (RS_dotNetObject.rowStyleObject "absolute" 20)
		SinglePanel.RowStyles.add (RS_dotNetObject.rowStyleObject "percent" 100)
		SinglePanel.Tag = "single"
		
		IDCheckBox = dotNetObject "CheckBox"
		IDCheckBox.Text = "affect all material id's"
		IDCheckBox.Checked = false
		IDCheckBox.Dock = DS_Fill
		dotNet.AddEventHandler IDCheckBox "CheckedChanged" ToggleAffectAll
		SinglePanel.Controls.Add IDCheckBox 0 0
		
		FlowPanel = dotNetObject "FlowLayoutPanel"
		FlowPanel.Dock = DS_Fill
		--FlowPanel.BackColor = ARGB 255 255 255
		FlowPanel.FlowDirection = FlowPanel.FlowDirection.TopDown
		SinglePanel.Controls.Add FlowPanel 0 1
		
		MultiPanel = dotNetObject "ListBox"
		MultiPanel.Dock = DS_Fill
		MultiPanel.Tag = "multi"
		
		--draw form
		Form.ShowModeless()
		Form
	)
)

if NormalTweaker != undefined then
(
	NormalTweaker.Form.Close()
)

NormalTweaker = NormalTweakerStruct()
NormalTweaker.CreateUI()