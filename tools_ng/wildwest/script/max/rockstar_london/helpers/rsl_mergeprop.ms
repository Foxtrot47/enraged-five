fileIn (RsConfigGetWildWestDir() + "script/max/Rockstar_London/pipeline/RSL_LevelConfig.ms")

try (destroyDialog RSL_propMerger) catch()

rollout RSL_propMerger "R* Prop Merger" width:150
(
	local foundNode
	local TXDindex = getattrindex "Gta Object" "TXD"
	local dynamicIndex = getattrindex "Gta Object" "Is Dynamic"
	local fragmentIndex = getattrindex "Gta Object" "Is Fragment"
	
	checkBox chkb_fragment "Fragment Props"
	checkBox chkb_dynamic "Dynamic Props"
	editText edt_TXD "TDX:" text:"CHANGEME" labelOnTop:true width:140 offset:[-7,0]
	button btn_merge "Merge Selected" width:140
	
	fn extract level list filetype mapType:"" =
	(
		local rageEXE = "ragebuilder_0327.exe"
		local outputDirectory
		local result = #()
		
		progressStart ("Extracting ." + filetype)
		for i = 1 to list.count do
		(
			local image = list[i]
			
			outputDirectory = "c:\\temp_" + filetype + "\\" + level + "\\" + mapType + "\\" + (getFilenameFile image)
			
			makedir outputDirectory
			HiddenDOSCommand ("del " + outputDirectory + "\\*." + filetype) startpath:"c:\\"	
			
			outputDirectory += "\\"
			
			local arguments = (" x:\\tools\\bin\\script\\Extracter.rbs -input " + image + " -output " + outputDirectory + " -filetype " + filetype)
			local rageCommand = (rageEXE + arguments)
			
			HiddenDOSCommand rageCommand startpath:"x:\\tools\\bin\\"
			
			result += (getFiles (outputDirectory + "*." + filetype))
			
			local updateValue = (i as float / list.count) * 100.0
			progressUpdate updateValue
		)
		progressEnd()
		
		result
	)
	
	fn getXMLNodeByName XMLNode name =
	(
		local result
		
		for i = 0 to XMLNode.ChildNodes.Count - 1 do
		(
			local currentNode = XMLNode.ChildNodes.Item[i]
			
			if (toLower currentNode.name) == (toLower name) then
			(
				result = currentNode
			)
		)
		
		result
	)
	
	fn findNodeByName XMLNode nodeName =
	(
		for i = 0 to XMLNode.ChildNodes.Count - 1 do
		(
			local currentNode = XMLNode.ChildNodes.Item[i]
			local name = (currentNode.Attributes.ItemOf "name")
			
			if name != undefined and (toLower name.value) == (toLower nodeName) then
			(
				foundNode = currentNode
				exit
			)
			else
			(
				if currentNode.ChildNodes.Count > 0 then
				(
					findNodeByName currentNode nodeName
				)
			)
		)
	)
	
	fn recurseChildren childNode name &array =
	(
		for i = 0 to childNode.ChildNodes.Count - 1 do
		(
			local child = childNode.ChildNodes.Item[i]
			local name = (child.Attributes.ItemOf "name")
			
			if name != undefined then
			(
				append array name.value
			)
			
			local childElement = getXMLNodeByName child "children"
			
			if childElement != undefined then
			(
				recurseChildren childElement name array 
			)
		)
	)
	
	fn getTypeAndChildren xmlFile name =
	(
		local result
		
		local xml = XmlDocument()
		xml.init()
		xml.load xmlFile
		
		local objectsElement = getXMLNodeByName xml.document.DocumentElement "objects"
		
		foundNode = undefined
		findNodeByName objectsElement name
		
		if foundNode != undefined then
		(
			result = #("",#())
			
			local attributesElement = getXMLNodeByName foundNode "attributes"
				
			for a = 0 to attributesElement.ChildNodes.Count - 1 do
			(
				local attribute = attributesElement.ChildNodes.Item[a]
				
				local type = (attribute.Attributes.ItemOf "name").value
				local value = (attribute.Attributes.ItemOf "value").value
				
				case of
				(
					(type == "Is Fragment" and value == "true"):result[1] = "fragment"
					(type == "Is Dynamic" and value == "true" and result == ""):resul[1] = "dynamic"
				)
			)
			
			local childNode = getXMLNodeByName foundNode "children"
			
			if childNode != undefined then
			(
				recurseChildren childNode name &result[2]
			)
		)
		else
		(
			format "Failed to find % in %\n" name xmlFile
		)
		
		result
	)
	
	fn extractPropXmlFiles imageArray =
	(
		local propXmlFiles = extract levelConfig.level imageArray "xml" mapType:"props"
	)
	
	fn getPropParent array =
	(
		local result
		
		for entry in array do
		(
			if entry.parent == undefined then
			(
				result = entry
			)
		)
		
		result
	)
	
	fn getPropByName array =
	(
		local result = #()
		
		for entry in array do
		(
			local nodeArray = getNodeByName entry exact:true all:true
			
			for node in nodeArray do
			(
				if (isValidNode node) and (classOf node) != XrefObject then
				(
					append result node
				)
			)
		)
		
		result
	)
	
	on btn_merge pressed do
	(
		local propArray = for object in selection where (classOf object) == XRefObject collect object
		
		if propArray.count > 0 then
		(
			local okToContinue = queryBox (propArray.count as string + " Xrefs selected\n" + "Are you sure you want to merge the selection?") title:"Warning..."
			
			if edt_TXD.text.count == 0 then
			(
				okToContinue = queryBox "TXD name is invalid, do you want it set to \"CHANGEME\"?" title:"Warning..."
				
				if okToContinue then
				(
					edt_TXD.text = "CHANGEME"
				)
			)
			
			if okToContinue then
			(
				local failedArray = #()
				local rpfFileArray = #()
				local newArray = #()
				
				for entry in propArray do
				(
					local rpfFile = project.independent + "\\levels\\" + levelConfig.level + "\\props\\" + (getFilenameFile entry.filename) + ".rpf"
					
					if (doesFileExist rpfFile) then
					(
						append rpfFileArray rpfFile
					)
				)
				
				local xmlArray = extract levelConfig.level rpfFileArray "xml" mapType:"props"
				
				progressStart "Merging Props..."
				
				for i = 1 to propArray.count do
				(
					local prop = propArray[i]
					local propNames = #(prop.objectName)
					local xmlFile = "c:\\temp_xml\\" + levelConfig.level + "\\props\\" + (getFilenameFile prop.filename) + "\\" + (getFilenameFile prop.filename) + ".xml"
					
					if (doesFileExist xmlFile) then
					(
						local typeAndChildren = getTypeAndChildren xmlFile prop.objectName
						
						if typeAndChildren != undefined then
						(
							local okToContinue = true
							
							case of
							(
								(typeAndChildren[1] == "fragment" and not chkb_fragment.checked):okToContinue = false
								(typeAndChildren[1] == "dynamic" and not chkb_dynamic.checked):okToContinue = false
							)
							
							if okToContinue then
							(
								propNames += typeAndChildren[2]
								local merged = mergeMAXFile prop.filename propNames #select #mergeDups #renameMtlDups #alwaysReparent
								
								if merged then
								(
									local currentPropArray = (getPropByName propNames)
									for object in currentPropArray do object.isHidden = false
									
									local parent = getPropParent currentPropArray
									parent.transform = prop.transform
									parent.name = prop.name
									parent.wireColor = red --(color 255 63 255)
									
									for item in currentPropArray where (getAttrClass item) == "Gta Object" do
									(
										setAttr item TXDindex edt_TXD.text
										setAttr item dynamicIndex false
										setAttr item fragmentIndex false
									)
									
									newArray += currentPropArray
								)
								else
								(
									append failedArray (prop.name + " : Failed to mergeMAXFile()")
								)
							)
							else
							(
								append failedArray (prop.name + " : Failed because prop is a " + typeAndChildren[1] + " prop.")
							)
						)
						else
						(
							append failedArray (prop.name + " : Failed to find childNode in xml : " + xmlFile)
						)
					)
					else
					(
						append failedArray (prop.name + " : Failed to find xml file : " + xmlFile)
					)
					
					progressUpdate ((i as float) / propArray.count * 100.0)
				)
				
				progressEnd()
				
				if failedArray.count > 0 then
				(
					local errorString = stringStream ""
					
					format "The following props failed...\n\n" to:errorString
					
					for entry in failedArray do
					(
						format "%\n" entry to:errorString
					)
					
					format "%" (errorString as string) to:listener
					messageBox (errorString as string) title:"Error..."
				)
				
				if newArray.count > 0 then
				(
					delete propArray
					select newArray
				)
			)
		)
		else
		(
			messageBox "No Xrefs in the selection" title:"Error..."
		)
	)
)

createDialog RSL_propMerger style:#(#style_toolwindow,#style_sysmenu)