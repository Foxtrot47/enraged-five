
try (destroyDialog RSL_AICover) catch()

rollout RSL_AICover "RS AI Tagger" width:170
(

	local idxnoAICover = getattrindex "Gta Object" "Does Not Provide AI Cover"
	local idxDoesNotAvoidByPeds = getattrindex "Gta Object" "Don't Avoid By Peds"	
	local idxdynamic = getattrindex "Gta Object" "Is Dynamic"
	local idxFixed = getattrindex "Gta Object" "Is Fixed"
	local idxFragment = getattrindex "Gta Object" "Is Fragment"
	local idxAttribute = getattrindex "Gta Object" "Attribute"
	
	checkbox chk_noAICover "Does Not Provide AI Cover" checked:true
	checkbox chk_noPedAvoid "Don't Avoid By Peds" checked:true
	button btn_Process "Set Flags" width:160 align:#center	
	button btn_Remove "Remove Flags" width:160 align:#center
	button btn_Select "Select Flagged Objects" width:160 align:#center
	label lbl_txt "Works on a selection or entire" align:#left
	label lbl_txt2 "scene if nothing is selected" align:#left
	
	-- Variable to determine if we are setting or removing the flags
	local setflags = true
	
	-- FUNCTIONS --	
	
	fn SetObj obj =
	(
		print ("Found Valid Object: " + obj.name)
		if chk_noAICover.checked then
		(
			if setflags == true then
				setattr obj idxnoAICover true
			else
				setattr obj idxnoAICover false
		)
		if chk_noPedAvoid.checked then
		(--- Additional Check to determine if the object is a door or not
			if getattr obj idxAttribute != 7 then
				if setflags == true then
					setattr obj idxDoesNotAvoidByPeds true
				else
					setattr obj idxDoesNotAvoidByPeds false
		)			
	)
	
	mapped fn ProcessScene obj =
	(
		isGTAObject = (getAttrClass obj) == "Gta Object"
		
		if isGTAObject then
		(
			if getattr obj idxdynamic == true and getattr obj idxFixed == false then
				SetObj obj 
			else
			if getattr obj idxFragment == true then
				SetObj obj	
		)
	)	
	
	
	fn main =
	(
		if selection.count == 0 then
			ProcessScene (rootnode.children)
		else
		(
			local validObjs = #()
			for i in selection where i.parent == undefined do
				append validObjs i
			ProcessScene (validObjs)
		)
			
			
	)
	
	fn selFlagObjects =
	(
		clearSelection()
		for obj in rootnode.children do
		(
			isGTAObject = (getAttrClass obj) == "Gta Object"
			if isGTAObject then
			(
				if chk_noAICover.checked then
				(
					if getattr obj idxnoAICover then selectmore obj
				)
				if chk_noPedAvoid.checked then
				(				
					if getattr obj idxDoesNotAvoidByPeds then selectmore obj
				)
			)
				
		)
	)
	
	
	-- EVENT HANDLERS --
	
	on btn_Process pressed do
	(
		setflags = true
		main()
	)
	
	on btn_Remove pressed do
	(
		setflags = false
		main()
	)
	
	on btn_Select pressed do
	(
		selFlagObjects()
	)
	
)

createDialog RSL_AICover style:#(#style_toolwindow,#style_sysmenu)


