-- levels config update --
clearlistener()
struct GetLevelconfig 
(
	--configdir = rsconfig.toolsbin + "\\config\\content\\",
	--configdir = project.globalConfig,
	-- props
	-- interiors
	
	pathnames = (filterstring maxfilepath "\\"),
	fn levelname = 	
		(
			mapname = undefined
			for n = 1 to pathnames.count do
			(
				if (tolower pathnames[n]) == "maps" then 
				(
					mapname = pathnames[n+1]
				)
			)
			mapname
		),
	fn mapimage =
		(
			image = (filterstring maxfilename ".")[1]
		),
	fn mapimagetype =
		(
			type = "" -- used to move back a directory
			for n = 1 to pathnames.count do
			(
				if (tolower pathnames[n]) == "props" then type = "props"
				if (tolower pathnames[n]) == "interiors" then type = "interiors"
			)
			type
		),
		/*
	fn readxml file=
	(
		local stream = #()	
		fileHandle  = openfile (configdir)
			while eof fileHandle == false do
			(	
				append stream (readline fileHandle )
			)	
		close  fileHandle 
		stream
	),
	fn writexml file stream =
	(
		fileHandle  = openfile (configdir)
			for s in stream do
			(	
				format s to:fileHandle 
			)	
		close  fileHandle 
		
	),
	*/
	fn updatexml =
	(
		-- check for map name if not exist add it
		-- else cehck if already added else add it
	),
	level = levelname(),
	image = mapimage(),
	imagetype=mapimagetype()

)

Levelconfig = GetLevelconfig()

