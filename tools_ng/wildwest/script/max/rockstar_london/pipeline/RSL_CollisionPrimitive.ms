
struct RSL_CollisionPrimitive
(
	---------------------------------------------------------------------------------------------------------------------------------------
	-- compareHandle() Designed to be used in a qSort function. Compares the handle's for v1 and v2 so that we sort an array
	-- of objects by creation order.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn compareHandle v1 v2 =
	(
		case of
		(
			(v1.inode.handle > v2.inode.handle):1
			(v2.inode.handle > v1.inode.handle):-1
			defaul:0
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- recurseGetMILORoom() Recursively traverses the hierarchy tree of the parsed object looking for a MILO Room.
	-- If found, sets the parsed result to the found MILO Room object.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn recurseGetMILORoom object &result =
	(
		if (classOf object) == GtaMloRoom then
		(
			result = object
		)
		else
		(
			if (isValidNode object.parent) then
			(
				RSL_CollisionPrimitive.recurseGetMILORoom object.parent &result
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- setRoomID() Attempts to set the room ID for each parse object. The parsed array variable much contain one or more
	-- MILO Rooms
	---------------------------------------------------------------------------------------------------------------------------------------
	mapped fn setRoomID object array =
	(
		if (getAttrClass object) == "Gta Collision" and (classOf object) != Col_Mesh then
		(
			RSL_CollisionPrimitive.recurseGetMILORoom object &miloRoom
			
			if miloRoom != undefined then
			(
				local ID = findItem array miloRoom
				
				if ID > 0 then
				(
					setAttr object (getAttrIndex "Gta Collision" "Room ID") ID
				)
			)
		)
	),
	
	---------------------------------------------------------------------------------------------------------------------------------------
	-- setRoomIDForPrimitives() Attempts to set the room ID attribute on all collision primitives. Primitives need to be in a 
	-- MILO Room hierarchy for the ID to be set.
	---------------------------------------------------------------------------------------------------------------------------------------
	fn setRoomIDForPrimitives =
	(
		local miloRoomArray = for object in helpers where (classOf object) == GtaMloRoom collect object
		-- I don't think we need to qSort the array but we do so just to be sure the array is sorted by creation order.
		qSort miloRoomArray RSL_CollisionPrimitive.compareHandle
		
		RSL_CollisionPrimitive.setRoomID helpers miloRoomArray
	)
)

-- RSL_CollisionPrimitive.setRoomIDForPrimitives()

