
if RsMapSetupGlobals == undefined then
(
	fileIn "pipeline/export/maps/globals.ms"
)

-- struct RSL_MapFilePropData
-- (
-- 	name,
-- 	sceneXmlDef,
-- 	propArray = #()
-- )

struct RSL_SceneXml
(
	baseDll = (RsConfigGetBinDir() + "//RSG.Base.dll"),
	sceneXmlDll = (RsConfigGetBinDir() + "//RSG.SceneXml.dll"),
	
	baseAssembly,
	sceneXmlAssembly,
	
	rageEXE = "ragebuilder_0327.exe",
	
	dllsLoaded = false,
	
	sceneXmlObject,
	lastXml,
	
	fn loadAssembly =
	(
		baseAssembly = (dotNetClass "System.Reflection.Assembly").loadfrom baseDll
		sceneXmlAssembly = (dotNetClass "System.Reflection.Assembly").loadfrom sceneXmlDll
		
		if baseAssembly == undefined then
		(
			format "---------------------\nFalied to load RSG.Base.dll\n---------------------\n"
			dllsLoaded = false
		)
		
		if sceneXmlAssembly == undefined then
		(
			format "---------------------\nFalied to load RSG.SceneXml.dll\n---------------------\n"
			dllsLoaded = false
		)
		
		if baseAssembly != undefined and sceneXmlAssembly != undefined then
		(
			format "---------------------\nBase and SceneXml dlls loaded\n---------------------\n"
			dllsLoaded = true
		)
	),
	
	fn extractXml level image mapType:"" =
	(
		local result = #()
		
		local imageName = (getFilenameFile image)
		local outputDirectory = "c:\\temp_" + "xml" + "\\" + level + "\\" + mapType + "\\" + imageName
		makedir outputDirectory
		outputDirectory += "\\"
		
		result = (outputDirectory + imageName + "." + "xml")
		
		if lastXml == undefined or lastXml.file != result or (getFileModDate image) != lastXml.time then
		(
			local arguments = (" x:\\tools\\bin\\script\\Extracter.rbs -input " + image + " -output " + outputDirectory + " -filetype xml")
			local rageCommand = (rageEXE + arguments)
			
			HiddenDOSCommand rageCommand startpath:"x:\\tools\\bin\\"
			
			lastXml = dataPair file:result time:(getFileModDate image)
		)
		
		result
	),
	
	fn RPFfromXref XrefObject mapType =
	(
		local result
		
		if rsMapLevel != undefined then
		(
			local rpfFile = project.independent + "/levels/" + rsMapLevel + "/" + mapType + "/" + (getFilenameFile XrefObject.filename) + ".rpf"
			
			if (doesFileExist rpfFile) then
			(
				result = rpfFile
			)
			else
			(
				format "Missing RFP:%\n" rpfFile
			)
		)
		
		result
	),
	
	fn getMapType filename =
	(
		local result = ""
		
		if MatchPattern filename pattern:"*\\props\\*" then
		(
			result = "props"
		)
		if MatchPattern filename pattern:"*\\interiors\\*" then
		(
			result = "interiors"
		)
		
		result
	),
	
	fn instanceLightFromXml data object parentPosition =
	(
		local childPosition = data.ObjectTransform.D
		local offset = [childPosition.x - parentPosition.x, childPosition.y - parentPosition.y, childPosition.z - parentPosition.z]
		local direction = data.ObjectTransform.ToEulers()
		
		local newInstance = RageLightInstance()
		newInstance.setLightType data.class		
		newInstance.parent = object
		newInstance.pos = object.pos	
		newInstance.dir = Point3 direction.x direction.y direction.z
		newInstance.pos += offset
		newInstance.originalOffset = newInstance.pos - object.pos
		newInstance.createChangeHandler()
		
		--Color, Attenutation, Hotspot, Falloff
		local colourProperty = data.GetProperty("Colour")
		if colourProperty != undefined then
		(
			newInstance.delegate.lightColour = color colourProperty.x colourProperty.y colourProperty.z 0
	
			--HACK:  The easiest way is to set newInstance.delegate.lightColour * 255.0 but there appears to be some very strange
			--rounding error where instead of the number being truncated it is being rounded.  And later on, there's an assumption it is truncated
			--which cause some confusion later on.
			newInstance.delegate.lightColour.r = (newInstance.delegate.lightColour.r * 255.0) as integer
			newInstance.delegate.lightColour.g = (newInstance.delegate.lightColour.g * 255.0) as integer
			newInstance.delegate.lightColour.b = (newInstance.delegate.lightColour.b * 255.0) as integer
		)
		
		local multiplierProperty = data.GetProperty("Multiplier") 
		if multiplierProperty != undefined then
		(
			newInstance.delegate.lightIntensity = multiplierProperty
		)
		
		local hotspotProperty = data.GetProperty("Hotspot")
		if hotspotProperty != undefined then
		(
			newInstance.delegate.lightHotspot = hotspotProperty
		)
		
		local falloffProperty = data.GetProperty("Fallsize")
		if falloffProperty != undefined then
		(
			newInstance.delegate.lightFalloff = falloffProperty 
		)
		
		local endAttenuationProperty = data.GetProperty("Attenuation End")
		if endAttenuationProperty != undefined then
		(
			newInstance.delegate.lightAttenEnd = endAttenuationProperty
		)
		
		-- Copy across any attributes.
		local attributesList = data.ConvertAttributesToArray()
		for attributeIndex = 1 to attributesList.Count do 
		(
			local currentAttributeKey = attributesList[attributeIndex].AttributeName
			local currentAttributeValue = attributesList[attributeIndex].AttributeObject
			
			local lightClassName = GetAttrClass newInstance --This should be a "Gta LightPhoto" or a "Gta Light"
			local numAttributes = GetNumAttr lightClassName 
			
			--Attributes' names are case-sensitive.  Must convert these strings to names and do a 
			--manual attribute test.
			local currentAttributeName = currentAttributeKey as name
			local attributeIndex = undefined
			for index = 1 to numAttributes do
			(	
				local attributeName = (GetAttrName lightClassName index) as name 
				if attributeName == currentAttributeName then
				(
					attributeIndex = index
					exit  --The attribute has been found.
				)
			)
	
			if attributeIndex != undefined then
			(
				SetAttr newInstance attributeIndex currentAttributeValue
			)
		)
		
		newInstance.resetToggle = true
	),
	
	fn recurseGetLight objectDef parentPosition =
	(
		for i = 1 to objectDef.Children.Count do
		(
			local child = objectDef.Children[i]
			
			-- if the child is a light, create an instance light.
			if child.Superclass == "light" then
			(
				instanceLightFromXml child object parentPosition
			)
			
			recurseGetLight child parentPosition
		)
	),
	
	mapped fn getInstanceLight object =
	(
		-- check to see if the object has not been deleted.
		if (isValidNode object) then
		(
			-- get the map type and rpf file. map type could be "props", "interiors" or "" (level map file)
			local mapType = getMapType object.filename
			local rpfFile = RPFfromXref object mapType
			
			-- so long as we can find the independent rpf...
			if rpfFile != undefined then
			(
				-- extract the xml if we need to
				local xmlFile = extractXml rsMapLevel rpfFile mapType:mapType
				
				sceneXmlObject = dotNetObject "RSG.SceneXml.Scene" xmlFile
				
				local objectDef = sceneXMLObject.FindObject object.objectName
				
				local parentPosition = objectDef.ObjectTransform.D
				
				recurseGetLight objectDef parentPosition
			)
			else
			(
				format "Unable to get rpf file from Xref Object:\n\tName:%\n\tFile:%" object.name object.filename
			)
		)
	),
	
	fn recurseCheckForLight objectDef &result =
	(
		for i = 1 to objectDef.Children.Count do
		(
			local child = objectDef.Children[i]
			
			-- if the child is a light, create an instance light.
			if child.Superclass == "light" then
			(
				result = true
			)
			else
			(
				recurseCheckForLight child &result
			)
		)
	),
	
	fn hasPropLight object sceneXmlObject =
	(
		local result = false
		
		if (isValidNode object) then
		(
			local objectDef = sceneXMLObject.FindObject object.objectName
			
			if objectDef != undefined then
			(
				recurseCheckForLight objectDef &result
			)
		)
		
		result
	),
	
	fn compareFilename v1 v2 =
	(
		case of
		(
			(v1.filename > v2.filename):1
			(v2.filename > v1.filename):-1
			default:0
		)
	),
	
	fn getXmlByName array name =
	(
		local result
		
		for entry in array do 
		(
			if (stricmp (getFilenameFile entry) name) == 0 then
			(
				result = entry
			)
		)
		
		result
	),
	
	fn findXmlByName array name =
	(
		local result = 0
		
		for i = 1 to array.count do 
		(
			if (stricmp (getFilenameFile array[i]) name) == 0 then
			(
				result = i
			)
		)
		
		result
	),
	
	fn getPropsWithLights =
	(
		local result = #()
		RsMapSetupGlobals()
		
		if baseAssembly == undefined or sceneXmlAssembly == undefined then (loadAssembly())
		
		local xrefArray = for object in objects where (isKindOf object XRefObject) collect object
-- 		local xrefArray = for object in (selection as array) where (isKindOf object XRefObject) collect object
-- 		qsort xrefArray compareFilename
		
		local RPFArray = #()
		local xmlArray = #()
		local sceneObjectArray = #()
		
		for object in xrefArray do 
		(
			if object.filename != undefined then
			(
				local rpf = (RPFfromXref object (getMapType object.filename))
				
				if rpf != undefined then
				(
					appendIfUnique RPFArray rpf
				)
			)
		)
		
		progressStart "Extracting Xml Files..."
		for i = 1 to RPFArray.count do 
		(
			local rpf = RPFArray[i]
			local xml = (extractXml rsMapLevel rpf mapType:(getMapType rpf))
			
			append xmlArray xml
			append sceneObjectArray (dotNetObject "RSG.SceneXml.Scene" xml)
			
			progressUpdate (i as float / RPFArray.count * 100.0)
		)
		progressEnd()
		
		progressStart "Collecting Light Props..."
		
		for i = 1 to xrefArray.count do 
		(
			local index = findXmlByName xmlArray (getFilenameFile xrefArray[i].filename)
			
			if index > 0 then
			(
				local sceneXmlObject = sceneObjectArray[index]
				
				if (hasPropLight xrefArray[i] sceneXmlObject) then
				(
					append result xrefArray[i]
				)
			)
			
			progressUpdate (i as float / xrefArray.count * 100.0)
		)
		
		progressEnd()
		
		result
	),
	
	fn importLights =
	(
		-- if the dlls have not been loaded, try to load them
		if baseAssembly == undefined or sceneXmlAssembly == undefined then (loadAssembly())
		
		-- if the dlls have been loaded
		if dllsLoaded then
		(
			RsMapSetupGlobals()
			-- get all the xRef objects in the selection
			local xrefArray = for object in (selection as array) where (isKindOf object XRefObject) collect object
			
			-- sor the aray by object.filename so that we onlt extract xml files when we need to
			qsort xrefArray compareFilename
			
			getInstanceLight xrefArray
		)
		else
		(
			messageBox "Failed to load dlls. Check the listener to see which one failed." title:"Error..."
		)
	),
	
	fn readSceneXml xmlFile =
	(
		if baseAssembly == undefined or sceneXmlAssembly == undefined then (loadAssembly())
		
		if dllsLoaded then
		(
			sceneXmlObject = dotNetObject "RSG.SceneXml.Scene" xmlFile
			
			for sceneObject in sceneXmlObject.Objects do 
			(
				local attributesArray = sceneObject.ConvertAttributesToArray()
			)
		)
		else
		(
			messageBox "Failed to load dlls. Check the listener to see which one failed." title:"Error..."
		)
	),
	
	fn getUser xmlFile =
	(
		local result = ""
		
		if baseAssembly == undefined or sceneXmlAssembly == undefined then (loadAssembly())
		
		if dllsLoaded then
		(
			local tempXml = (getFilenamePath xmlFile) + (getFilenameFile xmlFile) + "_temp.xml"
			copyFile xmlFile tempXml
			
			local sceneXml = dotNetObject "RSG.SceneXml.Scene" tempXml
			
			result = sceneXml.ExportUser
			
			sceneXml = undefined
			
			deleteFile tempXml
		)
		
		result
	),
	
	fn isInIPL objectDef =
	(
		local result = true
		
		local attributesArray = objectDef.ConvertAttributesToArray()
		
		for entry in attributesArray while result do 
		(
			if entry.AttributeName == "dont add to ipl" and entry.AttributeObject then
			(
				result = false
			)
		)
		
		result
	),
	
	fn getUsedPropArray xmlArray =
	(
		local result = #()
		
-- 		local tempDef = dotNetObject "RSG.SceneXml.Scene" xmlArray[1]
-- 		local objectDef = tempDef.objects[1]
-- 		showProperties objectDef
		
		if baseAssembly == undefined or sceneXmlAssembly == undefined then (loadAssembly())
		
		if dllsLoaded then
		(
			for xml in xmlArray do 
			(
				local sceneXmlDef = dotNetObject "RSG.SceneXml.Scene" xml
				
				for object in sceneXmlDef.objects do 
				(
					if object.RefName != undefined and object.AttributeClass == "gta object" and (isInIPL object) then
					(
						appendIfUnique result object.RefName
					)
				)
			)
		)
		
		result
	),
	
	mapped fn getMapFilePropData xml &result =
	(
		if baseAssembly == undefined or sceneXmlAssembly == undefined then (loadAssembly())
		
		if dllsLoaded then
		(
			local newData = dataPair name:(getFilenameFile xml) propArray:#()
-- 			local newData = RSL_MapFilePropData name:(getFilenameFile xml) --sceneXmlDef:(dotNetObject "RSG.SceneXml.Scene" xml)
			local sceneXmlDef = (dotNetObject "RSG.SceneXml.Scene" xml)
			
			for object in sceneXmlDef.objects do 
			(
				if object.RefName != undefined and object.AttributeClass == "gta object" and (isInIPL object) then
				(
					appendIfUnique newData.propArray object.RefName
				)
			)
			
-- 			sceneXmlDef = undefined
-- 			gc()
			
			append result newData
		)
	)
)

global RSL_SceneXmlOps = RSL_SceneXml()
-- select (RSL_SceneXmlOps.getPropsWithLights())